#if !defined(AFX_DEVTHREAD_H__59548F81_F6CF_430C_9297_79FB7DEA6A94__INCLUDED_)
#define AFX_DEVTHREAD_H__59548F81_F6CF_430C_9297_79FB7DEA6A94__INCLUDED_

#include <afxtempl.h>
#include "DevSock.h"	// Added by ClassView

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DevThread.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CDevThread thread

class CDevThread : public CWinThread
{
	DECLARE_DYNCREATE(CDevThread)
protected:
	CDevThread();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:
	CCmdPkt * GetResponse ( HANDLE hEvent );
	UCHAR m_RxSerialBuf[4][MAX_SERIAL_DATA];
	UINT  m_RxBufCnt[4];
	bool m_bDone;
	bool m_bConnected;
	bool m_bConnecting;
	SCODE TransmitCommand (unsigned long Cmd, const unsigned char* data, unsigned long Len, PHANDLE pEvent);
	SCODE TransmitCommand (unsigned long Cmd, CString sData, PHANDLE pEvent);
	_SERIAL_PACKET * GetSerialData ( short nPort );
	CPtrList m_PS2Data;
	BYTE m_DigitalInputs;
	virtual ~CDevThread();
	void Shutdown ( void );

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDevThread)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();
	//}}AFX_VIRTUAL

// Implementation
protected:
	void CheckConnection ( void );
	void OnConnectComplete ( UINT uParam, LONG lParam );
	bool m_bAddressChanged;
	bool ProcessTxCue ( void );
	void UpdatePendingCue ( void );
	long m_CurrentBatchID;
	CDevSock *m_pSocket;
	UINT m_nPort;
	CString m_sAddress;
	HANDLE m_hExit;
	HANDLE m_hTxCueHasData;
	CDiagnosticCriticalSection m_SerialCueSentinel;
	CDiagnosticCriticalSection m_PS2CueSentinel;
	CDiagnosticCriticalSection m_PendingCueSentinel;
	CDiagnosticCriticalSection m_TxCueSentinel;
	CDiagnosticCriticalSection m_ResponseCueSentinel;
	HWND m_hNotifyWnd;
	CMap < ULONG, ULONG, CCmdPkt *, CCmdPkt *> m_PendingCue;
	CMap < HANDLE, HANDLE, CCmdPkt *, CCmdPkt *> m_ResponseCue;
	CPtrList m_TxCue;
	CPtrList m_RxSerial[NUM_SERIAL_PORTS];

	// Generated message map functions
	//{{AFX_MSG(CDevThread)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	
	afx_msg void OnSocketData (WPARAM nParam, LPARAM lParam);
	afx_msg void OnSetWindowHandle (WPARAM nParam, LPARAM lParam);
	afx_msg void OnAddressChange (WPARAM nParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEVTHREAD_H__59548F81_F6CF_430C_9297_79FB7DEA6A94__INCLUDED_)
