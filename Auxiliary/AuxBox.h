// AuxBox.h: interface for the CAuxBox class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AUXBOX_H__C8E9E383_9EAB_42BD_B86C_D59DA5706E55__INCLUDED_)
#define AFX_AUXBOX_H__C8E9E383_9EAB_42BD_B86C_D59DA5706E55__INCLUDED_

#include "AuxBoxApi.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CDevThread;

class AUXBOX_API CAuxBox  
{
public:
	LONG SetIpAddress(LPCTSTR lpszAddress);
	CString GetIpAddress ( void );
	bool IsOpen ( void );
	LONG SetDigitalOutputs(VARIANT FAR *vOutput);
	LONG GetSerialData(short nPort, VARIANT *sBuf, short *nSize);
	LONG GetDigitalInputs(VARIANT FAR* vInput);
	LONG GetPS2Data(VARIANT FAR* pData, short *nSize);
	LONG SetSerialPortParams(short PortID, short Enabled, LPCTSTR Baud, LPCTSTR Parity, LPCTSTR Databits, LPCTSTR Stopbits);
	LONG PutSerialData(short PortID, VARIANT FAR* sData, short nCnt);
	LONG SetStrobe(short Port, short StrobeID, short State);
	void Close ( void );
	LONG Open ( LPCTSTR lpszIpAddress, UINT SocketPort, HWND hWnd);
	CAuxBox();
	virtual ~CAuxBox();

	bool HasDigitalInputError ();
	void SetDigitalState (bool bLowInk, bool bSystemReady);

protected:
	bool m_bOpen;
	UINT m_nPort;
	CString m_sAddress;
	void ThrowError ( LONG Err, const int resID );
	void ThrowError ( LONG Err, LPCTSTR pStr );
	CDevThread * m_pDevThread;
	BYTE m_nLastDigitalInput;
};

#endif // !defined(AFX_AUXBOX_H__C8E9E383_9EAB_42BD_B86C_D59DA5706E55__INCLUDED_)
