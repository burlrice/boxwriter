// Endian.h: interface for the CEndian class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ENDIAN_H__97D281E0_E1DC_49B7_9521_C7084C53F37D__INCLUDED_)
#define AFX_ENDIAN_H__97D281E0_E1DC_49B7_9521_C7084C53F37D__INCLUDED_

#include "TypeDefs.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CEndian  
{
public:
	static void hton (_SOCKET_PACKET &sm);
	static void ntoh (_SOCKET_PACKET &sm);
	static void hton (_SOCKET_PACKET *sm);
	static void ntoh (_SOCKET_PACKET *sm);
	CEndian();
	virtual ~CEndian();

};

#endif // !defined(AFX_ENDIAN_H__97D281E0_E1DC_49B7_9521_C7084C53F37D__INCLUDED_)
