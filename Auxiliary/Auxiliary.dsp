# Microsoft Developer Studio Project File - Name="Auxiliary" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=Auxiliary - Win32 IP Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Auxiliary.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Auxiliary.mak" CFG="Auxiliary - Win32 IP Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Auxiliary - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "Auxiliary - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "Auxiliary - Win32 IP Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "Auxiliary - Win32 IP Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Auxiliary - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\Editor\DLL\Common" /I "..\Editor\DLL\Database" /I "..\Editor\DLL\Debug" /D "DLLEXPORT" /D "_WINDLL" /D "_AFXDLL" /D "_AFXEXT" /D "VALVEJET" /D "NDEBUG" /D "_UNICODE" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 Database.lib /nologo /subsystem:windows /dll /machine:I386 /libpath:"..\Editor\DLL\Database\Release"
# Begin Custom Build - Updating Files
TargetPath=.\Release\Auxiliary.dll
TargetName=Auxiliary
InputPath=.\Release\Auxiliary.dll
SOURCE="$(InputPath)"

"\FoxJet\$(TargetName).dll" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Echo $(TargetPath) \FoxJet 
	Copy $(TargetPath) \FoxJet 
	
# End Custom Build

!ELSEIF  "$(CFG)" == "Auxiliary - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\Editor\DLL\Common" /I "..\Editor\DLL\Database" /I "..\Editor\DLL\Debug" /D "DLLEXPORT" /D "_WINDLL" /D "_AFXDLL" /D "_AFXEXT" /D "VALVEJET" /D "_DEBUG" /D "_UNICODE" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Debug.lib Database.lib /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept /libpath:"..\Editor\DLL\Database\Debug" /libpath:"..\Editor\DLL\Debug\Debug"
# Begin Custom Build - Updating Files
TargetPath=.\Debug\Auxiliary.dll
TargetName=Auxiliary
InputPath=.\Debug\Auxiliary.dll
SOURCE="$(InputPath)"

BuildCmds= \
	Echo $(TargetPath) \FoxJet \
	Copy $(TargetPath) \FoxJet \
	Echo $(TargetPath) \FoxJet\IP \
	Copy $(TargetPath) \FoxJet\IP \
	

"\FoxJet\$(TargetName).dll" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)

"\FoxJet\IP\$(TargetName).dll" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ELSEIF  "$(CFG)" == "Auxiliary - Win32 IP Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Auxiliary___Win32_IP_Release"
# PROP BASE Intermediate_Dir "Auxiliary___Win32_IP_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "IP_Release"
# PROP Intermediate_Dir "IP_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "NDEBUG" /D "DLLEXPORT" /D "WIN32" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_AFXEXT" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\Editor\DLL\Common" /I "..\Editor\DLL\Database" /I "..\Editor\DLL\Debug" /D "NDEBUG" /D "DLLEXPORT" /D "_WINDLL" /D "_AFXDLL" /D "_AFXEXT" /D "_UNICODE" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 Database.lib /nologo /subsystem:windows /dll /machine:I386 /libpath:"..\Editor\DLL\Database\IP_Release"
# Begin Custom Build - Updating Files
TargetPath=.\IP_Release\Auxiliary.dll
TargetName=Auxiliary
InputPath=.\IP_Release\Auxiliary.dll
SOURCE="$(InputPath)"

"\FoxJet\IP\$(TargetName).dll" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Echo $(TargetPath) \FoxJet\IP 
	Copy $(TargetPath) \FoxJet\IP 
	
# End Custom Build

!ELSEIF  "$(CFG)" == "Auxiliary - Win32 IP Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Auxiliary___Win32_IP_Debug"
# PROP BASE Intermediate_Dir "Auxiliary___Win32_IP_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "IP_Debug"
# PROP Intermediate_Dir "IP_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "_DEBUG" /D "DLLEXPORT" /D "WIN32" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_AFXEXT" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\Editor\DLL\Common" /I "..\Editor\DLL\Database" /I "..\Editor\DLL\Debug" /D "DLLEXPORT" /D "_WINDLL" /D "_AFXEXT" /D "_DEBUG" /D "_AFXDLL" /D "_UNICODE" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Debug.lib Database.lib /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept /libpath:"..\Editor\DLL\Debug\IP_Debug" /libpath:"..\Editor\DLL\Database\IP_Debug"
# Begin Custom Build - Updating Files
TargetPath=.\IP_Debug\Auxiliary.dll
TargetName=Auxiliary
InputPath=.\IP_Debug\Auxiliary.dll
SOURCE="$(InputPath)"

"\FoxJet\IP\$(TargetName).dll" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Echo $(TargetPath) \FoxJet\IP 
	Copy $(TargetPath) \FoxJet\IP 
	
# End Custom Build

!ENDIF 

# Begin Target

# Name "Auxiliary - Win32 Release"
# Name "Auxiliary - Win32 Debug"
# Name "Auxiliary - Win32 IP Release"
# Name "Auxiliary - Win32 IP Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AuxBox.cpp
# End Source File
# Begin Source File

SOURCE=.\Auxiliary.cpp
# End Source File
# Begin Source File

SOURCE=.\Auxiliary.def
# End Source File
# Begin Source File

SOURCE=.\Auxiliary.rc
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409 /i "..\Editor\DLL\Common\\"
# End Source File
# Begin Source File

SOURCE=.\CmdPkt.cpp
# End Source File
# Begin Source File

SOURCE=.\DebugLog.cpp
# End Source File
# Begin Source File

SOURCE=.\DevSock.cpp
# End Source File
# Begin Source File

SOURCE=.\DevThread.cpp
# End Source File
# Begin Source File

SOURCE=.\Endian.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AuxBox.h
# End Source File
# Begin Source File

SOURCE=.\AuxBoxApi.h
# End Source File
# Begin Source File

SOURCE=.\CmdPkt.h
# End Source File
# Begin Source File

SOURCE=.\DebugLog.h
# End Source File
# Begin Source File

SOURCE=.\Defines.h
# End Source File
# Begin Source File

SOURCE=.\DevSock.h
# End Source File
# Begin Source File

SOURCE=.\DevThread.h
# End Source File
# Begin Source File

SOURCE=.\Endian.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TypeDefs.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\Auxiliary.rc2
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
