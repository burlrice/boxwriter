// CmdPkt.h: interface for the CCmdPkt class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CMDPKT_H__8975435D_D16C_4F37_BE52_186BAC3FFD95__INCLUDED_)
#define AFX_CMDPKT_H__8975435D_D16C_4F37_BE52_186BAC3FFD95__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "TypeDefs.h"

class CCmdPkt
{
public:
	CTime TimeStamp;
	HANDLE hEvent;
	_SOCKET_PACKET Pkt;
	CCmdPkt ();
	~CCmdPkt();
};
#endif // !defined(AFX_CMDPKT_H__8975435D_D16C_4F37_BE52_186BAC3FFD95__INCLUDED_)
