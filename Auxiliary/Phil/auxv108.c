/*******************************************************************************
        AUX.c
        Aux input/output device designed to sent data in and of 4 serial ports
        and have 8 outputs, 8 inputs, 2 relay outputs, and a PS2 port.
		
		REV:
			1.06
					// InitializeSerialPorts Changed to set all ports and !Skip port 1;
			1.07	// Enabled Front panel LED's for connection status.
			1.08  // Move setup to port B
*******************************************************************************/

char gIpAddress[24];

#define _DEBUG 1

#define VER_MAJOR 1
#define VER_MINOR 0
#define VER_BUILD 8

#define MY_IP_ADDRESS gIpAddress
#define MY_NETMASK    "255.255.255.0"
#define MY_GATEWAY    "192.168.0.103"

#define MAX_CONNECTIONS		0x10		// CGH


#define PORT 1200

#define MSSG_TMOUT        	64UL

#define port_Conf_size       32
#define ip_offset_size       24
#define port1_offset         64 + port_Conf_size
#define port2_offset        port1_offset + port_Conf_size
#define port3_offset        port2_offset + port_Conf_size
#define port4_offset        port3_offset + port_Conf_size
#define ip_address_offset   port4_offset + port_Conf_size
#define subnet_offset       ip_address_offset + port_Conf_size

#define AINBUFSIZE          127
#define AOUTBUFSIZE         127
#define BINBUFSIZE          127
#define BOUTBUFSIZE         127
#define CINBUFSIZE          127
#define COUTBUFSIZE         127
#define DINBUFSIZE          127
#define DOUTBUFSIZE         127

#define SOCKET_BUFFER_SIZE  255

#define SERIAL_BUFFER_SIZE   64

#define WRITE_PORT1         10 //0x09
#define WRITE_PORT2         11 //0x0A
#define WRITE_PORT3         12 //0x32
#define WRITE_PORT4         13 //0x05

#define SET_STROBE_1A       14 //0x35
#define SET_STROBE_1B       16 //0x06
#define SET_STROBE_1C       18 //0x09

#define DATA_FROM_PORT1     20 //0x0b
#define DATA_FROM_PORT2     21 //0x0c
#define DATA_FROM_PORT3     22 //0x0d
#define DATA_FROM_PORT4     23 //0x0e

#define DATA_FROM_PS2       25

#define SETUP_PORT1         30 //0x1B
#define SETUP_PORT2         31 //0x1C
#define SETUP_PORT3         32 //0x1D
#define SETUP_PORT4         33 //0x1E

#define SET_STROBE_2A       34 //0x35
#define SET_STROBE_2B       36 //0x06
#define SET_STROBE_2C       38 //0x09

#define SET_OUTPUT_CONF     50 //0x0a
#define SET_OUTPUT_1        51 //0x0a


#define SET_INPUT_CONF      60 //0x0a
#define READ_INPUT_1        61 //0x0a

#define SET_IP_ADDRESS      70 

#define Read_SETUP_PORT1   240 //0xF1
#define Read_SETUP_PORT2   241 //0xF2
#define Read_SETUP_PORT3   242 //0xF3
#define Read_SETUP_PORT4   243 //0xF4

#define RESET_MICRO        254
#define COMMAND_ACK    255 //0xFF

#define	S_OK						0x00	// OK
#define	E_LOAD_FAILED			0x01	// Load Failed
#define	E_PARAM_INVALID		0x02	// Invalid Paramater
#define	E_PORT_CLOSED			0x03	// Serial Port Closed
#define	E_WRITE_FALIED			0x04	// Updating of NVOL RAM failed

#define micro_c   27000

#memmap xmem
#use "dcrtcp.lib"
#use "tcp.lib"

int writeIDBlock();
int calcCRC(int initCRC, void *data, int blocksize);

char Port1_Act, Port2_Act, Port3_Act, Port4_Act;
unsigned char StrobeFlashMask;

typedef struct {
    unsigned long  Cmd;                  // NetworkCmd value
    unsigned long  Batch;                // this stays the same for all packets that are part of one command. also used for command response.
    unsigned long  Len;                  // length of Data
}_SOCKET_PACKET_HEADER;

typedef struct {
    _SOCKET_PACKET_HEADER Header;
    unsigned char Data[SOCKET_BUFFER_SIZE];
    char Padding[1];                     // Byte aligment fixup
}_SOCKET_PACKET;

//tcp_Socket echosock;
tcp_Socket Sockets[MAX_CONNECTIONS];
tcp_Socket *ConnectedSockets[MAX_CONNECTIONS];
tcp_Socket *WaitingSockets[MAX_CONNECTIONS];

int Strobe1_RED_Active[MAX_CONNECTIONS];
int Strobe2_RED_Active[MAX_CONNECTIONS];

/********************************************************************/
void DbgPrint ( char *pData )
{
	if ( _DEBUG )
	{
		serCwrite( pData, strlen(pData)); 
		while ( serCwrUsed() );
	}
}

int writeIDBlock()
{
	static unsigned long	physaddr;
	static int	addr, blockSize;
	static char	xpc;
	static int	err;


	blockSize = sizeof(SysIDBlock);
	physaddr = 0x00080000ul - blockSize;

	addr = 0xE000 + (int)(physaddr & 0x0FFFul);
	xpc  = (int)(((physaddr - (unsigned long)addr) & 0xFF000ul) >> 12ul);

	_overwrite_block_flag = 1;
	err = WriteFlash(physaddr, &SysIDBlock, blockSize);
	_overwrite_block_flag = 0;

	return(err);
}

/********************************************************************/

int setTimeStamp()
{
	static struct tm	t;

	tm_rd(&t);
	SysIDBlock.timestamp[0] = (t.tm_year + 1900) / 100;
	SysIDBlock.timestamp[1] = t.tm_year % 100;
	SysIDBlock.timestamp[2] = t.tm_mon;
	SysIDBlock.timestamp[3] = t.tm_mday;
	SysIDBlock.timestamp[4] = t.tm_hour;
	SysIDBlock.timestamp[5] = t.tm_min;
	SysIDBlock.timestamp[6] = t.tm_sec;
}

/********************************************************************/

#define CRC_POLY		0x1021		// 16-bit CRC polynomial
											//		(recommended by CCITT X25 standard)

/*
 *	CRC calculation functions
 *		- can be called from C (calcCRC) or assembly (_calcCRC)
 *		- reference:  "A painless guide to CRC error detection algorithms",
 *							8/19/1993, Ross N. Williams
 */

#asm
//	int calcCRC(int initCRC, void *data, int blocksize);
calcCRC::
	ld		hl, 2
	add	hl, sp
	ld		hl, (hl)
	ex		de, hl				; initial CRC value

	ld		hl, 6					; the "blocksize" variable
	add	hl, sp
	ld		hl, (hl)
	ld		b, h
	ld		c, l					; get data size off stack

	ld		hl, 4					; the "data" variable
	add	hl, sp
	ld		hl, (hl)				; hl now contains pointer to data

;; assembly entry point
_calcCRC::
;		de contains initial CRC value
;		hl contains pointer to data
;		bc contains number of bytes
dataloop:
	push	hl
	ld		h, (hl)				; get next byte into hl (and shift it left 8 bits)
	ld		l, 0x00

	push	bc						; save byte counter
	call	_byteCRC				; call CRC function from BIOS
	pop	bc						; restore byte counter

	pop	hl
	inc	hl
	dec	bc

	xor	a
	cp		b
	jr		nz, dataloop
	cp		c
	jr		nz, dataloop

	ex		de, hl				; CRC returned in hl
	ret
#endasm

//////////////////////////////////////////////////////////////////////

#asm
;;
;;	Calculate the CRC value for a single byte.  Can be called multiple
;;	times (without resetting the CRC value passed to it) to do a CRC
;;	check on a larger block of data.
;;
;;		data byte passed in h (l should be 0x00)
;;		crc value passed, returned in de
;;
_byteCRC::
	push	bc
	push	af

	ld		b, 0x08				; bit counter
crcloop:
	push	bc						; save bit counter
	ld		a, h
	xor	d
	scf
	ccf							; clear carry bit
	rl		de						; roll crc left one bit
	ex		de, hl
	scf
	ccf							; clear carry bit
	rl		de						; roll data left one bit
	ex		de, hl
	bit	7, a					; result from earlier XOR
	jr		z, bit7iszero
bit7isone:
	ld		bc, CRC_POLY		; recommended 16-bit polynomial (X.25)
	ld		a, e
	xor	c
	ld		e, a
	ld		a, d					; XOR crc with polynomial
	xor	b
	ld		d, a
bit7iszero:
	pop	bc						; restore bit counter
	djnz	crcloop

	pop	af
	pop	bc
	ret
#endasm

//***************************************************************************************************************
int GetPacket ( void *pSock, _SOCKET_PACKET *pPkt ) {
    int result;
    int len;
    int PacketSize;
    
    PacketSize = sizeof ( _SOCKET_PACKET );
    result = 0;
    memset (pPkt, 0x00, sizeof ( _SOCKET_PACKET ) );

    len = sock_bytesready(pSock);
    if ( len >= PacketSize )
    {
         if ( sock_read ( pSock, (byte *)pPkt, PacketSize ) == PacketSize )
			{
				pPkt->Header.Cmd		= ntohl ( pPkt->Header.Cmd );			// 1.01 CGH
				pPkt->Header.Batch	= ntohl ( pPkt->Header.Batch );		// 1.01 CGH
				pPkt->Header.Len		= ntohl ( pPkt->Header.Len );			// 1.01 CGH
				result = 1;
			}
    }
    return result;
}

int SendPacket ( void *pSock, unsigned long lCmd, unsigned long lBatch, unsigned long lLen, unsigned char *pData ){
    _SOCKET_PACKET sp;
	 int PacketSize;

	 PacketSize = sizeof ( _SOCKET_PACKET );
    memset ( &sp, 0x00, sizeof ( _SOCKET_PACKET ));
    sp.Header.Cmd		= htonl ( lCmd );			// 1.01 CGH
    sp.Header.Batch	= htonl ( lBatch );		// 1.01 CGH
    sp.Header.Len		= htonl ( lLen );			// 1.01 CGH
    memcpy ( sp.Data, pData, (unsigned int)lLen );		// 1.01 CGH
    return sock_write ( pSock, (byte *)&sp, PacketSize );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// 1.01 CGH
// Added pSock paramater.
// Changed SendPacket using global socket echosock to local socket pSock
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void ProcessPacket ( void *pSock, _SOCKET_PACKET *pPkt )
{
   int port_num;
   unsigned char ser_Pkt[64];
   int idx;

   switch ( (unsigned int) pPkt->Header.Cmd )
   {
        case COMMAND_ACK:
//         SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, pPkt->Header.Len, pPkt->Data );
//         printf ( "Answering Ack Command\r\n");
//        	break;
        case WRITE_PORT1:
           if ( Port1_Act == 1 ) 
				  serAwrite (pPkt->Data, (int)pPkt->Header.Len);
           else
			  {
               sprintf ( ser_Pkt, "%iPort 1 Closed", E_PORT_CLOSED);
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           }
           break;

       case WRITE_PORT2:
           if ( Port2_Act == 1)
				  serBwrite( pPkt->Data, (int)pPkt->Header.Len);
           else 
			  {
               sprintf ( ser_Pkt, "%iPort 2 Closed", E_PORT_CLOSED);
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           }
           break;

       case WRITE_PORT3:
           if ( Port3_Act == 1)
				  serCwrite( pPkt->Data, (int)pPkt->Header.Len );
           else
			  {
               sprintf ( ser_Pkt, "%iPort 3 Closed", E_PORT_CLOSED);
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt ), ser_Pkt );
           }
           break;

       case WRITE_PORT4:
           if ( Port4_Act == 1)
				  serDwrite ( pPkt->Data, (int)pPkt->Header.Len );
           else
			  {
               sprintf ( ser_Pkt, "%iPort 4 Closed", E_PORT_CLOSED);
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           }
           break;

       case SET_STROBE_1A:	// Red
           switch ( pPkt->Data[0] )
			  {
               case '0':
               	 for ( idx = 0; idx < MAX_CONNECTIONS; idx++ )
               	 {
               	 	if ( ConnectedSockets[idx] == pSock )
               	 	{
               	 		Strobe1_RED_Active[idx] = 0;
               	 		break;
               	 	}
               	 }
                   break;
               case '1':
               	 for ( idx = 0; idx < MAX_CONNECTIONS; idx++ )
               	 {
               	 	if ( ConnectedSockets[idx] == pSock )
               	 	{
               	 		Strobe1_RED_Active[idx] = 1;
               	 		break;
               	 	}
               	 }
                   break;
               case '2':
               	 for ( idx = 0; idx < MAX_CONNECTIONS; idx++ )
               	 {
               	 	if ( ConnectedSockets[idx] == pSock )
               	 	{
               	 		Strobe1_RED_Active[idx] = 2;
               	 		break;
               	 	}
               	 }
                   break;
           }
           break;

       case SET_STROBE_1B: // Green
           switch ( pPkt->Data[0] )
			  {
               case '0': // Off
                   WrPortI( PADR, &PADRShadow, ( PADRShadow & 0xFD ));
                   StrobeFlashMask = ( StrobeFlashMask & 0xFD);
                   break;
               case '1':	// On
	                WrPortI( PADR, &PADRShadow, ( PADRShadow | 0x02 ));
		             StrobeFlashMask = (StrobeFlashMask | 0x02 );     
                   break;
               case '2':	// Flash
						 WrPortI( PADR, &PADRShadow, ( PADRShadow | 0x02 ));
						 StrobeFlashMask = (StrobeFlashMask & 0xFD );
                   break;
           }
           break;

       case SET_STROBE_1C:
           switch ( pPkt->Data[0] )
			  {
               case '0':
                   WrPortI( PADR, &PADRShadow, ( PADRShadow & 0xFB ));
                   StrobeFlashMask = ( StrobeFlashMask & 0xFB);
                   break;
               case '1':
                   WrPortI( PADR, &PADRShadow, ( PADRShadow | 0x04 ));
                   StrobeFlashMask = (StrobeFlashMask | 0x04 );     
                   break;   
               case '2':
                   WrPortI( PADR, &PADRShadow, ( PADRShadow | 0x04 ));
                   StrobeFlashMask = (StrobeFlashMask & 0xFB );
                   break;
           }
           break;

       case SET_STROBE_2A:	// Red
           switch ( pPkt->Data[0] ){
               case '0':
               	 for ( idx = 0; idx < MAX_CONNECTIONS; idx++ )
               	 {
               	 	if ( ConnectedSockets[idx] == pSock )
               	 	{
               	 		Strobe2_RED_Active[idx] = 0;
               	 		break;
               	 	}
               	 }
                   break;
               case '1':
               	 for ( idx = 0; idx < MAX_CONNECTIONS; idx++ )
               	 {
               	 	if ( ConnectedSockets[idx] == pSock )
               	 	{
               	 		Strobe2_RED_Active[idx] = 1;
               	 		break;
               	 	}
               	 }
                 break;
               case '2':
               	 for ( idx = 0; idx < MAX_CONNECTIONS; idx++ )
               	 {
               	 	if ( ConnectedSockets[idx] == pSock )
               	 	{
               	 		Strobe2_RED_Active[idx] = 2;
               	 		break;
               	 	}
               	 }
                 break;
           }
           break;

       case SET_STROBE_2B: // Green
           switch ( pPkt->Data[0] ){
               case '0':
                   WrPortI( PADR, &PADRShadow, ( PADRShadow & ~0x10 ));
                   StrobeFlashMask = ( StrobeFlashMask & ~0x10);
                   break;
                   
               case '1':
						 WrPortI( PADR, &PADRShadow, ( PADRShadow | 0x10 ));
						 StrobeFlashMask = (StrobeFlashMask | 0x10 );     
                   break;
               case '2':
	                WrPortI( PADR, &PADRShadow, ( PADRShadow | 0x10 ));
		             StrobeFlashMask = (StrobeFlashMask & ~0x10 );
                   break;
           }
           break;

       case SET_STROBE_2C:
           switch ( pPkt->Data[0] ){
               case '0':
                   WrPortI( PADR, &PADRShadow, ( PADRShadow & 0xDF ));
                   StrobeFlashMask = ( StrobeFlashMask & 0xDF);
                   break;

               case '1':
                   WrPortI( PADR, &PADRShadow, ( PADRShadow | 0x20 ));
                   StrobeFlashMask = (StrobeFlashMask | 0x20 );     
                   break;
                   
               case '2':
                   WrPortI( PADR, &PADRShadow, ( PADRShadow | 0x20 ));
                   StrobeFlashMask = (StrobeFlashMask & 0xDF );
                   break;
           }
           break;

       case SETUP_PORT1:
           port_num=1;
           if ( ParsePortParams( pPkt->Data, port_num ) != 1 )
			  {
               sprintf ( ser_Pkt, "%iSerial Port %i Config Error", E_PARAM_INVALID, port_num);
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           }
           else
			  {
               strncpy( ser_Pkt, pPkt->Data, port_Conf_size );
               if (( writeUserBlock( port1_offset, ser_Pkt, port_Conf_size )) != 0){
                   sprintf ( ser_Pkt, "%iSerial Port %i Config not Saved", E_WRITE_FALIED, port_num);
                   SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
               }
           }
           break;

       case SETUP_PORT2:
           port_num=2;
           if ( ParsePortParams( pPkt->Data, port_num ) !=1 )
			  {
               sprintf ( ser_Pkt, "%iSerial Port %i Config Error", E_PARAM_INVALID, port_num);
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           }
           else
			  {
               strncpy( ser_Pkt, pPkt->Data, port_Conf_size );
               if (( writeUserBlock( port2_offset, ser_Pkt, port_Conf_size )) != 0){
                   sprintf ( ser_Pkt, "%iSerial Port %i Config not Saved", E_WRITE_FALIED, port_num);
                   SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
               }
           }
           break;

       case SETUP_PORT3:
           port_num=3;
           if ( ParsePortParams( pPkt->Data, port_num ) != 1 )
			  {
               sprintf ( ser_Pkt, "%iSerial Port %i Config Error", E_PARAM_INVALID, port_num);
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           }
           else
			  {
               strncpy( ser_Pkt, pPkt->Data, port_Conf_size );
               if (( writeUserBlock( port3_offset, ser_Pkt, port_Conf_size )) != 0){
                   sprintf ( ser_Pkt, "%iSerial Port %i Config not Saved", E_WRITE_FALIED, port_num);
                   SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
               }
           }
           break;

       case SETUP_PORT4: //30
           port_num=4;
           if ( ParsePortParams( pPkt->Data, port_num ) != 1 )
			  {
               sprintf ( ser_Pkt, "%iSerial Port %i Config Error", E_PARAM_INVALID, port_num);
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           }
           else
			  {
               strncpy( ser_Pkt, pPkt->Data, port_Conf_size );
               if (( writeUserBlock( port4_offset, ser_Pkt, port_Conf_size )) != 0){
                   sprintf ( ser_Pkt, "%iSerial Port %i Config not Saved", E_WRITE_FALIED, port_num);
                   SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
               }
           }
           break;

        case  SET_IP_ADDRESS:
           memset( ser_Pkt, 0x00, 64);
           memcpy( ser_Pkt, pPkt->Data, (int)pPkt->Header.Len);

           if ( inet_addr(ser_Pkt) == 0)
			  {                // Ck for valid IP address
               sprintf ( ser_Pkt, "%d%s", E_LOAD_FAILED, "IP Address not Changed");
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           }
           else
			  {
				  if (( writeUserBlock( ip_address_offset, ser_Pkt, ip_offset_size )) != 0)
				  {
						sprintf ( ser_Pkt, "%d%s", E_LOAD_FAILED, "IP Address not Changed");
						SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
				  }
				  else
				  {
						printf("New IP Address %s", ser_Pkt);
						sprintf ( ser_Pkt, "%d%s", S_OK, "IP Address Changed");
						SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
						sock_close(pSock);
						forceSoftReset();    
				  }
			  }
           break;
            
       case  Read_SETUP_PORT1:  // 241
           port_num=1;
           if (( readUserBlock( ser_Pkt, port1_offset, port_Conf_size)) == 0) {
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           }
           else {
               sprintf ( ser_Pkt, "%iSerial Port %i Config load Error", E_LOAD_FAILED, port_num);
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           }
           break;

       case  Read_SETUP_PORT2:  // 242
           port_num=2;
           if (( readUserBlock( ser_Pkt, port2_offset, port_Conf_size)) == 0)
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           else {
               sprintf ( ser_Pkt, "%iSerial Port %i Config load Error", E_LOAD_FAILED, port_num);
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           }
           break;
         
       case  Read_SETUP_PORT3:  // 243
           port_num=3;
           if (( readUserBlock( ser_Pkt, port3_offset, port_Conf_size)) == 0) 
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           else {
               sprintf ( ser_Pkt, "%iSerial Port %i Config load Error", E_LOAD_FAILED, port_num);
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           }
           break;

       case  Read_SETUP_PORT4:  // 244
           port_num=4;
           if (( readUserBlock( ser_Pkt, port4_offset, port_Conf_size )) == 0)
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           else {
               sprintf ( ser_Pkt, "%iSerial Port %i Config load Error", E_LOAD_FAILED, port_num);
               SendPacket ( pSock, COMMAND_ACK, pPkt->Header.Batch, strlen(ser_Pkt), ser_Pkt );
           }
           break;

       case  SET_OUTPUT_1:
           WrPortE( micro_c, NULL, pPkt->Data[0]);
           break;
       
       case  RESET_MICRO:    // 254
           BitWrPortI(PBDR, &PADRShadow, 0, 7);          // Turns micro-c off
           BitWrPortI(PBDR, &PADRShadow, 1, 7);          // Turns micro-c on
           break;
   }
}

///////////////////////////////////////////////////////////////////////////////////
// Function:	ParsePortParams
//
// Paramaters	unsigned char *
//
// Return:
//    1 OK
//    0 Invalid String Format
//		-1 Invalid Baud Rate Data
//		-2 Invalid Parity	Bit Data
//		-3 Invalid Data Bit Data
//		-4 Invalid Stop Bit Data
//    -5 Invalid State(active=1, inactive=0)
//
///////////////////////////////////////////////////////////////////////////////////

int ParsePortParams ( unsigned char *pParams, int port )
{
	long lBaudRate;
	char State;
	int  nParity;
	int  nDatabits;
	int  Data_bit;
	int  nStopbits;
	char *pszBaud;
	char *pszDatabits;
	char *pszStopbits;
	char *pszParity;
	char *pszState;
	char Str[255];

// Given the data is "Y,115200,N,8,1"

   strcpy ( Str, pParams );
   printf ("Port Params %s\n", Str );

   pszState = strtok ( Str, "," );
   *pszState=toupper ( *pszState );

	switch ( *pszState )
	{
	  case 'Y':
			State= 1;
			break;
	  case 'N':
			State= 0;
			break;
	  default:
			return -5;
	}

	pszBaud = strtok ( NULL, "," );
	if ( pszBaud ) {
	  lBaudRate = atol ( pszBaud );
	  if (( lBaudRate > 115200 ) || ( lBaudRate < 300 ))
			return -1;
	}
	else
		return 0;

	pszParity = strtok ( NULL, "," );
	if ( *pszParity ) 
	{
		*pszParity=toupper ( *pszParity );
		strcpy ( Str, pszParity);
		switch ( *pszParity )
		{
			case 'N':                       // No Parity Bit
				nParity = 0;
				break;
			case 'O':                       // Odd Parity Bit
				nParity = 1;
				break;
			case 'E':                       // Even Parity Bit
				nParity = 2;
				break;
			default:
				return -2;
				break;
		}
	}
	else
	  return 0;

	pszDatabits = strtok ( NULL, "," );
	if ( pszDatabits ) 
	{
		nDatabits = atoi ( pszDatabits );
		switch ( nDatabits ) 
		{
			case 8:
				Data_bit=0;
				break;
			case 7:
				Data_bit=1;
				break;
			default:
				return -3;
		}
	}
	else
	  return 0;

	pszStopbits = strtok ( NULL, "," );
	if ( pszStopbits ) 
	{
	  nStopbits = atoi ( pszStopbits );
		switch ( nStopbits ) 
		{
			case 0:
			case 1:
				break;
			default:
				return -4;
		}
	}
	else
	  return 0;

	switch ( port )
	{
		case 1:
			Port1_Act = State;
			if ( State == 0 )
			{
//				while ( serAwrUsed() );	// Flush Port
				serAclose();
			}
			else {
				 serAopen( lBaudRate );
				 serAdatabits( Data_bit );
				 serAparity( nParity );
				 serAgetc();
			}
			break;
		case 2:
			Port2_Act = State;
			if ( State == 0 ) 
			{
//				while ( serBwrUsed() );	// Flush Port
				serBclose();
			}
			else {
				 serBopen( lBaudRate );
				 serBdatabits( Data_bit );
				 serBparity( nParity );
				 serBgetc();
			}
			break;
		case 3:
			Port3_Act = State;
			if ( State == 0 ) 
			{
//				while ( serCwrUsed() );	// Flush Port
				serCclose();
			}
			else {
				 serCopen( lBaudRate );
				 serCdatabits( Data_bit );
				 serCparity( nParity );
				 serCgetc();
			}
			break;

		case 4:
			Port4_Act = State;
			if (State == 0) 
			{
//				while ( serDwrUsed() );	// Flush Port
				serDclose();
			}
			else {
				 serDopen( lBaudRate );
				 serDdatabits( Data_bit );
				 serDparity( nParity );
				 serDgetc();
			}
			break;
	}
	return 1;
}

void SendToAll ( unsigned long lCmd, unsigned long lBatch, unsigned long lLen, unsigned char *pData )
{
	int index;
	for ( index = 0; index < MAX_CONNECTIONS; index++ )
		if ( ConnectedSockets[index] != NULL )
			SendPacket ( ConnectedSockets[index], lCmd, lBatch, lLen, pData);
}

void MonitorSerialPorts ( void )
{
	int n;
	unsigned char ser_Pkt[SERIAL_BUFFER_SIZE];

	n=0;
	
	if ( Port1_Act )
	{
		memset ( ser_Pkt, 0x00, sizeof ( ser_Pkt ) );
		if ((n = serAread ( ser_Pkt, SERIAL_BUFFER_SIZE, MSSG_TMOUT )) > 0)
		{
			SendToAll ( DATA_FROM_PORT1 , 1, n, ser_Pkt );
//			printf ( "SerialA: %s\n", ser_Pkt); 
		}
	}


	if ( Port2_Act )
	{
		memset ( ser_Pkt, 0x00, sizeof ( ser_Pkt ) );
		if (( n = serBread( ser_Pkt, SERIAL_BUFFER_SIZE, MSSG_TMOUT )) > 0)
		{
			SendToAll ( DATA_FROM_PORT2 , 1, n, ser_Pkt );
//			printf ( "SerialB: %s\n", ser_Pkt); 
		}
	}

	if ( Port3_Act )
	{
		memset ( ser_Pkt, 0x00, sizeof ( ser_Pkt ) );
		if (( n = serCread ( ser_Pkt, SERIAL_BUFFER_SIZE, MSSG_TMOUT)) > 0)
		{
			SendToAll ( DATA_FROM_PORT3 , 1, n, ser_Pkt );
//			printf ( "SerialC: %s\n", ser_Pkt); 
		}
	}
 
	if ( Port4_Act )
	{
		memset ( ser_Pkt, 0x00, sizeof ( ser_Pkt ) );
		if (( n = serDread ( ser_Pkt, SERIAL_BUFFER_SIZE, MSSG_TMOUT )) > 0)
		{
			SendToAll ( DATA_FROM_PORT4 , 1, n, ser_Pkt );
//			printf ( "SerialD: %s\n", ser_Pkt);
		}
	}
}

void MonitorPS2Port ( void )
{
	static int j;
	unsigned char Input_ps2[64];
	unsigned char ser_Pkt[64];
	
	j = 0;

	if (BitRdPortI (PBDR, 0) ) // Read PS/2 port
	{                   
		if (0x0D == (Input_ps2[j] = RdPortE(micro_c)))
		{
			Input_ps2[j+1]= 0x00;
			SendToAll ( DATA_FROM_PS2, 1, strlen(Input_ps2), Input_ps2 );
			j= 0;
		}
		else
		{
			if (j==0 && (0xF0 == Input_ps2[0] ))
			{
				sprintf ( ser_Pkt, "%d%s", 0, "Micro Controller PS/2 Reset");
				SendToAll ( COMMAND_ACK, 1, strlen(ser_Pkt), ser_Pkt );
				j = 0;
			}
			else
			{
				if (j < 61)
					j++;
			}
		}
	}
}

void MonitorDigitalIO ( void )
{
	unsigned char ser_Pkt[64];
	
	if (BitRdPortI (PBDR, 1) ) // Read digital input from micro
	{                   
		ser_Pkt[0] = RdPortE(micro_c);
		SendToAll ( READ_INPUT_1, 1, 1, ser_Pkt);
	}
}

void HandleDuplicateConnections ( void *pSock )
{
	int result;
	int i;
	sockaddr s1;
	sockaddr s2;

	result = 0;
	
	getpeername ( (sock_type *)pSock, &s2, NULL );

	for ( i = 0; i < MAX_CONNECTIONS; i++ )
	{
		if ( ConnectedSockets[i] != NULL )
		{
			getpeername ( (sock_type *)ConnectedSockets[i], &s1, NULL );
			if ( s2.s_ip == s1.s_ip )
			{
				tcp_close ( ConnectedSockets[i] );
				WaitingSockets[i] = ConnectedSockets[i];
				ConnectedSockets[i] = NULL;
				tcp_listen ( WaitingSockets[i], PORT, 0, 0, NULL, 0 );
			}
		}
	}
}

void CheckForNewConnections ( void )
{
	int index;
	for ( index = 0; index < MAX_CONNECTIONS; index++ )
	{
		if ( WaitingSockets[index] != NULL )
		{
			tcp_tick ( NULL );
			if ( sock_established ( WaitingSockets[index] ) )
			{
				HandleDuplicateConnections ( &WaitingSockets[index] );
				ConnectedSockets[index] = WaitingSockets[index];
				Strobe1_RED_Active[index] = 0;
				Strobe2_RED_Active[index] = 0;
				WaitingSockets[index] = NULL;
			}
		}
	}
}

int ProcessConnectedSockets ( void )
{
	int index;
	int Connections;
	_SOCKET_PACKET myPkt;

	Connections = 0;
	
	for ( index = 0; index < MAX_CONNECTIONS; index++ )
	{
		if ( ConnectedSockets[index] != NULL )
		{
			if ( tcp_tick ( ConnectedSockets[index] ) )
			{
				Connections++;
				if ( GetPacket( ConnectedSockets[index], &myPkt ) )
					ProcessPacket( ConnectedSockets[index], &myPkt);
			}
			else
			{
				WaitingSockets[index] = ConnectedSockets[index];
				ConnectedSockets[index] = NULL;					// Remove it from the connected list
				Strobe1_RED_Active[index] = 0;
				Strobe2_RED_Active[index] = 0;
				tcp_listen ( WaitingSockets[index], PORT, 0, 0, NULL, 0 );		// Set it up to listen again.
			}
		}
	}
	return Connections;
}

void PromptForIpAddress ( void )
{
	int state;
	struct tm Start;
	struct tm Current;
	int bTimeout;
	int done;
	char IpAddr[255];
	char sMsg[255];
	int index;
	int c;
	unsigned long lStartTime;
	unsigned long lCurrentTime;
	unsigned long lElapsedTime;
	
	
	tm_rd ( &Start );	
	lStartTime = mktime ( &Start );
	
	state = 0;
	bTimeout = 0;
	done = false;

	sprintf ( sMsg, "\r\nMarksman Hub.\r\nVersion %d.%d%d\r\nPlease Type SET with in 10 seconds if you wish to set IP Address\r\n", VER_MAJOR, VER_MINOR, VER_BUILD);
	serBwrite ( sMsg, strlen (sMsg) );
	printf ( sMsg );
		
	while ( (!bTimeout) && ( state != 3) ) 
	{
		if ( (c = serBgetc()) != -1 )
		{
			switch ( c )
			{
				case 'S':
				case 's':
					state = 1;
					tm_rd ( &Start );	
					break;
				case 'E':
				case 'e':
					if ( state == 1 )
						state = 2;
					tm_rd ( &Start );	
					break;
				case 'T':
				case 't':
					if ( state == 2 )
						state = 3;
					tm_rd ( &Start );	
					break;
			}
		}
		tm_rd ( &Current );
		lCurrentTime = mktime ( &Current );
		lElapsedTime = lCurrentTime - lStartTime;
		if ( lElapsedTime > 10 )
			bTimeout = true;
	}

	if ( state == 3 )
	{
		while ( done != true )
		{
			index = 0;
			memset ( IpAddr, 0x00, sizeof ( IpAddr ) );
			strcpy ( sMsg, "\rPlease Enter New IP Address ");
			serBwrite ( sMsg, strlen (sMsg) );
			while ( 1 ) 
			{
				if ( (c = serBgetc()) != -1 )
				{
					if ( c == 8 )	//BS
					{
						if ( index > 0 )
						{
							index--;
							serBputc ( c );
						}
					}
					else 
					{
						if ( index < 15 )
						{
							if ( ( c > 47 && c < 58 ) || (c == 46))
							{
								IpAddr[index++] = c;
								serBputc ( c );
								if ( index == 15 )
									break;
							}
							else
							{
								if ( c == '\r' )
								{
									IpAddr[index++] = 0x00;
									break;
								}
							}
						}
					}
				}
			}
			if ( inet_addr(IpAddr) != 0)
			{
      		sprintf( gIpAddress, IpAddr);
       		writeUserBlock( ip_address_offset, gIpAddress, 1+strlen(gIpAddress) ); 
				done = true;
   		}
			else
			{
				strcpy ( sMsg, "\r\nInvalid Ip Address.\r\n");
				serBwrite ( sMsg, strlen ( sMsg ) );
			}
		}
	}
	
	sprintf ( sMsg, "\r\nUsing IP Address %s\r\n", gIpAddress );
	printf ( sMsg );
	serBwrite ( sMsg, strlen ( sMsg ) );
	while ( serBwrUsed() );
}

void InitializeSerialPorts ( void )
{
	int i;
	unsigned char Settings[64];

//   for ( i = 1; i < 4; i++ ) // 1.06 CGH
   for ( i = 0; i < 4; i++ ) // 1.06 CGH
   {
		if (( readUserBlock( Settings, port1_offset + (i * port_Conf_size), port_Conf_size)) == 0)
      {
			if ( ParsePortParams( Settings, i+1 ) != 1 )
      	{
           	printf ( "Invalid Serial Port Configuration. Defaulting Port Configuration for Port %i\n", i );	
         	strcpy ( Settings, "Y,9600,N,8,1" );
            ParsePortParams ( Settings, i+1 );
            if (( writeUserBlock( port1_offset + (i * port_Conf_size), Settings, port_Conf_size )) != 0)
            	printf ( "Error %i -> Serial Port %i Config not Saved", E_WRITE_FALIED, i);
      	}
      }
      else
      {
			strcpy ( Settings, "Y,9600,N,8,1" );
			ParsePortParams ( Settings, i+1 );
			if (( writeUserBlock( port1_offset + (i * port_Conf_size), Settings, port_Conf_size )) != 0)
				printf ("Error %i -> Serial Port %i Config not Saved", E_WRITE_FALIED, i);
		}
   }   
	PromptForIpAddress();
}

void CheckUserBlockSetup ( void )
{
	static int	i, crc;
	int	saveMB1;
	char	*div19200ptr;

	saveMB1 = MB1CRShadow;
	WrPortI(MB1CR, &MB1CRShadow, FLASH_WSTATES | 0x00);
	_InitFlashDriver(0x01 | 0x02);

	memset(&SysIDBlock, 0x00, sizeof(SysIDBlock));
	i = _readIDBlock(0x01 | 0x02);

	if (i != 0) 
	{
//		if (HASUSERBLOCK) {
			SysIDBlock.userBlockSize = 0x4000 - (int)SysIDBlock.idBlockSize;
			SysIDBlock.userBlockLoc = 0x4000 - (int)SysIDBlock.idBlockSize;
//		} else {
//			SysIDBlock.userBlockSize = 0x0000;
//			SysIDBlock.userBlockLoc = 0x0000;
//		}

		//////////////////////////////////////////////////
		// recalculate the ID block's CRC sum

		SysIDBlock.idBlockCRC = 0x0000;

		i = (int)(&SysIDBlock.reserved) - (int)(&SysIDBlock.tableVersion);
		crc = calcCRC(0x0000, &SysIDBlock, i);
		crc = calcCRC(crc, &SysIDBlock.idBlockSize, 16);

		SysIDBlock.idBlockCRC = crc;

		i = writeIDBlock();
		if (i != 0)	{
			printf("\nError writing ID block (%d): \n", i);
			if (i == -1)	printf("  attempt to write to non-flash area\n");
			else if (i == -2)	printf("  source not located in root\n");
			else if (i == -3)	printf("  timeout during flash write\n");
			else printf("  unknown error type\n");
			exit(1);

		} else
			printf("\nID block successfully written.\n\n");

		//////////////////////////////////////////////////
		// now try and read the new ID block back...

		memset(&SysIDBlock, 0x00, sizeof(SysIDBlock));
		i = _readIDBlock(0x01 | 0x02);

		if (i != 0) {
			printf("Error reading ID block (%d)\n", i);
			exit(1);

		} else {
			printf("ID block read successfully:\n");
			printf("  Version       = %d\n", SysIDBlock.tableVersion);
			printf("  ID block size = %d bytes\n", SysIDBlock.idBlockSize);
			
			printf("\n");			
			printf("  User block offset = %04x\n", SysIDBlock.userBlockLoc);
			printf("  User block size   = %04x\n", SysIDBlock.userBlockSize);
		}
	} 
	else 
	{
		printf("ID block read successfully:\n");
		printf("  Version       = %d\n", SysIDBlock.tableVersion);
		printf("  ID block size = %d bytes\n", SysIDBlock.idBlockSize);
		
		printf("\n");			
		printf("  User block offset = %04x\n", SysIDBlock.userBlockLoc);
		printf("  User block size   = %04x\n", SysIDBlock.userBlockSize);
	}
	//////////////////////////////////////////////////
	// restore memory quadrant 1

	WrPortI(MB1CR, &MB1CRShadow, saveMB1);
}

//**********************  Main Routine  ********************************
//
//**********************************************************************

main() 
{
	int microPresent;
	int status;
	int i;
	int idx;
	int strobeON[2];
	int strobeFLASH[2];
	long  BaudB;
	int index;
	int PortI_Temp;
	unsigned long n;
	unsigned char startup_error;
	unsigned char ser_Pkt[64];
	unsigned char IP_Address_Var[24];

   startup_error = 0;

//////////////////////////////////////////////////////////////////////////////
// All Strobe are active HIGH   
//   Strobe_1_LED1 = 0x01;
//   Strobe_1_LED2 = 0x02;
//   Strobe_1_LED3 = 0x04;

//   Strobe_2_LED1 = 0x08;
//   Strobe_2_LED2 = 0x10;
//   Strobe_2_LED3 = 0x20;

// All Front Panel LED are active LOW
//   Front_LED1	= 0x40;
//   Front_LED2  = 0x80; 
//////////////////////////////////////////////////////////////////////////////

//	CheckUserBlockSetup();

// ************************  Enable Micro Controller	**************************
   WrPortI( SPCR, NULL, 0x84);                   // Set Port A to Output

   WrPortI(IB3CR,  NULL,  0x88);                             //Setup pin E2 for !CS for micro-c
   WrPortI(PEDDR, &PEDDRShadow, (PEDDRShadow | 0x08 ));      //set port E bit 3 as output
   WrPortI(PEFR,  &PEFRShadow, (PEFRShadow | 0x08 ));
   WrPortI(GCSR,  &GCSRShadow, (GCSRShadow | 0x08 ));
   WrPortI(GOCR,  &GOCRShadow, (GOCRShadow & 0x3F ));

   BitWrPortI(PBDR, &PBDRShadow, 1, 7);          // Turns micro-c on
// ************************  Micro Controller Enabled	*************************

   PADRShadow = 0x00;
   sprintf ( gIpAddress, "10.1.2.50");

   

// ************* Read Flash and Setup IP Address  *******************************

   readUserBlock( IP_Address_Var, ip_address_offset, ip_offset_size);
   printf("IP adddress= %s\n", IP_Address_Var);

   if ( inet_addr(IP_Address_Var) == 0)
	{
       sprintf( IP_Address_Var, gIpAddress);
       writeUserBlock( ip_address_offset, IP_Address_Var, 1+strlen(IP_Address_Var) ); 
   }

	strcpy( gIpAddress, IP_Address_Var);

	serBopen( 9600 );
	serBwrite( gIpAddress, strlen(gIpAddress)); 
	while ( serBwrUsed() );
   
//************************* End IP Address Setup ***********************************************

	InitializeSerialPorts();

	serBwrite( gIpAddress, strlen(gIpAddress)); 
	while ( serBwrUsed() );
//************************** SETUP I/O PORTS 		
	

	StrobeFlashMask = 0xC0;
  	WrPortI( PADR, &PADRShadow, StrobeFlashMask );

// Check to see if Micro-Controller is actually present
	if ( RdPortE ( micro_c ) == 0xFF )
		microPresent = 0;
	else
		microPresent = 1;

   sock_init();	// Initialize the socket subsystem.

	// Assign Sockets to the waiting list and set them to listen;
	for ( index = 0; index < MAX_CONNECTIONS; index++ )
	{
		ConnectedSockets[index] = NULL;
		Strobe1_RED_Active[index] = 0;
		Strobe2_RED_Active[index] = 0;
		WaitingSockets[index] = &Sockets[index];
		tcp_listen ( WaitingSockets[index], PORT, 0, 0, NULL, 0 );
	}

   while(1)
	{
		costate
		{
			while ( 1 )
			{
				CheckForNewConnections ();
				if ( ProcessConnectedSockets() > 0 )	// If there is at least one connected Socket
				{
					// Enabled Connection Front Panel LED // 1.07 CGH
					PortI_Temp = RdPortI (PADR);
					WrPortI( PADR, NULL, ( PortI_Temp | 0x80 ));	// Turn OFF Front Panel LED2
					// End Changes 1.07 CGH
					yield;
					MonitorSerialPorts();
					if ( microPresent )
					{
						MonitorPS2Port();
						MonitorDigitalIO();
					}
				}
				else
				{
					// Enabled Connection Front Panel LED // 1.07 CGH
					PortI_Temp = RdPortI (PADR);
					WrPortI( PADR, NULL, ( PortI_Temp & ~0x80 ));	// Turn ON Front Panel LED2
					// End Changes 1.07 CGH
				}
            yield;
			}
		}	//costate
		
		costate Strobe always_on
		{
			PADRShadow = PADRShadow & ~0x09;
			strobeON[0] = strobeON[1] = 0;
			strobeFLASH[0] = strobeFLASH[1] = 0;
			
			for ( idx = 0; idx < MAX_CONNECTIONS; idx++ )
			{
				if ( Strobe1_RED_Active[idx] != 0 )
				{
					strobeON[0] = 1;
					if ( Strobe1_RED_Active[idx] == 2 )
						strobeFLASH[0] = 1;
				}
				
				if ( Strobe2_RED_Active[idx] != 0 )
				{
					strobeON[1] = 1;
					if ( Strobe2_RED_Active[idx] == 2 )
						strobeFLASH[1] = 1;
				}				
			}

			if ( strobeON[0] )
			{
				PADRShadow = PADRShadow | 0x01;
				if ( strobeFLASH[0] )
					StrobeFlashMask = StrobeFlashMask & ~(0x01);
				else
					StrobeFlashMask = StrobeFlashMask | 0x01;
			}	
			else
				StrobeFlashMask = StrobeFlashMask & ~(0x01);
				
			if ( strobeON[1] )
			{
				PADRShadow = PADRShadow | 0x08;
				if ( strobeFLASH[1] )
					StrobeFlashMask = StrobeFlashMask & ~(0x08);
				else
					StrobeFlashMask = StrobeFlashMask | 0x08;
			}
			else
				StrobeFlashMask = StrobeFlashMask & ~(0x08);
			waitfor( DelayMs (1000) );                    // waits until 1000 ms have passed since
			WrPortI( PADR, NULL, StrobeFlashMask );
			waitfor( DelayMs (1000) );                    // waits until 1000 ms have passed since
			WrPortI( PADR, NULL, PADRShadow );
		} 
   }
}
