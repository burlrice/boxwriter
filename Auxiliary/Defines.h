#if !defined (__DEFINES_H__)
#define __DEFINES_H__

#define	SOCKET_BUFFER_SIZE		0xFF
#define	NUM_STROBE_PORTS			0x02
#define	NUM_SERIAL_PORTS			0x04

#define STROBE_1						0x01
#define STROBE_2						0x02
#define STROBE_3						0x03

//////////////////////////////////////////////////////////////////////////////////////
//										Device Commands													//
//////////////////////////////////////////////////////////////////////////////////////

// PS2 Data Port Recieve only
#define	DATA_FROM_PS2_PORT		0x19

// Serial Ports
// Read
#define	DATA_FROM_PORT1			0x14
#define	DATA_FROM_PORT2			0x15
#define	DATA_FROM_PORT3			0x16
#define	DATA_FROM_PORT4			0x17
//	Write
#define	WRITE_SERIAL_PORT1		0x0A
#define	WRITE_SERIAL_PORT2		0x0B
#define	WRITE_SERIAL_PORT3		0x0C
#define	WRITE_SERIAL_PORT4		0x0D

#define	SETUP_PORT1					0x1E
#define	SETUP_PORT2					0x1F
#define	SETUP_PORT3					0x20
#define	SETUP_PORT4					0x21

// Strobe 1
#define	SET_STROBE_1A				0x0E
#define	SET_STROBE_1B				0x10
#define	SET_STROBE_1C				0x12

// Strobe 2
#define	SET_STROBE_2A				0x22
#define	SET_STROBE_2B				0x24
#define	SET_STROBE_2C				0x26

#define STROBE_OFF             0x30								 //0x00
#define STROBE_ON              0x31								 //0x01
#define STROBE_FLASH           0x32								 //0x02
//#define	STROBE_OFF					0x00
//#define	STROBE_ON					0x01
//#define	STROBE_FLASH				0x02

// Digital I/O
#define SET_OUTPUT_1					0x33
#define READ_INPUT_1					0x3D

// Change Device IP Address - Requires reconnect after COMMAND_ACK
#define	SET_DEVICE_ADDRESS		70

#define	COMMAND_ACK					0xFF

//////////////////////////////////////////////////////////////////////////////////////
//										Window Messages													//
//////////////////////////////////////////////////////////////////////////////////////
#define	WM_AUX_SERIAL_DATA				WM_APP + 401
#define	WM_AUX_PS2_DATA					WM_APP + 402
#define	WM_AUX_CONNECT_STATE_CHANGED	WM_APP + 403
#define	WM_AUX_DIGITAL_INPUT_DATA		WM_APP + 404
#define	WM_AUX_KEEP_ALIVE				WM_APP + 405

//////////////////////////////////////////////////////////////////////////////////////
//										Thread Messages													//
//////////////////////////////////////////////////////////////////////////////////////
#define	TM_AUX_SOCKET_DATA			WM_APP + 510
#define	TM_AUX_SET_WND_HANDLE		WM_APP + 511
#define	TM_AUX_ADDRESS_CHANGE		WM_APP + 512
#define	TM_AUX_CONNECT_COMPLETE		WM_APP + 513

#define MAX_SERIAL_DATA				2048
#endif