#if !defined(AFX_DEVSOCK_H__C764BA74_D6F1_4147_9A19_ED829950BA2B__INCLUDED_)
#define AFX_DEVSOCK_H__C764BA74_D6F1_4147_9A19_ED829950BA2B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DevSock.h : header file
//

#include <afxsock.h>
#include <afxmt.h>
#include "TypeDefs.h"


/////////////////////////////////////////////////////////////////////////////
// CDevSock command target

class CDevSock : public CAsyncSocket
{

public:

// Operations
public:
	CDevSock( DWORD nThreadID );
	virtual ~CDevSock();

// Overrides
public:
	void GetPackets ( CPtrList &List );
	bool TransmittPacket (unsigned long Cmd, long BatchID, const unsigned char* data, unsigned long Len);
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDevSock)
	public:
	virtual void OnReceive(int nErrorCode);
	virtual void OnConnect(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CDevSock)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
	DWORD m_nThreadID;
	void ReportError ( DWORD dwError );
	unsigned char *m_pRxBuffer;
	CPtrList m_RxCue;
	CDiagnosticCriticalSection m_CueSentinel;
	void *holdBuf;
	unsigned long holdBufLen;
	void ExtractPackets (unsigned char *pMsg, unsigned pMsgLength);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEVSOCK_H__C764BA74_D6F1_4147_9A19_ED829950BA2B__INCLUDED_)
