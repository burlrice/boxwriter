//////////////////////////////////////////////////////////////////////
//
//	Endian.cpp:	implementation of the CEndian class.
//
//	Purpose:	To provide a central place for the conversion of 
//				Little-Endian to Big-Endian and back again for 
//				communications to and from the IP Print Head.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <afxsock.h>
#include "Endian.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEndian::CEndian()
{

}

CEndian::~CEndian()
{

}

void CEndian::hton (_SOCKET_PACKET &sm)
{
	sm.Header.BatchID = htonl (sm.Header.BatchID);
	sm.Header.Cmd = htonl (sm.Header.Cmd);
	sm.Header.Length = htonl (sm.Header.Length);
}

void CEndian::hton (_SOCKET_PACKET *sm)
{
	sm->Header.BatchID = htonl (sm->Header.BatchID);
	sm->Header.Cmd = htonl (sm->Header.Cmd);
	sm->Header.Length = htonl (sm->Header.Length);
}

void CEndian::ntoh (_SOCKET_PACKET &sm)
{
	sm.Header.BatchID = ntohl (sm.Header.BatchID);
	sm.Header.Cmd = ntohl (sm.Header.Cmd);
	sm.Header.Length = ntohl (sm.Header.Length);
}

void CEndian::ntoh (_SOCKET_PACKET *sm)
{
	sm->Header.BatchID = ntohl (sm->Header.BatchID);
	sm->Header.Cmd = ntohl (sm->Header.Cmd);
	sm->Header.Length = ntohl (sm->Header.Length);
}
