// DevSock.cpp : implementation file
//

#include "stdafx.h"
#include "DevSock.h"
#include "Defines.h"
#include "TypeDefs.h"
#include "Endian.h"
#include "Debug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define RX_BUFFER_SIZE 32768

/////////////////////////////////////////////////////////////////////////////
// CDevSock

CDevSock::CDevSock( DWORD nThreadID) : m_pRxBuffer(NULL), m_nThreadID (nThreadID)
{
	try {
		m_pRxBuffer = new unsigned char [ RX_BUFFER_SIZE ];
		memset ( m_pRxBuffer, 0x00, RX_BUFFER_SIZE);
	}
	catch ( CMemoryException *e) { e->ReportError(); e->Delete();}
	holdBufLen = 0;
	holdBuf = NULL;
}

CDevSock::~CDevSock()
{
	if ( m_pRxBuffer != NULL )
		delete [] m_pRxBuffer;
	m_pRxBuffer = NULL;
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CDevSock, CAsyncSocket)
	//{{AFX_MSG_MAP(CDevSock)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CDevSock member functions

void CDevSock::OnReceive(int nErrorCode) 
{
	int Result;
	memset (m_pRxBuffer, NULL, RX_BUFFER_SIZE);
	Result = Receive( m_pRxBuffer, RX_BUFFER_SIZE );
	switch (Result) {
	case SOCKET_ERROR:
		ReportError ( GetLastError() );
		break;
	case 0:
//		Debug.Log( "Socket Error: The Socket has been Closed" );
		break;
	default:
		ExtractPackets(m_pRxBuffer, Result);
		if ( !m_RxCue.IsEmpty() )
			while ( !PostThreadMessage ( m_nThreadID, TM_AUX_SOCKET_DATA, 0, 0L) ) Sleep(0);
	}
	CString sMsg;
	sMsg.Format (_T ("Socket Recieve"));
//	TRACEF (sMsg);
	CAsyncSocket::OnReceive(nErrorCode);
}

bool CDevSock::TransmittPacket(unsigned long Cmd, long BatchID, const unsigned char* data, unsigned long Len)
{
	bool result = true;
	const unsigned char *p = data;
	unsigned long lDataLen = Len;
	_SOCKET_PACKET sm;

	do {
		memset (&sm, 0x00, sizeof (_SOCKET_PACKET));
		sm.Header.Cmd = Cmd;
		sm.Header.BatchID = BatchID;
		if (lDataLen > SOCKET_BUFFER_SIZE) {
			memcpy (&sm.Data, p, SOCKET_BUFFER_SIZE);
			sm.Header.Length = SOCKET_BUFFER_SIZE;
			lDataLen -= SOCKET_BUFFER_SIZE;
			p += SOCKET_BUFFER_SIZE;
		}
		else {
			if ( ( lDataLen != 0 ) && ( data != NULL ) ) {
				memcpy (&sm.Data, p, lDataLen);
				sm.Header.Length = lDataLen;
				lDataLen = 0;
			}
			else 
				sm.Header.Length = lDataLen = 0;
		}
		CEndian::hton ( sm );
		if ( Send (&sm, sizeof(_SOCKET_PACKET)) == SOCKET_ERROR) {
			result = false;
			break;
		}
	} while (lDataLen != 0);
	return result;
}

void CDevSock::ExtractPackets(unsigned char * pMsg, unsigned pMsgLength)
{
	LOCK (m_CueSentinel);
	unsigned msgLen = pMsgLength;
	PUCHAR pData, p;
	_SOCKET_PACKET *sm = NULL;
	
	if ( holdBufLen ) {
		PUCHAR pNew = new UCHAR [msgLen + holdBufLen];
		memcpy (pNew, holdBuf, holdBufLen);
		pData = pNew + holdBufLen;
		memcpy (pData, pMsg, pMsgLength);
		msgLen += holdBufLen;
		delete holdBuf;
		holdBuf = NULL;
		holdBufLen = 0;
		pData = pNew;
	}
	else {
		pData = new UCHAR[pMsgLength];
		memcpy (pData, pMsg, pMsgLength);
	}

	p = pData;
	while ( msgLen >= sizeof ( _SOCKET_PACKET ) ) {
		sm = new _SOCKET_PACKET;
		memcpy (sm, p, sizeof (_SOCKET_PACKET));
		CEndian::ntoh ( sm );
			m_RxCue.AddTail (sm);
		p += sizeof (_SOCKET_PACKET);
		msgLen -= sizeof (_SOCKET_PACKET);
	}

	if ( msgLen != 0 ) {
		holdBuf = new PUCHAR [msgLen];
		memcpy (holdBuf, p, msgLen);
		delete pData;
		holdBufLen = msgLen;
	}
	else
		delete pData;
}

void CDevSock::ReportError(DWORD dwError)
{
	CString Entry;
	switch ( dwError) {
	case WSANOTINITIALISED:
		Entry.Format(_T ("Socket Error: Must Call AfxSocketInit before using this API."));
		break;
	case WSAENETDOWN:
		Entry.Format(_T ("Socket Error: Windows Socket implementation detected that the network subsystem failed."));
		break;
	case WSAENOTCONN:
		Entry.Format(_T ("Socket Error: The Socket is not connected."));
		break;
	case WSAEINPROGRESS:
		Entry.Format(_T ("Socket Error: A Blocking Windows Socket operation is in progress."));
		break;
	case WSAESHUTDOWN:
		Entry.Format(_T ("Socket Error: The Socket has been shut down.  A Recieve is not Possiable."));
		break;
	case WSAEMSGSIZE:
		Entry.Format(_T ("Socket Error: The datagram was too large to fit in the specified buffer and was truncated."));
		break;
	case WSAECONNABORTED:
		Entry.Format(_T ("Socket Error: The Virtual Circuit was aborted due to timeout or other failure."));
		break;
	case WSAECONNRESET:
		Entry.Format(_T ("Socket Error: The Virtual Circuit wa reset by the Host side."));
		break;
	default:
		Entry.Format(_T ("Socket Error: UnKnown Error."));
	}
//	Debug.Log(Entry);
}

void CDevSock::GetPackets(CPtrList &List)
{
	LOCK (m_CueSentinel);
	while ( !m_RxCue.IsEmpty() ) 
		List.AddTail( m_RxCue.RemoveHead() );
}

void CDevSock::OnConnect(int nErrorCode) 
{
	CAsyncSocket::OnConnect(nErrorCode);
/*
	if (0 != nErrorCode)
	{
		switch( nErrorCode )
		{
		case WSAEADDRINUSE: 
		  AfxMessageBox("The specified address is in use.\n");
		  break;
		case WSAEADDRNOTAVAIL: 
		  AfxMessageBox("The specified address is not available from the local machine.\n");
		  break;
		case WSAEAFNOSUPPORT: 
		  AfxMessageBox("Addresses in the specified family cannot be used with this socket.\n");
		  break;
		case WSAECONNREFUSED: 
		  AfxMessageBox("The attempt to connect was forcefully rejected.\n");
		  break;
		case WSAEDESTADDRREQ: 
		  AfxMessageBox("A destination address is required.\n");
		  break;
		case WSAEFAULT: 
		  AfxMessageBox("The lpSockAddrLen argument is incorrect.\n");
		  break;
		case WSAEINVAL: 
		  AfxMessageBox("The socket is already bound to an address.\n");
		  break;
		case WSAEISCONN: 
		  AfxMessageBox("The socket is already connected.\n");
		  break;
		case WSAEMFILE: 
		  AfxMessageBox("No more file descriptors are available.\n");
		  break;
		case WSAENETUNREACH: 
		  AfxMessageBox("The network cannot be reached from this host at this time.\n");
		  break;
		case WSAENOBUFS: 
		  AfxMessageBox("No buffer space is available. The socket cannot be connected.\n");
		  break;
		case WSAENOTCONN: 
		  AfxMessageBox("The socket is not connected.\n");
		  break;
		case WSAENOTSOCK: 
		  AfxMessageBox("The descriptor is a file, not a socket.\n");
		  break;
		case WSAETIMEDOUT: 
		  AfxMessageBox("The attempt to connect timed out without establishing a connection. \n");
		  break;
		default:
		  TCHAR szError[256];
		  wsprintf(szError, "OnConnect error: %d", nErrorCode);
		  AfxMessageBox(szError);
		  break;
		}
	}
*/
	while ( !::PostThreadMessage (m_nThreadID, TM_AUX_CONNECT_COMPLETE, nErrorCode, 0L )) Sleep (0);
}
