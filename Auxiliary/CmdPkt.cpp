// CmdPkt.cpp: implementation of the CCmdPkt class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CmdPkt.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCmdPkt::CCmdPkt()
{
	TimeStamp = CTime::GetCurrentTime();
	hEvent = NULL;
	memset ( &Pkt, 0x00, sizeof ( _SOCKET_PACKET ) );
}

CCmdPkt::~CCmdPkt()
{

}
