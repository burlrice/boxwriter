#if !defined (__AUXBOX_API_H__)
	#define __AUXBOX_API_H__
	#ifdef DLLEXPORT
	#define AUXBOX_API __declspec(dllexport)
	#else
	#define AUXBOX_API __declspec(dllimport)
	#endif
#endif