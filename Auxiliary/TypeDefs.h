#if !defined (__TYPEDEFS_H__)
	#define __TYPEDEFS_H__

	#include "Defines.h"	

	typedef struct {
		unsigned long  Cmd;			// NetworkCmd value
		unsigned long  BatchID;    // this stays the same for all packets that are part of one command. also used for command response.
		unsigned long  Length;     // length of Data
	}_SOCKET_PACKET_HEADER;

	typedef struct {
		_SOCKET_PACKET_HEADER Header;
		unsigned char Data[SOCKET_BUFFER_SIZE];
	}_SOCKET_PACKET;

	typedef struct {
		UINT Length;
		unsigned char Data[MAX_SERIAL_DATA];
	}_SERIAL_PACKET;

	enum API_ERRORS 
	{
		E_DEVICENOTOPEN = 0x80000001,
		E_BUFFERTOSMALL
	};

#endif
