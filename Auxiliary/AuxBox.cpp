// AuxBox.cpp: implementation of the CAuxBox class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TypeDefs.h"
#include "CmdPkt.h"
#include "DevThread.h"
#include "AuxBox.h"
#include "Resource.h"
#include "Debug.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//CDebugLog Debug;

CAuxBox::CAuxBox() 
:	m_nLastDigitalInput (0),
	m_pDevThread ( NULL ), m_bOpen(false)
{
	SetPriorityClass ( this, REALTIME_PRIORITY_CLASS);
}

CAuxBox::~CAuxBox()
{
	if (m_pDevThread) 
		m_pDevThread->Shutdown();
}

LONG CAuxBox::Open(LPCTSTR lpszIpAddress, UINT SocketPort, HWND hWnd)
{
	LONG result = 0;
	if ( m_bOpen == false )
	{
		if ( m_pDevThread == NULL ) {
			m_sAddress = lpszIpAddress;
			m_nPort = (UINT) SocketPort;
			m_pDevThread = ( CDevThread* ) AfxBeginThread ( RUNTIME_CLASS (CDevThread) );
			if ( m_pDevThread ) {
				while ( !PostThreadMessage (m_pDevThread->m_nThreadID, TM_AUX_SET_WND_HANDLE, 0, (LPARAM) hWnd)) Sleep(0);
				ULONG lAddress = inet_addr ( w2a (m_sAddress) );
				while ( !PostThreadMessage (m_pDevThread->m_nThreadID, TM_AUX_ADDRESS_CHANGE, m_nPort, lAddress)) Sleep(0);
				m_bOpen = true;
				result = (LONG)m_pDevThread->m_hThread;
			}
		}
	}
//	else
//	{
//		result = E_DEVICENOTOPEN;
//		ThrowError( result, IDS_DEVICE_NOT_OPEN );
//	}
	return result;
}

void CAuxBox::Close()
{
	if ( m_bOpen )
	{
		if (m_pDevThread) {
			HANDLE hThread = m_pDevThread->m_hThread;

			m_pDevThread->Shutdown();
			WaitForSingleObject (hThread, 1000);
			//delete m_pDevThread;
			m_pDevThread = NULL;
		}

		m_bOpen = false;
	}

}

LONG CAuxBox::SetStrobe(short Port, short StrobeID, short State)
{
	SCODE hr = E_INVALIDARG;
	bool error = false;
	unsigned char Data = LOBYTE (State);
	CString sData;
	CString sEvent;
	ULONG Cmd;

	if ( m_bOpen ) {
		switch ( Port ) {
		case 1:
			switch ( StrobeID )
			{
				case 1:
					Cmd = SET_STROBE_1A;
					break;
				case 2:
					Cmd = SET_STROBE_1B;
					break;
				case 3:
					Cmd = SET_STROBE_1C;
					break;
				default:
					error = true;
					ThrowError ( E_INVALIDARG, LoadString (IDS_STROBEOUTOFRANGE));
			}
			break;
		case 2:
			switch ( StrobeID )
			{
				case STROBE_1:
					Cmd = SET_STROBE_2A;
					break;
				case STROBE_2:
					Cmd = SET_STROBE_2B;
					break;
				case STROBE_3:
					Cmd = SET_STROBE_2C;
					break;
				default:
					error = true;
					ThrowError ( E_INVALIDARG, LoadString (IDS_STROBEOUTOFRANGE));
			}
			break;
		}
		if (( State < STROBE_OFF ) ||( State > STROBE_FLASH))	
		{
			error = true;
			ThrowError ( E_INVALIDARG, LoadString (IDS_STROBESTATEINVALID));
		}

		if ( !error )
		{
			sData.Format (_T ("%i"), State - 0x30);
			
			if (m_pDevThread)
				hr = m_pDevThread->TransmitCommand ( Cmd, sData, NULL );

			sData.ReleaseBuffer();
		}
	}
	else
	{
		hr = E_DEVICENOTOPEN;
		ThrowError( hr, IDS_DEVICE_NOT_OPEN );
	}
	return hr;
}

void CAuxBox::ThrowError(LONG Err, const int resID )
{
	CString sMsg;
	sMsg.LoadString ( resID );
	ThrowError ( Err, sMsg );
}

void CAuxBox::ThrowError(LONG Err, LPCTSTR pStr)
{
	CString sMsg;
	sMsg.Format (_T ("%s %ld/n%s"), LoadString (IDS_ERROR), Err, pStr );
	AfxMessageBox ( sMsg );
}

LONG CAuxBox::PutSerialData(short PortID, VARIANT *sData, short nCnt)
{
	CString sEvent;
	SCODE hr = E_DEVICENOTOPEN;

	if (m_bOpen && m_pDevThread) {
		switch ( PortID ) {
			case 1:
				hr = m_pDevThread->TransmitCommand ( WRITE_SERIAL_PORT1, sData->pbVal, nCnt, NULL );
				break;
			case 2:
				hr = m_pDevThread->TransmitCommand ( WRITE_SERIAL_PORT2, sData->pbVal, nCnt, NULL );
				break;
			case 3:
				hr = m_pDevThread->TransmitCommand ( WRITE_SERIAL_PORT3, sData->pbVal, nCnt, NULL );
				break;
			case 4:
				hr = m_pDevThread->TransmitCommand ( WRITE_SERIAL_PORT4, sData->pbVal, nCnt, NULL );
				break;
			default:
				hr = E_INVALIDARG;
				sEvent.Format (LoadString (IDS_INVALIDPORT), PortID );
				ThrowError( hr, sEvent);
		}
	}
	else
		ThrowError( hr, IDS_DEVICE_NOT_OPEN);
	return hr;
}

LONG CAuxBox::SetSerialPortParams(short PortID, short Enabled, LPCTSTR Baud, LPCTSTR Parity, LPCTSTR Databits, LPCTSTR Stopbits)
{
	SCODE hr = E_INVALIDARG;
	CString sData;
	CString sEvent;

	sData.Format (_T ("%s,%s,%s,%s,%s"), (Enabled ? _T("Y") : _T("N")), Baud, Parity, Databits, Stopbits);

	if (m_bOpen && m_pDevThread) {
		switch ( PortID ) {
			case 1:
				hr =  m_pDevThread->TransmitCommand ( SETUP_PORT1, sData, NULL );
				break;
			case 2:
				hr =  m_pDevThread->TransmitCommand ( SETUP_PORT2, sData, NULL );
				break;
			case 3:
				hr =  m_pDevThread->TransmitCommand ( SETUP_PORT3, sData, NULL );
				break;
			case 4:
				hr =  m_pDevThread->TransmitCommand ( SETUP_PORT4, sData, NULL );
				break;
			default:
				ThrowError( hr, LoadString (IDS_INVALIDPORTARG));
		}
	}
	else
	{
		hr = E_DEVICENOTOPEN;
		ThrowError( hr, IDS_DEVICE_NOT_OPEN);
	}
	return hr;
}

LONG CAuxBox::GetPS2Data(VARIANT *pData, short *nSize)
{
	SCODE hr = S_OK;
	if (m_bOpen && m_pDevThread)
	{
		if ( pData->vt == (VT_UI1|VT_BYREF) ) {
			if ( !m_pDevThread->m_PS2Data.IsEmpty() ) {
				_SOCKET_PACKET *p = (_SOCKET_PACKET *) m_pDevThread->m_PS2Data.RemoveHead();
				if ( (unsigned int) *nSize >= p->Header.Length ) {
					memcpy ( pData->pbVal, p->Data, p->Header.Length);
					*nSize = (short) p->Header.Length;
					delete p;
				}
				else {
					m_pDevThread->m_PS2Data.AddHead( p );
					hr = E_BUFFERTOSMALL;
				}
			}
			else
				hr = E_FAIL;
		}
		else
			hr = E_INVALIDARG;
	}
	else
	{
		hr = E_DEVICENOTOPEN;
		ThrowError( hr, IDS_DEVICE_NOT_OPEN);
	}
	return hr;
}

LONG CAuxBox::GetDigitalInputs(VARIANT *vInput)
{
	SCODE hr = S_OK;
	if (m_bOpen && m_pDevThread)
	{
		if ( vInput->vt == VT_UI1 ) 
			vInput->bVal = m_pDevThread->m_DigitalInputs;			
	}
	else
	{
		hr = E_DEVICENOTOPEN;
		ThrowError( hr, IDS_DEVICE_NOT_OPEN);
	}
	return hr;

}

LONG CAuxBox::GetSerialData(short nPort, VARIANT *sBuf, short *nSize)
{
	SCODE hr = E_INVALIDARG;
	if (m_bOpen && m_pDevThread)
	{
		if (( nPort <= NUM_SERIAL_PORTS ) && (nPort > -1) ) 
		{
			if ( sBuf->vt == (VT_UI1|VT_BYREF) ) 
			{
				_SERIAL_PACKET *p = m_pDevThread->GetSerialData ( nPort );
				if ( p )
				{
					if ( (unsigned int) *nSize >= p->Length ) {
						memcpy ( sBuf->pbVal, p->Data, p->Length);
						*nSize = (short) p->Length;
						hr = S_OK;
					}
					else 
					{
						memcpy ( sBuf->pbVal, p->Data, *nSize);
						hr = E_BUFFERTOSMALL;
					}
					delete p;
				}
				else
					*nSize = 0;
			}
		}
	}
	else
	{
		hr = E_DEVICENOTOPEN;
		ThrowError( hr, IDS_DEVICE_NOT_OPEN);
	}
	return hr;
}

LONG CAuxBox::SetDigitalOutputs(VARIANT *vOutput)
{
	SCODE hr = S_OK;
	CString sEvent;
	unsigned char Data;

	if ( m_bOpen )
	{
		if ( m_pDevThread != NULL ) 
		{
			if ( vOutput->vt == VT_UI1 ) 
			{			
				if (m_nLastDigitalInput != vOutput->bVal) {
					TRACEF (TOBINSTR (vOutput->bVal));
					m_nLastDigitalInput = Data = vOutput->bVal;
					hr = m_pDevThread->TransmitCommand ( SET_OUTPUT_1, &Data, 1, NULL );
				}
			}
		}
	}
	else
	{
		hr = E_DEVICENOTOPEN;
		ThrowError( hr, IDS_DEVICE_NOT_OPEN);
	}
	return hr;
}

bool CAuxBox::IsOpen()
{
	if (m_pDevThread)
		return m_pDevThread->m_bConnected;

	return false;

//	return m_bOpen;
}

CString CAuxBox::GetIpAddress()
{
	return m_sAddress;
}

LONG CAuxBox::SetIpAddress(LPCTSTR lpszAddress)
{
	SCODE hr = E_INVALIDARG;
//	HANDLE hEvent;
	if (m_bOpen && m_pDevThread) 
	{
		if ( m_pDevThread != NULL ) 
		{
			hr = m_pDevThread->TransmitCommand ( SET_DEVICE_ADDRESS, (PUCHAR)(LPCSTR)w2a (lpszAddress), _tcslen (lpszAddress), NULL );
//			hr = m_pDevThread->TransmitCommand ( SET_DEVICE_ADDRESS, (PUCHAR)lpszAddress, strlen(lpszAddress), &hEvent );
//			if ( hr == S_OK )
//			{
//				if ( WaitForSingleObject ( hEvent, 2000) != WAIT_TIMEOUT )
//				{
//					CCmdPkt *pCmd = m_pDevThread->GetResponse( hEvent );
//					if ( pCmd )
//					{
//						if ( pCmd->Pkt.Header.Cmd == COMMAND_ACK )
//						{
//							ULONG lAddress = inet_addr ( lpszAddress );
//							while ( !PostThreadMessage ( m_pDevThread->m_nThreadID, TM_ADDRESS_CHANGE, 0, lAddress)) Sleep(0);
//						}
//						else
//							hr = E_COMMANDFAILED;
//					}
//				}
//				else
//					hr = E_COMMANDTIMEOUT;
//			}
//			else
//				hr = E_TRANSMITFAILED;
			ULONG lAddress = inet_addr (w2a (lpszAddress));
			while ( !PostThreadMessage ( m_pDevThread->m_nThreadID, TM_AUX_ADDRESS_CHANGE, m_nPort, lAddress)) Sleep(0);
		}
	}
	else
	{
		hr = E_DEVICENOTOPEN;
		ThrowError( hr, IDS_DEVICE_NOT_OPEN);
	}
	return hr;
}

bool CAuxBox::HasDigitalInputError ()
{
	VARIANT v;

	::VariantInit (&v);
	
	v.vt	= VT_UI1;
	v.bVal	= 0;

	if (IsOpen ())
		GetDigitalInputs (&v);
	
	// examine least significant bit
	// 0 - stop printing
	// 1 - state is clear
	return !(v.bVal & 0x01);
}

void CAuxBox::SetDigitalState (bool bLowInk, bool bSystemReady)
{
	if (IsOpen ()) {
		VARIANT v;

		::VariantInit (&v);
		
		v.vt	= VT_UI1;
		v.bVal	= 0;

		v.bVal |= bLowInk		? (1 << 7) : 0; // 8th bit: low ink
		v.bVal |= bSystemReady	? (1 << 6) : 0; // 7th bit: system ready

		SetDigitalOutputs (&v);
	}
}