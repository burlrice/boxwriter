// DevThread.cpp : implementation file
//

#include "stdafx.h"
#include <afxmt.h>

#include "TypeDefs.h"
#include "CmdPkt.h"
#include "DevThread.h"
#include "CmdPkt.h"
#include "Resource.h"
#include "Defines.h"
#include "Debug.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDevThread

IMPLEMENT_DYNCREATE(CDevThread, CWinThread)

CDevThread::CDevThread() 
:	m_pSocket(NULL), 
	m_CurrentBatchID(0),
	m_hExit(0), 
	m_hTxCueHasData(0), 
	m_bAddressChanged (false), 
	m_bConnected (false), 
	m_bConnecting( false),
	m_DigitalInputs (0x01) // hub logic is really inverted. see: CAuxBox::HasDigitalInputError
{
//	m_bAutoDelete = false;
}

CDevThread::~CDevThread()
{
}

BOOL CDevThread::InitInstance()
{
	SetPriorityClass ( this, REALTIME_PRIORITY_CLASS);
	CString sEvent;
	BOOL result = false;
	if ( AfxSocketInit(NULL) )
	{
		
		sEvent.Format (_T ("Exit %lu"), m_nThreadID);
		m_hExit = CreateEvent ( NULL, true, false, sEvent);

		sEvent.Format (_T ("TxCueHasData %lu"), m_nThreadID);
		m_hTxCueHasData = CreateEvent ( NULL, true, false, sEvent);
		
		m_bConnecting = false;

		m_RxBufCnt[0] = 0;
		m_RxBufCnt[1] = 0;
		m_RxBufCnt[2] = 0;
		m_RxBufCnt[3] = 0;
		m_pSocket = new CDevSock( m_nThreadID );
//		if ( m_pSocket->Create() )
			result = true;
//		else
//		{
//			AfxMessageBox (_T ("Socket Create Failed"));
//			AfxThrowUserException();
//		}
	}
	else
		AfxMessageBox (LoadString (IDS_SOCKETINITFAILED));
	return result;
}

int CDevThread::ExitInstance()
{
	if ( m_hExit != NULL )
		CloseHandle ( m_hExit );
	if ( m_hTxCueHasData ) 
		CloseHandle ( m_hTxCueHasData );

	while  ( !m_TxCue.IsEmpty() )
		delete (CCmdPkt *) m_TxCue.RemoveHead();
	CPtrList m_RxSerial[NUM_SERIAL_PORTS];

	_SERIAL_PACKET *p = NULL;

	for ( int i=0; i < NUM_SERIAL_PORTS; i++ )
	{
		while  ( (p = GetSerialData ( i )) )
		{
			delete p;
			p = NULL;
		}
	}

	if ( m_pSocket != NULL ) {
		m_pSocket->Close();
		delete m_pSocket;
		m_pSocket = NULL;
	}
	return 0;
}

BEGIN_MESSAGE_MAP(CDevThread, CWinThread)
	//{{AFX_MSG_MAP(CDevThread)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_THREAD_MESSAGE (TM_AUX_SOCKET_DATA, OnSocketData)
	ON_THREAD_MESSAGE (TM_AUX_SET_WND_HANDLE, OnSetWindowHandle)
	ON_THREAD_MESSAGE (TM_AUX_ADDRESS_CHANGE, OnAddressChange)
	ON_THREAD_MESSAGE (TM_AUX_CONNECT_COMPLETE, OnConnectComplete)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDevThread message handlers

int CDevThread::Run() 
{
	MSG WinMsg;
	int nEventCnt = 0;
	HANDLE hEvents[4];
	hEvents[nEventCnt++] = m_hExit;
	hEvents[nEventCnt++] = m_hTxCueHasData;
	CTime tmLast = CTime::GetCurrentTime ();
	
//	SetThreadPriority ( THREAD_PRIORITY_ABOVE_NORMAL );

	while ( 1 ) 
	{
		switch ( MsgWaitForMultipleObjects ( nEventCnt, hEvents, false, 100, QS_ALLINPUT ) )
		{
			case WAIT_OBJECT_0:
				PostMessage( m_hNotifyWnd, WM_AUX_CONNECT_STATE_CHANGED, 0, (LPARAM)m_hThread );
				return ExitInstance();
			case WAIT_OBJECT_0 +1:	//TxCueHasData
				ResetEvent ( m_hTxCueHasData );
				if ( !ProcessTxCue() )
					SendMessage( m_hNotifyWnd, WM_AUX_CONNECT_STATE_CHANGED, 0, (LPARAM)m_hThread );
				break;
		}
		while (PeekMessage (&WinMsg, NULL, NULL, NULL, PM_NOREMOVE))
			if ( !PumpMessage() )
				return ExitInstance();

		if ( m_bAddressChanged )
		{
			m_bAddressChanged = false;
			if ( m_bConnected )
			{
				m_pSocket->Close();
				m_bConnected = false;
				SendMessage (m_hNotifyWnd, WM_AUX_CONNECT_STATE_CHANGED, 0, (LPARAM)m_hThread);
			}
		}

		if (( !m_bConnected ) && ( !m_bConnecting ) )
		{
			if ( !m_sAddress.IsEmpty() )
			{
				if ( m_pSocket->Create() )
				{
					ULONG lResult;
					
					//TRACEF (m_sAddress);

					if ( !m_pSocket->Connect ( m_sAddress, m_nPort ) ) {
						lResult = GetLastError ();
						if (  lResult == WSAEWOULDBLOCK )
							m_bConnecting = true;
						else
							ASSERT ( 0 );
					}
					else { TRACEF ("Connect: " + m_sAddress + " failed"); }

				}
				else
					ASSERT ( 0 );
			}
		}

		CTimeSpan span = CTime::GetCurrentTime () - tmLast;

		if (span.GetTotalSeconds () > 0) {
			CheckConnection();
			UpdatePendingCue();
			tmLast = CTime::GetCurrentTime ();
		}

		//::Sleep (0);

	}
	return ExitInstance();;
}

void CDevThread::Shutdown()
{
	SetEvent ( m_hExit );
}

void CDevThread::OnSetWindowHandle(WPARAM nParam, LPARAM lParam)
{
	m_hNotifyWnd = (HWND) lParam;
}

void CDevThread::OnAddressChange(WPARAM nParam, LPARAM lParam)
{
	m_nPort = nParam;
	IN_ADDR addr;
	addr.S_un.S_addr = lParam;
	m_sAddress = inet_ntoa( addr );
	TRACEF (m_sAddress);
	m_bAddressChanged = true;
}

void CDevThread::OnSocketData(WPARAM nParam, LPARAM lParam)
{
	_SOCKET_PACKET *pPacket;
	CPtrList PacketList;
	m_pSocket->GetPackets ( PacketList );
	LOCK (m_PS2CueSentinel);

	while ( !PacketList.IsEmpty() ) {
		bool Transfered = false;
		pPacket = ( _SOCKET_PACKET * )PacketList.RemoveHead();
		/* TODO: rem
		{  
			CString str (' ', pPacket->Header.Length);
			CString strCmd;
			
			strCmd.Format ("0x%02X", pPacket->Header.Cmd);

			for (int i = 0; i < (int)pPacket->Header.Length; i++)
				if (isascii (pPacket->Data [i]))
					str.SetAt (i, pPacket->Data [i]);

			TRACEF (strCmd + ": " + str);
		}
		*/
		switch ( pPacket->Header.Cmd ) 
		{
			case DATA_FROM_PORT1:	// Serial Data from port A
			case DATA_FROM_PORT2:	// Serial Data from port B
			case DATA_FROM_PORT3:	// Serial Data from port C
			case DATA_FROM_PORT4:	// Serial Data from port D
				{
					int PortID = pPacket->Header.Cmd - DATA_FROM_PORT1;
					if ( m_RxBufCnt[PortID] + pPacket->Header.Length < MAX_SERIAL_DATA )
					{
						memcpy ( m_RxSerialBuf[PortID] + m_RxBufCnt[PortID], pPacket->Data, pPacket->Header.Length);
						m_RxBufCnt[PortID] += pPacket->Header.Length;
					}
					else
					{
						memset (m_RxSerialBuf[PortID], 0x00, MAX_SERIAL_DATA );
						m_RxBufCnt[PortID] = 0;
					}
					PUCHAR p = (PUCHAR) memchr ( m_RxSerialBuf[PortID], '\r', m_RxBufCnt[PortID]);
					if ( p != NULL )
					{
						_SERIAL_PACKET *pPkt = new _SERIAL_PACKET;
						memset ( pPkt->Data, 0x00, MAX_SERIAL_DATA);
						pPkt->Length = (p - m_RxSerialBuf[PortID]);
						memcpy (pPkt->Data, m_RxSerialBuf[PortID], pPkt->Length);
						
						{
							LOCK (m_SerialCueSentinel);
							m_RxSerial[PortID].AddTail ( pPkt );

							p++;
							m_RxBufCnt[PortID] -= (p - m_RxSerialBuf[PortID]);
							memcpy ( m_RxSerialBuf[PortID], p, m_RxBufCnt[PortID] );
						}

						SendMessage (m_hNotifyWnd, WM_AUX_SERIAL_DATA, PortID, (LPARAM)m_hThread);
					}
				}
				break;
			case DATA_FROM_PS2_PORT:	// Data from PS2 Port
				m_PS2Data.AddTail ( pPacket );
				Transfered = true;
				SendMessage (m_hNotifyWnd, WM_AUX_PS2_DATA, 0, (LPARAM)m_hThread);
				break;
			case READ_INPUT_1:
				m_DigitalInputs = pPacket->Data[0];
				SendMessage (m_hNotifyWnd, WM_AUX_DIGITAL_INPUT_DATA, m_DigitalInputs, (LPARAM)m_hThread);
				break;
		}
		LOCK (m_PendingCueSentinel);
		CCmdPkt *pCmd;
		if ( m_PendingCue.Lookup ( pPacket->Header.BatchID, pCmd ) )
		{
			m_PendingCue.RemoveKey ( pPacket->Header.BatchID );
			LOCK (m_ResponseCueSentinel);
			pCmd->Pkt = *pPacket;
			m_ResponseCue.SetAt ( pCmd->hEvent, pCmd );
			SetEvent ( pCmd->hEvent );
		}
		if ( !Transfered )
			delete pPacket;
	}
}

_SERIAL_PACKET * CDevThread::GetSerialData(short nPort)
{
	LOCK (m_SerialCueSentinel);
	_SERIAL_PACKET *p = NULL;
	if (( nPort < NUM_SERIAL_PORTS) && ( nPort > -1) )
		if ( !m_RxSerial[nPort].IsEmpty() )
			p = (_SERIAL_PACKET *)m_RxSerial[nPort].RemoveHead();
	return p;
}

SCODE CDevThread::TransmitCommand (unsigned long Cmd, CString sData, PHANDLE pEvent)
{
	return TransmitCommand ( Cmd, (PUCHAR)(LPSTR)w2a (sData), sData.GetLength(), pEvent );
}

SCODE CDevThread::TransmitCommand (unsigned long Cmd, const unsigned char* data, unsigned long Len, PHANDLE pEvent)
{
	LOCK (m_TxCueSentinel);

	SCODE result = CTL_E_DEVICEUNAVAILABLE;
	CString sEvent;
	if ( m_pSocket )
	{
		if ( m_bConnected )
		{
			CCmdPkt *pCmd = new CCmdPkt();
			pCmd->Pkt.Header.Cmd = Cmd;
			pCmd->Pkt.Header.Length = Len;
			pCmd->Pkt.Header.BatchID = ++m_CurrentBatchID;
			memcpy ( pCmd->Pkt.Data, data, Len);
			if ( pEvent ) 
			{
				sEvent.Format (_T ("Response%lu:%lu"), m_hThread, m_CurrentBatchID);
				*pEvent = pCmd->hEvent = CreateEvent ( NULL, true, false, sEvent);
			}
			m_TxCue.AddTail ( pCmd );
			SetEvent ( m_hTxCueHasData );
			result = S_OK;
		}
	}
	return result;
}

void CDevThread::UpdatePendingCue()
{
	LOCK (m_PendingCueSentinel);
	CCmdPkt *pCmd = NULL;
	ULONG lKey;
	POSITION pos = m_PendingCue.GetStartPosition();
	while ( pos ) {
		m_PendingCue.GetNextAssoc( pos, lKey, pCmd );
		CTime CurTime = CTime::GetCurrentTime();
		CTimeSpan Elasped = CurTime - pCmd->TimeStamp;
		if ( Elasped.GetTotalSeconds() > 1 ) {
			m_PendingCue.RemoveKey ( lKey );
			if ( pCmd->hEvent != NULL ) 
				SetEvent ( pCmd->hEvent );
			delete pCmd;
		}
	}
}

bool CDevThread::ProcessTxCue ( void )
{
	LOCK (m_TxCueSentinel);
	bool result = false;
	while ( !m_TxCue.IsEmpty() )
	{
		CCmdPkt *pCmd = ( CCmdPkt * ) m_TxCue.RemoveHead();
		if (m_pSocket->TransmittPacket( pCmd->Pkt.Header.Cmd, pCmd->Pkt.Header.BatchID, pCmd->Pkt.Data, pCmd->Pkt.Header.Length) ) 
		{
			if ( pCmd->hEvent )
			{
				CSingleLock sLock ( &m_PendingCueSentinel );
				sLock.Lock();
					m_PendingCue.SetAt ( pCmd->Pkt.Header.BatchID, pCmd );
				sLock.Unlock();
			}
			else
				delete pCmd;
			result = true;
		}
		else
			TRACE0 ( "Auxiliary.DLL -> Transmit Failed. Closing Socket\n" );
	}
	return result;
}

CCmdPkt * CDevThread::GetResponse(HANDLE hEvent)
{
	LOCK (m_ResponseCueSentinel);
	CCmdPkt *pCmd = NULL;
	if ( m_ResponseCue.Lookup ( hEvent, pCmd ) )
		m_ResponseCue.RemoveKey ( hEvent );
	return pCmd;
}

void CDevThread::OnConnectComplete(UINT uParam, LONG lParam)
{
	UINT ErrorCode = uParam;
	m_bConnecting = false;
	if ( ErrorCode == 0 )
	{
		m_bConnected = true;
		SendMessage (m_hNotifyWnd, WM_AUX_CONNECT_STATE_CHANGED, 1, (LPARAM)m_hThread);
	}
	else
		m_pSocket->Close();
}

void CDevThread::CheckConnection()
{
	if (m_bConnected) {
		DWORD dwResult = 0;

		LRESULT lResult = ::SendMessageTimeout (m_hNotifyWnd, WM_AUX_KEEP_ALIVE, 0, 0,
			SMTO_BLOCK | SMTO_ABORTIFHUNG, 500, &dwResult);

		if (dwResult == 0)
			TRACEF ("no response to WM_AUX_KEEP_ALIVE");

		if (!m_pSocket->TransmittPacket (COMMAND_ACK, 0, NULL, 0) ||
			dwResult == 0) 
		{
			m_bConnected = false;
			m_pSocket->Close();
			SendMessage (m_hNotifyWnd, WM_AUX_CONNECT_STATE_CHANGED, 0, (LPARAM)m_hThread);
		}
	}
}
