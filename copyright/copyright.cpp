// copyright.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "copyright.h"
#include <string>
#include <map>
#include <memory>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

typedef struct
{
	int a;
	int b;
	int c;
	int d;
} VERSION;

// The one and only application object

CWinApp theApp;

using namespace std;

std::vector<std::wstring> tokenize(const std::wstring & str, const std::wstring & delimeters = _T (" ,\t\n"))
{
	std::vector<std::wstring> v;
	wstring strInternal = str;
	wchar_t * const s = (wchar_t * const)strInternal.c_str ();

	for (TCHAR * token = _tcstok(s, delimeters.c_str ()); token != NULL; token = _tcstok(NULL, delimeters.c_str())) {
		v.push_back (token);
	}

	return v;
}

VERSION GetVersion(const string & strFile)
{
	CString str = ReadDirect(a2w(strFile).c_str());
	vector <wstring> v = explode((wstring)str, '\n');
	LPCTSTR lpsz[] =
	{
		_T("__MAJOR__"),
		_T("__MINOR__"),
		_T("__CUSTOM__"),
		_T("__BUILD__"),
	};
	int n[4] = { 0 };

	for (int i = 0; i < v.size (); i++) {
		vector <wstring> vLine = tokenize (v [i]);

		if (vLine.size() >= 3) {
			for (int j = 0; j < ARRAYSIZE(lpsz); j++) {
				if (vLine[0] == _T("#define") && vLine[1] == lpsz[j]) {
					wstring s = vLine [2];
					int x = 0;
					CString strTmp = implode(vLine, (wstring)_T(" ")).c_str ();

					strTmp.Trim ();
					TRACEF(strTmp);

					if (_stscanf(s.c_str(), _T("%d"), &x) == 1)
						n[j] = x;
					else {
						for (int k = 0; k < s.length(); k++)
							s[k] = _isdigit_l(s[k], 0) ? s[k] : ' ';

						if (_stscanf(s.c_str(), _T("%d"), &x) == 1)
							n[j] = x;
					}
				}
			}
		}
	}

	return VERSION { n[0], n[1], n[2], n[3] };
}

CString GetVersion (const VERSION & version)
{
	CString str;

	str.Format(_T(" %d, %d, %d, %d"), version.a, version.b, version.c, version.d);

	return str;
}


int main(int argv, char * argc [])
{
    int nRetCode = 0;
	const CString strCompany = _T("FoxJet, An ITW Company");

    HMODULE hModule = ::GetModuleHandle(nullptr);

    if (hModule != nullptr)
    {
        // initialize MFC and print and error on failure
        if (!AfxWinInit(hModule, nullptr, ::GetCommandLine(), 0))
        {
            // TODO: change error code to suit your needs
            wprintf(L"Fatal Error: MFC initialization failed\n");
            nRetCode = 1;
        }
        else
        {
            // TODO: code your application's behavior here.
        }
    }
    else
    {
        // TODO: change error code to suit your needs
        wprintf(L"Fatal Error: GetModuleHandle failed\n");
        nRetCode = 1;
    }

	std::string strAppVer	= argv >= 2 ? argc[1] : "C:\\Dev\\sw0925\\Editor\\DLL\\Common\\AppVer.h";
	std::string strRC		= argv >= 3 ? argc[2] : "C:\\Dev\\sw0925\\Editor\\mkdraw.rc";

	//strRC = "C:\\Dev\\sw0925\\Editor\\DLL\\Document\\Document.rc";
	//strRC = ("C:\\Dev\\sw0925\\Editor\\DLL\\Debug\\Debug.rc");
	//strRC = ("C:\\Dev\\sw0925\\Editor\\DLL\\Database\\Database.rc");
	//strRC = ("C:\\Dev\\sw0925\\Editor\\DLL\\Utils\\Utils.rc");
	//strRC = ("C:\\Dev\\sw0925\\Editor\\DLL\\ElementList\\Win\\ElementList.rc");
	//strRC = ("C:\\Dev\\sw0925\\Editor\\DLL\\3d\\3d.rc");
	//strRC = ("C:\\Dev\\sw0925\\Editor\\mkdraw.rc");

	//strAppVer =  "C:\\Dev\\sw0925\\Keyboard\\..\\Editor\\DLL\\Common\\AppVer.h";
	//strRC = "C:\\Dev\\sw0925\\Restore\\Restore\\Restore.rc";

	//strRC = "C:\\Dev\\sw0925\\Control\\Control.rc";
	//strRC = "C:\\Dev\\sw0925\\Keyboard\\WinKey\\WinKey.rc";

	//strAppVer = "C:\\Dev\\sw0925\\Keyboard\\..\\Editor\\DLL\\Common\\AppVer.h";
	//strRC = "C:\\Dev\\sw0925\\Keyboard\\KbMonitor\\KbMonitor.rc";

	TRACEF (strAppVer);
	TRACEF (strRC);

	if (argv >= 3 && strRC != argc[2]) {
		TRACEF(_T("******************** DEBUG ***** DEBUG ***** DEBUG ***** DEBUG *************************"));
		ASSERT (0);
	}

	/*
	if (FILE * fp = _tfopen(_T("c:\\Foxjet\\copyright.txt"), _T("a"))) {
		std:string str;

		str += std::to_string(::GetFileAttributesA(strAppVer.c_str())) + " ";
		str += strAppVer + std::string("\t\t");

		str += std::to_string(::GetFileAttributesA(strRC.c_str())) + " ";
		str += strRC + std::string("\t\t");

		str += "\n";
		
		fwrite(str.c_str(), str.length(), 1, fp);
	}
	*/

	CString str = ReadDirect(a2w(strRC).c_str());
	const CString strOriginal = str;
	vector <wstring> v = explode ((wstring)str, '\n');
	int nCount = 0;
	map <wstring, wstring> mapUpdate;

	VERSION version = GetVersion (strAppVer);
	TRACEF (GetVersion (version));

	if (!version.a || !version.b)
		return -1;

	for (int i = 0; i < v.size(); i++) {
		vector <wstring> vLine = tokenize(v[i], _T(" "));

		if (vLine.size() >= 2) {
			if (vLine[0] == _T("FILEVERSION") || vLine [0] == _T ("PRODUCTVERSION")) {
				CString strVersion;

				strVersion.Format (_T ("%d,%d,%d,%d"), version.a, version.b, version.c, version.d);
				CString strFind = v [i].c_str ();
				CString strReplace = strFind;
				int nIndex = strFind.Find (vLine [1].c_str ());

				if (nIndex != -1) {
					strReplace.Delete (nIndex, vLine [1].size ());
					strReplace.Insert (nIndex, strVersion);

					//TRACEF(strFind);
					//TRACEF(strReplace);

					if (strFind != strReplace) {
						Trace(make_printable(trim(v[i])), a2w(strRC).c_str(), i + 1);
						mapUpdate[(LPCTSTR)strFind] = strReplace;
					}
				}
			}
		}
	}

	for (int i = 0; i < v.size(); i++) {
		vector <wstring> vLine = tokenize(v[i], _T("\""));

		//for (const wstring & wstr : vLine) {
		for (int n = 0; n < vLine.size (); n++) {
			const wstring & wstr = vLine [n];
			static const wstring strCopyright = _T ("copyright");
			CString s = wstr.c_str ();

			s.MakeLower ();

			if (s.Find(strCopyright.c_str()) != -1) {
				bool bUpdated = false;

				//TRACEF (v [i]);
				//TRACEF(implode(vLine, (wstring)_T("'")));

				if (s.Find(_T("2003")) != -1) {
					CString strReplace = wstr.c_str();
					int nIndex = strReplace.Find(_T("-"));

					if (nIndex != -1) {
						wstring wstrReplace = wstring ((LPCTSTR)(strReplace.Left(nIndex + 1) + CTime::GetCurrentTime().Format(_T("%Y, ") + strCompany)));

						if (wstr != wstrReplace) {
							Trace (make_printable(trim (v [i])), a2w (strRC).c_str (), i + 1);
							mapUpdate[wstr] = wstrReplace;
							bUpdated = true;
						}
					}
				}
				else if (vLine.size() == 2 && n == 1) {
					// IDS_COPYRIGHT           "Copyright (C) 2016"

					if (!_tcsicmp(wstr.substr(0, strCopyright.length()).c_str(), strCopyright.c_str())) {
						wstring wstrReplace = (LPCTSTR)CTime::GetCurrentTime().Format(_T("Copyright � %Y, ") + strCompany);

						if (wstr != wstrReplace) {
							Trace(make_printable(trim(v[i])), a2w(strRC).c_str(), i + 1);
							mapUpdate[wstr] = wstrReplace;
							bUpdated = true;
						}
					}
				}
			}
		}

		//TRACEF(implode(vLine, (wstring)_T("'")));

		if (vLine.size() >= 4) {
			if (vLine[1] == _T("FileVersion") || vLine[1] == _T("ProductVersion") || vLine[1] == _T("LegalCopyright") || vLine[1] == _T ("CompanyName")) {
				CString strFind = v[i].c_str();
				CString strReplace = strFind;

				//TRACEF(v[i]);

				int nIndex = strReplace.Find(vLine[3].c_str());

				if (nIndex != -1) {
					strReplace.Delete(nIndex, vLine[3].length());

					if (vLine[1] == _T("LegalCopyright")) 
						strReplace.Insert(nIndex, CTime::GetCurrentTime ().Format (_T ("Copyright � 2003-%Y, ") + strCompany));
					else if (vLine[1] == _T("CompanyName"))
						strReplace.Insert (nIndex, strCompany);
					else
						strReplace.Insert(nIndex, GetVersion(version));

					if (strFind != strReplace) {
						//TRACEF (strReplace);
						Trace(make_printable(trim(v[i])), a2w(strRC).c_str(), i + 1);
						mapUpdate[(LPCTSTR)strFind] = strReplace;
					}
				}
			}
		}
	}

	TRACEF (std::to_string (mapUpdate.size()) + " updates pending...");

	{
		int nMax = 0;

		for (const auto & pair : mapUpdate) 
			nMax = max (wstring (pair.first + _T("[ ]")).length (), nMax);

		for (const auto & pair : mapUpdate) {
			CString s;

			s += _T("[");
			s += pair.first.c_str();
			//s.Trim();
			s += _T("] ");
			s += CString (' ', abs (nMax - s.GetLength()));
			s += _T (" --> [");
			s += pair.second.c_str();
			//s.Trim();
			s += _T("]");
			s.Replace(_T("\n"), _T(""));
			s.Replace(_T("\r"), _T(""));

			TRACEF (make_printable (wstring ((LPCTSTR)s)));
			//TRACEF(to_hex_string((const UCHAR *)pair.first.c_str(), pair.first.length() * 2));
			//TRACEF(to_hex_string((const UCHAR *)pair.second.c_str(), pair.second.length() * 2));
		}
	}

	if (mapUpdate.size ()) {
		int n = 0;

		bool bUTF16 = IsFileUTF16(a2w(strRC).c_str());

		for (const auto & pair : mapUpdate) 
			n += str.Replace (pair.first.c_str (), pair.second.c_str ());

		if (!bUTF16)
			str.Replace (_T("\r\n"), _T("\n"));

		//TRACEF(str);

		TRACEF(std::to_string(n) + " replaced");

		if (n) {
			/* debugging 
			{
				CString strOut = a2w(strRC).c_str();

				int nIndex = strOut.ReverseFind('\\');

				if (nIndex != -1) {
					strOut = _T("C:\\Foxjet\\Debug\\input") + strOut.Mid(nIndex);
					::DeleteFile (strOut);
					::CopyFile (a2w (strRC).c_str (), strOut, FALSE);
				}
			}

			{
				CString strOut = a2w (strRC).c_str ();

				int nIndex = strOut.ReverseFind ('\\');

				if (nIndex != -1) {
					strOut = _T ("C:\\Foxjet\\Debug\\output") + strOut.Mid (nIndex);

					if (FILE * fp = _tfopen(strOut, _T("wb"))) {
						TRACEF (_T ("Updating: ") + strOut);

						if (!bUTF16) {
							string s = w2a((wstring)str);
							fwrite(s.c_str(), s.length(), 1, fp);
						}
						else {
							wstring s = str;
							const BYTE nUTF16[] = { 0xFF, 0xFE };
							int nLen = (s.length() * sizeof(wchar_t)) + ARRAYSIZE(nUTF16);
							std::unique_ptr <BYTE> p (new BYTE [nLen]);

							memset (p.get(), 0, nLen);
							memcpy (p.get (), nUTF16, ARRAYSIZE (nUTF16));
							memcpy (p.get () + ARRAYSIZE(nUTF16), s.c_str (), s.length() * sizeof(wchar_t));

							fwrite(p.get (), nLen, 1, fp);
						}

						fclose (fp);
					}
				}
			}
			*/
			CString strOut = a2w(strRC).c_str();

			if (FILE * fp = _tfopen(strOut, _T("wb"))) {
				TRACEF (_T ("Updating: ") + strOut);

				if (!bUTF16) {
					string s = w2a((wstring)str);
					fwrite(s.c_str(), s.length(), 1, fp);
				}
				else {
					wstring s = str;
					const BYTE nUTF16[] = { 0xFF, 0xFE };
					int nLen = (s.length() * sizeof(wchar_t)) + ARRAYSIZE(nUTF16);
					std::unique_ptr <BYTE> p(new BYTE[nLen]);

					memset(p.get(), 0, nLen);
					memcpy(p.get(), nUTF16, ARRAYSIZE(nUTF16));
					memcpy(p.get() + ARRAYSIZE(nUTF16), s.c_str(), s.length() * sizeof(wchar_t));

					fwrite(p.get(), nLen, 1, fp);
				}

				fclose(fp);
			}

			return n;
		}
	}

	return nRetCode;
}
