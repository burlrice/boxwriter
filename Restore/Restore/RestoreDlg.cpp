
// RestoreDlg.cpp : implementation file
//

#include "stdafx.h"

#include <WinUser.h>
#include <Portabledevice.h>
#include <PortableDeviceTypes.h>
#include <setupapi.h>
#include <initguid.h>
#include <dbt.h>
#include <odbcinst.h>

#include "Parse.h"
#include "Restore.h"
#include "RestoreDlg.h"
#include "afxdialogex.h"
#include "WinMsg.h"
#include "Registry.h"
#include "ContentTransfer.h"
#include "DbTypeDefs.h"

using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CRestoreDlg::CItem::CItem (const CString & strName, const CTime & tmModified, const CString & strID)
:	m_strName (strName), 
	m_strID (strID),
	m_ptmModified (new CTime (tmModified))
{
}

CRestoreDlg::CItem::~CItem ()
{
	if (m_ptmModified) {
		delete m_ptmModified;
		m_ptmModified = NULL;
	}
}

int CRestoreDlg::CItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	switch (nColumn) {
	case 0: return m_strName.Compare (((CItem &)rhs).m_strName);
	case 1: 
		if (m_ptmModified && ((CItem &)rhs).m_ptmModified) {
			FILETIME f1, f2;
			SYSTEMTIME s1, s2;

			m_ptmModified->GetAsSystemTime (s1);
			::SystemTimeToFileTime (&s1, &f1);

			((CItem &)rhs).m_ptmModified->GetAsSystemTime (s2);
			::SystemTimeToFileTime (&s2, &f2);

			return ::CompareFileTime (&f1, &f2);
		}
	}

	return CItem::Compare (rhs, nColumn);
}

CString CRestoreDlg::CItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:	return m_strName;
	case 1: return m_ptmModified ? m_ptmModified->Format (_T ("%c")) : _T ("");
	}

	return _T ("");
}





// CRestoreDlg dialog




CRestoreDlg::CRestoreDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRestoreDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_MAINFRAME);
}

void CRestoreDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_RESTORE,	IDB_RESTORE);
}

BEGIN_MESSAGE_MAP(CRestoreDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_CBN_SELCHANGE(CB_DRIVES, &CRestoreDlg::OnSelchangeCbDrives)
	ON_BN_CLICKED(BTN_RESTORE, &CRestoreDlg::OnBnClickedRestore)
END_MESSAGE_MAP()


// CRestoreDlg message handlers

void CRestoreDlg::EnumDrives (const CString & strSelect)
{
	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_DRIVES)) {
		CString strSelectLower = strSelect;
		DWORD dw = GetLogicalDrives();
		CDeviceList v = EnumDevices (GUID_DEVINTERFACE_WPD);

		if (p->GetDroppedState ())
			p->ShowDropDown (FALSE);

		strSelectLower.MakeLower ();
		p->ResetContent ();

		for (int i = 3; i < sizeof (DWORD) * 8; i++) {
			if (dw & (1 << i)) {
				CString str, strDir = CString ((TCHAR)((int)'A' + i)) + _T (":");
				TCHAR szName [MAX_PATH] = { 0 };
				TCHAR szSysName [MAX_PATH] = { 0 };
				DWORD dwSerial = 0, dwMax = 0;

				GetVolumeInformation (strDir + _T ("\\"), szName, ARRAYSIZE (szName), &dwSerial, &dwMax, NULL, szSysName, ARRAYSIZE (szSysName));

				if (_tcslen (szName))
					str.Format (_T ("%s (%s)"), szName, strDir);
				else
					str.Format (_T ("(%s)"), strDir);

				int nIndex = p->AddString (str);

				str.MakeLower ();

				if (str.Find (strSelectLower) != -1)
					p->SetCurSel (nIndex);
			}
		}

		p->InsertString (0, LoadString (IDS_LOCAL));

		for (int i = 0; i < v.GetSize (); i++) {
			CDevice & d = v [i];
			CString str = _T ("\\\\") + d.m_strDeviceDescription + _T ("\\");
			int nIndex = p->AddString (str);

			if (!str.CompareNoCase (strSelect))
				p->SetCurSel (nIndex);
		}

		if (p->GetCurSel () == CB_ERR) 
			p->SetCurSel (0);

		EnumFiles (GetCurSelDir ());
	}
}

void CRestoreDlg::EnumFiles (const CString & strDir)
{
	CDeviceList v = EnumDevices (GUID_DEVINTERFACE_WPD);

	m_lv.DeleteCtrlData ();

	for (int i = 0; i < v.GetSize (); i++) {
		CDevice & d = v [i];
		CString str = _T ("\\\\") + d.m_strDeviceDescription + _T ("\\");

		if (!str.CompareNoCase (strDir)) {
			CComPtr <IPortableDevice> device = OpenDevice (d.m_interfaceDetail.DevicePath);

			if (device) {
				CComPtr<IPortableDeviceContent> content;
							
				if (SUCCEEDED (device->Content (&content))) {
					DEVICE_FILE_STRUCT root;

					BeginWaitCursor ();
					Enumerate (content, WPD_DEVICE_OBJECT_ID, root, 10);
					Trace (root);
					DEVICE_FILE_STRUCT zip = GetFiles (root, _T (".zip"));
					Trace (zip);

					for (int j = 0; j < zip.m_v.GetSize (); j++) {
						DEVICE_FILE_STRUCT & f = zip.m_v [j];

						m_lv.InsertCtrlData (new CItem (f.m_strName, f.m_tm, f.m_strID));
					}

					m_lv->SortItems (ItiLibrary::ListCtrlImp::CListCtrlImp::DyslexicCompareFunc, 1);
					m_lv.GetListCtrl ().SetItemState (0, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
					EndWaitCursor ();
					return;
				}
			}
		}
	}

	if (!strDir.CompareNoCase (LoadString (IDS_LOCAL))) {
		CString str = ::GetCommandLine () + CString (_T (" "));
		str.MakeLower ();
		CString strDir = FoxjetDatabase::Extract (str, _T ("/local_backup_dir=\""), _T ("\""));

		if (strDir.GetLength ()) {
			CStringArray v;

			GetFiles (strDir, _T ("*.mdb"), v);

			for (int i = 0; i < v.GetSize (); i++) {
				CTime tm = CTime::GetCurrentTime ();
				CString strTitle = v [i];
				HANDLE hFile = ::CreateFile (v [i], 
					GENERIC_WRITE, 
					FILE_SHARE_READ | FILE_SHARE_WRITE, 
					NULL, 
					OPEN_EXISTING, 
					FILE_ATTRIBUTE_NORMAL, 
					0);

				if (hFile) {
					FILETIME ft;

					::GetFileTime (hFile, NULL, NULL, &ft);
					tm = CTime (ft);

					::CloseHandle (hFile);
				}

				strTitle.Replace (strDir + _T ("\\"), _T (""));
				m_lv.InsertCtrlData (new CItem (strTitle, tm));
			}
		}
	}
	else {
		CStringArray v;
		CString str = FoxjetDatabase::Extract (strDir, _T ("("), _T (":)"));

		GetFiles (str + _T (":\\"), _T ("*.zip"), v);

		for (int i = 0; i < v.GetSize (); i++) {
			CTime tm = CTime::GetCurrentTime ();
			CString strTitle = v [i];
			HANDLE hFile = ::CreateFile (v [i], 
				GENERIC_WRITE, 
				FILE_SHARE_READ | FILE_SHARE_WRITE, 
				NULL, 
				OPEN_EXISTING, 
				FILE_ATTRIBUTE_NORMAL, 
				0);

			strTitle.Replace (str + _T (":\\"), _T (""));

			if (hFile) {
				FILETIME ft;

				::GetFileTime (hFile, NULL, NULL, &ft);
				tm = CTime (ft);

				::CloseHandle (hFile);
			}

			m_lv.InsertCtrlData (new CItem (strTitle, tm));
		}
	}

	m_lv->SortItems (ItiLibrary::ListCtrlImp::CListCtrlImp::DyslexicCompareFunc, 1);
	m_lv.GetListCtrl ().SetItemState (0, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
}

CString CRestoreDlg::GetCurSelDir () const
{
	CString strResult;

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_DRIVES))
		p->GetLBText (p->GetCurSel (), strResult);

	return strResult;
}

BOOL CRestoreDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	CArray <ItiLibrary::ListCtrlImp::CColumn, ItiLibrary::ListCtrlImp::CColumn> vCols;

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	vCols.Add (ItiLibrary::ListCtrlImp::CColumn (_T ("Name"), 440));
	vCols.Add (ItiLibrary::ListCtrlImp::CColumn (_T ("Date"), 200));

	m_vPortableDevices = EnumDevices (GUID_DEVINTERFACE_WPD);
	m_lv.Create (LV_FILES, vCols, this, _T ("SOFTWARE\\FoxJet\\Control\\Settings\\CRestoreDlg"));

	
	{
		CString str = ::GetCommandLine ();

		str.MakeLower ();

		EnumDrives (FoxjetDatabase::Extract (str, _T ("/select=\""), _T ("\"")));
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CRestoreDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CRestoreDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



LRESULT CRestoreDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
	case WM_DEVICECHANGE:
		OnDeviceChange (wParam, lParam);
		break;
	}

	return CDialogEx::WindowProc(message, wParam, lParam);
}


LRESULT CRestoreDlg::OnDeviceChange (WPARAM wParam, LPARAM lParam)
{
	switch (wParam) {
	case DBT_DEVICEARRIVAL:		
		{
			bool bLogicalDrive = false;

			if (DEV_BROADCAST_HDR * p = (DEV_BROADCAST_HDR *)lParam) {
				switch (p->dbch_devicetype) {
				case DBT_DEVTYP_VOLUME:	
					if (DEV_BROADCAST_VOLUME * pInfo = (DEV_BROADCAST_VOLUME *)p) {
						for (int i = 0; i < sizeof (DWORD) * 8; i++) {
							if (pInfo->dbcv_unitmask & (1 << i)) {
								CString strDir = CString ((TCHAR)((int)'A' + i)) + _T (":");
								CStringArray v;

								bLogicalDrive = true;
								TRACEF (_T ("DEVICE ARRIVAL: ") + strDir);
								GetFiles (strDir + _T ("\\"), _T ("*.zip"), v);

								for (int i = 0; i < v.GetSize (); i++)
									TRACEF (v [i]);

								EnumDrives (v.GetSize () ? strDir : GetCurSelDir ());
							}
						}
					}
					break;
				}
			}

			if (!bLogicalDrive) {
				CDeviceList v = EnumDevices (GUID_DEVINTERFACE_WPD);

				for (int i = 0; i < v.GetSize (); i++) {
					if (Find (m_vPortableDevices, v [i]) == -1) {
						CString strDevice = v [i].m_interfaceDetail.DevicePath;
						CString strDesc = v [i].m_strDeviceDescription;

						EnumDrives (_T ("\\\\") + strDesc + _T ("\\"));
						m_vPortableDevices = v;
					}
				}
				
				m_vPortableDevices = v;
			}
		}
		break;
	case DBT_DEVICEREMOVECOMPLETE:		
		if (DEV_BROADCAST_HDR * p = (DEV_BROADCAST_HDR *)lParam) {
			m_vPortableDevices = EnumDevices (GUID_DEVINTERFACE_WPD);
			EnumDrives (GetCurSelDir ());
		}
		break;
	case DBT_DEVNODES_CHANGED:
		//{
		//	CDeviceList v = EnumDevices ();
		//	
		//	for (int i = 0; i < v.GetSize (); i++) {
		//		if (CMainFrame::Find (m_vPortableDevices, v [i]) == -1) {
		//			if (!v [i].m_strDeviceDescription.CompareNoCase (_T ("Apple iPhone"))) {
		//				m_vUSB = v;
		//				break;
		//			}
		//		}
		//	}

		//	m_vUSB = v;
		//}
		break;
	}

	return TRUE;
}


void CRestoreDlg::OnSelchangeCbDrives()
{
	EnumFiles (GetCurSelDir ());
}

bool CRestoreDlg::Extract (UINT nID, const CString & strPath, const CString & strType, const CString & strModule)
{
	bool bResult = false; //::GetFileAttributes (strPath) != -1;

	if (!bResult) {
		HMODULE hModule = ::GetModuleHandle (strModule);

		if (HRSRC hRSRC = ::FindResource (hModule, MAKEINTRESOURCE (nID), strType)) {
			DWORD dwSize = ::SizeofResource (hModule, hRSRC);

			if (HGLOBAL hGlobal = ::LoadResource (hModule, hRSRC)) {
				if (LPVOID lpData = ::LockResource (hGlobal)) {
					if (FILE * fp = _tfopen (strPath, _T ("wb"))) {
						fwrite (lpData, dwSize, 1, fp);
						fclose (fp);
						TRACEF (_T ("wrote: ") + strPath);
						bResult = true;
					}
				}
			}
		}
	}

	return bResult;
}

CString CRestoreDlg::GetTempPath ()
{
	TCHAR szTemp [MAX_PATH] = { 0 };
	
	::GetTempPath (ARRAYSIZE (szTemp) - 1, szTemp);
	::GetLongPathName (szTemp, szTemp, ARRAYSIZE (szTemp) - 1);
	CString strPath = szTemp;

	if (strPath.GetLength ())
		if (strPath [strPath.GetLength () - 1] != '\\')
			strPath += '\\';

	return strPath;
}

static void CreateMdbDsn (const CString & strDSN, const CString & strFilename, const CString & strDir)
{
	static const LPCTSTR lpszDBDriver = SQL_MDB_DRIVER;
	TCHAR sz [512];
	TCHAR szAttr [512];
	LPCTSTR lpszFormat = 
		_T ("DSN=%s~ ")					// dsn
		_T ("DBQ=%s\\%s~ ")				// file
		_T ("Description=%s~ ")			// dsn
		_T ("FileType=MicrosoftAccess~ ")
		_T ("DataDirectory=%s~ ")
		_T ("MaxScanRows=20~ ")
		_T ("DefaultDir=%s~ ")
		_T ("FIL=MS Access;~ ")
		_T ("MaxBufferSize=2048~ ")
		_T ("MaxScanRows=8~ ")
		_T ("PageTimeout=5~ ")
		_T ("Threads=3~ ");

	_stprintf (sz, lpszFormat, 
		strDSN, 
		strDir, strFilename,
		strDSN,
		strDir, 
		strDir);

	int nLen = _tcsclen (sz);

	_tcscpy (szAttr, (sz));
	TRACEF (szAttr);

	for (int i = 0; i < nLen; i++)
		if (szAttr [i] == '~')
			szAttr [i] = '\0';

	::SQLConfigDataSource (NULL, ODBC_REMOVE_SYS_DSN, lpszDBDriver, (_T ("DSN=") + strDSN));

	BOOL bSQLConfigDataSource = SQLConfigDataSource (NULL, ODBC_ADD_SYS_DSN, lpszDBDriver, szAttr);

	if (!bSQLConfigDataSource)
		TRACE_SQLERROR ();

	ASSERT (bSQLConfigDataSource);
}

static void CreateAccdbDsn (const CString & strDSN, const CString & strFilename)
{
	static const LPCTSTR lpszACCDBDriver = SQL_ACCDB_DRIVER;
	TCHAR sz [512];
	TCHAR szAttr [512];
	LPCTSTR lpszFormat = 
		_T ("DSN=%s~ ")				// dsn
		_T ("DBQ=%s~ ");			// file

	_stprintf (sz, lpszFormat, strDSN, strFilename);

	int nLen = _tcsclen (sz);

	_tcscpy (szAttr, (sz));
	TRACEF (szAttr);

	for (int i = 0; i < nLen; i++)
		if (szAttr [i] == '~')
			szAttr [i] = '\0';

	::SQLConfigDataSource (NULL, ODBC_REMOVE_SYS_DSN, lpszACCDBDriver, (_T ("DSN=") + strDSN));
	BOOL bSQLConfigDataSource = SQLConfigDataSource (NULL, ODBC_ADD_SYS_DSN, lpszACCDBDriver, szAttr);
	
	if (!bSQLConfigDataSource)
		TRACE_SQLERROR ();

	ASSERT (bSQLConfigDataSource);

}

static void CreateXlsDsn (const CString & strDSN, const CString & strFilename)
{
	static const LPCTSTR lpszACCDBDriver = SQL_XLS_DRIVER;
	TCHAR sz [512];
	TCHAR szAttr [512];
	LPCTSTR lpszFormat = 
		_T ("DSN=%s~ ")				// dsn
		_T ("DBQ=%s~ ");			// file

	_stprintf (sz, lpszFormat, strDSN, strFilename);

	int nLen = _tcsclen (sz);

	_tcscpy (szAttr, (sz));
	TRACEF (szAttr);

	for (int i = 0; i < nLen; i++)
		if (szAttr [i] == '~')
			szAttr [i] = '\0';

	::SQLConfigDataSource (NULL, ODBC_REMOVE_SYS_DSN, lpszACCDBDriver, (_T ("DSN=") + strDSN));
	BOOL bSQLConfigDataSource = SQLConfigDataSource (NULL, ODBC_ADD_SYS_DSN, lpszACCDBDriver, szAttr);

	if (!bSQLConfigDataSource)
		TRACE_SQLERROR ();

	ASSERT (bSQLConfigDataSource);
}

bool CRestoreDlg::DoRestore (const CString & strZipSrc, const CString & strZipDest)
{
	CString str;
	bool bResult = false;

	TRACEF (strZipSrc + _T (" --> ") + strZipDest);

	if (strZipSrc.CompareNoCase (strZipDest) != 0) {
		if (!::CopyFile (strZipSrc, strZipDest, FALSE)) {
			MessageBox (LoadString (IDS_COPYFAILED) + _T ("\n") + strZipSrc + _T ("\n") + strZipDest + _T ("\n\n") + FormatMessage (GetLastError ()));
			return false;
		}
	}

	{
		CString strDirDest = strZipDest;
		CStringArray v;
		const CString strZip = GetTempPath () + _T ("7z.exe");

		VERIFY (Extract (IDR_7Z_EXE, strZip));
		VERIFY (Extract (IDR_7Z_DLL, GetTempPath () + _T ("7z.dll")));
		VERIFY (Extract (IDR_7ZIP_DLL, GetTempPath () + _T ("7-zip.dll")));

		strDirDest.Replace (_T (".zip"), _T (""));
		GetFiles (strDirDest, _T ("*.*"), v);

		for (int i = 0; i < v.GetSize (); i++)
			::DeleteFile (v [i]);

		DeleteDirectory (strDirDest);

		str.Format (_T ("\"%s\" x \"%s\" -o\"%s\""), strZip, strZipDest, strDirDest);
		TRACEF (str);
		str = execute (str);
		TRACEF (_T ("execute: ") + str);

		v.RemoveAll ();
		GetFiles (strDirDest, _T ("*.mdb"), v);

		if (v.GetSize () > 0) {
			CString strMdbSrc = v [0];
			CString strMdbDest = strMdbSrc;

			strMdbDest.Replace (strDirDest, GetHomeDir ());

			TRACEF (strMdbSrc + _T (" --> ") + strMdbDest);

			if (!::CopyFile (strMdbSrc, strMdbDest, FALSE))
				MessageBox (LoadString (IDS_COPYFAILED) + _T ("\n") + strMdbSrc + _T ("\n") + strMdbDest + _T ("\n\n") + FormatMessage (GetLastError ()));
			else {
				::DeleteFile (strMdbSrc);

				{
					CStringArray vSkip, vManual;

					v.RemoveAll ();
					GetFiles (strDirDest, _T ("*.dbq"), v);

					for (int i = 0; i < v.GetSize (); i++) {
						CString strDSN = v [i];

						strDSN.Replace (strDirDest + _T ("\\"), _T (""));
						strDSN.Replace (_T (".dbq"), _T (""));
						CString strDBQ = FoxjetDatabase::Extract (ReadDirect (v [i]), _T ("DBQ="), _T (";"));
						TRACEF (strDSN + _T (": ") + strDBQ);

						try 
						{
							CDatabase db;

							if (db.OpenEx (_T ("DSN=") + strDSN, CDatabase::noOdbcDialog)) {
								CString strDBQ2 = FoxjetDatabase::Extract (db.GetConnect (), _T ("DBQ="), _T (";"));

								if (!strDBQ.CompareNoCase (strDBQ2))
									vSkip.Add (strDSN);
							}
						}
						catch (CDBException * e)		{ TCHAR sz [1028] = { 0 }; e->GetErrorMessage (sz, ARRAYSIZE (sz)); TRACEF (sz); }
						catch (CMemoryException * e)	{ TCHAR sz [1028] = { 0 }; e->GetErrorMessage (sz, ARRAYSIZE (sz)); TRACEF (sz); }
					}

					v.RemoveAll ();
					GetFiles (strDirDest, _T ("*.reg"), v);

					for (int i = 0; i < v.GetSize (); i++) {
						const CString strFile = v [i];	
						CString strDSN = v [i];
						HKEY hKey = NULL;
						bool bOpenedDSN = false;

						strDSN.Replace (strDirDest + _T ("\\"), _T (""));
						strDSN.Replace (_T (".reg"), _T (""));
						TRACEF (strDSN);

						if (Find (vSkip, strDSN, false) != -1)
							continue;

						vManual.Add (strDSN);

						{
							CString strKey= _T ("SOFTWARE\\ODBC\\ODBC.INI\\") + strDSN;

							VERIFY (EnablePrivilege (SE_RESTORE_NAME));
							VERIFY (EnablePrivilege (SE_BACKUP_NAME));

							TRACEF (strKey);
							TRACEF (strFile);

							HKEY h [] = { HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE };

							for (int j = 0; j < ARRAYSIZE (h); j++) {
								LONG lResult = ::SHDeleteKey (h [j], strKey);

								if (lResult != ERROR_SUCCESS) 
									TRACEF (_T ("SHDeleteKey:\n") + FormatMessage (lResult));

								if (::RegCreateKey (h [j], strKey, &hKey) != ERROR_SUCCESS) 
									TRACEF (_T ("RegCreateKey:\n") + FormatMessage (lResult));
								else {
									lResult = ::RegRestoreKey (hKey, strFile, 0);

									if (lResult != ERROR_SUCCESS) 
										TRACEF (_T ("RegRestoreKey:\n") + FormatMessage (lResult));
									else {
										try {
											CDatabase db;

											if (db.OpenEx (_T ("DSN=") + strDSN, CDatabase::noOdbcDialog)) {
												bOpenedDSN = true;
												break;
											}
										}
										catch (CDBException * e)		{ TCHAR sz [1028] = { 0 }; e->GetErrorMessage (sz, ARRAYSIZE (sz)); TRACEF (sz); }
										catch (CMemoryException * e)	{ TCHAR sz [1028] = { 0 }; e->GetErrorMessage (sz, ARRAYSIZE (sz)); TRACEF (sz); }
									}
						
									::RegCloseKey (hKey);
								}
							}
						}

						if (!bOpenedDSN) {
							CString strAltFile = strFile;

							strAltFile.Replace (_T (".reg"), _T (".dbq"));
							CString strDBQ = FoxjetDatabase::Extract (ReadDirect (strAltFile), _T ("DBQ="), _T (";"));
							int nIndex = strDBQ.ReverseFind ('.');

							if (nIndex != -1) {
								const CString strExt = strDBQ.Mid (nIndex);

								if (strExt == _T (".mdb")) {
									int nIndex = strDBQ.ReverseFind ('\\');

									if (nIndex != -1) 
										CreateMdbDsn (strDSN, strDBQ.Mid (nIndex + 1), strDBQ.Left (nIndex));
								}
								else if (strExt == _T (".accdb"))
									CreateAccdbDsn (strDSN, strDBQ);
								else if (strExt == _T (".xls") || strExt == _T (".xlsx")) 
									CreateXlsDsn (strDSN, strDBQ);
							}
						}
					}

					if (vManual.GetSize ()) {
						CString strMsg = LoadString (IDS_MANUAL_CONFIG_DSNS);

						for (int i = 0; i < vManual.GetSize (); i++)
							strMsg += _T ("\n") + vManual [i];

						MessageBox (strMsg);
					}
				}


				v.RemoveAll ();
				GetFiles (strDirDest, _T ("*.*"), v);

				for (int i = 0; i < v.GetSize (); i++) {
					CString strFile = v [i].MakeLower ();

					if (strFile.Find (_T ("\\data.txt")) != -1) {
						CString strDest = GetHomeDir () + _T ("\\data.txt");
						::DeleteFile (strDest);
						::CopyFile (v [i], strDest, FALSE);
						break;
					}
				}

				if (v.GetSize ()) {
					const CString strProgramFilesSrc = v [0] + _T ("\\");

					for (int i = v.GetSize () - 1; i >= 0; i--) {
						if (::GetFileAttributes (v [i]) & FILE_ATTRIBUTE_DIRECTORY)
							v.RemoveAt (i);
						else if (BeginsWith (v [i], strProgramFilesSrc + _T ("Language\\")))
							v.RemoveAt (i);
						else {
							CString strTitle = v [i];

							strTitle.Replace (strProgramFilesSrc, _T (""));

							if (strTitle.Find ('\\') == -1) {
								if (EndsWith (strTitle, _T (".exe")) ||
									EndsWith (strTitle, _T (".dll")) ||
									EndsWith (strTitle, _T (".ocx")) ||
									EndsWith (strTitle, _T (".bit")))
								{
									v.RemoveAt (i);
								}
							}
						}
					}

					CStringArray vFailed;

					for (int i = 0; i < v.GetSize (); i++) {
						CString strSrc = v [i];
						CString strDest = strSrc;

						if (strDest.Replace (strProgramFilesSrc, GetHomeDir () + _T ("\\"))) {
							CreateDir (strDest);

							TRACEF (strSrc + _T (" --> ") + strDest);

							if (!::CopyFile (strSrc, strDest, FALSE)) {
								CString strTmp = strSrc;
										
								strTmp.Replace (strProgramFilesSrc, _T (""));
								vFailed.Add (strTmp);
							}
						}
					}

					if (!vFailed.GetSize ()) {
						bResult = true;
					}
					else {
						CString strError = LoadString (IDS_COPYFAILED) + _T (": ");

						for (int i = 0; i < vFailed.GetSize (); i++)
							strError += _T ("\n") + vFailed [i];

						MessageBox (strError);
					}
				}
			}

			TRACEF (_T ("DeleteDirectory :") + strDirDest);
			DeleteDirectory (strDirDest);
		}
	}

	return bResult;
}

void CRestoreDlg::OnBnClickedRestore()
{
	bool bShutdown			= false;
	bool bControlRunning	= false;
	bool bEditorRunning		= false;
	bool bKBRunning			= false;
	bool bKBMonRunning		= false;
	bool bClose				= false;
	HANDLE hMutexEditor = NULL, hMutexPrinter = NULL;
	HANDLE hMutexKB = NULL, hMutexKBMon = NULL;
	FoxjetCommon::CLongArray sel = ItiLibrary::GetSelectedItems (m_lv);

	if (sel.GetSize () != 1) {
		MessageBox (LoadString (IDS_NOTHINGSELECTED));
		return;
	}

	hMutexEditor = ::CreateMutex (NULL, FALSE, ISEDITORRUNNING);

	if (GetLastError () == ERROR_ALREADY_EXISTS || GetLastError () == ERROR_ACCESS_DENIED)
		bEditorRunning = bShutdown = true;

	hMutexPrinter = ::CreateMutex (NULL, FALSE, ISPRINTERRUNNING);

	if (GetLastError () == ERROR_ALREADY_EXISTS || GetLastError () == ERROR_ACCESS_DENIED)
		bControlRunning = bShutdown = true;

	hMutexKB = ::CreateMutex (NULL, FALSE, ISKEYBOARDRUNNING);

	if (GetLastError () == ERROR_ALREADY_EXISTS || GetLastError () == ERROR_ACCESS_DENIED)
		bKBRunning = bShutdown = true;

	hMutexKBMon = ::CreateMutex (NULL, FALSE, ISKEYBOARDMONITORRUNNING);

	if (GetLastError () == ERROR_ALREADY_EXISTS || GetLastError () == ERROR_ACCESS_DENIED)
		bKBMonRunning = bShutdown = true;

	{
		HANDLE h [] = { hMutexEditor, hMutexPrinter, hMutexKB, hMutexKBMon };

		for (int i = 0; i < ARRAYSIZE (h); i++)
			::CloseHandle (h [i]);
	}

	if (bShutdown) {
		CString str = LoadString (IDS_SHUTDOWN_APPS);

		if (MessageBox (str, NULL, MB_YESNO) == IDYES) {
			DWORD dwResult;

			BeginWaitCursor ();
			LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_SHUTDOWNAPP, SHUTDOWN_ALL, 0, 
				SMTO_BLOCK | SMTO_ABORTIFHUNG, 10000, &dwResult);
			::Sleep (2000);
			EndWaitCursor ();
		}
		else
			return;
	}


	BeginWaitCursor ();

	if (GetCurSelDir () == LoadString (IDS_LOCAL)) {
		CString str = ::GetCommandLine () + CString (_T (" "));
		str.MakeLower ();
		CString strDir = FoxjetDatabase::Extract (str, _T ("/local_backup_dir=\""), _T ("\""));
		CString strDBQ = FoxjetDatabase::Extract (str, _T ("/dbq=\""), _T ("\""));

		if (CItem * p = (CItem *)m_lv->GetItemData (sel [0])) {
			CString strSrc = strDir + _T ("\\") + p->m_strName;
			CString strDest = strDBQ;

			//MessageBox (strSrc + _T ("\n\n") + strDest);

			TRACEF (strSrc + _T (" --> ") + strDest);

			if (!::CopyFile (strSrc, strDest, FALSE))
				MessageBox (LoadString (IDS_COPYFAILED) + _T ("\n") + strSrc + _T ("\n") + strDest + _T ("\n\n") + FormatMessage (GetLastError ()));
		}
	}
	else {
		CString str = FoxjetDatabase::Extract (GetCurSelDir (), _T ("("), _T (":)"));

		if (str.GetLength ()) {
			if (CItem * p = (CItem *)m_lv->GetItemData (sel [0])) {
				CString strZipSrc = str + _T (":\\") + p->m_strName;
				CString strZipDest = GetTempPath () + p->m_strName;

				bClose = DoRestore (strZipSrc, strZipDest);
			}
		}
		else {
			if (CItem * p = (CItem *)m_lv->GetItemData (sel [0])) {
				if (p->m_strID.GetLength ()) {
					CDeviceList v = EnumDevices (GUID_DEVINTERFACE_WPD);
					CString strDevice = FoxjetDatabase::Extract (GetCurSelDir (), _T ("\\\\"), _T ("\\"));

					for (int i = 0; i < v.GetSize (); i++) {
						CDevice & d = v [i];

						if (!d.m_strDeviceDescription.CompareNoCase (strDevice)) {
							CComPtr <IPortableDevice> device = OpenDevice (d.m_interfaceDetail.DevicePath);

							if (device) {
								CString strZipDest = GetTempPath () + p->m_strName;
								CString strZipSrc = strZipDest;

								if (TransferContentFromDevice (device, strZipSrc, p->m_strID)) {
									TRACEF (strZipSrc);
									ASSERT (::GetFileAttributes (strZipSrc) != -1);
									bClose = DoRestore (strZipSrc, strZipDest);
								}
							}
						}

						break;
					}

				}
			}
		}
	}

	EndWaitCursor ();

	{
		CStringArray vLaunch;
		//CString strCmdLine = ::GetCommandLine ();

		//strCmdLine.Replace (FoxjetDatabase::Extract (strCmdLine, _T ("/DBQ=\""), _T ("\\")), _T (""));
		//strCmdLine.Replace (FoxjetDatabase::Extract (strCmdLine, _T ("/LOCAL_BACKUP_DIR=\""), _T ("\\")), _T (""));
		//strCmdLine.Replace (FoxjetDatabase::Extract (strCmdLine, _T ("/SELECT=\""), _T ("\\")), _T (""));

		//{
		//	TCHAR sz [MAX_PATH] = { 0 };

		//	::GetModuleFileName (::GetModuleHandle (NULL), sz, ARRAYSIZE (sz) - 1);
		//	strCmdLine = ::GetCommandLine ();
		//	strCmdLine.Delete (0, _tcslen (sz) + 2);
		//}

		{
			CString strKey = _T ("Software\\FoxJet\\Translator\\Cmd line");
		
			
			if (bControlRunning) 
				vLaunch.Add (_T ("\"") + GetHomeDir () + _T ("\\Control.exe\" ") + FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("Control"), _T ("")));
		
			if (bEditorRunning) 
				vLaunch.Add (_T ("\"") + GetHomeDir () + _T ("\\mkdraw.exe\" ") + FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("Editor"), _T ("")));

			//if (bKBRunning)
			//	vLaunch.Add (_T ("\"") + GetHomeDir () + _T ("\\Keyboard.exe\" ") + FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("Keyboard"), _T ("")));

			//if (bKBMonRunning)
			//	vLaunch.Add (_T ("\"") + GetHomeDir () + _T ("\\KbMonitor.exe\" ") + FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("Keyboard"), _T ("")));
		}

		theApp.WriteProfileString (_T ("Cmd line"), _T ("Control"),		_T (""));
		theApp.WriteProfileString (_T ("Cmd line"), _T ("Editor"),		_T (""));
		theApp.WriteProfileString (_T ("Cmd line"), _T ("Keyboard"),	_T (""));


		for (int i = 0; i < vLaunch.GetSize (); i++) {
			const CString strApp = _T ("Launch.exe");
			CString strPath = GetHomeDir () + _T ("\\") + strApp;
			CString strCmdLine = strPath + _T (" ") + vLaunch [i];

			STARTUPINFO si;     
			PROCESS_INFORMATION pi; 
			TCHAR szCmdLine [MAX_PATH];

			_tcsncpy (szCmdLine, strCmdLine, MAX_PATH);
			memset (&pi, 0, sizeof(pi));
			memset (&si, 0, sizeof(si));
			si.cb = sizeof(si);     
			si.dwFlags = STARTF_USESHOWWINDOW;     
			si.wShowWindow = SW_SHOW;     

			TRACEF (szCmdLine);
			::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
		}
	}

	if (bClose)
		PostMessage (WM_CLOSE);
}
