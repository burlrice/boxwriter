// RoundButtons.h: interface for the CRoundButtons class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROUNDBUTTONS_H__07091AD6_2563_4305_A74E_54A6BFB6B912__INCLUDED_)
#define AFX_ROUNDBUTTONS_H__07091AD6_2563_4305_A74E_54A6BFB6B912__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>
#include "RoundButton2.h"

namespace FoxjetUtils
{
	class CRoundButtons  
	{
	public:
		CRoundButtons ();
		virtual ~CRoundButtons ();

		void DoDataExchange(CDataExchange* pDX, UINT nCtrlID, UINT nBmpID = 0);

		COLORREF * m_prgbBack;

	protected:
		void Add (UINT nCtrlID, UINT nBmpID = 0);

		FoxjetUtils::CRoundButtonStyle m_styleDefault;
		CMap <UINT, UINT, CBitmap *, CBitmap *> m_mapBitmaps;
		CMap <UINT, UINT, FoxjetUtils::CRoundButton2 *, FoxjetUtils::CRoundButton2 *> m_mapButtons;
		CMap <UINT, UINT, UINT, UINT> m_map;
		bool m_bInit;
	};

	COLORREF GetDefColor (const CString & strRegKey);
	COLORREF GetButtonColor (const CString & strRegKey);
};

#endif // !defined(AFX_ROUNDBUTTONS_H__07091AD6_2563_4305_A74E_54A6BFB6B912__INCLUDED_)
