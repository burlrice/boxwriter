// RoundButtons.cpp: implementation of the CRoundButtons class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RoundButtons.h"
#include "Registry.h"
#include "Color.h"
#include "TemplExt.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
using namespace FoxjetUtils;

CRoundButtons::CRoundButtons ()
:	m_prgbBack (NULL),
	m_bInit (false)
{
}

CRoundButtons::~CRoundButtons ()
{
	if (m_prgbBack) {
		delete m_prgbBack;
		m_prgbBack = NULL;
	}

	{
		for (POSITION pos = m_mapButtons.GetStartPosition (); pos; ) {
			CRoundButton2 * p = NULL;
			UINT nID = 0;

			m_mapButtons.GetNextAssoc (pos, nID, p);

			if (p) 
				delete p;
		}

		m_mapButtons.RemoveAll ();
	}

	{
		for (POSITION pos = m_mapBitmaps.GetStartPosition (); pos; ) {
			CBitmap * p = NULL;
			UINT nID = 0;

			m_mapBitmaps.GetNextAssoc (pos, nID, p);

			if (p) 
				delete p;
		}

		m_mapBitmaps.RemoveAll ();
	}
}

void CRoundButtons::DoDataExchange(CDataExchange* pDX, UINT nCtrlID, UINT nBmpID)
{
	{
		UINT n = 0;

		if (!m_map.Lookup (nCtrlID, n))
			Add (nCtrlID, nBmpID);
	}

	if (!pDX->m_bSaveAndValidate) {
		if (CWnd * p = pDX->m_pDlgWnd->GetDlgItem (nCtrlID))
			p->UnsubclassWindow ();

		for (POSITION pos = m_mapButtons.GetStartPosition (); pos; ) {
			CRoundButton2 * pButton = NULL;
			UINT nCtrlID = 0;

			m_mapButtons.GetNextAssoc (pos, nCtrlID, pButton);

			if (pButton) {
				bool bInit = pButton->m_hWnd == NULL;

				if (CButton * p = (CButton *)pDX->m_pDlgWnd->GetDlgItem (nCtrlID))
					p->ModifyStyle (0, BS_BITMAP);

				DDX_Control(pDX, nCtrlID, * pButton);

				if (bInit) {
					CBitmap * pBitmap = NULL;
					UINT nBmpID = 0;
				
					m_map.Lookup (nCtrlID, nBmpID);

					if (nBmpID) {
						VERIFY (m_mapBitmaps.Lookup (nBmpID, pBitmap));
						pButton->SetBitmap ((HBITMAP)pBitmap->m_hObject);
					}
				}
			}
		}
	}
}

void CRoundButtons::Add (UINT nCtrlID, UINT nBmpID)
{
	COLORREF rgbFont			= GetButtonColor (_T ("button font"));
	COLORREF rgbFace			= GetButtonColor (_T ("button face"));
	COLORREF rgbBorder			= GetButtonColor (_T ("button border"));
	COLORREF rgbBack			= GetButtonColor (_T ("background"));
	COLORREF rgbShaded			= GetButtonColor (_T ("shaded"));
	COLORREF rgbClickedFace		= GetButtonColor (_T ("button face clicked"));
	COLORREF rgbClickedBorder	= GetButtonColor (_T ("button border clicked"));

	if (m_prgbBack)
		rgbBack = * m_prgbBack;

	if (!m_bInit) {
		tButtonStyle tStyle;

		m_styleDefault.GetButtonStyle (&tStyle);

		tStyle.m_tColorFace.m_tEnabled		= rgbFace;
		tStyle.m_tColorBorder.m_tEnabled	= rgbBorder;

		tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
		tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);

		tStyle.m_tColorFace.m_tPressed		= rgbClickedFace;
		tStyle.m_tColorBorder.m_tPressed	= rgbClickedBorder;

		tStyle.m_tColorBack.m_tEnabled		= 
		tStyle.m_tColorBack.m_tClicked		= 
		tStyle.m_tColorBack.m_tPressed		= 
		tStyle.m_tColorBack.m_tDisabled		= 
		tStyle.m_tColorBack.m_tHot			= rgbBack;//lb.lbColor;//::GetSysColor (COLOR_BTNFACE);

		m_styleDefault.SetButtonStyle (&tStyle);
		m_bInit = true;
	}

	CRoundButton2 * pButton = new CRoundButton2 ();
	tColorScheme tColor = { Color::rgbLightGray, rgbFont, rgbFont, rgbFont, rgbFont };

	pButton->SetRoundButtonStyle(&m_styleDefault);
	pButton->SetTextColor (&tColor);
	
	if (nBmpID) {
		CBitmap * pBitmap = new CBitmap ();
		
		VERIFY (pBitmap->LoadBitmap (nBmpID));

		m_map.SetAt (nCtrlID, nBmpID);
		m_mapBitmaps.SetAt (nBmpID, pBitmap);
	}

	m_mapButtons.SetAt (nCtrlID, pButton);
}

COLORREF FoxjetUtils::GetDefColor (const CString & strRegKey)
{
	struct 
	{
		LPCTSTR m_lpsz;
		COLORREF m_rgb;
	}
	map [] = 
	{
		{ _T ("button font"),			0x000000,				},
		{ _T ("button face"),			0xEEEEEE,				},
		{ _T ("button border"),			0x696969,				},
		{ _T ("background"),			0xF0F0F0,				},
		{ _T ("shaded"),				0xF0F0F0,				},
		{ _T ("button face clicked"),	RGB(0xFF, 0x80, 0x80),	},
		{ _T ("button border clicked"),	RGB(0xFF, 0x40, 0x40),	},
	};

	for (int i = 0; i < ARRAYSIZE (map); i++) 
		if (!strRegKey.Compare (map [i].m_lpsz))
			return map [i].m_rgb;

	return 0;
}

COLORREF FoxjetUtils::GetButtonColor (const CString & strRegKey)
{
	const CString strKey = _T ("Software\\Foxjet\\Control\\Settings\\Colors");

	return FoxjetDatabase::GetProfileInt (strKey, strRegKey, GetDefColor (strRegKey));
}