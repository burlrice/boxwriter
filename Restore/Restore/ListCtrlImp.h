// ListCtrlImp.h: interface for the CListCtrlImp class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LISTCTRLIMP_H__5501B7E9_388C_4441_BC73_C614BF6FE01D__INCLUDED_)
#define AFX_LISTCTRLIMP_H__5501B7E9_388C_4441_BC73_C614BF6FE01D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>
#include "CopyArray.h"

namespace ItiLibrary 
{
	FoxjetCommon::CLongArray GetSelectedItems (CListBox & lb);
	FoxjetCommon::CLongArray GetSelectedItems (CListCtrl & lv);
	void SelectAll (CListCtrl & lv, bool bSelect = true);
	void Select (CListCtrl & lv, int nIndex);
	bool SelectItemData(CComboBox *pCB, DWORD dwData);
	void SaveSettings(CListCtrl & lv, int nCols, const CString & strRegSection, const CString & strSection);
	void LoadSettings(CListCtrl & lv, const CString & strRegSection, const CString & strSection); 

	namespace ListCtrlImp
	{
		class CItem 
		{
		public:
			CItem ();
			virtual ~CItem ();

			virtual int Compare (const CItem & rhs, int nColumn) const;
			virtual CString GetDispText (int nColumn) const = 0;
			virtual bool IsNumeric (int nColumn) const { return false; }
		};

		class CSortInfo 
		{
		public:
			CSortInfo () : m_nLastCol (-1), m_bReverseOrder (false) {}

			int m_nLastCol;
			bool m_bReverseOrder;
		};

		class CColumn
		{
		public:
			CColumn (const CString & str = _T (""), 
					int nWidth = 80, int nFormat = LVCFMT_LEFT)
				: m_str (str), m_nWidth (nWidth), m_nFormat (nFormat)
			{
			}

			int			m_nFormat; 
			int			m_nWidth; 
			CString		m_str; 
		};

		class CListCtrlImp  
		{
		public:
			CListCtrlImp ();
			virtual ~CListCtrlImp();

			operator const CListCtrl & () const { return GetListCtrl (); }
			operator CListCtrl & () { return GetListCtrl (); }

			const CListCtrl * operator -> () const { return &GetListCtrl (); }
			CListCtrl * operator -> () { return &GetListCtrl (); }

			virtual void Create (UINT nListCtrlID, CArray <CColumn, CColumn> & vCols, CWnd * pParent,
				const CString & strRegSection);

			void RefreshCtrlData ();
			bool InsertCtrlData (CItem * pData);
			bool InsertCtrlData (CItem * pData, int nIndex);
			bool UpdateCtrlData (CItem * pItem);
			bool UpdateCtrlData (CItem * pItem, int nIndex);
			bool DeleteCtrlData (int nIndex);
			void DeleteCtrlData ();

			int GetDataIndex (const CItem * pData) const;
			CItem * GetCtrlData (int nIndex) const;

			CListCtrl & GetListCtrl () const;
			UINT GetListCtrlID () const;
			int GetColumnCount () const;

			void SetTouchScreenMultiSelect (bool b) { m_bMultiSelect = b; }
			bool IsTouchScreenMultiSelect () const { return m_bMultiSelect; }

			void SelectAll (bool bSelect = true) { ItiLibrary::SelectAll (GetListCtrl (), bSelect); }

			static int CALLBACK CompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParam3);
			static int CALLBACK DyslexicCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParam3);

		protected:

			void InitHeaders (CArray <CColumn, CColumn> & vCols);
			virtual void Destroy ();
			CWnd * GetParent () const;

			static void GetDispatch (HWND hWnd, CArray <CListCtrlImp *, CListCtrlImp *> & v);

			void OnColumnClick (NMHDR* pNMHDR, LRESULT* pResult);

			typedef LRESULT (CALLBACK * LISTCTRLIMP_WNDPROC) (HWND, UINT, WPARAM, LPARAM);
		
			void Create (UINT nListCtrlID, CArray <CColumn, CColumn> & vCols, CWnd * pParent,
				const CString & strRegSection, LISTCTRLIMP_WNDPROC lpWndProc);

			static LRESULT CALLBACK WindowProc (HWND hWnd, UINT nMsg, WPARAM wParam, LPARAM lParam);

			CSortInfo					m_sort;
			UINT						m_nListCtrlID;
			UINT						m_nColumns;
			CWnd *						m_pParent;
			WNDPROC						m_lpParentWndProc;
			CArray <CItem *, CItem *>	m_vCtrlData;
			static CMapPtrToPtr			m_vWndMap;
			UINT						m_nID;
			CString						m_strRegSection;
			bool						m_bMultiSelect;

			CImageList m_imglist;
			CImageList m_imglistBlank;
			CBitmap m_bmpUnchecked;
			CBitmap m_bmpChecked;
			int m_nUncheckedIndex;
			int m_nCheckedIndex;

		private:
			CListCtrlImp (const CListCtrlImp & rhs);					// no impl
			CListCtrlImp & operator = (const CListCtrlImp & rhs);		// no impl
		};
	}; //namespace ListCtrlImp

	bool Export (ListCtrlImp::CListCtrlImp & lv, const CString & strFile);
}; //namespace ItiLibrary


#endif // !defined(AFX_LISTCTRLIMP_H__5501B7E9_388C_4441_BC73_C614BF6FE01D__INCLUDED_)
