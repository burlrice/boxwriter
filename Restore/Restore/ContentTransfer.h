#pragma once
#include <PortableDeviceApi.h>
#include <PortableDevice.h>
#include <Strsafe.h>

// Reads a string property from the IPortableDeviceProperties
// interface and returns it
HRESULT GetStringValue(
    IPortableDeviceProperties* properties,
    PCWSTR                     objectID,
    REFPROPERTYKEY             key,
    PWSTR*                     value);

// Copies data from a source stream to a destination stream using the
// specified transferSizeBytes as the temporary buffer size.
HRESULT StreamCopy(
    _In_  IStream*    destStream,
    _In_  IStream*    sourceStream,
          DWORD       transferSizeBytes,
    _Out_ DWORD*      bytesWrittenOut);

// Transfers a selected object's data (WPD_RESOURCE_DEFAULT) to a temporary
// file.
bool TransferContentFromDevice(IPortableDevice* device, const CString & strFileName, const CString & strID);

// Deletes a selected object from the device.
//<SnippetDeleteContent1>
void DeleteContentFromDevice(
    _In_ IPortableDevice* device);

// Moves a selected object (which is already on the device) to another location on the device.
void MoveContentAlreadyOnDevice(
    _In_ IPortableDevice* device);

// Fills out the required properties for ALL content types...
HRESULT GetRequiredPropertiesForAllContentTypes(
    _In_ IPortableDeviceValues*  objectProperties,
    _In_ PCWSTR                  parentObjectID,
    _In_ PCWSTR                  filePath,
    _In_ IStream*                fileStream);

// Fills out the required properties for WPD_CONTENT_TYPE_IMAGE
HRESULT GetRequiredPropertiesForImageContentTypes(
    _In_ IPortableDeviceValues*  pObjectProperties);

// Fills out the required properties for WPD_CONTENT_TYPE_AUDIO
HRESULT GetRequiredPropertiesForMusicContentTypes(
    _In_ IPortableDeviceValues*  objectProperties);

// Fills out the required properties for WPD_CONTENT_TYPE_CONTACT
HRESULT GetRequiredPropertiesForContactContentTypes(
    _In_ IPortableDeviceValues*  objectProperties);

// Fills out the required properties for specific WPD content types.
HRESULT GetRequiredPropertiesForContentType(
    REFGUID                 contentType,
    PCWSTR                  parentObjectID,
    PCWSTR                  filePath,
    IStream*                fileStream,
    IPortableDeviceValues** objectProperties);

// Transfers a user selected file to the device
bool TransferContentToDevice(
    _In_ IPortableDevice* device,
    _In_ REFGUID          contentType,
    _In_ PCWSTR           lpszFrom,
    _In_ PCWSTR           lpszID);

// Transfers a user selected file to the device as a new WPD_RESOURCE_CONTACT_PHOTO resource
void CreateContactPhotoResourceOnDevice(
    _In_ IPortableDevice* device);

// Fills out the required properties for a properties-only
// contact named "John Kane".  This is a hard-coded
// contact.
HRESULT GetRequiredPropertiesForPropertiesOnlyContact(
    PCWSTR                  parentObjectID,
    IPortableDeviceValues** objectProperties);

HRESULT GetRequiredPropertiesForFolder(
   PCWSTR                  parentObjectID,
   PCWSTR                  folderName,
   IPortableDeviceValues** objectProperties);

// Creates a properties-only object on the device which is
// WPD_CONTENT_TYPE_CONTACT specific.
// NOTE: This function creates a hard-coded contact for
// "John Kane" always.
void TransferContactToDevice(
    _In_ IPortableDevice*    device);

// Creates a properties-only object on the device which is
// WPD_CONTENT_TYPE_FOLDER specific.
void CreateFolderOnDevice(
    _In_ IPortableDevice*    device);

// Fills out properties that accompany updating an object's data...
HRESULT GetPropertiesForUpdateData(
    PCWSTR                  filePath,
    IStream*                fileStream,
    IPortableDeviceValues** objectProperties);

// Updates a selected object's properties and data (WPD_RESOURCE_DEFAULT).
void UpdateContentOnDevice(
    _In_ IPortableDevice* device,
    _In_ REFGUID          contentType,
    _In_ PCWSTR           fileTypeFilter,
    _In_ PCWSTR           defaultFileExtension);
