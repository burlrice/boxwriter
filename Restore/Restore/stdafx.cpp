
// stdafx.cpp : source file that includes just the standard includes
// Restore.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include <SYS\STAT.H>
#include <afxtempl.h>
#include <io.h>
#include "WinMsg.h"
#include "..\..\Control\Host.h"
#include "ContentTransfer.h"
#include "AppVer.h"
#include "odbcinst.h"

using namespace FoxjetCommon;

/////////////////////////////////////////////////////////////////////////////

CDevice::CDevice ()
{
	memset (&m_dev, 0, sizeof (m_dev));
	memset (&m_interface, 0, sizeof (m_interface));
	memset (&m_interfaceDetail, 0, sizeof (m_interfaceDetail));
	memset (m_szDevicePath, 0, ARRAYSIZE (m_szDevicePath) * sizeof (m_szDevicePath [0]));
}

CDevice::CDevice (const SP_DEVINFO_DATA * pDev, const SP_DEVICE_INTERFACE_DATA * pInterface, const SP_DEVICE_INTERFACE_DETAIL_DATA_W * pInterfaceDetail)
{
	memcpy (&m_dev, pDev, sizeof (m_dev));
	memcpy (&m_interface, pInterface, sizeof (m_interface));
	memcpy (&m_interfaceDetail, pInterfaceDetail, sizeof (m_interfaceDetail));
	memset (m_szDevicePath, 0, ARRAYSIZE (m_szDevicePath) * sizeof (m_szDevicePath [0]));
	_tcsncpy (m_interfaceDetail.DevicePath, pInterfaceDetail->DevicePath, ARRAYSIZE (m_szDevicePath));
}

CDevice::CDevice (const CDevice & rhs)
{
	memcpy (&m_dev, &rhs.m_dev, sizeof (m_dev));
	memcpy (&m_interface, &rhs.m_interface, sizeof (m_interface));
	memcpy (&m_interfaceDetail, &rhs.m_interfaceDetail, sizeof (m_interfaceDetail));
	_tcsncpy (m_interfaceDetail.DevicePath, rhs.m_interfaceDetail.DevicePath, ARRAYSIZE (m_szDevicePath));
	m_strFriendlyName = rhs.m_strFriendlyName;
	m_strDeviceDescription = rhs.m_strDeviceDescription;
	m_strLocationInformation = rhs.m_strLocationInformation;
}

CDevice & CDevice::operator = (const CDevice & rhs)
{
	if (this != &rhs) {
		memcpy (&m_dev, &rhs.m_dev, sizeof (m_dev));
		memcpy (&m_interface, &rhs.m_interface, sizeof (m_interface));
		memcpy (&m_interfaceDetail, &rhs.m_interfaceDetail, sizeof (m_interfaceDetail));
		_tcsncpy (m_interfaceDetail.DevicePath, rhs.m_interfaceDetail.DevicePath, ARRAYSIZE (m_szDevicePath));
		m_strFriendlyName = rhs.m_strFriendlyName;
		m_strDeviceDescription = rhs.m_strDeviceDescription;
		m_strLocationInformation = rhs.m_strLocationInformation;
	}

	return * this;
}

CDevice::~CDevice ()
{
}

CString ToString (const GUID & guid)
{
	CString str;

	str.Format (_T ("%04X:%04X:%04X:%04X"), guid.Data1, guid.Data2, guid.Data3, guid.Data4);

	if (IsEqualGUID(WPD_CONTENT_TYPE_FOLDER, guid))
		str = _T ("WPD_CONTENT_TYPE_FOLDER: ") + str;

	return str;
}


CDeviceList EnumDevices (const GUID & guid)
{
	CDeviceList result;
	SP_DEVICE_INTERFACE_DATA         DevIntfData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA DevIntfDetailData;
	SP_DEVINFO_DATA                  DevData;

	DWORD dwSize, dwType, dwMemberIdx;
	HKEY hKey;
	BYTE lpData[1024];

	HDEVINFO hDevInfo = SetupDiGetClassDevs (&guid, NULL, 0, DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);
	
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DevIntfData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		dwMemberIdx = 0;
		
		SetupDiEnumDeviceInterfaces(hDevInfo, NULL, &guid, dwMemberIdx, &DevIntfData);

		while(GetLastError() != ERROR_NO_MORE_ITEMS)
		{

			DevData.cbSize = sizeof(DevData);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &DevIntfData, NULL, 0, &dwSize, NULL);

			DevIntfDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwSize);
			DevIntfDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

			if (SetupDiGetDeviceInterfaceDetail(hDevInfo, &DevIntfData, DevIntfDetailData, dwSize, &dwSize, &DevData)) {
				CDevice d (&DevData, &DevIntfData, DevIntfDetailData);
				TCHAR sz [MAX_PATH];
				bool bDuplicate = false;

				memset (sz, 0, ARRAYSIZE (sz) * sizeof (sz [0]));
				::SetupDiGetDeviceRegistryProperty (hDevInfo, &DevData, SPDRP_FRIENDLYNAME, NULL, (PBYTE)sz, ARRAYSIZE (sz), NULL);
				d.m_strFriendlyName = sz;

				memset (sz, 0, ARRAYSIZE (sz) * sizeof (sz [0]));
				::SetupDiGetDeviceRegistryProperty (hDevInfo, &DevData, SPDRP_DEVICEDESC, NULL, (PBYTE)sz, ARRAYSIZE (sz), NULL);
				d.m_strDeviceDescription = sz;

				memset (sz, 0, ARRAYSIZE (sz) * sizeof (sz [0]));
				::SetupDiGetDeviceRegistryProperty (hDevInfo, &DevData, SPDRP_LOCATION_INFORMATION, NULL, (PBYTE)sz, ARRAYSIZE (sz), NULL);
				d.m_strLocationInformation = sz;

				for (int i = 0; !bDuplicate && i < result.GetSize (); i++) 
					bDuplicate = _tcscmp (d.m_interfaceDetail.DevicePath, result [i].m_interfaceDetail.DevicePath) == 0;

				if (!bDuplicate)
					result.Add (d);
			}

			HeapFree(GetProcessHeap(), 0, DevIntfDetailData);

			// Continue looping
			SetupDiEnumDeviceInterfaces (hDevInfo, NULL, &guid, ++dwMemberIdx, &DevIntfData);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}

	#ifdef _DEBUG
	for (int i = 0; i < result.GetSize (); i++) {
		CString str;

		str.Format (_T ("[%03d] %-40s: %s"),
			i,
			result [i].m_strDeviceDescription,
			result [i].m_interfaceDetail.DevicePath);
		TRACEF (str);
	}
	#endif //_DEBUG

	return result;
}

int Find (const ::CDeviceList & v, const CDevice & find)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i] == find)
			return i;

	return -1;
}

/////////////////////////////////////////////////////////////////////////////

void Trace (LPCTSTR psz, LPCTSTR pszFile, ULONG nLine)
{
	CString str;

	str.Format (_T ("%s(%u): %s\n"), pszFile, nLine, psz);
	::OutputDebugString (str);
}

int GetFiles (const CString & strDir, const CString & strFile, CStringArray & v)
{
	if (strFile.Find (_T ("*")) == -1 && strFile != _T ("*.*")) {
		CStringArray vTmp;

		GetFiles (strDir, _T ("*.*"), vTmp);

		for (int i = 0; i < vTmp.GetSize (); i++) {
			CString str = vTmp [i];

			if (str.Replace (_T ("\\") + strFile, _T ("")))
				v.Add (vTmp [i]);
		}

		return v.GetSize ();
	}

	int nFiles = 0;
	WIN32_FIND_DATA fd;
	const CString str = strDir + _T ("\\") + strFile;
	HANDLE hFind = ::FindFirstFile (str, &fd);
	bool bMore = hFind != INVALID_HANDLE_VALUE;

	//TRACEF (str);

	while (bMore) {
		CString str = strDir;
		
		if (_tcscmp (fd.cFileName, _T (".")) != 0 && _tcscmp (fd.cFileName, _T (".."))) {
			if (str [str.GetLength () - 1] != '\\')
				str += '\\';

			str += fd.cFileName;

			v.Add (str);
			nFiles++;

			if (::GetFileAttributes (str) & FILE_ATTRIBUTE_DIRECTORY) 
				nFiles += GetFiles (strDir + _T ("\\") + fd.cFileName, strFile, v);
		}

		ZeroMemory (&fd, sizeof (fd));
		bMore = ::FindNextFile (hFind, &fd) ? true : false;
	}

	::FindClose (hFind);

	return nFiles;
}

CString Extract (const CString & strParse, const CString & strBegin)
{
	CString str;
	int nIndex = strParse.Find (strBegin);

	if (nIndex != -1) 
		str = strParse.Mid (nIndex + strBegin.GetLength ());

	return str;
}

CString Extract (const CString & strParse, const CString & strBegin, const CString & strEnd)
{
	CString str;
	int nIndex [2] = 
	{
		strParse.Find (strBegin),

		// start looking for strEnd after strBegin
		strParse.Find (strEnd, (nIndex [0] == -1) ? 0 : nIndex [0] + strBegin.GetLength ()), 
	};

	if (nIndex [0] != -1 && nIndex [1] != -1) {
		int nStart = nIndex [0] + strBegin.GetLength ();
		int nLen = nIndex [1] - nStart;

		if (nLen > 0)
			str = strParse.Mid (nStart, nLen);
	}

	return str;
}

bool IsRunning (LPCTSTR lpsz)
{
	HANDLE h = ::CreateMutex (NULL, FALSE, lpsz);
	bool bResult = GetLastError () == ERROR_ALREADY_EXISTS || GetLastError () == ERROR_ACCESS_DENIED;
	::CloseHandle (h);
	return bResult;
}

CString LoadString (UINT nID)
{
	CString str;

	str.LoadString (nID);

	return str;
}

CString GetHomeDir ()
{
	TCHAR sz [MAX_PATH] = { 0 };

	HMODULE hModule = ::GetModuleHandle (NULL);
	::GetModuleFileName (hModule, sz, ARRAYSIZE (sz) - 1);

	const CString strPath (sz);
	CString strPathLower (sz);
	strPathLower.MakeLower ();

	VERIFY (::GetFileTitle (strPath, sz, ARRAYSIZE (sz) - 1) == 0);
	
	CString strTitle (sz);
	strTitle.MakeLower ();

	int nIndex = strPathLower.Find (strTitle);

	ASSERT (nIndex != -1);
	CString str = strPath.Left (nIndex - 1);

	return str;
}

CString execute (const CString & str)
{
	CString strResult;
	HANDLE hOutputReadTmp,hOutputRead,hOutputWrite;
	HANDLE hInputWriteTmp,hInputRead,hInputWrite;
	HANDLE hErrorWrite;
	SECURITY_ATTRIBUTES sa;
	HANDLE hStdIn = NULL; // Handle to parents std input.

	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;

	if (!::CreatePipe (&hOutputReadTmp, &hOutputWrite, &sa, 0))
		TRACEF (FormatMessage (::GetLastError ()));

	// Create a duplicate of the output write handle for the std error
	// write handle. This is necessary in case the child application
	// closes one of its std output handles.
	if (!::DuplicateHandle (::GetCurrentProcess (), hOutputWrite, GetCurrentProcess(), &hErrorWrite, 0, TRUE, DUPLICATE_SAME_ACCESS))
		TRACEF (FormatMessage (::GetLastError ()));


	// Create the child input pipe.
	if (!::CreatePipe (&hInputRead, &hInputWriteTmp, &sa, 0))
		TRACEF (FormatMessage (::GetLastError ()));


	// Create new output read handle and the input write handles. Set
	// the Properties to FALSE. Otherwise, the child inherits the
	// properties and, as a result, non-closeable handles to the pipes
	// are created.
	if (!::DuplicateHandle (::GetCurrentProcess (), hOutputReadTmp, ::GetCurrentProcess (), &hOutputRead, 0, FALSE, DUPLICATE_SAME_ACCESS))
		TRACEF (FormatMessage (::GetLastError ()));

	if (!::DuplicateHandle (::GetCurrentProcess (), hInputWriteTmp, ::GetCurrentProcess (), &hInputWrite, 0, FALSE, DUPLICATE_SAME_ACCESS))
		TRACEF (FormatMessage (::GetLastError ()));


	// Close inheritable copies of the handles you do not want to be inherited.
	if (!::CloseHandle (hOutputReadTmp)) TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hInputWriteTmp)) TRACEF (FormatMessage (::GetLastError ()));


	// Get std input handle so you can close it and force the ReadFile to
	// fail when you want the input thread to exit.
	if ((hStdIn = GetStdHandle(STD_INPUT_HANDLE)) == INVALID_HANDLE_VALUE)
		TRACEF (FormatMessage (::GetLastError ()));

	{
		PROCESS_INFORMATION pi;
		STARTUPINFO si;
		TCHAR sz [MAX_PATH] = { 0 };

		ZeroMemory (&si, sizeof (STARTUPINFO));
		si.cb			= sizeof (STARTUPINFO);
		si.dwFlags		= STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
		si.wShowWindow	= SW_HIDE;
		si.hStdOutput	= hOutputWrite;
		si.hStdInput	= hInputRead;
		si.hStdError	= hErrorWrite;

		_tcscpy (sz, str);
		TRACEF (sz);

		if (!::CreateProcess (NULL, sz, NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi))
			TRACEF (FormatMessage (::GetLastError ()));

		if (!::CloseHandle (pi.hThread)) 
			TRACEF (FormatMessage (::GetLastError ()));
	}


	if (!::CloseHandle (hOutputWrite))	TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hInputRead ))	TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hErrorWrite))	TRACEF (FormatMessage (::GetLastError ()));

	while (1) {
		const int nSize = 256;
		CHAR lpBuffer [nSize + 1] = { 0 };
		DWORD nBytesRead = 0;
		DWORD nCharsWritten = 0;

		if (!::ReadFile (hOutputRead, lpBuffer, nSize, &nBytesRead, NULL) || !nBytesRead) {
			if (::GetLastError() == ERROR_BROKEN_PIPE)
				break; // pipe done - normal exit path.
			else
				TRACEF (FormatMessage (::GetLastError ()));
		}

		strResult += /* a2w */ (lpBuffer);
		//::OutputDebugString (a2w (lpBuffer)); ::Sleep (1);
	}

	if (!::CloseHandle (hOutputRead))	TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hInputWrite))	TRACEF (FormatMessage (::GetLastError ()));

	return strResult;
}

bool IsWin7 ()
{
	OSVERSIONINFO osver = { sizeof (osver) };

	::GetVersionEx (&osver);

	return (osver.dwMajorVersion >= 6 && osver.dwMinorVersion > 0);
}

bool Elevate ()
{
	if (IsWin7 ()) {
		CString str = ::GetCommandLine ();

		if (str.Find (_T ("/requireAdministrator")) == -1) {
			SHELLEXECUTEINFO sei = { 0 };
			TCHAR sz [MAX_PATH] = { 0 };

			::GetModuleFileName (NULL, sz, ARRAYSIZE (sz));
			str.Replace ('"' + CString (sz) + '"', _T (""));
			str.Replace (sz, _T (""));
			str += CString (_T (" /requireAdministrator"));

			sei.cbSize = sizeof (sei);
			sei.lpVerb = _T ("runas");
			sei.lpFile = sz;
			sei.lpParameters = str;
			sei.nShow = SW_NORMAL;

			::ShellExecuteEx (&sei);
			TRACEF (sz);
			TRACEF (str);
			TRACEF (_T ("exit (0);"));
			exit (0);

			return false;
		}
	}

	return true;
}

BOOL IsDots(const TCHAR* str) 
{
    if(_tcscmp(str, _T (".")) && _tcscmp(str, _T (".."))) 
		return FALSE;

	return TRUE;
}

BOOL BeginsWith (LPCTSTR lpsz, LPCTSTR lpszEnd, bool bCaseSensitive)
{
	CString str (lpsz);
	CString strEnd (lpszEnd);

	if (!bCaseSensitive) {
		str.MakeLower ();
		strEnd.MakeLower ();
	}

	if (str.GetLength () >= strEnd.GetLength ()) {
		CString strTmp = str.Left (strEnd.GetLength ());
	
		return strTmp == strEnd;
	}

	return FALSE;
}

BOOL EndsWith (LPCTSTR lpsz, LPCTSTR lpszEnd, bool bCaseSensitive)
{
	CString str (lpsz);
	CString strEnd (lpszEnd);

	if (!bCaseSensitive) {
		str.MakeLower ();
		strEnd.MakeLower ();
	}

	if (str.GetLength () >= strEnd.GetLength ()) {
		CString strTmp = str.Right (strEnd.GetLength ());
	
		return strTmp == strEnd;
	}

	return FALSE;
}

BOOL EndsWith (LPCTSTR lpsz, TCHAR c)
{
	BOOL bResult = FALSE;

	int n = _tcslen (lpsz);

	if (n > 0)
		bResult = lpsz [n - 1] == c;

	return bResult;
}

BOOL DeleteDirectory(const TCHAR* sPath) 
{
	if (IsDots (sPath) || _tcslen (sPath) < 3)
		return FALSE;

	if (!(::GetFileAttributes (sPath) & FILE_ATTRIBUTE_DIRECTORY))
		return FALSE;

	if (::GetFileAttributes (sPath) == -1)
		return FALSE;

    HANDLE hFind;  // file handle
    WIN32_FIND_DATA FindFileData;

	TCHAR DirPath[MAX_PATH] = { 0 };
    TCHAR FileName[MAX_PATH] = { 0 };

	ZeroMemory (&FindFileData, sizeof (FindFileData));

    _tcscpy(DirPath, sPath);
    _tcscat(DirPath, EndsWith (DirPath, '\\') ? _T ("*") : _T ("\\*"));    // searching all files
    _tcscpy(FileName, sPath);
    _tcscat(FileName, EndsWith (FileName, '\\') ? _T ("") : _T ("\\"));

	//if (MessageBox (NULL, CString (sPath) + _T ("\n") + CString (DirPath) + _T ("\n") + CString (FileName), 0, MB_YESNO) == IDNO)
	//	exit (0);

    hFind = FindFirstFile(DirPath,&FindFileData); // find the first file
    if(hFind == INVALID_HANDLE_VALUE) return FALSE;
    _tcscpy(DirPath,FileName);
        
	TRACEF (_T ("DeleteDirectory: ") + CString (sPath));

    bool bSearch = true;
    while(bSearch) { // until we finds an entry
        if(FindNextFile(hFind,&FindFileData)) {
            if(IsDots(FindFileData.cFileName)) continue;
            _tcscat(FileName,FindFileData.cFileName);
            if((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {

                // we have found a directory, recurse
                if(!DeleteDirectory(FileName)) { 
                    FindClose(hFind); 
					TRACEF (_T ("**** DeleteDirectory failed: ") + (CString)FileName);
                    return FALSE; // directory couldn't be deleted
                }
                RemoveDirectory(FileName); // remove the empty directory
				TRACEF (_T ("RemoveDirectory: ") + (CString)FileName);
                _tcscpy(FileName,DirPath);
            }
            else {
				CStringArray vSkip;
				CString strFile = FileName;
				bool bSkip = false;

				strFile.MakeLower ();

                if(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_READONLY)
                    _tchmod (FileName, _S_IWRITE); // change read-only file mode

				vSkip.Add (_T ("uninstall.exe"));
				vSkip.Add (_T ("selfdelete.dll"));
				vSkip.Add (_T ("backup"));
				vSkip.Add (_T ("\\logos\\"));

				for (int i = 0; !bSkip && i < vSkip.GetSize (); i++) 
					bSkip = strFile.Find (vSkip [i]) != -1 ? true : false;

				if (bSkip) {
					TRACEF (_T ("skipping: " ) + (CString)FileName);
				}
				else {
					TRACEF (_T ("DeleteFile: ") + (CString)FileName);

					if(!DeleteFile(FileName)) {  // delete the file
						TRACEF (_T ("**** DeleteFile failed: ") + (CString)FileName);
						FindClose(hFind); 
						return FALSE; 
					}                 
				}

				_tcscpy(FileName,DirPath);
            }
        }
        else {
            if(GetLastError() == ERROR_NO_MORE_FILES) // no more files there
            bSearch = false;
            else {
                // some error occured, close the handle and return FALSE
				TRACEF (_T ("*** FindNextFile failed: ") + FormatMessage (::GetLastError ()));
                FindClose(hFind); 
                return FALSE;
            }
        }
    }
	
	if (hFind != INVALID_HANDLE_VALUE)
	    FindClose(hFind);  // closing file handle
 
	TRACEF (_T ("RemoveDirectory: ") + (CString)sPath);
	RemoveDirectory(sPath); // remove the empty directory

	return true;
}

void CreateDir (CString strFullPath)
{
	// this fct is intentionally different than FoxjetDatabase::CreateDir

	int nIndex = 0;
	int nLevel = 0;

	if (strFullPath [strFullPath.GetLength () - 1] != '\\') {
		int nIndex = strFullPath.ReverseFind ('\\');

		if (nIndex != -1) 
			strFullPath = strFullPath.Left (nIndex) + '\\';
	}

	while ((nIndex = strFullPath.Find (_T ("\\"), nIndex)) != -1) {
		CString str = strFullPath.Left (nIndex);

		//TRACEF (str);
		nIndex++;
		nLevel++;

		if (::GetFileAttributes (str) == -1) {
			if (::CreateDirectory (str, NULL)) {
				TRACEF (_T ("CreateDirectory: ") + str);
			}
		}
	}
}

BOOL EnablePrivilege (LPCTSTR lpName)
{
	HANDLE hToken;
	LUID sedebugnameValue;
	TOKEN_PRIVILEGES tp;

	if (!::OpenProcessToken (GetCurrentProcess (), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
		return FALSE;

 

	if (!::LookupPrivilegeValue (NULL, lpName, &sedebugnameValue)) {
		::CloseHandle( hToken );
	
		return FALSE;
	}

	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = sedebugnameValue;
	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	if (!::AdjustTokenPrivileges (hToken, FALSE, &tp, sizeof(tp), NULL, NULL)) {
		::CloseHandle( hToken );

		return FALSE;
	}

	::CloseHandle (hToken);

	return TRUE;

}

FoxjetCommon::CCopyArray <CString, CString &> ToString (const DEVICE_FILE_STRUCT & d, bool bDirectory, bool bFile, const CString & strDir)
{
	FoxjetCommon::CCopyArray <CString, CString &> result;

	if ((bDirectory && !d.m_lSize) || (bFile && d.m_lSize)) {
		CString str;

		str.Format (_T ("%s\\%s"), strDir, d.m_strName);
		result.Add (str);
	}

	for (int i = 0; i < d.m_v.GetSize (); i++) 
		result.Append (ToString (d.m_v [i], bDirectory, bFile, strDir + _T ("\\") + d.m_strName));

	return result;
}

CString GetID (const DEVICE_FILE_STRUCT & d, const CString & strName, const CString & strDir)
{
	if (!strName.CompareNoCase (strDir + '/' + d.m_strName))
		return d.m_strID;

	for (int i = 0; i < d.m_v.GetSize (); i++) {
		CString str = GetID (d.m_v [i], strName, strDir + '/' + d.m_strName);

		if (str.GetLength ())
			return str;
	}

	return _T ("");
}



void GetClientInformation(IPortableDeviceValues** clientInformation)
{
    // Client information is optional.  The client can choose to identify itself, or
    // to remain unknown to the driver.  It is beneficial to identify yourself because
    // drivers may be able to optimize their behavior for known clients. (e.g. An
    // IHV may want their bundled driver to perform differently when connected to their
    // bundled software.)

    // CoCreate an IPortableDeviceValues interface to hold the client information.
    //<SnippetDeviceEnum7>
    HRESULT hr = CoCreateInstance(CLSID_PortableDeviceValues,
                                  nullptr,
                                  CLSCTX_INPROC_SERVER,
                                  IID_PPV_ARGS(clientInformation));
    //</SnippetDeviceEnum7>
    //<SnippetDeviceEnum8>
    if (SUCCEEDED(hr))
    {
        // Attempt to set all bits of client information
        hr = (*clientInformation)->SetStringValue(WPD_CLIENT_NAME, _T ("Box Writer"));
        if (FAILED(hr))
        {
            TRACEF (_T ("! Failed to set WPD_CLIENT_NAME, hr = ") + FormatMessage (hr));
        }

        hr = (*clientInformation)->SetUnsignedIntegerValue(WPD_CLIENT_MAJOR_VERSION, FoxjetCommon::verApp.m_nMajor);
        if (FAILED(hr))
        {
            TRACEF (_T ("! Failed to set WPD_CLIENT_MAJOR_VERSION, hr = ") + FormatMessage (hr));
        }

        hr = (*clientInformation)->SetUnsignedIntegerValue(WPD_CLIENT_MINOR_VERSION, FoxjetCommon::verApp.m_nMinor);
        if (FAILED(hr))
        {
            TRACEF (_T ("! Failed to set WPD_CLIENT_MINOR_VERSION, hr = ") + FormatMessage (hr));
        }

		hr = (*clientInformation)->SetUnsignedIntegerValue(WPD_CLIENT_REVISION, FoxjetCommon::verApp.m_nInternal);
        if (FAILED(hr))
        {
            TRACEF (_T ("! Failed to set WPD_CLIENT_REVISION, hr = ") + FormatMessage (hr));
        }

        //  Some device drivers need to impersonate the caller in order to function correctly.  Since our application does not
        //  need to restrict its identity, specify SECURITY_IMPERSONATION so that we work with all devices.
        hr = (*clientInformation)->SetUnsignedIntegerValue(WPD_CLIENT_SECURITY_QUALITY_OF_SERVICE, SECURITY_IMPERSONATION);
        if (FAILED(hr))
        {
            TRACEF (_T ("! Failed to set WPD_CLIENT_SECURITY_QUALITY_OF_SERVICE, hr = ") + FormatMessage (hr));
        }
    }
    else
    {
        TRACEF (_T ("! Failed to CoCreateInstance CLSID_PortableDeviceValues, hr = ") + FormatMessage (hr));
    }
    //</SnippetDeviceEnum8>
}

void Trace (const DEVICE_FILE_STRUCT & dir, int nCalls)
{
	CString str;

	str.Format (_T ("%s[%s] %s (%d bytes)"), CString (' ', nCalls * 4), dir.m_strID, dir.m_strName, dir.m_lSize);
	TRACEF (str);

	for (int i = 0; i < dir.m_v.GetSize (); i++)
		Trace (dir.m_v [i], nCalls + 1);
}

DEVICE_FILE_STRUCT GetFiles (const DEVICE_FILE_STRUCT & dir, const CString & strExt, const CString & strDir)
{
	DEVICE_FILE_STRUCT result;

	if (dir.m_strName.GetLength () > strExt.GetLength ()) {
		CString str = dir.m_strName.Right (strExt.GetLength ());

		if (!str.CompareNoCase (strExt))
			result.m_v.Add (DEVICE_FILE_STRUCT (dir));
	}

	for (int i = 0; i < dir.m_v.GetSize (); i++) {
		DEVICE_FILE_STRUCT d = GetFiles (dir.m_v [i], strExt, strDir + _T ("/") + dir.m_strName);

		if (d.m_v.GetSize ())
			result.m_v.Append (d.m_v);
	}

	return result;
}

HRESULT GetProperties(IPortableDeviceProperties* pProperties, LPCWSTR pwszObjectID, DEVICE_FILE_STRUCT & file)
{
    HRESULT hr = S_OK;
    // Specify the properties we are interested in spPropertyKeys
    CComPtr<IPortableDeviceKeyCollection> spPropertyKeys;

    if (hr == S_OK)

    {
        hr = CoCreateInstance(CLSID_PortableDeviceKeyCollection,
                              NULL,
                              CLSCTX_INPROC_SERVER,
                              IID_IPortableDeviceKeyCollection,
                              (VOID**)&spPropertyKeys);
    }

    if (hr == S_OK)
    {
        hr = spPropertyKeys->Add(WPD_OBJECT_NAME);
    }

    if (hr == S_OK)
    {
        hr = spPropertyKeys->Add(WPD_OBJECT_SIZE);
    }

    spPropertyKeys->Add (WPD_OBJECT_ORIGINAL_FILE_NAME);
    //spPropertyKeys->Add (WPD_OBJECT_DATE_CREATED);
	spPropertyKeys->Add (WPD_OBJECT_DATE_MODIFIED);

    // Use the GetValues API to get the desired values
    CComPtr<IPortableDeviceValues> spPropertyValues;

    if (hr == S_OK)
    {
        hr = pProperties->GetValues(pwszObjectID, spPropertyKeys, &spPropertyValues);
        // GetValues may return S_FALSE if one or more properties could not be retrieved

        if (hr == S_FALSE)
        {
            hr = S_OK;
        }
    }

    // Get value of each requested property
    LPWSTR pwszName = NULL, pwszOriginalFileName = NULL;

    if (hr == S_OK)
    {
        hr = spPropertyValues->GetStringValue(WPD_OBJECT_NAME, &pwszName);
    }

    ULONGLONG ullSize = 0;

    if (hr == S_OK)
    {
        hr = spPropertyValues->GetUnsignedLargeIntegerValue(WPD_OBJECT_SIZE, &ullSize);
        // WPD_OBJECT_SIZE may not be supported some objects

        if (FAILED(hr))
        {
            hr = S_OK;
        }
    }

	spPropertyValues->GetStringValue(WPD_OBJECT_ORIGINAL_FILE_NAME, &pwszOriginalFileName);

    // Display object properties
	if (hr == S_OK)
    {
		SYSTEMTIME tm = { 0 };
		PROPVARIANT pvdt = { 0 };
		
		PropVariantInit(&pvdt);
		pvdt.vt = VT_DATE;
		HRESULT hr = spPropertyValues->GetValue (WPD_OBJECT_DATE_MODIFIED, &pvdt);
		VariantTimeToSystemTime (pvdt.date, &tm);
		TRACEF (CTime (tm).Format (_T ("%c")));
		PropVariantClear(&pvdt);

		file.m_strID = pwszObjectID;
		file.m_strName = pwszOriginalFileName ? pwszOriginalFileName : pwszName;
		file.m_lSize = ullSize;
		file.m_tm = CTime (tm);
    }

    // Free any memory allocated by GetStringValue
    if (pwszName != NULL)
    {
        CoTaskMemFree(pwszName);
    }

	if (pwszOriginalFileName)
		CoTaskMemFree (pwszOriginalFileName);

    return hr;
}

HRESULT Enumerate (IPortableDeviceContent* pContent, LPCWSTR pwszParentObjectId, DEVICE_FILE_STRUCT & dir, int nDepth)
{
    HRESULT hr = S_OK;
    // Display properties of supplied object
    CComPtr<IPortableDeviceProperties> spProperties;
    hr = pContent->Properties(&spProperties);

    if (hr == S_OK)
    {
        hr = GetProperties(spProperties, pwszParentObjectId, dir);
    }

    // Enumerate children (if any) of provided object
    CComPtr <IEnumPortableDeviceObjectIDs> spEnum;

    if (hr == S_OK)
    {
        hr = pContent->EnumObjects(0, pwszParentObjectId, NULL, &spEnum);
    }

    while (hr == S_OK)
    {
        // We�ll enumerate one object at a time, but be aware that an array can
        // be supplied to the Next API to optimize enumeration
        LPWSTR pwszObjectId = NULL;
        ULONG celtFetched = 0;

        hr = spEnum->Next(1, &pwszObjectId, &celtFetched);

		//TRACER (FoxjetDatabase::ToString ((int)celtFetched)  + _T (": ") + (CString)pwszObjectId);

        // Try enumerating children of this object
        if (hr == S_OK)
        {
			if (nDepth >= 0) {
				DEVICE_FILE_STRUCT d;
				hr = Enumerate(pContent, pwszObjectId, d, nDepth - 1);
				dir.m_v.Add (d);
			}
        }
    }

    // Once no more children are available, S_FALSE is returned which we promote to S_OK
    if (hr == S_FALSE)
    {
        hr = S_OK;
    }

    return hr;
}

CComPtr<IPortableDevice> OpenDevice (const CString & strDevicePath)
{
	bool bResult = false;
	HRESULT hr = S_OK;
	CComPtr<IPortableDevice>			device;
	CComPtr<IPortableDeviceValues>		clientInformation;
	CComPtr<IPortableDeviceContent>     content;

    GetClientInformation(&clientInformation);

	if (SUCCEEDED (CoCreateInstance (CLSID_PortableDeviceFTM, nullptr, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&device)))) {
		hr = device->Open (strDevicePath, clientInformation);
		                        
		if (hr == E_ACCESSDENIED) {
			TRACEF (_T ("E_ACCESSDENIED"));
			return NULL;
		}
		else if (FAILED (hr)) {
			TRACEF (_T ("FAILED"));
			return NULL;
		}
		else 
			return device;
	}
}

DWORD GetFileSize (const CString & strFile)
{
	DWORD dw = 0;

    if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
		fseek (fp, 0, SEEK_END);
		dw = ftell (fp);
		fclose (fp);
	}

	return dw;
}

static CString FormatFilename (CString strFile)
{
	int nIndex = strFile.ReverseFind ('\\');

	if (nIndex != -1) {
		const CString strOldTitle = strFile.Mid (nIndex + 1);
		CString strNewTitle (strOldTitle);
		TCHAR c [] = { '\\', '/', ':', '*', '?', '\"', '<', '>', };

		for (int i = 0; i < ARRAYSIZE (c); i++) { 
			CString str;

			str.Format (_T ("&#%02d;"), (int)c [i]);
			strNewTitle.Replace (CString (c [i]), str);
		}

		strFile.Replace (strOldTitle, strNewTitle);
	}

	return strFile;
}

CString ReadDirect (CString strFile, const CString & strDefault)
{
	strFile = FormatFilename (strFile);
	DWORD dwSize = GetFileSize (strFile);
	CString strData = strDefault;

	if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
		const BYTE nUTF16 [] = { 0xFF, 0xFE };
		BYTE * p = new BYTE [dwSize + 1];

		memset (p, 0, dwSize + 1);
		fread (p, dwSize, 1, fp);
		fclose (fp);

		if (!memcmp (p, nUTF16, ARRAYSIZE (nUTF16))) {
			TCHAR * pData = (TCHAR *)p;
			strData = ++pData;
		}
		else {
			for (int i = 0; i < dwSize; i++)
				strData += (TCHAR)p [i];
		}

		delete [] p;
	}

	return strData;
}

void TraceSQLInstallerError (LPCTSTR lpszFile, ULONG lLine)
{
	DWORD dw = 0;
	TCHAR sz [256] = { 0 };  

	SQLInstallerError (1, &dw, sz, ARRAYSIZE (sz), NULL);

	//#ifdef _DEBUG
	//CDebug::Trace (sz, true, lpszFile, lLine);
	//#else
	CString str;

	str.Format (_T ("%s(%d): %s\n"), lpszFile, lLine, sz);
	::OutputDebugString (str);
	//#endif
}
