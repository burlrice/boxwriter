#ifndef __PARSE_H__
#define __PARSE_H__

#include <afxcoll.h>
#include "CopyArray.h"

namespace FoxjetDatabase
{
	CString ToString (ULONG lVal); 
	CString ToString (int nVal); 
	CString ToString (bool bVal); 
	CString ToString (double dVal); 
	CString ToString (__int64 iVal); 

	CString Tokenize (const CString &strInput, CStringArray &v, TCHAR cDelim = ',', bool bTrim = true);
	FoxjetCommon::CLongArray CountChars (const CString & str, TCHAR c);
	CString TokenizeCmdLine (const CString &strInput, CStringArray &v);

	CString FormatString(const CString &str);
	CString UnformatString(const CString &str);
	CString Trim(CString str);
	CString ToString (const CStringArray & v);
	int FromString (const CString & strInput, CStringArray & v);
	
	CString GetKey (const CString & strPacket);
	void ParsePackets (const CString & str, CStringArray & v);
	void ExtractPackets (const CString & strKey, const CString & str, CStringArray & v);
	CString SetPackets (const CString & strPacket, const CString & strData);
	CString GetParam (const CStringArray & v, int nIndex, const CString & strDef = _T (""));
	
	CString Extract (const CString & strParse, const CString & strBegin);
	CString Extract (const CString & strParse, const CString & strBegin, const CString & strEnd);
}; //namespace FoxjetDatabase


#endif //__PARSE_H__
