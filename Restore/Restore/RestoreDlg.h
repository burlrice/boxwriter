
// RestoreDlg.h : header file
//

#pragma once

#include "RoundButtons.h"
#include "ListCtrlImp.h"

// CRestoreDlg dialog
class CRestoreDlg : public CDialogEx
{
	class CItem : public ItiLibrary::ListCtrlImp::CItem 
	{
	public:
		CItem (const CString & strName = _T (""), const CTime & tmModified = CTime::GetCurrentTime (), const CString & strID = _T (""));
		virtual ~CItem ();

		virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
		virtual CString GetDispText (int nColumn) const;

		CString m_strName;
		CString m_strID;

	protected:
		CTime * m_ptmModified;
	};
// Construction
public:
	CRestoreDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_RESTORE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	static CString GetTempPath ();
	static bool Extract (UINT nID, const CString & strPath, const CString & strType = _T ("EXE"), const CString & strModule = _T ("Restore.exe"));

// Implementation
protected:
	HICON m_hIcon;
	FoxjetUtils::CRoundButtons m_buttons;
	void EnumDrives (const CString & strSelect = _T (""));
	CString GetCurSelDir () const;
	void EnumFiles (const CString & strDir = _T (""));
	bool CRestoreDlg::DoRestore (const CString & strZipSrc, const CString & strZipDest);

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
	CDeviceList m_vPortableDevices;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg LRESULT OnDeviceChange (WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnSelchangeCbDrives();
	afx_msg void OnBnClickedRestore();
};
