
// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions


#include <afxdisp.h>        // MFC Automation classes



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC support for ribbons and control bars

#include <afxdb.h>
#include <Dbt.h>
#include <PortableDeviceApi.h>
#include <PortableDevice.h>
#include <setupapi.h>
#include <initguid.h>
#include "TemplExt.h"
#include "CopyArray.h"


void Trace (LPCTSTR psz, LPCTSTR pszFile, ULONG nLine);
#define TRACEF(s) Trace ((s), _T (__FILE__), __LINE__)

typedef struct tagDEVICE_FILE_STRUCT
{
	tagDEVICE_FILE_STRUCT () : m_tm (1999, 1, 1, 0, 0, 0) { }

	CString m_strID;
	CString m_strName;
	ULONG m_lSize;
	CTime m_tm;
	FoxjetCommon::CCopyArray <tagDEVICE_FILE_STRUCT, tagDEVICE_FILE_STRUCT &> m_v;
} DEVICE_FILE_STRUCT;

// This is the GUID for the USB device class
DEFINE_GUID(GUID_DEVINTERFACE_USB_DEVICE,
			0xA5DCBF10L, 0x6530, 0x11D2, 0x90, 0x1F, 0x00, 0xC0, 0x4F, 0xB9, 0x51, 0xED);
// (A5DCBF10-6530-11D2-901F-00C04FB951ED)

CString Extract (const CString & strParse, const CString & strBegin);
CString Extract (const CString & strParse, const CString & strBegin, const CString & strEnd);


class CDevice
{
public:
	CDevice ();
	CDevice (const CDevice & rhs);
	CDevice (const SP_DEVINFO_DATA * pDev, const SP_DEVICE_INTERFACE_DATA * pInterface, const SP_DEVICE_INTERFACE_DETAIL_DATA_W * pInterfaceDetail);
	CDevice & operator = (const CDevice & rhs);
	virtual ~CDevice ();

	friend bool operator == (const CDevice & lhs, const CDevice & rhs) { return !_tcscmp (lhs.m_interfaceDetail.DevicePath, rhs.m_interfaceDetail.DevicePath); }
	friend bool operator != (const CDevice & lhs, const CDevice & rhs) { return !(lhs == rhs); }

	SP_DEVINFO_DATA						m_dev;
	SP_DEVICE_INTERFACE_DATA			m_interface;
	SP_DEVICE_INTERFACE_DETAIL_DATA_W	m_interfaceDetail;
	TCHAR								m_szDevicePath [MAX_PATH];
	
	CString m_strFriendlyName;
	CString m_strDeviceDescription;
	CString m_strLocationInformation;

	WORD GetVID () const { return _tcstoul (Extract (m_interfaceDetail.DevicePath, _T ("vid_"), _T ("&")), NULL, 16); } 
	WORD GetPID () const { return _tcstoul (Extract (m_interfaceDetail.DevicePath, _T ("pid_"), _T ("#")), NULL, 16); } 
};

typedef struct FoxjetCommon::CCopyArray <CDevice, CDevice &> CDeviceList;


int Find (const CDeviceList & v, const CDevice & find);
CDeviceList EnumDevices (const GUID & guid = GUID_DEVINTERFACE_WPD);
FoxjetCommon::CCopyArray <CString, CString &> ToString (const DEVICE_FILE_STRUCT & d, bool bDirectory = true, bool bFile = true, const CString & strDir = _T ("\\"));
CString GetID (const DEVICE_FILE_STRUCT & d, const CString & strName, const CString & strDir = _T ("/"));
CComPtr<IPortableDevice> OpenDevice (const CString & strDevicePath);
HRESULT Enumerate (IPortableDeviceContent* pContent, LPCWSTR pwszParentObjectId, DEVICE_FILE_STRUCT & dir, int nDepth);
void Trace (const DEVICE_FILE_STRUCT & dir, int nCalls = 0);
bool Delete (CComPtr<IPortableDevice> & device, const CString & strID);
HRESULT GetProperties(IPortableDeviceProperties* pProperties, LPCWSTR pwszObjectID, DEVICE_FILE_STRUCT & file);
void GetClientInformation(IPortableDeviceValues** clientInformation);
DEVICE_FILE_STRUCT GetFiles (const DEVICE_FILE_STRUCT & root, const CString & strExt, const CString & strDir = _T ("/"));
CString ReadDirect (CString strFile, const CString & strDefault = _T (""));
DWORD GetFileSize (const CString & strFile);

int GetFiles (const CString & strDir, const CString & strFile, CStringArray & v);
bool IsRunning (LPCTSTR lpsz);
CString LoadString (UINT nID);
CString GetHomeDir ();
CString execute (const CString & str);
bool Elevate ();
bool IsWin7 ();
BOOL DeleteDirectory(const TCHAR* sPath); 
BOOL EndsWith (LPCTSTR lpsz, LPCTSTR lpszEnd, bool bCaseSensitive = false);
BOOL BeginsWith (LPCTSTR lpsz, LPCTSTR lpszEnd, bool bCaseSensitive = false);
void CreateDir (CString strFullPath);
BOOL EnablePrivilege (LPCTSTR lpName);

inline CString FormatMessage (DWORD dwError)
{
	LPVOID lpMsgBuf = NULL;
	CString str;

	str.Format (_T ("[%d: 0x%p] "), dwError, dwError);

	::FormatMessage (
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, dwError, MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT), 
		(LPTSTR)&lpMsgBuf, 0, NULL);
	str += (LPCTSTR)lpMsgBuf;
	::LocalFree( lpMsgBuf );

	return str;
}


void TraceSQLInstallerError (LPCTSTR lpszFile, ULONG lLine);

#define TRACE_SQLERROR()				TraceSQLInstallerError (_T (__FILE__), __LINE__)











