#include "iv.h"

void main( void ) {
    PrintheadInterfaceInit(BASE_ADDR);
    outportb(BASE_ADDR+2, (1<<IRQ_FLAG)); /* clear irq flag */
    for(;;) {
        if (inportb(BASE_ADDR+READ_IO) & (1<<IRQ_FLAG)) { /* poll irq flag */
            outportb(BASE_ADDR+CMD, (1<<IRQ_FLAG)); /* clear irq flag */
            CardIrq(); /* execute interrupt code */
        }
    }
}
