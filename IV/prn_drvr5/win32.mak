TARGET=test
OBJ=$(TARGET).o iv.o image.o iv_fpga.o
CC=gcc
CFLAGS= -g -Wall

$(TARGET).exe: $(OBJ)
	$(CC) $(CFLAGS) -o $(TARGET) $(OBJ)


test.o: test.c iv.h
iv.o: iv.c iv.h
image.o: image.c
iv_fpga.o: iv_fpga.c
clean:
	$(RM) $(OBJ)