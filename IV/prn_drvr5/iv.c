#include "iv.h"

#ifdef WIN32
int usleep (unsigned int us) {
  static LARGE_INTEGER freq;
  static int initted = 0;
  LARGE_INTEGER s, e, d;
  if (!initted) {
    QueryPerformanceFrequency(&freq);
    initted = 1;
  }
  QueryPerformanceCounter(&s);
  d.QuadPart = freq.QuadPart * ((double)us / 1000000.0);
  do {
    QueryPerformanceCounter(&e);
  } while (e.QuadPart - s.QuadPart < d.QuadPart);
  return 0;
}
#endif

// masks of register INT_REG_DOWNLOAD
#define DONE_MASK          (0x04)
#define INIT_MASK          (0x10)

unsigned long Position = 0;
unsigned char Encoder = INTERNAL;
unsigned char ContinuousPrint = 1;
extern unsigned char * getIVFPGAptr(void);
extern const unsigned char iv_fpga[];
extern const unsigned char iv_fpga_end[];
unsigned short *imagePtr;

enum {RIGHT_TO_LEFT=0, LEFT_TO_RIGHT};
char printDir = LEFT_TO_RIGHT;
int wordDir;
int wordsPerCol;
int wordsToNextCol;
unsigned short shiftcntl1;
unsigned short shiftcntl2;

enum {NONE=0, IV_9DOT, IV_18DOT};
#define TICKS_IV (19)
struct printheadStruct {
  unsigned char type;
  unsigned char direction;
  unsigned short offset;
  unsigned char orfice_spacing;
};

#define MAX_HEADS 8
struct printheadStruct printhead[MAX_HEADS] =
{
 {IV_18DOT,LEFT_TO_RIGHT,0,TICKS_IV},
 {IV_18DOT,LEFT_TO_RIGHT,0,TICKS_IV},
 {IV_18DOT,LEFT_TO_RIGHT,0,TICKS_IV},
 {IV_18DOT,LEFT_TO_RIGHT,0,TICKS_IV},
 {NONE,LEFT_TO_RIGHT,0,TICKS_IV},
 {NONE,LEFT_TO_RIGHT,0,TICKS_IV},
 {NONE,LEFT_TO_RIGHT,0,TICKS_IV},
 {NONE,LEFT_TO_RIGHT,0,TICKS_IV}
};

void flip_bytes( void *pv, int len ) {
  char *p = (char *)pv;
  int i, j;
  char tmp;
  for (i=0, j=len-1; i<j; i++, j--) {
    tmp = p[i];
    p[i] = p[j];
    p[j] = tmp;
  }
}

extern unsigned char * getImagePtr(void);
void CardIrq (void) {
    short temp;
    short bpp;
    volatile char wordCnt;
    unsigned short* colData;
    if (Position) { /* send column */
        Position--;
        outport(BASE_ADDR+SHIFT_CNTL, shiftcntl1);
        outport(BASE_ADDR+SHIFT_CNTL+2, shiftcntl2);
        wordCnt = wordsPerCol;
        colData = imagePtr; /* get pointer to column */
        while (2 < wordCnt) {
            wordCnt-=2;
            outport(BASE_ADDR+DATA_PORT, *(colData));
            colData+=wordDir; /* move pointer to next word in column */
            outport(BASE_ADDR+DATA_PORT+2, *(colData));
            colData+=wordDir; /* move pointer to next word in column */
            #ifdef WIN32
            usleep(2); /* fpga needs some time to shift data */
            #endif
        }
        outport(BASE_ADDR+DATA_PORT, *(colData)); /* last word in column */
        colData+=wordDir; /* move pointer to next word in column */
        outport(BASE_ADDR+DATA_PORT+2, *(colData));
        #ifdef WIN32
        usleep(2); /* fpga needs some time to shift data */
        #endif
        #ifdef LITTLE_ENDIAN
        outport(BASE_ADDR+SHIFT_CNTL, 0xFF01); /* dummy start dot is 511 */
        outport(BASE_ADDR+SHIFT_CNTL+2, 0x0100); /* dummy height is 1 */
        #else
        outport(BASE_ADDR+SHIFT_CNTL, 0x01FF); /* dummy start dot is 511 */
        outport(BASE_ADDR+SHIFT_CNTL+2, 0x0001); /* dummy height is 1 */
        #endif
        if (Position) {
            imagePtr+=wordsToNextCol; /* move image pointer to next column */
            return; /* exit and wait for next column interrupt */
        } else if (!ContinuousPrint) {
            outportb(BASE_ADDR+PRINT_STATUS, (inportb(BASE_ADDR+PRINT_STATUS)&(~(1<<IRQ_SEL)))|(PHOTO_IRQ<<IRQ_SEL));
            return; /* exit and wait for next photocell interrupt */
        }
    }
    if ((COLUMN_IRQ<<IRQ_SEL) == (inportb(BASE_ADDR+PRINT_STATUS)&(1<<IRQ_SEL))) {
        #ifdef LITTLE_ENDIAN
        outport(BASE_ADDR+SHIFT_CNTL, 0xFF01); /* dummy start dot is 511 */
        outport(BASE_ADDR+SHIFT_CNTL+2, 0x0100); /* dummy height is 1 */
        #else
        outport(BASE_ADDR+SHIFT_CNTL, 0x01FF); /* dummy start dot is 511 */
        outport(BASE_ADDR+SHIFT_CNTL+2, 0x0001); /* dummy height is 1 */
        #endif
    }
    /* refresh image */
    if (inportb(BASE_ADDR+READ_IO)&(1<<PRODUCT_DETECT)) {
        /* get image type from bmp header */
        temp = *((unsigned short *)getImagePtr());
        #ifndef LITTLE_ENDIAN
        flip_bytes(&temp,sizeof(temp));
        #endif
        /* get bits per pixel from bmp header */
        bpp = *((unsigned short *)(getImagePtr() + 26));
        #ifndef LITTLE_ENDIAN
        flip_bytes(&bpp,sizeof(bpp));
        #endif
        if((0x4D42==temp) && (1==bpp)){
            wordDir = 1;
            /* get image height from bmp header */
            temp = *((unsigned short *)(getImagePtr() + 18));
            #ifndef LITTLE_ENDIAN
            flip_bytes(&temp,sizeof(temp));
            #endif
            shiftcntl1 = (0<<VFLIP); /* start dot = 0 */
            #ifdef LITTLE_ENDIAN
            flip_bytes(&shiftcntl1,sizeof(shiftcntl1));
            #endif
            shiftcntl2 = (1<<BMP)|(0<<DRAFT)|temp; /* bmp on, draft off, height */
            #ifdef LITTLE_ENDIAN
            flip_bytes(&shiftcntl2,sizeof(shiftcntl2));
            #endif
            wordsPerCol = ((temp-1)/16)+1;
            if(0 != (wordsPerCol%2)){
                wordsToNextCol = wordsPerCol/2;
                wordsToNextCol = (wordsToNextCol+1)*2;
                wordsPerCol = wordsToNextCol;
            }else{
                wordsToNextCol = wordsPerCol;
            }
            /* get image length from bmp header */
            temp = *((unsigned short *)(getImagePtr() + 22));
            #ifndef LITTLE_ENDIAN
            flip_bytes(&temp,sizeof(temp));
            #endif
            Position = temp; /* set Position to length of image (cols) */
            temp = *((unsigned short *)(getImagePtr() + 10));
            #ifndef LITTLE_ENDIAN
            flip_bytes(&temp,sizeof(temp));
            #endif
            imagePtr = (unsigned short *)(getImagePtr() + temp);
            if (LEFT_TO_RIGHT == printDir) {
                imagePtr += ((Position-1) * wordsPerCol);
            }
            wordsToNextCol *= -1;
        }
        /* set DPI of image */
        outportb(BASE_ADDR+DPI_IRQ, 5); /* 100dpi/5 = 20dpi */
        /* irq = column */
        outportb(BASE_ADDR+PRINT_STATUS, (inportb(BASE_ADDR+PRINT_STATUS)&(~(1<<IRQ_SEL)))|(COLUMN_IRQ<<IRQ_SEL));
        return; /* exit and wait for next column interrupt */
    }
}

void setPHOffset(unsigned short BaseAddr, int ndx, unsigned short val)  {
    flip_bytes(&val, sizeof(val));
    outportb(BaseAddr+CMD, 0x01); /* Page 1 */
    #ifdef LITTLE_ENDIAN
    flip_bytes(&val,sizeof(val));
    #endif
    outport(BaseAddr+PH_OFFSETS+(2 * ndx), val);
    outportb(BaseAddr+CMD, 0x00); /* Page 0 */
}

void StorePHConfig(struct printheadStruct printhead[], unsigned short BaseAddr) {
    unsigned short type = 0;
    int size=0, sector=0;
    int i = 0;
    for (i=0; i < MAX_HEADS; i++) {
        switch (printhead[i].type) {
            case IV_9DOT:
                if(LEFT_TO_RIGHT == printhead[i].direction) {
                    type = (type << 1) | 0x0001;
                    setPHOffset(BaseAddr, sector++, printhead[i].offset + printhead[i].orfice_spacing);
                } else {
                    type = (type << 1) | 0x0000;
                    setPHOffset(BaseAddr, sector++, printhead[i].offset);
                }
                size += 1;
                break;
            case IV_18DOT:
                if(LEFT_TO_RIGHT == printhead[i].direction) {
                    type = (type << 1) | 0x0001;
                    setPHOffset(BaseAddr, sector++, printhead[i].offset + printhead[i].orfice_spacing);
                    type = (type << 1) | 0x0000;
                    setPHOffset(BaseAddr, sector++, printhead[i].offset);
                } else {
                    type = (type << 1) | 0x0000;
                    setPHOffset(BaseAddr, sector++, printhead[i].offset);
                    type = (type << 1) | 0x0001;
                    setPHOffset(BaseAddr, sector++, printhead[i].offset + printhead[i].orfice_spacing);
                }
                size += 2;
                break;
        }
    }
    if (8 > size) type = type << (8 - size);
    #ifdef LITTLE_ENDIAN
    flip_bytes(&type,sizeof(type));
    #endif
    outport(BaseAddr+HEAD_TYPES, type);
}

void setLineSpeed(unsigned short BaseAddr, int val) {
        unsigned long scalar = 0;
        unsigned short RegisterVal;
        if (val != 0) {
//            scalar = 16000000/(val*20*6); /* generate 600 dpi fixed encoder */
            scalar = 16000000/(650*20*6); /* generate 600 dpi fixed encoder */
        }
        RegisterVal=scalar&0x0000FFFF;
        #ifndef LITTLE_ENDIAN
        flip_bytes(&RegisterVal, sizeof(RegisterVal));
        #endif
        outport(BaseAddr+LINE_SPEED, RegisterVal);
        RegisterVal=inport(BaseAddr + LINE_SPEED);
        #ifndef LITTLE_ENDIAN
        flip_bytes(&RegisterVal, sizeof(RegisterVal));
        #endif
        
        RegisterVal=scalar>>16;
        #ifndef LITTLE_ENDIAN
        flip_bytes(&RegisterVal, sizeof(RegisterVal));
        #endif
        outport(BaseAddr+LINE_SPEED+2, RegisterVal);
        RegisterVal=inport(BaseAddr+LINE_SPEED+2);
        #ifndef LITTLE_ENDIAN
        flip_bytes(&RegisterVal, sizeof(RegisterVal));
        #endif
}

#if 1
int getLineSpeed(unsigned short BaseAddr) {
    unsigned long scalar;
    unsigned short RegisterVal;
    RegisterVal=inport(BaseAddr + LINE_SPEED);
    #ifndef LITTLE_ENDIAN
    flip_bytes(&RegisterVal, sizeof(RegisterVal));
    #endif
    scalar=RegisterVal;
    RegisterVal=inport(BaseAddr + LINE_SPEED+2);
    #ifndef LITTLE_ENDIAN
    flip_bytes(&RegisterVal, sizeof(RegisterVal));
    #endif
    RegisterVal&=0x000F;
    scalar += RegisterVal*0x10000;
    scalar = (scalar)*20;
    if (scalar != 0) {
        return 16000000/scalar;
    } else {
        return 0;
    }
}
#endif
///////////////////////////////////////////////////
// Download XILINX code
//////////////////////////////////////////////////
unsigned char binary_download(unsigned short BaseAddr, unsigned char* fpga, unsigned long fpga_size)
{
    int i;
    unsigned char Byte;
    unsigned char* BytePtr;
    /* toggle the program signal */
    outportb(BaseAddr+GAL_REG,0xFF);
    outportb(BaseAddr+GAL_REG,0);
    outportb(BaseAddr+GAL_REG,0xFF);
    #ifdef DELAY
    /* allow FPGA enough time to clear its memory, at least 500uS */
    for ((volatile int)i=0; (volatile int)i<1000; (volatile int)i++) {}
    #else
    /* poll INIT siganl until memory is clear */
    while (0 == (inportb(BaseAddr+GAL_REG) & INIT_MASK));
    #endif
    /* download configuration bytes*/
    BytePtr = fpga;
    for (i=0; i<fpga_size; i++) {
        /* write configuration byte */
        outportb(BaseAddr, *BytePtr);
        BytePtr++;
    }
    /* need 10 more clock cycles, one for each startup sequence */
    for (i=0; i<10; i++) {
        /* write dummy byte */
        outportb(BaseAddr, 0X00);
    }
    Byte = inportb(BaseAddr+GAL_REG);
    Byte &= DONE_MASK;
    return(Byte);
}

////////////////////////////////////////////////////////////////////////////
//
// ClearBuffer
//
////////////////////////////////////////////////////////////////////////////
void ClearBuffer(unsigned short BaseAddr)
{
    int col_cnt;
    unsigned char Dpi;
    unsigned char PrintStatus;
    /* set CW to 1 or 100 dpi */
    Dpi=inportb(BaseAddr+DPI_IRQ);
    outportb(BaseAddr+DPI_IRQ, 1);
    PrintStatus = inportb(BaseAddr+PRINT_STATUS);
    /* pause  */
    outportb(BASE_ADDR+PRINT_STATUS, inportb(BASE_ADDR+PRINT_STATUS)&(~(1<<UNPAUSE)));
    /* irq = column */
    outportb(BASE_ADDR+PRINT_STATUS, (inportb(BASE_ADDR+PRINT_STATUS)&(~(1<<IRQ_SEL)))|(COLUMN_IRQ<<IRQ_SEL));
    /* toggle print reset       */
    outportb(BASE_ADDR+PRINT_STATUS, inportb(BASE_ADDR+PRINT_STATUS)&(~(1<<RESET)));
    outportb(BASE_ADDR+PRINT_STATUS, inportb(BASE_ADDR+PRINT_STATUS)|(1<<RESET));
    outportb(BASE_ADDR+PRINT_STATUS, inportb(BASE_ADDR+PRINT_STATUS)&(~(1<<RESET)));
    // write blank columns to ram data port
    for (col_cnt=0; col_cnt<0x8000; col_cnt++){
        /* inc column counter */
        outportb(BASE_ADDR+PRINT_STATUS, inportb(BASE_ADDR+PRINT_STATUS)|(1<<INC_COL_CNT));
        inportb(BaseAddr+READ_IO); //clear COL_RESET
        outportb(BASE_ADDR+PRINT_STATUS, inportb(BASE_ADDR+PRINT_STATUS)&(~(1<<INC_COL_CNT)));
        #ifdef LITTLE_ENDIAN
        outport(BaseAddr+SHIFT_CNTL, 0xFF01); /* dummy start dot is 511 */
        outport(BaseAddr+SHIFT_CNTL+2, 0x0100); /* dummy height is 1 */
        #else
        outport(BaseAddr+SHIFT_CNTL, 0x01FF); /* dummy start dot is 511 */
        outport(BaseAddr+SHIFT_CNTL+2, 0x0001); /* dummy height is 1 */
        #endif
    }
    outportb(BaseAddr+PRINT_STATUS, PrintStatus);
    outportb(BaseAddr+DPI_IRQ, Dpi);
}

#define TYPEMASK           (0x03)
int PrintheadInterfaceInit(unsigned short BaseAddr) {

    unsigned char* fpga;
    unsigned long fpga_size;
    int i;
    Position = 0;
    ContinuousPrint = 1;
    Encoder = INTERNAL;
    printDir = LEFT_TO_RIGHT;
    for (i=0; i<4; i++) {
        printhead [i].type = IV_18DOT;
        printhead [i].direction = LEFT_TO_RIGHT;
        printhead [i].offset = 200;
        printhead [i].orfice_spacing = TICKS_IV;
    }
    for (i=4; i<MAX_HEADS; i++) {
        printhead [i].type = NONE;
        printhead [i].direction = LEFT_TO_RIGHT;
        printhead [i].offset = 200;
        printhead [i].orfice_spacing = TICKS_IV;
    }

    if ((inportb(BaseAddr+GAL_REG)&TYPEMASK)!=0X03){
        fpga = getIVFPGAptr();
        fpga_size = (iv_fpga_end - iv_fpga);
    } else {
        return -1;
    }
    /* download FPGA configuration */
    if (binary_download(BaseAddr, fpga, fpga_size)) {
        /* Page 0 */
        outportb(BaseAddr+CMD, 0x08);
        /* toggle print reset */
        outportb(BASE_ADDR+PRINT_STATUS, inportb(BASE_ADDR+PRINT_STATUS)&(~(1<<RESET)));
        outportb(BASE_ADDR+PRINT_STATUS, inportb(BASE_ADDR+PRINT_STATUS)|(1<<RESET));
        outportb(BASE_ADDR+PRINT_STATUS, inportb(BASE_ADDR+PRINT_STATUS)&(~(1<<RESET)));
        /* clear image SRAM */
        ClearBuffer(BaseAddr);
        /* printing=pause, encoder=external, irq=DPI, sharing=none */
        if (INTERNAL == Encoder) {
            setLineSpeed(BaseAddr, 200); /* default to 50 ft/min */
            outportb(BASE_ADDR+PRINT_STATUS, (inportb(BASE_ADDR+PRINT_STATUS)&(~(1<<ENCODER_SEL)))|(INTERNAL<<ENCODER_SEL));
        }
        else {
            outportb(BASE_ADDR+PRINT_STATUS, (inportb(BASE_ADDR+PRINT_STATUS)&(~(1<<ENCODER_SEL)))|(EXTERNAL<<ENCODER_SEL));
        }
        /* set printhead types */
        StorePHConfig(printhead, BaseAddr);
        /* set encoder ticks per column */
        outportb(BaseAddr+DIV_ENCODER_BY, 6);
        /* unpause printing */
        outportb(BASE_ADDR+PRINT_STATUS, inportb(BASE_ADDR+PRINT_STATUS)|(1<<UNPAUSE));
        /* irq = photocell */
        outportb(BASE_ADDR+PRINT_STATUS, (inportb(BASE_ADDR+PRINT_STATUS)&(~(1<<IRQ_SEL)))|(PHOTO_IRQ<<IRQ_SEL));
        outportb(BaseAddr+DPI_IRQ, 1);
        #ifdef LITTLE_ENDIAN
        outport(BaseAddr+SHIFT_CNTL, 0xFF01); /* dummy start dot is 511 */
        outport(BaseAddr+SHIFT_CNTL+2, 0x0100); /* dummy height is 1 */
        #else
        outport(BaseAddr+SHIFT_CNTL, 0x01FF); /* dummy start dot is 511 */
        outport(BaseAddr+SHIFT_CNTL+2, 0x0001); /* dummy height is 1 */
        #endif
    } else {
        return -2;
    }
    return 0;
}

// End of file.
