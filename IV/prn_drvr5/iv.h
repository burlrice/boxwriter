//#define MX1
#define BASE_ADDR (0x320)
// I/O register addresses
#define GAL_REG            (0x01)
#define CMD                (0x02)
#define READ_IO            (0x02)
/* Page 0 Registers*/
#define DATA_PORT          (0x04)
#define DIV_ENCODER_BY     (0x08)
#define PRINT_STATUS       (0x0A)
#define DPI_IRQ            (0x0C)
#define SHIFT_CNTL         (0x0E)
#define LINE_SPEED         (0x14)
#define HEAD_TYPES         (0x1A)
/* Page 1 Registers*/
#define PH_OFFSETS         (0x04)

enum {RESET=0, ENCODER_SEL, UNPAUSE, IRQ_SEL, PHOTO_SHARING, ENCODER_SHARING, INC_COL_CNT=7};
enum {EXTERNAL=0, INTERNAL};
enum {COLUMN_IRQ=0, PHOTO_IRQ};
enum {SHARING_OFF=0, SHARING_ON};
/* PRINT_STATUS
 * x x x x  x x x x
 * | | | |  | | | |-> RESET, (active high)
 * | | | |  | | |---> ENCODER_SEL, EXTERNAL(0)/INTERNAL(1)
 * | | | |  | |-----> UNPAUSE, (active high)
 * | | | |  |-------> IRQ_SEL, COLUMN_IRQ(0)/PHOTO_IRQ(1)
 * | | | |----------> PHOTO_SHARING, (active high)
 * | | |------------> ENCODER_SHARING, (active high)
 * | |--------------> RESERVED
 * |----------------> INC_COL_CNT, (active high)
 */

enum {PRODUCT_DETECT=0, IRQ_FLAG};
/* READ_IO
 * x x x x  x x x x
 * | | | |  | | | |-> PRODUCT_DETECT
 * | | | |  | | |---> IRQ flag
 * | | | |  | |-----> reserved
 * | | | |  |-------> reserved
 * | | | |----------> reserved
 * | | |------------> reserved
 * | |--------------> reserved
 * |----------------> reserved
 */

enum {VFLIP=15};
/* SHIFT_CNTL1
 * x x x x  x x x x  x x x x  x x x x
 * | |------------------------------|-> START_DOT
 * |----------------------------------> VFLIP
 */

enum {DRAFT=14, BMP};
/* SHIFT_CNTL2
 * x x x x  x x x x  x x x x  x x x x
 * | | |----------------------------|-> HEIGHT
 * | |--------------------------------> DRAFT
 * |----------------------------------> RLSHIFT
 */

#ifdef WIN32
  #include <windows.h>
  #define LITTLE_ENDIAN
  /* Read in value from port. */
  static unsigned char inportb(unsigned short port) {
    unsigned char t;
    asm volatile ("in %1, %0" : "=a" (t) : "d" (port));
    return t;
  }

  /* Write value to port. */
  static void outportb(unsigned short port, unsigned char value) {
    asm volatile ("out %1, %0" : : "d" (port), "a" (value) );
    return;
  }

  /* Read in value from port. */
  static unsigned short inport(unsigned short port) {
    unsigned char t;
    asm volatile ("in %1, %0" : "=a" (t) : "d" (port));
    return t;
  }

  /* Write value to port. */
  static void outport(unsigned short port, unsigned short value) {
    asm volatile ("out %1, %0" : : "d" (port), "a" (value) );
    return;
 }
#elif defined (MX1)
  #define LITTLE_ENDIAN
  #define BUS_IO_BASE (0x16000000)
  #define inportb(port) (*(volatile char *)(port+BUS_IO_BASE))
  #define outportb(port, data) (*(volatile char *)(port+BUS_IO_BASE)=(data))
  #define inport(port) (*(volatile short *)(port+BUS_IO_BASE))
  #define outport(port, data) (*(volatile short *)(port+BUS_IO_BASE)=(data))

  #ifndef __ASSEMBLY__
  #define __REG(x)	(*((volatile unsigned long *)(x)))
  #define __REG2(x,y)	\
  	( __builtin_constant_p(y) ? (__REG((x) + (y))) \
  	: (*(volatile unsigned long *)((unsigned long)&__REG(x) + (y))) )
  #else
  #define __REG(x) (x)
  #define __REG2(x,y) ((x)+(y))
  #endif
  #define CCM_BASE            0x0021B000
  #define _reg_CCM_CSCR       (*((volatile unsigned long *)(CCM_BASE+0x00)))
  #define _reg_CCM_MPCTL0     (*((volatile unsigned long *)(CCM_BASE+0x04)))
  #define AITC_BASE               0x00223000
  #define _reg_AITC_INTTYPEH      (*((volatile unsigned long *)(AITC_BASE+0x18)))
  #define _reg_AITC_INTTYPEL      (*((volatile unsigned long *)(AITC_BASE+0x1C)))
  #define _reg_CCM_PCDR		(*((volatile unsigned long *)(CCM_BASE+0x20)))

  #define IMX_IO_BASE 0x00200000
  #define IMX_EIM_BASE (0x20000 + IMX_IO_BASE)
  #define CS5U __REG(IMX_EIM_BASE + 0x28) /* Chip Select 5 Upper Register */
  #define CS5L __REG(IMX_EIM_BASE + 0x2c) /* Chip Select 5 Lower Register */
  #define CFG_CS5U_VAL 0x00000400
  #define CFG_CS5L_VAL 0x22220503
#else
  //#define DELAY
  #define BUS_IO_BASE (0xC20000)
  #define inportb(port) (*(volatile char *)(port+BUS_IO_BASE))
  #define outportb(port, data) (*(volatile char *)(port+BUS_IO_BASE)=(data))
  #define inport(port) (*(volatile short *)(port+BUS_IO_BASE))
  #define outport(port, data) (*(volatile short *)(port+BUS_IO_BASE)=(data))
#endif

int PrintheadInterfaceInit(unsigned short BaseAddr);
void CardIrq(void);
