#include "iv.h"

int main( void ) {
    //volatile int speed;
    int i;
    int end_cnt;
    unsigned long dummy;
    #if defined (MX1)
    // set FCLK to 150 MHz, BCLK to 48 MHz
    _reg_CCM_CSCR = 0xAF008403;
    // Set PD=0, MFD=99, MFI=6, MFN=10 150M
    _reg_CCM_MPCTL0 = 0x04632410;
    // Trigger the restart bit(bit 21)
    _reg_CCM_CSCR |= 0x00200000;
    // Program PRESC bit(bit 15) to 0 to divide-by-1
    _reg_CCM_CSCR &= 0xFFFF7FFF;
	__asm__(
    	"mrc p15,0,r0,c1,c0,0\n\t"
    	"mov r2, #0xC0000000\n\t"
    	"orr r0,r2,r0\n\t"
    	"mcr p15,0,r0,c1,c0,0\n\t"
	);
    // all sources selected as normal interrupt
    _reg_AITC_INTTYPEH = 0;
    _reg_AITC_INTTYPEL = 0;
    
    // set PERCLKs
    dummy = _reg_CCM_PCDR;
    dummy &= ~0x000000FF;
    dummy |= 0x00000055;
    _reg_CCM_PCDR = dummy;	
    // PERCLK3 is only used by SSI so the SSI driver can set it any value it likes
    // PERCLK1 and PERCLK2 are shared so DO NOT change it in any other place	
    // all sources selected as normal interrupt

    CS5U = (CFG_CS5U_VAL);
    CS5L = (CFG_CS5L_VAL);
    *(volatile unsigned long *)(0x0021C020) &= 0xFF3FFFFF;
    *(volatile unsigned long *)(0x0021C038) &= 0xFF3FFFFF;
    #endif
    PrintheadInterfaceInit(BASE_ADDR);
    outportb(BASE_ADDR+2, (1<<IRQ_FLAG)); /* clear irq flag */
    dummy = 181;
    end_cnt = 20000;
    for(;;) {
        if (inportb(BASE_ADDR+READ_IO) & (1<<IRQ_FLAG)) { /* poll irq flag */
            if (dummy) {
                dummy--;
            } else {
                dummy = 181;
                if (end_cnt) {
                    end_cnt-=1000;
                } else {
                    end_cnt=20000;
                }
                for ((volatile int)i=0; (volatile int)i<end_cnt; (volatile int)i++) {}
            }
            outportb(BASE_ADDR+CMD, (1<<IRQ_FLAG)); /* clear irq flag */
            CardIrq(); /* execute interrupt code */
        }
        //speed = getLineSpeed(BASE_ADDR);
    }
}
