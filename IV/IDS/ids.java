/*
	example of the command line parameters:

	java ids -server -ip 10.1.2.7 -p 2100 -low -broken

	-server, IDS will listen for status requests (listens on port 1025, also 
	sends reply on port 1025)

	-ip 10.1.2.7, will send status to controller with ip address 10.1.2.7 
	(real IDS will send status to the ip address that requested status)
	
	-p 2100, will send status indicating ink pressure is 21.00 PSI
	
	-low, will send status indicating ink is low (-out, will send status 
	indicating ink is out)
	
	-broken, will send status indicating a broken line
*/

import java.net.*;
/* Packet structure
    xx xx xx xx xx xx xx xx xx
    |  |  |  |  |  |     |-> vacuum 100ths psi
    |  |  |  |  |  |----> pressure 100ths psi
    |  |  |  |  |-------> broken flag
    |  |  |  |----------> ink-low(1)/out(2) flag 
    |  |  |-------------> minor version
    |  |----------------> major version
    |-------------------> number of bytes
*/
 
class ids {

public static void main(String argv[]) {
    int port = 1025, num=1, delay = 0;
    short s = 0;
    byte buf[] = {6, 1, 23, 0, 0, 0, 0, 0, 0};
    String addr = "10.1.2.3";
    boolean bVerbose=false, bServer=false;

    if (argv.length == 0) {
        System.out.print("Usage: java ids");
        System.out.println("\t[-low][-broken][-p ####][-v ####]");
        System.out.println("\t[-out][-ip xx.xx.xx.xx][-v]");
        System.out.println("\t[-server]");
        return;
    }
    for (int i=0; i < argv.length; i++) {
        if (argv[i].equals("-out")) {
            buf[3] = 2;
        } else if (argv[i].equals("-server")) {
            bServer = true;
        } else if (argv[i].equals("-verbose")) {
            bVerbose = true;
        } else if (argv[i].equals("-low")) {
            buf[3] = 1;
        } else if (argv[i].equals("-broken")) {
            buf[4] = 1;
        } else if (argv[i].equals("-p")) {
            if (++i < argv.length) {
                s = (short)Integer.parseInt(argv[i]);
                buf[5] = (byte)(s >> 8 );
                buf[6] = (byte)(s); // >> 8);
            }
        } else if (argv[i].equals("-v")) {
            if (++i < argv.length) {
                s = (short)Integer.parseInt(argv[i]);
                buf[7] = (byte)(s >> 8);
                buf[8] = (byte)(s); // >> 8);
            }
        } else if (argv[i].equals("-num")) {
            if (++i < argv.length) {
                num = Integer.parseInt(argv[i]);
            }
        } else if (argv[i].equals("-loop")) {
            if (++i < argv.length) {
                delay = Integer.parseInt(argv[i]);
            }
        } else if (argv[i].equals("-ip")) {
            if (++i < argv.length) addr = argv[i];
        }
    }
    try {
        DatagramSocket skt;
        DatagramPacket pkt;
        byte[] buffer = new byte[1024];

        pkt = new DatagramPacket(buffer, buffer.length);
        skt = new DatagramSocket(port);
        for (;bServer;) {
            skt.receive(pkt);
            if (bVerbose) System.out.print("srv received from " + 
                pkt.getAddress().getHostName() + ":" + 
                pkt.getPort());

            InetAddress address = InetAddress.getByName(addr);
            for (;;) {
                for (int i=0; i < num; i++) {
                    DatagramPacket packet = 
                        new DatagramPacket(buf, buf.length, address, port);
                    DatagramSocket socket = new DatagramSocket();
                    if (bVerbose) showPacket(buf);
                    else System.out.print(".");
                    socket.send(packet);
                    buf[6]-=5;
                    buf[8]+=5;
                }
                if (0 != delay) {
                    System.out.println("\nWaiting for " + delay + " sec(s)");
                    Thread.sleep(delay);
                }
                else break;
            }
        }
    }catch (Exception e) { e.printStackTrace(); }
}
static void showPacket(byte [] b) {
    short s;
    String buf = "Packet contains:" + b[0] + " bytes\n";
    buf += "Version: " + b[1] + "." + b[2] + "\n";
    if (1 == b[3]) buf += "low ink ";
    else if (2 == b[3]) buf += "out of ink ";
    if (1 == b[4]) buf += "broken line ";
    if ((0 != b[5]) || (0 != b[6])) {
        s = (short)((b[6] << 8) | b[5]);
        buf += "pressure=" + Short.toString(s) + "\n";
    }
    if ((0 != b[7]) || (0 != b[8])) {
        s = (short)((b[8] << 8) | b[7]);
        buf += "vacuum=" + Short.toString(s);
    }
    
    System.out.print(buf);
}

}
