// masks of register INT_REG_DOWNLOAD
#define MASK1DONE          (0x04)
#define CLK1MASK           (0x02)
#define CLK0MASK           (0XFD)
#define DATA_MASK          (0x0F)
#define TYPEMASK           (0x03)
// I/O register addresses
#define GAL_REG            (0x01) // 8 bit
#define CMD                (0x02) // 8 bit	// write only
#define READ_IO            (0x02) // 8 bit	// read only
/* Page 0 Registers*/
#define DATA_PORT          (0x04) // 16 bit
#define DIV_ENCODER_BY     (0x08) // 8 bit
#define PRINT_STATUS_ADDR  (0x0A) // 16 bit
#define DPI_IRQ            (0x0C) // 16 bit
#define SHIFT_CNTL         (0x0E) // 16 bit (2 registers)
#define SPI                (0x12)
#define LS_LINESPEED       (0x14) // 16 bit
#define MS_LINESPEED       (0x16) // 16 bit
#define IDS_IO             (0x18)
#define HEAD_TYPES_ADDR    (0x1A) // 16 bit
#define BYTES_PER_COL      (0x1C) // 8 bit
/* Page 1 Registers*/
#define PH1_OFFSET         (0x04) // 16 bit	// only used in setPHOffset (set to page 1, write val, set to page 0

#define BIT_CLR(x,y) x&=~(1<<y)
#define BIT_SET(x,y) x|=(1<<y)
#define BIT_TST(x,y) x&(1<<y)

enum {REFRESH, SENDCOLUMN};
/*
 * Print Status register
 * x x x x  x x x x
 * | | | |  | | | |-> Reset Print Engine (active high)			// used once in init	
 * | | | |  | | |---> External(0)/Internal(1) Encoder
 * | | | |  | |-----> Hardware Pause (active high)				// prevents buffer pointer from moving (no printing)
 * | | | |  |-------> IRQ Encoder(0)/Photocell(1)				// select for photocell interrupt, or encoder
 * | | | |----------> Photocell Sharing (active high)
 * | | |------------> Encoder Sharing (active high)
 * | |--------------> CIDS Sharing (active low)					// Ink Delivery System (ignore)
 * |----------------> Increment Column Counter (active high)	// only used in ClearBuffer (manual increment, no encoder tick needed)
 *
 * READ_IO
 * x x x x  x x x x
 * | | | |  | | | |-> photocell
 * | | | |  | | |---> ink out (PEL)
 * | | | |  | |-----> waste full (PEL)
 * | | | |  |-------> ink low (PEL)
 * | | | |----------> at temp (PEL)
 * | | |------------> voltage error (I.V. & PEL)
 * | |--------------> vacuum (PEL)
 * |----------------> pump(PEL)/inklow(WAX)
 */
unsigned long Position = 0;
extern unsigned char ExternalEncoder;
extern unsigned short EncoderSpeed;
extern "C" unsigned char * getFPGAptr(void);
int TaskState = REFRESH;

void CardIrq(void) {
    volatile char wordCnt;
    unsigned long* colData;
    if (Position) { // >= 2nd interrupt
        /* send column */
        Position--;
        outport(BaseAddr+SHIFT_CNTL, shiftCntl);
        wordCnt = wordPerCol;
        colData = (unsigned long *)imagePtr; /* get pointer to column */
        while (2 < wordCnt) {
            outport(BaseAddr+DATA_PORT, *(colData));
            colData+=wordDir; /* move pointer to next word in column */
        }
        outport(BaseAddr+DATA_PORT, *(colData)); /* last word in column */
        outport(BaseAddr+SHIFT_CNTL, 0x01FF0001); /* end column */
        imagePtr+=wordPerCol; /* move image pointer to next column */
     } else  {
        /* refresh image */
        if (SENDCOLUMN == TaskState) { // end of image
            /* irq = photocell */
            outport(BaseAddr+PRINT_STATUS_ADDR, inport(BaseAddr+PRINT_STATUS_ADDR) | 0x08);
            /* set dpi to max */
            outport(BaseAddr+DPI_IRQ, 1);
            TaskState = REFRESH;
            return;
        }
        if ((REFRESH == TaskState)// 1st interrupt 
            /* ink ok(D1), at temp(D4), photocell(D0) */
            && (0x13 == (inportb(BaseAddr+READ_IO) & 0x13))
           ) {
            ImagePtr = Image; /* reset image pointer */
            Position = getMessageLength(); /* set Position to length of image (cols) */
            /* set DPI of image */
            outport(BaseAddr+DPI_IRQ, getMessageDpi());
            /* irq = encoder */
            outport(BaseAddr+PRINT_STATUS_ADDR, inport(BaseAddr+PRINT_STATUS_ADDR) & 0xF7);
            TaskState = SENDCOLUMN;
            return;
        }
    }
}

void setPHOffset(unsigned short BaseAddr, int ndx, unsigned short val)  {
    flip_bytes(&val, sizeof(val));
    outportb(BaseAddr + CMD, 0x01); /* Page 1 */
    outport(BaseAddr + PH1_OFFSET + (2 * ndx), val);
    outportb(BaseAddr+CMD, 0x00); /* Page 0 */
}

void StorePHConfig(PrintHead chain[], unsigned short BaseAddr) {
    unsigned short type = 0;
    int size=0, sector=0;
    for (int i=0; i < 16; i++) {
        switch (headSize) {
            case IV_9DOT:
                if(reversed) {
                    type = (type << 1) | 0x0001;
                    setPHOffset(BaseAddr, sector++, chain[i].Offset() + TICKS_IV);
                } else {
                    type = (type << 1) | 0x0000;
                    setPHOffset(BaseAddr, sector++, chain[i].Offset());
                }
                size += 1;
                break;
            case IV_18DOT:
                if(reversed) {
                    type = (type << 1) | 0x0001;
                    setPHOffset(BaseAddr, sector++, chain[i].Offset() + TICKS_IV);
                    type = (type << 1) | 0x0000;
                    setPHOffset(BaseAddr, sector++, chain[i].Offset());
                } else {
                    type = (type << 1) | 0x0000;
                    setPHOffset(BaseAddr, sector++, chain[i].Offset());
                    type = (type << 1) | 0x0001;
                    setPHOffset(BaseAddr, sector++, chain[i].Offset() + TICKS_IV);
                }
                size += 2;
                break;
        }
    }
    if (8 > size) type = type << (8 - size);
    outport(BaseAddr+HEAD_TYPES_ADDR, type);
    //outportb(BaseAddr+BYTES_PER_COL, size*4);//PEL only
}

void PrintTask::setLineSpeed(int val) {
    EncoderSpeed = val;
    unsigned long scalar = 0;
    unsigned short RegisterVal;
    if (val != 0) {
        scalar = 16000000/(val*20*2);
    }
    RegisterVal=scalar&0x0000FFFF;
    flip_bytes(&RegisterVal, sizeof(RegisterVal));
    outport(BaseAddr + LS_LINESPEED, RegisterVal);
    RegisterVal=inport(BaseAddr + LS_LINESPEED);
    flip_bytes(&RegisterVal, sizeof(RegisterVal));

    RegisterVal=scalar>>16;
    flip_bytes(&RegisterVal, sizeof(RegisterVal));
    outport(BaseAddr + MS_LINESPEED, RegisterVal);
    RegisterVal=inport(BaseAddr + MS_LINESPEED);
    flip_bytes(&RegisterVal, sizeof(RegisterVal));
}

int PrintTask::getLineSpeed() {
    unsigned long scalar;
    unsigned short RegisterVal;
    RegisterVal=inport(BaseAddr + LS_LINESPEED);
    flip_bytes(&RegisterVal, sizeof(RegisterVal));
    scalar=RegisterVal;
    RegisterVal=inport(BaseAddr + MS_LINESPEED);
    flip_bytes(&RegisterVal, sizeof(RegisterVal));
    RegisterVal&=0x000F;
    scalar += RegisterVal*0x10000;
    scalar = (scalar)*20*6;
    if (scalar != 0) {
        return 16000000/scalar;
    } else {
        return 0;
    }
}

///////////////////////////////////////////////////
// Download XILINX code
//////////////////////////////////////////////////
unsigned char binary_download(unsigned short BaseAddr, unsigned char* fpga, unsigned long fpga_size)
{
    int byte_cnt;
    unsigned char Byte;
    unsigned char* BytePtr;
    /* toggle the program signal */
    outportb(BaseAddr+GAL_REG,0xFF);
    outportb(BaseAddr+GAL_REG,0);
    outportb(BaseAddr+GAL_REG,0xFF);
    /* allow FPGA enough time to clear its memory */
    /* Delay more than 500 uS */
    Delay500uS();
    /* download configuration bytes*/
    BytePtr = fpga;
    for (byte_cnt=0; byte_cnt<fpga_size; byte_cnt++)
    {
        /* write configuration byte */
        outportb(BaseAddr,*BytePtr);
        BytePtr++;
    }
    /* need 10 more clock cycles, one for each startup sequence */
    for (byte_cnt=0; byte_cnt<10; byte_cnt++)
    {
        /* write dummy byte */
        outportb(BaseAddr,0X00);
    }
    Byte = inportb(BaseAddr+GAL_REG);
    Byte &= MASK1DONE;
    return(Byte);
}

int PrintheadInterfaceInit(unsigned short BaseAddr, unsigned long TpuIRQ1, unsigned long TpuIRQ2) {
    unsigned char RegisterVal;
    int Cnt;
    unsigned char* fpga;
    unsigned long fpga_size;

    unsigned char RegisterVal = inportb(BaseAddr+GAL_REG);
    RegisterVal &= TYPEMASK;
    CardType = RegisterVal;

    if ((RegisterVal & TYPEMASK)!=0X03){
        FILE * in = fopen("5760950.bin", "r");
        if (NULL == in) {
            fpga = getPELFPGAptr();
        } else {
            fpga = ((vfpair *)in)->pos;
        }
        fpga_size = 0x1FBDC;
    } else {
        return -1;
    }
    /* download FPGA configuration */
    if (binary_download(BaseAddr, fpga, fpga_size)) {
        /* Page 0 */
        outportb(BaseAddr+CMD,0x00);
        /* toggle print reset, this will also set TpuIRQ2 interrupt flag */
        outportb(BaseAddr+PRINT_STATUS_ADDR,0x00);
        outportb(BaseAddr+PRINT_STATUS_ADDR,0x01);
        outportb(BaseAddr+PRINT_STATUS_ADDR,0xF0);
        /* clear image SRAM */
        ClearBuffer(BaseAddr);
        /* printing=pause, encoder=external, irq=DPI, sharing=none */
        if (ExternalEncoder) {
            setLineSpeed(EncoderSpeed);
            outportb(BaseAddr+PRINT_STATUS_ADDR,0x02);
        }
        else {
            outportb(BaseAddr+PRINT_STATUS_ADDR,0x00);
        }
        /* set printhead types */
        StorePHConfig(BaseAddr == FPGA_BASE1 ? DaisyChainA : DaisyChainB, BaseAddr);
        /* set encoder ticks per column */
        outportb(BaseAddr+DIV_ENCODER_BY, 6);
        /* unpause printing */
        RegisterVal = inportb(BaseAddr+PRINT_STATUS_ADDR);
        outportb(BaseAddr+PRINT_STATUS_ADDR,RegisterVal|0x04);
        /* get interrupt signal from photocell */
        RegisterVal = inportb(BaseAddr+PRINT_STATUS_ADDR);
        outportb(BaseAddr+PRINT_STATUS_ADDR,RegisterVal|0x08);
        outportb(BaseAddr+DPI_IRQ, 1);
    } else {
        return -2;
    }
    return 0;
}

////////////////////////////////////////////////////////////////////////////
//
// ClearBuffer
//
////////////////////////////////////////////////////////////////////////////
void ClearBuffer(unsigned short BaseAddr)
{
    int col_cnt;
    unsigned char Dpi;
    unsigned char PrintStatus, tmp;
    /* set CW to 1 or 100 dpi */
    Dpi=inportb(BaseAddr+DPI_IRQ);
    outportb(BaseAddr+DPI_IRQ, 1);
    PrintStatus = inportb(BaseAddr+PRINT_STATUS_ADDR);
    tmp = PrintStatus;
    /* pause    */
    tmp &= 0xFB;
    outportb(BaseAddr+PRINT_STATUS_ADDR,tmp);
    /* DPI mode */
    tmp &= 0xF7;
    outportb(BaseAddr+PRINT_STATUS_ADDR,tmp);
    /* toggle print reset       */
    outportb(BaseAddr+PRINT_STATUS_ADDR,tmp&0xFE);
    outportb(BaseAddr+PRINT_STATUS_ADDR,tmp|0x01);
    outportb(BaseAddr+PRINT_STATUS_ADDR,tmp&0xFE);
    // write blank columns to ram data port
    for (col_cnt=0; col_cnt<0x8000; col_cnt++){
        /* inc column counter */
        outportb(BaseAddr+PRINT_STATUS_ADDR,tmp|0x80);
        inportb(BaseAddr+READ_IO); //clear COL_RESET
        outportb(BaseAddr+PRINT_STATUS_ADDR,tmp&0x7F);
        outportb(BaseAddr+PRINT_STATUS_ADDR,tmp&0x7F);//delay
        outportb(BaseAddr+PRINT_STATUS_ADDR,tmp&0x7F);//delay
        outportb(BaseAddr+PRINT_STATUS_ADDR,tmp&0x7F);//delay
        outportb(BaseAddr+PRINT_STATUS_ADDR,tmp&0x7F);//delay
        *(volatile unsigned long *)(BUS_IO_BASE+BaseAddr+SHIFT_CNTL) = 0x01FF0001;
    }
    outportb(BaseAddr+PRINT_STATUS_ADDR,PrintStatus);
    outportb(BaseAddr+DPI_IRQ,Dpi);
}

unsigned int PrintTask::bProdDetect(void) {
    return (inportb(BaseAddr+READ_IO) & 0x01);
}

unsigned int PrintTask::bPELTemp(void) {
    if((inportb(BaseAddr+READ_IO) & 0x10) == 0x10) return 1; // Head not at temp
    return 0; // Head at temp
}

unsigned int PrintTask::bPELWaste(void) {
    if((inportb(BaseAddr+READ_IO) & 0x04) == 0x04) return 0; // Waste bottle OK
    return 1; // Waste bottle full
}

unsigned int PrintTask::PowerError(void) {
    if((inportb(BaseAddr+READ_IO) & 0x20) == 0x20) return 0; // no power error
    return 1;
}

unsigned int PrintTask::bPELVacuumOn(void) {
    if ((isTaskType(PEL_TASK)) && ((inportb(BaseAddr+READ_IO) & 0x40) == 0x40)) return 1; // Vacuum ON
    return 0; // Vacuum OFF
}

// End of file.
