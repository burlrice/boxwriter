///////////////////////////////////////////////////////////////////////////////
// Main program for FxMphc driver
// Copyright (C) 2002 By FoxJet Tech Center
// All rights reserved
//
//	Author: Chris G. Hodge
//	Version History:
//		1.00	Creation
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// General Driver Information
//
// This comment is to help those who need to make small changes to this driver 
// but do not have an intuitive knowledge of how drivers work.  This is not 
// intended to be a tutorial on writing device drivers.  If you understand 
// device drivers then this section will not benifit you as you already know 
// more than I do.
//
// With all of that said here is the ideal. At some point(Now that's definitive)
// the I/O manager gains control of the system and begins loading the drivers. 
// When our driver is loaded the I/O manager calls our DriverEntry routine.  This 
// is where we setup the function pointers for the PnP, Power, I/O Control, 
// ReadFile/WriteFile and etc. handlers.  Next our supplied DriverObject->
// DriverExtension->AddDevice function is called. Once this completes successfuly
// the StartDevice function is called.  Once this has completed successful we are 
// done until PnP or Power managerment system ask us to do someting or a user mode 
// program interacts with us. I am not going to go into the PnP or Power ideal but 
// keep in mind that the PnP manager could ask you to stop processing while it 
// reconfigures you or one of the other devices in the system.  If this occurs then
// you are responsiable for cueing and processing any I/O request packets (IRP's) 
// that you recieve while you are in this suppended state.  If you lose or fail to 
// complete and IRP the system will hang.
//
// Upon a UserMode app calling CreateFile our supplied DispathCreate routine is 
// called.  The user now has two main methods of interfacing with us, these are the 
// Win32 function calls: DeviceIoCtrl and WriteFile/ReadFile.  For DeviceIoCtrl calls
// our supplied DispatchControl function is called at PASSIVE_LEVEL(The level at which 
// a fuction is called in important. Pay attetion to the level that each DDK function 
// is documented to work at. If you hack this stuff enough you will learn that some 
// of the functions actuall work at IRQL's higher than they are documented at, but I 
// do not recommend using them in a way that is inconsistant with the documentation, 
// Bill Gates might change his mind one day and decide to enforce the restriction!).
// If the DispatchControl request can be handle easily and does not need syncronizing
// then go ahead and handle it in the function. Otherwise schedule it for later and place
// it in our cue.  For this project all I/O with the card will require syncronization.
// To syncronize request we use a CRITIAL_SECTION through the KeSynchronizeExecution(....)
//	To accomplish this we mark the IRP pending by using IoMarkIrpPending(Irp) then hand the
// IRP packet to StartPacket and return STATUS_PENDING.  The IRP will be passed to the
// StartIo routine. One of the parameters to KeSynchronizeExecution(....) is the 
//	Syncronization routine.  One such routine for our projects is RunCommandsSync.  When
// this function is called it is called at DIRQL.  This means we are running at the same
// IRQL as our Interrupt routine thus it can not preempt us.  Keep in mind though that
//	extensive work while in this INTERRUPT_LEVEL will slow down the system. In short,
// Get in and Get out quickly.
//
// --Chris Hodge
//
////////////////////////////////////////////////////////////////////////////////////

#include "stddcls.h"
#include "driver.h"
#include <initguid.h>
#include "DevGuids.h"

NTSTATUS AddDevice(IN PDRIVER_OBJECT DriverObject, IN PDEVICE_OBJECT pdo);
VOID DriverUnload(IN PDRIVER_OBJECT fdo);
NTSTATUS OnRequestComplete(IN PDEVICE_OBJECT fdo, IN PIRP Irp, IN PKEVENT pev);

BOOLEAN IsWin98();
BOOLEAN win98 = FALSE;

UNICODE_STRING servkey;

///////////////////////////////////////////////////////////////////////////////

#pragma INITCODE

extern "C" NTSTATUS DriverEntry(IN PDRIVER_OBJECT DriverObject, IN PUNICODE_STRING RegistryPath)
{
	DbgPrint (DRIVERNAME " - Entering DriverEntry: DriverObject %8.8lX\n", DriverObject);

	// Insist that OS support at least the WDM level of the DDK we use
	if (!IoIsWdmVersionAvailable(1, 0))
	{
		DbgPrint (DRIVERNAME " - Expected version of WDM (%d.%2.2d) not available\n", 1, 0);
		return STATUS_UNSUCCESSFUL;
	}

	// See if we're running under Win98 or NT:
	win98 = IsWin98();

#if DBG
	if (win98)
		DbgPrint (DRIVERNAME " - Running under Windows 98\n");
	else
		DbgPrint (DRIVERNAME " - Running under NT\n");
#endif

	// Save the name of the service key
	servkey.Buffer = (PWSTR) ExAllocatePool(PagedPool, RegistryPath->Length + sizeof(WCHAR));
	if (!servkey.Buffer)
	{
		DbgPrint (DRIVERNAME " - Unable to allocate %d bytes for copy of service key name\n", RegistryPath->Length + sizeof(WCHAR));
		return STATUS_INSUFFICIENT_RESOURCES;
	}
	servkey.MaximumLength = RegistryPath->Length + sizeof(WCHAR);
	RtlCopyUnicodeString(&servkey, RegistryPath);

	// Initialize function pointers
	DriverObject->DriverUnload = DriverUnload;
	DriverObject->DriverExtension->AddDevice = AddDevice;

	DriverObject->MajorFunction[IRP_MJ_CREATE] = DispatchCreate;
	DriverObject->MajorFunction[IRP_MJ_CLOSE] = DispatchClose;
	DriverObject->MajorFunction[IRP_MJ_READ] = DispatchReadWrite;
	DriverObject->MajorFunction[IRP_MJ_WRITE] = DispatchReadWrite;
	DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = DispatchControl;
	DriverObject->MajorFunction[IRP_MJ_POWER] = DispatchPower;
	DriverObject->MajorFunction[IRP_MJ_PNP] = DispatchPnp;
	
	return STATUS_SUCCESS;
}

#pragma PAGEDCODE

VOID DriverUnload(IN PDRIVER_OBJECT DriverObject)
{
	PAGED_CODE();
	DbgPrint (DRIVERNAME " - Entering DriverUnload: DriverObject %8.8lX\n", DriverObject);
	RtlFreeUnicodeString(&servkey);
}

NTSTATUS AddDevice(IN PDRIVER_OBJECT DriverObject, IN PDEVICE_OBJECT pdo)
{
	PAGED_CODE();
	DbgPrint (DRIVERNAME " - Entering AddDevice: DriverObject %8.8lX, pdo %8.8lX\n", DriverObject, pdo);

	NTSTATUS status;

	// Create a functional device object to represent the hardware we're managing.
	PDEVICE_OBJECT fdo;
	#define xsize sizeof(DEVICE_EXTENSION)
	status = IoCreateDevice(DriverObject, xsize, NULL,	FILE_DEVICE_UNKNOWN, FILE_DEVICE_SECURE_OPEN, FALSE, &fdo);
	if (!NT_SUCCESS(status))
	{
		DbgPrint (DRIVERNAME " - IoCreateDevice failed - %X\n", status);
		return status;
	}
	
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;

	// From this point forward, any error will have side effects that need to
	// be cleaned up. Using a try-finally block allows us to modify the program
	// easily without losing track of the side effects.
	__try
	{
		pdx->DeviceObject = fdo;
		pdx->Pdo = pdo;
		IoInitializeRemoveLock(&pdx->RemoveLock, 0, 0, 0);
		pdx->state = STOPPED;		// device starts in the stopped state
		InitializeQueue(&pdx->dqReadWrite, StartIo);

		// Declare the buffering method we'll use for read/write requests
//		fdo->Flags |= DO_DIRECT_IO;
		fdo->Flags |= DO_BUFFERED_IO;

		// Initialize DPC object
		IoInitializeDpcRequest(fdo, DpcForIsr);
		KeInitializeDpc( &pdx->DpcCustomQue, (PKDEFERRED_ROUTINE) &DpcCustom, fdo );

		// Link our device object into the stack leading to the PDO
		pdx->LowerDeviceObject = IoAttachDeviceToDeviceStack(fdo, pdo);
		if (!pdx->LowerDeviceObject)
		{
			DbgPrint (DRIVERNAME " - IoAttachDeviceToDeviceStack failed\n");
			status = STATUS_DEVICE_REMOVED;
			__leave;
		}

		// Set power management flags in the device object
		fdo->Flags |= DO_POWER_PAGABLE;

		// Register a device interface
		status = IoRegisterDeviceInterface(pdo, &GUID_INTERFACE_IVPHC, NULL, &pdx->ifname);
		if (!NT_SUCCESS(status))
		{
			DbgPrint (DRIVERNAME " - IoRegisterDeviceInterface failed - %8.8lX\n", status);
			__leave;
		}

		// Indicate that our initial power state is D0 (fully on). Also indicate that
		// we have a pagable power handler (otherwise, we'll never get idle shutdown
		// messages!)
		pdx->syspower = PowerSystemWorking;
		pdx->devpower = PowerDeviceD0;
		POWER_STATE state;
		state.DeviceState = PowerDeviceD0;
		PoSetPowerState(fdo, DevicePowerState, state);

		// Clear the "initializing" flag so that we can get IRPs
		fdo->Flags &= ~DO_DEVICE_INITIALIZING;
	}						// finish initialization
	__finally
	{						// cleanup side effects
		if (!NT_SUCCESS(status))
		{
			if (pdx->ifname.Buffer)
				RtlFreeUnicodeString(&pdx->ifname);
			if (pdx->LowerDeviceObject)
				IoDetachDevice(pdx->LowerDeviceObject);
			IoDeleteDevice(fdo);
		}
	}

	return status;
}

#pragma LOCKEDCODE

NTSTATUS CompleteRequest(IN PIRP Irp, IN NTSTATUS status, IN ULONG_PTR info)
{
	Irp->IoStatus.Status = status;
	Irp->IoStatus.Information = info;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	return status;
}

NTSTATUS CompleteRequest(IN PIRP Irp, IN NTSTATUS status)
{
	Irp->IoStatus.Status = status;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	return status;
}

#pragma PAGEDCODE

NTSTATUS ForwardAndWait(IN PDEVICE_OBJECT fdo, IN PIRP Irp)
{
	ASSERT(KeGetCurrentIrql() == PASSIVE_LEVEL);
	PAGED_CODE();
	
	KEVENT event;
	KeInitializeEvent(&event, NotificationEvent, FALSE);

	IoCopyCurrentIrpStackLocationToNext(Irp);
	IoSetCompletionRoutine(Irp, (PIO_COMPLETION_ROUTINE) OnRequestComplete,
		(PVOID) &event, TRUE, TRUE, TRUE);

	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;
	IoCallDriver(pdx->LowerDeviceObject, Irp);
	KeWaitForSingleObject(&event, Executive, KernelMode, FALSE, NULL);
	return Irp->IoStatus.Status;
}

#pragma LOCKEDCODE

NTSTATUS OnRequestComplete(IN PDEVICE_OBJECT fdo, IN PIRP Irp, IN PKEVENT pev)
{
	KeSetEvent(pev, 0, FALSE);
	return STATUS_MORE_PROCESSING_REQUIRED;
}

VOID EnableAllInterfaces(PDEVICE_EXTENSION pdx, BOOLEAN enable)
{
	IoSetDeviceInterfaceState(&pdx->ifname, enable);
}

VOID DeregisterAllInterfaces(PDEVICE_EXTENSION pdx)
{
	IoSetDeviceInterfaceState(&pdx->ifname, FALSE);
	RtlFreeUnicodeString(&pdx->ifname);
}

#pragma PAGEDCODE

VOID RemoveDevice(IN PDEVICE_OBJECT fdo)
{
	PAGED_CODE();
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;
	NTSTATUS status;


	if (pdx->LowerDeviceObject)
		IoDetachDevice(pdx->LowerDeviceObject);

	IoDeleteDevice(fdo);
}

#pragma INITCODE

BOOLEAN IsWin98()
{
#ifdef _X86_

	// Windows 98 (including 2d ed) supports WDM version 1.0, whereas Win2K
	// supports 1.10.
	return !IoIsWdmVersionAvailable(1, 0x10);
#else // not _X86_
	return FALSE;
#endif // not _X86_
}

#if DBG && defined(_X86_)
#pragma LOCKEDCODE

extern "C" void __declspec(naked) __cdecl _chkesp()
{
	_asm je okay
	ASSERT(!DRIVERNAME " - Stack pointer mismatch!");
okay:
	_asm ret
}
#endif // DBG
