///////////////////////////////////////////////////////////////////////////////
// Read/Write request processors for FxMphc driver
// Copyright (C) 2002 By FoxJet Tech Center
// All rights reserved
//
//	Author: Chris G. Hodge
//	Version History:
//		1.00	Creation
///////////////////////////////////////////////////////////////////////////////

#include "stddcls.h"
#include "Defines.h"
#include "driver.h"
#include "..\..\FxMphc\src\IoCtls.h"

#pragma PAGEDCODE

NTSTATUS DispatchCreate(PDEVICE_OBJECT fdo, PIRP Irp)
{
	PAGED_CODE();
	DbgPrint (DRIVERNAME " - IRP_MJ_CREATE\n");
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;

	PIO_STACK_LOCATION stack = IoGetCurrentIrpStackLocation(Irp);

	// Claim the remove lock in Win2K so that removal waits until the
	// handle closes. Don't do this in Win98, however, because this
	// device might be removed by surprise with handles open, whereupon
	// we'll deadlock in HandleRemoveDevice waiting for a close that
	// can never happen because we can't run the user-mode code that
	// would do the close.
	NTSTATUS status;
	if (win98)
		status = STATUS_SUCCESS;
	else 
		status = IoAcquireRemoveLock(&pdx->RemoveLock, stack->FileObject);

	if (NT_SUCCESS(status))
	{
		if (InterlockedIncrement(&pdx->handles) == 1)
		{
		}
      DbgPrint ( "%s - Instance 0x%x Created", DRIVERNAME, pdx->handles );
	}
	return CompleteRequest(Irp, status, 0);
}

#pragma PAGEDCODE

NTSTATUS DispatchClose(PDEVICE_OBJECT fdo, PIRP Irp)
{
	PAGED_CODE();
	DbgPrint (DRIVERNAME " - IRP_MJ_CLOSE\n");
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;
	PIO_STACK_LOCATION stack = IoGetCurrentIrpStackLocation(Irp);
	if (InterlockedDecrement(&pdx->handles) == 0)
	{
	   for ( int i = 0; i < MAX_DEVICES; i++ ) {
			if ( pdx->m_Devices[i].GetIOAddress() != NULL )
			{
				DbgPrint ( "%s- Destroying Device 0x%X", DRIVERNAME, pdx->m_Devices[i].GetIOAddress() );
				pdx->m_Devices[i].Destroy();
			}
	   }
	}

	// Release the remove lock to match the acquisition done in DispatchCreate
	if (!win98)
		IoReleaseRemoveLock(&pdx->RemoveLock, stack->FileObject);

	return CompleteRequest(Irp, STATUS_SUCCESS, 0);
}

#pragma PAGEDCODE

NTSTATUS DispatchReadWrite(PDEVICE_OBJECT fdo, PIRP Irp)
{
	PAGED_CODE();
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;

	IoMarkIrpPending(Irp);
	StartPacket(&pdx->dqReadWrite, fdo, Irp, OnCancelReadWrite);
	return STATUS_PENDING;
}

#pragma LOCKEDCODE

VOID OnCancelReadWrite(IN PDEVICE_OBJECT fdo, IN PIRP Irp)
{
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;
	CancelRequest(&pdx->dqReadWrite, Irp);
}

#pragma LOCKEDCODE

// This is the routine which process all IRPs form the Interrupt function or
// from any function which must run sync. with the ISR.
VOID DpcCustom(PKDPC Dpc, PDEVICE_OBJECT fdo, ULONG lCards, PDEVICE_EXTENSION pdx)
{
	KeAcquireSpinLockAtDpcLevel (&pdx->IsrSpinLock);

	KeRemoveQueueDpc( &pdx->DpcCustomQue );

	for (int i = 0; i < ARRAYSIZE (pdx->m_Devices); i++) {
		if (lCards & (1 << i)) {
			pdx->m_Devices [i].OnInterrupt ();
		}

		if (pdx->m_Devices[i].m_bSerialDataReady == TRUE)
			pdx->m_Devices[i].FireSerialEvent();			
	}
	
	KeReleaseSpinLockFromDpcLevel (&pdx->IsrSpinLock);
}

VOID DpcForIsr(PKDPC Dpc, PDEVICE_OBJECT fdo, PIRP Irp, PDEVICE_EXTENSION pdx)
{
	IoAcquireRemoveLock(&pdx->RemoveLock, Irp);
		Irp = GetCurrentIrp(&pdx->dqReadWrite);
		if  ( Irp )
		{
			pdx->bActiveRequest = pdx->bActiveRequestComplete = FALSE;
			StartNextPacket(&pdx->dqReadWrite, fdo);
			IoCompleteRequest(Irp, IO_SERIAL_INCREMENT);
		}
	IoReleaseRemoveLock(&pdx->RemoveLock, Irp);

}							// DpcForIsr

#pragma LOCKEDCODE
BOOLEAN OnInterrupt(PKINTERRUPT InterruptObject, PDEVICE_EXTENSION pdx)
{
	{
		for (ULONG i = 0; i < ARRAYSIZE (pdx->m_Devices); i++) 
			pdx->m_Devices [i].m_lInterrupt++;
	}

	ULONG lCards = 0;

	ASSERT (ARRAYSIZE (pdx->m_Devices) <= (sizeof (lCards) * 8));

	if (pdx->m_pMasterController->m_bFpgaLoaded == FALSE)
        return FALSE;

    PIRP pIrp = GetCurrentIrp(&pdx->dqReadWrite);

    if (pIrp)
        if (pIrp->Cancel || AreRequestsBeingAborted (&pdx->dqReadWrite))
            return TRUE;

	/*
	static ULONG lInterrupt = 0;

	bool bDebug = ((lInterrupt++ % (150)) == 0);
	*/

	for (ULONG i = 0; i < ARRAYSIZE (pdx->m_Devices); i++) {
		/*
		if (bDebug) {
			DbgPrint (DRIVERNAME " - [%d, 0x%X] %s%s%s%s%s%s", 
				i, pdx->m_Devices [i].GetIOAddress (), 
				pdx->m_Devices [i].GetInterruptType () == CCard::INTERRUPT_PHOTOCELL ? "PC " : "ENC",
				pdx->m_Devices [i].IsPresent ()			? " IsPresent"		: " NOT PRESENT",
				pdx->m_Devices [i].IsEnabled ()			? " IsEnabled"		: " DISABLED",
				pdx->m_Devices [i].IsPaused ()			? " PAUSED"			: "",
				pdx->m_Devices [i].IsImageReady ()		? ""				: " IMAGE NOT READY",
				pdx->m_Devices [i].IsPrintEnabled ()	? ""				: " PRINT DISABLED"); 
		}
		*/

		if (pdx->m_Devices [i].IsPresent () && pdx->m_Devices [i].IsEnabled ()) {
			UCHAR nIO = pdx->m_Devices [i].ReadByte (IV_REG_READ_IO);
			BOOLEAN bInterrupt		= (nIO & (1 << INTERRUPT))	? TRUE : FALSE;
			BOOLEAN bPhotoDetect	= (nIO & (1 << PHOTOSTATE))	? TRUE : FALSE;

			//* if (bDebug) */ DbgPrint (DRIVERNAME " - [%d, 0x%X] nIO: 0x%p", i, pdx->m_Devices [i].GetIOAddress (), nIO); 

			if (pdx->m_Devices [i].GetIOAddress () >= 0x320 && pdx->m_Devices [i].IsSharedPhotocell ()) {
				// shared photocell from card 2 means we need card 1's photo detect state
				for (ULONG j = 0; j < ARRAYSIZE (pdx->m_Devices); j++) {
					if (pdx->m_Devices [j].m_bMaster) {
						pdx->m_Devices [j].m_bPhotoDetect = pdx->m_Devices [i].m_bPhotoDetect;
						break;
					}
				}
			}
			else
				pdx->m_Devices [i].m_bPhotoDetect = bPhotoDetect;

//			pdx->m_Devices [i].m_bPhotoDetect = bPhotoDetect;

			if (bInterrupt) {
				if (!pdx->m_Devices [i].m_bMaster)
					pdx->m_Devices [i].ClearIRQ ();
	
				if (pdx->m_Devices [i].IsEnabled () && pdx->m_Devices [i].IsPrintEnabled ()) {
					if (pdx->m_Devices [i].IsDPC ()) 
						lCards |= (1 << i);
					else 
						pdx->m_Devices [i].OnInterrupt ();
				}
				else 
					pdx->m_Devices [i].OnEndOfColumn ();
			}
			else 
				pdx->m_Devices [i].OnEndOfColumn ();

			if (i == 0) {
				BOOLEAN bUART = (nIO & (1 << UART)) ? TRUE : FALSE;
			
				if (bUART) {
					//DbgPrint (DRIVERNAME " - pdx->m_Devices [%d].ProcessSerialData ()", i);
					pdx->m_Devices [i].ProcessSerialData ();
				}
			}
		}
	}

	//if (bDebug) DbgPrint (DRIVERNAME " - "); 

	pdx->m_pMasterController->ClearIRQ (); // clear master irq

	{
		int nTries = 0;
		BOOLEAN bQueued = FALSE;
		
		while (!bQueued && ((nTries++) < 2)) {
			bQueued = KeInsertQueueDpc (&pdx->DpcCustomQue, (PVOID)lCards, pdx);

			if (!bQueued) {
				KIRQL irql;

				//DbgPrint (DRIVERNAME " - *** KeInsertQueueDpc failed: lCards: 0x%p [%d] ***", lCards, nTries);

				KeAcquireSpinLock (&pdx->IsrSpinLock, &irql);
				KeRemoveQueueDpc (&pdx->DpcCustomQue);
				KeReleaseSpinLock (&pdx->IsrSpinLock, irql);
			}
		}

		if (!bQueued) {
			DbgPrint (DRIVERNAME " - *** OnInterrupt returned FALSE ***");
			return FALSE;
		}
	}

	return TRUE;
}

#pragma PAGEDCODE
NTSTATUS StartDevice(PDEVICE_OBJECT fdo, PCM_PARTIAL_RESOURCE_LIST raw, PCM_PARTIAL_RESOURCE_LIST translated)
{
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;
	NTSTATUS status;
	KIRQL irql;
	KINTERRUPT_MODE mode;
	KAFFINITY affinity;
	BOOLEAN irqshare;
	BOOLEAN gotinterrupt = FALSE;

	PHYSICAL_ADDRESS portbase;
	BOOLEAN gotport = FALSE;

	PHYSICAL_ADDRESS membase;
	BOOLEAN gotmemory = FALSE;
	unsigned long i = 0;
	
	DbgPrint ( "%s - Start Device.\n", DRIVERNAME);
	if (!translated) {
		DbgPrint ( "%s - No Resources Assigned\n", DRIVERNAME);
		return STATUS_DEVICE_CONFIGURATION_ERROR;		// no resources assigned??
	}

	PCM_PARTIAL_RESOURCE_DESCRIPTOR resource = translated->PartialDescriptors;
	ULONG nPorts = 0;
	ULONG nres = translated->Count;
	for ( i = 0; i < MAX_DEVICES; i++ )
	{
		if ( (status = pdx->m_Devices[i].Initialize() ) != STATUS_SUCCESS)
		{
			DbgPrint ( "%s - Card Object %i Failed initialize()\n", DRIVERNAME, i );
			return status;
		}
	}

	for (i = 0; i < nres; ++i, ++resource)
	{
		switch (resource->Type)
		{					// switch on resource type
			case CmResourceTypePort:
				pdx->m_Devices[nPorts].PhyAddress = resource->u.Port.Start;
				pdx->m_Devices[nPorts].m_lPortLen = resource->u.Port.Length;
				pdx->m_Devices[nPorts].m_bMappedPort = (resource->Flags & CM_RESOURCE_PORT_IO) == 0;
				nPorts++;
//				if ( nPorts == MAX_DEVICES )
					gotport = TRUE;
				break;
			case CmResourceTypeInterrupt:
				irql = (KIRQL) resource->u.Interrupt.Level;
				pdx->IntVectorNum = resource->u.Interrupt.Vector;
				affinity = resource->u.Interrupt.Affinity;
				mode = (resource->Flags == CM_RESOURCE_INTERRUPT_LATCHED)
					? Latched : LevelSensitive;
				irqshare = resource->ShareDisposition == CmResourceShareShared;
				gotinterrupt = TRUE;
				break;
			case CmResourceTypeMemory:
				membase = resource->u.Memory.Start;
				pdx->memlength = resource->u.Memory.Length;
				switch ( resource->Flags ) {
					case CM_RESOURCE_MEMORY_READ_WRITE:
						DbgPrint("%s - Memory Base: %X Length %lu [READ_WRITE]\n", DRIVERNAME, membase, pdx->memlength);
						break;
					case CM_RESOURCE_MEMORY_WRITE_ONLY:
						DbgPrint("%s - Memory Base: %X Length %lu [WRITE_ONLY]\n", DRIVERNAME, membase, pdx->memlength);
						break;
					case CM_RESOURCE_MEMORY_READ_ONLY:
						DbgPrint("%s - Memory Base: %X Length %lu [READ_ONLY]\n", DRIVERNAME, membase, pdx->memlength);
						break;
				}
				gotmemory = TRUE;
				break;
			default:
				DbgPrint (DRIVERNAME " - Unexpected I/O resource type %d\n", resource->Type);
				break;
		}					// switch on resource type
	}

	pdx->SerialDataEvent = NULL;

   // Verify that we got all the resources we were expecting
//	if ( !(TRUE && gotinterrupt && gotport && gotmemory) ) {
//		if ( !gotport )
//			DbgPrint (DRIVERNAME " - Didn't get expected I/O resources.\n");
//		if ( !gotinterrupt )
//			DbgPrint (DRIVERNAME " - Didn't get expected Interrupt resource.\n");
//		if ( !gotmemory )
//			DbgPrint (DRIVERNAME " - Didn't get expected Memory resource.\n");
//		return STATUS_DEVICE_CONFIGURATION_ERROR;
//	}

	if ( !(TRUE && gotinterrupt && gotport) ) {
		if ( !gotport )
			DbgPrint (DRIVERNAME " - Didn't get expected I/O resources.\n");
		if ( !gotinterrupt )
			DbgPrint (DRIVERNAME " - Didn't get expected Interrupt resource.\n");
		return STATUS_DEVICE_CONFIGURATION_ERROR;
	}

	ULONG lowAddr = 0xFFFFFFFF;
	for ( i = 0; i < nPorts; i++ ) {
		if (pdx->m_Devices[i].m_bMappedPort)
		{
			PUCHAR Address;
			Address = (PUCHAR) MmMapIoSpace(pdx->m_Devices[i].PhyAddress, pdx->m_Devices[i].m_lPortLen, MmNonCached);
			if (!Address) {
				DbgPrint (DRIVERNAME " - Unable to map port range %I64X, length %X\n",
					pdx->m_Devices[i].PhyAddress, pdx->m_Devices[i].m_lPortLen);
				return STATUS_INSUFFICIENT_RESOURCES;
			}
			pdx->m_Devices[i].AssignAddress ( Address );
		}
		else
			pdx->m_Devices[i].AssignAddress ( (PUCHAR) pdx->m_Devices[i].PhyAddress.QuadPart);

		pdx->m_Devices[i].m_cPhysicalIrq = 0x0F & (UCHAR)pdx->IntVectorNum;

		// The lowest address will be the Master board, the one that maps its interrupt to the ISA Bus.
		if ( pdx->m_Devices[i].IsPresent() )
		{
			if ( lowAddr > (ULONG)pdx->m_Devices[i].GetIOAddress() )
				lowAddr = (ULONG)pdx->m_Devices[i].GetIOAddress();
		}
	}

	int nIrqNum = 1;
	for ( i = 0; i < nPorts; i++ ) 
	{
		if ( ((ULONG)pdx->m_Devices[i].GetIOAddress()) == lowAddr ) 
		{
			pdx->m_Devices[i].SetInterruptMode ( MASTER );
			pdx->m_pMasterController = &pdx->m_Devices[i];
			DbgPrint( "%s - SetInterrputMode to MASTER for 0x%X \n", DRIVERNAME, lowAddr);
		}
		else
		{
			pdx->m_Devices[i].m_cOTBIrqId = nIrqNum++;
			pdx->m_Devices[i].m_cPhysicalIrq = 0x0F & (UCHAR)pdx->IntVectorNum;
			pdx->m_Devices[i].SetInterruptMode ( SLAVE );
			if ( pdx->m_Devices[i].IsPresent() )
				DbgPrint( "%s - SetInterrputMode to SLAVE for 0x%X \n", DRIVERNAME, lowAddr);
		}
	}

	DbgPrint ( "%s - Initializing Isr SpinLock\n", DRIVERNAME );
	KeInitializeSpinLock ( &pdx->IsrSpinLock );

	DbgPrint ("%s - Connecting Interrupt\n", DRIVERNAME );
	status = IoConnectInterrupt(&pdx->InterruptObject, (PKSERVICE_ROUTINE) OnInterrupt,
		(PVOID) pdx, NULL, pdx->IntVectorNum, irql, irql, mode, irqshare, affinity, FALSE);

	if (!NT_SUCCESS(status)) 
	{
		DbgPrint("%s - IoConnectInterrupt failed - 0x%X\n", DRIVERNAME, status);
		return status;
	}
	
	DbgPrint ("%s - StartDevice Successful.\n", DRIVERNAME );
	return STATUS_SUCCESS;
}

#pragma LOCKEDCODE
VOID RunWriteMajorSync (PDEVICE_EXTENSION pdx)
{
	PIRP Irp = GetCurrentIrp( &pdx->dqReadWrite );
	PIO_STACK_LOCATION IrpStack = IoGetCurrentIrpStackLocation(Irp);
	ULONG InputLength		= IrpStack->Parameters.Write.Length;
	PVOID SystemBuffer	= Irp->AssociatedIrp.SystemBuffer;

	ULONG info = 0;
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ( "Write Major Called" );
	Irp->IoStatus.Status = status;
	Irp->IoStatus.Information = info;
	IoRequestDpc(pdx->DeviceObject, Irp, pdx);
}

VOID RunReadMajorSync (PDEVICE_EXTENSION pdx)
{
	PIRP Irp = GetCurrentIrp( &pdx->dqReadWrite );
	PIO_STACK_LOCATION IrpStack = IoGetCurrentIrpStackLocation(Irp);
	ULONG OutputLength	= IrpStack->Parameters.DeviceIoControl.InputBufferLength;
	PVOID SystemBuffer	= Irp->AssociatedIrp.SystemBuffer;

	ULONG info = 0;
	NTSTATUS status = STATUS_SUCCESS;
	Irp->IoStatus.Status = status;
	Irp->IoStatus.Information = info;
	IoRequestDpc(pdx->DeviceObject, Irp, pdx);
}

VOID RunIOCTLSync (PDEVICE_EXTENSION pdx) 
{
	PIRP Irp = GetCurrentIrp( &pdx->dqReadWrite );
	ULONG info = 0;
	NTSTATUS status = STATUS_SUCCESS;
	PIO_STACK_LOCATION IrpStack = IoGetCurrentIrpStackLocation(Irp);
	ULONG ControlCode		= IrpStack->Parameters.DeviceIoControl.IoControlCode;
	ULONG InputLength		= IrpStack->Parameters.DeviceIoControl.InputBufferLength;
	ULONG OutputLength	= IrpStack->Parameters.DeviceIoControl.OutputBufferLength;
	PVOID SystemBuffer	= Irp->AssociatedIrp.SystemBuffer;
	
	pdx->bActiveRequest = TRUE;
	pdx->bActiveRequestComplete = FALSE;
	switch ( ControlCode ) 	// process request
	{
		case IOCTL_WRITE_FPGA:
			status = IoCtlWriteFPGA (SystemBuffer, InputLength, OutputLength, info, pdx); 
			break;
		case IOCTL_READ_SERIAL_DATA:
			{
				status = STATUS_SUCCESS;
				info = 0;
				DbgPrint ( "%s - IOCTL_READ_SERIAL_DATA\n", DRIVERNAME );
				BOOLEAN found = FALSE;
				if ( InputLength < sizeof(HANDLE) )
				{	
					DbgPrint("Invalid Input Parameter");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}

				if (OutputLength < SERIAL_BUFFER_SIZE )
				{	
					DbgPrint("Invalid Output Buffer Size");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}

				ULONG Address	= *( (PULONG) SystemBuffer );
				PUCHAR pData	=  (PUCHAR) SystemBuffer;

				for ( int i = 0; i < MAX_DEVICES; i++ ) 
				{
					if ( pdx->m_Devices[i].IsPresent() )
					{
						if ( pdx->m_Devices[i].PhyAddress.QuadPart == Address ) 
						{
							DbgPrint ( "%s - Getting Serial Data from 0x%X\n", DRIVERNAME, pdx->m_Devices[i].PhyAddress.QuadPart );
							info = pdx->m_Devices[i].GetSerialData ( pData );
							found = TRUE;
						}
					}
				}

				if ( found == FALSE ) 
				{
					info = 0;
					DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, Address);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_IMAGE_UPDATE:
			{
				status = STATUS_SUCCESS;
				info = 0;

				if (InputLength < sizeof(tagImgUpdateHdr)) {	// Input Buffer too Small
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - IOCTL_IMAGE_UPDATE: Input Paramater INVALID\n", DRIVERNAME);
					break;
				}

				PUCHAR pData = (PUCHAR) SystemBuffer;
				PHCB *pPHCB  = (PHCB *) SystemBuffer;
				BOOLEAN found = FALSE;
				tagImgUpdateHdr ImageHdr;
				RtlCopyMemory ( &ImageHdr, pPHCB->Data, sizeof ( tagImgUpdateHdr ) );
				
				// Skip over Print Head Control Block.
				pData += sizeof ( PHCB );

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].GetIOAddress() == pPHCB->lAddress ) {
						pdx->m_Devices[i].TxImage ( ImageHdr._wStartCol, ImageHdr._wNumCols, ImageHdr._nBPC, pData );
						found = TRUE;
						break;
					}
				}
				if ( !found )
				{
					DbgPrint ( "Unable to Locate Device Address %X", pPHCB->lAddress);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_CLEAR_BUFFER:
			{
				status = STATUS_SUCCESS;
				info = 0;
				DbgPrint ( "%s - IOCTL_CLEAR_BUFFER\n", DRIVERNAME );
				BOOLEAN found = FALSE;
				if ( InputLength < sizeof(ULONG) )
				{	
					DbgPrint("Invalid Input Parameter\n");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}

				ULONG Address	= *( (PULONG) SystemBuffer );
				for ( int i = 0; i < MAX_DEVICES; i++ ) 
				{
					if ( pdx->m_Devices[i].IsPresent() )
					{
						if ( pdx->m_Devices[i].PhyAddress.QuadPart == Address ) 
						{
							pdx->m_Devices[i].ClearOnBoardRam();
							found = TRUE;
						}
					}
				}

				if ( found == FALSE ) 
				{
					DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, Address);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_SET_SERIAL_PARAMS:
			{
				status = STATUS_SUCCESS;
				info = 0;

				if (InputLength < sizeof( PHCB ) ) {	// Output Buffer too Small
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - Invalid Input Parameter.\n", DRIVERNAME );
					break;
				}
		
				PHCB *pPHCB = ( PHCB * ) SystemBuffer;
				tagSerialParams *Port = ( tagSerialParams * ) pPHCB->Data;
				
				DbgPrint ( "%s - Serial Params: Baud %lu, Data %i, Parity %i, Stop %i", DRIVERNAME, Port->BaudRate, Port->DataBits, Port->Parity, Port->StopBits);
				BOOLEAN found = false;
				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].PhyAddress.QuadPart == pPHCB->lAddress ) {
						found = TRUE;
						status = pdx->m_Devices[i].SetSerialParams ( Port );
						break;
					}
				}

				if ( found == FALSE ) {
					DbgPrint( "%s - Address Located. Writing Params\n", DRIVERNAME );
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_GET_STATE:
			{
				status = STATUS_SUCCESS;
				info = 0;
				BOOLEAN found = FALSE;
				IVCARDSTATESTRUCT * pState = (IVCARDSTATESTRUCT *)SystemBuffer;

				if (OutputLength < sizeof (IVCARDSTATESTRUCT)) {
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - Invalid Input Parameter.\n", DRIVERNAME );
					break;
				}

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if (pdx->m_Devices[i].PhyAddress.QuadPart == pState->m_lAddress) {
						status = pdx->m_Devices[i].GetState (pState);
						info = sizeof (IVCARDSTATESTRUCT);
						found = TRUE;
						break;
					}
				}

				if ( found == FALSE ) {
					info = 0;
					DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, pState->m_lAddress);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_SET_CONFIG:
			{
				status = STATUS_SUCCESS;
				info = 0;
				DbgPrint (DRIVERNAME  " - IOCTL_SET_CONFIG");
				BOOLEAN found = FALSE;
				IVCARDCONFIGSTRUCT * pConfig = (IVCARDCONFIGSTRUCT *)SystemBuffer;

				if (InputLength < sizeof (IVCARDCONFIGSTRUCT)) {
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - Invalid Input Parameter.\n", DRIVERNAME );
					break;
				}

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if (pdx->m_Devices[i].PhyAddress.QuadPart == pConfig->m_lAddress) {
						status = pdx->m_Devices[i].SetConfig (pConfig);
						info = sizeof (IVCARDCONFIGSTRUCT);
						found = TRUE;
						break;
					}
				}

				if ( found == FALSE ) {
					info = 0;
					DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, pConfig->m_lAddress);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_READ_REGISTER:
			{
				status = STATUS_SUCCESS;
				info = 0;
				BOOLEAN found = FALSE;
				IVREGISTERSTRUCT * pReg = (IVREGISTERSTRUCT *)SystemBuffer;

				if (InputLength < sizeof (IVREGISTERSTRUCT)) {
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - Invalid Input Parameter.\n", DRIVERNAME );
					break;
				}

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if (pdx->m_Devices[i].PhyAddress.QuadPart == pReg->m_lAddress) {
						status = pdx->m_Devices[i].ReadRegister (pReg);
						info = sizeof (IVREGISTERSTRUCT);
						found = TRUE;
						break;
					}
				}

				if ( found == FALSE ) {
					info = 0;
					DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, pReg->m_lAddress);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_WRITE_REGISTER:
			{
				status = STATUS_SUCCESS;
				info = 0;
				BOOLEAN found = FALSE;
				IVREGISTERSTRUCT * pReg = (IVREGISTERSTRUCT *)SystemBuffer;

				if (InputLength < sizeof (IVREGISTERSTRUCT)) {
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - Invalid Input Parameter.\n", DRIVERNAME );
					break;
				}

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if (pdx->m_Devices[i].PhyAddress.QuadPart == pReg->m_lAddress) {
						status = pdx->m_Devices[i].WriteRegister (pReg);
						info = sizeof (IVREGISTERSTRUCT);
						found = TRUE;
						break;
					}
				}

				if ( found == FALSE ) {
					info = 0;
					DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, pReg->m_lAddress);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_SET_STROBE:
			{
				status = STATUS_SUCCESS;
				info = 0;
				BOOLEAN found = FALSE;
				STROBESTRUCT * p = (STROBESTRUCT *)SystemBuffer;

				if (InputLength < sizeof (STROBESTRUCT)) {
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - Invalid Input Parameter.\n", DRIVERNAME );
					break;
				}

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if (pdx->m_Devices[i].PhyAddress.QuadPart == p->m_lAddress) {
						status = pdx->m_Devices[i].SetStrobe (p->m_nColor);
						info = sizeof (STROBESTRUCT);
						found = TRUE;
						break;
					}
				}

				if ( found == FALSE ) {
					info = 0;
					DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, p->m_lAddress);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		default:
			DbgPrint ( "%s - Invalid Device Request: Control Code: 0x%X\n", DRIVERNAME, ControlCode );
			status = STATUS_INVALID_DEVICE_REQUEST;
			break;

	}						// process request
	Irp->IoStatus.Status = status;
	Irp->IoStatus.Information = info;
	pdx->bActiveRequestComplete = TRUE;
	IoRequestDpc(pdx->DeviceObject, Irp, pdx);
}

VOID StartIo(IN PDEVICE_OBJECT fdo, IN PIRP Irp)
{
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;
	PIO_STACK_LOCATION IrpStack = IoGetCurrentIrpStackLocation(Irp);
	NTSTATUS status = IoAcquireRemoveLock(&pdx->RemoveLock, Irp);
	ULONG ControlCode		= IrpStack->Parameters.DeviceIoControl.IoControlCode;
	ULONG info = 0;

	if (!NT_SUCCESS(status)) {
		CompleteRequest(Irp, status, 0);
		return;
	}

	switch ( IrpStack->MajorFunction )	{
		case IRP_MJ_DEVICE_CONTROL:
			if(!KeSynchronizeExecution(pdx->InterruptObject, (PKSYNCHRONIZE_ROUTINE)RunIOCTLSync, (PVOID)pdx))
				status = STATUS_UNSUCCESSFUL;
			else
				return;
			break;
		case IRP_MJ_WRITE:
			if(!KeSynchronizeExecution(pdx->InterruptObject, (PKSYNCHRONIZE_ROUTINE)RunWriteMajorSync, (PVOID)pdx))
				status = STATUS_UNSUCCESSFUL;
			else
				return;
			break;
		case IRP_MJ_READ:
			if(!KeSynchronizeExecution(pdx->InterruptObject, (PKSYNCHRONIZE_ROUTINE)RunReadMajorSync, (PVOID)pdx))
				status = STATUS_UNSUCCESSFUL;
			else
				return;
			break;
		default:
			status = STATUS_INVALID_DEVICE_REQUEST;
	}
	IoReleaseRemoveLock(&pdx->RemoveLock, Irp);
	CompleteRequest(Irp, status, info);
}

#pragma PAGEDCODE

VOID StopDevice(IN PDEVICE_OBJECT fdo, BOOLEAN oktouch /* = FALSE */)
{
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;

	DbgPrint ( "%s- Disconnecting interrupt", DRIVERNAME);
	if (pdx->InterruptObject)
	{
		// TODO prevent device from generating more interrupts if possible
		IoDisconnectInterrupt(pdx->InterruptObject);
		pdx->InterruptObject = NULL;
	}
}
