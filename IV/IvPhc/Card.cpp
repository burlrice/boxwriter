// Card.cpp: implementation of the CCard class.
//
//////////////////////////////////////////////////////////////////////

#include "stddcls.h"
#include "Card.h"
#include "..\..\FxMphc\src\TypeDefs.h"
#include "Driver.h"
#include "IvRegs.h"
#include "DbTypeDefs.h"
#include <c:\ntddk\inc\ddk\ntddk.h>

#define IMGBUFFERTAG 'TJXV'

#define FREEBUFFER(p) \
if ((p)) { \
	ExFreePool ((p)); \
	(p) = NULL; \
}

//static const ULONG lImageBufferLen = (BUFFERWIDTH / 8) * BUFFERCOLUMNS;
#define IMAGEBUFFERLEN ((BUFFERWIDTH / 8) * BUFFERCOLUMNS)

void Format (USHORT n, CHAR * psz, BOOLEAN bPhotoDetect) 
{
	int nIndex= 0;

	for (int i = (sizeof (n) * 8) - 1; i >= 0; i--) 
//		psz [nIndex++] = (n & (1 << i)) ? '.' : 'X';
		psz [nIndex++] = (n & (1 << i)) ? (bPhotoDetect ? '-' : '.') : (bPhotoDetect ? 'P' : 'X');
}

void DbgPrintBuffer (USHORT wColumns, UCHAR cBPC, USHORT wRowWords, PUCHAR pImage)  // TODO: rem
{
	PUSHORT pBuffer = (PUSHORT)pImage;

	for (int j = 0; j < wColumns; j++) {
		CHAR szDebug [512];
		int nDebugIndex = 0;

		{
			for (int i = 0; i < ARRAYSIZE (szDebug); i++) 
				szDebug [i] = ' ';
		}

		PUCHAR p = (PUCHAR)pBuffer;

		for (int i = 0; i < cBPC; i += 2) {
			USHORT w = p [i] << 8 | p [i + 1];
			int nReg = IV_REG_DATA_PORT + ((i % 4) ? 2 : 0);

			Format (w, &szDebug [nDebugIndex], false);
			nDebugIndex += 17;
		}

		if (cBPC % 2) {
			int nReg = IV_REG_DATA_PORT + 2;
			Format (0x0000, &szDebug [nDebugIndex], false);
			nDebugIndex += 17;
		}

		pBuffer += wRowWords;

		szDebug [nDebugIndex] = 0;
		DbgPrint (DRIVERNAME " - %03d: %s", j, szDebug);
	}
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//const ULONG lClockSpeed = (29491200 / 2);
#define lClockSpeed  (29491200L / 2L)

CCard::CCard()
{
	// never called
}

CCard::~CCard()
{
	// Do not call this manaully, linker better not let you anyway!!!!
}

NTSTATUS CCard::Initialize()
{
	NTSTATUS status = STATUS_SUCCESS;

	m_cPhysicalIrq			= 0x00;
	m_pAddress				= NULL;
	m_bPresent				= FALSE;
	m_bMappedPort			= FALSE;
	m_evtPhoto				= NULL;	
	m_evtEOP				= NULL;
	m_bFpgaLoaded			= FALSE;
							
	m_pSerialTxBuffer		= NULL;
	m_pSerialTxTail			= m_pSerialTxHead = m_pSerialTxBuffer;
	m_pSerialRxBuffer		= NULL;
	m_pSerialRxTail			= m_pSerialRxHead = m_pSerialRxBuffer;
	m_bSerialDataReady		= FALSE;
							
	m_pcBuffer				= NULL;
	m_pwColumn				= NULL;
	m_cBPC					= 0;
	m_wColumns				= 1;//0;
	m_wCurrentCol			= 1;//0;
	m_wRowWords				= 0;
	m_lPhotocellCount		= 0;
	m_cEncoderDivisor		= 5;
	m_cCmdReg				= 0;
	m_nStrobeColor			= STROBE_LIGHT_OFF;
	m_bEnabled				= FALSE;
	m_bPause				= FALSE;
	m_bPauseAtEOP			= FALSE;
	m_bDebug				= FALSE;
	m_bPrintEnabled			= false;
							
	m_lInterrupt			= 0;

	m_bInternalPhotocell	= FALSE;
	m_bSharedPhotocell		= FALSE;
	m_lAutoPrintCols		= 0;
	m_lEncoderTicks			= 0;
	m_bPhotoDetect			= FALSE;
	m_bAutoPrint			= false;
	m_bDPC					= TRUE;
	m_bInitialPhotoState	= false;

	return status;
}

PUCHAR CCard::GetBufferPtr ()
{
	if (m_pcBuffer == NULL) {
		if (KeGetCurrentIrql () <= DISPATCH_LEVEL) {
			m_pcBuffer = (PUCHAR)ExAllocatePoolWithTag (NonPagedPool, IMAGEBUFFERLEN, IMGBUFFERTAG);

			if (m_pcBuffer == NULL) {
				DbgPrint (DRIVERNAME " - [0x%X] GetBufferPtr: FAILED TO ALLOCATE IMAGE BUFFER", m_pAddress);
				return NULL; //STATUS_INSUFFICIENT_RESOURCES;
			}
			else {
				DbgPrint (DRIVERNAME " - [0x%X] GetBufferPtr: m_pcBuffer: 0x%p [len: 0x%p]", 
					m_pAddress,
					m_pcBuffer,
					IMAGEBUFFERLEN);
			}

			m_pSerialTxTail = m_pSerialTxHead = m_pSerialTxBuffer;
			
			if ((m_pSerialRxBuffer = (PUCHAR)ExAllocatePool (NonPagedPool, SERIAL_BUFFER_SIZE)) == NULL) {
				DbgPrint ("%s - [0x%X] SerialRxBuffer Allocation FALIED\n", DRIVERNAME, m_pAddress);
				//return STATUS_INSUFFICIENT_RESOURCES;
			}
			else
				m_pSerialRxTail = m_pSerialRxHead = m_pSerialRxBuffer;
		}
		else {
			DbgPrint (DRIVERNAME " - [0x%X] GetBufferPtr: m_pcBuffer: 0x%p, IRQL: %d [DISPATCH_LEVEL: %d]", m_pAddress, m_pcBuffer, KeGetCurrentIrql (), DISPATCH_LEVEL);
		}
	}

	return m_pcBuffer;
}

BOOLEAN CCard::FreeBuffers ()
{
	if (KeGetCurrentIrql () <= DISPATCH_LEVEL) {
		FREEBUFFER (m_pcBuffer);

		// These should only be valid for Master device.
		FREEBUFFER (m_pSerialTxBuffer);
		FREEBUFFER (m_pSerialRxBuffer);

		return TRUE;
	}

	DbgPrint (DRIVERNAME " - [0x%X] FreeBuffers IRQL: %d [DISPATCH_LEVEL: %d]", m_pAddress, KeGetCurrentIrql (), DISPATCH_LEVEL);

	return FALSE;
}

UCHAR CCard::ReadByte( IN UCHAR Offset )
{
	UCHAR data;

	if (m_bMappedPort) 	// Memory mapped addressing 
		data = READ_REGISTER_UCHAR ( m_pAddress + Offset );
	else
		data = READ_PORT_UCHAR ( m_pAddress + Offset );

	return data;
}

ULONG CCard::ReadLong (IN ULONG Offset)
{
	ULONG data;

	if (m_bMappedPort) 	// Memory mapped addressing 
		data = READ_REGISTER_ULONG ((PULONG)(m_pAddress + Offset));
	else
		data = READ_PORT_ULONG ((PULONG)(m_pAddress + Offset));

	return data;
}

USHORT CCard::ReadWord (IN USHORT Offset)
{
	USHORT data;

	if (m_bMappedPort) 	// Memory mapped addressing 
		data = READ_REGISTER_USHORT ((PUSHORT)(m_pAddress + Offset));
	else
		data = READ_PORT_USHORT ((PUSHORT)(m_pAddress + Offset));

	return data;
}

VOID CCard::ReadBuffer ( IN UCHAR offset, IN PUCHAR pData, IN ULONG len)
{
	if (m_bMappedPort) 	// Memory mapped addressing 
		READ_REGISTER_BUFFER_UCHAR ( m_pAddress + offset, pData, len);
	else 
		READ_PORT_BUFFER_UCHAR ( m_pAddress + offset, pData, len);
}

VOID CCard::WriteBuffer (IN UCHAR offset, IN PUCHAR pData, IN ULONG len)
{
	if (m_bMappedPort) 	// Memory mapped addressing 
		WRITE_REGISTER_BUFFER_UCHAR ( m_pAddress + offset, pData, len );
	else 
		WRITE_PORT_BUFFER_UCHAR ( m_pAddress + offset, pData, len );
}

VOID CCard::WriteByte (IN UCHAR offset, IN UCHAR data)
{
	if (m_bMappedPort) 	// Memory mapped addressing 
		WRITE_REGISTER_UCHAR ( m_pAddress + offset, data );
	else 
		WRITE_PORT_UCHAR ( m_pAddress + offset, data );
}

VOID CCard::WriteLong (IN ULONG offset, IN ULONG data)
{
	if (m_bMappedPort) 	// Memory mapped addressing 
		WRITE_REGISTER_ULONG ((PULONG)(m_pAddress + offset), data);
	else 
		WRITE_PORT_ULONG ((PULONG)(m_pAddress + offset), data);
}

VOID CCard::WriteWord (IN USHORT offset, IN USHORT data)
{
	if (m_bMappedPort) 	// Memory mapped addressing 
		WRITE_REGISTER_USHORT ((PUSHORT)(m_pAddress + offset), data);
	else 
		WRITE_PORT_USHORT ((PUSHORT)(m_pAddress + offset), data);
}


NTSTATUS CCard::ProgramFPGA(PVOID pCode, ULONG lBytes)
{
	int nCount = 0;
	UCHAR nRead = 0;
	const UCHAR nDoneReg = IV_REG_BASE; // IV_REG_GAL

	// toggle the program signal 
    WriteByte (IV_REG_GAL, 0xFF);
    WriteByte (IV_REG_GAL, 0);

	nCount = 0;

	while (!((nRead = ReadByte (nDoneReg)) & 0x01)) {
		KeStallExecutionProcessor (1);
		nCount++;

		if (nCount > 10) {
			DbgPrint ("%s - [0x%X] timed out looking for 0x01 [0x%X]\n", DRIVERNAME, m_pAddress, nRead);
			return STATUS_IO_TIMEOUT;
		}
	}

    WriteByte (IV_REG_GAL, 0xFF);

	nCount = 0;

	while (!((nRead = ReadByte (nDoneReg)) & 0x10)) {
		KeStallExecutionProcessor (1);
		nCount++;

		if (nCount > 10) {
			DbgPrint ("%s - [0x%X] timed out looking for 0x10 [0x%X]\n", DRIVERNAME, m_pAddress, nRead);
			return STATUS_IO_TIMEOUT;
		}
	}
    
	// download configuration bytes 
	WriteBuffer (0x00, (PUCHAR)pCode, lBytes);
	DbgPrint ("%s - [0x%X] FPGA transfered\n", DRIVERNAME, m_pAddress);

/*
	// need 10 more clock cycles, one for each startup sequence [?]
	for (int nBytes = 0; nBytes < 10; nBytes++) {
		// write dummy byte 
		WriteByte (0x00, 0x00);
	}
*/
	nCount = 0;

	// 0001 0001 // 0x11
	// 0011 0000 // 0x30
	while ((nRead = ReadByte (nDoneReg) & 0x30) != 0x30) {
		KeStallExecutionProcessor (1); 
		nCount++;

		if (nCount > 100) {
			DbgPrint ("%s - [0x%X] timed out looking for 0x30 [0x%X]\n", DRIVERNAME, m_pAddress, nRead);
			return STATUS_IO_TIMEOUT;
		}
	}

	DbgPrint ( "%s - [0x%X] FPGA loaded", DRIVERNAME, m_pAddress);
	m_bFpgaLoaded = TRUE;

	SetInterruptType (INTERRUPT_PHOTOCELL);

	{
		m_bInitialPhotoState = IsPhotocellBlocked ();
		
		if (m_bInitialPhotoState)
			DbgPrint ("%s - [0x%X] photocell state: BLOCKED", DRIVERNAME, m_pAddress);
	}

	return Activate ();
}

bool CCard::IsPhotocellBlocked ()
{
	UCHAR nIO = ReadByte (IV_REG_READ_IO);

	return (nIO & (1 << PHOTOSTATE)) ? true : false;
}

NTSTATUS CCard::SetSerialParams (tagSerialParams * pParams)
{
	tagSerialParams params;

	RtlCopyMemory ((PVOID)&params, pParams, sizeof (tagSerialParams));

	if ((params.BaudRate < 300) || (params.BaudRate > 115200)) 
		return STATUS_INVALID_PARAMETER;
	if ((params.DataBits < 7) || (params.DataBits > 8))
		return STATUS_INVALID_PARAMETER;
	if ((params.Parity < NO_PARITY)|| (params.Parity > ODD_PARITY))
		return STATUS_INVALID_PARAMETER;
	if ((params.StopBits < 1) || (params.StopBits > 2))
		return STATUS_INVALID_PARAMETER;

//	unsigned short iBaud = (unsigned short) (115200 / params.BaudRate);
//	unsigned short iBaud = (unsigned short) (lClockSpeed / 16 / params.BaudRate);
	unsigned short iBaud = (unsigned short) (lClockSpeed / 256 / params.BaudRate);
	UCHAR cLCR = 0x03;

	switch (params.Parity) {
		default:
		case NO_PARITY:		
			cLCR &= 0xF7;	
			break;
		case EVEN_PARITY:	
			cLCR |= 0x18;	
			break;
		case ODD_PARITY:	
			cLCR &= 0xEF;	
			cLCR |= 0x08;
			break;
	}

	switch (params.StopBits) {
	default:
	case 1:
		cLCR &= 0xF7;
		break;
	case 2:
		cLCR |= 0x04;
		break;
	}

	switch (params.DataBits) {
	case 7:
		cLCR &= 0xFE;
		cLCR |= 0x02;
		break;
	default:
	case 8:
		cLCR |= 0x03;
		break;
	}

	DbgPrint (DRIVERNAME " - [0x%X] SetSerialParams: 0x%p, 0x%p", m_pAddress, iBaud, cLCR);

	if (m_bMappedPort) {
		WRITE_REGISTER_UCHAR (COMM_BASE_ADDR + LCR, cLCR | 0x80);
		WRITE_REGISTER_UCHAR (COMM_BASE_ADDR + DLL, (UCHAR) (iBaud & 0x00FF));
		WRITE_REGISTER_UCHAR (COMM_BASE_ADDR + DLH, (UCHAR) ((iBaud >> 8) & 0x00FF));
		WRITE_REGISTER_UCHAR (COMM_BASE_ADDR + LCR, cLCR & 0x7F);

		{
			UCHAR cDLL = READ_REGISTER_UCHAR (COMM_BASE_ADDR + DLL);
			UCHAR cDLH = READ_REGISTER_UCHAR (COMM_BASE_ADDR + DLH);

			DbgPrint (DRIVERNAME " - [0x%X] baud: DLL: 0x%p, DLH: 0x%p", m_pAddress, cDLL, cDLH);
		}

		READ_REGISTER_UCHAR (COMM_BASE_ADDR);
		READ_REGISTER_UCHAR (COMM_BASE_ADDR + MSR);				// Clear Pending Interrupts

//		WRITE_REGISTER_UCHAR (COMM_BASE_ADDR + IER, 0x0F); // Enable Interrupts
		WRITE_REGISTER_UCHAR (COMM_BASE_ADDR + IER, 0x01); // Enable Interrupts
		WRITE_REGISTER_UCHAR (COMM_BASE_ADDR + MCR, DTR);

		UCHAR cIIR = READ_REGISTER_UCHAR (COMM_BASE_ADDR + IIR);
		DbgPrint (DRIVERNAME " - [0x%X] READ_REGISTER_UCHAR (IIR): 0x%p", m_pAddress, cIIR);
	}
	else {
		WRITE_PORT_UCHAR (COMM_BASE_ADDR + LCR, cLCR | 0x80);
		WRITE_PORT_UCHAR (COMM_BASE_ADDR + DLL, (UCHAR) (iBaud & 0x00FF));
		WRITE_PORT_UCHAR (COMM_BASE_ADDR + DLH, (UCHAR) ((iBaud >> 8) & 0x00FF));
		WRITE_PORT_UCHAR (COMM_BASE_ADDR + LCR, cLCR & 0x7F);

		{
			UCHAR cDLL = READ_PORT_UCHAR (COMM_BASE_ADDR + DLL);
			UCHAR cDLH = READ_PORT_UCHAR (COMM_BASE_ADDR + DLH);

			DbgPrint (DRIVERNAME " - [0x%X] baud: DLL: 0x%p, DLH: 0x%p", m_pAddress, cDLL, cDLH);
		}

		READ_PORT_UCHAR (COMM_BASE_ADDR);
		READ_PORT_UCHAR (COMM_BASE_ADDR + MSR);				// Clear Pending Interrupts

//		WRITE_PORT_UCHAR (COMM_BASE_ADDR + IER, 0x0F); // Enable Interrupts
		WRITE_PORT_UCHAR (COMM_BASE_ADDR + IER, 0x01); // Enable Interrupts
		WRITE_PORT_UCHAR (COMM_BASE_ADDR + MCR, DTR);

		UCHAR cIIR = READ_PORT_UCHAR (COMM_BASE_ADDR + IIR);
		DbgPrint (DRIVERNAME " - [0x%X] READ_PORT_UCHAR (IIR): 0x%p", m_pAddress, cIIR);
	}

	return STATUS_SUCCESS;
}

void CCard::ProcessSerialData()
{

	if (m_bMappedPort) {	// Memory mapped addressing 
		* m_pSerialRxTail = READ_REGISTER_UCHAR (COMM_BASE_ADDR);
		READ_REGISTER_UCHAR (COMM_BASE_ADDR + MSR);				// Clear Pending Interrupts
	}
	else {
		* m_pSerialRxTail = READ_PORT_UCHAR (COMM_BASE_ADDR);
		READ_PORT_UCHAR (COMM_BASE_ADDR + MSR);				// Clear Pending Interrupts
	}

	DbgPrint (DRIVERNAME " - [0x%X] ProcessSerialData: '%c' (%d)", m_pAddress, * m_pSerialRxTail, * m_pSerialRxTail);

	if (* m_pSerialRxTail == '\r')
		m_bSerialDataReady = TRUE;

	if ((m_pSerialRxTail - m_pSerialRxBuffer) < SERIAL_BUFFER_SIZE)
		m_pSerialRxTail++;
}

void CCard::TxImage (USHORT wStartCol, USHORT wNumCols, UCHAR cBPC, UCHAR * pImage)
{
	const USHORT wWidthBits	= cBPC * 8;
	const USHORT wRowWords	= (wWidthBits / 16) + ((wWidthBits % 16) ? 1 : 0); // win bitmap is word aligned

	m_bImageReady = false;

	DbgPrint (DRIVERNAME " - [0x%X] TxImage: wStartCol: %d, wNumCols: %d, cBPC: %d", 
		m_pAddress, 
		wStartCol, 
		wNumCols, 
		cBPC);

//	if (m_pcBuffer)
//		RtlZeroMemory (m_pcBuffer, ::lImageBufferLen);

	if ((cBPC * wNumCols) > IMAGEBUFFERLEN) {
		wNumCols = (USHORT)(IMAGEBUFFERLEN / cBPC);

		DbgPrint (DRIVERNAME " - [0x%X] TxImage: image too long, truncated to %d columns", m_pAddress, wNumCols); 
	}

	if (PUCHAR p = GetBufferPtr ()) {
		ULONG lBufferSize	= (wRowWords * 2 * wNumCols);
		
		RtlZeroMemory (p, IMAGEBUFFERLEN);

		m_cBPC				= cBPC;
		m_wColumns			= wNumCols;
		m_wRowWords			= wRowWords;
		m_wCurrentCol		= 0;

		m_wShift1			= (0 << VFLIP);
		m_wShift2			= (1 << IV_BMP) | (0 << DRAFT) | (USHORT)wWidthBits;

		FlipBytes (&m_wShift1, sizeof (m_wShift1));
		FlipBytes (&m_wShift2, sizeof (m_wShift2));

		DbgPrint (DRIVERNAME " - [0x%X] wWidthBits: %d, lBufferSize: %d, m_cBPC: %d, m_wColumns: %d, m_wRowWords: %d",
			m_pAddress, 
			wWidthBits,
			lBufferSize,
			m_cBPC,
			m_wColumns,
			m_wRowWords);

		RtlCopyMemory (p, pImage, lBufferSize);
		m_bImageReady = true;
	}
}

NTSTATUS CCard::RegisterPhotoEvent( HANDLE hEvent )
{
	NTSTATUS status = STATUS_SUCCESS;

	if (hEvent) {
		if ( m_evtPhoto )
			ObDereferenceObject ( m_evtPhoto );

		PKEVENT pEvent;

		if ((status = ObReferenceObjectByHandle(hEvent, EVENT_MODIFY_STATE, *ExEventObjectType, KernelMode, (PVOID*)&pEvent, NULL)) == STATUS_SUCCESS) {
			m_evtPhoto = pEvent;
			DbgPrint (DRIVERNAME " - [0x%X] RegisterPhotoEvent: 0x%p [0x%p]", m_pAddress, hEvent, m_evtPhoto);
		}
	}

	return status;
}

NTSTATUS CCard::RegisterSerialEvent( HANDLE hEvent )
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - [0x%X] RegisterSerialEvent()", DRIVERNAME, m_pAddress);

	if (hEvent) {
		if ( m_evtSerial )
			ObDereferenceObject ( m_evtSerial );

		PKEVENT pEvent;
		if ( (status = ObReferenceObjectByHandle(hEvent, EVENT_MODIFY_STATE, *ExEventObjectType, KernelMode, (PVOID*)&pEvent, NULL)) == STATUS_SUCCESS)
			m_evtSerial = pEvent;
		else
			DbgPrint ("%s - [0x%X] RegisterSerialEvent() FAILED\n", DRIVERNAME, m_pAddress);
	}
	else
		DbgPrint ("%s - [0x%X] RegisterSerialEvent() passed NULL handle\n", DRIVERNAME, m_pAddress);
	return status;
}

NTSTATUS CCard::UnregisterPhotoEvent ()
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - [0x%X] UnregisterPhotoEvent", DRIVERNAME, m_pAddress);

//	ReadByte ( IIR );				// Clear Interrupt Identification Register
//	ReadByte ( MSR);				// Clear Pending Interrupts;

	if (m_evtPhoto)
		ObDereferenceObject (m_evtPhoto);

	m_evtPhoto = NULL;
	
	return status;
}

NTSTATUS CCard::UnregisterSerialEvent( )
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - [0x%X] UnregisterSerialEvent", DRIVERNAME, m_pAddress);

/*
	WriteByte ( IER, 0x00 );	// Disable Interrupts
	ReadByte ( IIR );				// Clear Interrupt Identification Register
	ReadByte ( MSR);				// Clear Pending Interrupts;
*/
	if ( m_evtSerial )
		ObDereferenceObject ( m_evtSerial );

	m_evtSerial = NULL;
//	WriteByte ( IER, 0x0F );	// Enable Interrupts
	
	return status;
}

BOOLEAN CCard::SetEvent (PKEVENT pEvent)
{
	BOOLEAN bResult = FALSE;

	if (pEvent) {
		LONG lSetEvent = -1;
		
		if ((lSetEvent = KeSetEvent (pEvent, HIGH_PRIORITY, FALSE)) == 0)
			bResult = TRUE;
		/*
		else {
			KeClearEvent (pEvent);
			DbgPrint (DRIVERNAME " - [0x%X] KeClearEvent: 0x%p (%d)", m_pAddress, pEvent, lSetEvent);
		}
		*/
	}

	return bResult;
}

void CCard::FirePhotoEvent ()
{
	BOOLEAN bSet = SetEvent (m_evtPhoto);
	DbgPrint (DRIVERNAME " - [0x%X] FirePhotoEvent: 0x%p [%d] %s", m_pAddress, m_evtPhoto, m_wColumns, bSet ? "" : "[SetEvent failed]");
}

void CCard::SetInterruptMode(BOOLEAN bMaster)
{
	m_bMaster = bMaster;

	if (m_bMaster) {
		if (IsPresent ())
			DbgPrint ("%s - [0x%X] Set to MASTER on IRQ 0x%x", DRIVERNAME, m_pAddress, m_cPhysicalIrq);
	}
	else {
		if (IsPresent ())
			DbgPrint ("%s - [0x%X] Set to SLAVE on IRQ 0x%x", DRIVERNAME, m_pAddress, m_cOTBIrqId);
	}
}


unsigned long CCard::GetIOAddress()
{
	return (unsigned long) m_pAddress;
}

void CCard::EnableInterrupts (bool bEnable)
{
	UCHAR nData		= ReadByte (IV_REG_PRINT_STATUS);
	UCHAR nEnable	= 0x04;		// 0000 0100
	UCHAR nMask		= 0xFB;		// 1111 1011
	
	if (bEnable)
		WriteByte (IV_REG_PRINT_STATUS, nData | nEnable); 
	else
		WriteByte (IV_REG_PRINT_STATUS, nData & nMask); 
}

NTSTATUS CCard::Activate()
{
	GetBufferPtr ();

	// default serial params
	tagSerialParams p;
	p.BaudRate	= 9600;
	p.DataBits	= 8;
	p.Parity	= 0;
	p.StopBits	= 1;
	SetSerialParams (&p);

	m_bSerialDataReady = FALSE;

	return STATUS_SUCCESS;
}

BOOLEAN CCard::IsPresent()
{
	return m_bPresent;
}

void CCard::AssignAddress(PUCHAR Addr)
{
	m_pAddress = Addr;

	const UCHAR nIndex = IV_REG_BASE;
    UCHAR nValue = ReadByte (nIndex);

	DbgPrint (DRIVERNAME " - [0x%X] CCard::AssignAddress: ReadByte (0x%X): 0x%X", m_pAddress, nIndex, nValue);

	m_lPhotocellCount = 0;

    if (nValue == 0xFF) { 
		m_bPresent = FALSE;
		DbgPrint ( "%s - [0x%X] Card not found", DRIVERNAME, m_pAddress);
	}
	else {
		m_bPresent = TRUE;
		DbgPrint ( "%s - [0x%X] Card at found [0x%X]", DRIVERNAME, m_pAddress, nValue);
	}
}

void CCard::Destroy()
{
	// We call this manually just before deallocating the object;

	EnableInterrupts (false); //WriteByte ( IER, 0x00 );   // Disable Interrupts

	UnregisterPhotoEvent ();
	UnregisterEOPEvent ();

	FreeBuffers ();
}


NTSTATUS CCard::QueueSerialData(PUCHAR pData, ULONG uLen)
{
   NTSTATUS status = STATUS_SUCCESS;
   return status;
}

void CCard::ClearOnBoardRam()
{
	DbgPrint ( "%s - [0x%X] Clearing On Board RAM", DRIVERNAME, m_pAddress);

	m_pwColumn		= NULL;
	m_bImageReady	= false;
	m_wCurrentCol	= 0;

	m_pSerialRxTail = m_pSerialRxHead = m_pSerialRxBuffer;

    // set CW to 1 or 100 dpi 
    const UCHAR nDPI = ReadByte (IV_REG_DPI_IRQ);
    
	WriteByte (IV_REG_DPI_IRQ, 1);

    const UCHAR nStatus = ReadByte (IV_REG_PRINT_STATUS);
    
	{ // pause  
		UCHAR n = (UCHAR)(ReadByte (IV_REG_PRINT_STATUS) & (~(1 << UNPAUSE)));
		WriteByte (IV_REG_PRINT_STATUS, n);
	}    
	
	{// irq = column 
		UCHAR n = (UCHAR)((ReadByte(IV_REG_PRINT_STATUS) & (~(1 << IRQ_SEL))) | (COLUMN_IRQ << IRQ_SEL));
		WriteByte (IV_REG_PRINT_STATUS, n);
    }

	{ // toggle print reset       
		UCHAR n;
		
		n = (UCHAR)(ReadByte (IV_REG_PRINT_STATUS) & (~(1 << RESET)));
		WriteByte (IV_REG_PRINT_STATUS, n);

		n = (UCHAR)(ReadByte (IV_REG_PRINT_STATUS) | (1 << RESET));
		WriteByte (IV_REG_PRINT_STATUS, n);

		n = (UCHAR)(ReadByte (IV_REG_PRINT_STATUS) & (~(1 << RESET)));
		WriteByte (IV_REG_PRINT_STATUS, n);
	}

    // write blank columns to ram data port
    for (int nCols=0; nCols < BUFFERCOLUMNS; nCols++){
		UCHAR n;

		n = (UCHAR)(ReadByte (IV_REG_PRINT_STATUS) | (1 << INC_COL_CNT));
        WriteByte (IV_REG_PRINT_STATUS, n);
        ReadByte (IV_REG_READ_IO);					//clear COL_RESET

		n = (UCHAR)(ReadByte (IV_REG_PRINT_STATUS) & (~(1 << INC_COL_CNT)));
        WriteByte (IV_REG_PRINT_STATUS, n);

        OnEndOfColumn ();
    }

    WriteByte (IV_REG_PRINT_STATUS, nStatus);
    WriteByte (IV_REG_DPI_IRQ, nDPI);

	OnEndOfColumn ();
}

void CCard::OnEndOfColumn ()
{
    WriteWord (IV_REG_SHIFT_CNTL,		0xFF01);	// dummy start dot is 511 
    WriteWord (IV_REG_SHIFT_CNTL + 2,	0x0100);	// dummy height is 1 
}

int CCard::GetSerialData(PUCHAR pData)
{
	int count = m_pSerialRxTail - m_pSerialRxBuffer;
	RtlZeroMemory ( pData, count + 1 );
	RtlCopyMemory ( pData, m_pSerialRxBuffer, count);
	m_pSerialRxTail = m_pSerialRxBuffer;
	DbgPrint ("%s - [0x%X] GetSerialData() Count 0x%X Data %s\n", DRIVERNAME, m_pAddress, count, pData);
	return count;
}

void CCard::FireSerialEvent()
{
	BOOLEAN bSet = SetEvent (m_evtSerial);
	m_bSerialDataReady = FALSE;
	DbgPrint (DRIVERNAME " - [0x%X] FireSerialEvent: 0x%p %s", m_pAddress, m_evtSerial, bSet ? "" : "[SetEvent failed]");
}

NTSTATUS CCard::GetState (IVCARDSTATESTRUCT * pState)
{
	if (pState) {
		{ // line speed
			USHORT nVal = ReadWord (IV_REG_LINE_SPEED);
			
			ULONG lScalar = nVal;
			
			nVal = ReadWord (IV_REG_LINE_SPEED + 2);

			nVal &= 0x000F;
			lScalar += nVal * 0x10000;
			lScalar = (lScalar) * 20;
			
			if (lScalar != 0) 
				pState->m_lLineSpeed = (USHORT)(lClockSpeed / lScalar);
			else
				pState->m_lLineSpeed = 0;
		}

		const UCHAR nReadIO = ReadByte (IV_REG_READ_IO);

		/* READ_IO
		 * x x x x  x x x x
		 * | | | |  | | | |-> photocell						// 0000 0001	0x0001
		 * | | | |  | | |---> ink out (PEL)					// 0000 0010	0x0002	// not used
		 * | | | |  | |-----> waste full (PEL)				// 0000 0100	0x0004
		 * | | | |  |-------> ink low (PEL)					// 0000 1000	0x0008	// not used
		 * | | | |----------> at temp (PEL)					// 0001 0000	0x0010
		 * | | |------------> voltage error (I.V. & PEL)	// 0010 0000	0x0020
		 * | |--------------> vacuum (PEL)					// 0100 0000	0x0040
		 * |----------------> pump(PEL)/inklow(WAX)			// 1000 0000	0x0080	// not used
		 */

		pState->m_bProdDetect	= ((nReadIO & 0x01));
		pState->m_bPELWaste		= ((nReadIO & 0x04) == 0x04) ? 0 : 1;
		pState->m_bPELTemp		= ((nReadIO & 0x10) == 0x10);
		pState->m_bPowerError	= ((nReadIO & 0x20) == 0x20) ? 0 : 1;
		pState->m_bPELVacuumOn	= ((nReadIO & 0x40) == 0x40);
	}

	return STATUS_SUCCESS;
}

void CCard::FlipBytes (PUSHORT lpData, ULONG lSize)
{
	PUCHAR p = (PUCHAR)lpData;
	ULONG i, j;
	UCHAR tmp;

	for (i=0, j=lSize-1; i<j; i++, j--) {
		tmp = p [i];
		p [i] = p [j];
		p [j] = tmp;
	}
}


NTSTATUS CCard::SetConfig (IVCARDCONFIGSTRUCT * pConfig)
{
//	INTERRUPT_TYPE oldINT = GetInterruptType ();

	if (pConfig) {
		m_bEnabled				= pConfig->m_bEnabled;
		m_cEncoderDivisor		= pConfig->m_nEncoderDivisor;
		m_bInternalPhotocell	= pConfig->m_bInternalPhotocell;
		m_lAutoPrintCols		= pConfig->m_lAutoPrintCols;
		m_bSharedPhotocell		= pConfig->m_bSharedPhotocell;
		m_bDPC					= pConfig->m_bDPC;

		{ // dbg info
			const LPCTSTR lpszType [] =
			{
				"IV_12_9",
				"IV_78_9",
				"IV_10_18",
				"IV_20_18",
				"IV_NONE",
			};

			DbgPrint (DRIVERNAME " - [0x%X] m_lAddress: 0x%X",			m_pAddress, pConfig->m_lAddress); 
			DbgPrint (DRIVERNAME " - [0x%X] m_lLineSpeed: %d",			m_pAddress, pConfig->m_lLineSpeed); 
			DbgPrint (DRIVERNAME " - [0x%X] m_bSharedPhotocell: %s",	m_pAddress, pConfig->m_bSharedPhotocell		? "true" : "false"); 
			DbgPrint (DRIVERNAME " - [0x%X] m_bEnabled: %s",			m_pAddress, pConfig->m_bEnabled				? "true" : "false"); 
			DbgPrint (DRIVERNAME " - [0x%X] m_nEncoderDivisor: %d",		m_pAddress, pConfig->m_nEncoderDivisor); 
			DbgPrint (DRIVERNAME " - [0x%X] m_bExternalEncoder: %s",	m_pAddress, pConfig->m_bExternalEncoder		? "true" : "false"); 
			DbgPrint (DRIVERNAME " - [0x%X] m_bSharedEncoder: %s",		m_pAddress, pConfig->m_bSharedEncoder		? "true" : "false"); 
			DbgPrint (DRIVERNAME " - [0x%X] m_bInternalPhotocell: %s",	m_pAddress, pConfig->m_bInternalPhotocell	? "true" : "false"); 
			DbgPrint (DRIVERNAME " - [0x%X] m_lAutoPrintCols: %d",		m_pAddress, pConfig->m_lAutoPrintCols); 
			DbgPrint (DRIVERNAME " - [0x%X] m_bDPC: %d",				m_pAddress, pConfig->m_bDPC); 

			for (int i = 0; i < ARRAYSIZE (pConfig->m_heads); i++) {
				int nIndex = pConfig->m_heads [i].m_nType;

				if (nIndex < FoxjetDatabase::IV_12_9 || nIndex > FoxjetDatabase::IV_20_18)
					nIndex = ARRAYSIZE (lpszType) - 1;

				DbgPrint (DRIVERNAME " - [0x%X] [%d] m_nType: %s",				m_pAddress, i, lpszType [nIndex]); 
				DbgPrint (DRIVERNAME " - [0x%X] [%d] m_lPhotocellDelay: %d",	m_pAddress, i, pConfig->m_heads [i].m_lPhotocellDelay); 
				DbgPrint (DRIVERNAME " - [0x%X] [%d] m_bReversed: %s",			m_pAddress, i, pConfig->m_heads [i].m_bReversed ? "true" : "false"); 
			}
		}

		// Page 0 
		WriteByte (IV_REG_CMD, 0x00);
		
		{ // toggle print reset 
			UCHAR n;

			n = (UCHAR)(ReadByte (IV_REG_PRINT_STATUS) & (~(1 << RESET)));
			WriteByte (IV_REG_PRINT_STATUS, n);

			n = (UCHAR)(ReadByte (IV_REG_PRINT_STATUS) | (1 << RESET));
			WriteByte (IV_REG_PRINT_STATUS, n);

			n = (UCHAR)(ReadByte (IV_REG_PRINT_STATUS) & (~(1 << RESET)));
			WriteByte (IV_REG_PRINT_STATUS, n);
		}
		
		if (!pConfig->m_bExternalEncoder) { // internal encoder

			{ // line speed
				ULONG lScalar = 0;
				
				if (pConfig->m_lLineSpeed)
					lScalar = lClockSpeed / (pConfig->m_lLineSpeed * 20 * 6); // see IV_REG_DIV_ENCODER_BY

				USHORT nVal = (USHORT)(lScalar & 0x0000FFFF);
				WriteWord (IV_REG_LINE_SPEED, nVal);
				nVal = ReadWord(IV_REG_LINE_SPEED);

				nVal = (USHORT)(lScalar >> 16);
				WriteWord (IV_REG_LINE_SPEED + 2, nVal);
				nVal=ReadWord (IV_REG_LINE_SPEED + 2);
			}

			UCHAR n = (UCHAR)((ReadByte (IV_REG_PRINT_STATUS) & (~(1 << ENCODER_SEL))) | (INTERNAL << ENCODER_SEL));
            WriteByte (IV_REG_PRINT_STATUS, n);
		}
		else { // external encoder
			UCHAR n = (UCHAR)((ReadByte (IV_REG_PRINT_STATUS) & (~(1 << ENCODER_SEL))) | (EXTERNAL << ENCODER_SEL));
            WriteByte (IV_REG_PRINT_STATUS, n);
		}

		{ // StorePHConfig
			USHORT nType = 0;
			int nSize = 0, nSector = 0;

			for (int i = 0; i < ARRAYSIZE (pConfig->m_heads); i++) {
				switch (pConfig->m_heads [i].m_nType) {
				case FoxjetDatabase::IV_12_9:
				case FoxjetDatabase::IV_78_9:
					if (pConfig->m_heads [i].m_bReversed) {
						nType = (nType << 1) | 0x0001;
						SetOffset (nSector++, pConfig->m_heads [i].m_lPhotocellDelay + TICKS_IV);
						DbgOffset (pConfig->m_heads [i], i);
					} 
					else {
						nType = (nType << 1) | 0x0000;
						SetOffset (nSector++, pConfig->m_heads [i].m_lPhotocellDelay);
						DbgOffset (pConfig->m_heads [i], i);
					}
					
					nSize += 1;
					break;
				case FoxjetDatabase::IV_10_18:
				case FoxjetDatabase::IV_20_18:
					if (pConfig->m_heads [i].m_bReversed) {
						nType = (nType << 1) | 0x0001;
						SetOffset (nSector++, pConfig->m_heads [i].m_lPhotocellDelay + TICKS_IV);
						nType = (nType << 1) | 0x0000;
						SetOffset (nSector++, pConfig->m_heads [i].m_lPhotocellDelay);
						DbgOffset (pConfig->m_heads [i], i);
					} 
					else {
						nType = (nType << 1) | 0x0000;
						SetOffset (nSector++, pConfig->m_heads [i].m_lPhotocellDelay);
						nType = (nType << 1) | 0x0001;
						SetOffset (nSector++, pConfig->m_heads [i].m_lPhotocellDelay + TICKS_IV);
						DbgOffset (pConfig->m_heads [i], i);
					}

					nSize += 2;
					break;
				}
			}

			if (8 > nSize) 
				nType = nType << (8 - nSize);

			FlipBytes (&nType, sizeof (nType));
			WriteWord (IV_REG_HEAD_TYPES, nType);
		}

        // set encoder ticks per column 
        WriteByte (IV_REG_DIV_ENCODER_BY, 6);

		{ // photocell/encoder sharing
			UCHAR n = ReadByte (IV_REG_PRINT_STATUS);

			if (pConfig->m_bSharedPhotocell)
				n |= (1 << PHOTO_SHARING);
			else
				n &= ~(1 << PHOTO_SHARING);
			
			if (pConfig->m_bSharedEncoder) 
				n |= (1 << ENCODER_SHARING);
			else
				n &= ~(1 << ENCODER_SHARING);

			WriteByte (IV_REG_PRINT_STATUS, n);
		}

        { // unpause printing 
			UCHAR n = (UCHAR)(ReadByte (IV_REG_PRINT_STATUS) | (1 << UNPAUSE));
	        WriteByte (IV_REG_PRINT_STATUS, n);
		}

		/*
		{ // irq = photocell 
			UCHAR n = (UCHAR)((ReadByte (IV_REG_PRINT_STATUS) & (~(1 << IRQ_SEL))) | (PHOTO_IRQ << IRQ_SEL));
			WriteByte (IV_REG_PRINT_STATUS, n);
			WriteByte (IV_REG_DPI_IRQ, 1);
			//DbgPrint (DRIVERNAME " - [0x%X] irq = photocell", m_pAddress);
		}
		*/

		WriteByte (IV_REG_CMD, (1 << IRQ_FLAG)); // clear irq flag 
	}

//	SetInterruptType (oldINT);

	return STATUS_SUCCESS;
}

void CCard::DbgOffset (const IVHEADCONFIGSTRUCT & head, int nIndex)
{
	int nChannels = 0;
	int nHeight = 0;

	switch (head.m_nType) {
	case FoxjetDatabase::IV_12_9:	nChannels = 9;	nHeight = 500;	break;
	case FoxjetDatabase::IV_78_9:	nChannels = 9;	nHeight = 875;	break;
	case FoxjetDatabase::IV_10_18:	nChannels = 18;	nHeight = 1000;	break;
	case FoxjetDatabase::IV_20_18:	nChannels = 18;	nHeight = 2000;	break;
	}

	DbgPrint (DRIVERNAME " - [0x%X] SetOffset: [%d] %d, %d channels, %d \" %s", 
		m_pAddress,
		nIndex,
		head.m_lPhotocellDelay,
		nChannels, 
		nHeight,
		head.m_bReversed ? "[reversed]" : "");
}

void CCard::SetOffset (int nIndex, USHORT nVal)
{
	//FlipBytes (&nVal, sizeof (nVal));
	
	WriteByte (IV_REG_CMD, 0x01); // Page 1
	WriteWord (IV_REG_PH_OFFSETS + (2 * nIndex), nVal);
	WriteByte (IV_REG_CMD, 0x00); // Page 0 
}

BOOLEAN CCard::OnPhotocell ()
{
	m_lInterrupt = 0;

	SetInterruptType (INTERRUPT_ENCODER);
	SetPause (true);

	m_pwColumn = (PUSHORT)GetBufferPtr ();

	// set DPI of image 
	//WriteByte (IV_REG_DPI_IRQ, 5); // 100dpi / 5 = 20dpi 
	WriteByte (IV_REG_DPI_IRQ, m_cEncoderDivisor);
	
	m_lPhotocellCount++;
	FirePhotoEvent ();

	return TRUE; 
}

BOOLEAN CCard::OnEncoderPulse ()
{
	if (PUCHAR p = (PUCHAR)m_pwColumn) {
		CHAR szDebug [512];
		int nDebugIndex = 0;
		PUCHAR pTest [2] = 
		{	
			(PUCHAR)m_pwColumn,
			(PUCHAR)m_pwColumn + m_wRowWords,
		};

		if (pTest [0] < m_pcBuffer || pTest [1] >= (m_pcBuffer + IMAGEBUFFERLEN)) {
			DbgPrint (DRIVERNAME " - [0x%X] **** out of bounds *****: 0x%p [0x%p - 0x%p]", 
				m_pAddress,
				p,
				m_pcBuffer,
				m_pcBuffer + IMAGEBUFFERLEN);
			return FALSE;
		}
		
		if (m_bDebug) {
			for (int i = 0; i < ARRAYSIZE (szDebug); i++)
				szDebug [i] = ' ';
		}

		WriteWord (IV_REG_SHIFT_CNTL,		m_wShift1);	// start at dot 0 
		WriteWord (IV_REG_SHIFT_CNTL + 2,	m_wShift2);	// column width (pixels)

		for (int i = 0; i < m_cBPC; i += 2) {
			USHORT w = * (PUSHORT)&p [i];
			
			if (m_bDebug) {
				USHORT wTmp = w;
				FlipBytes (&wTmp, sizeof (wTmp));
				Format (wTmp, &szDebug [nDebugIndex], m_bPhotoDetect); nDebugIndex += 17;
			}
 
			USHORT wReg = IV_REG_DATA_PORT + ((i % 4) ? 2 : 0);
			WriteWord (wReg, w);

			if (wReg == (IV_REG_DATA_PORT + 2))
				KeStallExecutionProcessor (2);
		}

		if (m_cBPC % 2) {
			if (m_bDebug) {
				Format (0x0000, &szDebug [nDebugIndex], m_bPhotoDetect); nDebugIndex += 17;
			}

			WriteWord (IV_REG_DATA_PORT + 2, 0x0000);
			KeStallExecutionProcessor (2);
		}

		if (m_bDebug) {
			szDebug [nDebugIndex] = 0; 
			DbgPrint (DRIVERNAME " - [0x%X] %04d (%04d): %s%s%s", 
				m_pAddress, 
				m_wCurrentCol,
				m_lInterrupt,
				m_wCurrentCol == m_lInterrupt ? "" : "[E] ",
				IsDPC () ? "" : "--> ",
				szDebug);
		}

		m_pwColumn += m_wRowWords;
	}

	return TRUE;
}

BOOLEAN CCard::OnEncoderPulseBlank ()
{
	if (m_bDebug) 
		DbgPrint (DRIVERNAME " - [0x%X] %04d: ", m_pAddress, m_lEncoderTicks);

	return TRUE;
}

BOOLEAN CCard::OnEndOfImage ()
{
	m_pwColumn		= NULL;

	FireEOPEvent ();

	if (m_bPauseAtEOP) {
		SetPause (true);
		m_bPauseAtEOP = FALSE;
	}

	SetInterruptType (INTERRUPT_PHOTOCELL);
	WriteByte (IV_REG_DPI_IRQ, 1); // set dpi to max 

	return TRUE;
}

void CCard::SetAutoPrint(bool bAutoPrint)
{
	if (m_bInternalPhotocell)
		m_bAutoPrint = bAutoPrint;
	else
		m_bAutoPrint = false;
	
	DbgPrint (DRIVERNAME " - [0x%X] SetAutoPrint: %s [%s]", m_pAddress, 
		bAutoPrint ? "true" : "false", 
		m_bAutoPrint ? "true" : "false");
}

BOOLEAN CCard::OnInterrupt()
{
	/*
	if ((m_lInterrupt % (300 * 1)) == 0) {
		DbgPrint (DRIVERNAME " - [0x%X] OnInterrupt: [%d] %s%s%s", m_pAddress, m_lInterrupt,
			GetInterruptType () == INTERRUPT_PHOTOCELL ? "PC " : "ENC",
			IsPaused ()			? " IsPaused" : "",
			!IsImageReady ()	? " !IsImageReady" : "");
	}
	*/

	if (GetInterruptType () == INTERRUPT_PHOTOCELL) {
		if (m_bInternalPhotocell) {
			SetAutoPrint (true);
			m_lEncoderTicks = 0;
		}

		OnPhotocell ();
		return TRUE;
	}

	if (KeGetCurrentIrql () < DISPATCH_LEVEL) {
		KIRQL irql = KeGetCurrentIrql ();

		DbgPrint (DRIVERNAME " - [0x%X] KeGetCurrentIrql [%d] < DISPATCH_LEVEL", m_pAddress, irql);
		
		return FALSE;
	}

	if (IsPaused ()) {
		OnEndOfColumn ();
		return TRUE;
	}

	if (IsImageReady ()) {
		if (m_bInternalPhotocell && GetAutoPrint ()) {
			if (!m_bPhotoDetect) 
				SetAutoPrint (false);

			if (m_lEncoderTicks++ < m_lAutoPrintCols) {
				if (m_wCurrentCol++ < m_wColumns) 
					OnEncoderPulse ();
				else 
					OnEncoderPulseBlank ();
			}
			else {
				m_lEncoderTicks = 0;

				if (m_wCurrentCol >= m_wColumns) {
					m_wCurrentCol	= 0;

					if (GetAutoPrint ()) {
						FireEOPEvent ();
						OnPhotocell ();
					}
				}
			}
		}
		else {
			if (GetAutoPrint ()) 
				SetAutoPrint (false);

			if (m_wCurrentCol++ < m_wColumns) 
				OnEncoderPulse ();
			else {
				OnEndOfImage ();
				m_wCurrentCol = 0;
			}
		}
	}

	OnEndOfColumn ();

	return TRUE;
}

NTSTATUS CCard::ReadRegister (IVREGISTERSTRUCT * p)
{
	if (p) {
		switch (p->m_lSize) {
		case sizeof (UCHAR):	p->m_lData = ReadByte (p->m_nRegID);	break;
		case sizeof (USHORT):	p->m_lData = ReadWord (p->m_nRegID);	break;
		case sizeof (ULONG):	p->m_lData = ReadLong (p->m_nRegID);	break;
		}
	}

	return STATUS_SUCCESS;
}

NTSTATUS CCard::WriteRegister (IVREGISTERSTRUCT * p)
{
	if (p) {
		switch (p->m_lSize) {
		case sizeof (UCHAR):	WriteByte (p->m_nRegID, (UCHAR)p->m_lData);		break;
		case sizeof (USHORT):	WriteWord (p->m_nRegID, (USHORT)p->m_lData);	break;
		case sizeof (ULONG):	WriteLong (p->m_nRegID, p->m_lData);			break;
		}
	}

	return STATUS_SUCCESS;
}

void CCard::SetInterruptType (INTERRUPT_TYPE type)
{
	UCHAR n = ReadByte (IV_REG_PRINT_STATUS);

	//m_interruptType = type;

	if (type == INTERRUPT_PHOTOCELL) 
		n = (1 << IRQ_SEL) | n;
	else 
		n = ~(1 << IRQ_SEL) & n;

	ClearIRQ ();
	WriteByte (IV_REG_PRINT_STATUS, n);

	DbgPrint (DRIVERNAME " - [0x%X] SetInterruptType: %s", 
		  m_pAddress,
		  (type == INTERRUPT_PHOTOCELL ? "PHOTOCELL" : "ENCODER"));
}

CCard::INTERRUPT_TYPE CCard::GetInterruptType ()  
{ 
	UCHAR n = ReadByte (IV_REG_PRINT_STATUS);
	bool bPCInt = (n & (1 << IRQ_SEL)) & (PHOTO_IRQ << IRQ_SEL) ? true : false;

	return bPCInt ? INTERRUPT_PHOTOCELL : INTERRUPT_ENCODER;
/*
	bool bBug = (m_interruptType == INTERRUPT_PHOTOCELL) ? !bPCInt : bPCInt;

	if (bBug) {
		DbgPrint (DRIVERNAME " - [0x%X] GetInterruptType: %s [%s] ******************************", 
			  m_pAddress,
			  (m_interruptType == INTERRUPT_PHOTOCELL	? "PC" : "ENC"),
			  (bPCInt									? "PC" : "ENC"));
	}

	return m_interruptType; 
*/
}

NTSTATUS CCard::SetStrobe (int nColor)
{
	const int nRed		= 5;
	const int nGreen	= 6;
	
	m_cCmdReg &= ~((1 << nRed) | (1 << nGreen)); //STROBE_LIGHT_OFF

	if (nColor == RED_STROBE) 
		m_cCmdReg |= (1 << nRed); 

	if (nColor == GREEN_STROBE) 
		m_cCmdReg |= (1 << nGreen); 

	if (m_nStrobeColor != nColor) {
		UCHAR n = (UCHAR)(m_cCmdReg | (1 << IRQ_FLAG)); 
		/* TODO: rem
		LPCTSTR lpsz [] = 
		{
			"STROBE_LIGHT_OFF",
			"RED_STROBE",
			"GREEN_STROBE",
		};
		
		DbgPrint (DRIVERNAME " - [0x%X] SetStrobe: %s [0x%02X]",
			m_pAddress,
			lpsz [nColor],
			n);
		*/

		// NOTE: always clear irq flag when writing to cmd reg
		WriteByte (IV_REG_CMD, n);
	}

	m_nStrobeColor = nColor;

	return STATUS_SUCCESS;
}

void CCard::ClearIRQ ()
{
	UCHAR n = m_cCmdReg;

	n |= (1 << IRQ_FLAG); // clear irq flag 

	// NOTE: always clear irq flag when writing to cmd reg
	WriteByte (IV_REG_CMD, n); 
}

NTSTATUS CCard::RegisterEOPEvent (HANDLE hEvent)
{
	NTSTATUS status = STATUS_SUCCESS;

	if (hEvent) {
		if (m_evtEOP)
			ObDereferenceObject (m_evtEOP);

		PKEVENT pEvent;

		if ((status = ObReferenceObjectByHandle(hEvent, EVENT_MODIFY_STATE, *ExEventObjectType, KernelMode, (PVOID*)&pEvent, NULL)) == STATUS_SUCCESS) {
			m_evtEOP = pEvent;
			DbgPrint (DRIVERNAME " - [0x%X] RegisterEOPEvent: 0x%p [0x%p]", m_pAddress, hEvent, m_evtEOP);
		}
	}

	return status;
}

NTSTATUS CCard::UnregisterEOPEvent ()
{
	DbgPrint (DRIVERNAME " - [0x%X] UnregisterEOPEvent", m_pAddress);

	if (m_evtEOP)
		ObDereferenceObject (m_evtEOP);

	m_evtEOP = NULL;
	
	return STATUS_SUCCESS;
}

void CCard::FireEOPEvent ()
{
	BOOLEAN bSet = SetEvent (m_evtEOP);
	DbgPrint (DRIVERNAME " - [0x%X] FireEOPEvent: 0x%p %s", m_pAddress, m_evtEOP, bSet ? "" : "[SetEvent failed]");
}

void CCard::SetPause (bool bPause)
{ 

	//DbgPrint (DRIVERNAME " - [0x%X] SetPause: %s", m_pAddress, bPause ? "true" : "false");

	m_bPause = bPause;

	/* true hardware pause prevents reading encoder speed
	UCHAR n = ReadByte (IV_REG_PRINT_STATUS);

	if (bPause)
		n &= ~(1 << UNPAUSE);
	else 
		n |= (1 << UNPAUSE);

	WriteByte (IV_REG_PRINT_STATUS, n);
	*/
}    

NTSTATUS CCard::EnablePrint (bool bEnable)
{
	GetBufferPtr ();

	if (bEnable) 
		SetPause (false);
	else {
		if (GetInterruptType () == INTERRUPT_PHOTOCELL)
			SetPause (true);
		else
			m_bPauseAtEOP = TRUE;
	}

	m_bPrintEnabled = bEnable;
	DbgPrint (DRIVERNAME " - [0x%X] EnablePrint: %s", m_pAddress, bEnable ? "true" : "false");

	if (m_bPrintEnabled) {
		if (m_bInternalPhotocell) {
			if (m_bInitialPhotoState && IsPhotocellBlocked ()) {
				DbgPrint ("%s - [0x%X] photocell state: BLOCKED --> begin first image", DRIVERNAME, m_pAddress);

				m_bInitialPhotoState = false;
				SetAutoPrint (true);
				m_lEncoderTicks = 0;
				OnPhotocell ();
			}
		}
	}

	return STATUS_SUCCESS;
}

NTSTATUS CCard::Abort ()
{
	m_wCurrentCol = 0;
	OnEndOfImage ();

	return STATUS_SUCCESS;
}
