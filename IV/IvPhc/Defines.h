#ifndef	DEFINES_H
#define	DEFINES_H

#define MAJOR						0x01
#define MINOR						0x00
#define REV							0x01

#define SERIAL_BUFFER_SIZE		1024
//#define IMAGE_BUFFER_SIZE		1024 * 64

#define WRITE_MODE				0
#define READ_MODE					1

#define CMD_REG					0x00
#define IER							0x01
#define IIR							0x02
#define LCR							0x03
#define MCR							0x04
#define LSR							0x05
#define MSR							0x06
#define OTB_STATUS				SCRATCH_PAD
#define DLAB						0x83
#define DLL							0x00
#define DLH							0x01

#define DTR							0x01
#define RTS							0x02
										
#define LCR_8_MARK_2				0x2F	// Programming Mode
#define LCR_8_SPACE_2			0x3F	// Command Mode.
#define LCR_7_SPACE_2			0x3E	// Non Vol .

#define LCR_6_SPACE_1			0x29	
#define LCR_6_SPACE_2			0x3D	// Data Transfer Mode Read
#define LCR_6_MARK_2				0x2D	// Data Transfer mode Write.

#define LCR_8_NONE_1				0x03	// Normal Operation mode.

#define PC_INT					0x08
#define AMS_STATE_CHANGE		0x04
#define AMS_STATE_ACTIVE		0x40
#define PRINT_CYCLE_STATE		0x02	// cgh04d07
#define PRINT_CYCLE_ACTIVE		0x20	
#define OTB_INT					0x01

#define RX_CHAR					0x01
#define RX_OVERRUN				0x02
#define PARITY_ERROR				0x04
#define FRAMING_ERROR			0x08
#define BREAK						0x10
#define TBE							0x20
#define TXE							0x40

#define COMM_BASE_ADDR			((PUCHAR)0x340)

//#define MASTER						0x01
//#define SLAVE						0x00
#endif