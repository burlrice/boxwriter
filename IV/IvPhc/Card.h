// Card.h: interface for the CCard class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_Card_H__B87F81C6_C93A_4A60_B610_EC4E7C10790C__INCLUDED_)
#define AFX_Card_H__B87F81C6_C93A_4A60_B610_EC4E7C10790C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Defines.h"
#include "..\..\FxMphc\src\TypeDefs.h"
#include "IvRegs.h"

#define ARRAYSIZE(s)	(sizeof ((s)) / sizeof ((s) [0]))
#define BUFFERWIDTH		(72)
#define BUFFERCOLUMNS	(0x8000)

using namespace ISA;

class CCard  
{
public:
	CCard();
	~CCard();

protected:

	BOOLEAN m_bPresent;
	PUCHAR	m_pAddress;
	PUCHAR	m_pSerialRxBuffer;
	PUCHAR	m_pSerialTxBuffer;
	PUCHAR	m_pSerialTxTail;
	PUCHAR	m_pSerialRxTail;
	PUCHAR	m_pSerialRxHead;
	PUCHAR	m_pSerialTxHead;

public:
	BOOLEAN m_bSerialDataReady;
	BOOLEAN m_bPhotoDetect;

public:

	int GetSerialData ( PUCHAR pData );
	void ClearOnBoardRam ( void );
	BOOLEAN OnInterrupt ( void );
	NTSTATUS QueueSerialData ( PUCHAR pData, ULONG uLen );
	void Destroy(void);
	void AssignAddress ( PUCHAR Addr );
	void SetInterruptMode ( BOOLEAN nMode );
	void FirePhotoEvent ();
	void FireEOPEvent ();
	void FireSerialEvent ( void );
	void ProcessSerialData ( void );
	void TxImage ( unsigned short wStartCol, unsigned short wNumCols, unsigned char nBPC, unsigned char * pImage );
	BOOLEAN IsPresent(void);
	NTSTATUS Activate (void);
	unsigned long GetIOAddress ( void );
	NTSTATUS Initialize ( void );
	NTSTATUS RegisterPhotoEvent ( HANDLE hEvent );
	NTSTATUS UnregisterPhotoEvent( void );
	NTSTATUS RegisterSerialEvent ( HANDLE hEvent );
	NTSTATUS UnregisterSerialEvent( void );
	NTSTATUS SetSerialParams ( tagSerialParams *pParams );
	NTSTATUS ProgramFPGA ( PVOID pCode, ULONG lBytes );
	NTSTATUS RegisterEOPEvent (HANDLE hEvent);
	NTSTATUS UnregisterEOPEvent ();

	UCHAR ReadByte (IN UCHAR offset );
	VOID WriteByte (IN UCHAR offset, IN UCHAR data );

	USHORT ReadWord (IN USHORT offset);
	VOID WriteWord (IN USHORT offset, IN USHORT data);

	ULONG ReadLong (IN ULONG offset);
	VOID WriteLong (IN ULONG offset, IN ULONG data);

	VOID ReadBuffer (IN UCHAR offset, IN PUCHAR pData, IN ULONG len);
	VOID WriteBuffer (IN UCHAR offset, IN PUCHAR pData, IN ULONG len);

	void EnableInterrupts (bool bEnable);

	void FlipBytes (PUSHORT lpData, ULONG lSize);
	void SetOffset (int nIndex, USHORT nVal);
	void DbgOffset (const IVHEADCONFIGSTRUCT & head, int nIndex);

	NTSTATUS GetState (IVCARDSTATESTRUCT  * pState);
	NTSTATUS SetConfig (IVCARDCONFIGSTRUCT * pConfig);

	NTSTATUS ReadRegister (IVREGISTERSTRUCT * p);
	NTSTATUS WriteRegister (IVREGISTERSTRUCT * p);

	NTSTATUS SetStrobe (int nColor);
	NTSTATUS Abort ();

	void ClearIRQ ();

	typedef enum 
	{
		INTERRUPT_PHOTOCELL,
		INTERRUPT_ENCODER,
	} INTERRUPT_TYPE;

	void SetInterruptType (INTERRUPT_TYPE type);
	INTERRUPT_TYPE GetInterruptType ();

	BOOLEAN	IsEnabled () const { return m_bEnabled; }
	void SetPause (bool bPause);
	BOOLEAN IsPaused () const { return m_bPause; }
	NTSTATUS EnablePrint (bool bEnable);
	BOOLEAN IsImageReady () const { return m_bImageReady; }
	bool IsPrintEnabled () const { return m_bPrintEnabled; }
	BOOLEAN IsSharedPhotocell () const { return m_bSharedPhotocell; }

	void OnEndOfColumn ();
	PUCHAR GetBufferPtr ();
	BOOLEAN FreeBuffers ();

	bool IsDPC () const { return m_bDPC ? true : false; }
	bool IsPhotocellBlocked ();

	UCHAR m_cPhysicalIrq;		// Physical Interrupt;  This is assigned by the resource manager.  Should be 0 for Slave Devices.
	UCHAR m_cOTBIrqId;			// This is Interrupt Id for slave devices.  The LSB is reserved thus valid Ids are 0x02 - 0x80
	PHYSICAL_ADDRESS PhyAddress;
	ULONG m_lPortLen;
	BOOLEAN m_bMaster;
	BOOLEAN m_bMappedPort;
	PKEVENT m_evtPhoto;
	PKEVENT m_evtSerial;
	PKEVENT m_evtEOP;
	BOOLEAN m_bFpgaLoaded;
	BOOLEAN	m_bDebug;

protected:

	void SetAutoPrint(bool bAutoPrint);
	bool GetAutoPrint () { return m_bAutoPrint; }

	BOOLEAN OnEncoderPulse ();
	BOOLEAN OnEncoderPulseBlank ();
	BOOLEAN OnPhotocell ();
	BOOLEAN OnEndOfImage ();
	BOOLEAN SetEvent (PKEVENT pEvent);
	
	PUCHAR	m_pcBuffer;
	PUSHORT m_pwColumn;

	UCHAR	m_cBPC;
	UCHAR	m_cEncoderDivisor;
	UCHAR	m_cCmdReg;

	USHORT	m_wColumns;
	USHORT	m_wCurrentCol;
	USHORT	m_wRowWords;
	USHORT	m_wShift1;
	USHORT	m_wShift2;

	int		m_nStrobeColor;

	ULONG	m_lPhotocellCount;

	BOOLEAN m_bImageReady;
	BOOLEAN	m_bEnabled;
	BOOLEAN	m_bPauseAtEOP;
	BOOLEAN	m_bPause;
	bool	m_bPrintEnabled;

	UCHAR	m_bInternalPhotocell;
	UCHAR	m_bSharedPhotocell;
	USHORT	m_lAutoPrintCols;
	USHORT	m_lEncoderTicks;
	bool	m_bAutoPrint;
	UCHAR	m_bDPC;
	bool	m_bInitialPhotoState;

public:
	ULONG m_lInterrupt;
};
#endif // !defined(AFX_Card_H__B87F81C6_C93A_4A60_B610_EC4E7C10790C__INCLUDED_)
