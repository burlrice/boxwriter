///////////////////////////////////////////////////////////////////////////////
// Control.cpp -- IOCTL handlers for FxMphc driver
// Copyright (C) 2002 By FoxJet Tech Center
// All rights reserved.
//
//	Author: Chris G. Hodge
//	Version History:
//		1.00	Creation
///////////////////////////////////////////////////////////////////////////////

#include "stddcls.h"
#include "Defines.h"
#include "driver.h"
#include "..\..\FxMphc\src\ioctls.h"

#pragma PAGEDCODE

NTSTATUS IoCtlWriteFPGA (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
	NTSTATUS status = STATUS_SUCCESS;
	info = 0;
	DbgPrint ( "%s - IOCTL_WRITE_FPGA\n", DRIVERNAME );
	BOOLEAN found = FALSE;
	ULONG Address = *( (PULONG) SystemBuffer );
	PUCHAR pData = (PUCHAR) SystemBuffer;
	pData += sizeof (ULONG);

	for ( int i = 0; i < MAX_DEVICES; i++ ) {
		if ( pdx->m_Devices[i].PhyAddress.QuadPart == Address ) {
			DbgPrint ( "%s - Programming FPGA at %x", DRIVERNAME, Address);
			status = pdx->m_Devices[i].ProgramFPGA ( pData, InputLength - sizeof ( ULONG ) );
			found = TRUE;
			break;
		}
	}

	if ( found == FALSE ) {
		info = 0;
		DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, Address);
		status = STATUS_INVALID_PARAMETER;
	}

	return status;
}

NTSTATUS IoCtlResetFlags (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
	info = 0;
	//DbgPrint ( "%s - IOCTL_RESET_FLAGS\n", DRIVERNAME );
	BOOLEAN found = FALSE;
	if ( InputLength < sizeof(ULONG) )
	{	
		DbgPrint (DRIVERNAME " - %s(%d): Invalid input parameter", __FILE__, __LINE__);
		return STATUS_INVALID_BUFFER_SIZE;
	}

	ULONG Address	= *( (PULONG) SystemBuffer );
	for ( int i = 0; i < MAX_DEVICES; i++ ) 
	{
		if ( pdx->m_Devices[i].IsPresent() )
		{
			if ( pdx->m_Devices[i].PhyAddress.QuadPart == Address ) 
			{
				//pdx->m_Devices[i].ClearDebugFlags();
				found = TRUE;
			}
		}
	}

	if ( found == FALSE ) 
	{
		DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, Address);
		return STATUS_INVALID_PARAMETER;
	}

	return STATUS_SUCCESS;
}

NTSTATUS IoCtlGetFlags (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
	info = 0;

	if ( InputLength < sizeof (PHCB) ) {
		DbgPrint (DRIVERNAME " - %s(%d): Invalid input parameter", __FILE__, __LINE__);
		return STATUS_INVALID_PARAMETER;
	}


	PHCB *pPHCB  = (PHCB *) SystemBuffer;
	BOOLEAN found = FALSE;

	for ( int i = 0; i < MAX_DEVICES; i++ ) {
		if ( pdx->m_Devices[i].GetIOAddress() == pPHCB->lAddress ) {
			_DBG_FLAGS *pFlags = ( _DBG_FLAGS * ) pPHCB->Data;
/*
			pFlags->ISRInt		= pdx->m_Devices[i].flagISR;
			pFlags->OTBInt		= pdx->m_Devices[i].flagOTBInt;
			pFlags->PhotoInt	= pdx->m_Devices[i].flagPhotoInt;
			pFlags->SerialInt	= pdx->m_Devices[i].flagSerialInt;
			pFlags->TachInt		= pdx->m_Devices[i].flagTachInt;
			pFlags->Reserved1	= pdx->m_Devices[i].flagReserved1;
			pFlags->Reserved2	= pdx->m_Devices[i].flagReserved2;
			pFlags->uICR		= pdx->m_Devices[i].Get_ICR (); // Burl
			pFlags->uIER		= pdx->m_Devices[i].ReadByte (IER); // Burl
*/

			info = sizeof ( PHCB );
			found = TRUE;
			break;
		}
	}

	if ( found == FALSE ) {
		DbgPrint ("Unable to locate Device Address: %X", pPHCB->lAddress);
		return STATUS_INVALID_PARAMETER;
	}

	return STATUS_SUCCESS;
}

NTSTATUS IoCtlGetVersion (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
DbgPrint (DRIVERNAME " - %s(%d): IRQL: %d", __FILE__, __LINE__, KeGetCurrentIrql ());

	if (OutputLength < sizeof(_VERSION)) {	// Output Buffer too Small
		DbgPrint (DRIVERNAME " - %s(%d): Invalid input parameter", __FILE__, __LINE__);
		return STATUS_INVALID_BUFFER_SIZE;
	}
	
	_VERSION Version;
	Version.Major = MAJOR;
	Version.Minor = MINOR;
	Version.Revision = REV;
	RtlCopyMemory (SystemBuffer, (PVOID)&Version, sizeof(_VERSION));
	info = sizeof(_VERSION);

	return STATUS_SUCCESS;
}

NTSTATUS IoCtlSerSerialEventHandle (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
	NTSTATUS status = STATUS_SUCCESS;
	
	info = 0;

	if (InputLength < sizeof(HANDLE)) {	
		DbgPrint (DRIVERNAME " - %s(%d): Invalid input parameter", __FILE__, __LINE__);
		return STATUS_INVALID_BUFFER_SIZE;
	}

	HANDLE hEvent = *(PHANDLE)SystemBuffer;

	if (pdx->SerialDataEvent) 
		ObDereferenceObject(pdx->SerialDataEvent);
	
	pdx->SerialDataEvent = NULL;
	
	if (hEvent) {
		PKEVENT pEvent;

		status = ObReferenceObjectByHandle(hEvent, EVENT_MODIFY_STATE, *ExEventObjectType, UserMode, (PVOID*)&pEvent, NULL);
		
		if (status != STATUS_SUCCESS) {
			switch (status) {
				case STATUS_OBJECT_TYPE_MISMATCH:
					DbgPrint ("ObReferenceObjectByHandle Failed - Object Type Mismatch\n");
					break;
				case STATUS_ACCESS_DENIED:
					DbgPrint ("ObReferenceObjectByHandle Failed - Access Denied\n");
					break;
				case STATUS_INVALID_HANDLE:
					DbgPrint ("ObReferenceObjectByHandle Failed - Invalid Handle\n");
					break;
				default:
					DbgPrint ("ObReferenceObjectByHandle Failed - UnKnown Error\n");
			}
		}
		else 
			pdx->SerialDataEvent = pEvent;
	}

	return status;
}

NTSTATUS IoCtrlRegisterSerialEvent (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
	NTSTATUS status = STATUS_SUCCESS;

	info = 0;
	
	if (InputLength < sizeof(ULONG) * 2) {	
		DbgPrint (DRIVERNAME " - %s(%d): Invalid input parameter", __FILE__, __LINE__);
		return STATUS_INVALID_BUFFER_SIZE;
	}
	
	PULONG p = (PULONG)SystemBuffer;
	ULONG Address = *p++;
	HANDLE hEvent = *(PHANDLE)p;

	for ( int i = 0; i < MAX_DEVICES; i++ ) {
		if ( pdx->m_Devices[i].GetIOAddress() == Address ) {
			status = pdx->m_Devices[i].RegisterSerialEvent ( hEvent );
			break;
		}
	}

	if (status != STATUS_SUCCESS) {
		switch (status) {
			case STATUS_OBJECT_TYPE_MISMATCH:
				DbgPrint ("ObReferenceObjectByHandle Failed - Object Type Mismatch\n");
				break;
			case STATUS_ACCESS_DENIED:
				DbgPrint ("ObReferenceObjectByHandle Failed - Access Denied\n");
				break;
			case STATUS_INVALID_HANDLE:
				DbgPrint ("ObReferenceObjectByHandle Failed - Invalid Handle\n");
				break;
			default:
				DbgPrint ("ObReferenceObjectByHandle Failed - UnKnown Error\n");
		}
	}

	return status;
}

NTSTATUS IoCtlUnregisterSerialEvent (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
	NTSTATUS status = STATUS_SUCCESS;

	info = 0;
	
	if (InputLength < sizeof(ULONG)) {	
		DbgPrint (DRIVERNAME " - %s(%d): Invalid input parameter", __FILE__, __LINE__);
		return STATUS_INVALID_BUFFER_SIZE;
	}
	
	ULONG Address = *( ( PULONG ) SystemBuffer);
	
	for ( int i = 0; i < MAX_DEVICES; i++ ) {
		if ( pdx->m_Devices[i].GetIOAddress() == Address ) {
			status = pdx->m_Devices[i].UnregisterSerialEvent ( );
			break;
		}
	}

	return status;
}

NTSTATUS IoCtlEnumIoAddresses (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
	NTSTATUS status = STATUS_SUCCESS;

	info = 0;
	
	if (OutputLength < ( MAX_DEVICES * sizeof(ULONG) ) ) {	
		DbgPrint (DRIVERNAME " - %s(%d): Invalid input parameter", __FILE__, __LINE__);
		return STATUS_INVALID_BUFFER_SIZE;
	}
	
	PULONG p = (PULONG) SystemBuffer;
	
	for ( int i = 0; i < MAX_DEVICES; i++ ) {

		{
			PUCHAR pAddr	= (PUCHAR)pdx->m_Devices[i].PhyAddress.QuadPart;
			ULONG nLen		= pdx->m_Devices[i].m_lPortLen;
			PUCHAR pIOAddr	= (PUCHAR)pdx->m_Devices[i].GetIOAddress ();
			bool bPresent	= pdx->m_Devices[i].IsPresent () ? true : false;

			DbgPrint(DRIVERNAME " - m_Devices [%d]: 0x%p [0x%X, 0x%X] %s", 
				i, 
				pIOAddr,
				pAddr,
				nLen,
				bPresent ? "[PRESENT]" : "");
		}

		if (pdx->m_Devices[i].IsPresent ()) {
			*p = (ULONG) pdx->m_Devices[i].GetIOAddress();
			p++;
			info += sizeof (ULONG);
		}
	}

	return status;
}

NTSTATUS IoCtlRegisterPhotoEvent (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
	NTSTATUS status = STATUS_SUCCESS;

	info = 0;
	
	if (InputLength < sizeof(ULONG) * 2) {	
		DbgPrint (DRIVERNAME " - %s(%d): Invalid input parameter", __FILE__, __LINE__);
		return STATUS_INVALID_BUFFER_SIZE;
	}

	PULONG p = (PULONG)SystemBuffer;
	ULONG Address = *p++;
	HANDLE hEvent = *(PHANDLE)p;
	
	for ( int i = 0; i < MAX_DEVICES; i++ ) {
		if ( pdx->m_Devices[i].GetIOAddress() == Address ) {
			status = pdx->m_Devices[i].RegisterPhotoEvent ( hEvent );
			break;
		}
	}

	if (status != STATUS_SUCCESS) {
		switch (status) {
			case STATUS_OBJECT_TYPE_MISMATCH:
				DbgPrint ("ObReferenceObjectByHandle Failed - Object Type Mismatch\n");
				break;
			case STATUS_ACCESS_DENIED:
				DbgPrint ("ObReferenceObjectByHandle Failed - Access Denied\n");
				break;
			case STATUS_INVALID_HANDLE:
				DbgPrint ("ObReferenceObjectByHandle Failed - Invalid Handle\n");
				break;
			default:
				DbgPrint ("ObReferenceObjectByHandle Failed - UnKnown Error\n");
		}
	}

	return status;
}

NTSTATUS IoCtlRegisterEOPEvent (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
	NTSTATUS status = STATUS_SUCCESS;

	info = 0;
	
	if (InputLength < sizeof(ULONG) * 2) {	
		DbgPrint (DRIVERNAME " - %s(%d): Invalid input parameter", __FILE__, __LINE__);
		return STATUS_INVALID_BUFFER_SIZE;
	}

	PULONG p = (PULONG)SystemBuffer;
	ULONG Address = *p++;
	HANDLE hEvent = *(PHANDLE)p;
	
	for ( int i = 0; i < MAX_DEVICES; i++ ) {
		if ( pdx->m_Devices[i].GetIOAddress() == Address ) {
			status = pdx->m_Devices[i].RegisterEOPEvent (hEvent);
			break;
		}
	}

	if (status != STATUS_SUCCESS) {
		switch (status) {
			case STATUS_OBJECT_TYPE_MISMATCH:
				DbgPrint ("ObReferenceObjectByHandle Failed - Object Type Mismatch\n");
				break;
			case STATUS_ACCESS_DENIED:
				DbgPrint ("ObReferenceObjectByHandle Failed - Access Denied\n");
				break;
			case STATUS_INVALID_HANDLE:
				DbgPrint ("ObReferenceObjectByHandle Failed - Invalid Handle\n");
				break;
			default:
				DbgPrint ("ObReferenceObjectByHandle Failed - UnKnown Error\n");
		}
	}

	return status;
}

NTSTATUS IoCtlUnregisterPhotoEvent (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
	NTSTATUS status = STATUS_SUCCESS;

	info = 0;
	
	if (InputLength < sizeof(ULONG)) {	
		DbgPrint (DRIVERNAME " - %s(%d): Invalid input parameter", __FILE__, __LINE__);
		return STATUS_INVALID_BUFFER_SIZE;
	}
	
	ULONG Address = *( ( PULONG ) SystemBuffer);
	
	for ( int i = 0; i < MAX_DEVICES; i++ ) {
		if ( pdx->m_Devices[i].GetIOAddress() == Address ) {
			status = pdx->m_Devices[i].UnregisterPhotoEvent ( );
			break;
		}
	}

	return status;
}

NTSTATUS IoCtlUnregisterEOPEvent (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
	NTSTATUS status = STATUS_SUCCESS;

	info = 0;
	
	if (InputLength < sizeof(ULONG)) {	
		DbgPrint (DRIVERNAME " - %s(%d): Invalid input parameter", __FILE__, __LINE__);
		return STATUS_INVALID_BUFFER_SIZE;
	}
	
	ULONG Address = *( ( PULONG ) SystemBuffer);
	
	for ( int i = 0; i < MAX_DEVICES; i++ ) {
		if ( pdx->m_Devices[i].GetIOAddress() == Address ) {
			status = pdx->m_Devices[i].UnregisterEOPEvent ();
			break;
		}
	}

	return status;
}

NTSTATUS IoCtlEnableDebug (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
	NTSTATUS status = STATUS_SUCCESS;
	info = 0;
	BOOLEAN found = FALSE;
	ENABLESTRUCT * pEnable = (ENABLESTRUCT *)SystemBuffer;

	if (InputLength < sizeof (ENABLESTRUCT)) {
		DbgPrint (DRIVERNAME " - %s(%d): Invalid input parameter", __FILE__, __LINE__);
		return STATUS_INVALID_PARAMETER;
	}

	for ( int i = 0; i < MAX_DEVICES; i++ ) {
		if (pdx->m_Devices[i].PhyAddress.QuadPart == pEnable->m_lAddress) {
			status = pdx->m_Devices[i].m_bDebug = pEnable->m_bEnabled;
			info = sizeof (ENABLESTRUCT);
			found = TRUE;
			break;
		}
	}

	if ( found == FALSE ) {
		info = 0;
		DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, pEnable->m_lAddress);
		status = STATUS_INVALID_PARAMETER;
	}

	return status;
}

NTSTATUS IoCtlAbort (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
	NTSTATUS status = STATUS_SUCCESS;
	BOOLEAN found = FALSE;
	ENABLESTRUCT * pEnable = (ENABLESTRUCT *)SystemBuffer;

	info = 0;

	if (InputLength < sizeof (ENABLESTRUCT)) {
		DbgPrint (DRIVERNAME " - %s(%d): Invalid input parameter", __FILE__, __LINE__);
		return STATUS_INVALID_PARAMETER;
	}

	for ( int i = 0; i < MAX_DEVICES; i++ ) {
		if (pdx->m_Devices[i].PhyAddress.QuadPart == pEnable->m_lAddress) {
			status = pdx->m_Devices[i].Abort ();
			info = sizeof (ENABLESTRUCT);
			found = TRUE;
			break;
		}
	}

	if ( found == FALSE ) {
		info = 0;
		DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, pEnable->m_lAddress);
		status = STATUS_INVALID_PARAMETER;
	}

	return status;
}

NTSTATUS IoCtlEnablePrint (PVOID SystemBuffer, ULONG InputLength, ULONG OutputLength, ULONG & info, PDEVICE_EXTENSION pdx)
{
	NTSTATUS status = STATUS_SUCCESS;
	BOOLEAN found = FALSE;
	ENABLESTRUCT * pEnable = (ENABLESTRUCT *)SystemBuffer;

	info = 0;

	if (InputLength < sizeof (ENABLESTRUCT)) {
		DbgPrint (DRIVERNAME " - %s(%d): Invalid input parameter", __FILE__, __LINE__);
		return STATUS_INVALID_PARAMETER;
	}

	for ( int i = 0; i < MAX_DEVICES; i++ ) {
		if (pdx->m_Devices[i].PhyAddress.QuadPart == pEnable->m_lAddress) {
			status = pdx->m_Devices[i].EnablePrint (pEnable->m_bEnabled ? true : false);
			info = sizeof (ENABLESTRUCT);
			found = TRUE;
			break;
		}
	}

	if ( found == FALSE ) {
		info = 0;
		DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, pEnable->m_lAddress);
		status = STATUS_INVALID_PARAMETER;
	}

	return status;
}

NTSTATUS DispatchControl(PDEVICE_OBJECT fdo, PIRP Irp)
{
	PAGED_CODE();
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;

	NTSTATUS status = IoAcquireRemoveLock(&pdx->RemoveLock, Irp);
	if (!NT_SUCCESS(status))
		return CompleteRequest(Irp, status, 0);

	ULONG info = 0;
	PIO_STACK_LOCATION IrpStack = IoGetCurrentIrpStackLocation(Irp);

	ULONG ControlCode		= IrpStack->Parameters.DeviceIoControl.IoControlCode;
	ULONG InputLength		= IrpStack->Parameters.DeviceIoControl.InputBufferLength;
	ULONG OutputLength	= IrpStack->Parameters.DeviceIoControl.OutputBufferLength;
	PVOID SystemBuffer	= Irp->AssociatedIrp.SystemBuffer;

	switch ( ControlCode ) {						// process request
		// Begin Debug Code
		case IOCTL_RESET_FLAGS:				status = IoCtlResetFlags				(SystemBuffer, InputLength, OutputLength, info, pdx);	break;
		case IOCTL_GET_FLAGS:				status = IoCtlGetFlags					(SystemBuffer, InputLength, OutputLength, info, pdx);	break;
		// End Debug Code.
		case IOCTL_GET_VERSION:				status = IoCtlGetVersion				(SystemBuffer, InputLength, OutputLength, info, pdx);	break;
		case IOCTL_SET_SERIAL_EVENT_HANDLE:	status = IoCtlSerSerialEventHandle		(SystemBuffer, InputLength, OutputLength, info, pdx);	break;
		case IOCTL_REGISTER_SERIAL_EVENT:	status = IoCtrlRegisterSerialEvent		(SystemBuffer, InputLength, OutputLength, info, pdx);	break;
		case IOCTL_UNREGISTER_SERIAL_EVENT:	status = IoCtlUnregisterSerialEvent		(SystemBuffer, InputLength, OutputLength, info, pdx);	break;
		case IOCTL_ENUM_IO_ADDRESSES:		status = IoCtlEnumIoAddresses			(SystemBuffer, InputLength, OutputLength, info, pdx);	break;
		case IOCTL_REGISTER_PHOTO_EVENT:	status = IoCtlRegisterPhotoEvent		(SystemBuffer, InputLength, OutputLength, info, pdx);	break;
		case IOCTL_REGISTER_EOP_EVENT:		status = IoCtlRegisterEOPEvent			(SystemBuffer, InputLength, OutputLength, info, pdx);	break;
		case IOCTL_UNREGISTER_PHOTO_EVENT:	status = IoCtlUnregisterPhotoEvent		(SystemBuffer, InputLength, OutputLength, info, pdx);	break;
		case IOCTL_UNREGISTER_EOP_EVENT:	status = IoCtlUnregisterEOPEvent		(SystemBuffer, InputLength, OutputLength, info, pdx);	break;
		case IOCTL_ENABLE_DEBUG:			status = IoCtlEnableDebug				(SystemBuffer, InputLength, OutputLength, info, pdx);	break;
		case IOCTL_ABORT:					status = IoCtlAbort						(SystemBuffer, InputLength, OutputLength, info, pdx);	break;
		case IOCTL_ENABLE_PRINT:			status = IoCtlEnablePrint				(SystemBuffer, InputLength, OutputLength, info, pdx);	break;

		// Any operation which require changing any of the regesters of the card must be syncronized
		// with the interrupt routine.  Thus we mark them as pending IO so they will be ran later in
		// the RunIOCTLSync routine.
		case IOCTL_WRITE_FPGA:				//return IoCtlWriteFPGA (SystemBuffer, InputLength, OutputLength, info, pdx);
		case IOCTL_READ_SERIAL_DATA:
		case IOCTL_IMAGE_UPDATE:
		case IOCTL_SET_SERIAL_PARAMS:
		case IOCTL_CLEAR_BUFFER:
		case IOCTL_SET_CONFIG:
		case IOCTL_GET_STATE:
		case IOCTL_READ_REGISTER:
		case IOCTL_WRITE_REGISTER:
		case IOCTL_SET_STROBE:
			IoMarkIrpPending(Irp);
			StartPacket(&pdx->dqReadWrite, fdo, Irp, OnCancelReadWrite);
			return STATUS_PENDING;
			break;
		default:
			status = STATUS_INVALID_DEVICE_REQUEST;
			break;
	}						// process request
	IoReleaseRemoveLock(&pdx->RemoveLock, Irp);
	return CompleteRequest(Irp, status, info);
}
