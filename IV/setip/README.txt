
            Remote IP Address Configuration Via the Network
            -----------------------------------------------

You can be able to change the IP address of the device using the setip.exe
program.

Here is the "usage" info for setip.exe:

  usage: setip [options] MAC IP
  where:
     MAC       - Ethernet address assigned to the device as follows:
                   hexadecimal: xx:xx:xx:xx:xx:xx
                       decimal: ddd.ddd.ddd.ddd.ddd.ddd
     IP        - new IP address for the device as follows:
                       decimal: ddd.ddd.ddd.ddd
                          name: host
  where options:
    -d         - set debug mode
    -b IPaddr  - broascast to IP address "IPaddr"
    -N num     - number of repeats


To use "setip", make sure the device is on the same LAN as the host
you will be running "setip" on and issue a command like the following:

    setip 0.1.2.3.4.5 10.1.2.3

Note that "setip" uses standard "BOOTP reply" UDP broadcast packets.
Being UDP, packets can be lost.  So you need to check that the device
"saw" the packets and did indeed change it's IP address.  "Ping" is a
good way to check this.

The new IP address set using "setip" is stored in the devices's nov-volatile
memory where it will remain, even if the power is cycled.

