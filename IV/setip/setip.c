
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bootp.h"
#include "getopt.h"

/************************************************************************/

#ifdef SunOS
#define UNIX
#endif

#if defined(UNIX)
#include <ioctls.h>
#include <net/if.h>

#include    <unistd.h>
#include    <errno.h>
#include    <sys/types.h>
#include    <sys/stat.h>

#ifdef SunOS
#include    <malloc.h>
#include    <sys/filio.h>
#else /* !SunOS */
#include    <sys/select.h>
#include    <asm/ioctls.h>
#endif /* !SunOS */

#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#endif /* UNIX */

#if defined(_CONSOLE)

#define IS_WINDOWS
#include <io.h>
#include <fcntl.h>
#include <sys/types.h>
#include <winsock.h>
#define SockClose(x)    closesocket(x)

#else /* !_CONSOLE */

#define SockClose(x)  close(x)

#endif /* !_CONSOLE */

#ifndef INADDR_NONE
#define INADDR_NONE (-1)
#endif

/************************************************************************/

static int bDebug = 0;
static char *program = "";
#define BOOTP_REPLY_PORT 68

static void Err(int bAbort, char *why)
{
#ifdef IS_WINDOWS
     LPVOID lpMsgBuf = NULL;
     FormatMessage(
          FORMAT_MESSAGE_ALLOCATE_BUFFER | 
                 FORMAT_MESSAGE_FROM_SYSTEM |
                 FORMAT_MESSAGE_IGNORE_INSERTS,
          NULL,
          WSAGetLastError(),
          MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
          (LPTSTR) &lpMsgBuf,
          0,
          NULL );
     if (lpMsgBuf == NULL) lpMsgBuf = (LPVOID)"???\n";
     fflush(stdout);
     fprintf(stderr,"%s: %s: Error %lu/%lu: %s",
          program,why,GetLastError(),(unsigned long)WSAGetLastError(),(char *)lpMsgBuf);
     LocalFree(lpMsgBuf) ; // free the buffer
#else
     fflush(stdout);
     fprintf(stderr,"%s: ",program);
     perror(why);
#endif
     fflush(stderr);
     if (bAbort) exit(2);
}

int SendBootpReply(
     int times,
     int Socket,
     unsigned long serverIP,
     unsigned long broadcastIP,
     unsigned long clientIP,
     unsigned char *clientMAC
) {
     int i;
     char udpbuf[sizeof(BOOTP_PACKET)];
     int udpbuflen = sizeof(udpbuf);
     BOOTP_PACKET *ppkt = (BOOTP_PACKET *)udpbuf;
     struct sockaddr dst;

     //  typedef struct  {
     //      unsigned char optype;
     //      unsigned char hwtype;
     //      unsigned char hwlen;
     //      unsigned char hops;
     //      unsigned long xid;
     //      unsigned short secs;
     //      unsigned short unused;
     //      unsigned long ciaddr;
     //      unsigned long yiaddr;
     //      unsigned long siaddr;
     //      unsigned long giaddr;
     //      unsigned char chaddr[16];
     //      unsigned char sname[64];
     //      unsigned char bootfile[128];
     //      unsigned char vendor[64];
     //  } BOOTP_PACKET;

     if (bDebug) {
          fflush(stdout);
          fprintf(stderr,"SendBootpReply(%d, %d, "
            "%d.%d.%d.%d, "
            "%d.%d.%d.%d, "
            "%d.%d.%d.%d:%d, "
            "%02x:%02x:%02x:%02x:%02x:%02x)\n",
            times,
            (int)Socket,
            (int)(serverIP>>24)&255, (int)(serverIP>>16)&255, (int)(serverIP>>8)&255, (int)serverIP&255,
            (int)(broadcastIP>>24)&255, (int)(broadcastIP>>16)&255, (int)(broadcastIP>>8)&255, (int)broadcastIP&255,
            (int)(clientIP>>24)&255, (int)(clientIP>>16)&255, (int)(clientIP>>8)&255, (int)clientIP&255,
            BOOTP_REPLY_PORT,
            clientMAC[0],clientMAC[1],clientMAC[2],clientMAC[3],clientMAC[4],clientMAC[5]);
          fflush(stderr);
     }

     ((struct sockaddr_in *)(&dst))->sin_family = AF_INET;
     ((struct sockaddr_in *)(&dst))->sin_addr.s_addr = htonl(broadcastIP);
     ((struct sockaddr_in *)(&dst))->sin_port = htons(BOOTP_REPLY_PORT);

     memset(ppkt, 0, udpbuflen);      /* clear BOOTP packet */

     ppkt->optype = 2;                /* BOOTREPLY */
     ppkt->hwtype = 1;                /* 10mb ethernet */
     ppkt->hwlen = 6;
     ppkt->hops = 0;
     ppkt->xid = 0;                   /* transaction ID */
     ppkt->secs = 0;                  /* client's secs since boot */
     ppkt->ciaddr = ntohl(0);         /* client IP (from client) */
     ppkt->yiaddr = ntohl(clientIP);  /* client IP (from server) */
     ppkt->siaddr = ntohl(serverIP);  /* server IP (from server) */
     ppkt->giaddr = ntohl(0);         /* gateway IP (optional) */
     ppkt->chaddr[0] = clientMAC[0];  /* client Ethernet address */
     ppkt->chaddr[1] = clientMAC[1];
     ppkt->chaddr[2] = clientMAC[2];
     ppkt->chaddr[3] = clientMAC[3];
     ppkt->chaddr[4] = clientMAC[4];
     ppkt->chaddr[5] = clientMAC[5];

     for (i = 0; i < times; ++i) {
          if (sendto(Socket, udpbuf, udpbuflen, 0, &dst, sizeof(struct sockaddr)) != udpbuflen) {
               Err(0,"sendto");
               return(0);         /* error exit */
          }
     }
     return 1;     /* no error exit */
}

#ifndef IS_WINDOWS
int BroadcastBootpReply(
     int times,
     unsigned long clientIP,
     unsigned char *clientMAC
) {
     int s;
     struct sockaddr_in sin;
     struct sockaddr dst;
     int on = 1;
     struct ifconf ifc; 
     struct ifreq *ifr;
     char buf[BUFSIZ];
     int n;
     unsigned long serverIP;
     unsigned long broadcastIP;

     // To send a broadcast message, a datagram socket should be created: 
     s = socket(AF_INET, SOCK_DGRAM, 0);

     // The socket is marked as allowing broadcasting, 
     if (setsockopt(s, SOL_SOCKET, SO_BROADCAST, &on, sizeof (on)) < 0) { 
          perror("setsockopt(SO_BROADCAST)");
          return(1);
     }

     // and at least a port number should be bound to the socket: 
     sin.sin_family = AF_INET; 
     sin.sin_addr.s_addr = htonl(INADDR_ANY); 
     sin.sin_port = htons(BOOTP_REPLY_PORT); 
     bind(s, (struct sockaddr *) &sin, sizeof (sin));

     // The destination address of the message to be broadcast depends
     // on the network(s) on which the message is to be broadcast. The
     // Internet domain supports a shorthand notation for broadcast
     // on the local network, the address INADDR_BROADCAST (defined
     // in <netinet/in.h>. To determine the list of addresses for
     // all reachable neighbors requires knowledge of the networks to
     // which the host is connected. Since this information should be
     // obtained in a host-independent fashion and may be impossible
     // to derive, 4.4BSD provides a method of retrieving this
     // information from the system data structures. The SIOCGIFCONF
     // ioctl call returns the interface configuration of a host
     // in the form of a single ifconf structure; this structure
     // contains a ``data area'' which is made up of an array of of
     // ifreq structures, one for each network interface to which the
     // host is connected. These structures are defined in <net/if.h>
     // as follows:
     //
     // struct ifconf { 
     //      int      ifc_len;                  /* size of associated buffer */ 
     //      union { 
     //           caddr_t      ifcu_buf; 
     //           struct ifreq *ifcu_req; 
     //      } ifc_ifcu; 
     // };
     //
     // #define ifc_buf ifc_ifcu.ifcu_buf       /* buffer address */ 
     // #define ifc_req ifc_ifcu.ifcu_req       /* array of structures returned */
     //
     // #define IFNAMSIZ 16
     //
     // struct ifreq { 
     //      char      ifr_name[IFNAMSIZ];      /* if name, e.g. "en0" */ 
     //      union { 
     //           struct sockaddr ifru_addr; 
     //           struct sockaddr ifru_dstaddr; 
     //           struct sockaddr ifru_broadaddr; 
     //           short           ifru_flags; 
     //           caddr_t         ifru_data; 
     //      } ifr_ifru; 
     // };
     //
     // #define ifr_addr      ifr_ifru.ifru_addr      /* address */ 
     // #define ifr_dstaddr   ifr_ifru.ifru_dstaddr   /* other end of p-to-p link */ 
     // #define ifr_broadaddr ifr_ifru.ifru_broadaddr /* broadcast address */ 
     // #define ifr_flags     ifr_ifru.ifru_flags     /* flags */ 
     // #define ifr_data      ifr_ifru.ifru_data      /* for use by interface */
     //
     // The actual call which obtains the interface configuration is 

     ifc.ifc_len = sizeof (buf); 
     ifc.ifc_buf = buf; 
     if (ioctl(s, SIOCGIFCONF, (char *) &ifc) < 0) { 
          perror("ioctl(SIOCGIFCONF)");
          return(1);
     }

     // After this call buf will contain one ifreq structure for each
     // network to which the host is connected, and ifc.ifc_len will
     // have been modified to reflect the number of bytes used by
     // the ifreq structures.
     // 
     // For each structure there exists a set of ``interface
     // flags'' which tell whether the network correspond ing to
     // that interface is up or down, point to point or broadcast,
     // etc. The SIOCGIFFLAGS ioctl retrieves these flags for an
     // interface specified by an ifreq structure as follows:

     ifr = ifc.ifc_req;

     for (n = ifc.ifc_len / sizeof (struct ifreq); --n >= 0; ifr++) { 
          /* 
           * We must be careful that we don't use an interface 
           * dev oted to an address family other than those intended; 
           * if we were interested in NS interfaces, the 
           * AF_INET would be AF_NS. 
           */ 
          if (ifr->ifr_addr.sa_family != AF_INET) 
               continue; 
          if (ioctl(s, SIOCGIFFLAGS, (char *) ifr) < 0) { 
               perror("ioctl(SIOCGIFFLAGS)");
               return(1);
          } 
          /* 
           * Skip boring cases. 
           */ 
#if 0
          if ((ifr->ifr_flags & IFF_UP) == 0 || 
             (ifr->ifr_flags & IFF_LOOPBACK) || 
             (ifr->ifr_flags & (IFF_BROADCAST
#ifdef IFF_POINTTOPOINT
                               | IFF_POINTTOPOINT
#endif
             )) == 0) 
               continue;
#else
          if ((ifr->ifr_flags & IFF_UP) == 0 || 
              (ifr->ifr_flags & IFF_LOOPBACK)
#ifdef IFF_POINTTOPOINT
              || (ifr->ifr_flags & IFF_POINTTOPOINT) == 0)
#endif
             ) 
               continue;
          /* 
           * Skip "non-broadcast" entries
           */ 
          if ((ifr->ifr_flags & IFF_BROADCAST) == 0) continue;
#endif

          /* 
           * save interface's IP address
           */ 
          serverIP = htonl(((struct sockaddr_in *)(&ifr->ifr_addr))->sin_addr.s_addr);

          // Once the flags have been obtained, the broadcast address
          // must be obtained. In the case of broadcast networks this is
          // done via the SIOCGIFBRDADDR ioctl, while for point-to-point
          // networks the address of the destination host is obtained
          // with SIOCGIFDSTADDR.

#ifdef IFF_POINTTOPOINT
          if (ifr->ifr_flags & IFF_POINTTOPOINT) { 
               if (ioctl(s, SIOCGIFDSTADDR, (char *) ifr) < 0) { 
                    perror("ioctl(SIOCGIFDSTADDR)");
                    return(1);
               } 
               bcopy((char *) ifr->ifr_dstaddr, (char *) &dst, sizeof (ifr->ifr_dstaddr)); 
          } else
#endif
          if (ifr->ifr_flags & IFF_BROADCAST) { 
               if (ioctl(s, SIOCGIFBRDADDR, (char *) ifr) < 0) { 
                    perror("ioctl(SIOCGIFBRDADDR)");
                    return(1);
               } 
               bcopy((char *) &ifr->ifr_broadaddr, (char *) &dst, sizeof (ifr->ifr_broadaddr)); 
          }
     
          // After the appropriate ioctl's have obtained the broadcast or
          // destination address (now in dst), the sendto call may be used:

          broadcastIP = htonl(((struct sockaddr_in *)(&dst))->sin_addr.s_addr);
          SendBootpReply(times, s, serverIP, broadcastIP, clientIP, clientMAC);

     }

     // In the above loop one sendto occurs for every interface
     // to which the host is connected that supports the notion of
     // broadcast or point-to-point addressing. If a process only
     // wished to send broadcast messages on a given network, code
     // similar to that outlined above would be used, but the loop
     // would need to find the correct destination address.

     // Received broadcast messages contain the senders address
     // and port, as datagram sockets are bound before a message is
     // allowed to go out.

     SockClose(s);
}
#endif /* !IS_WINDOWS */

void Usage(void) {
     fprintf(stderr,
         "usage: %s [options] MAC IP\n"
         "where:\n"
         "   MAC       - Ethernet address assigned to PicoWeb as follows:\n"
         "                 hexadecimal: xx:xx:xx:xx:xx:xx\n"
         "                     decimal: ddd.ddd.ddd.ddd.ddd.ddd\n"
         "   IP        - new IP address for PicoWeb as follows:\n"
         "                     decimal: ddd.ddd.ddd.ddd\n"
         "                        name: host\n"
         "where options:\n"
         "  -d         - set debug mode\n"
#ifdef IS_WINDOWS
         "  -b IPaddr  - broascast to IP address \"IPaddr\"\n"
#endif
         "  -N num     - number of repeats\n"
          ,program);
     exit(1);
}

int main(
     int argc,
     char *argv[]
){
     int           ReplySocket;
     struct        sockaddr_in SockAddr;
     int           n = 3;
/*     int           port = -1;
     char          *ip = NULL;*/
     char          c;
     char          *szClientIP = NULL;
     char          *szClientMAC = NULL;
     unsigned long clientIP = 0;
     unsigned long broadcastIP = INADDR_BROADCAST;
     unsigned long serverIP = 0;
     unsigned char clientMAC[6];
     int           bBroad = 1;
     char mac_str[7];
     FILE *InFile;

     program = argv[0];
#define CMD_SWITCHES "db:N:?h"
     while ((c = getopt(argc,argv,CMD_SWITCHES)) != EOF)  {
          switch (c)  {
               case 'd':  {
                    bDebug = 1;
                    break;
               }
               case 'b':  {
#ifdef IS_WINDOWS
                    broadcastIP = inet_addr(optarg);
                    break;
#else
                    Usage();
#endif
               }
               case 'N':  {
                    n = atoi(optarg);
                    break;
               }
               case '?':
                    Usage();
               default:  {
                    Usage();
               }
          }
     }

     if (optind < argc) {
          if (optind < argc) szClientMAC = argv[optind++];
          if (optind < argc) szClientIP = argv[optind++];
          if (optind < argc) {
               Usage();
          }
     }

#ifdef IS_WINDOWS
     {
          WORD wVer;
          static WSADATA wsaData;
          wVer = MAKEWORD(1,1) ;                 // request WinSock version 1.1
          if (WSAStartup(wVer,&wsaData) != 0)  {  // if startup failed
               Err(1,"wsastartup");
          }
     }
#endif /* IS_WINDOWS */


     if (!szClientIP) {
          Usage();
     } else {
          clientIP = htonl(inet_addr(szClientIP));
          if ((clientIP == 0) || (clientIP == ((unsigned long)-1))) {
               struct hostent *pHostEnt;
               pHostEnt = gethostbyname(szClientIP);
               if (pHostEnt) {
                    struct  sockaddr_in SockAddr;
                    SockAddr.sin_addr =
                            *((struct in_addr *)pHostEnt->h_addr_list[0]);
                    clientIP = htonl(SockAddr.sin_addr.s_addr);
               }
          }
     }

     if (!szClientMAC) {
          Usage();
     } else {
          char ch;
          int p[6];
          int i, n;

          ch = 0;

          n = sscanf(szClientMAC,"%x:%x:%x:%x:%x:%x%c",
                                 p+0, p+1, p+2, p+3, p+4, p+5, &ch);
          if (n == 4){
	      if ((InFile=fopen("mac.txt","r+"))==NULL){
	          printf ("Unable to open mac.txt");
                  return 1;
	      }
	      fread(mac_str, 6, 1, InFile);
	      mac_str[6]=0;
	      fclose(InFile);
              n += sscanf(mac_str+2,"%02x%02x%c", p+4, p+5, &ch);
          }
          
          if (n != 6) {
               /* alternate decimal form */
               ch = 0;
               n = sscanf(szClientMAC,"%d.%d.%d.%d.%d.%d%c",
                                      p+0, p+1, p+2, p+3, p+4, p+5, &ch);
          }
          for (i = 0; i < 6; ++i) {
               if (p[i] > 255)
                    n = -1; /* force error if octet too big */
               if (p[i] < 0)
                    n = -1; /* force error if octet negative */
          }
          if (n != 6 || ch != 0) {
               fprintf(stderr, "%s: Bad PicoWeb MAC address (%s)!\n", program, szClientMAC);
               return 1;
          }
          for (i = 0; i < 6; ++i)
               clientMAC[i] = p[i];
     }

     if ((clientIP == 0) || (clientIP == ((unsigned long)-1))) {
          fprintf(stderr, "%s: Bad PicoWeb IP address (%s)!\n", program, szClientIP);
          return 1;
     }

#ifdef IS_WINDOWS

     ReplySocket = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
     if (ReplySocket < 0)  {
          Err(1,"socket");
     }

     SockAddr.sin_family = AF_INET;
     SockAddr.sin_addr.s_addr = INADDR_ANY;
     SockAddr.sin_port = 0 ;  /* use any old port to reply */

     if (bind(ReplySocket,(struct sockaddr *)&SockAddr, sizeof SockAddr) != 0)  {
          Err(1,"bind");
     }

     if (setsockopt(ReplySocket,SOL_SOCKET,SO_BROADCAST,
               (char *)&bBroad,sizeof bBroad) != 0)  {
          Err(1,"so_broadcast");
     }
     SendBootpReply(n, ReplySocket, serverIP, broadcastIP, clientIP, clientMAC);
     SockClose(ReplySocket);
#else /* !IS_WINDOWS */
     BroadcastBootpReply(n, clientIP, clientMAC);
#endif /* !IS_WINDOWS */
}
