typedef struct  {
    unsigned char optype ;
    unsigned char hwtype ;
    unsigned char hwlen ;
    unsigned char hops ;
    unsigned long xid ;
    unsigned short secs ;
    unsigned short unused ;
    unsigned long ciaddr ;
    unsigned long yiaddr ;
    unsigned long siaddr ;
    unsigned long giaddr ;
    unsigned char chaddr[16] ;
    unsigned char sname[64] ;
    unsigned char bootfile[128] ;
    unsigned char vendor[64] ;
} BOOTP_PACKET ;
