import java.awt.*;
import java.awt.event.*;

class msgbox extends Dialog implements ActionListener {
    int ret = 0;
    public msgbox(Frame f, String msg) {
        super(f, msg, true);
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        try {
            jbInit(msg);
        } catch (Exception e) {}
    }
    void jbInit(String msg) throws Exception {
        setLayout(new FlowLayout());
        Button cmdYes = new Button("Yes");
        Button cmdNo = new Button("No");
        Button cmdCancel = new Button("Cancel");
        cmdYes.setSize(10, 10);
        add(new Label(msg));
        add(cmdYes);
        add(cmdNo);
        add(cmdCancel);
        cmdNo.addActionListener(this);
        cmdYes.addActionListener(this);
        cmdCancel.addActionListener(this);
        setSize(200, 100);
        util.center(this);
    }
    
    public int run() {
        setVisible(true);
        return ret;
    }
    public void actionPerformed(ActionEvent e) {
        String s = e.getActionCommand();
        if (s.equals("Yes")) {
            ret = 1;
            dispose();
        } else if (s.equals("No")) {
            dispose();
        } else if (s.equals("Cancel")) {
            ret = 2;
            dispose();
        }
    }

    protected void processWindowEvent(WindowEvent e) {
        super.processWindowEvent(e);
        if(e.getID() == WindowEvent.WINDOW_CLOSING) windowClosing(e);
    }

    public void windowClosing(WindowEvent e) {
        dispose();
    }
}
