import java.io.*;
import javax.swing.*;
import java.awt.*;
import java.util.Hashtable;

public class util {

	static boolean bErrorsInText=true;
    static Hashtable props = new Hashtable();
	public util() {
	}

    public static void setShowErrorInText(boolean b) {
    	bErrorsInText = b;
    }

	public static void showError(Throwable t) {
    	showError(t, true);
    }

    public static void showError(Throwable t, boolean bShowStackTrace) {
    	if (bErrorsInText) {
        	if (bShowStackTrace)
	        	t.printStackTrace();
            else
            	System.err.println(t.toString());
        } else {
   			StringWriter sw = new StringWriter();
   			PrintWriter p = new PrintWriter(sw);
        	t.printStackTrace(p);
        	JOptionPane.showMessageDialog(null, sw.toString());
        }
    }

   public static void center(Window w) {
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension frameSize;
      frameSize = w.getSize();

      if (frameSize.height > screenSize.height)
         frameSize.height = screenSize.height;
      if (frameSize.width > screenSize.width)
         frameSize.width = screenSize.width;
      w.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
   }

	public static void setProperty(String name, String val) {
		props.put(name, val);
	}

    public static String getProperty(String name) {
    	Object o = props.get(name);
        if (null != o) return o.toString();
        return null;
    }
}
