import java.io.*;

public class ByteArrayInput extends ByteArrayInputStream {
/*	byte[] buf = null;
    protected int pos = 0;*/

    public ByteArrayInput(byte[] b) {
    	super(b);
//    	buf = b;
    }
	public ByteArrayInput(String name) throws IOException {
    	super(new byte[]{0});
        getData(name);
	}

    protected void getData(String sName) {
        File f = null;
        FileInputStream file = null;
	    f = new File(sName);
        int i, j;
//        short b;
        long len;

        try {
    	    file = new FileInputStream(f);
        	len = f.length();
        	buf = new byte[(int)len];
            file.read(buf);
        	if (null == file) buf = null;
            file.close();
            return;
        } catch (IOException e) {
        	util.showError(e);
        }
        buf = null;
    }

    long size() {
    	return buf.length;
    }

    public byte readByte() {
    	return (byte)(read()); //stream[pos++] & 0xFF);
    }
    public int read() {
    	int i = buf[pos++];
        return i;
    }

    public int readUnsignedShort() {
    	int i = ((read() << 8) | (readByte() & 0xFF));
    	return i;
    }

    public int readInt() {
    	int i = ((readUnsignedShort() << 16) | readUnsignedShort());
        return i;
    }
}
