import java.awt.*;
import java.awt.Window;
import java.awt.image.*;
import java.awt.geom.*;

class TTFont {
    int h, baseline, scale, storage;
    final int inset=5; 
    IJFont ijfnt;
    Graphics2D g;
    BufferedImage bi;
    int widths[];
    Window frm;
    boolean dbg;
    FontImage img;
    Graphics fg;

    TTFont(Font fnt, boolean dbg, int bline, int scale, int start, int end) {
        this.dbg = dbg;
        this.baseline = bline;
        this.scale = scale;
        h = fnt.getSize();
        storage = FontColumn.getStorageSize(h);
        scale = Math.max(scale, 1);
        bi = new BufferedImage(20, h, BufferedImage.TYPE_BYTE_BINARY); 
        ijfnt = new IJFont(fnt.getSize(), 1, start, end);

        g = bi.createGraphics();
        g.setFont(fnt);
        widths = g.getFontMetrics(fnt).getWidths();
        int w = 0;
        for (int i=start; i <= end; i++) w = Math.max(w, widths[i]);
        g.dispose();

        frm = new Window(new Frame()); //new Frame();
        frm.setSize(600, 600);
        frm.setLocation(inset, inset);
        img = new FontImage();
        img.setFont(ijfnt);
        if (dbg) {
            frm.add(img, null);
            img.setLocation(h+inset, inset);
            img.setMaximumSize(new Dimension(400, 400));
            frm.setVisible(true);
        }
        fg = frm.getGraphics();
        bi = new BufferedImage(storage * 8, scale * (w/*h*/ + 1), BufferedImage.TYPE_BYTE_BINARY);
        g = bi.createGraphics();
        g.setFont(fnt);
        AffineTransform af = AffineTransform.getRotateInstance(Math.toRadians(90), w/2, h/2);
        af.concatenate(AffineTransform.getScaleInstance(scale, 1.0));
        g.transform(af);
    }

    void process() {
        int start = ijfnt.startval(), end = ijfnt.endval(), begin = 0;
        String s = "";
        for (byte i=(byte)start; i <= end; i++) {
            int width = scale * widths[i];
            s = String.valueOf((char)i);
            g.clearRect(0, 0, bi.getWidth(), bi.getHeight());
            g.drawString(s, 0, (h-baseline));
            ijfnt.setFontColumns((char)i, width); // ?? img.setFontColumns(); ??
            try {
                begin = processChar(width, begin, ijfnt.getChar((char)i));
            } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
            if (dbg) {
                fg.clearRect(0, 0, bi.getWidth(), bi.getHeight());
                fg.drawImage(bi, 0, 0, bi.getWidth(), bi.getHeight(), null);
                img.setText(String.valueOf((char)i));
            }
        }
        if (dbg) frm.setVisible(false);
    }

    protected int processChar(int width, int begin, FontChar fchar ) {
        Raster r = bi.getData(new Rectangle(0, begin, bi.getWidth(), width));
        DataBuffer db = r.getDataBuffer();
        if (DataBuffer.TYPE_BYTE == db.getDataType()) {
            byte data[] = ((DataBufferByte)db).getData();
            int k = 0;
            for (int j=0; j < width; j++) {
                byte b[] = new byte[storage];
                System.arraycopy(data, k, b, 0, b.length);
                k += b.length;
                FontColumn fc = fchar.getColumn(j);
                fc.setData(b);
            }
        }
        return begin; //+width;
    }

    IJFont getFont() {
        return ijfnt;
    }

    public static IJFont convert(Font fnt, boolean dbg, int baseline, 
            int scale, int start, int end) {
        TTFont ttf = new TTFont(fnt, dbg, baseline, scale, start, end);
        ttf.process();
        return ttf.getFont();
    }
}
