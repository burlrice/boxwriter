import java.awt.*;
import java.awt.event.*;

public class AdjstChrsDialog extends Dialog implements ActionListener{
    Panel panel = new Panel();
    Button cmdOK = new Button();
    Button cmdCancel = new Button();
    Label lblSRng = new Label();
    Label lblERng = new Label();
    TextField txtSRng = new TextField();
    TextField txtERng = new TextField();
    int start, end;
    boolean bIsOK = false;

    public AdjstChrsDialog(Frame frame, int start, int end){
        super(frame, "Adjust Characters", true);
        this.start = start;
        this.end = end;
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        try  {
            jbInit();
            add(panel);
            pack();
            setSize(300, 200);
            util.center(this);
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    public AdjstChrsDialog(Frame frame, int end) {
        this(frame, -1, end);
    }

    void jbInit() throws Exception {
        panel.setLayout(null);

        cmdCancel.setBounds(new Rectangle(40, 100, 100, 25));
        cmdCancel.setLabel("Cancel");
        cmdCancel.addActionListener(this);

        cmdOK.setBounds(new Rectangle(150, 100, 100, 25));
        cmdOK.setLabel("OK");
        cmdOK.addActionListener(this);

        lblSRng.setText("Start Range:");
        lblSRng.setBounds(new Rectangle(60, 35, 75, 20));
        if (0 <= start)
            lblERng.setText("End Range:");
        else
            lblERng.setText("Height:");
        lblERng.setBounds(new Rectangle(60, 60, 75, 20));
        txtSRng.setBounds(new Rectangle(155, 35, 75, 20));
        txtSRng.setText(Integer.toString(start));
        txtERng.setBounds(new Rectangle(155, 60, 75, 20));
        txtERng.setText(Integer.toString(end));

        if (start != -1) {
            panel.add(lblSRng);
            panel.add(txtSRng);
        }
        panel.add(lblERng);
        panel.add(txtERng);
        panel.add(cmdOK, null);
        panel.add(cmdCancel, null);
    }

    protected void bldChoices() {
    }

    protected void processWindowEvent(WindowEvent e) {
        if(e.getID() == WindowEvent.WINDOW_CLOSING) {
            cancel();
        }
        super.processWindowEvent(e);
    }

    void cancel() {
        dispose();
    }

    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if (o.equals(cmdCancel)) {
            setVisible(false);
        } else if (o.equals(cmdOK)) {
            setVisible(false);
            start = Integer.parseInt(txtSRng.getText());
            end = Integer.parseInt(txtERng.getText());
            bIsOK = true;
        }
    }
    public int start() { return this.start; }
    public int end() { return this.end; }
    public boolean isOK() {
        return bIsOK;
    }
}
