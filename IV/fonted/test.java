import java.awt.*;
public class test {
        static TestSuite t = new TestSuite();

        public static void go() {
                // add new TestCases's here
                t.addTest(new testFontColumn(t));
                t.addTest(new testIO(t));
                t.addTest(new testSeries1(t));
                t.addTest(new testExtract(t));
            t.addTest(new testTrueType(t));
                t.run();
                t.finish();
        }
}

class testSeries1 extends TestCase {
        public testSeries1(TestSuite t) {
        super(t);
    }

    public void testImport() {
//      String s = "C:\\src\\ivpro\\fonts\\9s.fnt";
//      DiagraphFont fnt = new IJFont(new Series1Font(null, s));
//      DiagraphFont fnt = new Series1Font(null, s);
    }
}

class testIO extends TestCase {
        public testIO(TestSuite t) {
        super(t);
    }

    public void testRead() {
        byte[] b = new byte[] {0, 0, 0, (byte)0x84, (byte)0x01};
            ByteArrayInput bf = new ByteArrayInput(b);
        int i = bf.readInt();
        t.assert("", i == 0x84);
    }
    public void testWrite() {
            ByteArrayOutput bf = new ByteArrayOutput();
        bf.writeInt(0xaabbccdd);
        bf.writeShort(0x1122);

        byte[] b = bf.toByteArray();
        t.assert("size != 6", bf.size() == 6);
        t.assert("b[0] == (byte)0xaa", b[0] == (byte)0xaa);
        t.assert("b[1] == (byte)0xbb", b[1] == (byte)0xbb);
        t.assert("b[2] == (byte)0xcc", b[2] == (byte)0xcc);
        t.assert("b[3] == (byte)0xdd", b[3] == (byte)0xdd);
        t.assert("b[4] == (byte)0x11", b[4] == (byte)0x11);
        t.assert("b[5] == (byte)0x22", b[5] == (byte)0x22);
    }
}

class testFontColumn extends TestCase {
        public testFontColumn(TestSuite t) {
        super(t);
    }

    public void testAppend() {
        byte[] b = new byte [] {(byte)0x01, (byte)0xff};
//      byte[] b = new byte [] {(byte)0xff, (byte)0x80};
        int dots = 9;
        FontColumn fc = new FontColumn(dots, b);
        fc.append(b, dots);
        b = fc.getData();
        t.assert("append:" + helper.toHex(b[0]) + " != 0x03", b[0] == 0x03);
        t.assert("append:" + helper.toHex(b[1]) + " != 0xff", b[1] == (byte)0xff);
        t.assert("append:" + helper.toHex(b[2]) + " != 0xff", b[2] == (byte)0xff);
    }
}

class testExtract extends TestCase {
        public testExtract(TestSuite t) {
        super(t);
    }

    public void testExtraction() {
        byte[] b = new byte [] {(byte)0x01, (byte)0x17};
        int dots = 9;
        FontColumn fc = new FontColumn(dots, b);
        fc.trim(1, 5);
        b = fc.getData();
        t.assert("extract: " + b.length + " != 2", b.length == 2);
        t.assert("extract: " + fc.getDots() + " != 5", fc.getDots() == 5);
        t.assert("extract: " + helper.toHex(b[1]) + " != 0x17", b[1] == (byte)0x17);
//      t.assert(helper.toHex(b[2]) + " != 0xff", b[2] == (byte)0xff);
        b = new byte [] {(byte)0xaa, (byte)0x55, (byte)0xdd, (byte)0xee};
        fc = new FontColumn(32, b);
        fc.trim(5, 28);
        byte[] c = fc.getData();
        t.assert("extract: " + c.length + " != 4", c.length == 4);
        t.assert("extract: " + fc.getDots() + " != 24", fc.getDots() == 24);
        for (int i=1; i < c.length; i++) {
            //System.out.println(helper.toHex(c[i]) + " " + helper.toHex(b[i]));
            t.assert("extract: " + helper.toHex(b[i]) + " != " + helper.toHex(c[i]), b[i] == c[i]);
        }
    }
}

class testTrueType extends TestCase {
    public testTrueType(TestSuite t) { super(t); }
    public void testTrueTypeConvert() {
        IJFont f;
        int h = 43;
        f = TTFont.convert(new Font("Arial", Font.PLAIN, 43), true, 7, 6, '%', ')');
        f = TTFont.convert(new Font("Arial", Font.PLAIN, h), true, 7, 1, '!', '0');
        t.assert("incorrect height", f.getHeight() == h);

        f = TTFont.convert(new Font("Arial", Font.PLAIN, 32), true, 7, 6, ' ', '~');
        FontChar fc = f.getChar('I');
        t.assert("width wrong", fc.getWidth() == 54);
    }
}
