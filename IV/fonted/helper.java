/*
 * Created on Jul 29, 2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */

/**
 * @author cnauman
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
class helper {
    static String toHex(char b) {
        String s = toHex((byte)(b >> 8)) + toHex((byte)(b & 0x00FF));
        return s;
    }
    static String toHex(byte b) {
        String s = Integer.toHexString(b);
        int l = s.length();
        if (l > 1) s = s.substring(l - 2, l);
        return s;
    }
    static String printBuf(byte[] b, int len) {
        String s = "";
        int num = 0;
        for (int i=0; i < len; i++) {
            if (((i+1) < b.length) && (b[i] == b[i+1])) {
                num++;
            } else {
                if (num > 0 && ((i-1) > 0)) {
                    s += " " + (num+1) + "(0x" + toHex(b[i-1]) +
                                        ")";
                } else {
                    s += " 0x" + toHex(b[i]);
                }
                num = 0;
            }
        }
        if (num > 0) s += " " + num + "(" + toHex(b[b.length-1]) + ")";
        return s;
    }
}