import java.awt.*;

public abstract class DiagraphFont {
    String sName;
    String sFullName;
    FontChar[] chars;
    int iSpace = 1; //3;
    int start=-1, end=256;

    public DiagraphFont() {}
    public DiagraphFont(String path, String n) {
        sName = n;
    	if (path != null)
        	sFullName = path + System.getProperty("file.separator") + n;
//        	sFullName = path + "/" + n;
        else
        	sFullName = n;
/*        if (!(sFullName.length() >= 4 &&
        	sFullName.substring(sFullName.length()-4).equalsIgnoreCase(".fnt")))
            		sFullName += ".fnt";*/
		load();
    }

    public void load() {
    	if (null != sFullName && ! sFullName.equals(""))
        	load(sFullName);
        else
        	util.showError(new Exception("Invalid file name" + sFullName));
    }
    protected abstract void load(String s);
    public abstract void save(String fname);
    public int getHeight() {
    	int x=0;
    	for (int i=0; i < chars.length; i++)
        	x = Math.max(x, chars[i].getHeight());
    	return x;
    }
    public void Extract(int start, int end) {
        if ((1 == start) && (getHeight() == end)) return;
        for (int i=0; i < chars.length; i++)
            chars[i].trim(start, end);
    }

    public void space(int n) { iSpace = n; }
    public int space() { return iSpace; }
    public String getName() { return sName; }
    public String getCharName(int i) { 
        try {
            return (new Character(chars[i].getName())).toString(); 
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }
    public int startval() { return start; }
    public int endval() { return end; }

    public void flipDot(char c, int col, int dot) {
    	FontChar fc = chars[c - start];
        if (null == fc) return;
        fc.flipdot(col, dot);
    }
    public FontColumn getColumn(char c, int col) {
    	FontChar fc = chars[c - start];
        if (null == fc) return null;
        return fc.getColumn(col);
    }

    public FontChar getChar(char c) {
        return chars[c - start];
    }

    public int getWidth(String s) {
        int width=0;
        for (int i=0; i < s.length(); i++) {
        	width += chars[s.charAt(i) - start].getWidth();
            width += iSpace;
        }
        return width;
    }

    public int getMaxWidth() {
    	int x=0;
    	for (int i=start; i < end; i++) {
			x = Math.max(x, getWidth(String.valueOf((char)i)));
        }
        return x;
    }

    public String toString() {
    	return ("[Name: " + sName + ", space: " + iSpace +
        		", start: " + start + ", end: " + end);
    }

    public void drawString(Graphics g, Rectangle r, String s, int mag, int num) {
        int col = 0, ndx; //1;
        FontChar fc;
        if (null == r) throw new Error("Null Rectangle passed to drawString()");
        if (null == chars) return;
        // used for multiple characters
        for (int i=0; i < s.length(); i++) {
            fc = null;
            ndx = s.charAt(i) - start;
            if (0 <= ndx && ndx < chars.length) fc = chars[s.charAt(i) - start];
            if (fc != null) {
            	fc.draw(g, r, col++, mag, num);
	        	col += this.iSpace;
            }
        }
    }

    public int getFontColumns(char curChar){
    	FontChar fc = chars[curChar - start];
        if(fc == null) return 0;
        return fc.getWidth();
    }

    public void setFontColumns(char curChar, int c){
    	FontChar fc = chars[curChar - start];
        if(fc == null) return;
        fc.setColumns(c, getHeight());
    }

    public void setCharRange(int start, int end) {
        FontChar[] fc = new FontChar[end - start + 1];
        int dots = getHeight();
        for (int i=start; i <= end; i++) {
            if (this.start <= i && i < this.end)
                fc[i - start] = chars[i - this.start];
            else
                fc[i - start] = new FontChar(dots);
        }
        this.start = start;
        this.end = end;
        chars = fc;
    }
}


