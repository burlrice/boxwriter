import java.io.*;
import java.util.*;

public class Series1Font extends DiagraphFont {

    public Series1Font(String path,String name) {
    	super(path, name);
    }

    protected  void load(String s) {
        boolean bDelimit=true;
        String sDefinition="";
        Vector v = new Vector();
        short b,x;

	try {
        ByteArrayInput bf = new ByteArrayInput(s);
        bf.read(); bf.read(); bf.read(); bf.read();
        bf.read();

        for (int i=5; i < bf.size(); i++) {
        	b = (short)(bf.read());
        	if (bDelimit) {
                if (-1 == start) start = b; //bytes[i];
                end = Math.max(b, end); //bytes[i];
                sDefinition = "";
                bDelimit = false;
            } else {
            	sDefinition += (char)b; //bytes[i];
            }
            if ((b & 128) != 0) {
		v.addElement(new FontChar(sDefinition));
                bDelimit = true;
            }
        }
        } catch (IOException e) {
        	util.showError(e);
        }
        chars = new FontChar[v.size()];
        end = start + v.size() - 1;
        for (int i=0; i < v.size(); i++) {
        	chars[i] = (FontChar)v.elementAt(i);
        }
    }

    public void save(String fname) {
    }
}
