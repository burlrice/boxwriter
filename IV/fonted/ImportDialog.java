import java.awt.*;
import java.awt.event.*;

public class ImportDialog extends Dialog 
        implements ItemListener, ActionListener {
    Panel panel = new Panel();
    Button btnLower = new Button();
    Button btnUpper = new Button();
    TextField txtUpper = new TextField();
    TextField txtLower = new TextField();
    Label lblUpper = new Label();
    Label lblLower = new Label();
    CheckboxGroup checkboxGroup = new CheckboxGroup();
    Panel panel1 = new Panel();
    Checkbox chk18Dot = new Checkbox();
    Checkbox chk9Dot = new Checkbox();
    Button cmdOK = new Button();
    Button cmdCancel = new Button();
    Frame parent;
    Label lblStartDot = new Label();
    Label lblEndDot = new Label();
    Choice chcStartDot = new Choice();
    Choice chcEndDot = new Choice();

    public ImportDialog(Frame frame, String title, boolean modal) {
        super(frame, title, modal);
        parent = frame;
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        try  {
            jbInit();
            add(panel);
            pack();
            setSize(300, 200);
            util.center(this);
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public ImportDialog() {
        this(null, "", false);
    }

    public ImportDialog(Frame frame) {
        this(frame, "", false);
    }

    public ImportDialog(Frame frame, boolean modal) {
        this(frame, "", modal);
    }

    public ImportDialog(Frame frame, String title) {
        this(frame, title, false);
    }

    void jbInit() throws Exception {
        panel.setLayout(null);


        chk9Dot.setCheckboxGroup(checkboxGroup);
        chk9Dot.setLabel("9 Dot");
        chk9Dot.addItemListener(this);
        chk9Dot.setBackground(SystemColor.control);

        chk18Dot.setCheckboxGroup(checkboxGroup);
        chk18Dot.setLabel("18 Dot");
        chk18Dot.addItemListener(this);
        chk18Dot.setBackground(SystemColor.control);
        checkboxGroup.setSelectedCheckbox(chk18Dot);

        int y = 5;
        panel1.setBounds(new Rectangle(5, y, 150, 25));
        panel1.add(chk9Dot, null);
        panel1.add(chk18Dot, null);

        y += 30;
        btnUpper.setBounds(new Rectangle(232, y, 25, 24));
        btnUpper.setLabel("...");
        btnUpper.addActionListener(this);
        txtUpper.setBounds(new Rectangle(92, y, 133, 26));
        lblUpper.setBounds(new Rectangle(7, y, 83, 25));
        lblUpper.setText("Upper 9 Dots");
        lblUpper.setBackground(SystemColor.control);


        y += 30;
        btnLower.setBounds(new Rectangle(232, y, 25, 24));
        btnLower.setLabel("...");
        btnLower.addActionListener(this);
        txtLower.setBounds(new Rectangle(92, y, 133, 26));
        lblLower.setBounds(new Rectangle(7, y, 83, 25));
        lblLower.setText("Lower 9 Dots");
        lblLower.setBackground(SystemColor.control);

        y+= 30;
        cmdOK.setBounds(new Rectangle(180, y, 75, 25));
        cmdOK.setLabel("OK");
        cmdOK.addActionListener(this);

        lblStartDot.setText("Start Dot");
        lblStartDot.setBounds(new Rectangle(5, y, 75, 23));
        chcStartDot.setBounds(new Rectangle(80, y, 50, 23));

        y+= 30;
        cmdCancel.setBounds(new Rectangle(180, y, 75, 25));
        cmdCancel.setLabel("Cancel");
        cmdCancel.addActionListener(this);

        lblEndDot.setText("End Dot");
        lblEndDot.setBounds(new Rectangle(5, y, 75, 23));
        chcEndDot.setBounds(new Rectangle(80, y, 50, 23));
        bldChoice();

        panel.add(txtUpper, null);
        panel.add(txtLower, null);
        panel.add(lblLower, null);
        panel.add(lblUpper, null);
        panel.add(btnLower, null);
        panel.add(panel1, null);
        panel.add(lblStartDot);
        panel.add(lblEndDot);
        panel.add(chcStartDot);
        panel.add(chcEndDot);
        panel.add(cmdOK, null);
        panel.add(cmdCancel, null);
        panel.add(btnUpper, null);
    }
    protected void bldChoice() {
        String tmp;
        int end = 18;
        if (checkboxGroup.getSelectedCheckbox().equals(chk9Dot)) end = 9;
        chcStartDot.removeAll();
        chcEndDot.removeAll();
        for (int i=1; i <= end; i++) {
            tmp = Integer.toString(i);
            chcStartDot.add(tmp);
            chcEndDot.add(tmp);
        }
        chcEndDot.select(end-1);
    }

    protected void processWindowEvent(WindowEvent e) {
        if(e.getID() == WindowEvent.WINDOW_CLOSING) {
            cancel();
        }
        super.processWindowEvent(e);
    }

    void cancel() {
        dispose();
    }

    public void itemStateChanged(ItemEvent e) {
        boolean bState = false;
        Object o = e.getSource();
        if (o.equals(chk18Dot))
            bState = true;
        btnLower.setVisible(bState);
        lblLower.setVisible(bState);
        txtLower.setVisible(bState);
        bldChoice();
    }

    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if (o.equals(cmdCancel)) {
            txtUpper.setText("");
            chk9Dot.setState(true);;
            setVisible(false);
        } else if (o.equals(btnUpper)) {
            txtUpper.setText(getFile());
        } else if (o.equals(btnLower)) {
            txtLower.setText(getFile());
        } else if (o.equals(cmdOK)) {
            setVisible(false);
        }
    }
    public String upper() {
        return txtUpper.getText();
    }

    public String lower() {
        return txtLower.getText();
    }

    public int StartDot() {
        return chcStartDot.getSelectedIndex() + 1;
    }

    public int EndDot() {
        return chcEndDot.getSelectedIndex() + 1;
    }

    public boolean is18Dot() {
        return txtLower.isVisible();
    }

    protected String getFile() {
        String sFile = null;
        FileDialog fd = new FileDialog(parent, this.getTitle(), FileDialog.LOAD);
        fd.setVisible(true);
        sFile = fd.getDirectory();
        if (null == sFile) return "";
        sFile += fd.getFile();
        if (null == sFile) return "";
        return sFile;
    }
}
