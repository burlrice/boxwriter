import java.awt.*;
import java.awt.event.*;

public class fonted extends Frame implements ActionListener, KeyListener {
    boolean bNeedsSave = false;
    MenuBar mnuBar = new MenuBar();
    Menu mnuFile = new Menu();
    Menu mnuChar = new Menu();
    MenuItem mnuFileOpen = new MenuItem();
    Menu mnuFileNew = new Menu();
    Menu mnuFileImport = new Menu();
    MenuItem mnuFileNew5dot = new MenuItem();
    MenuItem mnuFileNew7dot = new MenuItem();
    MenuItem mnuFileNew9dot = new MenuItem();
    MenuItem mnuFileNew18dot = new MenuItem();
    MenuItem mnuFileNewOtherdot = new MenuItem();
    MenuItem mnuFileExit = new MenuItem();
    MenuItem mnuFileSave = new MenuItem();
    MenuItem mnuFileSaveas = new MenuItem();
    MenuItem mnuFileImportSeries1 = new MenuItem();
    MenuItem mnuFileImportTrueType = new MenuItem();
    Menu mnuOpts = new Menu();
    MenuItem mnuOptsARCols = new MenuItem();
    MenuItem mnuOptsAdjstChrRng = new MenuItem();
    MenuItem mnuOptsAdjHeight = new MenuItem();

    BorderLayout borderLayout1 = new BorderLayout();
    BorderLayout borderLayout2 = new BorderLayout();
    FontImage img;
    //Panel panel = new Panel();
    String text="", sFileName="";
    //GridLayout gridLayout1 = new GridLayout();
    //GridLayout gridLayout2 = new GridLayout();
    static String HYPHEN = "hyphen";

    public fonted(String fname) {
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        try  {
            img = new FontImage();
            jbInit();
            //setLocation(0, 150);
            center();
            if (null != fname && ! fname.equals("")) openFile(fname);
            setVisible(true);
        }
        catch(Exception e) {
            util.showError(e);
        }
    }

    private void jbInit() throws Exception {
        this.setMenuBar(mnuBar);
        //this.setLayout(null); //gridLayout2);
        mnuFile.setLabel("File");
        mnuChar.setLabel("Char");
        mnuFileNew.setLabel("New");
        mnuFileNew5dot.setLabel("5 dot");
        mnuFileNew7dot.setLabel("7 dot");
        mnuFileNew9dot.setLabel("9 dot");
        mnuFileNew18dot.setLabel("18 dot");
        mnuFileNewOtherdot.setLabel("Other ...");
        mnuFileOpen.setLabel("Open ...");
        mnuFileExit.setLabel("Exit");
        mnuFileSaveas.setLabel("Save As ...");
        mnuOpts.setLabel("Options");
        mnuOptsARCols.setLabel("Number of columns");
        mnuOptsAdjstChrRng.setLabel("Adjust Character Range");
        mnuFileImportSeries1.setLabel("Series 1");
        mnuFileImportTrueType.setLabel("True Type");
        mnuOptsAdjHeight.setLabel("Adjust Character Height");
        
        mnuFileSave.setLabel("Save ...");
        /*panel.setLayout(gridLayout1);
        //img.setSize(350, 100);
        gridLayout1.setColumns(8);
        gridLayout1.setRows(12);
        gridLayout2.setColumns(2);*/
        mnuFileImport.setLabel("Import ...");
        mnuFileImport.add(mnuFileImportSeries1);
        mnuFileImport.add(mnuFileImportTrueType);
        mnuBar.add(mnuFile);
        mnuFileNew.add(mnuFileNew5dot);
        mnuFileNew.add(mnuFileNew7dot);
        mnuFileNew.add(mnuFileNew9dot);
        mnuFileNew.add(mnuFileNew18dot);
        mnuFileNew.add(mnuFileNewOtherdot);
        mnuFile.add(mnuFileNew);
        mnuFile.add(mnuFileOpen);
        mnuFile.add(mnuFileImport);
        mnuFile.add(mnuFileSave);
        mnuFile.add(mnuFileSaveas);
        mnuFile.add(mnuFileExit);
        mnuFileNew5dot.addActionListener(this);
        mnuFileNew7dot.addActionListener(this);
        mnuFileNew9dot.addActionListener(this);
        mnuFileNew18dot.addActionListener(this);
        mnuFileNewOtherdot.addActionListener(this);
        mnuFileOpen.addActionListener(this);
        mnuFileExit.addActionListener(this);
        mnuFileSave.addActionListener(this);
        mnuFileSaveas.addActionListener(this);
        mnuFileImportSeries1.addActionListener(this);
        mnuFileImportTrueType.addActionListener(this);
        mnuBar.add(mnuOpts);
        mnuBar.add(mnuChar);
        mnuOpts.add(mnuOptsARCols);
        mnuOpts.add(mnuOptsAdjstChrRng);
        mnuOpts.add(mnuOptsAdjHeight);
        mnuOptsARCols.addActionListener(this);
        mnuOptsAdjstChrRng.addActionListener(this);
        mnuOptsAdjHeight.addActionListener(this);
        setSize(400, 300);
        img.setMaximumSize(new Dimension(395, 295));
        //setLayout(new FlowLayout());
        this.add(img, null);
        //this.pack();
        //this.add(panel, null);
        //panel.setSize(400, 200);
        this.addKeyListener(this);
    }

   protected void processWindowEvent(WindowEvent e) {
      super.processWindowEvent(e);
      if(e.getID() == WindowEvent.WINDOW_CLOSING) windowClosing(e);
   }

   public void windowClosing(WindowEvent e) {
        if (CheckSave()) System.exit(0);
   }
   
   boolean CheckSave() {
        if (bNeedsSave) {
            int i =(new msgbox(this, "Save File?")).run();
            if (1 == i) saveFile(sFileName);
            else if (2 == i) return false;
        }
        return true;
    }

   public void center() {
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension frameSize = this.getSize();
      if (frameSize.height > screenSize.height)
         frameSize.height = screenSize.height;
      if (frameSize.width > screenSize.width)
         frameSize.width = screenSize.width;
      this.setLocation((screenSize.width - frameSize.width) / 2, 
                        (screenSize.height - frameSize.height) / 2);
   }

    public void actionPerformed(ActionEvent e) {
        try {
            Object o = e.getSource();
            if (o.equals(mnuFileOpen)) {
                openFile(null);
            } else if (o.equals(mnuFileExit)) {
                windowClosing(null);
            } else if (o.equals(mnuFileSave)) {
                saveFile(sFileName);
            } else if (o.equals(mnuFileSaveas)) {
                saveFile(null);
            } else if (o.equals(mnuFileImportSeries1)) {
                importFile();
            } else if (o.equals(mnuFileImportTrueType)) {
                importTTFont();
            } else if (o.equals(mnuOptsARCols)) {
                addRemoveColumns();
            } else if (o.equals(mnuOptsAdjstChrRng)) {
                adjustCharRange();
            } else if (o.equals(mnuOptsAdjHeight)) {
                adjustCharHeight();
            } else if (o.equals(mnuFileNew5dot)) {
                doNew(5, 5);
            } else if (o.equals(mnuFileNew7dot)) {
                doNew(7, 7);
            } else if (o.equals(mnuFileNew9dot)) {
                doNew(9, 9);
            } else if (o.equals(mnuFileNew18dot)) {
                doNew(18, 10);
            } else if (o.equals(mnuFileNewOtherdot)) {
                NewDlg dlg = new NewDlg(this);
                dlg.setVisible(true);
                if (0 < dlg.dots()) doNew(dlg.dots(), dlg.cols());
            } else if (o instanceof MenuItem) { //Button) {
                //img.setString(((Button)o).getName());
                String s = ((MenuItem)o).getLabel();
                if (s.equals(HYPHEN)) s = "-";
                img.setText(s); //((Button)o).getLabel());
                //Display d = new Display(this, (, true, font);
            }
        } catch (Exception ex) {
            util.showError(ex, true);
        }
    }

    void saveFile(String sFile) {
        if (null == sFile || sFile.equals("")) {
            FileDialog opn = new FileDialog(this, "Save File", FileDialog.SAVE);
            opn.setVisible(true);
            sFile = opn.getDirectory();
            if (null == sFile || sFile.equals("")) return;
            sFile += opn.getFile();
            if (null == sFile || sFile.equals("")) return;
        }
        if (null == sFile || sFile.equals("")) return;
        img.font().save(sFile);
        this.setTitle("Font Editor - " + sFile);
        bNeedsSave = false;
    }

    void openFile(String sFile) {
        if (!CheckSave()) return;
        if (null == sFile || sFile.equals("")) {
            FileDialog opn = new FileDialog(this, "Open File", FileDialog.LOAD);
            opn.setVisible(true);
            sFile = opn.getDirectory();
            if (null == sFile) return;
            sFile += opn.getFile();
        }
        if (null == sFile || sFile.equals("")) return;
        //System.out.println(sFile);
        this.setTitle("Font Editor - " + sFile);
        String s = sFile;
        s = s.substring(s.lastIndexOf('.'), s.length());
        if (s.equals(".pfnt")) img.setPEL(true);
        else img.setPEL(false);
        img.font(sFile);
        sFileName = sFile;
        display();
    }
    void importTTFont() throws Exception {
        if (!CheckSave()) return;
        String sFileUpper = null, sFileLower = null, sTitle = "Font Editor - ";
        TTImportDialog opn = new TTImportDialog(this, "Import TrueType font", true);
        
        opn.setVisible(true);
        if (opn.isCancel()) return;
        IJFont fnt = TTFont.convert(opn.getSelectedFont(), false, 
                opn.getBaseLine(), opn.getScaleFactor(), 
                opn.getStart(), opn.getEnd());
        if (null == fnt) return;
        this.setTitle(sTitle);
        if (null == img) img = new FontImage();
        img.font(fnt);
        sFileName = "";
        display();
    }

    void importFile() throws Exception {
        if (!CheckSave()) return;
        String sFileUpper = null, sFileLower = null, sTitle = "Font Editor - ";
        ImportDialog opn = new ImportDialog(this, "Import File", true);
        IJFont fnt = null;
        
        opn.setVisible(true);
        sFileUpper = opn.upper();
        if (null == sFileUpper) return;
        if (null == sFileUpper || sFileUpper.equals("")) return;
        //System.out.println(sFile);
        sTitle += sFileUpper;
        fnt = new IJFont(new Series1Font(null, sFileUpper));
        if (opn.is18Dot()) {
            sFileLower = opn.lower();
            if (null == sFileLower) return;
            if (null == sFileLower || sFileLower.equals("")) return;
            sTitle += sFileLower;
            fnt.merge(new Series1Font(null, sFileLower));
        }
        fnt.Extract(opn.StartDot(), opn.EndDot());

        this.setTitle(sTitle);
        if (null == img) img = new FontImage();
        img.font(fnt);
        sFileName = "";
        display();
    }

    void addRemoveColumns() {
        ARColsDialog arc = new ARColsDialog(this, "Adjust number of columns", 
                                        true, img.getFontColumns());
        arc.setVisible(true);
        if(img != null && arc.isOK()) {
            NeedsSave();
            img.setColumns(arc.getColNum());
        }
    }

    void adjustCharRange() {
        if (img != null) {
            DiagraphFont df = img.font();
            if (df == null) return;
            AdjstChrsDialog dlg = new AdjstChrsDialog(this, df.startval(), df.endval());
            dlg.setVisible(true);
            if(dlg.isOK()) df.setCharRange(dlg.start(), dlg.end());
            display();
        }
    }
    void adjustCharHeight() {
        if (img != null) {
            DiagraphFont df = img.font();
            if (df == null) return;
            AdjstChrsDialog dlg = new AdjstChrsDialog(this, df.getHeight());
            dlg.setVisible(true);
            if(dlg.isOK()) df.Extract(1, dlg.end());
            display();            
        }
    }

    void display() {
        //setCursor(Cursor.WAIT_CURSOR);
        Dimension d = new Dimension(10, 10);
        mnuChar.removeAll();
        Menu mnu = mnuChar;
        DiagraphFont fnt = img.font();
        int start = fnt.startval(), end = fnt.endval();
        int ndx = 0, sze = 10;
        for (int n=start; n <= end; n++) {
            String name = String.valueOf((char)n);
            if (name.equals("-")) name = HYPHEN;
            MenuItem btn = new MenuItem(name);
            btn.setName(fnt.getCharName(n)); //Integer.toString(n));
            btn.addActionListener(this);
            mnu.add(btn);
        }
        this.invalidate();
        this.validate();
        img.setText(String.valueOf((char)start));
        //setCursor(Cursor.DEFAULT_CURSOR);
    }
    public void NeedsSave() { bNeedsSave = true; }
    protected void doNew(int dots, int cols) {
        img.setFont(new IJFont(dots, cols));
        sFileName = "";
        display();
    }
   public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if (img.getKeyCode() == key)
            img.setKeyCode(0);
        else
            img.setKeyCode(key);
   }
   public void keyReleased(KeyEvent e) { }
   public void keyTyped(KeyEvent e) { }


    public static void main(String argv[]) {
        String sFile = null;
        boolean bTest = false;
        for (int i=0; i < argv.length; i++) {
            if (argv[i].equals("-test"))
                bTest = true;
            else
                sFile = argv[i];
        }
        if (bTest)
            test.go();
        new fonted(sFile);
    }
}
