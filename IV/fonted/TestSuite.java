import java.util.*;
import java.lang.reflect.*;

public class TestSuite {
	Vector errors, tests;
	long time;
    int nAssert;

	TestSuite() {
		nAssert = 0;
		errors = new Vector();
      tests = new Vector();
		time = System.currentTimeMillis();
	}

	public void run() {
    	String sMethod = "";
    	TestCase tst;
      	for (Enumeration enm=tests.elements(); enm.hasMoreElements(); ) {
      		tst = (TestCase)enm.nextElement();
        	Method[] m = tst.getClass().getMethods();

        	for (int i=0; i < m.length; i++) {
        		sMethod = m[i].getName();
        		try {
	        		if (sMethod.startsWith("test"))
                    	m[i].invoke(tst, new Object[]{});
            	} catch (Exception e) {
            		String s = "";
            		if (e instanceof InvocationTargetException)
                		s = ((InvocationTargetException) e).getTargetException().toString();
                	else
                		s = e.toString();
                	s += " in " + tst.getClass().getName() + "." + sMethod;
            		fail(s);
            	}
        	}
      	}
   	}

   	public void addTest(TestCase x) {
    	tests.addElement(x);
   	}

	public void assert(String err, boolean b) {
    	nAssert++;
		if (!b) {
			fail(err);
		} else
			System.out.print(".");
	}

	void fail(String err) {
    	System.out.print("F");
        errors.addElement(err);
	}

	public int finish() {
		double tme = (System.currentTimeMillis() - time);
		tme = tme / 1000;

		System.out.print("\nTest results for tests: " + tests.size());
		System.out.print(", errors: " + errors.size());
        System.out.println(", assertions: " + nAssert);
		System.out.println("Time: " + tme);
		for (int i=0; i < errors.size(); i++) {
			System.out.println(i + ") " + errors.elementAt(i));
		}
		return errors.size();
	}
}
