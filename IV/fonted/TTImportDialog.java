import java.awt.*;
import java.awt.event.*;

public class TTImportDialog extends Dialog 
        implements ItemListener, ActionListener {
    Panel panel = new Panel();
    Label lblFont = new Label();
    Label lblBase = new Label();
    Label lblSize = new Label();
    Label lblScale = new Label();
    Label lblStart = new Label();
    Label lblEnd = new Label();
    Choice lstFonts = new Choice();
    Choice lstSize = new Choice();
    Choice lstStart = new Choice();
    Choice lstEnd = new Choice();
    Button cmdOK = new Button();
    Button cmdCancel = new Button();
    TextField txtBaseLine = new TextField();
    TextField txtScale = new TextField();
    Frame parent;
    Font fnt = null;
    boolean bCancel = true;

    public TTImportDialog(Frame frame, String title, boolean modal) {
        super(frame, title, modal);
        parent = frame;
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        try  {
            jbInit();
            add(panel);
            pack();
            setSize(400, 200);
            util.center(this);
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public TTImportDialog() {
        this(null, "", false);
    }

    public TTImportDialog(Frame frame) {
        this(frame, "", false);
    }

    public TTImportDialog(Frame frame, boolean modal) {
        this(frame, "", modal);
    }

    public TTImportDialog(Frame frame, String title) {
        this(frame, title, false);
    }

    void jbInit() throws Exception {
        panel.setLayout(null);

        int y = 5;
        char start = ' ', end = '~';
        
        for (char c=start; c <= end; c++) {
            lstStart.add(String.valueOf(c));
            lstEnd.add(String.valueOf(c));
        }

        lblFont.setText("Font");
        lblFont.setBounds(new Rectangle(5, y, 75, 25));
        lstFonts.setBounds(new Rectangle(75, y, 200, 25));
        Font fnts[] = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
        for (int i=0; i < fnts.length; i++) {
            lstFonts.add(fnts[i].getName());
        }

        y+= 30;
        lblSize.setText("Size");
        lblSize.setBounds(new Rectangle(5, y, 75, 25));
        lstSize.setBounds(new Rectangle(75, y, 150, 25));
        for (int i=1; i < 256; i++) {
            lstSize.add(Integer.toString(i));
        }
        lstSize.select(31);
        lstFonts.addItemListener(this);
        lstSize.addItemListener(this);

        y+= 30;
        lblBase.setText("Base Line");
        lblBase.setBounds(new Rectangle(5, y, 75, 25));
        txtBaseLine.setBounds(new Rectangle(100, y, 75, 25));
        txtBaseLine.setText("7");
        txtBaseLine.addActionListener(this);
        
        lblStart.setText("Start Char");
        lblStart.setBounds(new Rectangle(180, y, 75, 25));
        lstStart.setBounds(new Rectangle(255, y, 75, 25));
        lstStart.select(0);
        lstStart.addItemListener(this);

        y+= 30;
        lblScale.setText("Scale");
        lblScale.setBounds(new Rectangle(5, y, 75, 25));
        txtScale.setBounds(new Rectangle(100, y, 75, 25));
        txtScale.setText("1");
        txtScale.addActionListener(this);
        
        lblEnd.setText("End Char");
        lblEnd.setBounds(new Rectangle(180, y, 75, 25));
        lstEnd.setBounds(new Rectangle(255, y, 75, 25));
        lstEnd.select(end - start);
        lstEnd.addItemListener(this);

        y+= 30;
        cmdOK.setBounds(new Rectangle(25, y, 75, 25));
        cmdOK.setLabel("OK");
        cmdOK.addActionListener(this);

        cmdCancel.setBounds(new Rectangle(105, y, 75, 25));
        cmdCancel.setLabel("Cancel");
        cmdCancel.addActionListener(this);
        
        panel.add(cmdOK, null);
        panel.add(cmdCancel, null);
        panel.add(lstFonts, null);
        panel.add(lblFont, null);
        panel.add(lstSize, null);
        panel.add(txtBaseLine, null);
        panel.add(lblBase, null);
        panel.add(lblSize, null);
        panel.add(lblScale, null);
        panel.add(txtScale, null);
        panel.add(lblStart, null);
        panel.add(lstStart, null);
        panel.add(lblEnd, null);
        panel.add(lstEnd, null);
    }

    protected void processWindowEvent(WindowEvent e) {
        if(e.getID() == WindowEvent.WINDOW_CLOSING) {
            cancel();
        }
        super.processWindowEvent(e);
    }

    void cancel() {
        dispose();
    }

    public void itemStateChanged(ItemEvent e) {
        boolean bState = false;
        Object o = e.getSource();
//        if (o.equals(chk18Dot))
//            bState = true;
//        bldChoice();
    }

    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if (o.equals(cmdCancel)) {
            //txtUpper.setText("");
            //chk9Dot.setState(true);;
            setVisible(false);
        } else if (o.equals(lstSize)) {
            bCancel = false;
        } else if (o.equals(txtBaseLine)) {
            bCancel = false;
        } else if (o.equals(lstFonts)) {
            bCancel = false;
        } else if (o.equals(txtScale)) {
            bCancel = false;
        } else if (o.equals(lstStart)) {
            bCancel = false;
        } else if (o.equals(lstEnd)) {
            bCancel = false;
        } else if (o.equals(cmdOK)) {
            bCancel = false;
            setVisible(false);
        }
    }

    public Font getSelectedFont() {
        String name = lstFonts.getSelectedItem();
        int sze = lstSize.getSelectedIndex() + 1;
        fnt = new Font(name, Font.PLAIN, sze);
        return fnt;
    }
    private int getNum(TextField txt) {
        int ret = 0;
        try {
            ret = Integer.parseInt(txt.getText());
        } catch (NumberFormatException e) {
        }
        return ret;
    }
    private int getNum(Choice c) {
        return c.getSelectedItem().charAt(0);
    }
    public int getStart() {
        return getNum(lstStart);
    }
    public int getEnd() {
        return getNum(lstEnd);
    }
    public int getBaseLine() {
        return getNum(txtBaseLine);
    }
    public int getScaleFactor() {
        return getNum(txtScale);
    }
    public boolean isCancel() { return bCancel; }
}
