import java.awt.*;
import java.awt.event.*;

public class NewDlg extends Dialog implements ActionListener {
    protected int dots = 0;
    protected int cols = 0;
    /** Creates new form newDlg */
    public NewDlg(Frame parent) {
        super(parent, true);
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        initComponents();
        setSize(200, 150);
        util.center(this);
    }
   protected void processWindowEvent(WindowEvent e) {
      super.processWindowEvent(e);
      if(e.getID() == WindowEvent.WINDOW_CLOSING) dispose();
   }
   
   private void fillBox(Choice chc, int sze, int offset) {
        for (int i=0; i < sze; i++) {
            chc.addItem(Integer.toString(i+offset));
        }
   }

    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.Label lblHght;

        chcHeight = new java.awt.Choice();
        fillBox(chcHeight, 129, 0);

        lblHght = new java.awt.Label();
        cmdCancel = new java.awt.Button();
        cmdCancel.addActionListener(this);
        cmdOK = new java.awt.Button();
        cmdOK.addActionListener(this);
        lblCols = new java.awt.Label();
        chcCols = new java.awt.Choice();
        fillBox(chcCols, 100,0);

        setLayout(null);

        setModal(true);
        setTitle("New Font Height");
        chcHeight.setName("");
        add(chcHeight);
        chcHeight.setBounds(80, 30, 100, 21);

        lblHght.setText("Height");
        lblHght.setAlignment(java.awt.Label.CENTER);
        add(lblHght);
        lblHght.setBounds(20, 30, 40, 21);

        cmdCancel.setLabel("Cancel");
        add(cmdCancel);
        cmdCancel.setBounds(110, 90, 80, 25);

        cmdOK.setLabel("OK");
        add(cmdOK);
        cmdOK.setBounds(10, 90, 60, 25);

        lblCols.setText("Columns");
        add(lblCols);
        lblCols.setBounds(20, 60, 55, 21);

        add(chcCols);
        chcCols.setBounds(80, 60, 100, 21);

    }//GEN-END:initComponents
    public int dots() {
        return this.dots;
    }
    public int cols() {
        return this.cols;
    }
    /** Closes the dialog */    
    /**
     * @param args the command line arguments
     */
    public void actionPerformed(java.awt.event.ActionEvent actionEvent) {
        Object o = actionEvent.getSource();
        if (o.equals(cmdOK)) {
            dots = chcHeight.getSelectedIndex();
            cols = chcCols.getSelectedIndex();
            dispose();
        } else if (o.equals(cmdCancel)) {
            dots = 0;
            dispose();
        }
    }    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Label lblCols;
    private java.awt.Choice chcHeight;
    private java.awt.Button cmdOK;
    private java.awt.Choice chcCols;
    private java.awt.Button cmdCancel;
    // End of variables declaration//GEN-END:variables
    
}
