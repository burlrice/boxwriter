import java.io.*;
public class IJFont extends DiagraphFont {

    public IJFont(int dots, int numcols) {
        this(dots, numcols, 32, 127);
    }
    public IJFont(int dots, int numcols, int strt, int nd) {
        start = strt;
        end = nd;
        int sze = end - start + 1;
        chars = new FontChar[sze];
        sName = "New";
        iSpace = 1;
        for (int i=0; i < chars.length; i++) {
            chars[i] = new FontChar(dots);
            chars[i].setColumns(numcols, dots);
            chars[i].setName((char)(i+start));
        }
    }

    public IJFont(String path, String s) {
        super(path, s);
    }

    public IJFont(Series1Font fnt) {
        FontChar v[] = fnt.chars;
        start = fnt.start;
        end = fnt.end;
        sName = fnt.getName();
        iSpace = fnt.iSpace;
        if (null == v) return;
        chars = new FontChar[v.length];
        for (int i=0; i < chars.length; i++) {
            chars[i] = new FontChar(v[i]);
        }
    }

    public int getWidth(String s) {
        int width=0;
        FontChar b;
        for (int i=0; i < s.length(); i++) {
            if (null == chars) break;
            b = chars[s.charAt(i) - start];
            if (null != b) {
                width += b.getWidth();
                width += iSpace;
            }
        }
        return width;
    }

    public void save(String fname) {
        try {
            ByteArrayOutput file = new ByteArrayOutput();
            file.writeShort(VERSION);
            file.writeShort((short)getHeight());

            file.writeShort((short)start);
            file.writeShort((short)end);
            // write out index table
            int col = 0, i;
            for (i=start; i <= end; i++) {
                if (null != chars[i - start]) {
                    col += chars[i - start].getWidth();
                    file.writeInt(col);
                } else {
                    util.showError(new NullPointerException());
                }
            }
            // write final value
//            if (0 < col) file.writeInt(col);

            for (i=0; i < chars.length; i++) {
                if (null != chars[i]) {
                    for (int j=0; j < chars[i].getWidth(); j++)
                        file.write(chars[i].getValue(j));
                }
            }
            file.writeTo(new FileOutputStream(new File(fname)));
            file.close();
        } catch (Exception e) {
            util.showError(e);
        }
    }

    // version number
    final short VERSION = 2;

    protected void load(String s) {
        int i, n, j;
        long size[];
        try {
            ByteArrayInput file = new ByteArrayInput(s);
            int version = file.readUnsignedShort();
            if (VERSION < version) return;
            int dots = file.readUnsignedShort();
            int bytespercol = FontColumn.getStorageSize(dots);
            if (VERSION > version) bytespercol = (int)Math.ceil(dots / 8.0);
            start = file.readUnsignedShort();
            end = file.readUnsignedShort();
            size = new long[end - start + 1];

//            size[0] = file.readInt();
            for (i=1; i <= (end - start); i++) {
                size[i] = file.readInt();
                size[i - 1] = size[i] - size[i - 1];
            }
            n = file.readInt();
            size[i - 1] = n - size[i - 1];
//            size[i] = file.size() - (6 + n * 4);
            // size of a column in bytes
            chars = new FontChar[size.length];
            for (i=0; i < chars.length; i++) {
                chars[i] = new FontChar(file, (int)size[i], dots, bytespercol);
                chars[i].setName((char)(i + start));
            }
            file.close();
        } catch (Exception e) {
            util.showError(e, true);
        }
    }

    protected String strip(String src) {
        String s = src.toLowerCase();
        int i = s.indexOf('l');
        if (0 <= i)
            s = s.substring(0, i) + s.substring(i+1);
        i = s.indexOf('u');
        if (0 <= i)
            s = s.substring(0, i) + s.substring(i+1);
        return s;
    }

    public void merge(Series1Font fnt) throws Exception {
        FontChar[] v = fnt.chars;
        String sTmp = "";

        if (fnt.start != start) throw new Exception("invalid start");
        if (fnt.end != end) throw new Exception("invalid end");
        if (fnt.iSpace != iSpace) throw new Exception("invalid space");
        if (null == v) throw new NullPointerException();
        if (v.length != chars.length) throw new Exception("invalid length");

        sName = strip(sName);
        sTmp = strip(fnt.getName());
        sName = sTmp;
        for (int i=0; i < chars.length; i++) {
            chars[i].append(v[i]);
            chars[i].setName((char)(i+start));
        }
    }
}


