import java.awt.*;
import java.io.*;

public class FontChar {
    FontColumn[] cols;
    char name;

    public FontChar(int dots) {
        cols = new FontColumn[1];
        cols[0] = new FontColumn(dots);
    }

    public FontChar(FontChar fc) {
        int len = fc.getWidth();
        cols = new FontColumn[len];
        name = fc.getName();
        FontColumn c;
        int dots = fc.getHeight();
        for (int i=0; i < len; i++) {
            cols[i] = new FontColumn(dots, fc.getValue(i));
        }
    
    }

    public FontChar (ByteArrayInput f, int numcols, int dots, int bytespercol)
                        throws IOException {
        cols = new FontColumn[numcols];
        for (int i=0; i < numcols; i ++)
            cols[i] = new FontColumn(f, dots, bytespercol);
    }

    public void setValue(int i, byte[] b) {
        cols[i].setData(b);
    }
    public byte[] getValue(int i) {
/*      if (null == cols[i]) {
            System.out.println((int) name);
            return null;
        }*/
        if (i < cols.length)
            return cols[i].getData();
        return null; 
    }
    public void setName(char c) {
        name = c;
    }
    public char getName() {
        return name;
    }
    public void draw(Graphics g, Rectangle r, int col, int mag, int ratio) {
        for (int i=0; i < getWidth(); i++) {
            cols[i].draw(g, r, col++, mag, ratio);
        }
    }
    public int getHeight() {
        int x=0;
        for (int i=0; i < cols.length; i++) {
            if(null == cols[i])
                throw new NullPointerException(Integer.toString(name) +
                                                Integer.toString(i));
            x = Math.max(x, cols[i].getDots());
        }
        return x;
    }

    public void flipdot(int col, int dot) {
        if(col >= 0 && col < cols.length) cols[col].flipdot(dot);
    }
    public FontChar (String s) {
        if (null == s) return;
        byte[] b;
        int dots = 9, j = 0;
        char c;
        int len = s.length();
        cols = new FontColumn[(int)Math.ceil(len / 2.0)];
        for (int i=0; i < len; i += 2) {
            b = new byte[2];
            c = extractCol(s.charAt(i), ((i+1) < len ? s.charAt(i + 1) : 0));
            b[0] = (byte)(c >> 8);
            b[1] = (byte)(c & 0x00ff);
            cols[j++] = new FontColumn(dots, b);
        }
    }

    private char extractCol(int iOdd, int iEven) {
        char x=0;
        if ((iOdd & 32) != 0) x |= 1;
        if ((iEven & 32) != 0) x |= 2;
        if ((iOdd & 16) != 0) x |= 4;
        if ((iEven & 16) != 0) x |= 8;
        if ((iOdd & 8) != 0) x |= 16;
        if ((iEven & 8) != 0) x |= 32;
        if ((iOdd & 4) != 0) x |= 64;
        if ((iEven & 4) != 0) x |= 128;
        if ((iOdd & 2) != 0) x |= 256;
        return x;
    }

    public void append(FontChar fc) throws Exception {
        int i;
        byte[] b;
        int size = Math.max(getWidth(), fc.getWidth());
        int dots;
        if (size > cols.length) {
            FontColumn[] v = new FontColumn[size];
            for (i=0; i < cols.length; i++) {
                v[i] = cols[i];
            }
            dots = getHeight();

            int storage = FontColumn.getStorageSize(dots);
            for (; i < size; i++)
                v[i] = new FontColumn(dots, new byte[storage]);
            cols = v;
        }
        dots = fc.getHeight();
        for (i=0; i < cols.length; i++) {
            b = fc.getValue(i);
            if (null != b) cols[i].append(b, dots);
        }
    }

    public int getByteSize() {
        int x = 0;
        for (int i=0; i < cols.length; i++) {
            x += cols[i].getData().length;
        }
        return x;
    }
    public void trim(int start, int end) {
        for (int i=0; i < cols.length; i++) 
            cols[i].trim(start, end);
    }

    public int getWidth(){
        return cols.length;
    }

    public void setColumns(int ndx, int height){
        FontColumn[] fc = new FontColumn[ndx];
        int size = 0;
        for(int i = 0; i < cols.length; i++){
            if(cols[i] != null) height = cols[i].getDots();
        }
        if(ndx < cols.length) {
            size = ndx;
        } else {
            int sze = FontColumn.getStorageSize(height);
            size = cols.length;
            byte [] b;
            for(int i = size; i < ndx; i++){
                b = new byte[sze];
                fc[i] = new FontColumn(height, b);
            } 
        }
        for(int i = 0; i < size; i++){
            fc[i] = cols[i];
        }
        cols = fc;
    }
    FontColumn getColumn(int i) {
        return cols[i];
    }
}

