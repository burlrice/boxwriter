import java.awt.*;
import java.awt.event.*;

public class FontImage extends Component implements MouseListener {
    String text = "";
    DiagraphFont font;
    Rectangle home, rec;
    int mag=10, ratio=1, keyCode=0;

    public FontImage(boolean b, Rectangle r) {
        enableEvents(AWTEvent.COMPONENT_EVENT_MASK);
        addMouseListener(this);
        home = r;
        //rec = new Rectangle(getLocation(), getSize());
        setPEL(b);
    }

    public FontImage() {
        this(false, new Rectangle());
    }
    public void setMaximumSize(Dimension d) {
        home.setSize(d);
    }

    public void setLocation(int x, int y) {
        super.setLocation(x,y);
        home.setLocation(x,y);
    }
    public void setLocation(Point p) {
        super.setLocation(p);
        home.setLocation(p);
    }
        

    public void setPEL(boolean b) {
        if (b) ratio = 6;
        else ratio = 1;
    }

    public void setText(String s) {
        text = s;
        resized();
    }

    public void font(String f) {
        font = new IJFont(null, f);
        //font = new IJFont(new Series1Font(f));
    }

    public void font(DiagraphFont f) {
        setFont(f);
    }

    public void setFont(DiagraphFont f) {
        font = f;
        resized();
    }

    public DiagraphFont font() {
        return font;
    }

    public int getFontColumns(){
        if(text != null && text.length() >= 1 && font != null)
        return font.getFontColumns(text.charAt(0));
        else return 0;
    }

    public void setColumns(int c){
        if(text != null && text.length() >= 1 && font != null){
            if(c > getFontColumns()){
                for(int i = getFontColumns(); i <= c; i++){
                    font.setFontColumns(text.charAt(0), i);
                }
            }
            else font.setFontColumns(text.charAt(0), c);
            resized();
        }
    }

    protected void processComponentEvent(ComponentEvent e) {
         super.processComponentEvent(e);
         if (e.getID() == ComponentEvent.COMPONENT_RESIZED) {
             this.setMaximumSize(getSize());
             this.resized();
         }
    }


   protected void resized() {
        int width = 0;
        rec = new Rectangle(home);
        width = getFontColumns();//font.getMaxWidth();
        if (null == font) return;
        int h = font.getHeight();
        if ((0 < h) && (0 < width)) {
            mag = Math.min(rec.width/width*ratio, rec.height/h) / ratio;
            rec.width = width * mag;
            rec.height = h * mag * ratio;
            //setLocation(rec.getLocation());
            //setSize(rec.width, rec.height);
        }
        repaint();
   }

    public void paint(Graphics g) {
        super.paint(g);
        //g.drawRect(home.x, home.y, home.width-1, home.height-1);
        for (int i=rec.x; i < rec.x+rec.width; i+=mag) {
            g.drawLine(i, rec.y, i, rec.y+rec.height);
        }
        for (int j=rec.y; j < rec.y+rec.height; j+=mag*ratio) {
            g.drawLine(rec.x, j, rec.x+rec.width, j);
        }
        g.drawRect(rec.x, rec.y, rec.width, rec.height);
        if (null != font)
            font.drawString(g, rec, text, mag, ratio);
        //else
        //      System.err.println("Font is null");
    }

   public void mousePressed(MouseEvent e) {
        Point p = e.getPoint();
        if ((p.x < rec.x) || (p.x > (rec.x + rec.width)) ||
                (p.y < rec.y) || (p.y > (rec.y + rec.height))) return;

        int col, row;
        col = (p.x-rec.x) / mag;
        row = (p.y-rec.y) / (mag * ratio);
        //System.out.println(col + " " + row);
        if (null != text && 1 <= text.length() && null != font) {
            char c = text.charAt(0);
            if (0 == keyCode) {
                font.flipDot(c, col, row);
            } else if (KeyEvent.VK_C == keyCode) {
                FontColumn fc = font.getColumn(c, col);
                if (null == fc) return;
                int start = 0, h = fc.getDots(), last = h + 1;
                boolean b = fc.getdot(row);
                for (int i=row; i >= 0; i--) {
                    if (b != fc.getdot(i)) {
                        start = i+1;
                        break;
                    }
                }
                for (int i=row; i < h; i++) {
                    if (b != fc.getdot(i)) {
                        last = i;
                        break;
                    }
                }
                for (int i=start; i < last; i++) fc.flipdot(i);
            }
            else if (KeyEvent.VK_U == keyCode) {
                FontColumn fc = font.getColumn(c, col);
                if (null == fc) return;
                int start = 0, h = fc.getDots(), last = h + 1;
                boolean b = fc.getdot(row);
                for (int i=row; i >= 0; i--) {
                    if (b != fc.getdot(i)) {
                        start = i+1;
                        break;
                    }
                }
                last = row+1;
                for (int i=start; i < last; i++) fc.flipdot(i);
            }
            else if (KeyEvent.VK_D == keyCode) {
                FontColumn fc = font.getColumn(c, col);
                if (null == fc) return;
                int start = 0, h = fc.getDots(), last = h + 1;
                boolean b = fc.getdot(row);
                start = row;
                for (int i=row; i < h; i++) {
                    if (b != fc.getdot(i)) {
                        last = i;
                        break;
                    }
                }
                for (int i=start; i < last; i++) fc.flipdot(i);
            }
        }
        repaint();
        ((fonted)getParent()).NeedsSave();
    }
   public void setKeyCode(int code) { 
       keyCode = code;
   }
   public int getKeyCode() { return keyCode; }

   public void mouseReleased(MouseEvent e) {}
   public void mouseExited(MouseEvent e) {}
   public void mouseEntered(MouseEvent e) {}
   public void mouseClicked(MouseEvent e) {}
}
