import java.io.*;
import java.awt.*;

public class FontColumn {
    byte[] vals;
    int storage;
    int dots;
    public static int getStorageSize(int d) {
        return 2*(int)Math.ceil(d / 16.0);
        //return (int)Math.ceil(d / 8.0);
    }

    public FontColumn(int d) {
        dots = d;
        storage = getStorageSize(d);
        vals = new byte[storage];
    }

    public FontColumn(ByteArrayInput f, int s, int bytespercol)
                    throws IOException {
        this(s);
        vals = new byte[storage];
        int start = Math.abs(storage - bytespercol);
        for (int i=start; i < storage; i ++)
            vals[i] = f.readByte();
    }
    public FontColumn(int d, byte[] b) {
        this(d);
        setData(b);
    }
    public void setData(byte[] b) {
        vals = b;
    }
    public int getStorage() {
        return storage;
    }

    public byte[] getData() {
        return vals;
    }

    void draw(Graphics g, Rectangle r, int col, int mag, int ratio) {
        int row = dots, mask = 0;
        for (int i=vals.length-1; i >= 0; i--) {
            mask = 0;
            for (int j=0; j < 8; j++) { // 8 is because of byte alignment
                mask = 1 << j;
                if (((vals[i] & mask) == mask) && ((col <= r.width))) {
                    int x = col * mag + r.x;
                    int y = (dots - row) * mag * ratio + r.y;
                    int yend = (dots - row + 1) * mag * ratio + r.y;
                    for (int k=y; k < yend; k+=mag)
                        g.fillOval(x, k, mag, mag);
                }
                row--;
                if (0 >= row) return; // number of dots has been reached
            }
        }
    }

    public void flipdot(int row) {
        int ndx = Math.max(vals.length - row / 8 - 1, 0);
        if ((vals.length - 1) < ndx) return;
        int x = vals[ndx];
        if ((x & (1 << (row % 8))) == 0)
            x |= (1 << (row % 8));
        else {
            x &= (~ (1 << (row % 8)));
        }
        vals[ndx] = (byte)x;
    }
    public boolean getdot(int row) {
        int ndx = Math.max(vals.length - row / 8 - 1, 0);
        if ((vals.length - 1) < ndx) return false;
        int x = vals[ndx];
        int mask = 1 << (row % 8);
        return ((x & mask) == 0);
    }

    public int getDots() { return dots; }

    public void append(byte[] b, int newdots) {
        byte[] data = new byte[storage + 2 + b.length + 2];
        int ndx = 0;
        data[ndx++] = 0;
        data[ndx++] = (byte)newdots;
        for (int i=0; i < b.length; i++)
            data[ndx++] = b[i];
        data[ndx++] = (byte)dots;
        data[ndx++] = (byte)dots;
        for (int i=0; i < vals.length; i++)
            data[ndx++] = vals[i];

        dots += newdots;
        byte[] x = BuildColumn(data, getStorageSize(dots));
//        System.out.print(helper.printBuf(x, x.length));
        vals = x;
        storage = vals.length;
    }
    byte[] BuildColumn(byte[] data, int sze) {
        byte[] c = new byte[sze];
        c[0] |= data[2] << 1;
        c[0] |= (data[3] >> 7) & 0x01;
        c[1] |= data[3] << 1;

        c[1] |= data[6];
        c[2] |= data[7];
        return c;
    }

    public void trim(int start, int end) {
        if ((1 == start) && (end >= dots)) return;
        FontColumn fc = new FontColumn(end - start + 1);
        int k = 0;
        for (int i = start; i <= end; i++) {
            if (!getdot(i - start)) 
                fc.flipdot(k);
            k++;
        }
        setData(fc.getData());
        dots = fc.getDots();
        storage = fc.getStorage();
    }
}
