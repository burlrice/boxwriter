import java.awt.*;
import java.awt.event.*;

public class ARColsDialog extends Dialog implements ActionListener {
    Panel panel = new Panel();
    Button cmdOK = new Button();
    Button cmdCancel = new Button();
    Frame parent;
    Label lblNumCols = new Label();
    Choice chcNumCols = new Choice();
    boolean bIsOK = false;

    static int end = 256;

    public ARColsDialog(Frame frame, String title, boolean modal, int fc) {
        super(frame, title, modal);
        parent = frame;
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        try  {
            jbInit(fc);
            add(panel);
            pack();
            setSize(300, 200);
            util.center(this);
            //System.out.println("ARColsDialog constructed..."); 
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public ARColsDialog(Frame frame, String title, boolean modal) {
        this(frame, title, modal, 0);
    }

    public ARColsDialog() {
        this(null, "", false);
    }

    public ARColsDialog(Frame frame) {
        this(frame, "", false);
    }

    public ARColsDialog(Frame frame, boolean modal) {
        this(frame, "", modal);
    }

    public ARColsDialog(Frame frame, String title) {
        this(frame, title, false);
    }

    void jbInit(int fc) throws Exception {
        panel.setLayout(null);

        cmdCancel.setBounds(new Rectangle(40, 100, 100, 25));
        cmdCancel.setLabel("Cancel");
        cmdCancel.addActionListener(this);

        cmdOK.setBounds(new Rectangle(150, 100, 100, 25));
        cmdOK.setLabel("OK");
        cmdOK.addActionListener(this);

        lblNumCols.setText("Number of Columns:");
        lblNumCols.setBounds(new Rectangle(40, 50, 125, 25));
        chcNumCols.setBounds(new Rectangle(175, 50, 75, 25));
        bldChoice(fc);

        panel.add(lblNumCols);
        panel.add(chcNumCols);
        panel.add(cmdOK, null);
        panel.add(cmdCancel, null);
    }

    protected void bldChoice(int fc){
        String tmp;
        chcNumCols.removeAll();
        for (int i=0; i <= end; i++) {
            tmp = Integer.toString(i);
            chcNumCols.add(tmp);
            if (i == fc) chcNumCols.select(fc);
        }
    }

    protected void processWindowEvent(WindowEvent e) {
        if(e.getID() == WindowEvent.WINDOW_CLOSING) {
            cancel();
        }
        super.processWindowEvent(e);
    }

    void cancel() {
        bIsOK = false;
        dispose();
    }

    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if (o.equals(cmdCancel)) {
            bIsOK = false;
            setVisible(false);
        } else if (o.equals(cmdOK)) {
            bIsOK = true;
            setVisible(false);
        }
    }

    public int getColNum(){
        return Integer.parseInt(chcNumCols.getSelectedItem());
    }
    public boolean isOK() {
        return bIsOK;
    }
}
