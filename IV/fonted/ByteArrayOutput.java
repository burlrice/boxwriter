import java.io.*;

public class ByteArrayOutput extends ByteArrayOutputStream {

	public ByteArrayOutput() {
    	super();
	}
    public void writeInt(int i) {
    	byte[] b = new byte[4];
        int m = i;
        for (int j=0; j < 4; j++) {
        	b[j] = (byte)((i >> ((3 - j) * 8)) & 0xff);
        }
        write(b, 0, b.length);
    }
    public void writeShort(int v) {
    	byte b = (byte)(v >> 8);
    	write(b);
        b = (byte)(v & 0xff);
        write(b);
    }
}
