#include "iv.h"

// masks of register INT_REG_DOWNLOAD
#define DONE_MASK          (0x04)
#define INIT_MASK          (0x10)

unsigned long Position = 0;
unsigned char Encoder = INTERNAL;
extern unsigned char * getIVFPGAptr(void);
unsigned short *imagePtr;

enum {REFRESH=0, SENDCOLUMN};
unsigned char TaskState = REFRESH;

enum {RIGHT_TO_LEFT=0, LEFT_TO_RIGHT};
char printDir = LEFT_TO_RIGHT;
char wordDir;
char wordsPerCol;
char wordsToNextCol;
unsigned short shiftcntl1;
unsigned short shiftcntl2;

enum {NONE=0, IV_9DOT, IV_18DOT};
#define TICKS_IV (19)
struct printheadStruct {
  unsigned char type;
  unsigned char direction;
  unsigned short offset;
  unsigned char orfice_spacing;
};

#define MAX_HEADS 8
struct printheadStruct printhead[MAX_HEADS] =
{
 {IV_18DOT,LEFT_TO_RIGHT,0,TICKS_IV},
 {IV_18DOT,LEFT_TO_RIGHT,0,TICKS_IV},
 {IV_18DOT,LEFT_TO_RIGHT,0,TICKS_IV},
 {IV_18DOT,LEFT_TO_RIGHT,0,TICKS_IV},
 {NONE,LEFT_TO_RIGHT,0,TICKS_IV},
 {NONE,LEFT_TO_RIGHT,0,TICKS_IV},
 {NONE,LEFT_TO_RIGHT,0,TICKS_IV},
 {NONE,LEFT_TO_RIGHT,0,TICKS_IV}
};

void flip_bytes( void *pv, int len ) {
  char *p = (char *)pv;
  int i, j;
  char tmp;
  for (i=0, j=len-1; i<j; i++, j--) {
    tmp = p[i];
    p[i] = p[j];
    p[j] = tmp;
  }
}

extern unsigned char * getImagePtr(void);
void CardIrq (void) {
    short temp;
    short bpp;
    volatile char wordCnt;
    unsigned short* colData;
    if (Position) { // >= 2nd interrupt [OnEncoderPulse]
        /* send column */
        Position--;
        WriteWord(BASE_ADDR+SHIFT_CNTL, shiftcntl1);
        WriteWord(BASE_ADDR+SHIFT_CNTL+2, shiftcntl2);
        wordCnt = wordsPerCol;
        colData = imagePtr; /* get pointer to column */
        while (2 < wordCnt) {
            /*delay*/wordCnt--;
            /*delay*/wordCnt--;
            WriteWord(BASE_ADDR+DATA_PORT, *(colData));
            colData+=wordDir; /* move pointer to next word in column */
            WriteWord(BASE_ADDR+DATA_PORT+2, *(colData));
            colData+=wordDir; /* move pointer to next word in column */
        }
        WriteWord(BASE_ADDR+DATA_PORT, *(colData)); /* last word in column */
        WriteWord(BASE_ADDR+SHIFT_CNTL, 0x01FF); /* dummy start dot is 511 */
        WriteWord(BASE_ADDR+SHIFT_CNTL+2, 1); /* dummy height is 1 */
        imagePtr+=wordsToNextCol; /* move image pointer to next column */
     } else  {
        /* refresh image */
        if (SENDCOLUMN == TaskState) { // end of image
            /* irq = photocell */
            WriteByte(BASE_ADDR+PRINT_STATUS, (ReadByte(BASE_ADDR+PRINT_STATUS)&(~(1<<IRQ_SEL)))|(PHOTO_IRQ<<IRQ_SEL));
            /* set dpi to max */
            WriteByte(BASE_ADDR+DPI_IRQ, 1);
            TaskState = REFRESH;
            return;
        }
        if ((REFRESH == TaskState) // 1st interrupt [OnPhotocell]
            /* photocell(D0) */
            && (ReadByte(BASE_ADDR+READ_IO)&(1<<PHOTO))
           ) {
            /* get image type from bmp header */
            temp = *((unsigned short *)getImagePtr());
            #ifndef WIN32
            flip_bytes(&temp,sizeof(temp));
            #endif
            /* get bits per pixel from bmp header */
            bpp = *((unsigned short *)(getImagePtr() + 26));
            #ifndef WIN32
            flip_bytes(&bpp,sizeof(bpp));
            #endif
            if((0x4D42==temp) && (1==bpp)){
                wordDir = 1;
                /* get image height from bmp header */
                temp = *((unsigned short *)(getImagePtr() + 18));
                #ifndef WIN32
                flip_bytes(&temp,sizeof(temp));
                #endif
                shiftcntl1 = (0<<VFLIP); /* start dot = 0 */
                shiftcntl2 = (1<<BMP)|(0<<DRAFT)|temp; /* bmp on, draft off, height */
                wordsPerCol = ((temp-1)/16)+1;
                if(0 != (wordsPerCol%2)){
                    wordsToNextCol = wordsPerCol/2;
                    wordsToNextCol = (wordsToNextCol+1)*2;
                    wordsPerCol = wordsToNextCol;
                }else{
                    wordsToNextCol = wordsPerCol;
                }
                /* get image length from bmp header */
                temp = *((unsigned short *)(getImagePtr() + 22));
                #ifndef WIN32
                flip_bytes(&temp,sizeof(temp));
                #endif
                Position = temp; /* set Position to length of image (cols) */
                temp = *((unsigned short *)(getImagePtr() + 10));
                #ifndef WIN32
                flip_bytes(&temp,sizeof(temp));
                #endif
                imagePtr = (unsigned short *)(getImagePtr() + temp);
                if (LEFT_TO_RIGHT == printDir) {
                    imagePtr += ((Position-1) * wordsPerCol);
                }
                wordsToNextCol *= -1;
            }
            /* set DPI of image */
            WriteByte(BASE_ADDR+DPI_IRQ, 5); /* 100dpi/5 = 20dpi */
            /* irq = column */
            WriteByte(BASE_ADDR+PRINT_STATUS, (ReadByte(BASE_ADDR+PRINT_STATUS)&(~(1<<IRQ_SEL)))|(COLUMN_IRQ<<IRQ_SEL));
            TaskState = SENDCOLUMN;
            return;
        }
    }
}

void setPHOffset(unsigned short BaseAddr, int ndx, unsigned short val)  {
    flip_bytes(&val, sizeof(val));
    WriteByte(BaseAddr+CMD, 0x01); /* Page 1 */
    WriteWord(BaseAddr+PH_OFFSETS+(2 * ndx), val);
    WriteByte(BaseAddr+CMD, 0x00); /* Page 0 */
}

void StorePHConfig(struct printheadStruct printhead[], unsigned short BaseAddr) {
    unsigned short type = 0;
    int size=0, sector=0;
    int i = 0;
    for (i=0; i < MAX_HEADS; i++) {
        switch (printhead[i].type) {
            case IV_9DOT:
                if(LEFT_TO_RIGHT == printhead[i].direction) {
                    type = (type << 1) | 0x0001;
                    setPHOffset(BaseAddr, sector++, printhead[i].offset + printhead[i].orfice_spacing);
                } else {
                    type = (type << 1) | 0x0000;
                    setPHOffset(BaseAddr, sector++, printhead[i].offset);
                }
                size += 1;
                break;
            case IV_18DOT:
                if(LEFT_TO_RIGHT == printhead[i].direction) {
                    type = (type << 1) | 0x0001;
                    setPHOffset(BaseAddr, sector++, printhead[i].offset + printhead[i].orfice_spacing);
                    type = (type << 1) | 0x0000;
                    setPHOffset(BaseAddr, sector++, printhead[i].offset);
                } else {
                    type = (type << 1) | 0x0000;
                    setPHOffset(BaseAddr, sector++, printhead[i].offset);
                    type = (type << 1) | 0x0001;
                    setPHOffset(BaseAddr, sector++, printhead[i].offset + printhead[i].orfice_spacing);
                }
                size += 2;
                break;
        }
    }
    if (8 > size) type = type << (8 - size);
    WriteWord(BaseAddr+HEAD_TYPES, type);
}

void setLineSpeed(unsigned short BaseAddr, int val) {
        unsigned long scalar = 0;
        unsigned short RegisterVal;
        if (val != 0) {
            scalar = 16000000/(val*20*2);
        }
        RegisterVal=scalar&0x0000FFFF;
        flip_bytes(&RegisterVal, sizeof(RegisterVal));
        WriteWord(BaseAddr+LINE_SPEED, RegisterVal);
        RegisterVal=ReadWord(BaseAddr + LINE_SPEED);
        flip_bytes(&RegisterVal, sizeof(RegisterVal));

        RegisterVal=scalar>>16;
        flip_bytes(&RegisterVal, sizeof(RegisterVal));
        WriteWord(BaseAddr+LINE_SPEED+2, RegisterVal);
        RegisterVal=ReadWord(BaseAddr+LINE_SPEED+2);
        flip_bytes(&RegisterVal, sizeof(RegisterVal));
}

int getLineSpeed(unsigned short BaseAddr) {
    unsigned long scalar;
    unsigned short RegisterVal;
    RegisterVal=ReadWord(BaseAddr + LINE_SPEED);
    flip_bytes(&RegisterVal, sizeof(RegisterVal));
    scalar=RegisterVal;
    RegisterVal=ReadWord(BaseAddr + LINE_SPEED+2);
    flip_bytes(&RegisterVal, sizeof(RegisterVal));
    RegisterVal&=0x000F;
    scalar += RegisterVal*0x10000;
    scalar = (scalar)*20*6;
    if (scalar != 0) {
        return 16000000/scalar;
    } else {
        return 0;
    }
}

///////////////////////////////////////////////////
// Download XILINX code
//////////////////////////////////////////////////
unsigned char binary_download(unsigned short BaseAddr, unsigned char* fpga, unsigned long fpga_size)
{
    int byte_cnt;
    unsigned char Byte;
    unsigned char* BytePtr;
    /* toggle the program signal */
    WriteByte(BaseAddr+GAL_REG,0xFF);
    WriteByte(BaseAddr+GAL_REG,0);
    WriteByte(BaseAddr+GAL_REG,0xFF);
    #ifdef DELAY
    /* allow FPGA enough time to clear its memory, at least 500uS */
    delay(1); /* delay 1ms */
    #else
    /* poll INIT siganl until memory is clear */
    while (0 == (ReadByte(BaseAddr+GAL_REG) & INIT_MASK));
    #endif
    /* download configuration bytes*/
    BytePtr = fpga;
    for (byte_cnt=0; byte_cnt<fpga_size; byte_cnt++) {
        /* write configuration byte */
        WriteByte(BaseAddr, *BytePtr);
        BytePtr++;
    }
    /* need 10 more clock cycles, one for each startup sequence */
    for (byte_cnt=0; byte_cnt<10; byte_cnt++) {
        /* write dummy byte */
        WriteByte(BaseAddr, 0X00);
    }
    Byte = ReadByte(BaseAddr+GAL_REG);
    Byte &= DONE_MASK;
    return(Byte);
}

////////////////////////////////////////////////////////////////////////////
//
// ClearBuffer
//
////////////////////////////////////////////////////////////////////////////
void ClearBuffer(unsigned short BaseAddr)
{
    int col_cnt;
    unsigned char Dpi;
    unsigned char PrintStatus, tmp;
    /* set CW to 1 or 100 dpi */
    Dpi=ReadByte(BaseAddr+DPI_IRQ);
    WriteByte(BaseAddr+DPI_IRQ, 1);
    PrintStatus = ReadByte(BaseAddr+PRINT_STATUS);
    /* pause  */
    WriteByte(BASE_ADDR+PRINT_STATUS, ReadByte(BASE_ADDR+PRINT_STATUS)&(~(1<<UNPAUSE)));
    /* irq = column */
    WriteByte(BASE_ADDR+PRINT_STATUS, (ReadByte(BASE_ADDR+PRINT_STATUS)&(~(1<<IRQ_SEL)))|(COLUMN_IRQ<<IRQ_SEL));
    /* toggle print reset       */
    WriteByte(BASE_ADDR+PRINT_STATUS, ReadByte(BASE_ADDR+PRINT_STATUS)&(~(1<<RESET)));
    WriteByte(BASE_ADDR+PRINT_STATUS, ReadByte(BASE_ADDR+PRINT_STATUS)|(1<<RESET));
    WriteByte(BASE_ADDR+PRINT_STATUS, ReadByte(BASE_ADDR+PRINT_STATUS)&(~(1<<RESET)));
    // write blank columns to ram data port
    for (col_cnt=0; col_cnt<0x8000; col_cnt++){
        /* inc column counter */
        WriteByte(BASE_ADDR+PRINT_STATUS, ReadByte(BASE_ADDR+PRINT_STATUS)|(1<<INC_COL_CNT));
        ReadByte(BaseAddr+READ_IO); //clear COL_RESET
        WriteByte(BASE_ADDR+PRINT_STATUS, ReadByte(BASE_ADDR+PRINT_STATUS)&(~(1<<INC_COL_CNT)));
        WriteWord(BaseAddr+SHIFT_CNTL, 0x01FF); /* dummy start dot is 511 */
        WriteWord(BaseAddr+SHIFT_CNTL+2, 1); /* dummy height is 1 */
    }
    WriteByte(BaseAddr+PRINT_STATUS, PrintStatus);
    WriteByte(BaseAddr+DPI_IRQ, Dpi);
}

#define TYPEMASK           (0x03)
int PrintheadInterfaceInit(unsigned short BaseAddr) {
    unsigned char* fpga;
    unsigned long fpga_size;

    if ((ReadByte(BaseAddr+GAL_REG)&TYPEMASK)==0X02){
        #ifdef WIN32
        FILE * in = fopen("2465121.bin", "r");
        fpga = ((vfpair *)in)->pos;
        #else
        fpga = getIVFPGAptr();
        #endif
        fpga_size = 0x1FBDC;
    } else {
        return -1;
    }
    /* download FPGA configuration */
    if (binary_download(BaseAddr, fpga, fpga_size)) {
        /* Page 0 */
        WriteByte(BaseAddr+CMD, 0x00);
        /* toggle print reset */
        WriteByte(BASE_ADDR+PRINT_STATUS, ReadByte(BASE_ADDR+PRINT_STATUS)&(~(1<<RESET)));
        WriteByte(BASE_ADDR+PRINT_STATUS, ReadByte(BASE_ADDR+PRINT_STATUS)|(1<<RESET));
        WriteByte(BASE_ADDR+PRINT_STATUS, ReadByte(BASE_ADDR+PRINT_STATUS)&(~(1<<RESET)));
        /* clear image SRAM */
        ClearBuffer(BaseAddr);
        /* printing=pause, encoder=external, irq=DPI, sharing=none */
        if (INTERNAL == Encoder) {
            setLineSpeed(BaseAddr, 50); /* default to 50 ft/min */
            WriteByte(BASE_ADDR+PRINT_STATUS, (ReadByte(BASE_ADDR+PRINT_STATUS)&(~(1<<ENCODER_SEL)))|(INTERNAL<<ENCODER_SEL));
        }
        else {
            WriteByte(BASE_ADDR+PRINT_STATUS, (ReadByte(BASE_ADDR+PRINT_STATUS)&(~(1<<ENCODER_SEL)))|(EXTERNAL<<ENCODER_SEL));
        }
        /* set printhead types */
        StorePHConfig(printhead, BaseAddr);
        /* set encoder ticks per column */
        WriteByte(BaseAddr+DIV_ENCODER_BY, 6);
        /* unpause printing */
        WriteByte(BASE_ADDR+PRINT_STATUS, ReadByte(BASE_ADDR+PRINT_STATUS)|(1<<UNPAUSE));
        /* irq = photocell */
        WriteByte(BASE_ADDR+PRINT_STATUS, (ReadByte(BASE_ADDR+PRINT_STATUS)&(~(1<<IRQ_SEL)))|(PHOTO_IRQ<<IRQ_SEL));
        WriteByte(BaseAddr+DPI_IRQ, 1);
    } else {
        return -2;
    }
    return 0;
}

// End of file.
