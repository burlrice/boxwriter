#define BASE_ADDR (0x220)
// I/O register addresses
#define GAL_REG            (0x01)
#define CMD                (0x02)
#define READ_IO            (0x02)
/* Page 0 Registers*/
#define DATA_PORT          (0x04)
#define DIV_ENCODER_BY     (0x08)
#define PRINT_STATUS       (0x0A)
#define DPI_IRQ            (0x0C)
#define SHIFT_CNTL         (0x0E)
#define LINE_SPEED         (0x14)
#define HEAD_TYPES         (0x1A)
/* Page 1 Registers*/
#define PH_OFFSETS         (0x04)

enum {RESET=0, ENCODER_SEL, UNPAUSE, IRQ_SEL, PHOTO_SHARING, ENCODER_SHARING, INC_COL_CNT=7};
enum {EXTERNAL=0, INTERNAL};
enum {COLUMN_IRQ=0, PHOTO_IRQ};
enum {SHARING_OFF=0, SHARING_ON};
/* PRINT_STATUS
 * x x x x  x x x x
 * | | | |  | | | |-> RESET, (active high)
 * | | | |  | | |---> ENCODER_SEL, EXTERNAL(0)/INTERNAL(1)
 * | | | |  | |-----> UNPAUSE, (active high)
 * | | | |  |-------> IRQ_SEL, COLUMN_IRQ(0)/PHOTO_IRQ(1)
 * | | | |----------> PHOTO_SHARING, (active high)
 * | | |------------> ENCODER_SHARING, (active high)
 * | |--------------> RESERVED
 * |----------------> INC_COL_CNT, (active high)
 */

enum {PHOTO=0, IRQ_FLAG};
/* READ_IO
 * x x x x  x x x x
 * | | | |  | | | |-> photocell
 * | | | |  | | |---> IRQ flag
 * | | | |  | |-----> reserved
 * | | | |  |-------> reserved
 * | | | |----------> reserved
 * | | |------------> reserved
 * | |--------------> reserved
 * |----------------> reserved
 */

enum {VFLIP=15};
/* SHIFT_CNTL1
 * x x x x  x x x x  x x x x  x x x x
 * | |------------------------------|-> START_DOT
 * |----------------------------------> VFLIP
 */

enum {DRAFT=14, BMP};
/* SHIFT_CNTL2
 * x x x x  x x x x  x x x x  x x x x
 * | | |----------------------------|-> HEIGHT
 * | |--------------------------------> DRAFT
 * |----------------------------------> RLSHIFT
 */

#define BUS_IO_BASE (0xC20000)
#define inportb(port) (*(volatile char *)(port+BUS_IO_BASE))
#define outportb(port, data) (*(volatile char *)(port+BUS_IO_BASE)=(data))
#define inport(port) (*(volatile short *)(port+BUS_IO_BASE))
#define outport(port, data) (*(volatile short *)(port+BUS_IO_BASE)=(data))

int PrintheadInterfaceInit(unsigned short BaseAddr);
