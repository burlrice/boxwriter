#ifndef __IVREGS_H__
#define __IVREGS_H__

#include "DbTypeDefs.h"

using FoxjetDatabase::IMAGINGMODE;
using FoxjetDatabase::IMAGE_ON_PHOTOCELL;
using FoxjetDatabase::IMAGE_ON_EOP;


#define TICKS_IV (19) // 0.190"

#define IV_MAX_HEADS (8)

// masks of register INT_REG_DOWNLOAD
#define IV_MASK1DONE				(0x04)
#define IV_CLK1MASK					(0x02)
#define IV_CLK0MASK					(0XFD)
#define IV_DATA_MASK				(0x0F)
#define IV_TYPEMASK					(0x03)

// I/O register addresses
#define IV_REG_BASE					(0x00) // 8 bit
#define IV_REG_GAL					(0x01) // 8 bit
#define IV_REG_CMD					(0x02) // 8 bit	// write only
#define IV_REG_READ_IO				(0x02) // 8 bit	// read only

#define IV_REG_DATA_PORT			(0x04)
#define IV_REG_DIV_ENCODER_BY		(0x08)
#define IV_REG_PRINT_STATUS			(0x0A)
#define IV_REG_DPI_IRQ				(0x0C)
#define IV_REG_SHIFT_CNTL			(0x0E)
#define IV_REG_LINE_SPEED			(0x14)
#define IV_REG_HEAD_TYPES			(0x1A)
/* Page 1 Registers*/				
#define IV_REG_PH_OFFSETS			(0x04)

namespace ISA
{

	enum {RESET=0, ENCODER_SEL, UNPAUSE, IRQ_SEL, PHOTO_SHARING, ENCODER_SHARING, INC_COL_CNT=7};
	enum {EXTERNAL=0, INTERNAL};
	enum {COLUMN_IRQ=0, PHOTO_IRQ};
	enum {SHARING_OFF=0, SHARING_ON};

	/* PRINT_STATUS
	 * x x x x  x x x x
	 * | | | |  | | | |-> RESET, (active high)
	 * | | | |  | | |---> ENCODER_SEL, EXTERNAL(0)/INTERNAL(1)
	 * | | | |  | |-----> UNPAUSE, (active high)
	 * | | | |  |-------> IRQ_SEL, COLUMN_IRQ(0)/PHOTO_IRQ(1)
	 * | | | |----------> PHOTO_SHARING, (active high)
	 * | | |------------> ENCODER_SHARING, (active high)
	 * | |--------------> RESERVED
	 * |----------------> INC_COL_CNT, (active high)
	 */

	enum {INTERRUPT = 0, IRQ_FLAG, UART, PHOTOSTATE};
	/* READ_IO
	 * x x x x  x x x x
	 * | | | |  | | | |-> photocell
	 * | | | |  | | |---> IRQ flag
	 * | | | |  | |-----> reserved
	 * | | | |  |-------> reserved
	 * | | | |----------> reserved
	 * | | |------------> reserved
	 * | |--------------> reserved
	 * |----------------> reserved
	 */

	enum {VFLIP=15};
	/* SHIFT_CNTL1
	 * x x x x  x x x x  x x x x  x x x x
	 * | |------------------------------|-> START_DOT
	 * |----------------------------------> VFLIP
	 */

	enum {DRAFT=14, IV_BMP};
	/* SHIFT_CNTL2
	 * x x x x  x x x x  x x x x  x x x x
	 * | | |----------------------------|-> HEIGHT
	 * | |--------------------------------> DRAFT
	 * |----------------------------------> RLSHIFT
	 */

	//////////////////////////////////////////////////////////////////////

	#pragma pack(1)
	typedef struct
	{
		UCHAR				m_nType;
		USHORT				m_lPhotocellDelay;				// units: 1/100" ex: 2.5" --> 250
		UCHAR				m_bReversed;
	} IVHEADCONFIGSTRUCT;

	typedef struct
	{
		ULONG				m_lAddress;						
		IVHEADCONFIGSTRUCT	m_heads [IV_MAX_HEADS];
		USHORT				m_lLineSpeed;					// units: ft/min
		UCHAR				m_bExternalEncoder;
		UCHAR				m_bSharedPhotocell;
		UCHAR				m_bSharedEncoder;
		UCHAR				m_bEnabled;
		UCHAR				m_nEncoderDivisor;
		UCHAR				m_bInternalPhotocell;
		USHORT				m_lAutoPrintCols;
		UCHAR				m_bDPC;
		IMAGINGMODE			m_nImgMode;
	} IVCARDCONFIGSTRUCT;

	typedef struct
	{
		ULONG m_lAddress;						
		UCHAR m_nRegID;
		ULONG m_lData;
		ULONG m_lSize;
	} IVREGISTERSTRUCT;

	typedef struct
	{
		ULONG m_lAddress;						
		UCHAR m_nColor;
	} STROBESTRUCT;

	typedef struct
	{
		ULONG m_lAddress;						
		UCHAR m_bEnabled;
	} ENABLESTRUCT;

	typedef struct
	{
		ULONG				m_lAddress;						// to be initialized by caller
		USHORT				m_lLineSpeed;					// units: ft/min
		UCHAR				m_bProdDetect;
		UCHAR				m_bPELTemp;
		UCHAR				m_bPELWaste;
		UCHAR				m_bPowerError;
		UCHAR				m_bPELVacuumOn;
	} IVCARDSTATESTRUCT;
	#pragma pack()
};

//////////////////////////////////////////////////////////////////////


#endif //__IVREGS_H__