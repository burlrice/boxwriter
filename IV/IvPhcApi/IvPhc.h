// IvPhc.h: interface for the CIvPhc class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IVPHC_H__58A1AC7F_F6B3_4916_9EF7_445E30504633__INCLUDED_)
#define AFX_IVPHC_H__58A1AC7F_F6B3_4916_9EF7_445E30504633__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Mphc.h"
#include "IvRegs.h"

namespace ISA
{
	class AFX_EXT_CLASS CIvPhc : public ISA::CDriverInterface
	{
	public:
		CIvPhc(bool bDemo);
		virtual ~CIvPhc();

	public:
		virtual UINT GetAddressCount();
		virtual bool GetAddressList (ULONG * pList, UINT & nItems);

		virtual bool EnablePrint(HANDLE DeviceID, bool bEnabled);
		
		bool GetState (HANDLE DeviceID, IVCARDSTATESTRUCT & config);
		bool SetConfig (HANDLE DeviceID, IVCARDCONFIGSTRUCT & config);

		bool ReadRegister (HANDLE DeviceID, IVREGISTERSTRUCT & reg);
		bool WriteRegister (HANDLE DeviceID, IVREGISTERSTRUCT & reg);

		virtual bool RegisterEOPEvent(HANDLE DeviceID, HANDLE hEvent);
		virtual bool UnregisterEOPEvent(HANDLE DeviceID);

		virtual bool RegisterPhotoEvent(HANDLE DeviceID, HANDLE hEvent);
		virtual bool UnregisterPhotoEvent(HANDLE DeviceID);

		virtual bool RegisterSerialEvent(HANDLE DeviceID, HANDLE hEvent);
		virtual bool UnregisterSerialEvent(HANDLE DeviceID);

		virtual bool SetImageLen(HANDLE DeviceID, WORD wImageCols);
		virtual bool SetPrintDelay(HANDLE DeviceID, WORD wDelay);
		virtual bool SetProductLen(HANDLE DeviceID, WORD wProdLen);

		virtual bool SetStrobe(HANDLE DeviceID, int Color);
		
		int UpdateImageData(HANDLE DeviceID, void* pImageBuffer, ULONG lBufferLen, UCHAR nBPC, WORD StartCol, WORD wNumCols);

		bool EnableDebug (HANDLE DeviceID, bool bEnabled);
		bool Abort (HANDLE DeviceID);

	//	bool GetIndicators(HANDLE DeviceID, tagIndicators& Indicators);
	//	bool SetSerialParams(HANDLE DeviceID, tagSerialParams Params);

	protected:
		typedef struct tagDEMOSTRUCT
		{
			tagDEMOSTRUCT () : m_hPhoto (NULL), m_lCalls (0) { }
			
			HANDLE m_hPhoto;
			HANDLE m_hEOP;
			ULONG m_lCalls;
		} DEMOSTRUCT;

		void DemoMemicPhotocell (ULONG lAddr);

		CMap <ULONG, ULONG, DEMOSTRUCT, DEMOSTRUCT> m_mapDemo;
	};
};

#endif // !defined(AFX_IVPHC_H__58A1AC7F_F6B3_4916_9EF7_445E30504633__INCLUDED_)
