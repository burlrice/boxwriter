// IvPhc.cpp: implementation of the CIvPhc class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <winioctl.h>
#include "IvPhc.h"
#include "DevGuids.h"
#include "..\..\FxMphc\src\Ioctls.h"
#include "..\..\FxMphc\src\TypeDefs.h"
#include "Debug.h"
#include "DllVer.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CIvPhc::CIvPhc(bool bDemo)
:	CDriverInterface (bDemo, GUID_INTERFACE_IVPHC)
{
}

CIvPhc::~CIvPhc()
{

}

bool CIvPhc::RegisterEOPEvent(HANDLE hDevice, HANDLE hEvent)
{
	CSingleLock (&m_cs).Lock ();
	ULONG lAddr = 0;

	if (m_mapDevID.Lookup ((ULONG)hDevice, (ULONG &)lAddr)) {
		if (m_mapAddress.Lookup (lAddr, (ULONG &)hDevice)) {
			if (m_bDemo) {
				DEMOSTRUCT photo;

				memset (&photo, 0, sizeof (photo));
				photo.m_hEOP = hEvent;

				if (m_mapDemo.Lookup (lAddr, photo)) 
					photo.m_hEOP = hEvent;

				m_mapDemo.SetAt (lAddr, photo);
			}
			else {
				ULONG lData [] = { lAddr, (ULONG)hEvent };
				ULONG nBytes = 0;

				if (DeviceIo (hDevice, IOCTL_REGISTER_EOP_EVENT, &lData, sizeof (lData), NULL, 0, &nBytes, NULL) == 0)
					return true;
			}
		}
	}

	return false;
}

bool CIvPhc::UnregisterEOPEvent(HANDLE hDevice)
{
	CSingleLock (&m_cs).Lock ();
	bool value = false;
	ULONG lAddr = 0;

	if (m_mapDevID.Lookup ((ULONG)hDevice, (ULONG &)lAddr)) {
		ULONG nBytes = 0;
		HANDLE hDevice = NULL;

		if (m_mapAddress.Lookup (lAddr, (ULONG &)hDevice)) {
			if (DeviceIo (hDevice, IOCTL_UNREGISTER_PHOTO_EVENT, &lAddr, sizeof (lAddr), NULL, 0, &nBytes, NULL) == 0)
				value = true;

			if (m_bDemo)
				m_mapDemo.RemoveKey (lAddr);
		}
	}

	return value;
}

bool CIvPhc::RegisterPhotoEvent (HANDLE hDevice, HANDLE hEvent)
{
	CSingleLock (&m_cs).Lock ();
	ULONG lAddr = 0;

	if (m_mapDevID.Lookup ((ULONG)hDevice, (ULONG &)lAddr)) {
		if (m_mapAddress.Lookup (lAddr, (ULONG &)hDevice)) {
			if (m_bDemo) {
				DEMOSTRUCT photo;

				memset (&photo, 0, sizeof (photo));
				photo.m_hPhoto = hEvent;

				if (m_mapDemo.Lookup (lAddr, photo)) 
					photo.m_hPhoto = hEvent;

				m_mapDemo.SetAt (lAddr, photo);
			}
			else {
				ULONG lData [] = { lAddr, (ULONG)hEvent };
				ULONG nBytes = 0;

				if (DeviceIo (hDevice, IOCTL_REGISTER_PHOTO_EVENT, &lData, sizeof (lData), NULL, 0, &nBytes, NULL) == 0)
					return true;
			}
		}
	}

	return false;
}

UINT CIvPhc::GetAddressCount()
{
   return m_mapAddress.GetCount();
}


bool CIvPhc::GetAddressList (ULONG * pList, UINT & nItems)
{
	CSingleLock (&m_cs).Lock ();
	bool bResult = false;
	UINT nCount = 0;
	ULONG * p = pList;

	POSITION pos = m_mapAddress.GetStartPosition();

	while (pos) {
		ULONG lAddr;
		ULONG hDevice;

		bResult = true;
		m_mapAddress.GetNextAssoc (pos, lAddr, hDevice);
		
		if (nCount++ < nItems)
			* p++ = lAddr;
		else
			break;
	}

	nItems = nCount;
	
	return bResult;
}

bool CIvPhc::EnablePrint(HANDLE DeviceID, bool bEnabled)
{
	CSingleLock (&m_cs).Lock();

	HANDLE hDevice = NULL; 
	ULONG lAddr = 0L;
	USHORT nSpeed = 0;
	ULONG nBytes = 0;

	if (m_mapDevID.Lookup ((ULONG)DeviceID, (ULONG &)lAddr)) {
		if (m_mapAddress.Lookup (lAddr, (ULONG &)hDevice)) {
			ULONG lBytes = 0;
			ENABLESTRUCT s;

			s.m_lAddress = lAddr;
			s.m_bEnabled = bEnabled;
		
			#ifdef _DEBUG
			CString str;

			str.Format (_T ("EnablePrint [0x%X]: %s"), lAddr, bEnabled ? _T ("true") : _T ("false"));
			//TRACEF (str);
			#endif

			if (DeviceIo (hDevice, IOCTL_ENABLE_PRINT, &s, sizeof (s), NULL, 0, &lBytes, NULL) == 0) {
				return true;
			}
		}
	}

	return false; 
}

bool CIvPhc::GetState (HANDLE DeviceID, IVCARDSTATESTRUCT & state)
{
	CSingleLock (&m_cs).Lock();

	bool bResult = false;
	HANDLE hDevice = NULL; 
	ULONG lAddr = 0L;
	USHORT nSpeed = 0;
	ULONG nBytes = 0;

	if (m_mapDevID.Lookup ((ULONG)DeviceID, (ULONG &)lAddr)) {
		if (m_mapAddress.Lookup (lAddr, (ULONG &)hDevice)) {
			ULONG lSpeed = 0;
			ULONG lBytes = 0;
			IVCARDSTATESTRUCT in;

			ZeroMemory (&in, sizeof (in));
			in.m_lAddress = state.m_lAddress;
			
			if (DeviceIo (hDevice, IOCTL_GET_STATE, &in, sizeof (in), &state, sizeof (state), &lBytes, NULL) == 0) {
				bResult = true;
			}
		}

		DemoMemicPhotocell (lAddr);
	}

	return bResult; 
}

bool CIvPhc::SetConfig (HANDLE DeviceID, IVCARDCONFIGSTRUCT & config)
{
	CSingleLock (&m_cs).Lock();

	HANDLE hDevice = NULL; 
	ULONG lAddr = 0L;
	USHORT nSpeed = 0;
	ULONG nBytes = 0;

	if (m_mapDevID.Lookup ((ULONG)DeviceID, (ULONG &)lAddr)) {
		if (m_mapAddress.Lookup (lAddr, (ULONG &)hDevice)) {
			ULONG lBytes = 0;
			
			if (DeviceIo (hDevice, IOCTL_SET_CONFIG, &config, sizeof (config), NULL, 0, &lBytes, NULL) == 0) {
				return true;
			}
		}
	}

	return false; 
}

bool CIvPhc::SetStrobe(HANDLE DeviceID, int nColor)
{
	CSingleLock (&m_cs).Lock();

	HANDLE hDevice = NULL; 
	ULONG lAddr = 0L;
	ULONG nBytes = 0;

	/*
	#ifdef _DEBUG
	{
		CString str;

		str.Format ("SetStrobe: 0x%X, ", DeviceID);
		switch (nColor) {
		case STROBE_LIGHT_OFF:	str += "STROBE_LIGHT_OFF";	break;
		case RED_STROBE:		str += "RED_STROBE";		break;
		case GREEN_STROBE:		str += "GREEN_STROBE";		break;
		}

		TRACEF (str);
	}
	#endif
	*/

	if (m_mapDevID.Lookup ((ULONG)DeviceID, (ULONG &)lAddr)) {
		if (m_mapAddress.Lookup (lAddr, (ULONG &)hDevice)) {
			ULONG lBytes = 0;
			STROBESTRUCT s;

			s.m_lAddress = lAddr;
			s.m_nColor = nColor;
			
			if (DeviceIo (hDevice, IOCTL_SET_STROBE, &s, sizeof (s), NULL, 0, &lBytes, NULL) == 0) {
				return true;
			}
		}
	}

	return false; 
}

bool CIvPhc::UnregisterPhotoEvent(HANDLE DeviceID)
{
	CSingleLock (&m_cs).Lock ();
	bool value = false;
	ULONG lAddr = 0;

	if (m_mapDevID.Lookup ((ULONG)DeviceID, (ULONG &)lAddr)) {
		ULONG nBytes = 0;
		HANDLE hDevice = NULL;

		if (m_mapAddress.Lookup (lAddr, (ULONG &)hDevice)) {
			if (DeviceIo (hDevice, IOCTL_UNREGISTER_PHOTO_EVENT, &lAddr, sizeof (lAddr), NULL, 0, &nBytes, NULL) == 0)
				value = true;

			if (m_bDemo)
				m_mapDemo.RemoveKey (lAddr);
		}
	}

	return value;
}

bool CIvPhc::WriteRegister (HANDLE DeviceID, IVREGISTERSTRUCT & reg)
{
	CSingleLock (&m_cs).Lock();

	HANDLE hDevice = NULL; 
	ULONG lAddr = 0L;
	USHORT nSpeed = 0;
	ULONG nBytes = 0;

	if (m_mapDevID.Lookup ((ULONG)DeviceID, (ULONG &)lAddr)) {
		if (m_mapAddress.Lookup (lAddr, (ULONG &)hDevice)) {
			ULONG lBytes = 0;
			
			if (DeviceIo (hDevice, IOCTL_WRITE_REGISTER, &reg, sizeof (reg), NULL, 0, &lBytes, NULL) == 0) {
				return true;
			}
		}
	}

	return false; 
}

bool CIvPhc::ReadRegister (HANDLE DeviceID, IVREGISTERSTRUCT & reg)
{
	CSingleLock (&m_cs).Lock();

	bool bResult = false;
	HANDLE hDevice = NULL; 
	ULONG lAddr = 0L;
	ULONG nBytes = 0;

	if (m_mapDevID.Lookup ((ULONG)DeviceID, (ULONG &)lAddr)) {
		if (m_mapAddress.Lookup (lAddr, (ULONG &)hDevice)) {
			ULONG lBytes = 0;
			IVREGISTERSTRUCT in;

			ZeroMemory (&in, sizeof (in));
			in.m_lAddress	= reg.m_lAddress;
			in.m_lSize		= reg.m_lSize;
			in.m_nRegID		= reg.m_nRegID;
			
			if (DeviceIo (hDevice, IOCTL_READ_REGISTER, &in, sizeof (in), &reg, sizeof (reg), &lBytes, NULL) == 0) {
				return true;
			}
		}
	}

	return false; 
}

int CIvPhc::UpdateImageData (HANDLE DeviceID, void* pImageBuffer, ULONG lBufferLen, 
							 UCHAR nBPC, WORD wStartCol, WORD wNumCols)
{
	CSingleLock (&m_cs).Lock ();

	bool value = false;
	HANDLE hDevice; 
	unsigned long Addr;
	ULONG nBytes;

	ASSERT (wStartCol == 0);
	ASSERT (nBPC == 9);

	if ( m_mapDevID.Lookup((ULONG)DeviceID, (ULONG &)Addr)) {
		if (m_mapAddress.Lookup (Addr, (ULONG &)hDevice)) {
			PHCB in;
			tagImgUpdateHdr img;
			ULONG lBlockSize = sizeof (PHCB) + lBufferLen;

			ZeroMemory (&in, sizeof (in));
			ZeroMemory (&img, sizeof (img));

			in.lAddress		= Addr;
	
			img._wStartCol	= wStartCol;
			img._wNumCols	= wNumCols;
			img._nBPC		= nBPC;

			memcpy (&in.Data, &img, sizeof (img));
			
			USHORT wWidthBits = img._nBPC * 8;
			USHORT wRowWords = (wWidthBits / 16) + ((wWidthBits % 16) ? 1 : 0); // win bitmap is word aligned

			PUCHAR pData = NULL;

			try 
			{
				pData = new UCHAR [lBlockSize];
				//ZeroMemory (pData, lBlockSize);

				PUCHAR p = pData;
				memcpy (p, &in, sizeof (in));
				
				p += sizeof (PHCB);
				memcpy (p, pImageBuffer, lBufferLen);
			}
			catch (CMemoryException *e) {e->ReportError(); e->Delete(); return value;}

			if (DeviceIo (hDevice, IOCTL_IMAGE_UPDATE, pData, lBlockSize, NULL, 0, &nBytes, NULL) == 0) {
				value = true;
			}
			
			if (pData)
				delete pData;
		}
	}
	
	return value;
}

void CIvPhc::DemoMemicPhotocell (ULONG lAddr)
{
	DEMOSTRUCT photo;
	int nFreq = 2;

	#ifdef _DEBUG
	nFreq = 5;
	#endif
	
	if (m_mapDemo.Lookup (lAddr, photo)) {
		if ((photo.m_lCalls % (nFreq * 2)) == 0) {
			if (photo.m_hPhoto)
				::SetEvent (photo.m_hPhoto);
		}
		else if ((photo.m_lCalls % nFreq) == 0) {
			if (photo.m_hEOP)
				::SetEvent (photo.m_hEOP);
		}
		
		photo.m_lCalls++;
		m_mapDemo.SetAt (lAddr, photo);
	}
}

bool CIvPhc::EnableDebug (HANDLE DeviceID, bool bEnabled)
{
	CSingleLock (&m_cs).Lock();

	HANDLE hDevice = NULL; 
	ULONG lAddr = 0L;
	USHORT nSpeed = 0;
	ULONG nBytes = 0;

	if (m_mapDevID.Lookup ((ULONG)DeviceID, (ULONG &)lAddr)) {
		if (m_mapAddress.Lookup (lAddr, (ULONG &)hDevice)) {
			ULONG lBytes = 0;
			ENABLESTRUCT s;

			s.m_lAddress = lAddr;
			s.m_bEnabled = bEnabled;
		
			if (DeviceIo (hDevice, IOCTL_ENABLE_DEBUG, &s, sizeof (s), NULL, 0, &lBytes, NULL) == 0) {
				return true;
			}
		}
	}

	return false; 
}

bool CIvPhc::RegisterSerialEvent(HANDLE DeviceID, HANDLE hEvent)
{
   bool value = false;
   ULONG Data[2];
   ULONG nBytes = 0;
   ULONG Addr;
   HANDLE hDevice;

   // Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			if ( !m_bDemo ) 
			{
				Data[0] = Addr;
				Data[1] = (ULONG) hEvent;
				if ( DeviceIo (hDevice, IOCTL_REGISTER_SERIAL_EVENT, &Data, sizeof (Data), NULL, 0, &nBytes, NULL) == 0)
					value = true;
			}
		}
   }
   return value;
}

bool CIvPhc::UnregisterSerialEvent(HANDLE DeviceID)
{
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool value = false;
   ULONG nBytes = 0;
   ULONG Addr;
   HANDLE hDevice;

   // Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			if ( DeviceIo (hDevice, IOCTL_UNREGISTER_SERIAL_EVENT, &Addr, sizeof (Addr), NULL, 0, &nBytes, NULL) == 0)
				value = true;
		}
   }
	sLock.Unlock();
   return value;
}

bool CIvPhc::SetImageLen(HANDLE DeviceID, WORD wImageCols)
{
	return false; // TODO
}

bool CIvPhc::SetPrintDelay(HANDLE DeviceID, WORD wDelay)
{
	return false; // TODO
}

bool CIvPhc::SetProductLen(HANDLE DeviceID, WORD wProdLen)
{
	return false; // TODO
}

bool CIvPhc::Abort (HANDLE DeviceID)
{
	CSingleLock (&m_cs).Lock();

	HANDLE hDevice = NULL; 
	ULONG lAddr = 0L;
	USHORT nSpeed = 0;
	ULONG nBytes = 0;

	if (m_mapDevID.Lookup ((ULONG)DeviceID, (ULONG &)lAddr)) {
		if (m_mapAddress.Lookup (lAddr, (ULONG &)hDevice)) {
			ULONG lBytes = 0;
			ENABLESTRUCT s;

			s.m_lAddress = lAddr;
			s.m_bEnabled = true;
		
			if (DeviceIo (hDevice, IOCTL_ABORT, &s, sizeof (s), NULL, 0, &lBytes, NULL) == 0) {
				return true;
			}
		}
	}

	return false; 
}

