/******************************************************************************\
*
* File:          DeviceListEntry.h
* Creation date: November 14, 2003 15:19
*                Foxjet Tech Center
*
* Case Tool:     ClassBuilder 2.3
* Purpose:       Declaration of class 'CDeviceListEntry'
*
* Modifications: @INSERT_MODIFICATIONS(* )
*
* Copyright 2003, Foxjet Tech Center
* All rights are reserved. Reproduction in whole or part is prohibited
* without the written consent of the copyright owner.
*
\******************************************************************************/
#ifndef _ISA_DEVICELISTENTRY_H
#define _ISA_DEVICELISTENTRY_H

//@START_USER1
#include <afxtempl.h>
//@END_USER1


//namespace ISA  {

	class AFX_EXT_CLASS CDeviceListEntry
	{

	//@START_USER2
	//@END_USER2

	//
	// Group: ClassBuilder methods
	//

	private:
	   void ConstructorInclude();
	   void DestructorInclude();

	public:
	   CDeviceListEntry(CString linkname, CString friendlyname);
	   CDeviceListEntry();

	//
	// Non-Grouped Members
	//

	public:
	   CString m_linkname;
	   CString m_friendlyname;

	//
	// Non-Grouped Methods
	//

	public:
	   virtual ~CDeviceListEntry();
	};
//};
#endif


#ifdef CB_INLINES
#ifndef _DEVICELISTENTRY_H_INLINES
#define _DEVICELISTENTRY_H_INLINES

//@START_USER3
//@END_USER3

#endif
#endif
