//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by MphcApi.rc
//
#define IDD_CONFIG                      130
#define IDC_HT_DISABLED                 1004
#define IDC_HT_SLANTED                  1005
#define IDC_HT_STAGGERED                1006
#define IDC_HT_GRAPHIC                  1007
#define IDC_VIB_OFF                     1008
#define IDC_VIB_AUTO                    1009
#define IDC_VIB_SPIT                    1010
#define IDC_SOFT_RESET                  1011
#define IDC_DIR_LTOR                    1012
#define IDC_DIR_RTOL                    1013
#define IDC_ORIENT_NORM                 1014
#define IDC_ORIENT_INVERTED             1015
#define IDC_TACH_EXTERN                 1016
#define IDC_TACH_INTERN                 1017
#define IDC_TACH_OTB1                   1018
#define IDC_TACH_OTB2                   1019
#define IDC_PHOTO_EXTERN                1020
#define IDC_PHOTO_INTERN                1021
#define IDC_PHOTO_OTB1                  1022
#define IDC_PHOTO_OTB2                  1023
#define IDC_DRV_TACH1                   1024
#define IDC_DRV_TACH2                   1025
#define IDC_DRV_PHOTO1                  1026
#define IDC_DRV_PHOTO2                  1027
#define IDC_TACHDIV_1                   1031
#define IDC_TACHDIV_2                   1032
#define IDC_TACHDIV_4                   1033
#define IDC_TACHDIV_8                   1034
#define IDC_SCLK_0                      1035
#define IDC_SCLK_1                      1036
#define IDC_SCLK_2                      1037
#define IDC_SCLK_3                      1038
#define IDC_WIN_SIZE_0                  1039
#define IDC_WIN_SIZE_1                  1040
#define IDC_WIN_SIZE_2                  1041
#define IDC_WIN_SIZE_3                  1042
#define IDC_TACH_RATE                   1043
#define IDC_INTERRUPT_RATE              1044

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        26000
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         26000
#define _APS_NEXT_SYMED_VALUE           26000
#endif
#endif
