/******************************************************************************\
*
* File:          Mphc.h
* Creation date: November 14, 2003 15:19
*                Foxjet Tech Center
*
* Case Tool:     ClassBuilder 2.3
* Purpose:       Declaration of class 'CMphc'
*
* Modifications: @INSERT_MODIFICATIONS(* )
* May 25, 2005 16:14 cgh AMS Fix
*     Updated interface of method 'SetAmsState'
* May 25, 2005 10:41 cgh Changed for AMS Fix
*     Added method 'UnregisterAmsEvent'
* May 25, 2005 10:38 cgh Changed for AMS Fix
*     Added method 'RegisterAmsEvent'
* May 13, 2004 18:52 cgh - Added function to set and unset print head reset
*     Updated interface of method 'SetResetBit'
* May 13, 2004 18:48 cgh - Added function to set and unset print head reset
*     Added method 'SetResetBit'
* April 09, 2004 15:36 cgh - Modification for End Of Print Ack.
*     Added method 'UnregisterEOPEvent'
*     Added method 'RegisterEOPEvent'
* November 21, 2003 12:11 CGH
*     Added method 'ClearDebugFlags'
*
* Copyright 2003, Foxjet Tech Center
* All rights are reserved. Reproduction in whole or part is prohibited
* without the written consent of the copyright owner.
*
\******************************************************************************/
#ifndef _ISA_MPHC_H
#define _ISA_MPHC_H

//@START_USER1
#include "DeviceList.h"
#include "Master.h"
#include <Afxmt.h>
#include "..\FxMphc\Src\TypeDefs.h"
//@END_USER1

namespace ISA 
{
	class AFX_EXT_CLASS CDriverInterface : protected CDeviceList
	{
	public:
		CDriverInterface (bool bDemo, const GUID & guid);
		virtual ~CDriverInterface ();

		
	public:
		virtual UINT GetAddressCount() = 0;
		virtual bool GetAddressList (ULONG * pList, UINT & nItems) = 0;

		virtual HANDLE ClaimAddress(ULONG Addr);
		virtual bool ReleaseAddress(HANDLE DeviceID);

		virtual int ProgramFPGA(HANDLE DeviceID, const CString & strFile);

		virtual bool ClearPrintBuffer(HANDLE DeviceID);
		virtual bool EnablePrint(HANDLE DeviceID, bool bEnabled) = 0;
		
		virtual bool ReadSerialData(HANDLE DeviceID, UCHAR * pData, UINT & nBytes);
		
		virtual bool RegisterEOPEvent(HANDLE DeviceID, HANDLE hEvent) = 0;
		virtual bool UnregisterEOPEvent(HANDLE DeviceID) = 0;

		virtual bool RegisterPhotoEvent(HANDLE DeviceID, HANDLE hEvent) = 0;
		virtual bool UnregisterPhotoEvent(HANDLE DeviceID) = 0;

		virtual bool RegisterSerialEvent(HANDLE DeviceID, HANDLE hEvent) = 0;
		virtual bool UnregisterSerialEvent(HANDLE DeviceID) = 0;

		virtual bool SetImageLen(HANDLE DeviceID, WORD wImageCols) = 0;
		virtual bool SetPrintDelay(HANDLE DeviceID, WORD wDelay) = 0;
		virtual bool SetProductLen(HANDLE DeviceID, WORD wProdLen) = 0;

		virtual bool SetStrobe(HANDLE DeviceID, int Color) = 0;
		
		virtual bool SetSerialParams(HANDLE DeviceID, tagSerialParams Params);

	protected:
		virtual void DemoMemicLineSpeed(PHCB* pPHCB);
		virtual void DemoMemicIndicators(PHCB* pPHCB);
		virtual void DemoMemicPhotocell(HANDLE DeviceID);

		DWORD DeviceIo (HANDLE hDevice, DWORD dwIoControlCode, void* pInBuffer,
			DWORD dwInBufferSize, void* pOutBuffer,
			DWORD dwOutBufferSize, DWORD* pBytesRtn,
			_OVERLAPPED* pOverLapped);
		void DeviceError();

		CMap <DWORD, DWORD, DWORD, DWORD>	m_mapDevID;			// m_ClaimedAddr
		CMap <DWORD, DWORD, DWORD, DWORD>	m_mapAddress;		// m_AddressMap
		CArray <HANDLE, HANDLE>				m_vDevices;
		ULONG								m_lNextID;
		bool								m_bDemo;
		CCriticalSection					m_cs;
	};

	class AFX_EXT_CLASS CMphc : public ISA::CDriverInterface
	{

	//@START_USER2
		CArray <ULONG, ULONG> DemoAddress;

	//@END_USER2

	//
	// Group: ClassBuilder methods
	//

	private:
	   void ConstructorInclude();
	   void DestructorInclude();

	//
	// Group: Demo Members and Methods
	//

	private:
	   CPtrList m_DemoPointers;
	   CMap<HANDLE, HANDLE, tagPhotoDemo, tagPhotoDemo > m_DemoPhotoEvent;
   
	   void DemoMemicLineSpeed(PHCB* pPHCB);
	   void DemoMemicIndicators(PHCB* pPHCB);
	   void DemoMemicPhotocell(HANDLE DeviceID);

	public:
	   CMphc(bool Demo);
	   virtual ~CMphc();

   
	   virtual bool EnablePrint(HANDLE DeviceID, bool bEnabled);
	   virtual UINT GetAddressCount();
	   virtual bool GetAddressList (ULONG * pList, UINT & Items);
	   virtual bool RegisterEOPEvent(HANDLE DeviceID, HANDLE hEvent);
	   virtual bool RegisterPhotoEvent(HANDLE DeviceID, HANDLE hEvent);
	   virtual bool RegisterSerialEvent(HANDLE DeviceID, HANDLE hEvent);
	   virtual bool SetPrintDelay(HANDLE DeviceID, WORD wDelay);
	   virtual bool SetProductLen(HANDLE DeviceID, WORD wProdLen);
	   virtual bool SetStrobe(HANDLE DeviceID, int Color);
	   virtual bool UnregisterAmsEvent(HANDLE DeviceID);
	   virtual bool UnregisterEOPEvent(HANDLE DeviceID);
	   virtual bool UnregisterPhotoEvent(HANDLE DeviceID);
	   virtual bool UnregisterSerialEvent(HANDLE DeviceID);
	   virtual bool ReadLineSpeed(HANDLE DeviceID, double& dSpeed);
	   virtual bool SetImageLen(HANDLE DeviceID, WORD wImageCols);
	   virtual bool WriteWatchdog (HANDLE hDeviceID, tagPeak603VLWatchdog reg);
	   virtual bool WriteWatchdog (HANDLE hDeviceID, UCHAR c);
	   virtual bool ReadWatchdog (HANDLE hDeviceID, tagPeak603VLWatchdog & reg);
	   virtual bool ReadWatchdog (HANDLE hDeviceID, UCHAR & c);

	   int UpdateImageData(HANDLE DeviceID, void* pImageBuffer, unsigned char nBPC,
		   WORD StartCol, WORD wNumCols);

	   BOOL ClearDebugFlags(HANDLE DeviceID);
	   unsigned short ComputeSpeedGateWidth(unsigned short nEncoderRes);

	   BOOL GetDebugFlags(HANDLE DeviceID, _DBG_FLAGS& Flags);
	   unsigned char* GetImageBuffer(HANDLE DeviceID);
	   BOOL GetIndicators(HANDLE DeviceID, tagIndicators& Indicators, UCHAR & cRawStatus);
	   BOOL IsAmsActive(HANDLE DeviceID);
	   bool ProgramAmsRegs(HANDLE DeviceID, tagAMSRegs tAmsRegs);

	   bool ProgramSlantRegs(HANDLE DeviceID, tagSlantRegs SlantRegs);
	   bool ReadAmsRegs(HANDLE DeviceID, tagAMSRegs& tAmsRegs);
	   bool ReadConfiguration(HANDLE DeviceID, tagRegisters& tConfig);
	   bool ReadImage(HANDLE DeviceID, unsigned char* pBuffer, unsigned long lBytes);
	   bool RegisterAmsEvent(HANDLE DeviceID, HANDLE hEvent);
   
	   void ReleaseImageBuffer(unsigned char* pBuffer);
	   bool SetAmsState(HANDLE DeviceID, BOOL State);
	   bool SetMasterMode(HANDLE DeviceID);
      
	   BOOL SetResetBit(HANDLE DeviceID, bool mode);
	   bool SetSpeedGateWidth(HANDLE DeviceID, WORD wEncRes);
   
	   bool WriteSetup(HANDLE DeviceID, tagSetupRegs tSetup,
					   tagFireVibPulse tPulseRegs);
   
	   bool SetProductionMode (HANDLE DeviceID, bool bProduction);
	};
};

#endif


#ifdef CB_INLINES
#ifndef _MPHC_H_INLINES
#define _MPHC_H_INLINES

//@START_USER3
//@END_USER3

#endif
#endif
