/******************************************************************************\
*
* File:          Mphc.cpp
* Creation date: November 14, 2003 15:19
*                Foxjet Tech Center
*
* Case Tool:     ClassBuilder 2.4
*                
* Purpose:       Method implementations of class 'CMphc'
*
* Modifications: @INSERT_MODIFICATIONS(* )
* May 25, 2005 16:14 cgh AMS Fix
*     Updated interface of method 'SetAmsState'
* May 25, 2005 10:41 cgh Changed for AMS Fix
*     Added method 'UnregisterAmsEvent'
* May 25, 2005 10:38 cgh Changed for AMS Fix
*     Added method 'RegisterAmsEvent'
* May 13, 2004 18:52 cgh - Added function to set and unset print head reset
*     Updated interface of method 'SetResetBit'
* May 13, 2004 18:48 cgh - Added function to set and unset print head reset
*     Added method 'SetResetBit'
* April 09, 2004 15:36 cgh - Modification for End Of Print Ack.
*     Added method 'UnregisterEOPEvent'
*     Added method 'RegisterEOPEvent'
* November 21, 2003 12:11 CGH
*     Added method 'ClearDebugFlags'
* November 14, 2003 15:20 CGH - Updated note on Enable Print rev2
*     Updated code of method 'EnablePrint'
* November 14, 2003 15:19 CGH - Updated note on Enable Print
*     Updated code of method 'EnablePrint'
*
* Copyright 2003, Foxjet Tech Center
* All rights are reserved. Reproduction in whole or part is prohibited
* without the written consent of the copyright owner.
*
\******************************************************************************/
//@START_USER1
//@END_USER1


// Master include file
#include "StdAfx.h"


//@START_USER2
#include <WinIoctl.h>
#include <initguid.h>
#include "Mphc.h"
#include "Master.h"
#include "..\FxMphc\Src\IoCtls.h"
#include "..\FxMphc\Src\Defines.h"
#include "Debug.h"
#include "DevGuids.h"
#include "AppVer.h"

////////////////////////////////////////////////////////////////////////////////



//DEFINE_GUID(GUID_INTERFACE_FXMPHC, 0x18acebc3L, 0x76cc, 0x11d6, 0xa1, 0x22, 0x00, 0x60, 0x67, 0x4b, 0xa9, 0x76);

//
// Group: Demo Members and Methods
//
/*@NOTE_197
Memics the reading of the Line speed for demo mode only
*/
void CMphc::DemoMemicLineSpeed(PHCB* pPHCB)
{//@CODE_197
	static bool flag = false;

	if ( flag ) {
		pPHCB->Data[0] = 0x57;
		pPHCB->Data[1] = 0x02;
	}
	else {
		pPHCB->Data[0] = 0x58;
		pPHCB->Data[1] = 0x02;
	}

	flag = !flag;
}//@CODE_197


/*@NOTE_198
Memics reading the head indicators for demo mode only.
*/
void CMphc::DemoMemicIndicators(PHCB* pPHCB)
{//@CODE_198
//	static On = false;
//	static CTime Time = CTime::GetCurrentTime();
//	CTimeSpan Elasped = CTime::GetCurrentTime() - Time;
//	if ( Elasped.GetTotalSeconds() > 30 )
//	{
//		On = !On;
//		Time = CTime::GetCurrentTime();
//	}
//	if ( On ) 
//	{
		pPHCB->Data[0] = 0xD0;
		pPHCB->Data[1] = 0x00;
//	}
//	else
//	{
//		pPHCB->Data[0] = 0x00;
//		pPHCB->Data[1] = 0x00;
//	}
}//@CODE_198


/*@NOTE_199
Memic the firing of a photocell for demo mode only.
*/
void CMphc::DemoMemicPhotocell(HANDLE DeviceID)
{//@CODE_199
	tagPhotoDemo photo;
	
	if (m_DemoPhotoEvent.Lookup (DeviceID, photo)) {
		HANDLE hEvent = NULL;

		if (!photo.m_bEOP) {
			if (photo.m_hPhotoEvent)
				hEvent = photo.m_hPhotoEvent;
		}
		else {
			if (photo.m_hEOPEvent)
				hEvent = photo.m_hEOPEvent;
		}

		if (hEvent) {
//CString str;  str.Format ("SetEvent: 0x%p, [%d]", hEvent, DeviceID); TRACEF (str);
			SetEvent (hEvent);
		}

		photo.m_bEOP = !photo.m_bEOP;
		m_DemoPhotoEvent.SetAt (DeviceID, photo);
	}
}//@CODE_199


// End of Group: Demo Members and Methods



/*@NOTE_26
Constructor method.
*/
CMphc::CMphc(bool bDemo) //@INIT_26
 : CDriverInterface (bDemo, GUID_INTERFACE_FXMPHC)
{//@CODE_26
   ConstructorInclude();
}//@CODE_26


/*@NOTE_19
Destructor method.
*/
CMphc::~CMphc()
{//@CODE_19
   DestructorInclude();
	while ( !m_DemoPointers.IsEmpty() )
		delete ((PUCHAR) m_DemoPointers.RemoveHead());
}//@CODE_19



/*@NOTE_466
Resets the interrupt debug flags to zero
*/
BOOL CMphc::ClearDebugFlags(HANDLE DeviceID)
{//@CODE_466
 	CSingleLock sLock ( &m_cs );
	sLock.Lock();
   
	BOOL value = false;
   unsigned long Addr;
   HANDLE hDevice; 
   ULONG nBytes = 0;

   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
         if ( DeviceIo (hDevice, IOCTL_RESET_FLAGS, &Addr, sizeof(ULONG), NULL, 0, &nBytes, NULL) )
				value = true;
   }
   return value;
}//@CODE_466


/*@NOTE_151
Compute the sample gate width for reading back the conveyor speed.
*/
unsigned short CMphc::ComputeSpeedGateWidth(unsigned short nEncoderRes)
{//@CODE_151
   return ( USHORT )((double) (FTACH_100 * FGATE ) / (double) ( 20 * nEncoderRes));
}//@CODE_151


bool CMphc::EnablePrint(HANDLE DeviceID, bool bEnabled)
{//@CODE_356
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

	bool value = false;
	HANDLE hDevice; 
	unsigned long Addr;
	ULONG nBytes;
	PHCB inCtrlBlock;
	tagCtrlReg tCtrl;
	tagRegisters tCurrent;
	
	// Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			inCtrlBlock.lAddress = Addr;
			if ( ReadConfiguration ( DeviceID, tCurrent ) )
			{
				tCtrl = tCurrent._CtrlReg;
				tCtrl._PrintBuffer = 0;
				tCtrl._FireAMS = 0x00;
				
				if ( bEnabled == true )
					tCtrl._PrintEnable = 1;
				else 
					tCtrl._PrintEnable = 0;
				
				inCtrlBlock.RegID = 0x00;
				inCtrlBlock.TxBytes = sizeof ( tCtrl );
				memcpy ( &inCtrlBlock.Data, &tCtrl, sizeof ( tCtrl ) );
				if ( DeviceIo (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof ( PHCB ), NULL, 0, &nBytes, NULL) == 0) 
					value = true;
			}
		}
	}

//	CString str; str.Format ("EnablePrint (0x%X, %d): %d", DeviceID, bEnabled, value); TRACEF (str);
	return value;
}//@CODE_356


/*@NOTE_90
Return the number of address provide by the driver.
*/
unsigned int CMphc::GetAddressCount()
{//@CODE_90
   return m_mapAddress.GetCount();
}//@CODE_90


/*@NOTE_87
Get a list of addresses provided by the driver.
Note: Use GetAddressCount() to determine the number of address provided by the driver.
*/
bool CMphc::GetAddressList(unsigned long* pList, unsigned int& Items)
{//@CODE_87
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool result = false;
   POSITION pos = NULL;
   unsigned long Addr;
   unsigned long hDevice;
   unsigned cnt = 0;
   unsigned long *p = pList;
   pos = m_mapAddress.GetStartPosition();
   while ( pos )
   {
      result = true;
      m_mapAddress.GetNextAssoc ( pos, Addr, hDevice );
      if ( cnt++ < Items )
         *p++ = Addr;
      else
         break;
   }
   Items = cnt;
   return result;
}//@CODE_87


/*@NOTE_141
Use to read the Debug flags from the driver.
*/
BOOL CMphc::GetDebugFlags(HANDLE DeviceID, _DBG_FLAGS& Flags)
{//@CODE_141
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool value = false;
   ULONG Addr;
   HANDLE hDevice;
   ULONG nBytes = 0;
   PHCB inCtrlBlock;
	PHCB outCtrlBlock;
   
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
         memset ( &inCtrlBlock, 0x00, sizeof ( PHCB ) );
			inCtrlBlock.lAddress = Addr;
			memcpy ( &outCtrlBlock, &inCtrlBlock, sizeof (PHCB) );

			if ( DeviceIo(hDevice, IOCTL_GET_FLAGS, &inCtrlBlock, sizeof ( PHCB ), &outCtrlBlock, sizeof ( PHCB ), &nBytes, NULL) == 0) {
				memcpy ( &Flags, outCtrlBlock.Data, sizeof ( _DBG_FLAGS ) );
				value = true;
			}
		}
   }
	sLock.Unlock();
   return value;
}//@CODE_141


unsigned char* CMphc::GetImageBuffer(HANDLE DeviceID)
{//@CODE_135
   ULONG Addr;
   HANDLE hDevice;
   unsigned char* pBuffer = NULL;
   
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
//	NT does not like this method.
//			ULONG nBytes;
//			ULONG Address = 0L;
//			if ( DeviceIo (hDevice, IOCTL_GET_IMAGE_POINTER, &Addr, sizeof ( ULONG), &Address, sizeof ( ULONG ), &nBytes, NULL) == 0)
//			{
//				pBuffer = (unsigned char *) Address;
//				*pBuffer = 0x00;
//
//			}
			try 
			{
				pBuffer = new unsigned char [ IMAGE_BUFFER_SIZE ];
				memset ( pBuffer, 0x00, IMAGE_BUFFER_SIZE );
			}
			catch ( CMemoryException *e ) { e->ReportError(); throw ( e ); return NULL;}
		}
   }
   return pBuffer;
}//@CODE_135


/*@NOTE_168
Retrieves the head indicators such as Ink, Heater and HVOK status.
*/
BOOL CMphc::GetIndicators(HANDLE DeviceID, tagIndicators& Indicators, UCHAR & cRawStatus)
{//@CODE_168
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   BOOL value = false;
   HANDLE hDevice; 
   unsigned long Addr;
	unsigned short wSpeed = 0L;
   ULONG nBytes = 0;
   PHCB inCtrlBlock;
   PHCB outCtrlBlock;

   cRawStatus = 0xFF;

   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
         inCtrlBlock.lAddress = Addr;
         inCtrlBlock.RegID = 0x04;
         inCtrlBlock.TxBytes = sizeof ( unsigned short );
			memcpy ( &outCtrlBlock, &inCtrlBlock, sizeof (PHCB) );

			if ( DeviceIo (hDevice, IOCTL_READ_CTRL_REG, &inCtrlBlock, sizeof(PHCB), &outCtrlBlock, sizeof ( PHCB ), &nBytes, NULL) == 0)
			{
				// For now we are only using the first byte.
				// The Second byte is reserved and not used at this time.
				UCHAR uMask = 0x10;

				cRawStatus = outCtrlBlock.Data[0];
				Indicators.HVOK		= ( cRawStatus & uMask ) ? TRUE : FALSE; uMask <<= 1;
				Indicators.InkLow		= ( cRawStatus & uMask ) ? TRUE : FALSE; uMask <<= 1;
				Indicators.AtTemp		= ( cRawStatus & uMask ) ? TRUE : FALSE; uMask <<= 1;
				Indicators.HeaterOn	= ( cRawStatus & uMask ) ? TRUE : FALSE;
				value = true;
			}
      }
   }
   sLock.Unlock();
	return value;
}//@CODE_168


BOOL CMphc::IsAmsActive(HANDLE DeviceID)
{//@CODE_458
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   BOOL value = false;
   unsigned long Addr;
   HANDLE hDevice; 
   ULONG nBytes = 0;

   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
         DeviceIo (hDevice, IOCTL_READ_AMS_STATE, &Addr, sizeof(ULONG), &value, sizeof ( BOOL ), &nBytes, NULL);
      }
   }
   return value;
}//@CODE_458


bool CMphc::ProgramAmsRegs(HANDLE DeviceID, tagAMSRegs tAmsRegs)
{//@CODE_399
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool value = false;
   HANDLE hDevice; 
   unsigned long Addr;
   ULONG nBytes;
	PHCB inCtrlBlock;

   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
         inCtrlBlock.lAddress = Addr;
         inCtrlBlock.RegID = 0x30;
         inCtrlBlock.TxBytes = sizeof ( tagAMSRegs );
			memcpy ( &inCtrlBlock.Data, &tAmsRegs, sizeof ( tAmsRegs ) );
			if ( DeviceIo (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof ( PHCB ), NULL, 0, &nBytes, NULL) == 0)
				value = true;
      }
   }
   return value;
}//@CODE_399




/*@NOTE_351
Programs the Address Generator Registers ( Slant Registers ) with the direction and slant values.
*/
bool CMphc::ProgramSlantRegs(HANDLE DeviceID, tagSlantRegs SlantRegs)
{//@CODE_351
 	CSingleLock sLock ( &m_cs );
	sLock.Lock();

	bool value = false;
   HANDLE hDevice; 
   unsigned long Addr;
   ULONG nBytes;
	PHCB inCtrlBlock;

   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
         inCtrlBlock.lAddress = Addr;
         inCtrlBlock.RegID = 0x24;
         inCtrlBlock.TxBytes = sizeof ( tagSlantRegs );
			memcpy ( &inCtrlBlock.Data, &SlantRegs, sizeof ( tagSlantRegs ) );
			if ( DeviceIo (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof ( PHCB ), NULL, 0, &nBytes, NULL) == 0)
				value = true;
      }
   }
   return value;
}//@CODE_351


bool CMphc::ReadAmsRegs(HANDLE DeviceID, tagAMSRegs& tAmsRegs)
{//@CODE_402
   bool value = false;
   HANDLE hDevice; 
   unsigned long Addr;
   ULONG nBytes = 0;
   PHCB inCtrlBlock;
	PHCB outCtrlBlock;

   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
         memset ( &tAmsRegs, 0x00, sizeof (tagAMSRegs) );
         inCtrlBlock.lAddress = Addr;
         inCtrlBlock.RegID = 0x30;
         inCtrlBlock.TxBytes = sizeof ( tAmsRegs );
			memcpy ( &outCtrlBlock, &inCtrlBlock, sizeof (PHCB) );

         if ( DeviceIo (hDevice, IOCTL_READ_CTRL_REG, &inCtrlBlock, sizeof(PHCB), &outCtrlBlock, sizeof ( PHCB ), &nBytes, NULL) == 0)
			{
            value = true;
            memcpy ( &tAmsRegs, &outCtrlBlock.Data, outCtrlBlock.TxBytes);
         }
      }
   }
   return value;
}//@CODE_402


/*@NOTE_85
Reads the current configuration from the MPHC card
*/
bool CMphc::ReadConfiguration(HANDLE DeviceID, tagRegisters& tConfig)
{//@CODE_85
   bool value = false;
   HANDLE hDevice; 
   unsigned long Addr;
   ULONG nBytes = 0;
   PHCB inCtrlBlock;
   PHCB outCtrlBlock;
   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
         memset ( &tConfig, 0x00, sizeof (tagRegisters) );
         inCtrlBlock.lAddress = Addr;
         inCtrlBlock.RegID = 0x00;
         inCtrlBlock.TxBytes = sizeof ( tConfig );
			memcpy ( &outCtrlBlock, &inCtrlBlock, sizeof (PHCB) );
         if ( DeviceIo (hDevice, IOCTL_READ_CTRL_REG, &inCtrlBlock, sizeof(PHCB), &outCtrlBlock, sizeof ( PHCB ), &nBytes, NULL) == 0)
			{
            value = true;
            memcpy ( &tConfig, &outCtrlBlock.Data, outCtrlBlock.TxBytes);
         }
      }
   }
   return value;
}//@CODE_85


/*@NOTE_359
Reads image currently in RAM of Print Head Controller Card.
This is the Image the card is printing.
*/
bool CMphc::ReadImage(HANDLE DeviceID, unsigned char* pBuffer,
                      unsigned long lBytes)
{//@CODE_359
   bool value = false;
   HANDLE hDevice; 
   unsigned long Addr;
   ULONG nBytes;
   PHCB inCtrlBlock;

	// Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			inCtrlBlock.lAddress = Addr;
			if ( DeviceIo (hDevice, IOCTL_READ_IMAGE, &inCtrlBlock, sizeof ( PHCB ), pBuffer, lBytes, &nBytes, NULL) == 0)
				value = true;
		}
	}
   return value;
}//@CODE_359

bool CMphc::WriteWatchdog (HANDLE DeviceID, tagPeak603VLWatchdog reg)
{
 	CSingleLock lock (&m_cs, TRUE);

	bool bResult = false;
	HANDLE hDevice; 
	unsigned long Addr;
	unsigned short wSpeed = 0L;
	ULONG nBytes = 0;
	PHCB inCtrlBlock;

	if (m_mapDevID.Lookup((ULONG)DeviceID, (ULONG &)Addr)) {
		if (m_mapAddress.Lookup (Addr, (ULONG &)hDevice)) {
			memset (&inCtrlBlock, 0x00, sizeof (inCtrlBlock));

			inCtrlBlock.lAddress	= Addr;
			inCtrlBlock.TxBytes		= sizeof (unsigned short);
			memcpy (inCtrlBlock.Data, &reg, sizeof (reg));

			if (DeviceIo (hDevice, IOCTL_WRITE_WATCHDOG, &inCtrlBlock, sizeof (PHCB), NULL, 0, &nBytes, NULL) == 0)
				bResult = true;
		}
	}
	
	return bResult;
}

bool CMphc::WriteWatchdog (HANDLE DeviceID, UCHAR c)
{
 	CSingleLock lock (&m_cs, TRUE);

	bool bResult = false;
	HANDLE hDevice; 
	unsigned long Addr;
	unsigned short wSpeed = 0L;
	ULONG nBytes = 0;
	PHCB inCtrlBlock;

	if (m_mapDevID.Lookup((ULONG)DeviceID, (ULONG &)Addr)) {
		if (m_mapAddress.Lookup (Addr, (ULONG &)hDevice)) {
			memset (&inCtrlBlock, 0x00, sizeof (inCtrlBlock));

			inCtrlBlock.lAddress	= Addr;
			inCtrlBlock.TxBytes		= sizeof (unsigned short);
			memcpy (inCtrlBlock.Data, &c, sizeof (c));

			if (DeviceIo (hDevice, IOCTL_WRITE_WATCHDOG, &inCtrlBlock, sizeof (PHCB), NULL, 0, &nBytes, NULL) == 0)
				bResult = true;
		}
	}
	
	return bResult;
}

bool CMphc::ReadWatchdog (HANDLE DeviceID, tagPeak603VLWatchdog & reg)
{
 	CSingleLock lock (&m_cs, TRUE);

	bool bResult = false;
	HANDLE hDevice; 
	unsigned long Addr;
	unsigned short wSpeed = 0L;
	ULONG nBytes = 0;
	PHCB inCtrlBlock;
	PHCB outCtrlBlock;

	if (m_mapDevID.Lookup((ULONG)DeviceID, (ULONG &)Addr)) {
		if (m_mapAddress.Lookup (Addr, (ULONG &)hDevice)) {
			memset (&inCtrlBlock, 0x00, sizeof (inCtrlBlock));

			inCtrlBlock.lAddress	= Addr;
			inCtrlBlock.TxBytes		= sizeof (unsigned short);
			memcpy (&outCtrlBlock, &inCtrlBlock, sizeof (PHCB));

			if (DeviceIo (hDevice, IOCTL_READ_WATCHDOG, &inCtrlBlock, sizeof (PHCB), NULL, 0, &nBytes, NULL) == 0) {
				memcpy (&reg, outCtrlBlock.Data, sizeof (reg));
				bResult = true;
			}
		}
	}
	
	return bResult;
}

bool CMphc::ReadWatchdog (HANDLE DeviceID, UCHAR & c)
{
 	CSingleLock lock (&m_cs, TRUE);

	bool bResult = false;
	HANDLE hDevice; 
	unsigned long Addr;
	unsigned short wSpeed = 0L;
	ULONG nBytes = 0;
	PHCB inCtrlBlock;
	PHCB outCtrlBlock;

	if (m_mapDevID.Lookup((ULONG)DeviceID, (ULONG &)Addr)) {
		if (m_mapAddress.Lookup (Addr, (ULONG &)hDevice)) {
			memset (&inCtrlBlock, 0x00, sizeof (inCtrlBlock));

			inCtrlBlock.lAddress	= Addr;
			inCtrlBlock.TxBytes		= sizeof (unsigned short);
			memcpy (&outCtrlBlock, &inCtrlBlock, sizeof (PHCB));

			if (DeviceIo (hDevice, IOCTL_READ_WATCHDOG, &inCtrlBlock, sizeof (PHCB), NULL, 0, &nBytes, NULL) == 0) {
				memcpy (&c, outCtrlBlock.Data, sizeof (c));
				bResult = true;
			}
		}
	}
	
	return bResult;
}

/*@NOTE_146
Reads the line ( conveyor ) speed from the card.
*/
bool CMphc::ReadLineSpeed(HANDLE DeviceID, double& dSpeed)
{//@CODE_146
 	CSingleLock sLock ( &m_cs );
	sLock.Lock();

	bool value = false;
   HANDLE hDevice; 
   unsigned long Addr;
	unsigned short wSpeed = 0L;
   ULONG nBytes = 0;
   PHCB inCtrlBlock;
   PHCB outCtrlBlock;

   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
         inCtrlBlock.lAddress = Addr;
         inCtrlBlock.RegID = 0x0E;
         inCtrlBlock.TxBytes = sizeof ( unsigned short );
			memcpy (&outCtrlBlock, &inCtrlBlock, sizeof ( PHCB ) );

         if ( DeviceIo (hDevice, IOCTL_READ_CTRL_REG, &inCtrlBlock, sizeof(PHCB), &outCtrlBlock, sizeof ( PHCB ), &nBytes, NULL) == 0)
         {
				wSpeed = outCtrlBlock.Data[1];
				wSpeed <<= 8;
				wSpeed |= outCtrlBlock.Data[0];
				//{ CString str; str.Format (_T ("ReadLineSpeed: 0x%02X 0x%02X"), outCtrlBlock.Data[1], outCtrlBlock.Data[0]); TRACEF (str); }
				dSpeed = ((double)wSpeed / (double)((double) FTACH_100/ (double)100));
            value = true;
         }
      }

		#if( __CUSTOM__ == __SW0882__ )
			#ifdef _DEBUG
				   return value;
			#endif
		#endif

		DemoMemicPhotocell ( DeviceID );
   }
   return value;
}//@CODE_146



/*@NOTE_477
Register and event for notification of End of Ams Cycle
*/
bool CMphc::RegisterAmsEvent(HANDLE DeviceID, HANDLE hEvent)
{//@CODE_477
   bool value = false;
   ULONG Data[2];
   ULONG nBytes = 0;
   ULONG Addr;
   HANDLE hDevice;

   // Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			if ( !m_bDemo ) 
			{
				Data[0] = Addr;
				Data[1] = (ULONG) hEvent;
				if ( DeviceIo (hDevice, IOCTL_REGISTER_AMS_EVENT, &Data, sizeof (Data), NULL, 0, &nBytes, NULL) == 0)
					value = true;
			}
		}
   }
   return value;
}//@CODE_477


bool CMphc::RegisterEOPEvent(HANDLE DeviceID, HANDLE hEvent)
{//@CODE_468
 	CSingleLock sLock ( &m_cs );
	sLock.Lock();

	bool value = false;
   ULONG Data[2];
   ULONG nBytes = 0;
   ULONG Addr;
   HANDLE hDevice;

   // Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			if ( !m_bDemo ) 
			{
				Data[0] = Addr;
				Data[1] = (ULONG) hEvent;
				if ( DeviceIo (hDevice, IOCTL_REGISTER_EOP_EVENT, &Data, sizeof (Data), NULL, 0, &nBytes, NULL) == 0)
					value = true;
			}
			else {
				tagPhotoDemo photo;
				
				memset (&photo, 0, sizeof (photo));
				photo.m_hEOPEvent = hEvent;

				if (m_DemoPhotoEvent.Lookup (DeviceID, photo)) 
					photo.m_hEOPEvent = hEvent;

				m_DemoPhotoEvent.SetAt ( DeviceID, photo );
			}
		}
   }

   return value;
}//@CODE_468


/*@NOTE_137
Registers an event handle with the driver for the Photocell Event then Enables interrupts 
on the Card.
*/
bool CMphc::RegisterPhotoEvent(HANDLE DeviceID, HANDLE hEvent)
{//@CODE_137
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool value = false;
   ULONG Data[2];
   ULONG nBytes = 0;
   ULONG Addr;
   HANDLE hDevice;

   // Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			if ( m_bDemo ) 
			{
				tagPhotoDemo photo;

				memset (&photo, 0, sizeof (photo));
				photo.m_hPhotoEvent	= hEvent;

				if (m_DemoPhotoEvent.Lookup (DeviceID, photo)) 
					photo.m_hPhotoEvent = hEvent;

				m_DemoPhotoEvent.SetAt ( DeviceID, photo );
			}
			else {
				Data[0] = Addr;
				Data[1] = (ULONG) hEvent;
				if ( DeviceIo (hDevice, IOCTL_REGISTER_PHOTO_EVENT, &Data, sizeof (Data), NULL, 0, &nBytes, NULL) == 0)
					value = true;
			}
		}
   }
   return value;
}//@CODE_137


/*@NOTE_387
Registers an Event for serial data with the driver.
*/
bool CMphc::RegisterSerialEvent(HANDLE DeviceID, HANDLE hEvent)
{//@CODE_387
   bool value = false;
   ULONG Data[2];
   ULONG nBytes = 0;
   ULONG Addr;
   HANDLE hDevice;

   // Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			if ( !m_bDemo ) 
			{
				Data[0] = Addr;
				Data[1] = (ULONG) hEvent;
				if ( DeviceIo (hDevice, IOCTL_REGISTER_SERIAL_EVENT, &Data, sizeof (Data), NULL, 0, &nBytes, NULL) == 0)
					value = true;
			}
		}
   }
   return value;
}//@CODE_387



/*@NOTE_207
DeAllocates Image buffer allcated by GetImageBuffer.
*/
void CMphc::ReleaseImageBuffer(unsigned char* pBuffer)
{//@CODE_207
	delete [] pBuffer;
	pBuffer = NULL;
}//@CODE_207


bool CMphc::SetAmsState(HANDLE DeviceID, BOOL State)
{//@CODE_456
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool value = false;
   HANDLE hDevice; 
   unsigned long Addr;
   ULONG nBytes;
   PHCB inCtrlBlock;
   tagCtrlReg tCtrl;
	tagRegisters tCurrent;

   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
         inCtrlBlock.lAddress = Addr;
         if ( ReadConfiguration ( DeviceID, tCurrent ) )
         {
				tCtrl = tCurrent._CtrlReg;
				tCtrl._PrintBuffer = 0x00;
				if ( State == 0x00 )
  				   tCtrl._FireAMS = 0x00;
            else
  				   tCtrl._FireAMS = 0x01;

            inCtrlBlock.RegID = 0x00;
				inCtrlBlock.TxBytes = sizeof ( tagCtrlReg );
				memcpy ( &inCtrlBlock.Data, &tCtrl, sizeof ( tagCtrlReg ) );
				if ( DeviceIo (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof ( PHCB ), NULL, 0, &nBytes, NULL) == 0)
					value = true;
			}
		}
	}
   return value;
}//@CODE_456


/*@NOTE_364
Sets the Length of the actual image.  This needs to include flush buffer
*/
bool CMphc::SetImageLen(HANDLE DeviceID, WORD wImageCols)
{//@CODE_364
//	{ CString str; str.Format ("SetImageLen: 0x%X, %d", DeviceID, wImageCols); TRACEF (str);}

	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool value = false;
   HANDLE hDevice; 
   unsigned long Addr;
   ULONG nBytes;
   PHCB inCtrlBlock;

   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
     // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
			inCtrlBlock.lAddress = Addr;
			inCtrlBlock.RegID = 0x22;
			inCtrlBlock.TxBytes = 2;
			memset ( &inCtrlBlock.Data, 0x00, sizeof (inCtrlBlock.Data) );
			inCtrlBlock.Data[0] = LOBYTE(wImageCols);
			inCtrlBlock.Data[1] = HIBYTE(wImageCols);
			if ( DeviceIo (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof ( PHCB ), NULL, 0, &nBytes, NULL) == 0)
				value = true;
		}
	}
	sLock.Unlock();
   return value;
}//@CODE_364


/*@NOTE_382
Set card to MASTER mode.  Forces remaining cards to SLAVE mode.
MASTER mode card routes its interrupt onto the ISA bus
SLAVE mode card routes its interrupt onto the Over-The-Top bus.

DO NOT CALL

*/
bool CMphc::SetMasterMode(HANDLE DeviceID)
{//@CODE_382
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

	bool value = false;
	HANDLE hDevice; 
	unsigned long Addr;
	ULONG nBytes;
	// Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			if ( DeviceIo (hDevice, IOCTL_SET_MASTER_MODE, &Addr, sizeof (Addr), NULL, 0, &nBytes, NULL) == 0)
				value = true;
		}
	}
	sLock.Unlock();

	return value;
}//@CODE_382


bool CMphc::SetPrintDelay(HANDLE DeviceID, WORD wDelay)
{//@CODE_343
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool value;
   HANDLE hDevice; 
   unsigned long Addr;
   ULONG nBytes;
	PHCB inCtrlBlock;

   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
         inCtrlBlock.lAddress = Addr;
         inCtrlBlock.RegID = 0x20;
         inCtrlBlock.TxBytes = sizeof ( WORD );
			inCtrlBlock.Data[0] = LOBYTE ( wDelay );
			inCtrlBlock.Data[1] = HIBYTE ( wDelay );
			if ( DeviceIo (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof ( PHCB ), NULL, 0, &nBytes, NULL) == 0)
				value = true;
      }
   }
   sLock.Unlock();
	return value;
}//@CODE_343


/*@NOTE_214
Sets the Product Lenght.
The MPHC treats this as a delay, so this is the number of columns to delay after the
image is printed before allowing the next photocell.
*/
bool CMphc::SetProductLen(HANDLE DeviceID, WORD wProdLen)
{//@CODE_214
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

	bool value = false;
   HANDLE hDevice; 
   unsigned long Addr;
   ULONG nBytes;
	PHCB inCtrlBlock;

   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
         inCtrlBlock.lAddress = Addr;
         inCtrlBlock.RegID = 0x2C;
         inCtrlBlock.TxBytes = sizeof ( WORD );
			inCtrlBlock.Data[0] = LOBYTE ( wProdLen );
			inCtrlBlock.Data[1] = HIBYTE ( wProdLen );
			if ( DeviceIo (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof ( PHCB ), NULL, 0, &nBytes, NULL) == 0)
				value = true;
      }
   }
	sLock.Unlock();
	return value;
}//@CODE_214


/*@NOTE_474
Place Printhead into reset
*/
BOOL CMphc::SetResetBit(HANDLE DeviceID, bool mode)
{//@CODE_474
	CSingleLock sLock ( &m_cs );
	sLock.Lock();
	
	bool value = false;
	HANDLE hDevice; 
	unsigned long Addr;
	ULONG nBytes;
	PHCB inCtrlBlock;
	tagCtrlReg tCtrl;
	tagRegisters tCurrent;
	
	// Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			inCtrlBlock.lAddress = Addr;
			if ( ReadConfiguration ( DeviceID, tCurrent ) )
			{
				tCtrl = tCurrent._CtrlReg;
				tCtrl._PrintBuffer = 0;
				tCtrl._FireAMS = 0x00;
				
				if ( mode == true ) {
					tCtrl._PrintEnable = 1;
					tCtrl._HeadType = 0;
				}
				else {
					tCtrl._PrintEnable = 0;
					tCtrl._HeadType = 0;
				}
				
				inCtrlBlock.RegID = 0x00;
				inCtrlBlock.TxBytes = sizeof ( tCtrl );
				memcpy ( &inCtrlBlock.Data, &tCtrl, sizeof ( tCtrl ) );
				if ( DeviceIo (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof ( PHCB ), NULL, 0, &nBytes, NULL) == 0) 
					value = true;
			}
		}
	}
	return value;
}//@CODE_474

bool CMphc::SetSpeedGateWidth(HANDLE DeviceID, WORD wEncRes)
{//@CODE_347
	CSingleLock sLock ( &m_cs );
	sLock.Lock();
 
	bool value;
   HANDLE hDevice; 
   unsigned long Addr;
   ULONG nBytes;
	PHCB inCtrlBlock;
	WORD Speed;

   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
         inCtrlBlock.lAddress = Addr;
         inCtrlBlock.RegID = 0x0E;
         inCtrlBlock.TxBytes = sizeof ( WORD );
			Speed = ComputeSpeedGateWidth ( wEncRes );
			memcpy ( &inCtrlBlock.Data, &Speed, sizeof ( WORD ) );
			{ CString str; str.Format (_T ("SetSpeedGateWidth: 0x%02X 0x%02X"), inCtrlBlock.Data [0], inCtrlBlock.Data[1]); TRACEF (str); }
			if ( DeviceIo (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof ( PHCB ), NULL, 0, &nBytes, NULL) == 0)
				value = true;
      }
   }
   return value;
}//@CODE_347


bool CMphc::SetStrobe(HANDLE DeviceID, int Color)
{//@CODE_461
 	CSingleLock sLock ( &m_cs );
	sLock.Lock();
   
	bool value = false;
   HANDLE hDevice; 
   unsigned long Addr;
   ULONG nBytes = 0;
   PHCB inCtrlBlock;
   PHCB outCtrlBlock;

	tagInterruptCtrlReg	StrobeReg;

   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
         memset ( &StrobeReg, 0x00, sizeof (tagInterruptCtrlReg) );
			memset ( &inCtrlBlock, 0x00, sizeof ( PHCB ) );
         inCtrlBlock.lAddress = Addr;
         inCtrlBlock.RegID = 0x01;
         inCtrlBlock.TxBytes = sizeof ( tagInterruptCtrlReg );
         if ( DeviceIo (hDevice, IOCTL_READ_CTRL_REG, &inCtrlBlock, sizeof(PHCB), &outCtrlBlock, sizeof ( PHCB ), &nBytes, NULL) == 0)
			{
				memcpy ( &StrobeReg, outCtrlBlock.Data, sizeof ( tagInterruptCtrlReg ) );
				StrobeReg._ErrDrv = Color;
				memcpy ( outCtrlBlock.Data, &StrobeReg, sizeof ( tagInterruptCtrlReg ) );
				if ( DeviceIo (hDevice, IOCTL_WRITE_CTRL_REG, &outCtrlBlock, sizeof ( PHCB ), NULL, 0, &nBytes, NULL) == 0)
					value = true;
         }
      }
   }
	sLock.Unlock();
	return value;
}//@CODE_461


/*@NOTE_480
Un Register previously register event
*/
bool CMphc::UnregisterAmsEvent(HANDLE DeviceID)
{//@CODE_480
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool value = false;
   ULONG nBytes = 0;
   ULONG Addr;
   HANDLE hDevice;

   // Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			if ( DeviceIo (hDevice, IOCTL_UNREGISTER_AMS_EVENT, &Addr, sizeof (Addr), NULL, 0, &nBytes, NULL) == 0)
				value = true;
		}
   }
	sLock.Unlock();
   return value;
}//@CODE_480


bool CMphc::UnregisterEOPEvent(HANDLE DeviceID)
{//@CODE_472
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool value = false;
   ULONG nBytes = 0;
   ULONG Addr;
   HANDLE hDevice;

   // Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			if ( DeviceIo (hDevice, IOCTL_UNREGISTER_EOP_EVENT, &Addr, sizeof (Addr), NULL, 0, &nBytes, NULL) == 0)
				value = true;

			if (m_bDemo)
				m_DemoPhotoEvent.RemoveKey (DeviceID);
		}
   }
	sLock.Unlock();
   return value;
}//@CODE_472


/*@NOTE_144
Unregister Photocell event and disable card interrupts.
This function should be called before exit.
*/
bool CMphc::UnregisterPhotoEvent(HANDLE DeviceID)
{//@CODE_144
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool value = false;
   ULONG nBytes = 0;
   ULONG Addr;
   HANDLE hDevice;

   // Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			if ( DeviceIo (hDevice, IOCTL_UNREGISTER_PHOTO_EVENT, &Addr, sizeof (Addr), NULL, 0, &nBytes, NULL) == 0)
				value = true;

			if (m_bDemo)
				m_DemoPhotoEvent.RemoveKey (DeviceID);
		}
   }
	sLock.Unlock();
   return value;
}//@CODE_144


/*@NOTE_391
Unregisters a previously registered serial event with the driver.
*/
bool CMphc::UnregisterSerialEvent(HANDLE DeviceID)
{//@CODE_391
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool value = false;
   ULONG nBytes = 0;
   ULONG Addr;
   HANDLE hDevice;

   // Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			if ( DeviceIo (hDevice, IOCTL_UNREGISTER_SERIAL_EVENT, &Addr, sizeof (Addr), NULL, 0, &nBytes, NULL) == 0)
				value = true;
		}
   }
	sLock.Unlock();
   return value;
}//@CODE_391


int CMphc::UpdateImageData(HANDLE DeviceID, void* pImageBuffer,
                           unsigned char nBPC, WORD wStartCol, WORD wNumCols)
{//@CODE_23
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool value = false;
   HANDLE hDevice; 
   unsigned long Addr;
   ULONG nBytes;
   PHCB inCtrlBlock;

	/*
	{
		CBitmap bmp;
		CString str;
		int cx = nBPC * 8;
		int cy = wNumCols;

		str.Format ("C:\\Temp\\Debug\\[%d] %05d, %05d.bmp", DeviceID, wStartCol, wStartCol + wNumCols);
		VERIFY (bmp.CreateBitmap (cx, cy, 1, 1, pImageBuffer));
		SAVEBITMAP (bmp, str);

		//str.Format ("[%d] %05d, %05d [%d]", DeviceID, wStartCol, wStartCol + wNumCols, wNumCols); TRACEF (str);
	}
	*/

   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
     // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
			inCtrlBlock.lAddress = Addr;
			inCtrlBlock.RegID = 0xFF;
			inCtrlBlock.TxBytes = 0;
			memset ( &inCtrlBlock.Data, 0x00, sizeof (inCtrlBlock.Data) );
			tagImgUpdateHdr ImageHdr;
			ImageHdr._wStartCol = wStartCol;
			ImageHdr._wNumCols = wNumCols;
			ImageHdr._nBPC = nBPC;
			memcpy ( &inCtrlBlock.Data, &ImageHdr, sizeof ( ImageHdr ) );
			ULONG ImgLen = ImageHdr._wNumCols * ImageHdr._nBPC;
			PUCHAR pData = NULL;
			try 
			{
				pData = new UCHAR[sizeof (PHCB) + ImgLen];
				PUCHAR p = pData;
				memcpy ( p, &inCtrlBlock, sizeof (PHCB) );
				p += sizeof (PHCB);
				memcpy ( p, pImageBuffer,	ImgLen);
			}
			catch ( CMemoryException *e ) {e->ReportError(); e->Delete(); return value;}

//			if ( DeviceIo (hDevice, IOCTL_IMAGE_UPDATE, pData, sizeof (PHCB) + ImgLen, NULL, 0, &nBytes, NULL) == 0)
			if ( DeviceIo (hDevice, IOCTL_IMAGE_UPDATE_DIRECTIO, NULL, 0, pData, sizeof (PHCB) + ImgLen, &nBytes, NULL) == 0)
				value = true;

			if ( pData )
				delete pData;
		}
   }
	sLock.Unlock();
   return value;
}//@CODE_23


/*@NOTE_91
Writes an updated print head configuration to the approprate card.
Uses a Database head structure.
*/
bool CMphc::WriteSetup(HANDLE DeviceID, tagSetupRegs tSetup,
                       tagFireVibPulse tPulseRegs)
{//@CODE_91
	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool value = false;
   HANDLE hDevice; 
   unsigned long Addr;
   ULONG nBytes;
   PHCB inCtrlBlock;
   tagRegisters tCurrent;
   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
         inCtrlBlock.lAddress = Addr;
         if ( ReadConfiguration ( DeviceID, tCurrent ) )
         {
				tSetup._Interrupt_Reg = tCurrent._Interrupt_Reg;
				tSetup._CtrlReg._PrintEnable = tCurrent._CtrlReg._PrintEnable;

// Debug Stuff
//				tSetup._CtrlReg._DoublePulse = !tCurrent._CtrlReg._DoublePulse;
				
				tSetup._CtrlReg._FireAMS = 0x00;
				inCtrlBlock.RegID = 0x00;
				inCtrlBlock.TxBytes = sizeof ( tSetup );
				memcpy ( &inCtrlBlock.Data, &tSetup, sizeof (tSetup));
				if ( DeviceIo (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof ( PHCB ), NULL, 0, &nBytes, NULL) == 0)
					value = true;

				inCtrlBlock.RegID = 0x10;
				inCtrlBlock.TxBytes = sizeof ( tagFireVibPulse );
				memcpy ( &inCtrlBlock.Data, &tPulseRegs, inCtrlBlock.TxBytes );
				if ( DeviceIo (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof ( PHCB ), NULL, 0, &nBytes, NULL) == 0)
					value = true;
			}	// ReadConfiguration
      }
   }
   return value;
}//@CODE_91


//{{AFX DO NOT EDIT CODE BELOW THIS LINE !!!

/*@NOTE_20
Method which must be called first in a constructor.
*/
void CMphc::ConstructorInclude()
{
}


/*@NOTE_21
Method which must be called first in a destructor.
*/
void CMphc::DestructorInclude()
{
}


// Methods for the relation(s) of the class

//}}AFX DO NOT EDIT CODE ABOVE THIS LINE !!!

//@START_USER3


bool CMphc::SetProductionMode (HANDLE DeviceID, bool bProduction)
{
	CSingleLock lock (&m_cs, TRUE);

	bool value = false;
/*
	HANDLE hDevice; 
	unsigned long Addr;
	ULONG nBytes;
	PHCB inCtrlBlock;

	if (m_mapDevID.Lookup ((ULONG)DeviceID, (ULONG &)Addr)) {
		if (m_mapAddress.Lookup (Addr, (ULONG &)hDevice)) {
			tagReg8 reg;

			ZeroMemory (&reg, sizeof (reg));

			reg.data = bProduction ? 1 : 0;

			inCtrlBlock.lAddress = Addr;
			inCtrlBlock.RegID = OTB_STATUS;
			inCtrlBlock.TxBytes = sizeof (reg);
			memcpy ( &inCtrlBlock.Data, &reg, sizeof (reg));

			if ( DeviceIo (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof (PHCB), NULL, 0, &nBytes, NULL) == 0)
				value = true;
		}
	}
*/
	return value;
}
