/******************************************************************************\
*
* File:          MphcCfg.cpp
* Creation date: June 28, 2002 20:23
* Author:        Chris Hodge
*                Foxjet Tech Center
*
* Case Tool:     ClassBuilder 2.3
*                
* Purpose:       Method implementations of class 'CMphcCfg'
*
* Modifications: @INSERT_MODIFICATIONS(* )
* June 28, 2002 20:44 Chris Hodge
*     Updated code of method '~CMphcCfg'
*     Updated code of method 'CMphcCfg'
*     Updated member 'm_Inverted'
*     Updated member 'm_Direction'
*     Updated member 'm_HeadType'
* June 28, 2002 20:42 Chris Hodge
*     Updated member '_Inverted'
*     Updated member '_Direction'
* June 28, 2002 20:41 Chris Hodge
*     Deleted member '_Test'
*     Deleted member '_HeadType'
*     Added member '_Inverted'
*     Added member '_Direction'
*     Added member '_HeadType'
*     Updated code of method 'CMphcCfg'
* June 28, 2002 20:34 Chris Hodge
*     Added member '_Test'
* June 28, 2002 20:24 Chris Hodge
*     Added method 'CMphcCfg'
* June 28, 2002 20:23 Chris Hodge
*     Added method 'DestructorInclude'
*     Added method 'ConstructorInclude'
*     Added method '~CMphcCfg'
*     Added member 'm_HeadType'
*
* Copyright 2002, Foxjet Tech Center
* All rights are reserved. Reproduction in whole or part is prohibited
* without the written consent of the copyright owner.
*
\******************************************************************************/
//@START_USER1
//@END_USER1


// Master include file
#include "StdAfx.h"


//@START_USER2
#include "MphcCfg.h"
//@END_USER2


// Static members


/*@NOTE_164
Constructor method.
*/
CMphcCfg::CMphcCfg() //@INIT_164

	: m_HeadType(0)
   , m_Direction(0)
   , m_Inverted(0)
{//@CODE_164
   ConstructorInclude();

   // Put in your own code
}//@CODE_164


/*@NOTE_160
Destructor method.
*/
CMphcCfg::~CMphcCfg()
{//@CODE_160
   DestructorInclude();

   // Put in your own code
}//@CODE_160


//{{AFX DO NOT EDIT CODE BELOW THIS LINE !!!

/*@NOTE_161
Method which must be called first in a constructor.
*/
void CMphcCfg::ConstructorInclude()
{
}


/*@NOTE_162
Method which must be called first in a destructor.
*/
void CMphcCfg::DestructorInclude()
{
}


// Methods for the relation(s) of the class

//}}AFX DO NOT EDIT CODE ABOVE THIS LINE !!!

//@START_USER3
