#include "StdAfx.h"
#include <WinIoctl.h>
#include <initguid.h>
#include "..\FxMphc\Src\IoCtls.h"
#include "Mphc.h"
#include "Debug.h"
#include "DevGuids.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
namespace DriverDebug
{
	#define DEBUG_IOCTL_ENTRY(n) { (n), _T (#n), }

	struct 
	{
		DWORD	m_dw;
		LPCTSTR m_lpsz;
	}
	static const map [] = 
	{
		DEBUG_IOCTL_ENTRY (IOCTL_GET_VERSION),
		DEBUG_IOCTL_ENTRY (IOCTL_ENUM_IO_ADDRESSES),
		DEBUG_IOCTL_ENTRY (IOCTL_READ_CTRL_REG),
		DEBUG_IOCTL_ENTRY (IOCTL_READ_IMAGE),
		DEBUG_IOCTL_ENTRY (IOCTL_READ_SERIAL_DATA),
		DEBUG_IOCTL_ENTRY (IOCTL_READ_AMS_STATE),
		DEBUG_IOCTL_ENTRY (IOCTL_WRITE_FPGA),
		DEBUG_IOCTL_ENTRY (IOCTL_SET_SERIAL_PARAMS),
		DEBUG_IOCTL_ENTRY (IOCTL_SET_SERIAL_EVENT_HANDLE),
		DEBUG_IOCTL_ENTRY (IOCTL_REGISTER_PHOTO_EVENT),
		DEBUG_IOCTL_ENTRY (IOCTL_UNREGISTER_PHOTO_EVENT),
		DEBUG_IOCTL_ENTRY (IOCTL_IMAGE_UPDATE),
		DEBUG_IOCTL_ENTRY (IOCTL_WRITE_CTRL_REG),
		DEBUG_IOCTL_ENTRY (IOCTL_SET_MASTER_MODE),
		DEBUG_IOCTL_ENTRY (IOCTL_REGISTER_SERIAL_EVENT),
		DEBUG_IOCTL_ENTRY (IOCTL_UNREGISTER_SERIAL_EVENT),
		DEBUG_IOCTL_ENTRY (IOCTL_CLEAR_BUFFER),
		DEBUG_IOCTL_ENTRY (IOCTL_REGISTER_EOP_EVENT),
		DEBUG_IOCTL_ENTRY (IOCTL_UNREGISTER_EOP_EVENT),
		DEBUG_IOCTL_ENTRY (IOCTL_REGISTER_AMS_EVENT),
		DEBUG_IOCTL_ENTRY (IOCTL_UNREGISTER_AMS_EVENT),
		DEBUG_IOCTL_ENTRY (IOCTL_RESET_FLAGS),
		DEBUG_IOCTL_ENTRY (IOCTL_GET_FLAGS),
		DEBUG_IOCTL_ENTRY (IOCTL_GET_STATE),
		DEBUG_IOCTL_ENTRY (IOCTL_SET_CONFIG),		
		DEBUG_IOCTL_ENTRY (IOCTL_READ_REGISTER),
		DEBUG_IOCTL_ENTRY (IOCTL_WRITE_REGISTER),
		DEBUG_IOCTL_ENTRY (IOCTL_SET_STROBE),
		DEBUG_IOCTL_ENTRY (IOCTL_ENABLE_PRINT),		
		DEBUG_IOCTL_ENTRY (IOCTL_ENABLE_DEBUG),		
		DEBUG_IOCTL_ENTRY (IOCTL_READ_WATCHDOG),		
		DEBUG_IOCTL_ENTRY (IOCTL_WRITE_WATCHDOG),		
	};

	CString GetString (DWORD dw)
	{
		for (int i = 0; i < ARRAYSIZE (map); i++) 
			if (map [i].m_dw == dw)
				return map [i].m_lpsz;

		DWORD dwFunction = (0xFFF) & (dw >> 2);
		CString str;

		str.Format (_T ("0x%03X (Unknown)"), dwFunction);

		return str;
	}
}; //namespace DriverDebug;

#endif //_DEBUG

CDriverInterface::CDriverInterface (bool bDemo, const GUID & guid)
:	CDeviceList (guid),
	m_lNextID (0),
	m_bDemo (bDemo)
{
	Initialize();

	if (m_bDemo) {
		CArray <ULONG, ULONG> vAddr;

		if (guid == GUID_INTERFACE_FXMPHC) {
			vAddr.Add (0x300);
			vAddr.Add (0x310);
			vAddr.Add (0x320);
			vAddr.Add (0x330);
			vAddr.Add (0x340);
			vAddr.Add (0x350);
		}
		else if (guid == GUID_INTERFACE_IVPHC) {
			vAddr.Add (0x300);
			vAddr.Add (0x320);
		}

		m_vDevices.Add (INVALID_HANDLE_VALUE);

		for (int i = 0; i < vAddr.GetSize (); i++)
			m_mapAddress.SetAt (vAddr [i], (DWORD)INVALID_HANDLE_VALUE);
	}
	else {
		for (int i = 0; i < m_list.GetSize(); i++) {
			CDeviceListEntry Entry = m_list.GetAt(i);
			HANDLE hDevice = CreateFile (Entry.m_linkname, GENERIC_READ|GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

			if (hDevice != NULL) {
				m_vDevices.Add (hDevice);
				_VERSION ver;
				ULONG lBytes = 0;

				memset (&ver, 0x00, sizeof (ver));
				DeviceIo (hDevice, IOCTL_GET_VERSION, NULL, 0, &ver, sizeof (ver), &lBytes, NULL);

				#ifdef _DEBUG
				CString str;

				str.Format (_T ("IOCTL_GET_VERSION: %d.%03d.%03d"), ver.Major, ver.Minor, ver.Revision);
				TRACEF (str);
				#endif
			}
			else  {
				CString str;
				str.Format (_T ("Attempt to open %s\nID{%s} FAILED!"), Entry.m_friendlyname, Entry.m_linkname);
				TRACEF (str);
				::AfxMessageBox (str);
			}
		}

		for (int i = 0; i < m_vDevices.GetSize(); i++) {
			ULONG lAddressData [8];
			ULONG lBytes = 0;
			memset (&lAddressData, 0x00, sizeof (lAddressData)); 
			HANDLE hDevice = m_vDevices [i];

			DeviceIo (hDevice, IOCTL_ENUM_IO_ADDRESSES, NULL, 0, &lAddressData, sizeof (lAddressData), &lBytes, NULL);
			
			for (unsigned long j = 0; j < lBytes/sizeof(ULONG); j++)
				m_mapAddress.SetAt (lAddressData[j], (DWORD)hDevice);
		}
	}
}

CDriverInterface::~CDriverInterface ()
{
}

/*
This function provides a single entry point for all DeviceIoControl Calls.  The only reason
this function exist is so that we can handle the Demo mode of the application easily.  All
DeviceIoControl calls go through here. Here we can check to see if we are in Demo Mode,
if we are then we should not call DeviceIoControl but instead memic it's results.
*/
DWORD CDriverInterface::DeviceIo(HANDLE hDevice, DWORD dwIoControlCode, 
								 void* pInBuffer, DWORD dwInBufferSize, 
								 void* pOutBuffer, DWORD dwOutBufferSize, 
								 DWORD* pBytesRtn, _OVERLAPPED* pOverLapped)
{
	DWORD dwResult = 0;
	static bool bFlipFlop1 = false;
	static bool bFlipFlop2 = false;

	if (!m_bDemo) {
		if (!::DeviceIoControl (hDevice, dwIoControlCode, pInBuffer, dwInBufferSize, pOutBuffer, dwOutBufferSize, pBytesRtn, pOverLapped)) {
			//DeviceError ();
			dwResult = GetLastError();
			TRACEF (DriverDebug::GetString (dwIoControlCode) + " failed: " + FormatMessage (dwResult));
		}
	}
	else
	{
		switch ( dwIoControlCode )
		{
		case IOCTL_READ_CTRL_REG:
			{
				PHCB* pPHCB = (PHCB*)pOutBuffer;
				switch ( pPHCB->RegID )
				{
					case 14:	// Conveyor Speed;
						DemoMemicLineSpeed ( pPHCB );
						break;
					case 4:	// head Indicators;
						DemoMemicIndicators ( pPHCB );
						break;
				}
			}
			break;
		}
		dwResult = 0;
	}
	
	return dwResult;
}

/*
Claim the card address as being owned.  Functions checks to see if address has
been previously claimed.  If not it claims it and returns a unique handle to
reference the address with.  If the address has been claimed the the return
value is NULL
*/
HANDLE CDriverInterface::ClaimAddress(unsigned long Addr)
{
   unsigned long value = NULL;
   unsigned long hDevice = -1;
   unsigned long Key;
   bool claimed = false;
   POSITION pos;

	CSingleLock sLock ( &m_cs );
	sLock.Lock();
	if ( !m_mapAddress.IsEmpty() )
	{
		if ( m_mapAddress.Lookup ( Addr, hDevice ) ) // Check to see if this is a valid address.
		{
			// It is a valid address so see if it has already been claimed.
			pos = m_mapDevID.GetStartPosition();
			while ( pos )
			{
				m_mapDevID.GetNextAssoc( pos, Key, value);
				if ( value == Addr )
				{
					claimed = true;
					break;
				}
			}
			// If is has not been previously claimed then assign it a handle and claim it.
			if ( !claimed ) 
			{
				m_mapDevID.SetAt ( ++m_lNextID, Addr);
				value = m_lNextID;
			}
			else
				value = NULL;
		}
	}
	sLock.Unlock();
	return (HANDLE)value; // Return the assigned handle.
}

bool CDriverInterface::ReleaseAddress(HANDLE DeviceID)
{
	CSingleLock sLock ( &m_cs );
	sLock.Lock();
	bool value = false;
	ULONG Addr;

	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		m_mapDevID.RemoveKey ( (ULONG) DeviceID );
		value = true;
	}
	return value;
}

/*
Writes a new FPGA bit file to the MPHC Params: DeviceID \tab Handle obtained
from ClaimAddress FilePathName\tab Full qualifing path of bit file
*/
int CDriverInterface::ProgramFPGA(HANDLE DeviceID, const CString & strFile)
{
	CSingleLock (&m_cs).Lock();

	int nResult = 0;
	HANDLE hDevice; 
	unsigned long Addr;
	ULONG nBytes;
	CFileException e;
	CFile File;

	if (m_bDemo)
		return 0;

	if (m_mapDevID.Lookup ((ULONG)DeviceID, (ULONG &)Addr)) {
		if (m_mapAddress.Lookup (Addr, (ULONG &)hDevice)) {
			if (File.Open (strFile, CFile::modeRead | CFile::shareDenyWrite, &e)) {
				ULONG Len = File.GetLength();
				PUCHAR p = new UCHAR [Len + sizeof (ULONG)];
				PUCHAR p3 = p;
				PULONG p2 = (PULONG) p;

				*p2 = Addr;
				p3 = (PUCHAR)(p + sizeof (ULONG));
				nBytes = File.Read(p3, Len);
				Len += sizeof (ULONG);
				File.Close();

				nResult = DeviceIo (hDevice, IOCTL_WRITE_FPGA, p, Len, NULL, 0, &nBytes, NULL);
				
				delete [] p;
			}
			else
				e.ReportError();
		}
	}

	return nResult;
}

bool CDriverInterface::ClearPrintBuffer(HANDLE DeviceID)
{
 	CSingleLock sLock ( &m_cs );
	sLock.Lock();

	bool value = false;
	unsigned long Addr;
	HANDLE hDevice; 
	ULONG nBytes = 0;

//	TRACEF ("ClearPrintBuffer");

	// Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
	  // Get the Device Handle associatied with the address.
	  if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		 if ( DeviceIo (hDevice, IOCTL_CLEAR_BUFFER, &Addr, sizeof(ULONG), NULL, 0, &nBytes, NULL) )
				value = true;
	}

	return value;
}

bool CDriverInterface::SetSerialParams(HANDLE DeviceID, tagSerialParams Params)
{//@CODE_368
	bool value = false;
   HANDLE hDevice; 
   unsigned long Addr;
   ULONG nBytes;
	PHCB inCtrlBlock;

   // Get the address associated with the DeviceID
   if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
   {
      // Get the Device Handle associatied with the address.
      if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
      {
         inCtrlBlock.lAddress = Addr;
         inCtrlBlock.RegID = 0xFF;
         inCtrlBlock.TxBytes = sizeof ( tagSerialParams );
			memcpy ( &inCtrlBlock.Data, &Params, inCtrlBlock.TxBytes );
			if ( DeviceIo (hDevice, IOCTL_SET_SERIAL_PARAMS, &inCtrlBlock, sizeof ( PHCB ), NULL, 0, &nBytes, NULL) == 0)
				value = true;
      }
   }
   return value;
}//@CODE_368

bool CDriverInterface::ReadSerialData (HANDLE DeviceID, unsigned char* pData,
									   unsigned int& nBytes)
{//@CODE_393
 	CSingleLock sLock ( &m_cs );
	sLock.Lock();

   bool value = false;
   HANDLE hDevice = NULL; 
   unsigned long Addr = 0;
   ULONG nBytesRet;
   PHCB inCtrlBlock;

	// Get the address associated with the DeviceID
	if ( m_mapDevID.Lookup((ULONG)DeviceID, ( ULONG & )Addr) )
	{
		// Get the Device Handle associatied with the address.
		if ( m_mapAddress.Lookup ( Addr, (ULONG &)hDevice ) )
		{
			inCtrlBlock.lAddress = Addr;
			if ( DeviceIo (hDevice, IOCTL_READ_SERIAL_DATA, &inCtrlBlock, sizeof ( PHCB ), pData, nBytes, &nBytesRet, NULL) == 0)
			{
				nBytes = nBytesRet;
				value = true;
			}
		}
	}
   return value;
}//@CODE_393


void CDriverInterface::DemoMemicLineSpeed(PHCB* pPHCB)
{
}

void CDriverInterface::DemoMemicIndicators(PHCB* pPHCB)
{
}

void CDriverInterface::DemoMemicPhotocell(HANDLE DeviceID)
{
}
