#ifndef _ISA_MASTER_H
#define _ISA_MASTER_H

// Date, Time & Version defines
#define MPHCAPI_DATE    20050525
#define MPHCAPI_TIME    161415
#define MPHCAPI_VERSION 118

// Context define declarations

// Forward extern class declarations
class CWnd;

//@START_USER1
//@END_USER1

// Defines needed for relations between templated classes
#define _CBT_TYPE_ARG_TYPE	<TYPE, ARG_TYPE >
#define _CBT_KEY_ARG_KEY_VALUE_ARG_VALUE	<KEY, ARG_KEY, VALUE, ARG_VALUE >

// Type declarations
//@START_DECLARATION_67 HANDLE

//@END_DECLARATION_67

//@START_DECLARATION_72 DWORD

//@END_DECLARATION_72

//namespace ISA {

	//@START_DECLARATION_134 tagImagePkt
	typedef struct tagImagePkt
	{
		ULONG _lAddress;
		ULONG _lStartCol;
		ULONG _lBytes;
		ULONG _lBytesPerCol;
	}_IMAGE_TX;
	//@END_DECLARATION_134

	//@START_DECLARATION_140 _DBG_FLAGS
	// Defined in FxMphc Driver
	//@END_DECLARATION_140

	//@START_DECLARATION_149 FGATE
	// This is a fixed constant within the MPHC Board.
	const int FGATE = 115200;
	//@END_DECLARATION_149

	//@START_DECLARATION_150 FTACH_100
	// This is the units when the conveyor speed is 100 ft/min
	// This means, if this value is 1000 then when the conveyor is running 
	// at 100 ft/min the value returned when you read the speed register 
	// will be 1000  This allows for as much presision as needed.
	const int FTACH_100 = 1000;
	//@END_DECLARATION_150

	//@START_DECLARATION_170 tagIndicators
	typedef struct tagIndicators
	{
	   bool HVOK;
	   bool AtTemp;
	   bool InkLow;
	   bool HeaterOn;
	}_INDICATORS;
	//@END_DECLARATION_170

	//@START_DECLARATION_205 tagPhotoDemo
	typedef struct tagPhotoDemo
	{
	   HANDLE m_hPhotoEvent;
	   HANDLE m_hEOPEvent;
	   bool m_bEOP;
	}_PHOTODEMO;
	//@END_DECLARATION_205

	//@START_DECLARATION_206 IMAGE_BUFFER_SIZE
	const int IMAGE_BUFFER_SIZE =1024*128;
	//@END_DECLARATION_206

	//@START_DECLARATION_216 tagCtrlReg
	#pragma pack(1)
	struct tagCtrlReg {						// Control Register
		unsigned char _HeadType		: 2;	// 0-Disabled, 1-Slanted, 2 Staggered, 3-Graphic
		unsigned char _SingleBuffer: 1;	// 1-Single Buffer, 0-DoubleBuffers.
		unsigned char _Inverted		: 1;	// 0-Normal, 1-Inverted
		unsigned char _DoublePulse	: 1;	// Enable Double Pulse.
		unsigned char _FireAMS	   : 1;	// Fire AMS Cycle.
		unsigned char _PrintBuffer	: 1;	// 0-Buffer0, 1-Buffer1 // Selects the buffer which the printer uses when we filling the image. 
		unsigned char _PrintEnable	: 1;	// 1-Enable Printing.
	};
	#pragma pack()
	//@END_DECLARATION_216

	//@START_DECLARATION_217 tagInterruptCtrlReg
	#pragma pack(1)
	struct tagInterruptCtrlReg {			// interrupt Control Register
		unsigned char _Channel		: 4;	// Interrupt Channel Number [0 - 7]
		unsigned char _Mode			: 1;	// Interrupt mode	0-OTB, 1-ISA
		unsigned char _ErrDrv		: 3;	// Do not know what this is.
	};
	#pragma pack()
	//@END_DECLARATION_217

	//@START_DECLARATION_218 tagInterfaceCtrlReg
	#pragma pack(1)
	struct tagInterfaceCtrlReg {			// Interface Control Register
		unsigned char _TachSrc		: 2;	// Tach Source; 0-External, 1-internal, 2-OTB Tach 1, 3-OTB Tach2
		unsigned char _OTBTach1		: 1;	// Drive OTB Tach 1
		unsigned char _OTBTach2		: 1;	// Drive OTB Tach 2
		unsigned char _PhotoSrc		: 2;	// Photocell Source. 0-External, 1-Internal, 2-OTB PC1, 3-OTB PC2
		unsigned char _OTBPhoto1	: 1;	// Drive OTB Photocell 1 (PC1)
		unsigned char _OTBPhoto2	: 1;	// Drive OTB Photocell 2 (PC2)
	};
	#pragma pack()
	//@END_DECLARATION_218

	//@START_DECLARATION_221 tagOTBStatusReg
	#pragma pack(1)
	struct tagOTBStatusReg
	{                                      // Over-the-Top-Bus Status Register
		unsigned char _OTBI			: 7;	// OTB Interrupt
		unsigned char _Reserved		: 1;	// Reserved.
	};
	#pragma pack()
	//@END_DECLARATION_221

	//@START_DECLARATION_222 tagInterfaceStatusReg
	#pragma pack(1)
	struct tagInterfaceStatusReg
	{												// Interface Status Register
		unsigned char _OTBIP			: 1;	// Over-the-Top interrupt Pending.
		unsigned char _TachIntr		: 1;	// Tachometer interrupt
		unsigned char _Photocell	: 1;	// Photocell Interrupt.
		unsigned char _HeadError	: 1;	// Print Head Error.
		unsigned char _HVOK			: 1;	// High Voltage Ok
		unsigned char _InkLow		: 1;	// Ink Low
		unsigned char _AtTemp		: 1;	// At Temp.
		unsigned char _HeaterOn		: 1;	// Heater ON
		unsigned char _Reserved		: 8;	// Reserved. (Read as 0 ).
	};
	#pragma pack()
	//@END_DECLARATION_222

	//@START_DECLARATION_223 tagDividerCtrlReg
	#pragma pack(1)
	struct tagDividerCtrlReg
	{												// Divider Control Register
		unsigned char _TachDiv		: 2;	// Reserved
		unsigned char _SClkRate		: 2;	// 3- 7.3728MHz, 2- 3.6864MHz, 1- 1.8432MHz, 0- 921.6kHz
		unsigned char _TackIntRate	: 3;	// Tach Interrupt Rate 16*2^x
		unsigned char _Reserved		: 1;	// Reserved
	};
	#pragma pack()
	//@END_DECLARATION_223

	//@START_DECLARATION_224 tagImgBufBaseAddrReg
	#pragma pack(1)
	struct tagImgBufBaseAddrReg
	{												// Image Buffer Base Address Register
		unsigned char _WindowSize	: 2;	// Window Size: 0- Disabled, 1- 16kB, 2- 32kB, 3- 64kB
		unsigned char _WindowBase	: 6;	// ???
	};
	#pragma pack()
	//@END_DECLARATION_224

	//@START_DECLARATION_225 tagImgLenReg
	#pragma pack(1)
	struct tagImgLenReg
	{												// Image Buffer Offset Register
		unsigned short _Len	: 16;
	};
	#pragma pack()
	//@END_DECLARATION_225

	//@START_DECLARATION_226 tagIntTachGenReg
	#pragma pack(1)
	struct tagIntTachGenReg
	{													// Internal Tach Generator Register
		unsigned short _TachGenFreg	: 16;	// Freguency given by (desired freg / 7.3728) * 2^16
	};
	#pragma pack()
	//@END_DECLARATION_226

	//@START_DECLARATION_227 tagIntPhotoGenReg
	#pragma pack(1)
	struct tagIntPhotoGenReg
	{													// Internal Photocell Generator Count Register
		unsigned short _PhotoGenFreg	: 16;
	};
	#pragma pack()
	//@END_DECLARATION_227

	//@START_DECLARATION_228 tagConveyorSpeed
	#pragma pack(1)
	struct tagConveyorSpeed
	{													// Conveyor Speed Register
		unsigned short _Speed			: 16; // Read as ft/min
	};
	#pragma pack()
	//@END_DECLARATION_228

	//@START_DECLARATION_229 tagFireVibPulse
	#pragma pack(1)
	struct tagFireVibPulse
	{												// Fire / Vibration Pluse Register
		unsigned short _FirePulseDelay			: 16; // 
		unsigned short _FirePulseWidth			: 16; // 
		unsigned short _DampPulseDelay			: 16; // 
		unsigned short _DampPulseWidth			: 16; // 
	// Remaining values are for vibration which are no longer used.
		unsigned short _v5			: 16; // 
		unsigned short _v6			: 16; // 
		unsigned short _v7			: 16; // 
		unsigned short _v8			: 16; // 
	};
	#pragma pack()
	//@END_DECLARATION_229

	//@START_DECLARATION_230 tagPrintDelay
	#pragma pack(1)
	struct tagPrintDelay
	{												// Print Delay Register.
		unsigned short _PrintDelay	: 16; // (Value = Columns)
	};
	#pragma pack()
	//@END_DECLARATION_230

	//@START_DECLARATION_231 tagImageLen
	#pragma pack(1)
	struct tagImageLen
	{												// Image Length Register.
		unsigned short _Len			: 16; // (Value = Columns)
	};
	#pragma pack()
	//@END_DECLARATION_231

	//@START_DECLARATION_232 tagImageStartAddr
	#pragma pack(1)
	struct tagImageStartAddr
	{												// Address of Image Register
		unsigned short _Addr1		: 16; // Address of first image WORD 
		unsigned short _Addr2		: 16; // Address of second image WORD
	};
	#pragma pack()
	//@END_DECLARATION_232

	//@START_DECLARATION_233 tagAddrIncFactor
	#pragma pack(1)
	struct tagAddrIncFactor
	{												// Address Increment Factor Register
		unsigned short _Incr			: 16; // 
	};
	#pragma pack()
	//@END_DECLARATION_233

	//@START_DECLARATION_234 tagColAdjustFactor
	#pragma pack(1)
	struct tagColAdjustFactor
	{												// Column Adjust Factor Register
		unsigned short _Adjust		: 16; // 
	};
	#pragma pack()
	//@END_DECLARATION_234

	//@START_DECLARATION_235 tagProdLenDelay
	#pragma pack(1)
	struct tagProdLenDelay
	{												// Product Length Delay Register
		unsigned short _Delay		: 16; // ( Value = Columns )
	};
	#pragma pack()
	//@END_DECLARATION_235

	//@START_DECLARATION_236 tagRegisters
	// The following structure is read and written directly.  Thus the order
	// of the variables is important and must match what the card will give
	// back.  Think of it as a linear buffer.
	#pragma pack(1)
	typedef struct tagRegisters {
		tagCtrlReg					_CtrlReg;				// Control Register.
		tagInterruptCtrlReg		_Interrupt_Reg;		// interrupt Control Register.
		tagInterfaceCtrlReg		_InterfaceCtrlReg;	// Interface Control Register.
		tagOTBStatusReg			_OTBStatusReg;			// Over-the-Top-Bus Status Register.
		tagInterfaceStatusReg	_InterfaceStatusReg;	// Interface Status Register.
		tagDividerCtrlReg			_DividerCtrlReg;		// Divider Control Register.
		tagImgBufBaseAddrReg		_ImgBufBaseAddr;		// Image Buffer Base Address Register
		tagImgLenReg				_ImgLenReg;				// Image Length Register
		tagIntTachGenReg			_IntTachGenReg;		// Internal Tach Generator Register
		tagIntPhotoGenReg			_IntPhotoGenReg;		// Internal Photocell Generator Count Register
		tagConveyorSpeed			_Conveyor;				// Conveyor Speed Register
		tagFireVibPulse			_FireVibPulse;			// Fire / Vibration Pulse Register
		tagPrintDelay				_PrintDelay;			// Print Delay Register.
		tagImageLen					_ImageLen;				// Image Length Register.
		tagImageStartAddr			_ImageStartAddr;		// Address of Image Register
		tagAddrIncFactor			_AddrIncFactor;		// Address Increment Factor Register
		tagColAdjustFactor		_ColAdjustFactor;		// Column Adjust Factor Register
		tagProdLenDelay			_ProdLenDelay;			// Product Length Delay Register
	}_REGISTERS;
	#pragma pack()
	//@END_DECLARATION_236

	//@START_DECLARATION_346 tagSetupRegs
	// The following structure is read and written directly.  Thus the order
	// of the variables is important and must match what the card will give
	// back.  Think of it as a linear buffer.
	#pragma pack(1)
	typedef struct tagSetupRegs {
		tagCtrlReg					_CtrlReg;				// Control Register.
		tagInterruptCtrlReg		_Interrupt_Reg;		// interrupt Control Register.
		tagInterfaceCtrlReg		_InterfaceCtrlReg;	// Interface Control Register.
		tagOTBStatusReg			_OTBStatusReg;			// Over-the-Top-Bus Status Register.
		tagInterfaceStatusReg	_InterfaceStatusReg;	// Interface Status Register.
		tagDividerCtrlReg			_DividerCtrlReg;		// Divider Control Register.
		tagImgBufBaseAddrReg		_ImgBufBaseAddr;		// Image Buffer Base Address Register
		tagImgLenReg		      _ImgLenReg;				// Image Length Register
		tagIntTachGenReg			_IntTachGenReg;		// Internal Tach Generator Register
		tagIntPhotoGenReg			_IntPhotoGenReg;		// Internal Photocell Generator Count Register
	}_SETUPREGS;
	#pragma pack()
	//@END_DECLARATION_346

	//@START_DECLARATION_352 tagSlantRegs
	// The following structure is read and written directly.  Thus the order
	// of the variables is important and must match what the card will give
	// back.  Think of it as a linear buffer.
	#pragma pack(1)
	typedef struct tagSlantRegs {
		tagImageStartAddr			_ImageStartAddr;		// Address of Image Register
		tagAddrIncFactor			_AddrIncFactor;		// Address Increment Factor Register
		tagColAdjustFactor		_ColAdjustFactor;		// Column Adjust Factor Register
	}_SLANTREGS;
	#pragma pack()
	//@END_DECLARATION_352

	//@START_DECLARATION_369 tagSerialParams
	//Defined in FxMphc Driver
	//@END_DECLARATION_369

	//@START_DECLARATION_383 INTERRUPT_MODES
	// Defined in FxMphc Driver
	//@END_DECLARATION_383

	//@START_DECLARATION_397 tagAMSRegs
	#pragma pack(1)
	struct tagAMSRegs
	{
	   unsigned short A1;	// A1:Number of pulse sets ( purge cycles )
	   unsigned short A2;	// A2:Time from vaccum on until purge cyle start
	   unsigned short A3;	// A3:
	   unsigned short A4;	// A4:
	   unsigned short A5;	// A5:
	   unsigned short A6;	// A6:
	   unsigned short A7;	// A7:
	};
	#pragma pack()
	//@END_DECLARATION_397

	#pragma pack(1)
	struct tagReg8
	{													
		unsigned short data	: 8;
	};
	#pragma pack()

	#pragma pack(1)
	struct tagReg16
	{													
		unsigned short data	: 16;
	};
	#pragma pack()

	#pragma pack(1)
	struct tagPeak603VLWatchdog
	{
		unsigned char _Interval		: 3;	
		unsigned char _IntervalType : 1;	
		unsigned char _WDTTimeOut	: 1;	
		unsigned char _NMIEnable	: 1;	
		unsigned char _ResetEnable	: 1;	
		unsigned char _WDTEnable	: 1;	
	};
	#pragma pack()

	// Needed ClassBuilder include files

	// Make sure the inline implementations are skipped
	#ifdef CB_INLINES
	#undef CB_INLINES
	#endif

	#ifndef __MPHC_NO_HEADER_INCLUDE__

		// Include classes, for declarations
		#include "DeviceList.h"
		#include "Mphc.h"
		#include "DeviceListEntry.h"

	#endif //__MPHC_NO_HEADER_INCLUDE__


	// Include classes again, for inline implementation
	#define CB_INLINES

	//@START_USER2
	//@END_USER2

//};

#endif
