/******************************************************************************\
*
* File:          DeviceList.cpp
* Creation date: November 14, 2003 15:19
*                Foxjet Tech Center
*
* Case Tool:     ClassBuilder 2.4
*                
* Purpose:       Method implementations of class 'CDeviceList'
*
* Modifications: @INSERT_MODIFICATIONS(* )
*
* Copyright 2003, Foxjet Tech Center
* All rights are reserved. Reproduction in whole or part is prohibited
* without the written consent of the copyright owner.
*
\******************************************************************************/
//@START_USER1
//@END_USER1


// Master include file
#include "StdAfx.h"


//@START_USER2
#include <setupapi.h>
#include <regstr.h>
#include "DeviceList.h"
//@END_USER2




/*@NOTE_33
Constructor method.
*/
CDeviceList::CDeviceList(const GUID& guid) //@INIT_33
   : m_list()
   , m_guid(guid)
{//@CODE_33
   ConstructorInclude();

   // Put in your own code
}//@CODE_33


/*@NOTE_28
Destructor method.
*/
CDeviceList::~CDeviceList()
{//@CODE_28
   DestructorInclude();

   // Put in your own code
}//@CODE_28


int CDeviceList::Initialize()
{//@CODE_63
   // Open an enumeration handle so we can locate all devices of our
   // own class
   
   HDEVINFO info = SetupDiGetClassDevs(&m_guid, NULL, NULL, DIGCF_PRESENT | DIGCF_INTERFACEDEVICE);
   if (info == INVALID_HANDLE_VALUE)
      return 0;
   
   // Enumerate all devices of our class. For each one, create a
   // CDeviceEntryList object. Then determine the friendly name of the
   // device by reading the registry.
   
   SP_DEVICE_INTERFACE_DATA ifdata;
   ifdata.cbSize = sizeof(ifdata);
   DWORD devindex;
   for (devindex = 0; SetupDiEnumDeviceInterfaces(info, NULL, &m_guid, devindex, &ifdata); ++devindex)
   {						// for each device
   
      // Determine the symbolic link name for this device instance. Since
      // this is variable in length, make an initial call to determine
      // the required length.
      
      DWORD needed;
      SetupDiGetDeviceInterfaceDetail(info, &ifdata, NULL, 0, &needed, NULL);
      
      PSP_INTERFACE_DEVICE_DETAIL_DATA detail = (PSP_INTERFACE_DEVICE_DETAIL_DATA) malloc(needed);
      SP_DEVINFO_DATA did = {sizeof(SP_DEVINFO_DATA)};
      detail->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);
      if (!SetupDiGetDeviceInterfaceDetail(info, &ifdata, detail, needed, NULL, &did))
      {						// can't get detail info
         free((PVOID) detail);
         continue;
      }						// can't get detail info
      
      // Determine the device's friendly name
      
      TCHAR fname[256];
      if (!SetupDiGetDeviceRegistryProperty(info, &did, SPDRP_FRIENDLYNAME, NULL, (PBYTE) fname, sizeof(fname), NULL)
      && !SetupDiGetDeviceRegistryProperty(info, &did, SPDRP_DEVICEDESC, NULL, (PBYTE) fname, sizeof(fname), NULL))
         _tcsncpy(fname, detail->DevicePath, 256);
        
      CDeviceListEntry e(detail->DevicePath, fname);
      free((PVOID) detail);
      
      m_list.Add(e);
   }						// for each device
   
   SetupDiDestroyDeviceInfoList(info);
   
   // Return the number of entries in the list
   return m_list.GetSize();
}//@CODE_63


//{{AFX DO NOT EDIT CODE BELOW THIS LINE !!!

/*@NOTE_29
Method which must be called first in a constructor.
*/
void CDeviceList::ConstructorInclude()
{
}


/*@NOTE_30
Method which must be called first in a destructor.
*/
void CDeviceList::DestructorInclude()
{
}


// Methods for the relation(s) of the class

//}}AFX DO NOT EDIT CODE ABOVE THIS LINE !!!

//@START_USER3
