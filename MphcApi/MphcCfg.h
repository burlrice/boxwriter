/******************************************************************************\
*
* File:          MphcCfg.h
* Creation date: June 28, 2002 20:23
* Author:        Chris Hodge
*                Foxjet Tech Center
*
* Case Tool:     ClassBuilder 2.3
* Purpose:       Declaration of class 'CMphcCfg'
*
* Modifications: @INSERT_MODIFICATIONS(* )
* June 28, 2002 20:44 Chris Hodge
*     Updated member 'm_Inverted'
*     Updated member 'm_Direction'
*     Updated member 'm_HeadType'
* June 28, 2002 20:42 Chris Hodge
*     Updated member '_Inverted'
*     Updated member '_Direction'
* June 28, 2002 20:41 Chris Hodge
*     Deleted member '_Test'
*     Deleted member '_HeadType'
*     Added member '_Inverted'
*     Added member '_Direction'
*     Added member '_HeadType'
* June 28, 2002 20:34 Chris Hodge
*     Added member '_Test'
* June 28, 2002 20:24 Chris Hodge
*     Added method 'CMphcCfg'
* June 28, 2002 20:23 Chris Hodge
*     Added method 'DestructorInclude'
*     Added method 'ConstructorInclude'
*     Added method '~CMphcCfg'
*     Added member 'm_HeadType'
*
* Copyright 2002, Foxjet Tech Center
* All rights are reserved. Reproduction in whole or part is prohibited
* without the written consent of the copyright owner.
*
\******************************************************************************/
#ifndef _ISA_MPHCCFG_H
#define _ISA_MPHCCFG_H

//@START_USER1
//@END_USER1


//namespace ISA {

	class AFX_EXT_CLASS CMphcCfg
	{

	//@START_USER2
	//@END_USER2

	// Members
	private:

	protected:

	public:

	// Methods
	private:
	   void ConstructorInclude();
	   void DestructorInclude();

	protected:

	public:
	   CMphcCfg();
	   virtual ~CMphcCfg();
	};
//};

#endif


#ifdef CB_INLINES
#ifndef _MPHCCFG_H_INLINES
#define _MPHCCFG_H_INLINES

//@START_USER3
//@END_USER3

#endif
#endif
