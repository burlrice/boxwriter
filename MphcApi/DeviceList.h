/******************************************************************************\
*
* File:          DeviceList.h
* Creation date: November 14, 2003 15:19
*                Foxjet Tech Center
*
* Case Tool:     ClassBuilder 2.3
* Purpose:       Declaration of class 'CDeviceList'
*
* Modifications: @INSERT_MODIFICATIONS(* )
*
* Copyright 2003, Foxjet Tech Center
* All rights are reserved. Reproduction in whole or part is prohibited
* without the written consent of the copyright owner.
*
\******************************************************************************/
#ifndef _ISA_DEVICELIST_H
#define _ISA_DEVICELIST_H

//@START_USER1
#include <afxtempl.h>
#include "DeviceListEntry.h"
//@END_USER1

namespace ISA 
{
	class AFX_EXT_CLASS CDeviceList
	{

	//@START_USER2
	//@END_USER2

	//
	// Group: ClassBuilder methods
	//

	private:
	   void ConstructorInclude();
	   void DestructorInclude();

	//
	// Non-Grouped Members
	//

	private:
	   GUID m_guid;

	protected:
	   CArray<CDeviceListEntry,CDeviceListEntry > m_list;

	//
	// Non-Grouped Methods
	//

	public:
	   CDeviceList(const GUID& guid);
	   virtual ~CDeviceList();
	   int Initialize();
	};
};

#endif


#ifdef CB_INLINES
#ifndef _DEVICELIST_H_INLINES
#define _DEVICELIST_H_INLINES

//@START_USER3
//@END_USER3

#endif
#endif
