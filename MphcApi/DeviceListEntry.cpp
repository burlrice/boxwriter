/******************************************************************************\
*
* File:          DeviceListEntry.cpp
* Creation date: November 14, 2003 15:19
*                Foxjet Tech Center
*
* Case Tool:     ClassBuilder 2.4
*                
* Purpose:       Method implementations of class 'CDeviceListEntry'
*
* Modifications: @INSERT_MODIFICATIONS(* )
*
* Copyright 2003, Foxjet Tech Center
* All rights are reserved. Reproduction in whole or part is prohibited
* without the written consent of the copyright owner.
*
\******************************************************************************/
//@START_USER1
//@END_USER1


// Master include file
#include "StdAfx.h"


//@START_USER2
#include "DeviceListEntry.h"
//@END_USER2





//
// Group: ClassBuilder methods
//
/*@NOTE_40
Constructor method.
*/
CDeviceListEntry::CDeviceListEntry(CString linkname,
                                   CString friendlyname) //@INIT_40
   : m_linkname(linkname)
   , m_friendlyname(friendlyname)
{//@CODE_40
   ConstructorInclude();

  
}//@CODE_40


/*@NOTE_79
Constructor method.
*/
CDeviceListEntry::CDeviceListEntry() //@INIT_79
{//@CODE_79
   ConstructorInclude();

 
}//@CODE_79


// End of Group: ClassBuilder methods

/*@NOTE_36
Destructor method.
*/
CDeviceListEntry::~CDeviceListEntry()
{//@CODE_36
   DestructorInclude();

   // Put in your own code
}//@CODE_36


//{{AFX DO NOT EDIT CODE BELOW THIS LINE !!!

/*@NOTE_37
Method which must be called first in a constructor.
*/
void CDeviceListEntry::ConstructorInclude()
{
}


/*@NOTE_38
Method which must be called first in a destructor.
*/
void CDeviceListEntry::DestructorInclude()
{
}


// Methods for the relation(s) of the class

//}}AFX DO NOT EDIT CODE ABOVE THIS LINE !!!

//@START_USER3
