// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "Image.h"
#include <afxinet.h>
#include <Winsock2.h>
#include "MainFrm.h"
#include "Debug.h"
#include "ximage.h"
#include "fj_image.h"
#include "IpBaseElement.h"
#include "fj_socket.h"

#define REVERSEWORD32(word) ( ((word&0x000000ff)<<24) | ((word&0x0000ff00)<<8) | ((word&0x00ff0000)>>8) | ((word&0xff000000)>>24) )
#define REVERSEWORD16(word) ( ((word&0x000000ff)<<8) | ((word&0x0000ff00)>>8) )

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CImageApp theApp;


struct 
{
	DWORD	m_dw;
	TCHAR * m_lpsz;
} static const map [] =
{
	{ IPC_COMPLETE,						_T ("IPC_COMPLETE"),					},
	{ IPC_CONNECTION_CLOSE,				_T ("IPC_CONNECTION_CLOSE"),			},
	{ IPC_INVALID,						_T ("IPC_INVALID"),						},
	{ IPC_ECHO,							_T ("IPC_ECHO"),						},
	{ IPC_STATUS,						_T ("IPC_STATUS"),						},
	{ IPC_GET_STATUS,					_T ("IPC_GET_STATUS"),					},
	{ IPC_PHOTO_TRIGGER,				_T ("IPC_PHOTO_TRIGGER"),				},
	{ IPC_EDIT_STATUS,					_T ("IPC_EDIT_STATUS"),					},
	{ IPC_GET_EDIT_STATUS,				_T ("IPC_GET_EDIT_STATUS"),				},
	{ IPC_EDIT_START,					_T ("IPC_EDIT_START"),					},
	{ IPC_EDIT_SAVE,					_T ("IPC_EDIT_SAVE"),					},
	{ IPC_EDIT_CANCEL,					_T ("IPC_EDIT_CANCEL"),					},
	{ IPC_IMAGE_HEADER,					_T ("IPC_IMAGE_HEADER"),				},
	{ IPC_IMAGE_DATA,					_T ("IPC_IMAGE_DATA"),					},
	{ IPC_GET_LABEL_IDS,				_T ("IPC_GET_LABEL_IDS"),				},			
	{ IPC_SET_LABEL_IDS,				_T ("IPC_SET_LABEL_IDS"),				},			
	{ IPC_GET_LABELS,					_T ("IPC_GET_LABELS"),					},			
	{ IPC_GET_LABEL,					_T ("IPC_GET_LABEL"),					},			
	{ IPC_PUT_LABEL,					_T ("IPC_PUT_LABEL"),					},			
	{ IPC_DELETE_LABEL,					_T ("IPC_DELETE_LABEL"),				},			
	{ IPC_SELECT_LABEL,					_T ("IPC_SELECT_LABEL"),				},			
	{ IPC_GET_LABEL_ID_SELECTED,		_T ("IPC_GET_LABEL_ID_SELECTED"),		},			
	{ IPC_SET_LABEL_ID_SELECTED,		_T ("IPC_SET_LABEL_ID_SELECTED"),		},			
	{ IPC_DELETE_ALL_LABELS,			_T ("IPC_DELETE_ALL_LABELS"),			},			
	{ IPC_GET_MSG_IDS,					_T ("IPC_GET_MSG_IDS"),					},			
	{ IPC_SET_MSG_IDS,					_T ("IPC_SET_MSG_IDS"),					},			
	{ IPC_GET_MSGS,						_T ("IPC_GET_MSGS"),					},			
	{ IPC_GET_MSG,						_T ("IPC_GET_MSG"),						},			
	{ IPC_PUT_MSG,						_T ("IPC_PUT_MSG"),						},			
	{ IPC_DELETE_MSG,					_T ("IPC_DELETE_MSG"),					},			
	{ IPC_GET_TEXT_IDS,					_T ("IPC_GET_TEXT_IDS"),				},			
	{ IPC_SET_TEXT_IDS,					_T ("IPC_SET_TEXT_IDS"),				},			
	{ IPC_GET_TEXT_ELEMENTS,			_T ("IPC_GET_TEXT_ELEMENTS"),			},			
	{ IPC_GET_TEXT,						_T ("IPC_GET_TEXT"),					},			
	{ IPC_PUT_TEXT,						_T ("IPC_PUT_TEXT"),					},			
	{ IPC_DELETE_TEXT,					_T ("IPC_DELETE_TEXT"),					},			
	{ IPC_GET_BITMAP_IDS,				_T ("IPC_GET_BITMAP_IDS"),				},			
	{ IPC_SET_BITMAP_IDS,				_T ("IPC_SET_BITMAP_IDS"),				},			
	{ IPC_GET_BITMAP_ELEMENTS,			_T ("IPC_GET_BITMAP_ELEMENTS"),			},			
	{ IPC_GET_BITMAP,					_T ("IPC_GET_BITMAP"),					},			
	{ IPC_PUT_BITMAP,					_T ("IPC_PUT_BITMAP"),					},			
	{ IPC_DELETE_BITMAP,				_T ("IPC_DELETE_BITMAP"),				},			
	{ IPC_GET_DYNIMAGE_IDS,				_T ("IPC_GET_DYNIMAGE_IDS"),			},			
	{ IPC_SET_DYNIMAGE_IDS,				_T ("IPC_SET_DYNIMAGE_IDS"),			},			
	{ IPC_GET_DYNIMAGE_ELEMENTS,		_T ("IPC_GET_DYNIMAGE_ELEMENTS"),		},			
	{ IPC_GET_DYNIMAGE,					_T ("IPC_GET_DYNIMAGE"),				},			
	{ IPC_PUT_DYNIMAGE,					_T ("IPC_PUT_DYNIMAGE"),				},			
	{ IPC_DELETE_DYNIMAGE,				_T ("IPC_DELETE_DYNIMAGE"),				},			
	{ IPC_PUT_DYNIMAGE_DATA,			_T ("IPC_PUT_DYNIMAGE_DATA"),			},			
	{ IPC_GET_DATETIME_IDS,				_T ("IPC_GET_DATETIME_IDS"),			},			
	{ IPC_SET_DATETIME_IDS,				_T ("IPC_SET_DATETIME_IDS"),			},			
	{ IPC_GET_DATETIME_ELEMENTS,		_T ("IPC_GET_DATETIME_ELEMENTS"),		},			
	{ IPC_GET_DATETIME,					_T ("IPC_GET_DATETIME"),				},			
	{ IPC_PUT_DATETIME,					_T ("IPC_PUT_DATETIME"),				},			 
	{ IPC_DELETE_DATETIME,				_T ("IPC_DELETE_DATETIME"),				},			
	{ IPC_GET_COUNTER_IDS,				_T ("IPC_GET_COUNTER_IDS"),				},			
	{ IPC_SET_COUNTER_IDS,				_T ("IPC_SET_COUNTER_IDS"),				},			
	{ IPC_GET_COUNTER_ELEMENTS,			_T ("IPC_GET_COUNTER_ELEMENTS"),		},			
	{ IPC_GET_COUNTER,					_T ("IPC_GET_COUNTER"),					},			
	{ IPC_PUT_COUNTER,					_T ("IPC_PUT_COUNTER"),					},			
	{ IPC_DELETE_COUNTER,				_T ("IPC_DELETE_COUNTER"),				},			
	{ IPC_GET_BARCODE_IDS,				_T ("IPC_GET_BARCODE_IDS"),				},			
	{ IPC_SET_BARCODE_IDS,				_T ("IPC_SET_BARCODE_IDS"),				},			
	{ IPC_GET_BARCODE_ELEMENTS,			_T ("IPC_GET_BARCODE_ELEMENTS"),		},			
	{ IPC_GET_BARCODE,					_T ("IPC_GET_BARCODE"),					},			
	{ IPC_PUT_BARCODE,					_T ("IPC_PUT_BARCODE"),					},			
	{ IPC_DELETE_BARCODE,				_T ("IPC_DELETE_BARCODE"),				},			
	{ IPC_GET_FONT_IDS,					_T ("IPC_GET_FONT_IDS"),				},			
	{ IPC_SET_FONT_IDS,					_T ("IPC_SET_FONT_IDS"),				},			
	{ IPC_GET_FONTS,					_T ("IPC_GET_FONTS"),					},			
	{ IPC_GET_FONT,						_T ("IPC_GET_FONT"),					},			
	{ IPC_PUT_FONT,						_T ("IPC_PUT_FONT"),					},			
	{ IPC_DELETE_FONT,					_T ("IPC_DELETE_FONT"),					},			
	{ IPC_GET_BMP_IDS,					_T ("IPC_GET_BMP_IDS"),					},			
	{ IPC_SET_BMP_IDS,					_T ("IPC_SET_BMP_IDS"),					},			
	{ IPC_GET_BMPS,						_T ("IPC_GET_BMPS"),					},			
	{ IPC_GET_BMP,						_T ("IPC_GET_BMP"),						},			
	{ IPC_PUT_BMP,						_T ("IPC_PUT_BMP"),						},			
	{ IPC_DELETE_BMP,					_T ("IPC_DELETE_BMP"),					},			
	{ IPC_DELETE_ALL_BMPS,				_T ("IPC_DELETE_ALL_BMPS"),				},			
	{ IPC_GET_DYNTEXT_IDS,				_T ("IPC_GET_DYNTEXT_IDS"),				},			
	{ IPC_SET_DYNTEXT_IDS,				_T ("IPC_SET_DYNTEXT_IDS"),				},			
	{ IPC_GET_DYNTEXT_ELEMENTS,			_T ("IPC_GET_DYNTEXT_ELEMENTS"),		},			
	{ IPC_GET_DYNTEXT,					_T ("IPC_GET_DYNTEXT"),					},			
	{ IPC_PUT_DYNTEXT,					_T ("IPC_PUT_DYNTEXT"),					},			
	{ IPC_DELETE_DYNTEXT,				_T ("IPC_DELETE_DYNTEXT"),				},			
	{ IPC_GET_DYNBARCODE_IDS,			_T ("IPC_GET_DYNBARCODE_IDS"),			},			
	{ IPC_SET_DYNBARCODE_IDS,			_T ("IPC_SET_DYNBARCODE_IDS"),			},			
	{ IPC_GET_DYNBARCODE_ELEMENTS,		_T ("IPC_GET_DYNBARCODE_ELEMENTS"),		},			
	{ IPC_GET_DYNBARCODE,				_T ("IPC_GET_DYNBARCODE"),				},			
	{ IPC_PUT_DYNBARCODE,				_T ("IPC_PUT_DYNBARCODE"),				},			
	{ IPC_DELETE_DYNBARCODE,			_T ("IPC_DELETE_DYNBARCODE"),			},			
	{ IPC_GET_GROUP_INFO,				_T ("IPC_GET_GROUP_INFO"),				},			
	{ IPC_SET_GROUP_INFO,				_T ("IPC_SET_GROUP_INFO"),				},			
	{ IPC_GET_SYSTEM_INFO,				_T ("IPC_GET_SYSTEM_INFO"),				},			
	{ IPC_GET_SYSTEM_INTERNET_INFO,		_T ("IPC_GET_SYSTEM_INTERNET_INFO"),	},			
	{ IPC_GET_SYSTEM_TIME_INFO,			_T ("IPC_GET_SYSTEM_TIME_INFO"),		},			
	{ IPC_GET_SYSTEM_DATE_STRINGS,		_T ("IPC_GET_SYSTEM_DATE_STRINGS"),		},			
	{ IPC_GET_SYSTEM_PASSWORDS,			_T ("IPC_GET_SYSTEM_PASSWORDS"),		},			
	{ IPC_GET_SYSTEM_BARCODES,			_T ("IPC_GET_SYSTEM_BARCODES"),			},			
	{ IPC_SET_SYSTEM_INFO,				_T ("IPC_SET_SYSTEM_INFO"),				},			
	{ IPC_GET_PRINTER_ID,				_T ("IPC_GET_PRINTER_ID"),				},			
	{ IPC_GET_PRINTER_INFO,				_T ("IPC_GET_PRINTER_INFO"),			},			
	{ IPC_GET_PRINTER_PKG_INFO,			_T ("IPC_GET_PRINTER_PKG_INFO"),		},			
	{ IPC_GET_PRINTER_PHY_INFO,			_T ("IPC_GET_PRINTER_PHY_INFO"),		},			
	{ IPC_GET_PRINTER_OP_INFO,			_T ("IPC_GET_PRINTER_OP_INFO"),			},			
	{ IPC_SET_PRINTER_INFO,				_T ("IPC_SET_PRINTER_INFO"),			},			
	{ IPC_GET_PRINTER_ELEMENTS,			_T ("IPC_GET_PRINTER_ELEMENTS"),		},			
	{ IPC_PUT_PRINTER_ELEMENT,			_T ("IPC_PUT_PRINTER_ELEMENT"),			},			
	{ IPC_DELETE_ALL_PRINTER_ELEMENTS,	_T ("IPC_DELETE_ALL_PRINTER_ELEMENTS"),	},			
	{ IPC_FIRMWARE_UPGRADE,				_T ("IPC_FIRMWARE_UPGRADE"),			},			
	{ IPC_SET_PH_TYPE,					_T ("IPC_SET_PH_TYPE"),					},			
	{ IPC_SET_PRINTER_MODE,				_T ("IPC_SET_PRINTER_MODE"),			},			
	{ IPC_GET_PRINTER_MODE,				_T ("IPC_GET_PRINTER_MODE"),			},			
	{ IPC_SET_SELECTED_COUNT,			_T ("IPC_SET_SELECTED_COUNT"),			},			
	{ IPC_GET_SELECTED_COUNT,			_T ("IPC_GET_SELECTED_COUNT"),			},			
	{ IPC_SELECT_VARDATA_ID,			_T ("IPC_SELECT_VARDATA_ID"),			},			
	{ IPC_GET_VARDATA_ID,				_T ("IPC_GET_VARDATA_ID"),				},			
	{ IPC_SET_VARDATA_ID,				_T ("IPC_SET_VARDATA_ID"),				},			
	{ IPC_SAVE_ALL_PARAMS,				_T ("IPC_SAVE_ALL_PARAMS"),				},			
	{ IPC_GET_SW_VER_INFO,				_T ("IPC_GET_SW_VER_INFO"),				},			
	{ IPC_GET_GA_VER_INFO,				_T ("IPC_GET_GA_VER_INFO"),				},
	{ IPC_GA_UPGRADE,					_T ("IPC_GA_UPGRADE"),					},
	{ IPC_SET_STATUS_MODE,				_T ("IPC_SET_STATUS_MODE"),				},
	{ IPC_GET_STATUS_MODE,				_T ("IPC_GET_STATUS_MODE"),				},
	{ IPC_GET_DISK_USAGE,				_T ("IPC_GET_DISK_USAGE"),					},			
	{ IPC_GET_PH_TYPE,					_T ("IPC_GET_PH_TYPE"),					},			
};

TCHAR * GetIpcCmdName (DWORD dw)
{
	static TCHAR * lpszDef = _T ("[not found]");

	for (int i = 0; i < ARRAYSIZE (::map); i++)
		if (::map [i].m_dw == dw)
			return ::map [i].m_lpsz;

	return lpszDef;
}

DWORD GetIpcCmd (const TCHAR * lpsz)
{
	for (int i = 0; i < ARRAYSIZE (::map); i++)
		if (!wcscmp (lpsz, ::map [i].m_lpsz))
			return ::map [i].m_dw;

	return map [0].m_dw;
}

#define REVERSEWORD32(word) ( ((word&0x000000ff)<<24) | ((word&0x0000ff00)<<8) | ((word&0x00ff0000)>>8) | ((word&0xff000000)>>24) )
#define REVERSEWORD16(word) ( ((word&0x000000ff)<<8) | ((word&0x0000ff00)>>8) )
#define LOADREVERSEWORD32(pointer) ( (((*(((BYTE*)pointer))))) | (((*(((BYTE*)pointer)+1)))<<8) | (((*(((BYTE*)pointer)+2)))<<16) | (((*(((BYTE*)pointer)+3)))<<24) )
#define LOADREVERSEWORD16(pointer) ( (((*(((BYTE*)pointer))))) | (((*(((BYTE*)pointer)+1)))<<8) )

bool fj_bmpCheckFormat( LPBYTE pbmp )
{
	BITMAPFILEHEADER *pbmfh;
	BITMAPINFOHEADER *pbmih;
	LONG lLength;						// size of buffer in bytes
	LONG lOffset;						// bmp offset to rows of bits
	LONG lHeight;						// bmp height in bits
	LONG lWidth;						// bmp width in bits
	LONG lHeightBytes;					// bmp height in bytes
	LONG lWidthBytes;					// bmp width in bytes
	LONG lLengthCalc;					// calculated length in bytes
	bool bRet = false;

	if ( NULL != pbmp )
	{
		pbmfh = (BITMAPFILEHEADER *)pbmp;
		pbmih = (BITMAPINFOHEADER *)(pbmp+sizeof(BITMAPFILEHEADER));
										// bmp magic number
		if ( ('B' == *pbmp) && ('M' == *(pbmp+1)) )
		{
			lLength = LOADREVERSEWORD32(&pbmfh->bfSize);
			lOffset = LOADREVERSEWORD32(&pbmfh->bfOffBits);
			// Windows bmp header length
			if ( sizeof(BITMAPINFOHEADER) == LOADREVERSEWORD32(&pbmih->biSize) )
			{
				if ( (1 == LOADREVERSEWORD16(&pbmih->biPlanes)) && (1 == LOADREVERSEWORD16(&pbmih->biBitCount)) && (0 == LOADREVERSEWORD32(&pbmih->biCompression)) )
				{
					lHeight = LOADREVERSEWORD32(&pbmih->biHeight);
					lWidth  = LOADREVERSEWORD32(&pbmih->biWidth);
					lHeightBytes =  (lHeight+ 7)/8;
					lWidthBytes  = ((lWidth +31)/32)*4;
					lLengthCalc = lOffset + (lHeight * lWidthBytes);
										// header plus rowdata must equal size

					if ( lLength == lLengthCalc )
					{
						bRet = true;
					}
					// actual size might be rounded up to a multiple of 4
					else if ( (lLength > lLengthCalc) && (lLength <= (lLengthCalc+4)))
					{
						bRet = true;
					}
					else 
						TRACEF (_T ("fj_bmpCheckFormat: FAILED"));
				}
				else
					TRACEF (_T ("fj_bmpCheckFormat: FAILED: pbmih->biPlanes or pbmih->biBitCount or pbmih->biCompression"));
			}
			else
				TRACEF (_T ("fj_bmpCheckFormat: sizeof(BITMAPINFOHEADER) != LOADREVERSEWORD32(&pbmih->biSize)"));
		}
		else
			TRACEF (_T ("fj_bmpCheckFormat: 'BM' failed"));
	}
	else
		TRACEF (_T ("fj_bmpCheckFormat: pbmp == NULL"));

	return( bRet );
}
/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_COMMAND(IDM_GET_IMAGE, OnGetImage)
	ON_COMMAND(IDM_PUT_IMAGE, OnPutImage)
	ON_COMMAND(ID_BMP_DELETE, OnBmpDelete)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	// create a view to occupy the client area of the frame
	if (!m_wndView.Create(NULL, NULL, AFX_WS_DEFAULT_VIEW,
		CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
	{
		TRACE0("Failed to create view window\n");
		return -1;
	}
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	WSADATA wsaData;

	if (::WSAStartup (MAKEWORD (2,1), &wsaData) != 0) {
		TRACEF (_T ("WSAStartup failed: ") + FormatMessage (WSAGetLastError ()));
		ASSERT (0);
	}

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.lpszClass = AfxRegisterWndClass(0);
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
void CMainFrame::OnSetFocus(CWnd* pOldWnd)
{
	// forward focus to the view window
	m_wndView.SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	// let the view have first crack at the command
	if (m_wndView.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	// otherwise, do default handling
	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void CMainFrame::GetImage (CFtpConnection * pFTP, const CString & strRemote, const CString & strLocal)
{
	bool bResult = false;

	if (::GetFileAttributes (strLocal) != -1)
		VERIFY (::DeleteFile (strLocal));

	if (pFTP->GetFile (strRemote, strLocal, FALSE, FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_BINARY)) {

		TRACEF (strRemote);
		TRACEF (strLocal);

		try
		{
			CFile f;
			CBitmap bmp;

			f.Open (strLocal, CFile::modeRead | CFile::shareDenyWrite);
			
			ULONG lLen = f.GetLength ();
			PUCHAR pData = new UCHAR [lLen];
			
			f.Read (pData, lLen);
			f.Close();

			LPFJIMAGE pfi = (LPFJIMAGE)pData;

			pfi->lHeight		= REVERSEWORD32 (pfi->lHeight);
			pfi->lLength		= REVERSEWORD32 (pfi->lLength);
			pfi->lTransforms	= REVERSEWORD32 (pfi->lTransforms);
			pfi->lBits			= REVERSEWORD32 (pfi->lBits);
			pfi->lUseCounter	= REVERSEWORD32 (pfi->lUseCounter);
			pfi->lShifted		= REVERSEWORD32 (pfi->lShifted);

			const PUCHAR pSrcBuffer = (LPBYTE)pfi + sizeof(FJIMAGE);
			CSize size (0, 0);
			LPBYTE pDestBuffer = FoxjetIpElements::CIpBaseElement::ImageIpToWinRotate (pfi, pSrcBuffer, size);

			if (bmp.CreateBitmap (size.cx, size.cy, 1, 1, pDestBuffer)) {
				CString str = strLocal;
				CxImage img;

				img.CreateFromHBITMAP (bmp);
				img.Flip ();

				::DeleteFile (str);
				str.Replace (_T (".FJIMAGE"), _T (".bmp"));
				SAVEBITMAP (img.MakeBitmap (), str);
				bResult = true;
			}

			delete [] pData;
		}
		catch ( CFileException *e )  { HANDLEEXCEPTION (e); }
	}
	else
		TRACEF (FORMATMESSAGE (::GetLastError ()));

	if (!bResult)
		TRACEF (_T ("GetImage failed: ") + strRemote);
}

void CMainFrame::OnGetImage() 
{
	const CString strServer = _T ("192.168.2.163");
	const CString strUser	= _T ("root");
	const CString strPass	= _T ("password");
	CInternetSession session (_T ("Debug image download"));

	if (CFtpConnection * pFTP = session.GetFtpConnection (strServer, strUser, strPass)) {
		GetImage (pFTP, _T ("pfe.FJIMAGE"), _T ("C:\\Temp\\Debug\\pfe.FJIMAGE"));
		GetImage (pFTP, _T ("txImage.FJIMAGE"), _T ("C:\\Temp\\Debug\\txImage.FJIMAGE"));

		pFTP->Close ();

		delete pFTP;
	}
	else {
		CString str;

		str.Format (_T ("FTP failed: %s"), strServer);
		AfxMessageBox (str, MB_OK | MB_ICONERROR);
	}
}

static const DWORD dwSleep = 25;//250;

char * w2a (const TCHAR * w, int nLenW, char * a, int nLenA)
{
	for (int i = 0; i < nLenW; i++) {
		TCHAR c = w [i];

		if (i < nLenA)
			a [i] = (char)(c & 0xFF);
	}

	return a;
}

TCHAR * a2w (const char * a, int nLenA, TCHAR * w, int nLenW)
{
	for (int i = 0; i < nLenA; i++) {
		char c = a [i];

		if (i < nLenW)
			w [i] = c;
	}

	return w;
}

void ntohl (_FJ_SOCKET_MESSAGE & cmd)
{
	cmd.Header.Cmd		= ntohl (cmd.Header.Cmd);
	cmd.Header.BatchID	= ntohl (cmd.Header.BatchID);
	cmd.Header.Length	= ntohl (cmd.Header.Length);
}

bool IsSocketDataPending (SOCKET s)
{
	struct timeval timeout;
	fd_set read;

	memset (&read, 0, sizeof (read));
	read.fd_count = 1;
	read.fd_array [0] = s;

	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

	int nSelect = ::select (0, &read, NULL, NULL, &timeout);

	return (nSelect != SOCKET_ERROR && nSelect > 0) ? true : false;
}

bool SendBinary (SOCKET s, ULONG lBatchID, ULONG lCmd, LPBYTE lpData, ULONG lLen, _FJ_SOCKET_MESSAGE * pReply = NULL)
{
	bool bSent = false;
	const DWORD dwTimeout = 1000;
	const double dTimeout = (double)dwTimeout / 1000.0;
	struct
	{
		ULONG m_lCmd;
		ULONG m_lReply;
	}
	static const synonym [] = 
	{
		{ IPC_EDIT_START,			IPC_EDIT_STATUS },
		{ IPC_EDIT_SAVE,			IPC_EDIT_STATUS },
		{ IPC_GET_SELECTED_COUNT,	IPC_STATUS		},
	};

	if (s != INVALID_SOCKET) {
		DWORD dwLen = sizeof (_FJ_SOCKET_MESSAGE_HEADER) + lLen;

		if (HGLOBAL hData = GlobalAlloc (GPTR, dwLen)) {
			if (_FJ_SOCKET_MESSAGE * p = (_FJ_SOCKET_MESSAGE *)GlobalLock (hData)) {
				memset (p, 0, dwLen);
				p->Header.Cmd		= htonl (lCmd);
				p->Header.BatchID	= htonl (lBatchID);
				p->Header.Length	= htonl (lLen);
				memcpy (p->Data, lpData, lLen);

				while (IsSocketDataPending (s)) { // clear any pending data
					_FJ_SOCKET_MESSAGE reply;

					memset (&reply, 0, sizeof (reply));
					recv (s, (char *)&reply, sizeof (reply), 0);
					ntohl (reply);
					//DebugMsg (DLLTEXT ("recv: %d"), nRecv);
					//DEBUGIPC (reply, _T ("              "));
				}

				DWORD dwSend = send (s, (const char *)p, dwLen, 0);
				bSent = dwSend == dwLen ? true : false;
				//DebugMsg (DLLTEXT ("send                 [%-03d, %-05d, %-06d] (%s)%s"), lCmd, lBatchID, lLen, GetIpcCmdName (lCmd), bSent ? _T ("") : _T (" FAILED"));

				if (pReply) { 
					clock_t clock_tStart = clock ();
					double dDiff = 0;

					do {
						dDiff = (double)(clock () - clock_tStart) / CLOCKS_PER_SEC;

						if (IsSocketDataPending (s)) {
							memset (pReply, 0, sizeof (_FJ_SOCKET_MESSAGE));
							::Sleep (::dwSleep);

							if (recv (s, (char *)&pReply->Header, sizeof (pReply->Header), 0) == sizeof (_FJ_SOCKET_MESSAGE_HEADER)) {
								ntohl (* pReply);

								bool bMatched = pReply->Header.BatchID == lBatchID ? true : false;

								for (int i = 0; !bMatched && (i < ARRAYSIZE (synonym)); i++) 
									if (lCmd == synonym [i].m_lCmd) 
										bMatched = (pReply->Header.Cmd == synonym [i].m_lReply) ? true : false;

								if (pReply->Header.Length) {
									::recv (s, (char *)&pReply->Data, pReply->Header.Length, 0);

									//if (bMatched) 
										//DEBUGIPC (* pReply, bMatched ? _T ("[MATCHED    ] ") : _T ("[not matched] "));
								}

								if (bMatched) 
									return bSent;
							}
						}
					}
					while (dDiff < dTimeout);

					if (dDiff >= dTimeout) 
						TRACEF ("Send: timed out waiting for reply"); // [%.03fs (%.03fs)]"), dTimeout, dDiff);
				}
			}

			GlobalUnlock (hData);
			GlobalFree (hData);
		}
	}
	
	return bSent;
}

bool Send (SOCKET s, ULONG lBatchID, ULONG lCmd, TCHAR * lpsz, _FJ_SOCKET_MESSAGE * pReply = NULL, TCHAR * lpszReply = NULL, ULONG lReplyLen = 0)
{
	char sz [_FJ_SOCKET_BUFFER_SIZE] = { 0 };
	const ULONG lLen = lpsz == NULL ? 0 : wcslen (lpsz);

	if ((!pReply && lpszReply) || (!pReply && lReplyLen))
		TRACEF ("can't fill in lpszReply without pReply");

	if (lpsz && lLen) 
		w2a (lpsz, lLen, sz, ARRAYSIZE (sz));

	if (SendBinary (s, lBatchID, lCmd, (LPBYTE)sz, lLen, pReply)) {
		TRACEF (CString (_T ("Send:   ")) + GetIpcCmdName (lCmd) + CString (_T (": ")) + lpsz);
		TRACEF (sz);

		if (pReply && lpszReply && lReplyLen) {
			a2w ((char *)pReply->Data, pReply->Header.Length, lpszReply, lReplyLen);
			TRACEF (CString (_T ("[reply] ")) + GetIpcCmdName (pReply->Header.Cmd) + CString (_T (": ")) + lpszReply);
		}

		return true;
	}

	return false;
}

void CMainFrame::OnPutImage() 
{
	CString strFile = theApp.GetProfileString (_T ("file"), _T ("upload"), _T ("C:\\Temp\\Debug\\192.168.0.163_slice.bmp"));
	CFileDialog dlg (TRUE, _T ("*.bmp"), strFile, OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, _T ("Bitmaps (*.bmp)|*.bmp||"), this);
	bool bPut = true; //dlg.DoModal () == IDOK ? true : false;

	if (bPut) {
		ULONG lBatchID = 1;
		SOCKADDR_IN target;
		char szPort [32] = { 0 };
		strFile = dlg.GetPathName ();

		theApp.WriteProfileString (_T ("file"), _T ("upload"), strFile);

		target.sin_family = AF_INET; 
		target.sin_port = htons (1281); 
		target.sin_addr.s_addr = inet_addr ("192.168.2.163");

		SOCKET s = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP); 

		if (connect(s, (SOCKADDR *)&target, sizeof(target)) != SOCKET_ERROR) {
			try 
			{
				CFile f;

				if (f.Open (strFile, CFile::modeRead | CFile::shareDenyWrite)) {
					DWORD dwLen = f.GetLength ();
					char sz [128] = { 0 };

					sprintf (sz, "{bmp,\"PrintDriver\",%lu}", dwLen);
					DWORD dwStrLen = strlen (sz);
					DWORD dwTotalLen = dwLen + dwStrLen + 1;
					LPBYTE lpData = new BYTE [dwTotalLen];

					memset (lpData, NULL, dwTotalLen);
					memcpy (lpData, sz, dwStrLen);
					DWORD dwRead = f.Read ((lpData + dwStrLen + 1), dwLen);
					f.Close();

					if (!fj_bmpCheckFormat (lpData + dwStrLen + 1)) 
						TRACEF (_T ("fj_bmpCheckFormat FAILED"));

					Send (s, lBatchID++, IPC_GET_SW_VER_INFO,		_T ("PRINT_DRIVER=T;PRINT_DRIVER_DEBUG=T;"));
					Send (s, lBatchID++, IPC_SET_PRINTER_MODE,		_T ("PRINT_DRIVER_COUNT=1;"));
					SendBinary (s, lBatchID++, IPC_PUT_BMP_DRIVER, lpData, dwTotalLen);				TRACEF (sz);

					delete [] lpData;
				}
			}
			catch ( CMemoryException *e) { HANDLEEXCEPTION (e); }
			catch ( CFileException *e) { HANDLEEXCEPTION (e);  }

			closesocket (s);
		}
	}
}

void CMainFrame::OnBmpDelete() 
{
	const CString strServer = _T ("192.168.2.163");
	const CString strUser	= _T ("root");
	const CString strPass	= _T ("password");
	CInternetSession session (_T ("Debug image download"));

	if (CFtpConnection * pFTP = session.GetFtpConnection (strServer, strUser, strPass)) {
		pFTP->Remove (_T ("pfe.FJIMAGE"));
		pFTP->Remove (_T ("txImage.FJIMAGE"));

		::DeleteFile (_T ("C:\\Temp\\Debug\\pfe.bmp"));
		::DeleteFile (_T ("C:\\Temp\\Debug\\txImage.bmp"));
		::DeleteFile (_T ("C:\\Temp\\Debug\\pfe.FJIMAGE"));
		::DeleteFile (_T ("C:\\Temp\\Debug\\txImage.FJIMAGE"));

		pFTP->Close ();

		delete pFTP;
	}
	else {
		CString str;

		str.Format (_T ("FTP failed: %s"), strServer);
		AfxMessageBox (str, MB_OK | MB_ICONERROR);
	}
}
