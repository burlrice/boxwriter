// PrinterDriverDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PrinterDriver.h"
#include "PrinterDriverDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	CString str;

	str.Format ("%s(%d): %s\n", lpszFile, lLine, lpsz);
	::OutputDebugString (str);
}

/////////////////////////////////////////////////////////////////////////////
// CPrinterDriverDlg dialog

CPrinterDriverDlg::CPrinterDriverDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPrinterDriverDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPrinterDriverDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPrinterDriverDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrinterDriverDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPrinterDriverDlg, CDialog)
	//{{AFX_MSG_MAP(CPrinterDriverDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_PRINT, OnPrint)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrinterDriverDlg message handlers

BOOL CPrinterDriverDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	OnPrint ();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPrinterDriverDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CPrinterDriverDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CPrinterDriverDlg::OnPrint() 
{
	BeginWaitCursor ();

	DOCINFO docinfo;
	CDC dc;
	CSize size (0, 0), res (0, 0);
	CFont fnt;
	const int nFontSize = 64;

	VERIFY (fnt.CreateFont (nFontSize, 0, 0, 0, FW_BOLD, FALSE, 0, 0,
		DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("Arial")));

//	VERIFY (dc.CreateDC (_T ("winspool"), _T ("HP Color LaserJet CP1510 Series PCL 6"), _T ("USB001"), NULL));
//	VERIFY (dc.CreateDC (_T ("winspool"), _T ("Bitmap Driver"), _T ("Bitmap2"), NULL));
	VERIFY (dc.CreateDC (_T ("winspool"), _T ("Foxjet Marksman NEXT"), _T ("Foxjet"), NULL));

	memset(&docinfo, 0, sizeof(docinfo));
	docinfo.cbSize = sizeof(docinfo);
	docinfo.lpszDocName = _T ("CPrinterDriverDlg::OnPrint");

	CFont * pFont = dc.SelectObject (&fnt);
	int nTextColor = dc.SetTextColor (RGB (0, 0, 0));

	VERIFY (dc.StartDoc (&docinfo) >= 0);

	size.cx = ::GetDeviceCaps (dc.m_hDC, HORZRES);
	size.cy = ::GetDeviceCaps (dc.m_hDC, VERTRES);
	res.cx	= ::GetDeviceCaps (dc.m_hDC, LOGPIXELSX);
	res.cy	= ::GetDeviceCaps (dc.m_hDC, LOGPIXELSY);

	for (int i = 0; i < 1; i++) {
		CString str;

		str.Format ("Page %d [%s]", i + 1, CTime::GetCurrentTime ().Format (_T ("%c")));

		if (dc.StartPage () < 0) {
			MessageBox ("StartPage failed");
			dc.AbortDoc();
			EndWaitCursor ();
			return;
		}

		dc.TextOut (0, 0, str);

		for (int i = 1; i <= 49; i++) {
			str.Format ("Line %02d", i);
			dc.TextOut (0, nFontSize * i, str);
		}

		dc.EndPage();
	}

	dc.EndDoc();

	dc.SetTextColor (nTextColor);
	dc.SelectObject (pFont);
	EndWaitCursor ();
}
