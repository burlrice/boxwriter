// DriverDlg.cpp : implementation file
//

#include "stdafx.h"
#include <Winspool.h>
#include "Debug.h"
#include "Utils.h"
#include "PrintMonitor.h"
#include "DriverDlg.h"
#include "Database.h"
#include "PrintMonitorDlg.h"
#include "Parse.h"
#include "Registry.h"
#include "AnsiString.h"

extern CPrintMonitorApp theApp;

using namespace ItiLibrary;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

using CDriverDlg::CPrinterItem;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////

CPrinterItem::CPrinterItem (const FoxjetDatabase::HEADSTRUCT & head)
:	m_head (head)
{
}

CPrinterItem::~CPrinterItem ()
{
}

CString CPrinterItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:	return m_head.m_strName;
	case 1:	return m_head.m_strUID;
	case 2: return m_strState;
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CDriverDlg dialog


CDriverDlg::CDriverDlg(CWnd* pParent /*=NULL*/)
:	CDialog(IDD_DRIVER, pParent)
{
	//{{AFX_DATA_INIT(CDriverDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDriverDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDriverDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDriverDlg, CDialog)
	//{{AFX_MSG_MAP(CDriverDlg)
	ON_NOTIFY(LVN_ITEMCHANGED, LV_DATA, OnItemchangedData)
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDriverDlg message handlers

BOOL CDriverDlg::OnInitDialog() 
{
	using CListCtrlImp::CColumn;

	CArray <CColumn, CColumn> vCols;
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

	BeginWaitCursor ();
	CDialog::OnInitDialog();
	
	SetDlgItemText (TXT_PATH, FoxjetDatabase::GetProfileString (::lpszDriverKey, _T ("inf"), GetHomeDir () + _T ("\\PrintDriver\\fjmknext.inf"))); 
	vCols.Add (CColumn (LoadString (IDS_NAME), 120));
	vCols.Add (CColumn (LoadString (IDS_ADDRESS), 120));
	vCols.Add (CColumn (LoadString (IDS_STATE), 300));

	m_lv.Create (LV_DATA, vCols, this, _T ("CDriverDlg"));
	m_lv->SetExtendedStyle (LVS_EX_CHECKBOXES | m_lv->GetExtendedStyle ());
	
	GetHeadRecords (theApp.m_db, vHeads);

	for (int i = 0; i < vHeads.GetSize (); i++) {
		HEADSTRUCT & head = vHeads [i];

		CPrinterItem * p = new CPrinterItem (head);
		m_lv.InsertCtrlData (p, 0);
		m_lv->SetCheck (0, true);
	}

	for (int n = 0; n < m_lv->GetItemCount (); n++) {
		if (CPrinterItem * pItem = (CPrinterItem *)m_lv.GetCtrlData (n)) {
			if (IsPrinterInstalled (pItem->m_head.m_strUID)) {
				pItem->m_strState = LoadString (IDS_INSTALLED);
				m_lv.UpdateCtrlData (pItem);
				m_lv->SetCheck (n, false);
			}
		}
	}

	EndWaitCursor ();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

bool CDriverDlg::IsPrinterInstalled (const CString & strPort)
{
	DWORD dwNeeded = 0, dwReturned = 0;
	bool bResult = false;

	::EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 2, NULL, 0, &dwNeeded, &dwReturned);

	if (dwNeeded > 0) {
		PRINTER_INFO_2 * p = (PRINTER_INFO_2 *)new BYTE [dwNeeded];

		if (::EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 2, (LPBYTE)p, dwNeeded, &dwNeeded, &dwReturned)) {
			for (DWORD i = 0; i < dwReturned; i++) {
				TRACEF (p [i].pDriverName + CString (_T (": ")) + p [i].pPrinterName + CString (_T (": ")) + p [i].pPortName);
				
				if (!_tcscmp (_T ("Foxjet Marksman NEXT"), p [i].pDriverName)) {
					if (!_tcscmp (strPort, p [i].pPortName)) {
						bResult = true;
						break;
					}
				}
			}
		}
		
		delete [] p;
	}

	return bResult;
}

void CDriverDlg::OnItemchangedData(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	CLongArray sel = GetSelectedItems (m_lv);
	static const CString strInstalled = LoadString (IDS_INSTALLED);
	bool bFound = false;

	if (CPrinterItem * pItem = (CPrinterItem *)m_lv.GetCtrlData (pNMListView->iItem)) {
		if (!_tcscmp (pItem->m_strState, strInstalled)) {
			m_lv->SetCheck (pNMListView->iItem, false);
			*pResult = 0;
			return;
		}
	}

	for (int i = 0; !bFound && i < sel.GetSize (); i++)
		bFound = sel [i] == (ULONG)pNMListView->iItem;

	if (sel.GetSize () > 1 && bFound) {
		if (pNMListView->uChanged & LVIF_STATE) {
			bool bCheck = m_lv->GetCheck (pNMListView->iItem) ? true : false;

			for (int i = 0; i < sel.GetSize (); i++) 
				m_lv->SetCheck (sel [i], bCheck);
		}
	}
	
	*pResult = 0;
}

static BOOL CALLBACK EnumWindowProc (HWND hwnd, LPARAM lParam)
{
	TCHAR sz [512] = { 0 };

	::GetWindowText (hwnd, sz, ARRAYSIZE (sz) - 1);
	::OutputDebugString (CString (sz) + _T ("\n"));

	if (!_tcscmp (sz, _T ("Hardware Installation"))) {
		HWND * hResult = (HWND *)lParam;

		* hResult = hwnd;

		return FALSE;
	}

	return TRUE;
}

void CDriverDlg::OnOK()
{
	CProgressCtrl * pProgress = (CProgressCtrl *)GetDlgItem (IDC_STATUS);
	UINT n [] = 
	{
		IDOK,
		IDCANCEL,
		LV_DATA,
		BTN_BROWSE,
	};
	int nTotal = 0;
	int nNext = 1;
	CString strINF;
	int nInstalled = 0;

	ASSERT (pProgress);

	GetDlgItemText (TXT_PATH, strINF);

	if (::GetFileAttributes (strINF) == -1) {
		MessageBox (LoadString (IDS_NOTFOUND) + _T ("\n") + strINF, NULL, MB_OK | MB_ICONERROR);
		return;
	}

	for (int i = 0; i < ARRAYSIZE (n); i++)
		if (CWnd * p = GetDlgItem (n [i]))
			p->EnableWindow (false);

	for (i = 0; i < m_lv->GetItemCount (); i++) 
		if (m_lv->GetCheck (i)) 
			nTotal++;

	pProgress->SetRange (0, nTotal);
	pProgress->SetPos (0);

	for (i = 0; i < m_lv->GetItemCount (); i++) {
		if (m_lv->GetCheck (i)) {
			if (CPrinterItem * pItem = (CPrinterItem *)m_lv.GetCtrlData (i)) {
				BeginWaitCursor ();

				CString strCmdLine;
				STARTUPINFO si;     
				PROCESS_INFORMATION pi; 
				TCHAR szCmdLine [MAX_PATH];

				#ifdef _DEBUG
				{
					DWORD dwNeeded = 0;
					DWORD dwReturned = 0;
					bool bResult = false;

					::EnumMonitors (NULL, 1, NULL, 0, &dwNeeded, &dwReturned);
					MONITOR_INFO_1 * p = (MONITOR_INFO_1 *)new BYTE [dwNeeded];
					
					if (!::EnumMonitors (NULL, 1, (LPBYTE)p, dwNeeded, &dwNeeded, &dwReturned))
						TRACEF (FormatMessage (::GetLastError ()));

					for (DWORD n = 0; n < dwReturned; n++) {
						TRACEF (p [n].pName);
					}

					delete p;
				}
				#endif _DEBUG

				if (!IsPrinterPortInstalled (pItem->m_head.m_strUID)) 
					AddPrinterPort (PORTSTRUCT (m_hWnd, pItem->m_head.m_strUID));

				strCmdLine.Format (_T ("RUNDLL32 PRINTUI.DLL,PrintUIEntry /if /b \"%s\" /f \"%s\" /r \"%s\" /m \"%s\""),
					pItem->m_head.m_strName,
					strINF,
					pItem->m_head.m_strUID,
					_T ("Foxjet Marksman NEXT"));
				TRACEF (strCmdLine);

				_tcsncpy (szCmdLine, strCmdLine, MAX_PATH);
				memset (&pi, 0, sizeof(pi));
				memset (&si, 0, sizeof(si));
				si.cb = sizeof(si);     
				si.dwFlags = STARTF_USESHOWWINDOW;     
				si.wShowWindow = SW_SHOW;     

				if (!::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) 
					TRACEF (FormatMessage (::GetLastError ()));
				else {
					const CTime tmStart = CTime::GetCurrentTime ();
					CTimeSpan tm = CTime::GetCurrentTime () - tmStart;
					bool bInstalled = false;

					while (tm.GetTotalSeconds () < 30) {
						HWND hwnd = NULL;

						::EnumWindows (::EnumWindowProc, (LPARAM)&hwnd);

						if (hwnd) {
							for (HWND h = ::GetWindow (hwnd, GW_CHILD | GW_HWNDFIRST); h != NULL; h = ::GetWindow (h, GW_HWNDNEXT)) {
								TCHAR sz [128] = { 0 };
								TCHAR szClass [128] = { 0 };
								ULONG lID = ::GetWindowLong (h, GWL_ID);

								::GetClassName (h, szClass, ARRAYSIZE (szClass) - 1);
								::GetWindowText (h, sz, ARRAYSIZE (sz) - 1); 
								::OutputDebugString (szClass + CString (_T (": ")) + sz + CString (_T (" [")) + ToString (lID) + _T ("]\n"));

								if (!_tcscmp (szClass, _T ("Button")) && _tcsstr (sz, _T ("Continue Anyway")) != NULL) {
									::PostMessage (hwnd, WM_COMMAND, MAKEWPARAM (lID, BN_CLICKED), NULL);
								}
							}
						}

						if (IsPrinterInstalled (pItem->m_head.m_strUID)) {
							pItem->m_strState = LoadString (IDS_INSTALLED);
							m_lv.UpdateCtrlData (pItem);
							PumpMessages ();
							bInstalled = true;
							nInstalled++;
							break;
						}

						tm = CTime::GetCurrentTime () - tmStart;
					}

					if (!bInstalled) {
						pItem->m_strState = LoadString (IDS_INSTALLFAILED);
						m_lv.UpdateCtrlData (pItem);
						PumpMessages ();
					}
				}

				EndWaitCursor ();
			}

			pProgress->SetPos (nNext++);
		}

		PumpMessages ();
	}

	for (i = 0; i < ARRAYSIZE (n); i++)
		if (CWnd * p = GetDlgItem (n [i]))
			p->EnableWindow (true);

	pProgress->SetPos (0);

	if (nInstalled == nTotal)
		CDialog::OnOK ();
}

void CDriverDlg::PumpMessages()
{
	MSG msg;

	while (::PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE)) {
		::GetMessage (&msg, (HWND)NULL, 0, 0);
		::TranslateMessage (&msg);
		::DispatchMessage (&msg);
	}
}


void CDriverDlg::OnBrowse() 
{
	CString str;

	GetDlgItemText (TXT_PATH, str);

	CFileDialog dlg (TRUE, _T ("*.inf"), str, OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, _T ("Setup Information files (*.inf)|*.inf||"), this);

	if (dlg.DoModal () == IDOK) {
		str = dlg.GetPathName ();
		FoxjetDatabase::WriteProfileString (::lpszDriverKey, _T ("inf"), str); 
		SetDlgItemText (TXT_PATH, str);
	}
}

