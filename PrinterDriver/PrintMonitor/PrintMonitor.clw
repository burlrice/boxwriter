; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDriverStoreDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "PrintMonitor.h"

ClassCount=9
Class1=CPrintMonitorApp
Class2=CPrintMonitorDlg
Class3=CAboutDlg

ResourceCount=10
Resource1=IDD_PREVIEW
Resource2=IDR_MAINFRAME
Resource3=IDD_DRIVER
Resource4=IDM_SYSTRAY
Resource5=IDD_FIRMWARE
Class4=CPrintMonitorHeadConfigDlg
Class5=CDriverDlg
Resource6=IDD_DRIVERSTORE
Class6=CFirmwareDlg
Resource7=IDD_PRINTMONITOR_DIALOG
Class7=CPreviewDlg
Resource8=IDD_SELECTPRINTER
Class8=CSelectPrinter
Resource9=IDM_MENU
Class9=CDriverStoreDlg
Resource10=IDM_CONTEXT

[CLS:CPrintMonitorApp]
Type=0
HeaderFile=PrintMonitor.h
ImplementationFile=PrintMonitor.cpp
Filter=N

[CLS:CPrintMonitorDlg]
Type=0
HeaderFile=PrintMonitorDlg.h
ImplementationFile=PrintMonitorDlg.cpp
Filter=W
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=PrintMonitorDlg.h
ImplementationFile=PrintMonitorDlg.cpp
Filter=D

[DLG:IDD_PRINTMONITOR_DIALOG]
Type=1
Class=CPrintMonitorDlg
ControlCount=2
Control1=LV_DATA,SysListView32,1350631453
Control2=LBL_STATUS,static,1342312460

[MNU:IDM_SYSTRAY]
Type=1
Class=?
Command1=ID__SHOWWINDOW
Command2=ID_FILE_EXIT
CommandCount=2

[MNU:IDM_MENU]
Type=1
Class=?
Command1=ID_FILE_HEADCONFIGURATION
Command2=ID_FILE_UPDATEALLCONFIGURATIONS
Command3=ID_FILE_INSTALLDRIVER
Command4=ID_FILE_DRIVERCACHE
Command5=ID_FILE_DEBUGVIEWER
Command6=ID_FILE_EXIT
Command7=ID_DOCUMENT_CANCEL
Command8=ID_HELP_ABOUT
CommandCount=8

[CLS:CPrintMonitorHeadConfigDlg]
Type=0
HeaderFile=PrintMonitorHeadConfigDlg.h
ImplementationFile=PrintMonitorHeadConfigDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_STATUS

[MNU:IDM_CONTEXT]
Type=1
Class=?
Command1=ID_DOCUMENT_PREVIEW
Command2=ID_DOCUMENT_CANCEL
Command3=ID_DOCUMENT_HEADPROPERTIES
Command4=ID_DOCUMENT_PRINTDRIVERPROPERTIES
Command5=ID_DOCUMENT_FIRMWAREVERSION
Command6=ID_DOCUMENT_SYNCRONIZECONFIGURATION
CommandCount=6

[DLG:IDD_DRIVER]
Type=1
Class=CDriverDlg
ControlCount=6
Control1=LV_DATA,SysListView32,1350631449
Control2=TXT_PATH,edit,1350633600
Control3=BTN_BROWSE,button,1342242816
Control4=IDC_STATUS,msctls_progress32,1342177280
Control5=IDOK,button,1342242816
Control6=IDCANCEL,button,1342242816

[CLS:CDriverDlg]
Type=0
HeaderFile=DriverDlg.h
ImplementationFile=DriverDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=BTN_UPDATEGA

[DLG:IDD_FIRMWARE]
Type=1
Class=CFirmwareDlg
ControlCount=20
Control1=TXT_NAME,edit,1350633600
Control2=TXT_FIRMWAREVER,edit,1350633600
Control3=TXT_FIRMWAREPATH,edit,1350633600
Control4=BTN_BROWSEFIRMWARE,button,1342242816
Control5=BTN_UPDATEFIRMWARE,button,1342242816
Control6=TXT_GAVER,edit,1350633600
Control7=TXT_GAPATH,edit,1350633600
Control8=BTN_BROWSEGA,button,1342242816
Control9=BTN_UPDATEGA,button,1342242816
Control10=IDOK,button,1342242817
Control11=IDCANCEL,button,1342242816
Control12=IDC_STATIC,static,1342308352
Control13=IDC_STATIC,static,1342308352
Control14=IDC_STATIC,button,1342177287
Control15=IDC_STATIC,static,1342308352
Control16=IDC_STATIC,static,1342308352
Control17=IDC_STATIC,button,1342177287
Control18=IDC_STATIC,static,1342308352
Control19=TXT_ADDR,edit,1350633600
Control20=IDC_STATIC,static,1342308352

[CLS:CFirmwareDlg]
Type=0
HeaderFile=FirmwareDlg.h
ImplementationFile=FirmwareDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=BTN_BROWSEFIRMWARE

[DLG:IDD_PREVIEW]
Type=1
Class=CPreviewDlg
ControlCount=0

[CLS:CPreviewDlg]
Type=0
HeaderFile=PreviewDlg.h
ImplementationFile=PreviewDlg.cpp
BaseClass=CDialog
Filter=W
LastObject=CPreviewDlg
VirtualFilter=dWC

[DLG:IDD_SELECTPRINTER]
Type=1
Class=CSelectPrinter
ControlCount=3
Control1=LV_PRINTERS,SysListView32,1350631437
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816

[CLS:CSelectPrinter]
Type=0
HeaderFile=SelectPrinter.h
ImplementationFile=SelectPrinter.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC

[DLG:IDD_DRIVERSTORE]
Type=1
Class=CDriverStoreDlg
ControlCount=3
Control1=IDOK,button,1342242817
Control2=LV_DRIVERS,SysListView32,1350631433
Control3=BTN_DELETE,button,1342242816

[CLS:CDriverStoreDlg]
Type=0
HeaderFile=DriverStoreDlg.h
ImplementationFile=DriverStoreDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=BTN_DELETE
VirtualFilter=dWC

