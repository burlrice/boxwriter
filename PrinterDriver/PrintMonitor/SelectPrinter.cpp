// SelectPrinter.cpp : implementation file
//

#include "stdafx.h"
#include "PrintMonitor.h"
#include "SelectPrinter.h"
#include "resource.h"

using namespace ItiLibrary;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CSelectPrinterDlg::CPrinterItem::CPrinterItem (const CString & strDriver, const CString & strName, const CString  & strPort)
:	m_strDriver (strDriver),
	m_strName (strName),
	m_strPort (strPort)
{
}

CSelectPrinterDlg::CPrinterItem::~CPrinterItem ()
{
}

CString CSelectPrinterDlg::CPrinterItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:	return m_strName;
	case 1:	return m_strPort;
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CSelectPrinterDlg dialog


CSelectPrinterDlg::CSelectPrinterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectPrinterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectPrinterDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CSelectPrinterDlg::~CSelectPrinterDlg ()
{
}


void CSelectPrinterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectPrinterDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectPrinterDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectPrinterDlg)
	//}}AFX_MSG_MAP
	ON_NOTIFY(NM_DBLCLK, LV_PRINTERS, OnDblclkPrinters)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectPrinterDlg message handlers

BOOL CSelectPrinterDlg::OnInitDialog() 
{
	CArray <CListCtrlImp::CColumn, CListCtrlImp::CColumn> vCols;

	CDialog::OnInitDialog();

	vCols.Add (CListCtrlImp::CColumn (LoadString (IDS_NAME), 180));
	vCols.Add (CListCtrlImp::CColumn (LoadString (IDS_ADDRESS), 100));

	m_lv.Create (LV_PRINTERS, vCols, this, _T ("CSelectPrinterDlg"));
	
	for (int i = 0; i < m_vPrinters.GetSize (); i++)
		m_lv.InsertCtrlData (m_vPrinters [i]);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelectPrinterDlg::OnOK()
{
	FoxjetCommon::CLongArray sel = GetSelectedItems (m_lv);

	if (!sel.GetSize ()) {
		MessageBox (LoadString (IDS_NOTHINGSELECTED));
		return;
	}

	if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (sel [0])) {
		m_strDriver = p->m_strDriver;
		m_strPort = p->m_strPort;
		CDialog::OnOK ();
	}
}

void CSelectPrinterDlg::OnDblclkPrinters(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnOK ();

	if (pResult)
		* pResult = 0;
}
