// stdafx.cpp : source file that includes just the standard includes
//	PrintMonitor.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include <Winspool.h>
#include <shlwapi.h>
#include "PrintMonitor.h"
#include "Debug.h"
#include "Database.h"
#include "Utils.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;

LPCTSTR lpszDriverKey = _T ("SOFTWARE\\FoxJet_Driver");

extern CPrintMonitorApp theApp;
const CVersion verNull (0, 0, 0, 0);

CString LoadString (UINT nID)
{
	HINSTANCE hInstance = theApp.m_hInstance;
	HINSTANCE hInst = ::AfxGetResourceHandle ();
	CString str;

	ASSERT (hInstance);
	::AfxSetResourceHandle (hInstance);

	BOOL bLoad = str.LoadString (nID);
	ASSERT (bLoad);
	
	::AfxSetResourceHandle (hInst);

	return str;
}

CVersion GetVersion (const CString & strFile)
{
	CVersion verResult = ::verNull;

	if (HMODULE hComponent = ::LoadLibraryEx (strFile, NULL, LOAD_LIBRARY_AS_DATAFILE)) {
		DLLGETVERSIONPROC lpDllGetVersion = (DLLGETVERSIONPROC)GetProcAddress (hComponent, "DllGetVersion");

		if (lpDllGetVersion) {
			DLLVERSIONINFO dvi;
			HRESULT hr;

			ZeroMemory(&dvi, sizeof(dvi));
			dvi.cbSize = sizeof(dvi);
			hr = (* lpDllGetVersion)(&dvi);
			
			if (SUCCEEDED (hr)) 
				verResult = CVersion (dvi.dwMajorVersion, dvi.dwMinorVersion, 0, 0, dvi.dwBuildNumber);
		}

		::FreeLibrary (hComponent);
	}

	if (verResult == ::verNull) {
		DWORD dw = 0;
		CString str = strFile;
		DWORD dwSize = ::GetFileVersionInfoSize (str.GetBuffer (str.GetLength ()), &dw);
		BYTE * pVer = new BYTE [dwSize];

		memset (pVer, 0, dwSize);

		if (::GetFileVersionInfo (str.GetBuffer (str.GetLength ()), 0, dwSize, pVer)) {
			LPVOID pData = NULL;
			UINT nLen = 0;

			if (::VerQueryValue (pVer, _T ("\\"), &pData, &nLen)) {
				if (VS_FIXEDFILEINFO * pInfo = (VS_FIXEDFILEINFO *)pData) {
					verResult = CVersion (HIWORD (pInfo->dwFileVersionMS), LOWORD (pInfo->dwFileVersionMS), HIWORD (pInfo->dwFileVersionLS), 0, LOWORD (pInfo->dwFileVersionLS));

					str.Format (_T ("%d.%d.%d.%d: %s"),
						HIWORD (pInfo->dwFileVersionMS),
						LOWORD (pInfo->dwFileVersionMS),
						HIWORD (pInfo->dwFileVersionLS),
						LOWORD (pInfo->dwFileVersionLS),
						strFile);
					TRACEF (str);
 				}
			}
		}

		delete [] pVer;
	}

	return verResult;
}


static BOOL CALLBACK EnumChildProc (HWND hwnd, LPARAM lParam)
{
	HWND * hwndParent = (HWND *)lParam;

	/*
	{
		#ifdef _DEBUG
		TCHAR sz [128] = { 0 };
		CString str;

		str.Format (_T ("0x%p: "), hwnd);

		::GetClassName (hwnd, sz, ARRAYSIZE (sz) - 1); str += sz + CString (_T (": "));
		::GetWindowText (hwnd, sz, ARRAYSIZE (sz) - 1); str += sz;
		TRACEF (str);
		#endif
	}
	*/

	if (::GetParent (hwnd) == (* hwndParent)) {
		(* hwndParent) = hwnd;
		return FALSE;
	}

	return TRUE;
}

static ULONG CALLBACK PortFunc (LPVOID lpData) 
{
	const PORTSTRUCT & params = * (PORTSTRUCT *)lpData;
	const CTime tmStart = CTime::GetCurrentTime ();
	CTimeSpan tm = CTime::GetCurrentTime () - tmStart;

	while (tm.GetTotalSeconds () < 5) {
		HWND hwnd = params.m_hWnd;

		::EnumWindows (EnumChildProc, (LPARAM)&hwnd);

		if (::GetParent (hwnd) == params.m_hWnd) {
			TCHAR sz [128] = { 0 };

			#ifdef _DEBUG
			CString str;

			str.Format (_T ("0x%p [0x%p]: "), hwnd, ::GetParent (hwnd));

			::GetClassName (hwnd, sz, ARRAYSIZE (sz) - 1); str += sz + CString (_T (": "));
			::GetWindowText (hwnd, sz, ARRAYSIZE (sz) - 1); str += sz;
			TRACEF (str);
			#endif

			for (HWND h = ::GetWindow (hwnd, GW_CHILD | GW_HWNDFIRST); h != NULL; h = ::GetWindow (h, GW_HWNDNEXT)) {
				TCHAR sz [128] = { 0 };
				TCHAR szClass [128] = { 0 };

				::GetClassName (h, szClass, ARRAYSIZE (szClass) - 1);
				::GetWindowText (h, sz, ARRAYSIZE (sz) - 1); 
				::OutputDebugString (szClass + CString (_T (": ")) + sz + CString (_T ("\n")));

				if (!_tcscmp (szClass, _T ("Static")) && _tcsstr (sz, _T ("Enter a port name")) != NULL) {
					if (h = ::GetWindow (h, GW_HWNDNEXT)) {
						HWND hOK = ::GetDlgItem (hwnd, IDOK);

						::GetClassName (h, szClass, ARRAYSIZE (szClass) - 1);
						::GetWindowText (h, sz, ARRAYSIZE (sz) - 1); 
						::OutputDebugString (szClass + CString (_T (": ")) + sz + CString (_T ("\n")));

						::SendMessage (h, WM_SETTEXT, 0, (LPARAM)(LPCTSTR)params.m_strPort);
						::Sleep (500);
						::SendMessage (hwnd, WM_COMMAND, (WPARAM)IDOK, (LPARAM)hOK);
						return 0;
					}
				}
			}
		}

		::Sleep (500);
		tm = CTime::GetCurrentTime () - tmStart;
	}

	TRACEF (_T ("PortFunc failed: ") + params.m_strPort);

	return 0;
}

bool IsPrinterPortInstalled(const CString & str)
{
	DWORD dwNeeded = 0;
	DWORD dwReturned = 0;
	bool bResult = false;

	::EnumPorts (NULL, 1, NULL, 0, &dwNeeded, &dwReturned);
	PORT_INFO_1 * pPorts = (PORT_INFO_1 *)new BYTE [dwNeeded];
	
	if (!::EnumPorts (NULL, 1, (LPBYTE)pPorts, dwNeeded, &dwNeeded, &dwReturned))
		TRACEF (FormatMessage (::GetLastError ()));

	for (DWORD nPort = 0; nPort < dwReturned; nPort++) {
		//TRACEF (pPorts [nPort].pName);

		if (!_tcscmp (str, pPorts [nPort].pName)) {
			bResult = true;
			break;
		}
	}

	delete pPorts;

	return bResult;
}

void AddPrinterPort (const PORTSTRUCT & port)
{
	if (!IsPrinterPortInstalled (port.m_strPort)) {
		TCHAR sz [128];
		DWORD dwThreadID = 0;

		_tcscpy (sz, _T ("Local Port"));
		HANDLE hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)PortFunc, (LPVOID)&port, 0, &dwThreadID);

		if (!::AddPort (NULL, port.m_hWnd, sz))
			TRACEF (FormatMessage (::GetLastError ()));
		else {
			ASSERT (IsPrinterPortInstalled (port.m_strPort));
		}

		DWORD dwWait = ::WaitForSingleObject (hThread, 6 * 1000);

		if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
		if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
		if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
		if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");
	}
}

void UpdatePrinterPorts (HWND hwnd, FoxjetDatabase::COdbcDatabase & db)
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
	DWORD dwNeeded = 0, dwReturned = 0;

	GetHeadRecords (db, vHeads);
	::EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 2, NULL, 0, &dwNeeded, &dwReturned);

	if (dwNeeded > 0) {
		PRINTER_INFO_2 * p = (PRINTER_INFO_2 *)new BYTE [dwNeeded] ;//new PRINTER_INFO_2 [nCount];

		if (::EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 2, (LPBYTE)p, dwNeeded, &dwNeeded, &dwReturned)) {
			for (DWORD i = 0; i < dwReturned; i++) {

				TRACEF (p [i].pDriverName + CString (_T (": ")) + p [i].pPrinterName + CString (_T (": ")) + p [i].pPortName);

				if (!_tcscmp (_T ("Foxjet Marksman NEXT"), p [i].pDriverName)) {
					bool bFound = false;

					for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
						HEADSTRUCT & h = vHeads [nHead];

						if (!h.m_strName.CompareNoCase (p [i].pPrinterName)) {
							if (h.m_strUID.CompareNoCase (p [i].pPortName) != 0) {
								TCHAR sz [_MAX_PATH] = { 0 };
								PRINTER_DEFAULTS pd;
								HANDLE hPrinter = NULL;

								memset (&pd, 0, sizeof (pd));
								pd.DesiredAccess = PRINTER_ALL_ACCESS;

								_tcsncpy (sz, h.m_strName, ARRAYSIZE (sz));

								if (::OpenPrinter (sz, &hPrinter, &pd)) {
									DWORD dwNeeded = 0;

									::GetPrinter (hPrinter, 5, NULL, 0, &dwNeeded);
									BYTE * pInfo = new BYTE [dwNeeded];
									memset (pInfo, 0, dwNeeded);

									if (::GetPrinter (hPrinter, 5, pInfo, dwNeeded, &dwNeeded)) {
										PRINTER_INFO_5 & info = * (PRINTER_INFO_5 *)pInfo;
										
										TRACEF (info.pPrinterName + CString (_T (": ")) + info.pPortName);
										_tcscpy (info.pPortName, h.m_strUID);
										TRACEF (info.pPrinterName + CString (_T (": ")) + info.pPortName);

										if (!IsPrinterPortInstalled (h.m_strUID)) 
											AddPrinterPort (PORTSTRUCT (hwnd, h.m_strUID));

										VERIFY (::SetPrinter (hPrinter, 5, pInfo, 0));
									}

									delete pInfo;
									::ClosePrinter (hPrinter);
								}

								break;
							}
						}
					}
				}
			}
		}
	}
}

