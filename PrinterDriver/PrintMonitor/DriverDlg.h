#if !defined(AFX_DRIVERDLG_H__C9BBA9F4_51E1_46CF_9931_D0A8D826BECA__INCLUDED_)
#define AFX_DRIVERDLG_H__C9BBA9F4_51E1_46CF_9931_D0A8D826BECA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DriverDlg.h : header file
//

#include "ListCtrlImp.h"
#include "Database.h"

/////////////////////////////////////////////////////////////////////////////
// CDriverDlg dialog

class CDriverDlg : public CDialog
{
// Construction
public:
	CDriverDlg(CWnd* pParent = NULL);   // standard constructor

	class CPrinterItem : public ItiLibrary::CListCtrlImp::CItem
	{
	public:
		CPrinterItem (const FoxjetDatabase::HEADSTRUCT & head);
		virtual ~CPrinterItem ();

		virtual CString GetDispText (int nColumn) const;

		FoxjetDatabase::HEADSTRUCT m_head;
		CString m_strState;
	};

	static bool IsPrinterInstalled (const CString & strPort);

// Dialog Data
	//{{AFX_DATA(CDriverDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDriverDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	void PumpMessages();
	virtual void OnOK();

	ItiLibrary::CListCtrlImp m_lv;

	// Generated message map functions
	//{{AFX_MSG(CDriverDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnItemchangedData(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBrowse();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CString m_strAddrCopy;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DRIVERDLG_H__C9BBA9F4_51E1_46CF_9931_D0A8D826BECA__INCLUDED_)
