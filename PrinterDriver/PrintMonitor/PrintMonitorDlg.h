// PrintMonitorDlg.h : header file
//

#if !defined(AFX_PRINTMONITORDLG_H__2DD84037_8DFE_4AAC_8DF5_0B7629D31306__INCLUDED_)
#define AFX_PRINTMONITORDLG_H__2DD84037_8DFE_4AAC_8DF5_0B7629D31306__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxsock.h>
#include "ListCtrlImp.h"
#include "Database.h"

interface ITaskbarList : public IUnknown {
   virtual HRESULT __stdcall ActivateTab(HWND hwnd) = 0;
   virtual HRESULT __stdcall AddTab(HWND hwnd) = 0;
   virtual HRESULT __stdcall DeleteTab(HWND hwnd) = 0;
   virtual HRESULT __stdcall HrInit(void) = 0;
   virtual HRESULT __stdcall SetActiveAlt(HWND hwnd);
};

/////////////////////////////////////////////////////////////////////////////
// CPrintMonitorDlg dialog

class CPrintMonitorDlg : public CDialog
{
	class CPrinterItem : public ItiLibrary::CListCtrlImp::CItem
	{
	public:
		CPrinterItem (int nHeadNumber, const CString & strAddr);
		virtual ~CPrinterItem ();

		virtual CString GetDispText (int nColumn) const;

		int m_nHeadNumber;
		CString m_strName;
		CString m_strAddr;
		CString m_strState;
		CString m_strSwVer;
		CString m_strGaVer;
		CString m_strSpeed;
		CString m_strErrors;
		DWORD m_dwThreadID;
		bool m_bUpdating;
	};

	class CHeadData 
	{
	public:
		CHeadData (CPrintMonitorDlg & dlg, const FoxjetDatabase::HEADSTRUCT & head, int nHead) : m_dlg (dlg), m_head (head), m_nHead (nHead) { }

		const int m_nHead;
		CPrintMonitorDlg & m_dlg;
		FoxjetDatabase::HEADSTRUCT m_head;
	};

	typedef struct tagIPCSTRUCT
	{
		tagIPCSTRUCT (ULONG lHeadID, ULONG lCmdID) : m_lHeadID (lHeadID), m_lCmdID (lCmdID) { }

		ULONG m_lHeadID;
		ULONG m_lCmdID;
	} IPCSTRUCT;

// Construction
public:
	CString GetError (DWORD dwError);
	static CString GetKey (UINT nHeadNumber);
	static int GetHeadNumberFromID (ULONG lFindID);

	void FillListCtrl ();
	void RemoveTaskbarButton();
	CPrintMonitorDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CPrintMonitorDlg ();

// Dialog Data
	//{{AFX_DATA(CPrintMonitorDlg)
	enum { IDD = IDD_PRINTMONITOR_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintMonitorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void OpenPrinter (const CString & strPrinter, const CString & strPort);
	void TraceListCtrl();
	void PumpMessages ();
	void SendConfiguration (int nHead);
	void SendConfiguration ();
	
	static void SendConfiguration (const CPrintMonitorDlg::CHeadData & data, SOCKET s);
	static bool CompareConfiguration (const CPrintMonitorDlg::CHeadData & data, SOCKET s);

	virtual void OnCancel();
	virtual void OnOK();

	HICON m_hIcon;
	ItiLibrary::CListCtrlImp m_lv;
	NOTIFYICONDATA m_nid;
	ITaskbarList * m_pTaskbar;
	HANDLE m_hExit;

	CCriticalSection m_csThread;
	CMap <HANDLE, HANDLE, DWORD, DWORD> m_vThreads;
	HANDLE m_hStatusThread;

	void Update (int nHead, const CString & strState);

	static ULONG CALLBACK StatusFunc (LPVOID lpData);
	static ULONG CALLBACK HeadFunc (LPVOID lpData);


	afx_msg LRESULT OnTrayNotify (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnData (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnConfigStatus (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnConfigStatusByID (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNextStatus (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHeadDeleted (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHeadChanged (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnThreadInit (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHeadConnectFailed (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnLoadHeadRecord (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetLocalConfig (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnIpcToString (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHeadReconnect (WPARAM wParam, LPARAM lParam);

	afx_msg void OnShow ();
	afx_msg void OnFileExit ();
	afx_msg void OnDocumentCancel ();

	// Generated message map functions
	//{{AFX_MSG(CPrintMonitorDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
	afx_msg void OnWindowPosChanging(WINDOWPOS FAR* lpwndpos);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnRclickData(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg void OnHelpAbout();
	afx_msg void OnFileHeadconfiguration();
	afx_msg void OnDocumentHeadproperties();
	afx_msg void OnDocumentSyncronizeconfiguration();
	afx_msg void OnFileUpdateallconfigurations();
	afx_msg void OnDblclkData(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDocumentPrintdriverproperties();
	afx_msg void OnFileInstalldriver();
	afx_msg void OnDocumentFirmwareversion();
	afx_msg void OnDocumentPreview();
	afx_msg void OnInitMenu(CMenu* pMenu);
	afx_msg void OnFileDebugviewer();
	afx_msg void OnFileDrivercache();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTMONITORDLG_H__2DD84037_8DFE_4AAC_8DF5_0B7629D31306__INCLUDED_)
