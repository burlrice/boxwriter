#if !defined(AFX_PRINTMONITORHEADCONFIGDLG_H__25F32044_28F6_4F10_9D0D_F3D2B94D313F__INCLUDED_)
#define AFX_PRINTMONITORHEADCONFIGDLG_H__25F32044_28F6_4F10_9D0D_F3D2B94D313F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrintMonitorHeadConfigDlg.h : header file
//

#include "OdbcDatabase.h"
#include "HeadConfigDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CPrintMonitorHeadConfigDlg dialog

class CPrintMonitorHeadConfigDlg : public Foxjet3d::CHeadConfigDlg
{
// Construction
public:
	CPrintMonitorHeadConfigDlg(FoxjetDatabase::COdbcDatabase & db, UNITS units, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPrintMonitorHeadConfigDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintMonitorHeadConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK ();
	virtual void InitTreeCtrl();

	// Generated message map functions
	//{{AFX_MSG(CPrintMonitorHeadConfigDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTMONITORHEADCONFIGDLG_H__25F32044_28F6_4F10_9D0D_F3D2B94D313F__INCLUDED_)
