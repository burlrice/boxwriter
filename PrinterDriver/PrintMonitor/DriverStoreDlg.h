#if !defined(AFX_DRIVERSTOREDLG_H__FD71F6E9_EE34_4E09_9252_5144B8D35F57__INCLUDED_)
#define AFX_DRIVERSTOREDLG_H__FD71F6E9_EE34_4E09_9252_5144B8D35F57__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DriverStoreDlg.h : header file
//

#include "resource.h"
#include "ListCtrlImp.h"

/////////////////////////////////////////////////////////////////////////////
// CDriverStoreDlg dialog

class CDriverStoreDlg : public CDialog
{
// Construction
public:
	class CDriverItem : public ItiLibrary::CListCtrlImp::CItem
	{
	public:
		virtual CString GetDispText (int nColumn) const;
		virtual int Compare (const ItiLibrary::CListCtrlImp::CItem & rhs, int nColumn) const;

		CString m_strClass;
		CString m_strClassGuid;
		CString m_strProvider;
		CString m_strCatalogFile;
		CString m_strDriverVer;
		CString m_strInf;
	};

	CDriverStoreDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDriverStoreDlg)
	enum { IDD = IDD_DRIVERSTORE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDriverStoreDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void FillListCtrl();
	ItiLibrary::CListCtrlImp m_lv;

	// Generated message map functions
	//{{AFX_MSG(CDriverStoreDlg)
	afx_msg void OnDelete();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DRIVERSTOREDLG_H__FD71F6E9_EE34_4E09_9252_5144B8D35F57__INCLUDED_)
