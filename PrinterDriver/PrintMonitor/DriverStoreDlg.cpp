// DriverStoreDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PrintMonitor.h"
#include "DriverStoreDlg.h"
#include "Database.h"
#include "Utils.h"
#include "Parse.h"
#include "Debug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ItiLibrary;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;

CString CDriverStoreDlg::CDriverItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:	return m_strClass;
	case 1:	return m_strClassGuid;
	case 2:	return m_strProvider;
	case 3:	return m_strCatalogFile;
	case 4:	return m_strDriverVer;
	case 5:	return m_strInf;
	}

	return _T ("");
}

int CDriverStoreDlg::CDriverItem::Compare (const ItiLibrary::CListCtrlImp::CItem & rhs, int nColumn) const
{
	//return GetDispText (nColumn).Compare (rhs.GetDispText (nColumn));
	
	switch (nColumn) {
	case 4:	//m_strDriverVer;
		{
			CDriverItem & item = (CDriverItem &)rhs;
			int n [2][7] = { 0 };

			//dd/mm/yy, x.x.x.x

			int nLHS = _stscanf (m_strDriverVer,		_T ("%d/%d/%d, %d.%d.%d.%d"), 
				&n [0][0], &n [0][1], &n [0][2], &n [0][3], &n [0][4], &n [0][5], &n [0][6]);
			int nRHS = _stscanf (item.m_strDriverVer,	_T ("%d/%d/%d, %d.%d.%d.%d"), 
				&n [1][0], &n [1][1], &n [1][2], &n [1][3], &n [1][4], &n [1][5], &n [1][6]);

			if (nLHS == 7 && nRHS == 7) {
				CVersion verRHS (n [0][3], n [0][4], n [0][5], n [0][6]);
				CVersion verLHS (n [1][3], n [1][4], n [1][5], n [1][6]);

				return verRHS > verLHS;
			}
		}
		break;
	case 5:	//m_strInf;
		{
			CDriverItem & item = (CDriverItem &)rhs;
			int n [2] = { 0 };
			CString str [2] = { m_strInf, item.m_strInf };

			// oem[x].inf

			str [0].MakeLower ();
			str [1].MakeLower ();

			if (_stscanf (str [0], _T ("oem%d.inf"), &n [0]) == 1 &&
				_stscanf (str [1], _T ("oem%d.inf"), &n [1]) == 1)
			{
				return n [0] > n [1] ? 1 : -1;
			}
		}
		break;
	}

	return CItem::Compare (rhs, nColumn);
}

/////////////////////////////////////////////////////////////////////////////
// CDriverStoreDlg dialog


CDriverStoreDlg::CDriverStoreDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDriverStoreDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDriverStoreDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDriverStoreDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDriverStoreDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDriverStoreDlg, CDialog)
	//{{AFX_MSG_MAP(CDriverStoreDlg)
	ON_BN_CLICKED(BTN_DELETE, OnDelete)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDriverStoreDlg message handlers

void CDriverStoreDlg::OnDelete() 
{
	CLongArray sel = GetSelectedItems (m_lv);
	
	if (!sel.GetSize ()) {
		MessageBox (LoadString (IDS_NOTHINGSELECTED));
		return;
	}
	
	if (MessageBox (LoadString (IDS_CONFIRMDELETE), _T (""), MB_YESNO | MB_ICONQUESTION) == IDYES) {
		for (int i = 0; i < sel.GetSize (); i++) {
			if (CDriverItem * p = (CDriverItem *)m_lv.GetCtrlData (sel [i])) {
				CString str;

				//str.Format (_T ("cmd /c pnputil -d %s"), p->m_strInf);
				str.Format (_T ("cmd /c \"%s\\PnpFind.exe\" /delete %s"), GetHomeDir (), p->m_strInf);
				str = execute (str);
				TRACEF (str);
				MessageBox (str);
			}
		}

		m_lv.DeleteCtrlData ();
		FillListCtrl ();
	}
}

BOOL CDriverStoreDlg::OnInitDialog() 
{
	using CListCtrlImp::CColumn;

	CArray <CColumn, CColumn> vCols;

	CDialog::OnInitDialog();	
	
	vCols.Add (CColumn (LoadString (IDS_CLASS)));
	vCols.Add (CColumn (LoadString (IDS_GUID), 300));
	vCols.Add (CColumn (LoadString (IDS_PROVIDER)));
	vCols.Add (CColumn (LoadString (IDS_CATALOGFILE), 120));
	vCols.Add (CColumn (LoadString (IDS_DRIVERVERSION), 120));
	vCols.Add (CColumn (LoadString (IDS_INF)));

	m_lv.Create (LV_DRIVERS, vCols, this, _T ("CDriverStoreDlg"));

	FillListCtrl ();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDriverStoreDlg::FillListCtrl()
{
	CString str;
	CStringArray v;
	CDriverItem * p = NULL;

	BeginWaitCursor ();

	str.Format (_T ("\"%s\\PnpFind.exe\""), GetHomeDir ());
	str = execute (str);
	Tokenize (str, v, '\n', false);

	/*
	if (FILE * fp = _tfopen (_T ("c:\\temp\\debug\\PnpUtil.txt"), _T ("w"))) {
		for (int i = 0; i < v.GetSize (); i++) {
			CString str = v [i];

			fwrite (w2a (str), str.GetLength (), 1, fp);
		}

		fclose (fp);
	}
	*/
	for (int i = 0; i < v.GetSize (); i++) {
		CString str = v [i];

		str.TrimLeft ();
		str.TrimRight ();

		if (!str.GetLength ()) {
			if (p) {
				m_lv.InsertCtrlData (p);
				p = NULL;
			}
		}
		else {
			if (!p)
				p = new CDriverItem;

			int nIndex = str.Find (_T (":"));

			if (nIndex != -1) {
				CString strLeft		= str.Left (nIndex);
				CString strRight	= str.Mid (nIndex + 1);

				strLeft.TrimLeft ();	strLeft.TrimRight ();
				strRight.TrimLeft ();	strRight.TrimRight ();

				if (!strLeft.CompareNoCase (_T ("Class")))			p->m_strClass		= strRight;
				if (!strLeft.CompareNoCase (_T ("ClassGuid")))		p->m_strClassGuid	= strRight;
				if (!strLeft.CompareNoCase (_T ("Provider")))		p->m_strProvider	= strRight;
				if (!strLeft.CompareNoCase (_T ("CatalogFile")))	p->m_strCatalogFile = strRight;
				if (!strLeft.CompareNoCase (_T ("DriverVer")))		p->m_strDriverVer	= strRight;
				if (!strLeft.CompareNoCase (_T ("Inf")))			p->m_strInf			= strRight;
			}
		}
	}
	
	m_lv->SortItems (m_lv.CompareFunc, (DWORD)2);
	EndWaitCursor ();
}
