// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__B65D4A6E_B219_4315_A1C4_102928209B50__INCLUDED_)
#define AFX_STDAFX_H__B65D4A6E_B219_4315_A1C4_102928209B50__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include "PrintMonitor.h"
#include "Version.h"

#define NM_SYSTRAY					(WM_APP+1)
#define WM_DATA						(WM_APP+2)
#define WM_CONFIGSTATUS_BYINDEX		(WM_APP+3)
#define WM_CONFIGSTATUS_BYID		(WM_APP+4)
#define WM_NEXT_STATUS				(WM_APP+5)
#define WM_HEAD_DELETED				(WM_APP+6)
#define WM_HEAD_CHANGED				(WM_APP+7)
#define WM_THREAD_INIT				(WM_APP+8)
#define WM_HEAD_CONNECT_FAILED		(WM_APP+9)
#define WM_LOAD_HEAD_RECORD			(WM_APP+10)
#define WM_GET_LOCAL_CONFIG			(WM_APP+11)
#define WM_IPC_TOSTRING				(WM_APP+12)
#define WM_HEAD_RECONNECT			(WM_APP+13)

extern LPCTSTR lpszDriverKey;

typedef struct tagPORTSTRUCT
{
	tagPORTSTRUCT (HWND hwnd, const CString & strPort) : m_hWnd (hwnd), m_strPort (strPort) { }

	HWND m_hWnd;
	CString m_strPort;
} PORTSTRUCT;

CString LoadString (UINT nID);
FoxjetCommon::CVersion GetVersion (const CString & strFile);
bool IsPrinterPortInstalled(const CString & str);
void AddPrinterPort (const PORTSTRUCT & port);
void UpdatePrinterPorts (HWND hwnd, FoxjetDatabase::COdbcDatabase & db);

extern CPrintMonitorApp theApp;
extern const FoxjetCommon::CVersion verNull;

#define REPAINT(wnd) { (wnd)->Invalidate (FALSE); (wnd)->RedrawWindow (NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW); /* (wnd)->RedrawWindow (); */ /* (wnd)->UpdateWindow (); */ }

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__B65D4A6E_B219_4315_A1C4_102928209B50__INCLUDED_)
