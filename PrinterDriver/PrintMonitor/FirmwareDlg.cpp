// FirmwareDlg.cpp : implementation file
//

#include "stdafx.h"
#include <afxinet.h>
#include "..\..\IP\Marksman\appconf.h"
#include "PrintMonitor.h"
#include "FirmwareDlg.h"
#include "Extern.h"
#include "Registry.h"
#include "FileExt.h"
#include "Debug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ListGlobals;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;

static LPCTSTR lpszFirmwareKey	= _T ("Firmware");
static LPCTSTR lpszGaKey		= _T ("GA");

static LPCTSTR lpszFirmwareExt	= _T ("*.bin");
static LPCTSTR lpszGaExt		= _T ("*.ga");

/////////////////////////////////////////////////////////////////////////////
// CFirmwareDlg dialog


CFirmwareDlg::CFirmwareDlg(CWnd* pParent /*=NULL*/)
:	m_nHead (0),
	CDialog(CFirmwareDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFirmwareDlg)
	m_strGaVer = _T("");
	m_strName = _T("");
	m_strSwVer = _T("");
	//}}AFX_DATA_INIT
}


void CFirmwareDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFirmwareDlg)
	DDX_Text(pDX, TXT_GAVER, m_strGaVer);
	DDX_Text(pDX, TXT_NAME, m_strName);
	DDX_Text(pDX, TXT_FIRMWAREVER, m_strSwVer);
	//}}AFX_DATA_MAP
	DDX_Text(pDX, TXT_ADDR, m_strAddr);
}


BEGIN_MESSAGE_MAP(CFirmwareDlg, CDialog)
	//{{AFX_MSG_MAP(CFirmwareDlg)
	ON_BN_CLICKED(BTN_BROWSEFIRMWARE, OnBrowsefirmware)
	ON_BN_CLICKED(BTN_BROWSEGA, OnBrowsega)
	ON_BN_CLICKED(BTN_UPDATEFIRMWARE, OnUpdatefirmware)
	ON_BN_CLICKED(BTN_UPDATEGA, OnUpdatega)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFirmwareDlg message handlers

BOOL CFirmwareDlg::OnInitDialog() 
{
	CString strSection = defElements.m_strRegSection + _T ("\\CFirmwareDlg");

	CDialog::OnInitDialog();
	
	SetDlgItemText (TXT_FIRMWAREPATH, FoxjetDatabase::GetProfileString (strSection, ::lpszFirmwareKey, GetHomeDir () + _T ("\\image-3.5012.bin")));
	SetDlgItemText (TXT_GAPATH, FoxjetDatabase::GetProfileString (strSection, ::lpszGaKey, GetHomeDir () + _T ("\\net.ga")));
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFirmwareDlg::OnBrowsefirmware() 
{
	CString str;

	GetDlgItemText (TXT_FIRMWAREPATH, str);

	CFileDialog dlg (true, ::lpszFirmwareExt, str, 
		OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST|OFN_READONLY|OFN_HIDEREADONLY, 
		"Firmware files (" + CString (::lpszFirmwareExt) + ")|" + CString (::lpszFirmwareExt) + "|");

	if (dlg.DoModal () == IDOK) {
		CString strSection = defElements.m_strRegSection + _T ("\\CFirmwareDlg");

		if (FoxjetFile::GetFileExt (dlg.GetPathName ()).CompareNoCase (_T ("bin")) != 0) {
			MessageBox (LoadString (IDS_INVALIDFILETYPE), NULL, MB_ICONERROR);
			return;
		}

		str = dlg.GetPathName ();
		SetDlgItemText (TXT_FIRMWAREPATH, str);
		FoxjetDatabase::WriteProfileString (strSection, lpszFirmwareKey, str);
	}
}

void CFirmwareDlg::OnBrowsega() 
{
	CString str;

	GetDlgItemText (TXT_GAPATH, str);

	CFileDialog dlg (true, ::lpszGaExt, str, 
		OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST|OFN_READONLY|OFN_HIDEREADONLY, 
		"Gate array files (" + CString (::lpszGaExt) + ")|" + CString (::lpszGaExt) + "|");

	if (dlg.DoModal () == IDOK) {
		CString strSection = defElements.m_strRegSection + _T ("\\CFirmwareDlg");
		
		if (FoxjetFile::GetFileExt (dlg.GetPathName ()).CompareNoCase (_T ("ga")) != 0) {
			MessageBox (LoadString (IDS_INVALIDFILETYPE), NULL, MB_ICONERROR);
			return;
		}

		str = dlg.GetPathName ();
		SetDlgItemText (TXT_GAPATH, str);
		FoxjetDatabase::WriteProfileString (strSection, lpszGaKey, str);
	}
}

void CFirmwareDlg::OnUpdatefirmware() 
{
	CString strServer = m_strAddr;
	CInternetSession session (_T ("DigiNET firmware upload"));
	const CString strUser = _T ("root");
	const CString strPass = _T (APP_ROOT_PASSWORD);

	BeginWaitCursor ();

	while (!::PostMessage (::AfxGetMainWnd ()->m_hWnd, WM_CONFIGSTATUS_BYINDEX, (WPARAM)m_nHead, (LPARAM)new CString (LoadString (IDS_ATTEMPTINGCONNECTION))))
		::Sleep (0);

	if (CFtpConnection * pFTP = session.GetFtpConnection (strServer, strUser, strPass)) {
		CString strLocal;
		const CString strRemote = _T ("image.bin");

		GetDlgItemText (TXT_FIRMWAREPATH, strLocal);

		TRACEF (_T ("FTP [") + strServer + _T (", ") + strUser + _T (", ") + strPass + _T ("]: ") + strLocal + _T (" --> ") + strRemote);

		BOOL bPutFile = pFTP->PutFile (strLocal, strRemote, FTP_TRANSFER_TYPE_BINARY);

		pFTP->Close ();

		if (!bPutFile) {
			CString str = LoadString (IDS_FIRMWARE_UPDATE_FAILED);

			while (!::PostMessage (::AfxGetMainWnd ()->m_hWnd, WM_CONFIGSTATUS_BYINDEX, (WPARAM)m_nHead, (LPARAM)new CString (str)))
				::Sleep (0);
			
			AfxMessageBox (str, MB_OK | MB_ICONERROR);
		}
		else {
			while (!::PostMessage (::AfxGetMainWnd ()->m_hWnd, WM_HEAD_RECONNECT, (WPARAM)m_nHead, 0))
				::Sleep (0);

			while (!::PostMessage (::AfxGetMainWnd ()->m_hWnd, WM_CONFIGSTATUS_BYINDEX, (WPARAM)m_nHead, (LPARAM)new CString (LoadString (IDS_FIRMWAREUPLOADCOMPLETE))))
				::Sleep (0);
		}

		delete pFTP;
	}

	EndWaitCursor ();
}

void CFirmwareDlg::OnUpdatega() 
{
	CString strServer = m_strAddr;
	CInternetSession session (_T ("DigiNET firmware upload"));
	const CString strUser = _T ("root");
	const CString strPass = _T (APP_ROOT_PASSWORD);

	BeginWaitCursor ();

	while (!::PostMessage (::AfxGetMainWnd ()->m_hWnd, WM_CONFIGSTATUS_BYINDEX, (WPARAM)m_nHead, (LPARAM)new CString (LoadString (IDS_ATTEMPTINGCONNECTION))))
		::Sleep (0);

	if (CFtpConnection * pFTP = session.GetFtpConnection (strServer, strUser, strPass)) {
		CString strLocal;
		const CString strRemote = _T ("firmware/ga.bin");

		GetDlgItemText (TXT_GAPATH, strLocal);

		TRACEF (_T ("FTP [") + strServer + _T (", ") + strUser + _T (", ") + strPass + _T ("]: ") + strLocal + _T (" --> ") + strRemote);

		BOOL bPutFile = pFTP->PutFile (strLocal, strRemote, FTP_TRANSFER_TYPE_BINARY);

		pFTP->Close ();

		if (!bPutFile) {
			CString str = LoadString (IDS_GA_UPDATE_FAILED);

			while (!::PostMessage (::AfxGetMainWnd ()->m_hWnd, WM_CONFIGSTATUS_BYINDEX, (WPARAM)m_nHead, (LPARAM)new CString (str)))
				::Sleep (0);
			
			AfxMessageBox (str, MB_OK | MB_ICONERROR);
		}
		else {
			while (!::PostMessage (::AfxGetMainWnd ()->m_hWnd, WM_CONFIGSTATUS_BYINDEX, (WPARAM)m_nHead, (LPARAM)new CString (LoadString (IDS_GAUPLOADCOMPLETE))))
				::Sleep (0);
		}

		delete pFTP;
	}

	EndWaitCursor ();
}
