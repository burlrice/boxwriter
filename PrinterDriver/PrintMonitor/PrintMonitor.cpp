// PrintMonitor.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include <Winspool.h>
#include <shlwapi.h>
#include "PrintMonitor.h"
#include "PrintMonitorDlg.h"
#include "..\NEXT\src\Broadcast.h"
#include "Debug.h"
#include "Database.h"
#include "List.h"
#include "Extern.h"
#include "Parse.h"
#include "Registry.h"
#include "HeadDlg.h"
#include "DriverDlg.h"
#include "FieldDefs.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace Foxjet3d;

const CString CPrintMonitorApp::m_strUpdate = _T ("send config");


/////////////////////////////////////////////////////////////////////////////
// CPrintMonitorApp

BEGIN_MESSAGE_MAP(CPrintMonitorApp, CWinApp)
	//{{AFX_MSG_MAP(CPrintMonitorApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintMonitorApp construction


CPrintMonitorApp::CPrintMonitorApp()
:	m_bXP (false),
	m_strDSN (FoxjetCommon::GetDSN ())
{
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CPrintMonitorApp object

CPrintMonitorApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CPrintMonitorApp initialization

BOOL CPrintMonitorApp::InitInstance()
{
	if (!CInstance::Elevate ())
		return FALSE;

	using namespace FoxjetDatabase;

	{
		OSVERSIONINFO os;

		memset (&os, 0, sizeof (os));
		os.dwOSVersionInfoSize = sizeof (os);
		
		if (::GetVersionEx (&os)) {
			CString str;
	
			str.Format (_T ("%d.%d.%d.%d %s"), 
				os.dwMajorVersion,
				os.dwMinorVersion,
				os.dwBuildNumber,
				os.dwPlatformId,
				os.szCSDVersion);
			TRACEF (str);

			m_bXP = os.dwMajorVersion <= 5 ? true : false;
		}
	}

	FoxjetDatabase::SetHighResDisplay (true);
	AfxEnableControlContainer();
	SetRegistryKey (_T ("Foxjet\\Print Monitor"));

	CStringArray v;
	
	TokenizeCmdLine (::GetCommandLine (), v);

	for (int i = 0; i < v.GetSize (); i++) {
		CString strTmp, str = v [i];

		TRACEF (str);

		if (!str.CompareNoCase (_T ("/CLEANREGISTRY"))) {
			::SHDeleteKey (HKEY_CURRENT_USER, _T ("Software\\FoxJet\\Print Monitor"));

			return TRUE;
		}

		strTmp = Extract (str, _T ("/delete=\""), _T ("\""));
		
		if (strTmp.GetLength ()) { 
			DWORD dwSize = 256;
			TCHAR szUser [256] = { 0 };

			::DeleteFile (strTmp);
			::OutputDebugString (_T ("delete: ") + strTmp + _T ("\n"));

			// this is a hack.  installer creates shortcuts under
			// "all users", uninstall.exe attempts to delete using current username

			::GetUserName (szUser, &dwSize);
			strTmp.Replace (szUser, _T ("All Users"));

			::DeleteFile (strTmp);
			::OutputDebugString (_T ("delete: ") + strTmp + _T ("\n"));

			return TRUE;
		}

		strTmp = Extract (str, _T ("/dsn="));	if (strTmp.GetLength ()) { m_strDSN = strTmp; continue; }
		strTmp = Extract (str, _T ("/user="));	if (strTmp.GetLength ()) { m_strUser = strTmp; continue; }
		strTmp = Extract (str, _T ("/pass="));	if (strTmp.GetLength ()) { m_strPass = strTmp; continue; }
	}

	#ifdef _DEBUG
	{
		CStringArray v;

		GetFiles (GetHomeDir (), _T ("fjmknext.dll"), v);

		if (v.GetSize () > 0) {
			CVersion verFirst = GetVersion (v [0]);

			for (int i = 0; i < v.GetSize (); i++) {
				CVersion verNext = GetVersion (v [i]);

				//TRACEF (verNext.Format () + _T (": ") + v [i]);
				ASSERT ((VERSIONSTRUCT)verFirst == (VERSIONSTRUCT)verNext);
			}
		}
	}
	#endif

	CString strOld = GetPrintDriverDirectory () + _T ("\\fjmknext.dll");
	CString strNew = GetHomeDir () + _T ("\\PrintDriver\\fjmknext\\x86\\fjmknext.dll");
	CVersion verNew = GetVersion (strNew);
	CVersion verOld = GetVersion (strOld);

	if (verNew == ::verNull) {
		TRACEF (_T ("no version found: ") + strNew);
		ASSERT (0);
	}

	if (verNew != ::verNull && verOld != ::verNull) {
		if (verNew > verOld) {
			CString str;

			str.Format (
				_T ("%s\n\n")
				_T ("%s\n[%d.%d.%d.%d]\n\n")
				_T ("%s\n[%d.%d.%d.%d]"),
				LoadString (IDS_VERSIONOUTOFDATE),
				strNew, verNew.GetMajor (), verNew.GetMinor (), verNew.GetCustomMajor (), verNew.GetInternal (),
				strOld, verOld.GetMajor (), verOld.GetMinor (), verOld.GetCustomMajor (), verOld.GetInternal ());

			::AfxMessageBox (str);

			/*
			if (::AfxMessageBox (str, MB_YESNO) == IDYES) {
				if (!::CopyFile (strNew, strOld, FALSE)) {
					str.Format (_T ("%s:\n%s\n"), LoadString (IDS_FAILEDTOCOPY), strNew, strOld);
					::AfxMessageBox (str);
				}
			}
			*/
		}
	}

	{
		CString strDir = GetHomeDir () + _T ("\\preview");

		if (::GetFileAttributes (strDir) == -1) 
			::CreateDirectory (strDir, NULL);

		FoxjetDatabase::WriteProfileString (::lpszDriverKey, _T ("Dir"), strDir);
	}

	// Standard initialization

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CString strName = GetDatabaseName ();

	if (!m_db.IsOpen ()) {
		if (!FoxjetCommon::OpenDatabase (m_db, m_strDSN, strName)) {
			CString strPath, strTitle;

			FoxjetDatabase::GetBackupPath (strPath, strTitle);

			::MessageBox (NULL, 
				LoadString (IDS_FAILEDTOINITIALIZEDB) + _T ("\nCheck: ") + strPath + '\\' + strTitle, 
				::AfxGetAppName (), MB_ICONERROR | MB_OK);
			return FALSE;
		}
	}

	ListGlobals::SetDB (m_db);
	InitHeadNames ();

	CPrintMonitorDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();

	m_db.Close ();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


void CPrintMonitorApp::InitHeadNames ()
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

	GetHeadRecords (theApp.m_db, vHeads);

	{
		DWORD dwNeeded = 0, dwReturned = 0;

		::EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 2, NULL, 0, &dwNeeded, &dwReturned);

		if (dwNeeded > 0) {
			PRINTER_INFO_2 * p = (PRINTER_INFO_2 *)new BYTE [dwNeeded] ;//new PRINTER_INFO_2 [nCount];

			if (::EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 2, (LPBYTE)p, dwNeeded, &dwNeeded, &dwReturned)) {
				for (DWORD i = 0; i < dwReturned; i++) {

					TRACEF (p [i].pDriverName + CString (_T (": ")) + p [i].pPrinterName + CString (_T (": ")) + p [i].pPortName);

					if (!_tcscmp (_T ("Foxjet Marksman NEXT"), p [i].pDriverName)) {
						bool bFound = false;

						for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
							HEADSTRUCT & h = vHeads [nHead];

							if (!h.m_strUID.CompareNoCase (p [i].pPortName)) {
								h.m_strName = p [i].pPrinterName;
								UpdateHeadRecord (theApp.m_db, h);
								bFound = true;
								break;
							}
						}

						if (!bFound) {
							HEADSTRUCT newHead;

							if (vHeads.GetSize ()) {
								newHead = vHeads [0];
								newHead.m_bMasterHead		= false;
							}
							else {
								LINESTRUCT line;
								CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;

								GetFirstLineRecord (theApp.m_db, line, GetPrinterID (theApp.m_db));
								GetPanelRecords (theApp.m_db, line.m_lID, vPanels);

								newHead.m_lPanelID			= vPanels.GetSize () ? vPanels [0].m_lID : -1;
								newHead.m_nHorzRes			= 300;
								newHead.m_nChannels			= 32;
								newHead.m_lPhotocellDelay	= 1000;
								newHead.m_lRelativeHeight	= 1000;
								newHead.m_nDirection		= LTOR;
								newHead.m_dHeadAngle		= 90.0;
								newHead.m_bInverted			= FALSE;
								newHead.m_nPhotocell		= EXTERNAL_PC;
								newHead.m_nSharePhoto		= SHARED_PCNONE;
								newHead.m_nEncoder			= EXTERNAL_ENC;
								newHead.m_nShareEnc			= SHARED_ENCNONE;
								newHead.m_nHeadType			= GRAPHICS_768_256;
								newHead.m_bRemoteHead		= FALSE;
								newHead.m_bMasterHead		= false;
								newHead.m_bEnabled			= true;
								newHead.m_nIntTachSpeed		= 60;
								newHead.m_dNozzleSpan		= CHeadDlg::GetNozzleSpan (newHead.m_nHeadType);
								newHead.m_nEncoderDivisor	= 2;
								newHead.m_dPhotocellRate	= 36.0;
								newHead.m_strSerialParams	= _T ("38400,0");
								newHead.m_bDigiNET			= true;
							}

							newHead.m_strName = p [i].pPrinterName;
							newHead.m_strUID = p [i].pPortName;

							VERIFY (AddHeadRecord (theApp.m_db, newHead));
							vHeads.Add (newHead);
						}
					}
				}
			}
		}
	}
}

void CPrintMonitorApp::UpdateRegistry ()
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

	//InitHeadNames ();

	try {
		using namespace FoxjetDatabase::Heads;

		CString str;

		str.Format (_T ("UPDATE [%s] SET [%s].[%s]=False;"), m_lpszTable, m_lpszTable, m_lpszMasterHead);
		TRACEF (str);
		m_db.ExecuteSQL (str);
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	GetHeadRecords (m_db, vHeads);
    
	::RegDeleteKey (HKEY_CURRENT_USER, CPrintMonitorDlg::GetKey (-1));
	::RegDeleteKey (HKEY_CURRENT_USER, CPrintMonitorDlg::GetKey (0));

	for (int i = 0; i < MAX_HEADS; i++) {
		CString str = CPrintMonitorDlg::GetKey (i + 1);

		::RegDeleteKey (HKEY_CURRENT_USER, str);
	}

	for (i = 0; i < vHeads.GetSize (); i++) {
		HEADSTRUCT & head = vHeads [i];
		int nHead = i + 1;
		CString str = CPrintMonitorDlg::GetKey (nHead);

		head.WriteProfile (HKEY_CURRENT_USER, str, -1);
		FoxjetDatabase::WriteProfileInt (str, m_strUpdate, 1);

		{
			CString strPrinter;

			if (head.m_lLinkedTo > 0) {
				DWORD dwNeeded = 0, dwReturned = 0;
				HEADSTRUCT h;

				if (GetHeadRecord (m_db, head.m_lLinkedTo, h)) {
					::EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 2, NULL, 0, &dwNeeded, &dwReturned);

					if (dwNeeded > 0) {
						PRINTER_INFO_2 * p = (PRINTER_INFO_2 *)new BYTE [dwNeeded]; //new PRINTER_INFO_2 [nCount];

						if (::EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 2, (LPBYTE)p, dwNeeded, &dwNeeded, &dwReturned)) {
							for (DWORD i = 0; i < dwReturned; i++) {

								TRACEF (p [i].pDriverName + CString (_T (": ")) + p [i].pPrinterName + CString (_T (": ")) + p [i].pPortName);
								
								if (!_tcscmp (_T ("Foxjet Marksman NEXT"), p [i].pDriverName) && !_tcscmp (h.m_strUID, p [i].pPortName)) {
									strPrinter = p [i].pPrinterName;
									break;
								}
							}
						}

						delete [] p;
					}
				}
			}

			//TRACEF (head.m_strName + _T (" --> ") + strPrinter);
			FoxjetDatabase::WriteProfileString (str, _T ("spawn"), strPrinter);
		}
	}
}

