// PreviewDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "PrintMonitor.h"
#include "PreviewDlg.h"
#include "ximage.h"
#include "Color.h"
#include "Debug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPreviewDlg dialog


CPreviewDlg::CPreviewDlg(CWnd* pParent /*=NULL*/)
:	m_scroll (0, 0),
	CDialog(CPreviewDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPreviewDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CPreviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPreviewDlg)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPreviewDlg, CDialog)
	//{{AFX_MSG_MAP(CPreviewDlg)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPreviewDlg message handlers

void CPreviewDlg::OnPaint() 
{
	CPaintDC dc (this);

	if (m_bmp.m_hObject) {
		CDC dcMem;
		CBitmap bmp;
		DIBSECTION ds;
		int x = -m_scroll.cx; //-GetScrollPos (SB_HORZ);
		int y = -m_scroll.cy; //-GetScrollPos (SB_VERT);

		memset (&ds, 0, sizeof (ds));
		m_bmp.GetObject (sizeof (ds), &ds);
		dcMem.CreateCompatibleDC (&dc);

		CBitmap * pBmp = dcMem.SelectObject (&m_bmp);
		dc.BitBlt (x, y, ds.dsBm.bmWidth, ds.dsBm.bmHeight, &dcMem, 0, 0, SRCCOPY);
		dcMem.SelectObject (pBmp);
	}
}

BOOL CPreviewDlg::OnInitDialog() 
{
	CxImage img;

	if (img.Load (m_strFile)) 
		m_bmp.Attach (img.MakeBitmap ());

	CDialog::OnInitDialog();
	SetWindowText (m_strFile);
	
	{
		CRect rc;

		GetWindowRect (rc);
		SetWindowPos (NULL, 0, 0, rc.Width () + 1, rc.Height (), SWP_NOZORDER | SWP_NOMOVE | SWP_SHOWWINDOW);
		SetWindowPos (NULL, 0, 0, rc.Width (), rc.Height (), SWP_NOZORDER | SWP_NOMOVE | SWP_SHOWWINDOW);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPreviewDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if (m_bmp.m_hObject) {
		DIBSECTION ds;
		SCROLLINFO si;
		CSize scroll (GetScrollPos (SB_HORZ), GetScrollPos (SB_VERT));

		memset (&ds, 0, sizeof (ds));
		m_bmp.GetObject (sizeof (ds), &ds);

		si.cbSize = sizeof(SCROLLINFO);
		si.fMask = SIF_ALL;
		si.nMin = 0;

		si.nMax = (cy < ds.dsBm.bmHeight) ? ds.dsBm.bmHeight - cy : 0;
		si.nPage = si.nMax / 10;
		si.nPos = scroll.cx;
        SetScrollInfo(SB_VERT, &si, TRUE); 

		si.nMax = (cx < ds.dsBm.bmWidth) ? ds.dsBm.bmWidth - cy : 0;
		si.nPage = si.nMax / 10;
		si.nPos = scroll.cy;
        SetScrollInfo(SB_HORZ, &si, TRUE); 

		REPAINT (this);

		SetScrollPos (SB_HORZ, scroll.cx);
		SetScrollPos (SB_VERT, scroll.cy);
	}
}

void CPreviewDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	if (m_bmp.m_hObject) {
		DIBSECTION ds;
		CRect rcWin;
		int nDelta;

		GetWindowRect (rcWin);
		memset (&ds, 0, sizeof (ds));
		m_bmp.GetObject (sizeof (ds), &ds);

		CRect rc (0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight);
		int nMaxPos = rc.Height() - rcWin.Height ();

		switch (nSBCode)
		{
		case SB_LINEDOWN:
			if (m_scroll.cy >= nMaxPos)
				return;
			nDelta = min(nMaxPos/100,nMaxPos-m_scroll.cy);
			break;
		case SB_LINEUP:
			if (m_scroll.cy <= 0)
				return;
			nDelta = -min(nMaxPos/100,m_scroll.cy);
			break;
			 case SB_PAGEDOWN:
			if (m_scroll.cy >= nMaxPos)
				return;
			nDelta = min(nMaxPos/10,nMaxPos-m_scroll.cy);
			break;
		case SB_THUMBPOSITION:
			nDelta = (int)nPos - m_scroll.cy;
			break;
		case SB_PAGEUP:
			if (m_scroll.cy <= 0)
				return;
			nDelta = -min(nMaxPos/10,m_scroll.cy);
			break;
		case SB_THUMBTRACK:
			m_scroll.cy = nPos;
			REPAINT (this);
			return;
		default:
			return;
		}

		m_scroll.cy += nDelta;
		SetScrollPos (SB_VERT, m_scroll.cy, TRUE);
		ScrollWindow (0, -nDelta);
		REPAINT (this);
	}

	CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CPreviewDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	if (m_bmp.m_hObject) {
		DIBSECTION ds;
		CRect rcWin;
		int nDelta;

		GetWindowRect (rcWin);
		memset (&ds, 0, sizeof (ds));
		m_bmp.GetObject (sizeof (ds), &ds);

		CRect rc (0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight);
		int nMaxPos = rc.Width () - rcWin.Width ();

		switch (nSBCode)
		{
		case SB_LINEDOWN:
			if (m_scroll.cx >= nMaxPos)
				return;
			nDelta = min(nMaxPos/100,nMaxPos-m_scroll.cx);
			break;
		case SB_LINEUP:
			if (m_scroll.cx <= 0)
				return;
			nDelta = -min(nMaxPos/100,m_scroll.cx);
			break;

			 case SB_PAGEDOWN:
			if (m_scroll.cx >= nMaxPos)
				return;
			nDelta = min(nMaxPos/10,nMaxPos-m_scroll.cx);
			break;
		case SB_THUMBPOSITION:
			nDelta = (int)nPos - m_scroll.cx;
			break;
		case SB_PAGEUP:
			if (m_scroll.cx <= 0)
				return;
			nDelta = -min(nMaxPos/10,m_scroll.cx);
			break;
		case SB_THUMBTRACK:
			m_scroll.cx = nPos;
			REPAINT (this);
			return;
		default:
			return;
		}

		m_scroll.cx += nDelta;
		SetScrollPos (SB_HORZ, m_scroll.cx, TRUE);
		ScrollWindow (-nDelta, 0);
		REPAINT (this);
	}

	CDialog::OnHScroll (nSBCode, nPos, pScrollBar);
}
