// PrintMonitor.h : main header file for the PRINTMONITOR application
//

#if !defined(AFX_PRINTMONITOR_H__3362D34E_A00D_44D2_9C70_57470973982E__INCLUDED_)
#define AFX_PRINTMONITOR_H__3362D34E_A00D_44D2_9C70_57470973982E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "OdbcDatabase.h"

/////////////////////////////////////////////////////////////////////////////
// CPrintMonitorApp:
// See PrintMonitor.cpp for the implementation of this class
//

class CPrintMonitorApp : public CWinApp
{
public:
	CPrintMonitorApp();

	bool IsXP () const { return m_bXP; }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintMonitorApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

	FoxjetDatabase::COdbcDatabase m_db;
	CString m_strDSN;
	CString m_strUser;
	CString m_strPass;
	
	void UpdateRegistry ();
	void InitHeadNames ();

	static const CString m_strUpdate;

// Implementation

	//{{AFX_MSG(CPrintMonitorApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


protected:
	bool m_bXP;
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTMONITOR_H__3362D34E_A00D_44D2_9C70_57470973982E__INCLUDED_)
