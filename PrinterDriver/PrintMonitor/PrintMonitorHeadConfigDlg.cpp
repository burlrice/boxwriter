// PrintMonitorHeadConfigDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PrintMonitor.h"
#include "PrintMonitorHeadConfigDlg.h"
#include "resource.h"
#include "Debug.h"
#include "PrintMonitorDlg.h"

#define BTN_SYNC	4		// from Editor\DLL\3d\resource.h
#define CB_LINE		3001	// from Editor\DLL\3d\resource.h
#define LBL_LINE	3116	// from Editor\DLL\3d\resource.h

using namespace Foxjet3d;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrintMonitorHeadConfigDlg dialog


CPrintMonitorHeadConfigDlg::CPrintMonitorHeadConfigDlg(FoxjetDatabase::COdbcDatabase & db, UNITS units, CWnd* pParent)
:	CHeadConfigDlg (db, units, -1, pParent)
{
	//{{AFX_DATA_INIT(CPrintMonitorHeadConfigDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_bSync = false;
}


void CPrintMonitorHeadConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CHeadConfigDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrintMonitorHeadConfigDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPrintMonitorHeadConfigDlg, CHeadConfigDlg)
	//{{AFX_MSG_MAP(CPrintMonitorHeadConfigDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintMonitorHeadConfigDlg message handlers

void CPrintMonitorHeadConfigDlg::InitTreeCtrl ()
{
	CTreeCtrl & tree = GetTreeCtrl ();
	CArray <HEADSTRUCT, HEADSTRUCT &> v;

	tree.DeleteAllItems ();
	GetHeadRecords (theApp.m_db, v);

	for (int i = 0; i < v.GetSize (); i++) {
		HEADSTRUCT & h = v [i];

		//TRACEF (h.m_strUID);
		InsertItem (h, NULL);
	}

	UINT n [] = 
	{
		BTN_SYNC,
		CB_LINE,
		LBL_LINE,
	};

	for (i = 0; i < ARRAYSIZE (n); i++)
		if (CWnd * p = GetDlgItem (n [i]))
			p->ShowWindow (SW_HIDE);
}

void CPrintMonitorHeadConfigDlg::OnOK ()
{
	CStringArray vAddress;
	bool bValve = ISVALVE ();

	m_bAddressConflict = IsAddressConflict (vAddress);

	if (vAddress.GetSize ()) {
		CString str = LoadString (IDS_ADDRESSCONFLICT);

		for (int i = 0; i < vAddress.GetSize (); i++) 
			str += vAddress [i] + _T ("\n");

		MessageBox (str, NULL, MB_OK | MB_ICONINFORMATION);
		return;
	}

	CDialog::OnOK ();
}

