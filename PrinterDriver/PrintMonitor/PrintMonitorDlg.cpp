// PrintMonitorDlg.cpp : implementation file
//

#include "stdafx.h"
#include <afxsock.h>
#include <Winspool.h>
#include <io.h>
#include <iostream>
#include <fstream>
#include <string>
#include "PrintMonitor.h"
#include "PrintMonitorDlg.h"
#include "Debug.h"
#include "Utils.h"
#include "..\NEXT\src\Broadcast.h"
#include "Registry.h"
#include "AnsiString.h"
#include "Parse.h"
#include "AboutDlg.h"
#include "PrintMonitorHeadConfigDlg.h"
#include "HeadDlg.h"
#include "resource.h"
#include "..\..\IP\Marksman\fj_socket.h"
#include "DriverDlg.h"
#include "FirmwareDlg.h"
#include "PreviewDlg.h"
#include "SelectPrinter.h"
#include "DriverStoreDlg.h"

#define TM_INIT				(WM_USER+1)
#define TM_CONFIG_CHANGE	(WM_USER+2)
#define TM_RECONNECT		(WM_USER+3)


using namespace ItiLibrary;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;

using CPrintMonitorDlg::CPrinterItem;

////////////////////////////////////////////////////////////////////////////////////////////////////
//

#define DEBUGIPC(c, s) DebugIpcCmd ((c), (s), _T (__FILE__), __LINE__);

static const DWORD dwSleep = 25;//250;

struct 
{
	DWORD	m_dw;
	LPCTSTR m_lpsz;
} static const map [] =
{
	{ IPC_COMPLETE,						_T ("IPC_COMPLETE"),					},
	{ IPC_CONNECTION_CLOSE,				_T ("IPC_CONNECTION_CLOSE"),			},
	{ IPC_INVALID,						_T ("IPC_INVALID"),						},
	{ IPC_ECHO,							_T ("IPC_ECHO"),						},
	{ IPC_STATUS,						_T ("IPC_STATUS"),						},
	{ IPC_GET_STATUS,					_T ("IPC_GET_STATUS"),					},
	{ IPC_PHOTO_TRIGGER,				_T ("IPC_PHOTO_TRIGGER"),				},
	{ IPC_EDIT_STATUS,					_T ("IPC_EDIT_STATUS"),					},
	{ IPC_GET_EDIT_STATUS,				_T ("IPC_GET_EDIT_STATUS"),				},
	{ IPC_EDIT_START,					_T ("IPC_EDIT_START"),					},
	{ IPC_EDIT_SAVE,					_T ("IPC_EDIT_SAVE"),					},
	{ IPC_EDIT_CANCEL,					_T ("IPC_EDIT_CANCEL"),					},
	{ IPC_IMAGE_HEADER,					_T ("IPC_IMAGE_HEADER"),				},
	{ IPC_IMAGE_DATA,					_T ("IPC_IMAGE_DATA"),					},
	{ IPC_GET_LABEL_IDS,				_T ("IPC_GET_LABEL_IDS"),				},			
	{ IPC_SET_LABEL_IDS,				_T ("IPC_SET_LABEL_IDS"),				},			
	{ IPC_GET_LABELS,					_T ("IPC_GET_LABELS"),					},			
	{ IPC_GET_LABEL,					_T ("IPC_GET_LABEL"),					},			
	{ IPC_PUT_LABEL,					_T ("IPC_PUT_LABEL"),					},			
	{ IPC_DELETE_LABEL,					_T ("IPC_DELETE_LABEL"),				},			
	{ IPC_SELECT_LABEL,					_T ("IPC_SELECT_LABEL"),				},			
	{ IPC_GET_LABEL_ID_SELECTED,		_T ("IPC_GET_LABEL_ID_SELECTED"),		},			
	{ IPC_SET_LABEL_ID_SELECTED,		_T ("IPC_SET_LABEL_ID_SELECTED"),		},			
	{ IPC_DELETE_ALL_LABELS,			_T ("IPC_DELETE_ALL_LABELS"),			},			
	{ IPC_GET_MSG_IDS,					_T ("IPC_GET_MSG_IDS"),					},			
	{ IPC_SET_MSG_IDS,					_T ("IPC_SET_MSG_IDS"),					},			
	{ IPC_GET_MSGS,						_T ("IPC_GET_MSGS"),					},			
	{ IPC_GET_MSG,						_T ("IPC_GET_MSG"),						},			
	{ IPC_PUT_MSG,						_T ("IPC_PUT_MSG"),						},			
	{ IPC_DELETE_MSG,					_T ("IPC_DELETE_MSG"),					},			
	{ IPC_GET_TEXT_IDS,					_T ("IPC_GET_TEXT_IDS"),				},			
	{ IPC_SET_TEXT_IDS,					_T ("IPC_SET_TEXT_IDS"),				},			
	{ IPC_GET_TEXT_ELEMENTS,			_T ("IPC_GET_TEXT_ELEMENTS"),			},			
	{ IPC_GET_TEXT,						_T ("IPC_GET_TEXT"),					},			
	{ IPC_PUT_TEXT,						_T ("IPC_PUT_TEXT"),					},			
	{ IPC_DELETE_TEXT,					_T ("IPC_DELETE_TEXT"),					},			
	{ IPC_GET_BITMAP_IDS,				_T ("IPC_GET_BITMAP_IDS"),				},			
	{ IPC_SET_BITMAP_IDS,				_T ("IPC_SET_BITMAP_IDS"),				},			
	{ IPC_GET_BITMAP_ELEMENTS,			_T ("IPC_GET_BITMAP_ELEMENTS"),			},			
	{ IPC_GET_BITMAP,					_T ("IPC_GET_BITMAP"),					},			
	{ IPC_PUT_BITMAP,					_T ("IPC_PUT_BITMAP"),					},			
	{ IPC_DELETE_BITMAP,				_T ("IPC_DELETE_BITMAP"),				},			
	{ IPC_GET_DYNIMAGE_IDS,				_T ("IPC_GET_DYNIMAGE_IDS"),			},			
	{ IPC_SET_DYNIMAGE_IDS,				_T ("IPC_SET_DYNIMAGE_IDS"),			},			
	{ IPC_GET_DYNIMAGE_ELEMENTS,		_T ("IPC_GET_DYNIMAGE_ELEMENTS"),		},			
	{ IPC_GET_DYNIMAGE,					_T ("IPC_GET_DYNIMAGE"),				},			
	{ IPC_PUT_DYNIMAGE,					_T ("IPC_PUT_DYNIMAGE"),				},			
	{ IPC_DELETE_DYNIMAGE,				_T ("IPC_DELETE_DYNIMAGE"),				},			
	{ IPC_PUT_DYNIMAGE_DATA,			_T ("IPC_PUT_DYNIMAGE_DATA"),			},			
	{ IPC_GET_DATETIME_IDS,				_T ("IPC_GET_DATETIME_IDS"),			},			
	{ IPC_SET_DATETIME_IDS,				_T ("IPC_SET_DATETIME_IDS"),			},			
	{ IPC_GET_DATETIME_ELEMENTS,		_T ("IPC_GET_DATETIME_ELEMENTS"),		},			
	{ IPC_GET_DATETIME,					_T ("IPC_GET_DATETIME"),				},			
	{ IPC_PUT_DATETIME,					_T ("IPC_PUT_DATETIME"),				},			 
	{ IPC_DELETE_DATETIME,				_T ("IPC_DELETE_DATETIME"),				},			
	{ IPC_GET_COUNTER_IDS,				_T ("IPC_GET_COUNTER_IDS"),				},			
	{ IPC_SET_COUNTER_IDS,				_T ("IPC_SET_COUNTER_IDS"),				},			
	{ IPC_GET_COUNTER_ELEMENTS,			_T ("IPC_GET_COUNTER_ELEMENTS"),		},			
	{ IPC_GET_COUNTER,					_T ("IPC_GET_COUNTER"),					},			
	{ IPC_PUT_COUNTER,					_T ("IPC_PUT_COUNTER"),					},			
	{ IPC_DELETE_COUNTER,				_T ("IPC_DELETE_COUNTER"),				},			
	{ IPC_GET_BARCODE_IDS,				_T ("IPC_GET_BARCODE_IDS"),				},			
	{ IPC_SET_BARCODE_IDS,				_T ("IPC_SET_BARCODE_IDS"),				},			
	{ IPC_GET_BARCODE_ELEMENTS,			_T ("IPC_GET_BARCODE_ELEMENTS"),		},			
	{ IPC_GET_BARCODE,					_T ("IPC_GET_BARCODE"),					},			
	{ IPC_PUT_BARCODE,					_T ("IPC_PUT_BARCODE"),					},			
	{ IPC_DELETE_BARCODE,				_T ("IPC_DELETE_BARCODE"),				},			
	{ IPC_GET_FONT_IDS,					_T ("IPC_GET_FONT_IDS"),				},			
	{ IPC_SET_FONT_IDS,					_T ("IPC_SET_FONT_IDS"),				},			
	{ IPC_GET_FONTS,					_T ("IPC_GET_FONTS"),					},			
	{ IPC_GET_FONT,						_T ("IPC_GET_FONT"),					},			
	{ IPC_PUT_FONT,						_T ("IPC_PUT_FONT"),					},			
	{ IPC_DELETE_FONT,					_T ("IPC_DELETE_FONT"),					},			
	{ IPC_GET_BMP_IDS,					_T ("IPC_GET_BMP_IDS"),					},			
	{ IPC_SET_BMP_IDS,					_T ("IPC_SET_BMP_IDS"),					},			
	{ IPC_GET_BMPS,						_T ("IPC_GET_BMPS"),					},			
	{ IPC_GET_BMP,						_T ("IPC_GET_BMP"),						},			
	{ IPC_PUT_BMP,						_T ("IPC_PUT_BMP"),						},			
	{ IPC_DELETE_BMP,					_T ("IPC_DELETE_BMP"),					},			
	{ IPC_DELETE_ALL_BMPS,				_T ("IPC_DELETE_ALL_BMPS"),				},			
	{ IPC_GET_DYNTEXT_IDS,				_T ("IPC_GET_DYNTEXT_IDS"),				},			
	{ IPC_SET_DYNTEXT_IDS,				_T ("IPC_SET_DYNTEXT_IDS"),				},			
	{ IPC_GET_DYNTEXT_ELEMENTS,			_T ("IPC_GET_DYNTEXT_ELEMENTS"),		},			
	{ IPC_GET_DYNTEXT,					_T ("IPC_GET_DYNTEXT"),					},			
	{ IPC_PUT_DYNTEXT,					_T ("IPC_PUT_DYNTEXT"),					},			
	{ IPC_DELETE_DYNTEXT,				_T ("IPC_DELETE_DYNTEXT"),				},			
	{ IPC_GET_DYNBARCODE_IDS,			_T ("IPC_GET_DYNBARCODE_IDS"),			},			
	{ IPC_SET_DYNBARCODE_IDS,			_T ("IPC_SET_DYNBARCODE_IDS"),			},			
	{ IPC_GET_DYNBARCODE_ELEMENTS,		_T ("IPC_GET_DYNBARCODE_ELEMENTS"),		},			
	{ IPC_GET_DYNBARCODE,				_T ("IPC_GET_DYNBARCODE"),				},			
	{ IPC_PUT_DYNBARCODE,				_T ("IPC_PUT_DYNBARCODE"),				},			
	{ IPC_DELETE_DYNBARCODE,			_T ("IPC_DELETE_DYNBARCODE"),			},			
	{ IPC_GET_GROUP_INFO,				_T ("IPC_GET_GROUP_INFO"),				},			
	{ IPC_SET_GROUP_INFO,				_T ("IPC_SET_GROUP_INFO"),				},			
	{ IPC_GET_SYSTEM_INFO,				_T ("IPC_GET_SYSTEM_INFO"),				},			
	{ IPC_GET_SYSTEM_INTERNET_INFO,		_T ("IPC_GET_SYSTEM_INTERNET_INFO"),	},			
	{ IPC_GET_SYSTEM_TIME_INFO,			_T ("IPC_GET_SYSTEM_TIME_INFO"),		},			
	{ IPC_GET_SYSTEM_DATE_STRINGS,		_T ("IPC_GET_SYSTEM_DATE_STRINGS"),		},			
	{ IPC_GET_SYSTEM_PASSWORDS,			_T ("IPC_GET_SYSTEM_PASSWORDS"),		},			
	{ IPC_GET_SYSTEM_BARCODES,			_T ("IPC_GET_SYSTEM_BARCODES"),			},			
	{ IPC_SET_SYSTEM_INFO,				_T ("IPC_SET_SYSTEM_INFO"),				},			
	{ IPC_GET_PRINTER_ID,				_T ("IPC_GET_PRINTER_ID"),				},			
	{ IPC_GET_PRINTER_INFO,				_T ("IPC_GET_PRINTER_INFO"),			},			
	{ IPC_GET_PRINTER_PKG_INFO,			_T ("IPC_GET_PRINTER_PKG_INFO"),		},			
	{ IPC_GET_PRINTER_PHY_INFO,			_T ("IPC_GET_PRINTER_PHY_INFO"),		},			
	{ IPC_GET_PRINTER_OP_INFO,			_T ("IPC_GET_PRINTER_OP_INFO"),			},			
	{ IPC_SET_PRINTER_INFO,				_T ("IPC_SET_PRINTER_INFO"),			},			
	{ IPC_GET_PRINTER_ELEMENTS,			_T ("IPC_GET_PRINTER_ELEMENTS"),		},			
	{ IPC_PUT_PRINTER_ELEMENT,			_T ("IPC_PUT_PRINTER_ELEMENT"),			},			
	{ IPC_DELETE_ALL_PRINTER_ELEMENTS,	_T ("IPC_DELETE_ALL_PRINTER_ELEMENTS"),	},			
	{ IPC_FIRMWARE_UPGRADE,				_T ("IPC_FIRMWARE_UPGRADE"),			},			
	{ IPC_SET_PH_TYPE,					_T ("IPC_SET_PH_TYPE"),					},			
	{ IPC_SET_PRINTER_MODE,				_T ("IPC_SET_PRINTER_MODE"),			},			
	{ IPC_GET_PRINTER_MODE,				_T ("IPC_GET_PRINTER_MODE"),			},			
	{ IPC_SET_SELECTED_COUNT,			_T ("IPC_SET_SELECTED_COUNT"),			},			
	{ IPC_GET_SELECTED_COUNT,			_T ("IPC_GET_SELECTED_COUNT"),			},			
	{ IPC_SELECT_VARDATA_ID,			_T ("IPC_SELECT_VARDATA_ID"),			},			
	{ IPC_GET_VARDATA_ID,				_T ("IPC_GET_VARDATA_ID"),				},			
	{ IPC_SET_VARDATA_ID,				_T ("IPC_SET_VARDATA_ID"),				},			
	{ IPC_SAVE_ALL_PARAMS,				_T ("IPC_SAVE_ALL_PARAMS"),				},			
	{ IPC_GET_SW_VER_INFO,				_T ("IPC_GET_SW_VER_INFO"),				},			
	{ IPC_GET_GA_VER_INFO,				_T ("IPC_GET_GA_VER_INFO"),				},
	{ IPC_GA_UPGRADE,					_T ("IPC_GA_UPGRADE"),					},
	{ IPC_SET_STATUS_MODE,				_T ("IPC_SET_STATUS_MODE"),				},
	{ IPC_GET_STATUS_MODE,				_T ("IPC_GET_STATUS_MODE"),				},
	{ IPC_GET_DISK_USAGE,				_T ("IPC_GET_DISK_USAGE"),					},			
	{ IPC_GET_PH_TYPE,					_T ("IPC_GET_PH_TYPE"),					},			
};

static CString GetIpcCmdName (DWORD dw)
{
	static LPCTSTR lpszDef = _T ("[not found]");

	for (int i = 0; i < ARRAYSIZE (::map); i++)
		if (::map [i].m_dw == dw)
			return ::map [i].m_lpsz;

	return lpszDef;
}

static DWORD GetIpcCmd (const CString & str)
{
	for (int i = 0; i < ARRAYSIZE (::map); i++)
		if (!str.CompareNoCase (::map [i].m_lpsz))
			return ::map [i].m_dw;

	return map [0].m_dw;
}

static void DebugIpcCmd (const _FJ_SOCKET_MESSAGE & cmd, LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
#ifdef _DEBUG
	CString str, strData = a2w (cmd.Data);

	str.Format (_T ("(%-03d): %s[%-03d, %-05d, %-06d] (%s) %s"), 
		lLine,
		lpsz,
		cmd.Header.Cmd, 
		cmd.Header.BatchID, 
		cmd.Header.Length,
		GetIpcCmdName (cmd.Header.Cmd),
		str);
	CDebug::Trace (str, true, lpszFile, lLine);
#endif //_DEBUG
}

static void ntohl (_FJ_SOCKET_MESSAGE & cmd)
{
	cmd.Header.Cmd		= ntohl (cmd.Header.Cmd);
	cmd.Header.BatchID	= ntohl (cmd.Header.BatchID);
	cmd.Header.Length	= ntohl (cmd.Header.Length);
}

static bool IsSocketDataPending (SOCKET s)
{
	struct timeval timeout;
	fd_set read;

	memset (&read, 0, sizeof (read));
	read.fd_count = 1;
	read.fd_array [0] = s;

	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

	int nSelect = ::select (0, &read, NULL, NULL, &timeout);

	return (nSelect != SOCKET_ERROR && nSelect > 0) ? true : false;
}

static bool SendBinary (SOCKET s, ULONG lBatchID, ULONG lCmd, LPBYTE lpData, ULONG lLen, _FJ_SOCKET_MESSAGE * pReply = NULL)
{
	bool bSent = false;
	const DWORD dwTimeout = BindTo <DWORD> (FoxjetDatabase::GetProfileInt (::lpszDriverKey, _T ("IpcTimeout"), 1000 * 30), 1000, 1000 * 120); 
	const double dTimeout = (double)dwTimeout / 1000.0;
	struct
	{
		ULONG m_lCmd;
		ULONG m_lReply;
	}
	static const synonym [] = 
	{
		{ IPC_EDIT_START,			IPC_EDIT_STATUS },
		{ IPC_EDIT_SAVE,			IPC_EDIT_STATUS },
		{ IPC_GET_SELECTED_COUNT,	IPC_STATUS		},
	};

	if (s != INVALID_SOCKET) {
		DWORD dwTotalLen = sizeof (_FJ_SOCKET_MESSAGE_HEADER) + lLen;

		if (HGLOBAL hData = GlobalAlloc (GPTR, dwTotalLen)) {
			if (_FJ_SOCKET_MESSAGE * p = (_FJ_SOCKET_MESSAGE *)GlobalLock (hData)) {
				memset (p, 0, dwTotalLen);
				p->Header.Cmd		= htonl (lCmd);
				p->Header.BatchID	= htonl (lBatchID);
				p->Header.Length	= htonl (lLen);
				memcpy (p->Data, lpData, lLen);

				while (IsSocketDataPending (s)) { // clear any pending data
					_FJ_SOCKET_MESSAGE reply;

					memset (&reply, 0, sizeof (reply));
					recv (s, (char *)&reply, sizeof (reply), 0);
					ntohl (reply);
					TRACEF (CString (_T ("              ")) + a2w (reply.Data));
				}

				DWORD dwSend = send (s, (const char *)p, dwTotalLen, 0);
				bSent = dwSend == dwTotalLen ? true : false;
				{ CString str; str.Format (_T ("send                 [%-03d, %-05d, %-06d] (%s)%s"), lCmd, lBatchID, lLen, GetIpcCmdName (lCmd), bSent ? _T ("") : _T (" FAILED")); TRACEF (str); }

				if (pReply) { 
					clock_t clock_tStart = clock ();
					double dDiff = 0;

					do {
						dDiff = (double)(clock () - clock_tStart) / CLOCKS_PER_SEC;

						if (IsSocketDataPending (s)) {
							memset (pReply, 0, sizeof (_FJ_SOCKET_MESSAGE));
							::Sleep (::dwSleep);

							if (recv (s, (char *)&pReply->Header, sizeof (pReply->Header), 0) == sizeof (_FJ_SOCKET_MESSAGE_HEADER)) {
								ntohl (* pReply);

								bool bMatched = pReply->Header.BatchID == lBatchID ? true : false;

								for (int i = 0; !bMatched && (i < ARRAYSIZE (synonym)); i++) 
									if (lCmd == synonym [i].m_lCmd) 
										bMatched = (pReply->Header.Cmd == synonym [i].m_lReply) ? true : false;

								if (pReply->Header.Length) {
									::recv (s, (char *)&pReply->Data, pReply->Header.Length, 0);

									if (bMatched) 
										TRACEF (CString (bMatched ? _T ("[MATCHED    ] ") : _T ("[not matched] ")) + a2w (pReply->Data));
								}

								if (bMatched) 
									return bSent;
							}
						}
					}
					while (dDiff < dTimeout);

					if (dDiff >= dTimeout) {
						CString str;

						str.Format (_T ("Send: timed out waiting for reply [%.03fs (%.03fs)]"), dTimeout, dDiff);
						TRACEF (str);
					}
				}
			}

			GlobalUnlock (hData);
			GlobalFree (hData);
		}
	}
	
	return bSent;
}

static bool Send (SOCKET s, ULONG lBatchID, ULONG lCmd, LPCTSTR lpsz = _T (""), _FJ_SOCKET_MESSAGE * pReply = NULL, CString * pstrReply = NULL)
{
	bool bResult = true;
	const TCHAR cBreak = 0x0c;
	CStringArray v;

	if ((!pReply && pstrReply))
		TRACEF ("can't fill in pstrReply without pReply");

	Tokenize (lpsz, v, cBreak);

	if (!v.GetSize ())
		v.Add (_T (""));

	for (int i = 0; bResult && (i < v.GetSize ()); i++) {
		CString str = v [i];

		TRACEF (str);
		
		bResult &= SendBinary (s, lBatchID, lCmd, (LPBYTE)(char *)w2a (str), str.GetLength (), pReply);

		if (pstrReply)
			* pstrReply = a2w (pReply->Data);
	}

	return bResult;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//

CPrinterItem::CPrinterItem (int nHeadNumber, const CString & strAddr)
:	m_nHeadNumber (nHeadNumber),
	m_strAddr (strAddr),
	m_dwThreadID (0),
	m_bUpdating (false)
{
}

CPrinterItem::~CPrinterItem ()
{
}

CString CPrinterItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:	return m_strName;
	case 1:	return m_strAddr;
	case 2:	return m_strSpeed;
	case 3:	return m_strErrors;
	case 4: 
		#ifdef _DEBUG
			return _T ("[") + ToString (m_dwThreadID) + _T ("] ") + m_strState;
		#else
			return m_strState;
		#endif
	}

	return _T ("");
}


/////////////////////////////////////////////////////////////////////////////
// CPrintMonitorDlg dialog

CPrintMonitorDlg::CPrintMonitorDlg(CWnd* pParent /*=NULL*/)
:	m_pTaskbar (NULL),
	m_hExit (NULL),
	m_hStatusThread (NULL),
	CDialog(CPrintMonitorDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPrintMonitorDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CPrintMonitorDlg::~CPrintMonitorDlg ()
{
}

void CPrintMonitorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrintMonitorDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPrintMonitorDlg, CDialog)
	//{{AFX_MSG_MAP(CPrintMonitorDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_WINDOWPOSCHANGING()
	ON_WM_SHOWWINDOW()
	ON_NOTIFY(NM_RCLICK, LV_DATA, OnRclickData)
	ON_WM_INITMENUPOPUP()
	ON_COMMAND(ID_HELP_ABOUT, OnHelpAbout)
	ON_COMMAND(ID_FILE_HEADCONFIGURATION, OnFileHeadconfiguration)
	ON_COMMAND(ID_DOCUMENT_HEADPROPERTIES, OnDocumentHeadproperties)
	ON_COMMAND(ID_DOCUMENT_SYNCRONIZECONFIGURATION, OnDocumentSyncronizeconfiguration)
	ON_COMMAND(ID_FILE_UPDATEALLCONFIGURATIONS, OnFileUpdateallconfigurations)
	ON_NOTIFY(NM_DBLCLK, LV_DATA, OnDblclkData)
	ON_COMMAND(ID_DOCUMENT_PRINTDRIVERPROPERTIES, OnDocumentPrintdriverproperties)
	ON_COMMAND(ID_FILE_INSTALLDRIVER, OnFileInstalldriver)
	ON_COMMAND(ID_DOCUMENT_FIRMWAREVERSION, OnDocumentFirmwareversion)
	ON_COMMAND(ID_DOCUMENT_PREVIEW, OnDocumentPreview)
	ON_WM_INITMENU()
	ON_COMMAND(ID_FILE_DEBUGVIEWER, OnFileDebugviewer)
	ON_COMMAND(ID_FILE_DRIVERCACHE, OnFileDrivercache)
	//}}AFX_MSG_MAP
	ON_MESSAGE(NM_SYSTRAY, OnTrayNotify)
	ON_MESSAGE(WM_DATA, OnData)
	ON_MESSAGE(WM_CONFIGSTATUS_BYINDEX, OnConfigStatus)
	ON_MESSAGE(WM_CONFIGSTATUS_BYID, OnConfigStatusByID)
	ON_MESSAGE(WM_THREAD_INIT, OnThreadInit)
	ON_MESSAGE(WM_NEXT_STATUS, OnNextStatus)
	ON_MESSAGE(WM_HEAD_DELETED, OnHeadDeleted)
	ON_MESSAGE(WM_HEAD_CHANGED, OnHeadChanged)
	ON_MESSAGE(WM_HEAD_CONNECT_FAILED, OnHeadConnectFailed)
	ON_MESSAGE(WM_LOAD_HEAD_RECORD, OnLoadHeadRecord)
	ON_MESSAGE(WM_GET_LOCAL_CONFIG, OnGetLocalConfig)
	ON_MESSAGE(WM_IPC_TOSTRING, OnIpcToString)
	ON_MESSAGE(WM_HEAD_RECONNECT, OnHeadReconnect)

	ON_COMMAND(ID__SHOWWINDOW, OnShow)
	ON_COMMAND(ID_FILE_EXIT, OnFileExit)
	ON_COMMAND(ID_DOCUMENT_CANCEL, OnDocumentCancel)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintMonitorDlg message handlers

BOOL CPrintMonitorDlg::OnInitDialog()
{
	using CListCtrlImp::CColumn;

	CArray <CColumn, CColumn> vCols;

	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	vCols.Add (CColumn (LoadString (IDS_NAME), 120));
	vCols.Add (CColumn (LoadString (IDS_ADDRESS), 120));
	vCols.Add (CColumn (LoadString (IDS_SPEED), 100));
	vCols.Add (CColumn (LoadString (IDS_ERRORS), 100));
	vCols.Add (CColumn (LoadString (IDS_STATE), 300));

	m_lv.Create (LV_DATA, vCols, this, _T ("Print Monitor"));

	CString strTitle;

	GetWindowText (strTitle);
	TRACEF (strTitle);

	::CoInitialize(NULL);

	{
		CString str = GetCommandLine ();

		str.MakeUpper ();

		if (str.Find (_T ("/SYSTRAY")) != -1) {
			VERIFY (SUCCEEDED (::CoCreateInstance (CLSID_TaskbarList, NULL, CLSCTX_ALL, IID_ITaskbarList, (void**)&m_pTaskbar)));

			memset (&m_nid, 0, sizeof (m_nid));
			m_nid.cbSize = sizeof (m_nid);
			m_nid.hWnd = m_hWnd;
			m_nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
			m_nid.hIcon = m_hIcon;
			m_nid.uCallbackMessage = NM_SYSTRAY;
			_tcsncpy (m_nid.szTip, strTitle, ARRAYSIZE (m_nid.szTip));
			::Shell_NotifyIcon (NIM_ADD, &m_nid);

			SetWindowPos (&CWnd::wndTopMost, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
		}
		else
			SetWindowPos (&CWnd::wndNoTopMost, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	}


	{ // force initial WM_SIZE
		WINDOWPLACEMENT wp;

		ZeroMemory (&wp, sizeof (wp));
		wp.length = sizeof (WINDOWPLACEMENT);
		GetWindowPlacement (&wp);
		wp.rcNormalPosition.right++;
		SetWindowPlacement (&wp);
	}

	RestoreState (this, _T ("Print Monitor"));
	RemoveTaskbarButton ();

	m_hExit = ::CreateEvent (NULL, TRUE, FALSE, _T ("CPrintMonitorDlg::Exit"));

	{
		CSingleLock (&m_csThread).Lock();
		DWORD dwThreadID = 0;
		m_hStatusThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)StatusFunc, (LPVOID)this, 0, &dwThreadID);
	}


	theApp.UpdateRegistry ();

	{
		CSingleLock (&m_csThread).Lock();
		CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

		GetHeadRecords (theApp.m_db, vHeads);
		
		for (int i = 0; i < vHeads.GetSize (); i++) {
			DWORD dwThreadID = 0;
			CPrintMonitorDlg::CHeadData * p = new CPrintMonitorDlg::CHeadData (* this, vHeads [i], i);
			HANDLE hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)HeadFunc, (LPVOID)p, 0, &dwThreadID);
			m_vThreads.SetAt (hThread, dwThreadID);
		}
	}

	FillListCtrl ();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CPrintMonitorDlg::FillListCtrl ()
{
	CSingleLock (&m_csThread).Lock();
	CLongArray sel = GetSelectedItems (m_lv);
	int nSelHead = sel.GetSize () ? ((CPrinterItem *)m_lv.GetCtrlData (sel [0]))->m_nHeadNumber : 0;
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

	m_lv.DeleteCtrlData ();
	GetHeadRecords (theApp.m_db, vHeads);
	
	for (int i = 0; i < vHeads.GetSize (); i++) {
		HEADSTRUCT & h = vHeads [i];
		int nHead = GetHeadNumberFromID (h.m_lID);
		CPrinterItem * p = new CPrinterItem (nHead, h.m_strUID);
		p->m_strName = FoxjetDatabase::GetProfileString (GetKey (nHead), _T ("Name"), _T (""));
		m_lv.InsertCtrlData (p, 0);

		TRACEF (h.m_strName + _T (" [") + p->m_strName + _T ("] [") + GetKey (nHead) + _T (" (") + ToString (nHead) + _T (")]"));

		if (nHead == nSelHead)
			m_lv->SetItemState (i /*m_lv.GetDataIndex (p)*/, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
	}

	for (POSITION pos = m_vThreads.GetStartPosition (); pos; i++) {
		HANDLE hThread = NULL;
		DWORD dw = 0;
		DWORD dwExit = 0;

		m_vThreads.GetNextAssoc (pos, hThread, dw);
		
		if (::GetExitCodeThread (hThread, &dwExit))
			if (dwExit != STILL_ACTIVE)
				break;

		while (!::PostThreadMessage (dw, TM_INIT, (WPARAM)hThread, (LPARAM)dw))
			::Sleep (0);
	}

	m_lv->SortItems (m_lv.CompareFunc, (DWORD)0);
}


void CPrintMonitorDlg::Update (int nHead, const CString & strState)
{
	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (i)) {
			if (p->m_nHeadNumber == nHead) {
				p->m_strName = FoxjetDatabase::GetProfileString (GetKey (nHead), _T ("Name"), _T (""));
				p->m_strState = strState;
				
				//int nItem = m_lv.GetDataIndex (p); ASSERT (nItem == i);

				m_lv.UpdateCtrlData (p, i);
				return;
			}
		}
	}
}

void CPrintMonitorDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		OnHelpAbout ();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPrintMonitorDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CPrintMonitorDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CPrintMonitorDlg::OnSize(UINT nType, int cx, int cy) 
{
	CRect rc (0, 0, 0, 0);

	CDialog::OnSize(nType, cx, cy);
	
	if (CWnd * p = GetDlgItem (LBL_STATUS)) {
		p->GetWindowRect (rc);
		p->SetWindowPos (NULL, 0, cy - rc.Height (), cx, rc.Height (), SWP_NOZORDER);
	}

	if (CListCtrl * p = (CListCtrl *)GetDlgItem (LV_DATA)) 
		p->SetWindowPos (NULL, 0, 0, cx, cy - rc.Height (), SWP_NOZORDER);
}

void CPrintMonitorDlg::OnClose() 
{
	BeginWaitCursor ();
	
	CSingleLock (&m_csThread).Lock();
	const int nThreads = m_vThreads.GetCount () + 1;
	HANDLE * p = new HANDLE [nThreads];
	const clock_t clock_tLast = clock ();
	double dTimeout = 30.0;
	bool bTimeout = false;

	m_lv->EnableWindow (FALSE);

	if (m_pTaskbar)
		::Shell_NotifyIcon (NIM_DELETE, &m_nid);

	SaveState (this, _T ("Print Monitor"));

	if (m_hExit) 
		::SetEvent (m_hExit);

	{
		int i = 1;
	
		p [0] = m_hStatusThread;

		for (POSITION pos = m_vThreads.GetStartPosition (); pos; i++) {
			HANDLE hThread = NULL;
			DWORD dw = 0;

			m_vThreads.GetNextAssoc (pos, hThread, dw);
			p [i] = hThread;
		}
	}

	while (!bTimeout) {
		double dDiff = (double)(clock () - clock_tLast) / CLOCKS_PER_SEC;

		bTimeout = (dDiff > dTimeout);
		BeginWaitCursor ();

		//DWORD dwWait = ::WaitForSingleObject (m_hThread, 5000);
		DWORD dwWait = ::MsgWaitForMultipleObjects (nThreads, p, TRUE, 250, QS_ALLINPUT);

		if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
		if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
		if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
		if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");

		if (dwWait == WAIT_OBJECT_0) {
			m_vThreads.RemoveAll ();
			break;
		}

		PumpMessages ();
	}

	if (m_hExit) {
		::CloseHandle (m_hExit);
		m_hExit = NULL;
	}

	if (m_pTaskbar) {
		m_pTaskbar->Release ();
		m_pTaskbar = NULL;
	}

	EndDialog (IDOK);
	delete [] p;
	EndWaitCursor ();

	{
		CString str;

		double dDiff = (double)(clock () - clock_tLast) / CLOCKS_PER_SEC;
		str.Format (_T ("shut down in %.1f seconds"), dDiff);
		TRACEF (str);
	}
}

void CPrintMonitorDlg::OnWindowPosChanging(WINDOWPOS FAR* lpwndpos) 
{
	CDialog::OnWindowPosChanging(lpwndpos);
	RemoveTaskbarButton ();
}

void CPrintMonitorDlg::OnShow() 
{
	ShowWindow (SW_SHOWNORMAL);	
}

LRESULT CPrintMonitorDlg::OnTrayNotify (WPARAM wParam, LPARAM lParam)
{
	switch (lParam) {
	case WM_LBUTTONDBLCLK:
		OnShow ();
		break;
	case WM_RBUTTONDOWN:
		{
			CMenu menu;
			CPoint pt (0, 0);
			
			::GetCursorPos (&pt);
			menu.LoadMenu (IDM_SYSTRAY);
			
			if (CMenu * pMenu = menu.GetSubMenu (0)) {
				pMenu->SetDefaultItem (ID__SHOWWINDOW);
				pMenu->TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON, pt.x, pt.y, this);
			}
		}
		break;
	}

	return 0;
}

void CPrintMonitorDlg::OnFileExit() 
{
	OnClose();
}


void CPrintMonitorDlg::OnOK()
{

}

void CPrintMonitorDlg::OnCancel()
{

}

void CPrintMonitorDlg::RemoveTaskbarButton()
{
	if (m_pTaskbar) {
		m_pTaskbar->DeleteTab (m_hWnd);
		m_pTaskbar->DeleteTab (::AfxGetMainWnd ()->m_hWnd);
	}
}

int CPrintMonitorDlg::GetHeadNumberFromID (ULONG lFindID)
{
	for (int i = 0; i < MAX_HEADS; i++) {
		ULONG lID = FoxjetDatabase::GetProfileInt (GetKey (i), _T ("ID"), NOTFOUND);

		if (lID == lFindID)
			return i;
	}

	return -1;
}

CString CPrintMonitorDlg::GetKey(UINT nHeadNumber)
{
	CString str;

	str.Format (_T ("%s\\Head%d"), ::lpszDriverKey, nHeadNumber);

	return str;
}

void CPrintMonitorDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);

	if (!bShow)
		RemoveTaskbarButton ();	
}


ULONG CALLBACK CPrintMonitorDlg::HeadFunc (LPVOID lpData) 
{
	CPrintMonitorDlg::CHeadData & data = * (CPrintMonitorDlg::CHeadData *)lpData;
	SOCKADDR_IN target;
	ULONG lCmdID = 1;
	bool bConnected = false;
	HANDLE hThread = NULL;
	DWORD dwThreadID = 0;
	const DWORD dwTimeout = BindTo <DWORD> (FoxjetDatabase::GetProfileInt (::lpszDriverKey, _T ("IpcTimeout"), 1000 * 30), 1000, 1000 * 120); 
	const double dTimeout = (double)dwTimeout / 1000.0;
	clock_t clock_tLast = clock ();

	SOCKET s = INVALID_SOCKET;

	while (1) {
		HANDLE hEvent [] = { data.m_dlg.m_hExit };
		DWORD dwWait = ::MsgWaitForMultipleObjects (ARRAYSIZE (hEvent), hEvent, false, 1, QS_ALLINPUT);
		MSG msg;

		switch (dwWait) {
		case WAIT_OBJECT_0:
			delete &data;

			if (bConnected)
				closesocket (s);

			return 0;
		}

		if (!bConnected) {
			CString str = CTime::GetCurrentTime ().Format (_T ("%c: ")) + LoadString (IDS_ATTEMPTINGCONNECTION);
			
			::PostMessage (data.m_dlg.m_hWnd, WM_CONFIGSTATUS_BYID, data.m_head.m_lID, (LPARAM)new CString (str));
	
			target.sin_family = AF_INET; 
			target.sin_port = htons (_FJ_SOCKET_NUMBER); 
			target.sin_addr.s_addr = inet_addr (w2a (data.m_head.m_strUID));

			if (s == INVALID_SOCKET)
				s = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP); 

			if (connect(s, (SOCKADDR *)&target, sizeof(target)) != SOCKET_ERROR) {
				_FJ_SOCKET_MESSAGE reply;
				
				::PostMessage (data.m_dlg.m_hWnd, WM_CONFIGSTATUS_BYID, data.m_head.m_lID, (LPARAM)new CString (LoadString (IDS_CONNECTED)));
				bConnected = true;
				clock_tLast = clock ();
				Send (s, lCmdID++, IPC_GET_SW_VER_INFO, NULL, &reply); 
	
				if (reply.Header.Length)
					::PostMessage (data.m_dlg.m_hWnd, WM_NEXT_STATUS, data.m_head.m_lID, (LPARAM)new CString (a2w (reply.Data)));

				Send (s, lCmdID++, IPC_GET_GA_VER_INFO, NULL, &reply); TRACEF (a2w (reply.Data));

				if (reply.Header.Length)
					::PostMessage (data.m_dlg.m_hWnd, WM_NEXT_STATUS, data.m_head.m_lID, (LPARAM)new CString (a2w (reply.Data)));

				Send (s, lCmdID++, IPC_SET_STATUS_MODE, _T ("STATUS_MODE=ON"));

				::PostThreadMessage (::GetCurrentThreadId (), TM_CONFIG_CHANGE, 1, 0);
			}
			else {
				CString str = FormatMessage (::WSAGetLastError ());
	
				::PostMessage (data.m_dlg.m_hWnd, WM_CONFIGSTATUS_BYID, data.m_head.m_lID, (LPARAM)new CString (str));
				//::Sleep (2000);

				if (!::SendMessage (data.m_dlg.m_hWnd, WM_LOAD_HEAD_RECORD, data.m_head.m_lID, (LPARAM)&data.m_head)) { // deleted
					if (bConnected)
						closesocket (s);
					
					::PostMessage (data.m_dlg.m_hWnd, WM_HEAD_DELETED, 0, dwThreadID);
					delete &data;
					return 0;
				}

				::SendMessage (data.m_dlg.m_hWnd, WM_HEAD_CONNECT_FAILED, data.m_head.m_lID, dwThreadID);
				::PostThreadMessage (::GetCurrentThreadId (), TM_CONFIG_CHANGE, 1, 0);
			}
		}

		if (bConnected) {
			while (IsSocketDataPending (s)) { 
				_FJ_SOCKET_MESSAGE reply;

				memset (&reply, 0, sizeof (reply));
				recv (s, (char *)&reply, sizeof (reply), 0);
				ntohl (reply);

				if (reply.Header.Length) {
					clock_tLast = clock ();
					::PostMessage (data.m_dlg.m_hWnd, WM_NEXT_STATUS, data.m_head.m_lID, (LPARAM)new CString (a2w (reply.Data)));
				}
				else
					break;
			}

			double dDiff = (double)(clock () - clock_tLast) / CLOCKS_PER_SEC;

			if (dDiff > dTimeout) {
				TRACEF (data.m_head.m_strUID + _T (" lost connection"));
				::PostMessage (data.m_dlg.m_hWnd, WM_NEXT_STATUS, data.m_head.m_lID, (LPARAM)new CString (_T ("CONVEYOR_SPEED= ;InkLevel=OK;VoltReady=T;Heater1Ready=T;")));
				bConnected = false;
				closesocket (s);
				s = INVALID_SOCKET;
			}
		}

		if (::PeekMessage (&msg, NULL, 0, 0xFFFFFFFF, PM_NOREMOVE)) {
			if (::GetMessage (&msg, NULL, 0, 0xFFFFFFFF)) {
				switch (msg.message) {
				case TM_INIT:
					TRACEF (_T ("TM_INIT: ") + data.m_head.m_strUID);
					hThread		= (HANDLE)msg.wParam;
					dwThreadID	= (DWORD)msg.lParam;
					::PostMessage (data.m_dlg.m_hWnd, WM_THREAD_INIT, data.m_head.m_lID, msg.lParam);
					break;
				case TM_RECONNECT:
					{
						if (bConnected) {
							::PostMessage (data.m_dlg.m_hWnd, WM_NEXT_STATUS, data.m_head.m_lID, (LPARAM)new CString (_T ("CONVEYOR_SPEED= ;InkLevel=OK;VoltReady=T;Heater1Ready=T;")));
							bConnected = false;
							closesocket (s);
							s = INVALID_SOCKET;
							::Sleep (10 * 1000);
						}
					}
					break;
				case TM_CONFIG_CHANGE:
					{
						CString strUID = data.m_head.m_strUID;

						TRACEF (data.m_head.m_strUID + _T (": TM_CONFIG_CHANGE: ") + CString (bConnected ? _T ("connected") : _T ("NOT connected")));

						if (!::SendMessage (data.m_dlg.m_hWnd, WM_LOAD_HEAD_RECORD, data.m_head.m_lID, (LPARAM)&data.m_head)) { // deleted
							if (bConnected)
								closesocket (s);
							
							::PostMessage (data.m_dlg.m_hWnd, WM_HEAD_DELETED, 0, dwThreadID);
							delete &data;
							return 0;
						}

						if (strUID != data.m_head.m_strUID) { // addr changed
							::PostMessage (data.m_dlg.m_hWnd, WM_HEAD_CHANGED, data.m_head.m_lID, 0);
						
							if (bConnected) {
								::PostMessage (data.m_dlg.m_hWnd, WM_NEXT_STATUS, data.m_head.m_lID, (LPARAM)new CString (_T ("CONVEYOR_SPEED= ;InkLevel=OK;VoltReady=T;Heater1Ready=T;")));
								bConnected = false;
								closesocket (s);
								s = INVALID_SOCKET;
							}
						}

						if (bConnected) {
							if (msg.wParam) {
								if (CompareConfiguration (data, s)) {
									::PostMessage (data.m_dlg.m_hWnd, WM_CONFIGSTATUS_BYID, (WPARAM)data.m_head.m_lID, (LPARAM)new CString (LoadString (IDS_CONFIGUPTODATE)));
									break;
								}
							}

							SendConfiguration (data, s);
						}
					}
					break;
				}
			}

			::Sleep (1);
		}
	}

	return 0;
}

ULONG CALLBACK CPrintMonitorDlg::StatusFunc (LPVOID lpData) 
{
	CPrintMonitorDlg & dlg = * (CPrintMonitorDlg *)lpData;
	DWORD dwTimeout = 1;
	struct sockaddr_in addrUDP;
	bool bMore = true;
	char iOption;
	ULONG lRet;
	PRINTDRIVER_SOCKETMSG msg;

	WSADATA wsaData;

	if (::WSAStartup (MAKEWORD (2,1), &wsaData) != 0) {
		TRACEF (_T ("WSAStartup failed: ") + FormatMessage (WSAGetLastError ()));
	}

	SOCKET fdUDP = socket (AF_INET, SOCK_DGRAM, 0);
	
	memset((char *)&addrUDP, 0, sizeof(addrUDP));
	addrUDP.sin_family      = AF_INET;
	addrUDP.sin_addr.s_addr = inet_addr ("127.0.0.1"); //htonl (INADDR_ANY);
	addrUDP.sin_port        = htons (PRINTDRIVER_SOCKET_NUMBER);

	iOption = 1;
	lRet = setsockopt (fdUDP, SOL_SOCKET, SO_KEEPALIVE, (char *)&iOption, sizeof (iOption));
	iOption = 1;
	lRet = setsockopt (fdUDP, SOL_SOCKET, SO_REUSEADDR, (char *)&iOption, sizeof (iOption));
	iOption = 1;
	lRet = setsockopt (fdUDP, SOL_SOCKET, SO_BROADCAST, (char *)&iOption, sizeof (iOption));
	
    lRet = bind (fdUDP, (const struct sockaddr *)&addrUDP, sizeof (addrUDP));

	if (CStatic * p = (CStatic *)dlg.GetDlgItem (LBL_STATUS)) 
		p->SetWindowText (_T ("Monitoring..."));

	while (bMore) {
		HANDLE hEvent [] = { dlg.m_hExit };
		DWORD dwWait = ::MsgWaitForMultipleObjects (ARRAYSIZE (hEvent), hEvent, false, dwTimeout, QS_ALLINPUT);
		struct timeval timeout;
		fd_set read;

		switch (dwWait) {
		case WAIT_OBJECT_0:
			bMore = false;
			break;
		}

		memset (&read, 0, sizeof (read));
		read.fd_count = 1;
		read.fd_array [0] = fdUDP;

		timeout.tv_sec = 1;
		timeout.tv_usec = 0;
		int nSelect = ::select (0, &read, NULL, NULL, &timeout);

		if (nSelect > 0) {
			SOCKADDR_IN addr;
			int nSize = sizeof (addr);

			memset (&msg, 0, sizeof (msg));
			memset (&addr, 0, sizeof (addr));
			int nRecv = recvfrom (fdUDP, (char *)&msg, sizeof (msg), 0, (struct sockaddr *)&addr, &nSize);

			if (nRecv == SOCKET_ERROR) {
				TRACEF (FormatMessage (::WSAGetLastError ()));
			}
			else {
				PRINTDRIVER_SOCKETMSG * pMsg = new PRINTDRIVER_SOCKETMSG;

				pMsg->m_nMsg	= msg.m_nMsg;
				pMsg->m_wParam	= msg.m_wParam;
				pMsg->m_lParam	= msg.m_lParam;

				while (!::PostMessage (dlg.m_hWnd, WM_DATA, (WPARAM)pMsg, 0))
					::Sleep (0);

				//{ CString str; str.Format (_T ("recvfrom (%d, %d): %d %d %d"), nRecv, nSize, msg.m_nMsg, msg.m_wParam, msg.m_lParam); TRACEF (str); }
			}
		}

		::Sleep (10);
	}

	if (fdUDP != SOCKET_ERROR)
		closesocket (fdUDP);

	return 0;
}

LRESULT CPrintMonitorDlg::OnData (WPARAM wParam, LPARAM lParam)
{
	if (PRINTDRIVER_SOCKETMSG * pMsg = (PRINTDRIVER_SOCKETMSG *)wParam) {
		CString str;

		str.Format (_T ("CPrintMonitorDlg::OnData: %d %d %d"), pMsg->m_nMsg, pMsg->m_wParam, pMsg->m_lParam); TRACEF (str);

		switch (pMsg->m_nMsg) {
		case PRINTDRIVER_MSG_JOBREADY:
			Update (pMsg->m_wParam, LoadString (IDS_READY));
			break;
		case PRINTDRIVER_MSG_BEGINPRINT:
			Update (pMsg->m_wParam, LoadString (IDS_BEGIN));
			break;
		case PRINTDRIVER_MSG_ABORTPRINT:
			Update (pMsg->m_wParam, LoadString (IDS_ABORT));
			break;
		case PRINTDRIVER_MSG_DOPRINT:
			str.Format (LoadString (IDS_COPY), pMsg->m_lParam);
			Update (pMsg->m_wParam, str);
			break;
		case PRINTDRIVER_MSG_ENDPRINT:
			Update (pMsg->m_wParam, LoadString (IDS_END));
			break;
		case PRINTDRIVER_MSG_ERROR:
			str.Format (LoadString (IDS_ERROR), GetError (pMsg->m_lParam));
			Update (pMsg->m_wParam, str);
			break;
		case PRINTDRIVER_MSG_JOBSTART:
			Update (pMsg->m_wParam, LoadString (IDS_JOBSTART));
			break;
		}

		delete pMsg;
	}

	return 0;
}

LRESULT CPrintMonitorDlg::OnHeadDeleted (WPARAM wParam, LPARAM lParam)
{
	{
		CSingleLock (&m_csThread).Lock ();
		DWORD dwThreadID = (DWORD)lParam;

		for (POSITION pos = m_vThreads.GetStartPosition (); pos; ) {
			HANDLE hThread = NULL;
			DWORD dw = 0;

			m_vThreads.GetNextAssoc (pos, hThread, dw);

			if (dw == dwThreadID) {
				m_vThreads.RemoveKey (hThread);
				break;
			}
		}
	}

	FillListCtrl ();

	return 0;
}

LRESULT CPrintMonitorDlg::OnThreadInit (WPARAM wParam, LPARAM lParam)
{
	ULONG lID = (ULONG)wParam;
	DWORD dwThreadID = (DWORD)lParam;
	int nHead = GetHeadNumberFromID (lID);

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (i)) {
			if (p->m_nHeadNumber == nHead) {
				p->m_dwThreadID = dwThreadID;
				m_lv.UpdateCtrlData (p);
				break;
			}
		}
	}

	return 0;
}


LRESULT CPrintMonitorDlg::OnLoadHeadRecord (WPARAM wParam, LPARAM lParam)
{
	ULONG lID = (ULONG)wParam;
	HEADSTRUCT & h = * (HEADSTRUCT *)lParam;

	return GetHeadRecord (theApp.m_db, lID, h);
}

LRESULT CPrintMonitorDlg::OnHeadConnectFailed (WPARAM wParam, LPARAM lParam)
{
	ULONG lID = (ULONG)wParam;
	HEADSTRUCT h;

	if (GetHeadRecord (theApp.m_db, lID, h)) {
		TCHAR sz [_MAX_PATH] = { 0 };
		PRINTER_DEFAULTS pd;
		HANDLE hPrinter = NULL;

		memset (&pd, 0, sizeof (pd));
		pd.DesiredAccess = PRINTER_ALL_ACCESS;

		_tcsncpy (sz, h.m_strName, ARRAYSIZE (sz));

		if (::OpenPrinter (sz, &hPrinter, &pd)) {
			DWORD dwNeeded = 0;

			::GetPrinter (hPrinter, 5, NULL, 0, &dwNeeded);
			BYTE * pInfo = new BYTE [dwNeeded];
			memset (pInfo, 0, dwNeeded);

			if (::GetPrinter (hPrinter, 5, pInfo, dwNeeded, &dwNeeded)) {
				PRINTER_INFO_5 & info = * (PRINTER_INFO_5 *)pInfo;
				
				TRACEF (info.pPrinterName + CString (_T (": ")) + info.pPortName);

				if (_tcscmp (h.m_strUID, info.pPortName) != 0) {
					int nHead = GetHeadNumberFromID (lID);

					h.m_strUID = info.pPortName;

					TRACEF (_T ("port changed to: ") + h.m_strUID);
					VERIFY (UpdateHeadRecord (theApp.m_db, h));

					for (int i = 0; i < m_lv->GetItemCount (); i++) {
						if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (i)) {
							if (p->m_nHeadNumber == nHead) {
								p->m_strAddr = h.m_strUID;
								m_lv.UpdateCtrlData (p);
								break;
							}
						}
					}
				}
			}

			delete pInfo;
			::ClosePrinter (hPrinter);
		}
	}

	return 0;
}

LRESULT CPrintMonitorDlg::OnHeadChanged (WPARAM wParam, LPARAM lParam)
{
	ULONG lID = (ULONG)wParam;
	int nHead = GetHeadNumberFromID (lID);

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (i)) {
			if (p->m_nHeadNumber == nHead) {
				HEADSTRUCT head;

				GetHeadRecord (theApp.m_db, lID, head);
				p->m_strName = head.m_strName;
				p->m_strAddr = head.m_strUID;
				m_lv.UpdateCtrlData (p);
				break;
			}
		}
	}

	return 0;
}

static bool UpdateState (CString & strOld, const CString & strNew, bool bNullable = false)
{
	bool bResult = strOld != strNew;

	if (bResult)
		strOld = strNew;

	return bResult;
}

LRESULT CPrintMonitorDlg::OnNextStatus (WPARAM wParam, LPARAM lParam)
{
	int nHead = GetHeadNumberFromID ((ULONG)wParam);
	CString * pstr = (CString *)lParam;

	TRACEF (* pstr);

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (i)) {
			if (p->m_nHeadNumber == nHead) {
				bool bUpdate = false;
				CString strSW		= Extract (* pstr, _T ("SW_VER="), _T (";"));
				CString strGA		= Extract (* pstr, _T ("GA_VER="), _T (";"));
				CString strSpeed	= Extract (* pstr, _T ("CONVEYOR_SPEED="), _T (";"));

				if (strSpeed.GetLength ()) 
					strSpeed.Format (_T ("%0.1f"), _ttof (strSpeed));

				if (pstr->Find (_T ("InkLevel")) != -1) {
					CString strErrors;
					CString strInk		= Extract (* pstr, _T ("InkLevel="), _T (";"));
					CString strHV		= Extract (* pstr, _T ("VoltReady="), _T (";"));
					CString strTemp		= Extract (* pstr, _T ("Heater1Ready="), _T (";"));

					strInk.MakeUpper ();
					strHV.MakeUpper ();
					strTemp.MakeUpper ();

					if (strInk != _T ("OK"))		strErrors += LoadString (IDS_INK) + _T (":") + strInk + _T (" ");
					if (strHV == _T ("F"))			strErrors += LoadString (IDS_HV) + _T (" ");
					if (strTemp == _T ("F"))		strErrors += LoadString (IDS_TEMP) + _T (" ");
				
					bUpdate |= UpdateState (p->m_strErrors, strErrors);
				}

				if (strSpeed.GetLength ())
					bUpdate |= UpdateState (p->m_strSpeed, strSpeed);

				if (strSW.GetLength ()) p->m_strSwVer = strSW; 
				if (strGA.GetLength ()) p->m_strGaVer = strGA; 

				if (bUpdate)
					m_lv.UpdateCtrlData (p);
			}
		}
	}

	if (pstr)
		delete pstr;

	return 0;
}

LRESULT CPrintMonitorDlg::OnConfigStatusByID (WPARAM wParam, LPARAM lParam)
{
	UINT nHead = GetHeadNumberFromID ((ULONG)wParam);
	CString * pstr = (CString *)lParam;

	Update (nHead, pstr ? * pstr : _T (""));

	if (pstr)
		delete pstr;

	return 0;
}

LRESULT CPrintMonitorDlg::OnHeadReconnect (WPARAM wParam, LPARAM lParam)
{
	UINT nHead = GetHeadNumberFromID ((ULONG)wParam);

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (i)) {
			if (p->m_nHeadNumber == nHead) {
				::PostThreadMessage (p->m_dwThreadID, TM_RECONNECT, 0, 0);
			}
		}
	}

	return 0;
}

LRESULT CPrintMonitorDlg::OnConfigStatus (WPARAM wParam, LPARAM lParam)
{
	UINT nHead = (UINT)wParam;
	CString * pstr = (CString *)lParam;

	Update (nHead, pstr ? * pstr : _T (""));

	if (pstr)
		delete pstr;

	return 0;
}

void CPrintMonitorDlg::OnDocumentCancel() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize () == 0) {
		MessageBox (LoadString (IDS_NOTHINGSELECTED), NULL, MB_OK | MB_ICONINFORMATION);
		return;
	}
	else {
		for (int i = sel.GetSize () - 1; i >= 0; i--) {
			if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (sel [i])) {
				CString strKey = GetKey (p->m_nHeadNumber);

				TRACEF (strKey);
				FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strKey, _T ("Cancel"), 1);
				p->m_strState = LoadString (IDS_CANCELLING);
				m_lv.UpdateCtrlData (p);
			}
		}
	}
}

void CPrintMonitorDlg::OnRclickData(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize () > 0) {
		CMenu menu;
		CPoint pt (0, 0);
		
		::GetCursorPos (&pt);
		menu.LoadMenu (IDM_CONTEXT);
		
		if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (sel [0])) {
			CString strDir = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, ::lpszDriverKey, _T ("Dir"), _T ("C:\\Temp\\Debug"));
			CString strSlice = strDir + _T ("\\") + p->m_strAddr + _T ("_slice.bmp");

			if (::GetFileAttributes (strSlice) == -1)
				menu.EnableMenuItem (ID_DOCUMENT_PREVIEW, MF_GRAYED);
		}

		if (CMenu * pMenu = menu.GetSubMenu (0)) 
			pMenu->TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON, pt.x, pt.y, this);
	}
	
	if (pResult)
		*pResult = 0;
}

void CPrintMonitorDlg::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu) 
{
	int nSize = GetSelectedItems (m_lv).GetSize ();
	
	CDialog::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
	
	pPopupMenu->EnableMenuItem (ID_DOCUMENT_CANCEL,						nSize ? MF_ENABLED : MF_GRAYED);	
	pPopupMenu->EnableMenuItem (ID_DOCUMENT_HEADPROPERTIES,				nSize ? MF_ENABLED : MF_GRAYED);	
	pPopupMenu->EnableMenuItem (ID_DOCUMENT_PRINTDRIVERPROPERTIES,		nSize ? MF_ENABLED : MF_GRAYED);	
	pPopupMenu->EnableMenuItem (ID_DOCUMENT_SYNCRONIZECONFIGURATION,	nSize ? MF_ENABLED : MF_GRAYED);	

	if (theApp.IsXP ())
		pPopupMenu->DeleteMenu (ID_FILE_DRIVERCACHE, MF_BYCOMMAND);

	if (!nSize)
		pPopupMenu->EnableMenuItem (ID_DOCUMENT_PREVIEW, MF_GRAYED);	
}

void CPrintMonitorDlg::OnHelpAbout() 
{
	CStringArray vCmdLine;

	FoxjetDatabase::TokenizeCmdLine (::GetCommandLine (), vCmdLine);

	FoxjetCommon::CAboutDlg dlg (verApp, vCmdLine, LoadString (IDS_ABOUT));

	dlg.DoModal ();
}

CString CPrintMonitorDlg::GetError(DWORD dwError)
{
	CString str;
	struct
	{
		DWORD m_dw;
		UINT m_nID;
	} 
	static const map [] = 
	{
		{ PRINTDRIVER_ERROR_CONNECTFAILED,	IDS_PRINTDRIVER_ERROR_CONNECTFAILED, },
		{ PRINTDRIVER_ERROR_CANCELSW,		IDS_PRINTDRIVER_ERROR_CANCELSW,		 },
		{ PRINTDRIVER_ERROR_CANCELHW,		IDS_PRINTDRIVER_ERROR_CANCELHW,		 },
	};

	for (int i = 0; i < ARRAYSIZE (map); i++) 
		if (map [i].m_dw == dwError) 
			return LoadString (map [i].m_nID);


	str.Format (_T ("(%d)"), dwError);

	return str;
}

void CPrintMonitorDlg::OnFileHeadconfiguration() 
{
	CPrintMonitorHeadConfigDlg dlg (theApp.m_db, INCHES, this);
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
	CArray <DWORD, DWORD> vThreadIDs;

	for (int i = 0; i < m_lv->GetItemCount (); i++) 
		vThreadIDs.Add (((CPrinterItem *)m_lv.GetCtrlData (i))->m_dwThreadID);

	GetHeadRecords (theApp.m_db, vHeads);

	for (i = 0; i < vHeads.GetSize (); i++) 
		vHeads [i].m_bDigiNET = true;

	BEGIN_TRANS (theApp.m_db);

	if (dlg.DoModal () == IDOK) {
		CArray <ULONG, ULONG> v;

		COMMIT_TRANS (theApp.m_db);

		for (int i = 0; i < dlg.m_vEdited.GetSize (); i++) 
			v.Add (dlg.m_vEdited [i]);

		for (i = 0; i < dlg.m_vAdded.GetSize (); i++) 
			v.Add (dlg.m_vAdded [i]);

		for (i = 0; i < v.GetSize (); i++) {
			ULONG lID = v [i];
			int nHead = GetHeadNumberFromID (lID);

			if (nHead > 0) {
				CString str = CPrintMonitorDlg::GetKey (nHead);
				HEADSTRUCT head;

				FoxjetDatabase::WriteProfileInt (str, CPrintMonitorApp::m_strUpdate, 1);
			}
		}

		for (i = 0; i < dlg.m_vDeleted.GetSize (); i++) {
			HANDLE hPrinter = NULL;
			ULONG lHeadID = dlg.m_vDeleted [i];

			for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
				HEADSTRUCT & h = vHeads [nHead];

				if (h.m_lID == lHeadID) {
					TCHAR sz [_MAX_PATH] = { 0 };
					PRINTER_DEFAULTS pd;

					memset (&pd, 0, sizeof (pd));
					pd.DesiredAccess = PRINTER_ALL_ACCESS;

					_tcsncpy (sz, h.m_strName, ARRAYSIZE (sz));

					if (::OpenPrinter (sz, &hPrinter, &pd)) {
						if (!::DeletePrinter (hPrinter))
							MessageBox (LoadString (IDS_FAILEDTODELETE) + h.m_strName + _T ("\n") + FormatMessage (::GetLastError ()));

						::ClosePrinter (hPrinter);
					}
				}
			}
		}

		UpdatePrinterPorts (m_hWnd, theApp.m_db);
		theApp.UpdateRegistry ();

		for (i = 0; i < vThreadIDs.GetSize (); i++) 
			::PostThreadMessage (vThreadIDs [i], TM_CONFIG_CHANGE, 0, 0);

		if (dlg.m_vAdded.GetSize ()) {
			for (i = 0; i < dlg.m_vAdded.GetSize (); i++) {
				CSingleLock (&m_csThread).Lock();
				HEADSTRUCT head;
				DWORD dwThreadID = 0;

				GetHeadRecord (theApp.m_db, dlg.m_vAdded [i], head);

				CPrintMonitorDlg::CHeadData * p = new CPrintMonitorDlg::CHeadData (* this, head, i);
				HANDLE hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)HeadFunc, (LPVOID)p, 0, &dwThreadID);
				m_vThreads.SetAt (hThread, dwThreadID);

				FillListCtrl ();
			}

			CDriverDlg (this).DoModal ();	
		}

		SendConfiguration ();
	}
	else {
		ROLLBACK_TRANS (theApp.m_db);
	}
}

void CPrintMonitorDlg::OnDocumentHeadproperties() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize () == 0) {
		MessageBox (LoadString (IDS_NOTHINGSELECTED), NULL, MB_OK | MB_ICONINFORMATION);
		return;
	}

	CMapStringToString vTmp;
	HEADSTRUCT head;

	if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (sel [0])) {
		CString strKey = GetKey (p->m_nHeadNumber);

		ULONG lID = FoxjetDatabase::GetProfileInt (strKey, _T ("ID"), NOTFOUND);
		
		head.m_bDigiNET = true;

		if (GetHeadRecord (theApp.m_db, lID, head)) {
			Foxjet3d::CHeadDlg dlg (theApp.m_db, head, INCHES, vTmp, this);

			dlg.m_bNew		= false;
			dlg.m_lLineID	= -1;
			dlg.m_bSync		= false;

			BEGIN_TRANS (theApp.m_db);

			if (dlg.DoModal () == IDOK) {
				head = dlg.GetHead ();
				COMMIT_TRANS (theApp.m_db);
				VERIFY (UpdateHeadRecord (theApp.m_db, head));
				p->m_strAddr = head.m_strUID;
				Update (p->m_nHeadNumber, _T (""));
				UpdatePrinterPorts (m_hWnd, theApp.m_db);
				theApp.UpdateRegistry ();
				FoxjetDatabase::WriteProfileInt (strKey, CPrintMonitorApp::m_strUpdate, 1);				
				SendConfiguration (p->m_nHeadNumber);
			}
			else {
				ROLLBACK_TRANS (theApp.m_db);
			}
		}
		else {
			CString str;

			str.Format (LoadString (IDS_HEADNOTFOUND), p->m_strAddr);
			MessageBox (str, NULL, MB_OK | MB_ICONINFORMATION);
		}
	}
}

void CPrintMonitorDlg::SendConfiguration ()
{
	for (int i = 0; i < MAX_HEADS; i++) {
		CString strKey = GetKey (i);

		if (FoxjetDatabase::GetProfileInt (strKey, CPrintMonitorApp::m_strUpdate, 0) ? true : false) 
			SendConfiguration (i);
	}
}

void CPrintMonitorDlg::SendConfiguration (int nHead)
{
	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (i)) {
			if (p->m_nHeadNumber == nHead) {
				::PostThreadMessage (p->m_dwThreadID, TM_CONFIG_CHANGE, 0, 0);
			}
		}
	}

	PumpMessages ();
}

void CPrintMonitorDlg::PumpMessages ()
{
/*
	MSG msg;

	while (::PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE)) {
		::GetMessage (&msg, (HWND)NULL, 0, 0);
		::TranslateMessage (&msg);
		::DispatchMessage (&msg);
		::Sleep (0);
	}
*/
}

void CPrintMonitorDlg::OnDocumentSyncronizeconfiguration() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize () == 0) {
		MessageBox (LoadString (IDS_NOTHINGSELECTED), NULL, MB_OK | MB_ICONINFORMATION);
		return;
	}

	if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (sel [0])) 
		SendConfiguration (p->m_nHeadNumber);
}


bool CPrintMonitorDlg::CompareConfiguration (const CPrintMonitorDlg::CHeadData & data, SOCKET s)
{
	CString strLocal, strRemote;
	CStringArray vLocal, vRemote;

	::PostMessage (data.m_dlg.m_hWnd, WM_CONFIGSTATUS_BYID, (WPARAM)data.m_head.m_lID, (LPARAM)new CString (LoadString (IDS_VERIFYINGCONFIG)));
	::SendMessage (data.m_dlg.m_hWnd, WM_GET_LOCAL_CONFIG, (WPARAM)data.m_head.m_lID, (LPARAM)&strLocal);

	Tokenize (strLocal, vLocal, ';');
	Tokenize (strRemote, vRemote, ';');

	for (int nRemote = vRemote.GetSize () - 1; nRemote >= 0; nRemote--) {
		CString strFind = vRemote [nRemote];

		if (!strFind.GetLength ()) {
			vRemote.RemoveAt (nRemote);
			continue;
		}

		for (int nLocal = 0; nLocal < vLocal.GetSize (); nLocal++) {
			CString str = vLocal [nLocal];

			if (strFind.Find (str) != -1) {
				vRemote.RemoveAt (nRemote);
				TRACEF (_T ("matched: ") + strFind);
				break;
			}
		}
	}

	{
		ULONG lCmd [] = 
		{
			IPC_GET_SYSTEM_INFO,
			IPC_GET_PRINTER_INFO,
			IPC_GET_PH_TYPE, 
			IPC_GET_PRINTER_INFO, 
	//		IPC_GET_SYSTEM_TIME_INFO,
	//		IPC_GET_SYSTEM_DATE_STRINGS,
	//		IPC_GET_SYSTEM_BARCODES,
	//		IPC_GET_SYSTEM_DATAMATRIX,
			IPC_GET_PRINTER_MODE,
		};
		ULONG lCmdID = 1;

		for (int i = 0; i < ARRAYSIZE (lCmd); i++) {
			_FJ_SOCKET_MESSAGE reply;
		
			memset (&reply, 0, sizeof (reply));
			Send (s, lCmdID++, lCmd [i], NULL, &reply); 
			strRemote += Trim (a2w (reply.Data));
		}
	}

	return (vRemote.GetSize () == 0);
/* TODO: rem
	CString strLocal, strRemote;
	CStringArray vLocal, vRemote;

	{ // TODO: rem
		PANELSTRUCT panel;
		LINESTRUCT line;
		ULONG lCmd [] = 
		{
			IPC_SET_PRINTER_INFO, 
			IPC_SET_SYSTEM_INFO,
			//IPC_SET_PRINTER_OP_INFO,
			IPC_SET_PRINTER_MODE,
		};

		VERIFY (GetPanelRecord (theApp.m_db, head.m_lPanelID, panel));
		VERIFY (GetLineRecord (theApp.m_db, panel.m_lLineID, line));

		{
			CString str;

			FoxjetCommon::ToString (theApp.m_db, head, &line, IPC_SET_PH_TYPE, str);
			strLocal = _T ("HeadType=") + str + _T (";");
		}

		for (int i = 0; i < ARRAYSIZE (lCmd); i++) {
			CString str;

			FoxjetCommon::ToString (theApp.m_db, head, &line, lCmd [i], str);
			strLocal += str;
		}
	}

	{
		ULONG lCmd [] = 
		{
			IPC_GET_SYSTEM_INFO,
			IPC_GET_PRINTER_INFO,
			IPC_GET_PH_TYPE, 
			IPC_GET_PRINTER_INFO, 
	//		IPC_GET_SYSTEM_TIME_INFO,
	//		IPC_GET_SYSTEM_DATE_STRINGS,
	//		IPC_GET_SYSTEM_BARCODES,
	//		IPC_GET_SYSTEM_DATAMATRIX,
			IPC_GET_PRINTER_MODE,
		};
		ULONG lCmdID = 1;

		for (int i = 0; i < ARRAYSIZE (lCmd); i++) {
			_FJ_SOCKET_MESSAGE reply;
		
			memset (&reply, 0, sizeof (reply));
			Send (s, lCmdID++, lCmd [i], NULL, &reply); 
			strRemote += Trim (a2w (reply.Data));
		}
	}

	Tokenize (strLocal, vLocal, ';');
	Tokenize (strRemote, vRemote, ';');

//	{ for (int i = 0; i < vLocal.GetSize (); i++) TRACEF (vLocal [i]); }
//	{ for (int i = 0; i < vRemote.GetSize (); i++) TRACEF (vRemote [i]); }

	for (int nRemote = vRemote.GetSize () - 1; nRemote >= 0; nRemote--) {
		CString strFind = vRemote [nRemote];

		if (!strFind.GetLength ()) {
			vRemote.RemoveAt (nRemote);
			continue;
		}

		for (int nLocal = 0; nLocal < vLocal.GetSize (); nLocal++) {
			CString str = vLocal [nLocal];

			if (strFind.Find (str) != -1) {
				vRemote.RemoveAt (nRemote);
				TRACEF (_T ("matched: ") + strFind);
				break;
			}
		}
	}

//	{ for (int i = 0; i < vLocal.GetSize (); i++) TRACEF (vLocal [i]); }
//	{ for (int i = 0; i < vRemote.GetSize (); i++) TRACEF (vRemote [i]); }

	return vRemote.GetSize () == 0;
*/
}

LRESULT CPrintMonitorDlg::OnGetLocalConfig (WPARAM wParam, LPARAM lParam)
{
	ULONG lID = (ULONG)wParam;
	CString strLocal;
	HEADSTRUCT head;
	PANELSTRUCT panel;
	LINESTRUCT line;
	ULONG lCmd [] = 
	{
		IPC_SET_PRINTER_INFO, 
		IPC_SET_SYSTEM_INFO,
		//IPC_SET_PRINTER_OP_INFO,
		IPC_SET_PRINTER_MODE,
	};

	VERIFY (GetHeadRecord (theApp.m_db, lID, head));

	if (head.m_lPanelID != -1) {
		VERIFY (GetPanelRecord (theApp.m_db, head.m_lPanelID, panel));
		VERIFY (GetLineRecord (theApp.m_db, panel.m_lLineID, line));
	}
	else {
		if (GetFirstLineRecord (theApp.m_db, line, GetPrinterID (theApp.m_db))) {
			CArray <PANELSTRUCT, PANELSTRUCT &> v;

			GetPanelRecords (theApp.m_db, line.m_lID, v);

			if (v.GetSize ())
				panel = v [0];
		}
	}

	{
		CString str;

		FoxjetCommon::ToString (theApp.m_db, head, &line, IPC_SET_PH_TYPE, str);
		strLocal = _T ("HeadType=") + str + _T (";");
	}

	for (int i = 0; i < ARRAYSIZE (lCmd); i++) {
		CString str;

		FoxjetCommon::ToString (theApp.m_db, head, &line, lCmd [i], str);
		strLocal += str;
	}

	if (CString * p = (CString *)lParam)
		* p = strLocal;

	return 0;
}

LRESULT CPrintMonitorDlg::OnIpcToString (WPARAM wParam, LPARAM lParam)
{
	IPCSTRUCT * pParam = (IPCSTRUCT *)wParam;
	HEADSTRUCT head;
	PANELSTRUCT panel;
	LINESTRUCT line;
	CString str;

	VERIFY (GetHeadRecord (theApp.m_db, pParam->m_lHeadID, head));

	if (head.m_lPanelID != -1) {
		VERIFY (GetPanelRecord (theApp.m_db, head.m_lPanelID, panel));
		VERIFY (GetLineRecord (theApp.m_db, panel.m_lLineID, line));
	}
	else {
		if (GetFirstLineRecord (theApp.m_db, line, GetPrinterID (theApp.m_db))) {
			CArray <PANELSTRUCT, PANELSTRUCT &> v;

			GetPanelRecords (theApp.m_db, line.m_lID, v);

			if (v.GetSize ())
				panel = v [0];
		}
	}
	
	FoxjetCommon::ToString (theApp.m_db, head, &line, pParam->m_lCmdID, str);

	if (CString * p = (CString *)lParam)
		* p = str;

	return 0;
}

void CPrintMonitorDlg::SendConfiguration (const CPrintMonitorDlg::CHeadData & data, SOCKET s) 
{	
	bool bResult = false;
	ULONG lCmdID = 1;

	while (!::PostMessage (data.m_dlg.m_hWnd, WM_CONFIGSTATUS_BYID, (WPARAM)data.m_head.m_lID, (LPARAM)new CString (LoadString (IDS_ATTEMPTINGCONNECTION))))
		::Sleep (0);

	::Sleep (100);

	ULONG lCmd [] = 
	{
		IPC_SET_PH_TYPE, 
		IPC_SET_PRINTER_INFO, 
		IPC_SET_SYSTEM_INFO,
		//IPC_SET_PRINTER_OP_INFO,
		IPC_SET_PRINTER_MODE,
	};

	while (!::PostMessage (data.m_dlg.m_hWnd, WM_CONFIGSTATUS_BYID, (WPARAM)data.m_head.m_lID, (LPARAM)new CString (LoadString (IDS_UPDATECONFIG) + _T ("..."))))
		::Sleep (0);

	for (int i = 0; i < ARRAYSIZE (lCmd); i++) {
		CString str;

		::SendMessage (data.m_dlg.m_hWnd, WM_IPC_TOSTRING, (WPARAM)&IPCSTRUCT (data.m_head.m_lID, lCmd [i]), (LPARAM)&str);

		CString strUpdate = LoadString (IDS_UPDATECONFIG) + _T (": ") + GetIpcCmdName (lCmd [i]) + _T ("...");

		while (!::PostMessage (data.m_dlg.m_hWnd, WM_CONFIGSTATUS_BYID, (WPARAM)data.m_head.m_lID, (LPARAM)new CString (strUpdate)))
			::Sleep (0);
		
		Send (s, lCmdID++, lCmd [i], str, NULL);
		::Sleep (500);
	}

	if (!data.m_head.m_bDigiNET) {
		Send (s, lCmdID++, IPC_SAVE_ALL_PARAMS);
		bResult = true;
	}
	else {
		_FJ_SOCKET_MESSAGE reply;
	
		memset (&reply, 0, sizeof (reply));

		Send (s, lCmdID++, IPC_SAVE_ALL_PARAMS, _T (""), &reply);
		DEBUGIPC (reply, _T ("IPC_SAVE_ALL_PARAMS: "));
		bResult = true;

		CString str = LoadString (IDS_UPDATECONFIG) + _T (" ");
		str += LoadString (reply.Header.BatchID == (lCmdID - 1) ? IDS_SUCCEEDED : IDS_FAILED);

		while (!::PostMessage (data.m_dlg.m_hWnd, WM_CONFIGSTATUS_BYID, (WPARAM)data.m_head.m_lID, (LPARAM)new CString (str)))
			::Sleep (0);
	}

	{
		CString str = LoadString (IDS_UPDATECONFIG) + _T (" ") + LoadString (bResult ? IDS_SUCCEEDED : IDS_FAILED);

		while (!::PostMessage (data.m_dlg.m_hWnd, WM_CONFIGSTATUS_BYID, (WPARAM)data.m_head.m_lID, (LPARAM)new CString (str)))
			::Sleep (0);
	}

	if (bResult)
		FoxjetDatabase::WriteProfileInt (GetKey (data.m_nHead), CPrintMonitorApp::m_strUpdate, 0);
}

void CPrintMonitorDlg::OnDblclkData(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnDocumentHeadproperties ();

	if (pResult)
		* pResult = 0;
}

void CPrintMonitorDlg::OnFileUpdateallconfigurations() 
{
	BeginWaitCursor ();
	CArray <UINT, UINT> v;

	for (int i = 0; i < MAX_HEADS; i++) {
		CString strKey = GetKey (i);
		CString strAddr = FoxjetDatabase::GetProfileString (strKey, _T ("UID"), _T (""));

		if (strAddr.GetLength ()) {
			Update (i, LoadString (IDS_ATTEMPTINGCONNECTION));
			v.Add (i);
		}
	}

	m_lv->SortItems (CListCtrlImp::CompareFunc, 0);

	for (i = 0; i < v.GetSize (); i++)
		SendConfiguration (v [i]);
	
	EndWaitCursor ();
}

void CPrintMonitorDlg::TraceListCtrl()
{
	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (i)) {
			TRACEF (_T ("[") + ToString (i) + _T ("] ") + ToString (p->m_nHeadNumber) + _T (": ") + p->m_strAddr);
		}
	}

	TRACEF ("");
}

void CPrintMonitorDlg::OpenPrinter (const CString & strPrinter, const CString & strPort)
{
	DWORD dwNeeded = 0, dwReturned = 0;

	::EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 2, NULL, 0, &dwNeeded, &dwReturned);

	if (dwNeeded > 0) {
		PRINTER_INFO_2 * p = (PRINTER_INFO_2 *)new BYTE [dwNeeded] ;//new PRINTER_INFO_2 [nCount];

		if (::EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 2, (LPBYTE)p, dwNeeded, &dwNeeded, &dwReturned)) {
			for (DWORD i = 0; i < dwReturned; i++) {

				TRACEF (p [i].pDriverName + CString (_T (": ")) + p [i].pPrinterName + CString (_T (": ")) + p [i].pPortName);
				
				if (!_tcscmp (strPrinter, p [i].pDriverName)) {

					if (!_tcscmp (strPort, p [i].pPortName)) {
						HANDLE hPrinter = NULL;
				
						if (::OpenPrinter (p [i].pPrinterName, &hPrinter, NULL)) {
							CString strCmdLine;
							STARTUPINFO si;     
							PROCESS_INFORMATION pi; 
							TCHAR szCmdLine [MAX_PATH];

							//strCmdLine.Format (_T ("RUNDLL32 PRINTUI.DLL,PrintUIEntry /p /n\"\\\\%s\\%s\""), _T ("VISTALAPTOP"), p [i].pPrinterName);
							strCmdLine.Format (_T ("RUNDLL32 PRINTUI.DLL,PrintUIEntry /p /n\"%s\""), p [i].pPrinterName);
							TRACEF (strCmdLine);

							_tcsncpy (szCmdLine, strCmdLine, MAX_PATH);
							memset (&pi, 0, sizeof(pi));
							memset (&si, 0, sizeof(si));
							si.cb = sizeof(si);     
							si.dwFlags = STARTF_USESHOWWINDOW;     
							si.wShowWindow = SW_SHOW;     

							if (!::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) 
								TRACEF (FormatMessage (::GetLastError ()));
							else {
								/*
								DWORD dwWait = ::WaitForSingleObject (pi.hThread, INFINITE);
								
								if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
								if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
								if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
								if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");
								*/
							}

							::ClosePrinter (hPrinter);
							break;
						}
					}
				}
			}
		}

		delete [] p;
	}
}

void CPrintMonitorDlg::OnDocumentPrintdriverproperties() 
{
	CLongArray sel = GetSelectedItems (m_lv);
	CStringArray v;
	bool bFound = false;
	CSelectPrinterDlg dlg (this);

	if (sel.GetSize () == 0) {
		MessageBox (LoadString (IDS_NOTHINGSELECTED), NULL, MB_OK | MB_ICONINFORMATION);
		return;
	}

	if (CPrinterItem * pItem = (CPrinterItem *)m_lv.GetCtrlData (sel [0])) {
		CString strKey = GetKey (pItem->m_nHeadNumber);
		CString strAddr = FoxjetDatabase::GetProfileString (strKey, _T ("UID"), _T (""));
		DWORD dwNeeded = 0, dwReturned = 0;

		::EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 2, NULL, 0, &dwNeeded, &dwReturned);

		if (dwNeeded > 0) {
			PRINTER_INFO_2 * p = (PRINTER_INFO_2 *)new BYTE [dwNeeded] ;//new PRINTER_INFO_2 [nCount];

			if (::EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 2, (LPBYTE)p, dwNeeded, &dwNeeded, &dwReturned)) {
				for (DWORD i = 0; i < dwReturned; i++) {

					TRACEF (p [i].pDriverName + CString (_T (": ")) + p [i].pPrinterName + CString (_T (": ")) + p [i].pPortName);
					
					if (!_tcscmp (_T ("Foxjet Marksman NEXT"), p [i].pDriverName)) {
						dlg.m_vPrinters.Add (new CSelectPrinterDlg::CPrinterItem (p [i].pDriverName, p [i].pPrinterName, p [i].pPortName));

						if (!_tcscmp (pItem->m_strAddr, p [i].pPortName)) {
							OpenPrinter (p [i].pDriverName, pItem->m_strAddr);
							bFound = true;
							break;
						}
					}
				}
			}

			delete [] p;
		}
	}

	if (!bFound) {
		if (dlg.DoModal () == IDOK)
			OpenPrinter (dlg.m_strDriver, dlg.m_strPort);
	}
}


void CPrintMonitorDlg::OnFileInstalldriver() 
{
	CDriverDlg (this).DoModal ();	
}

void CPrintMonitorDlg::OnDocumentFirmwareversion() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (sel [0])) {
			CFirmwareDlg dlg (this);

			dlg.m_strName	= FoxjetDatabase::GetProfileString (GetKey (p->m_nHeadNumber), _T ("Name"), _T (""));
			dlg.m_strAddr	= p->m_strAddr;
			dlg.m_strSwVer	= p->m_strSwVer;
			dlg.m_strGaVer	= p->m_strGaVer;
			dlg.m_nHead		= p->m_nHeadNumber;

			if (dlg.DoModal () == IDOK) {
			}
		}
	}
}

void CPrintMonitorDlg::OnDocumentPreview() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		if (CPrinterItem * p = (CPrinterItem *)m_lv.GetCtrlData (sel [0])) {
			CPreviewDlg dlg (this);
			CString strKey = ::lpszDriverKey;
			CString strDir = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("Dir"), _T ("C:\\Temp\\Debug"));
			CString strWhole = strDir + _T ("\\") + p->m_strAddr + _T (".bmp");
			CString strSlice = strDir + _T ("\\") + p->m_strAddr + _T ("_slice.bmp");

			TRACEF (strWhole);
			TRACEF (strSlice);

			if (::GetFileAttributes (strSlice) != -1)
				dlg.m_strFile = strSlice;

			if (dlg.m_strFile.GetLength ())
				dlg.DoModal ();
		}
	}
}

void CPrintMonitorDlg::OnInitMenu(CMenu* pMenu) 
{
	CDialog::OnInitMenu(pMenu);
	CString strPath = GetHomeDir () + _T ("\\Dbgview.exe");

	if (::GetFileAttributes (strPath) == -1)
		pMenu->EnableMenuItem (ID_FILE_DEBUGVIEWER, MF_GRAYED);
}

void CPrintMonitorDlg::OnFileDebugviewer() 
{
	CString strPath = GetHomeDir () + _T ("\\Dbgview.exe");
	CString strCmdLine;
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [MAX_PATH];

	strCmdLine.Format (_T ("\"%s\""), strPath);
	TRACEF (strCmdLine);

	_tcsncpy (szCmdLine, strCmdLine, MAX_PATH);
	memset (&pi, 0, sizeof(pi));
	memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_HIDE;     

	if (!::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) 
		TRACEF (FormatMessage (::GetLastError ()));
}

void CPrintMonitorDlg::OnFileDrivercache() 
{
	CDriverStoreDlg (this).DoModal ();
}



