#if !defined(AFX_FIRMWAREDLG_H__19283D2B_0E9C_46CE_90FD_7BA7B4C657DD__INCLUDED_)
#define AFX_FIRMWAREDLG_H__19283D2B_0E9C_46CE_90FD_7BA7B4C657DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FirmwareDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFirmwareDlg dialog

class CFirmwareDlg : public CDialog
{
// Construction
public:
	CFirmwareDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CFirmwareDlg)
	enum { IDD = IDD_FIRMWARE };
	CString	m_strGaVer;
	CString	m_strName;
	CString	m_strSwVer;
	//}}AFX_DATA
	CString m_strAddr;
	int m_nHead;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFirmwareDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFirmwareDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBrowsefirmware();
	afx_msg void OnBrowsega();
	afx_msg void OnUpdatefirmware();
	afx_msg void OnUpdatega();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIRMWAREDLG_H__19283D2B_0E9C_46CE_90FD_7BA7B4C657DD__INCLUDED_)
