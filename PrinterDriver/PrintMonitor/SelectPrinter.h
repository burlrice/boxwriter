#if !defined(AFX_SELECTPRINTER_H__1A0F13CC_0A86_40AA_ABCA_0469ABBDCC40__INCLUDED_)
#define AFX_SELECTPRINTER_H__1A0F13CC_0A86_40AA_ABCA_0469ABBDCC40__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectPrinter.h : header file
//

#include "ListCtrlImp.h"
#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// CSelectPrinterDlg dialog

class CSelectPrinterDlg : public CDialog
{
// Construction
public:
	class CPrinterItem : public ItiLibrary::CListCtrlImp::CItem
	{
	public:
		CPrinterItem (const CString & strDriver, const CString & strName, const CString & strPort);
		virtual ~CPrinterItem ();

		virtual CString GetDispText (int nColumn) const;

		CString m_strDriver;
		CString m_strName;
		CString m_strPort;
	};

	CSelectPrinterDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectPrinterDlg ();

	CArray <CPrinterItem *, CPrinterItem *> m_vPrinters;
	CString m_strDriver;
	CString m_strPort;

// Dialog Data
	//{{AFX_DATA(CSelectPrinterDlg)
	enum { IDD = IDD_SELECTPRINTER };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectPrinterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	ItiLibrary::CListCtrlImp m_lv;

	virtual void OnOK();
	afx_msg void OnDblclkPrinters(NMHDR* pNMHDR, LRESULT* pResult);

	// Generated message map functions
	//{{AFX_MSG(CSelectPrinterDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTPRINTER_H__1A0F13CC_0A86_40AA_ABCA_0469ABBDCC40__INCLUDED_)
