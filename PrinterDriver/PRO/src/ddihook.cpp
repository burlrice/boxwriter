//  THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
//  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
//  PARTICULAR PURPOSE.
//
//  Copyright  1998 - 2003  Microsoft Corporation.  All Rights Reserved.
//
//  FILE:    DDIHook.cpp
//    
//
//  PURPOSE:  Implementation of DDI Hook OEMEndDoc. This function
//          dumps the buffered bitmap data out. 
//
//
//  Functions:
//          OEMEndDoc
//
//      
//
//
//  PLATFORMS:  Windows XP, Windows Server 2003, Windows codenamed Longhorn
//
//
//  History: 
//          06/24/03    xxx created.
//
//

#include "precomp.h"
#include <WINSPOOL.H>
#include <Winsplp.h>
#include <Winddiui.h>
#include <math.h>
#include "ddihook.h"
#include "bitmap.h"
#include "debug.h"
#include "strsafe.h"
#include "Ioctls.h"
#include "TypeDefs.h"
#include "DevGuids.h"
#include "..\..\MPHCAPI\Master.h"
#include "..\..\Editor\DLL\Common\DbTypeDefs.h"

#ifndef PI
	#define PI			3.1415926535897932384626433832795
#endif

#define DEG2RAD( n )	n * ((PI) / 180.00)			// degrees to radians
#define RAD2DEG( n )	((n * 180) / (PI))			// radian to degrees

template <class T>
T BindTo (T value, T lBound, T uBound)
{
	return max (min (value, uBound), lBound);
}

int Round (double d) 
{
	double dFloor = floor (d);
	int n = (int)((d - dFloor) * 100.0);

	return (int)dFloor + (n >= 50 ? 1 : 0);
}

static const GUID guid = GUID_INTERFACE_FXMPHC;
static const LPCWSTR lpszRootSection = _T ("SOFTWARE\\FoxJet_Driver");
static const LPCWSTR lpszSection [] = 
{
	_T ("SOFTWARE\\FoxJet_Driver\\Head1"),
	_T ("SOFTWARE\\FoxJet_Driver\\Head2"),
	_T ("SOFTWARE\\FoxJet_Driver\\Head3"),
	_T ("SOFTWARE\\FoxJet_Driver\\Head4"),
	_T ("SOFTWARE\\FoxJet_Driver\\Head5"),
	_T ("SOFTWARE\\FoxJet_Driver\\Head6"),
};

struct
{
	HANDLE m_hDevice;
	WCHAR m_szFriendlyName	[_MAX_PATH];
	WCHAR m_szLinkName		[_MAX_PATH];
	ULONG m_lAddress;
}
static cards [6] = 
{
	{ NULL, _T (""), _T (""), 0x300 },
	{ NULL, _T (""), _T (""), 0x310 },
	{ NULL, _T (""), _T (""), 0x320 },
	{ NULL, _T (""), _T (""), 0x330 },
	{ NULL, _T (""), _T (""), 0x340 },
	{ NULL, _T (""), _T (""), 0x350 },
};
static const double dChannelSeparation = 0.05848; 

//////////////////////////////////////////////////////////////////////////////////////////

DWORD GetProfileDWORD (LPCWSTR lpszSection, LPCWSTR lpszKey, DWORD dwDefault = 0);
bool GetProfileString (LPCWSTR lpszSection, LPCWSTR lpszKey, LPWSTR lpsz, ULONG lLen);
void RotateBlt (LPBYTE lpDest, int nXDest, int nYDest, int nWidthDest, int nHeightDest,
				LPBYTE lpSrc,  int nXSrc,  int nYSrc,  int nWidthSrc,  int nHeightSrc);
void RotateBlt (LPBYTE lpDest, int nXDest, int nYDest, int nWidthDest, int nHeightDest, int nXDpiDest, int nYDpiDest, 
				LPBYTE lpSrc,  int nXSrc,  int nYSrc,  int nWidthSrc,  int nHeightSrc, int nXDpiSrc, int nYDpiSrc,
				int nHeadType);

HANDLE GetDevice (ULONG lAddr); 
HANDLE GetDevice (LPCWSTR lpsz); 
int InitMphc ();
void FreeMphc ();

bool Activate (HANDLE hDevice, ULONG lAddr);
bool IsFpgaLoaded (HANDLE hDevice, ULONG lAddr);
bool LoadFpga (HANDLE hDevice, ULONG lAddr);
int ProgramHeadSettings (LPCWSTR lpszSection, HANDLE hDevice, ULONG lAddr, UINT & nFlush);
bool ReadConfiguration (HANDLE hDevice, ULONG lAddr, tagRegisters & tConfig);
bool WriteSetup (HANDLE hDevice, ULONG lAddr, tagSetupRegs tSetup, tagFireVibPulse tPulseRegs);
bool EnablePrint (HANDLE hDevice, ULONG lAddr, bool bEnabled);
bool RegisterEOPEvent (HANDLE hDevice, ULONG lAddr, HANDLE hEvent, bool bRegister);
bool RegisterPhotoEvent (HANDLE hDevice, ULONG lAddr, HANDLE hEvent, bool bRegister);
bool UpdateImageData (HANDLE hDevice, ULONG lAddr, LPVOID pImageBuffer, unsigned char nBPC, WORD wStartCol, WORD wNumCols);
int CalculateNFactor (int nHeadType, int nHorzRes, double angle, double res);
double GetRes (int nHorzRes, int nEncoderDivisor);
int GetPhcChannels (int nHeadType, int nChannels);
void CalcProductLen (ULONG & lMaxPhotocellDelay, ULONG & lMinPhotocellDelay, UINT & nMaxFlush);
UINT CalcFlushBuffer (LPCWSTR lpszSection);

//////////////////////////////////////////////////////////////////////////////////////////

HANDLE GetDevice (LPCWSTR lpsz) 
{
	return GetDevice (wcstoul (lpsz, NULL, 16)); 
}

HANDLE GetDevice (ULONG lAddr) 
{
	for (int i = 0; i < ARRAYSIZE (::cards); i++) 
		if (::cards [i].m_lAddress == lAddr)
			return ::cards [i].m_hDevice;

	return NULL;
}

bool Activate (HANDLE hDevice, ULONG lAddr)
{
	OEMDBG(DBG_VERBOSE, L"Activate");
	DWORD dwBytes = 0;

	if (!DeviceIoControl (hDevice, IOCTL_ACTIVATE, &lAddr, sizeof (lAddr), NULL, 0, &dwBytes, NULL)) {
		DebugMsg (DLLTEXT ("IOCTL_ACTIVATE failed: 0x%X"), lAddr);
		return false;
	}

	return true;
}

bool IsFpgaLoaded (HANDLE hDevice, ULONG lAddr)
{
	OEMDBG(DBG_VERBOSE, L"IsFpgaLoaded");

	PHCB inCtrlBlock;
	PHCB outCtrlBlock;
	DWORD dwBytes = 0;
	tagRegisters tConfig;

	FillMemory (&tConfig, sizeof (tConfig), 0x00);
	FillMemory (&inCtrlBlock, sizeof (inCtrlBlock), 0);
	inCtrlBlock.lAddress = lAddr;
	inCtrlBlock.RegID = 0x00;
	inCtrlBlock.TxBytes = sizeof (tConfig);
	CopyMemory (&outCtrlBlock, &inCtrlBlock, sizeof (PHCB));

	if (DeviceIoControl (hDevice, IOCTL_READ_CTRL_REG, &inCtrlBlock, sizeof(PHCB), &outCtrlBlock, sizeof (PHCB), &dwBytes, NULL)) {
		CopyMemory (&tConfig, &outCtrlBlock.Data, outCtrlBlock.TxBytes);
		DebugMsg (DLLTEXT ("head type: %d"), tConfig._CtrlReg._HeadType);

		if (tConfig._CtrlReg._HeadType != 0)
			return true;
	}

	return false;
}

bool LoadFpga (HANDLE hDevice, ULONG lAddr)
{
	OEMDBG(DBG_VERBOSE, L"LoadFpga");

	DWORD dwBytes = 0;
	WCHAR szFile [_MAX_PATH] = { 0 };
	bool bResult = false;

	GetProfileString (lpszRootSection, _T ("FPGA"), szFile, ARRAYSIZE (szFile));

	HANDLE h = ::CreateFile (szFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL); 

	if (h != INVALID_HANDLE_VALUE) {
		DWORD dwLenHigh = 0;
		DWORD dwLen = GetFileSize (h, &dwLenHigh);
		HGLOBAL hBuffer = GlobalAlloc (GPTR, dwLen + sizeof (ULONG));
		PUCHAR p = (PUCHAR)GlobalLock (hBuffer);
		PUCHAR p3 = p;
		PULONG p2 = (PULONG)p;

		DebugMsg (DLLTEXT ("%s: (%d bytes)"), szFile, dwLen);

		*p2 = lAddr;
		p3 = (PUCHAR)(p + sizeof (ULONG));
		
		BOOL bReadFile = ReadFile (h, p3, dwLen, &dwBytes, NULL);
		
		if (!bReadFile)
			DebugMsg (DLLTEXT ("failed to read file: %s"), szFile);
		else
			DebugMsg (DLLTEXT ("read %d bytes"), dwBytes);

		dwLen += sizeof (ULONG);
		CloseHandle (h);

		bResult = DeviceIoControl (hDevice, IOCTL_WRITE_FPGA, p, dwLen, NULL, 0, &dwBytes, NULL) ? true : false;
		
		GlobalUnlock (hBuffer);
		GlobalFree (hBuffer);
	}
	else
		DebugMsg (DLLTEXT ("failed to open file: %s"), szFile);

	return bResult; 
}

bool ReadConfiguration (HANDLE hDevice, ULONG lAddr, tagRegisters & tConfig)
{
	ULONG dwBytes = 0;
	PHCB inCtrlBlock;
	PHCB outCtrlBlock;
	bool bResult = false;

	FillMemory (&tConfig, sizeof (tagRegisters), 0);

	inCtrlBlock.lAddress	= lAddr;
	inCtrlBlock.RegID		= 0x00;
	inCtrlBlock.TxBytes		= sizeof (tConfig);

	CopyMemory (&outCtrlBlock, &inCtrlBlock, sizeof (PHCB));
	
	if (DeviceIoControl (hDevice, IOCTL_READ_CTRL_REG, &inCtrlBlock, sizeof(PHCB), &outCtrlBlock, sizeof (PHCB), &dwBytes, NULL)) {
		bResult = true;
		CopyMemory (&tConfig, &outCtrlBlock.Data, outCtrlBlock.TxBytes);
	}

	return bResult;
}

bool WriteSetup (HANDLE hDevice, ULONG lAddr, tagSetupRegs tSetup, tagFireVibPulse tPulseRegs)
{
	OEMDBG(DBG_VERBOSE, L"WriteSetup");

	ULONG dwBytes;
	PHCB inCtrlBlock;
	tagRegisters tCurrent;
	bool bResult = false;

	inCtrlBlock.lAddress = lAddr;

    if (ReadConfiguration (hDevice, lAddr, tCurrent)) {
		tSetup._Interrupt_Reg = tCurrent._Interrupt_Reg;
		tSetup._CtrlReg._PrintEnable = tCurrent._CtrlReg._PrintEnable;
		tSetup._CtrlReg._FireAMS = 0x00;

		inCtrlBlock.RegID	= 0x00;
		inCtrlBlock.TxBytes = sizeof (tSetup);
		CopyMemory (&inCtrlBlock.Data, &tSetup, sizeof (tSetup));

		if (DeviceIoControl (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof (PHCB), NULL, 0, &dwBytes, NULL)) {
			inCtrlBlock.RegID = 0x10;
			inCtrlBlock.TxBytes = sizeof ( tagFireVibPulse );
			CopyMemory (&inCtrlBlock.Data, &tPulseRegs, inCtrlBlock.TxBytes);

			if (DeviceIoControl (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof (PHCB), NULL, 0, &dwBytes, NULL)) {
				DebugMsg (DLLTEXT ("WriteSetup succeeded: 0x%X"), lAddr);
				bResult = true;
			}
			else 
				DebugMsg (DLLTEXT ("failed to write tPulseRegs: 0x%X"), lAddr);
		}
		else 
			DebugMsg (DLLTEXT ("failed to write tSetup: 0x%X"), lAddr);
	}
	else
		DebugMsg (DLLTEXT ("failed to read config: 0x%X"), lAddr);
	
	return bResult;
}

int CalculateNFactor (int nHeadType, int nHorzRes, double angle, double res)
{
	double NFactor;
	int nAngle = (int)(angle * 100.0);

	if (nAngle >= 90.0) {
		switch (nHeadType) {
		case FoxjetDatabase::GRAPHICS_768_256: 
		case FoxjetDatabase::GRAPHICS_384_128:
		case FoxjetDatabase::UJ2_352_32:
		{
			if (nHorzRes == 426)
				return 8;

			return 6;
		}
		case FoxjetDatabase::GRAPHICS_768_256_L310:
			return 46;

		case FoxjetDatabase::IV_72:
		{
			// 0.192" is channel separation for valve heads (0.040" for others?)
			int n = (int)(0.192 * res); // 0.192 / (1 / res)

			return n;
		}
		default:
			return 0;
		}
	}

	NFactor = 0.05848 * cos ( DEG2RAD(angle) );
//	NFactor *= res / 2;
	NFactor *= res;
	NFactor += .5;

	int t = ( int ) floor(NFactor);

	return t;
}

double GetRes (int nHorzRes, int nEncoderDivisor)
{
	double dRes = (double)nHorzRes / max (1.0, (double)nEncoderDivisor);

	if (nEncoderDivisor == 3)
		dRes *= 2.0;

	return dRes;
}

int GetPhcChannels (int nHeadType, int nChannels)
{
	if (nHeadType == FoxjetDatabase::GRAPHICS_384_128)
		return 256;

	return nChannels;
}

int ProgramHeadSettings (LPCWSTR lpszSection, HANDLE hDevice, ULONG lAddr, UINT & nFlush)
{
	OEMDBG(DBG_VERBOSE, L"ProgramHeadSettings");

	WCHAR szName [128] = { 0 };
	WCHAR szUID [16] = { 0 };
	tagSetupRegs tConfig;
	tagFireVibPulse tPulseRegs;

	GetProfileString (lpszSection, _T ("Name"), szName, ARRAYSIZE (szName));
	GetProfileString (lpszSection, _T ("UID"), szUID, ARRAYSIZE (szUID));

	DebugMsg (DLLTEXT ("ProgramHeadSettings: %s %s"), szName, szUID);

	UINT nChannels			= GetProfileDWORD (lpszSection, _T ("Channels"));
	UINT nHorzRes			= GetProfileDWORD (lpszSection, _T ("HorzRes"));
	ULONG lPhotocellDelay	= GetProfileDWORD (lpszSection, _T ("PhotocellDelay"));
	BOOL bEnabled			= GetProfileDWORD (lpszSection, _T ("Enabled"));
	int nEncoder			= GetProfileDWORD (lpszSection, _T ("Encoder"));
	int nDirection			= GetProfileDWORD (lpszSection, _T ("Direction"));
	double dHeadAngle		= (double)GetProfileDWORD (lpszSection, _T ("Angle")) / 100.0;
	bool bInverted			= GetProfileDWORD (lpszSection, _T ("Inverted")) ? true : false;
	int nPhotocell			= GetProfileDWORD (lpszSection, _T ("Photocell"));
	int nSharePhoto			= GetProfileDWORD (lpszSection, _T ("SharePhotocell"));
	int nShareEnc			= GetProfileDWORD (lpszSection, _T ("ShareEncoder"));
	int nHeadType			= GetProfileDWORD (lpszSection, _T ("HeadType"));
	bool bMasterHead		= GetProfileDWORD (lpszSection, _T ("MasterHead")) ? true : false;
	int nIntTachSpeed		= GetProfileDWORD (lpszSection, _T ("IntTachSpeed"));
	double dNozzleSpan		= (double)GetProfileDWORD (lpszSection, _T ("NozzleSpan")) / 100.0;
	double dPhotocellRate	= (double)GetProfileDWORD (lpszSection, _T ("PhotocellRate")) / 100.0;
	int nEncoderDivisor		= GetProfileDWORD (lpszSection, _T ("EncoderDivisor"));
	bool bDoublePulse		= GetProfileDWORD (lpszSection, _T ("DoublePulse")) ? true : false;

	UNREFERENCED_PARAMETER(nDirection);
	UNREFERENCED_PARAMETER(nChannels);
	UNREFERENCED_PARAMETER(dHeadAngle );
	UNREFERENCED_PARAMETER(bMasterHead);
	UNREFERENCED_PARAMETER(dNozzleSpan);

	if (!bEnabled)
		return false;

	{ // CHeadCfg::ToRegisters
		FillMemory (&tConfig, sizeof (tConfig), 0);
		FillMemory (&tPulseRegs, sizeof (tPulseRegs), 0);

		tConfig._CtrlReg._SingleBuffer		= 1;
		tConfig._CtrlReg._PrintBuffer		= 0;
		tConfig._DividerCtrlReg._SClkRate	= 1;
		tConfig._CtrlReg._FireAMS			= 0x00;
		tConfig._CtrlReg._DoublePulse		= bDoublePulse;
		tConfig._DividerCtrlReg._Reserved = 0; //pHead->GetStandbyMode () ? 0 : 1;

		switch (nEncoderDivisor) {
		default:
		case 2:
			tConfig._DividerCtrlReg._TachDiv = 0x00; // 150 dpi
			break;
		case 3:
			tConfig._DividerCtrlReg._TachDiv = 0x01; // 200 dpi 
			break;
		case 1:
			tConfig._DividerCtrlReg._TachDiv = 0x02; // 300 dpi
			break;
		}

		// Note: 29.4912 MHz is the clock rate with which pulse widths are base off of.
		// Thus every 29.4912 clock cycles is 1 Micro-Second.
		switch (nHeadType)
		{
			case FoxjetDatabase::ALPHA_CODER: // 17/5/6
				tPulseRegs._DampPulseDelay = (WORD) (6.0 * 29.4912);
				tPulseRegs._DampPulseWidth = (WORD) (5.0 * 29.4912);
				tPulseRegs._FirePulseDelay = (WORD) (0.0 * 29.4912);
				tPulseRegs._FirePulseWidth	= (WORD) (17.0 * 29.4912);
				tConfig._CtrlReg._HeadType = 1; //2;
				break;
			case FoxjetDatabase::UJ2_192_32:
			case FoxjetDatabase::UJ2_192_32NP:
				tPulseRegs._DampPulseDelay = (WORD) (5.0 * 29.4912);
				tPulseRegs._DampPulseWidth = (WORD) (6.0 * 29.4912);
				tPulseRegs._FirePulseDelay = (WORD) (1.0 * 29.4912);
				tPulseRegs._FirePulseWidth	= (WORD) (13.5 * 29.4912);
				tConfig._CtrlReg._HeadType = 1;
				break;
			case FoxjetDatabase::UJ2_96_32:
				tPulseRegs._DampPulseDelay = (WORD) (0.0 * 29.4912);
				tPulseRegs._DampPulseWidth = (WORD) (0.0 * 29.4912);
				tPulseRegs._FirePulseDelay = (WORD) (0.0 * 29.4912);
				tPulseRegs._FirePulseWidth = (WORD) (17.0 * 29.4912);
				tConfig._CtrlReg._HeadType = 1;
				break;
			case FoxjetDatabase::UJ2_352_32:
				tPulseRegs._DampPulseDelay = (WORD) (5.0 * 29.4912);
				tPulseRegs._DampPulseWidth = (WORD) (6.0 * 29.4912);
				tPulseRegs._FirePulseDelay = (WORD) (1.0 * 29.4912);
				tPulseRegs._FirePulseWidth = (WORD) (13.5 * 29.4912);
				tConfig._CtrlReg._HeadType = 2;
				break;
			case FoxjetDatabase::GRAPHICS_384_128:
			case FoxjetDatabase::GRAPHICS_768_256:
			case FoxjetDatabase::GRAPHICS_768_256_L310:
				tPulseRegs._DampPulseDelay = (WORD) (0.0 * 29.4912);
				tPulseRegs._DampPulseWidth = (WORD) (0.0 * 29.4912);
				tPulseRegs._FirePulseDelay = (WORD) (0.0 * 29.4912);
				tPulseRegs._FirePulseWidth = (WORD) (5.0 * 29.4912);
				tConfig._CtrlReg._HeadType = 3;
				break;
			case FoxjetDatabase::UJI_96_32: // normal 17 microsecond pulse
				tPulseRegs._DampPulseDelay = (WORD) (0.0 * 29.4912);
				tPulseRegs._DampPulseWidth = (WORD) (0.0 * 29.4912);
				tPulseRegs._FirePulseDelay = (WORD) (0.0 * 29.4912);
				tPulseRegs._FirePulseWidth = (WORD) (17.0 * 29.4912);
				tConfig._CtrlReg._HeadType = 1;
				break;
			case FoxjetDatabase::UJI_192_32: // 17/4/4 
				tPulseRegs._DampPulseDelay = (WORD) (4.0 * 29.4912);
				tPulseRegs._DampPulseWidth = (WORD) (4.0 * 29.4912);
				tPulseRegs._FirePulseDelay = (WORD) (0.0 * 29.4912);
				tPulseRegs._FirePulseWidth = (WORD) (17.0 * 29.4912);
				tConfig._CtrlReg._HeadType = 1;
				break;
			case FoxjetDatabase::UJI_256_32: // 13.5/5/7 
				tPulseRegs._DampPulseDelay = (WORD) (7.0 * 29.4912);
				tPulseRegs._DampPulseWidth = (WORD) (5.0 * 29.4912);
				tPulseRegs._FirePulseDelay = (WORD) (0.0 * 29.4912);
				tPulseRegs._FirePulseWidth = (WORD) (13.5 * 29.4912);
				tConfig._CtrlReg._HeadType = 1;
				break;
		}
		tConfig._CtrlReg._Inverted = bInverted;

		switch (nHeadType)
		{
			case FoxjetDatabase::ALPHA_CODER:			DebugMsg (DLLTEXT ("ALPHA_CODER"));				break;	 
			case FoxjetDatabase::UJ2_192_32:			DebugMsg (DLLTEXT ("UJ2_192_32"));				break;	
			case FoxjetDatabase::UJ2_192_32NP:			DebugMsg (DLLTEXT ("UJ2_192_32NP"));			break;	
			case FoxjetDatabase::UJ2_96_32:				DebugMsg (DLLTEXT ("UJ2_96_32"));				break;	
			case FoxjetDatabase::UJ2_352_32:			DebugMsg (DLLTEXT ("UJ2_352_32"));				break;	
			case FoxjetDatabase::GRAPHICS_384_128:		DebugMsg (DLLTEXT ("GRAPHICS_384_128"));		break;	
			case FoxjetDatabase::GRAPHICS_768_256:		DebugMsg (DLLTEXT ("GRAPHICS_768_256"));		break;	
			case FoxjetDatabase::GRAPHICS_768_256_L310:	DebugMsg (DLLTEXT ("GRAPHICS_768_256_L310"));	break;	
			case FoxjetDatabase::UJI_96_32:				DebugMsg (DLLTEXT ("UJI_96_32"));				break;	
			case FoxjetDatabase::UJI_192_32:			DebugMsg (DLLTEXT ("UJI_192_32"));				break;	
			case FoxjetDatabase::UJI_256_32:			DebugMsg (DLLTEXT ("UJI_256_32"));				break;
		}

		DebugMsg (DLLTEXT ("tPulseRegs._DampPulseDelay: %d"), tPulseRegs._DampPulseDelay);
		DebugMsg (DLLTEXT ("tPulseRegs._DampPulseWidth: %d"), tPulseRegs._DampPulseWidth);
		DebugMsg (DLLTEXT ("tPulseRegs._FirePulseDelay: %d"), tPulseRegs._FirePulseDelay);
		DebugMsg (DLLTEXT ("tPulseRegs._FirePulseWidth: %d"), tPulseRegs._FirePulseWidth);
		DebugMsg (DLLTEXT ("tConfig._CtrlReg._HeadType: %d"), tConfig._CtrlReg._HeadType);

		switch (nEncoder)
		{
			case FoxjetDatabase::EXTERNAL_ENC:	// External
				switch (nShareEnc)
				{
					case 0:	// Not Shared
						tConfig._InterfaceCtrlReg._TachSrc = 0;
						tConfig._InterfaceCtrlReg._OTBTach1 = 0;
						tConfig._InterfaceCtrlReg._OTBTach2 = 0;
						break;
					case 1:	// Shared as A or 1
						tConfig._InterfaceCtrlReg._TachSrc = 0;
						tConfig._InterfaceCtrlReg._OTBTach1 = 1;
						tConfig._InterfaceCtrlReg._OTBTach2 = 0;
						break;
					case 2:	// Shared as B or 2
						tConfig._InterfaceCtrlReg._TachSrc = 0;
						tConfig._InterfaceCtrlReg._OTBTach1 = 0;
						tConfig._InterfaceCtrlReg._OTBTach2 = 1;
						break;
				}
				break;
			case FoxjetDatabase::INTERNAL_ENC:	// Internal
				switch (nShareEnc)
				{
					case 0:	// Not Shared
						tConfig._InterfaceCtrlReg._TachSrc = 1;
						tConfig._InterfaceCtrlReg._OTBTach1 = 0;
						tConfig._InterfaceCtrlReg._OTBTach2 = 0;
						break;
					case 1:	// Shared as A or 1
						tConfig._InterfaceCtrlReg._TachSrc = 1;
						tConfig._InterfaceCtrlReg._OTBTach1 = 1;
						tConfig._InterfaceCtrlReg._OTBTach2 = 0;
						break;
					case 2:	// Shared as B or 2
						tConfig._InterfaceCtrlReg._TachSrc = 1;
						tConfig._InterfaceCtrlReg._OTBTach1 = 0;
						tConfig._InterfaceCtrlReg._OTBTach2 = 1;
						break;
				}
				break;
			case FoxjetDatabase::SHARED_ENC:	// Shared Encoder
				switch (nShareEnc)
				{
					case 0:	// Not Shared  This is a invalid state.  We will force to OTB1
						tConfig._InterfaceCtrlReg._TachSrc = 2;
						tConfig._InterfaceCtrlReg._OTBTach1 = 0;
						tConfig._InterfaceCtrlReg._OTBTach2 = 0;
						break;
					case 1:	// Shared as A or 1
						tConfig._InterfaceCtrlReg._TachSrc = 2;
						tConfig._InterfaceCtrlReg._OTBTach1 = 0;
						tConfig._InterfaceCtrlReg._OTBTach2 = 0;
						break;
					case 2:	// Shared as B or 2
						tConfig._InterfaceCtrlReg._TachSrc = 3;
						tConfig._InterfaceCtrlReg._OTBTach1 = 0;
						tConfig._InterfaceCtrlReg._OTBTach2 = 0;
						break;
				}
				break;
		}

		switch (nPhotocell)
		{
			case FoxjetDatabase::EXTERNAL_PC:	// External
				switch (nSharePhoto)
				{
					case 0:	// Not Shared
						tConfig._InterfaceCtrlReg._PhotoSrc = 0;
						tConfig._InterfaceCtrlReg._OTBPhoto1 = 0;
						tConfig._InterfaceCtrlReg._OTBPhoto2 = 0;
						break;
					case 1:	// Shared as A or 1
						tConfig._InterfaceCtrlReg._PhotoSrc = 0;
						tConfig._InterfaceCtrlReg._OTBPhoto1 = 1;
						tConfig._InterfaceCtrlReg._OTBPhoto2 = 0;
						break;
					case 2:	// Shared as B or 2
						tConfig._InterfaceCtrlReg._PhotoSrc = 0;
						tConfig._InterfaceCtrlReg._OTBPhoto1 = 0;
						tConfig._InterfaceCtrlReg._OTBPhoto2 = 1;
						break;
				}
				break;
			case FoxjetDatabase::INTERNAL_PC:	// Internal
				switch (nSharePhoto)
				{
					case 0:	// Not Shared
						tConfig._InterfaceCtrlReg._PhotoSrc		= 1;
						tConfig._InterfaceCtrlReg._OTBPhoto1	= 0;
						tConfig._InterfaceCtrlReg._OTBPhoto2	= 0;
						break;
					case 1:	// Shared as A or 1
						tConfig._InterfaceCtrlReg._PhotoSrc		= 1;
						tConfig._InterfaceCtrlReg._OTBPhoto1	= 1;
						tConfig._InterfaceCtrlReg._OTBPhoto2	= 0;
						break;
					case 2:	// Shared as B or 2
						tConfig._InterfaceCtrlReg._PhotoSrc		= 1;
						tConfig._InterfaceCtrlReg._OTBPhoto1	= 0;
						tConfig._InterfaceCtrlReg._OTBPhoto2	= 1;
						break;
				}
				break;
			case FoxjetDatabase::SHARED_PC:	// Using a shared photocell
				switch (nSharePhoto)
				{
					case 0:	// Not Shared This is an invalid state  We will force to OTB1
						tConfig._InterfaceCtrlReg._PhotoSrc = 2;
						tConfig._InterfaceCtrlReg._OTBPhoto1 = 0;
						tConfig._InterfaceCtrlReg._OTBPhoto2 = 0;
						break;
					case 1:	// Shared as A or 1
						tConfig._InterfaceCtrlReg._PhotoSrc = 2;
						tConfig._InterfaceCtrlReg._OTBPhoto1 = 0;
						tConfig._InterfaceCtrlReg._OTBPhoto2 = 0;
						break;
					case 2:	// Shared as B or 2
						tConfig._InterfaceCtrlReg._PhotoSrc = 3;
						tConfig._InterfaceCtrlReg._OTBPhoto1 = 0;
						tConfig._InterfaceCtrlReg._OTBPhoto2 = 0;
						break;
				}
				break;
		}

		double dTemp =  ((double)nIntTachSpeed / 5.0);
		dTemp *= (double)nHorzRes;
		dTemp = (dTemp / (double)1843200) * (double)65536.0;

		tConfig._IntTachGenReg._TachGenFreg = (unsigned short)dTemp;

		// Note:
		// Internal Photocell is ( Inches desired * (Encoder Resolution / 2))  / 2 
		// Encoder is specified in real resolution.  This is always divided by .
		// The final divide by 2 is because the hardware duty-cycles this by 2, thus
		// its minimum period is 60us.
		tConfig._IntPhotoGenReg._PhotoGenFreg = (USHORT)((dPhotocellRate * ((double)nHorzRes / 2.0)) /  2.0);

		DebugMsg (DLLTEXT ("tConfig._IntTachGenReg._TachGenFreg:   %d"), tConfig._IntTachGenReg._TachGenFreg);
		DebugMsg (DLLTEXT ("tConfig._IntPhotoGenReg._PhotoGenFreg: %d"), tConfig._IntPhotoGenReg._PhotoGenFreg);
	}

	/*
	if (bMasterHead)
		if (!DeviceIoControl (hDevice, IOCTL_SET_MASTER_MODE, &lAddr, sizeof (lAddr), NULL, 0, &dwBytes, NULL))
			return false;
	*/

	nFlush = CalcFlushBuffer (lpszSection);

	if (!WriteSetup (hDevice, lAddr, tConfig, tPulseRegs))
		return false;

	{ // CMphc::SetSpeedGateWidth
		DWORD dwBytes;
		PHCB inCtrlBlock;

		inCtrlBlock.lAddress	= lAddr;
		inCtrlBlock.RegID		= 0x0E;
		inCtrlBlock.TxBytes		= sizeof (WORD);
		
		WORD wSpeed = (USHORT)((double)(FTACH_100 * FGATE ) / (double) (20.0 * nHorzRes));
		CopyMemory (&inCtrlBlock.Data, &wSpeed, sizeof (WORD));

		DebugMsg (DLLTEXT ("wSpeed: %d"), wSpeed);

		if (!DeviceIoControl (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof (PHCB), NULL, 0, &dwBytes, NULL)) {
			DebugMsg (DLLTEXT ("failed to write speed [%d]: %s %s"), wSpeed, szName, szUID);
			return false;
		}

	}

	{ // CMphc::SetPrintDelay
		WORD wPrintDly = (WORD)(((double)lPhotocellDelay / (double)1000) * ((double)nHorzRes / (double)nEncoderDivisor));
		DWORD dwBytes;
		PHCB inCtrlBlock;

		inCtrlBlock.lAddress	= lAddr;
		inCtrlBlock.RegID		= 0x20;
		inCtrlBlock.TxBytes		= sizeof (WORD);
		inCtrlBlock.Data [0]	= LOBYTE (wPrintDly);
		inCtrlBlock.Data [1]	= HIBYTE (wPrintDly);

		DebugMsg (DLLTEXT ("wPrintDly: %d (lPhotocellDelay: %d, nHorzRes: %d, nEncoderDivisor: %d)"), 
			wPrintDly,
			lPhotocellDelay, 
			nHorzRes, 
			nEncoderDivisor);

		if (!DeviceIoControl (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof (PHCB), NULL, 0, &dwBytes, NULL)) {
			DebugMsg (DLLTEXT ("failed to write print delay [%d]: %s %s"), wPrintDly, szName, szUID);
			return false;
		}
	}

	DebugMsg (DLLTEXT ("ProgramHeadSettings succeeded: %s %s"), szName, szUID);

	return true;
}

void ProgramSlantRegs (LPCWSTR lpszSection, HANDLE hDevice, ULONG lAddr, DWORD dwWidth, DWORD dwHeight)
{ 
	OEMDBG(DBG_VERBOSE, L"CalcFlushBuffer");

	UINT nChannels			= GetProfileDWORD (lpszSection, _T ("Channels"));
	UINT nHorzRes			= GetProfileDWORD (lpszSection, _T ("HorzRes"));
	ULONG lPhotocellDelay	= GetProfileDWORD (lpszSection, _T ("PhotocellDelay"));
	BOOL bEnabled			= GetProfileDWORD (lpszSection, _T ("Enabled"));
	int nEncoder			= GetProfileDWORD (lpszSection, _T ("Encoder"));
	int nDirection			= GetProfileDWORD (lpszSection, _T ("Direction"));
	double dHeadAngle		= (double)GetProfileDWORD (lpszSection, _T ("Angle")) / 100.0;
	bool bInverted			= GetProfileDWORD (lpszSection, _T ("Inverted")) ? true : false;
	int nPhotocell			= GetProfileDWORD (lpszSection, _T ("Photocell"));
	int nSharePhoto			= GetProfileDWORD (lpszSection, _T ("SharePhotocell"));
	int nShareEnc			= GetProfileDWORD (lpszSection, _T ("ShareEncoder"));
	int nHeadType			= GetProfileDWORD (lpszSection, _T ("HeadType"));
	bool bMasterHead		= GetProfileDWORD (lpszSection, _T ("MasterHead")) ? true : false;
	int nIntTachSpeed		= GetProfileDWORD (lpszSection, _T ("IntTachSpeed"));
	double dNozzleSpan		= (double)GetProfileDWORD (lpszSection, _T ("NozzleSpan")) / 100.0;
	double dPhotocellRate	= (double)GetProfileDWORD (lpszSection, _T ("PhotocellRate")) / 100.0;
	int nEncoderDivisor		= GetProfileDWORD (lpszSection, _T ("EncoderDivisor"));
	bool bDoublePulse		= GetProfileDWORD (lpszSection, _T ("DoublePulse")) ? true : false;
	double dRes				= GetRes (nHorzRes, nEncoderDivisor);

	UNREFERENCED_PARAMETER (bEnabled);
	UNREFERENCED_PARAMETER (bDoublePulse);
	UNREFERENCED_PARAMETER (nSharePhoto);
	UNREFERENCED_PARAMETER (nPhotocell);
	UNREFERENCED_PARAMETER (nShareEnc);
	UNREFERENCED_PARAMETER (lPhotocellDelay);
	UNREFERENCED_PARAMETER (dPhotocellRate);
	UNREFERENCED_PARAMETER (bMasterHead);
	UNREFERENCED_PARAMETER (dNozzleSpan);
	UNREFERENCED_PARAMETER (nEncoder);
	UNREFERENCED_PARAMETER (nIntTachSpeed);

	int nWidthBytes			= abs ((((int)dwWidth + 31) / 32) * 4);
	int nFlush				= CalcFlushBuffer (lpszSection);
	int ippc				= GetPhcChannels (nHeadType, nChannels);
	int ibpc				= (BYTE)(ippc / 8);
	int ilen				= nWidthBytes * dwHeight;
	int N_Factor			= CalculateNFactor (nHeadType, nHorzRes, dHeadAngle, dRes);
	INT nWordsPerCol		= ibpc / 2;
	WORD wImageCols			= (WORD)((ULONG)ilen / (ULONG)ibpc);
	tagSlantRegs SlantRegs;

	UCHAR state = (nDirection == FoxjetDatabase::LTOR) ? 1 : 0;
	state |= bInverted ? 0x02 : 0x00;

	switch ( state ) 
	{
		case 0:	// R->L Normal
		case 3:	// L->R Inverted
			switch (nHeadType)
			{
				case FoxjetDatabase::ALPHA_CODER:
				case FoxjetDatabase::UJ2_192_32:
				case FoxjetDatabase::UJ2_192_32NP:
				case FoxjetDatabase::UJ2_96_32:
				case FoxjetDatabase::UJI_96_32:
				case FoxjetDatabase::UJI_192_32:
				case FoxjetDatabase::UJI_256_32:
					//flush = (31 * N_Factor * nWordsPerCol); 
					SlantRegs._ImageStartAddr._Addr1	= (WORD) 0x0000;
					SlantRegs._ImageStartAddr._Addr2	= (WORD) ( -nWordsPerCol * N_Factor );
					SlantRegs._AddrIncFactor._Incr		= (WORD) ( -2 * nWordsPerCol * N_Factor );
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) ( 32 * nWordsPerCol * N_Factor );
					break;
				case FoxjetDatabase::UJ2_352_32:
					//flush = N_Factor * nWordsPerCol; 
					SlantRegs._ImageStartAddr._Addr1	= (WORD) 0x0000;
					SlantRegs._ImageStartAddr._Addr2	= (WORD) (-nWordsPerCol * N_Factor);
					SlantRegs._AddrIncFactor._Incr		= (WORD) 0x0000;
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) 0x0000;
					break;
				case FoxjetDatabase::GRAPHICS_384_128:
				case FoxjetDatabase::GRAPHICS_768_256:
				case FoxjetDatabase::GRAPHICS_768_256_L310:
					//flush = N_Factor * nWordsPerCol; 
					SlantRegs._ImageStartAddr._Addr2	= (WORD) 0x0000;
					SlantRegs._ImageStartAddr._Addr1	= (WORD) (-nWordsPerCol * N_Factor);
					SlantRegs._AddrIncFactor._Incr		= (WORD) 0x0000;
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) 0x0000;
					break;
			}
			break;
		case 1:	// L->R Normal
		case 2:	// R->L Inverted
			switch (nHeadType)
			{
				case FoxjetDatabase::ALPHA_CODER:
				case FoxjetDatabase::UJ2_192_32:
				case FoxjetDatabase::UJ2_192_32NP:
				case FoxjetDatabase::UJ2_96_32:
				case FoxjetDatabase::UJI_96_32:
				case FoxjetDatabase::UJI_192_32:
				case FoxjetDatabase::UJI_256_32:
					//flush = (31 * N_Factor * nWordsPerCol); 
					SlantRegs._ImageStartAddr._Addr1	= (WORD) ( nWordsPerCol * ( wImageCols + nFlush - 1) );
					SlantRegs._ImageStartAddr._Addr2	= (WORD) ( SlantRegs._ImageStartAddr._Addr1 - ( N_Factor * nWordsPerCol) );
					SlantRegs._AddrIncFactor._Incr		= (WORD) ( -2 * nWordsPerCol * N_Factor );
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) /* max */ (((32 * nWordsPerCol * N_Factor ) - 2 * nWordsPerCol));
					break;
				case FoxjetDatabase::UJ2_352_32:	
					//flush = N_Factor * nWordsPerCol; 
					SlantRegs._ImageStartAddr._Addr1	= (WORD) (  nWordsPerCol * (wImageCols + N_Factor - 1) );
					SlantRegs._ImageStartAddr._Addr2	= (WORD) ( SlantRegs._ImageStartAddr._Addr1 - ( N_Factor * nWordsPerCol) );
					SlantRegs._AddrIncFactor._Incr		= (WORD) 0x0000;
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) -2 * nWordsPerCol;
					break;
				case FoxjetDatabase::GRAPHICS_384_128:
				case FoxjetDatabase::GRAPHICS_768_256:	
				case FoxjetDatabase::GRAPHICS_768_256_L310:
					//flush = N_Factor * nWordsPerCol; 
					SlantRegs._ImageStartAddr._Addr2	= (WORD) (  nWordsPerCol * (wImageCols + N_Factor - 1) );
					SlantRegs._ImageStartAddr._Addr1	= (WORD) ( SlantRegs._ImageStartAddr._Addr2 - ( N_Factor * nWordsPerCol) );
					SlantRegs._AddrIncFactor._Incr		= (WORD) 0x0000;
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) -2 * nWordsPerCol;
					break;
			}
			break;
	}

	DebugMsg (DLLTEXT ("[0x%X] nWidthBytes: %d, nFlush: %d, ippc: %d, ibpc: %d, ilen: %d, N_Factor: %d, nWordsPerCol: %d, wImageCols: %d"), 
		lAddr,  nWidthBytes, nFlush, ippc, ibpc, ilen, N_Factor, nWordsPerCol, wImageCols);

	{
		ULONG nBytes;
		PHCB inCtrlBlock;

		inCtrlBlock.lAddress	= lAddr;
		inCtrlBlock.RegID		= 0x24;
		inCtrlBlock.TxBytes		= sizeof (tagSlantRegs);
		memcpy ( &inCtrlBlock.Data, &SlantRegs, sizeof (tagSlantRegs));
		
		if (DeviceIoControl (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof (PHCB), NULL, 0, &nBytes, NULL)) {
			DebugMsg (DLLTEXT ("[0x%X] SlantRegs._ImageStartAddr._Addr2:   %d"), lAddr, SlantRegs._ImageStartAddr._Addr2);
			DebugMsg (DLLTEXT ("[0x%X] SlantRegs._ImageStartAddr._Addr1:   %d"), lAddr, SlantRegs._ImageStartAddr._Addr1);
			DebugMsg (DLLTEXT ("[0x%X] SlantRegs._AddrIncFactor._Incr:     %d"), lAddr, SlantRegs._AddrIncFactor._Incr);
			DebugMsg (DLLTEXT ("[0x%X] SlantRegs._ColAdjustFactor._Adjust: %d"), lAddr, SlantRegs._ColAdjustFactor._Adjust);
		}
		else
			DebugMsg (DLLTEXT ("ProgramSlantRegs failed: 0x%X"), lAddr);
	}
}

UINT CalcFlushBuffer (LPCWSTR lpszSection)
{
	OEMDBG(DBG_VERBOSE, L"CalcFlushBuffer");

	UINT nFlush = 0;

	UINT nChannels			= GetProfileDWORD (lpszSection, _T ("Channels"));
	UINT nHorzRes			= GetProfileDWORD (lpszSection, _T ("HorzRes"));
	ULONG lPhotocellDelay	= GetProfileDWORD (lpszSection, _T ("PhotocellDelay"));
	BOOL bEnabled			= GetProfileDWORD (lpszSection, _T ("Enabled"));
	int nEncoder			= GetProfileDWORD (lpszSection, _T ("Encoder"));
	int nDirection			= GetProfileDWORD (lpszSection, _T ("Direction"));
	double dHeadAngle		= (double)GetProfileDWORD (lpszSection, _T ("Angle")) / 100.0;
	bool bInverted			= GetProfileDWORD (lpszSection, _T ("Inverted")) ? true : false;
	int nPhotocell			= GetProfileDWORD (lpszSection, _T ("Photocell"));
	int nSharePhoto			= GetProfileDWORD (lpszSection, _T ("SharePhotocell"));
	int nShareEnc			= GetProfileDWORD (lpszSection, _T ("ShareEncoder"));
	int nHeadType			= GetProfileDWORD (lpszSection, _T ("HeadType"));
	bool bMasterHead		= GetProfileDWORD (lpszSection, _T ("MasterHead")) ? true : false;
	int nIntTachSpeed		= GetProfileDWORD (lpszSection, _T ("IntTachSpeed"));
	double dNozzleSpan		= (double)GetProfileDWORD (lpszSection, _T ("NozzleSpan")) / 100.0;
	double dPhotocellRate	= (double)GetProfileDWORD (lpszSection, _T ("PhotocellRate")) / 100.0;
	int nEncoderDivisor		= GetProfileDWORD (lpszSection, _T ("EncoderDivisor"));
	bool bDoublePulse		= GetProfileDWORD (lpszSection, _T ("DoublePulse")) ? true : false;
	double dRes				= GetRes (nHorzRes, nEncoderDivisor);

	UNREFERENCED_PARAMETER(bEnabled);
	UNREFERENCED_PARAMETER(bDoublePulse);
	UNREFERENCED_PARAMETER(nSharePhoto);
	UNREFERENCED_PARAMETER(nPhotocell);
	UNREFERENCED_PARAMETER(nShareEnc);
	UNREFERENCED_PARAMETER(lPhotocellDelay);
	UNREFERENCED_PARAMETER(dPhotocellRate);
	UNREFERENCED_PARAMETER(bMasterHead);
	UNREFERENCED_PARAMETER(dNozzleSpan);
	UNREFERENCED_PARAMETER(nEncoder);
	UNREFERENCED_PARAMETER(nIntTachSpeed);

	int N_Factor = CalculateNFactor (nHeadType, nHorzRes, dHeadAngle, dRes);
	INT nWordsPerCol = GetPhcChannels (nHeadType, nChannels) / 16;
	UCHAR state = (nDirection == FoxjetDatabase::LTOR) ? 1 : 0;
	state |= bInverted ? 0x02 : 0x00;

	DebugMsg (DLLTEXT ("GetRes (%d, %d): %f"), nHorzRes, nEncoderDivisor, dRes);
	DebugMsg (DLLTEXT ("CalculateNFactor (%d, %d, %f, %d): %d"), nHeadType, nHorzRes, dHeadAngle, dRes, N_Factor);
	DebugMsg (DLLTEXT ("nHeadType: %d, nHorzRes: %d, dHeadAngle: %f, N_Factor: %d, nWordsPerCol: %d"), 
		nHeadType, 
		nHorzRes, 
		dHeadAngle,
		N_Factor,
		nWordsPerCol);

	switch ( state ) 
	{
		case 0:	// R->L Normal
		case 3:	// L->R Inverted
			switch (nHeadType)
			{
				case FoxjetDatabase::ALPHA_CODER:
				case FoxjetDatabase::UJ2_192_32:
				case FoxjetDatabase::UJ2_192_32NP:
				case FoxjetDatabase::UJ2_96_32:
				case FoxjetDatabase::UJI_96_32:
				case FoxjetDatabase::UJI_192_32:
				case FoxjetDatabase::UJI_256_32:
					//nFlush = (31 * N_Factor * nWordsPerCol); 
					nFlush = (31 * N_Factor * nWordsPerCol) / 2; //m_Config.m_HeadSettings.m_nEncoderDivisor; 
					break;
				case FoxjetDatabase::UJ2_352_32:
					nFlush = N_Factor * nWordsPerCol; 
					break;
				case FoxjetDatabase::GRAPHICS_384_128:
				case FoxjetDatabase::GRAPHICS_768_256:
				case FoxjetDatabase::GRAPHICS_768_256_L310:
					nFlush = N_Factor * nWordsPerCol; 
					break;
			}
			break;
		case 1:	// L->R Normal
		case 2:	// R->L Inverted
			switch (nHeadType)
			{
				case FoxjetDatabase::ALPHA_CODER:
				case FoxjetDatabase::UJ2_192_32:
				case FoxjetDatabase::UJ2_192_32NP:
				case FoxjetDatabase::UJ2_96_32:
				case FoxjetDatabase::UJI_96_32:
				case FoxjetDatabase::UJI_192_32:
				case FoxjetDatabase::UJI_256_32:
					//nFlush = (31 * N_Factor * nWordsPerCol); 
					nFlush = (31 * N_Factor * nWordsPerCol) / 2; //m_Config.m_HeadSettings.m_nEncoderDivisor; 
					break;
				case FoxjetDatabase::UJ2_352_32:	
					nFlush = N_Factor * nWordsPerCol; 
					break;
				case FoxjetDatabase::GRAPHICS_384_128:
				case FoxjetDatabase::GRAPHICS_768_256:	
				case FoxjetDatabase::GRAPHICS_768_256_L310:
					nFlush = N_Factor * nWordsPerCol; 
					break;
			}
			break;
	}

	DebugMsg (DLLTEXT ("nFlush: %d"), nFlush);

	return nFlush;
}

int GetPhcChannels (int nHeadType, UINT nChannels)
{
	if (nHeadType == FoxjetDatabase::GRAPHICS_384_128)
		return 256;

	return nChannels;
}

bool SetImageLen (HANDLE hDevice, ULONG lAddr, WORD wImageCols)
{
	OEMDBG(DBG_VERBOSE, L"SetImageLen");
	
	DWORD dwBytes;
	PHCB inCtrlBlock;

	inCtrlBlock.lAddress	= lAddr;
	inCtrlBlock.RegID		= 0x22;
	inCtrlBlock.TxBytes		= 2;
	FillMemory (&inCtrlBlock.Data, sizeof (inCtrlBlock.Data), 0);

	inCtrlBlock.Data [0] = LOBYTE(wImageCols);
	inCtrlBlock.Data [1] = HIBYTE(wImageCols);
	
	if (DeviceIoControl (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof (PHCB), NULL, 0, &dwBytes, NULL))
		return true;

	DebugMsg (DLLTEXT ("SetImageLen failed"));
	return false;
}

void CalcProductLen (ULONG & lMaxPhotocellDelay, ULONG & lMinPhotocellDelay, UINT & nMaxFlush)
{
	OEMDBG(DBG_VERBOSE, L"CalcProductLen");

	lMaxPhotocellDelay = 500;//Foxjet3d::CHeadDlg::m_lmtMphcPhotocellDelay.m_dwMin;
	lMinPhotocellDelay = 30000;//Foxjet3d::CHeadDlg::m_lmtMphcPhotocellDelay.m_dwMax;
	nMaxFlush = 0;

	// TODO: should be based on master head(s) lineID

	for (int nHead = 0; nHead < ARRAYSIZE (lpszSection); nHead++) {
		BOOL bEnabled			= GetProfileDWORD (lpszSection [nHead], _T ("Enabled"));
		ULONG lPhotocellDelay	= GetProfileDWORD (lpszSection [nHead], _T ("PhotocellDelay"));

		if (bEnabled) {
			lMinPhotocellDelay = min (lPhotocellDelay, lMinPhotocellDelay);
			lMaxPhotocellDelay = max (lPhotocellDelay, lMaxPhotocellDelay);
			nMaxFlush = max (CalcFlushBuffer (lpszSection [nHead]), nMaxFlush); 
		}
	}
}


bool EnablePrint (HANDLE hDevice, ULONG lAddr, bool bEnabled)
{
	OEMDBG(DBG_VERBOSE, L"EnablePrint");

	bool bResult = false;
	DWORD dwBytes;
	PHCB inCtrlBlock;
	tagCtrlReg tCtrl;
	tagRegisters tCurrent;

	inCtrlBlock.lAddress = lAddr;
	
	if (ReadConfiguration (hDevice, lAddr, tCurrent)) {
		tCtrl = tCurrent._CtrlReg;
		
		tCtrl._PrintBuffer	= 0;
		tCtrl._FireAMS		= 0x00;
		tCtrl._PrintEnable	= bEnabled ? 1 : 0;
		inCtrlBlock.RegID	= 0x00;
		inCtrlBlock.TxBytes = sizeof ( tCtrl );

		CopyMemory (&inCtrlBlock.Data, &tCtrl, sizeof (tCtrl));
		
		if (DeviceIoControl (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof (PHCB), NULL, 0, &dwBytes, NULL))
			bResult = true;
		else
			DebugMsg (DLLTEXT ("EnablePrint (%s) failed"), bEnabled ? _T ("true") : _T ("false"));
	}

	return bResult;
}

bool RegisterPhotoEvent (HANDLE hDevice, ULONG lAddr, HANDLE hEvent, bool bRegister)
{
	OEMDBG(DBG_VERBOSE, L"RegisterPhotoEvent");
	bool bResult = false;
	ULONG dwBytes = 0;

	if (bRegister) {
		ULONG lData [2];
		
		lData [0] = lAddr;
		lData [1] = (ULONG)hEvent;
		
		if (DeviceIoControl (hDevice, IOCTL_REGISTER_DRIVER_PHOTO_EVENT, &lData, sizeof (lData), NULL, 0, &dwBytes, NULL))
			bResult = true;
		else
			DebugMsg (DLLTEXT ("IOCTL_REGISTER_DRIVER_PHOTO_EVENT failed"));
	}
	else {
		if (DeviceIoControl (hDevice, IOCTL_UNREGISTER_DRIVER_PHOTO_EVENT, &lAddr, sizeof (lAddr), NULL, 0, &dwBytes, NULL))
			bResult = true;
		else
			DebugMsg (DLLTEXT ("IOCTL_UNREGISTER_DRIVER_PHOTO_EVENT failed"));
	}

	return bResult;
}

bool RegisterEOPEvent (HANDLE hDevice, ULONG lAddr, HANDLE hEvent, bool bRegister)
{
	OEMDBG(DBG_VERBOSE, L"RegisterEOPEvent");
	bool bResult = false;
	ULONG dwBytes = 0;

	if (bRegister) {
		ULONG lData [2];
		
		lData [0] = lAddr;
		lData [1] = (ULONG)hEvent;
		
		if (DeviceIoControl (hDevice, IOCTL_REGISTER_DRIVER_EOP_EVENT, &lData, sizeof (lData), NULL, 0, &dwBytes, NULL))
			bResult = true;
		else
			DebugMsg (DLLTEXT ("IOCTL_REGISTER_DRIVER_EOP_EVENT failed"));
	}
	else {
		if (DeviceIoControl (hDevice, IOCTL_UNREGISTER_DRIVER_EOP_EVENT, &lAddr, sizeof (lAddr), NULL, 0, &dwBytes, NULL))
			bResult = true;
		else
			DebugMsg (DLLTEXT ("IOCTL_UNREGISTER_DRIVER_EOP_EVENT failed"));
	}

	return bResult;
}

bool UpdateImageData (HANDLE hDevice, ULONG lAddr, LPVOID pImageBuffer, unsigned char nBPC, WORD wStartCol, WORD wNumCols)
{
	OEMDBG(DBG_VERBOSE, L"UpdateImageData");

	PHCB inCtrlBlock;
	DWORD dwBytes = 0;
	tagImgUpdateHdr ImageHdr;
	bool bResult = false;

	inCtrlBlock.lAddress	= lAddr;
	inCtrlBlock.RegID		= 0xFF;
	inCtrlBlock.TxBytes		= 0;
	FillMemory (&inCtrlBlock.Data, sizeof (inCtrlBlock.Data), 0);

	ImageHdr._wStartCol		= wStartCol;
	ImageHdr._wNumCols		= wNumCols;
	ImageHdr._nBPC			= nBPC;
	CopyMemory (&inCtrlBlock.Data, &ImageHdr, sizeof (ImageHdr));

	ULONG lImgLen = ImageHdr._wNumCols * ImageHdr._nBPC;

	HGLOBAL hBuffer = GlobalAlloc (GPTR, sizeof (PHCB) + lImgLen);
	
	DebugMsg (DLLTEXT ("UpdateImageData (0x%X, 0x%p, 0x%p, %d, %d, %d)"), 
		hDevice, 
		lAddr, 
		pImageBuffer, 
		nBPC, 
		wStartCol, 
		wNumCols);

	if (PUCHAR pData = (PUCHAR)GlobalLock (hBuffer)) {
		PUCHAR p = pData;

		CopyMemory (p, &inCtrlBlock, sizeof (PHCB));
		p += sizeof (PHCB);
		CopyMemory (p, pImageBuffer, lImgLen);

		if (DeviceIoControl (hDevice, IOCTL_IMAGE_UPDATE_DIRECTIO, NULL, 0, pData, sizeof (PHCB) + lImgLen, &dwBytes, NULL)) 
			bResult = true;
		else
			DebugMsg (DLLTEXT ("IOCTL_IMAGE_UPDATE_DIRECTIO failed 0x%X"), lAddr);

		GlobalUnlock (hBuffer);
	}
	else
		DebugMsg (DLLTEXT ("GlobalLock (0x%p) failed (%d bytes): 0x%X"), hBuffer, sizeof (PHCB) + lImgLen, lAddr);

	GlobalFree (hBuffer);

	return bResult;
}

void FreeMphc ()
{
	OEMDBG(DBG_VERBOSE, L"FreeMphc");

	for (int i = 0; i < ARRAYSIZE (::cards); i++) {
		if (::cards [i].m_hDevice) {
			DebugMsg (DLLTEXT ("close: 0x%X [0x%p]"), ::cards [i].m_lAddress, ::cards [i].m_hDevice);
			CloseHandle (::cards [i].m_hDevice);
			::cards [i].m_hDevice = NULL;
		}
	}
}

int InitMphc ()
{
	OEMDBG(DBG_VERBOSE, L"InitMphc");

	int nResult = 0;
	HDEVINFO info = SetupDiGetClassDevs (&guid, NULL, NULL, DIGCF_PRESENT | DIGCF_INTERFACEDEVICE);
	
	if (info == INVALID_HANDLE_VALUE) {
		DebugMsg (DLLTEXT ("SetupDiGetClassDevs == INVALID_HANDLE_VALUE"));
		return 0;
	}

	// Enumerate all devices of our class. For each one, create a
	// CDeviceEntryList object. Then determine the friendly name of the
	// device by reading the registry.

	SP_DEVICE_INTERFACE_DATA ifdata;
	ifdata.cbSize = sizeof(ifdata);
	DWORD devindex;

	DebugMsg (DLLTEXT ("SetupDiEnumDeviceInterfaces..."));

	for (devindex = 0; SetupDiEnumDeviceInterfaces (info, NULL, &guid, devindex, &ifdata); ++devindex) {
		DWORD needed;

		// Determine the symbolic link name for this device instance. Since
		// this is variable in length, make an initial call to determine
		// the required length.
		SetupDiGetDeviceInterfaceDetail(info, &ifdata, NULL, 0, &needed, NULL);
		DebugMsg (DLLTEXT ("needed: %d"), needed);
		HGLOBAL hNeeded = GlobalAlloc (GPTR, needed);

		PSP_INTERFACE_DEVICE_DETAIL_DATA detail = (PSP_INTERFACE_DEVICE_DETAIL_DATA)GlobalLock (hNeeded);
		SP_DEVINFO_DATA did = {sizeof(SP_DEVINFO_DATA)};

		detail->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);

		if (!SetupDiGetDeviceInterfaceDetail(info, &ifdata, detail, needed, NULL, &did)) {
			DebugMsg (DLLTEXT ("SetupDiGetDeviceInterfaceDetail failed"));
			GlobalUnlock (hNeeded);
			GlobalFree (hNeeded);
			continue;
		}

		// Determine the device's friendly name
		TCHAR fname[256] = { 0 };

		if (!SetupDiGetDeviceRegistryProperty(info, &did, SPDRP_FRIENDLYNAME, NULL, (PBYTE) fname, sizeof(fname), NULL)
			&& !SetupDiGetDeviceRegistryProperty(info, &did, SPDRP_DEVICEDESC, NULL, (PBYTE) fname, sizeof(fname), NULL))
		{
			_tcsncpy (fname, detail->DevicePath, 256);
			DebugMsg (DLLTEXT ("fname: %s"), fname);
		}

		DebugMsg (DLLTEXT ("detail->DevicePath: %s"), detail->DevicePath);

		HANDLE hDevice = CreateFile (detail->DevicePath, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

		if (hDevice) {
			_VERSION ver;
			ULONG lBytes = 0;

			FillMemory (&ver, sizeof (ver), 0x00);
			DeviceIoControl (hDevice, IOCTL_GET_VERSION, NULL, 0, &ver, sizeof (ver), &lBytes, NULL);
			DebugMsg (DLLTEXT ("IOCTL_GET_VERSION: %d.%03d.%03d"), ver.Major, ver.Minor, ver.Revision);

			{
				ULONG lAddressData [8];
				ULONG lBytes = 0;
				
				FillMemory (&lAddressData, ARRAYSIZE (lAddressData), 0x00); 
				DeviceIoControl (hDevice, IOCTL_ENUM_IO_ADDRESSES, NULL, 0, &lAddressData, sizeof (lAddressData), &lBytes, NULL);
				
				for (unsigned long j = 0; j < lBytes/sizeof(ULONG); j++) {
					DebugMsg (DLLTEXT ("IOCTL_ENUM_IO_ADDRESSES: %d, 0x%p"), lAddressData [j], hDevice);

					for (int nCard = 0; nCard < ARRAYSIZE (::cards); nCard++) {
						if (lAddressData [j] == ::cards [nCard].m_lAddress) {

							// is head marked as disabled?
							for (int nHead = 0; nHead < ARRAYSIZE (lpszSection); nHead++) {
								WCHAR szAddr [16] = { 0 };
								BOOL bEnabled = GetProfileDWORD (lpszSection [nHead], _T ("Enabled"));

								GetProfileString (lpszSection [nHead], _T ("UID"), szAddr, ARRAYSIZE (szAddr));
								ULONG lAddr = wcstoul (szAddr, NULL, 16);

								if (lAddr == ::cards [nCard].m_lAddress && !bEnabled)
									break;
							}

							wcsncpy (::cards [nCard].m_szLinkName, detail->DevicePath, ARRAYSIZE (::cards [nCard].m_szLinkName));
							wcsncpy (::cards [nCard].m_szFriendlyName, fname, ARRAYSIZE (::cards [nCard].m_szFriendlyName));
							::cards [nCard].m_hDevice = hDevice;

							DebugMsg (DLLTEXT ("card [%d]: 0x%p: %s [%s]"), 
								nCard, 
								::cards [nCard].m_hDevice, 
								::cards [nCard].m_szFriendlyName, 
								::cards [nCard].m_szLinkName);

							break;
						}
					}
				}
			}
		}

		nResult++;
		GlobalUnlock (hNeeded);
		GlobalFree (hNeeded);

	}	
   
	SetupDiDestroyDeviceInfoList(info);

	return nResult;
}

/*
static void TraceImage (BYTE * pBuffer, int cx, int cy)
{
	WCHAR szDebug [256];
	WCHAR * pszDebug;
	DWORD dwWidth = abs (((cx + 31) / 32) * 4);

	for (int y = 0; y < cy; y++) {
		const BYTE * pRow = pBuffer + (y * dwWidth);

		FillMemory (szDebug, ARRAYSIZE (szDebug), 0x00);
		pszDebug = szDebug;

		for (UINT x = 0; x < dwWidth; x++, pRow++) {
			for (int i = 0; i < 8; i++) {
				BOOL bBit = !((* pRow) & (1 << (7 - i)));
				
				if (pszDebug > (&szDebug [0] + ARRAYSIZE (szDebug) - 3))
					break;

				* pszDebug++ = bBit ? 'X' : '.';
			}
		}

		* pszDebug = '\0';

		::OutputDebugString (szDebug);

		if (y > 64) 
			break;
	}
}
*/

DWORD GetProfileDWORD (LPCWSTR lpszSection, LPCWSTR lpszKey, DWORD dwDefault)
{
	HKEY hKey;
	DWORD dwType = REG_DWORD, dwSize = sizeof (DWORD), dwData = dwDefault;

	if (::RegCreateKey (HKEY_LOCAL_MACHINE, lpszSection, &hKey) == ERROR_SUCCESS) {
		::RegQueryValueEx (hKey, lpszKey, NULL, &dwType, (LPBYTE)&dwData, &dwSize);
		::RegCloseKey (hKey);
	}

	return dwData;
}

bool GetProfileString (LPCWSTR lpszSection, LPCWSTR lpszKey, LPWSTR lpsz, ULONG lLen)
{
	HKEY hKey;
	DWORD dwType = REG_SZ, dwSize = lLen;
	bool bResult = false;

	if (::RegCreateKey (HKEY_LOCAL_MACHINE, lpszSection, &hKey) == ERROR_SUCCESS) {
		bResult = ::RegQueryValueEx (hKey, lpszKey, NULL, &dwType, (LPBYTE)lpsz, &dwSize) == ERROR_SUCCESS ? true : false;
		::RegCloseKey (hKey);
	}

	return bResult;
}

void RotateBlt (LPBYTE lpDest, int nXDest, int nYDest, int nWidthDest, int nHeightDest,
				LPBYTE lpSrc,  int nXSrc,  int nYSrc,  int nWidthSrc,  int nHeightSrc)
{
	int nWidthBytesDest	= abs (((nWidthDest + 31) / 32) * 4);
	int nWidthBytesSrc	= abs (((nWidthSrc	+ 31) / 32) * 4);
 
	OEMDBG(DBG_VERBOSE, L"RotateBlt entry.");
	DebugMsg (DLLTEXT ("RotateBlt: %d, %d [%d, %d] --> %d, %d [%d, %d]"), 
		nXDest, nYDest, nWidthDest, nHeightDest,
		nXSrc,  nYSrc,  nWidthSrc,  nHeightSrc);

	for (int nRow = 0; nRow < nWidthDest; nRow++) {
		const BYTE * pRowSrc = lpSrc + ((nRow + nYSrc) * nWidthBytesSrc);

		for (int nByte = 0; nByte < nWidthBytesSrc; nByte++, pRowSrc++) {
			for (int nBit = 0; nBit < 8; nBit++) {
				BOOL bBit = !((* pRowSrc) & (1 << (7 - nBit)));

				if (bBit) {
					int i		= (nByte * 8) + nBit;
					int j		= nRow;
					int x		= j;
					int y		= i;
					div_t d		= div (x, 8);
					BYTE * pByteDest = lpDest + (y * nWidthBytesDest) + d.quot;

					* pByteDest |= (1 << (7 - d.rem));
				}
			}
		}
	}
}

void RotateBlt (LPBYTE lpDest, int nXDest, int nYDest, int nWidthDest, int nHeightDest, int nXDpiDest, int nYDpiDest, 
				LPBYTE lpSrc,  int nXSrc,  int nYSrc,  int nWidthSrc,  int nHeightSrc, int nXDpiSrc, int nYDpiSrc,
				int nHeadType)

{
	OEMDBG(DBG_VERBOSE, L"RotateBlt entry.");

	const bool b384 = nHeadType == FoxjetDatabase::GRAPHICS_384_128 ? true : false;
	const int nWidthBytesDest	= abs (((nWidthDest + 31) / 32) * 4);
	const int nWidthBytesSrc	= abs (((nWidthSrc	+ 31) / 32) * 4);
	const double dStep = (double)nYDpiSrc / (double)nYDpiDest;
	const int nYStep = (int)BindTo ((double)nXDpiDest / (double)nXDpiSrc, 1.0, 10.0);
	const double dHeight = ((double)(!b384 ? nWidthDest : nWidthDest / 2) / (double)nYDpiDest) * (double)nYDpiSrc;
 	int nRowCount = 0;

	///*
	DebugMsg (DLLTEXT ("RotateBlt: %d, %d [%d, %d] (%d x %d) --> %d, %d [%d, %d] (%d x %d)"), 
		nXDest, nYDest, nWidthDest, nHeightDest,	nXDpiDest, nYDpiDest,
		nXSrc,  nYSrc,  nWidthSrc,  nHeightSrc,		nXDpiSrc, nYDpiSrc);
	DebugMsg (DLLTEXT ("RotateBlt: dStep: %f, nYStep: %d, dHeight: %f"), 
		dStep, nYStep, dHeight);
	//*/

	for (double dRow = 0.0; dRow < dHeight; dRow += dStep, nRowCount++) {
		int nRow = Round (dRow);
		const BYTE * pRowSrc = lpSrc + ((nRow + nYSrc) * nWidthBytesSrc);

		//DebugMsg (DLLTEXT ("row: [%d] %d [%f]"), nRowCount, nRow, dRow);

		for (int nByte = 0; nByte < nWidthBytesSrc; nByte++, pRowSrc++) {
			for (int nBit = 0; nBit < 8; nBit++) {
				BOOL bBit = !((* pRowSrc) & (1 << (7 - nBit)));

				for (int nRepeat = 0; nRepeat < nYStep; nRepeat++) {
					if (bBit) {
						int i		= (nByte * 8) + nBit;
						int j		= nRowCount; 
						int x		= j;
						int y		= (i * nYStep) + nRepeat; 
						div_t d		= div (x, 8);

						if (y < nHeightDest) {
							BYTE * pByteDest = lpDest + (y * nWidthBytesDest) + d.quot;

							if (b384)
								pByteDest += ((nWidthDest / 8) / 2);

							* pByteDest |= (1 << (7 - d.rem));
						}
						else 
							DebugMsg (DLLTEXT ("y [%d] < nHeightDest [%d]"), y, nHeightDest);
					}
				}
			}
		}
	}
}

BOOL APIENTRY OEMStartDoc (SURFOBJ * pso, PWSTR pwszDocName, DWORD dwJobId)
{
    OEMDBG(DBG_VERBOSE, L"OEMStartDoc entry.");
    
	PDEVOBJ pDevObj = (PDEVOBJ)pso->dhpdev;
    POEMPDEV pOemPDEV = (POEMPDEV)pDevObj->pdevOEM;

	DebugMsg (DLLTEXT ("OEMStartDoc")); 

	/*
	if (pwszDocName) 
		StringCbPrintf (pOemPDEV->m_szDocName, ARRAYSIZE (pOemPDEV->m_szDocName) - 1, _T ("%s"), pwszDocName);
	*/

	return TRUE;
}

BOOL APIENTRY OEMStartPage (SURFOBJ * pso)
{
    PDEVOBJ pDevObj = (PDEVOBJ)pso->dhpdev;
    POEMPDEV pOemPDEV = (POEMPDEV)pDevObj->pdevOEM;

    OEMDBG(DBG_VERBOSE, L"OEMStartPage entry.");
	DebugMsg (DLLTEXT ("OEMStartPage")); 

	/* TODO: rem
	{
		DEVMODEW devmode;
		ATTRIBUTE_INFO_3 info;

		memset (&devmode, 0, sizeof (devmode));
		memset (&info, 0, sizeof (info));

		BOOL bGetJobAttributes = GetJobAttributes (_T ("Foxjet Marksman PRO"), &devmode, &info);
	
		DebugMsg (DLLTEXT ("bGetJobAttributes: %d: %d (%d, %d)"), bGetJobAttributes, devmode.dmCopies, info.dwJobNumberOfCopies, info.dwDrvNumberOfCopies); 
	}
	*/

    return (pOemPDEV->m_pfnDrvStartPage) (pso);
}


BOOL APIENTRY
OEMEndDoc(
    SURFOBJ     *pso,
    FLONG       fl
    )

/*++

Routine Description:

    Implementation of DDI hook for DrvEndDoc.

    DrvEndDoc is called by GDI when it has finished 
    sending a document to the driver for rendering.
    
    Please refer to DDK documentation for more details.

    This particular implementation of OEMEndDoc performs
    the following operations:
    - Dump the bitmap file header
    - Dump the bitmap info header
    - Dump the color table if one exists
    - Dump the buffered bitmap data
    - Free the memory for the data buffers

Arguments:

    pso - Defines the surface object
    flags - A set of flag bits

Return Value:

    TRUE if successful, FALSE if there is an error

--*/

{
    OEMDBG(DBG_VERBOSE, L"OEMEndDoc entry.");

    PDEVOBJ pDevObj = (PDEVOBJ)pso->dhpdev;
    POEMPDEV pOemPDEV = (POEMPDEV)pDevObj->pdevOEM;
    DWORD dwWritten;
    INT cScans;
	BOOL bDebug = GetProfileDWORD (lpszRootSection, _T ("Debug"), 0) > 0;
	BOOL bDemo	= GetProfileDWORD (lpszRootSection, _T ("Demo"), 0) > 0;
	int ySrc = 0;

	DebugMsg (DLLTEXT ("bDebug: %d"), bDebug); 
	DebugMsg (DLLTEXT ("biBitCount: %d"), pOemPDEV->bmInfoHeader.biBitCount); 

    if (pOemPDEV->pBufStart && pOemPDEV->bmInfoHeader.biBitCount == 1) {
        // Fill BitmapFileHeader
        //
        DWORD dwTotalBytes = pOemPDEV->cbHeaderOffBits + pOemPDEV->bmInfoHeader.biSizeImage;        // File size
    
        pOemPDEV->bmFileHeader.bfType = 0x4d42;     // Signature = 'BM'
        pOemPDEV->bmFileHeader.bfSize = dwTotalBytes;  // Bytes in whole file.
        pOemPDEV->bmFileHeader.bfReserved1 = 0;
        pOemPDEV->bmFileHeader.bfReserved2 = 0;
        pOemPDEV->bmFileHeader.bfOffBits   = pOemPDEV->cbHeaderOffBits; // Offset to bits in file.

        if (pOemPDEV->bColorTable)
            pOemPDEV->bmFileHeader.bfOffBits += pOemPDEV->cPalColors * sizeof(ULONG);

        // Num of scanlines
        //
        cScans = pOemPDEV->bmInfoHeader.biHeight;

        // Flip the biHeight member so that it denotes top-down bitmap 
        //
        pOemPDEV->bmInfoHeader.biHeight = cScans * -1;

		//TraceImage (pOemPDEV->pBufStart, abs (pOemPDEV->bmInfoHeader.biWidth), abs (pOemPDEV->bmInfoHeader.biHeight));

		int nCards = InitMphc ();
		DebugMsg (DLLTEXT ("detected %d cards"), nCards); 

		for (int nHead = 0; nHead < ARRAYSIZE (lpszSection); nHead++) {
			const int nHeadType			= GetProfileDWORD (lpszSection [nHead], _T ("HeadType"));
			const int nChannels			= GetProfileDWORD (lpszSection [nHead], _T ("Channels"));
			const int nChannelsLogical	= GetPhcChannels (nHeadType, nChannels);
			const BOOL bEnabled			= GetProfileDWORD (lpszSection [nHead], _T ("Enabled"));
			const ULONG lPhotocellDelay	= GetProfileDWORD (lpszSection [nHead], _T ("PhotocellDelay"));
			const UINT nHorzRes			= GetProfileDWORD (lpszSection [nHead], _T ("HorzRes"));
			const int nEncoderDivisor	= GetProfileDWORD (lpszSection [nHead], _T ("EncoderDivisor"));
			const double dNozzleSpan	= (double)GetProfileDWORD (lpszSection [nHead], _T ("NozzleSpan")) / 100.0;
			const double dRes			= GetRes (nHorzRes, nEncoderDivisor);
			const LONG lHeight			= pOemPDEV->bmInfoHeader.biHeight;
			const LONG lWidth			= pOemPDEV->bmInfoHeader.biWidth;
			const LONG lWidthHead		= (LONG)(((double)pOemPDEV->bmInfoHeader.biWidth / (double)pOemPDEV->bmInfoHeader.biXPelsPerMeter) * dRes);

			if (nChannels > 0 && bEnabled) {
				DWORD dwWidthDest	= abs ((((pOemPDEV->bmInfoHeader.biBitCount * nChannelsLogical /* nChannels */) + 31) / 32) * 4);
				//DWORD dwWidthSrc	= abs ((((pOemPDEV->bmInfoHeader.biBitCount * pOemPDEV->bmInfoHeader.biWidth) + 31) / 32) * 4);
				DWORD dwSlice		= dwWidthDest * abs (lWidthHead);
				HGLOBAL hSlice		= GlobalAlloc (GPTR, dwSlice);
				LPBYTE lpSlice		= (LPBYTE)GlobalLock (hSlice);

				//DebugMsg (DLLTEXT ("Channels: %s: %d [%d]"), lpszSection [nHead], nChannels, nChannelsLogical); 
				//DebugMsg (DLLTEXT ("buffer size: %d [%d * %d / 8]"), dwSlice, dwWidthDest, abs (lWidth)); 

				/* TODO
				{
					 DEVMODEW curr, last;
					 
					 BOOL bGdiGetDevmodeForPage = GdiGetDevmodeForPage (hSpoolFile, 1, &curr, &last);
				}
				*/

				//RotateBlt (lpSlice, 0, 0, nChannels, lWidth, pOemPDEV->pBufStart, 0, ySrc, lWidth, abs (lHeight) - ySrc);
				RotateBlt (	//				x	y		cx					cy						x-dpi										y-dpi
					//lpSlice,				0,	0,		nChannels,			lWidthHead,				(int)dRes,									(int)floor (nChannels / dNozzleSpan), 
					lpSlice,				0,	0,		nChannelsLogical,	lWidthHead,				(int)dRes,									(int)floor (nChannels / dNozzleSpan), 
					pOemPDEV->pBufStart,	0,	ySrc,	lWidth,				abs (lHeight) - ySrc,	pOemPDEV->bmInfoHeader.biXPelsPerMeter,		pOemPDEV->bmInfoHeader.biYPelsPerMeter,
					nHeadType);
				//TraceImage (lpSlice, nChannels, lWidth);

				if (!bDemo) {
					WCHAR szAddr [16] = { 0 };

					GetProfileString (lpszSection [nHead], _T ("UID"), szAddr, ARRAYSIZE (szAddr));
					DebugMsg (DLLTEXT ("%s"), szAddr); 
					HANDLE hDevice = GetDevice (szAddr);
					DebugMsg (DLLTEXT ("hDevice: 0x%p"), hDevice); 

					if (hDevice) {
						const ULONG lAddr = wcstoul (szAddr, NULL, 16);
						WCHAR szEvent [64] = { 0 };
						UINT nFlush = 0;
						DWORD dwBytes = 0;
						DWORD dwTimeout = GetProfileDWORD (lpszRootSection, _T ("Timeout"), 5 * (60 * 1000)); // INFINITE = 0xFFFFFFFF

//						if (!IsFpgaLoaded (hDevice, lAddr)) {
							DebugMsg (DLLTEXT ("Loading FPGA: 0x%X [0x%p]"), lAddr, hDevice); 

							if (!LoadFpga (hDevice, lAddr)) {
								DebugMsg (DLLTEXT ("FPGA FAILED TO LOAD: 0x%X [0x%p]"), lAddr, hDevice); 
							}
							else {
								bool bLoaded = IsFpgaLoaded (hDevice, lAddr);
								
								DebugMsg (DLLTEXT ("IsFpgaLoaded: %d"), bLoaded); 
							}
//						}
//						else 
//							DebugMsg (DLLTEXT ("FPGA present: 0x%X [0x%p]"), lAddr, hDevice); 

						//Activate (hDevice, lAddr);
						ProgramHeadSettings (lpszSection [nHead], hDevice, lAddr, nFlush);
						
						StringCbPrintf (szEvent, ARRAYSIZE (szEvent) - 1, _T ("Photo 0x%X"), lAddr);
						HANDLE hPhoto = CreateEvent (NULL, true, false, szEvent);

						StringCbPrintf (szEvent, ARRAYSIZE (szEvent) - 1, _T ("EOP 0x%X"), lAddr);
						HANDLE hEOP = CreateEvent (NULL, true, false, szEvent);

						{ // clear buffer
							ULONG l = lAddr;
							DeviceIoControl (hDevice, IOCTL_CLEAR_BUFFER, &l, sizeof(ULONG), NULL, 0, &dwBytes, NULL);
						}

						DebugMsg (DLLTEXT ("hPhoto: 0x%p"), hPhoto); 
						DebugMsg (DLLTEXT ("hEOP: 0x%p"), hEOP); 

						RegisterPhotoEvent (hDevice, lAddr, hPhoto, true);
						RegisterEOPEvent (hDevice, lAddr, hEOP, true);
						EnablePrint (hDevice, lAddr, true);

						SetImageLen (hDevice, lAddr, (WORD)(lWidthHead + nFlush));

						{ // pDriver->SetProductLen (hDevice, wProdLen);
							ULONG nBytes;
							PHCB inCtrlBlock;
							ULONG lMinPhotocellDelay = 0, lMaxPhotocellDelay = 0;
							double dDelay = max (0.0, ((double)lMaxPhotocellDelay - (double)lPhotocellDelay)) / 1000.0;
							WORD wCols = (WORD)(dDelay * dRes);
							WORD wProdLen = wCols + 1;
							UINT nMaxFlush = 0;

							CalcProductLen (lMaxPhotocellDelay, lMinPhotocellDelay, nMaxFlush);
							wProdLen += (WORD)nMaxFlush - (WORD)nFlush;

							DebugMsg (DLLTEXT ("lPhotocellDelay: %d, lMaxPhotocellDelay: %d, dDelay: %d [%f], wCols: %d, wProdLen: %d, nMaxFlush: %d"), 
								lPhotocellDelay,
								lMaxPhotocellDelay,
								(ULONG)(dDelay * 1000.0), dDelay,
								wCols,
								wProdLen,
								nMaxFlush); 

							inCtrlBlock.lAddress	= lAddr;
							inCtrlBlock.RegID		= 0x2C;
							inCtrlBlock.TxBytes		= sizeof (WORD);
							inCtrlBlock.Data [0]	= LOBYTE (wProdLen);
							inCtrlBlock.Data [1]	= HIBYTE (wProdLen);

							if (!DeviceIoControl (hDevice, IOCTL_WRITE_CTRL_REG, &inCtrlBlock, sizeof (PHCB), NULL, 0, &nBytes, NULL)) 
								DebugMsg (DLLTEXT ("SetProductLen FAILED: %d"), wProdLen); 
							else
								DebugMsg (DLLTEXT ("SetProductLen: %d"), wProdLen); 
						}

						ProgramSlantRegs (lpszSection [nHead], hDevice, lAddr, abs (pOemPDEV->bmInfoHeader.biWidth), abs (pOemPDEV->bmInfoHeader.biHeight));

						div_t bpc = div (nChannelsLogical /* nChannels */, 8);

						UpdateImageData (hDevice, lAddr, lpSlice, (UCHAR)bpc.quot, 0, (WORD)lWidthHead);

						/*
						{ // TODO: rem ?
							DWORD dwWait = ::WaitForSingleObject (hPhoto, dwTimeout);
							
							if (dwWait == WAIT_OBJECT_0)	DebugMsg (DLLTEXT ("hPhoto: WAIT_OBJECT_0:  0x%X [%d]"), lAddr, dwTimeout);
							if (dwWait == WAIT_TIMEOUT)		DebugMsg (DLLTEXT ("hPhoto: WAIT_TIMEOUT:   0x%X [%d]"), lAddr, dwTimeout);
							if (dwWait == WAIT_ABANDONED)	DebugMsg (DLLTEXT ("hPhoto: WAIT_ABANDONED: 0x%X [%d]"), lAddr, dwTimeout);
							if (dwWait == WAIT_FAILED)		DebugMsg (DLLTEXT ("hPhoto: WAIT_FAILED:    0x%X [%d]"), lAddr, dwTimeout);
						}
						*/
						
						{
							DWORD dwWait = ::WaitForSingleObject (hEOP, dwTimeout);
							
							if (dwWait == WAIT_OBJECT_0)	DebugMsg (DLLTEXT ("hEOP: WAIT_OBJECT_0:  0x%X [%d]"), lAddr, dwTimeout);
							if (dwWait == WAIT_TIMEOUT)		DebugMsg (DLLTEXT ("hEOP: WAIT_TIMEOUT:   0x%X [%d]"), lAddr, dwTimeout);
							if (dwWait == WAIT_ABANDONED)	DebugMsg (DLLTEXT ("hEOP: WAIT_ABANDONED: 0x%X [%d]"), lAddr, dwTimeout);
							if (dwWait == WAIT_FAILED)		DebugMsg (DLLTEXT ("hEOP: WAIT_FAILED:    0x%X [%d]"), lAddr, dwTimeout);
						}
						
						EnablePrint (hDevice, lAddr, false);
						RegisterEOPEvent (hDevice, lAddr, hEOP, false);
						RegisterPhotoEvent (hDevice, lAddr, hPhoto, false);
						
						CloseHandle (hEOP);
					}
				}


				if (bDebug) {
					WCHAR szFile [_MAX_PATH], szName [_MAX_PATH];
					DWORD dwType = REG_SZ, dwSize = ARRAYSIZE (szName);
					HKEY hKey;
					
					if (::RegCreateKey (HKEY_LOCAL_MACHINE, lpszSection [nHead], &hKey) == ERROR_SUCCESS) {
						if (::RegQueryValueEx (hKey, _T ("UID"), NULL, &dwType, (LPBYTE)szName, &dwSize) == ERROR_SUCCESS) {
							StringCbPrintf (szFile, ARRAYSIZE (szFile) - 1, _T ("C:\\Temp\\Debug\\0x%s.bmp"), szName);

							HANDLE h = ::CreateFile (szFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL); 

							if (h != INVALID_HANDLE_VALUE) {
								BITMAPFILEHEADER    bmFileHeader; 
								BITMAPINFOHEADER    bmInfoHeader; 
    
								CopyMemory (&bmFileHeader, &pOemPDEV->bmFileHeader, sizeof (bmFileHeader));

								//bmFileHeader.bfType		= 0x4d42;
								bmFileHeader.bfSize			= sizeof (bmFileHeader) + sizeof (bmInfoHeader) + dwSlice;
								//bmFileHeader.bfReserved1	= 0;
								//bmFileHeader.bfReserved2	= 0;
								//bmFileHeader.bfOffBits	= sizeof (bmInfoHeader);

								CopyMemory (&bmInfoHeader, &pOemPDEV->bmInfoHeader, sizeof (bmInfoHeader));

								//bmInfoHeader.biSize			= sizeof (bmInfoHeader); 
								bmInfoHeader.biWidth			= nChannelsLogical /* nChannels */; 
								bmInfoHeader.biHeight			= -abs (lWidthHead); 
								//bmInfoHeader.biPlanes			= 1; 
								//bmInfoHeader.biBitCount		= 1; 
								//bmInfoHeader.biCompression	= BI_RGB; 
								bmInfoHeader.biSizeImage		= dwSlice; 
								//bmInfoHeader.biXPelsPerMeter	= pOemPDEV->bmInfoHeader.biXPelsPerMeter; 
								//bmInfoHeader.biYPelsPerMeter	= pOemPDEV->bmInfoHeader.biYPelsPerMeter; 
								//bmInfoHeader.biClrUsed		= pOemPDEV->bmInfoHeader.biClrUsed; 
								//bmInfoHeader.biClrImportant	= pOemPDEV->bmInfoHeader.biClrImportant; 

								/*
								DebugMsg (
									DLLTEXT ("file            = %s\r\n") 
									DLLTEXT ("bfType          = %d\r\n") 
									DLLTEXT ("bfSize          = %d\r\n") 
									DLLTEXT ("bfReserved1     = %d\r\n") 
									DLLTEXT ("bfReserved2     = %d\r\n") 
									DLLTEXT ("bfOffBits       = %d\r\n"),
									szFile,
									bmFileHeader.bfType,
									bmFileHeader.bfSize,
									bmFileHeader.bfReserved1,
									bmFileHeader.bfReserved2,
									bmFileHeader.bfOffBits);
								DebugMsg (
									DLLTEXT ("biSize          = %d\r\n") 
									DLLTEXT ("biWidth         = %d\r\n") 
									DLLTEXT ("biHeight        = %d\r\n") 
									DLLTEXT ("biPlanes        = %d\r\n") 
									DLLTEXT ("biBitCount      = %d\r\n") 
									DLLTEXT ("biCompression   = %d\r\n") 
									DLLTEXT ("biSizeImage     = %d\r\n") 
									DLLTEXT ("biXPelsPerMeter = %d\r\n") 
									DLLTEXT ("biYPelsPerMeter = %d\r\n") 
									DLLTEXT ("biClrUsed       = %d\r\n") 
									DLLTEXT ("biClrImportant  = %d\r\n"),
									bmInfoHeader.biSize, 
									bmInfoHeader.biWidth, 
									bmInfoHeader.biHeight, 
									bmInfoHeader.biPlanes, 
									bmInfoHeader.biBitCount,
									bmInfoHeader.biCompression, 
									bmInfoHeader.biSizeImage, 
									bmInfoHeader.biXPelsPerMeter, 
									bmInfoHeader.biYPelsPerMeter, 
									bmInfoHeader.biClrUsed, 
									bmInfoHeader.biClrImportant);
								*/

								// Flip the biHeight member so that it denotes top-down bitmap 
								bmInfoHeader.biHeight = abs (bmInfoHeader.biHeight) * -1;

								::WriteFile (h, (LPSTR)&bmFileHeader, sizeof (BITMAPFILEHEADER), &dwWritten, NULL);
								::WriteFile (h, (LPSTR)&bmInfoHeader, sizeof (BITMAPINFOHEADER), &dwWritten, NULL);
								
								if (pOemPDEV->bColorTable)
									::WriteFile (h, pOemPDEV->prgbq, pOemPDEV->cPalColors * sizeof (ULONG), &dwWritten, NULL); 

								::WriteFile (h, (LPSTR)lpSlice, bmInfoHeader.biSizeImage, &dwWritten, NULL);
								::CloseHandle (h);

								DebugMsg (DLLTEXT ("%s [%d x %d dpi] [%d x %d]"), 
									szFile, 
									bmInfoHeader.biXPelsPerMeter, bmInfoHeader.biYPelsPerMeter,
									bmInfoHeader.biWidth, abs (bmInfoHeader.biHeight)); 
							}
						}
					}

				}

				GlobalUnlock (hSlice);
				GlobalFree (hSlice);
				//ySrc += nChannelsLogical /* nChannels */;
				ySrc += (int)floor (dNozzleSpan * pOemPDEV->bmInfoHeader.biYPelsPerMeter);

				if (ySrc >= abs (lHeight)) 
					break;
			}
		}

		if (bDebug) {
			WCHAR szFile [_MAX_PATH];

			StringCbPrintf (szFile, ARRAYSIZE (szFile) - 1, _T ("C:\\Temp\\Debug\\%s.bmp"), _T ("PrintJob"));

			HANDLE h = ::CreateFile (szFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL); 

			if (h != INVALID_HANDLE_VALUE) {
				/*
				DebugMsg (
					DLLTEXT ("file            = %s\r\n") 
					DLLTEXT ("bfType          = %d\r\n") 
					DLLTEXT ("bfSize          = %d\r\n") 
					DLLTEXT ("bfReserved1     = %d\r\n") 
					DLLTEXT ("bfReserved2     = %d\r\n") 
					DLLTEXT ("bfOffBits       = %d\r\n"),
					szFile,
					pOemPDEV->bmFileHeader.bfType,
					pOemPDEV->bmFileHeader.bfSize,
					pOemPDEV->bmFileHeader.bfReserved1,
					pOemPDEV->bmFileHeader.bfReserved2,
					pOemPDEV->bmFileHeader.bfOffBits);
				DebugMsg (
					DLLTEXT ("biSize          = %d\r\n") 
					DLLTEXT ("biWidth         = %d\r\n") 
					DLLTEXT ("biHeight        = %d\r\n") 
					DLLTEXT ("biPlanes        = %d\r\n") 
					DLLTEXT ("biBitCount      = %d\r\n") 
					DLLTEXT ("biCompression   = %d\r\n") 
					DLLTEXT ("biSizeImage     = %d\r\n") 
					DLLTEXT ("biXPelsPerMeter = %d\r\n") 
					DLLTEXT ("biYPelsPerMeter = %d\r\n") 
					DLLTEXT ("biClrUsed       = %d\r\n") 
					DLLTEXT ("biClrImportant  = %d\r\n"),
					pOemPDEV->bmInfoHeader.biSize, 
					pOemPDEV->bmInfoHeader.biWidth, 
					pOemPDEV->bmInfoHeader.biHeight, 
					pOemPDEV->bmInfoHeader.biPlanes, 
					pOemPDEV->bmInfoHeader.biBitCount,
					pOemPDEV->bmInfoHeader.biCompression, 
					pOemPDEV->bmInfoHeader.biSizeImage, 
					pOemPDEV->bmInfoHeader.biXPelsPerMeter, 
					pOemPDEV->bmInfoHeader.biYPelsPerMeter, 
					pOemPDEV->bmInfoHeader.biClrUsed, 
					pOemPDEV->bmInfoHeader.biClrImportant);
				*/

				::WriteFile (h, (LPSTR)&pOemPDEV->bmFileHeader, sizeof (BITMAPFILEHEADER), &dwWritten, NULL);
				::WriteFile (h, (LPSTR)&pOemPDEV->bmInfoHeader, sizeof (BITMAPINFOHEADER), &dwWritten, NULL);
				
				if (pOemPDEV->bColorTable)
					::WriteFile (h, pOemPDEV->prgbq, pOemPDEV->cPalColors * sizeof (ULONG), &dwWritten, NULL); 

				::WriteFile (h, (LPSTR)pOemPDEV->pBufStart, pOemPDEV->bmInfoHeader.biSizeImage, &dwWritten, NULL);
				::CloseHandle (h);

				DebugMsg (DLLTEXT ("%s [%d x %d dpi] [%f\" x %f\"]"), 
					szFile, 
					pOemPDEV->bmInfoHeader.biXPelsPerMeter, pOemPDEV->bmInfoHeader.biYPelsPerMeter,
					(double)pOemPDEV->bmInfoHeader.biWidth / (double)pOemPDEV->bmInfoHeader.biXPelsPerMeter, 
					(double)abs (pOemPDEV->bmInfoHeader.biHeight) / (double)pOemPDEV->bmInfoHeader.biYPelsPerMeter); 
			}
		}

        // Dump headers first
        //
        dwWritten = pDevObj->pDrvProcs->DrvWriteSpoolBuf(pDevObj, (void*)&(pOemPDEV->bmFileHeader), sizeof(BITMAPFILEHEADER));
        dwWritten = pDevObj->pDrvProcs->DrvWriteSpoolBuf(pDevObj, (void*)&(pOemPDEV->bmInfoHeader), sizeof(BITMAPINFOHEADER));
        if (pOemPDEV->bColorTable)
        {
            dwWritten = pDevObj->pDrvProcs->DrvWriteSpoolBuf(pDevObj, pOemPDEV->prgbq, pOemPDEV->cPalColors * sizeof(ULONG));
            LocalFree(pOemPDEV->prgbq);
        }

        // Dump the data now
        //
        dwWritten = pDevObj->pDrvProcs->DrvWriteSpoolBuf(pDevObj, pOemPDEV->pBufStart, pOemPDEV->bmInfoHeader.biSizeImage);

        // Free memory for the data buffers
        //
        vFreeBuffer(pOemPDEV);
    }

    FreeMphc ();

    // Punt call back to UNIDRV.
    //
    return (pOemPDEV->m_pfnDrvEndDoc)(pso, fl);
}


