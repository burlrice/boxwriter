// PrinterDriverDlg.h : header file
//

#if !defined(AFX_PRINTERDRIVERDLG_H__7965EA94_4096_40C5_A4C9_A3D2237CD08F__INCLUDED_)
#define AFX_PRINTERDRIVERDLG_H__7965EA94_4096_40C5_A4C9_A3D2237CD08F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CPrinterDriverDlg dialog

class CPrinterDriverDlg : public CDialog
{
// Construction
public:
	CPrinterDriverDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CPrinterDriverDlg)
	enum { IDD = IDD_PRINTERDRIVER__DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrinterDriverDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CPrinterDriverDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnPrint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTERDRIVERDLG_H__7965EA94_4096_40C5_A4C9_A3D2237CD08F__INCLUDED_)
