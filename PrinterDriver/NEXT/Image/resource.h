//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Image.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_IMAGETYPE                   129
#define IDM_GET_IMAGE                   32771
#define IDM_PUT_IMAGE                   32772
#define ID_BMP_DELETE                   32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
