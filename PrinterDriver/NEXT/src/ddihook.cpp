//  THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
//  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
//  PARTICULAR PURPOSE.
//
//  Copyright  1998 - 2003  Microsoft Corporation.  All Rights Reserved.
//
//  FILE:    DDIHook.cpp
//    
//
//  PURPOSE:  Implementation of DDI Hook OEMEndDoc. This function
//          dumps the buffered bitmap data out. 
//
//
//  Functions:
//          OEMEndDoc
//
//      
//
//
//  PLATFORMS:  Windows XP, Windows Server 2003, Windows codenamed Longhorn
//
//
//  History: 
//          06/24/03    xxx created.
//
//

#include "precomp.h"
#include <WINSPOOL.H>
#include <Winsplp.h>
#include <Winddiui.h>
#include <math.h>
#include <Winsock2.h>
#include <time.h>
#include "ddihook.h"
#include "bitmap.h"
#include "debug.h"
#include "strsafe.h"
//#include "Ioctls.h"
#include "TypeDefs.h"
#include "DevGuids.h"
#include "..\..\MPHCAPI\Master.h"
#include "..\..\Editor\DLL\Common\DbTypeDefs.h"
#include "..\..\IP\Marksman\fj_socket.h"
#include "Broadcast.h"

#define REVERSEWORD32(word) ( ((word&0x000000ff)<<24) | ((word&0x0000ff00)<<8) | ((word&0x00ff0000)>>8) | ((word&0xff000000)>>24) )
#define REVERSEWORD16(word) ( ((word&0x000000ff)<<8) | ((word&0x0000ff00)>>8) )
#define LOADREVERSEWORD32(pointer) ( (((*(((BYTE*)pointer))))) | (((*(((BYTE*)pointer)+1)))<<8) | (((*(((BYTE*)pointer)+2)))<<16) | (((*(((BYTE*)pointer)+3)))<<24) )
#define LOADREVERSEWORD16(pointer) ( (((*(((BYTE*)pointer))))) | (((*(((BYTE*)pointer)+1)))<<8) )

static const DWORD dwSleep = 25;//250;
static const LPCWSTR lpszRootSection = _T ("SOFTWARE\\FoxJet_Driver");

WCHAR * FormatMessage (DWORD dwError);
bool SendBinary (SOCKET s, ULONG lBatchID, ULONG lCmd, LPBYTE lpData, ULONG lLen, _FJ_SOCKET_MESSAGE * pReply = NULL);
bool Send (SOCKET s, ULONG lBatchID, ULONG lCmd, WCHAR * lpsz, _FJ_SOCKET_MESSAGE * pReply = NULL, WCHAR * lpszReply = NULL, ULONG lReplyLen = 0);
DWORD GetIpcCmd (const WCHAR * lpsz);
WCHAR * GetIpcCmdName (DWORD dw);
bool fj_bmpCheckFormat (LPBYTE pbmp);
void DebugIpcCmd (const _FJ_SOCKET_MESSAGE & cmd, WCHAR * lpsz, WCHAR * lpszFile, ULONG lLine);
void ntohl (_FJ_SOCKET_MESSAGE & cmd);
int ReadINT (WCHAR * lpsz, WCHAR * lpszKeyword, int nDef = 0);
WCHAR * ReadString (WCHAR * lpsz, WCHAR * lpszKeyword, WCHAR * lpszValue, ULONG lLen);
int GetChannels (SOCKET s, POEMPDEV pOemPDEV, int nHeadNumber, double & dHeight);
void SaveSlice (BITMAPFILEHEADER & bmFileHeader, BITMAPINFOHEADER & bmInfoHeader, POEMPDEV pOemPDEV, LPBYTE lpBits);
void CreateTempDir ();
void SaveBitmap (POEMPDEV pOemPDEV);
void Slice (BITMAPFILEHEADER & bmFileHeader, BITMAPINFOHEADER & bmInfoHeader, POEMPDEV pOemPDEV, int nChannels);
bool SendSlice (SOCKET s, BITMAPFILEHEADER & bmFileHeader, BITMAPINFOHEADER & bmInfoHeader, POEMPDEV pOemPDEV, double dHeight, int nChannels, bool bDriver);
bool BeginEditSession (SOCKET s, POEMPDEV pOemPDEV);
bool EndEditSession (SOCKET s, POEMPDEV pOemPDEV);
bool DoPrint (SOCKET s, POEMPDEV pOemPDEV, bool bDriver, int nCopies, int nHeadNumber, SOCKET sockUDP);
int Round (double d);
int GetBoldFactor (BITMAPFILEHEADER & bmFileHeader, BITMAPINFOHEADER & bmInfoHeader, double dRes);
bool GetVersionNumber (SOCKET s, POEMPDEV pOemPDEV, int & nMajor, int & nMinor);
bool IsSocketDataPending (SOCKET s);
void ntohl (_FJ_SOCKET_MESSAGE & cmd);
BOOL Broadcast (SOCKET s, UINT nMsg, WPARAM wParam, LPARAM lParam);
void DeletePreview (LPCWSTR szPort);

#define DEBUGIPC(c, s) DebugIpcCmd ((c), (s), _T (__FILE__), __LINE__);

template <class T>
T BindTo (T value, T lBound, T uBound)
{
	return max (min (value, uBound), lBound);
}

//////////////////////////////////////////////////////////////////////////////////////////

BOOL Broadcast (SOCKET s, UINT nMsg, WPARAM wParam, LPARAM lParam)
{
	OEMDBG(DBG_VERBOSE, L"");
	BOOL bResult = false;

	if (s != SOCKET_ERROR) {
		PRINTDRIVER_SOCKETMSG msg;

		memset (&msg, 0, sizeof (msg));
		msg.m_nMsg = nMsg;
		msg.m_wParam = (DWORD)wParam;
		msg.m_lParam = (DWORD)lParam;

		SOCKADDR_IN remoteAddr;
		remoteAddr.sin_family = AF_INET;
		remoteAddr.sin_port = htons (PRINTDRIVER_SOCKET_NUMBER);
		remoteAddr.sin_addr.s_addr = inet_addr ("127.0.0.1"); //htonl(INADDR_BROADCAST);

		DWORD dwBytes = sendto (s, (const char *)&msg, sizeof (msg), 0, (const SOCKADDR* )&remoteAddr, sizeof(remoteAddr));
		bResult = dwBytes == sizeof (msg);

		DebugMsg (DLLTEXT ("Broadcast: %d %d %d [%d: %d]"), nMsg, wParam, lParam, dwBytes, bResult);
	}

	return bResult;
}

bool GetVersionNumber (SOCKET s, POEMPDEV pOemPDEV, int & nMajor, int & nMinor)
{
	WCHAR sz [128] = { 0 };
	WCHAR szVer [32] = { 0 };
	_FJ_SOCKET_MESSAGE reply;

	nMajor = nMinor = 0;
	Send (s, pOemPDEV->m_lCmdID++, IPC_GET_SW_VER_INFO, _T ("PRINT_DRIVER=T;"), &reply, sz, ARRAYSIZE (sz));
	ReadString (sz, _T ("SW_VER"), szVer, ARRAYSIZE (szVer));

	return swscanf (szVer, _T ("%d.%d"), &nMajor, &nMinor) == 2 ? true : false;
}

int GetBoldFactor (BITMAPFILEHEADER & bmFileHeader, BITMAPINFOHEADER & bmInfoHeader, double dRes)
{
	OEMDBG(DBG_VERBOSE, L"GetBoldFactor entry.");
	const double dTargetWidth = (double)bmInfoHeader.biWidth / (double)bmInfoHeader.biXPelsPerMeter;
	double dMin = abs (((double)bmInfoHeader.biWidth / dRes) - dTargetWidth);
	int nResult = 1;

	UNREFERENCED_PARAMETER (bmFileHeader);

	//DebugMsg (DLLTEXT ("dTargetWidth: %.03f, dMin: %.03f"), dTargetWidth, dMin);

	for (int i = 1; i < 10; i++) {
		double dBoxWidth = (((double)bmInfoHeader.biWidth * (double)i) / dRes);
		double dDiff = abs (dBoxWidth - dTargetWidth);

		if (dDiff < dMin) {
			nResult = i;
			dMin = dDiff;
		}

		//DebugMsg (DLLTEXT ("[%d (%d)]: dDiff: %.03f, dMin: %.03f. dBoxWidth: %.03f"), i, nResult, dDiff, dMin, dBoxWidth);
	}

	return nResult;
}

bool DoPrint (SOCKET s, POEMPDEV pOemPDEV, bool bDriver, int nCopies, int nHeadNumber, SOCKET sockUDP)
{
	OEMDBG(DBG_VERBOSE, L"DoPrint entry.");
	const DWORD dwTimeout = BindTo <DWORD> (Foxjet::GetProfileDWORD (::lpszRootSection, _T ("PrintTimeout"), 1000 * 60), 1000, 1000 * 60 * 10); 
	const double dTimeout = (double)dwTimeout / 1000.0;
	clock_t clock_tStart = clock ();
	double dDiff = 0;
	bool bResult = false;

	if (!bDriver) {
		Send (s, pOemPDEV->m_lCmdID++, IPC_SET_PRINTER_MODE, _T ("PRINT_MODE=RESUME;"));
		Send (s, pOemPDEV->m_lCmdID++, IPC_SET_LABEL_ID_SELECTED, _T ("LABELID=\"PrintDriver\""));
		Send (s, pOemPDEV->m_lCmdID++, IPC_SET_SELECTED_COUNT, _T ("CountSel=0;"));
		Send (s, pOemPDEV->m_lCmdID++, IPC_SET_STATUS_MODE, _T ("STATUS_MODE=ON;"));
	
		DebugMsg (DLLTEXT ("*** ready to print ***"));

		do {
			_FJ_SOCKET_MESSAGE reply;
			WCHAR sz [_FJ_SOCKET_BUFFER_SIZE] = { 0 };

			Send (s, pOemPDEV->m_lCmdID++, IPC_GET_SELECTED_COUNT, _T (""), &reply, sz, ARRAYSIZE (sz));
			int nCount = ReadINT (sz, _T ("CountSel"));
			bResult = nCount >= nCopies ? true : false;

			if (!bResult) {
				dDiff = (double)(clock () - clock_tStart) / CLOCKS_PER_SEC;
				::Sleep (::dwSleep);
			}
		}
		while ((dDiff < dTimeout) && !bResult);

		Send (s, pOemPDEV->m_lCmdID++, IPC_SET_PRINTER_MODE, _T ("PRINT_MODE=STOP;"));
	}
	else {
		int nCount = 0;

		Broadcast (sockUDP, PRINTDRIVER_MSG_JOBREADY, nHeadNumber, nCopies);
		DebugMsg (DLLTEXT ("*** waiting for CountSel ***"));

		do {
			if (IsSocketDataPending (s)) {
				_FJ_SOCKET_MESSAGE reply;
				
				memset (&reply, 0, sizeof (_FJ_SOCKET_MESSAGE));
				::Sleep (::dwSleep);

				if (recv (s, (char *)&reply.Header, sizeof (reply.Header), 0) == sizeof (_FJ_SOCKET_MESSAGE_HEADER)) {
					ntohl (reply);
					DEBUGIPC (reply, _T ("              "));

					if (reply.Header.Length) {
						WCHAR sz [_FJ_SOCKET_BUFFER_SIZE] = { 0 };

						::recv (s, (char *)reply.Data, reply.Header.Length, 0);
						a2w ((char *)reply.Data, reply.Header.Length, sz, ARRAYSIZE (sz));
						int nCancel = ReadINT (sz, _T ("CancelPrintJob"));

						nCount = ReadINT (sz, _T ("CountSel"));
						bResult = nCount >= nCopies ? true : false;
						DEBUGIPC (reply, bResult ? _T ("[MATCHED    ] ") : _T ("[not matched] "));

						if (nCancel == 1) {
							DebugMsg (DLLTEXT ("HW cancel detected"));
							Broadcast (sockUDP, PRINTDRIVER_MSG_ABORTPRINT, nHeadNumber, PRINTDRIVER_ERROR_CANCELHW);
							DeletePreview (pOemPDEV->szPort);
							return false;
						}
					}
				}
			}

			//if (!bResult) 
			{
				dDiff = (double)(clock () - clock_tStart) / CLOCKS_PER_SEC;
				::Sleep (::dwSleep);

				if (dDiff > 1.0) {
					WCHAR sz [_MAX_PATH];
					
					StringCbPrintf (sz, ARRAYSIZE (sz), _T ("SOFTWARE\\FoxJet_Driver\\Head%d"), nHeadNumber);
					const DWORD dwCancel = Foxjet::GetProfileDWORD (sz, _T ("Cancel"), 0);

					if (dwCancel == 1) {
						DebugMsg (DLLTEXT ("SW cancel detected"));
						Broadcast (sockUDP, PRINTDRIVER_MSG_ABORTPRINT, nHeadNumber, PRINTDRIVER_ERROR_CANCELSW);
						Foxjet::WriteProfileDWORD (sz, _T ("Cancel"), 0);
						DeletePreview (pOemPDEV->szPort);
						return false;
					}
					else {
						if (nCount)
							Broadcast (sockUDP, PRINTDRIVER_MSG_DOPRINT, nHeadNumber, nCount);
						//else
						//	Broadcast (sockUDP, PRINTDRIVER_MSG_BEGINPRINT, nHeadNumber, nCopies);
					}

					clock_tStart = clock ();
				}
			}
		}
		while (/* (dDiff < dTimeout) && */ !bResult);
	}

	if (bResult)
		DebugMsg (DLLTEXT ("*** print job completed ***"));
	else
		DebugMsg (DLLTEXT ("timed out waiting for print cycle [%.03fs (%.03fs)]"), dTimeout, dDiff);

	return bResult;
}

bool BeginEditSession (SOCKET s, POEMPDEV pOemPDEV)
{
	OEMDBG(DBG_VERBOSE, L"BeginEditSession entry.");
	_FJ_SOCKET_MESSAGE reply;
	WCHAR sz [_FJ_SOCKET_BUFFER_SIZE] = { 0 };
	WCHAR szValue [128] = { 0 };
	WCHAR szAddr [128] = { 0 };
	bool bResult = false;
	
	if (s != INVALID_SOCKET) {
		SOCKADDR_IN addr;

		memset (&addr, 0, sizeof(addr));
		int nLen = sizeof(addr);

		getsockname (s, (SOCKADDR *)&addr, &nLen);
		a2w (inet_ntoa (addr.sin_addr), (int)strlen (inet_ntoa (addr.sin_addr)), szAddr, ARRAYSIZE (szAddr));
		DebugMsg (DLLTEXT ("local address: %s"), szAddr);
	}

	//EDIT_LOCK=SET;EDIT_IP=192.168.2.3;EDIT_TIME=2010/07/03 12:05:19;

	if (Send (s, pOemPDEV->m_lCmdID++, IPC_EDIT_START, _T (""), &reply, sz, ARRAYSIZE (sz))) 
		if (ReadString (sz, _T ("EDIT_LOCK"), szValue, ARRAYSIZE (szValue))) 
			if (!_wcsicmp (szValue, _T ("SET"))) 
				if (ReadString (sz, _T ("EDIT_IP"), szValue, ARRAYSIZE (szValue))) 
					bResult = _wcsicmp (szValue, szAddr) == 0 ? true : false;

	if (!bResult) 
		DebugMsg (DLLTEXT ("FAILED: IPC_EDIT_START: %s;"), sz);

	return bResult;
}

bool EndEditSession (SOCKET s, POEMPDEV pOemPDEV)
{
	OEMDBG(DBG_VERBOSE, L"EndEditSession entry.");
	_FJ_SOCKET_MESSAGE reply;
	WCHAR sz [_FJ_SOCKET_BUFFER_SIZE] = { 0 };
	WCHAR szValue [128] = { 0 };
	bool bResult = false;
	
	if (Send (s, pOemPDEV->m_lCmdID++, IPC_EDIT_SAVE, _T (""), &reply, sz, ARRAYSIZE (sz))) 
		if (ReadString (sz, _T ("EDIT_LOCK"), szValue, ARRAYSIZE (szValue))) 
			bResult = _wcsicmp (szValue, _T ("FREE")) == 0 ? true : false;

	if (!bResult) 
		DebugMsg (DLLTEXT ("FAILED: IPC_EDIT_SAVE: %s;"), sz);

	return bResult;
}

bool SendSlice (SOCKET s, BITMAPFILEHEADER & bmFileHeader, BITMAPINFOHEADER & bmInfoHeader, POEMPDEV pOemPDEV, double dHeight, int nChannels, bool bDriver)
{
	OEMDBG(DBG_VERBOSE, L"SendSlice entry.");
	WCHAR sz [1024] = { 0 };
	DWORD dwBitmapLen = bmFileHeader.bfSize + pOemPDEV->cbHeaderOffBits;
	bool bSelectLabel = false;
	BOOL bDebug = TRUE;//Foxjet::GetProfileDWORD (::lpszRootSection, _T ("Debug"), 0) > 0;

	StringCbPrintf (sz, ARRAYSIZE (sz), _T ("{bmp,\"PrintDriver\",%lu}"), dwBitmapLen);
	DWORD dwStrLen = (DWORD)wcslen (sz);
	DWORD dwTotalLen = dwStrLen + dwBitmapLen + 1;
	DebugMsg (DLLTEXT ("%s (%d), dwBitmapLen: %d, dwTotalLen: %d"), sz, dwStrLen, dwBitmapLen, dwTotalLen);
	
	if (HGLOBAL hData = GlobalAlloc (GPTR, dwTotalLen)) {
		if (LPBYTE lpData = (LPBYTE)GlobalLock (hData)) {
			LPBYTE p = lpData;
			const ULONG lHeight = bmInfoHeader.biHeight;
			double dRes = 150.0;

			memset (p, 0, dwTotalLen);
			w2a (sz, dwStrLen, (char *)p, dwStrLen); 
			p += (dwStrLen + 1);

			bmInfoHeader.biHeight = abs (bmInfoHeader.biHeight);

			memcpy (p, (LPSTR)&bmFileHeader, sizeof (BITMAPFILEHEADER));				p += sizeof (BITMAPFILEHEADER);
			memcpy (p, (LPSTR)&bmInfoHeader, sizeof (BITMAPINFOHEADER));				p += sizeof (BITMAPINFOHEADER);
			
			if (pOemPDEV->bColorTable) {
				memcpy (p, pOemPDEV->prgbq, pOemPDEV->cPalColors * sizeof (ULONG));		p += (pOemPDEV->cPalColors * sizeof (ULONG));
			}

			if (!bDriver) {
				_FJ_SOCKET_MESSAGE reply;

				if (Send (s, pOemPDEV->m_lCmdID++, IPC_GET_SYSTEM_INFO, _T (""), &reply, sz, ARRAYSIZE (sz))) {
					double dCount = ReadINT (sz, _T ("ENCODER_WHEEL"), 300);
					int nDivisor = ReadINT (sz, _T ("ENCODER_DIVISOR"), 2);

					switch (nDivisor) {
					case 1:	dRes = dCount;			break; // 300
					default:
					case 2:	dRes = dCount / 2.0;	break; // 150
					case 3:	dRes = dCount / 1.5;	break; // 200
					}

					DebugMsg (DLLTEXT ("ENCODER_COUNT=%f;ENCODER_DIVISOR=%d;"), dCount, nDivisor);
				}
			}

			//memcpy (p, (LPSTR)pOemPDEV->pBufStart, bmInfoHeader.biSizeImage);			p += bmInfoHeader.biSizeImage;
			{
				const LPBYTE lpBits = p;
				const double dDpiSrc = (double)pOemPDEV->bmInfoHeader.biYPelsPerMeter;
				const double dDpiDest = (double)nChannels / dHeight;
				const double dStep = dDpiSrc / dDpiDest;
				LONG lRowLenBits = (int)ceil ((long double)bmInfoHeader.biWidth / 16.0) * 16; // win bitmap is word aligned
				LONG lRowLenBytes = (LONG)(lRowLenBits / 8.0);

				/*
				DebugMsg (DLLTEXT ("lRowLenBytes:          %d"), lRowLenBytes);
				DebugMsg (DLLTEXT ("bmInfoHeader.biWidth:  %d [%d]"), bmInfoHeader.biWidth, pOemPDEV->bmInfoHeader.biWidth);
				DebugMsg (DLLTEXT ("bmInfoHeader.biHeight: %d [%d]"), bmInfoHeader.biHeight, pOemPDEV->bmInfoHeader.biHeight);
				DebugMsg (DLLTEXT ("dDpiSrc:               %f"), dDpiSrc);
				DebugMsg (DLLTEXT ("dDpiDest:              %f"), dDpiDest);
				DebugMsg (DLLTEXT ("dStep:                 %f"), dStep);
				DebugMsg (DLLTEXT ("dHeight:               %f"), dHeight);
				*/

				for (int i = 0; i < bmInfoHeader.biHeight; i++) {
					int nRowSrc = bmInfoHeader.biHeight - (i + 1);
					double dRowDest = (nRowSrc * dStep);
					int nRowDest = Round (dRowDest);
					
					//DebugMsg (DLLTEXT ("[%-03d (%-03d)]: %-03d [%-04f]"), nRowSrc, i, nRowDest, dRowDest);

					LPBYTE pRow = &pOemPDEV->pBufStart [nRowDest * lRowLenBytes];
					memcpy (p, pRow, lRowLenBytes);
					p += lRowLenBytes;
				}

				if (bDebug) 
					SaveSlice (bmFileHeader, bmInfoHeader, pOemPDEV, lpBits);
			}

			if (!fj_bmpCheckFormat (lpData + dwStrLen + 1)) 
				DebugMsg (DLLTEXT ("fj_bmpCheckFormat FAILED"));
			else {
				SendBinary (s, pOemPDEV->m_lCmdID++, bDriver ? IPC_PUT_BMP_DRIVER : IPC_PUT_BMP, lpData, dwTotalLen);
				DebugMsg (DLLTEXT ("biWidth:   %d"), bmInfoHeader.biWidth);

				if (bDriver) 
					bSelectLabel = true;
				else {
					_FJ_SOCKET_MESSAGE reply;
					const int nBold = GetBoldFactor (bmFileHeader, bmInfoHeader, dRes);
					double dBoxWidth = (bmInfoHeader.biWidth * (double)nBold) / dRes;

					///*
					DebugMsg (DLLTEXT ("nBold:     %d"), nBold);
					DebugMsg (DLLTEXT ("dRes:      %f"), dRes);
					DebugMsg (DLLTEXT ("dBoxWidth: %f"), dBoxWidth);
					//*/

					StringCbPrintf (sz, ARRAYSIZE (sz), 
						_T ("{Label,\"PrintDriver\",12.0,%.03f,0,3,F,\"PrintDriver\"}")
						_T ("{Message,\"PrintDriver\",1,\"PrintDriver-Bitmap-1\"}")
						_T ("{Bitmap,\"PrintDriver-Bitmap-1\",0.000000,0,D,%d,1,F,F,F,\"PrintDriver\"}"),
						dBoxWidth,
						nBold);
					Send (s, pOemPDEV->m_lCmdID++, IPC_PUT_PRINTER_ELEMENT, sz, &reply);

					if (reply.Header.Cmd == IPC_PUT_PRINTER_ELEMENT)
						bSelectLabel = true;
				}
			}

			bmInfoHeader.biHeight = lHeight;
		}

		GlobalUnlock (hData);
		GlobalFree (hData);
	}

	return bSelectLabel;
}

void Slice (BITMAPFILEHEADER & bmFileHeader, BITMAPINFOHEADER & bmInfoHeader, POEMPDEV pOemPDEV, int nChannels)
{
	OEMDBG(DBG_VERBOSE, L"Slice entry.");

	CopyMemory (&bmFileHeader, &pOemPDEV->bmFileHeader, sizeof (bmFileHeader));
	CopyMemory (&bmInfoHeader, &pOemPDEV->bmInfoHeader, sizeof (bmInfoHeader));

	bmInfoHeader.biHeight		= -nChannels; // Flip the biHeight member so that it denotes top-down bitmap 
	bmInfoHeader.biWidth		= abs ((((int)bmInfoHeader.biWidth + 31) / 32) * 32);
	bmInfoHeader.biSizeImage	= (DWORD)(abs (bmInfoHeader.biHeight) * bmInfoHeader.biWidth / 8.0); 
	bmFileHeader.bfSize			= bmFileHeader.bfOffBits + bmInfoHeader.biSizeImage;
	
	/*
	{
		ULONG lLength = LOADREVERSEWORD32(&bmFileHeader.bfSize);
		ULONG lOffset = LOADREVERSEWORD32(&bmFileHeader.bfOffBits);

		ULONG lHeight = abs (LOADREVERSEWORD32(&bmInfoHeader.biHeight));
		ULONG lWidth  = LOADREVERSEWORD32(&bmInfoHeader.biWidth);
		ULONG lHeightBytes =  (lHeight+ 7)/8;
		ULONG lWidthBytes  = ((lWidth +31)/32)*4;
		ULONG lLengthCalc = lOffset + (lHeight * lWidthBytes);

		DebugMsg (DLLTEXT ("lLength:        %d"), lLength);
		DebugMsg (DLLTEXT ("lOffset:        %d"), lOffset);
		DebugMsg (DLLTEXT ("lHeight:        %d"), lHeight);
		DebugMsg (DLLTEXT ("lWidth:         %d"), lWidth);
		DebugMsg (DLLTEXT ("lHeightBytes:   %d"), lHeightBytes);
		DebugMsg (DLLTEXT ("lWidthBytes:    %d"), lWidthBytes);
		DebugMsg (DLLTEXT ("lLengthCalc:    %d"), lLengthCalc);
		DebugMsg (DLLTEXT (""));
	}
	*/
}

void SaveBitmap (POEMPDEV pOemPDEV)
{
	OEMDBG(DBG_VERBOSE, L"SaveBitmap entry.");
	WCHAR szFile [_MAX_PATH];
	WCHAR szTempDir [_MAX_PATH];
	DWORD dwWritten;

	Foxjet::GetProfileString (::lpszRootSection, _T ("Dir"), szTempDir, ARRAYSIZE (szTempDir), _T ("C:\\Temp\\Debug"));
	StringCbPrintf (szFile, ARRAYSIZE (szFile), _T ("%s\\%s.bmp"), szTempDir, pOemPDEV->szPort);
	DebugMsg (DLLTEXT ("szFile: %s"), szFile); 

	HANDLE h = ::CreateFile (szFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL); 

	if (h == INVALID_HANDLE_VALUE) {
		DebugMsg (DLLTEXT ("failed to create file: %s"), szFile); 
	}
	else {
		/*
		DebugMsg (DLLTEXT ("file            = %s"), szFile);
		DebugMsg (DLLTEXT ("bfType          = %d"), pOemPDEV->bmFileHeader.bfType);
		DebugMsg (DLLTEXT ("bfSize          = %d"), pOemPDEV->bmFileHeader.bfSize);
		DebugMsg (DLLTEXT ("bfReserved1     = %d"), pOemPDEV->bmFileHeader.bfReserved1);
		DebugMsg (DLLTEXT ("bfReserved2     = %d"), pOemPDEV->bmFileHeader.bfReserved2);
		DebugMsg (DLLTEXT ("bfOffBits       = %d"), pOemPDEV->bmFileHeader.bfOffBits);
			
		DebugMsg (DLLTEXT ("biSize          = %d"), pOemPDEV->bmInfoHeader.biSize);
		DebugMsg (DLLTEXT ("biWidth         = %d"), pOemPDEV->bmInfoHeader.biWidth); 
		DebugMsg (DLLTEXT ("biHeight        = %d"), pOemPDEV->bmInfoHeader.biHeight); 
		DebugMsg (DLLTEXT ("biPlanes        = %d"), pOemPDEV->bmInfoHeader.biPlanes);
		DebugMsg (DLLTEXT ("biBitCount      = %d"), pOemPDEV->bmInfoHeader.biBitCount);
		DebugMsg (DLLTEXT ("biCompression   = %d"), pOemPDEV->bmInfoHeader.biCompression); 
		DebugMsg (DLLTEXT ("biSizeImage     = %d"), pOemPDEV->bmInfoHeader.biSizeImage);
		DebugMsg (DLLTEXT ("biXPelsPerMeter = %d"), pOemPDEV->bmInfoHeader.biXPelsPerMeter);
		DebugMsg (DLLTEXT ("biYPelsPerMeter = %d"), pOemPDEV->bmInfoHeader.biYPelsPerMeter);
		DebugMsg (DLLTEXT ("biClrUsed       = %d"), pOemPDEV->bmInfoHeader.biClrUsed);
		DebugMsg (DLLTEXT ("biClrImportant  = %d"), pOemPDEV->bmInfoHeader.biClrImportant);
		*/

		::WriteFile (h, (LPSTR)&pOemPDEV->bmFileHeader, sizeof (BITMAPFILEHEADER), &dwWritten, NULL);
		::WriteFile (h, (LPSTR)&pOemPDEV->bmInfoHeader, sizeof (BITMAPINFOHEADER), &dwWritten, NULL);
		
		if (pOemPDEV->bColorTable)
			::WriteFile (h, pOemPDEV->prgbq, pOemPDEV->cPalColors * sizeof (ULONG), &dwWritten, NULL); 

		::WriteFile (h, (LPSTR)pOemPDEV->pBufStart, pOemPDEV->bmInfoHeader.biSizeImage, &dwWritten, NULL);
		::CloseHandle (h);

		DebugMsg (DLLTEXT ("%s [%d x %d dpi] [%f\" x %f\"]"), 
			szFile, 
			pOemPDEV->bmInfoHeader.biXPelsPerMeter, pOemPDEV->bmInfoHeader.biYPelsPerMeter,
			(double)pOemPDEV->bmInfoHeader.biWidth / (double)pOemPDEV->bmInfoHeader.biXPelsPerMeter, 
			(double)abs (pOemPDEV->bmInfoHeader.biHeight) / (double)pOemPDEV->bmInfoHeader.biYPelsPerMeter); 
	}
}

void CreateTempDir ()
{
	OEMDBG(DBG_VERBOSE, L"CreateTempDir entry.");
	WCHAR szTempDir [_MAX_PATH];

	Foxjet::GetProfileString (::lpszRootSection, _T ("Dir"), szTempDir, ARRAYSIZE (szTempDir), _T ("C:\\Temp\\Debug"));

	if (::GetFileAttributes (szTempDir) == -1) {
		if (!::CreateDirectory (szTempDir, NULL)) {
			DebugMsg (DLLTEXT ("failed to create dir: %s"), szTempDir); 
		}
	}
}

void DeletePreview (LPCWSTR szPort)
{
	OEMDBG(DBG_VERBOSE, L"DeletePreview entry.");

	WCHAR szFile [_MAX_PATH];
	WCHAR szTempDir [_MAX_PATH];
	BOOL bDelete;

	Foxjet::GetProfileString (::lpszRootSection, _T ("Dir"), szTempDir, ARRAYSIZE (szTempDir), _T ("C:\\Temp\\Debug"));
	StringCbPrintf (szFile, ARRAYSIZE (szFile), _T ("%s\\%s_slice.bmp"), szTempDir, szPort);
	bDelete = DeleteFile (szFile);
	DebugMsg (DLLTEXT ("DeleteFile: %s [%d]"), szFile, bDelete); 

	StringCbPrintf (szFile, ARRAYSIZE (szFile), _T ("%s\\%s.bmp"), szTempDir, szPort);
	bDelete = DeleteFile (szFile);
	DebugMsg (DLLTEXT ("DeleteFile: %s [%d]"), szFile, bDelete); 
}

void SaveSlice (BITMAPFILEHEADER & bmFileHeader, BITMAPINFOHEADER & bmInfoHeader, POEMPDEV pOemPDEV, LPBYTE lpBits)
{
	OEMDBG(DBG_VERBOSE, L"SaveSlice entry.");
	WCHAR szFile [_MAX_PATH];
	WCHAR szTempDir [_MAX_PATH];
	DWORD dwWritten;

	Foxjet::GetProfileString (::lpszRootSection, _T ("Dir"), szTempDir, ARRAYSIZE (szTempDir), _T ("C:\\Temp\\Debug"));
	StringCbPrintf (szFile, ARRAYSIZE (szFile), _T ("%s\\%s_slice.bmp"), szTempDir, pOemPDEV->szPort);
	DebugMsg (DLLTEXT ("szFile: %s"), szFile); 

	HANDLE h = ::CreateFile (szFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL); 

	if (h != INVALID_HANDLE_VALUE) {
		/*
		DebugMsg (DLLTEXT ("file            = %s"), szFile);
		DebugMsg (DLLTEXT ("bfType          = %d"), bmFileHeader.bfType);
		DebugMsg (DLLTEXT ("bfSize          = %d"), bmFileHeader.bfSize);
		DebugMsg (DLLTEXT ("bfReserved1     = %d"), bmFileHeader.bfReserved1);
		DebugMsg (DLLTEXT ("bfReserved2     = %d"), bmFileHeader.bfReserved2);
		DebugMsg (DLLTEXT ("bfOffBits       = %d"), bmFileHeader.bfOffBits);
			
		DebugMsg (DLLTEXT ("biSize          = %d"), bmInfoHeader.biSize);
		DebugMsg (DLLTEXT ("biWidth         = %d"), bmInfoHeader.biWidth); 
		DebugMsg (DLLTEXT ("biHeight        = %d"), bmInfoHeader.biHeight); 
		DebugMsg (DLLTEXT ("biPlanes        = %d"), bmInfoHeader.biPlanes);
		DebugMsg (DLLTEXT ("biBitCount      = %d"), bmInfoHeader.biBitCount);
		DebugMsg (DLLTEXT ("biCompression   = %d"), bmInfoHeader.biCompression); 
		DebugMsg (DLLTEXT ("biSizeImage     = %d"), bmInfoHeader.biSizeImage);
		DebugMsg (DLLTEXT ("biXPelsPerMeter = %d"), bmInfoHeader.biXPelsPerMeter);
		DebugMsg (DLLTEXT ("biYPelsPerMeter = %d"), bmInfoHeader.biYPelsPerMeter);
		DebugMsg (DLLTEXT ("biClrUsed       = %d"), bmInfoHeader.biClrUsed);
		DebugMsg (DLLTEXT ("biClrImportant  = %d"), bmInfoHeader.biClrImportant);
		*/

		::WriteFile (h, (LPSTR)&bmFileHeader, sizeof (BITMAPFILEHEADER), &dwWritten, NULL);
		::WriteFile (h, (LPSTR)&bmInfoHeader, sizeof (BITMAPINFOHEADER), &dwWritten, NULL);
		
		if (pOemPDEV->bColorTable)
			::WriteFile (h, pOemPDEV->prgbq, pOemPDEV->cPalColors * sizeof (ULONG), &dwWritten, NULL); 

		::WriteFile (h, (LPSTR)lpBits, bmInfoHeader.biSizeImage, &dwWritten, NULL);
		::CloseHandle (h);

		DebugMsg (DLLTEXT ("%s"), szFile); 
	}
}

int GetChannels (SOCKET s, POEMPDEV pOemPDEV, int nHeadNumber, double & dHeight)
{
	OEMDBG(DBG_VERBOSE, L"GetChannels entry.");
	_FJ_SOCKET_MESSAGE reply;
	WCHAR sz [128] = { 0 };
	int nChannels = -1;
	
	if (Send (s, pOemPDEV->m_lCmdID++, IPC_GET_PRINTER_PHY_INFO, _T (""), &reply, sz, ARRAYSIZE (sz))) {
		int n = ReadINT (sz, _T ("PHY_CHANNELS"), -1);

		if (n != -1) 
			nChannels = n;
		else
			DebugMsg (DLLTEXT ("not found: PHY_CHANNELS: %s"), sz);
	}

	if (nChannels == -1) {
		if (Send (s, pOemPDEV->m_lCmdID++, IPC_GET_PH_TYPE, _T (""), &reply, sz, ARRAYSIZE (sz))) {
			int nType = ReadINT (sz, _T ("HeadType"), -1);

			switch (nType) {
			case 0: //UJ2_352_32
			case 2: //UJ2_96_32						
			case 1: //UJ2_192_32
			case 6: //ALPHA_CODER
			case 5: //UJ2_192_32NP
				nChannels	= 32;
				break;
			case 4: //GRAPHICS_384_128
				nChannels	= 128;
				break;
			case 3: //GRAPHICS_768_256
				nChannels	= 256;
				break;
			default: // for old NET (no HeadType param)
				{
					WCHAR szKey [_MAX_PATH], szUID [32] = { 0 };

					StringCbPrintf (szKey, ARRAYSIZE (szKey), _T ("SOFTWARE\\FoxJet_Driver\\Head%d"), nHeadNumber);
					Foxjet::GetProfileString (szKey, _T ("UID"), szUID, ARRAYSIZE (szUID), _T (""));

					if (!wcscmp (szUID, pOemPDEV->szPort)) {
						nType = (int)Foxjet::GetProfileDWORD (szKey, _T ("HeadType"), FoxjetDatabase::GRAPHICS_768_256);
						
						switch (nType) {
						case FoxjetDatabase::GRAPHICS_384_128:	nChannels = 128;	break;
						case FoxjetDatabase::GRAPHICS_768_256:	nChannels = 256;	break;
						default:								nChannels = 32;		break;
						}
					}
				}
			}

			DebugMsg (DLLTEXT ("HeadType: %d"), nType);
		}
	}

	switch (nChannels) {
	default:	dHeight		= 1.813;	break;
	case 128:	dHeight		= 2.000;	break;
	case 256:	dHeight		= 4.000;	break;
	}

	DebugMsg (DLLTEXT ("nChannels: %d, dHeight: %.03f"), nChannels, dHeight);

	return nChannels;
}

WCHAR * ReadString (WCHAR * lpsz, WCHAR * lpszKeyword, WCHAR * lpszValue, ULONG lLen)
{
	OEMDBG(DBG_VERBOSE, L"ReadString entry.");
	WCHAR sz [128] = { 0 };

	StringCbPrintf (sz, ARRAYSIZE (sz), _T ("%s="), lpszKeyword); 

	if (WCHAR * lpszFind = wcsstr (lpsz, sz)) {
		ULONG i;

		for (i = 0; i < (lLen - 1); i++) {
			WCHAR c = lpszFind [i + wcslen (sz)];

			switch (c) {
			case '\0':
			case ';':
				return lpszValue;
			}

			lpszValue [i] = c;
		}

		lpszValue [i] = '\0';
	}

	return NULL;
}

int ReadINT (WCHAR * lpsz, WCHAR * lpszKeyword, int nDef)
{
	OEMDBG(DBG_VERBOSE, L"ReadINT entry.");
	WCHAR sz [128] = { 0 };
	int n = nDef;

	StringCbPrintf (sz, ARRAYSIZE (sz), _T ("%s="), lpszKeyword); 

	if (WCHAR * p = wcsstr (lpsz, lpszKeyword)) {
		StringCbPrintf (sz, ARRAYSIZE (sz), _T ("%s=%%d"), lpszKeyword); 

		swscanf (p, sz, &n);
	}

	return n;
}

void ntohl (_FJ_SOCKET_MESSAGE & cmd)
{
	cmd.Header.Cmd		= ntohl (cmd.Header.Cmd);
	cmd.Header.BatchID	= ntohl (cmd.Header.BatchID);
	cmd.Header.Length	= ntohl (cmd.Header.Length);
}

bool IsSocketDataPending (SOCKET s)
{
	struct timeval timeout;
	fd_set read;

	memset (&read, 0, sizeof (read));
	read.fd_count = 1;
	read.fd_array [0] = s;

	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

	int nSelect = ::select (0, &read, NULL, NULL, &timeout);

	return (nSelect != SOCKET_ERROR && nSelect > 0) ? true : false;
}


void DebugIpcCmd (const _FJ_SOCKET_MESSAGE & cmd, WCHAR * lpsz, WCHAR * lpszFile, ULONG lLine)
{
	OEMDBG(DBG_VERBOSE, L"DebugIpcCmd entry.");
	WCHAR sz [_FJ_SOCKET_BUFFER_SIZE] = { 0 };

	UNREFERENCED_PARAMETER (lpszFile);

	if (cmd.Header.Length && (cmd.Header.Length < ARRAYSIZE (sz)))
		a2w ((const char *)cmd.Data, cmd.Header.Length, sz, ARRAYSIZE (sz));

	DebugMsg (DLLTEXT ("(%-03d): %s[%-03d, %-05d, %-06d] (%s) %s"), 
		lLine,
		lpsz,
		cmd.Header.Cmd, 
		cmd.Header.BatchID, 
		cmd.Header.Length,
		GetIpcCmdName (cmd.Header.Cmd),
		sz);
}

bool Send (SOCKET s, ULONG lBatchID, ULONG lCmd, WCHAR * lpsz, _FJ_SOCKET_MESSAGE * pReply, WCHAR * lpszReply, ULONG lReplyLen)
{
	OEMDBG(DBG_VERBOSE, L"Send entry.");
	char sz [_FJ_SOCKET_BUFFER_SIZE] = { 0 };
	const ULONG lLen = lpsz == NULL ? 0 : (ULONG)wcslen (lpsz);

	if ((!pReply && lpszReply) || (!pReply && lReplyLen))
		DebugMsg (DLLTEXT ("can't fill in lpszReply without pReply"));

	if (lpsz && lLen) 
		w2a (lpsz, lLen, sz, ARRAYSIZE (sz));

	if (SendBinary (s, lBatchID, lCmd, (LPBYTE)sz, lLen, pReply)) {
		if (pReply && lpszReply && lReplyLen) 
			a2w ((char *)pReply->Data, pReply->Header.Length, lpszReply, lReplyLen);

		if (lpsz && lLen) 
			DebugMsg (DLLTEXT ("Send:                                      %s"), lpsz);

		return true;
	}

	return false;
}

bool SendBinary (SOCKET s, ULONG lBatchID, ULONG lCmd, LPBYTE lpData, ULONG lLen, _FJ_SOCKET_MESSAGE * pReply)
{
	OEMDBG(DBG_VERBOSE, L"Send entry.");
	bool bSent = false;
	const DWORD dwTimeout = BindTo <DWORD> (Foxjet::GetProfileDWORD (::lpszRootSection, _T ("IpcTimeout"), 1000 * 10), 1000, 1000 * 60); 
	const double dTimeout = (double)dwTimeout / 1000.0;
	struct
	{
		ULONG m_lCmd;
		ULONG m_lReply;
	}
	static const synonym [] = 
	{
		{ IPC_EDIT_START,			IPC_EDIT_STATUS },
		{ IPC_EDIT_SAVE,			IPC_EDIT_STATUS },
		{ IPC_GET_SELECTED_COUNT,	IPC_STATUS		},
	};

	if (s != INVALID_SOCKET) {
		DWORD dwTotalLen = sizeof (_FJ_SOCKET_MESSAGE_HEADER) + lLen;

		if (HGLOBAL hData = GlobalAlloc (GPTR, dwTotalLen)) {
			if (_FJ_SOCKET_MESSAGE * p = (_FJ_SOCKET_MESSAGE *)GlobalLock (hData)) {
				memset (p, 0, dwTotalLen);
				p->Header.Cmd		= htonl (lCmd);
				p->Header.BatchID	= htonl (lBatchID);
				p->Header.Length	= htonl (lLen);
				memcpy (p->Data, lpData, lLen);

				while (IsSocketDataPending (s)) { // clear any pending data
					_FJ_SOCKET_MESSAGE reply;

					memset (&reply, 0, sizeof (reply));
					recv (s, (char *)&reply, sizeof (reply), 0);
					ntohl (reply);
					//DebugMsg (DLLTEXT ("recv: %d"), nRecv);
					DEBUGIPC (reply, _T ("              "));
				}

				DWORD dwSend = send (s, (const char *)p, dwTotalLen, 0);
				bSent = dwSend == dwTotalLen ? true : false;
				DebugMsg (DLLTEXT ("send                 [%-03d, %-05d, %-06d] (%s)%s"), lCmd, lBatchID, lLen, GetIpcCmdName (lCmd), bSent ? _T ("") : _T (" FAILED"));

				if (pReply) { 
					clock_t clock_tStart = clock ();
					double dDiff = 0;

					do {
						dDiff = (double)(clock () - clock_tStart) / CLOCKS_PER_SEC;

						if (IsSocketDataPending (s)) {
							memset (pReply, 0, sizeof (_FJ_SOCKET_MESSAGE));
							::Sleep (::dwSleep);

							if (recv (s, (char *)&pReply->Header, sizeof (pReply->Header), 0) == sizeof (_FJ_SOCKET_MESSAGE_HEADER)) {
								ntohl (* pReply);

								bool bMatched = pReply->Header.BatchID == lBatchID ? true : false;

								for (int i = 0; !bMatched && (i < ARRAYSIZE (synonym)); i++) 
									if (lCmd == synonym [i].m_lCmd) 
										bMatched = (pReply->Header.Cmd == synonym [i].m_lReply) ? true : false;

								if (pReply->Header.Length) {
									::recv (s, (char *)&pReply->Data, pReply->Header.Length, 0);

									//if (bMatched) 
										DEBUGIPC (* pReply, bMatched ? _T ("[MATCHED    ] ") : _T ("[not matched] "));
								}

								if (bMatched) {
									//::Sleep (::dwSleep * 4);
									return bSent;
								}
							}
						}
					}
					while (dDiff < dTimeout);

					if (dDiff >= dTimeout) 
						DebugMsg (DLLTEXT ("Send: timed out waiting for reply [%.03fs (%.03fs)]"), dTimeout, dDiff);
				}
			}

			GlobalUnlock (hData);
			GlobalFree (hData);
		}
	}
	
	//::Sleep (::dwSleep * 4);
	return bSent;
}

//////////////////////////////////////////////////////////////////////////////////////////

struct 
{
	DWORD	m_dw;
	WCHAR * m_lpsz;
} static const map [] =
{
	{ IPC_COMPLETE,						_T ("IPC_COMPLETE"),					},
	{ IPC_CONNECTION_CLOSE,				_T ("IPC_CONNECTION_CLOSE"),			},
	{ IPC_INVALID,						_T ("IPC_INVALID"),						},
	{ IPC_ECHO,							_T ("IPC_ECHO"),						},
	{ IPC_STATUS,						_T ("IPC_STATUS"),						},
	{ IPC_GET_STATUS,					_T ("IPC_GET_STATUS"),					},
	{ IPC_PHOTO_TRIGGER,				_T ("IPC_PHOTO_TRIGGER"),				},
	{ IPC_EDIT_STATUS,					_T ("IPC_EDIT_STATUS"),					},
	{ IPC_GET_EDIT_STATUS,				_T ("IPC_GET_EDIT_STATUS"),				},
	{ IPC_EDIT_START,					_T ("IPC_EDIT_START"),					},
	{ IPC_EDIT_SAVE,					_T ("IPC_EDIT_SAVE"),					},
	{ IPC_EDIT_CANCEL,					_T ("IPC_EDIT_CANCEL"),					},
	{ IPC_IMAGE_HEADER,					_T ("IPC_IMAGE_HEADER"),				},
	{ IPC_IMAGE_DATA,					_T ("IPC_IMAGE_DATA"),					},
	{ IPC_GET_LABEL_IDS,				_T ("IPC_GET_LABEL_IDS"),				},			
	{ IPC_SET_LABEL_IDS,				_T ("IPC_SET_LABEL_IDS"),				},			
	{ IPC_GET_LABELS,					_T ("IPC_GET_LABELS"),					},			
	{ IPC_GET_LABEL,					_T ("IPC_GET_LABEL"),					},			
	{ IPC_PUT_LABEL,					_T ("IPC_PUT_LABEL"),					},			
	{ IPC_DELETE_LABEL,					_T ("IPC_DELETE_LABEL"),				},			
	{ IPC_SELECT_LABEL,					_T ("IPC_SELECT_LABEL"),				},			
	{ IPC_GET_LABEL_ID_SELECTED,		_T ("IPC_GET_LABEL_ID_SELECTED"),		},			
	{ IPC_SET_LABEL_ID_SELECTED,		_T ("IPC_SET_LABEL_ID_SELECTED"),		},			
	{ IPC_DELETE_ALL_LABELS,			_T ("IPC_DELETE_ALL_LABELS"),			},			
	{ IPC_GET_MSG_IDS,					_T ("IPC_GET_MSG_IDS"),					},			
	{ IPC_SET_MSG_IDS,					_T ("IPC_SET_MSG_IDS"),					},			
	{ IPC_GET_MSGS,						_T ("IPC_GET_MSGS"),					},			
	{ IPC_GET_MSG,						_T ("IPC_GET_MSG"),						},			
	{ IPC_PUT_MSG,						_T ("IPC_PUT_MSG"),						},			
	{ IPC_DELETE_MSG,					_T ("IPC_DELETE_MSG"),					},			
	{ IPC_GET_TEXT_IDS,					_T ("IPC_GET_TEXT_IDS"),				},			
	{ IPC_SET_TEXT_IDS,					_T ("IPC_SET_TEXT_IDS"),				},			
	{ IPC_GET_TEXT_ELEMENTS,			_T ("IPC_GET_TEXT_ELEMENTS"),			},			
	{ IPC_GET_TEXT,						_T ("IPC_GET_TEXT"),					},			
	{ IPC_PUT_TEXT,						_T ("IPC_PUT_TEXT"),					},			
	{ IPC_DELETE_TEXT,					_T ("IPC_DELETE_TEXT"),					},			
	{ IPC_GET_BITMAP_IDS,				_T ("IPC_GET_BITMAP_IDS"),				},			
	{ IPC_SET_BITMAP_IDS,				_T ("IPC_SET_BITMAP_IDS"),				},			
	{ IPC_GET_BITMAP_ELEMENTS,			_T ("IPC_GET_BITMAP_ELEMENTS"),			},			
	{ IPC_GET_BITMAP,					_T ("IPC_GET_BITMAP"),					},			
	{ IPC_PUT_BITMAP,					_T ("IPC_PUT_BITMAP"),					},			
	{ IPC_DELETE_BITMAP,				_T ("IPC_DELETE_BITMAP"),				},			
	{ IPC_GET_DYNIMAGE_IDS,				_T ("IPC_GET_DYNIMAGE_IDS"),			},			
	{ IPC_SET_DYNIMAGE_IDS,				_T ("IPC_SET_DYNIMAGE_IDS"),			},			
	{ IPC_GET_DYNIMAGE_ELEMENTS,		_T ("IPC_GET_DYNIMAGE_ELEMENTS"),		},			
	{ IPC_GET_DYNIMAGE,					_T ("IPC_GET_DYNIMAGE"),				},			
	{ IPC_PUT_DYNIMAGE,					_T ("IPC_PUT_DYNIMAGE"),				},			
	{ IPC_DELETE_DYNIMAGE,				_T ("IPC_DELETE_DYNIMAGE"),				},			
	{ IPC_PUT_DYNIMAGE_DATA,			_T ("IPC_PUT_DYNIMAGE_DATA"),			},			
	{ IPC_GET_DATETIME_IDS,				_T ("IPC_GET_DATETIME_IDS"),			},			
	{ IPC_SET_DATETIME_IDS,				_T ("IPC_SET_DATETIME_IDS"),			},			
	{ IPC_GET_DATETIME_ELEMENTS,		_T ("IPC_GET_DATETIME_ELEMENTS"),		},			
	{ IPC_GET_DATETIME,					_T ("IPC_GET_DATETIME"),				},			
	{ IPC_PUT_DATETIME,					_T ("IPC_PUT_DATETIME"),				},			 
	{ IPC_DELETE_DATETIME,				_T ("IPC_DELETE_DATETIME"),				},			
	{ IPC_GET_COUNTER_IDS,				_T ("IPC_GET_COUNTER_IDS"),				},			
	{ IPC_SET_COUNTER_IDS,				_T ("IPC_SET_COUNTER_IDS"),				},			
	{ IPC_GET_COUNTER_ELEMENTS,			_T ("IPC_GET_COUNTER_ELEMENTS"),		},			
	{ IPC_GET_COUNTER,					_T ("IPC_GET_COUNTER"),					},			
	{ IPC_PUT_COUNTER,					_T ("IPC_PUT_COUNTER"),					},			
	{ IPC_DELETE_COUNTER,				_T ("IPC_DELETE_COUNTER"),				},			
	{ IPC_GET_BARCODE_IDS,				_T ("IPC_GET_BARCODE_IDS"),				},			
	{ IPC_SET_BARCODE_IDS,				_T ("IPC_SET_BARCODE_IDS"),				},			
	{ IPC_GET_BARCODE_ELEMENTS,			_T ("IPC_GET_BARCODE_ELEMENTS"),		},			
	{ IPC_GET_BARCODE,					_T ("IPC_GET_BARCODE"),					},			
	{ IPC_PUT_BARCODE,					_T ("IPC_PUT_BARCODE"),					},			
	{ IPC_DELETE_BARCODE,				_T ("IPC_DELETE_BARCODE"),				},			
	{ IPC_GET_FONT_IDS,					_T ("IPC_GET_FONT_IDS"),				},			
	{ IPC_SET_FONT_IDS,					_T ("IPC_SET_FONT_IDS"),				},			
	{ IPC_GET_FONTS,					_T ("IPC_GET_FONTS"),					},			
	{ IPC_GET_FONT,						_T ("IPC_GET_FONT"),					},			
	{ IPC_PUT_FONT,						_T ("IPC_PUT_FONT"),					},			
	{ IPC_DELETE_FONT,					_T ("IPC_DELETE_FONT"),					},			
	{ IPC_GET_BMP_IDS,					_T ("IPC_GET_BMP_IDS"),					},			
	{ IPC_SET_BMP_IDS,					_T ("IPC_SET_BMP_IDS"),					},			
	{ IPC_GET_BMPS,						_T ("IPC_GET_BMPS"),					},			
	{ IPC_GET_BMP,						_T ("IPC_GET_BMP"),						},			
	{ IPC_PUT_BMP,						_T ("IPC_PUT_BMP"),						},			
	{ IPC_DELETE_BMP,					_T ("IPC_DELETE_BMP"),					},			
	{ IPC_DELETE_ALL_BMPS,				_T ("IPC_DELETE_ALL_BMPS"),				},			
	{ IPC_GET_DYNTEXT_IDS,				_T ("IPC_GET_DYNTEXT_IDS"),				},			
	{ IPC_SET_DYNTEXT_IDS,				_T ("IPC_SET_DYNTEXT_IDS"),				},			
	{ IPC_GET_DYNTEXT_ELEMENTS,			_T ("IPC_GET_DYNTEXT_ELEMENTS"),		},			
	{ IPC_GET_DYNTEXT,					_T ("IPC_GET_DYNTEXT"),					},			
	{ IPC_PUT_DYNTEXT,					_T ("IPC_PUT_DYNTEXT"),					},			
	{ IPC_DELETE_DYNTEXT,				_T ("IPC_DELETE_DYNTEXT"),				},			
	{ IPC_GET_DYNBARCODE_IDS,			_T ("IPC_GET_DYNBARCODE_IDS"),			},			
	{ IPC_SET_DYNBARCODE_IDS,			_T ("IPC_SET_DYNBARCODE_IDS"),			},			
	{ IPC_GET_DYNBARCODE_ELEMENTS,		_T ("IPC_GET_DYNBARCODE_ELEMENTS"),		},			
	{ IPC_GET_DYNBARCODE,				_T ("IPC_GET_DYNBARCODE"),				},			
	{ IPC_PUT_DYNBARCODE,				_T ("IPC_PUT_DYNBARCODE"),				},			
	{ IPC_DELETE_DYNBARCODE,			_T ("IPC_DELETE_DYNBARCODE"),			},			
	{ IPC_GET_GROUP_INFO,				_T ("IPC_GET_GROUP_INFO"),				},			
	{ IPC_SET_GROUP_INFO,				_T ("IPC_SET_GROUP_INFO"),				},			
	{ IPC_GET_SYSTEM_INFO,				_T ("IPC_GET_SYSTEM_INFO"),				},			
	{ IPC_GET_SYSTEM_INTERNET_INFO,		_T ("IPC_GET_SYSTEM_INTERNET_INFO"),	},			
	{ IPC_GET_SYSTEM_TIME_INFO,			_T ("IPC_GET_SYSTEM_TIME_INFO"),		},			
	{ IPC_GET_SYSTEM_DATE_STRINGS,		_T ("IPC_GET_SYSTEM_DATE_STRINGS"),		},			
	{ IPC_GET_SYSTEM_PASSWORDS,			_T ("IPC_GET_SYSTEM_PASSWORDS"),		},			
	{ IPC_GET_SYSTEM_BARCODES,			_T ("IPC_GET_SYSTEM_BARCODES"),			},			
	{ IPC_SET_SYSTEM_INFO,				_T ("IPC_SET_SYSTEM_INFO"),				},			
	{ IPC_GET_PRINTER_ID,				_T ("IPC_GET_PRINTER_ID"),				},			
	{ IPC_GET_PRINTER_INFO,				_T ("IPC_GET_PRINTER_INFO"),			},			
	{ IPC_GET_PRINTER_PKG_INFO,			_T ("IPC_GET_PRINTER_PKG_INFO"),		},			
	{ IPC_GET_PRINTER_PHY_INFO,			_T ("IPC_GET_PRINTER_PHY_INFO"),		},			
	{ IPC_GET_PRINTER_OP_INFO,			_T ("IPC_GET_PRINTER_OP_INFO"),			},			
	{ IPC_SET_PRINTER_INFO,				_T ("IPC_SET_PRINTER_INFO"),			},			
	{ IPC_GET_PRINTER_ELEMENTS,			_T ("IPC_GET_PRINTER_ELEMENTS"),		},			
	{ IPC_PUT_PRINTER_ELEMENT,			_T ("IPC_PUT_PRINTER_ELEMENT"),			},			
	{ IPC_DELETE_ALL_PRINTER_ELEMENTS,	_T ("IPC_DELETE_ALL_PRINTER_ELEMENTS"),	},			
	{ IPC_FIRMWARE_UPGRADE,				_T ("IPC_FIRMWARE_UPGRADE"),			},			
	{ IPC_SET_PH_TYPE,					_T ("IPC_SET_PH_TYPE"),					},			
	{ IPC_SET_PRINTER_MODE,				_T ("IPC_SET_PRINTER_MODE"),			},			
	{ IPC_GET_PRINTER_MODE,				_T ("IPC_GET_PRINTER_MODE"),			},			
	{ IPC_SET_SELECTED_COUNT,			_T ("IPC_SET_SELECTED_COUNT"),			},			
	{ IPC_GET_SELECTED_COUNT,			_T ("IPC_GET_SELECTED_COUNT"),			},			
	{ IPC_SELECT_VARDATA_ID,			_T ("IPC_SELECT_VARDATA_ID"),			},			
	{ IPC_GET_VARDATA_ID,				_T ("IPC_GET_VARDATA_ID"),				},			
	{ IPC_SET_VARDATA_ID,				_T ("IPC_SET_VARDATA_ID"),				},			
	{ IPC_SAVE_ALL_PARAMS,				_T ("IPC_SAVE_ALL_PARAMS"),				},			
	{ IPC_GET_SW_VER_INFO,				_T ("IPC_GET_SW_VER_INFO"),				},			
	{ IPC_GET_GA_VER_INFO,				_T ("IPC_GET_GA_VER_INFO"),				},
	{ IPC_GA_UPGRADE,					_T ("IPC_GA_UPGRADE"),					},
	{ IPC_SET_STATUS_MODE,				_T ("IPC_SET_STATUS_MODE"),				},
	{ IPC_GET_STATUS_MODE,				_T ("IPC_GET_STATUS_MODE"),				},
	{ IPC_GET_DISK_USAGE,				_T ("IPC_GET_DISK_USAGE"),				},			
	{ IPC_GET_PH_TYPE,					_T ("IPC_GET_PH_TYPE"),					},			
	{ IPC_PUT_BMP_DRIVER,				_T ("IPC_PUT_BMP_DRIVER"),				},
};

WCHAR * GetIpcCmdName (DWORD dw)
{
	static WCHAR * lpszDef = _T ("[not found]");

	for (int i = 0; i < ARRAYSIZE (::map); i++)
		if (::map [i].m_dw == dw)
			return ::map [i].m_lpsz;

	return lpszDef;
}

DWORD GetIpcCmd (const WCHAR * lpsz)
{
	for (int i = 0; i < ARRAYSIZE (::map); i++)
		if (!wcscmp (lpsz, ::map [i].m_lpsz))
			return ::map [i].m_dw;

	return map [0].m_dw;
}


bool fj_bmpCheckFormat( LPBYTE pbmp )
{
	OEMDBG(DBG_VERBOSE, L"fj_bmpCheckFormat entry.");
	BITMAPFILEHEADER *pbmfh;
	BITMAPINFOHEADER *pbmih;
	LONG lLength;						// size of buffer in bytes
	LONG lOffset;						// bmp offset to rows of bits
	LONG lHeight;						// bmp height in bits
	LONG lWidth;						// bmp width in bits
	LONG lHeightBytes;					// bmp height in bytes
	LONG lWidthBytes;					// bmp width in bytes
	LONG lLengthCalc;					// calculated length in bytes
	bool bRet = false;

	if ( NULL != pbmp )
	{
		pbmfh = (BITMAPFILEHEADER *)pbmp;
		pbmih = (BITMAPINFOHEADER *)(pbmp+sizeof(BITMAPFILEHEADER));
										// bmp magic number
		if ( ('B' == *pbmp) && ('M' == *(pbmp+1)) )
		{
			lLength = LOADREVERSEWORD32(&pbmfh->bfSize);
			lOffset = LOADREVERSEWORD32(&pbmfh->bfOffBits);
			// Windows bmp header length
			if ( sizeof(BITMAPINFOHEADER) == LOADREVERSEWORD32(&pbmih->biSize) )
			{
				if ( (1 == LOADREVERSEWORD16(&pbmih->biPlanes)) && (1 == LOADREVERSEWORD16(&pbmih->biBitCount)) && (0 == LOADREVERSEWORD32(&pbmih->biCompression)) )
				{
					lHeight = LOADREVERSEWORD32(&pbmih->biHeight);
					lWidth  = LOADREVERSEWORD32(&pbmih->biWidth);
					lHeightBytes =  (lHeight+ 7)/8;
					lWidthBytes  = ((lWidth +31)/32)*4;
					lLengthCalc = lOffset + (lHeight * lWidthBytes);
										// header plus rowdata must equal size

					if ( lLength == lLengthCalc )
					{
						bRet = true;
					}
					// actual size might be rounded up to a multiple of 4
					else if ( (lLength > lLengthCalc) && (lLength <= (lLengthCalc+4)))
					{
						bRet = true;
					}
					else 
						DebugMsg (DLLTEXT ("fj_bmpCheckFormat: FAILED: lLength: %d, lLengthCalc: %d"), lLength, lLengthCalc);
				}
				else
					DebugMsg (DLLTEXT ("fj_bmpCheckFormat: FAILED: pbmih->biPlanes or pbmih->biBitCount or pbmih->biCompression"));
			}
			else
				DebugMsg (DLLTEXT ("fj_bmpCheckFormat: sizeof(BITMAPINFOHEADER) != LOADREVERSEWORD32(&pbmih->biSize)"));
		}
		else
			DebugMsg (DLLTEXT ("fj_bmpCheckFormat: 'BM' failed"));
	}
	else
		DebugMsg (DLLTEXT ("fj_bmpCheckFormat: pbmp == NULL"));

	return( bRet );
}

int Round (double d) 
{
	double dFloor = floor (d);
	int n = (int)((d - dFloor) * 100.0);

	return (int)dFloor + (n >= 50 ? 1 : 0);
}

WCHAR * FormatMessage (DWORD dwError)
{
	LPVOID lpMsgBuf = NULL;
	static WCHAR sz [1024] = { 0 };

	::FormatMessage (
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, dwError, MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT), 
		(LPTSTR)&lpMsgBuf, 0, NULL);
	StringCbPrintf (sz, ARRAYSIZE (sz), _T ("[%d: 0x%p]: %s"), dwError, dwError, lpMsgBuf);
	::LocalFree( lpMsgBuf );

	return sz;
}

bool Foxjet::WriteProfileDWORD (LPCWSTR lpszSection, LPCWSTR lpszKey, DWORD dw)
{
	HKEY hKey;
	DWORD dwType = REG_DWORD, dwSize = sizeof (DWORD);
	bool bResult = false;

	if (::RegCreateKey (HKEY_CURRENT_USER, lpszSection, &hKey) == ERROR_SUCCESS) {
		bResult = ::RegSetValueEx (hKey, lpszKey, NULL, dwType, (LPBYTE)&dw, dwSize) == ERROR_SUCCESS;
		::RegCloseKey (hKey);
	}

	return bResult;
}

DWORD Foxjet::GetProfileDWORD (LPCWSTR lpszSection, LPCWSTR lpszKey, DWORD dwDefault)
{
	HKEY hKey;
	DWORD dwType = REG_DWORD, dwSize = sizeof (DWORD), dwData = dwDefault;

	if (::RegCreateKey (HKEY_CURRENT_USER, lpszSection, &hKey) == ERROR_SUCCESS) {
		::RegQueryValueEx (hKey, lpszKey, NULL, &dwType, (LPBYTE)&dwData, &dwSize);
		::RegCloseKey (hKey);
	}

	return dwData;
}

bool Foxjet::GetProfileString (LPCWSTR lpszSection, LPCWSTR lpszKey, LPWSTR lpsz, ULONG lLen, LPWSTR lpszDef)
{
	HKEY hKey;
	DWORD dwType = REG_SZ, dwSize = lLen;
	bool bResult = false;

	if (::RegCreateKey (HKEY_CURRENT_USER, lpszSection, &hKey) == ERROR_SUCCESS) {
		bResult = ::RegQueryValueEx (hKey, lpszKey, NULL, &dwType, (LPBYTE)lpsz, &dwSize) == ERROR_SUCCESS ? true : false;
		::RegCloseKey (hKey);
	}

	if (!bResult)
		StringCbPrintf (lpsz, lLen, _T ("%s"), lpszDef);

	return bResult;
}

BOOL APIENTRY OEMStartDoc (SURFOBJ * pso, PWSTR pwszDocName, DWORD dwJobId)
{
    OEMDBG(DBG_VERBOSE, L"OEMStartDoc entry.");
    
    PDEVOBJ pDevObj = (PDEVOBJ)pso->dhpdev;
    POEMPDEV pOemPDEV = (POEMPDEV)pDevObj->pdevOEM;

	UNREFERENCED_PARAMETER(pso);
//	UNREFERENCED_PARAMETER(dwJobId);
//	UNREFERENCED_PARAMETER(pwszDocName);

	DebugMsg (DLLTEXT ("OEMStartDoc: %s [%d]"), pwszDocName, dwJobId); 
	StringCbCopy (pOemPDEV->szDocName, ARRAYSIZE (pOemPDEV->szDocName), pwszDocName);
	DebugMsg (DLLTEXT ("pOemPDEV->szDocName: %s"), pOemPDEV->szDocName); 

	return TRUE;
}

/*
BOOL APIENTRY OEMStartPage (SURFOBJ * pso)
{
    PDEVOBJ pDevObj = (PDEVOBJ)pso->dhpdev;
    POEMPDEV pOemPDEV = (POEMPDEV)pDevObj->pdevOEM;

    OEMDBG(DBG_VERBOSE, L"OEMStartPage entry.");
	DebugMsg (DLLTEXT ("OEMStartPage")); 

    return (pOemPDEV->m_pfnDrvStartPage) (pso);
}
*/

void TraceRegValue (PDEVOBJ pDevObj, LPCTSTR lpszValue)
{
    OEMDBG(DBG_VERBOSE, L"TraceRegValue entry.");

	DWORD dwNeeded = 0;
	DWORD dwType = REG_MULTI_SZ;
	WCHAR sz [128] = { 0 };

	GetPrinterDataEx (pDevObj->hPrinter, SPLDS_SPOOLER_KEY, lpszValue, &dwType, (LPBYTE)(LPVOID)&sz [0], ARRAYSIZE (sz), &dwNeeded);
	DebugMsg (DLLTEXT ("%s:   %s"), lpszValue, sz); 
}

BOOL APIENTRY
OEMEndDoc(
    SURFOBJ     *pso,
    FLONG       fl
    )

/*++

Routine Description:

    Implementation of DDI hook for DrvEndDoc.

    DrvEndDoc is called by GDI when it has finished 
    sending a document to the driver for rendering.
    
    Please refer to DDK documentation for more details.

    This particular implementation of OEMEndDoc performs
    the following operations:
    - Dump the bitmap file header
    - Dump the bitmap info header
    - Dump the color table if one exists
    - Dump the buffered bitmap data
    - Free the memory for the data buffers

Arguments:

    pso - Defines the surface object
    flags - A set of flag bits

Return Value:

    TRUE if successful, FALSE if there is an error

--*/

{
    OEMDBG(DBG_VERBOSE, L"OEMEndDoc entry.");

    PDEVOBJ pDevObj = (PDEVOBJ)pso->dhpdev;
    POEMPDEV pOemPDEV = (POEMPDEV)pDevObj->pdevOEM;
    DWORD dwWritten;
    INT cScans;
	BOOL bDebug = TRUE;//Foxjet::GetProfileDWORD (::lpszRootSection, _T ("Debug"), 0) > 0;
	int nCopies = pDevObj->pPublicDM->dmCopies;
	char szPort [32] = { 0 };
	int nHeadNumber = -1;
	WCHAR szPrinterName [128] = { 0 };

	w2a (pOemPDEV->szPort, ARRAYSIZE (pOemPDEV->szPort), szPort, ARRAYSIZE (szPort));
	DebugMsg (DLLTEXT ("dmDeviceName: %s"), pDevObj->pPublicDM->dmDeviceName);
	DebugMsg (DLLTEXT ("port: %s"), pOemPDEV->szPort);

	/*
	TraceRegValue (pDevObj, SPLDS_ASSET_NUMBER                      );
	TraceRegValue (pDevObj, SPLDS_BYTES_PER_MINUTE                  );
	TraceRegValue (pDevObj, SPLDS_DESCRIPTION                       );
	TraceRegValue (pDevObj, SPLDS_DRIVER_NAME                       );
	TraceRegValue (pDevObj, SPLDS_DRIVER_VERSION                    );
	TraceRegValue (pDevObj, SPLDS_LOCATION                          );
	TraceRegValue (pDevObj, SPLDS_PORT_NAME                         );
	TraceRegValue (pDevObj, SPLDS_PRINT_ATTRIBUTES                  );
	TraceRegValue (pDevObj, SPLDS_PRINT_BIN_NAMES                   );
	TraceRegValue (pDevObj, SPLDS_PRINT_COLLATE                     );
	TraceRegValue (pDevObj, SPLDS_PRINT_COLOR                       );
	TraceRegValue (pDevObj, SPLDS_PRINT_DUPLEX_SUPPORTED            );
	TraceRegValue (pDevObj, SPLDS_PRINT_END_TIME                    );
	TraceRegValue (pDevObj, SPLDS_PRINTER_CLASS                     );
	TraceRegValue (pDevObj, SPLDS_PRINTER_NAME                      );
	TraceRegValue (pDevObj, SPLDS_PRINT_KEEP_PRINTED_JOBS           );
	TraceRegValue (pDevObj, SPLDS_PRINT_LANGUAGE                    );
	TraceRegValue (pDevObj, SPLDS_PRINT_MAC_ADDRESS                 );
	TraceRegValue (pDevObj, SPLDS_PRINT_MAX_X_EXTENT                );
	TraceRegValue (pDevObj, SPLDS_PRINT_MAX_Y_EXTENT                );
	TraceRegValue (pDevObj, SPLDS_PRINT_MAX_RESOLUTION_SUPPORTED    );
	TraceRegValue (pDevObj, SPLDS_PRINT_MEDIA_READY                 );
	TraceRegValue (pDevObj, SPLDS_PRINT_MEDIA_SUPPORTED             );
	TraceRegValue (pDevObj, SPLDS_PRINT_MEMORY                      );
	TraceRegValue (pDevObj, SPLDS_PRINT_MIN_X_EXTENT                );
	TraceRegValue (pDevObj, SPLDS_PRINT_MIN_Y_EXTENT                );
	TraceRegValue (pDevObj, SPLDS_PRINT_NETWORK_ADDRESS             );
	TraceRegValue (pDevObj, SPLDS_PRINT_NOTIFY                      );
	TraceRegValue (pDevObj, SPLDS_PRINT_NUMBER_UP                   );
	TraceRegValue (pDevObj, SPLDS_PRINT_ORIENTATIONS_SUPPORTED      );
	TraceRegValue (pDevObj, SPLDS_PRINT_OWNER                       );
	TraceRegValue (pDevObj, SPLDS_PRINT_PAGES_PER_MINUTE            );
	TraceRegValue (pDevObj, SPLDS_PRINT_RATE                        );
	TraceRegValue (pDevObj, SPLDS_PRINT_RATE_UNIT                   );
	TraceRegValue (pDevObj, SPLDS_PRINT_SEPARATOR_FILE              );
	TraceRegValue (pDevObj, SPLDS_PRINT_SHARE_NAME                  );
	TraceRegValue (pDevObj, SPLDS_PRINT_SPOOLING                    );
	TraceRegValue (pDevObj, SPLDS_PRINT_STAPLING_SUPPORTED          );
	TraceRegValue (pDevObj, SPLDS_PRINT_START_TIME                  );
	TraceRegValue (pDevObj, SPLDS_PRINT_STATUS                      );
	TraceRegValue (pDevObj, SPLDS_PRIORITY                          );
	TraceRegValue (pDevObj, SPLDS_SERVER_NAME                       );
	TraceRegValue (pDevObj, SPLDS_SHORT_SERVER_NAME                 );
	TraceRegValue (pDevObj, SPLDS_UNC_NAME                          );
	TraceRegValue (pDevObj, SPLDS_URL                               );
	TraceRegValue (pDevObj, SPLDS_FLAGS                             );
	TraceRegValue (pDevObj, SPLDS_VERSION_NUMBER                    );
	*/

	{
		DWORD dwNeeded = 0;
		DWORD dwType = REG_MULTI_SZ;

		GetPrinterDataEx (pDevObj->hPrinter, SPLDS_SPOOLER_KEY, SPLDS_PRINTER_NAME, &dwType, (LPBYTE)(LPVOID)&szPrinterName [0], ARRAYSIZE (szPrinterName), &dwNeeded);
		DebugMsg (DLLTEXT ("szPrinterName: %s"), szPrinterName); 
	}
	
	/*
	{
		STARTUPINFO si;     
		PROCESS_INFORMATION pi; 
		WCHAR szCmdLine [0xFF] = { 0 };

		StringCbPrintf (szCmdLine, ARRAYSIZE (szCmdLine), _T ("C:\\Temp\\Debug\\MessageBox\\Debug\\MessageBox.exe \"%s\""), pOemPDEV->szDocName);
		DebugMsg (DLLTEXT ("szCmdLine: %s"), szCmdLine);

		memset (&pi, 0, sizeof(pi));
		memset (&si, 0, sizeof(si));
		si.cb = sizeof(si);     
		si.dwFlags = STARTF_USESHOWWINDOW;     
		si.wShowWindow = SW_SHOW;     

		BOOL bCreateProcess = 0;//::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
		DebugMsg (DLLTEXT ("bCreateProcess: %d"), bCreateProcess);
	}
	*/

	for (int i = 1; (i <= MAX_HEADS) && (nHeadNumber == -1); i++) {
		WCHAR szKey [_MAX_PATH], szUID [32] = { 0 };

		StringCbPrintf (szKey, ARRAYSIZE (szKey), _T ("SOFTWARE\\FoxJet_Driver\\Head%d"), i);
		Foxjet::GetProfileString (szKey, _T ("UID"), szUID, ARRAYSIZE (szUID));

		if (!_wcsicmp (szUID, pOemPDEV->szPort)) 
			nHeadNumber = i;
	}

	DebugMsg (DLLTEXT ("nHeadNumber: %d"), nHeadNumber);

	{
		WCHAR szKey [_MAX_PATH] = { 0 };
		WCHAR sz [_MAX_PATH] = { 0 };

		StringCbPrintf (szKey, ARRAYSIZE (szKey), _T ("SOFTWARE\\FoxJet_Driver\\Head%d"), nHeadNumber);
		Foxjet::GetProfileString (szKey, _T ("spawn"), sz, ARRAYSIZE (sz));

		if (wcslen (sz) > 0) {
			DebugMsg (DLLTEXT ("***** SPAWN: %s ******"), sz);

			if (HDC hdc = ::CreateDC (_T ("winspool"), sz, _T ("Foxjet"), NULL)) {
				HDC hdcMem = ::CreateCompatibleDC (hdc);
				DOCINFO docinfo;

				memset(&docinfo, 0, sizeof(docinfo));
				docinfo.cbSize = sizeof(docinfo);
				docinfo.lpszDocName = pOemPDEV->szDocName;

				int nStartDoc = ::StartDoc (hdc, &docinfo);
				DebugMsg (DLLTEXT ("StartDoc: %d"), nStartDoc);

				int nStartPage = ::StartPage (hdc);
				DebugMsg (DLLTEXT ("StartPage: %d"), nStartPage);

				HBITMAP hbmp = ::CreateBitmap (
					pOemPDEV->bmInfoHeader.biWidth,
					pOemPDEV->bmInfoHeader.biHeight,
					pOemPDEV->bmInfoHeader.biPlanes,
					pOemPDEV->bmInfoHeader.biBitCount,
					(const LPVOID)pOemPDEV->pBufStart);

				HGDIOBJ hOld = ::SelectObject (hdcMem, hbmp);

				BOOL bBitBlt = ::BitBlt (hdc, 0, 0, pOemPDEV->bmInfoHeader.biWidth, pOemPDEV->bmInfoHeader.biHeight, hdcMem, 0, 0, SRCCOPY);
				DebugMsg (DLLTEXT ("BitBlt: %d"), bBitBlt);

				::SelectObject (hdcMem, hOld);
				::DeleteObject (hbmp);

				int nEndPage = ::EndPage (hdc);
				DebugMsg (DLLTEXT ("EndPage: %d"), nEndPage);

				int nEndDoc = ::EndDoc (hdc);
				DebugMsg (DLLTEXT ("EndDoc: %d"), nEndDoc);

				BOOL bDeleteDC = ::DeleteDC (hdc);
				DebugMsg (DLLTEXT ("DeleteDC: %d"), bDeleteDC);
				
				bDeleteDC = ::DeleteDC (hdcMem);
				DebugMsg (DLLTEXT ("DeleteDC: %d"), bDeleteDC);
			}
			else
				DebugMsg (DLLTEXT ("CreateDC failed"));
		}
	}


	/*
	if (!_tcscmp (szPrinterName, _T ("NEXT"))) {
		DebugMsg (DLLTEXT ("***** SPAWN ******"));

	}
	*/

	WSADATA wsaData;

	if (::WSAStartup (MAKEWORD (2,1), &wsaData) != 0) {
		DebugMsg (DLLTEXT ("WSAStartup failed: %s"), FormatMessage (WSAGetLastError ()));
	}

	SOCKET sockUDP = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP); 
	DebugMsg (DLLTEXT ("sockUDP: %d"), sockUDP); 
	
	{
		char nFlag = 1;
		setsockopt (sockUDP, SOL_SOCKET, SO_BROADCAST, &nFlag, sizeof (int));
		nFlag = 1;
		setsockopt (sockUDP, SOL_SOCKET, SO_REUSEADDR, &nFlag, sizeof (int));
	}

	DebugMsg (DLLTEXT ("bDebug: %d"), bDebug); 
	DebugMsg (DLLTEXT ("biBitCount: %d"), pOemPDEV->bmInfoHeader.biBitCount); 
	DebugMsg (DLLTEXT ("Port: %s"), pOemPDEV->szPort);
	DebugMsg (DLLTEXT ("dmCopies: %d"), nCopies);
	DebugMsg (DLLTEXT ("PRINTDRIVER_SOCKET_NUMBER: %d"), PRINTDRIVER_SOCKET_NUMBER);

	Broadcast (sockUDP, PRINTDRIVER_MSG_JOBSTART, nHeadNumber, nCopies);

    if (pOemPDEV->pBufStart && pOemPDEV->bmInfoHeader.biBitCount == 1) {
        // Fill BitmapFileHeader
        //
        DWORD dwTotalBytes = pOemPDEV->cbHeaderOffBits + pOemPDEV->bmInfoHeader.biSizeImage;        // File size
    
        pOemPDEV->bmFileHeader.bfType = 0x4d42;     // Signature = 'BM'
        pOemPDEV->bmFileHeader.bfSize = dwTotalBytes;  // Bytes in whole file.
        pOemPDEV->bmFileHeader.bfReserved1 = 0;
        pOemPDEV->bmFileHeader.bfReserved2 = 0;
        pOemPDEV->bmFileHeader.bfOffBits   = pOemPDEV->cbHeaderOffBits; // Offset to bits in file.

        if (pOemPDEV->bColorTable)
            pOemPDEV->bmFileHeader.bfOffBits += pOemPDEV->cPalColors * sizeof(ULONG);

        // Num of scanlines
        //
        cScans = pOemPDEV->bmInfoHeader.biHeight;

        // Flip the biHeight member so that it denotes top-down bitmap 
        //
        pOemPDEV->bmInfoHeader.biHeight = cScans * -1;

		//TraceImage (pOemPDEV->pBufStart, abs (pOemPDEV->bmInfoHeader.biWidth), abs (pOemPDEV->bmInfoHeader.biHeight));

		if (bDebug) {
			CreateTempDir ();
			SaveBitmap (pOemPDEV);
		}

		{
			SOCKADDR_IN target;

			target.sin_family = AF_INET; 
			target.sin_port = htons (_FJ_SOCKET_NUMBER); 
			target.sin_addr.s_addr = inet_addr (szPort);

			SOCKET s = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP); 

			if (connect(s, (SOCKADDR *)&target, sizeof(target)) != SOCKET_ERROR) {
				BITMAPFILEHEADER bmFileHeader; 
				BITMAPINFOHEADER bmInfoHeader; 

				DebugMsg (DLLTEXT ("connected to: %s"), pOemPDEV->szPort);

				memset (&bmFileHeader, 0, sizeof (bmFileHeader));
				memset (&bmInfoHeader, 0, sizeof (bmInfoHeader));

				int iOption = 1;
				ULONG lRet;
				lRet = setsockopt (s, SOL_SOCKET, SO_KEEPALIVE, (char*)&iOption, sizeof (iOption));
				lRet = setsockopt (s, SOL_SOCKET, SO_REUSEADDR, (char*)&iOption, sizeof (iOption));
				//lRet = setsockopt (s, SOL_SOCKET, SO_NBIO,      (char*)&iOption, sizeof (iOption));

				int nMajor, nMinor;
				GetVersionNumber (s, pOemPDEV, nMajor, nMinor);
				DebugMsg (DLLTEXT ("%d.%04d"), nMajor, nMinor); 
				const bool bDriver = (nMajor >= 3 && nMinor >= 5002) ? true : false;
				const bool bEditSession = bDriver ? true : BeginEditSession (s, pOemPDEV);

				Broadcast (sockUDP, PRINTDRIVER_MSG_BEGINPRINT, nHeadNumber, nCopies);

				if (bEditSession) {
					if (bDriver) {
						WCHAR sz [128];

						StringCbPrintf (sz, ARRAYSIZE (sz), _T ("PRINT_DRIVER_COUNT=%d;"), nCopies);
						DebugMsg (DLLTEXT ("%s"), sz);
						Send (s, pOemPDEV->m_lCmdID++, IPC_SET_PRINTER_MODE, sz);
					}
					else {
						Send (s, pOemPDEV->m_lCmdID++, IPC_SET_STATUS_MODE, _T ("STATUS_MODE=OFF;"));
						Send (s, pOemPDEV->m_lCmdID++, IPC_SET_PRINTER_MODE, _T ("PRINT_MODE=STOP;"));
					}

					double dHeight = 1.813;
					int nChannels = GetChannels (s, pOemPDEV, nHeadNumber, dHeight);

					Slice (bmFileHeader, bmInfoHeader, pOemPDEV, nChannels);
					
					bool bSelectLabel = SendSlice (s, bmFileHeader, bmInfoHeader, pOemPDEV, dHeight, nChannels, bDriver);

					if (!bDriver)
						EndEditSession (s, pOemPDEV);

					if (bSelectLabel) {
						if (!DoPrint (s, pOemPDEV, bDriver, nCopies, nHeadNumber, sockUDP))
							Send (s, pOemPDEV->m_lCmdID++, IPC_SET_PRINTER_MODE, _T ("PRINT_MODE=STOP;"));
						else
							Broadcast (sockUDP, PRINTDRIVER_MSG_ENDPRINT, nHeadNumber, nCopies);
					}
				}

				closesocket (s);
				DebugMsg (DLLTEXT ("close: %s"), pOemPDEV->szPort);
			}
			else {
				DebugMsg (DLLTEXT ("connect failed: %s: %s"), pOemPDEV->szPort, FormatMessage (::WSAGetLastError ()));
				Broadcast (sockUDP, PRINTDRIVER_MSG_ERROR, nHeadNumber, PRINTDRIVER_ERROR_CONNECTFAILED);
			}
		}

        // Dump headers first
        //
        dwWritten = pDevObj->pDrvProcs->DrvWriteSpoolBuf(pDevObj, (void*)&(pOemPDEV->bmFileHeader), sizeof(BITMAPFILEHEADER));
        dwWritten = pDevObj->pDrvProcs->DrvWriteSpoolBuf(pDevObj, (void*)&(pOemPDEV->bmInfoHeader), sizeof(BITMAPINFOHEADER));
        if (pOemPDEV->bColorTable)
        {
            dwWritten = pDevObj->pDrvProcs->DrvWriteSpoolBuf(pDevObj, pOemPDEV->prgbq, pOemPDEV->cPalColors * sizeof(ULONG));
            LocalFree(pOemPDEV->prgbq);
        }

        // Dump the data now
        //
        dwWritten = pDevObj->pDrvProcs->DrvWriteSpoolBuf(pDevObj, pOemPDEV->pBufStart, pOemPDEV->bmInfoHeader.biSizeImage);

        // Free memory for the data buffers
        //
        vFreeBuffer(pOemPDEV);
    }

	
	if (sockUDP != SOCKET_ERROR)
		closesocket (sockUDP);

	DeletePreview (pOemPDEV->szPort);
	DebugMsg (DLLTEXT ("OEMEndDoc complete"));

    // Punt call back to UNIDRV.
    //
    return (pOemPDEV->m_pfnDrvEndDoc)(pso, fl);
}


