// PrinterDriver_.h : main header file for the PRINTERDRIVER_ application
//

#if !defined(AFX_PRINTERDRIVER__H__9FE29E18_7B8F_4414_8893_E1A06E53D6DA__INCLUDED_)
#define AFX_PRINTERDRIVER__H__9FE29E18_7B8F_4414_8893_E1A06E53D6DA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine);

#ifdef _DEBUG
	#define TRACEF(s) Trace (s, __FILE__, __LINE__)
#else
	#define TRACEF(s)
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CPrinterDriverApp:
// See PrinterDriver_.cpp for the implementation of this class
//

class CPrinterDriverApp : public CWinApp
{
public:
	CPrinterDriverApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrinterDriverApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CPrinterDriverApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTERDRIVER__H__9FE29E18_7B8F_4414_8893_E1A06E53D6DA__INCLUDED_)
