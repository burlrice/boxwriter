// HostDoc.cpp : implementation of the CHostDoc class
//

#include "stdafx.h"
#include "Host.h"
#include <ODBCInst.h>
#include <afxsock.h>

#include "HostDoc.h"
#include "resource.h"
#include "Database.h"
#include "Export.h"
#include "FieldDefs.h"
#include "Serialize.h"
#include "PrinterView.h"
#include "ImportDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;



/////////////////////////////////////////////////////////////////////////////
// CHostDoc

IMPLEMENT_DYNCREATE(CHostDoc, CDocument)

BEGIN_MESSAGE_MAP(CHostDoc, CDocument)
	//{{AFX_MSG_MAP(CHostDoc)
	ON_COMMAND(ID_FILE_EDIT, OnFileEdit)
	ON_UPDATE_COMMAND_UI(ID_FILE_EDIT, OnUpdateFileEdit)
	ON_COMMAND(ID_PRINTER_DATABASE_DOWNLOAD, OnPrinterDatabaseDownload)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHostDoc construction/destruction

CHostDoc::CHostDoc()
:	m_hEditThread (NULL),
	m_bAlive (true)
{

}

CHostDoc::~CHostDoc()
{
	if (m_hEditThread) {
		m_bAlive = false;

		DWORD dwWait = ::WaitForSingleObject (m_hEditThread, 2000);

		if (dwWait == WAIT_TIMEOUT) {
			TRACEF ("WaitForSingleObject failed");
			ASSERT (0);
		}

		m_hEditThread = NULL;
	}
}

BOOL CHostDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CHostDoc serialization

void CHostDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CHostDoc diagnostics

#ifdef _DEBUG
void CHostDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CHostDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CHostDoc commands

bool CHostDoc::CreateDatabase (const CString & strFilepath)
{
	HMODULE hModule = ::AfxGetInstanceHandle ();
	bool bResult = false;

	if (HRSRC hRSRC = ::FindResource (hModule, MAKEINTRESOURCE (IDR_DATABASE), _T ("Access_Database"))) {
		DWORD dwSize = ::SizeofResource (hModule, hRSRC);

		if (HGLOBAL hGlobal = ::LoadResource (hModule, hRSRC)) {
			if (LPVOID lpData = ::LockResource (hGlobal)) {
				if (FILE * fp = _tfopen (strFilepath, _T ("wb"))) {
					if (fwrite (lpData, dwSize, 1, fp) == 1)
						bResult = true;
	
					fclose (fp);
				}
			}
		}
	}

	return bResult;
}

/* TODO: rem
LPCTSTR lpszDB [] = 
{
	"{LINESTRUCT,1,LINE0001,Test line 1,12:00,NO READ,1,3,0,10,10.1.2.50,0,9600\\,8\\,N\\,1\\,0,9600\\,8\\,N\\,1\\,0,9600\\,8\\,N\\,1\\,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,4.000000}",

	"{BOXSTRUCT,39,Transform,Test box,8000,12000,4000}",
	"{BOXSTRUCT,40,12x12x12,Beta Box,12000,12000,12000}",
	"{BOXSTRUCT,41,Beta (6.5x6x6),,6000,6500,6000}",
	"{BOXSTRUCT,45,Imageless,,27000,27000,27000}",
	"{BOXSTRUCT,46,Long,,1823,108000,1823}",
	"{BOXSTRUCT,47,Max,,12000,27000,12000}",
	"{BOXSTRUCT,49,Rumford,3d demo box,9000,12000,5000}",

	"{BOXLINKSTRUCT,1,39}",
	"{BOXLINKSTRUCT,1,40}",
	"{BOXLINKSTRUCT,1,41}",
	"{BOXLINKSTRUCT,1,45}",
	"{BOXLINKSTRUCT,1,46}",
	"{BOXLINKSTRUCT,1,47}",
	"{BOXLINKSTRUCT,1,49}",

	"{TASKSTRUCT,461,1,39,Export,Export/import test message,,0,{MESSAGESTRUCT,3797,21,461,Export,,\\{Ver\\,1\\,20\\,0\\,0\\,21\\}\\{ProdArea\\,12000\\,1771\\,0\\,21\\}\\{Text\\,1\\,0\\,0\\,0\\,0\\,0\\,MK Arial\\,32\\,1\\,0\\,Export/import test element\\,0\\,0\\,0\\},170,true}}",

	"{HEADSTRUCT,21,1,Front,300,0,32,300,1000,0,1,1,0,76.800000,0,1,1,0,0,1,240,1.813000,6.000000,9600\\,8\\,N\\,2\\,0,2,0}",

	"{PANELSTRUCT,1,1,Front,1,0,0}",
	"{PANELSTRUCT,2,1,Right,2,1,0}",
	"{PANELSTRUCT,3,1,Back,3,1,0}",
	"{PANELSTRUCT,4,1,Left,4,0,0}",
	"{PANELSTRUCT,5,1,Top,5,1,0}",
	"{PANELSTRUCT,6,1,Bottom,6,0,0}",

	"{OPTIONSSTRUCT,32773,Start Print Task}",
	"{OPTIONSSTRUCT,32774,Stop Print Task}",
	"{OPTIONSSTRUCT,32784,Run Editor}",
	"{OPTIONSSTRUCT,57665,Quit the application}",
	"{OPTIONSSTRUCT,32799,View Printer Report}",
	"{OPTIONSSTRUCT,32800,View Scanner Report}",
	"{OPTIONSSTRUCT,32775,Enable / Disable Preview Mode}",
	"{OPTIONSSTRUCT,32821,Refresh preview}",
	"{OPTIONSSTRUCT,32772,Configure Print Head Settings}",
	"{OPTIONSSTRUCT,32786,Configure Production Lines}",
	"{OPTIONSSTRUCT,32804,Define global barcode parameters}",
	"{OPTIONSSTRUCT,32802,Configure date/time settings}",
	"{OPTIONSSTRUCT,32806,Define the shift codes}",
	"{OPTIONSSTRUCT,32818,Configure dynamic data table}",
	"{OPTIONSSTRUCT,32819,Configure general Windows settings}",
	"{OPTIONSSTRUCT,32789,Configure Users}",
	"{OPTIONSSTRUCT,32791,Configure Group Options.}",
	"{OPTIONSSTRUCT,32815,Translate the software}",

	"{OPTIONSLINKSTRUCT,32772,6}",
	"{OPTIONSLINKSTRUCT,32772,3}",
	"{OPTIONSLINKSTRUCT,32772,10}",
	"{OPTIONSLINKSTRUCT,32772,2}",
	"{OPTIONSLINKSTRUCT,32772,5}",
	"{OPTIONSLINKSTRUCT,32773,3}",
	"{OPTIONSLINKSTRUCT,32773,10}",
	"{OPTIONSLINKSTRUCT,32773,1}",
	"{OPTIONSLINKSTRUCT,32774,4}",
	"{OPTIONSLINKSTRUCT,32774,3}",
	"{OPTIONSLINKSTRUCT,32774,10}",
	"{OPTIONSLINKSTRUCT,32774,5}",
	"{OPTIONSLINKSTRUCT,32774,1}",
	"{OPTIONSLINKSTRUCT,32783,10}",
	"{OPTIONSLINKSTRUCT,32784,10}",
	"{OPTIONSLINKSTRUCT,32786,4}",
	"{OPTIONSLINKSTRUCT,32786,6}",
	"{OPTIONSLINKSTRUCT,32786,3}",
	"{OPTIONSLINKSTRUCT,32786,10}",
	"{OPTIONSLINKSTRUCT,32789,10}",
	"{OPTIONSLINKSTRUCT,32791,10}",
	"{OPTIONSLINKSTRUCT,32793,10}",
	"{OPTIONSLINKSTRUCT,32802,10}",
	"{OPTIONSLINKSTRUCT,32804,10}",
	"{OPTIONSLINKSTRUCT,32806,10}",
	"{OPTIONSLINKSTRUCT,57665,10}",
	"{OPTIONSLINKSTRUCT,32818,10}",
	"{OPTIONSLINKSTRUCT,32819,10}",
	"{OPTIONSLINKSTRUCT,32775,10}",
	"{OPTIONSLINKSTRUCT,32821,10}",
	"{OPTIONSLINKSTRUCT,32799,10}",
	"{OPTIONSLINKSTRUCT,32800,10}",
	"{OPTIONSLINKSTRUCT,32815,10}",

	"{USERSTRUCT,3,Admin,Admin,admin,FOXJET,10}",
	"{SECURITYGROUPSTRUCT,1,Operator}",
	"{SECURITYGROUPSTRUCT,2,Manager}",
	"{SECURITYGROUPSTRUCT,3,Level 3}",
	"{SECURITYGROUPSTRUCT,4,Graphics Dept}",
	"{SECURITYGROUPSTRUCT,5,Supervisor}",
	"{SECURITYGROUPSTRUCT,6,Level 6}",
	"{SECURITYGROUPSTRUCT,7,Level 7}",
	"{SECURITYGROUPSTRUCT,8,Level 8}",
	"{SECURITYGROUPSTRUCT,9,Level 9}",
	"{SECURITYGROUPSTRUCT,10,Administrator}",
};
*/

void CHostDoc::OnFileEdit() 
{
	using namespace FoxjetDatabase;

	if (theApp.IsEditorRunning ()) {
		AfxMessageBox (LoadString (IDS_EDITORRUNNING));
		return;
	}

	CPrinter * pPrinter = GetSelectedPrinter ();

	if (pPrinter == NULL) {
		AfxMessageBox (LoadString (IDS_NOTHINGSELECTED));
		return;
	}

	BeginWaitCursor ();

	pPrinter->InitDb ();

	try
	{
		COdbcDatabase db;
		CString strDSN = pPrinter->GetDSN ();
		CString strFile = pPrinter->GetDbFilename ();
		DWORD dw = 0;

		VERIFY (OpenDatabase (db, strDSN, strFile));

		m_vCurrent.RemoveAll ();

		if (!pPrinter->IsConnected ()) 
			(* pPrinter)->Connect (pPrinter->m_strAddress.AllocSysString ()); // TODO: rem

		if (!pPrinter->IsConnected ()) 
			AfxMessageBox (LoadString (IDS_PRINTERNOTCONNECTED));
		else {

			GETRECORDS (db, LINESTRUCT,				Lines::m_lpszTable,				m_vCurrent);
			GETRECORDS (db, BOXSTRUCT,				Boxes::m_lpszTable,				m_vCurrent);
			GETRECORDS (db, BOXLINKSTRUCT,			BoxToLine::m_lpszTable,			m_vCurrent);
			GETRECORDS (db, TASKSTRUCT,				Tasks::m_lpszTable,				m_vCurrent);	
			GETRECORDS (db, HEADSTRUCT,				Heads::m_lpszTable,				m_vCurrent);	
			GETRECORDS (db, PANELSTRUCT,			Panels::m_lpszTable,			m_vCurrent);
			GETRECORDS (db, OPTIONSSTRUCT,			Options::m_lpszTable,			m_vCurrent);
			GETRECORDS (db, OPTIONSLINKSTRUCT,		OptionsToGroup::m_lpszTable,	m_vCurrent);
			GETRECORDS (db, USERSTRUCT,				Users::m_lpszTable,				m_vCurrent);	
			GETRECORDS (db, SECURITYGROUPSTRUCT,	SecurityGroups::m_lpszTable,	m_vCurrent);
			GETRECORDS (db, SHIFTSTRUCT,			Shifts::m_lpszTable,			m_vCurrent);
			GETRECORDS (db, REPORTSTRUCT,			Reports::m_lpszTable,			m_vCurrent);
			GETRECORDS (db, BCSCANSTRUCT,			BarcodeScans::m_lpszTable,		m_vCurrent);
			GETRECORDS (db, DELETEDSTRUCT,			Deleted::m_lpszTable,			m_vCurrent);
			GETRECORDS (db, SETTINGSSTRUCT,			Settings::m_lpszTable,			m_vCurrent);
			GETRECORDS (db, IMAGESTRUCT,			Images::m_lpszTable,			m_vCurrent);

			if (theApp.m_pEdit) {
				delete theApp.m_pEdit;
				theApp.m_pEdit = NULL;
			}

			theApp.m_pEdit = new EDITSTRUCT;

			theApp.m_pEdit->m_pDoc			= this;
			theApp.m_pEdit->m_pPrinter		= pPrinter;

			VERIFY (m_hEditThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)Edit, (LPVOID)theApp.m_pEdit, 0, &dw));
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	catch (CException * e)			{ HANDLEEXCEPTION (e); }
	catch (_com_error & e)			{ TRACEF (e.ErrorMessage ()); }
	catch (...)						{ TRACEF ("WTF?"); }

	EndWaitCursor ();
}

ULONG CALLBACK CHostDoc::Edit (LPVOID lpData)
{
	CHostDoc & doc = * theApp.m_pEdit->m_pDoc;
	CString strApp = GetHomeDir () + _T ("\\mkdraw.exe");
	CString strCmdLine = strApp + _T (" /nosplash /DSN=\"") + theApp.m_pEdit->m_pPrinter->GetDSN () + _T ("\"");
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	char szCmdLine [0xFF];

	_tcsncpy (szCmdLine, strCmdLine, 0xFF);
    memset (&pi, 0, sizeof(pi));
    memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	TRACEF (szCmdLine);
	BOOL bResult = ::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi); 

	if (!bResult) {
		::AfxMessageBox (LoadString (IDS_FAILEDTOLANUCHEDITOR));
	}
	else {
		bool bMore;
		
		TRACEF ("begin edit");

		do {
			DWORD dwExitCode = 0;
			
			if (!GetExitCodeProcess (pi.hProcess, &dwExitCode))
				bMore = false;
			else if (!doc.m_bAlive)
				bMore = false;
			else {
				bMore = (dwExitCode == STILL_ACTIVE);
				WaitForSingleObjectEx(pi.hProcess, 3000, TRUE);
				TRACEF ("still waiting");
			}
		}
		while (bMore);

		TRACEF ("edit complete");
	}

	CView * pView = theApp.GetView (RUNTIME_CLASS (CPrinterView));
	ASSERT (pView);
	
	while (!::PostMessage (pView->m_hWnd, WM_EDIT_COMPLETE, 0, 0))
		::Sleep (0);

	return 0;
}

void CHostDoc::OnEditComplete()
{
	BeginWaitCursor ();

	VERIFY (::WaitForSingleObject (m_hEditThread, 2000) != WAIT_TIMEOUT);
	m_hEditThread = NULL;

	ASSERT (theApp.m_pEdit);

	try {
		COdbcDatabase db;
		CStringArray v, vUpdate;
		CString strDSN = theApp.m_pEdit->m_pPrinter->GetDSN ();
		CString strFile = theApp.m_pEdit->m_pPrinter->GetDbFilename ();
		CString strSQL;
		COdbcRecordset rst (db);

		VERIFY (OpenDatabase (db, strDSN, strFile));

		strSQL.Format (_T ("SELECT [%s].* FROM [%s] INNER JOIN [%s] ON [%s].[%s] = [%s].[%s] WHERE ((([%s].[%s])=True));"),
			Tasks::m_lpszTable, 
			Tasks::m_lpszTable, 
			Messages::m_lpszTable,
			Tasks::m_lpszTable, 
			Tasks::m_lpszID, 
			Messages::m_lpszTable, 
			Messages::m_lpszTaskID,
			Messages::m_lpszTable,
			Messages::m_lpszIsUpdated);
		TRACEF (strSQL);

		rst.Open (strSQL, CRecordset::snapshot);

		while (!rst.IsEOF ()) {
			TASKSTRUCT t;

			rst >> t;

			CString str = FoxjetCommon::ToString (t);
			vUpdate.Add (str);
			rst.MoveNext ();
		}

		//GETRECORDS (db, LINESTRUCT,			Lines::m_lpszTable,				v);
		GETRECORDS (db, BOXSTRUCT,				Boxes::m_lpszTable,				v);
		GETRECORDS (db, BOXLINKSTRUCT,			BoxToLine::m_lpszTable,			v);
		//GETRECORDS (db, TASKSTRUCT,				Tasks::m_lpszTable,				v);	
		//GETRECORDS (db, HEADSTRUCT,			Heads::m_lpszTable,				v);	
		//GETRECORDS (db, PANELSTRUCT,			Panels::m_lpszTable,			v);
		//GETRECORDS (db, OPTIONSSTRUCT,		Options::m_lpszTable,			v);
		//GETRECORDS (db, OPTIONSLINKSTRUCT,	OptionsToGroup::m_lpszTable,	v);
		//GETRECORDS (db, USERSTRUCT,			Users::m_lpszTable,				v);	
		//GETRECORDS (db, SECURITYGROUPSTRUCT,	SecurityGroups::m_lpszTable,	v);
		GETRECORDS (db, SHIFTSTRUCT,			Shifts::m_lpszTable,			v);
		//GETRECORDS (db, REPORTSTRUCT,			Reports::m_lpszTable,			v);
		//GETRECORDS (db, BCSCANSTRUCT,			BarcodeScans::m_lpszTable,		v);
		//GETRECORDS (db, DELETEDSTRUCT,		Deleted::m_lpszTable,			v);
		GETRECORDS (db, SETTINGSSTRUCT,			Settings::m_lpszTable,			v);
		GETRECORDS (db, IMAGESTRUCT,			Images::m_lpszTable,			v);
		
		for (int i = 0; i < m_vCurrent.GetSize (); i++) 
			m_vCurrent.SetAt (i, ZeroMessageID (m_vCurrent [i]));

		for (i = 0; i < v.GetSize (); i++) {
			CString str = v [i];
			CString strZero = ZeroMessageID (str);
			int nIndex = Find (m_vCurrent, strZero, true);

			if (nIndex == -1) {
				vUpdate.Add (str);
			}
		}

		m_vCurrent.RemoveAll ();

		{ 
			CComPtr <IStringArray> vDB;
			int nCount = 0;
			
			VERIFY (SUCCEEDED (vDB.CoCreateInstance (CLSID_StringArray)));
			
			for (int i = 0; i < vUpdate.GetSize (); i++) {
				CString str = vUpdate [i];

				TRACEF (str);
				vDB->Add (str.AllocSysString ());
			}

			if (vUpdate.GetSize ()) {
				CString strSQL;
				(*  theApp.m_pEdit->m_pPrinter)->FromString (vDB, FoxjetDatabase::IMPORT_ALL, 5 * 60); 

				strSQL.Format (_T ("UPDATE [%s] SET [%s].[%s] = False;"),
					Messages::m_lpszTable,
					Messages::m_lpszTable,
					Messages::m_lpszIsUpdated);
				db.ExecuteSQL (strSQL);
			}
		}

		db.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	catch (CException * e)			{ HANDLEEXCEPTION (e); }
	catch (_com_error & e)			{ TRACEF (e.ErrorMessage ()); }
	catch (...)						{ TRACEF ("WTF?"); }

	delete theApp.m_pEdit;
	theApp.m_pEdit = NULL;

	EndWaitCursor ();
}

void CHostDoc::OnUpdateFileEdit(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable (m_hEditThread == NULL);
}

CString CHostDoc::ZeroMessageID (CString str)
{
	if (str.Find (_T ("TASKSTRUCT")) != -1) {
		FoxjetDatabase::TASKSTRUCT t;

		if (FoxjetCommon::FromString (str, t)) {
			t.m_lID = 0;

			for (int i = 0; i < t.m_vMsgs.GetSize (); i++) 
				t.m_vMsgs [i].m_lID = 0;

			str = FoxjetCommon::ToString (t);
		}
	}

	return str;
}

CPrinter * CHostDoc::GetSelectedPrinter () const
{
	CPrinterView * pView = (CPrinterView *)theApp.GetView (RUNTIME_CLASS (CPrinterView));

	ASSERT (pView);

	CTreeCtrl & tree = pView->GetTreeCtrl ();
	
	if (HTREEITEM hItem = tree.GetSelectedItem ())
		return (CPrinter *)tree.GetItemData (hItem); 

	return NULL;
}

void CHostDoc::OnPrinterDatabaseDownload() 
{
	CPrinter * pPrinter = GetSelectedPrinter ();

	if (!pPrinter) {
		AfxMessageBox (LoadString (IDS_NOTHINGSELECTED));
		return;
	}

	BeginWaitCursor ();

	pPrinter->InitDb ();

	try
	{
		if (!pPrinter->IsConnected ())  
			(* pPrinter)->Connect (pPrinter->m_strAddress.AllocSysString ()); // TODO: rem

		if (!pPrinter->IsConnected ())  
			AfxMessageBox (LoadString (IDS_PRINTERNOTCONNECTED));
		else {
			CImportDlg dlg (AfxGetMainWnd ());

			if (dlg.DoModal () == IDOK) {
				COdbcDatabase db;
				CComPtr <IStringArray> vDB;
				CString strDSN = pPrinter->GetDSN ();
				CString strDir = pPrinter->GetDbDir ();
				CString strFilename = pPrinter->GetDbFilename ();
				int nCount = 0;
				DWORD dwImport = 0; 
				CStringArray v;

				dwImport |= dlg.m_bConfig	? FoxjetDatabase::IMPORT_CONFIG		: 0;
				dwImport |= dlg.m_bReports	? FoxjetDatabase::IMPORT_REPORTS	: 0;
				dwImport |= dlg.m_bTasks	? FoxjetDatabase::IMPORT_TASKS		: 0;
				dwImport |= dlg.m_bUser		? FoxjetDatabase::IMPORT_USERS		: 0;

				VERIFY (OpenDatabase (db, strDSN, strFilename));

				VERIFY (SUCCEEDED (vDB.CoCreateInstance (CLSID_StringArray)));
				(* pPrinter)->ToString (vDB, dwImport, 5 * 60);

				vDB->get_Count (&nCount);

				for (int i = 0; i < nCount; i++) {
					BSTR bstr;

					vDB->get_GetAt (i, &bstr);
					v.Add ((LPCTSTR)_bstr_t (bstr));
				}

				DeleteAllRecords (db);
				int nImport = FoxjetCommon::Import (db, v, FoxjetDatabase::IMPORT_ALL);

				ASSERT (nImport == v.GetSize ());

				{
					CString strSQL;

					strSQL.Format (_T ("UPDATE [%s] SET [%s].[%s] = False;"),
						Messages::m_lpszTable,
						Messages::m_lpszTable,
						Messages::m_lpszIsUpdated);
					db.ExecuteSQL (strSQL);
				}

				db.Close ();
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	catch (CException * e)	{ HANDLEEXCEPTION (e); }
	catch (_com_error & e)	{ TRACEF (e.ErrorMessage ()); }
	catch (...)				{ TRACEF ("WTF?"); }

	EndWaitCursor ();
}
