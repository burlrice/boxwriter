# Microsoft Developer Studio Project File - Name="Host" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Host - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Host.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Host.mak" CFG="Host - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Host - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Host - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Host - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "c:\foxjet" /I "..\Editor\DLL\Utils" /I "..\Editor\DLL\Common" /I "..\Editor\DLL\Debug" /I "..\Editor\DLL\Database" /I "..\Editor\DLL\3d" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 Utils.lib Database.lib 3d.lib /nologo /subsystem:windows /machine:I386 /libpath:"..\Editor\DLL\3d\Release" /libpath:"..\Editor\DLL\Database\Release" /libpath:"..\Editor\DLL\Utils\Release"

!ELSEIF  "$(CFG)" == "Host - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\Editor\DLL\Utils" /I "c:\foxjet" /I "..\Editor\DLL\Common" /I "..\Editor\DLL\Debug" /I "..\Editor\DLL\Database" /I "..\Editor\DLL\3d" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Debug.lib Utils.lib Database.lib 3d.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"..\Editor\DLL\Debug\Debug" /libpath:"..\Editor\DLL\3d\Debug" /libpath:"..\Editor\DLL\Database\Debug" /libpath:"..\Editor\DLL\Utils\Debug"
# Begin Custom Build - Updating \FoxJet\$(TargetName).exe
TargetPath=.\Debug\Host.exe
TargetName=Host
InputPath=.\Debug\Host.exe
SOURCE="$(InputPath)"

BuildCmds= \
	Copy $(TargetPath) \FoxJet \
	Copy $(TargetPath) \FoxJet\IP \
	

"\FoxJet\$(TargetName).exe" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)

"\FoxJet\IP\$(TargetName).exe" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ENDIF 

# Begin Target

# Name "Host - Win32 Release"
# Name "Host - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\boxwriter.cpp
# End Source File
# Begin Source File

SOURCE=.\HeadView.cpp
# End Source File
# Begin Source File

SOURCE=.\Host.cpp
# End Source File
# Begin Source File

SOURCE=.\Host.rc
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409 /i "..\Editor\DLL\Common"
# End Source File
# Begin Source File

SOURCE=.\HostDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\ImportDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\PrinterRecordset.cpp
# End Source File
# Begin Source File

SOURCE=.\PrinterView.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TaskView.cpp
# End Source File
# Begin Source File

SOURCE=.\UpdatedRecordset.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\boxwriter.h
# End Source File
# Begin Source File

SOURCE=.\HeadView.h
# End Source File
# Begin Source File

SOURCE=.\Host.h
# End Source File
# Begin Source File

SOURCE=.\HostDoc.h
# End Source File
# Begin Source File

SOURCE=.\ImportDlg.h
# End Source File
# Begin Source File

SOURCE=.\Interface.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\PrinterRecordset.h
# End Source File
# Begin Source File

SOURCE=.\PrinterView.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TaskView.h
# End Source File
# Begin Source File

SOURCE=.\UpdatedRecordset.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\boxwrite.ico
# End Source File
# Begin Source File

SOURCE=.\res\conveyor.ico
# End Source File
# Begin Source File

SOURCE=.\res\head.ico
# End Source File
# Begin Source File

SOURCE=.\res\headdisa.ico
# End Source File
# Begin Source File

SOURCE=.\res\headmast.ico
# End Source File
# Begin Source File

SOURCE=.\res\Host.ico
# End Source File
# Begin Source File

SOURCE=.\res\Host.rc2
# End Source File
# Begin Source File

SOURCE=.\res\HostDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_conv.ico
# End Source File
# Begin Source File

SOURCE=.\res\mkdraw.ico
# End Source File
# Begin Source File

SOURCE=.\res\panel.ico
# End Source File
# Begin Source File

SOURCE=.\res\task.ico
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\res\MarksmanPro.mdb
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
# Section Host : {49D715D3-0166-4774-BFAC-F8E57EE482D7}
# 	2:5:Class:CBoxWriter
# 	2:10:HeaderFile:boxwriter.h
# 	2:8:ImplFile:boxwriter.cpp
# End Section
# Section Host : {2E5D9215-26EF-4024-8BAC-93C45C8E6C9D}
# 	2:21:DefaultSinkHeaderFile:boxwriter.h
# 	2:16:DefaultSinkClass:CBoxWriter
# End Section
