// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "Host.h"

#include "MainFrm.h"
#include "PrinterView.h"
#include "TaskView.h"
#include "HeadView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	return 0;
}





BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/,

	CCreateContext* pContext)
{
	const CString strClass = a2w (GetRuntimeClass ()->m_lpszClassName);
	const CString strPane [3] =
	{
		strClass + _T ("\\WindowState\\m_wndSplitter [0]\\Pane [0, 0]"),
		strClass + _T ("\\WindowState\\m_wndSplitter [1]\\Pane [0, 0]"),
		strClass + _T ("\\WindowState\\m_wndSplitter [1]\\Pane [1, 0]"),
	};
	const CString strDef [3] = 
	{
		_T ("140, 260"),
		_T ("440, 80"),
		_T ("440, 170"),
	};
	CSize size [3];

	for (int i = 0; i < ARRAYSIZE (size); i++) {
		CString str = theApp.GetProfileString (strPane [i], _T ("size"), strDef [i]);
		_stscanf (str, _T ("%d, %d"), &size [i].cx, &size [i].cy);
	}

	// create splitter window
	if (!m_wndSplitter [0].CreateStatic(this, 1, 2))
		return FALSE;

	if (!m_wndSplitter [0].CreateView(0, 0, pContext->m_pNewViewClass, size [0], pContext)) 
	{
		TRACEF ("CreateStatic failed");
		return FALSE;
	}

	if (!m_wndSplitter [1].CreateStatic (
		&m_wndSplitter [0],     // our parent window is the first splitter
		2, 1,					// the new splitter is 2 rows, 1 column
		WS_CHILD | WS_VISIBLE | WS_BORDER,  // style, WS_BORDER is needed
		m_wndSplitter [0].IdFromRowCol(0, 1)))
	{
		TRACEF ("CreateStatic failed");
		return FALSE;
	}

	if (!m_wndSplitter [1].CreateView (1, 0, RUNTIME_CLASS (CTaskView), size [1], pContext))
	{
		TRACEF ("CreateView failed");
		return FALSE;
	}

	if (!m_wndSplitter [1].CreateView (0, 0, RUNTIME_CLASS (CHeadView), size [2], pContext))
	{
		TRACEF ("CreateView failed");
		return FALSE;
	}

	if (CWnd * pWnd = m_wndSplitter [1].GetPane (1, 0))
		if (pWnd->IsKindOf (RUNTIME_CLASS (CView)))
			SetActiveView ((CView *)pWnd);

	return TRUE;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

CTaskView* CMainFrame::GetRightPane()
{
	CWnd* pWnd = m_wndSplitter [0].GetPane(0, 1);
	CTaskView* pView = DYNAMIC_DOWNCAST(CTaskView, pWnd);
	return pView;
}





