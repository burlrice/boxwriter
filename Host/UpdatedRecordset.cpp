#include "stdafx.h"
#include "UpdatedRecordset.h"
#include "Database.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;

static const LPCTSTR lpszTable		= _T ("Updated");
static const LPCTSTR lpszTaskID		= _T ("TaskID");
static const LPCTSTR lpszPrinterID	= _T ("PrinterID");

const FoxjetDatabase::COdbcRecordset & operator >> (const FoxjetDatabase::COdbcRecordset & rst, UPDATEDSTRUCT & s)
{
	try {
		s.m_lTaskID			= (long)rst.GetFieldValue (lpszTaskID);
		s.m_lPrinterID		= (long)rst.GetFieldValue (lpszPrinterID);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

FoxjetDatabase::COdbcRecordset & operator << (FoxjetDatabase::COdbcRecordset & rst, const UPDATEDSTRUCT & s)
{
	try {
		rst.SetFieldValue (lpszTaskID,		(long)s.m_lTaskID);
		rst.SetFieldValue (lpszPrinterID,	(long)s.m_lPrinterID);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

////////////////////////////////////////////////////////////////////////////////

bool AddUpdatedRecord (FoxjetDatabase::COdbcDatabase & database, UPDATEDSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	int nField = 1;

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s]"), lpszTable);
		rst.Open (str, CRecordset::dynaset);
		rst.AddNew ();
		rst << s;
		rst.Update ();
		rst.Close ();
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

bool GetUpdatedRecords (FoxjetDatabase::COdbcDatabase & database, ULONG lPrinterID, CArray <UPDATEDSTRUCT, UPDATEDSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	v.RemoveAll ();

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%d]=%u"),
			lpszTable, lpszPrinterID, lPrinterID);
		rst.Open (str);

		while (!rst.IsEOF ()) {
			UPDATEDSTRUCT s;
			rst >> s;
			v.Add (s);
			rst.MoveNext ();
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return v.GetSize () > 0;
}

bool DeletePrinterRecords (FoxjetDatabase::COdbcDatabase & database, ULONG lPrinterID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	CString str;
	
	str.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
		lpszTable, lpszPrinterID, lPrinterID);

	try {
		database.ExecuteSQL (str);
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}
