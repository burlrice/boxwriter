#if !defined(AFX_HEADVIEW_H__D4C90C32_F04E_4C25_A12A_C5016B892385__INCLUDED_)
#define AFX_HEADVIEW_H__D4C90C32_F04E_4C25_A12A_C5016B892385__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HeadView.h : header file
//

#include "ListCtrlImp.h"

/////////////////////////////////////////////////////////////////////////////
// CHeadView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CHeadView : public CFormView
{
public:
	class CHeadItem : public ItiLibrary::CListCtrlImp::CItem
	{
	public:
		CHeadItem ();
		virtual ~CHeadItem ();

		virtual CString GetDispText (int nColumn) const;

		CString m_strPanel;
		CString m_strName;
		CString m_strTask;
		CString m_strCount;
		CString m_strState;
		CString m_strStatus;
	};

protected:
	CHeadView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CHeadView)

// Form Data
public:
	//{{AFX_DATA(CHeadView)
	enum { IDD = IDD_HEAD_VIEW };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHeadView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CHeadView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	ItiLibrary::CListCtrlImp m_lv;

	// Generated message map functions
	//{{AFX_MSG(CHeadView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEADVIEW_H__D4C90C32_F04E_4C25_A12A_C5016B892385__INCLUDED_)
