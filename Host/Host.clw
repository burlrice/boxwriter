; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CImportDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Host.h"
LastPage=0

ClassCount=8
Class1=CHostApp
Class2=CHostDoc
Class3=CTaskView
Class4=CMainFrame

ResourceCount=4
Resource1=IDR_MAINFRAME
Resource2=IDD_HEAD_VIEW
Class5=CPrinterView
Class6=CAboutDlg
Class7=CHeadView
Resource3=IDD_ABOUTBOX
Class8=CImportDlg
Resource4=IDD_IMPORT

[CLS:CHostApp]
Type=0
HeaderFile=Host.h
ImplementationFile=Host.cpp
Filter=N
BaseClass=CWinApp
VirtualFilter=AC

[CLS:CHostDoc]
Type=0
HeaderFile=HostDoc.h
ImplementationFile=HostDoc.cpp
Filter=N
BaseClass=CDocument
VirtualFilter=DC

[CLS:CTaskView]
Type=0
HeaderFile=TaskView.h
ImplementationFile=TaskView.cpp
Filter=C


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T



[CLS:CPrinterView]
Type=0
HeaderFile=PrinterView.h
ImplementationFile=PrinterView.cpp
Filter=T
BaseClass=CTreeView
VirtualFilter=VWC

[CLS:CAboutDlg]
Type=0
HeaderFile=Host.cpp
ImplementationFile=Host.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_APP_EXIT
Command2=ID_FILE_EDIT
Command3=ID_PRINTER_DATABASE_DOWNLOAD
Command4=ID_PRINTER_DATABASE_UPLOAD
Command5=ID_VIEW_TOOLBAR
Command6=ID_VIEW_STATUS_BAR
Command7=ID_APP_ABOUT
CommandCount=7

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
CommandCount=8

[DLG:IDD_HEAD_VIEW]
Type=1
Class=CHeadView
ControlCount=2
Control1=IDC_BW,{2E5D9215-26EF-4024-8BAC-93C45C8E6C9D},1342242816
Control2=LV_HEADS,SysListView32,1342275597

[CLS:CHeadView]
Type=0
HeaderFile=HeadView.h
ImplementationFile=HeadView.cpp
BaseClass=CFormView
Filter=D
VirtualFilter=VWC
LastObject=CHeadView

[DLG:IDD_IMPORT]
Type=1
Class=CImportDlg
ControlCount=7
Control1=CHK_TASKS,button,1342242819
Control2=CHK_CONFIG,button,1342242819
Control3=CHK_USER,button,1342242819
Control4=CHK_REPORTS,button,1342242819
Control5=CHK_ALL,button,1342242819
Control6=IDOK,button,1342242817
Control7=IDCANCEL,button,1342242816

[CLS:CImportDlg]
Type=0
HeaderFile=ImportDlg.h
ImplementationFile=ImportDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CHK_ALL

