#ifndef __PRINTERRECORDSET_H__
#define __PRINTERRECORDSET_H__

#include "OdbcDatabase.h"
#include "OdbcRecordset.h"

typedef struct
{
	ULONG m_lID;
	CString m_strName;
	CString m_strAddress;
} PRINTERSTRUCT;

const FoxjetDatabase::COdbcRecordset & operator >> (const FoxjetDatabase::COdbcRecordset & rst, PRINTERSTRUCT & s);
FoxjetDatabase::COdbcRecordset & operator << (FoxjetDatabase::COdbcRecordset & rst, const PRINTERSTRUCT & s);
// throw CDBException, CMemoryException 

bool AddPrinterRecord (FoxjetDatabase::COdbcDatabase & database, PRINTERSTRUCT & s);
bool UpdatePrinterRecord (FoxjetDatabase::COdbcDatabase & database, const PRINTERSTRUCT & s);
bool GetPrinterRecords (FoxjetDatabase::COdbcDatabase & database, CArray <PRINTERSTRUCT, PRINTERSTRUCT &> & v);
bool DeletePrinterRecord (FoxjetDatabase::COdbcDatabase & database, ULONG lID);

#endif //__PRINTERRECORDSET_H__
