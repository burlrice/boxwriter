// Host.h : main header file for the HOST application
//

#if !defined(AFX_HOST_H__FDE20F97_9B53_4E72_B594_B0B3E94CCE09__INCLUDED_)
#define AFX_HOST_H__FDE20F97_9B53_4E72_B594_B0B3E94CCE09__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "OdbcDatabase.h"
#include "Interface.h"
#include "PrinterRecordset.h"
#include "MainFrm.h"


#define WM_EDIT_COMPLETE		(WM_APP + 1)

class CHostDoc;
class CPrinter;

typedef struct
{
	CHostDoc *	m_pDoc;
	CPrinter *	m_pPrinter;
} EDITSTRUCT;

class CPrinter : public CComPtr <IPrinter>, public PRINTERSTRUCT
{
public:
	CPrinter (const PRINTERSTRUCT & p);
	virtual ~CPrinter ();

	CString GetAddress () const;
	bool IsConnected () const;

	CString GetDSN () const;
	CString GetDbDir () const;
	CString GetDbFilename () const;

	static CString FormatAddress (const CString & str);

	bool InitDb ();
};

class CHostApp : public CWinApp
{
public:
	CHostApp();

public:
	bool IsEditorRunning () const;
	CMainFrame * GetActiveFrame () const;
	CView * GetActiveView() const;
	CDocument * GetActiveDocument() const;
	CBoxWriter * GetBwCtrl () const;
	CView * GetView (const CRuntimeClass * pClass) const;

	FoxjetDatabase::COdbcDatabase m_db;
	CArray <CPrinter * , CPrinter *> m_vPrinters;
	EDITSTRUCT * m_pEdit;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHostApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	afx_msg LRESULT OnIsAppRunning (WPARAM wParam, LPARAM lParam);

// Implementation
	//{{AFX_MSG(CHostApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HOST_H__FDE20F97_9B53_4E72_B594_B0B3E94CCE09__INCLUDED_)
