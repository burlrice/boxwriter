// TaskView.cpp : implementation of the CTaskView class
//

#include "stdafx.h"
#include "Host.h"

#include "HostDoc.h"
#include "TaskView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTaskView

IMPLEMENT_DYNCREATE(CTaskView, CView)

BEGIN_MESSAGE_MAP(CTaskView, CView)
	//{{AFX_MSG_MAP(CTaskView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTaskView construction/destruction

CTaskView::CTaskView()
{
	// TODO: add construction code here

}

CTaskView::~CTaskView()
{
}

BOOL CTaskView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTaskView drawing

void CTaskView::OnDraw(CDC* pDC)
{
	CHostDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	pDC->TextOut (0, 0, a2w (GetRuntimeClass ()->m_lpszClassName));
}

void CTaskView::OnInitialUpdate()
{
	CView::OnInitialUpdate();
}

/////////////////////////////////////////////////////////////////////////////
// CTaskView diagnostics

#ifdef _DEBUG
void CTaskView::AssertValid() const
{
	CView::AssertValid();
}

void CTaskView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CHostDoc* CTaskView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CHostDoc)));
	return (CHostDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTaskView message handlers
void CTaskView::OnStyleChanged(int nStyleType, LPSTYLESTRUCT lpStyleStruct)
{
	//TODO: add code to react to the user changing the view style of your window
}
