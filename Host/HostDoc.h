// HostDoc.h : interface of the CHostDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_HOSTDOC_H__1905162B_297C_4BA6_A940_B8991D70B5D0__INCLUDED_)
#define AFX_HOSTDOC_H__1905162B_297C_4BA6_A940_B8991D70B5D0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "OdbcDatabase.h"

class CPrinter;

class CHostDoc : public CDocument
{
	friend class CPrinterView;
protected: // create from serialization only
	CHostDoc();
	DECLARE_DYNCREATE(CHostDoc)

// Attributes
public:

// Operations
public:
	CPrinter * GetSelectedPrinter () const;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHostDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CHostDoc();

	static CString ZeroMessageID (CString str);
	static bool CreateDatabase (const CString & strFilepath);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

	static ULONG CALLBACK Edit (LPVOID lpData);

// Generated message map functions
protected:
	void OnEditComplete();

	bool m_bEditorRunning;
	bool m_bAlive;
	HANDLE m_hEditThread;
	CStringArray m_vCurrent;

	//{{AFX_MSG(CHostDoc)
	afx_msg void OnFileEdit();
	afx_msg void OnUpdateFileEdit(CCmdUI* pCmdUI);
	afx_msg void OnPrinterDatabaseDownload();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HOSTDOC_H__1905162B_297C_4BA6_A940_B8991D70B5D0__INCLUDED_)
