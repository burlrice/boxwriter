// PrinterView.h : interface of the CPrinterView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PRINTERVIEW_H__456DECAD_43E0_4B6B_AB2E_032E311BF626__INCLUDED_)
#define AFX_PRINTERVIEW_H__456DECAD_43E0_4B6B_AB2E_032E311BF626__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CHostDoc;

class CPrinterView : public CTreeView
{
protected: // create from serialization only
	CPrinterView();
	DECLARE_DYNCREATE(CPrinterView)

// Attributes
public:
	CHostDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrinterView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CPrinterView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CImageList m_il;

	HICON m_hPanel;
	HICON m_hHead;
	HICON m_hMasterHead;
	HICON m_hDisabledHead;
	HICON m_hBox;
	HICON m_hBoxWriter;
	HICON m_hBoxWriterError;
	HICON m_hBoxWriterDisconnected;
	HICON m_hConveyor;
	HICON m_hConveyorError;

	int m_nPanelIndex;
	int m_nHeadIndex;
	int m_nMasterHeadIndex;
	int m_nDisabledHeadIndex;
	int m_nBoxIndex;
	int m_nBoxWriterIndex;
	int m_nBoxWriterErrorIndex;
	int m_nBoxWriterDisconnectedIndex;
	int m_nConveyorIndex;
	int m_nConveyorErrorIndex;

// Generated message map functions
protected:
	afx_msg void OnEditComplete (WPARAM wParam, LPARAM lParam);

	//{{AFX_MSG(CPrinterView)
	afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in PrinterView.cpp
inline CHostDoc* CPrinterView::GetDocument()
   { return (CHostDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTERVIEW_H__456DECAD_43E0_4B6B_AB2E_032E311BF626__INCLUDED_)
