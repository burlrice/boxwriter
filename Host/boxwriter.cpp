// Machine generated IDispatch wrapper class(es) created by Microsoft Visual C++

// NOTE: Do not modify the contents of this file.  If this class is regenerated by
//  Microsoft Visual C++, your modifications will be overwritten.


#include "stdafx.h"
#include "boxwriter.h"

/////////////////////////////////////////////////////////////////////////////
// CBoxWriter

IMPLEMENT_DYNCREATE(CBoxWriter, CWnd)

/////////////////////////////////////////////////////////////////////////////
// CBoxWriter properties

/////////////////////////////////////////////////////////////////////////////
// CBoxWriter operations

CString CBoxWriter::Locate(long lTimeout)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		lTimeout);
	return result;
}

void CBoxWriter::SetWarnings(long bWarningsOn)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 bWarningsOn);
}
