//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Host.rc
//
#define IDD_ABOUTBOX                    100
#define ID_VIEW_ARRANGE                 127
#define IDR_MAINFRAME                   128
#define IDR_HOSTTYPE                    129
#define IDD_HEAD_VIEW                   130
#define IDI_CONVEYOR                    131
#define IDI_BOX                         132
#define IDI_HEAD                        133
#define IDI_HEADDISABLED                134
#define IDI_HEADMASTER                  135
#define IDI_BOXWRITER                   136
#define IDI_BOXWRITER_ERROR             137
#define IDI_CONVEYOR_ERROR              138
#define IDI_PANEL                       139
#define IDR_DATABASE                    140
#define IDI_BOXWRITER_DISCONNECTED      140
#define IDD_IMPORT                      141
#define IDC_BW                          1000
#define LV_HEADS                        1001
#define CHK_CONFIG                      1002
#define CHK_TASKS                       1003
#define CHK_USER                        1004
#define CHK_REPORTS                     1005
#define CHK_ALL                         1006
#define ID_FILE_EDIT                    32771
#define ID_PRINTER_DATABASE_DOWNLOAD    32772
#define ID_PRINTER_DATABASE_UPLOAD      32773
#define IDS_PANEL                       61204
#define IDS_NAME                        61205
#define IDS_TASK                        61206
#define IDS_COUNT                       61207
#define IDS_STATE                       61208
#define IDS_STATUS                      61209
#define IDS_FAILEDTOLANUCHEDITOR        61210
#define IDS_EDITORRUNNING               61211
#define IDS_FAILEDTOINITIALIZEDB        61212
#define IDS_NOTHINGSELECTED             61213
#define IDS_PRINTERNOTCONNECTED         61214

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        142
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
