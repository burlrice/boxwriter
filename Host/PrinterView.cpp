// PrinterView.cpp : implementation of the CPrinterView class
//

#include "stdafx.h"
#include "Host.h"
#include "resource.h"

#include "HostDoc.h"
#include "PrinterView.h"
#include "PrinterRecordset.h"
#include "Database.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrinterView

IMPLEMENT_DYNCREATE(CPrinterView, CTreeView)

BEGIN_MESSAGE_MAP(CPrinterView, CTreeView)
	//{{AFX_MSG_MAP(CPrinterView)
	ON_NOTIFY_REFLECT(NM_DBLCLK, OnDblclk)
	//}}AFX_MSG_MAP
	ON_MESSAGE (WM_EDIT_COMPLETE, OnEditComplete)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrinterView construction/destruction

CPrinterView::CPrinterView()
{
	HINSTANCE hInst = ::AfxGetInstanceHandle ();
	const int nSize = 32; 

	m_il.Create (nSize, nSize, ILC_COLOR, 0, 4);

	VERIFY (m_hPanel					= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_PANEL),					IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
	VERIFY (m_hHead						= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_HEAD),					IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
	VERIFY (m_hMasterHead				= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_HEADMASTER),				IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
	VERIFY (m_hDisabledHead				= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_HEADDISABLED),			IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
	VERIFY (m_hBox						= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_BOX),						IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
	VERIFY (m_hBoxWriter				= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_BOXWRITER),				IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
	VERIFY (m_hBoxWriterError			= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_BOXWRITER_ERROR),			IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
	VERIFY (m_hBoxWriterDisconnected	= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_BOXWRITER_DISCONNECTED),	IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
	VERIFY (m_hConveyor					= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_CONVEYOR),				IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
	VERIFY (m_hConveyorError			= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_CONVEYOR_ERROR),			IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));

	m_nPanelIndex						= m_il.Add (m_hPanel);
	m_nHeadIndex						= m_il.Add (m_hHead);
	m_nMasterHeadIndex					= m_il.Add (m_hMasterHead);
	m_nDisabledHeadIndex				= m_il.Add (m_hDisabledHead);
	m_nBoxIndex							= m_il.Add (m_hBox);
	m_nBoxWriterIndex					= m_il.Add (m_hBoxWriter);
	m_nBoxWriterErrorIndex				= m_il.Add (m_hBoxWriterError);
	m_nBoxWriterDisconnectedIndex		= m_il.Add (m_hBoxWriterDisconnected);
	m_nConveyorIndex					= m_il.Add (m_hConveyor);
	m_nConveyorErrorIndex				= m_il.Add (m_hConveyorError);
}

CPrinterView::~CPrinterView()
{
	HICON * p [] =
	{
		&m_hPanel,
		&m_hHead,
		&m_hMasterHead,
		&m_hDisabledHead,
		&m_hBox,
		&m_hBoxWriter,
		&m_hBoxWriterError,
		&m_hConveyor,
		&m_hConveyorError,
	};

	for (int i = 0; i < ARRAYSIZE (p); i++) {
		HICON & h = * p [i];

		if (h) {
			::DestroyIcon (h);
			h = NULL;
		}
	}
}

BOOL CPrinterView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CTreeView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CPrinterView drawing

void CPrinterView::OnDraw(CDC* pDC)
{
	CHostDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	pDC->TextOut (0, 0, a2w (GetRuntimeClass ()->m_lpszClassName));
}


void CPrinterView::OnInitialUpdate()
{
	CTreeCtrl & tree = GetTreeCtrl ();
	CArray <PRINTERSTRUCT, PRINTERSTRUCT &> v;

	CTreeView::OnInitialUpdate();

	tree.ModifyStyle (0, TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS | TVS_SHOWSELALWAYS);
	tree.SetImageList (&m_il, TVSIL_NORMAL);

	GetPrinterRecords (theApp.m_db, v);

	if (CBoxWriter * pCtrl = theApp.GetBwCtrl ())
		pCtrl->SetWarnings (false);

	theApp.m_vPrinters.RemoveAll ();

	for (int i = 0; i < v.GetSize (); i++) {
		PRINTERSTRUCT & printer = v [i];
		CPrinter * p = new CPrinter (printer);

		theApp.m_vPrinters.Add (p);

		HTREEITEM hItem = tree.InsertItem (printer.m_strName, 
			m_nBoxWriterDisconnectedIndex, m_nBoxWriterDisconnectedIndex);

		tree.SetItemData (hItem, (DWORD)p);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CPrinterView diagnostics

#ifdef _DEBUG
void CPrinterView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CPrinterView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}

CHostDoc* CPrinterView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CHostDoc)));
	return (CHostDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPrinterView message handlers

void CPrinterView::OnDblclk(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CTreeCtrl & tree = GetTreeCtrl ();

	if (HTREEITEM h = tree.GetSelectedItem ()) {
	}
	
	*pResult = 0;
}


void CPrinterView::OnEditComplete(WPARAM wParam, LPARAM lParam) 
{
	if (CHostDoc * pDoc = (CHostDoc *)GetDocument ())
		pDoc->OnEditComplete ();
}
