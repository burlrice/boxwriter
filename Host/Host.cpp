// Host.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Host.h"
#include <ODBCInst.h>

#include "MainFrm.h"
#include "HostDoc.h"
#include "PrinterView.h"
#include "WinMsg.h"
#include "Database.h"
#include "PrinterRecordset.h"
#include "UpdatedRecordset.h"
#include "HeadView.h"
#include "Database.h"

using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CPrinter::CPrinter (const PRINTERSTRUCT & p)
{
	PRINTERSTRUCT::operator = (p);
	VERIFY (SUCCEEDED (CoCreateInstance (CLSID_Printer)));
}

CPrinter::~CPrinter ()
{
	if (IsConnected ())
		(* this)->Disconnect ();
}

CString CPrinter::GetAddress () const
{
	BSTR bstr;

	(* this)->get_m_strAddress (&bstr);
	return ((LPCTSTR)_bstr_t (bstr));
}

bool CPrinter::IsConnected () const
{
	return GetAddress().GetLength () > 0;
}

CString CPrinter::FormatAddress (const CString & strFormat) 
{
	CString str (strFormat);

	str.Remove ('[');
	str.Remove (']');
	str.Replace (',', '_');
	str.Replace ('.', '_');

	return str;
}

CString CPrinter::GetDSN () const
{
	CString str = GetAddress ();

	if (!str.GetLength ())
		str = m_strAddress.GetLength () ? m_strAddress : _T ("Local host");

	return _T ("Host::") + FormatAddress (str);
}

CString CPrinter::GetDbFilename () const
{
	CString str = GetAddress ();

	if (!str.GetLength ())
		str = m_strAddress.GetLength () ? m_strAddress : _T ("Local host");

	return FormatAddress (str) + _T (".mdb");
}

CString CPrinter::GetDbDir () const
{
	return GetHomeDir () + _T ("\\Cache\\");
}

bool CPrinter::InitDb ()
{
	const LPCTSTR lpszDBDriver = _T ("Microsoft Access Driver (*.mdb)\0");
	CString strDSN = GetDSN ();
	CString strDir = GetDbDir ();
	CString strFilename = GetDbFilename ();

	if (::GetFileAttributes (strDir + strFilename) == -1) {
		if (::GetFileAttributes (strDir) != FILE_ATTRIBUTE_DIRECTORY)
			VERIFY (::CreateDirectory (strDir, NULL));
	
		VERIFY (CHostDoc::CreateDatabase (strDir + strFilename));
	}

	LPCTSTR lpszFormat = _T (
		"DSN=%s~ "					// dsn
		"DBQ=%s%s~ "				// file
		"Description=%s~ "			// dsn
		"FileType=MicrosoftAccess~ "
		"DataDirectory=%s~ "
		"MaxScanRows=20~ "
		"DefaultDir=%s~ "
		"FIL=MS Access;~ "
		"MaxBufferSize=2048~ "
		"MaxScanRows=8~ "
		"PageTimeout=5~ "
		"Threads=3~ ");
	TCHAR sz [512];

	_stprintf (sz, lpszFormat, 
		strDSN, 
		strDir, strFilename,
		strDSN,
		strDir, 
		strDir);
	int nLen = _tcsclen (sz);
	TRACEF (sz);

	for (int i = 0; i < nLen; i++)
		if (sz [i] == '~')
			sz [i] = '\0';

	bool bResult = ::SQLConfigDataSource (NULL, ODBC_ADD_SYS_DSN, lpszDBDriver, sz) == TRUE;

	if (!bResult)
		TRACE_SQLERROR ();

	return bResult;
}

/////////////////////////////////////////////////////////////////////////////
// CHostApp

BEGIN_MESSAGE_MAP(CHostApp, CWinApp)
	//{{AFX_MSG_MAP(CHostApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)

	ON_REGISTERED_MESSAGE (WM_ISHOSTRUNNING, OnIsAppRunning)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHostApp construction

CHostApp::CHostApp()
:	m_pEdit (NULL)
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CHostApp object

CHostApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CHostApp initialization

BOOL CHostApp::InitInstance()
{
	HANDLE hMutex = ::CreateMutex (NULL, FALSE, ISHOSTRUNNING);
	bool bRunning = 
		GetLastError () == ERROR_ALREADY_EXISTS || 
		GetLastError () == ERROR_ACCESS_DENIED;
	bool bExisits = GetLastError () == ERROR_ALREADY_EXISTS;
	bool bDenied = GetLastError () == ERROR_ACCESS_DENIED;

	if (bRunning)
		return FALSE;

	CString strDSN = _T ("Host");
	CString strName = _T ("Host.mdb");

	if (!FoxjetDatabase::OpenDatabase (m_db, strDSN, strName)) {
		::MessageBox (NULL, LoadString (IDS_FAILEDTOINITIALIZEDB), 
			::AfxGetAppName (), MB_ICONERROR | MB_OK);
		::AfxAbort ();
		return FALSE;
	}

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CHostDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CPrinterView));
	AddDocTemplate(pDocTemplate);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it.
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	return TRUE;
}


void CHostApp::OnAppAbout()
{
}

/////////////////////////////////////////////////////////////////////////////
// CHostApp message handlers


bool CHostApp::IsEditorRunning() const
{
	HANDLE hMutex = ::CreateMutex (NULL, FALSE, ISEDITORRUNNING);
	bool bRunning = 
		GetLastError () == ERROR_ALREADY_EXISTS || 
		GetLastError () == ERROR_ACCESS_DENIED;
	bool bExisits = GetLastError () == ERROR_ALREADY_EXISTS;
	bool bDenied = GetLastError () == ERROR_ACCESS_DENIED;

	if (hMutex)
		VERIFY (::CloseHandle (hMutex));

	return bRunning;
}

int CHostApp::ExitInstance() 
{
 	for (int i = 0; i < m_vPrinters.GetSize (); i++) 
 		if (CPrinter * p = m_vPrinters [i])
 			delete p;
 	
 	m_vPrinters.RemoveAll ();
	
	if (m_db.IsOpen ())
		m_db.Close ();

	return CWinApp::ExitInstance();
}

LRESULT CHostApp::OnIsAppRunning (WPARAM wParam, LPARAM lParam)
{
	::AfxGetMainWnd ()->SetForegroundWindow ();

	return WM_ISHOSTRUNNING;
}

CMainFrame * CHostApp::GetActiveFrame () const
{
	CMainFrame * pResult = NULL;
	CWinApp * pApp = ::AfxGetApp();
	ASSERT (pApp);
	
	if (pApp->m_pMainWnd) {
		ASSERT_KINDOF (CMainFrame, pApp->m_pMainWnd);
		pResult = DYNAMIC_DOWNCAST (CMainFrame, pApp->m_pMainWnd);
	}

	return pResult;
}

CView * CHostApp::GetActiveView() const
{
	CView * pView = NULL;

	if (CMainFrame * pFrame = GetActiveFrame ()) 
		pView = pFrame->GetActiveView ();

	return pView;
}

CDocument * CHostApp::GetActiveDocument() const
{
	CView * p = GetActiveView ();
	
	if (p) {
		ASSERT (p->GetDocument ());
		return p->GetDocument ();
	}
	else
		return NULL;
}

CView * CHostApp::GetView (const CRuntimeClass * pClass) const
{
	if (CDocument * pDoc = GetActiveDocument ()) {
		for (POSITION pos = pDoc->GetFirstViewPosition (); pos; ) {
			CView * pView = pDoc->GetNextView (pos);

			if (pView->IsKindOf (pClass))
				return pView;
		}
	}

	return NULL;
}

CBoxWriter * CHostApp::GetBwCtrl () const
{
	if (CHeadView * pView = (CHeadView *)GetView (RUNTIME_CLASS (CHeadView)))
		if (CBoxWriter * pCtrl = (CBoxWriter *)pView->GetDlgItem (IDC_BW)) 
			return pCtrl;

	return NULL;
}

