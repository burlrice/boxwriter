// stdafx.cpp : source file that includes just the standard includes
//	Host.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"


CString LoadString (UINT nID)
{
	HINSTANCE hInstance = theApp.m_hInstance;
	HINSTANCE hInst = ::AfxGetResourceHandle ();
	CString str;

	ASSERT (hInstance);
	::AfxSetResourceHandle (hInstance);

	BOOL bLoad = str.LoadString (nID);
	ASSERT (bLoad);
	
	::AfxSetResourceHandle (hInst);

	return str;
}


