#include "stdafx.h"
#include "PrinterRecordset.h"
#include "Database.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;

static const LPCTSTR lpszTable		= _T ("Printers");
static const LPCTSTR lpszID			= _T ("ID");
static const LPCTSTR lpszName		= _T ("Name");
static const LPCTSTR lpszAddress	= _T ("Address");

const FoxjetDatabase::COdbcRecordset & operator >> (const FoxjetDatabase::COdbcRecordset & rst, PRINTERSTRUCT & s)
{
	try {
		s.m_lID				= (long)rst.GetFieldValue (lpszID);
		s.m_strName			= (CString)rst.GetFieldValue (lpszName);
		s.m_strAddress		= (CString)rst.GetFieldValue (lpszAddress);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

FoxjetDatabase::COdbcRecordset & operator << (FoxjetDatabase::COdbcRecordset & rst, const PRINTERSTRUCT & s)
{
	try {
		rst.SetFieldValue (lpszName,		s.m_strName);
		rst.SetFieldValue (lpszAddress,		s.m_strAddress);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

bool AddPrinterRecord (FoxjetDatabase::COdbcDatabase & database, PRINTERSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	int nField = 1;

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s]"), lpszTable);
		rst.Open (str, CRecordset::dynaset);
		rst.AddNew ();
		rst << s;
		rst.Update ();
		rst.MoveLast ();
		s.m_lID = (long)rst.GetFieldValue (lpszID);
		rst.Close ();
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

bool UpdatePrinterRecord (FoxjetDatabase::COdbcDatabase & database, const PRINTERSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u"), 
			lpszTable, lpszID, s.m_lID);
		rst.Open (str, CRecordset::dynaset);
		rst.Edit ();
		rst << s;
		rst.Update();
		rst.Close ();
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

bool GetPrinterRecords (FoxjetDatabase::COdbcDatabase & database, CArray <PRINTERSTRUCT, PRINTERSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	v.RemoveAll ();

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s]"), lpszTable);
		rst.Open (str);

		while (!rst.IsEOF ()) {
			PRINTERSTRUCT s;
			rst >> s;
			v.Add (s);
			rst.MoveNext ();
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return v.GetSize () > 0;
}

bool DeletePrinterRecord (FoxjetDatabase::COdbcDatabase & database, ULONG lID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	CString str;
	
	str.Format (
		_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
		lpszTable, lpszID, lID);

	try {
		database.ExecuteSQL (str);
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}
