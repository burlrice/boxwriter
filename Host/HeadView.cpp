// HeadView.cpp : implementation file
//

#include "stdafx.h"
#include "Host.h"
#include "HostDoc.h"
#include "HeadView.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHeadView

IMPLEMENT_DYNCREATE(CHeadView, CFormView)

CHeadView::CHeadView()
	: CFormView(CHeadView::IDD)
{
	//{{AFX_DATA_INIT(CHeadView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CHeadView::~CHeadView()
{
}

void CHeadView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHeadView)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHeadView, CFormView)
	//{{AFX_MSG_MAP(CHeadView)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHeadView diagnostics

#ifdef _DEBUG
void CHeadView::AssertValid() const
{
	CFormView::AssertValid();
}

void CHeadView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CHeadView message handlers

void CHeadView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
		
	if (CListCtrl * p = (CListCtrl *)GetDlgItem (LV_HEADS))
		p->SetWindowPos (NULL, 0, 0, cx, cy, SWP_NOZORDER);
}


void CHeadView::OnInitialUpdate() 
{
	using namespace ItiLibrary;
	using CListCtrlImp::CColumn;

	CArray <CColumn, CColumn> vCols;

	CFormView::OnInitialUpdate();
	
	vCols.Add (CColumn (LoadString (IDS_PANEL)));
	vCols.Add (CColumn (LoadString (IDS_NAME)));
	vCols.Add (CColumn (LoadString (IDS_TASK)));
	vCols.Add (CColumn (LoadString (IDS_COUNT)));
	vCols.Add (CColumn (LoadString (IDS_STATE)));
	vCols.Add (CColumn (LoadString (IDS_STATUS)));

	m_lv.Create (LV_HEADS, vCols, this, _T ("Software\\FoxJet\\Host"));
	m_lv->SetExtendedStyle (m_lv->GetExtendedStyle () | LVS_EX_GRIDLINES);
}
