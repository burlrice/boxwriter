// ImportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "Host.h"
#include "ImportDlg.h"
#include "TemplExt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImportDlg dialog


CImportDlg::CImportDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CImportDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CImportDlg)
	m_bConfig = FALSE;
	m_bReports = FALSE;
	m_bTasks = TRUE;
	m_bUser = FALSE;
	//}}AFX_DATA_INIT
}


void CImportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CImportDlg)
	DDX_Check(pDX, CHK_CONFIG, m_bConfig);
	DDX_Check(pDX, CHK_REPORTS, m_bReports);
	DDX_Check(pDX, CHK_TASKS, m_bTasks);
	DDX_Check(pDX, CHK_USER, m_bUser);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CImportDlg, CDialog)
	//{{AFX_MSG_MAP(CImportDlg)
	ON_BN_CLICKED(CHK_ALL, OnAll)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImportDlg message handlers

void CImportDlg::OnAll() 
{
	CButton * pAll = (CButton *)GetDlgItem (CHK_ALL);
	UINT nID [] = 
	{
		CHK_CONFIG,
		CHK_REPORTS,
		CHK_TASKS,
		CHK_USER,
	};

	ASSERT (pAll);

	bool bAll = pAll->GetCheck () == 1;

	if (bAll) {
		m_bConfig	= TRUE;
		m_bReports	= TRUE;
		m_bTasks	= TRUE;
		m_bUser		= TRUE;
		
		UpdateData (FALSE);
	}

	for (int i = 0; i < ARRAYSIZE (nID); i++)
		if (CWnd * p = GetDlgItem (nID [i]))
			p->EnableWindow (!bAll);
}
