// StrArray.h: Definition of the CStrArray class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STRARRAY_H__7C359593_E859_46DC_A34A_48E3F001ED53__INCLUDED_)
#define AFX_STRARRAY_H__7C359593_E859_46DC_A34A_48E3F001ED53__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CStrArray

class CStrArray : 
	public IDispatchImpl<IStringArray, &IID_IStringArray, &LIBID_BoxWriter>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<CStrArray,&CLSID_StringArray>
{
public:
	CStrArray() {}
BEGIN_COM_MAP(CStrArray)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IStringArray)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(CStrArray) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_StringArray)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IStringArray
public:
	STDMETHOD(ToString)(BSTR * str);
	STDMETHOD(FromString)(BSTR str);
	STDMETHOD(RemoveAll)();
	STDMETHOD(Remove)(int nIndex);
	STDMETHOD(InsertAt)(int nIndex, BSTR str);
	STDMETHOD(Add)(BSTR str);
	STDMETHOD(SetAt)(int nIndex, BSTR str);
	CStringArray m_v;
	STDMETHOD(get_GetAt)(int nIndex, /*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_Count)(/*[out, retval]*/ int *pVal);
};

#endif // !defined(AFX_STRARRAY_H__7C359593_E859_46DC_A34A_48E3F001ED53__INCLUDED_)
