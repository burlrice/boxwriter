#if !defined(AFX_ERRORTHREAD_H__CDC0E2CC_70B2_4E64_8E80_FB5F20604F1F__INCLUDED_)
#define AFX_ERRORTHREAD_H__CDC0E2CC_70B2_4E64_8E80_FB5F20604F1F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ErrorThread.h : header file
//

#include "ErrorSocket.h"

/////////////////////////////////////////////////////////////////////////////
// CErrorThread thread

class CErrorThread : public CWinThread
{
	DECLARE_DYNCREATE(CErrorThread)
protected:
	CErrorThread();           // protected constructor used by dynamic creation

// Attributes
public:

	CErrorSocket m_sock;
	CPrinter * m_pPrinter;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CErrorThread)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CErrorThread();

	afx_msg void OnKillThread (WPARAM wParam, LPARAM lParam);
	afx_msg void OnError (WPARAM wParam, LPARAM lParam);
	afx_msg void OnErrorCleared (WPARAM wParam, LPARAM lParam);

	// Generated message map functions
	//{{AFX_MSG(CErrorThread)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ERRORTHREAD_H__CDC0E2CC_70B2_4E64_8E80_FB5F20604F1F__INCLUDED_)
