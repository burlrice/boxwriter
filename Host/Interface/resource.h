//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Interface.rc
//
#define IDS_BOXWRITER                   1
#define IDB_BOXWRITER                   1
#define IDS_BOXWRITER_PPG               2
#define IDR_INTERFACE                   102
#define IDS_PRINTER_DESC                103
#define IDR_Printer                     104
#define IDS_LINE_DESC                   105
#define IDR_Line                        106
#define IDS_STRINGARRAY_DESC            107
#define IDR_StringArray                 108
#define IDS_HEAD_DESC                   109
#define IDR_Head                        110
#define IDS_BOXWRITER_PPG_CAPTION       200
#define IDD_PROPPAGE_BOXWRITER          200
#define IDB_BOXWRITER_CONTROL           205

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        206
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           111
#endif
#endif
