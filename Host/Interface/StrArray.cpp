// StrArray.cpp : Implementation of CInterfaceApp and DLL registration.

#include "stdafx.h"
#include "Interface.h"
#include "StrArray.h"
#include "Utils.h"
#include "Parse.h"

/////////////////////////////////////////////////////////////////////////////
//

STDMETHODIMP CStrArray::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IStringArray,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CStrArray::get_Count(int *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (pVal);

	* pVal = m_v.GetSize ();

	return S_OK;
}

STDMETHODIMP CStrArray::get_GetAt(int nIndex, BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (pVal);

	if (nIndex >= 0 && nIndex < m_v.GetSize ())
		* pVal = m_v [nIndex].AllocSysString ();

	return S_OK;
}

STDMETHODIMP CStrArray::SetAt(int nIndex, BSTR str)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if (nIndex >= 0 && nIndex < m_v.GetSize ())
		m_v.SetAt (nIndex, (LPCTSTR)_bstr_t (str));

	return S_OK;
}

STDMETHODIMP CStrArray::Add(BSTR str)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_v.Add ((LPCTSTR)_bstr_t (str));

	return S_OK;
}

STDMETHODIMP CStrArray::InsertAt(int nIndex, BSTR str)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_v.InsertAt (nIndex, (LPCTSTR)_bstr_t (str));

	return S_OK;
}

STDMETHODIMP CStrArray::Remove(int nIndex)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if (nIndex >= 0 && nIndex < m_v.GetSize ())
		m_v.RemoveAt (nIndex);

	return S_OK;
}

STDMETHODIMP CStrArray::RemoveAll()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	m_v.RemoveAll ();

	return S_OK;
}

STDMETHODIMP CStrArray::FromString(BSTR bstr)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString str = (LPCTSTR)_bstr_t (bstr);

	str.TrimLeft ();
	str.TrimRight ();

	if (str.GetLength () && str [0] == '{')
		str.Delete (0);

	if (str.GetLength () && str [str.GetLength () - 1] == '}')
		str.Delete (str.GetLength () - 1);

	m_v.RemoveAll ();
	FoxjetDatabase::FromString (str, m_v);

	return S_OK;
}

STDMETHODIMP CStrArray::ToString(BSTR *str)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if (str)
		* str = FoxjetDatabase::ToString (m_v).AllocSysString ();

	return S_OK;
}
