#if !defined(AFX_BOXWRITERCTL_H__ECD9F934_B21C_427C_8722_B2CD2B26DBF7__INCLUDED_)
#define AFX_BOXWRITERCTL_H__ECD9F934_B21C_427C_8722_B2CD2B26DBF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxsock.h>

#include "Line.h"

// BoxWriterCtl.h : Declaration of the CBoxWriterCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CBoxWriterCtrl : See BoxWriterCtl.cpp for implementation.

class CBoxWriterCtrl : public COleControl
{
	DECLARE_DYNCREATE(CBoxWriterCtrl)

// Constructor
public:
	CBoxWriterCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBoxWriterCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CBoxWriterCtrl();

	DECLARE_OLECREATE_EX(CBoxWriterCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CBoxWriterCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CBoxWriterCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CBoxWriterCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CBoxWriterCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CBoxWriterCtrl)
	afx_msg BSTR Locate(long lTimeout);
	afx_msg void SetWarnings(long bWarningsOn);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

public:
// Event maps
	//{{AFX_EVENT(CBoxWriterCtrl)
	void FireOnError(LPCTSTR strAddress, LPCTSTR strLine)
		{FireEvent(eventidOnError,EVENT_PARAM(VTS_BSTR  VTS_BSTR), strAddress, strLine);}
	void FireOnErrorCleared(LPCTSTR strAddress, LPCTSTR strLine)
		{FireEvent(eventidOnErrorCleared,EVENT_PARAM(VTS_BSTR  VTS_BSTR), strAddress, strLine);}
	void FireOnEOP(long lHeadID, unsigned __int64  lCount)
		{FireEvent(eventidOnEOP,EVENT_PARAM(VTS_I4  VTS_UI8), lHeadID, lCount);}
	void FireOnSOP(long lHeadID, unsigned __int64  lCount)
		{FireEvent(eventidOnSOP,EVENT_PARAM(VTS_I4  VTS_UI8), lHeadID, lCount);}
	void FireOnHeadStateChanged(long lHeadID, LPCTSTR str)
		{FireEvent(eventidOnHeadStateChanged,EVENT_PARAM(VTS_I4 VTS_BSTR), lHeadID, str);}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CBoxWriterCtrl)
	dispidLocate = 1L,
	dispidSetWarnings = 2L,
	eventidOnError = 1L,
	eventidOnErrorCleared = 2L,
	eventidOnEOP = 3L,
	eventidOnSOP = 4L,
	eventidOnHeadStateChanged = 5L,
	//}}AFX_DISP_ID
	};

protected:

	HBITMAP m_hIcon;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BOXWRITERCTL_H__ECD9F934_B21C_427C_8722_B2CD2B26DBF7__INCLUDED)
