// BoxWriterPpg.cpp : Implementation of the CBoxWriterPropPage property page class.

#include "stdafx.h"
#include "Interface.h"
#include "BoxWriterPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CBoxWriterPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CBoxWriterPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CBoxWriterPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CBoxWriterPropPage, "INTERFACE.BoxWriterPropPage.1",
	0xedd76737, 0xb580, 0x4aab, 0x9a, 0x29, 0x73, 0x87, 0x2c, 0x19, 0xb0, 0xd5)


/////////////////////////////////////////////////////////////////////////////
// CBoxWriterPropPage::CBoxWriterPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CBoxWriterPropPage

BOOL CBoxWriterPropPage::CBoxWriterPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_BOXWRITER_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CBoxWriterPropPage::CBoxWriterPropPage - Constructor

CBoxWriterPropPage::CBoxWriterPropPage() :
	COlePropertyPage(IDD, IDS_BOXWRITER_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CBoxWriterPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CBoxWriterPropPage::DoDataExchange - Moves data between page and properties

void CBoxWriterPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CBoxWriterPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CBoxWriterPropPage message handlers
