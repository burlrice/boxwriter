// Head.h: Definition of the CHead class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HEAD_H__154CD891_7C91_4999_827E_A8C1D7F4B1FF__INCLUDED_)
#define AFX_HEAD_H__154CD891_7C91_4999_827E_A8C1D7F4B1FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols
#include "Database.h"

class CPrinter;
class CLine;

/////////////////////////////////////////////////////////////////////////////
// CHead

class CHead : 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<CHead,&CLSID_Head>,
	public IDispatchImpl<IHead, &IID_IHead, &LIBID_BoxWriter>
{
public:
	CHead();
BEGIN_COM_MAP(CHead)
//DEL 	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
	COM_INTERFACE_ENTRY2(IDispatch, IHead)
	COM_INTERFACE_ENTRY(IHead)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(CHead) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_Head)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IHead
public:
	STDMETHOD(GetCurrentState)(IStringArray * vState, TASKSTATE * pTask, /*[out, retval]*/ BOOL * pResult);
	STDMETHOD(UpdateElement)(BSTR strElement, /*[out, retval]*/ long * pResult);
	STDMETHOD(AddElement)(BSTR strElement, long * lID);
	STDMETHOD(DeleteElement)(long lIndex);
	STDMETHOD(GetElements)(IStringArray * vElements, /*[out, retval]*/ BOOL * pResult);

	FoxjetDatabase::HEADSTRUCT m_head;
	CPrinter * m_pPrinter;
	CLine * m_pLine;

// IHead
	STDMETHOD(get_m_lID)(long * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_lID;
		return S_OK;
	}
	STDMETHOD(get_m_lPanelID)(long * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_lPanelID;
		return S_OK;
	}
	STDMETHOD(get_m_strName)(BSTR * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_strName.AllocSysString ();
		return S_OK;
	}
	STDMETHOD(get_m_strUID)(BSTR * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_strUID.AllocSysString ();
		return S_OK;
	}
	STDMETHOD(get_m_lRelativeHeight)(long * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_lRelativeHeight;
		return S_OK;
	}
	STDMETHOD(get_m_nChannels)(int * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_nChannels;
		return S_OK;
	}
	STDMETHOD(get_m_nHorzRes)(int * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_nHorzRes;
		return S_OK;
	}
	STDMETHOD(get_m_lPhotocellDelay)(long * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_lPhotocellDelay;
		return S_OK;
	}
	STDMETHOD(get_m_nEncoder)(ENCODERS * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = (ENCODERS)m_head.m_nEncoder;
		return S_OK;
	}
	STDMETHOD(get_m_nDirection)(DIRECTION * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = (DIRECTION)m_head.m_nDirection;
		return S_OK;
	}
	STDMETHOD(get_m_bEnabled)(BOOL * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = (BOOL)m_head.m_bEnabled;
		return S_OK;
	}
	STDMETHOD(get_m_bInverted)(BOOL * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_bInverted;
		return S_OK;
	}
	STDMETHOD(get_m_dHeadAngle)(DOUBLE * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_dHeadAngle;
		return S_OK;
	}
	STDMETHOD(get_m_nPhotocell)(PHOTOCELLS * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = (PHOTOCELLS)m_head.m_nPhotocell;
		return S_OK;
	}
	STDMETHOD(get_m_nSharePhoto)(SHAREDPHOTOCELLS * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = (SHAREDPHOTOCELLS)m_head.m_nSharePhoto;
		return S_OK;
	}
	STDMETHOD(get_m_nShareEnc)(SHAREDENCODERS * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = (SHAREDENCODERS)m_head.m_nShareEnc;
		return S_OK;
	}
	STDMETHOD(get_m_type)(HEADTYPE * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = (HEADTYPE)m_head.m_nHeadType;
		return S_OK;
	}
	STDMETHOD(get_m_nInternalTachSpeed)(int * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_nIntTachSpeed;
		return S_OK;
	}
	STDMETHOD(get_m_dNozzleSpan)(DOUBLE * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_dNozzleSpan;
		return S_OK;
	}
	STDMETHOD(get_m_dPhotocellRate)(DOUBLE * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_dPhotocellRate;
		return S_OK;
	}
	STDMETHOD(get_m_nEncoderDivisor)(INT * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_nEncoderDivisor;
		return S_OK;
	}
	STDMETHOD(get_m_bDoublePulse)(BOOL * pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;
			
		* pVal = m_head.m_bDoublePulse;
		return S_OK;
	}
	STDMETHODIMP CHead::get_m_bRemoteHead(long *pVal)
	{
		AFX_MANAGE_STATE(AfxGetStaticModuleState())

		if (pVal == NULL)
			return E_POINTER;

		* pVal = (BOOL)m_head.m_bRemoteHead;
		return S_OK;
	}
};

#endif // !defined(AFX_HEAD_H__154CD891_7C91_4999_827E_A8C1D7F4B1FF__INCLUDED_)
