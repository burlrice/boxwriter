#if !defined(AFX_INTERFACE_H__BC32F5D5_922E_4CDD_888F_ABD11A4237DE__INCLUDED_)
#define AFX_INTERFACE_H__BC32F5D5_922E_4CDD_888F_ABD11A4237DE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Interface.h : main header file for INTERFACE.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols
#include "Interface_i.h"

class CPrinter;
class CBoxWriterCtrl;

/////////////////////////////////////////////////////////////////////////////
// CInterfaceApp : See Interface.cpp for implementation.

class CInterfaceApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();

	int Register (CBoxWriterCtrl * p);
	int Register (CPrinter * p);

	bool Unregister (CBoxWriterCtrl * p);
	bool Unregister (CPrinter * p);

	void OnPrinterError (const CPrinter * pPrinter, ULONG lLineID, bool bError);
	void OnSOP (ULONG lHeadID, unsigned __int64 lCount);
	void OnEOP (ULONG lHeadID, unsigned __int64 lCount);
	void OnHeadStateChanged (ULONG lHeadID, LPCTSTR str);

	const CPrinter * FindPrinter (const CString & strAddress) const;

private:
	BOOL InitATL();

	CArray <CPrinter *, CPrinter *> m_vPrinters;
	CArray <CBoxWriterCtrl *, CBoxWriterCtrl *> m_vCtrls;
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INTERFACE_H__BC32F5D5_922E_4CDD_888F_ABD11A4237DE__INCLUDED)
