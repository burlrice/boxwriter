#if !defined(AFX_BOXWRITERPPG_H__7BFF8C01_4276_4D03_BFBC_5921C1D94A07__INCLUDED_)
#define AFX_BOXWRITERPPG_H__7BFF8C01_4276_4D03_BFBC_5921C1D94A07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// BoxWriterPpg.h : Declaration of the CBoxWriterPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CBoxWriterPropPage : See BoxWriterPpg.cpp.cpp for implementation.

class CBoxWriterPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CBoxWriterPropPage)
	DECLARE_OLECREATE_EX(CBoxWriterPropPage)

// Constructor
public:
	CBoxWriterPropPage();

// Dialog Data
	//{{AFX_DATA(CBoxWriterPropPage)
	enum { IDD = IDD_PROPPAGE_BOXWRITER };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CBoxWriterPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BOXWRITERPPG_H__7BFF8C01_4276_4D03_BFBC_5921C1D94A07__INCLUDED)
