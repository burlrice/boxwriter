// Line.cpp : Implementation of CInterfaceApp and DLL registration.

#include "stdafx.h"
#include "Interface.h"
#include "Line.h"
#include "Printer.h"
#include "Parse.h"
#include "Head.h"
#include <tchar.h>

#include <comdef.h>

extern CInterfaceApp NEAR theApp;

/////////////////////////////////////////////////////////////////////////////
//

CLine::CLine ()
:	m_strName ("[default line]"),
	m_pPrinter (NULL),
	m_lID (-1)
{
}

CLine::~CLine ()
{
	for (int i = 0; i < m_vHeads.GetSize (); i++) {
		if (CHead * p = m_vHeads [i]) {
			p->Release ();
			//delete p;
		}
	}

	m_vHeads.RemoveAll ();
}

STDMETHODIMP CLine::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_ILine,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CLine::get_m_strName(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	* pVal = m_strName.AllocSysString ();

	return S_OK;
}

STDMETHODIMP CLine::Stop(/*[out, retval]*/ BOOL * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);

	CStringArray vCmd, vResponse;

	if (pResult) * pResult = FALSE;

	vCmd.Add (BW_HOST_PACKET_STOP_TASK);
	vCmd.Add (m_strName);

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		TRACEF ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (vResponse.GetSize () >= 3) 
		if (pResult)
			* pResult = _ttoi (vResponse [2]) ? true : false;

	if (!(* pResult)) 
		TRACEF (m_strName + ": Stop failed");

	return S_OK;
}

STDMETHODIMP CLine::Idle(/*[out, retval]*/ BOOL * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);

	CStringArray vCmd, vResponse;

	if (pResult) * pResult = FALSE;

	vCmd.Add (BW_HOST_PACKET_IDLE_TASK);
	vCmd.Add (m_strName);

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		MSGBOX ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (vResponse.GetSize () >= 3) 
		if (pResult)
			* pResult = _ttoi (vResponse [2]) ? true : false;

	if (!(* pResult)) 
		TRACEF (m_strName + ": Idle failed");

	return S_OK;
}

STDMETHODIMP CLine::Resume(/*[out, retval]*/ BOOL * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);

	CStringArray vCmd, vResponse;

	if (pResult) * pResult = FALSE;

	vCmd.Add (BW_HOST_PACKET_RESUME_TASK);
	vCmd.Add (m_strName);

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		TRACEF ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (vResponse.GetSize () >= 3) 
		if (pResult)
			* pResult = _ttoi (vResponse [2]) ? true : false;

	if (!(* pResult)) 
		TRACEF (m_strName + ": Resume failed");

	return S_OK;
}

STDMETHODIMP CLine::GetCurrentTask(BSTR *strTask, /*[out, retval]*/ BOOL * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);

	CStringArray vCmd, vResponse;

	if (pResult) * pResult = FALSE;

	vCmd.Add (BW_HOST_PACKET_GET_CURRENT_TASK);
	vCmd.Add (m_strName);

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		TRACEF ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (vResponse.GetSize () >= 3) {
		* strTask = vResponse [2].AllocSysString ();

		if (pResult) * pResult = TRUE;
	}

	return S_OK;
}

STDMETHODIMP CLine::GetTasks(IStringArray * p, /*[out, retval]*/ long * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if (pResult) * pResult = FALSE;

	if (p) {
		p->RemoveAll ();

		CStringArray vCmd, vResponse;

		vCmd.Add (BW_HOST_PACKET_GET_TASKS);
		vCmd.Add (m_strName);

		TRACEARRAY (vCmd);
		
		if (!m_pPrinter->SendCmd (vCmd, vResponse))
			TRACEF ("SendCmd failed: " + vCmd [0]);

		TRACEARRAY (vResponse);

		for (int i = 2; i < vResponse.GetSize (); i++) {
			p->Add (vResponse [i].AllocSysString ());

			if (pResult) 
				(* pResult)++;
		}
	}

	return S_OK;
}

STDMETHODIMP CLine::get_m_lCount(unsigned __int64 *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);

	CStringArray vCmd, vResponse;

	vCmd.Add (BW_HOST_PACKET_GET_COUNT);
	vCmd.Add (m_strName);

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		MSGBOX ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (vResponse.GetSize () >= 3) 
		* pVal = _tcstoui64 (vResponse [2], NULL, 10);

	return S_OK;
}

STDMETHODIMP CLine::put_m_lCount(unsigned __int64 newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);

	bool bResult = true;
	CStringArray vCmd, vResponse;

	vCmd.Add (BW_HOST_PACKET_SET_COUNT);
	vCmd.Add (m_strName);
	vCmd.Add (FoxjetDatabase::ToString (newVal));

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		MSGBOX ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (vResponse.GetSize () >= 3) 
		bResult = _ttol (vResponse [2]) ? true : false;

	if (!bResult) 
		MSGBOX (m_strName + ": [Set] m_lCount failed");

	return S_OK;
}

CComObject <CHead> * CLine::GetHead (long lIndex)
{
	if (lIndex >= 0 && lIndex < m_vHeads.GetSize ())
		return m_vHeads [lIndex];

	return NULL;
}

STDMETHODIMP CLine::get_m_head(long lIndex, IHead **pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CComObject <CHead>* pHead = GetHead (lIndex);

	if (!pHead) {
		#ifdef _DEBUG
		CString str;
		str.Format (_T ("m_head(\"%d\") not found"), lIndex);
		MSGBOX (str);
		#endif

		return E_INVALIDARG;
	}

	pHead->AddRef ();
	*pVal = pHead;

	return S_OK;
}


STDMETHODIMP CLine::get_m_nHeads(long *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if (pVal)
		* pVal = m_vHeads.GetSize ();

	return S_OK;
}

STDMETHODIMP CLine::Load(BSTR bstrTask, /*[out, retval]*/ BOOL * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);

	CStringArray vCmd, vResponse;
	CString strTask = (LPCTSTR)_bstr_t (bstrTask);

	if (pResult) * pResult = FALSE;

	vCmd.Add (BW_HOST_PACKET_LOAD_TASK);
	vCmd.Add (m_strName);
	vCmd.Add (strTask);

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		TRACEF ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (vResponse.GetSize () >= 3) 
		if (pResult)
			* pResult = _ttoi (vResponse [2]) ? true : false;

	if (!(* pResult)) 
		TRACEF (m_strName + ": Load failed: " + strTask);

	return S_OK;
}

STDMETHODIMP CLine::Start(/*[out, retval]*/ BOOL * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);

	CStringArray vCmd, vResponse;

	if (pResult) * pResult = FALSE;

	vCmd.Add (BW_HOST_PACKET_START_TASK);
	vCmd.Add (m_strName);

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		TRACEF ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (vResponse.GetSize () >= 3) 
		if (pResult)
			* pResult = _ttoi (vResponse [2]) ? true : false;

	if (!(* pResult)) 
		TRACEF (m_strName + ": Start failed: " + m_strName);

	return S_OK;
}

STDMETHODIMP CLine::DatabaseStart(BSTR bstrKeyValue, /*[out, retval]*/ BOOL * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);

	CStringArray vCmd, vResponse;
	CString strKeyValue = (LPCTSTR)_bstr_t (bstrKeyValue);

	if (pResult) * pResult = FALSE;

	vCmd.Add (BW_HOST_PACKET_START_TASK_DATABASE);
	vCmd.Add (m_strName);
	vCmd.Add (strKeyValue);

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		TRACEF ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (vResponse.GetSize () >= 3) 
		if (pResult)
			* pResult = _ttoi (vResponse [2]) ? true : false;

	if (!(* pResult)) 
		TRACEF (m_strName + ": DatabaseStart failed: " + strKeyValue);

	return S_OK;
}


STDMETHODIMP CLine::GetUserElementData(IStringArray *vPrompt, IStringArray *vData, /*[out, retval]*/ BOOL * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);

	CStringArray vCmd, vResponse;

	if (pResult) * pResult = FALSE;

	if (!vPrompt || !vData)
		return E_POINTER;

	vCmd.Add (BW_HOST_PACKET_GET_USER_ELEMENTS);
	vCmd.Add (m_strName);

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		TRACEF ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	vPrompt->RemoveAll ();
	vData->RemoveAll ();

	for (int i = 2; i < vResponse.GetSize (); i += 2) {
		CString strPrompt = vResponse [i];
		CString strData;

		if ((i + 1) < vResponse.GetSize ())
			strData = vResponse [i + 1];

		vPrompt->Add (strPrompt.AllocSysString ());
		vData->Add (strData.AllocSysString ());

		if (pResult) * pResult = TRUE;
	}

	return S_OK;
}

STDMETHODIMP CLine::SetUserElementData(IStringArray *vPrompt, IStringArray *vData, /*[out, retval]*/ long * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);

	CStringArray vCmd, vResponse;

	if (pResult) * pResult = 0;

	if (!vPrompt || !vData)
		return E_POINTER;

	int nPrompt = 0;
	int nData = 0;

	vPrompt->get_Count (&nPrompt);
	vData->get_Count (&nData);

	vCmd.Add (BW_HOST_PACKET_SET_USER_ELEMENTS);
	vCmd.Add (m_strName);

	int nCount = min (nPrompt, nData);

	for (int i = 0; i < nCount; i++) {
		BSTR bstrPrompt, bstrData;

		vPrompt->get_GetAt (i, &bstrPrompt);
		vData->get_GetAt (i, &bstrData);

		vCmd.Add ((LPCTSTR)_bstr_t (bstrPrompt));
		vCmd.Add ((LPCTSTR)_bstr_t (bstrData));
	}

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		TRACEF ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (vResponse.GetSize () >= 3) {
		int nResult = _ttoi (vResponse [2]);

		if (nResult < nCount)
			TRACEF ("Failed to set all user elements");

			if (pResult) * pResult = nResult;
	}

	return S_OK;
}


STDMETHODIMP CLine::DatabaseStart(BSTR strKeyValue)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: Add your implementation code here

	return S_OK;
}


STDMETHODIMP CLine::get_m_strSerialData(BSTR* pVal)
{
	ASSERT (m_pPrinter);

	CStringArray vCmd, vResponse;

	vCmd.Add (BW_HOST_PACKET_GETSERIAL);
	vCmd.Add (m_strName);

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		TRACEF ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (vResponse.GetSize () >= 3) {
		* pVal = vResponse [2].AllocSysString ();
		return S_OK;
	}

	return E_INVALIDARG;
}


STDMETHODIMP CLine::put_m_strSerialData(BSTR newVal)
{
	ASSERT (m_pPrinter);

	CStringArray vCmd, vResponse;

	vCmd.Add (BW_HOST_PACKET_SETSERIAL);
	vCmd.Add (m_strName);
	vCmd.Add ((LPCTSTR)_bstr_t (newVal));

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		TRACEF ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (vResponse.GetSize () >= 3) {
		int nResult = _ttoi (vResponse [2]);

		if (nResult > 0)
			return S_OK;
	}

	return E_INVALIDARG;
}

//HRESULT CLine::QueryInterface (REFIID riid, LPVOID * ppvObj)
//{
//	 if (!ppvObj)
//		 return E_INVALIDARG;
//
//	 if (riid == IID_IUnknown ||
//		 riid == IID_ILine)
//	 {
//        *ppvObj = (LPVOID)this;
//        AddRef();
//        return NOERROR;
//    }
//
//	return E_NOINTERFACE;
//}