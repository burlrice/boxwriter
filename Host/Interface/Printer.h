// Printer.h: Definition of the CPrinter class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PRINTER_H__F8582A43_6971_4166_895E_BE81DF58B757__INCLUDED_)
#define AFX_PRINTER_H__F8582A43_6971_4166_895E_BE81DF58B757__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols

#include <afxtempl.h>
#include <afxsock.h>

#include "Line.h"
#include "ErrorThread.h"
#include "SerialThread.h"

class CBoxWriterCtrl;

/////////////////////////////////////////////////////////////////////////////
// CPrinter

class CPrinter : 
	public IDispatchImpl<IPrinter, &IID_IPrinter, &LIBID_BoxWriter>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<CPrinter,&CLSID_Printer>
{
	friend class CSerialErrorHandler;

public:
	CPrinter();
	virtual ~CPrinter ();

BEGIN_COM_MAP(CPrinter)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IPrinter)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(CPrinter) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_Printer)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IPrinter
public:
	STDMETHOD(Disconnect)();
	STDMETHOD(Connect)(BSTR strAddress, /*[out, retval]*/ BOOL * pResult);
	STDMETHOD(get_m_line)(BSTR strLine, /*[out, retval]*/ ILine* *pVal);

public:
	STDMETHOD(Refresh)();
	STDMETHOD(FromString)(IStringArray * v, long lFlags, long lTimeoutSeconds, /*[out, retval]*/ BOOL * pResult);
	STDMETHOD(ToString)(IStringArray * v, long lFlags, long lTimeoutSeconds, /*[out, retval]*/ BOOL * pResult);
	STDMETHOD(get_m_strAddress)(/*[out, retval]*/ BSTR *pVal);
	ULONG GetLineID (const CString & strLine) const;
	STDMETHOD(GetLines)(IStringArray * v);
	bool SendCmd (const CStringArray & vCmd, CStringArray & vResponse);

	CArray <CComObject <CLine> *, CComObject <CLine> *> m_vLines;
	CString m_strAddress;

	static const DWORD m_dwTimeout;
	static const LPCTSTR m_lpszLocalHost;

protected:

	void Free();
	HWND LocateControlWnd ();
	CComObject <CLine> * GetLine(LPCTSTR lpsz);

	CSocket * m_pSocket;
	CErrorThread * m_pErrorThread;
	CSerialThread * m_pSerialThread;

	HANDLE m_hFile;
	HANDLE m_hMapFile;
	LPVOID m_lpMapAddress;
	HWND m_hwndControl;
	HANDLE m_hComm;
};

#endif // !defined(AFX_PRINTER_H__F8582A43_6971_4166_895E_BE81DF58B757__INCLUDED_)
