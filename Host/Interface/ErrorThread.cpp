// ErrorThread.cpp : implementation file
//

#include "stdafx.h"
#include <process.h>
#include "Interface.h"
#include "ErrorThread.h"
#include "Debug.h"
#include "Printer.h"
#include "Head.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CErrorThread

IMPLEMENT_DYNCREATE(CErrorThread, CWinThread)

CErrorThread::CErrorThread()
:	m_sock (this),
	m_pPrinter (NULL)
{
}

CErrorThread::~CErrorThread()
{
}

BOOL CErrorThread::InitInstance()
{
	// TODO:  perform and per-thread initialization here
	return TRUE;
}

int CErrorThread::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CErrorThread, CWinThread)
	//{{AFX_MSG_MAP(CErrorThread)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_THREAD_MESSAGE (TM_ERROR_KILLTHREAD, OnKillThread)
	ON_THREAD_MESSAGE (TM_ERROR_RAISED, OnError)
	ON_THREAD_MESSAGE (TM_ERROR_CLEARED, OnErrorCleared)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CErrorThread message handlers

void CErrorThread::OnKillThread (WPARAM wParam, LPARAM lParam)
{
	delete this;
	_endthreadex (0); //::ExitThread (0);
}

void CErrorThread::OnError (WPARAM wParam, LPARAM lParam)
{
	ULONG lLineID = (ULONG)wParam;
	const CPrinter * pPrinter = (const CPrinter *)lParam;

	if (lParam == -1)
		pPrinter = theApp.FindPrinter (CPrinter::m_lpszLocalHost);

	ASSERT (pPrinter);

	theApp.OnPrinterError (pPrinter, lLineID, true);
}

void CErrorThread::OnErrorCleared (WPARAM wParam, LPARAM lParam)
{
	ULONG lLineID = (ULONG)wParam;
	const CPrinter * pPrinter = (const CPrinter *)lParam;

	if (lParam == -1)
		pPrinter = theApp.FindPrinter (CPrinter::m_lpszLocalHost);

	if (pPrinter)
		theApp.OnPrinterError (pPrinter, lLineID, false);
}