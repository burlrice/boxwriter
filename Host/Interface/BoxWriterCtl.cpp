// BoxWriterCtl.cpp : Implementation of the CBoxWriterCtrl ActiveX Control class.

#include "stdafx.h"
#include "Interface.h"
#include "BoxWriterCtl.h"
#include "BoxWriterPpg.h"
#include "Printer.h"
#include "Utils.h"
#include "Parse.h"
#include "Head.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CBoxWriterCtrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CBoxWriterCtrl, COleControl)
	//{{AFX_MSG_MAP(CBoxWriterCtrl)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)

END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CBoxWriterCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CBoxWriterCtrl)
	DISP_FUNCTION(CBoxWriterCtrl, "Locate", Locate, VT_BSTR, VTS_I4)
	DISP_FUNCTION(CBoxWriterCtrl, "SetWarnings", SetWarnings, VT_EMPTY, VTS_I4)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CBoxWriterCtrl, COleControl)
	//{{AFX_EVENT_MAP(CBoxWriterCtrl)
	EVENT_CUSTOM("OnError", FireOnError, VTS_BSTR  VTS_BSTR)
	EVENT_CUSTOM("OnErrorCleared", FireOnErrorCleared, VTS_BSTR  VTS_BSTR)
	EVENT_CUSTOM("OnEOP", FireOnEOP, VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnSOP", FireOnSOP, VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnHeadStateChanged", FireOnHeadStateChanged, VTS_I4 VTS_BSTR)
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CBoxWriterCtrl, 1)
	PROPPAGEID(CBoxWriterPropPage::guid)
END_PROPPAGEIDS(CBoxWriterCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CBoxWriterCtrl, "INTERFACE.BoxWriterCtrl.1",
	0x2e5d9215, 0x26ef, 0x4024, 0x8b, 0xac, 0x93, 0xc4, 0x5c, 0x8e, 0x6c, 0x9d)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CBoxWriterCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DBoxWriter =
		{ 0x49d715d3, 0x166, 0x4774, { 0xbf, 0xac, 0xf8, 0xe5, 0x7e, 0xe4, 0x82, 0xd7 } };
const IID BASED_CODE IID_DBoxWriterEvents =
		{ 0x667970bd, 0xec2, 0x41b2, { 0x99, 0xf6, 0x53, 0x28, 0x47, 0x9b, 0x50, 0xf4 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwBoxWriterOleMisc =
	OLEMISC_INVISIBLEATRUNTIME |
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CBoxWriterCtrl, IDS_BOXWRITER, _dwBoxWriterOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CBoxWriterCtrl::CBoxWriterCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CBoxWriterCtrl

BOOL CBoxWriterCtrl::CBoxWriterCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_BOXWRITER,
			IDB_BOXWRITER,
			afxRegApartmentThreading,
			_dwBoxWriterOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CBoxWriterCtrl::CBoxWriterCtrl - Constructor

CBoxWriterCtrl::CBoxWriterCtrl()
{
	InitializeIIDs(&IID_DBoxWriter, &IID_DBoxWriterEvents);
	theApp.Register (this);

	m_hIcon = (HBITMAP)::LoadImage (::AfxGetInstanceHandle (), 
		MAKEINTRESOURCE (IDB_BOXWRITER_CONTROL), IMAGE_BITMAP, 0, 0, 
		LR_DEFAULTSIZE | LR_LOADTRANSPARENT);
}


/////////////////////////////////////////////////////////////////////////////
// CBoxWriterCtrl::~CBoxWriterCtrl - Destructor

CBoxWriterCtrl::~CBoxWriterCtrl()
{
	theApp.Unregister (this);

	if (m_hIcon) {
		::DeleteObject (m_hIcon);
		m_hIcon = NULL;
	}
}


/////////////////////////////////////////////////////////////////////////////
// CBoxWriterCtrl::OnDraw - Drawing function

void CBoxWriterCtrl::OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	if (AmbientUserMode () == 0) {
		pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(LTGRAY_BRUSH)));

		if (CBitmap * pBmp = CBitmap::FromHandle (m_hIcon)) {
			DIBSECTION ds;
			CDC dcMem;

			::ZeroMemory (&ds, sizeof (ds));
			VERIFY (::GetObject (m_hIcon, sizeof (ds), &ds));
			CSize size (ds.dsBm.bmWidth, ds.dsBm.bmHeight);

			dcMem.CreateCompatibleDC (pdc);
			CBitmap * pMem = dcMem.SelectObject (pBmp);

			pdc->BitBlt (rcBounds.left, rcBounds.top, size.cx, size.cy, &dcMem, 0, 0, SRCCOPY);
			
			dcMem.SelectObject (pMem);
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// CBoxWriterCtrl::DoPropExchange - Persistence support

void CBoxWriterCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.

}


/////////////////////////////////////////////////////////////////////////////
// CBoxWriterCtrl::OnResetState - Reset control to default state

void CBoxWriterCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CBoxWriterCtrl message handlers



BSTR CBoxWriterCtrl::Locate(long lTimeout) 
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	using namespace FoxjetCommon;

	CAsyncSocket sock;
	int nFlag, nPort = BW_HOST_BROADCAST_PORT;
	int nSockType = SOCK_DGRAM;
	CString strFind;
	CStringArray v, vResult;

	lTimeout = BindTo (lTimeout, 1L, 30L);

	v.Add (BW_HOST_PACKET_LOCATE);
	CString str = FoxjetDatabase::ToString (v);

	if (!sock.Create (nPort, nSockType)) {
		nPort = 0;
		VERIFY (sock.Create (nPort, nSockType));
	}

	nFlag = 1;
	VERIFY (sock.SetSockOpt (SO_BROADCAST, &nFlag, sizeof (int)));

	nFlag = 1;
	VERIFY (sock.SetSockOpt (SO_REUSEADDR, &nFlag, sizeof (int)));

	SOCKADDR_IN remoteAddr;
	remoteAddr.sin_family = AF_INET;
	remoteAddr.sin_port = htons (BW_HOST_BROADCAST_PORT);
	remoteAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);

	DWORD dwBytes = sock.SendTo (w2a (str), str.GetLength (), (const SOCKADDR* )&remoteAddr, sizeof(remoteAddr));
	
	if (dwBytes == SOCKET_ERROR) {
		MSGBOX (FormatMessage (::WSAGetLastError ()));
		return CString ().AllocSysString ();
	}

	const CTime tmStart = CTime::GetCurrentTime ();
	CTime tmLast = CTime::GetCurrentTime ();

	while (1) {
		char sz [512] = { 0 };
		CString strSock;
		UINT nReceivePort = 0;
		CTimeSpan tm = tmLast - tmStart;
		CStringArray v;

		if (tm.GetTotalSeconds () >= lTimeout) 
			break;

		for (int i = 0; i < 2; i++) {
			ZeroMemory (sz, sizeof (sz));
			int nResult = sock.ReceiveFrom (sz, sizeof (sz), strSock, nReceivePort, 0); 

			if (nResult != SOCKET_ERROR) {
				TRACEF (sz);
				TRACEF (strSock);

				if (!FoxjetDatabase::Tokenize (a2w (sz), v).CompareNoCase (BW_HOST_PACKET_LOCATE)) {
					if (v.GetSize () >= 4) {
						CString str = strSock + ": " + a2w (sz);

						TRACEF (str);
						vResult.Add (strSock);
						break;
					}
				}
			}

		} 

		tmLast = CTime::GetCurrentTime ();
		::Sleep (0);
	}

	return FoxjetDatabase::ToString (vResult).AllocSysString();
}

void CBoxWriterCtrl::SetWarnings(long bWarningsOn) 
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	SetMsgBoxMode (bWarningsOn ? true : false);
}
