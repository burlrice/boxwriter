// socket.cpp : Defines the entry point for the console application.
//

#pragma warning(disable:4786)

#include <conio.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include "parse.h"

using namespace std;

int main(int argc, char* argv[])
{
	stringArray v, vTmp;

	v.push_back ("1");
	v.push_back ("2");
	v.push_back ("3");
	v.push_back ("{a,b,c}");

	string str = ToString (v);
	string strFormat = FormatString (str);
	string strUnformat = UnformatString (strFormat);

	cout << str << endl;
	cout << strFormat << endl;
	cout << strUnformat << endl;

	FromString (strUnformat, vTmp);

	for (int i = 0; i < vTmp.size (); i++) 
		cout << "\t" << vTmp [i] << endl;

	cout << "hit any key to exit..." << endl;
	getch ();

	return 0;
}
