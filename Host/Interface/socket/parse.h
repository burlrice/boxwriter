#ifndef __SOCKETDEMO_PARSE_H__
#define __SOCKETDEMO_PARSE_H__

#include <string>

#define ARRAYSIZE(s) (sizeof ((s)) / sizeof ((s) [0]))

typedef std::vector <std::string> stringArray;

std::string FormatString (const std::string & str);
std::string UnformatString (const std::string & str);

std::string ToString (const stringArray & v);
int FromString (const std::string & strInput, stringArray & v);

#endif //__SOCKETDEMO_PARSE_H__
