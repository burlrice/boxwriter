#include <conio.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include "parse.h"

#pragma warning(disable:4786)

using namespace std;

////////////////////////////////////////////////////////////////////////////////

typedef std::vector <int> intArray;

////////////////////////////////////////////////////////////////////////////////

string Tokenize (const string & strInput, stringArray & v, char cDelim = ',')
intArray CountChars (const string & str, char c)
string Trim (string str)

////////////////////////////////////////////////////////////////////////////////

string Trim (string str)
{
	for (int i = str.length () - 1; i >= 0; i--) {
		if (isspace (str [i]))
			str.erase (i, i + 1);
		else 
			break;
	}

	while (str.length ()) {
		if (isspace (str [0]))
			str.erase (0, 1);
		else
			break;
	}

	if (str.length () && str [0] == '{')
		str.erase (0, 1);

	int nLen = str.length ();

	if (str.length () > 2 && str [nLen - 2] != '\\' && str [nLen - 1] == '}')
		str.erase (str.length () - 1, str.length ());

	return str;
}

intArray CountChars (const string & str, char c)
{
	intArray index;

	for (int i = 0; i < str.length (); i++) {
		char cCur = str [i];

		if (cCur == '\\') {
			i++;
			continue;
		}
		else if (cCur == c) 
			index.push_back (i);
	}

	return index;
}

string Tokenize (const string & strInput, stringArray & v, char cDelim = ',')
{
	string str = Trim (strInput);
	intArray commas = CountChars (str, cDelim);
	int nIndex = 0;

	v.clear ();

	for (int i = 0; i < commas.size (); i++) {
		int nLen = commas [i] - nIndex;
		v.push_back (str.substr (nIndex, nLen));
		nIndex = commas [i] + 1;
	}

	string strRemainder = str.substr (nIndex);

	if (strRemainder.length ())
		v.push_back (strRemainder);
	else {
		if (strInput.length () && strInput [strInput.length () - 1] == cDelim)
			v.push_back ("");
	}

	return v.size () ? v [0] : "";
}

////////////////////////////////////////////////////////////////////////////////

std::string FormatString (const std::string & str)
{
	string strResult (str);
	char cDelim [] = { '\\', ',', '{', '}' }; // the '\' char must be first

	for (int i = 0; i < ARRAYSIZE (cDelim); i++) {
		int nIndex = 0;
	
		do {
			nIndex = strResult.find (cDelim [i], nIndex);

			if (nIndex != -1) {
				strResult.insert (nIndex, "\\");
				nIndex += 2;
			}
		}
		while (nIndex != -1);
	}

	return strResult;
}

std::string UnformatString (const std::string & str)
{
	string strResult;

	for (int i = 0; i < str.length (); i++) {
		if (str [i] == '\\') {
			i++;

			if (i >= str.length ())
				break;
		}

		strResult += str [i];
	}

	return strResult;
}

std::string ToString (const std::vector <std::string> & v) 
{
	string str;

	for (int i = 0; i < v.size (); i++) {
		str += FormatString (v [i]);

		if ((i + 1) < v.size ())
			str += ",";
	}

	return "{" + str + "}";
}

int FromString (const std::string & strInput, stringArray & v)
{
	const int nCount = v.size ();
	string str = Trim (strInput);

	Tokenize (str, v);

	for (int i = nCount; i < v.size (); i++) 
		v [i] = UnformatString (v [i]);

	return v.size () - nCount;
}
