// stdafx.cpp : source file that includes just the standard includes
//  stdafx.pch will be the pre-compiled header
//  stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
	#ifdef _ATL_STATIC_REGISTRY
#include <statreg.h>
#endif
//#include <atlimpl.cpp>

#include "Parse.h"

static bool bMsgBox = true;

void TraceArray (const CStringArray & v, LPCTSTR lpszFile, ULONG lLine)
{
#ifdef _DEBUG
	CDebug::Trace (FoxjetDatabase::ToString (v), true, lpszFile, lLine);
#endif
}

void SetMsgBoxMode (bool bOn)
{
	::bMsgBox = bOn;
}

void DoMsgBox (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
#ifdef _DEBUG
	CDebug::Trace (lpsz, true, lpszFile, lLine);
#endif //_DEBUG

//	if (::bMsgBox)
//		DbgMessageBox (lpsz, lpszFile, lLine);
}
