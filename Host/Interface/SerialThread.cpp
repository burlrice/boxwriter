// SerialThread.cpp : implementation file
//

#include "stdafx.h"
#include <process.h>
#include "Interface.h"
#include "SerialThread.h"
#include "Debug.h"
#include "Printer.h"
#include "Utils.h"
#include "Parse.h"
#include "Head.h"

using namespace FoxjetCommon;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSerialThread

IMPLEMENT_DYNCREATE(CSerialThread, CWinThread)

CSerialThread::CSerialThread()
:	m_hComm (NULL),
	m_hInit (NULL),
	m_hExit (NULL),
	m_hData (NULL),
	m_hMode (NULL),
	m_bErrorHandling (true),
	m_pPrinter (NULL)
{
}

CSerialThread::~CSerialThread()
{
}

BOOL CSerialThread::InitInstance()
{
	CString strEvent;

	strEvent.Format (_T ("%lu CSerialThread::Exit"), m_nThreadID);
	m_hExit = CreateEvent (NULL, true, false, strEvent);

	strEvent.Format (_T ("%lu CSerialThread::Data received"), m_nThreadID);
	m_hData = CreateEvent (NULL, true, false, strEvent);

	strEvent.Format (_T ("%lu CSerialThread::Mode change"), m_nThreadID);
	m_hMode = CreateEvent (NULL, true, false, strEvent);
	
	if (m_hInit)
		::SetEvent (m_hInit);

	return TRUE;
}

int CSerialThread::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CSerialThread, CWinThread)
	//{{AFX_MSG_MAP(CSerialThread)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerialThread message handlers

int CSerialThread::Run() 
{
	MSG msg;
	BYTE buffer [512];
	DWORD dwBytesRead = 0;
	ULONG lError = 0;
	OVERLAPPED os;
	CString strEvent;

	strEvent.Format (_T ("%lu CSerialThread::Read"), m_nThreadID);

	os.Offset		= 0;
	os.OffsetHigh	= 0;
	os.hEvent		= CreateEvent (NULL, true, false, strEvent);

	while (1) {
		while (::PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE))
			if (!PumpMessage())
				return ExitInstance();

		if (!m_hComm) {
			::Sleep (250);
			continue;
		}

		ZeroMemory (buffer, sizeof (buffer));
		::ResetEvent (os.hEvent);

		if (!ReadFile (m_hComm, buffer, sizeof (buffer), &dwBytesRead, &os)) {
			lError = ::GetLastError();

			if (lError != ERROR_IO_PENDING) {
				MESSAGEBOX (FormatMessage (lError));
				return -1;
			}
			else {
				HANDLE hEvents [] = { os.hEvent, m_hMode, m_hExit };

// TODO: rem //	switch(WaitForMultipleObjects (ARRAYSIZE (hEvents), hEvents, FALSE, INFINITE)) {
				switch(WaitForMultipleObjects (ARRAYSIZE (hEvents), hEvents, FALSE, 100)) {
					case WAIT_OBJECT_0:	
						::ResetEvent (os.hEvent);

						if (!::GetOverlappedResult (m_hComm, &os, &dwBytesRead, FALSE)) {
							if ((lError = ::GetLastError()) != ERROR_OPERATION_ABORTED) {
								MESSAGEBOX (FormatMessage (lError));
								return -2;
							}
						}
						else { // read completed successfully
							if (dwBytesRead) {
								CString str (a2w ((LPCSTR)buffer));

								m_strData += str;
								ProcessData ();
							}
						}
						break;

					case WAIT_OBJECT_0 + 1:	
						::ResetEvent (m_hMode);
						m_bErrorHandling = !m_bErrorHandling;
						break;
					case WAIT_OBJECT_0 + 2:	
						delete this;
						_endthreadex (0); //::ExitThread (0);
						return 0;
				}
			}
		}
		else { // read completed immediately
			if (dwBytesRead) {
				CString str (a2w ((LPCSTR)buffer));

				m_strData += str;
				ProcessData ();
			}
		}
	}

	return ExitInstance ();
}

CString CSerialThread::GetPendingData ()
{
	CString str = m_strData;

	m_strData.Empty ();
	
	return str;
}

void CSerialThread::ProcessData () 
{
	int nLeftDelim = FoxjetDatabase::CountChars (m_strData, '{').GetSize ();
	int nRightDelim = FoxjetDatabase::CountChars (m_strData, '}').GetSize ();
	bool bSetEvent = false;

	if (nLeftDelim == nRightDelim) {
		CStringArray vPacket;

		FoxjetDatabase::ParsePackets (m_strData, vPacket);
		m_strData.Empty ();

		for (int i = 0; i < vPacket.GetSize (); i++) {
			CStringArray v;
			CString strPacket = vPacket [i];

			FoxjetDatabase::FromString (strPacket, v);

			const CString strCmd = FoxjetDatabase::GetParam (v, 0);

			if (!strCmd.CompareNoCase (BW_HOST_PACKET_PRINTER_EOP)) {
				ULONG lHeadID	= _tcstoul (FoxjetDatabase::GetParam (v, 1), NULL, 10);
				ULONG lCount	= _tcstoul (FoxjetDatabase::GetParam (v, 2), NULL, 10);

				theApp.OnEOP (lHeadID, lCount);
				::Sleep (0);
			}
			else if (!strCmd.CompareNoCase (BW_HOST_PACKET_PRINTER_SOP)) {
				ULONG lHeadID	= _tcstoul (FoxjetDatabase::GetParam (v, 1), NULL, 10);
				ULONG lCount	= _tcstoul (FoxjetDatabase::GetParam (v, 2), NULL, 10);

				theApp.OnSOP (lHeadID, lCount);
				::Sleep (0);
			}
			else if (!strCmd.CompareNoCase (BW_HOST_PACKET_PRINTER_HEAD_STATE_CHANGE)) {
				ULONG lHeadID	= _tcstoul (FoxjetDatabase::GetParam (v, 1), NULL, 10);

				theApp.OnHeadStateChanged (lHeadID, FoxjetDatabase::ToString (vPacket));
				::Sleep (0);
			}
			else {
				bool bError = strCmd.CompareNoCase (BW_HOST_PACKET_CLEAR_ERROR) != 0;

				if (bError && m_bErrorHandling) {
					if (v.GetSize () >= 2) {
						CString strLine = v [1];
						ULONG lLineID = m_pPrinter->GetLineID (strLine);

						theApp.OnPrinterError (m_pPrinter, lLineID, bError);
					}
				}
				else {
					m_strData += (_T ("{") + strPacket + _T ("}"));
					bSetEvent = true;
				}
			}
		}
	}

	if (bSetEvent) {
		::SetEvent (m_hData);
		//TRACEF ("SetEvent (m_hData);");
	}
}

/* TODO: rem
void CSerialThread::ProcessData () 
{
	int nLeftDelim = FoxjetDatabase::CountChars (m_strData, '{').GetSize ();
	int nRightDelim = FoxjetDatabase::CountChars (m_strData, '}').GetSize ();

	if (nLeftDelim == nRightDelim) {
		CStringArray v;

		FoxjetDatabase::FromString (m_strData, v);
		m_strData.Empty ();

		const CString strCmd = FoxjetDatabase::GetParam (v, 0);

		if (m_bErrorHandling) {
			if (v.GetSize () >= 2) {
				CString strLine = v [1];
				ULONG lLineID = m_pPrinter->GetLineID (strLine);
				bool bError = strCmd.CompareNoCase (BW_HOST_PACKET_CLEAR_ERROR) != 0;

				theApp.OnPrinterError (m_pPrinter, lLineID, bError);
			}
		}
		else
			::SetEvent (m_hData);
	}
}
*/