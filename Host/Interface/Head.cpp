// Head.cpp : Implementation of CInterfaceApp and DLL registration.

#include "stdafx.h"
#include "Interface.h"
#include "Head.h"
#include "Printer.h"
#include "Line.h"
#include "Parse.h"

/////////////////////////////////////////////////////////////////////////////
//

CHead::CHead ()
:	m_pPrinter (NULL),
	m_pLine (NULL)
{
}

STDMETHODIMP CHead::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IHead,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CHead::GetElements(IStringArray *vElements, /*[out, retval]*/ BOOL * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);
	ASSERT (m_pLine);

	bool bResult = false;
	CStringArray vCmd, vResponse;

	if (pResult) * pResult = FALSE;

	vCmd.Add (BW_HOST_PACKET_GET_ELEMENTS);
	vCmd.Add (m_pLine->m_strName);
	vCmd.Add (FoxjetDatabase::ToString (m_head.m_lID));

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		TRACEF ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (vElements) {
		vElements->RemoveAll ();

		for (int i = 3; i < vResponse.GetSize (); i++)
			vElements->Add (vResponse [i].AllocSysString ());

		if (pResult) * pResult = TRUE;
	}

	return S_OK;
}

STDMETHODIMP CHead::DeleteElement(long lID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);
	ASSERT (m_pLine);

	bool bResult = false;
	CStringArray vCmd, vResponse;

	vCmd.Add (BW_HOST_PACKET_DELETE_ELEMENT);
	vCmd.Add (m_pLine->m_strName);
	vCmd.Add (FoxjetDatabase::ToString (m_head.m_lID));
	vCmd.Add (FoxjetDatabase::ToString (lID));

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		MSGBOX ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	return S_OK;
}

STDMETHODIMP CHead::AddElement(BSTR strElement, long * lID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);
	ASSERT (m_pLine);

	bool bResult = false;
	CStringArray vCmd, vResponse;

	vCmd.Add (BW_HOST_PACKET_ADD_ELEMENT);
	vCmd.Add (m_pLine->m_strName);
	vCmd.Add (FoxjetDatabase::ToString (m_head.m_lID));
	vCmd.Add ((LPCTSTR)_bstr_t (strElement));

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		MSGBOX ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (lID && vResponse.GetSize () >= 4) 
		* lID = _ttol (vResponse [3]);

	return S_OK;
}

STDMETHODIMP CHead::UpdateElement(BSTR strElement, /*[out, retval]*/ long * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);
	ASSERT (m_pLine);

	CStringArray vCmd, vResponse;

	if (pResult) * pResult = FALSE;

	vCmd.Add (BW_HOST_PACKET_UPDATE_ELEMENT);
	vCmd.Add (m_pLine->m_strName);
	vCmd.Add (FoxjetDatabase::ToString (m_head.m_lID));
	vCmd.Add ((LPCTSTR)_bstr_t (strElement));

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse))
		TRACEF ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (pResult)
		for (int i = 3; i < vResponse.GetSize (); i++)
			if (_ttoi (vResponse [i]) )
				(* pResult)++;

	return S_OK;
}

STDMETHODIMP CHead::GetCurrentState(IStringArray *vState, TASKSTATE *pTask, /*[out, retval]*/ BOOL * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	ASSERT (m_pPrinter);
	ASSERT (m_pLine);

	CStringArray vCmd, vResponse;

	if (pResult) * pResult = FALSE;

	vCmd.Add (BW_HOST_PACKET_GET_CURRENT_STATE);
	vCmd.Add (m_pLine->m_strName);
	vCmd.Add (FoxjetDatabase::ToString (m_head.m_lID));

	TRACEARRAY (vCmd);

	if (!m_pPrinter->SendCmd (vCmd, vResponse)) 
		TRACEF ("SendCmd failed: " + vCmd [0]);

	TRACEARRAY (vResponse);

	if (vResponse.GetSize () >= 5) {
		if (pTask)
			* pTask = (TASKSTATE)_ttol (vResponse [3]);

		if (vState) {
			CStringArray vTmp;

			vState->RemoveAll ();
			FoxjetDatabase::FromString (vResponse [4], vTmp);
			
			for (int i = 0; i < vTmp.GetSize (); i++)
				vState->Add (vTmp [i].AllocSysString ());
		}

		if (pResult) * pResult = TRUE;
	}

	return S_OK;
}

