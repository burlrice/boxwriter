// Interface.cpp : Implementation of CInterfaceApp and DLL registration.

#include "stdafx.h"
#include "Interface.h"
#include <initguid.h>
#include "Interface_i.c"
#include "Printer.h"
#include "Line.h"
#include "Head.h"
#include "StrArray.h"
#include "Printer.h"
#include "BoxWriterCtl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CInterfaceApp NEAR theApp;

const GUID CDECL BASED_CODE _tlid =
		{ 0xab255b27, 0xe756, 0x4dac, { 0xb0, 0x6f, 0x99, 0xe0, 0x49, 0x68, 0xd6, 0x63 } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;

////////////////////////////////////////////////////////////////////////////
// CInterfaceApp::InitInstance - DLL initialization

BOOL CInterfaceApp::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();
	if (!InitATL())
		return FALSE;


	if (bInit) {
		VERIFY (::AfxSocketInit (NULL));
	}

	return bInit;
}


////////////////////////////////////////////////////////////////////////////
// CInterfaceApp::ExitInstance - DLL termination

int CInterfaceApp::ExitInstance()
{
	// TODO: Add your own module termination code here.

	_Module.Term();

	return COleControlModule::ExitInstance();
}

int CInterfaceApp::Register (CBoxWriterCtrl * p)
{
	m_vCtrls.Add (p);

	return m_vCtrls.GetSize () - 1;
}

int CInterfaceApp::Register (CPrinter * p)
{
	m_vPrinters.Add (p);

	return m_vPrinters.GetSize () - 1;
}

bool CInterfaceApp::Unregister (CBoxWriterCtrl * p)
{
	for (int i = 0; i < m_vCtrls.GetSize (); i++) {
		if (m_vCtrls [i] == p) {
			m_vCtrls.RemoveAt (i);
			return true;
		}
	}

	return false;
}

bool CInterfaceApp::Unregister (CPrinter * p)
{
	for (int i = 0; i < m_vPrinters.GetSize (); i++) {
		if (m_vPrinters [i] == p) {
			m_vPrinters.RemoveAt (i);
			return true;
		}
	}

	return false;
}

void CInterfaceApp::OnPrinterError (const CPrinter * pPrinter, ULONG lLineID, bool bError)
{
 	CString str;

	ASSERT (pPrinter);

	for (int nLine = 0; nLine < pPrinter->m_vLines.GetSize (); nLine++) {
		CComObject <CLine> * pLine = pPrinter->m_vLines [nLine];

		if (pLine->m_lID == lLineID) {
			for (int nCtrl = 0; nCtrl < m_vCtrls.GetSize (); nCtrl++) {
				if (CBoxWriterCtrl * pCtrl = m_vCtrls [nCtrl]) {
					if (bError)
						pCtrl->FireOnError (pPrinter->m_strAddress, pLine->m_strName);
					else
						pCtrl->FireOnErrorCleared (pPrinter->m_strAddress, pLine->m_strName);
				}
			}

			return;
		}
	}

/* TODO: rem
 	CString str;

	for (int i = 0; i < m_vPrinters.GetSize (); i++) {
		if (CPrinter * pPrinter = m_vPrinters [i]) {
			for (int nLine = 0; nLine < pPrinter->m_vLines.GetSize (); nLine++) {
				CComObject <CLine> * pLine = pPrinter->m_vLines [nLine];

				if (pLine->m_lID == lLineID) {
					for (int nCtrl = 0; nCtrl < m_vCtrls.GetSize (); nCtrl++) {
						if (CBoxWriterCtrl * pCtrl = m_vCtrls [nCtrl]) {
							if (bError)
								pCtrl->FireOnError (pPrinter->m_strAddress, pLine->m_strName);
							else
								pCtrl->FireOnErrorCleared (pPrinter->m_strAddress, pLine->m_strName);
						}
					}

					return;
				}
			}
		}
	}
*/
}

void CInterfaceApp::OnSOP (ULONG lHeadID, unsigned __int64  lCount)
{
	for (int nCtrl = 0; nCtrl < m_vCtrls.GetSize (); nCtrl++) 
		if (CBoxWriterCtrl * pCtrl = m_vCtrls [nCtrl]) 
			pCtrl->FireOnSOP (lHeadID, lCount);
}

void CInterfaceApp::OnEOP (ULONG lHeadID, unsigned __int64 lCount)
{
	for (int nCtrl = 0; nCtrl < m_vCtrls.GetSize (); nCtrl++) 
		if (CBoxWriterCtrl * pCtrl = m_vCtrls [nCtrl]) 
			pCtrl->FireOnEOP (lHeadID, lCount);
}

void CInterfaceApp::OnHeadStateChanged (ULONG lHeadID, LPCTSTR str)
{
	for (int nCtrl = 0; nCtrl < m_vCtrls.GetSize (); nCtrl++) 
		if (CBoxWriterCtrl * pCtrl = m_vCtrls [nCtrl]) 
			pCtrl->FireOnHeadStateChanged (lHeadID, str);
}

/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return _Module.RegisterServer(TRUE);

	return NOERROR;
}


/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	_Module.UnregisterServer(TRUE); //TRUE indicates that typelib is unreg'd

	return NOERROR;
}

	
CComModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
OBJECT_ENTRY(CLSID_Printer, CPrinter)
OBJECT_ENTRY(CLSID_Line, CLine)
OBJECT_ENTRY(CLSID_StringArray, CStrArray)
OBJECT_ENTRY(CLSID_Head, CHead)
END_OBJECT_MAP()

STDAPI DllCanUnloadNow(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return (AfxDllCanUnloadNow()==S_OK && _Module.GetLockCount()==0) ? S_OK : S_FALSE;
}
/////////////////////////////////////////////////////////////////////////////
// Returns a class factory to create an object of the requested type
STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if(AfxDllGetClassObject(rclsid, riid, ppv) == S_OK)
		return S_OK;
	return _Module.GetClassObject(rclsid, riid, ppv);
}

BOOL CInterfaceApp::InitATL()
{
	_Module.Init(ObjectMap, AfxGetInstanceHandle());
	return TRUE;

}

const CPrinter * CInterfaceApp::FindPrinter (const CString & strAddress) const
{
	for (int i = 0; i < m_vPrinters.GetSize (); i++) 
		if (const CPrinter * p = m_vPrinters [i]) 
			if (!p->m_strAddress.CompareNoCase (strAddress))
				return p;

	return NULL;
}
