#if !defined(AFX_SERIALTHREAD_H__39FA175F_4260_474E_98B8_4F7FAFCD6EEF__INCLUDED_)
#define AFX_SERIALTHREAD_H__39FA175F_4260_474E_98B8_4F7FAFCD6EEF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SerialThread.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CSerialThread thread

class CSerialThread : public CWinThread
{
	DECLARE_DYNCREATE(CSerialThread)
protected:
	CSerialThread();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialThread)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();
	//}}AFX_VIRTUAL

	CString GetPendingData ();
	bool IsErrorHandlingOn () const { return m_bErrorHandling; }

	HANDLE m_hComm;
	HANDLE m_hInit;
	HANDLE m_hExit;
	HANDLE m_hData;
	HANDLE m_hMode;
	CPrinter * m_pPrinter;

// Implementation
protected:
	virtual ~CSerialThread();

	// Generated message map functions
	//{{AFX_MSG(CSerialThread)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	void ProcessData ();

	CString m_strData;
	bool m_bErrorHandling;

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIALTHREAD_H__39FA175F_4260_474E_98B8_4F7FAFCD6EEF__INCLUDED_)
