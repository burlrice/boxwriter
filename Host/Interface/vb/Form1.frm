VERSION 5.00
Object = "{AB255B27-E756-4DAC-B06F-99E04968D663}#1.1#0"; "INTERF~1.OCX"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   4845
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6075
   LinkTopic       =   "Form1"
   ScaleHeight     =   4845
   ScaleWidth      =   6075
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnTest2 
      Caption         =   "Test 2"
      Height          =   495
      Left            =   4680
      TabIndex        =   18
      Top             =   2640
      Width           =   1335
   End
   Begin VB.Frame Frame2 
      Caption         =   "Address"
      Height          =   2295
      Left            =   4200
      TabIndex        =   14
      Top             =   120
      Width           =   1815
      Begin VB.CommandButton btnDisconnect 
         Caption         =   "Disconnect"
         Height          =   495
         Left            =   480
         TabIndex        =   17
         Top             =   1440
         Width           =   1095
      End
      Begin VB.CommandButton btnConnect 
         Caption         =   "Connect"
         Height          =   495
         Left            =   480
         TabIndex        =   16
         Top             =   840
         Width           =   1095
      End
      Begin VB.TextBox txtAddress 
         Height          =   375
         Left            =   480
         TabIndex        =   15
         Text            =   "192.168.2.4"
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.CommandButton btnTest 
      Caption         =   "Test"
      Height          =   495
      Left            =   4680
      TabIndex        =   13
      Top             =   4080
      Width           =   1335
   End
   Begin VB.CommandButton btnSet 
      Caption         =   "Set"
      Height          =   495
      Left            =   1560
      TabIndex        =   11
      Top             =   3960
      Width           =   1095
   End
   Begin VB.CommandButton btnGet 
      Caption         =   "Get"
      Height          =   495
      Left            =   120
      TabIndex        =   10
      Top             =   3960
      Width           =   1095
   End
   Begin VB.TextBox txtCount 
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   3480
      Width           =   2535
   End
   Begin VB.CommandButton btnLocate 
      Caption         =   "Locate..."
      Height          =   495
      Left            =   2880
      TabIndex        =   7
      Top             =   4080
      Width           =   1215
   End
   Begin VB.CheckBox chkError 
      Caption         =   "Error"
      Enabled         =   0   'False
      Height          =   255
      Left            =   2760
      TabIndex        =   6
      Top             =   2880
      Width           =   1215
   End
   Begin VB.TextBox txtTest 
      Height          =   375
      Left            =   4680
      TabIndex        =   5
      Text            =   "DATA"
      Top             =   3600
      Width           =   1335
   End
   Begin VB.CommandButton btnResume 
      Caption         =   "Resume"
      Height          =   495
      Left            =   2760
      TabIndex        =   4
      Top             =   2280
      Width           =   1095
   End
   Begin VB.CommandButton btnIdle 
      Caption         =   "Idle"
      Height          =   495
      Left            =   2760
      TabIndex        =   3
      Top             =   1680
      Width           =   1095
   End
   Begin VB.CommandButton btnStop 
      Caption         =   "Stop"
      Height          =   495
      Left            =   2760
      TabIndex        =   2
      Top             =   1080
      Width           =   1095
   End
   Begin VB.CommandButton btnStart 
      Caption         =   "Start"
      Height          =   495
      Left            =   2760
      TabIndex        =   1
      Top             =   480
      Width           =   1095
   End
   Begin VB.ListBox lbTask 
      Height          =   2595
      Left            =   120
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   480
      Width           =   2535
   End
   Begin BoxWriter.BoxWriter ctrlBW 
      Left            =   2880
      Top             =   3360
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   0
   End
   Begin VB.Frame Frame1 
      Caption         =   "Count"
      Height          =   1335
      Left            =   0
      TabIndex        =   9
      Top             =   3240
      Width           =   2775
   End
   Begin VB.Label Label1 
      Caption         =   "Tasks"
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   120
      Width           =   2535
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim p As BoxWriter.Printer

Private Sub BoxWriter1_OnErrorCleared(ByVal strAddress As String, ByVal strLine As String)
    chkError.Value = 0
End Sub

Private Sub btnConnect_Click()
    Dim v As BoxWriter.StringArray
    Dim lResult As Long
    
    Set v = New BoxWriter.StringArray

    If Len(p.m_strAddress) > 0 Then
        p.Disconnect
    End If
    
    lResult = p.Connect(txtAddress.Text)

    If (lResult = 0) Then
        MsgBox "Failed to connect: " & txtAddress.Text
    Else
        txtCount.Text = p.m_line.m_lCount
        p.m_line.GetTasks v
        txtAddress.Text = p.m_strAddress
    
        lbTask.Clear
        
        For i = 0 To v.Count - 1
            lbTask.AddItem v.GetAt(i)
        Next
    End If
End Sub

Private Sub btnDisconnect_Click()
    If Len(p.m_strAddress) Then
        p.Disconnect
        lbTask.Clear
        txtCount.Text = ""
        txtAddress.Text = ""
    End If
End Sub

Private Sub btnGet_Click()
    txtCount.Text = p.m_line.m_lCount
End Sub

Private Sub btnLocate_Click()
    Dim str As String
    Dim v As BoxWriter.StringArray
    
    Set v = New BoxWriter.StringArray
    str = ctrlBW.Locate(5)
    
    v.FromString (str)
    
    For i = 0 To v.Count - 1
        MsgBox v.GetAt(i)
    Next
End Sub

Private Sub btnSet_Click()
    p.m_line.m_lCount = CLng(txtCount.Text)
End Sub

Private Sub btnTest_Click()
    Dim str As String
    
    str = lbTask.List(lbTask.ListIndex)
    
    If (Len(str) = 0) Then
        MsgBox "Nothing selected"
    Else
        p.m_line.Load (str)
        
        If (Len(txtTest.Text) > 0) Then
            For i = 0 To p.m_line.m_nHeads - 1
                With p.m_line.m_head(i)
                    If (.m_bRemoteHead = 1) Then
                        .AddElement "{Text,""1"",0.000000,0,D,2,1,0,F,F,F,""ArialReg128""," & txtTest.Text & "}"
                    Else
                        .AddElement "{Text,2,4000,0,0,0,0,MK Arial,32,1,0," & txtTest.Text & ",0,0,0}"
                    End If
                End With
            Next
        End If
        
        p.m_line.Start
    End If
End Sub

Private Sub btnTest2_Click()
    Dim vPrompt As New BoxWriter.StringArray
    Dim vData As New BoxWriter.StringArray
    
    p.m_line.GetUserElementData vPrompt, vData
    
    For i = 0 To vPrompt.Count - 1
'        MsgBox vPrompt.GetAt(i) & ": " & vData.GetAt(i)
        
        vData.SetAt i, CStr(i + 1)
    Next
    
    p.m_line.SetUserElementData vPrompt, vData

    p.m_line.GetUserElementData vPrompt, vData
    
    For i = 0 To vPrompt.Count - 1
        MsgBox vPrompt.GetAt(i) & ": " & vData.GetAt(i)
    Next
End Sub

Private Sub ctrlBW_OnError(ByVal strAddress As String, ByVal strLine As String)
    chkError.Value = 1
End Sub

Private Sub ctrlBW_OnErrorCleared(ByVal strAddress As String, ByVal strLine As String)
    chkError.Value = 0
End Sub

Private Sub Form_Load()
    Set p = New BoxWriter.Printer
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Len(p.m_strAddress) Then
        p.Disconnect
    End If
    
    Set p = Nothing
End Sub

Private Sub btnIdle_Click()
    p.m_line.Idle
End Sub

Private Sub btnResume_Click()
    p.m_line.Resume
End Sub

Private Sub btnStart_Click()
    Dim str As String
    
    str = lbTask.List(lbTask.ListIndex)
    
    If (Len(str) = 0) Then
        MsgBox "Nothing selected"
    Else
        p.m_line.Load (str)
        p.m_line.Start
    End If
End Sub

Private Sub btnStop_Click()
    p.m_line.Stop
End Sub

Private Sub Command1_Click()

    Dim strTask As String
    Dim str As String
    Dim vLines As BoxWriter.StringArray
    Dim vTasks As BoxWriter.StringArray
    Dim v As BoxWriter.StringArray


'    Set vLines = New BoxWriter.StringArray
'    Set vTasks = New BoxWriter.StringArray
    Set v = New BoxWriter.StringArray

    p.m_line.Load "test"
    
    For i = 0 To p.m_line.m_nHeads - 1
        Dim nCount As Integer
        Dim lID As Long
        
        str = p.m_line.m_strName & vbCrLf & p.m_line.m_head(i).m_strName & vbCrLf
    
        p.m_line.m_head(i).GetElements v
        nCount = v.Count
        
'        If (nCount > 0) Then
'           For j = 0 To v.Count - 1
'                str = str & v.GetAt(j) & vbCrLf
'           Next
'        End If
'
'        MsgBox str
        
        '{Text,2,0,0,0,0,0,MK Arial,32,1,0,[VB TEXT],0,0,0}

        v.RemoveAll ' make sure v is empty
        v.Add "Text"
        v.Add "2"
        v.Add "0"
        v.Add "0"
        v.Add "0"
        v.Add "0"
        v.Add "0"
        v.Add "MK Arial"
        v.Add "32"
        v.Add "1"
        v.Add "0"
        v.Add "[VB TEXT]"
        v.Add "0"
        v.Add "0"
        v.Add "0"
            
        If (p.m_line.m_head(i).m_bEnabled = 1) Then
        
            
            v.ToString str
            p.m_line.m_head(i).AddElement str, lID
            
            p.m_line.m_head(i).UpdateElement "{Text,2,0,0,0,0,0,MK Arial,32,1,0,[2],0,0,0}"
            p.m_line.m_head(i).UpdateElement "{Text,9,0,0,0,0,0,MK Arial,32,1,0,[3],0,0,0}"
'            p.m_line.m_head(i).DeleteElement
        
            p.m_line.m_head(i).GetElements v
            
            For j = 0 To v.Count - 1
                MsgBox v.GetAt(j)
            Next
        End If
        
    Next

    p.m_line.Start
    
    
End Sub

