// Printer.cpp : Implementation of CInterfaceApp and DLL registration.

#include "stdafx.h"
#include "Interface.h"
#include "Printer.h"
#include "Utils.h" 
#include "StrArray.h"
#include "Head.h"
#include "Export.h"
#include "Debug.h"
#include "Parse.h"
#include "Utils.h"
#include "Comm32.h"

using namespace FoxjetCommon;

/////////////////////////////////////////////////////////////////////////////
//
class CSerialErrorHandler
{
public:
	CSerialErrorHandler (CPrinter & printer, bool bState)
	:	m_printer (printer),
		m_bState (bState)
	{
		SetSerialErrorHandler (m_bState);
	}
	virtual ~CSerialErrorHandler ()
	{
		SetSerialErrorHandler (!m_bState);
	}

	void SetSerialErrorHandler (bool bState)
	{
		ASSERT (m_printer.m_pSerialThread);

		if (m_printer.m_pSerialThread->IsErrorHandlingOn () != bState) {
			::SetEvent (m_printer.m_pSerialThread->m_hMode);

			while (m_printer.m_pSerialThread->IsErrorHandlingOn () != bState)
				::Sleep (0);
		}
	}

protected:
	CPrinter & m_printer;
	bool m_bState;
};

/////////////////////////////////////////////////////////////////////////////
//
const DWORD CPrinter::m_dwTimeout = 5000;
const LPCTSTR CPrinter::m_lpszLocalHost = _T ("[Local host]");

CPrinter::CPrinter ()
:	m_hFile (NULL),
	m_hMapFile (NULL),
	m_lpMapAddress (NULL),
	m_hwndControl (NULL),
	m_pSocket (NULL),
	m_pErrorThread (NULL),
	m_pSerialThread (NULL),
	m_hComm (NULL)
{
}

CPrinter::~CPrinter ()
{
	Free ();
}

STDMETHODIMP CPrinter::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IPrinter,
	};

	for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

CComObject<CLine> * CPrinter::GetLine(LPCTSTR lpsz)
{
	if (!_tcslen (lpsz) && m_vLines.GetSize ())
		return m_vLines [0];

	for (int i = 0; i < m_vLines.GetSize (); i++) {
		CComObject<CLine> * p = m_vLines [i];

		if (!p->m_strName.CompareNoCase (lpsz))
			return p;
	}

	if (m_vLines.GetSize ())
		return m_vLines [0];

	return NULL;
}

STDMETHODIMP CPrinter::get_m_line(BSTR bstrLine, ILine **pVal)
{ 
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString strLine = (LPCTSTR)_bstr_t (bstrLine);

	CComObject <CLine>* pLine = GetLine (strLine);

	if (!pLine) {
		#ifdef _DEBUG
		CString str;
		str.Format (_T ("m_line(\"%s\") not found"), strLine);
		MSGBOX (str);
		#endif

		return E_INVALIDARG;
	}

	pLine->AddRef ();
	* pVal = pLine;

	return S_OK;
}

STDMETHODIMP CPrinter::Connect(BSTR bstrAddress, /*[out, retval]*/ BOOL * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString strAddress = (LPCTSTR)_bstr_t (bstrAddress);

	if (pResult) * pResult = FALSE;

	Disconnect ();
	Free ();
	theApp.Register (this);

	if (!strAddress.GetLength ()) {
		m_hFile = ::CreateFile (COMMAPPEDFILE, GENERIC_READ | GENERIC_WRITE, 
			FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, (HANDLE)0);

		if (m_hFile != INVALID_HANDLE_VALUE)
			m_hMapFile = ::CreateFileMapping (m_hFile, NULL, 
				PAGE_READWRITE, 0, COMMAPPEDFILESIZE, COMMAPPEDFILE);
		else 
			m_hMapFile = ::OpenFileMapping (FILE_MAP_WRITE, TRUE, COMMAPPEDFILE);

		if (!m_hMapFile) {
			TRACEF (FormatMessage (GetLastError ()));
			return S_OK;
		}

		m_lpMapAddress = ::MapViewOfFile (m_hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, 0);
		
		if (!m_lpMapAddress) {
			TRACEF (FormatMessage (GetLastError ()));
			return S_OK;
		}

		m_hwndControl = LocateControlWnd ();
		
		if (!m_hwndControl) {
			TRACEF ("Connect failed\nControl app not running");
			return S_OK;
		}

		DWORD dw = 0;

		strAddress = m_lpszLocalHost;

		m_pErrorThread = (CErrorThread *)::AfxBeginThread (RUNTIME_CLASS (CErrorThread));
		m_pErrorThread->m_pPrinter = this;

		::SendMessageTimeout (m_hwndControl, WM_OPENMAPFILE, 0, (LPARAM)m_pErrorThread->m_nThreadID,  
			SMTO_BLOCK | SMTO_ABORTIFHUNG, m_dwTimeout, &dw);
		ASSERT (dw);
	}
	else {
		CStringArray v;

		FoxjetDatabase::Tokenize (strAddress, v);

		if (v.GetSize () >= 5) {
			//1,9600,n,8,1
			int nComport		= _ttoi (v [0]);
			DWORD dwBaudRate	= _ttol (v [1]);
			BYTE nParity;
			BYTE nByteSize		= _ttoi (v [3]);
			BYTE nStopBits		= _ttoi (v [4]) * 10;
			TCHAR cParity;

			_stscanf (v [2], _T ("%c"), &cParity);

			switch (cParity) {
			default:
			case 'n': case 'N': nParity = NOPARITY;	break;
			case 'e': case 'E': nParity = EVENPARITY;	break;
			case 'o': case 'O': nParity = ODDPARITY;	break;
			case 's': case 'S': nParity = SPACEPARITY;	break;
			case 'm': case 'M': nParity = ODDPARITY;	break;
			}
			
			switch (nStopBits) {
			default:
			case 10:	nStopBits = ONESTOPBIT;		break;
			case 15:	nStopBits = ONE5STOPBITS;	break;
			case 20:	nStopBits = TWOSTOPBITS;	break;
			}

			m_hComm = Open_Comport (nComport, dwBaudRate, nByteSize, nParity, nStopBits, FILE_FLAG_RANDOM_ACCESS | FILE_FLAG_OVERLAPPED);

			if (m_hComm == NULL || m_hComm == INVALID_HANDLE_VALUE) {
				TRACEF ("Connect failed: " + strAddress + "\n" + FormatMessage (::GetLastError ()));
				return S_OK;
			}

			{
				CString strEvent;

				m_pSerialThread = (CSerialThread *)::AfxBeginThread (RUNTIME_CLASS (CSerialThread));
				m_pSerialThread->m_hComm = m_hComm;
				m_pSerialThread->m_pPrinter = this;
				strEvent.Format (_T ("%lu CSerialThread::InitInstance"), m_pSerialThread->m_nThreadID);
				m_pSerialThread->m_hInit = CreateEvent (NULL, true, false, strEvent);

				VERIFY (::WaitForSingleObject (m_pSerialThread->m_hInit, INFINITE) == WAIT_OBJECT_0);
			}
		}
		else {
			ASSERT (m_pSocket == NULL);
			m_pSocket = new CSocket;

			if (!m_pSocket->Create (0, SOCK_STREAM)) {
				TRACEF ("Create failed: " + strAddress + "\n" + FormatMessage (::GetLastError ()));
				return S_OK;
			}

			if (!m_pSocket->Connect (strAddress, BW_HOST_TCP_PORT)) {
				TRACEF ("Connect failed: " + strAddress + "\n" + FormatMessage (::GetLastError ()));
				return S_OK;
			}

			CStringArray vCmd, vResponse;

			vCmd.Add (BW_HOST_PACKET_SET_ERROR_SOCKET);
			vCmd.Add (_T ("1"));
			vCmd.Add (strAddress);
			vCmd.Add (FoxjetDatabase::ToString (BW_HOST_UDP_PORT));

			if (!SendCmd (vCmd, vResponse))
				return S_OK;

			m_pErrorThread = (CErrorThread *)::AfxBeginThread (RUNTIME_CLASS (CErrorThread));
			m_pErrorThread->m_pPrinter = this;
		
			if (!m_pErrorThread->m_sock.Create (BW_HOST_UDP_PORT, SOCK_DGRAM))
				MSGBOX (FormatMessage (::GetLastError ()));

			int nFlag = 1;

			if (!m_pErrorThread->m_sock.SetSockOpt (SO_BROADCAST, &nFlag, sizeof (nFlag)))
				MSGBOX (FormatMessage (::GetLastError ()));
		}
	}

	CStringArray vCmd, vResponse;

	vCmd.Add (BW_HOST_PACKET_GET_LINES);
	
	m_strAddress = strAddress;

	if (!SendCmd (vCmd, vResponse))
		return S_OK;
	 
	for (int i = 1; i < vResponse.GetSize (); i++) {
		CComObject <CLine> * pLine;//new CComObject <CLine> ();
		CStringArray vID, vHeads;
		CString strLine = vResponse [i];
		
		HRESULT hRes = CComObject<CLine>::CreateInstance(&pLine);
		ATLASSERT(SUCCEEDED(hRes));
		pLine->AddRef ();

		vCmd.RemoveAll ();
		vCmd.Add (BW_HOST_PACKET_GET_LINE_ID);
		vCmd.Add (strLine);

		if (!SendCmd (vCmd, vID))
			return S_OK;

		pLine->m_strName = strLine;
		pLine->m_pPrinter = this;

		if (vID.GetSize () >= 3)
			pLine->m_lID = _ttol (vID [2]);

		vCmd.RemoveAll ();
		vCmd.Add (BW_HOST_PACKET_GET_HEADS);
		vCmd.Add (strLine);

		if (!SendCmd (vCmd, vHeads))
			return S_OK;

		for (int nHead = 2; nHead < vHeads.GetSize (); nHead++) {
			CString str = vHeads [nHead];
			CComObject <CHead> * pHead;// = new CComObject <CHead> ();

			HRESULT hRes = CComObject<CHead>::CreateInstance(&pHead);
			ATLASSERT(SUCCEEDED(hRes));
			pHead->AddRef ();

			VERIFY (FoxjetDatabase::FromString (str, pHead->m_head));
			pHead->m_pPrinter = this;
			pHead->m_pLine = pLine;

			pLine->m_vHeads.Add (pHead);
		}

		m_vLines.Add (pLine);
	}

	if (pResult) * pResult = TRUE;

	return S_OK;
}

bool CPrinter::SendCmd (const CStringArray & vCmd, CStringArray & vResponse)
{
	CString strCmd = FoxjetDatabase::ToString (vCmd);
	int nLen = strCmd.GetLength ();

	vResponse.RemoveAll ();

	if (m_pSocket) {
		ASSERT (strCmd.Find (BW_HOST_PACKET_CLOSESOCKET) == -1);

		bool bSend = m_pSocket->Send ((LPVOID)(LPCSTR)w2a (strCmd), nLen) != SOCKET_ERROR;

		if (!bSend) 
			Free ();
		else {
			bool bMore = true;
			CString strBuffer;
			int nTries = 0;

			do {
				char szBuffer [1024];
				ZeroMemory (szBuffer, sizeof (szBuffer));

				int nRevc = m_pSocket->Receive (szBuffer, sizeof (szBuffer));

				if (nRevc == SOCKET_ERROR) {
					Free ();
					MSGBOX (_T ("Receive failed: \n") + FormatMessage (::GetLastError ()));
					return false;
				}
				
				strBuffer += a2w (szBuffer);

				int nLeft = FoxjetDatabase::CountChars (strBuffer, '{').GetSize ();
				int nRight = FoxjetDatabase::CountChars (strBuffer, '}').GetSize ();

				if (nLeft && nRight == nLeft)
					bMore = false;
				else {
					if (nTries++ > 20) {
						MSGBOX ("No TCP/IP response from printer at: " + m_strAddress);
						ASSERT (0);
						return false;
					}
				}
			} 
			while (bMore);

			FoxjetDatabase::FromString (strBuffer, vResponse);

			#ifdef _DEBUG
			ASSERT (vCmd.GetSize ());
			ASSERT (vResponse.GetSize ());
			CString strCmd = vCmd [0];
			CString strResponse = vResponse [0];
			ASSERT (!strCmd.CompareNoCase (strResponse)); // cmd/response mismatched
			#endif //_DEBUG

			return true;
		}
	}
	else if (m_lpMapAddress && m_hwndControl) {
		DWORD dw = 0;

		for (int nTries = 0; nTries < 2; nTries++) {
			CopyToMapFile (m_lpMapAddress, w2a (strCmd));

			::SendMessageTimeout (m_hwndControl, WM_SENDMAPPEDCMD, 0, 0,
				SMTO_BLOCK | SMTO_ABORTIFHUNG, m_dwTimeout, &dw);
			
			if (!dw) {
				BOOL bResult = 0;
				Free ();
				Connect (CString ().AllocSysString (), &bResult);
			}
			else
				break;
		}

		FoxjetDatabase::FromString (a2w ((LPCSTR)m_lpMapAddress), vResponse);

		#ifdef _DEBUG
		ASSERT (vCmd.GetSize ());
		ASSERT (vResponse.GetSize ());
		CString strCmd = vCmd [0];
		CString strResponse = vResponse [0];
		ASSERT (!strCmd.CompareNoCase (strResponse)); // cmd/response mismatched
		#endif //_DEBUG

		return true;
	}
	else if (m_hComm) {
		DWORD dwWritten = 0;
		OVERLAPPED os;
		CString strEvent;

		ASSERT (m_pSerialThread);

		CSerialErrorHandler tmp (* this, false);

		strEvent.Format (_T ("%lu CSerialThread::Write"), m_pSerialThread->m_nThreadID);
		os.Offset		= 0;
		os.OffsetHigh	= 0;
		os.hEvent		= ::CreateEvent (NULL, true, false, strEvent);

		if (::WaitForSingleObject (m_pSerialThread->m_hData, 0) == WAIT_OBJECT_0) {
			::ResetEvent (m_pSerialThread->m_hData);
			TRACEF (m_pSerialThread->GetPendingData ());
		}

		if (!::WriteFile (m_hComm, (LPVOID)(LPCSTR)w2a (strCmd), nLen, &dwWritten, &os)) {
			if (::WaitForSingleObject (os.hEvent, m_dwTimeout) == WAIT_OBJECT_0) {
				if (!::GetOverlappedResult (m_hComm, &os, &dwWritten, FALSE)) {
					MSGBOX (_T ("GetOverlappedResult failed:\n") + FormatMessage (::GetLastError ()));
					return false;
				}

				if (dwWritten != (DWORD)nLen) {
					MSGBOX ("WriteFile failed:\n" + strCmd);
					return false;
				}

				::FlushFileBuffers (m_hComm);
				::ResetEvent (os.hEvent);
			}
		}

		::CloseHandle (os.hEvent);

		{
			bool bMore = true;
			CString strBuffer;
			int nTries = 0;

			do {
				if (::WaitForSingleObject (m_pSerialThread->m_hData, m_dwTimeout) == WAIT_OBJECT_0) {
					::ResetEvent (m_pSerialThread->m_hData);

					strBuffer += m_pSerialThread->GetPendingData ();

					int nLeft = FoxjetDatabase::CountChars (strBuffer, '{').GetSize ();
					int nRight = FoxjetDatabase::CountChars (strBuffer, '}').GetSize ();

					if (nLeft && nRight == nLeft)
						bMore = false;
				}

				if (nTries++ > 20) {
					MSGBOX ("No serial response from printer at: " + m_strAddress);
					ASSERT (0);
					return false;
				}
			} 
			while (bMore);

			FoxjetDatabase::FromString (strBuffer, vResponse);

			#ifdef _DEBUG
			ASSERT (vCmd.GetSize ());
			ASSERT (vResponse.GetSize ());
			CString strCmd = vCmd [0];
			CString strResponse = vResponse [0];
			ASSERT (!strCmd.CompareNoCase (strResponse)); // cmd/response mismatched
			#endif //_DEBUG

			return true;
		}
	}

	return false;
}

STDMETHODIMP CPrinter::Disconnect()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if (m_pSocket) {
		CStringArray vCmd, vResponse;

		vCmd.Add (BW_HOST_PACKET_SET_ERROR_SOCKET);
		vCmd.Add (_T ("0"));
		vCmd.Add (m_strAddress);
		vCmd.Add (FoxjetDatabase::ToString (BW_HOST_UDP_PORT));

		SendCmd (vCmd, vResponse);

		vCmd.RemoveAll ();
		vCmd.Add (BW_HOST_PACKET_CLOSESOCKET);
		CString str = FoxjetDatabase::ToString (vCmd);
		int nLen = str.GetLength ();

		m_pSocket->Send ((LPVOID)(LPCSTR)w2a (str), nLen);
	}

	Free ();

	return S_OK;
}

static BOOL CALLBACK EnumWindowsProc (HWND hWnd, LPARAM lParam)
{
	// this fct is not fool-proof.  other apps (Acrobat IEHelper) can respond to 
	// the WM_GETPRINTERWND message.
	// thus, hFind must be a valid, visible window with an MFC generated classname before
	// we consider it the real control m_hWnd

	HWND hFind = NULL;
	
	::SendMessageTimeout (hWnd, WM_GETPRINTERWND, 0, 0,  
		SMTO_BLOCK | SMTO_ABORTIFHUNG, CPrinter::m_dwTimeout * 5, (DWORD *)&hFind);

	if (hFind && ::IsWindow (hFind) && ::IsWindowVisible (hFind)) {
		TCHAR sz [512] = { 0 };
		
		::GetClassName (hWnd, sz, sizeof (sz));

		if (CString (sz).Find (_T ("Afx:")) != -1) {
			HWND * hResult = (HWND *)lParam;
			* hResult = hFind;
			
			return FALSE;
		}
	}

	return TRUE;
}

HWND CPrinter::LocateControlWnd ()
{
	HWND hWnd = NULL;

	::EnumWindows (::EnumWindowsProc, (LPARAM)&hWnd);

	return hWnd;
}

void CPrinter::Free()
{
	theApp.Unregister (this);

	if (m_pSocket) {
		m_pSocket->Close ();
		delete m_pSocket;
		m_pSocket = NULL;

		if (m_pErrorThread) {
			m_pErrorThread->m_sock.Close ();
			::Sleep (0);
		}
	}

	if (m_pErrorThread) {
		while (!::PostThreadMessage (m_pErrorThread->m_nThreadID, TM_ERROR_KILLTHREAD, 0, 0))
			::Sleep (0);

		DWORD dwWait = ::WaitForSingleObject (m_pErrorThread->m_hThread, 2000);

		if (dwWait == WAIT_TIMEOUT) 
			MSGBOX (_T ("WaitForSingleObject failed"));

		m_pErrorThread = NULL;
	}

	if (m_pSerialThread) {
		::SetEvent (m_pSerialThread->m_hExit);

		DWORD dwWait = ::WaitForSingleObject (m_pSerialThread->m_hThread, 2000);

		if (dwWait == WAIT_TIMEOUT) 
			MSGBOX (_T ("WaitForSingleObject failed"));

		m_pSerialThread = NULL;
	}

	if (m_hComm && m_hComm != INVALID_HANDLE_VALUE) {
		Close_Comport (m_hComm);
		m_hComm = NULL;
	}

	if (m_hwndControl) {
		DWORD dw = 0;

		::SendMessageTimeout (m_hwndControl, WM_CLOSEMAPFILE, 0, 0, 
			SMTO_BLOCK | SMTO_ABORTIFHUNG, m_dwTimeout, &dw);
		m_hwndControl = NULL;
	}

	for (int i = 0; i < m_vLines.GetSize (); i++) {
		CComObject<CLine> * p = m_vLines [i];
		p->Release ();
		//delete p;
	}

	m_vLines.RemoveAll ();

	if (m_hMapFile) {
		::CloseHandle (m_hMapFile);
		m_hMapFile = NULL;
	}

	if (m_hFile) {
		::CloseHandle (m_hFile);
		m_hFile = NULL;
	}

	if (m_lpMapAddress) {
		::UnmapViewOfFile (m_lpMapAddress);
		m_lpMapAddress = NULL;
	}

	m_strAddress.Empty ();
}

STDMETHODIMP CPrinter::GetLines(IStringArray * p)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if (p) {
		p->RemoveAll ();

		for (int i = 0; i < m_vLines.GetSize (); i++) {
			CComObject<CLine> * pLine = m_vLines [i];

			p->Add (pLine->m_strName.AllocSysString ());
		}
	}

	return S_OK;
}


ULONG CPrinter::GetLineID(const CString &strLine) const
{
	for (int i = 0; i < m_vLines.GetSize (); i++) {
		CComObject<CLine> * pLine = m_vLines [i];

		if (!pLine->m_strName.CompareNoCase (strLine))
			return pLine->m_lID;
	}

	return -1;
}

STDMETHODIMP CPrinter::get_m_strAddress(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if (pVal == NULL)
		return E_POINTER;

	* pVal = m_strAddress.AllocSysString ();

	return S_OK;
}

STDMETHODIMP CPrinter::ToString(IStringArray *v, long lFlags, long lTimeoutSeconds, /*[out, retval]*/ BOOL * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CStringArray vCmd, vResponse;
	bool bReady = false;
	int nStrings = 0;

	if (pResult) * pResult = FALSE;

	v->RemoveAll ();
	vCmd.Add (BW_HOST_PACKET_DB_BUILD_STRINGS);

	TRACEARRAY (vCmd);

	if (!SendCmd (vCmd, vResponse)) {
		TRACEF ("SendCmd failed: " + vCmd [0]);
		return S_OK;
	}

	TRACEARRAY (vResponse);

	const CTime tmStart = CTime::GetCurrentTime ();

	while (!bReady) {
		vCmd.RemoveAll ();
		vResponse.RemoveAll ();
		vCmd.Add (BW_HOST_PACKET_DB_IS_READY);

		if (!SendCmd (vCmd, vResponse)) {
			TRACEF ("SendCmd failed: " + vCmd [0]);
			return S_OK;
		}

		TRACEARRAY (vResponse);

		if (vResponse.GetSize () >= 2) {
			nStrings = _ttoi (vResponse [1]);
			bReady = true;
		}
		else {
			CTimeSpan tm = CTime::GetCurrentTime () - tmStart;
			int nTotal = (int)tm.GetTotalSeconds ();

			if (nTotal >= (lTimeoutSeconds)) {
				TRACEF ("Timed out");
				return S_OK;
			}

			::Sleep (500);
		}
	}

	{
		CStringArray vDB;
		int nCount = 0;
		bool bMore;

		do {
			vCmd.RemoveAll ();
			vResponse.RemoveAll ();
			vCmd.Add (BW_HOST_PACKET_DB_GET_NEXT_STRING);

			if (!SendCmd (vCmd, vResponse)) {
				TRACEF ("SendCmd failed: " + vCmd [0]);
				return S_OK;
			}

			if (bMore = (vResponse.GetSize () >= 2)) 
				v->Add (vResponse [1].AllocSysString ());

		} while (bMore);

		v->get_Count (&nCount);
		ASSERT (nCount == nStrings);

		if (pResult)
			* pResult = (nCount == nStrings);
	}

	return S_OK;
}

STDMETHODIMP CPrinter::FromString(IStringArray *v, long lFlags, long lTimeoutSeconds, /*[out, retval]*/ BOOL * pResult)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CStringArray vCmd, vResponse;
	int nCount = 0;

	if (pResult) * pResult = FALSE;

	vCmd.Add (BW_HOST_PACKET_DB_BEGIN_UPDATE);

	if (!SendCmd (vCmd, vResponse)) {
		TRACEF ("SendCmd failed: " + vCmd [0]);
		return S_OK;
	}

	TRACEARRAY (vResponse);
	
	v->get_Count (&nCount);

	for (int i = 0; i < nCount; i++) {
		BSTR bstr;

		v->get_GetAt (i, &bstr);

		vCmd.RemoveAll ();
		vResponse.RemoveAll ();
		vCmd.Add (BW_HOST_PACKET_DB_GET_NEXT_STRING);
		vCmd.Add ((LPCTSTR)_bstr_t (bstr));

		TRACEARRAY (vCmd);

		if (!SendCmd (vCmd, vResponse)) {
			TRACEF ("SendCmd failed: " + vCmd [0]);
			return S_OK;
		}

		TRACEARRAY (vResponse);
	}

	vCmd.RemoveAll ();
	vResponse.RemoveAll ();
	vCmd.Add (BW_HOST_PACKET_DB_END_UPDATE);

	if (!SendCmd (vCmd, vResponse)) {
		TRACEF ("SendCmd failed: " + vCmd [0]);
		return S_OK;
	}

	const CTime tmStart = CTime::GetCurrentTime ();
	bool bReady = false;

	while (!bReady) {
		vCmd.RemoveAll ();
		vResponse.RemoveAll ();
		vCmd.Add (BW_HOST_PACKET_DB_IS_READY);

		if (!SendCmd (vCmd, vResponse)) {
			TRACEF ("SendCmd failed: " + vCmd [0]);
			return S_OK;
		}

		TRACEARRAY (vResponse);

		if (vResponse.GetSize () >= 2) {
			bReady = _ttoi (vResponse [1]) ? true : false;

			if (pResult)
				* pResult = true;
		}
		else {
			CTimeSpan tm = CTime::GetCurrentTime () - tmStart;
			int nTotal = tm.GetTotalSeconds ();

			if (nTotal >= lTimeoutSeconds) {
				TRACEF ("Timed out");
				return S_OK;
			}

			::Sleep (500);
		}
	}

	return S_OK;
}

STDMETHODIMP CPrinter::Refresh()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CStringArray vCmd, vResponse;
	bool bReady = false;
	int nStrings = 0;

	vCmd.Add (BW_HOST_PACKET_REFRESH_PREVIEW);

	TRACEARRAY (vCmd);

	if (!SendCmd (vCmd, vResponse)) {
		TRACEF ("SendCmd failed: " + vCmd [0]);
		return S_OK;
	}

	TRACEARRAY (vResponse);

	return S_OK;
}
