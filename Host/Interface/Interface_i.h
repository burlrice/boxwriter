

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Tue Jan 14 08:10:28 2020
 */
/* Compiler settings for Interface.odl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __Interface_i_h__
#define __Interface_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IPrinter_FWD_DEFINED__
#define __IPrinter_FWD_DEFINED__
typedef interface IPrinter IPrinter;
#endif 	/* __IPrinter_FWD_DEFINED__ */


#ifndef __IStringArray_FWD_DEFINED__
#define __IStringArray_FWD_DEFINED__
typedef interface IStringArray IStringArray;
#endif 	/* __IStringArray_FWD_DEFINED__ */


#ifndef __ILine_FWD_DEFINED__
#define __ILine_FWD_DEFINED__
typedef interface ILine ILine;
#endif 	/* __ILine_FWD_DEFINED__ */


#ifndef __IHead_FWD_DEFINED__
#define __IHead_FWD_DEFINED__
typedef interface IHead IHead;
#endif 	/* __IHead_FWD_DEFINED__ */


#ifndef ___DBoxWriter_FWD_DEFINED__
#define ___DBoxWriter_FWD_DEFINED__
typedef interface _DBoxWriter _DBoxWriter;
#endif 	/* ___DBoxWriter_FWD_DEFINED__ */


#ifndef ___DBoxWriterEvents_FWD_DEFINED__
#define ___DBoxWriterEvents_FWD_DEFINED__
typedef interface _DBoxWriterEvents _DBoxWriterEvents;
#endif 	/* ___DBoxWriterEvents_FWD_DEFINED__ */


#ifndef __BoxWriter_FWD_DEFINED__
#define __BoxWriter_FWD_DEFINED__

#ifdef __cplusplus
typedef class BoxWriter BoxWriter;
#else
typedef struct BoxWriter BoxWriter;
#endif /* __cplusplus */

#endif 	/* __BoxWriter_FWD_DEFINED__ */


#ifndef __Printer_FWD_DEFINED__
#define __Printer_FWD_DEFINED__

#ifdef __cplusplus
typedef class Printer Printer;
#else
typedef struct Printer Printer;
#endif /* __cplusplus */

#endif 	/* __Printer_FWD_DEFINED__ */


#ifndef __Line_FWD_DEFINED__
#define __Line_FWD_DEFINED__

#ifdef __cplusplus
typedef class Line Line;
#else
typedef struct Line Line;
#endif /* __cplusplus */

#endif 	/* __Line_FWD_DEFINED__ */


#ifndef __StringArray_FWD_DEFINED__
#define __StringArray_FWD_DEFINED__

#ifdef __cplusplus
typedef class StringArray StringArray;
#else
typedef struct StringArray StringArray;
#endif /* __cplusplus */

#endif 	/* __StringArray_FWD_DEFINED__ */


#ifndef __Head_FWD_DEFINED__
#define __Head_FWD_DEFINED__

#ifdef __cplusplus
typedef class Head Head;
#else
typedef struct Head Head;
#endif /* __cplusplus */

#endif 	/* __Head_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __BoxWriter_LIBRARY_DEFINED__
#define __BoxWriter_LIBRARY_DEFINED__

/* library BoxWriter */
/* [control][helpstring][helpfile][version][uuid] */ 







typedef /* [public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0001
    {	INK_CODE_WARNGING_LEVEL_0	= 0,
	INK_CODE_WARNGING_LEVEL_1	= 0x1,
	INK_CODE_WARNGING_LEVEL_2	= 0x2,
	INK_CODE_WARNGING_LEVEL_3	= 0x4,
	INK_CODE_WARNGING_LEVEL_MASK	= ( ( INK_CODE_WARNGING_LEVEL_1 | INK_CODE_WARNGING_LEVEL_2 )  | INK_CODE_WARNGING_LEVEL_3 ) ,
	INK_CODE_WARNGING_LOW_INK	= 0x8,
	INK_CODE_WARNGING_INK_OUT	= 0x10,
	INK_CODE_WARNGING_INVALID_CODE	= 0x20,
	INK_CODE_WARNGING_STATE_MASK	= ( ( INK_CODE_WARNGING_LOW_INK | INK_CODE_WARNGING_INK_OUT )  | INK_CODE_WARNGING_INVALID_CODE ) 
    } 	INK_CODE_WARNGING_LEVEL;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0002
    {	INK_CODE_THRESHOLD_WARNING	= 175,
	INK_CODE_THRESHOLD_ERROR	= 200
    } 	INK_CODE_THRESHOLD;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0003
    {	NOTFOUND	= 0,
	ALL	= 0xffffffff
    } 	ID_CONSTANTS;

typedef /* [public][public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0004
    {	DISABLED	= 0,
	RUNNING	= ( DISABLED + 1 ) ,
	IDLE	= ( RUNNING + 1 ) ,
	STOPPED	= ( IDLE + 1 ) 
    } 	TASKSTATE;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0005
    {	ROT_0	= 0,
	ROT_90	= ( ROT_0 + 1 ) ,
	ROT_180	= ( ROT_90 + 1 ) ,
	ROT_270	= ( ROT_180 + 1 ) 
    } 	ORIENTATION;

typedef /* [public][public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0006
    {	RTOL	= 0,
	LTOR	= ( RTOL + 1 ) 
    } 	DIRECTION;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0007
    {	TL	= 0,
	TR	= ( TL + 1 ) ,
	BR	= ( TR + 1 ) ,
	BL	= ( BR + 1 ) ,
	STRETCH	= ( BL + 1 ) 
    } 	IMAGEATTRIBUTES;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0008
    {	MATRIX	= 0,
	ELITE	= ( MATRIX + 1 ) 
    } 	PRINTERTYPE;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0009
    {	IMAGE_ON_PHOTOCELL	= 0,
	IMAGE_ON_EOP	= ( IMAGE_ON_PHOTOCELL + 1 ) 
    } 	IMAGINGMODE;

typedef 
enum tagSTROBESTATE
    {	STROBESTATE_OFF	= 0,
	STROBESTATE_ON	= ( STROBESTATE_OFF + 1 ) ,
	STROBESTATE_FLASH	= ( STROBESTATE_ON + 1 ) 
    } 	STROBESTATE;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0010
    {	HEADERROR_READY	= 0,
	HEADERROR_HIGHVOLTAGE	= 0x1,
	HEADERROR_LOWTEMP	= 0x2,
	HEADERROR_LOWINK	= 0x4,
	HEADERROR_OUTOFINK	= 0x8,
	IVERROR_BROKENLINE	= 0x10,
	IVERROR_DISCONNECTED	= 0x20,
	HPERROR_COMMPORTFAILURE	= 0x40,
	HEADERROR_DISCONNECTED	= 0x80,
	HEADERROR_STOPPED	= 0x100,
	HEADERROR_PAUSED	= 0x200,
	HEADERROR_EP8	= 0x400,
	HUBERROR_IO	= 0x1000,
	HEADERROR_APSCYCLE	= 0x2000,
	HEADERROR_NOFPGA	= 0x4000,
	ERROR_NETWORK	= 0x8000
    } 	HEADERRORTYPE;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0011
    {	IV_12_9	= 0,
	IV_78_9	= ( IV_12_9 + 1 ) ,
	IV_10_18	= ( IV_78_9 + 1 ) ,
	IV_20_18	= ( IV_10_18 + 1 ) ,
	IV_LYNX_32	= ( IV_20_18 + 1 ) ,
	IV_NONE	= -1
    } 	VALVETYPE;

typedef /* [public][public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0012
    {	ALPHA_CODER	= 0,
	UJ2_352_32	= ( ALPHA_CODER + 1 ) ,
	UJ2_192_32	= ( UJ2_352_32 + 1 ) ,
	UJ2_96_32	= ( UJ2_192_32 + 1 ) ,
	GRAPHICS_768_256	= ( UJ2_96_32 + 1 ) ,
	GRAPHICS_384_128	= ( GRAPHICS_768_256 + 1 ) ,
	UJ2_192_32NP	= ( GRAPHICS_384_128 + 1 ) ,
	IV_72	= ( UJ2_192_32NP + 1 ) ,
	GRAPHICS_768_256_L310	= ( IV_72 + 1 ) ,
	UJI_96_32	= ( GRAPHICS_768_256_L310 + 1 ) ,
	UJI_192_32	= ( UJI_96_32 + 1 ) ,
	UJI_256_32	= ( UJI_192_32 + 1 ) ,
	GRAPHICS_768_256_L330	= ( UJI_256_32 + 1 ) ,
	GRAPHICS_768_256_0	= ( GRAPHICS_768_256_L330 + 1 ) ,
	UR2	= ( GRAPHICS_768_256_0 + 1 ) ,
	UR4	= ( UR2 + 1 ) 
    } 	HEADTYPE;

typedef /* [public][public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0013
    {	EXTERNAL_PC	= 0,
	INTERNAL_PC	= ( EXTERNAL_PC + 1 ) ,
	SHARED_PC	= ( INTERNAL_PC + 1 ) 
    } 	PHOTOCELLS;

typedef /* [public][public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0014
    {	SHARED_PCNONE	= 0,
	SHARED_PCA	= ( SHARED_PCNONE + 1 ) ,
	SHARED_PCB	= ( SHARED_PCA + 1 ) 
    } 	SHAREDPHOTOCELLS;

typedef /* [public][public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0015
    {	EXTERNAL_ENC	= 0,
	INTERNAL_ENC	= ( EXTERNAL_ENC + 1 ) ,
	SHARED_ENC	= ( INTERNAL_ENC + 1 ) ,
	DUAL_TRIGGER	= EXTERNAL_ENC
    } 	ENCODERS;

typedef /* [public][public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0016
    {	SHARED_ENCNONE	= 0,
	SHARED_ENCA	= ( SHARED_ENCNONE + 1 ) ,
	SHARED_ENCB	= ( SHARED_ENCA + 1 ) 
    } 	SHAREDENCODERS;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0017
    {	INKTYPE_FIRST	= 0,
	INKTYPE_V300	= ( INKTYPE_FIRST + 1 ) ,
	INKTYPE_SCANTRUE2	= ( INKTYPE_V300 + 1 ) 
    } 	INKTYPE;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0018
    {	PRINTER	= 0,
	EDITOR	= ( PRINTER + 1 ) 
    } 	REPORTING_APP;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0019
    {	REPORT_ALL	= -1,
	REPORT_TASK_CREATE	= 0,
	REPORT_TASK_MODIFY	= ( REPORT_TASK_CREATE + 1 ) ,
	REPORT_TASK_DESTROY	= ( REPORT_TASK_MODIFY + 1 ) ,
	REPORT_TASK_START	= ( REPORT_TASK_DESTROY + 1 ) ,
	REPORT_TASK_START_REMOTE	= ( REPORT_TASK_START + 1 ) ,
	REPORT_TASK_START_POWERUP	= ( REPORT_TASK_START_REMOTE + 1 ) ,
	REPORT_TASK_IDLE	= ( REPORT_TASK_START_POWERUP + 1 ) ,
	REPORT_TASK_IDLE_ONERROR	= ( REPORT_TASK_IDLE + 1 ) ,
	REPORT_TASK_RESUME	= ( REPORT_TASK_IDLE_ONERROR + 1 ) ,
	REPORT_TASK_STOP	= ( REPORT_TASK_RESUME + 1 ) ,
	REPORT_AMS_CYCLE	= ( REPORT_TASK_STOP + 1 ) ,
	REPORT_EXECSTART	= ( REPORT_AMS_CYCLE + 1 ) ,
	REPORT_EXECSTOP	= ( REPORT_EXECSTART + 1 ) ,
	REPORT_KEYFIELDSTART	= ( REPORT_EXECSTOP + 1 ) ,
	REPORT_STROBESTATE	= ( REPORT_KEYFIELDSTART + 1 ) ,
	REPORT_TIMECHANGE	= ( REPORT_STROBESTATE + 1 ) ,
	REPORT_INKCODE	= ( REPORT_TIMECHANGE + 1 ) ,
	REPORT_DIAGNOSTIC	= ( REPORT_INKCODE + 1 ) 
    } 	REPORTTYPE;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0020
    {	IMPORT_LINESTRUCT	= 0x1,
	IMPORT_BOXSTRUCT	= 0x2,
	IMPORT_BOXLINKSTRUCT	= 0x4,
	IMPORT_TASKSTRUCT	= 0x8,
	IMPORT_HEADSTRUCT	= 0x10,
	IMPORT_PANELSTRUCT	= 0x20,
	IMPORT_OPTIONSSTRUCT	= 0x40,
	IMPORT_OPTIONSLINKSTRUCT	= 0x80,
	IMPORT_USERSTRUCT	= 0x100,
	IMPORT_SECURITYGROUPSTRUCT	= 0x200,
	IMPORT_SHIFTSTRUCT	= 0x400,
	IMPORT_REPORTSTRUCT	= 0x800,
	IMPORT_BCSCANSTRUCT	= 0x1000,
	IMPORT_DELETEDSTRUCT	= 0x2000,
	IMPORT_SETTINGSSTRUCT	= 0x4000,
	IMPORT_IMAGESTRUCT	= 0x8000,
	IMPORT_PRINTERSTRUCT	= 0x10000,
	IMPORT_BITMAPS	= 0x20000,
	IMPORT_ALL	= 0xffffffff,
	IMPORT_CONFIG	= ( ( ( ( IMPORT_LINESTRUCT | IMPORT_HEADSTRUCT )  | IMPORT_PANELSTRUCT )  | IMPORT_PRINTERSTRUCT )  | IMPORT_IMAGESTRUCT ) ,
	IMPORT_TASKS	= ( ( ( ( ( IMPORT_BOXSTRUCT | IMPORT_BOXLINKSTRUCT )  | IMPORT_TASKSTRUCT )  | IMPORT_SHIFTSTRUCT )  | IMPORT_SETTINGSSTRUCT )  | IMPORT_BITMAPS ) ,
	IMPORT_USERS	= ( ( ( IMPORT_OPTIONSSTRUCT | IMPORT_OPTIONSLINKSTRUCT )  | IMPORT_USERSTRUCT )  | IMPORT_SECURITYGROUPSTRUCT ) ,
	IMPORT_REPORTS	= ( ( IMPORT_REPORTSTRUCT | IMPORT_BCSCANSTRUCT )  | IMPORT_DELETEDSTRUCT ) 
    } 	IMPORT_TYPES;

typedef /* [public] */ 
enum __MIDL___MIDL_itf_Interface_0000_0000_0021
    {	MOTHERBOARD_REV2	= 2,
	MOTHERBOARD_REV3	= 3
    } 	MOTHERBOARDTYPE;


EXTERN_C const IID LIBID_BoxWriter;

#ifndef __IPrinter_INTERFACE_DEFINED__
#define __IPrinter_INTERFACE_DEFINED__

/* interface IPrinter */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IPrinter;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("0D2CBCE3-7726-44BA-AE3F-E0D644B71480")
    IPrinter : public IDispatch
    {
    public:
        virtual /* [helpstring][bindable][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_line( 
            /* [defaultvalue][optional] */ BSTR strLine,
            /* [retval][out] */ ILine **pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Connect( 
            /* [defaultvalue][optional] */ BSTR strAddress,
            /* [retval][out] */ BOOL *pResult) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Disconnect( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetLines( 
            IStringArray *v) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_strAddress( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ToString( 
            IStringArray *v,
            long lFlags,
            long lTimeoutSeconds,
            /* [retval][out] */ BOOL *pResult) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FromString( 
            IStringArray *v,
            long lFlags,
            long lTimeoutSeconds,
            /* [retval][out] */ BOOL *pResult) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Refresh( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPrinterVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IPrinter * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IPrinter * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IPrinter * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IPrinter * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IPrinter * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IPrinter * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IPrinter * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][bindable][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_line )( 
            IPrinter * This,
            /* [defaultvalue][optional] */ BSTR strLine,
            /* [retval][out] */ ILine **pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Connect )( 
            IPrinter * This,
            /* [defaultvalue][optional] */ BSTR strAddress,
            /* [retval][out] */ BOOL *pResult);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Disconnect )( 
            IPrinter * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetLines )( 
            IPrinter * This,
            IStringArray *v);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_strAddress )( 
            IPrinter * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ToString )( 
            IPrinter * This,
            IStringArray *v,
            long lFlags,
            long lTimeoutSeconds,
            /* [retval][out] */ BOOL *pResult);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FromString )( 
            IPrinter * This,
            IStringArray *v,
            long lFlags,
            long lTimeoutSeconds,
            /* [retval][out] */ BOOL *pResult);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Refresh )( 
            IPrinter * This);
        
        END_INTERFACE
    } IPrinterVtbl;

    interface IPrinter
    {
        CONST_VTBL struct IPrinterVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPrinter_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IPrinter_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IPrinter_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IPrinter_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IPrinter_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IPrinter_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IPrinter_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IPrinter_get_m_line(This,strLine,pVal)	\
    ( (This)->lpVtbl -> get_m_line(This,strLine,pVal) ) 

#define IPrinter_Connect(This,strAddress,pResult)	\
    ( (This)->lpVtbl -> Connect(This,strAddress,pResult) ) 

#define IPrinter_Disconnect(This)	\
    ( (This)->lpVtbl -> Disconnect(This) ) 

#define IPrinter_GetLines(This,v)	\
    ( (This)->lpVtbl -> GetLines(This,v) ) 

#define IPrinter_get_m_strAddress(This,pVal)	\
    ( (This)->lpVtbl -> get_m_strAddress(This,pVal) ) 

#define IPrinter_ToString(This,v,lFlags,lTimeoutSeconds,pResult)	\
    ( (This)->lpVtbl -> ToString(This,v,lFlags,lTimeoutSeconds,pResult) ) 

#define IPrinter_FromString(This,v,lFlags,lTimeoutSeconds,pResult)	\
    ( (This)->lpVtbl -> FromString(This,v,lFlags,lTimeoutSeconds,pResult) ) 

#define IPrinter_Refresh(This)	\
    ( (This)->lpVtbl -> Refresh(This) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IPrinter_INTERFACE_DEFINED__ */


#ifndef __IStringArray_INTERFACE_DEFINED__
#define __IStringArray_INTERFACE_DEFINED__

/* interface IStringArray */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IStringArray;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6D72A637-602A-483D-9B7F-F721C64F8815")
    IStringArray : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ int *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_GetAt( 
            int nIndex,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetAt( 
            int nIndex,
            BSTR str) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Add( 
            BSTR str) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE InsertAt( 
            int nIndex,
            BSTR str) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Remove( 
            int nIndex) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE RemoveAll( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE FromString( 
            BSTR str) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ToString( 
            BSTR *str) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IStringArrayVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IStringArray * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IStringArray * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IStringArray * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IStringArray * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IStringArray * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IStringArray * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IStringArray * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            IStringArray * This,
            /* [retval][out] */ int *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GetAt )( 
            IStringArray * This,
            int nIndex,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetAt )( 
            IStringArray * This,
            int nIndex,
            BSTR str);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IStringArray * This,
            BSTR str);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *InsertAt )( 
            IStringArray * This,
            int nIndex,
            BSTR str);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IStringArray * This,
            int nIndex);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *RemoveAll )( 
            IStringArray * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *FromString )( 
            IStringArray * This,
            BSTR str);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *ToString )( 
            IStringArray * This,
            BSTR *str);
        
        END_INTERFACE
    } IStringArrayVtbl;

    interface IStringArray
    {
        CONST_VTBL struct IStringArrayVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IStringArray_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IStringArray_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IStringArray_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IStringArray_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IStringArray_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IStringArray_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IStringArray_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IStringArray_get_Count(This,pVal)	\
    ( (This)->lpVtbl -> get_Count(This,pVal) ) 

#define IStringArray_get_GetAt(This,nIndex,pVal)	\
    ( (This)->lpVtbl -> get_GetAt(This,nIndex,pVal) ) 

#define IStringArray_SetAt(This,nIndex,str)	\
    ( (This)->lpVtbl -> SetAt(This,nIndex,str) ) 

#define IStringArray_Add(This,str)	\
    ( (This)->lpVtbl -> Add(This,str) ) 

#define IStringArray_InsertAt(This,nIndex,str)	\
    ( (This)->lpVtbl -> InsertAt(This,nIndex,str) ) 

#define IStringArray_Remove(This,nIndex)	\
    ( (This)->lpVtbl -> Remove(This,nIndex) ) 

#define IStringArray_RemoveAll(This)	\
    ( (This)->lpVtbl -> RemoveAll(This) ) 

#define IStringArray_FromString(This,str)	\
    ( (This)->lpVtbl -> FromString(This,str) ) 

#define IStringArray_ToString(This,str)	\
    ( (This)->lpVtbl -> ToString(This,str) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IStringArray_INTERFACE_DEFINED__ */


#ifndef __ILine_INTERFACE_DEFINED__
#define __ILine_INTERFACE_DEFINED__

/* interface ILine */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_ILine;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EE6D8498-3D73-428A-8A8A-940D848E2EF6")
    ILine : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_strName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Start( 
            /* [retval][out] */ BOOL *pResult) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Stop( 
            /* [retval][out] */ BOOL *pResult) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Idle( 
            /* [retval][out] */ BOOL *pResult) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Resume( 
            /* [retval][out] */ BOOL *pResult) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCurrentTask( 
            BSTR *strTask,
            /* [retval][out] */ BOOL *pResult) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetTasks( 
            IStringArray *vTasks,
            /* [retval][out] */ long *pResult) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_lCount( 
            /* [retval][out] */ unsigned __int64 *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_m_lCount( 
            /* [in] */ unsigned __int64 newVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_head( 
            long lIndex,
            /* [retval][out] */ IHead **pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_nHeads( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Load( 
            BSTR strTask,
            /* [retval][out] */ BOOL *pResult) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetUserElementData( 
            IStringArray *vPrompt,
            IStringArray *vData,
            /* [retval][out] */ BOOL *pResult) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetUserElementData( 
            IStringArray *vPrompt,
            IStringArray *vData,
            /* [retval][out] */ long *pResult) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DatabaseStart( 
            BSTR strKeyValue,
            /* [retval][out] */ BOOL *pResult) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_m_strSerialData( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_m_strSerialData( 
            /* [in] */ BSTR newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ILineVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ILine * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ILine * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ILine * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ILine * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ILine * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ILine * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ILine * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_strName )( 
            ILine * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Start )( 
            ILine * This,
            /* [retval][out] */ BOOL *pResult);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Stop )( 
            ILine * This,
            /* [retval][out] */ BOOL *pResult);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Idle )( 
            ILine * This,
            /* [retval][out] */ BOOL *pResult);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Resume )( 
            ILine * This,
            /* [retval][out] */ BOOL *pResult);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCurrentTask )( 
            ILine * This,
            BSTR *strTask,
            /* [retval][out] */ BOOL *pResult);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetTasks )( 
            ILine * This,
            IStringArray *vTasks,
            /* [retval][out] */ long *pResult);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_lCount )( 
            ILine * This,
            /* [retval][out] */ unsigned __int64 *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_m_lCount )( 
            ILine * This,
            /* [in] */ unsigned __int64 newVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_head )( 
            ILine * This,
            long lIndex,
            /* [retval][out] */ IHead **pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_nHeads )( 
            ILine * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Load )( 
            ILine * This,
            BSTR strTask,
            /* [retval][out] */ BOOL *pResult);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetUserElementData )( 
            ILine * This,
            IStringArray *vPrompt,
            IStringArray *vData,
            /* [retval][out] */ BOOL *pResult);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *SetUserElementData )( 
            ILine * This,
            IStringArray *vPrompt,
            IStringArray *vData,
            /* [retval][out] */ long *pResult);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DatabaseStart )( 
            ILine * This,
            BSTR strKeyValue,
            /* [retval][out] */ BOOL *pResult);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_strSerialData )( 
            ILine * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_m_strSerialData )( 
            ILine * This,
            /* [in] */ BSTR newVal);
        
        END_INTERFACE
    } ILineVtbl;

    interface ILine
    {
        CONST_VTBL struct ILineVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ILine_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ILine_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ILine_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ILine_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ILine_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ILine_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ILine_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ILine_get_m_strName(This,pVal)	\
    ( (This)->lpVtbl -> get_m_strName(This,pVal) ) 

#define ILine_Start(This,pResult)	\
    ( (This)->lpVtbl -> Start(This,pResult) ) 

#define ILine_Stop(This,pResult)	\
    ( (This)->lpVtbl -> Stop(This,pResult) ) 

#define ILine_Idle(This,pResult)	\
    ( (This)->lpVtbl -> Idle(This,pResult) ) 

#define ILine_Resume(This,pResult)	\
    ( (This)->lpVtbl -> Resume(This,pResult) ) 

#define ILine_GetCurrentTask(This,strTask,pResult)	\
    ( (This)->lpVtbl -> GetCurrentTask(This,strTask,pResult) ) 

#define ILine_GetTasks(This,vTasks,pResult)	\
    ( (This)->lpVtbl -> GetTasks(This,vTasks,pResult) ) 

#define ILine_get_m_lCount(This,pVal)	\
    ( (This)->lpVtbl -> get_m_lCount(This,pVal) ) 

#define ILine_put_m_lCount(This,newVal)	\
    ( (This)->lpVtbl -> put_m_lCount(This,newVal) ) 

#define ILine_get_m_head(This,lIndex,pVal)	\
    ( (This)->lpVtbl -> get_m_head(This,lIndex,pVal) ) 

#define ILine_get_m_nHeads(This,pVal)	\
    ( (This)->lpVtbl -> get_m_nHeads(This,pVal) ) 

#define ILine_Load(This,strTask,pResult)	\
    ( (This)->lpVtbl -> Load(This,strTask,pResult) ) 

#define ILine_GetUserElementData(This,vPrompt,vData,pResult)	\
    ( (This)->lpVtbl -> GetUserElementData(This,vPrompt,vData,pResult) ) 

#define ILine_SetUserElementData(This,vPrompt,vData,pResult)	\
    ( (This)->lpVtbl -> SetUserElementData(This,vPrompt,vData,pResult) ) 

#define ILine_DatabaseStart(This,strKeyValue,pResult)	\
    ( (This)->lpVtbl -> DatabaseStart(This,strKeyValue,pResult) ) 

#define ILine_get_m_strSerialData(This,pVal)	\
    ( (This)->lpVtbl -> get_m_strSerialData(This,pVal) ) 

#define ILine_put_m_strSerialData(This,newVal)	\
    ( (This)->lpVtbl -> put_m_strSerialData(This,newVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ILine_INTERFACE_DEFINED__ */


#ifndef __IHead_INTERFACE_DEFINED__
#define __IHead_INTERFACE_DEFINED__

/* interface IHead */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IHead;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("4AF2FF41-7779-496D-A557-C97AC73C9577")
    IHead : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_lID( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_lPanelID( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_strName( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_strUID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_lRelativeHeight( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_nChannels( 
            /* [retval][out] */ int *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_nHorzRes( 
            /* [retval][out] */ int *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_lPhotocellDelay( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_nEncoder( 
            /* [retval][out] */ ENCODERS *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_nDirection( 
            /* [retval][out] */ DIRECTION *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_bEnabled( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_bInverted( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_dHeadAngle( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_nPhotocell( 
            /* [retval][out] */ PHOTOCELLS *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_nSharePhoto( 
            /* [retval][out] */ SHAREDPHOTOCELLS *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_nShareEnc( 
            /* [retval][out] */ SHAREDENCODERS *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_type( 
            /* [retval][out] */ HEADTYPE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_nInternalTachSpeed( 
            /* [retval][out] */ int *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_dNozzleSpan( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_dPhotocellRate( 
            /* [retval][out] */ double *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_nEncoderDivisor( 
            /* [retval][out] */ int *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_bDoublePulse( 
            /* [retval][out] */ BOOL *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetElements( 
            IStringArray *vElements,
            /* [retval][out] */ BOOL *pResult) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DeleteElement( 
            /* [defaultvalue][optional] */ long lID = ALL) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE AddElement( 
            BSTR strElement,
            /* [optional] */ long *lID) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE UpdateElement( 
            BSTR strElement,
            /* [retval][out] */ long *pResult) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetCurrentState( 
            IStringArray *vState,
            TASKSTATE *task,
            /* [retval][out] */ BOOL *pResult) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_m_bRemoteHead( 
            /* [retval][out] */ long *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IHeadVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IHead * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IHead * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IHead * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IHead * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IHead * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IHead * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IHead * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_lID )( 
            IHead * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_lPanelID )( 
            IHead * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_strName )( 
            IHead * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_strUID )( 
            IHead * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_lRelativeHeight )( 
            IHead * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_nChannels )( 
            IHead * This,
            /* [retval][out] */ int *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_nHorzRes )( 
            IHead * This,
            /* [retval][out] */ int *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_lPhotocellDelay )( 
            IHead * This,
            /* [retval][out] */ long *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_nEncoder )( 
            IHead * This,
            /* [retval][out] */ ENCODERS *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_nDirection )( 
            IHead * This,
            /* [retval][out] */ DIRECTION *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_bEnabled )( 
            IHead * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_bInverted )( 
            IHead * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_dHeadAngle )( 
            IHead * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_nPhotocell )( 
            IHead * This,
            /* [retval][out] */ PHOTOCELLS *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_nSharePhoto )( 
            IHead * This,
            /* [retval][out] */ SHAREDPHOTOCELLS *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_nShareEnc )( 
            IHead * This,
            /* [retval][out] */ SHAREDENCODERS *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_type )( 
            IHead * This,
            /* [retval][out] */ HEADTYPE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_nInternalTachSpeed )( 
            IHead * This,
            /* [retval][out] */ int *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_dNozzleSpan )( 
            IHead * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_dPhotocellRate )( 
            IHead * This,
            /* [retval][out] */ double *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_nEncoderDivisor )( 
            IHead * This,
            /* [retval][out] */ int *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_bDoublePulse )( 
            IHead * This,
            /* [retval][out] */ BOOL *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetElements )( 
            IHead * This,
            IStringArray *vElements,
            /* [retval][out] */ BOOL *pResult);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DeleteElement )( 
            IHead * This,
            /* [defaultvalue][optional] */ long lID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *AddElement )( 
            IHead * This,
            BSTR strElement,
            /* [optional] */ long *lID);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UpdateElement )( 
            IHead * This,
            BSTR strElement,
            /* [retval][out] */ long *pResult);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetCurrentState )( 
            IHead * This,
            IStringArray *vState,
            TASKSTATE *task,
            /* [retval][out] */ BOOL *pResult);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_m_bRemoteHead )( 
            IHead * This,
            /* [retval][out] */ long *pVal);
        
        END_INTERFACE
    } IHeadVtbl;

    interface IHead
    {
        CONST_VTBL struct IHeadVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHead_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IHead_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IHead_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IHead_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IHead_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IHead_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IHead_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IHead_get_m_lID(This,pVal)	\
    ( (This)->lpVtbl -> get_m_lID(This,pVal) ) 

#define IHead_get_m_lPanelID(This,pVal)	\
    ( (This)->lpVtbl -> get_m_lPanelID(This,pVal) ) 

#define IHead_get_m_strName(This,pVal)	\
    ( (This)->lpVtbl -> get_m_strName(This,pVal) ) 

#define IHead_get_m_strUID(This,pVal)	\
    ( (This)->lpVtbl -> get_m_strUID(This,pVal) ) 

#define IHead_get_m_lRelativeHeight(This,pVal)	\
    ( (This)->lpVtbl -> get_m_lRelativeHeight(This,pVal) ) 

#define IHead_get_m_nChannels(This,pVal)	\
    ( (This)->lpVtbl -> get_m_nChannels(This,pVal) ) 

#define IHead_get_m_nHorzRes(This,pVal)	\
    ( (This)->lpVtbl -> get_m_nHorzRes(This,pVal) ) 

#define IHead_get_m_lPhotocellDelay(This,pVal)	\
    ( (This)->lpVtbl -> get_m_lPhotocellDelay(This,pVal) ) 

#define IHead_get_m_nEncoder(This,pVal)	\
    ( (This)->lpVtbl -> get_m_nEncoder(This,pVal) ) 

#define IHead_get_m_nDirection(This,pVal)	\
    ( (This)->lpVtbl -> get_m_nDirection(This,pVal) ) 

#define IHead_get_m_bEnabled(This,pVal)	\
    ( (This)->lpVtbl -> get_m_bEnabled(This,pVal) ) 

#define IHead_get_m_bInverted(This,pVal)	\
    ( (This)->lpVtbl -> get_m_bInverted(This,pVal) ) 

#define IHead_get_m_dHeadAngle(This,pVal)	\
    ( (This)->lpVtbl -> get_m_dHeadAngle(This,pVal) ) 

#define IHead_get_m_nPhotocell(This,pVal)	\
    ( (This)->lpVtbl -> get_m_nPhotocell(This,pVal) ) 

#define IHead_get_m_nSharePhoto(This,pVal)	\
    ( (This)->lpVtbl -> get_m_nSharePhoto(This,pVal) ) 

#define IHead_get_m_nShareEnc(This,pVal)	\
    ( (This)->lpVtbl -> get_m_nShareEnc(This,pVal) ) 

#define IHead_get_m_type(This,pVal)	\
    ( (This)->lpVtbl -> get_m_type(This,pVal) ) 

#define IHead_get_m_nInternalTachSpeed(This,pVal)	\
    ( (This)->lpVtbl -> get_m_nInternalTachSpeed(This,pVal) ) 

#define IHead_get_m_dNozzleSpan(This,pVal)	\
    ( (This)->lpVtbl -> get_m_dNozzleSpan(This,pVal) ) 

#define IHead_get_m_dPhotocellRate(This,pVal)	\
    ( (This)->lpVtbl -> get_m_dPhotocellRate(This,pVal) ) 

#define IHead_get_m_nEncoderDivisor(This,pVal)	\
    ( (This)->lpVtbl -> get_m_nEncoderDivisor(This,pVal) ) 

#define IHead_get_m_bDoublePulse(This,pVal)	\
    ( (This)->lpVtbl -> get_m_bDoublePulse(This,pVal) ) 

#define IHead_GetElements(This,vElements,pResult)	\
    ( (This)->lpVtbl -> GetElements(This,vElements,pResult) ) 

#define IHead_DeleteElement(This,lID)	\
    ( (This)->lpVtbl -> DeleteElement(This,lID) ) 

#define IHead_AddElement(This,strElement,lID)	\
    ( (This)->lpVtbl -> AddElement(This,strElement,lID) ) 

#define IHead_UpdateElement(This,strElement,pResult)	\
    ( (This)->lpVtbl -> UpdateElement(This,strElement,pResult) ) 

#define IHead_GetCurrentState(This,vState,task,pResult)	\
    ( (This)->lpVtbl -> GetCurrentState(This,vState,task,pResult) ) 

#define IHead_get_m_bRemoteHead(This,pVal)	\
    ( (This)->lpVtbl -> get_m_bRemoteHead(This,pVal) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IHead_INTERFACE_DEFINED__ */


#ifndef ___DBoxWriter_DISPINTERFACE_DEFINED__
#define ___DBoxWriter_DISPINTERFACE_DEFINED__

/* dispinterface _DBoxWriter */
/* [hidden][helpstring][uuid] */ 


EXTERN_C const IID DIID__DBoxWriter;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("49D715D3-0166-4774-BFAC-F8E57EE482D7")
    _DBoxWriter : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DBoxWriterVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DBoxWriter * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DBoxWriter * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DBoxWriter * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DBoxWriter * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DBoxWriter * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DBoxWriter * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DBoxWriter * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _DBoxWriterVtbl;

    interface _DBoxWriter
    {
        CONST_VTBL struct _DBoxWriterVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DBoxWriter_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _DBoxWriter_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _DBoxWriter_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _DBoxWriter_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _DBoxWriter_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _DBoxWriter_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _DBoxWriter_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DBoxWriter_DISPINTERFACE_DEFINED__ */


#ifndef ___DBoxWriterEvents_DISPINTERFACE_DEFINED__
#define ___DBoxWriterEvents_DISPINTERFACE_DEFINED__

/* dispinterface _DBoxWriterEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__DBoxWriterEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("667970BD-0EC2-41B2-99F6-5328479B50F4")
    _DBoxWriterEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DBoxWriterEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DBoxWriterEvents * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DBoxWriterEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DBoxWriterEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DBoxWriterEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DBoxWriterEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DBoxWriterEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DBoxWriterEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _DBoxWriterEventsVtbl;

    interface _DBoxWriterEvents
    {
        CONST_VTBL struct _DBoxWriterEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DBoxWriterEvents_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _DBoxWriterEvents_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _DBoxWriterEvents_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _DBoxWriterEvents_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _DBoxWriterEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _DBoxWriterEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _DBoxWriterEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DBoxWriterEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_BoxWriter;

#ifdef __cplusplus

class DECLSPEC_UUID("2E5D9215-26EF-4024-8BAC-93C45C8E6C9D")
BoxWriter;
#endif

EXTERN_C const CLSID CLSID_Printer;

#ifdef __cplusplus

class DECLSPEC_UUID("FE7667D1-F896-4E5A-897D-9AC727ADBCDE")
Printer;
#endif

EXTERN_C const CLSID CLSID_Line;

#ifdef __cplusplus

class DECLSPEC_UUID("7B0962EE-BDDD-4343-95E7-B210914C8E57")
Line;
#endif

EXTERN_C const CLSID CLSID_StringArray;

#ifdef __cplusplus

class DECLSPEC_UUID("0A743697-6DED-4420-878D-CBC4713DFC73")
StringArray;
#endif

EXTERN_C const CLSID CLSID_Head;

#ifdef __cplusplus

class DECLSPEC_UUID("E46A0CBC-293B-43F1-AE8E-3F134007321E")
Head;
#endif
#endif /* __BoxWriter_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


