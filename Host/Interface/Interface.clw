; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CBoxWriterCtrl
LastTemplate=CWinThread
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Interface.h"
CDK=Y

ClassCount=5
Class1=CBoxWriterCtrl
Class2=CBoxWriterPropPage

ResourceCount=1
LastPage=0
Class3=CErrorThread
Class4=CErrorSocket
Class5=CSerialThread
Resource1=IDD_PROPPAGE_BOXWRITER

[CLS:CBoxWriterCtrl]
Type=0
HeaderFile=BoxWriterCtl.h
ImplementationFile=BoxWriterCtl.cpp
Filter=W
BaseClass=COleControl
VirtualFilter=wWC
LastObject=CBoxWriterCtrl

[CLS:CBoxWriterPropPage]
Type=0
HeaderFile=BoxWriterPpg.h
ImplementationFile=BoxWriterPpg.cpp
Filter=D

[DLG:IDD_PROPPAGE_BOXWRITER]
Type=1
Class=CBoxWriterPropPage
ControlCount=0

[CLS:CErrorThread]
Type=0
HeaderFile=ErrorThread.h
ImplementationFile=ErrorThread.cpp
BaseClass=CWinThread
Filter=N
LastObject=CErrorThread
VirtualFilter=TC

[CLS:CErrorSocket]
Type=0
HeaderFile=ErrorSocket.h
ImplementationFile=ErrorSocket.cpp
BaseClass=CSocket
Filter=N
VirtualFilter=uq

[CLS:CSerialThread]
Type=0
HeaderFile=SerialThread.h
ImplementationFile=SerialThread.cpp
BaseClass=CWinThread
Filter=N
VirtualFilter=TC
LastObject=CSerialThread

