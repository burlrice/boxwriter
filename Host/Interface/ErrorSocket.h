#if !defined(AFX_ERRORSOCKET_H__04A05A85_0EEE_4AA6_A701_2B512785ACA3__INCLUDED_)
#define AFX_ERRORSOCKET_H__04A05A85_0EEE_4AA6_A701_2B512785ACA3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ErrorSocket.h : header file
//

#include <afxsock.h>

class CErrorThread;

/////////////////////////////////////////////////////////////////////////////
// CErrorSocket command target

class CErrorSocket : public CAsyncSocket
{
// Attributes
public:

// Operations
public:
	CErrorSocket(CErrorThread * pThread);
	virtual ~CErrorSocket();

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CErrorSocket)
	public:
	virtual void OnReceive(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CErrorSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
	CErrorThread * m_pThread;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ERRORSOCKET_H__04A05A85_0EEE_4AA6_A701_2B512785ACA3__INCLUDED_)
