// Line.h: Definition of the CLine class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LINE_H__4226FB49_4861_47E2_B909_AAEE0A45ACD8__INCLUDED_)
#define AFX_LINE_H__4226FB49_4861_47E2_B909_AAEE0A45ACD8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols

class CHead;

/////////////////////////////////////////////////////////////////////////////
// CLine

class CLine : 
	public IDispatchImpl<ILine, &IID_ILine, &LIBID_BoxWriter>, 
	public ISupportErrorInfo,
	public CComObjectRoot,
	public CComCoClass<CLine,&CLSID_Line>
{
public:
	friend class CPrinter;

	CLine();
	~CLine();

BEGIN_COM_MAP(CLine)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ILine)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()
//DECLARE_NOT_AGGREGATABLE(CLine) 
// Remove the comment from the line above if you don't want your object to 
// support aggregation. 

DECLARE_REGISTRY_RESOURCEID(IDR_Line)
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);
//	virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid,void  **ppv);

// ILine
public:
	STDMETHOD(get_m_nHeads)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_m_head)(long lID, /*[out, retval]*/ IHead* *pVal);
	STDMETHOD(get_m_lCount)(/*[out, retval]*/ unsigned __int64 *pVal);
	STDMETHOD(put_m_lCount)(/*[in]*/ unsigned __int64 newVal);
	STDMETHOD(get_m_strName)(/*[out, retval]*/ BSTR *pVal);

	STDMETHOD(Load)(BSTR strTask, /*[out, retval]*/ BOOL * pResult);
	STDMETHOD(Resume)(/*[out, retval]*/ BOOL * pResult);
	STDMETHOD(Idle)(/*[out, retval]*/ BOOL * pResult);
	STDMETHOD(Stop)(/*[out, retval]*/ BOOL * pResult);
	STDMETHOD(Start)(/*[out, retval]*/ BOOL * pResult);
	STDMETHOD(DatabaseStart)(BSTR strKeyValue, /*[out, retval]*/ BOOL * pResult);
	STDMETHOD(GetTasks)(IStringArray * vTasks, /*[out, retval]*/ long * pResult);
	STDMETHOD(GetCurrentTask)(BSTR * strTask, /*[out, retval]*/ BOOL * pResult);
	STDMETHOD(SetUserElementData)(IStringArray * vPrompt, IStringArray * vData, /*[out, retval]*/ long * pResult);
	STDMETHOD(GetUserElementData)(IStringArray * vPrompt, IStringArray * vData, /*[out, retval]*/ BOOL * pResult);

	CComObject <CHead> * GetHead (long lIndex);

	CString m_strName;
	CPrinter * m_pPrinter;
	ULONG m_lID;
	CArray <CComObject <CHead> *, CComObject <CHead> *> m_vHeads;
	STDMETHOD(DatabaseStart)(BSTR strKeyValue);
	STDMETHOD(get_m_strSerialData)(BSTR* pVal);
	STDMETHOD(put_m_strSerialData)(BSTR newVal);
};

#endif // !defined(AFX_LINE_H__4226FB49_4861_47E2_B909_AAEE0A45ACD8__INCLUDED_)
