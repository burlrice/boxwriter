// ErrorSocket.cpp : implementation file
//

#include "stdafx.h"
#include "Interface.h"
#include "ErrorSocket.h"
#include "ErrorThread.h"
#include "Debug.h"
#include "Printer.h"
#include "Parse.h"
#include "Head.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CErrorSocket

CErrorSocket::CErrorSocket(CErrorThread * pThread)
:	m_pThread (pThread)
{
	ASSERT (pThread);
}

CErrorSocket::~CErrorSocket()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CErrorSocket, CAsyncSocket)
	//{{AFX_MSG_MAP(CErrorSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CErrorSocket member functions

void CErrorSocket::OnReceive(int nErrorCode) 
{
	CString strAddr;
	UINT nPort = 0;
	char sz [512] = { 0 };

	int nRecv = ReceiveFrom (sz, sizeof (sz), strAddr, nPort);

	ASSERT (m_pThread);
	ASSERT (m_pThread->m_pPrinter);

	if (nRecv) {
		CStringArray v;

		#if 0 //#ifdef _DEBUG
		{
			CString str;

			str.Format ("CErrorSocket::OnReceive: %s [%d]", strAddr, nPort);
			TRACEF (str);
		}

		TRACEF (sz);
		#endif

		FoxjetDatabase::FromString (a2w (sz), v);

		if (v.GetSize () >= 2) {
			CString strCmd = v [0];
			CString strLine = v [1];
			ULONG lLineID = m_pThread->m_pPrinter->GetLineID (strLine);
			UINT nMsg;
			
			if (const CPrinter * pPrinter = theApp.FindPrinter (strAddr)) {
				if (!strCmd.CompareNoCase (BW_HOST_PACKET_PRINTER_EOP)) {
					ULONG lHeadID	= _tcstoul (FoxjetDatabase::GetParam (v, 1), NULL, 10);
					unsigned __int64 lCount	= _tcstoui64 (FoxjetDatabase::GetParam (v, 2), NULL, 10);

					theApp.OnEOP (lHeadID, lCount);
				}
				else if (!strCmd.CompareNoCase (BW_HOST_PACKET_PRINTER_SOP)) {
					ULONG lHeadID	= _tcstoul (FoxjetDatabase::GetParam (v, 1), NULL, 10);
					unsigned __int64 lCount	= _tcstoui64 (FoxjetDatabase::GetParam (v, 2), NULL, 10);

					theApp.OnSOP (lHeadID, lCount);
				}
				else if (!strCmd.CompareNoCase (BW_HOST_PACKET_PRINTER_HEAD_STATE_CHANGE)) {
					ULONG lHeadID	= _tcstoul (FoxjetDatabase::GetParam (v, 1), NULL, 10);

					theApp.OnHeadStateChanged (lHeadID, a2w (sz));
				}
				else {
					if (!strCmd.CompareNoCase (BW_HOST_PACKET_GET_CURRENT_STATE))
						nMsg = TM_ERROR_RAISED;
					else  
						nMsg = TM_ERROR_CLEARED;

					while (!::PostThreadMessage (m_pThread->m_nThreadID, nMsg, (WPARAM)lLineID, (LPARAM)pPrinter))
						::Sleep (0);
				}
			}
		}
	}
	
	CAsyncSocket::OnReceive(nErrorCode);
}

 
