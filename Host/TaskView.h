// TaskView.h : interface of the CTaskView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TASKVIEW_H__D7752ABC_F801_42F5_B286_997A5164AC45__INCLUDED_)
#define AFX_TASKVIEW_H__D7752ABC_F801_42F5_B286_997A5164AC45__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CTaskView : public CView
{
protected: // create from serialization only
	CTaskView();
	DECLARE_DYNCREATE(CTaskView)

// Attributes
public:
	CHostDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTaskView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTaskView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CTaskView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	afx_msg void OnStyleChanged(int nStyleType, LPSTYLESTRUCT lpStyleStruct);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in TaskView.cpp
inline CHostDoc* CTaskView::GetDocument()
   { return (CHostDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TASKVIEW_H__D7752ABC_F801_42F5_B286_997A5164AC45__INCLUDED_)
