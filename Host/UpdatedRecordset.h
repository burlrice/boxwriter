#ifndef __UPDATEDRECORDSET_H__
#define __UPDATEDRECORDSET_H__

#include "OdbcDatabase.h"
#include "OdbcRecordset.h"


typedef struct
{
	ULONG m_lTaskID;
	ULONG m_lPrinterID;
} UPDATEDSTRUCT;

const FoxjetDatabase::COdbcRecordset & operator >> (const FoxjetDatabase::COdbcRecordset & rst, UPDATEDSTRUCT & s);
FoxjetDatabase::COdbcRecordset & operator << (FoxjetDatabase::COdbcRecordset & rst, const UPDATEDSTRUCT & s);
// throw CDBException, CMemoryException 

bool AddUpdatedRecord (FoxjetDatabase::COdbcDatabase & database, UPDATEDSTRUCT & s);
bool GetUpdatedRecords (FoxjetDatabase::COdbcDatabase & database, ULONG lPrinterID, CArray <UPDATEDSTRUCT, UPDATEDSTRUCT &> & v);
bool DeletePrinterRecords (FoxjetDatabase::COdbcDatabase & database, ULONG lPrinterID);

#endif //__UPDATEDRECORDSET_H__
