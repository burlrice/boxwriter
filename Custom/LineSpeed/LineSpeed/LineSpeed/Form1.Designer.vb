﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.lblDisplay = New System.Windows.Forms.Label()
        Me.dlgFont = New System.Windows.Forms.FontDialog()
        Me.bw = New AxBoxWriter.AxBoxWriter()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.bw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblDisplay
        '
        Me.lblDisplay.AutoEllipsis = True
        Me.lblDisplay.BackColor = System.Drawing.SystemColors.Control
        Me.lblDisplay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDisplay.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblDisplay.Font = New System.Drawing.Font("Microsoft Sans Serif", 72.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisplay.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.lblDisplay.Location = New System.Drawing.Point(0, 0)
        Me.lblDisplay.Margin = New System.Windows.Forms.Padding(0)
        Me.lblDisplay.Name = "lblDisplay"
        Me.lblDisplay.Size = New System.Drawing.Size(461, 110)
        Me.lblDisplay.TabIndex = 0
        Me.lblDisplay.Text = "200 fpm"
        '
        'bw
        '
        Me.bw.Enabled = True
        Me.bw.Location = New System.Drawing.Point(361, 60)
        Me.bw.Name = "bw"
        Me.bw.OcxState = CType(resources.GetObject("bw.OcxState"), System.Windows.Forms.AxHost.State)
        Me.bw.Size = New System.Drawing.Size(100, 50)
        Me.bw.TabIndex = 1
        '
        'Timer1
        '
        '
        'Timer2
        '
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(504, 111)
        Me.Controls.Add(Me.bw)
        Me.Controls.Add(Me.lblDisplay)
        Me.Name = "Form1"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.Text = "Line speed"
        CType(Me.bw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lblDisplay As Label
    Friend WithEvents dlgFont As FontDialog
    Friend WithEvents bw As AxBoxWriter.AxBoxWriter
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Timer2 As Timer
End Class
