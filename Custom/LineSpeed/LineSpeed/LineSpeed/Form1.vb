﻿Imports AxBoxWriter

Public Class Form1
    Public m_printer As New BoxWriter.PRINTER
    Public m_lMaster As Int32 = 0

    Private Sub Connect()
        lblDisplay.Text = ""

        m_printer = New BoxWriter.PRINTER

        If (m_printer.Connect("127.0.0.1") = 0) Then
            lblDisplay.Text = "..."
            Timer1.Interval = 3000
            Timer1.Start()
        Else
            m_lMaster = m_printer.m_line.m_head(0).m_lID
            lblDisplay.Text = m_printer.m_line.m_head(0).m_nInternalTachSpeed
            Timer1.Stop()
        End If
    End Sub

    Private Sub DoResize()
        Dim newSize = New System.Drawing.Size(Me.Size - New System.Drawing.Size(10, 35))

        lblDisplay.Location = New System.Drawing.Size(0, 0)
        lblDisplay.Size = newSize
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Opacity = 0.75
        DoResize()
        Connect()
        Timer2.Interval = 1000
        Timer2.Start()
    End Sub

    Private Sub Form1_ResizeEnd(sender As Object, e As EventArgs) Handles Me.ResizeEnd
        DoResize()
    End Sub

    Private Sub lblDisplay_DoubleClick(sender As Object, e As EventArgs) Handles lblDisplay.DoubleClick
        dlgFont.Font = lblDisplay.Font

        If dlgFont.ShowDialog <> DialogResult.Cancel Then
            lblDisplay.Font = dlgFont.Font
        End If
    End Sub

    Private Sub bw_OnHeadStateChanged(sender As Object, e As _DBoxWriterEvents_OnHeadStateChangedEvent) Handles bw.OnHeadStateChanged
        Dim vState As New BoxWriter.StringArray

        vState.FromString(e.str)

        If (vState.Count >= 3) Then
            Dim v As New BoxWriter.StringArray

            v.FromString(vState.GetAt(2))

            If (v.Count >= 6) Then
                Dim lID As Int32 = CInt(vState.GetAt(1))

                If (lID = m_lMaster) Then
                    Debug.WriteLine(e.str)
                    lblDisplay.Text = Format(CDbl(v.GetAt(5)), "000.0") & " fpm"
                    TopMost = True
                End If
            End If
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Connect()
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        If (TopMost = False) Then
            TopMost = True
        End If
    End Sub
End Class
