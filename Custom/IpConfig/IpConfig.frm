VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmIpConfig 
   Caption         =   "IP Config"
   ClientHeight    =   5505
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7365
   LinkTopic       =   "Form1"
   ScaleHeight     =   5505
   ScaleWidth      =   7365
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer timerCmd 
      Left            =   2280
      Top             =   120
   End
   Begin VB.CommandButton btnExecute 
      Caption         =   "Execute"
      Height          =   375
      Left            =   6240
      TabIndex        =   7
      Top             =   600
      Width           =   1095
   End
   Begin VB.CommandButton btnConnect 
      Caption         =   "Connect"
      Height          =   375
      Left            =   2400
      TabIndex        =   6
      Top             =   600
      Width           =   1095
   End
   Begin VB.ComboBox cbScript 
      Height          =   315
      Left            =   3840
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   600
      Width           =   2295
   End
   Begin MSComctlLib.ListView lv 
      Height          =   4455
      Left            =   0
      TabIndex        =   2
      Top             =   1080
      Width           =   7335
      _ExtentX        =   12938
      _ExtentY        =   7858
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Command"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Data"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Response"
         Object.Width           =   5292
      EndProperty
   End
   Begin VB.TextBox txtAddr 
      Height          =   285
      Left            =   120
      TabIndex        =   1
      Text            =   "192.168.2.124"
      Top             =   600
      Width           =   2175
   End
   Begin MSWinsockLib.Winsock sockUDP 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      Protocol        =   1
   End
   Begin MSWinsockLib.Winsock sockTCP 
      Left            =   480
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Label Label2 
      Caption         =   "Script"
      Height          =   255
      Left            =   3840
      TabIndex        =   5
      Top             =   240
      Width           =   2295
   End
   Begin VB.Label Label1 
      Caption         =   "Address"
      Height          =   255
      Index           =   1
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Address"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   1455
   End
End
Attribute VB_Name = "frmIpConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Private db As Database
Private lBatchIDCounter As Long
Private lCmdsPending As Long

Public Function GetCmdName(ByVal lCmdID As Long)
    Dim rst As Recordset
    
    Set rst = db.OpenRecordset("SELECT * FROM Commands WHERE ID=" & lCmdID)
    
    If (Not rst.EOF) Then
        GetCmdName = rst("Name")
    Else
        GetCmdName = "[Unknown]"
    End If
End Function

Public Function GetState(ByVal nState As Integer)

    Select Case (nState)
        Case sckClosed
            GetState = "Closed"
            Exit Function
        Case sckClosing
            GetState = "Closing"
            Exit Function
        Case sckConnected
            GetState = "Connected"
            Exit Function
        Case sckConnecting
            GetState = "Connecting"
            Exit Function
        Case sckConnectionPending
            GetState = "ConnectionPending"
            Exit Function
        Case sckError
            GetState = "Error"
            Exit Function
        Case sckHostResolved
            GetState = "HostResolved"
            Exit Function
        Case sckOpen
            GetState = "Open"
            Exit Function
        Case sckResolvingHost
            GetState = "ResolvingHost"
            Exit Function
            
    End Select

    GetState = "[Unknown state]"
    Exit Function
    
End Function


Public Function CloseSocket()
    sockTCP.Close
    btnConnect.Caption = "Connect"
    btnConnect.Enabled = True
    btnExecute.Enabled = False
    txtAddr.Enabled = True
End Function

Private Sub btnConnect_Click()
    If Not StrComp(btnConnect.Caption, "Disconnect") Then
        CloseSocket
    Else
        btnConnect.Enabled = False
        btnExecute.Enabled = True
        txtAddr.Enabled = False
        sockTCP.Connect txtAddr.Text, 1281
    End If
End Sub

Private Sub cbScript_Click()
    Dim strSQL As String
    
    strSQL = _
        "SELECT ScriptCmds.*, Commands.Name " & _
        "FROM Commands INNER JOIN (Scripts INNER JOIN ScriptCmds ON Scripts.ID = ScriptCmds.ScriptID) ON Commands.ID = ScriptCmds.CmdID " & _
        "Where (((Scripts.Name) = '" & cbScript.Text & "')) " & _
        "ORDER BY ScriptCmds.Order; "

    Set rst = db.OpenRecordset(strSQL)
    
    lv.ListItems.Clear
    lv.FullRowSelect = True
    lv.LabelEdit = lvwManual
    
    While (Not rst.EOF)
        Dim item As ListItem
        Dim strData As String
        
        Set item = lv.ListItems.Add(, , Nz(rst("Name")))    ' name      0
        
        strData = Nz(rst("Data"))
        
        item.ListSubItems.Add , , strData                   ' data      1
        item.ListSubItems.Add , , ""                        ' response  2
        item.ListSubItems.Add , , rst("CmdID")              ' cmdID     3
        item.ListSubItems.Add , , lBatchIDCounter           ' batchID   4
        item.ListSubItems.Add , , 0                         ' state     5
        item.ListSubItems.Add , , Len(strData)              ' len sent  6
        item.ListSubItems.Add , , 0                         ' len recv  7
        
        lBatchIDCounter = lBatchIDCounter + 1
        
        rst.MoveNext
    Wend
        
End Sub

Private Sub Form_Load()
    Dim rst As Recordset
    
    Set db = DBEngine.Workspaces(0).OpenDatabase(App.Path & "\IpConfig.mdb")
    Set rst = db.OpenRecordset("Scripts")
    
    btnExecute.Enabled = False
    lBatchIDCounter = 1
    cbScript.Clear
    
    While (Not rst.EOF)
        cbScript.AddItem (rst("Name"))
        rst.MoveNext
    Wend
    
    cbScript.ListIndex = 0
    
'    Dim packet As FJSOCKETPACKET
'    Dim buffer() As Byte
'
'    packet.lCmd = 256
'    packet.lBatchID = 1
'    packet.lLength = 0
'
'    buffer = ToRawData(packet)
'
'    sockUDP.RemoteHost = "255.255.255.255"
'    sockUDP.RemotePort = 1281
'    sockUDP.SendData buffer
End Sub


Private Sub lv_DblClick()
    Dim lCmd As Long
    
    Load dlgPacket
    
    lCmd = CLng(lv.SelectedItem.ListSubItems(3).Text)
    
    dlgPacket.txtCmd = GetCmdName(lCmd) & " [" & lCmd & "]"
    dlgPacket.txtBatch = lv.SelectedItem.ListSubItems(4).Text
    dlgPacket.txtLength = lv.SelectedItem.ListSubItems(6).Text
    dlgPacket.txtData = lv.SelectedItem.ListSubItems(1).Text
    dlgPacket.txtResponse = lv.SelectedItem.ListSubItems(2).Text
    dlgPacket.txtLenRecv = lv.SelectedItem.ListSubItems(7).Text
    
    dlgPacket.Show 1, Me
End Sub

Private Sub sockTCP_Close()
    CloseSocket
End Sub

Private Sub sockTCP_Connect()
    Dim buffer() As Byte
    Dim nDataLen As Long
    
    btnConnect.Caption = "Disconnect"
    btnConnect.Enabled = True
            
End Sub



Private Sub sockTCP_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    MsgBox Description
    CloseSocket
End Sub

Private Sub sockUDP_DataArrival(ByVal bytesTotal As Long)
    Dim buffer() As Byte
    Dim Packet As FJSOCKETPACKET
    
    sockTCP.GetData buffer, vbByte
    FromRawData Packet, buffer, bytesTotal
    
    MsgBox Packet.strCmd
End Sub

Public Function ExecNextCmd() As Boolean
    ExecNextCmd = False
    
    For i = 1 To lv.ListItems.Count
        Dim Packet As FJSOCKETPACKET
        Dim item As ListItem
        Dim lBatchID As Long
        Dim lState As Long
        
        Set item = lv.ListItems(i)
        lBatchID = CLng(item.ListSubItems(4).Text)
        lState = CLng(item.ListSubItems(5).Text)
       
        If (lBatchID > 0 And lState = 0) Then
            Packet.lCmd = CLng(Nz(item.ListSubItems(3).Text, 0))
            Packet.lBatchID = CLng(Nz(item.ListSubItems(4).Text, 0))
            Packet.strCmd = item.ListSubItems(1).Text
            Packet.lLength = Len(Packet.strCmd)
            
            item.ListSubItems(5) = 1
            
'            MsgBox "Send:" & vbCrLf & _
                GetCmdName(packet.lCmd) & " [" & packet.lCmd & "]" & vbCrLf & _
                "BatchID: " & packet.lBatchID & vbCrLf & _
                "Length: " & packet.lLength & vbCrLf & _
                packet.strCmd & vbCrLf
    
            buffer = ToRawData(Packet)
        
            sockTCP.SendData buffer
            Sleep (100) ' gives controller time to process packet
            ' if Winsock::SendData is called too quickly, it appears to send only the last command
                        
            timerCmd.Enabled = True
            timerCmd.Interval = 1000
            lCmdsPending = lCmdsPending - 1
            
            ExecNextCmd = True
            Exit Function
        End If
    Next
End Function

Private Sub btnExecute_Click()
    lCmdsPending = lv.ListItems.Count
    ExecNextCmd
End Sub

Private Sub sockTCP_DataArrival(ByVal bytesTotal As Long)
    Dim buffer() As Byte
    Dim Packet As FJSOCKETPACKET
    Dim bFound As Boolean
    
    timerCmd.Enabled = False
    bFound = False
        
    sockTCP.GetData buffer, vbByte
    FromRawData Packet, buffer, bytesTotal
    
'    MsgBox "Recv:" & vbCrLf & _
        GetCmdName(packet.lCmd) & " [" & packet.lCmd & "]" & vbCrLf & _
        "BatchID: " & packet.lBatchID & vbCrLf & _
        "Length: " & packet.lLength & vbCrLf & _
        packet.strCmd & vbCrLf
        
    For i = 1 To lv.ListItems.Count
        Dim item As ListItem
        
        Set item = lv.ListItems(i)
        
        If (CLng(item.ListSubItems(4).Text) = Packet.lBatchID) Then
            item.ListSubItems(2).Text = Packet.strCmd
            item.ListSubItems(7).Text = Packet.lLength
            bFound = True
        End If
    Next
    
    If (Not bFound) Then
        Set item = lv.ListItems.Add(, , GetCmdName(Packet.lCmd))    ' name      0
        
        item.ListSubItems.Add , , ""                                ' data      1
        item.ListSubItems.Add , , Packet.strCmd                     ' response  2
        item.ListSubItems.Add , , ""                                ' cmdID     3
        item.ListSubItems.Add , , "0"                               ' batchID   4
        item.ListSubItems.Add , , -1                                ' state     5
        item.ListSubItems.Add , , 0                                 ' len sent  6
        item.ListSubItems.Add , , Packet.lLength                    ' len recv  7
    End If
    
    
    ExecNextCmd
    
End Sub

Private Sub timerCmd_Timer()
    If (lCmdsPending <= 0) Then
        timerCmd.Enabled = False
    Else
        ExecNextCmd
    End If
End Sub
