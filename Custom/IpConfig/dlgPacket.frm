VERSION 5.00
Begin VB.Form dlgPacket 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Packet"
   ClientHeight    =   4995
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   6030
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4995
   ScaleWidth      =   6030
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtLenRecv 
      Enabled         =   0   'False
      Height          =   375
      Left            =   240
      TabIndex        =   11
      Top             =   3360
      Width           =   1455
   End
   Begin VB.TextBox txtResponse 
      Enabled         =   0   'False
      Height          =   375
      Left            =   240
      TabIndex        =   10
      Text            =   "txtResponse"
      Top             =   2520
      Width           =   5175
   End
   Begin VB.TextBox txtData 
      Enabled         =   0   'False
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Text            =   "Text1"
      Top             =   1320
      Width           =   5775
   End
   Begin VB.TextBox txtLength 
      Enabled         =   0   'False
      Height          =   375
      Left            =   4440
      TabIndex        =   6
      Text            =   "Text1"
      Top             =   480
      Width           =   1455
   End
   Begin VB.TextBox txtBatch 
      Enabled         =   0   'False
      Height          =   375
      Left            =   2880
      TabIndex        =   4
      Text            =   "Text1"
      Top             =   480
      Width           =   1455
   End
   Begin VB.TextBox txtCmd 
      Enabled         =   0   'False
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Text            =   "Text1"
      Top             =   480
      Width           =   2655
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "OK"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   4440
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Response"
      Height          =   2055
      Left            =   120
      TabIndex        =   13
      Top             =   2040
      Width           =   5775
   End
   Begin VB.Label Label6 
      Caption         =   "Length"
      Height          =   375
      Left            =   240
      TabIndex        =   12
      Top             =   3000
      Width           =   1215
   End
   Begin VB.Label Label5 
      Caption         =   "Data"
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   2160
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "Data"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   960
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "Length"
      Height          =   255
      Left            =   4440
      TabIndex        =   5
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "Batch ID"
      Height          =   255
      Left            =   2880
      TabIndex        =   3
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Cmd"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1455
   End
End
Attribute VB_Name = "dlgPacket"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub OKButton_Click()
    Hide
End Sub
