Attribute VB_Name = "Module1"
Public Declare Function ntohl Lib "ws2_32.dll" (ByVal netlong As Long) As Long
Public Declare Function htonl Lib "ws2_32.dll" (ByVal hostlong As Long) As Long
Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (hpvDest As Any, hpvSource As Any, ByVal cbCopy As Long)
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

    
Public Type FJSOCKETPACKET
    lCmd As Long
    lBatchID As Long
    lLength As Long
    strCmd As String
End Type


Public Function Nz(ByRef s As Variant, Optional ByVal def As Variant = "") As String
    If IsNull(s) Then
        Nz = def
    Else
        Nz = s
    End If
End Function

 
Public Function FromRawData(ByRef Packet As FJSOCKETPACKET, ByRef buffer() As Byte, ByVal bytesTotal As Long)
    CopyMemory Packet.lCmd, buffer(0), 4
    CopyMemory Packet.lBatchID, buffer(4), 4
    CopyMemory Packet.lLength, buffer(8), 4
    
    Packet.lCmd = ntohl(Packet.lCmd)
    Packet.lBatchID = ntohl(Packet.lBatchID)
    Packet.lLength = ntohl(Packet.lLength)
    
    For i = 12 To bytesTotal - 1
        Packet.strCmd = Packet.strCmd & Chr(buffer(i))
    Next
End Function

Public Function ToRawData(ByRef Packet As FJSOCKETPACKET)
    Dim str As String
    Dim Cmd() As Byte
    Dim BatchID() As Byte
    Dim Length() As Byte
    
    ReDim buffer(12 + Len(Packet.strCmd)) As Byte
    
    CopyMemory buffer(0), htonl(Packet.lCmd), 4
    CopyMemory buffer(4), htonl(Packet.lBatchID), 4
    CopyMemory buffer(8), htonl(Packet.lLength), 4
    
    For i = 1 To Len(Packet.strCmd)
        buffer(i + 11) = Asc(Mid(Packet.strCmd, i, i))
    Next
    
    ToRawData = buffer
End Function


