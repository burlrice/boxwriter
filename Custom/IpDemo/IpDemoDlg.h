// IpDemoDlg.h : header file
//

#if !defined(AFX_IPDEMODLG_H__F23C091F_ACB1_46F4_B09F_7ECEE57DE0AE__INCLUDED_)
#define AFX_IPDEMODLG_H__F23C091F_ACB1_46F4_B09F_7ECEE57DE0AE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxmt.h>
#include <afxdao.h>

#include "fj_socket.h"


typedef struct
{
	_FJ_SOCKET_MESSAGE send;
	_FJ_SOCKET_MESSAGE recv;
} IPCSTRUCT;

/////////////////////////////////////////////////////////////////////////////
// CIpDemoDlg dialog

class CIpDemoDlg : public CDialog
{
// Construction
public:
	CIpDemoDlg(CWnd* pParent = NULL);	// standard constructor

public:
	static CString ToString (const _FJ_SOCKET_MESSAGE & s);

	CDaoDatabase m_db;

// Dialog Data
	//{{AFX_DATA(CIpDemoDlg)
	enum { IDD = IDD_IPDEMO_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIpDemoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();
	void KillThread();
	int GetSelectedIndex ();
	int Add (DWORD dw, const CString & str);
	int Add (IPCSTRUCT * pItem);

	static void CheckRx (CIpDemoDlg & dlg, CSocket & s);
	static bool CheckTx (CIpDemoDlg & dlg, CSocket & s, ULONG & lBatchID);

	static ULONG CALLBACK ThreadFunc (LPVOID lpData);

	HICON m_hIcon;
	HANDLE m_hThread;
	HANDLE m_hExitEvent;
	CCriticalSection m_cs;


	// Generated message map functions
	//{{AFX_MSG(CIpDemoDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnConnect();
	afx_msg void OnDestroy();
	afx_msg void OnProperties();
	afx_msg void OnNew();
	afx_msg void OnDblclkCmds(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDemo();
	afx_msg void OnClear();
	afx_msg void OnClose();
	afx_msg void OnSelchangeScript();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IPDEMODLG_H__F23C091F_ACB1_46F4_B09F_7ECEE57DE0AE__INCLUDED_)
