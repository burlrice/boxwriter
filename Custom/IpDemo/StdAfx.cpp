// stdafx.cpp : source file that includes just the standard includes
//	IpDemo.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"


void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	CString str;

	str.Format ("%s(%d): %s\n", lpszFile, lLine, lpsz);
	::OutputDebugString (str);
}
