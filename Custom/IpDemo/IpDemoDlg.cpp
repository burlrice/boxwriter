// IpDemoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IpDemo.h"
#include "IpDemoDlg.h"
#include "fj_socket.h"
#include "CmdDlg.h"
#include "Debug.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CIpDemoApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIpDemoDlg dialog

CIpDemoDlg::CIpDemoDlg(CWnd* pParent /*=NULL*/)
:	m_hThread (NULL),
	m_hExitEvent (NULL),
	CDialog(CIpDemoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CIpDemoDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CIpDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIpDemoDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CIpDemoDlg, CDialog)
	//{{AFX_MSG_MAP(CIpDemoDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_CONNECT, OnConnect)
	ON_WM_DESTROY()
	ON_BN_CLICKED(BTN_PROPERTIES, OnProperties)
	ON_BN_CLICKED(BTN_NEW, OnNew)
	ON_NOTIFY(NM_DBLCLK, LV_CMDS, OnDblclkCmds)
	ON_BN_CLICKED(BTN_DEMO, OnDemo)
	ON_BN_CLICKED(BTN_CLEAR, OnClear)
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(CB_SCRIPT, OnSelchangeScript)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIpDemoDlg message handlers

BOOL CIpDemoDlg::OnInitDialog()
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);
	CComboBox * pScript = (CComboBox *)GetDlgItem (CB_SCRIPT);

	ASSERT (pLV);
	ASSERT (pScript);
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	if (CIPAddressCtrl * p = (CIPAddressCtrl *)GetDlgItem (IDC_ADDRESS)) {
		BYTE n [4] = 
		{
			theApp.GetProfileInt ("IP Address", "1", 192),
			theApp.GetProfileInt ("IP Address", "2", 168),
			theApp.GetProfileInt ("IP Address", "3", 2),
			theApp.GetProfileInt ("IP Address", "4", 163),
		};
		
		p->SetAddress (n [0], n [1], n [2], n [3]);
	}

	pLV->SetExtendedStyle (pLV->GetExtendedStyle () | LVS_EX_FULLROWSELECT);
	pLV->InsertColumn (0, "Command",	LVCFMT_LEFT, 110);
	pLV->InsertColumn (1, "Data",		LVCFMT_LEFT, 160);
	pLV->InsertColumn (2, "Response",	LVCFMT_LEFT, 160);

	try {
		m_db.Open ("IpConfig.mdb");

		CDaoRecordset rst (&m_db);

		rst.Open (dbOpenSnapshot, "SELECT * FROM Scripts;", dbReadOnly);

		while (!rst.IsEOF ()) {
			ULONG lID = rst.GetFieldValue ("ID").lVal;
			CString strName = (LPCTSTR)rst.GetFieldValue ("Name").bstrVal;

			int nIndex = pScript->AddString (strName);
			pScript->SetItemData (nIndex, lID);

			rst.MoveNext ();
		}
		
		pScript->SetCurSel (0);
		OnSelchangeScript ();
		rst.Close ();
	}
	catch (CDaoException * e)		{ e->ReportError (); e->Delete (); ASSERT (0); }
	catch (CMemoryException * e)	{ e->ReportError (); e->Delete (); ASSERT (0); }

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CIpDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CIpDemoDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CIpDemoDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CIpDemoDlg::OnConnect() 
{
	if (!m_hThread) {
		DWORD dw = 0;
		VERIFY (m_hExitEvent = ::CreateEvent (NULL, TRUE, FALSE, "Exit"));
		VERIFY (m_hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)ThreadFunc, (LPVOID)this, 0, &dw));
	}
	else
		KillThread ();
}

CString CIpDemoDlg::ToString (const _FJ_SOCKET_MESSAGE & s)
{
	CString str;

	str.Format ("%s [%d]: %s",
		GetIpcCmdName (s.Header.Cmd),
		s.Header.BatchID,
		s.Data);

	return str;
}

void CIpDemoDlg::KillThread()
{
	if (m_hThread) {
		BeginWaitCursor ();
		
		::SetEvent (m_hExitEvent);
		::Sleep (100);
		
		::WaitForSingleObject (m_hThread, 3000);

		if (m_hThread)
			TRACEF ("Failed to end thread");

		EndWaitCursor ();
	}
}

void CIpDemoDlg::OnDestroy() 
{
	CDialog::OnDestroy();	
}

void CIpDemoDlg::OnClose() 
{
	OnClear ();
	KillThread ();
	CDialog::OnClose();
}

void CIpDemoDlg::OnOK()
{

}

int CIpDemoDlg::GetSelectedIndex ()
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);

	ASSERT (pLV);

	for (int i = 0; i < pLV->GetItemCount (); i++) 
		if (pLV->GetItemState (i, (LVIS_SELECTED | LVIS_FOCUSED)))
			return i;

	return -1;
}

void CIpDemoDlg::OnProperties() 
{
	CSingleLock lock (&m_cs);

	lock.Lock ();

	if (lock.IsLocked ()) {
		int nIndex = GetSelectedIndex ();
		
		if (nIndex == -1)
			MessageBox ("Nothing selected");
		else {
			CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);

			ASSERT (pLV);

			if (IPCSTRUCT * p = (IPCSTRUCT *)pLV->GetItemData (nIndex)) {
				CCmdDlg dlg (this);

				dlg.m_dwCmd = p->send.Header.Cmd;
				dlg.m_strData = (LPCTSTR)p->send.Data;
				dlg.m_strResponse = (LPCTSTR)p->recv.Data;
				dlg.m_lBatchID = p->send.Header.BatchID;

				dlg.DoModal ();
			}
		}

		lock.Unlock ();
	}
}

void CIpDemoDlg::OnNew() 
{
	CCmdDlg dlg (this);
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);

	ASSERT (pLV);

	if (dlg.DoModal () == IDOK) 
		Add (dlg.m_dwCmd, dlg.m_strData);
}

void CIpDemoDlg::OnDblclkCmds(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnProperties ();	
	*pResult = 0;
}

int CIpDemoDlg::Add (IPCSTRUCT * pItem)
{
	BeginWaitCursor ();

	CSingleLock lock (&m_cs);
	int nIndex = -1;

	lock.Lock ();

	if (lock.IsLocked ()) {
		LV_ITEM lvi;
		CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);

		ASSERT (pLV);

		lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM; 
		lvi.iItem = pLV->GetItemCount (); 
		lvi.iSubItem = 0; 
		lvi.iImage = 0;
		lvi.pszText = _T (""); 
		lvi.lParam = (LPARAM)pItem;

		nIndex = pLV->InsertItem (&lvi);
		
		ASSERT (nIndex != -1);

		pLV->SetItemText (nIndex, 0, GetIpcCmdName (pItem->send.Header.Cmd));
		pLV->SetItemText (nIndex, 1, (LPCTSTR)pItem->send.Data);
		pLV->SetItemText (nIndex, 2, (LPCTSTR)pItem->recv.Data);

		pLV->EnsureVisible (nIndex, FALSE);

		lock.Unlock ();
	}

	EndWaitCursor ();

	return nIndex;
}

int CIpDemoDlg::Add (DWORD dw, const CString & str)
{
	BeginWaitCursor ();

	CSingleLock lock (&m_cs);
	int nIndex = -1;

	lock.Lock ();

	if (lock.IsLocked ()) {
		IPCSTRUCT * pItem = new IPCSTRUCT;
		CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);

		ASSERT (pLV);

		memset (pItem, 0, sizeof (IPCSTRUCT));

		int nLen = min (str.GetLength (), sizeof (pItem->send.Data));

		pItem->send.Header.Cmd = dw;
		memcpy (pItem->send.Data, str, nLen);

		nIndex = Add (pItem);
	}

	EndWaitCursor ();

	return nIndex;
}

void CIpDemoDlg::OnDemo() 
{
	struct
	{
		DWORD m_dw;
		LPCTSTR m_lpsz;
	}
	static const map [] =
	{
		{ IPC_SET_PH_TYPE,				"1" },
		{ IPC_SET_PRINTER_INFO,			"DIRECTION=LEFT_TO_RIGHT;SLANTVALUE=0;SLANTANGLE=90.000000;DISTANCE=1.000000;HEIGHT=0.000000;PHOTOCELL_BACK=F;PHOTOCELL_INTERNAL=F;OP_AUTOPRINT=10800;" },
		{ IPC_SET_SYSTEM_INFO,			"BC_N=0;BC_NAME=\"Mag 100\";BC_TY=2;BC_H=32;BC_CD=F;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=37;BC_B1=1;BC_B2=4;BC_B3=0;BC_B4=0;BC_N=1;BC_NAME=\"Mag  90\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=2;BC_VB=15;BC_QZ=85;BC_BASE=34;BC_B1=1;BC_B2=4;BC_B3=0;BC_B4=0;BC_N=2;BC_NAME=\"Mag  80\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=2;BC_VB=15;BC_QZ=78;BC_BASE=30;BC_B1=1;BC_B2=4;BC_B3=0;BC_B4=0;BC_N=3;BC_NAME=\"Mag  70\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=2;BC_VB=15;BC_QZ=75;BC_BASE=26;BC_B1=1;BC_B2=4;BC_B3=0;BC_B4=0;BC_N=4;BC_NAME=\"Mag  62\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=2;BC_VB=15;BC_QZ=50;BC_BASE=24;BC_B1=1;BC_B2=4;BC_B3=0;BC_B4=0;BC_N=5;BC_NAME=\"Custom 1\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;BC_N=6;BC_NAME=\"Custom 2\";BC_TY=1;BC_H=32;BC_CD=F;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;BC_N=7;BC_NAME=\"Peanut\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;UNITS=ENGLISH_INCHES;TIME=24;INTERNAL_CLOCK=T;SYS" },
		{ IPC_SET_PRINTER_INFO,			"AMS_INTERVAL=4.0" },
		{ IPC_SET_PRINTER_MODE,			"APS_MODE=DISABLE;" },
		{ IPC_GET_LABEL_ID_SELECTED,	"" },
		{ IPC_GET_SW_VER_INFO,			"" },
		{ IPC_GET_PRINTER_MODE,			"" },
		{ IPC_GET_LABEL_IDS,			"" },
		{ IPC_GET_FONT_IDS,				"" },
		{ IPC_GET_GA_VER_INFO,			"" },
		{ IPC_GET_LABEL_ID_SELECTED,	"" },
		{ IPC_EDIT_START,				"" },
		{ IPC_PUT_PRINTER_ELEMENT,		"{Label,\"Barcode\",20.0,20.0,0,3,F,\"Barcode\"}{Message,\"Barcode\",1,\"Barcode-Barcode-4\"}{Barcode,\"Barcode-Barcode-4\",0.000000,0,D,0,1,F,F,F,0,T,C,1,\"123456\"}" },
		{ IPC_EDIT_SAVE,				"" },
		{ IPC_SET_LABEL_ID_SELECTED,	"LABELID=\"Barcode\";" },
		{ IPC_GET_LABEL_ID_SELECTED,	"" },
		{ IPC_GET_SELECTED_COUNT,		"" },
		{ IPC_SET_SELECTED_COUNT,		"999" },
		{ IPC_GET_SELECTED_COUNT,		"" },
	};
	
	for (int i = 0; i < ARRAYSIZE (map); i++) 
		Add (map [i].m_dw, map [i].m_lpsz);
}

void CIpDemoDlg::OnClear() 
{
	CSingleLock lock (&m_cs);

	lock.Lock ();

	if (lock.IsLocked ()) {
		CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_CMDS);

		ASSERT (pLV);

		for (int i = 0; i < pLV->GetItemCount (); i++) 
			if (IPCSTRUCT * p = (IPCSTRUCT *)pLV->GetItemData (i))
				delete p;

		pLV->DeleteAllItems ();
	}
}

bool CIpDemoDlg::CheckTx (CIpDemoDlg & dlg, CSocket & socket, ULONG & lBatchID)
{
	CListCtrl * pLV = (CListCtrl *)dlg.GetDlgItem (LV_CMDS);

	ASSERT (pLV);

	for (int nItem = 0; nItem < pLV->GetItemCount (); nItem++) {
		if (IPCSTRUCT * pCmd = (IPCSTRUCT *)pLV->GetItemData (nItem)) {
			if (pCmd->send.Header.BatchID == 0) {
				CSingleLock lock (&dlg.m_cs);

				lock.Lock ();

				if (lock.IsLocked ()) {
					_FJ_SOCKET_MESSAGE send;

					memset (&send, 0, sizeof (send));

					pCmd->send.Header.BatchID = ++lBatchID;

					send.Header.Cmd		= pCmd->send.Header.Cmd;
					send.Header.BatchID	= lBatchID;
					send.Header.Length	= sizeof (send.Data);

					memcpy (send.Data, pCmd->send.Data, sizeof (send.Data));

					TRACEF ("Send:    " + ToString (send));

					{
						CString str;

						str.Format ("%d %d %d", 
							send.Header.Cmd,
							send.Header.BatchID,
							send.Header.Length);
						TRACEF (str);
						str.Format ("%p %p %p", 
							send.Header.Cmd,
							send.Header.BatchID,
							send.Header.Length);
						TRACEF (str);
					}

					send.Header.Cmd		= htonl (send.Header.Cmd);
					send.Header.BatchID	= htonl (send.Header.BatchID);
					send.Header.Length	= htonl (send.Header.Length);

					{
						CString str;

						str.Format ("%d %d %d", 
							send.Header.Cmd,
							send.Header.BatchID,
							send.Header.Length);
						TRACEF (str);
						str.Format ("%p %p %p", 
							send.Header.Cmd,
							send.Header.BatchID,
							send.Header.Length);
						TRACEF (str);
					}
					{
						CString strFile = GetIpcCmdName (ntohl (send.Header.Cmd));
						
						if (!strFile.GetLength ()) 
							strFile = "cmd";

						strFile += ".bin";

						if (FILE * fp = fopen (strFile, "wb")) {
							fwrite (&send, sizeof (send), 1, fp);
							fclose (fp);
						}
					}

					//int nSend = socket.SendTo (&send, sizeof (send), _FJ_SOCKET_NUMBER, strAddress);
					int nSend = socket.Send (&send, sizeof (send));

					if (nSend == SOCKET_ERROR) {
						DWORD dw = GetLastError ();

						switch (dw) {
						case WSAECONNRESET:
						case WSAESHUTDOWN :
							TRACEF ("Lost connection");
							return false;
						}
					}
				}
			}
		}
	}

	return true;
}

/*
{ULONG lBatchID = 0; CSocket socket;
////////////////////////////////////////////////////////////
	_FJ_SOCKET_MESSAGE send;
	char szCmd [SOCKET_BUFFER_SIZE];

	memset (&send, 0, sizeof (send));

	send.Header.Cmd		= IPC_SELECT_LABEL; // note: this an enum, not a string
	send.Header.BatchID	= lBatchID; // we normally use this as a serial number
	send.Header.Length	= sizeof (send.Data);

	sprintf (szCmd, "LABELID=\"%s\";", "TEST");
	memcpy (send.Data, szCmd, strlen (szCmd));

	// convert from host byte order to network byte order.
	send.Header.Cmd		= htonl (send.Header.Cmd);
	send.Header.BatchID	= htonl (send.Header.BatchID);
	send.Header.Length	= htonl (send.Header.Length);

	int nSend = socket.Send (&send, sizeof (send));
////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////
	_FJ_SOCKET_MESSAGE recv;

	memset (&recv, 0, sizeof (recv));

	int nRevc = socket.Receive (&recv, sizeof (recv));

	// convert network byte order to host byte order.
	recv.Header.Cmd		= ntohl (recv.Header.Cmd);
	recv.Header.BatchID	= ntohl (recv.Header.BatchID);
	recv.Header.Length	= ntohl (recv.Header.Length);
////////////////////////////////////////////////////////////
}
*/

void CIpDemoDlg::CheckRx (CIpDemoDlg & dlg, CSocket & s)
{
	CListCtrl * pLV = (CListCtrl *)dlg.GetDlgItem (LV_CMDS);

	ASSERT (pLV);

	timeval timeout = { 1, 0 };
	fd_set readfds;

	readfds.fd_count = 1;
	readfds.fd_array [0] = s.m_hSocket;

	::SetLastError (0);
	int nSelect = ::select (0, &readfds, NULL, NULL, &timeout);

	if (nSelect > 0) {
		_FJ_SOCKET_MESSAGE recv;

		memset (&recv, 0, sizeof (recv));

		int nRevc = s.Receive (&recv, sizeof (recv));

		if (nRevc != SOCKET_ERROR) {
			CSingleLock lock (&dlg.m_cs);

			lock.Lock ();
			
			bool bMatch = false;

			recv.Header.Cmd		= ntohl (recv.Header.Cmd);
			recv.Header.BatchID	= ntohl (recv.Header.BatchID);
			recv.Header.Length	= ntohl (recv.Header.Length);

			if (lock.IsLocked ()) {
				for (int nItem = 0; nItem < pLV->GetItemCount (); nItem++) {
					if (IPCSTRUCT * p = (IPCSTRUCT *)pLV->GetItemData (nItem)) {
						if (p->send.Header.BatchID == recv.Header.BatchID) {
							TRACEF ("Receive: " + ToString (recv));
							memcpy (&p->recv, &recv, sizeof (p->recv));
							pLV->SetItemText (nItem, 2, (LPCTSTR)recv.Data);
							bMatch = true;
							break;
						}
					}
				}

				if (!bMatch) {
					IPCSTRUCT * p = new IPCSTRUCT;

					TRACEF ("Receive: " + ToString (recv));
					memset (p, 0, sizeof (IPCSTRUCT));
					p->send.Header.BatchID = -1;
					p->send.Header.Cmd = recv.Header.Cmd;
					memcpy (&p->recv, &recv, sizeof (recv));
					dlg.Add (p);
				}
			}
		}
	}
}

ULONG CALLBACK CIpDemoDlg::ThreadFunc (LPVOID lpData)
{
	ASSERT (lpData);
	
	CIpDemoDlg & dlg = * (CIpDemoDlg *)lpData;
	bool bConnected = false;
	CString strAddress, strConnect;
	ULONG lBatchID = 0;

	if (CButton * p = (CButton *)dlg.GetDlgItem (BTN_CONNECT)) {
		p->GetWindowText (strConnect);
		p->SetWindowText ("Connecting...");
		p->EnableWindow (FALSE);
	}

	if (CButton * p = (CButton *)dlg.GetDlgItem (IDC_ADDRESS))
		p->EnableWindow (FALSE);

	if (CIPAddressCtrl * p = (CIPAddressCtrl *)dlg.GetDlgItem (IDC_ADDRESS)) {
		BYTE n [4] = { 0 };

		if (p->GetAddress (n [0], n [1], n [2], n [3])) 
			strAddress.Format ("%d.%d.%d.%d", n [0], n [1], n [2], n [3]);
	}

	TRACEF ("Attempting connection: " + strAddress);

	if (!::AfxSocketInit ()) {
		AfxMessageBox (IDP_SOCKETS_INIT_FAILED);
		return 0;
	}

	CSocket s;

	if (!s.Create (0, SOCK_STREAM)) {
		TRACEF ("Create failed");
		return 0;
	}


	dlg.BeginWaitCursor ();
	bool bConnect = s.Connect (strAddress, _FJ_SOCKET_NUMBER) ? true : false;
	dlg.EndWaitCursor ();


	if (!bConnect) {
		dlg.MessageBox ("Failed to connect: " + strAddress);
	}
	else {
		TRACEF ("Established connection on: " + strAddress);
		bConnected = true;

		if (CIPAddressCtrl * p = (CIPAddressCtrl *)dlg.GetDlgItem (IDC_ADDRESS)) {
			BYTE n [4] = { 0 };

			if (p->GetAddress (n [0], n [1], n [2], n [3])) {
				theApp.WriteProfileInt ("IP Address", "1", n [0]);
				theApp.WriteProfileInt ("IP Address", "2", n [1]);
				theApp.WriteProfileInt ("IP Address", "3", n [2]);
				theApp.WriteProfileInt ("IP Address", "4", n [3]);
			}
		}

		if (CButton * pConnect = (CButton *)dlg.GetDlgItem (BTN_CONNECT)) 
			pConnect->SetWindowText ("&Disconnect");
	}

	if (CButton * p = (CButton *)dlg.GetDlgItem (BTN_CONNECT))
		p->EnableWindow (TRUE);

	while (bConnected) {
		if (::WaitForSingleObject (dlg.m_hExitEvent, 250) == WAIT_OBJECT_0) {
			::ResetEvent (dlg.m_hExitEvent);
			bConnected = false;
			break;
		}

		if (CheckTx (dlg, s, lBatchID)) 
			CheckRx (dlg, s);
		else
			bConnected = false;
	}

	TRACEF ("Closing connection: " + strAddress + " ...");
	s.Close ();

	::CloseHandle (dlg.m_hExitEvent);
	dlg.m_hExitEvent = NULL;
	dlg.m_hThread = NULL;

	if (CButton * pConnect = (CButton *)dlg.GetDlgItem (BTN_CONNECT)) 
		pConnect->SetWindowText (strConnect);

	if (CButton * p = (CButton *)dlg.GetDlgItem (IDC_ADDRESS))
		p->EnableWindow (TRUE);

	TRACEF ("Closed");

	return 0;
}

void CIpDemoDlg::OnSelchangeScript() 
{
	CComboBox * pScript = (CComboBox *)GetDlgItem (CB_SCRIPT);

	ASSERT (pScript);

	ULONG lScriptID = pScript->GetItemData (pScript->GetCurSel ());

	OnClear ();

	try {
		CDaoRecordset rst (&m_db);
		CString str;

		str.Format ("SELECT * FROM ScriptCmds WHERE ScriptID=%d;", lScriptID);
		rst.Open (dbOpenSnapshot, str, dbReadOnly);

		while (!rst.IsEOF ()) {
			ULONG lCmdID = rst.GetFieldValue ("CmdID").lVal;
			CString strData = (LPCTSTR)rst.GetFieldValue ("Data").bstrVal;

			Add (lCmdID, strData);

			rst.MoveNext ();
		}
		
		rst.Close ();
	}
	catch (CDaoException * e)		{ e->ReportError (); e->Delete (); ASSERT (0); }
	catch (CMemoryException * e)	{ e->ReportError (); e->Delete (); ASSERT (0); }

}
