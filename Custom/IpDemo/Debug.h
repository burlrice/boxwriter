#ifndef __IPCDEBUG_H__
#define __IPCDEBUG_H__

CString GetIpcCmdName (DWORD dw);
DWORD GetIpcCmd (const CString & str);

#endif //__IPCDEBUG_H__
