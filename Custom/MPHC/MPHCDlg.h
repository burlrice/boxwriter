// MPHCDlg.h : header file
//

#if !defined(AFX_MPHCDLG_H__D6A670DC_DA89_412E_9746_F47082ACD229__INCLUDED_)
#define AFX_MPHCDLG_H__D6A670DC_DA89_412E_9746_F47082ACD229__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DbTypeDefs.h"

/////////////////////////////////////////////////////////////////////////////
// CMPHCDlg dialog

class CMPHCDlg : public CDialog
{
// Construction
public:
	CMPHCDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CMPHCDlg ();

public:
	void Free();
	ULONG GetSelectedAddr();
	HANDLE GetSelectedHandle();
	virtual void OnCancel();
	virtual void OnOK();

// Dialog Data
	//{{AFX_DATA(CMPHCDlg)
	enum { IDD = IDD_MPHC_DIALOG };
	CString	m_strFirmware;
	CString	m_strTestPattern;
	DWORD	m_dwSpeed;
	DWORD	m_dwDelay;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMPHCDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	bool m_bPrint;
	int m_nHeadType;

	// Generated message map functions
	//{{AFX_MSG(CMPHCDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBrowsefirmware();
	afx_msg void OnBrowsetestpattern();
	afx_msg void OnDownloadfirmware();
	afx_msg void OnSelchangeCard();
	afx_msg void OnEnableprint();
	afx_msg void OnClose();
	afx_msg void OnDownloadimage();
	afx_msg void OnClearprintbuffer();
	afx_msg void OnSetprintdelay();
	afx_msg void OnSetspeed();
	afx_msg void OnSelchangeHead();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MPHCDLG_H__D6A670DC_DA89_412E_9746_F47082ACD229__INCLUDED_)
