// MPHC.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "MPHC.h"
#include "MPHCDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMPHCApp

BEGIN_MESSAGE_MAP(CMPHCApp, CWinApp)
	//{{AFX_MSG_MAP(CMPHCApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMPHCApp construction

CMPHCApp::CMPHCApp()
:	m_bDemo (false),
	m_pMphcDriver (NULL)
{
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CMPHCApp object

CMPHCApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CMPHCApp initialization

BOOL CMPHCApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif


	{
		const CString strDemo = _T ("/DEMO");
		CString strCmdLine = m_lpCmdLine;

		strCmdLine.MakeUpper ();

		m_bDemo = (strCmdLine.Find (strDemo) != -1) ;
	}

	try
	{
		m_pMphcDriver = new CMphc (m_bDemo);
	}
	catch (CMemoryException * e) { HANDLEEXCEPTION (e); }

	CMPHCDlg dlg;
	m_pMainWnd = &dlg;

	SetRegistryKey ("Foxjet\\MPHC demo");

	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
	}
	else if (nResponse == IDCANCEL)
	{
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int CMPHCApp::ExitInstance() 
{
	if (m_pMphcDriver) {
		delete m_pMphcDriver;
		m_pMphcDriver = NULL;
	}

	return CWinApp::ExitInstance();
}
