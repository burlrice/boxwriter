// MPHCDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MPHC.h"
#include "MPHCDlg.h"
#include "Resource.h"
#include "ximage.h"
#include "ValueDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;

struct
{
	int			m_nType;
	LPCTSTR		m_lpsz;
}
static const heads [] =
{
	{ ALPHA_CODER,						"ALPHA_CODER",			},
	{ UJ2_352_32,						"UJ2_352_32",			},
	{ UJ2_192_32,						"UJ2_192_32",			},
	{ UJ2_96_32,						"UJ2_96_32",			},
	{ GRAPHICS_768_256,					"GRAPHICS_768_256",		},
	{ GRAPHICS_384_128,					"GRAPHICS_384_128",		},
	{ UJ2_192_32NP,						"UJ2_192_32NP",			},
	{ UJ2_192_32NP + 1 /*IV_72*/,		"IV_72",				},
};

/////////////////////////////////////////////////////////////////////////////
// CMPHCDlg dialog

CMPHCDlg::CMPHCDlg(CWnd* pParent /*=NULL*/)
:	m_bPrint (false),
	m_dwDelay (1000),
	m_dwSpeed (300),
	CDialog(CMPHCDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMPHCDlg)
	m_strFirmware = _T("");
	m_strTestPattern = _T("");
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CMPHCDlg::~CMPHCDlg ()
{
}

void CMPHCDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMPHCDlg)
	DDX_Text(pDX, TXT_FIRMWARE, m_strFirmware);
	DDX_Text(pDX, TXT_TESTPATTERN, m_strTestPattern);
	DDX_Text(pDX, TXT_SPEED, m_dwSpeed);
	DDX_Text(pDX, TXT_PRINTDELAY, m_dwDelay);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CMPHCDlg, CDialog)
	//{{AFX_MSG_MAP(CMPHCDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_BROWSEFIRMWARE, OnBrowsefirmware)
	ON_BN_CLICKED(BTN_BROWSETESTPATTERN, OnBrowsetestpattern)
	ON_BN_CLICKED(BTN_DOWNLOADFIRMWARE, OnDownloadfirmware)
	ON_CBN_SELCHANGE(CB_CARD, OnSelchangeCard)
	ON_BN_CLICKED(CHK_ENABLEPRINT, OnEnableprint)
	ON_WM_CLOSE()
	ON_BN_CLICKED(BTN_DOWNLOADIMAGE, OnDownloadimage)
	ON_BN_CLICKED(BTN_CLEARPRINTBUFFER, OnClearprintbuffer)
	ON_BN_CLICKED(BTN_SETPRINTDELAY, OnSetprintdelay)
	ON_BN_CLICKED(BTN_SETSPEED, OnSetspeed)
	ON_CBN_SELCHANGE(CB_HEAD, OnSelchangeHead)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMPHCDlg message handlers

BOOL CMPHCDlg::OnInitDialog()
{
	CComboBox * pCard = (CComboBox *)GetDlgItem (CB_CARD);
	CComboBox * pHead = (CComboBox *)GetDlgItem (CB_HEAD);

	ASSERT (pCard);
	ASSERT (pHead);

	ULONG lSelectedAddr = theApp.GetProfileInt ("Settings", "Card", 0x300);
	m_strFirmware = theApp.GetProfileString ("Settings", "Firmware", "Mphcv9r1.bit");
	m_strTestPattern = theApp.GetProfileString ("Settings", "Test pattern", "Test.bmp");
	m_nHeadType = theApp.GetProfileInt ("Settings", "Head type", UJ2_352_32);

	UINT nCards = theApp.m_pMphcDriver->GetAddressCount();
	PULONG pAddr = new ULONG [nCards];
	
	theApp.m_pMphcDriver->GetAddressList (pAddr, nCards);

	for (UINT i = 0; i < nCards; i++) {
		ULONG lAddress = pAddr [i];
		CString str;

		str.Format ("%X", lAddress);
		
		int nIndex = pCard->AddString (str);

		HANDLE hDevice = theApp.m_pMphcDriver->ClaimAddress (lAddress);
		pCard->SetItemData (nIndex, (DWORD)hDevice);

		if (lAddress == lSelectedAddr) 
			pCard->SetCurSel (nIndex);
	}

	delete [] pAddr;

	if (pCard->GetCurSel () == CB_ERR)
		pCard->SetCurSel (0);

	for (i = 0; i < ARRAYSIZE (::heads); i++) {
		int nIndex = pHead->AddString (::heads [i].m_lpsz);

		pHead->SetItemData (nIndex, ::heads [i].m_nType);

		if (m_nHeadType == ::heads [i].m_nType)
			pHead->SetCurSel (nIndex);
	}

	if (pCard->GetCurSel () == CB_ERR)
		pCard->SetCurSel (0);

	if (pHead->GetCurSel () == CB_ERR)
		pHead->SetCurSel (0);

	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	OnSelchangeCard ();
	OnSelchangeHead ();

	VERIFY (theApp.m_pMphcDriver->SetPrintDelay (GetSelectedHandle (), (WORD)m_dwDelay));
	VERIFY (theApp.m_pMphcDriver->SetSpeedGateWidth (GetSelectedHandle (), (WORD)m_dwSpeed));

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMPHCDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CMPHCDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CMPHCDlg::OnOK()
{

}

void CMPHCDlg::OnCancel()
{
	CDialog::OnOK ();
}

void CMPHCDlg::OnBrowsefirmware() 
{
	CString str;

	GetDlgItemText (TXT_FIRMWARE, str);

	CFileDialog dlg (TRUE, _T ("*.*"), "Mphcv9r1.bit", 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_NOCHANGEDIR, 
		_T ("Firmware files (*.bit)|*.bit; *.exe|All files (*.*)|*.*||"), this);

	dlg.m_ofn.lpstrInitialDir = str.GetBuffer (str.GetLength ());

	if (dlg.DoModal () == IDOK) {
		m_strFirmware = str = dlg.GetPathName ();
		theApp.WriteProfileString ("Settings", "Firmware", str);
		SetDlgItemText (TXT_FIRMWARE, str);
	}
}

void CMPHCDlg::OnBrowsetestpattern() 
{
	CString str;

	GetDlgItemText (TXT_TESTPATTERN, str);

	CFileDialog dlg (TRUE, _T ("*.*"), "Test.bmp", 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_NOCHANGEDIR, 
		_T ("Bitmap files (*.bmp)|*.bmp; *.exe|All files (*.*)|*.*||"), this);

	dlg.m_ofn.lpstrInitialDir = str.GetBuffer (str.GetLength ());

	if (dlg.DoModal () == IDOK) {
		m_strTestPattern = str = dlg.GetPathName ();
		theApp.WriteProfileString ("Settings", "Test pattern", str);
		SetDlgItemText (TXT_TESTPATTERN, str);
	}
}

void CMPHCDlg::OnDownloadfirmware() 
{	
	CString strFPGA;
	HANDLE hDevice = GetSelectedHandle ();

	BeginWaitCursor ();

	GetDlgItemText (TXT_FIRMWARE, strFPGA);
	int nResult = theApp.m_pMphcDriver->ProgramFPGA (hDevice, strFPGA);	

	if (nResult) {
		CString str;

		str.Format ("ProgramFPGA failed [0x%p]", nResult);
		MessageBox (str);
	}

	EndWaitCursor ();
}

ULONG CMPHCDlg::GetSelectedAddr()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_CARD);
	CString str = "300";

	ASSERT (pCB);

	int nIndex = pCB->GetCurSel ();

	if (nIndex != CB_ERR) 
		pCB->GetLBText (nIndex, str);

	return _tcstoul (str, NULL, 16);
}

HANDLE CMPHCDlg::GetSelectedHandle()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_CARD);
	HANDLE hResult = NULL;

	ASSERT (pCB);

	int nIndex = pCB->GetCurSel ();

	if (nIndex != CB_ERR) 
		hResult = (HANDLE)pCB->GetItemData (nIndex);

	return hResult;
}

void CMPHCDlg::OnSelchangeCard() 
{
	CString str;
	HANDLE hDevice = GetSelectedHandle ();

	str.Format ("0x%p", hDevice);
	SetDlgItemText (TXT_HANDLE, str);
}

void CMPHCDlg::Free()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_CARD);

	for (int i = 0; i < pCB->GetCount (); i++) 
		if (HANDLE h = (HANDLE)pCB->GetItemData (i))
			VERIFY (theApp.m_pMphcDriver->ReleaseAddress (h));

	pCB->ResetContent ();
}

void CMPHCDlg::OnEnableprint() 
{
	CButton * p = (CButton *)GetDlgItem (CHK_ENABLEPRINT);
	HANDLE hDevice = GetSelectedHandle ();
	ULONG lAddr = GetSelectedAddr ();

	ASSERT (p);

	m_bPrint = !m_bPrint;
	p->SetCheck (m_bPrint ? 1 : 0);
	
	if (!theApp.m_pMphcDriver->EnablePrint (hDevice, m_bPrint))
		MessageBox ("EnablePrint failed");
}

void CMPHCDlg::OnClose() 
{
	Free ();
	CDialog::OnClose();
}

void CMPHCDlg::OnClearprintbuffer() 
{
	HANDLE hDevice = GetSelectedHandle ();

	if (!theApp.m_pMphcDriver->ClearPrintBuffer (hDevice))
		MessageBox ("ClearPrintBuffer failed");
}

void CMPHCDlg::OnDownloadimage() 
{
	CxImage img;
	HBITMAP hBmp = NULL;

	if (img.Load (m_strTestPattern, CXIMAGE_FORMAT_BMP)) {
		if (img.IsValid ()) {
			CDC dc;
			
			dc.CreateCompatibleDC (NULL); // force monochrome
			hBmp = img.MakeBitmap (dc);
		}
	}

	if (!hBmp) {
		MessageBox ("Failed to load: " + m_strTestPattern);
		return;
	}

	DIBSECTION ds;

	ZeroMemory (&ds, sizeof (ds));
	::GetObject (hBmp, sizeof (ds), &ds);

	int nImageLen = ds.dsBm.bmWidthBytes * ds.dsBm.bmHeight;
	
	if (nImageLen > IMAGE_BUFFER_SIZE) {
		MessageBox ("Image is too long for MPHC");
		return;
	}

	HANDLE hDevice = GetSelectedHandle ();
	UCHAR * pImageBuffer = new UCHAR [IMAGE_BUFFER_SIZE];

	ZeroMemory (pImageBuffer, IMAGE_BUFFER_SIZE);
	::GetBitmapBits (hBmp, nImageLen, pImageBuffer);

	int nBPC = ds.dsBm.bmWidthBytes;
	WORD wStartCol = 0;
	WORD wEndCol = (WORD)ds.dsBm.bmHeight;

	int nUpdateImageData = theApp.m_pMphcDriver->UpdateImageData (hDevice, pImageBuffer, nBPC, wStartCol, wEndCol);

	delete [] pImageBuffer;

	if (!nUpdateImageData)
		MessageBox ("UpdateImageData failed");
}


void CMPHCDlg::OnSetprintdelay() 
{
	CValueDlg dlg (this);

	dlg.m_dwValue = m_dwDelay;

	if (dlg.DoModal () == IDOK) {
		m_dwDelay = dlg.m_dwValue;
		UpdateData (FALSE);
		
		if (!theApp.m_pMphcDriver->SetPrintDelay (GetSelectedHandle (), (WORD)m_dwDelay))
			MessageBox ("SetPrintDelay failed");
	}
}

void CMPHCDlg::OnSetspeed() 
{
	CValueDlg dlg (this);

	dlg.m_dwValue = m_dwSpeed;

	if (dlg.DoModal () == IDOK) {
		m_dwSpeed = dlg.m_dwValue;
		UpdateData (FALSE);
		
		if (!theApp.m_pMphcDriver->SetSpeedGateWidth (GetSelectedHandle (), (WORD)m_dwSpeed))
			MessageBox ("SetSpeedGateWidth failed");
	}
}

void CMPHCDlg::OnSelchangeHead() 
{
	CComboBox * pHead = (CComboBox *)GetDlgItem (CB_HEAD);

	ASSERT (pHead);

	m_nHeadType = pHead->GetItemData (pHead->GetCurSel ());
	theApp.WriteProfileInt ("Settings", "Head type", m_nHeadType);
}
