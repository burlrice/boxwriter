#if !defined(AFX_VALUEDLG_H__22973506_F0D5_441F_B0BD_B5DA21468E76__INCLUDED_)
#define AFX_VALUEDLG_H__22973506_F0D5_441F_B0BD_B5DA21468E76__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ValueDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CValueDlg dialog

class CValueDlg : public CDialog
{
// Construction
public:
	CValueDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CValueDlg)
	enum { IDD = IDD_VALUE };
	DWORD	m_dwValue;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CValueDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CValueDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VALUEDLG_H__22973506_F0D5_441F_B0BD_B5DA21468E76__INCLUDED_)
