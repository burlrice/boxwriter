// ValueDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MPHC.h"
#include "ValueDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CValueDlg dialog


CValueDlg::CValueDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CValueDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CValueDlg)
	m_dwValue = 0;
	//}}AFX_DATA_INIT
}


void CValueDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CValueDlg)
	DDX_Text(pDX, TXT_VALUE, m_dwValue);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CValueDlg, CDialog)
	//{{AFX_MSG_MAP(CValueDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CValueDlg message handlers
