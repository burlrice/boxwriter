; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CMPHCDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "MPHC.h"

ClassCount=3
Class1=CMPHCApp
Class2=CMPHCDlg

ResourceCount=3
Resource2=IDD_MPHC_DIALOG
Resource1=IDR_MAINFRAME
Class3=CValueDlg
Resource3=IDD_VALUE

[CLS:CMPHCApp]
Type=0
HeaderFile=MPHC.h
ImplementationFile=MPHC.cpp
Filter=N
BaseClass=CWinApp
VirtualFilter=AC

[CLS:CMPHCDlg]
Type=0
HeaderFile=MPHCDlg.h
ImplementationFile=MPHCDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=BTN_BROWSEFIRMWARE



[DLG:IDD_MPHC_DIALOG]
Type=1
Class=CMPHCDlg
ControlCount=22
Control1=CB_CARD,combobox,1344340227
Control2=TXT_HANDLE,edit,1350633600
Control3=CB_HEAD,combobox,1344339971
Control4=TXT_FIRMWARE,edit,1350633600
Control5=BTN_DOWNLOADFIRMWARE,button,1342242816
Control6=BTN_BROWSEFIRMWARE,button,1342242816
Control7=TXT_TESTPATTERN,edit,1350633600
Control8=BTN_DOWNLOADIMAGE,button,1342242816
Control9=BTN_CLEARPRINTBUFFER,button,1342242816
Control10=BTN_BROWSETESTPATTERN,button,1342242816
Control11=CHK_ENABLEPRINT,button,1342242819
Control12=TXT_PRINTDELAY,edit,1350633600
Control13=BTN_SETPRINTDELAY,button,1342242816
Control14=TXT_SPEED,edit,1350633600
Control15=BTN_SETSPEED,button,1342242816
Control16=IDC_STATIC,button,1342177287
Control17=IDC_STATIC,static,1342308352
Control18=IDC_STATIC,static,1342308352
Control19=IDC_STATIC,button,1342177287
Control20=IDC_STATIC,static,1342308352
Control21=IDC_STATIC,static,1342308352
Control22=IDC_STATIC,static,1342308352

[DLG:IDD_VALUE]
Type=1
Class=CValueDlg
ControlCount=4
Control1=TXT_VALUE,edit,1350631552
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816
Control4=IDC_STATIC,static,1342308352

[CLS:CValueDlg]
Type=0
HeaderFile=ValueDlg.h
ImplementationFile=ValueDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CValueDlg

