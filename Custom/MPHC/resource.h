//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by MPHC.rc
//
#define IDD_MPHC_DIALOG                 102
#define IDR_MAINFRAME                   128
#define IDD_VALUE                       129
#define TXT_FIRMWARE                    1000
#define BTN_BROWSEFIRMWARE              1001
#define TXT_TESTPATTERN                 1002
#define BTN_BROWSETESTPATTERN           1003
#define BTN_DOWNLOADFIRMWARE            1004
#define CB_CARD                         1005
#define TXT_HANDLE                      1006
#define BTN_DOWNLOADIMAGE               1007
#define CHK_ENABLEPRINT                 1008
#define BTN_CLEARPRINTBUFFER            1009
#define TXT_VALUE                       1009
#define TXT_PRINTDELAY                  1010
#define BTN_SETPRINTDELAY               1011
#define TXT_SPEED                       1012
#define BTN_SETSPEED                    1013
#define CB_HEAD                         1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
