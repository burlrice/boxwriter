// MPHC.h : main header file for the MPHC application
//

#if !defined(AFX_MPHC_H__093982C4_AE10_4D9F_95A3_ABB1DC921D59__INCLUDED_)
#define AFX_MPHC_H__093982C4_AE10_4D9F_95A3_ABB1DC921D59__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "..\..\MphcApi\Mphc.h"

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CMPHCApp:
// See MPHC.cpp for the implementation of this class
//

class CMPHCApp : public CWinApp
{
public:
	CMPHCApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMPHCApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	CMphc * m_pMphcDriver;

protected:
	bool m_bDemo;

// Implementation

	//{{AFX_MSG(CMPHCApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MPHC_H__093982C4_AE10_4D9F_95A3_ABB1DC921D59__INCLUDED_)
