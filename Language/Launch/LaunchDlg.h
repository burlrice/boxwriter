// LaunchDlg.h : header file
//

#if !defined(AFX_LAUNCHDLG_H__E730D231_6B9D_4D64_92A1_A58136B6617D__INCLUDED_)
#define AFX_LAUNCHDLG_H__E730D231_6B9D_4D64_92A1_A58136B6617D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CLaunchDlg dialog

class CLaunchDlg : public CDialog
{
// Construction
public:
	CLaunchDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CLaunchDlg)
	enum { IDD = IDD_LAUNCH_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLaunchDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

public:

	CString m_strFileName;

// Implementation
protected:

	HICON m_hIcon;

	static ULONG CALLBACK ThreadFunc (LPVOID lpData);

	// Generated message map functions
	//{{AFX_MSG(CLaunchDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LAUNCHDLG_H__E730D231_6B9D_4D64_92A1_A58136B6617D__INCLUDED_)
