// Launch.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Launch.h"
#include "LaunchDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLaunchApp

BEGIN_MESSAGE_MAP(CLaunchApp, CWinApp)
	//{{AFX_MSG_MAP(CLaunchApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLaunchApp construction

CLaunchApp::CLaunchApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CLaunchApp object

CLaunchApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CLaunchApp initialization

BOOL CLaunchApp::InitInstance()
{
	CCommandLineInfo cmdInfo;
	
	ParseCommandLine (cmdInfo);

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	if (cmdInfo.m_strFileName.GetLength ()) {
		CLaunchDlg dlg;
		CString str = ::GetCommandLine ();
		TCHAR sz [MAX_PATH] = { 0 };

		HMODULE hModule = ::GetModuleHandle (NULL);
		::GetModuleFileName (hModule, sz, ARRAYSIZE (sz) - 1);

		//"C:\Program Files\Foxjet\MarksmanELITE\MkDraw.exe" /lowres /dsn=MarksmanELITE /pass=foxjet /nosplash /IMAGE_ON_PHOTOCELL /DBTIMEOUT=60 /user="ADMIN" /PrinterID=1246576929

		str.Replace (_T ("\"") + CString (sz) + _T ("\""), _T (""));
		str.Replace (sz, _T (""));
		str.TrimLeft ();

		//::OutputDebugString (CString (sz) + _T ("\n"));
		//::OutputDebugString (cmdInfo.m_strFileName + _T ("\n"));
		//::OutputDebugString (str + _T ("\n"));

		dlg.m_strFileName = str; //cmdInfo.m_strFileName;

		m_pMainWnd = &dlg;
		dlg.DoModal();
	}


	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
