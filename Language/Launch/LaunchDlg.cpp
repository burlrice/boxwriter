// LaunchDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Launch.h"
#include "LaunchDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLaunchDlg dialog

CLaunchDlg::CLaunchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLaunchDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLaunchDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLaunchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLaunchDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CLaunchDlg, CDialog)
	//{{AFX_MSG_MAP(CLaunchDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLaunchDlg message handlers

BOOL CLaunchDlg::OnInitDialog()
{
	DWORD dw;

	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	SetDlgItemText (LBL_TEXT, _T ("Starting ") + m_strFileName + _T ("..."));
	
	HANDLE hThread = ::CreateThread ((LPSECURITY_ATTRIBUTES)NULL, 0,
		(LPTHREAD_START_ROUTINE)ThreadFunc, (LPVOID)this, 0, &dw);

	ASSERT (hThread);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLaunchDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLaunchDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

ULONG CALLBACK CLaunchDlg::ThreadFunc (LPVOID lpData) 
{
	ASSERT (lpData);

	CLaunchDlg & dlg = * (CLaunchDlg *)lpData;
	const DWORD dwSleep = 1000;
	OSVERSIONINFO osver = { sizeof(osver) };

	::GetVersionEx (&osver);

	if (dlg.m_strFileName.GetLength ()) {
		const CString strPath = dlg.m_strFileName;
		CString strCmdLine = ::GetCommandLine ();
		int nIndex = strCmdLine.Find (_T ("\"") + strPath + _T ("\""));

		::OutputDebugString (strCmdLine); ::OutputDebugString (_T ("\n"));

		if (nIndex != -1) 
			strCmdLine = strCmdLine.Mid (nIndex);
		else {
			int nIndex = strCmdLine.Find (strPath);
	
			if (nIndex != -1) 
				strCmdLine = strCmdLine.Mid (nIndex);
		}

		//if (osver.dwMajorVersion <= 6 && osver.dwMinorVersion == 0) { // Vista, or earlier
			STARTUPINFO si;     
			PROCESS_INFORMATION pi; 
			char szCmdLine [MAX_PATH];

			_tcsncpy (szCmdLine, strCmdLine, MAX_PATH);
			memset (&pi, 0, sizeof(pi));
			memset (&si, 0, sizeof(si));
			si.cb = sizeof(si);     
			si.dwFlags = STARTF_USESHOWWINDOW;     
			si.wShowWindow = SW_SHOW;     

			::Sleep (dwSleep);
			::OutputDebugString (strCmdLine); ::OutputDebugString (_T ("\n"));
			::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
			::Sleep (dwSleep);
		/*
		}
		else {
            SHELLEXECUTEINFO sei = { sizeof(sei) };

			if (strCmdLine.Find (_T ("/elevated")) == -1)
				strCmdLine += _T (" /elevated");

            sei.lpVerb = _T ("runas");
            sei.lpFile = szExe;
			sei.lpParameters = strCmdLine; // minus .exe name
            //sei.hwnd = hWnd;
            sei.nShow = SW_NORMAL;

			::Sleep (dwSleep);
			::OutputDebugString (strCmdLine); ::OutputDebugString (_T ("\n"));
            ::ShellExecuteEx (&sei);
			::Sleep (dwSleep);
		}
		*/
		
		dlg.PostMessage (WM_CLOSE);
	}

	return 0;
}

