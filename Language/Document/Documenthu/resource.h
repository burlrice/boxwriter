//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Document.rc
//
#define IDM_ELEMENT_TEMPLATE            1009
#define IDM_ELEMENTCONTEXT              1031
#define IDM_DEFCONTEXT                  1048
#define IDR_MESSAGE                     1060
#define IDR_FONTBARSIZE                 1064
#define ID_STATUS_ZOOM                  40000
#define ID_STATUS_DEFAULT               40001
#define ID_STATUS_PHOTO                 40002
#define ID_STATUS_LOCATION              40003
#define ID_UP                           40004
#define ID_DOWN                         40005
#define ID_LEFT                         40006
#define ID_RIGHT                        40007
#define ID_PAGEUP                       40008
#define ID_PAGEDOWN                     40009
#define ID_PAGERIGHT                    40010
#define ID_PAGELEFT                     40011
#define ID_PROPERTIES_MOVABLE           40012
#define ID_PROPERTIES_RESIZABLE         40013
#define ID_PROPERTIES_BACKWARDS         40014
#define ID_PROPERTIES_REVERSED          40015
#define ID_PROPERTIES_UPSIDEDOWN        40016
#define ID_DEFINE_PRODUCTAREA           40017
#define ID_VIEW_ZOOMIN                  40020
#define ID_VIEW_ZOOMOUT                 40021
#define ID_VIEW_ZOOMNORMAL100           40022
#define ID_FONT_BOLD                    40031
#define ID_FONT_ITALIC                  40032
#define ID_VIEW_ZOOM_CUSTOM             40033
#define ID_VIEW_ZOOM_MAX                40034
#define ID_VIEW_FONTBAR                 40036
#define ID_VIEW_EDITBAR                 40037
#define ID_ELEMENT_EDIT                 40044
#define ID_PROPERTIES_FLIPH             40045
#define ID_PROPERTIES_FLIPV             40046
#define ID_PROPERTIES_INVERSE           40047
#define ID_DEFINE_ELEMENTDEFAULTS       40049
#define ID_ELEMENTS_DELETE              40050
#define ID_HOME                         40051
#define ID_END                          40052
#define ID_BOTTOM                       40053
#define ID_TOP                          40054
#define ID_MOVEDOWN                     40055
#define ID_MOVEUP                       40056
#define ID_MOVERIGHT                    40057
#define ID_MOVELEFT                     40058
#define ID_DEFINE_EDITORDEFAULTS        40059
#define ID_EDIT_SELECTALL               40060
#define ID_DEBUGGINGSTUFF_READONLY      40064
#define ID_ELEMENTS_LIST                40065
#define ID_DEBUGGINGSTUFF_SAVETOTEXTFILE 40066
#define ID_DEBUGGINGSTUFF_GENERATEELEMENTS 40067
#define ID_DEBUGGINGSTUFF_MEMORYSTATUS  40068
#define ID_DEBUGGINGSTUFF_CLOSETRACEFILE 40069
#define ID_DEBUGGINGSTUFF_OPENTRACEFILE 40070
#define ID_DEBUGGINGSTUFF_RUNCSUBOBJISVALIDDATATEST 40071
#define ID_JUMPRIGHT                    40072
#define ID_JUMPLEFT                     40073
#define ID_JUMPUP                       40074
#define ID_JUMPDOWN                     40075
#define ID_DEBUGGINGSTUFF_SHOWSPLASHSCREEN 40076
#define ID_DEBUGGINGSTUFF_READFROMTEXTFILE 40078
#define ID_DEBUGGINGSTUFF_CONFINING     40081
#define ID_VIEW_RULERS_VERTICAL         40082
#define ID_VIEW_RULERS_HORIZONAL        40083
#define ID_FILE_PROPERTIES              40084
#define ID_DEBUGGINGSTUFF_SHOWDDECONVERSATIONS 40085
#define ID_DEBUGGINGSTUFF_ENUMERATEDATABASE 40086
#define ID_RETURN                       40089
#define ID_FILE_SAVEALL                 40091
#define ID_DEFINE_BOXES                 40092
#define ID_DEFINE_BOXUSAGE              40093
#define ID_DEBUGGINGSTUFF_SHOWCOPYDIALOG 40115
#define IDS_TASKSAVEFAILED              61251

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        6000
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         6000
#define _APS_NEXT_SYMED_VALUE           6000
#endif
#endif
