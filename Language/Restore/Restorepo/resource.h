//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Restore.rc
//
#define BTN_RESTORE                     3
#define IDD_RESTORE_DIALOG              102
#define IDB_BROWSE                      129
#define IDB_CANCEL                      130
#define IDB_INFO                        131
#define IDB_OK                          132
#define IDR_7Z_DLL                      133
#define IDR_7Z_EXE                      134
#define IDR_7ZIP_DLL                    135
#define IDS_LOCAL                       136
#define IDB_CHECKED                     137
#define IDS_NOTHINGSELECTED             137
#define IDB_UNCHECKED                   138
#define IDS_COPYFAILED                  138
#define IDB_RESTORE                     139
#define IDS_MANUAL_CONFIG_DSNS          139
#define IDI_MAINFRAME                   140
#define IDS_STRING140                   140
#define IDS_SHUTDOWN_APPS               140
#define CB_DRIVES                       1000
#define LV_FILES                        1001

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
