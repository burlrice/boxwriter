#if !defined(AFX_HTMLDLG_H__7EF5AFD8_BED3_4F46_B34E_CD154E44DC8F__INCLUDED_)
#define AFX_HTMLDLG_H__7EF5AFD8_BED3_4F46_B34E_CD154E44DC8F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HTMLDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CHTMLDlg dialog

class CHTMLDlg : public CDialog
{
// Construction
public:
	CHTMLDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CHTMLDlg)
	enum { IDD = IDD_HTML };
	CString	m_strEnglish;
	CString	m_strHTML;
	CString	m_strRaw;
	CString	m_strFormatted;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHTMLDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CHTMLDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HTMLDLG_H__7EF5AFD8_BED3_4F46_B34E_CD154E44DC8F__INCLUDED_)
