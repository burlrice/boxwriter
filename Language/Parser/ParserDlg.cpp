// ParserDlg.cpp : implementation file
//

#include "stdafx.h"

#include <afxdao.h>
#include <afxinet.h>

#include "Parser.h"
#include "ParserDlg.h"
#include "Debug.h"
#include "OdbcDatabase.h"
#include "OdbcRecordset.h"
#include "Database.h"
#include "CopyArray.h"
#include "Utils.h"
#include "TemplExt.h"
#include "Parse.h"
#include "FileExt.h"
#include "HTMLDlg.h"
#include "OdbcTable.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const int nTranslations = 5;
const LANGTYPE types [::nTranslations] = 
{
	LANGTYPE_SPANISH,
	LANGTYPE_PORTUGUESE,
	LANGTYPE_DUTCH,
	LANGTYPE_RUSSIAN,
	LANGTYPE_HUNGARIAN,
};

extern CParserApp theApp;

const TCHAR cDelim = '\"';
const TCHAR szSNAFU [2] = { (TCHAR)-123, 0 }; // don't know why, but access changes "..." to this sometimes

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;

////////////////////////////////////////////////////////////////////////////////

typedef struct 
{
	CString m_strEnglish;
	CString m_strSpanish;
	CString m_strPortuguese;
	CString m_strDutch;
	CString m_strRussian;
	CString m_strHungarian;
	ULONG m_lRecordNumber;
	CLongArray m_vDuplicateOf;
} DUPLICATESTRUCT;

static int FindNext (const CString & strFind, CArray <DUPLICATESTRUCT, DUPLICATESTRUCT &> & v, int nStart);
static void InsertAsc (CArray <DUPLICATESTRUCT, DUPLICATESTRUCT &> & v, DUPLICATESTRUCT & s);
static void InsertAsc (CLongArray & v, ULONG l);
static bool IsStdChar (TCHAR ch); 
static bool IsHexChar (TCHAR ch); 
static CString URLEncode (const CString & strEncode);
static CString URLDecode (const CString & strDecode);
static CString UnformatGoogle (const CString & strInput);
static CString FormatGoogle (const CString & strInput);
static CString MultiByteToCString (const CString & strInput);
static bool DirExists (const CString & str);
static CString GetFileAttrString (DWORD dw);

static CString GetLangString (LANGTYPE type)
{
	switch (type) {
	case LANGTYPE_ENGLISH:		return _T ("English");
	case LANGTYPE_SPANISH:		return _T ("Spanish");
	case LANGTYPE_PORTUGUESE:	return _T ("Portuguese");
	case LANGTYPE_DUTCH:		return _T ("Dutch");
	case LANGTYPE_RUSSIAN:		return _T ("Russian");
	case LANGTYPE_HUNGARIAN:	return _T ("Hungarian");
	}

	ASSERT (0);
	return _T ("");
}

static LANGTYPE GetLangType (const CString & str)
{
	if (!str.CompareNoCase (_T ("English"))) 			return LANGTYPE_ENGLISH;
	else if (!str.CompareNoCase (_T ("Spanish"))) 		return LANGTYPE_SPANISH;
	else if (!str.CompareNoCase (_T ("Portuguese"))) 	return LANGTYPE_PORTUGUESE;
	else if (!str.CompareNoCase (_T ("Dutch"))) 		return LANGTYPE_DUTCH;
	else if (!str.CompareNoCase (_T ("Russian"))) 		return LANGTYPE_RUSSIAN;
	else if (!str.CompareNoCase (_T ("Hungarian"))) 	return LANGTYPE_HUNGARIAN;

	ASSERT (0);
	return LANGTYPE_ENGLISH;
}

static bool IsUnicode (const CString & strFile)
{
	bool bUnicode = false;

	if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
		WORD w = 0;

		fread (&w, sizeof (w), 1, fp); 
		bUnicode = (w == 0xFEFF) ? true : false;
		fclose (fp);
	}

	return bUnicode;
}

static bool IsStretched (const CString &str1, const CString &str2)
{
	int nStretch = (int)((double)str2.GetLength () / (double)str1.GetLength () * 100.0);

	return (str1.GetLength () > 10 && nStretch > 125);		
}

bool RecordExists (COdbcDatabase & db, LANGTYPE lang, const CString & str, TRANSLATEDSTRUCT & t)
{
	COdbcRecordset rst (&db);
	CString strType = GetLangString (lang);

	if (!CParserDlg::HasQuotes (str)) {
		CString strSQL;

		strSQL.Format (_T ("SELECT * FROM [Translated] WHERE [%s]='%s';"), strType, str);
		rst.Open (strSQL);

		if (!rst.IsEOF ()) {
			rst >> t;
			return true;
		}
	}
	else {
		rst.Open (_T ("SELECT * FROM [Translated]"));

		while (!rst.IsEOF ()) {
			CString strTmp = rst.GetFieldValue (strType);

			if (!strTmp.CompareNoCase (str)) {
				rst >> t;
				return true;
			}

			rst.MoveNext ();
		}
	}

	return false;
}

bool RecordExists (COdbcDatabase & db, LANGTYPE lang, const CString & str, const CString & strTable = _T ("Translated"))
{
	COdbcRecordset rst (&db);
	CString strType = GetLangString (lang);

	if (!CParserDlg::HasQuotes (str)) {
		CString strSQL;

		strSQL.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s';"), strTable, strType, str);
		rst.Open (strSQL);

		if (!rst.IsEOF ()) 
			return true;
	}
	else {
		rst.Open (_T ("SELECT * FROM [") + strTable + _T ("]"));

		while (!rst.IsEOF ()) {
			CString strTmp = rst.GetFieldValue (strType);

			if (!strTmp.CompareNoCase (str)) 
				return true;

			rst.MoveNext ();
		}
	}

	return false;
}

void DeleteRecord (COdbcDatabase & db, LANGTYPE lang, const CString & str, const CString & strTable)
{
	CString strType = GetLangString (lang);

	if (!CParserDlg::HasQuotes (str)) {
		CString strSQL;

		strSQL.Format (_T ("DELETE * FROM [%s] WHERE [%s]='%s';"), strTable, strType, str);
		db.ExecuteSQL (strSQL);
		return;
	}
	else {
		COdbcRecordset rst (&db);

		rst.Open (_T ("SELECT * FROM [") + strTable + _T ("]"), CRecordset::dynaset);

		while (!rst.IsEOF ()) {
			CString strTmp = rst.GetFieldValue (strType);

			if (!strTmp.CompareNoCase (str)) {
				rst.Delete ();
				return;
			}

			rst.MoveNext ();
		}
	}

	TRACEF (_T ("Delete FAILED: ") + str);
}

bool ReplaceTranslatedRecord (COdbcDatabase & db, LANGTYPE lang, const CString & strOriginal, const CString & strNew)
{
	COdbcRecordset rstTranslated (&db);
	CString strType = GetLangString (lang);

	CString strSQL;

	rstTranslated.SetEditMode (COdbcRecordset::edit);
	strSQL.Format (_T ("UPDATE Translated SET [%s]=%s WHERE [%s]='%s';"), 
		strType, FoxjetDatabase::Format (strOriginal),
		strType, FoxjetDatabase::Format (strNew));
	TRACEF (strSQL);

	bool bResult = db.ExecuteSQL (strSQL);

	if (!bResult)
		TRACEF (_T ("NOT FOUND [") + strType + _T ("]: ") + strOriginal);

	return bResult;
	
	/* TODO: rem
	if (!CParserDlg::HasQuotes (strOriginal)) {
		CString strSQL;

		strSQL.Format (_T ("SELECT * FROM Translated WHERE [%s]='%s';"), strType, strOriginal);
		rstTranslated.Open (strSQL, CRecordset::dynaset);

		if (!rstTranslated.IsEOF ()) {
			rstTranslated.Edit ();
			rstTranslated.SetFieldValue (strType, strNew);
			rstTranslated.Update ();
			return true;
		}
	}
	else {
		rstTranslated.Open (_T ("SELECT * FROM Translated"), CRecordset::dynaset);

		while (!rstTranslated.IsEOF ()) {
			CString strTmp = rstTranslated.GetFieldValue (strType);

			if (!strTmp.CompareNoCase (strOriginal)) {
				rstTranslated.Edit ();
				rstTranslated.SetFieldValue (strType, strNew);
				rstTranslated.Update ();
				return true;
			}

			rstTranslated.MoveNext ();
		}
	}

	TRACEF (_T ("NOT FOUND [") + strType + _T ("]: ") + strOriginal);
	return false;
	*/
}

static bool DirExists (const CString & str)
{
	DWORD dw = ::GetFileAttributes (str);
	
	if (dw != -1)
		return dw & FILE_ATTRIBUTE_DIRECTORY ? true : false;

	return false;
}

static CString GetFileAttrString (DWORD dw)
{
	struct 
	{
		DWORD m_dw;
		LPCTSTR m_lpsz;
	} map [] = 
	{
		{ FILE_ATTRIBUTE_ARCHIVE,				_T ("FILE_ATTRIBUTE_ARCHIVE") },
		{ FILE_ATTRIBUTE_COMPRESSED,			_T ("FILE_ATTRIBUTE_COMPRESSED") },
		//{ FILE_ATTRIBUTE_DEVICE,				_T ("FILE_ATTRIBUTE_DEVICE") },
		{ FILE_ATTRIBUTE_DIRECTORY,				_T ("FILE_ATTRIBUTE_DIRECTORY") },
		{ FILE_ATTRIBUTE_ENCRYPTED,				_T ("FILE_ATTRIBUTE_ENCRYPTED") },
		{ FILE_ATTRIBUTE_HIDDEN,				_T ("FILE_ATTRIBUTE_HIDDEN") },
		{ FILE_ATTRIBUTE_NORMAL,				_T ("FILE_ATTRIBUTE_NORMAL") },
		{ FILE_ATTRIBUTE_NOT_CONTENT_INDEXED,	_T ("FILE_ATTRIBUTE_NOT_CONTENT_INDEXED") },
		{ FILE_ATTRIBUTE_OFFLINE,				_T ("FILE_ATTRIBUTE_OFFLINE") },
		{ FILE_ATTRIBUTE_READONLY,				_T ("FILE_ATTRIBUTE_READONLY") },
		{ FILE_ATTRIBUTE_REPARSE_POINT,			_T ("FILE_ATTRIBUTE_REPARSE_POINT") },
		{ FILE_ATTRIBUTE_SPARSE_FILE,			_T ("FILE_ATTRIBUTE_SPARSE_FILE") },
		{ FILE_ATTRIBUTE_SYSTEM,				_T ("FILE_ATTRIBUTE_SYSTEM") },
		{ FILE_ATTRIBUTE_TEMPORARY,				_T ("FILE_ATTRIBUTE_TEMPORARY") },
	};
	CString str;

	if (dw != -1)
		for (int i = 0; i < ARRAYSIZE (map); i++)
			if (dw & map [i].m_dw)
				str += map [i].m_lpsz + CString (_T (" "));

	return str;
}

static CString FormatGoogle (const CString & strInput)
{
	CString str (strInput);
	int nIndex = 0;

	while ((nIndex = str.Find (_T ("&"), nIndex)) != -1) {
		if ((nIndex + 1) < str.GetLength ()) {
			if (_istalpha (str [nIndex + 1])) 
				str.Delete (nIndex);
		}

		nIndex++;
	}

	return str;
}

static CString MultiByteToCString (const CString & strInput)
{
	int nLen = strInput.GetLength ();
	char szIn [4096] = { 0 };
	WCHAR szOut [4096] = { 0 };

	ASSERT (nLen < ARRAYSIZE (szOut));
	strncpy (szIn, w2a (strInput), nLen);
	int n = MultiByteToWideChar (CP_UTF8, 0, szIn, nLen, szOut, ARRAYSIZE (szOut));

	return szOut;
}

static CString UnformatGoogle (const CString & strInput)
{
	CString str = URLDecode (strInput);

	//str = MultiByteToCString (str);

	str.Replace (_T ("% d"), _T (" %d"));
	str.Replace (_T ("% s"), _T (" %s"));
	str.Replace (_T ("% i"), _T (" %i"));
	str.Replace (_T ("% x"), _T (" %x"));
	str.Replace (_T ("% c"), _T (" %c"));
	str.Replace (_T ("% l"), _T (" %l"));

	str.Replace (_T ("% D"), _T (" %d"));
	str.Replace (_T ("% S"), _T (" %s"));
	str.Replace (_T ("% I"), _T (" %i"));
	str.Replace (_T ("% X"), _T (" %x"));
	str.Replace (_T ("% C"), _T (" %c"));
	str.Replace (_T ("% L"), _T (" %l"));
	
	str.Replace (_T ("&amp; "), _T ("&"));
	str.Replace (_T ("&amp;"), _T ("&"));
	str.Replace (_T ("&quot;"), _T ("\""));
	str.Replace (_T (" \\ N \\ n"), _T ("\\n\\n"));
	str.Replace (_T (" \\ N"), _T ("\\n"));
	str.Replace (_T (" \\ n"), _T ("\\n"));

	str.Replace (_T (" \\ r"), _T ("\\r"));
	str.Replace (_T (" \\ R"), _T ("\\r"));

	str.Replace (_T (" \\ t"), _T ("\\t"));
	str.Replace (_T (" \\ T"), _T ("\\t"));

	str.Replace (_T (" \\ \\ "), _T ("\\\\"));
	str.Replace (_T (" \\ \\"), _T ("\\\\"));

	CString strTmp = FoxjetDatabase::Extract (str, _T ("&#"), _T (";"));

	while (strTmp.GetLength ()) {
		TCHAR c = _ttoi (strTmp);

		str.Replace (_T ("&# ") + strTmp + _T (";"), CString (c));
		str.Replace (_T ("&#") + strTmp + _T (";"), CString (c));
		strTmp = FoxjetDatabase::Extract (str, _T ("&#"), _T (";"));
	}

	str.TrimLeft ();
	str.TrimRight ();

	return str;
}

static bool IsHexChar (TCHAR ch)
{
	return  
		( _T( 'a' ) <= ch && _T( 'f' ) >= ch ) ||
		( _T( 'A' ) <= ch && _T( 'F' ) >= ch ) ||
		( _T( '0' ) <= ch && _T( '9' ) >= ch );
}

static bool IsStdChar (TCHAR ch) 
{   
	return  
		( _T( 'a' ) <= ch && _T( 'z' ) >= ch ) ||
		( _T( 'A' ) <= ch && _T( 'Z' ) >= ch ) ||
		( _T( '0' ) <= ch && _T( '9' ) >= ch ) ||
		_T( '_' ) == ch || _T( '-' ) == ch || 
		_T( '.' ) == ch;
}

static CString URLEncode (const CString & strEncode)
{
	CString str;

	for (int i = 0; i < strEncode.GetLength (); i++) {
		TCHAR c = strEncode [i];

		if (IsStdChar (c))
			str += c;
		else {
			CString strTmp;

			strTmp.Format (_T ("%%%02lX"), c);
			str += strTmp;
		}
	}

	return str;
}

static CString URLDecode (const CString & strDecode)
{
	CString str (strDecode);
	int nIndex = -1;

	while ((nIndex = str.Find ('%', nIndex)) != -1) {
		if ((nIndex + 2) < str.GetLength ()) {
			if (IsHexChar (str [nIndex + 1]) && IsHexChar (str [nIndex + 2])) {
				TCHAR c = (TCHAR)_tcstoul (str.Mid (nIndex + 1, 2), NULL, 16);
				str.Delete (nIndex, 3);
				str.Insert (nIndex, c);
			}
			else
				nIndex++;
		}
	}

	return str;
}


////////////////////////////////////////////////////////////////////////////////////////////////////

const FoxjetDatabase::COdbcRecordset & operator >> (const FoxjetDatabase::COdbcRecordset & rst, RCSTRUCT & s)
{
// 	DB_API CString FormatSQL (COdbcRecordset & rst, const CString & strTable, const CStringArray & name, const CStringArray & value, const CString & strID, ULONG lID);

	try {
		s.m_bEnabled		= (bool)rst.GetFieldValue	(_T ("Enabled"));
		s.m_strSource		= rst.GetFieldValue			(_T ("Source"));
		s.m_strEnglish		= rst.GetFieldValue			(_T ("English"));
		s.m_strSpanish		= rst.GetFieldValue			(_T ("Spanish"));
		s.m_strPortuguese	= rst.GetFieldValue			(_T ("Portuguese"));
		s.m_strDutch		= rst.GetFieldValue			(_T ("Dutch"));
		s.m_strRussian		= rst.GetFieldValue			(_T ("Russian"));
		s.m_strHungarian	= rst.GetFieldValue			(_T ("Hungarian"));
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

FoxjetDatabase::COdbcRecordset & operator << (FoxjetDatabase::COdbcRecordset & rst, const RCSTRUCT & s)
{
	CStringArray name, value;

	/*
	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}
	*/

	name.Add (_T ("Enabled"));			value.Add (ToString ((int)s.m_bEnabled));
	name.Add (_T ("Source"));			value.Add (s.m_strSource);
	name.Add (_T ("English"));			value.Add (s.m_strEnglish);
	name.Add (_T ("Spanish"));			value.Add (s.m_strSpanish);
	name.Add (_T ("Portuguese"));		value.Add (s.m_strPortuguese);
	name.Add (_T ("Dutch"));			value.Add (s.m_strDutch);
	name.Add (_T ("Russian"));			value.Add (s.m_strRussian);
	name.Add (_T ("Hungarian"));		value.Add (s.m_strHungarian);
   
	CString strTable = Trim (Extract (rst.GetSQL (), _T ("FROM "), _T (" ")));
	CString strSQL = FormatSQL (rst, strTable, name, value, _T (""), 0);
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

/*
	try {
		rst.SetFieldValue (_T ("Enabled"),		s.m_bEnabled);
		rst.SetFieldValue (_T ("Source"),		s.m_strSource);
		rst.SetFieldValue (_T ("English"),		s.m_strEnglish);
		rst.SetFieldValue (_T ("Spanish"),		s.m_strSpanish);
		rst.SetFieldValue (_T ("Portuguese"),	s.m_strPortuguese);
		rst.SetFieldValue (_T ("Dutch"),		s.m_strDutch);
		rst.SetFieldValue (_T ("Russian"),		s.m_strRussian);
		rst.SetFieldValue (_T ("Hungarian"),	s.m_strHungarian);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/
	return rst;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

const FoxjetDatabase::COdbcRecordset & operator >> (const FoxjetDatabase::COdbcRecordset & rst, TRANSLATEDSTRUCT & s)
{
	try {
		s.m_strEnglish		= rst.GetFieldValue			(_T ("English"));
		s.m_strSpanish		= rst.GetFieldValue			(_T ("Spanish"));
		s.m_strPortuguese	= rst.GetFieldValue			(_T ("Portuguese"));
		s.m_strDutch		= rst.GetFieldValue			(_T ("Dutch"));
		s.m_strRussian		= rst.GetFieldValue			(_T ("Russian"));
		s.m_strHungarian	= rst.GetFieldValue			(_T ("Hungarian"));
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

FoxjetDatabase::COdbcRecordset & operator << (FoxjetDatabase::COdbcRecordset & rst, const TRANSLATEDSTRUCT & s)
{
	CStringArray name, value;

	/*
	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}
	*/

	name.Add (_T ("English"));			value.Add (s.m_strEnglish);
	name.Add (_T ("Spanish"));			value.Add (s.m_strSpanish);
	name.Add (_T ("Portuguese"));		value.Add (s.m_strPortuguese);
	name.Add (_T ("Dutch"));			value.Add (s.m_strDutch);
	name.Add (_T ("Russian"));			value.Add (s.m_strRussian);
	name.Add (_T ("Hungarian"));		value.Add (s.m_strHungarian);
   
	CString strTable = Trim (Extract (rst.GetSQL (), _T ("FROM "), _T (" ")));
	CString strSQL = FormatSQL (rst, strTable, name, value, _T (""), 0);
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

/*
	try {
		rst.SetFieldValue (_T ("English"),		s.m_strEnglish);
		rst.SetFieldValue (_T ("Spanish"),		s.m_strSpanish);
		rst.SetFieldValue (_T ("Portuguese"),	s.m_strPortuguese);
		rst.SetFieldValue (_T ("Dutch"),		s.m_strDutch);
		rst.SetFieldValue (_T ("Russian"),		s.m_strRussian);
		rst.SetFieldValue (_T ("Hungarian"),	s.m_strHungarian);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/

	return rst;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
const FoxjetDatabase::COdbcRecordset & operator >> (const FoxjetDatabase::COdbcRecordset & rst, INTEGRATESTRUCT & s)
{
	try {
		s.m_strEnglish		= rst.GetFieldValue			(_T ("English"));
		s.m_strTranslated	= rst.GetFieldValue			(_T ("Translated"));
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

FoxjetDatabase::COdbcRecordset & operator << (FoxjetDatabase::COdbcRecordset & rst, const INTEGRATESTRUCT & s)
{
	CStringArray name, value;

	/*
	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}
	*/

	name.Add (_T ("English"));			value.Add (s.m_strEnglish);
	name.Add (_T ("Translated"));		value.Add (s.m_strTranslated);
   
	CString strTable = Trim (Extract (rst.GetSQL (), _T ("FROM "), _T (" ")));
	CString strSQL = FormatSQL (rst, strTable, name, value, _T (""), 0);
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

/*
	try {
		rst.SetFieldValue (_T ("English"),		s.m_strEnglish);
		rst.SetFieldValue (_T ("Translated"),	s.m_strTranslated);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/
	return rst;
}

////////////////////////////////////////////////////////////////////////////////////////////////////


CRcItem::CRcItem (const RCSTRUCT & rhs)
{
	m_strSource		= rhs.m_strSource;
	m_strEnglish	= rhs.m_strEnglish;
	m_strSpanish	= rhs.m_strSpanish;
	m_strPortuguese	= rhs.m_strPortuguese;
	m_strDutch		= rhs.m_strDutch;
	m_strRussian	= rhs.m_strRussian;
	m_strHungarian	= rhs.m_strHungarian;
}

CRcItem::~CRcItem ()
{
}

int CRcItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	return CItem::Compare (rhs, nColumn);
}

CString CRcItem::GetDispText (int nColumn) const
{
	CString str;
	TCHAR sz [MAX_PATH + 1] = { 0 };

	switch (nColumn) {
	case 0:		
		str = m_strSource;
		::GetFileTitle (str, sz, ARRAYSIZE (sz) - 1);

		return sz;
	case 1:		
		str = m_strEnglish;
		break;
	case 2:		
		str = m_strSpanish;
		break;
	case 3:
		str = m_strPortuguese;
		break;
	case 4:
		str = m_strDutch;
		break;
	case 5:
		str = m_strRussian;
		break;
	case 6:
		str = m_strHungarian;
		break;
	}

	CLongArray v = CountChars (str, '\\');

	if (v.GetSize () >= 2) 
		str = str.Mid (v [v.GetSize () - 2] + 1);

	return str;
}


////////////////////////////////////////////////////////////////////////////////////////////////////

class CDelim
{
public:
	CDelim (int nIndex = 0, int nLen = 0) : m_nIndex (nIndex), m_nLen (nLen) { }
	CDelim (const CDelim & rhs) : m_nIndex (rhs.m_nIndex), m_nLen (rhs.m_nLen) { }
	virtual ~CDelim () { }

	CDelim & operator = (const CDelim & rhs)
	{
		if (this != &rhs) {
			m_nIndex	= rhs.m_nIndex;
			m_nLen		= rhs.m_nLen;
		}

		return * this;
	}

	int GetIndex () const { return m_nIndex; }
	int GetLen () const { return m_nLen; }

protected:
	int m_nIndex;
	int m_nLen;
};

////////////////////////////////////////////////////////////////////////////////////////////////////

class CPair
{
public:
	CPair (const CString & strSrc = "", const CString & strExtracted = "") 
		: m_strEnglish (strSrc), m_strExtracted (strExtracted) { }
	CPair (const CPair & rhs) : m_strEnglish (rhs.m_strEnglish), m_strExtracted (rhs.m_strExtracted) { }
	virtual ~CPair () { }

	CPair & operator = (const CPair & rhs)
	{
		if (this != &rhs) {
			m_strEnglish		= rhs.m_strEnglish;
			m_strExtracted	= rhs.m_strExtracted;
		}

		return * this;
	}

	CString m_strEnglish;
	CString m_strExtracted;
};

typedef CArray <CDelim, CDelim &> CDelimArray;
typedef CArray <CPair, CPair &> CPairArray;

////////////////////////////////////////////////////////////////////////////////////////////////////

static int Find (const CString & strFind, const CPairArray & v);
static int Find (const CString & strFind, const CStringArray & v);
static void Count (const CString & str, TCHAR cDelim, CDelimArray & v);
static CLongArray Count (const CString & str, const CString & strFind);
static void ExtractStrings (const CString & strFile, CPairArray & v, CStringArray & vRejected);
static void Extract (const CString & str, CPairArray & v, CStringArray & vRejected);

////////////////////////////////////////////////////////////////////////////////////////////////////

static int Find (const CString & strFind, const CPairArray & v)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (!v [i].m_strExtracted.CompareNoCase (strFind))
			return i;

	return -1;
}

static int Find (const CString & strFind, const CStringArray & v)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (!v [i].CompareNoCase (strFind))
			return i;

	return -1;
}

static void Count (const CString & str, TCHAR cDelim, CDelimArray & v)
{
	int nIndex = 0;

	while ((nIndex = str.Find (cDelim, nIndex)) != -1) {
		int nLen = 0;

		for (int i = nIndex; i < str.GetLength (); i++, nIndex++) {
			if (str [i] == cDelim)
				nLen++;
			else
				break;
		}

		v.Add (CDelim (nIndex - nLen, nLen));
	}

	for (int i = v.GetSize () - 1; i >= 0; i--) 
		if (!(v [i].GetLen () % 2)) // remove even lengths
			v.RemoveAt (i);
}

static void Extract (const CString & str, CPairArray & v, CStringArray & vRejected)
{
/*
DEFPUSHBUTTON   """No read"" string",IDOK,129,7,50,14
IDS_STRING1             """No read"" string"
IDS_STRING2             "\\""No read\\"" string"
IDS_STRING3             "string ""No read"""

"""No read"" string"
"""No read"" string"
"\\""No read\\"" str
"string ""No read"""
*/
	CDelimArray vDelim;
	CPairArray vResult;
	CStringArray vTokens;
	bool bReject = false;
/* TODO: rem
	LPCTSTR lpszReject [] =
	{
		"VIRTKEY",
		"NOINVERT",
		"ASCII",
	};

	Tokenize (str, vTokens, cDelim);

	for (int i = 0; i < vTokens.GetSize (); i++) {
		CString strToken = vTokens [i];

		strToken.TrimLeft ();
		strToken.TrimRight ();

		for (int j = 0; j < (sizeof (lpszReject) / sizeof (lpszReject [0])); j++) {
			if (strToken.Find (lpszReject [j]) != -1) {
				bReject = true;
				break;
			}
		}
	}
*/

	if (!bReject) {
		Count (str, ::cDelim, vDelim);

		for (int i = 0; i < vDelim.GetSize (); i += 2) {
			if ((i + 1) < vDelim.GetSize ()) {
				CDelim start = vDelim [i];
				CDelim end = vDelim [i + 1];
				int nFirst = start.GetIndex () + start.GetLen ();
				int nCount = end.GetIndex () - nFirst;

				CString strExtract = 
					CString (::cDelim, start.GetLen ()) + 
					str.Mid (nFirst, nCount) +
					CString (::cDelim, end.GetLen ());

				if (Find (strExtract, v) == -1) 
					vResult.Add (CPair (str, strExtract));
			}
			else {
				TRACEF (str);
				ASSERT (0);
			}
		}
	}

	v.Append (vResult);

	if (!vDelim.GetSize () && str.Find (cDelim) != -1) {
		CString strTrace (str);

		strTrace.Remove ('\n');
		strTrace.Remove ('\r');
		TRACEF (strTrace);

 		for (int i = 0; i < vResult.GetSize (); i++)
			TRACEF (vResult [i].m_strExtracted);

		vRejected.Add (str);
	}
}

CString ReadDirect (CString strFile, const CString & strDefault = _T(""))
{
	DWORD dwSize = GetFileSize (strFile);
	CString strData = strDefault;

	if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
		const BYTE nUTF16 [] = { 0xFF, 0xFE };
		BYTE * p = new BYTE [dwSize + 1];

		memset (p, 0, dwSize + 1);
		fread (p, dwSize, 1, fp);
		fclose (fp);

		if (!memcmp (p, nUTF16, ARRAYSIZE (nUTF16))) {
			TCHAR * pData = (TCHAR *)p;
			strData = ++pData;
		}
		else
			strData = a2w (p);

		delete [] p;
	}

	return strData;
}

static void ExtractStrings (const CString & strFile, CPairArray & v, CStringArray & vRejected)
{
	TCHAR sz [256]; //char sz [256];
//	bool bParse = true;

	bool bUnicode = IsUnicode (strFile);
//	ASSERT (!bUnicode);

	if (bUnicode) {
		CStringArray vLines;

		Tokenize (ReadDirect (strFile), vLines, '\n');

		for (int i = 0; i < vLines.GetSize (); i++)
			Extract (vLines [i], v, vRejected);
	}
	else {
		if (FILE * fp = _tfopen (strFile, _T ("r"))) {
			do {
				CString str;

				ZeroMemory (sz, ARRAYSIZE (sz));
				_fgetts /* fgets */ (sz, sizeof (sz) - 1, fp);
				str = /* a2w */ (sz);

				while (str.GetLength () && str [str.GetLength () - 1] != '\n') {
					ZeroMemory (sz, ARRAYSIZE (sz));
					_fgetts /* fgets */ (sz, ARRAYSIZE (sz) - 1, fp);
					str += /* a2w */ (sz);
				}

	//			if (str.Find ("APSTUDIO_INVOKED") != -1)
	//				bParse = !bParse;

	//			if (bParse) 
					Extract (str, v, vRejected);
			}
			while (!feof (fp));

			fclose (fp);
		}
	}
}

static CLongArray Count (const CString & str, const CString & strFind)
{
	CLongArray index;
	int nIndex = 0;
	bool bMore = true;

	while (bMore) {
		nIndex = str.Find (strFind, nIndex);

		if (nIndex != -1) {
			index.Add (nIndex);
			nIndex += strFind.GetLength ();
		}
		else
			bMore = false;
	}

	return index;
}

/////////////////////////////////////////////////////////////////////////////
// CParserDlg dialog

const CString CParserDlg::m_strRegSection = _T ("Software\\FoxJet\\Parser\\Defaults");

CParserDlg::CParserDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CParserDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CParserDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

}

void CParserDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CParserDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CParserDlg, CDialog)
	//{{AFX_MSG_MAP(CParserDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_EXTRACTSTRINGS, OnExtractstrings)
	ON_BN_CLICKED(BTN_COPY, OnCopy)
	ON_BN_CLICKED(BTN_FINDNEW, OnFindnew)
	ON_BN_CLICKED(BTN_VERIFY, OnVerify)
	ON_BN_CLICKED(BTN_TRANSLATE, OnTranslate)
	ON_BN_CLICKED(BTN_INTEGRATE, OnIntegrate)
	ON_BN_CLICKED(BTN_COMPACT, OnCompact)
	ON_BN_CLICKED(BTN_GOOGLE, OnGoogle)
	ON_COMMAND(ID_FILE_PARSE_HTML, OnFileParseHtml)
	ON_COMMAND(ID_FILE_GENERATEHTMLTABLE, OnFileGeneratehtmltable)
	ON_COMMAND(ID_FILE_IMPORTSUNNYVALESTABLE, OnFileImportsunnyvalestable)
	ON_BN_CLICKED(BTN_PATH, OnPath)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CParserDlg message handlers


BOOL CParserDlg::OnInitDialog()
{
	using namespace ListCtrlImp;
	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;
	COdbcRecordset rst (theApp.m_db);

	CDialog::OnInitDialog();

	VERIFY (EnablePrivilege (SE_RESTORE_NAME));
	VERIFY (EnablePrivilege (SE_BACKUP_NAME));

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	vCols.Add (ListCtrlImp::CColumn (_T ("Source")));
	vCols.Add (ListCtrlImp::CColumn (_T ("English")));
	vCols.Add (ListCtrlImp::CColumn (_T ("Spanish")));
	vCols.Add (ListCtrlImp::CColumn (_T ("Portuguese")));
	vCols.Add (ListCtrlImp::CColumn (_T ("Dutch")));
	vCols.Add (ListCtrlImp::CColumn (_T ("Russian")));
	vCols.Add (ListCtrlImp::CColumn (_T ("Hungarian")));

	m_lv.Create (LV_FILES, vCols, this, m_strRegSection);

	try {
		rst.Open (_T ("SELECT * FROM Files WHERE Enabled=True;"));

		while (!rst.IsEOF ()) {
			RCSTRUCT rc;

			rst >> rc;
			m_lv.InsertCtrlData (new CRcItem (rc));

			rst.MoveNext ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	//TRACEF (URLDecode (_T ("Invalid%20length&langpair=en%7cnl")));

	{ 
		CString strPath = theApp.GetProfileString (_T ("Settings"), _T ("DevPath"), _T ("C:\\Dev\\BoxWriter\\"));
		SetDlgItemText (TXT_PATH, strPath);
		SetPath (strPath);
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CParserDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CParserDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CParserDlg::OnExtractstrings() 
{
	CStringArray vFiles, vExclude;
	int nAdded = 0, nRejected = 0;

	ASSERT (GetDlgItem (BTN_EXTRACTSTRINGS));

	EnableUI (false);
	
	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		CRcItem * p = (CRcItem *)m_lv.GetCtrlData (i);

		ASSERT (p);
		TRACEF (p->m_strSource);
		vFiles.Add (p->m_strSource);
	}

	try {
		COdbcRecordset rst (theApp.m_db), rstExclude (theApp.m_db), rstRejected (theApp.m_db);

		theApp.m_db.ExecuteSQL (_T ("DELETE * FROM [Extracted]"));
		theApp.m_db.ExecuteSQL (_T ("DELETE * FROM [Rejected]"));

		rst.Open (_T ("SELECT * FROM [Extracted];"), CRecordset::dynaset);
		rstRejected.Open (_T ("SELECT * FROM [Rejected];"), CRecordset::dynaset);
		rstExclude.Open (_T ("SELECT * FROM [Exclude];"));

		while (!rstExclude.IsEOF ()) {
			CString str = rstExclude.GetFieldValue (_T ("English"));

			if (Find (str, vExclude) == -1)
				vExclude.Add (str);

			rstExclude.MoveNext ();
		}

		{ // pick off pending items marked to be excluded
			COdbcRecordset rstPending (theApp.m_db), rstExclude (theApp.m_db);

			rstExclude.Open (_T ("SELECT * FROM [Exclude];"), CRecordset::dynaset);
			rstPending.Open (_T ("SELECT * FROM [Pending];"), CRecordset::dynaset);

			while (!rstPending.IsEOF ()) {
				bool bExclude = (bool)rstPending.GetFieldValue (_T ("Exclude"));

				if (bExclude) {
					CString str = rstPending.GetFieldValue (_T ("English"));

					TRACEF (str);

					if (Find (str, vExclude) == -1) {
						CStringArray name, value;

						vExclude.Add (str);

						name.Add (_T ("English"));			value.Add (str);
						
						rstExclude.SetEditMode (COdbcRecordset::addnew);
						CString strSQL = FormatSQL (rstExclude, _T ("Exclude"), name, value, _T (""), 0);
						VERIFY (theApp.m_db.ExecuteSQL (strSQL));

						/* TODO: rem
						rstExclude.AddNew ();
						rstExclude.SetFieldValue (_T ("English"), str);
						rstExclude.Update ();
						*/
					}
				}

				rstPending.MoveNext ();
			}

			rstPending.Close ();
			rstExclude.Close ();

			theApp.m_db.ExecuteSQL (_T ("DELETE * FROM [Pending] WHERE Exclude=True;"));
		}

		for (int nFile = 0; nFile < vFiles.GetSize (); nFile++) {
			CPairArray vStrings;
			CStringArray vRejected;
			CString strFile = vFiles [nFile];
			TCHAR szTitle [MAX_PATH + 1] = { 0 };

			::GetFileTitle (strFile, szTitle, ARRAYSIZE (szTitle) - 1);
			const CString strTitle (szTitle);
			ExtractStrings (strFile, vStrings, vRejected);

			for (int nString = 0; nString < vStrings.GetSize (); nString++) {
				CString str = vStrings [nString].m_strExtracted;
				CString strSrc = vStrings [nString].m_strEnglish;

				if (str.GetLength ()) {
					if (str [0] == ::cDelim && str [str.GetLength () - 1] == ::cDelim) {
						str.Delete (str.GetLength () - 1);
						str.Delete (0);
					}
				}

				if (Find (str, vExclude) == -1) {
					CStringArray name, value;

					name.Add (_T ("File"));				value.Add (strTitle);
					name.Add (_T ("English"));			value.Add (str);
					name.Add (_T ("Source"));			value.Add (strSrc);
						
					rst.SetEditMode (COdbcRecordset::addnew);
					CString strSQL = FormatSQL (rst, _T ("Extracted"), name, value, _T (""), 0);
					VERIFY (theApp.m_db.ExecuteSQL (strSQL));

					/* TODO: rem
					rst.AddNew ();
					rst.SetFieldValue (_T ("File"), strTitle);
					rst.SetFieldValue (_T ("English"), str);
					rst.SetFieldValue (_T ("Source"), strSrc);
					rst.Update ();
					*/

					nAdded++;
				}
				else {
					CStringArray name, value;

					name.Add (_T ("File"));				value.Add (strTitle);
					name.Add (_T ("Source"));			value.Add (str);
					name.Add (_T ("Duplicate"));		value.Add (ToString ((int)true));
						
					rstRejected.SetEditMode (COdbcRecordset::addnew);
					CString strSQL = FormatSQL (rstRejected, _T ("Rejected"), name, value, _T (""), 0);
					VERIFY (theApp.m_db.ExecuteSQL (strSQL));

					/* TODO: rem
					rstRejected.AddNew ();
					rstRejected.SetFieldValue (_T ("File"), strTitle);
					rstRejected.SetFieldValue (_T ("Source"), str);
					rstRejected.SetFieldValue (_T ("Duplicate"), true);
					rstRejected.Update ();
					*/

					nRejected++;
				}
			}

			for (int nString = 0; nString < vRejected.GetSize (); nString++) {
				CString str = vRejected [nString];
				CStringArray name, value;

				name.Add (_T ("File"));				value.Add (strTitle);
				name.Add (_T ("Source"));			value.Add (str);
				name.Add (_T ("Duplicate"));		value.Add (ToString ((int)false));
						
				rstRejected.SetEditMode (COdbcRecordset::addnew);
				CString strSQL = FormatSQL (rstRejected, _T ("Rejected"), name, value, _T (""), 0);
				VERIFY (theApp.m_db.ExecuteSQL (strSQL));

				/* TODO: rem
				rstRejected.AddNew ();
				rstRejected.SetFieldValue (_T ("File"), strTitle);
				rstRejected.SetFieldValue (_T ("Source"), str);
				rstRejected.SetFieldValue (_T ("Duplicate"), false);
				rstRejected.Update ();
				*/

			}
		}			

		rstExclude.Close ();
		rstRejected.Close ();
		rst.Close ();
		Purge ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	CString str;

	str.Format (
		_T ("%d strings added to [Extracted]\n")
		_T ("%d strings rejected to [Rejected]"),
		nAdded, 
		nRejected);
	MessageBox (str);

	EnableUI (true);
}

void CParserDlg::Purge ()
{
	const CString strReason = _T ("No longer referenced in .rc files");
	CStringArray v;

	try 
	{
		COdbcRecordset rstTranslated (theApp.m_db);
		COdbcRecordset rstObsolete (theApp.m_db);

		theApp.m_db.ExecuteSQL (_T ("DELETE * FROM Obsolete"));

		rstTranslated.Open (_T ("SELECT * FROM [Translated];"), CRecordset::dynaset);
		rstObsolete.Open (_T ("SELECT * FROM [Obsolete];"), CRecordset::dynaset);

		for (int nLine = 1; !rstTranslated.IsEOF (); nLine++) {
			TRANSLATEDSTRUCT t;
			CString str = rstTranslated.GetFieldValue (_T ("English"));
			bool bExists = RecordExists (theApp.m_db, LANGTYPE_ENGLISH, str, _T ("Extracted"));

			if (!bExists) {
				TRACEF (strReason + _T (": ") + str);

				CStringArray name, value;

				name.Add (_T ("English"));			value.Add (str);
				name.Add (_T ("Reason"));			value.Add (strReason);
				name.Add (_T ("Record"));			value.Add (ToString ((int)nLine));
						
				rstObsolete.SetEditMode (COdbcRecordset::addnew);
				CString strSQL = FormatSQL (rstObsolete, _T ("Obsolete"), name, value, _T (""), 0);
				VERIFY (theApp.m_db.ExecuteSQL (strSQL));

				/* TODO: rem
				rstObsolete.AddNew ();
				rstObsolete.SetFieldValue (_T ("English"), str);
				rstObsolete.SetFieldValue (_T ("Reason"), strReason);
				rstObsolete.SetFieldValue (_T ("Record"), nLine);
				rstObsolete.Update ();
				*/

				v.Add (str);
			}

			rstTranslated.MoveNext ();
		}

		rstTranslated.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	/*
	if (v.GetSize ()) {
		CString str;

		str.Format (_T ("%d records: %s\nsee: [Obsolete]\n\nPurge them from [Translated]?"), v.GetSize (), strReason);
		
		if (MessageBox (str, NULL, MB_ICONINFORMATION | MB_YESNO) == IDYES) {
			try
			{
				for (int i = 0; i < v.GetSize (); i++) 
					DeleteRecord (theApp.m_db, LANGTYPE_ENGLISH, v [i], _T ("Translated"));
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
		}
	}
	*/
}

void CParserDlg::GetFiles (const CString & strDir, const CString & strFile, CStringArray & v)
{
	WIN32_FIND_DATA fd;
	const CString str = strDir + _T ("\\") + strFile;
	HANDLE hFind = ::FindFirstFile (str, &fd);
	bool bMore = hFind != INVALID_HANDLE_VALUE;

	while (bMore) {
		CString str = strDir;
		
		if (str [str.GetLength () - 1] != '\\')
			str += '\\';

		str += fd.cFileName;

		v.Add (str);
		ZeroMemory (&fd, sizeof (fd));
		bMore = ::FindNextFile (hFind, &fd) ? true : false;
	}
}

CString CParserDlg::GetDir (const CString & strFullPath)
{
	TCHAR szTitle [MAX_PATH + 1] = { 0 };

	::GetFileTitle (strFullPath, szTitle, ARRAYSIZE (szTitle) - 1);
	
	CString str (strFullPath);
	int nTitleLen = _tcslen (szTitle);
	str.Delete (str.GetLength () - nTitleLen, nTitleLen);

	return str;
}

bool CParserDlg::CopyDir (const CString & strSrc, const CString & strDest)
{
	CStringArray v;

	GetFiles (strSrc, "*.*", v);

	for (int i = 0; i < v.GetSize (); i++) {
		TCHAR szTitle [MAX_PATH + 1] = { 0 };
		CString strFile = v [i];

		::GetFileTitle (strFile, szTitle, ARRAYSIZE (szTitle) - 1);
		CString strTitle (szTitle);

		if (strTitle != "." && strTitle != "..") {
			CString str = strDest + strTitle;

			if (!DirExists (strDest))
				if (!::CreateDirectory (strDest, NULL))
					return false;

			if (DirExists (strFile)) {
				if (!CopyDir (strFile + "\\", str + "\\"))
					return false;
			}
			else if (!::CopyFile (strFile, str, FALSE))
				return false;
		}
	}

	return true;
}

void CParserDlg::OnCopy() 
{
	EnableUI (false);

	CStringArray vErrors;
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize () == 0) 
		for (int i = 0; i < m_lv->GetItemCount (); i++) 
			sel.Add (i);

	for (int i = 0; i < sel.GetSize (); i++) {
		CRcItem * p = (CRcItem *)m_lv.GetCtrlData (sel [i]);

		ASSERT (p);
		
/*
		TRACEF (CString ("Copying...") +
			"\n\t" + p->m_strSource + 
			"\n\t\t--> " + p->m_strEnglish +
			"\n\t\t--> " + p->m_strSpanish +
			"\n\t\t--> " + p->m_strPortuguese);
*/

		DWORD dwAttr = ::GetFileAttributes (p->m_strSource);

		if (dwAttr == -1)
			vErrors.Add ("File does not exist: " + p->m_strSource);
		else if (dwAttr & FILE_ATTRIBUTE_DIRECTORY)
			vErrors.Add ("File is a directory: " + p->m_strSource);
		else {
			CString strRes			= _T ("res\\");
			CString strHeader		= _T ("resource.h");
			CString strSource		= GetDir (p->m_strSource);
			CString strEnglish		= GetDir (p->m_strEnglish);
			CString strSpanish		= GetDir (p->m_strSpanish);
			CString strPortuguese	= GetDir (p->m_strPortuguese);
			CString strDutch		= GetDir (p->m_strDutch);
			CString strRussian		= GetDir (p->m_strRussian);
			CString strHungarian	= GetDir (p->m_strHungarian);

			if (!DirExists (strEnglish))
				VERIFY (::CreateDirectory (strEnglish, NULL));
			
			if (!DirExists (strSpanish))
				VERIFY (::CreateDirectory (strSpanish, NULL));

			if (!DirExists (strPortuguese))
				VERIFY (::CreateDirectory (strPortuguese, NULL));

			if (!DirExists (strDutch))
				VERIFY (::CreateDirectory (strDutch, NULL));

			if (!DirExists (strRussian))
				VERIFY (::CreateDirectory (strRussian, NULL));

			if (!DirExists (strHungarian))
				VERIFY (::CreateDirectory (strHungarian, NULL));


			if (!::CopyFile (p->m_strSource, p->m_strEnglish, FALSE))
				vErrors.Add ("Failed to copy to: " + p->m_strEnglish);

			if (!::CopyFile (p->m_strSource, p->m_strSpanish, FALSE))
				vErrors.Add ("Failed to copy to: " + p->m_strSpanish);

			if (!::CopyFile (p->m_strSource, p->m_strPortuguese, FALSE))
				vErrors.Add ("Failed to copy to: " + p->m_strPortuguese);

			if (!::CopyFile (p->m_strSource, p->m_strDutch, FALSE))
				vErrors.Add ("Failed to copy to: " + p->m_strDutch);

			if (!::CopyFile (p->m_strSource, p->m_strRussian, FALSE))
				vErrors.Add ("Failed to copy to: " + p->m_strRussian);

			if (!::CopyFile (p->m_strSource, p->m_strHungarian, FALSE))
				vErrors.Add ("Failed to copy to: " + p->m_strHungarian);

			// copy resource.h
			if (!::CopyFile (strSource + strHeader, strEnglish + strHeader, FALSE))
				vErrors.Add ("Failed to copy to: " + strEnglish + strHeader);

			if (!::CopyFile (strSource + strHeader, strSpanish + strHeader, FALSE))
				vErrors.Add ("Failed to copy to: " + strSpanish + strHeader);

			if (!::CopyFile (strSource + strHeader, strPortuguese + strHeader, FALSE))
				vErrors.Add ("Failed to copy to: " + strPortuguese + strHeader);

			if (!::CopyFile (strSource + strHeader, strDutch + strHeader, FALSE))
				vErrors.Add ("Failed to copy to: " + strDutch + strHeader);

			if (!::CopyFile (strSource + strHeader, strRussian + strHeader, FALSE))
				vErrors.Add ("Failed to copy to: " + strRussian + strHeader);

			if (!::CopyFile (strSource + strHeader, strHungarian + strHeader, FALSE))
				vErrors.Add ("Failed to copy to: " + strHungarian + strHeader);

			// copy res dir
			if (!CopyDir (strSource + strRes, strEnglish + strRes)) 
				vErrors.Add ("Failed to copy to: " + strEnglish + strRes);

			if (!CopyDir (strSource + strRes, strSpanish + strRes))
				vErrors.Add ("Failed to copy to: " + strSpanish + strRes);

			if (!CopyDir (strSource + strRes, strPortuguese + strRes))
				vErrors.Add ("Failed to copy to: " + strPortuguese + strRes);

			if (!CopyDir (strSource + strRes, strDutch + strRes))
				vErrors.Add ("Failed to copy to: " + strDutch + strRes);

			if (!CopyDir (strSource + strRes, strRussian + strRes))
				vErrors.Add ("Failed to copy to: " + strRussian + strRes);

			if (!CopyDir (strSource + strRes, strHungarian + strRes))
				vErrors.Add ("Failed to copy to: " + strHungarian + strRes);
		}
	}

	if (vErrors.GetSize ()) {
		CString str;

		for (int i = 0; i < vErrors.GetSize (); i++) {
			TRACEF (vErrors [i]);
			str += vErrors [i] + "\n";
		}

		MessageBox (str, NULL, MB_ICONERROR);
	}
	else 
		MessageBox (_T ("All files copied"));

	EnableUI (true);
}

bool CParserDlg::IsExcluded (const CString & str)
{
	bool bResult = false;

	try {
		COdbcRecordset rst (theApp.m_db);

//		if (str.Find ('\"') == -1 && str.Find ('\'') == -1) {
		if (!HasQuotes (str)) {
			CString strSQL;

			strSQL.Format (_T ("SELECT * FROM Exclude WHERE English='%s';"), str);
			rst.Open (strSQL);
			bResult = !rst.IsEOF ();
			rst.Close ();
		}
		else {
			rst.Open (_T ("SELECT * FROM Exclude"));

			while (!rst.IsEOF ()) {
				CString strCompare = rst.GetFieldValue (_T ("English"));

				if (!str.CompareNoCase (strCompare)) {
					bResult = true;
					break;
				}

				rst.MoveNext ();
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

bool CParserDlg::HasQuotes (const CString & str)
{
	return (str.Find ('\"') != -1 || str.Find ('\'') != -1);
}

bool CParserDlg::Translated (const CString & str)
{
	bool bResult = false;

	try {
		COdbcRecordset rst (theApp.m_db);

		if (!HasQuotes (str)) {
			CString strSQL;

			strSQL.Format (_T ("SELECT * FROM Translated WHERE English='%s';"), str);
			rst.Open (strSQL);
			bResult = !rst.IsEOF ();
			rst.Close ();
		}
		else {
			rst.Open (_T ("SELECT * FROM Translated"));


			while (!rst.IsEOF ()) {
				CString strTranslated = rst.GetFieldValue (_T ("English"));

				if (!str.CompareNoCase (strTranslated)) {
					bResult = true;
					break;
				}

				rst.MoveNext ();
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

void CParserDlg::OnFindnew() 
{
	EnableUI (false);
	int nCount = 0;

	try {
		COdbcRecordset rstPending (theApp.m_db), rstExtracted (theApp.m_db);

		theApp.m_db.ExecuteSQL (_T ("DELETE * FROM [Pending]"));

		rstPending.Open (_T ("SELECT * FROM [Pending];"), CRecordset::dynaset);
		rstExtracted.Open (_T ("SELECT * FROM [Extracted];"));

		while (!rstExtracted.IsEOF ()) {
			CString str = rstExtracted.GetFieldValue (_T ("English"));

			bool bTranslated = Translated (str);

			if (!bTranslated) {
				CString strSNAFU (str);

				strSNAFU.Replace (_T ("..."), ::szSNAFU);
				bTranslated = Translated (strSNAFU);
			}

			if (!bTranslated) {
				if (str.GetLength ()) {
					TRACEF (str);

					CStringArray name, value;

					name.Add (_T ("English"));			value.Add (str);
					name.Add (_T ("Needed"));			value.Add (_T ("Spanish, Portuguese, Dutch, Russian, Hungarian"));
						
					rstPending.SetEditMode (COdbcRecordset::addnew);
					CString strSQL = FormatSQL (rstPending, _T ("Pending"), name, value, _T (""), 0);
					VERIFY (theApp.m_db.ExecuteSQL (strSQL));

					/* TODO: rem
					rstPending.AddNew ();
					rstPending.SetFieldValue (_T ("English"), str);
					rstPending.SetFieldValue (_T ("Needed"), CString (_T ("Spanish, Portuguese, Dutch, Russian, Hungarian")));
					rstPending.Update ();
					*/

					nCount++;
				}
			}

			rstExtracted.MoveNext ();
		}

		rstExtracted.Close ();
		rstPending.Close ();

		for (int i = 0; i < ARRAYSIZE (types); i++) 
			nCount += FindNulls (types [i]);
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	CString str;

	str.Format (_T ("%d new strings added to [Pending]"), nCount);
	MessageBox (str);

	EnableUI (true);
}

int CParserDlg::FindNulls (LANGTYPE type)
{
	CString strLang = GetLangString (type);
	int nCount = 0;

	try {
		COdbcRecordset rstPending (theApp.m_db), rstTranslated (theApp.m_db);
		CString strSQL;

		rstPending.Open (_T ("SELECT * FROM [Pending];"), CRecordset::dynaset);

		strSQL.Format (_T ("SELECT * FROM [Translated] WHERE ([%s] Is Null);"), strLang);
		rstTranslated.Open (strSQL);

		while (!rstTranslated.IsEOF ()) {
			CString str = rstTranslated.GetFieldValue (_T ("English"));

			if (str.GetLength ()) {
				CStringArray name, value;

				name.Add (_T ("English"));			value.Add (str);
				name.Add (_T ("Needed"));			value.Add (strLang);
						
				rstPending.SetEditMode (COdbcRecordset::addnew);
				CString strSQL = FormatSQL (rstPending, _T ("Pending"), name, value, _T (""), 0);
				VERIFY (theApp.m_db.ExecuteSQL (strSQL));

				/* TODO: rem
				rstPending.AddNew ();
				rstPending.SetFieldValue (_T ("English"), str);
				rstPending.SetFieldValue (_T ("Needed"), strLang);
				rstPending.Update ();
				*/
				nCount++;
			}

			rstTranslated.MoveNext ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return nCount;
}

void CParserDlg::EnableUI(bool bEnable)
{
	UINT nID [] = 
	{
		BTN_COPY,
		BTN_INTEGRATE,
		BTN_EXTRACTSTRINGS,
		BTN_FINDNEW,
		BTN_GOOGLE,
		BTN_VERIFY,
		BTN_TRANSLATE,
		BTN_COMPACT,
	};

	if (!bEnable) 
		BeginWaitCursor ();
	else
		EndWaitCursor ();

	for (int i = 0; i < ARRAYSIZE (nID); i++)
		if (CWnd * p = GetDlgItem (nID [i]))
			p->EnableWindow (bEnable);
}

void CParserDlg::OnVerify() 
{
	EnableUI (false);
	int nCount = 0, nPreviousCount = 0;
	int nLine = 1;
	const CString strDutchErrors = _T ("C:\\temp\\debug\\dutch errors.txt");

	FILE * fpDutch = NULL;//_tfopen (strDutchErrors, _T ("w"));

	if (fpDutch) {
		WORD w = 0xFEFF;
		//fwrite (&w, sizeof (w), 1, fpDutch); // Unicode header
	}

	try {
		{
			COdbcRecordset rstUnverified (theApp.m_db);

			rstUnverified.Open (_T ("SELECT * FROM [Unverified];"), CRecordset::dynaset);

			for ( ; !rstUnverified.IsEOF (); rstUnverified.MoveNext ())
				nPreviousCount++;
		}

		COdbcRecordset rst (theApp.m_db);
		COdbcRecordset rstUnverified (theApp.m_db);

		theApp.m_db.ExecuteSQL (_T ("DELETE * FROM Unverified"));

		rstUnverified.Open (_T ("SELECT * FROM [Unverified];"), CRecordset::dynaset);
		rst.Open (_T ("SELECT * FROM [Translated] ORDER BY [English] ASC;"), CRecordset::dynaset);

		while (!rst.IsEOF ()) {
			CString strReason;
			CString strEnglish		= rst.GetFieldValue (_T ("English"));
			CString strSpanish		= rst.GetFieldValue (_T ("Spanish"));
			CString strPortuguese	= rst.GetFieldValue (_T ("Portuguese"));			
			CString strDutch		= rst.GetFieldValue (_T ("Dutch"));			
			CString strRussian		= rst.GetFieldValue (_T ("Russian"));			
			CString strHungarian	= rst.GetFieldValue (_T ("Hungarian"));			
			/*
			CLongArray periods [::nTranslations + 1] = 
			{
				CountChars (strEnglish, '.'),
				CountChars (strSpanish, '.'),
				CountChars (strPortuguese, '.'),
				CountChars (strDutch, '.'),
				CountChars (strRussian, '.'),
				CountChars (strHungarian, '.'),
			};
			CLongArray singleQuotes [::nTranslations + 1] = 
			{
				CountChars (strEnglish, '\''),
				CountChars (strSpanish, '\''),
				CountChars (strPortuguese, '\''),
				CountChars (strDutch, '\''),
				CountChars (strRussian, '\''),
				CountChars (strHungarian, '\''),
			};
			*/
			CLongArray doubleQuotes [::nTranslations + 1] = 
			{
				CountChars (strEnglish, '\"'),
				CountChars (strSpanish, '\"'),
				CountChars (strPortuguese, '\"'),
				CountChars (strDutch, '\"'),
				CountChars (strRussian, '\"'),
				CountChars (strHungarian, '\"'),
			};
			CLongArray percent = Count (strEnglish, '%');
			CLongArray backslash = Count (strEnglish, '\\');
			static const LPCTSTR lpszSingleQuote = _T ("'");
			static const LPCTSTR lpszDoubleQuote = _T ("\"");
			static const LPCTSTR lpszPeriod = _T (".");

			//ASSERT (ARRAYSIZE (singleQuotes) == ARRAYSIZE (doubleQuotes));
			//ASSERT (ARRAYSIZE (singleQuotes) == ARRAYSIZE (periods));

			for (int i = 0; i < (ARRAYSIZE (doubleQuotes) - 1); i++) {
				/*
				if (singleQuotes [i].GetSize () != singleQuotes [i + 1].GetSize ())
					if (strReason.Find (lpszSingleQuote) == -1)
						strReason += lpszSingleQuote + CString (" ");
				*/

				if (doubleQuotes [i].GetSize () != doubleQuotes [i + 1].GetSize ())
					if (strReason.Find (lpszDoubleQuote) == -1)
						strReason += lpszDoubleQuote + CString (" ");

				/*
				if (periods [i].GetSize () != periods [i + 1].GetSize ())
					if (strReason.Find (lpszPeriod) == -1)
						strReason += lpszPeriod + CString (" ");
				*/
			}

			for (int i = 0; i < percent.GetSize (); i++) {
				CString strSpecifier = strEnglish.Mid (percent [i], 2);
				CLongArray specifiers [::nTranslations + 1] = 
				{
					Count (strEnglish, strSpecifier),
					Count (strSpanish, strSpecifier),
					Count (strPortuguese, strSpecifier),
					Count (strDutch, strSpecifier),
					Count (strRussian, strSpecifier),
					Count (strHungarian, strSpecifier),
				};

				for (int j = 0; j < (ARRAYSIZE (specifiers) - 1); j++) {
					if (specifiers [j].GetSize () != specifiers [j + 1].GetSize ()) {
						if (strReason.Find (strSpecifier) == -1) {
							strReason += strSpecifier + CString (" ");
							break;
						}
					}
				}
			}
			if (percent.GetSize () == 0) {
				TCHAR c = '%';
				CLongArray specifiers [::nTranslations] = 
				{
					Count (strSpanish,	c),
					Count (strPortuguese, c),
					Count (strDutch, c),
					Count (strRussian, c),
					Count (strHungarian, c),
				};

				for (int i = 0; i < ARRAYSIZE (specifiers); i++) {
					if (specifiers [i].GetSize ()) {
						strReason += CString ("[") + c + CString ("] ");
						break;
					}
				}
			}

			////////////////////////////////////////////////////////////////////////////////

			for (int i = 0; i < backslash.GetSize (); i++) {
				CString strSpecifier = strEnglish.Mid (backslash [i], 2);
				CLongArray specifiers [::nTranslations + 1] = 
				{
					Count (strEnglish, strSpecifier),
					Count (strSpanish, strSpecifier),
					Count (strPortuguese, strSpecifier),
					Count (strDutch, strSpecifier),
					Count (strRussian, strSpecifier),
					Count (strHungarian, strSpecifier),
				};

				for (int j = 0; j < (ARRAYSIZE (specifiers) - 1); j++) {
					if (specifiers [j].GetSize () != specifiers [j + 1].GetSize ()) {
						if (strReason.Find (strSpecifier) == -1) {
							strReason += strSpecifier + CString (" ");
							break;
						}
					}
				}
			}
			if (backslash.GetSize () == 0) {
				TCHAR c = '\\';
				CLongArray specifiers [::nTranslations] = 
				{
					Count (strSpanish, c),
					Count (strPortuguese, c),
					Count (strDutch, c),
					Count (strRussian, c),
					Count (strHungarian, c),
				};

				for (int i = 0; i < ARRAYSIZE (specifiers); i++) {
					if (specifiers [i].GetSize ()) {
						strReason += CString ("[") + c + CString ("] ");
						break;
					}
				}
			}

			////////////////////////////////////////////////////////////////////////////////
			{
				const CString strAmp = _T ("amp;");

				if ((strSpanish.Find (strAmp) != -1) ||
					(strPortuguese.Find (strAmp) != -1) ||
					(strDutch.Find (strAmp) != -1) ||
					(strRussian.Find (strAmp) != -1) ||
					(strHungarian.Find (strAmp) != -1))
				{
					strReason += CString ("[") + strAmp + CString ("] ");
				}
			}

			{
				if (strDutch.Find (_T ("ë")) != -1)
					strReason += CString (_T ("[ë] "));

				/*
				for (int i = 0; i < strDutch.GetLength (); i++) {
					TCHAR c = strDutch [i];

					if (c > 128) { // seriële should be seri�le 
						strReason += CString ("[UNICODE] ");
						break;
					}
				}
				*/
			}

			////////////////////////////////////////////////////////////////////////////////

			if (IsExcluded (strEnglish)) 
				strReason += "[Excluded] ";

			/*
			if (strEnglish == strSpanish)		strReason += _T ("!sp ");
			if (strEnglish == strPortuguese)	strReason += _T ("!po ");
			if (strEnglish == strDutch)			strReason += _T ("!nl ");
			if (strEnglish == strRussian)		strReason += _T ("!ru ");
			if (strEnglish == strHungarian)		strReason += _T ("!hu ");
			*/

			if (strReason.GetLength ()) {
				if (strReason.Find (lpszPeriod) != -1) {
					int nEnglish = CountChars (strEnglish, '.').GetSize ();
					int nDutch = CountChars (strDutch, '.').GetSize ();
					int nRussian = CountChars (strRussian, '.').GetSize ();
					int nHungarian = CountChars (strHungarian, '.').GetSize ();

					if (nEnglish == nDutch == nRussian == nHungarian) {
						strReason.Replace (lpszPeriod, _T (""));
						strReason.TrimLeft (); 
						strReason.TrimRight ();
					}
				}
			}
			
			if (strReason.GetLength ()) {
				CString strLine;

				strLine.Format (_T ("%d"), nLine);

				if (fpDutch) {
					CString str;

					strDutch = UnformatGoogle (strDutch);

					if (strDutch.GetLength ()) {
						str = strEnglish + _T ("\n") + strDutch + _T ("\n\n");
						//fwrite ((LPVOID)(LPCTSTR)str, str.GetLength () * sizeof (TCHAR), 1, fpDutch);
						fwrite ((LPVOID)w2a (str), str.GetLength (), 1, fpDutch);

						TRACEF (strDutch);

						rst.Edit ();
						rst.SetFieldValue (_T ("Dutch"), strDutch);
						rst.Update ();
					}
				}

				CStringArray name, value;

				name.Add (_T ("English"));			value.Add (strEnglish);
				name.Add (_T ("Spanish"));			value.Add (strSpanish);
				name.Add (_T ("Portuguese"));		value.Add (strPortuguese);
				name.Add (_T ("Dutch"));			value.Add (strDutch);
				name.Add (_T ("Russian"));			value.Add (strRussian);
				name.Add (_T ("Hungarian"));		value.Add (strHungarian);
				name.Add (_T ("Reason"));			value.Add (strReason);
				name.Add (_T ("Record"));			value.Add (strLine);
						
				rstUnverified.SetEditMode (COdbcRecordset::addnew);
				CString strSQL = FormatSQL (rstUnverified, _T ("Unverified"), name, value, _T (""), 0);
				VERIFY (theApp.m_db.ExecuteSQL (strSQL));

				/* TODO: rem
				rstUnverified.AddNew ();
				rstUnverified.SetFieldValue (_T ("English"), strEnglish);
				rstUnverified.SetFieldValue (_T ("Spanish"), strSpanish);
				rstUnverified.SetFieldValue (_T ("Portuguese"), strPortuguese);
				rstUnverified.SetFieldValue (_T ("Dutch"), strDutch);
				rstUnverified.SetFieldValue (_T ("Russian"), strRussian);
				rstUnverified.SetFieldValue (_T ("Hungarian"), strHungarian);
				rstUnverified.SetFieldValue (_T ("Reason"), strReason);
				rstUnverified.SetFieldValue (_T ("Record"), strLine);
				rstUnverified.Update ();
				*/

				nCount++;
			}

			rst.MoveNext ();
			nLine++;
		}

		rst.Close ();
		rstUnverified.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	CString str;

	str.Format (_T ("%d strings [%d previous] found in an inconsistent state\nsee [Unverified]"), nCount, nPreviousCount);

	if (fpDutch) {
		fclose (fpDutch);
		str += _T ("\n\nsee: ") + strDutchErrors;
	}

	MessageBox (str);

	EnableUI (true);
} 

int CParserDlg::Replace (CString & str, const CString & strOld, const CString & strNew)
{
	int nResult = 0;
	CString strFind (strOld);
	CString strTmp (str);
	bool bMore = true;
	int nFind = 0;

	strFind.MakeUpper ();
	strTmp.MakeUpper ();

	while (bMore) { 
		nFind = strTmp.Find (strFind, nFind);

		if (nFind == -1)
			bMore = false;
		else {
			//TRACEF (str); TRACEF (strTmp);
			str.Delete (nFind, strFind.GetLength ());
			strTmp.Delete (nFind, strFind.GetLength ());

			str.Insert (nFind, strNew);
			strTmp.Insert (nFind, strNew);
			//TRACEF (str); TRACEF (strTmp);

			nFind += strNew.GetLength ();

			nResult++;
		}
	}

	return nResult;
}

int CParserDlg::Translate (LANGTYPE type, const CStringArray & vInput, const CRcItem * p,
						   CArray <TRANSLATEDSTRUCT, TRANSLATEDSTRUCT &> & vTranslated,
						   CString & strError)
{
	int nCount = 0;
	CStringArray vOutput;
	CString strOutputFile;

	ASSERT (p);

	switch (type) {
	case LANGTYPE_SPANISH:		strOutputFile = p->m_strSpanish;	break;
	case LANGTYPE_PORTUGUESE:	strOutputFile = p->m_strPortuguese;	break;
	case LANGTYPE_DUTCH:		strOutputFile = p->m_strDutch;		break;
	case LANGTYPE_RUSSIAN:		strOutputFile = p->m_strRussian;	break;
	case LANGTYPE_HUNGARIAN:	strOutputFile = p->m_strHungarian;	break;
	default:
		ASSERT (0);
		break;
	}

	for (int nLine = 0; nLine < vInput.GetSize (); nLine++) {
		CString str = vInput [nLine];

		for (int nTrans = 0; nTrans < vTranslated.GetSize (); nTrans++) {
			const TRANSLATEDSTRUCT & t = vTranslated [nTrans];
			CString strTranslate;
			
			switch (type) {
			case LANGTYPE_SPANISH:		strTranslate = t.m_strSpanish;		break;
			case LANGTYPE_PORTUGUESE:	strTranslate = t.m_strPortuguese;	break;
			case LANGTYPE_DUTCH:		strTranslate = t.m_strDutch;		break;
			case LANGTYPE_RUSSIAN:		strTranslate = t.m_strRussian;		break;
			case LANGTYPE_HUNGARIAN:	strTranslate = t.m_strHungarian;	break;
			default:
				ASSERT (0);
				break;
			}

			if (strTranslate != "\"\"") { // not empty
				//nCount += str.Replace (t.m_strEnglish, strTranslate);
				nCount += Replace (str, t.m_strEnglish, strTranslate);
			}
		}

		vOutput.Add (str);
	}

	if (FILE * fp = _tfopen (strOutputFile, _T ("wb"))) {
		WORD w = 0xFEFF;
		TRACEF ("Writing: " + strOutputFile);

		fwrite (&w, sizeof (w), 1, fp); // Unicode header

		for (int nLine = 0; nLine < vOutput.GetSize (); nLine++) {
			CString str = vOutput [nLine];

			fwrite ((LPVOID)(LPCTSTR)str, str.GetLength () * sizeof (TCHAR), 1, fp);
		}
		
		fclose (fp);
	}
	else
		strError += "Failed to write: " + strOutputFile + "\n";

	return nCount;
}

void CParserDlg::OnTranslate() 
{
	EnableUI (false);

	CString strError;
	CArray <DUPLICATESTRUCT, DUPLICATESTRUCT &> vStretch;
	CArray <TRANSLATEDSTRUCT, TRANSLATEDSTRUCT &> vTranslated;
	CLongArray sel; 
	int nCount = 0;

#ifdef _DEBUG
	sel = GetSelectedItems (m_lv);
#endif // _DEBUG

	if (!sel.GetSize ()) 
		for (int i = 0; i < m_lv->GetItemCount (); i++)
			sel.Add (i);

	try {
		COdbcRecordset rst (theApp.m_db);
		ULONG lRecord = 1;

		rst.Open (_T ("SELECT * FROM Translated;"));

		while (!rst.IsEOF ()) {
			TRANSLATEDSTRUCT t;

			rst >> t;
			
			t.m_strSpanish.Replace (::szSNAFU, _T ("...")); 
			t.m_strPortuguese.Replace (::szSNAFU, _T ("...")); 
			t.m_strDutch.Replace (::szSNAFU, _T ("...")); 
			t.m_strRussian.Replace (::szSNAFU, _T ("...")); 
			t.m_strHungarian.Replace (::szSNAFU, _T ("...")); 

			t.m_strEnglish		= _T ("\"") + t.m_strEnglish + _T ("\"");
			t.m_strSpanish		= _T ("\"") + t.m_strSpanish + _T ("\"");
			t.m_strPortuguese	= _T ("\"") + t.m_strPortuguese + _T ("\"");
			t.m_strDutch		= _T ("\"") + t.m_strDutch	+ _T ("\"");
			t.m_strRussian		= _T ("\"") + t.m_strRussian + _T ("\"");
			t.m_strHungarian	= _T ("\"") + t.m_strHungarian	+ _T ("\"");
			
			{ // length check
				CString strEnglish = t.m_strEnglish;
				CString strSpanish = t.m_strSpanish;
				CString strPortuguese = t.m_strPortuguese;
				CString strDutch = t.m_strDutch;
				CString strRussian = t.m_strRussian;
				CString strHungarian = t.m_strHungarian;
				
				// remove first/last quotes 
				strEnglish.Delete (0);		strEnglish.Delete (strEnglish.GetLength () - 1);
				strSpanish.Delete (0);		strSpanish.Delete (strSpanish.GetLength () - 1);
				strPortuguese.Delete (0);	strPortuguese.Delete (strPortuguese.GetLength () - 1);
				strDutch.Delete (0);		strDutch.Delete (strDutch.GetLength () - 1);
				strRussian.Delete (0);		strRussian.Delete (strRussian.GetLength () - 1);
				strHungarian.Delete (0);	strHungarian.Delete (strDutch.GetLength () - 1);

				if (IsStretched (strEnglish, strSpanish) || 
					IsStretched (strEnglish, strPortuguese) ||
					IsStretched (strEnglish, strDutch) ||
					IsStretched (strEnglish, strRussian) ||
					IsStretched (strEnglish, strHungarian))
				{
					int nStretch [::nTranslations] = {
						(int)((double)strSpanish.GetLength ()	/ (double)strEnglish.GetLength () * 100.0),
						(int)((double)strPortuguese.GetLength () / (double)strEnglish.GetLength () * 100.0),
						(int)((double)strDutch.GetLength () / (double)strDutch.GetLength () * 100.0),
						(int)((double)strRussian.GetLength () / (double)strRussian.GetLength () * 100.0),
						(int)((double)strHungarian.GetLength () / (double)strHungarian.GetLength () * 100.0),
					};

					bool bSkip = false;

					// 110 % wider than spanish
					bSkip = ((int)((double)nStretch [0] * 1.10) >= nStretch [1]); // TODO: rem

					if (!bSkip) {
						DUPLICATESTRUCT s;

						s.m_strEnglish		= strEnglish;
						s.m_strSpanish		= strSpanish;
						s.m_strPortuguese	= strPortuguese;
						s.m_strDutch		= strDutch;
						s.m_strRussian		= strRussian;
						s.m_strHungarian	= strHungarian;
						s.m_lRecordNumber	= lRecord;

						InsertAsc (vStretch, s);
					}
				}
			}

			vTranslated.Add (t);

			lRecord++;
			rst.MoveNext ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	try {
		COdbcRecordset rst (theApp.m_db);
		CString str;

		theApp.m_db.ExecuteSQL (_T ("DELETE * FROM Stretched"));
		rst.Open (_T ("SELECT * FROM Stretched"), CRecordset::dynaset);

		for (int i = 0; i < vStretch.GetSize (); i++) {
			DUPLICATESTRUCT & s = vStretch [i];
			CString strSpanish, strPortuguese, strDutch, strRussian, strHungarian;

			if (IsStretched (s.m_strEnglish, s.m_strSpanish))
				strSpanish.Format	(_T ("[%d %%] %s"), (int)((double)s.m_strSpanish.GetLength () / (double)s.m_strEnglish.GetLength () * 100.0), s.m_strSpanish);
			if (IsStretched (s.m_strEnglish, s.m_strPortuguese))
				strPortuguese.Format (_T ("[%d %%] %s"), (int)((double)s.m_strPortuguese.GetLength () / (double)s.m_strEnglish.GetLength () * 100.0), s.m_strPortuguese);
			if (IsStretched (s.m_strEnglish, s.m_strDutch))
				strDutch.Format (_T ("[%d %%] %s"), (int)((double)s.m_strDutch.GetLength () / (double)s.m_strEnglish.GetLength () * 100.0), s.m_strDutch);
			if (IsStretched (s.m_strEnglish, s.m_strRussian))
				strRussian.Format (_T ("[%d %%] %s"), (int)((double)s.m_strRussian.GetLength () / (double)s.m_strEnglish.GetLength () * 100.0), s.m_strRussian);
			if (IsStretched (s.m_strEnglish, s.m_strHungarian))
				strHungarian.Format (_T ("[%d %%] %s"), (int)((double)s.m_strHungarian.GetLength () / (double)s.m_strEnglish.GetLength () * 100.0), s.m_strHungarian);

			CStringArray name, value;

			name.Add (_T ("English"));			value.Add (s.m_strEnglish);
			name.Add (_T ("Spanish"));			value.Add (strSpanish);
			name.Add (_T ("Portuguese"));		value.Add (strPortuguese);
			name.Add (_T ("Dutch"));			value.Add (strDutch);
			name.Add (_T ("Russian"));			value.Add (strRussian);
			name.Add (_T ("Hungarian"));		value.Add (strHungarian);
			name.Add (_T ("Record"));			value.Add (ToString ((long)s.m_lRecordNumber));
						
			rst.SetEditMode (COdbcRecordset::addnew);
			CString strSQL = FormatSQL (rst, _T ("Stretched"), name, value, _T (""), 0);
			VERIFY (theApp.m_db.ExecuteSQL (strSQL));

			/* TODO: rem
			rst.AddNew ();
			rst.SetFieldValue (_T ("English"),		s.m_strEnglish);
			rst.SetFieldValue (_T ("Spanish"),		strSpanish);
			rst.SetFieldValue (_T ("Portuguese"),	strPortuguese);
			rst.SetFieldValue (_T ("Dutch"),		strDutch);
			rst.SetFieldValue (_T ("Russian"),		strRussian);
			rst.SetFieldValue (_T ("Hungarian"),	strHungarian);
			rst.SetFieldValue (_T ("Record"),		(long)s.m_lRecordNumber);
			rst.Update ();
			*/
		}

		rst.Close ();
		str.Format (_T ("%d stretched"), vStretch.GetSize ());
		TRACEF (str);
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	for (int i = 0; i < sel.GetSize (); i++) {
		CRcItem * p = (CRcItem *)m_lv.GetCtrlData (sel [i]);
		CStringArray vInput, vOutput;

		CString strInput = p->m_strSource;

		ASSERT (p);

		bool bUnicode = IsUnicode (strInput);
		
		if (bUnicode) {
			CString strTmp = ReadDirect (strInput);

			if (strTmp.GetLength ())
				Tokenize (strTmp, vInput, '\n');
			else
				strError += _T ("Failed to read: ") + strInput + _T ("\n");
		}
		else {
			if (FILE * fp = _tfopen (strInput, _T ("r"))) {
				char sz [256];
				bool bMore = true;

				TRACEF ("Reading: " + strInput);

				do {
					CString str;

					do {
						ZeroMemory (sz, sizeof (sz));
						fgets (sz, sizeof (sz), fp);
						str = a2w (sz);

						if (str.GetLength ())
							bMore = str [str.GetLength () - 1] != '\n';
						else
							bMore = !feof (fp);
					}
					while (bMore);

					vInput.Add (str);
				}
				while (!feof (fp));

				fclose (fp);
			}
			else
				strError += _T ("Failed to read: ") + strInput + _T ("\n");
		}

		TRACEF ("Translating: " + strInput);

		{
			const CString strPathUntranslated	= _T ("..\\\\..\\\\..\\\\..\\\\Installation\\\\");
			const CString strPathTranslated		= _T ("..\\\\..\\\\..\\\\Installation\\\\");
			const CString strOutputFile			= p->m_strEnglish;

			if (FILE * fp = _tfopen (strOutputFile, _T ("wb"))) {
				WORD w = 0xFEFF;

				TRACEF ("Writing: " + strOutputFile);
				fwrite (&w, sizeof (w), 1, fp); // Unicode header

				for (int nLine = 0; nLine < vInput.GetSize (); nLine++) {
					CString str = vInput [nLine];

					if (str.Find (strPathUntranslated) != -1) {
						str.Replace (strPathUntranslated, strPathTranslated);
						vInput [nLine] = str;
						TRACEF (vInput [nLine]);
					}

					fwrite ((LPVOID)(LPCTSTR)str, str.GetLength () * sizeof (TCHAR), 1, fp);
				}
				
				fclose (fp);
			}
			else { ASSERT (0); }
		}

		for (int nLang = 0; nLang < ARRAYSIZE (::types); nLang++)
			nCount += Translate (::types [nLang], vInput, p, vTranslated, strError);
	}
	
	if (strError.GetLength ())
		MessageBox (strError);
	else {
		CString str;

		str.Format (_T ("%d strings translated"), nCount);
		MessageBox (str);
	}

	EnableUI (true);
}

int CParserDlg::Integrate(LANGTYPE type)
{
	int nCount = 0;
	CString strTable = GetLangString (type);

	try {
		int nRecord = 0;
		CString strSQL;
		COdbcRecordset rst (theApp.m_db);

		strSQL.Format (_T ("SELECT * FROM [TODO %s]"), strTable);
		rst.Open (strSQL, CRecordset::dynaset);

		while (!rst.IsEOF ()) {
			COdbcRecordset rstTranslated (theApp.m_db);
			INTEGRATESTRUCT s;
			bool bUpdated = false;

			rst >> s;
			nRecord++;

			strSQL.Format (_T ("UPDATE Translated SET [%s]='%s' WHERE [%s]='%s';"), 
				_T ("English"), FoxjetDatabase::Format (s.m_strEnglish),
				strTable, FoxjetDatabase::Format (s.m_strTranslated));
			rstTranslated.SetEditMode (COdbcRecordset::edit);
			TRACEF (strSQL);

			if (theApp.m_db.ExecuteSQL (strSQL)) {
				nCount++;
				bUpdated = true;
			}

			if (s.m_strEnglish.GetLength ()) {
				/* TODO: rem
				if (!HasQuotes (s.m_strEnglish)) {
					CString strSQL;

					strSQL.Format (_T ("SELECT * FROM Translated WHERE English='%s';"), s.m_strEnglish);
					rstTranslated.Open (strSQL, CRecordset::dynaset);

					if (!rstTranslated.IsEOF ()) {
						rstTranslated.Edit ();
						rstTranslated.SetFieldValue (strTable, s.m_strTranslated);
						rstTranslated.Update ();
						nCount++;
						bUpdated = true;
					}
					else {
						CString str;

						str.Format (_T ("not found [%d]: \"%s\""), nRecord, s.m_strEnglish); 
						TRACEF (str);
						ASSERT (0);
					}
				}
				else {
					rstTranslated.Open (_T ("SELECT * FROM Translated"), CRecordset::dynaset);

					while (!rstTranslated.IsEOF ()) {
						CString str = rstTranslated.GetFieldValue (_T ("English"));

						if (!str.CompareNoCase (s.m_strEnglish)) {
							rstTranslated.Edit ();
							rstTranslated.SetFieldValue (strTable, s.m_strTranslated);
							rstTranslated.Update ();
							nCount++;
							bUpdated = true;
							break;
						}

						rstTranslated.MoveNext ();
					}
				}
				*/

				if (!bUpdated) {
					CStringArray name, value;

					name.Add (_T ("English"));			value.Add (s.m_strEnglish);
					name.Add (strTable);				value.Add (s.m_strTranslated);
						
					rstTranslated.SetEditMode (COdbcRecordset::addnew);
					CString strSQL = FormatSQL (rstTranslated, _T ("Translated"), name, value, _T (""), 0);
					VERIFY (theApp.m_db.ExecuteSQL (strSQL));

					/* TODO: rem
					if (type != LANGTYPE_PORTUGUESE) {
						rstTranslated.AddNew ();
						rstTranslated.SetFieldValue (_T ("English"), s.m_strEnglish);
						rstTranslated.SetFieldValue (strTable, s.m_strTranslated);
						rstTranslated.Update ();
						nCount++;
						bUpdated = true;
					}
					*/
				}
			}

			rstTranslated.Close ();

			if (bUpdated) {
				rst.Delete ();
			}
			else 
				TRACEF (s.m_strEnglish);

			rst.MoveNext ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	CString str;
	str.Format (_T ("%s: %d"), strTable, nCount);
	TRACEF (str);

	return nCount;
}
void CParserDlg::OnIntegrate() 
{
	CString str;

	if (FindDuplicates ()) {
		MessageBox (_T ("Duplicate entries found.\nsee [Duplicates]"), NULL, MB_ICONERROR);

		return;
	}

	BeginWaitCursor ();

	int nCount = 0;

	for (int i = 0; i < ARRAYSIZE (::types); i++)
		nCount += Integrate (::types [i]);
	
	str.Format (_T ("%d strings integrated"), nCount);
	MessageBox (str);

	EndWaitCursor ();
}

void CParserDlg::Compact ()
{
	CString strSrc = FoxjetDatabase::Extract (theApp.m_db.GetConnect (), _T ("DBQ="), _T (";"));

	theApp.m_db.Close ();

	if (strSrc.GetLength ()) {
		TCHAR szTitle [MAX_PATH] = { 0 };
		CString strDest = strSrc;
		CString strExt = _T (".") + FoxjetFile::GetFileExt (strSrc);

		::GetFileTitle (strSrc, szTitle, ARRAYSIZE (szTitle) - 1);
		CString strTitle (szTitle);
		
		if (strTitle.Find (strExt) == -1)
			strTitle += strExt;

		strDest.Replace (strTitle, _T (""));
		strTitle.Replace (strExt, _T (""));
		strExt.Replace (_T ("."), _T (""));
		strDest = strDest + strTitle + _T ("_000.") + strExt;

		try 
		{
			ASSERT (!theApp.m_db.IsOpen ());
			::DeleteFile (strDest);												
			CDaoWorkspace::CompactDatabase (strSrc, strDest);					
			VERIFY (::CopyFile (strDest, strSrc, FALSE));						
			VERIFY (::DeleteFile (strDest));									
		}
		catch (CDaoException * e)		{ HANDLEEXCEPTION (e); }
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	try {
		theApp.m_db.OpenEx (_T ("DSN=Parser"));
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
}


void CParserDlg::OnCompact() 
{
	BeginWaitCursor ();
	Compact ();	
	EndWaitCursor ();
}


////////////////////////////////////////////////////////////////////////////////

static int FindNext (const CString & strFind, CArray <DUPLICATESTRUCT, DUPLICATESTRUCT &> & v, int nStart)
{
	for (int i = nStart + 1; i < v.GetSize (); i++) 
		if (!strFind.Compare (v [i].m_strEnglish))
			return i;

	return -1;
}

static void InsertAsc (CArray <DUPLICATESTRUCT, DUPLICATESTRUCT &> & v, DUPLICATESTRUCT & s)
{
	for (int i = 0; i < v.GetSize (); i++) {
		DUPLICATESTRUCT & tmp = v [i];

		if (tmp.m_strEnglish.CompareNoCase (s.m_strEnglish) > 0) {
			v.InsertAt (i, s);
			return;
		}
	}

	v.Add (s);
}

static void InsertAsc (CLongArray & v, ULONG l)
{
	for (int i = 0; i < v.GetSize (); i++) {
		ULONG lCompare = v [i];

		if (lCompare == l)
			return;

		if (lCompare > l) {
			v.InsertAt (i, l);
			return;
		}
	}

	v.Add (l);
}

////////////////////////////////////////////////////////////////////////////////

bool CParserDlg::FindDuplicates ()
{
	CArray <DUPLICATESTRUCT, DUPLICATESTRUCT &> vTranslated;
	CArray <DUPLICATESTRUCT, DUPLICATESTRUCT &> vDuplicates;
	ULONG lOrdinal = 0;

	CString strSrc = "Translated";

//strSrc = "tmp"; // TODO: rem

	BeginWaitCursor ();

	try {
		{
			COdbcRecordset rstTranslated (theApp.m_db);
			COdbcRecordset rstDuplicates (theApp.m_db);
			ULONG lRecord = 1;

			theApp.m_db.ExecuteSQL (_T ("DELETE * FROM [Duplicates];"));

			rstDuplicates.Open (_T ("SELECT * FROM [Duplicates];"), CRecordset::dynaset);
			rstTranslated.Open (_T ("SELECT * FROM [") + strSrc + _T ("]"));

			while (!rstTranslated.IsEOF ()) {
				DUPLICATESTRUCT s;

				s.m_strEnglish		= rstTranslated.GetFieldValue (_T ("English"));
				s.m_strSpanish		= rstTranslated.GetFieldValue (_T ("Spanish"));
				s.m_strPortuguese	= rstTranslated.GetFieldValue (_T ("Portuguese"));
				s.m_strDutch		= rstTranslated.GetFieldValue (_T ("Dutch"));
				s.m_strRussian		= rstTranslated.GetFieldValue (_T ("Russian"));
				s.m_strHungarian	= rstTranslated.GetFieldValue (_T ("Hungarian"));
				s.m_lRecordNumber	= lRecord++;

				InsertAsc (vTranslated, s);
				rstTranslated.MoveNext ();
			}

			for (int i = 0; i < vTranslated.GetSize (); i++) {
				DUPLICATESTRUCT & a = vTranslated [i];

				if ((i + 1) < vTranslated.GetSize ()) {
					DUPLICATESTRUCT & b = vTranslated [i + 1];

					if (!a.m_strEnglish.CompareNoCase (b.m_strEnglish)) {
						DUPLICATESTRUCT tmp = a;

						if (tmp.m_strSpanish.CompareNoCase (b.m_strSpanish) || 
							tmp.m_strPortuguese.CompareNoCase (b.m_strPortuguese) ||
							tmp.m_strDutch.CompareNoCase (b.m_strDutch) ||
							tmp.m_strRussian.CompareNoCase (b.m_strRussian) || 
							tmp.m_strHungarian.CompareNoCase (b.m_strHungarian))
						{
							CString str;

							str.Format (
								_T ("\n\t[%04d] %s: %s [%s] [%s] [%s] [%s]")
								_T ("\n\t[%04d] %s: %s [%s] [%s] [%s] [%s]"), 
								a.m_lRecordNumber, a.m_strEnglish, a.m_strPortuguese, a.m_strSpanish, a.m_strDutch, a.m_strRussian, a.m_strHungarian,
								b.m_lRecordNumber, b.m_strEnglish, b.m_strPortuguese, b.m_strSpanish, b.m_strDutch, b.m_strRussian, b.m_strHungarian);

							TRACEF (str);

							{
								CStringArray name, value;

								name.Add (_T ("English"));				value.Add (a.m_strEnglish);
								name.Add (_T ("Spanish"));				value.Add (a.m_strSpanish);
								name.Add (_T ("Portuguese"));			value.Add (a.m_strPortuguese);
								name.Add (_T ("Dutch"));				value.Add (a.m_strDutch);
								name.Add (_T ("Russian"));				value.Add (a.m_strRussian);
								name.Add (_T ("Hungarian"));			value.Add (a.m_strHungarian);
								name.Add (_T ("Record number"));		value.Add (ToString ((long)a.m_lRecordNumber));
								name.Add (_T ("Duplicate"));			value.Add (ToString ((long)b.m_lRecordNumber));
								name.Add (_T ("Ordinal"));				value.Add (ToString ((long)lOrdinal++));
						
								rstDuplicates.SetEditMode (COdbcRecordset::addnew);
								CString strSQL = FormatSQL (rstDuplicates, _T ("Duplicates"), name, value, _T (""), 0);
								VERIFY (theApp.m_db.ExecuteSQL (strSQL));
							}
							/* TODO: rem
							rstDuplicates.AddNew ();
							rstDuplicates.SetFieldValue (_T ("English"),		a.m_strEnglish);
							rstDuplicates.SetFieldValue (_T ("Spanish"),		a.m_strSpanish);
							rstDuplicates.SetFieldValue (_T ("Portuguese"),		a.m_strPortuguese);
							rstDuplicates.SetFieldValue (_T ("Dutch"),			a.m_strDutch);
							rstDuplicates.SetFieldValue (_T ("Russian"),		a.m_strRussian);
							rstDuplicates.SetFieldValue (_T ("Hungarian"),		a.m_strHungarian);
							rstDuplicates.SetFieldValue (_T ("Record number"),	(long)a.m_lRecordNumber);
							rstDuplicates.SetFieldValue (_T ("Duplicate"),		(long)b.m_lRecordNumber);
							rstDuplicates.SetFieldValue (_T ("Ordinal"),		(long)lOrdinal++);
							rstDuplicates.Update ();
							*/

							{
								CStringArray name, value;

								name.Add (_T ("English"));				value.Add (b.m_strEnglish);
								name.Add (_T ("Spanish"));				value.Add (b.m_strSpanish);
								name.Add (_T ("Portuguese"));			value.Add (b.m_strPortuguese);
								name.Add (_T ("Dutch"));				value.Add (b.m_strDutch);
								name.Add (_T ("Russian"));				value.Add (b.m_strRussian);
								name.Add (_T ("Hungarian"));			value.Add (b.m_strHungarian);
								name.Add (_T ("Record number"));		value.Add (ToString ((long)b.m_lRecordNumber));
								name.Add (_T ("Duplicate"));			value.Add (ToString ((long)a.m_lRecordNumber));
								name.Add (_T ("Ordinal"));				value.Add (ToString ((long)lOrdinal++));
						
								rstDuplicates.SetEditMode (COdbcRecordset::addnew);
								CString strSQL = FormatSQL (rstDuplicates, _T ("Duplicates"), name, value, _T (""), 0);
								VERIFY (theApp.m_db.ExecuteSQL (strSQL));
							}
							/* TODO: rem
							rstDuplicates.AddNew ();
							rstDuplicates.SetFieldValue (_T ("English"),		b.m_strEnglish);
							rstDuplicates.SetFieldValue (_T ("Spanish"),		b.m_strSpanish);
							rstDuplicates.SetFieldValue (_T ("Portuguese"),		b.m_strPortuguese);
							rstDuplicates.SetFieldValue (_T ("Dutch"),			b.m_strDutch);
							rstDuplicates.SetFieldValue (_T ("Russian"),		b.m_strRussian);
							rstDuplicates.SetFieldValue (_T ("Hungarian"),		b.m_strHungarian);
							rstDuplicates.SetFieldValue (_T ("Record number"),	(long)b.m_lRecordNumber);
							rstDuplicates.SetFieldValue (_T ("Duplicate"),		(long)a.m_lRecordNumber);
							rstDuplicates.SetFieldValue (_T ("Ordinal"),		(long)lOrdinal++);
							rstDuplicates.Update ();
							*/
						}
					}
				}
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	EndWaitCursor ();

	return lOrdinal ? true : false;
}

static CString BuildLangString (LANGTYPE type, const CString & strData)
{
	CString str;

	switch (type) {
	case LANGTYPE_SPANISH:
		str.Format (_T ("text=%s&langpair=en%%7ces"), URLEncode (strData));
		break;
	case LANGTYPE_PORTUGUESE:
		str.Format (_T ("text=%s&langpair=en%%7cpt"), URLEncode (strData));
		break;
	case LANGTYPE_DUTCH:
		str.Format (_T ("text=%s&langpair=en%%7cnl"), URLEncode (strData));
		break;
	case LANGTYPE_RUSSIAN:
		str.Format (_T ("text=%s&langpair=en%%7cru"), URLEncode (strData));
		break;
	default:
		ASSERT (0);
		break;
	}

	return str;
}

static CString GoogleTranslate (CInternetSession & session, LANGTYPE type, const CString & str)
{
	CString strResult;

	try	{
		CString strURL = _T ("http://translate.google.com/translate_t");
		CString strData = BuildLangString (type, str);//_T ("text=This%20is%20a%20test&langpair=en%7ces");
		CString strHeaders = _T ("Content-Type: application/x-www-form-urlencoded");

		//strHeaders += _T ("Accept: text/*\r\n");
		strHeaders += _T ("Accept: text/xml,application/xml,application/xhtml+xml,text/html; q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5\r\n");
		strHeaders += _T ("User-Agent: HttpCall\r\n");
		strHeaders += _T ("Accept-Language: en-us, zh;\r\n");
		strHeaders += _T ("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\n");

		//strData = _T ("This%20is%20a%20test&langpair=en%7ces%23en%7czh%2dTW%7cThis%20is%20a%20test"); // chinese
		//strData = _T ("text=This%20is%20a%20test&langpair=en%7cnl"); // dutch
		//strData = _T ("text=This%20is%20a%20test&langpair=en%7cru"); // russian

		strURL += _T ("?") + strData;
		//TRACEF (strURL);
			
		if (CStdioFile * pFile = session.OpenURL (strURL, 1, INTERNET_FLAG_TRANSFER_ASCII, strHeaders)) {
			char szBuff [4096] = { 0 };
			CString str;

			while (pFile->Read (szBuff, ARRAYSIZE (szBuff) - 1) > 0) {
				//str += a2w (szBuff);
				str += szBuff;
				ZeroMemory (szBuff, ARRAYSIZE (szBuff));
			}

			if (str.Find (_T ("403 Forbidden"))) {
				TRACEF (str);
				ASSERT (0);
			}

			CString strTranslated = FoxjetDatabase::Extract (str, _T ("name=gtrans value=\""), _T ("\">"));

			if (strTranslated.GetLength ()) {
				strResult = URLDecode (strTranslated);
				//TRACEF (strTranslated);
			}
			else {
				strTranslated = FoxjetDatabase::Extract (str, _T ("<div id=result_box dir=\"ltr\">"), _T ("</div>"));

				if (strTranslated.GetLength ()) {
					strResult = URLDecode (strTranslated);
					//TRACEF (strTranslated);
				}
			}

			/*
			if (FILE * fp = _tfopen (_T ("C:\\Temp\\Debug\\google.html"), _T ("w"))) {
				fwrite ((LPVOID)w2a (str), str.GetLength (), 1, fp);
				fclose (fp);
			}

			if (FILE * fp = _tfopen (_T ("C:\\Temp\\Debug\\translated.txt"), _T ("w"))) {
				WORD w = 0xFEFF;

				fwrite (&w, sizeof (w), 1, fp); // Unicode header
				fwrite ((LPVOID)(LPCTSTR)strTranslated, strTranslated.GetLength () * sizeof (TCHAR), 1, fp);
				fclose (fp);
			}
			*/

			pFile->Close ();
			delete pFile;
		}
	}
	catch (CInternetException* e)  { HANDLEEXCEPTION (e); }

	return strResult;
}

void CParserDlg::OnGoogle() 
{
	BeginWaitCursor ();
	int nCount = 0;

	try {
		CStringArray v;
		CInternetSession session (_T ("Foxjet Translator"));

		{ // if using google to translate a spreadsheet, need to remove original string from translation
			int nUpdated = 0;
			COdbcRecordset rstTranslated (theApp.m_db);

			rstTranslated.Open (_T ("SELECT * FROM Translated"), CRecordset::dynaset);

			while (!rstTranslated.IsEOF ()) {
				const CString strEnglish = rstTranslated.GetFieldValue (_T ("English")) + CString (_T (" "));
				const int nLen = strEnglish.GetLength ();

				if (strEnglish.Find (_T ("Display program information")) != -1)
					int nBreak = 0;

				for (int nType = LANGTYPE_SPANISH; nType <= LANGTYPE_LAST; nType++) {
					const CString strLang = GetLangString ((LANGTYPE)nType);
					const CString strTranslated = rstTranslated.GetFieldValue (strLang);

					if (strTranslated.GetLength () > nLen) {
						CString strTmp = strTranslated.Left (nLen);

						if (strTmp == strEnglish) {
							CString strUpdated = strTranslated.Mid (nLen);

							if (strUpdated.GetLength ()) {
								TRACEF (strTranslated + _T (" --> ") + strUpdated);
								rstTranslated.Edit ();
								rstTranslated.SetFieldValue (strLang, strUpdated);
								rstTranslated.Update ();
								nUpdated++;
							}
							else { ASSERT (0); }
						}
					}
				}

				rstTranslated.MoveNext ();
			}

			CString str;
			
			str.Format (_T ("%d strings updated internally\nContinue with google translate?"), nUpdated);
			
			if (MessageBox (str, NULL, MB_YESNO) == IDNO)
				return;
		}

		{
			for (int nType = LANGTYPE_SPANISH; nType <= LANGTYPE_LAST; nType++) {
				bool bMore = true;
				int nGoogle = 0;

				do {
					COdbcRecordset rstTranslated (theApp.m_db);
					CString strSQL, strLang = GetLangString ((LANGTYPE)nType);

					strSQL.Format (_T ("SELECT * FROM Translated WHERE [%s] Is Null;"), strLang);
					TRACEF (strSQL);
					rstTranslated.Open (strSQL, CRecordset::dynaset);

					while (bMore = !rstTranslated.IsEOF ()) {
						CString strEnglish = rstTranslated.GetFieldValue (_T ("English"));

						if (strEnglish.GetLength ()) {
							CString strTranslated = GoogleTranslate (session, (LANGTYPE)nType, strEnglish);

							nGoogle++;

							if (strTranslated.GetLength ()) {
								TRACEF (strEnglish + _T (" --> ") + strTranslated);
								rstTranslated.Edit ();
								rstTranslated.SetFieldValue (strLang, strTranslated);
								rstTranslated.Update ();
								nCount++;
							}
						}

						if (nGoogle && ((nGoogle % 30) == 0)) 
							break;

						rstTranslated.MoveNext ();
					}

					if (nGoogle && ((nGoogle % 30) == 0)) {
						rstTranslated.Close ();
						::Sleep (3000);
						continue;
					}
				}
				while (bMore);
			}
		}

		{ // pick off pending items marked to be excluded
			COdbcRecordset rstPending (theApp.m_db);

			rstPending.Open (_T ("SELECT * FROM [Pending];"), CRecordset::snapshot);

			while (!rstPending.IsEOF ()) {
				bool bExclude = (bool)rstPending.GetFieldValue (_T ("Exclude"));

				if (!bExclude) {
					CString str = rstPending.GetFieldValue (_T ("English"));

					if (Find (str, v) == -1) 
						v.Add (str);
				}

				rstPending.MoveNext ();
			}

			rstPending.Close ();

		}

		for (int nPending = 0; nPending < v.GetSize (); nPending++) {
			CString strEnglish = v [nPending];
			bool bFailed = false;
			bool bUpdated = false;

			if (!CParserDlg::Translated (strEnglish)) {
				COdbcRecordset rstTranslated (theApp.m_db);

				rstTranslated.Open (_T ("SELECT * FROM Translated"), CRecordset::dynaset);
				rstTranslated.AddNew ();
				rstTranslated.SetFieldValue (_T ("English"), strEnglish);
				rstTranslated.Update ();
				rstTranslated.Close ();
			}

			for (int nType = 0; nType < ARRAYSIZE (::types); nType++) {
				LANGTYPE type = ::types [nType];
				CString strTable = GetLangString (type);
				CString strTranslated = GoogleTranslate (session, type, strEnglish);

				bFailed &= strTranslated.IsEmpty () ? true : false;
				//TRACE (_T ("\t") + strTranslated + _T ("\n"));

				if (strTranslated.GetLength ()) {
					COdbcRecordset rstTranslated (theApp.m_db);
		
					if (!HasQuotes (strEnglish)) {
						CString strSQL;

						strSQL.Format (_T ("SELECT * FROM Translated WHERE English='%s';"), strEnglish);
						rstTranslated.Open (strSQL, CRecordset::dynaset);

						if (!rstTranslated.IsEOF ()) {
							rstTranslated.Edit ();
							rstTranslated.SetFieldValue (strTable, strTranslated);
							rstTranslated.Update ();
							bUpdated = true;
						}
						else { CString str;  str.Format (_T ("not found: \"%s\""), strEnglish);  TRACEF (str); ASSERT (0); }
					}
					else {
						rstTranslated.Open (_T ("SELECT * FROM Translated"), CRecordset::dynaset);

						while (!rstTranslated.IsEOF ()) {
							CString str = rstTranslated.GetFieldValue (_T ("English"));

							if (!str.CompareNoCase (strEnglish)) {
								rstTranslated.Edit ();
								rstTranslated.SetFieldValue (strTable, strTranslated);
								rstTranslated.Update ();
								bUpdated = true;
								break;
							}

							rstTranslated.MoveNext ();
						}
					}
				}
			}

			if (!bUpdated) { CString str;  str.Format (_T ("not found: \"%s\""), strEnglish);  TRACEF (str); ASSERT (0); }

			if (!bFailed && bUpdated) {
				CString strSQL;

				if (!HasQuotes (strEnglish)) {
					strSQL.Format (_T ("DELETE * FROM [Pending] WHERE English='%s';"), strEnglish);
					theApp.m_db.ExecuteSQL (strSQL);
					nCount++;
				}
				else {
					COdbcRecordset rstPending (theApp.m_db);

					rstPending.Open (_T ("SELECT * FROM [Pending];"), CRecordset::dynaset);

					while (!rstPending.IsEOF ()) {
						CString str = rstPending.GetFieldValue (_T ("English"));

						if (!str.CompareNoCase (strEnglish)) {
							rstPending.Edit ();
							rstPending.Delete ();
							rstPending.Update ();
							break;
						}

						rstPending.MoveNext ();
					}

					rstPending.Close ();
				}
			}
		}
		
		session.Close();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	{ 
		CString str;
		
		str.Format (_T ("%d strings translated"), nCount);
		MessageBox (str);
	}

	EndWaitCursor ();
}

typedef struct
{
	CString m_strTranslatedRaw;
	CString m_strTranslatedFormatted;
	CString m_strEnglishFormatted;
	CString m_strEnglishRaw;
} GOOGLEPARSESTRUCT;

void CParserDlg::OnFileParseHtml() 
{
	const CString strLang = _T ("Russian");
	CString strFile = theApp.GetProfileString (_T ("Settings"), _T ("google html input"), 
		_T ("C:\\Dev\\BoxWriter\\Language\\Parser\\Input\\Pending\\") + strLang + _T (".html"));

	CFileDialog dlg (TRUE, _T ("*.*"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY,
		_T ("All files (*.*)|*.*||"), this);
	
	if (dlg.DoModal () == IDOK) {
		CString str;
		CArray <GOOGLEPARSESTRUCT, GOOGLEPARSESTRUCT &> v;

		strFile = dlg.GetPathName ();
		theApp.WriteProfileString (_T ("Settings"), _T ("google html input"), strFile);

		EnableUI (false);
		BeginWaitCursor ();

		{
			CStdioFile f;
			CString strTmp;

			if (f.Open (strFile, CFile::modeRead)) {
				while (f.ReadString (strTmp))
					str += strTmp;
			}
			else {
				MessageBox (_T ("Failed to open: ") + strFile);
				return;
			}
		}

		int nCount = 0;//, nFailed = 0;

		CString strTableBegin = _T ("<TR><TD>");
		CString strTableEnd = _T ("</TD></TR>");
		int nTableBegin = str.Find (strTableBegin);

		if (str.Find (strTableBegin) == -1)
			strTableBegin = _T ("<tr><td>");

		ASSERT (str.Find (strTableBegin) != -1);

		if (str.Find (strTableEnd) == -1)
			strTableEnd = _T ("</td></tr>");

		ASSERT (str.Find (strTableEnd) != -1);

		while ((nTableBegin = str.Find (strTableBegin, nTableBegin)) != -1) {
			int nTableEnd = str.Find (strTableEnd, nTableBegin + strTableBegin.GetLength ());
			
			if (nTableEnd != -1) {
				const CString strTable = str.Mid (nTableBegin, nTableEnd - nTableBegin + strTableEnd.GetLength ());
				const CString strBegin = _T ("<span class=\"google-src-text\" style=\"direction: ltr; text-align: left;\">");
				const CString strEnd = _T ("</span>");
				int nBegin = strTable.Find (strBegin);
				CString strRowEnglish, strRowTranslated, strRowRaw;
				bool bParsed = false;

				//if (strTable.Find (strBegin) == -1)
				//	TRACEF (_T ("Failed to find: ") + strBegin + _T ("\n\t") + strTable);
				//else if (strTable.Find (strEnd) == -1)
				//	TRACEF (_T ("Failed to find: ") + strEnd + _T ("\n\t") + strTable);

				while ((nBegin = strTable.Find (strBegin, nBegin)) != -1) {
					int nEnd = strTable.Find (strEnd, nBegin + strBegin.GetLength ());

					if (nEnd != -1) {
						int nEnglishBegin		= nBegin + strBegin.GetLength ();
						int nEnglishLen			= (nEnd - nBegin) - strBegin.GetLength ();
						CString	strEnglish		= UnformatGoogle (strTable.Mid (nEnglishBegin, nEnglishLen));
						CString strRaw			= FoxjetDatabase::Extract (strTable, strEnglish + strEnd, strEnd); 
						CString strFormatted	= UnformatGoogle (strRaw);
						CString	strSpan			= strTable.Mid (nBegin, (nEnd - nBegin) + strEnd.GetLength ());

						strRowEnglish		+= _T ("  ") + strEnglish;
						strRowRaw			+= _T ("  ") + strRaw;
						strRowTranslated	+= _T ("  ") + strFormatted;
						bParsed = true;
					}

					nBegin += strBegin.GetLength ();
				}

				if (!bParsed) { // assume no translation was possible
					//TRACEF (_T ("Untranslatable: ") + strTable);

					strRowEnglish = FoxjetDatabase::Extract (strTable, strTableBegin, strTableEnd);
					strRowEnglish.TrimLeft (); 
					strRowEnglish.TrimRight ();
					strRowRaw = strRowTranslated = strRowEnglish;
				}

				{
					GOOGLEPARSESTRUCT parsed;

					parsed.m_strEnglishFormatted	= strRowEnglish;
					parsed.m_strEnglishRaw			= strTable;
					parsed.m_strTranslatedRaw		= strRowRaw;
					parsed.m_strTranslatedFormatted = strRowTranslated;

					parsed.m_strEnglishFormatted.TrimLeft ();
					parsed.m_strEnglishFormatted.TrimRight ();
					parsed.m_strTranslatedFormatted.TrimLeft ();
					parsed.m_strTranslatedFormatted.TrimRight ();

					parsed.m_strTranslatedFormatted = /* MultiByteToCString */ (parsed.m_strTranslatedFormatted);

					v.Add (parsed);
				}
			}

			nTableBegin += strTableBegin.GetLength ();
		}

		/*
		try {
			//theApp.m_db.ExecuteSQL (_T ("UPDATE Translated SET Translated.") + strLang + _T (" = Null;"));

			COdbcRecordset rstTranslated (theApp.m_db);
			CString strSQL = _T ("SELECT * FROM [Translated] WHERE [") + strLang + _T ("] Is Null");

			rstTranslated.Open (strSQL, CRecordset::dynaset);

			for ( ; !rstTranslated.IsEOF (); rstTranslated.MoveNext ()) {
				const CString strRecord = rstTranslated.GetFieldValue (_T ("English"));

				for (int i = v.GetSize () - 1; i >= 0; i--) {
					GOOGLEPARSESTRUCT & p = v [i];

					if (!p.m_strEnglishFormatted.Compare (strRecord)) {
						if (p.m_strTranslatedFormatted.GetLength ()) {
							rstTranslated.Edit ();
							rstTranslated.SetFieldValue (strLang, p.m_strTranslatedFormatted);
							rstTranslated.Update ();
							nCount++;
							v.RemoveAt (i);
							CDebug::Trace (strRecord, false, _T (__FILE__), __LINE__); 
								::OutputDebugString (_T (" --> ")); 
								::OutputDebugString (p.m_strTranslatedRaw); 
								::OutputDebugString (_T (" --> ")); 
								::OutputDebugString (p.m_strTranslatedFormatted); 
								::OutputDebugString (_T ("\n"));
						}
					}
				}
			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		int nRemaning = 0;

		try {
			COdbcRecordset rstTranslated (theApp.m_db);
			CString strSQL = _T ("SELECT * FROM [Translated] WHERE [") + strLang + _T ("] Is Null");

			rstTranslated.Open (strSQL, CRecordset::dynaset);

			for ( ; !rstTranslated.IsEOF (); rstTranslated.MoveNext ()) {
				CString strEnglish = rstTranslated.GetFieldValue (_T ("English"));
				
				TRACEF (strEnglish);
				nRemaning++;
			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		{
			CString str;

			str.Format (_T ("%d records updated\n%d remaining"), nCount, nRemaning);
			MessageBox (str);
		}
		*/

		/*
		try {
			COdbcRecordset rstTranslated (theApp.m_db);
			CString strSQL = _T ("SELECT * FROM [Translated]");

			theApp.m_db.ExecuteSQL (_T ("UPDATE Translated SET Translated.") + strLang + _T (" = Null;"));

			rstTranslated.Open (strSQL, CRecordset::dynaset);

			// NOTE: input/output html files must match for this to work

			for (int i = 0; i < v.GetSize () && !rstTranslated.IsEOF (); i++, rstTranslated.MoveNext ()) {
				GOOGLEPARSESTRUCT & p = v [i];
				
				rstTranslated.Edit ();
				rstTranslated.SetFieldValue (strLang, p.m_strTranslatedFormatted);
				rstTranslated.Update ();
			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
		*/

		CStdioFile fOut;

		strFile.Replace (dlg.GetFileTitle (), dlg.GetFileTitle () + _T ("_2"));

		if (fOut.Open (strFile, CFile::modeWrite | CFile::modeCreate)) {

/*
<tr><td> 
<span onmouseover="_tipon(this)" onmouseout="_tipoff()">
<span class="google-src-text" style="direction: ltr; text-align: left;">Invalid class of service</span>
 Неверный класс обслуживания</span>
 </td></tr>
*/

			CString strHeaderKey [] = { _T ("<html><head>"), _T ("<table border=\"1\">") };
			CString strFooterKey = _T ("</table>");
			CString strHeader = strHeaderKey [0] + FoxjetDatabase::Extract (str, strHeaderKey [0], strHeaderKey [1]) + strHeaderKey [1] + _T ("\n\n");
			CString strFooter = strFooterKey + FoxjetDatabase::Extract (str, strFooterKey);

			strHeader.Replace (_T (">"), _T (">\n"));
			strFooter.Replace (_T (">"), _T (">\n"));
			strHeader += _T ("\n<FONT FACE=\"Times New Roman CYR\" SIZE=3>\n");
			strFooter += _T ("\n</FONT>\n");

			TRACEF (strHeader);
			TRACEF (strFooter);

			fOut.WriteString (strHeader);

			for (int i = 0; i < v.GetSize (); i++) {
				GOOGLEPARSESTRUCT & p = v [i];
				CString strTranslated;
				
				strTranslated.Format (_T ("<tr><td>%s</td><td>%s</td></tr>\n"), 
					p.m_strEnglishFormatted, p.m_strTranslatedFormatted);
				fOut.WriteString (strTranslated);
			}

			fOut.WriteString (strFooter);
			fOut.Close ();

			MessageBox (_T ("File written to: ") + strFile);
		}
		else
			MessageBox (_T ("Failed to write: ") + strFile);


		EndWaitCursor ();
		EnableUI (true);
	}
}

void CParserDlg::OnFileGeneratehtmltable() 
{
	CString strFile = theApp.GetProfileString (_T ("Settings"), _T ("html table"), _T ("C:\\Dev\\BoxWriter\\Language\\Parser\\Input\\Pending\\English.html"));

	CFileDialog dlg (FALSE, _T ("*.*"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		_T ("All files (*.*)|*.*||"), this);
	
	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();
		theApp.WriteProfileString (_T ("Settings"), _T ("html table"), strFile);

		if (FILE * fp = _tfopen (strFile, _T ("w"))) {
			CString str;

			BeginWaitCursor ();

			//TRACEF (FormatGoogle (_T ("&a&b&c & d&e&f")));

			try
			{
				CString strTable [] = 
				{
					_T ("SELECT * FROM Translated"),
					_T ("SELECT * FROM Pending")
				};

				str = _T ("<HTML><HEAD> </HEAD><BODY><TABLE BORDER>\n"); 
				fwrite ((LPVOID)w2a (str), str.GetLength (), 1, fp);

				for (int nTable = 0; nTable < ARRAYSIZE (strTable); nTable++) {
					CString strSQL = strTable [nTable];
					COdbcRecordset rst (theApp.m_db);

					rst.Open (strSQL);
					
					if (!rst.IsEOF ()) {
						str = _T ("\n\n");
						fwrite ((LPVOID)w2a (str), str.GetLength (), 1, fp);
					}

					while (!rst.IsEOF ()) {
						CString strEnglish = FormatGoogle (rst.GetFieldValue (_T ("English")));

						str.Format (_T ("<TR><TD>%s</TD></TR>\n"), strEnglish);
						fwrite ((LPVOID)w2a (str), str.GetLength (), 1, fp);
						rst.MoveNext ();
					}
				}

				str = _T ("</TABLE></BODY></HTML>\n"); 
				fwrite ((LPVOID)w2a (str), str.GetLength (), 1, fp);
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

			fclose (fp);
			EndWaitCursor ();
		}
		else
			MessageBox (_T ("Failed to open: ") + strFile);
	}
}




typedef struct 
{
	CString	m_strEnglish;
	CString m_strOriginal;
	CString m_strNew;
} NEWSTRUCT;


void CParserDlg::OnFileImportsunnyvalestable() 
{
	CArray <NEWSTRUCT, NEWSTRUCT &> vSpanish;
	CArray <NEWSTRUCT, NEWSTRUCT &> vPortuguese;
	COdbcDatabase db (_T (__FILE__), __LINE__);
	COdbcRecordset rstSpanish (&theApp.m_db);
	COdbcRecordset rstPortuguese (&theApp.m_db);
	int nRecord = 0;
	bool bExtract = false;

	if (bExtract) {
		theApp.m_db.ExecuteSQL (_T ("DELETE * FROM Sunnyvale_Spanish"));
		theApp.m_db.ExecuteSQL (_T ("DELETE * FROM Sunnyvale_Portuguese"));
	}

	rstSpanish.Open (_T ("SELECT * FROM Sunnyvale_Spanish"), CRecordset::dynaset);
	rstPortuguese.Open (_T ("SELECT * FROM Sunnyvale_Portuguese"), CRecordset::dynaset);

	db.Open (_T ("tmp"));

	if (bExtract) { 
		COdbcRecordset rst (&db);

		rst.Open (_T ("SELECT * FROM Translated;"));

		while (!rst.IsEOF ()) {
			TRANSLATEDSTRUCT t;
			CString strEnglish		= (CString)rst.GetFieldValue ((short)0);
			CString strSpanish		= (CString)rst.GetFieldValue ((short)1);
			CString strPortuguese	= (CString)rst.GetFieldValue ((short)2);

			//LANGTYPE_ENGLISH
			//LANGTYPE_SPANISH
			//LANGTYPE_PORTUGUESE
			
			if (!RecordExists (theApp.m_db, LANGTYPE_SPANISH, strSpanish, t)) {
				NEWSTRUCT n;

				VERIFY (RecordExists (theApp.m_db, LANGTYPE_ENGLISH, strEnglish, t));
				n.m_strEnglish = strEnglish;
				n.m_strOriginal = t.m_strSpanish;
				n.m_strNew = strSpanish;
				vSpanish.Add (n);
			}
			if (!RecordExists (theApp.m_db, LANGTYPE_PORTUGUESE, strPortuguese, t)) {
				NEWSTRUCT n;

				if (t.m_strEnglish.IsEmpty ()) {
					VERIFY (RecordExists (theApp.m_db, LANGTYPE_ENGLISH, strEnglish, t));
				}

				n.m_strEnglish = strEnglish;
				n.m_strOriginal = t.m_strPortuguese;
				n.m_strNew = strPortuguese;
				vPortuguese.Add (n);
			}

			rst.MoveNext ();
			nRecord++;

			//if (nRecord > 10)
			//	break;
		}
	}

	if (!bExtract) {
		while (!rstSpanish.IsEOF ()) {
			NEWSTRUCT n;

			n.m_strEnglish		= (CString)rstSpanish.GetFieldValue ((short)1);
			n.m_strOriginal		= (CString)rstSpanish.GetFieldValue ((short)2);
			n.m_strNew			= (CString)rstSpanish.GetFieldValue ((short)3);
			vSpanish.Add (n);
			rstSpanish.MoveNext ();
		}

		while (!rstPortuguese.IsEOF ()) {
			NEWSTRUCT n;

			n.m_strEnglish		= (CString)rstPortuguese.GetFieldValue ((short)1);
			n.m_strOriginal		= (CString)rstPortuguese.GetFieldValue ((short)2);
			n.m_strNew			= (CString)rstPortuguese.GetFieldValue ((short)3);
			vPortuguese.Add (n);
			rstPortuguese.MoveNext ();
		}
	}
	else {
		TRACEF ("NEW SPANISH:");
		for (int i = 0; i < vSpanish.GetSize (); i++) {
			NEWSTRUCT & n = vSpanish [i];
			double dDiff = (double)n.m_strNew.GetLength () / (double)n.m_strOriginal.GetLength ();
			CString str;

			str.Format (_T ("[%.02f]: %s [%s] [%s]"), dDiff, n.m_strEnglish, n.m_strOriginal, n.m_strNew);
			TRACEF (str);

			rstSpanish.AddNew();
			rstSpanish.SetFieldValue (_T ("Stretch"),	COdbcVariant (dDiff));
			rstSpanish.SetFieldValue (_T ("English"),	n.m_strEnglish);
			rstSpanish.SetFieldValue (_T ("Original"),	n.m_strOriginal);
			rstSpanish.SetFieldValue (_T ("New"),		n.m_strNew);
			rstSpanish.Update ();
		}
		TRACEF ("");
		TRACEF ("NEW PORTUGUESE:");
		for (int i = 0; i < vPortuguese.GetSize (); i++) {
			NEWSTRUCT & n = vPortuguese [i];
			double dDiff = (double)n.m_strNew.GetLength () / (double)n.m_strOriginal.GetLength ();
			CString str;

			str.Format (_T ("[%.02f]: %s [%s] [%s]"), dDiff, n.m_strEnglish, n.m_strOriginal, n.m_strNew);
			TRACEF (str);

			rstPortuguese.AddNew();
			rstPortuguese.SetFieldValue (_T ("Stretch"),	COdbcVariant (dDiff));
			rstPortuguese.SetFieldValue (_T ("English"),	n.m_strEnglish);
			rstPortuguese.SetFieldValue (_T ("Original"),	n.m_strOriginal);
			rstPortuguese.SetFieldValue (_T ("New"),		n.m_strNew);
			rstPortuguese.Update ();
		}
	}
	
	for (int i = 0; i < vSpanish.GetSize (); i++) {
		NEWSTRUCT & n = vSpanish [i];

		if (ReplaceTranslatedRecord (theApp.m_db, LANGTYPE_SPANISH, n.m_strOriginal, n.m_strNew)) {
			if (!HasQuotes (n.m_strOriginal)) {
				CString strSQL;

				strSQL.Format (_T ("DELETE * FROM Sunnyvale_Spanish WHERE [New]='%s';"), n.m_strNew);
				theApp.m_db.ExecuteSQL (strSQL);
			}
			else {
				COdbcRecordset rstTmp (&theApp.m_db);

				rstTmp.Open (_T ("SELECT * FROM Sunnyvale_Spanish"), CRecordset::dynaset);

				while (!rstTmp.IsEOF ()) {
					CString strTmp = rstTmp.GetFieldValue (_T ("New"));

					if (!strTmp.Compare (n.m_strNew)) {
						rstTmp.Delete ();
						break;
					}

					rstTmp.MoveNext ();
				}
			}
		}
	}
	for (int i = 0; i < vPortuguese.GetSize (); i++) {
		NEWSTRUCT & n = vPortuguese [i];

		if (ReplaceTranslatedRecord (theApp.m_db, LANGTYPE_PORTUGUESE, n.m_strOriginal, n.m_strNew)) {
			if (!HasQuotes (n.m_strOriginal)) {
				CString strSQL;

				strSQL.Format (_T ("DELETE * FROM Sunnyvale_Portuguese WHERE [New]='%s';"), n.m_strNew);
				theApp.m_db.ExecuteSQL (strSQL);
			}
			else {
				COdbcRecordset rstTmp (&theApp.m_db);

				rstTmp.Open (_T ("SELECT * FROM Sunnyvale_Portuguese"), CRecordset::dynaset);

				while (!rstTmp.IsEOF ()) {
					CString strTmp = rstTmp.GetFieldValue (_T ("New"));

					if (!strTmp.Compare (n.m_strNew)) {
						rstTmp.Delete ();
						break;
					}

					rstTmp.MoveNext ();
				}
			}
		}
	}
}

void CParserDlg::OnPath() 
{
	CString strFile = theApp.GetProfileString (_T ("Settings"), _T ("DevPath"), _T ("C:\\Dev\\BoxWriter\\"));
	CFileDialog dlg (TRUE, _T ("*.*"), strFile, OFN_NOVALIDATE, _T ("All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();
		strFile.Replace (dlg.GetFileName (), _T (""));
		theApp.WriteProfileString (_T ("Settings"), _T ("DevPath"), strFile);
		SetDlgItemText (TXT_PATH, strFile);
		SetPath (strFile);
	}
}

void CParserDlg::SetPath(const CString &strPath)
{
	CString strOld;

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (CRcItem * p = (CRcItem *)m_lv.GetCtrlData (i)) {
			CString * pstr [] =
			{
				&p->m_strSource,
				&p->m_strEnglish,
				&p->m_strSpanish,
				&p->m_strPortuguese,
				&p->m_strDutch,
				&p->m_strRussian,
				&p->m_strHungarian,
			};

			for (int j = 0; j < ARRAYSIZE (pstr); j++) {
				int nIndex = pstr [j]->Find (_T ("Language\\"));

				if (nIndex != -1) {
					strOld = pstr [j]->Left (nIndex);
					break;
				}
			}

			for (int j = 0; j < ARRAYSIZE (pstr); j++) {
				VERIFY (pstr [j]->Replace (strOld, strPath));
				TRACEF (* pstr [j]);
			}

			TRACEF ("");
			m_lv.UpdateCtrlData (p);
		}
	}
}
