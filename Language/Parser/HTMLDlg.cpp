// HTMLDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Parser.h"
#include "HTMLDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHTMLDlg dialog


CHTMLDlg::CHTMLDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHTMLDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CHTMLDlg)
	m_strEnglish = _T("");
	m_strHTML = _T("");
	m_strRaw = _T("");
	m_strFormatted = _T("");
	//}}AFX_DATA_INIT
}


void CHTMLDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHTMLDlg)
	DDX_Text(pDX, TXT_ENGLISH, m_strEnglish);
	DDX_Text(pDX, TXT_HTML, m_strHTML);
	DDX_Text(pDX, TXT_RAW, m_strRaw);
	DDX_Text(pDX, TXT_FORMATTED, m_strFormatted);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHTMLDlg, CDialog)
	//{{AFX_MSG_MAP(CHTMLDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHTMLDlg message handlers
