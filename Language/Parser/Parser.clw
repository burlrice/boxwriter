; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CHTMLDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Parser.h"

ClassCount=3
Class1=CParserApp
Class2=CParserDlg

ResourceCount=5
Resource2=IDD_PARSER_DIALOG
Resource3=IDD_HTML
Resource4=IDD_DIALOG1
Resource1=IDR_MAINFRAME
Class3=CHTMLDlg
Resource5=IDR_MENU1

[CLS:CParserApp]
Type=0
HeaderFile=Parser.h
ImplementationFile=Parser.cpp
Filter=N
BaseClass=CWinApp
VirtualFilter=AC

[CLS:CParserDlg]
Type=0
HeaderFile=ParserDlg.h
ImplementationFile=ParserDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=BTN_COMPACT



[DLG:IDD_PARSER_DIALOG]
Type=1
Class=CParserDlg
ControlCount=11
Control1=LV_FILES,SysListView32,1350631433
Control2=BTN_COPY,button,1342242816
Control3=BTN_INTEGRATE,button,1342242816
Control4=BTN_EXTRACTSTRINGS,button,1342242816
Control5=BTN_FINDNEW,button,1342242816
Control6=BTN_GOOGLE,button,1342242816
Control7=BTN_VERIFY,button,1342242816
Control8=BTN_TRANSLATE,button,1342242816
Control9=BTN_COMPACT,button,1342242816
Control10=TXT_PATH,edit,1350633600
Control11=BTN_PATH,button,1342242816

[DLG:IDD_DIALOG1]
Type=1
Class=?
ControlCount=24
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342177287
Control5=IDC_EDIT1,edit,1350631552
Control6=IDC_CHECK1,button,1342242819
Control7=IDC_RADIO1,button,1342177289
Control8=IDC_COMBO1,combobox,1344340226
Control9=IDC_LIST1,listbox,1352728835
Control10=IDC_SCROLLBAR1,scrollbar,1342177280
Control11=IDC_SCROLLBAR2,scrollbar,1342177281
Control12=IDC_SPIN1,msctls_updown32,1342177312
Control13=IDC_PROGRESS1,msctls_progress32,1350565888
Control14=IDC_SLIDER1,msctls_trackbar32,1342242840
Control15=IDC_LIST2,SysListView32,1350631424
Control16=IDC_TREE1,SysTreeView32,1350631424
Control17=IDC_TAB1,SysTabControl32,1342177280
Control18=IDC_ANIMATE1,SysAnimate32,1350631424
Control19=IDC_RICHEDIT1,RICHEDIT,1350631552
Control20=IDC_DATETIMEPICKER1,SysDateTimePick32,1342242848
Control21=IDC_MONTHCALENDAR1,SysMonthCal32,1342242832
Control22=IDC_IPADDRESS2,SysIPAddress32,1342242816
Control23=IDC_CUSTOM1,,1342242816
Control24=IDC_COMBOBOXEX1,ComboBoxEx32,1344340226

[MNU:IDR_MENU1]
Type=1
Class=?
Command1=ID_FILE_PARSE_HTML
Command2=ID_FILE_GENERATEHTMLTABLE
Command3=ID_FILE_IMPORTSUNNYVALESTABLE
Command4=ID_VIEW_UNVERIFIED
CommandCount=4

[DLG:IDD_HTML]
Type=1
Class=CHTMLDlg
ControlCount=8
Control1=TXT_HTML,edit,1352728772
Control2=TXT_ENGLISH,edit,1352728772
Control3=TXT_RAW,edit,1352728772
Control4=TXT_FORMATTED,edit,1352728772
Control5=IDOK,button,1342242817
Control6=IDCANCEL,button,1342242816
Control7=IDC_STATIC,button,1342177287
Control8=IDC_STATIC,button,1342177287

[CLS:CHTMLDlg]
Type=0
HeaderFile=HTMLDlg.h
ImplementationFile=HTMLDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=TXT_FORMATTED
VirtualFilter=dWC

