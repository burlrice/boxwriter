// ParserDlg.h : header file
//

#if !defined(AFX_PARSERDLG_H__6470ADD4_6B1D_4E4D_A975_EECE630CE187__INCLUDED_)
#define AFX_PARSERDLG_H__6470ADD4_6B1D_4E4D_A975_EECE630CE187__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ListCtrlImp.h"
#include "OdbcRecordset.h"

typedef enum 
{
	LANGTYPE_ENGLISH,
	LANGTYPE_SPANISH,
	LANGTYPE_PORTUGUESE,
	LANGTYPE_DUTCH,
	LANGTYPE_RUSSIAN,
	LANGTYPE_HUNGARIAN,
	LANGTYPE_LAST = LANGTYPE_HUNGARIAN,
} LANGTYPE;

typedef struct 
{
	bool	m_bEnabled;
	CString	m_strSource;
	CString	m_strEnglish;
	CString m_strSpanish;
	CString m_strPortuguese;
	CString m_strDutch;
	CString m_strRussian;
	CString m_strHungarian;
} RCSTRUCT;

typedef struct 
{
	CString	m_strEnglish;
	CString m_strSpanish;
	CString m_strPortuguese;
	CString m_strDutch;
	CString m_strRussian;
	CString m_strHungarian;
} TRANSLATEDSTRUCT;

typedef struct 
{
	CString	m_strEnglish;
	CString m_strTranslated;
} INTEGRATESTRUCT;

class CRcItem : 
	public ItiLibrary::ListCtrlImp::CItem,
	public RCSTRUCT
{
public:
	CRcItem (const RCSTRUCT & rhs);
	virtual ~CRcItem ();

	virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
	virtual CString GetDispText (int nColumn) const;
};

const FoxjetDatabase::COdbcRecordset & operator >> (const FoxjetDatabase::COdbcRecordset & rst, RCSTRUCT & s);
FoxjetDatabase::COdbcRecordset & operator << (FoxjetDatabase::COdbcRecordset & rst, const RCSTRUCT & s);
// throw CDBException, CMemoryException 

const FoxjetDatabase::COdbcRecordset & operator >> (const FoxjetDatabase::COdbcRecordset & rst, TRANSLATEDSTRUCT & s);
FoxjetDatabase::COdbcRecordset & operator << (FoxjetDatabase::COdbcRecordset & rst, const TRANSLATEDSTRUCT & s);
// throw CDBException, CMemoryException 

const FoxjetDatabase::COdbcRecordset & operator >> (const FoxjetDatabase::COdbcRecordset & rst, INTEGRATESTRUCT & s);
FoxjetDatabase::COdbcRecordset & operator << (FoxjetDatabase::COdbcRecordset & rst, const INTEGRATESTRUCT & s);
// throw CDBException, CMemoryException 

/////////////////////////////////////////////////////////////////////////////
// CParserDlg dialog

class CParserDlg : public CDialog
{
// Construction
public:
	CParserDlg(CWnd* pParent = NULL);	// standard constructor

public:
	static const CString m_strRegSection;

// Dialog Data
	//{{AFX_DATA(CParserDlg)
	enum { IDD = IDD_PARSER_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CParserDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

public:

	static void GetFiles (const CString & strDir, const CString & strFile, CStringArray & v);
	static CString GetDir (const CString & strFullPath);
	static bool CopyDir (const CString & strSrc, const CString & strDest);

	bool IsExcluded (const CString & str);
	bool Translated (const CString & str);
	void EnableUI (bool bEnable = true);
	void SetPath (const CString & strPath);

	static bool HasQuotes (const CString & str);
	static int Replace (CString & str, const CString & strOld, const CString & strNew);

// Implementation
protected:

	int Translate (LANGTYPE type, const CStringArray & vInput, const CRcItem * p,
		CArray <TRANSLATEDSTRUCT, TRANSLATEDSTRUCT &> & vTranslated, CString & strError);
	int Integrate(LANGTYPE type);
	void Compact ();
	void Purge ();
	int FindNulls (LANGTYPE type);
	bool FindDuplicates ();

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CParserDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnExtractstrings();
	afx_msg void OnCopy();
	afx_msg void OnFindnew();
	afx_msg void OnVerify();
	afx_msg void OnTranslate();
	afx_msg void OnIntegrate();
	afx_msg void OnCompact();
	afx_msg void OnGoogle();
	afx_msg void OnFileParseHtml();
	afx_msg void OnFileGeneratehtmltable();
	afx_msg void OnFileImportsunnyvalestable();
	afx_msg void OnPath();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARSERDLG_H__6470ADD4_6B1D_4E4D_A975_EECE630CE187__INCLUDED_)
