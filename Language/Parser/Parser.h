// Parser.h : main header file for the PARSER application
//

#if !defined(AFX_PARSER_H__53D64E1F_CFBE_404A_8BE3_46228BA0224B__INCLUDED_)
#define AFX_PARSER_H__53D64E1F_CFBE_404A_8BE3_46228BA0224B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#include "OdbcDatabase.h"

/////////////////////////////////////////////////////////////////////////////
// CParserApp:
// See Parser.cpp for the implementation of this class
//

class CParserApp : public CWinApp
{
public:
	CParserApp();

	FoxjetDatabase::COdbcDatabase m_db;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CParserApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CParserApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARSER_H__53D64E1F_CFBE_404A_8BE3_46228BA0224B__INCLUDED_)
