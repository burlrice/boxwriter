//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Parser.rc
//
#define IDS_STRING1                     1
#define IDS_STRING2                     2
#define IDS_STRING3                     3
#define IDD_PARSER_DIALOG               102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDR_MENU1                       130
#define IDD_HTML                        131
#define BTN_EXTRACTSTRINGS              1001
#define LV_FILES                        1002
#define BTN_FINDNEW                     1003
#define IDC_EDIT1                       1003
#define BTN_VERIFY                      1004
#define IDC_CHECK1                      1004
#define BTN_TRANSLATE                   1005
#define IDC_RADIO1                      1005
#define IDC_COMBO1                      1006
#define BTN_COMPACT                     1006
#define BTN_COPY                        1007
#define IDC_LIST1                       1007
#define IDC_SCROLLBAR1                  1008
#define BTN_INTEGRATE                   1008
#define IDC_SCROLLBAR2                  1009
#define BTN_GOOGLE                      1009
#define IDC_SPIN1                       1010
#define BTN_PATH                        1010
#define IDC_PROGRESS1                   1011
#define IDC_SLIDER1                     1012
#define IDC_LIST2                       1013
#define IDC_TREE1                       1014
#define IDC_TAB1                        1015
#define IDC_ANIMATE1                    1016
#define IDC_RICHEDIT1                   1017
#define IDC_DATETIMEPICKER1             1018
#define IDC_MONTHCALENDAR1              1019
#define IDC_IPADDRESS2                  1021
#define IDC_CUSTOM1                     1022
#define IDC_COMBOBOXEX1                 1023
#define TXT_HTML                        1024
#define TXT_ENGLISH                     1025
#define TXT_PATH                        1025
#define TXT_RAW                         1026
#define TXT_FORMATTED                   1027
#define ID_FILE_PARSE_HTML              32771
#define ID_FILE_GENERATEHTMLTABLE       32772
#define ID_VIEW_UNVERIFIED              32773
#define ID_FILE_IMPORTSUNNYVALESTABLE   32774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
