// Parser.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Parser.h"
#include "ParserDlg.h"
#include "Database.h"

using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CParserApp

BEGIN_MESSAGE_MAP(CParserApp, CWinApp)
	//{{AFX_MSG_MAP(CParserApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CParserApp construction

CParserApp::CParserApp()
:	m_db (_T (__FILE__), __LINE__)
{
	FoxjetDatabase::SetFormatUnicode (false);
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CParserApp object

CParserApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CParserApp initialization

BOOL CParserApp::InitInstance()
{
	try {
		m_db.CDatabase::OpenEx (_T ("DSN=Parser"));
	}
	catch (CDBException * e) { HANDLEEXCEPTION (e); }

	AfxEnableControlContainer();

	// Standard initialization

#ifdef _AFXDLL
//	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CParserDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
	}
	else if (nResponse == IDCANCEL)
	{
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int CParserApp::ExitInstance() 
{
	m_db.Close ();
	
	return CWinApp::ExitInstance();
}
