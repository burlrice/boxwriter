//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Auxiliary.rc
//
#define IDS_DEVICE_NOT_OPEN             1
#define IDS_STROBEOUTOFRANGE            2
#define IDS_STROBESTATEINVALID          3
#define IDS_ERROR                       4
#define IDS_INVALIDPORT                 5
#define IDS_INVALIDPORTARG              6
#define IDS_SOCKETINITFAILED            7

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        6000
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         6000
#define _APS_NEXT_SYMED_VALUE           6000
#endif
#endif
