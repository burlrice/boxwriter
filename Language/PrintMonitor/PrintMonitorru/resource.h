//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by PrintMonitor.rc
//
#define BTN_BROWSE                      3
#define BTN_BROWSEFIRMWARE              3
#define BTN_UPDATEFIRMWARE              4
#define BTN_BROWSEGA                    5
#define BTN_UPDATEGA                    6
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PRINTMONITOR_DIALOG         102
#define IDS_ADDRESS                     102
#define IDS_STATE                       103
#define IDD_DRIVER                      103
#define IDS_COPY                        104
#define IDS_ERROR                       105
#define IDS_END                         106
#define IDS_ABORT                       107
#define IDS_BEGIN                       108
#define IDS_JOBSTART                    109
#define IDS_NOTHINGSELECTED             110
#define IDS_CANCELLING                  111
#define IDS_ABOUT                       112
#define IDS_PRINTDRIVER_ERROR_CONNECTFAILED 113
#define IDS_PRINTDRIVER_ERROR_CANCELSW  114
#define IDS_PRINTDRIVER_ERROR_CANCELHW  115
#define IDS_FAILEDTOINITIALIZEDB        116
#define IDS_ADDRESSCONFLICT             117
#define IDS_HEADNOTFOUND                118
#define IDS_UPDATECONFIG                119
#define IDS_SUCCEEDED                   120
#define IDS_FAILED                      121
#define IDS_CONNECTFAILED               122
#define IDS_UPDATEINPROGRESS            123
#define IDS_ATTEMPTINGCONNECTION        124
#define IDS_INVALIDID                   125
#define IDS_HEADNOTINDATABASE           126
#define IDS_INSTALLED                   127
#define IDR_MAINFRAME                   128
#define IDS_NAME                        128
#define IDM_SYSTRAY                     129
#define IDS_INSTALLFAILED               129
#define IDM_MENU                        130
#define IDS_NOTFOUND                    130
#define IDM_CONTEXT                     131
#define IDS_FIRMWARE                    131
#define IDD_FIRMWARE                    131
#define IDS_GATEARRAY                   132
#define IDD_PREVIEW                     132
#define IDS_INVALIDFILETYPE             133
#define IDD_SELECTPRINTER               133
#define IDS_FIRMWARE_UPDATE_FAILED      134
#define IDD_DRIVERSTORE                 134
#define IDS_FIRMWAREUPLOADCOMPLETE      135
#define IDS_GA_UPDATE_FAILED            136
#define IDS_GAUPLOADCOMPLETE            137
#define IDS_VERSIONOUTOFDATE            138
#define IDS_FAILEDTOCOPY                139
#define IDS_SPEED                       140
#define IDS_ERRORS                      141
#define IDS_CONFIGUPTODATE              142
#define IDS_VERIFYINGCONFIG             143
#define IDS_CONNECTED                   144
#define IDS_READY                       145
#define IDS_FAILEDTODELETE              146
#define IDS_INK                         147
#define IDS_HV                          148
#define IDS_TEMP                        149
#define IDS_CLASS                       150
#define IDS_GUID                        151
#define IDS_PROVIDER                    152
#define IDS_CATALOGFILE                 153
#define IDS_DRIVERVERSION               154
#define IDS_INF                         155
#define IDS_CONFIRMDELETE               156
#define LV_DATA                         1000
#define LBL_STATUS                      1001
#define IDC_STATUS                      1002
#define TXT_PATH                        1004
#define TXT_FIRMWAREVER                 1005
#define TXT_GAVER                       1006
#define TXT_NAME                        1007
#define TXT_FIRMWAREPATH                1008
#define TXT_GAPATH                      1009
#define TXT_ADDR                        1010
#define LV_PRINTERS                     1010
#define LV_DRIVERS                      1011
#define BTN_DELETE                      1012
#define ID__SHOWWINDOW                  32772
#define ID_FILE_EXIT                    32773
#define ID_DOCUMENT_CANCEL              32775
#define ID_HELP_ABOUT                   32776
#define ID_FILE_HEADCONFIGURATION       32777
#define ID_DOCUMENT_HEADPROPERTIES      32778
#define ID_DOCUMENT_SYNCRONIZECONFIGURATION 32779
#define ID_FILE_UPDATEALLCONFIGURATIONS 32781
#define ID_DOCUMENT_PRINTDRIVERPROPERTIES 32782
#define ID_FILE_INSTALLDRIVER           32783
#define ID_DOCUMENT_FIRMWAREVERSION     32784
#define ID_DOCUMENT_PREVIEW             32785
#define ID_FILE_DEBUGVIEWER             32786
#define ID_FILE_DRIVERCACHE             32787
#define ID_DOCUMENT_PRINTDRIVERVERSION  32788

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32789
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
