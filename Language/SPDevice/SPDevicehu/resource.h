//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SPDevice.rc
//
#define IDS_INVALID_APP_POINTER         1
#define IDS_INVALIDREGCODE              2
#define IDD_LICENSE_KEY                 156
#define IDC_HASHPART                    1094
#define IDC_REG_CODE                    1094
#define IDC_VER                         1095
#define IDC_SERIALNO                    1096

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1000
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           1000
#endif
#endif
