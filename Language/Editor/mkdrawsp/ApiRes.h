#ifndef __APIRES_H__
#define __APIRES_H__

#define ID_ELEMENT_01                   1201
#define ID_ELEMENT_02                   1202
#define ID_ELEMENT_03                   1203
#define ID_ELEMENT_04                   1204
#define ID_ELEMENT_05                   1205
#define ID_ELEMENT_06                   1206
#define ID_ELEMENT_07                   1207
#define ID_ELEMENT_08                   1208
#define ID_ELEMENT_09                   1209
#define ID_ELEMENT_10                   1210
#define ID_ELEMENT_11                   1211
#define ID_ELEMENT_12                   1212
#define ID_ELEMENT_13                   1213
#define ID_ELEMENT_14                   1214
#define ID_ELEMENT_15                   1215
#define ID_ELEMENT_16                   1216
#define ID_ELEMENT_17                   1217
#define ID_ELEMENT_18                   1218
#define ID_ELEMENT_19                   1219
#define ID_ELEMENT_20                   1220
#define ID_ELEMENT_21                   1221
#define ID_ELEMENT_22                   1222
#define ID_ELEMENT_23                   1223
#define ID_ELEMENT_24                   1224
#define ID_ELEMENT_25                   1225
#define ID_ELEMENT_26                   1226
#define ID_ELEMENT_27                   1227
#define ID_ELEMENT_28                   1228
#define ID_ELEMENT_29                   1229
#define ID_ELEMENT_30                   1230

#define ID_DEFINE_01                    1301
#define ID_DEFINE_02                    1302
#define ID_DEFINE_03                    1303
#define ID_DEFINE_04                    1304
#define ID_DEFINE_05                    1305
#define ID_DEFINE_06                    1306
#define ID_DEFINE_07                    1307
#define ID_DEFINE_08                    1308
#define ID_DEFINE_09                    1309
#define ID_DEFINE_10                    1310
#define ID_DEFINE_11                    1311
#define ID_DEFINE_12                    1312
#define ID_DEFINE_13                    1313
#define ID_DEFINE_14                    1314
#define ID_DEFINE_15                    1315
#define ID_DEFINE_16                    1316
#define ID_DEFINE_17                    1317
#define ID_DEFINE_18                    1318
#define ID_DEFINE_19                    1319
#define ID_DEFINE_20                    1320
#define ID_DEFINE_21                    1321
#define ID_DEFINE_22                    1322
#define ID_DEFINE_23                    1323
#define ID_DEFINE_24                    1324
#define ID_DEFINE_25                    1325
#define ID_DEFINE_26                    1326
#define ID_DEFINE_27                    1327
#define ID_DEFINE_28                    1328
#define ID_DEFINE_29                    1329
#define ID_DEFINE_30                    1330

#endif //__APIRES_H__
