#ifndef __REGISTRY_H__
#define __REGISTRY_H__

namespace FoxjetDatabase
{
	bool WriteProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szData);
	bool WriteProfileString (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szData);
	CString GetProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szDefault);
	CString GetProfileString (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szDefault);
	bool WriteProfileInt (LPCTSTR szSection, LPCTSTR szKey, UINT nData);
	bool WriteProfileInt (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, UINT nData);
	UINT GetProfileInt (LPCTSTR szSection, LPCTSTR szKey, UINT nDefault);
	UINT GetProfileInt (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, UINT nDefault);

	CString GetCmdlineValue (const CString & strCmdLine, const CString & strFlag);
}; // namespace FoxjetDatabase

#endif //__REGISTRY_H__
