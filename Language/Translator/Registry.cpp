#include "stdafx.h"
#include "Registry.h"
#include "Parse.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


bool FoxjetDatabase::WriteProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szData) 
{
	return WriteProfileString (HKEY_CURRENT_USER, szSection, szKey, szData);
}

bool FoxjetDatabase::WriteProfileString (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szData) 
{
	HKEY hKey;

	// if szSection created/opened okay
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = _tcslen (szData) + 1;

		#ifdef _UNICODE
		dwSize *= 2;
		#endif

		LONG lStatus = ::RegSetValueEx (hKey, szKey, 0, REG_SZ, 
			(const BYTE *) szData, dwSize);
		::RegCloseKey (hKey);

		return lStatus == ERROR_SUCCESS;
	}

	return false;
}

CString FoxjetDatabase::GetProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szDefault) 
{
	return GetProfileString (HKEY_CURRENT_USER, szSection, szKey, szDefault);
}

CString FoxjetDatabase::GetProfileString (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szDefault) 
{
	HKEY hKey;
	CString strResult (szDefault);

	// if szSection was in the registry
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwMaxValueLen = 256, dwType = REG_SZ;

		if (::RegQueryInfoKey (hKey, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, &dwMaxValueLen, NULL, NULL) != ERROR_SUCCESS)
		{
			dwMaxValueLen = 256;
		}

		TCHAR * pszData = new TCHAR [dwMaxValueLen + 1];

		if (::RegQueryValueEx (hKey, szKey, NULL, &dwType, 
			(LPBYTE) pszData, &dwMaxValueLen) == ERROR_SUCCESS) 
		{
			strResult = pszData;
		}

		delete [] pszData;
		::RegCloseKey (hKey);
	}

	return strResult;
}

bool FoxjetDatabase::WriteProfileInt (LPCTSTR szSection, LPCTSTR szKey, UINT nData) 
{
	return WriteProfileInt (HKEY_CURRENT_USER, szSection, szKey, nData);
}

bool FoxjetDatabase::WriteProfileInt (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, UINT nData) 
{
	HKEY hKey;

	// if szSection created/opened okay
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = sizeof (DWORD);
		DWORD dwData = nData;

		LONG lStatus = ::RegSetValueEx (hKey, szKey, 0, REG_DWORD, 
			(const BYTE *) &dwData, dwSize);
		::RegCloseKey (hKey);
		return lStatus == ERROR_SUCCESS;
	}

	return false;
}

UINT FoxjetDatabase::GetProfileInt (LPCTSTR szSection, LPCTSTR szKey, UINT nDefault) 
{
	return FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, szSection, szKey, nDefault);
}

UINT FoxjetDatabase::GetProfileInt (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, UINT nDefault) 
{
	HKEY hKey;
	UINT nResult = nDefault;

	// if szSection was in the registry
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = sizeof (DWORD), dwType = REG_DWORD;
		DWORD dwData;

		if (::RegQueryValueEx (hKey, szKey, NULL, &dwType, 
			(LPBYTE) &dwData, &dwSize) == ERROR_SUCCESS) 
		{
			nResult = (UINT)dwData;
		}

		::RegCloseKey (hKey);
	}

	return nResult;
}

CString FoxjetDatabase::GetCmdlineValue (const CString & strCmdLine, const CString & strFlag)
{
	CString str;
	int nIndex = strCmdLine.Find (strFlag);
	CStringArray v;
	
	if (nIndex != -1) 
		str = FoxjetDatabase::Tokenize (strCmdLine.Mid (nIndex + strFlag.GetLength ()), v, ' ');

	return str;
}
