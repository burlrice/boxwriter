//LibDump.CPP 


 //==========================================
 // Matt Pietrek
 // Microsoft Systems Journal, April 1998
 // Program: LibDump
 // FILE: LibDump.CPP
 //==========================================
 #include "stdafx.h"
 #include <windows.h>
 #include <stdio.h>
 #include <string.h>
 #include "MemoryMappedFile.h"
 
 //=============================================================================
 // Forward prototypes for functions in this file.
 //=============================================================================
 
 DWORD ConvertBigEndian(DWORD bigEndian);
 
 void DisplayLibInfoForSymbol(
         PSTR pszSymbol,
         PVOID pFileBase,
         DWORD archiveMemberOffset );
 
 BOOL IsRegularLibSymbol( PSTR pszSymbolName );
 
 //=============================================================================
 // Other miscellaneous variables and macros
 //=============================================================================
 
 char szHelpString[] = 
     "LibDump - Matt Pietrek 1998 for Microsoft Systems Journal\n"
     "  syntax: LibDump <library filename>\n";
 
 // MakePtr is a macro that allows you to easily add to values (including
 // pointers) together without dealing with C's pointer arithmetic.  It
 // essentially treats the last two parameters as DWORDs.  The first
 // parameter is used to typecast the result to the appropriate pointer type.
 #define MakePtr( cast, ptr, addValue ) (cast)( (DWORD)(ptr) + (DWORD)(addValue))
     
 //=============================================================================
 // Start of program code
 //=============================================================================
                                 
/*
 int main( int argc, char *argv[] )
 {
     if ( argc != 2 )    // Valid command line is "LibDump <filename>"
     {
         printf( szHelpString );
         return 0;   
     }
 
     PSTR pszLibFileName = argv[1];  // argv[1] is the first cmd line arg
 
     //
     // Map the file into memory so that we can access it as a region of bytes
     //      
     MEMORY_MAPPED_FILE libFile( pszLibFileName );
     
     // Ensure that the file mapping worked
     if ( FALSE == libFile.IsValid() )
     {
         printf( "Unable to access file %s\n", pszLibFileName );
         return 0;
     }
 
     // All COFF libraries start with the string "!<arch>\n".  Verify that this
     // string is at the beginning of the mapped file
     PSTR pArchiveStartString = (PSTR)libFile.GetBase();
     if ( 0 != strncmp( pArchiveStartString, IMAGE_ARCHIVE_START,
                        IMAGE_ARCHIVE_START_SIZE )  )
     {
         printf( "Not a valid COFF LIB file\n" );
         return 0;
     }
 
     // Point to the first archive member.  This entry contains the LIB symbols,
     // and immediately follows the archive start string ("!<arch>\n")
     PIMAGE_ARCHIVE_MEMBER_HEADER pMbrHdr;
     pMbrHdr = MakePtr( PIMAGE_ARCHIVE_MEMBER_HEADER, pArchiveStartString,
                         IMAGE_ARCHIVE_START_SIZE );
 
     // First DWORD after this member header is a symbol count
     PDWORD pcbSymbols = (PDWORD)(pMbrHdr + 1);  // Pointer math!
 
     // The symbol count is stored in big endian format, so adjust as
     // appropriate for the target architecture
     DWORD cSymbols = ConvertBigEndian( *pcbSymbols );
 
     // Following the symbol count is an array of offsets to archive members
     // (essentially, embedded .OBJ files)
     PDWORD pMemberOffsets = pcbSymbols + 1;     // Pointer math!
 
     // Following the array of member offsets is an array of offsets to symbol
     // names.
     PSTR pszSymbolName = MakePtr( PSTR, pMemberOffsets, 4 * cSymbols );
 
     // Print out a standard header consisting of the .LIB file name and column
     // headers for the output that will follow
     printf( "Library: %s\n", pszLibFileName );
     printf( "Ord/Hnt  Type  Symbol Information\n" );
     printf( "=======  ====  ==================\n" );
 
     //
     // Loop through every symbol in the first archive member
     //      
     for ( unsigned i = 0; i < cSymbols; i++ )
     {
         DWORD offset;
 
         // The offsets to the archive member that contains the symbol is stored
         // in big endian format, so convert it appropriately.        
         offset = ConvertBigEndian( *pMemberOffsets );
 
         // Call DisplayLibInfoForSymbol, which figures out what kind of symbol
         // it is.  The "IsRegularLibSymbol" filters out symbols that are
         // internal to the linking process
         if ( IsRegularLibSymbol( pszSymbolName ) )
             DisplayLibInfoForSymbol(pszSymbolName, libFile.GetBase(), offset);
                 
         // Advanced to the next symbol offset and name.  The MemberOffsets
         // array has fixed length entries, while the symbol names are
         // sequential null-terminated strings
         pMemberOffsets++;
         pszSymbolName += strlen(pszSymbolName) + 1;
     }
 
             
     return 0;
 }
*/
	 
 //=============================================================================
 // Function: DisplayLibInfoForSymbol
 //
 // Parameters:
 //      PSTR  pszSymbol           - The symbol name
 //      PVOID FileBase            - Base address of the memory mapped file
 //      DWORD archiveMemberOffset - Offset of archive member containing symbol
 //
 // Description:
 //
 // Given a symbol name, and the offset to the archive member that contains it,
 // the routine determines what kind of symbol it is (import by name, import by
 // ordinal, a GUID), and displays it accordingly.  If the symbol isn't one of
 // these types (e.g., it's a regular staticly linked symbol), the routine just
 // displays the symbol name.
 //=============================================================================
 void DisplayLibInfoForSymbol(
         PSTR pszSymbol,
         PVOID pFileBase,
         DWORD archiveMemberOffset )
 {
     //
     // Make a pointer to the archive member containing the symbol.  The
     // archive member is just an embedded .OBJ preceded by a standard archive
     // member header
     //
     PIMAGE_ARCHIVE_MEMBER_HEADER pMbrHdr
         = MakePtr(PIMAGE_ARCHIVE_MEMBER_HEADER, pFileBase, archiveMemberOffset);
 
     // Get a pointer to the start of the embedded .OBJ file that follows the
     // archive member header.  COFF .OBJ files begin with an IMAGE_FILE_HEADER
     // structure, which is defined in WINNT.H
     PIMAGE_FILE_HEADER pImgFileHdr = (PIMAGE_FILE_HEADER)(pMbrHdr + 1);
 
     // Following the IMAGE_FILE_HEADER is the IMAGE_SECTION_HEADER array.
     PIMAGE_SECTION_HEADER pSectHdr = (PIMAGE_SECTION_HEADER)(pImgFileHdr + 1);
 
     PDWORD pIdata5 = 0;     // Pointers to sections that we'll be looking for
     PWORD  pIdata6 = 0;     // in this .OBJ file.  The presence or absence of
     PBYTE  pRdata  = 0;     // certain sections gives hints what kind of symbol
     PVOID  pText   = 0;     // it is.
 
     BOOL fSaw_rdata = FALSE;
     
     // Loop through all of the IMAGE_SECTION_HEADERS, looking for .idata$5,
     // .idata$6, and .rdata.  As we find them, create a pointer to the raw
     // data for that section.            
     for ( unsigned i = 0; i < pImgFileHdr->NumberOfSections; i++ )
     {
         if ( 0 == strnicmp( (char *)pSectHdr->Name, ".idata$5",
                             IMAGE_SIZEOF_SHORT_NAME) )
         {
             pIdata5 = MakePtr(PDWORD, pImgFileHdr, pSectHdr->PointerToRawData);
         }
         else if ( 0 == strnicmp( (char *)pSectHdr->Name, ".idata$6",
                                  IMAGE_SIZEOF_SHORT_NAME) )
         {
             pIdata6 = MakePtr(PWORD, pImgFileHdr, pSectHdr->PointerToRawData);
         }
         else if ( 0 == strnicmp( (char *)pSectHdr->Name, ".text",
                                  IMAGE_SIZEOF_SHORT_NAME) )
         {
             pText = MakePtr(PVOID, pImgFileHdr, pSectHdr->PointerToRawData);
         }
         else if ( 0 == strnicmp( (char *)pSectHdr->Name, ".rdata",
                                  IMAGE_SIZEOF_SHORT_NAME) )
         {           
             // Hack!  We'll assume that if the .rdata section is the size of
             // a GUID (0x10 bytes) that it's really a GUID.  There's an obvious
             // flaw here, since sometimes multiple GUIDs are contained within
             // a single .rdata section.
             if ( !fSaw_rdata && (sizeof(GUID) == pSectHdr->SizeOfRawData) )
             {
                 pRdata = MakePtr(PBYTE, pImgFileHdr,pSectHdr->PointerToRawData);
             }
             else    // If we've already seen an .rdata section, then we're
             {       // probably not looking at a GUID, so set pRdata to NULL
                 pRdata = 0;
             }
             
             fSaw_rdata = TRUE;
         }
         
         pSectHdr++;     // Advance to the next section
     }   
 
     if ( !pIdata5 ) // If no .idata$5 section, it's not an import symbol
     {
         // If an .rdata section is found, but no .text section, assume it's
         // a GUID symbol
         if ( pRdata && !pText)
         {
             // Funky code to break apart the 0x10 bytes of a GUID into its
             // component parts for display
             DWORD part1 = *(PDWORD)pRdata;
             WORD  part2 = *(PWORD)(pRdata+4);
             WORD  part3 = *(PWORD)(pRdata+6);
             
             printf( "         GUID  "
                     "{%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X} %s\n",
                     part1, part2, part3,
                     pRdata[8],  pRdata[9],  pRdata[10], pRdata[11], 
                     pRdata[12], pRdata[13], pRdata[14], pRdata[15],
                     pszSymbol );
         }
         else    // No .rdata section, so just display the symbol name.  This
         {       // will be the case for staticly linked symbols (e.g., the
                 // C++ runtime library functions)
                 
             printf( "               %s\n", pszSymbol );
         }
         
         return;
     }
 
     //
     // If we get here, and .idata$5 section was found, indicating that it's
     // an import symbol
     //
     WORD ordinal = 0xFFFF;
     BOOL fOrdinal = FALSE;
     PSTR pszExportName = 0;
 
     // If the first DWORD in the .idata$5 section has the high bit set, it's
     // an import by ordinal symbol, and the low WORD is the ordinal.  If it's
     // not an ordinal import, this DWORD is zero.
     if ( *pIdata5 & IMAGE_ORDINAL_FLAG )
     {
         ordinal = (WORD)(*pIdata5 & ~IMAGE_ORDINAL_FLAG);
         fOrdinal = TRUE;
     }
 
     // If the .idata$6 section is found, the symbol is imported by name
     if ( pIdata6 )
     {
         // The first WORD is a "hint" ordinal value.  Immediately following
         // that is the name of the function that will appear in the EXE/DLLs
         // imports table.  Note that this name can be (and often is) different
         // from the name of the symbol that the linker actually references.
         // For example, "_TlsGetValue@4" becomes "TlsGetValue"
         pszExportName = (PSTR)(pIdata6 + 1);    // Pointer math
 
         if ( FALSE == fOrdinal )
             ordinal = *pIdata6;     // Get the hint ordinal
     }
 
     // Emit the import (or hint) ordinal, what type of import it is
     // (ORDN, NAME), and the name of the referenced symbol.
     printf( "%7u  %s  %s",
             ordinal, fOrdinal ? "ORDN" : "NAME", pszSymbol );
 
     // If the symbol is imported by name, append the name that will appear
     // in the imports section of the EXE/DLL.
     if ( pszExportName )
         printf( " imported as %s", pszExportName );
         
     printf( "\n" );
 }
 
 //=============================================================================
 // Filters out symbols that are internal to the linking process, and which
 // the programmer never explicitly sees.
 //=============================================================================
 
 BOOL IsRegularLibSymbol( PSTR pszSymbolName )
 {
     if ( 0 == strncmp( pszSymbolName, "__IMPORT_DESCRIPTOR_", 20 ) )
         return FALSE;
 
     if ( 0 == strncmp( pszSymbolName, "__NULL_IMPORT_DESCRIPTOR", 24 ) )
         return FALSE;
         
     if ( strstr( pszSymbolName, "_NULL_THUNK_DATA" ) )
         return FALSE;
         
     return TRUE;
 }
 
 //=============================================================================
 // Converts from big endian to little endian numbers.
 //=============================================================================
 DWORD ConvertBigEndian(DWORD bigEndian)
 {
     DWORD temp = 0;
 
     temp |= bigEndian >> 24;
     temp |= ((bigEndian & 0x00FF0000) >> 8);
     temp |= ((bigEndian & 0x0000FF00) << 8);
     temp |= ((bigEndian & 0x000000FF) << 24);
 
     return temp;
 }

 

