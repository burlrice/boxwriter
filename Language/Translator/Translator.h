// Translator.h : main header file for the TRANSLATOR application
//

#if !defined(AFX_TRANSLATOR_H__A57E9A4A_1567_4B1C_93D3_E58579DCA6CC__INCLUDED_)
#define AFX_TRANSLATOR_H__A57E9A4A_1567_4B1C_93D3_E58579DCA6CC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

CString GetDir ();
CString GetDir (const CString & strFullPath);
CString GetTitle (const CString & strFullPath);
void TraceAndLog (const CString & strTrace, LPCTSTR lpszFile, ULONG lLine);

#define TRACEL(s) TraceAndLog((s), __FILE__, __LINE__)

/////////////////////////////////////////////////////////////////////////////
// CTranslatorApp:
// See Translator.cpp for the implementation of this class
//

class CTranslatorApp : public CWinApp
{
public:
	CTranslatorApp();

	static const CString m_strLogfile;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTranslatorApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CTranslatorApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRANSLATOR_H__A57E9A4A_1567_4B1C_93D3_E58579DCA6CC__INCLUDED_)
