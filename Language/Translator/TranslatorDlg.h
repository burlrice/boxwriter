// TranslatorDlg.h : header file
//

#if !defined(AFX_TRANSLATORDLG_H__9E458B46_C773_49C9_B147_96315DCA5268__INCLUDED_)
#define AFX_TRANSLATORDLG_H__9E458B46_C773_49C9_B147_96315DCA5268__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>
#include "AppVer.h"
#include "RoundButtons.h"


/////////////////////////////////////////////////////////////////////////////
// CTranslatorDlg dialog

namespace TranslatorDlg
{
	class CResource
	{
	public:
		CResource (LPCTSTR lpType = NULL, LPCTSTR lpName = NULL);
		CResource (const CResource & rhs);
		CResource & operator = (const CResource & rhs);
		virtual ~CResource ();

		LPCTSTR m_lpType;
		LPCTSTR	m_lpName;
	};

	class CResFile 
	{
	public:
		CResFile (const CString & strPath);

		CString m_strTitle;
		CString m_strPath;
		CString m_strExt;
	};

	class CResFilePair 
	{
	public:
		CResFilePair (const CResFile & rTranslate, const CResFile & rDll);

		CResFile m_rTranslate;
		CResFile m_rDll;
	};

	class CUpdateException : public CException
	{
		DECLARE_DYNAMIC (CUpdateException);

	public:
		CUpdateException (const CString & str, bool bAutoDelete = true);

		CString m_str;
	};
};
// namespace TranslatorDlg

class CTranslatorDlg : public CDialog
{
// Construction
public:	
	CTranslatorDlg(CWnd* pParent = NULL);	// standard constructor

public:	
	bool IsLangDir (const CString & str) const;


	typedef CArray <TranslatorDlg::CResource, TranslatorDlg::CResource &> CResourceArray;
	typedef CArray <TranslatorDlg::CResFile, TranslatorDlg::CResFile &> CResFileArray;

// Dialog Data
	//{{AFX_DATA(CTranslatorDlg)
	enum { IDD = IDD_TRANSLATOR_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTranslatorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

protected:
	CString GetLangDir();
	CString GetAppDir();

	void Free ();
	static CString GetType (LPCTSTR lp);
	bool GetResources (const CString & str, CResourceArray & v);
	void CopyResources (const CString & strModule, const CString & strResDll);
	static BOOL CALLBACK EnumResTypeProc (HMODULE hModule, LPTSTR lpType, LONG lParam);
	static BOOL CALLBACK EnumResNameProc (HMODULE hModule, LPCTSTR lpType, LPTSTR lpName, LONG lParam);
	static int GetFiles (const CString & strDir, const CString & strFile, CStringArray & v);
	static int Find (const CString & str, const CStringArray & v);
	static CString FormatError (DWORD dwError);


	CListCtrl & GetListCtrl () const;
	void ResetListCtrlContent ();
	void FillLangCtrl ();

	static ULONG CALLBACK TranslateFunc (LPVOID lpData);
	void EnableUI (bool bEnable);
	
// Implementation
public:
	static CString Format (const FoxjetCommon::VERSIONSTRUCT & ver);
	
	CString m_strLaunch;
	bool m_bAuto;

protected:

	CString m_strAppDir;
	CString m_strLangDir;
	HICON m_hIcon;
	HANDLE m_hThread;
	bool m_bControlRunning;
	bool m_bEditorRunning;
	bool m_bKBRunning;
	bool m_bKBMonRunning;
	FoxjetUtils::CRoundButtons m_buttons;

	afx_msg LRESULT OnEndTranslate(WPARAM wParam, LPARAM lParam);

	// Generated message map functions
	//{{AFX_MSG(CTranslatorDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTranslate();
	afx_msg void OnDblclkLanguages();
	afx_msg void OnClose();
	afx_msg void OnSelchangeLanguages();
	afx_msg void OnDestroy();
	afx_msg void OnBrowse();
	afx_msg void OnBrowselang();
	afx_msg void OnDblclkComponents(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRANSLATORDLG_H__9E458B46_C773_49C9_B147_96315DCA5268__INCLUDED_)
