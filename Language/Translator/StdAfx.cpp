// stdafx.cpp : source file that includes just the standard includes
//	Translator.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include <afxtempl.h>
#include "WinMsg.h"
#include "..\..\Control\Host.h"

void Trace (LPCTSTR psz, LPCTSTR pszFile, ULONG nLine)
{
	CString str;

	str.Format (_T ("%s(%u): %s\n"), pszFile, nLine, psz);
	::OutputDebugString (str);
}

CString Extract (const CString & strParse, const CString & strBegin)
{
	CString str;
	int nIndex = strParse.Find (strBegin);

	if (nIndex != -1) 
		str = strParse.Mid (nIndex + strBegin.GetLength ());

	return str;
}

CString Extract (const CString & strParse, const CString & strBegin, const CString & strEnd)
{
	CString str;
	int nIndex [2] = 
	{
		strParse.Find (strBegin),

		// start looking for strEnd after strBegin
		strParse.Find (strEnd, (nIndex [0] == -1) ? 0 : nIndex [0] + strBegin.GetLength ()), 
	};

	if (nIndex [0] != -1 && nIndex [1] != -1) {
		int nStart = nIndex [0] + strBegin.GetLength ();
		int nLen = nIndex [1] - nStart;

		if (nLen > 0)
			str = strParse.Mid (nStart, nLen);
	}

	return str;
}

static BOOL CALLBACK EnumHiddenWindowsProc (HWND hWnd, LPARAM lParam)
{
	CArray <HWND, HWND> & v = * (CArray <HWND, HWND> *)lParam;
	
	v.Add (hWnd);

	return TRUE;
}

HWND FindKeyboard ()
{
	CArray <HWND, HWND> v;

	::EnumWindows (EnumHiddenWindowsProc, (LPARAM)&v);

	for (int i = 0; i < v.GetSize (); i++) {
		DWORD dwResult = 0;
		HWND hwnd = v [i];

		#ifdef _DEBUG
		TCHAR sz [256] = { 0 };

		::GetWindowText (hwnd, sz, ARRAYSIZE (sz));
		#endif

		LRESULT lResult = ::SendMessageTimeout (hwnd, WM_ISKEYBOARD, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 100, &dwResult);
		
		//TRACEF (_T ("WM_ISKEYBOARD: ") + FoxjetDatabase::ToString ((int)lResult) + _T (" :") + FoxjetDatabase::ToString ((int)dwResult));

		if (dwResult == BW_HOST_TCP_PORT) {
			#ifdef _DEBUG
			TRACEF (sz);
			#endif

			return hwnd;
		}
	}

	return NULL;
}

bool IsPopupKeyboard (HWND hwnd) 
{
	DWORD dwResult = 0;
	LRESULT lResult = ::SendMessageTimeout (hwnd, WM_ISKEYBOARDRUNNING, 1, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dwResult);
	return dwResult == 1;
}

void AlignToKeyboard (CWnd * pwnd)
{
	if (pwnd) {// && !IsMatrix ()) {
		if (HWND hwnd = FindKeyboard ()) {
			if (!IsPopupKeyboard (hwnd)) {
				CRect rc (0, 0, 0, 0);
				CRect rcKeyboard (0, 0, 0, 0);

				::GetWindowRect (pwnd->m_hWnd, rc);
				::GetWindowRect (hwnd, rcKeyboard);

				int nDiff = rc.bottom - rcKeyboard.top;

				if (nDiff > 0) 
					::SetWindowPos (pwnd->m_hWnd, NULL, rc.left, rc.top - nDiff, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
			}
		}
	}
}
