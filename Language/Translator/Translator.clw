; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CTranslatorDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Translator.h"

ClassCount=4
Class1=CTranslatorApp
Class2=CTranslatorDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_TRANSLATOR_DIALOG

[CLS:CTranslatorApp]
Type=0
HeaderFile=Translator.h
ImplementationFile=Translator.cpp
Filter=N

[CLS:CTranslatorDlg]
Type=0
HeaderFile=TranslatorDlg.h
ImplementationFile=TranslatorDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=TranslatorDlg.h
ImplementationFile=TranslatorDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_TRANSLATOR_DIALOG]
Type=1
Class=CTranslatorDlg
ControlCount=8
Control1=IDC_STATIC,static,1342308352
Control2=LB_LANGUAGES,listbox,1352728835
Control3=LV_COMPONENTS,SysListView32,1350664201
Control4=BTN_BROWSE,button,1342242816
Control5=BTN_BROWSELANG,button,1342242816
Control6=BTN_TRANSLATE,button,1342242817
Control7=IDC_STATIC,static,1342308352
Control8=TXT_PATH,edit,1350633600

