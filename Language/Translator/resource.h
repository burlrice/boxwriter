//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Translator.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TRANSLATOR_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDB_BROWSE                      129
#define LB_LANGUAGES                    1000
#define BTN_TRANSLATE                   1001
#define LV_COMPONENTS                   1002
#define TXT_PATH                        1003
#define BTN_BROWSE                      1004
#define LBL_VERSION                     1005
#define BTN_BROWSELANG                  1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
