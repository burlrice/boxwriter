// TranslatorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Translator.h"
#include "TranslatorDlg.h"
#include "WinMsg.h"
#include "TemplExt.h"
#include "AppVer.h"

extern CTranslatorApp theApp;

#define WM_ENDTRANSLATE (WM_APP + 1)

using namespace FoxjetCommon;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

typedef struct
{
	CString m_strName;
	CStringArray m_vDLLs;
} LANGDIRSTRUCT;

static const LPCTSTR lpszLanguages [] = 
{
	_T ("English"),
	_T ("Spanish"),
	_T ("Portuguese"),
	_T ("Dutch"),
	_T ("Russian"),
};

/////////////////////////////////////////////////////////////////////////////

static const CString strUpdated = _T ("Updated");

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////

using namespace TranslatorDlg;

CResource::CResource (LPCTSTR lpType, LPCTSTR lpName)
:	m_lpType (lpType),
	m_lpName (lpName)
{
}

CResource::CResource (const CResource & rhs)
:	m_lpType (rhs.m_lpType),
	m_lpName (rhs.m_lpName)
{
}

CResource & CResource::operator = (const CResource & rhs)
{
	if (this != &rhs) {
		m_lpType = rhs.m_lpType;
		m_lpName = rhs.m_lpName;
	}

	return * this;
}

CResource::~CResource ()
{
}


/////////////////////////////////////////////////////////////////////////////

CResFile::CResFile (const CString & strPath)
:	m_strPath (strPath)
{
	TCHAR sz [MAX_PATH] = { 0 };

	::GetFileTitle (strPath, sz, ARRAYSIZE (sz) - 1);
	CString str = sz;
	int nIndex = str.Find (_T ("."));

	if (nIndex != -1) {
		m_strTitle = str.Left (nIndex);

		if ((nIndex + 1) < str.GetLength ())
			m_strExt = str.Mid (nIndex + 1);
	}	
	else { // file extensions turned off
		nIndex = strPath.Find (_T ("."));
		ASSERT (nIndex != -1);
		m_strTitle = str;

		if ((nIndex + 1) < strPath.GetLength ())
			m_strExt = strPath.Mid (nIndex + 1);
	}
}

/////////////////////////////////////////////////////////////////////////////
		
CResFilePair::CResFilePair (const CResFile & rTranslate, const CResFile & rDll)
:	m_rTranslate (rTranslate),
	m_rDll (rDll)
{
}

/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC (CUpdateException, CException);

CUpdateException::CUpdateException (const CString & str, bool bAutoDelete)
:	m_str (str),
	CException (bAutoDelete)
{
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTranslatorDlg dialog

CTranslatorDlg::CTranslatorDlg(CWnd* pParent /*=NULL*/)
:	m_hThread (NULL),
	m_bControlRunning (false),
	m_bEditorRunning (false),
	m_bKBRunning (false),
	m_bKBMonRunning (false),
	m_bAuto (true),
	CDialog(CTranslatorDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTranslatorDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

}

void CTranslatorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTranslatorDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, BTN_BROWSE,		IDB_BROWSE);
	m_buttons.DoDataExchange (pDX, BTN_BROWSELANG,	IDB_BROWSE);
	m_buttons.DoDataExchange (pDX, BTN_TRANSLATE);
}

BEGIN_MESSAGE_MAP(CTranslatorDlg, CDialog)
	//{{AFX_MSG_MAP(CTranslatorDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_TRANSLATE, OnTranslate)
	ON_LBN_DBLCLK(LB_LANGUAGES, OnDblclkLanguages)
	ON_WM_CLOSE()
	ON_LBN_SELCHANGE(LB_LANGUAGES, OnSelchangeLanguages)
	ON_WM_DESTROY()
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_BN_CLICKED(BTN_BROWSELANG, OnBrowselang)
	ON_NOTIFY(NM_DBLCLK, LV_COMPONENTS, OnDblclkComponents)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
	ON_MESSAGE (WM_ENDTRANSLATE, OnEndTranslate)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTranslatorDlg message handlers

CString CTranslatorDlg::Format (const FoxjetCommon::VERSIONSTRUCT & ver)
{
	CString str;

	if (ver.m_nCustomMajor || ver.m_nCustomMinor)
		str.Format (_T ("%d.%02d.%04d.%03d"),
			ver.m_nMajor, ver.m_nMinor, ver.m_nCustomMajor, ver.m_nInternal);
	else
		str.Format (_T ("%d.%02d [%d]"),
			ver.m_nMajor, ver.m_nMinor, ver.m_nInternal);

	return str;
}

BOOL CTranslatorDlg::OnInitDialog()
{
	using namespace FoxjetCommon;

	CListCtrl & lv = GetListCtrl ();
	CString strVer = Format (verApp);
	
	CDialog::OnInitDialog();
	SetWindowText (_T ("Translator (") + strVer + _T (")"));

/*
	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
*/

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

/*
	m_strAppDir = theApp.GetProfileString (_T ("Settings"), _T ("AppDir"), GetDir ());
	m_strLangDir = theApp.GetProfileString (_T ("Settings"), _T ("LangDir"), GetDir () + _T ("\\Language\\"));
*/
	m_strAppDir = GetDir ();
	m_strLangDir = GetDir () + _T ("\\Language\\");

	SetDlgItemText (TXT_PATH, m_strAppDir);

	lv.InsertColumn (0, _T ("Component"),		LVCFMT_LEFT, 200);
	lv.InsertColumn (1, _T ("Language DLL"),	LVCFMT_LEFT, 200);
	lv.InsertColumn (2, _T ("Status"),			LVCFMT_LEFT, 250);

	lv.SetExtendedStyle (LVS_EX_FULLROWSELECT);

	if (CListBox * p = (CListBox *)GetDlgItem (LB_LANGUAGES)) 
		p->SetItemHeight (0, p->GetItemHeight (0) * 2);

	FillLangCtrl ();
	OnSelchangeLanguages ();

	{
		CString str = ::GetCommandLine ();

		str.MakeLower ();

		if (m_bAuto = str.Find (_T ("/auto ")) != -1) {
			const CString strLang = Extract (str, _T ("/lang=\""), _T ("\""));
			bool bSelected = false;

			if (CListBox * pLang = (CListBox *)GetDlgItem (LB_LANGUAGES)) {
				for (int i = 0; i < pLang->GetCount (); i++) {
					if (LANGDIRSTRUCT * p = (LANGDIRSTRUCT *)pLang->GetItemData (i)) {
						TRACEF (p->m_strName);

						if (!p->m_strName.CompareNoCase (strLang)) {
							pLang->SetCurSel (i);
							OnSelchangeLanguages ();
							SendMessage (WM_COMMAND, MAKEWPARAM (BTN_TRANSLATE, BN_CLICKED), NULL);
							bSelected = true;
							break;
						}
					}
				}						
			}

			if (!bSelected)
				m_bAuto = false;
		}
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTranslatorDlg::Free ()
{
	CListBox * pLang = (CListBox *)GetDlgItem (LB_LANGUAGES);

	ASSERT (pLang);

	for (int i = 0; i < pLang->GetCount (); i++) 
		if (LANGDIRSTRUCT * p = (LANGDIRSTRUCT *)pLang->GetItemData (i))
			delete p;

	pLang->ResetContent ();
}

void CTranslatorDlg::FillLangCtrl ()
{
	CListBox * pLang = (CListBox *)GetDlgItem (LB_LANGUAGES);
	CString strLangDir = GetLangDir ();

	Free ();
	ASSERT (pLang);

	for (int i = 0; i < 2; i++) {
		WIN32_FIND_DATA fd;
		CString strFind = strLangDir + _T ("*.*");
		HANDLE hFind = ::FindFirstFile (strFind, &fd);
		bool bMore = hFind != INVALID_HANDLE_VALUE;

		while (bMore) {
			if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				CString str = fd.cFileName;
				
				if (str != _T (".") && str != _T ("..")) {
					CStringArray v;

					GetFiles (strLangDir + str, _T ("*.dll"), v);

					if (/* IsLangDir (str) && */ v.GetSize ()) {
						LANGDIRSTRUCT * p = new LANGDIRSTRUCT;

						m_strLangDir = strLangDir;

						p->m_strName = str;
						p->m_vDLLs.Append (v);

						int nIndex = pLang->AddString (str);
						pLang->SetItemData (nIndex, (DWORD)p);
					}
				}
			}

			ZeroMemory (&fd, sizeof (fd));
			bMore = ::FindNextFile (hFind, &fd) ? true : false;
		}

		::FindClose (hFind);

		if (pLang->GetCount ())
			break;
		else {
			int nIndex;

			if ((nIndex = strLangDir.ReverseFind ('\\')) != -1) {
				CString str = strLangDir.Left (nIndex);
				
				if ((nIndex = str.ReverseFind ('\\')) != -1)
					strLangDir = strLangDir.Left (nIndex) + '\\';
			}
		}
	}

	if (pLang->GetCurSel () == LB_ERR)
		pLang->SetCurSel (0);
}

int CTranslatorDlg::GetFiles (const CString & strDir, const CString & strFile, CStringArray & v)
{
	WIN32_FIND_DATA fd;
	const CString str = strDir + _T ("\\") + strFile;
	HANDLE hFind = ::FindFirstFile (str, &fd);
	bool bMore = hFind != INVALID_HANDLE_VALUE;
	int nFiles = 0;

	TRACEF (str);

	while (bMore) {
		CString str = strDir;
		
		if (str [str.GetLength () - 1] != '\\')
			str += '\\';

		str += fd.cFileName;

		v.Add (str);
		TRACEF ("\t" + str);
		nFiles++;
		ZeroMemory (&fd, sizeof (fd));
		bMore = ::FindNextFile (hFind, &fd) ? true : false;
	}

	::FindClose (hFind);

	return nFiles;
}

void CTranslatorDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTranslatorDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTranslatorDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

int CTranslatorDlg::Find (const CString & str, const CStringArray & v)
{
	for (int i = 0; i < v.GetSize (); i++) {
		CResFile r (v [i]);

		if (r.m_strTitle.Find (str) != -1)
			return i;
	}

	return -1;
}


ULONG CALLBACK CTranslatorDlg::TranslateFunc (LPVOID lpData)
{
	ASSERT (lpData);

	CTranslatorDlg & dlg = * (CTranslatorDlg *)lpData;
	CListCtrl & lv = dlg.GetListCtrl ();
	
	dlg.BeginWaitCursor ();

	for (int i = 0; i < lv.GetItemCount (); i++) {
		if (CResFilePair * p = (CResFilePair *)lv.GetItemData (i)) {
			const CString strDir = GetDir (p->m_rTranslate.m_strPath);
			CStringArray vPre, vPost;

			GetFiles (strDir, _T ("*.tmp"), vPre);

			for (int nFile = 0; nFile < vPre.GetSize (); nFile++) {
				CString strFile = vPre [nFile];

				if (!::DeleteFile (strFile)) {
					TRACEL ("DeleteFile failed: " + strFile + ": " + FormatError (::GetLastError ()));
				}
			}

			vPre.RemoveAll ();
			GetFiles (strDir, _T ("*.tmp"), vPre);

			for (int nFile = 0; nFile < vPre.GetSize (); nFile++) 
				TRACEL (vPre [nFile]);
			
			try {
				lv.SetItemText (i, 2, _T ("Translating..."));
				TRACEF (p->m_rTranslate.m_strPath + " <-- " + p->m_rDll.m_strPath + "\n");
				dlg.CopyResources (p->m_rTranslate.m_strPath, p->m_rDll.m_strPath);
				lv.SetItemText (i, 2, ::strUpdated);
			}
			catch (CUpdateException * e) { 
				lv.SetItemText (i, 2, e->m_str);
				TRACEL (e->m_str);
				e->Delete ();
			}

			if (GetFiles (strDir, _T ("*.tmp"), vPost)) {
				CString str = "Failure: ";

				for (int nFile = 0; nFile < vPost.GetSize (); nFile++) 
					str += " " + GetTitle (vPost [nFile]);

				lv.SetItemText (i, 2, str);
				TRACEL (p->m_rTranslate.m_strPath + ": " + str);
			}

			::Sleep (0);
		}
	}

	while (!::PostMessage (dlg.m_hWnd, WM_ENDTRANSLATE, 0, 0))
		::Sleep (0);

	return 0;
}

void CTranslatorDlg::OnTranslate() 
{
	bool bShutdown = false;
	HANDLE hMutexEditor = NULL, hMutexPrinter = NULL;
	HANDLE hMutexKB = NULL, hMutexKBMon = NULL;
	CListCtrl & lv = GetListCtrl ();

	if (!lv.GetItemCount ()) {
		MessageBox (_T ("No language files found.  Cannot translate."), NULL, MB_ICONERROR);
		return;
	}

	hMutexEditor = ::CreateMutex (NULL, FALSE, ISEDITORRUNNING);

	if (GetLastError () == ERROR_ALREADY_EXISTS || GetLastError () == ERROR_ACCESS_DENIED)
		m_bEditorRunning = bShutdown = true;

	hMutexPrinter = ::CreateMutex (NULL, FALSE, ISPRINTERRUNNING);

	if (GetLastError () == ERROR_ALREADY_EXISTS || GetLastError () == ERROR_ACCESS_DENIED)
		m_bControlRunning = bShutdown = true;

	hMutexKB = ::CreateMutex (NULL, FALSE, ISKEYBOARDRUNNING);

	if (GetLastError () == ERROR_ALREADY_EXISTS || GetLastError () == ERROR_ACCESS_DENIED)
		m_bKBRunning = bShutdown = true;

	hMutexKBMon = ::CreateMutex (NULL, FALSE, ISKEYBOARDMONITORRUNNING);

	if (GetLastError () == ERROR_ALREADY_EXISTS || GetLastError () == ERROR_ACCESS_DENIED)
		m_bKBMonRunning = bShutdown = true;

	{
		HANDLE h [] = { hMutexEditor, hMutexPrinter, hMutexKB, hMutexKBMon };

		for (int i = 0; i < ARRAYSIZE (h); i++)
			::CloseHandle (h [i]);
	}

	if (bShutdown) {
		CString str = _T ("One or more applications are running.  They must be closed to translate.\nClose them now?");

		if (MessageBox (str, NULL, MB_YESNO) == IDYES) {
			DWORD dwResult;

			BeginWaitCursor ();
			LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_SHUTDOWNAPP, SHUTDOWN_ALL, 0, 
				SMTO_BLOCK | SMTO_ABORTIFHUNG, 10000, &dwResult);
			::Sleep (2000);
			EndWaitCursor ();
		}
		else
			return;
	}

	if (!m_hThread) {
		DWORD dw;

		EnableUI (false);

		for (int i = 0; i < lv.GetItemCount (); i++) 
			lv.SetItemText (i, 2, _T (""));

		m_hThread = ::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0,
			(LPTHREAD_START_ROUTINE)TranslateFunc, (LPVOID)this, 0, &dw);
	}
}

void CTranslatorDlg::EnableUI (bool bEnable)
{
	UINT nID [] = 
	{
		BTN_BROWSE,
		BTN_TRANSLATE,
		BTN_BROWSELANG,
	};

	if (!bEnable)
		BeginWaitCursor ();
	else
		EndWaitCursor ();

	for (int i = 0; i < ARRAYSIZE (nID); i++)
		if (CWnd * p = GetDlgItem (nID [i]))
			p->EnableWindow (bEnable);
}

LRESULT CTranslatorDlg::OnEndTranslate(WPARAM wParam, LPARAM lParam)
{
	bool bError = false;
	CListCtrl & lv = GetListCtrl ();

	m_hThread = NULL;
	EnableUI (true);
	
	for (int i = 0; i < lv.GetItemCount (); i++) {
		if (CResFilePair * p = (CResFilePair *)lv.GetItemData (i)) {
			CString str = lv.GetItemText (i, 2);

			if (str != ::strUpdated) {
				bError = true;
				break;
			}
		}
	}

	if (!bError) {
		CStringArray vLaunch;

		if (!m_bAuto) 
			MessageBox ("Translation complete\nClick OK to close the Translator");
		else
			::Sleep (1000);

		// need to launch with a separate app to ensure exe's are completely 
		// unreferenced from this app

		if (m_bControlRunning) 
			vLaunch.Add (_T ("\"") + GetAppDir () + _T ("Control.exe\" ") + theApp.GetProfileString (_T ("Cmd line"), _T ("Control"), _T ("")));
		
		if (m_bEditorRunning) 
			vLaunch.Add (_T ("\"") + GetAppDir () + _T ("mkdraw.exe\" ") + theApp.GetProfileString (_T ("Cmd line"), _T ("Editor"), _T ("")));

		if (m_bKBRunning)
			vLaunch.Add (_T ("\"") + GetAppDir () + _T ("Keyboard.exe\" ") + theApp.GetProfileString (_T ("Cmd line"), _T ("Keyboard"), _T ("")));

		if (m_bKBMonRunning)
			vLaunch.Add (_T ("\"") + GetAppDir () + _T ("KbMonitor.exe\" ") + theApp.GetProfileString (_T ("Cmd line"), _T ("Keyboard"), _T ("")));

		theApp.WriteProfileString (_T ("Cmd line"), _T ("Control"),		_T (""));
		theApp.WriteProfileString (_T ("Cmd line"), _T ("Editor"),		_T (""));
		theApp.WriteProfileString (_T ("Cmd line"), _T ("Keyboard"),	_T (""));

		for (int i = 0; i < vLaunch.GetSize (); i++) {
			const CString strApp = _T ("Launch.exe");
			CString strPath = GetAppDir () + strApp;
			CString strCmdLine = strPath + _T (" ") + vLaunch [i];

			STARTUPINFO si;     
			PROCESS_INFORMATION pi; 
			char szCmdLine [MAX_PATH];

			_tcsncpy (szCmdLine, strCmdLine, MAX_PATH);
			memset (&pi, 0, sizeof(pi));
			memset (&si, 0, sizeof(si));
			si.cb = sizeof(si);     
			si.dwFlags = STARTF_USESHOWWINDOW;     
			si.wShowWindow = SW_SHOW;     

			TRACEF (szCmdLine);
			::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
		}

		PostMessage (WM_CLOSE);
	}

	return 0;
}

void CTranslatorDlg::OnDblclkLanguages() 
{
	OnTranslate ();
}

void CTranslatorDlg::CopyResources(const CString &strModule, const CString &strResDll)
{
	CResourceArray vModule, vDll;

	if (strModule.Find ("Debug") != -1)
		TRACEL (strModule);
	
	if (strResDll.Find ("Debug") != -1)
		TRACEL (strResDll);

	if (!GetResources (strModule, vModule))
		TRACEL ("GetResources failed: " + strModule);

	if (!GetResources (strResDll, vDll))
		TRACEL ("GetResources failed: " + strResDll);

	if (vModule.GetSize () == vDll.GetSize ()) {
		for (int i = 0; i < vModule.GetSize (); i++) { // TODO: rem
			CResource rModule = vModule [i];
			CResource rDll = vDll [i];

			if (rDll.m_lpType != rModule.m_lpType && rDll.m_lpName != rModule.m_lpName) {
				CString str;

				str.Format ("%s: %d", 
					GetType (rDll.m_lpType),
					rDll.m_lpName);
				TRACEL (str);
				str.Format ("%s: %d", 
					GetType (rModule.m_lpType),
					rModule.m_lpName);
				TRACEL (str);
			}
		}
	}


	if (HANDLE hUpdate = ::BeginUpdateResource (strModule, FALSE)) {
		if (HMODULE hDLL = ::LoadLibrary (strResDll)) {
			for (int i = 0; i < vModule.GetSize (); i++) { 
				CResource r = vModule [i];

				if (HRSRC hFind = ::FindResource (hDLL, r.m_lpName, r.m_lpType)) {
					HGLOBAL hGlobal = ::LoadResource (hDLL, hFind);

					if (!hGlobal)
						throw new CUpdateException (_T ("LoadResource failed: ") + strModule);

					LPVOID lpData = ::LockResource (hGlobal);
					DWORD cbData = ::SizeofResource (hDLL, hFind);

					if (!lpData)
						throw new CUpdateException (_T ("LockResource failed") + strModule);

					BOOL bUpdate = ::UpdateResource (hUpdate, r.m_lpType, r.m_lpName,
						MAKELANGID (LANG_NEUTRAL, SUBLANG_NEUTRAL), lpData, cbData);

					if (!bUpdate) {
						CString str;
						str.Format ("UpdateResource failed: [%s] %s, %d",
							strModule,
							GetType (r.m_lpType), 
							r.m_lpName);
						throw new CUpdateException (str);
					}

					::FreeResource (hGlobal);
				}
				else {
					CString str;

					str.Format ("FindResourceEx failed: [%s] %s: %d", 
						strModule,
						GetType (r.m_lpName), 
						r.m_lpType);
					TRACEF (str); 

					// not necessarily an error
					//throw new CUpdateException (str);
				}
			}

			::FreeLibrary (hDLL);
		}
		else 
			throw new CUpdateException ("LoadLibrary failed on: " + strResDll);

		if (!::EndUpdateResource (hUpdate, FALSE)) {
			throw new CUpdateException ("EndUpdateResource failed on: " + strModule + ": [" + FormatError (::GetLastError ()) + "]");
		}
	}
	else 
		throw new CUpdateException ("BeginUpdateResource failed on: " + strModule);
}

BOOL CALLBACK CTranslatorDlg::EnumResNameProc (HMODULE hModule, LPCTSTR lpType, LPTSTR lpName, LONG lParam)
{
	CResourceArray * pv = (CResourceArray *)lParam;

	ASSERT (pv);

	pv->Add (CResource (lpType, lpName));

	return TRUE;
}

BOOL CALLBACK CTranslatorDlg::EnumResTypeProc (HMODULE hModule, LPTSTR lpType, LONG lParam)
{
	if (!::EnumResourceNames (hModule, lpType, EnumResNameProc, lParam)) {
		TRACEL ("EnumResourceNames failed: " + FormatError (::GetLastError ()));
	}

	return TRUE;
}

CString CTranslatorDlg::FormatError (DWORD dwError)
{
	CString str;
	LPVOID lpMsgBuf = NULL;

	::FormatMessage (
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, dwError, MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT), 
		(LPTSTR)&lpMsgBuf, 0, NULL);
	str = (LPCTSTR)lpMsgBuf;
	::LocalFree (lpMsgBuf);

	return str;
}

bool CTranslatorDlg::GetResources(const CString &str, CResourceArray & v)
{
	if (str.Find ("Debug") != -1)
		TRACEL (str);

	if (HMODULE h = ::LoadLibrary (str)) {
		if (LPGETDLLVERSION lpf = (LPGETDLLVERSION)::GetProcAddress (h, FJGETDLLVERSIONPROCADDRESS)) {
			VERSIONSTRUCT ver;

			memset (&ver, 0, sizeof (ver));

			(* lpf) (&ver);

			const CString strModuleVer = Format (ver);
			const CString strAppVer = Format (verApp);

			if (strModuleVer != strAppVer) {
				CString strException;

				strException.Format (_T ("Version numbers don't match:\n"
					"%s (%s)\n"
					"%s (%s)"),
					_T ("Translator"), strAppVer,
					str, strModuleVer);

				throw new CUpdateException (strException);
			}
		}

		if (!::EnumResourceTypes (h, (ENUMRESTYPEPROC)EnumResTypeProc, (LPARAM)&v)) {
			TRACEL ("EnumResourceTypes failed: " + FormatError (::GetLastError ()));
		}

		::FreeLibrary (h);
		return true;
	}

	return false;
}

CString CTranslatorDlg::GetType(LPCTSTR lp)
{
	struct
	{
		LPCTSTR m_lp;
		LPCTSTR m_lpsz;
	} static const map [] = 
	{
		{	RT_ACCELERATOR,		_T ("RT_ACCELERATOR"),		},
		{	RT_ANICURSOR,		_T ("RT_ANICURSOR"),		},
		{	RT_ANIICON,			_T ("RT_ANIICON"),			},
		{	RT_BITMAP,			_T ("RT_BITMAP"),			},
		{	RT_CURSOR,			_T ("RT_CURSOR"),			},
		{	RT_DIALOG,			_T ("RT_DIALOG"),			},
		{	RT_FONT,			_T ("RT_FONT"),				},
		{	RT_FONTDIR,			_T ("RT_FONTDIR"),			},
		{	RT_GROUP_CURSOR,	_T ("RT_GROUP_CURSOR"),		},
		{	RT_GROUP_ICON,		_T ("RT_GROUP_ICON"),		},	
		{	RT_HTML,			_T ("RT_HTML"),				},
		{	RT_ICON,			_T ("RT_ICON"),				},
		{	RT_MENU,			_T ("RT_MENU"),				},	
		{	RT_MESSAGETABLE,	_T ("RT_MESSAGETABLE"),		},	
		{	RT_PLUGPLAY,		_T ("RT_PLUGPLAY"),			},	
		{	RT_RCDATA,			_T ("RT_RCDATA"),			},	
		{	RT_STRING,			_T ("RT_STRING"),			},	
		{	RT_VERSION,			_T ("RT_VERSION"),			},	
		{	RT_VXD,				_T ("RT_VXD"),				},
	};

	for (int i = 0; i < (sizeof (map) / sizeof (map [0])); i++)
		if (map [i].m_lp == lp)
			return map [i].m_lpsz;

	CString str;
	
	str.Format ("%d", lp);
	return str;
}

CString CTranslatorDlg::GetLangDir()
{
	if (m_strLangDir.GetLength ())
		if (m_strLangDir [m_strLangDir.GetLength () - 1] != '\\')
			m_strLangDir += '\\';

	return m_strLangDir;
}


CString CTranslatorDlg::GetAppDir()
{
	if (m_strAppDir.GetLength ())
		if (m_strAppDir [m_strAppDir.GetLength () - 1] != '\\')
			m_strAppDir += '\\';

	return m_strAppDir;
}

void CTranslatorDlg::OnClose() 
{
	CDialog::OnClose();
}

CListCtrl & CTranslatorDlg::GetListCtrl () const
{
	CListCtrl * p = (CListCtrl *)GetDlgItem (LV_COMPONENTS);

	ASSERT (p);

	return * p;
}

void CTranslatorDlg::ResetListCtrlContent ()
{
	CListCtrl & lv = GetListCtrl ();

	for (int i = 0; i < lv.GetItemCount (); i++)
		if (CResFilePair * p = (CResFilePair *)lv.GetItemData (i))
			delete p;

	lv.DeleteAllItems ();
}

void CTranslatorDlg::OnSelchangeLanguages() 
{
	CListBox * pLang = (CListBox *)GetDlgItem (LB_LANGUAGES);
	CListCtrl & lv = GetListCtrl ();
	CString strDir;

	ASSERT (pLang);

	ResetListCtrlContent ();
	GetDlgItemText (TXT_PATH, strDir);

	int nIndex = pLang->GetCurSel ();

	if (nIndex != LB_ERR) {
		if (LANGDIRSTRUCT * p = (LANGDIRSTRUCT *)pLang->GetItemData (nIndex)) {
			CStringArray v, vLang, vDLL;
			CString strLang;
			int nComponents = 0;

			pLang->GetText (nIndex, strLang);

			GetFiles (strDir, _T ("*.exe"), v);

			for (int i = 0; i < v.GetSize (); i++) {
				CStringArray vDepends;

				//TRACEF (strDir + _T (":\t") + v [i]);
				GetDependencies (v [i], vDepends, true); 

				for (int j = 0; j < vDepends.GetSize (); j++) {
					CString str = strDir + _T ("\\") + vDepends [j];

					if (::GetFileAttributes (str) != -1) {
						if (::Find (vDLL, str, true) == -1) {
							//TRACEF (_T ("\t\t") + str);
							vDLL.Add (str);
						}
					}
				}
			}

			{
				CStringArray vTmp;

				for (int i = 0; i < vDLL.GetSize (); i++) 
					::InsertAscending (vTmp, vDLL [i]);

				v.Append (vTmp); 
			}

			//GetFiles (strDir, _T ("*.dll"), v);

			CString strLangDir = GetLangDir () + strLang;
			TRACEF (strLangDir);

			GetFiles (strLangDir, _T ("*.dll"), vLang);

			for (int i = 0; i < v.GetSize (); i++) {
				TRACEF (v [i]);

				CResFile r (v [i]);
				int nIndex = Find (r.m_strTitle, vLang);

				if (nIndex != -1) {
					TRACEF (vLang [nIndex]);

					CResFile rDll (vLang [nIndex]);

					TRACEF (r.m_strTitle + _T (".") + r.m_strExt);
					int nInsert = lv.InsertItem (nComponents++, r.m_strTitle + _T (".") + r.m_strExt);

					lv.SetItemText (nInsert, 1, rDll.m_strTitle + _T (".") + rDll.m_strExt);
					lv.SetItemData (nInsert, (DWORD)new CResFilePair (r, rDll));
				}
			}
		}	
	}
}

void CTranslatorDlg::OnDestroy() 
{
	Free ();
	ResetListCtrlContent ();	
	CDialog::OnDestroy();
}

LRESULT CTranslatorDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	switch (message) {
	case WM_CLOSE:
	case ID_APP_EXIT:
		if (m_hThread != NULL) {
			return 1;
		}
		break;
	}
	
	
	return CDialog::WindowProc(message, wParam, lParam);
}

void CTranslatorDlg::OnBrowse() 
{
	CString str;

	GetDlgItemText (TXT_PATH, str);

	CFileDialog dlg (TRUE, _T ("*.*"), _T ("mkdraw.exe"), 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_NOCHANGEDIR, 
		_T ("Excutables (*.dll; *.exe)|*.dll; *.exe|All files (*.*)|*.*||"), this);

	dlg.m_ofn.lpstrInitialDir = str.GetBuffer (str.GetLength ());

	if (dlg.DoModal () == IDOK) {
		m_strAppDir = str = GetDir (dlg.GetPathName ());
		theApp.WriteProfileString (_T ("Settings"), _T ("AppDir"), str);
		SetDlgItemText (TXT_PATH, str);
		OnSelchangeLanguages ();
	}
}

void CTranslatorDlg::OnBrowselang() 
{
	CString str (m_strLangDir);

	CFileDialog dlg (TRUE, NULL, NULL, 
		/* OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | */ OFN_HIDEREADONLY | OFN_NOCHANGEDIR, 
		_T ("Excutables (*.dll; *.exe)|*.dll; *.exe|All files (*.*)|*.*||"), this);

	dlg.m_ofn.lpstrInitialDir = str.GetBuffer (str.GetLength ());

	if (dlg.DoModal () == IDOK) {
		m_strLangDir = GetDir (dlg.GetPathName ());
		theApp.WriteProfileString (_T ("Settings"), _T ("LangDir"), m_strLangDir);
		FillLangCtrl ();
		OnSelchangeLanguages ();
	}
}

bool CTranslatorDlg::IsLangDir(const CString &str) const
{
	for (int i = 0; i < ARRAYSIZE (::lpszLanguages); i++) 
		if (!str.CompareNoCase (::lpszLanguages [i]))
			return true;

	return false;
}

void CTranslatorDlg::OnDblclkComponents(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CListCtrl & lv = GetListCtrl ();

	for (int i = 0; i < lv.GetItemCount (); i++) {
		if (lv.GetItemState (i, LVIS_SELECTED)) {
			CString str;

			str += lv.GetItemText (i, 0) + _T ("\n");
			str += lv.GetItemText (i, 1) + _T ("\n");
			str += lv.GetItemText (i, 2);

			MessageBox (str);
			return;
		}
	}
	
	*pResult = 0;
}

void CTranslatorDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	if (bShow) 
		AlignToKeyboard (this);
}
