// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__D165BF3B_2AAD_4166_8335_84D72AD81D09__INCLUDED_)
#define AFX_STDAFX_H__D165BF3B_2AAD_4166_8335_84D72AD81D09__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

void Trace (LPCTSTR psz, LPCTSTR pszFile, ULONG nLine);

#define TRACEF(s) Trace ((s), __FILE__, __LINE__)

CString Extract (const CString & strParse, const CString & strBegin);
CString Extract (const CString & strParse, const CString & strBegin, const CString & strEnd);
HWND FindKeyboard ();
void AlignToKeyboard (CWnd * pwnd);
DWORD GetDependencies (const CString & strFilename, CStringArray & v, bool bFjOnly = true);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__D165BF3B_2AAD_4166_8335_84D72AD81D09__INCLUDED_)
