
#include "stdafx.h"
#include "Parse.h"
#include "TemplExt.h"

using namespace FoxjetDatabase;
using namespace FoxjetCommon;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

static const LPCTSTR lpszVALVESTRUCT			= _T ("VALVESTRUCT");

////////////////////////////////////////////////////////////////////////////////

CString FoxjetDatabase::ToString (ULONG lVal) 
{
	CString str;
	str.Format (_T ("%u"), lVal);
	return str;
}

CString FoxjetDatabase::ToString (int nVal) 
{
	return ToString ((ULONG)nVal);
}

CString FoxjetDatabase::ToString (bool bVal) 
{
	return bVal ? _T ("true") : _T ("false");
}

CString FoxjetDatabase::ToString (double dVal) 
{
	CString str;
	str.Format (_T ("%f"), dVal);
	return str;
}

CString FoxjetDatabase::ToString (__int64 iVal)
{
	CString str;
	str.Format (_T ("%I64d"), iVal);
	return str;
}

CString FoxjetDatabase::Tokenize(const CString &strInput, CStringArray &v, TCHAR cDelim, bool bTrim)
{
	#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
	TRACEF ("Tokenize (" + strInput + ")");
	#endif //_DEBUG && __TRACE_TOFROMSTRING__
	
	CString str = bTrim ? Trim (strInput) : strInput;
	CLongArray commas = CountChars (str, cDelim);
	int nIndex = 0;

	v.RemoveAll ();

	for (int i = 0; i < commas.GetSize (); i++) {
		int nLen = commas [i] - nIndex;
		v.Add (str.Mid (nIndex, nLen));
		nIndex = commas [i] + 1;
	}

	CString strRemainder = str.Mid (nIndex);

	if (strRemainder.GetLength ())
		v.Add (strRemainder);
	else {
		if (strInput.GetLength () && strInput [strInput.GetLength () - 1] == cDelim)
			v.Add (_T (""));
	}

	#if defined( _DEBUG ) && defined( __SHOW_TOKENS__ )
	{
		CString strDebug;

		for (int i = 0; i < v.GetSize (); i++) {
			strDebug.Format ("v [%u] = \"%s\"%s", 
				i, v [i], (i + 1 == v.GetSize ()) ? "\n" : "");
			TRACEF (strDebug);
		}
	}
	#endif //_DEBUG && __SHOW_TOKENS__

	return v.GetSize () ? v [0] : _T ("");
}

FoxjetCommon::CLongArray FoxjetDatabase::CountChars (const CString & str, TCHAR c)
{
	CLongArray index;

	for (int i = 0; i < str.GetLength (); i++) {
		TCHAR cCur = str [i];

		if (cCur == '\\') {
			i++;
			continue;
		}
		else if (cCur == c) 
			index.Add (i);
	}
/*
	TCHAR cCur, cPrev = '\0';

	for (int i = 0; i < str.GetLength (); i++) {
		cCur = str [i];

		if (cCur == '\\') {
			i++;
			continue;
		}
		else if (cCur == c) 
			index.Add (i);

		cPrev = cCur;
	}
*/

	return index;
}

CString FoxjetDatabase::FormatString (const CString &str)
{
	CString strResult (str);
	TCHAR cDelim [] = { '\\', ',', '{', '}', 0 }; // the '\' char must be first

	strResult.Replace (_T ("\\u"), _T ("<u>"));
	strResult.Replace (_T ("\\U"), _T ("<U>"));
	strResult.Replace (_T ("\r\n"), _T ("<CR>"));

	for (int i = 0; cDelim [i] != 0; i++) {
		int nIndex = 0;
	
		do {
			nIndex = strResult.Find (cDelim [i], nIndex);

			if (nIndex != -1) {
				strResult.Insert (nIndex, '\\');
				nIndex += 2;
			}
		}
		while (nIndex != -1);
	}


	for (int i = strResult.GetLength () - 1; i >= 0; i--) {
		TCHAR c = strResult [i];

		if (c > 255) {
			CString strEncode;
			//int a = HIBYTE (c);
			//int b = LOBYTE (c);

			strEncode.Format (_T ("\\u%04X"), c);
			//strEncode.Format (_T ("\\C%c%c"), a, b);

			strResult.Delete (i);
			strResult.Insert (i, strEncode);
		}
	}

	//TRACEF (str + " --> " + strResult);

	return strResult;
}

CString FoxjetDatabase::UnformatString(const CString &strSrc)
{
	CString strResult, str (strSrc);

	for (int i = 0; i < str.GetLength (); i++) {
		if (str [i] == '\\') {
			i++;

			if (i >= str.GetLength ())
				break;

			if ((i + 4) < str.GetLength ()) {
				if (str [i] == 'u' || str [i] == 'U') {
					CString strEncode = str.Mid (i + 1, 4);
					TCHAR cEncode = (TCHAR)_tcstoul (strEncode, NULL, 16);
					strResult += cEncode;
					i += 4;
					continue;
				}
			}

			if ((i + 2) < str.GetLength ()) {
				if (str [i] == 'c' || str [i] == 'C') {
					CString strEncode = str.Mid (i + 1, 2);
					int a = strEncode [0];
					int b = strEncode [1];
					TCHAR cEncode = (TCHAR)MAKEWORD (b, a);

					strResult += cEncode;
					i += 2;
					continue;
				}
			}
		}

		strResult += str [i];
	}

	strResult.Replace (_T ("<CR>"), _T ("\r\n"));
	strResult.Replace (_T ("<u>"), _T ("\\u"));
	strResult.Replace (_T ("<U>"), _T ("\\U"));

	return strResult;
}

CString FoxjetDatabase::Trim (CString str)
{
	str.TrimLeft ();
	str.TrimRight ();

	if (str.GetLength () && str [0] == '{')
		str.Delete (0);

	int nLen = str.GetLength ();

	if (str.GetLength () > 2 && str [nLen - 2] != '\\' && str [nLen - 1] == '}')
		str.Delete (str.GetLength () - 1);

	return str;
}

CString FoxjetDatabase::Extract (const CString & strParse, const CString & strBegin)
{
	CString str;
	int nIndex = strParse.Find (strBegin);

	if (nIndex != -1) 
		str = strParse.Mid (nIndex + strBegin.GetLength ());

	return str;
}

CString FoxjetDatabase::Extract (const CString & strParse, const CString & strBegin, const CString & strEnd)
{
	CString str;
	int nIndex [2] = 
	{
		strParse.Find (strBegin),

		// start looking for strEnd after strBegin
		strParse.Find (strEnd, (nIndex [0] == -1) ? 0 : nIndex [0] + strBegin.GetLength ()), 
	};

	if (nIndex [0] != -1 && nIndex [1] != -1) {
		int nStart = nIndex [0] + strBegin.GetLength ();
		int nLen = nIndex [1] - nStart;

		if (nLen > 0)
			str = strParse.Mid (nStart, nLen);
	}

	return str;
}

CString FoxjetDatabase::ToString (const CStringArray & v) 
{
	CString str;

	for (int i = 0; i < v.GetSize (); i++) {
		str += FormatString (v [i]);// + _T (",");

		if ((i + 1) < v.GetSize ())
			str += _T (",");
	}

	return _T ("{") + str + _T ("}");
}

int FoxjetDatabase::FromString (const CString & strInput, CStringArray & v)
{
	const int nCount = v.GetSize ();
	CString str = Trim (strInput);

	Tokenize (str, v);

	for (int i = nCount; i < v.GetSize (); i++) 
		v [i] = UnformatString (v [i]);

	return v.GetSize () - nCount;
}


void FoxjetDatabase::ParsePackets (const CString & str, CStringArray & v)
{
	CLongArray vLeft = CountChars (str, '{');
	CLongArray vRight = CountChars (str, '}');
	CLongArray vDelim = MergeAcending (vLeft, vRight);
	int nCount = 0;

	for (int i = 0; i < vDelim.GetSize (); i++) {
		int nIndex = vDelim [i];

		if (str [nIndex] == '{')
			nCount++;
		else if (str [nIndex] == '}')
			nCount--;

		if (nCount == 0) {
			int nFirst = vLeft [0] + 1;
			int nLast = nIndex;
			int nLen = nLast - nFirst;
			CString strSegment = str.Mid (nFirst, nLen);

			v.Add (strSegment);

			if (nLast < str.GetLength () - 1) {
				CString strRemainder = str.Mid (nLast + 1);
				
				ParsePackets (strRemainder, v);
			}

			return;
		}
	}
}

CString FoxjetDatabase::GetKey (const CString & strPacket)
{
	CStringArray v;
	CString str;

	Tokenize (strPacket, v);

	if (v.GetSize ())
		str = v [0];

	return str;
}

void FoxjetDatabase::ExtractPackets (const CString & strKey, const CString & str, CStringArray & v)
{
	CStringArray vTmp;

	ParsePackets (str, vTmp);

	for (int i = 0; i < vTmp.GetSize (); i++) 
		if (!strKey.CompareNoCase (GetKey (vTmp [i])))
			v.Add (vTmp [i]);
}

CString FoxjetDatabase::SetPackets (const CString & strPacket, const CString & strData)
{
	CString str;
	CStringArray vPackets;
	const CString strKey = GetKey (strPacket);
	bool bFound = false;

	ParsePackets (strData, vPackets);

	for (int i = 0; i < vPackets.GetSize (); i++) {
		if (!strKey.CompareNoCase (GetKey (vPackets [i]))) { 
			str += strPacket;
			bFound = true;
		}
		else 
			str += '{' + vPackets [i] + '}';
	}

	if (!bFound)
		str += strPacket;

	return str;
}

CString FoxjetDatabase::GetParam (const CStringArray & v, int nIndex, const CString & strDef)
{
	CString strResult = strDef;

	if (nIndex >= 0 && nIndex < v.GetSize ()) 
		strResult = v [nIndex];

	return strResult;
}

////////////////////////////////////////////////////////////////////////////////

CString FoxjetDatabase::TokenizeCmdLine (const CString &strInput, CStringArray &v)
{
	CString strSeg, str (strInput);

	while (str.GetLength ()) {
		TCHAR c = str [0];

		if (c == '\"') {
			CString strTmp = FoxjetDatabase::Extract (str, _T ("\""), _T ("\""));
			
			if (strTmp.GetLength ()) {
				int nLen = strTmp.GetLength () + 2;

				strSeg += _T ("\"") + strTmp + _T ("\"");
	
				if (nLen <= str.GetLength ())
					str.Delete (0, nLen);
				
				v.Add (strSeg);
				strSeg.Empty ();
			}

			continue;
		}
		else if (c == ' ') {
			if (strSeg.GetLength ()) {
				v.Add (strSeg);
				strSeg.Empty ();
			}
		}
		else 
			strSeg += c;

		str.Delete (0);
	}

	if (strSeg.GetLength ())
		v.Add (strSeg);

	return v.GetSize () ? v [0] : _T ("");
}
