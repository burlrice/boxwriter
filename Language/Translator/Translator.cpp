// Translator.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Translator.h"
#include "TranslatorDlg.h"
#include "TemplExt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

bool bMatrix = false;

bool Elevate ()
{
	OSVERSIONINFO osver = { sizeof(osver) };

	::GetVersionEx (&osver);

	if (osver.dwMajorVersion >= 6 && osver.dwMinorVersion > 0) { // Windows7
		CString str = ::GetCommandLine ();

		if (str.Find (_T ("/runas_elevated")) == -1) {
			SHELLEXECUTEINFO sei = { 0 };
			TCHAR sz [MAX_PATH] = { 0 };

			::GetModuleFileName (NULL, sz, ARRAYSIZE (sz));
			str.Replace ('"' + CString (sz) + '"', _T (""));
			str.Replace (sz, _T (""));
			str += CString (_T (" /runas_elevated"));

			TRACEF (sz);
			TRACEF (str);

			sei.cbSize = sizeof (sei);
			sei.lpVerb = _T ("runas");
			sei.lpFile = sz;
			sei.lpParameters = str;
			sei.nShow = SW_NORMAL;

			::ShellExecuteEx (&sei);
			exit (0);

			return false;
		}
	}

	return true;
}

CString GetTitle (const CString & strFullPath)
{
	TCHAR szTitle [MAX_PATH + 1] = { 0 };

	::GetFileTitle (strFullPath, szTitle, ARRAYSIZE (szTitle) - 1);
	
	return szTitle;
}

CString GetDir (const CString & strFullPath)
{
	TCHAR szTitle [MAX_PATH + 1] = { 0 };

	::GetFileTitle (strFullPath, szTitle, ARRAYSIZE (szTitle) - 1);
	
	CString str (strFullPath);
	
	if (TCHAR * p = strstr (szTitle, _T ("."))) 
		* p = '\0';

	int nIndex = str.Find (szTitle);

	if (nIndex != -1) 
		str = str.Left (nIndex);

	return str;
}

CString GetDir ()
{
	TCHAR sz [MAX_PATH] = { 0 };

	HMODULE hModule = ::GetModuleHandle (NULL);
	::GetModuleFileName (hModule, sz, ARRAYSIZE (sz) - 1);

	const CString strPath (sz);
	CString strPathLower (sz);
	strPathLower.MakeLower ();

	VERIFY (::GetFileTitle (strPath, sz, ARRAYSIZE (sz) - 1) == 0);
	
	CString strTitle (sz);
	strTitle.MakeLower ();

	int nIndex = strPathLower.Find (strTitle);

	ASSERT (nIndex != -1);
	CString str = strPath.Left (nIndex - 1);

	return str;
}

void TraceAndLog (const CString & strTrace, LPCTSTR lpszFile, ULONG lLine)
{
	static bool bInit = false;

#ifdef _DEBUG
	Trace (strTrace, lpszFile, lLine);
#endif

	CString str;
	
	str.Format ("%s(%d): %s\n",
		lpszFile,
		lLine, 
		strTrace);

	if (!bInit) {
		str = CTime::GetCurrentTime ().Format ("\n\n----- Log started: %c -----\n") + str;
		bInit = true;
	}

	if (FILE * fp = _tfopen (CTranslatorApp::m_strLogfile, _T ("a"))) {
		fwrite ((LPVOID)(LPCTSTR)str, str.GetLength (), 1, fp);
		fclose (fp);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CTranslatorApp

BEGIN_MESSAGE_MAP(CTranslatorApp, CWinApp)
	//{{AFX_MSG_MAP(CTranslatorApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTranslatorApp construction

const CString CTranslatorApp::m_strLogfile = GetDir () + "\\Translator error log.txt";

CTranslatorApp::CTranslatorApp()
{
/*
	if (m_strLogfile.GetLength ()) {
		::DeleteFile (m_strLogfile);
	}
*/
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CTranslatorApp object

CTranslatorApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CTranslatorApp initialization

BOOL CTranslatorApp::InitInstance()
{
	if (!Elevate ())
		return FALSE;

	CCommandLineInfo cmdInfo;
	
	ParseCommandLine (cmdInfo);

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	SetRegistryKey (_T ("Foxjet"));
	SetDialogBkColor (FoxjetUtils::GetButtonColor (_T ("background")));

	CTranslatorDlg dlg;
	m_pMainWnd = &dlg;

	{
		CString str = ::GetCommandLine ();

		str.MakeLower ();

		bMatrix = str.Find (_T ("/matrix")) != -1 ? true : false;
	}

	dlg.m_strLaunch = cmdInfo.m_strFileName;

	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
