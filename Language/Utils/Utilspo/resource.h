//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Utils.rc
//
#define IDS_MINMAXCHARS                 1
#define IDS_STRING2                     2
#define BTN_RESETSCANNER                3
#define BTN_RESETINFO                   4
#define BTN_CLIPBOARD                   10
#define TXT_ZOOM                        101
#define IDD_ZOOM                        102
#define IDC_ZOOMSLIDE                   103
#define IDM_ABOUT                       105
#define IDD_LICENSE                     106
#define IDD_COMPONENTS                  107
#define IDD_FIXEDSCANNER                109
#define IDD_STARTUP                     110
#define IDD_WATCHDOGTIMEOUT             112
#define IDD_HEADTIMEOUT                 113
#define IDD_KEYBOARD                    114
#define IDA_KEYBOARD                    115
#define IDB_OK                          116
#define IDB_CANCEL                      117
#define IDB_UNCHECKED                   118
#define IDB_CHECKED                     119
#define IDB_EXCLAMATION                 120
#define IDB_INFORMATION                 121
#define IDB_QUESTION                    122
#define IDB_ERROR                       123
#define IDB_CLIPBOARD                   124
#define IDB_RESUME                      125
#define IDB_NO                          126
#define IDR_ACCELERATOR1                128
#define IDR_EXCEL1                      129
#define IDR_MANUAL_XLS                  129
#define IDR_7Z_DLL                      135
#define IDR_7Z_EXE                      136
#define IDR_7ZIP_DLL                    137
#define IDB_RESTORE                     138
#define IDR_EXE1                        139
#define IDR_PDF_TO_EXE                  139
#define IDD_COM_PROPERTIES              162
#define LBL_MIN                         285
#define LBL_MAX                         286
#define IDD_ABOUTBOX                    1000
#define IDI_ICON                        1000
#define TXT_KEY                         1001
#define TXT_DATE                        1002
#define LBL_LINE                        1002
#define TXT_NOREADSTRING                1003
#define LBL_ENCODING                    1003
#define TXT_NOREADS                     1004
#define CHK_RESET                       1005
#define CHK_IDLE                        1006
#define IDC_ADDR                        1007
#define TXT_MACADDR                     1008
#define BTN_SET                         1009
#define CB_ARGS                         1010
#define LBL_DEVICETYPE                  1011
#define CB_BAUD                         1012
#define CHK_SCANNERUSEONCE              1013
#define CHK_AUTOSTART                   1014
#define CHK_USERDATA                    1015
#define RDO_MOTHERBOARD1                1016
#define CHK_SERIALDATA                  1016
#define RDO_MOTHERBOARD2                1017
#define TXT_WATCHDOGTIMEOUT             1018
#define BTN_WATCHDOGTIMEOUT             1019
#define TXT_HEADTIMEOUT                 1020
#define CB_TIMEOUT                      1020
#define BTN_HEADTIMEOUT                 1021
#define TXT_TIMEOOUT                    1021
#define CHK_ENABLED                     1022
#define CHK_LOCALDB_ENABLED             1023
#define BTN_1_1                         1024
#define BTN_2_1                         1025
#define BTN_3_1                         1026
#define BTN_4_1                         1027
#define BTN_5_1                         1028
#define BTN_6_1                         1029
#define BTN_7_1                         1030
#define BTN_8_1                         1031
#define BTN_9_1                         1032
#define BTN_10_1                        1033
#define BTN_11_1                        1034
#define BTN_12_1                        1035
#define BTN_13_1                        1036
#define BTN_14_1                        1037
#define BTN_1_7                         1037
#define BTN_15_1                        1038
#define BTN_2_7                         1038
#define BTN_1_2                         1039
#define BTN_3_7                         1039
#define BTN_REFRESH                     1039
#define BTN_2_2                         1040
#define BTN_4_7                         1040
#define LV_ADDR                         1040
#define BTN_3_2                         1041
#define BTN_5_7                         1041
#define BTN_SAVE                        1041
#define CHK_USEONCE                     1041
#define BTN_4_2                         1042
#define BTN_6_7                         1042
#define CHK_SUPPRESS                    1042
#define BTN_5_2                         1043
#define BTN_7_7                         1043
#define TXT_SYSID                       1043
#define BTN_6_2                         1044
#define BTN_8_7                         1044
#define LBL_OFFSET                      1044
#define BTN_7_2                         1045
#define BTN_9_7                         1045
#define LBL_START                       1045
#define BTN_8_2                         1046
#define BTN_10_7                        1046
#define LBL_END                         1046
#define BTN_9_2                         1047
#define BTN_11_7                        1047
#define TXT_FORMAT1                     1047
#define BTN_10_2                        1048
#define BTN_12_7                        1048
#define LBL_FORMAT1                     1048
#define BTN_11_2                        1049
#define BTN_13_7                        1049
#define TXT_FORMAT2                     1049
#define BTN_12_2                        1050
#define BTN_1_8                         1050
#define LBL_FORMAT2                     1050
#define BTN_13_2                        1051
#define BTN_2_8                         1051
#define TXT_FORMAT3                     1051
#define BTN_14_2                        1052
#define BTN_3_8                         1052
#define LBL_FORMAT3                     1052
#define BTN_15_2                        1053
#define BTN_4_8                         1053
#define BTN_1_3                         1054
#define BTN_5_8                         1054
#define BTN_2_3                         1055
#define BTN_6_8                         1055
#define BTN_3_3                         1056
#define BTN_7_8                         1056
#define TXT_LOCAL_TIME                  1056
#define TXT_ADDR                        1056
#define BTN_4_3                         1057
#define BTN_8_8                         1057
#define CB_FREQ                         1057
#define BTN_5_3                         1058
#define BTN_9_8                         1058
#define BTN_TEST                        1058
#define BTN_6_3                         1059
#define BTN_10_8                        1059
#define BTN_7_3                         1060
#define BTN_11_8                        1060
#define LBL_STATE                       1060
#define BTN_8_3                         1061
#define BTN_12_8                        1061
#define BTN_BACKUP                      1061
#define BTN_9_3                         1062
#define BTN_13_8                        1062
#define BTN_BACKUP_ALL                  1062
#define BTN_10_3                        1063
#define BTN_1_9                         1063
#define BTN_STOP                        1063
#define BTN_11_3                        1064
#define BTN_2_9                         1064
#define BTN_VIEW                        1064
#define BTN_12_3                        1065
#define BTN_3_9                         1065
#define TXT_TIME                        1065
#define BTN_13_3                        1066
#define BTN_4_9                         1066
#define BTN_DIAGNOSTICS                 1066
#define BTN_14_3                        1067
#define BTN_5_9                         1067
#define BTN_RESTORE                     1067
#define BTN_15_3                        1068
#define BTN_6_9                         1068
#define BTN_1_4                         1069
#define BTN_7_9                         1069
#define TXT_PASSWORD                    1069
#define BTN_2_4                         1070
#define BTN_8_9                         1070
#define BTN_ENABLE                      1070
#define BTN_3_4                         1071
#define BTN_9_9                         1071
#define BTN_DISABLE                     1071
#define BTN_4_4                         1072
#define BTN_10_9                        1072
#define LBL_ENABLED                     1072
#define BTN_5_4                         1073
#define BTN_11_9                        1073
#define BTN_6_4                         1074
#define BTN_12_9                        1074
#define BTN_7_4                         1075
#define BTN_13_9                        1075
#define BTN_8_4                         1076
#define BTN_1_10                        1076
#define BTN_9_4                         1077
#define BTN_2_10                        1077
#define BTN_10_4                        1078
#define BTN_3_10                        1078
#define BTN_11_4                        1079
#define BTN_4_10                        1079
#define BTN_12_4                        1080
#define BTN_5_10                        1080
#define BTN_13_4                        1081
#define BTN_6_10                        1081
#define BTN_14_4                        1082
#define BTN_7_10                        1082
#define BTN_15_4                        1083
#define BTN_8_10                        1083
#define BTN_1_5                         1084
#define BTN_9_10                        1084
#define BTN_2_5                         1085
#define BTN_10_10                       1085
#define BTN_3_5                         1086
#define BTN_11_10                       1086
#define BTN_4_5                         1087
#define BTN_12_10                       1087
#define BTN_5_5                         1088
#define BTN_6_5                         1089
#define BTN_1_11                        1089
#define BTN_7_5                         1090
#define BTN_2_11                        1090
#define BTN_8_5                         1091
#define BTN_3_11                        1091
#define BTN_9_5                         1092
#define BTN_4_11                        1092
#define BTN_10_5                        1093
#define BTN_5_11                        1093
#define BTN_11_5                        1094
#define BTN_12_5                        1095
#define BTN_7_11                        1095
#define BTN_13_5                        1096
#define BTN_14_5                        1097
#define BTN_15_5                        1098
#define BTN_1_6                         1099
#define BTN_2_6                         1100
#define BTN_3_6                         1101
#define BTN_4_6                         1102
#define BTN_13_12                       1102
#define BTN_13_13                       1103
#define BTN_6_6                         1104
#define BTN_7_6                         1105
#define BTN_8_6                         1106
#define BTN_9_6                         1107
#define BTN_10_6                        1108
#define IDI_FJ                          1108
#define BTN_11_6                        1109
#define BTN_12_6                        1110
#define BTN_13_6                        1111
#define BTN_14_6                        1112
#define BTN_15_6                        1113
#define BTN_SHIFT1                      1114
#define BTN_SHIFT2                      1115
#define BTN_CTRL1                       1116
#define BTN_CTRL2                       1117
#define BTN_CAPS                        1118
#define BTN_ALT1                        1119
#define BTN_ALT2                        1120
#define BTN_5_6                         1121
#define LBL_DELAY                       1186
#define LBL_DELAY_SECONDS               1187
#define ID_ELEMENT_01                   1201
#define ID_ELEMENT_02                   1202
#define ID_ELEMENT_03                   1203
#define ID_ELEMENT_04                   1204
#define ID_ELEMENT_05                   1205
#define ID_ELEMENT_06                   1206
#define ID_ELEMENT_07                   1207
#define ID_ELEMENT_08                   1208
#define ID_ELEMENT_09                   1209
#define ID_ELEMENT_10                   1210
#define ID_ELEMENT_11                   1211
#define ID_ELEMENT_12                   1212
#define ID_ELEMENT_13                   1213
#define ID_ELEMENT_14                   1214
#define ID_ELEMENT_15                   1215
#define ID_ELEMENT_16                   1216
#define ID_ELEMENT_17                   1217
#define ID_ELEMENT_18                   1218
#define ID_ELEMENT_19                   1219
#define ID_ELEMENT_20                   1220
#define ID_ELEMENT_21                   1221
#define ID_ELEMENT_22                   1222
#define ID_ELEMENT_23                   1223
#define ID_ELEMENT_24                   1224
#define ID_ELEMENT_25                   1225
#define ID_ELEMENT_26                   1226
#define ID_ELEMENT_27                   1227
#define ID_ELEMENT_28                   1228
#define ID_ELEMENT_29                   1229
#define ID_ELEMENT_30                   1230
#define ID_DEFINE_01                    1301
#define ID_DEFINE_02                    1302
#define ID_DEFINE_03                    1303
#define ID_DEFINE_04                    1304
#define ID_DEFINE_05                    1305
#define ID_DEFINE_06                    1306
#define ID_DEFINE_07                    1307
#define ID_DEFINE_08                    1308
#define ID_DEFINE_09                    1309
#define ID_DEFINE_10                    1310
#define ID_DEFINE_11                    1311
#define ID_DEFINE_12                    1312
#define ID_DEFINE_13                    1313
#define ID_DEFINE_14                    1314
#define ID_DEFINE_15                    1315
#define ID_DEFINE_16                    1316
#define ID_DEFINE_17                    1317
#define ID_DEFINE_18                    1318
#define ID_DEFINE_19                    1319
#define ID_DEFINE_20                    1320
#define ID_DEFINE_21                    1321
#define ID_DEFINE_22                    1322
#define ID_DEFINE_23                    1323
#define ID_DEFINE_24                    1324
#define ID_DEFINE_25                    1325
#define ID_DEFINE_26                    1326
#define ID_DEFINE_27                    1327
#define ID_DEFINE_28                    1328
#define ID_DEFINE_29                    1329
#define ID_DEFINE_30                    1330
#define IDC_BAUD                        1501
#define IDC_DATABITS                    1502
#define IDC_PARITY                      1503
#define CB_USE                          1504
#define IDC_STOPBITS                    1505
#define IDC_DEVICETYPE                  1506
#define CB_LINE                         1507
#define BTN_EDIT                        1508
#define CB_ENCODING                     1508
#define LBL_COPYRIGHT                   1514
#define LBL_COMPANY                     1516
#define LV_RECORDS                      1584
#define TV_COMPONENTS                   1653
#define TXT_PATH                        1659
#define TXT_TITLE                       1661
#define TXT_MODIFIED                    1662
#define TXT_VERSION                     1664
#define CHK_TABLE                       1672
#define CHK_VIEW                        1673
#define CHK_SYSTEMTABLE                 1674
#define CHK_GLOBALTEMPORARY             1675
#define CHK_LOCALTEMPORARY              1676
#define CHK_ALIAS                       1677
#define CHK_SYNONYM                     1678
#define TXT_PARAMS                      1679
#define IDS_NOTHINGSELECTED             1738
#define LBL_TEXT                        2003
#define LBL_BMP                         2004
#define IDD_MSGBOX                      2005
#define IDD_DBFIELD                     2203
#define IDD_DATABASEPARAMS              2251
#define IDD_DATABASEPARAMS_SM           2254
#define IDD_DATABASEPARAMS_MATRIX       2254
#define IDB_NETWORK                     2305
#define IDD_WIN32COMM                   3045
#define CHK_RESETCOUNTS                 3068
#define CHK_CONTINUOUSPHOTOCELL         3087
#define LV_PORTS                        3088
#define IDD_IDSADDR                     3089
#define IDD_CMDLINE                     3090
#define IDD_NET_COM_PROPERTIES          3091
#define CB_DSN                          3092
#define BTN_DSN                         3093
#define CB_TABLE                        3094
#define CB_KEYFIELD                     3095
#define CB_TASKFIELD                    3096
#define BTN_FILTER                      3097
#define CB_START                        3098
#define IDD_WATCHDOG                    3099
#define CB_END                          3099
#define IDD_SW0883                      3100
#define CB_OFFSET                       3100
#define CHK_SAVESERIALDATA              3101
#define IDD_STROBE                      3102
#define LV_ERROR                        3103
#define IDD_STROBE_EDIT                 3104
#define TXT_NAME                        3105
#define IDD_STROBE_EDIT_IP              3106
#define RDO_GREEN_OFF                   3107
#define IDD_ABOUTBOX_SM                 3107
#define IDD_CMDLINE_SM                  3107
#define IDD_CMDLINE_MATRIX              3107
#define RDO_GREEN_SOLID                 3108
#define IDD_COM_PROPERTIES_SM           3108
#define IDD_COM_PROPERTIES_MATRIX       3108
#define RDO_RED_OFF                     3109
#define IDD_COMPONENTS_SM               3109
#define IDD_COMPONENTS_MATRIX           3109
#define IDD_DBFIELD_SM                  3110
#define IDD_DBFIELD_MATRIX              3110
#define IDD_FIXEDSCANNER_SM             3111
#define IDD_FIXEDSCANNER_MATRIX         3111
#define IDD_HEADTIMEOUT_SM              3112
#define IDD_IDSADDR_SM                  3113
#define IDD_LICENSE_SM                  3114
#define IDD_NET_COM_PROPERTIES_SM       3115
#define IDD_STARTUP_SM                  3116
#define IDD_STARTUP_MATRIX              3116
#define IDD_STROBE_SM                   3117
#define IDD_STROBE_MATRIX               3117
#define IDD_STROBE_EDIT_SM              3118
#define IDD_STROBE_EDIT_MATRIX          3118
#define IDD_STROBE_EDIT_IP_SM           3119
#define IDD_SW0858DATABASE_SM           3120
#define IDD_SW0858DATABASE_MATRIX       3120
#define IDD_SW0883_SM                   3121
#define IDD_SW0883_MATRIX               3121
#define IDD_WATCHDOG_SM                 3122
#define IDD_WATCHDOGTIMEOUT_SM          3123
#define IDD_WIN32COMM_SM                3124
#define IDD_WIN32COMM_MATRIX            3124
#define IDD_ZOOM_SM                     3125
#define IDD_NETSTAT                     3126
#define IDD_NETSTAT_SM                  3127
#define IDD_TIMECHANGE                  3128
#define IDD_SYSID                       3129
#define IDD_TIMECHANGE_MATRIX           3130
#define IDD_SYSID_MATRIX                3131
#define IDD_NETWORK                     3132
#define IDD_LOCALDB                     3133
#define IDD_INK_CODE_ENABLE             3134
#define RDO_RED_FLASHING                3200
#define RDO_RED_SOLID                   3201
#define TXT_DELAY                       3202
#define RDO_YELLOW_OFF                  3203
#define RDO_YELLOW_FLASHING             3204
#define RDO_YELLOW_SOLID                3205
#define LV_REPORT                       3206
#define IDC_CLEAR                       3207
#define BTN_EXPORT                      3208
#define CHK_AUTOEXPORT                  3209
#define CHK_ENABLE                      3210
#define ID_FILE_SETTINGS                3217
#define TXT_DAYS                        3218
#define ID_ABOUT_FOXJETCOMPONENTSONLY   40001
#define IDA_F1                          40002
#define IDA_F2                          40003
#define IDA_F3                          40004
#define IDA_F4                          40005
#define IDA_F5                          40006
#define IDA_F6                          40007
#define IDA_F7                          40008
#define IDA_F8                          40009
#define IDA_F9                          40010
#define IDA_F10                         40011
#define IDA_F11                         40012
#define IDA_F12                         40013
#define IDS_OFF                         42097
#define IDS_ON                          42098
#define IDS_FLASHING                    42099
#define IDS_HEADERROR_LOWTEMP           42100
#define IDS_HEADERROR_HIGHVOLTAGE       42101
#define IDS_HEADERROR_LOWINK            42102
#define IDS_HEADERROR_OUTOFINK          42103
#define IDS_IVERROR_BROKENLINE          42104
#define IDS_IVERROR_DISCONNECTED        42105
#define IDS_HPERROR_COMMPORTFAILURE     42106
#define IDS_HEADERROR_DISCONNECTED      42107
#define IDS_HUBERROR_IO                 42108
#define IDS_HEADERROR_READY             42109
#define IDS_HEADERROR_APSCYCLE          42110
#define IDS_ERRORCONDITION              42111
#define IDS_GREENALARM                  42112
#define IDS_REDALARM                    42113
#define IDS_YELLOWALARM                 42114
#define IDS_SERIALSETTINGS              61279
#define IDS_HANDSCANNER                 61280
#define IDS_FIXEDSCANNER                61281
#define IDS_REMOTEDEVICE                61282
#define IDS_HOSTINTERFACE               61283
#define IDS_IMAGEFILES                  61284
#define IDS_BITMAPS                     61285
#define IDS_FILESBMP                    61286
#define IDS_FILESGIF                    61287
#define IDS_FILESJPG                    61288
#define IDS_FILESPNG                    61289
#define IDS_FILESMNG                    61290
#define IDS_FILESICO                    61291
#define IDS_FILESTIF                    61292
#define IDS_FILESTGA                    61293
#define IDS_FILESPCX                    61294
#define IDS_FILESWBMP                   61295
#define IDS_FILESWMF                    61296
#define IDS_FILESJBG                    61297
#define IDS_FILESJP2                    61298
#define IDS_FILESJPC                    61299
#define IDS_FILESPGX                    61300
#define IDS_FILESPNM                    61301
#define IDS_FILESRAS                    61302
#define IDS_PROCESSINGLINE              61303
#define IDS_SYSTEMCONFIG                61308
#define IDS_PORT                        61309
#define IDS_SETTINGS                    61310
#define IDS_USAGE                       61311
#define IDS_NOTUSED                     61312
#define IDS_LINE                        61313
#define IDS_INVALIDMACADDR              61314
#define IDS_FAILEDTOCREATESOCKET        61315
#define IDS_FAILEDTOBINDSOCKET          61316
#define IDS_SETSOCKOPTFAILED            61317
#define IDS_PINGRESULT                  61318
#define IDS_NOTINSTALLED                61319
#define IDS_DBSTART                     61320
#define IDS_DSNHASNOTABLES              61321
#define IDS_NOTABLESELECTED             61322
#define IDS_NOFIELDSELECTED             61323
#define IDS_SEC                         61324
#define IDS_MIN                         61325
#define IDS_DISABLED                    61326
#define IDS_MAXINT64                    61327
#define IDS_MININT64                    61328
#define IDS_ADDRESS                     61329
#define IDS_LOCALADDRESS                61329
#define IDS_PROTOCOL                    61330
#define IDS_REMOTEADDRESS               61331
#define IDS_STATE                       61332
#define IDS_LOCALPORT                   61333
#define IDS_REMOTEPORT                  61334
#define IDS_OWNER                       61335
#define IDS_REPORT_TIMECHANGE           61336
#define IDS_NONE                        61336
#define IDS_ERROR_NETWORK               61337
#define IDS_DATETIME                    61338
#define IDS_ERROR_EP8                   61339
#define IDS_INVALIDTIME                 61340
#define IDS_DATE                        61341
#define IDS_INVALIDDATA                 61341
#define IDS_TASKNAME                    61342
#define IDS_ERROR                       61342
#define IDS_BARCODE                     61343
#define IDS_BUILDHELPINDEX              61343
#define IDS_TOTAL                       61344
#define IDS_HELPFILEMISSING             61344
#define IDS_GOOD                        61345
#define IDS_READERMISSING               61345
#define IDS_CLEARREPORTS                61346
#define IDS_LOCAL_BACKUP                61346
#define IDS_INKCODESENABLED             61346
#define IDS_EXPORTFAILED                61347
#define IDS_INKCODESDISABLED            61347
#define IDS_TIME                        61348
#define IDS_MISMATCHEDVERSIONS          61348
#define IDS_ACTION                      61349
#define IDS_REINSTALL                   61349
#define IDS_USER                        61350
#define IDS_COUNTS                      61351

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        140
#define _APS_NEXT_COMMAND_VALUE         40015
#define _APS_NEXT_CONTROL_VALUE         1073
#define _APS_NEXT_SYMED_VALUE           103
#endif
#endif
