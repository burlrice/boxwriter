// WinVersion.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "WinVersion.h"
#include "WinVersionDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWinVersionApp

BEGIN_MESSAGE_MAP(CWinVersionApp, CWinApp)
	//{{AFX_MSG_MAP(CWinVersionApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWinVersionApp construction

CWinVersionApp::CWinVersionApp()
{
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CWinVersionApp object

CWinVersionApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CWinVersionApp initialization

BOOL CWinVersionApp::InitInstance()
{
	// Standard initialization

	CWinVersionDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
	}
	else if (nResponse == IDCANCEL)
	{
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
