// WinVersionDlg.h : header file
//

#if !defined(AFX_WINVERSIONDLG_H__E501DDE3_0543_43E0_A038_1053ECCEC8F4__INCLUDED_)
#define AFX_WINVERSIONDLG_H__E501DDE3_0543_43E0_A038_1053ECCEC8F4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CWinVersionDlg dialog

class CWinVersionDlg : public CDialog
{
// Construction
public:
	CWinVersionDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CWinVersionDlg)
	enum { IDD = IDD_WINVERSION_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWinVersionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CWinVersionDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WINVERSIONDLG_H__E501DDE3_0543_43E0_A038_1053ECCEC8F4__INCLUDED_)
