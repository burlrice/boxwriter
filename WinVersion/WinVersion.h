// WinVersion.h : main header file for the WINVERSION application
//

#if !defined(AFX_WINVERSION_H__C492B9B4_674B_4D93_BEF5_A0472343E3C6__INCLUDED_)
#define AFX_WINVERSION_H__C492B9B4_674B_4D93_BEF5_A0472343E3C6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CWinVersionApp:
// See WinVersion.cpp for the implementation of this class
//

class CWinVersionApp : public CWinApp
{
public:
	CWinVersionApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWinVersionApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CWinVersionApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WINVERSION_H__C492B9B4_674B_4D93_BEF5_A0472343E3C6__INCLUDED_)
