// WinVersionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "WinVersion.h"
#include "WinVersionDlg.h"
#include "Winbase.h"

#ifdef ARRAYSIZE
	#undef ARRAYSIZE
#endif

#define ARRAYSIZE(s) (sizeof ((s)) / sizeof ((s) [0]))
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWinVersionDlg dialog

CWinVersionDlg::CWinVersionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWinVersionDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWinVersionDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CWinVersionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWinVersionDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CWinVersionDlg, CDialog)
	//{{AFX_MSG_MAP(CWinVersionDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWinVersionDlg message handlers

BOOL CWinVersionDlg::OnInitDialog()
{
	CString str, strFile;
	TCHAR sz [MAX_PATH] = { 0 };

	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	OSVERSIONINFO os = { sizeof (os) };

	::GetVersionEx (&os);
	str.Format (
		_T ("dwOSVersionInfoSize: %d\r\n")
		_T ("dwMajorVersion:      %d\r\n")
		_T ("dwMinorVersion:      %d\r\n")
		_T ("dwBuildNumber:       %d\r\n")
		_T ("dwPlatformId:        %d\r\n")
		_T ("szCSDVersion:        %s\r\n\r\n"), 
		os.dwOSVersionInfoSize,
		os.dwMajorVersion,
		os.dwMinorVersion,
		os.dwBuildNumber,
		os.dwPlatformId,
		os.szCSDVersion);
	::GetModuleFileName (NULL, sz, ARRAYSIZE (sz) - 1);
	strFile = sz;
	
	{
		DWORD dw = ARRAYSIZE (sz);

		ZeroMemory (sz, ARRAYSIZE (sz) * sizeof (sz [0]));
		::GetComputerName (sz, &dw); 
		str = sz + CString (_T ("\r\n")) + str;
	}

	if (!strFile.Replace (_T (".exe"), _T (".txt")))
		strFile += _T (".txt");

	SetDlgItemText (TXT_VERSION, strFile + _T ("\r\n") + str);

	if (FILE * fp = _tfopen (strFile, _T ("a"))) {
		//str.Replace (_T ("\n"), _T ("\r\n"));
		fwrite ((LPVOID)(LPCTSTR)str, str.GetLength (), 1, fp);
		fclose (fp);
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CWinVersionDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CWinVersionDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}
