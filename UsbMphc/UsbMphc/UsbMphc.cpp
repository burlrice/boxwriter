// UsbMphc.cpp : implementation file
//

#include "stdafx.h"
#include "UsbMphc.h"
#include <Afxmt.h>
#include "Typedefs.h"

using namespace USB;

void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	CString str, strFile (lpszFile);
	int nIndex = strFile.ReverseFind ('\\');
	const int nJustify = 20;

	if (nIndex != -1)
		strFile.Delete (0, nIndex + 1);

	if (strFile.GetLength () < nJustify)
		strFile += CString (' ', nJustify - strFile.GetLength ());

	str.Format (_T ("%s(%4d): %s\n"), strFile, lLine, lpsz);
	::OutputDebugString (str);
}

// CUsbMphc

IMPLEMENT_DYNCREATE(CUsbMphc, CWinThread)

CUsbMphc::CUsbMphc()
{
	m_bAutoDelete		= false;
	m_pDevice			= NULL;
	m_hPhotoEvent[0]	= NULL;
	m_hPhotoEvent[1]	= NULL;
	m_hEOPEvent[0]		= NULL;
	m_hEOPEvent[1]		= NULL;
	m_hSerialEvent[0]	= NULL;
	m_hSerialEvent[1]	= NULL;
	memset ( &m_rdRegisters, 0x00, sizeof( Registers ) );
	m_rdPHRegisters[0]	= &m_rdRegisters._Print_0_HeadRegs;
	m_rdPHRegisters[1]	= &m_rdRegisters._Print_1_HeadRegs;

	memset ( &m_wrRegisters, 0x00, sizeof( Registers ) );
	m_wrPHRegisters[0]	= &m_wrRegisters._Print_0_HeadRegs;
	m_wrPHRegisters[1]	= &m_wrRegisters._Print_1_HeadRegs;
}

CUsbMphc::~CUsbMphc()
{
}

BOOL CUsbMphc::InitInstance()
{
	// TODO:  perform and per-thread initialization here
	return TRUE;
}

int CUsbMphc::ExitInstance()
{
	if ( m_pDevice )
		delete m_pDevice;
	return 0;
}

BEGIN_MESSAGE_MAP(CUsbMphc, CWinThread)
END_MESSAGE_MAP()


// CUsbMphc message handlers
typedef struct tagEp8Packet {
	tagEpPktHdr hdr;
	Registers Regs;
}_Ep8Packet;

int CUsbMphc::Run()
{
	CSingleLock sLock ( &m_sync );
	Registers RegData;
	bool done = false;
	unsigned int nEvents = 0x00;
	HANDLE hEvents[4];
	PUCHAR inContext;
	LONG nLength;
	MSG Msg;
	OVERLAPPED ioEp8;
	m_nEp8Cnt = 0;

	hEvents[nEvents++] = m_hExit = CreateEvent(NULL, false, false, NULL);
	ioEp8.hEvent = hEvents[nEvents++] = CreateEvent(NULL, false, false, NULL);
	
	sLock.Lock();
	ReadReg ( 0x0000, &m_wrRegisters, sizeof ( m_wrRegisters) );

	if ( ( m_wrRegisters._MstCtrlReg.CardID == 0x00) || (m_wrRegisters._MstCtrlReg.CardID == 0x10) )
		m_wrRegisters._MstCtrlReg.CardID = 0x01;
	else
		m_wrRegisters._MstCtrlReg.CardID = 0x01;

	m_wrRegisters._MstCtrlReg.AutoStatus = 0x01;
	WriteReg ( 0x0000, &m_wrRegisters, sizeof ( MasterCtrlReg ) );

	m_wrRegisters._Print_0_HeadRegs._SetupRegs._IFStatusReg._EnEndPrintEvent	= 0x01;
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._IFStatusReg._EnPhotoEvent		= 0x01;
	
	m_wrRegisters._Print_1_HeadRegs._SetupRegs._IFStatusReg._EnEndPrintEvent	= 0x00;
	m_wrRegisters._Print_1_HeadRegs._SetupRegs._IFStatusReg._EnPhotoEvent		= 0x00;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Debug Setup.
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._IFCtrlReg._OTBTach			= 0;
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._IFCtrlReg._OTBEdge			= 0;
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._IFCtrlReg._PhotoSrc			= 1;
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._IFCtrlReg._TachSrc			= 1;

	m_wrRegisters._Print_0_HeadRegs._SetupRegs._CtrlReg._DoublePulse		= 0x0000;
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._CtrlReg._HeadType			= 0x0003;
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._CtrlReg._PrintMode			= 0x0000;
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._CtrlReg._GraphicHeadType	= 0x0000;
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._CtrlReg._Inverted			= 0x0000;

//	m_wrRegisters._Print_0_HeadRegs._SetupRegs._IntTachReg._TachGenFreg		= (USHORT) (((300.0 * (60.0 / 5.0 )) * 65535.0) / 1843200.0);
//	m_wrRegisters._Print_0_HeadRegs._SetupRegs._IntTachReg._TachGenFreg		= 0x034B;
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._IntTachReg._TachGenFreg		= 0x0100;
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._IntPhotoReg._PhotoGenFreg	= 0x04B0;
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._ConveyorSpeed._Speed		= ComputeSpeedGateWidth(300);

	m_wrRegisters._Print_0_HeadRegs._ImageRegs._PrintDelay					= 0x00000080;
	m_wrRegisters._Print_0_HeadRegs._ImageRegs._Delay						= 0x00000080;
	m_wrRegisters._Print_0_HeadRegs._ImageRegs._Len							= (ULONG) (150.0 * 8.0);
	
	m_wrRegisters._Print_0_HeadRegs._ImageRegs._SlantRegs._Addr1			= 0x00000000;
	m_wrRegisters._Print_0_HeadRegs._ImageRegs._SlantRegs._Addr2			= 0xFFFFFF7F;
	m_wrRegisters._Print_0_HeadRegs._ImageRegs._SlantRegs._Adjust			= 0x00000000;
	m_wrRegisters._Print_0_HeadRegs._ImageRegs._SlantRegs._Incr				= 0x00000000;

	m_wrRegisters._Print_0_HeadRegs._SetupRegs._FireVibPulse._DampPulseDelay	= 0x0000;
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._FireVibPulse._DampPulseWidth	= 0x0000;
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._FireVibPulse._FirePulseDelay	= 0x0010;
	m_wrRegisters._Print_0_HeadRegs._SetupRegs._FireVibPulse._FirePulseWidth	= 0x0075;
// End Debug Setup.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	memcpy ( m_wrPHRegisters[1], m_wrPHRegisters[0], sizeof (m_wrRegisters._Print_1_HeadRegs) );
	WriteReg ( PH_0_REG_ADDR, m_wrPHRegisters[0], sizeof ( m_wrRegisters._Print_0_HeadRegs ) );
	WriteReg ( PH_1_REG_ADDR, m_wrPHRegisters[1], sizeof ( m_wrRegisters._Print_1_HeadRegs ) );

	// Sync both copies.
	memcpy ( &m_rdRegisters, &m_wrRegisters, sizeof ( m_rdRegisters) );
	
	// Setup pending read for autostatus.
	if ( m_pDevice )
		inContext = m_pDevice->m_pStatusIn->BeginDataXfer((PUCHAR)&RegData, sizeof(RegData), &ioEp8);

	sLock.Unlock();

	while ( !done ) {
		switch ( WaitForMultipleObjects ( nEvents, hEvents, false, INFINITE ) ) {
//		switch ( WaitForMultipleObjects ( nEvents, hEvents, false, 10 ) ) {
			case WAIT_OBJECT_0:
				m_pDevice->m_pStatusIn->Abort();
				m_wrRegisters._MstCtrlReg.AutoStatus = 0x00;
				WriteReg ( 0, &m_wrRegisters, sizeof ( MasterCtrlReg ) );
				return ExitInstance();

				/*
				////////////////////////////////////////////////////////////
				// this block causes the app to hang on shutdown
				ReadReg ( 0x0000, &m_wrRegisters, sizeof ( m_wrRegisters) );

				if ( ( m_wrRegisters._MstCtrlReg.CardID == 0) || (m_wrRegisters._MstCtrlReg.CardID == 1) )
					m_wrRegisters._MstCtrlReg.CardID = 0x01;
				else
					m_wrRegisters._MstCtrlReg.CardID = 0x00;

				m_wrRegisters._MstCtrlReg.AutoStatus = 0x00;
				WriteReg ( 0, &m_wrRegisters, sizeof ( MasterCtrlReg ) );
				return ExitInstance();
				break;
				////////////////////////////////////////////////////////////
				*/

				/*
				////////////////////////////////////////////////////////////
				// this block allows the app to shutdown normally
				// RegData._MstCtrlReg.AutoStatus = 0x00;
				// WriteReg ( 0x0000, &RegData._MstCtrlReg, sizeof ( RegData._MstCtrlReg ) );
				m_pDevice->m_pStatusIn->Abort();
				m_pDevice->m_pCtrlOut->Abort();
				m_pDevice->m_pCtrlIn->Abort();
				return ExitInstance();
				break;
				////////////////////////////////////////////////////////////
				*/

			case WAIT_OBJECT_0 + 0x01:
				sLock.Lock();
				{
					/*
					#ifdef __DBGVIEW__
					CString str, strTmp;
					#endif //__DBGVIEW__
					*/

					ResetEvent ( ioEp8.hEvent );
//					TRACE0("EP8\n");
					m_pDevice->m_pStatusIn->FinishDataXfer( (PUCHAR)&RegData, nLength, &ioEp8,inContext);
		
					/*
					#ifdef __DBGVIEW__
					str.Format (_T ("[%d] "), nLength);
					#endif //__DBGVIEW__
					*/

					if ( nLength == sizeof ( RegData ) ) {
						m_nEp8Cnt++;

						/*
						#ifdef __DBGVIEW__
						ULONG * p = (ULONG *)&RegData;
						
						strTmp.Format (_T ("[%d] "), m_nEp8Cnt); 
						str += strTmp;

						for (int i = 0; i < sizeof (RegData); i++) {
							ULONG lData = p [i];
							strTmp.Format (_T ("0x%p "), lData);
							str += strTmp;
						}
						#endif //__DBGVIEW__
						*/

						memcpy ( &m_rdRegisters, &RegData, sizeof ( m_rdRegisters ) );
						ProcessEventTriggers();

						/*
						#ifdef __DBGVIEW__
						TRACEF (str);
						#endif //__DBGVIEW__
						*/
					}

					inContext = m_pDevice->m_pStatusIn->BeginDataXfer((PUCHAR)&RegData, sizeof(RegData), &ioEp8);
				}
				sLock.Unlock();
				break;
			default:
				sLock.Lock();
				if (ReadReg ( 0x0000, &RegData, sizeof ( RegData) ) ) {
					memcpy ( &m_rdRegisters, &RegData, sizeof ( Registers ) );
				}
				else
					TRACE0("Update Failed\n" );
				sLock.Unlock();

		}
		while ( PeekMessage ( &Msg, 0, 0, 0, PM_NOREMOVE ) )
			PumpMessage();
	}
	return ExitInstance();
}

unsigned int CUsbMphc::ComputeSpeedGateWidth(unsigned short nEncoderRes)
{
   return ( unsigned int )((double) (FTACH_100 * FGATE ) / (double) ( 20 * nEncoderRes));
}

bool CUsbMphc::ProgramFPGA ( const void *pData, const long nLen )
{
	bool result = false;
	unsigned char *p = ( unsigned char *) pData;
	long nTxBytes = 0;
	long nRxBytes = 0;
	long nTotal = nLen;
	long lFailed = 0;
	const long lMaxTries = 20;
	UCHAR Data[512];

	if ( m_pDevice ) {
		if ( m_pDevice->m_pCtrlOut ) {
			m_pDevice->m_pCtrlOut->Abort();
			m_pDevice->m_pCtrlIn->Abort();
			nTxBytes = 512;
			Data[0] = BEGIN_PROG_FPGA;
			Data[1] = 0x00; // Bit Reverse
			m_pDevice->m_pCtrlOut->XferData( Data, nTxBytes );
			nRxBytes = 512;
			memset( Data, 0x00, sizeof(Data));
			if ( m_pDevice->m_pCtrlIn->XferData( Data, nRxBytes ) ) {
				if ( Data[0] == (unsigned char )~END_PROG_FPGA ) {
					return true;	// FPGA is already programmed
				} else {
					TRACE0("Programming FPGA\n");
					if ( Data[0] == (unsigned char )~BEGIN_PROG_FPGA ) {
						while ( nTotal > 0 ) {
							bool bWritten = false;

							Data[0] = ((unsigned char ) WRITE_FPGA);
							if ( nTotal >= 511 ) {
								memcpy ( &Data[1], p, 511 );
								p += 511;
								nTxBytes = 512;
							}
							else {
								memcpy ( &Data[1], p, nTotal );
								nTxBytes = 512;
							}
							if ( m_pDevice->m_pCtrlOut->XferData( Data, nTxBytes ) ) {
								memset ( Data, 0x00, sizeof(Data) );
								nRxBytes = 512;
								if ( m_pDevice->m_pCtrlIn->XferData( Data, nRxBytes ) ) {
									if ( Data[0] == (unsigned char)~WRITE_FPGA ) {
										nTotal -= (nTxBytes - 1);
//										TRACE1(".", nLen - nTotal);
										bWritten = true;
									}
									else
										break;
								}
							}

							if (!bWritten)
								if (lFailed++ >= lMaxTries)
									break;
						}
						
						nTxBytes = 512;
						Data[0] = END_PROG_FPGA;
						m_pDevice->m_pCtrlOut->XferData( Data, nTxBytes );
						nRxBytes = 512;
						if ( m_pDevice->m_pCtrlIn->XferData( Data, nRxBytes ) ) {
							if ( Data[0] == (unsigned char)~END_PROG_FPGA ) {
								result = true;
								TRACE0("Programming Complete\n");
							}
						}
						else
							TRACE0("PROG_END_FAILED\n");
					}
				}
			}
			else {
				TRACE("BEGIN_PROG_FAILED\n");
			}
		}
	}

	return result;
}

inline bool CUsbMphc::WriteReg( unsigned __int16 nRegAddr, PVOID pRegData, ULONG nBytes)
{
	CSingleLock sLock ( &m_sync );
	bool result = false;
	tagEpPktHdr Hdr;
	unsigned char *pBuf, *p;
	long nBufLen;

	if ( m_pDevice ) {
		if ( m_pDevice->m_pPrnCtrlOut ) {
			Hdr.Cmd = CMD_REG_WR;
			Hdr.Addr = nRegAddr;
			Hdr.Len = nBytes / 2;

			nBufLen = nBytes + sizeof ( tagEpPktHdr);
			p = pBuf = new UCHAR[nBufLen];
			memcpy ( p, &Hdr, sizeof ( Hdr ) );
			p += sizeof (Hdr);
			memcpy ( p, pRegData, nBytes );
			
			sLock.Lock();
				result = m_pDevice->m_pPrnCtrlOut->XferData( pBuf, nBufLen );
			sLock.Unlock();
			delete pBuf;
		}
	}
	return result;

}

inline bool CUsbMphc::ReadReg( unsigned __int16 nRegAddr, PVOID pRegData, ULONG nBytes)
{

	bool result = false;
	tagEpPktHdr Hdr;
	unsigned char *pBuf, *p;
	long nBufLen;

	if ( m_pDevice ) {
		if ( m_pDevice->m_pPrnCtrlOut ) {
			Hdr.Cmd = CMD_REG_RD;
			Hdr.Addr = nRegAddr;
			Hdr.Len = nBytes / 2;

			nBufLen = sizeof ( tagEpPktHdr);
			p = pBuf = new UCHAR[nBufLen];
			memcpy ( p, &Hdr, sizeof ( Hdr ) );	
			if ( (result = m_pDevice->m_pPrnCtrlOut->XferData( pBuf, nBufLen )) )
				result = m_pDevice->m_pPrnCtrlIn->XferData( (PUCHAR)pRegData, (LONG &)nBytes );
			delete pBuf;
		}
	}
	return result;
}

bool CUsbMphc::WriteImage(unsigned int nHead, PVOID pData, ULONG nStartLoc, ULONG nBytes)
{
	CSingleLock sLock ( &m_sync );
	bool result = false;
	tagEpPktHdr Hdr;
	PUCHAR pBuf, p;
	long nBufLen;

	if ( m_pDevice ) {
		if ( m_pDevice->m_pPrnCtrlOut ) {

			if ( nBytes % IMG_TX_BOUNDARY )	// Image operations must be mutiple of 16 long words ( 64 bytes )
				nBytes += (IMG_TX_BOUNDARY - (nBytes % IMG_TX_BOUNDARY ));

			nBufLen = nBytes + sizeof ( tagEpPktHdr);
			p = pBuf = new UCHAR[ nBufLen ];
			
			switch ( nHead ) {
			case 0x00:
				Hdr.Addr	= ( PH_0_IMAGE_ADDR + nStartLoc );
				break;
			case 0x01:
				Hdr.Addr	= ( PH_1_IMAGE_ADDR + nStartLoc );
				break;
			}
			
			Hdr.Cmd		= CMD_IMG_WR;
			Hdr.Len		= (nBytes / 2);

			memcpy ( p, &Hdr, sizeof ( Hdr ) );
			p += sizeof (Hdr);
			memcpy ( p, pData, nBytes );
			sLock.Lock();
				result = m_pDevice->m_pPrnCtrlOut->XferData( pBuf, nBufLen );
			sLock.Unlock();
			delete pBuf;
		}
	}
	return result;
}

bool CUsbMphc::ReadImage(unsigned int nHead, PVOID pData, ULONG nStartLoc, ULONG &nBytes)
{
	CSingleLock sLock ( &m_sync );

	bool result = false;
	tagEpPktHdr Hdr;
	long nBufLen;
	unsigned long nLocalBytes = nBytes;

	if ( m_pDevice ) {
		if ( m_pDevice->m_pPrnCtrlOut ) {
			
			if ( nLocalBytes % IMG_TX_BOUNDARY )	// Image operations must be mutiple of 16 long words ( 64 bytes )
				nLocalBytes += (IMG_TX_BOUNDARY - (nLocalBytes % IMG_TX_BOUNDARY ));

			switch ( nHead ) {
			case 0x00:
				Hdr.Addr	= ( PH_0_IMAGE_ADDR + nStartLoc );
				break;
			case 0x01:
				Hdr.Addr	= ( PH_1_IMAGE_ADDR + nStartLoc );
				break;
			}
			
			Hdr.Cmd		= CMD_IMG_RD;
			Hdr.Len		= ( nLocalBytes / 2 );

			nBufLen = sizeof ( tagEpPktHdr );
			sLock.Lock();
			if ( (result = m_pDevice->m_pPrnCtrlOut->XferData( (PUCHAR)&Hdr, nBufLen )) ) {
				unsigned char *pTemp = new UCHAR[nLocalBytes];
				result = m_pDevice->m_pPrnCtrlIn->XferData( ( PUCHAR )pTemp, (LONG&)nLocalBytes );
				if ( result ) {
					memcpy ( pData, pTemp, nBytes );
				}
				delete pTemp;
			}
			sLock.Unlock();
		}
	}
	return result;
}

bool CUsbMphc::ClearPrintBuffer(unsigned int nHead, ULONG nStartLoc, ULONG nBytes)
{
	CSingleLock sLock ( &m_sync );
	sLock.Lock();
	bool result = false;
	tagEpPktHdr Hdr;
	long nBufLen;

	if ( m_pDevice ) {
		if ( m_pDevice->m_pPrnCtrlOut ) {

			if ( nBytes % IMG_TX_BOUNDARY )	// Image operations must be mutiple of 16 long words ( 64 bytes )
				nBytes += (IMG_TX_BOUNDARY - (nBytes % IMG_TX_BOUNDARY ));

			switch ( nHead ) {
			case 0x00:
				Hdr.Addr	= ( PH_0_IMAGE_ADDR + nStartLoc );
				break;
			case 0x01:
				Hdr.Addr	= ( PH_1_IMAGE_ADDR + nStartLoc );
				break;
			}
			
			Hdr.Cmd		= CMD_CLR_IMG;
			Hdr.Len		= (nBytes / 2);
			nBufLen = sizeof ( tagEpPktHdr );
			result = m_pDevice->m_pPrnCtrlOut->XferData( (PUCHAR)&Hdr, nBufLen );
		}
	}
	return result;
}

bool CUsbMphc::WriteSetup(unsigned int nHead, SetupRegs tSetup, FireVibPulse tPulseRegs)
{
	CSingleLock sLock ( &m_sync );
	sLock.Lock();

	bool result = false;
	unsigned __int16 nRegAddr;


	memcpy ( &m_wrPHRegisters[nHead]->_SetupRegs, &tSetup, sizeof ( SetupRegs ) );
	memcpy ( &m_wrPHRegisters[nHead]->_SetupRegs._FireVibPulse, &tPulseRegs, sizeof ( FireVibPulse ) );
	switch ( nHead ) {
	case 1:
		nRegAddr = PH_1_REG_ADDR;
		break;
	case 0:
	default:
		nRegAddr = PH_0_REG_ADDR;
	}
	result = WriteReg ( nRegAddr, m_wrPHRegisters[nHead], sizeof(PrintHeadRegs) );
	return result;
}

bool CUsbMphc::EnablePrint(unsigned int nHead, bool bEnabled)
{
	CSingleLock sLock ( &m_sync );
	sLock.Lock();

	bool result = false;
	unsigned __int16 nRegAddr;

	{ 
		CString str;

		str.Format (_T ("EnablePrint: %d, %d"), nHead, bEnabled);
		::OutputDebugString (str);
	}

	m_wrPHRegisters[nHead]->_SetupRegs._CtrlReg._PrintEnable = bEnabled;
	switch ( nHead ) {
	case 1:
		nRegAddr = PH_1_REG_ADDR;
		break;
	case 0:
	default:
		nRegAddr = PH_0_REG_ADDR;
	}
	result = WriteReg ( nRegAddr, m_wrPHRegisters[nHead], sizeof(PrintHeadRegs) );
	return result;
}

bool CUsbMphc::ProgramSlantRegs(unsigned int nHead, SlantRegs SlantRegs)
{
	CSingleLock sLock ( &m_sync );
	sLock.Lock();

	bool result = false;
	unsigned __int16 nRegAddr;

	memcpy ( &m_wrPHRegisters[nHead]->_ImageRegs._SlantRegs, &SlantRegs, sizeof ( SlantRegs ) );
	switch ( nHead ) {
	case 1:
		nRegAddr = PH_1_REG_ADDR;
		break;
	case 0:
	default:
		nRegAddr = PH_0_REG_ADDR;
	}
	result = WriteReg ( nRegAddr, m_wrPHRegisters[nHead], sizeof(PrintHeadRegs) );
	return result;
}

bool CUsbMphc::SetImageLen(unsigned int nHead, unsigned short wImageCols)
{
	CSingleLock sLock ( &m_sync );
	sLock.Lock();

	bool result = false;
	unsigned __int16 nRegAddr;

	m_wrPHRegisters[nHead]->_ImageRegs._Len = wImageCols;
	switch ( nHead ) {
	case 1:
		nRegAddr = PH_1_REG_ADDR;
		break;
	case 0:
	default:
		nRegAddr = PH_0_REG_ADDR;
	}
	result = WriteReg ( nRegAddr, m_wrPHRegisters[nHead], sizeof(PrintHeadRegs) );

	return result;
}

bool CUsbMphc::SetPrintDelay(unsigned int nHead, unsigned short wDelay)
{
	CSingleLock sLock ( &m_sync );
	sLock.Lock();

	bool result = false;
	unsigned __int16 nRegAddr;

	m_wrPHRegisters[nHead]->_ImageRegs._PrintDelay = wDelay;
	switch ( nHead ) {
	case 1:
		nRegAddr = PH_1_REG_ADDR;
		break;
	case 0:
	default:
		nRegAddr = PH_0_REG_ADDR;
	}
	result = WriteReg ( nRegAddr, m_wrPHRegisters[nHead], sizeof(PrintHeadRegs) );

	return result;
}

bool CUsbMphc::SetProductLen(unsigned int nHead, unsigned short wProdLen)
{
	CSingleLock sLock ( &m_sync );
	sLock.Lock();

	bool result = false;
	unsigned __int16 nRegAddr;

	m_wrPHRegisters[nHead]->_ImageRegs._Delay = wProdLen;
	switch ( nHead ) {
	case 1:
		nRegAddr = PH_1_REG_ADDR;
		break;
	case 0:
	default:
		nRegAddr = PH_0_REG_ADDR;
	}
	result = WriteReg ( nRegAddr, m_wrPHRegisters[nHead], sizeof(PrintHeadRegs) );

	return result;
}

bool CUsbMphc::SetSerialParams(unsigned int nHead, tagSerialParams Params)
{
	CSingleLock sLock ( &m_sync );
	sLock.Lock();

	bool result = false;
	return result;
}

bool CUsbMphc::SetSpeedGateWidth(unsigned int nHead, unsigned short wEncRes)
{
	CSingleLock sLock ( &m_sync );
	sLock.Lock();

	bool result = false;
	unsigned __int16 nRegAddr;

	m_wrPHRegisters[nHead]->_SetupRegs._ConveyorSpeed._Speed = ComputeSpeedGateWidth ( wEncRes );
	switch ( nHead ) {
	case 1:
		nRegAddr = PH_1_REG_ADDR;
		break;
	case 0:
	default:
		nRegAddr = PH_0_REG_ADDR;
	}
	result = WriteReg ( nRegAddr, m_wrPHRegisters[nHead], sizeof(PrintHeadRegs) );

	return result;
}

bool CUsbMphc::SetStrobe(unsigned int nHead, INT Color)
{
	CSingleLock sLock ( &m_sync );
	sLock.Lock();

	bool result = false;
	unsigned __int16 nRegAddr;

	m_wrPHRegisters[nHead]->_SetupRegs._CtrlReg._ErrDrv = Color;
	switch ( nHead ) {
	case 1:
		nRegAddr = PH_1_REG_ADDR;
		break;
	case 0:
	default:
		nRegAddr = PH_0_REG_ADDR;
	}

	result = WriteReg ( nRegAddr, m_wrPHRegisters[nHead], sizeof(PrintHeadRegs) );

	return result;
}

void CUsbMphc::RegisterPhotoEvent(unsigned int nHead, HANDLE hEvent)
{
	m_hPhotoEvent[nHead] = hEvent;
}

void CUsbMphc::RegisterEOPEvent(unsigned int nHead, HANDLE hEvent)
{
	m_hEOPEvent[nHead] = hEvent;
}

void CUsbMphc::RegisterSerialEvent(unsigned int nHead, HANDLE hEvent)
{
	m_hSerialEvent[nHead] = hEvent;
}

void CUsbMphc::Shutdown(void)
{
	SetEvent(m_hExit);
}

bool CUsbMphc::GetIndicators(unsigned int nHead, tagIndicators& Indicators)
{
	CSingleLock sLock ( &m_sync );
	sLock.Lock();

	bool result = true;
	
	Indicators.HVOK		= m_rdPHRegisters[nHead]->_SetupRegs._IFStatusReg._HVOK		? TRUE : FALSE;
	Indicators.InkLow	= m_rdPHRegisters[nHead]->_SetupRegs._IFStatusReg._InkLow	? TRUE : FALSE;
	Indicators.AtTemp	= m_rdPHRegisters[nHead]->_SetupRegs._IFStatusReg._AtTemp	? TRUE : FALSE;
	Indicators.HeaterOn	= m_rdPHRegisters[nHead]->_SetupRegs._IFStatusReg._HeaterOn	? TRUE : FALSE;
	Indicators.nEp8Cnt	= m_nEp8Cnt;
	ReadLineSpeed (nHead, Indicators.dSpeed);

	return result;
}

bool CUsbMphc::ReadConfiguration(unsigned int nHead, PrintHeadRegs& tConfig)
{
	CSingleLock sLock ( &m_sync );
	bool result = false;

	sLock.Lock();
		memcpy ( &tConfig, m_wrPHRegisters[nHead], sizeof ( PrintHeadRegs ) );
		result = true;
	sLock.Unlock();
	return result;
}

bool CUsbMphc::ReadLineSpeed(unsigned int nHead, double& dSpeed)
{
	CSingleLock sLock ( &m_sync );
	sLock.Lock();

	bool result = true;
	dSpeed = (m_rdPHRegisters[nHead]->_SetupRegs._ConveyorSpeed._Speed / (double)((double) FTACH_100/ (double)100));
	return result;
}

bool CUsbMphc::ReadSerialData(unsigned int nHead, unsigned char *pData, unsigned int& nBytes)
{
	CSingleLock sLock ( &m_sync );
	sLock.Lock();

	bool result = false;
	return result;
}

inline bool IsSignalled (HANDLE hEvent)
{
	return ::WaitForSingleObject (hEvent, 0) == WAIT_OBJECT_0 ? true : false;
}

inline void CUsbMphc::ProcessEventTriggers(void)
{
	if ( m_hPhotoEvent[0] ) {
		if ( m_rdRegisters._Print_0_HeadRegs._SetupRegs._IFStatusReg._Photocell == 0x01 ) {
			CString str;
			str.Format (_T ("0x%p: SetEvent (m_hPhotoEvent [0])%s"), m_hPhotoEvent [0], (IsSignalled (m_hPhotoEvent [0]) ? _T (" [SIGNALLED]") : _T ("")));
			TRACEF (str);
			SetEvent ( m_hPhotoEvent[0] );
		}
	}

	if ( m_hPhotoEvent[1] ) {
		if ( m_rdRegisters._Print_1_HeadRegs._SetupRegs._IFStatusReg._Photocell == 0x01 ) {
			CString str;
			str.Format (_T ("0x%p: SetEvent (m_hPhotoEvent [1])%s"), m_hPhotoEvent [1], (IsSignalled (m_hPhotoEvent [1]) ? _T (" [SIGNALLED]") : _T ("")));
			TRACEF (str);
			SetEvent ( m_hPhotoEvent[1] );
		}
	}

	if ( m_hEOPEvent[0] ) {
		if ( m_rdRegisters._Print_0_HeadRegs._SetupRegs._IFStatusReg._EndPrint == 0x01 ) {
			CString str;
			str.Format (_T ("0x%p: SetEvent (m_hEOPEvent [0])%s"), m_hEOPEvent [0], (IsSignalled (m_hEOPEvent [0]) ? _T (" [SIGNALLED]") : _T ("")));
			TRACEF (str);
			SetEvent ( m_hEOPEvent[0] );
		}
	}

	if ( m_hEOPEvent[1] ) {
		if ( m_rdRegisters._Print_1_HeadRegs._SetupRegs._IFStatusReg._EndPrint == 0x01 ) {
			CString str;
			str.Format (_T ("0x%p: SetEvent (m_hEOPEvent [1])%s"), m_hEOPEvent [1], (IsSignalled (m_hEOPEvent [1]) ? _T (" [SIGNALLED]") : _T ("")));
			TRACEF (str);
			SetEvent ( m_hEOPEvent[1] );
		}
	}
}
