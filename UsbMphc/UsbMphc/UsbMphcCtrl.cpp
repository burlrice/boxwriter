/***************************************************************
 * Name:      UsbMphcCtrl.cpp
 * Purpose:   Code for Main Class
 * Author:    Chris Hodge (chodge@risc-design.com)
 * Created:   2010-05-12
 * Copyright: Chris Hodge (www.risc-design.com)
 * License:   All rights are reserved. Reproduction in whole or 
 *            part is prohibited without the written consent of 
 *            the copyright owner.
 **************************************************************/

#include "stdafx.h"
#include "Typedefs.h"
#include "UsbMphcCtrl.h"
#include "UsbMphc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


VOID CUsbMphcCtrl::DemoMemicLineSpeed(PHCB* pPHCB)
{
	static bool flag = false;

	if ( flag ) {
		pPHCB->Data[0] = 0x57;
		pPHCB->Data[1] = 0x02;
	}
	else {
		pPHCB->Data[0] = 0x58;
		pPHCB->Data[1] = 0x02;
	}

	flag = !flag;
}

/////////////////////////////////////////////////////////////////
//Memics reading the head indicators for demo mode only.
/////////////////////////////////////////////////////////////////
VOID CUsbMphcCtrl::DemoMemicIndicators(PHCB* pPHCB)
{
	pPHCB->Data[0] = 0xD0;
	pPHCB->Data[1] = 0x00;
}


/////////////////////////////////////////////////////////////////
// Memic the firing of a photocell for demo mode only.
/////////////////////////////////////////////////////////////////
VOID CUsbMphcCtrl::DemoMemicPhotocell(unsigned int DeviceID)
{
	if (m_bDemoMemicPhotocell) {
		_PHOTODEMO d;
		const UINT nMaster = 0x01;
		
		if (m_DemoPhotoEvent.Lookup (DeviceID, d)) {
			//const int nFullInterval = 2;
			const int nHalfInterval = m_nFullInterval / 2;
			int nCall = d.m_nCall % m_nFullInterval;

			//if (DeviceID == nMaster) 
			{
				if (nCall == 0) {
					if (m_nFullInterval > 2) {
						for (POSITION pos = m_DemoPhotoEvent.GetStartPosition (); pos; ) {
							UINT n = 0;
							_PHOTODEMO tmp;
							
							m_DemoPhotoEvent.GetNextAssoc (pos, n, tmp);
							#ifdef _DEBUG
							{ CString str; str.Format (_T ("DemoMemicPhotocell [PC ]: 0x%x (%d) %d/%d: %d %s"), n, tmp.m_nCall, nHalfInterval, m_nFullInterval, d.m_nCall, CTime::GetCurrentTime ().Format (_T ("%c"))); TRACEF (str); }
							#endif
		
							SetEvent (tmp.m_hPhoto);
						}
					}
					else {
						#ifdef _DEBUG
						{ CString str; str.Format (_T ("DemoMemicPhotocell [PC ]: 0x%x (%d) %s"), DeviceID, d.m_nCall, CTime::GetCurrentTime ().Format (_T ("%c"))); TRACEF (str); }
						#endif
	
						SetEvent (d.m_hPhoto);
					}
				}

				else if (nCall == nHalfInterval) {
					if (m_nFullInterval > 2) {
						for (POSITION pos = m_DemoPhotoEvent.GetStartPosition (); pos; ) {
							UINT n = 0;
							_PHOTODEMO tmp;
							
							m_DemoPhotoEvent.GetNextAssoc (pos, n, tmp);
							#ifdef _DEBUG
							{ CString str; str.Format (_T ("DemoMemicPhotocell [EOP]: 0x%x (%d) %s"), n, tmp.m_nCall, CTime::GetCurrentTime ().Format (_T ("%c"))); TRACEF (str); }
							#endif
		
							SetEvent (tmp.m_hEOP);
						}
					}
					else {
						#ifdef _DEBUG
						{ CString str; str.Format (_T ("DemoMemicPhotocell [EOP]: 0x%x (%d) %s"), DeviceID, d.m_nCall, CTime::GetCurrentTime ().Format (_T ("%c"))); TRACEF (str); }
						#endif
	
						SetEvent (d.m_hEOP);
					}
				}
			}

			d.m_nCall++;
			m_DemoPhotoEvent.SetAt (DeviceID, d);
		}
	}
}

void CUsbMphcCtrl::SetDemoPhotocellInterval (int n)
{
	ASSERT ((m_nFullInterval % 2) == 0);

	if ((m_nFullInterval % 2) == 0)
		m_nFullInterval = n;
}

VOID CUsbMphcCtrl::DeviceError()
{
   LPVOID lpMsgBuf;
   FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,
   NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),(LPTSTR) &lpMsgBuf, 0, NULL );
   AfxMessageBox( (LPCTSTR)lpMsgBuf, MB_OK|MB_ICONINFORMATION );
   LocalFree( lpMsgBuf );
}

DWORD GetProfileDWORD (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, DWORD dwDefault) 
{
	HKEY hKey;
	UINT dwResult = dwDefault;

	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = sizeof (DWORD), dwType = REG_DWORD;
		DWORD dwData;

		if (::RegQueryValueEx (hKey, szKey, NULL, &dwType, (LPBYTE) &dwData, &dwSize) == ERROR_SUCCESS) 
			dwResult = (UINT)dwData;

		::RegCloseKey (hKey);
	}

	return dwResult;
}

static short GetProdID ()
{
    OSVERSIONINFO osvi;

    ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

    GetVersionEx(&osvi);

	short nDefault = (osvi.dwMajorVersion == 7) ? 0x900A : 0x000A;

	return (short)GetProfileDWORD (HKEY_LOCAL_MACHINE, _T ("Software\\FoxJet\\BoxWriter ELITE"), _T ("UsbProdID"), nDefault);
}

CUsbMphcCtrl::CUsbMphcCtrl(char * lpszFirmware, bool bDemo, bool bDemoMemicPhotocell)
//:	CDeviceList(0x3411, 0x000A),
:	CDeviceList(0x3411, GetProdID ()),
	m_nextID (0),
	m_bDemo (bDemo),
	m_DemoPointers (),
	m_DemoPhotoEvent (),
	m_sync (),
	m_nFullInterval (2),
	m_bDemoMemicPhotocell (bDemoMemicPhotocell)
{
	unsigned long nBytes = 0;
	m_pUSBDevice = NULL;

	{
		CString str;

		for (int i = 0; i < strlen (lpszFirmware); i++) {
			char c = lpszFirmware [i];

			if (c == ';') {
				m_vFirmware.Add (str);
				str.Empty ();
			}
			else 
				str += c;
		}

		if (str.GetLength ())
			m_vFirmware.Add (str);
	}

	if ( m_bDemo ) 
	{
		m_Devices.Add ( INVALID_HANDLE_VALUE );

		for (int i = 1; i <= 8; i++)
			DemoAddress.Add (i);

		for ( unsigned long j = 0; j < (unsigned long)DemoAddress.GetSize(); j++ )
			m_AddressMap.SetAt ( DemoAddress.GetAt(j), INVALID_HANDLE_VALUE);
	}
}

CUsbMphcCtrl::~CUsbMphcCtrl()
{
	StopService();
}

/*!
Stops the UsbMphc Services. Should be called prior to shutdown

	\return NONE

*/
VOID CUsbMphcCtrl::StopService(VOID)
{
	CUsbMphc *pUsbMphc;
	unsigned long Addr;

	POSITION pos = m_AddressMap.GetStartPosition();
	while ( pos ) {
		m_AddressMap.GetNextAssoc( pos, Addr, (VOID *&)pUsbMphc );
		
		if (pUsbMphc != INVALID_HANDLE_VALUE)
			pUsbMphc->Shutdown();
	}
	if ( m_pUSBDevice ) {
		m_pUSBDevice->Close();
		delete m_pUSBDevice;
		m_pUSBDevice = NULL;
	}

	while ( !m_DemoPointers.IsEmpty() )
		delete ((unsigned char *) m_DemoPointers.RemoveHead());
}

/*!
 Starts the UsbMphc services.  This must be called prior to making
 any calls in the API.  This locates and enumerates the devices
 connected to the system.

	\return		true upon success 

*/

bool CUsbMphcCtrl::StartService()
{
	bool result = false;
	CCyUSBDevice *pUSBDevice = new CCyUSBDevice( ::GetActiveWindow() ); // Create an instance of CCyUSBDevice
	int devices = pUSBDevice->DeviceCount();
	int d = 0;
	int idx = 0;
	unsigned long Addr = 0x01;
	bool MasterCardSet = false;
	const DWORD dwProdID = GetProdID ();

	do {
		pUSBDevice->Open(d); // Open automatically calls Close() if necessary
		if ( ( pUSBDevice->VendorID == 0x3411) && (pUSBDevice->ProductID == dwProdID)) { 
		//if ( ( pUSBDevice->VendorID == 0x3411) && (pUSBDevice->ProductID == 0x000A) ) {
			if ( pUSBDevice->EndPointCount() == 6 ) {
				pUSBDevice->Close();			
				CCyUSBDevice *p = new CCyUSBDevice( ::GetActiveWindow() );
				p->Open(d);
				m_DeviceList.AddTail( p );
				CUsbMphc *pUsbMphc  = (CUsbMphc *)AfxBeginThread ( RUNTIME_CLASS( CUsbMphc ), 0, 0, CREATE_SUSPENDED, 0 );
				pUsbMphc->m_pDevice = new CDeviceListEntry((CCyBulkEndPoint *) p->EndPoints[1], 
															(CCyBulkEndPoint *) p->EndPoints[2], 
															(CCyBulkEndPoint *) p->EndPoints[3], 
															(CCyBulkEndPoint *) p->EndPoints[4], 
															(CCyBulkEndPoint *) p->EndPoints[5]);

				bool bProgramFPGA = false;

				for (int nFirmware = 0; !bProgramFPGA && (nFirmware < m_vFirmware.GetSize ()); nFirmware++) {
					CString strFirmware = m_vFirmware [nFirmware];

					UINT nBytes = 0;
					unsigned char *pData = NULL;
					try {
						CString str;
						str.Format (_T (".\\%s"), strFirmware);
						TRACE0 (str + _T ("\n"));

						CFile inFile  (str, CFile::modeRead|CFile::shareDenyWrite );
						if ( inFile.m_hFile != INVALID_HANDLE_VALUE) {
							ULONG nLen = ( ULONG ) inFile.GetLength();
							pData = new UCHAR[nLen];
							nBytes = inFile.Read( pData, nLen );
							inFile.Close();
						}
						else {
							CString strMsg;
							strMsg.Format (_T ("Unable to load .\\%s"), strFirmware);
							MessageBox ( ::GetActiveWindow(), strMsg, _T("ERROR"), MB_OK );
							return false;
						}
					}
					catch ( CFileException *e ) { e->ReportError(); e->Delete(); }

					bProgramFPGA = pUsbMphc->ProgramFPGA( pData, nBytes );

					if (pData)
						delete [] pData;
				}

				unsigned char Data[512];
				SecureZeroMemory ( Data, sizeof(Data) );
				Data[0] = 0x7F;	// Issue FPGA Reset
				long nDataLen = 512;
				pUsbMphc->m_pDevice->m_pCtrlOut->XferData(Data, nDataLen, NULL );
				MasterCtrlReg Mcr;
				CUsbMphc *pTemp;
				if ( pUsbMphc->ReadReg( 0x0000, &Mcr, sizeof ( Mcr )) ) {
					switch ( Mcr.CardID ) {
						case 0:	// This is to be a Master Card.  Master means it can drive Over-The-Bus
							Addr = 1;
							if ( !m_AddressMap.Lookup( Addr, (void *&)pTemp )) {	
								Mcr.CardID = 0x01;
								pUsbMphc->WriteReg( 0x0000, &Mcr, sizeof ( Mcr ));
								m_AddressMap.SetAt ( Addr, pUsbMphc);
								Addr += 1;
								m_AddressMap.SetAt ( Addr, pUsbMphc);
								while ( ResumeThread( pUsbMphc->m_hThread ) > 0x01 ) Sleep(0);
							}else {
								::MessageBox ( ::GetForegroundWindow(), "Duplicate Card ID's", "HARDWARE ERROR", MB_OK );
								delete pUsbMphc;
							}
							break;
						case 1:	// This is to be a Master Card.  Master means it can drive Over-The-Bus
							Addr = 3;
							if ( !m_AddressMap.Lookup( Addr, (void *&)pTemp ) ) {	
								Mcr.CardID = 0x01;
								pUsbMphc->WriteReg( 0x0000, &Mcr, sizeof ( Mcr ));
								m_AddressMap.SetAt ( Addr, pUsbMphc);
								Addr += 1;
								m_AddressMap.SetAt ( Addr, pUsbMphc);
								while ( ResumeThread( pUsbMphc->m_hThread ) > 0x01 ) Sleep(0);
							}else {
								::MessageBox ( ::GetForegroundWindow(), "Duplicate Card ID's", "HARDWARE ERROR", MB_OK );
								delete pUsbMphc;
							}
							break;
						case 2:
							Addr = 5;
							if ( !m_AddressMap.Lookup( Addr, (void *&)pTemp ) ) {	
								Mcr.CardID = 0x00;
								pUsbMphc->WriteReg( 0x0000, &Mcr, sizeof ( Mcr ));
								m_AddressMap.SetAt ( Addr, pUsbMphc);
								Addr += 1;
								m_AddressMap.SetAt ( Addr, pUsbMphc);
								while ( ResumeThread( pUsbMphc->m_hThread ) > 0x01 ) Sleep(0);
							}else {
								::MessageBox ( ::GetForegroundWindow(), "Duplicate Card ID's", "HARDWARE ERROR", MB_OK );
								delete pUsbMphc;
							}
							break;
						case 3:
							Addr = 7;
							if ( !m_AddressMap.Lookup( Addr, (void *&)pTemp ) ) {	
								Mcr.CardID = 0x00;
								pUsbMphc->WriteReg( 0x0000, &Mcr, sizeof ( Mcr ));
								m_AddressMap.SetAt ( Addr, pUsbMphc);
								Addr += 1;
								m_AddressMap.SetAt ( Addr, pUsbMphc);
								while ( ResumeThread( pUsbMphc->m_hThread ) > 0x01 ) Sleep(0);
							}else {
								::MessageBox ( ::GetForegroundWindow(), "Duplicate Card ID's", "HARDWARE ERROR", MB_OK );
								delete pUsbMphc;
							}
							break;
					}
					if ( m_AddressMap.GetCount() >= MAX_DEVICES )
						break;
				}
				else {
					//Log ("Read Registers failed");
				}
			}
		}
		d++;
	} while ( d < devices );

	delete pUSBDevice;

	if ( m_AddressMap.GetCount() )
		result = true;
	
	return result;
}

/*!
 Claim the PHC as being in-use (owned).  Checks to see if PHC has
 been previously claimed.

	\param		PHC Address
	\return		Unique reference handle to PHC.  0x00 if PHC
				previously claimed.

*/

unsigned long CUsbMphcCtrl::ClaimAddress(unsigned long Addr)
{
	CSingleLock (&m_sync).Lock ();

	unsigned long value = 0;

	PVOID pDevice, pTemp;

	if ( !m_AddressMap.IsEmpty() ) {
		if ( m_AddressMap.Lookup ( Addr, pDevice ) ) {	// Check to see if this is a valid address.
			// It is a valid address so see if it has already been claimed.
			if ( !m_ClaimedAddr.Lookup( Addr, pTemp ) ) {
				m_ClaimedAddr.SetAt ( Addr, pDevice);
				value = Addr;
			}
		}
	}

	return value; // Return the assigned id.
}

bool CUsbMphcCtrl::ClearPrintBuffer(unsigned int DeviceID, ULONG nStartLoc, ULONG nBytes)
{
 	CSingleLock (&m_sync).Lock ();   
	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) 
		result = pDevice->ClearPrintBuffer( nHead, nStartLoc, nBytes );

	return result;
}


/*!
Compute the sample gate width for reading back the conveyor speed.
	\param		Encoder Resolution
	\return		Computed speed gate width
*/

unsigned int CUsbMphcCtrl::ComputeSpeedGateWidth(unsigned short nEncoderRes)
{
   return ( unsigned int )((double) (FTACH_100 * FGATE ) / (double) ( 20 * nEncoderRes));
}

/*

*/
bool CUsbMphcCtrl::EnablePrint(unsigned int DeviceID, bool bEnabled)
{
	CSingleLock (&m_sync).Lock ();   
	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) 
		result = pDevice->EnablePrint( nHead, bEnabled );

	return result;
}

/*!
	Gets the number of address provide by the interface.

	\return	 number of addresses
*/

unsigned int CUsbMphcCtrl::GetAddressCount()
{
   return m_AddressMap.GetCount();
}

/*!
	Get a list of addresses provided by the driver.

	\param		unsigned long *pList
	\param		unsigned int &Items
	\return		true on success, with number of items in Items
	\see		GetAddressCount
*/

bool CUsbMphcCtrl::GetAddressList(unsigned long* pList, unsigned int& Items)
{
	CSingleLock (&m_sync).Lock ();
	bool result = false;
	POSITION pos = NULL;
	unsigned long Addr;
	PVOID pDevice;
	unsigned cnt = 0;
	unsigned long *p = pList;
	pos = m_AddressMap.GetStartPosition();
	while ( pos ) {
		result = true;
		m_AddressMap.GetNextAssoc ( pos, Addr, pDevice );
		if ( cnt++ < Items )
			*p++ = Addr;
		else
			break;
	}
	Items = cnt;
	return result;
}

/*!
	Retrieves the head indicators such as Ink, Heater and HVOK status.

	\param		DeviceID Handle obtained from ClaimAddress
	\param		tagIndicators& Indicators
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::GetIndicators(unsigned int DeviceID, tagIndicators& Indicators)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE) {
		if (m_bDemo) {
			Indicators.HVOK		= TRUE;
			Indicators.InkLow	= FALSE;
			Indicators.AtTemp	= TRUE;
			Indicators.HeaterOn	= FALSE;
			Indicators.dSpeed = 60.0;

			if (m_bDemoMemicPhotocell)
				DemoMemicPhotocell (DeviceID);

			result = true;
		}
	}
	else if (pDevice) 
		result = pDevice->GetIndicators( nHead, Indicators );

	return result;
}


/*!
 Writes a new FPGA bit file to the PHC

	\param		DeviceID Handle obtained from ClaimAddress
	\param		FilePathName Full qualifing path of bit file
	\return		0 on success
	\see		ClaimAddress
*/

bool CUsbMphcCtrl::ProgramFPGA(unsigned int DeviceID, LPCSTR FilePathName)
{
	bool result = false;
	// Function not used see StartServices
	return result;
}

/*!
	Programs the Address Generator Registers ( Slant Registers ) with the direction and slant values.

	\param		DeviceID Handle obtained from ClaimAddress
	\param		SlantRegs
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::ProgramSlantRegs(unsigned int DeviceID, SlantRegs tSlantRegs)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) 
		result = pDevice->ProgramSlantRegs( nHead, tSlantRegs );

	return result;
}

/*!
	Reads the current configuration from the UsbMphc card

	\param		DeviceID Handle obtained from ClaimAddress
	\param		Registers
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::ReadConfiguration(unsigned int DeviceID, PrintHeadRegs& tConfig)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) 
		result = pDevice->ReadConfiguration( nHead, tConfig );

	return result;
}


/*!
	Reads the line ( conveyor ) speed from the card.

	\param		DeviceID Handle obtained from ClaimAddress
	\param		double &dSpeed
	\return		true on success, Speed returned in dSpeed
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::ReadLineSpeed(unsigned int DeviceID, double& dSpeed)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;

	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE) {
	}
	else if (pDevice) 
		result = pDevice->ReadLineSpeed( nHead, dSpeed );

	return result;
}

/*!
	Reads the serial data recieved by the UsbMphc

	\param		DeviceID Handle obtained from ClaimAddress
	\param		unsigned char *pData
	\param		unsigned int &nBytes
	\return		true on success, number of bytes read return in nBytes
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::ReadSerialData(unsigned int DeviceID, unsigned char *pData, unsigned int& nBytes)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;

	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) 
		result = pDevice->ReadSerialData( nHead, pData, nBytes );

	return result;
}

/*!
	Release a previously claimed address

	\param		DeviceID Handle obtained from ClaimAddress
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::ReleaseAddress(unsigned int DeviceID)
{
	CSingleLock (&m_sync).Lock ();
	bool value = false;
//	unsigned long Addr;
	PVOID pDevice;

	if ( m_ClaimedAddr.Lookup((unsigned long)DeviceID, ( PVOID & )pDevice) ) {
		m_ClaimedAddr.RemoveKey ( (unsigned long) DeviceID );
		value = true;
	}

	return value;
}

/*!
	Sets the length of the actual image, this should include 
	flush buffer.

	\param		DeviceID Handle obtained from ClaimAddress
	\param		WORD wImageCols
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::SetImageLen(unsigned int DeviceID, WORD wImageCols)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;

	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) 
		result = pDevice->SetImageLen( nHead, wImageCols );

	return result;
}

/*!
	Set card to MASTER mode.  Forces remaining cards to SLAVE mode.
	MASTER mode card routes its interrupt onto the ISA bus
	SLAVE mode card routes its interrupt onto the Over-The-Top bus.
	
	DO NOT USE AT THIS TIME

	\param		DeviceID Handle obtained from ClaimAddress
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::SetMasterMode(unsigned int DeviceID)
{
//	CSingleLock (&m_sync).Lock ();
	bool value = false;

//	UINT nHead = 0x00;
//	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 
//
//	if (pDevice == INVALID_HANDLE_VALUE)
//		result = true;
//	else if (pDevice) 
//		value = pDevice->SetMasterMode();

	return value;
}

/*!
	Sets the print delay

	\param		DeviceID Handle obtained from ClaimAddress
	\param		WORD wDelay
	\return		true on success
	\see		ClaimAddress for DeviceID
*/
bool CUsbMphcCtrl::SetPrintDelay(unsigned int DeviceID, WORD wDelay)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;

	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) 
		result = pDevice->SetPrintDelay( nHead, wDelay );

	return result;
}

/*!
	Sets the Product Lenght.  The UsbMphc treats this as a delay, 
	so this becomes the number of columns to delay, after the
	image is printed, before allowing the next photocell.

	\param		DeviceID Handle obtained from ClaimAddress
	\param		WORD wProdLen
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::SetProductLen(unsigned int DeviceID, WORD wProdLen)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;

	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) 
		result = pDevice->SetProductLen( nHead, wProdLen );

	return result;
}

/*!
	Sets the serial paramaters ( baud parity data bits etc. )

	\param		DeviceID Handle obtained from ClaimAddress
	\param		tagSerialParams Params
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::SetSerialParams(unsigned int DeviceID, tagSerialParams Params)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) 
		result = pDevice->SetSerialParams ( nHead, Params );

	return result;
}

/*!
	Sets the Speed Gate Width

	\param		DeviceID Handle obtained from ClaimAddress
	\param		WORD wENcRes
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::SetSpeedGateWidth(unsigned int DeviceID, WORD wEncRes)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;

	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) 
		pDevice->SetSpeedGateWidth( nHead, wEncRes );

	return result;
}

/*!
	Sets the various strobes

	\param		DeviceID Handle obtained from ClaimAddress
	\param		int Color ( Color defines which strobe )
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::SetStrobe(unsigned int DeviceID, int Color)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) 
		result = pDevice->SetStrobe( nHead, Color );

	return result;
}

/*!
	Registers an End-Of-Print event with the device thread
	Upon recieving the End-Of-Print signal from the UsbMphc
	this event will be set.

	\param		DeviceID Handle obtained from ClaimAddress
	\param		HANDLE hEvent
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::RegisterEOPEvent(unsigned int DeviceID, HANDLE hEvent)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE) {
		if (m_bDemo) {
			_PHOTODEMO d;

			m_DemoPhotoEvent.Lookup (DeviceID, d);
			d.m_hEOP = hEvent;
			m_DemoPhotoEvent.SetAt (DeviceID, d);
			result = true;
		}
	}
	else if (pDevice) {
		pDevice->RegisterEOPEvent (nHead, hEvent);
		result = true;
	}

	return result;
}

/*!
	Un-registers the End-Of-Print event with the device thread

	\param		DeviceID Handle obtained from ClaimAddress
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::UnRegisterEOPEvent(unsigned int DeviceID)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE) {
		if (m_bDemo) {
			_PHOTODEMO d;

			m_DemoPhotoEvent.Lookup (DeviceID, d);
			d.m_hEOP = NULL;
			m_DemoPhotoEvent.SetAt (DeviceID, d);
			result = true;
		}
	}
	else if (pDevice) {
		pDevice->RegisterEOPEvent (nHead, NULL);
		result = true;
	}

	return result;
}

/*!
	Registers an photocell event with the device thread
	Upon recieving the photocell signal from the UsbMphc
	this event will be set.

	\param		DeviceID Handle obtained from ClaimAddress
	\param		HANDLE hEvent
	\return		true on success
	\see		ClaimAddress for DeviceID
*/
bool CUsbMphcCtrl::RegisterPhotoEvent(unsigned int DeviceID, HANDLE hEvent)
{
	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE) {
		if (m_bDemo) {
			_PHOTODEMO d;

			m_DemoPhotoEvent.Lookup (DeviceID, d);
			d.m_hPhoto = hEvent;
			m_DemoPhotoEvent.SetAt (DeviceID, d);
			result = true;
		}
	}
	else if (pDevice) {
		CSingleLock (&m_sync).Lock ();
		pDevice->RegisterPhotoEvent (nHead, hEvent);
		result = true;
	}

	return result;
}

/*!
	Un-registers the photocell event with the device thread

	\param		DeviceID Handle obtained from ClaimAddress
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::UnRegisterPhotoEvent(unsigned int DeviceID)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE) {
		if (m_bDemo) {
			_PHOTODEMO d;

			m_DemoPhotoEvent.Lookup (DeviceID, d);
			d.m_hPhoto = NULL;
			m_DemoPhotoEvent.SetAt (DeviceID, d);
			result = true;
		}
	}
	else if (pDevice) {
		pDevice->RegisterPhotoEvent (nHead, NULL);
		result = true;
	}

	return result;
}

/*
Registers an Event for serial data with the driver.
*/
/*!
	Registers an serial data event with the device thread
	Upon recieving the serial data signal from the UsbMphc
	this event will be set.

	\param		DeviceID Handle obtained from ClaimAddress
	\param		HANDLE hEvent
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::RegisterSerialEvent(unsigned int DeviceID, HANDLE hEvent)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) {
		pDevice->RegisterSerialEvent (nHead, hEvent);
		result = true;
	}

	return result;
}

/*!
	Un-registers the serial data event with the device thread

	\param		unsigned int DeviceID Handle obtained from ClaimAddress
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::UnRegisterSerialEvent(unsigned int DeviceID)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) {
		pDevice->RegisterSerialEvent (nHead, NULL);
		result = true;
	}

	return result;
}

/*!
	Reads image currently in RAM of Print Head Controller Card.

	\param		DeviceID Handle obtained from ClaimAddress
	\param		unsigned char *pBuffer
	\param		unsigned long lBytes
	\return		true on success
	\see		ClaimAddress for DeviceID
*/

bool CUsbMphcCtrl::ReadImage(unsigned int DeviceID, unsigned char *pBuffer, unsigned long lBytes)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = false;
	else if (pDevice) 
		result = pDevice->ReadImage( nHead, pBuffer, 0x00, lBytes);

	return result;
}

bool CUsbMphcCtrl::UpdateImageData(unsigned int DeviceID, VOID* pImageBuffer,
                           unsigned char nBPC, WORD StartCol, WORD wNumCols)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	unsigned int nHead;
	unsigned long nBytes	= ( wNumCols * nBPC);
	unsigned long nStartLoc	= (( StartCol * nBPC ) / 2); // Calculate bytes then convert to woreds
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) 
		result = pDevice->WriteImage( nHead, pImageBuffer, nStartLoc, nBytes);

	return result;
}

/*!
Writes an updated print head configuration to the approprate card.

	\param	unsigned int DeviceID
	\param SetupRegs Print head setup registers
	\param FireVibPulse Fire pulse and vibration registers
	\return true on success
	\see		ClaimAddress for DeviceID

*/
bool CUsbMphcCtrl::WriteSetup(unsigned int DeviceID, struct SetupRegs tSetup, struct FireVibPulse tPulseRegs)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = true;
	else if (pDevice) 
		result = pDevice->WriteSetup ( nHead, tSetup, tPulseRegs );

	return result;
}

void * CUsbMphcCtrl::GetHeadFromDeviceID(unsigned int DeviceID, unsigned int & nHead)
{
	CSingleLock (&m_sync).Lock ();

	void *pDevice = INVALID_HANDLE_VALUE;

   // Get the address associated with the DeviceID
	if ( m_ClaimedAddr.Lookup((unsigned long)DeviceID, (VOID * &)pDevice ) ) {
		if ( (DeviceID %2) )
			nHead = 0x00;
		else
			nHead = 0x01;
	}

	return pDevice;
}

bool CUsbMphcCtrl::ReadAll(unsigned int DeviceID, struct Registers &Reg)
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead);

	if (pDevice == INVALID_HANDLE_VALUE)
		result = false;
	else if (pDevice) 
		result = pDevice->ReadReg ( nHead, &Reg, sizeof( Registers ) );

	return result;
}

bool CUsbMphcCtrl::WriteAll(unsigned int DeviceID, struct Registers &Reg )
{
	CSingleLock (&m_sync).Lock ();

	bool result = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice == INVALID_HANDLE_VALUE)
		result = false;
	else if (pDevice) 
		result = pDevice->WriteReg ( nHead, &Reg, sizeof( Registers ) );

	return result;
}

void CUsbMphcCtrl::Log(LPCSTR szEntry)
{
	::MessageBox ( ::GetActiveWindow(), szEntry, "Error Log Entry", MB_OK );
}

bool CUsbMphcCtrl::ProgramFPGA (unsigned int DeviceID)
{
	CSingleLock (&m_sync).Lock ();   
	bool bResult = false;
	UINT nHead = 0x00;
	CUsbMphc * pDevice = (CUsbMphc *)GetHeadFromDeviceID(DeviceID, nHead); 

	if (pDevice != INVALID_HANDLE_VALUE && pDevice) {
		for (int nFirmware = 0; !bResult && (nFirmware < m_vFirmware.GetSize ()); nFirmware++) {
			CString strFirmware = m_vFirmware [nFirmware];

			UINT nBytes = 0;
			unsigned char *pData = NULL;
			try {
				CString str;
				str.Format (_T (".\\%s"), strFirmware);
				TRACE0 (str + _T ("\n"));

				CFile inFile  (str, CFile::modeRead|CFile::shareDenyWrite );
				if ( inFile.m_hFile != INVALID_HANDLE_VALUE) {
					ULONG nLen = ( ULONG ) inFile.GetLength();
					pData = new UCHAR[nLen];
					nBytes = inFile.Read( pData, nLen );
					inFile.Close();
				}
				else {
					CString strMsg;
					strMsg.Format (_T ("Unable to load .\\%s"), strFirmware);
					MessageBox ( ::GetActiveWindow(), strMsg, _T("ERROR"), MB_OK );
					return false;
				}
			}
			catch ( CFileException *e ) { e->ReportError(); e->Delete(); }

			bResult = pDevice->ProgramFPGA( pData, nBytes );

			if (pData)
				delete [] pData;
		}
	}

	return bResult;
}
