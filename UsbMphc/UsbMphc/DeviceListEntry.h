/***************************************************************
 * Name:      DeviceListEntry.h
 * Purpose:   Header for DeviceListEntry
 * Author:    Chris Hodge (chodge@risc-design.com)
 * Created:   2010-05-12
 * Copyright: Chris Hodge (www.risc-design.com)
 * License:   All rights are reserved. Reproduction in whole or 
 *            part is prohibited without the written consent of 
 *            the copyright owner.
 **************************************************************/
#ifndef _DEVICELISTENTRY_H
#define _DEVICELISTENTRY_H

#include "CyAPI.h"
namespace USB {
	class CDeviceListEntry
	{

	public:
		CDeviceListEntry(CCyBulkEndPoint* pCtrlOut, CCyBulkEndPoint* pCtrlIn, CCyBulkEndPoint* pPrnCtrlOut, 
			CCyBulkEndPoint* pPrnCtrlIn, CCyBulkEndPoint* pStatusIn );
		CDeviceListEntry();

	public:
		CCyBulkEndPoint *m_pCtrlIn;
		CCyBulkEndPoint *m_pCtrlOut;
		CCyBulkEndPoint *m_pPrnCtrlIn;
		CCyBulkEndPoint *m_pPrnCtrlOut;
		CCyBulkEndPoint *m_pStatusIn;
		OVERLAPPED m_outIo, m_inIo;
	//	unsigned int DeviceID;
	//	CWinThread *m_pThread;
	public:
	   virtual ~CDeviceListEntry();
	};
};
#endif
