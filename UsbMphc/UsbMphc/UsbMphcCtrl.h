/***************************************************************
 * Name:      UsbMphcCtrl.h
 * Purpose:   Header for CUsbMphcCtrl Class
 * Author:    Chris Hodge (chodge@risc-design.com)
 * Created:   2010-05-12
 * Copyright: Chris Hodge (www.risc-design.com)
 * License:   All rights are reserved. Reproduction in whole or 
 *            part is prohibited without the written consent of 
 *            the copyright owner.
 **************************************************************/
#ifndef _MPHC_H
#define _MPHC_H

#include <Afxmt.h>
#include "DeviceList.h"
#include "Typedefs.h"
#include "CyAPI.h"

namespace USB
{
	class AFX_EXT_CLASS CUsbMphcCtrl : protected CDeviceList
	{

	protected:
		bool m_bDemo;
		ULONG m_nextID;
		CPtrList m_DemoPointers;
		CPtrList m_DeviceList;
		CStringArray m_vFirmware;

		CArray <unsigned long, unsigned long>								DemoAddress;
		CMap<unsigned int, unsigned int, tagPhotoDemo, tagPhotoDemo>		m_DemoPhotoEvent;
		CMap<unsigned long, unsigned long, VOID*, VOID* >					m_AddressMap;
		CMap<unsigned long, unsigned long, VOID*, VOID* >					m_ClaimedAddr;
//		CMap<unsigned long, unsigned long, unsigned long, unsigned long >	m_ClaimedAddr;
		CArray<HANDLE, HANDLE>												m_Devices;
		CArray<CDeviceListEntry*, CDeviceListEntry *>						m_UsbDevices;
		bool																m_bDemoMemicPhotocell;
		int																	m_nFullInterval;

	protected:
		CCriticalSection m_sync;
		void DemoMemicLineSpeed(PHCB* pPHCB);
		void DemoMemicIndicators(PHCB* pPHCB);
		void DemoMemicPhotocell(unsigned int DeviceID);
		void DeviceError();
		void *GetHeadFromDeviceID(unsigned int DeviceID, unsigned int & nHead);

	public:
		CUsbMphcCtrl(char * lpszFirmware, bool bDemo, bool bDemoMemicPhotocell);
		virtual ~CUsbMphcCtrl();

		void StopService(VOID);

		void SetDemoPhotocellInterval (int n);
		unsigned long ClaimAddress(unsigned long Addr);
		unsigned int ComputeSpeedGateWidth(unsigned short nEncoderRes);
		unsigned int GetAddressCount();
		
		// Debug Interface
		bool ReadAll(unsigned int DeviceID, struct Registers &Reg);
		bool WriteAll(unsigned int DeviceID, struct Registers &Reg);
		// End Debug Interface

		bool StartService();
		bool UpdateImageData(unsigned int DeviceID, void *pImageBuffer, unsigned char nBPC, unsigned short StartCol, unsigned short wNumCols);
		bool ClearPrintBuffer(unsigned int DeviceID, ULONG nStartLoc, ULONG nBytes);
		bool GetAddressList(PULONG pList, unsigned int& Items);
		bool GetIndicators(unsigned int DeviceID, tagIndicators& Indicators);
		bool ProgramFPGA(unsigned int DeviceID, LPCSTR FilePathName);
		bool ReadConfiguration(unsigned int DeviceID, struct PrintHeadRegs& tConfig);
		bool ReadImage(unsigned int DeviceID, unsigned char *pBuffer, ULONG lBytes);
		bool ReadLineSpeed(unsigned int DeviceID, double& dSpeed);
		bool ReadSerialData(unsigned int DeviceID, unsigned char *pData, unsigned int& nBytes);
		bool RegisterEOPEvent(unsigned int DeviceID, HANDLE hEvent);
		bool RegisterPhotoEvent(unsigned int DeviceID, HANDLE hEvent);
		bool RegisterSerialEvent(unsigned int DeviceID, HANDLE hEvent);
		bool ReleaseAddress(unsigned int DeviceID);
		bool SetMasterMode(unsigned int DeviceID);
		bool UnRegisterEOPEvent(unsigned int DeviceID);
		bool UnRegisterPhotoEvent(unsigned int DeviceID);
		bool UnRegisterSerialEvent(unsigned int DeviceID);
		bool WriteSetup(unsigned int DeviceID, struct SetupRegs tSetup, struct FireVibPulse tPulseRegs);
		bool EnablePrint(unsigned int DeviceID, bool bEnabled);
		bool ProgramSlantRegs(unsigned int DeviceID, struct SlantRegs SlantRegs);
		bool SetImageLen(unsigned int DeviceID, unsigned short wImageCols);
		bool SetPrintDelay(unsigned int DeviceID, unsigned short wDelay);
		bool SetProductLen(unsigned int DeviceID, unsigned short wProdLen);
		bool SetSerialParams(unsigned int DeviceID, tagSerialParams Params);
		bool SetSpeedGateWidth(unsigned int DeviceID, unsigned short wEncRes);
		bool SetStrobe(unsigned int DeviceID, INT Color);
		void Log(LPCSTR szEntry);
		bool ProgramFPGA (unsigned int DeviceID);
	};
};
#endif
