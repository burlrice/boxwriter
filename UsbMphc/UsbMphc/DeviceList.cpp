/***************************************************************
 * Name:      DeviceList.cpp
 * Purpose:   Code for DeviceList
 * Author:    Chris Hodge (chodge@risc-design.com)
 * Created:   2010-05-12
 * Copyright: Chris Hodge (www.risc-design.com)
 * License:   All rights are reserved. Reproduction in whole or 
 *            part is prohibited without the written consent of 
 *            the copyright owner.
 **************************************************************/

#include "StdAfx.h"
#include "DeviceList.h"
#include "TypeDefs.h"
#include "CyAPI.h"

CDeviceList::CDeviceList(__int16 nVendorID, __int16 nProductID)
   : m_list()
{
	m_nVendorID		= 0x0000;
	m_nProductID	= 0x0000;
	m_pUSBDevice	= NULL;
}


CDeviceList::~CDeviceList()
{
}


// Open an enumeration handle so we can locate all devices of our
// own class
int CDeviceList::Initialize()
{
	return m_list.GetSize();
}


//{{AFX DO NOT EDIT CODE BELOW THIS LINE !!!
//}}AFX DO NOT EDIT CODE ABOVE THIS LINE !!!

