/***************************************************************
 * Name:      DeviceListEntry.cpp
 * Purpose:   Code for DeviceListEntry
 * Author:    Chris Hodge (chodge@risc-design.com)
 * Created:   2010-05-12
 * Copyright: Chris Hodge (www.risc-design.com)
 * License:   All rights are reserved. Reproduction in whole or 
 *            part is prohibited without the written consent of 
 *            the copyright owner.
 **************************************************************/


#include "StdAfx.h"
#include "DeviceListEntry.h"
#include "CyAPI.h"


CDeviceListEntry::CDeviceListEntry(CCyBulkEndPoint* pCtrlOut, CCyBulkEndPoint* pCtrlIn, CCyBulkEndPoint* pPrnCtrlOut, 
									CCyBulkEndPoint* pPrnCtrlIn, CCyBulkEndPoint* pStatusIn )

{
	m_pCtrlIn		= pCtrlIn;
	m_pCtrlIn->TimeOut = 500;

	m_pCtrlOut		= pCtrlOut;
	m_pCtrlOut->TimeOut = 500;
	
	m_pPrnCtrlIn	= pPrnCtrlIn;
	m_pPrnCtrlIn->TimeOut = 500;
	
	m_pPrnCtrlOut	= pPrnCtrlOut;
	m_pPrnCtrlOut->TimeOut = 500;
	
	m_pStatusIn		= pStatusIn;
}

CDeviceListEntry::CDeviceListEntry()
{
	m_pCtrlIn		= NULL;
	m_pCtrlOut		= NULL;
	m_pPrnCtrlIn	= NULL;
	m_pPrnCtrlOut	= NULL;
	m_pStatusIn		= NULL;
}

CDeviceListEntry::~CDeviceListEntry()
{
}
