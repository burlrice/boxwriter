#if !defined (__TYPEDEFS_H__CFA9CC56_CA28_44e0_B93E_4C728D4AC864)
	#define __TYPEDEFS_H__CFA9CC56_CA28_44e0_B93E_4C728D4AC864
namespace USB {
	#define MAX_DEVICES			8
	#define IMG_TX_BOUNDARY		64
	#define CMD_IMG_RD			0x00000004
	#define CMD_IMG_WR			0x00000000
	#define CMD_REG_RD			0x00000005
	#define CMD_REG_WR			0x00000001
	#define CMD_CLR_IMG			0x00000002

	#define PH_0_REG_ADDR		0x00000020
	#define UART_0_REG_ADDR		0x00000010
	#define FERITE_MEM_ADDR		0x0000004C
	#define PH_1_REG_ADDR		0x00000060

	#define PH_0_IMAGE_ADDR		0x00000000
	#define PH_1_IMAGE_ADDR		0x00080000

	enum PARITY {
		NO_PARITY,
		EVEN_PARITY,
		ODD_PARITY
	};


	#pragma pack (1)

	typedef struct EpPktHdr{
		unsigned __int32 Cmd; // 0 - ImgWr, 4 - ImgRd, 1 - RegWr, 5 - RegRd
		unsigned __int32 Addr;
		unsigned __int32 Len;
	}tagEpPktHdr;
	/*
	typedef struct EpPkt {
		tagEpPktHdr Hdr;
		unsigned char* pData;
	}tagEpPkt;
	*/

	typedef struct {
		unsigned long lAddress;		// Card Address
		unsigned char RegID;			// Register ID
		unsigned char TxBytes;		// Bytes to Transfer
		unsigned char Data[256];	// Data Read or Written
	}PHCB;

	typedef struct
	{
		UCHAR Major;		// Major Version		(0..255)
		UCHAR Minor;		// Minor Version		(0..255)
		UCHAR Revision;	// Revision Number	(0..255)
	}_VERSION;

	typedef struct
	{
	//	ULONG Addr;
		ULONG BaudRate;
		CHAR	DataBits;
		CHAR	StopBits;
		CHAR	Parity;
	}tagSerialParams;
/*
	typedef struct tagImgUpdateHdr
	{
		unsigned short	_wStartCol;
		unsigned short	_wNumCols;
		unsigned char	_nBPC;
	}_IMG_UPDATE_HDR;


	typedef struct tagImagePkt
	{
		ULONG _lAddress;
		ULONG _lStartCol;
		ULONG _lBytes;
		ULONG _lBytesPerCol;
	}_IMAGE_TX;
*/
	// This is a fixed constant within the MPHC Board.
	const unsigned long FGATE = 115200;

	// This is the units when the conveyor speed is 100 ft/min
	// This means, if this value is 1000 then when the conveyor is running 
	// at 100 ft/min the value returned when you read the speed register 
	// will be 1000  This allows for as much presision as needed.
	const unsigned long FTACH_100 = 1000;

	typedef struct tagIndicators
	{
	   bool HVOK;
	   bool AtTemp;
	   bool InkLow;
	   bool HeaterOn;
	   double dSpeed;
	   unsigned int nEp8Cnt;
	}_INDICATORS;

	typedef struct tagPhotoDemo
	{
		tagPhotoDemo () : m_hPhoto (NULL), m_hEOP (NULL), m_nCall (0) { }
	   HANDLE m_hPhoto;
	   HANDLE m_hEOP;
	   int m_nCall;
	}_PHOTODEMO;

//	const unsigned long IMAGE_BUFFER_SIZE =1024*128;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Begin Multi Function Print-Head Controller Registers Map
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	struct MasterCtrlReg {							// Master Control Register
		unsigned __int16 CardID				: 2;	// Card ID
		unsigned __int16 AutoStatus			: 1;	// 1 -Enable Auto Status Msg Generation
		unsigned __int16 Rsvd				: 13;	// Reserved
	};
	
	struct EventPeriod {
		unsigned __int16 Period;
	};

	struct EventTimer {
		unsigned __int16 Value;
	};

	struct UartRegs {
		unsigned __int16 Regs[16];
	};

	struct CtrlReg {								// Control Register
		unsigned __int16 _HeadType			: 2;	// 0-Disabled, 1-Slanted, 2 Staggered, 3-Graphic
		unsigned __int16 _GraphicHeadType	: 1;	// 1 - 384, 0 - 768.
		unsigned __int16 _DoublePulse		: 1;	// Enable Double Pulse.
		unsigned __int16 _PrintMode			: 1;	// 0 - Single Buffered, 1 - Double Buffered
		unsigned __int16 _PrintBuffer		: 1;	// 0 - Buffer0, 1 - Buffer1 // Selects the buffer which the printer uses when we filling the image. 
		unsigned __int16 _Inverted			: 1;	// 0-Normal, 1-Inverted
		unsigned __int16 _PrintEnable		: 1;	// 1-Enable Printing.
		unsigned __int16 _Rsvd				: 5;	// Reserved
		unsigned __int16 _ErrDrv			: 3;	// 1 - Red, 2 - Green; 4 - Yellow
	};

	struct IFCtrlReg {							// Interface Control Register
		unsigned __int16 _TachSrc			: 3;	// Tach Source; 0-Ext, 1-Int, 4-OTB Tach 1, 5-OTB Tach2, 6-OTB Tach3, 7-OTB Tach4
		unsigned __int16 _OTBTach			: 1;	// Drive OTB Tach
		unsigned __int16 _PhotoSrc			: 3;	// Photocell Source. 0-Ext, 1-Int, 4-OTB PC1, 5-OTB PC2, 6-PC3, 7-PC4
		unsigned __int16 _OTBEdge			: 1;	// Drive OTB Edge
		unsigned __int16 _Rsvd				: 8;	// Reserved
	};

	struct IFStatusReg {							// Interface Status Register
		unsigned __int16 _Photocell			: 1;	// Photocell Interrupt.
		unsigned __int16 _StartPrint		: 1;	// Photocell Interrupt.
		unsigned __int16 _EndPrint			: 1;	// Photocell Interrupt.
		unsigned __int16 _Rsvd1				: 1;	// Reserved
		unsigned __int16 _HVOK				: 1;	// High Voltage Ok
		unsigned __int16 _InkLow			: 1;	// Ink Low
		unsigned __int16 _AtTemp			: 1;	// At Temp.
		unsigned __int16 _HeaterOn			: 1;	// Heater ON
		unsigned __int16 _EnPhotoEvent		: 1;	// Enable Photo Event
		unsigned __int16 _EnStartPrintEvent	: 1;	// Enable Start Print Event
		unsigned __int16 _EnEndPrintEvent	: 1;	// Enable End Print Event
		unsigned __int16 _Rsvd2				: 1;	// Reserved
		unsigned __int16 _EnHVEvent			: 1;	// Enable High Voltage Event
		unsigned __int16 _EnInkLowEvent		: 1;	// Enable Ink Low Event
		unsigned __int16 _EnAtTempEvent		: 1;	// Enable At Tempature Event
		unsigned __int16 _EnHeatOnEvent		: 1;	// Enable Heater On Event
	};

	struct MiscCtrlReg {
		unsigned __int16 _TachRes			: 2;	// tachometer resolution: 0 - 150dpi, 1 - 200dpi, 2 - 300dpi, 3 - 120dpi
		unsigned __int16 _Rsvd				: 14;	// Reserved
	};

	struct RsvdReg {
		unsigned __int16 _Rsvd				: 16;	// Reserved
	};

	struct IntTachGenReg {							// Internal Tach Generator Register
		unsigned __int16 _TachGenFreg		: 16;	// Freguency given by (desired freg / 1.8432) * 2^16
	};

	struct IntPhotoGenReg {						// Internal Photocell Generator Count Register
		unsigned __int16 _PhotoGenFreg		: 16;
	};

	struct ConveyorSpeed {							// Conveyor Speed Register
		unsigned __int16 _Speed				: 16;	// Read as ft/min
	};

	struct FireVibPulse {							// Fire / Vibration Pluse Register
		unsigned __int16 _FirePulseDelay	: 16;	// 
		unsigned __int16 _FirePulseWidth	: 16;	// 
		unsigned __int16 _DampPulseDelay	: 16;	// 
		unsigned __int16 _DampPulseWidth	: 16;	// 
													// Remaining values are for vibration which are no longer used.
		unsigned __int16 _v5				: 16;	// 
		unsigned __int16 _v6				: 16;	// 
		unsigned __int16 _v7				: 16;	// 
		unsigned __int16 _v8				: 16;	// 
	};

	struct SlantRegs {
		unsigned __int32 _Addr1;					// Address of first image WORD 
		unsigned __int32 _Addr2;					// Address of second image WORD
		unsigned __int32 _Incr;					// Address Incr Factor Register
		unsigned __int32 _Adjust;					// Column Adjust Factor Register
	};

	struct ImageRegs {
		unsigned __int32	_PrintDelay;			// (Value = Columns)
		unsigned __int32	_Len;					// (Value = Columns)
		SlantRegs			_SlantRegs;
		unsigned __int32	_Delay;					// Product Length Delay ( Value = Columns )
		unsigned __int32	_Resvr;					// Resvered;
	};

	struct FeriteRegs {
		unsigned __int16 Reg[4];
	};
	
	struct SetupRegs {
		CtrlReg			_CtrlReg;				// Control Register.
		IFCtrlReg		_IFCtrlReg;				// Interface Control Register.
		IFStatusReg		_IFStatusReg;			// Interface Status Register.
		MiscCtrlReg		_MiscCtrlReg;			// Misc Control Register.
		RsvdReg			_RsvdReg;				// Reserved Register
		IntTachGenReg	_IntTachReg;			// Internal Tach Generator Register
		IntPhotoGenReg	_IntPhotoReg;			// Internal Photocell Generator Count Register
		ConveyorSpeed	_ConveyorSpeed;			// Conveyor Speed Register
		FireVibPulse	_FireVibPulse;			// Fire / Vibration Pluse Register
	};

	struct PrintHeadRegs {
		SetupRegs		_SetupRegs;			// Print Head Setup Registers.
		ImageRegs		_ImageRegs;			// Print Head Image Registers.
	};
	
	struct CmdHdr {
		unsigned __int16 Cmd[2];
		unsigned __int16 Addr[2];
		unsigned __int16 Len[2];
	};

	struct StackCmd {
		unsigned __int16 Cmd[2];
		unsigned __int16 Addr[2];
		unsigned __int16 Len[2];
	};

	struct Registers {
		MasterCtrlReg		_MstCtrlReg;		// Master Control Register
		EventPeriod 		_EventPeriod;		// Event Period Register
		CmdHdr				_CmdHdr;			// Command Header Returned;
		MasterCtrlReg		_MstCtrlRegDup;		// Master Control Register (Dup)
		EventTimer			_EventTimer;		// Event Timer Register
		StackCmd			_StackedCmd;		// Stacked Command Register
		UartRegs			_Uart_0;			// Uart 0 Registers;
		PrintHeadRegs		_Print_0_HeadRegs;	// Print Head 0 Registers
		RsvdReg 			_RsvReg2[12];		// Extra Resvered Registers.
		FeriteRegs			_FeriteRegs;		// Ferite Memory Registers.
		UartRegs			_Uart_1;			// Uart 1 Registers;
		PrintHeadRegs		_Print_1_HeadRegs;	// Print Head 1 Registers
	};

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// End Multi Function Print-Head Controller Registers Map
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma pack()
};
#endif