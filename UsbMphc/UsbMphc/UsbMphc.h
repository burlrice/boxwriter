#pragma once

#include <Afxmt.h>

#include "CyAPI.h"
#include "TypeDefs.h"
#include "DeviceListEntry.h"

// CUsbMphc
namespace USB
{
	#define CMD_FAILED			0x55
	#define RESET_FPGA_STATE	0x7F
	#define BEGIN_PROG_FPGA		0x80
	#define WRITE_FPGA			0x81
	#define END_PROG_FPGA		0x82

	class CUsbMphc : public CWinThread
	{
		DECLARE_DYNCREATE(CUsbMphc)

	protected:
		CUsbMphc();           // protected constructor used by dynamic creation

	public:
		virtual ~CUsbMphc();

	protected:
		CCriticalSection m_sync; //CMutex m_sync;
		PrintHeadRegs *m_wrPHRegisters[2];
		PrintHeadRegs *m_rdPHRegisters[2];
		// Some registers (ie ConveyorSpeed ) Reads the speed but writing this register set gate width thus
		// we need a read and write copy of the registers.
		Registers m_rdRegisters;	// We need one copy for writing and one for reading.
		Registers m_wrRegisters;	// We need one copy for writing and one for reading.
		HANDLE m_hPhotoEvent[2];
		HANDLE m_hEOPEvent[2];
		HANDLE m_hSerialEvent[2];
		HANDLE m_hExit;
	public:
		virtual BOOL InitInstance();
		virtual int ExitInstance();

	protected:
		DECLARE_MESSAGE_MAP()
	public:
		CDeviceListEntry *m_pDevice;
		unsigned int m_nEp8Cnt;
		virtual int Run();
		void RegisterPhotoEvent(unsigned int nHead, HANDLE hEvent);
		void RegisterEOPEvent(unsigned int nHead, HANDLE hEvent);
		void RegisterSerialEvent(unsigned int nHead, HANDLE hEvent);
		bool WriteReg( unsigned __int16 nRegAddr, PVOID pRegData, ULONG nBytes);
		bool ReadReg( unsigned __int16 nRegAddr, PVOID pRegData, ULONG nBytes);
		bool ProgramFPGA ( const void *pData, const long nLen );
		bool ReadImage(unsigned int nHead, PVOID pData, ULONG nStartLoc, ULONG &nBytes);
		bool WriteImage(unsigned int nHead, PVOID pData, ULONG nStartLoc, ULONG nBytes);
		bool WriteSetup(unsigned int nHead, SetupRegs tSetup, FireVibPulse tPulseRegs);
		bool EnablePrint(unsigned int nHead, bool bEnabled);
		bool ProgramSlantRegs(unsigned int nHead, SlantRegs SlantRegs);
		bool SetImageLen(unsigned int nHead, unsigned short wImageCols);
		bool SetPrintDelay(unsigned int nHead, unsigned short wDelay);
		bool SetProductLen(unsigned int nHead, unsigned short wProdLen);
		bool SetSerialParams(unsigned int nHead, tagSerialParams Params);
		bool SetSpeedGateWidth(unsigned int nHead, unsigned short wEncRes);
		bool SetStrobe(unsigned int nHead, INT Color);
		bool GetIndicators(unsigned int nHead, tagIndicators& Indicators);
		bool ReadConfiguration(unsigned int nHead, PrintHeadRegs& tConfig);
		bool ReadLineSpeed(unsigned int nHead, double& dSpeed);
		bool ReadSerialData(unsigned int nHead, unsigned char *pData, unsigned int& nBytes);
		bool ClearPrintBuffer(unsigned int nHead, ULONG nStartLoc, ULONG nBytes);
		unsigned int ComputeSpeedGateWidth(unsigned short nEncoderRes);
		void Shutdown(void);
	protected:
		void ProcessEventTriggers(void);
	};

};
