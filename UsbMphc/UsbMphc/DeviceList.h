/***************************************************************
 * Name:      DeviceList.h
 * Purpose:   Header for DeviceList
 * Author:    Chris Hodge (chodge@risc-design.com)
 * Created:   2010-05-12
 * Copyright: Chris Hodge (www.risc-design.com)
 * License:   All rights are reserved. Reproduction in whole or 
 *            part is prohibited without the written consent of 
 *            the copyright owner.
 **************************************************************/

#ifndef _2AE411BA_7A90_4f8f_B096_338093BA6CFA_DEVICELIST_H
#define _2AE411BA_7A90_4f8f_B096_338093BA6CFA_DEVICELIST_H

#include <afxtempl.h>
#include "DeviceListEntry.h"
namespace USB {
	class CDeviceList
	{

	private:

	protected:
		CArray<CDeviceListEntry,CDeviceListEntry > m_list;
		CCyUSBDevice *m_pUSBDevice;
		__int16 m_nVendorID;
		__int16 m_nProductID;

	public:
	   CDeviceList(__int16 nVendorID, __int16 nProductID);
	   virtual ~CDeviceList();
	   int Initialize();
	};
};
#endif