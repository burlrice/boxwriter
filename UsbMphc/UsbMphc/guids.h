// guids.h -- WMI Guids for FxMphc driver
// Copyright (C) 1999 by Walter Oney
// All rights reserved

#ifndef GUIDS_H
#define GUIDS_H

// {18ACEBC3-76CC-11D6-A122-0060674BA976}
DEFINE_GUID(GUID_INTERFACE_FXMPHC, 0x18acebc3L, 0x76cc, 0x11d6, 0xa1, 0x22, 0x00, 0x60, 0x67, 0x4b, 0xa9, 0x76);

#endif
