
// Ink license generator.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CInkApp:
// See Ink license generator.cpp for the implementation of this class
//

class CInkApp : public CWinApp
{
public:
	CInkApp();

// Overrides
public:
	virtual BOOL InitInstance();

	CString GetRegSection () const { return  _T ("SOFTWARE\\FoxJet\\Ink license generator"); }
// Implementation

	DECLARE_MESSAGE_MAP()
	afx_msg void OnHelpAbout();
};

extern CInkApp theApp;