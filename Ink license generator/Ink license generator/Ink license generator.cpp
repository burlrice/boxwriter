
// Ink license generator.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Ink license generator.h"
#include "InkDlg.h"
#include "afxdialogex.h"
#include "zlib.h"
#include <math.h>
#include <string>
#include <vector>
#include "Base32.h"
#include "Registry.h"
#include "TemplExt.h"
#include "GenerateListDlg.h"
#include "PasswordDlg.h"
#include "ExcelFormat.h"
#include "Database.h"
#include "Utils.h"
#include "Parse.h"
#include "AboutDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CInkApp

BEGIN_MESSAGE_MAP(CInkApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
	ON_COMMAND(ID_HELP_ABOUT, &CInkApp::OnHelpAbout)
END_MESSAGE_MAP()


// CInkApp construction

CInkApp::CInkApp()
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CInkApp object

CInkApp theApp;


// CInkApp initialization

BOOL CInkApp::InitInstance()
{
	CWinApp::InitInstance();


	AfxEnableControlContainer();

	// Create the shell manager, in case the dialog contains
	// any shell tree view or shell list view controls.
	CShellManager *pShellManager = new CShellManager;

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	CInkDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Delete the shell manager created above.
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}



void CInkApp::OnHelpAbout()
{
	CString str;

	if (m_pMainWnd)
		m_pMainWnd->GetWindowText (str);

	FoxjetCommon::CAboutDlg dlg (FoxjetCommon::verApp, CStringArray (), str);

	dlg.m_strApp = str;
	dlg.DoModal ();
}
