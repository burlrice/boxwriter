
// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions


#include <afxdisp.h>        // MFC Automation classes



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC support for ribbons and control bars

#include "AnsiString.h"
#include "Registry.h"
#include "Debug.h"


using namespace FoxjetDatabase::Encrypted::InkCodes;

#define TRACEF(s) Trace ((s), _T (__FILE__), __LINE__)
void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine);
void Trace (LPCSTR lpsz, LPCTSTR lpszFile, ULONG lLine);

CString GetHomeDir ();
DWORD GetFileSize (const CString & strFile);

CString ToString (__int8 n);
CString ToString (__int16 n);
CString ToString (__int32 n);
CString ToString (__int64 n);
CString ToString (long n);

CString ToString (unsigned __int8 n);
CString ToString (unsigned __int16 n);
CString ToString (unsigned __int32 n);
CString ToString (unsigned __int64 n);
CString ToString (unsigned long n);

CString ToString (double d);
CString ToString (long double d);
CString ToString (float f);

ULONG CalcPrime (int nExponent);


