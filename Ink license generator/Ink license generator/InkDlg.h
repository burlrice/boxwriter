
// InkDlg.h : header file
//

#pragma once

#include "maskededit_src\OXMaskedEdit.h"

// CInkDlg dialog
class CInkDlg : public CDialogEx
{
// Construction
public:
	CInkDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CInkDlg ();

// Dialog Data
	enum { IDD = IDD_INKLICENSEGENERATOR_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	int GetManufacturer () const;
	void SetManufacturer (int n);

	int GetExpPeriod () const;
	void SetExpPeriod (int n);

	int GetExpMonths () const;
	CString AddMonths (CTime & tm, int nMonths) const;

// Implementation
protected:
	HICON m_hIcon;

	CTime m_tmDay;
	int m_nQuantity;
	std::vector <int> m_vSerial;
	CString m_strBatchCode;
	CString m_strUseBy;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:

	afx_msg void OnBnClickedSave();
	afx_msg void OnDtnDatetimechangeDay(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedExp12();
	afx_msg void OnBnClickedExp18();
	afx_msg void OnBnClickedExp24();
	afx_msg void OnBnClickedExit();
	afx_msg void OnFileLabelSoftware();
	afx_msg void OnBnClickedLabelSoftware();
	virtual void OnOK();
	afx_msg void OnFileExit();
};
