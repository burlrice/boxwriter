//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Ink license generator.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_INKLICENSEGENERATOR_DIALOG  102
#define IDR_MAINFRAME                   128
#define IDD_LIST                        129
#define IDM_MAINMENU                    130
#define IDD_PASSWORDS                   131
#define IDR_DATABASE                    132
#define IDR_ACCELERATOR1                133
#define BTN_ENCODE                      1001
#define TXT_INKCODE                     1002
#define BTN_DECODE                      1003
#define TXT_SERIAL                      1004
#define TXT_QUANTITY                    1004
#define RDO_FOXJET                      1005
#define RDO_DIAGRAPH                    1006
#define RDO_EXP_12                      1007
#define RDO_EXP_18                      1008
#define RDO_EXP_24                      1009
#define DT_DAY                          1010
#define TXT_BATCH_CODE                  1011
#define TXT_RANGE                       1012
#define TXT_USE_BY                      1012
#define BTN_GENERATE                    1013
#define BTN_GENERATE_DISABLE            1014
#define BTN_SAVE                        1014
#define TXT_CODES                       1015
#define BTN_LABEL_SOFTWARE              1015
#define TXT_DISABLE                     1016
#define BTN_EXIT                        1016
#define TXT_MACADDRESS                  1018
#define TXT_BATCH_NUMBER                1018
#define CB_ENABLE                       1020
#define TXT_ENABLE                      1021
#define CB_DISABLE                      1022
#define DT_EXP                          1023
#define ID_FILE_EXIT                    32771
#define ID_FILE_PASSWORDS               32772
#define ID_FILE_LABEL_SOFTWARE          32773
#define ID_HELP_ABOUT                   32774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1025
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
