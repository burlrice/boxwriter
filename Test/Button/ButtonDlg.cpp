// ButtonDlg.cpp : implementation file
//

#include "stdafx.h"
#include <shlwapi.h>
#include "Button.h"
#include "ButtonDlg.h"
#include "ximage.h"
#include "Color.h"
#include "WinMsg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define KEYEVENTF_UNICODE	0x0004
#define ARRAYSIZE(s) (sizeof ((s)) / sizeof ((s) [0]))

extern CButtonApp theApp;

inline CString FormatMessage (DWORD dwError)
{
	LPVOID lpMsgBuf = NULL;
	CString str;

	str.Format (_T ("[%d: 0x%p] "), dwError, dwError);

	::FormatMessage (
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, dwError, MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT), 
		(LPTSTR)&lpMsgBuf, 0, NULL);
	str += (LPCTSTR)lpMsgBuf;
	::LocalFree( lpMsgBuf );

	return str;
}


#define FORMATMESSAGE(dw)	FormatMessage ((dw))

bool WriteProfileInt (LPCTSTR szKey, UINT nData) 
{
	const CString strKey = _T ("Software\\Foxjet\\Control\\Settings\\Colors");
	HKEY hKey;
	HKEY hKeyRoot = HKEY_CURRENT_USER;

	// if strKey created/opened okay
	if (::RegCreateKey (hKeyRoot, strKey, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = sizeof (DWORD);
		DWORD dwData = nData;

		LONG lStatus = ::RegSetValueEx (hKey, szKey, 0, REG_DWORD, 
			(const BYTE *) &dwData, dwSize);
		::RegCloseKey (hKey);
		return lStatus == ERROR_SUCCESS;
	}

	return false;
}

UINT GetProfileInt (LPCTSTR szKey, UINT nDefault) 
{
	const CString strKey = _T ("Software\\Foxjet\\Control\\Settings\\Colors");
	HKEY hKey;
	HKEY hKeyRoot = HKEY_CURRENT_USER;
	UINT nResult = nDefault;

	// if strKey was in the registry
	if (::RegCreateKey (hKeyRoot, strKey, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = sizeof (DWORD), dwType = REG_DWORD;
		DWORD dwData;

		if (::RegQueryValueEx (hKey, szKey, NULL, &dwType, 
			(LPBYTE) &dwData, &dwSize) == ERROR_SUCCESS) 
		{
			nResult = (UINT)dwData;
		}

		::RegCloseKey (hKey);
	}

	return nResult;
}

/////////////////////////////////////////////////////////////////////////////
// CButtonDlg dialog

CButtonDlg::CButtonDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CButtonDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CButtonDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CButtonDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CButtonDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	DDX_Control(pDX, BTN_MAIN, m_btn);
}

BEGIN_MESSAGE_MAP(CButtonDlg, CDialog)
	//{{AFX_MSG_MAP(CButtonDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_EN_CHANGE(TXT_WIDTH, OnChangeWidth)
	ON_EN_CHANGE(TXT_HEIGHT, OnChangeHeight)
	ON_BN_CLICKED(CHK_ENABLED, OnEnabled)
	ON_EN_CHANGE(TXT_TEXT, OnChangeText)
	ON_BN_CLICKED(BTN_FONT, OnFont)
	ON_BN_CLICKED(BTN_COLOR, OnColor)
	ON_BN_CLICKED(BTN_BORDER, OnBorder)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(BTN_BACKGROUND, OnBackground)
	ON_BN_CLICKED(BTN_EXPORT, OnExport)
	ON_BN_CLICKED(BTN_SHADED, OnShaded)
	ON_BN_CLICKED(BTN_MAIN, OnMain)
	ON_BN_CLICKED(BTN_CLICKED, OnClicked)
	ON_BN_CLICKED(BTN_CLICKED_BORDER, OnClickedBorder)
	ON_BN_CLICKED(BTN_CONFIG_FILL, OnConfigFill)
	ON_BN_CLICKED(BTN_CONFIG_OUTLINE, OnConfigOutline)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CButtonDlg message handlers

BOOL CButtonDlg::OnInitDialog()
{
	CxImage img;
	CString strFile = theApp.GetProfileString (_T ("settings"), _T ("file"), _T (""));
	CString strText = theApp.GetProfileString (_T ("settings"), _T ("text"), _T ("Button"));

	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	SetDlgItemInt (TXT_WIDTH, theApp.GetProfileInt (_T ("settings"), _T ("cx"), 64));
	SetDlgItemInt (TXT_HEIGHT, theApp.GetProfileInt (_T ("settings"), _T ("cy"), 64));
	SetDlgItemText (TXT_FILE, strFile);
	SetDlgItemText (TXT_TEXT, strText);
	
	{
		tButtonStyle tStyle;

		m_styleDefault.GetButtonStyle (&tStyle);

		tStyle.m_tColorFace.m_tEnabled		= GetProfileInt (_T ("button face"), RGB(0x80, 0x80, 0xFF));
		tStyle.m_tColorBorder.m_tEnabled	= GetProfileInt (_T ("button border"), RGB(0x40, 0x40, 0xFF));

		tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
		tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);

		tStyle.m_tColorFace.m_tPressed		= GetProfileInt (_T ("button face clicked"), RGB(0xFF, 0x80, 0x80));
		tStyle.m_tColorBorder.m_tPressed	= GetProfileInt (_T ("button border clicked"), RGB(0xFF, 0x40, 0x40));

		tStyle.m_tColorBack.m_tEnabled		= 
		tStyle.m_tColorBack.m_tClicked		= 
		tStyle.m_tColorBack.m_tPressed		= 
		tStyle.m_tColorBack.m_tDisabled		= 
		tStyle.m_tColorBack.m_tHot			= GetProfileInt (_T ("background"), ::GetSysColor (COLOR_BTNFACE));

		m_styleDefault.SetButtonStyle (&tStyle);
	}

	m_hbrDlg = ::CreateSolidBrush (GetProfileInt (_T ("background"), ::GetSysColor (COLOR_BTNFACE)));

	if (img.Load (strFile))
		m_btn.SetBitmap (img.MakeBitmap ());

	m_btn.SetRoundButtonStyle(&m_styleDefault);
	m_btn.SetWindowText (strText);
	m_btn.Invalidate ();
	m_btn.RedrawWindow ();
	UpdateUI ();

	{
		COLORREF rgb = GetProfileInt (_T ("button font"), Color::rgbWhite);

		tColorScheme tColor = { rgb, rgb, rgb, rgb, rgb };
		m_btn.SetTextColor (&tColor);
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CButtonDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CButtonDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CButtonDlg::OnBrowse() 
{
	CString str;

	GetDlgItemText (TXT_FILE, str);

	CFileDialog dlg (TRUE, _T ("*.bmp"), str, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("Bitmaps (*.bmp)|*.bmp|All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		CxImage img;

		theApp.WriteProfileString (_T ("settings"), _T ("file"), dlg.GetPathName ());
		SetDlgItemText (TXT_FILE, dlg.GetPathName ());
	
		if (img.Load (dlg.GetPathName ()))
			m_btn.SetBitmap (img.MakeBitmap ());
	}
}

void CButtonDlg::OnChangeWidth() 
{
	OnChangeHeight ();
}

void CButtonDlg::OnChangeHeight() 
{
	CString strWidth, strHeight;

	GetDlgItemText (TXT_WIDTH, strWidth);
	GetDlgItemText (TXT_HEIGHT, strHeight);
	
	int cx = _tcstoul (strWidth, NULL, 10);
	int cy = _tcstoul (strHeight, NULL, 10);

	if (cx > 5 && cy > 5) {
		theApp.WriteProfileInt (_T ("settings"), _T ("cx"), cx);
		theApp.WriteProfileInt (_T ("settings"), _T ("cy"), cy);

		if (CWnd * p = GetDlgItem (BTN_MAIN))
			p->SetWindowPos (NULL, 0, 0, cx, cy, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
	}
}

void CButtonDlg::OnEnabled() 
{
	if (CButton * p = (CButton *)GetDlgItem (CHK_ENABLED)) 
		m_btn.EnableWindow (p->GetCheck () == 0 ? TRUE : FALSE);
}

void CButtonDlg::OnChangeText() 
{	
	CString str;

	GetDlgItemText (TXT_TEXT, str);
	m_btn.SetWindowText (str);
	theApp.WriteProfileString (_T ("settings"), _T ("text"), str);
}

void CButtonDlg::OnFont() 
{
	CColorDialog dlg;
	
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	dlg.m_cc.rgbResult = GetProfileInt (_T ("button font"), Color::rgbWhite);
	
	if (dlg.DoModal () == IDOK) {
		COLORREF rgb = dlg.m_cc.rgbResult;
		tColorScheme tColor = { rgb, rgb, rgb, rgb, rgb };

		WriteProfileInt (_T ("button font"), dlg.m_cc.rgbResult);
		m_btn.SetTextColor (&tColor);
		m_btn.Invalidate ();
		m_btn.RedrawWindow ();
		UpdateUI ();
	}
}

void CButtonDlg::OnColor() 
{
	CColorDialog dlg;
	
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	dlg.m_cc.rgbResult = GetProfileInt (_T ("button face"), RGB(0x80, 0x80, 0xFF));
	
	if (dlg.DoModal () == IDOK) {
		COLORREF rgb = dlg.m_cc.rgbResult;
		FoxjetUtils::tButtonStyle tStyle;

		WriteProfileInt (_T ("button face"), rgb);

		m_styleDefault.GetButtonStyle (&tStyle);
		tStyle.m_tColorBack.m_tEnabled		= 
		tStyle.m_tColorBack.m_tClicked		= 
		tStyle.m_tColorBack.m_tPressed		= 
		tStyle.m_tColorBack.m_tDisabled		= 
		tStyle.m_tColorBack.m_tHot			= GetProfileInt (_T ("background"), ::GetSysColor (COLOR_BTNFACE));
		tStyle.m_tColorFace.m_tEnabled = rgb;
		m_styleDefault.SetButtonStyle (&tStyle);
		m_btn.SetRoundButtonStyle (&m_styleDefault);
		m_btn.Invalidate ();
		m_btn.RedrawWindow ();
		UpdateUI ();
	}
}

void CButtonDlg::OnBorder() 
{
	CColorDialog dlg;
	
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	dlg.m_cc.rgbResult = GetProfileInt (_T ("button border"), RGB(0x40, 0x40, 0xFF));
	
	if (dlg.DoModal () == IDOK) {
		COLORREF rgb = dlg.m_cc.rgbResult;
		FoxjetUtils::tButtonStyle tStyle;

		WriteProfileInt (_T ("button border"), rgb);

		m_styleDefault.GetButtonStyle (&tStyle);
		tStyle.m_tColorBack.m_tEnabled		= 
		tStyle.m_tColorBack.m_tClicked		= 
		tStyle.m_tColorBack.m_tPressed		= 
		tStyle.m_tColorBack.m_tDisabled		= 
		tStyle.m_tColorBack.m_tHot			= GetProfileInt (_T ("background"), ::GetSysColor (COLOR_BTNFACE));
		tStyle.m_tColorBorder.m_tEnabled = rgb;
		m_styleDefault.SetButtonStyle (&tStyle);
		m_btn.SetRoundButtonStyle (&m_styleDefault);
		m_btn.Invalidate ();
		m_btn.RedrawWindow ();
		UpdateUI ();
	}
}

HBRUSH CButtonDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	if (pWnd->GetDlgCtrlID() != TXT_COLORS) {
		switch (nCtlColor) {
		case CTLCOLOR_DLG:
		case CTLCOLOR_STATIC:
		case CTLCOLOR_BTN:
			pDC->SetBkMode (TRANSPARENT);
			return m_hbrDlg;
		}
	}

	return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
}

void CButtonDlg::OnBackground() 
{
	CColorDialog dlg;
	
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	dlg.m_cc.rgbResult = GetProfileInt (_T ("background"), ::GetSysColor (COLOR_BTNFACE));
	
	if (dlg.DoModal () == IDOK) {
		COLORREF rgb = dlg.m_cc.rgbResult;
		tColorScheme tColor = { rgb, rgb, rgb, rgb, rgb };
		FoxjetUtils::tButtonStyle tStyle;

		if (m_hbrDlg) 
			::DeleteObject (m_hbrDlg);

		m_hbrDlg = ::CreateSolidBrush (dlg.m_cc.rgbResult);
		WriteProfileInt (_T ("background"), dlg.m_cc.rgbResult);
		Invalidate ();
		RedrawWindow ();

		m_styleDefault.GetButtonStyle (&tStyle);
		tStyle.m_tColorBack.m_tEnabled		= 
		tStyle.m_tColorBack.m_tClicked		= 
		tStyle.m_tColorBack.m_tPressed		= 
		tStyle.m_tColorBack.m_tDisabled		= 
		tStyle.m_tColorBack.m_tHot			= GetProfileInt (_T ("background"), ::GetSysColor (COLOR_BTNFACE));
		m_styleDefault.SetButtonStyle (&tStyle);
		m_btn.SetRoundButtonStyle (&m_styleDefault);
		m_btn.Invalidate ();
		m_btn.RedrawWindow ();
		UpdateUI ();
	}
}

BOOL SetPrivilege(
    HANDLE hToken,          // access token handle
    LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
    BOOL bEnablePrivilege   // to enable or disable privilege
    ) 
{
    TOKEN_PRIVILEGES tp;
    LUID luid;

    if ( !LookupPrivilegeValue( 
            NULL,            // lookup privilege on local system
            lpszPrivilege,   // privilege to lookup 
            &luid ) )        // receives LUID of privilege
    {
        printf("LookupPrivilegeValue error: %u\n", GetLastError() ); 
        return FALSE; 
    }

    tp.PrivilegeCount = 1;
    tp.Privileges[0].Luid = luid;
    if (bEnablePrivilege)
        tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
    else
        tp.Privileges[0].Attributes = 0;

    // Enable the privilege or disable all privileges.

    if ( !AdjustTokenPrivileges(
           hToken, 
           FALSE, 
           &tp, 
           sizeof(TOKEN_PRIVILEGES), 
           (PTOKEN_PRIVILEGES) NULL, 
           (PDWORD) NULL) )
    { 
          printf("AdjustTokenPrivileges error: %u\n", GetLastError() ); 
          return FALSE; 
    } 

    if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)

    {
          printf("The token does not have the specified privilege. \n");
          return FALSE;
    } 

    return TRUE;
}

void CButtonDlg::OnExport() 
{
	CString strFile = theApp.GetProfileString (_T ("settings"), _T ("export file"), _T ("color.reg"));
	CFileDialog dlg (FALSE, _T ("*.*"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		_T ("All files (*.*)|*.*||"), this);
	
	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();
		theApp.WriteProfileString (_T ("settings"), _T ("export file"), strFile);
		const CString strKey = _T ("SOFTWARE\\Foxjet\\Control\\Settings\\Colors");
		HKEY hKey;
		HANDLE ProcessToken;

		if (::RegCreateKey (HKEY_CURRENT_USER, strKey, &hKey) == ERROR_SUCCESS) {
			TCHAR sz [MAX_PATH] = { 0 };

			if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &ProcessToken)) {
				SetPrivilege(ProcessToken, SE_BACKUP_NAME, TRUE);

				::DeleteFile (strFile);

				if (::RegSaveKey (hKey, strFile, NULL) != ERROR_SUCCESS) {
					MessageBox (FORMATMESSAGE (::GetLastError ()));
				}
				
				::RegCloseKey (hKey);
				_tcscpy (sz, _T ("%SystemRoot%"));

				if (HMODULE hDLL = ::LoadLibrary (_T ("Shell32.dll"))) {
					typedef int (CALLBACK * LPFCT) (LPCTSTR pszString, UINT cbSize);

					if (LPFCT lp = (LPFCT)GetProcAddress (hDLL, "DoEnvironmentSubstW")) 
						(* lp) (sz, ARRAYSIZE (sz));

					::FreeLibrary (hDLL);
				}

				CString strCmdLine = CString (sz) + _T ("\\system32\\notepad.exe \"") + strFile + _T ("\"");
				STARTUPINFO si;     
				PROCESS_INFORMATION pi; 
				TCHAR szCmdLine [0xFF];

				_tcsncpy (szCmdLine, strCmdLine, 0xFF);
				memset (&pi, 0, sizeof(pi));
				memset (&si, 0, sizeof(si));
				si.cb = sizeof(si);     
				si.dwFlags = STARTF_USESHOWWINDOW;     
				si.wShowWindow = SW_SHOW;     

				if (!::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) 
					MessageBox (_T ("failed to start: \n") + strCmdLine);
			}
		}
	}
}

void CButtonDlg::OnShaded() 
{
	CColorDialog dlg;
	
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	dlg.m_cc.rgbResult = GetProfileInt (_T ("shaded"), ::GetSysColor (COLOR_BTNFACE));
	
	if (dlg.DoModal () == IDOK) {
		COLORREF rgb = dlg.m_cc.rgbResult;

		WriteProfileInt (_T ("shaded"), dlg.m_cc.rgbResult);
		
		/*
		tColorScheme tColor = { rgb, rgb, rgb, rgb, rgb };
		FoxjetUtils::tButtonStyle tStyle;

		if (m_hbrDlg) 
			::DeleteObject (m_hbrDlg);

		m_hbrDlg = ::CreateSolidBrush (dlg.m_cc.rgbResult);
		Invalidate ();
		RedrawWindow ();

		m_styleDefault.GetButtonStyle (&tStyle);
		tStyle.m_tColorBack.m_tEnabled		= 
		tStyle.m_tColorBack.m_tClicked		= 
		tStyle.m_tColorBack.m_tPressed		= 
		tStyle.m_tColorBack.m_tDisabled		= 
		tStyle.m_tColorBack.m_tHot			= GetProfileInt (_T ("shaded"), ::GetSysColor (COLOR_BTNFACE));
		m_styleDefault.SetButtonStyle (&tStyle);
		m_btn.SetRoundButtonStyle (&m_styleDefault);
		m_btn.Invalidate ();
		m_btn.RedrawWindow ();
		*/
		UpdateUI ();
	}
}

void CButtonDlg::OnMain() 
{
}

void CButtonDlg::UpdateUI()
{
	CString str;
	CString strKey [] = 
	{	
		_T ("button font"),
		_T ("button face"),
		_T ("button border"),
		_T ("background"),
		_T ("shaded"),
		_T ("button face clicked"), 
		_T ("button border clicked"),
		_T ("config::text::fill"),
		_T ("config::text::outline"),
	};

	for (int i = 0; i < ARRAYSIZE (strKey); i++) {
		CString strTmp;
		int n = GetProfileInt (strKey [i], -1);

		if (n != -1) {
			COLORREF rgb = (COLORREF)n;
			strTmp.Format (_T ("%s: %02X%02X%02X"), strKey [i], GetRValue (rgb), GetGValue (rgb), GetBValue (rgb));
		}
		else
			strTmp = strKey [i] + _T (": [not set]");

		str += (strTmp + _T ("\r\n"));
	}

	SetDlgItemText (TXT_COLORS, str);
}

void CButtonDlg::OnClicked() 
{
	const CString strKey = _T ("button face clicked");
	CColorDialog dlg;
	
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	dlg.m_cc.rgbResult = GetProfileInt (strKey, RGB(0xFF, 0x80, 0x80));
	
	if (dlg.DoModal () == IDOK) {
		COLORREF rgb = dlg.m_cc.rgbResult;
		FoxjetUtils::tButtonStyle tStyle;

		WriteProfileInt (strKey, dlg.m_cc.rgbResult);

		m_styleDefault.GetButtonStyle (&tStyle);
		tStyle.m_tColorFace.m_tPressed = rgb;
		m_styleDefault.SetButtonStyle (&tStyle);
		m_btn.SetRoundButtonStyle (&m_styleDefault);
		m_btn.Invalidate ();
		m_btn.RedrawWindow ();
		UpdateUI ();
	}
}

void CButtonDlg::OnClickedBorder() 
{
	const CString strKey = _T ("button border clicked");
	CColorDialog dlg;
	
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	dlg.m_cc.rgbResult = GetProfileInt (strKey, RGB(0xFF, 0x80, 0x80));
	
	if (dlg.DoModal () == IDOK) {
		COLORREF rgb = dlg.m_cc.rgbResult;
		FoxjetUtils::tButtonStyle tStyle;

		WriteProfileInt (strKey, dlg.m_cc.rgbResult);

		m_styleDefault.GetButtonStyle (&tStyle);
		tStyle.m_tColorBorder.m_tPressed = rgb;
		m_styleDefault.SetButtonStyle (&tStyle);
		m_btn.SetRoundButtonStyle (&m_styleDefault);
		m_btn.Invalidate ();
		m_btn.RedrawWindow ();
		UpdateUI ();
	}
}

void CButtonDlg::OnConfigFill() 
{
	const CString strKey = _T ("config::text::fill");
	CColorDialog dlg;
	
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	dlg.m_cc.rgbResult = GetProfileInt (strKey, Color::rgbBlue);
	
	if (dlg.DoModal () == IDOK) {
		WriteProfileInt (strKey, dlg.m_cc.rgbResult);
		UpdateUI ();
		::PostMessage (HWND_BROADCAST, WM_SYSTEM_COLOR_CHANGE, 0, 0);
	}
}

void CButtonDlg::OnConfigOutline() 
{
	const CString strKey = _T ("config::text::outline");
	CColorDialog dlg;
	
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	dlg.m_cc.rgbResult = GetProfileInt (strKey, Color::rgbLightGray);
	
	if (dlg.DoModal () == IDOK) {
		WriteProfileInt (strKey, dlg.m_cc.rgbResult);
		UpdateUI ();
		::PostMessage (HWND_BROADCAST, WM_SYSTEM_COLOR_CHANGE, 0, 0);
	}
}
