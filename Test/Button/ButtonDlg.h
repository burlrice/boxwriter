// ButtonDlg.h : header file
//

#if !defined(AFX_BUTTONDLG_H__A5D2B92D_4269_465D_9517_EEB6961EA827__INCLUDED_)
#define AFX_BUTTONDLG_H__A5D2B92D_4269_465D_9517_EEB6961EA827__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxwin.h>
#include "RoundButton2.h"

using namespace FoxjetUtils;

/////////////////////////////////////////////////////////////////////////////
// CButtonDlg dialog

class CButtonDlg : public CDialog
{
// Construction
public:
	void UpdateUI();
	CButtonDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CButtonDlg)
	enum { IDD = IDD_BUTTON_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CButtonDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	CRoundButtonStyle m_styleDefault;
	CRoundButton2 m_btn;
	HBRUSH m_hbrDlg;

	// Generated message map functions
	//{{AFX_MSG(CButtonDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBrowse();
	afx_msg void OnChangeWidth();
	afx_msg void OnChangeHeight();
	afx_msg void OnEnabled();
	afx_msg void OnChangeText();
	afx_msg void OnFont();
	afx_msg void OnColor();
	afx_msg void OnBorder();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBackground();
	afx_msg void OnExport();
	afx_msg void OnShaded();
	afx_msg void OnMain();
	afx_msg void OnClicked();
	afx_msg void OnClickedBorder();
	afx_msg void OnConfigFill();
	afx_msg void OnConfigOutline();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BUTTONDLG_H__A5D2B92D_4269_465D_9517_EEB6961EA827__INCLUDED_)
