; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CButtonDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Button.h"

ClassCount=2
Class1=CButtonApp
Class2=CButtonDlg

ResourceCount=3
Resource2=IDR_MAINFRAME
Resource3=IDD_BUTTON_DIALOG

[CLS:CButtonApp]
Type=0
HeaderFile=Button.h
ImplementationFile=Button.cpp
Filter=N

[CLS:CButtonDlg]
Type=0
HeaderFile=ButtonDlg.h
ImplementationFile=ButtonDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=BTN_BACKGROUND



[DLG:IDD_BUTTON_DIALOG]
Type=1
Class=CButtonDlg
ControlCount=24
Control1=BTN_FONT,button,1342242816
Control2=BTN_MAIN,button,1342242944
Control3=IDC_STATIC,button,1342177287
Control4=BTN_COLOR,button,1342242816
Control5=BTN_BORDER,button,1342242816
Control6=BTN_SHADED,button,1342242816
Control7=IDC_STATIC,button,1342177287
Control8=BTN_CLICKED,button,1342242816
Control9=BTN_CLICKED_BORDER,button,1342242816
Control10=TXT_COLORS,edit,1350633604
Control11=TXT_TEXT,edit,1350631552
Control12=CHK_ENABLED,button,1342242819
Control13=BTN_BACKGROUND,button,1342242816
Control14=BTN_EXPORT,button,1073807360
Control15=IDC_STATIC,static,1342308352
Control16=TXT_WIDTH,edit,1350631552
Control17=IDC_STATIC,static,1342308352
Control18=TXT_HEIGHT,edit,1350631552
Control19=TXT_FILE,edit,1350633600
Control20=BTN_BROWSE,button,1342242816
Control21=IDC_STATIC,static,1342308352
Control22=IDC_STATIC,button,1342177287
Control23=BTN_CONFIG_FILL,button,1342242816
Control24=BTN_CONFIG_OUTLINE,button,1342242816

