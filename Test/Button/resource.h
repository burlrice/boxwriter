//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Button.rc
//
#define IDD_BUTTON_DIALOG               102
#define IDR_MAINFRAME                   128
#define BTN_MAIN                        1000
#define TXT_WIDTH                       1001
#define TXT_HEIGHT                      1002
#define TXT_FILE                        1003
#define BTN_BROWSE                      1004
#define CHK_ENABLED                     1005
#define TXT_TEXT                        1006
#define BTN_FONT                        1007
#define BTN_COLOR                       1008
#define BTN_BORDER                      1009
#define BTN_BACKGROUND                  1010
#define BTN_EXPORT                      1011
#define BTN_SHADED                      1012
#define TXT_COLORS                      1013
#define BTN_CLICKED                     1014
#define BTN_CLICKED_BORDER              1015
#define BTN_CONFIG_FILL                 1016
#define BTN_CONFIG_OUTLINE              1017

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
