// stdafx.cpp : source file that includes just the standard includes
//	Compact.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include <afxdao.h>
#include <afxdb.h>

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file

#define a2w(s) (s)

using namespace std;

CString GetHomeDir ()
{
	TCHAR sz [MAX_PATH] = { 0 };

	HMODULE hModule = ::GetModuleHandle (NULL);
	::GetModuleFileName (hModule, sz, ARRAYSIZE (sz) - 1);

	const CString strPath (sz);
	CString strPathLower (sz);
	strPathLower.MakeLower ();

	VERIFY (::GetFileTitle (strPath, sz, ARRAYSIZE (sz) - 1) == 0);
	
	CString strTitle (sz);
	strTitle.MakeLower ();

	int nIndex = strPathLower.Find (strTitle);

	ASSERT (nIndex != -1);
	CString str = strPath.Left (nIndex - 1);

	return str;
}

void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	CString str, strFile (lpszFile);

	int nIndex = strFile.ReverseFind ('\\');

	if (nIndex != -1)
		strFile = strFile.Mid (nIndex + 1);

	str.Format (_T ("%s(%d): %s\n"), strFile, lLine, lpsz);

	if (FILE * fp = _tfopen (GetHomeDir () + _T ("\\Compact.txt"), _T ("a"))) {
		fwrite ((LPVOID)(LPCTSTR)str, str.GetLength (), 1, fp);
		fclose (fp);
	}

	::OutputDebugString (str);
	cout << (LPCTSTR)str;
}

CString FormatException (CException * p, LPCTSTR pszFile, ULONG nLine)
{
	CString str;
	CString strClass = _T ("[Unknown]");

	if (p) {
		ASSERT (p);
		const UINT nSize = 512;
		TCHAR szMsg [nSize];

		strClass = a2w (p->GetRuntimeClass ()->m_lpszClassName);
		p->GetErrorMessage (szMsg, nSize, NULL);

		#if 1 //def _DEBUG
		CRuntimeClass * pClass = p->GetRuntimeClass ();
		CString strMsg (szMsg);

		if (p->IsKindOf (RUNTIME_CLASS (CDaoException))) {
			CDaoException * pDao = (CDaoException *)p;

			if (pDao->m_pErrorInfo) {
				CString str;

				str.Format (
					_T ("\n")
					_T ("m_lErrorCode = %u (0x%08X)\n")
					_T ("m_strSource = %s\n")
					_T ("m_strDescription = %s\n")
					_T ("m_strHelpFile = %s\n")
					_T ("m_lHelpContext = %u (0x%08X)"),
					pDao->m_pErrorInfo->m_lErrorCode,	pDao->m_pErrorInfo->m_lErrorCode,
					pDao->m_pErrorInfo->m_strSource,
					pDao->m_pErrorInfo->m_strDescription,
					pDao->m_pErrorInfo->m_strHelpFile,
					pDao->m_pErrorInfo->m_lHelpContext, pDao->m_pErrorInfo->m_lHelpContext);
				strMsg += str;
			}
		}
		else if (p->IsKindOf (RUNTIME_CLASS (CDBException))) {
			CDBException * pDB = (CDBException *)p;
			CString str, strRetCode = "Unknown";

			struct EXECEPTIONSTRUCT
			{
				EXECEPTIONSTRUCT (RETCODE nCode, LPCTSTR lpsz) 
					: m_nCode (nCode), m_str (lpsz) 
				{ 
				}

				RETCODE m_nCode;
				CString m_str;
			} static const e [] = 
			{
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_API_CONFORMANCE,		_T ("The driver for a CDatabase::OpenEx or CDatabase::Open call does not conform to required ODBC API Conformance level 1 (SQL_OAC_LEVEL1).")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_CONNECT_FAIL,			_T ("Connection to the data source failed. You passed a NULL CDatabase pointer to your recordset constructor and the subsequent attempt to create a connection based on GetDefaultConnect failed. ")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_DATA_TRUNCATED,			_T ("You requested more data than you have provided storage for. For information on increasing the provided data storage for CString or CByteArray data types, see the nMaxLength argument for RFX_Text and RFX_Binary under �Macros and Globals.�")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_DYNASET_NOT_SUPPORTED,	_T ("A call to CRecordset::Open requesting a dynaset failed. Dynasets are not supported by the driver.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_EMPTY_COLUMN_LIST,		_T ("You attempted to open a table (or what you gave could not be identified as a procedure call or SELECT statement) but there are no columns identified in record field exchange (RFX) function calls in your DoFieldExchange override.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_FIELD_SCHEMA_MISMATCH,	_T ("The type of an RFX function in your DoFieldExchange override is not compatible with the column data type in the recordset.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_ILLEGAL_MODE,			_T ("You called CRecordset::Update without previously calling CRecordset::AddNew or CRecordset::Edit. ")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_LOCK_MODE_NOT_SUPPORTED,_T ("Your request to lock records for update could not be fulfilled because your ODBC driver does not support locking.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_MULTIPLE_ROWS_AFFECTED,	_T ("You called CRecordset::Update or Delete for a table with no unique key and changed multiple records.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_NO_CURRENT_RECORD,		_T ("You attempted to edit or delete a previously deleted record. You must scroll to a new current record after a deletion.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_NO_POSITIONED_UPDATES,	_T ("Your request for a dynaset could not be fulfilled because your ODBC driver does not support positioned updates.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_NO_ROWS_AFFECTED,		_T ("You called CRecordset::Update or Delete, but when the operation began the record could no longer be found.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_ODBC_LOAD_FAILED,		_T ("An attempt to load the ODBC.DLL failed; Windows could not find or could not load this DLL. This error is fatal.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_ODBC_V2_REQUIRED,		_T ("Your request for a dynaset could not be fulfilled because a Level 2-compliant ODBC driver is required.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_RECORDSET_FORWARD_ONLY,	_T ("An attempt to scroll did not succeed because the data source does not support backward scrolling.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_SNAPSHOT_NOT_SUPPORTED,	_T ("A call to CRecordset::Open requesting a snapshot failed. Snapshots are not supported by the driver. (This should only occur when the ODBC cursor library � ODBCCURS.DLL � is not present.)")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_SQL_CONFORMANCE,		_T ("The driver for a CDatabase::OpenEx or CDatabase::Open call does not conform to the required ODBC SQL Conformance level of \"Minimum\" (SQL_OSC_MINIMUM).")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_SQL_NO_TOTAL,			_T ("The ODBC driver was unable to specify the total size of a CLongBinary data value. The operation probably failed because a global memory block could not be preallocated.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_RECORDSET_READONLY,		_T ("You attempted to update a read-only recordset, or the data source is read-only. No update operations can be performed with the recordset or the CDatabase object it is associated with.")),
				EXECEPTIONSTRUCT (SQL_ERROR,							_T ("Function failed. The error message returned by ::SQLError is stored in the m_strError data member.")),
				EXECEPTIONSTRUCT (SQL_INVALID_HANDLE,					_T ("Function failed due to an invalid environment handle, connection handle, or statement handle. This indicates a programming error. No additional information is available from ::SQLError.")),
				EXECEPTIONSTRUCT (-1,									NULL),
			};

			for (int i = 0; e [i].m_nCode != -1; i++) {
				if (pDB->m_nRetCode == e [i].m_nCode) {
					strRetCode = e [i].m_str;
					break;
				}
			}

			str.Format (
				_T ("\n")
				_T ("m_nRetCode = %u (0x%p) [%s]\n")
				_T ("m_strError = %s\n")
				_T ("m_strStateNativeOrigin = %s"),
				pDB->m_nRetCode, pDB->m_nRetCode, strRetCode,
				pDB->m_strError,
				pDB->m_strStateNativeOrigin);
			strMsg += str;
		}

		str.Format (
			_T ("%s\n\n")
			_T ("exception class name: %s\n")
			_T ("%s(%u):"), 
			(LPCTSTR)strMsg, 
			a2w (pClass->m_lpszClassName), 
			pszFile, nLine);
		#else
		str = szMsg;
		#endif //_DEBUG
	}
	else 
		str = _T ("p == NULL");

	return str;
}

void HandleException (_com_error & e, LPCTSTR lpszFile, ULONG lLine)
{
	Trace ((LPCTSTR)e.Description (), lpszFile, lLine);
}

void HandleException (CException * p, LPCTSTR lpszFile, ULONG lLine)
{
	CString strClass = _T ("[Unknown]");
	CString str = FormatException (p, lpszFile, lLine);
	CString strMsg;

	if (p) {
		strClass = a2w (p->GetRuntimeClass ()->m_lpszClassName);
		p->Delete ();
	}

	Trace (str, lpszFile, lLine);
}