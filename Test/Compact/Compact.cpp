// Compact.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <afxdb.h>
#include <Odbcinst.h>
#include "Compact.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#import "C:\Program Files\Common Files\System\ado\msado28.tlb" no_namespace \
	rename( "EOF", "adoEOF" ) \
	rename( "LockTypeEnum", "adoLockTypeEnum" ) \
	rename( "DataTypeEnum", "adoDataTypeEnum") \
	rename( "FieldAttributeEnum", "FieldAttributeEnumado") \
	rename( "EditModeEnum", "adoEditModeEnum") \
	rename( "RecordStatusEnum", "adoRecordStatusEnum") \
	rename( "ParameterDirectionEnum", "adoParameterDirectionEnum") 

#import "C:\Program Files\Common Files\System\ado\msjro.dll" no_namespace \
	rename( "ReplicaTypeEnum", "adoReplicaTypeEnum" ) \

#import "C:\Program Files\Common Files\Microsoft Shared\DAO\dao360.dll" rename( "EOF", "daoEOF" )

/////////////////////////////////////////////////////////////////////////////
// The one and only application object

CWinApp theApp;

using namespace std;

bool Extract (UINT nID, const CString & strFilepath)
{
	HINSTANCE hModule = ::AfxGetInstanceHandle ();
	bool bResult = false;

	if (HRSRC hRSRC = ::FindResource (hModule, MAKEINTRESOURCE (nID), _T ("MSAccess"))) {
		DWORD dwSize = ::SizeofResource (hModule, hRSRC);

		if (HGLOBAL hGlobal = ::LoadResource (hModule, hRSRC)) {
			if (LPVOID lpData = ::LockResource (hGlobal)) {
				if (FILE * fp = _tfopen (strFilepath, _T ("wb"))) {
					if (fwrite (lpData, dwSize, 1, fp) == 1) {
						TRACEF (_T ("Extracted: ") + strFilepath);
						bResult = true;
					}
	
					fclose (fp);
				}
			}
		}
	}

	return bResult;
}

static const LPCTSTR lpszMDBDriver		= _T ("Microsoft Access Driver (*.mdb)\0");
static const LPCTSTR lpszACCDBDriver	= _T ("Microsoft Access Driver (*.mdb, *.accdb)\0");

bool DeleteDSN (CString strDSN, CString strFilename)
{
	return false;

	/*
	bool bACCDB = false;
	{
		CString str = strFilename;

		str.MakeLower ();

		bACCDB = str.Find (_T (".accdb")) != -1 ? true : false;
	}

	TRACEF (_T ("DeleteDSN: ") + strDSN);

	return ::SQLConfigDataSource (NULL, ODBC_REMOVE_SYS_DSN, bACCDB ? lpszACCDBDriver : lpszMDBDriver, _T ("DSN=") + strDSN) ? true : false;
	*/
}

bool CreateDSN (CString strDSN, CString strDir, CString strFilename)
{
	CString strFormat = 
		_T ("DSN=%s~ ")					// dsn
		_T ("DBQ=%s%s~ ")				// file
		_T ("Description=%s~ ")			// dsn
		_T ("FileType=MicrosoftAccess~ ")
		_T ("DataDirectory=%s~ ")
		_T ("MaxScanRows=20~ ")
		_T ("DefaultDir=%s~ ")
		_T ("FIL=MS Access;~ ")
		_T ("MaxBufferSize=2048~ ")
		_T ("MaxScanRows=8~ ")
		_T ("PageTimeout=5~ ")
		_T ("Threads=3~ ");
	TCHAR sz [512];
	TCHAR szAttr [512];
	bool bACCDB = false;

	if (strDir.GetLength () && strDir [strDir.GetLength () - 1] != '\\')
		strDir += '\\';

	{
		CString str = strFilename;

		str.MakeLower ();

		bACCDB = str.Find (_T (".accdb")) != -1 ? true : false;
	}

	_stprintf (sz, strFormat, 
		strDSN, 
		strDir, strFilename,
		strDSN,
		strDir, 
		strDir);

	if (bACCDB) {
		CString strFormat = 
			_T ("DSN=%s~ ")					// dsn
			_T ("DBQ=%s%s~ ")				// file
			_T ("Description=%s~ ");		// dsn
		_stprintf (sz, strFormat, 
			strDSN, 
			strDir, strFilename,
			strDSN);
	}

	int nLen = _tcsclen (sz);
	_tcscpy (szAttr, sz);
	TRACEF (szAttr);

	CString strResult = szAttr;
						
	for (int i = 0; i < nLen; i++)
		if (szAttr [i] == '~')
			szAttr [i] = '\0';

	DeleteDSN (strDSN, strFilename);

	if (::SQLConfigDataSource (NULL, ODBC_ADD_SYS_DSN, bACCDB ? lpszACCDBDriver : lpszMDBDriver, szAttr)) {
		TRACE_SQLERROR ();
		TRACEF (_T ("CreateDSN: ") + strDSN);
		TRACEF (_T ("driver: ") + CString (bACCDB ? lpszACCDBDriver : lpszMDBDriver));
		return true;
	}

	return false;
}

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	::DeleteFile (GetHomeDir () + _T ("\\Compact.txt"));
	::CoInitialize (0);

	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		cerr << _T("Fatal Error: MFC initialization failed") << endl;
		return 1;
	}

	TRACEF (CTime::GetCurrentTime ().Format (_T ("%c")));

	struct
	{
		UINT m_nID;
		LPCTSTR m_lpsz;
	}
	static const map [] =
	{
		{ IDR_MDB,		_T ("MarksmanELITE.mdb"),	},		
		{ IDR_ACCDB,	_T ("MarksmanELITE.accdb"),	},
	};

	for (int nFile = 0; nFile < ARRAYSIZE (map); nFile++) {
		CString strFile = GetHomeDir () + _T ("\\") + CString (map [nFile].m_lpsz);

		if (!Extract (map [nFile].m_nID, strFile))
			TRACEF (_T ("Extract failed: ") + strFile);
		else {
			CString strDSN;
			CDatabase db;

			strDSN.Format (_T ("%s_test"), map [nFile].m_lpsz);
			strDSN.Replace (_T ("."), _T ("_"));

			TRACEF ("");

			if (!CreateDSN (strDSN, GetHomeDir (), CString (map [nFile].m_lpsz)))
				TRACEF (_T ("Failed to create DSN: ") + strDSN + _T (", ") + strFile);
			/* else */ {
				try
				{
					CString strSQL = _T ("SELECT Tasks.Name FROM Tasks ORDER BY Tasks.Name;");

					TRACEF (_T ("Opening: ") + strDSN + _T (", ") + strFile + _T ("..."));

					db.OpenEx (_T ("DSN=") + strDSN, CDatabase::noOdbcDialog);
					CRecordset rst (&db);

					TRACEF (strSQL);

					if (rst.Open (CRecordset::snapshot, strSQL, CRecordset::readOnly)) {
						while (!rst.IsEOF ()) {
							CString str;
							
							rst.GetFieldValue ((short)0, str);
							rst.MoveNext ();
							TRACEF (str);
						}
					}

					db.Close ();
				}
				catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
				catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

				try
				{
					CString strSrc = strFile;
					CString strDest = GetHomeDir () + _T ("\\DAO.DBEngine.120_") + CString (map [nFile].m_lpsz);

					::DeleteFile (strDest);
					TRACEF (_T ("Compacting with DAO.DBEngine.120...")); 
					TRACEF (strSrc + _T (" --> ") + strDest); 
					DAO::_DBEnginePtr dbe (_T ("DAO.DBEngine.120"));
					dbe->CompactDatabase (_bstr_t (strSrc), _bstr_t (strDest));//, _variant_t (), _variant_t (_T ("128"), _variant_t ()));
					TRACEF (_T ("***** Success ***** "));
				}
				catch(_com_error &e) { HANDLEEXCEPTION (e); TRACEF (_T ("***** Compact failed ***** ")); }

				try
				{
					CString strSrc = strFile;
					CString strDest = GetHomeDir () + _T ("\\IJetEnginePtr_") + CString (map [nFile].m_lpsz);

					::DeleteFile (strDest);
					
					TRACEF (_T ("Compacting with IJetEnginePtr...")); 
					TRACEF (strSrc + _T (" --> ") + strDest); 

					CString strSourceConnection = _T ("Data Source=") + strSrc		+ _T (";");
					CString strDestConnection	= _T ("Data Source=") + strDest		+ _T (";");
					
					if (map [nFile].m_nID == IDR_ACCDB) {
						strSourceConnection = _T ("Provider=Microsoft.ACE.OLEDB.12.0;") + strSourceConnection + _T ("Jet OLEDB:Engine Type=5;");
						strDestConnection = _T ("Provider=Microsoft.ACE.OLEDB.12.0;") + strDestConnection + _T ("Jet OLEDB:Engine Type=5;");
					}

					TRACEF (strSourceConnection);
					TRACEF (strDestConnection);

					IJetEnginePtr jet(__uuidof(JetEngine));
					jet->CompactDatabase (_bstr_t (strSourceConnection), _bstr_t (strDestConnection));

					TRACEF (_T ("***** Success ***** "));
				}
				catch(_com_error &e) { HANDLEEXCEPTION (e); TRACEF (_T ("*****  Compact failed ***** ")); }

				DeleteDSN (strDSN, strFile);
			}

			TRACEF ("");
		}
	}

	cout << endl << endl << _T ("Results written to: ") << (LPCTSTR)GetHomeDir () <<  _T ("Compact.txt") << endl;

	{
		CString strCmdLine = _T ("notepad.exe  \"") + GetHomeDir () + _T ("\\Compact.txt") + _T ("\"");
		STARTUPINFO si;     
		PROCESS_INFORMATION pi; 
		TCHAR szCmdLine [0xFF];

		_tcsncpy (szCmdLine, strCmdLine, 0xFF);
		memset (&pi, 0, sizeof(pi));
		memset (&si, 0, sizeof(si));
		si.cb = sizeof(si);     
		si.dwFlags = STARTF_USESHOWWINDOW;     
		si.wShowWindow = SW_SHOW;     

		::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
	}

	::CoUninitialize ();
	return nRetCode;
}


