// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__6F0C4AF2_E666_488F_8799_E4E654D5E272__INCLUDED_)
#define AFX_STDAFX_H__6F0C4AF2_E666_488F_8799_E4E654D5E272__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afx.h>
#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <comdef.h>
#include <iostream>

CString GetHomeDir ();
void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine);
void HandleException (CException * p, LPCTSTR lpszFile, ULONG lLine);
void HandleException (_com_error & e, LPCTSTR lpszFile, ULONG lLine);

#define TRACEF(s) Trace ((s), _T (__FILE__), __LINE__)
#define HANDLEEXCEPTION(e) HandleException((e), _T (__FILE__), __LINE__)

#ifdef ARRAYSIZE
	#undef ARRAYSIZE
#endif

#define ARRAYSIZE(s) (sizeof ((s)) / sizeof ((s) [0]))

// TODO: reference additional headers your program requires here

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__6F0C4AF2_E666_488F_8799_E4E654D5E272__INCLUDED_)
