
// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions


#include <afxdisp.h>        // MFC Automation classes



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC support for ribbons and control bars









#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


#include <iostream>
#include <string>
#include <vector>
#include <map>

std::wstring a2w(const std::string & str);
std::string w2a(const std::wstring & str);
CString a2w(const CString & str);
CString w2a(const CString & str);

void Trace(LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine);
void Trace(const std::string & str, LPCTSTR lpszFile, ULONG lLine);
void Trace(const std::wstring & str, LPCTSTR lpszFile, ULONG lLine);
#define TRACEF(s) ::Trace ((s), _T (__FILE__), __LINE__)

CString execute (const CString & str);
CString GetHomeDir ();


typedef struct
{
	FILETIME	m_ftCreate;
	FILETIME	m_ftExit;
	FILETIME	m_ftKernel;
	FILETIME	m_ftUser;
	CTime		m_tmLast;
} THREAD_TIMES_STRUCT;

std::vector <DWORD> Find (const CString & strExe);
std::map <DWORD, THREAD_TIMES_STRUCT> GetThreadTimes (DWORD dwProcessID); 


inline int Find (const std::vector <std::wstring> & v, const std::wstring & str, bool bCaseSensitive = true)
{
	if (bCaseSensitive) {
		for (int i = 0; i < v.size (); i++) 
			if (!_tcscmp (str.c_str (), v [i].c_str ()))
				return i;
	}
	else {
		for (int i = 0; i < v.size (); i++) 
			if (!_tcsicmp (str.c_str (), v [i].c_str ()))
				return i;
	}

	return -1;
}

inline int Find (const std::wstring & s, TCHAR c)
{
	for (int i = 0; i < s.length (); i++)
		if (s [i] == c)
			return i;

	return -1;
}

template <class T>
inline int Find (const std::vector <T> & v, const T & p)
{
	for (int i = 0; i < v.size (); i++)
		if (v [i] == p)
			return i;

	return -1;
}

template <class T>
inline T implode (const std::vector<T> & v, const T & delim)
{
	T result;

	for (UINT i = 0; i < v.size (); i++) {
		result += v [i];

		if (i < (v.size () - 1))
			result += delim;
	}

	return result;
}

inline CString implode (const CStringArray & v, const CString & strDelim = _T (","))
{
	std::vector <std::wstring> a;

	for (int i = 0; i < v.GetSize (); i++)
		a.push_back ((std::wstring)(LPCTSTR)v [i]);

	return implode <std::wstring> (a, (std::wstring)(LPCTSTR)strDelim).c_str ();
}

template <class T, class T2>
T2 GetChar(const T & str, int nIndex)
{
	if (nIndex >= 0 && nIndex < (int)str.length())
		return str[nIndex];

	return (T2)0;
}

template <class T, class T2>
std::vector <int> Count(const T & str, T2 c)
{
	std::vector <int> v;

	for (int i = 0; i < (int)str.size(); i++)
		if (str[i] == c)
			v.push_back(i);

	return v;
}

template <class T, class T2>
int Count(const std::vector <T> & v, const T2 c)
{
	int nResult = 0;

	for (int i = 0; i < (int)v.size(); i++) {
		if (GetChar(v[i], i) == c)
			nResult++;
	}

	return nResult;
}

template <class T, class T2>
std::vector<T> explode(const T & str, T2 delim)
{
	std::vector<T> result;
	std::vector <int> v = Count(str, delim);
	size_t nIndex = 0;

	for (size_t i = 0; i < v.size(); i++) {
		int nLen = v[i] - nIndex;
		result.push_back(str.substr(nIndex, nLen));
		nIndex = v[i] + 1;
	}

	if (nIndex < str.length()) {
		result.push_back(str.substr(nIndex, str.length() - nIndex));
	}
	else {
		if (str.length() && (T2)str[str.length() - 1] == delim)
			result.push_back(T());
	}

	return result;
}

inline CString FormatMessage (DWORD dwError)
{
	LPVOID lpMsgBuf = NULL;
	CString str;

	str.Format (_T ("[%d: 0x%p] "), dwError, dwError);

	::FormatMessage (
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, dwError, MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT), 
		(LPTSTR)&lpMsgBuf, 0, NULL);
	str += (LPCTSTR)lpMsgBuf;
	::LocalFree( lpMsgBuf );

	return str;
}
