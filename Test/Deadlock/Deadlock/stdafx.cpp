
// stdafx.cpp : source file that includes just the standard includes
// Deadlock.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include <tlhelp32.h>
#include <psapi.h>


using namespace std;

void Trace(LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	CString str;
	CString strFile = lpszFile;
	int nIndex = strFile.ReverseFind('\\');

	//if (nIndex != -1)
	//	strFile = strFile.Mid(nIndex + 1);

	str.Format(_T("%s(%d): %s\n"), strFile, lLine, lpsz);
	std::wstring s = str;

	while (s.length()) {
		std::wstring strSeg = s.substr(0, 512);
		s.erase(0, 512);
		std::wcout << strSeg.c_str();
		::OutputDebugString(strSeg.c_str());
		::Sleep(1);
	}
}

void Trace(const std::string & str, LPCTSTR lpszFile, ULONG lLine)
{
	Trace(CString(a2w(str).c_str()), lpszFile, lLine);
}

void Trace(const std::wstring & str, LPCTSTR lpszFile, ULONG lLine)
{
	Trace(str.c_str(), lpszFile, lLine);
}

std::wstring a2w(const std::string & str)
{
	std::wstring str2(str.length(), L' ');

	std::copy(str.begin(), str.end(), str2.begin());

	return str2;
}

std::string trim(const std::string & str)
{
	CString s = a2w(str).c_str();
	s.Trim();
	return w2a(wstring((LPCTSTR)s));
}

std::wstring trim(const std::wstring & str)
{
	CString s = str.c_str();
	s.Trim();
	return wstring((LPCTSTR)s);
}

std::string w2a(const std::wstring & str)
{
	std::string str2(str.length(), L' ');

	std::copy(str.begin(), str.end(), str2.begin());

	return str2;
}

CString a2w(const CString & str)
{
	std::string a;// = std::string((LPCSTR)(LPCTSTR)str);

	for (int i = 0; i < str.GetLength(); i++)
		a += (char)str[i];

	std::wstring w = a2w(a);
	return (CString)w.c_str();
}

CString w2a(const CString & str)
{
	std::wstring w = std::wstring((LPCTSTR)str);
	std::string a = w2a(w);
	return (CString)a.c_str();
}

std::vector <DWORD> Find (const CString & strExe)
{
	std::vector <DWORD> vResult;
	DWORD dwResult = 0, dwProcesses[1024] = { 0 }, dwNeeded = 0;

    if (::EnumProcesses (dwProcesses, sizeof(dwProcesses), &dwNeeded)) {
	    int n = dwNeeded / sizeof(DWORD);

		for (int i = 0; dwResult == 0 && i < n; i++) {
			if (HANDLE hProcess = ::OpenProcess (PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, dwProcesses [i])) {
				HMODULE hModule = NULL;

				if (::EnumProcessModules (hProcess, &hModule, sizeof(hModule), &dwNeeded)) {
					TCHAR sz [MAX_PATH] = { 0 };

					::GetModuleBaseName (hProcess, hModule, sz, sizeof(sz) / sizeof(TCHAR));
					CString str = sz;

					if (!str.CompareNoCase (strExe))
						vResult.push_back (dwProcesses [i]);
				}

				::CloseHandle (hProcess);
			}
		}
	}

	return vResult;
}

std::map <DWORD, THREAD_TIMES_STRUCT> GetThreadTimes (DWORD dwProcessID) 
{
	std::map <DWORD, THREAD_TIMES_STRUCT> result;

	HANDLE hSnapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	
	if (hSnapshot != INVALID_HANDLE_VALUE) {
		THREADENTRY32 te;
		te.dwSize = sizeof(te);

		if (::Thread32First(hSnapshot, &te)) {
			do {
				if (te.dwSize >= FIELD_OFFSET(THREADENTRY32, th32OwnerProcessID) + sizeof(te.th32OwnerProcessID)) {
					if (te.th32OwnerProcessID == dwProcessID) {
						if (HANDLE hThread = ::OpenThread (THREAD_QUERY_INFORMATION, FALSE, te.th32ThreadID)) {
							THREAD_TIMES_STRUCT ft = { 0 };

							if (::GetThreadTimes (hThread, &ft.m_ftCreate, &ft.m_ftExit, &ft.m_ftKernel, &ft.m_ftUser)) {
								//CString str;
								//const ULONGLONG & lCreate = * reinterpret_cast<const ULONGLONG*>(&ftCreate);
								//const ULONGLONG & lExit = * reinterpret_cast<const ULONGLONG*>(&ftExit);
								//const ULONGLONG & lKernel = * reinterpret_cast<const ULONGLONG*>(&ft.m_ftKernel);
								//const ULONGLONG & lUser = * reinterpret_cast<const ULONGLONG*>(&ft.m_ftUser);

								ft.m_tmLast = CTime::GetCurrentTime ();
								result [te.th32ThreadID] = ft;
								//result [te.th32ThreadID] = (__int64)(lKernel + lUser) / 10000.0;

							}

							::CloseHandle (hThread);
						}
					}
				}

				te.dwSize = sizeof(te);
			} 
			while (::Thread32Next(hSnapshot, &te));
		}

		::CloseHandle(hSnapshot);
	}

	return result;
}

CString GetHomeDir ()
{
	TCHAR sz [MAX_PATH] = { 0 };

	HMODULE hModule = ::GetModuleHandle (NULL);
	::GetModuleFileName (hModule, sz, ARRAYSIZE (sz) - 1);

	const CString strPath (sz);
	CString strPathLower (sz);
	strPathLower.MakeLower ();

	VERIFY (::GetFileTitle (strPath, sz, ARRAYSIZE (sz) - 1) == 0);
	
	CString strTitle (sz);
	strTitle.MakeLower ();

	int nIndex = strPathLower.Find (strTitle);

	ASSERT (nIndex != -1);
	CString str = strPath.Left (nIndex - 1);

	return str;
}


CString execute (const CString & str)
{
	CString strResult;
	HANDLE hOutputReadTmp,hOutputRead,hOutputWrite;
	HANDLE hInputWriteTmp,hInputRead,hInputWrite;
	HANDLE hErrorWrite;
	SECURITY_ATTRIBUTES sa;
	HANDLE hStdIn = NULL; // Handle to parents std input.

	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;

	if (!::CreatePipe (&hOutputReadTmp, &hOutputWrite, &sa, 0))
		TRACEF (FormatMessage (::GetLastError ()));

	// Create a duplicate of the output write handle for the std error
	// write handle. This is necessary in case the child application
	// closes one of its std output handles.
	if (!::DuplicateHandle (::GetCurrentProcess (), hOutputWrite, GetCurrentProcess(), &hErrorWrite, 0, TRUE, DUPLICATE_SAME_ACCESS))
		TRACEF (FormatMessage (::GetLastError ()));


	// Create the child input pipe.
	if (!::CreatePipe (&hInputRead, &hInputWriteTmp, &sa, 0))
		TRACEF (FormatMessage (::GetLastError ()));


	// Create new output read handle and the input write handles. Set
	// the Properties to FALSE. Otherwise, the child inherits the
	// properties and, as a result, non-closeable handles to the pipes
	// are created.
	if (!::DuplicateHandle (::GetCurrentProcess (), hOutputReadTmp, ::GetCurrentProcess (), &hOutputRead, 0, FALSE, DUPLICATE_SAME_ACCESS))
		TRACEF (FormatMessage (::GetLastError ()));

	if (!::DuplicateHandle (::GetCurrentProcess (), hInputWriteTmp, ::GetCurrentProcess (), &hInputWrite, 0, FALSE, DUPLICATE_SAME_ACCESS))
		TRACEF (FormatMessage (::GetLastError ()));


	// Close inheritable copies of the handles you do not want to be inherited.
	if (!::CloseHandle (hOutputReadTmp)) TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hInputWriteTmp)) TRACEF (FormatMessage (::GetLastError ()));


	// Get std input handle so you can close it and force the ReadFile to
	// fail when you want the input thread to exit.
	if ((hStdIn = GetStdHandle(STD_INPUT_HANDLE)) == INVALID_HANDLE_VALUE)
		TRACEF (FormatMessage (::GetLastError ()));

	{
		PROCESS_INFORMATION pi;
		STARTUPINFO si;
		TCHAR * psz = new TCHAR [str.GetLength () + 1];

		
		ZeroMemory (psz, str.GetLength () + 1);
		ZeroMemory (&si, sizeof (STARTUPINFO));

		si.cb			= sizeof (STARTUPINFO);
		si.dwFlags		= STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
		si.wShowWindow	= SW_HIDE;
		si.hStdOutput	= hOutputWrite;
		si.hStdInput	= hInputRead;
		si.hStdError	= hErrorWrite;

		_tcscpy (psz, str);
		TRACEF (psz);

		if (!::CreateProcess (NULL, psz, NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi))
			TRACEF (FormatMessage (::GetLastError ()));

		if (!::CloseHandle (pi.hThread)) 
			TRACEF (FormatMessage (::GetLastError ()));

		delete [] psz;
	}


	if (!::CloseHandle (hOutputWrite))	TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hInputRead ))	TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hErrorWrite))	TRACEF (FormatMessage (::GetLastError ()));

	while (1) {
		const int nSize = 256;
		CHAR lpBuffer [nSize + 1] = { 0 };
		DWORD nBytesRead = 0;
		DWORD nCharsWritten = 0;

		if (!::ReadFile (hOutputRead, lpBuffer, nSize, &nBytesRead, NULL) || !nBytesRead) {
			if (::GetLastError() == ERROR_BROKEN_PIPE)
				break; // pipe done - normal exit path.
			else
				TRACEF (FormatMessage (::GetLastError ()));
		}

		strResult += /* a2w */ (lpBuffer);
		//::OutputDebugString (a2w (lpBuffer)); ::Sleep (1);
	}

	if (!::CloseHandle (hOutputRead))	TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hInputWrite))	TRACEF (FormatMessage (::GetLastError ()));

	return strResult;
}

