//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Deadlock.rc
//
#define IDD_DEADLOCK_DIALOG             102
#define IDR_MAINFRAME                   128
#define LV_THREADS                      1000
#define BTN_FIND_CONTROL                1001
#define BTN_TRIGGER_DBG                 1002
#define IDC_LIST2                       1003
#define LB_CALLSTACK                    1003
#define BTN_TRIGGER_DBG2                1004
#define BTN_DUMP                        1004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
