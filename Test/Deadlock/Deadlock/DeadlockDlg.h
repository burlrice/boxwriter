
// DeadlockDlg.h : header file
//

#pragma once


// CDeadlockDlg dialog
class CDeadlockDlg : public CDialogEx
{
// Construction
public:
	CDeadlockDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_DEADLOCK_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;
	DWORD m_dwProcessID;
	std::map <DWORD, DWORD>					m_mapRegisteredThreads;
	std::map <DWORD, THREAD_TIMES_STRUCT>	m_mapLastThreadTimes;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	afx_msg LRESULT OnDebugRegisterThread (WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedFindControl();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedTriggerDbg();
	afx_msg void OnDblclkLvThreads(NMHDR *pNMHDR, LRESULT *pResult);
//	afx_msg void OnLbnSelchangeCallstack();
	afx_msg void OnDblclkLbCallstack();
	afx_msg void OnBnClickedDump();
};
