
// DeadlockDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Deadlock.h"
#include "DeadlockDlg.h"
#include "afxdialogex.h"
#include "WinMsg.h"
#include "Resource.h"
#include "StackWalker.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CDeadlockDlg dialog

#define IDT_THREAD	1000
#define IDT_FIND	1001


CDeadlockDlg::CDeadlockDlg(CWnd* pParent /*=NULL*/)
:	m_dwProcessID (0),
	CDialogEx(CDeadlockDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDeadlockDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CDeadlockDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_REGISTERED_MESSAGE (WM_DEBUG_REGISTER_THREAD, OnDebugRegisterThread)
	ON_BN_CLICKED(BTN_FIND_CONTROL, &CDeadlockDlg::OnBnClickedFindControl)
	ON_WM_TIMER()
	ON_BN_CLICKED(BTN_TRIGGER_DBG, &CDeadlockDlg::OnBnClickedTriggerDbg)
	ON_NOTIFY(NM_DBLCLK, LV_THREADS, &CDeadlockDlg::OnDblclkLvThreads)
	ON_LBN_DBLCLK(LB_CALLSTACK, &CDeadlockDlg::OnDblclkLbCallstack)
	ON_BN_CLICKED(BTN_DUMP, &CDeadlockDlg::OnBnClickedDump)
END_MESSAGE_MAP()


// CDeadlockDlg message handlers

BOOL CDeadlockDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	if (CListCtrl * p = (CListCtrl *)GetDlgItem (LV_THREADS)) {
		int i = 0;

		p->InsertColumn (i++, _T ("Thread ID"),		0, 60);
		p->InsertColumn (i++, _T ("Create"),		0, 120);
		p->InsertColumn (i++, _T ("Exit"),			0, 50);
		p->InsertColumn (i++, _T ("Kernel"),		0, 50);
		p->InsertColumn (i++, _T ("User"),			0, 50);
		p->InsertColumn (i++, _T ("Last"),			0, 120);

		p->SetExtendedStyle (p->GetExtendedStyle () | LVS_EX_FULLROWSELECT);
	}

	OnBnClickedFindControl ();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDeadlockDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDeadlockDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

LRESULT CDeadlockDlg::OnDebugRegisterThread (WPARAM wParam, LPARAM lParam)
{
	CString str;

	str.Format (_T ("OnDebugRegisterThread: %d, %d"), wParam, lParam);
	TRACEF (str);
	m_mapRegisteredThreads [(DWORD)lParam] = (DWORD)wParam;

	return 0;
}

void CDeadlockDlg::OnBnClickedFindControl()
{
	DWORD dwResult = 0;

	std::vector <DWORD> vProcessIDs = Find (_T ("Control.exe"));

	if (vProcessIDs.size ()) {
		if (vProcessIDs.size () > 1)
			MessageBox (_T ("Multiple processes found"));

		m_dwProcessID = vProcessIDs [0];
		KillTimer (IDT_FIND);
	}
	else
		SetTimer (IDT_FIND, 10 * 1000, NULL);

	m_mapRegisteredThreads.clear ();
	::SendMessageTimeout (HWND_BROADCAST, WM_DEBUG_ENUM_REGISTERED_THREADS, 0, 0, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);

	if (CListCtrl * p = (CListCtrl *)GetDlgItem (LV_THREADS)) 
		p->DeleteAllItems ();

	if (CListBox * p = (CListBox *)GetDlgItem (LB_CALLSTACK)) 
		p->ResetContent ();

	if (!m_dwProcessID) 
		KillTimer (IDT_THREAD);
	else 
		SetTimer (IDT_THREAD, 1000, NULL);
}


void CDeadlockDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent) {
	case IDT_FIND:
		{
			OnBnClickedFindControl ();
		}
		break;
	case IDT_THREAD:
		{
			if (m_dwProcessID) {
				if (CListCtrl * p = (CListCtrl *)GetDlgItem (LV_THREADS)) {
					std::map <DWORD, THREAD_TIMES_STRUCT> map = GetThreadTimes (m_dwProcessID);

					p->SetRedraw (FALSE);

					for (std::map <DWORD, THREAD_TIMES_STRUCT>::iterator i = map.begin (); i != map.end (); i++) {
						bool bFound = false;
						int nIndex = 0, nColumn = 1;

						for (int j = 0; !bFound && j < p->GetItemCount (); j++) {
							DWORD dw = _tstol (p->GetItemText (j, 0));

							if (dw == i->first) {
								nIndex = j;
								bFound = true;
							}
						}

						if (!bFound) 
							nIndex = p->InsertItem (p->GetItemCount (), std::to_wstring ((__int64)i->first).c_str ());
				
						const ULONGLONG & lExit = * reinterpret_cast<const ULONGLONG*>	(&i->second.m_ftExit);
						const ULONGLONG & lKernel = * reinterpret_cast<const ULONGLONG*>(&i->second.m_ftKernel);
						const ULONGLONG & lUser = * reinterpret_cast<const ULONGLONG*>	(&i->second.m_ftUser);

						const ULONGLONG & lLastExit = * reinterpret_cast<const ULONGLONG*>	(&m_mapLastThreadTimes [i->first].m_ftExit);
						const ULONGLONG & lLastKernel = * reinterpret_cast<const ULONGLONG*>(&m_mapLastThreadTimes [i->first].m_ftKernel);
						const ULONGLONG & lLastUser = * reinterpret_cast<const ULONGLONG*>	(&m_mapLastThreadTimes [i->first].m_ftUser);

						if (lExit != lLastExit || lKernel != lLastKernel) 
							m_mapLastThreadTimes [i->first].m_tmLast = CTime::GetCurrentTime ();

						CTimeSpan tm = CTime::GetCurrentTime () - m_mapLastThreadTimes [i->first].m_tmLast;
						bool bRegistered = (m_mapRegisteredThreads [i->first] == m_dwProcessID) ? true : false;
						CString str = bRegistered ? tm.Format (_T ("%Dd %H:%M:%S")) : _T ("");


						if (bRegistered) {
							if (tm.GetTotalMinutes () >= 5) {
								::SetWindowPos (m_hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
								ShowWindow (SW_RESTORE);
							}
						}

						p->SetItemText (nIndex, nColumn++, CTime (i->second.m_ftCreate). Format (_T ("%c")));
						p->SetItemText (nIndex, nColumn++, std::to_wstring ((__int64)(lExit				/ 10000.0)).c_str ());
						p->SetItemText (nIndex, nColumn++, std::to_wstring ((__int64)(lKernel			/ 10000.0)).c_str ());
						p->SetItemText (nIndex, nColumn++, std::to_wstring ((__int64)(lUser				/ 10000.0)).c_str ());
						p->SetItemText (nIndex, nColumn++, str);

						m_mapLastThreadTimes [i->first].m_ftExit	= i->second.m_ftExit;	
						m_mapLastThreadTimes [i->first].m_ftKernel	= i->second.m_ftKernel;	
						m_mapLastThreadTimes [i->first].m_ftUser	= i->second.m_ftUser;	
					}

					p->SetRedraw (TRUE);
				}
			}
		}
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}


void CDeadlockDlg::OnBnClickedTriggerDbg()
{
	if (HANDLE hTrace = ::OpenEvent (EVENT_ALL_ACCESS, FALSE, _T ("Foxjet::CDiagnosticDataThread::Trace"))) {
		::SetEvent (hTrace);
		::CloseHandle (hTrace);
	}
}


void CDeadlockDlg::OnDblclkLvThreads(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	*pResult = 0;

	if (CListCtrl * p = (CListCtrl *)GetDlgItem (LV_THREADS)) {
		CString str = p->GetItemText (pNMItemActivate->iItem, 0);
		DWORD dwThreadID = (DWORD)_tcstoul (str, NULL, 10);

		if (HANDLE hProcess = ::OpenProcess (PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, m_dwProcessID)) {
			StackWalker s (StackWalker::OptionsAll, NULL, m_dwProcessID, hProcess);

			if (HANDLE hThread = ::OpenThread (THREAD_ALL_ACCESS, FALSE, dwThreadID)) {
				if (dwThreadID == GetCurrentThreadId ())
					hThread = GetCurrentThread ();

				BeginWaitCursor ();
				s.ShowCallstack (_T (__FILE__), __LINE__, hThread);
				EndWaitCursor ();

				if (CListBox * pLB = (CListBox *)GetDlgItem (LB_CALLSTACK)) {
					CDC * pDC = pLB->GetDC ();
					std::vector <std::wstring> v = explode ((std::wstring)s.GetOutput (), (TCHAR)'\n');
					int cx = 0;

					pLB->ResetContent ();

					for (int i = 0; i < v.size (); i++) {
						CString str = v [i].c_str ();
						cx = max (cx, pDC->GetTextExtent (str).cx);
						pLB->AddString (str);
					}

					pLB->SetHorizontalExtent (cx);
				}

				::CloseHandle (hThread);
			}
			else 
				MessageBox (_T ("Cannot open thread"));

			::CloseHandle (hProcess);
		}
		else
			MessageBox (_T ("Cannot open process"));
	}
}



void CDeadlockDlg::OnDblclkLbCallstack()
{
	if (CListBox * pLB = (CListBox *)GetDlgItem (LB_CALLSTACK)) {
		CString str;

		pLB->GetText (pLB->GetCurSel (), str);
		MessageBox (str);
	}
}


void CDeadlockDlg::OnBnClickedDump()
{
	CString str = _T ("\"") + GetHomeDir () + _T ("\\procdump\" -ma Control.exe");
	TRACEF (str);
	str = execute (str);
	TRACEF (str);
	MessageBox (str);
}
