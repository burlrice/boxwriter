// stdafx.cpp : source file that includes just the standard includes
// socket.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"


using namespace std;

void Trace(LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	CString str;
	CString strFile = lpszFile;
	int nIndex = strFile.ReverseFind('\\');

	//if (nIndex != -1)
	//	strFile = strFile.Mid(nIndex + 1);

	str.Format(_T("%s(%d): %s\n"), strFile, lLine, lpsz);
	std::wstring s = str;

	while (s.length()) {
		std::wstring strSeg = s.substr(0, 512);
		s.erase(0, 512);
		std::wcout << strSeg.c_str();
		::OutputDebugString(strSeg.c_str());
		::Sleep(1);
	}
}

void Trace(const std::string & str, LPCTSTR lpszFile, ULONG lLine)
{
	Trace(CString(a2w(str).c_str()), lpszFile, lLine);
}

void Trace(const std::wstring & str, LPCTSTR lpszFile, ULONG lLine)
{
	Trace(str.c_str(), lpszFile, lLine);
}

std::wstring a2w(const std::string & str)
{
	std::wstring str2(str.length(), L' ');

	std::copy(str.begin(), str.end(), str2.begin());

	return str2;
}

std::string trim(const std::string & str)
{
	CString s = a2w(str).c_str();
	s.Trim();
	return w2a(wstring((LPCTSTR)s));
}

std::wstring trim(const std::wstring & str)
{
	CString s = str.c_str();
	s.Trim();
	return wstring((LPCTSTR)s);
}

std::string w2a(const std::wstring & str)
{
	std::string str2(str.length(), L' ');

	std::copy(str.begin(), str.end(), str2.begin());

	return str2;
}

CString a2w(const CString & str)
{
	std::string a;// = std::string((LPCSTR)(LPCTSTR)str);

	for (int i = 0; i < str.GetLength(); i++)
		a += (char)str[i];

	std::wstring w = a2w(a);
	return (CString)w.c_str();
}

CString w2a(const CString & str)
{
	std::wstring w = std::wstring((LPCTSTR)str);
	std::string a = w2a(w);
	return (CString)a.c_str();
}

DWORD GetFileSize(const CString & strFile)
{
	DWORD dw = 0;

	if (FILE * fp = _tfopen(strFile, _T("rb"))) {
		fseek(fp, 0, SEEK_END);
		dw = ftell(fp);
		fclose(fp);
	}

	return dw;
}

bool IsFileUTF16(const CString & strFile)
{
	if (FILE * fp = _tfopen(strFile, _T("rb"))) {
		const BYTE nUTF16[] = { 0xFF, 0xFE };
		const BYTE n[] = { 0x00, 0x00 };

		fread((void *)n, ARRAYSIZE(n), 1, fp);
		fclose(fp);

		return !memcmp(n, nUTF16, ARRAYSIZE(nUTF16));
	}

	return false;
}

CString ReadDirect(const CString & strFile, const CString & strDefault)
{
	DWORD dwSize = GetFileSize(strFile);
	int nLen = dwSize + sizeof(wchar_t);
	CString strData = strDefault;

	if (FILE * fp = _tfopen(strFile, _T("rb"))) {
		const BYTE nUTF16[] = { 0xFF, 0xFE };
		BYTE * p = new BYTE[nLen];

		memset(p, 0, nLen);
		fread(p, dwSize, 1, fp);
		fclose(fp);

		if (!memcmp(p, nUTF16, ARRAYSIZE(nUTF16))) {
			TCHAR * pData = (TCHAR *)p;
			strData = ++pData;
		}
		else
			strData = (const char *)p;

		delete[] p;
	}

	return strData;
}

std::string urldecode(const std::string & str)
{
	CString strResult(str.c_str());

	for (int i = 0; i < 256; i++) {
		CString strTmp;

		strTmp.Format(_T("%%%02X"), i);

		strResult.Replace(strTmp, CString((TCHAR)i));
	}

	for (int i = 0; i < 256; i++) {
		CString strTmp;

		strTmp.Format(_T("%%%X"), i);

		strResult.Replace(strTmp, CString((TCHAR)i));
	}

	return w2a((wstring)(LPCTSTR)strResult);
}

std::vector <unsigned char> GetIpAddress(const std::string & str)
{
	const std::vector <std::string> vDef = GetIpAddresses();
	string strDef = "127.0.0.1";

	for (int i = 0; i < vDef.size(); i++) {
		vector <string> v = explode(vDef[i], '.');

		if (v[0] == "192") {
			strDef = vDef[i];
			break;
		}
	}

	string s = !str.length() ? strDef : str;
	vector <string> v = explode(s, '.');
	vector <unsigned char > result;

	for (int i = 0; i < v.size(); i++)
		result.push_back((unsigned char)std::stoi(v[i].c_str()));

	return result;
}

std::vector <std::string> GetIpAddresses()
{
	std::vector <std::string> v;
	WSADATA wsaData;
	char szName[255] = { 0 };
	PHOSTENT hostinfo;
	WORD wVersionRequested = MAKEWORD(1, 1);

	if (::WSAStartup(wVersionRequested, &wsaData) == 0) {
		if (::gethostname(szName, sizeof(szName)) == 0) {
			//TRACEF (szName);

			if ((hostinfo = ::gethostbyname(szName)) != NULL) {
				for (int i = 0; hostinfo->h_addr_list[i] != NULL; i++)
					v.push_back((string)inet_ntoa(*(struct in_addr *)hostinfo->h_addr_list[i]));
			}
		}
	}

	return v;
}

CString GetFileExt(const CString & strFile)
{
	int nIndex = strFile.ReverseFind('.');

	if (nIndex != -1)
		return strFile.Mid(nIndex + 1);

	return _T("");
}

int GetFiles(const CString & strDir, const CString & strFile, CStringArray & v)
{
	if (strFile.Find(_T("*")) == -1 && strFile != _T("*.*")) {
		CStringArray vTmp;

		GetFiles(strDir, _T("*.*"), vTmp);

		for (int i = 0; i < vTmp.GetSize(); i++) {
			CString str = vTmp[i];

			if (str.Replace(_T("\\") + strFile, _T("")))
				v.Add(vTmp[i]);
		}

		return v.GetSize();
	}

	int nFiles = 0;
	WIN32_FIND_DATA fd;
	const CString str = strDir + _T("\\") + strFile;
	HANDLE hFind = ::FindFirstFile(str, &fd);
	bool bMore = hFind != INVALID_HANDLE_VALUE;

	//TRACEF (str);

	while (bMore) {
		CString str = strDir;

		if (_tcscmp(fd.cFileName, _T(".")) != 0 && _tcscmp(fd.cFileName, _T(".."))) {
			if (str[str.GetLength() - 1] != '\\')
				str += '\\';

			str += fd.cFileName;

			v.Add(str);
			nFiles++;

			if (::GetFileAttributes(str) & FILE_ATTRIBUTE_DIRECTORY)
				nFiles += GetFiles(strDir + _T("\\") + fd.cFileName, strFile, v);
		}

		ZeroMemory(&fd, sizeof(fd));
		bMore = ::FindNextFile(hFind, &fd) ? true : false;
	}

	::FindClose(hFind);

	return nFiles;
}

std::string to_string(const char * pData, int nLen)
{
	std::string s;

	for (int i = 0; i < nLen; i++) {
		if ((pData[i] >= ' ' && pData[i] <= '~') || pData[i] == '\r' || pData[i] == '\n')
			s += pData[i];
		else
			s += '.';
	}

	return s;
}

std::string to_hex_string(const unsigned char * pData, int nLen)
{
	std::string s;

	for (int i = 0; i < nLen; i++) {
		char sz[8] = { 0 };

		sprintf(sz, "%02X", (unsigned char)pData[i]);
		s += sz;

		if (!((i + 1) % 4))
			s += ' ';
	}

	return s;
}
