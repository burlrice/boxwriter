// socket.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "socket.h"
#include <afxsock.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// The one and only application object

CWinApp theApp;

using namespace std;

typedef struct
{
	std::string m_strAddr;
	int m_nLine;
} THREAD_STRUCT;

LRESULT CALLBACK ThreadFunc (LPVOID lpData)
{
	THREAD_STRUCT s = *(THREAD_STRUCT *)lpData;
	std::string strLine = "LINE000" + std::to_string(s.m_nLine);
	/*
	const char * lpszTask [] =
	{
	("LSP VEGE"),
	("WEIS PEANUT OIL"),
	//("FOOD CLUB VEG"),
	//("FOOD CLUB PEANUT14449"),
	//("FOOD CLUB CANOLA"),
	//("FOOD CLUB CORN"),
	//("ROYAL SUPREME VEG"),
	("URBAN MEADOW CORN 14153"),
	};
	*/
	const char * lpszTask[] =
	{
		("LSP VEGE"),
		("Custom Julian Date Code [1]"),
		("WEIS PEANUT OIL"),
		("Custom Julian Date Code"),
		("URBAN MEADOW CORN 14153"),
		("Custom Julian Date Code (hour)"),
	};

	const CString strAddr = a2w (s.m_strAddr).c_str ();

	WSADATA wsaData;

	VERIFY(::WSAStartup(MAKEWORD(1, 1), &wsaData) == 0);
	srand(time(NULL));
	TRACEF(strAddr);

	int nTask = rand ();

	while (1) {
		CTime tm = CTime::GetCurrentTime();
		int nMin = (rand() % 8) + 1;
		CTimeSpan span = (tm + CTimeSpan(0, 0, nMin, rand() % 60)) - tm;
		int nSleep = span.GetTotalSeconds() * 1000;
		std::string str = "{Start task," + strLine + "," + std::string(lpszTask[nTask++ % ARRAYSIZE(lpszTask)]) + "}";

		{
			CSocket s;

			if (!s.Create(0, SOCK_STREAM))
				TRACEF(_T("CSocket::Create error "));

			if (s.Connect(strAddr, 2202)) {
				int n = s.Send(str.c_str(), str.length());

				if (n != str.length())
					TRACEF(_T("CSocket::Send error: ") + (CString)to_wstring(n).c_str() + _T(" [") + CString(to_wstring(str.length()).c_str()));
			}
			else
				TRACEF(_T("Failed to connect to: ") + strAddr);
		}

		TRACEF(CTime::GetCurrentTime().Format(_T("[%H:%M:%S] ")) + a2w(str).c_str() + span.Format(_T(", sleeping: %H:%M:%S...")));
		::Sleep(nSleep);
	}

	return 0;
}

int main(int argc, char ** argv)
{
    int nRetCode = 0;

    HMODULE hModule = ::GetModuleHandle(nullptr);
	
    if (hModule != nullptr)
    {
        // initialize MFC and print and error on failure
        if (!AfxWinInit(hModule, nullptr, ::GetCommandLine(), 0))
        {
            // TODO: change error code to suit your needs
            wprintf(L"Fatal Error: MFC initialization failed\n");
            nRetCode = 1;
        }
        else
        {
            // TODO: code your application's behavior here.
        }
    }
    else
    {
        // TODO: change error code to suit your needs
        wprintf(L"Fatal Error: GetModuleHandle failed\n");
        nRetCode = 1;
    }

	DWORD dw = 0;
	THREAD_STRUCT s;
	HANDLE h[1] = { NULL };

	s.m_strAddr = argc >= 2 ? argv[1] : "127.0.0.1"; //"192.168.1.62";

	s.m_nLine = 1;
	h [0] = ::CreateThread((LPSECURITY_ATTRIBUTES)NULL, 0, (LPTHREAD_START_ROUTINE)ThreadFunc, (LPVOID)&s, 0, &dw);
	::Sleep (1000);

	//s.m_nLine = 2;
	//h[1] = ::CreateThread((LPSECURITY_ATTRIBUTES)NULL, 0, (LPTHREAD_START_ROUTINE)ThreadFunc, (LPVOID)&s, 0, &dw);

	::WaitForMultipleObjects (ARRAYSIZE (h), h, TRUE, INFINITE);

    return nRetCode;
}
