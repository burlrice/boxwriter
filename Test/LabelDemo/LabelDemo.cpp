// LabelDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "LabelDemo.h"
#include <afxsock.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// The one and only application object

CWinApp theApp;

using namespace std;

bool SendCmd (CSocket & socket, CStringArray & vCmd, CStringArray & vResponse)
{
	CString strCmd = ToString (vCmd);
	int nLen = strCmd.GetLength ();

	vResponse.RemoveAll ();

	ASSERT (strCmd.Find (BW_HOST_PACKET_CLOSESOCKET) == -1);

	bool bSend = socket.Send ((LPVOID)(LPCSTR)w2a (strCmd), nLen) != SOCKET_ERROR;

	if (bSend) {
		bool bMore = true;
		CString strBuffer;
		int nTries = 0;

		do {
			char szBuffer [1024];
			ZeroMemory (szBuffer, sizeof (szBuffer));

			int nRevc = socket.Receive (szBuffer, sizeof (szBuffer));

			if (nRevc == SOCKET_ERROR) {
				TRACEF (_T ("Receive failed: \n") + FormatMessage (::GetLastError ()));
				return false;
			}
			
			strBuffer += a2w (szBuffer);

			int nLeft = CountChars (strBuffer, '{').GetSize ();
			int nRight = CountChars (strBuffer, '}').GetSize ();

			if (nLeft && nRight == nLeft)
				bMore = false;
			else {
				if (nTries++ > 20) {
					TRACEF (_T ("No TCP/IP response from printer"));
					ASSERT (0);
					return false;
				}
			}
		} 
		while (bMore);

		FromString (strBuffer, vResponse);

		#ifdef _DEBUG
		ASSERT (vCmd.GetSize ());
		ASSERT (vResponse.GetSize ());
		CString strCmd = vCmd [0];
		CString strResponse = vResponse [0];
		ASSERT (!strCmd.CompareNoCase (strResponse)); // cmd/response mismatched
		#endif //_DEBUG

		return true;
	}

	return false;
}

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		cerr << _T("Fatal Error: MFC initialization failed") << endl;
		return 0;
	}

	CSocket socket;
	CString strAddress = _T ("127.0.0.1");
	ULONG lHeadID = -1;

	if (!socket.Create (0, SOCK_STREAM)) {
		TRACEF (_T ("Create failed: ") + strAddress + _T ("\n") + FormatMessage (::GetLastError ()));
		return S_OK;
	}

	if (!socket.Connect (strAddress, BW_HOST_TCP_PORT)) {
		TRACEF (_T ("Connect failed: ") + strAddress + _T ("\n") + FormatMessage (::GetLastError ()));
		return S_OK;
	}

	{
		CStringArray vCmd, vResponse;
	
		vCmd.Add (BW_HOST_PACKET_LOAD_TASK);
		vCmd.Add (_T ("")); // line
		vCmd.Add (_T ("[Label]"));

		SendCmd (socket, vCmd, vResponse);
		TRACEF (ToString (vResponse));
	}

	{
		CStringArray vCmd, vResponse;
	
		vCmd.Add (BW_HOST_PACKET_GET_HEADS);
		vCmd.Add (_T ("")); // line

		SendCmd (socket, vCmd, vResponse);
		TRACEF (ToString (vResponse));

		if (vResponse.GetSize () > 2)
			lHeadID = _ttoi (vResponse [2]);
	}

	{
		CStringArray vCmd, vResponse;
	
		vCmd.Add (BW_HOST_PACKET_ADD_ELEMENT);
		vCmd.Add (_T ("")); // line
		vCmd.Add (ToString ((int)lHeadID)); 
		vCmd.Add (_T ("")); // element

		SendCmd (socket, vCmd, vResponse);
		TRACEF (ToString (vResponse));
	}
	
	{
		CStringArray vCmd, vResponse;
	
		vCmd.Add (BW_HOST_PACKET_START_TASK);
		vCmd.Add (_T ("")); // line
		vCmd.Add (_T ("[Label]"));

		SendCmd (socket, vCmd, vResponse);
		TRACEF (ToString (vResponse));
	}

	socket.Close ();

	return 0;
}


