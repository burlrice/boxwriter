// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__8433A194_1805_4EDB_ACE8_5A5B6FA9BB6D__INCLUDED_)
#define AFX_STDAFX_H__8433A194_1805_4EDB_ACE8_5A5B6FA9BB6D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afx.h>
#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <iostream>
#include <afxtempl.h>

#include "AnsiString.h"

template<class TYPE, class ARG_TYPE>
class CCopyArray : public CArray <TYPE, ARG_TYPE>
{
public:
	CCopyArray ();
	CCopyArray (const CCopyArray & rhs);
	CCopyArray & operator = (const CCopyArray & rhs);
	virtual ~CCopyArray ();
};
typedef CCopyArray <ULONG, ULONG> CLongArray;

CString ToString (int n);
CString ToString (HRESULT hResult);
CString ToString (const CStringArray & v);
int FromString (const CString & strInput, CStringArray & v);
CString Tokenize(const CString &strInput, CStringArray &v, TCHAR cDelim = ',', bool bTrim = true);
CLongArray CountChars (const CString & str, TCHAR c);
CString FormatString (const CString &str, bool bFilename = false);
CString UnformatString(const CString &strSrc);
void Trace (const char * lpsz, LPCTSTR lpszFile, ULONG lLine);
void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine);
void HandleException (CException * p, LPCTSTR lpszFile, ULONG lLine);

#define TRACEF(s) Trace ((s), _T (__FILE__), __LINE__)
#define HANDLEEXCEPTION(e) HandleException((e), _T (__FILE__), __LINE__)

#ifdef ARRAYSIZE
	#undef ARRAYSIZE
#endif

#define ARRAYSIZE(s) (sizeof ((s)) / sizeof ((s) [0]))

inline CString FormatMessage (DWORD dwError)
{
	LPVOID lpMsgBuf = NULL;
	CString str;

	str.Format (_T ("[%d: 0x%p] "), dwError, dwError);

	::FormatMessage (
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, dwError, MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT), 
		(LPTSTR)&lpMsgBuf, 0, NULL);
	str += (LPCTSTR)lpMsgBuf;
	::LocalFree( lpMsgBuf );

	return str;
}

template<class TYPE, class ARG_TYPE>
inline CCopyArray<TYPE, ARG_TYPE>::CCopyArray ()
{
}

template<class TYPE, class ARG_TYPE>
inline CCopyArray<TYPE, ARG_TYPE>::CCopyArray (const CCopyArray<TYPE, ARG_TYPE> & rhs) 
{
	RemoveAll ();

	for (int i = 0; i < rhs.GetSize (); i++)
		Add (rhs [i]);
}

template<class TYPE, class ARG_TYPE>
inline CCopyArray<TYPE, ARG_TYPE> & CCopyArray<TYPE, ARG_TYPE>::operator = (const CCopyArray<TYPE, ARG_TYPE> & rhs)
{
	if (this != &rhs) {
		RemoveAll ();
		Copy (rhs);
	}

	return * this;
}

template<class TYPE, class ARG_TYPE>
inline CCopyArray<TYPE, ARG_TYPE>::~CCopyArray ()
{
}

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__8433A194_1805_4EDB_ACE8_5A5B6FA9BB6D__INCLUDED_)
