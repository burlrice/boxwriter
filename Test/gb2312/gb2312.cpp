﻿// gb2312.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "gb2312.h"
#include <Windows.h>
#include <memory>
#include <vector>
#include <locale>
#include <codecvt>
#include <fstream>
#include <iostream>
#include <io.h>
#include <fcntl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


void TraceLength(const CString & str, const char * lpszFile, ULONG lLine) { Trace(_T("[") + CString(std::to_wstring(str.GetLength()).c_str()) + _T("] ") + str, lpszFile, lLine); }
void TraceLength(const std::wstring & str, const char * lpszFile, ULONG lLine) { Trace(_T("[") + std::to_wstring(str.length()) + _T("] ") + str, lpszFile, lLine); }
void TraceLength(const std::string & str, const char * lpszFile, ULONG lLine) { Trace("[" + std::to_string(str.length()) + "] " + str, lpszFile, lLine); }

#define TRACEL(s) TraceLength ((s),__FILE__, __LINE__)

// The one and only application object

CWinApp theApp;

using namespace std;



std::wstring ConvertFromUtf8ToUtf16(const std::string& str)
{
	std::wstring convertedString;
	int requiredSize = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, 0, 0);
	if (requiredSize > 0)
	{
		std::vector<wchar_t> buffer(requiredSize);
		MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, &buffer[0], requiredSize);
		convertedString.assign(buffer.begin(), buffer.end() - 1);
	}

	return convertedString;
}

std::string ConvertFromUtf16ToUtf8(const std::wstring& wstr)
{
	std::string convertedString;
	int requiredSize = WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), -1, 0, 0, 0, 0);
	if (requiredSize > 0)
	{
		std::vector<char> buffer(requiredSize);
		WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), -1, &buffer[0], requiredSize, 0, 0);		
		convertedString.assign(buffer.begin(), buffer.end() - 1);
	}

	return convertedString;
}

std::string windows1252(const std::wstring& wstr)
{
	std::string convertedString;
	int requiredSize = WideCharToMultiByte(1252, 0, wstr.c_str(), -1, 0, 0, 0, 0);
	if (requiredSize > 0)
	{
		std::vector<char> buffer(requiredSize);
		WideCharToMultiByte(1252, 0, wstr.c_str(), -1, &buffer[0], requiredSize, 0, 0);
		convertedString.assign(buffer.begin(), buffer.end() - 1);
	}

	return convertedString;
}

std::wstring gb2312(const std::string& str)
{
	std::wstring convertedString;
	int requiredSize = MultiByteToWideChar(936, 0, str.c_str(), -1, 0, 0);
	if (requiredSize > 0)
	{
		std::vector<wchar_t> buffer(requiredSize);
		MultiByteToWideChar(936, 0, str.c_str(), -1, &buffer[0], requiredSize);
		convertedString.assign(buffer.begin(), buffer.end() - 1);
	}

	return convertedString;
}



int main()
{
    int nRetCode = 0;

    HMODULE hModule = ::GetModuleHandle(nullptr);

    if (hModule != nullptr)
    {
        // initialize MFC and print and error on failure
        if (!AfxWinInit(hModule, nullptr, ::GetCommandLine(), 0))
        {
            // TODO: change error code to suit your needs
            wprintf(L"Fatal Error: MFC initialization failed\n");
            nRetCode = 1;
        }
        else
        {
            // TODO: code your application's behavior here.
        }
    }
    else
    {
        // TODO: change error code to suit your needs
        wprintf(L"Fatal Error: GetModuleHandle failed\n");
        nRetCode = 1;
    }

	//const std::string strInput		= "¼¦";// "201512040012600000010962,¼¦²¡¶¾ÐÔ¹Ø½ÚÑ×Ãð»îÒßÃç£¨S1133Öê+1733Öê£©,£¨2011£©ÍâÊÞÒ©Ö¤×Ö35ºÅ,µÂ¹úÂÞÂü,+86 10 8455 1497";
	//const std::wstring wstrControl	= _T ("鸡"); _T("201512040012600000010962，鸡病毒性关节炎灭活疫苗（S1133株+1733株），（2011）外兽药证字35号，德国罗曼，+86 10 8455 1497");
	const std::string strInput		= "201512040012600000010962,¼¦²¡¶¾ÐÔ¹Ø½ÚÑ×Ãð»îÒßÃç£¨S1133Öê+1733Öê£©,£¨2011£©ÍâÊÞÒ©Ö¤×Ö35ºÅ,µÂ¹úÂÞÂü,+86 10 8455 1497";
	const std::wstring wstrInput	= _T ("201512040012600000010962,¼¦²¡¶¾ÐÔ¹Ø½ÚÑ×Ãð»îÒßÃç£¨S1133Öê+1733Öê£©,£¨2011£©ÍâÊÞÒ©Ö¤×Ö35ºÅ,µÂ¹úÂÞÂü,+86 10 8455 1497");
	const std::wstring wstrControl	= _T ("201512040012600000010962,鸡病毒性关节炎灭活疫苗（S1133株+1733株）,（2011）外兽药证字35号,德国罗曼,+86 10 8455 1497"); //_T("201512040012600000010962，鸡病毒性关节炎灭活疫苗（S1133株+1733株），（2011）外兽药证字35号，德国罗曼，+86 10 8455 1497");
	const std::wstring wstrUTF8		= _T ("��");

	/*

	https://2cyr.com/decode/?lang=en
		Expert: source encoding: GB2312 
		displayed as: Windows-1252

		TODO
			¼¦ --> [x] --> 鸡
					��
			convert from Windows-1252 to UTF8
			convert from UTF8 to GB2312

			201512040012600000010962，鸡病毒性关节炎灭活疫苗（S1133株+1733株），（2011）外兽药证字35号，德国罗曼，+86 10 8455 1497
			201512040012600000010962，鸡病毒性关节炎灭活疫苗（S1133株+1733株），（2011）外兽药证字35号，德国罗曼，+86 10 8455 1497

	*/



	TRACEL (wstrControl);
	TRACEL (strInput);
	TRACEL (wstrInput);
	TRACEL (wstrUTF8);

	const std::string s = strInput; //windows1252(wstrInput);
	const std::wstring ws = gb2312(s);
	CString str = ws.c_str ();
	str.Trim ();
	TRACEF (str);
	TRACEL (ws);

	{
		FILE *fStream;
		errno_t e = _tfopen_s(&fStream, _T("c:\\Foxjet\\kanji.txt"), _T("wt,ccs=UNICODE")); // or ccs=UTF-8
		CStdioFile f(fStream);  // open the file from this stream

		f.WriteString(wstrControl.c_str ());
		f.WriteString (_T ("\n"));
		f.WriteString(str);
		f.Close();
	}

	/*
	201512040012600000010962，鸡病毒性关节炎灭活疫苗（S1133株+1733株），（2011）外兽药证字35号，德国罗曼，+86 10 8455 1497
	201512040012600000010962,鸡病毒性关节炎灭活疫苗（S1133株+1733株）,（2011）外兽药证字35号,德国罗曼,+86 10 8455 1497

	0c ff
	2c 00
	*/

    return nRetCode;
}
