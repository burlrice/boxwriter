// stdafx.cpp : source file that includes just the standard includes
// gb2312.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include <sstream>

using namespace std;

void Trace(const char * lpsz, const char * lpszFile, ULONG lLine)
{
	string str;
	string strFile = lpszFile;
	ostringstream ss;

	ss << strFile << "(" << lLine << "): " << string(lpsz);
	ss << "\n";

	str = ss.str();
	string s = str;

	while (s.length()) {
		string strSeg = s.substr(0, 512);
		s.erase(0, 512);

		std::cout << strSeg.c_str();
		::OutputDebugStringA(strSeg.c_str());
		::Sleep(1);
	}
}

std::wstring a2w(const std::string & str)
{
	std::wstring str2(str.length(), L' ');

	std::copy(str.begin(), str.end(), str2.begin());

	return str2;
}

std::string w2a(const std::wstring & str)
{
	std::string str2(str.length(), L' ');

	std::copy(str.begin(), str.end(), str2.begin());

	return str2;
}
