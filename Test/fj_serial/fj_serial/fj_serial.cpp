// fj_serial.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <Windows.h>
#include <io.h>
#include <conio.h>
#include <iostream>
#include <assert.h>

#define FJ_DEFAULT_NAME_SIZE       32	// default name size for the following items
#define FJTEXT_SIZE                256	// maximum number of characters in a text element
#define FJLABEL_NAME_SIZE          FJ_DEFAULT_NAME_SIZE
#define FJMESSAGE_NUMBER_SEGMENTS  50	// maximum number of segments/elements in one message
#define FJMESSAGE_DYNAMIC_SEGMENTS  FJMESSAGE_NUMBER_SEGMENTS

using namespace std;

void Trace (const char * psz, char * pszFile, unsigned long lLine)
{
	char sz [1024] = { 0 };

	sprintf (sz, "%s(%d): %s\n", pszFile, lLine, psz);
	cout << sz;
	::OutputDebugStringA (sz);
}
#define TRACEF(s) Trace ((s), (__FILE__), __LINE__)

#define ARRAYSIZE(s) (sizeof (s) / sizeof (s [0]))

typedef struct
{
	char m_cTo;
	char m_szFrom [8];
} FJ_HTML_SPECIAL_CHARS;

FJ_HTML_SPECIAL_CHARS fj_mapHtmlSpecialChars [127 + 5];

int fj_strReplace (char * psz, int nLen, const char * pszFind, const char cReplace)
{
	int nResult = 0;
	char * p;

	if (strlen (psz) && strlen (pszFind)) {
		while (p = strstr (psz, pszFind)) {
			int nIndexFrom = (p - psz) + 1;
			int nLenFrom = strlen (pszFind);
			int nIndexTo = nIndexFrom + nLenFrom;
			int nLenTo = strlen (psz) - nIndexFrom - nLenFrom + 2;

			psz [nIndexFrom - 1] = cReplace;
			memcpy (&psz [nIndexFrom], &psz [nIndexTo - 1], nLenTo);
			//TRACEF (psz);
			nResult++;
		}
	}

	return nResult;
}

void fj_unformatHtml (char * psz, int nLen)
{
	int i;

	//TRACEF (psz);

	for (i = 0; i < ARRAYSIZE (fj_mapHtmlSpecialChars); i++) {
		fj_strReplace (psz, nLen, fj_mapHtmlSpecialChars [i].m_szFrom, fj_mapHtmlSpecialChars [i].m_cTo);

		if (i < 127) {
			char sz [16];

			sprintf (sz, "&#%02d;", i);
			fj_strReplace (psz, nLen, sz, (char)i);

			sprintf (sz, "&#%d;", i);
			fj_strReplace (psz, nLen, sz, (char)i);
		}
	}
}

int fj_isHtmlSpecialChar (const char * psz)
{
	int i;

	for (i = 0; i < ARRAYSIZE (fj_mapHtmlSpecialChars); i++) {
		int nLen = strlen (fj_mapHtmlSpecialChars [i].m_szFrom);

		if (nLen)
			if (!strncmp (psz, fj_mapHtmlSpecialChars [i].m_szFrom, nLen))
				return nLen;

		if (i < 127) {
			char sz [16];

			sprintf (sz, "&#%02d;", i);
			nLen = strlen (sz);

			if (!strncmp (sz, psz, nLen))
				return nLen;

			sprintf (sz, "&#%d;", i);
			nLen = strlen (sz);

			if (!strncmp (sz, psz, nLen))
				return nLen;
		}
	}

	return 0;
}

bool fj_strExtract (const char * psz, int nLen, const char * pszBegin, const char * pszEnd, char * pszResult, int nResultLen)
{
	const char * pBegin = strstr (psz, pszBegin);

	memset (pszResult, 0, nResultLen);

	if (pBegin) {
		const char * pEnd = pBegin + strlen (pszBegin);
		int n;

		pEnd = strstr (pEnd, pszEnd);

		while (pEnd && ((n = fj_isHtmlSpecialChar (pEnd)) != 0)) {
			pEnd += n;
			pEnd = strstr (pEnd, pszEnd);
			//TRACEF (pEnd);
		}

		if (!pEnd)
			pEnd = &psz [strlen (psz)];

		int nSubStrLen = pEnd - pBegin - strlen (pszBegin);
		strncpy (pszResult, pBegin + strlen (pszBegin), nSubStrLen < nResultLen ? nSubStrLen : nResultLen);
		fj_unformatHtml (pszResult, nSubStrLen);
		return true;
	}

	return false;
}

int _tmain(int argc, _TCHAR* argv[])
{
	char sz [512];
	int i;

	{
		const char * psz = 
			//"serial.cgi?idx=0"
			//"&nme=TEST.prd&fst=1&lst=1"
			//"&d1=&lt;&#039;&#84;&#069;&#83;&#084;&#032;&amp;&#032;&#84;&#069;&#83;&#084;&#039;&gt;"
			//"&d3=&lt;&#039;&#84;&#069;&#83;&#084;+3&#039;&gt;"
			//"&d2=&lt;&#039;&#84;this+is+a+test+&#069;&#83;&#084;+3&#039;&gt;"
			//"&d5=&lt;&#039;&#84;&#069;&#83;&#084;+5+5+5+5&#039;&gt;"
			//"&status=ResetCount"

			"print.cgi?idx=0&prd=TEST.prd"
			;

		int n = strlen (psz);
		assert (strlen (psz) < ARRAYSIZE (sz));
		strcpy (sz, psz);
	}

	{
		int nIndex;

		memset (fj_mapHtmlSpecialChars, 0, sizeof (FJ_HTML_SPECIAL_CHARS) * ARRAYSIZE (fj_mapHtmlSpecialChars));

		nIndex = 127;
		fj_mapHtmlSpecialChars [nIndex].m_cTo = '&';
		strcpy (fj_mapHtmlSpecialChars [nIndex].m_szFrom, "&amp;");

		nIndex++;
		fj_mapHtmlSpecialChars [nIndex].m_cTo = '\'';
		strcpy (fj_mapHtmlSpecialChars [nIndex].m_szFrom, "&quot;");

		nIndex++;
		fj_mapHtmlSpecialChars [nIndex].m_cTo = '<';
		strcpy (fj_mapHtmlSpecialChars [nIndex].m_szFrom, "&lt;");

		nIndex++;
		fj_mapHtmlSpecialChars [nIndex].m_cTo = '>';
		strcpy (fj_mapHtmlSpecialChars [nIndex].m_szFrom, "&gt;");

		nIndex++;
		fj_mapHtmlSpecialChars [nIndex].m_cTo = ' ';
		strcpy (fj_mapHtmlSpecialChars [nIndex].m_szFrom, "+");

		for (i = 0; i < 127; i++) {
			fj_mapHtmlSpecialChars [i].m_cTo = (char)i;
			sprintf (fj_mapHtmlSpecialChars [i].m_szFrom, "&#%03d;", i);
		}
	}

	if (strstr (sz, ".cgi?") != NULL) {
		char szLabelName [FJLABEL_NAME_SIZE + 1];
		char szStatus [FJLABEL_NAME_SIZE + 1];
		//char strDynData [FJMESSAGE_DYNAMIC_SEGMENTS][FJTEXT_SIZE+1] = { 0 };

		TRACEF (sz);
		//fj_unformatHtml (sz, ARRAYSIZE (sz)); TRACEF (sz);

		fj_strExtract (sz, ARRAYSIZE (sz), "&nme=", "&", szLabelName, ARRAYSIZE (szLabelName));

		if (!strlen (szLabelName))
			fj_strExtract (sz, ARRAYSIZE (sz), "&prd=", "&", szLabelName, ARRAYSIZE (szLabelName));

		fj_strExtract (sz, ARRAYSIZE (sz), "&status=", "\r\n", szStatus, ARRAYSIZE (szStatus));

		TRACEF (szLabelName);
		TRACEF (szStatus);

		if (strlen (szLabelName)) {
			char strLabel [FJLABEL_NAME_SIZE + 1] 	= { 0 };

			/* TODO
			fj_scratchpadReadLabelName (strLabel, FJLABEL_NAME_SIZE);

			if (strcmp (strLabel, szLabelName) != 0) {
				LPFJLABEL pfl = fj_SystemBuildSelectedLabel (pfsys, sz);
	
				if (pfl == NULL) {
					if (pfsys->pflScanner != NULL) 
						pfl = fj_SystemCopyScannerDefaultLabelToSelectedLabel (pfsys);
				}
				else // save selected label name
					fj_scratchpadWriteLabelName (sz);

				gbSerialStart = TRUE;
			}
			*/
		}

		if (!strcmp (szStatus, "Pause")) {
			// TODO // fj_SysSetPrintMode (fj_CVT.pfsys, "IDLE");
		}
		else if (!strcmp (szStatus, "Start")) {
			// TODO // fj_SysSetPrintMode (fj_CVT.pfsys, "RESUME");
		}
		else if (!strcmp (szStatus, "Cancel")) {
			// TODO // fj_SysSetPrintMode (fj_CVT.pfsys, "STOP");
		}
		else if (!strcmp (szStatus, "ResetCount")) {
			// TODO // fj_SetCounts (pfph->pfmSelected, 0);
		}

		for (i = 0; i < FJMESSAGE_DYNAMIC_SEGMENTS; i++) {
			char szIndex [16];
			LONG lIndex = i + 1;

			sprintf (szIndex, "&d%d=", i + 1);
	
			bool bChanged = false; // TODO // fj_strExtract (sz, ARRAYSIZE (sz), szIndex, "&", pGlobal->DynamicSegments [lIndex], FJTEXT_SIZE);

			if (bChanged) {
				// TRACEF (strDynData [i]);
				// TODO // pGlobal->DynamicSegments[lIndex].bChanged = TRUE;
			}
		}
	}

	TRACEF ("hit any key to exit..."); getch ();
	return 0;
}

