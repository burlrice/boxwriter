// CFS.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "CFS.h"
#include "OdbcDatabase.h"
#include "OdbcRecordset.h"
#include "Parse.h"
#include "TemplExt.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifdef TRACEF
#undef TRACEF
#endif

#define TRACEF(s) ::Trace ((s), _T (__FILE__), __LINE__)

// The one and only application object

CWinApp theApp;

using namespace std;

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	HMODULE hModule = ::GetModuleHandle(NULL);

	if (hModule != NULL)
	{
		// initialize MFC and print and error on failure
		if (!AfxWinInit(hModule, NULL, ::GetCommandLine(), 0))
		{
			// TODO: change error code to suit your needs
			_tprintf(_T("Fatal Error: MFC initialization failed\n"));
			return -1;
		}
	}
	else
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: GetModuleHandle failed\n"));
		return -1;
	}

	using namespace FoxjetDatabase;

	CString strDSN = _T ("PRDN_PRINT_ACCESS_DB_FOXJET_sql");
	CString strTable = _T ("PRODUCTION_LABEL_DATA");
	CString strUID, strPWD;
	CString strColumns;
	const int nInterval = 15;
	CTime tmLast = CTime::GetCurrentTime () - CTimeSpan (0, 0, nInterval + 1, 0);
	std::vector <CString> v;

	for (int i = 0; i < argc; i++) {
		CString str;
		
		str = Extract (argv [i], _T ("/DSN="));
		if (str.GetLength ()) 
			strDSN = str;

		str = Extract (argv [i], _T ("/UID="));
		if (str.GetLength ()) 
			strUID = str;

		str = Extract (argv [i], _T ("/PWD="));
		if (str.GetLength ()) 
			strPWD = str;
	}

	CString strConnect = _T ("DSN=") + strDSN + _T (";"); 
		
	if (strUID.GetLength () && strPWD.GetLength ())
		strConnect += _T ("UID=") + strUID + _T (";PWD=") + strPWD + _T (";");

	TRACEF (strConnect);

	TRACEF (_T ("Opening: ") + strDSN + _T (" ..."));

	try
	{
		COdbcDatabase db (_T (__FILE__), __LINE__);

		//db.Open (strDSN, 0, 0, _T ("ODBC;"), 0);
		VERIFY (db.OpenEx (strConnect, CDatabase::noOdbcDialog));

		if (db.IsSqlServer ()) {
			//strTable = _T ("[") + strDSN + _T ("].[dbo].[") + strTable + _T ("]");
			TRACEF (strTable);
		}

		COdbcRecordset rst (db);
		CString strSQL = _T ("SELECT * FROM ") + strTable;
		std::vector <CString> vCols;

		TRACEF (strSQL);
		rst.Open (strSQL);

		for (short i = 0; i < rst.GetODBCFieldCount (); i++) {
			CODBCFieldInfo info;

			rst.GetODBCFieldInfo (i, info);
			vCols.push_back (Format (info.m_strName));
		}

		strColumns = implode <CString> (vCols, _T (", "));
		TRACEF (strColumns);

		while (!rst.IsEOF ()) {
			std::vector <CString> vRow;

			for (short i = 0; i < rst.GetODBCFieldCount (); i++) {
				CODBCFieldInfo info;
				CString str, s = (CString)rst.GetFieldValue (i);
			
				rst.GetODBCFieldInfo (i, info);
				vCols.push_back (info.m_strName);
				
				if (IsSqlString (info.m_nSQLType)) {
					if (!s.GetLength ()) 
						str = _T ("Null");
					else
						str = _T ("'") + Format (s) + _T ("'");
				}
				else
					str = Format (s);


				vRow.push_back (str);
			}

			CString strSQL = _T ("INSERT INTO ") + strTable + _T (" (") + strColumns + _T (") VALUES (") + implode <CString> (vRow, _T (", ")) + _T ("); ");

			for (int i = 0; i < v.size (); i++) {
				ASSERT (v [i].CompareNoCase (strSQL) != 0);
			}

			v.push_back (strSQL);

			std::wcout << _T (".");
			rst.MoveNext ();
		}

		std::wcout << _T ("\n");
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); return -1; }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); return -1; }

	while (1) {
		CTimeSpan diff = CTime::GetCurrentTime () - tmLast;

		if (diff.GetTotalMinutes () >= nInterval) {
			CString strSQL = _T ("DELETE * FROM ") + strTable;

			TRACEF (CTime::GetCurrentTime ().Format (_T ("%c")));
			tmLast = CTime::GetCurrentTime ();

			try {
				COdbcDatabase db (_T (__FILE__), __LINE__);
				int nUpdated = 0;

				//db.Open (strDSN, 0, 0, _T ("ODBC;"), 0);
				VERIFY (db.OpenEx (strConnect, CDatabase::noOdbcDialog));

				if (db.IsSqlServer ())
					strSQL = _T ("DELETE FROM ") + strTable;

				TRACEF (strSQL);
				db.ExecuteSQL (strSQL);

				BEGIN_TRANS (db);

				for (_ULonglong i = 0; i < v.size (); i++) {
					strSQL = v [i];
					//TRACEF (FoxjetDatabase::ToString (i) + _T (": ") + (strSQL.GetLength () > 60 ? (strSQL.Left (60) + _T (" ...")) : strSQL));
					std::wcout << _T ("-");

					try {
						db.ExecuteSQL (strSQL);
						nUpdated++;
					}
					catch (CDBException * e)		{ 
						TRACEF (strSQL); 
						HANDLEEXCEPTION_TRACEONLY (e); 
					}
					catch (CMemoryException * e) { 
						TRACEF (strSQL); 
						HANDLEEXCEPTION_TRACEONLY (e); 
					}

					if (!((i + 1) % 100)) {
						std::wcout << _T ("Commit") << endl; 
						COMMIT_TRANS (db);
						::Sleep (1);
						BEGIN_TRANS (db);
					}
				}

				std::wcout << endl; 
				TRACEF ("Commit");
				COMMIT_TRANS (db);	
				TRACEF (_T ("Updated: ") + FoxjetDatabase::ToString (nUpdated));
			}
			catch (CDBException * e)		{ TRACEF (strSQL); HANDLEEXCEPTION (e); }
			catch (CMemoryException * e)	{ TRACEF (strSQL); HANDLEEXCEPTION (e); }
		}

		::Sleep (1000);
	}

	return nRetCode;
}
