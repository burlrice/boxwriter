// stdafx.cpp : source file that includes just the standard includes
// CFS.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include <vector>

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file

void Trace(const std::wstring & str, const wchar_t * lpszFile, unsigned long lLine)
{
	std::wstring w;

	w = std::wstring(lpszFile) + _T("(") + std::to_wstring((_ULonglong)lLine) + _T("): ") + str + std::wstring(_T("\n"));

	::OutputDebugStringW(w.c_str());
	std::wcout << w.c_str();
}

void Trace(const std::string & str, const wchar_t * lpszFile, unsigned long lLine)
{
	Trace(str.c_str(), lpszFile, lLine);
}

void Trace(const wchar_t * lpsz, const wchar_t * lpszFile, unsigned long lLine)
{
	return Trace(std::wstring(lpsz), lpszFile, lLine);
}

void Trace(const char * lpsz, const wchar_t * lpszFile, unsigned long lLine)
{
	std::wstring s;

	for (int i = 0; lpsz[i]; i++)
		s += (wchar_t)lpsz[i];

	Trace(s, lpszFile, lLine);
}
