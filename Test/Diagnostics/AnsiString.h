// AnsiString.h: interface for the CAnsiString class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __ANSISTRING_H__
#define __ANSISTRING_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CAnsiString  
{
public:
	CAnsiString ();
	CAnsiString (const char * psz);
	CAnsiString (const CString & rhs);
	CAnsiString (const CAnsiString & rhs);
	CAnsiString & operator = (const CAnsiString & rhs);
	virtual ~CAnsiString ();

	operator LPCSTR() const;
	operator LPSTR();
	operator CString() const;
	const UCHAR & operator [] (int nIndex) const;

	int GetLength () const;
	CString GetCString () const;

protected:
	UCHAR * Alloc (int nLen);
	void Free ();

	UCHAR * m_psz;
};

#ifdef _UNICODE
	#define w2a(s) CAnsiString ((s))
	#define a2w(s) (CString)CAnsiString ((s))
#else
	#define w2a(s) (s)
	#define a2w(s) (s)
#endif

#endif //__ANSISTRING_H__
