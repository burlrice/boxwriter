/******************************************************************************\
* ping.c - Simple ping utility using SOCK_RAW
* 
*       This is a part of the Microsoft Source Code Samples.
*       Copyright 1996-1997 Microsoft Corporation.
*       All rights reserved.
*       This source code is only intended as a supplement to
*       Microsoft Development Tools and/or WinHelp documentation.
*       See these sources for detailed information regarding the
*       Microsoft samples programs.
\******************************************************************************/

#include "stdafx.h"

#pragma pack(4)

#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>

#define ICMP_ECHO 8
#define ICMP_ECHOREPLY 0

#define ICMP_MIN 8 // minimum 8 byte icmp packet (just header)

/* The IP header */
typedef struct iphdr {
	unsigned int h_len:4;          // length of the header
	unsigned int version:4;        // Version of IP
	unsigned char tos;             // Type of service
	unsigned short total_len;      // total length of the packet
	unsigned short ident;          // unique identifier
	unsigned short frag_and_flags; // flags
	unsigned char  ttl; 
	unsigned char proto;           // protocol (TCP, UDP etc)
	unsigned short checksum;       // IP checksum

	unsigned int sourceIP;
	unsigned int destIP;

}IpHeader;

//
// ICMP header
//
typedef struct _ihdr {
  BYTE i_type;
  BYTE i_code; /* type sub code */
  USHORT i_cksum;
  USHORT i_id;
  USHORT i_seq;
  /* This is not the std header, but we reserve space for time */
  ULONG timestamp;
}IcmpHeader;

#define STATUS_FAILED 0xFFFF
#define DEF_PACKET_SIZE 32
#define MAX_PACKET 1024

static HWND hWndDebug = NULL;

void fill_icmp_data(char *, int);
USHORT checksum(USHORT *, int);
int decode_resp(char *,int ,struct sockaddr_in *);


int Ping (LPCTSTR lpszAddr, int nTries, HWND hWndDebug)
{
	struct sockaddr_in dest,from;
	int nDatasize = DEF_PACKET_SIZE;
	int fromlen = sizeof(from);
	char *dest_ip;
	char icmp_data [MAX_PACKET];
	char recvbuf [MAX_PACKET];
	unsigned int addr=0;
	USHORT seq_no = 0;
	int nResult = 0;
	int nTimeout = 1000;

	if (hWndDebug)
		::hWndDebug = hWndDebug;

	SOCKET sockRaw = ::WSASocket (AF_INET, SOCK_RAW, IPPROTO_ICMP, NULL, 0,0);
  
	if (sockRaw == INVALID_SOCKET) {
		TRACEF ("WSASocket() failed: " + FormatMessage (WSAGetLastError ()));
		return 0;
	}

	int nRead = ::setsockopt (sockRaw, SOL_SOCKET, SO_RCVTIMEO, (char*)&nTimeout, sizeof (nTimeout));
	
	if (nRead == SOCKET_ERROR) {
		TRACEF ("failed to set recv timeout: " + FormatMessage (WSAGetLastError ()));
		return 0;
	}
	
	nRead = ::setsockopt (sockRaw, SOL_SOCKET, SO_SNDTIMEO, (char*)&nTimeout, sizeof(nTimeout));
	
	if (nRead == SOCKET_ERROR) {
		TRACEF ("failed to set send timeout: " + FormatMessage (WSAGetLastError ()));
		return 0;
	}

	memset (&dest, 0, sizeof (dest));
	
	struct hostent * hp = ::gethostbyname (w2a (lpszAddr));

	if (!hp)
		addr = inet_addr (w2a (lpszAddr));

	if ((!hp) && (addr == INADDR_NONE) ) {
		TRACEF ("Unable to resolve " + CString (lpszAddr));
		return 0;
	}

	if (hp != NULL)
		memcpy(&(dest.sin_addr),hp->h_addr,hp->h_length);
	else
		dest.sin_addr.s_addr = addr;

	if (hp)
		dest.sin_family = hp->h_addrtype;
	else
		dest.sin_family = AF_INET;

	dest_ip = inet_ntoa(dest.sin_addr);

	nDatasize += sizeof(IcmpHeader);  

	memset (icmp_data, 0, MAX_PACKET);
	fill_icmp_data (icmp_data, nDatasize);

	for (int i = 0; i < nTries; i++) {
		((IcmpHeader*)icmp_data)->i_cksum = 0;
		((IcmpHeader*)icmp_data)->timestamp = GetTickCount();

		((IcmpHeader*)icmp_data)->i_seq = seq_no++;
		((IcmpHeader*)icmp_data)->i_cksum = checksum((USHORT*)icmp_data, nDatasize);

		//{ CString str; str.Format (_T ("::sendto (0x%p, 0x%p, %d, %d, 0x%p, %d)"), sockRaw, icmp_data, nDatasize, 0, &dest, sizeof(dest)); TRACEF (str); }
		int nWrote = ::sendto (sockRaw, icmp_data, nDatasize, 0, (struct sockaddr*)&dest, sizeof(dest));
		//TRACEF (_T ("::sendto: ") + ToString (nWrote));
	
		if (nWrote == SOCKET_ERROR){
			if (WSAGetLastError () == WSAETIMEDOUT) {
				TRACEF (_T ("timed out"));
				continue;
			}
		  
			TRACEF (_T ("sendto failed ") + FormatMessage (WSAGetLastError ()));

			return nResult;
		}
		
		if (nWrote < nDatasize) {
			CString str;

			str.Format (_T ("Wrote %d bytes"), nWrote);
		}

		{
			bool bTimeout = true;

			for (int nWait = 0; nWait < 10; nWait++) {
				struct timeval timeout;
				fd_set read;

				memset (&read, 0, sizeof (read));
				read.fd_count = 1;
				read.fd_array [0] = sockRaw;

				timeout.tv_sec = 1;
				timeout.tv_usec = 0;
				::Sleep (100);
				//TRACEF (_T ("::select [") + ToString (nWait) + _T ("]"));
				int nSelect = ::select (0, &read, NULL, NULL, &timeout);
				//TRACEF (_T ("::select: ") + ToString (nSelect));

				if (nSelect == 1) {
					bTimeout = false; 
					break;
				}
			}

			if (bTimeout) {
				TRACEF (_T ("select timed out"));
				continue;
			}
		}

		//{ CString str; str.Format (_T ("::recvfrom (0x%p, 0x%p, %d, %d, 0x%p, %d)"), sockRaw, recvbuf, MAX_PACKET, 0, &from, &fromlen); TRACEF (str); }
		nRead = ::recvfrom (sockRaw, recvbuf, MAX_PACKET, 0, (struct sockaddr*)&from, &fromlen);
		//TRACEF (_T ("::recvfrom: ") + ToString (nRead));

		if (nRead == SOCKET_ERROR){
			if (WSAGetLastError() == WSAETIMEDOUT) {
				TRACEF (_T ("timed out"));
				continue;
			}
			
			TRACEF (_T ("recvfrom failed: ") + FormatMessage (WSAGetLastError ()));

			return nResult;
		}

		nResult += decode_resp (recvbuf, nRead, &from);
		//Sleep (500);
	}

	return nResult;
}

/* 
	The response is an IP packet. We must decode the IP header to locate 
	the ICMP data 
*/
int decode_resp(char *buf, int bytes,struct sockaddr_in *from) 
{
	IpHeader *iphdr;
	IcmpHeader *icmphdr;
	unsigned short iphdrlen;
	CString str;

	iphdr = (IpHeader *)buf;

	iphdrlen = iphdr->h_len * 4 ; // number of 32-bit words *4 = bytes

	if (bytes  < iphdrlen + ICMP_MIN) {
		TRACEF ("Too few bytes from " + CString (inet_ntoa (from->sin_addr)));
	}

	icmphdr = (IcmpHeader*)(buf + iphdrlen);

	if (icmphdr->i_type != ICMP_ECHOREPLY) {
		str.Format (_T ("non-echo type %d recvd"), icmphdr->i_type);
		TRACEF (str);
		return 0;
	}
	if (icmphdr->i_id != (USHORT)GetCurrentProcessId()) {
		TRACEF (_T ("someone else's packet!"));
		return 0;
	}

	str.Format (_T ("%d bytes from %s: , icmp_seq = %d, time: %d ms"), 
		bytes, a2w (inet_ntoa (from->sin_addr)),
		icmphdr->i_seq,
		GetTickCount()-icmphdr->timestamp);		
	TRACEF (str);

	return 1;
}


USHORT checksum(USHORT *buffer, int size) {

  unsigned long cksum=0;

  while(size >1) {
	cksum+=*buffer++;
	size -=sizeof(USHORT);
  }
  
  if(size ) {
	cksum += *(UCHAR*)buffer;
  }

  cksum = (cksum >> 16) + (cksum & 0xffff);
  cksum += (cksum >>16);
  return (USHORT)(~cksum);
}
/* 
	Helper function to fill in various stuff in our ICMP request.
*/
void fill_icmp_data(char * icmp_data, int datasize){

  IcmpHeader *icmp_hdr;
  char *datapart;

  icmp_hdr = (IcmpHeader*)icmp_data;

  icmp_hdr->i_type = ICMP_ECHO;
  icmp_hdr->i_code = 0;
  icmp_hdr->i_id = (USHORT)GetCurrentProcessId();
  icmp_hdr->i_cksum = 0;
  icmp_hdr->i_seq = 0;
  
  datapart = icmp_data + sizeof(IcmpHeader);
  //
  // Place some junk in the buffer.
  //
  memset(datapart,'E', datasize - sizeof(IcmpHeader));

}
