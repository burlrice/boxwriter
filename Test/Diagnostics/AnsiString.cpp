// AnsiString.cpp: implementation of the CAnsiString class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AnsiString.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAnsiString::CAnsiString ()
:	m_psz (NULL)
{
	m_psz = Alloc (1);
}

CAnsiString::CAnsiString (const char * psz)
:	m_psz (NULL)
{
	m_psz = Alloc (strlen ((const char *)psz));
	strcpy ((char *)m_psz, (const char *)psz);
}

CAnsiString::CAnsiString (const CString & rhs)
:	m_psz (NULL)
{
	m_psz = Alloc (rhs.GetLength ());

	for (int i = 0; i < rhs.GetLength (); i++)
		m_psz [i] = (UCHAR)rhs [i];
}

CAnsiString::CAnsiString (const CAnsiString & rhs)
:	m_psz (NULL)
{
	m_psz = Alloc (rhs.GetLength ());
	strcpy ((char *)m_psz, (const char *)rhs.m_psz);
}

CAnsiString & CAnsiString::operator = (const CAnsiString & rhs)
{
	if (&rhs != this) {
		Free ();
		m_psz = Alloc (rhs.GetLength ());
		strcpy ((char *)m_psz, (const char *)rhs.m_psz);
	}

	return * this;
}

CAnsiString::~CAnsiString ()
{
	Free ();
}

UCHAR * CAnsiString::Alloc (int nLen)
{
	if (m_psz)
		Free ();

	if (nLen <= 0)
		nLen = 1;

	UCHAR * psz = new UCHAR [nLen + 1];
	memset (psz, 0, nLen + 1);

	return psz;
}

void CAnsiString::Free ()
{
	if (m_psz) {
		delete [] m_psz;
		m_psz = NULL;
	}
}

CAnsiString::operator LPCSTR() const
{
	return (const char *)m_psz;
}

CAnsiString::operator LPSTR()
{
	return (char *)m_psz;
}

CAnsiString::operator CString () const
{
	CString str;

	for (int i = 0; i < GetLength (); i++)
		str += (TCHAR)m_psz [i];
	
	return str;
}

CString CAnsiString::GetCString() const
{
	return this->operator CString ();
}

int CAnsiString::GetLength () const
{
	return m_psz ? strlen ((char *)m_psz) : 0;
}

const UCHAR & CAnsiString::operator [] (int nIndex) const
{
	static const UCHAR cErr = NULL;

	if (nIndex >= 0 && nIndex < GetLength ())
		return m_psz [nIndex];

	ASSERT (0);
	return cErr;
}
