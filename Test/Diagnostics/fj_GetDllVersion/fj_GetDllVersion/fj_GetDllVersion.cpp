// fj_DllGetVersion.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "fj_GetDllVersion.h"
#include <stdio.h>
#include <afx.h>
#include "..\..\..\..\Editor\DLL\Common\DllVer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;

// The one and only application object
CWinApp theApp;


CString ToString (VERSIONSTRUCT & v)
{
	CString str;

	if (v.m_nCustomMajor || v.m_nCustomMinor) 
		str.Format (_T ("%d.%02d.%04d.%03d"), v.m_nMajor, v.m_nMinor, v.m_nCustomMajor, v.m_nInternal);
	else {
		if (v.m_nInternal)
			str.Format (_T ("%d.%02d [%d]"), v.m_nMajor, v.m_nMinor, v.m_nInternal);
		else
			str.Format (_T ("%d.%02d"), v.m_nMajor, v.m_nMinor);
	}

	return str;
}

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	HMODULE hModule = ::GetModuleHandle(NULL);

	if (hModule != NULL)
	{
		// initialize MFC and print and error on failure
		if (!AfxWinInit(hModule, NULL, ::GetCommandLine(), 0))
		{
			// TODO: change error code to suit your needs
			_tprintf(_T("Fatal Error: MFC initialization failed\n"));
			return 1;
		}
	}
	else
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: GetModuleHandle failed\n"));
		return 1;
	}

	if (argc > 1) {
		if (HMODULE hComponent = ::LoadLibraryEx (argv [1], NULL, DONT_RESOLVE_DLL_REFERENCES)) {
			LPGETDLLVERSION lpfGetVer = (LPGETDLLVERSION)::GetProcAddress (hComponent, ("fj_GetDllVersion"));

			if (lpfGetVer) {
				VERSIONSTRUCT ver;
				int n = sizeof (ver);

				memset (&ver, 0, sizeof (ver));
				(* lpfGetVer) (&ver);
				CString strVer = ToString (ver);
				_tprintf (strVer);
				::OutputDebugString (strVer);
			}

			::FreeLibrary (hComponent);
		}

	}

	return 0;
}
