// stdafx.cpp : source file that includes just the standard includes
//	Diagnostics.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include <AFXPRIV.H>
#include <setupapi.h>
#include <Odbcinst.h>
#include <Winsock2.h>

using namespace std;

//---------------------------------------------------//
// fixes: error LNK2019: unresolved external symbol __imp___vsnprintf referenced in function _StringVPrintfWorkerA@20

#include <stdio.h>
#include <wtypes.h>

extern "C" int _imp___vsnprintf(
    char *buffer,
    size_t count,
    const char *format,
    va_list argptr
    )
{
    return vsnprintf( buffer, count, format, argptr );
}
//---------------------------------------------------//


CString GetTitle (const CString & strFile)
{
	int nIndex = strFile.ReverseFind ('\\');

	if (nIndex != -1)
		return strFile.Mid (nIndex + 1);

	return strFile;
}

CString GetOutputFilename ()
{
	TCHAR sz [MAX_PATH] = { 0 };
	HMODULE hModule = ::GetModuleHandle (NULL);
	::GetModuleFileName (hModule, sz, ARRAYSIZE (sz) - 1);
	CString strOutput = sz;

	CString str = _T ("_") + GetComputerName () + _T ("_") + GetIpAddress () + _T (".txt");

	if (!strOutput.Replace (_T (".exe"), str))
		strOutput += _T (".txt");

	return strOutput;
}

void Trace (const char * lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	Trace (a2w (lpsz), lpszFile, lLine);
}

void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	CString str, strFile (lpszFile), strHeader;


	int nIndex = strFile.ReverseFind ('\\');

	if (nIndex != -1)
		strFile = strFile.Mid (nIndex + 1);

	strHeader.Format (_T ("%s(%d):"), strFile, lLine);

	int n = COLUMN_ALIGNMENT - strHeader.GetLength ();

	if (n > 0)
		strHeader += CString (' ', n);

	str.Format (_T ("%s%s\n"), strHeader, lpsz);

	if (FILE * fp = _tfopen (GetOutputFilename (), _T ("a"))) {
		fwrite (w2a (str), str.GetLength (), 1, fp);
		fclose (fp);
	}

	::OutputDebugString (str);
	str.TrimRight ();
	cout << w2a (str) << endl;
}

void Trace (const std::vector <std::vector <std::wstring>> & v, LPCTSTR lpszFile, ULONG lLine)
{
	std::vector <int> vWidth;

	for (int i = 0; i < v.size (); i++) {
		for (int j = 0; j < v [i].size (); j++) {
			if (j >= vWidth.size ())
				vWidth.push_back (0);

			vWidth [j] = max (vWidth [j], v [i][j].length () + 2);
		}
	}

	for (int i = 0; i < v.size (); i++) {
		std::wstring str = CString (' ', 5);

		for (int j = 0; j < v [i].size (); j++) {
			int n = vWidth [j];
			
			str += v [i][j];

			if (j < (v [i].size () - 1))
				str += _T (", ");

			if (v [i][j].length () < n)
				str += (LPCTSTR)CString (' ', n - v [i][j].length ());
		}

		Trace (str.c_str (), lpszFile, lLine);
	}
}

CString FormatException (CException * p, LPCTSTR pszFile, ULONG nLine)
{
	CString str;
	CString strClass = _T ("[Unknown]");

	if (p) {
		ASSERT (p);
		const UINT nSize = 512;
		TCHAR szMsg [nSize];

		strClass = a2w (p->GetRuntimeClass ()->m_lpszClassName);
		p->GetErrorMessage (szMsg, nSize, NULL);

		#if 1 //def _DEBUG
		CRuntimeClass * pClass = p->GetRuntimeClass ();
		CString strMsg (szMsg);

		if (p->IsKindOf (RUNTIME_CLASS (CDaoException))) {
			CDaoException * pDao = (CDaoException *)p;

			if (pDao->m_pErrorInfo) {
				CString str;

				str.Format (
					_T ("\n")
					_T ("m_lErrorCode = %u (0x%08X)\n")
					_T ("m_strSource = %s\n")
					_T ("m_strDescription = %s\n")
					_T ("m_strHelpFile = %s\n")
					_T ("m_lHelpContext = %u (0x%08X)"),
					pDao->m_pErrorInfo->m_lErrorCode,	pDao->m_pErrorInfo->m_lErrorCode,
					pDao->m_pErrorInfo->m_strSource,
					pDao->m_pErrorInfo->m_strDescription,
					pDao->m_pErrorInfo->m_strHelpFile,
					pDao->m_pErrorInfo->m_lHelpContext, pDao->m_pErrorInfo->m_lHelpContext);
				strMsg += str;
			}
		}
		else if (p->IsKindOf (RUNTIME_CLASS (CDBException))) {
			CDBException * pDB = (CDBException *)p;
			CString str, strRetCode = "Unknown";

			struct EXECEPTIONSTRUCT
			{
				EXECEPTIONSTRUCT (RETCODE nCode, LPCTSTR lpsz) 
					: m_nCode (nCode), m_str (lpsz) 
				{ 
				}

				RETCODE m_nCode;
				CString m_str;
			} static const e [] = 
			{
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_API_CONFORMANCE,		_T ("The driver for a CDatabase::OpenEx or CDatabase::Open call does not conform to required ODBC API Conformance level 1 (SQL_OAC_LEVEL1).")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_CONNECT_FAIL,			_T ("Connection to the data source failed. You passed a NULL CDatabase pointer to your recordset constructor and the subsequent attempt to create a connection based on GetDefaultConnect failed. ")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_DATA_TRUNCATED,			_T ("You requested more data than you have provided storage for. For information on increasing the provided data storage for CString or CByteArray data types, see the nMaxLength argument for RFX_Text and RFX_Binary under �Macros and Globals.�")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_DYNASET_NOT_SUPPORTED,	_T ("A call to CRecordset::Open requesting a dynaset failed. Dynasets are not supported by the driver.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_EMPTY_COLUMN_LIST,		_T ("You attempted to open a table (or what you gave could not be identified as a procedure call or SELECT statement) but there are no columns identified in record field exchange (RFX) function calls in your DoFieldExchange override.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_FIELD_SCHEMA_MISMATCH,	_T ("The type of an RFX function in your DoFieldExchange override is not compatible with the column data type in the recordset.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_ILLEGAL_MODE,			_T ("You called CRecordset::Update without previously calling CRecordset::AddNew or CRecordset::Edit. ")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_LOCK_MODE_NOT_SUPPORTED,_T ("Your request to lock records for update could not be fulfilled because your ODBC driver does not support locking.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_MULTIPLE_ROWS_AFFECTED,	_T ("You called CRecordset::Update or Delete for a table with no unique key and changed multiple records.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_NO_CURRENT_RECORD,		_T ("You attempted to edit or delete a previously deleted record. You must scroll to a new current record after a deletion.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_NO_POSITIONED_UPDATES,	_T ("Your request for a dynaset could not be fulfilled because your ODBC driver does not support positioned updates.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_NO_ROWS_AFFECTED,		_T ("You called CRecordset::Update or Delete, but when the operation began the record could no longer be found.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_ODBC_LOAD_FAILED,		_T ("An attempt to load the ODBC.DLL failed; Windows could not find or could not load this DLL. This error is fatal.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_ODBC_V2_REQUIRED,		_T ("Your request for a dynaset could not be fulfilled because a Level 2-compliant ODBC driver is required.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_RECORDSET_FORWARD_ONLY,	_T ("An attempt to scroll did not succeed because the data source does not support backward scrolling.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_SNAPSHOT_NOT_SUPPORTED,	_T ("A call to CRecordset::Open requesting a snapshot failed. Snapshots are not supported by the driver. (This should only occur when the ODBC cursor library � ODBCCURS.DLL � is not present.)")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_SQL_CONFORMANCE,		_T ("The driver for a CDatabase::OpenEx or CDatabase::Open call does not conform to the required ODBC SQL Conformance level of \"Minimum\" (SQL_OSC_MINIMUM).")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_SQL_NO_TOTAL,			_T ("The ODBC driver was unable to specify the total size of a CLongBinary data value. The operation probably failed because a global memory block could not be preallocated.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_RECORDSET_READONLY,		_T ("You attempted to update a read-only recordset, or the data source is read-only. No update operations can be performed with the recordset or the CDatabase object it is associated with.")),
				EXECEPTIONSTRUCT (SQL_ERROR,							_T ("Function failed. The error message returned by ::SQLError is stored in the m_strError data member.")),
				EXECEPTIONSTRUCT (SQL_INVALID_HANDLE,					_T ("Function failed due to an invalid environment handle, connection handle, or statement handle. This indicates a programming error. No additional information is available from ::SQLError.")),
				EXECEPTIONSTRUCT (-1,									NULL),
			};

			for (int i = 0; e [i].m_nCode != -1; i++) {
				if (pDB->m_nRetCode == e [i].m_nCode) {
					strRetCode = e [i].m_str;
					break;
				}
			}

			str.Format (
				_T ("\n")
				_T ("m_nRetCode = %u (0x%p) [%s]\n")
				_T ("m_strError = %s\n")
				_T ("m_strStateNativeOrigin = %s"),
				pDB->m_nRetCode, pDB->m_nRetCode, strRetCode,
				pDB->m_strError,
				pDB->m_strStateNativeOrigin);
			strMsg += str;
		}

		str.Format (
			_T ("%s\n\n")
			_T ("exception class name: %s\n")
			_T ("%s(%u):"), 
			(LPCTSTR)strMsg, 
			a2w (pClass->m_lpszClassName), 
			pszFile, nLine);
		#else
		str = szMsg;
		#endif //_DEBUG
	}
	else 
		str = _T ("p == NULL");

	return str;
}

void HandleException (_com_error & e, LPCTSTR lpszFile, ULONG lLine)
{
	CString str = (LPCTSTR)e.Description ();

	str.Replace (_T ("\n"), _T ("\n") + CString (' ', COLUMN_ALIGNMENT));

	Trace (str, lpszFile, lLine);
}

void HandleException (CException * p, LPCTSTR lpszFile, ULONG lLine)
{
	CString strClass = _T ("[Unknown]");
	CString str = FormatException (p, lpszFile, lLine);
	CString strMsg;

	if (p) {
		strClass = a2w (p->GetRuntimeClass ()->m_lpszClassName);
		p->Delete ();
	}

	str.Replace (_T ("\n"), _T ("\n") + CString (' ', COLUMN_ALIGNMENT));

	Trace (str, lpszFile, lLine);
}

bool IsRunning (LPCTSTR lpsz)
{
	HANDLE h = ::CreateMutex (NULL, FALSE, lpsz);
	bool bResult = GetLastError () == ERROR_ALREADY_EXISTS || GetLastError () == ERROR_ACCESS_DENIED;
	::CloseHandle (h);
	return bResult;
}

CString LoadString (UINT nID)
{
	CString str;

	str.LoadString (nID);

	return str;
}

class CSystemDlgTemplate : public CDialogTemplate
{
public:
    void Attach (LPDLGTEMPLATE pTemplate)
	{
		m_hTemplate      = ::GlobalHandle( pTemplate );
		m_dwTemplateSize = GetTemplateSize( pTemplate );
		m_bSystemFont    = false;
	}
	HANDLE m_hThread;
};


static LPCTSTR lpszFont = _T ("Microsoft Sans Serif");
static const int nSize = 10;

UINT CALLBACK PropSheetPageProc (HWND hwnd, UINT uMsg, LPPROPSHEETPAGE ppsp)
{
	extern UINT CALLBACK AfxPropPageCallback(HWND, UINT message, LPPROPSHEETPAGE pPropPage);

	int nRes = AfxPropPageCallback(hwnd, uMsg, ppsp);

    switch (uMsg)
    {
	case PSPCB_CREATE:
        if (LPDLGTEMPLATE pTemplate = (LPDLGTEMPLATE)ppsp->pResource) {
			CSystemDlgTemplate dlgTemplate;

			dlgTemplate.Attach (pTemplate);
			dlgTemplate.SetFont (lpszFont, ::nSize);
			dlgTemplate.Detach ();
		}
		break;
	}

	return nRes;
}

int CALLBACK PropSheetProc (HWND hwndDlg, UINT uMsg, LPARAM lParam)
{
    switch (uMsg)
    {
        case PSCB_PRECREATE:
        {
            LPDLGTEMPLATE pTemplate = (LPDLGTEMPLATE)lParam;
            CSystemDlgTemplate dlgTemplate;

            dlgTemplate.Attach (pTemplate);
			dlgTemplate.SetFont (lpszFont, ::nSize);
            dlgTemplate.Detach ();
            break;
        }
        default:
            break;
    }

    return 0;
}

bool Elevate ()
{
	OSVERSIONINFO osver = { sizeof(osver) };

	::GetVersionEx (&osver);

	if (osver.dwMajorVersion >= 6 && osver.dwMinorVersion > 0) { // Windows7
		CString str = ::GetCommandLine ();

		if (str.Find (_T ("/runas_elevated")) == -1) {
			SHELLEXECUTEINFO sei = { 0 };
			TCHAR sz [MAX_PATH] = { 0 };

			::GetModuleFileName (NULL, sz, ARRAYSIZE (sz) - 1);
			str.Replace ('"' + CString (sz) + '"', _T (""));
			str.Replace (sz, _T (""));
			str += CString (_T (" /runas_elevated"));

			TRACEF (sz);
			TRACEF (str);

			sei.cbSize = sizeof (sei);
			sei.lpVerb = _T ("runas");
			sei.lpFile = sz;
			sei.lpParameters = str;
			sei.nShow = SW_NORMAL;

			::ShellExecuteEx (&sei);
			exit (0);

			return false;
		}
	}

	return true;
}

int FindCaseInsensitive (const CString & str, const CString & strFind)
{
	LPCTSTR lpsz = str;

	for (int i = 0; i < str.GetLength () /* - strFind.GetLength () */; i++, lpsz++) 
		if (!_tcsncicmp (lpsz, strFind, strFind.GetLength ()))
			return i;

	return -1;
}

int FindCaseInsensitive (const CStringArray & v, const CString & strFind)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (!v [i].CompareNoCase (strFind))
			return i;

	return -1;
}

void Append (CStringArray & v, const CString & str)
{
	if (FindCaseInsensitive (v, str) == -1)
		v.Add (str);
}

void Remove (CStringArray & v, const CString & str)
{
	int nIndex = FindCaseInsensitive (v, str);

	while (nIndex != -1) {
		v.RemoveAt (nIndex);
		nIndex = FindCaseInsensitive (v, str);
	}
}

CString Update (const CString & strCmdLine, const CString & str, bool bAdd)
{
	CStringArray v;

	TokenizeCmdLine (strCmdLine, v);
	return Update (v, str, bAdd);
}

CString Update (CStringArray & v, const CString & str, bool bAdd)
{
	CString strResult;

	if (bAdd)
		Append (v, str);
	else
		Remove (v, str);

	for (int i = 0; i < v.GetSize (); i++)
		strResult += v [i] + _T (" ");

	return strResult;
}

CString Extract (const CString & strParse, const CString & strBegin)
{
	CString str;
	int nIndex = strParse.Find (strBegin);

	if (nIndex != -1) 
		str = strParse.Mid (nIndex + strBegin.GetLength ());

	return str;
}

CString Extract (const CString & strParse, const CString & strBegin, const CString & strEnd)
{
	CString str;
	int nIndex [2] = 
	{
		strParse.Find (strBegin),

		// start looking for strEnd after strBegin
		strParse.Find (strEnd, (nIndex [0] == -1) ? 0 : nIndex [0] + strBegin.GetLength ()), 
	};

	if (nIndex [0] != -1 && nIndex [1] != -1) {
		int nStart = nIndex [0] + strBegin.GetLength ();
		int nLen = nIndex [1] - nStart;

		if (nLen > 0)
			str = strParse.Mid (nStart, nLen);
	}

	if (str.GetLength () == 0 && strEnd == _T (" "))
		str = Extract (strParse, strBegin);

	return str;
}

CString TokenizeCmdLine (const CString &strInput, CStringArray &v)
{
	CString strSeg, str (strInput);

	while (str.GetLength ()) {
		TCHAR c = str [0];

		if (c == '\"') {
			CString strTmp = Extract (str, _T ("\""), _T ("\""));
			
			if (strTmp.GetLength ()) {
				int nLen = strTmp.GetLength () + 2;

				strSeg += _T ("\"") + strTmp + _T ("\"");
	
				if (nLen <= str.GetLength ())
					str.Delete (0, nLen);
				
				v.Add (strSeg);
				strSeg.Empty ();
			}

			continue;
		}
		else if (c == ' ') {
			if (strSeg.GetLength ()) {
				v.Add (strSeg);
				strSeg.Empty ();
			}
		}
		else 
			strSeg += c;

		str.Delete (0);
	}

	if (strSeg.GetLength ())
		v.Add (strSeg);

	return v.GetSize () ? v [0] : _T ("");
}

CString GetHomeDir ()
{
	TCHAR sz [MAX_PATH] = { 0 };

	HMODULE hModule = ::GetModuleHandle (NULL);
	::GetModuleFileName (hModule, sz, ARRAYSIZE (sz) - 1);

	const CString strPath (sz);
	CString strPathLower (sz);
	strPathLower.MakeLower ();

	VERIFY (::GetFileTitle (strPath, sz, ARRAYSIZE (sz) - 1) == 0);
	
	CString strTitle (sz);
	strTitle.MakeLower ();

	int nIndex = strPathLower.Find (strTitle);

	ASSERT (nIndex != -1);
	CString str = strPath.Left (nIndex - 1);

	return str;
}

CString execute (const CString & str)
{
	CString strResult;
	HANDLE hOutputReadTmp,hOutputRead,hOutputWrite;
	HANDLE hInputWriteTmp,hInputRead,hInputWrite;
	HANDLE hErrorWrite;
	SECURITY_ATTRIBUTES sa;
	HANDLE hStdIn = NULL; // Handle to parents std input.

	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;

	if (!::CreatePipe (&hOutputReadTmp, &hOutputWrite, &sa, 0))
		TRACEF (FormatMessage (::GetLastError ()));

	// Create a duplicate of the output write handle for the std error
	// write handle. This is necessary in case the child application
	// closes one of its std output handles.
	if (!::DuplicateHandle (::GetCurrentProcess (), hOutputWrite, GetCurrentProcess(), &hErrorWrite, 0, TRUE, DUPLICATE_SAME_ACCESS))
		TRACEF (FormatMessage (::GetLastError ()));


	// Create the child input pipe.
	if (!::CreatePipe (&hInputRead, &hInputWriteTmp, &sa, 0))
		TRACEF (FormatMessage (::GetLastError ()));


	// Create new output read handle and the input write handles. Set
	// the Properties to FALSE. Otherwise, the child inherits the
	// properties and, as a result, non-closeable handles to the pipes
	// are created.
	if (!::DuplicateHandle (::GetCurrentProcess (), hOutputReadTmp, ::GetCurrentProcess (), &hOutputRead, 0, FALSE, DUPLICATE_SAME_ACCESS))
		TRACEF (FormatMessage (::GetLastError ()));

	if (!::DuplicateHandle (::GetCurrentProcess (), hInputWriteTmp, ::GetCurrentProcess (), &hInputWrite, 0, FALSE, DUPLICATE_SAME_ACCESS))
		TRACEF (FormatMessage (::GetLastError ()));


	// Close inheritable copies of the handles you do not want to be inherited.
	if (!::CloseHandle (hOutputReadTmp)) TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hInputWriteTmp)) TRACEF (FormatMessage (::GetLastError ()));


	// Get std input handle so you can close it and force the ReadFile to
	// fail when you want the input thread to exit.
	if ((hStdIn = GetStdHandle(STD_INPUT_HANDLE)) == INVALID_HANDLE_VALUE)
		TRACEF (FormatMessage (::GetLastError ()));

	{
		PROCESS_INFORMATION pi;
		STARTUPINFO si;
		TCHAR sz [MAX_PATH] = { 0 };

		ZeroMemory (&si, sizeof (STARTUPINFO));
		si.cb			= sizeof (STARTUPINFO);
		si.dwFlags		= STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
		si.wShowWindow	= SW_HIDE;
		si.hStdOutput	= hOutputWrite;
		si.hStdInput	= hInputRead;
		si.hStdError	= hErrorWrite;

		_tcscpy (sz, str);
		//TRACEF (sz);

		if (!::CreateProcess (NULL, sz, NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi))
			TRACEF (FormatMessage (::GetLastError ()));

		if (!::CloseHandle (pi.hThread)) 
			TRACEF (FormatMessage (::GetLastError ()));
	}


	if (!::CloseHandle (hOutputWrite))	TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hInputRead ))	TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hErrorWrite))	TRACEF (FormatMessage (::GetLastError ()));

	while (1) {
		const int nSize = 256;
		CHAR lpBuffer [nSize + 1] = { 0 };
		DWORD nBytesRead = 0;
		DWORD nCharsWritten = 0;

		if (!::ReadFile (hOutputRead, lpBuffer, nSize, &nBytesRead, NULL) || !nBytesRead) {
			if (::GetLastError() == ERROR_BROKEN_PIPE)
				break; // pipe done - normal exit path.
			else
				TRACEF (FormatMessage (::GetLastError ()));
		}

		strResult += /* a2w */ (lpBuffer);
		//::OutputDebugString (a2w (lpBuffer)); ::Sleep (1);
	}

	if (!::CloseHandle (hOutputRead))	TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hInputWrite))	TRACEF (FormatMessage (::GetLastError ()));

	return strResult;
}

static BOOL CALLBACK EnumWindowsProc (HWND hWnd, LPARAM lParam)
{
	CArray <HWND, HWND> & v = * (CArray <HWND, HWND> *)lParam;

	v.Add (hWnd);

	return TRUE;
}

HWND FindKeyboard()
{
	const CString strFind =  _T ("Virtual keyboard");
	CArray <HWND, HWND> v;

	::EnumWindows (EnumWindowsProc, (LPARAM)&v);

	for (int i = 0; i < v.GetSize (); i++) {
		HWND hwnd = v [i];
		TCHAR sz [512] = { 0 };

		::GetWindowText (hwnd, sz, ARRAYSIZE (sz) - 1);

		if (!_tcsncmp (sz, strFind, strFind.GetLength ()))
			return hwnd;
	}

	return NULL;
}


bool IsChecked (CMap <UINT, UINT, bool, bool> & v, UINT nStrID)
{
	bool bResult = false;

	v.Lookup (nStrID, bResult);

	return bResult;
}



int Find (CStringArray & v, const CString & strFile)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (!v [i].CompareNoCase (strFile))
			return i;

	return -1;
}

CString FormatFileTime (const FILETIME & ft)
{
	TCHAR lpszString [MAX_PATH] = { 0 };
	DWORD dwSize = sizeof (lpszString);
    SYSTEMTIME stUTC, stLocal;
    DWORD dwRet;

    // Convert the last-write time to local time.
    FileTimeToSystemTime(&ft, &stUTC);
    SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

    // Build a string showing the date and time.
    dwRet = _stprintf (lpszString, 
        TEXT("%02d/%02d/%d  %02d:%02d"),
        stLocal.wMonth, stLocal.wDay, stLocal.wYear,
        stLocal.wHour, stLocal.wMinute);

	return lpszString;
}

HRESULT CreateShortcut (CString strTargetfile, CString strTargetargs, CString strLinkfile, CString strDescription, 
	int iShowmode, CString strCurdir, CString strIconfile, int iIconindex, HWND hwndProgress, UINT nMsg)
{
	HRESULT       hRes;
	IShellLink*   pShellLink;
	IPersistFile* pPersistFile;

	{
		TCHAR szTarget [MAX_PATH] = { 0 };
		TCHAR szLink [MAX_PATH] = { 0 };

		::GetFileTitle (strTargetfile, szTarget, ARRAYSIZE (szTarget));
		::GetFileTitle (strLinkfile, szLink, ARRAYSIZE (szLink));

		CString strTarget = szTarget;
		CString strLink = szLink;

		strTarget.MakeLower ();
		strLink.MakeLower ();

		strTarget.Replace (_T (".exe"), _T (""));
		strLink.Replace (_T (".lnk"), _T (""));

		ASSERT (strTarget == strLink || strLink.Find (strTarget) != -1);
	}

	hRes = E_INVALIDARG;
	if ((strTargetfile.GetLength () > 0) &&
		(strLinkfile.GetLength () > 0) &&
		(iShowmode >= 0) &&
		(iIconindex >= 0))
	{
		
		::DeleteFile (strLinkfile);

		hRes = CoCreateInstance (
			CLSID_ShellLink,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IShellLink,
			(LPVOID *)&pShellLink);
		
		if (SUCCEEDED (hRes)) {
			hRes = pShellLink->SetPath(strTargetfile);
			hRes = pShellLink->SetArguments(strTargetargs);
	
			if (strDescription.GetLength () > 0) 
				hRes = pShellLink->SetDescription(strDescription);
			
			if (iShowmode > 0) 
				hRes = pShellLink->SetShowCmd(iShowmode);

			if (strCurdir.GetLength () > 0) 
				hRes = pShellLink->SetWorkingDirectory(strCurdir);

			if (strIconfile.GetLength () > 0 && iIconindex >= 0) 
				hRes = pShellLink->SetIconLocation(strIconfile, iIconindex);

			hRes = pShellLink->QueryInterface (IID_IPersistFile, (LPVOID*)&pPersistFile);

			if (SUCCEEDED(hRes)) {
				BSTR b = strLinkfile.AllocSysString ();
				hRes = pPersistFile->Save (b, TRUE);

				//#ifdef _DEBUG
				{
					CString str;

					str.Format (
						_T ("%sCreateShortcut: %s\n")
						_T ("%sstrTargetfile:  %s\n")
						_T ("%sstrTargetargs:  %s\n")
						_T ("%sstrLinkfile:    %s\n")
						_T ("strDescription: %s\n")
						_T ("%siShowmode:      %d\n")
						_T ("%sstrCurdir:      %s\n")
						_T ("%sstrIconfile:    %s\n")
						_T ("%siIconindex:     %d\n")
						_T ("%s%s"),
						CString (' ', COLUMN_ALIGNMENT), CString (b),
						CString (' ', COLUMN_ALIGNMENT), strTargetfile, 
						CString (' ', COLUMN_ALIGNMENT), strTargetargs, 
						CString (' ', COLUMN_ALIGNMENT), strLinkfile, 
						CString (' ', COLUMN_ALIGNMENT), strDescription, 
						CString (' ', COLUMN_ALIGNMENT), iShowmode, 
						CString (' ', COLUMN_ALIGNMENT), strCurdir, 
						CString (' ', COLUMN_ALIGNMENT), strIconfile, 
						CString (' ', COLUMN_ALIGNMENT), iIconindex,
						CString (' ', COLUMN_ALIGNMENT), SUCCEEDED (hRes) ? _T ("SUCCEEDED") : _T ("FAILED"));
					TRACEF (str);

					if (hwndProgress)
						::PostMessage (hwndProgress, nMsg, (LPARAM)new CString (str), 0);
				}
				//#endif

				::SysFreeString (b);

				pPersistFile->Release();
			}

			pShellLink->Release();
		}
	}

	return hRes;
}

CString GetShortcutCmdLine (const CString & strTargetfile, CString & strExe)
{
	HRESULT       hRes;
	IShellLink*   pShellLink;
	IPersistFile* pPersistFile;
	CString strResult;

	hRes = E_INVALIDARG;

	if ((strTargetfile.GetLength () > 0)) {
		hRes = CoCreateInstance (
			CLSID_ShellLink,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IShellLink,
			(LPVOID *)&pShellLink);
		
		if (SUCCEEDED (hRes)) {
			TCHAR sz [MAX_PATH] = { 0 };
			//hRes = pShellLink->SetPath(strTargetfile);

			hRes = pShellLink->QueryInterface (IID_IPersistFile, (LPVOID*)&pPersistFile);

			if (SUCCEEDED(hRes)) {
				hRes = pPersistFile->Load (strTargetfile, 	STGM_READ);

				hRes = pShellLink->GetArguments (sz, ARRAYSIZE (sz));
				strResult = sz;

				memset (sz, 0, ARRAYSIZE (sz));
				hRes = pShellLink->GetPath (sz, ARRAYSIZE (sz), NULL, 0);
				strExe = sz;

				pPersistFile->Release();
			}
	
			pShellLink->Release();
		}
	}

	return strResult;
}

int GetFiles (const CString & strDir, const CString & strFile, CStringArray & v)
{
	if (strFile.Find (_T ("*")) == -1 && strFile != _T ("*.*")) {
		CStringArray vTmp;

		GetFiles (strDir, _T ("*.*"), vTmp);

		for (int i = 0; i < vTmp.GetSize (); i++) {
			CString str = vTmp [i];

			if (str.Replace (_T ("\\") + strFile, _T ("")))
				v.Add (vTmp [i]);
		}

		return v.GetSize ();
	}

	int nFiles = 0;
	WIN32_FIND_DATA fd;
	const CString str = strDir + _T ("\\") + strFile;
	HANDLE hFind = ::FindFirstFile (str, &fd);
	bool bMore = hFind != INVALID_HANDLE_VALUE;

	//TRACEF (str);

	while (bMore) {
		CString str = strDir;
		
		if (_tcscmp (fd.cFileName, _T (".")) != 0 && _tcscmp (fd.cFileName, _T (".."))) {
			if (str [str.GetLength () - 1] != '\\')
				str += '\\';

			str += fd.cFileName;

			v.Add (str);
			nFiles++;

			if (::GetFileAttributes (str) & FILE_ATTRIBUTE_DIRECTORY) 
				nFiles += GetFiles (strDir + _T ("\\") + fd.cFileName, strFile, v);
		}

		ZeroMemory (&fd, sizeof (fd));
		bMore = ::FindNextFile (hFind, &fd) ? true : false;
	}

	::FindClose (hFind);

	return nFiles;
}

//#ifdef _DEBUG
void EnumKnownFolders ()
{
	#define MAKEMAPENTRY(k) { &k, _T (#k), }

	struct
	{
		const KNOWNFOLDERID * m_k;
		LPCTSTR m_lpsz;
	} map [] =
	{
		MAKEMAPENTRY(FOLDERID_NetworkFolder),
		MAKEMAPENTRY(FOLDERID_ComputerFolder),  
		MAKEMAPENTRY(FOLDERID_InternetFolder), 
		MAKEMAPENTRY(FOLDERID_ControlPanelFolder),
		MAKEMAPENTRY(FOLDERID_PrintersFolder),   
		MAKEMAPENTRY(FOLDERID_SyncManagerFolder),
		MAKEMAPENTRY(FOLDERID_SyncSetupFolder), 
		MAKEMAPENTRY(FOLDERID_ConflictFolder),      
		MAKEMAPENTRY(FOLDERID_SyncResultsFolder),   
		MAKEMAPENTRY(FOLDERID_RecycleBinFolder),    
		MAKEMAPENTRY(FOLDERID_ConnectionsFolder),   
		MAKEMAPENTRY(FOLDERID_Fonts),               
		MAKEMAPENTRY(FOLDERID_Desktop),             
		MAKEMAPENTRY(FOLDERID_Startup),             
		MAKEMAPENTRY(FOLDERID_Programs),            
		MAKEMAPENTRY(FOLDERID_StartMenu),           
		MAKEMAPENTRY(FOLDERID_Recent),              
		MAKEMAPENTRY(FOLDERID_SendTo),              
		MAKEMAPENTRY(FOLDERID_Documents),           
		MAKEMAPENTRY(FOLDERID_Favorites),           
		MAKEMAPENTRY(FOLDERID_NetHood),             
		MAKEMAPENTRY(FOLDERID_PrintHood),           
		MAKEMAPENTRY(FOLDERID_Templates),           
		MAKEMAPENTRY(FOLDERID_CommonStartup),       
		MAKEMAPENTRY(FOLDERID_CommonPrograms),      
		MAKEMAPENTRY(FOLDERID_CommonStartMenu),     
		MAKEMAPENTRY(FOLDERID_PublicDesktop),       
		MAKEMAPENTRY(FOLDERID_ProgramData),         
		MAKEMAPENTRY(FOLDERID_CommonTemplates),     
		MAKEMAPENTRY(FOLDERID_PublicDocuments),     
		MAKEMAPENTRY(FOLDERID_RoamingAppData),      
		MAKEMAPENTRY(FOLDERID_LocalAppData),        
		MAKEMAPENTRY(FOLDERID_LocalAppDataLow),     
		MAKEMAPENTRY(FOLDERID_InternetCache),       
		MAKEMAPENTRY(FOLDERID_Cookies),             
		MAKEMAPENTRY(FOLDERID_History),             
		MAKEMAPENTRY(FOLDERID_System),              
		MAKEMAPENTRY(FOLDERID_SystemX86),           
		MAKEMAPENTRY(FOLDERID_Windows),             
		MAKEMAPENTRY(FOLDERID_Profile),             
		MAKEMAPENTRY(FOLDERID_Pictures),            
		MAKEMAPENTRY(FOLDERID_ProgramFilesX86),     
		MAKEMAPENTRY(FOLDERID_ProgramFilesCommonX86),
		MAKEMAPENTRY(FOLDERID_ProgramFilesX64),     
		MAKEMAPENTRY(FOLDERID_ProgramFilesCommonX64),
		MAKEMAPENTRY(FOLDERID_ProgramFiles),       
		MAKEMAPENTRY(FOLDERID_ProgramFilesCommon), 
		MAKEMAPENTRY(FOLDERID_UserProgramFiles),   
		MAKEMAPENTRY(FOLDERID_UserProgramFilesCommon),
		MAKEMAPENTRY(FOLDERID_AdminTools),          
		MAKEMAPENTRY(FOLDERID_CommonAdminTools),    
		MAKEMAPENTRY(FOLDERID_Music),               
		MAKEMAPENTRY(FOLDERID_Videos),              
		MAKEMAPENTRY(FOLDERID_Ringtones),           
		MAKEMAPENTRY(FOLDERID_PublicPictures),      
		MAKEMAPENTRY(FOLDERID_PublicMusic),         
		MAKEMAPENTRY(FOLDERID_PublicVideos),        
		MAKEMAPENTRY(FOLDERID_PublicRingtones),     
		MAKEMAPENTRY(FOLDERID_ResourceDir),         
		MAKEMAPENTRY(FOLDERID_LocalizedResourcesDir), 
		MAKEMAPENTRY(FOLDERID_CommonOEMLinks),      
		MAKEMAPENTRY(FOLDERID_CDBurning),           
		MAKEMAPENTRY(FOLDERID_UserProfiles),        
		MAKEMAPENTRY(FOLDERID_Playlists),           
		MAKEMAPENTRY(FOLDERID_SamplePlaylists),     
		MAKEMAPENTRY(FOLDERID_SampleMusic),         
		MAKEMAPENTRY(FOLDERID_SamplePictures),      
		MAKEMAPENTRY(FOLDERID_SampleVideos),        
		MAKEMAPENTRY(FOLDERID_PhotoAlbums),         
		MAKEMAPENTRY(FOLDERID_Public),              
		MAKEMAPENTRY(FOLDERID_ChangeRemovePrograms),
		MAKEMAPENTRY(FOLDERID_AppUpdates),          
		MAKEMAPENTRY(FOLDERID_AddNewPrograms),      
		MAKEMAPENTRY(FOLDERID_Downloads),           
		MAKEMAPENTRY(FOLDERID_PublicDownloads),     
		MAKEMAPENTRY(FOLDERID_SavedSearches),       
		MAKEMAPENTRY(FOLDERID_QuickLaunch),         
		MAKEMAPENTRY(FOLDERID_Contacts),            
//		MAKEMAPENTRY(FOLDERID_SidebarParts),                
//		MAKEMAPENTRY(FOLDERID_SidebarDefaultParts),         
		MAKEMAPENTRY(FOLDERID_PublicGameTasks),     
		MAKEMAPENTRY(FOLDERID_GameTasks),           
		MAKEMAPENTRY(FOLDERID_SavedGames),          
		MAKEMAPENTRY(FOLDERID_Games),               
		MAKEMAPENTRY(FOLDERID_SEARCH_MAPI),         
		MAKEMAPENTRY(FOLDERID_SEARCH_CSC),          
		MAKEMAPENTRY(FOLDERID_Links),               
		MAKEMAPENTRY(FOLDERID_UsersFiles),          
		MAKEMAPENTRY(FOLDERID_UsersLibraries),      
		MAKEMAPENTRY(FOLDERID_SearchHome),          
		MAKEMAPENTRY(FOLDERID_OriginalImages),      
		MAKEMAPENTRY(FOLDERID_DocumentsLibrary),    
		MAKEMAPENTRY(FOLDERID_MusicLibrary),        
		MAKEMAPENTRY(FOLDERID_PicturesLibrary),     
		MAKEMAPENTRY(FOLDERID_VideosLibrary),       
		MAKEMAPENTRY(FOLDERID_RecordedTVLibrary),   
		MAKEMAPENTRY(FOLDERID_HomeGroup),           
		MAKEMAPENTRY(FOLDERID_DeviceMetadataStore), 
		MAKEMAPENTRY(FOLDERID_Libraries),           
		MAKEMAPENTRY(FOLDERID_PublicLibraries),     
		MAKEMAPENTRY(FOLDERID_UserPinned),          
		MAKEMAPENTRY(FOLDERID_ImplicitAppShortcuts),
	};

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		CString str, strPath = GetKnownFolderPath ((* map [i].m_k));
		int csidl = GetCSIDL ((* map [i].m_k));

		str.Format (
			_T ("%s --> [%d] %s\n")
			_T ("%s\"%s\""),
			map [i].m_lpsz,
			csidl, GetCSIDLName (csidl), 
			CString (' ', COLUMN_ALIGNMENT), strPath);
		TRACEF (str);
	}
}
//#endif

#include <shlobj.h>
/*
//#include "C:\Program Files\Microsoft SDKs\Windows\v5.0\Include\ShFolder.h"
#define CSIDL_DESKTOP                   0x0000        // <desktop>
#define CSIDL_INTERNET                  0x0001        // Internet Explorer (icon on desktop)
#define CSIDL_PROGRAMS                  0x0002        // Start Menu\Programs
#define CSIDL_CONTROLS                  0x0003        // My Computer\Control Panel
#define CSIDL_PRINTERS                  0x0004        // My Computer\Printers
#define CSIDL_PERSONAL                  0x0005        // My Documents
#define CSIDL_FAVORITES                 0x0006        // <user name>\Favorites
#define CSIDL_STARTUP                   0x0007        // Start Menu\Programs\Startup
#define CSIDL_RECENT                    0x0008        // <user name>\Recent
#define CSIDL_SENDTO                    0x0009        // <user name>\SendTo
#define CSIDL_BITBUCKET                 0x000a        // <desktop>\Recycle Bin
#define CSIDL_STARTMENU                 0x000b        // <user name>\Start Menu
#define CSIDL_MYDOCUMENTS               0x000c        // logical "My Documents" desktop icon
#define CSIDL_MYMUSIC                   0x000d        // "My Music" folder
#define CSIDL_MYVIDEO                   0x000e        // "My Videos" folder
#define CSIDL_DESKTOPDIRECTORY          0x0010        // <user name>\Desktop
#define CSIDL_DRIVES                    0x0011        // My Computer
#define CSIDL_NETWORK                   0x0012        // Network Neighborhood (My Network Places)
#define CSIDL_NETHOOD                   0x0013        // <user name>\nethood
#define CSIDL_FONTS                     0x0014        // windows\fonts
#define CSIDL_TEMPLATES                 0x0015
#define CSIDL_COMMON_STARTMENU          0x0016        // All Users\Start Menu
#define CSIDL_COMMON_PROGRAMS           0X0017        // All Users\Start Menu\Programs
#define CSIDL_COMMON_STARTUP            0x0018        // All Users\Startup
#define CSIDL_COMMON_DESKTOPDIRECTORY   0x0019        // All Users\Desktop
#define CSIDL_APPDATA                   0x001a        // <user name>\Application Data
#define CSIDL_PRINTHOOD                 0x001b        // <user name>\PrintHood

#ifndef CSIDL_LOCAL_APPDATA
#define CSIDL_LOCAL_APPDATA             0x001c        // <user name>\Local Settings\Applicaiton Data (non roaming)
#endif // CSIDL_LOCAL_APPDATA

#define CSIDL_ALTSTARTUP                0x001d        // non localized startup
#define CSIDL_COMMON_ALTSTARTUP         0x001e        // non localized common startup
#define CSIDL_COMMON_FAVORITES          0x001f

#ifndef _SHFOLDER_H_
#define CSIDL_INTERNET_CACHE            0x0020
#define CSIDL_COOKIES                   0x0021
#define CSIDL_HISTORY                   0x0022
#define CSIDL_COMMON_APPDATA            0x0023        // All Users\Application Data
#define CSIDL_WINDOWS                   0x0024        // GetWindowsDirectory()
#define CSIDL_SYSTEM                    0x0025        // GetSystemDirectory()
#define CSIDL_PROGRAM_FILES             0x0026        // C:\Program Files
#define CSIDL_MYPICTURES                0x0027        // C:\Program Files\My Pictures
#endif // _SHFOLDER_H_

#define CSIDL_PROFILE                   0x0028        // USERPROFILE
#define CSIDL_SYSTEMX86                 0x0029        // x86 system directory on RISC
#define CSIDL_PROGRAM_FILESX86          0x002a        // x86 C:\Program Files on RISC

#ifndef _SHFOLDER_H_
#define CSIDL_PROGRAM_FILES_COMMON      0x002b        // C:\Program Files\Common
#endif // _SHFOLDER_H_

#define CSIDL_PROGRAM_FILES_COMMONX86   0x002c        // x86 Program Files\Common on RISC
#define CSIDL_COMMON_TEMPLATES          0x002d        // All Users\Templates

#ifndef _SHFOLDER_H_
#define CSIDL_COMMON_DOCUMENTS          0x002e        // All Users\Documents
#define CSIDL_COMMON_ADMINTOOLS         0x002f        // All Users\Start Menu\Programs\Administrative Tools
#define CSIDL_ADMINTOOLS                0x0030        // <user name>\Start Menu\Programs\Administrative Tools
#endif // _SHFOLDER_H_

#define CSIDL_CONNECTIONS               0x0031        // Network and Dial-up Connections
#define CSIDL_COMMON_MUSIC              0x0035        // All Users\My Music
#define CSIDL_COMMON_PICTURES           0x0036        // All Users\My Pictures
#define CSIDL_COMMON_VIDEO              0x0037        // All Users\My Video
#define CSIDL_RESOURCES                 0x0038        // Resource Direcotry

#ifndef _SHFOLDER_H_
#define CSIDL_RESOURCES_LOCALIZED       0x0039        // Localized Resource Direcotry
#endif // _SHFOLDER_H_

#define CSIDL_COMMON_OEM_LINKS          0x003a        // Links to All Users OEM specific apps
#define CSIDL_CDBURN_AREA               0x003b        // USERPROFILE\Local Settings\Application Data\Microsoft\CD Burning
// unused                               0x003c
#define CSIDL_COMPUTERSNEARME           0x003d        // Computers Near Me (computered from Workgroup membership)

#ifndef _SHFOLDER_H_
#define CSIDL_FLAG_CREATE               0x8000        // combine with CSIDL_ value to force folder creation in SHGetFolderPath()
#endif // _SHFOLDER_H_

#define CSIDL_FLAG_DONT_VERIFY          0x4000        // combine with CSIDL_ value to return an unverified folder path
#define CSIDL_FLAG_NO_ALIAS             0x1000        // combine with CSIDL_ value to insure non-alias versions of the pidl
#define CSIDL_FLAG_PER_USER_INIT        0x0800        // combine with CSIDL_ value to indicate per-user init (eg. upgrade)
#define CSIDL_FLAG_MASK                 0xFF00        // mask for all possible flag values
*/ 

struct
{
	int m_csidl;
	const KNOWNFOLDERID * m_prfid;
	LPCTSTR m_lpsz;
} 
static const foldermap [] = 
{
	{ CSIDL_ADMINTOOLS,					&FOLDERID_AdminTools,				_T ("CSIDL_ADMINTOOLS"),				},
	{ CSIDL_ALTSTARTUP,					&FOLDERID_Startup,					_T ("CSIDL_ALTSTARTUP"),				},
	{ CSIDL_APPDATA,					&FOLDERID_RoamingAppData,			_T ("CSIDL_APPDATA"),					},
	{ CSIDL_BITBUCKET,					&FOLDERID_RecycleBinFolder,			_T ("CSIDL_BITBUCKET"),					},
	{ CSIDL_CDBURN_AREA,				&FOLDERID_CDBurning,				_T ("CSIDL_CDBURN_AREA"),				},
	{ CSIDL_COMMON_ADMINTOOLS,			&FOLDERID_CommonAdminTools,			_T ("CSIDL_COMMON_ADMINTOOLS"),			},
	{ CSIDL_COMMON_ALTSTARTUP,			&FOLDERID_CommonStartup,			_T ("CSIDL_COMMON_ALTSTARTUP"),			},
	{ CSIDL_COMMON_APPDATA,				&FOLDERID_ProgramData,				_T ("CSIDL_COMMON_APPDATA"),			},
	{ CSIDL_COMMON_DESKTOPDIRECTORY,	&FOLDERID_PublicDesktop,			_T ("CSIDL_COMMON_DESKTOPDIRECTORY"),	},
	{ CSIDL_COMMON_DOCUMENTS,			&FOLDERID_PublicDocuments,			_T ("CSIDL_COMMON_DOCUMENTS"),			},
	{ CSIDL_COMMON_FAVORITES,			&FOLDERID_Favorites,				_T ("CSIDL_COMMON_FAVORITES"),			},
	{ CSIDL_COMMON_MUSIC,				&FOLDERID_PublicMusic,				_T ("CSIDL_COMMON_MUSIC"),				},
	{ CSIDL_COMMON_OEM_LINKS,			&FOLDERID_CommonOEMLinks,			_T ("CSIDL_COMMON_OEM_LINKS"),			},
	{ CSIDL_COMMON_PICTURES,			&FOLDERID_PublicPictures,			_T ("CSIDL_COMMON_PICTURES"),			},
	{ CSIDL_COMMON_PROGRAMS,			&FOLDERID_CommonPrograms,			_T ("CSIDL_COMMON_PROGRAMS"),			},
	{ CSIDL_COMMON_STARTMENU,			&FOLDERID_CommonStartMenu,			_T ("CSIDL_COMMON_STARTMENU"),			},
	{ CSIDL_COMMON_STARTUP,				&FOLDERID_CommonStartup,			_T ("CSIDL_COMMON_STARTUP"),			},
	{ CSIDL_COMMON_TEMPLATES,			&FOLDERID_CommonTemplates,			_T ("CSIDL_COMMON_TEMPLATES"),			},
	{ CSIDL_COMMON_VIDEO,				&FOLDERID_PublicVideos,				_T ("CSIDL_COMMON_VIDEO"),				},
	{ CSIDL_COMPUTERSNEARME,			&FOLDERID_NetworkFolder,			_T ("CSIDL_COMPUTERSNEARME"),			},
	{ CSIDL_CONNECTIONS,				&FOLDERID_ConnectionsFolder,		_T ("CSIDL_CONNECTIONS"),				},
	{ CSIDL_CONTROLS,					&FOLDERID_ControlPanelFolder,		_T ("CSIDL_CONTROLS"),					},
	{ CSIDL_COOKIES,					&FOLDERID_Cookies,					_T ("CSIDL_COOKIES"),					},
	{ CSIDL_DESKTOP,					&FOLDERID_Desktop,					_T ("CSIDL_DESKTOP"),					},
	{ CSIDL_DESKTOPDIRECTORY,			&FOLDERID_Desktop,					_T ("CSIDL_DESKTOPDIRECTORY"),			},
	{ CSIDL_DRIVES,						&FOLDERID_ComputerFolder,			_T ("CSIDL_DRIVES"),					},
	{ CSIDL_FAVORITES,					&FOLDERID_Favorites,				_T ("CSIDL_FAVORITES"),					},
	{ CSIDL_FONTS,						&FOLDERID_Fonts,					_T ("CSIDL_FONTS"),						},
	{ CSIDL_HISTORY,					&FOLDERID_History,					_T ("CSIDL_HISTORY"),					},
	{ CSIDL_INTERNET,					&FOLDERID_InternetFolder,			_T ("CSIDL_INTERNET"),					},
	{ CSIDL_INTERNET_CACHE,				&FOLDERID_InternetCache,			_T ("CSIDL_INTERNET_CACHE"),			},
	{ CSIDL_LOCAL_APPDATA,				&FOLDERID_LocalAppData,				_T ("CSIDL_LOCAL_APPDATA"),				},
	{ CSIDL_MYDOCUMENTS,				&FOLDERID_Documents,				_T ("CSIDL_MYDOCUMENTS"),				},
	{ CSIDL_MYMUSIC,					&FOLDERID_Music,					_T ("CSIDL_MYMUSIC"),					},
	{ CSIDL_MYPICTURES,					&FOLDERID_Pictures,					_T ("CSIDL_MYPICTURES"),				},
	{ CSIDL_MYVIDEO,					&FOLDERID_Videos,					_T ("CSIDL_MYVIDEO"),					},
	{ CSIDL_NETHOOD,					&FOLDERID_NetHood,					_T ("CSIDL_NETHOOD"),					},
	{ CSIDL_NETWORK,					&FOLDERID_NetworkFolder,			_T ("CSIDL_NETWORK"),					},
	{ CSIDL_PERSONAL,					&FOLDERID_Documents,				_T ("CSIDL_PERSONAL"),					},
	{ CSIDL_PRINTERS,					&FOLDERID_PrintersFolder,			_T ("CSIDL_PRINTERS"),					},
	{ CSIDL_PRINTHOOD,					&FOLDERID_PrintHood,				_T ("CSIDL_PRINTHOOD"),					},
	{ CSIDL_PROFILE,					&FOLDERID_Profile,					_T ("CSIDL_PROFILE"),					},
	{ CSIDL_PROGRAM_FILES,				&FOLDERID_ProgramFiles,				_T ("CSIDL_PROGRAM_FILES"),				},
	{ CSIDL_PROGRAM_FILESX86,			&FOLDERID_ProgramFilesX86,			_T ("CSIDL_PROGRAM_FILESX86"),			},
	{ CSIDL_PROGRAM_FILES_COMMON,		&FOLDERID_ProgramFilesCommon,		_T ("CSIDL_PROGRAM_FILES_COMMON"),		},
	{ CSIDL_PROGRAM_FILES_COMMONX86,	&FOLDERID_ProgramFilesCommonX86,	_T ("CSIDL_PROGRAM_FILES_COMMONX86"),	},
	{ CSIDL_PROGRAMS,					&FOLDERID_Programs,					_T ("CSIDL_PROGRAMS"),					},
	{ CSIDL_RECENT,						&FOLDERID_Recent,					_T ("CSIDL_RECENT"),					},
	{ CSIDL_RESOURCES,					&FOLDERID_ResourceDir,				_T ("CSIDL_RESOURCES"),					},
	{ CSIDL_RESOURCES_LOCALIZED,		&FOLDERID_LocalizedResourcesDir,	_T ("CSIDL_RESOURCES_LOCALIZED"),		},
	{ CSIDL_SENDTO,						&FOLDERID_SendTo,					_T ("CSIDL_SENDTO"),					},
	{ CSIDL_STARTMENU,					&FOLDERID_StartMenu,				_T ("CSIDL_STARTMENU"),					},
	{ CSIDL_STARTUP,					&FOLDERID_Startup,					_T ("CSIDL_STARTUP"),					},
	{ CSIDL_SYSTEM,						&FOLDERID_System,					_T ("CSIDL_SYSTEM"),					},
	{ CSIDL_SYSTEMX86,					&FOLDERID_SystemX86,				_T ("CSIDL_SYSTEMX86"),					},
	{ CSIDL_TEMPLATES,					&FOLDERID_Templates,				_T ("CSIDL_TEMPLATES"),					},
	{ CSIDL_WINDOWS,					&FOLDERID_Windows,					_T ("CSIDL_WINDOWS"),					}, 
};

CString GetCSIDLName (const KNOWNFOLDERID & k)
{
	for (int i = 0; i < ARRAYSIZE (foldermap); i++)
		if ((* foldermap [i].m_prfid) == k)
			return foldermap [i].m_lpsz;

	return _T ("[not found]");
}

CString GetCSIDLName (int csidl)
{
	for (int i = 0; i < ARRAYSIZE (foldermap); i++)
		if (foldermap [i].m_csidl == csidl)
			return foldermap [i].m_lpsz;

	return _T ("[not found]");
}

int GetCSIDL (const KNOWNFOLDERID & k)
{
	for (int i = 0; i < ARRAYSIZE (foldermap); i++)
		if ((* foldermap [i].m_prfid) == k)
			return foldermap [i].m_csidl;

	return 0;
}

CString GetKnownFolderPath (const KNOWNFOLDERID & k)
{
	typedef HRESULT (WINAPI* lpSHGetKnownFolderPath)(REFKNOWNFOLDERID rfid,DWORD dwFlags,HANDLE hToken,PWSTR *ppszPath);

	CString strResult;

	{
		wchar_t * pwsz = 0;

		HMODULE hModule = LoadLibrary (_T ("shell32.dll"));
    
		if (lpSHGetKnownFolderPath pFct = (lpSHGetKnownFolderPath)GetProcAddress (hModule, "SHGetKnownFolderPath")) {
			HRESULT hResult = (* pFct) (k, 0, NULL, &pwsz);

			if (hResult != S_OK) {
				TRACEF (GetCSIDLName (k));
				TRACEF (_T ("SHGetKnownFolderPath: ") + ToString (hResult));
				TRACEF (FormatMessage (::GetLastError ()));
			}
		}

		strResult = pwsz;

		if (pwsz)
			::CoTaskMemFree (static_cast<void*>(pwsz));
	}

	if (strResult.IsEmpty ()) {
		TCHAR sz [MAX_PATH] = { 0 };
		int csidl = GetCSIDL (k);

		if (::SHGetSpecialFolderPath (NULL, sz, csidl, TRUE))
			strResult = sz;
	}

	if (strResult.IsEmpty ()) {
		if (k == FOLDERID_CommonStartup) 
			strResult = GetKnownFolderPath (FOLDERID_CommonPrograms) + _T ("\\Startup");
	}

	return strResult;
}


/*
bool Search (const CString & str, const CString & strRegExpr)
{
	CString strSearch (strRegExpr);
	CString strTmp (str);
	std::wregex e (strSearch.MakeLower ());
	std::wsmatch m;

	if (std::regex_search (std::wstring (strTmp.MakeLower ()), m, e))
		return true;

	return false;
}
*/

bool IsXP ()
{
    OSVERSIONINFOEX osver;

    ZeroMemory (&osver, sizeof(osver));
    osver.dwOSVersionInfoSize = sizeof(osver);
 
    GetVersionEx((OSVERSIONINFO *)&osver);

    return osver.dwMajorVersion <= 5 ? true : false;
}

/*
bool IsOSVerWindowsVista ()
{
    OSVERSIONINFOEX osver;

    ZeroMemory (&osver, sizeof(osver));
    osver.dwOSVersionInfoSize = sizeof(osver);
 
    if (!GetVersionEx((OSVERSIONINFO *)&osver))
        return false;
 
    // dwMajorVersion 6 and dwMinorVersion 0 means Vista or 2k8:
    if ( (osver.dwMajorVersion != 6) || (osver.dwMinorVersion != 0) )
        return false;
 
    // wProductType of NT_WORKSTATION means it's Vista:
    if (osver.wProductType != VER_NT_WORKSTATION)
        return false;
 
    return true;  
}
*/

/*
LANGID GetLanguage ()
{
	LANGID id = 0;

	if (!IsXP ()) {
		typedef LANGID (WINAPI *FP_GetThreadUILanguage)();

		HMODULE hKernel32 = GetModuleHandle (_T("Kernel32.dll"));
		FP_GetThreadUILanguage pFct = (FP_GetThreadUILanguage)GetProcAddress (hKernel32, "GetThreadUILanguage");

		if (pFct) 
			id = (* pFct) ();
	}
	else 
		id = MAKELCID (::GetThreadLocale (), SORT_DEFAULT);

	return id;
}


void SetLanguage (LANGID id)
{
	if (!IsXP ()) {
		typedef LANGID (WINAPI *FP_SetThreadUILanguage)(LANGID id);

		HMODULE hKernel32 = GetModuleHandle (_T("Kernel32.dll"));
		FP_SetThreadUILanguage pFct = (FP_SetThreadUILanguage)GetProcAddress (hKernel32, "SetThreadUILanguage");

		if (pFct)
			(* pFct) (id);
	}
	else 
		SetThreadLocale(MAKELCID (id, SORT_DEFAULT));
}
*/

CString GetTempPath ()
{
	TCHAR szTemp [MAX_PATH] = { 0 };
	::GetTempPath (ARRAYSIZE (szTemp), szTemp);
	::GetLongPathName (szTemp, szTemp, ARRAYSIZE (szTemp));
	szTemp [_tcslen (szTemp) - 1] = '\0';

	return szTemp;
}

bool WriteProfileString (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szData) 
{
	HKEY hKey;

	// if szSection created/opened okay
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = _tcslen (szData) + 1;

	
	#ifdef _UNICODE
		dwSize *= 2;
		#endif

		LONG lStatus = ::RegSetValueEx (hKey, szKey, 0, REG_SZ, 
			(const BYTE *) szData, dwSize);
		::RegCloseKey (hKey);

		return lStatus == ERROR_SUCCESS;
	}

	return false;
}

CString GetProfileString (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szDefault) 
{
	HKEY hKey;
	CString strResult (szDefault);

	// if szSection was in the registry
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwMaxValueLen = 256, dwType = REG_SZ;

		if (::RegQueryInfoKey (hKey, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, &dwMaxValueLen, NULL, NULL) != ERROR_SUCCESS)
		{
			dwMaxValueLen = 256;
		}

		TCHAR * pszData = new TCHAR [dwMaxValueLen + 1];

		if (::RegQueryValueEx (hKey, szKey, NULL, &dwType, 
			(LPBYTE) pszData, &dwMaxValueLen) == ERROR_SUCCESS) 
		{
			strResult = pszData;
		}

		delete [] pszData;
		::RegCloseKey (hKey);
	}

	return strResult;
}

CString Tokenize(const CString &strInput, CStringArray &v, TCHAR cDelim, bool bTrim)
{
	#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
	TRACEF ("Tokenize (" + strInput + ")");
	#endif //_DEBUG && __TRACE_TOFROMSTRING__
	
	CString str = strInput;

	if (bTrim) {
		str.TrimLeft ();
		str.TrimRight ();
	}

	CLongArray commas = CountChars (str, cDelim);
	int nIndex = 0;

	v.RemoveAll ();

	for (int i = 0; i < commas.GetSize (); i++) {
		int nLen = commas [i] - nIndex;
		v.Add (str.Mid (nIndex, nLen));
		nIndex = commas [i] + 1;
	}

	CString strRemainder = str.Mid (nIndex);

	if (strRemainder.GetLength ())
		v.Add (strRemainder);
	else {
		if (strInput.GetLength () && strInput [strInput.GetLength () - 1] == cDelim)
			v.Add (_T (""));
	}

	#if defined( _DEBUG ) && defined( __SHOW_TOKENS__ )
	{
		CString strDebug;

		for (int i = 0; i < v.GetSize (); i++) {
			strDebug.Format ("v [%u] = \"%s\"%s", 
				i, v [i], (i + 1 == v.GetSize ()) ? "\n" : "");
			TRACEF (strDebug);
		}
	}
	#endif //_DEBUG && __SHOW_TOKENS__

	return v.GetSize () ? v [0] : _T ("");
}

CLongArray CountChars (const CString & str, TCHAR c)
{
	CLongArray index;

	for (int i = 0; i < str.GetLength (); i++) {
		TCHAR cCur = str [i];

		if (cCur == '\\') {
			i++;
			continue;
		}
		else if (cCur == c) 
			index.Add (i);
	}
/*
	TCHAR cCur, cPrev = '\0';

	for (int i = 0; i < str.GetLength (); i++) {
		cCur = str [i];

		if (cCur == '\\') {
			i++;
			continue;
		}
		else if (cCur == c) 
			index.Add (i);

		cPrev = cCur;
	}
*/

	return index;
}

CString FormatString (const CString &str, bool bFilename)
{
	CString strResult (str);
	TCHAR cDelim [] = { '\\', ',', '{', '}', 0 }; // the '\' char must be first

	if (!bFilename) {
		strResult.Replace (_T ("\\u"), _T ("<u>"));
		strResult.Replace (_T ("\\U"), _T ("<U>"));
		strResult.Replace (_T ("\r\n"), _T ("<CR>"));
	}

	for (int i = 0; cDelim [i] != 0; i++) {
		int nIndex = 0;
	
		do {
			nIndex = strResult.Find (cDelim [i], nIndex);

			if (nIndex != -1) {
				strResult.Insert (nIndex, '\\');
				nIndex += 2;
			}
		}
		while (nIndex != -1);
	}


	if (!bFilename) {
		for (int i = strResult.GetLength () - 1; i >= 0; i--) {
			TCHAR c = strResult [i];

			if (c > 255) {
				CString strEncode;
				//int a = HIBYTE (c);
				//int b = LOBYTE (c);

				strEncode.Format (_T ("\\u%04X"), c);
				//strEncode.Format (_T ("\\C%c%c"), a, b);

				strResult.Delete (i);
				strResult.Insert (i, strEncode);
			}
		}
	}

	//TRACEF (str + " --> " + strResult);

	return strResult;
}

CString UnformatString(const CString &strSrc)
{
	CString strResult, str (strSrc);

	for (int i = 0; i < str.GetLength (); i++) {
		if (str [i] == '\\') {
			i++;

			if (i >= str.GetLength ())
				break;

			if ((i + 4) < str.GetLength ()) {
				if (str [i] == 'u' || str [i] == 'U') {
					CString strEncode = str.Mid (i + 1, 4);
					TCHAR cEncode = (TCHAR)_tcstoul (strEncode, NULL, 16);
					strResult += cEncode;
					i += 4;
					continue;
				}
			}

			if ((i + 2) < str.GetLength ()) {
				if (str [i] == 'c' || str [i] == 'C') {
					CString strEncode = str.Mid (i + 1, 2);
					int a = strEncode [0];
					int b = strEncode [1];
					TCHAR cEncode = MAKEWORD (b, a);

					strResult += cEncode;
					i += 2;
					continue;
				}
			}
		}

		strResult += str [i];
	}

	strResult.Replace (_T ("<CR>"), _T ("\r\n"));
	strResult.Replace (_T ("<u>"), _T ("\\u"));
	strResult.Replace (_T ("<U>"), _T ("\\U"));

	return strResult;
}

CString ToString (int n)
{
	CString str;
	str.Format (_T ("%d"), n);
	return str;
}

CString ToString (HRESULT hResult)
{
	CString str;
	str.Format (_T ("0x%p [%d]"), hResult, hResult);
	return str;
}

CString ToString (const CStringArray & v) 
{
	CString str;

	for (int i = 0; i < v.GetSize (); i++) {
		str += FormatString (v [i]);// + _T (",");

		if ((i + 1) < v.GetSize ())
			str += _T (",");
	}

	return _T ("{") + str + _T ("}");
}

CString ToString (VERSIONSTRUCT & v)
{
	CString str;

	if (v.m_nCustomMajor || v.m_nCustomMinor) 
		str.Format (_T ("%d.%02d.%04d.%03d"), v.m_nMajor, v.m_nMinor, v.m_nCustomMajor, v.m_nInternal);
	else {
		if (v.m_nInternal)
			str.Format (_T ("%d.%02d [%d]"), v.m_nMajor, v.m_nMinor, v.m_nInternal);
		else
			str.Format (_T ("%d.%02d"), v.m_nMajor, v.m_nMinor);
	}

	return str;
}

int FromString (const CString & strInput, CStringArray & v)
{
	const int nCount = v.GetSize ();
	CString str (strInput);

	str.TrimLeft ();
	str.TrimRight ();

	Tokenize (str, v);

	for (int i = nCount; i < v.GetSize (); i++) 
		v [i] = UnformatString (v [i]);

	return v.GetSize () - nCount;
}

bool IsNetworkedFilename ()
{
	TCHAR sz [MAX_PATH] = { 0 };

	::GetModuleFileName (NULL, sz, ARRAYSIZE (sz) - 1);
	::GetFileTitle (sz, sz, ARRAYSIZE (sz));
	TRACEF (sz);

	CString str = sz;
	str.MakeLower ();
	bool bResult = str.Find (_T ("network")) != -1 ? true : false;
	
	#ifdef _DEBUG
	bResult = true;
	#endif

	return bResult;
}

static const GUID guidNull	= { 0, 0, 0, { 0, 0, 0, 0, 0, 0, 0, 0 } };
static GUID guidDriver		= ::guidNull;

bool IsDriverInstalled (const GUID & guid)
{
	if (::guidDriver != ::guidNull)
		return guid == ::guidDriver ? true : false;

	bool bInstalled = false;
	GUID g = guid;
	HDEVINFO info = SetupDiGetClassDevs (&g, NULL, NULL, DIGCF_PRESENT | DIGCF_INTERFACEDEVICE);

	if (info != INVALID_HANDLE_VALUE) {
		SP_DEVICE_INTERFACE_DATA ifdata;

		ifdata.cbSize = sizeof(ifdata);

		for (DWORD devindex = 0; ::SetupDiEnumDeviceInterfaces (info, NULL, &g, devindex, &ifdata); ++devindex) {
			DWORD needed  = 0;
	
			::SetupDiGetDeviceInterfaceDetail (info, &ifdata, NULL, 0, &needed, NULL);

			PSP_INTERFACE_DEVICE_DETAIL_DATA detail = (PSP_INTERFACE_DEVICE_DETAIL_DATA) malloc(needed);
			SP_DEVINFO_DATA did = {sizeof(SP_DEVINFO_DATA)};
			detail->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);

			if (::SetupDiGetDeviceInterfaceDetail (info, &ifdata, detail, needed, NULL, &did)) 
				bInstalled = true;

			free ((PVOID)detail);

			if (bInstalled)
				break;

		}
	}
	
   ::SetupDiDestroyDeviceInfoList (info);
   
   return bInstalled;
}

static const LPCTSTR lpszODBC = _T ("SOFTWARE\\ODBC\\ODBC.INI");
//struct
//{
//	LPCTSTR m_lpsz;
//	HKEY m_hkey;
//}
//static const keys [] = 
//{
//	{ _T ("HKEY_CURRENT_USER"),		HKEY_CURRENT_USER,	},
//	{ _T ("HKEY_LOCAL_MACHINE"),	HKEY_LOCAL_MACHINE,	},
//};

void GetDsnEntries (CStringArray & v, HKEY hKeyRoot)
{
	//for (int nKey = 0; nKey < ARRAYSIZE (::keys); nKey++) 
	{
		DWORD dwSubKeyLength = _MAX_FNAME;
		DWORD dwSubKeyIndex = 0;
		TCHAR szSubKey [_MAX_FNAME] = { 0 };
		HKEY hKey = NULL;

		//LONG lRegOpenKey = ::RegOpenKey (::keys [nKey].m_hkey, ::lpszODBC, &hKey);
		LONG lRegOpenKey = ::RegOpenKey (hKeyRoot, ::lpszODBC, &hKey);

		if (lRegOpenKey == ERROR_SUCCESS) {
			LONG lRegEnumKeyEx = 0;

			while (lRegEnumKeyEx != ERROR_NO_MORE_ITEMS) { 
				lRegEnumKeyEx = ::RegEnumKeyEx (hKey, dwSubKeyIndex, szSubKey, &dwSubKeyLength, 
					NULL, NULL, NULL, NULL);

				if (lRegEnumKeyEx == ERROR_SUCCESS) {
					const CString strSection = (::lpszODBC + CString (_T ("\\")) + szSubKey);
					CString strDriver = GetProfileString (hKeyRoot, //::keys [nKey].m_hkey, 
						strSection, _T ("Driver"), _T (""));

					dwSubKeyIndex++;
					dwSubKeyLength = _MAX_FNAME;

					if (strDriver.GetLength ())
						InsertAscending (v, szSubKey);
				}
			}

			::RegCloseKey (hKey);
		}
	}
}

bool IsPysicalKeyboardPresent ()
{
	int nResult = 0;

	SP_DEVINFO_DATA data;
	GUID guid = GUID_DEVCLASS_KEYBOARD;
	HDEVINFO hDevInfo = ::SetupDiGetClassDevs (&guid, 0, 0, DIGCF_PRESENT);
	//CString strResult;

	if (hDevInfo != INVALID_HANDLE_VALUE) {

		data.cbSize = sizeof(SP_DEVINFO_DATA);

		for (DWORD i = 0; ::SetupDiEnumDeviceInfo (hDevInfo,i, &data); i++) {
			DWORD dw = 0;
			TCHAR sz [256] = { 0 };
			DWORD dwLen = ARRAYSIZE (sz);
			DWORD dwProp [] =
			{
				SPDRP_DEVICEDESC,
				SPDRP_HARDWAREID,
				SPDRP_COMPATIBLEIDS,
				//SPDRP_NTDEVICEPATHS,
				SPDRP_SERVICE,
				//SPDRP_CONFIGURATION,
				//SPDRP_CONFIGURATIONVECTOR,
				SPDRP_CLASS,
				SPDRP_CLASSGUID,
				SPDRP_DRIVER,
				SPDRP_CONFIGFLAGS,
				SPDRP_MFG,
				SPDRP_FRIENDLYNAME,
			};
			CString str;

			for (int j = 0; j < ARRAYSIZE (dwProp); j++) {
				memset (sz, 0, ARRAYSIZE (sz));
				dwLen = ARRAYSIZE (sz);
				::SetupDiGetDeviceRegistryProperty (hDevInfo, &data, dwProp [j], &dw, (PBYTE)sz, dwLen, &dwLen);
				str += CString (sz) + _T (", ");
			}

			::OutputDebugString (str + _T ("\n"));

			if (str.Find (_T ("Standard PS/2 Keyboard, ACPI\\PNP0303, *PNP030B, i8042prt")) != -1)
				continue;

			if (data.ClassGuid == GUID_DEVCLASS_KEYBOARD) {
				TRACEF (_T ("keyboard: ") + str);
				nResult++;
				//CString strTmp; strTmp.Format (_T ("[%d]: %s\n"), nResult, str); strResult += strTmp;
			}
			else {
				CString strTmp = str;

				strTmp.MakeLower ();

				if (strTmp.Find (_T ("keyboard")) != -1) 
					TRACEF (_T ("          ") + CString (str));
			}
		}       
             
		::SetupDiDestroyDeviceInfoList (hDevInfo);
	}

	//MessageBox (NULL, strResult, 0, 0);

	return nResult > 0 ? true : false;
}

template <class TYPE, class ARG_TYPE>
CCopyArray <TYPE, ARG_TYPE> MergeAcending (CArray <TYPE, ARG_TYPE> & v1, CArray <TYPE, ARG_TYPE> & v2)
{
	CCopyArray <TYPE, ARG_TYPE> vResult;

	for (int i = 0; i < v1.GetSize (); i++) 
		InsertAscending (vResult, v1 [i]);

	for (int i = 0; i < v2.GetSize (); i++) 
		InsertAscending (vResult, v2 [i]);

	return vResult;
};

void ParsePackets (const CString & str, CStringArray & v)
{
	CLongArray vLeft = CountChars (str, '{');
	CLongArray vRight = CountChars (str, '}');
	CLongArray vDelim = MergeAcending (vLeft, vRight);
	int nCount = 0;

	for (int i = 0; i < vDelim.GetSize (); i++) {
		int nIndex = vDelim [i];

		if (str [nIndex] == '{')
			nCount++;
		else if (str [nIndex] == '}')
			nCount--;

		if (nCount == 0) {
			int nFirst = vLeft [0] + 1;
			int nLast = nIndex;
			int nLen = nLast - nFirst;
			CString strSegment = str.Mid (nFirst, nLen);

			v.Add (strSegment);

			if (nLast < str.GetLength () - 1) {
				CString strRemainder = str.Mid (nLast + 1);
				
				ParsePackets (strRemainder, v);
			}

			return;
		}
	}
}

CString GetPath (const CString & strFile)
{
	CString str;
	int nIndex = strFile.ReverseFind ('\\');

	if (nIndex != -1)
		str = strFile.Left (nIndex);

	return str;
}


typedef struct
{
	int m_nCount;
	CString m_strPath;
} RESULTSTRUCT;

void LocateLogosFolder (CDatabase & db, CStringArray & vResult) 
{
	CMapStringToString vPath;
	CArray <RESULTSTRUCT, RESULTSTRUCT &> v;

	if (db.IsOpen ()) {
		try 
		{
			CRecordset rst (&db);
			CString strSQL;

			strSQL.Format (_T ("SELECT [Data] FROM [Messages]"));

			if (rst.Open (CRecordset::snapshot, strSQL, CRecordset::readOnly)) {
				
				while (!rst.IsEOF ()) {
					CString strData;
					CStringArray vParams;
					
					rst.GetFieldValue ((short)0, strData);
					//TRACEF (strData);
					ParsePackets (strData, vParams);

					for (int nParam = 0; nParam < vParams.GetSize (); nParam++) {
						CString strParam = _T ("{") +  vParams [nParam] + _T ("}");
						const int nIndex = 9;
						const CString strBMP = _T ("{Bitmap,");
						CLongArray v = CountChars (strParam, ',');

						if (strParam.Find (strBMP) != -1) {
							CStringArray v;

							Tokenize (strParam, v);

							CString strFile = UnformatString (v [nIndex]);
							CString strPath = GetPath (strFile);

							{
								CString str;
								vPath.Lookup (strPath, str);
								str.Format (_T ("%d"), _ttoi (str) + 1);
								vPath.SetAt (strPath, str);
							}
						}
					}
				
					rst.MoveNext ();
				}
			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	for (POSITION pos = vPath.GetStartPosition (); pos; ) {
		CString strPath, strCount;
		bool bFound = false;
		RESULTSTRUCT s;

		vPath.GetNextAssoc (pos, strPath, strCount);
		s.m_nCount = _ttoi (strCount);
		s.m_strPath = strPath;

		for (int i = 0; !bFound && i < v.GetSize (); i++) {
			if (v [i].m_nCount < s.m_nCount) {
				v.InsertAt (i, s);
				bFound = true;
			}
		}

		if (!bFound)
			v.Add (s);
	}
	
	vResult.RemoveAll ();

	for (int i = 0; i < v.GetSize (); i++) {
		RESULTSTRUCT & s = v [i];

		/*
		#ifdef _DEBUG
		CString str;
		str.Format (_T ("%d: %s"), s.m_nCount, s.m_strPath);
		TRACEF (str);
		#endif
		*/

		//vResult.Add (s.m_strPath);
		if (Find (vResult, s.m_strPath) == -1)
			InsertAscending (vResult, s.m_strPath);
	}
}

int FindNoCase (const CString & str, const CString & strFind)
{
	CString strLower (str), strFindLower (strFind);

	strLower.MakeLower ();
	strFindLower.MakeLower ();

	return strLower.Find (strFindLower);
}

int ReplaceNoCase (CString & str, const CString & strOld, const CString & strNew)
{
	int nIndex, nResult = 0;

	if (strOld.CompareNoCase (strNew) != 0) {
		while ((nIndex = FindNoCase (str, strOld)) != -1) {
			str.Delete (nIndex, strOld.GetLength ());
			str.Insert (nIndex, strNew);
			nResult++;
		}
	}

	return nResult;
}

bool OpenDatabase (CDatabase & db, const CString & strDSN)
{
	try 
	{
		if (::GetFileAttributes (strDSN) != -1) {
			int nIndex = strDSN.ReverseFind ('\\');

			if (nIndex != -1) {
				const CString strTmpDSN = _T ("FoxjetInstaller_TmpDSN");
				CString strDir = strDSN.Left (nIndex);
				CString strFilename = strDSN.Mid (nIndex + 1);

				CreateDSN (strTmpDSN, strDir, strFilename);
				db.OpenEx (_T ("DSN=") + strTmpDSN, CDatabase::noOdbcDialog);
				::SQLConfigDataSource (NULL, ODBC_REMOVE_SYS_DSN, /* w2a */ (lpszDBDriver), /* CAnsiString */ (_T ("DSN=") + strTmpDSN));
			}
		}
		else
			db.OpenEx (_T ("DSN=") + strDSN, CDatabase::noOdbcDialog);
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); return false; }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); return false; }

	return db.IsOpen () ? true : false;
}

CString CreateDSN (CString strDSN, CString strDir, CString strFilename)
{
	LPCTSTR lpszFormat = 
		_T ("DSN=%s~ ")					// dsn
		_T ("DBQ=%s%s~ ")				// file
		_T ("Description=%s~ ")			// dsn
		_T ("FileType=MicrosoftAccess~ ")
		_T ("DataDirectory=%s~ ")
		_T ("MaxScanRows=20~ ")
		_T ("DefaultDir=%s~ ")
		_T ("FIL=MS Access;~ ")
		_T ("MaxBufferSize=2048~ ")
		_T ("MaxScanRows=8~ ")
		_T ("PageTimeout=5~ ")
		_T ("Threads=3~ ");
	TCHAR sz [512];
	TCHAR szAttr [512];

	if (strDir.GetLength () && strDir [strDir.GetLength () - 1] != '\\')
		strDir += '\\';

	_stprintf (sz, lpszFormat, 
		strDSN, 
		strDir, strFilename,
		strDSN,
		strDir, 
		strDir);
	int nLen = _tcsclen (sz);

	_tcscpy (szAttr, sz);
	TRACEF (szAttr);

	CString strResult = szAttr;
						
	for (int i = 0; i < nLen; i++)
		if (szAttr [i] == '~')
			szAttr [i] = '\0';

	::SQLConfigDataSource (NULL, ODBC_REMOVE_SYS_DSN, /* w2a */ (lpszDBDriver), /* CAnsiString */ (_T ("DSN=") + strDSN));
	VERIFY (::SQLConfigDataSource (NULL, ODBC_ADD_SYS_DSN, /* w2a */ (lpszDBDriver), /* w2a */ (szAttr)));

	return strResult;
}

CString FormatDir (const CString & strDir)
{
	if (strDir.GetLength () && strDir [strDir.GetLength () - 1] != '\\')
		return strDir + '\\';

	return strDir;
}

CString GetComputerName ()
{ 
	TCHAR sz [128] = { 0 };
	DWORD dwLen = ARRAYSIZE (sz);

	::GetComputerName (sz, &dwLen);
	return sz;
}

CString GetIpAddress () 
{ 
	WSADATA wsaData;
	char szName [255] = { 0 };
	PHOSTENT hostinfo;
	WORD wVersionRequested = MAKEWORD (1, 1);

	if (::WSAStartup (wVersionRequested, &wsaData) == 0) {
		if (::gethostname (szName, sizeof (szName)) == 0) {
			//TRACEF (szName);

			if ((hostinfo = ::gethostbyname (szName)) != NULL) {
				if (hostinfo->h_addr_list [0]) {
					CString str = /*a2w*/ (inet_ntoa (*(struct in_addr *)hostinfo->h_addr_list [0]));
					
					//TRACEF (str);
					return str;
				}
			}
		}
	}

	return _T ("");
}


CUseInfo2::CUseInfo2 ()
{
}

CUseInfo2::CUseInfo2 (USE_INFO_2 * p)
:	m_strLocal		(p->ui2_local),
	m_strRemote		(p->ui2_remote),
	m_strPassword	(p->ui2_password),
	m_dwStatus		(p->ui2_status),
	m_dwAsg_type	(p->ui2_asg_type),
	m_dwRefcount	(p->ui2_refcount),
	m_dwUsecount	(p->ui2_usecount),
	m_strUsername	(p->ui2_username),
	m_strDomainname (p->ui2_domainname)
{
}


void EnumMappedDrives (CArray <CUseInfo2, CUseInfo2 &> & v)
{
   USE_INFO_2 * BufPtr = NULL, * pth = NULL;
   NET_API_STATUS  res;
   LPTSTR lpszServer = NULL;
   DWORD er = 0, tr = 0, resume = 0;

   do {

      res = NetUseEnum (lpszServer, 2, (LPBYTE *)&BufPtr, -1, &er, &tr, &resume);

      // If the call succeeds,

      if (res == ERROR_SUCCESS || res == ERROR_MORE_DATA) {

         pth = BufPtr;

         for(int i = 1; i <= (int)er; i++) {
			 CUseInfo2 s (pth);
			 v.Add (s);
			 pth++;
         }
      }
      else {
		  CString str;

		  str.Format (_T ("NetUseEnum failed: %d"), res);
		  TRACEF (str);
	  }

   }
   while (res == ERROR_MORE_DATA); 

   NetApiBufferFree (BufPtr);
}

DWORD GetFileSize (const CString & strFile)
{
	DWORD dw = 0;

    if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
		fseek (fp, 0, SEEK_END);
		dw = ftell (fp);
		fclose (fp);
	}

	return dw;
}

CTime GetFileDate (const CString & strPath)
{
	WIN32_FIND_DATA wfd;
	HANDLE hFile = ::FindFirstFile (strPath, &wfd);

	if (hFile != INVALID_HANDLE_VALUE) {
		::FindClose (hFile);
		return CTime (wfd.ftLastWriteTime);
	}

	return CTime ();
}

#define CREATEMAPENTRY(nType, lpszType) { nType, lpszType, }

struct
{
	ELEMENTTYPE			m_type;
	LPCTSTR				m_lpszType;
} static const elements [] = 
{
	CREATEMAPENTRY (TEXT,				_T ("Text")			),
	CREATEMAPENTRY (BMP,				_T ("Bitmap")		),
	CREATEMAPENTRY (COUNT,				_T ("Count")		),
	CREATEMAPENTRY (DATETIME,			_T ("DateTime")		),
	CREATEMAPENTRY (EXPDATE,			_T ("ExpDate")		),
	CREATEMAPENTRY (USER,				_T ("User")			),
	CREATEMAPENTRY (SHIFTCODE,			_T ("Shift")		),
	CREATEMAPENTRY (BARCODE,			_T ("Barcode")		),
	CREATEMAPENTRY (DATABASE,			_T ("Database")		),
	CREATEMAPENTRY (SERIAL,				_T ("Serial")		),
	CREATEMAPENTRY (LABEL,				_T ("Label")		),
	CREATEMAPENTRY (SHAPE,				_T ("Shape")		),
};

int GetElementType (const CString & strPrefix)
{
	for (int i = 0; i < ARRAYSIZE (elements); i++) 
		if (!strPrefix.CompareNoCase (elements [i].m_lpszType)) 
			return elements [i].m_type;

	return -1;
}

static ULONG CALLBACK GetFileAttributesTimeoutFunc (LPVOID lpData)
{
	DWORD dwResult = -1;

	if (CString * p = (CString *)lpData) {
		dwResult = ::GetFileAttributes (* p);
		delete p;
	}

	return dwResult;
}

DWORD GetFileAttributesTimeout (LPCTSTR lpszFile, DWORD dwTimeout)
{
	DWORD dwThreadID = 0;
	HANDLE h = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)GetFileAttributesTimeoutFunc, (LPVOID)new CString (lpszFile), 0, &dwThreadID);

	DWORD dw = ::WaitForSingleObject (h, dwTimeout);

	if (dw == WAIT_OBJECT_0) {
		DWORD dwResult = -1;

		if (::GetExitCodeThread (h, &dwResult))
			return dwResult;
	}

	return -1;
}