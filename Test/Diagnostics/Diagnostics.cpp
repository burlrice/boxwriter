// Diagnostics.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Diagnostics.h"
#include "Shlwapi.h"
#include "AclInfo.h"
#include <afxsock.h>
#include <vector>
#include <string>

#define SECURITY_WIN32

#include <Security.h>

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// The one and only application object

CWinApp theApp;

CString GetNetworkPath (CArray <CUseInfo2, CUseInfo2 &> & v, const CString & strLocal)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (!v [i].m_strLocal.CompareNoCase (strLocal))
			return v [i].m_strRemote;

	return _T ("");
}

CString GetFileAttributes (DWORD dw)
{
	#define CREATEMAPENTRY(s) { (s), _T (#s) }

	struct {
		DWORD m_dw;
		LPCTSTR m_lpsz;
	} static const map [] = 
	{
		CREATEMAPENTRY (FILE_ATTRIBUTE_ARCHIVE),
		CREATEMAPENTRY (FILE_ATTRIBUTE_COMPRESSED),
		//CREATEMAPENTRY (FILE_ATTRIBUTE_DEVICE),
		CREATEMAPENTRY (FILE_ATTRIBUTE_DIRECTORY),
		CREATEMAPENTRY (FILE_ATTRIBUTE_ENCRYPTED),
		CREATEMAPENTRY (FILE_ATTRIBUTE_HIDDEN),
		CREATEMAPENTRY (FILE_ATTRIBUTE_NORMAL),
		CREATEMAPENTRY (FILE_ATTRIBUTE_NOT_CONTENT_INDEXED),
		CREATEMAPENTRY (FILE_ATTRIBUTE_OFFLINE),
		CREATEMAPENTRY (FILE_ATTRIBUTE_READONLY),
		CREATEMAPENTRY (FILE_ATTRIBUTE_REPARSE_POINT),
		CREATEMAPENTRY (FILE_ATTRIBUTE_SPARSE_FILE),
		CREATEMAPENTRY (FILE_ATTRIBUTE_SYSTEM),
		CREATEMAPENTRY (FILE_ATTRIBUTE_TEMPORARY),
	};
	CString str;

	if (dw != -1) {
		for (int i = 0; i < ARRAYSIZE (map); i++) {
			if ((map [i].m_dw & dw) == dw)
				str += CString (map [i].m_lpsz) + _T (" ");
		}
	}
	else
		str = _T ("not found");

	str.TrimRight ();

	return str;
}

DWORD GetFileProperties (const CString & strFile)
{
	TRACEF (_T ("GetFileAttributes: ") + strFile);

	CString str, strError;
	DWORD dw = ::GetFileAttributes (strFile);

	if (dw == -1)
		strError = FormatMessage (::GetLastError ());

	str.Format (
		_T ("%s\n")
		_T ("%sattributes: %d [0x%p] [%s]\n")
		_T ("%ssize:       %d bytes\n")
		_T ("%sdate:       %s\n"),
		strFile, 
		CString (' ', COLUMN_ALIGNMENT), dw, dw, GetFileAttributes (dw),
		CString (' ', COLUMN_ALIGNMENT), GetFileSize (strFile),
		CString (' ', COLUMN_ALIGNMENT), GetFileDate (strFile).Format (_T ("%c")));
	TRACEF (str);

	if (dw == -1)
		TRACEF (strError);

	return dw;
}

void LocateDatabaseElements (CDatabase & db, CStringArray & vDSN)
{
	CRecordset rst (&db);
	CString strSQL = _T ("SELECT [Data] FROM Messages;");
	int nFound = 1;

	TRACEF (strSQL);

	if (rst.Open (CRecordset::snapshot, strSQL, CRecordset::readOnly)) {
		CString strData;

		while (!rst.IsEOF ()) {
			CStringArray v;

			rst.GetFieldValue ((short)0, strData);
			ParsePackets (strData, v);

			for (int i = 0; i < v.GetSize (); i++) {
				CStringArray vPacket;
				CString & strSeg = v [i];

				Tokenize (strSeg, vPacket);

				if (vPacket.GetSize ()) { 
					CString & strToken = vPacket [0];
					int nType = GetElementType (strToken);

					if (nType == DATABASE) {
						const int nIndex = 11;

						TRACEF (CString (' ', COLUMN_ALIGNMENT) + _T ("[") + ToString (nFound++) + _T ("] ") + strSeg);

						if (vPacket.GetSize () >= nIndex) {
							CString strDSN = vPacket [nIndex];

							if (Find (vDSN, strDSN) == -1)
								vDSN.Add (strDSN);
						}
					}
					else if (nType == BARCODE) {
						CStringArray vSub;

						ParsePackets (strSeg, vSub);

						for (int nSub = 0; nSub < vSub.GetSize (); nSub++) {
							CStringArray vElement;

							Tokenize (vSub [nSub], vElement);

							if (vElement.GetSize ()) {
								const CString strPrefix = _T ("DatabaseObj");

								if (vElement [0].Find (strPrefix) != -1) {
									CString str = vElement [0];

									vElement [0] = strPrefix;
									str.Replace (strPrefix, _T (""));

									if (str.GetLength ())
										vElement.InsertAt (1, str);
								}

								if (!vElement [0].CompareNoCase (_T ("DatabaseObj"))) {
									const int nIndex = 4;

									if (vElement.GetSize () >= nIndex) {
										CString strDSN = vElement [nIndex];

										TRACEF (CString (' ', COLUMN_ALIGNMENT) + _T ("[") + ToString (nFound++) + _T ("] ") + strSeg);

										if (Find (vDSN, strDSN) == -1)
											vDSN.Add (strDSN);
									}
								}
							}
						}
					}
				}
			}

			rst.MoveNext ();
		}
	}
}

bool CheckDbConnection (CDatabase & db, const CString & strDSN, CArray <CUseInfo2, CUseInfo2 &> & vMappedDrives)
{
	bool bResult = false;
	CString strDBQ = GetProfileString (HKEY_LOCAL_MACHINE, _T ("SOFTWARE\\ODBC\\ODBC.INI\\") + strDSN, _T ("DBQ"), _T (""));
	CString strDBQ2;
	DWORD dwFileProperties1 = -1;
	DWORD dwFileProperties2 = -1;

	{
		int nIndex = strDBQ.Find ('\\');

		if (nIndex != -1) {
			CString strDrive = strDBQ.Left (nIndex);
			CString strRemote = GetNetworkPath (vMappedDrives, strDrive);
			
			if (strRemote.GetLength ()) {
				strDBQ2 = strDBQ;
				strDBQ2.Replace (strDrive, strRemote);
			}
		}
	}

	TRACEF (_T ("checking connection to: DSN=\"") + strDSN + _T ("\", DBQ=\"") + strDBQ + _T ("\""));

	if (strDBQ.GetLength ())
		dwFileProperties1 = GetFileProperties (strDBQ);

	if (strDBQ2.GetLength ()) {
		dwFileProperties2 = GetFileProperties (strDBQ2);

		if (::PathIsUNC (strDBQ2)) {
			CString str = Extract (strDBQ2, _T ("\\\\"), _T ("\\"));
			
			if (struct hostent * hp = ::gethostbyname (w2a (str))) {
				struct sockaddr_in dest;
				memcpy(&(dest.sin_addr),hp->h_addr,hp->h_length);
				CString strAddr = a2w (inet_ntoa (dest.sin_addr));
				TRACEF (strDBQ2 + _T (" --> ") + strAddr);
				TRACEF (_T ("Ping: ") + strAddr); 
				Ping (strAddr, 4, NULL);
			}
		}
	}

	if (dwFileProperties1 == -1 && dwFileProperties2 == -1) {
		TRACEF (_T ("**** ERROR: could not connect to: ") + strDSN);
	}
	else {
		try 
		{
			CString strKey = _T ("Software\\Foxjet\\MarksmanELITE\\ODBC\\") + strDSN;
			CString str = strDSN;
			CString strUID = GetProfileString (HKEY_CURRENT_USER, strKey, _T ("UID"), _T ("")); 
			CString strPWD = GetProfileString (HKEY_CURRENT_USER, strKey, _T ("PWD"), _T ("")); 

			if (strUID.GetLength () && strPWD.GetLength ())
				str += _T (";UID=") + strUID + _T (";PWD=") + strPWD + _T (";");

			str = _T ("DSN=") + str;
			TRACEF (_T ("Testing connection: ") + str);
			db.OpenEx (str, CDatabase::noOdbcDialog);
			TRACEF (_T ("connected: ") + db.GetConnect ());
			bResult = true;
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); TRACEF (_T ("***** FAILED *****")); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	return bResult;
}

void ExtractComponents ()
{
	HMODULE hModule = ::GetModuleHandle(NULL);
	const CString strFilepath = GetHomeDir () + _T ("\\fj_GetDllVersion.exe");
	
	if (HRSRC hRSRC = ::FindResource (hModule, MAKEINTRESOURCE (IDR_DLLGETVERSION), _T ("EXE"))) {
		DWORD dwSize = ::SizeofResource (hModule, hRSRC);

		if (HGLOBAL hGlobal = ::LoadResource (hModule, hRSRC)) {
			if (LPVOID lpData = ::LockResource (hGlobal)) {
				if (FILE * fp = _tfopen (strFilepath, _T ("wb"))) {
					fwrite (lpData, dwSize, 1, fp);
					fclose (fp);
				}
			}
		}
	}
}

CString GetVolumeInformation_SystemFlags (DWORD dw)
{
	struct {
		DWORD m_dw;
		LPCTSTR m_lpsz;
	} static const map [] = 
	{
		CREATEMAPENTRY (FILE_CASE_PRESERVED_NAMES),
		CREATEMAPENTRY (FILE_CASE_SENSITIVE_SEARCH),
		//CREATEMAPENTRY (FILE_DAX_VOLUME),
		CREATEMAPENTRY (FILE_FILE_COMPRESSION),
		CREATEMAPENTRY (FILE_NAMED_STREAMS),
		CREATEMAPENTRY (FILE_PERSISTENT_ACLS),
		CREATEMAPENTRY (FILE_READ_ONLY_VOLUME),
		CREATEMAPENTRY (FILE_SEQUENTIAL_WRITE_ONCE),
		CREATEMAPENTRY (FILE_SUPPORTS_ENCRYPTION),
		CREATEMAPENTRY (FILE_SUPPORTS_EXTENDED_ATTRIBUTES),
		CREATEMAPENTRY (FILE_SUPPORTS_HARD_LINKS),
		CREATEMAPENTRY (FILE_SUPPORTS_OBJECT_IDS),
		CREATEMAPENTRY (FILE_SUPPORTS_OPEN_BY_FILE_ID),
		CREATEMAPENTRY (FILE_SUPPORTS_REPARSE_POINTS),
		CREATEMAPENTRY (FILE_SUPPORTS_SPARSE_FILES),
		CREATEMAPENTRY (FILE_SUPPORTS_TRANSACTIONS),
		CREATEMAPENTRY (FILE_SUPPORTS_USN_JOURNAL),
		CREATEMAPENTRY (FILE_UNICODE_ON_DISK),
		CREATEMAPENTRY (FILE_VOLUME_IS_COMPRESSED),
		CREATEMAPENTRY (FILE_VOLUME_QUOTAS),
	};
	CString str;

	str.Format (_T ("[0x%p] "), dw);

	for (int i = 0; i < ARRAYSIZE (map); i++)
		if (dw & map [i].m_dw)
			str += map [i].m_lpsz + CString (' ');

	return str;
}


int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	Elevate ();

	int nRetCode = 0;

	::CoInitialize (NULL);

	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		cerr << _T("Fatal Error: MFC initialization failed") << endl;
		nRetCode = 1;
	}
	else
	{
		CArray <CUseInfo2, CUseInfo2 &> vMappedDrives;

		if (FILE * fp = _tfopen (GetOutputFilename (), _T ("w"))) 
			fclose (fp);

		cout << w2a (GetOutputFilename ()) << endl;

		TRACEF (::GetCommandLine ());
		TRACEF (GetComputerName ());
		TRACEF (GetIpAddress ());
		const CTime tmStart = CTime::GetCurrentTime ();
		TRACEF (tmStart.Format (_T ("%c")));

		{
			#define MAPENTRY(s) { _T (#s), s }

			struct
			{
				LPCTSTR m_lpsz;
				EXTENDED_NAME_FORMAT m_n;
			} level [] = 
			{
				MAPENTRY (NameUnknown),
				MAPENTRY (NameFullyQualifiedDN),
				MAPENTRY (NameSamCompatible),
				MAPENTRY (NameDisplay),
				MAPENTRY (NameUniqueId),
				MAPENTRY (NameCanonical),
				MAPENTRY (NameUserPrincipal),
				MAPENTRY (NameCanonicalEx),
				MAPENTRY (NameServicePrincipal),
				MAPENTRY (NameDnsDomain),
			};

			std::vector <std::vector <std::wstring>> v;

			for (int i = 0; i < ARRAYSIZE (level); i++) {
				TCHAR sz [MAX_PATH] = { 0 };
				DWORD dw = ARRAYSIZE (sz);
				std::vector <std::wstring> vRow;
			
				GetUserNameEx(level [i].m_n, sz, &dw);
				vRow.push_back (level [i].m_lpsz);
				vRow.push_back (sz);
				v.push_back (vRow);
			}

			TRACEF ("Current user:");
			TRACEF (v);
		}

		ExtractComponents ();

		{
			OSVERSIONINFO osver = { sizeof (osver) };
			CString strTmp;

			::GetVersionEx (&osver);

			strTmp.Format (
				_T ("OS version:\n")
				_T ("%sdwOSVersionInfoSize: %d\n")
				_T ("%sdwMajorVersion:      %d\n")
				_T ("%sdwMinorVersion:      %d\n")
				_T ("%sdwBuildNumber:       %d\n")
				_T ("%sdwPlatformId:        %d\n")
				_T ("%sszCSDVersion:        %s\n"),
				CString (' ', COLUMN_ALIGNMENT), osver.dwOSVersionInfoSize,
				CString (' ', COLUMN_ALIGNMENT), osver.dwMajorVersion,
				CString (' ', COLUMN_ALIGNMENT), osver.dwMinorVersion,
				CString (' ', COLUMN_ALIGNMENT), osver.dwBuildNumber,
				CString (' ', COLUMN_ALIGNMENT), osver.dwPlatformId,
				CString (' ', COLUMN_ALIGNMENT), osver.szCSDVersion);
			TRACEF (strTmp);
		}

		#define MAKEMAPENTRY(k) { &k, _T (#k), }

		struct
		{
			const KNOWNFOLDERID * m_k;
			LPCTSTR m_lpsz;
		} mapShortcuts [] =
		{
			MAKEMAPENTRY (FOLDERID_CommonPrograms),
			MAKEMAPENTRY (FOLDERID_Desktop),
			MAKEMAPENTRY (FOLDERID_CommonStartup),
			MAKEMAPENTRY (FOLDERID_QuickLaunch),
		};
		LPCTSTR lpszExe [] = 
		{
			_T ("Control.exe"),
			_T ("mkdraw.exe"),
			_T ("Config.exe"),
			_T ("SQL Diagnostic.exe"),
			_T ("Keyboard.exe"),
		};
		CStringArray vCmdLine, vInstallPath, vVersions;

		TRACEF (CString ('-', 80));
		TRACEF ("Checking shortcuts...");

		{
			for (int i = 0; i < ARRAYSIZE (mapShortcuts); i++) {
				CString strShortcut = GetKnownFolderPath (* mapShortcuts [i].m_k);
				TRACEF (mapShortcuts [i].m_lpsz + CString (_T (" --> ")) + strShortcut);
			}

			TRACEF (_T ("FOLDERID_ProgramFiles --> ") + GetKnownFolderPath (FOLDERID_ProgramFiles));
			TRACEF (_T ("FOLDERID_ProgramFilesX86 --> ") + GetKnownFolderPath (FOLDERID_ProgramFilesX86));

			TRACEF (CString ('-', 80));
		}

		for (int nDir = 0; nDir < ARRAYSIZE (mapShortcuts); nDir++) {
			CStringArray v;
			int nShortcuts = 0;
			CString strShortcut = GetKnownFolderPath (* mapShortcuts [nDir].m_k);

			TRACEF ("");
			TRACEF (_T ("Checking: ") + strShortcut + _T (" ..."));
			GetFiles (strShortcut, _T ("*.*"), v);

			for (int i = 0; i < v.GetSize (); i++) {
				int nIndex = v [i].ReverseFind ('.');

				if (nIndex != -1) {
					CString strSuffix = v [i].Mid (nIndex);

					if (!strSuffix.CompareNoCase (_T (".lnk"))) {
						CString strExe, strFile = v [i];
						CString strCmdLine = GetShortcutCmdLine (strFile, strExe);
						CString strTitle = GetTitle (strExe);

						{
							CString str = strExe;

							str.MakeLower ();
	
							if (str.Find (_T ("\\system32\\control.exe")) != -1)
								continue;
						}

						for (int j = 0; j < ARRAYSIZE (lpszExe); j++) {
							if (!strTitle.CompareNoCase (lpszExe [j])) {
								CString strInstallPath = GetPath (strExe);
								TRACEF (CString ('-', 80));
								TRACEF (_T ("[") + ToString (nShortcuts + 1) + _T ("]: ") + 
									GetTitle (strFile) + _T ("\n") 
									+ CString (' ', COLUMN_ALIGNMENT * 2) + strFile + _T ("\n") 
									+ CString (' ', COLUMN_ALIGNMENT * 2) + strExe + _T ("\n") 
									+ CString (' ', COLUMN_ALIGNMENT * 2) + strCmdLine);

								if (Find (vInstallPath, strInstallPath) == -1)
									vInstallPath.Add (strInstallPath);

								if (Find (vCmdLine, strCmdLine) == -1)
									vCmdLine.Add (strCmdLine);

								nShortcuts++;
								break;
							}
						}
					}
				}
			}

			if (!nShortcuts)
				TRACEF ("no shortcuts found");
		}

		if (vCmdLine.GetSize () > 1)
			TRACEF ("***** WARNING: not all command line arguments match");

		TRACEF ("");

		{
			TRACEF (CString ('-', 80));
			TRACEF ("enumerating mapped drives...");

			{
				struct
				{
					LPCTSTR m_lpsz;
					int m_n;
				} type [] = 
				{
					MAPENTRY (DRIVE_UNKNOWN),
					MAPENTRY (DRIVE_NO_ROOT_DIR),
					MAPENTRY (DRIVE_REMOVABLE),
					MAPENTRY (DRIVE_FIXED),
					MAPENTRY (DRIVE_REMOTE),
					MAPENTRY (DRIVE_CDROM),
					MAPENTRY (DRIVE_RAMDISK),
				};

				DWORD dw = GetLogicalDrives ();
				std::vector <std::vector <std::wstring>> v;

				{
					std::vector <std::wstring> vRow;

					vRow.push_back (_T ("Drive"));
					vRow.push_back (_T ("Type"));
					vRow.push_back (_T ("VolumeName"));
					vRow.push_back (_T ("SystemName"));
					vRow.push_back (_T ("dwVolumeSerialNumber"));
					vRow.push_back (_T ("dwMaximumComponentLength"));
					vRow.push_back (_T ("dwFileSystemFlags"));

					v.push_back (vRow);
				}

				for (TCHAR c = 'A'; c <= 'Z'; c++) {
					if (dw & (1 << (c - 'A'))) {
						std::vector <std::wstring> vRow;
						const CString str = CString (c) + _T (":\\");

						vRow.push_back ((LPCTSTR)str);

						if (GetFileAttributesTimeout (str) == -1) {
							vRow.push_back (_T ("[unreachable]"));
						}
						else {
							int nType = GetDriveType (str);
							
							for (int i = 0; i < ARRAYSIZE (type); i++) {
								if (nType == type [i].m_n) {
									vRow.push_back (type [i].m_lpsz);
									break;
								}
							}

							{
								TCHAR volumeName[MAX_PATH + 1] = { 0 };						
								TCHAR fileSystemName[MAX_PATH + 1] = { 0 };
								DWORD serialNumber = 0;
								DWORD maxComponentLen = 0;
								DWORD fileSystemFlags = 0;

								if (GetVolumeInformation(
									str,
									volumeName,
									ARRAYSIZE(volumeName),
									&serialNumber,
									&maxComponentLen,
									&fileSystemFlags,
									fileSystemName,
									ARRAYSIZE(fileSystemName)))
								{
									vRow.push_back (volumeName);
									vRow.push_back (fileSystemName);
									vRow.push_back (std::to_wstring ((unsigned __int64)serialNumber));
									vRow.push_back (std::to_wstring ((unsigned __int64)maxComponentLen));
									vRow.push_back ((LPCTSTR)GetVolumeInformation_SystemFlags (fileSystemFlags));
								}
								else
									TRACEF (FormatMessage (GetLastError ()));
							}
						}
															
						v.push_back (vRow);
					}
				}

				TRACEF (v);
			}

			EnumMappedDrives (vMappedDrives);

			for (int i = 0; i < vMappedDrives.GetSize (); i++) {
				CUseInfo2 s = vMappedDrives [i];

				TRACEF (s.m_strLocal + _T (" --> ") + s.m_strRemote);
			}

			TRACEF ("");
		}

		{
			CString str [] = 
			{
				_T ("DBQ"),
				_T ("Database"),
				_T ("Driver"),
				_T ("LastUser"),
				_T ("Server"),
				_T ("FIL"),
			};

			{
				CStringArray v;
				TRACEF (CString ('-', 80));
				TRACEF ("enumerating DSNs at HKEY_LOCAL_MACHINE...");

				GetDsnEntries (v, HKEY_LOCAL_MACHINE);

				for (int i = 0; i < v.GetSize (); i++) {
					CString strDSN = v [i];

					TRACEF (strDSN);

					for (int j = 0; j < ARRAYSIZE (str); j++)
						TRACEF (CString (' ', COLUMN_ALIGNMENT) + str [j] + _T (": ") + GetProfileString (HKEY_LOCAL_MACHINE, _T ("SOFTWARE\\ODBC\\ODBC.INI\\") + strDSN, str [j], _T ("")));
				}

				TRACEF ("");
			}

			{
				CStringArray v;
				TRACEF (CString ('-', 80));
				TRACEF ("enumerating DSNs at HKEY_CURRENT_USER...");

				GetDsnEntries (v, HKEY_CURRENT_USER);

				for (int i = 0; i < v.GetSize (); i++) {
					CString strDSN = v [i];

					TRACEF (strDSN);

					for (int j = 0; j < ARRAYSIZE (str); j++)
						TRACEF (CString (' ', COLUMN_ALIGNMENT) + str [j] + _T (": ") + GetProfileString (HKEY_CURRENT_USER, _T ("SOFTWARE\\ODBC\\ODBC.INI\\") + strDSN, str [j], _T ("")));
				}

				TRACEF ("");
			}
		}

		{
			TRACEF (CString ('-', 80));
			TRACEF ("checking installation folders for version numbers...");

			if (vInstallPath.GetSize () > 1)
				TRACEF ("***** WARNING: multiple install locations");

			{
				CString str = GetKnownFolderPath (FOLDERID_ProgramFiles) + _T ("\\Foxjet\\MarksmanELITE");

				if (Find (vInstallPath, str) == -1)
					vInstallPath.Add (str);
			}

			{
				CString str = GetKnownFolderPath (FOLDERID_ProgramFilesX86) + _T ("\\Foxjet\\MarksmanELITE");

				if (Find (vInstallPath, str) == -1)
					vInstallPath.Add (str);
			}

			{
				CString str = GetHomeDir ();

				if (Find (vInstallPath, str) == -1)
					vInstallPath.Add (str);
			}

			{
				TRACEF (_T ("vInstallPath: ") + ToString (vInstallPath.GetSize ()));

				for (int nPath = 0; nPath < vInstallPath.GetSize (); nPath++) 
					TRACEF (_T ("vInstallPath [") + ToString (nPath) +  _T ("]: ") + vInstallPath [nPath]);
			}

			for (int nPath = 0; nPath < vInstallPath.GetSize (); nPath++) {
				CStringArray vComponents;

				TRACEF (CString ('-', 80));
				TRACEF (_T ("folder [") + ToString (nPath + 1) + _T ("]: ") + vInstallPath [nPath]);

				GetFiles (vInstallPath [nPath], _T ("*.dll"), vComponents);

				{
					TRACEF (_T ("vComponents: ") + ToString (vComponents.GetSize ()));

					//for (int nComponent = 0; nComponent < vComponents.GetSize (); nComponent++) 
					//	TRACEF (_T ("vComponents [") + ToString (nComponent) +  _T ("]: ") + vComponents [nComponent]);
				}

				for (int nComponent = 0; nComponent < vComponents.GetSize (); nComponent++) {
					CString strComponent = vComponents [nComponent];

					//TRACEF (_T ("vComponents [") + ToString (nComponent) +  _T ("]: ") + vComponents [nComponent]);

					if (strComponent.CompareNoCase (_T ("mfc90.dll")) != 0 &&
						strComponent.CompareNoCase (_T ("MSVCR90.dll")) != 0) 
					{
						if (HMODULE hComponent = ::LoadLibraryEx (strComponent, NULL, DONT_RESOLVE_DLL_REFERENCES)) {
							LPGETDLLVERSION lpfGetVer = (LPGETDLLVERSION)::GetProcAddress (hComponent, CAnsiString (FJGETDLLVERSIONPROCADDRESS));
							CString strGetDllVer = _T ("\"") + GetHomeDir () + _T ("\\fj_GetDllVersion.exe\" \"") + strComponent + _T ("\"");

							if (lpfGetVer)  {
								//VERSIONSTRUCT ver;

								//memset (&ver, 0, sizeof (ver));
								//(* lpfGetVer) (&ver);
								//CString strVer = ToString (ver);
								CString strVer = execute (strGetDllVer);

								if (!strVer.GetLength ())
									TRACEF (CString (' ', COLUMN_ALIGNMENT) + _T ("*** ") + CString (FJGETDLLVERSIONPROCADDRESS) + _T (" failed : ") + strComponent);
								else
									TRACEF (CString (' ', COLUMN_ALIGNMENT) + strVer + _T (": ") + strComponent);

								if (Find (vVersions, strVer) == -1)
									vVersions.Add (strVer);
							}

							::FreeLibrary (hComponent);
						}
						else
							TRACEF (CString (' ', COLUMN_ALIGNMENT) + _T ("LoadLibraryEx failed: ") + strComponent);
					}
				}
			}

			TRACEF (CString ('-', 80));

			if (vVersions.GetSize () > 1) 
				TRACEF (_T ("***** WARNING: multiple versions installed"));
			else
				TRACEF (_T ("versions installed: "));

			for (int i = 0; i < vVersions.GetSize (); i++) 
				TRACEF (CString (' ', COLUMN_ALIGNMENT) + vVersions [i]);

			TRACEF (CString ('-', 80));
		}

		{
			CStringArray vDSN;

			TRACEF (CString ('-', 80));
			TRACEF ("checking DSNs...");

			{
				for (int i = 0; i < vCmdLine.GetSize (); i++) {
					TRACEF (_T ("cmd line [") + ToString (i + 1) + _T ("]: ") + vCmdLine [i]);

					CString strCmdLine = vCmdLine [i];
					strCmdLine.MakeUpper ();
					CString strDSN = Extract (strCmdLine, _T ("/DSN="), _T (" "));

					if (strDSN.GetLength ())
						if (Find (vDSN, strDSN) == -1)
							vDSN.Add (strDSN);
				}

				TRACEF ("");

				for (int i = 0; i < vDSN.GetSize (); i++) {
					TRACEF (CString (' ', COLUMN_ALIGNMENT) + _T ("[") + ToString (i + 1) + _T ("] ") + vDSN [i]);
				}

				TRACEF ("");
			}

			for (int i = 0; i < vDSN.GetSize (); i++) {
				TRACEF (CString ('-', 80));
				CString strDSN = vDSN [i];

				if (strDSN.GetLength ()) {
					try 
					{
						CDatabase db;

						if (CheckDbConnection (db, strDSN, vMappedDrives)) {
							CRecordset rst (&db);
							CString strSQL = _T ("SELECT * FROM Printers;");
						
							{
								CString strDBQ = GetProfileString (HKEY_LOCAL_MACHINE, _T ("SOFTWARE\\ODBC\\ODBC.INI\\") + strDSN, _T ("DBQ"), _T (""));
								CString strPath = strDBQ;
								int nIndex = strPath.ReverseFind ('\\');

								if (nIndex != -1) {
									strPath = strPath.Left (nIndex);

									if (FindCaseInsensitive (vInstallPath, strPath) == -1)
										vInstallPath.Add (strPath);	
								}
							}

							TRACEF (strSQL);

							if (rst.Open (CRecordset::snapshot, strSQL, CRecordset::readOnly)) {
								for (int n = 1; !rst.IsEOF (); n++) {
									CString strRow;

									strRow.Format (_T ("[%d]: "), n);
									
									for (short i = 0; i < rst.GetODBCFieldCount (); i++) { 
										CString strField;

										rst.GetFieldValue (i, strField);
										strRow += strField + _T (", ");
									}

									TRACEF (strRow);

									rst.MoveNext ();
								}
							}

							{
								CStringArray vTmp;
							
								TRACEF (CString ('-', 80));
								TRACEF (_T ("checking for database elements..."));
								LocateDatabaseElements (db, vTmp);
								TRACEF (CString ('-', 80));

								if (vTmp.GetSize ())
									TRACEF (_T ("DSNs referenced:"));

								for (int i = 0; i < vTmp.GetSize (); i++) {
									TRACEF (CString (' ', COLUMN_ALIGNMENT) + _T ("[") + ToString (i + 1) + _T ("] ") + vTmp [i]);
								}

								TRACEF ("");

								for (int i = 0; i < vTmp.GetSize (); i++) {
									try
									{
										CDatabase dbTmp;

										CheckDbConnection (dbTmp, vTmp [i], vMappedDrives);

										{
											CString strDBQ = GetProfileString (HKEY_LOCAL_MACHINE, _T ("SOFTWARE\\ODBC\\ODBC.INI\\") + vTmp [i], _T ("DBQ"), _T (""));
											CString strPath = strDBQ;
											int nIndex = strPath.ReverseFind ('\\');

											if (nIndex != -1) {
												strPath = strPath.Left (nIndex);

												if (FindCaseInsensitive (vInstallPath, strPath) == -1)
													vInstallPath.Add (strPath);	
											}
										}

										dbTmp.Close ();
									}
									catch (CDBException * e)		{ HANDLEEXCEPTION (e); TRACEF (_T ("***** FAILED *****")); }
									catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
								}

								TRACEF (CString ('-', 80));
							}

							{
								CStringArray vLogos;

								TRACEF ("");
								TRACEF ("Locating logos folders...");
								LocateLogosFolder (db, vLogos);
								TRACEF ("");

								for (int i = 0; i < vLogos.GetSize (); i++) {
									CString str;

									str.Format (_T ("[%d]: %s"), i + 1, vLogos [i]);
									TRACEF (str);
								}

								TRACEF ("");
							}

							db.Close ();
						}
					}
					catch (CDBException * e)		{ HANDLEEXCEPTION (e); TRACEF (_T ("***** FAILED *****")); }
					catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

					TRACEF (CString ('-', 80));
					TRACEF ("");
				}
			}

			TRACEF ("");
		}

		{
			TRACEF (CString ('-', 80));

			for (int nPath = 0; nPath < vInstallPath.GetSize (); nPath++) {
				CStringArray v;
				CString strPath = vInstallPath [nPath];
				
				TRACEF (_T ("enumerating files: ") + strPath + _T (" ..."));
				GetFiles (strPath, _T ("*.*"), v);
				TRACEF (CString (' ', COLUMN_ALIGNMENT) + _T ("found: ") + ToString (v.GetSize ()));

				for (int i = 0; i < v.GetSize (); i++) {
					CString str;
					
					str.Format (_T ("[%6d] %s"), i + 1, v [i]);
					str.Replace (strPath, _T (""));
					TRACEF (CString (' ', COLUMN_ALIGNMENT) + str);

					str = v [i];
					str.MakeLower ();

					if (str.Find (_T (".mdb")) != -1 || str.Find (_T (".accdb"))  != -1 || str.Find (_T (".txt"))  != -1 || str.Find (_T (".log")) != -1) {
						FoxjetDatabase::ACL::CACLInfo info((LPCTSTR)v[i]);

						info.Query();

						std::vector <FoxjetDatabase::ACL::PERMISSION> v = info.GetPermissions();

						for (int n = 0; n < v.size(); n++) {
							CString s;

							s.Format(_T("%d%d%d [%s]: %s\\%s"), v[n].m_mod.owner, v[n].m_mod.group, v[n].m_mod.other, CString(to_string(v[n].m_mod).c_str()),
								v[n].m_strDomain.c_str(),
								v[n].m_strName.c_str());
							TRACEF(CString (' ', COLUMN_ALIGNMENT * 2) + s);
						}
					}
				}
			}
		}

		{
			TRACEF (CString ('-', 80));
			TRACEF (_T ("enumerating processes..."));

			if (HMODULE hlib = ::LoadLibrary (_T ("psapi.dll"))) {
				typedef DWORD (CALLBACK * LPENUMPROCESSES) (DWORD *lpidProcess, DWORD cb, DWORD *cbNeeded);
				typedef DWORD (CALLBACK * LPGETMODULEBASENAME) (HANDLE hProcess, HMODULE hModule, LPTSTR lpBaseName, DWORD nSize);
				typedef DWORD (CALLBACK * LPGENUMPROCESSMODULES) (HANDLE hProcess, HMODULE *lphModule, DWORD cb, LPDWORD lpcbNeeded);
				typedef BOOL (CALLBACK * LPGETPROCESSMEMORYINFO) (HANDLE Process, PPROCESS_MEMORY_COUNTERS ppsmemCounters, DWORD cb);
				typedef DWORD (CALLBACK * LPGETMODULEFILENAMEEX) (HANDLE hProcess, HMODULE hModule, LPTSTR lpFilename, DWORD nSize);

				if (LPENUMPROCESSES lpEnumProcesses = (LPENUMPROCESSES)::GetProcAddress (hlib, ("EnumProcesses"))) {
					DWORD aProcesses[1024], cbNeeded;
					int nFound = 1;

					if ((* lpEnumProcesses) (aProcesses, sizeof(aProcesses), &cbNeeded)) {
						DWORD cProcesses = cbNeeded / sizeof(DWORD);

						for (UINT i = 0; i < cProcesses; i++) {
							if (aProcesses [i]) {
								TCHAR szTitle [MAX_PATH] = _T ("<unknown>");
								TCHAR szFilepath [MAX_PATH] = { 0 };
								CString strProcess;

								strProcess.Format (_T ("[%d]: %d [0x%p] "), nFound++, aProcesses [i], aProcesses [i]);	

								if (HANDLE hProcess = ::OpenProcess (PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, aProcesses [i])) {
									if (LPGENUMPROCESSMODULES lpEnumProcessModules = (LPGENUMPROCESSMODULES)::GetProcAddress (hlib, ("EnumProcessModules"))) {
										HMODULE hMod = NULL;

										if (lpEnumProcessModules (hProcess, &hMod, sizeof(hMod), &cbNeeded)) {

											if (LPGETMODULEBASENAME lpf = (LPGETMODULEBASENAME)::GetProcAddress (hlib, ("GetModuleBaseNameW"))) {
												(* lpf) (hProcess, hMod, szTitle, ARRAYSIZE (szTitle));
												
												strProcess += _T ("\n") + CString (' ', COLUMN_ALIGNMENT) + szTitle;
											}

											if (hMod) {
												if (LPGETMODULEFILENAMEEX lpGetModuleFileNameEx = (LPGETMODULEFILENAMEEX)::GetProcAddress (hlib, ("GetModuleFileNameExW"))) {
		
													(* lpGetModuleFileNameEx) (hProcess, hMod, szFilepath, ARRAYSIZE (szFilepath) - 1);
													strProcess += _T ("\n") + CString (' ', COLUMN_ALIGNMENT) + _T ("\"") + CString (szFilepath) + _T ("\"");

													{
														CString strFilepath (szFilepath), strTitle (szTitle);

														strFilepath.MakeLower ();
														strTitle.MakeLower ();

														ASSERT (strFilepath.Find (strTitle) != -1);
													}
												}
											}

											strProcess += _T ("\n");
										}

										if (LPGETPROCESSMEMORYINFO lpGetProcessMemoryInfo = (LPGETPROCESSMEMORYINFO)::GetProcAddress (hlib, ("GetProcessMemoryInfo"))) {
											PROCESS_MEMORY_COUNTERS counters;
											CString str, strAlign (' ', COLUMN_ALIGNMENT);

											memset (&counters, 0, sizeof (counters));
											(* lpGetProcessMemoryInfo) (hProcess, &counters, sizeof (counters));

											str.Format (
												_T ("%sPageFaultCount             = %d\n")
												_T ("%sPeakWorkingSetSize         = %d\n")
												_T ("%sWorkingSetSize             = %d\n")
												_T ("%sQuotaPeakPagedPoolUsage    = %d\n")
												_T ("%sQuotaPagedPoolUsage        = %d\n")
												_T ("%sQuotaPeakNonPagedPoolUsage = %d\n")
												_T ("%sQuotaNonPagedPoolUsage     = %d\n")
												_T ("%sPagefileUsage              = %d\n")
												_T ("%sPeakPagefileUsage          = %d\n"),
												strAlign, counters.PageFaultCount,
												strAlign, counters.PeakWorkingSetSize,
												strAlign, counters.WorkingSetSize,
												strAlign, counters.QuotaPeakPagedPoolUsage,
												strAlign, counters.QuotaPagedPoolUsage,
												strAlign, counters.QuotaPeakNonPagedPoolUsage,
												strAlign, counters.QuotaNonPagedPoolUsage,
												strAlign, counters.PagefileUsage,
												strAlign, counters.PeakPagefileUsage);
											strProcess += str;
										}
									}
								}

								TRACEF (strProcess);
							}
						}
					}
				}

				::FreeLibrary (hlib);
			}
			else
				TRACEF (_T ("LoadLibrary failed: psapi.dll"));

			TRACEF (CString ('-', 80));
		}

		{
			TRACEF (CString ('-', 80));

			struct 
			{
				LPCTSTR m_lpszKey;
				LPCTSTR m_lpszFile;
			}
			static const map [] =
			{
				{ _T ("HKEY_CURRENT_USER\\Software\\FoxJet"),			_T ("Foxjet.reg") },
				{ _T ("HKEY_LOCAL_MACHINE\\SOFTWARE\\ODBC\\ODBC.INI"),	_T ("ODBC.reg") },
			};

			for (int i = 0; i < ARRAYSIZE (map); i++) {
				CString str, strFile; 

				TRACEF (CString ('-', 80));
				TRACEF (map [i].m_lpszKey);
				strFile.Format (_T ("%s\\%s"), GetTempPath (), map [i].m_lpszFile);
				::DeleteFile (strFile);
				str.Format (_T ("reg export \"%s\" \"%s\""), map [i].m_lpszKey, strFile);
				TRACEF (str);
				str = execute (str);
				TRACEF (str);
				str.Empty ();

				TRACEF ("");

				if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
					WORD w = 0;
					fread (&w, sizeof (w), 1, fp);

					if (w == 0xFEFF) {
						TCHAR sz [1024];

						do
						{
							memset (sz, 0, ARRAYSIZE (sz));
							_fgetts (sz, ARRAYSIZE (sz) - 1, fp);
							str += sz;
						}
						while (!feof (fp));
					}
					else {
						char sz [1024];

						do
						{
							memset (sz, 0, ARRAYSIZE (sz));
							fgets (sz, ARRAYSIZE (sz) - 1, fp);
							str += a2w (sz);
						}
						while (!feof (fp));
					}

					str.Replace (_T ("\r\n"), _T ("\n"));
					str.Replace (_T ("\n"), _T ("\n") + CString (' ', COLUMN_ALIGNMENT));
					TRACEF (str);

					fclose (fp);
				}

				::DeleteFile (strFile);
			}

			TRACEF (CString ('-', 80));
		}


		CTime tmStop = CTime::GetCurrentTime ();
		CTimeSpan tm = tmStop - tmStart;
		TRACEF (tmStop.Format (_T ("%c [") + tm.Format (_T ("%D %H:%M:%S")) + _T ("]")));

		//#ifndef _DEBUG
		{
			CString str = ::GetCommandLine ();
			
			str.MakeLower ();

			if (str.Find (_T ("/no_notepad")) == -1) {
				CString strCmdLine = _T ("notepad.exe  \"") + GetOutputFilename () + _T ("\"");
				STARTUPINFO si;     
				PROCESS_INFORMATION pi; 
				TCHAR szCmdLine [0xFF];

				_tcsncpy (szCmdLine, strCmdLine, 0xFF);
				memset (&pi, 0, sizeof(pi));
				memset (&si, 0, sizeof(si));
				si.cb = sizeof(si);     
				si.dwFlags = STARTF_USESHOWWINDOW;     
				si.wShowWindow = SW_SHOW;     

				::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
			}
		}
		//#endif

	}

	::CoUninitialize ();

	return nRetCode;
}


