// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit
#define _AFX_NO_MFC_CONTROLS_IN_DIALOGS         // remove support for MFC controls in dialogs

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#include <afx.h>
#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>                     // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <iostream>
#include <string>
#include <vector>

std::wstring a2w(const std::string & str);
std::string w2a(const std::wstring & str);
CString a2w(const CString & str);
CString w2a(const CString & str);

void Trace(LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine);
void Trace(const std::string & str, LPCTSTR lpszFile, ULONG lLine);
void Trace(const std::wstring & str, LPCTSTR lpszFile, ULONG lLine);
#define TRACEF(s) Trace ((s), _T (__FILE__), __LINE__)

std::string trim(const std::string & str);
std::wstring trim(const std::wstring & str);

DWORD GetFileSize(const CString & strFile);
bool IsFileUTF16(const CString & strFile);
CString ReadDirect(const CString & strFile, const CString & strDefault = _T(""));
CString GetFileExt(const CString & strFile);
int GetFiles(const CString & strDir, const CString & strFile, CStringArray & v);

std::string urldecode(const std::string & str);
std::vector <unsigned char> GetIpAddress(const std::string & str);
std::vector <std::string> GetIpAddresses();
std::string to_string(const char * pData, int nLen);
std::string to_hex_string(const unsigned char * pData, int nLen);

template <class T>
T make_printable(T str)
{
	for (int i = 0; i < str.length(); i++)
		if (str[i] < ' ' || str[i] > '~')
			str[i] = '.';

	return str;
}

template <class T, class T2>
T2 GetChar(const T & str, int nIndex)
{
	if (nIndex >= 0 && nIndex < (int)str.length())
		return str[nIndex];

	return (T2)0;
}

template <class T, class T2>
std::vector <int> Count(const T & str, T2 c)
{
	std::vector <int> v;

	for (int i = 0; i < (int)str.size(); i++)
		if (str[i] == c)
			v.push_back(i);

	return v;
}

template <class T, class T2>
int Count(const std::vector <T> & v, const T2 c)
{
	int nResult = 0;

	for (int i = 0; i < (int)v.size(); i++) {
		if (GetChar(v[i], i) == c)
			nResult++;
	}

	return nResult;
}

template <class T, class T2>
std::vector<T> explode(const T & str, T2 delim)
{
	std::vector<T> result;
	std::vector <int> v = Count(str, delim);
	size_t nIndex = 0;

	for (size_t i = 0; i < v.size(); i++) {
		int nLen = v[i] - nIndex;
		result.push_back(str.substr(nIndex, nLen));
		nIndex = v[i] + 1;
	}

	if (nIndex < str.length()) {
		result.push_back(str.substr(nIndex, str.length() - nIndex));
	}
	else {
		if (str.length() && (T2)str[str.length() - 1] == delim)
			result.push_back(T());
	}

	return result;
}


template <class T>
inline T implode(const std::vector<T> & v, const T & delim)
{
	T result;

	for (UINT i = 0; i < v.size(); i++) {
		result += v[i];

		if (i < (v.size() - 1))
			result += delim;
	}

	return result;
}