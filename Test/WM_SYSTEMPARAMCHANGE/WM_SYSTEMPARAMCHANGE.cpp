// WM_SYSTEMPARAMCHANGE.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "WM_SYSTEMPARAMCHANGE.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// The one and only application object

CWinApp theApp;

using namespace std;

int main()
{
    int nRetCode = 0;

    HMODULE hModule = ::GetModuleHandle(nullptr);

    if (hModule != nullptr)
    {
        // initialize MFC and print and error on failure
        if (!AfxWinInit(hModule, nullptr, ::GetCommandLine(), 0))
        {
            // TODO: change error code to suit your needs
            wprintf(L"Fatal Error: MFC initialization failed\n");
            nRetCode = 1;
        }
        else
        {
            // TODO: code your application's behavior here.
        }
    }
    else
    {
        // TODO: change error code to suit your needs
        wprintf(L"Fatal Error: GetModuleHandle failed\n");
        nRetCode = 1;
    }

	const UINT WM_SYSTEMPARAMCHANGE = ::RegisterWindowMessage (_T("Foxjet::System params changed"));

	srand (time (NULL));

	while (1) {
		CTime tm = CTime::GetCurrentTime ();
		int nMin = (rand () % 3) + 1;
		CTimeSpan span = (tm + CTimeSpan(0, 0, nMin, rand() % 60)) - tm;
		int nSleep = span.GetTotalSeconds() * 1000;
		
		::PostMessage(HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, 0);
		TRACEF (CTime::GetCurrentTime().Format (_T ("[%H:%M:%S] ")) + span.Format (_T ("WM_SYSTEMPARAMCHANGE, sleeping: %H:%M:%S...")));

		::Sleep (nSleep);
	}

    return nRetCode;
}
