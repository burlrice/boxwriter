// ImageCapture.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <afxsock.h>
#include <afxinet.h>
#include "ImageCapture.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// The one and only application object

CWinApp theApp;

using namespace std;

int main(int argc, char ** argv)
{
    int nRetCode = 0;
	WSADATA wsaData;

	VERIFY(::WSAStartup(MAKEWORD(1, 1), &wsaData) == 0);

    HMODULE hModule = ::GetModuleHandle(nullptr);

    if (hModule != nullptr)
    {
        // initialize MFC and print and error on failure
        if (!AfxWinInit(hModule, nullptr, ::GetCommandLine(), 0))
        {
            // TODO: change error code to suit your needs
            wprintf(L"Fatal Error: MFC initialization failed\n");
            nRetCode = 1;
        }
        else
        {
            // TODO: code your application's behavior here.
        }
    }
    else
    {
        // TODO: change error code to suit your needs
        wprintf(L"Fatal Error: GetModuleHandle failed\n");
        nRetCode = 1;
    }

	const CString strAddr = argc >= 2 ? a2w(argv[1]).c_str() : _T("192.168.1.62"); // _T("127.0.0.1"); 
	static const int nMaxHeads = 2;
	CString strLastHash[nMaxHeads];
	CInternetSession session(_T("x"));

	TRACEF(strAddr);

	while (1) {

		if (CHttpConnection * pURL = session.GetHttpConnection(strAddr, (INTERNET_PORT)2204)) {
			for (int nHead = 0; nHead < nMaxHeads; nHead++) {
				CString strPage;
				CString strHash;
				int nRead;
				char sz[1024] = { 0 };

				strPage.Format(_T("/%d.hash.txt"), nHead + 1);

				CHttpFile * pFile = pURL->OpenRequest(_T("GET"), strPage, NULL, 1, NULL, _T("HTTP/1.1"), INTERNET_FLAG_RELOAD | INTERNET_FLAG_DONT_CACHE);

				if (pFile && (HANDLE)(*pFile) != INVALID_HANDLE_VALUE) {
					CString strResult;

					if (pFile->SendRequest()) {
						while (nRead = pFile->Read(sz, ARRAYSIZE(sz) - 1)) {
							//::OutputDebugString (a2w (sz));
							strHash += a2w(sz).c_str();
							memset(sz, 0, ARRAYSIZE(sz));
						}

						pFile->Close();
						delete pFile;
						pFile = NULL;
					}
				}
				
				if (strHash.IsEmpty()) {
					strPage.Format(_T("/USB%d.php"), nHead + 1);

					CHttpFile * pFile = pURL->OpenRequest(_T("GET"), strPage, NULL, 1, NULL, _T("HTTP/1.1"), INTERNET_FLAG_RELOAD | INTERNET_FLAG_DONT_CACHE);

					if (pFile && (HANDLE)(*pFile) != INVALID_HANDLE_VALUE) {
						CString strResult;
						char sz[1024] = { 0 };

						if (pFile->SendRequest()) {
							while (nRead = pFile->Read(sz, ARRAYSIZE(sz) - 1)) {
								//::OutputDebugString (a2w (sz));
								strHash += a2w(sz).c_str();
								memset(sz, 0, ARRAYSIZE(sz));
							}

							strHash.Trim ();
							pFile->Close();
							delete pFile;
							pFile = NULL;
						}
					}
				}

				if (strHash.GetLength() && strLastHash[nHead] != strHash) {
					TRACEF(strPage + _T(": ") + strHash);
					strLastHash[nHead] = strHash;

					strPage.Format(_T("/%d.bmp?context=EDITOR&t=%d"), nHead + 1, ::GetTickCount());
					pFile = pURL->OpenRequest(_T("GET"), strPage, NULL, 1, NULL, _T("HTTP/1.1"), INTERNET_FLAG_RELOAD | INTERNET_FLAG_DONT_CACHE);

					if (pFile && (HANDLE)(*pFile) != INVALID_HANDLE_VALUE) {
						if (pFile->SendRequest()) {
							CString strFile;

							strFile.Format(_T("C:\\Foxjet\\Debug\\capture\\[%d]%s.bmp"),
								nHead + 1,
								CTime::GetCurrentTime().Format(_T(" %H_%M_%S")));

							if (FILE * fp = _tfopen(strFile, _T("wb"))) {
								TRACEF(strFile);

								while (nRead = pFile->Read(sz, ARRAYSIZE(sz) - 1)) {
									fwrite(sz, nRead, 1, fp);
									memset(sz, 0, ARRAYSIZE(sz));
								}

								fclose(fp);
							}

							pFile->Close();
							delete pFile;
							pFile = NULL;
						}
					}
				}
			}

			pURL->Close();
			delete pURL;
		}

		::Sleep (5 * 1000);
	}

    return nRetCode;
}
