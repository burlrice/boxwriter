
// SqlTestDlg.h : header file
//

#pragma once


// CSqlTestDlg dialog
class CSqlTestDlg : public CDialogEx
{
// Construction
public:
	CSqlTestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_SQLTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	int DoTest ();

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	virtual void OnOK ();
	virtual void OnCancel ();

	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClicked3();
	afx_msg void OnBnClicked1();
	afx_msg void OnBnClicked2();
};
