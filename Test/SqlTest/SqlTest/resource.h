//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SqlTest.rc
//
#define BTN_1                           102
#define BTN_2                           103
#define BTN_3                           104
#define IDD_SQLTEST_DIALOG              102
#define IDR_MAINFRAME                   128
#define TXT_DSN                         1000
#define TXT_SQL                         1001
#define BTN_TEST                        1002
#define TXT_DSN2                        1003
#define TXT_LANGID                      1003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
