
// SqlTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SqlTest.h"
#include "SqlTestDlg.h"
#include "afxdialogex.h"
#include <afxdb.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSqlTestDlg dialog

class CDatabaseEx : public CDatabase
{
public:
	int ExecuteSQL (LPCTSTR lpszSQL)
	{
	//	CDatabase::ExecuteSQL (FormatSQL (COdbcVariant::FormatUnicode (lpszSQL)));
		RETCODE nRetCode;
		HSTMT hstmt;
		SQLINTEGER nResult = 0;

		//TRACER (lpszSQL);

		//if (FILE * fp = _tfopen (_T ("C:\\Foxjet\\sql.txt"), _T ("a"))) {
		//	fwrite (w2a (lpszSQL), _tcslen (lpszSQL), 1, fp);
		//	fwrite ("\n", 1, 1, fp);
		//	fclose (fp);
		//}

		ENSURE_VALID(this);
		ENSURE_ARG(AfxIsValidString(lpszSQL));

		AFX_SQL_SYNC(::SQLAllocStmt(m_hdbc, &hstmt));
		if (!CheckHstmt(nRetCode, hstmt))
			AfxThrowDBException(nRetCode, this, hstmt);

		TRY
		{
			OnSetOptions(hstmt);

			// Give derived CDatabase classes option to use parameters
			BindParameters(hstmt);

			LPTSTR pszSQL = const_cast<LPTSTR>(lpszSQL);
			AFX_ODBC_CALL(::SQLExecDirect(hstmt, reinterpret_cast<SQLTCHAR *>(pszSQL), SQL_NTS));
			if (!CheckHstmt(nRetCode, hstmt))
				AfxThrowDBException(nRetCode, this, hstmt);

			AFX_ODBC_CALL(::SQLRowCount (hstmt, &nResult));

			SWORD nResultColumns;
			do
			{
				AFX_ODBC_CALL(::SQLNumResultCols(hstmt, &nResultColumns));
				if(!CheckHstmt(nRetCode, hstmt))
					AfxThrowDBException(nRetCode, this, hstmt);

				if (nResultColumns != 0)
					do
					{
						AFX_ODBC_CALL(::SQLFetch(hstmt));
						if(!CheckHstmt(nRetCode, hstmt))
							AfxThrowDBException(nRetCode, this, hstmt);
					} while (nRetCode != SQL_NO_DATA_FOUND);
				AFX_ODBC_CALL(::SQLMoreResults(hstmt));
				if(!CheckHstmt(nRetCode, hstmt))
					AfxThrowDBException(nRetCode, this, hstmt);
			} while (nRetCode != SQL_NO_DATA_FOUND);
		}
		CATCH_ALL(e)
		{
			::SQLCancel(hstmt);
			AFX_SQL_SYNC(::SQLFreeStmt(hstmt, SQL_DROP));
			THROW_LAST();
		}
		END_CATCH_ALL

		AFX_SQL_SYNC(::SQLFreeStmt(hstmt, SQL_DROP));

		//GetTables ();

		return nResult;
	}
};


CSqlTestDlg::CSqlTestDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSqlTestDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSqlTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSqlTestDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_TEST, &CSqlTestDlg::OnBnClickedOk)
	ON_BN_CLICKED(BTN_1, &CSqlTestDlg::OnBnClicked1)
	ON_BN_CLICKED(BTN_2, &CSqlTestDlg::OnBnClicked2)
	ON_BN_CLICKED(BTN_3, &CSqlTestDlg::OnBnClicked3)
END_MESSAGE_MAP()


// CSqlTestDlg message handlers

BOOL CSqlTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	SetDlgItemText (TXT_DSN, _T ("MKNetwork"));
	SetDlgItemText (TXT_SQL, _T ("UPDATE Lines SET \r\r\n\t\"Name\"='Lamontagne',\r\r\n\t\"Maintenance\"='12:00',\r\r\n\t\"ResetScanInfo\"='1',\r\r\n\t\"MaxConsecutiveNoReads\"='3',\r\r\n\t\"SerialParams1\"='9600,8,N,1,0',\r\r\n\t\"PrinterID\"='1' \r\r\nWHERE [ID]=7;"));

	{
		CString str;

		str.Format (_T ("0x%04X, 0x%04X"), GetUserDefaultUILanguage (), GetSystemDefaultUILanguage ());
		SetDlgItemText (TXT_LANGID, str);
	}
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSqlTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSqlTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CSqlTestDlg::OnOK ()
{
	CDialog::OnOK ();
}

void CSqlTestDlg::OnCancel ()
{
	CDialog::OnCancel ();
}

int CSqlTestDlg::DoTest ()
{
	CString strDSN, strSQL;
	
	GetDlgItemText (TXT_DSN, strDSN);
	GetDlgItemText (TXT_SQL, strSQL);

	CDatabaseEx db;

	db.OpenEx (_T ("DSN=") + strDSN, /* CDatabase::useCursorLib | */ CDatabase::noOdbcDialog); 
	//db.CDatabase::ExecuteSQL (strSQL);
	return db.ExecuteSQL (strSQL);
}

void CSqlTestDlg::OnBnClickedOk()
{
	try
	{
		CString str;

		str.Format (_T ("%d records updated"), DoTest ());
		MessageBox (str);
	}
	catch (CDBException * e) {
		MessageBox (e->m_strError, _T ("Error"), MB_ICONERROR);
		e->Delete ();
	}
}



void CSqlTestDlg::OnBnClicked1()
{
	SetDlgItemText (TXT_SQL, _T ("UPDATE Lines SET \r\n\t\"Name\"='Lamontagne',\r\n\t\"Desc\"='impression foxjet matrix',\r\n\t\"Maintenance\"='12:00',\r\n\t\"NoRead\"='NOREAD',\r\n\t\"ResetScanInfo\"='1',\r\n\t\"MaxConsecutiveNoReads\"='3',\r\n\t\"BufferOffset\"='0',\r\n\t\"SignificantChars\"='5',\r\n\t\"AuxBoxIp\"='0.0.0.0',\r\n\t\"AuxBoxEnabled\"='0',\r\n\t\"SerialParams1\"='9600,8,N,1,0',\r\n\t\"SerialParams2\"='9600,8,N,1,0',\r\n\t\"SerialParams3\"='9600,8,N,1,0',\r\n\t\"AMS32_A1\"='3',\r\n\t\"AMS32_A2\"='1000',\r\n\t\"AMS32_A3\"='10',\r\n\t\"AMS32_A4\"='1000',\r\n\t\"AMS32_A5\"='2500',\r\n\t\"AMS32_A6\"='3',\r\n\t\"AMS32_A7\"='20',\r\n\t\"AMS256_A1\"='3',\r\n\t\"AMS256_A2\"='1000',\r\n\t\"AMS256_A3\"='28',\r\n\t\"AMS256_A4\"='1000',\r\n\t\"AMS256_A5\"='5500',\r\n\t\"AMS256_A6\"='2',\r\n\t\"AMS256_A7\"='100',\r\n\t\"AMS_Interval\"='4.0',\r\n\t\"PrinterID\"='1' WHERE [ID]=7;"));
	OnBnClickedOk ();
}


void CSqlTestDlg::OnBnClicked2()
{
	SetDlgItemText (TXT_SQL, _T ("UPDATE Heads SET \r\n\t\"PanelID\"='37',\r\n\t\"Name\"='FJ0001',\r\n\t\"UID\"='1',\r\n\t\"RelativeHeight\"='1000',\r\n\t\"Channels\"='256',\r\n\t\"HorzRes\"='300',\r\n\t\"PhotocellDelay\"='1000',\r\n\t\"Enabled\"='1',\r\n\t\"Encoder\"='0',\r\n\t\"Direction\"='1',\r\n\t\"Angle\"='90.0',\r\n\t\"Inverted\"='0',\r\n\t\"Photocell\"='0',\r\n\t\"SharePhotocell\"='0',\r\n\t\"ShareEncoder\"='0',\r\n\t\"HeadType\"='4',\r\n\t\"RemoteHead\"='0',\r\n\t\"MasterHead\"='1',\r\n\t\"IntTachSpeed\"='60',\r\n\t\"NozzleSpan\"='4.0',\r\n\t\"PhotocellRate\"='36.0',\r\n\t\"SerialParams\"='9600,8,N,1,0',\r\n\t\"EncoderDivisor\"='3',\r\n\t\"DoublePulse\"='1' WHERE [ID]=1;"));
	OnBnClickedOk ();
}

void CSqlTestDlg::OnBnClicked3()
{
	LPCTSTR lpsz [] = 
	{
		_T ("UPDATE Settings SET \"LineID\"='0',\r\n\t\"Key\"='1, Standby mode',\r\n\t\"Data\"='0'  WHERE [LineID]=0 AND [Key]='1, Standby mode';"),
		_T ("UPDATE Heads SET \"PanelID\"='37',\r\n\t\"Name\"='FJ0001',\r\n\t\"UID\"='1',\r\n\t\"RelativeHeight\"='1000',\r\n\t\"Channels\"='256',\r\n\t\"HorzRes\"='300',\r\n\t\"PhotocellDelay\"='1000',\r\n\t\"Enabled\"='1',\r\n\t\"Encoder\"='0',\r\n\t\"Direction\"='1',\r\n\t\"Angle\"='90.0',\r\n\t\"Inverted\"='0',\r\n\t\"Photocell\"='0',\r\n\t\"SharePhotocell\"='0',\r\n\t\"ShareEncoder\"='0',\r\n\t\"HeadType\"='4',\r\n\t\"RemoteHead\"='0',\r\n\t\"MasterHead\"='1',\r\n\t\"IntTachSpeed\"='60',\r\n\t\"NozzleSpan\"='4.0',\r\n\t\"PhotocellRate\"='36.0',\r\n\t\"SerialParams\"='9600,8,N,1,0',\r\n\t\"EncoderDivisor\"='3',\r\n\t\"DoublePulse\"='1' WHERE [ID]=1;"),
		_T ("UPDATE Settings SET \"LineID\"='0',\r\n\t\"Key\"='Valve head:1',\r\n\t\"Data\"=' '  WHERE [LineID]=0 AND [Key]='Valve head:1';"),
		_T ("INSERT INTO Settings (\"LineID\",\r\n\t\"Key\",\r\n\t\"Data\") VALUES ('0','Valve head:1',' ');"),
		_T ("UPDATE Settings SET \"LineID\"='0',\r\n\t\"Key\"='HEADSTRUCT:1',\r\n\t\"Data\"='{0,0,0}'  WHERE [LineID]=0 AND [Key]='HEADSTRUCT:1';"),
		_T ("UPDATE Heads SET \"PanelID\"='37',\r\n\t\"Name\"='FJ0001',\r\n\t\"UID\"='1',\r\n\t\"RelativeHeight\"='1000',\r\n\t\"Channels\"='256',\r\n\t\"HorzRes\"='300',\r\n\t\"PhotocellDelay\"='1000',\r\n\t\"Enabled\"='1',\r\n\t\"Encoder\"='0',\r\n\t\"Direction\"='1',\r\n\t\"Angle\"='90.0',\r\n\t\"Inverted\"='0',\r\n\t\"Photocell\"='0',\r\n\t\"SharePhotocell\"='0',\r\n\t\"ShareEncoder\"='0',\r\n\t\"HeadType\"='4',\r\n\t\"RemoteHead\"='0',\r\n\t\"MasterHead\"='1',\r\n\t\"IntTachSpeed\"='60',\r\n\t\"NozzleSpan\"='4.0',\r\n\t\"PhotocellRate\"='36.0',\r\n\t\"SerialParams\"='9600,8,N,1,0',\r\n\t\"EncoderDivisor\"='3',\r\n\t\"DoublePulse\"='1' WHERE [ID]=1;"),
		_T ("UPDATE Settings SET \"LineID\"='0',\r\n\t\"Key\"='Valve head:1',\r\n\t\"Data\"='{VALVESTRUCT,4294967295,1,2000,1,0,0,0,4294967295,,1,0,0}'  WHERE [LineID]=0 AND [Key]='Valve head:1';"),
		_T ("UPDATE Settings SET \"LineID\"='0',\r\n\t\"Key\"='HEADSTRUCT:1',\r\n\t\"Data\"='{0,0,0}'  WHERE [LineID]=0 AND [Key]='HEADSTRUCT:1';"),
		_T ("UPDATE Heads SET \"PanelID\"='73',\r\n\t\"Name\"='FJ0001',\r\n\t\"UID\"='1',\r\n\t\"RelativeHeight\"='1000',\r\n\t\"Channels\"='256',\r\n\t\"HorzRes\"='300',\r\n\t\"PhotocellDelay\"='1000',\r\n\t\"Enabled\"='1',\r\n\t\"Encoder\"='0',\r\n\t\"Direction\"='1',\r\n\t\"Angle\"='90.0',\r\n\t\"Inverted\"='0',\r\n\t\"Photocell\"='0',\r\n\t\"SharePhotocell\"='0',\r\n\t\"ShareEncoder\"='0',\r\n\t\"HeadType\"='4',\r\n\t\"RemoteHead\"='0',\r\n\t\"MasterHead\"='1',\r\n\t\"IntTachSpeed\"='60',\r\n\t\"NozzleSpan\"='4.0',\r\n\t\"PhotocellRate\"='36.0',\r\n\t\"SerialParams\"='9600,8,N,1,0',\r\n\t\"EncoderDivisor\"='3',\r\n\t\"DoublePulse\"='1' WHERE [ID]=18;"),
		_T ("UPDATE Settings SET \"LineID\"='0',\r\n\t\"Key\"='Valve head:18',\r\n\t\"Data\"='{VALVESTRUCT,4294967295,18,2000,1,0,0,0,4294967295,,1,0,0}'  WHERE [LineID]=0 AND [Key]='Valve head:18';"),
		_T ("INSERT INTO Settings (\"LineID\",\r\n\t\"Key\",\r\n\t\"Data\") VALUES ('0','Valve head:18','{VALVESTRUCT,4294967295,18,2000,1,0,0,0,4294967295,,1,0,0}');"),
		_T ("UPDATE Settings SET \"LineID\"='0',\r\n\t\"Key\"='HEADSTRUCT:18',\r\n\t\"Data\"='{0,0,0}'  WHERE [LineID]=0 AND [Key]='HEADSTRUCT:18';"),
		_T ("UPDATE Heads SET \"PanelID\"='67',\r\n\t\"Name\"='FJ0001',\r\n\t\"UID\"='1',\r\n\t\"RelativeHeight\"='1000',\r\n\t\"Channels\"='256',\r\n\t\"HorzRes\"='300',\r\n\t\"PhotocellDelay\"='1000',\r\n\t\"Enabled\"='1',\r\n\t\"Encoder\"='0',\r\n\t\"Direction\"='0',\r\n\t\"Angle\"='90.0',\r\n\t\"Inverted\"='0',\r\n\t\"Photocell\"='0',\r\n\t\"SharePhotocell\"='0',\r\n\t\"ShareEncoder\"='0',\r\n\t\"HeadType\"='4',\r\n\t\"RemoteHead\"='0',\r\n\t\"MasterHead\"='1',\r\n\t\"IntTachSpeed\"='60',\r\n\t\"NozzleSpan\"='4.0',\r\n\t\"PhotocellRate\"='36.0',\r\n\t\"SerialParams\"='9600,8,N,1,0',\r\n\t\"EncoderDivisor\"='3',\r\n\t\"DoublePulse\"='1' WHERE [ID]=14;"),
		_T ("UPDATE Settings SET \"LineID\"='0',\r\n\t\"Key\"='Valve head:14',\r\n\t\"Data\"='{VALVESTRUCT,4294967295,14,2000,1,0,0,0,4294967295,,1,0,0}'  WHERE [LineID]=0 AND [Key]='Valve head:14';"),
		_T ("INSERT INTO Settings (\"LineID\",\r\n\t\"Key\",\r\n\t\"Data\") VALUES ('0','Valve head:14','{VALVESTRUCT,4294967295,14,2000,1,0,0,0,4294967295,,1,0,0}');"),
		_T ("UPDATE Settings SET \"LineID\"='0',\r\n\t\"Key\"='HEADSTRUCT:14',\r\n\t\"Data\"='{0,0,0}'  WHERE [LineID]=0 AND [Key]='HEADSTRUCT:14';"),
		_T ("UPDATE Heads SET \"PanelID\"='37',\r\n\t\"Name\"='FJ0001',\r\n\t\"UID\"='1',\r\n\t\"RelativeHeight\"='1000',\r\n\t\"Channels\"='256',\r\n\t\"HorzRes\"='300',\r\n\t\"PhotocellDelay\"='1000',\r\n\t\"Enabled\"='1',\r\n\t\"Encoder\"='0',\r\n\t\"Direction\"='1',\r\n\t\"Angle\"='90.0',\r\n\t\"Inverted\"='0',\r\n\t\"Photocell\"='0',\r\n\t\"SharePhotocell\"='0',\r\n\t\"ShareEncoder\"='0',\r\n\t\"HeadType\"='4',\r\n\t\"RemoteHead\"='0',\r\n\t\"MasterHead\"='1',\r\n\t\"IntTachSpeed\"='60',\r\n\t\"NozzleSpan\"='4.0',\r\n\t\"PhotocellRate\"='36.0',\r\n\t\"SerialParams\"='9600,8,N,1,0',\r\n\t\"EncoderDivisor\"='3',\r\n\t\"DoublePulse\"='1' WHERE [ID]=1;"),
		_T ("UPDATE Settings SET \"LineID\"='0',\r\n\t\"Key\"='Valve head:1',\r\n\t\"Data\"=' '  WHERE [LineID]=0 AND [Key]='Valve head:1';"),
		_T ("UPDATE Settings SET \"LineID\"='0',\r\n\t\"Key\"='HEADSTRUCT:1',\r\n\t\"Data\"='{0,0,0}'  WHERE [LineID]=0 AND [Key]='HEADSTRUCT:1';"),
	};
	int nResult = 0;

	BeginWaitCursor ();

	try 
	{
		CString str;

		for (int i = 0; i < ARRAYSIZE (lpsz); i++) {
			SetDlgItemText (TXT_SQL, lpsz [i]);
			nResult += DoTest ();
		}

		str.Format (_T ("%d records updated"), nResult);
		MessageBox (str);
	}
	catch (CDBException * e) {
		MessageBox (e->m_strError, _T ("Error"), MB_ICONERROR);
		e->Delete ();
	}

	EndWaitCursor ();
}

