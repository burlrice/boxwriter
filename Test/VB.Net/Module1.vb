﻿' VB2019

Imports System
Imports System.Net
Imports System.Net.Sockets
Imports System.Text

Module Module1
    Sub Main()
        Dim s As New System.Net.Sockets.TcpClient()

        s.Connect("192.168.1.62", 2202) ' IP address of the FoxJet printer
        s.ReceiveBufferSize = 4096

        Dim os As Byte() = System.Text.Encoding.ASCII.GetBytes("{Get Count}")
        Dim ss As NetworkStream = s.GetStream()

        ss.Write(os, 0, os.Length)
        ss.Flush()

        Dim rx(s.ReceiveBufferSize) As Byte
        Dim nBytes As Int16 = ss.Read(rx, 0, CInt(s.ReceiveBufferSize))
        Dim str As String = System.Text.Encoding.ASCII.GetString(rx)
        str = str.Substring(1, nBytes - 2)
        Dim Count As Integer = CInt(str.Split(",").ElementAt(2))

        MsgBox("Count: " & Count)


        os = System.Text.Encoding.ASCII.GetBytes("{Get user elements}")
        ss.Write(os, 0, os.Length)
        ss.Flush()

        nBytes = ss.Read(rx, 0, CInt(s.ReceiveBufferSize))
        str = System.Text.Encoding.ASCII.GetString(rx)
        str = str.Substring(1, nBytes - 2)
        Dim v As String() = str.Split(",")

        For i = 2 To v.Count() - 1
            MsgBox(v.ElementAt(i) & ": " & v.ElementAt(i + 1))
            i = i + 1
        Next
    End Sub

End Module
