// stdafx.cpp : source file that includes just the standard includes
// sync_test.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

using namespace std;

void Trace(LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	CString str;
	CString strFile = lpszFile;
	int nIndex = strFile.ReverseFind('\\');

	//if (nIndex != -1)
	//	strFile = strFile.Mid(nIndex + 1);

	str.Format(_T("%s(%d): %s\n"), strFile, lLine, lpsz);
	std::wstring s = str;

	while (s.length()) {
		std::wstring strSeg = s.substr(0, 512);
		s.erase(0, 512);
		std::wcout << strSeg.c_str();
		::OutputDebugString(strSeg.c_str());
		::Sleep(1);
	}
}

void Trace(const std::string & str, LPCTSTR lpszFile, ULONG lLine)
{
	Trace(CString(a2w(str).c_str()), lpszFile, lLine);
}

void Trace(const std::wstring & str, LPCTSTR lpszFile, ULONG lLine)
{
	Trace(str.c_str(), lpszFile, lLine);
}

std::wstring a2w(const std::string & str)
{
	std::wstring str2(str.length(), L' ');

	std::copy(str.begin(), str.end(), str2.begin());

	return str2;
}

std::string trim(const std::string & str)
{
	CString s = a2w(str).c_str();
	s.Trim();
	return w2a(wstring((LPCTSTR)s));
}

std::wstring trim(const std::wstring & str)
{
	CString s = str.c_str();
	s.Trim();
	return wstring((LPCTSTR)s);
}

std::string w2a(const std::wstring & str)
{
	std::string str2(str.length(), L' ');

	std::copy(str.begin(), str.end(), str2.begin());

	return str2;
}

CString a2w(const CString & str)
{
	std::string a;// = std::string((LPCSTR)(LPCTSTR)str);

	for (int i = 0; i < str.GetLength(); i++)
		a += (char)str[i];

	std::wstring w = a2w(a);
	return (CString)w.c_str();
}

CString w2a(const CString & str)
{
	std::wstring w = std::wstring((LPCTSTR)str);
	std::string a = w2a(w);
	return (CString)a.c_str();
}
