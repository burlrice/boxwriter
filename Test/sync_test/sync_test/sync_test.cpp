// sync_test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "sync_test.h"
#include <tlhelp32.h>
#include <psapi.h>
#include "DiagnosticSingleLock.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// The one and only application object

CWinApp theApp;

using namespace std;

DIAGNOSTIC_CRITICAL_SECTION (cs1);
DIAGNOSTIC_CRITICAL_SECTION (cs2);
DIAGNOSTIC_CRITICAL_SECTION (cs3);
DIAGNOSTIC_CRITICAL_SECTION (cs4);

std::vector <CDiagnosticCriticalSection *> GetCriticalSections ()
{
	std::vector <CDiagnosticCriticalSection *> v, vRand;

	v.push_back (&::cs1);
	v.push_back (&::cs2);
	v.push_back (&::cs3);
	v.push_back (&::cs4);

	while (v.size ()) {
		int nIndex = rand () % v.size ();
		CDiagnosticCriticalSection * p = v [nIndex];

		v.erase (v.begin () + nIndex);
		vRand.push_back (p);
	}

	return vRand;
}

DWORD CalcSleep ()
{
	return /* ((rand () % 2) * 1000) + */ (rand () % 1000);
}

LRESULT TestThread (LPVOID lpParam)
{
	CString str;
	const DWORD dwThreadID = ::GetCurrentThreadId ();
	int nThread = (int)lpParam;

	str.Format (_T ("TestThread:%d(%d)"), lpParam, dwThreadID);
	CDiagnosticSingleLock::RegisterThreadName (str, dwThreadID);

	srand (time (NULL) + (UINT)lpParam);

	while (1) {
		const DWORD dwSleep = CalcSleep ();

		{
			std::vector <CDiagnosticCriticalSection *> v = GetCriticalSections ();

			LOCK (* v [0]);
			LOCK (* v [1]);
			LOCK (* v [2]);
			LOCK (* v [3]);

			str.Format (_T ("%s(%d): thread:%d %d: sleep: %d\n"), _T (__FILE__), __LINE__, nThread, dwThreadID, dwSleep);
		
			for (int i = 0; i < str.GetLength (); i++) {
				//::OutputDebugString (CString (str [i]));
			
				if (((i + 1) % 5) == 0)
					::Sleep (1);

				putchar ((int)str [i]);
			}
		}

		::Sleep (dwSleep);
	}

	return 0;
}

LRESULT DiagnosticSyncThread (LPVOID lpParam)
{
	const int nSleepSeconds = 15;
	CTime tmLast = CTime::GetCurrentTime ();

	while (1) {
		::Sleep (nSleepSeconds * 1000);

		HANDLE h = ::CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
		DWORD dwProcessID = GetCurrentProcessId ();
		CStringArray vThreads;

		if (h != INVALID_HANDLE_VALUE) {
			THREADENTRY32 te;
			te.dwSize = sizeof(te);

			if (::Thread32First(h, &te)) {
				do {
					if (te.dwSize >= FIELD_OFFSET(THREADENTRY32, th32OwnerProcessID) + sizeof(te.th32OwnerProcessID)) {
						if (te.th32OwnerProcessID == dwProcessID) 
							vThreads.Add ((CString)std::to_string ((__int64)te.th32ThreadID).c_str ());
					}

					te.dwSize = sizeof(te);
				} 
				while (::Thread32Next(h, &te));
			}

			::CloseHandle(h);
		}

		//CTimeSpan tm = tmLast - CDiagnosticSingleLock::GetWatchdog ();

		//CCriticalSection c;
		//LOCK (c);

		TRACEF (_T ("[BW] [sync] active threads: ") + implode (vThreads));
		CString str = CDiagnosticSingleLock::TraceAll (_T (__FILE__), __LINE__, false);

		TRACEF (str);

		str.Format (_T ("cs1: 0x%p"), &::cs1);	TRACEF (str);
		str.Format (_T ("cs2: 0x%p"), &::cs2);	TRACEF (str);
		str.Format (_T ("cs3: 0x%p"), &::cs3);	TRACEF (str);
		str.Format (_T ("cs4: 0x%p"), &::cs4);	TRACEF (str);

		if (str.Find (_T ("deadlock")) != -1) {
			//::OutputDebugString (str);
			//int n = 0;
		}

		//if (tm.GetTotalSeconds () > nSleepSeconds) {
		//	TRACEF (tm.Format (_T ("[BW] [sync] ********** dead: %H:%M:%S")));
		//	TRACEF (str);
		//}

		tmLast = CTime::GetCurrentTime ();
	}

	return 0;
}

DWORD Find (const CString & strExe)
{
	DWORD dwResult = 0, dwProcesses[1024] = { 0 }, dwNeeded = 0;

    if (::EnumProcesses (dwProcesses, sizeof(dwProcesses), &dwNeeded)) {
	    int n = dwNeeded / sizeof(DWORD);

		for (int i = 0; dwResult == 0 && i < n; i++) {
			if (HANDLE hProcess = ::OpenProcess (PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, dwProcesses [i])) {
				HMODULE hModule = NULL;

				if (::EnumProcessModules (hProcess, &hModule, sizeof(hModule), &dwNeeded)) {
					TCHAR sz [MAX_PATH] = { 0 };

					::GetModuleBaseName (hProcess, hModule, sz, sizeof(sz) / sizeof(TCHAR));
					CString str = sz;

					if (!str.CompareNoCase (strExe))
						dwResult = dwProcesses [i];
				}

				::CloseHandle (hProcess);
			}
		}
	}

	return dwResult;
}

std::map <DWORD, __int64> GetThreadTimes (DWORD dwProcessID) 
{
	std::map <DWORD, __int64> result;

	HANDLE hSnapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	
	if (hSnapshot != INVALID_HANDLE_VALUE) {
		THREADENTRY32 te;
		te.dwSize = sizeof(te);

		if (::Thread32First(hSnapshot, &te)) {
			do {
				if (te.dwSize >= FIELD_OFFSET(THREADENTRY32, th32OwnerProcessID) + sizeof(te.th32OwnerProcessID)) {
					if (te.th32OwnerProcessID == dwProcessID) {
						if (HANDLE hThread = ::OpenThread (THREAD_QUERY_INFORMATION, FALSE, te.th32ThreadID)) {
							FILETIME ftCreate = { 0 }, ftExit = { 0 }, ftKernel = { 0 }, ftUser = { 0 };

							if (::GetThreadTimes (hThread, &ftCreate, &ftExit, &ftKernel, &ftUser)) {
								//CString str;
								//const ULONGLONG & lCreate = * reinterpret_cast<const ULONGLONG*>(&ftCreate);
								//const ULONGLONG & lExit = * reinterpret_cast<const ULONGLONG*>(&ftExit);
								const ULONGLONG & lKernel = * reinterpret_cast<const ULONGLONG*>(&ftKernel);
								const ULONGLONG & lUser = * reinterpret_cast<const ULONGLONG*>(&ftUser);

								result [te.th32ThreadID] = (__int64)(lKernel + lUser) / 10000.0;

								//str.Format (_T ("%6d: create: %s, exit: %6.3f, kernel: %6.3f, user: %6.3f"),
								//	te.th32ThreadID,
								//	CTime (ftCreate). Format (_T ("%c")), //lCreate/ 10000.0,
								//	lExit / 10000.0,
								//	lKernel / 10000.0,
								//	lUser / 10000.0);
								//TRACEF (str);
							}

							::CloseHandle (hThread);
						}
					}
				}

				te.dwSize = sizeof(te);
			} 
			while (::Thread32Next(hSnapshot, &te));
		}

		::CloseHandle(hSnapshot);
	}

	return result;
}

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	HMODULE hModule = ::GetModuleHandle(NULL);

	if (hModule != NULL)
	{
		// initialize MFC and print and error on failure
		if (!AfxWinInit(hModule, NULL, ::GetCommandLine(), 0))
		{
			// TODO: change error code to suit your needs
			_tprintf(_T("Fatal Error: MFC initialization failed\n"));
			nRetCode = 1;
		}
		else
		{
			// TODO: code your application's behavior here.
		}
	}
	else
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: GetModuleHandle failed\n"));
		nRetCode = 1;
	}

	DWORD dwProcessID = Find (_T ("Control.exe"));

	if (dwProcessID) {
		std::map <DWORD, __int64> mapLastTimes;

		while (1) {
			std::map <DWORD, __int64> mapTimes = GetThreadTimes (dwProcessID);

			for (std::map <DWORD, __int64>::iterator i = mapTimes.begin (); i != mapTimes.end (); i++) {
				if (mapLastTimes [i->first] != i->second) {
					CString str;

					str.Format (_T ("%6d: %lld"), i->first, i->second);
					TRACEF (str);
				}
			}

			TRACEF ("");
			mapLastTimes = mapTimes;
			::Sleep (5000);
		}
	}

	{
		HANDLE hTrace = ::OpenEvent (EVENT_ALL_ACCESS, FALSE, _T ("Foxjet::CDiagnosticDataThread::Trace"));
		::SetEvent (hTrace);
		::CloseHandle (hTrace);
		return 0;
	}

	srand (time (NULL));

	CDiagnosticSingleLock::Init (5);

	for (int i = 0; i < 2; i++) {
		DWORD dw = 0;

		::CreateThread ((LPSECURITY_ATTRIBUTES)NULL, 0, (LPTHREAD_START_ROUTINE)TestThread, (LPVOID)i, 0, &dw);
		::Sleep (rand () % 1000);
	}

	//{
	//	DWORD dw = 0;
	//	CDiagnosticSingleLock::RegisterThreadName (_T ("main application thread"), ::GetCurrentThreadId ());

	//	::CreateThread ((LPSECURITY_ATTRIBUTES)NULL, 0, (LPTHREAD_START_ROUTINE)DiagnosticSyncThread, (LPVOID)0, 0, &dw);
	//}

	while (1) {
		const DWORD dwSleep = CalcSleep ();

		{
			std::vector <CDiagnosticCriticalSection *> v = GetCriticalSections ();

			LOCK (* v [0]);
			LOCK (* v [1]);
			LOCK (* v [2]);
			LOCK (* v [3]);

			CString str;

			str.Format (_T ("%s(%d): main thread %d: sleep: %d\n"), _T (__FILE__), __LINE__, ::GetCurrentThreadId (), dwSleep);

			for (int i = 0; i < str.GetLength (); i++) {
				//::OutputDebugString (CString (str [i]));
			
				if (((i + 1) % 5) == 0)
					::Sleep (1);

				putchar ((int)str [i]);
			}
		}

		::Sleep (dwSleep);
	}

	return nRetCode;
}
