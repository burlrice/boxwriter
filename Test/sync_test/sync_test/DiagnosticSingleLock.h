#pragma once
#include <afxmt.h>
#include <map>
#include <vector>

#define CONCAT_IMPL( x, y ) x##y
#define MACRO_CONCAT( x, y ) CONCAT_IMPL( x, y )

#define DIAGNOSTIC_CRITICAL_SECTION(s) CDiagnosticCriticalSection s (_T (#s))
#define LOCK(obj) CDiagnosticSingleLock MACRO_CONCAT ( $_lock, __COUNTER__ ) (&(obj), TRUE, _T (__FILE__), __LINE__, _T (#obj))

class CDiagnosticCriticalSection : public CCriticalSection
{
public:
	CDiagnosticCriticalSection (const CString & strName = _T (""));
	virtual ~CDiagnosticCriticalSection ();

	HANDLE m_hMutex;
};

class CDiagnosticSingleLock : public CSingleLock
{
public:
	explicit CDiagnosticSingleLock (CDiagnosticCriticalSection* pObject, BOOL bInitialLock = FALSE, LPCTSTR lpszFile = NULL, ULONG lLine = 0, LPCTSTR lpszName = NULL);
	virtual ~CDiagnosticSingleLock ();

	BOOL Lock (DWORD dwTimeOut = INFINITE);
	BOOL Unlock();
	BOOL Unlock(LONG lCount, LPLONG lPrevCount = NULL);
	BOOL IsLocked ();

	void SchedulePush ();
	void Push ();
	void SchedulePop ();
	void Pop ();

	static void Init (int nInvertvalSeconds = 60);
	static void RegisterThreadName (const CString & strName, DWORD dwThreadID);

	static CString TraceAll (LPCTSTR lpszFile, ULONG lLine, bool bShowCallstack = false);

protected:
	CDiagnosticCriticalSection * Init (CDiagnosticCriticalSection* pObject, BOOL bInitialLock, LPCTSTR lpszFile, ULONG lLine, LPCTSTR lpszName);

	CDiagnosticCriticalSection *	m_pObject;
	const CString					m_strFile;
	const CString					m_strName;
	const ULONG						m_lLine;
	DWORD							m_dwThreadID;
	bool							m_bLocked;
};

