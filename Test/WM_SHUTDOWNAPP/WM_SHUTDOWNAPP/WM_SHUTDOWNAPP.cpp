// WM_SHUTDOWNAPP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "WM_SHUTDOWNAPP.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// The one and only application object

CWinApp theApp;

using namespace std;

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	HMODULE hModule = ::GetModuleHandle(NULL);

	if (hModule != NULL)
	{
		// initialize MFC and print and error on failure
		if (!AfxWinInit(hModule, NULL, ::GetCommandLine(), 0))
		{
			// TODO: change error code to suit your needs
			_tprintf(_T("Fatal Error: MFC initialization failed\n"));
			return 1;
		}
		else
		{
			// TODO: code your application's behavior here.
		}
	}
	else
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: GetModuleHandle failed\n"));
		return 1;
	}

	typedef enum 
	{
		SHUTDOWN_EDITOR		= 0x0001,
		SHUTDOWN_CONTROL	= 0x0002,
		SHUTDOWN_CONFIG		= 0x0004,
		SHUTDOWN_HOST		= 0x0008,
		SHUTDOWN_ALL		= 0xFFFF,
	} SHUTDOWN_TYPE;

	#define SHUTDOWNAPP							(_T ("Foxjet::Shut down app"))
	const UINT WM_SHUTDOWNAPP					= ::RegisterWindowMessage (SHUTDOWNAPP);

	DWORD dwResult = 0;
	LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_SHUTDOWNAPP, SHUTDOWN_ALL, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 10000, &dwResult);

	return nRetCode;
}
