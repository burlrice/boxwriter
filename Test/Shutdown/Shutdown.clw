; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CShutdownDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Shutdown.h"

ClassCount=4
Class1=CShutdownApp
Class2=CShutdownDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_SHUTDOWN_DIALOG

[CLS:CShutdownApp]
Type=0
HeaderFile=Shutdown.h
ImplementationFile=Shutdown.cpp
Filter=N

[CLS:CShutdownDlg]
Type=0
HeaderFile=ShutdownDlg.h
ImplementationFile=ShutdownDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=ShutdownDlg.h
ImplementationFile=ShutdownDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_SHUTDOWN_DIALOG]
Type=1
Class=CShutdownDlg
ControlCount=3
Control1=LV_LOG,SysListView32,1350631437
Control2=BTN_START,button,1342242816
Control3=BTN_STOP,button,1342242816

