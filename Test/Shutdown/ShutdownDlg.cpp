// ShutdownDlg.cpp : implementation file
//

#include "stdafx.h"
#include <afxmt.h>
#include "Shutdown.h"
#include "ShutdownDlg.h"
#include "..\..\Editor\DLL\Common\WinMsg.h"

static void Trace (const CString & str, LPCTSTR lpszFile, ULONG lLine)
{
	CString strDebug;

	strDebug.Format ("%s(%d): %s\n", lpszFile, lLine, str);

	::OutputDebugString (strDebug);
}

#define TRACEF(str) Trace (str, _T (__FILE__), __LINE__)

#define TM_KILLTHREAD (WM_USER+1)

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CShutdownDlg dialog

CShutdownDlg::CShutdownDlg(CWnd* pParent /*=NULL*/)
:	m_hThread (NULL),
	m_dwThreadID (0),
	CDialog(CShutdownDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CShutdownDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	srand (time (NULL));
}

void CShutdownDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CShutdownDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CShutdownDlg, CDialog)
	//{{AFX_MSG_MAP(CShutdownDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_START, OnStart)
	ON_BN_CLICKED(BTN_STOP, OnStop)
	ON_WM_CLOSE()
	ON_NOTIFY(LVN_COLUMNCLICK, LV_LOG, OnColumnclickLog)
	//}}AFX_MSG_MAP

	ON_REGISTERED_MESSAGE (WM_APP_INITIALIZED, OnAppInit)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CShutdownDlg message handlers

BOOL CShutdownDlg::OnInitDialog()
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_LOG);

	CDialog::OnInitDialog();

	ASSERT (pLV);

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	GetDlgItem (BTN_STOP)->EnableWindow (FALSE);

	pLV->InsertColumn (0, "Start",		LVCFMT_LEFT, 200);
	pLV->InsertColumn (1, "Stop",		LVCFMT_LEFT, 200);
	pLV->InsertColumn (2, "Exit code",	LVCFMT_LEFT, 80);
	pLV->InsertColumn (3, "Task ID",	LVCFMT_LEFT, 80);
	pLV->SetExtendedStyle (LVS_EX_FULLROWSELECT);

	::SetWindowPos (m_hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CShutdownDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CShutdownDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CShutdownDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

ULONG CALLBACK CShutdownDlg::ThreadFunc (LPVOID lpData)
{
	MSG msg;
	CTime tmStart, tmStop;
	HANDLE hControl = NULL;
	CShutdownDlg & dlg = * (CShutdownDlg *)lpData;

	srand (time (NULL));

	while (1) {
		::Sleep (0);

		if (::PeekMessage (&msg, NULL, WM_USER, 0x7FFF, PM_NOREMOVE)) {
			::GetMessage (&msg, NULL, WM_USER, 0x7FFF);

			switch (msg.message) {
			case TM_KILLTHREAD:
				if (HANDLE h = (HANDLE)msg.wParam)
					::SetEvent (h);

				TRACEF ("end thread");
				//::AfxEndThread (0);
				return 0;
			}
		}

		if (HANDLE hProcess = CShutdownDlg::Launch (dlg)) {
			CString str = _T ("Control::WM_APP_INITIALIZED");

			if (CListCtrl * p = (CListCtrl *)dlg.GetDlgItem (LV_LOG)) {
				int nIndex = p->GetItemCount ();

				p->InsertItem (nIndex, CTime::GetCurrentTime ().Format (_T ("%c")));
				p->SetItemData (nIndex, nIndex);
				p->EnsureVisible (nIndex, FALSE);
				::Sleep (1);

				if (HANDLE hEvent = ::CreateEvent (NULL, TRUE, FALSE, str)) {
					DWORD dwExitCode = 0;
					bool bMore = true;

					if (::WaitForSingleObject (hEvent, 20000) != WAIT_OBJECT_0) {
						TRACEF (_T ("WaitForSingleObject failed: ") + str);
						return 0;
					}

					::CloseHandle (hEvent);
					::Sleep (1);

					while (bMore) {
						//DWORD dwResult = 0;
						//LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_SHUTDOWNAPP, SHUTDOWN_ALL, 0,  SMTO_NORMAL | SMTO_ABORTIFHUNG, 1, &dwResult);
						//TRACEF ("WM_SHUTDOWNAPP");
						
						if (::GetExitCodeProcess (hProcess, &dwExitCode)) {
							for (int i = 0; i < 10; i++) {
								if (dwExitCode == STILL_ACTIVE) 
									::WaitForSingleObjectEx (hProcess, 100, TRUE);
								else {
									CString str;
									const CString strFile = _T ("C:\\Foxjet\\shutdown.log");

									str.Format (_T ("%d"), dwExitCode);
									p->SetItemText (nIndex, 1, CTime::GetCurrentTime ().Format (_T ("%c")));
									p->SetItemText (nIndex, 2, str);
									p->EnsureVisible (nIndex, FALSE);
									::Sleep (0);

									if (::GetFileAttributes (strFile) == -1) {
										if (FILE * fp = _tfopen (strFile, _T ("a"))) {
											CString strWrite = _T ("Start\t\t\tStop\t\t\tExit code\tTask ID\r\n");

											fwrite ((LPVOID)(LPCTSTR)strWrite, strWrite.GetLength (), 1, fp);
											fclose (fp);
										}
									}

									if (FILE * fp = _tfopen (strFile, _T ("a"))) {
										CString strWrite;

										strWrite += p->GetItemText (nIndex, 0);
										strWrite += _T ("\t") + p->GetItemText (nIndex, 1);
										strWrite += _T ("\t") + p->GetItemText (nIndex, 2);
										strWrite += _T ("\t\t") + p->GetItemText (nIndex, 3);
										strWrite += _T ("\r\n");

										//TRACEF (strWrite);

										fwrite ((LPVOID)(LPCTSTR)strWrite, strWrite.GetLength (), 1, fp);
										fclose (fp);
									}

									bMore = false;
									break;
								}
							}
						}
					}
				}
			}
		}

		::Sleep (3000 + (rand () % 5000));
	}

	return 0;
}

HANDLE CShutdownDlg::Launch (CShutdownDlg & dlg) 
{
	CString strDir = _T ("C:\\Foxjet");
	CString strApp = strDir + _T ("\\Control.exe");
	CString strCmdLine = strApp + _T (" /SHUTDOWNTEST /PRINTCYCLE /demo /dsn=MarksmanELITE /netstat /highres /user=root /pass=user");
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [MAX_PATH];

	_tcsncpy (szCmdLine, strCmdLine, MAX_PATH);
	memset (&pi, 0, sizeof(pi));
	memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	TRACEF (szCmdLine);

	BOOL bResult = ::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, strDir, &si, &pi); 

	return bResult ? pi.hProcess : NULL;
}

void CShutdownDlg::OnStart() 
{
	if (m_hThread == NULL) {
		GetDlgItem (BTN_START)->EnableWindow (FALSE);
		GetDlgItem (BTN_STOP)->EnableWindow (TRUE);

		VERIFY (m_hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)ThreadFunc, this, 0, &m_dwThreadID));
	}
}

void CShutdownDlg::OnStop() 
{
	if (m_hThread) {
		CEvent event (false, true, _T ("TM_KILLTHREAD"), NULL);

		while (!::PostThreadMessage (m_dwThreadID, TM_KILLTHREAD, (WPARAM)(HANDLE)event, 0))
			::Sleep (0);

		if (::WaitForSingleObject (event, INFINITE) == WAIT_OBJECT_0) {
			m_dwThreadID = 0;
			m_hThread = NULL;

			GetDlgItem (BTN_START)->EnableWindow (TRUE);
			GetDlgItem (BTN_STOP)->EnableWindow (FALSE);
		}
	}
}

void CShutdownDlg::OnClose() 
{
	OnStop ();	
	CDialog::OnClose();
}

int CALLBACK CShutdownDlg::CompareFunc (LPARAM lParam1, LPARAM lParam2, LPARAM lParam3)
{
	if (SORTSTRUCT * p = (SORTSTRUCT *)lParam3) {
		CString str1 = p->m_pLV->GetItemText(lParam1, p->m_nCol);
		CString str2 = p->m_pLV->GetItemText(lParam2, p->m_nCol);

		switch (p->m_nCol) {
		case 0:
		case 1:
			COleDateTime tm1, tm2;

			VERIFY (tm1.ParseDateTime (str1));
			VERIFY (tm2.ParseDateTime (str2));

			COleDateTimeSpan tm = tm2 - tm1;

			return (int)((!p->m_bReverseOrder ? tm.GetTotalSeconds () : -tm.GetTotalSeconds ()) * 1000.0);
		}

		if (!p->m_bReverseOrder)
			return str1.Compare (str2);
		else 
			return str2.Compare (str1);
	}

	return 0;
}

void CShutdownDlg::OnColumnclickLog(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW * pnmlv = (NM_LISTVIEW*)pNMHDR;
	CListCtrl & lv = * (CListCtrl *)GetDlgItem (LV_LOG);
	
	// "pnmlv->iSubItem" is the column to sort on
	int nCol = pnmlv->iSubItem;

	if (nCol == m_sort.m_nLastCol)
		// if the user clicks the same column several times in a row,
		// flip-flop the sort order
		m_sort.m_bReverseOrder = !m_sort.m_bReverseOrder;
	else
		// otherwise, normal sort order
		m_sort.m_bReverseOrder = false;

	SORTSTRUCT s;

	s.m_nCol = nCol;
	s.m_pLV = &lv;
	s.m_bReverseOrder = m_sort.m_bReverseOrder;

	lv.SortItems (CompareFunc, (DWORD)&s);
	m_sort.m_nLastCol = pnmlv->iSubItem;

	*pResult = 0;
}

typedef struct
{
	CShutdownDlg *	m_pDlg;
	HWND			m_hWnd;
} KILLSTRUCT;

ULONG CALLBACK CShutdownDlg::KillFunc (LPVOID lpData)
{
	srand (time (NULL));

	if (KILLSTRUCT * p = (KILLSTRUCT *)lpData) {
		CShutdownDlg & dlg = * p->m_pDlg;
	
		if (CListCtrl * pItem = (CListCtrl *)dlg.GetDlgItem (LV_LOG)) {
			int nIndex = pItem->GetItemCount () - 1;
			CString str;
			DWORD dwSleep = 10000 + (rand () % 10000);

			str.Format (_T ("WM_APP_INITIALIZED [%d]"), dwSleep);
			pItem->SetItemText (nIndex, 1, str);
			::Sleep (dwSleep);
			pItem->SetItemText (nIndex, 1, _T ("sent ID_APP_EXIT"));
			::PostMessage (p->m_hWnd, WM_COMMAND, ID_APP_EXIT, 0);
		}
		
		delete p;
	}

	return 0;
}

LRESULT CShutdownDlg::OnAppInit (WPARAM wParam, LPARAM lParam)
{
	if (HWND hwnd = (HWND)wParam) {
		DWORD dw = 0;
		KILLSTRUCT * p = new KILLSTRUCT;

		p->m_hWnd = hwnd;
		p->m_pDlg = this;
		VERIFY (m_hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)KillFunc, (LPVOID)p, 0, &dw));

		if (CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_LOG)) {
			int nIndex = pLV->GetItemCount () - 1;
			CString str;

			str.Format (_T ("%d"), lParam);
			pLV->SetItemText (nIndex, 3, str);
		}
	}

	return 0;
}