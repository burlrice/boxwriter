// ShutdownDlg.h : header file
//

#if !defined(AFX_SHUTDOWNDLG_H__2055F0B0_39DC_4F95_9FB5_E61CF8FA558B__INCLUDED_)
#define AFX_SHUTDOWNDLG_H__2055F0B0_39DC_4F95_9FB5_E61CF8FA558B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CShutdownDlg dialog

class CShutdownDlg : public CDialog
{
// Construction
public:
	CShutdownDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CShutdownDlg)
	enum { IDD = IDD_SHUTDOWN_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShutdownDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	class CSortInfo 
	{
	public:
		CSortInfo () : m_nLastCol (-1), m_bReverseOrder (false) {}

		int m_nLastCol;
		bool m_bReverseOrder;
	};

	typedef struct
	{
		int m_nCol;
		CListCtrl * m_pLV;
		bool m_bReverseOrder;
	} SORTSTRUCT;

	static int CALLBACK CompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParam3);

	HICON m_hIcon;
	HANDLE m_hThread;
	DWORD m_dwThreadID;
	CSortInfo m_sort;

	static ULONG CALLBACK ThreadFunc (LPVOID lpData);
	static HANDLE Launch (CShutdownDlg & dlg); 
	static ULONG CALLBACK KillFunc (LPVOID lpData);

	afx_msg LRESULT OnAppInit (WPARAM wParam, LPARAM lParam);

	// Generated message map functions
	//{{AFX_MSG(CShutdownDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnStart();
	afx_msg void OnStop();
	afx_msg void OnClose();
	afx_msg void OnColumnclickLog(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHUTDOWNDLG_H__2055F0B0_39DC_4F95_9FB5_E61CF8FA558B__INCLUDED_)
