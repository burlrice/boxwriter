// Shutdown.h : main header file for the SHUTDOWN application
//

#if !defined(AFX_SHUTDOWN_H__102B8538_5819_44E0_BAFB_3C420FC42FAC__INCLUDED_)
#define AFX_SHUTDOWN_H__102B8538_5819_44E0_BAFB_3C420FC42FAC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CShutdownApp:
// See Shutdown.cpp for the implementation of this class
//

class CShutdownApp : public CWinApp
{
public:
	CShutdownApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShutdownApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CShutdownApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHUTDOWN_H__102B8538_5819_44E0_BAFB_3C420FC42FAC__INCLUDED_)
