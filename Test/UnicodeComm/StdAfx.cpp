// stdafx.cpp : source file that includes just the standard includes
//	UnicodeComm.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include "UnicodeComm.h"
#include "UnicodeCommDlg.h"

extern CUnicodeCommApp theApp;

void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	CString str;

	str.Format (_T ("%s(%d): %s\n"), lpszFile, lLine, lpsz);
	::OutputDebugString (str);


	if (theApp.m_pMainWnd) {
		if (theApp.m_pMainWnd->IsWindowVisible ()) {
			CString strOutput;

			theApp.m_pMainWnd->GetDlgItemText (TXT_OUTPUT, strOutput);
			strOutput += lpsz;
			strOutput += _T ("\r\n");
			theApp.m_pMainWnd->SetDlgItemText (TXT_OUTPUT, strOutput);

			if (CEdit * p = (CEdit *)theApp.m_pMainWnd->GetDlgItem (TXT_OUTPUT)) {
				p->LineScroll (p->GetLineCount ());
			}
		}
	}
}


