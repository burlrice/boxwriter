// UnicodeComm.h : main header file for the UNICODECOMM application
//

#if !defined(AFX_UNICODECOMM_H__36C66897_2B97_43C9_A65B_883EAD6C9B11__INCLUDED_)
#define AFX_UNICODECOMM_H__36C66897_2B97_43C9_A65B_883EAD6C9B11__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CUnicodeCommApp:
// See UnicodeComm.cpp for the implementation of this class
//

class CUnicodeCommApp : public CWinApp
{
public:
	CUnicodeCommApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUnicodeCommApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CUnicodeCommApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UNICODECOMM_H__36C66897_2B97_43C9_A65B_883EAD6C9B11__INCLUDED_)
