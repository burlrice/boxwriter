// UnicodeCommDlg.h : header file
//

#if !defined(AFX_UNICODECOMMDLG_H__E8A57CDB_C3E5_4176_A5DC_D5E8CF97D4C1__INCLUDED_)
#define AFX_UNICODECOMMDLG_H__E8A57CDB_C3E5_4176_A5DC_D5E8CF97D4C1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CUnicodeCommDlg dialog

class CUnicodeCommDlg : public CDialog
{
// Construction
public:
	CUnicodeCommDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CUnicodeCommDlg)
	enum { IDD = IDD_UNICODECOMM_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUnicodeCommDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CUnicodeCommDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTest();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UNICODECOMMDLG_H__E8A57CDB_C3E5_4176_A5DC_D5E8CF97D4C1__INCLUDED_)
