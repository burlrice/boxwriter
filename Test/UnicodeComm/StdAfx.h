// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__B1BF1FC6_A3DD_4939_9B43_69A8D4AE8C20__INCLUDED_)
#define AFX_STDAFX_H__B1BF1FC6_A3DD_4939_9B43_69A8D4AE8C20__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#define ARRAYSIZE(s) (sizeof ((s)) / sizeof ((s) [0]))

inline CString FormatMessage (DWORD dwError)
{
	LPVOID lpMsgBuf = NULL;
	CString str;

	str.Format (_T ("[%d: 0x%p] "), dwError, dwError);

	::FormatMessage (
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, dwError, MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT), 
		(LPTSTR)&lpMsgBuf, 0, NULL);
	str += (LPCTSTR)lpMsgBuf;
	::LocalFree( lpMsgBuf );

	return str;
}

void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine);

#define TRACEF(s) Trace ((s), __FILE__, __LINE__)

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__B1BF1FC6_A3DD_4939_9B43_69A8D4AE8C20__INCLUDED_)
