// UnicodeCommDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UnicodeComm.h"
#include "UnicodeCommDlg.h"
#include "Comm32.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CUnicodeCommApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CUnicodeCommDlg dialog

CUnicodeCommDlg::CUnicodeCommDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUnicodeCommDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUnicodeCommDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CUnicodeCommDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUnicodeCommDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CUnicodeCommDlg, CDialog)
	//{{AFX_MSG_MAP(CUnicodeCommDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_TEST, OnTest)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUnicodeCommDlg message handlers

BOOL CUnicodeCommDlg::OnInitDialog()
{
	CUIntArray v;

	EnumerateSerialPorts (v);

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_PORT)) {
		for (int i = 0; i < v.GetSize (); i++) {
			CString str;
			DWORD dw = v [i];

			str.Format (_T ("COM%d"), dw);
			int nIndex = p->AddString (str);
			p->SetItemData (nIndex, dw);

			if (theApp.GetProfileInt (_T ("Settings"), _T ("COM"), -1) == dw)
				p->SetCurSel (nIndex);
		}

		if (p->GetCurSel () == CB_ERR)
			p->SetCurSel (0);
	}

	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CUnicodeCommDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CUnicodeCommDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CUnicodeCommDlg::OnTest() 
{
	BeginWaitCursor ();

	if (CEdit * p = (CEdit *)GetDlgItem (TXT_OUTPUT)) 
		p->SetRedraw (FALSE);

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_PORT)) {
		DWORD dwPort = p->GetItemData (p->GetCurSel ());

		theApp.WriteProfileInt (_T ("Settings"), _T ("COM"), dwPort);

		if (HANDLE h = Open_Comport (dwPort, CBR_9600, 8, NOPARITY, ONESTOPBIT)) {
			UCHAR sz [256] = { 0 };

			for (int i = 0; i < ARRAYSIZE (sz); i++)
				sz [i] = (char)i;

			for (i = 0; i < ARRAYSIZE (sz); i++) {
				CString str, strChar; 
				UCHAR c = sz [i];

				strChar.Format (_T ("%c"), c);
				str.Format (_T ("sz [%d]: %s [%d]"), i, strChar, c);
				TRACEF (str);
				::Sleep (0);
			}

			Write_Comport (h, ARRAYSIZE (sz), sz);
			Write_Comport (h, 1, _T ("\r"));
			Close_Comport (h);
		}
	}

	if (CEdit * p = (CEdit *)GetDlgItem (TXT_OUTPUT)) 
		p->SetRedraw (TRUE);

	EndWaitCursor ();
}
