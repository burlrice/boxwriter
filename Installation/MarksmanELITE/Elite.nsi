;---------------------
;Include Modern UI

	!include "MUI.nsh"

;--------------------------------

!macro BIMAGE IMAGE PARMS
	Call repositionWindow
	Push $0
	GetTempFileName $0
	File /oname=$0 "${IMAGE}"
	SetBrandingImage ${PARMS} $0
	Delete $0
	Pop $0
!macroend

;--------------------------------
;Pages

	Page custom CustomPageBrandingImage
	!insertmacro MUI_PAGE_LICENSE "Eula.txt"
	!insertmacro MUI_PAGE_COMPONENTS
	Page custom CustomPageCmdLine
	!insertmacro MUI_PAGE_DIRECTORY
	!insertmacro MUI_PAGE_INSTFILES

	!insertmacro MUI_UNPAGE_CONFIRM
	!insertmacro MUI_UNPAGE_INSTFILES
	!insertmacro MUI_PAGE_FINISH

;--------------------------------
;Languages
 
	!insertmacro MUI_LANGUAGE "English"
	!insertmacro MUI_LANGUAGE "Spanish"
	!insertmacro MUI_LANGUAGE "Portuguese"
	!insertmacro MUI_LANGUAGE "Dutch"
	!insertmacro MUI_LANGUAGE "Hungarian"

;--------------------------------
;language strings

	LangString strLicense			${LANG_ENGLISH} "Please Read Carefully."
	LangString strApplication		${LANG_ENGLISH} "Please choose a BoxWriter application to install."
	LangString strOverwriteDB		${LANG_ENGLISH} "A database from a previous installation was found.  Do you want to keep the old database?"
	LangString strUserPass			${LANG_ENGLISH} "Your new username and password for the Print Control application is: ADMIN, FOXJET"
	LangString strDesktop			${LANG_ENGLISH} "Desktop shortcut"
	LangString strStartMenu			${LANG_ENGLISH} "Startup menu shortcut"
	LangString strKeyboard			${LANG_ENGLISH} "Keyboard"
	LangString strKbDesktop			${LANG_ENGLISH} "Keyboard desktop shortcut"
	LangString strKbStartMenu		${LANG_ENGLISH} "Keyboard startup menu shortcut"
	LangString strUninstallText		${LANG_ENGLISH} "This will uninstall the BoxWriter software. Hit next to continue."
	LangString strInstallDir		${LANG_ENGLISH} "Please choose install directory."
	LangString strUpdateDriver		${LANG_ENGLISH} "Update the driver?  You will have to reboot before the changes take effect."
	LangString strFonts				${LANG_ENGLISH} "Fonts"
	LangString strCmdLineArgs		${LANG_ENGLISH} "Command line arguments"
	LangString strCmdLineArgsEx		${LANG_ENGLISH} "Example: /demo /no_keyboard"
	LangString strSecBoxWriter		${LANG_ENGLISH} "Print Control and Editor applications"
	LangString strSecFonts			${LANG_ENGLISH} "TrueType MK fonts"
	LangString strCleanRegistry		${LANG_ENGLISH} "Delete registry settings"
	
	LangString strLicense			${LANG_SPANISH} "Por favor Lea con Cuidado."
	LangString strApplication		${LANG_SPANISH} "Escoja por favor una aplicaci�n de BoxWriter para instalar. "
	LangString strOverwriteDB		${LANG_SPANISH} "Una base de datos de una instalaci�n previa se encontr�. �Quiere usted mantener la base de datos vieja? "
	LangString strUserPass			${LANG_SPANISH} "Su nombre de usuario y la contrase�a nueva para la aplicaci�n del Control de la Impresi�n son: ADMIN, FOXJET"
	LangString strDesktop			${LANG_SPANISH} "Acceso directo del escritorio"
	LangString strStartMenu			${LANG_SPANISH} "De inicio del men� contextua"
	LangString strKeyboard			${LANG_SPANISH} "Teclado"
	LangString strKbDesktop			${LANG_SPANISH} "M�todo abreviado de teclado de escritorio"
	LangString strKbStartMenu		${LANG_SPANISH} "M�todo abreviado de teclado del men� de inicio"
	LangString strUninstallText		${LANG_SPANISH} "Esto desinstalar� el software de BoxWriter. Golpee Proximo (NEXT) para continuar."
	LangString strInstallDir		${LANG_SPANISH} "Por favor Escoja  Directorio de instalacion."
	LangString strUpdateDriver		${LANG_SPANISH} "�Actualizar el driver? Usted tendr� que reiniciar para que los cambios surten efecto. "
	LangString strFonts				${LANG_SPANISH} "Fuentes"
	LangString strCmdLineArgs		${LANG_SPANISH} "Command line arguments"
	LangString strCmdLineArgsEx		${LANG_SPANISH} "Example: /demo /no_keyboard"
	LangString strSecBoxWriter		${LANG_SPANISH} "Print Control and Editor applications"
	LangString strSecFonts			${LANG_SPANISH} "TrueType MK fonts"
	LangString strCleanRegistry		${LANG_SPANISH} "Eliminar la configuraci�n del Registro"
									
	LangString strLicense			${LANG_PORTUGUESE} "Favor ler atentamente."
	LangString strApplication		${LANG_PORTUGUESE} "Escolha um aplicativo BoxWriter para instalar."
	LangString strOverwriteDB		${LANG_PORTUGUESE} "Um banco de dados de uma instala��o anterior foi encontrado. Voc� deseja manter o banco de dados antigo?"
	LangString strUserPass			${LANG_PORTUGUESE} "O seu nome de usu�rio e sua senha para o aplicativo Print Control �: ADMIN, FOXJET"
	LangString strDesktop			${LANG_PORTUGUESE} "Atalho Desktop"
	LangString strStartMenu			${LANG_PORTUGUESE} "Startup do menu de atalho"
	LangString strKeyboard			${LANG_PORTUGUESE} "Teclado"
	LangString strKbDesktop			${LANG_PORTUGUESE} "Atalho do teclado de desktop"
	LangString strKbStartMenu		${LANG_PORTUGUESE} "Atalho do teclado menu de inicializa��o"
	LangString strUninstallText		${LANG_PORTUGUESE} "Isto ir� desinstalar o software BoxWriter. Clique em avan�ar para continuar."
	LangString strInstallDir		${LANG_PORTUGUESE} "Selecione o diret�rio de instala��o."
	LangString strUpdateDriver		${LANG_PORTUGUESE} "Atualizar o driver? Voc� dever� reinicializar para que as altera��es tenham efeito."
	LangString strFonts				${LANG_PORTUGUESE} "Fontes"
	LangString strCmdLineArgs		${LANG_PORTUGUESE} "Argumentos da linha de comando"
	LangString strCmdLineArgsEx		${LANG_PORTUGUESE} "Exemplo: /demo /no_keyboard"
	LangString strSecBoxWriter		${LANG_PORTUGUESE} "Aplicativos Print Control e Editor"
	LangString strSecFonts			${LANG_PORTUGUESE} "Fontes MK True Type"
	LangString strCleanRegistry		${LANG_PORTUGUESE} "Excluir as configura��es do Registro"

	LangString strLicense			${LANG_DUTCH} "Lees aandachtig."
	LangString strApplication		${LANG_DUTCH} "Kies een BoxWriter applicatie te installeren."
	LangString strOverwriteDB		${LANG_DUTCH} "Een database van een vorige installatie werd gevonden. Wilt u de oude database te houden?"
	LangString strUserPass			${LANG_DUTCH} "Uw nieuwe gebruikersnaam en wachtwoord voor de Print Control toepassing is: ADMIN, FOXJET"
	LangString strDesktop			${LANG_DUTCH} "Snelkoppeling op het bureaublad"
	LangString strStartMenu			${LANG_DUTCH} "Opstartmenu snelkoppeling"
	LangString strKeyboard			${LANG_DUTCH} "Toetsenbord"
	LangString strKbDesktop			${LANG_DUTCH} "Toetsenbord snelkoppeling op het bureaublad"
	LangString strKbStartMenu		${LANG_DUTCH} "Toetsenbord opstartmenu snelkoppeling"
	LangString strUninstallText		${LANG_DUTCH} "Dit zal verwijderen van de BoxWriter software. Hit Volgende om verder te gaan."
	LangString strInstallDir		${LANG_DUTCH} "Kies install directory."
	LangString strUpdateDriver		${LANG_DUTCH} "Bijwerken van de driver? U moet opnieuw opstarten voordat de wijzigingen van kracht worden."
	LangString strFonts				${LANG_DUTCH} "Fonts"
	LangString strCmdLineArgs		${LANG_DUTCH} "Opdrachtregelargumenten"
	LangString strCmdLineArgsEx		${LANG_DUTCH} "Voorbeeld: / demo"
	LangString strSecBoxWriter		${LANG_DUTCH} "Print Control en redacteur toepassingen"
	LangString strSecFonts			${LANG_DUTCH} "TrueType fonts MK"
	LangString strCleanRegistry		${LANG_DUTCH} "Verwijder registerinstellingen "

	LangString strLicense			${LANG_HUNGARIAN} "K�rj�k, olvassa el figyelmesen."
	LangString strApplication		${LANG_HUNGARIAN} "K�rj�k, v�lasszon egy BoxWriter alkalmaz�s telep�t�s�hez."
	LangString strOverwriteDB		${LANG_HUNGARIAN} "Az adatb�zis egy kor�bbi telep�t�s tal�ltak. Szeretn� megtartani a r�gi adatb�zisb�l?"
	LangString strUserPass			${LANG_HUNGARIAN} "Az �j felhaszn�l�nevet �s jelsz�t a Print Control alkalmaz�s: ADMIN, FOXJET"
	LangString strDesktop			${LANG_HUNGARIAN} "Asztali parancsikon"
	LangString strStartMenu			${LANG_HUNGARIAN} "Startup men� parancsikont"
	LangString strKeyboard			${LANG_HUNGARIAN} "Billenty�zet"
	LangString strKbDesktop			${LANG_HUNGARIAN} "Billenty�zet asztali parancsikon"
	LangString strKbStartMenu		${LANG_HUNGARIAN} "Billenty�zet ind�t�si men�j�ben parancsikon"
	LangString strUninstallText		${LANG_HUNGARIAN} "Ez elt�vol�tja a BoxWriter szoftver. Hit mellett folytat�dik."
	LangString strInstallDir		${LANG_HUNGARIAN} "K�rj�k, v�lassza az Install k�nyvt�rat."
	LangString strUpdateDriver		${LANG_HUNGARIAN} "Friss�teni a drivert? Lesz �jra kell ind�tani, miel�tt a m�dos�t�sok �rv�nybe l�ptet�s�hez."
	LangString strFonts				${LANG_HUNGARIAN} "Bet�t�pusok"
	LangString strCmdLineArgs		${LANG_HUNGARIAN} "Parancssori argumentumok"
	LangString strCmdLineArgsEx		${LANG_HUNGARIAN} "P�lda: /demo /no_keyboard"
	LangString strSecBoxWriter		${LANG_HUNGARIAN} "Print Control and Editor alkalmaz�s"
	LangString strSecFonts			${LANG_HUNGARIAN} "TrueType bet�k�szletek MK"
	LangString strCleanRegistry		${LANG_HUNGARIAN} "T�rl�s adatb�zis be�ll�t�sai"

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING
  
;--------------------------------
;Reserve Files
  
  ;These files should be inserted before other files in the data block
  ;Keep these lines before any File command
  ;Only for solid compression (by default, solid compression is enabled for BZIP2 and LZMA)
  
  ReserveFile "cmdline.ini"
  !insertmacro MUI_RESERVEFILE_INSTALLOPTIONS

;--------------------------------
;Variables

	Var "strCmdLine"
	Var "strNewVer"
	Var "strOldVer"
	Var "bShowCmdLine"

;--------------------------------
;General

	Name "BoxWriter ELITE"
	OutFile "BwEliteSetup.exe"

	InstallDir "$PROGRAMFILES\Foxjet\MarksmanELITE"
	ComponentText $(strApplication) 

	BGGradient 000000 000080 FFFFFF
	InstallColors 00FF00 000030
	XPStyle on
	AddBrandingImage top 80

	Icon "mkdraw.ico"
	UninstallIcon "uninstall.ico"

;--------------------------------
;Installer Sections

!include "${NSISDIR}\Examples\System\System.nsh"

Function repositionWindow
	;Save existing register values to the stack
	Push $0
	Push $1
	Push $2
	Push $3
	Push $4
	Push $5
	Push $6
	Push $7
 
;	!define SPI_GETWORKAREA             0x0030
;	!define SWP_NOSIZE                  0x0001
;	!define SWP_NOOWNERZORDER	    0x0200
 
	; Reposition window in the lower left
	; Create RECT struct
	System::Call "*${stRECT} .r1"
	; Find Window info for the window we're displaying
	System::Call "User32::GetWindowRect(i, i) i ($HWNDPARENT, r1) .r2"
	; Get left/top/right/bottom
	System::Call "*$1${stRECT} (.r2, .r3, .r4, .r5)"
 
	; Calculate width/height of our window
	IntOp $2 $4 - $2 ; $2 now contains the width
	IntOp $3 $5 - $3 ; $3 now contains the height
 
	; Determine the screen work area
	System::Call "User32::SystemParametersInfo(i, i, i, i) i (${SPI_GETWORKAREA}, 0, r1, 0) .r4" 
	; Get left/top/right/bottom
	System::Call "*$1${stRECT} (.r4, .r5, .r6, .r7)"
 
	System::Free $1
 
	; Right side of screen - window - 10
	IntOp $0 $6 - $2
	IntOp $0 $0 - 10
	IntOp $0 $0 / 2
	; Left side of screen + 10
	;IntOp $0 $4 + 10
 
	; Bottom of screen - window - 5
	;IntOp $1 $7 - $3
	;IntOp $1 $1 - 5
 
	System::Call "User32::SetWindowPos(i, i, i, i, i, i, i) b ($HWNDPARENT, 0, $0, 100, 0, 0, ${SWP_NOOWNERZORDER}|${SWP_NOSIZE})"

	;Restore register values from the stack
	Pop $7
	Pop $6
	Pop $5
	Pop $4
	Pop $3
	Pop $2
	Pop $1
	Pop $0
FunctionEnd

Section "BoxWriter ELITE" SecBoxWriter
	Delete $INSTDIR\Debug\*.*
	WriteRegDWORD HKCU "Software\FoxJet\BoxWriter ELITE" "InstallerRunning" 0x00000001
	SetOutPath $INSTDIR\Utilities
	File ".\Runtime Files\Utilities\Kill.exe" 
	ExecWait '"$INSTDIR\Utilities\Kill.exe" /lang=$LANGUAGE'
	Sleep 3000
	Delete $INSTDIR\Utilities\Kill.exe

	SetOutPath $INSTDIR\Logos
	File ".\Runtime Files\Logos\*.*"
	WriteUninstaller "uninstall.exe"

	SetOutPath $INSTDIR\Fonts
	File ".\Runtime Files\Fonts\*.*"

	SetShellVarContext All

	SetOutPath $INSTDIR
	File /r ".\Runtime Files\Printer Files\*.*"
	File ".\Runtime Files\Drivers\*.*"

	UnRegDLL "$INSTDIR\Interface.ocx"
	RegDLL "$INSTDIR\Interface.ocx"

;	IfFileExists "$INSTDIR\MarksmanELITE.mdb" 0 OverwriteDatabase
;		MessageBox MB_YESNO|MB_ICONQUESTION $(strOverwriteDB) IDYES CheckDriver
;
;	OverwriteDatabase: 
;
;		File ".\Runtime Files\Database\MarksmanELITE.mdb" 
;		ExecWait '"$INSTDIR\MkDraw.exe" /resetdsn' 
;		MessageBox MB_ICONINFORMATION $(strUserPass)

CheckDriver:
;	GetDllVersion "$SYSDIR\Drivers\FxMphc.sys" $R0 $R1
;	IntOp $R2 $R0 / 0x00010000
;	IntOp $R3 $R0 & 0x0000FFFF
;	IntOp $R4 $R1 / 0x00010000
;	IntOp $R5 $R1 & 0x0000FFFF
;	StrCpy "$strOldVer" "$R2.$R3.$R4.$R5"
;
;	GetDLLVersionLocal ".\Runtime Files\Drivers\FxMphc.sys" $R0 $R1
;	IntOp $R2 $R0 / 0x00010000
;	IntOp $R3 $R0 & 0x0000FFFF
;	IntOp $R4 $R1 / 0x00010000
;	IntOp $R5 $R1 & 0x0000FFFF
;	StrCpy "$strNewVer" "$R2.$R3.$R4.$R5"
;
;	StrCmp $strOldVer $strNewVer CreateShortcuts PromptUpdateDriver
;
;	PromptUpdateDriver:
;		IfFileExists "$SYSDIR\Drivers\FxMphc.sys" 0 UpdateDriver
;			MessageBox MB_YESNO|MB_ICONQUESTION $(strUpdateDriver) IDNO CreateShortcuts
;
;	UpdateDriver:
;			SetOutPath $SYSDIR\Drivers
;			File ".\Runtime Files\Drivers\FxMphc.sys"
;			SetRebootFlag true

CreateShortcuts:
  	CreateDirectory "$STARTMENU\Programs\FoxJet"
	CreateShortCut "$STARTMENU\Programs\FoxJet\Print Control.lnk" $INSTDIR\Control.exe "$strCmdLine" $INSTDIR\Control.exe
	CreateShortCut "$STARTMENU\Programs\FoxJet\Keyboard.lnk" $INSTDIR\Keyboard.exe "$strCmdLine" $INSTDIR\Keyboard.exe
;  	CreateShortCut "$STARTMENU\Programs\FoxJet\BoxWriter Editor.lnk" $INSTDIR\MkDraw.exe "$strCmdLine" $INSTDIR\MkDraw.exe
  	CreateShortCut "$STARTMENU\Programs\FoxJet\UnInstall.lnk" $INSTDIR\Uninstall.exe "" $INSTDIR\UnInstall.exe

	WriteRegDWORD HKCU "Software\FoxJet\BoxWriter ELITE" "InstallerRunning" 0x00000000
SectionEnd

Section /o $(strDesktop) SecDesktopShortCuts
  	CreateShortCut "$DESKTOP\Print Control.lnk" $INSTDIR\Control.exe "$strCmdLine" $INSTDIR\Control.exe
SectionEnd

Section /o $(strStartMenu) SecStartMenuShortCuts
  	CreateShortCut "$SMSTARTUP\Print Control.lnk" $INSTDIR\Control.exe "$strCmdLine" $INSTDIR\Control.exe
SectionEnd

Section /o $(strKeyboard) SecKeyboard
	WriteRegDWORD HKCU "Software\FoxJet\BoxWriter ELITE" "InstallerRunning" 0x00000001
	SetOutPath $INSTDIR\Utilities
	File ".\Runtime Files\Utilities\Kill.exe" 
	ExecWait '"$INSTDIR\Utilities\Kill.exe" /lang=$LANGUAGE'
	Sleep 3000
	Delete $INSTDIR\Utilities\Kill.exe

	WriteUninstaller "uninstall.exe"

	SetShellVarContext All

	SetOutPath $INSTDIR
	File ".\Runtime Files\Printer Files\Database.dll"
	File ".\Runtime Files\Printer Files\Utils.dll"
	File ".\Runtime Files\Printer Files\kb.dll"
	File ".\Runtime Files\Printer Files\KbMonitor.exe"
	File ".\Runtime Files\Printer Files\Keyboard*.*"
	File ".\Runtime Files\Printer Files\mfc*.dll"

CreateShortcuts:
  	CreateDirectory "$STARTMENU\Programs\FoxJet"
	CreateShortCut "$STARTMENU\Programs\FoxJet\Keyboard.lnk" $INSTDIR\Keyboard.exe "$strCmdLine" $INSTDIR\Keyboard.exe
  	CreateShortCut "$STARTMENU\Programs\FoxJet\UnInstall.lnk" $INSTDIR\Uninstall.exe "" $INSTDIR\UnInstall.exe

	WriteRegDWORD HKCU "Software\FoxJet\BoxWriter ELITE" "InstallerRunning" 0x00000000
SectionEnd

Section /o $(strKbDesktop) SecKbDesktopShortCuts
  	CreateShortCut "$DESKTOP\Virtual keyboard.lnk" $INSTDIR\Keyboard.exe "$strCmdLine" $INSTDIR\Keyboard.exe
	Exec '"$INSTDIR\kbMonitor.exe"' 
SectionEnd

Section /o $(strKbStartMenu) SecKbStartupShortCuts
  	CreateShortCut "$SMSTARTUP\Virtual keyboard.lnk" $INSTDIR\Keyboard.exe "$strCmdLine" $INSTDIR\Keyboard.exe
	Exec '"$INSTDIR\kbMonitor.exe"' 
SectionEnd



;Section /o $(strFonts) SecFonts
;	SetOutPath $INSTDIR\Fonts
;	File ".\Runtime Files\Fonts\*.*"
;
;	SetOverwrite try
;	SetOutPath $FONTS
;	File ".\Runtime Files\Fonts\*.*"
;	SetOverwrite on
;SectionEnd

Section /o $(strCleanRegistry) CleanRegistry
	ExecWait '"$INSTDIR\MkDraw.exe" /CleanRegistry' 
SectionEnd

;--------------------------------
;Installer Functions

Function .onInit

	StrCpy "$LANGUAGE" "${LANG_ENGLISH}"

	Push ""
	Push ${LANG_ENGLISH}
	Push English
	Push ${LANG_SPANISH}
	Push Spanish
	Push ${LANG_PORTUGUESE}
	Push Portuguese
	Push ${LANG_DUTCH}
	Push Dutch
	Push ${LANG_HUNGARIAN}
	Push Hungarian
	Push A ; A means auto count languages
	       ; for the auto count to work the first empty push (Push "") must remain
	LangDLL::LangDialog "Installer Language" "Please select the language of the installer"

	Pop $LANGUAGE
	StrCmp $LANGUAGE "cancel" 0 +2
		Abort

	Push $0

  ;Extract InstallOptions INI files
  !insertmacro MUI_INSTALLOPTIONS_EXTRACT "cmdline.ini"

  StrCpy $bShowCmdLine "1"

FunctionEnd

Function .onSelChange
	SectionGetFlags "0" $0
	SectionGetFlags "1" $1
	StrCpy $bShowCmdLine "0"

	StrCmp $0 "1" showcmdline_onSelChange
	StrCmp $1 "1" showcmdline_onSelChange

	Goto end_onSelChange

showcmdline_onSelChange:
	StrCpy $bShowCmdLine "1"

end_onSelChange:
FunctionEnd


Function CustomPageBrandingImage
	!insertmacro BIMAGE Foxjet2.bmp /RESIZETOFIT
FunctionEnd

Function CustomPageCmdLine
	StrCmp $bShowCmdLine "1" 0 endCustomPageCmdLine
	
	SetOutPath $INSTDIR
	File "eula.txt"

	;ReadRegStr $strCmdLine HKLM SOFTWARE\Foxjet\Installer "strCmdLine" 
	ReadINIStr	$strCmdLine "$INSTDIR\Install.ini" "Options" "strCmdLine" 
	!insertmacro MUI_INSTALLOPTIONS_WRITE "cmdline.ini" "Field 1" "State" $strCmdLine

	!insertmacro MUI_HEADER_TEXT "$(strCmdLineArgs)" "$(strCmdLineArgsEx)"
	!insertmacro MUI_INSTALLOPTIONS_DISPLAY "cmdline.ini"

	!insertmacro MUI_INSTALLOPTIONS_READ $strCmdLine "cmdline.ini" "Field 1" "State"

	;WriteRegStr HKLM SOFTWARE\Foxjet\Installer "strCmdLine" "$strCmdLine"
	WriteINIStr	"$INSTDIR\Install.ini" "Options" "strCmdLine" $strCmdLine
	;MessageBox MB_OK "$strCmdLine"
endCustomPageCmdLine:
FunctionEnd

;--------------------------------
;Descriptions


;Assign language strings to sections
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
!insertmacro MUI_DESCRIPTION_TEXT ${SecBoxWriter}			$(strSecBoxWriter)
!insertmacro MUI_DESCRIPTION_TEXT ${SecDesktopShortCuts}	$(strDesktop)
!insertmacro MUI_DESCRIPTION_TEXT ${SecStartMenuShortCuts}  $(strStartMenu)
!insertmacro MUI_DESCRIPTION_TEXT ${SecKeyboard}			$(strKeyboard)
!insertmacro MUI_DESCRIPTION_TEXT ${SecKbDesktopShortCuts}  $(strKbDesktop)
!insertmacro MUI_DESCRIPTION_TEXT ${SecKbStartupShortCuts}  $(strKbStartMenu)
!insertmacro MUI_DESCRIPTION_TEXT ${SecFonts}				$(strSecFonts)
!insertmacro MUI_FUNCTION_DESCRIPTION_END


;--------------------------------
;Uninstaller Section

UninstallText $(strUninstallText)
Section "Uninstall"
	; MUST REMOVE UNINSTALLER, too
	Delete $INSTDIR\uninstall.exe

	; remove shortcuts, if any.

	ExecWait '"$INSTDIR\MkDraw.exe" /kill"' 

	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$DESKTOP\Print Control.lnk"' 
	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$SMSTARTUP\Print Control.lnk"' 
	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$STARTMENU\Programs\FoxJet\Print Control.lnk"' 
	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$STARTMENU\Programs\FoxJet\BoxWriter Editor.lnk"' 
	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$STARTMENU\Programs\FoxJet\UnInstall.lnk"' 

	RMDir /r "$INSTDIR"
	RMDir /REBOOTOK "$INSTDIR"

	Delete $SYSDIR\Drivers\FxMphc.sys

SectionEnd
