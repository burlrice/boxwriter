//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Demo.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DEMO_DIALOG                 102
#define IDR_DEMO                        103
#define IDR_MAINFRAME                   128
#define IDC_BW                          1000
#define LB_TASKS                        1001
#define BTN_LOCATE                      1002
#define BTN_START                       1003
#define BTN_STOP                        1004
#define BTN_IDLE                        1005
#define BTN_RESUME                      1006
#define TXT_ADDRESS                     1007
#define BTN_CONNECT                     1008
#define BTN_DISCONNECT                  1009
#define TXT_COUNT                       1010
#define BTN_GETCOUNT                    1011
#define BTN_SETCOUNT                    1012
#define CHK_ERROR                       1013
#define TXT_USER                        1014
#define BTN_GETUSER                     1015
#define BTN_SETUSER                     1016
#define TXT_PROMPT                      1017
#define TXT_DATA                        1018
#define TXT_KEYVALUE                    1020
#define BTN_DBSTART                     1021
#define TXT_SERIALDATA                  1022
#define BTN_SETSERIAL                   1023
#define BTN_GETSERIAL                   1024

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1022
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
