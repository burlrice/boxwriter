import socket
import time

BW = "127.0.0.1"
CALLBACK_PORT = 2205

SELF = socket.gethostbyname(socket.gethostname())    
print("Local IP Address is: " + SELF)

locate = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP) 
locate.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
locate.bind(("", 2204))
locate.sendto("{Locate Boxwriter}".encode(), ('<broadcast>', 2200))

data, (ip, port) = locate.recvfrom(1024)
BW = ip
print("Located: %s:%d: %s" % (ip, port, data))

print("BoxWriter's IP Address is: " + BW)

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcp.connect((BW, 2202))

command = "{Set error socket,1,%s,%d}" % (SELF,CALLBACK_PORT)
print("Sending..." + command)
tcp.send(command.encode())
data = tcp.recv(1024)
print("Received: " + data.decode())

udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp.bind((SELF, CALLBACK_PORT))

while True:
    data = udp.recvfrom(1024)
    print("Received: " + data[0].decode())


