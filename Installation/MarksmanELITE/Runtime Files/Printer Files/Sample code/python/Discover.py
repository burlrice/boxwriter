import socket
from datetime import datetime

locate = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP) 
locate.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
locate.bind(("", 2204))
locate.sendto("{Locate Boxwriter}".encode(), ('<broadcast>', 2200))

while True:
	data, (ip, port) = locate.recvfrom(1024)
	print("FoxJet printer detected at: %s, responded with: %s" % (ip, data))

