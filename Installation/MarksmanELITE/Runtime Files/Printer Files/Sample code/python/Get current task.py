import socket
import re

BW = "127.0.0.1"
LINE = "" # blank to get first line

SELF = socket.gethostbyname(socket.gethostname())    
print("Local IP Address is: " + SELF)

locate = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP) 
locate.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
locate.bind(("", 2204))
locate.sendto("{Locate Boxwriter}".encode(), ('<broadcast>', 2200))

data, (ip, port) = locate.recvfrom(1024)
BW = ip
print("Located: %s:%d: %s" % (ip, port, data))

print("BoxWriter's IP Address is: " + BW)

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcp.connect((BW, 2202))

command = "{Get current task,%s}" % (LINE)
tcp.send(command.encode())
data = tcp.recv(1024).decode()
result = re.search('{Get current task,(.*)}', data).group(1).split(',')
print "Current task: {0}".format (result[1])

