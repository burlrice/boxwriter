﻿Imports AxBoxWriter
Imports BoxWriter

Public Class Form1

    Public m_printer As New BoxWriter.PRINTER


    Private Sub btnLocate_Click(sender As Object, e As EventArgs) Handles btnLocate.Click
        Dim v As New BoxWriter.StringArray
        Dim s As String

        Cursor = Cursors.WaitCursor
        s = bw.Locate(4)
        v.FromString(s)
        Cursor = Cursors.Default

        If (v.Count() >= 1) Then
            txtAddress.Text = v.GetAt(0)
            s = "The following addresses replied: " + vbCrLf

            For i As Integer = 0 To (v.Count - 1)
                s += v.GetAt(i) + vbCrLf
            Next

            MsgBox(s)
        End If
    End Sub

    Private Sub btnConnect_Click(sender As Object, e As EventArgs) Handles btnConnect.Click
        Dim strAddress As String

        strAddress = txtAddress.Text

        If (strAddress.Length = 0) Then
            strAddress = "192.168.1.5"
        End If

        Cursor = Cursors.WaitCursor

        If (m_printer.Connect(strAddress) = 0) Then
            MsgBox("Could not connect to: " + strAddress)
        Else
            Dim v As New BoxWriter.StringArray

            lbTasks.Items.Clear()

            txtAddress.Text = m_printer.m_strAddress
            txtCount.Text = m_printer.m_line.m_lCount
            m_printer.m_line.GetTasks(v)

            For i As Integer = 0 To (v.Count - 1)
                lbTasks.Items.Add(v.GetAt(i))
            Next
        End If

        Cursor = Cursors.Default
    End Sub

    Private Sub btnDisconnect_Click(sender As Object, e As EventArgs) Handles btnDisconnect.Click
        m_printer.Disconnect()
        lbTasks.Items.Clear()
        txtCount.Text = ""
    End Sub

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        m_printer.m_line.Load(lbTasks.SelectedItem)
        m_printer.m_line.Start()
    End Sub

    Private Sub btnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click
        m_printer.m_line.Stop()
    End Sub

    Private Sub btnIdle_Click(sender As Object, e As EventArgs) Handles btnIdle.Click
        m_printer.m_line.Idle()
    End Sub

    Private Sub btnResume_Click(sender As Object, e As EventArgs) Handles btnResume.Click
        m_printer.m_line.Resume()
    End Sub

    Private Sub btnCountGet_Click(sender As Object, e As EventArgs) Handles btnCountGet.Click
        txtCount.Text = m_printer.m_line.m_lCount
    End Sub

    Private Sub btnCountSet_Click(sender As Object, e As EventArgs) Handles btnCountSet.Click
        m_printer.m_line.m_lCount = txtCount.Text
    End Sub

    Private Sub bw_OnError(sender As Object, e As AxBoxWriter._DBoxWriterEvents_OnErrorEvent) Handles bw.OnError
        cbError.Enabled = True
    End Sub

    Private Sub bw_OnErrorCleared(sender As Object, e As _DBoxWriterEvents_OnErrorClearedEvent) Handles bw.OnErrorCleared
        cbError.Enabled = False
    End Sub

    Private Sub bw_OnHeadStateChanged(sender As Object, e As _DBoxWriterEvents_OnHeadStateChangedEvent) Handles bw.OnHeadStateChanged
        Dim vState As New BoxWriter.StringArray

        vState.FromString(e.str)

        If (vState.Count >= 3) Then
            Dim v As New BoxWriter.StringArray

            v.FromString(vState.GetAt(2))

            If (v.Count >= 6) Then
                Debug.WriteLine("line speed: " & v.GetAt(5))
            End If
        End If
    End Sub

    Private Sub bw_OnSOP(sender As Object, e As _DBoxWriterEvents_OnSOPEvent) Handles bw.OnSOP
        Debug.WriteLine("Start of print: " & e.lHeadID & ": " & e.lCount)
    End Sub

    Private Sub bw_OnEOP(sender As Object, e As _DBoxWriterEvents_OnEOPEvent) Handles bw.OnEOP
        Debug.WriteLine("end of print:   " & e.lHeadID & ": " & e.lCount)
    End Sub

    Private Sub Form1_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        m_printer.Disconnect()
    End Sub

    Private Sub btnUserPromptedGet_Click(sender As Object, e As EventArgs) Handles btnUserPromptedGet.Click
        Dim vPrompt As New BoxWriter.StringArray
        Dim vData As New BoxWriter.StringArray

        m_printer.m_line.GetUserElementData(vPrompt, vData)
        txtUser.Text = ""

        For i As Integer = 0 To (vData.Count - 1)
            txtUser.Text = txtUser.Text & vPrompt.GetAt(i) & ", " & vData.GetAt(i) & vbCrLf
        Next

        If (vData.Count > 0) Then
            txtData.Text = vData.GetAt(0)
        End If

        If (vPrompt.Count > 0) Then
            txtPrompt.Text = vPrompt.GetAt(0)
        End If
    End Sub

    Private Sub btnUserPromptedSet_Click(sender As Object, e As EventArgs) Handles btnUserPromptedSet.Click
        Dim vPrompt As New BoxWriter.StringArray
        Dim vData As New BoxWriter.StringArray

        vPrompt.Add(txtPrompt.Text)
        vData.Add(txtData.Text)

        m_printer.m_line.SetUserElementData(vPrompt, vData)
    End Sub

    Private Sub btnDbStart_Click(sender As Object, e As EventArgs) Handles btnDbStart.Click
        m_printer.m_line.DatabaseStart(txtKeyValue.Text)
    End Sub

    Private Sub btnSerialGet_Click(sender As Object, e As EventArgs) Handles btnSerialGet.Click
        txtSerial.Text = m_printer.m_line.m_strSerialData
    End Sub

    Private Sub btnSerialSet_Click(sender As Object, e As EventArgs) Handles btnSerialSet.Click
        m_printer.m_line.m_strSerialData = txtSerial.Text
    End Sub
End Class
