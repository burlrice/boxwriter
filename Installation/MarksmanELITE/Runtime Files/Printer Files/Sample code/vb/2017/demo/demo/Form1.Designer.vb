﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.bw = New AxBoxWriter.AxBoxWriter()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnDisconnect = New System.Windows.Forms.Button()
        Me.btnLocate = New System.Windows.Forms.Button()
        Me.btnConnect = New System.Windows.Forms.Button()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbError = New System.Windows.Forms.CheckBox()
        Me.lbTasks = New System.Windows.Forms.ListBox()
        Me.btnResume = New System.Windows.Forms.Button()
        Me.btnIdle = New System.Windows.Forms.Button()
        Me.btnStop = New System.Windows.Forms.Button()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnCountSet = New System.Windows.Forms.Button()
        Me.btnCountGet = New System.Windows.Forms.Button()
        Me.txtCount = New System.Windows.Forms.TextBox()
        Me.btnUserPromptedSet = New System.Windows.Forms.Button()
        Me.btnUserPromptedGet = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.btnDbStart = New System.Windows.Forms.Button()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtKeyValue = New System.Windows.Forms.TextBox()
        Me.btnSerialSet = New System.Windows.Forms.Button()
        Me.btnSerialGet = New System.Windows.Forms.Button()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.txtSerial = New System.Windows.Forms.TextBox()
        Me.txtPrompt = New System.Windows.Forms.TextBox()
        Me.txtData = New System.Windows.Forms.TextBox()
        CType(Me.bw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.SuspendLayout()
        '
        'bw
        '
        Me.bw.Enabled = True
        Me.bw.Location = New System.Drawing.Point(384, 12)
        Me.bw.Name = "bw"
        Me.bw.OcxState = CType(resources.GetObject("bw.OcxState"), System.Windows.Forms.AxHost.State)
        Me.bw.Size = New System.Drawing.Size(28, 30)
        Me.bw.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnDisconnect)
        Me.GroupBox1.Controls.Add(Me.btnLocate)
        Me.GroupBox1.Controls.Add(Me.btnConnect)
        Me.GroupBox1.Controls.Add(Me.txtAddress)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(204, 84)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Address"
        '
        'btnDisconnect
        '
        Me.btnDisconnect.Location = New System.Drawing.Point(119, 45)
        Me.btnDisconnect.Name = "btnDisconnect"
        Me.btnDisconnect.Size = New System.Drawing.Size(75, 23)
        Me.btnDisconnect.TabIndex = 3
        Me.btnDisconnect.Text = "Disconnect"
        Me.btnDisconnect.UseVisualStyleBackColor = True
        '
        'btnLocate
        '
        Me.btnLocate.Location = New System.Drawing.Point(6, 45)
        Me.btnLocate.Name = "btnLocate"
        Me.btnLocate.Size = New System.Drawing.Size(75, 23)
        Me.btnLocate.TabIndex = 2
        Me.btnLocate.Text = "Locate"
        Me.btnLocate.UseVisualStyleBackColor = True
        '
        'btnConnect
        '
        Me.btnConnect.Location = New System.Drawing.Point(119, 16)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(75, 23)
        Me.btnConnect.TabIndex = 1
        Me.btnConnect.Text = "Connect"
        Me.btnConnect.UseVisualStyleBackColor = True
        '
        'txtAddress
        '
        Me.txtAddress.Location = New System.Drawing.Point(6, 19)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(100, 20)
        Me.txtAddress.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbError)
        Me.GroupBox2.Controls.Add(Me.lbTasks)
        Me.GroupBox2.Controls.Add(Me.btnResume)
        Me.GroupBox2.Controls.Add(Me.btnIdle)
        Me.GroupBox2.Controls.Add(Me.btnStop)
        Me.GroupBox2.Controls.Add(Me.btnStart)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 102)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(278, 448)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Tasks"
        '
        'cbError
        '
        Me.cbError.AutoSize = True
        Me.cbError.Enabled = False
        Me.cbError.Location = New System.Drawing.Point(197, 135)
        Me.cbError.Name = "cbError"
        Me.cbError.Size = New System.Drawing.Size(48, 17)
        Me.cbError.TabIndex = 5
        Me.cbError.Text = "Error"
        Me.cbError.UseVisualStyleBackColor = True
        '
        'lbTasks
        '
        Me.lbTasks.FormattingEnabled = True
        Me.lbTasks.Location = New System.Drawing.Point(13, 18)
        Me.lbTasks.Name = "lbTasks"
        Me.lbTasks.Size = New System.Drawing.Size(163, 420)
        Me.lbTasks.TabIndex = 0
        '
        'btnResume
        '
        Me.btnResume.Location = New System.Drawing.Point(197, 106)
        Me.btnResume.Name = "btnResume"
        Me.btnResume.Size = New System.Drawing.Size(75, 23)
        Me.btnResume.TabIndex = 4
        Me.btnResume.Text = "Resume"
        Me.btnResume.UseVisualStyleBackColor = True
        '
        'btnIdle
        '
        Me.btnIdle.Location = New System.Drawing.Point(197, 77)
        Me.btnIdle.Name = "btnIdle"
        Me.btnIdle.Size = New System.Drawing.Size(75, 23)
        Me.btnIdle.TabIndex = 3
        Me.btnIdle.Text = "Idle"
        Me.btnIdle.UseVisualStyleBackColor = True
        '
        'btnStop
        '
        Me.btnStop.Location = New System.Drawing.Point(197, 48)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(75, 23)
        Me.btnStop.TabIndex = 2
        Me.btnStop.Text = "Stop"
        Me.btnStop.UseVisualStyleBackColor = True
        '
        'btnStart
        '
        Me.btnStart.Location = New System.Drawing.Point(197, 19)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(75, 23)
        Me.btnStart.TabIndex = 1
        Me.btnStart.Text = "Start"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnCountSet)
        Me.GroupBox3.Controls.Add(Me.btnCountGet)
        Me.GroupBox3.Controls.Add(Me.txtCount)
        Me.GroupBox3.Location = New System.Drawing.Point(306, 102)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(178, 84)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Count"
        '
        'btnCountSet
        '
        Me.btnCountSet.Location = New System.Drawing.Point(87, 45)
        Me.btnCountSet.Name = "btnCountSet"
        Me.btnCountSet.Size = New System.Drawing.Size(75, 23)
        Me.btnCountSet.TabIndex = 2
        Me.btnCountSet.Text = "Set"
        Me.btnCountSet.UseVisualStyleBackColor = True
        '
        'btnCountGet
        '
        Me.btnCountGet.Location = New System.Drawing.Point(6, 45)
        Me.btnCountGet.Name = "btnCountGet"
        Me.btnCountGet.Size = New System.Drawing.Size(75, 23)
        Me.btnCountGet.TabIndex = 1
        Me.btnCountGet.Text = "Get"
        Me.btnCountGet.UseVisualStyleBackColor = True
        '
        'txtCount
        '
        Me.txtCount.Location = New System.Drawing.Point(6, 19)
        Me.txtCount.Name = "txtCount"
        Me.txtCount.Size = New System.Drawing.Size(100, 20)
        Me.txtCount.TabIndex = 0
        '
        'btnUserPromptedSet
        '
        Me.btnUserPromptedSet.Location = New System.Drawing.Point(393, 327)
        Me.btnUserPromptedSet.Name = "btnUserPromptedSet"
        Me.btnUserPromptedSet.Size = New System.Drawing.Size(75, 23)
        Me.btnUserPromptedSet.TabIndex = 6
        Me.btnUserPromptedSet.Text = "Set"
        Me.btnUserPromptedSet.UseVisualStyleBackColor = True
        '
        'btnUserPromptedGet
        '
        Me.btnUserPromptedGet.Location = New System.Drawing.Point(312, 327)
        Me.btnUserPromptedGet.Name = "btnUserPromptedGet"
        Me.btnUserPromptedGet.Size = New System.Drawing.Size(75, 23)
        Me.btnUserPromptedGet.TabIndex = 5
        Me.btnUserPromptedGet.Text = "Get"
        Me.btnUserPromptedGet.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.GroupBox7)
        Me.GroupBox4.Controls.Add(Me.GroupBox9)
        Me.GroupBox4.Controls.Add(Me.GroupBox8)
        Me.GroupBox4.Controls.Add(Me.TextBox1)
        Me.GroupBox4.Location = New System.Drawing.Point(306, 192)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(178, 100)
        Me.GroupBox4.TabIndex = 6
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Count"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.txtUser)
        Me.GroupBox7.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(178, 162)
        Me.GroupBox7.TabIndex = 1
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "User prompted"
        '
        'txtUser
        '
        Me.txtUser.Enabled = False
        Me.txtUser.Location = New System.Drawing.Point(9, 19)
        Me.txtUser.Multiline = True
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(153, 75)
        Me.txtUser.TabIndex = 0
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.Button11)
        Me.GroupBox9.Controls.Add(Me.Button12)
        Me.GroupBox9.Controls.Add(Me.TextBox6)
        Me.GroupBox9.Location = New System.Drawing.Point(0, 196)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(178, 84)
        Me.GroupBox9.TabIndex = 6
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Count"
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(87, 45)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(75, 23)
        Me.Button11.TabIndex = 4
        Me.Button11.Text = "Set"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(6, 45)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(75, 23)
        Me.Button12.TabIndex = 3
        Me.Button12.Text = "Get"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(6, 19)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(100, 20)
        Me.TextBox6.TabIndex = 2
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.Button9)
        Me.GroupBox8.Controls.Add(Me.Button10)
        Me.GroupBox8.Controls.Add(Me.TextBox5)
        Me.GroupBox8.Location = New System.Drawing.Point(0, 106)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(178, 84)
        Me.GroupBox8.TabIndex = 6
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Count"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(87, 45)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(75, 23)
        Me.Button9.TabIndex = 4
        Me.Button9.Text = "Set"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(6, 45)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(75, 23)
        Me.Button10.TabIndex = 3
        Me.Button10.Text = "Get"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(6, 19)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(100, 20)
        Me.TextBox5.TabIndex = 2
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(6, 19)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1.TabIndex = 2
        '
        'btnDbStart
        '
        Me.btnDbStart.Location = New System.Drawing.Point(87, 45)
        Me.btnDbStart.Name = "btnDbStart"
        Me.btnDbStart.Size = New System.Drawing.Size(75, 23)
        Me.btnDbStart.TabIndex = 2
        Me.btnDbStart.Text = "Start"
        Me.btnDbStart.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label1)
        Me.GroupBox5.Controls.Add(Me.btnDbStart)
        Me.GroupBox5.Controls.Add(Me.txtKeyValue)
        Me.GroupBox5.Location = New System.Drawing.Point(306, 372)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(178, 84)
        Me.GroupBox5.TabIndex = 7
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Database task start"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Key value"
        '
        'txtKeyValue
        '
        Me.txtKeyValue.Location = New System.Drawing.Point(6, 45)
        Me.txtKeyValue.Name = "txtKeyValue"
        Me.txtKeyValue.Size = New System.Drawing.Size(75, 20)
        Me.txtKeyValue.TabIndex = 1
        '
        'btnSerialSet
        '
        Me.btnSerialSet.Location = New System.Drawing.Point(87, 45)
        Me.btnSerialSet.Name = "btnSerialSet"
        Me.btnSerialSet.Size = New System.Drawing.Size(75, 23)
        Me.btnSerialSet.TabIndex = 0
        Me.btnSerialSet.Text = "Set"
        Me.btnSerialSet.UseVisualStyleBackColor = True
        '
        'btnSerialGet
        '
        Me.btnSerialGet.Location = New System.Drawing.Point(6, 45)
        Me.btnSerialGet.Name = "btnSerialGet"
        Me.btnSerialGet.Size = New System.Drawing.Size(75, 23)
        Me.btnSerialGet.TabIndex = 1
        Me.btnSerialGet.Text = "Get"
        Me.btnSerialGet.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.btnSerialSet)
        Me.GroupBox6.Controls.Add(Me.btnSerialGet)
        Me.GroupBox6.Controls.Add(Me.txtSerial)
        Me.GroupBox6.Location = New System.Drawing.Point(306, 462)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(178, 84)
        Me.GroupBox6.TabIndex = 8
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Serial data"
        '
        'txtSerial
        '
        Me.txtSerial.Location = New System.Drawing.Point(6, 19)
        Me.txtSerial.Name = "txtSerial"
        Me.txtSerial.Size = New System.Drawing.Size(156, 20)
        Me.txtSerial.TabIndex = 0
        '
        'txtPrompt
        '
        Me.txtPrompt.Location = New System.Drawing.Point(312, 301)
        Me.txtPrompt.Name = "txtPrompt"
        Me.txtPrompt.Size = New System.Drawing.Size(75, 20)
        Me.txtPrompt.TabIndex = 3
        '
        'txtData
        '
        Me.txtData.Location = New System.Drawing.Point(393, 301)
        Me.txtData.Name = "txtData"
        Me.txtData.Size = New System.Drawing.Size(75, 20)
        Me.txtData.TabIndex = 4
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(496, 562)
        Me.Controls.Add(Me.txtData)
        Me.Controls.Add(Me.txtPrompt)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.btnUserPromptedSet)
        Me.Controls.Add(Me.btnUserPromptedGet)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.bw)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.bw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents bw As AxBoxWriter.AxBoxWriter
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btnDisconnect As Button
    Friend WithEvents btnLocate As Button
    Friend WithEvents btnConnect As Button
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents lbTasks As ListBox
    Friend WithEvents btnResume As Button
    Friend WithEvents btnIdle As Button
    Friend WithEvents btnStop As Button
    Friend WithEvents btnStart As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents btnCountSet As Button
    Friend WithEvents btnCountGet As Button
    Friend WithEvents txtCount As TextBox
    Friend WithEvents cbError As CheckBox
    Friend WithEvents btnUserPromptedSet As Button
    Friend WithEvents btnUserPromptedGet As Button
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents GroupBox9 As GroupBox
    Friend WithEvents Button11 As Button
    Friend WithEvents Button12 As Button
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents Button9 As Button
    Friend WithEvents Button10 As Button
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents txtPrompt As TextBox
    Friend WithEvents btnDbStart As Button
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents txtKeyValue As TextBox
    Friend WithEvents btnSerialSet As Button
    Friend WithEvents btnSerialGet As Button
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents txtSerial As TextBox
    Friend WithEvents txtData As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtUser As TextBox
End Class
