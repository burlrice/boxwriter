// xml.h : main header file for the XML application
//

#if !defined(AFX_XML_H__8F55ABD5_43A2_481D_8FBC_DB6ED34F1F92__INCLUDED_)
#define AFX_XML_H__8F55ABD5_43A2_481D_8FBC_DB6ED34F1F92__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CXmlApp:
// See xml.cpp for the implementation of this class
//

class CXmlApp : public CWinApp
{
public:
	CXmlApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXmlApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CXmlApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XML_H__8F55ABD5_43A2_481D_8FBC_DB6ED34F1F92__INCLUDED_)
