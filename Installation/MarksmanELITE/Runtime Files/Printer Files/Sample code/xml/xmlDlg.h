// xmlDlg.h : header file
//

#if !defined(AFX_XMLDLG_H__199B064E_00D0_4D8E_BFF9_9600EE05EEA8__INCLUDED_)
#define AFX_XMLDLG_H__199B064E_00D0_4D8E_BFF9_9600EE05EEA8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CXmlDlg dialog

class CXmlDlg : public CDialog
{
// Construction
public:
	CXmlDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CXmlDlg)
	enum { IDD = IDD_XML_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXmlDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CXmlDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBrowse();
	afx_msg void OnSend();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XMLDLG_H__199B064E_00D0_4D8E_BFF9_9600EE05EEA8__INCLUDED_)
