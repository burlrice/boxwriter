// xmlDlg.cpp : implementation file
//

#include "stdafx.h"
#include "xml.h"
#include "xmlDlg.h"
#include <afxsock.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CXmlApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CXmlDlg dialog

CXmlDlg::CXmlDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CXmlDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CXmlDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CXmlDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CXmlDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CXmlDlg, CDialog)
	//{{AFX_MSG_MAP(CXmlDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_BN_CLICKED(BTN_SEND, OnSend)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXmlDlg message handlers

BOOL CXmlDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	SetDlgItemText (TXT_XML, theApp.GetProfileString (_T ("Settings"), _T ("xml file"), _T ("Foxjet.xml")));
	
	if (CIPAddressCtrl * p = (CIPAddressCtrl *)GetDlgItem (IPC_MATRIX_ADDR)) {
		BYTE n [4] = 
		{
			theApp.GetProfileInt ("IP Address", "1", 192),
			theApp.GetProfileInt ("IP Address", "2", 168),
			theApp.GetProfileInt ("IP Address", "3", 2),
			theApp.GetProfileInt ("IP Address", "4", 163),
		};
		
		p->SetAddress (n [0], n [1], n [2], n [3]);
	}

	::AfxSocketInit ();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CXmlDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CXmlDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CXmlDlg::OnBrowse() 
{
	CString strPath;

	GetDlgItemText (TXT_XML, strPath);
	
	CFileDialog dlg (TRUE, _T ("XML file"), strPath, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("XML files (*.xml)|*.xml|All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		strPath = dlg.GetPathName ();
		theApp.WriteProfileString (_T ("Settings"), _T ("xml file"), strPath);
		SetDlgItemText (TXT_XML, strPath);
	}
}

DWORD GetFileSize (const CString & strFile)
{
	DWORD dw = 0;

    if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
		fseek (fp, 0, SEEK_END);
		dw = ftell (fp);
		fclose (fp);
	}

	return dw;
}

void CXmlDlg::OnSend() 
{
	CString strPath;

	BeginWaitCursor ();
	GetDlgItemText (TXT_XML, strPath);

	if (CIPAddressCtrl * p = (CIPAddressCtrl *)GetDlgItem (IPC_MATRIX_ADDR)) {
		BYTE n [4] = { 0 };

		if (p->GetAddress (n [0], n [1], n [2], n [3])) {
			CSocket sock;
			CString strAddress;

			strAddress.Format ("%d.%d.%d.%d", n [0], n [1], n [2], n [3]);

			if (sock.Create (0, SOCK_STREAM)) {
				int nTimeout = 5000;

				sock.SetSockOpt (SO_RCVTIMEO, (char*)&nTimeout, sizeof (nTimeout), SOL_SOCKET);

				if (sock.Connect (strAddress, 2202)) {
					if (DWORD dw = GetFileSize (strPath)) {
						if (FILE * fp = _tfopen (strPath, _T ("rb"))) {
							BYTE * p = new BYTE [dw];

							fread (p, dw, 1, fp);
							fclose (fp);

							sock.Send (p, dw);

							theApp.WriteProfileInt ("IP Address", "1", n [0]);
							theApp.WriteProfileInt ("IP Address", "2", n [1]);
							theApp.WriteProfileInt ("IP Address", "3", n [2]);
							theApp.WriteProfileInt ("IP Address", "4", n [3]);

							memset (p, 0, dw);
							dw = sock.Receive (p, dw);
							MessageBox (CString (p));

							delete [] p;
						}
						else
							MessageBox (_T ("Input file not found"));
					}
					else
						MessageBox (_T ("Input file not found"));
				}
				else
					MessageBox (_T ("CSocket::Connect failed"));
			}
			else
				MessageBox (_T ("CSocket::Create failed"));
		}
	}

	EndWaitCursor ();
}
