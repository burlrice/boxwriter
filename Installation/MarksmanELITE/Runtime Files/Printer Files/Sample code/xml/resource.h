//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by xml.rc
//
#define IDD_XML_DIALOG                  102
#define IDR_MAINFRAME                   128
#define BTN_BROWSE                      1000
#define BTN_SEND                        1001
#define TXT_XML                         1002
#define IPC_MATRIX_ADDR                 1003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
