;---------------------
;Include Modern UI

	!include "MUI.nsh"

;--------------------------------

!macro BIMAGE IMAGE PARMS
	Push $0
	GetTempFileName $0
	File /oname=$0 "${IMAGE}"
	SetBrandingImage ${PARMS} $0
	Delete $0
	Pop $0
!macroend

;--------------------------------
;Pages

	Page custom CustomPageBrandingImage
	!insertmacro MUI_PAGE_LICENSE "Eula.txt"
	!insertmacro MUI_PAGE_COMPONENTS
	Page custom CustomPageCmdLine
	!insertmacro MUI_PAGE_DIRECTORY
	!insertmacro MUI_PAGE_INSTFILES

	!insertmacro MUI_UNPAGE_CONFIRM
	!insertmacro MUI_UNPAGE_INSTFILES
	!insertmacro MUI_PAGE_FINISH

;--------------------------------
;Languages
 
	!insertmacro MUI_LANGUAGE "English"
	!insertmacro MUI_LANGUAGE "Spanish"
	!insertmacro MUI_LANGUAGE "Portuguese"

;--------------------------------
;language strings

	LangString strLicense			${LANG_ENGLISH} "Please Read Carefully."
	LangString strApplication		${LANG_ENGLISH} "Please choose a BoxWriter application to install."
	LangString strOverwriteDB		${LANG_ENGLISH} "A database from a previous installation was found.  Do you want to keep the old database?"
	LangString strUserPass			${LANG_ENGLISH} "Your new username and password for the Print Control application is: ADMIN, FOXJET"
	LangString strShortcuts			${LANG_ENGLISH} "Start Menu and Desktop Shortcuts"
	LangString strUninstallText		${LANG_ENGLISH} "This will uninstall the BoxWriter software. Hit next to continue."
	LangString strInstallDir		${LANG_ENGLISH} "Please choose install directory."
	LangString strUpdateDriver		${LANG_ENGLISH} "Update the driver?  You will have to reboot before the changes take effect."
	LangString strFonts				${LANG_ENGLISH} "Fonts"
	LangString strCmdLineArgs		${LANG_ENGLISH} "Command line arguments"
	LangString strCmdLineArgsEx		${LANG_ENGLISH} "Example: /demo"
	LangString strSecBoxWriter		${LANG_ENGLISH} "Print Control and Editor applications"
	LangString strSecFonts			${LANG_ENGLISH} "TrueType MK fonts"

	LangString strLicense			${LANG_SPANISH} "Por favor Lea con Cuidado."
	LangString strApplication		${LANG_SPANISH} "Escoja por favor una aplicaci�n de BoxWriter para instalar. "
	LangString strOverwriteDB		${LANG_SPANISH} "Una base de datos de una instalaci�n previa se encontr�. �Quiere usted mantener la base de datos vieja? "
	LangString strUserPass			${LANG_SPANISH} "Su nombre de usuario y la contrase�a nueva para la aplicaci�n del Control de la Impresi�n son: ADMIN, FOXJET"
	LangString strShortcuts			${LANG_SPANISH} "Men� de Comienzo y  Atajos de controlador"
	LangString strUninstallText		${LANG_SPANISH} "Esto desinstalar� el software de BoxWriter. Golpee Proximo (NEXT) para continuar."
	LangString strInstallDir		${LANG_SPANISH} "Por favor Escoja  Directorio de instalacion."
	LangString strUpdateDriver		${LANG_SPANISH} "�Actualizar el driver? Usted tendr� que reiniciar para que los cambios surten efecto. "
	LangString strFonts				${LANG_SPANISH} "Fuentes"
	LangString strCmdLineArgs		${LANG_SPANISH} "Command line arguments"
	LangString strCmdLineArgsEx		${LANG_SPANISH} "Example: /demo"
	LangString strSecBoxWriter		${LANG_SPANISH} "Print Control and Editor applications"
	LangString strSecFonts			${LANG_SPANISH} "TrueType MK fonts"
									
	LangString strLicense			${LANG_PORTUGUESE} "Favor ler atentamente."
	LangString strApplication		${LANG_PORTUGUESE} "Escolha um aplicativo BoxWriter para instalar."
	LangString strOverwriteDB		${LANG_PORTUGUESE} "Um banco de dados de uma instala��o anterior foi encontrado. Voc� deseja manter o banco de dados antigo?"
	LangString strUserPass			${LANG_PORTUGUESE} "O seu nome de usu�rio e sua senha para o aplicativo Print Control �: ADMIN, FOXJET"
	LangString strShortcuts			${LANG_PORTUGUESE} "Menu Iniciar e Atalhos da �rea de Trabalho"
	LangString strUninstallText		${LANG_PORTUGUESE} "Isto ir� desinstalar o software BoxWriter. Clique em avan�ar para continuar."
	LangString strInstallDir		${LANG_PORTUGUESE} "Selecione o diret�rio de instala��o."
	LangString strUpdateDriver		${LANG_PORTUGUESE} "Atualizar o driver? Voc� dever� reinicializar para que as altera��es tenham efeito."
	LangString strFonts				${LANG_PORTUGUESE} "Fontes"
	LangString strCmdLineArgs		${LANG_PORTUGUESE} "Argumentos da linha de comando"
	LangString strCmdLineArgsEx		${LANG_PORTUGUESE} "Exemplo: /demo"
	LangString strSecBoxWriter		${LANG_PORTUGUESE} "Aplicativos Print Control e Editor"
	LangString strSecFonts			${LANG_PORTUGUESE} "Fontes MK True Type"

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING
  
;--------------------------------
;Reserve Files
  
  ;These files should be inserted before other files in the data block
  ;Keep these lines before any File command
  ;Only for solid compression (by default, solid compression is enabled for BZIP2 and LZMA)
  
  ReserveFile "cmdline.ini"
  !insertmacro MUI_RESERVEFILE_INSTALLOPTIONS

;--------------------------------
;Variables

	Var "strCmdLine"
	Var "strNewVer"
	Var "strOldVer"
	Var "bShowCmdLine"

;--------------------------------
;General

	Name "BoxWriter ELITE Remote Configurator"
	OutFile "BwConfig.exe"

	InstallDir "$PROGRAMFILES\Foxjet\MarksmanELITE"
	ComponentText $(strApplication) 

	BGGradient 000000 000080 FFFFFF
	InstallColors 00FF00 000030
	XPStyle on
	AddBrandingImage top 80

	Icon "mkdraw.ico"
	UninstallIcon "uninstall.ico"

;--------------------------------
;Installer Sections

Section "BoxWriter ELITE Remote Configurator" SecBoxWriterConfig
	SetOutPath $INSTDIR\Logos
	File ".\Runtime Files\Logos\*.*"
	WriteUninstaller "uninstall.exe"

	SetShellVarContext All

	SetOutPath $INSTDIR
	File /r ".\Runtime Files\Printer Files\*.*"
	File ".\Runtime Files\Drivers\*.*"

	Delete "$INSTDIR\Control.exe"

;	IfFileExists "$INSTDIR\MarksmanPro.mdb" 0 OverwriteDatabase
;		MessageBox MB_YESNO|MB_ICONQUESTION $(strOverwriteDB) IDYES CreateShortcuts
;
;	OverwriteDatabase: 
;
;		File ".\Runtime Files\Database\MarksmanPro.mdb" 
;		ExecWait '"$INSTDIR\MkDraw.exe" /resetdsn' 
;		MessageBox MB_ICONINFORMATION $(strUserPass)
;
CreateShortcuts:

;	ExecWait '"$INSTDIR\MkDraw.exe" /CleanRegistry' 

	MessageBox MB_ICONINFORMATION "Note: You will have to manually configure the DSN before starting the BoxWriter ELITE Remote Configurator"
SectionEnd

Section "Program files shortcut" SecProgramFiles
  	CreateDirectory "$STARTMENU\Programs\FoxJet"
	CreateShortCut "$STARTMENU\Programs\FoxJet\Configurator.lnk" $INSTDIR\Config.exe "$strCmdLine" $INSTDIR\Config.exe
  	CreateShortCut "$STARTMENU\Programs\FoxJet\BoxWriter Editor.lnk" $INSTDIR\Legacy\MkDraw.exe "$strCmdLine" $INSTDIR\Legacy\MkDraw.exe

  	CreateShortCut "$STARTMENU\Programs\FoxJet\UnInstall.lnk" $INSTDIR\Uninstall.exe "" $INSTDIR\UnInstall.exe
SectionEnd

Section "Startup shortcut" SecStartup
	CreateShortCut "$SMSTARTUP\Configurator.lnk" $INSTDIR\Config.exe "$strCmdLine" $INSTDIR\Config.exe
SectionEnd

Section "Desktop shortcut" SecDesktop
  	CreateShortCut "$DESKTOP\Configurator.lnk" $INSTDIR\Config.exe "$strCmdLine" $INSTDIR\Config.exe
  	CreateShortCut "$DESKTOP\Editor.lnk" $INSTDIR\Legacy\mkdraw.exe "$strCmdLine" $INSTDIR\Legacy\mkdraw.exe
SectionEnd


Section /o $(strFonts) SecFonts
	SetOutPath $INSTDIR\Fonts
	File ".\Runtime Files\Fonts\*.*"

	SetOverwrite try
	SetOutPath $FONTS
	File ".\Runtime Files\Fonts\*.*"
	SetOverwrite on
SectionEnd

Section /o $(strCleanRegistry) CleanRegistry
	ExecWait '"$INSTDIR\MkDraw.exe" /CleanRegistry' 
SectionEnd

;--------------------------------
;Installer Functions

Function .onInit
	StrCpy "$LANGUAGE" "${LANG_ENGLISH}"

	Push ""
	Push ${LANG_ENGLISH}
	Push English
	Push ${LANG_SPANISH}
	Push Spanish
	Push ${LANG_PORTUGUESE}
	Push Portuguese
	Push A ; A means auto count languages
	       ; for the auto count to work the first empty push (Push "") must remain
	LangDLL::LangDialog "Installer Language" "Please select the language of the installer"

	Pop $LANGUAGE
	StrCmp $LANGUAGE "cancel" 0 +2
		Abort

	Push $0

  ;Extract InstallOptions INI files
  !insertmacro MUI_INSTALLOPTIONS_EXTRACT "cmdline.ini"

  StrCpy $bShowCmdLine "1"

FunctionEnd

Function .onSelChange
	SectionGetFlags "0" $0
	SectionGetFlags "1" $1
	StrCpy $bShowCmdLine "0"

	StrCmp $0 "1" showcmdline_onSelChange
	StrCmp $1 "1" showcmdline_onSelChange

	Goto end_onSelChange

showcmdline_onSelChange:
	StrCpy $bShowCmdLine "1"

end_onSelChange:
FunctionEnd


Function CustomPageBrandingImage
	!insertmacro BIMAGE Foxjet2.bmp /RESIZETOFIT
FunctionEnd

Function CustomPageCmdLine
	StrCmp $bShowCmdLine "1" 0 endCustomPageCmdLine
	
	SetOutPath $INSTDIR
	File "eula.txt"

	;ReadRegStr $strCmdLine HKLM SOFTWARE\Foxjet\Installer "strCmdLine" 
	ReadINIStr	$strCmdLine "$INSTDIR\Install.ini" "Options" "strCmdLine" 
	!insertmacro MUI_INSTALLOPTIONS_WRITE "cmdline.ini" "Field 1" "State" $strCmdLine

	!insertmacro MUI_HEADER_TEXT "$(strCmdLineArgs)" "$(strCmdLineArgsEx)"
	!insertmacro MUI_INSTALLOPTIONS_DISPLAY "cmdline.ini"

	!insertmacro MUI_INSTALLOPTIONS_READ $strCmdLine "cmdline.ini" "Field 1" "State"

	;WriteRegStr HKLM SOFTWARE\Foxjet\Installer "strCmdLine" "$strCmdLine"
	WriteINIStr	"$INSTDIR\Install.ini" "Options" "strCmdLine" $strCmdLine
	;MessageBox MB_OK "$strCmdLine"
endCustomPageCmdLine:
FunctionEnd

;--------------------------------
;Descriptions


;Assign language strings to sections
;!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
;!insertmacro MUI_DESCRIPTION_TEXT ${SecBoxWriter} $(strSecBoxWriter)
;!insertmacro MUI_DESCRIPTION_TEXT ${SecProgramFiles)	"Program files shortcuts"
;!insertmacro MUI_DESCRIPTION_TEXT ${SecStartup)			"Startup folder shortcut"
;!insertmacro MUI_DESCRIPTION_TEXT ${SecDesktop)			"Desktop shortcut"
;!insertmacro MUI_DESCRIPTION_TEXT ${SecBoxWriterConfig)	"BoxWriter ELITE Remote Configurator"
;!insertmacro MUI_DESCRIPTION_TEXT ${SecFonts} $(strSecFonts)
;!insertmacro MUI_DESCRIPTION_TEXT ${SecShortCuts} $(strShortcuts)
;!insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Section

UninstallText $(strUninstallText)
Section "Uninstall"
	; MUST REMOVE UNINSTALLER, too
	Delete $INSTDIR\uninstall.exe

	; remove shortcuts, if any.

	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$DESKTOP\Print Control.lnk"' 
	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$DESKTOP\Configurator.lnk' 
	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$DESKTOP\Editor.lnk"' 

	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$SMSTARTUP\Print Control.lnk"' 
	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$SMSTARTUP\Configurator.lnk"' 
	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$SMSTARTUP\Editor.lnk"' 

	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$STARTMENU\Programs\FoxJet\Print Control.lnk"' 
	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$STARTMENU\Programs\FoxJet\BoxWriter Editor.lnk"' 
	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$STARTMENU\Programs\FoxJet\Configurator.lnk"' 
	ExecWait '"$INSTDIR\MkDraw.exe" /delete="$STARTMENU\Programs\FoxJet\UnInstall.lnk"' 

	RMDir /r "$INSTDIR"
	RMDir /REBOOTOK "$INSTDIR"

	Delete $SYSDIR\Drivers\FxMphc.sys

SectionEnd
