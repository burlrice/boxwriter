; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDemoDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Demo.h"

ClassCount=3
Class1=CDemoApp
Class2=CDemoDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_DEMO_DIALOG

[CLS:CDemoApp]
Type=0
HeaderFile=Demo.h
ImplementationFile=Demo.cpp
Filter=N

[CLS:CDemoDlg]
Type=0
HeaderFile=DemoDlg.h
ImplementationFile=DemoDlg.cpp
Filter=D
LastObject=IDC_BW
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=DemoDlg.h
ImplementationFile=DemoDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_DEMO_DIALOG]
Type=1
Class=CDemoDlg
ControlCount=17
Control1=LB_TASKS,listbox,1352728835
Control2=BTN_START,button,1342242816
Control3=BTN_STOP,button,1342242816
Control4=BTN_IDLE,button,1342242816
Control5=BTN_RESUME,button,1342242816
Control6=TXT_ADDRESS,edit,1350631552
Control7=BTN_CONNECT,button,1342242816
Control8=BTN_DISCONNECT,button,1342242816
Control9=BTN_LOCATE,button,1342242816
Control10=TXT_COUNT,edit,1350631552
Control11=BTN_GETCOUNT,button,1342242816
Control12=BTN_SETCOUNT,button,1342242816
Control13=IDC_BW,{2E5D9215-26EF-4024-8BAC-93C45C8E6C9D},1342242816
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,button,1342177287
Control16=IDC_STATIC,button,1342177287
Control17=CHK_ERROR,button,1476460547

