// DemoDlg.h : header file
//

#if !defined(AFX_DEMODLG_H__EDD80BCB_B941_4403_9D3E_2D7ECD14B8A1__INCLUDED_)
#define AFX_DEMODLG_H__EDD80BCB_B941_4403_9D3E_2D7ECD14B8A1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CDemoDlg dialog

class CDemoDlg : public CDialog
{
// Construction
public:
	CDemoDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CDemoDlg)
	enum { IDD = IDD_DEMO_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDemoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	CComPtr <IPrinter> m_printer;


	// Generated message map functions
	//{{AFX_MSG(CDemoDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnOnErrorBw(LPCTSTR strAddress, LPCTSTR strLine);
	afx_msg void OnOnErrorClearedBw(LPCTSTR strAddress, LPCTSTR strLine);
	afx_msg void OnLocate();
	afx_msg void OnConnect();
	afx_msg void OnDisconnect();
	afx_msg void OnGetcount();
	afx_msg void OnSetcount();
	afx_msg void OnStart();
	afx_msg void OnStop();
	afx_msg void OnIdle();
	afx_msg void OnResume();
	afx_msg void OnOnEOPBw(long lHeadID, long lCount);
	afx_msg void OnOnHeadStateChangedBw(long lHeadID);
	afx_msg void OnOnSOPBw(long lHeadID, long lCount);
	afx_msg void OnDestroy();
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMODLG_H__EDD80BCB_B941_4403_9D3E_2D7ECD14B8A1__INCLUDED_)
