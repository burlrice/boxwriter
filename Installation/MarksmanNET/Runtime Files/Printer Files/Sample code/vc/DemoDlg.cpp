// DemoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Demo.h"
#include "DemoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDemoDlg dialog

CDemoDlg::CDemoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDemoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDemoDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDemoDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDemoDlg, CDialog)
	//{{AFX_MSG_MAP(CDemoDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_LOCATE, OnLocate)
	ON_BN_CLICKED(BTN_CONNECT, OnConnect)
	ON_BN_CLICKED(BTN_DISCONNECT, OnDisconnect)
	ON_BN_CLICKED(BTN_GETCOUNT, OnGetcount)
	ON_BN_CLICKED(BTN_SETCOUNT, OnSetcount)
	ON_BN_CLICKED(BTN_START, OnStart)
	ON_BN_CLICKED(BTN_STOP, OnStop)
	ON_BN_CLICKED(BTN_IDLE, OnIdle)
	ON_BN_CLICKED(BTN_RESUME, OnResume)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDemoDlg message handlers

BOOL CDemoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	SetDlgItemText (TXT_ADDRESS, _T ("1,9600,n,8,1"));

	VERIFY (SUCCEEDED (m_printer.CoCreateInstance (CLSID_Printer)));
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDemoDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDemoDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

BEGIN_EVENTSINK_MAP(CDemoDlg, CDialog)
    //{{AFX_EVENTSINK_MAP(CDemoDlg)
	ON_EVENT(CDemoDlg, IDC_BW, 1 /* OnError */, OnOnErrorBw, VTS_BSTR VTS_BSTR)
	ON_EVENT(CDemoDlg, IDC_BW, 2 /* OnErrorCleared */, OnOnErrorClearedBw, VTS_BSTR VTS_BSTR)
	ON_EVENT(CDemoDlg, IDC_BW, 3 /* OnEOP */, OnOnEOPBw, VTS_I4 VTS_I4)
	ON_EVENT(CDemoDlg, IDC_BW, 5 /* OnHeadStateChanged */, OnOnHeadStateChangedBw, VTS_I4)
	ON_EVENT(CDemoDlg, IDC_BW, 4 /* OnSOP */, OnOnSOPBw, VTS_I4 VTS_I4)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

void CDemoDlg::OnOnErrorBw(LPCTSTR strAddress, LPCTSTR strLine) 
{
	CButton * p = (CButton *)GetDlgItem (CHK_ERROR);

	ASSERT (p);
	
	p->SetCheck (1);
}

void CDemoDlg::OnOnErrorClearedBw(LPCTSTR strAddress, LPCTSTR strLine) 
{
	CButton * p = (CButton *)GetDlgItem (CHK_ERROR);

	ASSERT (p);
	
	p->SetCheck (0);
}

void CDemoDlg::OnLocate() 
{
	if (CBoxWriter * pCtrl = (CBoxWriter *)GetDlgItem (IDC_BW)) {
		CString str = "The following addresses responded: \n";
		CString strLocate = pCtrl->Locate (4);
		CComPtr <IStringArray> v;

		VERIFY (SUCCEEDED (v.CoCreateInstance (CLSID_StringArray)));

		v->FromString (strLocate.AllocSysString ());

		for (int i = 0; i < v->GetCount (); i++)
			str += v->GetGetAt (i) + "\n";

		MessageBox (str);
	}
}

void CDemoDlg::OnConnect() 
{
	CString strAddress;
	BSTR bstrAddress;

	GetDlgItemText (TXT_ADDRESS, strAddress);
	
	if (!m_printer->Connect (strAddress.AllocSysString ())) {
		MessageBox ("Connect failed: " + strAddress);
		return;
	}

	m_printer->get_m_strAddress (&bstrAddress);
	SetDlgItemText (TXT_ADDRESS, (LPCTSTR)_bstr_t (bstrAddress));

	{ // get tasks
		ILine * pLine = NULL;

		m_printer->get_m_line (CString ().AllocSysString (), &pLine);

		if (pLine) {
			CComPtr <IStringArray> v;
			CListBox * pTasks = (CListBox *)GetDlgItem (LB_TASKS);

			ASSERT (pTasks);
			pTasks->ResetContent ();

			VERIFY (SUCCEEDED (v.CoCreateInstance (CLSID_StringArray)));
			pLine->GetTasks (v);
			pLine->Release ();

			for (int i = 0; i < v->GetCount (); i++) 
				pTasks->AddString (v->GetGetAt (i));
		}
	}
}

void CDemoDlg::OnDisconnect() 
{
	CString strAddress;
	CListBox * pTasks = (CListBox *)GetDlgItem (LB_TASKS);

	ASSERT (pTasks);

	GetDlgItemText (TXT_ADDRESS, strAddress);

	if (strAddress.GetLength ()) {
        m_printer->Disconnect ();
        pTasks->ResetContent ();
		SetDlgItemText (TXT_COUNT, _T (""));
		SetDlgItemText (TXT_ADDRESS, _T (""));
	}	
}

void CDemoDlg::OnGetcount() 
{
	ILine * pLine = NULL;

	m_printer->get_m_line (CString ().AllocSysString (), &pLine);

	if (pLine) {
		long lCount = 0;

		pLine->get_m_lCount (&lCount);
		SetDlgItemInt (TXT_COUNT, lCount);
		pLine->Release ();
	}
}

void CDemoDlg::OnSetcount() 
{
	ILine * pLine = NULL;

	m_printer->get_m_line (CString ().AllocSysString (), &pLine);

	if (pLine) {
		pLine->put_m_lCount (GetDlgItemInt (TXT_COUNT));
		pLine->Release ();
	}
}

void CDemoDlg::OnStart() 
{
	CString strTask;
	CListBox * pTasks = (CListBox *)GetDlgItem (LB_TASKS);
	ILine * pLine = NULL;

	ASSERT (pTasks);
	int nIndex = pTasks->GetCurSel ();

	if (nIndex == LB_ERR) {
		MessageBox (_T ("Nothing selected"));
		return;
	}

	pTasks->GetText (nIndex, strTask);

	m_printer->get_m_line (CString ().AllocSysString (), &pLine);

	if (pLine) {
		pLine->Load (strTask.AllocSysString ());
		pLine->Start ();
		pLine->Release ();
	}
}

void CDemoDlg::OnStop() 
{
	ILine * pLine = NULL;

	m_printer->get_m_line (CString ().AllocSysString (), &pLine);

	if (pLine) {
		pLine->Stop ();
		pLine->Release ();
	}
}

void CDemoDlg::OnIdle() 
{
	ILine * pLine = NULL;

	m_printer->get_m_line (CString ().AllocSysString (), &pLine);

	if (pLine) {
		pLine->Idle ();
		pLine->Release ();
	}
}

void CDemoDlg::OnResume() 
{
	ILine * pLine = NULL;

	m_printer->get_m_line (CString ().AllocSysString (), &pLine);

	if (pLine) {
		pLine->Resume ();
		pLine->Release ();
	}
}

void CDemoDlg::OnOnEOPBw(long lHeadID, long lCount) 
{
	CString str;
	
	str.Format (_T ("EOP: %d: %d\n"), lHeadID, lCount);
	::OutputDebugString (str);
}

void CDemoDlg::OnOnHeadStateChangedBw(long lHeadID) 
{
	CString str;
	
	str.Format (_T ("State changed: %d\n"), lHeadID);
	::OutputDebugString (str);
}

void CDemoDlg::OnOnSOPBw(long lHeadID, long lCount) 
{
	CString str;
	
	str.Format (_T ("SOP: %d: %d\n"), lHeadID, lCount);
	::OutputDebugString (str);
}

void CDemoDlg::OnDestroy() 
{
	OnDisconnect ();
	CDialog::OnDestroy();
}
