// ProdKeyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ProdKey.h"
#include "ProdKeyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CProdKeyApp theApp;

static const CString strDef = _T ("XXXXX-XXXXX-XXXXX-XXXXX-XXXXX");

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProdKeyDlg dialog

CProdKeyDlg::CProdKeyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CProdKeyDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CProdKeyDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CProdKeyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProdKeyDlg)
	DDX_Control(pDX, TXT_KEY2, m_ctrlKey2);
	DDX_Control(pDX, TXT_KEY1, m_ctrlKey1);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CProdKeyDlg, CDialog)
	//{{AFX_MSG_MAP(CProdKeyDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_BN_CLICKED(BTN_BROWSE2, OnBrowse2)
	ON_BN_CLICKED(BTN_BROWSE3, OnBrowse3)
	ON_BN_CLICKED(BTN_BROWSE4, OnBrowse4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProdKeyDlg message handlers

BOOL CProdKeyDlg::OnInitDialog()
{
	SetDlgItemText (TXT_KEY1, ::strDef);
	SetDlgItemText (TXT_KEY2, ::strDef);

#ifdef _DEBUG
	LPCTSTR lpsz = _T ("R82K8-37PQ8-BFPMC-K6FP9-236D8");

	SetDlgItemText (TXT_KEY1, lpsz);
	SetDlgItemText (TXT_KEY2, lpsz);
#endif

	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	if (CEdit * p = (CEdit *)GetDlgItem (TXT_KEY1))
		p->SetLimitText (30);
	if (CEdit * p = (CEdit *)GetDlgItem (TXT_KEY2))
		p->SetLimitText (30);

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	CString strFile = theApp.GetProfileString (_T ("ProdKey"),	_T ("File"),	_T ("C:\\windows\\system32\\oobe\\oobeinfo.ini"));
	CString strFile2 = theApp.GetProfileString (_T ("ProdKey"), _T ("File2"),	_T ("C:\\windows\\system32\\oobe\\sysprep.inf"));
	CString strFile3 = theApp.GetProfileString (_T ("ProdKey"), _T ("File3"),	_T ("C:\\sysprep"));
	CString strFile4 = theApp.GetProfileString (_T ("ProdKey"), _T ("File4"),	_T ("C:\\sysprep\\unattend.txt"));

	SetDlgItemText (TXT_FILE, strFile);
	SetDlgItemText (TXT_FILE2, strFile2);
	SetDlgItemText (TXT_FILE3, strFile3);
	SetDlgItemText (TXT_FILE4, strFile4);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CProdKeyDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CProdKeyDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CProdKeyDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

static bool IsValidKey (const CString & strKey)
{
	//R82K8-37PQ8-BFPMC-K6FP9-236D8
	// 123456789 123456789 12345678
	//           1         2

	if (!strKey.CompareNoCase (::strDef))
		return false;

	if (strKey.GetLength () == 29) {
		CString str (strKey);

		str.Delete (23);
		str.Delete (17);
		str.Delete (11);
		str.Delete (5);

		for (int i = 0; i < str.GetLength (); i++)
			if (!_istalpha (str [i]) && !_istdigit (str [i]))
				return false;

		return true;
	}

	return false;
}

void CProdKeyDlg::OnOK() 
{
	CString strKey [2], strFile, strFile2, strFile3, strFile4;

	GetDlgItemText (TXT_KEY1, strKey [0]);
	GetDlgItemText (TXT_KEY2, strKey [1]);
	GetDlgItemText (TXT_FILE, strFile);
	GetDlgItemText (TXT_FILE2, strFile2);
	GetDlgItemText (TXT_FILE3, strFile3);
	GetDlgItemText (TXT_FILE4, strFile4);
	
	if (!strKey [0].CompareNoCase (::strDef) || !strKey [1].CompareNoCase (::strDef))
		return;

	if (IsValidKey (strKey [0])) {
		if (!strKey [0].CompareNoCase (strKey [1])) {
			CString strUpdated, strFailed;
			bool bFailed = false;
			TCHAR szTitle [MAX_PATH] = { 0 };
			CString strDest;

			if (WritePrivateProfileString (_T ("Version"), _T ("ProductKey"), strKey [0], strFile)) { strUpdated += strFile + _T ("\n"); } else { strUpdated += strFile + _T ("\n"); }
			if (WritePrivateProfileString (_T ("UserData"), _T ("ProductID"), '\"' + strKey [0] + '\"', strFile2)) { strUpdated += strFile2 + _T ("\n"); } else { strUpdated += strFile2 + _T ("\n"); }
			if (WritePrivateProfileString (_T ("UserData"), _T ("ProductKey"), '\"' + strKey [0] + '\"', strFile4)) { strUpdated += strFile4 + _T ("\n"); } else { strUpdated += strFile3 + _T ("\n");  }

			{
				int nIndex = strFile2.ReverseFind ('\\');

				if (nIndex != -1) 
					strDest = strFile3 + '\\' + strFile2.Mid (nIndex + 1);
			}

			if (::CopyFile (strFile2, strDest, FALSE))
				strUpdated += strDest + _T ("\n");
			else
				strFailed += _T ("Failed to copy ") + strFile2 + _T (" to ") + strDest + _T ("\n");

			if (strFailed.GetLength ()) 
				MessageBox (_T ("Failed: ") + strFailed);
			else
				MessageBox (_T ("File successfully updated: ") + strUpdated + _T ("\n[Version]\nProductKey = ") + strKey [0]);

			CDialog::OnOK ();
		}
		else
			MessageBox (_T ("The keys you entered do not match"));
	}
	else
		MessageBox (_T ("Invalid key.\nPlease enter in the following format: ") + ::strDef);
}

void CProdKeyDlg::OnBrowse() 
{
	CString strFile;

	GetDlgItemText (TXT_FILE, strFile);

	CFileDialog dlg (TRUE, _T ("*.ini"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("INI files (*.ini)|*.ini|All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();
		SetDlgItemText (TXT_FILE, strFile);
		theApp.WriteProfileString (_T ("ProdKey"), _T ("File"), strFile);
	}
}

BOOL CProdKeyDlg::PreTranslateMessage(MSG* pMsg) 
{
	return CDialog::PreTranslateMessage(pMsg);
}

void CProdKeyDlg::OnBrowse2() 
{
	CString strFile;

	GetDlgItemText (TXT_FILE2, strFile);

	CFileDialog dlg (TRUE, _T ("*.inf"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("INF files (*.inf)|*.inf|All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();
		SetDlgItemText (TXT_FILE2, strFile);
		theApp.WriteProfileString (_T ("ProdKey"), _T ("File2"), strFile);
	}
}

void CProdKeyDlg::OnBrowse3() 
{
	CString strFile;

	GetDlgItemText (TXT_FILE3, strFile);

	CoInitialize(NULL);

	BROWSEINFO bfi;
	memset(&bfi, 0, sizeof(BROWSEINFO));
	bfi.hwndOwner = m_hWnd;
	bfi.ulFlags |= BIF_EDITBOX;

	LPITEMIDLIST pidl = SHBrowseForFolder((LPBROWSEINFO)&bfi);
	LPTSTR pszPath = new TCHAR[MAX_PATH];

	if (pidl) {
		SHGetPathFromIDList(pidl, pszPath);
		strFile = pszPath;
		SetDlgItemText (TXT_FILE3, strFile);
		theApp.WriteProfileString (_T ("ProdKey"), _T ("File3"), strFile);
	}

	IMalloc* memAllocator = NULL;
	HRESULT hr = CoGetMalloc(1, &memAllocator);
	memAllocator->Free((void*)pidl);

	CoUninitialize();
}

void CProdKeyDlg::OnBrowse4() 
{
	CString strFile;

	GetDlgItemText (TXT_FILE4, strFile);

	CFileDialog dlg (TRUE, _T ("*.txt"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("TXT files (*.txt)|*.txt|All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();
		SetDlgItemText (TXT_FILE4, strFile);
		theApp.WriteProfileString (_T ("ProdKey"), _T ("File4"), strFile);
	}
}

bool CProdKeyDlg::WriteFile (const CString & strFile, const CString & strKey)
{
	if (FILE * fp = _tfopen (strFile, _T ("r"))) {
		TCHAR sz [512];
		CString str;

		while (!feof (fp)) {
			ZeroMemory (sz, sizeof (sz));
			fgets (sz, sizeof (sz), fp);
			str += sz;
		}

		fclose (fp);

		LPCTSTR lpszKey = _T ("ProductKey = ");

		int nIndex = str.Find (lpszKey);

		if (nIndex == -1) 
			str += _T ("\n[Version]\n") + CString (lpszKey) + strKey;
		else {
			CString strReplace;

			for (int i = nIndex; (i < str.GetLength ()) && (str [i] != '\n'); i++)
				strReplace += str [i];

			int nReplace = str.Replace (strReplace, lpszKey + strKey);

			if (nReplace == 0)
				return false;
		}

		if (FILE * fp = _tfopen (strFile, _T ("w"))) {
			fwrite ((LPVOID)(LPCTSTR)str, str.GetLength (), 1, fp);
			fclose (fp);
			return true;
		}
	}

	return false;
}