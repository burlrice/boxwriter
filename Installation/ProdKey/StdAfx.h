// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__DAD5B600_38A1_4EC1_8155_2AE1E3E95C44__INCLUDED_)
#define AFX_STDAFX_H__DAD5B600_38A1_4EC1_8155_2AE1E3E95C44__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#ifdef _DEBUG
	#define TRACEF(s) Trace (s, __FILE__, __LINE__)
#else
	#define TRACEF(s)
#endif

void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine);


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__DAD5B600_38A1_4EC1_8155_2AE1E3E95C44__INCLUDED_)
