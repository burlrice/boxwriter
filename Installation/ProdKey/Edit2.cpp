// Edit2.cpp : implementation file
//

#include "stdafx.h"
#include "ProdKey.h"
#include "Edit2.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEdit2

CEdit2::CEdit2()
{
}

CEdit2::~CEdit2()
{
}


BEGIN_MESSAGE_MAP(CEdit2, CEdit)
	//{{AFX_MSG_MAP(CEdit2)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEdit2 message handlers

LRESULT CEdit2::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	#ifndef _DEBUG
	if (message == WM_PASTE) {
		return 0;
	}
	#endif

	return CEdit::WindowProc(message, wParam, lParam);
}
