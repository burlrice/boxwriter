//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ProdKey.rc
//
#define BTN_BROWSE                      3
#define BTN_BROWSE2                     4
#define BTN_BROWSE3                     5
#define BTN_BROWSE4                     6
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PRODKEY_DIALOG              102
#define IDR_MAINFRAME                   128
#define TXT_FILE                        1000
#define TXT_KEY1                        1001
#define TXT_KEY2                        1002
#define TXT_FILE2                       1003
#define TXT_FILE3                       1004
#define TXT_FILE4                       1005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
