// ProdKey.h : main header file for the PRODKEY application
//

#if !defined(AFX_PRODKEY_H__93030432_C269_48C0_A135_AD5BC764F3A5__INCLUDED_)
#define AFX_PRODKEY_H__93030432_C269_48C0_A135_AD5BC764F3A5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CProdKeyApp:
// See ProdKey.cpp for the implementation of this class
//

class CProdKeyApp : public CWinApp
{
public:
	CProdKeyApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProdKeyApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CProdKeyApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRODKEY_H__93030432_C269_48C0_A135_AD5BC764F3A5__INCLUDED_)
