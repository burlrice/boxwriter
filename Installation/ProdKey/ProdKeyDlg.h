// ProdKeyDlg.h : header file
//

#if !defined(AFX_PRODKEYDLG_H__538B4F5A_8565_4D14_B507_D2DA2E70D03C__INCLUDED_)
#define AFX_PRODKEYDLG_H__538B4F5A_8565_4D14_B507_D2DA2E70D03C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Edit2.h"

/////////////////////////////////////////////////////////////////////////////
// CProdKeyDlg dialog

class CProdKeyDlg : public CDialog
{
// Construction
public:
	CProdKeyDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CProdKeyDlg)
	enum { IDD = IDD_PRODKEY_DIALOG };
	CEdit2	m_ctrlKey2;
	CEdit2	m_ctrlKey1;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProdKeyDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	bool WriteFile (const CString & strFile, const CString & strKey);

	// Generated message map functions
	//{{AFX_MSG(CProdKeyDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	afx_msg void OnBrowse();
	afx_msg void OnBrowse2();
	afx_msg void OnBrowse3();
	afx_msg void OnBrowse4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRODKEYDLG_H__538B4F5A_8565_4D14_B507_D2DA2E70D03C__INCLUDED_)
