
// sql_testDlg.cpp : implementation file
//

#include "stdafx.h"
#include "sql_test.h"
#include "sql_testDlg.h"
#include "afxdialogex.h"
#include "afxdb.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define TRACEF(s) Trace ((s), _T (__FILE__), __LINE__);

void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	CString str;

	str.Format (_T ("%s(%d): %s\n"), lpszFile, lLine, lpsz);
	::OutputDebugString (str);
}

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Csql_testDlg dialog




Csql_testDlg::Csql_testDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Csql_testDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Csql_testDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(Csql_testDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &Csql_testDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// Csql_testDlg message handlers

BOOL Csql_testDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void Csql_testDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Csql_testDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Csql_testDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void Csql_testDlg::OnBnClickedOk()
{
	try
	{
		CDatabase db;
		CString str;

		db.OpenEx (_T ("DSN=MarksmanELITE"));

		/*
		str = _T ("")
			_T ("INSERT INTO Lines ")
			_T ("	(Name,\"Desc\",Maintenance,NoRead,ResetScanInfo,MaxConsecutiveNoReads,BufferOffset,SignificantChars,AuxBoxIp,AuxBoxEnabled,SerialParams1,SerialParams2,SerialParams3,AMS32_A1,AMS32_A2,AMS32_A3,AMS32_A4,AMS32_A5,AMS32_A6,AMS32_A7,AMS256_A1,AMS256_A2,AMS256_A3,AMS256_A4,AMS256_A5,AMS256_A6,AMS256_A7,AMS_Interval) ")
			_T ("VALUES ")
			_T ("	('LINE000x','Test line x','12:00','NO READ','1','3','0','5','10.1.2.50','0','9600,8,N,1,0','9600,8,N,1,0','9600,8,N,1,0','3','1000','10','1000','2500','3','20','3','1000','28','1000','5500','2','100','32.000000');");

		str = _T ("")
			_T ("INSERT INTO Lines ")
			_T ("	(ID) ")
			_T ("VALUES ")
			_T ("	('99');");

		str = _T ("INSERT INTO Reports (\"Time\",\"Action\",\"Username\",\"Counts\",\"Line\",\"TaskName\",\"Type\",\"PrinterID\") VALUES ('05/09/05 06:40:52','Modify task','Editor',' ','LINE0001','3d','1','0');");
		*/

		/*
		str = 
			_T ("UPDATE Lines SET ") 
			_T ("Name='LINE0001', ") 
			_T ("\"Desc\"='Test line 1', ") 
			_T ("Maintenance='12:00', ") 
			_T ("NoRead='NO READ', ") 
			_T ("ResetScanInfo='1', ") 
			_T ("MaxConsecutiveNoReads='3', ") 
			_T ("BufferOffset='0', ") 
			_T ("SignificantChars='5', ") 
			_T ("AuxBoxIp='10.1.2.50', ") 
			_T ("AuxBoxEnabled='0', ") 
			_T ("SerialParams1='9600,8,N,1,0', ") 
			_T ("SerialParams2='9600,8,N,1,0', ") 
			_T ("SerialParams3='9600,8,N,1,0', ") 
			_T ("AMS32_A1='3', ") 
			_T ("AMS32_A2='1000', ") 
			_T ("AMS32_A3='10', ") 
			_T ("AMS32_A4='1000', ") 
			_T ("AMS32_A5='2500', ") 
			_T ("AMS32_A6='3', ") 
			_T ("AMS32_A7='20', ") 
			_T ("AMS256_A1='3', ") 
			_T ("AMS256_A2='1000', ") 
			_T ("AMS256_A3='28', ") 
			_T ("AMS256_A4='1000', ") 
			_T ("AMS256_A5='5500', ") 
			_T ("AMS256_A6='2', ") 
			_T ("AMS256_A7='100', ") 
			_T ("AMS_Interval='24.000000', ") 
			_T ("PrinterID='0' ")
			_T ("WHERE [ID]=4;");
		str.Replace (_T ("LINE0001"), CTime::GetCurrentTime ().Format (_T ("%c")));
		str.Replace (_T ("Test line 1"), CTime::GetCurrentTime ().Format (_T ("%m")));

		str = _T ("UPDATE Heads SET [ID]=37 WHERE [ID]=36");
		*/

		str = _T ("INSERT INTO Messages (\"HeadID\",\"TaskID\",\"Name\",\"Desc\",\"Data\",\"Height\",\"IsUpdated\") VALUES ('21','100','768 Sample y',Null,'{Ver,4,10,922,0,7}{Timestamp,07/08/15 16:00:45}{ProdArea,15000,4000,0,21}{Bitmap,17,2802,750,5584,1250,0,0,0,C:\\Program Files\\Foxjet\\MarksmanELITE\\Logos\\FitnessSnacker.bmp,0}{Barcode,18,2666,2135,0,0,0,20,0,1,1 00 48456 16712 7,000000000,840,003,0,2,0,14,14,0{TextObj,DEF,Default barcode sub-element,zzzzzzzzzzzzz,1004845616712,0,0,0,0,0,0,0,0}}{Text,19,3770,1833,0,0,0,MK Arial,10,1,0,ACME NUTRIONALS\, INC.    BAKERSFIELDVILLETON\, IA,0,0,0,0,0,0,0,0}{Text,20,2906,0,0,0,1,Arial,26,1,0, 48456 ,0,26,0,0,0,0,0,0}{Text,21,4125,10,0,0,0,Arial,26,1,0,16712,0,26,0,0,0,0,0,0}{Text,22,2895,437,0,0,0,Arial,20,1,0,24/12-1.00 OZ. CHOCOLATE GRANOLA BARS,0,18,0,0,0,0,0,0}{Bitmap,24,0,10,2084,4000,0,0,0,C:\\Program Files\\Foxjet\\MarksmanELITE\\Logos\\nutri.bmp,0}{ExpDate,25,5302,83,0,0,0,Arial,20,1,0,EXPIRES: %m/%d/%Y,120,0,0,0,0,17,0,0,0,0}{Text,26,10416,0,0,0,0,Arial,256,1,0,Y,0,0,0,0,0,0,0,0}','0','1');");

		TRACEF (str);
		db.ExecuteSQL (str); 

		/*
		CString str [] = 
		{
			_T ("BarcodeScans"),
			_T ("Boxes"),
			_T ("BoxToLine"),
			_T ("Heads"),
			_T ("Images"),
			_T ("Panels"),
			_T ("Lines"),
			_T ("Messages"),
			_T ("Tasks"),
			_T ("Options"),
			_T ("OptionsToGroup"),
			_T ("Printers"),
			_T ("Reports"),
			_T ("SecurityGroups"),
			_T ("Settings"),
			_T ("Shifts"),
			_T ("Users"),
		};

		for (int i = 0; i < ARRAYSIZE (str); i++) {
			TRACEF (str [i]);
			db.ExecuteSQL (_T ("DELETE * FROM ") + str [i]); 
		}
		*/
	}
	catch (CDBException * e) { e->ReportError (); e->Delete (); }
}
