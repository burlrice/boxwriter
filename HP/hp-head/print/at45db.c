#include "at45db.h"
#include "uart.h"

#ifndef WIN32
#include "processor.h"
#include "TYPES.H"
#include "uart.h"
#else
#include <stdio.h>
#include <string.h>

char fnt_tbl[1024]={0};
char * fnt_lst[32];
unsigned char numfnts=0, readndx;
FILE * fntfile=NULL;
unsigned char data[32];

unsigned char spi_readb(void) {
    unsigned char ret = 0;
    /*if (readndx < sizeof(data))*/ ret = data[readndx++];
    /*else {
        printf("Out of bounds Failure: %d < %d\n", readndx, sizeof(data));
        exit(-1);
        ret = 0xff;
    }*/
    return ret;
}
void spi_write_addr(unsigned long a) {
    // open fontfile
    unsigned int fnt = (a >> 20) & 0xf;
    unsigned char chr = (a >> 12) & 0xff;
    int col = (a >> 5 ) & 0x7f;
    if (fnt && ((fnt-2) < numfnts)) {
        char buf[255]= "";
        sprintf(buf, "/src/tools/fonted/%s.hfnt", fnt_lst[fnt-1]);
        fntfile = fopen(buf, "rb");
    }
    if (fntfile) {
        unsigned short version, height;
        unsigned short start, end;
        unsigned long offset, cols;
        fread(&version, sizeof(version), 1, fntfile); // read version
        fread(&height, sizeof(height), 1, fntfile); // read version
        height = swapw(height);
        fread(&start, sizeof(start), 1, fntfile); // read version
        start = swapw(start);
        fread(&end, sizeof(end), 1, fntfile); // read version
        end = swapw(end);
        unsigned long ndx = (chr - start - 1)*sizeof(long);
        fseek(fntfile, ndx, SEEK_CUR);
        fread(&offset, sizeof(offset), 1, fntfile);
        offset = swapl(offset);
        fread(&cols, sizeof(cols), 1, fntfile);
        cols = swapl(cols) - offset;
        ndx = (end - chr)*sizeof(long);
        unsigned char bytes = ((height - 1) / 8) + 1;
        if (0 == col) {
            data[0] = bytes;
            data[1] = cols >> 8;
            data[2] = cols;
        } else {
            ndx += (offset + ((col-1))) * (((height - 1)/16) + 1) * 2;
            fseek(fntfile, ndx, SEEK_CUR);
            fread(&data, bytes, 1, fntfile);
        }
        fclose(fntfile);
        fntfile = NULL;
        readndx = 0;
    } else if ((0 == fnt) && (a < numfnts)) {
        strcpy(data, fnt_lst[a/FNAME_SZE]);
    }
}
#endif // WIN32

void at45db_init(void)
{
#ifndef WIN32
    unsigned char c;
    SSP1STATbits.SMP = 0; // input is valid in the middle of clock
    SSP1STATbits.CKE = 0; // rising edge is data capture
    SSP1CON1bits.CKP = 1; // high value is passive state
    SSP1CON1bits.SSPM0 = 0; // speed f/4(10MHz), Master
    SSP1CON1bits.SSPM1 = 0; // speed f/4(10MHz), Master
    SSP1CON1bits.SSPM2 = 0; // speed f/4(10MHz), Master
    SSP1CON1bits.SSPM3 = 0; // speed f/4(10MHz), Master
    SSP1CON1bits.SSPEN = 1; // enable SPI
    spi_close();
    spi_open();
    spi_write_byte(READ_STATUS);
    spi_read_byte(c);
    spi_close();
    if (0 == (c&1)) {
        spi_open();
        spi_write_byte(0x3d); spi_write_byte(0x2a); spi_write_byte(0x80); spi_write_byte(0xa6);
        spi_close();
        wait_while_busy();
        puts((ROM unsigned char *)"AT45DB page size has been set to 512. Please cycle power.\r\n");
        for(;;){}
    }
#else
    fntfile = fopen("font_tab.txt", "r");
    numfnts = 0;
    if (fntfile) {
        fread(&fnt_tbl, sizeof(fnt_tbl), 1, fntfile);
        fclose(fntfile);
        char * pc = fnt_tbl;
        while (*pc) {
            fnt_lst[numfnts++] = pc;
            pc = strchr(pc, '\n');
            if (pc) *pc++ = '\0';
        }
    }
    fntfile = NULL;
#endif
}


