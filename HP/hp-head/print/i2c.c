/***********************************************************\
*                                                           *
*   Program:    I2C driver                                  *
*                                                           *
\***********************************************************/

#include "processor.h"
#include "i2c.h"

unsigned char i2c_ack;

unsigned char i2c_get_ack(void){
  return i2c_ack;
}

void i2c_start(void){
  SCL_1; Nop(); //Nop(); // TODO: needs to be re-optimized
  while (0 == R_SCL); // wait until slave releases clock
  SDA_0; Nop(); Nop(); Nop(); Nop(); Nop();
  SCL_0; SDA_1;
}

void i2c_stop(void){
  SDA_0; Nop(); Nop();
  SCL_1; Nop(); Nop(); Nop(); Nop(); //Nop(); // TODO: needs to be re-optimized
  while (0 == R_SCL); // wait until slave releases clock
  SDA_1;
}

void i2c_init(){
#ifdef WIN32
    R_SCL = 1;
#else
  LATBbits.LATB6 = 0; //when set to output, line will be pulled low
  SCL_1;
  LATBbits.LATB7 = 0; //when set to output, line will be pulled low
  SDA_1;
#endif
}

void i2c_write(unsigned char byte)
{
  unsigned char bit = 0x80;
  do {
    if (byte & bit){
      SDA_1;
    } else {
      SDA_0;
      Nop();
    }
    Nop(); Nop(); Nop(); Nop(); Nop();
    SCL_1;
    bit>>=1;
    Nop(); Nop(); Nop(); Nop();
    while (0 == R_SCL); // wait until slave releases clock
    SCL_0;
  } while (bit);
  SDA_1;//release data line before pulling SCL low
  Nop(); Nop(); Nop(); Nop();
  Nop(); Nop(); Nop(); Nop();
  Nop(); Nop(); Nop(); Nop();
  SCL_1;
  Nop(); Nop();
  /* get ack */
  if (R_SDA)
    i2c_ack=NO_ACK;
  else
    i2c_ack=ACK;
  SCL_0;
  Nop(); Nop(); //data hold 300nS
  SDA_1;
}

unsigned char i2c_read(unsigned char ack)
{
  unsigned char bit = 0x80;
  unsigned char byte=0;
  SCL_1;
  Nop(); Nop(); Nop();
  while (0 == R_SCL); // wait until slave releases clock
  do {
    if (R_SDA) {
      Nop();
      SCL_0;
      byte|=bit;
      bit>>=1;
      if (bit) {
        Nop(); Nop(); Nop();
        SCL_1;
      } else {
        break;
      }
    } else {
      SCL_0;
      bit>>=1;
      if (bit) {
        Nop(); Nop(); Nop(); Nop();
        Nop(); Nop(); Nop(); Nop();
        Nop(); Nop(); Nop();
        SCL_1;
        Nop(); Nop();
      } else {
        Nop(); Nop(); Nop();
        Nop(); Nop(); Nop();
        Nop(); Nop();
        break;
      }
    }
  } while (1);
  if(ack)
    SDA_0; //send ack
  Nop(); Nop(); Nop();
  Nop(); Nop(); Nop();
  SCL_1;
  Nop(); Nop(); Nop();
  Nop(); Nop(); Nop();
  Nop(); Nop();
  SCL_0;
  Nop(); Nop(); //data hold 300nS
  SDA_1;
  return(byte);
}

