#include "types.h"
#define getchar UART_ReceiveByte
#define putchar UART_SendByte
char getchar(void);
void putchar(UCHAR);

#ifdef PIC
#define kbhit()		PIR1bits.RCIF
void puts(const RUCHAR *);
#define NextChar() getchar()
#elif defined(AVR)
#define kbhit() bit_is_set(UCSR0A, RXC)
#elif defined(WIN32)
#define kbhit() (!feof(infile))
#define NextChar() fgetc(infile)
#endif

unsigned char UART_init(UCHAR bitrate);
void printHex(unsigned char c);
void chkUart(UCHAR);
#define printShortHex(a) printHex(a>>8); printHex(a)
#define printLongHex(a) printShortHex(a>>16); printShortHex(a)
