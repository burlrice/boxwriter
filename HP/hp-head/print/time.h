#ifndef __TIME_H__
#define __TIME_H__
#include "types.h"
struct tm {
   unsigned char tm_sec;     /* seconds after the minute - [0,59]    */
   unsigned char tm_min;     /* minutes after the hour - [0,59]      */
   unsigned char tm_hour;    /* hours since midnight - [0,23]        */
   unsigned char tm_mday;    /* day of the month - [1,31]            */
   unsigned char tm_mon;     /* months since January - [1,12]        */
   unsigned char tm_year;    /* years since 2000                     */
   unsigned char tm_wday;    /* days since Sunday - [0,6]            */
};

extern struct tm time;
extern unsigned short nFrito;
extern void ConvertDate(struct tm *, UCHAR *, int);
extern void ExpOffset(struct tm * tme, unsigned short offset);
// driver functions
extern void rtc_set_date_time(struct tm * tme);
extern void rtc_get_time(struct tm * tme);
//extern unsigned short CalcDay (char cMonth, char cDay, char cYear);
#endif
