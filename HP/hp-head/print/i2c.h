#ifndef __I2C__
#define __I2C__
/************************************************************\
*                                                            *
*   Program:    I2C header                                   *
*                                                            *
\************************************************************/

//#include "processor.h"
#define I2C_RD    (1)
#define I2C_WR    (0)
#define NO_ACK 0
#define ACK    1
#define uDLY 1
#define SDA_0 {TRISBbits.TRISB7 = 0;} //when set to output, line will be driven low
#define SDA_1 {TRISBbits.TRISB7 = 1;} //when set to input, line will be pulled high
#define SCL_0 {TRISBbits.TRISB6 = 0;} //when set to output, line will be driven low
#define SCL_1 {TRISBbits.TRISB6 = 1;} //when set to input, line will be pulled high
#define R_SDA (PORTBbits.RB7) //LATBbits.LATB7)
#define R_SCL (PORTBbits.RB6) //LATBbits.LATB7)

unsigned char i2c_get_ack(void);
void i2c_delay(unsigned char n);
void i2c_start(void);
void i2c_stop(void);
void i2c_init(void);
void i2c_write(unsigned char byte);
unsigned char i2c_read(unsigned char last);
#endif
