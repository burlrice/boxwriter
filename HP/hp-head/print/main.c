#include <stdlib.h>
#ifndef WIN32
#include <timers.h>
#endif
#include "processor.h"
#include "types.h"
#include "uart.h"
#include "print.h"
#include "at45db.h"
#include "fm24cl16.h"
#include "i2c.h"
#include "time.h"

unsigned short time1msb, linespeed=0;
unsigned char state, direction = 0, cntr=0, ink_status = -1;
unsigned char act=0;
void usleep(unsigned short us);
void rtc_init(void);
#define ADDR (0x1c) //(0xD0)
#define ACTIVE (act)
#ifdef PIC
#define MAINRET void
#define MAINARGS void
#define REBOOT() { /*_asm reset _endasm*/ \
            TXSTA1bits.TXEN = 0; \
            i2c_start(); \
            i2c_write(ADDR); \
            i2c_write(0x80); \
}
#define RCHAR(a) ((RUCHAR *)(a))
rom unsigned char * getVersion(void);
void IRQHandlerHigh(void);
void IRQHandlerLow(void);
#define ProcessArgs()
//#define ACTIVE (TXSTA1bits.TXEN)
#elif defined(AVR)
#define MAINARGS void
#define MAINRET int 
#define REBOOT()
#define RCHAR
#define itoa(a,b) itoa(a,b,10)
#elif defined(WIN32)
#include <string.h>
#define MAINRET int 
#define MAINARGS int argc, char * argv[]
#define REBOOT() { puts("\n"); exit(0); }
#define RCHAR
#define ultoa(a,b) ultoa(a,b,10)
FILE * infile=NULL;
unsigned char * getVersion(void);
#define ProcessArgs() { \
    int i=0; \
    infile = stdin; \
    for (i=1; i < argc; i++) { \
        if (0 == strcmp(argv[i], "-")) infile = stdin; \
        else infile = fopen(argv[i], "rt"); \
    } \
}
#endif

unsigned char address = 0xFF;

#define UART_INIT_VAL (0x2a) // 40Mhz due to pll
void init(void) {
#ifdef PIC
    PORTA = 0x3f;
#ifdef NEWBRD
    //TRISA = 0x1f; 
    TRISA = 0x37; 
#else
    TRISA = 0x3f;
#endif

//    ADCON1 = 0x0f; // for trigger and prtgate
    ADCON0 = 0x00;	// Select Channel AN0		 		//PJ
    ADCON1 = 0x0e;      // select AN0 analog, AN1 digital
    ADCON2 = 0x22;	// Lt justified, 4 Tad and Fosc/32 clk		//PJ
    ADCON0bits.ADON = 1; // Enable the A/D
    
    CMCON = 0x07; // config

//    PORTB = 0xc3;
//    PORTB = 0xc2;
    PORTB = 0xd2;
//    TRISB = 0xc3;
//    TRISB = 0xc2;
#ifdef NEWBRD
    TRISB = 0x63;
#else
    TRISB = 0x42; // using pgming signal
#endif

    PORTC = 0xd2;
//    TRISC = 0xc2; // use this for unmodified V3 hardware
    TRISC = 0xd2;//0xc2;

    PORTD = 0;
    TRISD = 0;

    PORTE = 0x04;
    TRISE = 0;
#endif

    UART_init(UART_INIT_VAL);
    //UART_init(0x0a); // Open communications for default OSCTUNE=0/10Mhz

#ifdef PIC
    // init Rx irq
    PIE1bits.RCIE = 1;
    IPR1bits.RCIP = 0;

    OSCTUNE = 0x40; // 40Mhz due to pll
    //IOOpen(0x0a);		// Open communications for default OSCTUNE=0/10Mhz
    // INT1 setup
    //INTCON2bits.INTEDG1 = 0;
    //INTCON3bits.INT1IP = 1;
    //INTCON3bits.INT1IE = 1;
    RCONbits.IPEN = 1;

    // Timer0 setup
    T0CONbits.T08BIT = 1; // 8 bit timer
    T0CONbits.T0CS = 0; // select internal clock
    T0CONbits.PSA = 0; // select prescaler
    T0CONbits.T0PS2 = 1; // 
    T0CONbits.T0PS1 = 0; // 
    T0CONbits.T0PS0 = 0; // 
    INTCONbits.TMR0IF = 0; //clear timer 0 interrupt flag
    T0CONbits.TMR0ON = 1; //start timer 0
    INTCON2bits.TMR0IP = 0; //timer 0 interrupt low priority
    INTCONbits.TMR0IE = 1; //enable timer 0 interrupt
    INTCONbits.GIEL = 1; //enable interrupts

    //TIMER1 setup
    state = NO_PHOTO;
    T1CONbits.RD16 = 1; //select 16-bit read/writes to timer 1
    T1CONbits.T1CKPS1 = 0; T1CONbits.T1CKPS0 = 0; //use a 1:1 prescale for timer 1
    T1CONbits.TMR1CS = 0; //internal clock (Fosc/4)
    PIE1bits.TMR1IE = 0; //disable the timer 1 overflow interrupt
    PIE1bits.CCP1IE = 0; //disable the CCP interrupts
    T1CONbits.TMR1ON = 0; //stop timer 1
    TMR1H = 0; TMR1L = 0; //initialize timer 1 register to zero
    CCP1CON = 0; //reset CCP hardware
    //load the CCP compare registers
    CCPR1L = LOBYTE((unsigned int)PHOTO_SAMPLE_RATE); CCPR1H = HIBYTE((unsigned int)PHOTO_SAMPLE_RATE);
    CCP1CON = 0x0B; //compare mode, set interrupt flag and reset timer on match
    T1CONbits.TMR1ON = 1; //start timer 1
    IPR1bits.CCP1IP = 1; //set interrupt priority to high
    PIE1bits.CCP1IE = 1; //enable the CCP1 interrupt

    //INT1 setup
    INTCON2bits.INTEDG1 = 1; //rising edge
    INTCON3bits.INT1IP = 1; //high priority
    INTCON3bits.INT1IE = 0; //disable external encoder interrupt
#endif
}

void WriteActive(UCHAR x) {
    // send i2c command to change TX state
    ACTIVE = x;
    TXSTA1bits.TXEN = 0;
    i2c_start();
    i2c_write(ADDR);
    i2c_write(ACTIVE);
    i2c_stop();
    TXSTA1bits.TXEN = 1;
}

unsigned short GetVal(UCHAR *pc, UCHAR sze) {
    unsigned short val;
    UCHAR c;
    val = 0;
    while (sze--) {
        c = *pc++;
        if (('0' <= c) && (c <= '9')) 
            val = val * 10 + (c - '0'); 
        else 
            break;
    }
    return val;
}

RUCHAR DateFmt[] = "MMDDhhmmYYss";
extern void DnLoadFile(unsigned char fnt, unsigned char * name);

void PrintLong(/*ROM UCHAR *txt,*/ unsigned long x) {
    UCHAR buf[11], c, l;
    //puts(txt);
    //ultoa(x, (char *) buf);
    l = 0;
    while (x) {
        c = x % 10;
        buf[l++] = '0' + c;
        x /= 10;
    }
    while (l--) putchar(buf[l]);
    //p = buf;
    //while (*p) putchar(*p++);
    putchar('\n');
}
#define STATUS(a, b, c) { putchar(a); putchar(b); putchar(c); putchar('\n'); }

void DumpData(unsigned short addr, UCHAR bufr[]) {
    UCHAR i, c;
    printShortHex(addr); putchar(':');
    for (i=0; i < 16; i++) {
        printHex(bufr[i]);
        if (0x3 == (0x3 & i)) putchar(' ');
    }
    putchar('|'); putchar(' ');
    for (i=0; i < 16; i++) {
        c = bufr[i];
        if (' ' > c || c > '~') c = '.';
        putchar(c);
        if (0x3 == (0x3 & i)) putchar(' ');
    }
    putchar('\n');
}

extern void printVersion(void);
extern unsigned char left, right;
#define ESC (0x1b)
void ProcessCmd(UCHAR * buf) {
    RUCHAR * rpc;
    UCHAR *p, c, x;
    unsigned short tmp;
	char d, m, y;

    //if (ESC != *buf++) return;
    if (!ACTIVE) return;
    //while ((c = *buf++)) {
    c = *buf++;
    if (!c) return;
        switch (c) {
        case 'b':
            REBOOT();
            break;
        case 't': {
            time.tm_mon = GetVal(buf,2);
            buf+=2;
            time.tm_mday = GetVal(buf,2);
            buf+=2;
            time.tm_hour = GetVal(buf,2);
            buf+=2;
            time.tm_min = GetVal(buf,2);
            buf+=2;
            time.tm_year = GetVal(buf,2);
            buf+=2;
            time.tm_sec = 0;
            rtc_set_date_time(&time);
            break;
        }
/*
        case 'T': {
			// 012345
			// MMDDYY
			y = GetVal (buf [4], 2);
			m = GetVal (buf [0], 2);
			d = GetVal (buf [2], 2);
			nFrito = CalcDay (m, d, y);
            break;
        }
*/
        case 'x':
            c = GetVal(buf, 2);
            buf += 2;e
            DnLoadFile(c, buf);
            putchar('O'); putchar('K');
            putchar('\n');
            while (!TXSTAbits.TRMT) ; // wait till it is done txmting
            REBOOT();
            break;
        case 'z':
            ClearBuffers();
            break;
        case 'h':
        case 'v':
        case 'a':
        case 'f':
        case 'c':
        case '#':
            if (!ParseSubCmd(c, buf)) append(buf-1);
            return;

        case 'p': 
            buf = ParsePrintParams(buf);
            break;
        case 's': {
            putchar('\n');
            //puts(RCHAR("\r\n"));
            switch (*buf++) {
            case 'b':
                DumpFields();
                break;
            /*case 'f':
                c = 0;
                while (1) {
                    GetFileName(buf, c);
                    if (!*buf) break;
                    p = buf;
                    printHex(c+1); putchar(':');
                    while (*p) putchar(*p++);
                    putchar('\n');
                    c++;
                }
                //DumpCharData(0, 0);
                break;*/
            case 's':
                printVersion();
                //STATUS('a', ':', address);
                x = ink_status; //CheckPrintHd(0); //1);
                putchar('i'); putchar(':');
                if (0 == x) c = 'g';
                else if (1 == x) c = 'l';
                else if (3 == x) c = 'o';
                else c = '?';
                putchar(c);
                putchar('\n');
                //PrintLong(/*RCHAR("d:"),*/ TotalDots);
                if (!left && !right) c = 'b';
                else if (!left) c = 'l';
                else if (!right) c = 'r';
                else if (FlagBits.extphoto && EXTPHOTO) c = 'e';
                else c = 'o';
                STATUS('f', ':', c);

                putchar('t');
                rpc = DateFmt; //RCHAR("MM/DD/YYYY hh:mm:ss");
                p = buf;
                while (*rpc) *p++ = *rpc++;
                *p=0;
                ConvertDate(&time, buf, sizeof(DateFmt));
                p = buf;
                while (*p) putchar(*p++);
                putchar('\n');

                tmp = 0;
                if ((RIGHT_TO_LEFT == state) || (LEFT_TO_RIGHT == state))
                    tmp = time1msb; //linespeed;
                if (tmp) tmp = SPEED_FACTOR/tmp; 
                putchar('p'); putchar('s');
                PrintLong(/*RCHAR("ps"),*/ tmp);
                if (LEFT_TO_RIGHT == direction) c = 'l';
                else if (RIGHT_TO_LEFT == direction) c = 'r';
                else c = '0';
                STATUS('p', 'd', c);
                STATUS('p', 'f', '0' + FlagBits.extphoto);
                STATUS('p', 'e', '0' + FlagBits.extencoder);
                STATUS('p', 'p', '0' + FlagBits.pause);
                break;
            }
            //putchar('\n');
            break;
        }
//////////////////////////////////////////////////////////////////////////
#if 0
        case 'i': {
#ifdef WIN32
            extern void Print2BMP(unsigned char);
            Print2BMP(*buf++);
#else
            if (linespeed && direction) {
                state = direction;
                CCPR1L = LOBYTE((unsigned int)linespeed);
                CCPR1H = HIBYTE((unsigned int)linespeed);
                refresh_var = 1;
            }
#endif
            break;
        }
#endif
#ifdef BURN_FUSES
        case 'w': {
void WritePrintHd(UCHAR * buf);
            WritePrintHd(buf);
            break;
        }
#endif
#if 1 //def DEBUG
//        case 'e':
//            Fram_OpenWrite(0);
//            tmp = 2048;
//            while (tmp--) Fram_Write(0);
//            Fram_Close();
//            break;
//#ifdef DEBUG
        case 'm':
            c = *buf++;
            tmp = 0;
            if (*buf) {
                tmp = GetVal(buf, 4);
                buf+=4;
            }
            putchar('\n');
            if ('c' == c) CheckPrintHd(1, tmp);
            else if ('m' == c) {
                //DumpRam(x);
                for (x=0; x < 16; x++) {
                    DumpData(tmp, (UCHAR *)tmp);
                    tmp += 16;
                    if (1024 < tmp) break;
                }
            } else if ('r' == c) {
                for (x=0; x < 16; x++) {
                    Fram_OpenRead(tmp);
                    for (c=0; c < 15; c++) buf[c] = Fram_Read();
                    buf[c++] = Fram_ReadLast();
                    Fram_Close();
                    DumpData(tmp, buf);
                    tmp += c;
                    if (2048 < tmp) break;
                }
            }
            break;
//        case 'n': // fnt info
//            c = *buf++ - '0';
//            tmp = GetVal(buf, 3); //*buf++;
//            //if (*buf) *buf-='0';
//            DumpCharData(c/*+1*/, tmp, 0); //*buf++);
//            break;
//#endif
#define LED_CMD (0x10)
//        case 'F':
//            if ('0' <= *buf && *buf <= '3') {
//                c = (*buf - '0');
//                i2c_start();
//                i2c_write(ADDR);
//                i2c_write(c|LED_CMD);
//                i2c_stop();
//            }
//            break;
#endif
//////////////////////////////////////////////////////////////////////////
        default:
            break;
        }
//    }
}

volatile UCHAR rxbuf[66], iptr=0, optr=0;
enum {RX_START=0, RX_RECV, RX_INVALID};
unsigned char rxstate=RX_START;

void usleep(unsigned short us) {
    if (us<3) return;
    us -= 3;
    while (us) {
        us--; Nop();
    }
}
#ifdef WIN32
#define CHK_INPUT() 
#else
#define CHK_INPUT() 
#endif
#define INTERVAL (10) //(0x3)
MAINRET main(MAINARGS) {
    UCHAR c, chk_phd_cnt, purgecnt;
    ProcessArgs();

    chk_phd_cnt = 0;
    purgecnt = 0;
    init();
    at45db_init();
    i2c_init();
    rtc_init();
    WriteActive(ACTIVE);

    rtc_get_time(&time); // update date/time
    print_init();

    if (ACTIVE) putchar('#');
    INTCONbits.GIEH = 1; //enable interrupts
    while(1) {
        if (!chk_phd_cnt && !position) {
//LATBbits.LATB2 = 1;
             c = CheckPrintHd(0, 0);
            if (ink_status != c) {
                // update the led
                i2c_start();
                i2c_write(ADDR);
                i2c_write(c|LED_CMD);
                i2c_stop();
                ink_status = c; // & 0x3;
            }
            chk_phd_cnt = INTERVAL; // reset counter
//LATBbits.LATB2 = 0;
        }

        if ((refresh_var) && !purgecnt) {
            if (2 != ink_status) refresh();
            refresh_var = 0;
        }
        if (update_var) { // runs every 210 ms
            rtc_get_time(&time);
            // check phd every .210 * INTERVAL seconds
            if (chk_phd_cnt) chk_phd_cnt--;
            // read the state of the purge button
            i2c_start();
            i2c_write(ADDR|1); //0x03);
            c = i2c_read(NO_ACK); // if 0xff then comm failed
            i2c_stop();
            //ACTIVE = (current_state >> 1) & 1;
            if (c & 0x01) {
               if (3==purgecnt) purge();
               else purgecnt++;
            } else {
                if (purgecnt) purgecnt--;
            }
            // done with purge button check
            update_var = 0;
        }

	// Get any data from the input stream
        while (1) {
            ///////////////////////////////////////////////
            c = 0;
            CHK_INPUT();
            INTCONbits.GIEH = 0;
            if (optr != iptr) {
                if ((optr > iptr) && ACTIVE) {
                    optr--;
                    c = '\b';
                } else
                    c = rxbuf[optr++];
            }
            INTCONbits.GIEH = 1;
            if (!c) break;
            ///////////////////////////////////////////////
            if (!ACTIVE) {
                WriteActive(1);
                usleep(100); // see icl3221 datasheet
                putchar(address);
            }

            if (((' ' <= c) || ('\b' == c)) && (ACTIVE)) putchar(c);
            if ('\r' == c || '\n' == c) {
                //putchar('\n');
                if (0 < optr) rxbuf[optr-1] = '\0';
                ProcessCmd((UCHAR *)rxbuf);
                putchar('\n');
                while (!TXSTAbits.TRMT) ; /* wait till it is done txmting */
                WriteActive(0);
            }
        }
    }
}

#ifdef PIC
//#pragma code ivec_high = 0x808
#pragma code ivec_high = 0x008
void ivec_high (void) {
    _asm goto IRQHandlerHigh _endasm;
}

//#pragma code ivec_low = 0x818
#pragma code ivec_low = 0x018
void ivec_low (void) {
    _asm goto IRQHandlerLow _endasm;
}
#pragma code

#define RESOLUTION 5

unsigned char previous_state = 0;
unsigned char latch_a_bit3 = 0;
unsigned char period_t = 0;
unsigned char period_tin = 0;
unsigned char period_tout = 0;

#pragma interruptlow IRQHandlerLow
void IRQHandlerLow(void) {
    unsigned char tmp, byte;
    INTCONbits.GIEH = 0;
    if (PIR1bits.RCIF) {
//if (RCSTA1bits.OERR) LATBbits.LATB2 = 1;
//LATBbits.LATB2 = 1;
        byte = RCREG1;
        INTCONbits.GIEH = 1;
        switch(rxstate) {
        case RX_START:
            if (address == byte) {
                rxstate = RX_RECV;
                iptr = optr = 0;
            } else {
                rxstate = RX_INVALID;
            }
            break;
        case RX_RECV:
            if ('\b' == byte) {
                if (iptr > 0) rxbuf[iptr--] = '\0';
            } else if (iptr < sizeof(rxbuf)) {
                rxbuf[iptr++] = byte;
            } else {
                rxstate = RX_INVALID;
            }
        //case RX_INVALID:
            break;
        }
        if (('\r' == byte) || ('\n' == byte)) rxstate = RX_START;
//LATBbits.LATB2 = 0;
    }
    INTCONbits.GIEH = 1;

    if (INTCONbits.TMR0IF) {
        INTCONbits.TMR0IF = 0; 
        if (0xff == cntr++) {
            update_var = 1;
        }
        period_tin++;
        period_tout++;
        if (period_tout == (period_t/2)) {
            latch_a_bit3 = 0;
        }
        if (period_tout == period_t) {
            latch_a_bit3 = 1;
            period_tout = 0;
        }
        INTCONbits.GIEH = 0;
        TRISAbits.TRISA3 = 1;
        Nop();Nop();Nop();
        tmp = PORTAbits.RA3;
        LATAbits.LATA3 = latch_a_bit3;
        TRISAbits.TRISA3 = 0;
        INTCONbits.GIEH = 1;
        if ((248 == period_tin) || ((0 == previous_state) && (1 == tmp))) { //rising edge
            address = period_tin + 4;
            address /= 8;
            address = (31 + '0') - address;
//if (period_t != (period_tin - 10)) putchar('0' + address);
            period_t = period_tin - 8;
            period_tin = 0;
        }
        previous_state = tmp;
    }
}

#endif

