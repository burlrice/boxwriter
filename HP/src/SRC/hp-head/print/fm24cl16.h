#ifndef __FM24CL16__
#define __FM24CL16__

#define FRAM_ADDR   (0xa0) //(0xD0)
#define FRAM_READ   (1)
#define FRAM_WRITE  (0)

#define Fram_OpenWrite(a) { \
    i2c_start(); \
    i2c_write(FRAM_ADDR|FRAM_WRITE|((a >> 7) & 0x0e)); \
    i2c_write(a & 0xff); \
}
#define Fram_OpenRead(a) { \
    Fram_OpenWrite(a); \
    i2c_start(); \
    i2c_write(FRAM_ADDR|FRAM_READ|((a >> 7) & 0x0e)); \
}

#define Fram_Read() i2c_read(ACK)
#define Fram_ReadLast() i2c_read(NO_ACK)
#define Fram_Write(x) i2c_write(x)
#define Fram_Close() i2c_stop()

#endif
