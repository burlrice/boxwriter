#include "processor.h"
#include "time.h"
#include <string.h>
#ifdef PIC
//#include <delays.h>
#elif defined(WIN32)
#include <stdio.h>
#endif
#include "types.h"
#include "uart.h"
#include "print.h"

#include "at45db.h"
#include "i2c.h"
#include "fm24cl16.h"
//////////////////////////////////////////////
#define DOT_FRAM (0)
#define PARMS_FRAM (0x10)
#define MFGID_FRAM (PARMS_FRAM+4)
#define BUF_FRAM (0x20)
//////////////////////////////////////////////

#define ADDR_ROWS           (0x16)
//#define VPHP_pwron_delay    (0xa0) /* 320 uS */
//#define VPHP_pwroff_delay   (0x23)
//#define BURN_FUSES

#define fwRdySR() { while(logic_readbusy) ; }
//#define fwSptWr(a) { Sptcmd(a); }

//#define FNTCOLBYTES     (4)
#define FNTCHARWID      (20)
#define NLINES          (2)
#define CARTRDG_DOTS ((unsigned long)(40e-3/29e-12))

#define LEFT_LAST_ON     (1 == LATAbits.LATA5)
#define RIGHT_LAST_ON    (0 == LATAbits.LATA5)
#define PHOTO_ON         (0 == TRISAbits.TRISA5)
#define LEFT_PHOTO_ON()  {PORTAbits.RA5 = 1; TRISAbits.TRISA5 = 0;}
#define RIGHT_PHOTO_ON() {PORTAbits.RA5 = 0; TRISAbits.TRISA5 = 0;}
#define PHOTO_OFF()      {TRISAbits.TRISA5 = 1;}
#define PHOTO_OUT        (PORTBbits.RB5)
#define TIME_OUT_INIT 15
#define TIME_OUT (TIME_OUT_INIT-3)
#ifdef NEWBRD


__cmdRegA cmdRegA;
__cmdRegB cmdRegB;
__FlagBits FlagBits;
static unsigned char column[ADDR_ROWS+1]; //, CURCHAR;
unsigned short CurX = 0, CurY = 0;
short position = 0, photo_offset = 0, LASTCOL = -1;
unsigned short buflen=0;
unsigned long TotalDots=0;
unsigned char ink=0, mfg_id=0;

// Length is at 0 offset
#define PREVOFFSET      (1)
#define INDENTOFFSET    (PREVOFFSET+1)
#define WIDTHOFFSET     (INDENTOFFSET+2)
#define ROWOFFSET       (WIDTHOFFSET+2)
#define BUFOFFSET       (ROWOFFSET+1)
#define OPTSOFFSET      (BUFOFFSET+2)
#define HDRSZE          (OPTSOFFSET+1)

typedef union {
    struct {
        unsigned font:2;
        unsigned type:2;
        unsigned optlen:4;
    };
    unsigned char all;
} Opts;

//#define LINESZE         (96) /* 119 is to big */
//#define LINESZE         (112) /* 119 is to big */
#define LINESZE         (222) /* 119 is to big */

//#define Linetxt(ndx) (0 == ndx ? LineAtxt : LineBtxt)
//#define Linetxt(ndx) *Line[ndx].txt
#define Linetxt(ndx) Linetxt[ndx]

#pragma  idata bigdata
unsigned char Linetxt[NLINES][LINESZE];
#pragma idata

typedef struct {
    //unsigned char txt[LINESZE];
//    unsigned char ** txt;
    unsigned char len;
    unsigned short bytespercol;
    unsigned short curcol;
    unsigned char row;

    // these are for the font
    short x1, x2;

    unsigned char * curchar;
    unsigned char * next;
    Opts opts;
    unsigned char hi_addr;
    unsigned short lo_addr;
    unsigned char cspc;
} __Line;

enum {TEXT=0, SEQUENCE, CLOCK/* last printable type*/, BARCODE};

__Line Line[NLINES] = {
    {//&LineAtxt[0], //"", 
        0, 0, 0, 0, 0},
    {//&LineBtxt[0], //"", 
        0, 0, 0, 0, 0},
};
/*
h0000
v0000
fTArial_150,ITW
h0330
fTArial_75,TJ
v0077
fTArial_75,500
a0500
c0
*/
//#define LSPEED 833 // 200 ft/min @ 300dpi
//#define TLINE1 "\x11\x00\x00\x00\x00\x00\x00\x02" "\xb7\xa1\xd7\xc9\xc0\xc1\xc4\xc5\xa4\xe1\xe9\xe5\xe6\xe9\xf2\xa1\xb2"
//#define TLINE1 "\x1e\x00\x00\x00\x00\x00\x00\xf9" "MM/DD/YY hhmmss"
//#define TLINE1 "\x03\x00\x00\x00\x00\x00\x00\x02" "ITW" \
//               "\x02\x03\x01\x4a\x00\x00\x00\x01" "TJ"
//#define TLINE2 "\x03\x00\x01\x4a\x00\x00\x0b\x01" "500"

void WriteLong(UCHAR offset, unsigned long data) {
    Fram_OpenWrite(offset);
    Fram_Write((data>>24) & 0xff);
    Fram_Write((data>>16)&0xff);
    Fram_Write((data>>8)&0xff);
    Fram_Write(data&0xff);
    Fram_Close();
    ///////////////////////////////////////////////
}

void UpdateDots(void) {
    ///////////////////////////////////////////////
    // Write out the Dot Counter
    if (CARTRDG_DOTS < TotalDots) TotalDots = CARTRDG_DOTS;
    if (!ink) return;
    WriteLong(ink, TotalDots);
}

void ClearRamBuffer(void) {
    int n, j;
    UCHAR *pc;

    for (j=0; j < NLINES; j++) {
        pc = (UCHAR *)&Line[j];
        n = sizeof(__Line);
        while (n--) *pc++ = 0;
        pc = Linetxt(j);
        n = sizeof(Linetxt(j));
        while (n--) *pc++ = 0;
    }

    LASTCOL = -1;
    CurX = CurY = 0;
    FlagBits.continuous = 0;
}

void print_init(void) {
/*#ifdef TLINE1
    int n;
    ROM UCHAR *rpc;
#endif*/
    UCHAR c, buf[65]="", ndx;
    cmdRegA.reg = 0; //0x18;
    cmdRegB.reg = 1;
    // fwSetWarmupOff();
    CPLDcmd(0); CPLDcmd(0x03); // clear registers
    //fwSelectOddBank();
    fwResetSRClear();
    FlagBits.all = 0;
    position = 0;
    photo_offset = 0;
    ClearRamBuffer();
/*#ifdef TLINE1
    rpc = (ROM UCHAR *)(TLINE1);
    for (n = 0; n < sizeof(TLINE1); n++)
        Line[0].txt[n] = *rpc++;
#ifdef TLINE2
    rpc = (ROM UCHAR *)(TLINE2);
    for (n = 0; n < sizeof(TLINE2); n++)
        Line[1].txt[n] = *rpc++;
#endif
#ifdef LSPEED
    linespeed = LSPEED;
#endif
    LASTCOL = 499;
#endif*/
    TRISEbits.TRISE2 = 0; // on/off for power pic
    // Read in the dot counter from fram
    CheckPrintHd(0);

    Fram_OpenRead(PARMS_FRAM);
    linespeed = Fram_Read();
    linespeed <<= 8;
    linespeed |= Fram_Read();
    direction = Fram_Read();
    c = Fram_Read();
    FlagBits.pause = c & 1;
    FlagBits.extphoto = (c >> 1) & 1;
    FlagBits.extencoder = (c >> 2) & 1;
    Fram_ReadLast();
    Fram_Close();

    Fram_OpenRead(MFGID_FRAM);
    mfg_id = Fram_ReadLast();
    Fram_Close();

    buflen = 0;
    buf[0] = '\0';
    ndx = 0;
    Fram_OpenRead(BUF_FRAM);
    while (1) {
        c = Fram_Read();
        if (0xff == c || !c) break;
        if ('\n' == c) {
            ParseSubCmd(buf[0], &buf[1]);
            buflen += ndx + 1;
            ndx = 0;
        } else {
            buf[ndx++] = c;
        }
        buf[ndx] = '\0';
    }
    Fram_ReadLast();
    Fram_Close();
}

#define readShort(var, data) { \
    var = (short)(*((short*)(data))); \
    /*var = *(data);*/ \
    /*var <<= 8;*/ \
    /*var +=*((data)+1);*/ \
}

#define writeShort(arg, val) { \
    *((short*)(arg)) = (short)val; \
    /**(arg) = val >> 8;*/ \
    /**(arg+1) = val;*/ \
}

#if !defined (WIN32)
#define SPI_ADDR(NDX) { \
            spi_open(); \
            SSP1BUF = CONTINUOUS_READ; \
            byte = Line[NDX].hi_addr; \
            while (!SSP1STATbits.BF); \
            SSP1BUF = byte; \
            byte = Line[NDX].lo_addr>>8; \
            while (!SSP1STATbits.BF); \
            SSP1BUF = byte; \
            byte = Line[NDX].lo_addr; \
            while (!SSP1STATbits.BF); \
            SSP1BUF = byte; \
}
#define SPI_ADDR_SETUP(NDX) { \
            SPI_ADDR(NDX) \
            byte = Line[NDX].bytespercol; \
            while (!SSP1STATbits.BF); \
            SSP1BUF = 0xff; \
}
#define GET_SPI() SSP1BUF; SSP1BUF = 0xff
#else
#define SPI_ADDR_SETUP(NDX) { \
    spi_write_addr((ULONG)(((ULONG)(Line[NDX].hi_addr)<<16)|((ULONG)Line[NDX].lo_addr))); \
    byte = Line[NDX].bytespercol; \
}
#define SPI_ADDR(NDX) {\
    spi_open(); \
}
#define GET_SPI()  spi_readb()
#endif

#define UpdateImagePtr(i) do { \
    if ((0 == Line[i].len) || (position < Line[i].x1)) { \
        if (! Line[i].next) break; \
        Line[i].curchar = Line[i].next; \
        Line[i].len = *Line[i].curchar; \
        if (! Line[i].len) break; \
        if (FlagBits.reverse) { \
            if (Line[i].curchar[PREVOFFSET]) { \
                Line[i].next -= Line[i].curchar[PREVOFFSET];/*previous length*/\
                Line[i].next -= HDRSZE; \
            } else Line[i].next = 0; \
        } else Line[i].next += Line[i].len + HDRSZE; \
        Line[i].opts.all = Line[i].curchar[OPTSOFFSET]; /* options*/ \
        if (FlagBits.reverse) { \
            readShort(Line[i].x1, Line[i].curchar+INDENTOFFSET); \
            readShort(Line[i].x2, Line[i].curchar+WIDTHOFFSET); \
            Line[i].x2 += Line[i].x1 - 1; \
        } else { \
            readShort(Line[i].x2, Line[i].curchar+INDENTOFFSET); \
            readShort(Line[i].x1, Line[i].curchar+WIDTHOFFSET); \
            Line[i].x1 += Line[i].x2 - 1; \
            Line[i].x2 = LASTCOL - Line[i].x2; \
            Line[i].x1 = LASTCOL - Line[i].x1; \
        } \
        Line[i].row = Line[i].curchar[ROWOFFSET]; \
        Line[i].curchar += HDRSZE + Line[i].opts.optlen; \
        Line[i].len -= Line[i].opts.optlen; \
        if (FlagBits.reverse) Line[i].curchar += Line[i].len - 1; \
        Line[i].cspc = 0; \
    } else if (Line[i].cspc) { \
        Line[i].cspc--; \
    } else { \
        Line[i].cspc = CSPC - 1; \
        Line[i].hi_addr = ((Line[i].opts.font/*+1*/)<<4)|(*Line[i].curchar)>>4; \
        Line[i].lo_addr = *Line[i].curchar; \
        Line[i].lo_addr <<= 12; \
        if (FlagBits.reverse) Line[i].curchar--; \
        else Line[i].curchar++; \
        \
        Line[i].len--; \
        SPI_ADDR(i); \
        while (!SSP1STATbits.BF); /* wait */ \
        spi_read_byte(Line[i].bytespercol); /* bytespercol*/ \
        spi_read_short(Line[i].curcol); /* columns*/ \
        spi_close(); \
        if (FlagBits.reverse) Line[i].lo_addr |= ((Line[i].curcol+1)<<5); \
    } \
} while (0)

void TurnOn(void) {
    LED_RED_ON();
    UpdateDots();
    VPHP_power_on();
    fwFirmwarelock(0);
    if (CURBANK) { 
        fwSelectOddBank();
    } else {
        fwSelectEvenBank();
    }
}

#define PURGE_SPEED 20
#define PURGE_LENGTH 100
void purge(void) {
    UCHAR i;
    INTCON3bits.INT1IE = 0; //disable external encoder interrupt
    PIE1bits.CCP1IE = 0; //disable photocell sampling interrupt
//    TurnOn();
    i = ADDR_ROWS;
    while (i--) column[i] = 0xff;
    position = PURGE_LENGTH;
    state = PURGE;
    TotalDots += 150 * PURGE_LENGTH;
    TurnOn();//UpdateDots(); //UpdateDots() is called in TurnOn()
#ifndef WIN32
    CCPR1H = HIBYTE((unsigned int)(SPEED_FACTOR / PURGE_SPEED));
    CCPR1L = LOBYTE((unsigned int)(SPEED_FACTOR / PURGE_SPEED)); 
    refresh_var = 0;
    INTCON3bits.INT1IE = 0; //disable external encoder interrupt
    PIE1bits.CCP1IE = 1; //enable photocell sampling interrupt
#endif
}

#define send_chr_col(NDX) \
        if ((0 == Line[NDX].curcol) || (position < Line[NDX].x1)) { \
/*LATBbits.LATB2 = 1;*/ \
            UpdateImagePtr(NDX); \
/*LATBbits.LATB2 = 0;*/ \
        } else if (!(position > Line[NDX].x2)) { \
            while (row<Line[NDX].row) { \
                column[row] = 0; \
                row++; \
            } \
            if (FlagBits.reverse) Line[NDX].lo_addr-=32; \
            else Line[NDX].lo_addr+=32; \
            SPI_ADDR_SETUP(NDX); \
            if ((byte + row) <= ADDR_ROWS) { /* TODO: move to cpld/refresh */ \
                while (byte>0) { \
                    byte--; \
                    column[row++] = GET_SPI(); \
                } \
            } \
            spi_close(); \
            Line[NDX].curcol--; \
        }

#define SndByte(NDX) DATA(column[NDX]); TOGGLE_DATA()
#define Snd5Bytes(X) \
    SndByte(X); SndByte(X+1); SndByte(X+2); SndByte(X+3); SndByte(X+4)
#define SendBytes() \
    Snd5Bytes(0); Snd5Bytes(5); Snd5Bytes(10); Snd5Bytes(15); \
    SndByte(20); SndByte(21); SndByte(22)

void refresh() {
#ifndef BURN_FUSES
    UCHAR i, len, j;
    UCHAR * pc, *buf, *p, c, sze;
    unsigned short width, tmp;
    struct tm tme;
    if (LASTCOL < 0) return;
    FlagBits.reverse = (state == LEFT_TO_RIGHT); //RIGHT_TO_LEFT);
    //localtime_r(time, &tme);
    for (i=0; i < NLINES; i++) {
        // reset values
        Line[i].len = Line[i].curcol = Line[i].x1 = 0;
        Line[i].curchar = Line[i].next = Linetxt(i);
        Line[i].x2 = LASTCOL+3; //+1 position pre-decremented, +1 initial field switch, +1 initial ch switch

        pc = Linetxt(i);
        while ((len = *pc)) {
            if (FlagBits.reverse) Line[i].next = pc;
            Line[i].opts.all = *(pc+OPTSOFFSET);
            switch (Line[i].opts.type) {
            case SEQUENCE: {
                j = len;
                buf = pc+HDRSZE+len-1;
                while (j--) {
                    if ('9' == *buf) {
                        *buf = '0';
                    } else {
                        if (' ' == *buf) *buf='0';
                        *buf = *buf + 1;
                        break;
                    }
                    buf--;
                }
                // obtain buffer offset and update contents
                sze = len;
                buf = pc+HDRSZE;
                readShort(tmp, pc+BUFOFFSET);
                Fram_OpenWrite(BUF_FRAM+tmp);
                while (sze--) Fram_Write(*buf++);
                Fram_Close();
                break;
            }
            case CLOCK: {
                buf = pc+HDRSZE;
                p = buf + Line[i].opts.optlen;
                j = Line[i].opts.optlen;
                if (2 < j) {
                    readShort(tmp, buf);
                    j -= 2;
                    buf += 2;
                    while (j--) *p++ = *buf++;
                    tme = time;
                    ExpOffset(&tme, tmp);
                    ConvertDate(&tme, buf, Line[i].opts.optlen);
                } 
                break;
            }
            case BARCODE:
            case TEXT:
            default:
                break;
            }
            p = pc + HDRSZE + Line[i].opts.optlen;
            //width = getWidth(p, len - Line[i].opts.optlen, Line[i].opts.font);
            sze = len - Line[i].opts.optlen;
//unsigned short getWidth(UCHAR * pc, unsigned char sze, UCHAR fnt) {
//    unsigned short width, tmp;
//    UCHAR c;
    if (sze)
        width = (sze-1)*CSPC;
    else
        width = 0;
    while (sze--) {
        c = *p++;
        spi_open();
        spi_write_byte(CONTINUOUS_READ); 
        spi_write_addr((ULONG)((((ULONG)(Line[i].opts.font/*+1*/)<<20)|((ULONG)(c)<<12))+1));
        spi_read_short(tmp);
        width += tmp;
        spi_read_short(tmp);
        spi_close();
TotalDots += tmp;
    }
//    return width;
//}
            writeShort(pc+WIDTHOFFSET, width);
            //pc[WIDTHOFFSET] = width >> 8;
            //pc[WIDTHOFFSET+1] = width;
            pc += len + HDRSZE;
        }
    }
    TurnOn();
    position = LASTCOL+3; //+1 position pre-decremented, +1 initial field switch, + initial ch switch
    photo_offset = 0;
    if (FlagBits.reverse == CURBANK)
        photo_offset += 49;
    if (direction)
        photo_offset += 77;
    SET_OUTPUT();
#endif
}

unsigned short GetVal(UCHAR *pc, UCHAR sze);

void GetFileName(unsigned char * tmp, char ndx) {
    unsigned char i, c;
    tmp[0] = '\0';
    spi_open();
    spi_write_byte(CONTINUOUS_READ); 
    spi_write_addr(ndx * FNAME_SZE);
    for (i=0; i < FNAME_SZE; i++) {
        spi_read_byte(c);
        if ((' ' > c) || ('~' < c)) break;
        tmp[i] = c;
        tmp[i+1] = '\0';
    } 
    spi_close();
}

unsigned char SelectPrintFld(UCHAR * buf) {
#ifndef BURN_FUSES
    unsigned char i, c, n, len, prev;
    unsigned char * pc, * txt, tmp[FNAME_SZE];
    unsigned short offset, fram_offset;
    Opts opts;

    fram_offset = 0;
    opts.all = 0;

    n = 0;
    if (74 < CurY) n = 1;

    txt = Linetxt(n);
    c = i = 0;
    i = sizeof(Linetxt(n)) - 1;
    while ((len = *txt)) {
        c = len;
        txt += len + HDRSZE;
        i -= len + HDRSZE;
    }
    prev = c;
    if (i >= HDRSZE) i -= HDRSZE;
    else i = 0;

    //////////////////////////////////////////////////////
    // Start parsing data
    c = *buf++; // retrieve the type
    switch(c) {
    case 'S':
        opts.type = SEQUENCE;
        break;
    case 'C':
        opts.type = CLOCK;
        break;
    case 'B':
        opts.type = BARCODE;
        opts.font = 2;
        break;
    case 'L':
        opts.type = TEXT;
        // TODO: fix following kludge
        pc = buf; 
        while (*pc) pc++;
        *pc++ = ',';
        *pc++ = ' ';
        *pc++ = '\0';
        break;
    case 'T':
    default:
        opts.type = TEXT;
        break;
    }
    ///////////////////////////////////////////////////////
    // parse the font name and determine the index
    if (BARCODE != opts.type) {
        pc = buf;
        while ((*buf) && (',' != *buf)) buf++;
        n = buf - pc;
        if ((*buf) && (',' == *buf)) buf++;
        opts.font = 0; 
        c = 0;
        while (1) {
            GetFileName(tmp, c);
            if (!*tmp) break;
            if (0 == strncmp((char *)pc, (char *)tmp, n)) {
                opts.font = c+1;
                break;
            }
            c++;
        }
        if (!opts.font) return 3; // fail if font not found
        pc = buf;
        if (CLOCK == opts.type) {
            offset = 0;
            while (*pc && ',' != *pc) pc++;
            c = pc - buf;
            if (*pc == ',') {
                if (4 >= c) {
                    offset = GetVal(buf, 4);
                } 
                buf = ++pc;
            } else {
                pc = buf;
            }
        } else if (SEQUENCE == opts.type) {
            fram_offset = buflen + n + 3 /* fX and , */;
        }
        while (*pc++) ;
        c = pc - buf - 1;
        if (CLOCK == opts.type) {
            if (0xf < (c + 2)) return 2; // no room for options
            c = (c+1) * 2; // allow for enough room
        }
    } else {
        c = 17; // space for the bar code
    }
    if (i < c) return 1; // make sure adequate space
    //opts.font = (c - '0');
    //////////////////////////////////////////////////////
    // end of data pars
    pc = txt + HDRSZE;
    if (CLOCK == opts.type) pc += 2;
    n = 0;
    while (i--) {
        *pc = '\0';
        c = *buf++;
        if (0 == c) { //('\r' == c) || ('\n' == c)) {
            if (CLOCK == opts.type) {
                writeShort(txt + HDRSZE, offset);
                opts.optlen = (len+2) & 0xf;
                len = (len+1) * 2;
            }
            break;
        }
#define OFF (0x80) //0x78)
        if (BARCODE == opts.type) {
            if (0 == n) {
                *pc++ = c+OFF; len++; // HR only
                *pc++ = OFF+'!'; len++; // start
                c += OFF + 0x20; // Left side no HR
            } else if (6 == n) {
                *pc++ = OFF+'$'; len++; // switch
            }
            if ((1 <= n) && (n <= 5)) {
                c += OFF+0x10; // Left side w/ HR
            } else if ((6 <= n) && (n < 11)) {
                c += OFF+0x30; // Right side w/ HR
            }
            if (11 == n) {
                *pc++ = c+OFF+0x40; len++; // Right side no HR
                *pc++ = OFF+'!'; len++; // end
                c += OFF; // HR only
            }
            n++;
        }
        len++;
        *pc++ = c;
    }
    *txt = len;
    //if (BARCODE == type) type = TEXT;
    txt[PREVOFFSET] = prev; // previous length
    txt[OPTSOFFSET] = opts.all;
    writeShort(txt+BUFOFFSET, fram_offset);
    writeShort(txt+INDENTOFFSET, CurX);
    txt[ROWOFFSET] = CurY * (ADDR_ROWS/2) / 75;
#endif
    return 0;
}

/*void prntFld(UCHAR type, UCHAR fnt, UCHAR * txt, UCHAR len) {
    unsigned char tmp[FNAME_SZE], *pc;
    putchar(type);
    GetFileName(tmp, fnt-1);
    pc = tmp;
    while (*pc) putchar(*pc++);
    if (!txt) return;
    putchar(',');
    pc = txt;
    while (len--) putchar(*pc++);
}*/
extern void PrintItem(ROM UCHAR *txt, unsigned long x);

void DumpFields(void) {
    UCHAR c;
    unsigned short num;

    //PrintItem((ROM UCHAR *)"buflen:", buflen);
    num = buflen;
    Fram_OpenRead(BUF_FRAM);
    while (num--) {
        c = Fram_Read();
        if (0xff == c || !c) break;
        putchar(c);
    }
    Fram_ReadLast();
    Fram_Close();
}
#if 1
void DumpData(unsigned short addr, UCHAR bufr[]);

void DumpRam(void) {
    unsigned short sze, i;
    unsigned char * pc, j;

    putchar('\n');
    for (j=0; j < NLINES; j++) {
        pc = (UCHAR *)&Linetxt(j);
        sze = sizeof(Linetxt(j)); //2*sizeof(__Line);
        i = 0;
        while (i < sze) {
            DumpData(i, pc);
            pc += 16;
            i += 16;
        }
        putchar('\n');
    }
}
#endif

#if 0
void DumpCharData(unsigned char fnt, unsigned char chr, unsigned char num) {
    unsigned char byte, bufr[16];
    unsigned char n, c;
    unsigned long addr;
    putchar('\n');
    if (8<num) num = 0;
    //addr = (((ULONG)(fnt)<<20)|((ULONG)(chr)<<12)|((ULONG)num*512));
    spi_open();

    //printLongHex(addr);
    spi_write_byte(CONTINUOUS_READ); 

    printHex(0);

    byte = fnt << 4 | chr >> 4;
    printHex(byte);
    spi_write_byte(byte);

    byte = chr << 4 | num;
    printHex(byte);
    spi_write_byte(byte);

    byte = 0;
    printHex(byte);
    spi_write_byte(byte);
    //spi_write_addr(addr);
    putchar('\n');
    for (n=0; n < 16; n++) {
        for (c=0; c < 16; c++) {
            spi_read_byte(byte);
            bufr[c] = byte;
        }
        DumpData(n, bufr);
        //printHex(byte);
        //putchar(' ');
        //n++;
        //if (0 == (n&0x000F)) putchar('\n');
    }
    spi_close();
}
#endif

void ClearBuffers(void) {
    ClearRamBuffer();
    Fram_OpenWrite(BUF_FRAM);
    buflen += 2; // take care of null terminator
    while (buflen--) {
        Fram_Write(0xff);
    }
    Fram_Close();
    buflen = 0;
}

#ifdef WIN32
volatile struct _TRISBbits TRISBbits;
volatile struct _TRISEbits TRISEbits;
volatile union _PORTBbits PORTBbits;
volatile union _PORTAbits PORTAbits;
volatile union _INTCONbits INTCONbits;
volatile struct _LATBbits LATBbits;
volatile union _INTCON3bits INTCON3bits;
volatile near union _PIE1bits PIE1bits;
volatile near struct _LATEbits LATEbits;
volatile near unsigned char       CCPR1L;
volatile near unsigned char       CCPR1H;
volatile near union _PIR1bits PIR1bits;
volatile near union _TXSTA1bits TXSTA1bits;
volatile near union _TXSTAbits TXSTAbits;
volatile near union _SSP1STATbits SSP1STATbits;

FILE * fd=NULL;
void close_output() {
    if (fd) fclose(fd);
    fd = NULL;
}
void openFile(/*int i*/) {
    unsigned char bmphdr[62] = {0x42,0x4D,0x42,0x00,0x00,0x00,0x00,0x00,
                                0x00,0x00,0x3E,0x00,0x00,0x00,0x28,0x00,
                                0x00,0x00,0x64,0x00,0x00,0x00,0x00,0x00,
                                0x00,0x00,0x01,0x00,0x01,0x00,0x00,0x00,
                                0x00,0x00,0x80,0x1F,0x00,0x00,0xC2,0x1E,
                                0x00,0x00,0xC2,0x1E,0x00,0x00,0x02,0x00,
                                0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                                0x00,0x00,0xFF,0xFF,0xFF,0x00};
    /*memset(output[i], 0, sizeof(output[i]));*/
    //if (0 == i) {
        fd = fopen("out.bmp", "wb");
        if (fd) {
            bmphdr[18] = ADDR_ROWS*8; //150;
            unsigned long filesze = sizeof(bmphdr) + bmphdr[18] * position;
            memcpy(&bmphdr[2], &filesze, sizeof(filesze)); /* file size */
            memcpy(&bmphdr[22], &position, sizeof(LASTCOL+1)); /* bmp height */
            fwrite(bmphdr, 1, sizeof(bmphdr), fd);
        }
    //}
}

/*typedef union {
    struct {
        unsigned CCP1IF:1;
        unsigned RB2:1;
    };
    unsigned char all;
} __PICbits;
__PICbits PIR1bits, PORTBbits;*/
unsigned char left = 1;
unsigned char right = 1;
//unsigned char CCPR1L, CCPR1H;

void Print2BMP(unsigned char c) {
    //UCHAR row, byte;
    void IRQHandlerHigh(void);

    state = ('l' == c ? LEFT_TO_RIGHT : RIGHT_TO_LEFT);
    //puts("\n");
    refresh();
    while (position) {
        PIR1bits.CCP1IF = 1;
        IRQHandlerHigh(); //send_column();
    }
    close_output();
}
#define update_photo()

#elif defined(PIC)
#ifdef NEWBRD
unsigned char left = 1;
unsigned char right = 1;
#else
#define left (PORTAbits.RA3) //gate
#define right (PORTBbits.RB1) //trigger
#endif
#pragma interrupt IRQHandlerHigh

//#define update_photo()
#define update_photo() \
    if (RIGHT_LAST_ON) { \
        left = 1; \
        LEFT_PHOTO_ON(); \
        timeout = TIME_OUT_INIT; \
        while (timeout) { \
            timeout--; \
            if ((1 == PHOTO_OUT) && (timeout < TIME_OUT)) { \
                left = 0; \
                break; \
            } \
        } \
    } else { \
        right = 1; \
        RIGHT_PHOTO_ON(); \
        timeout = TIME_OUT_INIT; \
        while (timeout) { \
            timeout--; \
            if ((1 == PHOTO_OUT) && (timeout < TIME_OUT)) { \
                right = 0; \
                break; \
            } \
        } \
    } \
    PHOTO_OFF();
#endif
#endif

#define DbgPhoto(a) //putchar(a)

extern volatile unsigned char rxbuf[64], iptr, optr;
extern unsigned char rxstate, address;
enum {RX_START=0, RX_RECV, RX_INVALID};

void IRQHandlerHigh(void) {
    UCHAR row, byte, timeout;
//LATBbits.LATB2 = 1;
    if ((PIR1bits.CCP1IF) || (INTCON3bits.INT1IF)) {
        INTCON3bits.INT1IF = 0;
        PIR1bits.CCP1IF = 0; 
#ifndef BURN_FUSES
        if ((RIGHT_TO_LEFT == state) || (LEFT_TO_RIGHT == state) || (PURGE == state)) {
            if (!refresh_var) {
                if (photo_offset) photo_offset--;
                else if (position) {
//LEFT_PHOTO_ON();
                    position--;
                    if (PURGE != state) {
                        row = 0;
                        send_chr_col(0);
                        send_chr_col(1);
                        if (row) {
                            while (row<ADDR_ROWS) {
                                column[row] = 0; /*TOGGLE_DATA();*/
                                row++;
                            }
                            fwRdySR();
                            SendBytes();//send previously buffered column
                        }
                    } else {
                        fwRdySR();
                        SendBytes();//send previously buffered column
                    }
                    DBGOUTPUT();
//RIGHT_PHOTO_ON();
                } else {
//LATBbits.LATB2 = 0;
                    TurnOff();
                    update_photo();
                    if ((PURGE != state) && FlagBits.continuous 
                        && (
                            (((0 == left) || (0 == right)) && (0 == FlagBits.extphoto))
                            ||((1 == FlagBits.extphoto) && EXTPHOTO)
                           )
                       ) {
                        //update encoder setting between prints
                        if (FlagBits.extencoder) {
                            INTCON3bits.INT1IE = 1; //enable external encoder interrupt
                            PIE1bits.CCP1IE = 0; //disable fixed linespeed interrupt
                        } else if (linespeed) { 
                            //load the CCP compare registers
                            CCPR1L = LOBYTE((unsigned int)linespeed); CCPR1H = HIBYTE((unsigned int)linespeed);
                            INTCON3bits.INT1IE = 0; //disable external encoder interrupt
                            PIE1bits.CCP1IE = 1; //enable fixed linespeed interrupt
                        }
                        if (!FlagBits.pause) {
                            refresh_var = 1; //refresh();
                        }
                    } else {
                        // setup photocell timer
                        CCPR1L = LOBYTE((unsigned int)PHOTO_SAMPLE_RATE); CCPR1H = HIBYTE((unsigned int)PHOTO_SAMPLE_RATE);
                        INTCON3bits.INT1IE = 0; //disable external encoder interrupt
                        PIE1bits.CCP1IE = 1; //enable photocell sampling interrupt
                        state = START_PHOTO;
                        DbgPhoto('S');
                    }
                }
            }
        } else {
//LATBbits.LATB2 = 0;
            update_photo();
            time1msb++;
            if (
                  ((NO_PHOTO != state) && (1 == FlagBits.extphoto) && (0 == EXTPHOTO))
                ||((NO_PHOTO != state) && (0 == FlagBits.extphoto) && (1 == right) && (1 == left))
               ) {
                state = NO_PHOTO;
                DbgPhoto('N');
            } else if (((NO_PHOTO == state) || (FlagBits.continuous && (LEFT_PHOTO > state))) &&
                       (  ((0 == FlagBits.extphoto) && (0 == left) && (RIGHT_TO_LEFT != direction))
                        ||((1 == FlagBits.extphoto) && EXTPHOTO && (LEFT_TO_RIGHT == direction))
                       )
                      ) {
                state = LEFT_PHOTO;
                DbgPhoto('l');
                time1msb = 0; //reset timer
            } else if (((NO_PHOTO == state) || (FlagBits.continuous && (START_PHOTO == state))) &&
                       (  ((0 == FlagBits.extphoto) && (0 == right) && (LEFT_TO_RIGHT != direction))
                        ||((1 == FlagBits.extphoto) && EXTPHOTO && (RIGHT_TO_LEFT == direction))
                       )
                      ) {
                state = RIGHT_PHOTO;
                DbgPhoto('r');
                time1msb = 0; //reset timer
            } else if (
                         ((RIGHT_PHOTO == state) && ((0 == left) || (RIGHT_TO_LEFT == direction)))
                       ||((LEFT_PHOTO == state) && ((0 == right) || (LEFT_TO_RIGHT == direction)))
                      ) {
                if (linespeed) time1msb = linespeed;
                else time1msb *=  13;
                if (FlagBits.pause || ((MAXSPEED > time1msb) && (0 == FlagBits.extencoder))) {
                    state = START_PHOTO;
                    DbgPhoto('s');
                } else {
                    if (RIGHT_PHOTO == state) {
                        state = RIGHT_TO_LEFT;
                        DbgPhoto('R');
                    } else {
                        state = LEFT_TO_RIGHT;
                        DbgPhoto('L');
                    }
                    if (FlagBits.extencoder) {
                        INTCON3bits.INT1IE = 1; //enable external encoder interrupt
                        PIE1bits.CCP1IE = 0; //disable fixed linespeed interrupt
                    } else { 
                        //load the CCP compare registers
                        CCPR1L = LOBYTE((unsigned int)time1msb); CCPR1H = HIBYTE((unsigned int)time1msb);
                        INTCON3bits.INT1IE = 0; //disable external encoder interrupt
                        PIE1bits.CCP1IE = 1; //enable fixed linespeed interrupt
                    }
                    refresh_var = 1; //refresh();
                }
            }
            // this is doing 2048 instead of PHOTO_SAMPLE_RATE
            //if (0 == (time1msb & 0x7ff)) time++; 
        }
#endif
    }
//LATBbits.LATB2 = 0;
}
//#endif

extern void usleep(unsigned short us);
/*unsigned short ReadR10X(void) {
     ////////////////////////////////////////////
    ADCON1 = 0x0d; // select AN0, AN1
    // RTSR_ADC=AN0, R10X_ADC=AN1
    ADCON0 = 0x04;          // AN0=0x00, AN1=0x04, AN2=0x08
    ADCON2 = 0x87; //0b10111001; //0x12;          // 4 Tad, Fosc/32
    //ADCON2bits.ADFM = 1;    // right justified
//    ADCON1 = 0x0f; // all digital 
    ////////////////////////////////////////////

    unsigned short val;
//    ADCON0bits.ADON = 1;    // Enable the A/D
    usleep(250);
//    ADCON0bits.GO = 1;      // turn on the a2d
//    while (ADCON0bits.GO_DONE) ;
    val = ! PORTAbits.RA1; //ADRES;
    //val <<= 8;
    //val += ADRESL;
    return val;
}*/

unsigned long ReadLong(UCHAR offset) {
    unsigned long results;
    Fram_OpenRead(offset);
    results = Fram_Read();
    results <<= 8;
    results |= Fram_Read();
    results <<= 8;
    results |= Fram_Read(); 
    results <<= 8;
    results |= Fram_Read(); 
    Fram_ReadLast();
    Fram_Close();
    return results;
}

#ifdef BURN_FUSES
void msleep(unsigned short x) {
    while (x--) usleep(470);
}

unsigned long readCartridge(void) {
    unsigned long dat;
    unsigned char tmp;
    fwFirmwarelock(1);
    fwFirmwarelock(0);
    R10Xctl(1); //RTSRctl(1);
    DATA(0);
    tmp = 0;
    dat = 0;
    while (tmp<ADDR_ROWS) {
        data_io(1);
        dat <<= 1;
        dat += (! PORTAbits.RA1);
        data_io(0);
        tmp++;
    }
    R10Xctl(1); //RTSRctl(1);
    fwFirmwarelock(1);
    fwFirmwarelock(0);
    return dat;
}

void WritePrintHd(UCHAR * buf) {
    unsigned char tmp, c, x, wafer, mfg;
    unsigned short id;
    unsigned long dat;
    // convert from char to short
    id = 0;
    mfg = *buf++ & 0x7;
    if (',' != *buf++) return;
    id = GetVal(buf, 3);
    id <<= 3; // allow space for mfg
    /*while ((c=*buf++)) {
        if ('A' <= c && c <= 'F') c += 0x20;
        if ('0' <= c && c <= '9') x = c - '0';
        else if ('a' <= c && c <= 'f') x = 10 + c - 'a';
        id <<= 4;
        id += x;
    }*/
    //////////////////////////////////////////////////////////
    dat = readCartridge();
    wafer = dat >> 13;
    //////////////////////////////////////////////////////////
    // now compute the correct fuse value using serial and mfg
    mfg = ~(mfg ^ wafer) & 0x7;
    id += mfg;
    //////////////////////////////////////////////////////////
    putchar('\n');
    printHex(wafer); putchar(',');
    printShortHex(id);
    putchar('\n');

    fwFirmwarelock(1);
    fwFirmwarelock(0);
    R10Xctl(1); //RTSRctl(1);
    DATA(0);
    tmp = 0;
    // move past preset fuses
    while (tmp<ADDR_ROWS) {
        data_io(1);
//LATBbits.LATB2 = 1;
        if (tmp > 10) {
//        if ((tmp > 0) && (tmp < 9)) {
            if ((id>>(ADDR_ROWS-tmp-1)) & 1) {
//            if ((id>>(9-tmp-1)) & 1) {
                putchar('1');
                PWRCTL(0);
                msleep(1); //1% duty cycle at 15V, anything more than 10% may damage R10X
                PWRCTL(1);
                msleep(100); //1% duty cycle at 15V, anything more than 10% may damage R10X
            } else putchar('0');
        }
//LATBbits.LATB2 = 0;
        data_io(0);
        tmp++;
    }
    R10Xctl(0); //RTSRctl(0);
    fwFirmwarelock(1);
    fwFirmwarelock(0);
}
#else
void WritePrintHd(UCHAR * buf) { }
#endif

#define CARTRDG_FACTOR (CARTRDG_DOTS/100)
#define MISSING_CARTRIDGE (CARTRDG_DOTS+0x1000)
unsigned char CheckPrintHd(UCHAR flag) {
    unsigned char tmp, wafer, sad, x;
    unsigned short id;
    unsigned long data, data2;
//LATBbits.LATB2 = !LATBbits.LATB2;
    fwFirmwarelock(1);
    fwFirmwarelock(0);
   
    R10Xctl(1); RTSRctl(1); 	//Turn on RTSR		//PJ
    ADCON0bits.GO = 1;			//Start ADC			//PJ
    DATA(0);
    tmp = 0;
    id = 0;
    while (tmp<ADDR_ROWS) {
        data_io(1);
//LATBbits.LATB2 = 1;
        id <<= 1;
        id += (! PORTAbits.RA1);
//LATBbits.LATB2 = 0;
        data_io(0);
        if (8 == tmp) wafer = id;
        if (10 == tmp) sad = id;
        tmp++;
    }
    

  	//while (ADCON0bits.GO);	// Would be good ideal to add to make sure the ADC is complete 
  	//Read the AN0 value(ADRESH and ADRESL) here	//PJ
    R10Xctl(0); RTSRctl(0);		//Turn off RTSR		//PJ
    fwFirmwarelock(1);
    fwFirmwarelock(0);

    id &= 0x0fff;
    //if (0x0fff == id) id = 0;
    sad &= 0x03;
    tmp = 0;
    x = id;
    x = (~x ^ wafer) & 0x7;
    if ((0x0fff == id) && (0xff == wafer)) {
        // missing cartridge
        TotalDots = MISSING_CARTRIDGE;
    } else {
        if ((x == mfg_id) || !mfg_id) {
            TotalDots = 0;
            for (; tmp < 16; tmp += 8) {
                data = ReadLong(tmp);
                if (((data >> 16) == wafer) && (id == (0xffff & data))) {
                    TotalDots = ReadLong(tmp+4);
                    break;
                }
                //printLongHex(data); putchar(' ');
            }
            //putchar('\n');
            if (8 <= tmp) {
                // not found, move entries down and place as first
                data = ReadLong(0);
                data2 = ReadLong(4);
                WriteLong(8, data);
                WriteLong(12, data2);
            }
            data = wafer;
            data <<= 16;
            data += id;
            WriteLong(0, data);
            tmp = 4;
            WriteLong(tmp, TotalDots);
        } else if (MISSING_CARTRIDGE == TotalDots) {
            TotalDots = 0;
        }
    } 
    ink = tmp;

    if (flag) {
        printHex(wafer); putchar(' ');
        printHex(sad); putchar(' ');
        printShortHex(id); putchar('\n');
        printHex(ink); putchar(' ');
        printHex(mfg_id); putchar(' ');
        printHex(x); putchar(':');
        printLongHex(TotalDots); putchar('\n'); putchar('\n');				// Added new line PJ        
        printHex(ADRESH); putchar(':'); printHex(ADRESL);putchar('\n');		// Added to see AD0 value PJ
        
    }

    tmp = TotalDots / CARTRDG_FACTOR;
    if (!ink && (98 > tmp)) TotalDots = CARTRDG_DOTS - 2 * CARTRDG_FACTOR;

    if (tmp < 90) x = 0; //'g';
    else if (tmp < 98) x = 1; //'l';
    else if (tmp < 100) x = 3; //'o';
    else x = 2; //'?'
    if (0x0f == mfg_id) x = 0; // override for debugging

    if (flag) {
        printHex(tmp); putchar('%');
        printHex(x);
        putchar('\n');
    }

    return x;
}

void append(UCHAR * buf) {
    UCHAR c;
    //PrintItem((ROM UCHAR *)"buflen:", buflen);
    Fram_OpenWrite(buflen+BUF_FRAM);
    while ((c=*buf++)) {
        Fram_Write(c);
        buflen++;
    }
    Fram_Write('\n');
    buflen++;
    Fram_Write(0);
    Fram_Close();
}

UCHAR ParseSubCmd(UCHAR c, UCHAR * buf) {
    switch (c) {
        case 'h':
            CurX = GetVal(buf, 5);
            break;
        case 'v':
            CurY = GetVal(buf, 4);
            break;
        case 'a':
            LASTCOL = GetVal(buf, 5);
            if (32767 < LASTCOL) LASTCOL = 32767;
            else LASTCOL--;
            break;
        case 'f': 
            return SelectPrintFld(buf);
        case 'c':
            FlagBits.continuous = *buf - '0';
            break;
    }
    return 0;
}

UCHAR * ParsePrintParams(UCHAR * buf) {
    unsigned short tmp;
    switch (*buf++) {
        case 'i': {
            TotalDots = 0;
            while (*buf) {
                TotalDots = TotalDots*10 + (*buf++ - '0');
            }
            UpdateDots();
            break;
        }
        case 'm':
            if (('f' == *buf++) && ('g' == *buf++) && ('_' == *buf++)
                    && ('i' == *buf++) && ('d' == *buf++)) {
                mfg_id = *buf & 0xf;
                Fram_OpenWrite(MFGID_FRAM);
                Fram_Write(mfg_id);
                Fram_Close();
            }
            break;
        case 'u':
            purge();
            break;
        case 's':
            tmp = GetVal(buf, 3);
            buf += 3;
            if (tmp) {
                if (tmp<3) tmp = 3; // see comment below
                tmp = SPEED_FACTOR / tmp;// tmp<3 will result in more than 16 bits
                if (MAXSPEED > tmp) tmp = MAXSPEED;
            } else tmp = 0;
            INTCONbits.GIEH = 0;
            linespeed = tmp;
            INTCONbits.GIEH = 1;
            break;
        case 'd':
            if ('r' == *buf) direction = RIGHT_TO_LEFT;
            else if ('l' == *buf) direction = LEFT_TO_RIGHT;
            else direction = 0;
            buf++;
            break;
        case 'f':
            FlagBits.extphoto = *buf - '0';
            buf++;
            break;
        case 'e':
            FlagBits.extencoder = *buf - '0';
            buf++;
            break;
        case 'p':
            FlagBits.pause = *buf - '0';
            buf++;
            break;
    }
    Fram_OpenWrite(PARMS_FRAM);
    Fram_Write((linespeed >> 8)&0xff);
    Fram_Write(linespeed&0xff);
    Fram_Write(direction);
    Fram_Write((FlagBits.extencoder << 2 | FlagBits.extphoto << 1 | FlagBits.pause) & 0x7);
    Fram_Close();
    return buf;
}
