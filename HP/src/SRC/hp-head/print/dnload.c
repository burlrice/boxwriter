#include <stdlib.h>
#include "processor.h"
#include "types.h"
#include "uart.h"
//#include "print.h"
#include "at45db.h"
//#include "time.h"

#ifndef WIN32
#define BUFFERSIZE (128)
#define USE_7BIT
unsigned char ch, cl;
void crc16(void){
    unsigned char byte;
    unsigned char j;
    unsigned char i;
    unsigned int t;
    unsigned int crc;

    crc = 0;
    for(j = BUFFERSIZE; j > 0; j--) {
        //CRC1021 checksum
        spi_read_byte(byte);
        crc = (crc ^ (((unsigned int) byte) << 8));
        for(i = 8; i > 0; i--) {
            t = crc << 1;
            if(crc & 0x8000)
                t = t ^ 0x1021;
            crc = t;
        }
    }
    ch = crc / 256;
    cl = crc % 256;
}
#endif


    //Xmoden control command
    #define XMODEM_SOH         0x01
    #define XMODEM_EOT         0x04
    #define XMODEM_ACK         0x06
    #define XMODEM_NAK         0x15
    #define XMODEM_CAN         0x18
    #define XMODEM_RWC         'C'
    #define TimeOutCntC        5
RUCHAR inversion[] = {
    0x00, 0x08, 0x04, 0x0c,
    0x02, 0x0a, 0x06, 0x0e,
    0x01, 0x09, 0x05, 0x0d,
    0x03, 0x0b, 0x07, 0x0f
};

static unsigned long read_long(void) {
    unsigned long dum;
    spi_read_long(dum);
    return dum;
}

static unsigned short read_short(void) {
    unsigned short dum;
    spi_read_short(dum);
    return dum;
}

void DnLoadFile(unsigned char fnt, unsigned char * name) {
#ifndef WIN32
#if 1 //def MMC
    unsigned long FlashAddr = 0;
    unsigned short cnt;
    unsigned char packNO = 0;
    unsigned char crch, crcl;
    unsigned char li, bmp, fillbytenum;
    unsigned char * lineptr;
    unsigned short first, last;
    unsigned short width;
    unsigned char bytespercol;
    unsigned char x, ndx;
#ifdef USE_7BIT
    unsigned char bytes7bit, col[32];
#define ITM(a) col[a]
#else
#define ITM(a) lineptr[j+(a)]
#endif
    unsigned short chrcnt;
    unsigned short colcnt;
    unsigned short j, height;
    unsigned long offset1;
    unsigned long offset2;
    unsigned short sectorcnt, dots;
    INTCONbits.GIEH = 0; //disable interrupts
    //every interval send a "C",waiting XMODEM control command <soh>
    cnt = TimeOutCntC;
    bmp = 0;

    FlashAddr = 512; // Allow space for the file table

    spi_open();
    spi_write_byte(WRITE_BUFFER1); spi_write_byte(0); spi_write_byte(0); spi_write_byte(0);
    while(cnt) {
        unsigned short cntr;
        cntr = 0xffff;
        putchar(XMODEM_RWC) ;    //send "C"
        while (cntr-- && !kbhit()) {}
        if(kbhit()) {
            if(getchar() == XMODEM_SOH) break; //XMODEM command <soh>
        }
        cnt--;
    }
    if (0 == cnt) return;//break;
    //begin to receive data
    cnt = 0;
    do {
        packNO++;
        while(!kbhit());
        ch = getchar(); //get package number
        while(!kbhit());
        cl = ~getchar();
        if ((packNO == ch) && (packNO == cl)) {
            for(li = 0; li < BUFFERSIZE; li++) {     //receive a full data frame
                while(!kbhit());
                spi_write_byte(getchar());
            }
            while(!kbhit());
            crch = getchar(); //get checksum
            while(!kbhit());
            crcl = getchar();
//#define VERIFY
#ifdef VERIFY
            crc16(); //calculate checksum
            if((crch == ch) && (crcl == cl)) {
#endif
                FlashAddr += BUFFERSIZE; //modify Flash page address
                if (0 == (FlashAddr&0x000001FF)) {
                    FlashAddr -= 512; //modify Flash page address
                    spi_close(); //end write to buffer1
                    spi_open();
                    spi_write_byte(PROGRAM_BUFFER1); spi_write_addr(FlashAddr);
                    spi_close();
                    wait_while_busy();
                    spi_open();
                    spi_write_byte(WRITE_BUFFER1); spi_write_byte(0); spi_write_byte(0); spi_write_byte(0);
                    FlashAddr += 512; //modify Flash page address
                }
                putchar(XMODEM_ACK);
                cnt = 0;
#ifdef VERIFY
            } else { //CRC
                putchar(XMODEM_NAK); //ask resend
                cnt++;
            }
#endif
        } else { //PackNo
            putchar(XMODEM_NAK); //ask resend
            cnt++;
        }
        if(cnt > 100) {
            break; //too many errors, abort
        }
        while(!kbhit());
    } while(getchar() != XMODEM_EOT);
    putchar(XMODEM_ACK);
    do {
        spi_write_byte(0x1a);
        FlashAddr++;
    } while (0 != (FlashAddr&0x000001FF));
    FlashAddr -= 512; //modify Flash page address
    spi_close(); //end write to buffer1

    spi_open();
    spi_write_byte(PROGRAM_BUFFER1); spi_write_addr(FlashAddr);
    spi_close();
    wait_while_busy();
    lineptr = (unsigned char *)0x100;
    offset1 = 0;
    first = 0;
    last = 0xff;
    for (chrcnt = first; chrcnt<=last; chrcnt++) {
      sectorcnt = 0;
      colcnt = 0xFFFF;
      while (colcnt) {
        spi_open();
        FlashAddr = 512; // this is file table space
        spi_write_byte(CONTINUOUS_READ); spi_write_byte(0); spi_write_byte(FlashAddr>>8); spi_write_byte(FlashAddr);
        j = read_short(); //spi_read_short(j);
        if (0x424d == j) {
            bmp = 1;
            for (j=0; j < 16 /*18 started@2*/; j++) spi_read_byte(li); // skip some
            j = read_short(); //spi_read_short(j); // 18,19 column height (bmp width)
            height = swapw(j);
            bytespercol = (((height - 1) / 32) + 1)*sizeof(long); // data is dword aligned
            fillbytenum = bytespercol - ((height - 1) / 8) - 1;
#ifdef USE_7BIT
            bytes7bit = ((height-1) / 7) + 1;
#endif
            j = read_short(); //spi_read_short(j);      // 20,21
            j = read_short(); //spi_read_short(j);  // 22,23
            if (0xFFFF == colcnt) {
                colcnt = swapw(j);
                width = colcnt;
                first = chrcnt = 0x20;
                last = first;
                dots = 0;
            }
            FlashAddr += 62 + (width - colcnt) * bytespercol;
            j = read_short(); //spi_read_short(j);      // 24,25
            j = read_short(); //spi_read_short(j);      // 26,27 bpp
            if (0x0100 != j) {
                //printShortHex(j);
                //puts((RUCHAR *)("ERROR: BMP cannot be color!\r\n"));
                return;
            }
            bytespercol -= fillbytenum;
        } else {
            height = read_short(); //spi_read_short(height); //height
            bytespercol = (((height - 1) / 16) + 1)*2; // data is word aligned
#ifdef USE_7BIT
            bytes7bit = ((height-1) / 7) + 1;
#endif
            fillbytenum = 0;
            first = read_short(); //spi_read_short(first); //first character in font file
            if (0 == chrcnt) chrcnt = first;
            last = read_short(); //spi_read_short(last); //last character in font file
            for(cnt = first; cnt<chrcnt; cnt++) {
                offset2 = read_long();
                // spi_read_long(offset2); //skip table entry
            }
            offset2 = read_long(); //spi_read_long(offset2);
            if (0xFFFF == colcnt) {
                colcnt = offset2 - offset1;
                width = colcnt;
                dots = 0;
            }
            #define FNTHDROFFSET (8)
            FlashAddr += FNTHDROFFSET + ((last - first + 1) * sizeof(long));
            FlashAddr += offset1*bytespercol;//skip cols of previous chrs
            FlashAddr += (offset2 - offset1)*bytespercol;//skip previous cols of current chr
            FlashAddr -= colcnt*bytespercol;//skip previous cols of current chr
        }
        spi_close();
        spi_open();
        spi_write_byte(CONTINUOUS_READ); spi_write_addr(FlashAddr);
        for(cnt = 0; cnt<512; cnt++) {
            lineptr[cnt] = 0; //clear buffer
        }
        if (0 == sectorcnt) {
#ifdef USE_7BIT
            lineptr[0] = bytes7bit;
#else
            lineptr[0] = ((height - 1) / 8) + 1; // non word aligned
#endif
            lineptr[1] = width>>8;
            lineptr[2] = width;
            j = 32;
        } else {
            j = 0;
        }
        for(; j<512; j+=32) {
            if (!bmp) {
                for(cnt = bytespercol; cnt>0; cnt--) {
                    spi_read_byte(ITM(cnt-1)); //get column data
                }
            } else {
                for(cnt = 1; cnt<=bytespercol; cnt++) {
                    spi_read_byte(ITM(cnt-1)); //get column data
                }
                // read extra bmp bytes
                for (cnt=fillbytenum; cnt>0; cnt--) spi_read_byte(li);

                for(cnt = 1; cnt<=bytespercol; cnt++) {
                    li = ITM(cnt-1);
                    li = inversion[li>>4] | (inversion[0x0f&li] << 4);
                    ITM(cnt-1) = ~li;
                }
                x = bytespercol*8 - height;
                ITM(bytespercol-1) &= 0xff >> x;
            }
            //////////////////////////////////////////////////////////////////
#ifdef USE_7BIT
            // shift 8-bit into 7-bit
            ndx = 0; //bytespercol+1;
            x = 0;
            for (cnt=1; cnt <= bytes7bit; cnt++) {
                //lineptr[j+cnt-1] = col[ndx++];
                lineptr[j+cnt-1] = col[ndx] >> x;
                if (x) ndx++;
                if (bytespercol < ndx) break;
                lineptr[j+cnt-1] |= col[ndx] << 8-x;
                lineptr[j+cnt-1] &= 0x7f;
                if (x) x--;
                else x = 7;
            }
#endif
            //////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////
            // add up the dots for this column
#ifdef USE_7BIT
            for (cnt=1; cnt <= bytes7bit; cnt++)
#else
            for (cnt=1; cnt <= bytespercol; cnt++) 
#endif
            {
                li = lineptr[j+cnt-1];
                ndx = 7;
                while (ndx--) {
                    if (li & 1) dots++;
                    li >>= 1;
                }
            }
            //////////////////////////////////////////////////////////////////
            if (0 == (--colcnt)) {
                offset1 = offset2;
                break;
            }
        }
        spi_close();
        spi_open();
        spi_write_byte(WRITE_BUFFER1); spi_write_byte(0); spi_write_byte(0); spi_write_byte(0);
        for(cnt = 0; cnt<512; cnt++) {
            spi_write_byte(lineptr[cnt]);
        }
        spi_close();
        FlashAddr = fnt; //+1;
        FlashAddr <<= 8;
        FlashAddr |= chrcnt;
        FlashAddr <<= 3;
        FlashAddr |= sectorcnt;
        FlashAddr <<= 9;
        spi_open();
        spi_write_byte(PROGRAM_BUFFER1); spi_write_addr(FlashAddr);
        spi_close();
        wait_while_busy();
        if (last>255) last = 255;
        //if (chrcnt==last)
        //    DumpCharData(fnt/*+1*/, chrcnt, 0);
        sectorcnt++;
      }
      //////////////////////////////////////////////////////////////////
      // readback and modify write the dots for dot counting
      FlashAddr = fnt;
      FlashAddr <<= 8;
      FlashAddr |= chrcnt;
      FlashAddr <<= 12;
      spi_open();
      spi_write_byte(CONTINUOUS_READ); spi_write_addr(FlashAddr);
      for(cnt = 0; cnt<512; cnt++) {
          spi_read_byte(lineptr[cnt]);
      }
      spi_close();
      //dots = colcnt * height / 3;
      lineptr[3] = dots>>8;
      lineptr[4] = dots;
      spi_open();
      spi_write_byte(WRITE_BUFFER1);
      spi_write_byte(0);
      spi_write_byte(0);
      spi_write_byte(0);
      for(cnt = 0; cnt<512; cnt++) {
          spi_write_byte(lineptr[cnt]);
      }
      spi_close();
      spi_open();
      spi_write_byte(PROGRAM_BUFFER1);
      spi_write_addr(FlashAddr);
      spi_close();
      wait_while_busy();
      //printShortHex(dots);
      //////////////////////////////////////////////////////////////////
      // done modifying
      putchar('.');
    }

    //////////////////////////////////////////////////////////////////
    // write out the name in the next slot
    // first read previous entries
    spi_open();
    spi_write_byte(CONTINUOUS_READ); spi_write_byte(0); spi_write_byte(0); spi_write_byte(0);
    cnt = (fnt - 1) * FNAME_SZE;
    for (j=0; j < cnt; j++) spi_read_byte(lineptr[j]);
    spi_close();
    wait_while_busy();
    while (*name) lineptr[cnt++] = *name++;
    for(; cnt<512; cnt++) lineptr[cnt] = 0; //clear buffer
    //////////////////////////////////////////////////////////////////
    spi_open();
    spi_write_byte(WRITE_BUFFER1); spi_write_byte(0); spi_write_byte(0); spi_write_byte(0);
    for (j=0; j < cnt; j++) spi_write_byte(lineptr[j]);
    spi_close();
    spi_open();
    spi_write_byte(PROGRAM_BUFFER1); spi_write_addr(0);
    spi_close();
    wait_while_busy();
    // end of name writing
    //////////////////////////////////////////////////////////////////

#endif
#endif // WIN32
}

