enum {Red, Green};
extern void purge(void);
extern void refresh(void);
void send_column(void);
void print_init(void);
void DumpRam(void);
void DumpCharData(unsigned char, unsigned char, unsigned char);
void TurnOff(void);
void DumpFields(void);
void append(UCHAR *);
UCHAR ParseSubCmd(UCHAR, UCHAR *);
UCHAR * ParsePrintParams(UCHAR *);

#define START_PHOTO    0
#define NO_PHOTO       1
#define LEFT_PHOTO     2
#define RIGHT_PHOTO    3
#define LEFT_TO_RIGHT  4
#define RIGHT_TO_LEFT  5
#define PURGE          6
extern unsigned char state, direction;
#define PHOTO_SAMPLE_RATE 2000
#define HIBYTE(value) ((unsigned char)(((unsigned int)(value)>>8)&0xFF))
#define LOBYTE(value) ((unsigned char)(value))

#define CSPC 3
extern short position;
extern unsigned long TotalDots;

typedef union {
    struct {
        unsigned reverse :1;
        unsigned continuous :1;
        unsigned refresh :1;
        unsigned pause :1;
        unsigned update :1;
        unsigned extphoto :1;
        unsigned extencoder :1;
    };
    unsigned char all;
} __FlagBits;
extern __FlagBits FlagBits;

typedef union {
    struct {
        unsigned :2;
        unsigned writelock:1;
        unsigned bankselect:1;
        unsigned pwidth_0:1;
        unsigned pwidth_1:1;
        unsigned RTSR_Drive:1;
        unsigned R10X_Drive:1;
    };
    unsigned char reg;
} __cmdRegA;
extern __cmdRegA cmdRegA;

typedef union {
    struct {
        unsigned :8;
    };
    unsigned char reg;
} __cmdRegB;
extern __cmdRegB cmdRegB;

#define fwSelectOddBank() { cmdRegA.bankselect = 0; SndCmdA(); }
#define fwSelectEvenBank() { cmdRegA.bankselect = 1; SndCmdA(); }
#define CURBANK (cmdRegA.bankselect)
#define fwResetSRClear() { CPLDcmd(0x04|0x01); CPLDcmd(0|0x01); }

extern unsigned short CurX, CurY;
extern void ClearBuffers(void);
extern unsigned short ReadR10x(void);
extern unsigned char CheckPrintHd(UCHAR);
extern unsigned char ink;

#define CPLDcmd(cmd) { \
    DATA(cmd); \
    TOGGLE_WRITE(); \
    DATA(0); \
}

#define SndCmdA() { CPLDcmd(cmdRegA.reg);}
#define SndCmdB() { CPLDcmd(cmdRegB.reg); }

#define LedOps(a)
#define LED_GRN_ON() { LedOps(2/*0x08*/); }
#define LED_GRN_OFF() { LedOps(0); }
#define LED_RED_ON() { LedOps(1/*0x04*/); }
#define LED_RED_OFF() { LedOps(0); }

#ifdef PIC
#define databus     LATD
#define logic_readbusy (PORTCbits.RC1)
#define data_io(x) LATEbits.LATE1 = x
#define TOGGLE_DATA() { data_io(1); data_io(0); }
#define TOGGLE_WRITE() { LATCbits.LATC0 = 1; LATCbits.LATC0 = 0; }
#define DATA(a) { databus = a; }
//#define PWRCTL(a) { LATCbits.LATC4 = a; } // V3 unmodified hardware
#ifdef NEWBRD

//#define PWRCTL(a) { TRISBbits.TRISB7 = a; } 
//#define PWRCTL(a) { LATBbits.LATB7 = a; }
#define pwrbit LATEbits.LATE2
#define PWRCTL(a) { pwrbit = a; } 
#define isPWRoff() (1 == pwrbit)
#else
#define PWRCTL(a) { LATBbits.LATB4 = a; } 
#endif
#define RTSRctl(a) { cmdRegA.RTSR_Drive = a; SndCmdA(); }
#define R10Xctl(a) { cmdRegA.R10X_Drive = a; SndCmdA(); }
#define DELAY(a) Delay10TCYx(a)
#define SET_OUTPUT()
#define DBGOUTPUT() 
#define EXTPHOTO (PORTBbits.RB0)

#elif defined(WIN32)
#define data_io(x) LATEbits.LATE1 = x
#define R10Xctl(a) { cmdRegA.R10X_Drive = a; SndCmdA(); }
#define EXTPHOTO (PORTBbits.RB0)
#define logic_readbusy 0
#define TOGGLE_DATA() 
#define TOGGLE_WRITE() 
#define DATA(a)
#define PWRCTL(a) 
#define DELAY(a) 
#define rom
#define gate       1 
#define trigger    1 
#define SET_OUTPUT() openFile()
void openFile(/*int i*/);
#define DBGOUTPUT() { \
    if (fd) {\
        fwrite(column, 1, ADDR_ROWS, fd);\
        fputc(0, fd); fputc(0, fd);\
    }\
}
#else
#define databus PORTD
#define logic_readbusy  (inp(PORTC) & 0x2)
#define TOGGLE_DATA() { sbi(PORTE, 1); cbi(PORTE, 1); }
#define TOGGLE_WRITE() { sbi(PORTC, 0); cbi(PORTC, 0); }
#define DATA(a) { outp(a, databus); }
#define PWRCTL(a) { if (a) sbi(PORTC, 4); else cbi(PORTC, 4); }
#define SET_OUTPUT()
#define DELAY(a) 
#define DBGOUTPUT() 

#endif

#define TurnOff() {\
    VPHP_power_off();\
    LED_RED_OFF();\
}

//#define fwFirmwarelockOFF() { cmdRegA.writelock = 0; SndCmdA(); }
//#define fwFirmwarelockON() { cmdRegA.writelock = 1; SndCmdA(); }
#define fwFirmwarelock(a) { cmdRegA.writelock = a; SndCmdA(); }
#define VPHP_power_on() { fwFirmwarelock(1); PWRCTL(0); /*uDelay(VPHP_pwron_delay);*/ }
#define VPHP_power_off() { fwFirmwarelock(1); PWRCTL(1); /*uDelay(VPHP_pwroff_delay);*/ }
extern unsigned short time1msb, linespeed;
#define refresh_var (FlagBits.refresh)
#define update_var (FlagBits.update)
void GetFileName(unsigned char * tmp, char ndx);
#define MAXSPEED (833)      //200 fpm

#define DPI (300)
#define SPEED_FACTOR (10e6 / (DPI * 12 /*in/ft*/ / 60 /* sec/min */))
