/*module shift7 (clk, load, si, d, so);
    input        clk, si, load;
    input  [6:0] d;
    output       so;
    reg    [6:0] tmp;
    always @(posedge clk or posedge load)
    begin
	if (load) 
            tmp <= d;
        else
	    tmp <= {tmp[5:0], si};
    end
    assign so = tmp[7];
endmodule*/

module prim_gdff (q, d, clk, ena, clr, pre, ald, adt, sclr, sload );
    input d,clk,ena,clr,pre,ald,adt,sclr,sload;
    output q;
    reg q;
    reg clk_pre;
    initial q = 1'b0;

    always@ (clk or clr or pre or ald or adt)
    begin
        if (clr ==  1'b1)
            q <= 1'b0;
        else if (pre == 1'b1)
            q <= 1'b1;
        else if (ald == 1'b1)
            q <= adt;
        else if ((clk == 1'b1) && (clk_pre == 1'b0))
        begin
            if (ena == 1'b1)
            begin
                if (sclr == 1'b1)
                    q <= 1'b0;
                else if (sload == 1'b1)
                    q <= adt;
                else
                    q <= d;
            end
        end
        clk_pre <= clk;
    end
endmodule

module dffea (d, clk, ena, clrn, prn, aload, adata,q );
    input d,clk,ena,clrn,prn,aload,adata;
    output q;
    wire q;
    tri0 aload;
    tri1 prn, clrn, ena;

    reg stalled_adata;
    initial
    begin
        stalled_adata = adata;
    end

    always @(adata) begin
        #1 stalled_adata = adata;
    end

    prim_gdff inst (q, d, clk, ena, !clrn, !prn, aload, stalled_adata, 1'b0, 1'b0);

endmodule


module lcell(in, out);
    input in;
    output out;
    assign out = in;
endmodule

// TestBench code
/*module util_tb;
    wire out1;
    reg clock;
    initial begin
        $dumpfile("vector.vcd");
        $dumpvars; //(2, util_tb);
        $monitor ("clk=%b,out=%b", clock, out1);
        clock = 0;
        #1 clock = 0;
        //#5 clock = 0;
        #1 clock = 1;
        #10 $finish;
    end
    always begin
        #1 clock = !clock;
    end
    //not #1 lcell U1(.in(clock), .out(out1));
    lcell U1(.in(clock), .out(out1));
endmodule*/

`timescale 1 ps/ 1 ps

