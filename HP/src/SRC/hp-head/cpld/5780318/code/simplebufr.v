module simplebufr(outctrl, wdb, cdb, testpin);
    input	[3:0] outctrl;
    input	[6:0] wdb;
    output	[14:1] cdb;
	output testpin;

    reg	[14:1] cdb_out;
    wire	[6:0] iwdb;

    always@(posedge outctrl[2] or negedge outctrl[3])
    begin
        if (!outctrl[3])
	begin
	    cdb_out[2] <= 1;
	    cdb_out[4] <= 1;
	    cdb_out[6] <= 1;
	    cdb_out[8] <= 1;
	    cdb_out[10] <= 1;
	    cdb_out[12] <= 1;
	    cdb_out[14] <= 1;
	end
    else
	begin
	    cdb_out[2] <= iwdb[0];
	    cdb_out[4] <= iwdb[1];
	    cdb_out[6] <= iwdb[2];
	    cdb_out[8] <= iwdb[3];
	    cdb_out[10] <= iwdb[4];
	    cdb_out[12] <= iwdb[5];
	    cdb_out[14] <= iwdb[6];
	end
    end

    always@(posedge outctrl[0] or negedge outctrl[1])
    begin
    if (!outctrl[1])
	begin
	    cdb_out[9] <= 1;
	    cdb_out[7] <= 1;
	    cdb_out[5] <= 1;
	    cdb_out[1] <= 1;
	    cdb_out[3] <= 1;
	    cdb_out[11] <= 1;
	    cdb_out[13] <= 1;
	end
    else
	begin
	    cdb_out[1] <= iwdb[0];
    	    cdb_out[3] <= iwdb[1];
	    cdb_out[5] <= iwdb[2];
	    cdb_out[7] <= iwdb[3];
	    cdb_out[9] <= iwdb[4];
	    cdb_out[11] <= iwdb[5];
	    cdb_out[13] <= iwdb[6];
	end
    end
assign	iwdb[0] =  ~wdb[0];
assign	iwdb[1] =  ~wdb[1];
assign	iwdb[2] =  ~wdb[2];
assign	iwdb[3] =  ~wdb[3];
assign	iwdb[4] =  ~wdb[4];
assign	iwdb[5] =  ~wdb[5];
assign	iwdb[6] =  ~wdb[6];
assign	cdb = cdb_out;
endmodule
