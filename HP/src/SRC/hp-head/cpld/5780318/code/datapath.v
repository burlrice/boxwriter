module remapD(D, extrn, SRdrive, sys, Wadr, wctrl, SRoutodd, SRouteven, odb); 
    input	[7:0] D;
    input	[4:0] extrn;
    input	[6:0] SRdrive;
    input	[4:0] sys;
    input	[22:1] Wadr;
    input	[4:0] wctrl;
    output	SRoutodd;
    output	SRouteven;
    output	[6:0] odb;

    wire	[6:0] odb_out;
    wire	[6:0] oedb;
    wire	[6:0] oodb;
    wire	rst;
    wire	[6:0] datain;

    //remapDBgate	b2v_inst1(.Dgate(SRdrive[2]), .WriteLock(extrn[4]),.D(D),.gd(SYNTHESIZED_WIRE_5));
//////////////////////////////////////////////////////////////////////////////////////////
/*module remapDBgate(Dgate, WriteLock, D, gd); 
input	Dgate;
input	WriteLock;
input	[7:0] D;
output	[6:0] gd;*/

//wire	[6:0] gd_ALTERA_SYNTHESIZED;
    wire	SYNTHESIZED_WIRE_8;
//wire	SYNTHESIZED_WIRE_7;

    assign	datain[0] = D[0] & SYNTHESIZED_WIRE_8;
    assign	datain[1] = D[1] & SYNTHESIZED_WIRE_8;
    assign	datain[2] = D[2] & SYNTHESIZED_WIRE_8;
    assign	datain[3] = D[3] & SYNTHESIZED_WIRE_8;
    assign	datain[4] = D[4] & SYNTHESIZED_WIRE_8;
    assign	datain[5] = D[5] & SYNTHESIZED_WIRE_8;
    assign	datain[6] = D[6] & SYNTHESIZED_WIRE_8;
    assign	SYNTHESIZED_WIRE_8 = ~extrn[4] & SRdrive[2]; //Dgate & ~WriteLock;
//assign	SYNTHESIZED_WIRE_7 =  ~WriteLock;
//assign	gd = gd_ALTERA_SYNTHESIZED;
//endmodule
//////////////////////////////////////////////////////////////////////////////////////////
    remapSRodd	odd(.ClockSR(SRdrive[5]), .ShiftEnable(SRdrive[3]),.ResetSR(rst),.Readout(SRdrive[4]),.BankSelect(wctrl[0]),.D(datain),.Wadr(Wadr),.SRoutput(SRoutodd),.odb(oodb));

    remapSReven	even(.ClockSR(SRdrive[5]), .ShiftEnable(SRdrive[3]),.ResetSR(rst),.Readout(SRdrive[4]),.BankSelect(wctrl[0]),.D(datain),.Wadr(Wadr),.SRoutput(SRouteven),.odb(oedb));

    assign	odb_out[0] = oedb[0] | oodb[0];
    assign	odb_out[1] = oedb[1] | oodb[1];
    assign	odb_out[2] = oedb[2] | oodb[2];
    assign	odb_out[3] = oedb[3] | oodb[3];
    assign	odb_out[4] = oedb[4] | oodb[4];
    assign	odb_out[5] = oedb[5] | oodb[5];
    assign	odb_out[6] = oedb[6] | oodb[6];

    assign	rst = extrn[4] | sys[0];
    assign	odb = odb_out;
endmodule

module dotcountD(SRodddata, SRevendata, Readback, sysclock, SRdrive, sys, wctrl, dctr); 
input	SRodddata;
input	SRevendata;
input	Readback;
input	sysclock;
input	[6:0] SRdrive;
input	[4:0] sys;
input	[4:0] wctrl;
output	[7:0] dctr;

reg	[7:0] dctr_ALTERA_SYNTHESIZED;
wire	SYNTHESIZED_WIRE_32;
wire	SYNTHESIZED_WIRE_33;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_5;
wire	SYNTHESIZED_WIRE_7;
wire	SYNTHESIZED_WIRE_8;
wire	SYNTHESIZED_WIRE_9;
wire	SYNTHESIZED_WIRE_10;
wire	SYNTHESIZED_WIRE_13;
wire	SYNTHESIZED_WIRE_14;
wire	SYNTHESIZED_WIRE_15;
wire	SYNTHESIZED_WIRE_16;
reg	DFF_inst26;
reg	DFF_inst18;
wire	SYNTHESIZED_WIRE_19;
wire	SYNTHESIZED_WIRE_22;
wire	SYNTHESIZED_WIRE_25;
wire	SYNTHESIZED_WIRE_28;
wire	SYNTHESIZED_WIRE_31;

assign	SYNTHESIZED_WIRE_33 = 1;

always@(posedge SYNTHESIZED_WIRE_2 or negedge SYNTHESIZED_WIRE_32)
begin
if (!SYNTHESIZED_WIRE_32)
	begin
	dctr_ALTERA_SYNTHESIZED[0] = 0;
	end
else
	dctr_ALTERA_SYNTHESIZED[0] = dctr_ALTERA_SYNTHESIZED[0] ^ SYNTHESIZED_WIRE_33;
end

always@(posedge SYNTHESIZED_WIRE_5 or negedge SYNTHESIZED_WIRE_32)
begin
if (!SYNTHESIZED_WIRE_32)
	begin
	dctr_ALTERA_SYNTHESIZED[1] = 0;
	end
else
	dctr_ALTERA_SYNTHESIZED[1] = dctr_ALTERA_SYNTHESIZED[1] ^ SYNTHESIZED_WIRE_33;
end
assign	SYNTHESIZED_WIRE_5 =  ~dctr_ALTERA_SYNTHESIZED[0];
assign	SYNTHESIZED_WIRE_13 =  ~dctr_ALTERA_SYNTHESIZED[1];
assign	SYNTHESIZED_WIRE_19 =  ~dctr_ALTERA_SYNTHESIZED[2];
assign	SYNTHESIZED_WIRE_22 =  ~dctr_ALTERA_SYNTHESIZED[3];
assign	SYNTHESIZED_WIRE_25 =  ~dctr_ALTERA_SYNTHESIZED[4];
assign	SYNTHESIZED_WIRE_28 =  ~dctr_ALTERA_SYNTHESIZED[5];
assign	SYNTHESIZED_WIRE_31 =  ~dctr_ALTERA_SYNTHESIZED[6];

always@(posedge SYNTHESIZED_WIRE_7 or negedge SYNTHESIZED_WIRE_32)
begin
if (!SYNTHESIZED_WIRE_32)
	begin
	DFF_inst18 <= 0;
	end
else
	begin
	DFF_inst18 <= SYNTHESIZED_WIRE_8;
	end
end
assign	SYNTHESIZED_WIRE_2 = SYNTHESIZED_WIRE_9 & SYNTHESIZED_WIRE_10;

always@(posedge SYNTHESIZED_WIRE_13 or negedge SYNTHESIZED_WIRE_32)
begin
if (!SYNTHESIZED_WIRE_32)
	begin
	dctr_ALTERA_SYNTHESIZED[2] = 0;
	end
else
	dctr_ALTERA_SYNTHESIZED[2] = dctr_ALTERA_SYNTHESIZED[2] ^ SYNTHESIZED_WIRE_33;
end
assign	SYNTHESIZED_WIRE_16 = SRodddata & SYNTHESIZED_WIRE_14;
assign	SYNTHESIZED_WIRE_15 = SRevendata & wctrl[0];
assign	SYNTHESIZED_WIRE_9 = SYNTHESIZED_WIRE_15 | SYNTHESIZED_WIRE_16;
assign	SYNTHESIZED_WIRE_32 =  ~DFF_inst26;
assign	SYNTHESIZED_WIRE_14 =  ~wctrl[0];
assign	SYNTHESIZED_WIRE_7 =  ~Readback;

always@(posedge sysclock)
begin
	begin
	DFF_inst26 <= DFF_inst18;
	end
end
assign	SYNTHESIZED_WIRE_8 =  ~sys[3];

always@(posedge SYNTHESIZED_WIRE_19 or negedge SYNTHESIZED_WIRE_32)
begin
if (!SYNTHESIZED_WIRE_32)
	begin
	dctr_ALTERA_SYNTHESIZED[3] = 0;
	end
else
	dctr_ALTERA_SYNTHESIZED[3] = dctr_ALTERA_SYNTHESIZED[3] ^ SYNTHESIZED_WIRE_33;
end

always@(posedge SYNTHESIZED_WIRE_22 or negedge SYNTHESIZED_WIRE_32)
begin
if (!SYNTHESIZED_WIRE_32)
	begin
	dctr_ALTERA_SYNTHESIZED[4] = 0;
	end
else
	dctr_ALTERA_SYNTHESIZED[4] = dctr_ALTERA_SYNTHESIZED[4] ^ SYNTHESIZED_WIRE_33;
end

always@(posedge SYNTHESIZED_WIRE_25 or negedge SYNTHESIZED_WIRE_32)
begin
if (!SYNTHESIZED_WIRE_32)
	begin
	dctr_ALTERA_SYNTHESIZED[5] = 0;
	end
else
	dctr_ALTERA_SYNTHESIZED[5] = dctr_ALTERA_SYNTHESIZED[5] ^ SYNTHESIZED_WIRE_33;
end

always@(posedge SYNTHESIZED_WIRE_28 or negedge SYNTHESIZED_WIRE_32)
begin
if (!SYNTHESIZED_WIRE_32)
	begin
	dctr_ALTERA_SYNTHESIZED[6] = 0;
	end
else
	dctr_ALTERA_SYNTHESIZED[6] = dctr_ALTERA_SYNTHESIZED[6] ^ SYNTHESIZED_WIRE_33;
end

always@(posedge SYNTHESIZED_WIRE_31 or negedge SYNTHESIZED_WIRE_32)
begin
if (!SYNTHESIZED_WIRE_32)
	begin
	dctr_ALTERA_SYNTHESIZED[7] = 0;
	end
else
	dctr_ALTERA_SYNTHESIZED[7] = dctr_ALTERA_SYNTHESIZED[7] ^ SYNTHESIZED_WIRE_33;
end
assign	SYNTHESIZED_WIRE_10 =  ~SRdrive[5];
assign	dctr = dctr_ALTERA_SYNTHESIZED;
endmodule

/*module i2cbasic(
	i2cin_DAT,
	d,
	sys,
	i2cout_CLK,
	i2cout_RX,
	i2cout_DAT
);

input	i2cin_DAT;
input	[7:0] d;
input	[4:0] sys;
output	i2cout_CLK;
output	i2cout_RX;
output	i2cout_DAT;

wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_11;
wire	SYNTHESIZED_WIRE_12;
wire	SYNTHESIZED_WIRE_7;
wire	SYNTHESIZED_WIRE_9;
wire	SYNTHESIZED_WIRE_10;

assign	SYNTHESIZED_WIRE_12 = 0;



assign	i2cout_RX = SYNTHESIZED_WIRE_0 ^ SYNTHESIZED_WIRE_1;
assign	SYNTHESIZED_WIRE_7 = ~(SYNTHESIZED_WIRE_11 | d[0]);

lcell	b2v_inst10(.in(SYNTHESIZED_WIRE_11),
.out(SYNTHESIZED_WIRE_1));
assign	SYNTHESIZED_WIRE_0 = SYNTHESIZED_WIRE_11 | i2cin_DAT;
assign	SYNTHESIZED_WIRE_9 = ~(SYNTHESIZED_WIRE_11 | d[1]);
assign	i2cout_CLK = SYNTHESIZED_WIRE_7 ? SYNTHESIZED_WIRE_12 : 1'bz;

assign	i2cout_DAT = SYNTHESIZED_WIRE_9 ? SYNTHESIZED_WIRE_12 : 1'bz;

assign	SYNTHESIZED_WIRE_11 =  ~SYNTHESIZED_WIRE_10;
assign	SYNTHESIZED_WIRE_10 = sys[2] & d[7];


endmodule*/

module readback(Readback, rbb_rbpd7, 
    //i2c_SDAin, 
    rbdc, rbpd, sys, rbb);
    input	Readback, rbb_rbpd7;
    //input	i2c_SDAin;
input	[7:0] rbdc;
input	[6:0] rbpd;
input	[4:0] sys;
output	[7:0] rbb;

wire	[7:0] rbb_ALTERA_SYNTHESIZED;
wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
reg	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_49;
wire	SYNTHESIZED_WIRE_9;
wire	SYNTHESIZED_WIRE_50;
wire	SYNTHESIZED_WIRE_11;
wire	SYNTHESIZED_WIRE_12;
wire	SYNTHESIZED_WIRE_13;
wire	SYNTHESIZED_WIRE_15;
wire	SYNTHESIZED_WIRE_16;
wire	SYNTHESIZED_WIRE_17;
wire	SYNTHESIZED_WIRE_19;
wire	SYNTHESIZED_WIRE_20;
wire	SYNTHESIZED_WIRE_21;
wire	SYNTHESIZED_WIRE_23;
wire	SYNTHESIZED_WIRE_24;
wire	SYNTHESIZED_WIRE_25;
wire	SYNTHESIZED_WIRE_27;
wire	SYNTHESIZED_WIRE_28;
wire	SYNTHESIZED_WIRE_29;
wire	SYNTHESIZED_WIRE_30;
wire	SYNTHESIZED_WIRE_32;
wire	SYNTHESIZED_WIRE_33;
wire	SYNTHESIZED_WIRE_35;
wire	SYNTHESIZED_WIRE_37;
wire	SYNTHESIZED_WIRE_38;
wire	SYNTHESIZED_WIRE_40;
reg	SYNTHESIZED_WIRE_42;
reg	SYNTHESIZED_WIRE_43;
reg	SYNTHESIZED_WIRE_44;
reg	SYNTHESIZED_WIRE_45;
reg	SYNTHESIZED_WIRE_46;
reg	SYNTHESIZED_WIRE_47;
reg	SYNTHESIZED_WIRE_48;

assign	SYNTHESIZED_WIRE_9 = SYNTHESIZED_WIRE_0 | SYNTHESIZED_WIRE_1;
assign	rbb_ALTERA_SYNTHESIZED[0] = Readback ? SYNTHESIZED_WIRE_2 : 1'bz;

assign	SYNTHESIZED_WIRE_0 = rbdc[0] & SYNTHESIZED_WIRE_49;
assign	SYNTHESIZED_WIRE_12 = rbpd[1] & sys[3];
assign	SYNTHESIZED_WIRE_11 = rbdc[1] & SYNTHESIZED_WIRE_49;
assign	SYNTHESIZED_WIRE_16 = rbpd[2] & sys[3];
assign	SYNTHESIZED_WIRE_15 = rbdc[2] & SYNTHESIZED_WIRE_49;
assign	SYNTHESIZED_WIRE_20 = rbpd[3] & sys[3];
assign	SYNTHESIZED_WIRE_19 = rbdc[3] & SYNTHESIZED_WIRE_49;
assign	SYNTHESIZED_WIRE_24 = rbpd[4] & sys[3];
assign	SYNTHESIZED_WIRE_23 = rbdc[4] & SYNTHESIZED_WIRE_49;
assign	SYNTHESIZED_WIRE_29 = rbpd[5] & sys[3];
assign	SYNTHESIZED_WIRE_50 =  ~sys[4];
assign	SYNTHESIZED_WIRE_28 = rbdc[5] & SYNTHESIZED_WIRE_49;

always@(SYNTHESIZED_WIRE_50 or SYNTHESIZED_WIRE_9)
begin
if (SYNTHESIZED_WIRE_50)
	SYNTHESIZED_WIRE_2 <= SYNTHESIZED_WIRE_9;
end
assign	SYNTHESIZED_WIRE_13 = SYNTHESIZED_WIRE_11 | SYNTHESIZED_WIRE_12;

always@(SYNTHESIZED_WIRE_50 or SYNTHESIZED_WIRE_13)
begin
if (SYNTHESIZED_WIRE_50)
	SYNTHESIZED_WIRE_44 <= SYNTHESIZED_WIRE_13;
end
assign	SYNTHESIZED_WIRE_17 = SYNTHESIZED_WIRE_15 | SYNTHESIZED_WIRE_16;

always@(SYNTHESIZED_WIRE_50 or SYNTHESIZED_WIRE_17)
begin
if (SYNTHESIZED_WIRE_50)
	SYNTHESIZED_WIRE_42 <= SYNTHESIZED_WIRE_17;
end
assign	SYNTHESIZED_WIRE_21 = SYNTHESIZED_WIRE_19 | SYNTHESIZED_WIRE_20;

always@(SYNTHESIZED_WIRE_50 or SYNTHESIZED_WIRE_21)
begin
if (SYNTHESIZED_WIRE_50)
	SYNTHESIZED_WIRE_45 <= SYNTHESIZED_WIRE_21;
end
assign	SYNTHESIZED_WIRE_25 = SYNTHESIZED_WIRE_23 | SYNTHESIZED_WIRE_24;

always@(SYNTHESIZED_WIRE_50 or SYNTHESIZED_WIRE_25)
begin
if (SYNTHESIZED_WIRE_50)
	SYNTHESIZED_WIRE_46 <= SYNTHESIZED_WIRE_25;
end
assign	rbb_ALTERA_SYNTHESIZED[1] = Readback ? SYNTHESIZED_WIRE_27 : 1'bz;

assign	SYNTHESIZED_WIRE_30 = SYNTHESIZED_WIRE_28 | SYNTHESIZED_WIRE_29;

always@(SYNTHESIZED_WIRE_50 or SYNTHESIZED_WIRE_30)
begin
if (SYNTHESIZED_WIRE_50)
	SYNTHESIZED_WIRE_47 <= SYNTHESIZED_WIRE_30;
end
assign	SYNTHESIZED_WIRE_35 = SYNTHESIZED_WIRE_32 | SYNTHESIZED_WIRE_33;
assign	SYNTHESIZED_WIRE_33 = rbpd[6] & sys[3];
assign	SYNTHESIZED_WIRE_32 = rbdc[6] & SYNTHESIZED_WIRE_49;

always@(SYNTHESIZED_WIRE_50 or SYNTHESIZED_WIRE_35)
begin
if (SYNTHESIZED_WIRE_50)
	SYNTHESIZED_WIRE_48 <= SYNTHESIZED_WIRE_35;
end
assign	SYNTHESIZED_WIRE_40 = SYNTHESIZED_WIRE_37 | SYNTHESIZED_WIRE_38;
assign	SYNTHESIZED_WIRE_38 = rbb_rbpd7 & sys[3];
assign	SYNTHESIZED_WIRE_37 = rbdc[7] & SYNTHESIZED_WIRE_49;

always@(SYNTHESIZED_WIRE_50 or SYNTHESIZED_WIRE_40)
begin
if (SYNTHESIZED_WIRE_50)
	SYNTHESIZED_WIRE_43 <= SYNTHESIZED_WIRE_40;
end
assign	rbb_ALTERA_SYNTHESIZED[2] = Readback ? SYNTHESIZED_WIRE_42 : 1'bz;

assign	rbb_ALTERA_SYNTHESIZED[7] = Readback ? SYNTHESIZED_WIRE_43 : 1'bz;

assign	SYNTHESIZED_WIRE_49 =  ~sys[3];
assign	SYNTHESIZED_WIRE_27 = //i2c_SDAin |
        SYNTHESIZED_WIRE_44;
assign	rbb_ALTERA_SYNTHESIZED[3] = Readback ? SYNTHESIZED_WIRE_45 : 1'bz;

assign	rbb_ALTERA_SYNTHESIZED[4] = Readback ? SYNTHESIZED_WIRE_46 : 1'bz;

assign	rbb_ALTERA_SYNTHESIZED[5] = Readback ? SYNTHESIZED_WIRE_47 : 1'bz;

assign	rbb_ALTERA_SYNTHESIZED[6] = Readback ? SYNTHESIZED_WIRE_48 : 1'bz;

assign	SYNTHESIZED_WIRE_1 = rbpd[0] & sys[3];
assign	rbb = rbb_ALTERA_SYNTHESIZED;
endmodule

module datapathD( sysclock, Readback, i2cSDAin, 
        db, extrn, SRdrive, sys, wctrl,
	BurnBusy, LastAddress, I2C_CLK, I2C_SDA, AdrGen, cdb
        , rbb
        ); 
    input	sysclock, Readback;
input	i2cSDAin;
    input [7:0] db;
    input [6:0] SRdrive;
    input [4:0] extrn, sys, wctrl;
    output BurnBusy, LastAddress;
output	I2C_CLK, I2C_SDA;
    output [22:1] AdrGen;
    output [14:1] cdb;
    output [7:0] rbb;

    wire [22:1] SYNTHESIZED_WIRE_0;
    wire [3:0] octrl;
    wire [6:0] SYNTHESIZED_WIRE_9;
    wire rt_odd, rt_even, SYNTHESIZED_WIRE_6;
    wire [7:0] SYNTHESIZED_WIRE_7;

    remapD	b2v_inst(.D(db), .extrn(extrn), .SRdrive(SRdrive), 
        .sys(sys), .Wadr(SYNTHESIZED_WIRE_0), .wctrl(wctrl), 
        .SRoutodd(rt_odd), .SRouteven(rt_even), 
        .odb(SYNTHESIZED_WIRE_9));

    simplebufr	b2v_inst1(.outctrl(octrl), .wdb(SYNTHESIZED_WIRE_9),.cdb(cdb));

    datadriveD b2v_inst2(.sysclock(sysclock), .extrn(extrn), 
        .SRdrive(SRdrive), .wctrl(wctrl), .BurnBusy(BurnBusy), 
        .LastAddress(LastAddress), .AdrGen(AdrGen), 
        .AdrWrt(SYNTHESIZED_WIRE_0), .outctrl(octrl));

    dotcountD b2v_inst3(.sysclock(sysclock), .Readback(Readback),.SRodddata(rt_odd),.SRevendata(rt_even),.SRdrive(SRdrive),.sys(sys),.wctrl(wctrl),.dctr(SYNTHESIZED_WIRE_7));

    readback b2v_inst4(.Readback(Readback), .rbb_rbpd7(rt_odd), /*.i2c_SDAin(SYNTHESIZED_WIRE_6),*/ .rbdc(SYNTHESIZED_WIRE_7), .rbpd(SYNTHESIZED_WIRE_9), .sys(sys), .rbb(rbb));

    //i2cbasic	b2v_inst5(.i2cin_DAT(i2cSDAin), .d(db),.sys(sys),.i2cout_RX(SYNTHESIZED_WIRE_6),.i2cout_CLK(I2C_CLK),.i2cout_DAT(I2C_SDA));
endmodule


