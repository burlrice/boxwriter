module AdrGen_1r_22(ResetAdr, StepAdr, aGate, Extrn,
	LastAddress, AdrGen, AdrWrt); 
input	ResetAdr;
input	StepAdr;
input	aGate;
input	[4:0] Extrn;
output	LastAddress;
output	[22:1] AdrGen;
output	[22:1] AdrWrt;

wire	[22:1] AdrGen_ALTERA_SYNTHESIZED;
wire	[22:1] AdrWrt_ALTERA_SYNTHESIZED;
wire	SYNTHESIZED_WIRE_5;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_3;

ADRSR_1	b2v_inst(.Advance(StepAdr),
.Clear(SYNTHESIZED_WIRE_5),.aGate(aGate),.AdrGen8out(SYNTHESIZED_WIRE_3),.AdrGen(AdrGen_ALTERA_SYNTHESIZED[8:1]),.AdrWrt(AdrWrt_ALTERA_SYNTHESIZED[8:1]));

ADRSR_3	b2v_inst1(.Chainin(SYNTHESIZED_WIRE_1),
.Advance(StepAdr),.Clear(SYNTHESIZED_WIRE_5),.aGate(aGate),.LastAddress(LastAddress),.AdrGen(AdrGen_ALTERA_SYNTHESIZED[22:17]),.AdrWrt(AdrWrt_ALTERA_SYNTHESIZED[22:17]));

ADRSR_2	b2v_inst2(.Chainin(SYNTHESIZED_WIRE_3),
.Advance(StepAdr),.Clear(SYNTHESIZED_WIRE_5),.aGate(aGate),.AdrGen16out(SYNTHESIZED_WIRE_1),.AdrGen(AdrGen_ALTERA_SYNTHESIZED[16:9]),.AdrWrt(AdrWrt_ALTERA_SYNTHESIZED[16:9]));
assign	SYNTHESIZED_WIRE_5 = ResetAdr | Extrn[4];
assign	AdrGen = AdrGen_ALTERA_SYNTHESIZED;
assign	AdrWrt = AdrWrt_ALTERA_SYNTHESIZED;
endmodule

module ADRSR_1(Clear, Advance, aGate, AdrGen8out, AdrGen, AdrWrt); 
input	Clear;
input	Advance;
input	aGate;
output	AdrGen8out;
output	[8:1] AdrGen;
output	[8:1] AdrWrt;

reg	[8:1] AdrGen_ALTERA_SYNTHESIZED;
wire	[8:1] AdrWrt_ALTERA_SYNTHESIZED;
wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_10;
reg	DFF_158;

assign	SYNTHESIZED_WIRE_0 = 0;

always@(posedge Advance or negedge SYNTHESIZED_WIRE_10)
begin
if (!SYNTHESIZED_WIRE_10)
	begin
	DFF_158 <= 1;
	end
else
	begin
	DFF_158 <= SYNTHESIZED_WIRE_0;
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_10)
begin
if (!SYNTHESIZED_WIRE_10)
	begin
	AdrGen_ALTERA_SYNTHESIZED[1] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[1] <= DFF_158;
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_10)
begin
if (!SYNTHESIZED_WIRE_10)
	begin
	AdrGen_ALTERA_SYNTHESIZED[6] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[6] <= AdrGen_ALTERA_SYNTHESIZED[5];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_10)
begin
if (!SYNTHESIZED_WIRE_10)
	begin
	AdrGen_ALTERA_SYNTHESIZED[2] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[2] <= AdrGen_ALTERA_SYNTHESIZED[1];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_10)
begin
if (!SYNTHESIZED_WIRE_10)
	begin
	AdrGen_ALTERA_SYNTHESIZED[4] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[4] <= AdrGen_ALTERA_SYNTHESIZED[3];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_10)
begin
if (!SYNTHESIZED_WIRE_10)
	begin
	AdrGen_ALTERA_SYNTHESIZED[3] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[3] <= AdrGen_ALTERA_SYNTHESIZED[2];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_10)
begin
if (!SYNTHESIZED_WIRE_10)
	begin
	AdrGen_ALTERA_SYNTHESIZED[5] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[5] <= AdrGen_ALTERA_SYNTHESIZED[4];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_10)
begin
if (!SYNTHESIZED_WIRE_10)
	begin
	AdrGen_ALTERA_SYNTHESIZED[7] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[7] <= AdrGen_ALTERA_SYNTHESIZED[6];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_10)
begin
if (!SYNTHESIZED_WIRE_10)
	begin
	AdrGen_ALTERA_SYNTHESIZED[8] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[8] <= AdrGen_ALTERA_SYNTHESIZED[7];
	end
end
assign	SYNTHESIZED_WIRE_10 =  ~Clear;
assign	AdrWrt_ALTERA_SYNTHESIZED[1] = AdrGen_ALTERA_SYNTHESIZED[1] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[2] = AdrGen_ALTERA_SYNTHESIZED[2] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[3] = AdrGen_ALTERA_SYNTHESIZED[3] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[4] = AdrGen_ALTERA_SYNTHESIZED[4] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[5] = AdrGen_ALTERA_SYNTHESIZED[5] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[6] = AdrGen_ALTERA_SYNTHESIZED[6] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[7] = AdrGen_ALTERA_SYNTHESIZED[7] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[8] = AdrGen_ALTERA_SYNTHESIZED[8] & aGate;
assign	AdrGen8out = AdrGen_ALTERA_SYNTHESIZED[8];
assign	AdrGen = AdrGen_ALTERA_SYNTHESIZED;
assign	AdrWrt = AdrWrt_ALTERA_SYNTHESIZED;
endmodule

module ADRSR_2(Clear, Advance, Chainin, aGate, AdrGen16out, AdrGen,
	AdrWrt); 
input	Clear;
input	Advance;
input	Chainin;
input	aGate;
output	AdrGen16out;
output	[16:9] AdrGen;
output	[16:9] AdrWrt;

reg	[16:9] AdrGen_ALTERA_SYNTHESIZED;
wire	[16:9] AdrWrt_ALTERA_SYNTHESIZED;
wire	SYNTHESIZED_WIRE_8;

always@(posedge Advance or negedge SYNTHESIZED_WIRE_8)
begin
if (!SYNTHESIZED_WIRE_8)
	begin
	AdrGen_ALTERA_SYNTHESIZED[9] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[9] <= Chainin;
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_8)
begin
if (!SYNTHESIZED_WIRE_8)
	begin
	AdrGen_ALTERA_SYNTHESIZED[14] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[14] <= AdrGen_ALTERA_SYNTHESIZED[13];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_8)
begin
if (!SYNTHESIZED_WIRE_8)
	begin
	AdrGen_ALTERA_SYNTHESIZED[10] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[10] <= AdrGen_ALTERA_SYNTHESIZED[9];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_8)
begin
if (!SYNTHESIZED_WIRE_8)
	begin
	AdrGen_ALTERA_SYNTHESIZED[12] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[12] <= AdrGen_ALTERA_SYNTHESIZED[11];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_8)
begin
if (!SYNTHESIZED_WIRE_8)
	begin
	AdrGen_ALTERA_SYNTHESIZED[11] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[11] <= AdrGen_ALTERA_SYNTHESIZED[10];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_8)
begin
if (!SYNTHESIZED_WIRE_8)
	begin
	AdrGen_ALTERA_SYNTHESIZED[13] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[13] <= AdrGen_ALTERA_SYNTHESIZED[12];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_8)
begin
if (!SYNTHESIZED_WIRE_8)
	begin
	AdrGen_ALTERA_SYNTHESIZED[15] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[15] <= AdrGen_ALTERA_SYNTHESIZED[14];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_8)
begin
if (!SYNTHESIZED_WIRE_8)
	begin
	AdrGen_ALTERA_SYNTHESIZED[16] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[16] <= AdrGen_ALTERA_SYNTHESIZED[15];
	end
end
assign	SYNTHESIZED_WIRE_8 =  ~Clear;
assign	AdrWrt_ALTERA_SYNTHESIZED[9] = AdrGen_ALTERA_SYNTHESIZED[9] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[10] = AdrGen_ALTERA_SYNTHESIZED[10] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[11] = AdrGen_ALTERA_SYNTHESIZED[11] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[12] = AdrGen_ALTERA_SYNTHESIZED[12] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[13] = AdrGen_ALTERA_SYNTHESIZED[13] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[14] = AdrGen_ALTERA_SYNTHESIZED[14] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[15] = AdrGen_ALTERA_SYNTHESIZED[15] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[16] = AdrGen_ALTERA_SYNTHESIZED[16] & aGate;
assign	AdrGen16out = AdrGen_ALTERA_SYNTHESIZED[16];
assign	AdrGen = AdrGen_ALTERA_SYNTHESIZED;
assign	AdrWrt = AdrWrt_ALTERA_SYNTHESIZED;
endmodule

module ADRSR_3(Clear, Advance, Chainin, aGate, LastAddress,
	AdrGen, AdrWrt); 
input	Clear;
input	Advance;
input	Chainin;
input	aGate;
output	LastAddress;
output	[22:17] AdrGen;
output	[22:17] AdrWrt;

reg	[22:17] AdrGen_ALTERA_SYNTHESIZED;
wire	[22:17] AdrWrt_ALTERA_SYNTHESIZED;
wire	SYNTHESIZED_WIRE_6;

always@(posedge Advance or negedge SYNTHESIZED_WIRE_6)
begin
if (!SYNTHESIZED_WIRE_6)
	begin
	AdrGen_ALTERA_SYNTHESIZED[17] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[17] <= Chainin;
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_6)
begin
if (!SYNTHESIZED_WIRE_6)
	begin
	AdrGen_ALTERA_SYNTHESIZED[22] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[22] <= AdrGen_ALTERA_SYNTHESIZED[21];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_6)
begin
if (!SYNTHESIZED_WIRE_6)
	begin
	AdrGen_ALTERA_SYNTHESIZED[18] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[18] <= AdrGen_ALTERA_SYNTHESIZED[17];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_6)
begin
if (!SYNTHESIZED_WIRE_6)
	begin
	AdrGen_ALTERA_SYNTHESIZED[20] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[20] <= AdrGen_ALTERA_SYNTHESIZED[19];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_6)
begin
if (!SYNTHESIZED_WIRE_6)
	begin
	AdrGen_ALTERA_SYNTHESIZED[19] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[19] <= AdrGen_ALTERA_SYNTHESIZED[18];
	end
end

always@(posedge Advance or negedge SYNTHESIZED_WIRE_6)
begin
if (!SYNTHESIZED_WIRE_6)
	begin
	AdrGen_ALTERA_SYNTHESIZED[21] <= 0;
	end
else
	begin
	AdrGen_ALTERA_SYNTHESIZED[21] <= AdrGen_ALTERA_SYNTHESIZED[20];
	end
end
assign	SYNTHESIZED_WIRE_6 =  ~Clear;
assign	AdrWrt_ALTERA_SYNTHESIZED[17] = AdrGen_ALTERA_SYNTHESIZED[17] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[18] = AdrGen_ALTERA_SYNTHESIZED[18] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[19] = AdrGen_ALTERA_SYNTHESIZED[19] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[20] = AdrGen_ALTERA_SYNTHESIZED[20] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[21] = AdrGen_ALTERA_SYNTHESIZED[21] & aGate;
assign	AdrWrt_ALTERA_SYNTHESIZED[22] = AdrGen_ALTERA_SYNTHESIZED[22] & aGate;
assign	LastAddress = AdrGen_ALTERA_SYNTHESIZED[22];
assign	AdrGen = AdrGen_ALTERA_SYNTHESIZED;
assign	AdrWrt = AdrWrt_ALTERA_SYNTHESIZED;
endmodule

module datadriveD(sysclock, extrn, SRdrive, wctrl, BurnBusy, LastAddress,
	AdrGen, AdrWrt, outctrl); 
    input	sysclock;
    input	[4:0] extrn;
    input	[6:0] SRdrive;
    input	[4:0] wctrl;
    output	BurnBusy;
    output	LastAddress;
    output	[22:1] AdrGen;
    output	[22:1] AdrWrt;
    output	[3:0] outctrl;

    AdrGen_1r_22 b2v_inst(.ResetAdr(SRdrive[1]), .StepAdr(SRdrive[0]), 
        .aGate(SRdrive[6]), .Extrn(extrn), .LastAddress(LastAddress), 
        .AdrGen(AdrGen), .AdrWrt(AdrWrt)); 
    writectrl2 b2v_inst2(.sysclock(sysclock), .StartBurn(SRdrive[4]), 
        .extrn(extrn), .wctrl(wctrl), .BurnBusy(BurnBusy), .outctrl(outctrl));
endmodule

module pulsestops(WarmGate, SysClock, PulseWidthA, PulseWidthB, RunStop,
	BCtr, initiate, endcycle, terminate); 
input	WarmGate;
input	SysClock;
input	PulseWidthA;
input	PulseWidthB;
input	RunStop;
input	[5:0] BCtr;
output	initiate;
output	endcycle;
reg	endcycle;
output	terminate;

wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;
wire	SYNTHESIZED_WIRE_5;
wire	SYNTHESIZED_WIRE_6;
wire	SYNTHESIZED_WIRE_7;
wire	SYNTHESIZED_WIRE_8;
wire	SYNTHESIZED_WIRE_9;
wire	SYNTHESIZED_WIRE_10;
wire	SYNTHESIZED_WIRE_11;
wire	SYNTHESIZED_WIRE_12;
wire	SYNTHESIZED_WIRE_13;
wire	SYNTHESIZED_WIRE_14;
wire	SYNTHESIZED_WIRE_15;
wire	SYNTHESIZED_WIRE_16;
wire	SYNTHESIZED_WIRE_17;
wire	SYNTHESIZED_WIRE_18;
wire	SYNTHESIZED_WIRE_19;
wire	SYNTHESIZED_WIRE_20;
wire	SYNTHESIZED_WIRE_21;
wire	SYNTHESIZED_WIRE_22;
wire	SYNTHESIZED_WIRE_23;
wire	SYNTHESIZED_WIRE_26;

assign	SYNTHESIZED_WIRE_4 =  ~BCtr[1];
assign	SYNTHESIZED_WIRE_9 =  ~BCtr[4];

always@(posedge SYNTHESIZED_WIRE_0 or negedge RunStop)
begin
if (!RunStop)
	begin
	endcycle <= 0;
	end
else
	begin
	endcycle <= SYNTHESIZED_WIRE_1;
	end
end
assign	SYNTHESIZED_WIRE_26 = SYNTHESIZED_WIRE_2 & BCtr[4] & SYNTHESIZED_WIRE_3 & WarmGate;
assign	SYNTHESIZED_WIRE_2 = ~(BCtr[0] | BCtr[2] | BCtr[1]);
assign	SYNTHESIZED_WIRE_19 = ~(BCtr[1] | BCtr[5] | BCtr[3]);
assign	initiate = SYNTHESIZED_WIRE_4 & BCtr[0] & SYNTHESIZED_WIRE_5;
assign	SYNTHESIZED_WIRE_0 =  ~SysClock;
assign	SYNTHESIZED_WIRE_5 = ~(BCtr[3] | BCtr[2] | BCtr[4] | BCtr[5]);
assign	SYNTHESIZED_WIRE_20 = SYNTHESIZED_WIRE_6 & BCtr[2] & BCtr[5];
assign	SYNTHESIZED_WIRE_3 = ~(BCtr[3] | BCtr[5]);
assign	SYNTHESIZED_WIRE_7 = BCtr[0] & BCtr[1] & BCtr[2];
assign	SYNTHESIZED_WIRE_10 = ~(BCtr[3] | BCtr[0] | BCtr[4] | PulseWidthB);
assign	SYNTHESIZED_WIRE_8 = BCtr[3] & BCtr[5];
assign	SYNTHESIZED_WIRE_12 = SYNTHESIZED_WIRE_7 & SYNTHESIZED_WIRE_8 & SYNTHESIZED_WIRE_9;
assign	SYNTHESIZED_WIRE_22 = SYNTHESIZED_WIRE_10 & SYNTHESIZED_WIRE_11;
assign	SYNTHESIZED_WIRE_1 = SYNTHESIZED_WIRE_12 | SYNTHESIZED_WIRE_13;
assign	SYNTHESIZED_WIRE_21 = SYNTHESIZED_WIRE_14 & SYNTHESIZED_WIRE_15;
assign	SYNTHESIZED_WIRE_14 = ~(BCtr[1] | BCtr[0] | BCtr[2] | BCtr[4]);
assign	SYNTHESIZED_WIRE_11 = BCtr[1] & BCtr[2] & BCtr[5] & PulseWidthA;
assign	SYNTHESIZED_WIRE_18 =  ~PulseWidthA;
assign	SYNTHESIZED_WIRE_16 = ~(BCtr[2] | BCtr[4] | BCtr[0]);
assign	SYNTHESIZED_WIRE_17 = BCtr[1] & BCtr[3] & BCtr[5];
assign	SYNTHESIZED_WIRE_23 = SYNTHESIZED_WIRE_16 & PulseWidthA & SYNTHESIZED_WIRE_17 & PulseWidthB;
assign	SYNTHESIZED_WIRE_15 = BCtr[3] & BCtr[5] & SYNTHESIZED_WIRE_18 & PulseWidthB;
assign	SYNTHESIZED_WIRE_13 = BCtr[2] & SYNTHESIZED_WIRE_19 & BCtr[0] & BCtr[4] & WarmGate & WarmGate;
assign	terminate = SYNTHESIZED_WIRE_20 | SYNTHESIZED_WIRE_21 | SYNTHESIZED_WIRE_22 | SYNTHESIZED_WIRE_23 | SYNTHESIZED_WIRE_26 | SYNTHESIZED_WIRE_26;
assign	SYNTHESIZED_WIRE_6 = ~(BCtr[0] | BCtr[3] | BCtr[1] | BCtr[4] | PulseWidthA | PulseWidthB);
endmodule

module PULSTIME_2(WarmGate, PulseWidth0, Start, PulseWidth1,
	SysClock, burnbusy, loaddata, cleardata); 
    input WarmGate, PulseWidth0, Start;
    input PulseWidth1, SysClock;
    output burnbusy, loaddata, cleardata;
    reg	loaddata, cleardata;

    reg	[5:0] Bctr;
    reg	SYNTHESIZED_WIRE_17;
    wire SYNTHESIZED_WIRE_0, SYNTHESIZED_WIRE_1, SYNTHESIZED_WIRE_2;
    wire SYNTHESIZED_WIRE_3, SYNTHESIZED_WIRE_4, SYNTHESIZED_WIRE_5;
    wire SYNTHESIZED_WIRE_6, SYNTHESIZED_WIRE_7, SYNTHESIZED_WIRE_18;
    wire SYNTHESIZED_WIRE_9, SYNTHESIZED_WIRE_11, SYNTHESIZED_WIRE_12;
    wire SYNTHESIZED_WIRE_13, SYNTHESIZED_WIRE_14, SYNTHESIZED_WIRE_15;
    wire SYNTHESIZED_WIRE_16;

    assign	burnbusy = SYNTHESIZED_WIRE_17;
    assign	SYNTHESIZED_WIRE_2 = Bctr[0] ^ Bctr[1];
    assign	SYNTHESIZED_WIRE_3 = Bctr[2] ^ SYNTHESIZED_WIRE_0;
    assign	SYNTHESIZED_WIRE_0 = Bctr[0] & Bctr[1];

    always@(posedge SysClock or negedge SYNTHESIZED_WIRE_17)
    begin
    if (!SYNTHESIZED_WIRE_17)
	begin
	Bctr[0] <= 1;
	Bctr[1] <= 1;
	Bctr[2] <= 1;
	Bctr[3] <= 1;
	Bctr[4] <= 1;
	Bctr[5] <= 1;
	end
    else
	begin
	Bctr[0] <= SYNTHESIZED_WIRE_1;
	Bctr[1] <= SYNTHESIZED_WIRE_2;
	Bctr[2] <= SYNTHESIZED_WIRE_3;
	Bctr[3] <= SYNTHESIZED_WIRE_4;
	Bctr[4] <= SYNTHESIZED_WIRE_5;
	Bctr[5] <= SYNTHESIZED_WIRE_6;
	end
    end

    assign	SYNTHESIZED_WIRE_4 = Bctr[3] ^ SYNTHESIZED_WIRE_7;
    assign	SYNTHESIZED_WIRE_7 = Bctr[2] & Bctr[1] & Bctr[0];

    always@(posedge SYNTHESIZED_WIRE_18 or negedge SYNTHESIZED_WIRE_17)
    begin
    if (!SYNTHESIZED_WIRE_17)
	begin
	cleardata <= 0;
	loaddata <= 0;
	end
    else
	begin
	cleardata <= SYNTHESIZED_WIRE_9;
	loaddata <= SYNTHESIZED_WIRE_11;
	end
    end

    always@(posedge SysClock or negedge SYNTHESIZED_WIRE_12)
    begin
    if (!SYNTHESIZED_WIRE_12)
	begin
	SYNTHESIZED_WIRE_17 <= 0;
	end
    else
	begin
	SYNTHESIZED_WIRE_17 <= SYNTHESIZED_WIRE_13;
	end
    end
    assign	SYNTHESIZED_WIRE_14 = Bctr[0] & Bctr[1] & Bctr[2] & Bctr[3];
    assign	SYNTHESIZED_WIRE_5 = Bctr[4] ^ SYNTHESIZED_WIRE_14;
    assign	SYNTHESIZED_WIRE_15 = Bctr[1] & Bctr[2] & Bctr[3] & Bctr[0] & Bctr[4] & Bctr[4];
    assign	SYNTHESIZED_WIRE_6 = Bctr[5] ^ SYNTHESIZED_WIRE_15;
    assign	SYNTHESIZED_WIRE_1 =  ~Bctr[0];
    assign	SYNTHESIZED_WIRE_12 =  ~SYNTHESIZED_WIRE_16;

    pulsestops	b2v_inst(.SysClock(SysClock), .RunStop(SYNTHESIZED_WIRE_17),
        .PulseWidthA(PulseWidth0), .PulseWidthB(PulseWidth1), .WarmGate(WarmGate), 
        .BCtr(Bctr), .initiate(SYNTHESIZED_WIRE_11), 
        .terminate(SYNTHESIZED_WIRE_9), .endcycle(SYNTHESIZED_WIRE_16));
    assign	SYNTHESIZED_WIRE_13 = SYNTHESIZED_WIRE_17 | Start;
    assign	SYNTHESIZED_WIRE_18 =  ~SysClock;
endmodule

module sequencer(
	BurnBusy,
	sysclock,
	StartCycle,
	LastAddress,
	extrn,
	sys,
	ReadBusy,
	SRdrive
);

input	BurnBusy;
input	sysclock;
input	StartCycle;
input	LastAddress;
input	[4:0] extrn;
input	[4:0] sys;
output	ReadBusy;
output	[6:0] SRdrive;

wire	[6:0] SRdrive_ALTERA_SYNTHESIZED;
wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;
wire	SYNTHESIZED_WIRE_5;

SRreader	b2v_inst(.StartRead(SYNTHESIZED_WIRE_0),
.sysclock(sysclock),.LastAddress(LastAddress),.BurnBusy(BurnBusy),.ReadCycle(SRdrive_ALTERA_SYNTHESIZED[3]),.WriteLock(extrn[4]),.StepAdrGen(SYNTHESIZED_WIRE_1),.ReadRegister(SRdrive_ALTERA_SYNTHESIZED[4]),.ShiftClock(SRdrive_ALTERA_SYNTHESIZED[5]),.ReadAdrClr(SYNTHESIZED_WIRE_3),.CloseCycle(SYNTHESIZED_WIRE_5));
assign	SRdrive_ALTERA_SYNTHESIZED[0] = SYNTHESIZED_WIRE_1 | SYNTHESIZED_WIRE_2;
assign	SRdrive_ALTERA_SYNTHESIZED[1] = SYNTHESIZED_WIRE_3 | SYNTHESIZED_WIRE_4;

SRdatawrite	b2v_inst3(.WriteData(StartCycle),
.sysclock(sysclock),.LastAddress(LastAddress),.ReadOnly(sys[1]),.ClearCycle(sys[0]),.EndReadCycle(SYNTHESIZED_WIRE_5),.Lockout(extrn[4]),.StepWriteAdr(SYNTHESIZED_WIRE_2),.AdrGate(SRdrive_ALTERA_SYNTHESIZED[6]),.WriteGate(SRdrive_ALTERA_SYNTHESIZED[2]),.WriteAdrClr(SYNTHESIZED_WIRE_4),.ReadCycle(SRdrive_ALTERA_SYNTHESIZED[3]),.StartRead(SYNTHESIZED_WIRE_0));
assign	ReadBusy = SRdrive_ALTERA_SYNTHESIZED[3];
assign	SRdrive = SRdrive_ALTERA_SYNTHESIZED;
endmodule

module SRdatawrite(
	sysclock,
	LastAddress,
	ClearCycle,
	WriteData,
	Lockout,
	ReadOnly,
	EndReadCycle,
	WriteGate,
	WriteAdrClr,
	ReadCycle,
	StartRead,
	StepWriteAdr,
	AdrGate
);

input	sysclock;
input	LastAddress;
input	ClearCycle;
input	WriteData;
input	Lockout;
input	ReadOnly;
input	EndReadCycle;
output	WriteGate;
output	WriteAdrClr;
output	ReadCycle;
output	StartRead;
reg	StartRead;
output	StepWriteAdr;
output	AdrGate;

wire	SYNTHESIZED_WIRE_8;
reg	DFF_inst;
reg	DFF_inst9;
reg	SYNTHESIZED_WIRE_9;
reg	SYNTHESIZED_WIRE_10;
reg	DFF_inst13;
reg	SYNTHESIZED_WIRE_11;
reg	SYNTHESIZED_WIRE_12;
reg	SYNTHESIZED_WIRE_13;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_14;
wire	SYNTHESIZED_WIRE_5;
wire	SYNTHESIZED_WIRE_6;

assign	WriteAdrClr = SYNTHESIZED_WIRE_10;
assign	ReadCycle = SYNTHESIZED_WIRE_11;
assign	StepWriteAdr = DFF_inst;
assign	AdrGate = SYNTHESIZED_WIRE_9;




always@(posedge sysclock or negedge SYNTHESIZED_WIRE_8)
begin
if (!SYNTHESIZED_WIRE_8)
	begin
	DFF_inst <= 0;
	end
else
	begin
	DFF_inst <= WriteData;
	end
end

always@(posedge sysclock or negedge SYNTHESIZED_WIRE_8)
begin
if (!SYNTHESIZED_WIRE_8)
	begin
	SYNTHESIZED_WIRE_13 <= 0;
	end
else
	begin
	SYNTHESIZED_WIRE_13 <= DFF_inst;
	end
end

always@(posedge sysclock)
begin
	begin
	SYNTHESIZED_WIRE_10 <= DFF_inst9;
	end
end
assign	SYNTHESIZED_WIRE_2 = ~(ClearCycle | EndReadCycle | Lockout);

always@(posedge sysclock)
begin
	begin
	SYNTHESIZED_WIRE_12 <= SYNTHESIZED_WIRE_9;
	end
end

always@(posedge sysclock)
begin
	begin
	DFF_inst13 <= SYNTHESIZED_WIRE_10;
	end
end

always@(posedge sysclock)
begin
	begin
	StartRead <= DFF_inst13;
	end
end
assign	SYNTHESIZED_WIRE_8 = ~(Lockout | SYNTHESIZED_WIRE_11);
assign	WriteGate = SYNTHESIZED_WIRE_9 | SYNTHESIZED_WIRE_12 | SYNTHESIZED_WIRE_13;
assign	SYNTHESIZED_WIRE_6 =  ~SYNTHESIZED_WIRE_10;

always@(posedge sysclock or negedge SYNTHESIZED_WIRE_2)
begin
if (!SYNTHESIZED_WIRE_2)
	begin
	SYNTHESIZED_WIRE_11 <= 0;
	end
else
	begin
	SYNTHESIZED_WIRE_11 <= SYNTHESIZED_WIRE_3;
	end
end
assign	SYNTHESIZED_WIRE_5 = SYNTHESIZED_WIRE_12 & LastAddress;

always@(posedge sysclock)
begin
	begin
	SYNTHESIZED_WIRE_9 <= SYNTHESIZED_WIRE_13;
	end
end
assign	SYNTHESIZED_WIRE_3 = SYNTHESIZED_WIRE_11 | SYNTHESIZED_WIRE_14;
assign	SYNTHESIZED_WIRE_14 = ReadOnly | SYNTHESIZED_WIRE_5;

always@(posedge sysclock or negedge SYNTHESIZED_WIRE_6)
begin
if (!SYNTHESIZED_WIRE_6)
	begin
	DFF_inst9 <= 0;
	end
else
	begin
	DFF_inst9 <= SYNTHESIZED_WIRE_14;
	end
end
endmodule

module SRreadcnt(
	StartFlow,
	Sysclock,
	CountReset,
	RestartFlow,
	CountMarkA,
	CountMarkB,
	CountMarkD,
	CountMarkC
);

input	StartFlow;
input	Sysclock;
input	CountReset;
input	RestartFlow;
output	CountMarkA;
output	CountMarkB;
output	CountMarkD;
output	CountMarkC;

reg	SYNTHESIZED_WIRE_8;
reg	SYNTHESIZED_WIRE_9;
reg	SYNTHESIZED_WIRE_10;
wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
reg	SYNTHESIZED_WIRE_11;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;
reg	DFF_138;
wire	SYNTHESIZED_WIRE_5;
wire	SYNTHESIZED_WIRE_6;
wire	SYNTHESIZED_WIRE_7;

assign	CountMarkA = SYNTHESIZED_WIRE_8;
assign	CountMarkB = SYNTHESIZED_WIRE_9;
assign	CountMarkD = DFF_138;
assign	CountMarkC = SYNTHESIZED_WIRE_10;



assign	SYNTHESIZED_WIRE_1 = SYNTHESIZED_WIRE_8 ^ SYNTHESIZED_WIRE_9;
assign	SYNTHESIZED_WIRE_2 = SYNTHESIZED_WIRE_10 ^ SYNTHESIZED_WIRE_0;
assign	SYNTHESIZED_WIRE_0 = SYNTHESIZED_WIRE_8 & SYNTHESIZED_WIRE_9;

always@(posedge Sysclock or negedge SYNTHESIZED_WIRE_11)
begin
if (!SYNTHESIZED_WIRE_11)
	begin
	SYNTHESIZED_WIRE_9 <= 1;
	end
else
	begin
	SYNTHESIZED_WIRE_9 <= SYNTHESIZED_WIRE_1;
	end
end

always@(posedge Sysclock or negedge SYNTHESIZED_WIRE_11)
begin
if (!SYNTHESIZED_WIRE_11)
	begin
	SYNTHESIZED_WIRE_10 <= 1;
	end
else
	begin
	SYNTHESIZED_WIRE_10 <= SYNTHESIZED_WIRE_2;
	end
end

always@(posedge Sysclock or negedge SYNTHESIZED_WIRE_11)
begin
if (!SYNTHESIZED_WIRE_11)
	begin
	SYNTHESIZED_WIRE_8 <= 1;
	end
else
	begin
	SYNTHESIZED_WIRE_8 <= SYNTHESIZED_WIRE_3;
	end
end

always@(posedge Sysclock or negedge SYNTHESIZED_WIRE_11)
begin
if (!SYNTHESIZED_WIRE_11)
	begin
	DFF_138 <= 1;
	end
else
	begin
	DFF_138 <= SYNTHESIZED_WIRE_4;
	end
end
assign	SYNTHESIZED_WIRE_4 = DFF_138 ^ SYNTHESIZED_WIRE_5;
assign	SYNTHESIZED_WIRE_5 = SYNTHESIZED_WIRE_10 & SYNTHESIZED_WIRE_9 & SYNTHESIZED_WIRE_8;

always@(posedge Sysclock or negedge SYNTHESIZED_WIRE_6)
begin
if (!SYNTHESIZED_WIRE_6)
	begin
	SYNTHESIZED_WIRE_11 <= 0;
	end
else
	begin
	SYNTHESIZED_WIRE_11 <= SYNTHESIZED_WIRE_7;
	end
end
assign	SYNTHESIZED_WIRE_3 =  ~SYNTHESIZED_WIRE_8;
assign	SYNTHESIZED_WIRE_6 =  ~CountReset;
assign	SYNTHESIZED_WIRE_7 = RestartFlow | SYNTHESIZED_WIRE_11 | StartFlow;


endmodule

module SRreader(
	BurnBusy,
	LastAddress,
	WriteLock,
	StartRead,
	sysclock,
	ReadCycle,
	StepAdrGen,
	ReadRegister,
	ShiftClock,
	ReadAdrClr,
	CloseCycle
);

input	BurnBusy;
input	LastAddress;
input	WriteLock;
input	StartRead;
input	sysclock;
input	ReadCycle;
output	StepAdrGen;
output	ReadRegister;
output	ShiftClock;
output	ReadAdrClr;
output	CloseCycle;

wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;
wire	SYNTHESIZED_WIRE_5;
wire	SYNTHESIZED_WIRE_6;
wire	SYNTHESIZED_WIRE_7;

SRrestart	b2v_inst(.StartFlow(StartRead),
.Sysclock(sysclock),.ShiftClockEnd(SYNTHESIZED_WIRE_0),.LastAddress(LastAddress),.BurnBusy(BurnBusy),.ReadCycle(ReadCycle),.WriteLock(WriteLock),.StartPulse(SYNTHESIZED_WIRE_1),.RestartFlow(SYNTHESIZED_WIRE_2),.CounterReset(SYNTHESIZED_WIRE_3),.StartClear(ReadAdrClr),.CloseCycle(CloseCycle));

SRreadcnt	b2v_inst1(.StartFlow(SYNTHESIZED_WIRE_1),
.RestartFlow(SYNTHESIZED_WIRE_2),.Sysclock(sysclock),.CountReset(SYNTHESIZED_WIRE_3),.CountMarkA(SYNTHESIZED_WIRE_4),.CountMarkB(SYNTHESIZED_WIRE_5),.CountMarkC(SYNTHESIZED_WIRE_6),.CountMarkD(SYNTHESIZED_WIRE_7));

SRreadmrks	b2v_inst2(.Sysclock(sysclock),
.CountMarkA(SYNTHESIZED_WIRE_4),.CountMarkB(SYNTHESIZED_WIRE_5),.CountMarkC(SYNTHESIZED_WIRE_6),.CountMarkD(SYNTHESIZED_WIRE_7),.StepAdrGen(StepAdrGen),.ReadRegister(ReadRegister),.ShiftClock(ShiftClock),.ShiftClockEnd(SYNTHESIZED_WIRE_0));
endmodule

module SRreadmrks(
	CountMarkB,
	CountMarkC,
	CountMarkD,
	CountMarkA,
	Sysclock,
	StepAdrGen,
	ReadRegister,
	ShiftClock,
	ShiftClockEnd
);

input	CountMarkB;
input	CountMarkC;
input	CountMarkD;
input	CountMarkA;
input	Sysclock;
output	StepAdrGen;
output	ReadRegister;
output	ShiftClock;
output	ShiftClockEnd;

wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;
wire	SYNTHESIZED_WIRE_5;
wire	SYNTHESIZED_WIRE_6;
wire	SYNTHESIZED_WIRE_21;
wire	SYNTHESIZED_WIRE_8;
wire	SYNTHESIZED_WIRE_9;
reg	SYNTHESIZED_WIRE_22;
wire	SYNTHESIZED_WIRE_11;
reg	SYNTHESIZED_WIRE_23;
wire	SYNTHESIZED_WIRE_12;
wire	SYNTHESIZED_WIRE_13;
wire	SYNTHESIZED_WIRE_14;
wire	SYNTHESIZED_WIRE_15;
wire	SYNTHESIZED_WIRE_16;
wire	SYNTHESIZED_WIRE_17;
wire	SYNTHESIZED_WIRE_24;
wire	SYNTHESIZED_WIRE_19;

assign	ReadRegister = SYNTHESIZED_WIRE_22;

always@(posedge Sysclock or negedge SYNTHESIZED_WIRE_0)
begin
if (!SYNTHESIZED_WIRE_0)
	begin
	SYNTHESIZED_WIRE_22 <= 0;
	end
else
	begin
	SYNTHESIZED_WIRE_22 <= SYNTHESIZED_WIRE_1;
	end
end

always@(posedge Sysclock or negedge SYNTHESIZED_WIRE_2)
begin
if (!SYNTHESIZED_WIRE_2)
	begin
	SYNTHESIZED_WIRE_23 <= 0;
	end
else
	begin
	SYNTHESIZED_WIRE_23 <= SYNTHESIZED_WIRE_3;
	end
end
assign	SYNTHESIZED_WIRE_12 = SYNTHESIZED_WIRE_4 & SYNTHESIZED_WIRE_5;
assign	StepAdrGen = SYNTHESIZED_WIRE_6 & SYNTHESIZED_WIRE_21;
assign	SYNTHESIZED_WIRE_21 = SYNTHESIZED_WIRE_8 & SYNTHESIZED_WIRE_9;
assign	SYNTHESIZED_WIRE_17 = ~(CountMarkC | CountMarkD | CountMarkA);
assign	SYNTHESIZED_WIRE_4 = CountMarkA & CountMarkB & CountMarkC;
assign	SYNTHESIZED_WIRE_1 = SYNTHESIZED_WIRE_22 | SYNTHESIZED_WIRE_21;
assign	SYNTHESIZED_WIRE_3 = SYNTHESIZED_WIRE_11 | SYNTHESIZED_WIRE_23;
assign	SYNTHESIZED_WIRE_0 = ~(SYNTHESIZED_WIRE_12 & SYNTHESIZED_WIRE_13);
assign	SYNTHESIZED_WIRE_8 = ~(CountMarkB | CountMarkA | CountMarkC | CountMarkD);
assign	SYNTHESIZED_WIRE_14 = CountMarkB & CountMarkD;
assign	SYNTHESIZED_WIRE_13 =  ~Sysclock;
assign	SYNTHESIZED_WIRE_5 =  ~CountMarkD;
assign	SYNTHESIZED_WIRE_6 =  ~SYNTHESIZED_WIRE_22;
assign	SYNTHESIZED_WIRE_9 =  ~SYNTHESIZED_WIRE_23;
assign	SYNTHESIZED_WIRE_16 =  ~Sysclock;
assign	SYNTHESIZED_WIRE_19 =  ~SYNTHESIZED_WIRE_22;
assign	SYNTHESIZED_WIRE_11 = SYNTHESIZED_WIRE_14 & SYNTHESIZED_WIRE_15;
assign	SYNTHESIZED_WIRE_15 = ~(CountMarkA | CountMarkC);
assign	ShiftClock = SYNTHESIZED_WIRE_16 & SYNTHESIZED_WIRE_23;
assign	SYNTHESIZED_WIRE_24 = CountMarkB & SYNTHESIZED_WIRE_17;
assign	SYNTHESIZED_WIRE_2 = ~(SYNTHESIZED_WIRE_24 | SYNTHESIZED_WIRE_22);
assign	ShiftClockEnd = SYNTHESIZED_WIRE_19 & SYNTHESIZED_WIRE_24;
endmodule

module SRrestart(
	Sysclock,
	StartFlow,
	ShiftClockEnd,
	BurnBusy,
	LastAddress,
	WriteLock,
	ReadCycle,
	RestartFlow,
	CounterReset,
	StartClear,
	StartPulse,
	CloseCycle
);

input	Sysclock;
input	StartFlow;
input	ShiftClockEnd;
input	BurnBusy;
input	LastAddress;
input	WriteLock;
input	ReadCycle;
output	RestartFlow;
output	CounterReset;
output	StartClear;
output	StartPulse;
output	CloseCycle;
reg	CloseCycle;

wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_10;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;
reg	DFF_inst10;
reg	SYNTHESIZED_WIRE_11;
wire	SYNTHESIZED_WIRE_6;
reg	DFF_inst13;
reg	DFF_inst;
wire	SYNTHESIZED_WIRE_7;
reg	DFF_inst6;
wire	SYNTHESIZED_WIRE_8;
wire	SYNTHESIZED_WIRE_9;
reg	DFF_inst4;

assign	CounterReset = DFF_inst4;
assign	StartClear = DFF_inst13;
assign	StartPulse = DFF_inst;

always@(posedge Sysclock or negedge SYNTHESIZED_WIRE_0)
begin
if (!SYNTHESIZED_WIRE_0)
	begin
	DFF_inst <= 0;
	end
else
	begin
	DFF_inst <= StartFlow;
	end
end

always@(posedge Sysclock)
begin
	begin
	DFF_inst10 <= SYNTHESIZED_WIRE_10;
	end
end
assign	SYNTHESIZED_WIRE_7 =  ~ReadCycle;
assign	RestartFlow = SYNTHESIZED_WIRE_10 & SYNTHESIZED_WIRE_3;

always@(posedge Sysclock)
begin
	begin
	DFF_inst13 <= SYNTHESIZED_WIRE_4;
	end
end
assign	SYNTHESIZED_WIRE_8 =  ~DFF_inst10;
assign	SYNTHESIZED_WIRE_4 = SYNTHESIZED_WIRE_10 & LastAddress;
assign	SYNTHESIZED_WIRE_10 = SYNTHESIZED_WIRE_11 & SYNTHESIZED_WIRE_6;

always@(posedge Sysclock)
begin
	begin
	CloseCycle <= DFF_inst13;
	end
end
assign	SYNTHESIZED_WIRE_6 =  ~BurnBusy;

always@(posedge Sysclock or negedge ReadCycle)
begin
if (!ReadCycle)
	begin
	DFF_inst4 <= 0;
	end
else
	begin
	DFF_inst4 <= ShiftClockEnd;
	end
end
assign	SYNTHESIZED_WIRE_3 =  ~LastAddress;

always@(posedge Sysclock)
begin
	begin
	DFF_inst6 <= DFF_inst;
	end
end
assign	SYNTHESIZED_WIRE_0 = ~(SYNTHESIZED_WIRE_7 | DFF_inst6 | WriteLock);

always@(posedge Sysclock or negedge SYNTHESIZED_WIRE_8)
begin
if (!SYNTHESIZED_WIRE_8)
	begin
	SYNTHESIZED_WIRE_11 <= 0;
	end
else
	begin
	SYNTHESIZED_WIRE_11 <= SYNTHESIZED_WIRE_9;
	end
end
assign	SYNTHESIZED_WIRE_9 = SYNTHESIZED_WIRE_11 | DFF_inst4;
endmodule

module writectrl2(StartBurn, sysclock, extrn, wctrl, BurnBusy, outctrl);
    input StartBurn, sysclock;
    input [4:0] extrn, wctrl;
    output BurnBusy;
    output [3:0] outctrl;

    wire [3:0] outctrl_ALTERA_SYNTHESIZED;
    wire SYNTHESIZED_WIRE_8, SYNTHESIZED_WIRE_9, SYNTHESIZED_WIRE_2;
    wire SYNTHESIZED_WIRE_3, SYNTHESIZED_WIRE_10;

    assign outctrl_ALTERA_SYNTHESIZED[0] = SYNTHESIZED_WIRE_8 & SYNTHESIZED_WIRE_9;
    assign outctrl_ALTERA_SYNTHESIZED[3] = ~(extrn[4] | SYNTHESIZED_WIRE_2);
    assign outctrl_ALTERA_SYNTHESIZED[1] = ~(extrn[4] | SYNTHESIZED_WIRE_3);

    PULSTIME_2 b2v_inst2(.SysClock(sysclock), .PulseWidth0(wctrl[3]), 
        .PulseWidth1(wctrl[4]), .WarmGate(wctrl[1]), .Start(StartBurn), 
        .loaddata(SYNTHESIZED_WIRE_9), .cleardata(SYNTHESIZED_WIRE_10), 
        .burnbusy(BurnBusy));
    assign SYNTHESIZED_WIRE_3 = SYNTHESIZED_WIRE_8 & SYNTHESIZED_WIRE_10;
    assign outctrl_ALTERA_SYNTHESIZED[2] = wctrl[0] & SYNTHESIZED_WIRE_9;
    assign SYNTHESIZED_WIRE_2 = wctrl[0] & SYNTHESIZED_WIRE_10;
    assign SYNTHESIZED_WIRE_8 =  ~wctrl[0];
    assign outctrl = outctrl_ALTERA_SYNTHESIZED;
endmodule
