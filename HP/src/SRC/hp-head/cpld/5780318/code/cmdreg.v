module cmdreg1b(WriteCmd, CmdSelect_a, CmdSelect_b, 
        Rtsractive, R10xactive, PulseWidth_A, PulseWidth_B,
        Write_Lock, Rtsrdrive, PulseWidthA, PulseWidthB, 
	R10xdrive,bank_select, BankSelect,  
        WriteLock);
    input WriteCmd, Rtsractive, R10xactive;
    input bank_select, PulseWidth_A, PulseWidth_B;
    input CmdSelect_b, CmdSelect_a, Write_Lock;
    output Rtsrdrive, R10xdrive, BankSelect, PulseWidthA, PulseWidthB, WriteLock;
    reg Rtsrdrive, R10xdrive, BankSelect, PulseWidthA, PulseWidthB, WriteLock;

    wire active;

    always@(posedge active)
    begin
	begin
	Rtsrdrive <= Rtsractive;
	R10xdrive <= R10xactive;
	WriteLock <= Write_Lock;
	BankSelect <= bank_select;
	PulseWidthA <= PulseWidth_A;
	PulseWidthB <= PulseWidth_B;
	end
    end
    assign active = WriteCmd & ~CmdSelect_a & ~CmdSelect_b;
endmodule

module cmdreg2b(WriteCmd, CmdSelect_a, CmdSelect_b, 
        Reset_SR, Read_SR, ResetSR, ReadSR,
    Warm_up, Write_Temp, WarmUp, WriteTemp);
    input Reset_SR, Read_SR, WriteCmd, CmdSelect_a, CmdSelect_b,  Warm_up, Write_Temp;
    output ResetSR, ReadSR, WarmUp, WriteTemp;
    reg WarmUp, ResetSR, ReadSR, WriteTemp;

    wire active;

    always@(posedge active)
    begin
	begin
	ReadSR <= Read_SR;
	ResetSR <= Reset_SR;
	WarmUp <= Warm_up;
	WriteTemp <= Write_Temp;
	end
    end
    assign active = WriteCmd & CmdSelect_a & ~CmdSelect_b;
endmodule

module cmdreg3b(WriteCmd, CmdSelect_a, CmdSelect_b, 
    LEDred, LEDgreen, 
        Reddrive, Greendrive, 
	Select_RB, RB_capture, I2Cbus_ctrl, 
	SelectRB, RBcapture, I2Cbusctrl);
    input WriteCmd, LEDred, LEDgreen;
    input CmdSelect_a, CmdSelect_b, Select_RB, RB_capture;
    input I2Cbus_ctrl;
    output SelectRB, RBcapture, I2Cbusctrl, Reddrive, Greendrive;
    reg	SelectRB, RBcapture, I2Cbusctrl, Reddrive, Greendrive;

    wire active;

    always@(posedge active)
    begin
	begin
	Reddrive <= LEDred;
	Greendrive <= LEDgreen;
	I2Cbusctrl <= I2Cbus_ctrl;
	SelectRB <= Select_RB;
	RBcapture <= RB_capture;
	end
    end
    assign active = WriteCmd & ~CmdSelect_a & CmdSelect_b;
endmodule

module cmdreg4b(WriteCmd, CmdSelect_a, CmdSelect_b,
	Trig_edge, Gate_level, trig_4, Gate_trig, Reversal,
	TriggerEdge, GateLevel, Reverse, GatedTrig);
    input WriteCmd, CmdSelect_a, CmdSelect_b;
    input Trig_edge, Gate_level, trig_4, Gate_trig;
    input Reversal; 
    output TriggerEdge, GateLevel, Reverse, GatedTrig;
    reg TriggerEdge, GateLevel, Reverse, GatedTrig;

    wire active;

    always@(posedge active)
    begin
	begin
	TriggerEdge <= Trig_edge;
	GatedTrig <= Gate_trig;
	Reverse <= Reversal;
	GateLevel <= Gate_level;
	end
    end
    assign active = WriteCmd & CmdSelect_a & CmdSelect_b;
endmodule

module cmdreg4x5b(WriteCmd, d, extrn, sys, /*fun*/trig, wctrl);
    input	WriteCmd;
    input	[7:0] d;
    output	[4:0] extrn;
    output	[4:0] sys;
    output	[4:0] trig; //fun;
    output	[4:0] wctrl;

    wire	[4:0] o_extrn;
    wire	[4:0] o_sys;
    wire	[4:0] o_fun;
    wire	[4:0] o_wctrl;

    cmdreg1b b2v_inst(.WriteCmd(WriteCmd), 
        .CmdSelect_a(d[0]), .CmdSelect_b(d[1]), 
        .Write_Lock(d[2]), 
        .bank_select(d[3]),
        .PulseWidth_A(d[4]), 
        .PulseWidth_B(d[5]), 
        .Rtsractive(d[6]), 
        .R10xactive(d[7]),
        .BankSelect(o_wctrl[0]),
        .PulseWidthA(o_wctrl[3]), 
        .PulseWidthB(o_wctrl[4]),
        .Rtsrdrive(o_extrn[2]), 
        .R10xdrive(o_extrn[3]), 
        .WriteLock(o_extrn[4]) 
    );

    cmdreg2b b2v_inst1(.WriteCmd(WriteCmd), 
        .CmdSelect_a(d[0]), .CmdSelect_b(d[1]), 
        .Reset_SR(d[2]),
        .Read_SR(d[3]), 
        .Warm_up(d[4]), 
        .Write_Temp(d[5]), 
        .ResetSR(o_sys[0]), 
        .ReadSR(o_sys[1]), 
        .WarmUp(o_wctrl[1]), 
        .WriteTemp(o_wctrl[2])
    );

    cmdreg3b b2v_inst2(.WriteCmd(WriteCmd), 
        .CmdSelect_a(d[0]), .CmdSelect_b(d[1]), 
        .LEDred(d[2]), .LEDgreen(d[3]), 
        .I2Cbus_ctrl(d[5]), .Select_RB(d[6]), 
        .RB_capture(d[7]), 
        .I2Cbusctrl(o_sys[2]),
//        .Reddrive(o_extrn[0]), .Greendrive(o_extrn[1]), // correct
        .Reddrive(o_extrn[1]), .Greendrive(o_extrn[0]), // backwards
        .SelectRB(o_sys[3]), 
        .RBcapture(o_sys[4]));

    cmdreg4b b2v_inst3(.WriteCmd(WriteCmd), .CmdSelect_a(d[0]),
        .CmdSelect_b(d[1]), .Trig_edge(d[3]),
        .Gate_level(d[4]), .Gate_trig(d[5]), .Reversal(d[6]),
        .trig_4(d[7]), .TriggerEdge(o_fun[0]),
        .GateLevel(o_fun[1]), 
        .GatedTrig(o_fun[2]),
        .Reverse(o_fun[3]));
    assign	extrn = o_extrn;
    assign	sys = o_sys;
    //assign	fun = o_fun;
    assign	trig = o_fun;
    assign	wctrl = o_wctrl;
endmodule
