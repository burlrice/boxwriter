module DirectMapD(WriteCmd, WriteData, SysClock, ReadData,
	Clock10Mhz, D, LED_red, LED_green, RtsrDrive, R10xDrive,
	BurnBusy, ReadBusy, Clock20Mhz, CD, PA, RBB);
	input	WriteCmd, WriteData, SysClock, ReadData, Clock10Mhz;
	input	[7:0] D;
	output	LED_red, LED_green, RtsrDrive, R10xDrive;
	output	BurnBusy, ReadBusy, Clock20Mhz;
	output	[14:1] CD;
	output	[22:1] PA;
	output	[7:0] RBB;
	
	wire	[4:0] extrn;
	wire	SYNTHESIZED_WIRE_0, SYNTHESIZED_WIRE_1;
	wire	[4:0] SYNTHESIZED_WIRE_6;
	wire	[6:0] SYNTHESIZED_WIRE_3;
	wire	[4:0] SYNTHESIZED_WIRE_5;

	assign	BurnBusy = SYNTHESIZED_WIRE_0;

	sequencer	b2v_inst(.StartCycle(WriteData), .sysclock(SysClock),
	.BurnBusy(SYNTHESIZED_WIRE_0), .LastAddress(SYNTHESIZED_WIRE_1),
	.extrn(extrn),.sys(SYNTHESIZED_WIRE_6), .ReadBusy(ReadBusy),
	.SRdrive(SYNTHESIZED_WIRE_3));

	clockgen2x	b2v_inst1(.ClockIn1X(Clock10Mhz), .ClockOut2X(Clock20Mhz));

	cmdreg4x5b	b2v_inst2(.WriteCmd(WriteCmd), .d(D), .extrn(extrn),
		.sys(SYNTHESIZED_WIRE_6), .wctrl(SYNTHESIZED_WIRE_5));

	datapathD	b2v_inst4(.sysclock(SysClock), .Readback(ReadData), 
		.db(D), .extrn(extrn), .SRdrive(SYNTHESIZED_WIRE_3), .sys(SYNTHESIZED_WIRE_6),
		.wctrl(SYNTHESIZED_WIRE_5), .BurnBusy(SYNTHESIZED_WIRE_0), 
		.LastAddress(SYNTHESIZED_WIRE_1), .AdrGen(PA), .cdb(CD), .rbb(RBB));

	assign	LED_red = extrn[0];
	assign	LED_green = extrn[1];
	assign	RtsrDrive = extrn[2];
	assign	R10xDrive = extrn[3];
endmodule
