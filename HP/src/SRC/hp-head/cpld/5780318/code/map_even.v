module mapecell_1(ResetSR, ClockSR, SRinput, ShiftSR, SRdataout, D, Wadr, SRoutput, odb);
    input	ResetSR, ClockSR, SRinput, ShiftSR, SRdataout;
    input	[6:0] D;
    input	[22:1] Wadr;
    output	SRoutput;
    output	[6:0] odb;

    wire	[6:0] odb_out;
    reg DFFEA_inst6, SYNTHESIZED_WIRE_7, SYNTHESIZED_WIRE_9;
    reg SYNTHESIZED_WIRE_10, SYNTHESIZED_WIRE_11, SYNTHESIZED_WIRE_12;
    reg SYNTHESIZED_WIRE_13;
    wire rst;

    assign	SRoutput = DFFEA_inst6;
    assign	rst =  ~ResetSR;
    assign	odb_out[0] = DFFEA_inst6 & SRdataout;

    dffea	b2v_inst6(.aload(Wadr[1]), .adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(SYNTHESIZED_WIRE_13),.clrn(rst),.q(DFFEA_inst6));
    dffea	b2v_inst12(.aload(Wadr[3]), .adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(SYNTHESIZED_WIRE_7),.clrn(rst),.q(SYNTHESIZED_WIRE_13)); 
    dffea	b2v_inst13(.aload(Wadr[7]), .adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(SYNTHESIZED_WIRE_9),.clrn(rst),.q(SYNTHESIZED_WIRE_7)); 
    dffea	b2v_inst14(.aload(Wadr[10]), .adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(SYNTHESIZED_WIRE_10),.clrn(rst),.q(SYNTHESIZED_WIRE_9)); 
    dffea	b2v_inst15(.aload(Wadr[13]), .adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(SYNTHESIZED_WIRE_11),.clrn(rst),.q(SYNTHESIZED_WIRE_10)); 
    dffea	b2v_inst16(.aload(Wadr[16]), .adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(SYNTHESIZED_WIRE_12),.clrn(rst),.q(SYNTHESIZED_WIRE_11)); 
    dffea	b2v_inst17(.aload(Wadr[19]), .adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(rst),.q(SYNTHESIZED_WIRE_12));

    assign	odb_out[1] = SYNTHESIZED_WIRE_13 & SRdataout;
    assign	odb_out[2] = SYNTHESIZED_WIRE_7 & SRdataout;
    assign	odb_out[3] = SYNTHESIZED_WIRE_9 & SRdataout;
    assign	odb_out[4] = SYNTHESIZED_WIRE_10 & SRdataout;

    assign	odb_out[5] = SYNTHESIZED_WIRE_11 & SRdataout;
    assign	odb_out[6] = SYNTHESIZED_WIRE_12 & SRdataout;
    assign	odb = odb_out;
endmodule

// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_10(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[4]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[7]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[10]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[14]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[17]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_11(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[5]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[8]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[11]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[14]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[17]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_12(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[5]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[8]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[11]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[14]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[18]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[21]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[2]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_13(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[5]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[9]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[12]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[15]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[18]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[21]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[2]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_14(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[6]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[9]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[12]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[15]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[18]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[22]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[3]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_15(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[6]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[9]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[13]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[16]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[19]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[3]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_16(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[4]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[7]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[10]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[13]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[16]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[19]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_17(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[4]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[7]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[10]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[13]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[17]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_18(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[4]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[8]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[11]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[14]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[17]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_19(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[5]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[8]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[11]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[14]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[17]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[21]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[2]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_2(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[4]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[7]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[10]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[13]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[16]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_20(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[5]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[8]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[12]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[15]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[18]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[21]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[2]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_21(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[6]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[9]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[12]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[15]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[18]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[21]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[3]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_22(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[6]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[9]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[12]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[16]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[19]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[22]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[3]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_3(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[4]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[7]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[11]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[14]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[17]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_4(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[5]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[8]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[11]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[14]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[17]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[2]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_5(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[5]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[8]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[11]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[15]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[18]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[21]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[2]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_6(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[6]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[9]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[12]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[15]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[18]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[21]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[2]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_7(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[6]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[9]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[12]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[15]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[19]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[22]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[3]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_8(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[6]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[10]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[13]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[16]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[19]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[3]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapecell_9(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[4]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[7]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[10]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[13]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[16]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[19]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapegroup1(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftEnable,
	SRreadout,
	D,
	Wadr,
	SRoutput,
	odb
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftEnable;
input	SRreadout;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
output	[6:0] odb;

wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;





mapecell_1	b2v_inst(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_0),.SRdataout(SRreadout),.D(D),.Wadr(Wadr),.SRoutput(SRoutput),.odb(odb));

mapecell_2	b2v_inst1(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_1),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_0));

mapecell_3	b2v_inst2(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_2),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_1));

mapecell_4	b2v_inst3(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_3),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_2));

mapecell_5	b2v_inst4(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_4),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_3));

mapecell_6	b2v_inst5(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SRinput),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_4));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapegroup2(
	ClockSR,
	ResetSR,
	SRinput,
	ShiftEnable,
	D,
	Wadr,
	SRoutput
);

input	ClockSR;
input	ResetSR;
input	SRinput;
input	ShiftEnable;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;

wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;





mapecell_7	b2v_inst(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_0),.D(D),.Wadr(Wadr),.SRoutput(SRoutput));

mapecell_8	b2v_inst1(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_1),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_0));

mapecell_9	b2v_inst2(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_2),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_1));

mapecell_10	b2v_inst3(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_3),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_2));

mapecell_11	b2v_inst4(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_4),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_3));

mapecell_12	b2v_inst5(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SRinput),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_4));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapegroup3(
	ClockSR,
	ResetSR,
	SRinput,
	ShiftEnable,
	D,
	Wadr,
	SRoutput
);

input	ClockSR;
input	ResetSR;
input	SRinput;
input	ShiftEnable;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;

wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;





mapecell_13	b2v_inst(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_0),.D(D),.Wadr(Wadr),.SRoutput(SRoutput));

mapecell_14	b2v_inst2(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_1),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_0));

mapecell_15	b2v_inst3(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_2),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_1));

mapecell_16	b2v_inst4(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_3),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_2));

mapecell_17	b2v_inst5(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_4),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_3));

mapecell_18	b2v_inst6(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SRinput),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_4));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module mapegroup4(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftEnable,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftEnable;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;

wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;





mapecell_19	b2v_inst(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_0),.D(D),.Wadr(Wadr),.SRoutput(SRoutput));

mapecell_20	b2v_inst1(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_1),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_0));

mapecell_21	b2v_inst2(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_2),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_1));

mapecell_22	b2v_inst3(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SRinput),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_2));


endmodule
// Copyright (C) 1991-2007 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

module remapSReven(
	BankSelect,
	ClockSR,
	ShiftEnable,
	ResetSR,
	Readout,
	D,
	Wadr,
	SRoutput,
	odb
);

input	BankSelect;
input	ClockSR;
input	ShiftEnable;
input	ResetSR;
input	Readout;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
output	[6:0] odb;

wire	SYNTHESIZED_WIRE_17;
wire	SYNTHESIZED_WIRE_18;
wire	SYNTHESIZED_WIRE_19;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;
wire	SYNTHESIZED_WIRE_8;
wire	SYNTHESIZED_WIRE_12;
wire	SYNTHESIZED_WIRE_16;

assign	SYNTHESIZED_WIRE_16 = 0;

mapegroup1	b2v_inst(.ClockSR(SYNTHESIZED_WIRE_17),
.ShiftEnable(SYNTHESIZED_WIRE_18),.ResetSR(SYNTHESIZED_WIRE_19),.SRinput(SYNTHESIZED_WIRE_3),.SRreadout(SYNTHESIZED_WIRE_4),.D(D),.Wadr(Wadr),.SRoutput(SRoutput),.odb(odb));

mapegroup2	b2v_inst1(.ClockSR(SYNTHESIZED_WIRE_17),
.ShiftEnable(SYNTHESIZED_WIRE_18),.ResetSR(SYNTHESIZED_WIRE_19),.SRinput(SYNTHESIZED_WIRE_8),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_3));

mapegroup3	b2v_inst2(.ClockSR(SYNTHESIZED_WIRE_17),
.ShiftEnable(SYNTHESIZED_WIRE_18),.ResetSR(SYNTHESIZED_WIRE_19),.SRinput(SYNTHESIZED_WIRE_12),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_8));

mapegroup4	b2v_inst3(.ClockSR(SYNTHESIZED_WIRE_17),
.ShiftEnable(SYNTHESIZED_WIRE_18),.ResetSR(SYNTHESIZED_WIRE_19),.SRinput(SYNTHESIZED_WIRE_16),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_12));
assign	SYNTHESIZED_WIRE_17 = BankSelect & ClockSR;
assign	SYNTHESIZED_WIRE_18 = BankSelect & ShiftEnable;
assign	SYNTHESIZED_WIRE_19 = BankSelect & ResetSR;
assign	SYNTHESIZED_WIRE_4 = BankSelect & Readout;


endmodule
