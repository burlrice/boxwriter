module mapocell_1(ResetSR, ClockSR, SRinput, ShiftSR, SRdataout, D, Wadr,
	SRoutput, odb); 
    input ResetSR, ClockSR, SRinput, ShiftSR, SRdataout;
    input [6:0] D;
    input [22:1] Wadr;
    output SRoutput;
    output [6:0] odb;

    wire [6:0] odb_out;
    wire rst;
    reg [6:0] sr;
    //wire [6:0] sr;

dffea	b2v_inst6(.aload(Wadr[1]), .adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(sr[1]),.clrn(rst),.q(sr[0]));
dffea	b2v_inst12(.aload(Wadr[4]), .adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(sr[2]),.clrn(rst),.q(sr[1]));
dffea	b2v_inst13(.aload(Wadr[7]), .adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(sr[3]),.clrn(rst),.q(sr[2]));
dffea	b2v_inst14(.aload(Wadr[10]), .adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(sr[4]),.clrn(rst),.q(sr[3]));
dffea	b2v_inst15(.aload(Wadr[13]), .adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(sr[5]),.clrn(rst),.q(sr[4]));
dffea	b2v_inst16(.aload(Wadr[16]), .adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(sr[6]),.clrn(rst),.q(sr[5]));
dffea	b2v_inst17(.aload(Wadr[19]), .adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(rst),.q(sr[6]));
/*    wire load;
    wire [6:0] dload;
    assign dload[0] = Wadr[1]; // & D[0];
    assign dload[1] = Wadr[4]; // & D[1];
    assign dload[2] = Wadr[7]; // & D[2];
    assign dload[3] = Wadr[10]; // & D[3];
    assign dload[4] = Wadr[13]; // & D[4];
    assign dload[5] = Wadr[16]; // & D[5];
    assign dload[6] = Wadr[19]; // & D[6];

    assign load = dload[0] | dload[1] | dload[2] | dload[3] | dload[4] | 
        dload[5] | dload[6];

    always@(posedge ClockSR or posedge ShiftSR or posedge load or negedge rst) 
    begin
        if (0 == rst) 
            sr[6:0] <= 0;
        else if (ShiftSR) begin
            sr[5:0] <= sr[6:1];
            sr[6] <= SRinput;
        //end else begin
        end else if (load) begin
            if (dload[0]) sr[0] <= D[0];
            else if (dload[1]) sr[1] <= D[1];
            else if (dload[2]) sr[2] <= D[2];
            else if (dload[3]) sr[3] <= D[3];
            else if (dload[4]) sr[4] <= D[4];
            else if (dload[5]) sr[5] <= D[5];
            else if (dload[6]) sr[6] <= D[6];
        end
    end*/

    assign rst =  ~ResetSR;
    assign SRoutput = sr[0];
    assign odb_out[0] = sr[0] & SRdataout;
    assign odb_out[1] = sr[1] & SRdataout;
    assign odb_out[2] = sr[2] & SRdataout;
    assign odb_out[3] = sr[3] & SRdataout;
    assign odb_out[4] = sr[4] & SRdataout;
    assign odb_out[5] = sr[5] & SRdataout;
    assign odb_out[6] = sr[6] & SRdataout;
    assign odb = odb_out;
endmodule

module mapocell_2( ResetSR, ClockSR, SRinput, ShiftSR, D, Wadr, SRoutput); 
input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;

assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));

dffea	b2v_inst12(.aload(Wadr[4]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[7]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[10]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[14]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[17]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));
endmodule

module mapocell_10(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[4]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[8]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[11]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[14]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[17]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_11(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[5]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[8]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[11]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[14]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[17]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[21]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[2]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_12(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[5]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[8]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[12]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[15]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[18]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[21]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[2]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_13(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[6]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[9]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[12]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[15]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[18]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[21]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[3]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_14(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[6]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[9]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[12]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[16]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[19]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[22]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[3]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_15(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[3]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[7]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[10]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[13]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[16]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[19]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_16(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[4]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[7]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[10]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[13]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[16]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_17(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[4]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[7]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[11]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[14]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[17]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_18(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[5]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[8]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[11]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[14]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[17]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[2]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_19(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[5]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[8]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[11]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[15]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[18]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[21]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[2]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_20(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[6]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[9]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[12]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[15]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[18]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[21]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[2]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_21(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[6]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[9]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[12]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[15]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[19]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[22]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[3]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_22(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[6]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[10]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[13]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[16]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[19]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[3]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_3(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[5]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[8]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[11]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[14]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[17]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_4(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[5]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[8]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[11]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[14]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[18]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[21]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[2]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_5(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[5]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[9]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[12]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[15]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[18]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[21]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[2]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_6(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[6]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[9]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[12]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[15]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[18]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[22]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[3]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_7(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[6]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[9]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[13]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[16]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[19]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[3]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_8(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[4]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[7]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[10]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[13]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[16]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[19]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapocell_9(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftSR,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftSR;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
reg	SRoutput;

reg	DFFEA_inst13;
wire	SYNTHESIZED_WIRE_7;
reg	DFFEA_inst14;
reg	DFFEA_inst15;
reg	DFFEA_inst16;
reg	DFFEA_inst17;
reg	DFFEA_inst12;




assign	SYNTHESIZED_WIRE_7 =  ~ResetSR;

dffea	b2v_inst12(.aload(Wadr[4]),
.adata(D[3]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst13),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst12));

dffea	b2v_inst13(.aload(Wadr[7]),
.adata(D[4]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst14),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst13));

dffea	b2v_inst14(.aload(Wadr[10]),
.adata(D[5]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst15),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst14));

dffea	b2v_inst15(.aload(Wadr[13]),
.adata(D[6]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst16),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst15));

dffea	b2v_inst16(.aload(Wadr[17]),
.adata(D[0]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst17),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst16));

dffea	b2v_inst17(.aload(Wadr[20]),
.adata(D[1]),.ena(ShiftSR),.clk(ClockSR),.d(SRinput),.clrn(SYNTHESIZED_WIRE_7),.q(DFFEA_inst17));

dffea	b2v_inst6(.aload(Wadr[1]),
.adata(D[2]),.ena(ShiftSR),.clk(ClockSR),.d(DFFEA_inst12),.clrn(SYNTHESIZED_WIRE_7),.q(SRoutput));


endmodule

module mapogroup1(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftEnable,
	SRreadout,
	D,
	Wadr,
	SRoutput,
	odb
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftEnable;
input	SRreadout;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
output	[6:0] odb;

wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;





mapocell_1	b2v_inst(.ClockSR(ClockSR), .ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_0),.SRdataout(SRreadout),.D(D),.Wadr(Wadr),.SRoutput(SRoutput),.odb(odb));

mapocell_2	b2v_inst1(.ClockSR(ClockSR), .ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_1),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_0));

mapocell_3	b2v_inst2(.ClockSR(ClockSR), .ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_2),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_1));

mapocell_4	b2v_inst3(.ClockSR(ClockSR), .ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_3),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_2));

mapocell_5	b2v_inst4(.ClockSR(ClockSR), .ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_4),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_3));

mapocell_6	b2v_inst5(.ClockSR(ClockSR), .ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SRinput),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_4));
endmodule

module mapogroup2(
	ClockSR,
	ResetSR,
	SRinput,
	ShiftEnable,
	D,
	Wadr,
	SRoutput
);

input	ClockSR;
input	ResetSR;
input	SRinput;
input	ShiftEnable;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;

wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;





mapocell_7	b2v_inst(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_0),.D(D),.Wadr(Wadr),.SRoutput(SRoutput));

mapocell_8	b2v_inst1(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_1),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_0));

mapocell_9	b2v_inst2(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_2),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_1));

mapocell_10	b2v_inst3(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_3),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_2));

mapocell_11	b2v_inst4(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_4),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_3));

mapocell_12	b2v_inst5(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SRinput),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_4));


endmodule

module mapogroup3(
	ClockSR,
	ResetSR,
	SRinput,
	ShiftEnable,
	D,
	Wadr,
	SRoutput
);

input	ClockSR;
input	ResetSR;
input	SRinput;
input	ShiftEnable;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;

wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;





mapocell_13	b2v_inst(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_0),.D(D),.Wadr(Wadr),.SRoutput(SRoutput));

mapocell_14	b2v_inst1(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_1),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_0));

mapocell_15	b2v_inst2(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_2),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_1));

mapocell_16	b2v_inst3(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_3),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_2));

mapocell_17	b2v_inst4(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_4),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_3));

mapocell_18	b2v_inst5(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SRinput),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_4));


endmodule

module mapogroup4(
	ResetSR,
	ClockSR,
	SRinput,
	ShiftEnable,
	D,
	Wadr,
	SRoutput
);

input	ResetSR;
input	ClockSR;
input	SRinput;
input	ShiftEnable;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;

wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;





mapocell_19	b2v_inst(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_0),.D(D),.Wadr(Wadr),.SRoutput(SRoutput));

mapocell_20	b2v_inst1(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_1),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_0));

mapocell_21	b2v_inst2(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SYNTHESIZED_WIRE_2),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_1));

mapocell_22	b2v_inst3(.ClockSR(ClockSR),
.ShiftSR(ShiftEnable),.ResetSR(ResetSR),.SRinput(SRinput),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_2));


endmodule

module remapSRodd(
	ResetSR,
	ClockSR,
	ShiftEnable,
	Readout,
	BankSelect,
	D,
	Wadr,
	SRoutput,
	odb
);

input	ResetSR;
input	ClockSR;
input	ShiftEnable;
input	Readout;
input	BankSelect;
input	[6:0] D;
input	[22:1] Wadr;
output	SRoutput;
output	[6:0] odb;

wire	SYNTHESIZED_WIRE_21;
wire	SYNTHESIZED_WIRE_22;
wire	SYNTHESIZED_WIRE_23;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_4;
wire	SYNTHESIZED_WIRE_8;
wire	SYNTHESIZED_WIRE_12;
wire	SYNTHESIZED_WIRE_16;
wire	SYNTHESIZED_WIRE_24;

assign	SYNTHESIZED_WIRE_12 = 0;

mapogroup1	b2v_inst(.ClockSR(SYNTHESIZED_WIRE_21),
.ShiftEnable(SYNTHESIZED_WIRE_22),.ResetSR(SYNTHESIZED_WIRE_23),.SRinput(SYNTHESIZED_WIRE_3),.SRreadout(SYNTHESIZED_WIRE_4),.D(D),.Wadr(Wadr),.SRoutput(SRoutput),.odb(odb));

mapogroup2	b2v_inst1(.ClockSR(SYNTHESIZED_WIRE_21),
.ShiftEnable(SYNTHESIZED_WIRE_22),.ResetSR(SYNTHESIZED_WIRE_23),.SRinput(SYNTHESIZED_WIRE_8),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_3));

mapogroup4	b2v_inst10(.ClockSR(SYNTHESIZED_WIRE_21),
.ShiftEnable(SYNTHESIZED_WIRE_22),.ResetSR(SYNTHESIZED_WIRE_23),.SRinput(SYNTHESIZED_WIRE_12),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_16));

mapogroup3	b2v_inst3(.ClockSR(SYNTHESIZED_WIRE_21),
.ShiftEnable(SYNTHESIZED_WIRE_22),.ResetSR(SYNTHESIZED_WIRE_23),.SRinput(SYNTHESIZED_WIRE_16),.D(D),.Wadr(Wadr),.SRoutput(SYNTHESIZED_WIRE_8));
assign	SYNTHESIZED_WIRE_21 = SYNTHESIZED_WIRE_24 & ClockSR;
assign	SYNTHESIZED_WIRE_22 = SYNTHESIZED_WIRE_24 & ShiftEnable;
assign	SYNTHESIZED_WIRE_23 = SYNTHESIZED_WIRE_24 & ResetSR;
assign	SYNTHESIZED_WIRE_4 = SYNTHESIZED_WIRE_24 & Readout;
assign	SYNTHESIZED_WIRE_24 =  ~BankSelect;


endmodule
