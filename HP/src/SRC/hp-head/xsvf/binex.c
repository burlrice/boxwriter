#include <stdio.h>
#include <string.h>

int main(int argc, char * argv[]) {
    FILE * in, * out;
    unsigned char buf[16];
    int sze, i;
    if (argc < 3) {
        printf("Usage: %s infile outfile\n", argv[0]);
        return -1;
    }
    if ((in = fopen(argv[1], "rb"))) {
        if ((out = fopen(argv[2], "w"))) {
            while (!feof(in)) {
                sze = fread(buf, 1, sizeof(buf), in);
                if (sze < sizeof(buf))
                    memset(buf+sze-1, 0, sizeof(buf) - sze);
                for (i=0; i < sizeof(buf); i++) {
                    fprintf(out, "%02x", buf[i]);
                }
                fprintf(out, "\n");
            }
            fclose(out);
        }
        fclose(in);
    }
    return 0;
}
