#include "xsvf.h"
#include "host.h"
#include "at45db.h"
#define putchar PutChar 
extern char getchar(void);

#ifndef WIN32
#define BUFFERSIZE (128)
static unsigned char ch, cl;
void crc16(void){
    unsigned char byte;
    unsigned char j;
    unsigned char i;
    unsigned int t;
    unsigned int crc;

    crc = 0;
    for(j = BUFFERSIZE; j > 0; j--) {
        //CRC1021 checksum
        spi_read_byte(byte);
        crc = (crc ^ (((unsigned int) byte) << 8));
        for(i = 8; i > 0; i--) {
            t = crc << 1;
            if(crc & 0x8000)
                t = t ^ 0x1021;
            crc = t;
        }
    }
    ch = crc / 256;
    cl = crc % 256;
}
#endif


    //Xmoden control command
    #define XMODEM_SOH         0x01
    #define XMODEM_EOT         0x04
    #define XMODEM_ACK         0x06
    #define XMODEM_NAK         0x15
    #define XMODEM_CAN         0x18
    #define XMODEM_RWC         'C'
    #define TimeOutCntC        5

void DnLoadFile(void) {
#ifndef WIN32
#if 1 //def MMC
    unsigned long FlashAddr = 0;
    unsigned short cnt;
    unsigned char packNO = 0;
    unsigned char crch, crcl;
    unsigned char li;
    unsigned char x;
    unsigned short j;
    INTCONbits.GIEH = 0; //disable interrupts
    //every interval send a "C",waiting XMODEM control command <soh>
    cnt = TimeOutCntC;

    FlashAddr = 512; // Allow space for the file table

    spi_open();
    spi_write_byte(WRITE_BUFFER1); spi_write_byte(0); spi_write_byte(0); spi_write_byte(0);
    while(cnt) {
        unsigned short cntr;
        cntr = 0xffff;
        putchar(XMODEM_RWC) ;    //send "C"
        while (cntr-- && !kbhit()) {}
        if(kbhit()) {
            if(getchar() == XMODEM_SOH) break; //XMODEM command <soh>
        }
        cnt--;
    }
    if (0 == cnt) return;//break;
    //begin to receive data
    cnt = 0;
    do {
        packNO++;
        while(!kbhit());
        ch = getchar(); //get package number
        while(!kbhit());
        cl = ~getchar();
        if ((packNO == ch) && (packNO == cl)) {
            for(li = 0; li < BUFFERSIZE; li++) {     //receive a full data frame
                while(!kbhit());
                spi_write_byte(getchar());
            }
            while(!kbhit());
            crch = getchar(); //get checksum
            while(!kbhit());
            crcl = getchar();
//#define VERIFY
#ifdef VERIFY
            crc16(); //calculate checksum
            if((crch == ch) && (crcl == cl)) {
#endif
                FlashAddr += BUFFERSIZE; //modify Flash page address
                if (0 == (FlashAddr&0x000001FF)) {
                    FlashAddr -= 512; //modify Flash page address
                    spi_close(); //end write to buffer1
                    spi_open();
                    spi_write_byte(PROGRAM_BUFFER1); spi_write_addr(FlashAddr);
                    spi_close();
                    wait_while_busy();
                    spi_open();
                    spi_write_byte(WRITE_BUFFER1); spi_write_byte(0); spi_write_byte(0); spi_write_byte(0);
                    FlashAddr += 512; //modify Flash page address
                }
                putchar(XMODEM_ACK);
                cnt = 0;
#ifdef VERIFY
            } else { //CRC
                putchar(XMODEM_NAK); //ask resend
                cnt++;
            }
#endif
        } else { //PackNo
            putchar(XMODEM_NAK); //ask resend
            cnt++;
        }
        if(cnt > 100) {
            break; //too many errors, abort
        }
        while(!kbhit());
    } while(getchar() != XMODEM_EOT);
    putchar(XMODEM_ACK);
    do {
        spi_write_byte(0x1a);
        FlashAddr++;
    } while (0 != (FlashAddr&0x000001FF));
    FlashAddr -= 512; //modify Flash page address
    spi_close(); //end write to buffer1

    spi_open();
    spi_write_byte(PROGRAM_BUFFER1); spi_write_addr(FlashAddr);
    spi_close();
    wait_while_busy();

#endif
#endif // WIN32
}
