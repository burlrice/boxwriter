#ifndef __TYPES_H
#define __TYPES_H
#include "processor.h"
#ifndef WIN32
typedef enum _BOOL { FALSE = 0, TRUE } BOOL;		// Boolean
typedef char CHAR;
typedef int INT;
typedef  ROM unsigned char RUCHAR;
typedef unsigned int UINT;
typedef unsigned long ULONG;
#else
typedef unsigned char RUCHAR;
#endif
typedef unsigned char UCHAR;
#endif
