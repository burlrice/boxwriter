#ifndef __PROCESSOR
#define __PROCESSOR
#ifdef PIC
#define PutChar(txData) { \
    while (!PIR1bits.TXIF) ;  \
    TXREG1 = txData; Nop(); \
}
#ifdef SDCC
#include	<pic18fregs.H>
#define ROM
#define PutChar(txData) { \
    while (!PIR1bits.TXIF) ;  \
    TXREG1 = txData; \
}
#else
#include	<P18cxxx.H>
#define ROM rom
#endif
#else
#include <io.h>
#define Nop() asm volatile ("nop" ::)
#define ROM
#endif
#endif /* __PROCESSOR */
