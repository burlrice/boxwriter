#ifndef _HOST_H_
#define _HOST_H_

/*
 * Copyright (C) 2004 by egnite Software GmbH. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the names of
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY EGNITE SOFTWARE GMBH AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL EGNITE
 * SOFTWARE GMBH OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * For additional information see http://www.ethernut.de/
 */

/*
 * $Log: host.h,v $
 * Revision 1.3  2008/02/01 14:45:32  cnauman
 * no message
 *
 * Revision 1.2  2007/12/18 22:56:01  cnauman
 * no message
 *
 */

/*!
 * \file host.h
 * \brief Platform header file.
 */

/*!
 * \addtogroup xgXEDefs
 */
/*@{*/

#ifdef ETHERNUT2

/*! \brief The XSVF file is expected in the UROM file system. */
#define XSVFNAME    "UROM:enut202.xsvf"

#else

#define XSVFNAME    "cpld/enut202.xsvf"

#endif

/*@}*/

/*!
 * \addtogroup xgHost
 */
/*@{*/


/*! \brief Executor version string. */
#define XSVFEXEC_VERSION    "1.0.1"

/* Uncomment to enable debug output */
//#define XSVF_DEBUG

#ifdef ETHERNUT2

#include <sys/types.h>

/*! \brief Set TMS high on Ethernut 2. */
#define SET_TMS()   sbi(PORTF, 5)

/*! \brief Set TMS low on Ethernut 2. */
#define CLR_TMS()   cbi(PORTF, 5)

/*! \brief Set TDI high on Ethernut 2. */
#define SET_TDI()   sbi(PORTF, 6)

/*! \brief Set TDI low on Ethernut 2. */
#define CLR_TDI()   cbi(PORTF, 6)

/*! \brief Set TCK high on Ethernut 2. */
#define SET_TCK()   sbi(PORTF, 4)

/*! \brief Set TCK low on Ethernut 2. */
#define CLR_TCK()   cbi(PORTF, 4)

/*! \brief Get TDO status on Ethernut 2. */
#define GET_TDO()   bit_is_set(PINF, 7)

#elif defined (PIC)
#define PORT (PORTBbits)
#define SET_TMS() PORT.RB4 = 1
#define CLR_TMS() PORT.RB4 = 0
#define SET_TDI() PORT.RB5 = 1
#define CLR_TDI() PORT.RB5 = 0
#define SET_TCK() PORT.RB2 = 1
#define CLR_TCK() PORT.RB2 = 0

#define GET_TDO()   (PORT.RB3)
#else

/* Generic routines, do nothing. */
#define SET_TMS()
#define CLR_TMS()
#define SET_TDI()
#define CLR_TDI()
#define SET_TCK()
#define CLR_TCK()

#define GET_TDO()   (0)

#endif

/*! \brief Set TMS high and toggle TCK. */
#define SET_TMS_TCK()   { SET_TMS(); CLR_TCK(); SET_TCK(); }

/*! \brief Set TMS low and toggle TCK. */
#define CLR_TMS_TCK()   { CLR_TMS(); CLR_TCK(); SET_TCK(); }

extern int XsvfInit(void);
extern void XsvfExit(int rc);

extern int XsvfGetError(void);
extern u_char XsvfGetCmd(void);
extern u_char XsvfGetState(u_char state0, u_char state1);
extern u_char XsvfGetByte(void);
extern short XsvfGetShort(void);
extern long XsvfGetLong(void);
extern int XsvfReadBitString(void *buf, int num);
extern int XsvfSkipComment(void);

extern void XsvfDelay(long usecs);

/*@}*/
#include "processor.h"
#ifdef PIC
#define kbhit() PIR1bits.RCIF
//#include <p18cxxx.h>
char getchar(void);
#define TOGGLE_WRITE() { PORTCbits.RC0 = 1; PORTCbits.RC0 = 0; }
#define databus     PORTD
#define DATA(a) { databus = a; }
#endif
#endif
