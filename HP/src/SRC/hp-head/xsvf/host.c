/*
 * Copyright (C) 2004 by egnite Software GmbH. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holders nor the names of
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY EGNITE SOFTWARE GMBH AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL EGNITE
 * SOFTWARE GMBH OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * For additional information see http://www.ethernut.de/
 */

/*
 * $Log: host.c,v $
 * Revision 1.5  2008/05/07 11:55:34  cnauman
 * no message
 *
 * Revision 1.4  2008/02/01 14:45:32  cnauman
 * no message
 *
 * Revision 1.3  2007/12/19 19:51:50  cnauman
 * no message
 *
 * Revision 1.2  2007/12/18 22:56:01  cnauman
 * no message
 *
 */

/*#include <stdio.h>
#include <io.h>
#include <fcntl.h>*/

#ifdef ETHERNUT2

#include <sys/atom.h>
#include <sys/timer.h>
#include <sys/version.h>

#include <dev/debug.h>
#include <dev/urom.h>

#elif defined(_MSC_VER)

#include <winsock2.h>

#elif defined(PIC)
#include <stdio.h>
#define _open(a,b) 1
#define _close(a) spi_close()
#define ntohs(w) (((w<<8)&0xff00) | ((w>>8)&0x00ff))
#define ntohl(lw) (((lw<<24)&0xff000000L) | ((lw<<8 )&0x00ff0000L) | \
           ((lw>>8 )&0x0000ff00L) | ((lw>>24)&0x000000ffL))
//#define XSVF_DEBUG
#include "at45db.h"
#endif

#include "xsvf.h"
#include "tapsm.h"
#include "host.h"

#ifdef PIC
//#include "xmodem.h"

void init() {
    PORTA = 0x3f;
    TRISA = 0x3f;

    ADCON1 = 0x0f; // for trigger and prtgate
    CMCON = 0x07; // config

//    PORTB = 0xc3;
//    PORTB = 0xc2;
    PORTB = 0xd2;
//    TRISB = 0xc3;
    TRISB = 0xc2;

    PORTC = 0xd2;
//    TRISC = 0xc2;
    TRISC = 0xd2;//0xc2;

    PORTD = 0;
    TRISD = 0;

    PORTE = 0x04;
    TRISE = 0;

    at45db_init();
//    TOGGLE_WRITE();
}
//#define BUFFERSIZE 128
#define BUFFERSIZE 16
unsigned char buffer[BUFFERSIZE], packNO, Ndx;
char getchar(void) {
    char c;
    while (!kbhit()) ;
    c = RCREG1;
    return c;
}

unsigned char getnibble(void) {
    unsigned char c;
    while (1) {
        c = getchar();
        if (27 == c) _asm reset _endasm;
//        putc(c);
        if ('0' <= c && c <= 'f') break;
    }
    if ('A' <= c && c <= 'F') c += 0x20; // convert to lower case

    if ('a' <= c && c <= 'f') {
        c -= 'a';
        c += 10;
    } else {
        c -= '0';
    }
    return c;
}

//char vals[] = "0123456789abcdef";
unsigned char getbyte(void) {
    unsigned char c;
    c = getnibble();
    c <<= 4;
    c += getnibble();
    //putc('a' + c);
    return c;
}

unsigned char getHex(void) {
    unsigned char c, n;
/*    if (0 == Ndx) {
        //packNO = DnLoad(packNO, buffer);
        PutChar('>');
        c = 16; //getbyte();
        //printf("%d\r\n", c);
        for (n=0; n < c; n++) {
            buffer[n] = getbyte();
        }
    }
    c = buffer[Ndx++]; //0; //data[ndx++];
    if ((BUFFERSIZE-1) < Ndx) Ndx = 0;*/
    spi_read_byte(c);
    return c;
}

int _read(int x, void * c, int sze) {
    unsigned char * pc;
    int len;
    pc = c;
    len = sze;
    while (len--) {
        *pc++ = getHex();
    }
    return sze;
}
#endif
/*!
 * \file host.c
 * \brief Platform dependant routines..
 */

/*!
 * \addtogroup xgHost
 */
/*@{*/


#ifdef XSVF_DEBUG

/*!
 * \brief XSVF command names.
 *
 * Used for debugging output.
 */
rom static char *cmd_names[] = {
    "XCOMPLETE",
    "XTDOMASK",
    "XSIR",
    "XSDR",
    "XRUNTEST",
    "UNKNOWN",
    "UNKNOWN",
    "XREPEAT",
    "XSDRSIZE",
    "XSDRTDO",
    "XSETSDRMASKS",
    "XSDRINC",
    "XSDRB",
    "XSDRC",
    "XSDRE",
    "XSDRTDOB",
    "XSDRTDOC",
    "XSDRTDOE",
    "XSTATE",
    "XENDIR",
    "XENDDR",
    "XSIR2",
    "XCOMMENT",
    "XWAIT",
    "UNKNOWN"
};

/*!
 * \brief TAP state names.
 *
 * Used for debugging output.
 */
rom static char *tap_names[] = {
    "Test-Logic-Reset",
    "Run-Test-Idle",
    "Select-DR-Scan",
    "Capture-DR",
    "Shift-DR",
    "Exit1-DR",
    "Pause-DR",
    "Exit2-DR",
    "Update-DR",
    "Select-IR-Scan",
    "Capute-IR",
    "Shift-IR",
    "Exit1-IR",
    "Pause-IR",
    "Exit2-IR",
    "Update-IR",
    "Unknown"
};

#endif

/*!
 * \brief Handle of XSVF file.
 */
static int fh;

/*!
 * \brief Last error occured in this module.
 */
static int xsvf_err;

/*!
 * \brief Initialize the platform dependant interface.
 *
 * All required hardware initializations should be done in this
 * routine. We may also initiate debug output. If the XSVF date
 * is located in a file system, then the file will be opened
 * here as well.
 *
 * \return Zero on success, otherwise an error code is returned.
 */
int XsvfInit(void)
{
#ifdef ETHERNUT2
    u_long baud = 115200;

    /*
     * Prepare standard output and display a banner.
     */
    NutRegisterDevice(&devDebug0, 0, 0);
    freopen("uart0", "w", stdout);
    _ioctl(_fileno(stdout), UART_SETSPEED, &baud);
    printf("\n100 XSVF-Executor " XSVFEXEC_VERSION " on Nut/OS %s", NutVersionString());

    /*
     * Register our device for the file system.
     */
    NutRegisterDevice(&devUrom, 0, 0);

    /*
     * Disable JTAG. Ethernut 2 is using
     *
     * PF4 for target TCK.
     * PF5 for target TMS.
     * PF6 for target TDI.
     * PF7 for target TDO.
     */
    NutEnterCritical();
    outb(MCUCSR, _BV(JTD));
    outb(MCUCSR, _BV(JTD));
    NutExitCritical();
    outb(DDRF, 0x70);

#elif defined(_WIN32)

    printf("\n100 XSVF-Executor " XSVFEXEC_VERSION " on WIN32");
#elif defined(PIC)
    printf("\r\n100 XSVF-Executor " XSVFEXEC_VERSION " on PIC");
    xsvf_err = 0;
    packNO = 0;
    Ndx = 0;
    spi_open();
    spi_write_byte(CONTINUOUS_READ); 
    spi_write_addr(512);
    //spi_write_byte(0); spi_write_byte(0); spi_write_byte(512);
#endif

    /* The XSVF file is expected in the UROM file system. */
    fh = _open(XSVFNAME, _O_BINARY | _O_RDONLY);

    return (fh == -1) ? XE_DATAUNDERFLOW : 0;
}

/*!
 * \brief Shutdown the platform dependant interface.
 *
 * On most embedded platforms this routine will never return.
 *
 * \param rc Programming result code.
 */
void XsvfExit(int rc)
{
    /* Close XSVF file. */
    if(fh != -1) {
        _close(fh);
    }

    /* Display programming result. */
    if(rc) {
        printf("\r\n4%02d ERROR\r\n", rc);
    }
    else {
        printf("\r\n199 OK");
    }

#ifdef ETHERNUT2
    /* Nut/OS applications never return. */
    for (;;);
#endif
}

/*!
 * \brief Retrieve the last error occured in this module.
 *
 * \return Error code or 0 if no error occured.
 */
int XsvfGetError(void)
{
    return xsvf_err;
}

/*!
 * \brief Get next byte from XSVF buffer.
 *
 * Call XsvfGetError() to check for errors,
 *
 * \return Byte value.
 */
u_char XsvfGetByte(void)
{
    u_char rc;

    if(_read(fh, &rc, sizeof(rc)) != sizeof(rc)) {
        xsvf_err = XE_DATAUNDERFLOW;
    }
#ifdef XSVF_DEBUG
    printf("[%x]", rc);
#endif

    return rc;
}

/*!
 * \brief Get next command byte from XSVF buffer.
 *
 * \return XSVF command or XUNKNOWN if an error occured.
 */
u_char XsvfGetCmd(void)
{
    u_char rc;

    if(_read(fh, &rc, sizeof(rc)) != sizeof(rc) || rc >= XUNKNOWN) {
        rc = XUNKNOWN;
    }

#ifdef XSVF_DEBUG
    printf("\r\n2%02d ", rc); printf("%s", cmd_names[rc]);
#elif BUFFERSIZE < 128
    PutChar('.');
#endif

    return rc;
}

/*!
 * \brief Get next byte from XSVF buffer and select a TAP state.
 *
 * \param state0 Returned state, if the byte value is zero.
 * \param state1 Returned state, if the byte value is one.
 * 
 * \return TAP state or UNKNOWN_STATE if an error occured.
 */
u_char XsvfGetState(u_char state0, u_char state1)
{
    u_char rc;

    if(_read(fh, &rc, sizeof(rc)) != sizeof(rc) || rc > 1) {
        rc = UNKNOWN_STATE;
    }
    else if(rc) {
        rc = state1;
    }
    else {
        rc = state0;
    }

#ifdef XSVF_DEBUG
//    putc('{'); puts(tap_names[rc]); putc('}');
    printf("0x%02x", rc);
//    printf("{%s}", tap_names[rc]);
#endif

    return rc;
}

/*!
 * \brief Get next short value from XSVF buffer.
 *
 * Call XsvfGetError() to check for errors,
 *
 * \return Short value.
 */
short XsvfGetShort(void)
{
    short rc;

    if(_read(fh, &rc, sizeof(rc)) != sizeof(rc)) {
        xsvf_err = XE_DATAUNDERFLOW;
        return -1;
    }
    rc = ntohs(rc);

#ifdef XSVF_DEBUG
    //printf("[%d]", rc);
    printf("[%04x]", rc);
#endif

    return rc;
}

/*!
 * \brief Get next long value from XSVF buffer.
 *
 * Call XsvfGetError() to check for errors,
 *
 * \return Long value.
 */
long XsvfGetLong(void)
{
    long rc;

    if(_read(fh, &rc, sizeof(rc)) != sizeof(rc)) {
        xsvf_err = XE_DATAUNDERFLOW;
        rc = 0;
    }
    else {
        rc = ntohl(rc);
    }

#ifdef XSVF_DEBUG
    //printf("[%ld]", rc);
    printf("[%08lx]", rc);
#endif

    return rc;
}

/*!
 * \brief Read a specified number of bits from XSVF buffer.
 *
 * \param buf Pointer to the buffer which receives the bit string.
 * \param num Number of bits to read.
 *
 * \return Error code or 0 if no error occured.
 */
int XsvfReadBitString(void *buf, int num)
{
    int len; // = (num + 7) / 8;
    len = (num + 7) / 8;

    if (len > MAX_BITVEC_BYTES) {
        xsvf_err = len = XE_DATAOVERFLOW;
    }
    else if(_read(fh, buf, len) < len) {
        xsvf_err = len = XE_DATAUNDERFLOW;
    }

#ifdef XSVF_DEBUG
    else {
        u_char *cp; // = buf;
        cp = buf;

        printf("[%u:", num);
        while(len-- > 0) {
            printf("%02X", *cp);
            cp++;
        }
        printf("]");
    }
#endif

    return len;
}

/*!
 * \brief Skip comment in the XSVF buffer.
 *
 * \return Error code or 0 if no error occured.
 */
int XsvfSkipComment(void)
{
    u_char ch;

    for(;;) {
        if(_read(fh, &ch, sizeof(ch)) != sizeof(ch)) {
            return (xsvf_err = XE_DATAUNDERFLOW);
        }
        if(ch == 0) {
            break;
        }
#ifdef XSVF_DEBUG
        PutChar(ch);
#endif
    }
    return 0;
}

/*!
 * \brief Microsecond delay.
 *
 * \param usecs Number of microseconds.
 */
void XsvfDelay(long usecs)
{
#ifdef ETHERNUT2

    usecs = (usecs + 500L) / 1000L;
    if(usecs > 63L) {
        NutSleep(usecs);
    }
    else if(usecs) {
        NutDelay((u_char)usecs);
    }

#elif defined(_MSC_VER)

    Sleep((usecs + 500L) / 1000L);
#elif defined(PIC)
    //printf("sleep %ld uSecs!\r\n", usecs);
    usecs /= 2;
    while (usecs--) Nop();
#endif
}

/*@}*/
