@echo off
avrdude.exe -p m162 -c picoweb -e -U efuse:w:0xff:m -U hfuse:w:0xd8:m -U lfuse:w:0xcf:m -U flash:w:bootldr.rom:s -U signature:r:success:r -E noreset
if exist success goto success
echo "ERROR! Programming error."
goto end
:success
del success
echo "Success! Board programmed successful."
:end
