/*

                           e Y8b    Y8b YV3.18P888 88e
                          d8b Y8b    Y8b Y888P 888 888D
                         d888b Y8b    Y8b Y8P  888 88"
                        d888WuHan8b    Y8b Y   888 b,
                       d8888888b Y8b    Y8P    888 88b,
           8888 8888       ,e,                                  888
           8888 888820078e  " Y8b Y888P ,e e, 888,8, dP"Y ,"Y88b888
           8888 8888888 88b888 Y8b Y8P d88 88b888 " C88b "8" 888888
           8888 8888888 888888  Y8b "  888   ,888    Y88D,ee 888888
           'Y88 88P'888 888888   Y8P    "YeeP"888   d,dP "88 888888
   888 88b,                    d8  888                     888
   888 88P' e88 88e  e88 88e  d88  888 e88 88e  ,"Y88b e88 888 ,e e, 888,8,
   888 8K  d888 888bd888 8Shaoziyang88d888 888b"8" 888d888 888d88 88b888 "
   888 88b,Y888 888PY888 888P 888  888Y888 888P,ee 888Y888 888888   ,888
   888 88P' "88 88"  "88 88"  888  888 "88 88" "88 888 "88 888 "YeeP"888


  Project:       AVR Universal BootLoader
  File:          bootldr.c
                 main program code
  Version:       3.1

  Compiler:      GCC 4.1.2 + AVR Studio 4.13.528

  Author:        Shaoziyang
                 Shaoziyang@gmail.com
                 http://shaoziyang.googlepages.com      or
                 http://shaoziyang.bloger.com.cn (Chinese)
  Date:          2007.9

  Modify:        Add your modify log here

  See readme.txt to get more information.

*/

#include "bootcfg.h"
#include "bootldr.h"
#if 0
#include "mmc_if.h"
#endif
//user's application start address
#ifndef PIC
#define PROG_START         0x0000
#else
#define PROG_START         0x0800
#endif

//receive buffer' size will not smaller than SPM_PAGESIZE
#if BUFFERSIZE < SPM_PAGESIZE
#define BUFSIZE SPM_PAGESIZE
#else
#define BUFSIZE BUFFERSIZE
#endif

//define receive buffer
unsigned char buf[BUFSIZE];

#if BUFSIZE > 255
unsigned int bufptr, pagptr;
#else
unsigned char bufptr, pagptr;
#endif

unsigned char ch, cl;

//Flash address
#if FLASHEND > 0xFFFFUL
unsigned long int FlashAddr;
#else
unsigned int FlashAddr;
#endif


#ifndef PIC
//update one Flash page
void write_one_page(unsigned char *buf)
{
  boot_page_erase(FlashAddr);                  //erase one Flash page
  boot_spm_busy_wait();
  for(pagptr = 0; pagptr < SPM_PAGESIZE; pagptr += 2) //fill data to Flash buffer
  {
    boot_page_fill(pagptr, buf[pagptr] + (buf[pagptr + 1] << 8));
  }
  boot_page_write(FlashAddr);                  //write buffer to one Flash page
  boot_spm_busy_wait();                        //wait Flash page write finish
}


#else
/* initialize flash buffer */
void boot_page_write(unsigned long addr) {
  EECON2 = 0x55;
  EECON2 = 0xAA;
  EECON1bits.WR = 1;
  Nop();
}
void boot_page_erase(unsigned long addr) {
    if (0 == (addr & 0x3ff)) { // mask off sector boundaries
        TBLPTR = addr;
        EECON1 = 0x94;
        boot_page_write(0);
    }
}
void write_one_page(unsigned char *buf) {
    boot_page_erase(FlashAddr);         //erase one Flash page

    TBLPTRL = 0xc0; // boundary setup
    for (pagptr=0; pagptr < 0x40; pagptr++) TABLAT = 0xff;

    TBLPTR = FlashAddr;
    for(pagptr = 0; pagptr < SPM_PAGESIZE; pagptr++) {
        TABLAT = buf[pagptr];
        _asm tblwtpostinc _endasm
    }
    TBLPTR = FlashAddr;
    boot_page_write(FlashAddr);         //write buffer to one Flash page
}
unsigned char pgm_read_byte(unsigned short addr) {
    rom unsigned char * pc;
    pc = (rom unsigned char *)(addr);
    return *pc;
}
#pragma code ivec_high=0x08
void ivec_high(void) {
    _asm goto 0x808 _endasm;
//  (*((void(*)(void))(PROG_START+8)))();            //jump
}
#pragma code ivec_low=0x18
void ivec_low(void) {
    _asm goto 0x818 _endasm;
//  (*((void(*)(void))(PROG_START+0x18)))();            //jump
}

#pragma code

#endif

//jump to user's application
void quit(void) {
    boot_rww_enable();                           //enable application section
    (*((void(*)(void))PROG_START))();            //jump
}

//send data to comport
void WriteCom(unsigned char dat) {
#ifdef PIC
    putchar(dat);
#else
#if RS485
    RS485Enable();
#endif

    UDRREG(COMPORTNo) = dat;
    //wait send finish
    while(!(UCSRAREG(COMPORTNo) & (1<<TXCBIT(COMPORTNo))));
    UCSRAREG(COMPORTNo) |= (1 << TXCBIT(COMPORTNo));

#if RS485
    RS485Disable();
#endif
#endif
}

//wait receive a data from comport
unsigned char WaitCom(void) {
//    if ((RCSTA1bits.FERR || RCSTA1bits.OERR)) 
//        PORTB = 0xf | RCSTA1bits.FERR << 4 | RCSTA1bits.OERR << 5;
    while(!kbhit());
    return getchar();
}

#if VERBOSE
void putstr(ROMSTR *str) {
    while(*str)
        WriteCom(*str++);
    WriteCom(0x0D);
    WriteCom(0x0A);
}
#endif

//calculate CRC checksum
#if BUFSIZE > 255
void crc16(unsigned char *buf) {
    unsigned int j;
#else
void crc16(unsigned char *buf) {
    unsigned char j;
#endif

#if CRCMODE == 0
    unsigned char i;
    unsigned int t;
#endif
    unsigned int crc;

    crc = 0;
    for(j = BUFFERSIZE; j > 0; j--) {
#if CRCMODE == 0
        //CRC1021 checksum
        crc = (crc ^ (((unsigned int) *buf) << 8));
        for(i = 8; i > 0; i--) {
            t = crc << 1;
            if(crc & 0x8000)
                t = t ^ 0x1021;
            crc = t;
        }
#elif CRCMODE == 1
        //word add up checksum
        crc += (unsigned int)(*buf);
#else

#endif
        buf++;
    }
    ch = crc / 256;
    cl = crc % 256;
}

#ifdef PIC
#pragma config FOSC = HSPLL
#pragma config WDTEN = OFF
#pragma config XINST = OFF
#pragma config DEBUG = OFF

#pragma config FCMEN = ON
#pragma config STVREN = ON
#pragma config IESO = ON

void main(void) {
#else
int main(void) {
#endif
//  unsigned char c;
    unsigned char endless = 0;
    unsigned short cnt;
    unsigned char packNO;
    unsigned char crch, crcl;

#if InitDelay > 255
    unsigned int di;
#elif InitDelay
    unsigned char di;
#endif

#if BUFFERSIZE > 255
    unsigned int li;
#else
    unsigned char li;
#endif

#ifndef PIC
    //disable interrupt
    __asm__ __volatile__("cli": : );
#else
    TBLPTRU = 0;
#endif

#if WDGEn
    //if enable watchdog, setup timeout
    wdt_enable(WDTO_1S);
#endif

    //initialize timer1, CTC mode
    TimerInit();

#if RS485
    DDRREG(RS485PORT) |= (1 << RS485TXEn);
    RS485Disable();
#endif

#if LEDEn
    //set LED control port to output
    DDRREG(LEDPORT) = (1 << LEDPORTNo);
#endif

    //initialize comport with special config value
    ComInit();

#ifdef __18F44J10
#define			RXforceTXoff_pin	PORTCbits.RC0
    RXforceTXoff_pin = 1;
#elif __18F45J10
#endif

#if InitDelay
    //some kind of avr mcu need special delay after comport initialization
    for(di = InitDelay; di > 0; di--) {
#ifdef PIC
        Nop(); Nop(); Nop(); Nop(); Nop();
#else
        __asm__ __volatile__ ("nop": : );
#endif
    }
#endif

#if LEVELMODE == 1
    //port level launch boot
    //set port to input
    DDRREG(LEVELPORT) &= ~(1 << LEVELPIN);
#if PINLEVEL
    if(PINREG(LEVELPORT) & (1 << LEVELPIN))
#else
    if(!(PINREG(LEVELPORT) & (1 << LEVELPIN)))
#endif
    {
#if VERBOSE
        //prompt enter boot mode
        putstr(msg6);
#endif
    } else {
#if VERBOSE
        //prompt execute user application
        putstr(msg7);
#endif

        quit();
    }
#endif  //LEVELMODE == 1

#if LEVELMODE == 0
    //comport launch boot

#if VERBOSE
    //prompt waiting for password
    putstr(msg1);
#endif

    cnt = TimeOutCnt;
    cl = 0;
    while(1) {
#if WDGEn
        //clear watchdog
        wdt_reset();
#endif

#ifndef PIC
        if(TIFRREG & (1<<OCF1A)) {   //T1 overflow
            TIFRREG |= (1 << OCF1A);

            if(cl == CONNECTCNT)
                break;

#if LEDEn
            //toggle LED
            LEDAlt();
#endif

            cnt--;
            if(cnt == 0) {

#if VERBOSE
                //prompt timeout
                putstr(msg2);
#endif
                //quit bootloader
                quit();
            }
        }
#endif

        if(DataInCom()) {
            if(ReadCom() == KEY[cl])
                cl++;
            else
                cl = 0;
        }
    }

#endif  //LEVELMODE == 0

#if VERBOSE
    //prompt waiting for data
    putstr(msg3);
#endif

#define ACTIVE (TXSTA1bits.TXEN)
///////////////////////////////////////////////////////////////////
#ifdef __18F45J10
    ACTIVE = 1;
#endif
///////////////////////////////////////////////////////////////////

    //every interval send a "C",waiting XMODEM control command <soh>
    cnt = TimeOutCntC;
    while(1) {
#ifdef PIC
        unsigned short cntr;
        cntr = 0xffff;
        if (ACTIVE) WriteCom(XMODEM_RWC) ;    //send "C"
        while (cntr-- && !kbhit()) ;
        {
#else
        if(TIFRREG & (1 << OCF1A)) { //T1 overflow
            TIFRREG |= (1 << OCF1A);
            WriteCom(XMODEM_RWC) ;    //send "C"
#endif

#if LEDEn
            //toggle LED
            LEDAlt();
#endif

            cnt--;
            if(cnt == 0) {
#if VERBOSE
                //prompt timeout
                putstr(msg2);
#endif
                quit();
            }
        }

#if WDGEn
        //clear watchdog
        wdt_reset();
#endif
        if(kbhit()) {
            if(ReadCom() == XMODEM_SOH) { //XMODEM command <soh>
                break;
            } else if (ReadCom() == 'T') {
                if (ACTIVE && !endless) {
                    ACTIVE = 0;
                    for (;;) ;
                } else {
                    ACTIVE = 1;
                    endless = 1;
                    cnt = 500;
                }
            }
        }
    }
#ifndef PIC
  //close timer1
  TCCR1B = 0;
#endif
    if (!ACTIVE) quit();
  //begin to receive data
  packNO = 0;
  bufptr = 0;
  cnt = 0;
  FlashAddr = PROGSTART;
#if 0
  //sector = 0;
  //mmc_send_command(25,(sector>>7) & 0xffff, (sector<<9) & 0xffff);
  mmc_send_command(25,0,0);
  if (0 != spi_byte(0xff)) { // if no valid token
    mmc_clock_and_release(); // cleanup and	
  }
#endif
/*        di = 0;
        do {
            for (di=0; di < sizeof(buf); di++) {
                while (!kbhit()) ;
                buf[di] = getchar();
            }
            putchar(XMODEM_ACK);
            while (!kbhit()) ;
        } while (XMODEM_EOT != getchar());
        putchar(XMODEM_ACK);
    for (di=0; di < sizeof(buf); di++); putchar(buf[di]);
    quit();*/

  do
  {
    packNO++;
    ch = WaitCom();                          //get package number
    cl = ~WaitCom();
    if ((packNO == ch) && (packNO == cl))
    {
      for(li = 0; li < BUFFERSIZE; li++)      //receive a full data frame
      {
        buf[bufptr++] = WaitCom();
      }
#if 0
      while (0xff != spi_byte(0xff)); //wait until not busy
      spi_byte(0xff);
      spi_byte(0xff);
      spi_byte(0xfc);
      for (index=0; index<512/*udp_length-2*/; index+=2) {
        data=inport(BASE_ADR+NE_DATA);
        byte=data>>8;
        spi_byte(byte);
        byte=data&0x00FF;
        spi_byte(byte);
      }
      data=inport(BASE_ADR+NE_DATA);
      if (udp_length&1){
        #ifdef DO_FILEIO
        byte=data>>8;
        spi_byte(byte);
        #endif
      }else{
        #ifdef DO_FILEIO
        byte=data>>8;
        spi_byte(byte);
        byte=data&0x00FF;
        spi_byte(byte);
        #endif
      }
      spi_byte(0xff); // ignore dummy checksum
      spi_byte(0xff); // ignore dummy checksum
      spi_byte(0xff); // error
      sector++;
#endif
      crch = WaitCom();                       //get checksum
      crcl = WaitCom();
      crc16(&buf[bufptr - BUFFERSIZE]);       //calculate checksum
      if((crch == ch) && (crcl == cl))
      {
#if BootStart
        if(FlashAddr < BootStart)             //avoid write to boot section
        {
#elif ((0 < PROGSTART) && (0 < FLASHEND))
        if (FlashAddr <= FLASHEND) {
#endif

#if BUFFERSIZE < SPM_PAGESIZE
          if(bufptr >= SPM_PAGESIZE)          //Flash page full, write flash page;otherwise receive next frame
          {                                   //receive multi frames, write one page
            write_one_page(buf);              //write data to Flash
            FlashAddr += SPM_PAGESIZE;        //modify Flash page address
            bufptr = 0;
          }
#else
          while(bufptr > 0)                   //receive one frame, write multi pages
          {
            write_one_page(&buf[BUFSIZE - bufptr]);
            FlashAddr += SPM_PAGESIZE;        //modify Flash page address
            bufptr -= SPM_PAGESIZE;
          }
#endif

#if BootStart || ((0 < PROGSTART) && (0 < FLASHEND))
        }
        else                                  //ignore flash write when Flash address exceed BootStart
        {
          bufptr = 0;                         //reset receive pointer
        }
#endif

//read flash, compare with buffer's content
#if ((ChipCheck > 0) && (BootStart > 0)) || ((0 < PROGSTART) && (0 < FLASHEND))
#if BUFFERSIZE < SPM_PAGESIZE
        if((bufptr == 0) && (FlashAddr <= BootStart))
#else
#if (BootStart > 0)
        if(FlashAddr <= BootStart)
#elif ((0 < PROGSTART) && (0 < FLASHEND))
        if (FlashAddr <= FLASHEND)
#endif
#endif
        {
          boot_rww_enable();                  //enable application section
          cl = 1;                             //clear error flag
          for(pagptr = 0; pagptr < BUFSIZE; pagptr++)
          {
            if(pgm_read_byte(FlashAddr - BUFSIZE + pagptr) != buf[pagptr])
            {
              cl = 0;                         //set error flag
              break;
            }
          }
          if(cl)                              //checksum equal, send ACK
          {
            WriteCom(XMODEM_ACK);
            cnt = 0;
          }
          else
          {
            WriteCom(XMODEM_NAK);             //checksum error, ask resend
            cnt++;                            //increase error counter
            FlashAddr -= BUFSIZE;             //modify Flash page address
            packNO--;
          }
        }
        else                                  //no verify, send ACK directly
        {
          WriteCom(XMODEM_ACK);
          cnt = 0;
        }
#else
        //no verify, send ACK directly
        WriteCom(XMODEM_ACK);
        cnt = 0;
#endif

#if WDGEn
        //clear watchdog
        wdt_reset();
#endif

#if LEDEn
        //LED indicate update status
        LEDAlt();
#endif
      }
      else //CRC
      {
        //ask resend
        WriteCom(XMODEM_NAK);
        cnt++;
      }
    }
    else //PackNo
    {
      //ask resend
      WriteCom(XMODEM_NAK);
      cnt++;
    }
    //too much error, update abort
    if(cnt > 3)
      break;
  }
  while(WaitCom() != XMODEM_EOT);
  WriteCom(XMODEM_ACK);


#if VERBOSE
  if(cnt == 0)
  {
    //prompt update success
    putstr(msg4);
  }
  else
  {
    //prompt update fail
    putstr(msg5);

#if WDGEn
    //dead loop, wait watchdog reset
    while(1);
#endif

  }
#endif
  //quit boot mode
  quit();
#ifndef PIC
  return 0;
#endif
}
//End of file: bootldr.c
