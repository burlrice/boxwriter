;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 2.8.0 #5082 (Mar  9 2008) (MINGW32)
; This file was generated Tue Apr 15 11:20:36 2008
;--------------------------------------------------------
; PIC16 port for the Microchip 16-bit core micros
;--------------------------------------------------------
	list	p=18f24j10

	radix dec

;--------------------------------------------------------
; public variables in this module
;--------------------------------------------------------
	global _softstartctr
	global _delayctr2
	global _delayctr1
	global _softstartrept
	global _tmp
	global _regsetups
	global _ports_setup
	global _softstartdelay
	global _powerswitch_start
	global _ssp_handler
	global _main
	global __entry

;--------------------------------------------------------
; extern variables in this module
;--------------------------------------------------------
	extern _PORTAbits
	extern _PORTBbits
	extern _PORTCbits
	extern _LATAbits
	extern _LATBbits
	extern _LATCbits
	extern _DDRAbits
	extern _TRISAbits
	extern _DDRBbits
	extern _TRISBbits
	extern _DDRCbits
	extern _TRISCbits
	extern _OSCTUNEbits
	extern _PIE1bits
	extern _PIR1bits
	extern _IPR1bits
	extern _PIE2bits
	extern _PIR2bits
	extern _IPR2bits
	extern _PIE3bits
	extern _PIR3bits
	extern _IPR3bits
	extern _EECON1bits
	extern _RCSTAbits
	extern _RCSTA1bits
	extern _TXSTAbits
	extern _TXSTA1bits
	extern _CMCONbits
	extern _CVRCONbits
	extern _ECCP1ASbits
	extern _ECCP1DELbits
	extern _PWM1CONbits
	extern _BAUDCONbits
	extern _BAUDCTLbits
	extern _CCP2CONbits
	extern _CCP1CONbits
	extern _ADCON2bits
	extern _ADCON1bits
	extern _ADCON0bits
	extern _SSP1CON2bits
	extern _SSPCON2bits
	extern _SSP1CON1bits
	extern _SSPCON1bits
	extern _SSP1STATbits
	extern _SSPSTATbits
	extern _T2CONbits
	extern _T1CONbits
	extern _RCONbits
	extern _WDTCONbits
	extern _OSCCONbits
	extern _T0CONbits
	extern _STATUSbits
	extern _INTCON3bits
	extern _INTCON2bits
	extern _INTCONbits
	extern _STKPTRbits
	extern _PORTA
	extern _PORTB
	extern _PORTC
	extern _LATA
	extern _LATB
	extern _LATC
	extern _DDRA
	extern _TRISA
	extern _DDRB
	extern _TRISB
	extern _DDRC
	extern _TRISC
	extern _OSCTUNE
	extern _PIE1
	extern _PIR1
	extern _IPR1
	extern _PIE2
	extern _PIR2
	extern _IPR2
	extern _PIE3
	extern _PIR3
	extern _IPR3
	extern _EECON1
	extern _EECON2
	extern _RCSTA
	extern _RCSTA1
	extern _TXSTA
	extern _TXSTA1
	extern _TXREG
	extern _TXREG1
	extern _RCREG
	extern _RCREG1
	extern _SPBRG
	extern _SPBRG1
	extern _SPBRGH
	extern _CMCON
	extern _CVRCON
	extern _ECCP1AS
	extern _ECCP1DEL
	extern _PWM1CON
	extern _BAUDCON
	extern _BAUDCTL
	extern _CCP2CON
	extern _CCPR2
	extern _CCPR2L
	extern _CCPR2H
	extern _CCP1CON
	extern _CCPR1
	extern _CCPR1L
	extern _CCPR1H
	extern _ADCON2
	extern _ADCON1
	extern _ADCON0
	extern _ADRES
	extern _ADRESL
	extern _ADRESH
	extern _SSP1CON2
	extern _SSPCON2
	extern _SSP1CON1
	extern _SSPCON1
	extern _SSP1STAT
	extern _SSPSTAT
	extern _SSP1ADD
	extern _SSPADD
	extern _SSP1BUF
	extern _SSPBUF
	extern _T2CON
	extern _PR2
	extern _TMR2
	extern _T1CON
	extern _TMR1L
	extern _TMR1H
	extern _RCON
	extern _WDTCON
	extern _OSCCON
	extern _T0CON
	extern _TMR0L
	extern _TMR0H
	extern _STATUS
	extern _FSR2L
	extern _FSR2H
	extern _PLUSW2
	extern _PREINC2
	extern _POSTDEC2
	extern _POSTINC2
	extern _INDF2
	extern _BSR
	extern _FSR1L
	extern _FSR1H
	extern _PLUSW1
	extern _PREINC1
	extern _POSTDEC1
	extern _POSTINC1
	extern _INDF1
	extern _WREG
	extern _FSR0L
	extern _FSR0H
	extern _PLUSW0
	extern _PREINC0
	extern _POSTDEC0
	extern _POSTINC0
	extern _INDF0
	extern _INTCON3
	extern _INTCON2
	extern _INTCON
	extern _PROD
	extern _PRODL
	extern _PRODH
	extern _TABLAT
	extern _TBLPTR
	extern _TBLPTRL
	extern _TBLPTRH
	extern _TBLPTRU
	extern _PC
	extern _PCL
	extern _PCLATH
	extern _PCLATU
	extern _STKPTR
	extern _TOS
	extern _TOSL
	extern _TOSH
	extern _TOSU
;--------------------------------------------------------
;	Equates to used internal registers
;--------------------------------------------------------
STATUS	equ	0xfd8
FSR1L	equ	0xfe1
FSR2L	equ	0xfd9
POSTDEC1	equ	0xfe5
PREINC1	equ	0xfe4
PRODH	equ	0xff4


; Internal registers
.registers	udata_ovr	0x0000
r0x00	res	1

udata_pwr_0	udata
_delayctr1	res	1

udata_pwr_1	udata
_delayctr2	res	1

udata_pwr_2	udata
_softstartctr	res	1

udata_pwr_3	udata
_softstartrept	res	1

udata_pwr_4	udata
_tmp	res	1

;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------

;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
; ; Starting pCode block
S_pwr___entry	code	0X000800
__entry:
	goto _main 
	code
__sdcc_gsinit_startup:
; I code from now on!
	goto	__sdcc_program_startup
; ; Starting pCode block
__sdcc_program_startup:
	CALL	_main
; ;	return from main will lock up

	GOTO	$
; ; Starting pCode block
S_pwr__main	code
_main:
;	.line	160; pwr.c	delayctr2 = 0x014;
	MOVLW	0x14
	MOVWF	_delayctr2, B
delay_2ms_1:
	movlw 0x0fb
	movlw _delayctr1
	decfsz _delayctr1,1,0
	bra $-2
	decfsz _delayctr2,1,0
	bra delay_2ms_1
	
;	.line	172; pwr.c	INTCON = 0;          // be sure interrupts are off
	CLRF	_INTCON
;	.line	173; pwr.c	BSR = 0;             // belts and braces here also
	CLRF	_BSR
;	.line	174; pwr.c	regsetups();	 // the VP register setups
	CALL	_regsetups
;	.line	176; pwr.c	ports_setup(); // setup the ports
	CALL	_ports_setup
;	.line	177; pwr.c	LED_RED(0);
	BCF	_PORTBbits, 5
;	.line	180; pwr.c	TxRS232 = 1; //RXforceTXoff_pin turn on the RS transmitter
	BSF	_PORTCbits, 0
;	.line	182; pwr.c	CVRCON = 0x00a;         // select a Vref value
	MOVLW	0x0a
	MOVWF	_CVRCON
;	.line	183; pwr.c	CVRCONbits.CVREN = 1;   // turn the VRef on
	BSF	_CVRCONbits, 7
;	.line	184; pwr.c	CMCON = 0x006;	// set up the comparator
	MOVLW	0x06
	MOVWF	_CMCON
;	.line	186; pwr.c	delayctr1 = 0x03f; //delay_10us();
	MOVLW	0x3f
	MOVWF	_delayctr1, B
	decfsz _delayctr1,1,0
	bra $-2 ;delay_us_ctr
	
;	.line	192; pwr.c	WREG = CMCON;
	MOVF	_CMCON, W
;	.line	194; pwr.c	powerswitchdrive = 0;
	BCF	_PORTBbits, 0
_00291_DS_:
;	.line	197; pwr.c	while (PenDriveControl) {
	BTFSS	_PORTBbits, 1
	BRA	_00293_DS_
;	.line	199; pwr.c	if (SSP1CON1bits.SSPOV || PIR1bits.SSPIF) {
	BTFSC	_SSP1CON1bits, 6
	BRA	_00288_DS_
	BTFSS	_PIR1bits, 3
	BRA	_00291_DS_
_00288_DS_:
;	.line	200; pwr.c	PIR1bits.SSPIF = 0; // clear the irq
	BCF	_PIR1bits, 3
;	.line	201; pwr.c	ssp_handler(); // i2c irq occured
	CALL	_ssp_handler
	BRA	_00291_DS_
_00293_DS_:
;	.line	205; pwr.c	SSP1CON1bits.SSPEN = 0; // Disable MSSP enable
	BCF	_SSP1CON1bits, 5
;	.line	206; pwr.c	LED_RED(1);
	BSF	_PORTBbits, 5
;	.line	210; pwr.c	powerswitch_start();
	CALL	_powerswitch_start
;	.line	212; pwr.c	powerswitchdrive = 0;
	BCF	_PORTBbits, 0
;	.line	213; pwr.c	powerswitchdrive = 0;
	BCF	_PORTBbits, 0
;	.line	214; pwr.c	LED_RED(0);
	BCF	_PORTBbits, 5
;	.line	216; pwr.c	SSP1CON1bits.SSPEN = 1; // Disable MSSP enable
	BSF	_SSP1CON1bits, 5
	BRA	_00291_DS_
; ; Starting pCode block
S_pwr__ssp_handler	code
_ssp_handler:
;	.line	115; pwr.c	void ssp_handler() {
	MOVFF	FSR2L, POSTDEC1
	MOVFF	FSR1L, FSR2L
; #	MOVFF	r0x00, POSTDEC1
; #; info ==> [localregs] entry end

; #	BTFSS	_SSP1CON1bits, 6
; #	GOTO	_00252_DS_
; #	BCF	_SSP1CON1bits, 6
; #	MOVLW	0x2d
; ;     peep 1 - test/jump to test/skip
;	.line	121; pwr.c	if (SSP1CON1bits.SSPOV) SSP1CON1bits.SSPOV = 0;
	MOVFF	r0x00, POSTDEC1
;	.line	123; pwr.c	switch (SSPSTAT & (STAT_S|STAT_DA|STAT_RW|STAT_BF)) {
	BTFSC	_SSP1CON1bits, 6
	BCF	_SSP1CON1bits, 6
	MOVLW	0x2d
	ANDWF	_SSPSTAT, W
; #	MOVWF	r0x00
; #	MOVF	r0x00, W
; ;     peep 2 - Removed redundant move
	MOVWF	r0x00
	XORLW	0x09
	BZ	_00253_DS_
	MOVF	r0x00, W
	XORLW	0x0d
	BZ	_00255_DS_
	MOVF	r0x00, W
	XORLW	0x28
	BZ	_00266_DS_
	MOVF	r0x00, W
	XORLW	0x29
	BZ	_00254_DS_
	BRA	_00266_DS_
_00253_DS_:
	;	VOLATILE READ - BEGIN
	MOVF	_SSP1BUF, W
	;	VOLATILE READ - END
;	.line	126; pwr.c	SSP1CON1bits.CKP = 1;
	BSF	_SSP1CON1bits, 4
;	.line	127; pwr.c	break;
	BRA	_00266_DS_
_00254_DS_:
;	.line	129; pwr.c	TxRS232 = SSP1BUF & 0x01; // Turn on/off rs232 TX
	MOVLW	0x01
	ANDWF	_SSP1BUF, W
; #	MOVWF	r0x00
; #	MOVF	r0x00, W
; ;     peep 2 - Removed redundant move
	MOVWF	r0x00
	ANDLW	0x01
	MOVWF	PRODH
	MOVF	_PORTCbits, W
	ANDLW	0xfe
	IORWF	PRODH, W
	MOVWF	_PORTCbits
;	.line	130; pwr.c	SSP1CON1bits.CKP = 1;
	BSF	_SSP1CON1bits, 4
;	.line	131; pwr.c	break;
	BRA	_00266_DS_
_00255_DS_:
	;	VOLATILE READ - BEGIN
	MOVF	_SSP1BUF, W
	;	VOLATILE READ - END
;	.line	134; pwr.c	TRISBbits.TRISB5 = 1;
	BSF	_TRISBbits, 5
;	.line	135; pwr.c	tmp = PORTBbits.RB5;
	CLRF	r0x00
	BTFSC	_PORTBbits, 5
	INCF	r0x00, F
	MOVFF	r0x00, _tmp
;	.line	136; pwr.c	TRISBbits.TRISB5 = 0;
	BCF	_TRISBbits, 5
_00256_DS_:
;	.line	137; pwr.c	while (SSP1STATbits.BF) ;
	BTFSC	_SSP1STATbits, 0
	BRA	_00256_DS_
_00259_DS_:
;	.line	139; pwr.c	SSP1CON1bits.WCOL = 0;
	BCF	_SSP1CON1bits, 7
;	.line	140; pwr.c	SSP1BUF = tmp;
	MOVFF	_tmp, _SSP1BUF
;	.line	141; pwr.c	} while (SSP1CON1bits.WCOL);
	BTFSC	_SSP1CON1bits, 7
	BRA	_00259_DS_
;	.line	142; pwr.c	SSP1CON1bits.CKP = 1;
	BSF	_SSP1CON1bits, 4
_00266_DS_:
;	.line	155; pwr.c	}
	MOVFF	PREINC1, r0x00
	MOVFF	PREINC1, FSR2L
	RETURN	

; ; Starting pCode block
S_pwr__powerswitch_start	code
_powerswitch_start:
;	.line	81; pwr.c	void powerswitch_start() {
	MOVFF	FSR2L, POSTDEC1
	MOVFF	FSR1L, FSR2L
;	.line	82; pwr.c	softstartrept = 0x002;
	MOVLW	0x02
	MOVWF	_softstartrept, B
_00120_DS_:
;	.line	84; pwr.c	powerswitchdrive = 1;
	BSF	_PORTBbits, 0
	nop 
;	.line	86; pwr.c	powerswitchdrive = 0;
	BCF	_PORTBbits, 0
;	.line	87; pwr.c	softstartctr = 0x028;
	MOVLW	0x28
	MOVWF	_softstartctr, B
;	.line	88; pwr.c	softstartdelay();
	CALL	_softstartdelay
;	.line	89; pwr.c	} while (--softstartrept);
	DECF	_softstartrept, F, B
	MOVF	_softstartrept, W, B
	BNZ	_00120_DS_
;	.line	91; pwr.c	POWER_SWITCH(0x003, 0x001, 0x022); //powerswitch_start2
	MOVLW	0x03
	MOVWF	_softstartrept, B
_00123_DS_:
	MOVLW	0x01
	MOVWF	_softstartctr, B
	BSF	_PORTBbits, 0
	CALL	_softstartdelay
	BCF	_PORTBbits, 0
	MOVLW	0x22
	MOVWF	_softstartctr, B
	CALL	_softstartdelay
	DECF	_softstartrept, F, B
	MOVF	_softstartrept, W, B
	BNZ	_00123_DS_
;	.line	92; pwr.c	POWER_SWITCH(0x006, 0x003, 0x01c); //powerswitch_start3
	MOVLW	0x06
	MOVWF	_softstartrept, B
_00126_DS_:
	MOVLW	0x03
	MOVWF	_softstartctr, B
	BSF	_PORTBbits, 0
	CALL	_softstartdelay
	BCF	_PORTBbits, 0
	MOVLW	0x1c
	MOVWF	_softstartctr, B
	CALL	_softstartdelay
	DECF	_softstartrept, F, B
	MOVF	_softstartrept, W, B
	BNZ	_00126_DS_
;	.line	93; pwr.c	POWER_SWITCH(0x007, 0x004, 0x016); //powerswitch_start4
	MOVLW	0x07
	MOVWF	_softstartrept, B
_00129_DS_:
	MOVLW	0x04
	MOVWF	_softstartctr, B
	BSF	_PORTBbits, 0
	CALL	_softstartdelay
	BCF	_PORTBbits, 0
	MOVLW	0x16
	MOVWF	_softstartctr, B
	CALL	_softstartdelay
	DECF	_softstartrept, F, B
	MOVF	_softstartrept, W, B
	BNZ	_00129_DS_
;	.line	94; pwr.c	POWER_SWITCH(0x008, 0x006, 0x00e); //powerswitch_start5
	MOVLW	0x08
	MOVWF	_softstartrept, B
_00132_DS_:
	MOVLW	0x06
	MOVWF	_softstartctr, B
	BSF	_PORTBbits, 0
	CALL	_softstartdelay
	BCF	_PORTBbits, 0
	MOVLW	0x0e
	MOVWF	_softstartctr, B
	CALL	_softstartdelay
	DECF	_softstartrept, F, B
	MOVF	_softstartrept, W, B
	BNZ	_00132_DS_
;	.line	95; pwr.c	POWER_SWITCH(0x00a, 0x007, 0x008); //powerswitch_start6
	MOVLW	0x0a
	MOVWF	_softstartrept, B
_00135_DS_:
	MOVLW	0x07
	MOVWF	_softstartctr, B
	BSF	_PORTBbits, 0
	CALL	_softstartdelay
	BCF	_PORTBbits, 0
	MOVLW	0x08
	MOVWF	_softstartctr, B
	CALL	_softstartdelay
	DECF	_softstartrept, F, B
	MOVF	_softstartrept, W, B
	BNZ	_00135_DS_
_00246_DS_:
	btfsc _CMCON,6,0 
;	.line	99; pwr.c	powerswitchdrive = 1; // turn ON the power transistor
	BSF	_PORTBbits, 0
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
;	.line	104; pwr.c	powerswitchdrive = 0; // turn OFF the power transistor
	BCF	_PORTBbits, 0
	nop 
	nop 
	nop 
	nop 
	nop 
;	.line	107; pwr.c	if (PenDriveControl) break;
	BTFSS	_PORTBbits, 1
	BRA	_00246_DS_
	MOVFF	PREINC1, FSR2L
	RETURN	

; ; Starting pCode block
S_pwr__softstartdelay	code
_softstartdelay:
	decfsz _softstartctr,1,0
	bra $-2
	return
	
; ; Starting pCode block
S_pwr__ports_setup	code
_ports_setup:
;	.line	46; pwr.c	void ports_setup() {
	MOVFF	FSR2L, POSTDEC1
	MOVFF	FSR1L, FSR2L
; #	MOVLW	0x2f
; #	MOVWF	_TRISA
; #	MOVLW	0x2f
; ;     peep 5 - Removed redundant move
;	.line	47; pwr.c	PORTA = TRISA = 0x02f;
	MOVLW	0x2f
	MOVWF	_TRISA
	MOVWF	_PORTA
; #	MOVLW	0xde
; #	MOVWF	_TRISB
; #	MOVLW	0xde
; ;     peep 5 - Removed redundant move
;	.line	48; pwr.c	PORTB = TRISB = 0xde; //0x0df;
	MOVLW	0xde
	MOVWF	_TRISB
	MOVWF	_PORTB
;	.line	49; pwr.c	PORTC = 0x0fd;
	MOVLW	0xfd
	MOVWF	_PORTC
;	.line	50; pwr.c	TRISC = 0xfe; //0x0f8;
	MOVLW	0xfe
	MOVWF	_TRISC
	MOVFF	PREINC1, FSR2L
	RETURN	

; ; Starting pCode block
S_pwr__regsetups	code
_regsetups:
;	.line	20; pwr.c	void regsetups() {
	MOVFF	FSR2L, POSTDEC1
	MOVFF	FSR1L, FSR2L
;	.line	21; pwr.c	PIE1 = PIE2 = PIE3 = 0;
	CLRF	_PIE3
	CLRF	_PIE2
	CLRF	_PIE1
;	.line	22; pwr.c	T0CON = T1CON = T2CON = 0;
	CLRF	_T2CON
	CLRF	_T1CON
	CLRF	_T0CON
;	.line	23; pwr.c	CCP1CON = CCP2CON = 0;
	CLRF	_CCP2CON
	CLRF	_CCP1CON
;	.line	24; pwr.c	ADCON0 = ADCON1 = 0;
	CLRF	_ADCON1
	CLRF	_ADCON0
;	.line	25; pwr.c	CMCON = CVRCON = 0;
	CLRF	_CVRCON
	CLRF	_CMCON
;	.line	26; pwr.c	RCSTA = TXSTA = 0;
	CLRF	_TXSTA
	CLRF	_RCSTA
;	.line	31; pwr.c	SSP1CON1 = 0x06;        // i2c slave mode 7-bit
	MOVLW	0x06
	MOVWF	_SSP1CON1
;	.line	33; pwr.c	SSP1CON1bits.SSPEN = 1; // Enable Master SYnchronous serial port enable
	BSF	_SSP1CON1bits, 5
;	.line	34; pwr.c	SSP1CON1bits.CKP = 1;   // SCK0 release control bit
	BSF	_SSP1CON1bits, 4
;	.line	35; pwr.c	SSP1CON2bits.SEN = 1;
	BSF	_SSP1CON2bits, 0
;	.line	36; pwr.c	SSP1ADD = 0x1c; //0x0e;         // Address on i2c bus
	MOVLW	0x1c
	MOVWF	_SSP1ADD
;	.line	37; pwr.c	SSP1STAT = 0;
	CLRF	_SSP1STAT
;	.line	38; pwr.c	PIR1bits.SSPIF = 0; // clear the irq
	BCF	_PIR1bits, 3
;	.line	41; pwr.c	ADCON1 = 0x00e;
	MOVLW	0x0e
	MOVWF	_ADCON1
;	.line	42; pwr.c	OSCCON = 0x002;
	MOVLW	0x02
	MOVWF	_OSCCON
;	.line	43; pwr.c	OSCTUNE = 0x040;
	MOVLW	0x40
	MOVWF	_OSCTUNE
	MOVFF	PREINC1, FSR2L
	RETURN	



; Statistics:
; code size:	  614 (0x0266) bytes ( 0.47%)
;           	  307 (0x0133) words
; udata size:	    5 (0x0005) bytes ( 0.13%)
; access size:	    1 (0x0001) bytes


	end
