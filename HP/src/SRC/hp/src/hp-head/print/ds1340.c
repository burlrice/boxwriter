///////////////////////////////////////////////////////////////////////////////////
///                               DS1340.C                                     ///
///                   Driver for Real Time Clock                      ///
///      For Dallas/Maxim DS1340 RTC (I2C, trickle charge, etc)      ///
///         Based on DS1307 driver.             ///
///                                                                            ///
///    rtc_init()    -Call once at start-up. To Set trickle charge state   ///
///         uncomment the line you want below. No Calibration    ///
///         used here Control byte is set to all 0's      ///
///                                             ///
///                                                                            ///
///    rtc_set_date_time(day,mth,year,dow,hour,min,sec)  Set the date/time     ///
///                                                                           ///
///    rtc_get_date(day,mth,year,dow)               Get the date*      ///
///                                                                        ///
///   rtc_get_time(hr,min,sec)                     Get the time*                ///
///                              ///
///      *(if returned value's are all zeros this means that the clock   ///
///      stopped for some reason and the time is invalid)      ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////////

#include "i2c.h"
#include "time.h"
//#define RTC_SDA  PIN_C4
//#define RTC_SCL  PIN_C3

//#use i2c(master, sda=RTC_SDA, scl=RTC_SCL, FORCE_HW)

unsigned char bin2bcd(unsigned char binary_value);
unsigned char bcd2bin(unsigned char bcd_value);
void rtc_check_valid(void);

void rtc_init(void) {
    unsigned char seconds = 0;
   
    i2c_start();
    i2c_write(0xD0);      // WR to RTC
    i2c_write(0x00);      // REG 0
    i2c_start();
    i2c_write(0xD1);            // RD from RTC
    seconds = bcd2bin(i2c_read(0)); // Read current "seconds" in DS1340
    i2c_stop();
    seconds &= 0x7F;

//   delay_us(3);

    i2c_start();
    i2c_write(0xD0);            // WR to RTC
    i2c_write(0x00);            // REG 0
    i2c_write(bin2bcd(seconds));    // Start oscillator with current "seconds value

    i2c_start();
    i2c_write(0xD0);   // WR to RTC
    i2c_write(0x07);   // goto Control reg
    i2c_write(0);      // Set control byte to zer0s

    i2c_start();
    i2c_write(0xD0);   // WR to RTC
    i2c_write(0x08);   // goto trickle address

      // Set Trickle Charger byte (uncomment ONLY one of the following)*****************
//   i2c_write(0b00100101);      //  disabled
//   i2c_write(0b10100101);      //  No diode,   250? resistor
//   i2c_write(0b10101001);      //  One diode,   250? resistor
//   i2c_write(0b10100110);      //  No diode,   2k? resistor
//   i2c_write(0b10101010);      //  One diode,   2k? resistor
#ifndef WIN32
    i2c_write(0b10100111);      //  No diode,   4k? resistor
#endif
//   i2c_write(0b10101011);      //  One diode,   4k? resistor
      // Set Trickle Charger byte
    i2c_stop();
}

void rtc_set_date_time(struct tm * tme) { 
    //unsigned char day, unsigned char mth, unsigned char year, unsigned char dow, unsigned char hr, unsigned char min, unsigned char sec) {
    tme->tm_sec &= 0x7F;
    tme->tm_hour &= 0x3F;

    i2c_start();
    i2c_write(0xD0);      // I2C write address
    i2c_write(0x00);      // Start at REG 0 - Seconds
    i2c_write(bin2bcd(tme->tm_sec));        // REG 0
    i2c_write(bin2bcd(tme->tm_min));        // REG 1
    i2c_write(bin2bcd(tme->tm_hour));       // REG 2
    i2c_write(bin2bcd(tme->tm_wday));       // REG 3
    i2c_write(bin2bcd(tme->tm_mday));       // REG 4
    i2c_write(bin2bcd(tme->tm_mon));        // REG 5
    i2c_write(bin2bcd(tme->tm_year));       // REG 6
    i2c_start();
    i2c_write(0xD0);   // WR to RTC
    i2c_write(0x09);   // Set OSF address
    i2c_write(0);      // Clear OSF
    i2c_stop();
}

/*void rtc_get_date(unsigned char* day, unsigned char* mth, unsigned char* year, unsigned char* dow) {
    i2c_start();
    i2c_write(0xD0);
    i2c_write(0x03);            // Start at REG 3 - Day of week
    i2c_start();
    i2c_write(0xD1);
    dow  = bcd2bin(i2c_read(ACK) & 0x7f);   // REG 3
    day  = bcd2bin(i2c_read(ACK) & 0x3f);   // REG 4
    mth  = bcd2bin(i2c_read(ACK) & 0x1f);   // REG 5
    year = bcd2bin(i2c_read(NO_ACK));      // REG 6
    i2c_stop();

    rtc_check_valid();
}*/

void rtc_get_time(struct tm * tme) { //unsigned char* hr, unsigned char* min, unsigned char* sec) {
    unsigned char valid;
    i2c_start();
    i2c_write(0xD0);
    i2c_write(0x00);            // Start at REG 0 - Seconds
    i2c_start();
    i2c_write(0xD1);
    tme->tm_sec = bcd2bin(i2c_read(ACK) & 0x7f);        // reg 1
    tme->tm_min = bcd2bin(i2c_read(ACK) & 0x7f);        // reg 2
    tme->tm_hour = bcd2bin(i2c_read(ACK) & 0x3f);    // reg 3
    ////////////////////////////////////////////////
    tme->tm_wday = bcd2bin(i2c_read(ACK) & 0x07);       // REG 4
    tme->tm_mday = bcd2bin(i2c_read(ACK) & 0x3f);       // REG 5
    tme->tm_mon = bcd2bin(i2c_read(ACK) & 0x1f);        // REG 6
    tme->tm_year = bcd2bin(i2c_read(NO_ACK));           // REG 7
    ////////////////////////////////////////////////
    i2c_stop();

//    rtc_check_valid();
//}
//void rtc_check_valid(void) {
//    int   validity;
    i2c_start();      //read OSF bit to check data validity
    i2c_write(0xD0);
    i2c_write(0x09);
    i2c_start();
    i2c_write(0xD1);
    valid = i2c_read(NO_ACK);
    i2c_stop();

    if (valid != 0) {   //if data not valid make them zeros
        tme->tm_sec=0;
        tme->tm_min=0;
        tme->tm_hour=0;
        tme->tm_wday=0;      
        tme->tm_mday=0;
        tme->tm_mon=0;
        tme->tm_year=0;
    }
}

unsigned char bin2bcd(unsigned char binary_value) {
    unsigned char temp;
    unsigned char retval;

    temp = binary_value;
    retval = 0;

    while(1) {
        // Get the tens digit by doing multiple subtraction
        // of 10 from the binary value.
        if(temp >= 10) {
            temp -= 10;
            retval += 0x10;
        } else { // Get the ones digit by adding the remainder.
            retval += temp;
            break;
        }
    }

    return(retval);
}

// Input range - 00 to 99.
unsigned char bcd2bin(unsigned char bcd_value) {
    unsigned char temp;

    temp = bcd_value;
    // Shifting upper digit right by 1 is same as multiplying by 8.
    temp >>= 1;
    // Isolate the bits for the upper digit.
    temp &= 0x78;

    // Now return: (Tens * 8) + (Tens * 2) + Ones
    return(temp + (temp >> 2) + (bcd_value & 0x0f));
}

