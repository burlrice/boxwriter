#include <stdlib.h>
#include "time.h"
struct tm time;

ROM UCHAR DaysInMon[] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
ROM unsigned short DaysLeftInYr[] = {335, 306, 275, 245, 214, 184, 153, 122, 92, 61, 31, 0};
RUCHAR * months[] = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
    "SEP", "OCT", "NOV", "DEC"};

#define isleap(y) (0 == (y%4))

void ExpOffset(struct tm * tme, unsigned short offset) {
    UCHAR yr, mon, date, DaysToNextMonth;
    unsigned short DaysToNextYear;
    yr = tme->tm_year;
    mon = tme->tm_mon;
    date = tme->tm_mday;
    DaysToNextYear = DaysLeftInYr[mon-1] + DaysInMon[mon-1] - date + 1 - 
        ((!isleap(yr) && (mon < 3)) ? 1 : 0);
    while (DaysToNextYear <= offset) {
        offset -= DaysToNextYear;
        yr++;
        mon = 1;
        date = 1;
        DaysToNextYear = 365 + (isleap(yr) ? 1 : 0);
    }

    DaysToNextMonth = DaysInMon[mon - 1] - date + 1;
    if (!isleap(yr) && (mon == 2)) DaysToNextMonth--;
    while (offset >= DaysToNextMonth) {
        offset -= DaysToNextMonth;
        mon++;
        date = 1;
        DaysToNextMonth = DaysInMon[mon-1];
        if ((2 == mon) && !isleap(yr)) DaysToNextMonth--;
    }
    date += offset;
    tme->tm_year = yr;
    tme->tm_mday = date;
    tme->tm_mon = mon;
}

void ConvertDate(struct tm * tme, UCHAR * buf, int sze) {
    UCHAR tmp[6], c, *str, x, j;
    UCHAR * p;
    int i;
    p = buf;
    str = buf;
    while (p) {
        c = *p++;
        x = 0;
        if (0 == c) break;
        switch (c) {
        case 'J': {
            if (('J' == *p) && ('J' == *(p+1))) {
                p += 2;
                ///////////////////////////////////////////////
                i = 0;
                for(j=1; j < tme->tm_mon; j++) i += DaysInMon[j-1];
                i += tme->tm_mday;
                if ((tme->tm_mon > 2) && (!isleap(tme->tm_year))) i--;
                ///////////////////////////////////////////////
                tmp[0] = '0' + i / 100;
                tmp[1] = '0' + (i / 10) % 10;
                i = i % 100;
                tmp[2] = '0' + (i % 10);
                x = 3;
            }
            break;
        }
        case 'M': {
            if ('M' == *p) {
                p++;
                i = tme->tm_mon;
                tmp[0] = '0' + i / 10;
                tmp[1] = '0' + i % 10;
                x = 2;
            } else if (('O' == *p) && ('N' == *(p+1))) {
                p += 2;
                i = tme->tm_mon-1;
                tmp[0] = months[i][0];
                tmp[1] = months[i][1];
                tmp[2] = months[i][2];
                x = 3;
            }
            break;
        }
        case 'D': {
            if ('D' == *p) {
                p++;
                tmp[0] = '0' + tme->tm_mday / 10;
                tmp[1] = '0' + tme->tm_mday % 10;
                x = 2;
            }
            break;
        }
        case 'Y': {
            if ('Y' == *p) {
                if ('Y' == *(p+1) && 'Y' == *(p+2)) {
                    p += 3;
                    i = 20+tme->tm_year/100;
                    tmp[0] = '0' + i / 10;
                    tmp[1] = '0' + i % 10;
                    i = tme->tm_year % 100;
                    tmp[2] = '0' + i / 10;
                    tmp[3] = '0' + i % 10;
                    x = 4;
                } else {
                    p++;
                    i = tme->tm_year % 100;
                    tmp[0] = '0' + i / 10;
                    tmp[1] = '0' + i % 10;
                    x = 2;
                }
            }
            break;
        }
        case 'h': {
            if ('h' == *p) {
                p++;
                tmp[0] = '0' + tme->tm_hour / 10;
                tmp[1] = '0' + tme->tm_hour % 10;
                x = 2;
            }
            break;
        }
        case 'm': {
            if ('m' == *p) {
                p++;
                tmp[0] = '0' + tme->tm_min / 10;
                tmp[1] = '0' + tme->tm_min % 10;
                x = 2;
            }
            break;
        }
        case 's': {
            if ('s' == *p) {
                p++;
                tmp[0] = '0' + tme->tm_sec / 10;
                tmp[1] = '0' + tme->tm_sec % 10;
                x = 2;
            }
            break;
        }
        default:
            tmp[0] = c;
            x = 1;
            break;
        }
        i = 0;
        sze -= x;
        //*str = '\0';
        if (0 > sze) return;
        while (x--) *str++ = tmp[i++];
        if (0 == sze) return;
        //*str = '\0';
    }
}
