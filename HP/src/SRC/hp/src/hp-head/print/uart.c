#include "processor.h"
#include "TYPES.H"			// Standard types
//#include	"MEMIO.H"			// Memory movement routines
#ifdef PIC
#define mIOInit(BRGval)	RCSTA1 = 0; RCSTA1 = 0x90; TXSTA1 = 0; TXSTA1 = 0x24; SPBRG1 = BRGval;
#define chkIOError() if (RCSTA1bits.FERR || RCSTA1bits.OERR) { RCSTA1bits.CREN = 0; RCSTA1bits.CREN = 1;  /* AN774 */ }
#define mIOIsGetReady()		PIR1bits.RCIF
#define mIOGetByte()		RCREG1
#define mIOIsPutReady()		PIR1bits.TXIF
#define	mIOPutByte(txData)	{ \
    while (!PIR1bits.TXIF) ; \
    TXREG1 = txData; \
    /*while (!TXSTAbits.TRMT) ;*/ /* wait till it is done txmting */ \
}
#define WaitGetReady() while (!mIOIsGetReady()) ;
//#define UART_PUTC _usart_putc
#define UART_PUTC UART_SendByte
#elif defined(AVR)
#define mIOInit(a) \
    /* enable Rx and TX */ outp(BV(RXEN)|BV(TXEN),UCSR0B);\
    /* set baud rate */ outp( (unsigned char)8, UBRR0);
//    fdevopen(UART_SendByte, UART_ReceiveByte, 0);
#define mIOIsGetReady()		bit_is_set(UCSR0A, RXC)
#define mIOGetByte()		inp(UDR0)
#define mIOIsPutReady()		(bit_is_set(UCSR0A, UDRE))
#define	mIOPutByte(txData) {loop_until_bit_is_set(UCSR0A, UDRE); outp( txData, UDR0 ); }
#define WaitGetReady() loop_until_bit_is_set(UCSR0A, RXC)
#define UART_PUTC UART_SendByte
#elif defined(WIN32)
#define mIOInit(a)
#define WaitGetReady() 
#define mIOGetByte()    getchar()
#define UART_PUTC UART_SendByte
extern char getche();
extern char getch();
//#define getchar getch
#define	mIOPutByte(txData)      putchar(txData)	
#endif

void UART_PUTC(char c) {
    if ('\n' == c) {
        mIOPutByte('\r');
        Nop(); // see spec for why this has to be here
    }
    mIOPutByte(c);
}

char UART_ReceiveByte(void) {
    char c;
    //chkIOError();
    //WaitGetReady();
    c = mIOGetByte();
    return c;
}

// Open an IO channel
unsigned char UART_init(UCHAR bitrate) {
	// Setup async communications
	mIOInit(bitrate);
	return 1;
}

ROM char hexchar[] = {"0123456789abcdef"};
void printHex(unsigned char c) {
    UART_PUTC(hexchar[c >> 4]);
    UART_PUTC(hexchar[0x0f & c]);
}

#ifdef PIC
void puts(ROM unsigned char * buf) {
    ROM unsigned char * pc;
#else
int puts(const char * buf) {
    const char * pc = buf;
#endif
    pc = buf;
    while (*pc) {
        UART_PUTC(*pc++);
    }
#ifndef PIC
    return 0;
#endif
}
