#include <stdio.h>

#define UCHAR unsigned char
#define Fram_OpenRead(a) ndx = a
#define Fram_Read() fram_data[ndx++]
#define Fram_ReadLast()
#define Fram_Close()
#define Fram_OpenWrite Fram_OpenRead
#define Fram_Write(a) fram_data[ndx++] = a

//aa 02 0000 
unsigned char fram_data[] = 
        "\x00\xaa\x45\xbd\x00\x00\x00\x00"
        "\x00\xd7\x03\x4c\x00\x00\x00\x00";
int ndx;

unsigned long ReadLong(UCHAR offset) {
    unsigned long results;
    Fram_OpenRead(offset);
    results = Fram_Read();
    results <<= 8;
    results |= Fram_Read();
    results <<= 8;
    results |= Fram_Read(); 
    results <<= 8;
    results |= Fram_Read(); 
    Fram_ReadLast();
    Fram_Close();
    return results;
}

void WriteLong(UCHAR offset, unsigned long data) {
    Fram_OpenWrite(offset);
    Fram_Write((data>>24) & 0xff);
    Fram_Write((data>>16)&0xff);
    Fram_Write((data>>8)&0xff);
    Fram_Write(data&0xff);
    Fram_Close();
}

void DumpFram() {
    int i;
    for (i=0; i < sizeof(fram_data)-1; i++) {
        printf("%02x", fram_data[i]);
        if (0x3 == (3 & i)) putchar(' ');
    }
    printf("\n");
}

struct {
    unsigned char wafer;
    unsigned short id;
    unsigned long dots;
} itms[] = {
    {0xd7, 0x034c, 0},
    {0xef, 0,      0},
    {0xaa, 0x45bd, 0},
    {0x12, 0,      0},
    {0xd7, 0x034c, 32},
    {0xaa, 0x45bd, 32},
    {0x34, 0,      0},
    {0x56, 0,      0},
    {0x65, 0x9876, 0},
    {0xaa, 0x45bd, 64},
    {0x65, 0x9876, 32},
    {0, 0, 0}};

unsigned long TotalDots = 0;
unsigned char ink;

int main(int argc, char * argv[]) {
    int i;
    unsigned char tmp, wafer; //, sad;
    unsigned short id;
    unsigned long data, data2;

    DumpFram();
    for (i=0; itms[i].wafer; i++) {
        id = itms[i].id;
        wafer = itms[i].wafer;
//////////////////////////////////////////////
    tmp = 0;
    TotalDots = 0;
    if (id) {
        for (; tmp < 16; tmp += 8) {
            data = ReadLong(tmp);
            //printf("%08lx ", data); //printLongHex(data); putchar(' ');
            if (((data >> 16) == wafer) && (id == (0xffff & data))) {
                TotalDots = ReadLong(tmp+4);
                break;
            }
        }
        //putchar('\n');
        if (8 <= tmp) {
            // not found, move entries down and place as first
            data = ReadLong(0);
            data2 = ReadLong(4);
            WriteLong(8, data);
            WriteLong(12, data2);
        }
        WriteLong(0, wafer << 16 | id);
        tmp = 4;
    }
    ink = tmp;
//////////////////////////////////////////////
        if (TotalDots == itms[i].dots) putchar('.');
        else putchar('F');
        TotalDots += 32;
        if (ink) WriteLong(ink, TotalDots);
    }
    putchar('\n');
    DumpFram();

    return 0;
}
