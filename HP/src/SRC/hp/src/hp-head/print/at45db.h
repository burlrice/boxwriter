#ifndef __AT45DB_IF_H__
#define __AT45DB_IF_H__

#define swapw(w) (((w<<8)&0xff00) | ((w>>8)&0x00ff))
#define swapl(lw) (((lw<<24)&0xff000000L) | ((lw<<8 )&0x00ff0000L) | \
           ((lw>>8 )&0x0000ff00L) | ((lw>>24)&0x000000ffL))

void at45db_init(void);
#define WRITE_BUFFER1 (0x84)
#define CONTINUOUS_READ (0x03)
#define PROGRAM_BUFFER1 (0x83)
#define READ_STATUS (0xD7)
#ifndef WIN32
#define spi_open() { \
    LATCbits.LATC2 = 0; \
}

#define spi_close() { \
    LATCbits.LATC2 = 1; \
}

#define spi_write_byte(var1) { \
    SSP1BUF = var1; \
    while (!SSP1STATbits.BF); \
    SSP1BUF; \
}

#define spi_read_byte(var1) { \
    SSP1BUF = 0xFF; \
    while (!SSP1STATbits.BF); \
    var1 = SSP1BUF; \
}

#define spi_read_short(var2) { \
    SSP1BUF = 0xFF; \
    while (!SSP1STATbits.BF); \
    var2 = SSP1BUF; \
    SSP1BUF = 0xFF; \
    var2 <<= 8; \
    while (!SSP1STATbits.BF); \
    var2 += SSP1BUF; \
}

#define spi_write_addr(var3) { \
    SSP1BUF = var3>>16; \
    while (!SSP1STATbits.BF); \
    SSP1BUF; \
    SSP1BUF = var3>>8; \
    while (!SSP1STATbits.BF); \
    SSP1BUF; \
    SSP1BUF = var3; \
    while (!SSP1STATbits.BF); \
    SSP1BUF; \
}

#define spi_read_long(var4) { \
    SSP1BUF = 0xFF; \
    while (!SSP1STATbits.BF); \
    var4 = SSP1BUF; \
    SSP1BUF = 0xFF; \
    var4 <<= 8; \
    while (!SSP1STATbits.BF); \
    var4 += SSP1BUF; \
    SSP1BUF = 0xFF; \
    var4 <<= 8; \
    while (!SSP1STATbits.BF); \
    var4 += SSP1BUF; \
    SSP1BUF = 0xFF; \
    var4 <<= 8; \
    while (!SSP1STATbits.BF); \
    var4 += SSP1BUF; \
}

#define wait_while_busy() { \
    spi_open(); \
    spi_write_byte(READ_STATUS); \
    do { \
        SSP1BUF = 0xFF; \
        while (!SSP1STATbits.BF); \
    } while (0 == (0x80 & SSP1BUF)); \
    spi_close(); \
}
#else /* WIN32 */
extern unsigned char spi_readb(void);
#define spi_read_byte(a) a = spi_readb()
#define spi_read_long(var4) 
#define spi_read_short(a) { \
    a = spi_readb(); \
    a <<= 8; \
    a += spi_readb(); \
}

#define spi_write_byte(a)
extern void spi_write_addr(unsigned long a);
#define spi_open()
#define spi_close()
#endif
#endif

#define FNAME_SZE (16)
