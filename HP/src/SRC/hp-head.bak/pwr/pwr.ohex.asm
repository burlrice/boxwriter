;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 2.7.4 #5000 (Jan 30 2008) (MINGW32)
; This file was generated Mon Feb 18 11:25:50 2008
;--------------------------------------------------------
; PIC16 port for the Microchip 16-bit core micros
;--------------------------------------------------------
	list	p=18f44j10

	radix dec

;--------------------------------------------------------
; public variables in this module
;--------------------------------------------------------
	global _softstartctr
	global _delayctr2
	global _delayctr1
	global _softstartrept
	global _regsetups
	global _ports_setup
	global _softstartdelay
	global _powerswitch_start
	global _startup
	global __entry

;--------------------------------------------------------
; extern variables in this module
;--------------------------------------------------------
	extern _PORTAbits
	extern _PORTBbits
	extern _PORTCbits
	extern _PORTDbits
	extern _PORTEbits
	extern _SSP2CON2bits
	extern _SSP2CON1bits
	extern _SSP2STATbits
	extern _LATAbits
	extern _LATBbits
	extern _LATCbits
	extern _LATDbits
	extern _LATEbits
	extern _DDRAbits
	extern _TRISAbits
	extern _DDRBbits
	extern _TRISBbits
	extern _DDRCbits
	extern _TRISCbits
	extern _DDRDbits
	extern _TRISDbits
	extern _DDREbits
	extern _TRISEbits
	extern _OSCTUNEbits
	extern _PIE1bits
	extern _PIR1bits
	extern _IPR1bits
	extern _PIE2bits
	extern _PIR2bits
	extern _IPR2bits
	extern _PIE3bits
	extern _PIR3bits
	extern _IPR3bits
	extern _EECON1bits
	extern _RCSTAbits
	extern _RCSTA1bits
	extern _TXSTAbits
	extern _TXSTA1bits
	extern _CMCONbits
	extern _CVRCONbits
	extern _ECCP1ASbits
	extern _ECCP1DELbits
	extern _PWM1CONbits
	extern _BAUDCONbits
	extern _BAUDCTLbits
	extern _CCP2CONbits
	extern _CCP1CONbits
	extern _ECCP1CONbits
	extern _ADCON2bits
	extern _ADCON1bits
	extern _ADCON0bits
	extern _SSP1CON2bits
	extern _SSPCON2bits
	extern _SSP1CON1bits
	extern _SSPCON1bits
	extern _SSP1STATbits
	extern _SSPSTATbits
	extern _T2CONbits
	extern _T1CONbits
	extern _RCONbits
	extern _WDTCONbits
	extern _OSCCONbits
	extern _T0CONbits
	extern _STATUSbits
	extern _INTCON3bits
	extern _INTCON2bits
	extern _INTCONbits
	extern _STKPTRbits
	extern _PORTA
	extern _PORTB
	extern _PORTC
	extern _PORTD
	extern _PORTE
	extern _SSP2CON2
	extern _SSP2CON1
	extern _SSP2STAT
	extern _SSP2ADD
	extern _LATA
	extern _LATB
	extern _LATC
	extern _LATD
	extern _LATE
	extern _SSP2BUF
	extern _DDRA
	extern _TRISA
	extern _DDRB
	extern _TRISB
	extern _DDRC
	extern _TRISC
	extern _DDRD
	extern _TRISD
	extern _DDRE
	extern _TRISE
	extern _OSCTUNE
	extern _PIE1
	extern _PIR1
	extern _IPR1
	extern _PIE2
	extern _PIR2
	extern _IPR2
	extern _PIE3
	extern _PIR3
	extern _IPR3
	extern _EECON1
	extern _EECON2
	extern _RCSTA
	extern _RCSTA1
	extern _TXSTA
	extern _TXSTA1
	extern _TXREG
	extern _TXREG1
	extern _RCREG
	extern _RCREG1
	extern _SPBRG
	extern _SPBRG1
	extern _SPBRGH
	extern _CMCON
	extern _CVRCON
	extern _ECCP1AS
	extern _ECCP1DEL
	extern _PWM1CON
	extern _BAUDCON
	extern _BAUDCTL
	extern _CCP2CON
	extern _CCPR2
	extern _CCPR2L
	extern _CCPR2H
	extern _CCP1CON
	extern _ECCP1CON
	extern _CCPR1
	extern _CCPR1L
	extern _CCPR1H
	extern _ADCON2
	extern _ADCON1
	extern _ADCON0
	extern _ADRES
	extern _ADRESL
	extern _ADRESH
	extern _SSP1CON2
	extern _SSPCON2
	extern _SSP1CON1
	extern _SSPCON1
	extern _SSP1STAT
	extern _SSPSTAT
	extern _SSP1ADD
	extern _SSPADD
	extern _SSP1BUF
	extern _SSPBUF
	extern _T2CON
	extern _PR2
	extern _TMR2
	extern _T1CON
	extern _TMR1L
	extern _TMR1H
	extern _RCON
	extern _WDTCON
	extern _OSCCON
	extern _T0CON
	extern _TMR0L
	extern _TMR0H
	extern _STATUS
	extern _FSR2L
	extern _FSR2H
	extern _PLUSW2
	extern _PREINC2
	extern _POSTDEC2
	extern _POSTINC2
	extern _INDF2
	extern _BSR
	extern _FSR1L
	extern _FSR1H
	extern _PLUSW1
	extern _PREINC1
	extern _POSTDEC1
	extern _POSTINC1
	extern _INDF1
	extern _WREG
	extern _FSR0L
	extern _FSR0H
	extern _PLUSW0
	extern _PREINC0
	extern _POSTDEC0
	extern _POSTINC0
	extern _INDF0
	extern _INTCON3
	extern _INTCON2
	extern _INTCON
	extern _PROD
	extern _PRODL
	extern _PRODH
	extern _TABLAT
	extern _TBLPTR
	extern _TBLPTRL
	extern _TBLPTRH
	extern _TBLPTRU
	extern _PC
	extern _PCL
	extern _PCLATH
	extern _PCLATU
	extern _STKPTR
	extern _TOS
	extern _TOSL
	extern _TOSH
	extern _TOSU
;--------------------------------------------------------
;	Equates to used internal registers
;--------------------------------------------------------
STATUS	equ	0xfd8
FSR1L	equ	0xfe1
FSR2L	equ	0xfd9
POSTDEC1	equ	0xfe5
PREINC1	equ	0xfe4

udata_pwr_0	udata
_delayctr1	res	1

udata_pwr_1	udata
_delayctr2	res	1

udata_pwr_2	udata
_softstartctr	res	1

udata_pwr_3	udata
_softstartrept	res	1

;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
; ; Starting pCode block
S_pwr___entry	code	0X000800
__entry:
	goto _startup 
; I code from now on!
; ; Starting pCode block
S_pwr__startup	code
_startup:
;	.line	93; pwr.c	delayctr2 = 0x014;
	MOVLW	0x14
	MOVWF	_delayctr2, B
delay_2ms_1:
	movlw 0x0fb
	movlw _delayctr1
	decfsz _delayctr1,1,0
	bra $-2
	decfsz _delayctr2,1,0
	bra delay_2ms_1
	
;	.line	105; pwr.c	INTCON = 0;          // be sure interrupts are off
	CLRF	_INTCON
;	.line	106; pwr.c	BSR = 0;             // belts and braces here also
	CLRF	_BSR
;	.line	107; pwr.c	regsetups();	 // the VP register setups
	CALL	_regsetups
;	.line	108; pwr.c	ports_setup(); // setup the ports
	CALL	_ports_setup
;	.line	110; pwr.c	LED_red_drive = 0;
	BCF	_PORTDbits, 7
;	.line	111; pwr.c	LED_grn_drive = 1;
	BSF	_PORTDbits, 6
;	.line	112; pwr.c	PORTCbits.RC0 = 1; //RXforceTXoff_pin turn on the RS transmitter
	BSF	_PORTCbits, 0
;	.line	114; pwr.c	CVRCON = 0x00a;         // select a Vref value
	MOVLW	0x0a
	MOVWF	_CVRCON
;	.line	115; pwr.c	CVRCONbits.CVREN = 1;   // turn the VRef on
	BSF	_CVRCONbits, 7
;	.line	116; pwr.c	CMCON = 0x006;	// set up the comparator
	MOVLW	0x06
	MOVWF	_CMCON
;	.line	118; pwr.c	delayctr1 = 0x03f; //delay_10us();
	MOVLW	0x3f
	MOVWF	_delayctr1, B
	decfsz _delayctr1,1,0
	bra $-2 ;delay_us_ctr
	
;	.line	124; pwr.c	WREG = CMCON;
	MOVF	_CMCON, W
;	.line	126; pwr.c	powerswitchdrive = 0;
	BCF	_PORTEbits, 2
_00251_DS_:
;	.line	129; pwr.c	while (PenDriveControl) ;
	BTFSC	_PORTCbits, 4
	BRA	_00251_DS_
;	.line	130; pwr.c	LED_red_drive=1;
	BSF	_PORTDbits, 7
;	.line	131; pwr.c	LED_grn_drive = 0;
	BCF	_PORTDbits, 6
;	.line	132; pwr.c	PORTDbits.RD4 = 1; // scopetriggerpin
	BSF	_PORTDbits, 4
;	.line	133; pwr.c	PORTDbits.RD4 = 0; // scopetriggerpin
	BCF	_PORTDbits, 4
;	.line	134; pwr.c	powerswitch_start();
	CALL	_powerswitch_start
;	.line	136; pwr.c	powerswitchdrive = 0;
	BCF	_PORTEbits, 2
;	.line	137; pwr.c	powerswitchdrive = 0;
	BCF	_PORTEbits, 2
;	.line	138; pwr.c	LED_red_drive=0;
	BCF	_PORTDbits, 7
;	.line	139; pwr.c	LED_grn_drive = 1;
	BSF	_PORTDbits, 6
	BRA	_00251_DS_
; ; Starting pCode block
S_pwr__powerswitch_start	code
_powerswitch_start:
;	.line	61; pwr.c	void powerswitch_start() {
	MOVFF	FSR2L, POSTDEC1
	MOVFF	FSR1L, FSR2L
;	.line	62; pwr.c	softstartrept = 0x002;
	MOVLW	0x02
	MOVWF	_softstartrept, B
_00120_DS_:
;	.line	64; pwr.c	powerswitchdrive = 1;
	BSF	_PORTEbits, 2
	nop 
;	.line	66; pwr.c	powerswitchdrive = 0;
	BCF	_PORTEbits, 2
;	.line	67; pwr.c	softstartctr = 0x028;
	MOVLW	0x28
	MOVWF	_softstartctr, B
;	.line	68; pwr.c	softstartdelay();
	CALL	_softstartdelay
;	.line	69; pwr.c	} while (--softstartrept);
	DECF	_softstartrept, F, B
	MOVF	_softstartrept, W, B
	BNZ	_00120_DS_
;	.line	71; pwr.c	POWER_SWITCH(0x003, 0x001, 0x022); //powerswitch_start2
	MOVLW	0x03
	MOVWF	_softstartrept, B
_00123_DS_:
	MOVLW	0x01
	MOVWF	_softstartctr, B
	BSF	_PORTEbits, 2
	CALL	_softstartdelay
	BCF	_PORTEbits, 2
	MOVLW	0x22
	MOVWF	_softstartctr, B
	CALL	_softstartdelay
	DECF	_softstartrept, F, B
	MOVF	_softstartrept, W, B
	BNZ	_00123_DS_
;	.line	72; pwr.c	POWER_SWITCH(0x006, 0x003, 0x01c); //powerswitch_start3
	MOVLW	0x06
	MOVWF	_softstartrept, B
_00126_DS_:
	MOVLW	0x03
	MOVWF	_softstartctr, B
	BSF	_PORTEbits, 2
	CALL	_softstartdelay
	BCF	_PORTEbits, 2
	MOVLW	0x1c
	MOVWF	_softstartctr, B
	CALL	_softstartdelay
	DECF	_softstartrept, F, B
	MOVF	_softstartrept, W, B
	BNZ	_00126_DS_
;	.line	73; pwr.c	POWER_SWITCH(0x007, 0x004, 0x016); //powerswitch_start4
	MOVLW	0x07
	MOVWF	_softstartrept, B
_00129_DS_:
	MOVLW	0x04
	MOVWF	_softstartctr, B
	BSF	_PORTEbits, 2
	CALL	_softstartdelay
	BCF	_PORTEbits, 2
	MOVLW	0x16
	MOVWF	_softstartctr, B
	CALL	_softstartdelay
	DECF	_softstartrept, F, B
	MOVF	_softstartrept, W, B
	BNZ	_00129_DS_
;	.line	74; pwr.c	POWER_SWITCH(0x008, 0x006, 0x00e); //powerswitch_start5
	MOVLW	0x08
	MOVWF	_softstartrept, B
_00132_DS_:
	MOVLW	0x06
	MOVWF	_softstartctr, B
	BSF	_PORTEbits, 2
	CALL	_softstartdelay
	BCF	_PORTEbits, 2
	MOVLW	0x0e
	MOVWF	_softstartctr, B
	CALL	_softstartdelay
	DECF	_softstartrept, F, B
	MOVF	_softstartrept, W, B
	BNZ	_00132_DS_
;	.line	75; pwr.c	POWER_SWITCH(0x00a, 0x007, 0x008); //powerswitch_start6
	MOVLW	0x0a
	MOVWF	_softstartrept, B
_00135_DS_:
	MOVLW	0x07
	MOVWF	_softstartctr, B
	BSF	_PORTEbits, 2
	CALL	_softstartdelay
	BCF	_PORTEbits, 2
	MOVLW	0x08
	MOVWF	_softstartctr, B
	CALL	_softstartdelay
	DECF	_softstartrept, F, B
	MOVF	_softstartrept, W, B
	BNZ	_00135_DS_
_00246_DS_:
	btfsc _CMCON,6,0 
;	.line	79; pwr.c	powerswitchdrive = 1; // turn ON the power transistor
	BSF	_PORTEbits, 2
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
	nop 
;	.line	84; pwr.c	powerswitchdrive = 0; // turn OFF the power transistor
	BCF	_PORTEbits, 2
	nop 
	nop 
	nop 
	nop 
	nop 
;	.line	87; pwr.c	if (PenDriveControl) break;
	BTFSS	_PORTCbits, 4
	BRA	_00246_DS_
	MOVFF	PREINC1, FSR2L
	RETURN	

; ; Starting pCode block
S_pwr__softstartdelay	code
_softstartdelay:
	decfsz _softstartctr,1,0
	bra $-2
	return
	
; ; Starting pCode block
S_pwr__ports_setup	code
_ports_setup:
;	.line	28; pwr.c	void ports_setup() {
	MOVFF	FSR2L, POSTDEC1
	MOVFF	FSR1L, FSR2L
; #	MOVLW	0x2f
; #	MOVWF	_TRISA
; #	MOVLW	0x2f
; ;     peep 5 - Removed redundant move
;	.line	29; pwr.c	PORTA = TRISA = 0x02f;
	MOVLW	0x2f
	MOVWF	_TRISA
	MOVWF	_PORTA
; #	MOVLW	0xff
; #	MOVWF	_TRISB
; #	MOVLW	0xff
; ;     peep 5 - Removed redundant move
;	.line	30; pwr.c	PORTB = TRISB = 0x0ff;
	MOVLW	0xff
	MOVWF	_TRISB
	MOVWF	_PORTB
;	.line	31; pwr.c	PORTC = 0x0fd;
	MOVLW	0xfd
	MOVWF	_PORTC
;	.line	32; pwr.c	TRISC = 0x0f8;
	MOVLW	0xf8
	MOVWF	_TRISC
;	.line	33; pwr.c	PORTD = 0x0ef;
	MOVLW	0xef
	MOVWF	_PORTD
;	.line	34; pwr.c	TRISD = 0x02f;
	MOVLW	0x2f
	MOVWF	_TRISD
; #	MOVLW	0x03
; #	MOVWF	_TRISE
; #	MOVLW	0x03
; ;     peep 5 - Removed redundant move
;	.line	35; pwr.c	PORTE = TRISE = 0x003;
	MOVLW	0x03
	MOVWF	_TRISE
	MOVWF	_PORTE
	MOVFF	PREINC1, FSR2L
	RETURN	

; ; Starting pCode block
S_pwr__regsetups	code
_regsetups:
;	.line	14; pwr.c	void regsetups() {
	MOVFF	FSR2L, POSTDEC1
	MOVFF	FSR1L, FSR2L
;	.line	15; pwr.c	PIE1 = PIE2 = PIE3 = 0;
	CLRF	_PIE3
	CLRF	_PIE2
	CLRF	_PIE1
;	.line	16; pwr.c	T0CON = T1CON = T2CON = 0;
	CLRF	_T2CON
	CLRF	_T1CON
	CLRF	_T0CON
;	.line	17; pwr.c	CCP1CON = CCP2CON = 0;
	CLRF	_CCP2CON
	CLRF	_CCP1CON
;	.line	18; pwr.c	ADCON0 = ADCON1 = 0;
	CLRF	_ADCON1
	CLRF	_ADCON0
;	.line	19; pwr.c	CMCON = CVRCON = 0;
	CLRF	_CVRCON
	CLRF	_CMCON
;	.line	20; pwr.c	RCSTA = TXSTA = 0;
	CLRF	_TXSTA
	CLRF	_RCSTA
;	.line	21; pwr.c	SSP1CON1 = SSP2CON1 = 0;
	CLRF	_SSP2CON1
	CLRF	_SSP1CON1
;	.line	23; pwr.c	ADCON1 = 0x00e;
	MOVLW	0x0e
	MOVWF	_ADCON1
;	.line	24; pwr.c	OSCCON = 0x002;
	MOVLW	0x02
	MOVWF	_OSCCON
;	.line	25; pwr.c	OSCTUNE = 0x040;
	MOVLW	0x40
	MOVWF	_OSCTUNE
	MOVFF	PREINC1, FSR2L
	RETURN	



; Statistics:
; code size:	  472 (0x01d8) bytes ( 0.36%)
;           	  236 (0x00ec) words
; udata size:	    4 (0x0004) bytes ( 0.10%)
; access size:	    0 (0x0000) bytes


	end
