#include <pic18fregs.h>

#define			fa			1,0
#ifdef NEWBRD
#define			powerswitchdrive	PORTBbits.RB0 //PORTCbits.RC7
#define			LED_RED(a)              PORTBbits.RB5 = a
#define			LED_GRN(a)
#else
#define			powerswitchdrive	PORTEbits.RE2
#define			LED_RED(a)              PORTDbits.RD7 = a
#define			LED_GRN(a)		PORTDbits.RD6 = a
#endif
#define			powertrip		_CMCON,6,0
#define			Powertrip		CMCONbits.C1OUT_CMCON
#define			PenDriveControl		PORTBbits.RB1 //PORTCbits.RC4
#define                 TxRS232                 PORTCbits.RC0

unsigned char delayctr1, delayctr2, softstartrept, softstartctr, tmp;

void regsetups() {
    PIE1 = PIE2 = PIE3 = 0;
    T0CON = T1CON = T2CON = 0;
    CCP1CON = CCP2CON = 0;
    ADCON0 = ADCON1 = 0;
    CMCON = CVRCON = 0;
    RCSTA = TXSTA = 0;
#ifndef NEWBRD
    SSP1CON1 = SSP2CON1 = 0;
#endif
    // i2c setup
    SSP1CON1 = 0x06;        // i2c slave mode 7-bit
    //SSP1CON1 = 0x0e;        // I2C Slave mode, 7-bit address with Start and Stop bit interrupts enabled
    SSP1CON1bits.SSPEN = 1; // Enable Master SYnchronous serial port enable
    SSP1CON1bits.CKP = 1;   // SCK0 release control bit
    SSP1CON2bits.SEN = 1;
    SSP1ADD = 0x1c; //0x0e;         // Address on i2c bus
    SSP1STAT = 0;
    PIR1bits.SSPIF = 0; // clear the irq
    // end i2c setup

    ADCON1 = 0x00e;
    OSCCON = 0x002;
    OSCTUNE = 0x040;
}

void ports_setup() {
    PORTA = TRISA = 0x02f;
    PORTB = TRISB = 0xde; //0x0df;
    PORTC = 0x0fd;
    TRISC = 0xfe; //0x0f8;
#ifndef NEWBRD
    PORTD = 0x0ef;
    TRISD = 0x02f;
    PORTE = TRISE = 0x003;
#endif
}

void softstartdelay() __naked {
    __asm 
        decfsz _softstartctr,fa 
        bra	$-2 
        return 
    __endasm;
}

#define Nop5X() { Nop();Nop();Nop();Nop();Nop(); }
#define Nop10X() { Nop5X(); Nop5X(); }

#define POWER_SWITCH(LOOP, WIDTH, DELAY) { \
    softstartrept = LOOP; \
    do { \
        softstartctr = WIDTH; \
        powerswitchdrive = 1; \
        softstartdelay(); /* width */ \
        powerswitchdrive = 0; \
        softstartctr = DELAY; \
        softstartdelay(); /* delay */ \
    } while (--softstartrept); \
}

void powerswitch_start() {
    softstartrept = 0x002;
    do { // powerswitch_start1:	
        powerswitchdrive = 1;
        Nop();
        powerswitchdrive = 0;
        softstartctr = 0x028;
        softstartdelay();
    } while (--softstartrept);

    POWER_SWITCH(0x003, 0x001, 0x022); //powerswitch_start2
    POWER_SWITCH(0x006, 0x003, 0x01c); //powerswitch_start3
    POWER_SWITCH(0x007, 0x004, 0x016); //powerswitch_start4
    POWER_SWITCH(0x008, 0x006, 0x00e); //powerswitch_start5
    POWER_SWITCH(0x00a, 0x007, 0x008); //powerswitch_start6
    
    
        do {
        if (Powertrip) { // __asm btfsc powertrip __endasm;
            powerswitchdrive = 1; // turn ON the power transistor
            Nop10X();
    		Nop10X();
            Nop10X();
 	//	Nop10X();
 //		Nop5X();
 		Nop5X();
           Nop10X();
           Nop10X();
           Nop10X();
 //          Nop10X();
  
            powerswitchdrive = 0; // turn OFF the power transistor
//			Nop5X();
           Nop();  
           Nop();
            Nop();
            Nop();
//          Nop();
//          Nop();
        }
    } while (0 == PenDriveControl);
}
    
  

#define STAT_S      (0x08)
#define STAT_BF     (0x01)
#define STAT_DA     (0x20)
#define STAT_RW     (0x04)
void ssp_handler() {
/* ---------------------------------------------------------------------
 *	The I2C code below checks for 5 states:
 * ---------------------------------------------------------------------
 *      SSP1STAT bits : SMP | CKE | D(1)/A | P | S | R(1)/W | UA | BF
*/
    if (SSP1CON1bits.SSPOV) SSP1CON1bits.SSPOV = 0;

    switch (SSPSTAT & (STAT_S|STAT_DA|STAT_RW|STAT_BF)) {
    case (STAT_S|STAT_BF): // write op, last byte address
        SSP1BUF; // address ??
        SSP1CON1bits.CKP = 1;
        break;
    case (STAT_S|STAT_DA|STAT_BF): // write op, last byte data
        TxRS232 = SSP1BUF & 0x01; // Turn on/off rs232 TX
        SSP1CON1bits.CKP = 1;
        break;
    case (STAT_S|STAT_RW|STAT_BF): { // read op, last byte address
        SSP1BUF; // read the i2c buffer, not used
        TRISBbits.TRISB5 = 1;
        tmp = PORTBbits.RB5;
        TRISBbits.TRISB5 = 0;
        while (SSP1STATbits.BF) ;
        do {
            SSP1CON1bits.WCOL = 0;
            SSP1BUF = tmp;
        } while (SSP1CON1bits.WCOL);
        SSP1CON1bits.CKP = 1;
        break;
    }
    case (STAT_S|STAT_RW|STAT_DA): // read op, last byte data
        //WriteI2C(0xff);
        break;
    case (STAT_S|STAT_DA): // Slave I2C reset by NACK from master
        // do nothing
        //break;
    default: // i2c error
        //SSP1CON1bits.CKP = 1;
        // Something went wrong!
        break;
    }
}

void main() __naked {
    // let the PLL settle for 2ms
    delayctr2 = 0x014;
    __asm
delay_2ms_1:	movlw	0x0fb
	    	movlw	_delayctr1
//delay_2ms_0:		
                decfsz	_delayctr1,fa
	        bra	$-2
    	        decfsz	_delayctr2,fa
		bra	delay_2ms_1
    __endasm;

    // the basic ../startup package
    INTCON = 0;          // be sure interrupts are off
    BSR = 0;             // belts and braces here also
    regsetups();	 // the VP register setups

    ports_setup(); // setup the ports
    LED_RED(0);
    LED_GRN(1);

    TxRS232 = 1; //RXforceTXoff_pin turn on the RS transmitter

    CVRCON = 0x00a;         // select a Vref value Was A=12.4V, 9=11.9V, 8=11.2V
    CVRCONbits.CVREN = 1;   // turn the VRef on
    CMCON = 0x006;	// set up the comparator

    delayctr1 = 0x03f; //delay_10us();
    __asm
        decfsz	_delayctr1,fa
	bra	$-2 ;delay_us_ctr
    __endasm;

    WREG = CMCON;

    powerswitchdrive = 0;

    while (1) {
        while (PenDriveControl) {
            // i2c code
            if (SSP1CON1bits.SSPOV || PIR1bits.SSPIF) {
                PIR1bits.SSPIF = 0; // clear the irq
                ssp_handler(); // i2c irq occured
            }
            // end i2c
        }
        SSP1CON1bits.SSPEN = 0; // MSSP disable
        LED_RED(1);
        LED_GRN(0);
        //PORTDbits.RD4 = 1; // scopetriggerpin
        //PORTDbits.RD4 = 0; // scopetriggerpin
        powerswitch_start();
//	;;;;		call	powerswitch_loop
        powerswitchdrive = 0;
        powerswitchdrive = 0;
        LED_RED(0);
        LED_GRN(1);
        SSP1CON1bits.SSPEN = 1; // MSSP enable
    }
}

void _entry (void) __naked __interrupt 0 {
    __asm goto _main __endasm;
}
