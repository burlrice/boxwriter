/****************************************************************************/
/*                                                                          */
/*  Module:        jbiport.h                                                */
/*                                                                          */
/*                 Copyright (C) Altera Corporation 2000-2001               */
/*                                                                          */
/*  Description:   Defines porting macros                                   */
/*                                                                          */
/****************************************************************************/

#ifndef INC_JBIPORT_H
#define INC_JBIPORT_H

/*
*  PORT defines the target platform: DOS, WINDOWS, UNIX, or EMBEDDED
*
*  PORT = DOS      means a 16-bit DOS console-mode application
*
*  PORT = WINDOWS  means a 32-bit WIN32 console-mode application for
*                  Windows 95, 98, 2000, ME or NT.  On NT this will use the
*                  DeviceIoControl() API to access the Parallel Port.
*
*  PORT = UNIX     means any UNIX system.  BitBlaster access is support via
*                  the standard ANSI system calls open(), read(), write().
*                  The ByteBlaster is not supported.
*
*  PORT = EMBEDDED means all DOS, WINDOWS, and UNIX code is excluded. 
*                  Remaining code supports 16 and 32-bit compilers. 
*                  Additional porting steps may be necessary. See readme 
*                  file for more details.
*/

#define DOS      2
#define WINDOWS  3
#define UNIX     4
#define EMBEDDED 5

#ifndef PORT
/* change this line to build a different port */
#define PORT WINDOWS
#endif

////////////////////////////////////////////////////////////////////////////////
// burl

typedef void				*HANDLE;
typedef unsigned long       DWORD;
typedef int                 BOOL;
typedef unsigned char       BYTE;
typedef unsigned short      WORD;
typedef float               FLOAT;
typedef FLOAT               *PFLOAT;
typedef BOOL				*PBOOL;
typedef BOOL				 *LPBOOL;
typedef BYTE				*PBYTE;
typedef BYTE				 *LPBYTE;
typedef int					*PINT;
typedef int					 *LPINT;
typedef WORD				*PWORD;
typedef WORD				 *LPWORD;
typedef long				 *LPLONG;
typedef DWORD				*PDWORD;
typedef DWORD				 *LPDWORD;
typedef void				 *LPVOID;

void fj_open (const char * pszFile);
void fj_close ();
int fj_write (int handle, const void *buffer, unsigned int count);
int fj_read (int handle, void *buffer, unsigned int count);

BOOL fj_DeviceIoControl(
  HANDLE hDevice,              // handle to device
  DWORD dwIoControlCode,       // operation control code
  LPVOID lpInBuffer,           // input data buffer
  DWORD nInBufferSize,         // size of input data buffer
  LPVOID lpOutBuffer,          // output data buffer
  DWORD nOutBufferSize,        // size of output data buffer
  LPDWORD lpBytesReturned,     // byte count
  LPVOID lpOverlapped    // overlapped information
);
////////////////////////////////////////////////////////////////////////////////

#endif /* INC_JBIPORT_H */
