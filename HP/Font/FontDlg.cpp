// FontDlg.cpp : implementation file
//

#include "stdafx.h"
#include <afxdlgs.h>
#include "Font.h"
#include "FontDlg.h"
#include "TemplExt.h"
#include "Debug.h"
#include "Database.h"
#include "Parse.h"
#include "ximage.h"
#include "AboutDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;

CString ToBinStr (BYTE b)
{
	CString str;

	for (int i = 0; i < 8; i++) {
		int nMask = 1 << (8 - i - 1);

		str += (b & nMask) ? '1' : '0';
	}

	str.Insert (4, ' ');

	return str;
}

bool IsEOL (BYTE b)
{
	return ((b & bEndOfLineMask) == bEndOfLineMask);
}

bool IsEOC (BYTE b)
{
	return ((b & bEndOfCharMask) == bEndOfCharMask);
}

bool IsLiteralCmd (BYTE b)
{
	return BITCLEAR (b, 7);
}

bool IsEngineCmd (BYTE b)
{
	return BITSET (b, 7) && BITSET (b, 6) && BITSET (b, 5) && BITSET (b, 4) &&
		BITCLEAR (b, 3);
}

bool IsTagCmd (BYTE b)
{
	return 
		BITSET (b, 7) && BITSET (b, 6) && BITSET (b, 5) && 
		BITCLEAR (b, 4);
}

bool IsRepeatCmd (BYTE b)
{
	return 
		BITSET (b, 7) && BITSET (b, 6) && 
		BITCLEAR (b, 5);
}

bool IsCompressionCmd (BYTE b)
{
	return 
		BITSET (b, 7) && 
		BITCLEAR (b, 6);
}

static bool IsUnicode (const CString & strFile)
{
	bool bUnicode = false;

	if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
		WORD w = 0;

		fread (&w, sizeof (w), 1, fp); 
		bUnicode = (w == 0xFEFF) ? true : false;
		fclose (fp);
	}

	return bUnicode;
}

/////////////////////////////////////////////////////////////////////////////
// CFontDlg dialog

CFontDlg::CFontDlg(CWnd* pParent /*=NULL*/)
:	m_pFont (NULL),
	m_pCharsetFont (NULL),
	m_hAccel (NULL),
	CDialog(CFontDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFontDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFontDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFontDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CFontDlg, CDialog)
	//{{AFX_MSG_MAP(CFontDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(CB_FONT, OnSelchangeFont)
	ON_COMMAND(ID_FILE_EXIT, OnFileExit)
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_BN_CLICKED(BTN_GENERATE, OnGenerate)
	ON_CBN_SELCHANGE(CB_WEIGHT, OnSelchangeWeight)
	ON_EN_CHANGE(TXT_HEIGHT, OnChangeHeight)
	ON_EN_CHANGE(TXT_WIDTH, OnChangeWidth)
	ON_BN_CLICKED(CHK_ITALIC, OnItalic)
	ON_BN_CLICKED(CHK_UNDERLINE, OnUnderline)
	ON_EN_CHANGE(TXT_CHARSET, OnChangeCharset)
	ON_BN_CLICKED(BTN_DEFAULT, OnBrowseCharset)
	ON_COMMAND(ID_HELP_ABOUT, OnHelpAbout)
	ON_BN_CLICKED(BTN_BROWSEINPUT, OnBrowseinput)
	ON_BN_CLICKED(BTN_PARSE, OnParse)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFontDlg message handlers

int CALLBACK EnumFontFamExProc(ENUMLOGFONTEX *lpelfe,
							   NEWTEXTMETRICEX *lpntme, 
							   int FontType, LPARAM lParam)
{
	CStringArray * p = (CStringArray *)lParam;
	ASSERT (p);

	if (FontType & TRUETYPE_FONTTYPE) {
		CStringArray & v = * p;
		CString strFont = lpelfe->elfLogFont.lfFaceName;

		for (int i = 0; i < v.GetSize (); i++) 
			if (v [i] == strFont)
				return 1;

		p->Add (strFont);
	}

	return 1;
}

BOOL CFontDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	{
		CStringArray v;
		LOGFONT lf;
		HDC hDC = ::GetDC (NULL);
		CComboBox * pFont = (CComboBox *)GetDlgItem (CB_FONT);
		CString strLastFont = theApp.GetProfileString (_T ("font"), _T ("name"), _T ("MK Arial"));

		ASSERT (pFont);

		memset (&lf, 0, sizeof (LOGFONT));
		lf.lfCharSet = DEFAULT_CHARSET;
		lf.lfFaceName [0] =  '\0';
		lf.lfPitchAndFamily = 0;

		::EnumFontFamiliesEx (hDC, &lf, (FONTENUMPROC)EnumFontFamExProc, (LPARAM)&v, 0);
		::ReleaseDC (NULL, hDC);

		for (int i = 0; i < v.GetSize (); i++) {
			int nIndex = pFont->AddString (v [i]);

			if (!v [i].CompareNoCase (strLastFont))
				pFont->SetCurSel (nIndex);
		}
	}
	
	{
		struct 
		{
			LPCTSTR m_lpsz;
			int	m_n;
		}
		static const map [] = {
			{ _T ("FW_DONTCARE"),	0,		},
//			{ _T ("FW_THIN"),		100, 	},
//			{ _T ("FW_EXTRALIGHT"),	200,	},
//			{ _T ("FW_ULTRALIGHT"),	200,	},
//			{ _T ("FW_LIGHT"),		300,	},
//			{ _T ("FW_NORMAL"),		400,	},
//			{ _T ("FW_REGULAR"),	400,	},
//			{ _T ("FW_MEDIUM"),		500,	},
//			{ _T ("FW_SEMIBOLD"),	600,	},
//			{ _T ("FW_DEMIBOLD"),	600,	},
			{ _T ("FW_BOLD"),		700,	},
//			{ _T ("FW_EXTRABOLD"),	800,	},
//			{ _T ("FW_ULTRABOLD"),	800,	},
//			{ _T ("FW_HEAVY"),		900,	},
//			{ _T ("FW_BLACK"),		900,	},
		};
		CComboBox * p = (CComboBox *)GetDlgItem (CB_WEIGHT);
		CString strLast = theApp.GetProfileString (_T ("font"), _T ("weight"), _T ("FW_DONTCARE (0)"));

		ASSERT (p); 

		for (int i = 0; i < ARRAYSIZE (map); i++) {
			CString str;

			str.Format (_T ("%s (%d)"), map [i].m_lpsz, map [i].m_n);
			int nIndex = p->AddString (str);

			p->SetItemData (nIndex, map [i].m_n);

			if (!strLast.CompareNoCase (str))
				p->SetCurSel (nIndex);
		}
	}

	SetDlgItemText (TXT_OUTPATH, theApp.GetProfileString (_T ("font"), _T ("dir"), GetHomeDir ()));
	SetDlgItemText (TXT_HEIGHT, theApp.GetProfileString (_T ("font"), _T ("height"), _T ("64")));
	SetDlgItemText (TXT_WIDTH, theApp.GetProfileString (_T ("font"), _T ("width"), _T ("0")));
	SetDlgItemText (TXT_CHARSET, theApp.GetProfileString (_T ("font"), _T ("charset"), _T ("charset.txt")));

	if (CButton * p = (CButton *)GetDlgItem (CHK_ITALIC)) 
		p->SetCheck (theApp.GetProfileInt (_T ("font"), _T ("italic"), 0));

	if (CButton * p = (CButton *)GetDlgItem (CHK_UNDERLINE)) 
		p->SetCheck (theApp.GetProfileInt (_T ("font"), _T ("underline"), 0));

	UpdateUI ();

	{ 
		CString str;
	
		GetDlgItemText (TXT_FILENAME, str);
		SetDlgItemText (TXT_INPUT, theApp.GetProfileString (_T ("input"), _T ("filename"), str));
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CFontDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		OnHelpAbout ();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CFontDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CFontDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CFontDlg::OnOK()
{

}

void CFontDlg::OnCancel()
{
	
}

void CFontDlg::OnClose() 
{
	CDialog::OnClose();
	EndDialog (1);
}

void CFontDlg::OnFileExit() 
{
	OnClose ();	
}

void CFontDlg::OnSelchangeFont() 
{
	CComboBox * pFont = (CComboBox *)GetDlgItem (CB_FONT);
	CString str;

	ASSERT (pFont);
	pFont->GetLBText (pFont->GetCurSel (), str);
	theApp.WriteProfileString (_T ("font"), _T ("name"), str);
	UpdateUI ();
}

void CFontDlg::OnBrowse() 
{
	CString strResult;
	LPMALLOC lpMalloc;  
	TCHAR szBuffer[_MAX_PATH];
	BROWSEINFO browseInfo;

	if (::SHGetMalloc(&lpMalloc) != NOERROR)
		return;

	browseInfo.hwndOwner = m_hWnd;
	// set root at Desktop
	browseInfo.pidlRoot = NULL; 
	browseInfo.pszDisplayName = _T ("Browse");
	browseInfo.lpszTitle = _T ("Font file output folder");
	browseInfo.ulFlags = BIF_RETURNONLYFSDIRS;
	browseInfo.lpfn = NULL;
	browseInfo.lParam = 0;

	LPITEMIDLIST lpItemIDList;

	if ((lpItemIDList = ::SHBrowseForFolder(&browseInfo)) != NULL) {
		if (::SHGetPathFromIDList(lpItemIDList, szBuffer)) 
			if (szBuffer[0] == '\0') 
				return;

		strResult = szBuffer;
		SetDlgItemText (TXT_OUTPATH, strResult);
		theApp.WriteProfileString (_T ("font"), _T ("dir"), strResult);

		lpMalloc->Free(lpItemIDList);
		lpMalloc->Release();      
	}
}

void CFontDlg::UpdateUI ()
{
	CComboBox * pFont = (CComboBox *)GetDlgItem (CB_FONT);
	CComboBox * pWeight = (CComboBox *)GetDlgItem (CB_WEIGHT);
	CString str, strName;
	bool bItalic = false;
	bool bUnderline = false;

	ASSERT (pFont);
	ASSERT (pWeight);
	
	if (pFont->GetCurSel () != CB_ERR)
		pFont->GetLBText (pFont->GetCurSel (), strName);
	
	int nWeight = pWeight->GetItemData (pWeight->GetCurSel ());
	int nWidth	= GetDlgItemInt (TXT_WIDTH);
	int nHeight = GetDlgItemInt (TXT_HEIGHT);

	if (CButton * p = (CButton *)GetDlgItem (CHK_ITALIC)) 
		bItalic = p->GetCheck () ? true : false;

	if (CButton * p = (CButton *)GetDlgItem (CHK_UNDERLINE)) 
		bUnderline = p->GetCheck () ? true : false;

	Free ();

	if (CWnd * p = GetDlgItem (IDC_PREVIEW)) {
		m_pFont = new CFont ();
		m_pFont->CreateFont (nHeight, nWidth, 0, 0, nWeight, bItalic, bUnderline, FALSE,
			DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_CHARACTER_PRECIS,
			DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, strName);
		p->SetFont (m_pFont);
		p->SetWindowText (_T ("AaBbYyZz"));
	}

	if (CWnd * p = GetDlgItem (TXT_CHARSET)) {
		int nHeight = strName.Find (_T ("MK ")) != -1 ? 8 : 16;
		
		m_pCharsetFont = new CFont ();
		m_pCharsetFont->CreateFont (nHeight, nWidth, 0, 0, nWeight, bItalic, bUnderline, FALSE,
			DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_CHARACTER_PRECIS,
			DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, strName);

		//p->SetFont (m_pCharsetFont);
	}

	//strName.Replace (_T (" "), _T (""));
	str.Format (_T ("%s %d %d %d"), 
		strName, nHeight, nWidth, nWeight);

	str += bItalic		? _T (" i") : _T ("");
	str += bUnderline	? _T (" u") : _T ("");
	str += _T (".jfc");

	SetDlgItemText (TXT_FILENAME, str);
}

void CFontDlg::OnSelchangeWeight() 
{
	CComboBox * p = (CComboBox *)GetDlgItem (CB_WEIGHT);
	CString str;

	ASSERT (p);
	p->GetLBText (p->GetCurSel (), str);
	theApp.WriteProfileString (_T ("font"), _T ("weight"), str);
	UpdateUI ();
}

void CFontDlg::OnChangeHeight() 
{
	CString str;

	GetDlgItemText (TXT_HEIGHT, str);
	theApp.WriteProfileString (_T ("font"), _T ("height"), str);
	UpdateUI ();
}

void CFontDlg::OnChangeWidth() 
{	
	CString str;

	GetDlgItemText (TXT_WIDTH, str);
	theApp.WriteProfileString (_T ("font"), _T ("width"), str);
	UpdateUI ();
}

void CFontDlg::OnItalic() 
{
	if (CButton * p = (CButton *)GetDlgItem (CHK_ITALIC)) 
		theApp.WriteProfileInt (_T ("font"), _T ("italic"), p->GetCheck ());

	UpdateUI ();
}

void CFontDlg::OnUnderline() 
{
	if (CButton * p = (CButton *)GetDlgItem (CHK_UNDERLINE)) 
		theApp.WriteProfileInt (_T ("font"), _T ("underline"), p->GetCheck ());

	UpdateUI ();
}

void CFontDlg::OnChangeCharset() 
{
	CString str;

	GetDlgItemText (TXT_CHARSET, str);
	theApp.WriteProfileString (_T ("font"), _T ("charset"), str);

	UpdateUI ();
}

void CFontDlg::Free ()
{
	if (m_pFont) {
		delete m_pFont;
		m_pFont = NULL;
	}

	if (m_pCharsetFont) {
		delete m_pCharsetFont;
		m_pCharsetFont = NULL;
	}
}

BOOL CFontDlg::DestroyWindow() 
{
	Free ();
	
	return CDialog::DestroyWindow();
}


void CFontDlg::OnGenerate() 
{
	BeginWaitCursor ();

	CString strCharSet = _T ("ABC");
	CString strInputFile;
	CString strFile;
	CAlphabet alphabet;

	GetDlgItemText (TXT_CHARSET, strInputFile);

	if (::GetFileAttributes (strInputFile) == -1) {
		if (FILE * fp = _tfopen (_T ("input.txt"), _T ("wb"))) {
			CString str;
			WORD w = 0xFEFF;

			strInputFile = _T ("input.txt");
			SetDlgItemText (TXT_CHARSET, strInputFile);
			OnChangeCharset ();

			//fwrite (&w, sizeof (w), 1, fp); // Unicode header

			for (TCHAR c = ' '; c <= '~'; c++)
				str += c;

			fwrite ((LPVOID)(LPCTSTR)str, str.GetLength () * sizeof (TCHAR), 1, fp);
			fclose (fp);
		}
	}

	{
		FILE * fp = NULL;

		if (IsUnicode (strInputFile)) 
			fp = _tfopen (strInputFile, _T ("rb"));
		else
			fp = _tfopen (strInputFile, _T ("r"));

		if (fp) {
			TCHAR c = EOF; 
			bool bMore;

			strCharSet.Empty ();

			do {
				c = _fgettc (fp);
				bMore = c != EOF && c != WEOF;
					
				if (bMore)
					strCharSet += c;
			}
			while (bMore);

			TRACEF (strCharSet);
			fclose (fp);
		}
	}

	{
		CPrintDialog dlg (FALSE);
		CString strDocName;
		CString strOutput;

		GetDlgItemText (TXT_FILENAME, strDocName);
		GetDlgItemText (TXT_OUTPATH, strOutput);

		#ifdef _DEBUG
		strOutput = "C:\\Temp\\Debug";
		#endif

		if (!strDocName.Replace (_T (".jfc"), _T (".prn")))
			strDocName += _T (".jfc");

		strOutput += _T ("\\") + strDocName;
		TRACEF (strOutput);

		dlg.m_pd.Flags |= PD_PRINTTOFILE;

		if (!theApp.GetPrinterDeviceDefaults (&dlg.m_pd))
			if (theApp.DoPrintDialog (&dlg) != IDOK)
				return;

		HDC hDC = dlg.CreatePrinterDC ();
		ASSERT (hDC);

		CDC & dc = * CDC::FromHandle (hDC);
		DOCINFO doc;

		memset (&doc, 0, sizeof (doc));
		doc.cbSize = sizeof (doc);
		doc.lpszDocName		= strDocName;
		doc.lpszOutput		= strOutput;

		VERIFY (dc.StartDoc (&doc) > 0);
		VERIFY (dc.StartPage () > 0);

		{
			CString str = strCharSet;
			CRect rc (0, 0, 0, 0);
			UINT nFormat = DT_NOPREFIX;

			CFont * pFont = dc.SelectObject (m_pFont);
			dc.DrawText (str, &rc, DT_CALCRECT | nFormat);
			dc.TextOut (0, 0, str);
			dc.SelectObject (pFont);
		}

		dc.EndPage ();
		dc.EndDoc ();

		::DeleteDC (hDC);
	}

	{
		CString str;
	
		GetDlgItemText (TXT_OUTPATH, str);
		strFile += str;
		strFile += _T ("\\");

		GetDlgItemText (TXT_FILENAME, str);
		strFile += str;
	}

	ASSERT (m_pFont);
	alphabet.Build (strCharSet, * m_pFont);
	alphabet.Save (strFile);

	EndWaitCursor ();
}

////////////////////////////////////////////////////////////////////////////////


void CFontDlg::OnBrowseCharset() 
{
	CString strFile;

	GetDlgItemText (TXT_CHARSET, strFile);

	CFileDialog dlg (TRUE, _T ("*.*"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("All files (*.*)|*.*||"), this);
	
	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();
		SetDlgItemText (TXT_CHARSET, strFile);
		OnChangeCharset ();
	}
}

BOOL CFontDlg::PreTranslateMessage(MSG* pMsg) 
{
	if (!m_hAccel)
		VERIFY (m_hAccel = ::LoadAccelerators (::AfxGetInstanceHandle (), MAKEINTRESOURCE (IDA_MAIN)));

	if (::TranslateAccelerator (m_hWnd, m_hAccel, pMsg))
		return TRUE;
	
	return CDialog::PreTranslateMessage(pMsg);
}

void CFontDlg::OnHelpAbout() 
{
	CStringArray vCmdLine;

//	FoxjetDatabase::TokenizeCmdLine (m_strFlags, vCmdLine);

	FoxjetCommon::CAboutDlg dlg (theApp.m_ver, vCmdLine, _T ("About font generator"));

	dlg.DoModal (); 
}

void CFontDlg::OnBrowseinput() 
{
	CString strFile;

	GetDlgItemText (TXT_INPUT, strFile);

	CFileDialog dlg (TRUE, _T ("*.*"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("Jetmark font files (*.jfc)|*.jfc|All files (*.*)|*.*||"), this);
	
	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();
		SetDlgItemText (TXT_INPUT, strFile);
		theApp.WriteProfileString (_T ("input"), _T ("filename"), strFile);
	}
}

void CFontDlg::OnParse() 
{
	CString strFile;
	CString strOutDir;
	CAlphabet alphabet;

	BeginWaitCursor ();
	GetDlgItemText (TXT_INPUT, strFile);
	GetDlgItemText (TXT_OUTPATH, strOutDir);

	if (alphabet.Open (strFile)) {
		TCHAR sz [_MAX_FNAME + 1] = { 0 };

		::GetFileTitle (strFile, sz, ARRAYSIZE (sz) - 1);
		CString strTitle = sz;

		strTitle.Replace (_T (".jfc"), _T (""));
		strOutDir += _T ("\\") + strTitle;

		if (::GetFileAttributes (strOutDir) == -1)
			VERIFY (::CreateDirectory (strOutDir, NULL));

		for (int i = 0; i < alphabet.m_v.GetSize (); i++) {
			CString str;

			str.Format (_T ("%s\\%03d.bmp"), strOutDir, alphabet.m_v [i].m_c);
			SAVEBITMAP (alphabet.m_v [i].m_img.MakeBitmap (), str);
		}

		CString str;

		str.Format (_T ("Saved %d bitmaps to: %s"), 
			alphabet.m_v.GetSize (), strOutDir);

		MessageBox (str);
	}
	else
		MessageBox (_T ("File not found: ") + strFile);

	EndWaitCursor ();
}
