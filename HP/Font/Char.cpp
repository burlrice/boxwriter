#include "stdafx.h"
#include "Font.h"
#include "FontDlg.h"
#include "TemplExt.h"
#include "Debug.h"
#include "Database.h"
#include "Parse.h"
#include "ximage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;

////////////////////////////////////////////////////////////////////////

const int CChar::m_nHeaderLen = 6;

CChar::CChar (TCHAR c) 
:	m_c (c) 
{ 
}

CChar::CChar (const CChar & rhs)
:	m_c (rhs.m_c),
	m_vStrokes (rhs.m_vStrokes),
	m_vIndex (rhs.m_vIndex)
{
}
	
CChar & CChar::operator = (const CChar & rhs)
{
	if (this != &rhs) {
		m_c			= rhs.m_c;
		m_vStrokes	= rhs.m_vStrokes;
		m_vIndex	= rhs.m_vIndex;
	}

	return * this;
}

CChar::~CChar ()
{
}

int CChar::GetIndexBase () 
{
	ASSERT ((m_nHeaderLen % 2) == 0);

	return (int)(m_nHeaderLen / 2);
}

CString CChar::ToString () const
{
	CString str;

	for (int i = 0; i < m_vIndex.GetSize (); i++) {
		CString strWord;

		strWord.Format (_T ("%04X "), m_vIndex [i]);
		str += strWord;

		if (i && (i % 16) == 0)
			str += '\n';
	}

	return str;
}

int CChar::Count (const CString & str, TCHAR c)
{
	const int nMax = 49;//7 * 0x0F;
	int nCount = 0;

	for (int i = 0; i < min (str.GetLength (), nMax); i++) {
		if (c == str [i])
			nCount++;
		else
			break;
	}

	return nCount;
}

void CChar::Compress (const CString & strInput, CStroke & v)
{
	CString strBits (strInput);

	while (strBits.GetLength ()) {
		const TCHAR c = strBits [0];
		const int n = Count (strBits, c);
		BYTE b = 0x00;

		if (n > 7) {
			int nRepeat = (int)floor((double)n / 7.0);
			b = bCompressionMask;
			strBits.Delete (0, nRepeat * 7);

			ASSERT ((nRepeat & 0x0F) == nRepeat);

			b |= !strBits.GetLength ()	? (1 << 5) : 0x00;
			b |= c == 'X'				? (1 << 4) : 0x00;
			b |= (nRepeat & 0x0F);

			ASSERT (IsCompressionCmd (b));
		}
		else {
			const int nBits = min (strBits.GetLength (), 7);

			for (int i = 0; i < nBits; i++) 
				b |= ((strBits [i] == 'X') ? (1 << i) : 0);

			strBits.Delete (0, nBits);

			ASSERT (IsLiteralCmd (b));
		}

		//TRACEF (ToBinStr (b));
		v.Add (b);
	}

	v.Add (0xe1);
}

void CChar::BuildImage (const CStringArray & v)
{
	int cy = v.GetSize ();
	int cx = 0;

	for (int i = 0; i < v.GetSize (); i++) 
		cx = max (v [i].GetLength (), cx);

	const int nWidth = (int)ceil ((long double)cx / 16.0) * 16; 
	const int nHeight = cy;
	const int nBufferSize = nWidth * nHeight;
	BYTE * pBuffer = new BYTE [nBufferSize];

	memset (pBuffer, 0x00, sizeof (BYTE) * nBufferSize);

	for (int y = 0; y < v.GetSize (); y++) {
		CString str = v [y];

		for (int x = 0; x < nWidth; x++) {
			int nLen = min (str.GetLength (), 8);
			int nRow = (y * nWidth) / 8;
			CString strByte = str.Left (nLen);
			BYTE b = 0;

			str.Delete (0, nLen);

			for (int i = 0; i < nLen; i++) 
				b |= ((strByte [i] == 'X') ? (1 << (8 - i - 1)) : 0);

			// { CString strTmp; strTmp.Format (_T ("%-4d "), nRow); ::OutputDebugString (strTmp); }

			pBuffer [nRow + x] = b;

			/*
			CString strTmp = ToBinStr (b);
			strTmp.Replace ('0', '.');
			strTmp.Replace (_T (" "), _T (""));
			::OutputDebugString (strTmp + _T (" "));
			*/
		} 
		
		ASSERT (str.GetLength () == 0);

		//::OutputDebugString (_T ("\n"));
	}

	m_img.CreateFromHBITMAP (CreateBitmap (nWidth, nHeight, 1, 1, pBuffer));
	delete [] pBuffer;

	m_img.Negative ();
	m_img.RotateLeft ();
}
