//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Font.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FONT_DIALOG                 102
#define IDR_MAINFRAME                   128
#define IDM_MAIN                        129
#define IDA_MAIN                        130
#define CB_FONT                         1000
#define TXT_OUTPATH                     1001
#define BTN_BROWSE                      1002
#define BTN_GENERATE                    1003
#define TXT_FILENAME                    1004
#define TXT_HEIGHT                      1005
#define TXT_WIDTH                       1006
#define CB_WEIGHT                       1007
#define CHK_ITALIC                      1008
#define CHK_UNDERLINE                   1009
#define IDC_PREVIEW                     1010
#define TXT_CHARSET                     1011
#define BTN_DEFAULT                     1012
#define TXT_INPUT                       1013
#define BTN_PARSE                       1014
#define BTN_BROWSEINPUT                 1015
#define ID_FILE_EXIT                    32771
#define ID_HELP_ABOUT                   32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
