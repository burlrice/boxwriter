// FontDlg.h : header file
//

#if !defined(AFX_FONTDLG_H__F2E8CED2_6EAD_4B20_8A32_BC7BFEBF0AA8__INCLUDED_)
#define AFX_FONTDLG_H__F2E8CED2_6EAD_4B20_8A32_BC7BFEBF0AA8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Alphabet.h"
#include "IntelHexFile.h"

using namespace FoxjetCommon;

using CIntelHexFile::RECORDTYPE_HEADER;
using CIntelHexFile::RECORDTYPE_FONT_INDEX;
using CIntelHexFile::RECORDTYPE_FONT_HEADER;
using CIntelHexFile::RECORDTYPE_CHAR_INDEX;
using CIntelHexFile::RECORDTYPE_CHAR_STROKES;
using CIntelHexFile::RECORDTYPE_STROKE_CACHE;

#define BITSET(bByte, nBit) (((bByte) & (1 << (nBit))) ? true : false)
#define BITCLEAR(bByte, nBit) (((bByte) & (1 << (nBit))) == 0)

static const BYTE bCompressionMask	= 0x80; // 1000 0000
static const BYTE bRepeatMask		= 0xC0; // 1100 000
static const BYTE bTagMask			= 0xE0; // 1110 0000
static const BYTE bEngineMask		= 0xF0; // 1111 0000

static const BYTE bEndOfLineMask	= 0xE1; // 1110 0001
static const BYTE bEndOfCharMask	= 0xE2; // 1110 0010
static const BYTE bRepeatTag		= 0x10; // 0001 0000


bool IsEOL (BYTE b);
bool IsEOC (BYTE b);
bool IsLiteralCmd (BYTE b);
bool IsEngineCmd (BYTE b);
bool IsTagCmd (BYTE b);
bool IsRepeatCmd (BYTE b);
bool IsCompressionCmd (BYTE b);

CString ToBinStr (BYTE b);
bool IsUnicode (const CString & strFile);

/////////////////////////////////////////////////////////////////////////////
// CFontDlg dialog

class CFontDlg : public CDialog
{
// Construction
public: 
	CFontDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CFontDlg)
	enum { IDD = IDD_FONT_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFontDlg)
	public:
	virtual BOOL DestroyWindow();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnCancel();
	virtual void OnOK();
	HICON m_hIcon;
	CFont * m_pFont;
	CFont * m_pCharsetFont;
	HACCEL m_hAccel;
	void Free ();

	afx_msg void UpdateUI ();

	// Generated message map functions
	//{{AFX_MSG(CFontDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	afx_msg void OnSelchangeFont();
	afx_msg void OnFileExit();
	afx_msg void OnBrowse();
	afx_msg void OnGenerate();
	afx_msg void OnSelchangeWeight();
	afx_msg void OnChangeHeight();
	afx_msg void OnChangeWidth();
	afx_msg void OnItalic();
	afx_msg void OnUnderline();
	afx_msg void OnChangeCharset();
	afx_msg void OnBrowseCharset();
	afx_msg void OnHelpAbout();
	afx_msg void OnBrowseinput();
	afx_msg void OnParse();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FONTDLG_H__F2E8CED2_6EAD_4B20_8A32_BC7BFEBF0AA8__INCLUDED_)
