; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CFontDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Font.h"

ClassCount=3
Class1=CFontApp
Class2=CFontDlg
Class3=CAboutDlg

ResourceCount=4
Resource1=IDM_MAIN
Resource2=IDR_MAINFRAME
Resource3=IDD_FONT_DIALOG
Resource4=IDA_MAIN

[CLS:CFontApp]
Type=0
HeaderFile=Font.h
ImplementationFile=Font.cpp
Filter=N

[CLS:CFontDlg]
Type=0
HeaderFile=FontDlg.h
ImplementationFile=FontDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=BTN_BROWSE

[CLS:CAboutDlg]
Type=0
HeaderFile=FontDlg.h
ImplementationFile=FontDlg.cpp
Filter=D

[DLG:IDD_FONT_DIALOG]
Type=1
Class=CFontDlg
ControlCount=25
Control1=CB_FONT,combobox,1344340227
Control2=TXT_HEIGHT,edit,1350631552
Control3=TXT_WIDTH,edit,1350631552
Control4=CB_WEIGHT,combobox,1344339971
Control5=CHK_ITALIC,button,1342242819
Control6=CHK_UNDERLINE,button,1342242819
Control7=TXT_CHARSET,edit,1350633476
Control8=TXT_OUTPATH,edit,1350633600
Control9=BTN_BROWSE,button,1342242816
Control10=TXT_FILENAME,edit,1350633600
Control11=BTN_GENERATE,button,1342242817
Control12=IDC_STATIC,static,1342308352
Control13=IDC_STATIC,static,1342308352
Control14=IDC_STATIC,button,1342177287
Control15=IDC_STATIC,static,1342308352
Control16=IDC_STATIC,static,1342308352
Control17=IDC_STATIC,static,1342308352
Control18=IDC_STATIC,static,1342308352
Control19=IDC_PREVIEW,static,1342312448
Control20=IDC_STATIC,static,1342308352
Control21=BTN_DEFAULT,button,1342242816
Control22=TXT_INPUT,edit,1350633600
Control23=BTN_PARSE,button,1342242816
Control24=IDC_STATIC,static,1342308352
Control25=BTN_BROWSEINPUT,button,1342242816

[MNU:IDM_MAIN]
Type=1
Class=?
Command1=ID_FILE_EXIT
Command2=ID_HELP_ABOUT
CommandCount=2

[ACL:IDA_MAIN]
Type=1
Class=?
Command1=ID_HELP_ABOUT
CommandCount=1

