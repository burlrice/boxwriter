// Font.h : main header file for the FONT application
//

#if !defined(AFX_FONT_H__04339236_9409_41A7_AAC6_86A3DB5ADD32__INCLUDED_)
#define AFX_FONT_H__04339236_9409_41A7_AAC6_86A3DB5ADD32__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "Version.h"

/////////////////////////////////////////////////////////////////////////////
// CFontApp:
// See Font.cpp for the implementation of this class
//

class CFontApp : public CWinApp
{
public:
	CFontApp();

	static const FoxjetCommon::CVersion m_ver;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFontApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CFontApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FONT_H__04339236_9409_41A7_AAC6_86A3DB5ADD32__INCLUDED_)
