#ifndef __INTELHEXFILE_H__
#define __INTELHEXFILE_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CopyArray.h"

using namespace FoxjetCommon;

class CIntelHexFile
{
public:
	typedef enum 
	{
		RECORDTYPE_NONE			= 0x00,
		RECORDTYPE_HEADER		= 0x10,
		RECORDTYPE_FONT_INDEX	= 0x13,
		RECORDTYPE_FONT_HEADER	= 0x14,
		RECORDTYPE_CHAR_INDEX	= 0x16,
		RECORDTYPE_CHAR_STROKES = 0x17,
		RECORDTYPE_STROKE_CACHE = 0x18,
	} RECORDTYPE;

	CIntelHexFile ();
	virtual ~CIntelHexFile ();

	bool Open (const CString & str);
	void Close ();
	void WriteSection (BYTE * lpData, ULONG lLen, RECORDTYPE type = RECORDTYPE_NONE);
	void WriteSection (WORD * lpData, ULONG lLen, RECORDTYPE type = RECORDTYPE_NONE);

protected:
	void WriteLine (CByteArray & v, RECORDTYPE type);

	FILE * m_fp;
	WORD m_wPos;
	BYTE m_nLastWrite;
	ULONG m_nFilePos;
};



#endif //__INTELHEXFILE_H__
