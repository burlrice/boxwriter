#include "stdafx.h"
#include "Font.h"
#include "FontDlg.h"
#include "TemplExt.h"
#include "Debug.h"
#include "Database.h"
#include "Parse.h"
#include "ximage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;

////////////////////////////////////////////////////////////////////////

CIntelHexFile::CIntelHexFile ()
:	m_fp (NULL),
	m_wPos (0),
	m_nLastWrite (0),
	m_nFilePos (0)
{
}

CIntelHexFile::~CIntelHexFile ()
{
	Close ();
}

bool CIntelHexFile::Open (const CString & str)
{
	Close ();
	m_fp = _tfopen (str, _T ("wb"));

	return m_fp != NULL;
}

void CIntelHexFile::Close ()
{
	if (m_fp) {
		fprintf(m_fp, ":00000001FF\n");
		fclose (m_fp);

		m_fp			= NULL;
		m_wPos			= 0;
		m_nLastWrite	= 0;
		m_nFilePos		= 0;
	}
}

void CIntelHexFile::WriteSection (WORD * lpData, ULONG lLen, RECORDTYPE type)
{
	BYTE * pb = new BYTE [lLen * 2];
	int nPos = 0;

	ZeroMemory (pb, sizeof (BYTE) * lLen * 2);

	for (ULONG i = 0; i < lLen; i++) {
		// byte swap
		pb [nPos++] = LOBYTE (lpData [i]);
		pb [nPos++] = HIBYTE (lpData [i]);
	}

	WriteSection (pb, lLen * 2, type);
	delete [] pb;
}

void CIntelHexFile::WriteLine (CByteArray & v, RECORDTYPE type)
{
	ASSERT (v.GetSize () <= 16);
	fprintf (m_fp, ":%02x%04x%02x", v.GetSize (), m_nFilePos, type);

	for (int i = 0; i < v.GetSize (); i++) {
		CString strByte;

		fprintf (m_fp, "%02x", v [i]);
		m_nFilePos++;
	}

	{
		int nSum = v.GetSize () + ((m_nFilePos >> 8) & 255) + (m_nFilePos & 255);

		for (int i = 0; i < v.GetSize (); i++) 
			nSum += v [i] & 255;

		fprintf (m_fp, "%02x\n", (-nSum) & 255);
	}

	m_nLastWrite = v.GetSize ();
	v.RemoveAll ();
}

void CIntelHexFile::WriteSection (BYTE * lpData, ULONG lLen, RECORDTYPE type)
{

	if (m_fp) {
		CString str;
		CByteArray v;

		for (ULONG i = 0; i < lLen; i++) {
			v.Add (lpData [i]);

			if (i && (((i + 1) % 16) == 0)) 
				if (v.GetSize ()) 
					WriteLine (v, type);
		}

		if (v.GetSize ()) 
			WriteLine (v, type);
	}
}
