#ifndef __STROKE_H__
#define __STROKE_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CopyArray.h"

using namespace FoxjetCommon;

class CStroke : public CByteArray
{
public:
	CStroke ();
	CStroke (const CStroke & rhs);
	CStroke & operator = (const CStroke & rhs);
	virtual ~CStroke ();

	CString Format () const;
	CString ToString () const;
};

bool operator == (const CStroke & lhs, const CStroke & rhs);
bool operator != (const CStroke & lhs, const CStroke & rhs);

typedef CCopyArray <CStroke, CStroke &> CStrokeCache;
typedef CCopyArray <WORD, WORD> CIndexArray;

bool operator == (const CStrokeCache & lhs, const CStrokeCache & rhs);
bool operator != (const CStrokeCache & lhs, const CStrokeCache & rhs);

#endif //__STROKE_H__
