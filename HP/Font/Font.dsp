# Microsoft Developer Studio Project File - Name="Font" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Font - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Font.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Font.mak" CFG="Font - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Font - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Font - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Font - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\..\CxImage\CxImage" /I "..\..\Editor\DLL\Common\\" /I "..\..\Editor\DLL\Debug" /I "..\..\Editor\DLL\Database" /I "..\..\Editor\DLL\Utils" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "_UNICODE" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 cximage.lib Database.lib Utils.lib /nologo /entry:"wWinMainCRTStartup " /subsystem:windows /machine:I386 /libpath:"..\..\CxImage\CxImage\Release" /libpath:"..\..\Editor\DLL\Database\Release" /libpath:"..\..\Editor\DLL\Utils\Release"
# Begin Custom Build - Updating \FoxJet\$(TargetName).exe
TargetPath=.\Release\Font.exe
TargetName=Font
InputPath=.\Release\Font.exe
SOURCE="$(InputPath)"

"\FoxJet\$(TargetName).exe" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(TargetPath) \FoxJet

# End Custom Build

!ELSEIF  "$(CFG)" == "Font - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\..\CxImage\CxImage" /I "..\..\Editor\DLL\Common\\" /I "..\..\Editor\DLL\Debug" /I "..\..\Editor\DLL\Database" /I "..\..\Editor\DLL\Utils" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "_UNICODE" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Debug.lib cximage.lib Database.lib Utils.lib /nologo /entry:"wWinMainCRTStartup " /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"..\..\CxImage\CxImage\Debug" /libpath:"..\..\Editor\DLL\Debug\Debug" /libpath:"..\..\Editor\DLL\Database\Debug" /libpath:"..\..\Editor\DLL\Utils\Debug"
# Begin Custom Build - Updating \FoxJet\$(TargetName).exe
TargetPath=.\Debug\Font.exe
TargetName=Font
InputPath=.\Debug\Font.exe
SOURCE="$(InputPath)"

"\FoxJet\$(TargetName).exe" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(TargetPath) \FoxJet

# End Custom Build

!ENDIF 

# Begin Target

# Name "Font - Win32 Release"
# Name "Font - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Alphabet.cpp
# End Source File
# Begin Source File

SOURCE=.\Char.cpp
# End Source File
# Begin Source File

SOURCE=.\Font.cpp
# End Source File
# Begin Source File

SOURCE=.\Font.rc
# End Source File
# Begin Source File

SOURCE=.\FontDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\IntelHexFile.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\Stroke.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\Alphabet.h
# End Source File
# Begin Source File

SOURCE=.\Char.h
# End Source File
# Begin Source File

SOURCE=.\Font.h
# End Source File
# Begin Source File

SOURCE=.\FontDlg.h
# End Source File
# Begin Source File

SOURCE=.\IntelHexFile.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\Stroke.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\Font.ico
# End Source File
# Begin Source File

SOURCE=.\res\Font.rc2
# End Source File
# End Group
# Begin Source File

SOURCE=.\Changes.txt
# End Source File
# End Target
# End Project
