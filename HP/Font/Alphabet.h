#ifndef __ALPHABET_H__
#define __ALPHABET_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Char.h"

using namespace FoxjetCommon;

class CAlphabet
{
public:

	void Build (const CString & strCharSet, CFont & fnt);
	int Find (const CStroke & s) const;
	bool Save (const CString & strFile);
	bool Open (const CString & strFile);
	WORD GetOffset (TCHAR c);
	WORD GetOffset ();

	TEXTMETRIC m_tm;
	CCharArray m_v;
	CStrokeCache m_vStrokes;

protected:
	void BuildCache ();
};



#endif //__ALPHABET_H__
