#include "stdafx.h"
#include "Font.h"
#include "FontDlg.h"
#include "TemplExt.h"
#include "Debug.h"
#include "Database.h"
#include "Parse.h"
#include "ximage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;

////////////////////////////////////////////////////////////////////////

void CAlphabet::BuildCache ()
{
	m_vStrokes.RemoveAll ();

	//TRACEF ("");

	for (int nChar = 0; nChar < m_v.GetSize (); nChar++) {
		CChar & c = m_v [nChar];
		WORD wLastWord = (c.m_vStrokes.GetSize () - 1) * 2;
		WORD wHeader [3] = 
		{
			0x0000,
			0x0000,
			MAKEWORD (wLastWord & 0xFF, 0xc2),
		};

		if (wLastWord > 0xFF)
			wHeader [0] |= (1 << 7);

		c.m_vIndex.RemoveAll ();
		ASSERT (ARRAYSIZE (wHeader) == CChar::GetIndexBase ());

		for (int i = 0; i < CChar::GetIndexBase (); i++)
			c.m_vIndex.Add (wHeader [i]);

		#ifdef _DEBUG
		{
			CString str, strIndex;

			for (int i = 0; i < c.m_vStrokes.GetSize (); i++) 
				strIndex += _T ("123456789 ");

			str.Format (_T ("cached: %s"), strIndex);
			::OutputDebugString (str + '\n');
		}
		#endif

		for (int nStroke = 0; nStroke < c.m_vStrokes.GetSize (); nStroke++) {
			CStroke & s = c.m_vStrokes [nStroke];
			int nIndex = Find (s);

			if (nIndex == -1) {
				m_vStrokes.Add (s);
				nIndex = m_vStrokes.GetSize () - 1;
				//TRACEF (s.Format () + _T (" ") + s.ToString ());
			}

			WORD wOffset = 0;

			for (int i = 0; i < nIndex; i++) {
				//TRACEF (m_vStrokes [i].ToString ());
				wOffset += m_vStrokes [i].GetSize ();
			}

			ASSERT (s.ToString () == m_vStrokes [nIndex].ToString ());
			c.m_vIndex.Add (wOffset);

			#ifdef _DEBUG
			{
				CString str;

				str.Format (_T ("cached: %s [%04X] %s"), s.Format (), wOffset, s.ToString ());
				::OutputDebugString (str + '\n');
			}
			#endif

		}

		c.m_vIndex.Add (0xe200);

		#ifdef _DEBUG
		::OutputDebugString (_T ("\n"));
		#endif
	}
}

int CAlphabet::Find (const CStroke & s) const
{
	for (int i = 0; i < m_vStrokes.GetSize (); i++) 
		if (m_vStrokes [i] == s)
			return i;

	return -1;
}

bool CAlphabet::Save (const CString & strFile)
{
	CIntelHexFile f;
	int nFilePos = 0;
	const int nHeaderLen = 17; //16;
	const int nFontIndexLen = 16;

	if (!f.Open (strFile))
		return false;

	if (!m_v.GetSize ())
		return false;

	{ // TODO: font file index [1 - 8]
		BYTE b [nFontIndexLen] = { 1, 0 };

		f.WriteSection (b, ARRAYSIZE (b), RECORDTYPE_FONT_INDEX);
	}

	{ // header
		BYTE b [nHeaderLen] = 
		{ 
			nHeaderLen,	// s1			// record length
			0x00,		// sc			// security checksum for ii-aaaa-uuuuuuuu
			0x0c,		// ii			// licensee code = 0c
			0x2a,		// aaaa			// file security code 0x2a47
			0x47,		//  
			'1',		// uuuuuuuu		// user entry code for download = "1234"
			'2',		// 
			'3',		// 
			'4',		// 
			0x01,		// ee			// OEM ID = 01
			0x00,		// nn			// distortion flags, see notes
			0x00,		// bb			// bit rotate count, see notes
			0x01,		// sc			// file segment count = 01
			0x05,		// rr			// download version = 05
			'G',		// ft			// font file type = 'G'
			0x06,		// ct			// data compression type = 6
			0x00,		// ii			// ink flags
		};

		f.WriteSection (b, ARRAYSIZE (b), RECORDTYPE_HEADER);
	}

	{ // font header
		const WORD wLen			= 8;//12;
		WORD wCacheOffset		= 0;
		BYTE nLow				= (BYTE)m_v [0].m_c;
		BYTE nHigh				= (BYTE)m_v [m_v.GetSize () - 1].m_c;
		WORD wFileSize			= 0; 
		WORD wStructureOffset	= wLen * 2; // will change if copyright (etc) records added
		
		{ // low is first non-space char
			for (int i = 0; nLow == ' '; i++)
				nLow = (BYTE)m_v [i].m_c;
		}

		{
			wCacheOffset += GetOffset ();

			for (int i = 0; i < m_v.GetSize (); i++) 
				wCacheOffset += m_v [i].m_vIndex.GetSize () * 2;
		}

		{
			wFileSize += nHeaderLen;
			wFileSize += nFontIndexLen;
			wFileSize += wLen * 2;
			wFileSize += wCacheOffset;
			wFileSize += m_vStrokes.GetSize ();
		}

		WORD w [wLen] = 
		{ 
			/*
			//										HIBYTE					LOBYTE
			MAKEWORD (wLen * 2, nLow),	// [0]:		header length			low index
			MAKEWORD (nHigh, 0),		// [1]:		high index				font flags (see list)
			(WORD)m_tm.tmDescent,		// [2]:		baseline offset			
			(WORD)m_tm.tmAveCharWidth,	// [3]:		cell width				
			(WORD)m_tm.tmHeight,		// [4]:		cell height				
			MAKEWORD (0, 0),			// [5]:		underline				map flags (see list)
			wCacheOffset,				// [6]:		cache offset			
			wFileSize,					// [7]:		file size, data bytes	
			MAKEWORD (0, 0),			// [8]:		licensee code			OEM code
			0,							// [9]:		X DPI
			0,							// [10]:	Y DPI
			wStructureOffset,			// [11]:	offset to start of structure record
			*/

			//																		HIBYTE				LOBYTE
			MAKEWORD (nLow, nHigh),										// [0]:		low					high
			MAKEWORD (wCacheOffset, wStructureOffset),					// [1]:		cache offset		offset, index to structure record
			MAKEWORD (0, wLen),											// [2]:		font flags			header length
			MAKEWORD ((WORD)m_tm.tmDescent, (WORD)m_tm.tmAveCharWidth), // [3]:		baseline offset		cell width
			MAKEWORD ((WORD)m_tm.tmHeight, 0),							// [4]:		cell height			underline data
			MAKEWORD (0, wFileSize),									// [5]:		map flags, see list	font file size, bytes
			MAKEWORD (0, 0),											// [6]:		licensee code		OEM code
			MAKEWORD (0, 0),											// [7]:		X dpi				Y dpi
		};

		f.WriteSection (w, ARRAYSIZE (w), RECORDTYPE_FONT_HEADER);
	}

	{ // char index
		BYTE nFirst = (BYTE)m_v [0].m_c;
		BYTE nLen = m_v.GetSize () + nFirst;
		WORD * p = new WORD [nLen];

		ZeroMemory (p, sizeof (WORD) * nLen);

		for (int i = 0; i < nLen; i++)
			p [i] = GetOffset (m_v [0].m_c);

		for (i = 0; i < m_v.GetSize (); i++) 
			p [nFirst + i] = GetOffset (m_v [i].m_c);

		/*
		for (i = 0; i < nLen; i++) {
			CString str; 
			str.Format (_T ("p [%d]: %d"), i, p [i]); 
			TRACEF (str); 
		}
		*/

		f.WriteSection (p, nLen, RECORDTYPE_CHAR_INDEX);

		delete [] p;
	}

	{	// character stroke indicies
		const WORD wBase = GetOffset ();

		for (int nChar = 0; nChar < m_v.GetSize (); nChar++) {
			CChar & c = m_v [nChar];
			const ULONG lLen = c.m_vIndex.GetSize ();
			WORD * p = new WORD [lLen];
			int nPos = 0;

			ZeroMemory (p, sizeof (WORD) * lLen);

			//TRACEF (c.ToString ());

			for (int nIndex = 0; nIndex < c.m_vIndex.GetSize (); nIndex++, nPos++) 
				p [nPos] = c.m_vIndex [nIndex];

			/*
			{
				CString str;
				int nPos = 3;

				TRACEF ("p        m_vIndex");

				{ for (int i = 0; i < 3; i++) { CString str; str.Format (_T ("[%d] %04X"), i, p [i]); TRACEF (str); }}

				for (int i = 0; i < c.m_vIndex.GetSize (); i++, nPos++) {
					str.Format (_T ("[%d] %04X [%d] %04X"), 
						nPos, p [nPos],
						i, c.m_vIndex [i]);
					TRACEF (str);
					ASSERT (c.m_vIndex [i] == p [nPos]);
				}

				TRACEF (ToString (lLen));
			}
			*/

			f.WriteSection (p, lLen, RECORDTYPE_CHAR_STROKES);
			delete [] p;
		}
	}

	{ // stroke cache
		ULONG lLen = 0;
		int nPos = 0;

		for (int nStroke = 0; nStroke < m_vStrokes.GetSize (); nStroke++) 
			lLen += m_vStrokes [nStroke].GetSize ();

		BYTE * p = new BYTE [lLen];

		ZeroMemory (p, sizeof (BYTE) * lLen);

		for (nStroke = 0; nStroke < m_vStrokes.GetSize (); nStroke++) {
			CStroke & s = m_vStrokes [nStroke];

			for (int i = 0; i < s.GetSize (); i++) 
				p [nPos++] = s [i];
		}

		/*
		{
			CString str;
			WORD wOffset = GetOffset ();

			str.Format (_T ("\nwritten stroke cache\n[%04X] "), wOffset);

			for (ULONG i = 0; i < lLen; i++, wOffset++) {
				CString strByte;
				BYTE b = p [i];
				
				strByte.Format (_T ("%02X "), b);
				str += strByte;

				if (IsEOL (b)) {
					strByte.Format (_T ("\n[%04X] "), wOffset + 1);
					str += strByte;
				}
			}

			TRACEF (str);
		}
		*/

		f.WriteSection (p, lLen, RECORDTYPE_STROKE_CACHE);
		delete [] p;
	}

	f.Close ();

	return true;
}

WORD CAlphabet::GetOffset ()
{
	WORD w = m_v.GetSize () * 2;

	if (m_v.GetSize ()) {
		for (TCHAR c = 0; c < m_v [0].m_c; c++)
			w += 2;
	}

	return w;
}

WORD CAlphabet::GetOffset (TCHAR c)
{
	WORD wResult = GetOffset ();

	for (int i = 0; i < m_v.GetSize (); i++) {
		if (m_v [i].m_c == c)
			break;

		wResult += (m_v [i].m_vIndex.GetSize () * 2);
	}

	return wResult;
}

void CAlphabet::Build (const CString & strCharSet, CFont & fnt)
{
	LOGFONT lf;

	::ZeroMemory (&lf, sizeof (lf));
	fnt.GetLogFont(&lf);

	for (int nChar = 0; nChar < strCharSet.GetLength (); nChar++) {
		CString str = strCharSet [nChar];
		CBitmap bmpMem;
		CDC dcMem;
		CRect rc (0, 0, 0, 0);
		UINT nFormat = DT_NOPREFIX;

		dcMem.CreateCompatibleDC (NULL);
		CFont * pFont = dcMem.SelectObject (&fnt);
		dcMem.DrawText (str, &rc, DT_CALCRECT | nFormat);
		bmpMem.CreateCompatibleBitmap (&dcMem, rc.Width (), rc.Height ());

		ZeroMemory (&m_tm, sizeof (m_tm));
		dcMem.GetTextMetrics (&m_tm);

		CBitmap * pBmp = dcMem.SelectObject (&bmpMem);
		::BitBlt (dcMem, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
		dcMem.DrawText (str, &rc, nFormat);
		//SAVEBITMAP (bmpMem, _T ("C:\\Temp\\Debug\\") + str + _T (".bmp"));

		{
			CxImage img;
			BITMAP bm;

			::ZeroMemory (&bm, sizeof (bm)); 

			VERIFY (img.CreateFromHBITMAP (bmpMem));
			VERIFY (img.RotateRight ());
			HBITMAP hBmp = img.MakeBitmap (dcMem);
			//SAVEBITMAP (hBmp, _T ("C:\\Temp\\Debug\\90_") + str + _T (".bmp"));
			
			VERIFY (::GetObject (hBmp, sizeof (bm), &bm));
			const DWORD dwSize = bm.bmWidthBytes * bm.bmHeight;

			if (dwSize) {
				try
				{
					CChar c (str [0]);
					LPBYTE lpBits = new BYTE [dwSize];
					LPBYTE lpStart = lpBits;

					::ZeroMemory (lpBits, sizeof (BYTE) * dwSize);
					::GetBitmapBits (hBmp, dwSize, lpBits);

					for (int y = 0; y < bm.bmHeight; y++) {
						CString str;
						
						for (int x = 0; x < bm.bmWidthBytes; x++) {
							BYTE n = * (lpStart++);

							for (int i = 0; i < 8; i++) {
								bool bPixel = (n & (1 << (8 - i - 1))) ? false : true;

								str += bPixel ? _T ("X") : _T (".");
							}
						}

						/* TODO: rem
						if (str.GetLength () > lf.lfHeight) {
							//TRACEF (ToString (str.GetLength ()) + _T (": ") + str);
							str.Delete (lf.lfHeight, str.GetLength () - lf.lfHeight - 1);
							//TRACEF (ToString (str.GetLength ()) + _T (": ") + str);
						}
						*/

						for (int i = lf.lfHeight; i < str.GetLength (); i++) 
							str.SetAt (i, '.');

						{
							CStroke s;
							
							CChar::Compress (str, s);
							c.m_vStrokes.Add (s);
						}
					}

					delete [] lpBits;
					m_v.Add (c);
				}
				catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
			}
		}

		dcMem.SelectObject (pBmp);
		dcMem.SelectObject (pFont);
	}

	BuildCache ();
}

bool CAlphabet::Open (const CString & strFile)
{
	CByteArray vCharIndex;
	CByteArray vCharStrokes;
	CByteArray vStrokeCache;
	CWordArray vChar;

	if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
		CString str;
		char c = EOF; 
		bool bMore;

		do {
			c = getc (fp);
			bMore = c != EOF && c != WEOF;
				
			if (bMore) {
				switch (c) {
				case '\r':
				case '\n':
					if (str.GetLength () > 11) {
						str.Delete (str.GetLength () - 2, 2);

						CString strAddr = str.Mid (1, 8);
						CString strData = str.Mid (9);
						int nType = _tcstoul (strAddr.Mid (6), NULL, 16);
						CByteArray * pv = NULL;

						//TRACEF (strAddr.Mid (6) + _T (" ") + strData);

						switch (nType) {
						case RECORDTYPE_CHAR_INDEX:		pv = &vCharIndex;	break;
						case RECORDTYPE_CHAR_STROKES:	pv = &vCharStrokes;	break;
						case RECORDTYPE_STROKE_CACHE:	pv = &vStrokeCache;	break;
						}

						if (pv) {
							while (strData.GetLength ()) {
								CString strByte;

								for (int i = 0; i < 2; i++) {
									if (strData.GetLength ()) {
										strByte += strData [0];
										strData.Delete (0, 1);
									}
								}

								pv->Add ((BYTE)_tcstoul (strByte, NULL, 16));
							}
						}
					}

					str.Empty ();
					break;
				default:
					str += c;
					break;
				}
			}
		}
		while (bMore);

		fclose (fp);

		{ 
			CString str;

			for (int nChar = 0; nChar < vCharIndex.GetSize (); nChar += 2) {
				CString strByte;
				WORD w = (vCharIndex [nChar + 1] << 8) | vCharIndex [nChar];

				vChar.Add (w);

				//strByte.Format (_T ("%02X %02X "), vCharIndex [nChar + 1], vCharIndex [nChar]);
				strByte.Format (_T ("%04X "), w);

				if ((nChar % 16) == 0)
					str += _T ("\n\t");
				
				str += strByte;
			}

			TRACEF (ToString (vCharIndex.GetSize () / 2.0) + _T (" entries in char index"));
			TRACEF (str);
		}

		/*
		{
			CString str;
			WORD wOffset = vCharIndex.GetSize ();

			str.Format (_T ("\nparsed stroke cache\n[%04X] "), wOffset);

			for (int i = 0; i < vStrokeCache.GetSize (); i++, wOffset++) {
				CString strByte;
				BYTE b = vStrokeCache [i];
				
				strByte.Format (_T ("%02X "), b);
				str += strByte;

				if (IsEOL (b)) {
					strByte.Format (_T ("\n[%04X] "), wOffset + 1);
					str += strByte;
				}
			}

			TRACEF (str);
		}
		*/
		
		{
			const ULONG lBase = vCharIndex.GetSize ();
			const ULONG lFirstAddr = (vChar.GetSize () ? vChar [0] : 0) - lBase; 
			//TRACEF (ToString (lBase));

			for (int i = 0; i < vChar.GetSize (); i++) {
				ULONG lAddr = vChar [i] - lBase;
				bool bMore;
				CWordArray v;
				CStringArray vString;

				m_v.Add (CChar (i));
				CChar & c = m_v [m_v.GetSize () - 1];

				if (lAddr > lFirstAddr) {
					int nBreak = 0;
				}

				//{ CString str; str.Format (_T ("vChar [%d]: %d [%04x], lAddr: %d [%04x]"), i, vChar [i], vChar [i], lAddr, lAddr); TRACEF (str); }

				ULONG lIndex = lAddr + 6;

				do {
					BYTE bHI = vCharStrokes [lIndex + 1];
					BYTE bLO = vCharStrokes [lIndex];
					WORD w = (bHI << 8) | bLO;

					if (bMore = (!IsEOC (bHI))) 
						v.Add (w);

					//{ CString str; str.Format (_T ("%04X [%02X %02X] "), w, bHI, bLO); TRACEF (str); }
					lIndex += 2;
				}
				while (bMore);

				{
					CString strChar = _T ("\t\n");

					for (int j = 0; j < v.GetSize (); j++) {
						WORD w = v [j];

						//{ CString str; str.Format (_T ("%04X "), w); if ((j % 8) == 0) strChar += "\n\t"; strChar += str; }

						if (BITSET (HIBYTE (w), 7)) {
							BYTE b = HIBYTE (w);

							if (BITSET (b, 6)) {
								int nRepeatLine = b & 0x0F;

								for (int nRepeat = 0; nRepeat < nRepeatLine; nRepeat++) {
									CString str;

									str.Format (_T ("TODO: repeat [%-3d of %-3d]"), nRepeat + 1, nRepeatLine);
									TRACEF (str);
								}
							}
							else
								TRACEF (_T ("command: ") + ToBinStr (HIBYTE (w)) + _T (" ") + ToBinStr (LOBYTE (w)));

							continue;
						}

						if (w >= vStrokeCache.GetSize ()) {
							CString str; 
							
							str.Format (_T ("Out of range [0, %d): %04X [%d] [%s %s]"), 
								vStrokeCache.GetSize (), 
								w, w,
								ToBinStr (HIBYTE (w)), ToBinStr (LOBYTE (w))); 
							TRACEF (str);
							ASSERT (0);
							continue;
						}

						{ CString str; str.Format (_T ("%04X "), w); if ((j % 8) == 0) strChar += "\n\t"; strChar += str; }

						CString strStroke, strIndex;
						int nRepeatLine = 0;

						//strIndex.Format (_T ("[%s %s %04X]"), ToBinStr (HIBYTE (w)), ToBinStr (LOBYTE (w)), w);
						strIndex.Format (_T ("[%04X]"), w);

						while (1) { 
							BYTE b = vStrokeCache [w++];
							bool bHandled = true;

							//strIndex += _T (", ") + ToBinStr (b);
							{ CString strTmp; strTmp.Format (_T (" %02X"), b); strIndex += strTmp; }

							//TRACEF (ToBinStr (b));

							if (IsEOC (b) || IsEOL (b))
								break;

							if (IsLiteralCmd (b)) {
								for (int i = 0; i < 7; i++) {
									bool bPixel = (b & (1 << (i))) ? true : false;

									strStroke += (bPixel ? _T ("X") : _T ("."));
								}
							}
							else if (IsEngineCmd (b)) {
								TRACEF (_T ("engine: ") + ToBinStr (b));
								ASSERT (0);
							}
							else if (IsTagCmd (b)) {
								TRACEF (_T ("tag: ") + ToBinStr (b));
								ASSERT (0);
							}
							else if (IsRepeatCmd (b)) {
								if (BITSET (b, 4)) {
									TRACEF (_T ("repeat tag: ") + ToBinStr (b));
									ASSERT (0);
								}
								else
									nRepeatLine = b & 0xF;
							}
							else if (IsCompressionCmd (b)) {
								bool bPixel = BITSET (b, 4);
								int nRepeat = b & 0x0F;

								strStroke += CString (bPixel ? 'X' : '.', nRepeat * 7);
							}
							else 
								bHandled = false;

							if (!bHandled) {
								TRACEF (_T ("not handled: ") + ToBinStr (b));
								ASSERT (0);
							}
						}

						::OutputDebugString (strStroke + _T (" \t ") + strIndex + _T ("\n"));

						vString.Add (strStroke);

						for (int nRepeat = 0; nRepeat < nRepeatLine; nRepeat++) {
							::OutputDebugString (strStroke + _T (" \t ") + strIndex + _T ("\n"));
							vString.Add (strStroke);
						}
					}

					if (strChar.GetLength () > 2) {
						TRACEF (strChar);
						int nBreak = 0;
					}

					c.BuildImage (vString);
				}
			}
		}

		return true;
	}

	return false;
}