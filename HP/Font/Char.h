#ifndef __CHAR_H__
#define __CHAR_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ximage.h"
#include "Stroke.h"

using namespace FoxjetCommon;

class CChar
{
public:
	CChar (TCHAR c = ' ');
	CChar (const CChar & rhs);
	CChar & operator = (const CChar & rhs);
	virtual ~CChar ();

	static void Compress (const CString & str, CStroke & v);
	static int Count (const CString & str, TCHAR c);

	static int GetIndexBase ();
	static const int m_nHeaderLen;

	CString ToString () const;
	void BuildImage (const CStringArray & v);

	TCHAR m_c;
	CStrokeCache m_vStrokes;
	CIndexArray m_vIndex;
	CxImage m_img;

protected:

};


typedef CCopyArray <CChar, CChar &> CCharArray;


#endif //__CHAR_H__
