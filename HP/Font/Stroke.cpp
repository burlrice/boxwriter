#include "stdafx.h"
#include "Font.h"
#include "FontDlg.h"
#include "TemplExt.h"
#include "Debug.h"
#include "Database.h"
#include "Parse.h"
#include "ximage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;

////////////////////////////////////////////////////////////////////////

CStroke::CStroke ()
{
}

CStroke::CStroke (const CStroke & rhs)
{
	Append (rhs);
}

CStroke & CStroke::operator = (const CStroke & rhs)
{
	if (this != &rhs) {
		RemoveAll ();
		Append (rhs);
	}

	return * this;
}

CStroke::~CStroke ()
{
}

CString CStroke::ToString () const
{
	CString str;

	for (int i = 0; i < GetSize (); i++) {
		CString strByte;

		strByte.Format (_T ("%02X "), GetAt (i));
		str += strByte;

		if (i && (i % 16) == 0)
			str += '\n';
	}

	return str;
}

CString CStroke::Format () const
{
	CString strStroke;

	for (int i = 0; i < GetSize (); i++) {
		BYTE b = GetAt (i);

		if (IsLiteralCmd (b)) {
			for (int i = 0; i < 7; i++) {
				bool bPixel = (b & (1 << (i))) ? true : false;

				strStroke += (bPixel ? _T ("X") : _T ("."));
			}
		}
		else if (IsCompressionCmd (b)) {
			bool bPixel = BITSET (b, 4);
			int nRepeat = b & 0x0F;

			strStroke += CString (bPixel ? 'X' : '.', nRepeat * 7);
		}
		else if (!IsEOL (b) && !IsEOC (b)) { TRACEF (ToBinStr (b)); ASSERT (0); }
	}

	return strStroke;
}

bool operator == (const CStroke & lhs, const CStroke & rhs)
{
	if (lhs.GetSize () && rhs.GetSize ()) {
		for (int i = 0; i < lhs.GetSize (); i++) 
			if (lhs [i] != rhs [i])
				return false;

		return true;
	}

	return false;
}

bool operator != (const CStroke & lhs, const CStroke & rhs)
{
	return !(lhs == rhs);
}

/////////////////////////////////////////////////////////////////////////////

bool operator == (const CStrokeCache & lhs, const CStrokeCache & rhs)
{
	if (lhs.GetSize () && rhs.GetSize ()) {
		for (int i = 0; i < lhs.GetSize (); i++) 
			if (lhs [i] != rhs [i])
				return false;

		return true;
	}

	return false;
}

bool operator != (const CStrokeCache & lhs, const CStrokeCache & rhs)
{
	return !(lhs == rhs);
}
