Optimization / new features
	driver (FxMphc.sys) now uses direct I/O
		
	/IMAGE_ON_PHOTOCELL (default)
		works as prior standard software

	/IMAGE_ON_EOP
		generates image at the end of the previous print
		downloads iamge on photo event via round-robin time slicing, in 1" segments

	encoder changes
		300 dpi, 150 dpi now availible
			96 & NP heads always use 300 dpi (no double pulse)
			all others will use 150 dpi

	"About" dialog now displays the command line arguments that were displayed at startup

	Control app now detects when the system time changes & prompts the user to restart the app
		aka, "the Terry message"

Bug fixes
	Control app sometimes crashed when scrolling preview window (& other random times)
		aka, "MFC error"

	Barcode element
		added vertical barcodes (only for I205)

	Bitmap element
		converts color images (bmp, jpg, tiff, etc) to grayscale (dithered) images automatically

	Count element
		added "roll over on max" for pallet counts
			
	Database element
		clicking "Select...." could hang on an abmormally large recordset
			now limited to the first 100 records

		does not display binary data (can lock app up)

		could deadlock if a line has multiple heads (each having db multiple elements)

		added 'SYSTEM TABLE' filter to read Excel workbook names
