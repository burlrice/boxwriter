Linked heads
	"Head properties" > "More..." > "Linked to"

	when "Linked to" is set, if the linked head had data, the next time its opened in the Editor, 
		the old data will be replaced with a copy of the target head's data.

		to update existing tasks, each task must be opened and saved in the editor

		the linked head cannot be edited, only the target head it's linked to.

