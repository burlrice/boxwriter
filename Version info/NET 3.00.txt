////////////////////////////////////////////////////////////////////////////////
// new features

Added paragraph mode

Added OP_HEAD_INVERT

Added the abilibty to the Control app to login at command line (/user=xxx /pass=yyy)

Added hub relay/digial inputs for Bayer (from 1.20.00-0832 [21])

Offline heads are displayed in the Control app grayed out with a state of "disabled"

Editor - tab to "next element"

Compacts database when app exits

////////////////////////////////////////////////////////////////////////////////
// bug fixes

If the database is missing, the app recreates it at startup

Head properties didn't always show the correct head angles 
	eg: 14.3 for 300 dpi

Edtior was unable to correctly draw 40" x 40" box 

When trying to stop a test pattern, the error is "Following Head(s) failed to stop in time" sometimes occured

Date/time element did not display correct date when 'expiration' is selected

Editor - file properties were not reloaded

EAN128/C128 fix (for Sunnyvale)

Could not send "/" as dynamic data

IPC_PUT_FONT changed to send reply (should download fonts faster now)

Low ink state for wax head changed
	dot count is now 34,000,000 when low ink detected
	9.8 ml remains
	ink usage: 48 pl, or 288 pl per channel

App crashed sometimes when network connection was lost

Control app did not send APS values to NET controller

Dynamic text/barcode
	1) changes to dynamic table were not saved in db

	2) Start task dialog - "select" button was missing if dynamic table entry had "use database" checked

	3) Did not display actual data in preview

	4) Inserting a dynamic barcode may crash on prior versions