////////////////////////////////////////////////////////////////////////////////
// bug fixes

1) "Cancel strobe" button removed

2) Some element property dialogs did not save "preview" setting correctly

3) Control preview goes blank after task start 

4) Control can hang on shutdown (especially in demo mode)

5) Control can lock up starting task (most likely to happen w/4 heads, after system has been idle for several hours - see Tyco)

6) "Low ink" state causes status bar to display: "Cannot print until head errors clear"

7) "Out of ink" did not trigger strobe

8) Clearing "low ink" on one head caused strobe to disappear if other head still has low ink condition

9) Translating the software from English to Spanish, when the barcode parameters were already configured caused the wrong parameters to be loaded in the translation

10) Editor no longer allows '\' char in task names

11) "Hold start date" did not take rollover into account

12) Control's print preview didn't work with long elements

13) Hour code bug fix (was off by +1 index)

14) Disabled head showed erroneous data in preview (might show test pattern)

15) Task import crashed Control app 




////////////////////////////////////////////////////////////////////////////////
// features/enhancements

1) Sped up open/save task dialog

2) Control's task tab now highlights when head errors are present on a given line

3) Added zoom factor for Control preview

4) Added "Configure database start"

5) Added DataMatrix barcode (enable with /DATAMATRIX at cmd line)

6) Added linked text elements (for barcode captions)

7) Added linked heads

8) "Scan & shoot requires login" was moved from "fixed scanner" to "hand scanner" (shortened to "requires login")

9) If the Editor loads a task with elements that were out of the printable area, they are now moved into it, and the document is marked as modified/unsaved

10) Serial port usage was changed the following:
 "Hand scanner"  --> "Task start"
 "Fixed scanner"  --> "Barcode verification"
 "Remote/PC"   --> "Variable data"

11) Added "default" string to Database element (used if no records in result set)

12) Added ability to export "Ink usage" to a text file

13) Added to the Editor:
 Bring to front
 Bring forward
 Send backward
 Send to back

14) Added "Edit" button to Barcode dialog to go directly to barcode params dialog
 
15) Added "Shape" element

16) Added auto backup feature - when Editor or Control shuts down, the mdb file is copied to the D: partition (ex: MarksmanPro.backup.mdb)
 configurable from: Editor --> Define > Backup path

17) Control now prompts to restart task if head config change so requires

18) Test pattern now uses image similar to 768Jet.bmp

19) Unicode support

20) European Julian day for date/time / exp date elements (%J)

