// SPKey.h : main header file for the SPKEY application
//

#if !defined(AFX_SPKEY_H__0014C1F4_522C_4741_B80D_B517B6C8E278__INCLUDED_)
#define AFX_SPKEY_H__0014C1F4_522C_4741_B80D_B517B6C8E278__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSPKeyApp:
// See SPKey.cpp for the implementation of this class
//

class CSPKeyApp : public CWinApp
{
public:
	CSPKeyApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSPKeyApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSPKeyApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SPKEY_H__0014C1F4_522C_4741_B80D_B517B6C8E278__INCLUDED_)
