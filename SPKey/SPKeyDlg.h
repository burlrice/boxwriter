// SPKeyDlg.h : header file
//

#if !defined(AFX_SPKEYDLG_H__08EE15D2_482C_4D8F_9C79_3BD0CE68B137__INCLUDED_)
#define AFX_SPKEYDLG_H__08EE15D2_482C_4D8F_9C79_3BD0CE68B137__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSPKeyDlg dialog

class CSPKeyDlg : public CDialog
{
// Construction
public:
	CSPKeyDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSPKeyDlg)
	enum { IDD = IDD_SPKEY_DIALOG };
	CString	m_sSerialNo;
	CString	m_sVerMajor;
	CString	m_sVerMinor;
	CString	m_sRegKey;
	CString	m_sAccessLevel;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSPKeyDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSPKeyDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnGenKey();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SPKEYDLG_H__08EE15D2_482C_4D8F_9C79_3BD0CE68B137__INCLUDED_)
