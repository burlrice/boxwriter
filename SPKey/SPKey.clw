; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CSPKeyDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "SPKey.h"

ClassCount=3
Class1=CSPKeyApp
Class2=CSPKeyDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_SPKEY_DIALOG

[CLS:CSPKeyApp]
Type=0
HeaderFile=SPKey.h
ImplementationFile=SPKey.cpp
Filter=N

[CLS:CSPKeyDlg]
Type=0
HeaderFile=SPKeyDlg.h
ImplementationFile=SPKeyDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CSPKeyDlg

[CLS:CAboutDlg]
Type=0
HeaderFile=SPKeyDlg.h
ImplementationFile=SPKeyDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_SPKEY_DIALOG]
Type=1
Class=CSPKeyDlg
ControlCount=11
Control1=IDOK,button,1342242817
Control2=IDC_GEN_KEY,button,1342242816
Control3=IDC_SERIAL_NO,edit,1350631560
Control4=IDC_REG_KEY,edit,1484849280
Control5=IDC_VER_MAJOR,edit,1350639744
Control6=IDC_VER_MINOR,edit,1350639744
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_ACCESS_LEVEL,edit,1350639744

