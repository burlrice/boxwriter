//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DataMatrixVc6Test.rc
//
#define IDSAVE                          3
#define IDBUY                           4
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DATAMATRIXVC6TEST_DIALOG    102
#define IDR_MAINFRAME                   128
#define IDENCODE                        1000
#define IDC_ENCODEMODE                  1001
#define IDC_MODULESIZE                  1002
#define IDC_MARGIN                      1003
#define IDC_BACKCOLOR                   1004
#define IDC_FORECOLOR                   1005
#define IDC_TEXT                        1006
#define IDC_COLUMNS                     1007
#define IDC_ROWS                        1008

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
