// Sw0848Thread.cpp : implementation file
//

#include "stdafx.h"
#include "control.h"

#include <process.h>
#include "Sw0848Thread.h"
#include "Debug.h"
#include "Sw0848Dlg.h"

#ifdef __WINPRINTER__
	#include "DatabaseElement.h"

	using namespace FoxjetElements;
#endif //__WINPRINTER__

using namespace FoxjetDatabase;
using namespace Sw0848Thread;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSw0848Thread

IMPLEMENT_DYNCREATE(CSw0848Thread, CWinThread)

CSw0848Thread::CSw0848Thread()
:	m_pdb (NULL),
	m_prst (NULL)
{
}

CSw0848Thread::~CSw0848Thread()
{
}

BOOL CSw0848Thread::InitInstance()
{
	return TRUE;
}

void CSw0848Thread::OnInit (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	bool & bResult = * (bool *)lParam;
		
	bResult = false;

	Free ();

	try
	{
		Sw0848Dlg::CSw0848Dlg::Load (m_map);

		ASSERT (m_map.m_dsn.m_strOutput.GetLength ());
		ASSERT (m_pdb == NULL);

		if (!(m_pdb = new COdbcDatabase (_T (__FILE__), __LINE__))) {
			if (!m_pdb->Open (m_map.m_dsn.m_strOutput)) {
				MsgBox ("Failed to open: " + m_map.m_dsn.m_strOutput, MB_ICONERROR);
				return;
			}
		}
	
		const CString strSQL = _T ("SELECT * FROM ") + m_map.m_table.m_strOutput;

/*
		if (!m_db.Open (m_map.m_dsn.m_strOutput)) {
			MsgBox ("Failed to open: " + m_map.m_dsn.m_strOutput, MB_ICONERROR);

			return FALSE;
		}
*/

		m_prst = new COdbcRecordset (m_pdb);

		ASSERT (m_map.m_table.m_strOutput.GetLength ());

		if (!m_prst->Open (strSQL, AFX_DB_USE_DEFAULT_TYPE)) {
			delete m_prst;
			m_prst = NULL;
			MsgBox ("Failed to open: " + m_map.m_dsn.m_strOutput + "::" + m_map.m_table.m_strOutput, MB_ICONERROR);

			return;
		}
		else 
			bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	if (HANDLE h = (HANDLE)wParam)
		::SetEvent (h);
}

void CSw0848Thread::Free ()
{
	try
	{
		if (m_prst) {
			if (m_prst->IsOpen ())
				m_prst->Close ();

			delete m_prst;
			m_prst = NULL;
		}

		if (m_pdb) {
			if (m_pdb->IsOpen ())
				m_pdb->Close ();

			delete m_pdb; 
			m_pdb = NULL;
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
}

int CSw0848Thread::ExitInstance()
{
	Free ();
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CSw0848Thread, CWinThread)
	//{{AFX_MSG_MAP(CSw0848Thread)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_THREAD_MESSAGE (TM_KILLTHREAD, OnKillThread)
	ON_THREAD_MESSAGE (TM_POSTPHOTOCELL, OnPostPhotocell)
	ON_THREAD_MESSAGE (TM_SW0848INIT, OnInit)	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSw0848Thread message handlers

void CSw0848Thread::OnKillThread (WPARAM wParam, LPARAM lParam)
{
	ExitInstance ();
	delete this;
	_endthreadex (0);
}

void CSw0848Thread::OnPostPhotocell (WPARAM wParam, LPARAM lParam)
{
	if (CPostPhotocell * p = (CPostPhotocell *)lParam) {
		TRACEF (p->m_tm.Format ("%c"));
		
		try
		{
			m_prst->AddNew ();

			for (int i = 0; i < m_map.m_v.GetSize (); i++) {
				CString strInput = m_map.m_v [i].m_strInput;
				CString strOutput = m_map.m_v [i].m_strOutput;

				for (int j = 0; j < p->m_v.GetSize (); j++) {
					if (!p->m_v [j].m_strField.CompareNoCase (strInput)) {
						CString strData = p->m_v [j].m_strData;

						TRACEF (strInput + " --> " + strOutput + ": " + strData);
						m_prst->SetFieldValue (strOutput, strData);
						break;
					}
				}
			}

			m_prst->Update ();
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		delete p;
	}
}

int CSw0848Thread::FindField (const CString & str, CArray <CPair, CPair &> & v)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (str.CompareNoCase (v [i].m_strField) == 0)
			return i;

	return -1;
}

