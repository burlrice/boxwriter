// DiagDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "control.h"
#include "DiagDlg.h"
#include "TemplExt.h"
#include "Utils.h"
#include "Database.h"

using FoxjetDatabase::IsMatrix;
using namespace FoxjetCommon;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static LPCTSTR lpszLogfile = NULL; //_T ("Scan.log");

/////////////////////////////////////////////////////////////////////////////
// CDiagDlg dialog

CDiagDlg::CDiagDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg (IDD_DIAGNOSTIC_MATRIX /* IsMatrix () ? IDD_DIAGNOSTIC_MATRIX : IDD_DIAGNOSTIC */ , pParent)
{
	m_pParent = pParent;
	Create (IDD_DIAGNOSTIC_MATRIX /* IsMatrix () ? IDD_DIAGNOSTIC_MATRIX : IDD_DIAGNOSTIC */ , m_pParent);
	//{{AFX_DATA_INIT(CDiagDlg)
	//}}AFX_DATA_INIT
}


void CDiagDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDiagDlg)
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDC_CLEAR);
}


BEGIN_MESSAGE_MAP(CDiagDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDiagDlg)
	ON_BN_CLICKED(IDOK, OnClose)
	ON_BN_CLICKED(IDC_CLEAR, OnClear)
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDiagDlg message handlers

void CDiagDlg::UpdateSerialData(CString sData, bool bFormat)
{

	if ( !sData.IsEmpty() )
	{
		if (!bFormat) {
			for (int i = sData.GetLength () - 1; i >= 0; i--) {
				if (sData [i] == '\0') {
					sData.Delete (i);
					sData.Insert (i, _T ("<NULL>"));
				}
			}
		}

		CString sTemp = bFormat ? FoxjetDatabase::Format (/*(LPBYTE)(LPCTSTR)*/sData, sData.GetLength ()) : sData;

		if ( m_hWnd ) 
		{
			CEdit * p = (CEdit *)GetDlgItem (TXT_DATA);
			CString str;

			ASSERT (p);
			m_v.Add (sTemp);

			while (m_v.GetSize () > 500)
				m_v.RemoveAt (0);

			for (int i = 0; i < m_v.GetSize (); i++)
				str += m_v [i] + _T ("\r\n");

			p->SetWindowText (str);
			p->LineScroll (m_v.GetSize ());

			if (lpszLogfile) {
				if (FILE * fp = _tfopen (lpszLogfile, _T ("a"))) {
					sTemp += '\n';
					fwrite ((LPVOID)(LPCTSTR)sTemp, sTemp.GetLength (), 1, fp);
					fclose (fp);
				}
			}
		}
	}
}

void CDiagDlg::OnClose() 
{
	::PostMessage (m_pParent->m_hWnd, WM_CLOSE_DIAG, 0, 0 );
}

void CDiagDlg::OnClear() 
{
	CEdit * p = (CEdit *)GetDlgItem (TXT_DATA);

	ASSERT (p);
	p->SetWindowText (_T (""));
	m_v.RemoveAll ();

	if (lpszLogfile) 
		if (FILE * fp = _tfopen (lpszLogfile, _T ("w")))
			fclose (fp);
}

void CDiagDlg::OnSize(UINT nType, int cx, int cy) 
{
	FoxjetCommon::CEliteDlg::OnSize(nType, cx, cy);

	if (IsWindowVisible ()) {
		CWnd * pLB		= GetDlgItem (TXT_DATA);
		CWnd * pOK		= GetDlgItem (IDOK);
		CWnd * pClear	= GetDlgItem (IDC_CLEAR);
		CRect rcButton, rcClient;
		int nBorder = 15;
		
		ASSERT (pLB);
		ASSERT (pOK);
		ASSERT (pClear);

		pOK->GetWindowRect (&rcButton);
		GetClientRect (&rcClient);

		pLB->SetWindowPos (NULL, 
			0, 0, 
			cx - (nBorder * 2), 
			cy - rcButton.Height () - (nBorder * 2), 
			SWP_NOMOVE | SWP_NOZORDER);
		pOK->SetWindowPos (NULL, 
			nBorder, 
			cy - (nBorder * 2), 
			0, 0, SWP_NOSIZE | SWP_NOZORDER);
		pClear->SetWindowPos (NULL, 
			cx - (nBorder * 2) - rcButton.Width (), 
			cy - (nBorder * 2), 
			0, 0, SWP_NOSIZE | SWP_NOZORDER);
	}
}

void CDiagDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	FoxjetCommon::CEliteDlg::OnShowWindow(bShow, nStatus);
	
	if (bShow) {
		CRect rc;

		GetWindowRect (&rc);
		SetWindowPos (NULL, 0, 0, rc.Width (), rc.Height (), SWP_NOMOVE | SWP_NOZORDER);
	}
}
