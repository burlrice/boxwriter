# Microsoft Developer Studio Project File - Name="Control" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Control - Win32 IP Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Control.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Control.mak" CFG="Control - Win32 IP Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Control - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Control - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "Control - Win32 IP Debug" (based on "Win32 (x86) Application")
!MESSAGE "Control - Win32 IP Release" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 1
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Control - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /G5 /MD /W3 /GR /GX /O2 /I "..\Editor\DLL\ElementList\Win" /I "..\IP\Raster" /I "..\Editor\DLL\Document" /I "..\CxImage\CxImage\\" /I "..\IV\IvPhcApi" /I "..\NiceLabel" /I "..\Editor\DLL\Utils" /I "..\Editor\DLL\3d\DirectXExt" /I "..\Editor\DLL\Debug" /I "..\Editor\DLL\Database" /I "..\Editor\DLL\Common" /I "..\Editor\DLL\3d" /I "..\FxMphc\scr" /I "..\UsbMphc\UsbMphc\\" /I "..\MphcApi\\" /I "..\Auxiliary" /I "..\SPDevice" /D "__WINPRINTER__" /D "VALVEJET" /D "NDEBUG" /D "_AFXDLL" /D "_UNICODE" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 IvPhcApi.lib MphcApi.lib ElementList.lib UsbMphc.lib shlwapi.lib Document.lib Utils.lib Auxiliary.lib Database.lib SetupApi.lib SPDevice.lib 3d.lib Msimg32.lib Mpr.lib /nologo /entry:"wWinMainCRTStartup " /subsystem:windows /machine:I386 /libpath:"..\UsbMphc\Release" /libpath:"..\IV\IvPhcApi\Release" /libpath:"..\Editor\DLL\3d\Release" /libpath:"..\Editor\DLL\Database\Release" /libpath:"..\Editor\DLL\ElementList\Win\Release" /libpath:"..\SPDevice\Release" /libpath:"..\Auxiliary\Release" /libpath:"..\Editor\DLL\Document\Release" /libpath:"..\Editor\DLL\Utils\Release" /libpath:"..\MphcApi\Release"
# Begin Custom Build - Updating \FoxJet\$(TargetName).exe
TargetPath=.\Release\Control.exe
TargetName=Control
InputPath=.\Release\Control.exe
SOURCE="$(InputPath)"

"\FoxJet\$(TargetName).exe" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy                                                                                                                      $(TargetPath)                                                                                                                      \FoxJet\ 

# End Custom Build

!ELSEIF  "$(CFG)" == "Control - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "..\Editor\DLL\ElementList\Win" /I "...\Editor\DLL\ElementList\IP" /I "..\..\Editor\DLL\Utils" /I "..\..\MphcApi\\" /I "..\..\IP\Raster" /I "..\..\Editor\DLL\Database" /I "..\IP\Raster" /I "..\Editor\DLL\Document" /I "..\CxImage\CxImage\\" /I "..\IV\IvPhcApi" /I "..\NiceLabel" /I "..\Editor\DLL\Utils" /I "..\Editor\DLL\3d\DirectXExt" /I "..\Editor\DLL\Debug" /I "..\Editor\DLL\Database" /I "..\Editor\DLL\Common" /I "..\Editor\DLL\3d" /I "..\FxMphc\scr" /I "..\UsbMphc\UsbMphc" /I "..\MphcApi\\" /I "..\Auxiliary" /I "..\SPDevice" /D "__WINPRINTER__" /D "VALVEJET" /D "_DEBUG" /D "_AFXDLL" /D "_UNICODE" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 IvPhcApi.lib MphcApi.lib ElementList.lib Debug.lib UsbMphc.lib shlwapi.lib Document.lib Utils.lib Auxiliary.lib Database.lib SetupApi.lib SPDevice.lib 3d.lib Msimg32.lib Mpr.lib /nologo /version:1.1 /entry:"wWinMainCRTStartup " /subsystem:windows /profile /debug /machine:I386 /libpath:"..\UsbMphc\Debug" /libpath:"..\IV\IvPhcApi\Debug" /libpath:"..\Editor\DLL\3d\Debug" /libpath:"..\Editor\DLL\Database\Debug" /libpath:"..\Editor\DLL\ElementList\Win\Debug" /libpath:"..\SPDevice\Debug" /libpath:"..\Auxiliary\Debug" /libpath:"..\Editor\DLL\Debug\Debug" /libpath:"..\Editor\DLL\Document\Debug" /libpath:"..\Editor\DLL\Utils\Debug" /libpath:"..\MphcApi\Debug"
# Begin Custom Build - Updating \FoxJet\$(TargetName).exe
TargetPath=.\Debug\Control.exe
TargetName=Control
InputPath=.\Debug\Control.exe
SOURCE="$(InputPath)"

"\FoxJet\$(TargetName).exe" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy                                                               $(TargetPath)                                                                \FoxJet\ 

# End Custom Build

!ELSEIF  "$(CFG)" == "Control - Win32 IP Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Control___Win32_IP_Debug"
# PROP BASE Intermediate_Dir "Control___Win32_IP_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "IP_Debug"
# PROP Intermediate_Dir "IP_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\..\FxMphc\1.00.00\scr" /I "..\..\IP\Raster" /I "..\..\Editor\DLL\Database" /I "..\..\Editor\DLL\ElementList" /I "..\..\Editor\DLL\Common" /I "..\..\MphcApi\1.00.00" /I "..\..\SPDevice" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "..\Editor\DLL\ElementList\IP" /I "$(DDKROOT)\inc" /I "..\IP\Marksman" /I "..\IP\Marksman\elements" /I "..\Editor\DLL\Document" /I "..\CxImage\CxImage\\" /I "..\IV\IvPhcApi" /I "..\NiceLabel" /I "..\Editor\DLL\Utils" /I "..\Editor\DLL\3d\DirectXExt" /I "..\Editor\DLL\Debug" /I "..\Editor\DLL\Database" /I "..\Editor\DLL\Common" /I "..\Editor\DLL\3d" /I "..\FxMphc\scr" /I "..\MphcApi\\" /I "..\Auxiliary" /I "..\SPDevice" /D "__IPPRINTER__" /D "_DEBUG" /D "_AFXDLL" /D "_UNICODE" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Database.lib ElementList.lib SetupApi.lib MphcApi.lib SPDevice.lib /nologo /version:1.1 /subsystem:windows /profile /debug /machine:I386 /libpath:"..\..\Editor\DLL\Database\Debug" /libpath:"..\..\Editor\DLL\ElementList\Debug" /libpath:"..\..\MphcApi\1.00.00\Debug" /libpath:"..\..\SPDevice\Debug"
# ADD LINK32 Raster.lib IpElementList.lib Debug.lib User32.lib Document.lib Utils.lib Auxiliary.lib Database.lib SetupApi.lib SPDevice.lib 3d.lib Msimg32.lib /nologo /version:1.1 /entry:"wWinMainCRTStartup " /subsystem:windows /debug /machine:I386 /libpath:"..\IP\Raster\IP_Debug" /libpath:"..\Editor\DLL\Debug\IP_Debug" /libpath:"..\Editor\DLL\3d\IP_Debug" /libpath:"..\Editor\DLL\Database\IP_Debug" /libpath:"..\Editor\DLL\ElementList\IP\IP_Debug" /libpath:"..\Editor\DLL\Utils\IP_Debug" /libpath:"..\SPDevice\IP_Debug" /libpath:"..\Auxiliary\IP_Debug" /libpath:"..\Editor\DLL\Document\IP_Debug" /libpath:"..\MphcApi\Debug"
# SUBTRACT LINK32 /profile
# Begin Custom Build - Updating \FoxJet\IP\$(TargetName).exe
TargetPath=.\IP_Debug\Control.exe
TargetName=Control
InputPath=.\IP_Debug\Control.exe
SOURCE="$(InputPath)"

"\FoxJet\IP\$(TargetName).exe" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(TargetPath) \FoxJet\IP

# End Custom Build

!ELSEIF  "$(CFG)" == "Control - Win32 IP Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Control___Win32_IP_Release"
# PROP BASE Intermediate_Dir "Control___Win32_IP_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "IP_Release"
# PROP Intermediate_Dir "IP_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G5 /MD /W3 /GX /O2 /I "..\..\FxMphc\2002062801\scr" /I "..\..\..\IP\Raster" /I "..\..\..\Editor\DLL\Database" /I "..\..\..\Editor\DLL\ElementList" /I "..\..\..\Editor\DLL\Common" /I "..\..\FxMphc\1.00.00\scr" /I "..\..\IP\Raster" /I "..\..\Editor\DLL\Database" /I "..\..\Editor\DLL\ElementList" /I "..\..\Editor\DLL\Common" /I "..\..\MphcApi\1.00.00" /I "..\..\SPDevice" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /G5 /MD /W3 /GR /GX /O2 /I "..\Editor\DLL\ElementList\IP" /I "..\IP\Marksman" /I "..\IP\Marksman\elements" /I "..\Editor\DLL\Document" /I "..\CxImage\CxImage\\" /I "..\IV\IvPhcApi" /I "..\NiceLabel" /I "..\Editor\DLL\Utils" /I "..\Editor\DLL\3d\DirectXExt" /I "..\Editor\DLL\Debug" /I "..\Editor\DLL\Database" /I "..\Editor\DLL\Common" /I "..\Editor\DLL\3d" /I "..\FxMphc\scr" /I "..\MphcApi\\" /I "..\Auxiliary" /I "..\SPDevice" /D "NDEBUG" /D "__IPPRINTER__" /D "_AFXDLL" /D "_UNICODE" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 Database.lib ElementList.lib SetupApi.lib MphcApi.lib SPDevice.lib /nologo /subsystem:windows /machine:I386 /libpath:"..\..\Editor\DLL\Database\Debug" /libpath:"..\..\Editor\DLL\ElementList\Debug" /libpath:"..\..\MphcApi\1.00.00\Release" /libpath:"..\..\SPDevice\Release"
# ADD LINK32 Raster.lib IpElementList.lib Document.lib Utils.lib Auxiliary.lib Database.lib SetupApi.lib SPDevice.lib 3d.lib Msimg32.lib /nologo /entry:"wWinMainCRTStartup " /subsystem:windows /machine:I386 /libpath:"..\IP\Raster\IP_Release" /libpath:"..\Editor\DLL\3d\IP_Release" /libpath:"..\Editor\DLL\Database\IP_Release" /libpath:"..\Editor\DLL\ElementList\IP\IP_Release" /libpath:"..\Editor\DLL\Utils\IP_Release" /libpath:"..\SPDevice\IP_Release" /libpath:"..\Auxiliary\IP_Release" /libpath:"..\Editor\DLL\Document\IP_Release" /libpath:"..\MphcApi\Release"
# Begin Custom Build - Updating \FoxJet\IP\$(TargetName).exe
TargetPath=.\IP_Release\Control.exe
TargetName=Control
InputPath=.\IP_Release\Control.exe
SOURCE="$(InputPath)"

"\FoxJet\IP\$(TargetName).exe" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(TargetPath) \FoxJet\IP

# End Custom Build

!ENDIF 

# Begin Target

# Name "Control - Win32 Release"
# Name "Control - Win32 Debug"
# Name "Control - Win32 IP Debug"
# Name "Control - Win32 IP Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\2LinePromptDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BaxterConfigDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BaxterFormatDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CfgUsersDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ClientSock.cpp
# End Source File
# Begin Source File

SOURCE=.\CmdPacket.cpp
# End Source File
# Begin Source File

SOURCE=.\ConnectDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Control.cpp
# End Source File
# Begin Source File

SOURCE=.\Control.rc
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409 /i "..\Editor\DLL\Common\\"
# End Source File
# Begin Source File

SOURCE=.\ControlDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\ControlView.cpp
# End Source File
# Begin Source File

SOURCE=.\Count0828Dlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CountDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DbStartTaskDlg.cpp

!IF  "$(CFG)" == "Control - Win32 Release"

!ELSEIF  "$(CFG)" == "Control - Win32 Debug"

!ELSEIF  "$(CFG)" == "Control - Win32 IP Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Control - Win32 IP Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\Custom\IpDemo\Debug.cpp
# End Source File
# Begin Source File

SOURCE=.\DebugDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DebugLog.cpp
# End Source File
# Begin Source File

SOURCE=.\DebugPage.cpp
# End Source File
# Begin Source File

SOURCE=.\DiagDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DynDataDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\EliteFirmwareDlg.cpp

!IF  "$(CFG)" == "Control - Win32 Release"

!ELSEIF  "$(CFG)" == "Control - Win32 Debug"

!ELSEIF  "$(CFG)" == "Control - Win32 IP Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Control - Win32 IP Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Endian.cpp
# End Source File
# Begin Source File

SOURCE=.\EventsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ExportImpl.cpp
# End Source File
# Begin Source File

SOURCE=.\FirmwareDlg.cpp

!IF  "$(CFG)" == "Control - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Control - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Control - Win32 IP Debug"

!ELSEIF  "$(CFG)" == "Control - Win32 IP Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\GroupOptionsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Head.cpp
# End Source File
# Begin Source File

SOURCE=.\HeadCfg.cpp
# End Source File
# Begin Source File

SOURCE=.\HeadCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\HeadInfoCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\HpHead.cpp
# End Source File
# Begin Source File

SOURCE=.\IdsSocket.cpp
# End Source File
# Begin Source File

SOURCE=.\IdsThread.cpp
# End Source File
# Begin Source File

SOURCE=.\ImageEng.cpp
# End Source File
# Begin Source File

SOURCE=.\ImageView.cpp
# End Source File
# Begin Source File

SOURCE=.\ImportImpl.cpp
# End Source File
# Begin Source File

SOURCE=.\InfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\KeyParse.cpp
# End Source File
# Begin Source File

SOURCE=.\LabelBatchQtyDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LinePromptDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LineTab.cpp
# End Source File
# Begin Source File

SOURCE=.\LocalDbThread.cpp
# End Source File
# Begin Source File

SOURCE=.\LocalHead.cpp

!IF  "$(CFG)" == "Control - Win32 Release"

!ELSEIF  "$(CFG)" == "Control - Win32 Debug"

!ELSEIF  "$(CFG)" == "Control - Win32 IP Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Control - Win32 IP Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\NamedCountDlg.cpp

!IF  "$(CFG)" == "Control - Win32 Release"

!ELSEIF  "$(CFG)" == "Control - Win32 Debug"

!ELSEIF  "$(CFG)" == "Control - Win32 IP Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Control - Win32 IP Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\PreviewCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\PreviewDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ProdLine.cpp
# End Source File
# Begin Source File

SOURCE=.\ProgressDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RemoteHead.cpp

!IF  "$(CFG)" == "Control - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Control - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Control - Win32 IP Debug"

!ELSEIF  "$(CFG)" == "Control - Win32 IP Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\SerialPacket.cpp
# End Source File
# Begin Source File

SOURCE=.\ServerThread.cpp
# End Source File
# Begin Source File

SOURCE=.\Splash.cpp
# End Source File
# Begin Source File

SOURCE=.\StartTaskDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\Sw0835Dlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Sw0848Dlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Sw0848FieldsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Sw0848Thread.cpp
# End Source File
# Begin Source File

SOURCE=.\TaskPreview.cpp
# End Source File
# Begin Source File

SOURCE=.\TreeCtrl2.cpp
# End Source File
# Begin Source File

SOURCE=.\UdpThread.cpp
# End Source File
# Begin Source File

SOURCE=.\UsbDebugDlg.cpp

!IF  "$(CFG)" == "Control - Win32 Release"

!ELSEIF  "$(CFG)" == "Control - Win32 Debug"

!ELSEIF  "$(CFG)" == "Control - Win32 IP Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Control - Win32 IP Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\UserInfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\WordDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\2LinePromptDlg.h
# End Source File
# Begin Source File

SOURCE=.\BaxterConfigDlg.h
# End Source File
# Begin Source File

SOURCE=.\BaxterFormatDlg.h
# End Source File
# Begin Source File

SOURCE=.\ClientSock.h
# End Source File
# Begin Source File

SOURCE=.\CmdPacket.h
# End Source File
# Begin Source File

SOURCE=.\ConnectDlg.h
# End Source File
# Begin Source File

SOURCE=.\Control.h
# End Source File
# Begin Source File

SOURCE=.\ControlDoc.h
# End Source File
# Begin Source File

SOURCE=.\ControlView.h
# End Source File
# Begin Source File

SOURCE=.\Count0828Dlg.h
# End Source File
# Begin Source File

SOURCE=.\CountDlg.h
# End Source File
# Begin Source File

SOURCE=.\DbStartTaskDlg.h

!IF  "$(CFG)" == "Control - Win32 Release"

!ELSEIF  "$(CFG)" == "Control - Win32 Debug"

!ELSEIF  "$(CFG)" == "Control - Win32 IP Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Control - Win32 IP Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\DebugDlg.h
# End Source File
# Begin Source File

SOURCE=.\DebugLog.h
# End Source File
# Begin Source File

SOURCE=.\DebugPage.h
# End Source File
# Begin Source File

SOURCE=.\Defines.h
# End Source File
# Begin Source File

SOURCE=.\DiagDlg.h
# End Source File
# Begin Source File

SOURCE=.\DynDataDlg.h
# End Source File
# Begin Source File

SOURCE=.\EliteFirmwareDlg.h
# End Source File
# Begin Source File

SOURCE=.\Endian.h
# End Source File
# Begin Source File

SOURCE=.\EventsDlg.h
# End Source File
# Begin Source File

SOURCE=.\ExportImpl.h
# End Source File
# Begin Source File

SOURCE=..\..\Editor\DLL\ElementList\Extern.h
# End Source File
# Begin Source File

SOURCE=.\FirmwareDlg.h
# End Source File
# Begin Source File

SOURCE=.\GroupOptionsDlg.h
# End Source File
# Begin Source File

SOURCE=.\Head.h
# End Source File
# Begin Source File

SOURCE=.\HeadCfg.h
# End Source File
# Begin Source File

SOURCE=.\HeadCtrl.h
# End Source File
# Begin Source File

SOURCE=.\HeadInfoCtrl.h
# End Source File
# Begin Source File

SOURCE=.\Host.h
# End Source File
# Begin Source File

SOURCE=.\HpHead.h
# End Source File
# Begin Source File

SOURCE=.\IdsSocket.h
# End Source File
# Begin Source File

SOURCE=.\IdsThread.h
# End Source File
# Begin Source File

SOURCE=.\ImageEng.h
# End Source File
# Begin Source File

SOURCE=.\ImageView.h
# End Source File
# Begin Source File

SOURCE=.\ImportImpl.h
# End Source File
# Begin Source File

SOURCE=.\InfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\KeyParse.h
# End Source File
# Begin Source File

SOURCE=.\LabelBatchQtyDlg.h
# End Source File
# Begin Source File

SOURCE=.\LinePromptDlg.h
# End Source File
# Begin Source File

SOURCE=.\LineTab.h
# End Source File
# Begin Source File

SOURCE=.\LocalDbThread.h
# End Source File
# Begin Source File

SOURCE=.\LocalHead.h

!IF  "$(CFG)" == "Control - Win32 Release"

!ELSEIF  "$(CFG)" == "Control - Win32 Debug"

!ELSEIF  "$(CFG)" == "Control - Win32 IP Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Control - Win32 IP Release"

# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\NamedCountDlg.h
# End Source File
# Begin Source File

SOURCE=.\PreviewCtrl.h
# End Source File
# Begin Source File

SOURCE=.\PreviewDlg.h
# End Source File
# Begin Source File

SOURCE=.\ProdLine.h
# End Source File
# Begin Source File

SOURCE=.\ProgressDlg.h
# End Source File
# Begin Source File

SOURCE=.\RemoteHead.h

!IF  "$(CFG)" == "Control - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Control - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Control - Win32 IP Debug"

!ELSEIF  "$(CFG)" == "Control - Win32 IP Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SerialPacket.h
# End Source File
# Begin Source File

SOURCE=.\ServerThread.h
# End Source File
# Begin Source File

SOURCE=.\Splash.h
# End Source File
# Begin Source File

SOURCE=.\StartTaskDlg.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\Sw0835Dlg.h
# End Source File
# Begin Source File

SOURCE=.\Sw0848Dlg.h
# End Source File
# Begin Source File

SOURCE=.\Sw0848FieldsDlg.h
# End Source File
# Begin Source File

SOURCE=.\Sw0848Thread.h
# End Source File
# Begin Source File

SOURCE=.\TreeCtrl2.h
# End Source File
# Begin Source File

SOURCE=.\TypeDefs.h
# End Source File
# Begin Source File

SOURCE=.\UdpThread.h
# End Source File
# Begin Source File

SOURCE=.\UsbDebugDlg.h
# End Source File
# Begin Source File

SOURCE=.\UserInfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\WordDlg.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00004.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00005.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00006.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00007.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00008.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00009.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00010.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00011.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00012.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00013.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00014.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00015.bmp
# End Source File
# Begin Source File

SOURCE=.\res\box.bmp
# End Source File
# Begin Source File

SOURCE=.\res\box4.bmp
# End Source File
# Begin Source File

SOURCE=.\res\BoxView.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cancel.bmp
# End Source File
# Begin Source File

SOURCE=.\res\clear.bmp
# End Source File
# Begin Source File

SOURCE=.\res\clipboard.bmp
# End Source File
# Begin Source File

SOURCE=.\res\config1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\config2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\config3.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Configure.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Control.ico
# End Source File
# Begin Source File

SOURCE=.\res\Control.ico
# End Source File
# Begin Source File

SOURCE=.\res\Control.rc2
# End Source File
# Begin Source File

SOURCE=.\res\ControlDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\ControlDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\Counter.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dbstart.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Delay.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Editor.bmp
# End Source File
# Begin Source File

SOURCE=.\res\exit.bmp
# End Source File
# Begin Source File

SOURCE=.\res\FJ_Logo16.bmp
# End Source File
# Begin Source File

SOURCE=.\res\gray.bmp
# End Source File
# Begin Source File

SOURCE=.\res\gray_sm.bmp
# End Source File
# Begin Source File

SOURCE=.\res\green.bmp
# End Source File
# Begin Source File

SOURCE=.\res\green_sm.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Group.bmp
# End Source File
# Begin Source File

SOURCE=.\res\head_sta.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hv_error.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hv_ok.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00002.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00003.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00004.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00005.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\Info.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ink_green.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ink_red.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ink_yellow.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Locked.bmp
# End Source File
# Begin Source File

SOURCE=.\res\login1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\mainfram.bmp
# End Source File
# Begin Source File

SOURCE=.\res\network.bmp
# End Source File
# Begin Source File

SOURCE=.\res\network_lg.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ok.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Options.bmp
# End Source File
# Begin Source File

SOURCE=.\res\options2.ico
# End Source File
# Begin Source File

SOURCE=.\res\options3.ico
# End Source File
# Begin Source File

SOURCE=.\res\options4.ico
# End Source File
# Begin Source File

SOURCE=.\res\panel_im.bmp
# End Source File
# Begin Source File

SOURCE=.\res\pause.bmp
# End Source File
# Begin Source File

SOURCE=.\res\plus.bmp
# End Source File
# Begin Source File

SOURCE=.\res\preview.bmp
# End Source File
# Begin Source File

SOURCE=.\res\red.bmp
# End Source File
# Begin Source File

SOURCE=.\res\red.ico
# End Source File
# Begin Source File

SOURCE=.\res\red1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\red_sm.bmp
# End Source File
# Begin Source File

SOURCE=.\res\report.bmp
# End Source File
# Begin Source File

SOURCE=.\res\resume.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Start.bmp
# End Source File
# Begin Source File

SOURCE=.\res\start1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\start3.bmp
# End Source File
# Begin Source File

SOURCE=.\res\start4.bmp
# End Source File
# Begin Source File

SOURCE=.\res\stop.bmp
# End Source File
# Begin Source File

SOURCE=.\res\strobe_body.bmp
# End Source File
# Begin Source File

SOURCE=.\res\strobe_green.bmp
# End Source File
# Begin Source File

SOURCE=.\res\strobe_red.bmp
# End Source File
# Begin Source File

SOURCE=.\res\strobe_round.bmp
# End Source File
# Begin Source File

SOURCE=.\res\strobe_yellow.bmp
# End Source File
# Begin Source File

SOURCE=.\res\tab_back.bmp
# End Source File
# Begin Source File

SOURCE=.\res\task.bmp
# End Source File
# Begin Source File

SOURCE=.\res\temp_error.bmp
# End Source File
# Begin Source File

SOURCE=.\res\temp_ok.bmp
# End Source File
# Begin Source File

SOURCE=.\res\test1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\test128.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Test256.bmp
# End Source File
# Begin Source File

SOURCE=.\res\test257.bmp
# End Source File
# Begin Source File

SOURCE=.\res\test258.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Test32.bmp
# End Source File
# Begin Source File

SOURCE=.\res\testvalv.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar_sm.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ToolbarNet.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ToolbarNet_sm.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Unlocked.bmp
# End Source File
# Begin Source File

SOURCE=.\res\UserPrompt.bmp
# End Source File
# Begin Source File

SOURCE=.\res\x.bmp
# End Source File
# Begin Source File

SOURCE=.\res\yellow.bmp
# End Source File
# Begin Source File

SOURCE=.\res\yellow_sm.bmp
# End Source File
# Begin Source File

SOURCE=.\res\zoom.bmp
# End Source File
# Begin Source File

SOURCE=.\res\zoomfit.bmp
# End Source File
# Begin Source File

SOURCE=.\res\zoomin.bmp
# End Source File
# Begin Source File

SOURCE=.\res\zoomout.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=..\..\..\Temp\tmp.txt
# End Source File
# Begin Source File

SOURCE=.\TODO.TXT
# End Source File
# End Target
# End Project
# Section Control : {1ED47CC6-4C7F-45EE-9F39-E0E0DB2E4D7E}
# 	2:21:DefaultSinkHeaderFile:netiodev.h
# 	2:16:DefaultSinkClass:CNetIODev
# End Section
# Section Control : {19680571-ECB0-43A3-95E0-D6FE3A322117}
# 	2:5:Class:CNetIODev
# 	2:10:HeaderFile:netiodev.h
# 	2:8:ImplFile:netiodev.cpp
# End Section
