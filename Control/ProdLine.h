// ProdLine.h: interface for the CProdLine class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PRODLINE_H__7F303A63_FD06_4EDE_8DDA_B65805022988__INCLUDED_)
#define AFX_PRODLINE_H__7F303A63_FD06_4EDE_8DDA_B65805022988__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Control.h"
#include "Resource.h"
#include "TypeDefs.h"
#include "Database.h"
#include "Head.h"
#include "SerialPacket.h"
#include "AuxBox.h"
#include "Dde.h"
#include "UserElementDlg.h"
#include "nice3.h"
#include "AppVer.h"
#include "ScannerPropDlg.h"
#include "Sw0835Dlg.h"
#include "Sw0848Thread.h"
#include "RawBitmap.h"
#include "UserElementDlg.h"

//namespace Head
//{
//	class CHead;
//};

// return codes for CProdLine::StartTask
#define	TASK_START_LOGIN_REQUIRED			-5
#define	TASK_START_FAILED					-4
#define	TASK_START_NO_MASTER_HEAD			-3
#define	TASK_START_NO_ACTIVE_HEADS			-2
#define	TASK_START_NOT_FOUND				-1
#define TASK_START_CANCELLED				0
#define	TASK_START_SUCCESS					1
#define	TASK_START_SUCCEEDED(n)				((n) > 0)

class CControlView;

#include "StrobeDlg.h"

class CProdLine  
{
	friend class CDebugDlg;
	friend class CDebugPage;
	friend class CMainFrame;

public:
	
	CProdLine(HWND hWnd);
	virtual ~CProdLine();

private:
	CProdLine (const CProdLine & rhs);
	CProdLine ();

public:
	int GetDisconnectedCount ();
	void CalcProductLen  ();
	void GetCounts (Head::CHead * pFind, __int64 & lFind, __int64 & lMinCount, __int64 & lMaxCount, __int64 & lMasterCount);
	bool SetCounts (__int64 lCount, __int64 lPalletMax = 0, __int64 lUnitsPerPallet = 0, __int64 lPalletCount = 0, __int64 lPalletMin = 0);
	__int64 GetCountUntil () const;
	bool SetCountUntil (__int64 lCountUntil = -1);
	void UpdateStrobeState (CControlView * pView);
	bool IsAuxBoxConnected ( void );
	void UpdateHeadErrors (DWORD dwThreadID, eHeadStatus s );
	void OnAuxBoxDigitalInputs ( void );
	void OnAuxBoxPS2Data ( void );
	void OnAuxBoxConnectStateChange ( bool Connected );
	int OnAuxSerialData ( int PortNum, CControlView * pView );
	LONG GetAuxBoxHandle ( void );
	HWND m_hOwnerWnd;
	void ResetVerifierData ( void );
	CString m_strSerialResult;
//	void ProcessSerialData ( eSerialDevice DeviceType, const PUCHAR pData, UINT Len );
	int ProcessSerialData ( const CSerialPacket *pPkt, CControlView * pView );
	void ProcessVerifierData( LPCTSTR lpszData );
	int GetScannerPort ( void );
	tagSerialData * GetSerialData ( void );
	CString GetSerialData (const CString & str);
	static CString GetSerialData (const FoxjetDatabase::LINESTRUCT & line, const CString & str);
	void UpdateSerialData (LPCTSTR pData, int Len );
	void ShutDown ( void );
	bool ChangeUserElementData (bool bTaskStart, const CString & strKeyValue = _T (""), CMapStringToString * pvSet = NULL, int nPeriod = 0);
	bool GetLabelBatchQty (ULONG & lLabels);
	bool ChangeCounts (bool bTaskStart, __int64 lPalletMax, __int64 lUnitsPerPallet, bool bCountDlg, Head::CHead::CHANGECOUNTSTRUCT * pCount = NULL);
//	void HeadCfgUpdated ( void );
	void ResumeTask (bool bLogReport = true);
	void IdleTask (bool bLogReport = true, bool bHeadError = false);
	bool StopTask (bool bPrompt = true, CControlView * pView = NULL);
	int StartTask ( CString sTask, bool bRemoteStart, bool bStartup, bool bPromptForStop = false, CControlView * pView = NULL, 
		__int64 lPalletMax = 0, __int64 lUnitsPerPallet = 0, bool bCountDlg = false, const CString & strKeyValue = _T (""), CMapStringToString * pvUser = NULL, int nPeriod = 0,
		const CString & strLineAlt = _T (""), Head::CHead::CHANGECOUNTSTRUCT * pCount = NULL);
	int StartTask (bool bRemoteStart, bool bStartup, bool bPromptForStop = false, CControlView * pView = NULL, const CString & strKeyValue = _T (""));
	bool LoadTask(ULONG lTaskID, bool bRemoteStart, bool bStartup, bool bPromptForStop, CControlView * pView);
	bool RemoveActiveHead(Head::CHead *pHead, bool bShutdown = false);
	bool AddActiveHead(Head::CHead *pHead);
	void GetActiveHeads ( CPrintHeadList &List );
	FoxjetDatabase::LINESTRUCT m_LineRec;
	ULONG GetID ( void );
	CString GetName ( void );
	void AssignLine( FoxjetDatabase::LINESTRUCT Line);
	void DoAmsCycle ();
	void GetUserElements (Foxjet3d::UserElementDlg::CUserElementArray & vPrompt, bool bTaskStart);
	void AutoExport();

 	void GetElements (Head::CElementPtrArray & v, int nClassID);
	CString GetCurrentTask (CString * pstrKeyValue = NULL);
	ULONG GetCurrentTaskID ();
	bool DownloadSerialData (const CString & strDownload, CControlView * pView);
	void PrintTestPattern ();
	Head::CHead * GetHeadByUID (const CString & strUID);
	DWORD HasHeadErrors (CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vDisconnected);
	bool IsWaiting () const;
	void SetWaiting (bool bWait); 
	Head::CHead * GetHeadByID (ULONG lID);
	Head::CHead * GetHeadByThreadID (ULONG lID);

	bool GetSerialPending ();
	void SetSerialPending (bool bPending);
	bool IsOneToOnePrintEnabled ();

	void GetLineState (int & nLowInk, int & nOther);
	bool GetSerialResetCounts () const { return m_bSerialResetCounts; }

	int UpdateUserElements (const Foxjet3d::UserElementDlg::CUserElementArray & vElements, Head::CHead * pHead,
		bool bPrompt);

	void UpdateImages ();
	__int64 GetUnitsPerPallet ();
	__int64 GetPalletMax ();

	void UpdateDynamicData (ULONG lLineID, UINT nIndex, const CString & str, CControlView * pView);
	void OnSw0849 (const CSerialPacket * pData, CControlView * pView);
	void EnableDynamicDataDownload (bool bEnable); // sw0849

	#if __CUSTOM__ == __SW0828__
	ULONG GetLifetimeCount ();
	ULONG GetTotalBoxCount ();
	void SetTotalBoxCount (ULONG lCount);
	#endif
	
	static void InsertAscByDynID (Head::CElementPtrArray & vUser, FoxjetCommon::CBaseElement * pElement);
	static LONG GetDynamicID (FoxjetCommon::CBaseElement * pElement);

	Head::CHeadInfo GetHeadInfo (LPVOID lpAddr);

	void SetUserPrompted (Head::CElementPtrArray & list, const CString & strPrompt, const CString & strData, Head::CHead * pHead);
	void SetGojoElements (int nPeriod);

	CString GetDisplayInfo ();
	CString GetDebugInfo ();

	CString GetWarnings (CStringArray & v);

	FoxjetDatabase::REPORTSTRUCT GenerateReportRecord() const;
	void AddPrinterReportRecord(FoxjetDatabase::REPORTTYPE type, FoxjetDatabase::REPORTSTRUCT& report);

#if defined( __NICELABEL__ ) && defined ( __WINPRINTER__ )
	NiceLabel::CNiceApp m_appNiceLabel;
#endif
	
	CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> m_vHeads;
	CDiagnosticCriticalSection m_HeadListSentinel;
	CArray <FoxjetDatabase::PANELSTRUCT, FoxjetDatabase::PANELSTRUCT &> m_vPanels;

protected:
	void ParseSerialParams(CString sParams, CString &sBaud, CString &sDatabits, CString &sStopbits, CString &sParity, eSerialDevice &DeviceType );
	void BeginThread ();
	void EndThread ();
	void DeleteDebugImages ();

	bool m_bStrobeActive;
	int m_nLowInk;
	CMap <DWORD, DWORD, eHeadStatus, eHeadStatus> m_mapHeadErrors;
	LONG m_lAuxBoxID;
	CAuxBox m_AuxBox;
	FoxjetDatabase::BCSCANSTRUCT m_ScanRec;
	CString m_sNoRead;
	CString m_sMatchBarcode;
	tagSerialData *m_pSerialData;
	CPrintHeadList m_listHeads;
	int m_nScannerPort;
	CString m_strTaskname;
	int m_nConsecutiveNoReads;
	bool m_bWaiting;
	bool m_bSerialResetCounts;
	bool m_bRequireLogin;
	ULONG m_lTaskID;
	bool m_bIdledOnError;
	DWORD m_dwLastTaskState;

	static ULONG CALLBACK ThreadFunc (LPVOID lpData);

	HANDLE						m_hExit;
	CDiagnosticCriticalSection	m_csThread;
	HANDLE						m_hThread;
	COleDateTime				m_dtAms;

public:
	DWORD GetErrorState () const; 
	bool GetScannerError () const;
	void SetScannerError (bool bError);
	void ClearAllScannerErrors ();
	FoxjetDatabase::TASKSTATE GetTaskState () const;
	CString GetCount ();
	bool IsIdledOnError () const { return m_bIdledOnError; }

	ULONG GetTaskID () const { return m_lTaskID; }

//#if __CUSTOM__ == __SW0820__
	protected:
		FoxjetUtils::FIXEDSCANNERSTRUCT m_scanner;
		bool m_bScannerError;
//#endif

public:
	CDiagnosticCriticalSection		m_cs0835;
	CSw0835Dlg *					m_pdlg0835;
	Head::CHead::CHANGECOUNTSTRUCT	m_countLast;

	bool IsErrorPresent (FoxjetDatabase::HEADERRORTYPE e);
	CArray <FoxjetCommon::CStrobeItem, FoxjetCommon::CStrobeItem &> m_vStates;
	int m_nErrorIndex;
	int m_nPreviewPanel;
	int GetDriverStrobeColor () const { return m_nDriverStrobeColor; }
	int GetTabStrobeColor () const { return m_nTabStrobeColor; }
	
protected:
	static CString FormatHeadErrors (const Head::CHeadInfo & info , DWORD dw);

	int m_nDriverStrobeColor;
	int m_nTabStrobeColor;
	DWORD m_dwLastErrorState; 
	int m_nUpdateStrobeState;
	FoxjetCommon::CRawBitmap m_bmpGray;
	FoxjetCommon::CRawBitmap m_bmpRed;
	FoxjetCommon::CRawBitmap m_bmpGreen;
	FoxjetCommon::CRawBitmap m_bmpYellow;

protected:

	class CCache
	{
	public:
		CCache () { }
		virtual ~CCache () { }

		FoxjetDatabase::TASKSTRUCT m_task;

	private:
		CCache (const CCache & rhs);
		CCache & operator = (const CCache & rhs);
	};

	Head::CSyncMap <ULONG, ULONG, CCache *, CCache *> m_vCache;
	Head::CSyncMap <ULONG, ULONG, FoxjetDatabase::BOXSTRUCT, FoxjetDatabase::BOXSTRUCT &> m_vBoxCache;
	void FreeCache ();

public:
	void LoadCache (CMainFrame & frame);
	void DownloadImages ();

	Sw0848Thread::CSw0848Thread * m_pSw0848Thread; 
};

#endif // !defined(AFX_PRODLINE_H__7F303A63_FD06_4EDE_8DDA_B65805022988__INCLUDED_)
