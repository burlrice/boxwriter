#if !defined(AFX_DIAGDLG_H__98074200_F6EE_4A74_B8F1_640322E889AF__INCLUDED_)
#define AFX_DIAGDLG_H__98074200_F6EE_4A74_B8F1_640322E889AF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DiagDlg.h : header file
//

#include <afxwin.h>
#include "RoundButtons.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CDiagDlg dialog

class CDiagDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	void UpdateSerialData (CString sData, bool bFormat = true);
	CDiagDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDiagDlg)
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDiagDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CWnd *m_pParent;
	CStringArray m_v;
	FoxjetUtils::CRoundButtons m_buttons;

	// Generated message map functions
	//{{AFX_MSG(CDiagDlg)
	afx_msg void OnClose();
	afx_msg void OnClear();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIAGDLG_H__98074200_F6EE_4A74_B8F1_640322E889AF__INCLUDED_)
