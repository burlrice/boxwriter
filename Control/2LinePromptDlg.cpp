// 2LinePromptDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"       // main symbols
#include "Control.h"
#include "2LinePromptDlg.h"
#include "Database.h"
#include "WinMsg.h"
#include "Parse.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// C2LinePromptDlg dialog


C2LinePromptDlg::C2LinePromptDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg(IDD_2LINE_PROMPT_MATRIX /* IsMatrix () ? IDD_2LINE_PROMPT_MATRIX : IDD_2LINE_PROMPT */ , pParent)
{
	//{{AFX_DATA_INIT(C2LinePromptDlg)
	m_sLine1 = _T("");
	m_sLine2 = _T("");
	//}}AFX_DATA_INIT
	m_bLine2PasswordMode = false;
	m_PromptLine1.LoadString (IDS_USER_NAME);
	m_PromptLine2.LoadString (IDS_PASSWORD);
	m_nFocus = IDC_EDIT1;
}


void C2LinePromptDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(C2LinePromptDlg)
	DDX_Text(pDX, IDC_PROMPT_LINE1, m_PromptLine1);
	DDX_Text(pDX, IDC_PROMPT_LINE2, m_PromptLine2);
	DDX_Text(pDX, IDC_EDIT1, m_sLine1);
	DDX_Text(pDX, IDC_EDIT2, m_sLine2);
	//}}AFX_DATA_MAP

	//if (IsMatrix ()) 
	{
		m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
		m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	}
}


BEGIN_MESSAGE_MAP(C2LinePromptDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(C2LinePromptDlg)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// C2LinePromptDlg message handlers

BOOL C2LinePromptDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();

	if ( m_bLine2PasswordMode ) {
		if (CEdit * pEdit = (CEdit *)GetDlgItem (IDC_EDIT2)) {
			pEdit->SetPasswordChar ('*');
		}
	}

	if (CEdit * p = (CEdit *)GetDlgItem (m_nFocus)) {
		p->SetFocus();
		p->SetSel (0, -1);
	}

	return false;
}

void C2LinePromptDlg::OnShowWindow (BOOL bShow, UINT nStatus) 
{
	FoxjetCommon::CEliteDlg::OnShowWindow(bShow, nStatus);
	
	if (bShow) {
		if (IsMatrix ()) {
			if (HWND hwnd = FoxjetCommon::FindKeyboard ()) {
				if (CEdit * p = (CEdit *)GetDlgItem (IDC_EDIT1)) {
					#ifdef _DEBUG
					CString str;
					str.Format (_T ("PostMessage (0x%p, WM_CLICK_DETECTED, 0x%p, 0)"), hwnd, p->m_hWnd);
					TRACEF (str);
					#endif

					::PostMessage (hwnd, WM_CLICK_DETECTED, (WPARAM)p->m_hWnd, 0);
				}
			}
		}
	}
}
