#if !defined(AFX_2LINEPROMPTDLG_H__F30549C0_3803_4B96_AE41_FD4321B31976__INCLUDED_)
#define AFX_2LINEPROMPTDLG_H__F30549C0_3803_4B96_AE41_FD4321B31976__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// 2LinePromptDlg.h : header file
//

#include "RoundButtons.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// C2LinePromptDlg dialog

class C2LinePromptDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	C2LinePromptDlg(CWnd* pParent = NULL);   // standard constructor
	bool m_bLine2PasswordMode;
// Dialog Data
	//{{AFX_DATA(C2LinePromptDlg)
	CString	m_PromptLine1;
	CString	m_PromptLine2;
	CString	m_sLine1;
	CString	m_sLine2;
	UINT m_nFocus;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(C2LinePromptDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	FoxjetUtils::CRoundButtons m_buttons;

	// Generated message map functions
	//{{AFX_MSG(C2LinePromptDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_2LINEPROMPTDLG_H__F30549C0_3803_4B96_AE41_FD4321B31976__INCLUDED_)
