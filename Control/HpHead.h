#if !defined(AFX_HPHEAD_H__62876C03_38B8_415A_8C55_DDF918A1C8FB__INCLUDED_)
#define AFX_HPHEAD_H__62876C03_38B8_415A_8C55_DDF918A1C8FB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HpHead.h : header file
//


#include "Head.h"

/////////////////////////////////////////////////////////////////////////////
// CHpHead thread

class CHpHead : public Head::CHead
{
	DECLARE_DYNCREATE(CHpHead)
protected:
	CHpHead();           // protected constructor used by dynamic creation

// Attributes
public:
	virtual eHeadStatus GetErrorStatus ( void );
	bool IsCommportOpen () const { return !(!m_hComm || m_hComm == INVALID_HANDLE_VALUE); }

	static CString Format (const CString & str);
	static LONG CalcTime (const FoxjetDatabase::HEADSTRUCT & h, ULONG lPixels);

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHpHead)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CHpHead();

	DWORD ReadCommport (CString & str);
	bool WriteCommport (const CString & str);

	virtual int Run ();
	void FireAMS ();

	// Generated message map functions
	//{{AFX_MSG(CHpHead)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	afx_msg void OnSetIoAddress (UINT wParam, LONG lParam);

	virtual afx_msg void OnStartTask (UINT wParam, LONG lParam);
	virtual afx_msg void OnStopTask (UINT wParam, LONG lParam);
	virtual afx_msg void OnIdleTask (UINT wParam, LONG lParam);
	virtual afx_msg void OnResumeTask (UINT wParam, LONG lParam);
	virtual afx_msg void OnHeadConfigChanged (UINT wParam, LONG lParam);

	afx_msg void OnDoNothing (UINT wParam, LONG lParam);

	HANDLE m_hComm;
	CDiagnosticCriticalSection m_cs;

	INT m_nAmsInterval;
	CTime m_lastAMSCycle;

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HPHEAD_H__62876C03_38B8_415A_8C55_DDF918A1C8FB__INCLUDED_)
