// LinePromptDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "Control.h"
#include "LinePromptDlg.h"
#include "ListCtrlImp.h"
#include "Registry.h"
#include "ProdLine.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace ItiLibrary;

static const CString strRegSection = _T ("Software\\FoxJet\\Control");

/////////////////////////////////////////////////////////////////////////////

using namespace LinePromptDlg;

CLineItem::CLineItem (FoxjetDatabase::LINESTRUCT & line)
:	LINESTRUCT (line)
{

}

CLineItem::~CLineItem ()
{
}

CString CLineItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:		return m_strName;
	case 1:		return m_strDesc;
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CLinePromptDlg dialog


CLinePromptDlg::CLinePromptDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent /*=NULL*/)
:	m_db (db),
	FoxjetCommon::CEliteDlg (IDD_LINE_PROMPT_MATRIX /* FoxjetDatabase::IsMatrix () ? IDD_LINE_PROMPT_MATRIX : IDD_LINE_PROMPT */ , pParent)
{
	//{{AFX_DATA_INIT(CLinePromptDlg)
	//}}AFX_DATA_INIT
}


void CLinePromptDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLinePromptDlg)
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
}


BEGIN_MESSAGE_MAP(CLinePromptDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CLinePromptDlg)
	ON_NOTIFY(LVN_ITEMCHANGED, LV_LINE, OnItemchangedLine)
	//}}AFX_MSG_MAP
	ON_NOTIFY(NM_DBLCLK, LV_LINE, OnDblclkLine)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLinePromptDlg message handlers

BOOL CLinePromptDlg::OnInitDialog() 
{
	using namespace ListCtrlImp;

	CArray <FoxjetDatabase::LINESTRUCT, FoxjetDatabase::LINESTRUCT &> vLines;
	CArray <CColumn, CColumn> vCols;

	FoxjetCommon::CEliteDlg::OnInitDialog();

	FoxjetDatabase::GetPrinterLines (m_db, GetPrinterID (m_db), vLines); //FoxjetDatabase::GetLineRecords (m_db, vLines);

	vCols.Add (CColumn (LoadString (IDS_NAME)));
	vCols.Add (CColumn (LoadString (IDS_DESCRIPTION), 100));

	m_lv.Create (LV_LINE, vCols, this, ::strRegSection);
	CString strLine = FoxjetDatabase::GetProfileString (::strRegSection + _T ("\\CLinePromptDlg"), 
		_T ("Line"), _T (""));

	for (int i = 0; i < vLines.GetSize(); i++) {
		CLineItem * p = new CLineItem (vLines [i]);

		int nIndex = m_lv.InsertCtrlData (p);

		if (!p->m_strName.CompareNoCase (strLine))
			m_lv->SetItemState (nIndex, LVIS_FOCUSED | LVIS_SELECTED, 0xFFFF);
	}

	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize () == 0) 
		m_lv->SetItemState (0, LVIS_FOCUSED | LVIS_SELECTED, 0xFFFF);

	return FALSE;
}

void CLinePromptDlg::OnDblclkLine(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnOK ();	
	*pResult = 0;
}

void CLinePromptDlg::OnOK ()
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		m_line = * (CLineItem *)m_lv.GetCtrlData (sel [0]);
		FoxjetDatabase::WriteProfileString (::strRegSection + _T ("\\CLinePromptDlg"), _T ("Line"), m_line.m_strName);
		FoxjetCommon::CEliteDlg::OnOK ();
	}
	else 
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CLinePromptDlg::OnItemchangedLine(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	CLongArray sel = ItiLibrary::GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		LINESTRUCT line = * (CLineItem *)m_lv.GetCtrlData (sel [0]);

		if (m_strTask.GetLength ()) {
			CString str, strTask;

			try {
				strTask = CProdLine::GetSerialData (line, m_strTask); //m_strTask.Mid (line.m_nBufferOffset, line.m_nSignificantChars);
			}
			catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

			str.Format (LoadString (IDS_STARTINGTASKON), strTask);
			SetDlgItemText (LBL_TASK, str);
		}
	}

	
	*pResult = 0;
}
