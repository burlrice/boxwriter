#if !defined(AFX_TESTVIEW_H__9B7CA06B_A1CE_4144_9E06_646BE7BDCD91__INCLUDED_)
#define AFX_TESTVIEW_H__9B7CA06B_A1CE_4144_9E06_646BE7BDCD91__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImageView.h : header file
//

#include <afxtempl.h>
#include "HeadConfigDlg.h"

using namespace Foxjet3d::HeadConfigDlg;

/////////////////////////////////////////////////////////////////////////////
// CImageView view

class CImageView : public CScrollView
{
protected:
	DECLARE_DYNCREATE(CImageView)

// Attributes
public:
	CImageView();           // protected constructor used by dynamic creation
	virtual ~CImageView();
	void SetActiveDocument ( CControlDoc * pDoc );
	void SetPreviewPanel(Head::CHead *pHead, ULONG TaskID, ULONG lPanelID = -1);
	void OnHeadConfigUpdated ();
	void SaveScrollPos ();
	double CalcZoomFit ();

// Operations
public:
	static const double m_dZoomMin;
	static const double m_dZoomMax;
	static const int m_nMargin;
	static const double m_dZoomIncrement;

	bool m_bZoomToFit;

	bool SetZoom (double dZoom, bool bSnap = true);
	double GetZoom () const;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImageView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	void DrawPreview ( CDC *pDC );
	void GetWorkingHead (FoxjetDatabase::HEADSTRUCT & workingHead);
	void Draw (CDC & dc, FoxjetDatabase::HEADSTRUCT & workingHead);
	void Draw (CDC & dc, FoxjetDatabase::HEADSTRUCT & workingHead, FoxjetDatabase::VALVESTRUCT & valve, const CArray <Head::CHead *, Head::CHead *> & vHeads);

	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);

	static int Find (const CArray <Head::CHead *, Head::CHead *> & vHeads, ULONG lHeadID);

	double m_dZoom;
	CSize m_ScrollSizes;
	CControlDoc *m_pDoc;
	ULONG m_lPanelID;
	ULONG m_lTaskID;
	CRect m_rcPanel;
	CBitmap *m_pPreviewBitmap;
	CDC *m_pPreviewDC;
	CPoint * m_ptDrag;
	CPoint * m_ptScroll;
	CBitmap m_bmpZoom;
	COLORREF m_rgbTransparent;
	CSize m_sizeZoom;
	CRect m_rcZoom;
	CRect m_rcZoomIn;
	CRect m_rcZoomOut;
	CRect m_rcError;
	CFont m_fntWarning;


	ULONG m_lLineID;
	CArray <Foxjet3d::HeadConfigDlg::CPanel, Foxjet3d::HeadConfigDlg::CPanel &> m_vPanels;

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CImageView)
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTVIEW_H__9B7CA06B_A1CE_4144_9E06_646BE7BDCD91__INCLUDED_)
