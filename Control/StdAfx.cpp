// stdafx.cpp : source file that includes just the standard includes
//	Control.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include <AFXPRIV.H>
#include "TemplExt.h"
#include "Resource.h"
#include "Debug.h"
#include "Utils.h"
#include "Database.h"

using namespace FoxjetDatabase;

void Repaint (CWnd & wnd, LPCTSTR lpszFile, ULONG lLine) 
{ 
	wnd.Invalidate (FALSE); 
	wnd.RedrawWindow (NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW); 
	/* wnd.RedrawWindow (); */ 
	/* wnd.UpdateWindow (); */ 
	
	#ifdef _DEBUG
	CString str;
	TCHAR szClass [128] = { 0 };

	::GetClassName (wnd.m_hWnd, szClass, ARRAYSIZE (szClass) - 1);
	str.Format (_T ("REPAINT: 0x%p, %s"), wnd.m_hWnd, szClass);
	//CDebug::Trace (str, true, lpszFile, lLine);	
	#endif
}

SIZE GetDialogSize(INT nResourceId, BOOL bApproximateCalcMethod, LPCTSTR strDllName)
{
	typedef struct {  
	  WORD      dlgVer; 
	  WORD      signature; 
	  DWORD     helpID; 
	  DWORD     exStyle; 
	  DWORD     style; 
	  WORD      cDlgItems; 
	  short     x; 
	  short     y; 
	  short     cx; 
	  short     cy; 
	  /*
	  sz_Or_Ord menu; 
	  sz_Or_Ord windowClass; 
	  WCHAR     title[titleLen]; 
	// The following members exist only if the style member is 
	// set to DS_SETFONT or DS_SHELLFONT.
	  WORD     pointsize; 
	  WORD     weight; 
	  BYTE     italic;
	  BYTE     charset; 
	  WCHAR    typeface[stringLen];  
	  */
	} DLGTEMPLATEEX; 

    SIZE dlgSize = {0}; 
    HINSTANCE hModule = 0;

    if(strDllName != NULL)   
        hModule= ::LoadLibrary(strDllName);              
    else
        hModule = ::GetModuleHandle(NULL);   

    HRSRC hRsrc = ::FindResource(hModule, MAKEINTRESOURCE(nResourceId), RT_DIALOG);  

    HGLOBAL hTemplate = ::LoadResource(hModule, hRsrc);  

    DLGTEMPLATE* pTemplate = (DLGTEMPLATE*)::LockResource(hTemplate);

    if (bApproximateCalcMethod) // the approximate method of calculating
    {
        LONG dlgBaseUnits = GetDialogBaseUnits();
        int baseunitX = LOWORD(dlgBaseUnits), baseunitY = HIWORD(dlgBaseUnits);

		if (pTemplate->style == 0xFFFF0001) {
			DLGTEMPLATEEX * pEx = (DLGTEMPLATEEX *)pTemplate;

			dlgSize.cx = MulDiv(pEx->cx, baseunitX, 4);
			dlgSize.cy = MulDiv(pEx->cy, baseunitY, 8);
		}
		else {
			dlgSize.cx = MulDiv(pTemplate->cx, baseunitX, 4);
			dlgSize.cy = MulDiv(pTemplate->cy, baseunitY, 8);
		}
    }
    else // the accurate method of calculation
    {
        RECT rc = {0};
		FoxjetCommon::CEliteDlg dlg;

		dlg.CreateIndirect (hTemplate, NULL);
        dlg.GetWindowRect (&rc);

        dlgSize.cx = rc.right - rc.left;
        dlgSize.cy = rc.bottom - rc.top;
    }

    UnlockResource(hTemplate);
    ::FreeResource(hTemplate);

    if(strDllName != NULL)
        ::FreeLibrary(hModule);

    return dlgSize;
}

#ifdef _DEBUG
CString GetStyleString (UINT n, UINT nEX)
{
	struct
	{
		UINT m_dw;
		LPCTSTR m_lpsz;
	} const style [] = 
	{
		{ WS_BORDER,				_T ("WS_BORDER"),				},
		{ WS_CAPTION,				_T ("WS_CAPTION"),				},
		{ WS_CHILD,					_T ("WS_CHILD"),				},
		{ WS_CHILDWINDOW,			_T ("WS_CHILDWINDOW"),			},
		{ WS_CLIPCHILDREN,			_T ("WS_CLIPCHILDREN"),			},
		{ WS_CLIPSIBLINGS,			_T ("WS_CLIPSIBLINGS"),			},
		{ WS_DISABLED,				_T ("WS_DISABLED"),				},
		{ WS_DLGFRAME,				_T ("WS_DLGFRAME"),				},
		{ WS_GROUP,					_T ("WS_GROUP"),				},
		{ WS_HSCROLL,				_T ("WS_HSCROLL"),				},
		{ WS_ICONIC,				_T ("WS_ICONIC"),				},
		{ WS_MAXIMIZE,				_T ("WS_MAXIMIZE"),				},
		{ WS_MAXIMIZEBOX,			_T ("WS_MAXIMIZEBOX"),			},
		{ WS_MINIMIZE,				_T ("WS_MINIMIZE"),				},
		{ WS_MINIMIZEBOX,			_T ("WS_MINIMIZEBOX"),			},
		{ WS_OVERLAPPED,			_T ("WS_OVERLAPPED"),			},
		{ WS_OVERLAPPEDWINDOW,		_T ("WS_OVERLAPPEDWINDOW"),		},
		{ WS_POPUP,					_T ("WS_POPUP"),				},
		{ WS_POPUPWINDOW,			_T ("WS_POPUPWINDOW"),			},
		{ WS_SIZEBOX,				_T ("WS_SIZEBOX"),				},
		{ WS_SYSMENU,				_T ("WS_SYSMENU"),				},
		{ WS_TABSTOP,				_T ("WS_TABSTOP"),				},
		{ WS_THICKFRAME,			_T ("WS_THICKFRAME"),			},
		{ WS_TILED,					_T ("WS_TILED"),				},
		{ WS_TILEDWINDOW,			_T ("WS_TILEDWINDOW"),			},
		{ WS_VISIBLE,				_T ("WS_VISIBLE"),				},
		{ WS_VSCROLL,				_T ("WS_VSCROLL"),				},

		{ ES_AUTOHSCROLL,			_T ("ES_AUTOHSCROLL"),			},
		{ ES_AUTOVSCROLL,			_T ("ES_AUTOVSCROLL"),			},
		{ ES_CENTER,				_T ("ES_CENTER"),				},
		{ ES_LEFT,					_T ("ES_LEFT"),					},
		{ ES_LOWERCASE,				_T ("ES_LOWERCASE"),			},
		{ ES_MULTILINE,				_T ("ES_MULTILINE"),			},
		{ ES_NOHIDESEL,				_T ("ES_NOHIDESEL"),			},
		{ ES_OEMCONVERT,			_T ("ES_OEMCONVERT"),			},
		{ ES_PASSWORD,				_T ("ES_PASSWORD"),				},
		{ ES_RIGHT,					_T ("ES_RIGHT"),				},
		{ ES_UPPERCASE,				_T ("ES_UPPERCASE"),			},
		{ ES_READONLY,				_T ("ES_READONLY"),				},
		{ ES_WANTRETURN,			_T ("ES_WANTRETURN "),			},
	};
	struct
	{
		UINT m_dw;
		LPCTSTR m_lpsz;
	} const exstyle [] = 
	{
		{ WS_EX_ACCEPTFILES,		_T ("WS_EX_ACCEPTFILES"),		},	
		{ WS_EX_APPWINDOW,			_T ("WS_EX_APPWINDOW"),			},	
		{ WS_EX_CLIENTEDGE,			_T ("WS_EX_CLIENTEDGE"),		},		
		//{ WS_EX_COMPOSITED,			_T ("WS_EX_COMPOSITED"),		},
		{ WS_EX_CONTEXTHELP,		_T ("WS_EX_CONTEXTHELP"),		},	
		{ WS_EX_CONTROLPARENT,		_T ("WS_EX_CONTROLPARENT"),		},	
		{ WS_EX_DLGMODALFRAME,		_T ("WS_EX_DLGMODALFRAME"),		},	
		//{ WS_EX_LAYERED,			_T ("WS_EX_LAYERED"),			},	
		//{ WS_EX_LAYOUTRTL,			_T ("WS_EX_LAYOUTRTL"),			},	
		{ WS_EX_LEFT,				_T ("WS_EX_LEFT"),				},	
		{ WS_EX_LEFTSCROLLBAR,		_T ("WS_EX_LEFTSCROLLBAR"),		},	
		{ WS_EX_LTRREADING,			_T ("WS_EX_LTRREADING"),		},	
		{ WS_EX_MDICHILD,			_T ("WS_EX_MDICHILD"),			},	
		//{ WS_EX_NOACTIVATE,			_T ("WS_EX_NOACTIVATE"),		},	
		//{ WS_EX_NOINHERITLAYOUT,	_T ("WS_EX_NOINHERITLAYOUT"),	},	
		{ WS_EX_NOPARENTNOTIFY,		_T ("WS_EX_NOPARENTNOTIFY"),	},	
		{ WS_EX_OVERLAPPEDWINDOW,	_T ("WS_EX_OVERLAPPEDWINDOW"),	},	
		{ WS_EX_PALETTEWINDOW,		_T ("WS_EX_PALETTEWINDOW"),		},	
		{ WS_EX_RIGHT,				_T ("WS_EX_RIGHT"),				},	
		{ WS_EX_RIGHTSCROLLBAR,		_T ("WS_EX_RIGHTSCROLLBAR"),	},	
		{ WS_EX_RTLREADING,			_T ("WS_EX_RTLREADING"),		},	
		{ WS_EX_STATICEDGE,			_T ("WS_EX_STATICEDGE"),		},	
		{ WS_EX_TOOLWINDOW,			_T ("WS_EX_TOOLWINDOW"),		},	
		{ WS_EX_TOPMOST,			_T ("WS_EX_TOPMOST"),			},
		{ WS_EX_TRANSPARENT,		_T ("WS_EX_TRANSPARENT"),		},			
		{ WS_EX_WINDOWEDGE,			_T ("WS_EX_WINDOWEDGE"),		},
	}; 
	CString str;

	for (int i = 0; i < ARRAYSIZE (style); i++)
		if (style [i].m_dw & n)
			str += style [i].m_lpsz + CString (_T (" "));

	for (int i = 0; i < ARRAYSIZE (exstyle); i++)
		if (exstyle [i].m_dw & nEX)
			str += exstyle [i].m_lpsz + CString (_T (" "));

	return str;
}
#endif

#ifdef _DEBUG

#pragma pack(push, 1)
	typedef struct {  
	  WORD      dlgVer; 
	  WORD      signature; 
	  DWORD     helpID; 
	  DWORD     exStyle; 
	  DWORD     style; 
	  WORD      cDlgItems; 
	  short     x; 
	  short     y; 
	  short     cx; 
	  short     cy; 
	  /*
	  sz_Or_Ord menu; 
	  sz_Or_Ord windowClass; 
	  WCHAR     title[titleLen]; 
	// The following members exist only if the style member is 
	// set to DS_SETFONT or DS_SHELLFONT.
	  WORD     pointsize; 
	  WORD     weight; 
	  BYTE     italic;
	  BYTE     charset; 
	  WCHAR    typeface[stringLen];  
	  */
	} DLGTEMPLATEEX; 
#pragma pack(pop)

static inline BOOL IsDialogEx(const DLGTEMPLATE* pTemplate)
{
	return ((DLGTEMPLATEEX*)pTemplate)->signature == 0xFFFF;
}

static BOOL CALLBACK EnumResNameProc (HMODULE hModule, LPCTSTR lpszType, LPTSTR lpszName, LPARAM lParam)
{
	CArray <UINT, UINT> & v = * (CArray <UINT, UINT> *)lParam;

	if ((int)lpszName < 65536) {
		//CString str; str.Format (_T ("%d"), LOWORD (lpszName)); TRACEF (CString (_T (": ")) + str);
		v.Add ((int)lpszName);
	}

	return TRUE;
}

BYTE * GetFontSizeField (const DLGTEMPLATE* pTemplate)
{
	BOOL bDialogEx = IsDialogEx(pTemplate);
	WORD* pw;

	if (bDialogEx)
		pw = (WORD*)((DLGTEMPLATEEX*)pTemplate + 1);
	else
		pw = (WORD*)(pTemplate + 1);

	if (*pw == (WORD)-1)        // Skip menu name string or ordinal
		pw += 2; // WORDs
	else
		while(*pw++);

	if (*pw == (WORD)-1)        // Skip class name string or ordinal
		pw += 2; // WORDs
	else
		while(*pw++);

	while (*pw++);          // Skip caption string

	return (BYTE*)pw;
}

static int CALLBACK DialogProc (HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	return 0;
}

void CheckDlgSizes ()
{
	class CMemDlg : public FoxjetCommon::CEliteDlg
	{
	public:
		BOOL CreateIndirect(HGLOBAL hDialogTemplate, CWnd* pParentWnd, HINSTANCE hInst) 
		{
			return FoxjetCommon::CEliteDlg::CreateIndirect(hDialogTemplate, pParentWnd, hInst);
		}
	};

	using namespace FoxjetCommon;

	CStringArray vFiles, vWarn;
	const CSize sizeMain = GetDialogSize (IDD_CONTROL_FORM_MATRIX, true);
	struct
	{
		LPCTSTR m_lpsz;
		UINT m_nID;
	}
	const exclude [] = 
	{ 
		_T ("Control.exe"),		IDD_CONTROL_FORM,
		_T ("Control.exe"),		IDD_CONTROL_FORM_MATRIX,
		_T ("mkdraw.exe"),		1136,			// IDD_HEADBAR
		_T ("mkdraw.exe"),		1013,			// IDD_FONTBAR
		_T ("Control.exe"),		149,			// IDD_START_TASK
		_T ("mkdraw.exe"),		1067,			// IDD_OPEN
		_T ("mkdraw.exe"),		1091,			// IDD_EXPORT
		_T ("3d.dll"),			3000,			// IDD_BOXDIM
		_T ("3d.dll"),			3015,			// IDD_HEAD
		_T ("3d.dll"),			3040,			// IDD_LINE
		_T ("3d.dll"),			3054,			// IDD_HEAD_SM
		_T ("ElementList.dll"),	2193,			// IDD_BARCOCDE
		_T ("ElementList.dll"),	2197,			// IDD_DATABASE
		_T ("ElementList.dll"),	2206,			// IDD_BARCODEPARAMS
		_T ("ElementList.dll"),	2210,			// IDD_I2O5BARCODEPARAMS
		_T ("ElementList.dll"),	2233,			// IDD_C128PARAMS
		_T ("ElementList.dll"),	2234,			// IDD_C39PARAMS
		_T ("ElementList.dll"),	2235,			// IDD_C93PARAMS
		_T ("Keyboard.exe"),	102,			
		_T ("Keyboard.exe"),	1225,			
		_T ("Keyboard.exe"),	1226,			
	};

	GetFiles (GetHomeDir (), _T ("*.exe"), vFiles);
	GetFiles (GetHomeDir (), _T ("*.dll"), vFiles);
	::AfxEnableControlContainer (NULL);

	for (int i = 0; i < vFiles.GetSize (); i++) {
		if (HMODULE h = ::LoadLibrary (vFiles [i])) {
			const HINSTANCE hOld = ::AfxGetResourceHandle ();
			CArray <UINT, UINT> v;
			const CString strFile = vFiles [i];
	
			::AfxSetResourceHandle (h);
			TRACEF (strFile);
			::EnumResourceNames (h, RT_DIALOG, EnumResNameProc, (LPARAM)&v);

			for (int n = 0; n < v.GetSize (); n++) {
				const ULONG lID = v [n];
				CString str;
				CSize size (0, 0);
				bool bExclude = false;
				
				for (int j = 0; j < ARRAYSIZE (exclude); j++) {
					if (strFile.Find (exclude [j].m_lpsz) != -1 && lID == exclude [j].m_nID) {
						bExclude = true;
						break;
					}
				}

				if (!bExclude) {
					if (HRSRC hRsrc = ::FindResource (h, (LPCTSTR)lID, RT_DIALOG)) {
						if (HGLOBAL hTemplate = ::LoadResource (h, hRsrc)) {
							if (DLGTEMPLATE * pTemplate = (DLGTEMPLATE *)::LockResource(hTemplate)) {
								CRect rc (0, 0, 0, 0);
								CMemDlg dlg;
								CSize size;
								CString strFace;
								WORD wSize = 0;
								bool bFail = false;

								CDialogTemplate::GetFont(pTemplate, strFace, wSize);
								ASSERT (strFace.CompareNoCase (_T ("Microsoft Sans Serif")) == 0);

								LONG dlgBaseUnits = GetDialogBaseUnits();
								int baseunitX = LOWORD (dlgBaseUnits), baseunitY = HIWORD(dlgBaseUnits);

								if (IsDialogEx (pTemplate)) {
								//if (pTemplate->style == 0xFFFF0001) {
									DLGTEMPLATEEX * pEx = (DLGTEMPLATEEX *)pTemplate;

									pEx->style &= ~(WS_CHILD | WS_VISIBLE);
									size.cx = MulDiv(pEx->cx, baseunitX, 4);
									size.cy = MulDiv(pEx->cy, baseunitY, 8);
								}
								else {
									pTemplate->style &= ~(WS_CHILD | WS_VISIBLE);
									size.cx = MulDiv(pTemplate->cx, baseunitX, 4);
									size.cy = MulDiv(pTemplate->cy, baseunitY, 8);
								}

								if (HWND hwnd = ::CreateDialogIndirect (h, pTemplate, NULL, DialogProc)) {
									::GetWindowRect (hwnd, &rc);
									::DestroyWindow (hwnd);
								}
								else {
									str.Format (_T ("CreateIndirect failed: %-30s [%-5d, 0x%p 0x%p]: %-4d, %-4d, %s, %dpt"), strFile, lID, pTemplate->style, pTemplate->dwExtendedStyle, size.cx, size.cy, strFace, wSize);
									vWarn.Add (str);
								}

								if (size.cx > sizeMain.cx && size.cy > sizeMain.cy) {
									str.Format (_T ("too large:             %-30s [%-5d, 0x%p 0x%p]: %-4d, %-4d, %s, %dpt"), strFile, lID, pTemplate->style, pTemplate->dwExtendedStyle, size.cx, size.cy, strFace, wSize);
									vWarn.Add (str);
									bFail = true;
								}
								if (size.cx > sizeMain.cx) {
									str.Format (_T ("too wide:              %-30s [%-5d, 0x%p 0x%p]: %-4d, %-4d, %s, %dpt"), strFile, lID, pTemplate->style, pTemplate->dwExtendedStyle, size.cx, size.cy, strFace, wSize);
									vWarn.Add (str);
									bFail = true;
								}
								if (size.cy > sizeMain.cy) {
									str.Format (_T ("too tall:              %-30s [%-5d, 0x%p 0x%p]: %-4d, %-4d, %s, %dpt"), strFile, lID, pTemplate->style, pTemplate->dwExtendedStyle, size.cx, size.cy, strFace, wSize);
									vWarn.Add (str);
									bFail = true;
								}

								while (bFail) {
									CMemDlg dlg;
									CDialogTemplate t (pTemplate);

									t.SetFont (strFace, --wSize);

									VERIFY (dlg.CreateIndirect (t.m_hTemplate, NULL, h));

									if (dlg.m_hWnd) {
										dlg.GetWindowRect (&rc);
										size = rc.Size ();
									}
									
									if (size.cx <= sizeMain.cx && size.cy <= sizeMain.cy) {
										int nIndex = vWarn.GetSize () - 1;
										CString strWarn = vWarn [nIndex];
										str.Format (_T (" [recommended font size: %d]"), wSize);
										vWarn.SetAt (nIndex, strWarn + str);
										bFail = false;
									}
								}

								::UnlockResource (pTemplate);
							}

							::FreeResource (hTemplate);
						}
					}
				}
			}

			::AfxSetResourceHandle (hOld);
			::FreeLibrary (h);
		}
	}

	for (int i = 0; i < vWarn.GetSize (); i++) {
		TRACEF (vWarn [i]);
	}

	ASSERT (vWarn.GetSize () == 0);
}

#endif //_DEBUG

bool IsSocketDataPending (SOCKET s)
{
	struct timeval timeout;
	fd_set read;

	memset (&read, 0, sizeof (read));
	read.fd_count = 1;
	read.fd_array [0] = s;

	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

	int nSelect = ::select (0, &read, NULL, NULL, &timeout);

	return (nSelect != SOCKET_ERROR && nSelect > 0) ? true : false;
}

