#if !defined(AFX_DYNDATADLG_H__CDDD2613_C79D_4DE4_9F46_F3AB2EA9ECA3__INCLUDED_)
#define AFX_DYNDATADLG_H__CDDD2613_C79D_4DE4_9F46_F3AB2EA9ECA3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DynDataDlg.h : header file
//

#include "ListCtrlImp.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CDynDataDlg dialog
namespace DynDataDlg
{
	class CDataItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CDataItem (LONG lID = 0, const CString & str = _T (""));
		virtual ~CDataItem ();

		virtual CString GetDispText (int nColumn) const;

		CString m_strData;
		LONG m_lID;
	};

	class CDynDataDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CDynDataDlg(CWnd* pParent = NULL);   // standard constructor

	public:

	// Dialog Data
		//{{AFX_DATA(CDynDataDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		CStringArray m_vData;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CDynDataDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

		// Generated message map functions
		//{{AFX_MSG(CDynDataDlg)
		afx_msg void OnEdit();
		afx_msg void OnDblclkData(NMHDR* pNMHDR, LRESULT* pResult);
		virtual BOOL OnInitDialog();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; // namespace DynDataDlg

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DYNDATADLG_H__CDDD2613_C79D_4DE4_9F46_F3AB2EA9ECA3__INCLUDED_)
