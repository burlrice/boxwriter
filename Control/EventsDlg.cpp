// EventsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "EventsDlg.h"
#include "HeadDlg.h"
#include "EnTabCtrl.h"
#include "ProdLine.h"
#include "Head.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ItiLibrary;
using namespace ListGlobals;
using namespace Foxjet3d;
using namespace Head;

CEventsDlg::CEventItem::CEventItem ()
:	m_hPC (NULL),
	m_hEOP (NULL)
{
}

CString CEventsDlg::CEventItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:	return m_strAddr;
	case 1: { CString str; str.Format (_T ("0x%p"), m_nThreadID); return str; }
	case 2: { CString str; str.Format (_T ("0x%p"), m_hPC); return str; }
	case 3: return m_strPcState;
	case 4: { CString str; str.Format (_T ("0x%p"), m_hEOP); return str; }
	case 5: return m_strEopState;
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CEventsDlg property page

IMPLEMENT_DYNCREATE(CEventsDlg, CPropertyPage)

CEventsDlg::CEventsDlg() : CPropertyPage(CEventsDlg::IDD)
{
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;

	//{{AFX_DATA_INIT(CEventsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CEventsDlg::~CEventsDlg()
{
}

void CEventsDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEventsDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEventsDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CEventsDlg)
	ON_BN_CLICKED(BTN_REFRESH, OnRefresh)
	ON_BN_CLICKED(BTN_SAVE, OnSave)
	ON_BN_CLICKED(BTN_PULSE, OnPulse)
	ON_BN_CLICKED(BTN_RESET, OnReset)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEventsDlg message handlers

BOOL CEventsDlg::OnInitDialog() 
{
	using namespace ListCtrlImp;

	CArray <CColumn, CColumn> vCols;

	CPropertyPage::OnInitDialog();

	vCols.Add (CColumn (_T ("Card"), 160));
	vCols.Add (CColumn (_T ("ThreadID"), 160));
	vCols.Add (CColumn (_T ("Photocell"), 160));
	vCols.Add (CColumn (_T ("State")));
	vCols.Add (CColumn (_T ("End of Print"), 160));
	vCols.Add (CColumn (_T ("State")));

	m_lv.Create (LV_EVENTS, vCols, this, defElements.m_strRegSection);
	
	if (CControlDoc * pDoc = (CControlDoc *) theApp.GetActiveDocument ()) {
		CStringArray vLines;

		pDoc->GetProductLines (vLines);

		for (int i = 0; i < vLines.GetSize (); i++) {
			if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
				CPrintHeadList heads;

				pLine->GetActiveHeads (heads);

				for (POSITION pos = heads.GetStartPosition(); pos; ) {
					ULONG lKey; 
					Head::CHead * pHead = NULL;

					heads.GetNextAssoc (pos, lKey, (void *&)pHead);

					if (pHead) {
						Head::CHeadInfo info = CHead::GetInfo (pHead);
						CEventItem * pItem = new CEventItem ();

						#ifdef __WINPRINTER__
						pItem->m_strAddr = CHeadDlg::GetPHC (_tcstoul (info.GetUID (), NULL, 16));
						#else
						pItem->m_strAddr = info.m_Config.m_HeadSettings.m_strUID;
						#endif

						pItem->m_nThreadID = pHead->m_nThreadID;

						SEND_THREAD_MESSAGE (info, TM_GET_PHOTO_EVENT_HANDLE, &pItem->m_hPC);
						SEND_THREAD_MESSAGE (info, TM_GET_EOP_EVENT_HANDLE, &pItem->m_hEOP);

						m_lv.InsertCtrlData (pItem);
					}
				}
			}
		}

		m_lv->SortItems (ItiLibrary::ListCtrlImp::CListCtrlImp::CompareFunc, 0);
	}

	OnRefresh ();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CEventsDlg::OnRefresh() 
{
	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (CEventItem * p = (CEventItem *)m_lv.GetCtrlData (i)) {
			{
				DWORD dw = ::WaitForSingleObject (p->m_hPC, 0);
				p->m_strPcState.Format (_T ("%d"), dw);

				/*
				switch (dw) {
				case WAIT_ABANDONED: p->m_strPcState += _T (" [WAIT_ABANDONED]");	break;
				case WAIT_OBJECT_0:	 p->m_strPcState += _T (" [WAIT_OBJECT_0]");	break;
				case WAIT_TIMEOUT:	 p->m_strPcState += _T (" [WAIT_TIMEOUT]");		break;
				}
				*/
			}

			{
				DWORD dw = ::WaitForSingleObject (p->m_hEOP, 0);
				p->m_strEopState.Format (_T ("%d"), dw);

				/*
				switch (dw) {
				case WAIT_ABANDONED: p->m_strEopState += _T (" [WAIT_ABANDONED]");	break;
				case WAIT_OBJECT_0:	 p->m_strEopState += _T (" [WAIT_OBJECT_0]");	break;
				case WAIT_TIMEOUT:	 p->m_strEopState += _T (" [WAIT_TIMEOUT]");	break;
				}
				*/
			}

			m_lv.UpdateCtrlData (p);
		}
	}
}

void CEventsDlg::OnSave() 
{
	CString strFile = FoxjetDatabase::GetHomeDir () + _T ("\\") + _T ("events.txt");
	TRACEF (strFile);

	CFileDialog dlg (FALSE, _T ("*.*"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
		_T ("All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();

		if (!ItiLibrary::Export (m_lv, strFile)) {
			CString str;

			str.Format (LoadString (IDS_EXPORTFAILED), strFile);
			MsgBox (* this, str);
		}
	}
}

void CEventsDlg::OnPulse() 
{
	using namespace FoxjetCommon;

	CLongArray sel = GetSelectedItems (m_lv);

	for (int i = 0; i < sel.GetSize (); i++) {
		if (CEventItem * p = (CEventItem *)m_lv.GetCtrlData (sel [i])) {
			::PulseEvent (p->m_hPC);
			::PulseEvent (p->m_hEOP);
		}
	}

	OnRefresh ();
}

void CEventsDlg::OnReset() 
{
	using namespace FoxjetCommon;

	CLongArray sel = GetSelectedItems (m_lv);

	for (int i = 0; i < sel.GetSize (); i++) {
		if (CEventItem * p = (CEventItem *)m_lv.GetCtrlData (sel [i])) {
			::ResetEvent (p->m_hPC);
			::ResetEvent (p->m_hEOP);
		}
	}

	OnRefresh ();
}
