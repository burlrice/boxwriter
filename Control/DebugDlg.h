#if !defined(AFX_DEBUGDLG_H__82D7D501_253B_11D7_9EF4_0060674BA976__INCLUDED_)
#define AFX_DEBUGDLG_H__82D7D501_253B_11D7_9EF4_0060674BA976__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DebugDlg.h : header file
//
#include "Master.h"
#include "Utils.h"
#include "Head.h"

/////////////////////////////////////////////////////////////////////////////
// CDebugDlg dialog

class CDebugDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	void SetTitle ( CString sTitle );
	CString m_sTitle;
	void UpdateFlags (_DBG_FLAGS *pFlags, Head::CHead * pHead);
	CDebugDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDebugDlg)
	enum { IDD = IDD_DEBUG_DLG };
	CString	m_sISR;
	CString	m_sOTB;
	CString	m_sPhoto;
	CString	m_sSerial;
	CString	m_sTach;
	CString	m_sReserved1;
	CString	m_sReserved2;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDebugDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void UpdateUI ();
	
	CTime m_tmHighVoltage;
	CTime m_tmLowTemp;
	CTime m_tmLowInk;
	Head::CHead * m_pHead;

	// Generated message map functions
	//{{AFX_MSG(CDebugDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnClipboard();
	afx_msg void OnHighvoltage();
	afx_msg void OnLowink();
	afx_msg void OnLowtemp();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEBUGDLG_H__82D7D501_253B_11D7_9EF4_0060674BA976__INCLUDED_)
