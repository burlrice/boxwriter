#if !defined(__LOCALDBTHREAD_H__)
#define __LOCALDBTHREAD_H__

#include <lmcons.h>
#include <Lmapibuf.h>
#include <Lmuse.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define TM_LOCAL_MANUAL			(WM_APP + 1)
#define TM_LOCAL_CANCEL			(WM_APP + 2)
#define TM_LOCAL_APP_DB_OPEN	(WM_APP + 3)
#define TM_LOCAL_APP_DB_CLOSE	(WM_APP + 4)


CString GetRootPath (const CString & strFile);
CString GetDBQ (const CString & strDSN);
bool ConnectMappedDrive (const CString & strFile);

class CLocalDbThread : public CWinThread
{
	DECLARE_DYNCREATE(CLocalDbThread)
public:

	CLocalDbThread ();
	virtual ~CLocalDbThread ();

	afx_msg void OnKillThread (WPARAM wParam, LPARAM lParam);
	afx_msg void OnManual (WPARAM wParam, LPARAM lParam);
	afx_msg void OnCancel (WPARAM wParam, LPARAM lParam);
	afx_msg void OnAppDbOpen (WPARAM wParam, LPARAM lParam);
	afx_msg void OnAppDbClose (WPARAM wParam, LPARAM lParam);

protected:
	virtual int Run ();
	virtual bool Compare (int nCount);
	virtual void CopyNext ();
	virtual BOOL InitInstance ();

	static bool NeedsUpdate (const CString & strNetwork, const CString & strLocal);

	bool m_bRun;
	CMapStringToString m_mapCopy;
	CMapStringToString m_mapDeferredCopy;
	POSITION m_pos;
	CStringArray m_vDSN;
	HANDLE m_hManualTrigger;
	HANDLE m_hManualCancel;
	bool m_bInProgress;
	int m_nRetryLimit;

	DECLARE_MESSAGE_MAP()
};

class CLocalLogoThread : public CLocalDbThread
{
	DECLARE_DYNCREATE(CLocalLogoThread)
public:
	CLocalLogoThread ();
	virtual ~CLocalLogoThread ();

protected:
	virtual BOOL InitInstance ();
	virtual bool Compare (int nCount);
	virtual void CopyNext ();
};

#endif //__LOCALDBTHREAD_H__