#include "stdafx.h"

#include <vector>

#include "IpElements.h"
#include "NextThread.h"
#include "Debug.h"
#include "Parse.h"
#include "Head.h"
#include "ControlView.h"
#include "ControlDoc.h"
#include "Parse.h"
#include "FieldDefs.h"
#include "Registry.h"
#include "fj_system.h"
#include "Database.h"
#include "FieldDefs.h"
#include "TemplExt.h"
#include "Utils.h"
#include "BitmapElement.h"
#include "FileExt.h"
#include "fj_label.h"
#include "fj_bitmap.h"
#include "Version.h"
#include "Extern.h"
#include "fj_dyntext.h"
#include "MainFrm.h"
#include "HeadDlg.h"

using namespace NEXT;

#define MAKEMAPENTRY(s) { _T (#s),  &CNextThread::Get##s, &CNextThread::Set##s, }
#define MARKSETTINGDIRTY(s) \
	if (Find (m_vSQL, (s)) == -1) \
		m_vSQL.Add (CSQL ((s), _T (__FILE__), __LINE__));

static const LPCTSTR lpszNEXTTimeFormat = _T ("%Y/%m/%d %H:%M:%S");

#define DIAGNOSTIC(s) Diagnostic ((s), _T (__FILE__), __LINE__)

int CNextThread::sizeofBITMAPFILEHEADER (BITMAPFILEHEADER * p)
{
	// note: sizeof (BITMAPFILEHEADER) returns 16, when it should be 14 bytes
	
	if (p)
		return (sizeof (p->bfType) 
			+ sizeof (p->bfSize) 
			+ sizeof (p->bfReserved1) 
			+ sizeof (p->bfReserved2) 
			+ sizeof (p->bfOffBits));
	
	return 0;
}

int CNextThread::sizeofBITMAPINFOHEADER (BITMAPINFOHEADER * p)
{
	if (p)
		return (sizeof (p->biSize) 
			+ sizeof (p->biWidth) 
			+ sizeof (p->biHeight) 
			+ sizeof (p->biPlanes) 
			+ sizeof (p->biBitCount) 
			+ sizeof (p->biCompression) 
			+ sizeof (p->biSizeImage) 
			+ sizeof (p->biXPelsPerMeter)
			+ sizeof (p->biYPelsPerMeter) 
			+ sizeof (p->biClrUsed) 
			+ sizeof (p->biClrImportant));
	
	return 0;
}

struct 
{
	DWORD	m_dw;
	LPCTSTR m_lpsz;
} static const map [] =
{
	{ IPC_COMPLETE,						_T ("IPC_COMPLETE"),					},
	{ IPC_CONNECTION_CLOSE,				_T ("IPC_CONNECTION_CLOSE"),			},
	{ IPC_INVALID,						_T ("IPC_INVALID"),						},
	{ IPC_ECHO,							_T ("IPC_ECHO"),						},
	{ IPC_STATUS,						_T ("IPC_STATUS"),						},
	{ IPC_GET_STATUS,					_T ("IPC_GET_STATUS"),					},
	{ IPC_PHOTO_TRIGGER,				_T ("IPC_PHOTO_TRIGGER"),				},
	{ IPC_EDIT_STATUS,					_T ("IPC_EDIT_STATUS"),					},
	{ IPC_GET_EDIT_STATUS,				_T ("IPC_GET_EDIT_STATUS"),				},
	{ IPC_EDIT_START,					_T ("IPC_EDIT_START"),					},
	{ IPC_EDIT_SAVE,					_T ("IPC_EDIT_SAVE"),					},
	{ IPC_EDIT_CANCEL,					_T ("IPC_EDIT_CANCEL"),					},
	{ IPC_IMAGE_HEADER,					_T ("IPC_IMAGE_HEADER"),				},
	{ IPC_IMAGE_DATA,					_T ("IPC_IMAGE_DATA"),					},
	{ IPC_GET_LABEL_IDS,				_T ("IPC_GET_LABEL_IDS"),				},			
	{ IPC_SET_LABEL_IDS,				_T ("IPC_SET_LABEL_IDS"),				},			
	{ IPC_GET_LABELS,					_T ("IPC_GET_LABELS"),					},			
	{ IPC_GET_LABEL,					_T ("IPC_GET_LABEL"),					},			
	{ IPC_PUT_LABEL,					_T ("IPC_PUT_LABEL"),					},			
	{ IPC_DELETE_LABEL,					_T ("IPC_DELETE_LABEL"),				},			
	{ IPC_SELECT_LABEL,					_T ("IPC_SELECT_LABEL"),				},			
	{ IPC_GET_LABEL_ID_SELECTED,		_T ("IPC_GET_LABEL_ID_SELECTED"),		},			
	{ IPC_SET_LABEL_ID_SELECTED,		_T ("IPC_SET_LABEL_ID_SELECTED"),		},			
	{ IPC_DELETE_ALL_LABELS,			_T ("IPC_DELETE_ALL_LABELS"),			},			
	{ IPC_GET_MSG_IDS,					_T ("IPC_GET_MSG_IDS"),					},			
	{ IPC_SET_MSG_IDS,					_T ("IPC_SET_MSG_IDS"),					},			
	{ IPC_GET_MSGS,						_T ("IPC_GET_MSGS"),					},			
	{ IPC_GET_MSG,						_T ("IPC_GET_MSG"),						},			
	{ IPC_PUT_MSG,						_T ("IPC_PUT_MSG"),						},			
	{ IPC_DELETE_MSG,					_T ("IPC_DELETE_MSG"),					},			
	{ IPC_GET_TEXT_IDS,					_T ("IPC_GET_TEXT_IDS"),				},			
	{ IPC_SET_TEXT_IDS,					_T ("IPC_SET_TEXT_IDS"),				},			
	{ IPC_GET_TEXT_ELEMENTS,			_T ("IPC_GET_TEXT_ELEMENTS"),			},			
	{ IPC_GET_TEXT,						_T ("IPC_GET_TEXT"),					},			
	{ IPC_PUT_TEXT,						_T ("IPC_PUT_TEXT"),					},			
	{ IPC_DELETE_TEXT,					_T ("IPC_DELETE_TEXT"),					},			
	{ IPC_GET_BITMAP_IDS,				_T ("IPC_GET_BITMAP_IDS"),				},			
	{ IPC_SET_BITMAP_IDS,				_T ("IPC_SET_BITMAP_IDS"),				},			
	{ IPC_GET_BITMAP_ELEMENTS,			_T ("IPC_GET_BITMAP_ELEMENTS"),			},			
	{ IPC_GET_BITMAP,					_T ("IPC_GET_BITMAP"),					},			
	{ IPC_PUT_BITMAP,					_T ("IPC_PUT_BITMAP"),					},			
	{ IPC_DELETE_BITMAP,				_T ("IPC_DELETE_BITMAP"),				},			
	{ IPC_GET_DYNIMAGE_IDS,				_T ("IPC_GET_DYNIMAGE_IDS"),			},			
	{ IPC_SET_DYNIMAGE_IDS,				_T ("IPC_SET_DYNIMAGE_IDS"),			},			
	{ IPC_GET_DYNIMAGE_ELEMENTS,		_T ("IPC_GET_DYNIMAGE_ELEMENTS"),		},			
	{ IPC_GET_DYNIMAGE,					_T ("IPC_GET_DYNIMAGE"),				},			
	{ IPC_PUT_DYNIMAGE,					_T ("IPC_PUT_DYNIMAGE"),				},			
	{ IPC_DELETE_DYNIMAGE,				_T ("IPC_DELETE_DYNIMAGE"),				},			
	{ IPC_PUT_DYNIMAGE_DATA,			_T ("IPC_PUT_DYNIMAGE_DATA"),			},			
	{ IPC_GET_DATETIME_IDS,				_T ("IPC_GET_DATETIME_IDS"),			},			
	{ IPC_SET_DATETIME_IDS,				_T ("IPC_SET_DATETIME_IDS"),			},			
	{ IPC_GET_DATETIME_ELEMENTS,		_T ("IPC_GET_DATETIME_ELEMENTS"),		},			
	{ IPC_GET_DATETIME,					_T ("IPC_GET_DATETIME"),				},			
	{ IPC_PUT_DATETIME,					_T ("IPC_PUT_DATETIME"),				},			 
	{ IPC_DELETE_DATETIME,				_T ("IPC_DELETE_DATETIME"),				},			
	{ IPC_GET_COUNTER_IDS,				_T ("IPC_GET_COUNTER_IDS"),				},			
	{ IPC_SET_COUNTER_IDS,				_T ("IPC_SET_COUNTER_IDS"),				},			
	{ IPC_GET_COUNTER_ELEMENTS,			_T ("IPC_GET_COUNTER_ELEMENTS"),		},			
	{ IPC_GET_COUNTER,					_T ("IPC_GET_COUNTER"),					},			
	{ IPC_PUT_COUNTER,					_T ("IPC_PUT_COUNTER"),					},			
	{ IPC_DELETE_COUNTER,				_T ("IPC_DELETE_COUNTER"),				},			
	{ IPC_GET_BARCODE_IDS,				_T ("IPC_GET_BARCODE_IDS"),				},			
	{ IPC_SET_BARCODE_IDS,				_T ("IPC_SET_BARCODE_IDS"),				},			
	{ IPC_GET_BARCODE_ELEMENTS,			_T ("IPC_GET_BARCODE_ELEMENTS"),		},			
	{ IPC_GET_BARCODE,					_T ("IPC_GET_BARCODE"),					},			
	{ IPC_PUT_BARCODE,					_T ("IPC_PUT_BARCODE"),					},			
	{ IPC_DELETE_BARCODE,				_T ("IPC_DELETE_BARCODE"),				},			
	{ IPC_GET_FONT_IDS,					_T ("IPC_GET_FONT_IDS"),				},			
	{ IPC_SET_FONT_IDS,					_T ("IPC_SET_FONT_IDS"),				},			
	{ IPC_GET_FONTS,					_T ("IPC_GET_FONTS"),					},			
	{ IPC_GET_FONT,						_T ("IPC_GET_FONT"),					},			
	{ IPC_PUT_FONT,						_T ("IPC_PUT_FONT"),					},			
	{ IPC_DELETE_FONT,					_T ("IPC_DELETE_FONT"),					},			
	{ IPC_GET_BMP_IDS,					_T ("IPC_GET_BMP_IDS"),					},			
	{ IPC_SET_BMP_IDS,					_T ("IPC_SET_BMP_IDS"),					},			
	{ IPC_GET_BMPS,						_T ("IPC_GET_BMPS"),					},			
	{ IPC_GET_BMP,						_T ("IPC_GET_BMP"),						},			
	{ IPC_PUT_BMP,						_T ("IPC_PUT_BMP"),						},			
	{ IPC_DELETE_BMP,					_T ("IPC_DELETE_BMP"),					},			
	{ IPC_DELETE_ALL_BMPS,				_T ("IPC_DELETE_ALL_BMPS"),				},			
	{ IPC_GET_DYNTEXT_IDS,				_T ("IPC_GET_DYNTEXT_IDS"),				},			
	{ IPC_SET_DYNTEXT_IDS,				_T ("IPC_SET_DYNTEXT_IDS"),				},			
	{ IPC_GET_DYNTEXT_ELEMENTS,			_T ("IPC_GET_DYNTEXT_ELEMENTS"),		},			
	{ IPC_GET_DYNTEXT,					_T ("IPC_GET_DYNTEXT"),					},			
	{ IPC_PUT_DYNTEXT,					_T ("IPC_PUT_DYNTEXT"),					},			
	{ IPC_DELETE_DYNTEXT,				_T ("IPC_DELETE_DYNTEXT"),				},			
	{ IPC_GET_DYNBARCODE_IDS,			_T ("IPC_GET_DYNBARCODE_IDS"),			},			
	{ IPC_SET_DYNBARCODE_IDS,			_T ("IPC_SET_DYNBARCODE_IDS"),			},			
	{ IPC_GET_DYNBARCODE_ELEMENTS,		_T ("IPC_GET_DYNBARCODE_ELEMENTS"),		},			
	{ IPC_GET_DYNBARCODE,				_T ("IPC_GET_DYNBARCODE"),				},			
	{ IPC_PUT_DYNBARCODE,				_T ("IPC_PUT_DYNBARCODE"),				},			
	{ IPC_DELETE_DYNBARCODE,			_T ("IPC_DELETE_DYNBARCODE"),			},			
	{ IPC_GET_GROUP_INFO,				_T ("IPC_GET_GROUP_INFO"),				},			
	{ IPC_SET_GROUP_INFO,				_T ("IPC_SET_GROUP_INFO"),				},			
	{ IPC_GET_SYSTEM_INFO,				_T ("IPC_GET_SYSTEM_INFO"),				},			
	{ IPC_GET_SYSTEM_INTERNET_INFO,		_T ("IPC_GET_SYSTEM_INTERNET_INFO"),	},			
	{ IPC_GET_SYSTEM_TIME_INFO,			_T ("IPC_GET_SYSTEM_TIME_INFO"),		},			
	{ IPC_GET_SYSTEM_DATE_STRINGS,		_T ("IPC_GET_SYSTEM_DATE_STRINGS"),		},			
	{ IPC_GET_SYSTEM_PASSWORDS,			_T ("IPC_GET_SYSTEM_PASSWORDS"),		},			
	{ IPC_GET_SYSTEM_BARCODES,			_T ("IPC_GET_SYSTEM_BARCODES"),			},			
	{ IPC_SET_SYSTEM_INFO,				_T ("IPC_SET_SYSTEM_INFO"),				},			
	{ IPC_GET_PRINTER_ID,				_T ("IPC_GET_PRINTER_ID"),				},			
	{ IPC_GET_PRINTER_INFO,				_T ("IPC_GET_PRINTER_INFO"),			},			
	{ IPC_GET_PRINTER_PKG_INFO,			_T ("IPC_GET_PRINTER_PKG_INFO"),		},			
	{ IPC_GET_PRINTER_PHY_INFO,			_T ("IPC_GET_PRINTER_PHY_INFO"),		},			
	{ IPC_GET_PRINTER_OP_INFO,			_T ("IPC_GET_PRINTER_OP_INFO"),			},			
	{ IPC_SET_PRINTER_INFO,				_T ("IPC_SET_PRINTER_INFO"),			},			
	{ IPC_GET_PRINTER_ELEMENTS,			_T ("IPC_GET_PRINTER_ELEMENTS"),		},			
	{ IPC_PUT_PRINTER_ELEMENT,			_T ("IPC_PUT_PRINTER_ELEMENT"),			},			
	{ IPC_DELETE_ALL_PRINTER_ELEMENTS,	_T ("IPC_DELETE_ALL_PRINTER_ELEMENTS"),	},			
	{ IPC_FIRMWARE_UPGRADE,				_T ("IPC_FIRMWARE_UPGRADE"),			},			
	{ IPC_SET_PH_TYPE,					_T ("IPC_SET_PH_TYPE"),					},			
	{ IPC_SET_PRINTER_MODE,				_T ("IPC_SET_PRINTER_MODE"),			},			
	{ IPC_GET_PRINTER_MODE,				_T ("IPC_GET_PRINTER_MODE"),			},			
	{ IPC_SET_SELECTED_COUNT,			_T ("IPC_SET_SELECTED_COUNT"),			},			
	{ IPC_GET_SELECTED_COUNT,			_T ("IPC_GET_SELECTED_COUNT"),			},			
	{ IPC_SELECT_VARDATA_ID,			_T ("IPC_SELECT_VARDATA_ID"),			},			
	{ IPC_GET_VARDATA_ID,				_T ("IPC_GET_VARDATA_ID"),				},			
	{ IPC_SET_VARDATA_ID,				_T ("IPC_SET_VARDATA_ID"),				},			
	{ IPC_SAVE_ALL_PARAMS,				_T ("IPC_SAVE_ALL_PARAMS"),				},			
	{ IPC_GET_SW_VER_INFO,				_T ("IPC_GET_SW_VER_INFO"),				},			
	{ IPC_GET_GA_VER_INFO,				_T ("IPC_GET_GA_VER_INFO"),				},
	{ IPC_GA_UPGRADE,					_T ("IPC_GA_UPGRADE"),					},
	{ IPC_SET_STATUS_MODE,				_T ("IPC_SET_STATUS_MODE"),				},
	{ IPC_GET_STATUS_MODE,				_T ("IPC_GET_STATUS_MODE"),				},
	{ IPC_GET_DISK_USAGE,				_T ("IPC_GET_DISK_USAGE"),				},			
	{ IPC_GET_PH_TYPE,					_T ("IPC_GET_PH_TYPE"),					},			
	{ IPC_PUT_BMP_DRIVER,				_T ("IPC_PUT_BMP_DRIVER"),				},
};

CString CNextThread::GetIpcCmdName (DWORD dw)
{
	for (int i = 0; i < ARRAYSIZE (::map); i++)
		if (::map [i].m_dw == dw)
			return ::map [i].m_lpsz;

	return "[not found]";
}

DWORD CNextThread::GetIpcCmd (const CString & str)
{
	for (int i = 0; i < ARRAYSIZE (::map); i++)
		if (!str.CompareNoCase (::map [i].m_lpsz))
			return ::map [i].m_dw;

	return map [0].m_dw;
}

void CNextThread::ApplySw0923CommFix (bool bInit)
{
	bool bWrite = false;

	TRACEF (_T ("PORT_TYPE: ") + FoxjetDatabase::ToString ((int)FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, m_strRegSection, _T ("PORT_TYPE"), -1)));

	if (bInit) {
		bWrite = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, m_strRegSection, _T ("PORT_TYPE"), -1) == -1 ? true : false;
	}
	else
		bWrite = true;
	
	if (bWrite) {
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, m_strRegSection, _T ("PORT_TYPE"), 0);
		FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("SERIAL_PORT"), _T (""));
	}

	TRACEF (_T ("PORT_TYPE: ") + FoxjetDatabase::ToString ((int)FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, m_strRegSection, _T ("PORT_TYPE"), -1)));
}

void CNextThread::Diagnostic (const CString & strDebug, LPCTSTR lpszFile, ULONG lLine)
{
	CString str, strFile (lpszFile);

	int nIndex = strFile.ReverseFind ('\\');

	if (nIndex != -1)
		strFile.Delete (0, nIndex);

	str.Format (_T ("%s(%d): %s"), strFile, lLine, strDebug);

	while (!::PostMessage (m_pView->GetSafeHwnd(), WM_DIAGNOSTIC_DATA, (WPARAM)new CString (str), 0)) 
		::Sleep (0); 
}

//////////////////////////////////////////////////////////////////////////////////////////
bool CNextThread::CEditLock::Lock (const CString & str, CControlView * pView)
{
	if (!IsLocked ()) {
		m_strAddress	= str;
		m_tm			= CTime::GetCurrentTime ();
		m_pView			= pView;

		if (m_pView)
			m_pView->SendMessage (WM_NEXT_EDIT_SESSION, 1);

		return true;
	}

	return false;
}

bool CNextThread::CEditLock::Free ()
{
	if (IsLocked ()) {
		if (m_pView)
			m_pView->PostMessage (WM_NEXT_EDIT_SESSION, 0);

		m_strAddress.Empty ();
		m_pView = NULL;
		return true;
	}

	return false;
}
//////////////////////////////////////////////////////////////////////////////////////////

int DeleteAllBitmaps ()
{
	CStringArray v;
	int n = 0;

	FoxjetDatabase::GetFiles (FoxjetIpElements::CBitmapElement::GetDefPath (), _T ("*.bmp"), v);

	for (int i = 0; i < v.GetSize (); i++)
		if (::DeleteFile (v [i]))
			n++;

	return n;
}

int DeleteAllFonts ()
{
	CStringArray v;
	int n = 0;

	FoxjetDatabase::GetFiles (FoxjetIpElements::CTextElement::GetFontDir (), _T ("*.fjf"), v);

	for (int i = 0; i < v.GetSize (); i++)
		if (::DeleteFile (v [i]))
			n++;

	return n;
}

typedef CString (CNextThread::*lpfctGet) ();
typedef void (CNextThread::*lpfctSet) (const CString & str);

static CString StrobeStateToString (FoxjetDatabase::STROBESTATE s);
static FoxjetDatabase::STROBESTATE StrobeStateFromString (const CString & str);

typedef struct
{
	LPCTSTR m_lpszToken;
	lpfctGet m_lpfctGet;
	lpfctSet m_lpfctSet;
} GETSET_STRUCT;

static CString ToString (CNextThread * pThread, const GETSET_STRUCT * p)
{
	CString str;

	for (int i = 0; p [i].m_lpszToken; i++) 
		if (lpfctGet pfct = p [i].m_lpfctGet) 
			str += p [i].m_lpszToken + CString (_T ("=")) + (((* pThread).*(pfct)) ()) + CString (_T (";"));

	return str;
}

/*
	un-handled commands:
		UNITS
		INK_SIZE
		INK_COST
		SLANTVALUE
		INFO_STATUS
		INFO_SOCKET
		STANDBY_AUTO
*/

static const GETSET_STRUCT mapParams [] = 
{
	MAKEMAPENTRY (DIRECTION),           
	MAKEMAPENTRY (SLANTANGLE),          
	MAKEMAPENTRY (SLANTVALUE),          
	MAKEMAPENTRY (HEIGHT),              
	MAKEMAPENTRY (DISTANCE),            
	//MAKEMAPENTRY (AMS_INTERVAL),        
	//MAKEMAPENTRY (AMS_INACTIVE),        
	//MAKEMAPENTRY (STANDBY_INTERVAL),    
	MAKEMAPENTRY (STANDBY_AUTO),        
	MAKEMAPENTRY (PHOTOCELL_BACK),      
	//MAKEMAPENTRY (MSGSTORE_SIZE),       
	//MAKEMAPENTRY (TABLE_INDEX),         
	//MAKEMAPENTRY (DATE_MANU),           
	//MAKEMAPENTRY (DATE_SERVICE),        
	MAKEMAPENTRY (SCANNER_USE_ONCE),    
	MAKEMAPENTRY (SERIAL_PORT),         
	MAKEMAPENTRY (INFO_STATUS),         
	MAKEMAPENTRY (INFO_SOCKET),         
	//MAKEMAPENTRY (INFO_RAM),            
	//MAKEMAPENTRY (INFO_ROM),            
	//MAKEMAPENTRY (INFO_NAME),           
	//MAKEMAPENTRY (INFO_TIME),           
	//MAKEMAPENTRY (INFO_AD),             
	MAKEMAPENTRY (PHOTOCELL_INTERNAL),  
	MAKEMAPENTRY (APS_MODE),            
	//MAKEMAPENTRY (SOP),					
	MAKEMAPENTRY (BAUD_RATE),			  
	//MAKEMAPENTRY (DISTANCE2),           
	//MAKEMAPENTRY (BIDIRECTIONAL),       
	//MAKEMAPENTRY (DOUBLE_PULSE),        
	MAKEMAPENTRY (OP_AUTOPRINT),       
	MAKEMAPENTRY (IDLE_AFTER_PRINT),
	MAKEMAPENTRY (SAVE_DYN_DATA),
	MAKEMAPENTRY (SAVE_SERIAL_DATA),
	{ NULL, NULL, NULL },
};

static const GETSET_STRUCT mapSystem [] = 
{
	MAKEMAPENTRY (UNITS),
	MAKEMAPENTRY (TIME),
	MAKEMAPENTRY (ENCODER),
	MAKEMAPENTRY (ENCODER_COUNT),
	MAKEMAPENTRY (ENCODER_WHEEL),
	MAKEMAPENTRY (ENCODER_DIVISOR),
	MAKEMAPENTRY (CONVEYOR_SPEED),
	MAKEMAPENTRY (PRINT_RESOLUTION),
	MAKEMAPENTRY (INK_SIZE),
	MAKEMAPENTRY (INK_COST),
	MAKEMAPENTRY (SYSTEM_TIME),
	MAKEMAPENTRY (HeadType),
	{ NULL, NULL, NULL },
};

static const GETSET_STRUCT mapTime [] = 
{
	MAKEMAPENTRY (INTERNAL_CLOCK),     
	MAKEMAPENTRY (TIME_ZONE),          
	MAKEMAPENTRY (OBSERVE_USA_DST),    
	MAKEMAPENTRY (FIRST_DAY),          
	MAKEMAPENTRY (DAYS_WEEK1),         
	MAKEMAPENTRY (WEEK53),             
	MAKEMAPENTRY (ROLLOVER_HOURS),     
	MAKEMAPENTRY (SHIFT1),             
	MAKEMAPENTRY (SHIFT2),             
	MAKEMAPENTRY (SHIFT3),             
	MAKEMAPENTRY (SHIFTCODE1),         
	MAKEMAPENTRY (SHIFTCODE2),         
	MAKEMAPENTRY (SHIFTCODE3),
	{ NULL, NULL, NULL },
};

static const GETSET_STRUCT mapDate [] = 
{
	MAKEMAPENTRY (DAYS),               
	MAKEMAPENTRY (MONTHS),             
	MAKEMAPENTRY (DAYS_SPECIAL),       
	MAKEMAPENTRY (MONTHS_SPECIAL),     
	MAKEMAPENTRY (HOURS),              
	MAKEMAPENTRY (AMPM), 
	{ NULL, NULL, NULL },
};

static const GETSET_STRUCT mapBarcode [] = 
{
	MAKEMAPENTRY (BC_N),
	MAKEMAPENTRY (BC_NAME),
	MAKEMAPENTRY (BC_TY),
	MAKEMAPENTRY (BC_H),
	MAKEMAPENTRY (BC_CD),
	MAKEMAPENTRY (BC_HB),
	MAKEMAPENTRY (BC_VB),
	MAKEMAPENTRY (BC_QZ),
	MAKEMAPENTRY (BC_BASE),
	MAKEMAPENTRY (BC_B1),
	MAKEMAPENTRY (BC_B2),
	MAKEMAPENTRY (BC_B3),
	MAKEMAPENTRY (BC_B4),
	{ NULL, NULL, NULL },
};

static const GETSET_STRUCT mapDataMatrix [] = 
{
	MAKEMAPENTRY (DM_N),
	MAKEMAPENTRY (DM_NAME),
	MAKEMAPENTRY (DM_H),
	MAKEMAPENTRY (DM_HB),
	MAKEMAPENTRY (DM_W),
	MAKEMAPENTRY (DM_WB),
	{ NULL, NULL, NULL },
};

static const GETSET_STRUCT mapStatus [] = 
{
	MAKEMAPENTRY (STATUS_MODE),
	{ NULL, NULL, NULL },
};

static const GETSET_STRUCT mapPrintMode [] = 
{
	MAKEMAPENTRY (PRINT_MODE),             
	MAKEMAPENTRY (APS_MODE),          	  
	MAKEMAPENTRY (STROBE_MODE_READY_G),		
	MAKEMAPENTRY (STROBE_MODE_READY_Y),		
	MAKEMAPENTRY (STROBE_MODE_READY_R),		
	MAKEMAPENTRY (STROBE_MODE_APS_G),		
	MAKEMAPENTRY (STROBE_MODE_APS_Y),		
	MAKEMAPENTRY (STROBE_MODE_APS_R),		
	MAKEMAPENTRY (STROBE_MODE_LOWTEMP_G),	
	MAKEMAPENTRY (STROBE_MODE_LOWTEMP_Y),	
	MAKEMAPENTRY (STROBE_MODE_LOWTEMP_R),	
	MAKEMAPENTRY (STROBE_MODE_HV_G),			
	MAKEMAPENTRY (STROBE_MODE_HV_Y),			
	MAKEMAPENTRY (STROBE_MODE_HV_R),			
	MAKEMAPENTRY (STROBE_MODE_LOWINK_G),		
	MAKEMAPENTRY (STROBE_MODE_LOWINK_Y),		
	MAKEMAPENTRY (STROBE_MODE_LOWINK_R),		
	MAKEMAPENTRY (STROBE_MODE_OUTOFINK_G),
	MAKEMAPENTRY (STROBE_MODE_OUTOFINK_Y),
	MAKEMAPENTRY (STROBE_MODE_OUTOFINK_R),
	MAKEMAPENTRY (STROBE_MODE_DRIVER_G),		
	MAKEMAPENTRY (STROBE_MODE_DRIVER_Y),		
	MAKEMAPENTRY (STROBE_MODE_DRIVER_R),		
	MAKEMAPENTRY (PRINT_DRIVER),		
	MAKEMAPENTRY (PRINT_DRIVER_COUNT),		
	{ NULL, NULL, NULL },
};


static int fj_SysBCIndex = 0;
static int fj_SysDMIndex = 0;
static FJSYSBARCODE	bcArray[FJSYS_BARCODES];
static FJSYSDATAMATRIX bcDataMatrixArray[FJSYS_BARCODES];
static const LPCTSTR lpszBcArray			= _T ("bcArray");
static const LPCTSTR lpszDataMatrixArray	= _T ("bcDataMatrixArray");
static const LPCTSTR lpszStrobeSettings		= _T ("strobe settings");
static const LPCTSTR lpszTimeSettings		= _T ("TimeSettings");
static const LPCTSTR lpszDateSettings		= _T ("DateSettings");
static const LPCTSTR lpszSerialPort			= _T ("SerialPort");
static const LPCTSTR lpszFont				= _T ("Font");
static const LPCTSTR lpszAllSettings [] = 
{
	::lpszBcArray,
	::lpszDataMatrixArray,
	::lpszStrobeSettings,
	::lpszTimeSettings,
	::lpszDateSettings,
	::lpszSerialPort,
};


struct
{
	NEXT::PORT_TYPE	m_n;
	LPCTSTR			m_lpsz;
} 
static const mapPort [] =
{
	{ NEXT::NOTUSED,	 _T ("NONE"),			},
	{ VARDATA,			 _T ("VARDATA"),		},
	{ DYNMEM,			 _T ("DYNMEM"),			},
	{ SCANNER_LABEL,	 _T ("SCANNER_LABEL"),	},
	{ INFO,				 _T ("INFO"),			},
};

static lpfctGet FindGet (const CString & str, const GETSET_STRUCT map [])
{
	for (int i = 0; map [i].m_lpszToken; i++) 
		if (!str.CompareNoCase (map [i].m_lpszToken))
			return map [i].m_lpfctGet;

	return NULL;
}

static lpfctSet FindSet (const CString & str, const GETSET_STRUCT map [])
{
	for (int i = 0; map [i].m_lpszToken; i++) 
		if (!str.CompareNoCase (map [i].m_lpszToken))
			return map [i].m_lpfctSet;

	return NULL;
}


const CString CNextThread::m_strRegSection = ListGlobals::defElements.m_strRegSection + _T ("\\CNextThread");

#define SEND(sock,pmsg,length) Send ((sock), (pmsg), (length), _T (__FILE__), __LINE__)

IMPLEMENT_DYNCREATE(CNextThread, CWinThread) // CServerThread)

CNextThread::CNextThread()
:	m_hExit (NULL),
	m_pView (NULL),
	m_bSendStatusNow (false),
	CWinThread () //CServerThread ()
{
}


CNextThread::~CNextThread()
{
	Close (m_sockTCP.m_sock);
	Close (m_sockUDP.m_sock);

	for (int i = 0; i < ARRAYSIZE (m_sockClients); i++) {
		if (m_sockClients [i].m_sock != INVALID_SOCKET)
			Close (m_sockClients [i].m_sock);
	}
}

BEGIN_MESSAGE_MAP(CNextThread, CWinThread)
	//{{AFX_MSG_MAP(CUdpThread)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_THREAD_MESSAGE (TM_KILLTHREAD, OnKillThread)
	ON_THREAD_MESSAGE (TM_SYSTEMPARAMCHANGE, OnSystemParamsChanged)
	ON_THREAD_MESSAGE (TM_NEXT_TASK_START, OnTaskStart)
	ON_THREAD_MESSAGE (TM_NEXT_SEND_STATUS, OnSendStatus)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CUdpThread message handlers

BOOL CNextThread::InitInstance()
{
	CString str;
	WSADATA wsaData;

	VERIFY (::WSAStartup (MAKEWORD (1, 1), &wsaData) == 0);

	str.Format (_T ("Exit %lu"), m_hThread);
	m_hExit = CreateEvent (NULL, true, false, str);

	LoadSettings ();

	{
		typedef void (CNextThread::*lpfct) (char * psz);

		struct
		{
			lpfct m_lpfct;
			LPCTSTR m_lpszFile;
		}
		static const map [] = 
		{
			{ &CNextThread::fj_SystemFromString,	_T ("IPC_SET_PH_TYPE.next"),				},
			{ &CNextThread::fj_SystemFromString,	_T ("IPC_SET_PRINTER_INFO.next"),			},
			{ &CNextThread::fj_PrintHeadFromString,	_T ("IPC_GET_SYSTEM_BARCODES.next"),		},
			{ &CNextThread::fj_PrintHeadFromString,	_T ("IPC_GET_SYSTEM_DATAMATRIX.next"),		},
			{ &CNextThread::fj_PrintHeadFromString,	_T ("IPC_GET_SYSTEM_DATE_STRINGS.next"),	},
			{ &CNextThread::fj_PrintHeadFromString,	_T ("IPC_GET_SYSTEM_TIME_INFO.next"),		},
		};
		const CString strDir = FoxjetDatabase::GetHomeDir () + _T ("\\Cache\\");

		for (int i = 0; i < ARRAYSIZE (map); i++) {
			CString str = FoxjetFile::ReadDirect (strDir + map [i].m_lpszFile);

			if (str.GetLength ())
				((* this).*(map [i].m_lpfct)) (w2a (str));
		}

		#ifndef _DEBUG
		{
			CStringArray v;
		
			FoxjetDatabase::GetFiles (FoxjetDatabase::GetHomeDir () + _T ("\\Cache"), _T ("*.next"), v);
			
			for (int i = 0; i < v.GetSize (); i++) {
				CString strFile = v [i];

				VERIFY (::DeleteFile (strFile));
			}
		}
		#endif
	}

	return Open ();
}

void CNextThread::LoadSettings ()
{
	using namespace FoxjetDatabase;
	using namespace FoxjetCommon;

	m_pView->SendMessage (WM_NEXT_EDIT_SESSION, 1);

	CProdLine * pLine = GetLine ();

	if (!pLine)
		return;

	ULONG lLineID = pLine ? pLine->m_LineRec.m_lID : 0;
	HEADTYPE type = GetHead ().m_Config.m_HeadSettings.m_nHeadType;
	BCPARAMS params (lLineID, type);

	{ // reconstruct fj_[x] structs from last commands saved in the registry
		const CString strNotSet = _T ("[$_not set]");
		const GETSET_STRUCT * pmap [] = 
		{
			::mapSystem,
			::mapParams,
			::mapBarcode,
			::mapDataMatrix,
			::mapStatus,
			::mapPrintMode,
			::mapDate,
			::mapTime,
		};
		CStringArray vExclude;

		vExclude.Add (_T ("INK_SIZE"));
		vExclude.Add (_T ("INK_COST"));
		vExclude.Add (_T ("INFO_STATUS"));
		vExclude.Add (_T ("STROBE_MODE_DRIVER_G"));
		vExclude.Add (_T ("STROBE_MODE_DRIVER_Y"));
		vExclude.Add (_T ("STROBE_MODE_DRIVER_R"));
		vExclude.Add (_T ("PRINT_DRIVER"));
		vExclude.Add (_T ("PRINT_DRIVER_COUNT"));

		for (int i = 0; i < vExclude.GetSize (); i++) {
			CString str = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, vExclude [i], _T (""));

			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, m_strRegSection, vExclude [i], str);
		}

		for (int i = 0; i < ARRAYSIZE (pmap); i++) {
			std::vector <std::wstring> v;
			int j = 0;

			for (const GETSET_STRUCT * p = pmap [i]; p [j].m_lpfctGet && p [j].m_lpfctSet; j++) {
				CString strKey = p [j].m_lpszToken;
				CString strValue = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, strKey, strNotSet);
			
				if (strValue != strNotSet) 
					v.push_back (std::wstring (strKey + _T ("=") + strValue));
				else {
					TRACEF (strKey);
					//ASSERT (0);
				}
			}
			
			if (v.size () == (j - 1)) {
				CString str = implode (v, std::wstring (_T (";"))).c_str ();

				TRACEF (str);
				fj_SystemFromString (w2a (str));
			}
		}
	}

	m_pView->SendMessage (WM_NEXT_GET_BCPARAMS, (WPARAM)&params, (LPARAM)::bcArray);
	m_pView->SendMessage (WM_NEXT_GET_DMPARAMS, (WPARAM)&params, (LPARAM)::bcDataMatrixArray);
		
	m_vStrobe.RemoveAll ();
	m_pView->SendMessage (WM_NEXT_GET_STROBE,	(WPARAM)&m_vStrobe, 0);

	{
		CString str, strData;
		CStringArray v, v2;
		//ULONG lLineID = GetLine ()->m_LineRec.m_lID;

		str = ListGlobals::Keys::NEXT::m_strTime;
		m_pView->SendMessage (WM_NEXT_GET_SYSTEM_PARAMS, (WPARAM)lLineID, (LPARAM)&str);
		strData += str;
		TRACEF (str);

		str = ListGlobals::Keys::NEXT::m_strDays;
		m_pView->SendMessage (WM_NEXT_GET_SYSTEM_PARAMS, (WPARAM)lLineID, (LPARAM)&str);
		strData += str;
		TRACEF (str);
		//TRACEF (strData);

		FoxjetDatabase::Tokenize (strData, v, ';');

		for (int i = 0; i < v.GetSize (); i++) {
			FoxjetDatabase::Tokenize (v [i], v2, '=');

			if (v2.GetSize () >= 2) {
				//TRACEF (v2 [0] + _T ("=") + v2 [1]);
				CString strValue = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, v2 [0], _T (""));

				if (strValue != v2 [1]) {
					TRACEF (v2 [0] + _T ("= ") + strValue + _T (" != ") + v2 [1]);
					int nBreak = 0;
				}

				FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, m_strRegSection, v2 [0], v2 [1]);
			}
		}
	}


	for (int i = 0; i < FJMESSAGE_DYNAMIC_SEGMENTS; i++) {
		CString strKey;

		strKey.Format (_T ("DYN_DATA_ID[%d]"), i + 1);
		FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, m_strRegSection, strKey, _T (""));
		VERIFY (fj_setWinDynData (i + 1, ""));
	}

	bool bSaveDynData		= !GetSAVE_DYN_DATA ().CompareNoCase (_T ("T"))		? true : false;
	bool bSaveSerialData	= !GetSAVE_SERIAL_DATA ().CompareNoCase (_T ("T"))	? true : false;

	if (!bSaveDynData && !bSaveSerialData) {
		CStringArray v;

		v.Add (_T ("Dynamic data"));
		v.Add (_T (""));
		VERIFY (m_pView->SendMessage (WM_NEXT_SET_SYSTEM_PARAMS, (WPARAM)lLineID, (LPARAM)&v) == 1);
	}
	else {
		using FoxjetIpElements::CTableItem;

		ULONG lSerialVarDataID = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, m_strRegSection, _T ("SerialVarDataID"), 0) + 1;
		CArray <CTableItem, CTableItem &> vItems;
		CStringArray v;
		CString str;
		ULONG lLineID = pLine ? pLine->GetID () : 0;

		str = _T ("Dynamic data");
		m_pView->SendMessage (WM_NEXT_GET_SYSTEM_PARAMS, (WPARAM)lLineID, (LPARAM)&str);
			
		FoxjetDatabase::FromString (str, v);

		for (int i = 0; i < v.GetSize (); i++) {
			FoxjetIpElements::CTableItem item;

			if (item.FromString (v [i])) {
				if (bSaveDynData || (bSaveSerialData && item.m_lID == lSerialVarDataID)) {	
					CString strKey;

					item.m_lID = i + 1; // renumber

					strKey.Format (_T ("DYN_DATA_ID[%d]"), item.m_lID);
					FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, m_strRegSection, strKey, item.m_strData);
					VERIFY (fj_setWinDynData (item.m_lID, w2a (item.m_strData)));

					if (item.m_strData.GetLength ()) {
						TRACEF (_T ("[") + FoxjetDatabase::ToString (item.m_lID) + _T ("] ") + item.m_strData);
					}
				}
			}
		}
	}

	for (int i = 0; i < ARRAYSIZE (::lpszAllSettings); i++) {
		MARKSETTINGDIRTY (::lpszAllSettings [i]);
	}

	SaveParams (false);

	m_pView->PostMessage (WM_NEXT_EDIT_SESSION, 0);
}

int CNextThread::Run() 
{
	MSG Msg;
	HANDLE hEvents [] = { m_hExit };

	while (1) {
		DWORD dwEventID = MsgWaitForMultipleObjects (ARRAYSIZE (hEvents), hEvents, false, 250, QS_ALLINPUT);

		switch ( dwEventID )
		{
			case WAIT_OBJECT_0:
				TRACEF (GetRuntimeClass ()->m_lpszClassName + CString (_T (": m_hExit")));
				return ExitInstance();
			default:
				while (PeekMessage (&Msg, NULL, NULL, NULL, PM_NOREMOVE))
					if ( !PumpMessage() )
						return ExitInstance();
		}

		ProcessData ();
	}

	return ExitInstance ();
}

void CNextThread::OnSystemParamsChanged (WPARAM wParam, LPARAM lParam)
{
	LoadSettings ();
}

void CNextThread::OnKillThread (WPARAM wParam, LPARAM lParam)
{
	TRACEF ("CNextThread::OnKillThread");

	::SetEvent (m_hExit);
	ExitInstance ();
	delete this;

	if (HANDLE h = (HANDLE)wParam)
		::SetEvent (h);

	_endthreadex (0);
}

void CNextThread::Close (SOCKET & sock)
{
	CString str;

	str.Format (_T ("CNextThread::Close (0x%p [%d]): %s"),
		sock, sock,
		GetLocalAddress (sock));
	TRACEF (str);

	CUdpThread::Close (sock);
}

CString CNextThread::GetTrace (_FJ_SOCKET_MESSAGE * pmsg, ULONG lLength, LPCTSTR lpszFile, ULONG lLine)
{
	CString str;
	char sz [128] = { 0 };

	memcpy (sz, pmsg->Data, min (ARRAYSIZE (sz) - 1, lLength));
	str.Format (_T ("{%d [%s], BatchID: %d, Length: %d} %s"), 
		pmsg->Header.Cmd, fj_socketCommandString (pmsg->Header.Cmd),
		pmsg->Header.BatchID,
		pmsg->Header.Length,
		CString (a2w (sz)));

	return str;
}

std::vector <std::wstring> CNextThread::fj_getLabelIDs ()
{
	using namespace FoxjetDatabase;

	std::vector <std::wstring> v;
	
	m_pView->SendMessage (WM_NEXT_GET_TASK_NAMES, (WPARAM)GetLine ()->GetID (), (LPARAM)&v);

	return v;
}


std::vector <std::wstring> CNextThread::fj_getFontIDs ()
{
	return GetFileTitles (FoxjetIpElements::CTextElement::GetFontDir (), _T ("*.fjf"));
}

std::vector <std::wstring> CNextThread::fj_getBmpIDs ()
{
	return GetFileTitles (FoxjetIpElements::CBitmapElement::GetDefPath (), _T ("*.bmp"));
}

std::vector <std::wstring> CNextThread::GetFileTitles (const CString & strPath, const CString & strExt)
{
	using namespace FoxjetDatabase;
	using namespace FoxjetCommon;

	std::vector <std::wstring> v;
	CStringArray vFiles;

	GetFiles (strPath, strExt, vFiles);

	for (int i = 0; i < vFiles.GetSize (); i++) {
		CString str = vFiles [i];
		int nIndex [] = { str.ReverseFind ('\\'), str.ReverseFind ('.') };

		if (nIndex [0] != -1 && nIndex [1] != -1 && nIndex [1] > nIndex [0])
			v.push_back (std::wstring (str.Mid (nIndex [0] + 1, (nIndex [1] - nIndex [0]) - 1)));
	}

	return v;
}


int CNextThread::Find (CArray <CNextThread::CSQL, CNextThread::CSQL &> & v, const CString & strSQL)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (!strSQL.CompareNoCase (v [i].m_str))
			return i;

	return -1;
}

bool CNextThread::Send (CNextThread::CSocketHandler & s, _FJ_SOCKET_MESSAGE * pmsg, ULONG lLength, LPCTSTR lpszFile, ULONG lLine)
{
	const ULONG lTotalLen = sizeof(_FJ_SOCKET_MESSAGE_HEADER) + lLength;
	_FJ_SOCKET_MESSAGE * pmsgHTONL = (_FJ_SOCKET_MESSAGE *)new BYTE [lTotalLen];

	memcpy (pmsgHTONL, pmsg, lTotalLen);

	pmsgHTONL->Header.Cmd		= htonl (pmsgHTONL->Header.Cmd);
	pmsgHTONL->Header.BatchID	= htonl (pmsgHTONL->Header.BatchID);
	pmsgHTONL->Header.Length	= htonl (pmsgHTONL->Header.Length);

	int nSend = ::send (s, (const char *)pmsgHTONL, sizeof(_FJ_SOCKET_MESSAGE_HEADER) + lLength, 0);

	bool bResult = (nSend == (sizeof(_FJ_SOCKET_MESSAGE_HEADER) + lLength)) ? true : false;

	delete [] pmsgHTONL;

	if (nSend == 0) 
		Close (s);

	{
		char sz [128] = { 0 };
		CString str;
		CMainFrame * pFrame = (CMainFrame *)theApp.m_pMainWnd;
		CView * pView = pFrame->m_pControlView;

		memcpy (sz, pmsg->Data, min (ARRAYSIZE (sz) - 1, lLength));
		str.Format (_T ("%s (send: %04d bytes) %s"), GetLocalAddress (s), nSend, GetTrace (pmsg, lLength, lpszFile, lLine));

		#ifdef _DEBUG
		CDebug::Trace (str, true, lpszFile, lLine);
		#endif

		//while (!::PostMessage (pView->GetSafeHwnd(), WM_DIAGNOSTIC_DATA, (WPARAM)new CString (str), 0))
		//	::Sleep (0);
	}

	return bResult;
}

void CNextThread::SetSocketOptions (SOCKET s)
{
	int iOption = 1, iRet;

	iRet = setsockopt (s, SOL_SOCKET, SO_KEEPALIVE, (char*)&iOption, sizeof(iOption));
	iRet = setsockopt (s, SOL_SOCKET, SO_REUSEADDR, (char*)&iOption, sizeof(iOption));
	//iRet = setsockopt (s, SOL_SOCKET, SO_NBIO,      (char*)&iOption, sizeof(iOption));
}

CString NEXT::fj_socketCommandString (LONG lCmd)
{
	LPSTR pstr;

	switch ( lCmd )
	{
		case IPC_CONNECTION_CLOSE:				pstr = "IPC_CONNECTION_CLOSE";      break;
		case IPC_INVALID:						pstr = "IPC_INVALID";               break;
		case IPC_ECHO:							pstr = "IPC_ECHO";                  break;
		case IPC_STATUS:						pstr = "IPC_STATUS";                break;
		case IPC_GET_STATUS:					pstr = "IPC_GET_STATUS";            break;
		case IPC_PHOTO_TRIGGER:					pstr = "IPC_PHOTO_TRIGGER";         break;
		case IPC_EDIT_STATUS:					pstr = "IPC_EDIT_STATUS";           break;
		case IPC_GET_EDIT_STATUS:				pstr = "IPC_GET_EDIT_STATUS";       break;
		case IPC_EDIT_START:					pstr = "IPC_EDIT_START";            break;
		case IPC_EDIT_SAVE:						pstr = "IPC_EDIT_SAVE";             break;
		case IPC_EDIT_CANCEL:					pstr = "IPC_EDIT_CANCEL";           break;
		case IPC_GET_LABEL_IDS:					pstr = "IPC_GET_LABEL_IDS";         break;
		case IPC_SET_LABEL_IDS:					pstr = "IPC_SET_LABEL_IDS";         break;
		case IPC_GET_LABEL_ID_SELECTED:			pstr = "IPC_GET_LABEL_ID_SELECTED"; break;
		case IPC_SET_LABEL_ID_SELECTED:			pstr = "IPC_SET_LABEL_ID_SELECTED"; break;
		case IPC_GET_LABELS:					pstr = "IPC_GET_LABELS";            break;
		case IPC_GET_LABEL:						pstr = "IPC_GET_LABEL";             break;
		case IPC_PUT_LABEL:						pstr = "IPC_PUT_LABEL";             break;
		case IPC_DELETE_LABEL:					pstr = "IPC_DELETE_LABEL";          break;
		case IPC_DELETE_ALL_LABELS:				pstr = "IPC_DELETE_ALL_LABELS";     break;
		case IPC_SELECT_LABEL:					pstr = "IPC_SELECT_LABEL";          break;
		case IPC_GET_MSG_IDS:					pstr = "IPC_GET_MSG_IDS";           break;
		case IPC_SET_MSG_IDS:					pstr = "IPC_SET_MSG_IDS";           break;
		case IPC_GET_MSGS:						pstr = "IPC_GET_MSGS";              break;
		case IPC_GET_MSG:						pstr = "IPC_GET_MSG";               break;
		case IPC_PUT_MSG:						pstr = "IPC_PUT_MSG";               break;
		case IPC_DELETE_MSG:					pstr = "IPC_DELETE_MSG";            break;
		case IPC_GET_TEXT_IDS:					pstr = "IPC_GET_TEXT_IDS";          break;
		case IPC_SET_TEXT_IDS:					pstr = "IPC_SET_TEXT_IDS";          break;
		case IPC_GET_TEXT_ELEMENTS:				pstr = "IPC_GET_TEXT_ELEMENTS";     break;
		case IPC_GET_TEXT:						pstr = "IPC_GET_TEXT";              break;
		case IPC_PUT_TEXT:						pstr = "IPC_PUT_TEXT";              break;
		case IPC_DELETE_TEXT:					pstr = "IPC_DELETE_TEXT";           break;
		case IPC_GET_BITMAP_IDS:				pstr = "IPC_GET_BITMAP_IDS";        break;
		case IPC_SET_BITMAP_IDS:				pstr = "IPC_SET_BITMAP_IDS";        break;
		case IPC_GET_BITMAP_ELEMENTS:			pstr = "IPC_GET_BITMAP_ELEMENTS";   break;
		case IPC_GET_BITMAP:					pstr = "IPC_GET_BITMAP";            break;
		case IPC_PUT_BITMAP:					pstr = "IPC_PUT_BITMAP";            break;
		case IPC_DELETE_BITMAP:					pstr = "IPC_DELETE_BITMAP";         break;
		case IPC_GET_DYNIMAGE_IDS:				pstr = "IPC_GET_DYNIMAGE_IDS";      break;
		case IPC_SET_DYNIMAGE_IDS:				pstr = "IPC_SET_DYNIMAGE_IDS";      break;
		case IPC_GET_DYNIMAGE_ELEMENTS:			pstr = "IPC_GET_DYNIMAGE_ELEMENTS"; break;
		case IPC_GET_DYNIMAGE:					pstr = "IPC_GET_DYNIMAGE";          break;
		case IPC_PUT_DYNIMAGE:					pstr = "IPC_PUT_DYNIMAGE";          break;
		case IPC_DELETE_DYNIMAGE:				pstr = "IPC_DELETE_DYNIMAGE";       break;
		case IPC_PUT_DYNIMAGE_DATA:				pstr = "IPC_PUT_DYNIMAGE_DATA";     break;
		case IPC_GET_DATETIME_IDS:				pstr = "IPC_GET_DATETIME_IDS";      break;
		case IPC_SET_DATETIME_IDS:				pstr = "IPC_SET_DATETIME_IDS";      break;
		case IPC_GET_DATETIME_ELEMENTS:			pstr = "IPC_GET_DATETIME_ELEMENTS"; break;
		case IPC_GET_DATETIME:					pstr = "IPC_GET_DATETIME";          break;
		case IPC_PUT_DATETIME:					pstr = "IPC_PUT_DATETIME";          break;
		case IPC_DELETE_DATETIME:				pstr = "IPC_DELETE_DATETIME";       break;
		case IPC_GET_COUNTER_IDS:				pstr = "IPC_GET_COUNTER_IDS";       break;
		case IPC_SET_COUNTER_IDS:				pstr = "IPC_SET_COUNTER_IDS";       break;
		case IPC_GET_COUNTER_ELEMENTS:			pstr = "IPC_GET_COUNTER_ELEMENTS";  break;
		case IPC_GET_COUNTER:					pstr = "IPC_GET_COUNTER";           break;
		case IPC_PUT_COUNTER:					pstr = "IPC_PUT_COUNTER";           break;
		case IPC_DELETE_COUNTER:				pstr = "IPC_DELETE_COUNTER";        break;
		case IPC_GET_BARCODE_IDS:				pstr = "IPC_GET_BARCODE_IDS";       break;
		case IPC_SET_BARCODE_IDS:				pstr = "IPC_SET_BARCODE_IDS";       break;
		case IPC_GET_BARCODE_ELEMENTS:			pstr = "IPC_GET_BARCODE_ELEMENTS";  break;
		case IPC_GET_BARCODE:					pstr = "IPC_GET_BARCODE";           break;
		case IPC_PUT_BARCODE:					pstr = "IPC_PUT_BARCODE";           break;
		case IPC_DELETE_BARCODE:				pstr = "IPC_DELETE_BARCODE";        break;
		case IPC_GET_FONT_IDS:					pstr = "IPC_GET_FONT_IDS";          break;
		case IPC_SET_FONT_IDS:					pstr = "IPC_SET_FONT_IDS";          break;
		case IPC_GET_FONTS:						pstr = "IPC_GET_FONTS";             break;
		case IPC_GET_FONT:						pstr = "IPC_GET_FONT";              break;
		case IPC_PUT_FONT:						pstr = "IPC_PUT_FONT";              break;
		case IPC_DELETE_FONT:					pstr = "IPC_DELETE_FONT";           break;
		case IPC_GET_BMP_IDS:					pstr = "IPC_GET_BMP_IDS";           break;
		case IPC_SET_BMP_IDS:					pstr = "IPC_SET_BMP_IDS";           break;
		case IPC_GET_BMPS:						pstr = "IPC_GET_BMPS";              break;
		case IPC_GET_BMP:						pstr = "IPC_GET_BMP";               break;
		case IPC_PUT_BMP:						pstr = "IPC_PUT_BMP";               break;
		case IPC_DELETE_BMP:					pstr = "IPC_DELETE_BMP";            break;
		case IPC_DELETE_ALL_BMPS:				pstr = "IPC_DELETE_ALL_BMPS";       break;
		case IPC_GET_DYNTEXT_IDS:				pstr = "IPC_GET_DYNTEXT_IDS";       break;
		case IPC_SET_DYNTEXT_IDS:				pstr = "IPC_SET_DYNTEXT_IDS";       break;
		case IPC_GET_DYNTEXT_ELEMENTS:			pstr = "IPC_GET_DYNTEXT_ELEMENTS";  break;
		case IPC_GET_DYNTEXT:					pstr = "IPC_GET_DYNTEXT";			break;
		case IPC_PUT_DYNTEXT:					pstr = "IPC_PUT_DYNTEXT";			break;
		case IPC_DELETE_DYNTEXT:				pstr = "IPC_DELETE_DYNTEXT";		break;
		case IPC_GET_DYNBARCODE_IDS:			pstr = "IPC_GET_DYNBARCODE_IDS";    break;
		case IPC_SET_DYNBARCODE_IDS:			pstr = "IPC_SET_DYNBARCODE_IDS";    break;
		case IPC_GET_DYNBARCODE_ELEMENTS:		pstr = "IPC_GET_DYNBARCODE_ELEMENTS";       break;
		case IPC_GET_DYNBARCODE:				pstr = "IPC_GET_DYNBARCODE";		break;
		case IPC_PUT_DYNBARCODE:				pstr = "IPC_PUT_DYNBARCODE";		break;
		case IPC_DELETE_DYNBARCODE:				pstr = "IPC_DELETE_DYNBARCODE";     break;
		case IPC_GET_GROUP_INFO:				pstr = "IPC_GET_GROUP_INFO";        break;
		case IPC_SET_GROUP_INFO:				pstr = "IPC_SET_GROUP_INFO";        break;
		case IPC_GET_SYSTEM_INFO:				pstr = "IPC_GET_SYSTEM_INFO";       break;
		case IPC_GET_SYSTEM_INTERNET_INFO:		pstr = "IPC_GET_SYSTEM_INTERNET_INFO"; break;
		case IPC_GET_SYSTEM_TIME_INFO:			pstr = "IPC_GET_SYSTEM_TIME_INFO";  break;
		case IPC_GET_SYSTEM_DATE_STRINGS:		pstr = "IPC_GET_SYSTEM_DATE_STRINGS";   break;
		case IPC_GET_SYSTEM_PASSWORDS:			pstr = "IPC_GET_SYSTEM_PASSWORDS";  break;
		case IPC_GET_SYSTEM_BARCODES:			pstr = "IPC_GET_SYSTEM_BARCODES";   break;
		case IPC_GET_SYSTEM_DATAMATRIX:			pstr = "IPC_GET_SYSTEM_DATAMATRIX";   break;
		case IPC_SET_SYSTEM_INFO:				pstr = "IPC_SET_SYSTEM_INFO";       break;
		case IPC_GET_PRINTER_ID:				pstr = "IPC_GET_PRINTER_ID";        break;
		case IPC_GET_PRINTER_INFO:				pstr = "IPC_GET_PRINTER_INFO";      break;
		case IPC_GET_PRINTER_PKG_INFO:			pstr = "IPC_GET_PRINTER_PKG_INFO";  break;
		case IPC_GET_PRINTER_PHY_INFO:			pstr = "IPC_GET_PRINTER_PHY_INFO";  break;
		case IPC_GET_PRINTER_OP_INFO:			pstr = "IPC_GET_PRINTER_OP_INFO";   break;
		case IPC_SET_PRINTER_INFO:				pstr = "IPC_SET_PRINTER_INFO";      break;
		case IPC_GET_PRINTER_ELEMENTS:			pstr = "IPC_GET_PRINTER_ELEMENTS";  break;
		case IPC_PUT_PRINTER_ELEMENT:			pstr = "IPC_PUT_PRINTER_ELEMENT";   break;
		case IPC_DELETE_ALL_PRINTER_ELEMENTS:   pstr = "IPC_DELETE_ALL_PRINTER_ELEMENTS";   break;
		case IPC_FIRMWARE_UPGRADE:				pstr = "IPC_FIRMWARE_UPGRADE";      break;
		case IPC_SET_PH_TYPE:					pstr = "IPC_SET_PH_TYPE";           break;
		case IPC_SET_PRINTER_MODE:				pstr = "IPC_SET_PRINTER_MODE";      break;
		case IPC_GET_PRINTER_MODE:				pstr = "IPC_GET_PRINTER_MODE";      break;
		case IPC_SET_SELECTED_COUNT:			pstr = "IPC_SET_SELECTED_COUNT";    break;
		case IPC_GET_SELECTED_COUNT:			pstr = "IPC_GET_SELECTED_COUNT";    break;
		case IPC_SELECT_VARDATA_ID:				pstr = "IPC_SELECT_VARDATA_ID";     break;
		case IPC_GET_VARDATA_ID:				pstr = "IPC_GET_VARDATA_ID";        break;
		case IPC_SET_VARDATA_ID:				pstr = "IPC_SET_VARDATA_ID";        break;
		case IPC_SAVE_ALL_PARAMS:				pstr = "IPC_SAVE_ALL_PARAMS";       break;
		case IPC_GET_SW_VER_INFO:				pstr = "IPC_GET_SW_VER_INFO";       break;
		case IPC_GET_GA_VER_INFO:				pstr = "IPC_GET_GA_VER_INFO";       break;
		case IPC_GA_UPGRADE:					pstr = "IPC_GA_UPGRADE";			break;
		case IPC_SET_STATUS_MODE:				pstr = "IPC_SET_STATUS_MODE";		break;
		case IPC_GET_STATUS_MODE:				pstr = "IPC_GET_STATUS_MODE";		break;
		case IPC_GET_DISK_USAGE:				pstr = "IPC_GET_DISK_USAGE";		break;
		case IPC_GET_PH_TYPE:					pstr = "IPC_GET_PH_TYPE";           break;
		case IPC_PUT_BMP_DRIVER:				pstr = "IPC_PUT_BMP_DRIVER";		break;
		default:								pstr = "[unknown command]";			break;
	}

	return a2w (pstr);
}

ULONG CNextThread::Process_FJ_SOCKET_MESSAGE (CNextThread::CSocketHandler & s, _FJ_SOCKET_MESSAGE * psmsg)
{
	ULONG lResult = 0;

	for (int nPasses = 0; (lResult < psmsg->Header.Length); nPasses++) {
		UINT n = ::recv (s, (char *)&psmsg->Data [lResult], (psmsg->Header.Length - lResult), 0);
		::Sleep (1);
		lResult += n;
	}

	return lResult;

	//ULONG lBytesRead1 = lBytesRead;
	//ULONG lDataLength = psmsg->Header.Length;
	//ULONG lCmd        = psmsg->Header.Cmd;
	//ULONG lBytesRead2 = 0;
	//ULONG lBufferSize = _FJ_SOCKET_BUFFER_SIZE;

	//if ( (0 < lDataLength) && (lBufferSize >= lDataLength) )
	//{
	//	LPSTR pReadLoc;
	//	LONG  lReadLen;
	//	LONG  lReadTries;
	//	pReadLoc = (LPSTR)(&(psmsg->Data));
	//	lReadLen = lDataLength;
	//	lReadTries = 10;

	//	while ( (lReadLen>0) && (lReadTries>0) )
	//	{
	//		lBytesRead = ::recv (s, pReadLoc, lReadLen, 0 );
	//		lReadTries--;
	//		if ( lBytesRead >= 0 )
	//		{
	//			lBytesRead2 += lBytesRead;
	//			pReadLoc    += lBytesRead;
	//			lReadLen    -= lBytesRead;

	//			// test for partial read
	//			if ( lReadLen > 0 )
	//			{

	//				lReadTries++;
	//			}

	//			if ( lReadTries > 0 )
	//			{
	//				::Sleep (10);
	//			}
	//			else
	//			{
	//				// recv error. force our data error.
	//				lBytesRead2 = -1;
	//			}
	//		}
	//		else if ( -1 == lBytesRead )
	//		{
	//			TRACEF (CUdpThread::GetError (::WSAGetLastError ()));
	//		}
	//		else
	//		{
	//			lBytesRead2 = lBytesRead;
	//		}
	//	}
	//}

	//return lBytesRead2;
}

void CNextThread::fj_print_head_SetStatus ()
{
	m_strHeadStatus = "Ready;";

	if (CProdLine * pLine = GetLine ()) {
		DWORD dw = pLine->GetErrorState ();
		CString str, strInk;
		Head::CHeadInfo info = GetHead ();

		if (dw & INK_OUT)
			strInk += _T ("EMPTY");
		else if (dw & LOW_INK) 
			strInk += _T ("LOW");
		else 
			strInk = _T ("OK");

		str.Format (_T ("InkUsage=%f;CONVEYOR_SPEED=%f;InkLevel=%s;VoltReady=%s;Heater1Ready=%s;Heater1On=%s;PRINT_DRIVER=%d"),
			info.m_dRemainingInk,
			info.GetLineSpeed (),
			strInk, 											// InkLevel
			!(dw & LOW_VOLTAGE) 	? _T ("T") : _T ("F"), 		// VoltReady
			!(dw & LOW_TEMP)		? _T ("T") : _T ("F"),		// Heater1Ready
		 	(false)					? _T ("T") : _T ("F"),		// Heater1On
			info.m_bPrintDriver);

		//if (info.m_bPrintDriver) {
		//	CString strTmp;

		//	strTmp.Format (_T (";PRINT_DRIVER_BOLD_FACTOR=%d"), info.m_nPrintDriverBold);
		//	str += strTmp;
		//}

		m_strHeadStatus += str + _T (";");
	}
}

void CNextThread::fj_print_head_SendSelectedCNT (CNextThread::CSocketHandler & s)
{
	CString str;
	CString strLabel;
	ULONG lCount = 0; 
	_FJ_SOCKET_MESSAGE msg;

	if (CProdLine * pLine = GetLine ()) {
		__int64 lFind = 0, lMin = 0, lMax = 0, lMaster = 0;

		pLine->GetCounts (NULL, lFind, lMin, lMax, lMaster);
		lCount = (ULONG)lMaster;
		strLabel = pLine->GetCurrentTask ();

		if (strLabel.IsEmpty ())
			strLabel = _T ("(none)");
	}

	str.Format (_T ("LabelSel=%s;CountSel=%d;"),
		strLabel,
		lCount);

	msg.Header.Cmd = IPC_STATUS;
	msg.Header.BatchID = -1;
	msg.Header.Length = str.GetLength ();

	strcpy ((char *)msg.Data, w2a (str));

	SEND (s, &msg, msg.Header.Length);
}

void CNextThread::fj_print_head_SendInfoAll (CNextThread::CSocketHandler & s)
{
	CString str = _T ("HeadStatus=");
	_FJ_SOCKET_MESSAGE msg;

	str += m_strHeadStatus;

	fj_print_head_SendSelectedCNT (s);

	msg.Header.Cmd = IPC_STATUS;
	msg.Header.BatchID = -1;
	msg.Header.Length = str.GetLength ();

	strcpy ((char *)msg.Data, w2a (str));

	SEND (s, &msg, msg.Header.Length);
}

CProdLine * CNextThread::GetLine (int nLine)
{
	using namespace FoxjetDatabase;
	using namespace Head;

	if (m_pView) {
		if (CControlDoc * pDoc = (CControlDoc *)m_pView->GetDocument ()) 
			return pDoc->GetLine (nLine);
	}

	return NULL;
}

Head::CHeadInfo CNextThread::GetHead (int nLine)
{
	using namespace FoxjetDatabase;
	using namespace Head;

	if (m_pView) {
		if (CControlDoc * pDoc = (CControlDoc *)m_pView->GetDocument ()) {
			if (CProdLine * pLine = GetLine (nLine)) {
				CPrintHeadList list;
				int nMin = 0;

				pDoc->GetActiveHeads (pLine->GetName (), list);

				for (POSITION pos = list.GetStartPosition(); pos != NULL; ) {
					CHead * pHead = NULL;
					DWORD dwKey = 0;

					list.GetNextAssoc (pos, dwKey, (void *&)pHead);

					int n = _ttoi (CHead::GetInfo (pHead).GetUID ());
					nMin = nMin == 0 ? n : min (nMin, n);
				}

				return CHead::GetInfo (pLine->GetHeadByUID (ToString (nMin)));
			}
		}
	}

	return Head::CHeadInfo ();
}

void CNextThread::fj_StatusModeToString (char * psz)
{
	strcpy (psz, w2a (ToString (this, ::mapStatus)));
}

void CNextThread::fj_SystemToString (char * psz)
{
	strcpy (psz, w2a (ToString (this, ::mapSystem)));
}

void CNextThread::fj_SystemFromString (char * psz)
{
	CString str = a2w (psz);
	CStringArray v, v2;
	CMapStringToString map;
	bool bValid = false;

	{ // want PRINT_RESOLUTION before ENCODER_COUNT
		const LPCTSTR lpszENCODER_COUNT		= _T ("ENCODER_COUNT");
		const LPCTSTR lpszPRINT_RESOLUTION	= _T ("PRINT_RESOLUTION");

		if (str.Find (lpszENCODER_COUNT) != -1 && str.Find (lpszPRINT_RESOLUTION) != -1) {
			CString strEncoder	= FoxjetDatabase::Extract (str, lpszENCODER_COUNT,		_T (";"), true);
			CString strRes		= FoxjetDatabase::Extract (str, lpszPRINT_RESOLUTION,	_T (";"), true);

			if (str.Replace (strEncoder, _T ("")) > 0 && str.Replace (strRes, _T ("")) > 0) 
				str = strRes + strEncoder + str;
		}
	}

	{
		const LPCTSTR lpszENCODER_DIVISOR	= _T ("ENCODER_DIVISOR");
		const LPCTSTR lpszENCODER_WHEEL		= _T ("ENCODER_WHEEL");
		
		if (str.Find (lpszENCODER_DIVISOR) != -1 && str.Find (lpszENCODER_WHEEL) != -1) {
			CString strENCODER_DIVISOR	= FoxjetDatabase::Extract (str, lpszENCODER_DIVISOR,	_T (";"), true);
			CString strENCODER_WHEEL	= FoxjetDatabase::Extract (str, lpszENCODER_WHEEL,		_T (";"), true);

			if (str.Replace (strENCODER_DIVISOR, _T ("")) > 0 && str.Replace (strENCODER_WHEEL, _T (""))) 
				str = strENCODER_DIVISOR + strENCODER_WHEEL + str;
		}
	}

	FoxjetDatabase::Tokenize (str, v, ';');

	for (int i = 0; i < v.GetSize (); i++) {
		FoxjetDatabase::Tokenize (v [i], v2, '=');

		if (v2.GetSize () >= 2) {
			lpfctSet pfct = NULL;

			TRACEF (v2 [0] + _T (": ") + v2 [1]);

			pfct = FindSet (v2 [0], ::mapSystem);

			if (!pfct)
				pfct = FindSet (v2 [0], ::mapParams);
			if (!pfct)
				pfct = FindSet (v2 [0], ::mapBarcode);
			if (!pfct)
				pfct = FindSet (v2 [0], ::mapDataMatrix);
			if (!pfct)
				pfct = FindSet (v2 [0], ::mapStatus);
			if (!pfct)
				pfct = FindSet (v2 [0], ::mapPrintMode);
			if (!pfct)
				pfct = FindSet (v2 [0], ::mapDate);
			if (!pfct)
				pfct = FindSet (v2 [0], ::mapTime);

			if (pfct) {
				((* this).*(pfct)) (v2 [1]);
				FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, m_strRegSection, v2 [0], v2 [1]);
				bValid = true;
			}
		}
	}

	if (!bValid) {
		TRACEF (psz);
		int nBreak = 0;//ASSERT (0);
	}
}

void CNextThread::fj_PrintHeadToString (char * psz)
{
	strcpy (psz, w2a (ToString (this, ::mapParams)));
}

void CNextThread::fj_PrintHeadFromString (char * psz)
{
	fj_SystemFromString (psz);
}

void CNextThread::fj_PrintModeToString (char * psz)
{
	strcpy (psz, w2a (ToString (this, ::mapPrintMode)));
}

void CNextThread::fj_PrintModeFromString (char * psz)
{
	fj_SystemFromString (psz);
}

void CNextThread::fj_SystemTimeToString (char * psz)
{
	strcpy (psz, w2a (ToString (this, ::mapTime)));
}

void CNextThread::fj_SystemDateStringsToString (char * psz)
{
	strcpy (psz, w2a (ToString (this, ::mapDate)));
}

void CNextThread::fj_SystemBarCodesToString (char * psz)
{
	CString str;
	const int nLastIndex = ::fj_SysBCIndex;

	for (::fj_SysBCIndex = 0; ::fj_SysBCIndex < ARRAYSIZE (bcArray); ::fj_SysBCIndex++) 
		str += ToString (this, ::mapBarcode);

	::fj_SysBCIndex = nLastIndex;
	strcpy (psz, w2a (str));
}

void CNextThread::fj_SystemDataMatrixToString (char * psz)
{
	CString str;
	const int nLastIndex = ::fj_SysDMIndex;

	for (::fj_SysDMIndex = 0; ::fj_SysDMIndex < ARRAYSIZE (bcArray); ::fj_SysDMIndex++) 
		str += ToString (this, ::mapDataMatrix);

	::fj_SysDMIndex = nLastIndex;
	strcpy (psz, w2a (str));
}

void CNextThread::WriteCache (ULONG lCmd, LPSTR pData)
{
	CString strFile;
	ULONG lGetCmd = -1;
	/*
	IPC_SET_PRINTER_INFO
		DIRECTION=RIGHT_TO_LEFT;SLANTVALUE=0;SLANTANGLE=90.0;DISTANCE=3.0;HEIGHT=0.0;PHOTOCELL_BACK=F;DOUBLE_PULSE=F;PHOTOCELL_INTERNAL=T;OP_AUTOPRINT=1950;SAVE_DYN_DATA=F;SERIAL_PORT=NONE;SCANNER_USE_ONCE=F;BAUD_RATE=38400;SAVE_SERIAL_DATA=F;IDLE_AFTER_PRINT=F;OP_HEAD_INVERT=F;OP_AMS=T;OP_AMSA1=3;OP_AMSA2=1000;OP_AMSA3=28;OP_AMSA4=1000;OP_AMSA5=5500;OP_AMSA6=2;OP_AMSA7=100;STANDBY_INTERVAL=0.0;
	IPC_SET_SYSTEM_INFO
		IPC_GET_SYSTEM_TIME_INFO:		UNITS=ENGLISH_INCHES;TIME=24;INTERNAL_CLOCK=T;ENCODER_COUNT=150.0;PRINT_RESOLUTION=300.0;CONVEYOR_SPEED=12.0;ENCODER=F;ENCODER_WHEEL=300.0;ENCODER_DIVISOR=2;APP_VER={Ver,3,67,920,0,1};FIRST_DAY=1;DAYS_WEEK1=4;WEEK53=53;ROLLOVER_HOURS=0.0;SHIFT1=7:0;SHIFT2=15:0;SHIFT3=23:0;SHIFTCODE1="FIRST";SHIFTCODE2="2nd";SHIFTCODE3="Third";
		IPC_GET_SYSTEM_DATE_STRINGS:	DAYS="SUN","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday";MONTHS="JAN","February","March","April","May","June","July","August","September","October","November","December";DAYS_SPECIAL="SONNTAG","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag";MONTHS_SPECIAL="JANUAR","Februar","Marz","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember";HOURS="AA","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X";AMPM=" am"," PM";
		IPC_GET_SYSTEM_BARCODES:		BC_N=0;BC_NAME="Mag 100";BC_TY=8;BC_H=32;BC_CD=F;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;BC_N=1;BC_NAME="Mag  90";BC_TY=5;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;BC_N=2;BC_NAME="Mag  80";BC_TY=4;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;BC_N=3;BC_NAME="Mag  70";BC_TY=1;BC_H=32;BC_CD=F;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;BC_N=4;BC_NAME="Mag  62";BC_TY=13;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;BC_N=5;BC_NAME="Custom 1";BC_TY=2;BC_H=32;BC_CD=F;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;BC_N=6;BC_NAME="Custom 2";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;BC_N=7;BC_NAME="Custom 3";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;
		IPC_GET_SYSTEM_DATAMATRIX:		DM_N=0;DM_NAME="Mag 100";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;DM_N=1;DM_NAME="Mag  90";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;DM_N=2;DM_NAME="Mag  80";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;DM_N=3;DM_NAME="Mag  70";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;DM_N=4;DM_NAME="Mag  62";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;DM_N=5;DM_NAME="Custom 1";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;DM_N=6;DM_NAME="Custom 2";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;DM_N=7;DM_NAME="Custom 3";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;
	*/

	struct
	{
		ULONG m_lCmd;
		LPCSTR m_lpszStart;
		LPCSTR m_lpszEnd;
	}
	static const map [] = 
	{
		{ IPC_SET_PH_TYPE,				 ("HeadType"),			("="),					},
		{ IPC_SET_PRINTER_INFO,			 ("DIRECTION="),		("STANDBY_INTERVAL="),	},
		{ IPC_GET_SYSTEM_TIME_INFO,		 ("UNITS="),			("SHIFTCODE3="),		},
		{ IPC_GET_SYSTEM_DATE_STRINGS,	 ("DAYS="),				("AMPM="),				},
		{ IPC_GET_SYSTEM_BARCODES,		 ("BC_N=0;BC_NAME="),	("BC_B4="),				},
		{ IPC_GET_SYSTEM_DATAMATRIX,	 ("DM_N=0;DM_NAME="),	("DM_WB="),				},
	};

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		if (strstr (pData, map [i].m_lpszStart) != NULL && strstr (pData, map [i].m_lpszEnd) != NULL) {
			lGetCmd = map [i].m_lCmd;
			break;
		}
	}

	if (lGetCmd != -1) {
		strFile.Format (_T ("%s\\Cache\\%s.next"), 
			FoxjetDatabase::GetHomeDir (),
			fj_socketCommandString (lGetCmd));

		//if (FoxjetFile::IsWriteDirect ()) {
			FoxjetFile::WriteDirect (strFile, (LPBYTE)pData, strlen (pData));
		//}
		//else {
		//	if (FILE * fp = _tfopen (strFile, _T ("w"))) {
		//		fwrite (pData, strlen (pData), 1, fp);
		//		fflush (fp);
		//		fclose (fp);
		//		::Sleep (0);
		//	}
		//	else
		//		TRACEF (_T ("Failed to open: ") + strFile + _T (" --> ") + FORMATMESSAGE (::GetLastError ()));
		//}
	}
}

BOOL CNextThread::fj_IsPrintDriverCmd (ULONG lCmd)
{
	ULONG l [] = 
	{
		IPC_GET_SW_VER_INFO,
		IPC_SET_PRINTER_MODE,

		IPC_PUT_BMP_DRIVER,
		IPC_GET_SW_VER_INFO,
		IPC_GET_GA_VER_INFO,
		IPC_SET_STATUS_MODE,
		IPC_GET_STATUS,
		IPC_GET_PRINTER_INFO,
		IPC_GET_PH_TYPE, 
		IPC_GET_SYSTEM_INFO,
		IPC_GET_SYSTEM_TIME_INFO,
		IPC_GET_SYSTEM_DATE_STRINGS,
		IPC_GET_SYSTEM_BARCODES,
		IPC_GET_SYSTEM_DATAMATRIX,
		IPC_GET_PRINTER_INFO, 
		IPC_GET_PRINTER_MODE,
		IPC_GET_PRINTER_MODE,
	};
	int i = 0;

	for (i = 0; i < ARRAYSIZE (l); i++) 
		if (l [i] == lCmd)
			return TRUE;
				
	return FALSE;
}

void CNextThread::ProcessCmd (CNextThread::CSocketHandler & s, _FJ_SOCKET_MESSAGE & msg)
{
	_FJ_SOCKET_MESSAGE msgOut;
	_FJ_SOCKET_MESSAGE * psmsgOut = &msgOut;
	BOOL bSendString = FALSE;
	BOOL bInvalid = FALSE;
	CString str;
	ULONG lCmd			= msg.Header.Cmd;
	ULONG lBatchID		= msg.Header.BatchID;
	ULONG lDataLength	= msg.Header.Length;
	LPBYTE pData = (LPBYTE)msg.Data;

	ZeroMemory (&msgOut, sizeof (msgOut));
	msgOut.Header.BatchID = msg.Header.BatchID;
	Head::CHeadInfo head = GetHead ();

	TRACER (GetIpcCmdName (msg.Header.Cmd));

	if (head.m_bPrintDriver && !fj_IsPrintDriverCmd (msg.Header.Cmd)) {
		psmsgOut->Header.Cmd = IPC_INVALID;
		*(psmsgOut->Data) = 0;
		lDataLength = strlen( (LPSTR)psmsgOut->Data );
		psmsgOut->Header.Length = lDataLength;
		SEND (s, psmsgOut, lDataLength);
		return;
	}

	switch (msg.Header.Cmd)
	{
	case IPC_CONNECTION_CLOSE:
		Close (s);
	case IPC_GET_STATUS:
		fj_print_head_SetStatus ();
		fj_print_head_SendInfoAll (s);
		break;
	case IPC_GET_PH_TYPE:
		psmsgOut->Header.Cmd = IPC_GET_PH_TYPE;
		sprintf ((LPSTR)psmsgOut->Data, "HeadType=%s;", GetHeadType ());
		psmsgOut->Header.Length = strlen ((LPSTR)psmsgOut->Data);
		bSendString = TRUE;
		break;
	case IPC_SET_PH_TYPE:
		fj_SystemFromString (w2a (_T ("HeadType=") + a2w (pData)));
		WriteCache (msg.Header.Cmd, w2a (_T ("HeadType=") + a2w (pData)));
		break;

	case IPC_GET_SYSTEM_INFO:
		psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
		fj_SystemToString ((LPSTR)psmsgOut->Data);
		bSendString = TRUE;
		break;
	case IPC_SET_SYSTEM_INFO:
		pData[lDataLength] = 0;
		if (lDataLength) {
			fj_SystemFromString ((LPSTR)pData);
			WriteCache (msg.Header.Cmd, (LPSTR)pData);
		}
		break;

	case IPC_GET_PRINTER_INFO:
		psmsgOut->Header.Cmd = IPC_SET_PRINTER_INFO;
		fj_PrintHeadToString ((LPSTR)psmsgOut->Data);
		bSendString = TRUE;
		break;
	case IPC_SET_PRINTER_INFO:
		pData[lDataLength] = 0;
		fj_PrintHeadFromString ((LPSTR)pData);
		WriteCache (msg.Header.Cmd, (LPSTR)pData);
		//fj_PrintHeadReset( pfph );
		break;

	case IPC_GET_PRINTER_MODE:
		psmsgOut->Header.Cmd = IPC_SET_PRINTER_MODE;
		fj_PrintModeToString ((LPSTR)psmsgOut->Data);
		bSendString = TRUE;
		break;
	case IPC_SET_PRINTER_MODE:
		pData[lDataLength] = 0;
		fj_PrintModeFromString ((LPSTR)pData);
		break;

	case IPC_GET_SYSTEM_TIME_INFO:
		psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
		fj_SystemTimeToString ((LPSTR)psmsgOut->Data);
		bSendString = TRUE;
		break;

	case IPC_GET_SYSTEM_DATE_STRINGS:
		psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
		fj_SystemDateStringsToString ((LPSTR)psmsgOut->Data);
		bSendString = TRUE;
		break;

	case IPC_GET_SYSTEM_BARCODES:
		psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
		fj_SystemBarCodesToString ((LPSTR)psmsgOut->Data);
		bSendString = TRUE;
		break;

	case IPC_GET_SYSTEM_DATAMATRIX:
		psmsgOut->Header.Cmd = IPC_SET_SYSTEM_INFO;
		fj_SystemDataMatrixToString ((LPSTR)psmsgOut->Data);
		bSendString = TRUE;
		break;

	case IPC_GET_DISK_USAGE:
		{
			CString str;
			DWORD dwSectorsPerCluster		= 0;
			DWORD dwBytesPerSector			= 0;
			DWORD dwNumberOfFreeClusters	= 0;
			DWORD dwTotalNumberOfClusters	= 0;

			VERIFY (::GetDiskFreeSpace (_T ("C:\\"), &dwSectorsPerCluster, &dwBytesPerSector, &dwNumberOfFreeClusters, &dwTotalNumberOfClusters));
			str.Format (_T ("total_blocks=%d;free_blocks=%d;block_size=%d;total_inodes=%d;free_inodes=%d;"),
				dwTotalNumberOfClusters,
				dwNumberOfFreeClusters,
				dwBytesPerSector,
				dwSectorsPerCluster);
			psmsgOut->Header.Cmd = IPC_GET_DISK_USAGE;
			strcpy ((char *)psmsgOut->Data, w2a (str));
			psmsgOut->Header.Length = strlen ((LPSTR)psmsgOut->Data);
			bSendString = TRUE;
		}
		break;

	case IPC_SAVE_ALL_PARAMS:
		SaveParams ();
		psmsgOut->Header.Cmd = IPC_SAVE_ALL_PARAMS;
		sprintf((LPSTR)psmsgOut->Data, "IPC_SAVE_ALL_PARAMS;");
		bSendString = TRUE;
		break;

	case IPC_GET_STATUS_MODE:
		psmsgOut->Header.Cmd = IPC_SET_STATUS_MODE;
		fj_StatusModeToString ((LPSTR)psmsgOut->Data);
		bSendString = TRUE;
		break;
	case IPC_SET_STATUS_MODE:
		psmsgOut->Header.Cmd = IPC_SET_STATUS_MODE;
		pData[lDataLength] = 0;
		fj_SystemFromString ((LPSTR)pData);//fj_StatusModeFromString( pfph->pfsys, (LPSTR)pData );
		fj_StatusModeToString ((LPSTR)psmsgOut->Data);
		bSendString = TRUE;
		break;

	case IPC_GET_SW_VER_INFO:
		psmsgOut->Header.Cmd = IPC_STATUS;
		sprintf((LPSTR)psmsgOut->Data, "SW_VER=%s;", "4.000");
		bSendString = TRUE;
		break;

	case IPC_GET_GA_VER_INFO:
		{
			DWORD dwVer = GetHead ().GetGaVersion ();
			int n [2] = { (dwVer / 100), (int)dwVer - (n [0] * 100) };
			psmsgOut->Header.Cmd = IPC_STATUS;
			sprintf((LPSTR)psmsgOut->Data, "GA_VER=%d.%04d;", n [0], n [1]);
			bSendString = TRUE;
		}
		break;
	case IPC_GA_UPGRADE:
		bInvalid = FALSE;					
		psmsgOut->Header.Cmd = IPC_STATUS;
		sprintf((LPSTR)psmsgOut->Data, "IPC_GA_UPGRADE complete");
		bSendString = TRUE;
		break;

	case IPC_GET_LABEL_ID_SELECTED:
		psmsgOut->Header.Cmd = IPC_SET_LABEL_ID_SELECTED;
		sprintf ((LPSTR)psmsgOut->Data, "LABELID=%s;", w2a (GetLine ()->GetCurrentTask ()));
		bSendString = TRUE;
		break;

	case IPC_SET_LABEL_ID_SELECTED:
		{
			CStringArray v;
			v.Add (BW_HOST_PACKET_START_TASK);
			v.Add (GetLine ()->GetName ());
			v.Add (FoxjetDatabase::Extract (a2w (msg.Data), _T ("LABELID=\""), _T ("\";")));
			std::vector<std::wstring> vResult = FoxjetDatabase::FromString (CServerThread::ProcessCommand (FoxjetDatabase::ToString (v), m_pView));

			if (vResult.size () >= 3 && _ttoi (vResult [2].c_str ()) == 1) {
				psmsgOut->Header.Cmd = IPC_SET_LABEL_ID_SELECTED;
				sprintf ((LPSTR)psmsgOut->Data, "LABELID=%s;", w2a (GetLine ()->GetCurrentTask ()));
				bSendString = TRUE;
			}
		}
		break;

	case IPC_GET_LABEL_IDS:
		{
			std::vector <std::wstring> v = fj_getLabelIDs ();
			psmsgOut->Header.Cmd = IPC_SET_LABEL_IDS;
			for (int i = 0; i < (int)v.size (); i++)
				v [i] = std::wstring ((LPCTSTR)CString (_T ("\"") + CString (v [i].c_str ()) + _T ("\"")));
			std::string str = w2a (implode <std::wstring> (v, _T (",")).c_str ());
			sprintf ((LPSTR)psmsgOut->Data, "COUNT=%d;LABELIDS=%s;", v.size (), str.c_str ());
			bSendString = TRUE;
		}
		break;

	case IPC_DELETE_LABEL:
		if (m_editSession.IsLocked ()) {
			CString strTitle = FoxjetDatabase::Extract (a2w (msg.Data), _T ("LABELID=\""), _T ("\";"));

			if (m_pView->SendMessage (WM_NEXT_DELETE_TASK, (WPARAM)GetLine ()->GetID (), (LPARAM)&strTitle)) {
				sprintf ((LPSTR)psmsgOut->Data, "IPC_DELETE_LABEL=\"%s\";", strTitle);
				psmsgOut->Header.Cmd = IPC_DELETE_LABEL;
				bSendString = TRUE;
			}
		}
		break;

	case IPC_DELETE_ALL_LABELS:
		if (m_editSession.IsLocked ()) {
			int n = m_pView->SendMessage (WM_NEXT_DELETE_TASK, (WPARAM)GetLine ()->GetID ());
			sprintf ((LPSTR)psmsgOut->Data, "IPC_DELETE_ALL_LABELS=%d;", n);
			psmsgOut->Header.Cmd = IPC_DELETE_LABEL;
			bSendString = TRUE;
		}
		break;

	case IPC_DELETE_BMP:
		if (m_editSession.IsLocked ()) {
			CString strTitle = FoxjetDatabase::Extract (a2w (msg.Data), _T ("BMPID=\""), _T ("\";"));
			CString strPath = FoxjetIpElements::CBitmapElement::GetDefPath () + strTitle + _T ("*.bmp");

			if (::DeleteFile (strPath)) {
				sprintf ((LPSTR)psmsgOut->Data, "IPC_DELETE_BMP=\"%s\";", strTitle);
				psmsgOut->Header.Cmd = IPC_DELETE_FONT;
				bSendString = TRUE;
			}
		}
		break;

	case IPC_DELETE_ALL_BMPS:
		if (m_editSession.IsLocked ()) {
			int n = DeleteAllBitmaps ();
			sprintf ((LPSTR)psmsgOut->Data, "IPC_DELETE_ALL_BMPS=%d", n);
			psmsgOut->Header.Cmd = IPC_DELETE_FONT;
			bSendString = TRUE;
		}
		break;

	case IPC_DELETE_FONT:
		if (m_editSession.IsLocked ()) {
			CString strTitle = FoxjetDatabase::Extract (a2w (msg.Data), _T ("FONTID=\""), _T ("\";"));
			CString strPath = FoxjetIpElements::CTextElement::GetFontDir () + strTitle + _T ("*.fjf");

			if (::DeleteFile (strPath)) {
				sprintf ((LPSTR)psmsgOut->Data, "IPC_DELETE_FONT=\"%s\";", strTitle);
				psmsgOut->Header.Cmd = IPC_DELETE_FONT;
				bSendString = TRUE;
			}
		}
		break;

	case IPC_DELETE_ALL_PRINTER_ELEMENTS:
		if (m_editSession.IsLocked ()) {
			int n = 0;

			n += m_pView->SendMessage (WM_NEXT_DELETE_TASK, (WPARAM)GetLine ()->GetID ());
			n += DeleteAllFonts ();
			n += DeleteAllBitmaps ();

			sprintf ((LPSTR)psmsgOut->Data, "IPC_DELETE_ALL_PRINTER_ELEMENTS=\"%d\";", n);
			psmsgOut->Header.Cmd = IPC_DELETE_FONT;
			bSendString = TRUE;
		}
		break;

	case IPC_EDIT_START:
		if (m_editSession.IsFree ()) 
			m_editSession.Lock (s.m_strAddrFrom, m_pView);
		m_pView->SendMessage (WM_NEXT_REFINE_CONFIG);

		psmsgOut->Header.Cmd = IPC_EDIT_STATUS;
		sprintf ((LPSTR)psmsgOut->Data, "EDIT_LOCK=%s;EDIT_IP=%s;EDIT_TIME=%s;", 
			"SET",
			(const char *)w2a (m_editSession.GetAddress ()),
			(const char *)w2a (m_editSession.GetTime ().Format (::lpszNEXTTimeFormat)));
		bSendString = TRUE;
		break;

	case IPC_EDIT_SAVE:
		if (m_editSession.IsLocked ())
			m_editSession.Free ();
		
		{
			bool bReloadFonts = false;

			for (int i = m_vSQL.GetCount () - 1; i >= 0; i--) {
				CString str = m_vSQL [i], strTmp;

				if (!str.CompareNoCase (::lpszFont)) {
					m_vSQL.RemoveAt (i);
					bReloadFonts = true;
				}
			}

			if (bReloadFonts)
				m_pView->PostMessage (WM_NEXT_INIT_FONTS);
		}
		
		psmsgOut->Header.Cmd = IPC_EDIT_STATUS;
		sprintf ((LPSTR)psmsgOut->Data, "EDIT_LOCK=%s;EDIT_IP=%s;EDIT_TIME=%s;", 
			"FREE",
			(const char *)w2a (m_editSession.GetAddress ()),
			(const char *)w2a (m_editSession.GetTime ().Format (::lpszNEXTTimeFormat)));
		bSendString = TRUE;
		break;

	case IPC_GET_EDIT_STATUS:
		psmsgOut->Header.Cmd = IPC_EDIT_STATUS;
		sprintf ((LPSTR)psmsgOut->Data, "EDIT_LOCK=%s;EDIT_IP=%s;EDIT_TIME=%s;", 
			m_editSession.IsLocked () ? "SET" : "FREE",
			(const char *)w2a (m_editSession.GetAddress ()),
			(const char *)w2a (m_editSession.GetTime ().Format (::lpszNEXTTimeFormat)));
		bSendString = TRUE;
		break;
		break;

	case IPC_GET_BMP_IDS:
		{
			std::vector <std::wstring> v = fj_getBmpIDs ();
			psmsgOut->Header.Cmd = IPC_SET_LABEL_IDS;
			for (int i = 0; i < v.size (); i++)
				v [i] = std::wstring ((LPCTSTR)CString (_T ("\"") + CString (v [i].c_str ()) + _T ("\"")));
			std::string str = w2a (implode <std::wstring> (v, _T (",")).c_str ());
			while ((str.length () + 20) > sizeof (_FJ_SOCKET_MESSAGE)) {
				v.erase (v.end () - 1);
				str = w2a (implode <std::wstring> (v, _T (",")).c_str ());
			}
			sprintf ((LPSTR)psmsgOut->Data, "COUNT=%d;BMPIDS=%s;", v.size (), str.c_str ());
			bSendString = TRUE;
		}
		break;

	case IPC_PUT_BMP:
		if (m_editSession.IsLocked ()) {
			LPSTR pElement = (LPSTR)msg.Data;

			pElement += strlen (pElement) + 1;

			if (fj_bmpCheckFormat ((LPBYTE)pElement)) {
				CStringArray v;

				FoxjetDatabase::ParsePackets (a2w (msg.Data), v);

				if (v.GetSize () > 0) {
					CString str = v [0];

					v.RemoveAll ();
					FoxjetDatabase::Tokenize (str, v, ',');

					if (v.GetSize () >= 2) {
						const CString strDir = FoxjetIpElements::CBitmapElement::GetDefPath ();
						CString strFile = 
							strDir + 
							FoxjetDatabase::Extract (v [1], _T ("\""), _T ("\"")) + 
							_T (".bmp");

						if (::GetFileAttributes (strDir) == -1)
							FoxjetDatabase::CreateDir (strDir);

						if (FILE * fp = _tfopen (strFile, _T ("wb"))) {
							TRACEF (strFile);
							fwrite (pElement, lDataLength, 1, fp);
							fclose (fp);

							bSendString = TRUE;
							strcpy ((char *)psmsgOut->Data, "IPC_PUT_BMP");
							psmsgOut->Header.Cmd = IPC_PUT_BMP;
							psmsgOut->Header.BatchID = lBatchID;
							psmsgOut->Header.Length = strlen ((char *)psmsgOut->Data);
							bInvalid = FALSE;
						}
					}
				}
			}
		}
		else bInvalid = TRUE;
		break;

	case IPC_PUT_PRINTER_ELEMENT:
		if (m_editSession.IsLocked ()) {

			CStringArray v;

			FoxjetDatabase::ParsePackets (a2w (msg.Data), v);
			TRACEF (a2w (msg.Data));
			
			/*
			{Label,"Untitled",12.0,13.0,0,3,F,"Untitled"}{Message,"Untitled",3,"Untitled-Text-1","Untitled-Bitmap-2","Untitled-Barcode-3"}{Text,"Untitled-Text-1",0.000000,224,D,0,1,0,F,F,F,"fx32x22","Text element",L}{Bitmap,"Untitled-Bitmap-2",1.750000,128,D,0,1,F,F,F,"Def"}{Barcode,"Untitled-Barcode-3",9.115001,224,D,0,1,F,F,F,0,T,C,1,"123456"}

			Label,"Untitled",12.0,13.0,0,3,F,"Untitled"
			Message,"Untitled",3,"Untitled-Text-1","Untitled-Bitmap-2","Untitled-Barcode-3"
			Text,"Untitled-Text-1",0.000000,224,D,0,1,0,F,F,F,"fx32x22","Text element",L
			Bitmap,"Untitled-Bitmap-2",1.750000,128,D,0,1,F,F,F,"Def"
			Barcode,"Untitled-Barcode-3",9.115001,224,D,0,1,F,F,F,0,T,C,1,"123456"
			*/

			if (v.GetSize () > 2) {
				if (LPFJLABEL pLabel = fj_LabelFromStr (w2a (v [0]))) {
					std::vector <std::wstring> vIDs = explode (v [1]);

					if (vIDs.size () > 3)
						vIDs.erase (vIDs.begin (), vIDs.begin () + 3);

					if (vIDs.size () == (v.GetSize () - 2)) {
						FoxjetDatabase::TASKSTRUCT task;
						FoxjetDatabase::MESSAGESTRUCT msg;
						CString strProdArea;
						Head::CHeadInfo info = GetHead ();

						task.m_strName = a2w (pLabel->strName);
						task.m_lLineID = GetLine ()->GetID ();
						task.m_lBoxID = GetBoxID (task.m_strName,
							CSize ((int)(pLabel->fBoxWidth * 1000), (int)(pLabel->fBoxHeight * 1000)));

						msg.m_lHeadID = info.m_Config.m_HeadSettings.m_lID;
						msg.m_strName = task.m_strName;

						int nHeight = FoxjetCommon::CBaseElement::LogicalToThousandths (CPoint (0, info.m_Config.m_HeadSettings.Height ()), info.m_Config.m_HeadSettings).y;

						strProdArea.Format (_T ("{ProdArea,%u,%u,%u,%d}"), 
							(int)(pLabel->fBoxWidth * 1000), nHeight, 0, msg.m_lHeadID);

						msg.m_strData += FoxjetCommon::CVersion (FoxjetCommon::verApp).ToString ();
						msg.m_strData += CTime::GetCurrentTime ().Format (_T ("{Timestamp,%c}"));
						msg.m_strData += _T ("{") + v [0] + _T ("}");
						msg.m_strData += strProdArea;

						// {Ver,4,10,923,0,7}{Timestamp,04/20/16 14:01:33}{Label,,12.000000,13.000000,0,3,F}{ProdArea,13000,4000,0,21}

						for (int i = 0; i < vIDs.size (); i++) {
							int nIndex = i + 2;
							CString str = v [nIndex];
							CString strID = _T ("\"") + FoxjetDatabase::ToString (i + 1) + _T ("\"");

							str.Replace (CString (vIDs [i].c_str ()), strID);
							msg.m_strData += _T ("{") + str + _T ("}");
						}

						task.m_vMsgs.Add (msg);
						
						if (m_pView->SendMessage (WM_NEXT_ADD_TASK, (WPARAM)&task)) {
							bSendString = TRUE;
							strcpy ((char *)psmsgOut->Data, "IPC_PUT_PRINTER_ELEMENT");
							psmsgOut->Header.Cmd = IPC_PUT_PRINTER_ELEMENT;
							psmsgOut->Header.BatchID = lBatchID;
							psmsgOut->Header.Length = strlen ((char *)psmsgOut->Data);
						}
					}

					fj_LabelDestroy (pLabel);
				}
			}
		}
		else bInvalid = TRUE;
		break;

	case IPC_GET_SELECTED_COUNT:
		fj_print_head_SendSelectedCNT (s);
		break;

	case IPC_SET_SELECTED_COUNT:
		{
			CStringArray v;
			v.Add (BW_HOST_PACKET_SET_COUNT);
			v.Add (GetLine ()->GetName ());
			v.Add (FoxjetDatabase::Extract (a2w (msg.Data), _T ("CountSel="), _T (";")));
			CServerThread::ProcessCommand (FoxjetDatabase::ToString (v), m_pView);
			fj_print_head_SendSelectedCNT (s);
		}
		break;

	case IPC_GET_FONT_IDS:	
		{ //'COUNT=n;FONTIDS="aaa","bbb","ccc";'
			std::vector <std::wstring> v = fj_getFontIDs ();
			psmsgOut->Header.Cmd = IPC_SET_FONT_IDS;
			for (int i = 0; i < v.size (); i++)
				v [i] = std::wstring ((LPCTSTR)CString (_T ("\"") + CString (v [i].c_str ()) + _T ("\"")));
			std::string str = w2a (implode <std::wstring> (v, _T (",")).c_str ());
			while ((str.length () + 20) > sizeof (_FJ_SOCKET_MESSAGE)) {
				v.erase (v.end () - 1);
				str = w2a (implode <std::wstring> (v, _T (",")).c_str ());
			}
			sprintf ((LPSTR)psmsgOut->Data, "COUNT=%d;FONTIDS=%s;", v.size (), str.c_str ());
			bSendString = TRUE;
		}
		break;
	case IPC_PUT_FONT:
		DIAGNOSTIC (_T ("m_editSession.IsLocked (): ") + CString (m_editSession.IsLocked () ? _T ("true") : _T ("false")));

		if (m_editSession.IsLocked ()) { // {Font,"FONTID1",Type,Height,Width,FirstChar,LastChar,BinaryLength}
			LPSTR pData = (LPSTR)msg.Data;
			pData += strlen (pData) + 1;
			std::vector <std::wstring> v = FoxjetDatabase::FromString ((CString)a2w (msg.Data));

			DIAGNOSTIC (_T ("v.size (): ") + FoxjetDatabase::ToString ((int)v.size ()));

			if (v.size () >= 8) {
				CString strTitle = FoxjetDatabase::Extract (CString (v [1].c_str ()), _T ("\""), _T ("\""));
				CString strPath = FoxjetIpElements::CTextElement::GetFontDir () + strTitle + _T (".fjf");

				if (::GetFileAttributes (FoxjetIpElements::CTextElement::GetFontDir ()) == -1) 
					FoxjetDatabase::CreateDir (FoxjetIpElements::CTextElement::GetFontDir ());

				DIAGNOSTIC (strTitle);
				DIAGNOSTIC (strPath);

				if (FILE * fp = _tfopen (strPath, _T ("wb"))) {
					fwrite ((LPSTR)msg.Data, msg.Header.Length, 1, fp);
					fclose (fp);
					psmsgOut->Header.Cmd = IPC_PUT_FONT;
					bSendString = TRUE;
					MARKSETTINGDIRTY (::lpszFont);
				}
				else
					DIAGNOSTIC (_T ("Failed to write: ") + strPath);

			}
		}
		break;
	
	case IPC_SET_VARDATA_ID:
		{ //ID=1;DATA="data1";ID=2;DATA="data2";
			using FoxjetIpElements::CTableItem;

			std::vector <std::wstring> v, vTmp = explode (std::wstring (a2w (msg.Data)), ';');
			CArray <CTableItem, CTableItem &> vItemsChanged;

			for (int i = 0; i < vTmp.size (); i += 2) {
				if (i < (vTmp.size () - 2)) {
					CString str = vTmp [i].c_str () + CString (_T (";")) + vTmp [i + 1].c_str () + CString (_T (";"));
					v.push_back (std::wstring (str));
				}
			}

			for (int i = 0; i < v.size (); i++) {
				CString str = v [i].c_str ();
				ULONG lID = _ttoi (FoxjetDatabase::Extract (str, _T ("ID="), _T (";")));
				CString strData = FoxjetDatabase::Extract (str, _T ("DATA=\""), _T ("\";"));
				CString strKey;
				CTableItem item;

				item.m_lID = lID;
				item.m_strData = strData;

				strKey.Format (_T ("DYN_DATA_ID[%d]"), lID);
				fj_setWinDynData (lID, w2a (strData));
				FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, m_strRegSection, strKey, strData);

				vItemsChanged.Add (item);
			}

			m_pView->PostMessage (WM_REFRESH_IMAGE, (WPARAM)GetLine ()->GetID (), (LPARAM)5000);

			if (!GetSAVE_DYN_DATA ().CompareNoCase (_T ("T"))) {
				CArray <CTableItem, CTableItem &> vItems;
				ULONG lLineID = GetLine ()->GetID ();

				FoxjetIpElements::GetDynamicData (lLineID, vItems);

				for (int j = 0; j < vItemsChanged.GetSize (); j++) {
					CTableItem item = vItemsChanged [j];

					for (int i = 0; i < vItems.GetSize (); i++) {
						if (vItems [i].m_lID == item.m_lID) {
							vItems.SetAt (i, item);
							FoxjetIpElements::SetDynamicData (lLineID, vItems);
							break;
						}
					}
				}

				{
					CStringArray v, vData;

					for (int j= 0; j < vItems.GetSize (); j++) 
						vData.Add (vItems [j].ToString ());

					v.Add (_T ("Dynamic data"));
					v.Add (FoxjetDatabase::ToString (vData));
					VERIFY (m_pView->SendMessage (WM_NEXT_SET_SYSTEM_PARAMS, (WPARAM)lLineID, (LPARAM)&v) == 1);
				}
			}

			psmsgOut->Header.Cmd = IPC_SET_VARDATA_ID;
			sprintf ((LPSTR)psmsgOut->Data, "IPC_SET_VARDATA_ID=%d;", v.size ());
			bSendString = TRUE;
		}
		break;

	case IPC_SELECT_VARDATA_ID:
		{
			ULONG lID = _ttoi (FoxjetDatabase::Extract (a2w (msg.Data), _T ("ID="), _T (";")));
			FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, m_strRegSection, _T ("SerialVarDataID"), lID);
		}
		break;

	case IPC_GET_VARDATA_ID:
		{
			ULONG lID = _ttoi (FoxjetDatabase::Extract (a2w (msg.Data), _T ("ID="), _T (";")));
			char sz [FJTEXT_SIZE + 1] = { 0 };

			fj_getWinDynData (lID, sz);
			psmsgOut->Header.Cmd = IPC_SET_VARDATA_ID;
			sprintf ((LPSTR)psmsgOut->Data, "ID=%d;DATA=\"%s\";", lID, sz);
			bSendString = TRUE;
		}
		break;

	case IPC_PUT_BMP_DRIVER:
		{
			// StringCbPrintf (sz, ARRAYSIZE (sz), _T ("{bmp,\"PrintDriver\",%lu,%d}"), dwBitmapLen, nCopies);

			if (!m_editSession.IsLocked () && lDataLength) {
				if (char * pEnd = strstr ((char *)msg.Data, "}")) {
					char * pStart = (char *)msg.Data;
					int nLen = (pEnd - pStart) + 1;

					if (nLen > 0) {
						char * psz = new char [nLen + 1];
						CStringArray v;

						memset (psz, 0, nLen + 1);
						strncpy (psz, (char *)msg.Data, nLen);
						FoxjetDatabase::FromString (a2w (psz), v);
						delete [] psz;

						if (v.GetSize () >= 4) {
							LPSTR pElement = (LPSTR)&msg.Data [nLen + 1]; //pData;
							const CString strFile = FoxjetIpElements::CBitmapElement::GetDefPath () + _T ("IPC_PUT_BMP_DRIVER.bmp");

							FoxjetDatabase::CreateDir (FoxjetIpElements::CBitmapElement::GetDefPath ());
							::DeleteFile (strFile);

							if (FILE * fp = _tfopen (strFile, _T ("wb"))) {
								fwrite (pElement, lDataLength - (nLen + 1), 1, fp);
								fclose (fp);
							}
							else ASSERT (0);

							//#ifdef _DEBUG
							//{
							//	CSize sizePixels = FoxjetElements::CBitmapElement::GetSizePixels (strFile, head.m_Config.m_HeadSettings);
							//	CString s; 
							//	
							//	s.Format (_T ("(%d, %d)"), sizePixels.cx, sizePixels.cy); 
							//	TRACEF (s); 
							//	int n = 0;
							//}
							//#endif

							CString strResult;
							Head::CHeadInfo info = GetHead ();
							__int64 nCountUntil = _ttoi (v [3]);
							CStringArray v;

							v.Add (BW_HOST_PACKET_STOP_TASK);
							v.Add (GetLine ()->GetName ());
							TRACER (FoxjetDatabase::ToString (v));
							strResult = CServerThread::ProcessCommand (FoxjetDatabase::ToString (v), m_pView);
							TRACER (strResult);

							SEND_THREAD_MESSAGE (info, TM_SET_PRINT_DRIVER, true);

							v.RemoveAll ();
							v.Add (BW_HOST_PACKET_LOAD_TASK);
							v.Add (GetLine ()->GetName ());
							v.Add (_T ("PrintDriver"));
							TRACER (FoxjetDatabase::ToString (v));
							strResult = CServerThread::ProcessCommand (FoxjetDatabase::ToString (v), m_pView);
							TRACER (strResult);

							v.RemoveAll ();
							v.Add (BW_HOST_PACKET_SET_COUNT);
							v.Add (GetLine ()->GetName ());
							v.Add (_T ("0"));
							TRACER (FoxjetDatabase::ToString (v));
							strResult = CServerThread::ProcessCommand (FoxjetDatabase::ToString (v), m_pView);
							TRACER (strResult);

							v.RemoveAll ();
							v.Add (BW_HOST_PACKET_START_TASK);
							v.Add (GetLine ()->GetName ());
							v.Add (_T ("PrintDriver"));
							TRACER (FoxjetDatabase::ToString (v));
							strResult = CServerThread::ProcessCommand (FoxjetDatabase::ToString (v), m_pView);
							TRACER (strResult);

							//SEND_THREAD_MESSAGE (info, TM_SET_COUNT_UNTIL, &nCountUntil);
							v.RemoveAll ();
							v.Add (BW_HOST_PACKET_SET_COUNT_MAX);
							v.Add (GetLine ()->GetName ());
							v.Add (FoxjetDatabase::ToString (nCountUntil));
							TRACER (FoxjetDatabase::ToString (v));
							strResult = CServerThread::ProcessCommand (FoxjetDatabase::ToString (v), m_pView);
							TRACER (strResult);
						}
					}
				}
			}
		}

		break;

	default:
		bInvalid = TRUE;
		TRACEF (fj_socketCommandString (msg.Header.Cmd));
		int nBreak = 0;
		ASSERT (0);
		break;
	}

	if (bInvalid) {
		psmsgOut->Header.Cmd = IPC_INVALID;
		*(psmsgOut->Data) = 0;
		bSendString = TRUE;
	}

	if (bSendString) {
		ULONG lDataLength = strlen ((LPSTR)psmsgOut->Data);
		psmsgOut->Header.Length = lDataLength;
		
		SEND (s, psmsgOut, lDataLength);
	}
}

ULONG CNextThread::GetBoxID (const CString & strName, const CSize & size)
{
	const CString & str = _T ("NEXT_") + strName;
	return m_pView->SendMessage (WM_GET_BOX_ID, (WPARAM)&str, (LPARAM)&size);
}

CString CNextThread::GetLocalAddress (SOCKET sock, bool bPort)
{
	CString str = CServerThread::GetLocalAddress (sock);

	if (!bPort) {
		int nIndex = str.Find (':');

		if (nIndex != -1)
			str = str.Left (nIndex);
	}

	return str;
}

bool CNextThread::Open ()
{
	bool bResult = false;
	SOCKADDR_IN addrTCP; //static struct	 sockaddr_storage addrTCP;

	memset (&addrTCP, 0, sizeof (addrTCP));
	addrTCP.sin_family		= AF_INET;
	addrTCP.sin_addr.s_addr	= htonl(INADDR_ANY);
	addrTCP.sin_port		= htons (_FJ_SOCKET_NUMBER);

	m_sockTCP.m_sock = socket( AF_INET, SOCK_STREAM, 0 );
	
	if ( m_sockTCP.m_sock != INVALID_SOCKET )
	{
		int iOption = 1;					// set value to turn options on
		long lRet;
		
		lRet = setsockopt( m_sockTCP.m_sock, SOL_SOCKET, SO_KEEPALIVE, (char*)&iOption, sizeof(iOption) );
		lRet = setsockopt( m_sockTCP.m_sock, SOL_SOCKET, SO_REUSEADDR, (char*)&iOption, sizeof(iOption) );
//		lRet = setsockopt( m_sockTCP.m_sock, SOL_SOCKET, SO_NBIO,      (char*)&iOption, sizeof(iOption) );
		lRet = bind( m_sockTCP.m_sock, (const sockaddr *)&addrTCP, sizeof(addrTCP)); 
		
		if ( 0 == lRet )
		{
			lRet = listen( m_sockTCP.m_sock, 5 );
			TRACEF (_T ("listen: ") + m_sockTCP.ToString ());
			bResult = true;
		}
	}
	
	if ((m_sockUDP.m_sock = socket (AF_INET, SOCK_DGRAM, 0)) != INVALID_SOCKET) {
		int iOption = 1;					// set value to turn options on
		long lRet;

		memset (&addrTCP, 0, sizeof (addrTCP));
		addrTCP.sin_family		= AF_INET;
		addrTCP.sin_addr.s_addr	= htonl(INADDR_ANY);
		addrTCP.sin_port		= htons (_FJ_SOCKET_NUMBER);

		lRet = setsockopt (m_sockUDP, SOL_SOCKET, SO_KEEPALIVE, (char*)&iOption, sizeof(iOption));
		lRet = setsockopt (m_sockUDP, SOL_SOCKET, SO_REUSEADDR, (char*)&iOption, sizeof(iOption));
		//lRet = setsockopt (m_sockUDP, SOL_SOCKET, SO_NBIO,    (char*)&iOption, sizeof(iOption));

		if (!lRet) {
			lRet = bind (m_sockUDP.m_sock, (const sockaddr *)&addrTCP, sizeof(addrTCP));
			TRACEF (_T ("listen: ") + m_sockUDP.ToString ());
		}
	}

	if (!bResult) {
		DWORD dw;
		CString * pstr = new CString ();

		Close (m_sockTCP.m_sock);
		Close (m_sockUDP.m_sock);

		pstr->Format (LoadString (IDS_OPENSOCKETFAILED), GetPort (), CUdpThread::GetError (::WSAGetLastError ()));
		
		TRACEF (* pstr);
		::SendMessageTimeout (HWND_BROADCAST, WM_DISPLAY_MSG, (WPARAM)pstr, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 100, &dw);
	}

	return bResult;
}

ULONG CALLBACK CNextThread::SocketHandlerFct (LPVOID lpData)
{
	ASSERT (lpData);
	CSocketHandler & s = * (CSocketHandler *)lpData;
	WSADATA wsaData;
	SOCKADDR_IN sin;
	int nLen = sizeof (sin);
	const SOCKET sock = s.m_pParentThread->m_sockTCP.m_sock;
	const HANDLE hParentThread = s.m_pParentThread->m_hThread;
	const DWORD dwParentThreadID = s.m_pParentThread->m_nThreadID;

	WORD wVersionRequested = MAKEWORD (1, 1);

	VERIFY (::WSAStartup (wVersionRequested, &wsaData) == 0);
		
	#ifdef _DEBUG
	{
		CString str;

		str.Format (_T ("SocketHandlerFct: 0x%p"), ::GetCurrentThreadId ());
		TRACEF (str);
	}
	#endif

	SOCKET sockAccept = ::accept (sock, (struct sockaddr *)&sin, &nLen);
	DWORD dwExitCode = 0;

	if (sockAccept == INVALID_SOCKET) 
		return 0;

	::GetExitCodeThread (hParentThread, &dwExitCode);

	if (dwExitCode != STILL_ACTIVE) 
		return 0;

	if (sockAccept == INVALID_SOCKET) 
		TRACEF (CUdpThread::GetError (::WSAGetLastError ()));
	else {
		TRACER (_T ("SocketHandlerFct 'accept'ed"));

		s.m_sock = sockAccept;
		s.m_strAddrFrom = a2w (inet_ntoa (sin.sin_addr)); // GetLocalAddress (s.m_sock);

		TRACEF (GetLocalAddress (s.m_pParentThread->m_sockTCP.m_sock));
		TRACEF (GetLocalAddress (s.m_sock));
		TRACEF (inet_ntoa (sin.sin_addr));
		
		#ifdef _DEBUG
		CString str;

		str.Format (_T ("%s SocketHandlerFct: 0x%p, accept: %s, %d"),
			s.m_strAddrFrom,
			::GetCurrentThreadId (),
			s.m_pParentThread->GetSockType () == SOCK_STREAM ? _T ("SOCK_STREAM") : _T ("SOCK_DGRAM"),
			s.m_pParentThread->GetPort ());
		TRACEF (str);
		int nBreak = 0;
		#endif
	}

	TRACER (_T ("SocketHandlerFct exiting"));
	s.m_hThread = NULL;
	return 0;
}

void CNextThread::ProcessData ()
{
	if (m_sockTCP.m_sock == INVALID_SOCKET) 
		return;

	for (int i = 0; i < ARRAYSIZE (m_sockClients); i++) {
		CSocketHandler & s = m_sockClients [i];

		if (s.m_sock != INVALID_SOCKET) {
			timeval timeout = { 0, 100 };
			fd_set readfds;

			readfds.fd_count = 1;
			readfds.fd_array [0] = s.m_sock;

			if (::select (0, &readfds, NULL, NULL, &timeout) > 0) {
				_FJ_SOCKET_MESSAGE_HEADER header;
				ZeroMemory (&header, sizeof (header));
				int nLen = sizeof (_FJ_SOCKET_MESSAGE_HEADER);

				int lBytesRead = ::recv (s.m_sock, (char *)&header, nLen, 0);

				switch (lBytesRead) {
				//case 0:				// closed gracefully
				//	Close (s);
				//	break;
				case SOCKET_ERROR:	// lost connection
					Close (s.m_sock);
					TRACER (CUdpThread::GetError (::WSAGetLastError ()));
					break;
				default:			// recieved data
					::Sleep (1);

					if (lBytesRead == sizeof (_FJ_SOCKET_MESSAGE_HEADER)) {
						header.Cmd		= ntohl (header.Cmd);
						header.BatchID	= ntohl (header.BatchID);
						header.Length	= ntohl (header.Length);
						
						int nBufferSize = sizeof (_FJ_SOCKET_MESSAGE_HEADER) + max (_FJ_SOCKET_BUFFER_SIZE, header.Length);
						BYTE * pBuffer = new BYTE [nBufferSize];
						ZeroMemory (pBuffer, nBufferSize);

						_FJ_SOCKET_MESSAGE & msg = * (_FJ_SOCKET_MESSAGE *)pBuffer;
						msg.Header.Cmd		= header.Cmd;
						msg.Header.BatchID	= header.BatchID;
						msg.Header.Length	= header.Length;
					
						TRACER (fj_socketCommandString (header.Cmd));
						ULONG lRead = Process_FJ_SOCKET_MESSAGE (s, &msg);

						{
							CMainFrame * pFrame = (CMainFrame *)theApp.m_pMainWnd;
							CView * pView = pFrame->m_pControlView;
							CString str = s.ToString () + _T (" (revc)             ") + GetTrace (&msg, header.Length, _T (__FILE__), __LINE__);
						
							TRACER (str);

							while (!::PostMessage (pView->GetSafeHwnd(), WM_DIAGNOSTIC_DATA, (WPARAM)new CString (str), 0))
								::Sleep (0);
						}

						ProcessCmd (s, msg);
						delete [] pBuffer;
					}
					break;
				}
			}

			if (!GetSTATUS_MODE ().CompareNoCase (_T ("ON"))) {
				CTimeSpan tm = CTime::GetCurrentTime () - s.m_tmLastStatus;

				if (tm.GetTotalSeconds () >= 5 || m_bSendStatusNow) {
					fj_print_head_SetStatus ();
					fj_print_head_SendInfoAll (s);
					s.m_tmLastStatus = CTime::GetCurrentTime ();
					m_bSendStatusNow = false;
				}
			}
		}
		else {
			if (!s.m_hThread) {
				DWORD dwThreadID = 0;

				s.m_pParentThread = this;
				s.m_hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)SocketHandlerFct, (LPVOID)&m_sockClients [i], 0, &dwThreadID);
				ASSERT (s.m_hThread);
			}
		}
	}

	if (m_sockUDP.m_sock != INVALID_SOCKET) {
		timeval timeout = { 0, 100 };
		fd_set readfds;

		readfds.fd_count = 1;
		readfds.fd_array [0] = m_sockUDP.m_sock;

		if (::select (0, &readfds, NULL, NULL, &timeout) > 0) {
			BYTE buffer [sizeof (_FJ_SOCKET_MESSAGE)] = { 0 };
			SOCKADDR_IN addr;
			int n = ARRAYSIZE (buffer);

			int lBytesRead = ::recvfrom (m_sockUDP, (char *)buffer, n, 0, (SOCKADDR *)&addr, &n);

			if (lBytesRead == sizeof(_FJ_SOCKET_MESSAGE_HEADER)) {
				_FJ_SOCKET_MESSAGE * psmsg = (_FJ_SOCKET_MESSAGE *)buffer;

				psmsg->Header.Cmd		= ntohl (psmsg->Header.Cmd);
				psmsg->Header.BatchID	= ntohl (psmsg->Header.BatchID);
				psmsg->Header.Length	= ntohl (psmsg->Header.Length);

				if (psmsg->Header.Cmd == IPC_GET_GROUP_INFO) {
					CString str;

					str.Format (_T ("GROUP_NAME=%s;GROUP_COUNT=%s;GROUP_IDS=%s;GROUP_IPADDRS=%s;"),
						FoxjetCommon::GetDSN (),
						_T ("1"), 
						FoxjetDatabase::GetComputerName (), 
						FoxjetDatabase::GetIpAddress ());

					strcpy ((CHAR *)(&(psmsg->Data)), w2a (str));
					psmsg->Header.Length = strlen( (CHAR *)(&(psmsg->Data)));
					psmsg->Header.Cmd = IPC_SET_GROUP_INFO;

					ULONG lDataLength = strlen ((LPSTR)psmsg->Data);
					psmsg->Header.Length = lDataLength;
		
					n = psmsg->Header.Length + sizeof(_FJ_SOCKET_MESSAGE_HEADER);

					psmsg->Header.Cmd		= htonl (psmsg->Header.Cmd);
					psmsg->Header.BatchID	= htonl (psmsg->Header.BatchID);
					psmsg->Header.Length	= htonl (psmsg->Header.Length);

					int nSend = ::sendto (m_sockUDP, (CHAR *)psmsg, n, 0, (SOCKADDR *)&addr, sizeof (addr));
					TRACER (FoxjetDatabase::ToString (nSend) + _T (": ") + str);
				}
			}
		}
	}
}

static CString StrobeStateToString (FoxjetDatabase::STROBESTATE s)
{
	switch (s) {
	case FoxjetDatabase::STROBESTATE_OFF:	return _T ("OFF");
	case FoxjetDatabase::STROBESTATE_ON:	return _T ("ON");
	//case FoxjetDatabase::STROBESTATE_FLASH:	return _T ("");
	}

	return _T  ("OFF");
}

static FoxjetDatabase::STROBESTATE StrobeStateFromString (const CString & str)
{
	if (!str.CompareNoCase (_T ("ON"))) 
		return FoxjetDatabase::STROBESTATE_ON;
	
	return FoxjetDatabase::STROBESTATE_OFF;
}

CString CNextThread::GetStrobe (const CString & strNEXTState)
{
	using namespace FoxjetCommon;
	using namespace FoxjetDatabase;

	CString str = strNEXTState;
	static const struct
	{
		CString m_strMATRIX;
		CString m_strNEXT;
	}
	errors [] = 
	{
		{ LoadString (IDS_HEADERROR_READY),			_T ("STROBE_MODE_READY_"),		},
		{ LoadString (IDS_HEADERROR_APSCYCLE),		_T ("STROBE_MODE_APS_"),		},
		{ LoadString (IDS_HEADERROR_LOWTEMP),		_T ("STROBE_MODE_LOWTEMP_"),	},
		{ LoadString (IDS_HEADERROR_HIGHVOLTAGE),	_T ("STROBE_MODE_HV_"),			},
		{ LoadString (IDS_HEADERROR_LOWINK),		_T ("STROBE_MODE_LOWINK_"),		},
		{ LoadString (IDS_HEADERROR_OUTOFINK),		_T ("STROBE_MODE_OUTOFINK_"),	},
	};

	ASSERT (str.GetLength () > 2);
	TCHAR c = str [str.GetLength () - 1];
	str.Delete (str.GetLength () - 1);

	for (int i = 0; i < ARRAYSIZE (errors); i++) {
		if (!str.CompareNoCase (errors [i].m_strNEXT)) {
			for (int j = 0; j < m_vStrobe.GetSize (); j++) {
				CStrobeItem & s = m_vStrobe [j];

				if (!s.m_strName.CompareNoCase (errors [i].m_strMATRIX)) {
					switch (c) {
					case 'R':	case 'r':	return StrobeStateToString (s.m_red);
					case 'Y':	case 'y':	return StrobeStateToString (s.m_yellow);
					case 'G':	case 'g':	return StrobeStateToString (s.m_green);
					}
					break;
				}
			}
		}
	}

	return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, strNEXTState, _T (""));
}

void CNextThread::SaveParams (bool bPostChange)
{
	using namespace FoxjetDatabase;
	using namespace FoxjetIpElements;

	CString strError;
	CArray <CSQL, CSQL &> vSkip;

	m_pView->SendMessage (WM_NEXT_EDIT_SESSION, 1);

	for (int i = 0; i < m_vSQL.GetCount (); i++) {
		CString str = m_vSQL [i], strTmp;

		#ifdef _DEBUG
		CDebug::Trace (m_vSQL [i], true, m_vSQL [i].m_strFile, m_vSQL [i].m_lLine);
		#endif //_DEBUG

		if (!str.CompareNoCase (::lpszFont)) {
			vSkip.Add (m_vSQL [i]);
		}
		else if (!str.CompareNoCase (::lpszBcArray)) {
			char sz [1024] = { 0 };
			SETTINGSSTRUCT s;

			fj_SystemBarCodesToString (sz);
			ULONG lLineID = GetLine ()->m_LineRec.m_lID;
			HEADTYPE type = GetHead ().m_Config.m_HeadSettings.m_nHeadType;
			BCPARAMS params (lLineID, type, a2w (sz));

			VERIFY (m_pView->SendMessage (WM_NEXT_SET_BCPARAMS, (WPARAM)&params, (LPARAM)&strTmp) == 1);
		}
		else if (!str.CompareNoCase (::lpszDataMatrixArray)) {
			char sz [1024] = { 0 };
			SETTINGSSTRUCT s;

			fj_SystemDataMatrixToString (sz);
			ULONG lLineID = GetLine ()->m_LineRec.m_lID;
			HEADTYPE type = GetHead ().m_Config.m_HeadSettings.m_nHeadType;
			BCPARAMS params (lLineID, type, a2w (sz));

			VERIFY (m_pView->SendMessage (WM_NEXT_SET_DMPARAMS, (WPARAM)&params, (LPARAM)&strTmp) == 1);
		}
		else if (!str.CompareNoCase (::lpszTimeSettings)) {
			ULONG lLineID = GetLine ()->m_LineRec.m_lID;
			CStringArray v;
			char sz [1024] = { 0 };

			fj_SystemTimeToString (sz);
			v.Add (ListGlobals::Keys::NEXT::m_strTime);
			v.Add (a2w (sz));
			VERIFY (m_pView->SendMessage (WM_NEXT_SET_SYSTEM_PARAMS, (WPARAM)lLineID, (LPARAM)&v) == 1);
		}
		else if (!str.CompareNoCase (::lpszDateSettings)) {
			ULONG lLineID = GetLine ()->m_LineRec.m_lID;
			CStringArray v;
			char sz [1024] = { 0 };

			fj_SystemDateStringsToString (sz);
			v.Add (ListGlobals::Keys::NEXT::m_strDays);
			v.Add (a2w (sz));
			VERIFY (m_pView->SendMessage (WM_NEXT_SET_SYSTEM_PARAMS, (WPARAM)lLineID, (LPARAM)&v) == 1);
		}
		else if (!str.CompareNoCase (::lpszStrobeSettings)) {
			using namespace FoxjetCommon;

			const struct
			{
				CString m_strMATRIX;
				CString m_strNEXT;
			}
			errors [] = 
			{
				{ LoadString (IDS_HEADERROR_READY),			_T ("STROBE_MODE_READY_"),		},
				{ LoadString (IDS_HEADERROR_APSCYCLE),		_T ("STROBE_MODE_APS_"),		},
				{ LoadString (IDS_HEADERROR_LOWTEMP),		_T ("STROBE_MODE_LOWTEMP_"),	},
				{ LoadString (IDS_HEADERROR_HIGHVOLTAGE),	_T ("STROBE_MODE_HV_"),			},
				{ LoadString (IDS_HEADERROR_LOWINK),		_T ("STROBE_MODE_LOWINK_"),		},
				{ LoadString (IDS_HEADERROR_OUTOFINK),		_T ("STROBE_MODE_OUTOFINK_"),	},
			};
	
			m_vStrobe.RemoveAll ();
			m_pView->SendMessage (WM_NEXT_GET_STROBE,	(WPARAM)&m_vStrobe, 0);

			for (int i = 0; i < m_vStrobe.GetSize (); i++) {
				CStrobeItem & s = m_vStrobe [i];
				
				TRACEF (s.ToString ());

				for (int j = 0; j < ARRAYSIZE (errors); j++) {
					if (!s.m_strName.CompareNoCase (errors [j].m_strMATRIX)) {
						const CString str [] =
						{
							FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, errors [j].m_strNEXT + _T ("G"), StrobeStateToString (s.m_green)),
							FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, errors [j].m_strNEXT + _T ("R"), StrobeStateToString (s.m_red)),
							FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, errors [j].m_strNEXT + _T ("Y"), StrobeStateToString (s.m_yellow)),
						};

						s.m_green	= StrobeStateFromString (str [0]); 
						s.m_red		= StrobeStateFromString (str [1]);
						s.m_yellow	= StrobeStateFromString (str [2]); 
						break;
					}
				}
			}

			if (bPostChange)
				m_pView->SendMessage (WM_NEXT_SET_STROBE,	(WPARAM)&m_vStrobe, 0);
		}
		else if (!str.CompareNoCase (::lpszSerialPort)) {
			PORT_TYPE n = (PORT_TYPE)FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, m_strRegSection, _T ("PORT_TYPE"), VARDATA);

			for (int i = 0; i < ARRAYSIZE (::mapPort); i++) {
				if (!str.CompareNoCase (::mapPort [i].m_lpsz)) {
					n = ::mapPort [i].m_n;
					break;
				}
			}
	
			m_pView->SendMessage (WM_NEXT_SET_SERIAL_PORT, (WPARAM)n, 0);
		}
		else {
			VERIFY (m_pView->SendMessage (WM_SQL, (WPARAM)&str, (LPARAM)&strTmp) == 1);

			if (strTmp.GetLength ())
				strError += strTmp + _T ("\n");
		}
	}

	m_vSQL.RemoveAll ();
	m_vSQL.Append (vSkip);

	if (bPostChange)
		::PostMessage (HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, 0);

	m_pView->PostMessage (WM_NEXT_EDIT_SESSION, 0);

	if (strError.GetLength ()) {
		DWORD dw;
		CString * pstr = new CString (_T ("IPC_SAVE_ALL_PARAMS\n") + strError);

		TRACEF (* pstr);
		::PostMessage (::AfxGetMainWnd ()->m_hWnd, WM_DISPLAY_MSG, (WPARAM)pstr, 0);
	}
}


CString CNextThread::GetUNITS ()
{
	return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("UNITS"), _T ("ENGLISH_INCHES"));
}

void CNextThread::SetUNITS (const CString & str)
{
}



CString CNextThread::GetTIME ()
{
	return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("TIME"), _T ("24"));
}

void CNextThread::SetTIME (const CString & str)
{
}



CString CNextThread::GetENCODER ()
{
	return GetHead ().m_Config.m_HeadSettings.m_nEncoder ? _T ("F") : _T ("T");
}

void CNextThread::SetENCODER (const CString & str)
{
	using namespace FoxjetDatabase;

	CString strSQL;
	Head::CHeadInfo info = GetHead ();
	int n = !str.CompareNoCase (_T ("T")) ? 0 : 1;

	strSQL.Format (_T ("UPDATE [%s] SET [%s]=%d WHERE [%s]='%s';"), 
		Heads::m_lpszTable, 
		Heads::m_lpszExtEnc, n,
		Heads::m_lpszUID, info.GetUID ());
	m_vSQL.Add (CSQL (strSQL, _T (__FILE__), __LINE__));
}



CString CNextThread::GetENCODER_COUNT ()
{
	double d = 0;
	const FoxjetDatabase::HEADSTRUCT & h = GetHead ().m_Config.m_HeadSettings;

	switch (h.m_nEncoderDivisor) {
		case 1:	d = (double)h.m_nHorzRes;		break; // 300
		default:
		case 3:	d = (double)h.m_nHorzRes / 1.5;	break; // 200
		case 2:	d = (double)h.m_nHorzRes / 2.0;	break; // 150
	}

	return FoxjetDatabase::ToString (d);
}

void CNextThread::SetENCODER_COUNT (const CString & str)
{
	using namespace FoxjetDatabase;

	CString strSQL;
	Head::CHeadInfo info = GetHead ();
	const HEADSTRUCT & h = info.m_Config.m_HeadSettings;
	int nPRINT_RESOLUTION = _ttoi (FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("PRINT_RESOLUTION"), _T ("")));
	int nENCODER_COUNT = _ttoi (str);
	double d = (double)nPRINT_RESOLUTION / (double)nENCODER_COUNT;
	int n = 1;

	switch ((int)(d * 10)) {
	default:
	case 10:	n = 1;	break;
	case 15:	n = 3;	break;
	case 20:	n = 2;	break;
	}
	
	strSQL.Format (_T ("UPDATE [%s] SET [%s]=%d WHERE [%s]='%s';"), 
		Heads::m_lpszTable, 
		Heads::m_lpszEncoderDivisor, n,
		Heads::m_lpszUID, info.GetUID ());
	m_vSQL.Add (CSQL (strSQL, _T (__FILE__), __LINE__));
}



CString CNextThread::GetENCODER_WHEEL ()
{
	return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("ENCODER_WHEEL"), _T (""));
	/* 
	CString str;

	str.Format (_T ("%.1f"), (double)GetHead ().m_Config.m_HeadSettings.m_nHorzRes);
	return str;
	*/
}

void CNextThread::SetENCODER_WHEEL (const CString & str)
{ 	
	using namespace FoxjetDatabase;

	/* 
	CString strSQL;
	Head::CHeadInfo info = GetHead ();
	float f = FoxjetDatabase::_ttof (str);

	strSQL.Format (_T ("UPDATE [%s] SET [%s]=%0.1f WHERE [%s]='%s';"), 
		Heads::m_lpszTable, 
		Heads::m_lpszHorzRes, f,
		Heads::m_lpszUID, info.GetUID ());
	m_vSQL.Add (CSQL (strSQL, _T (__FILE__), __LINE__));
	*/

	for (int i = m_vSQL.GetSize () - 1; i >= 0; i--) {
		if (m_vSQL [i].m_str.Find (Heads::m_lpszPhotocellRate) != -1)
			m_vSQL.RemoveAt (i);
	}

	#ifdef _DEBUG
	for (int i = m_vSQL.GetSize () - 1; i >= 0; i--) 
		ASSERT (m_vSQL [i].m_str.Find (Heads::m_lpszPhotocellRate) == -1);
	#endif
	
	SetOP_AUTOPRINT (FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("OP_AUTOPRINT"), _T ("")));

	#ifdef _DEBUG
	for (int i = m_vSQL.GetSize () - 1; i >= 0; i--) {
		if (m_vSQL [i].m_str.Find (Heads::m_lpszPhotocellRate) != -1)
			TRACEF (m_vSQL [i].m_str);
	}

	int nBreak = 0;
	#endif
}



CString CNextThread::GetENCODER_DIVISOR ()
{
	return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("ENCODER_DIVISOR"), _T (""));
	//return FoxjetDatabase::ToString (GetHead ().m_Config.m_HeadSettings.m_nEncoderDivisor);
}

void CNextThread::SetENCODER_DIVISOR (const CString & str)
{ 
/*
	using namespace FoxjetDatabase;

	CString strSQL;
	Head::CHeadInfo info = GetHead ();
	float f = FoxjetDatabase::_ttof (str);

	strSQL.Format (_T ("UPDATE [%s] SET [%s]=%d WHERE [%s]='%s';"), 
		Heads::m_lpszTable, 
		Heads::m_lpszEncoderDivisor, _ttoi (str),
		Heads::m_lpszUID, info.GetUID ());
	m_vSQL.Add (CSQL (strSQL, _T (__FILE__), __LINE__));
*/
}



CString CNextThread::GetCONVEYOR_SPEED ()
{
	float f = (float)GetHead ().m_Config.m_HeadSettings.m_nIntTachSpeed / 5.0;
	return FoxjetDatabase::ToString (f);
}

void CNextThread::SetCONVEYOR_SPEED (const CString & str)
{
	using namespace FoxjetDatabase;

	CString strSQL;
	Head::CHeadInfo info = GetHead ();
	float f = FoxjetDatabase::_ttof (str);

	strSQL.Format (_T ("UPDATE [%s] SET [%s]=%0.1f WHERE [%s]='%s';"), 
		Heads::m_lpszTable, 
		Heads::m_lpszIntTachSpeed, (f * 5.0),
		Heads::m_lpszUID, info.GetUID ());
	m_vSQL.Add (CSQL (strSQL, _T (__FILE__), __LINE__));
}



CString CNextThread::GetPRINT_RESOLUTION ()
{
	Head::CHeadInfo info = GetHead ();
	//float fPrintRes = (((float)info.m_Config.m_HeadSettings.m_nHorzRes * 2.0) / (float)info.m_Config.m_HeadSettings.m_nEncoderDivisor);
	return FoxjetDatabase::ToString ((float)info.m_Config.m_HeadSettings.m_nHorzRes);
}

void CNextThread::SetPRINT_RESOLUTION (const CString & str)
{
	using namespace FoxjetDatabase;

	CString strSQL;
	Head::CHeadInfo info = GetHead ();
	const HEADSTRUCT & h = info.m_Config.m_HeadSettings;

	strSQL.Format (_T ("UPDATE [%s] SET [%s]=%d WHERE [%s]='%s';"), 
		Heads::m_lpszTable, 
		Heads::m_lpszHorzRes, _ttoi (str),
		Heads::m_lpszUID, info.GetUID ());
	m_vSQL.Add (CSQL (strSQL, _T (__FILE__), __LINE__));
}



CString CNextThread::GetINK_SIZE ()
{
	return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("INK_SIZE"), _T ("500.00"));
}

void CNextThread::SetINK_SIZE (const CString & str)
{
}



CString CNextThread::GetINK_COST ()
{
	return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("INK_COST"), _T ("230.00"));
}

void CNextThread::SetINK_COST (const CString & str)
{
}


CString CNextThread::GetSYSTEM_TIME ()
{
	return CTime::GetCurrentTime ().Format (::lpszNEXTTimeFormat);
}

void CNextThread::SetSYSTEM_TIME (const CString & str)
{
	try {
		COleDateTime dt;
		SYSTEMTIME t = { 0 };

		dt.ParseDateTime (str); // 2016/05/05 17:50:46
		//TRACEF (str);
		//TRACEF (dt.Format (::lpszNEXTTimeFormat));
		
		#ifndef _DEBUG
		if (dt.GetAsSystemTime (t)) {
			VERIFY (FoxjetDatabase::EnablePrivilege (SE_SYSTEMTIME_NAME));
			::SetLocalTime (&t);
		}
		#endif
	}
	catch (COleException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
}



void CNextThread::SetHeadType (const CString & str)
{
	using namespace FoxjetDatabase;

	HEADTYPE type = GRAPHICS_768_256;
	CString strSQL;

	switch (_ttoi (str)) {
		case 0: type = UJ2_352_32;				break;
		case 1: type = UJ2_192_32;				break;		
		case 2: type = UJ2_96_32;				break;
		default:
		case 3: type = GRAPHICS_768_256;		break;
		case 4: type = GRAPHICS_384_128;		break;
		case 5: type = UJ2_192_32NP;			break;
		case 6: type = ALPHA_CODER;			break;
		case 7: type = GRAPHICS_768_256_L330;	break;
		case 8: type = GRAPHICS_768_256_L310;	break;
		case 9: type = GRAPHICS_768_256_0;		break;
	}

	const double dNozzleSpan = Foxjet3d::CHeadDlg::GetNozzleSpan (type);
	const int nChannels = Foxjet3d::CHeadDlg::GetChannels (type);

	Head::CHeadInfo info = GetHead ();

	strSQL.Format (_T ("UPDATE [%s] SET [%s].[%s]=%d, [%s].[%s]=%f, [%s].[%s]=%d WHERE [%s].[%s]='%s';"), 
		Heads::m_lpszTable, 
		Heads::m_lpszTable, Heads::m_lpszHeadType, (int)type,
		Heads::m_lpszTable, Heads::m_lpszNozzleSpan, dNozzleSpan,
		Heads::m_lpszTable, Heads::m_lpszChannels, nChannels,
		Heads::m_lpszTable, Heads::m_lpszUID, info.GetUID ());
	m_vSQL.Add (CSQL (strSQL, _T (__FILE__), __LINE__));
}

CString CNextThread::GetHeadType ()
{
	using namespace FoxjetDatabase;

	Head::CHeadInfo info = GetHead ();

	switch (info.m_Config.m_HeadSettings.m_nHeadType) {
		case UJ2_352_32:			return _T ("0");
		case UJ2_192_32:			return _T ("1");
		case UJ2_96_32:				return _T ("2");
		case GRAPHICS_768_256:		return _T ("3");
		case GRAPHICS_384_128:		return _T ("4");
		case UJ2_192_32NP:			return _T ("5");
		case ALPHA_CODER:			return _T ("6");
		case GRAPHICS_768_256_L330:	return _T ("7");
		case GRAPHICS_768_256_L310:	return _T ("8");
		case GRAPHICS_768_256_0:	return _T ("9");
	}

	return _T ("-1");
}


CString CNextThread::GetBC_N ()
{
	return FoxjetDatabase::ToString (fj_SysBCIndex);
}
void CNextThread::SetBC_N (const CString & str)
{
	fj_SysBCIndex = _ttoi (str);
}


CString CNextThread::GetBC_NAME ()
{
	CString str = a2w (::bcArray [::fj_SysBCIndex].strName);

	if (FoxjetDatabase::Extract (str, _T ("\""), _T ("\"")).IsEmpty ())
		str = _T ("\"") + str + _T ("\"");

	return str;
}
void CNextThread::SetBC_NAME (const CString & str)
{
	strncpy (::bcArray [::fj_SysBCIndex].strName, w2a (str), ARRAYSIZE (bcArray [0].strName));
	MARKSETTINGDIRTY (::lpszBcArray);
}


CString CNextThread::GetBC_TY ()
{
	return FoxjetDatabase::ToString (::bcArray [::fj_SysBCIndex].lBCFlags);
}
void CNextThread::SetBC_TY (const CString & str)
{
	::bcArray [::fj_SysBCIndex].lBCFlags = _ttoi (str);
	MARKSETTINGDIRTY (::lpszBcArray);
}


CString CNextThread::GetBC_H ()
{
	return FoxjetDatabase::ToString (::bcArray [::fj_SysBCIndex].lBCHeight);
}
void CNextThread::SetBC_H (const CString & str)
{
	::bcArray [::fj_SysBCIndex].lBCHeight = _ttoi (str);
	MARKSETTINGDIRTY (::lpszBcArray);
}


CString CNextThread::GetBC_CD ()
{
	return ::bcArray [::fj_SysBCIndex].bCheckDigit ? _T ("T") : _T ("F");
}
void CNextThread::SetBC_CD (const CString & str)
{
	if (str.GetLength ()) {
		::bcArray [::fj_SysBCIndex].bCheckDigit = (str [0] == 'T' ? TRUE : FALSE);
		MARKSETTINGDIRTY (::lpszBcArray);
	}
}


CString CNextThread::GetBC_HB ()
{
	return FoxjetDatabase::ToString (::bcArray [::fj_SysBCIndex].lHor_Bearer);
}
void CNextThread::SetBC_HB (const CString & str)
{
	::bcArray [::fj_SysBCIndex].lHor_Bearer = _ttoi (str);
	MARKSETTINGDIRTY (::lpszBcArray);
}


CString CNextThread::GetBC_VB ()
{
	return FoxjetDatabase::ToString (::bcArray [::fj_SysBCIndex].lVer_Bearer);
}
void CNextThread::SetBC_VB (const CString & str)
{
	::bcArray [::fj_SysBCIndex].lVer_Bearer = _ttoi (str);
	MARKSETTINGDIRTY (::lpszBcArray);
}


CString CNextThread::GetBC_QZ ()
{
	return FoxjetDatabase::ToString (::bcArray [::fj_SysBCIndex].lQuietZone);
}
void CNextThread::SetBC_QZ (const CString & str)
{
	::bcArray [::fj_SysBCIndex].lQuietZone = _ttoi (str);
	MARKSETTINGDIRTY (::lpszBcArray);
}


CString CNextThread::GetBC_BASE ()
{
	return FoxjetDatabase::ToString (::bcArray [::fj_SysBCIndex].lBarVal [0]);
}
void CNextThread::SetBC_BASE (const CString & str)
{
	::bcArray [::fj_SysBCIndex].lBarVal [0] = _ttoi (str);
	MARKSETTINGDIRTY (::lpszBcArray);
}


CString CNextThread::GetBC_B1 ()
{
	return FoxjetDatabase::ToString (::bcArray [::fj_SysBCIndex].lBarVal [1]);
}
void CNextThread::SetBC_B1 (const CString & str)
{
	::bcArray [::fj_SysBCIndex].lBarVal [1] = _ttoi (str);
	MARKSETTINGDIRTY (::lpszBcArray);
}


CString CNextThread::GetBC_B2 ()
{
	return FoxjetDatabase::ToString (::bcArray [::fj_SysBCIndex].lBarVal [2]);
}
void CNextThread::SetBC_B2 (const CString & str)
{
	::bcArray [::fj_SysBCIndex].lBarVal [2] = _ttoi (str);
	MARKSETTINGDIRTY (::lpszBcArray);
}


CString CNextThread::GetBC_B3 ()
{
	return FoxjetDatabase::ToString (::bcArray [::fj_SysBCIndex].lBarVal [3]);
}
void CNextThread::SetBC_B3 (const CString & str)
{
	::bcArray [::fj_SysBCIndex].lBarVal [3] = _ttoi (str);
	MARKSETTINGDIRTY (::lpszBcArray);
}


CString CNextThread::GetBC_B4 ()
{
	return FoxjetDatabase::ToString (::bcArray [::fj_SysBCIndex].lBarVal [4]);
}
void CNextThread::SetBC_B4 (const CString & str)
{
	::bcArray [::fj_SysBCIndex].lBarVal [4] = _ttoi (str);
	MARKSETTINGDIRTY (::lpszBcArray);
}



CString CNextThread::GetDM_N ()
{
	return FoxjetDatabase::ToString (::fj_SysDMIndex);
}
void CNextThread::SetDM_N (const CString & str)
{
	::fj_SysDMIndex = _ttoi (str);
}


CString CNextThread::GetDM_NAME ()
{
	CString str = a2w (::bcDataMatrixArray [::fj_SysDMIndex].strName);

	if (FoxjetDatabase::Extract (str, _T ("\""), _T ("\"")).IsEmpty ())
		str = _T ("\"") + str + _T ("\"");

	return str;
}
void CNextThread::SetDM_NAME (const CString & str)
{
	strcpy (::bcDataMatrixArray [::fj_SysDMIndex].strName, w2a (str));
	MARKSETTINGDIRTY (::lpszDataMatrixArray);
}


CString CNextThread::GetDM_H ()
{
	return FoxjetDatabase::ToString (::bcDataMatrixArray [::fj_SysDMIndex].lHeight);
}
void CNextThread::SetDM_H (const CString & str)
{
	::bcDataMatrixArray [::fj_SysDMIndex].lHeight = _ttoi (str);
	MARKSETTINGDIRTY (::lpszDataMatrixArray);
}


CString CNextThread::GetDM_HB ()
{
	return FoxjetDatabase::ToString (::bcDataMatrixArray [::fj_SysDMIndex].lHeightBold);
}
void CNextThread::SetDM_HB (const CString & str)
{
	::bcDataMatrixArray [::fj_SysDMIndex].lHeightBold = _ttoi (str);
	MARKSETTINGDIRTY (::lpszDataMatrixArray);
}


CString CNextThread::GetDM_W ()
{
	return FoxjetDatabase::ToString (::bcDataMatrixArray [::fj_SysDMIndex].lWidth);
}
void CNextThread::SetDM_W (const CString & str)
{
	::bcDataMatrixArray [::fj_SysDMIndex].lWidth = _ttoi (str);
	MARKSETTINGDIRTY (::lpszDataMatrixArray);
}


CString CNextThread::GetDM_WB ()
{
	return FoxjetDatabase::ToString (::bcDataMatrixArray [::fj_SysDMIndex].lWidthBold);
}
void CNextThread::SetDM_WB (const CString & str)
{
	::bcDataMatrixArray [::fj_SysDMIndex].lWidthBold = _ttoi (str);
	MARKSETTINGDIRTY (::lpszDataMatrixArray);
}

CString CNextThread::GetSTATUS_MODE ()
{
	return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("STATUS_MODE"), _T ("ON"));
}
void CNextThread::SetSTATUS_MODE(const CString & str) { }

CString CNextThread::GetPRINT_MODE ()
{ 
	FoxjetDatabase::TASKSTATE e = GetLine ()->GetTaskState ();
	LPCTSTR lpsz [] = { _T ("DISABLED"), _T ("RUNNING"), _T ("IDLE"), _T ("STOPPED") };
	return lpsz [e];
//	return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("PRINT_MODE"),				_T ("")); 
}             

void CNextThread::SetPRINT_MODE (const CString & str) 
{ 
	CStringArray v;

	if (!str.CompareNoCase (_T ("IDLE")))
		v.Add (BW_HOST_PACKET_IDLE_TASK);
	else if (!str.CompareNoCase (_T ("RESUME"))) 
		v.Add (BW_HOST_PACKET_RESUME_TASK);
	else if (!str.CompareNoCase (_T ("STOP"))) 
		v.Add (BW_HOST_PACKET_STOP_TASK);

	v.Add (GetLine ()->GetName ());
	CServerThread::ProcessCommand (FoxjetDatabase::ToString (v), m_pView);
}



CString CNextThread::GetSTROBE_MODE_READY_G ()		{ return GetStrobe (_T ("STROBE_MODE_READY_G"));	}		
CString CNextThread::GetSTROBE_MODE_READY_Y ()		{ return GetStrobe (_T ("STROBE_MODE_READY_Y"));	}		
CString CNextThread::GetSTROBE_MODE_READY_R ()		{ return GetStrobe (_T ("STROBE_MODE_READY_R"));	}		
CString CNextThread::GetSTROBE_MODE_APS_G ()		{ return GetStrobe (_T ("STROBE_MODE_APS_G"));		}		
CString CNextThread::GetSTROBE_MODE_APS_Y ()		{ return GetStrobe (_T ("STROBE_MODE_APS_Y"));		}		
CString CNextThread::GetSTROBE_MODE_APS_R ()		{ return GetStrobe (_T ("STROBE_MODE_APS_R"));		}		
CString CNextThread::GetSTROBE_MODE_LOWTEMP_G ()	{ return GetStrobe (_T ("STROBE_MODE_LOWTEMP_G"));	}	
CString CNextThread::GetSTROBE_MODE_LOWTEMP_Y ()	{ return GetStrobe (_T ("STROBE_MODE_LOWTEMP_Y"));	}	
CString CNextThread::GetSTROBE_MODE_LOWTEMP_R ()	{ return GetStrobe (_T ("STROBE_MODE_LOWTEMP_R"));	}	
CString CNextThread::GetSTROBE_MODE_HV_G ()			{ return GetStrobe (_T ("STROBE_MODE_HV_G"));		}			
CString CNextThread::GetSTROBE_MODE_HV_Y ()			{ return GetStrobe (_T ("STROBE_MODE_HV_Y"));		}			
CString CNextThread::GetSTROBE_MODE_HV_R ()			{ return GetStrobe (_T ("STROBE_MODE_HV_R"));		}			
CString CNextThread::GetSTROBE_MODE_LOWINK_G ()		{ return GetStrobe (_T ("STROBE_MODE_LOWINK_G"));	}		
CString CNextThread::GetSTROBE_MODE_LOWINK_Y ()		{ return GetStrobe (_T ("STROBE_MODE_LOWINK_Y"));	}			
CString CNextThread::GetSTROBE_MODE_LOWINK_R ()		{ return GetStrobe (_T ("STROBE_MODE_LOWINK_R"));	}		
CString CNextThread::GetSTROBE_MODE_OUTOFINK_G ()	{ return GetStrobe (_T ("STROBE_MODE_OUTOFINK_G")); }
CString CNextThread::GetSTROBE_MODE_OUTOFINK_Y ()	{ return GetStrobe (_T ("STROBE_MODE_OUTOFINK_Y")); }
CString CNextThread::GetSTROBE_MODE_OUTOFINK_R ()	{ return GetStrobe (_T ("STROBE_MODE_OUTOFINK_R")); }
CString CNextThread::GetSTROBE_MODE_DRIVER_G ()		{ return GetStrobe (_T ("STROBE_MODE_DRIVER_G"));	}		
CString CNextThread::GetSTROBE_MODE_DRIVER_Y ()		{ return GetStrobe (_T ("STROBE_MODE_DRIVER_Y"));	}		
CString CNextThread::GetSTROBE_MODE_DRIVER_R ()		{ return GetStrobe (_T ("STROBE_MODE_DRIVER_R"));	}		
CString CNextThread::GetPRINT_DRIVER ()				
{ 
	bool b = false;
	SEND_THREAD_MESSAGE (GetHead (), TM_GET_PRINT_DRIVER, &b);
	return b ? _T ("T") : _T ("F");
}		
CString CNextThread::GetPRINT_DRIVER_COUNT ()		
{ 
	__int64 n = 0;
	SEND_THREAD_MESSAGE (GetHead (), TM_GET_COUNT_UNTIL, &n);
	return FoxjetDatabase::ToString (n);
}		



void CNextThread::SetSTROBE_MODE_READY_G 		(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_READY_Y 		(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_READY_R 		(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_APS_G 			(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_APS_Y 			(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_APS_R 			(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_LOWTEMP_G 		(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_LOWTEMP_Y 		(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_LOWTEMP_R 		(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_HV_G 			(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_HV_Y 			(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_HV_R 			(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_LOWINK_G 		(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_LOWINK_Y 		(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_LOWINK_R 		(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_OUTOFINK_G 	(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_OUTOFINK_Y 	(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_OUTOFINK_R 	(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_DRIVER_G 		(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_DRIVER_Y 		(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetSTROBE_MODE_DRIVER_R 		(const CString & str) { MARKSETTINGDIRTY (::lpszStrobeSettings) }
void CNextThread::SetPRINT_DRIVER				(const CString & str) 
{ 
	bool b = !str.CompareNoCase (_T ("T")) ? true : false;
	SEND_THREAD_MESSAGE (GetHead (), TM_SET_PRINT_DRIVER, b); 
	//if (b) SEND_THREAD_MESSAGE (GetHead (), TM_SET_PRINT_DRIVER_BOLD, 0); 
}		
void CNextThread::SetPRINT_DRIVER_COUNT 		(const CString & str) 
{ 
	__int64 n = _tcstoul (str, NULL, 10); 
	SEND_THREAD_MESSAGE (GetHead (), TM_SET_COUNT_UNTIL, &n); 
}		


CString CNextThread::GetINTERNAL_CLOCK ()	{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("INTERNAL_CLOCK"),	_T (""));	}     
CString CNextThread::GetTIME_ZONE ()		{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("TIME_ZONE"),		_T (""));	}          
CString CNextThread::GetOBSERVE_USA_DST ()	{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("OBSERVE_USA_DST"),	_T (""));	}    
CString CNextThread::GetFIRST_DAY ()		{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("FIRST_DAY"),		_T (""));	}          
CString CNextThread::GetDAYS_WEEK1 ()		{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("DAYS_WEEK1"),		_T (""));	}         
CString CNextThread::GetWEEK53 ()			{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("WEEK53"),			_T (""));	}             
CString CNextThread::GetROLLOVER_HOURS ()	{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("ROLLOVER_HOURS"),	_T (""));	}     
CString CNextThread::GetSHIFT1 ()			{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("SHIFT1"),			_T (""));	}             
CString CNextThread::GetSHIFT2 ()			{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("SHIFT2"),			_T (""));	}             
CString CNextThread::GetSHIFT3 ()			{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("SHIFT3"),			_T (""));	}             
CString CNextThread::GetSHIFTCODE1 ()		{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("SHIFTCODE1"),		_T (""));	}         
CString CNextThread::GetSHIFTCODE2 ()		{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("SHIFTCODE2"),		_T (""));	}         
CString CNextThread::GetSHIFTCODE3 ()		{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("SHIFTCODE3"),		_T (""));	}
CString CNextThread::GetDAYS ()				{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("DAYS"),				_T (""));	}               
CString CNextThread::GetMONTHS ()			{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("MONTHS"),			_T (""));	}             
CString CNextThread::GetDAYS_SPECIAL ()		{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("DAYS_SPECIAL"),		_T (""));	}       
CString CNextThread::GetMONTHS_SPECIAL ()	{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("MONTHS_SPECIAL"),	_T (""));	}     
CString CNextThread::GetHOURS ()			{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("HOURS"),			_T (""));	}              
CString CNextThread::GetAMPM ()				{ return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("AMPM"),				_T (""));	} 

void CNextThread::SetINTERNAL_CLOCK (const CString & str)	{ MARKSETTINGDIRTY (::lpszTimeSettings); }
void CNextThread::SetTIME_ZONE (const CString & str)		{ MARKSETTINGDIRTY (::lpszTimeSettings); }
void CNextThread::SetOBSERVE_USA_DST (const CString & str)	{ MARKSETTINGDIRTY (::lpszTimeSettings); }
void CNextThread::SetFIRST_DAY (const CString & str)		{ MARKSETTINGDIRTY (::lpszTimeSettings); }
void CNextThread::SetDAYS_WEEK1 (const CString & str)		{ MARKSETTINGDIRTY (::lpszTimeSettings); }
void CNextThread::SetWEEK53 (const CString & str)			{ MARKSETTINGDIRTY (::lpszTimeSettings); }
void CNextThread::SetROLLOVER_HOURS (const CString & str)	{ MARKSETTINGDIRTY (::lpszTimeSettings); }
void CNextThread::SetSHIFT1 (const CString & str)			{ MARKSETTINGDIRTY (::lpszTimeSettings); }
void CNextThread::SetSHIFT2 (const CString & str)			{ MARKSETTINGDIRTY (::lpszTimeSettings); }
void CNextThread::SetSHIFT3 (const CString & str)			{ MARKSETTINGDIRTY (::lpszTimeSettings); }
void CNextThread::SetSHIFTCODE1 (const CString & str)		{ MARKSETTINGDIRTY (::lpszTimeSettings); }
void CNextThread::SetSHIFTCODE2 (const CString & str)		{ MARKSETTINGDIRTY (::lpszTimeSettings); }
void CNextThread::SetSHIFTCODE3 (const CString & str)		{ MARKSETTINGDIRTY (::lpszTimeSettings); }

void CNextThread::SetDAYS (const CString & str)				{ MARKSETTINGDIRTY (::lpszDateSettings); }
void CNextThread::SetMONTHS (const CString & str)			{ MARKSETTINGDIRTY (::lpszDateSettings); }
void CNextThread::SetDAYS_SPECIAL (const CString & str)		{ MARKSETTINGDIRTY (::lpszDateSettings); }
void CNextThread::SetMONTHS_SPECIAL (const CString & str)	{ MARKSETTINGDIRTY (::lpszDateSettings); }
void CNextThread::SetHOURS (const CString & str)			{ MARKSETTINGDIRTY (::lpszDateSettings); }
void CNextThread::SetAMPM (const CString & str)				{ MARKSETTINGDIRTY (::lpszDateSettings); }


CString CNextThread::GetDIRECTION () { return GetHead ().m_Config.m_HeadSettings.m_nDirection ? _T ("LEFT_TO_RIGHT") : _T ("RIGHT_TO_LEFT"); }
void CNextThread::SetDIRECTION (const CString & str) 
{ 
	using namespace FoxjetDatabase;

	CString strSQL;
	Head::CHeadInfo info = GetHead ();
	int n = !str.CompareNoCase (_T ("LEFT_TO_RIGHT")) ? FoxjetDatabase::LTOR : FoxjetDatabase::RTOL;

	strSQL.Format (_T ("UPDATE [%s] SET [%s]=%d WHERE [%s]='%s';"), 
		Heads::m_lpszTable, 
		Heads::m_lpszDirection, n,
		Heads::m_lpszUID, info.GetUID ());
	m_vSQL.Add (CSQL (strSQL, _T (__FILE__), __LINE__));
}


CString CNextThread::GetSLANTANGLE () { return FoxjetDatabase::ToString (GetHead ().m_Config.m_HeadSettings.m_dHeadAngle); }
void CNextThread::SetSLANTANGLE (const CString & str) 
{
	using namespace FoxjetDatabase;

	CString strSQL;
	Head::CHeadInfo info = GetHead ();
	double d = FoxjetDatabase::_ttof (str);

	strSQL.Format (_T ("UPDATE [%s] SET [%s]=%f WHERE [%s]='%s';"), 
		Heads::m_lpszTable, 
		Heads::m_lpszHeadAngle, d,
		Heads::m_lpszUID, info.GetUID ());
	m_vSQL.Add (CSQL (strSQL, _T (__FILE__), __LINE__));
}


/*
static std::vector <double> GetAngles (int nHorzRes, int nEncoderDivisor)
{
	std::vector <double> v;
	double x = 0.0;
	double dRes = nHorzRes / nEncoderDivisor;
	double dNFactor = 0;
	bool bMore = true;
	const double dChannelSeparation = 0.05848; // Trident Heads

	while (bMore) {
		x = dNFactor / (dRes * dChannelSeparation);
				
		double dAngle = RAD2DEG (acos (x));
				
		if (bMore = (x >= 0.0) && (x < 0.97)) {
			if (dAngle <= 90.0) 
				v.push_back (dAngle);
		}
				
		dNFactor++;
	}

	return v;
}

static double SelectClosestAngle (const std::vector <double> & v, double dAngle) 
{
	const int nAngle = (int)(dAngle * 10.0);
	int nMinDiff = 0;
	int nIndex = 0;

	for (int i = 0; i < v.size (); i++) {
		int n = (int)(v [i] * 10);
		int nDiff = abs (nAngle - n);

		if (i == 0) 
			nMinDiff = nDiff;

		if (nDiff < nMinDiff) {
			nMinDiff = nDiff;
			nIndex = i;
		}
	}

	return v [nIndex];
}
*/

CString CNextThread::GetSLANTVALUE () { return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("SLANTVALUE"),	_T ("0")); /*  return FoxjetDatabase::ToString (GetHead ().m_Config.GetNFactor ()); */ }
void CNextThread::SetSLANTVALUE (const CString & str) 
{ 
	/* MATRIX caculates n-factor internally 
	int n = _ttoi (str);
	Head::CHeadInfo info = GetHead ();
	std::vector <double> v = GetAngles (info.m_Config.m_HeadSettings.m_nHorzRes, info.m_Config.m_HeadSettings.m_nEncoderDivisor);
	double dAngle = SelectClosestAngle (v, info.m_Config.m_HeadSettings.m_dHeadAngle);
	*/
}

CString CNextThread::GetHEIGHT () { return FoxjetDatabase::ToString ((double)GetHead ().m_Config.m_HeadSettings.m_lRelativeHeight / 1000.0);	}
void CNextThread::SetHEIGHT (const CString & str) 
{
	using namespace FoxjetDatabase;

	CString strSQL;
	Head::CHeadInfo info = GetHead ();
	int n = (int)(FoxjetDatabase::_ttof (str) * 1000.0);

	strSQL.Format (_T ("UPDATE [%s] SET [%s]=%d WHERE [%s]='%s';"), 
		Heads::m_lpszTable, 
		Heads::m_lpszRelativeHeight, n,
		Heads::m_lpszUID, info.GetUID ());
	m_vSQL.Add (CSQL (strSQL, _T (__FILE__), __LINE__));
}

CString CNextThread::GetDISTANCE () { return FoxjetDatabase::ToString ((double)GetHead ().m_Config.m_HeadSettings.m_lPhotocellDelay / 1000.0); }
void CNextThread::SetDISTANCE (const CString & str) 
{
	using namespace FoxjetDatabase;

	CString strSQL;
	Head::CHeadInfo info = GetHead ();
	int n = (int)(FoxjetDatabase::_ttof (str) * 1000.0);

	strSQL.Format (_T ("UPDATE [%s] SET [%s]=%d WHERE [%s]='%s';"), 
		Heads::m_lpszTable, 
		Heads::m_lpszPhotocellDelay, n,
		Heads::m_lpszUID, info.GetUID ());
	m_vSQL.Add (CSQL (strSQL, _T (__FILE__), __LINE__));
}

CString CNextThread::GetBAUD_RATE () { return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("BAUD_RATE"), _T ("9600"));	}
void CNextThread::SetBAUD_RATE (const CString & str) { MARKSETTINGDIRTY (::lpszSerialPort); }

CString CNextThread::GetSCANNER_USE_ONCE () { return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("SCANNER_USE_ONCE"),	_T (""));	}
void CNextThread::SetSCANNER_USE_ONCE (const CString & str) { MARKSETTINGDIRTY (::lpszSerialPort); }

CString CNextThread::GetSERIAL_PORT () 
{
	PORT_TYPE n = (PORT_TYPE)FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, m_strRegSection, _T ("PORT_TYPE"), VARDATA);

	for (int i = 0; i < ARRAYSIZE (::mapPort); i++) 
		if (n == ::mapPort [i].m_n)
			return ::mapPort [i].m_lpsz;

	return ::mapPort [0].m_lpsz;
}
void CNextThread::SetSERIAL_PORT (const CString & str) 
{ 
	MARKSETTINGDIRTY (::lpszSerialPort); 

	for (int i = 0; i < ARRAYSIZE (::mapPort); i++) {
		if (!str.CompareNoCase (::mapPort [i].m_lpsz)) {
			FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, m_strRegSection, _T ("PORT_TYPE"), ::mapPort [i].m_n);
			break;
		}
	}
}

void CNextThread::OnTaskStart (WPARAM wParam, LPARAM lParam)
{
	if (CString * p = (CString *)wParam) {
		CStringArray v;

		v.Add (BW_HOST_PACKET_START_TASK);
		v.Add (GetLine ()->GetName ());
		v.Add (* p);
		CServerThread::ProcessCommand (FoxjetDatabase::ToString (v), m_pView);

		if (lParam) {
			Head::CHeadInfo info = GetHead ();
			SEND_THREAD_MESSAGE (info, TM_SET_SERIAL_START, true);
		}

		delete p;
	}
}


void CNextThread::OnSendStatus (WPARAM wParam, LPARAM lParam)
{
	m_bSendStatusNow = true;
}

CString CNextThread::GetINFO_STATUS () { return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("INFO_STATUS"),		_T (""));	}
void CNextThread::SetINFO_STATUS (const CString & str) { /* TODO */ }

CString CNextThread::GetINFO_SOCKET () { return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("INFO_SOCKET"),		_T (""));	}
void CNextThread::SetINFO_SOCKET (const CString & str) { /* TODO */ }

CString CNextThread::GetPHOTOCELL_BACK () { return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("PHOTOCELL_BACK"), 	_T ("")); } //return GetHead ().m_Config.m_HeadSettings.m_nPhotocell == FoxjetDatabase::INTERNAL_PC ? _T ("T") : _T ("F"); }
void CNextThread::SetPHOTOCELL_BACK (const CString & str) 
{
	//using namespace FoxjetDatabase;

	//CString strSQL;
	//Head::CHeadInfo info = GetHead ();
	//PHOTOCELLS n = !str.CompareNoCase (_T ("T")) ? FoxjetDatabase::INTERNAL_PC : FoxjetDatabase::EXTERNAL_PC;

	//strSQL.Format (_T ("UPDATE [%s] SET [%s]=%d WHERE [%s]='%s';"), 
	//	Heads::m_lpszTable, 
	//	Heads::m_lpszPhotocell, n,
	//	Heads::m_lpszUID, info.GetUID ());
	//m_vSQL.Add (CSQL (strSQL, _T (__FILE__), __LINE__));
}

static const LPCTSTR lpszPcRepeater = _T ("PC repeater, %d");
CString CNextThread::GetPHOTOCELL_INTERNAL () 
{
	return GetHead ().m_Config.m_HeadSettings.m_nPhotocell == FoxjetDatabase::INTERNAL_PC ? _T ("T") : _T ("F"); 
	//CString str;
	//ULONG lLineID = GetLine ()->GetID ();

	//str.Format (lpszPcRepeater, GetHead ().m_Config.m_HeadSettings.m_lID);
	//m_pView->SendMessage (WM_NEXT_GET_SYSTEM_PARAMS, (WPARAM)lLineID, (LPARAM)&str);

	//return _ttoi (str) ? _T ("T") : _T ("F");
}
void CNextThread::SetPHOTOCELL_INTERNAL	(const CString & str) 
{
	using namespace FoxjetDatabase;

	CString strSQL;
	Head::CHeadInfo info = GetHead ();
	PHOTOCELLS n = !str.CompareNoCase (_T ("T")) ? FoxjetDatabase::INTERNAL_PC : FoxjetDatabase::EXTERNAL_PC;

	strSQL.Format (_T ("UPDATE [%s] SET [%s]=%d WHERE [%s]='%s';"), 
		Heads::m_lpszTable, 
		Heads::m_lpszPhotocell, n,
		Heads::m_lpszUID, info.GetUID ());
	m_vSQL.Add (CSQL (strSQL, _T (__FILE__), __LINE__));
	//ULONG lLineID = GetLine ()->GetID ();
	//CStringArray v;
	//CString strData;

	//strData.Format (lpszPcRepeater, GetHead ().m_Config.m_HeadSettings.m_lID);
	//v.Add (strData);
	//v.Add (!str.CompareNoCase (_T ("T")) ? _T ("1") : _T ("0"));
	//VERIFY (m_pView->SendMessage (WM_NEXT_SET_SYSTEM_PARAMS, (WPARAM)lLineID, (LPARAM)&v) == 1);
}

CString CNextThread::GetSTANDBY_AUTO () { return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("STANDBY_AUTO"), _T ("")); }
void CNextThread::SetSTANDBY_AUTO (const CString & str) { }

CString CNextThread::GetAPS_MODE () { return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("APS_MODE"),			_T (""));	}          	  
void CNextThread::SetAPS_MODE (const CString & str) { }

CString CNextThread::GetOP_AUTOPRINT () 
{
	using namespace FoxjetDatabase;

	HEADSTRUCT h = GetHead ().m_Config.m_HeadSettings;
	long lAutoPrint = (long)(h.GetRes () * (float)h.m_dPhotocellRate);

	int nEncoderDivisor = _ttoi (FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("ENCODER_DIVISOR"), _T ("")));
	float fEncoderWheel = FoxjetDatabase::_ttof (FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("ENCODER_WHEEL"), _T ("")));

	if (nEncoderDivisor == 3)
		lAutoPrint *= 1.5;

	if (nEncoderDivisor != 2)
		lAutoPrint /= 2.0;	
	
	return FoxjetDatabase::ToString (lAutoPrint);
}
void CNextThread::SetOP_AUTOPRINT (const CString & str) 
{
	using namespace FoxjetDatabase;

	CString strSQL;
	long lAutoPrint = _ttoi (str);
	Head::CHeadInfo info = GetHead ();
	HEADSTRUCT h = info.m_Config.m_HeadSettings;
	//float fEncoderWheel = (float)h.m_nHorzRes;
	//int nEncoderDivisor = h.m_nEncoderDivisor;
	//float fPrintRes = (float)((fEncoderWheel * 2.0) / (float)nEncoderDivisor);
	double d = (double)lAutoPrint / h.GetRes (); //fPrintRes;

	{
		int nEncoderDivisor = _ttoi (FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("ENCODER_DIVISOR"), _T ("")));
		float fEncoderWheel = FoxjetDatabase::_ttof (FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("ENCODER_WHEEL"), _T ("")));
		// float fPrintRes = (float)((fEncoderWheel * 2.0) / (float)nEncoderDivisor);
		// long lAutoPrint = (long)(fPrintRes * (float)h.m_dPhotocellRate);
		//d = (double)lAutoPrint / (double)fPrintRes;


		h.m_nEncoderDivisor = nEncoderDivisor;
		h.m_nHorzRes = (int)fEncoderWheel;

		TRACEF (FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("ENCODER_DIVISOR"), _T ("")));
		TRACEF (FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("ENCODER_WHEEL"), _T ("")));
		TRACEF (ToString (h.GetRes ()));

		d = ((double)lAutoPrint / h.GetRes ());

		if (nEncoderDivisor == 3)
			d /= 1.5;

		if (nEncoderDivisor != 2)
			d *= 2.0;
	}

	//TRACEF (ToString (lAutoPrint));
	//TRACEF (ToString (h.GetRes ()));

	d = BindTo (d, 1.0, 144.0);

	strSQL.Format (_T ("UPDATE [%s] SET [%s]=%f WHERE [%s]='%s';"), 
		Heads::m_lpszTable, 
		Heads::m_lpszPhotocellRate, d,
		Heads::m_lpszUID, info.GetUID ());
	m_vSQL.Add (CSQL (strSQL, _T (__FILE__), __LINE__));
}

CString CNextThread::GetIDLE_AFTER_PRINT () { return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("IDLE_AFTER_PRINT"),	_T ("")); }
void CNextThread::SetIDLE_AFTER_PRINT (const CString & str) { }

CString CNextThread::GetSAVE_DYN_DATA () { return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("SAVE_DYN_DATA"), _T ("")); }
void CNextThread::SetSAVE_DYN_DATA (const CString & str) { }

CString CNextThread::GetSAVE_SERIAL_DATA () { return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("SAVE_SERIAL_DATA"), _T ("")); }
void CNextThread::SetSAVE_SERIAL_DATA (const CString & str) { }
