#if !defined(AFX_COUNT0828DLG_H__9F9EA461_7248_4468_8AF5_4D3945C03E14__INCLUDED_)
#define AFX_COUNT0828DLG_H__9F9EA461_7248_4468_8AF5_4D3945C03E14__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Count0828Dlg.h : header file
//

#include "ProdLine.h"
#include "RoundButtons.h"
#include "Utils.h"

#if __CUSTOM__ == __SW0828__

/////////////////////////////////////////////////////////////////////////////
// CCount0828Dlg dialog

class CCount0828Dlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CCount0828Dlg(CProdLine & line, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCount0828Dlg)

	ULONG	m_lBoxStart;
	ULONG	m_lBoxRollover;
	ULONG	m_lBoxIncrement;
	ULONG	m_lBoxCurrent;

	ULONG	m_lPalletStart;
	ULONG	m_lPalletCurrent;
	ULONG	m_lPalletRollover;
	ULONG	m_lPalletIncrement;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCount0828Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	FoxjetUtils::CRoundButtons m_buttons;
	CProdLine & m_line;

	// Generated message map functions
	//{{AFX_MSG(CCount0828Dlg)
	afx_msg void OnReset();
	afx_msg void OnChangeBoxcurrent();
	afx_msg void OnChangeBoxrollover();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif //__CUSTOM__ == __SW0828__

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COUNT0828DLG_H__9F9EA461_7248_4468_8AF5_4D3945C03E14__INCLUDED_)
