// UserElementDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "control.h"
#include "UserElementDlg.h"
#include "Debug.h"
#include "AppVer.h"
#include "DbFieldDlg.h"
#include "Extern.h"
#include "DbFieldDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetUtils;

#ifdef __WINPRINTER__
	#include "DatabaseElement.h"
	#include "BitmapElement.h"
	#include "UserElement.h"

	using namespace FoxjetElements;
#endif //__WINPRINTER__

#ifdef __IPPRINTER__
	#include "DynamicTableDlg.h"
	#include "DynamicTextElement.h"
	#include "DynamicBarcodeElement.h"

	using namespace FoxjetIpElements;
#endif //__IPPRINTER__

using namespace ItiLibrary;
using namespace FoxjetCommon;
using CUserElementDlg::CUserItem;

CUserItem::CUserItem (FoxjetCommon::CBaseElement * pElement, FoxjetDatabase::HEADSTRUCT * pHead,
					  const CString & strPrompt, const CString & strData)
:	m_pElement (pElement),
	m_pHead (pHead),
	m_strPrompt (strPrompt),
	m_strData (strData)
{
	if (m_pElement) {
		if (!m_strPrompt.GetLength ()) {
			#ifdef __WINPRINTER__
				if (CUserElement * p = DYNAMIC_DOWNCAST (CUserElement, m_pElement))
					m_strPrompt = p->GetPrompt ();
				else if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, m_pElement))
					m_strPrompt = p->GetPrompt ();
				else if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, m_pElement)) //#if __CUSTOM__ == __SW0833__
					m_strPrompt = p->GetFilePath ();
			#endif //__WINPRINTER__
		}
		
		#ifdef __WINPRINTER__
			if (CUserElement * p = DYNAMIC_DOWNCAST (CUserElement, pElement))
				m_strData = p->GetDefaultData ();
			else if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, pElement))
				m_strData = p->GetKeyValue ();
			else if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, m_pElement))//#if __CUSTOM__ == __SW0833__
				m_strData = p->GetFilePath ();
		#endif

		if (!m_strData.GetLength ())
			m_pElement->GetDefaultData ();
	}
}


CUserItem::CUserItem (const CUserItem & rhs)
:	m_pElement (rhs.m_pElement),
	m_pHead (rhs.m_pHead),
	m_strPrompt (rhs.m_strPrompt),
	m_strData (rhs.m_strData)
{
}

CUserItem & CUserItem::operator = (const CUserItem & rhs)
{
	if (this != &rhs) {
		m_pElement		= rhs.m_pElement;
		m_pHead			= rhs.m_pHead;
		m_strPrompt		= rhs.m_strPrompt;
		m_strData	= rhs.m_strData;
	}

	return * this;
}

CUserItem::~CUserItem ()
{
}

CString CUserItem::GetDispText (int nColumn) const
{
	if (m_pHead && m_pElement) {
		switch (nColumn) {
		case 0:		return m_pHead->m_strName;
		case 1:		return m_strPrompt;
		case 2:		return m_strData;
		}
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CUserElementDlg dialog


CUserElementDlg::CUserElementDlg(ULONG lLineID, CWnd* pParent /*=NULL*/)
:	m_lLineID (lLineID),
	FoxjetCommon::CEliteDlg(CUserElementDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUserElementDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CUserElementDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserElementDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUserElementDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CUserElementDlg)
	ON_NOTIFY(LVN_ITEMCHANGED, LV_ELEMENTS, OnItemchangedElements)
	ON_BN_CLICKED(BTN_SELECT, OnSelect)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserElementDlg message handlers

BOOL CUserElementDlg::OnInitDialog() 
{	
	using CListCtrlImp::CColumn;

	CArray <CColumn, CColumn> vCols;

	FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_HEAD)));
	vCols.Add (CColumn (LoadString (IDS_PROMPT)));
	vCols.Add (CColumn (LoadString (IDS_DATA)));

	m_lv.Create (LV_ELEMENTS, vCols, this, _T ("Software\\FoxJet\\Control"));

	for (int i = 0; i < m_vElements.GetSize (); i++)
		m_lv.InsertCtrlData (new CUserItem (m_vElements [i]));

	if (m_lv->GetItemCount ())
		m_lv->SetItemState (0, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CUserElementDlg::OnItemchangedElements(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		CUserItem * p = (CUserItem *)m_lv.GetCtrlData (sel [0]);
		bool bDB = false;

		SetDlgItemText (LBL_PROMPT, p->m_strPrompt);
		SetDlgItemText (TXT_DATA, p->m_strData);

		#ifdef __WINPRINTER__
			if (CDatabaseElement * pDatabase = DYNAMIC_DOWNCAST (CDatabaseElement, p->m_pElement)) 
				bDB = true;
			if (CBitmapElement * pBitmap = DYNAMIC_DOWNCAST (CBitmapElement, p->m_pElement)) //#if __CUSTOM__ == __SW0833__
				bDB = true;
		#else
			LONG lDynID = 0;

			if (CDynamicTextElement * pText = DYNAMIC_DOWNCAST (CDynamicTextElement, p->m_pElement)) 
				lDynID = pText->GetDynamicID ();
			if (CDynamicBarcodeElement * pBarcode = DYNAMIC_DOWNCAST (CDynamicBarcodeElement, p->m_pElement))
				lDynID = pBarcode->GetDynamicID ();

			if (lDynID) {
				using CDynamicTableDlg::CItemArray;
				using CDynamicTableDlg::CTableItem;

				CItemArray vItems;

				CDynamicTableDlg::GetDynamicData (m_lLineID, vItems);

				for (int i = 0; i < vItems.GetSize (); i++) {
					CTableItem item = vItems [i];
					
					if (item.m_lID == lDynID) {
						bDB = item.m_bUseDatabase;
						break;
					}
				}
			}
		#endif

		if (CWnd * p = GetDlgItem (BTN_SELECT)) 
			p->EnableWindow (bDB);
	}

	if (pResult)
		* pResult = 0;
}

void CUserElementDlg::OnOK()
{
	CLongArray sel = GetSelectedItems (m_lv);
	CEdit * pData = (CEdit *)GetDlgItem (TXT_DATA);

	ASSERT (pData);

	if (sel.GetSize ()) {
		int nIndex = sel [0];
		CUserItem * p = (CUserItem *)m_lv.GetCtrlData (nIndex);
		CString strData;

		ASSERT (p);
		ASSERT (p->m_pElement);

		#ifdef __WINPRINTER__
			bool bSetData = false;

			GetDlgItemText (TXT_DATA, strData);

			if (CUserElement * pUser = DYNAMIC_DOWNCAST (CUserElement, p->m_pElement)) 
				bSetData = pUser->SetDefaultData (strData);
			if (CDatabaseElement * pUser = DYNAMIC_DOWNCAST (CDatabaseElement, p->m_pElement)) 
				bSetData = pUser->SetKeyValue (strData);
			if (CBitmapElement * pUser = DYNAMIC_DOWNCAST (CBitmapElement, p->m_pElement)) { //#if __CUSTOM__ == __SW0833__
				bSetData = pUser->SetFilePath (strData, * p->m_pHead);
				GlobalFuncs::ResetSize (pUser, * p->m_pHead);
			}
			if (CExpDateElement * pUser = DYNAMIC_DOWNCAST (CExpDateElement, p->m_pElement)) { // sw0924
				switch (pReplace->GetType ()) {
				case CExpDateElement::TYPE_USER:
					bSetData = pUser->SetDefaultData (strData);
					break;
				case CExpDateElement::TYPE_DATABASE:
					bSetData = pUser->SetKeyValue (strData);
					break;
				}
			}

			if (bSetData) {
				m_lv.UpdateCtrlData (p);

				if ((nIndex + 1) < m_lv->GetItemCount ()) {
					
					m_lv->SetItemState (nIndex + 1, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
					pData->SetSel (0, -1);
					return;
				}
			}
			else {
				CString str;

				str.Format (LoadString (IDS_INVALIDUSERDATA), strData);
				MsgBox (* this, str);
				pData->SetSel (0, -1);
				return;
			}
		#else
			GetDlgItemText (TXT_DATA, strData);

			p->m_strData = strData;

			m_lv.UpdateCtrlData (p);

			if ((nIndex + 1) < m_lv->GetItemCount ()) {
				
				m_lv->SetItemState (nIndex + 1, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
				pData->SetSel (0, -1);
				return;
			}
		#endif 
	}

	FoxjetCommon::CEliteDlg::OnOK ();

	#ifdef __IPPRINTER__
	m_vElements.RemoveAll ();
	#endif

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		CUserItem * p = (CUserItem *)m_lv.GetCtrlData (i);

		ASSERT (p);
		ASSERT (p->m_pElement);

		#ifdef __WINPRINTER__
			if (CUserElement * pUser = DYNAMIC_DOWNCAST (CUserElement, Find (p->m_pElement, m_vElements))) {
				CString str = p->m_pElement->GetDefaultData ();
				
				TRACEF (str);

				pUser->SetDefaultData (str);
			}
		#else
			m_vElements.Add (* p);
		#endif 
	}
}

FoxjetCommon::CBaseElement * CUserElementDlg::Find (FoxjetCommon::CBaseElement * pElement, 
													const CUserElementArray & vElements)
{
	for (int i = 0; i < vElements.GetSize (); i++) 
		if (vElements [i].m_pElement == pElement)
			return pElement;

	return NULL;
}

FoxjetCommon::CBaseElement * CUserElementDlg::Find (const CString & str,
													const CUserElementArray & vElements)
{
	for (int i = 0; i < vElements.GetSize (); i++) {
		CUserItem item = vElements [i];

		if (!item.m_strPrompt.CompareNoCase (str))
			return item.m_pElement;
	}

	return NULL;
}

void CUserElementDlg::OnSelect() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		int nIndex = sel [0];
		CUserItem * p = (CUserItem *)m_lv.GetCtrlData (nIndex);
		
#ifdef __WINPRINTER__
		if (CDatabaseElement * pElement = DYNAMIC_DOWNCAST (CDatabaseElement, p->m_pElement)) {
			CDbFieldDlg dlg (ListGlobals::defElements.m_strRegSection, this);

			dlg.m_dwFlags	= CCellCtrlImp::CELLCTRL_NOCHANGECOLSEL; 
			dlg.m_strDSN	= pElement->GetDSN ();
			dlg.m_strTable	= pElement->GetTable ();
			dlg.m_strField	= pElement->GetKeyField ();
			dlg.m_nRow		= 0;
			
			if (dlg.DoModal () == IDOK) 
				SetDlgItemText (TXT_DATA, dlg.m_strSelected);
		}
#endif //__WINPRINTER__

#ifdef __IPPRINTER__
		LONG lDynID = 0;

		if (CDynamicTextElement * pText = DYNAMIC_DOWNCAST (CDynamicTextElement, p->m_pElement)) 
			lDynID = pText->GetDynamicID ();
		if (CDynamicBarcodeElement * pBarcode = DYNAMIC_DOWNCAST (CDynamicBarcodeElement, p->m_pElement)) 
			lDynID = pBarcode->GetDynamicID ();

		if (lDynID) {
			using CDynamicTableDlg::CItemArray;
			using CDynamicTableDlg::CTableItem;

			CItemArray vItems;

			CDynamicTableDlg::GetDynamicData (m_lLineID, vItems);

			for (int i = 0; i < vItems.GetSize (); i++) {
				CTableItem item = vItems [i];
				
				if (item.m_lID == lDynID) {
					CDbFieldDlg dlg (ListGlobals::defElements.m_strRegSection, this);

					dlg.m_dwFlags	= CCellCtrlImp::CELLCTRL_NOCHANGECOLSEL; 
					dlg.m_strDSN	= item.m_params.m_strDSN;
					dlg.m_strTable	= item.m_params.m_strTable;
					dlg.m_strField	= item.m_params.m_strKeyField;
					dlg.m_nRow		= item.m_params.m_nRow;

					if (dlg.DoModal () == IDOK) 
						SetDlgItemText (TXT_DATA, dlg.m_strSelected);

					break;
				}
			}
		}
#endif //__IPPRINTER__

#ifdef __WINPRINTER__
		if (CBitmapElement * pElement = DYNAMIC_DOWNCAST (CBitmapElement, p->m_pElement)) { //#if __CUSTOM__ == __SW0833__
			CFileDialog dlg (TRUE, _T ("*.bmp"), pElement->GetFilePath (), 
				OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
				GetFileDlgList (), this);

			if (dlg.DoModal () == IDOK) 
				SetDlgItemText (TXT_DATA, dlg.GetPathName ());
		}
#endif //__WINPRINTER__
	}
}
