#pragma once

#include "Utils.h"
#include "resource.h"
#include <PortableDeviceApi.h>
#include <PortableDevice.h>

// CBackupDlg dialog

typedef struct tagDEVICE_FILE_STRUCT
{
	CString m_strID;
	CString m_strName;
	ULONG m_lSize;
	FoxjetCommon::CCopyArray <tagDEVICE_FILE_STRUCT, tagDEVICE_FILE_STRUCT &> m_v;
} DEVICE_FILE_STRUCT;


class CBackupDlg : public FoxjetCommon::CEliteDlg
{
	DECLARE_DYNAMIC(CBackupDlg)

public:
	CBackupDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CBackupDlg();

	bool IsLogicalDrive () const { return m_vRoot.m_v.GetSize () == 0; }

	static FoxjetCommon::CCopyArray <CString, CString &> ToString (const DEVICE_FILE_STRUCT & d, bool bDirectory = true, bool bFile = true, const CString & strDir = _T ("\\"));
	static CString GetID (const DEVICE_FILE_STRUCT & d, const CString & strName, const CString & strDir = _T ("/"));
	static CString GetTempPath ();
	static bool Extract (UINT nID, const CString & strPath, const CString & strType = _T ("EXE"), const CString & strModule = _T ("Utils.dll"));
	static bool CopyToPortableDevice (CComPtr<IPortableDevice> & device, const CString & strFrom, const CString & strTo, const DEVICE_FILE_STRUCT & root);
	static CComPtr<IPortableDevice> OpenDevice (const CString & strDevicePath);
	static HRESULT Enumerate (IPortableDeviceContent* pContent, LPCWSTR pwszParentObjectId, DEVICE_FILE_STRUCT & dir, int nDepth);
	static void Trace (const DEVICE_FILE_STRUCT & dir, int nCalls = 0);
	static bool Delete (CComPtr<IPortableDevice> & device, const CString & strID);

	typedef struct
	{
		int m_nCount;
		CString m_strPath;
	} RESULTSTRUCT;

	static void LocateLogosFolder (CDatabase & db, CStringArray & vResult);

// Dialog Data
	enum { IDD = IDD_BACKUP };
	DEVICE_FILE_STRUCT m_vRoot;

protected:
	FoxjetUtils::CRoundButtons m_buttons;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	CString m_strPath;
	CString m_strTempFile;
	CString m_strTitle;

	afx_msg void OnBnClickedBrowse();
	afx_msg void OnBnClickedDatabase();

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedAll();
	virtual void OnOK();
	afx_msg void OnBnClickedRestore();
};
