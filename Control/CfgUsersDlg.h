#if !defined(AFX_CFGUSERDLG_H__88DEB510_7505_43FF_93E6_98FBF41A3365__INCLUDED_)
#define AFX_CFGUSERDLG_H__88DEB510_7505_43FF_93E6_98FBF41A3365__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CfgfUserDlg.h : header file
//

#include "RoundButtons.h"
#include "ListCtrlImp.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CfgUsersDlg dialog

class CCfgUsersDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CCfgUsersDlg(CWnd* pParent = NULL);   // standard constructor

	static const CString m_strAdmin;

	class CUserItem : public ItiLibrary::ListCtrlImp::CItem, public FoxjetDatabase::USERSTRUCT
	{
	public:
		virtual CString GetDispText (int nColumn) const;

		CUserItem & operator = (const FoxjetDatabase::USERSTRUCT & rhs)
		{
			if (this != &rhs) {
				m_lID				= rhs.m_lID;
				m_strFirstName		= rhs.m_strFirstName;
				m_strLastName		= rhs.m_strLastName;
				m_strUserName		= rhs.m_strUserName;
				m_strPassW			= rhs.m_strPassW;
				m_lSecurityGroupID	= rhs.m_lSecurityGroupID;
			}

			return * this;
		}

		CString m_strGroup;
	};

// Dialog Data
	//{{AFX_DATA(CCfgUsersDlg)
	//}}AFX_DATA

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCfgUsersDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	FoxjetUtils::CRoundButtons m_buttons;

	void LoadUserTable (void);

	// Generated message map functions
	//{{AFX_MSG(CCfgUsersDlg)
	virtual void OnDestroy ();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnProperties();
	afx_msg void OnAdd();
	afx_msg void OnRemove();
	afx_msg void OnItemchangedUsers(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkUsers(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CFGUSERDLG_H__88DEB510_7505_43FF_93E6_98FBF41A3365__INCLUDED_)
