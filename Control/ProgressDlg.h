#if !defined(AFX_PROGRESSDLG_H__B6BE1F0A_1632_4648_990A_287FDED21BF4__INCLUDED_)
#define AFX_PROGRESSDLG_H__B6BE1F0A_1632_4648_990A_287FDED21BF4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProgressDlg.h : header file
//

#include <afxwin.h>
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CProgressDlg dialog

#define UPDATE_STATUS(dlg,id,str) (dlg).AddProcess ((id), (str), _T (__FILE__), __LINE__)
#define CLEAR_STATUS(dlg,id) (dlg).RemoveProcess ((id), _T (__FILE__), __LINE__)

class CProgressDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	int AddProcess (ULONG lID, CString sMsg, LPCTSTR lpszFile = _T (__FILE__), ULONG lLine = 0);
	int RemoveProcess (ULONG lID, LPCTSTR lpszFile = _T (__FILE__), ULONG lLine = 0);
	CProgressDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CProgressDlg ();

// Dialog Data
	//{{AFX_DATA(CProgressDlg)
	CString	m_sMessage;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgressDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CMap <ULONG, ULONG, CString, CString > m_mapMsg;
	// Generated message map functions
	//{{AFX_MSG(CProgressDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROGRESSDLG_H__B6BE1F0A_1632_4648_990A_287FDED21BF4__INCLUDED_)
