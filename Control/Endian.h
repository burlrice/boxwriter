// Endian.h: interface for the CEndian class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ENDIAN_H__97D281E0_E1DC_49B7_9521_C7084C53F37D__INCLUDED_)
#define AFX_ENDIAN_H__97D281E0_E1DC_49B7_9521_C7084C53F37D__INCLUDED_

#include "TypeDefs.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CEndian  
{
public:
//	static const _FJ_HEADCONFIG hton (const _FJ_HEADCONFIG Cfg);
//	static const _FJ_HEADCONFIG ntoh (const _FJ_HEADCONFIG Cfg);
	static void hton (_SOCKET_MESSAGE &sm);
	static void ntoh (_SOCKET_MESSAGE &sm);
	CEndian();
	virtual ~CEndian();

};

#endif // !defined(AFX_ENDIAN_H__97D281E0_E1DC_49B7_9521_C7084C53F37D__INCLUDED_)
