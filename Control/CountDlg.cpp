// CountDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "CountDlg.h"
#include "CountElement.h"
#include "Utils.h"

#ifdef __WINPRINTER__
	using namespace FoxjetElements;
#else
	using namespace FoxjetIpElements;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCountDlg dialog


CCountDlg::CCountDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(CCountDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCountDlg)
	m_lCount = 0;
	//}}AFX_DATA_INIT
}


void CCountDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCountDlg)
	//}}AFX_DATA_MAP

	FoxjetCommon::DDX_Text(pDX, TXT_COUNT, m_lCount);

	#ifdef __IPPRINTER__
	FoxjetCommon::DDV_MinMaxInt64(pDX, TXT_COUNT, m_lCount, 0, CCountElement::m_lmtStart.m_dwMax);
	#else
	FoxjetCommon::DDV_MinMaxInt64(pDX, TXT_COUNT, m_lCount, 0, CCountElement::m_iLmtStart);
	#endif
}


BEGIN_MESSAGE_MAP(CCountDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CCountDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCountDlg message handlers
