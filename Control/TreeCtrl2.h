#if !defined(AFX_TREECTRL2_H__A2326A86_38EF_436D_B741_2CEF1DAA5BCB__INCLUDED_)
#define AFX_TREECTRL2_H__A2326A86_38EF_436D_B741_2CEF1DAA5BCB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TreeCtrl2.h : header file
//

enum SCROLLMODE
{
	SCROLL_UP_SLOW,
	SCROLL_DOWN_SLOW,
	SCROLL_UP_NORMAL,
	SCROLL_DOWN_NORMAL
};

/////////////////////////////////////////////////////////////////////////////
// CTreeCtrl2 window

class CTreeCtrl2 : public CTreeCtrl
{
// Construction
public:
	CTreeCtrl2();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTreeCtrl2)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTreeCtrl2();

	// Generated message map functions
protected:
	void OnButtonUp (void);
	//{{AFX_MSG(CTreeCtrl2)
	afx_msg void OnBegindrag(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	BOOL		m_bDragging;
	HTREEITEM	m_hitemDrag;
	HTREEITEM	m_hitemDrop;
	int	m_nSlowScrollTimeout;
	UINT	m_nTimerID;
	int	m_nUpperYCoor;
	int	m_nLowerYCoor;
	int	m_nScrollBarSize;

	BOOL	TransferItem(HTREEITEM hitem, HTREEITEM hNewParent);
	BOOL	IsChildNodeOf(HTREEITEM hitemChild, HTREEITEM hitemSuspectedParent);
	BOOL		NeedToScroll( CPoint ptMouse );
	SCROLLMODE	RefineScrollMode( CPoint ptMouse );
	CImageList*	m_pImageList;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TREECTRL2_H__A2326A86_38EF_436D_B741_2CEF1DAA5BCB__INCLUDED_)
