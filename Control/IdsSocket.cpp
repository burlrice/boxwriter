// IdsSocket.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "Sw0848Thread.h"
#include "ProdLine.h"
#include "control.h"
#include "IdsSocket.h"
#include "Debug.h"

#ifdef __WINPRINTER__
	#include "LocalHead.h"
#endif //__WINPRINTER__

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIdsSocket

CIdsSocket::CIdsSocket()
:	m_pThread (NULL)
{
}

CIdsSocket::~CIdsSocket()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CIdsSocket, CAsyncSocket)
	//{{AFX_MSG_MAP(CIdsSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CIdsSocket member functions

/* Packet structure
	xx xx xx xx xx xx xx xx xx
	|  |  |  |  |  |     |-> vacuum 100ths psi
	|  |  |  |  |  |----> pressure 100ths psi
	|  |  |  |  |-------> broken flag
	|  |  |  |----------> ink-low(1)/out(2) flag 
	|  |  |-------------> minor version
	|  |----------------> major version
	|-------------------> number of bytes
*/
void CIdsSocket::OnReceive(int nErrorCode) 
{
	CString strAddr;
	UINT nPort = 0;
	UCHAR cBuffer [0xFF] = { 0 };

	int nRecv = ReceiveFrom (&cBuffer, sizeof (cBuffer), strAddr, nPort);

	if (nRecv > 0 && m_pThread) {
		STATUSSTRUCT * p = new STATUSSTRUCT;

		ZeroMemory (p, sizeof (STATUSSTRUCT));
		
		UCHAR cVerMajor	= cBuffer [1];
		UCHAR cVerMinor	= cBuffer [2];

		switch (cBuffer [3]) {
		case 0:		p->m_inkLevel = OK;			break;
		case 1:		p->m_inkLevel = LOW_INK;	break;
		case 2:		p->m_inkLevel =	INK_OUT;	break;
		}

		p->m_bBroken	= cBuffer [4] ? true : false;
		
		p->m_wPressure	|= cBuffer [5];
		p->m_wPressure	|= cBuffer [6] << 8;

		p->m_wVaccum	|= cBuffer [7];
		p->m_wVaccum	|= cBuffer [8] << 8;

		while (!::PostThreadMessage (m_pThread->m_nThreadID, TM_IDS_STATUS, (WPARAM)p, 0))
			::Sleep (0);
	}
	
	CAsyncSocket::OnReceive(nErrorCode);
}
