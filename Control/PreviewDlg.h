#if !defined(AFX_PREVIEWDLG_H__4AB66D78_68CD_448A_8286_F0B16E1E663E__INCLUDED_)
#define AFX_PREVIEWDLG_H__4AB66D78_68CD_448A_8286_F0B16E1E663E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PreviewDlg.h : header file
//

#include "Database.h"
#include "OdbcDatabase.h"
#include "PreviewCtrl.h"
#include "Box.h"
#include "Utils.h"

using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////
// CPreviewDlg dialog

namespace PreviewDlg
{
	class CTaskPreview
	{
	public:
		CTaskPreview (const FoxjetDatabase::TASKSTRUCT & task, CDC & dc, CProdLine * pLine);
		virtual ~CTaskPreview ();

		CSize GetSize (CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vHeads, 
			const Foxjet3d::Box::CBox & box, const FoxjetDatabase::COrientation & orient);

		FoxjetDatabase::TASKSTRUCT	m_task;
		CBitmap						m_bmp [6];
		CMap <ULONG, ULONG, CBitmap *, CBitmap *> m_map;

		void Blt (CDC & dcPanel, CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vHeads, 
			FoxjetDatabase::VALVESTRUCT & valve);
		void Blt (CDC & dcPanel, FoxjetDatabase::HEADSTRUCT & head);
	};

	class CPreviewDlg : public FoxjetCommon::CEliteDlg // sw0867
	{

	// Construction
	public:
		CPreviewDlg(COdbcDatabase & db, CProdLine * pLine, CWnd* pParent = NULL);   // standard constructor
		virtual ~CPreviewDlg ();

		TASKSTRUCT m_task;

	// Dialog Data
		//{{AFX_DATA(CPreviewDlg)
		enum { IDD = IDD_PREVIEW };
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		void Draw (CDC & dc);

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CPreviewDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		void Invalidate();
		virtual void OnOK();

		COdbcDatabase & m_db;
		CPreviewCtrl m_wndPreview;
		UINT m_nPanel;
		CSize m_sizeDef;
		CPoint m_ptDef;
		double m_dZoom;
		CTaskPreview * m_pTask;
		CBitmap * m_pBmp;
		bool m_bTracking;
		CProdLine * m_pLine;

		// Generated message map functions
		//{{AFX_MSG(CPreviewDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnSelchangePanel();
		afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
		afx_msg void OnSize(UINT nType, int cx, int cy);
		afx_msg void OnDestroy();
		afx_msg void OnReleasedcaptureZoom(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnPrint();
		//}}AFX_MSG

		afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

		DECLARE_MESSAGE_MAP()
	};
}; // namespace PreviewDlg

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PREVIEWDLG_H__4AB66D78_68CD_448A_8286_F0B16E1E663E__INCLUDED_)
