// ImageView.cpp : implementation file
//

#include "stdafx.h"
#include <AFXPRIV.H>
#include "Resource.h"
#include "Control.h"
#include "ImageView.h"
#include "TemplExt.h"
#include "Debug.h"
#include "ControlView.h"
#include "Color.h"
#include "Database.h"
#include "xImage.h"
#include "BaseElement.h"
#include "Registry.h"
#include "Coord.h"
#include "Parse.h"
#include "RoundButtons.h"
#include "ProdLine.h"
#include "TemplExt.h"

using namespace FoxjetDatabase;
using namespace FoxjetCommon;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const double CImageView::m_dZoomMin = 0.10;
const double CImageView::m_dZoomMax = 1.00;
const int CImageView::m_nMargin = 10;
const double CImageView::m_dZoomIncrement = 0.10;

/////////////////////////////////////////////////////////////////////////////
// CImageView

IMPLEMENT_DYNCREATE(CImageView, CScrollView)

CImageView::CImageView () 
:	m_pPreviewDC (NULL), 
	m_pPreviewBitmap (NULL), 
	m_dZoom (0.33),
	m_ScrollSizes (0, 0),
	m_lPanelID (0),
	m_lTaskID (0),
	m_rcPanel (0, 0, 0, 0),
	m_lLineID (FoxjetDatabase::NOTFOUND),
	m_ptDrag (NULL),
	m_ptScroll (NULL),
	m_bZoomToFit (false)
{
	int nZoom = FoxjetDatabase::GetProfileInt (CControlApp::GetRegKey () + _T ("\\Settings"), _T ("Zoom"), 33);
	SetZoom ((double)nZoom / 100.0, false);

	m_fntWarning.CreateFont (48, 0, 0, 0, FW_BOLD, FALSE, 0, 0,
		DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("Microsoft Sans Serif"));
}

CImageView::~CImageView()
{
}


BEGIN_MESSAGE_MAP(CImageView, CScrollView)
	//{{AFX_MSG_MAP(CImageView)
	ON_WM_DESTROY()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_WM_ERASEBKGND()
	ON_WM_CREATE()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_MESSAGE_VOID(WM_INITIALUPDATE, OnInitialUpdate)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImageView drawing

void CImageView::OnInitialUpdate()
{
	CRect rc;

	CScrollView::OnInitialUpdate();

	GetClientRect (rc);
	SetScrollSizes (MM_TEXT, rc.Size ());
}


void CImageView::OnDraw(CDC* pDC)
{
	if (m_rcPanel.IsRectEmpty ()) 
		return;

	pDC->FillSolidRect (CRect (CPoint (0, 0), GetTotalSize ()) / GetZoom (), Color::rgbDarkGray);
	
	/*
	{
		CRect rc = CPoint (m_nMargin, m_nMargin) + (m_rcPanel * GetZoom ());
		pDC->FillSolidRect (rc, Color::rgbRed);
		pDC->FrameRect (rc, &CBrush (Color::rgbBlue));
	}
	*/

	{
		CString strZoom;
		CRect rc = CPoint (m_nMargin, m_nMargin) + (m_rcPanel * GetZoom ());
	
		pDC->BitBlt (rc.left, rc.top, rc.Width (), rc.Height (), m_pPreviewDC, 0, 0, SRCCOPY);
	}

	CDC dcMem;
	CRect rc;
	CString strZoom;

	GetClientRect (rc);
	dcMem.CreateCompatibleDC (pDC);
	strZoom.Format (_T ("%d %%"), (int)(GetZoom () * 100.0));

	if (!m_bmpZoom.m_hObject) {
		DIBSECTION ds;

		VERIFY (m_bmpZoom.LoadBitmap (IDB_ZOOM));
		CBitmap * pbmp = dcMem.SelectObject (&m_bmpZoom);

		memset (&ds, 0, sizeof (ds));
		m_bmpZoom.GetObject (sizeof (ds), &ds);
		m_rgbTransparent = dcMem.GetPixel (0, 0);
		m_sizeZoom = CSize (ds.dsBm.bmWidth, ds.dsBm.bmHeight);
		dcMem.SelectObject (pbmp);
	}

	CBitmap * pbmp = dcMem.SelectObject (&m_bmpZoom);
	CPoint ptOffset = pDC->GetViewportOrg ();
	int cx = m_rcZoom.Width ();
	int cy = m_rcZoom.Height ();
	int x = (rc.Width () - cx) - ptOffset.x;
	int y = (rc.Height () - cy) - ptOffset.y;
	CRect rcZoom;// = CRect (CPoint (0, 0), m_rcZoom.Size ()) + CPoint (x, y);
	int nDrawText = pDC->DrawText (strZoom, &rcZoom, DT_CALCRECT);
	//rcZoom += CPoint ((m_sizeZoom.cx - rcZoom.Width ()) / 2, -nDrawText);
	int nSetBkMode = pDC->SetBkMode (TRANSPARENT);

	m_rcZoom = CRect (CPoint (rc.Width () - m_sizeZoom.cx - 1, rc.Height () - m_sizeZoom.cy - 1), m_sizeZoom);
	m_rcZoomIn = m_rcZoom;
	m_rcZoomIn.bottom -= (m_rcZoomIn.Height () / 2) - 2;
	m_rcZoomOut = m_rcZoomIn + CPoint (0, m_rcZoomIn.Height () + 1);

	rcZoom = CRect (CPoint (m_rcZoom.left, m_rcZoom.top - nDrawText), CSize (m_rcZoom.Width (), nDrawText)) - ptOffset;
	rcZoom += CPoint (0, m_rcZoomIn.Height () + m_rcZoomOut.Height () - nDrawText);


	{
		CStringArray vLines;
		CStringArray vErrors;
		CRect rcError = rcZoom;

		m_pDoc->GetProductLines (vLines);
		
		for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
			if (CProdLine * pLine = m_pDoc->GetProductionLine (vLines [nLine])) 
				pLine->GetWarnings (vErrors);
		}

		if (m_pDoc->IsLocal ()) {
			CString str = _T ("<0>") + LoadString (IDS_DATABASE) + _T (": ") + LoadString (IDS_LOCALDBCONNECTION);

			if (::Find (vErrors, str) == -1)
				InsertAscending (vErrors, str);
		}
			
		if (CControlView * pView = m_pDoc->GetView ()) {
			CStringArray * p = new CStringArray ();
			p->Append (vErrors);
			pView->PostMessage (WM_SET_WARNING_MESSAGES, (WPARAM)p, 0);
		}

		if (vErrors.GetSize ()) {
			CRect rcTmp = rcError;
			const int nFlags = DT_CENTER;
			CString str;
			static const CString strOutOfInk = LoadString (IDS_HEADERROR_OUTOFINK);

			for (int i = vErrors.GetSize () - 1; i >= 0; i--) {
				if (vErrors [i].Find (strOutOfInk) != -1)
					vErrors.RemoveAt (i);
			}

			for (int i = 0; i < vErrors.GetSize (); i++) {
				CString strHead = Extract (vErrors [i], _T (">"));

				if (Extract (strHead, _T (": ")).GetLength ())
					str += strHead + _T ("\n");
			}

			str.TrimRight ();
			//TRACEF (str);

			CFont * pFont = pDC->SelectObject (&m_fntWarning);
			int nDrawText = pDC->DrawText (str, rcTmp, nFlags | DT_CALCRECT);
			int cx = rcTmp.Width ();
			rcTmp = rcError - CPoint (0, (rcTmp.bottom - rcError.bottom) - (nDrawText / vErrors.GetSize ()));
			rcTmp.left = rcError.right - cx;
			rcTmp.top -= ((nDrawText / vErrors.GetSize ()) * (vErrors.GetSize () - 1));
			rcTmp.bottom = rcTmp.top + nDrawText;
			m_rcError = rcTmp;
			m_rcError.InflateRect (3, 3);

			if (str.GetLength ()) {
				CRect rcWin (0, 0, 0, 0);

				GetWindowRect (rcWin);
				CPoint pt ((rcWin.Width () - m_rcError.Width ()) / 2, (rcWin.Height () - m_rcError.Height ()) / 2);
				m_rcError -= pt;
				pDC->FillSolidRect (m_rcError, Color::rgbYellow);//FoxjetUtils::GetDefColor (_T ("background")));
				pDC->DrawEdge (m_rcError, EDGE_SUNKEN, BF_LEFT | BF_RIGHT | BF_TOP | BF_BOTTOM | BF_ADJUST);
				pDC->DrawText (str, m_rcError, nFlags);
			}

			pDC->SelectObject (pFont);
		}
		else
			m_rcError = CRect (0, 0, 0, 0);
	}

	/*
	#ifdef _DEBUG
	COLORREF rgbSetTextColor = pDC->SetTextColor (Color::rgbBlue);

	//::TransparentBlt (* pDC, x, y, cx, cy, dcMem, 0, 0, cx, cy, m_rgbTransparent);
	pDC->DrawText (strZoom, &rcZoom, DT_RIGHT | DT_BOTTOM | DT_SINGLELINE);
	pDC->SetTextColor (rgbSetTextColor);
	pDC->SetBkMode (nSetBkMode);

	//dcMem.SelectObject (pbmp);

	//pDC->FrameRect (m_rcZoomIn - ptOffset, &CBrush (Color::rgbGreen));
	//pDC->FrameRect (m_rcZoomOut - ptOffset, &CBrush (Color::rgbRed));
	//pDC->FrameRect (rcZoom - ptOffset, &CBrush (Color::rgbYellow));
	#endif
	*/

/* TODO: rem
	if (!m_rcPanel.IsRectEmpty ()) 
		pDC->BitBlt (0, 0, m_rcPanel.Width(), m_rcPanel.Height(), m_pPreviewDC, 0, 0, SRCCOPY);

	/*
	#ifdef _DEBUG
	if (CBitmap * pBmp = m_pPreviewDC->GetCurrentBitmap ()) {
		BITMAP bm;

		::ZeroMemory (&bm, sizeof (bm)); 

		if (pBmp->GetObject (sizeof (bm), &bm)) {
			ULONG lSize = bm.bmWidth * bm.bmHeight;
			ULONG lPixels = FoxjetCommon::CountPixels (* pBmp);

			CString str; str.Format (_T ("lPixels: %d (%.02f%%) [size: %d, %d (%d)]"), lPixels, ((double)lPixels / (double)lSize) * 100.0, bm.bmWidth, bm.bmHeight, lSize); TRACEF (str);
		}
	}
	#endif
	* /
*/
}

/////////////////////////////////////////////////////////////////////////////
// CImageView diagnostics

#ifdef _DEBUG
void CImageView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CImageView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CImageView message handlers

void CImageView::GetWorkingHead (FoxjetDatabase::HEADSTRUCT & workingHead) 
{
	using namespace FoxjetDatabase;

	Head::CHead * pWorkingHead = NULL;
	UINT nMaxRes = 0;
	CPrintHeadList vHeads;

	if ( m_pDoc )
	{
		CString sSelectedLine = m_pDoc->GetSelectedLine()->GetName();
		LOCK (m_pDoc->m_ConnectionListSentinel);
		m_pDoc->GetActiveHeads ( sSelectedLine, vHeads );
	}

	for (POSITION pos = vHeads.GetStartPosition (); pos != NULL; ) {
		Head::CHead * pHead = NULL;
		DWORD Key;

		vHeads.GetNextAssoc(pos, Key, (void *&)pHead);

		HEADSTRUCT h = Head::CHead::GetInfo (pHead).m_Config.GetHeadSettings ();

		if (h.m_nHorzRes > nMaxRes) {
			nMaxRes = (int)h.GetRes (); //h.m_nHorzRes;
			pWorkingHead = pHead;
		}
	}

	if ( pWorkingHead )
		workingHead = Head::CHead::GetInfo (pWorkingHead).m_Config.GetHeadSettings ();
}

void CImageView::Draw (CDC & dc, HEADSTRUCT & workingHead)
{
	CPrintHeadList list;
	CString strLine = m_pDoc->GetSelectedLine()->GetName ();

	if (!m_pDoc->GetActiveHeads (strLine, list)) 
		return;

	for (POSITION pos = list.GetStartPosition (); pos != NULL; ) {
		Head::CHead * pHead = NULL;
		DWORD dwKey;

		list.GetNextAssoc(pos, dwKey, (void *&)pHead);

		Head::CHeadInfo info = Head::CHead::GetInfo (pHead);

		if (info.GetBoxPanel() == m_lPanelID) {
			Head::CHead::PREVIEWSTRUCT s;

			SEND_THREAD_MESSAGE (info, TM_GET_PREVIEW_BMP, &s);

			//if (s.m_pLock) 
			if (CBitmap * pBmp = s.m_pBmp)
			{ 
				DIBSECTION ds = { 0 };
				//LOCK (* s.m_pLock);

				bool bDraw =  (info.GetTaskState() != FoxjetDatabase::STOPPED);

				#ifdef __IPPRINTER__
				bDraw |= !info.m_sTaskName.IsEmpty ();
				#endif

				pBmp->GetObject (sizeof (ds), &ds);

				if (pBmp && bDraw) {
					CSize sizeHead;
					sizeHead.cy  = info.m_Config.GetHeadSettings().m_lRelativeHeight;
					sizeHead = FoxjetCommon::CBaseElement::ThousandthsToLogical (sizeHead, info.m_Config.GetHeadSettings() );
					int nHeadPosition = sizeHead.cy;
					int nHeadHeight = info.m_Config.GetHeadSettings().Height();
					CSize sizeBmp (ds.dsBm.bmWidth, ds.dsBm.bmHeight);// = pBmp->GetBitmapDimension ();

					CDC dcMem;
					CBitmap *pOld;
					FoxjetDatabase::HEADSTRUCT head = info.m_Config.GetHeadSettings();
					double dXStretch = (double)workingHead.GetRes () / (double)head.GetRes ();

					dcMem.CreateCompatibleDC (&dc);
					pOld = dcMem.SelectObject (pBmp);

					CRect rDest ( 0, 0, m_rcPanel.Width(), nHeadHeight );

					CRect rHead ( 0, 0, sizeBmp.cx, sizeBmp.cy );
					int nXOffset = (int) ((double)rDest.left * dXStretch);
					rDest.OffsetRect ( nXOffset, m_rcPanel.Height() - ( nHeadPosition + nHeadHeight ));

					dc.StretchBlt (rDest.left, rDest.top, rDest.Width(), rDest.Height(), &dcMem, 0, 0, rHead.Width(), rHead.Height(), SRCAND );

					dcMem.SelectObject (pOld);
				}

				delete pBmp;
			}
		}
	}
}

int CImageView::Find (const CArray <Head::CHead *, Head::CHead *> & vHeads, ULONG lHeadID)
{
	for (int i = 0; i < vHeads.GetSize (); i++)
		if (Head::CHead * p = vHeads [i])
			if (Head::CHead::GetInfo (p).m_Config.m_HeadSettings.m_lID == lHeadID)
				return i;

	return -1;
}

void CImageView::Draw (CDC & dc, HEADSTRUCT & workingHead, VALVESTRUCT & valve, const CArray <Head::CHead *, Head::CHead *> & vHeads)
{
	int nIndex = Find (vHeads, valve.m_lCardID);

	if (nIndex == -1)
		return;

	Head::CHead * pHead = vHeads [nIndex];
	Head::CHeadInfo info = Head::CHead::GetInfo (pHead);
	HEADSTRUCT & head = info.m_Config.m_HeadSettings;
	Head::CHead::PREVIEWSTRUCT s;

	SEND_THREAD_MESSAGE (info, TM_GET_PREVIEW_BMP, &s);

	//if (s.m_pLock) 
	{ 
		//LOCK (* s.m_pLock);

		int y = 0, cy = GetValveChannels (valve.m_type);
		int nHeads = min (valve.m_nIndex - 1, head.m_vValve.GetSize ());

		for (int i = 0; i < nHeads; i++) {
			VALVESTRUCT & tmp = head.m_vValve [i];
			int n = GetValveChannels (tmp.m_type);

			y += n;
		}

		if (CBitmap * pImage = s.m_pBmp) {
			CxImage img;
			BITMAP bm;

			::ZeroMemory (&bm, sizeof (bm));
			pImage->GetObject (sizeof (bm), &bm);

			//{ CString str; str.Format ("C:\\Temp\\Debug\\pImage [%s, %s].bmp", head.m_strName, valve.m_strName); SAVEBITMAP (* pImage, str); }

			img.CreateFromHBITMAP (* pImage);
			img.Crop (0, y, bm.bmWidth, y + cy);
			img.Flip ();

			//img.Save (_T ("C:\\Foxjet\\Debug\\img.bmp"), CXIMAGE_FORMAT_BMP); 

			int nXDest		= 0;
			int nWidthDest	= 0;
			int nYDest		= FoxjetCommon::CBaseElement::ThousandthsToLogical (CPoint (0, valve.m_lHeight), head).y;
			int nHeightDest	= FoxjetCommon::CBaseElement::ThousandthsToLogical (CPoint (0, GetValveHeight (valve.m_type)), head).y;

			if (CBitmap * p = dc.GetCurrentBitmap ()) {
				BITMAP bm;

				::ZeroMemory (&bm, sizeof (bm));
				p->GetObject (sizeof (bm), &bm);
				nWidthDest = bm.bmWidth;

				nYDest = bm.bmHeight - (nYDest + nHeightDest); // adjust coordinate system
			}

			double dDot = 4.0; // TODO: must be 2.0 or greater
			double dHalfDot = dDot / 2.0;
			CBrush brush (Color::rgbBlack);
			CPen pen (PS_SOLID, 1, Color::rgbBlack);
			double dStretch [2] = 
			{ 
				(double)dc.GetDeviceCaps (LOGPIXELSX) / (double)head.GetRes (),
				(double)dc.GetDeviceCaps (LOGPIXELSY) / ((double)GetValveChannels (valve.m_type) / ((double)GetValveHeight (valve.m_type) / 1000.0)),
			};

			CBrush * pBrush = dc.SelectObject (&brush);
			CPen * pPen = dc.SelectObject (&pen);

			for (DWORD y = 0; y < img.GetHeight (); y++) {
				for (DWORD x = 0; x < img.GetWidth (); x++) {
					bool bBit = img.GetPixelGray (x, y) ? false : true;

					if (bBit) {
						double i = ((double)x * dStretch [0]);
						double j = ((double)y * dStretch [1]);
						CRect rc (CPoint (nXDest + (int)i, nYDest + (int)j), CSize ((int)dDot, (int)dDot));

						dc.Ellipse (rc);
					}
				}
			}

			//{ CString str; str.Format ("C:\\Temp\\Debug\\dc [%s, %s].bmp", head.m_strName, valve.m_strName); SAVEBITMAP (dc, str); }

			dc.SelectObject (pBrush);
			dc.SelectObject (pPen);

			/*
			{ 
				CDC dcMem;
				CBitmap bmpMem;

				dcMem.CreateCompatibleDC (&dc);

				bmpMem.Attach (img.MakeBitmap (dc));
				{ CString str; str.Format ("C:\\Temp\\Debug\\slice [%s, %s].bmp", head.m_strName, valve.m_strName); SAVEBITMAP (bmpMem, str); }

				CBitmap * pMem = dcMem.SelectObject (&bmpMem);

				dc.StretchBlt (nXDest, nYDest, nWidthDest, nHeightDest, &dcMem, 0, 0, bm.bmHeight, y + cy, SRCCOPY);
				{ CString str; str.Format ("C:\\Temp\\Debug\\dcPanel [%s, %s].bmp", head.m_strName, valve.m_strName); SAVEBITMAP (dc, str); }

				dcMem.SelectObject (pMem);
			}
			*/

			delete pImage;
		}
	}
}

void CImageView::DrawPreview(CDC *pDC)
{
	if ( m_pDoc )
	{
		LOCK (m_pDoc->m_ConnectionListSentinel);

		CControlView * pView = m_pDoc->GetView ();
		CString strLine = m_pDoc->GetSelectedLine()->GetName ();
		int nIndex = CPanel::Find (m_vPanels, m_lPanelID);
		CArray <Head::CHead *, Head::CHead *> vHeads;

		{
			CPrintHeadList list;

			m_pDoc->GetActiveHeads (strLine, list);

			for (POSITION pos = list.GetStartPosition(); pos != NULL; ) {
				Head::CHead * pHead = NULL;
				DWORD dw = 0;

				list.GetNextAssoc (pos, dw, (void *&)pHead);
				ASSERT (pHead);
				vHeads.Add (pHead);
			}
		}		

		if (nIndex != -1) {
			CPanel & panel = m_vPanels [nIndex];
			CBitmap bmpPanel;
			CDC dcPanel;
			FoxjetDatabase::HEADSTRUCT workingHead;

			GetWorkingHead (workingHead);

			dcPanel.CreateCompatibleDC (pDC);
			bmpPanel.CreateCompatibleBitmap (&dcPanel, m_rcPanel.Width(), m_rcPanel.Height());
			
			CBitmap * pOldPanel = dcPanel.SelectObject (&bmpPanel);

			{
				CRect rc = m_rcPanel;// * GetZoom ();
				::BitBlt (dcPanel, 0, 0, rc.Width(), rc.Height(), NULL, 0, 0, WHITENESS);
			}


			for (int nCard = 0; nCard < panel.m_vCards.GetSize (); nCard++) {
				HEADSTRUCT & card = panel.m_vCards [nCard];
				
				if (!IsValveHead (card)) {
					ASSERT (panel.m_vValve.GetSize () == 0);
					Draw (dcPanel, workingHead);
					//{ CString str; str.Format ("C:\\Temp\\Debug\\dcPanel [%s, %s].bmp", panel.m_panel.m_strName, card.m_strName); SAVEBITMAP (dcPanel, str); }
				}

				for (int nHead = 0; nHead < card.m_vValve.GetSize (); nHead++) {
					VALVESTRUCT valve = card.m_vValve [nHead];

					Draw (dcPanel, workingHead, valve, vHeads);
					//{ CString str; str.Format ("C:\\Temp\\Debug\\dcPanel [%s, %s].bmp", panel.m_panel.m_strName, valve.m_strName); SAVEBITMAP (dcPanel, str); }
				}
			}

			for (int nHead = 0; nHead < panel.m_vValve.GetSize (); nHead++) {
				VALVESTRUCT valve = panel.m_vValve [nHead];

				Draw (dcPanel, workingHead, valve, vHeads);
				//{ CString str; str.Format ("C:\\Temp\\Debug\\dcPanel [%s, %s].bmp", panel.m_panel.m_strName, valve.m_strName); SAVEBITMAP (dcPanel, str); }
			}


			{
				CRect rc = m_rcPanel;// * GetZoom ();
				::BitBlt (pDC->m_hDC, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
			}

			pDC->StretchBlt ( 0, 0, 
				(int)((double)m_rcPanel.Width() * m_dZoom), 
				(int)((double)m_rcPanel.Height() * m_dZoom), 
				&dcPanel, 0, 0, m_rcPanel.Width(), m_rcPanel.Height(), SRCAND );
			//SAVEBITMAP (dcPanel, "C:\\Temp\\Debug\\dcPanel [" + panel.m_panel.m_strName + "].bmp");
			//SAVEBITMAP (* pDC, "C:\\Temp\\Debug\\pDC [" + panel.m_panel.m_strName + "].bmp");
			
			dcPanel.SelectObject (pOldPanel);
		}
	}
}

void CImageView::SaveScrollPos ()
{
	CPoint ptScroll = GetScrollPosition ();
	CString strKey = CControlApp::GetRegKey () + _T ("\\Settings\\ScrollPosition");

	if (ptScroll.y == 0)
		if (FoxjetDatabase::GetProfileInt (strKey, _T ("y"), -1) == -1) 
			ptScroll.y = GetTotalSize ().cy;

	FoxjetDatabase::WriteProfileInt (strKey, _T ("x"), ptScroll.x);
	FoxjetDatabase::WriteProfileInt (strKey, _T ("y"), ptScroll.y);
}

void CImageView::SetPreviewPanel(Head::CHead *pHead, ULONG lTaskID, ULONG lPanelID)
{
/*
#ifdef _DEBUG
{
	CString str;
	str.Format (_T ("SetPreviewPanel: %s, %d"), (pHead ? pHead->GetFriendlyName () : "NULL"), lTaskID);
	TRACEF (str);
}
#endif //_DEBUG
*/

	FoxjetDatabase::tagTASKSTRUCT Ts;
	FoxjetDatabase::tagBOXSTRUCT BS;
	FoxjetDatabase::HEADSTRUCT workingHead;
	CPoint ptScroll = GetScrollPosition ();
	bool bStopped = false;
	Head::CHeadInfo info = Head::CHead::GetInfo (pHead);

	if (lTaskID == 0 && pHead)
		lTaskID = info.m_Task.m_lID;

	if (pHead)
		bStopped = (info.m_eTaskState == STOPPED);
	
	bStopped |= /* (lTaskID == -1) || */ (lTaskID == 0);

	if (!pHead || !theApp.GetPreview () || bStopped) {
		if (m_pPreviewBitmap) {
			delete m_pPreviewBitmap;
			m_pPreviewBitmap = NULL;
		}
		
		m_rcPanel = CRect (0, 0, 1, 1);
		m_pPreviewBitmap = new CBitmap;
		CDC *pDC = GetDC();

		m_pPreviewBitmap->CreateCompatibleBitmap ( pDC, m_rcPanel.Width(), m_rcPanel.Height() );
		if ( m_pPreviewDC == NULL )
		{
			m_pPreviewDC = new CDC();
			m_pPreviewDC->CreateCompatibleDC ( pDC );
		}
		ReleaseDC ( pDC );

		m_pPreviewDC->SelectObject ( m_pPreviewBitmap );

		{
			CRect rc = m_rcPanel;// * GetZoom ();
			::BitBlt ( m_pPreviewDC->m_hDC, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
		}

		return;
	}

	GetWorkingHead (workingHead);

	double dXStretch = (double)workingHead.m_nHorzRes / (double)info.m_Config.GetHeadSettings ().m_nHorzRes;
	CProdLine * pLine = m_pDoc->GetSelectedLine ();

	if (m_lLineID != pLine->m_LineRec.m_lID) {
		m_lLineID = pLine->m_LineRec.m_lID;
		OnHeadConfigUpdated ();
	}

	m_lTaskID = lTaskID;

	if (m_lTaskID != 0) {
		BS = info.m_Box;
		Ts = info.m_Task;
		m_rcPanel.SetRect (0, 0, 0, 0);

		if ((int)lPanelID <= 0)
			m_lPanelID = info.GetBoxPanel();
		else
			m_lPanelID = lPanelID;

		CSize LabelSize = Ts.GetPanelSize (pLine->m_vPanels, m_lPanelID, BS);

		LabelSize.cx = BindTo ((int)LabelSize.cx, (int)MIN_PRODLEN, (int)MAX_PRODLEN);
		LabelSize.cy = BindTo ((int)LabelSize.cy, (int)MIN_PRODLEN, (int)MAX_PRODLEN);

		if (m_lTaskID == -1) {
			CPrintHeadList vHeads;
			int cx = 1024, cy = 0;

			m_pDoc->GetActiveHeads (pLine->GetName (), vHeads);

			for (POSITION pos = vHeads.GetStartPosition (); pos != NULL; ) {
				Head::CHead * pHead = NULL;
				DWORD dwKey;

				vHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);

				FoxjetDatabase::HEADSTRUCT h = info.m_Config.GetHeadSettings ();
				int nOffset = FoxjetCommon::CBaseElement::ThousandthsToLogical (CPoint (0, h.m_lRelativeHeight), workingHead).y;
				int nTotal = h.Height () + nOffset;

				cy = max (cy, nTotal);
			}

			CSize sizeMaxLabel = FoxjetCommon::CBaseElement::LogicalToThousandths (CPoint (cx, cy), workingHead);

			LabelSize.cx = max (LabelSize.cx, sizeMaxLabel.cx);
			LabelSize.cy = max (LabelSize.cy, sizeMaxLabel.cy);
		}

		LabelSize.cx = (int)((double)LabelSize.cx * dXStretch);
		CSize PanelSize = FoxjetCommon::CBaseElement::ThousandthsToLogical (LabelSize, info.m_Config.GetHeadSettings() );
		
		m_rcPanel.SetRect ( 0, 0, PanelSize.cx, PanelSize.cy );

		if ( m_pPreviewBitmap )
			delete m_pPreviewBitmap;

		m_pPreviewBitmap = new CBitmap;
		
		CDC *pDC = GetDC();
		m_pPreviewBitmap->CreateCompatibleBitmap ( pDC, m_rcPanel.Width(), m_rcPanel.Height() );
		if ( m_pPreviewDC == NULL )
		{
			m_pPreviewDC = new CDC();
			m_pPreviewDC->CreateCompatibleDC ( pDC );
		}
		ReleaseDC ( pDC );

		CBitmap * pPreview = m_pPreviewDC->SelectObject ( m_pPreviewBitmap );
	
		DIBSECTION ds;

		if (m_pPreviewBitmap->m_hObject) {
			CRect rcWin (0, 0, 0, 0);
			memset (&ds, 0, sizeof (ds));
			m_pPreviewBitmap->GetObject (sizeof (ds), &ds);

			GetWindowRect (rcWin);
			CRect rc (0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight);
			::BitBlt ( m_pPreviewDC->m_hDC, 0, 0, m_rcPanel.Width(), m_rcPanel.Height(), NULL, 0, 0, WHITENESS);
			DrawPreview ( m_pPreviewDC );

			if ( !m_rcPanel.IsRectNull() )
			{
				m_ScrollSizes.cx = (int)((double)m_rcPanel.Width () * m_dZoom) + (m_nMargin * 2);
				m_ScrollSizes.cy = (int)((double)m_rcPanel.Height () * m_dZoom) + (m_nMargin * 2);

				if (m_ScrollSizes.cx <= rcWin.Width ())
					m_ScrollSizes.cx = rcWin.Width ();

				if (m_ScrollSizes.cy <= rcWin.Height ())
					m_ScrollSizes.cy = rcWin.Height ();

				SetScrollSizes (MM_TEXT, m_ScrollSizes);

				if (ptScroll == CPoint (0, 0)) {
					CString strKey = CControlApp::GetRegKey () + _T ("\\Settings\\ScrollPosition");

					ptScroll.x = FoxjetDatabase::GetProfileInt (strKey, _T ("x"), 0);
					ptScroll.y = FoxjetDatabase::GetProfileInt (strKey, _T ("y"), -1);

					if (ptScroll.y == -1) 
						ptScroll.y = 0;//GetTotalSize ().cy;
				}

				CSize sizeTotal = GetTotalSize ();

				ptScroll.x = min (ptScroll.x, sizeTotal.cx);
				ptScroll.y = min (ptScroll.y, sizeTotal.cy);

				/*
				#ifdef _DEBUG
				{
					CString str;
					CRect rc (0, 0, 0, 0);

					GetWindowRect (rc);

					str.Format (_T ("m_rcPanel: %d, %d (%d, %d), ds: %d, %d, m_dZoom: %f, m_nMargin: %d, m_ScrollSizes: %d, %d, ptScroll: %d, %d, GetTotalSize: %d, %d, GetWindowRect: %d, %d"),
						m_rcPanel.left, m_rcPanel.top, m_rcPanel.Width(), m_rcPanel.Height (),
						ds.dsBm.bmWidth, ds.dsBm.bmHeight,
						m_dZoom, m_nMargin,
						m_ScrollSizes.cx, m_ScrollSizes.cy,
						ptScroll.x, ptScroll.y,
						GetTotalSize ().cx, GetTotalSize ().cy,
						rc.Width (), rc.Height ());
					TRACEF (str);
				}
				#endif
				*/

				ScrollToPosition (ptScroll);
			}
		}
	}

	//Invalidate ();
	//RedrawWindow ();
	REPAINT (this);
}

void CImageView::SetActiveDocument(CControlDoc *pDoc)
{
	m_pDoc = pDoc;
}

void CImageView::OnDestroy() 
{
	if ( m_pPreviewBitmap )
	{
		delete m_pPreviewBitmap;
		m_pPreviewBitmap = NULL;
	}

	if ( m_pPreviewDC )
	{
		m_pPreviewDC->DeleteDC();
		delete m_pPreviewDC;
		m_pPreviewDC = NULL;
	}

	CScrollView::OnDestroy();
}

void CImageView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CRect rc;

	CScrollView::OnLButtonDown(nFlags, point);

	{ CString str; str.Format (_T ("point: %d, %d, pos: %d, %d [%d, %d]"), point.x, point.y, GetScrollPosition ().x, GetScrollPosition ().y, GetDeviceScrollPosition ().x, GetDeviceScrollPosition ().y); TRACEF (str); }

	if (m_ptDrag) 
		delete m_ptDrag;

	if (m_ptScroll)
		delete m_ptScroll;

	/*
	if (m_rcZoomIn.PtInRect (point)) {
		SetZoom (GetZoom () + m_dZoomIncrement);
		m_pDoc->GetView ()->PostMessage (WM_COMMAND, MAKEWPARAM (ID_VIEW_REFRESHPREVIEW, 0));
	}
	else if (m_rcZoomOut.PtInRect (point)) {
		SetZoom (GetZoom () - m_dZoomIncrement);
		m_pDoc->GetView ()->PostMessage (WM_COMMAND, MAKEWPARAM (ID_VIEW_REFRESHPREVIEW, 0));
	}
	else 
	*/
	{
		m_ptDrag = new CPoint (point);
		m_ptScroll = new CPoint (GetScrollPosition ());

		GetClientRect (rc);
		ClientToScreen (rc);
		::ClipCursor (rc);
	}
}

void CImageView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if (m_ptDrag) {
		delete m_ptDrag;
		m_ptDrag = NULL;
	}

	if (m_ptScroll) {
		delete m_ptScroll;
		m_ptScroll = NULL;
	}

	::ClipCursor (NULL);
	REPAINT (this); 

	CScrollView::OnLButtonUp(nFlags, point);
}

void CImageView::OnMouseMove(UINT nFlags, CPoint point) 
{
	CScrollView::OnMouseMove(nFlags, point);

	//if (!m_bZoomToFit) 
	{
		if (m_ptDrag && m_ptScroll) {
			CPoint pt = point - (* m_ptDrag);
			CPoint ptScroll = (* m_ptScroll) - pt;
			CRect rcInvalid (m_rcZoom);

			rcInvalid.UnionRect (rcInvalid, m_rcZoom - ptScroll);
			rcInvalid.UnionRect (rcInvalid, m_rcZoom + ptScroll);
			rcInvalid.UnionRect (rcInvalid, m_rcError + ptScroll);

			rcInvalid.InflateRect (5, 5);

			ptScroll.x = BindTo ((int)ptScroll.x, 0, GetScrollLimit (SB_HORZ));
			ptScroll.y = BindTo ((int)ptScroll.y, 0, GetScrollLimit (SB_VERT));

			if (ptScroll.x < 0 || ptScroll.x > GetScrollLimit (SB_HORZ) || ptScroll.y < 0 || ptScroll.y > GetScrollLimit (SB_VERT)) {
				TRACEF ("out of bounds");
			}
			else {
				//{ CString str; str.Format (_T ("(%-3d, %-3d)"), ptScroll.x, ptScroll.y); TRACEF (str); }
				ScrollToPosition (ptScroll);
				InvalidateRect (rcInvalid);
			}
		}
	}
}

void CImageView::OnHeadConfigUpdated ()
{
	DBCONNECTION (conn);
	m_vPanels.RemoveAll ();
	CPanel::GetPanels (theApp.m_Database, m_lLineID, m_vPanels);

	if (m_lLineID != FoxjetDatabase::NOTFOUND)
		ASSERT (m_vPanels.GetSize () == 6);
}

void CImageView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	CScrollView::OnUpdate (pSender, lHint, pHint);
}

bool CImageView::SetZoom (double dZoom, bool bSnap)
{
	const int nSnap = (int)(m_dZoomIncrement * 100.0);

	if (bSnap)
		dZoom = ((double)((int)(dZoom * 100) / nSnap) * (double)nSnap) / 100.0;

	if (dZoom >= CImageView::m_dZoomMin && dZoom <= CImageView::m_dZoomMax) {
		m_dZoom = dZoom;
		FoxjetDatabase::WriteProfileInt (CControlApp::GetRegKey () + _T ("\\Settings"), _T ("Zoom"), (int)(m_dZoom * 100.0));
		m_bZoomToFit = false;
		return true;
	}

	return false;
}

double CImageView::GetZoom () const
{
	return m_dZoom;
}

void CImageView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CScrollView::OnVScroll(nSBCode, nPos, pScrollBar);
	REPAINT (this); 
}

void CImageView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CScrollView::OnHScroll(nSBCode, nPos, pScrollBar);
	REPAINT (this); 
}

BOOL CImageView::OnEraseBkgnd(CDC* pDC) 
{
	return TRUE;//CScrollView::OnEraseBkgnd(pDC);
}

int CImageView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

BOOL CImageView::PreCreateWindow(CREATESTRUCT& cs) 
{
	return CScrollView::PreCreateWindow(cs);
}

double CImageView::CalcZoomFit ()
{
	CRect rc (0, 0, 0, 0);
	double dZoom = GetZoom ();
	int cx = m_rcPanel.Width ();
	int cy = m_rcPanel.Height ();

	GetWindowRect (rc);
	rc.DeflateRect (m_nMargin, m_nMargin);
	//cx += (int)ceil ((m_nMargin * 4) * dZoom);
	//cy += (int)ceil ((m_nMargin * 4)* dZoom);

	return min (100.0, min ((double)rc.Width () / (double)cx, (double)rc.Height () / (double)cy));
}

void CImageView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	if (m_pDoc)
		if (CControlView * pView = m_pDoc->GetView ())
			pView->PostMessage (WM_COMMAND, MAKEWPARAM (ID_VIEW_REFRESHPREVIEW, 0));
	/*
	double dZoom = GetZoom ();
	double dFit = CalcZoomFit ();
	int nZoom = (int)(dZoom * 100);
	int nFit = (int)(dFit * 100);
	CPoint ptScroll = GetScrollPosition ();

	CScrollView::OnLButtonDblClk(nFlags, point);

	if (nZoom == nFit) {
		SetZoom (1.00);
		double dDiff = GetZoom () - dZoom;
		TRACEF (_T ("diff: ") + ToString (dDiff));

		CPoint ptNew ((int)(point.x / dDiff), (int)(point.y / dDiff));
		CPoint ptDiff = ptNew - point;

		ptScroll.x += (int)(point.x * dDiff / dZoom);
		ptScroll.y += (int)(point.y * dDiff / dZoom);
	}
	else {
		SetZoom (dFit);
		ptScroll = CPoint (0, 0);
	}

	m_pDoc->GetView ()->SendMessage (WM_COMMAND, MAKEWPARAM (ID_VIEW_REFRESHPREVIEW, 0));
	ScrollToPosition (ptScroll);
	*/
}

void CImageView::OnSize(UINT nType, int cx, int cy) 
{
	EnableScrollBarCtrl(SB_BOTH, FALSE);
	CScrollView::OnSize(nType, cx, cy);
}
