#pragma once

#include "stdafx.h"
#include <memory>
#include <vector>
#include <string>

template <class T>
class CTemplateBuffer : public std::unique_ptr <T>
{
public:
	CTemplateBuffer(T * p, int nLen, bool bAllocate = false) 
		: m_nLen(nLen)
	{
		if (bAllocate) {
			unsigned char * pNew = new unsigned char[m_nLen];
			memcpy(pNew, p, m_nLen);
			reset (pNew);
		}
		else
			reset (p);
	}

	CTemplateBuffer(const std::string & str = "")
		: m_nLen(str.length())
	{
		unsigned char * p = new unsigned char[m_nLen];
		memcpy(p, str.c_str(), m_nLen);
		reset(p);
	}

	int size() const { return m_nLen; }

	CTemplateBuffer<T>(const CTemplateBuffer<T> & rhs)
		: m_nLen(rhs.m_nLen)
	{
		T * p = new T[rhs.m_nLen];
		memcpy(p, rhs.get(), rhs.size());
		reset(p);
	}

	CTemplateBuffer<T> & operator = (const CTemplateBuffer<T>  & rhs)
	{
		if (this != &rhs) {
			T * p = new T[rhs.m_nLen];
			m_nLen = rhs.m_nLen;
			memcpy(p, rhs.get(), rhs.size());
			reset(p);
		}

		return *this;
	}

	CTemplateBuffer <T> & operator += (const CTemplateBuffer <T> & rhs)
	{
		operator= (*this + rhs);
		return *this;
	}

	std::string ToString() const
	{
		return "[" + std::to_string ((__int64)m_nLen) + " bytes]: " + to_string ((const char *)get (), min (256, size ())); 
	}

	operator std::string () const { return ToString (); }

private:
	size_t m_nLen;
};

typedef CTemplateBuffer<unsigned char> CBuffer;

CBuffer encode(const std::vector <unsigned char> & v);
CBuffer encode(const std::string & str);
CBuffer operator+(const CBuffer & lhs, const CBuffer & rhs);