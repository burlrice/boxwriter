#if !defined(AFX_GROUPOPTIONSDLG_H__922B0381_C14E_11D5_8F07_00045A47C31C__INCLUDED_)
#define AFX_GROUPOPTIONSDLG_H__922B0381_C14E_11D5_8F07_00045A47C31C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GroupOptionsDlg.h : header file
//
#include "Database.h"
#include "RoundButtons.h"
#include "ListCtrlImp.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CGroupOptionsDlg dialog

namespace GroupOptionsDlg
{

	class CGroupItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		virtual CString GetDispText (int nColumn) const;

		FoxjetDatabase::SECURITYGROUPSTRUCT m_s;
	};

	class COptionsItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		virtual CString GetDispText (int nColumn) const;

		FoxjetDatabase::OPTIONSSTRUCT m_s;
	};

	class CGroupOptionsDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CGroupOptionsDlg(CWnd* pParent = NULL);   // standard constructor
		virtual ~CGroupOptionsDlg ();

		ItiLibrary::ListCtrlImp::CListCtrlImp	m_lvOptions;
		ItiLibrary::ListCtrlImp::CListCtrlImp	m_lvGroups;

	// Dialog Data
		//{{AFX_DATA(CGroupOptionsDlg)
		CTreeCtrl	m_GroupOptions;
		//}}AFX_DATA


	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CGroupOptionsDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		CMap <DWORD, DWORD, int, int> m_OptionsToIndex;
		void LoadOptionAssignments (void);
		CArray <FoxjetDatabase::OPTIONSLINKSTRUCT, FoxjetDatabase::OPTIONSLINKSTRUCT &>m_Links;
		void LoadOptionsList (void);
		void LoadGroups (void);
		FoxjetUtils::CRoundButtons m_buttons;

		// Generated message map functions
		//{{AFX_MSG(CGroupOptionsDlg)
		virtual BOOL OnInitDialog();
		virtual void OnOK();
		afx_msg void OnEndLabelEditGroups(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnClickOptions(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnItemchangedOptions(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnItemchangedGroups(NMHDR* pNMHDR, LRESULT* pResult);
		//}}AFX_MSG

		afx_msg LRESULT OnUpdateOptionsTable (WPARAM wParam, LPARAM lParam);

		DECLARE_MESSAGE_MAP()
	};
}; //namespace GroupOptionsDlg

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GROUPOPTIONSDLG_H__922B0381_C14E_11D5_8F07_00045A47C31C__INCLUDED_)
