#include "stdafx.h"
#include "ExportImpl.h"
#include "Export.h"
#include "Debug.h"
#include "FieldDefs.h"
#include "Serialize.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;
using namespace FoxjetCommon;

CExport::CExport ()
:	m_bFinished (false),
	m_hThread (NULL)
{
}

CExport::~CExport ()
{
	if (m_hThread)
		::WaitForSingleObject (m_hThread, INFINITE);

	LOCK (m_cs);
	m_v.RemoveAll ();
}

void CExport::Build (COdbcDatabase & db)
{
	if (!m_hThread) {
		DWORD dw = 0;

		m_bFinished = false;
		m_pDB = &db;

		m_hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)ThreadFunc, (LPVOID)this, 0, &dw);
		VERIFY (m_hThread);
	}
}

bool CExport::IsFinished () const
{
	return m_bFinished;
}

int CExport::GetSize ()
{
	int nSize = 0;
	LOCK (m_cs);
	nSize = m_v.GetSize ();
	return nSize;
}

CString CExport::GetNext ()
{
	CString str;
	LOCK (m_cs);

	if (m_v.GetSize ()) {
		str = m_v [0];
		m_v.RemoveAt (0);
	}

	return str;
}

ULONG CALLBACK CExport::ThreadFunc (LPVOID lpData)
{
	TRACEF ("CExport::ThreadFunc starting");

	ASSERT (lpData);
	CExport & e = * (CExport *)lpData;
	COdbcDatabase & db = * e.m_pDB;
	CStringArray & v = e.m_v;

	LOCK (e.m_cs);

	e.m_bFinished = false;

	{
		DBMANAGETHREADSTATE (db);

		v.RemoveAll ();

		GETRECORDS (db, LINESTRUCT,				Lines::m_lpszTable,				v);
		GETRECORDS (db, BOXSTRUCT,				Boxes::m_lpszTable,				v);
		GETRECORDS (db, BOXLINKSTRUCT,			BoxToLine::m_lpszTable,			v);
		GETRECORDS (db, TASKSTRUCT,				Tasks::m_lpszTable,				v);	
		GETRECORDS (db, HEADSTRUCT,				Heads::m_lpszTable,				v);	
		GETRECORDS (db, PANELSTRUCT,			Panels::m_lpszTable,			v);
		GETRECORDS (db, OPTIONSSTRUCT,			Options::m_lpszTable,			v);
		GETRECORDS (db, OPTIONSLINKSTRUCT,		OptionsToGroup::m_lpszTable,	v);
		GETRECORDS (db, USERSTRUCT,				Users::m_lpszTable,				v);	
		GETRECORDS (db, SECURITYGROUPSTRUCT,	SecurityGroups::m_lpszTable,	v);
		GETRECORDS (db, SHIFTSTRUCT,			Shifts::m_lpszTable,			v);
		GETRECORDS (db, SETTINGSSTRUCT,			Settings::m_lpszTable,			v);
		GETRECORDS (db, IMAGESTRUCT,			Images::m_lpszTable,			v);

		//GETRECORDS (db, REPORTSTRUCT,			Reports::m_lpszTable,			v);
		//GETRECORDS (db, BCSCANSTRUCT,			BarcodeScans::m_lpszTable,		v);
		//GETRECORDS (db, DELETEDSTRUCT,		Deleted::m_lpszTable,			v);

		#ifdef _DEBUG
		{
			CString str;

			str.Format (_T ("CExport::ThreadFunc: %d strings"), v.GetSize ());
			TRACEF (str);
		}
		#endif //_DEBUG

		e.m_bFinished = true;
	}

	e.m_hThread = NULL;
	TRACEF ("CExport::ThreadFunc exiting");

	return 0;
}

