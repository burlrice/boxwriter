#include "stdafx.h"
#include "Control.h"
#include "MainFrm.h"
#include "Resource.h"
#include <afxmt.h>
#include "memory.h"
#include "mbstring.h"

#include "Defines.h"
#include "ClientSock.h"
#include "Endian.h"
#include "fj_socket.h"
#include "Database.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClientSock

CClientSock::CClientSock(DWORD dwThreadID)
	:hPhotoEvent(INVALID_HANDLE_VALUE), holdBuf(NULL), 
	holdBufLen(0), m_bMessageTransfered(false), m_bConnected(false), m_nPacketState(0)
{
	VERIFY (dwThreadID != 0);
	m_nThreadID = dwThreadID;
	m_pRxBuffer = new unsigned char[RX_BUFFER_SIZE];
	ConnectTime = CTime::GetCurrentTime();
}

CClientSock::~CClientSock()
{
	Close();
	_SOCKET_MESSAGE *pMsg;
	if ( holdBuf )
		delete holdBuf;
	
	if (m_pRxBuffer)
		delete m_pRxBuffer;

	while ( !MessageCue.IsEmpty() ) {
		pMsg = ( _SOCKET_MESSAGE * ) MessageCue.RemoveHead();
		if ( pMsg->pData )
			delete pMsg->pData;
		delete pMsg;
		pMsg = NULL;
	}

	while ( !m_RxCue.IsEmpty() ) {
		pMsg = ( _SOCKET_MESSAGE * ) m_RxCue.RemoveHead();
		if ( pMsg->pData )
			delete pMsg->pData;
		delete pMsg;
		pMsg = NULL;
	}
}

// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CClientSock, CSocket)
	//{{AFX_MSG_MAP(CClientSock)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

void CClientSock::Initialize()
{
	BOOL bKeepAlive = TRUE;
	SetSockOpt(SO_KEEPALIVE, &bKeepAlive, sizeof(BOOL));
}

void CClientSock::OnReceive(int nErrorCode) 
{
	int result;
	
	result = Receive(m_pRxBuffer, RX_BUFFER_SIZE);

	if (result != SOCKET_ERROR) {
		Extract_Message(m_pRxBuffer, result);
		ProcessMessages();
	}
	CSocket::OnReceive(nErrorCode);
}

int CClientSock::SendPacket (unsigned int Cmd, long BatchID, const void* data, unsigned long Len)
{
	LOCK (m_csSendLock);
	int result = 0;
	int nTotalByteLen;
	unsigned char *pTxBuffer, *p;
	_SOCKET_MESSAGE sm;

	sm.Header.Cmd = Cmd;
	sm.Header.BatchID = BatchID;
	sm.Header.Length = Len;

	nTotalByteLen = sm.Header.Length + sizeof (_SOCKET_MESSAGE_HEADER);

	try {
		pTxBuffer = new unsigned char [nTotalByteLen +1];
		memset ( pTxBuffer, 0x00, nTotalByteLen +1);
		CEndian::hton (sm); // Convert to Big-endian (Internet format) before sending.
		memcpy ( pTxBuffer, &sm, sizeof (_SOCKET_MESSAGE_HEADER));
		
		p = pTxBuffer + sizeof (_SOCKET_MESSAGE_HEADER);
		if ( data ) 
			memcpy (p, data, Len);
		
		if ( SendTo ( pTxBuffer, nTotalByteLen, m_nPort, m_sAddress) == SOCKET_ERROR) 
			m_bConnected = false;
		else
			result = 1;
		delete pTxBuffer;
	}
	catch (CMemoryException *e) { HANDLEEXCEPTION (e); }

	return result;
}

void CClientSock::Extract_Message(unsigned char * pMsg, unsigned pMsgLength)
{
	const int nPacketHeaderSize = sizeof (_SOCKET_MESSAGE_HEADER);
	bool done = false;
	unsigned msgLen = pMsgLength;
	PUCHAR pData, p;
	_SOCKET_MESSAGE *pSM = NULL;

	if ( holdBufLen ) {
		PUCHAR pNew = new UCHAR [msgLen + holdBufLen];
		memcpy (pNew, holdBuf, holdBufLen);
		pData = pNew + holdBufLen;
		memcpy (pData, pMsg, pMsgLength);
		msgLen += holdBufLen;
		delete holdBuf;
		holdBuf = NULL;
		holdBufLen = 0;
		pData = pNew;
	}
	else {
		pData = new UCHAR[pMsgLength];
		memcpy (pData, pMsg, pMsgLength);
	}

	p = pData;
	while ( !done ) 
	{
		switch (m_nPacketState) {
		case 0:	// Default state:  No packet header recieved yet
			if ( msgLen >= nPacketHeaderSize ) {
				m_nPacketState = 1;
				memset (&tmpSm, 0x00, sizeof (_SOCKET_MESSAGE));
				memcpy (&tmpSm, p, nPacketHeaderSize);
				CEndian::ntoh (tmpSm); // Convert from (Big-Endian Internet Format)
				p += nPacketHeaderSize;
				msgLen -= nPacketHeaderSize;
			}
			else {
				done = true;
				break;
			}
	//		break;	// Fall Through intentional.
		case 1:
			if ( !done ) {
				if ( msgLen >= (tmpSm.Header.Length) ) { // If we have recieved enough for the whole packet.
					try {
						tmpSm.pData = new UCHAR[tmpSm.Header.Length+1];
						memset ( tmpSm.pData, 0x00, (tmpSm.Header.Length+1) );
						memcpy ( tmpSm.pData, p, (tmpSm.Header.Length) );
						pSM = new _SOCKET_MESSAGE;
						memcpy ( pSM, &tmpSm, sizeof (_SOCKET_MESSAGE) );
						MessageCue.AddTail( pSM );
					}
					catch (CMemoryException *e)  { HANDLEEXCEPTION (e); }
					p += (tmpSm.Header.Length);
					msgLen -= (tmpSm.Header.Length);
					m_nPacketState = 0;
				}
				else {
					done = true;
					break;
				}
			}
			break;
		}
	}

	if ( msgLen != 0 ) {
		holdBuf = new PUCHAR [msgLen];
		memcpy (holdBuf, p, msgLen);
		delete pData;
		holdBufLen = msgLen;
	}
	else
		delete pData;
}

void CClientSock::ProcessMessages()
{
	CString sLog;

	_SOCKET_MESSAGE *Message;
	while ( !MessageCue.IsEmpty() )
	{
		Message = ( _SOCKET_MESSAGE * )MessageCue.RemoveHead();
		CueRxData (Message);
	}
	while ( !PostThreadMessage (m_nThreadID, TM_RX_SOCKET_DATA, 0, 0L)) Sleep(0);
}

CTimeSpan CClientSock::GetConnectedTime()
{
	CTime CurrentTime = CTime::GetCurrentTime();
	CTimeSpan ElaspedTime = CurrentTime - ConnectTime;
	return ElaspedTime;
}


CString & CClientSock::GetNetworkName()
{
	IN_ADDR n;
	DWORD dwAddress;
	m_sNetworkName.Empty();
	if ( m_sAddress.IsEmpty() )
		m_sAddress = GetAddress();

	dwAddress = inet_addr (w2a (m_sAddress));
	n.S_un.S_addr = htonl(dwAddress);
	HOSTENT *h = gethostbyaddr ((const char *)&dwAddress, 4, AF_INET);

	if ( h ) {
		m_sNetworkName = h->h_name;
		m_sNetworkName.Replace('.', 0x00);
		m_sNetworkName.MakeUpper();
	}
	
	return m_sNetworkName;
}

CString & CClientSock::GetAddress()
{
	if ( m_sAddress.IsEmpty() )
		GetPeerName (m_sAddress, m_nPort);
	return m_sAddress;
}

unsigned int CClientSock::GetSocketID()
{
	return nSocketId;
}


_SOCKET_MESSAGE *CClientSock::GetRxData (void)
{
	_SOCKET_MESSAGE *pMsg = NULL;
	LOCK (m_RxCueSentinel);
	{
		if ( !m_RxCue.IsEmpty() )
			pMsg = ( _SOCKET_MESSAGE *)m_RxCue.RemoveHead();
	}	
	return pMsg;
}


void CClientSock::CueRxData(_SOCKET_MESSAGE *pMsg)
{
	LOCK (m_RxCueSentinel);
	m_RxCue.AddTail(pMsg);
}

