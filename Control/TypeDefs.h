#if !defined (__TYPEDEFS_H__81B0AC6C_B4E1_4651_B799_56D20919806D)
	#define __TYPEDEFS_H__81B0AC6C_B4E1_4651_B799_56D20919806D
	
	#include <afxtempl.h>
	#include "Defines.h"
	#include "AppVer.h"
	#include "DbTypeDefs.h"

	typedef CMap <DWORD, DWORD, void*, void*> CPrintHeadList;
	typedef CMap <DWORD, DWORD, bool, bool> CSecurityOptionsMap;

	// The order of these is important, always add to the bottom.
	typedef enum {
		NOTUSED		= -1,
		HANDSCANNER = 0,
		FIXEDSCANNER,
		REMOTEDEVICE,
		HOSTINTERFACE,
		SW0858_DBSTART,
	}eSerialDevice;

	typedef enum {
		SERIALSRC_MPHC,
		SERIALSRC_HUB,
		SERIALSRC_WIN32COMM,
	}eSerialSource;

	// Error codes are 32-bit values (bit 31 is the most significant bit). 
	// Bit 29 is reserved for application-defined error codes; no system error code has this bit set. 
	// If you are defining an error code for your application, set this bit to indicate that the error 
	// code has been defined by the application and to ensure that your error code does not conflict 
	// with any system-defined error codes. 
	enum USER_DEFINED_ERRORS 
	{
		START_EDIT_SESSION_FAILED_NO_RESPONCE = 0x40000000,
		START_EDIT_SESSION_FAILED_TIMEOUT,
		START_EDIT_SESSION_FAILED_SEND,
		START_EDIT_SESSION_LOCKED,
		SAVE_EDIT_SESSION_FAILED,
		TRANSFER_FAILED,
		BITMAP_TRANSFER_FAILED,
		FONT_TRANSFER_FAILED,
		FIRMWARE_UPDATE_FAILED,
		REBOOT_PRINTER,
		INVALID_PARAM,
		CLAIM_ADDRESS_FAILED,
		FPGA_PROGRAMMING_ERROR,
		RETREIVE_LABELNAMES_FALIED,
		DDE_ADVSTART_FALIED,
		DDE_CONNECT_FALIED,
		GA_UPDATE_FAILED,
	};

	enum PROCESSING_STATES
	{
		UPDATING_FINISHED,
		UPDATING_MESSAGES,
		UPDATING_FONTS,
		STARTING_MESSAGE,
		LABEL_CLEANUP,
		WAITING_FOR_SAVE_ACK,
		VERIFYING_IP_DATA,
		LOCAL_HEAD_INITIALIZING,
		FIRMWARE_UPDATE,
		STARTING_NICELABEL,
		GA_UPDATE,
		UPDATING_STATUS_MSG,
		ODBC_STATUS_DISPLAY,
		ODBC_STATUS_FINISHED,
		UPDATING_CONFIG,
	};

	typedef struct tagSerialData
	{
		UINT m_Len;
		TCHAR Data[COM_BUF_SIZE];
	}_SERIALDATA;


	typedef struct tagRmtSerialPort {
		ULONG m_lLineID;			// Production Line Association.
		eSerialDevice m_Device;		// Device attached to port.
		ULONG m_lBaud;				// Baud Rate
		char m_cParity;				// 'N', 'O', 'E'
		int m_nDatabits;			// Data Bits
		int m_nStopBits;			// Stop Bits
	}_RMT_SERIAL_PORT;

	typedef struct tagSettings {
		BYTE m_cIP[4];
		tagRmtSerialPort m_SerialPort[4];
		ULONG m_Strobe[2];		// Contains LineID for strobe;
		BOOL m_bEnabled;
	}_SETTINGS;

	typedef enum HEAD_INTERFACE_MODE {
		LOCAL,
		NETWORK
	}eHeadInfModes;

	enum UPDATEMODES {
		UPDATE_HEAD_ACTIVATION =1,
		UPDATE_HEAD_DISCONNECT,
		UPDATE_HEAD_STATUS,
		UPDATE_LINE_CHANGE,
		UPDATE_PREVIEW,
	};

	enum LABEL_UPDATE_MODES 
	{
		UPDATE_MODIFIED_ONLY = 0,
		UPDATE_TASK_START,
		UPDATE_ALL
	};

	enum HOST_COMMANDS 
	{
		HC_GET_STATUS,
		HC_PUT_STATUS,
		HC_GET_LINE_INFO,
		HC_PUT_LINE_INFO,
		HC_GET_HEAD_INFO,
		HC_PUT_HEAD_INFO,
	};

	typedef enum HEADSTATUS 
	{
		OK						= 0x0000,
		LOW_VOLTAGE				= 0x0001,
		LOW_TEMP				= 0x0002,
		LOW_INK					= 0x0004,
		INK_OUT					= 0x0008,
		IV_BROKENLINE			= 0x0010,
		IV_DISCONNECTED			= 0x0020,
		HP_COMMPORTFAILURE		= 0x0040,
	}eHeadStatus;

	typedef struct tagINKCODE
	{
		tagINKCODE () : m_bEnabled (false), m_lUsed (0), m_lMax (0), m_lHeadID (-1) { }

		bool				m_bEnabled;
		unsigned __int64	m_lUsed;
		unsigned __int64	m_lMax;
		ULONG				m_lHeadID;
		CString				m_strCode;
	} INKCODE;

	typedef struct tagSTATUS
	{
		bool	m_bAtTemp;			// Print Head is at temp
		bool	m_bVoltageOk;		// Print head voltage is ok
		BYTE	m_nInkLevel;		// Current ink level [see HEADSTATUS]
		__int64	m_lCount;
		__int64	m_lLastCount;		// used by CHead::CheckState
		__int64	m_lCountUntil;
		double	m_dLineSpeed;
		ULONG	m_lIntr;
		bool	m_bFPGA;

		bool	m_bBrokenLine;
		bool	m_bIDSConnected;
		bool	m_bEp8Error;

		#if __CUSTOM__ == __SW0828__
		ULONG	m_lLifetimeCount;
		ULONG	m_lTotalBoxCount;
		#endif

	}_STATUS;

	typedef struct
	{
		WORD wStartCol;			// Image Start Start Column
		WORD wEndCol;			// Image End Column
		BYTE nBPC;
		ULONG lLen;
		WORD wCurrentCol;
		LARGE_INTEGER tm;
	}tagImgUpdateDesc;                             

	typedef struct	tagIMAGE_HDR_PKT{
		ULONG istart;		// Image Start              
		ULONG ilen;			// Image Length              
		UINT  ibpc;			// Image Bytes Per Column  
		ULONG ippc;			// Image Pixels Per Column  
		ULONG irow;			// Image Base Row (pixels)   // ? is this the starting nozzle?
	}_IMAGE_HDR_PKT;                             

	typedef struct {
		ULONG  Cmd;				// NetworkCmd value
		ULONG  BatchID;         // this stays the same for all packets that are part of one command. also used for command response.
		ULONG  Length;          // length of Data
	}_SOCKET_MESSAGE_HEADER;

	typedef struct {
		_SOCKET_MESSAGE_HEADER Header;
//		BYTE Data[SOCKET_BUFFER_SIZE];
		BYTE *pData;
	}_SOCKET_MESSAGE;

#endif