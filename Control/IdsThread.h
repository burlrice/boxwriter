#if !defined(AFX_IDSTHREAD_H__D6486F7A_46EF_4E34_84CD_666C0FE0A909__INCLUDED_)
#define AFX_IDSTHREAD_H__D6486F7A_46EF_4E34_84CD_666C0FE0A909__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IdsThread.h : header file
//

#include "IdsSocket.h"
#include "TypeDefs.h"
#include <afxmt.h>

typedef struct tagSTATUSSTRUCT
{
	tagSTATUSSTRUCT ()
	:	m_inkLevel (OK),
		m_bBroken (false),
		m_wPressure (0),
		m_wVaccum (0),
		m_bConnected (true)
	{
	}

	eHeadStatus m_inkLevel;
	bool		m_bBroken;
	SHORT		m_wPressure;
	SHORT		m_wVaccum;
	bool		m_bConnected;
} STATUSSTRUCT;


/////////////////////////////////////////////////////////////////////////////
// CIdsThread thread

class CIdsThread : public CWinThread
{
	DECLARE_DYNCREATE(CIdsThread)
protected:
	CIdsThread();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:

	void UpdateStatus ();
	void GetStatus (STATUSSTRUCT & status, int nTimeoutSeconds = 30);
	void LoadAddress (FoxjetDatabase::COdbcDatabase & db);

	CDiagnosticCriticalSection	m_cs;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIdsThread)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CIdsThread();

	CIdsSocket		m_sock;
	STATUSSTRUCT	m_status;
	CString			m_strAddress;
	CTime			m_tmLast;
	int				m_nTimeoutSeconds;

	// Generated message map functions
	//{{AFX_MSG(CIdsThread)
	afx_msg void OnKillThread (WPARAM wParam, LPARAM lParam);
	afx_msg void OnStatus (WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IDSTHREAD_H__D6486F7A_46EF_4E34_84CD_666C0FE0A909__INCLUDED_)
