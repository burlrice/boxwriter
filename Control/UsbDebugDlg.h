#if !defined(AFX_USBDEBUGDLG_H__4D58111C_7F08_4528_B9AA_CBF9EC91F40A__INCLUDED_)
#define AFX_USBDEBUGDLG_H__4D58111C_7F08_4528_B9AA_CBF9EC91F40A__INCLUDED_

#include "ListCtrlImp.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UsbDebugDlg.h : header file
//

#include "resource.h"
#include "Utils.h"

class CLocalHead;

/////////////////////////////////////////////////////////////////////////////
// CUsbDebugDlg dialog

namespace UsbDebugDlg
{
	class CRegisterItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CRegisterItem () { } 
		virtual ~CRegisterItem () { }

		virtual CString GetDispText (int nColumn) const;

		int m_nIndex;
		WORD m_w;
	};

	class CUsbDebugDlg : public FoxjetCommon::CEliteDlg
	{

	// Construction
	public:
		CUsbDebugDlg(CLocalHead * pHead, CWnd* pParent = NULL);   // standard constructor
		virtual ~CUsbDebugDlg ();

	// Dialog Data
		//{{AFX_DATA(CUsbDebugDlg)
		enum { IDD = IDD_USB_DEBUG_DLG };
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA


	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CUsbDebugDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:

		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
		CLocalHead * m_pHead;

		// Generated message map functions
		//{{AFX_MSG(CUsbDebugDlg)
		afx_msg void OnBeginlabeleditData(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnRead();
		afx_msg void OnWrite();
		virtual BOOL OnInitDialog();
		afx_msg void OnEndlabeleditData(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnEdit();
		afx_msg void OnDblclkData(NMHDR* pNMHDR, LRESULT* pResult);
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; // namespace UsbDebugDlg

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USBDEBUGDLG_H__4D58111C_7F08_4528_B9AA_CBF9EC91F40A__INCLUDED_)
