// GroupOptionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "Control.h"
#include "Database.h"
#include "GroupOptionsDlg.h"
#include "Debug.h"
#include "MainFrm.h"
#include "Parse.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ItiLibrary;
using namespace ItiLibrary::ListCtrlImp;
using namespace FoxjetCommon;
using namespace GroupOptionsDlg;

static const UINT WM_UPDATE_OPTIONS_TABLE = ::RegisterWindowMessage (_T ("Foxjet::CGroupOptionsDlg::TM_UPDATE_OPTIONS_TABLE"));

CString CGroupItem::GetDispText (int nColumn) const
{
	return m_s.m_strName;
}

CString COptionsItem::GetDispText (int nColumn) const
{
	return m_s.m_strOptionDesc;
}

/////////////////////////////////////////////////////////////////////////////
// CGroupOptionsDlg dialog


CGroupOptionsDlg::CGroupOptionsDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg (IDD_GROUP_OPTIONS_MATRIX /* IsMatrix () ? IDD_GROUP_OPTIONS_MATRIX : IDD_GROUP_OPTIONS */ , pParent)
{
	//{{AFX_DATA_INIT(CGroupOptionsDlg)
	//}}AFX_DATA_INIT
}

CGroupOptionsDlg::~CGroupOptionsDlg ()
{
}

void CGroupOptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGroupOptionsDlg)
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
}


BEGIN_MESSAGE_MAP(CGroupOptionsDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CGroupOptionsDlg)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_GROUPS, OnEndLabelEditGroups)
	ON_NOTIFY(NM_CLICK, IDC_OPTIONS, OnClickOptions)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_GROUPS, OnItemchangedGroups)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_OPTIONS, OnItemchangedOptions)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE (WM_UPDATE_OPTIONS_TABLE, OnUpdateOptionsTable)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGroupOptionsDlg message handlers

BOOL CGroupOptionsDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();

	{
		CArray <CColumn, CColumn> vCols;

		vCols.Add (CColumn (LoadString (IDS_GROUP), 200));
		m_lvGroups.Create (IDC_GROUPS, vCols, this, ListGlobals::defElements.m_strRegSection);
		m_lvGroups->SetExtendedStyle (m_lvGroups->GetExtendedStyle()|LVS_EX_FULLROWSELECT);
	}

	{
		CArray <CColumn, CColumn> vCols;

		vCols.Add (CColumn (LoadString (IDS_OPTIONS), 400));
		m_lvOptions.Create (IDC_OPTIONS, vCols, this, ListGlobals::defElements.m_strRegSection);
		m_lvOptions->SetExtendedStyle (m_lvOptions->GetExtendedStyle()|LVS_EX_CHECKBOXES);
	}

	LoadGroups();
	LoadOptionsList();
	m_lvGroups->SetFocus();
	LoadOptionAssignments();
	m_lvGroups->SetSelectionMark (0);
	m_lvGroups->SetItemState(0, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGroupOptionsDlg::OnOK() 
{
	FoxjetCommon::CEliteDlg::OnOK();
}

void CGroupOptionsDlg::LoadGroups()
{
	FoxjetDatabase::SECURITYGROUPSTRUCT s;
	CArray <FoxjetDatabase::SECURITYGROUPSTRUCT, FoxjetDatabase::SECURITYGROUPSTRUCT &> v;
	
	FoxjetDatabase::GetGroupsRecords (theApp.m_Database, v);
	
	for (int i = 0; i < v.GetSize(); i++) {
		CGroupItem * p = new CGroupItem ();

		p->m_s = v [i];
		m_lvGroups.InsertCtrlData (p);
	}
}

void CGroupOptionsDlg::LoadOptionsList()
{
	using namespace FoxjetDatabase;

	CArray <OPTIONSSTRUCT, OPTIONSSTRUCT &> v;
	
	GetOptionsRecords (theApp.m_Database, v);

	for (int i = 0; i < v.GetSize (); i++) {
		OPTIONSSTRUCT o = v [i];
		CString str = CMainFrame::GetMenuPrompt (o);

		TRACEF (o.m_strOptionDesc + ": [" + str + "]");

		if (str.GetLength ()) {
			COptionsItem * p = new COptionsItem ();

			p->m_s = v [i];
			int nIndex = m_lvOptions.InsertCtrlData (p);
			m_OptionsToIndex.SetAt (v [i].m_lID, nIndex);
		}
	}
}

void CGroupOptionsDlg::OnEndLabelEditGroups(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	LVITEM lvItem = pDispInfo->item;
	
	if ( lvItem.mask | LVIF_TEXT ) {
		FoxjetDatabase::SECURITYGROUPSTRUCT s;
	
		//pGS = ( FoxjetDatabase::SECURITYGROUPSTRUCT * ) m_Groups.GetItemData (lvItem.iItem);

		if (pDispInfo->item.pszText && _tcslen(pDispInfo->item.pszText) > 0 ) {
			if (CGroupItem * p = (CGroupItem *)m_lvGroups.GetCtrlData (pDispInfo->item.iItem)) {
				p->m_s.m_strName = pDispInfo->item.pszText;
				FoxjetDatabase::UpdateGroupsRecord (theApp.m_Database, p->m_s);
				m_lvGroups.UpdateCtrlData (p);
			}
		}
	}

	*pResult = 0;
}

void CGroupOptionsDlg::LoadOptionAssignments()
{
	CLongArray sel = GetSelectedItems (m_lvGroups);

	if (sel.GetSize ()) {
		if (CGroupItem * p = (CGroupItem *)m_lvGroups.GetCtrlData (sel [0])) {
			m_Links.RemoveAll();
			FoxjetDatabase::GetOptionsLinks (theApp.m_Database, p->m_s.m_lID, m_Links);

			for (int i =0; i < m_lvOptions->GetItemCount(); i++ )
				m_lvOptions->SetCheck(i, false);

			for (int i = 0; i < m_Links.GetSize(); i++) {
				FoxjetDatabase::OPTIONSLINKSTRUCT link = m_Links.GetAt(i);

				for (int j = 0; j < m_lvOptions->GetItemCount (); j++) {
					if (COptionsItem * pOption = (COptionsItem *)m_lvOptions.GetCtrlData (j)) {
						if (pOption->m_s.m_lID == (UINT)link.m_lOptionsID) {
							m_lvOptions->SetCheck (j, true);
						}
					}
				}
			}
		}
	}
}

void CGroupOptionsDlg::OnItemchangedOptions(NMHDR* pNMHDR, LRESULT* pResult) 
{
}

void CGroupOptionsDlg::OnClickOptions (NMHDR* pNMHDR, LRESULT* pResult) 
{
	PostMessage (WM_UPDATE_OPTIONS_TABLE);
	
	if (pResult)
		* pResult = 0;
}

void CGroupOptionsDlg::OnItemchangedGroups(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	if (pNMListView->uChanged == 8)
		LoadOptionAssignments();

	if (pResult)
		* pResult = 0;
}

LRESULT CGroupOptionsDlg::OnUpdateOptionsTable (WPARAM wParam, LPARAM lParam) 
{
	CLongArray sel = GetSelectedItems (m_lvGroups);

	if (sel.GetSize ()) {
		if (CGroupItem * pGroup = (CGroupItem *)m_lvGroups.GetCtrlData (sel [0])) {
			m_Links.RemoveAll();

			for (int i = 0; i < m_lvOptions->GetItemCount(); i++ ) {
				if ( m_lvOptions->GetCheck(i) ) {
					if (COptionsItem * pOption = (COptionsItem *)m_lvOptions.GetCtrlData (i)) {
						FoxjetDatabase::OPTIONSLINKSTRUCT link;

						link.m_lGroupID = pGroup->m_s.m_lID;
						link.m_lOptionsID = pOption->m_s.m_lID;
						m_Links.Add (link);

						TRACEF (FoxjetDatabase::ToString (link.m_lGroupID) + _T (" --> ") + FoxjetDatabase::ToString (link.m_lOptionsID));
					}
				}
			}

			FoxjetDatabase::SetOptionsLinks (theApp.m_Database, pGroup->m_s.m_lID, m_Links);
		}
	}

	return 0;
}