// ImageEng.h: interface for the CImageEng class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IMAGEENG_H__48BEDF56_26A9_418B_A58A_2F57D543FC67__INCLUDED_)
#define AFX_IMAGEENG_H__48BEDF56_26A9_418B_A58A_2F57D543FC67__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "List.h"

using FoxjetCommon::INITIAL;
using FoxjetCommon::IMAGING;
#ifdef __WINPRINTER__
using FoxjetCommon::IMAGING_REFRESH;
#endif //__WINPRINTER__



class CImageEng
{
public:
	class CImageData
	{ 
	public:
		CImageData (ULONG lBmpHandle, const CRect & rc, ULONG lPixels, const CString & strData, FoxjetCommon::DRAWCONTEXT context);

		CTime	m_tm;
		ULONG	m_lBmpHandle;
		CRect	m_rc;
		ULONG	m_lPixels;
		CString m_strData;
		FoxjetCommon::DRAWCONTEXT m_context;
	};

	CImageEng();
	virtual ~CImageEng();

public:
	long double GetInkUsage () const { return m_dInkUsage; }
	unsigned __int64 GetPixelCount () const { return m_lPixelCount; }

	unsigned long GetImageLen (void);
	int GenImage (FoxjetCommon::CElementList *pElementList, 
		const FoxjetDatabase::HEADSTRUCT & HeadCfg, 
		void *pImageBuffer, unsigned long nImageBufferLen, FoxjetCommon::BUILDTYPE imgtype, CPtrList &UpdateList, 
		FoxjetCommon::DRAWCONTEXT Context,
		bool bPreviewOnly = false/* SW0828 */,
		Head::CHead * pHead = NULL);
	int GenImage(FoxjetCommon::CElementList * pList, const FoxjetDatabase::HEADSTRUCT & head, 
		Head::CHead * pThread);
	void Free ();

	CBitmap *	m_pImageBitmap;
	CBitmap *	m_pPreviewBitmap;
	CSize		m_productArea;
	//CDiagnosticCriticalSection m_csPreview;

protected:
	CSize GetMaxElementSize (FoxjetCommon::CElementList *pElementList, 
		const FoxjetDatabase::HEADSTRUCT & HeadCfg,
		void *pImageBuffer,
		unsigned long nImageBufferLen,
		FoxjetCommon::BUILDTYPE imgtype,
		CPtrList &UpdateList, 
		FoxjetCommon::DRAWCONTEXT Context,
		bool bPreviewOnly /* SW0828 */,
		Head::CHead * pThread,
		const Head::CHeadInfo & info);

	CDC 				m_dcTemp;
	CDC 				m_dcImage;
	CPtrList			m_vRectList;
	UINT				m_nImageLen;
	long double			m_dInkUsage;
	unsigned __int64	m_lPixelCount;
	ULONG				m_lEditorPixels;
	ULONG				m_lPrinterPixels;
};

#endif // !defined(AFX_IMAGEENG_H__48BEDF56_26A9_418B_A58A_2F57D543FC67__INCLUDED_)
