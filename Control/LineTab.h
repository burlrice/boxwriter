#if !defined(AFX_LINETAB_H__89FBD361_EC64_4333_A769_5F799E4D5F7F__INCLUDED_)
#define AFX_LINETAB_H__89FBD361_EC64_4333_A769_5F799E4D5F7F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LineTab.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLineTab window

class CLineTab : public CTabCtrl
{
// Construction
public:
	CLineTab();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLineTab)
	//}}AFX_VIRTUAL

	static void DrawTab (CDC & dc, COLORREF rgb, const CRect & rc, int nPen, int nRadius, bool bSelected, bool bMask);

	static int GetPenSize () { return 3; }
	static int GetRadius () { return 10; }
	static COLORREF GetLineColor ();

// Implementation
public:
	virtual ~CLineTab();

	// Generated message map functions
protected:

	//{{AFX_MSG(CLineTab)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnNcPaint();
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd( CDC* pDC );
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LINETAB_H__89FBD361_EC64_4333_A769_5F799E4D5F7F__INCLUDED_)
