// Control.h : main header file for the Control application
//

#if !defined(AFX_CONTROL_H__3855D467_9729_11D5_8F07_00045A47C31C__INCLUDED_)
#define AFX_CONTROL_H__3855D467_9729_11D5_8F07_00045A47C31C__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "OdbcDatabase.h"
#include "Defines.h"
#include "TypeDefs.h"

#include "extern.h"
#include "Database.h"
#include "Dde.h"
#include "ControlDoc.h"
#include "AppVer.h"

#ifdef __WINPRINTER__
	#include "Mphc.h"
	#include "IvPhc.h"
	#include "UsbMphcCtrl.h"
#endif

#include "IdsThread.h"
#include "Debug.h"
#include "DatabaseStartDlg.h"

#include "BitmapElement.h" //#if __CUSTOM__ == __SW0833__


// causes the WM_NCCALCSIZE message to be sent
#define NCINVALIDATE(p)	if (p) (p)->SetWindowPos (NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 


/////////////////////////////////////////////////////////////////////////////
// CControlApp:
// See Control.cpp for the implementation of this class
//

namespace GlobalFuncs
{
	void StandardizeIPAddress (CString &sAddress);
	bool IsIPAddress (CString &sAddress);
	LONG GetDllVersion(LPCTSTR lpszDllName);
	CString GetAppName ();
	int GetPhcChannels (const FoxjetDatabase::HEADSTRUCT & h);

	ULONG GetLineID (const CString & strLine);
	CString GetProfileString (const CString & strLine, const CString & strKey, const CString & strDef = _T (""));
	__int64 GetProfileInt (const CString & strLine, const CString & strKey, __int64 lDef = 0);
	void WriteProfileString (const CString & strLine, const CString & strKey, const CString & str);
	void WriteProfileInt (const CString & strLine, const CString & strKey, __int64 lValue);
	CString GetState (FoxjetDatabase::TASKSTATE state);
	FoxjetDatabase::TASKSTATE GetState (const CString & str);
	void UpdateSettings (const FoxjetDatabase::SETTINGSSTRUCT & s);

	#ifdef __IPPRINTER__
	bool IsEditSession (const FoxjetDatabase::HEADSTRUCT & h);
	void SetEditSession (const FoxjetDatabase::HEADSTRUCT & h, bool bLock);
	#endif

	HWND GetControlWnd ();
};

namespace GlobalVars
{
#ifdef  __IPPRINTER__
	extern INT StartEditSessionTimeout;
	extern INT SaveEditSessionTimeout;
	extern INT HeadResponseTimeout;
#endif

	extern FoxjetCommon::CVersion CurrentVersion;
	extern CString strTestPattern;
	extern CString strTask;
	extern CString strCount;
	extern CString strState;
	extern CString strDriverRegSection;
	extern CString strUserPrompted;
};

class CMainFrame;

typedef FoxjetCommon::CCopyArray <ULONG, ULONG> CULongArray;

class CDbConnection
{
public:
	CDbConnection (LPCTSTR lpszFile = _T (""), ULONG lLine = 0);
	virtual ~CDbConnection ();

	bool IsOpen () const;
	bool CDbConnection::OpenExclusive (CWnd * pParent);

protected:
	CString m_strFile;
	ULONG m_lLine;
	//HANDLE m_hLockThread;
	//DWORD m_dwLockThreadID;
};

class CDbContext
{
public:
	CDbContext (FoxjetDatabase::COdbcDatabase & db);
	virtual ~CDbContext ();

	const DWORD m_dwThreadID;
	FoxjetDatabase::COdbcDatabase & m_db;
};

#define DBCONNECTION(s) \
	CDbConnection (s) (_T (__FILE__), __LINE__); \
	\
	if (!conn.IsOpen ()) \
		return;

#define DBCONNECTION_WITH_RETURN(s,n) \
	CDbConnection (s) (_T (__FILE__), __LINE__); \
	\
	if (!conn.IsOpen ()) \
		return (n);

class CControlApp : public CWinApp
{

public:
	CString sConnect;
	CString sDatabase;
	CString sLicKey;
	FoxjetDatabase::COdbcDatabase m_Database;
	CSecurityOptionsMap m_SecurityTable;
	bool m_bRootUser;
	CString m_strUsername;

	CTime m_tmDatabase;
	CTime m_tmDatabaseOpened;
	UINT m_nDbTimeout;
	bool m_bDatabaseInUse;

	bool ReconnectDatabase ();
	void ColorsChanged ();

	static CString GetRegKey ();
	CULongArray GetDriverAddressList ();

	static bool IsPC ();

	bool IsNEXTEditSession () const { return m_bNEXTEditSession; }
	void SetNEXTEditSession (bool b) { m_bNEXTEditSession = b; UpdateUI (); }
	
	bool IsPrintReportLoggingEnabled ();
	bool IsScanReportLoggingEnabled ();

	void EnablePrintReportLogging (bool bEnable);
	void EnableScanReportLogging (bool bEnable);

	static void LoadDbStartParams ();

	FoxjetUtils::CDatabaseStartParams m_dbstart;

	bool IsContinuousCount () const { return m_bIsContinuousCount; }
	void SetContinuousCount (bool bValue) { m_bIsContinuousCount = bValue; }

	bool IsCaptureDebugImage () const { return m_bCaptureDebugImage; }
	void SetCaptureDebugImage (bool bValue) { m_bCaptureDebugImage = bValue; }

	bool IsDebugMode () const { return m_bDebug; }
	void SetDebugMode (bool b) { m_bDebug = b; }

	bool IsDroppedElementTest () const { return m_bDroppedElementTest; }
	void SetDroppedElementTest (bool bValue) { m_bDroppedElementTest = bValue; }

	void PostViewRefresh (DWORD dwWait);
	void UpdateUI ();

	void SetStrobeColor (int nStrobe);
	int GetStrobeColor () const { return m_nStrobe; }
	CString GetStrobeState () const;

	void SetSocketSetUserElements (bool bSet) { m_bSetSocketSetUserElements = bSet; }
	bool GetSocketSetUserElements () { return m_bSetSocketSetUserElements; }

	bool IsImageTimingEnabled () const { return m_bIsImageTimingEnabled; }

	virtual int DoMessageBox( LPCTSTR lpszPrompt, UINT nType, UINT nIDPrompt );

	void LoadPingSettings ();
	bool IsNetworkConnected () const { return m_bNetworkConnected; }
	void SetNetworkConnected (bool b) { m_bNetworkConnected = b; }
	bool DoConnectDatabase (FoxjetDatabase::COdbcDatabase & db, const CString & strDSN, const CString & strLocalDSN, const CString & strDatabase);

protected:
	int m_nStrobe;
	CStringArray m_vStrobeState;
	bool m_bPrintReport;
	bool m_bScanReport;
	bool m_bIsContinuousCount;
	bool m_bCaptureDebugImage;
	bool m_bDebug;
	bool m_bDroppedElementTest;
	bool m_bIsImageTimingEnabled;
	bool m_bSetSocketSetUserElements;
	HHOOK m_hCallWndProcRet;
	HANDLE m_hPingThread;
	FoxjetDatabase::CInstance m_instance;
	bool m_bNetworkConnected;
	bool m_bNEXTEditSession;

	static long CALLBACK CallWndProcRet (int nCode, WPARAM wParam, LPARAM lParam);

public:
	CStringList m_SerialPortList;
	bool m_bDemo;

	bool IsExpo () { return m_bExpo; }
	bool PromptForCountAtTaskStart () const { return m_bCountPromptAtTaskStart; }
	void SetPromptForCountAtTaskStart (bool bValue) { m_bCountPromptAtTaskStart = bValue; }
	BOOL DisplaySystemVersion();
	void ReportUserActivity (int nActivity);
	bool LoginPermits (unsigned int nCmdID);
	bool IsAdmin ();

	CControlApp();
	virtual ~CControlApp();

	static bool IsHostRunning ();

	void SetStatusMessage (const CString & str);
	
	void SetCoupledMode (bool bCoupled);
	bool GetCoupledMode ();

	void SetSingleMsgMode (bool bSingle);
	bool GetSingleMsgMode ();

	CView * GetActiveView() const;
	CDocument * GetActiveDocument() const;
	CMainFrame * GetActiveFrame () const;
	bool GetPreview () const;

	bool Login (const CString & strUser, const CString & strPass);
	CString GetDSN () const { return m_strDSN; }
	CString GetLocalDSN () const { return m_strLocalDSN; }
	bool IsLocalDB ();

	#ifdef __WINPRINTER__
		bool IsIV () { return m_pIVDriver != NULL; }
		bool IsMPHC () { return m_pMphcDriver != NULL || m_pUsbMphcCtrl != NULL; }

		CMphc * GetMPHCDriver ();
		CIvPhc * GetIVDriver ();
		USB::CUsbMphcCtrl * GetUSBDriver ();
		CDriverInterface * GetDriver ();

		CIdsThread * GetIdsThread () const { return m_pIdsThread; }
	#else
		bool IsIV () { return false; }
	#endif 

	void Enable0835 (bool bEnabled) { m_bSW0835 = bEnabled; }
	bool Is0835 () const { return m_bSW0835; }

	void Enable0844 (bool bEnabled) { m_bSW0844 = bEnabled; }
	bool Is0844 () const { return m_bSW0844; }

	void Enable0848 (bool bEnabled) { m_bSW0848 = bEnabled; }
	bool Is0848 () const { return m_bSW0848; }

#ifdef __WINPRINTER__
	void SetImagingMode (IMAGINGMODE mode) { m_nImgMode = mode; }
	IMAGINGMODE GetImagingMode () const { return m_nImgMode; }

	void SetImagingModeExpire (bool bExpire) { m_bImgModeExpire = bExpire; }
	bool GetImagingModeExpire () const { return m_bImgModeExpire; }
#endif //__WINPRINTER__

	CMapStringToString m_vErrSock;
	DWORD m_nInterfaceThreadID;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CControlApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnIdle(LONG lCount);
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CControlApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	void CancelStrobe ();
	void SetStrobe ();

	bool IsHubConnected ();
	void CacheHubState ();

	void BeginIdsThread ();
	void EndIdsThread ();
	 
	bool ShowPrintCycle () const { return m_bShowPrintCycle; }
	bool OutputToDbgView () const { return m_bDbgView; }
	bool IsVxDpc () const { return m_bVxDpc; }
	bool ShowNetStatDlg () const { return m_bShowNetStatDlg; }
	bool IsKeyboardEnabled () const { return m_bKeyboard; }

	CString GetCommandLine () const;
	static CString GetPreviewDirectory();

	CTime	m_tmStrobeCancelled;
	bool	m_bStrobeCancelled;
	bool	m_bShutdownTest;

protected:

	bool LoadSecurityOptionsTable (void);
	bool m_bCoupledLines;
	bool m_bSingleMsg;
	bool m_bIsHubConnected;
	CString m_strDSN;
	CString m_strLocalDSN;
	CString m_strLastDSN;
	bool m_bShowPrintCycle;
	bool m_bShowNetStatDlg;
	bool m_bDbgView;
	bool m_bVxDpc;
	bool m_bExpo;
	bool m_bCountPromptAtTaskStart;

	#ifdef __WINPRINTER__
	friend class CIdsThread;

	CMphc *				m_pMphcDriver;
	USB::CUsbMphcCtrl *	m_pUsbMphcCtrl;
	CIvPhc *			m_pIVDriver;
	CIdsThread *		m_pIdsThread;
	#endif //__WINPRINTER__

	bool m_bSW0835;
	bool m_bSW0844;
	bool m_bSW0848;
	bool m_bKeyboard;

	CString m_strFlags;

	typedef struct
	{
		TCHAR m_c;
		clock_t m_clock;
	} WM_KEY_STRUCT;

	void ProcessWmChar ();

	std::vector <WM_KEY_STRUCT> m_vWmChar;
	
#ifdef __WINPRINTER__
	IMAGINGMODE m_nImgMode;
	bool m_bImgModeExpire;
#endif //__WINPRINTER__
};

extern CControlApp theApp;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONTROL_H__3855D467_9729_11D5_8F07_00045A47C31C__INCLUDED_)
