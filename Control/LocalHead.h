#if !defined(AFX_LOCALHEAD_H__D127FEA8_C5C7_4E80_A5F3_9B68BD932B0E__INCLUDED_)
#define AFX_LOCALHEAD_H__D127FEA8_C5C7_4E80_A5F3_9B68BD932B0E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LocalHead.h : header file
//

#include "Head.h"
#include "UsbDebugDlg.h"
#include "BaseElement.h"
#include "DbTypeDefs.h"

/////////////////////////////////////////////////////////////////////////////
// CLocalHead thread

#define MAKEIMGPARAMS(context,id) new CLocalHead::BUILDIMAGESTRUCT ((context), _T (__FILE__), __LINE__, (id))

namespace Sw0848Thread
{
	class CPostPhotocell;
};

namespace LocalHead
{
	typedef struct
	{
		int m_nIndex;
		CString m_strData;
	} DYNDATA;

	class CError
	{
	public:
		CError (const CString & str = _T (""), FoxjetDatabase::HEADERRORTYPE dwType = FoxjetDatabase::HEADERROR_LOWTEMP, ULONG lLimit = 4000);
		CError (const CError & rhs);
		CError & operator = (const CError & rhs);
		virtual ~CError ();

		void Clear ();
		void Signal ();

		CString							m_str;
		FoxjetDatabase::HEADERRORTYPE	m_dwType;
		ULONG							m_lLimit;
		ULONG							m_lDuration;
		bool							m_bSignalled;
	};

	typedef CArray <CLocalHead *, CLocalHead *> CHeadPtrArray;
};

class CLocalHead : public Head::CHead
{
	friend class Head::CHead;
	friend class CImageEng;
	friend class CDebugDlg;
	friend class CDebugPage;
	friend class UsbDebugDlg::CUsbDebugDlg;

	DECLARE_DYNCREATE(CLocalHead)
protected:
	CLocalHead();           // protected constructor used by dynamic creation

	virtual BOOL PreTranslateMessage (MSG * pMsg);

public:

	typedef struct tagBUILDIMAGESTRUCT
	{
	public:
		tagBUILDIMAGESTRUCT (FoxjetCommon::BUILDTYPE context, LPCTSTR lpszFile = NULL, ULONG lLine = 0, UINT nThreadID = 0) 
		:	m_tm (CTime::GetCurrentTime ()),
			m_context (context),
			m_strFile (lpszFile),
			m_lLine (lLine),
			m_hEvent (NULL)
		{
			if (nThreadID) {
				CString str;

				str.Format (_T ("BUILDIMAGESTRUCT %lu [%s]"), nThreadID, CTime::GetCurrentTime ().Format (_T ("%c")));
				m_hEvent = ::CreateEvent (NULL, true, false, str);
			}
		}

		~tagBUILDIMAGESTRUCT ()
		{
			if (m_hEvent) {
				::CloseHandle (m_hEvent);
				m_hEvent = NULL;
			}
		}
		
		const CTime	& GetTime () const { return m_tm; }
		FoxjetCommon::BUILDTYPE GetContext () const { return m_context; }
		CString GetTraceFile () const { return m_strFile; }
		ULONG GetTraceLine () const { return m_lLine; }

	protected:
		CTime					m_tm;
		FoxjetCommon::BUILDTYPE	m_context;
		CString					m_strFile;
		ULONG					m_lLine;

	public:
		HANDLE		m_hEvent;
	} BUILDIMAGESTRUCT;

	typedef enum 
	{
		BUILDIMAGE_OVERRIDE				= 0x01,
		BUILDIMAGE_CONFIG_CHANGE		= 0x02,
	} BUILDIMAGETYPE;

	#ifdef _DEBUG
	static void DbgPrintBuffer (USHORT wColumns, UCHAR cBPC, USHORT wRowWords, PUCHAR pImage);
	static void Format (USHORT n, CHAR * psz);
	#endif

	static CString GetWDTFile ();

	HANDLE GetDeviceID () const { return m_hDeviceID; }
	WORD CalcPrintDelay (ULONG lPhotocellDelay);

protected:
	virtual ~CLocalHead();

	// TODO: rem //virtual DWORD GetImageBufferSize () { return IsUSB () ? 0x00080000 : IMAGE_BUFFER_SIZE; }

	// TODO: rem //bool IsUSB ();
	bool IsPhotoEnabled () const { return m_bPhotoEnabled; }
	bool SetPhotocellOffsets (ULONG lMin, ULONG lMax, WORD wMaxFlush);
	virtual void SetCounts (const CHead::CHANGECOUNTSTRUCT & count);

	virtual bool SetStandbyMode (bool bStandby);
	virtual afx_msg void OnProcessSerialData (UINT wParam, LONG lParam);

	WORD GetPrintDly () const { return m_wPrintDly; }
	WORD GetProdLen () const { return m_wProdLen; }
	WORD GetImageLen () const { return m_wImageLen; }
	UINT GetFlush () const { return m_nFlush; }

	void SetCountUntil (__int64 lCountUntil);

	void FindSlaves ();

	tagRegisters m_registersISA;
	USB::PrintHeadRegs m_registersUSB;

	void GetConfig (IVCARDCONFIGSTRUCT & config);
	void Abort ();

	WORD GetUsbImgLen (WORD wStartCol, WORD wEndCol) const;

	// --- driver debug --------------------
	bool WriteByte (UCHAR nRegID, UCHAR n);
	bool WriteWord (UCHAR nRegID, USHORT n);
	
	UCHAR ReadByte (UCHAR nRegID);
	USHORT ReadWord (UCHAR nRegID);
	
	void SetOffset (int nIndex, USHORT nVal);

	bool m_bDebug;
	bool m_bShowPrintCycle;
	// --- driver debug --------------------

	static void FlipBytes (PUSHORT lpData, ULONG lSize);

	virtual bool DeleteElement (ULONG lID);
	virtual ULONG AddElement (const CString & str);
	virtual bool UpdateElement (const CString & str);

	void Increment ();

	virtual void FreeImgUpdDesc ();
	virtual bool Load (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::TASKSTRUCT & task, 
		const FoxjetDatabase::BOXSTRUCT & box, const CMap <ULONG, ULONG, ULONG, ULONG> & mapHeads);

	void SetDynamicIndex (UINT nIndex);
	void IdleAll ();
	void StopAll ();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLocalHead)
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();
	//}}AFX_VIRTUAL

	virtual void SetSerialPending (bool bPending);
	//virtual int BuildImage (BUILDTYPE imgtype, bool bOverrideImagingFlag = false);

	FoxjetDatabase::STROBESTATE m_strobe;

	bool OnPhotocell (FoxjetCommon::BUILDTYPE imgtype, bool bOverrideImagingFlag = false);

	static int Find (const LocalHead::CHeadPtrArray & v, const CLocalHead * p);
	int GetSlaves (LocalHead::CHeadPtrArray & v);
	int GetLineHeads (LocalHead::CHeadPtrArray & v);
	void Insert (LocalHead::CHeadPtrArray & v, CLocalHead * p);
	void DownloadImages (LocalHead::CHeadPtrArray & v);

	afx_msg void OnEventPhoto (UINT	wParam, LONG lParam);
	afx_msg void OnEventEOP (UINT wParam, LONG lParam);
	afx_msg void OnEventSerial (UINT wParam, LONG lParam);
	afx_msg void OnToggleImageDebug(UINT nParam, LONG lParam);
	afx_msg void OnBuildImage (UINT nParam, LONG lParam);
	afx_msg void OnDownloadImageSegment (UINT nParam, LONG lParam);
	afx_msg void OnBuildNextImage (UINT nParam, LONG lParam);
	afx_msg void OnSetSerialPending(WPARAM wParam, LPARAM lParam);
	afx_msg void OnSignalRefresh (WPARAM wParam, LPARAM lParam);

	void ReadStatus ();									// sw0878 // void ReadStatus (bool & bFlashStrobe);
	void FireAMS ();
	void OnBeginApsCycle (UINT nParam, LONG lParam);	// sw0878
	void BeginApsCycle ();								// sw0878
	void EndApsCycle ();								// sw0878
	bool IsAmsActive () const;							// sw0878
	void Stop ();

	virtual DWORD GetErrorState () const;

	void DownloadImage (BUILDIMAGESTRUCT * p = NULL);
	void AbortImgRefreshMsgs ();
	bool AbortImgBuild (BUILDIMAGESTRUCT * p);
	bool CanDownloadImageSegment ();

	bool m_bImageBuilt;
	bool m_bLastIgnore;

	struct
	{
		bool m_bStop;
		WPARAM m_wParam;
		LPARAM m_lParam;
	} m_eop;

	bool m_bPhotoEnabled;
	bool m_bAmsActive;
	bool m_bAmsCountDown;
	INT m_nAmsInterval;
	CTime m_lastAMSCycle;
	int m_nStrobeColor;		// sw0878
	CTime m_tmApsBegin;		// sw0878
	DWORD m_dwApsTimeout;	// sw0878
	int m_nApsDelay;		// sw0878
	tagSerialParams params;
	CString m_sDatabaseName;
	CMapStringToString m_vSetUserElements;

#ifdef _DEBUG
	CDebugDlg *m_pDbgDlg;
#endif

	UsbDebugDlg::CUsbDebugDlg * m_pUsbDbgDlg;

//	PUCHAR pTest;
	_DBG_FLAGS Flags;
	HANDLE m_hDeviceID;
	HANDLE m_hPhotoEvent;
	HANDLE m_hAmsFinished;
	HANDLE m_hImageRefresh;
	DWORD m_dwImageRefresh;
	HANDLE m_hEOPEvent;				// End-of-Print Event // cgh 04d12
	HANDLE m_hSerialEvent;
	bool m_bDDE_Enabled;
	ULONG m_lMinPhotocellDelay;
	ULONG m_lMaxPhotocellDelay;
	WORD m_wMaxFlush;
	ULONG m_lStartTaskID;
	WORD m_wPrintDly;
	WORD m_wProdLen;
	WORD m_wImageLen;
	UINT m_nFlush;
	bool m_bSerialStart;

	LocalHead::CHeadPtrArray m_vSlaves;

private:

	static DWORD GetIndicatorState (const tagIndicators & s);

	static void ClearErrors (DWORD dwError, CArray <LocalHead::CError, LocalHead::CError &> & vErr, CArray <LocalHead::CError, LocalHead::CError &> & vClear);
	static void CheckErrors (DWORD dwError, CArray <LocalHead::CError, LocalHead::CError &> & v, int nDiff, CArray <LocalHead::CError, LocalHead::CError &> * pvClear = NULL);

	bool IsWDTEnabled ();
	void ProgramWDT ();
	void DisableWDT ();
	void StrokeWDT ();
	void CheckInkCode ();
	int GetInkWarningLevel () const;
	void ReadInkCode (const CString & strCode = _T (""));


	CArray <LocalHead::CError, LocalHead::CError &> m_vErrors;
	CArray <LocalHead::CError, LocalHead::CError &> m_vClear;

	clock_t m_clockLast;

	LARGE_INTEGER m_tmPhoto;
	LARGE_INTEGER m_tmEOP;
	CTime m_tmCard;
	ULONG m_lCardTimeout;
	ULONG m_lWDT;
	bool m_bWDT;
	FoxjetDatabase::MOTHERBOARDTYPE m_motherboard;
	CString m_strWDTFile;
	CTime m_tmWDT;
	clock_t m_tLastDownload;
	ULONG m_lGetIndicators;
	USB::tagIndicators m_lastIndicators;
	ULONG m_lEp8;
	CTime m_tmFPGA;
	int m_nInkWarning;

	LARGE_INTEGER m_tmImagingTimePC;
	LARGE_INTEGER m_tmImagingTimeEOP;
	LARGE_INTEGER m_freq; 

	void PostTimingInfo (const LARGE_INTEGER & tm, const CString & strDesc);

	Sw0848Thread::CPostPhotocell m_sw0848Def;

	DWORD GetErrorMask () const;

	// Generated message map functions
	//{{AFX_MSG(CLocalHead)
	//}}AFX_MSG
	virtual afx_msg void OnStartTask (UINT wParam, LONG lParam);
	virtual afx_msg void OnStopTask (UINT wParam, LONG lParam);
	virtual afx_msg void OnIdleTask (UINT wParam, LONG lParam);
	virtual afx_msg void OnResumeTask (UINT wParam, LONG lParam);
	virtual afx_msg void OnHeadConfigChanged (UINT wParam, LONG lParam);
	virtual afx_msg void OnSetIgnorePhotocell (UINT wParam, LONG lParam);

	afx_msg void OnSetIoAddress (UINT wParam, LONG lParam);
	afx_msg void OnShowDebugDialog (UINT wParam, LONG lParam);
	afx_msg void OnEnablePhotocell (UINT wParam, LONG lParam);
	afx_msg void OnUpdateStrobe ( UINT uParam, LONG lParam );
	afx_msg void OnDoDebugProcess (UINT nParam,  LONG lParam );
	afx_msg void OnReadConfig (UINT nParam,  LONG lParam );
	afx_msg void OnChangeCounts(UINT wParam, LONG lParam);
	afx_msg void OnIncrement (UINT wParam, LONG lParam);
	afx_msg void OnTriggerWDT (UINT wParam, LONG lParam);
	afx_msg void OnDownloadImages (UINT wParam, LONG lParam);
	
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOCALHEAD_H__D127FEA8_C5C7_4E80_A5F3_9B68BD932B0E__INCLUDED_)
