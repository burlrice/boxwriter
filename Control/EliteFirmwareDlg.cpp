// EliteFirmwareDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "Sw0848Thread.h"
#include "ProdLine.h"
#include "control.h"
#include "EliteFirmwareDlg.h"
#include "HeadDlg.h"
#include "Parse.h"
#include "SystemDlg.h"

using namespace FoxjetCommon;
using namespace Foxjet3d;
using namespace ListGlobals;
using namespace ItiLibrary;
using namespace EliteFirmwareDlg;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////

CHeadItem::CHeadItem (CLocalHead * pHead)
:	m_pHead (pHead)
{
	Head::CHeadInfo info = Head::CHead::GetInfo (pHead);
	ULONG lAddr = _tcstoul (info.m_Config.m_HeadSettings.m_strUID, NULL, 16);
	bool bUSB = false;

	SEND_THREAD_MESSAGE (info, TM_IS_USB, &bUSB);

	m_strName	= info.GetFriendlyName ();
	m_strAddr	= info.m_Config.m_HeadSettings.m_strUID;

	if (bUSB) {
		for (int i = 0; i < ARRAYSIZE (CHeadDlg::m_mapUSB); i++) {
			if (CHeadDlg::m_mapUSB [i].m_lAddr == lAddr) {
				m_strAddr = CHeadDlg::m_mapUSB [i].m_lpsz;
				break;
			}
		}

		CString str = FoxjetDatabase::ToString (info.GetGaVersion ());
	
		if (str.GetLength () >= 3)
			str.Insert (1, _T ("."));
		
		m_strGaVer = str;
	}
	else {
		for (int i = 0; i < ARRAYSIZE (CHeadDlg::m_mapPHC); i++) {
			if (CHeadDlg::m_mapPHC [i].m_lAddr == lAddr) {
				m_strAddr = CHeadDlg::m_mapPHC [i].m_lpsz;
				break;
			}
		}
	}
}

CHeadItem::~CHeadItem ()
{
}

int CHeadItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	return CItem::Compare (rhs, nColumn);
}

CString CHeadItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:		return m_strName;
	case 1:		return m_strAddr;
	case 2:		return m_strGaVer;
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CEliteFirmwareDlg property page

IMPLEMENT_DYNCREATE(CEliteFirmwareDlg, CPropertyPage)

CEliteFirmwareDlg::CEliteFirmwareDlg() 
:	m_hbrDlg (NULL),
	CPropertyPage (IDD_FIRMWARE_ELITE_MATRIX /* IsMatrix () ? IDD_FIRMWARE_ELITE_MATRIX : IDD_FIRMWARE_ELITE */)
{
	//{{AFX_DATA_INIT(CEliteFirmwareDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CEliteFirmwareDlg::~CEliteFirmwareDlg()
{
	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}
}

void CEliteFirmwareDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEliteFirmwareDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEliteFirmwareDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CEliteFirmwareDlg)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEliteFirmwareDlg message handlers

BOOL CEliteFirmwareDlg::OnInitDialog() 
{
	using namespace ListCtrlImp;

	CString strSection = defElements.m_strRegSection + _T ("\\CEliteFirmwareDlg");

	CArray <CColumn, CColumn> vCols;

	CDialog::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_HEAD), 150));
	vCols.Add (CColumn (LoadString (IDS_ADDRESS), 150));
	vCols.Add (CColumn (LoadString (IDS_GAVER), 150));

	m_lv.Create (LV_HEADS, vCols, this, defElements.m_strRegSection);

	for (int i = 0; i < m_vHeads.GetSize (); i++) {
		CHeadItem * p = new CHeadItem (m_vHeads [i]);

		m_lv.InsertCtrlData (p);
	}

	return TRUE;
}

HBRUSH CEliteFirmwareDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = ::GetSysColor (COLOR_BTNFACE);//FoxjetUtils::GetButtonColor (_T ("background"));
	
	if (FoxjetDatabase::IsControl (false)) 
		rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	if (!m_hbrDlg)	
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}
