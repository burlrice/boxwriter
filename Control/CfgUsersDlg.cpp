// CfgUsersDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Control.h"
#include "Resource.h"
#include "CfgUsersDlg.h"
#include "UserInfoDlg.h"
#include "Database.h"
#include "ListCtrlImp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace ItiLibrary;


CString CCfgUsersDlg::CUserItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:	return m_strFirstName;
	case 1:	return m_strLastName;
	case 2:	return m_strUserName;
	case 3:	return m_strGroup;
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CCfgUsersDlg dialog

const CString CCfgUsersDlg::m_strAdmin = _T ("admin");

CCfgUsersDlg::CCfgUsersDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg(IDD_CFG_USERS_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CCfgUsersDlg)
	//}}AFX_DATA_INIT
}


void CCfgUsersDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCfgUsersDlg)
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, IDC_ADD);
	m_buttons.DoDataExchange (pDX, IDC_REMOVE);
	m_buttons.DoDataExchange (pDX, IDC_PROPERTIES);
	m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);
}


BEGIN_MESSAGE_MAP(CCfgUsersDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CCfgUsersDlg)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_PROPERTIES, OnProperties)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_USERS, OnItemchangedUsers)
	ON_NOTIFY(NM_DBLCLK, IDC_USERS, OnDblclkUsers)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCfgUsersDlg message handlers

void CCfgUsersDlg::OnDestroy ()
{
	FoxjetCommon::CEliteDlg::OnDestroy ();
}

BOOL CCfgUsersDlg::OnInitDialog() 
{
	using namespace ListCtrlImp;

	CArray <CColumn, CColumn> vCols;

	FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_FIRSTNAME), 200));
	vCols.Add (CColumn (LoadString (IDS_LASTNAME), 200));
	vCols.Add (CColumn (LoadString (IDS_USERNAME), 200));
	vCols.Add (CColumn (LoadString (IDS_GROUP), 200));

	m_lv.Create (IDC_USERS, vCols, this, ListGlobals::defElements.m_strRegSection);

	LoadUserTable();
//	(GetDlgItem(IDC_PROPERTIES))->EnableWindow (false);
//	(GetDlgItem(IDC_REMOVE))->EnableWindow (false);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCfgUsersDlg::LoadUserTable()
{

	CArray <FoxjetDatabase::USERSTRUCT, FoxjetDatabase::USERSTRUCT &> v;
	FoxjetDatabase::GetUserRecords (theApp.m_Database, v);
	
	m_lv.DeleteCtrlData ();

	for ( int i= 0; i < v.GetSize(); i++) {
		FoxjetDatabase::SECURITYGROUPSTRUCT security;
		CUserItem * p = new CUserItem;

		* p = v [i];
		FoxjetDatabase::GetGroupsRecord (theApp.m_Database, p->m_lSecurityGroupID, security);
		p->m_strGroup = security.m_strName;

		m_lv.InsertCtrlData (p);
	}
	
	m_lv->SetSelectionMark (0);
	m_lv->SetItemState (0, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
}

void CCfgUsersDlg::OnOK() 
{
	FoxjetCommon::CEliteDlg::OnOK();
}


void CCfgUsersDlg::OnProperties() 
{
	using namespace FoxjetCommon;
	using namespace ItiLibrary;
	using namespace FoxjetDatabase;

	SECURITYGROUPSTRUCT security;
	CUserInfoDlg dlg;
	CLongArray sel = GetSelectedItems (m_lv);
	
	if (sel.GetSize () == 1) {
		int nItem = sel [0];
		if (CUserItem * p = (CUserItem *)m_lv.GetCtrlData (nItem)) {
			dlg.m_user = * p;
		
			if (dlg.DoModal() == IDOK) {
				* p = dlg.m_user;

				FoxjetDatabase::GetGroupsRecord (theApp.m_Database, p->m_lSecurityGroupID, security);
				p->m_strGroup = security.m_strName;
				FoxjetDatabase::UpdateUserRecord (theApp.m_Database, * p);
				m_lv.UpdateCtrlData (p);
			}
		}
	}
}

void CCfgUsersDlg::OnAdd() 
{
	bool bUserNameInUse = true;
	CUserInfoDlg dlg;

	while ( bUserNameInUse ) {
		if (dlg.DoModal() == IDOK) {
			FoxjetDatabase::USERSTRUCT user;

			if (!FoxjetDatabase::GetUserRecord (theApp.m_Database, dlg.m_user.m_strUserName, user)) {
				FoxjetDatabase::AddUserRecord (theApp.m_Database, dlg.m_user);
				bUserNameInUse = false;
				LoadUserTable ();
			}
			else {
				CString sMsg;
				sMsg.Format (LoadString (IDS_USERNAMEINUSE), user.m_strUserName, user.m_strFirstName, user.m_strLastName);
				MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
				MsgBox (* this, sMsg);
				dlg.m_user.m_strUserName.Empty ();
			}
		}
		else {
			break;
		}
	}
}

void CCfgUsersDlg::OnRemove() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		if (MsgBox (* this, LoadString (IDS_CONFIRMDELETE), LoadString (IDS_CONFIRM), MB_ICONINFORMATION | MB_YESNO) == IDYES) {
			if (CUserItem * p = (CUserItem *)m_lv.GetCtrlData (sel [0])) {
				FoxjetDatabase::DeleteUserRecord (theApp.m_Database, p->m_lID);
				m_lv.DeleteCtrlData (sel [0]);
			}
		}
	}
}

void CCfgUsersDlg::OnItemchangedUsers(NMHDR* pNMHDR, LRESULT* pResult) 
{
	using namespace FoxjetDatabase;
	using namespace FoxjetCommon;
	using namespace ItiLibrary;

	bool bEnable = true;

	CLongArray sel = GetSelectedItems (m_lv);

	for (int i = 0; i < sel.GetSize (); i++) {
		if (CUserItem * p = (CUserItem *)m_lv.GetCtrlData (sel [0])) {
			if (!p->m_strUserName.CompareNoCase (m_strAdmin)) {
				bEnable = false;
				break;
			}
		}
	}
	
	if (CWnd * p = GetDlgItem (IDC_REMOVE))
		p->EnableWindow (bEnable);

	*pResult = 0;
}

void CCfgUsersDlg::OnDblclkUsers(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnProperties ();	
	*pResult = 0;
}
