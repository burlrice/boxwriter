#if !defined(AFX_SW0835DLG_H__F8DEF7D2_8035_498A_8AB4_09BBC0A4A308__INCLUDED_)
#define AFX_SW0835DLG_H__F8DEF7D2_8035_498A_8AB4_09BBC0A4A308__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Sw0835Dlg.h : header file
//
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CSw0835Dlg dialog

class CControlView;
class CProdLine;

class CSw0835Dlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CSw0835Dlg(CControlView * pView, CProdLine * pLine);   // standard constructor
	virtual ~CSw0835Dlg ();

public:
	virtual void OnCancel();
	virtual void OnOK();
	int ProcessSerialData (const CString & str);

// Dialog Data
	//{{AFX_DATA(CSw0835Dlg)
	enum { IDD = IDD_BAXTER };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSw0835Dlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CControlView * m_pView;
	CProdLine * m_pLine;
	CTime m_tmInit;
	CFont m_fnt;

	CString m_strBarcode1;
	CString m_strBarcode2;
	CString m_strBarcode3;

	void GetData (CString & strTask,
		CString & strSerial1, CString & strSerial2, CString & strSerial3, 
		CString & strSubLot,
		CString & strOut);

	bool IsValidSubLot (const CString & str);
	static CString MakeAlpha (const CString & str);

	void Close ();

	// Generated message map functions
	//{{AFX_MSG(CSw0835Dlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnChangeSublot();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SW0835DLG_H__F8DEF7D2_8035_498A_8AB4_09BBC0A4A308__INCLUDED_)
