// Sw0848FieldsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "Sw0848FieldsDlg.h"
#include "TemplExt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSw0848FieldsDlg dialog

const CString CSw0848FieldsDlg::m_strTimestamp = _T ("[Timestamp]");

CSw0848FieldsDlg::CSw0848FieldsDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg (IDD_SW0848FIELDS_MATRIX /* IsMatrix () ? IDD_SW0848FIELDS_MATRIX : IDD_SW0848FIELDS */, pParent)
{
	//{{AFX_DATA_INIT(CSw0848FieldsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSw0848FieldsDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSw0848FieldsDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
}


BEGIN_MESSAGE_MAP(CSw0848FieldsDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CSw0848FieldsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSw0848FieldsDlg message handlers

BOOL CSw0848FieldsDlg::OnInitDialog() 
{
	CComboBox * pInput = (CComboBox *)GetDlgItem (CB_INPUT);
	CComboBox * pOutput = (CComboBox *)GetDlgItem (CB_OUTPUT);

	ASSERT (pInput);
	ASSERT (pOutput);

	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	{
		if (Find (m_vInput, CSw0848FieldsDlg::m_strTimestamp) == -1)
			m_vInput.Add (CSw0848FieldsDlg::m_strTimestamp);

		for (int i = 0; i < m_vInput.GetSize (); i++) {
			CString str = m_vInput [i];

			int nIndex = pInput->AddString (str);

			if (!str.CompareNoCase (m_strInput))
				pInput->SetCurSel (nIndex);
		}

		if (pInput->GetCurSel () == CB_ERR)
			pInput->SetCurSel (0);
	}

	{
		for (int i = 0; i < m_vOutput.GetSize (); i++) {
			CString str = m_vOutput [i];

			int nIndex = pOutput->AddString (str);

			if (!str.CompareNoCase (m_strOutput))
				pOutput->SetCurSel (nIndex);
		}

		if (pOutput->GetCurSel () == CB_ERR)
			pOutput->SetCurSel (0);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSw0848FieldsDlg::OnOK()
{
	CComboBox * pInput = (CComboBox *)GetDlgItem (CB_INPUT);
	CComboBox * pOutput = (CComboBox *)GetDlgItem (CB_OUTPUT);

	ASSERT (pInput);
	ASSERT (pOutput);

	pInput->GetLBText (pInput->GetCurSel (), m_strInput);
	pOutput->GetLBText (pOutput->GetCurSel (), m_strOutput);

	FoxjetCommon::CEliteDlg::OnOK ();
}
