#if !defined (_DEFINES_H_)
#define _DEFINES_H_

#define MAX_SCANNERS 4									// This is max Fixed Mount Scanners.
#define LOCAL_PORT MAX_SCANNERS + 1					// Local Serial Port.
#define COM_BUF_SIZE 1024

#define IMAGE_SUCCESS 0

#define TRANSFER_FONTS			100	// Tells system if it should actually Transfer the fonts or just generate the transfer List
#define GENERATE_FONT_LIST		0		// Tells system if it should actually Transfer the fonts or just generate the transfer List

//************************* Macro Definitions **************************
#define PACKVERSION(major,minor) MAKELONG(minor,major)

//******************* User Defined Thread Messages *********************
#define TM_STATUS_UPDATE				WM_APP +1	// Head status has been updated
#define TM_START_TASK					WM_APP +2	// Start Task New On Head.
#define TM_STOP_TASK					WM_APP +3	// Stop Task currently running on head.
#define TM_IDLE_TASK					WM_APP +4	// Idle Task currently running on head.
#define TM_RESUME_TASK					WM_APP +5	// Resume Task currently running on head.
#define TM_CONNECT_COMPLETE				WM_APP +7	// Notification that AsyncSock's Connection attempt is complete.
#define TM_CREATE_SOCKET				WM_APP +8	// Create Socket and attach to this handle.
#define TM_RX_SOCKET_DATA				WM_APP +9	// Client Socket has data waiting to be picked up.
#define TM_SET_IO_ADDRESS				WM_APP +10	// Set the I/O Address for the head.
//#define TM_CHANGE_USER_ELEMENT_DATA		WM_APP +11	// Notification for head to prompt for user data.
#define TM_DELETE_MEMSTORE				WM_APP +12	// Notification for head to Delete All Elements.
#define TM_UPDATE_FONTS					WM_APP +13	// Notification for head to transfer Fonts to IP Printer.
#define TM_GET_MSG_IDS					WM_APP +14	// Notification for head to request Message ID from IP Printer.
#define TM_DELETE_LABEL					WM_APP +15	// Notification for head to delete Label <name> from IP Printer.
#define TM_SHOW_DEBUG_DIALOG			WM_APP +17	// Notification to Show Debug Dialog
#define TM_UPDATE_LABELS				WM_APP +18	// Notification to Transfer updated labels to the IP Printer.
#define TM_SOCKET_DISCONNECT			WM_APP +19	// Notification to that socket has disconnected.
#define TM_DATA_RECIEVED				WM_APP +20
#define TM_INITIAL_STARTUP				WM_APP +21
#define TM_UPDATE_IP_LIST				WM_APP +22	// Notification to HeadConnector Thread to update IP List.
#define TM_SYNC_LABEL_STORE				WM_APP +23	// Notification to Check/Syncronize Label Store Check only is wParam = 0, Sync wParam = 1.
#define TM_UPDATE_FIRMWARE				WM_APP +24	// Notification to Update Firmaware on IP Printer
#define TM_UPDATE_STROBE				WM_APP +25	// Update Strobe on each head.
#define TM_CHANGE_COUNTS				WM_APP +26	// Update head's internal (on screen) count
#define TM_EVENT_PHOTO					WM_APP +27
#define TM_EVENT_EOP					WM_APP +28
#define TM_SET_IGNORE_PHOTOCELL			WM_APP +29
#define TM_UPDATE_GA					WM_APP +30	// Notification to Update GA on IP Printer
#define TM_BUILD_PREVIEW_IMAGE			WM_APP +32	
#define TM_PROCESS_SERIAL_DATA			WM_APP +33	
#define TM_BUILD_IMAGE					WM_APP +34	
#define TM_DOWNLOAD_IMAGE				WM_APP +35	
#define TM_DOWNLOAD_IMAGE_SEGMENT		WM_APP +36
#define TM_INCREMENT					WM_APP +37
#define TM_KILLTHREAD					WM_APP +38
#define TM_IDS_STATUS					WM_APP +39
#define TM_SOCKET_CONNECT				WM_APP +40
#define TM_SOCKET_RETRY					WM_APP +41
#define TM_UPDATE_COUNTS				WM_APP +42
#define TM_TRIGGER_WDT					WM_APP +43
#define TM_BUILD_NEXT_IMAGE				WM_APP +44
#define TM_DOWNLOAD_IMAGES				WM_APP +45
#define TM_BEGIN_APS					WM_APP +46
#define TM_GET_INFO						WM_APP +47

////////////////////////////////////////////////////////////////////////////////////////////////////

#define TM_POSTMESSAGESTRUCT_FIRST		WM_APP +48
#define TM_GET_ERROR_STATE				TM_POSTMESSAGESTRUCT_FIRST		
#define TM_INCREMENT_ERROR_INDEX		WM_APP +49
#define TM_FIND_SLAVES					WM_APP +50
#define TM_SET_DISPLAY_STRING			WM_APP +51
#define TM_GET_MISSING_FONTS			WM_APP +52
#define TM_SIM_PHOTOCELL				WM_APP +53
#define TM_IS_PRINTING					WM_APP +54
#define TM_IS_USB						WM_APP +55
#define TM_GET_ELEMENTS					WM_APP +56
#define TM_GET_PRODUCTAREA				WM_APP +57
#define TM_IS_PHOTOENABLED				WM_APP +58
#define TM_ENABLE_PHOTO					WM_APP +59
#define TM_GET_LASTERROR				WM_APP +60
#define TM_SET_USERDATA					WM_APP +61
#define TM_ADD_ELEMENT					WM_APP +62
#define TM_UPDATE_ELEMENT				WM_APP +63
#define TM_DELETE_ELEMENT				WM_APP +64
#define TM_DISPLAY_DEBUG_MSG			WM_APP +65
#define TM_UPDATE_STATUS_MSG			WM_APP +66
#define TM_CACHE						WM_APP +67
#define TM_FREE_CACHE					WM_APP +68
#define TM_LOAD							WM_APP +69
#define TM_IMAGE_FREE					WM_APP +70
#define	TM_GET_SERIALDATA				WM_APP +71
#define TM_GET_SERIALPENDING			WM_APP +72
#define TM_GET_PREVIEW_BMP				WM_APP +73
#define TM_IS_DBGVIEW					WM_APP +74
#define TM_IS_SHOW_PRINT_CYCLE			WM_APP +75
#define TM_SET_SHOW_PRINT_CYCLE			WM_APP +76
#define TM_GET_IMAGE_BUFFER_SIZE		WM_APP +77
#define TM_ABORT						WM_APP +78
#define TM_SET_PHOTOCELL_OFFSETS		WM_APP +79
#define TM_SUSPEND						WM_APP +80
#define TM_IS_ONE_TO_ONE_PRINT_ENABLED	WM_APP +81
#define TM_GET_TIME_DB					WM_APP +82
#define TM_SET_TIME_DB					WM_APP +83
#define TM_GET_DB						WM_APP +84
#define TM_HEAD_CONFIG_CHANGED			WM_APP +85	
#define TM_GET_PHOTO_EVENT_HANDLE		WM_APP +86	
#define TM_GET_EOP_EVENT_HANDLE			WM_APP +87
#define TM_GET_SERIAL_START				WM_APP +88
#define TM_SET_SERIAL_START				WM_APP +89
#define TM_NEXT_SET_DYNAMIC_INDEX		WM_APP +90
#define TM_GET_COUNT_UNTIL				WM_APP +91
#define TM_SET_COUNT_UNTIL				WM_APP +92
#define TM_SET_PRINT_DRIVER				WM_APP +93
#define TM_GET_PRINT_DRIVER				WM_APP +94
#define TM_GET_INK_LEVEL				WM_APP +95
#define TM_INK_CODE_CHANGE				WM_APP +96
#define TM_CHECK_INK_WARNING_LEVEL		WM_APP +97
#define TM_DO_CHECK_INK_CODE			WM_APP +98
#define TM_HEAD_PRINT_ONCE				WM_APP +99
#define TM_DOWNLOAD_DYNDATA				WM_APP +100
#define TM_GET_STATUS_MSG				WM_APP +101
#define WM_GET_TASK_STATE				WM_APP +102
#define TM_POSTMESSAGESTRUCT_LAST		WM_GET_TASK_STATE // always needs to be last

#define ISPOSTMESSAGESTRUCT(s) ((s) >= TM_POSTMESSAGESTRUCT_FIRST && (s) <= TM_POSTMESSAGESTRUCT_LAST)

#define TM_SYSTEMPARAMCHANGE			WM_APP +130
#define TM_SIGNAL_REFRESH				WM_APP +131
#define TM_NEXT_TASK_START				WM_APP +132
#define TM_NEXT_SEND_STATUS				WM_APP +133
#define TM_SET_KEY_VALUE				WM_APP +134
#define TM_BROADCAST_INK_CODE			WM_APP +135
#define TM_SET_EXP_SPAN					WM_APP +136

////////////////////////////////////////////////////////////////////////////////////////////////////

//***************************** Debug Thread Messages *********************
#define TM_DO_DEBUG_PROCESS				WM_APP +150	
#define TM_READ_CONFIG					WM_APP +151	
#define TM_TOGGLE_IMAGE_DEBUG			WM_APP +152
#define TM_SETSERIALPENDING				WM_APP +153

//******************* User Defined Windows Messages *********************
#define WM_HEAD_CONNECT					WM_APP +159	// Notification that AsyncSock Connected to a head.
#define WM_HEAD_DISCONNECT				WM_APP +160	// Head request to be disconnected due to an error.
#define WM_HEAD_STATUS_UPDATE			WM_APP +161	// Window notification that the head status has changed.
#define WM_SHOW_DIAG					WM_APP +162	// Window notification show Diagnostic Dialog.
#define WM_COMPORT_DATA					WM_APP +163	// Window notification that the Local COM Port has Data.
#define WM_INITIAL_STARTUP				WM_APP +164	// Window notification to start initial services.
#define WM_SYNC_HEAD_DATA				WM_APP +166	// Window notification to check local vs remote data for syncronization.
#define WM_HEAD_PROCESSING				WM_APP +168	// Window notification that head is processing requests.
#define WM_HEAD_SERIAL_DATA				WM_APP +169	// Window notification that head is has recieved serial data from mphc. (lParam: CSerialPacket * [NULL if only updating preview])
#define WM_PRINTER_SYNC_REQ				WM_APP +170	// Window notification that IP Printer requires syncronization.
#define WM_HEAD_AMSCYCLE				WM_APP +171	// Window notification to do AMS cycle
#define WM_IDLE_TASK					WM_APP +172	// Idle the task.  wParam: ULONG lLineID, lParam: CHead * pHead, with low ink (optional)
#define WM_CALCPRODUCTLEN				WM_APP +173  // sent after head config(s) changed, recalc value for SetProductLen
#define WM_SET_HEAD_ERROR				WM_APP +174
#define WM_HEAD_OBSOLETEFIRMWARE		WM_APP +175
#define WM_SET_HEAD_WARNING				WM_APP +176
#define WM_SET_STROBE					WM_APP +177
#define WM_NICELABEL_PRINT				WM_APP +178
#define WM_UPDATE_PREVIEW				WM_APP +179
#define WM_REFRESH_PREVIEW				WM_APP +180
#define WM_HEAD_CONNECTION				WM_APP +181
#define WM_HEAD_DELETE					WM_APP +182
#define WM_UPDATE_IP_LIST				WM_APP +183
#define WM_UPDATE_IMAGE					WM_APP +184
#define WM_HEAD_ERROR_CLEARED			WM_APP +185
#define WM_RESTART_TASK					WM_APP +186
#define WM_TRIGGER_WDT					WM_APP +187
#define WM_POSTPHOTOCELL				WM_APP +188
#define WM_CLOSE_DIAG					WM_APP +189 // Window notification to close Diagnostic Dialog
#define WM_GENIMAGE_DEBUG				WM_APP +190 
#define WM_SOCKET_SEND					WM_APP +191 
#define WM_SQL							WM_APP +192
#define WM_NEXT_SET_BCPARAMS			WM_APP +193
#define WM_NEXT_GET_BCPARAMS			WM_APP +194
#define WM_NEXT_SET_DMPARAMS			WM_APP +195
#define WM_NEXT_GET_DMPARAMS			WM_APP +196
#define WM_NEXT_GET_TASK_NAMES			WM_APP +197
#define WM_NEXT_ADD_TASK				WM_APP +198
#define WM_NEXT_DELETE_TASK				WM_APP +199
#define WM_NEXT_GET_STROBE				WM_APP +200
#define WM_NEXT_SET_STROBE				WM_APP +201
#define WM_NEXT_GET_SYSTEM_PARAMS		WM_APP +202
#define WM_NEXT_SET_SYSTEM_PARAMS		WM_APP +203
#define WM_REFRESH_IMAGE				WM_APP +204
#define WM_GET_BOX_ID					WM_APP +205
#define WM_NEXT_SET_DYNAMIC_DATA		WM_APP +206
#define WM_NEXT_SET_SERIAL_PORT			WM_APP +207
#define WM_NEXT_TASK_START				WM_APP +208
#define WM_NEXT_SET_DYNAMIC_INDEX		WM_APP +209
#define WM_NEXT_REFINE_CONFIG			WM_APP +210
#define WM_NEXT_INIT_FONTS				WM_APP +211
#define WM_NEXT_EDIT_SESSION			WM_APP +212
#define WM_XMLDATA						WM_APP +213
#define WM_SAVEXMLDATA					WM_APP +213
#define WM_INK_USAGE					WM_APP +214
#define WM_INK_USAGE_WARNING			WM_APP +215
#define WM_INK_CODE_CHANGE				WM_APP +216
#define WM_INK_CODE_CHECK_STATE			WM_APP +217
#define WM_BROADCAST_INK_CODE			WM_APP +218
#define WM_LOG_INK_CODE_REPORT			WM_APP +219
#define WM_GET_INK_CODES				WM_APP +220
#define WM_SET_WARNING_MESSAGES			WM_APP +221
#define WM_INK_CODE_SET_STATE			WM_APP +222
#define WM_INK_CODE_ACCEPTED			WM_APP +223
#define WM_GET_MAX_INK_WARNING			WM_APP +224
#define WM_RESUME_TASK					WM_APP +225
#define WM_INKCODEDLG_SETHEAD			WM_APP +226
#define WM_DOWNLOAD_SERIAL_DATA			WM_APP +227


//***************** User Defined Windows Messages For Host ***************
#define WM_HOST_CONNECT					WM_APP +300	// Window notification HOST is attempting to establish socket connection.
#define WM_HOST_DISCONNECT				WM_APP +301	// Window notification HOST has disconnected.
#define WM_HOST_REQUEST					WM_APP +302	// Window notification HOST request.

#define WM_COLORS_CHANGED				WM_APP +303
#define WM_DIAGNOSTIC_DATA				WM_APP +304

//******************* User Defined Windows Commands *********************
#define DOC_CMD_LINES_UPDATED			WM_APP +399	// Command to the document to update production line data.

//******************* User Defined Windows Commands from Auxiliary IO Box *********************
#define	WM_AUX_SERIAL_DATA							WM_APP + 401
#define	WM_AUX_PS2_DATA								WM_APP + 402
#define	WM_AUX_CONNECT_STATE_CHANGED				WM_APP + 403
#define	WM_AUX_DIGITAL_INPUT_DATA					WM_APP + 404
#define	WM_AUX_KEEP_ALIVE							WM_APP + 405
													
#define WM_SOCKET_CONNECT_DB						WM_APP + 499
#define	WM_SOCKET_START_TASK						WM_APP + 500
#define WM_SOCKET_STOP_TASK							WM_APP + 501
#define WM_SOCKET_IDLE_TASK							WM_APP + 502
#define WM_SOCKET_RESUME_TASK						WM_APP + 503
#define WM_SOCKET_GET_USER_ELEMENTS					WM_APP + 504
#define WM_SOCKET_SET_USER_ELEMENTS					WM_APP + 505
#define WM_SOCKET_SET_ERROR_SOCKET					WM_APP + 506
#define WM_SOCKET_GET_CURRENT_TASK					WM_APP + 507
#define WM_SOCKET_GET_CURRENT_STATE					WM_APP + 508
#define WM_SOCKET_GET_COUNT							WM_APP + 509
#define WM_SOCKET_SET_COUNT							WM_APP + 510
#define WM_SOCKET_CLOSE								WM_APP + 511
#define WM_SOCKET_GET_HEADS							WM_APP + 512
#define	WM_SOCKET_LOAD_TASK							WM_APP + 513
#define	WM_SOCKET_GET_ELEMENTS						WM_APP + 514
#define WM_SOCKET_DELETE_ELEMENT					WM_APP + 515
#define	WM_SOCKET_ADD_ELEMENT						WM_APP + 516
#define	WM_SOCKET_UPDATE_ELEMENT					WM_APP + 517
#define	WM_SOCKET_DB_BUILD_STRINGS					WM_APP + 518
#define	WM_SOCKET_DB_GET_NEXT_STRING				WM_APP + 519
#define	WM_SOCKET_DB_IS_READY						WM_APP + 520
#define	WM_SOCKET_DB_BEGIN_UPDATE					WM_APP + 521
#define	WM_SOCKET_DB_END_UPDATE						WM_APP + 522
#define WM_SET_SOCKET_ERROR_STATE					WM_APP + 523
#define WM_CHECKHEADERRORS							WM_APP + 524
#define WM_LINE_DO_AMS								WM_APP + 525
#define WM_LINE_IS_WAITING							WM_APP + 526
#define WM_LINE_SET_WAITING							WM_APP + 527
#define WM_SOCKET_PACKET_PRINTER_EOP				WM_APP + 528
#define WM_SOCKET_PACKET_PRINTER_SOP				WM_APP + 529
#define WM_SOCKET_PACKET_PRINTER_HEAD_STATE_CHANGE	WM_APP + 530
#define WM_SOCKET_REFRESH_PREVIEW					WM_APP + 531
#define WM_PING_STATUS								WM_APP + 532
#define WM_HOST_PACKET_SETSERIAL					WM_APP + 533
#define WM_HOST_PACKET_GETSERIAL					WM_APP + 534
#define WM_HOST_PACKET_GET_STROBE					WM_APP + 535
#define WM_SOCKET_START_TASK_DATABASE				WM_APP + 536
#define WM_SOCKET_GET_COUNT_MAX						WM_APP + 537
#define WM_SOCKET_SET_COUNT_MAX						WM_APP + 538
#define WM_SET_PRINT_DRIVER							WM_APP + 539
#define WM_HOST_RECIEVE_INK_CODE					WM_APP + 540
//#define WM_CONTROL_VIEW_GET_PROD_LINES				WM_APP + 541
//#define WM_CONTROL_VIEW_GET_ACTIVE_HEADS			WM_APP + 542
//#define WM_CONTROL_VIEW_GET_PROD_LINE_ID			WM_APP + 543
//#define WM_CONTROL_VIEW_GET_THREAD_BY_UID			WM_APP + 544
//#define WM_CONTROL_VIEW_GET_PROD_LINE_BY_HEAD_ID	WM_APP + 545
//#define WM_CONTROL_VIEW_LINE_NAME					WM_APP + 546
//#define WM_CONTROL_VIEW_GET_SERIAL_DATA_PTR			WM_APP + 547
//#define WM_CONTROL_VIEW_GET_SW048_THREAD			WM_APP + 548

//****************************** Timer IDs ******************************
const UINT TIMER_STATUS			= 2;							// Timer used to update preview panel.
const UINT TIMER_TAB			= 3;							// Timer used to Sync. of local vs remote head data ( IP ).
const UINT EDITOR_MONITOR_TIMER	= 4;							// Timer used to Monitor Editor Running.
const UINT TIMER_TIME			= 5;
const UINT TIMER_STARTMENU		= 6;

#endif
