// CG: This file was added by the Splash Screen component.
// Splash.cpp : implementation file
//

#include "stdafx.h"  // e. g. stdafx.h
#include "resource.h"  // e.g. resource.h

#include "Splash.h"  // e.g. splash.h
#include "Control.h"
#include "List.h"
#include "Color.h"
#include "Utils.h"

#define CHECKCLOSESTATE		1

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace Color;

/////////////////////////////////////////////////////////////////////////////
//   Splash Screen class


const UINT CSplashWnd::m_nMinDisplayTime = 3000; // milliseconds
BOOL CSplashWnd::c_bShowSplashWnd;
CSplashWnd* CSplashWnd::c_pSplashWnd;

CSplashWnd::CSplashWnd()
{
}

CSplashWnd::~CSplashWnd()
{
	// Clear the static window pointer.
	ASSERT(c_pSplashWnd == this);
	c_pSplashWnd = NULL;
}

BEGIN_MESSAGE_MAP(CSplashWnd, CWnd)
	//{{AFX_MSG_MAP(CSplashWnd)
	ON_WM_CREATE()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CSplashWnd::EnableSplashScreen(BOOL bEnable /*= TRUE*/)
{
	c_bShowSplashWnd = bEnable;
}

void CSplashWnd::ShowSplashScreen(CWnd* pParentWnd /*= NULL*/)
{
	if (!c_bShowSplashWnd || c_pSplashWnd != NULL)
		return;

	// Allocate a new splash screen, and create the window.
	c_pSplashWnd = new CSplashWnd;
	if (!c_pSplashWnd->Create(pParentWnd))
		delete c_pSplashWnd;
	else
		c_pSplashWnd->UpdateWindow();
}

BOOL CSplashWnd::PreTranslateAppMessage(MSG* pMsg)
{
	if (c_pSplashWnd == NULL)
		return FALSE;

	// If we get a keyboard or mouse message, hide the splash screen.
	if (IsCloseMsg (pMsg->message)) {
		c_pSplashWnd->m_bCanClose = true;//c_pSplashWnd->HideSplashScreen();
		return TRUE;	// message handled here
	}

	return FALSE;	// message not handled
}

BOOL CSplashWnd::Create(CWnd* pParentWnd /*= NULL*/)
{
	FoxjetCommon::GetSplashBitmap (m_bitmap);
	BITMAP bm;
	m_bitmap.GetBitmap(&bm);
	CRect rc (0, 0, bm.bmWidth, bm.bmHeight);

	return CreateEx(0,
		AfxRegisterWndClass(0, AfxGetApp()->LoadStandardCursor(IDC_ARROW)),
		NULL, WS_POPUP | WS_VISIBLE, rc, pParentWnd, 0);
}

void CSplashWnd::HideSplashScreen()
{
	// Destroy the window, and update the mainframe.
	DestroyWindow();
	AfxGetMainWnd()->UpdateWindow();
}

void CSplashWnd::PostNcDestroy()
{
	// Free the C++ class.
	delete this;
}

int CSplashWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	DWORD dw;

	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	CenterWindow(GetParent ());
	m_bCanClose = false;
	::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0,
			(LPTHREAD_START_ROUTINE)ThreadFunc, (LPVOID)this, 0, &dw);
	::Sleep (0);

	return 0;
}

void CSplashWnd::OnPaint()
{
	CPaintDC dc(this);

	CDC dcImage;
	if (!dcImage.CreateCompatibleDC(&dc))
		return;

	BITMAP bm;
	m_bitmap.GetBitmap(&bm);

	// Paint the image.
	CBitmap* pOldBitmap = dcImage.SelectObject(&m_bitmap);
	dc.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &dcImage, 0, 0, SRCCOPY);
	dcImage.SelectObject(pOldBitmap);

	CRect rcWindow;
	CFont fnt;
	COLORREF rgb = rgbIntermediateGray;

	GetWindowRect (&rcWindow);
	CRect rcDraw = FoxjetCommon::GetSplashTextRect ();

	fnt.CreateFont (15, 0, 0, 0, FW_BOLD, FALSE, 0, 0,
		DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("Verdana"));
	CFont * pOldFont = dc.SelectObject (&fnt);
	COLORREF rgbOld = dc.SetTextColor (rgb);
	int nBkMode = dc.SetBkMode (TRANSPARENT);

	CString str =
		CString (GlobalFuncs::GetAppName ()) + _T (" ") + LoadString (IDS_VERSION) +
		_T (" ") + GlobalVars::CurrentVersion.Format (false) + _T ("\n") +
		LoadString (IDS_COPYRIGHT) + _T ("\n") +
		LoadString (IDS_FOXJETINC) + _T ("\n") +
		_T ("\n\n") + 
		FoxjetCommon::GetSplashText (GetElementListVersion ());

	#ifdef _DEBUG
	CString strInterval;
	strInterval.Format (_T ("Min. display time = %.02f seconds\n%s"), 
		(double)m_nMinDisplayTime / 1000.0,
		m_bCanClose ? _T ("[Can close]") : _T (""));
	str += _T ("\n\n") + strInterval;
	#endif //_DEBUG

	dc.DrawText (str, rcDraw, DT_WORDBREAK);
	dc.SelectObject (pOldFont);
	dc.SetTextColor (rgbOld);
	dc.SetBkMode (nBkMode);
}

ULONG CALLBACK CSplashWnd::ThreadFunc (LPVOID lpData)
{
	ASSERT (lpData);
	CSplashWnd * pWnd = (CSplashWnd *)lpData;
	bool bMore = true;

	pWnd->SetForegroundWindow ();

//	#ifndef _DEBUG
	::Sleep (CSplashWnd::m_nMinDisplayTime);
//	#endif 

	const CTime tmStart = CTime::GetCurrentTime ();

	while (bMore) {
		CTimeSpan tm = CTime::GetCurrentTime () - tmStart;

		bMore = !pWnd->m_bCanClose;

		if (CWnd * p = ::AfxGetMainWnd ()) {
			if (::IsWindow(p->m_hWnd))
				p->SetForegroundWindow ();
		}

		if ((tm.GetTotalSeconds () * 1000) > CSplashWnd::m_nMinDisplayTime)
			bMore = false;

		if (!bMore) 
			::SendMessage (pWnd->m_hWnd, WM_CLOSE, 0, 0);
		else
			::Sleep (100);
	}


	return 0;
}

bool CSplashWnd::IsCloseMsg (UINT message)
{
	switch (message) {
	case WM_KEYDOWN:
	case WM_SYSKEYDOWN:
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_MBUTTONDOWN:
    case WM_NCLBUTTONDOWN:
	case WM_NCRBUTTONDOWN:
	case WM_NCMBUTTONDOWN:
//	case WM_KILLFOCUS:
		return true;
	}

	return false;
}

LRESULT CSplashWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (IsCloseMsg (message))
		m_bCanClose = true;

	return CWnd::WindowProc (message, wParam, lParam);
}

