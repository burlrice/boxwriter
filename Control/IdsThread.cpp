// IdsThread.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include <process.h>
#include "control.h"
#include "IdsThread.h"
#include "Debug.h"
#include "Host.h"
#include "Parse.h"
#include "IdsDlg.h"

using namespace FoxjetDatabase;
using namespace FoxjetUtils;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIdsThread

IMPLEMENT_DYNCREATE(CIdsThread, CWinThread)

CIdsThread::CIdsThread()
{
	m_tmLast			= CTime::GetCurrentTime ();
	m_nTimeoutSeconds	= 5;
	m_sock.m_pThread	= this;
	ZeroMemory (&m_status, sizeof (m_status));

	m_tmLast -= CTimeSpan (0, 0, 0, m_nTimeoutSeconds); // prevents temporary "ok" state if really not connected
	// will cause a temporary "disconnected" state if connected
}

CIdsThread::~CIdsThread()
{
}

BOOL CIdsThread::InitInstance()
{
	::AfxSocketInit (NULL);

	m_status.m_bConnected = false;

	if (!m_sock.Create (IV_IDS_PORT_REVCFROM, SOCK_DGRAM)) {
		CString str;
		
		str.Format (LoadString (IDS_SOCKCREATEFAILED), IV_IDS_PORT_REVCFROM);
		str += FormatMessage (::GetLastError ());

		TRACEF (str);
		MsgBox (str);
		ASSERT (0);
		//return FALSE;
	}

	return TRUE;
}

static const LPCTSTR lpszIDSKey = _T ("IDS");

void CIdsThread::LoadAddress (FoxjetDatabase::COdbcDatabase & db)
{
	LOCK (m_cs);
	SETTINGSSTRUCT s;
	in_addr addr;
	DWORD dw = CIdsDlg::GetDefAddr ();

	if (GetSettingsRecord (db, 0, lpszIDSKey, s)) {
		CStringArray v;

		FoxjetDatabase::FromString (s.m_strData, v);

		if (v.GetSize ()) 
			dw = _tcstoul (v [0], NULL, 16);
	}

	addr.S_un.S_addr = htonl (dw);
	m_strAddress = inet_ntoa (addr);
}

int CIdsThread::ExitInstance()
{
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CIdsThread, CWinThread)
	//{{AFX_MSG_MAP(CIdsThread)
	ON_THREAD_MESSAGE (TM_KILLTHREAD, OnKillThread)
	ON_THREAD_MESSAGE (TM_IDS_STATUS, OnStatus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIdsThread message handlers

void CIdsThread::OnKillThread (WPARAM wParam, LPARAM lParam)
{
	LOCK (m_cs);

	#ifdef __WINPRINTER__
	theApp.m_pIdsThread = NULL;
	#endif //__WINPRINTER__

	delete this;
	_endthreadex (0);
}

void CIdsThread::OnStatus (WPARAM wParam, LPARAM lParam)
{
	if (STATUSSTRUCT * p = (STATUSSTRUCT *)wParam) {
		LOCK (m_cs);
		{
			memcpy (&m_status, p, sizeof (m_status));
			m_status.m_bConnected = true;
			m_tmLast = CTime::GetCurrentTime ();
		}
		
		delete p;
	}
}

void CIdsThread::GetStatus (STATUSSTRUCT & status, int nTimeoutSeconds)
{
	LOCK (m_cs);
	{
		CTimeSpan tm = CTime::GetCurrentTime () - m_tmLast;
		int n = tm.GetTotalSeconds ();

		ASSERT (tm.GetTotalSeconds () >= 0);

		m_nTimeoutSeconds = nTimeoutSeconds;
		memcpy (&status, &m_status, sizeof (status));
		m_status.m_bConnected = status.m_bConnected = (n <= m_nTimeoutSeconds) ? true : false;
	}
}

void CIdsThread::UpdateStatus ()
{
	LOCK (m_cs);
	{
		UCHAR cBuffer [] = { 0 };
		int nLen = ARRAYSIZE (cBuffer);

		int nSendTo = m_sock.SendTo (&cBuffer, nLen, IV_IDS_PORT_SENDTO, m_strAddress);
	}
}