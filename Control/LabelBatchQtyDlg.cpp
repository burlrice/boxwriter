// LabelBatchQtyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "LabelBatchQtyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLabelBatchQtyDlg dialog


CLabelBatchQtyDlg::CLabelBatchQtyDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg (IDD_LABELBATCHQTY_MATRIX /* IsMatrix () ? IDD_LABELBATCHQTY_MATRIX : IDD_LABELBATCHQTY */ , pParent)
{
	//{{AFX_DATA_INIT(CLabelBatchQtyDlg)
	m_dwQuantity = 0;
	//}}AFX_DATA_INIT
}


void CLabelBatchQtyDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLabelBatchQtyDlg)
	DDX_Text(pDX, TXT_QTY, m_dwQuantity);
	DDV_MinMaxDWord(pDX, m_dwQuantity, 0, ULONG_MAX);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLabelBatchQtyDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CLabelBatchQtyDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLabelBatchQtyDlg message handlers
