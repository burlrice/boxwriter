#include "stdafx.h"
#include "Resource.h"
#include <AfxSock.h>
#include <map>
#include "buffer.h"
#include "Debug.h"
#include "AppVer.h"
#include "Version.h"
#include "Database.h"
#include "FileExt.h"
#include "Parse.h"
#include "ximage.h"
#include "ximabmp.h"
#include "color.h"
#include "DC.h"
#include "Control.h"

extern CControlApp theApp;

using namespace std;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

class CxImageBMPEx : public CxImageBMP
{
public:
	const BITMAPINFOHEADER & GetHeader () const { return head; }

	CBuffer GetBuffer () 
	{
		if (GetDIB ()) {
			BITMAPFILEHEADER	hdr;

			hdr.bfType = 0x4d42;   // 'BM' WINDOWS_BITMAP_SIGNATURE
			hdr.bfSize = GetSize() + 14 /*sizeof(BITMAPFILEHEADER)*/;
			hdr.bfReserved1 = hdr.bfReserved2 = 0;
			hdr.bfOffBits = 14 /*sizeof(BITMAPFILEHEADER)*/ + GetHeader ().biSize + GetPaletteSize();

				//copy attributes
			memcpy(GetDIB (),&GetHeader (),sizeof(BITMAPINFOHEADER));
				
			// Write the file header
			CBuffer header ((unsigned char *)&hdr,min(14,sizeof(BITMAPFILEHEADER)), true); //hFile->Write(&hdr,min(14,sizeof(BITMAPFILEHEADER)),1);
				
			// Write the DIB header and the pixels
			CBuffer dib ((unsigned char *)GetDIB (), GetSize (), true); //hFile->Write(pDib,GetSize(),1);

			return header + dib;
		}

		return CBuffer ();
	}	
};

typedef struct
{
	SOCKADDR_IN m_sin;
	int m_nLen;
	SOCKET m_sock;
	HANDLE m_hInit;
} HTTP_REQUEST_STRUCT;

std::string to_string(const char * pData, int nLen)
{
	std::string s;

	for (int i = 0; i < nLen; i++) {
		if ((pData[i] >= ' ' && pData[i] <= '~') || pData [i] == '\r' || pData [i] == '\n')
			s += pData[i];
		else
			s += '.';
	}

	return s;
}

std::string to_hex_string(const unsigned char * pData, int nLen)
{
	std::string s;

	for (int i = 0; i < nLen; i++) {
		char sz[8] = { 0 };

		sprintf(sz, "%02X", (unsigned char)pData[i]);
		s += sz;

		if (!((i + 1) % 4))
			s += ' ';
	}

	return s;
}
std::string urldecode(const std::string & str)
{
	CString strResult(str.c_str ());

	for (int i = 0; i < 256; i++) {
		CString strTmp;

		strTmp.Format(_T("%%%02X"), i);

		strResult.Replace(strTmp, CString((TCHAR)i));
	}

	for (int i = 0; i < 256; i++) {
		CString strTmp;

		strTmp.Format(_T("%%%X"), i);

		strResult.Replace(strTmp, CString((TCHAR)i));
	}

	return (std::string)w2a (strResult);
}

LPCTSTR lpsz404 = 
{
	_T ("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n")
	_T ("<html>\r\n")
	_T ("<head>\r\n")
	_T ("   <title>404 Not Found</title>\r\n")
	_T ("</head>\r\n")
	_T ("<body>\r\n")
	_T ("   <h1>Not Found</h1>\r\n")
	_T ("   <p>The requested URL was not found on this server:</p>\r\n")
	_T ("   <p>[url]</p>\r\n")
	_T ("</body>\r\n")
	_T ("</html>\r\n")
};

static CBuffer processHttpRequest(const std::string & str)
{
	static const std::string strServer = "BoxWriter MATRIX " + (string)w2a (FoxjetCommon::CVersion (FoxjetCommon::verApp).ToString ());
	static const string strGet = "GET ";
	string strFile = w2a(::lpsz404);
	string strTime = w2a(CTime::GetCurrentTime().Format(_T("%c")));
	string strResponse = "404 Not Found";
	string strWWW = w2a (CControlApp::GetPreviewDirectory());

	if (str.substr(0, strGet.length()) == strGet) {
		string strUrl;
		std::map <string, string> mapParams;

		for (int i = strGet.length(); i < str.length(); i++) {
			if (isspace (str [i]))
				break;

			strUrl += str [i];
		}

		if (strUrl.find ('?') != string::npos) {
			string s = strUrl.substr (strUrl.find ('?') + 1);
			strUrl = strUrl.substr (0, strUrl.find ('?'));

			vector <wstring> vParam = explode ((wstring)a2w (s.c_str ()), '&');

			for (int i = 0; i < vParam.size (); i++) {
				vector <wstring> v = explode ((wstring)a2w (vParam [i].c_str ()), '=');

				if (v.size () >= 2) {
					string key = w2a (v [0].c_str ());
					string value = w2a(v [1].c_str ());

					mapParams [urldecode (key)] = urldecode (value);
				}
			}
		}

		strUrl = urldecode (strUrl);

		{
			CString s = a2w(strFile.c_str ());

			s.Replace(_T("[url]"), a2w (strUrl.c_str ()));
			strFile = w2a (s);
		}

		if (strUrl == "/")
			strUrl = "/index.html";

		CString strWinUrl = a2w (string (strWWW + strUrl).c_str ());

		strWinUrl.Replace (_T ("/"), _T ("\\"));

		struct
		{
			CString m_strPage;
			CString m_strFile;
		}
		static const mapSSE [] = 
		{
			{ _T("\\USB1.php"),	_T("\\1.bmp") },
			{ _T("\\USB2.php"),	_T("\\2.bmp") },
			{ _T("\\USB3.php"),	_T("\\3.bmp") },
			{ _T("\\USB4.php"),	_T("\\4.bmp") },
		};

		for (int i = 0; i < ARRAYSIZE (mapSSE); i++) {
			if (strWinUrl.CompareNoCase((CString)a2w (strWWW.c_str ()) + mapSSE [i].m_strFile) == 0) {
				if (::GetFileAttributes (strWinUrl) == -1) {
					CxImageBMPEx img;
					string strExt = "bmp";

					img.Create (1, 1, 1);
					img.SetPixelColor (0, 0, Color::rgbWhite);
					CBuffer data = img.GetBuffer ();
					CBuffer header = CBuffer (
						"HTTP/1.1 " + strResponse + "\r\n"
						"Date: " + strTime + "\r\n"
						"Accept-Ranges: bytes\r\n"
						"Server: " + strServer + "\r\n"
						"Content-Length: " + std::to_string((__int64)data.size ()) + "\r\n"
						"Keep-Alive: timeout=2, max=99\r\n"
						"Connection: Keep-Alive\r\n"
						"Content-Type: image/" + strExt + "\r\n\r\n");
					return header + data;
				}
			}

			if (strWinUrl.CompareNoCase((CString)a2w (strWWW.c_str ()) + mapSSE [i].m_strPage) == 0) {
				const CString strPath = a2w (strWWW.c_str ()) + mapSSE [i].m_strFile;
				HANDLE hFile = ::CreateFile (strPath, 
					GENERIC_READ, 
					FILE_SHARE_READ, 
					NULL, 
					OPEN_EXISTING, 
					FILE_ATTRIBUTE_NORMAL, 
					0);

				strFile = "data: ";
			
				if (hFile != INVALID_HANDLE_VALUE) {
					FILETIME ft;
					SYSTEMTIME sys;

					::GetFileTime (hFile, NULL, NULL, &ft);
					::CloseHandle (hFile);
					strFile += w2a (CTime (ft).Format (_T ("%c")));
				}

				strFile += "\n\n";

				CBuffer result (
					"HTTP/1.1 200 OK\r\n"
					"Connection: Keep-Alive\r\n"
					"Content-Type: text/event-stream\r\n"
					"Cache-Control: no-cache\r\n"
					//"Date: " + strTime + "\r\n"
					//"Server: " + strServer + "\r\n"
					"Content-Length: " + std::to_string((__int64)strFile.length()) + "\r\n"
					"\r\n" + strFile);
				//TRACEF (result.ToString ().c_str ());

				return result;
			}
		}

		if (::GetFileAttributes(strWinUrl) != -1) {
			CString strExt = FoxjetFile::GetFileExt(strWinUrl);
			LPCTSTR lpsz [] = 
			{
				_T("jpg"),
				_T("jpeg"),
				_T("bmp"),
				_T("gif"),
			};
			bool bBinary = false;

			strResponse = "200 OK";

			for (int i = 0; !bBinary && i < ARRAYSIZE (lpsz); i++)
				if (strExt.CompareNoCase(lpsz [i]) == 0) 
					bBinary = true;

			if (bBinary) {
				CxImageBMPEx img;

				if (img.Load (strWinUrl)) {
					if (mapParams ["context"] == "EDITOR") {
						img.RotateRight ();
						img.Negative ();
						img.Mirror ();
					}

					CxImageBMPEx bmp (img);
					CBuffer data = bmp.GetBuffer ();
					CBuffer header = CBuffer (
						"HTTP/1.1 " + strResponse + "\r\n"
						"Date: " + strTime + "\r\n"
						"Accept-Ranges: bytes\r\n"
						"Server: " + strServer + "\r\n"
						"Content-Length: " + std::to_string((__int64)data.size ()) + "\r\n"
						"Keep-Alive: timeout=2, max=99\r\n"
						"Connection: Keep-Alive\r\n"
						"Content-Type: image/" + (string)w2a (strExt) + "\r\n\r\n");
					
					return header + data;
				}
			}
			else {
				strFile = w2a(FoxjetFile::ReadDirect(strWinUrl));
			}
		}
		else {
			if (strUrl == "/index.html") {
				strResponse = "200 OK";
				HINSTANCE hModule = ::AfxGetInstanceHandle (); 

				if (HRSRC hRSRC = ::FindResource (hModule, MAKEINTRESOURCE (IDR_INDEX_HTML), RT_HTML)) {
					DWORD dwSize = ::SizeofResource (hModule, hRSRC);

					if (HGLOBAL hGlobal = ::LoadResource (hModule, hRSRC)) {
						if (LPVOID lpData = ::LockResource (hGlobal)) {
							CString str = a2w ((LPCSTR)lpData);
							const CString strTableFind = Extract (str, _T ("<!-- table begin -->"), _T ("<!-- table end -->"));
							const CString strSSEFind = Extract (str, _T ("// SSE begin"), _T ("// SSE end"));
							const CString strFind [] = { _T ("usb1"),  _T ("date1"),  _T ("1.bmp"), _T ("img1"), _T ("USB1"), _T ("diff1") };
							CString strTableReplace, strSSEReplace;

							str.Replace (_T ("[title]"), _T ("BW MATRIX ") + FoxjetCommon::CVersion (FoxjetCommon::verApp).Format ());

							for (int i = 0; i < ARRAYSIZE (mapSSE); i++) {
								CString strFile = (CString)a2w (strWWW.c_str ()) + mapSSE [i].m_strFile;

								if (::GetFileAttributes (strFile) != -1) {
									const CString strReplace [] = 
									{ 
										_T ("usb") + ToString (i + 1),  
										_T ("date") + ToString (i + 1),  
										ToString (i + 1) + _T (".bmp"), 
										_T ("img") + ToString (i + 1),
										_T ("USB") + ToString (i + 1),
										_T ("diff") + ToString (i + 1),
									};

									ASSERT (ARRAYSIZE (strFind) == ARRAYSIZE (strReplace));
									
									CString strTable = strTableFind;
									CString strSSE = strSSEFind;

									for (int j = 0; j < ARRAYSIZE (strFind); j++) {

										strTable.Replace (strFind [j], strReplace [j]);
										strSSE.Replace (strFind [j], strReplace [j]);
									}

									strTableReplace += strTable + _T ("\n");
									strSSEReplace += strSSE + _T ("\n");
								}
							}

							str.Replace (strTableFind, strTableReplace);
							str.Replace (strSSEFind, strSSEReplace);
							strFile = w2a (str); //strFile = (LPCSTR)lpData;
						}
					}
				}
			}
			else if (strUrl.find("/preview") != -1) {
				CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
				CSize sizePreview(0, 0);
				CDbConnection conn(_T(__FILE__), __LINE__);
				std::vector <CxImage*> vImages;

				GetPrinterHeads(theApp.m_Database, GetPrinterID (theApp.m_Database), vHeads);

				for (int i = 0; i < ARRAYSIZE(mapSSE); i++) {
					CString str = a2w(strWWW.c_str()) + mapSSE[i].m_strFile;

					for (int nHead = 0; nHead < vHeads.GetSize(); nHead++) {
						HEADSTRUCT& h = vHeads[nHead];

						if (_ttoi(h.m_strUID) == (i + 1)) {
							if (GetFileAttributes(str) != -1) {
								CxImage* pImg = new CxImage();

								if (pImg->Load(str)) {
									FoxjetCommon::CTmpDC dc(NULL);
									const double dScreenDPI[] = { ::GetDeviceCaps(dc, LOGPIXELSX), ::GetDeviceCaps(dc, LOGPIXELSX) };
									const double dPrintDPI[] = { h.GetRes (), h.GetChannels () / h.GetSpan () };

									strResponse = "200 OK";
									pImg->RotateRight();
									pImg->Negative();
									pImg->Mirror();

									CSize size(pImg->GetWidth(), pImg->GetHeight());

									size.cx = size.cx / dPrintDPI[0] * dScreenDPI[0];
									size.cy = size.cy / dPrintDPI[1] * dScreenDPI[1];

									pImg->Resample(size.cx, size.cy);
									sizePreview.cx = max(sizePreview.cx, size.cx);
									sizePreview.cy += size.cy;
								}

								vImages.push_back(pImg);
							}
						}
					}
				}

				{
					int y = 0;
					CDC dcImage, dcMem;
					CBitmap bmpMem;
					FoxjetCommon::CTmpDC dc(CWnd::GetDesktopWindow());

					sizePreview.cy += (vImages.size());
					dcMem.CreateCompatibleDC(dc);
					bmpMem.CreateCompatibleBitmap(&dcMem, sizePreview.cx, sizePreview.cy);

					CBitmap* pMem = dcMem.SelectObject(&bmpMem);

					::BitBlt(dcMem, 0, 0, sizePreview.cx, sizePreview.cy, NULL, 0, 0, WHITENESS);

					for (int i = 0; i < vImages.size(); i++) {
						if (CxImage * p = vImages [i]) {
							CRect rc (CPoint(0, y), CSize(sizePreview.cx, p->GetHeight ()));
							CDC dcHead;
							CBitmap bmpHead;
							CPen pen(PS_DASH, 1, Color::rgbLightGray);

							dcHead.CreateCompatibleDC(&dcMem);
							bmpHead.Attach(p->MakeBitmap());
							CBitmap* pHead = dcHead.SelectObject(&bmpHead);
							CPen* pPen = dcMem.SelectObject(&pen);

							dcMem.BitBlt(rc.left, rc.top, rc.Width(), rc.Height(), &dcHead, 0, 0, SRCCOPY);
							
							if (i > 0) {
								dcMem.MoveTo(0, y);
								dcMem.LineTo(sizePreview.cx, y);
								y++;
							}

							y += p->GetHeight();
							dcMem.SelectObject(pPen);
							dcHead.SelectObject(pHead);
							delete p;
						}
					}

					CxImageBMPEx img;

					VERIFY(img.CreateFromHBITMAP(bmpMem));

					dcMem.SelectObject(pMem);

					CxImageBMPEx bmp (img);
					CBuffer data = bmp.GetBuffer();
					CBuffer header = CBuffer(
						"HTTP/1.1 " + strResponse + "\r\n"
						"Date: " + strTime + "\r\n"
						"Accept-Ranges: bytes\r\n"
						"Server: " + strServer + "\r\n"
						"Content-Length: " + std::to_string((__int64)data.size()) + "\r\n"
						"Keep-Alive: timeout=2, max=99\r\n"
						"Connection: Keep-Alive\r\n"
						"Content-Type: image/bmp\r\n\r\n");

					return header + data;
				}
			}
			//else 
			//	return CBuffer ("HTTP/1.1 302 Found\r\nLocation: " + ((string)w2a (GetIpAddress ()) + "/index.html") + "\r\n");
		}
	}

	return CBuffer ( 
		"HTTP/1.1 " + strResponse + "\r\n"
		"Date: " + strTime + "\r\n"
		"Server: " +  + "\r\n"
		"Content-Length: " + std::to_string ((__int64)strFile.length ()) + "\r\n"
		"Connection: Closed\r\n"
		"Content-Type: text/html; charset=iso-8859-1\r\n\r\n" +
		strFile);
}

static DWORD CALLBACK HttpRequestThread(LPVOID lpData)
{
	using namespace std;
	const CTime tmStart = CTime::GetCurrentTime ();

	ASSERT (lpData);
	HTTP_REQUEST_STRUCT * p = (HTTP_REQUEST_STRUCT *)lpData;
	WSADATA wsaData = { 0 };

	if (::WSAStartup(MAKEWORD(2, 1), &wsaData) != 0) {
		//TRACEF("WSAStartup failed: " + FormatMessage(GetLastError()));
		return 0;
	}

	for (CTimeSpan tm = CTime::GetCurrentTime () - tmStart; p->m_sock != INVALID_SOCKET && tm.GetTotalSeconds () <= 5; tm = CTime::GetCurrentTime () - tmStart) {
	//while (p->m_sock != INVALID_SOCKET) {
		timeval timeout = { 0, 100 };
		fd_set readfds;
		char buffer[1024] = { 0 };

		readfds.fd_count = 1;
		readfds.fd_array[0] = p->m_sock;

		if (::select(0, &readfds, NULL, NULL, &timeout) > 0) {
			int nLen = ARRAYSIZE(buffer);

			memset (buffer, 0, ARRAYSIZE (buffer));
			int lBytesRead = ::recv(p->m_sock, buffer, nLen, 0);

			if (lBytesRead > 0) {
				string strFile;

				TRACEF(buffer);
				CBuffer s = processHttpRequest(buffer);

				//TRACEF (s.ToString ().c_str ());
				int nSend = ::send(p->m_sock, (const char *)s.get (), s.size(), 0);
				ASSERT (nSend == s.size ());

				{
					CString str = a2w(s.ToString().c_str ());

					str.MakeLower();

					if (str.Find(_T("Connection: Close")) != -1) {
						::closesocket (p->m_sock);
						p->m_sock = INVALID_SOCKET;
					}
				}
			}
		}
	}

	closesocket(p->m_sock);

	return 0;
}

LRESULT CALLBACK HttpThread (LPVOID lpData)
{
	u_short nPort = lpData ? (u_short)lpData : 80;
	SOCKADDR_IN addrTCP;
	WSADATA wsaData = { 0 };

	if (::WSAStartup(MAKEWORD(2, 1), &wsaData) != 0) {
		//TRACEF("WSAStartup failed: " + FormatMessage(GetLastError()));
		return 0;
	}

	memset(&addrTCP, 0, sizeof(addrTCP));
	addrTCP.sin_family = AF_INET;
	addrTCP.sin_addr.s_addr = htonl(INADDR_ANY);
	addrTCP.sin_port = htons(nPort);

	SOCKET sockTCP = socket(AF_INET, SOCK_STREAM, 0);

	TRACER (_T ("HttpThread started on port: ") + ToString (nPort));

	if (sockTCP != INVALID_SOCKET)
	{
		int iOption = 1;					// set value to turn options on
		long lRet;

		lRet = setsockopt(sockTCP, SOL_SOCKET, SO_KEEPALIVE, (char*)&iOption, sizeof(iOption));
		lRet = setsockopt(sockTCP, SOL_SOCKET, SO_REUSEADDR, (char*)&iOption, sizeof(iOption));
		//		lRet = setsockopt( m_sockTCP.m_sock, SOL_SOCKET, SO_NBIO,      (char*)&iOption, sizeof(iOption) );
		lRet = bind(sockTCP, (const sockaddr *)&addrTCP, sizeof(addrTCP));

		if (0 == lRet)
		{
			lRet = listen(sockTCP, 5);

			while (1) {
				HTTP_REQUEST_STRUCT * p = new HTTP_REQUEST_STRUCT;

				p->m_nLen = sizeof(p->m_sin);
				p->m_sock = ::accept(sockTCP, (struct sockaddr *)&p->m_sin, &p->m_nLen);

				//TRACER (_T ("accept: ") + (CString)a2w (inet_ntoa(p->m_sin.sin_addr)));

				if (p->m_sock != INVALID_SOCKET) {
					DWORD dw = 0;
					HANDLE h = ::CreateThread(NULL, 0, HttpRequestThread, (LPVOID)p, 0, &dw);
				}
			}
		}
	}

	return 0;
}

