#if !defined(AFX_DEBUGPAGE_H__AA242444_10E0_44E5_91FF_2C9D673CB0AB__INCLUDED_)
#define AFX_DEBUGPAGE_H__AA242444_10E0_44E5_91FF_2C9D673CB0AB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DebugPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDebugPage dialog

class CDebugPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CDebugPage)

// Construction
public:
	CDebugPage();
	~CDebugPage();

// Dialog Data
	//{{AFX_DATA(CDebugPage)
	enum { IDD = IDD_DEBUG_PAGE };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CDebugPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HBRUSH m_hbrDlg;

	// Generated message map functions
	//{{AFX_MSG(CDebugPage)
	afx_msg void OnClipboard();
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEBUGPAGE_H__AA242444_10E0_44E5_91FF_2C9D673CB0AB__INCLUDED_)
