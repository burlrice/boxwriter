// Sw0848Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "Sw0848Dlg.h"
#include "OdbcDatabase.h"
#include "OdbcRecordset.h"
#include "OdbcTable.h"
#include "Parse.h"
#include "Extern.h"
#include "Sw0848FieldsDlg.h"
#include "Debug.h"

using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace ListGlobals;
using namespace FoxjetCommon;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
using namespace Sw0848Dlg;

CPair::CPair (const CPair & rhs)
:	m_strInput (rhs.m_strInput),
	m_strOutput (rhs.m_strOutput)
{
}

CPair & CPair::operator = (const CPair & rhs)
{
	if (&rhs != this) {
		m_strInput = rhs.m_strInput;
		m_strOutput = rhs.m_strOutput;
	}

	return * this;
}

CString CPair::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:	return m_strInput;	
	case 1: return m_strOutput;
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CSw0848Dlg dialog


CSw0848Dlg::CSw0848Dlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg (IDD_SW0848_MATRIX /* FoxjetDatabase::IsMatrix () ? IDD_SW0848_MATRIX : IDD_SW0848 */ , pParent)
{
	//{{AFX_DATA_INIT(CSw0848Dlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSw0848Dlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSw0848Dlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_ADD);
	m_buttons.DoDataExchange (pDX, BTN_EDIT);
	m_buttons.DoDataExchange (pDX, BTN_DELETE);
}


BEGIN_MESSAGE_MAP(CSw0848Dlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CSw0848Dlg)
	ON_BN_CLICKED(BTN_INPUTDSN, OnInputdsn)
	ON_BN_CLICKED(BTN_OUTPUTDSN, OnOutputdsn)
	ON_BN_CLICKED(BTN_ADD, OnAdd)
	ON_BN_CLICKED(BTN_DELETE, OnDelete)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_CBN_SELCHANGE(CB_INPUTTABLE, OnSelchangeInputtable)
	ON_CBN_SELCHANGE(CB_OUTPUTTABLE, OnSelchangeOutputtable)
	ON_NOTIFY(NM_DBLCLK, LV_FIELDS, OnDblclkFields)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSw0848Dlg message handlers

void CSw0848Dlg::Load (CSw0848Map & map)
{
	DBMANAGETHREADSTATE (theApp.m_Database);

	SETTINGSSTRUCT s;
	CStringArray v;
	int nIndex = 0;

	map.m_v.RemoveAll ();

	GetSettingsRecord (theApp.m_Database, 0, _T ("sw0848"), s);
	FromString (s.m_strData, v);

	map.m_dsn.m_strInput	= GetParam (v, nIndex++);
	map.m_table.m_strInput	= GetParam (v, nIndex++);

	map.m_dsn.m_strOutput	= GetParam (v, nIndex++);
	map.m_table.m_strOutput	= GetParam (v, nIndex++);

	for (int i = nIndex; i < v.GetSize (); i += 2) {
		CPair p;

		p.m_strInput		= GetParam (v, i);
		p.m_strOutput		= GetParam (v, i + 1);

		map.m_v.Add (p);
	}
}

void CSw0848Dlg::Save (CSw0848Map & map)
{
	DBMANAGETHREADSTATE (theApp.m_Database);

	CStringArray v;
	SETTINGSSTRUCT s;

	v.Add (map.m_dsn.m_strInput);
	v.Add (map.m_table.m_strInput);

	v.Add (map.m_dsn.m_strOutput);
	v.Add (map.m_table.m_strOutput);

	for (int i = 0; i < map.m_v.GetSize (); i++) {
		CPair & p = map.m_v [i];

		v.Add (p.m_strInput);
		v.Add (p.m_strOutput);
	}

	s.m_lLineID = 0;
	s.m_strKey = _T ("sw0848");
	s.m_strData = ToString (v);

	if (!UpdateSettingsRecord (theApp.m_Database, s))
		VERIFY (AddSettingsRecord (theApp.m_Database, s));
}

void CSw0848Dlg::OnAdd() 
{
	CSw0848FieldsDlg dlg (this);

	Init (dlg);

	if (dlg.DoModal () == IDOK) {
		CPair * p = new CPair ();

		p->m_strInput = dlg.m_strInput;
		p->m_strOutput = dlg.m_strOutput;

		m_lv.InsertCtrlData (p);
	}	
}

void CSw0848Dlg::OnEdit() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		if (CPair * p = (CPair *)m_lv.GetCtrlData (sel [0])) {
			CSw0848FieldsDlg dlg (this);

			Init (dlg);
			dlg.m_strInput = p->m_strInput;
			dlg.m_strOutput = p->m_strOutput;

			if (dlg.DoModal () == IDOK) {
				p->m_strInput = dlg.m_strInput;
				p->m_strOutput = dlg.m_strOutput;

				m_lv.UpdateCtrlData (p);
			}	
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CSw0848Dlg::OnDelete() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		if (MsgBox (* this, LoadString (IDS_CONFIRMDELETE), LoadString (IDS_CONFIRM), MB_YESNO) == IDNO)
			return;

		for (int i = 0; i < sel.GetSize (); i++)
			m_lv.DeleteCtrlData (sel [i]);
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CSw0848Dlg::Init (CSw0848FieldsDlg & dlg)
{
	CString strInputDSN, strOutputDSN, strInputTable, strOutputTable, strInputSQL, strOutputSQL;
	CComboBox * pInputTable = (CComboBox *)GetDlgItem (CB_INPUTTABLE);
	CComboBox * pOutputTable = (CComboBox *)GetDlgItem (CB_OUTPUTTABLE);

	ASSERT (pInputTable);
	ASSERT (pOutputTable);

	if (pInputTable->GetCurSel () == CB_ERR || pOutputTable->GetCurSel () == CB_ERR)
		return;

	GetDlgItemText (TXT_INPUTDSN, strInputDSN);
	GetDlgItemText (TXT_OUTPUTDSN, strOutputDSN);

	pInputTable->GetLBText (pInputTable->GetCurSel (), strInputTable);
	pOutputTable->GetLBText (pOutputTable->GetCurSel (), strOutputTable);

	try {
		COdbcDatabase dbInput (_T (__FILE__), __LINE__), dbOutput (_T (__FILE__), __LINE__);

		dbInput.Open (strInputDSN);
		dbOutput.Open (strOutputDSN);

		strInputSQL.Format (_T ("SELECT * FROM [%s]"), strInputTable);
		strOutputSQL.Format (_T ("SELECT * FROM [%s]"), strOutputTable);

		COdbcRecordset rstInput (dbInput), rstOutput (dbOutput);

		rstInput.Open (strInputSQL);
		rstOutput.Open (strOutputSQL);

		for (int i = 0; i < rstInput.GetODBCFieldCount (); i++) 
			dlg.m_vInput.Add (rstInput.GetFieldName (i));

		for (int i = 0; i < rstOutput.GetODBCFieldCount (); i++) 
			dlg.m_vOutput.Add (rstOutput.GetFieldName (i));
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
}

void CSw0848Dlg::OnOK()
{
	CComboBox * pInputTable = (CComboBox *)GetDlgItem (CB_INPUTTABLE);
	CComboBox * pOutputTable = (CComboBox *)GetDlgItem (CB_OUTPUTTABLE);

	ASSERT (pInputTable);
	ASSERT (pOutputTable);

	GetDlgItemText (TXT_INPUTDSN, m_map.m_dsn.m_strInput);
	GetDlgItemText (TXT_OUTPUTDSN, m_map.m_dsn.m_strOutput);

	pInputTable->GetLBText (pInputTable->GetCurSel (), m_map.m_table.m_strInput);
	pOutputTable->GetLBText (pOutputTable->GetCurSel (), m_map.m_table.m_strOutput);

	m_map.m_v.RemoveAll ();

	for (int i = 0; i < m_lv->GetItemCount (); i++) 
		if (CPair * p = (CPair *)m_lv.GetCtrlData (i)) 
			m_map.m_v.Add (CPair (* p));

	Save (m_map);

	FoxjetCommon::CEliteDlg::OnOK ();
}

BOOL CSw0848Dlg::OnInitDialog() 
{
	using namespace ListCtrlImp;

	CString strSection = defElements.m_strRegSection + _T ("\\CSw0848Dlg");
	CArray <CColumn, CColumn> vCols;

	FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_INPUT)));
	vCols.Add (CColumn (LoadString (IDS_OUTPUT)));

	m_lv.Create (LV_FIELDS, vCols, this, defElements.m_strRegSection);

	Load (m_map);

	for (int i = 0; i < m_map.m_v.GetSize (); i++) 
		m_lv.InsertCtrlData (new CPair (m_map.m_v [i]));
	
	SetDlgItemText (TXT_INPUTDSN, m_map.m_dsn.m_strInput);
	SetDlgItemText (TXT_OUTPUTDSN, m_map.m_dsn.m_strOutput);

	try {
		if (m_map.m_dsn.m_strInput.GetLength ()) {
			COdbcDatabase db (_T (__FILE__), __LINE__);
	
			db.Open (m_map.m_dsn.m_strInput);

			CComboBox * pTable = (CComboBox *)GetDlgItem (CB_INPUTTABLE);

			for (int i = 0; i < db.GetTableCount (); i++) {
				CString strName = db.GetTable (i)->GetName ();
				int nIndex = pTable->AddString (strName);

				if (!strName.CompareNoCase (m_map.m_table.m_strInput)) 
					pTable->SetCurSel (nIndex);
			}

			if (pTable->GetCurSel () == CB_ERR)
				pTable->SetCurSel (0);

			db.Close ();
		}

		if (m_map.m_dsn.m_strOutput.GetLength ()) {
			COdbcDatabase db (_T (__FILE__), __LINE__);

			db.Open (m_map.m_dsn.m_strOutput);

			CComboBox * pTable = (CComboBox *)GetDlgItem (CB_OUTPUTTABLE);

			for (int i = 0; i < db.GetTableCount (); i++) {
				CString strName = db.GetTable (i)->GetName ();
				int nIndex = pTable->AddString (strName);

				if (!strName.CompareNoCase (m_map.m_table.m_strOutput)) 
					pTable->SetCurSel (nIndex);
			}

			if (pTable->GetCurSel () == CB_ERR)
				pTable->SetCurSel (0);

			db.Close ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSw0848Dlg::OnInputdsn() 
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_INPUTTABLE);

	ASSERT (pTable);
	pTable->ResetContent ();

	try {
		COdbcDatabase db (_T (__FILE__), __LINE__);

		if (db.Open (NULL)) {
			CString strDSN = Extract (db.GetConnect (), _T ("DSN="), _T (";"));

			SetDlgItemText (TXT_INPUTDSN, strDSN);

			if (!db.GetTableCount ()) {
				CString str;

				str.Format (LoadString (IDS_DSNHASNOTABLES), strDSN);
				MsgBox (* this, str);
			}
			else {
				CStringArray v;

				for (int i = 0; i < db.GetTableCount (); i++) 
					v.Add (db.GetTable (i)->GetName ());

				v.Add (CSw0848FieldsDlg::m_strTimestamp);

				for (int i = 0; i < v.GetSize (); i++) {
					CString strName = v [i];
					int nIndex = pTable->AddString (strName);

					if (!strName.CompareNoCase (m_map.m_table.m_strInput)) 
						pTable->SetCurSel (nIndex);
				}

				if (pTable->GetCurSel () == CB_ERR)
					pTable->SetCurSel (0);
			}

			db.Close ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	MatchFields ();
}

void CSw0848Dlg::OnOutputdsn() 
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_OUTPUTTABLE);

	ASSERT (pTable);
	pTable->ResetContent ();

	try {
		COdbcDatabase db (_T (__FILE__), __LINE__);

		if (db.Open (NULL)) {
			CString strDSN = Extract (db.GetConnect (), _T ("DSN="), _T (";"));

			SetDlgItemText (TXT_OUTPUTDSN, strDSN);

			if (!db.GetTableCount ()) {
				CString str;

				str.Format (LoadString (IDS_DSNHASNOTABLES), strDSN);
				MsgBox (* this, str);
			}
			else {
				for (int i = 0; i < db.GetTableCount (); i++) {
					CString strName = db.GetTable (i)->GetName ();
					int nIndex = pTable->AddString (strName);

					if (!strName.CompareNoCase (m_map.m_table.m_strOutput)) 
						pTable->SetCurSel (nIndex);
				}

				if (pTable->GetCurSel () == CB_ERR)
					pTable->SetCurSel (0);
			}

			db.Close ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	MatchFields ();
}


void CSw0848Dlg::OnSelchangeInputtable() 
{
	MatchFields ();
}

void CSw0848Dlg::OnSelchangeOutputtable() 
{
	MatchFields ();
}

int CSw0848Dlg::FindTable (COdbcDatabase & db, const CString & strTable)
{
	for (int i = 0; i < db.GetTableCount (); i++) 
		if (const COdbcTable * p = db.GetTable (i)) 
			if (strTable.CompareNoCase (p->GetName ()) == 0)
				return i;

	return -1;
}

void CSw0848Dlg::MatchFields ()
{
	CString strInputDSN, strOutputDSN, strInputTable, strOutputTable, strInputSQL, strOutputSQL;
	CComboBox * pInputTable = (CComboBox *)GetDlgItem (CB_INPUTTABLE);
	CComboBox * pOutputTable = (CComboBox *)GetDlgItem (CB_OUTPUTTABLE);

	ASSERT (pInputTable);
	ASSERT (pOutputTable);

	if (pInputTable->GetCurSel () == CB_ERR || pOutputTable->GetCurSel () == CB_ERR)
		return;

	GetDlgItemText (TXT_INPUTDSN, strInputDSN);
	GetDlgItemText (TXT_OUTPUTDSN, strOutputDSN);

	pInputTable->GetLBText (pInputTable->GetCurSel (), strInputTable);
	pOutputTable->GetLBText (pOutputTable->GetCurSel (), strOutputTable);

	m_lv.DeleteCtrlData ();

	try {
		COdbcDatabase dbInput (_T (__FILE__), __LINE__), dbOutput (_T (__FILE__), __LINE__);

		dbInput.Open (strInputDSN);
		dbOutput.Open (strOutputDSN);

		if (FindTable (dbInput, strInputTable) == -1)
			return;
		if (FindTable (dbOutput, strOutputTable) == -1)
			return;

		strInputSQL.Format (_T ("SELECT * FROM [%s]"), strInputTable);
		strOutputSQL.Format (_T ("SELECT * FROM [%s]"), strOutputTable);

		COdbcRecordset rstInput (dbInput), rstOutput (dbOutput);

		rstInput.Open (strInputSQL);
		rstOutput.Open (strOutputSQL);

		for (int i = 0; i < rstInput.GetODBCFieldCount (); i++) {
			CODBCFieldInfo info;

			rstInput.GetODBCFieldInfo (i, info);

			CString strInput = info.m_strName;
			bool bFound = false;

			for (int j = 0; j < rstOutput.GetODBCFieldCount (); j++) {
				CODBCFieldInfo infoOutput;

				rstOutput.GetODBCFieldInfo (j, infoOutput);
				CString strOutput = infoOutput.m_strName;

				if (!strInput.CompareNoCase (strOutput)) {
					CPair * p = new CPair ();

					p->m_strInput = rstInput.GetFieldName (i);
					p->m_strOutput = rstOutput.GetFieldName (j);
					m_lv.InsertCtrlData (p);
					bFound = true;
					break;
				}
			}

			if (!bFound) {
				CPair * p = new CPair ();

				p->m_strInput = rstInput.GetFieldName (i);
				m_lv.InsertCtrlData (p);
			}
		}

		{ // map timestamp
			bool bFound = false;
		
			for (int j = 0; !bFound && j < rstOutput.GetODBCFieldCount (); j++) {
				CODBCFieldInfo info;
				
				rstOutput.GetODBCFieldInfo (j, info);

				CString strOutput = info.m_strName;
				
				switch (info.m_nSQLType) {
				case SQL_DATE:
				case SQL_TIME:
				case SQL_TIMESTAMP:
					if (!IsMapped (strOutput)) {
						CPair * p = new CPair ();

						p->m_strInput = CSw0848FieldsDlg::m_strTimestamp;
						p->m_strOutput = rstOutput.GetFieldName (j);
						m_lv.InsertCtrlData (p);
						bFound = true;
					}
					break;
				}
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
}

bool CSw0848Dlg::IsMapped (const CString & strOutput)
{
	for (int i = 0; i < m_lv->GetItemCount (); i++) 
		if (CPair * p = (CPair *)m_lv.GetCtrlData (i)) 
			if (!p->m_strOutput.CompareNoCase (strOutput))
				return true;

	return false;
}

void CSw0848Dlg::OnDblclkFields(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();

	if (pResult)
		* pResult = 0;
}
