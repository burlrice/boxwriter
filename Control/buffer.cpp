#include "stdafx.h"
#include "buffer.h"
#include "Parse.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;

CBuffer encode(const std::vector <unsigned char> & v)
{
	unsigned char * p = new unsigned char[v.size()];

	for (size_t i = 0; i < v.size(); i++)
		p[i] = v[i];

	return CBuffer(p, v.size());
}

CBuffer encode(const std::string & str)
{
	vector <wstring> vName = explode (CString (a2w (str.c_str ())));
//	vector <string> vName = explode(str, '.');
	vector <unsigned char> v;

	for (size_t i = 0; i < vName.size(); i++) {
		int n = vName[i].size();

		ASSERT(n <= 255);

		v.push_back(n);

		for (size_t j = 0; j < n; j++)
			v.push_back((unsigned char)vName[i][j]);
	}

	return encode(v);
};

CBuffer operator+(const CBuffer & lhs, const CBuffer & rhs)
{
	size_t n = lhs.size() + rhs.size();
	unsigned char * p = new unsigned char[n];

	memcpy(p, lhs.get(), lhs.size());
	memcpy(&p [lhs.size ()], rhs.get(), rhs.size());

	return CBuffer(p, n);
}

