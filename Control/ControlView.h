// ControlView.h : interface of the CControlView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONTROLVIEW_H__3855D46F_9729_11D5_8F07_00045A47C31C__INCLUDED_)
#define AFX_CONTROLVIEW_H__3855D46F_9729_11D5_8F07_00045A47C31C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TypeDefs.h"
#include "ImageView.h"
#include "DiagDlg.h"	// Added by ClassView
#include "UdpThread.h"
#include "ProgressDlg.h"
#include "ServerThread.h"
#include "StdCommDlg.h"
#include "ExportImpl.h"
#include "ImportImpl.h"
#include "RoundButton2.h"
#include "HeadInfoCtrl.h"
#include "InfoDlg.h"
#include "LineTab.h"
#include "LocalDbThread.h"
#include "Head.h"
#include "SerialPacket.h"
#include "NextThread.h"
#include "InkSerialNumberDlg.h"
#include "DiagnosticSingleLock.h"

namespace ControlView
{

	class CComm : public FoxjetCommon::StdCommDlg::CCommItem
	{
	public:
		CComm ();
		CComm (const CComm & rhs);
		CComm & operator = (const CComm & rhs);
		virtual ~CComm ();

		HANDLE	m_hThread;
		HANDLE	m_hComm;
		DWORD	m_dwThreadID;
		HWND	m_hControlWnd;

		CDiagnosticCriticalSection m_cs;

		static const DWORD m_dwTimeout;
		static int m_nInstances;
	};

	typedef struct tagWM_INK_CODE_CHANGE_STRUCT
	{
		tagWM_INK_CODE_CHANGE_STRUCT (int nDoModalResult, ULONG lHeadID, bool bDismiss) : m_nDoModalResult (nDoModalResult), m_lHeadID (lHeadID), m_bDismiss (bDismiss) { }

		int m_nDoModalResult;
		ULONG m_lHeadID;
		bool m_bDismiss;
	} WM_INK_CODE_CHANGE_STRUCT;

}; // namespace ControlView

class CControlView : public CFormView
{
	friend class CInfoDlg;
	friend class CServerThread;

	DECLARE_DYNCREATE(CControlView)

protected: // create from serialization only
	CControlView();

public:
	virtual ~CControlView();

	class CPhotocellCount
	{ 
	public:
		CPhotocellCount (__int64 lCount = 0, ULONG lLineID = -1, const CString & strLine = _T ("")) 
		:	m_count (lCount),
			m_lLineID (lLineID),
			m_strLine (strLine),
			m_tm (COleDateTime::GetCurrentTime ())
		{
		}

		CString ToString ();
		bool FromString (const CString & str);

		static ULONG CalcChecksum (const CStringArray & v);

		//__int64 m_lCount;
		Head::CHead::CHANGECOUNTSTRUCT m_count;
		COleDateTime m_tm;
		ULONG m_lLineID;
		CString m_strLine;
	};

public:
	//{{AFX_DATA(CControlView)
	//}}AFX_DATA
	
	FoxjetUtils::CRoundButton2 m_btnStart;
	FoxjetUtils::CRoundButton2 m_btnIdle;
	FoxjetUtils::CRoundButton2 m_btnStop;
	FoxjetUtils::CRoundButton2 m_btnEdit;
	FoxjetUtils::CRoundButton2 m_btnLogin;
	FoxjetUtils::CRoundButton2 m_btnConfig;
	FoxjetUtils::CRoundButton2 m_btnUser;
	FoxjetUtils::CRoundButton2 m_btnCount;
	FoxjetUtils::CRoundButton2 m_btnInfo;
	FoxjetUtils::CRoundButton2 m_btnBox;
	FoxjetUtils::CRoundButton2 m_btnExit;
	FoxjetUtils::CRoundButton2 m_btnReport;
	FoxjetUtils::CRoundButton2 m_btnZoomFit;
	FoxjetUtils::CRoundButton2 m_btnZoomIn;
	FoxjetUtils::CRoundButton2 m_btnZoomOut;
	FoxjetUtils::CRoundButton2 m_btnLimitedConfig;

	FoxjetUtils::CRoundButtonStyle m_styleDefault;

	CLineTab m_tabLine;

	CBitmap m_bmpStrobe;
	CBitmap m_bmpStrobeGreen;
	CBitmap m_bmpStrobeYellow;
	CBitmap m_bmpStrobeRed;
	CBitmap m_bmpStrobeGray;
	CBitmap m_bmpStart;
	CBitmap m_bmpIdle;
	CBitmap m_bmpResume;
	CBitmap m_bmpStop;
	CBitmap m_bmpLogin;
	CBitmap m_bmpLogout;

	CImageView *	m_pPreview;

// Attributes
public:
	CString m_sSerialData;
	CControlDoc* GetDocument();
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CControlView)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	bool m_bNetIODevConnected;
	void ChangeUserElementData ( void );
	void ChangeCounts ();
	void HeadCfgUpdated (const FoxjetCommon::CLongArray * pvNew = NULL);
	void DoAmsCycle ();
	void UpdateOptionsTable (CArray <FoxjetDatabase::OPTIONSSTRUCT, FoxjetDatabase::OPTIONSSTRUCT &> & v);
	ULONG GetAdminID ();
	bool GetOptionLink (ULONG lGroupID, ULONG lOptionID, FoxjetDatabase::OPTIONSLINKSTRUCT & link);
	bool ReconnectDatabase (Head::CHead * pHead);

	bool GetLineState (ULONG lExcludeID, int & nLowInk, int & nOther);

	bool StopTask ( LPCTSTR pLine = NULL, bool bPrompt = true );
	int StartTask ( LPCTSTR pLine = NULL, LPCTSTR pTask = NULL, bool bStartup = false, const CString & strKeyValue = _T (""), int nRemote = -1, int nPeriod = 0, Head::CHead::CHANGECOUNTSTRUCT * pCount = NULL);
	int IdleTask ( const CString *pLine );
	int ResumeTask ( const CString *pLine );

	void UpdateSerialData (CString str, bool bFormat = true);
	void ResetScannerInfo ();
	std::map<ULONG, __int64> GetCounts();

	unsigned long m_lTaskID;
	FoxjetDatabase::TASKSTATE m_eTaskState;

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:
	
public:
	void BeginLocalDbThreads ();
	void EndLocalDbThreads ();
	void BeginCommThreads ();
	void EndCommThreads ();

	ControlView::CComm * GetCommHandle (int nPort);

	void SetPreview (bool bPreview);
	bool GetPreview () const { return m_bPreview; }

	double GetZoom () const;
	bool SetZoom (double dZoom);

	static ULONG CALLBACK CommThreadFunc (LPVOID lpData);
	static ULONG CALLBACK ColorFunc (LPVOID lpData);
	static void KillThread (CWinThread ** pThread);

	LARGE_INTEGER m_counter;
	LARGE_INTEGER m_freq;

	static void CALLBACK DisplayStatus (const CString & strDisplay, bool bDisplay);

protected:

	CArray <ControlView::CComm *, ControlView::CComm *> m_vCommThreads;
	CArray <CLocalDbThread *, CLocalDbThread *> m_vLocalDbThreads;
	CStringArray m_vDiagnostic;
	CSize m_sizeInit;

	int m_nProcessTimer;
	CProgressDlg *m_pProgressDlg;
	CInfoDlg * m_pdlgInfo;
	CUdpThread *m_pUdpThread;
	UINT m_nDataSyncInterval;
	CDiagnosticCriticalSection m_CtrlSentinel;
	CImageList m_HeadState;
	CTime m_StrobeResetTime;
	eHeadInfModes m_HeadInfMode;
	CMap <DWORD, DWORD, eHeadStatus, eHeadStatus> m_mapHeadError;
	CServerThread * m_pServerThread;
	NEXT::CNextThread * m_pNextThread;
	CArray <Head::CHead *, Head::CHead *> m_vObsolete;
	CExport * m_pExport;
	CImport * m_pImport;
	int m_nFontLarge;
	int m_nFontSmall;
	CFont m_fntLarge;
	CFont m_fntMedium;
	CFont m_fntSmall;
	bool m_bNetworkConnected;
	CTime m_tmPing;
	CMapStringToString m_vTask;
	CMapStringToString m_vKeyValue;
	ULONG m_lTimerTab;


// Generated message map functions
protected:
	
	static void CALLBACK HeadConfigCallback (ULONG lHeadID, ULONG lOffset);
	static void CALLBACK InkCodeCallback (ULONG lHeadID);

	void ConnectDatabases (CArray <Head::CHead::TM_SET_IO_ADDRESS_STRUCT *, Head::CHead::TM_SET_IO_ADDRESS_STRUCT *> & v);
	bool DoHeadConfigCheck (CString & str);
	bool BeginThread (Head::CHead::TM_SET_IO_ADDRESS_STRUCT * pParam);
	int OnSerialShopOrder (CSerialPacket & packet, CProdLine & line);
	void Resize ();
	void GetPanelIDs (ULONG lLineID, ULONG lCardID, CArray <UINT, UINT> & vPanelIDs);
	void FillLineCtrl ();

	int		m_nGreenIndex;
	int		m_nRedIndex;
	int		m_nYellowIndex;
	int		m_nGrayIndex;
	bool	m_bPreview;
	UINT	m_nTimerStatus;
	UINT	m_nTimerTab;
	UINT	m_nTimerRefresh;
	UINT	m_nTimerTime;
	UINT	m_nTimerStartMenu;
	bool	m_bKeyboard;
	int		m_nLastLineIndex;
	bool	m_bShowInfo;
	int		m_nLineIndex;
	CFont	m_fntTimer;

	CStringArray m_vDbStrings;

public:
	typedef struct tagSOCKETERRORSTRUCT
	{
		tagSOCKETERRORSTRUCT (ULONG lLineID = -1, ULONG lTaskID = -1, bool bError = false) 
			:	m_lLineID (lLineID),
				m_lTaskID (lTaskID),
				m_bError  (bError)
		{
		}

		ULONG m_lLineID;
		ULONG m_lTaskID;
		bool m_bError;
	} SOCKETERRORSTRUCT;

protected:

	afx_msg LRESULT OnColorsChanged (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketSetErrorState (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDoAms (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnIsWaiting (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetWaiting (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHeadDisconnect ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnRestartTask (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetInkCodes (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketRecieveInkCode (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnLogInkCodeReport (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetHeadWarnings (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetHeadWarnings (WPARAM wParam, LPARAM lParam);

	CString GetLine (int nIndex, const CSerialPacket & packet);

public:
	afx_msg LRESULT OnHeadDelete (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHeadSerialData ( WPARAM wParam, LPARAM lParam );

protected:
	afx_msg LRESULT OnAuxSerialData ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnAuxPS2Data ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnAuxConnectStateChanged ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnAuxDigitalInputs ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnHeadProcessing ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnInitialStartup ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSyncHeadData ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnComPortData ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnHeadStatusUpdate ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnHeadConnect(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnIdleTask(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResumeTask(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCalcProductLen (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHeadObsoleteFirmware (WPARAM wParam, LPARAM lParam);	
	afx_msg LRESULT OnUpdatePreview (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnUpdateImage (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHeadErrorCleared (WPARAM wParam, LPARAM lParam); 

	afx_msg LRESULT OnSetHeadError (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetHeadWarning (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetStrobe (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRefreshPreview (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTriggerWDT (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPostPhotocell (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnXmlData(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSaveXmlData(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnSocketStartTask (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketStartTaskDatabase (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketLoadTask (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketStopTask (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketIdleTask (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketResumeTask (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketGetUserElements (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketSetUserElements (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketSetErrorSocket (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketGetCurrentTask (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketGetCurrentState (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketGetCount (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketSetCount (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketGetCountMax (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketSetCountMax (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketClose (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketGetHeads (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketGetElements (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketAddElement (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketUpdateElement (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketDeleteElement (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketDbBuild (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketDbGetNext (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketDbIsReady (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketDbBeginUpdate (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketDbEndUpdate (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCheckHeadErrors (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketRefreshPreview (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketConnectDb (WPARAM wParam, LPARAM lParam);
	
	afx_msg LRESULT OnSocketPrinterEOP (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketPrinterSOP (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketPrinterStateChange (WPARAM wParam, LPARAM lParam);

	//afx_msg LRESULT OnGetProdLines (WPARAM wParam, LPARAM lParam);
	//afx_msg LRESULT OnGetActiveHeads (WPARAM wParam, LPARAM lParam);
	//afx_msg LRESULT OnGetProdLineID (WPARAM wParam, LPARAM lParam);
	//afx_msg LRESULT OnGetThreadByUID (WPARAM wParam, LPARAM lParam);
	//afx_msg LRESULT OnGetProdLineIdByHeadId (WPARAM wParam, LPARAM lParam);
	//afx_msg LRESULT OnGetLineName (WPARAM wParam, LPARAM lParam);
	//afx_msg LRESULT OnGetSerialDataPtr (WPARAM wParam, LPARAM lParam);
	//afx_msg LRESULT OnGetSw0848Thread (WPARAM wParam, LPARAM lParam);


	void InterfaceNotify (const CString & strCmd);
	
	afx_msg void OnDeleteMemStore (void);
	afx_msg void OnUpdateMessages (void);
	afx_msg void OnViewRefreshpreview();
	afx_msg void OnLine (UINT nID);

	afx_msg LRESULT OnKeepAlive (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnHeadConnectionStateChanged (WPARAM wParam, LPARAM lParam);

	afx_msg void OnPhotocellSim1 ();
	afx_msg void OnPhotocellSim2 ();
	afx_msg LRESULT OnGenImageDebug (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPingStatus (WPARAM wParam, LPARAM lParam);

	afx_msg void OnStart ();
	afx_msg void OnDbStart ();
	afx_msg void OnIdle ();
	afx_msg void OnStop ();
	afx_msg void OnEdit ();
	afx_msg void OnLogin ();
	afx_msg void OnConfig ();
	afx_msg void OnInfo ();
	afx_msg void OnLimitedConfig ();
	afx_msg void OnCounts ();
	afx_msg void OnUserData ();
	afx_msg void OnExit ();
	afx_msg void OnBox ();
	afx_msg void OnZoomFit ();
	afx_msg void OnZoomIn ();
	afx_msg void OnZoomOut ();
	afx_msg void OnReports ();
	afx_msg void OnUpdateLogin ();

public:
	void DoStart ();
	void ViewRefreshpreview();
	LRESULT OnLocalDbBackup (WPARAM wParam, LPARAM lParam);
	void OnDbOpened ();
	void OnDbClosed ();
	void InitInkCodes ();

protected:
	virtual BOOL Create (LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	Head::CHead * StartRemoteHead (FoxjetDatabase::HEADSTRUCT & h);
	void LoadLocalDbSettings ();

	CMap <ULONG, ULONG, CString, CString &> m_vLineDisplayInfo;
	CMap <ULONG, ULONG, CString, CString &> m_vLineDebugInfo;
	CMap <ULONG, ULONG, CString, CString &> m_vLineWarningsInfo;
	CString m_strTimeFormat [3];
	HBRUSH m_hbrDlg;
	HBRUSH m_hbrDlgDisabled;

	CMap <ULONG, ULONG, CInkSerialNumberDlg *, CInkSerialNumberDlg *> m_mapInkSerialNumber;

	afx_msg void OnChangePanelTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeLineTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickProductLineTab (NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeProductLine ();
	afx_msg LRESULT OnDiagnosticData (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketSend (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketSetSerial (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketGetSerial (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSocketGetStrobe (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSql (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetNEXTBcParams (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetNEXTBcParams (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetNEXTDmParams (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetNEXTDmParams (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetTaskNames (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnAddTask (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDeleteTask (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetNextStrobe (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetNextStrobe (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetNEXTSystemParams (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetNEXTSystemParams (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRefreshImage (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetBoxID (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetNextDynamicData (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetNextPort (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNEXTTaskStart (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNEXTSetDynamicIndex (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNEXTRefineConfig (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNEXTInitFonts (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNEXTEditSession (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNEXTSetPrintDriver (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnInkUsage (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnInkUsageWarning (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnInkCodeChange (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCheckInkCodeState (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnInkCodeAccepted (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDownloadSerialData (WPARAM wParam, LPARAM lParam);



	void DrawLineDisplay (CDC & dc, UINT nIDCtl, const DRAWITEMSTRUCT & dis);
	// TODO: rem // CSize DrawInfoDisplay (CDC & dc, const CRect & rc);
	void DrawTimeDisplay (CDC & dc, CRect rc, int nIndex);

	//{{AFX_MSG(CControlView)

	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnCancelStrobe();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in ControlView.cpp
inline CControlDoc* CControlView::GetDocument()
   { return (CControlDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONTROLVIEW_H__3855D46F_9729_11D5_8F07_00045A47C31C__INCLUDED_)
