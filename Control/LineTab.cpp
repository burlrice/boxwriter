// LineTab.cpp : implementation file
//

#include "stdafx.h"
#include "control.h"
#include "LineTab.h"
#include "Color.h"
#include "ControlView.h"
#include "Registry.h"

using namespace Color;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLineTab

CLineTab::CLineTab()
{
}

CLineTab::~CLineTab()
{
}


BEGIN_MESSAGE_MAP(CLineTab, CTabCtrl)
	//{{AFX_MSG_MAP(CLineTab)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_NCPAINT()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLineTab message handlers

HBRUSH CLineTab::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	// TODO: Change any attributes of the DC here
	
	// TODO: Return a non-NULL brush if the parent's handler should not be called
	return NULL;
}

void CLineTab::OnNcPaint() 
{
	/*
	CPaintDC dc (this);
	CRect rc;

	GetWindowRect (rc);
	dc.FillRect (rc - rc.TopLeft (), &CBrush (Color::rgbRed));
	*/
	// Do not call CTabCtrl::OnNcPaint() for painting messages
}

void CLineTab::DrawTab (CDC & dc, COLORREF rgb, const CRect & rc, int nPen, int nRadius, bool bSelected, bool bMask)
{
	CPen pen (PS_SOLID, nPen, rgb);
	COLORREF rgbBorder = GetLineColor ();

	CRect rcCorner [4] = 
	{
		CRect (rc.left, rc.top, rc.left + (nRadius * 2), rc.top + (nRadius * 2)),			// top left
		CRect (rc.right - (nRadius * 2), rc.top, rc.right, rc.top + (nRadius * 2)),			// top right
		CRect (rc.left, rc.bottom - (nRadius * 2), rc.left + (nRadius * 2), rc.bottom),		// bottom left
		CRect (rc.right - (nRadius * 2), rc.bottom - (nRadius * 2), rc.right, rc.bottom),	// bottom right
	};

	/*
	dc.FrameRect (rcCorner [0], &CBrush (rgbRed));
	dc.FrameRect (rcCorner [1], &CBrush (rgbBlue));
	dc.FrameRect (rcCorner [2], &CBrush (rgbYellow));
	dc.FrameRect (rcCorner [3], &CBrush (rgbGreen));
	*/

	CPen * pPen = dc.SelectObject (&pen);

	if (bMask) {
		dc.BeginPath ();
		dc.SetPolyFillMode (WINDING);
	}

	const CPoint ptStart = CPoint (rcCorner [1].right - (nPen / 2) - 1, rcCorner [3].bottom - (nPen / 2));
	dc.MoveTo (ptStart);

	// right
	dc.LineTo (CPoint (rcCorner [1].right - (nPen / 2) - 1, rcCorner [1].top + nRadius + (nPen / 2)));

	// top right
	dc.ArcTo (
		rcCorner [1] + CPoint (-(nPen / 2), (nPen / 2)), 
		CPoint (rcCorner [1].right - (nPen / 2), rcCorner [1].top + nRadius + (nPen / 2)),
		CPoint (rcCorner [1].right - nRadius - (nPen / 2), rcCorner [1].top + (nPen / 2)));

	// top
	dc.LineTo (CPoint (rcCorner [0].left + (nPen / 2) + nRadius, rcCorner [0].top + (nPen / 2)));

	// top left
	dc.ArcTo (
		rcCorner [0] + CPoint ((nPen / 2), (nPen / 2)), 
		CPoint (rcCorner [0].left + nRadius + (nPen / 2), rcCorner [0].top + (nPen / 2)),
		CPoint (rcCorner [0].left + (nPen / 2), rcCorner [0].top + nRadius + (nPen / 2)));

	// left 
	dc.LineTo (CPoint (rcCorner [0].left + (nPen / 2), rcCorner [2].bottom - (nPen / 2)));

	// bottom
	if (!bSelected || bMask) 
		dc.LineTo (ptStart);

	if (bMask) {
		CRgn rgn;

		dc.EndPath ();
		rgn.CreateFromPath (&dc);
		dc.FillRgn (&rgn, &CBrush (rgb));
	}

	dc.SelectObject (pPen);
}

BOOL CLineTab::OnEraseBkgnd (CDC * pDC)
{
	return TRUE;
}

void CLineTab::OnPaint() 
{
	CPaintDC dcLive (this);
	CDC dc;
	CBitmap bmp;
	CRect rc;
	int nID = ::GetWindowLong (m_hWnd, GWL_ID);
	const int nPen = GetPenSize ();
	const int nRadius = GetRadius ();
	COLORREF rgbBorder = GetLineColor ();

	GetClientRect (rc);
	dc.CreateCompatibleDC (&dcLive);
	bmp.CreateCompatibleBitmap (&dcLive, rc.Width (), rc.Height ());
	CBitmap * pBitmap = dc.SelectObject (&bmp);

	DRAWITEMSTRUCT dis;
	dis.CtlType = ODT_TAB;
	dis.CtlID = GetDlgCtrlID();
	dis.hwndItem = GetSafeHwnd();
	dis.hDC = dc.GetSafeHdc();
	dis.itemAction = ODA_DRAWENTIRE;

	// draw the rest of the border
	CRect rClient, rPage;
	GetClientRect(&dis.rcItem);
	rPage = dis.rcItem;
	AdjustRect(FALSE, rPage);
	dis.rcItem.top = rPage.top - 2;

	if (CWnd * p = GetParent ()) {
		CRect rcTmp = rc - rc.TopLeft ();

		dis.itemID = -1;
		dis.rcItem = rcTmp;
		p->SendMessage (WM_DRAWITEM, nID, (LPARAM)&dis);
	//	dc.FillRect (rc - rc.TopLeft (), &CBrush (CControlView::m_rgbDlg));
	}

	//DrawMainBorder(&dis);

	int nTab = GetItemCount();
	int nSel = GetCurSel();

	if (!nTab) // no pages added
		return;

	while (nTab--)
	{
		if (nTab != nSel)
		{
			dis.itemID = nTab;
			dis.itemState = 0;

			VERIFY(GetItemRect(nTab, &dis.rcItem));

			dis.rcItem.bottom -= 2;

			if (CWnd * p = GetParent ())
				p->SendMessage (WM_DRAWITEM, nID, (LPARAM)&dis);

			DrawTab (dc, rgbBorder, dis.rcItem, nPen, nRadius, false, false);
		}

		dis.itemID = nSel;
		dis.itemState = ODS_SELECTED;

		VERIFY(GetItemRect(nSel, &dis.rcItem));

		dis.rcItem.top -= 2;
	
		if (CWnd * p = GetParent ())
			p->SendMessage (WM_DRAWITEM, nID, (LPARAM)&dis);

		DrawTab (dc, rgbBorder, dis.rcItem, nPen, nRadius, true, false);
	}


	{
		GetClientRect (rClient);
		rClient.top = dis.rcItem.bottom;
		CPen pen (PS_SOLID, nPen, rgbBorder);
	
		rClient.DeflateRect (nPen, 0, nPen, nPen);
		CPen * pPen = dc.SelectObject (&pen);
		dc.MoveTo (rClient.left, rClient.top);		dc.LineTo (rClient.left, rClient.bottom);
		dc.MoveTo (rClient.left, rClient.bottom);	dc.LineTo (rClient.right, rClient.bottom);
		dc.MoveTo (rClient.right, rClient.bottom);	dc.LineTo (rClient.right, rClient.top);
		dc.SelectObject (pPen);
	}

	{
		DIBSECTION ds;

		ZeroMemory (&ds, sizeof (ds));
		bmp.GetObject (sizeof (ds), &ds);
		dcLive.BitBlt (0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, &dc, 0, 0, SRCCOPY);
	}

	dc.SelectObject (pBitmap);
}

COLORREF CLineTab::GetLineColor () 
{ 
	const CString strKey = CControlApp::GetRegKey () + _T ("\\Settings\\Colors");
	
	return FoxjetDatabase::GetProfileInt (strKey, _T ("button border"),	FoxjetUtils::GetDefColor (_T ("button border")));
}
