// WordDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "WordDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWordDlg dialog


CWordDlg::CWordDlg(CWnd* pParent /*=NULL*/)
:	m_w (0),
	FoxjetCommon::CEliteDlg(CWordDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWordDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CWordDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWordDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWordDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CWordDlg)
	ON_COMMAND_RANGE(CHK_0, CHK_15, OnCheck)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWordDlg message handlers

BOOL CWordDlg::OnInitDialog() 
{
	CString str;

	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	str.Format (_T ("0x%04X"), m_w);
	SetDlgItemText (TXT_WORD, str);

	UINT n [] = 
	{
		CHK_15,
		CHK_14,
		CHK_13,
		CHK_12,
		CHK_11,
		CHK_10,
		CHK_9,
		CHK_8,
		CHK_7,
		CHK_6,
		CHK_5,
		CHK_4,
		CHK_3,
		CHK_2,
		CHK_1,
		CHK_0,
	};
	
	for (int i = 0; i < ARRAYSIZE (n); i++) {
		if (m_w & (1 << (15 - i))) {
			if (CButton * p = (CButton *)GetDlgItem (n [i]))
				p->SetCheck (1);
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CWordDlg::OnCheck(UINT nID) 
{
	CString str;
	WORD w = 0;
	UINT n [] = 
	{
		CHK_15,
		CHK_14,
		CHK_13,
		CHK_12,
		CHK_11,
		CHK_10,
		CHK_9,
		CHK_8,
		CHK_7,
		CHK_6,
		CHK_5,
		CHK_4,
		CHK_3,
		CHK_2,
		CHK_1,
		CHK_0,
	};
	
	for (int i = 0; i < ARRAYSIZE (n); i++) {
		if (CButton * p = (CButton *)GetDlgItem (n [i])) {
			if (p->GetCheck ())
				w |= (1 << (15 - i));
		}
	}	

	str.Format (_T ("0x%04X"), w);
	SetDlgItemText (TXT_WORD, str);
}

void CWordDlg::OnOK()
{
	CString str;

	GetDlgItemText (TXT_WORD, str);
	str.Replace (_T ("0x"), _T (""));
	m_w = (WORD)_tcstoul (str, NULL, 16);
	FoxjetCommon::CEliteDlg::OnOK ();
}
