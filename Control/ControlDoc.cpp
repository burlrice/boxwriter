// ControlDoc.cpp : implementation of the CControlDoc class
//

#include "stdafx.h"
#include "Resource.h"
#include <Afxtempl.h>

#include "Defines.h"
#include "TypeDefs.h"
#include "Control.h"
#include "Head.h"
#include "MainFrm.h"
#include "ControlDoc.h"
#include "Database.h"
#include "ProdLine.h"
#include "LineConfigDlg.h"
#include "ControlView.h"
#include "TemplExt.h"
#include "Debug.h"
#include "Registry.h"
#include "FieldDefs.h"
#include "DatabaseStartDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Head;

/////////////////////////////////////////////////////////////////////////////
// CControlDoc

IMPLEMENT_DYNCREATE(CControlDoc, CDocument)

BEGIN_MESSAGE_MAP(CControlDoc, CDocument)
	//{{AFX_MSG_MAP(CControlDoc)
	ON_COMMAND(DOC_CMD_LINES_UPDATED, OnLinesUpdated)
	ON_COMMAND(ID_VIEW_REFRESHPREVIEW, OnViewRefreshpreview)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CControlDoc construction/destruction

CControlDoc::CControlDoc()
{
	m_bLocal = false;
	m_pSelectedLine = NULL;
	m_ConnectionListSentinel.m_strName = _T ("CControlDoc::m_ConnectionListSentinel");
	m_csLines.m_strName = _T ("CControlDoc::m_csLines");
}

CControlDoc::~CControlDoc()
{
}

BOOL CControlDoc::OnNewDocument()
{
	LOCK (m_ConnectionListSentinel);
	BOOL result = TRUE;
	CDocument::OnNewDocument();
	LoadProductLines();
	result = true;
	return result;
}

/////////////////////////////////////////////////////////////////////////////
// CControlDoc serialization

void CControlDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CControlDoc diagnostics

#ifdef _DEBUG
void CControlDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CControlDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CControlDoc commands

int	CControlDoc::GetProductLines (CMapStringToPtr & v)
{
	LOCK (m_csLines);

	int nCount = 0;
	
	{
		CString sLineName;
		CProdLine *pLine;

		POSITION pos = m_ProductLines.GetStartPosition();
		while ( pos ) 
		{
			m_ProductLines.GetNextAssoc (pos, sLineName, (void *&)pLine);
			v.SetAt (sLineName, pLine);
			nCount++;
		}
	}

	return nCount;
}

int CControlDoc::GetProductLines(CStringList &List)
{
	LOCK (m_csLines);
	int nCount = 0;

	{
		CString sLineName;
		CProdLine *pLine;

		POSITION pos = m_ProductLines.GetStartPosition();
		while ( pos ) 
		{
			m_ProductLines.GetNextAssoc( pos, sLineName, (void *&)pLine );
			List.AddTail ( sLineName );
			nCount++;
		}
	}
	
	return nCount;
}

int CControlDoc::GetProductLines(CStringArray & v)
{
	LOCK (m_csLines);
	int nCount = 0;

	{
		CString sLineName;
		CProdLine *pLine;

		POSITION pos = m_ProductLines.GetStartPosition();
		while ( pos ) 
		{
			m_ProductLines.GetNextAssoc( pos, sLineName, (void *&)pLine );
			InsertAscending (v, sLineName);
			nCount++;
		}
	}
	
	return nCount;
}

bool CControlDoc::LoadProductLines(void)
{
	using namespace FoxjetDatabase;
	using namespace Foxjet3d;

	CString sLineName;
	CProdLine * pLine;
	CArray <LINESTRUCT, LINESTRUCT &> v;
	SETTINGSSTRUCT s;
	
	CString sSelectedLine = FoxjetDatabase::GetProfileString (CControlApp::GetRegKey () + _T ("\\Settings"), 
		_T ("Selected Line"), _T (""));

	bool linefound = false;	// Currently selected line was found or not.

	// Find the Control View 
	POSITION pos = GetFirstViewPosition();
	CView* pView = NULL;
	while ( pos )
	{
		pView = GetNextView(pos);
		if ( pView->IsKindOf ( RUNTIME_CLASS (CControlView) ))
			break;
		else
			pView = NULL;
	}

	ASSERT (pView != NULL);

	CProgressDlg dlgProgress (pView);

	GetPrinterLines (theApp.m_Database, GetPrinterID (theApp.m_Database), v); //GetLineRecords (theApp.m_Database, v);

	for ( int i= 0; i < v.GetSize(); i++)
	{
		LINESTRUCT LS = (LINESTRUCT ) v.GetAt(i);
		if ( !m_ProductLines.Lookup (LS.m_strName, (void *&)pLine) )
		{
			pLine = new CProdLine ( pView->m_hWnd );
			pLine->AssignLine ( (LINESTRUCT )v.GetAt(i) );
			m_ProductLines.SetAt(LS.m_strName, pLine);
		}
		if ( ( !linefound ) && ( !sSelectedLine.IsEmpty() )) {
			if ( sSelectedLine.CompareNoCase ( pLine->GetName() ) == 0)
			{
				m_pSelectedLine = pLine;
				linefound = true;
			}
		}
	}

#ifdef __WINPRINTER__
	GetSettingsRecord (theApp.m_Database, FoxjetDatabase::Globals::m_lGlobalID, FoxjetDatabase::Globals::m_lpszCoupled, s);
	theApp.SetCoupledMode (_ttoi (s.m_strData) ? true : false);
#else
	GetSettingsRecord (theApp.m_Database, FoxjetDatabase::Globals::m_lGlobalID, FoxjetDatabase::Globals::m_lpszLean, s);
	bool bSingle = _ttoi (s.m_strData) ? true : false;
	bool bDelete = !theApp.GetSingleMsgMode () && bSingle;
	
	theApp.SetSingleMsgMode (bSingle);

	if (bDelete)
		DeleteLabels ();
#endif

	if ( !linefound ) {
		pos = m_ProductLines.GetStartPosition();
		if ( pos )
			m_ProductLines.GetNextAssoc ( pos, sSelectedLine, (void *&)m_pSelectedLine);
		else
			m_pSelectedLine = NULL;
	}

	if (m_pSelectedLine)
		SetTitle ( m_pSelectedLine->GetName() );
	return true;
}

void CControlDoc::OnLinesUpdated() 
{
	LOCK (m_ConnectionListSentinel);
	{
		POSITION pos;
		pos = m_ProductLines.GetStartPosition();
		while ( pos ) 
		{
			CProdLine *pLine;
			CString sLineName;
			m_ProductLines.GetNextAssoc ( pos, sLineName, (void *&)pLine);
			
			FoxjetDatabase::LINESTRUCT LS;
			LS.m_lID = pLine->GetID();
			// Reload Line settings.  If we do not find our line ID then we have been deleted.
			// If deleted then remove it from the list.
			if ( !FoxjetDatabase::GetLineRecord (theApp.m_Database, LS.m_lID, LS ) )
			{
				CStringArray vLines;

				m_ProductLines.RemoveKey ( sLineName );
				pLine->ShutDown();
				delete pLine;

				GetProductLines (vLines);

				if (vLines.GetSize ()) {
					SetSelectedLine (vLines [0]);
					UpdateAllViews (NULL, 0, NULL);
					UpdateAllViews (NULL, UPDATE_LINE_CHANGE, NULL);
				}
			}
			else
			{	// If the line was renamed then remove old item from list and add the new one.
				if ( sLineName.CompareNoCase (LS.m_strName) != 0 )
				{
					m_ProductLines.RemoveKey ( sLineName );
					m_ProductLines.SetAt ( pLine->m_LineRec.m_strName, pLine );
					pos = m_ProductLines.GetStartPosition();
				}
				pLine->AssignLine ( LS );
			}

		}
		LoadProductLines();	// This will handle any newly added Lines.
		UpdateAllViews (NULL, UPDATE_LINE_CHANGE, NULL);
	}
	UpdateAllViews (NULL, 0);
}

bool CControlDoc::AddActiveHead(CString sProdLine, Head::CHead *pHead)
{
	bool result = false;
	CProdLine *pLine = GetProductionLine ( sProdLine );
	if ( pLine )
		result = pLine->AddActiveHead ( pHead );
	return result;
}

bool CControlDoc::GetActiveHeads(CString sProdLine, CPrintHeadList &List)
{
	bool result = false;
	CProdLine *pLine = GetProductionLine ( sProdLine );
	if ( pLine )
	{
		pLine->GetActiveHeads ( List );
		result = true;
	}
	return result;
}

bool CControlDoc::RemoveActiveHead(CString sProdLine, Head::CHead *pHead)
{
	LOCK (m_ConnectionListSentinel);
	
	bool result;
	CProdLine *pLine = GetProductionLine ( sProdLine );
	
	if ( pLine )
		result = pLine->RemoveActiveHead ( pHead );

	return result;
}

BOOL CControlDoc::CanCloseFrame(CFrameWnd* pFrame) 
{
	CString sLineName;
	CMainFrame *p = (CMainFrame *) pFrame;	

	CProdLine *pLine;
	// Clean up m_ProductLines List;
	POSITION pos;
	pos = m_ProductLines.GetStartPosition();
	while ( pos )
	{
		m_ProductLines.GetNextAssoc ( pos, sLineName, (void *&)pLine);
		if ( pLine )
		{
			pLine->ShutDown();
			delete pLine;
		}
		pLine = NULL;
	}
	m_ProductLines.RemoveAll();
	return CDocument::CanCloseFrame(pFrame);
}

CString CControlDoc::LookupLineAssoc(CString sUid)
{
	CString sLine;
	FoxjetDatabase::HEADSTRUCT HS;
	FoxjetDatabase::LINESTRUCT LS;
	FoxjetDatabase::PANELSTRUCT PS;
	LONG headID = FoxjetDatabase::GetHeadID (theApp.m_Database, sUid, FoxjetDatabase::GetPrinterID (theApp.m_Database));
	if ( headID > 0 )
	{
		if ( FoxjetDatabase::GetHeadRecord (theApp.m_Database, headID, HS) )
		{
			if ( FoxjetDatabase::GetPanelRecord (theApp.m_Database, HS.m_lPanelID, PS) )
			{
				if ( FoxjetDatabase::GetLineRecord (theApp.m_Database, PS.m_lLineID, LS) )
					sLine = LS.m_strName;
			}
		}
	}
	return sLine;
}

CProdLine * CControlDoc::GetSelectedLine()
{
//	ASSERT (m_pSelectedLine);
	return m_pSelectedLine;
}

CProdLine * CControlDoc::GetLine (int nIndex)
{
	if (CControlView * pView = GetView ()) {
		if (CLineTab * p = (CLineTab *)pView->GetDlgItem (TAB_LINE)) {
			TCITEM tc = { 0 };
			TCHAR sz [MAX_PATH] = { 0 };

			tc.mask = TCIF_TEXT;
			tc.pszText = sz;
			tc.cchTextMax = ARRAYSIZE (sz);
				
			if (p->GetItem (nIndex, &tc)) 
				return (CProdLine *)m_ProductLines [sz];
		}
	}

	return NULL;
}

CProdLine * CControlDoc::GetProductionLine(CString sLine)
{
	CProdLine *pLine = NULL;
	m_ProductLines.Lookup ( sLine, (void *&)pLine );
	return pLine;
}

void CControlDoc::SetSelectedLine(CString sLine)
{
	VERIFY (m_pSelectedLine = GetProductionLine (sLine));
}

int CControlDoc::GetLineIndex (CProdLine * pFind)
{
	int nLine = 0;

	for (POSITION pos = m_ProductLines.GetStartPosition (); pos != NULL; nLine++) {
		CProdLine * pLine = NULL;
		CString str;

		m_ProductLines.GetNextAssoc (pos, str, (void *&)pLine);

		if (pLine) 
			if (pFind->GetID () == pLine->GetID ())
				return nLine;
	}

	return 0;
}

void CControlDoc::SetSelectedLine(int nIndex)
{
	if (CControlView * pView = GetView ()) {
		if (CLineTab * p = (CLineTab *)pView->GetDlgItem (TAB_LINE)) {
			TCITEM tc = { 0 };
			TCHAR sz [MAX_PATH] = { 0 };

			tc.mask = TCIF_TEXT;
			tc.pszText = sz;
			tc.cchTextMax = ARRAYSIZE (sz);
				
			if (p->GetItem (nIndex, &tc)) {
				CString strLine = tc.pszText;

				TRACEF (strLine);
				CProdLine * pLine = (CProdLine *)m_ProductLines [strLine];
				VERIFY (m_pSelectedLine = pLine);
			}
		}
	}

	/* TODO: rem
	int nLine = 0;

	for (POSITION pos = m_ProductLines.GetStartPosition (); pos != NULL; nLine++) {
		CProdLine * pLine = NULL;
		CString str;

		m_ProductLines.GetNextAssoc (pos, str, (void *&)pLine);

		if (pLine && nLine == nIndex) {
			VERIFY (m_pSelectedLine = pLine);
			return;
		}
	}
	*/
}

void CControlDoc::DeleteLabels()
{
	CStringArray vLines;

	GetProductLines (vLines);

	for (int i = 0; i < vLines.GetSize (); i++) {
		if (CProdLine * pLine = GetProductionLine (vLines [i])) {
			CPrintHeadList list;
	
			pLine->GetActiveHeads (list);
	
			for (POSITION pos = list.GetStartPosition(); pos != NULL; ) {
				Head::CHead * pHead = NULL;
				DWORD dwKey = 0;

				list.GetNextAssoc (pos, dwKey, (void *&)pHead);

				if (pHead) {
					while (!PostThreadMessage (pHead->m_nThreadID, TM_DELETE_LABEL, 1, 0L)) 
						::Sleep (0);
					
					// re-send current label
					while (!PostThreadMessage (pHead->m_nThreadID, TM_UPDATE_LABELS, UPDATE_ALL, 0))
						::Sleep (0);	
				}
			}
		}
	}
}

CControlView * CControlDoc::GetView () const
{
	const CRuntimeClass * pFirstClass = NULL;
	const CFrameWnd * pFrame = theApp.GetActiveFrame ();
	int nFrame = 1, nCount = 0;

	if (pFrame)
		nCount = pFrame->m_nWindow;

	for (POSITION pos = GetFirstViewPosition (); pos != NULL; ) {
		if (CControlView * p = DYNAMIC_DOWNCAST (CControlView, GetNextView (pos)))
			return p;
	}

	return NULL;
}

void CControlDoc::OnViewRefreshpreview() 
{
	GetView ()->PostMessage (WM_COMMAND, MAKEWPARAM (ID_VIEW_REFRESHPREVIEW, 0));
}

CHead * CControlDoc::GetHeadByThreadID (DWORD dwThreadID)
{
	for (POSITION pos = m_ProductLines.GetStartPosition (); pos != NULL; ) {
		CProdLine * pLine = NULL;
		CString str;

		m_ProductLines.GetNextAssoc (pos, str, (void *&)pLine);

		if (pLine) {
			if (CHead * p = pLine->GetHeadByThreadID (dwThreadID))
				return p;
		}
	}

	return NULL;
}

CProdLine * CControlDoc::GetProductionLine (ULONG lLineID)
{
	
	for (POSITION pos = m_ProductLines.GetStartPosition (); pos != NULL; ) {
		CProdLine * pLine = NULL;
		CString str;

		m_ProductLines.GetNextAssoc (pos, str, (void *&)pLine);

		if (pLine && pLine->m_LineRec.m_lID == lLineID)
			return pLine;
	}

	return NULL;
}

