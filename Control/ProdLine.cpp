// ProdLine.cpp: implementation of the CProdLine class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Resource.h"
#include "Control.h"
#include "Head.h"
#include "ProdLine.h"
#include "MainFrm.h"
#include "Fj_Socket.h"
#include "Database.h"
#include "Debug.h"
#include "List.h"
#include "HeadDlg.h"
#include "Utils.h"
#include "DiagDlg.h"
#include "Comm32.h"
#include "LinePromptDlg.h"
#include "AppVer.h"
#include "LabelBatchQtyDlg.h"
#include "Edit.h"
#include "TemplExt.h"
#include "AppVer.h"
#include "Utils.h"
#include "LineConfigDlg.h"
#include "LineConfigDlg.h"
#include "UserElementDlg.h"
#include "Parse.h"
#include "CountDlg.h"
#include "Registry.h"
#include "OdbcRecordset.h"
#include "ScannerPropDlg.h"
#include "HpHead.h"
#include "DatabaseStartDlg.h"
#include "FieldDefs.h"
#include "Color.h"
#include "PreviewDlg.h"
#include "StartupDlg.h"
#include "fj_dyntext.h"
#include "FileExt.h"
#include "StartTaskDlg.h"
#include "ScanReportDlg.h"
#include "PrintReportDlg.h"

#ifdef __IPPRINTER__
	#include "DynamicTextElement.h"
	#include "DynamicBarcodeElement.h"
	#include "DynamicTableDlg.h"
	#include "RemoteHead.h"
	#include "UserElement.h"
#endif //__IPPRINTER__

#ifdef __WINPRINTER__
	#include "LabelElement.h"
	#include "DatabaseElement.h"
	#include "DateTimeElement.h"
	#include "BitmapElement.h"
	#include "NamedCountDlg.h"
	#include "DatabaseStartDlg.h"
	#include "UserElement.h"
	#include "LocalHead.h"
	#include "ExpDateElement.h"
	#include "BarcodeElement.h"
#endif //__WINPRINTER__

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#ifdef __IPPRINTER__
	static const int nMaxWaitSeconds = GlobalVars::HeadResponseTimeout / 1000;
#else
	static const int nMaxWaitSeconds = 60; //2;
#endif

using namespace FoxjetDatabase;
using namespace Foxjet3d;
using namespace GlobalFuncs;
using namespace Head;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProdLine::CProdLine(HWND hWnd )
:	m_bWaiting (false),
	m_hThread (NULL),
	m_bSerialResetCounts (false),
	m_bRequireLogin (false),
	m_lTaskID (-1),
	m_pSw0848Thread (NULL),
	m_pdlg0835 (NULL),
	m_nDriverStrobeColor (0),
	m_hExit (NULL),
	m_nLowInk (0),
	m_nPreviewPanel (0),
	m_nErrorIndex (0),
	m_bIdledOnError (false),
	m_dwLastTaskState (0),
	m_dwLastErrorState (-1),
	m_csThread (_T ("CProdLine:x"))
	// ,m_HeadListSentinel (_T ("CProdLine:") + ToString (++m_nInstances))
{
	DWORD dw = 0;
	
	FoxjetCommon::CStrobeDlg::Load (theApp.m_Database, m_vStates);

	CBitmap bmpGray, bmpRed, bmpYellow, bmpGreen;

	VERIFY (bmpGray.LoadBitmap		(IDB_GRAY));
	VERIFY (bmpRed.LoadBitmap		(IDB_RED));
	VERIFY (bmpYellow.LoadBitmap	(IDB_YELLOW));
	VERIFY (bmpGreen.LoadBitmap		(IDB_GREEN));

	VERIFY (m_bmpGray.Load			(bmpGray));
	VERIFY (m_bmpRed.Load			(bmpRed));
	VERIFY (m_bmpYellow.Load		(bmpYellow));
	VERIFY (m_bmpGreen.Load			(bmpGreen));

	m_LineRec.m_lID = 0;
	m_LineRec.m_strName = LoadString (IDS_DEFAULT);
	
	m_pSerialData = new tagSerialData;
	memset (m_pSerialData->Data, 0, COM_BUF_SIZE);
	_tcscpy ( m_pSerialData->Data, _T ("Default Serial Data"));
	m_pSerialData->m_Len = _tcslen (m_pSerialData->Data);

	if (FoxjetUtils::CStartupDlg::IsSerialDataEnabled (theApp.m_Database)) {
		const CString strFile = GetHomeDir () + _T ("\\Serial Data.txt");
		CString str = FoxjetFile::ReadDirect (strFile);
		//CString strKey = ListGlobals::defElements.m_strRegSection;
		//CString str = FoxjetDatabase::GetProfileString (strKey, _T ("Serial Data"), m_pSerialData->Data);

		memset (m_pSerialData->Data, 0, COM_BUF_SIZE);
		FoxjetDatabase::Unformat (str, m_pSerialData->Data);
		m_pSerialData->m_Len = _tcslen (m_pSerialData->Data);
		TRACEF (str);
	}

	TRACEF (a2w (m_pSerialData->Data));

	m_nScannerPort = -1;
	m_hOwnerWnd = hWnd;
	m_lAuxBoxID = 0;
//	m_bAuxBoxConnected = false;
	m_bStrobeActive = false;

//#if __CUSTOM__ == __SW0820__
	m_bScannerError = false;
//#endif

	ResetVerifierData ();

	m_dtAms = COleDateTime::GetCurrentTime ();
	m_nConsecutiveNoReads = 0;

	BeginThread ();

	{
		using namespace Sw0848Thread;
	
		if (theApp.Is0848 ())
			m_pSw0848Thread = (CSw0848Thread *)::AfxBeginThread (RUNTIME_CLASS (CSw0848Thread));
	}
}

CProdLine::~CProdLine()
{
	FoxjetElements::CBarcodeElement::ClearLocalParams (GetID ());
	FreeCache ();
}

void CProdLine::FreeCache ()
{
	LOCK (m_vCache.m_cs);
	LOCK (m_vBoxCache.m_cs);

	for (POSITION pos = m_vCache.GetStartPosition (); pos != NULL; ) {
		CCache * p = NULL;
		ULONG lTaskID = -1;

		m_vCache.GetNextAssoc (pos, lTaskID, p);
		ASSERT (p);

		delete p;
	}

	m_vCache.RemoveAll ();
	m_vBoxCache.RemoveAll ();
}

void CProdLine::AssignLine( FoxjetDatabase::LINESTRUCT Line)
{
	m_csThread.m_strName			= _T ("CProdLine::m_csThread:") + Line.m_strName;
	m_HeadListSentinel.m_strName	= _T ("CProdLine::m_HeadListSentinel:") + Line.m_strName;
	m_cs0835.m_strName				= _T ("CProdLine::m_cs0835") + Line.m_strName;

	LOCK (m_csThread);
	{
		if (m_LineRec.m_dAMS_Interval != Line.m_dAMS_Interval) {
			m_dtAms = COleDateTime::GetCurrentTime ();
			//TRACEF (_T ("Update AMS: ") + m_dtAms.Format (_T ("%c")));
		}

		m_LineRec = Line;
		m_ScanRec.m_LineID = GetID();
		VERIFY (GetPanelRecords (theApp.m_Database, m_LineRec.m_lID, m_vPanels));
		ASSERT (m_vPanels.GetSize () == 6);
		
	//#if __CUSTOM__ == __SW0820__
		{
			using namespace FoxjetDatabase;
			using namespace FoxjetUtils;

			SETTINGSSTRUCT s;

			CScannerPropDlg::Init (m_scanner);

			if (GetSettingsRecord (theApp.m_Database, m_LineRec.m_lID /* 0 */, lpszScannerKey, s)) {
				TRACEF (s.m_strData);
				CScannerPropDlg::FromString (s.m_strData, m_scanner);
				
				//m_LineRec.m_bResetScanInfo			= m_scanner.m_bReset;
				//m_LineRec.m_strNoRead					= m_scanner.m_strNoRead;
				//m_LineRec.m_nMaxConsecutiveNoReads	= m_scanner.m_nNoReads;
			}
		}
	//#endif

		if (m_LineRec.m_bAuxBoxEnabled) {
	/* TODO: rem
			if (m_AuxBox.GetIpAddress().CompareNoCase (m_LineRec.m_strAuxBoxIp) != 0) {
				m_AuxBox.Close ();
				m_lAuxBoxID = 0;
				OnAuxBoxConnectStateChange (false);
			}

			if (!m_AuxBox.IsOpen ()) {
				if (m_lAuxBoxID = m_AuxBox.Open (m_LineRec.m_strAuxBoxIp, 1200, m_hOwnerWnd))
					OnAuxBoxConnectStateChange (true);
			}
	*/
			if (m_AuxBox.GetIpAddress().CompareNoCase (m_LineRec.m_strAuxBoxIp) != 0) {
				m_AuxBox.Close ();
				m_lAuxBoxID = 0;
				OnAuxBoxConnectStateChange (false);
			}

			if (!m_AuxBox.IsOpen ()) 
				m_lAuxBoxID = m_AuxBox.Open (m_LineRec.m_strAuxBoxIp, 1200, m_hOwnerWnd);			

			OnAuxBoxConnectStateChange (true);
		}
		else {
			if (m_AuxBox.IsOpen ()) {
				OnAuxBoxConnectStateChange (false);
				m_AuxBox.Close ();
				m_lAuxBoxID = 0;
			}
		}

		{
			using namespace Foxjet3d;

			SETTINGSSTRUCT s;

			GetSettingsRecord (theApp.m_Database, m_LineRec.m_lID, FoxjetDatabase::Globals::m_lpszResetCounts, s);
			m_bSerialResetCounts = _ttoi (s.m_strData) ? true : false;

			s.m_strData = _T ("0");
			GetSettingsRecord (theApp.m_Database, m_LineRec.m_lID, FoxjetDatabase::Globals::m_lpszRequireLogin, s);
			m_bRequireLogin = _ttoi (s.m_strData) ? true : false;
		}
	}
}

CString CProdLine::GetName()
{
	return m_LineRec.m_strName;
}

ULONG CProdLine::GetID()
{
	return m_LineRec.m_lID;
}

void CProdLine::GetActiveHeads( CPrintHeadList &List ) 
{
	LOCK (m_HeadListSentinel);
	{
		POSITION pos = m_listHeads.GetStartPosition();
		ULONG lKey;
		Head::CHead *pHead;
		while ( pos )
		{
			m_listHeads.GetNextAssoc ( pos, lKey, (void *&)pHead );
			List.SetAt ( lKey, pHead );
		}
	}
}

Head::CHead * CProdLine::GetHeadByUID (const CString & strUID) 
{
	Head::CHead * pResult = NULL;

	LOCK (m_HeadListSentinel);
	{
		POSITION pos = m_listHeads.GetStartPosition();
		ULONG lKey;
		Head::CHead *pHead;
		while ( pos )
		{
			m_listHeads.GetNextAssoc ( pos, lKey, (void *&)pHead );
			CHeadInfo info = Head::CHead::GetInfo (pHead);

			if (!info.m_Config.m_HeadSettings.m_strUID.CompareNoCase (strUID)) {
				pResult = pHead;
				break;
			}
		}
	}

	return pResult;
}

Head::CHead * CProdLine::GetHeadByID (ULONG lID) 
{
	Head::CHead * pResult = NULL;
	LOCK (m_HeadListSentinel);

	{
		POSITION pos = m_listHeads.GetStartPosition();
		ULONG lKey;
		Head::CHead *pHead;
		while ( pos )
		{
			m_listHeads.GetNextAssoc ( pos, lKey, (void *&)pHead );
			CHeadInfo info = Head::CHead::GetInfo (pHead);

			if (info.m_Config.m_HeadSettings.m_lID == lID) {
				pResult = pHead;
				break;
			}
		}
	}

	return pResult;
}

Head::CHead * CProdLine::GetHeadByThreadID (DWORD dwThreadID) 
{
	Head::CHead * pResult = NULL;
	LOCK (m_HeadListSentinel);

	{
		POSITION pos = m_listHeads.GetStartPosition();
		ULONG lKey;
		Head::CHead *pHead;
		while ( pos )
		{
			m_listHeads.GetNextAssoc ( pos, lKey, (void *&)pHead );

			if (pHead->m_nThreadID == dwThreadID) 
				return pHead;
		}
	}

	return NULL;
}

bool CProdLine::AddActiveHead( Head::CHead *pHead )
{
	bool result = false;
	Head::CHead *pTemp;
	
	LOCK (m_HeadListSentinel);
	{
		if ( !m_listHeads.Lookup (pHead->m_nThreadID, (void *&)pTemp ) )
		{
			m_listHeads.SetAt ( pHead->m_nThreadID, pHead );
			result = true;
		}
	}

	return result;
}

bool CProdLine::RemoveActiveHead (Head::CHead *pHead, bool bShutdown)
{
	LOCK (m_HeadListSentinel);

	bool bResult = m_listHeads.RemoveKey (pHead->m_nThreadID) ? true : false;

	#ifdef __WINPRINTER__
	if (!bShutdown) {
		for (POSITION pos = m_listHeads.GetStartPosition(); pos != NULL; ) {
			Head::CHead * pHead = NULL;
			DWORD dw = 0;

			m_listHeads.GetNextAssoc(pos, dw, (void *&)pHead);

			if (CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pHead)) {
				CHeadInfo info = Head::CHead::GetInfo (pHead);

				if (info.IsMaster ())
					POST_THREAD_MESSAGE (info, TM_FIND_SLAVES, 0);
			}
		}
	}
	#endif //__WINPRINTER__

	return bResult;
}

void CProdLine::LoadCache (CMainFrame & frame)
{
	using namespace FoxjetDatabase;

	LOCK (m_vCache.m_cs);
	LOCK (m_vBoxCache.m_cs);

	CArray <TASKSTRUCT *, TASKSTRUCT *> vTasks;
	CArray <BOXSTRUCT, BOXSTRUCT &> vBoxes;
	LARGE_INTEGER start, counter, freq; 
	HWND hwnd = GetControlWnd ();

	for (POSITION pos = m_listHeads.GetStartPosition(); pos != NULL; ) {
		Head::CHead * pHead = NULL;
		DWORD dw = 0;
		CString str;

		m_listHeads.GetNextAssoc(pos, dw, (void *&)pHead);
		CHeadInfo info = Head::CHead::GetInfo (pHead);

		ASSERT (pHead);

		while (!PostMessage (hwnd, WM_HEAD_PROCESSING, UPDATING_MESSAGES, (LPARAM)pHead)) 
			::Sleep (0);

		str.Format (LoadString (IDS_LOAD_CACHE), info.GetFriendlyName ());
		POST_THREAD_MESSAGE (info, TM_SET_DISPLAY_STRING, new CString (str));
		
		while (!PostMessage (hwnd, WM_HEAD_PROCESSING, UPDATING_MESSAGES, (LPARAM)pHead)) 
			::Sleep (0);
	}

	::QueryPerformanceFrequency (&freq); 
	::QueryPerformanceCounter (&start); 

	FreeCache ();
	
	GetTaskRecords (theApp.m_Database, m_LineRec.m_lID, vTasks);
	GetBoxRecords (theApp.m_Database, m_LineRec.m_lID, vBoxes);

	{
		for (int i = 0; i < vTasks.GetSize (); i++) {
			CCache * p = new CCache ();
			TASKSTRUCT * pTask = vTasks [i];
			
			p->m_task = * pTask;
			/*
			TRACEF (p->m_task.m_strName);
			for (int j = 0; j < p->m_task.m_vMsgs.GetSize (); j++)
				TRACEF ("\t" + p->m_task.m_vMsgs [j].m_strName);
			*/

			m_vCache.SetAt (p->m_task.m_lID, p);
			delete pTask;
		}
	}

	{
		for (int i = 0; i < vBoxes.GetSize (); i++) {
			BOXSTRUCT & b = vBoxes [i];

			m_vBoxCache.SetAt (b.m_lID, b);
		}
	}

	for (POSITION pos = m_listHeads.GetStartPosition(); pos != NULL; ) {
		Head::CHead * pHead = NULL;
		DWORD dw = 0;
		CString str;

		m_listHeads.GetNextAssoc(pos, dw, (void *&)pHead);
		CHeadInfo info = Head::CHead::GetInfo (pHead);

		ASSERT (pHead);

		POST_THREAD_MESSAGE (info, TM_FREE_CACHE, 0);

		int i = 0, nSize = m_vCache.GetCount ();

		for (POSITION posCache = m_vCache.GetStartPosition (); posCache != NULL; ) {
			if (!frame.IsCacheThreadAlive ())
				return;

			CCache * p = NULL;
			ULONG lTaskID = -1;
			BOXSTRUCT box;

			m_vCache.GetNextAssoc (posCache, lTaskID, p);
			ASSERT (p);

			VERIFY (m_vBoxCache.Lookup (p->m_task.m_lBoxID, box));

			::QueryPerformanceCounter (&counter); 
			double dTime = ((double)counter.QuadPart - (double)start.QuadPart) / (double)freq.QuadPart; 

			str.Format (LoadString (IDS_LOAD_CACHE_N_OF_X), info.GetFriendlyName (), i++, nSize, dTime);
			POST_THREAD_MESSAGE (info, TM_SET_DISPLAY_STRING, new CString (str));

			while (!PostMessage (hwnd, WM_HEAD_PROCESSING, UPDATING_MESSAGES, (LPARAM)pHead)) 
				::Sleep (0);
			
			CMap <ULONG, ULONG, ULONG, ULONG> mapHeads;

			Head::CHead::CACHESTRUCT s (p->m_task, box, mapHeads);
			SEND_THREAD_MESSAGE (info, TM_CACHE, &s);
		}

		while (!PostMessage (GetControlWnd (), WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM)pHead))
			::Sleep (0);
	}
}

#include "fj_printhead.h"
#include "fj_system.h"
#include "IpElements.h"
#include "PrinterFont.h"

extern CArray <LPFJSYSTEM, LPFJSYSTEM> vIpSystems;

bool CProdLine::LoadTask(ULONG lTaskID, bool bRemoteStart, bool bStartup, bool bPromptForStop, CControlView * pView)
{
	int nResult = 0;

	CMap <ULONG, ULONG, ULONG, ULONG> mapHeads;
	#ifdef __WINPRINTER__
	CStringArray vFonts, vMissing;
	CStringArray * pFonts [2] = { &vFonts, &vMissing };

	FoxjetCommon::CHead::GetWinFontNames (vFonts);
	#endif //__WINPRINTER__

	COdbcDatabase::FreeRstCache ();

	if (IsSunnyvale ())
		Sunnyvale::Init ();

	if (m_LineRec.m_bResetScanInfo) {
		m_nConsecutiveNoReads = 0;
		ClearAllScannerErrors ();
	}

	for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {			
		Head::CHead * pHead = NULL;
		DWORD dw = 0;

		m_listHeads.GetNextAssoc(pos, dw, (void *&)pHead);

		ASSERT (pHead);
		CHeadInfo info = Head::CHead::GetInfo (pHead);

		if (LPFJPRINTHEAD pHead = FoxjetIpElements::CHead::GetHead (info.m_Config.m_HeadSettings)) {
			using namespace FoxjetCommon;

			CArray <CPrinterFont, CPrinterFont &> v;

			FoxjetIpElements::CHead::GetFonts (pHead, v);

			for (int i = 0; i < v.GetSize (); i++) {
				CString str = v [i].m_strName;

				if (Find (vFonts, str, false) == -1)
					vFonts.Add (str);
			}
		}

		if ( !bStartup ) {
			/*
			#ifdef __IPPRINTER__
				if (!bTestPattern)
					StopTask (bPromptForStop, pView);
			#else
					StopTask (bPromptForStop, pView);
			#endif 
			*/
			
			if (!StopTask (bPromptForStop, pView))
				return false;
		}

		Foxjet3d::UserElementDlg::CUserElementArray vUser;
		FoxjetDatabase::TASKSTRUCT task;
		FoxjetDatabase::BOXSTRUCT box;

		CCache * p = NULL;
		bool bFound = false;
		
		m_vCache.m_cs.m_strName = _T ("m_vCache.m_cs:") + m_LineRec.m_strName;
		m_vBoxCache.m_cs.m_strName = _T ("m_vBoxCache.m_cs:") + m_LineRec.m_strName;

		{
			LOCK (m_vCache.m_cs);
			bFound = m_vCache.Lookup (lTaskID, p) ? true : false;
		}

		if (!bFound) {
			if (!GetTaskRecord (theApp.m_Database, lTaskID, task)) {
				TRACEF ("GetTaskRecord failed");
				return false;
			}
		}
		else {
			ASSERT (p);

			task = p->m_task;
		}

		bFound = false;
		{
			LOCK (m_vBoxCache.m_cs);
			bFound = m_vBoxCache.Lookup (task.m_lBoxID, box) ? true : false;
		}

		if (task.m_lLineID != m_LineRec.m_lID) {
			CArray <HEADSTRUCT, HEADSTRUCT &> v;

			GetLineHeads (theApp.m_Database, m_LineRec.m_lID, v);

			for (int i = 0; i < min (task.m_vMsgs.GetSize (), v.GetSize ()); i++) {
				mapHeads.SetAt (v [i].m_lID, task.m_vMsgs [i].m_lHeadID);
				TRACEF (ToString (task.m_vMsgs [i].m_lHeadID) + _T (" --> ") + ToString (v [i].m_lID));
				task.m_vMsgs [i].m_lHeadID = v [i].m_lID;
			}
		}

		{
			TASKSTRUCT tmp = task;

			Remap (theApp.m_Database, tmp, GetPrinterID (theApp.m_Database), GetMasterPrinterID (theApp.m_Database));
			//m_vPanels.RemoveAll ();
			//VERIFY (GetPanelRecords (theApp.m_Database, tmp.m_lLineID, m_vPanels));
			//ASSERT (m_vPanels.GetSize () == 6);
		}


		if (!bFound) {
			if (!GetBoxRecord (theApp.m_Database, task.m_lBoxID, box)) {
				TRACEF ("GetBoxRecord failed");
				return false;
			}
			else {
				// this prevents TM_LOAD from hanging up in wrong thread context
				// can happen if using database elements
				for (int nMessage = 0; nMessage < task.m_vMsgs.GetSize (); nMessage++) {
					FoxjetCommon::CElementList list;
					FoxjetDatabase::MESSAGESTRUCT msg = task.m_vMsgs [nMessage];
					int nElements = list.FromString (msg.m_strData, GlobalVars::CurrentVersion);
					
					if (list.IsNEXT ()) {
						CSize pa = list.GetProductArea ();
						box.m_lLength = pa.cx;
						box.m_lHeight = pa.cy;
					}
				}
			}
		}

		bool bLoad = false;
		Head::CHead::CACHESTRUCT s (task, box, mapHeads);
		SEND_THREAD_MESSAGE (info, TM_LOAD, &s);
		nResult += s.m_bResult ? 1 : 0;

		{
			using namespace FoxjetCommon;

			#ifdef __WINPRINTER__
			CHeadInfo info = Head::CHead::GetInfo (pHead);

			SEND_THREAD_MESSAGE (info, TM_GET_MISSING_FONTS, &pFonts);
			#endif //__WINPRINTER__
		}
	}

	{
		using namespace FoxjetElements;
		std::map <std::wstring, int> mapSQLType;

		for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {			
			Head::CHead * pHead = NULL;
			DWORD dw = 0;
			Head::CElementPtrArray list;
			CHead::GETELEMENTSTRUCT elements (&list, DATABASE);

			m_listHeads.GetNextAssoc(pos, dw, (void *&)pHead);

			ASSERT (pHead);
			CHeadInfo info = Head::CHead::GetInfo (pHead);

			SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &elements);

			for (int i = 0; i < list.GetSize (); i++) {
				if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, list [i])) {
					if (!p->GetSQL ().GetLength ()) 
						mapSQLType [(LPCTSTR)p->GetKeyField ()] = p->GetSQLType ();
				}
			}
		}

		for (POSITION pos = m_listHeads.GetStartPosition(); pos != NULL; ) {
			Head::CHead * pHead = NULL;
			DWORD dw = 0;
			Head::CElementPtrArray list;
			CHead::GETELEMENTSTRUCT elements (&list, DATABASE);

			m_listHeads.GetNextAssoc(pos, dw, (void *&)pHead);
			ASSERT (pHead);
			CHeadInfo info = Head::CHead::GetInfo (pHead);
		
			SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &elements);

			for (int i = 0; i < list.GetSize (); i++) {
				if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, list [i])) {
					if (p->GetSQL ().GetLength ()) 
						p->SetSQLType (mapSQLType [(LPCTSTR)p->GetKeyField ()]);
				}
			}
		}
	}

	#ifdef __WINPRINTER__
	if (vMissing.GetSize ()) {
		CString str = LoadString (IDS_MISSINGFONTS);

		for (int i = 0; i < vMissing.GetSize (); i++)
			str += _T ("\r\n") + vMissing [i];

		TRACEF (str);
		MsgBox (str);
	}
	#endif

	if (nResult)
		m_lTaskID = lTaskID;

	return nResult > 0 ? true : false;
}

void CProdLine::DeleteDebugImages ()
{
	const CString strDir = FoxjetDatabase::GetHomeDir () + _T ("\\Debug");

	for (int i = 1; i <= 6; i++) {
		CString str = strDir + _T ("\\") + ToString (i) + _T (".bmp");
		
		::DeleteFile (str);
	}
}

FoxjetDatabase::REPORTSTRUCT CProdLine::GenerateReportRecord() const
{
	FoxjetDatabase::REPORTSTRUCT result;

	result.m_dtTime = COleDateTime::GetCurrentTime();
	result.m_strUsername = theApp.m_strUsername;
	result.m_strLine = m_LineRec.m_strName;
	result.m_strTaskName = m_strTaskname;
	result.m_strCounts.Format(_T("%lu"), GlobalFuncs::GetProfileInt(m_LineRec.m_strName, GlobalVars::strCount, 0));

	return result;
}

int CProdLine::StartTask (bool bRemoteStart, bool bStartup, bool bPromptForStop, CControlView * pView, const CString & strKeyValue)
{
	DECLARETRACECOUNT (200);
	MARKTRACECOUNT ();

	CArray <Head::CHead *, Head::CHead *> vHeads;
	int nEventCnt = 0;
	CString strDownload;
	CString strTask;
	bool bTimeOut = false;

	//TRACEF (GetCurrentTask ());

	CalcProductLen ();
	DeleteDebugImages ();

// 1.20 std // #if __CUSTOM__ == __SW0831__
	#ifdef __WINPRINTER__

	FoxjetElements::CDateTimeElement::ResetHold (m_LineRec.m_lID);
	
	#endif //__WINPRINTER__
// 1.20 std // #endif //__CUSTOM__ == __SW0831__


	if (m_pSw0848Thread) { // sw0848
		bool bResult = false;
		HANDLE h = ::CreateEvent (NULL, TRUE, FALSE, _T ("TM_SW0848INIT"));

		while (!::PostThreadMessage (m_pSw0848Thread->m_nThreadID, TM_SW0848INIT, (WPARAM)h, (LPARAM)&bResult))
			::Sleep (0);

		bool bSignalled = ::WaitForSingleObject (h, 10000) == WAIT_OBJECT_0;
		::CloseHandle (h);

		if (!bSignalled || !bResult) 
			MsgBox (LoadString (IDS_FAILEDTOOPENOUTPUTTABLE));
	}

	// m_lTaskID == -1 for test pattern

	for (POSITION pos = m_listHeads.GetStartPosition(); pos != NULL; ) {
		Head::CHead * pHead = NULL;
		DWORD dw = 0;

		m_listHeads.GetNextAssoc(pos, dw, (void *&)pHead);
		CHeadInfo info = pHead->GetInfo (0);

		if (!info.m_bDisabled)
			vHeads.Add (pHead);
		else 
			POST_THREAD_MESSAGE (info, TM_IMAGE_FREE, 0);
	}

	while (vHeads.GetSize ()) {
		Head::CHead * pHead = vHeads [0];
		CHeadInfo info = Head::CHead::GetInfo (pHead);
		CString sEvent;

		sEvent.Format (_T ("ImageReady %lu"), pHead->m_hThread);
		HANDLE hEvent = CreateEvent (NULL, TRUE, FALSE, sEvent);
		
		while (!::PostThreadMessage (pHead->m_nThreadID, TM_SET_KEY_VALUE, (WPARAM)new CString (strKeyValue), 0)) 
			::Sleep (0);

		while (!::PostThreadMessage (pHead->m_nThreadID, TM_START_TASK, (WPARAM)hEvent, m_lTaskID)) 
			::Sleep (0);
		
		DWORD dwWait = ::WaitForSingleObject (hEvent, ::nMaxWaitSeconds * 1000);

		::CloseHandle (hEvent);

		//if (dwWait == WAIT_OBJECT_0)	TRACEF (info.GetFriendlyName () + ": WAIT_OBJECT_0");
		if (dwWait == WAIT_TIMEOUT)		TRACEF (info.GetFriendlyName () + ": WAIT_TIMEOUT");
		if (dwWait == WAIT_ABANDONED)	TRACEF (info.GetFriendlyName () + ": WAIT_ABANDONED");
		if (dwWait == WAIT_FAILED)		TRACEF (info.GetFriendlyName () + ": WAIT_FAILED");

		if (dwWait == WAIT_OBJECT_0) 
			vHeads.RemoveAt (0);
		else {
			bTimeOut = true;
			break;
		}
		
		if (info.IsMaster ()) {
			strTask = info.m_Task.m_strName;

			if (strTask.CompareNoCase (GlobalVars::strTestPattern) != 0) {
				if (theApp.IsPrintReportLoggingEnabled ()) {
					auto report = GenerateReportRecord();

					report.m_strTaskName = info.m_Task.m_strName;
					report.m_strCounts.Format(_T("%lu"), GlobalFuncs::GetProfileInt(m_LineRec.m_strName, GlobalVars::strCount, 0));

					if (bStartup) {
						#ifdef __WINPRINTER__
						CString strWDT = CLocalHead::GetWDTFile ();

						if (FILE * fp = _tfopen (strWDT, _T ("r"))) {
							char sz [128] = { 0 };

							fread (sz, ARRAYSIZE (sz) - 1, 1, fp);
							fclose (fp);

							try {
								COleDateTime tmWDT;
								CString str = a2w (sz);

								if (tmWDT.ParseDateTime (str)) {
									COleDateTimeSpan tmSpan = report.m_dtTime - tmWDT;

									report.m_strUsername.Format (_T ("%s, WDT: %s [%s]"),
										theApp.m_strUsername,
										tmWDT.Format (_T ("%c")),
										tmSpan.Format (_T ("%H:%M:%S")));
								}
								else {
									report.m_strUsername.Format (_T ("%s, WDT: %s"), theApp.m_strUsername, str);
								}

								TRACEF (report.m_dtTime.Format (_T ("%c")));
								TRACEF (report.m_strUsername);
							}
							catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
							catch (COleException * e)		{ HANDLEEXCEPTION (e); }
						}
						#endif //__WINPRINTER__

						if (theApp.IsPrintReportLoggingEnabled ()) 
							AddPrinterReportRecord (FoxjetDatabase::REPORT_TASK_START_POWERUP, report);
					}
				}
			}
		}
	}

	{ // build download string
		for (POSITION pos = m_listHeads.GetStartPosition(); pos != NULL; ) {
			Head::CHead * pHead = NULL;
			DWORD dw = 0;

			m_listHeads.GetNextAssoc(pos, dw, (void *&)pHead);
			ASSERT (pHead);
			CHeadInfo info = Head::CHead::GetInfo (pHead);

			if (!info.m_bDisabled) {
				CString str;

				SEND_THREAD_MESSAGE (info, TM_GET_SERIALDATA, &str);
				strDownload	+= str;
			}
		}
	}

	if ( !bTimeOut )
	{
		#ifndef __IPPRINTER__
			bool bEnable = true;

			if (IsOneToOnePrintEnabled ()) { // __SW0808__
				SetSerialPending (false);
				bEnable = false;
			}

			if (FoxjetCommon::IsProductionConfig ())
				bEnable = true;

			POSITION pos = m_listHeads.GetStartPosition();

			while ( pos )
			{
				CHead * pHead = NULL;
				DWORD dw = 0;

				m_listHeads.GetNextAssoc (pos, dw, (void *&)pHead);

				if (!IsWaiting () || FoxjetCommon::IsProductionConfig ())
					while (!::PostThreadMessage (pHead->m_nThreadID, TM_ENABLE_PHOTO, bEnable, 0L))
						::Sleep (0);
			}
		#endif

		m_strTaskname = strTask;

		if ( m_LineRec.m_bResetScanInfo )
			ResetVerifierData ();

		if (strDownload.GetLength ()) {
			if (!DownloadSerialData (strDownload, pView)) {
			}
		}

		if (strTask.CompareNoCase (GlobalVars::strTestPattern) != 0) {
			GlobalFuncs::WriteProfileString (m_LineRec.m_strName, GlobalVars::strTask, strTask);
			GlobalFuncs::WriteProfileString (m_LineRec.m_strName, GlobalVars::strState, GlobalFuncs::GetState (RUNNING));
		}

		EnableDynamicDataDownload (true);

		#ifdef __WINPRINTER__
		if (!theApp.GetImagingModeExpire ()) 
			UpdateImages ();
		#endif

		MARKTRACECOUNT ();
		//TRACECOUNTARRAY ();

		theApp.PostViewRefresh (0);

		m_bIdledOnError = false;
		m_dwLastErrorState = 0;
		UpdateStrobeState (pView);
		m_dwLastTaskState = RUNNING;

		return TASK_START_SUCCESS;
	}
	else
	{
		CString sMsg = LoadString (IDS_FAILEDTOSTART);

		for (int idx = 0; idx < vHeads.GetSize(); idx++) {
			if (CHead * pHead = (CHead *) vHeads.GetAt (idx)) {
				CHeadInfo info = CHead::GetInfo (pHead);

				sMsg += info.GetFriendlyName () + _T("\n");
			}
		}

		MsgBox ( m_hOwnerWnd, sMsg, LoadString (IDS_STARTTASKERROR), MB_OK );
		return TASK_START_FAILED;
	}

	return TASK_START_FAILED;
}

int CProdLine::StartTask(CString sTask, bool bRemoteStart, bool bStartup, bool bPromptForStop, CControlView * pView,
						 __int64 lPalletMax, __int64 lUnitsPerPallet, bool bCountDlg, 
						 const CString & strKeyValue, CMapStringToString * pvUser, int nPeriod, const CString & strLineAlt, CHead::CHANGECOUNTSTRUCT * pCount)
{
	DECLARETRACECOUNT (200);
	MARKTRACECOUNT ();

#ifdef __WINPRINTER__
	using namespace FoxjetElements;
#endif //__WINPRINTER__

	bool MasterFound = false;
	int ActiveHeads =0;
	LONG lTaskID = -1;
	const bool bTestPattern = !sTask.CompareNoCase (GlobalVars::strTestPattern);
	ULONG lLabels = 0;
	ULONG lPrinterID = GetMasterPrinterID (theApp.m_Database); //GetPrinterID (theApp.m_Database);
	int nOriginalDbStart;
	CString strOriginalKeyValue;

	FoxjetElements::CBarcodeElement::ClearLocalParams (GetID ());

	::RegDeleteKey (HKEY_CURRENT_USER, ListGlobals::defElements.m_strRegSection + _T ("\\CHeadLimitedConfigDlg\\") + ToString (m_LineRec.m_lID));

	POSITION pos = m_listHeads.GetStartPosition();
	if ( pos ) 
	{	
		if (!bTestPattern) {
			Foxjet3d::UserElementDlg::CUserElementArray vUser;
			CString strLine = strLineAlt.GetLength () ? strLineAlt : m_LineRec.m_strName;

			if ((lTaskID = FoxjetDatabase::GetTaskID (theApp.m_Database, sTask, strLine, lPrinterID)) == 0)
				return TASK_START_NOT_FOUND;

			if (!LoadTask (lTaskID, bRemoteStart, bStartup, bPromptForStop, pView))
				return TASK_START_NO_ACTIVE_HEADS;

			{
				bool bSetCounts = false;
				CElementPtrArray vCount;

				GetElements (vCount, COUNT);

				{
					bool bIrregular = false;
				
					for (int i = 0; i < vCount.GetSize (); i++) {
						if (CCountElement * p = DYNAMIC_DOWNCAST (CCountElement, vCount [i])) {
							if (p->IsIrregularPalletSize ()) {
								bIrregular = true;
							}
						}
					}

					if (!bIrregular && pCount)
						pCount->m_lCount = -1;
				}

				if (theApp.Is0844 () && vCount.GetSize ())
					bSetCounts = true;
				else if (!bStartup && !bRemoteStart) {
					for (int i = 0; i < vCount.GetSize (); i++) {
						if (CCountElement * p = DYNAMIC_DOWNCAST (CCountElement, vCount [i])) {
							if (p->IsIrregularPalletSize ()) {
								bSetCounts = true;

								if (pCount)
									pCount->m_bIrregularPalletSize = true;

								break;
							}
						}
					}
				}
				
				if (bSetCounts)
					if (!ChangeCounts (true, lPalletMax, lUnitsPerPallet, bCountDlg, pCount))
						return TASK_START_NO_ACTIVE_HEADS;
			}

			GetUserElements (vUser, true);

			if (bStartup) {
				for (int i = 0; i < vUser.GetSize (); i++) {
					CString strPrompt = vUser [i].m_strPrompt;
					//CString strData = GlobalFuncs::GetProfileString (m_LineRec.m_strName, GlobalVars::strUserPrompted + CString (_T ("\\")) + strPrompt, vUser [i].m_strData);
					const CString strFile = GetHomeDir () + _T ("\\") + GlobalVars::strUserPrompted + _T ("\\") + m_LineRec.m_strName + _T ("\\") + strPrompt + _T (".txt");
					const CString strData = FoxjetFile::ReadDirect (strFile, vUser [i].m_strData);

					for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {
						CHead * pHead = NULL;
						DWORD dwKey;
						CElementPtrArray list;

						m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
						ASSERT (pHead);
						CHeadInfo info = CHead::GetInfo (pHead);

						#ifdef __WINPRINTER__
						CHead::GETELEMENTSTRUCT s (&list, USER);

						s.m_nClassID = USER;
						SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

						s.m_nClassID = DATABASE;
						SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

						s.m_nClassID = BMP;
						SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

						s.m_nClassID = EXPDATE;
						SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s); // sw0924
						#endif //__WINPRINTER__

						SetUserPrompted (list, strPrompt, strData, pHead);

						if (!bStartup) {
							const CString strFile = GetHomeDir () + _T ("\\") + GlobalVars::strUserPrompted + _T ("\\") + GetName () + _T ("\\") + strPrompt + _T (".txt");
							FoxjetFile::WriteDirect (strFile, strData);
						}
					}
				}

				if (IsGojo ())
					SetGojoElements (nPeriod);
			}
			else {
				PreviewDlg::CPreviewDlg dlg (theApp.m_Database, this, pView->m_pPreview);

				nOriginalDbStart	= theApp.GetProfileInt (_T ("sw0858"), _T ("db start"), 0);
				strOriginalKeyValue	= theApp.GetProfileString (_T ("sw0858"), _T ("Key value"), _T (""));

				bool bDbStart = strKeyValue.GetLength () ? true : false;

				theApp.WriteProfileInt (_T ("sw0858"), _T ("db start"), bDbStart ? 1 : 0);

				if (bDbStart) {
					theApp.WriteProfileString (_T ("sw0858"), _T ("Key value"), strKeyValue);

					if (IsGojo ())
						theApp.WriteProfileInt (_T ("sw0858"), _T ("nPeriod"), nPeriod);
				}
				else {
					theApp.WriteProfileString (_T ("sw0858"), _T ("Key value"), _T (""));
				}

				if (vUser.GetSize () && !ChangeUserElementData (true, strKeyValue, pvUser, nPeriod))
					return TASK_START_CANCELLED;

				//if (!bStartup) 
				{ 
					VERIFY (GetTaskRecord (theApp.m_Database, lTaskID, dlg.m_task));
					Remap (theApp.m_Database, dlg.m_task, GetPrinterID (theApp.m_Database), GetMasterPrinterID (theApp.m_Database));

					for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {
						CHead * pHead = NULL;
						DWORD dwKey;
						CElementPtrArray list;

						m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
						ASSERT (pHead);
						CHeadInfo info = CHead::GetInfo (pHead);

						for (int i = 0; i < dlg.m_task.m_vMsgs.GetSize (); i++) {
							MESSAGESTRUCT & msg = dlg.m_task.m_vMsgs [i];

							if (msg.m_lHeadID == info.m_Config.m_HeadSettings.m_lID) {
								CHead::GETELEMENTSTRUCT s (&list, -1);

								SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);
								msg.m_strData.Empty ();

								for (int nElement = 0; nElement < s.m_pv->GetSize (); nElement++) 
									msg.m_strData += s.m_pv->GetAt (nElement)->ToString (FoxjetCommon::verApp);

								TRACEF (msg.m_strData);
								dlg.m_task.m_vMsgs [i] = msg;
							}
						}
					}

					if (IsGojo () && dlg.DoModal () == IDCANCEL)
						return TASK_START_CANCELLED;
				}
			}

#if defined( __NICELABEL__ ) && defined ( __WINPRINTER__ )
			if (FoxjetElements::CLabelElement::IsLabelEditorPresent ()) {
				GetElements (vRFID, LABEL);

				if (vRFID.GetSize ()) {

					using namespace FoxjetElements;

					if (CLabelElement * p = DYNAMIC_DOWNCAST (CLabelElement, vRFID [0])) 
						lLabels = p->GetBatchQuantity ();

					if (!GetLabelBatchQty (lLabels))
						return TASK_START_CANCELLED;

					if (CLabelElement * p = DYNAMIC_DOWNCAST (CLabelElement, vRFID [0])) {
						CString strFailed, strFile = p->GetFilePath ();
						LONG lLabelID = -1;

						TRACEF (strFile);

						try
						{
							if ((lLabelID = m_appNiceLabel.LabelOpen (strFile)) != -1) {
								CString strLabels;
								
								strLabels.Format (_T ("%lu"), lLabels);

								if (!m_appNiceLabel.LabelSessionStart (lLabelID))
									strFailed += "LabelSessionStart failed\n";
								
								if (!m_appNiceLabel.LabelSessionPrint (lLabelID, strLabels))
									strFailed += "LabelSessionPrint failed\n";

								if (!m_appNiceLabel.LabelSessionEnd (lLabelID))
									strFailed += "LabelSessionEnd failed\n";

								if (!m_appNiceLabel.LabelClose (lLabelID))
									strFailed += "LabelClose failed\n";
							}
							else
								strFailed += "LabelOpen failed: " + strFile + "\n";

							if (strFailed.GetLength ()) {
								MsgBox (strFailed);
							
								return TASK_START_FAILED;
							}
						}
						catch (COleException * e)			{ HANDLEEXCEPTION (e); }
						catch (COleDispatchException  * e)	{ HANDLEEXCEPTION (e); }
					}
				}
			}
#endif // __NICELABEL__ && __WINPRINTER__

		}

		int nResult = StartTask (bRemoteStart, bStartup, bPromptForStop, pView, strKeyValue);

		if (TASK_START_SUCCEEDED (nResult)) {
			/* TODO: rem
			if (strKeyValue.GetLength ()) {
				REPORTSTRUCT s;

				s.m_strUsername = theApp.m_strUsername;
				s.m_strTaskName.Format (_T ("%s: %s"), sTask, strKeyValue);
				s.m_strLine		= GetName ();
				s.m_dtTime		= COleDateTime::GetCurrentTime ();
				s.m_strCounts.Format (_T ("%lu"), GlobalFuncs::GetProfileInt (m_LineRec.m_strName, GlobalVars::strCount, 0));

				if (theApp.IsPrintReportLoggingEnabled ()) 
					AddPrinterReportRecord (FoxjetDatabase::REPORT_KEYFIELDSTART, s);
			}
			*/
		}
		else {
			theApp.WriteProfileInt (_T ("sw0858"), _T ("db start"), nOriginalDbStart);
			theApp.WriteProfileString (_T ("sw0858"), _T ("Key value"), strOriginalKeyValue);
		}

		MARKTRACECOUNT ();
		//TRACECOUNTARRAY ();

		return nResult;
	}

	return TASK_START_NO_ACTIVE_HEADS;
}

bool CProdLine::StopTask(bool bPrompt, CControlView * pView)
{
	EnableDynamicDataDownload (false);

	POSITION pos = m_listHeads.GetStartPosition();
	ULONG Key;
	HANDLE hEventList [255] = { NULL };
	INT nEventCnt = 0;
	CPtrArray waitingHeadList;
	REPORTSTRUCT report = GenerateReportRecord();
	const TASKSTATE state = GetTaskState();

	if (m_strTaskname.IsEmpty ())
		m_strTaskname = GetCurrentTask ();

	bool bTestPattern = m_strTaskname.CompareNoCase (GlobalVars::strTestPattern) == 0;

	if ( !m_strTaskname.IsEmpty() ) {
		if (bPrompt) {
			ASSERT (pView);

			if (MsgBox (* pView, LoadString (IDS_STOPTASK), LoadString (IDS_STOP), MB_YESNO) == IDNO)
				return false;
		}

		COdbcDatabase::FreeDsnCache ();
		DeleteDebugImages ();

		while ( pos ) 
		{
			CHead * pHead = NULL;
			m_listHeads.GetNextAssoc(pos, Key, (void *&)pHead);

			ASSERT (pHead);
			CHeadInfo info = CHead::GetInfo (pHead);

			if (!info.m_bDisabled && info.m_eTaskState != STOPPED) {
				CString sEvent;
				sEvent.Format (_T ("StopTask %lu"), info.GetThreadID ());
				HANDLE h = hEventList [nEventCnt++] = CreateEvent (NULL, TRUE, FALSE, sEvent);
					
				waitingHeadList.Add (pHead);

				#ifdef _DEBUG
				//{ CString str; str.Format ("[0x%p] %s", h, sEvent); TRACEF (str); }
				#endif

				if (!bTestPattern) {
					if (info.IsMaster()) 
						report.m_strCounts.Format(_T("%lu"), info.m_Status.m_lCount);
				}

				while (!::PostThreadMessage (info.GetThreadID (), TM_STOP_TASK, 0, (LPARAM)h))
					::Sleep (0);

				if (!bTestPattern) {
					if (bPrompt) 
						GlobalFuncs::WriteProfileString (m_LineRec.m_strName, GlobalVars::strState, GlobalFuncs::GetState (STOPPED));
				}

				m_strTaskname = _T("");
				m_lTaskID = -1;
			}
		}
		
		MSG Msg;
		CTime StartTime = CTime::GetCurrentTime();
		bool done = nEventCnt == 0;
		bool error = false;
		while ( !done )
		{
			DWORD dwValue;
			dwValue =  WaitForMultipleObjects ( nEventCnt, hEventList, true, 20 );
			if ( (int) ( dwValue -WAIT_OBJECT_0) < nEventCnt )
			{
				int event = dwValue - WAIT_OBJECT_0;
				waitingHeadList.RemoveAt ( event );
				CloseHandle ( hEventList[ event ] );
				int n;

				#ifdef _DEBUG
				//{ CString str; str.Format ("[0x%p] CloseHandle", hEventList [event]); TRACEF (str); }
				#endif

				for (n = event; n < nEventCnt; n++)
					hEventList[n] = hEventList[n+1];

				hEventList[n] = NULL;
				nEventCnt--;
				if ( nEventCnt == 0 )
					done = true;
			}
			else
			{
				CTime CurrentTime = CTime::GetCurrentTime();
				CTimeSpan tm = CurrentTime - StartTime;

				if ((tm.GetTotalSeconds() > nEventCnt * ::nMaxWaitSeconds) || tm.GetTotalSeconds () < 0)
				{
					done = true;
					error = true;
				}
			}
			if ( PeekMessage ( &Msg, NULL, NULL, NULL, PM_NOREMOVE )) {
				GetMessage(&Msg, (HWND)NULL, 0, 0);
				TranslateMessage(&Msg); 
				DispatchMessage(&Msg);
			}
		}		

		for (int i = 0; i < ARRAYSIZE (hEventList); i++) {
			if (hEventList [i]) {
				#ifdef _DEBUG
				{ CString str; str.Format (_T ("[0x%p] CloseHandle"), hEventList [i]); TRACEF (str); }
				#endif
				::CloseHandle (hEventList [i]);
			}
		}

		if (error) {
			CString str = LoadString (IDS_FAILEDTOSTOP);
			int nPrintCycle = 0;

			for (int i = 0; i < waitingHeadList.GetSize(); i++) {
				if (CHead * pHead = (CHead *)waitingHeadList [i]) {
					CHeadInfo info = CHead::GetInfo (pHead);

					str += info.GetFriendlyName() + _T ("\n");

					#ifdef __WINPRINTER__
					if (CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pHead)) {
						bool bPrinting = false;

						SEND_THREAD_MESSAGE (info, TM_IS_PRINTING, &bPrinting);
						nPrintCycle += bPrinting ? 1 : 0;
					}
					#endif //__WINPRINTER__
				}
			}
			
			if (!nPrintCycle) {
				MsgBox (m_hOwnerWnd, str, LoadString (IDS_STOPTASKERROR), IDOK);
				return false;
			}
			else {
				str = LoadString (IDS_HEADSINPRINTCYCLE);

				for (int i = 0; i < waitingHeadList.GetSize(); i++) {
					if (CHead * pHead = (CHead *)waitingHeadList [i]) {
						CHeadInfo info = CHead::GetInfo (pHead);

						str += info.GetFriendlyName();

						#ifdef __WINPRINTER__
						if (CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pHead)) {
							bool bPrinting = false;

							SEND_THREAD_MESSAGE (info, TM_IS_PRINTING, &bPrinting);
								
							if (bPrinting)
								str += _T ("[") + LoadString (IDS_UNKNOWNSTATE) + _T ("]");
						}
						#endif //__WINPRINTER__

						str += _T ("\n");
					}
				}

				str += _T ("\n") + LoadString (IDS_ABORTPRINT);

				int nResult = MsgBox (m_hOwnerWnd, str, LoadString (IDS_STOPTASKERROR), MB_YESNO);

				if (nResult == IDNO)
					return false;
				else {
					for (int i = 0; i < waitingHeadList.GetSize(); i++) {
						#ifdef __WINPRINTER__
						if (CLocalHead * pHead = DYNAMIC_DOWNCAST (CLocalHead, (CHead *)waitingHeadList [i])) {
							while ( !PostThreadMessage (pHead->m_nThreadID, TM_ABORT, 0, 0)) 
								::Sleep (0);
						}
						#endif //__WINPRINTER__
					}
				}
			}
		}
	}
			
	if (state == RUNNING || state == IDLE) {
		const TASKSTATE newState = GetTaskState();

		if (newState == STOPPED) {
			if (theApp.IsPrintReportLoggingEnabled()) 
				AddPrinterReportRecord(FoxjetDatabase::REPORT_TASK_STOP, report);
		}
	}

	return true;
}

void CProdLine::IdleTask(bool bLogReport, bool bHeadError)
{
	using namespace FoxjetDatabase;

	EnableDynamicDataDownload (false);

	POSITION pos = m_listHeads.GetStartPosition();
	
	while ( pos ) 
	{
		DWORD Key;
		CHead * pHead = NULL;

		m_listHeads.GetNextAssoc(pos, Key, (void *&)pHead);
		ASSERT (pHead);

		while ( !PostThreadMessage (pHead->m_nThreadID, TM_IDLE_TASK, 0, 0L)) 
			::Sleep (0);
	}

	if (bLogReport) {
		if (m_strTaskname.CompareNoCase (GlobalVars::strTestPattern) != 0) {
			auto report = GenerateReportRecord();

			GlobalFuncs::WriteProfileString (m_LineRec.m_strName, GlobalVars::strState, GlobalFuncs::GetState (IDLE));
			report.m_strCounts.Format (_T ("%lu"), GlobalFuncs::GetProfileInt (m_LineRec.m_strName, GlobalVars::strCount, 0));

			if (theApp.IsPrintReportLoggingEnabled ()) {
				if (!bHeadError)
					AddPrinterReportRecord (REPORT_TASK_IDLE, report);
				else
					AddPrinterReportRecord (REPORT_TASK_IDLE_ONERROR, report);
			}
		}
	}
}

void CProdLine::ResumeTask(bool bLogReport)
{
	if (m_bIdledOnError)
		return;

	POSITION pos = m_listHeads.GetStartPosition();
	bool bEnablePhoto = !IsWaiting ();
	
	if (IsOneToOnePrintEnabled ()) // __SW0808__
		bEnablePhoto = GetSerialPending () && !IsWaiting ();

	ClearAllScannerErrors ();

	while ( pos ) 	{
		DWORD Key;
		CHead *pHead = NULL;

		m_listHeads.GetNextAssoc(pos, Key, (void *&)pHead);
		ASSERT (pHead);

//		while ( !PostThreadMessage (pHead->m_nThreadID, TM_RESUME_TASK, !IsWaiting (), 0L)) Sleep (0);
		while ( !PostThreadMessage (pHead->m_nThreadID, TM_RESUME_TASK, bEnablePhoto, 0L)) Sleep (0);
	}

	if (bLogReport) {
		if (m_strTaskname.CompareNoCase (GlobalVars::strTestPattern) != 0) {
			auto report = GenerateReportRecord();
			
			report.m_dtTime = COleDateTime::GetCurrentTime();
			GlobalFuncs::WriteProfileString (m_LineRec.m_strName, GlobalVars::strState, GlobalFuncs::GetState (RUNNING));
			report.m_strCounts.Format (_T ("%lu"), GlobalFuncs::GetProfileInt (m_LineRec.m_strName, GlobalVars::strCount, 0));

			if (theApp.IsPrintReportLoggingEnabled ()) 
				AddPrinterReportRecord (FoxjetDatabase::REPORT_TASK_RESUME, report);
		}
	}

	EnableDynamicDataDownload (true);
}

void CProdLine::CalcProductLen ()
{
#ifdef __WINPRINTER__
	LOCK (m_HeadListSentinel);
	{
		// calc min/max photo offsets
		CArray <CLocalHead *, CLocalHead *> vPC1, vPC2, vPC3, vPC4, vPC5, vPC6, vPC7, vPC8;
		CArray <CLocalHead *, CLocalHead *> * pv [] = { &vPC1, &vPC2, &vPC3, &vPC4, &vPC5, &vPC6, &vPC7, &vPC8 };

		/* TODO: rem
		{
			CArray <HEADSTRUCT, HEADSTRUCT &> v;

			GetLineHeads (theApp.m_Database, m_LineRec.m_lID, v);

			for (int i = 0; i < v.GetSize (); i++) {
				CString str;
				HEADSTRUCT h = v [i];

				str.Format (_T ("%s: m_nPhotocell: %d, m_nSharePhoto: %d"), h.m_strName, h.m_nPhotocell, h.m_nSharePhoto);
				TRACEF (str);
			}
		}
		*/ 

		for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {
			CHead * pHead = NULL;
			DWORD dwKey;

			m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
			ASSERT (pHead);

			if (CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pHead)) {
				CHeadInfo info = CHead::GetInfo (p);
				bool bEnabled = !info.m_bDisabled;

				if (bEnabled) {
					const ULONG lAddr = _tcstoul (info.m_Config.m_HeadSettings.m_strUID, 0, 16);
					int nIndex = -1;
					bool bUSB = false;

					SEND_THREAD_MESSAGE (info, TM_IS_USB, &bUSB);

					//TRACEF (info.m_Config.m_HeadSettings.m_strName);

					if (bUSB) {
						switch (info.m_Config.m_HeadSettings.m_nPhotocell) {
						case EXTERNAL_PC:	
						case INTERNAL_PC:	
							nIndex = lAddr - 1;
							break;
						default:
						case SHARED_PC:
							switch (info.m_Config.m_HeadSettings.m_nSharePhoto) {
							case SHARED_PCA:	
								nIndex = 0;
								break;
							case SHARED_PCB:
								nIndex = 1;
								break;
							default:
								/*
								m_head.m_nSharePhoto
									USB_1a [1] --> 4
									USB_1b [2] --> 5
									USB_2a [3] --> 6
									USB_2b [4] --> 7
									USB_3a [5] --> 8
									USB_3b [6] --> 9
									USB_4a [7] --> 10
									USB_4b [8] --> 11
								*/
								nIndex = (info.m_Config.m_HeadSettings.m_nSharePhoto - 3) - 1;	
								break;
							}
						}
					}
					else {
						switch (info.m_Config.m_HeadSettings.m_nPhotocell) {
						case EXTERNAL_PC:	
						case INTERNAL_PC:	
							/*
								0x300 [768] : 0
								0x310 [784] : 1
								0x320 [800] : 2
								0x330 [816] : 3
							*/
							nIndex = ((lAddr - 0x300) / 0x10);
							TRACEF (ToString (lAddr) + _T (" --> ") + ToString (nIndex));
							break;
						default:
						case SHARED_PC:
							switch (info.m_Config.m_HeadSettings.m_nSharePhoto) {
							case SHARED_PCA:	
								nIndex = 0;
								break;
							case SHARED_PCB:
								nIndex = 1;
								break;
							default:
								ASSERT (0);
								break;
							}
						}
					}
					
					ASSERT (nIndex >= 0 && nIndex < ARRAYSIZE (pv));
					pv [nIndex]->Add (p);
				}
			}
		}

		for (int i = 0; i < ARRAYSIZE (pv); i++) {
			ULONG lMaxPhotocellDelay = Foxjet3d::CHeadDlg::m_lmtMphcPhotocellDelay.m_dwMin;
			ULONG lMinPhotocellDelay = Foxjet3d::CHeadDlg::m_lmtMphcPhotocellDelay.m_dwMax;
			UINT nMaxFlush = 0;

			{
				for (int nHead = 0; nHead < pv [i]->GetSize (); nHead++) {
					CLocalHead * p = pv [i]->GetAt (nHead);
					//TRACEF (info.m_Config.m_HeadSettings.m_strName);
				}

				//TRACEF ("");
			}

			for (int nHead = 0; nHead < pv [i]->GetSize (); nHead++) {
				if (CLocalHead * p = pv [i]->GetAt (nHead)) {
					CHeadInfo info = CHead::GetInfo (p);

					lMinPhotocellDelay = min (info.m_Config.m_HeadSettings.m_lPhotocellDelay, lMinPhotocellDelay);
					lMaxPhotocellDelay = max (info.m_Config.m_HeadSettings.m_lPhotocellDelay, lMaxPhotocellDelay);
					nMaxFlush = max (info.CalcFlushBuffer (), nMaxFlush);
				}
			}

			for (int nHead = 0; nHead < pv [i]->GetSize (); nHead++) {
				CHead::PHOTOCELLOFFSETSTRUCT s (lMinPhotocellDelay, lMaxPhotocellDelay, nMaxFlush);
				CHeadInfo info = CHead::GetInfo (pv [i]->GetAt (nHead));

				SEND_THREAD_MESSAGE (info, TM_SET_PHOTOCELL_OFFSETS, &s);
			}
		}
	}

#endif //__WINPRINTER__
}

CString CProdLine::GetCount () 
{
	__int64 lTmp, lMinCount = 0, lMaxCount = 0, lMasterCount = 0;
	__int64 lCountUntil = GetCountUntil ();

	GetCounts (NULL, lTmp, lMinCount, lMaxCount, lMasterCount);
	CString str = FoxjetCommon::FormatI64 (lMasterCount);

	if (lCountUntil > 0) 
		str += _T ( "[") + FoxjetCommon::FormatI64 (lCountUntil) + _T ("]");

	return str;
}

FoxjetDatabase::TASKSTATE CProdLine::GetTaskState () const
{
	for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {
		CHead * pHead = NULL;
		DWORD dwKey;

		m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
		ASSERT (pHead);
		CHeadInfo info = CHead::GetInfo (pHead);

		if (info.IsMaster()) {
			TASKSTATE result = DISABLED;
			SEND_THREAD_MESSAGE(info, WM_GET_TASK_STATE, &result);
			return result;
		}
	}

	return STOPPED;
}

LONG CProdLine::GetDynamicID (FoxjetCommon::CBaseElement * pElement)
{
	LONG lResult = -1;

	using namespace FoxjetIpElements;

	if (CDynamicTextElement * p = DYNAMIC_DOWNCAST (CDynamicTextElement, pElement)) 
		lResult = (int)p->GetDynamicID ();
	else if (CDynamicBarcodeElement * p = DYNAMIC_DOWNCAST (CDynamicBarcodeElement, pElement)) 
		lResult = (int)p->GetDynamicID ();


	return lResult;
}

void CProdLine::InsertAscByDynID (CElementPtrArray & vUser, FoxjetCommon::CBaseElement * pElement) 
{
	using namespace FoxjetIpElements;

	int nIndex = GetDynamicID (pElement);

	for (int i = 0; i < vUser.GetSize (); i++) {
		if (nIndex < GetDynamicID (vUser [i])) {
			vUser.InsertAt (i, pElement);
			return;
		}
	}

	vUser.Add (pElement);
}

void CProdLine::GetUserElements (Foxjet3d::UserElementDlg::CUserElementArray & vPrompt, bool bTaskStart)
{
#ifdef __WINPRINTER__
	using namespace FoxjetElements;
#else
	using namespace FoxjetIpElements;
	using FoxjetIpElements::CTableItem;

	CArray <CTableItem, CTableItem &> vItems;

	CDynamicTableDlg::GetDynamicData (GetID (), vItems);
#endif

	for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {
		DWORD dwKey;
		CHead * pHead = NULL;
		CElementPtrArray vUser;
		bool bNEXT = false;

		m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
		ASSERT (pHead);
		CHeadInfo info = CHead::GetInfo (pHead);

		#ifdef __WINPRINTER__
			CHead::GETELEMENTSTRUCT s (&vUser, USER);

			s.m_nClassID = USER;
			SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);
			
			s.m_nClassID = DATABASE;
			SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

			s.m_nClassID = BMP;
			SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

			s.m_nClassID = EXPDATE;
			SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s); // sw0924

			{ 
				int n = vUser.GetSize ();
				s.m_nClassID = FoxjetIpElements::DYNAMIC_TEXT;
				SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

				s.m_nClassID = FoxjetIpElements::DYNAMIC_BARCODE;
				SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

				bNEXT = (n == 0 && vUser.GetSize () > n) ? true : false;
			}

			if (!bNEXT) {
				CString strKeyValue;
				FoxjetDatabase::USERSTRUCT user;
				FoxjetUtils::CDatabaseStartDlg dlgDB (NULL);

				dlgDB.Load (ListGlobals::GetDB ());
				
				for (int i = 0; i < vUser.GetSize (); i++) {
					bool bSet = Foxjet3d::UserElementDlg::CUserElementDlg::Get (vUser [i], info.m_Config.m_HeadSettings, vPrompt, bTaskStart);

					if (!bTaskStart && strKeyValue.GetLength ()) {
						for (int j = 0; j < vPrompt.GetSize (); j++) {
							Foxjet3d::UserElementDlg::CUserElementDlg::Set (vUser [i], info.m_Config.m_HeadSettings, vPrompt [j].m_strPrompt, vPrompt [j].m_strData, strKeyValue, user, dlgDB);
						}
					}

					if (!bSet) {
						if (FoxjetCommon::IsProductionConfig ()) //#if __CUSTOM__ == __SW0833__ 
							if (CLocalHead * pLocalHead = DYNAMIC_DOWNCAST (CLocalHead, pHead)) 
								if (Foxjet3d::GetTestPHC (info.GetIOAddress ())) 
									if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, vUser [i])) 
										vPrompt.Add (Foxjet3d::UserElementDlg::CUserItem (p, &info.m_Config.m_HeadSettings));
					}
				}
			}
			else {
				using namespace FoxjetIpElements;
				using FoxjetIpElements::CTableItem;

				CArray <CTableItem, CTableItem &> vItems;
				CElementPtrArray vTmp;

				vTmp.Append (vUser);
				vUser.RemoveAll ();
				FoxjetIpElements::GetDynamicData (GetID (), vItems);

				// merge sort
				for (int i = 0; i < vTmp.GetSize (); i++)
					InsertAscByDynID (vUser, vTmp [i]);

				for (int i = 0; i < vItems.GetSize (); i++) 
					if (vItems [i].m_strData.GetLength ())
						TRACEF (ToString (vItems [i].m_lID) + _T (": ") + vItems [i].m_strPrompt + _T (": ") + vItems [i].m_strData);

				for (int i = 0; i < vUser.GetSize (); i++) {
					FoxjetCommon::CBaseElement * pElement = vUser [i];
					int nIndex = GetDynamicID (pElement) - 1;

					if (nIndex >= 0 && nIndex < vItems.GetSize ()) {
						CTableItem item = vItems [nIndex];
						CString strData = item.m_strData;// pRemote->m_strDynData [nIndex];

						item.m_strPrompt = ToString (nIndex + 1);

						if (bTaskStart) {
							strData = item.m_strData;
						}
						else {
							if (FoxjetIpElements::CDynamicTextElement * p = DYNAMIC_DOWNCAST (FoxjetIpElements::CDynamicTextElement, pElement))
								strData = p->GetImageData ();
							else if (FoxjetIpElements::CDynamicBarcodeElement * p = DYNAMIC_DOWNCAST (FoxjetIpElements::CDynamicBarcodeElement, pElement))
								strData = p->GetImageData ();
						}

						if ((bTaskStart && item.m_bPrompt) || !bTaskStart)
							if (!UserElementDlg::CUserElementDlg::Find (item.m_strPrompt, vPrompt)) 
								vPrompt.Add (UserElementDlg::CUserItem (pElement, &info.m_Config.m_HeadSettings, item.m_strPrompt, strData));
					}
				}
			}
		#else
			if (CRemoteHead * pRemote = DYNAMIC_DOWNCAST (CRemoteHead, pHead)) {
				CElementPtrArray vTmp;
				CHead::GETELEMENTSTRUCT s (&vTmp, DYNAMIC_TEXT);

				s.m_nClassID = DYNAMIC_TEXT;
				SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);
				s.m_nClassID = DYNAMIC_BARCODE;
				SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

				// merge sort
				for (int i = 0; i < vTmp.GetSize (); i++)
					InsertAscByDynID (vUser, vTmp [i]);

				for (int i = 0; i < vUser.GetSize (); i++) {
					FoxjetCommon::CBaseElement * pElement = vUser [i];
					int nIndex = GetDynamicID (pElement);

					ASSERT (nIndex >= 0 && nIndex < ARRAYSIZE (pRemote->m_strDynData));

					CTableItem item = CDynamicTableDlg::GetDynamicData (vItems, nIndex);
					CString strData = pRemote->m_strDynData [nIndex];

					if (bTaskStart)
						strData = item.m_strData;

					if ((bTaskStart && item.m_bPrompt) || !bTaskStart)
						if (!UserElementDlg::CUserElementDlg::Find (item.m_strPrompt, vPrompt)) 
							vPrompt.Add (UserElementDlg::CUserItem (pElement, &info.m_Config.m_HeadSettings, 
								item.m_strPrompt, strData));
				}
			}
		#endif
	}
}

void CProdLine::GetElements (CElementPtrArray & v, int nClassID)
{
	for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {
		DWORD dwKey;
		Head::CHead * pHead = NULL;

		m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
		ASSERT (pHead);
		CHeadInfo info = CHead::GetInfo (pHead);
		CHead::GETELEMENTSTRUCT s (&v, nClassID);

		SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);
	}
}

bool CProdLine::GetLabelBatchQty (ULONG & lLabels)
{
	CLabelBatchQtyDlg dlg (theApp.GetActiveFrame ());
	bool bResult = false;

	dlg.m_dwQuantity = lLabels;

	if (dlg.DoModal () == IDOK) {
		lLabels = dlg.m_dwQuantity;
		bResult = true;
	}

	return bResult;
}

int CProdLine::UpdateUserElements (const Foxjet3d::UserElementDlg::CUserElementArray & vElements, CHead * pHead,
								   bool bPrompt)
{
	int nResult = 0;

	ASSERT (pHead);

#ifdef __IPPRINTER__

	using namespace FoxjetIpElements;

	CItemArray vItems;
	CItemArray * pvUpdate = new CItemArray ();

	CDynamicTableDlg::GetDynamicData (GetID (), vItems);

	for (int nReplace = 0; nReplace < vElements.GetSize (); nReplace++) {
		UserElementDlg::CUserItem replace = vElements [nReplace];
		const CString strPrompt = replace.m_strPrompt;
		const CString strData = replace.m_strData;
		ULONG lID = -1;

		if (CDynamicTextElement * p = DYNAMIC_DOWNCAST (CDynamicTextElement, replace.m_pElement)) 
			lID = p->GetDynamicID ();
		else if (CDynamicBarcodeElement * p = DYNAMIC_DOWNCAST (CDynamicBarcodeElement, replace.m_pElement)) 
			lID = p->GetDynamicID ();

		int nIndex = CDynamicTableDlg::Find (vItems, lID);

		if (nIndex != -1) {
			CTableItem update = vItems [nIndex];

			update.m_strData = strData;

			if (update.m_bUseDatabase) {
				update.m_params.m_strKeyValue = strData;

				try
				{
					update.m_strData = update.GetSqlResult ();
				}
				catch (CDBException * e) { 
					CString str = FoxjetDatabase::FormatException (e, _T (__FILE__), __LINE__) + _T ("\n\n") +
						update.CreateSQL ();

					if (bPrompt)
						MsgBox (str); 

					HANDLEEXCEPTION (e);  
					return false; 
				}
				catch (CMemoryException * e) {
					CString str = FoxjetDatabase::FormatException (e, _T (__FILE__), __LINE__) + _T ("\n\n") +
						update.CreateSQL ();

					if (bPrompt)
						MsgBox (str); 
	
					HANDLEEXCEPTION (e);  
					return false; 
				}
			}

			/* TODO: rem // 3.09
			if (CDynamicTableDlg::Find (* pvUpdate, update.m_lID) == -1)
				pvUpdate->Add (update);
			*/

			// 3.09
			for (int nFind = 0; nFind < vItems.GetSize (); nFind++) {
				CTableItem & item = vItems [nFind];

				if (!item.m_strPrompt.CompareNoCase (strPrompt)) {
					item.m_strData = update.m_strData;
					nResult++;
					TRACEF (item.m_strPrompt + " --> " + update.m_strData);

					if (CDynamicTableDlg::Find (* pvUpdate, item.m_lID) == -1) 
						pvUpdate->Add (item);
				}
			}
		}
	}

	{ // send any data that is not prompted for
		CElementPtrArray vTmp;

		GetElements (vTmp, DYNAMIC_TEXT);
		GetElements (vTmp, DYNAMIC_BARCODE);

		for (int i = 0; i < vTmp.GetSize (); i++) {
			ULONG lDynamicID = -1;

			if (CDynamicTextElement * p = DYNAMIC_DOWNCAST (CDynamicTextElement, vTmp [i]))
				lDynamicID = p->GetDynamicID ();
			else if (CDynamicBarcodeElement * p = DYNAMIC_DOWNCAST (CDynamicBarcodeElement, vTmp [i]))
				lDynamicID = p->GetDynamicID ();

			int nIndex = CDynamicTableDlg::Find (vItems, lDynamicID);

			if (nIndex != -1) {
				CTableItem item = vItems [nIndex];

				if (CDynamicTableDlg::Find (* pvUpdate, item.m_lID) == -1) 
					pvUpdate->Add (item);
			}
		}
	}

	if (CRemoteHead * pRemoteHead = DYNAMIC_DOWNCAST (CRemoteHead, pHead)) {
		CEvent event (false, true, _T ("TM_DOWNLOAD_DYNDATA: ") + ToString (pHead->m_nThreadID), NULL);
		bool bEnabled = pRemoteHead->IsDynamicDataDownloadEnabled ();

		pRemoteHead->EnableDynamicDataDownload (true);

		while (!::PostThreadMessage (pHead->m_nThreadID, TM_DOWNLOAD_DYNDATA, (WPARAM)(HANDLE)event, (LPARAM)pvUpdate))
			::Sleep (0);

		DWORD dwWait = ::WaitForSingleObject (event, pvUpdate->GetSize () * 1000);

		//if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
		if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
		if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
		if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");

		ASSERT (dwWait == WAIT_OBJECT_0);

		pRemoteHead->EnableDynamicDataDownload (bEnabled);
	}
#endif //__IPPRINTER__

	return nResult;
}

bool CProdLine::ChangeUserElementData(bool bTaskStart, const CString & strKeyValue, CMapStringToString * pvSet, int nPeriod)
{
	using namespace Foxjet3d;

	CDbConnection conn (_T (__FILE__), __LINE__); 

	if (!conn.OpenExclusive (theApp.GetActiveFrame ()))
		return false;

#ifdef __WINPRINTER__
	using namespace FoxjetElements;
#else
	using namespace FoxjetIpElements;
#endif 

	UserElementDlg::CUserElementDlg dlg (GetID (), theApp.GetActiveFrame ());
	bool bResult = false;
	UserElementDlg::CUserElementArray & vElements = dlg.m_vElements;
	CString strSysID;

	BEGIN_TRANS (theApp.m_Database);

	{
		SETTINGSSTRUCT s;

		s.m_strData = _T ("XX");
		GetSettingsRecord (theApp.m_Database, 0, _T ("SYSID: ") + ToString (GetPrinterID (theApp.m_Database)), s);
		strSysID = s.m_strData;
		TRACEF (s.m_strData);
	}

	//Remap (theApp.m_Database, task, GetPrinterID (theApp.m_Database), GetMasterPrinterID (theApp.m_Database));

	LOCK (m_HeadListSentinel);
	{
		GetUserElements (vElements, bTaskStart);

		#ifdef __WINPRINTER__
		if (strKeyValue.GetLength ()) { // sw0858 / sw0866
			if (vElements.GetSize ()) {
				FoxjetUtils::CDatabaseStartDlg dlgDB (NULL);

				dlgDB.Load (theApp.m_Database);
						
				TRACEF (dlgDB.m_strDSN);
				TRACEF (dlgDB.m_strTable);
				TRACEF (dlgDB.m_strKeyField);
				TRACEF ("");

				for (int i = vElements.GetSize () - 1; i >= 0; i--) {
					if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, vElements [i].m_pElement)) {
						TRACEF (p->GetDSN ());
						TRACEF (p->GetTable ());
						TRACEF (p->GetKeyField ());
						TRACEF ("");

						if (!p->GetDSN ().CompareNoCase (dlgDB.m_strDSN) &&
							!p->GetTable ().CompareNoCase (dlgDB.m_strTable) &&
							!p->GetKeyField ().CompareNoCase (dlgDB.m_strKeyField))
						{
							vElements.RemoveAt (i);

							for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {
								CHead * pHead = NULL;
								DWORD dwKey;
								bool bBuild = false;

								m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
								ASSERT (pHead);

								CElementPtrArray list;
								CHeadInfo info = CHead::GetInfo (pHead);
								CHead::GETELEMENTSTRUCT s (&list, DATABASE);

								SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

								s.m_nClassID = EXPDATE;
								SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s); // sw0924

								s.m_nClassID = BMP;
								SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

								for (int nElement = 0; nElement < list.GetSize (); nElement++) {
									if (CDatabaseElement * pSet = DYNAMIC_DOWNCAST (CDatabaseElement, list [nElement])) {
										if (!pSet->GetDSN ().CompareNoCase (dlgDB.m_strDSN) &&
											!pSet->GetTable ().CompareNoCase (dlgDB.m_strTable) &&
											!pSet->GetKeyField ().CompareNoCase (dlgDB.m_strKeyField))
										{
											pSet->SetKeyValue (strKeyValue);
										}
									}
									else if (CBitmapElement * pSet = DYNAMIC_DOWNCAST (CBitmapElement, list [nElement])) {
										if (pSet->IsDatabaseLookup ()) {
											if (!pSet->GetDatabaseDSN ().CompareNoCase (dlgDB.m_strDSN) &&
												!pSet->GetDatabaseTable ().CompareNoCase (dlgDB.m_strTable) &&
												!pSet->GetDatabaseKeyField ().CompareNoCase (dlgDB.m_strKeyField))
											{
												pSet->SetDatabaseKeyValue (strKeyValue);
											}
										}
									}
								}
							}
						}
					}
					else if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, vElements [i].m_pElement)) {
						TRACEF (p->GetDatabaseDSN ());
						TRACEF (p->GetDatabaseTable ());
						TRACEF (p->GetDatabaseKeyField ());
						TRACEF ("");

						if (!p->GetDatabaseDSN ().CompareNoCase (dlgDB.m_strDSN) &&
							!p->GetDatabaseTable ().CompareNoCase (dlgDB.m_strTable) &&
							!p->GetDatabaseKeyField ().CompareNoCase (dlgDB.m_strKeyField))
						{
							vElements.RemoveAt (i);

							for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {
								CHead * pHead = NULL;
								DWORD dwKey;
								bool bBuild = false;

								m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
								ASSERT (pHead);

								CElementPtrArray list;
								CHeadInfo info = CHead::GetInfo (pHead);
								CHead::GETELEMENTSTRUCT s (&list, DATABASE);

								SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

								s.m_nClassID = BMP;
								SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s); // sw0924

								for (int nElement = 0; nElement < list.GetSize (); nElement++) {
									if (CBitmapElement * pSet = DYNAMIC_DOWNCAST (CBitmapElement, list [nElement])) {
										if (pSet->IsDatabaseLookup ()) {
											if (!pSet->GetDatabaseDSN ().CompareNoCase (dlgDB.m_strDSN) &&
												!pSet->GetDatabaseTable ().CompareNoCase (dlgDB.m_strTable) &&
												!pSet->GetDatabaseKeyField ().CompareNoCase (dlgDB.m_strKeyField))
											{
												pSet->SetDatabaseKeyValue (strKeyValue);
											}
										}
									}
								}
							}
						}
					}
					else if (CExpDateElement * p = DYNAMIC_DOWNCAST (CExpDateElement, vElements [i].m_pElement)) { // sw0924
						if (p->GetType () == CExpDateElement::TYPE_DATABASE) {
							TRACEF (p->GetDSN ());
							TRACEF (p->GetTable ());
							TRACEF (p->GetKeyField ());
							TRACEF ("");

							if (p->GetType () == CExpDateElement::TYPE_DATABASE) {
								TRACEF (p->ToString (FoxjetCommon::verApp));
							}

							if (!p->GetDSN ().CompareNoCase (dlgDB.m_strDSN) &&
								!p->GetTable ().CompareNoCase (dlgDB.m_strTable) &&
								!p->GetKeyField ().CompareNoCase (dlgDB.m_strKeyField))
							{
								vElements.RemoveAt (i);

								for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {
									CHead * pHead = NULL;
									DWORD dwKey;
									bool bBuild = false;

									m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
									ASSERT (pHead);

									CElementPtrArray list;
									CHeadInfo info = CHead::GetInfo (pHead);
									CHead::GETELEMENTSTRUCT s (&list, EXPDATE);

									SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

									for (int nElement = 0; nElement < list.GetSize (); nElement++) {
										if (CExpDateElement * pSet = DYNAMIC_DOWNCAST (CExpDateElement, list [nElement])) {
											if (pSet->GetType () == CExpDateElement::TYPE_DATABASE) {
												if (!pSet->GetDSN ().CompareNoCase (dlgDB.m_strDSN) &&
													!pSet->GetTable ().CompareNoCase (dlgDB.m_strTable) &&
													!pSet->GetKeyField ().CompareNoCase (dlgDB.m_strKeyField))
												{
													CDatabaseElement e (pSet->GetHead ());

													pSet->SetKeyValue (strKeyValue);

													e.Set (* pSet);
													e.Build ();
													CString str = e.GetImageData ();
													TRACEF (str);
													pSet->SetSpan (_ttoi (str));
												}
											}
										}
									}
								}
							}
							break;
						}
					}
				}
			}

			if (!vElements.GetSize ())
				bResult = true;
		}
		#endif //__WINPRINTER__

		bool bSet;
		int nSilent = 0;
		CArray <CUserElement *, CUserElement *> vSysID;
		CArray <CUserElement *, CUserElement *> vUserFirst, vUserLast, vUserUser;
	
		#ifdef __WINPRINTER__
		if (IsGojo ()) {
			for (int i = vElements.GetSize () - 1; i >= 0; i--) {
				if (CUserElement * p = DYNAMIC_DOWNCAST (CUserElement, vElements [i].m_pElement)) {
					if (p->GetPrompt ().CompareNoCase (CUserElement::m_lpszSysID) == 0) {
						vElements.RemoveAt (i);
						vSysID.Add (p);
					}
				}
			}
		}

		for (int i = vElements.GetSize () - 1; i >= 0; i--) {
			if (CUserElement * p = DYNAMIC_DOWNCAST (CUserElement, vElements [i].m_pElement)) {
				if (p->IsNoPrompt ())
					nSilent++;

				if (p->GetPrompt ().CompareNoCase (CUserElement::m_lpszUserFirst) == 0) {
					vElements.RemoveAt (i);
					vUserFirst.Add (p);
				}
				else if (p->GetPrompt ().CompareNoCase (CUserElement::m_lpszUserLast) == 0) {
					vElements.RemoveAt (i);
					vUserLast.Add (p);
				}
				else if (p->GetPrompt ().CompareNoCase (CUserElement::m_lpszUserName) == 0) {
					vElements.RemoveAt (i);
					vUserUser.Add (p);
				}
			}
		}
		#endif //__WINPRINTER__

		if (!pvSet)
			bSet = (vElements.GetSize () && dlg.DoModal () == IDOK);
		else {
			bSet = vElements.GetSize () ? true : false;

			for (POSITION pos = pvSet->GetStartPosition (); pos; ) {
				CString strPrompt, strData;

				pvSet->GetNextAssoc (pos, strPrompt, strData);

				#ifdef __WINPRINTER__
				for (int i = 0; i < vElements.GetSize (); i++) {
					UserElementDlg::CUserItem & item = vElements [i];

					if (!item.m_strPrompt.CompareNoCase (strPrompt)) {
						item.m_strData = strData;

						if (CUserElement * pReplace = DYNAMIC_DOWNCAST (CUserElement, item.m_pElement)) 
							pReplace->SetDefaultData (strData);
						else if (CDatabaseElement * pReplace = DYNAMIC_DOWNCAST (CDatabaseElement, item.m_pElement)) 
							pReplace->SetKeyValue (strData);
						else if (CBitmapElement * pReplace = DYNAMIC_DOWNCAST (CBitmapElement, item.m_pElement)) {
							if (pReplace->IsDatabaseLookup ())
								pReplace->SetDatabaseKeyValue (strData);
							else {
								item.m_strData = strData = pReplace->GetFilePath ();
								pReplace->SetFilePath (strData, item.m_head);
								Foxjet3d::UserElementDlg::CUserElementDlg::ResetSize (pReplace, item.m_head);
							}
						}
						else if (CExpDateElement * pReplace = DYNAMIC_DOWNCAST (CExpDateElement, item.m_pElement)) { // sw0924
							switch (pReplace->GetType ()) {
							case CExpDateElement::TYPE_DATABASE:
								{
									CDatabaseElement e (pReplace->GetHead ());

									e.Set (* pReplace);
									e.Build ();
									CString str = e.GetImageData ();
									TRACEF (str);
									pReplace->SetSpan (_ttoi (str));
								}
								break;
							case CExpDateElement::TYPE_USER:
								pReplace->SetSpan (_ttoi (strData));
								break;
							}
						}
					}
				}
				#endif //__WINPRINTER__
			}
		}

		#ifdef __WINPRINTER__
		if (IsGojo ()) {
			if (vSysID.GetSize ()) {
				vElements.Add (UserElementDlg::CUserItem (vSysID [0], NULL, CUserElement::m_lpszSysID, strSysID));
				bSet = true;
			}
		}
		#endif //__WINPRINTER__

		if (bSet) {
			CArray <UserElementDlg::CUserItem, UserElementDlg::CUserItem &> vNEXT;

			bResult = true;

			for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {
				CHead * pHead = NULL;
				DWORD dwKey;
				bool bBuild = false;

				m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
				ASSERT (pHead);
				CHeadInfo info = CHead::GetInfo (pHead);

				#ifdef __WINPRINTER__
					for (int nReplace = 0; nReplace < vElements.GetSize (); nReplace++) {
						CElementPtrArray list;
						CString strPrompt, strData;
						CHead::GETELEMENTSTRUCT s (&list, USER);

						s.m_nClassID = USER;
						SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

						s.m_nClassID = DATABASE;
						SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

						s.m_nClassID = BMP;
						SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

						{
							CElementPtrArray listBmp;

							CHead::GETELEMENTSTRUCT tmp (&listBmp, BMP);
							SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &tmp);

							for (int i = 0; i < listBmp.GetSize (); i++) {
								if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, listBmp [i])) {
									if (p->IsUserPrompted ())
										list.Add (listBmp [i]);
								}
							}
						}

						if (theApp.IsExpo () || FoxjetCommon::IsProductionConfig ()) {//#if __CUSTOM__ == __SW0833__
							s.m_nClassID = BMP;
							SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);
						}

						{ 
							s.m_nClassID = FoxjetIpElements::DYNAMIC_TEXT;
							SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

							s.m_nClassID = FoxjetIpElements::DYNAMIC_BARCODE;
							SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);
						}

						if (CUserElement * pReplace = DYNAMIC_DOWNCAST (CUserElement, vElements [nReplace].m_pElement)) {
							strPrompt = pReplace->GetPrompt ();
							strData = pReplace->GetDefaultData ();

							#ifdef __WINPRINTER__
							if (IsGojo ()) {
								if (!strPrompt.CompareNoCase (CUserElement::m_lpszDate)) {
									try {
										COleDateTime t = COleDateTime::GetCurrentTime ();
										CString str = strData;

										TRACEF (str);
										
										if (!t.ParseDateTime (str)) {
											AfxMessageBox (LoadString (IDS_INVALIDTIME) + str);
											return false;
										}
									}
									catch (COleException * e)		{ HANDLEEXCEPTION (e); }
									catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
								}
							}								
							#endif //__WINPRINTER__
						}
						else if (CDatabaseElement * pReplace = DYNAMIC_DOWNCAST (CDatabaseElement, vElements [nReplace].m_pElement)) {
							strPrompt = pReplace->GetPrompt ();
							strData = pReplace->GetKeyValue ();
						}
						else if (CBitmapElement * pReplace = DYNAMIC_DOWNCAST (CBitmapElement, vElements [nReplace].m_pElement)) {
							if (pReplace->IsDatabaseLookup ()) {
								strPrompt = pReplace->GetDatabasePrompt ();
								strData = pReplace->GetDatabaseKeyValue ();
							}
							else { // __SW0833__
								strPrompt = pReplace->GetFilePath ();

								if (pReplace->IsUserPrompted ()) 
									strPrompt = pReplace->GetUserPrompt ();

								strData = pReplace->GetFilePath ();
							}
						}
						//#endif
						else if (CExpDateElement * pReplace = DYNAMIC_DOWNCAST (CExpDateElement, vElements [nReplace].m_pElement)) { // sw0924
							switch (pReplace->GetType ()) {
							case CExpDateElement::TYPE_DATABASE:
							case CExpDateElement::TYPE_USER:
								strPrompt = pReplace->GetPrompt ();
								strData = pReplace->GetKeyValue ();
								break;
							}
						}
						
						switch (vElements [nReplace].m_pElement->GetClassID ()) {
						case FoxjetIpElements::DYNAMIC_TEXT:
						case FoxjetIpElements::DYNAMIC_BARCODE:
							{
								UserElementDlg::CUserItem & item = vElements [nReplace];

								vNEXT.Add (item);
								fj_setWinDynData (GetDynamicID (item.m_pElement), w2a (item.m_strData));
							}
							break;
						}

						if (!pvSet) {
							//TRACEF (GlobalVars::strUserPrompted + CString (_T ("\\")) + strPrompt);
							//GlobalFuncs::WriteProfileString (m_LineRec.m_strName, GlobalVars::strUserPrompted + CString (_T ("\\")) + strPrompt, strData);
							const CString strFile = GetHomeDir () + _T ("\\") + GlobalVars::strUserPrompted + _T ("\\") + m_LineRec.m_strName + _T ("\\") + strPrompt + _T (".txt");
							FoxjetFile::WriteDirect (strFile, strData);
						}
						
						SetUserPrompted (list, strPrompt, strData, pHead);

						if (!bTaskStart) {
							const CString strFile = GetHomeDir () + _T ("\\") + GlobalVars::strUserPrompted + _T ("\\") + GetName () + _T ("\\") + strPrompt + _T (".txt");
							FoxjetFile::WriteDirect (strFile, strData);
						}
					}
				#else
					bResult = UpdateUserElements (vElements, pHead, true) ? true : false;
				#endif 
			}

			if (vNEXT.GetSize ()) {
				CArray <FoxjetIpElements::CTableItem, FoxjetIpElements::CTableItem &> vItems;
				CStringArray v, vData;

				FoxjetIpElements::GetDynamicData (GetID (), vItems);

				for (int j = 0; j < vNEXT.GetSize (); j++) {
					for (int i = 0; i < vItems.GetSize (); i++) {
						int nID = _ttoi (vNEXT [j].m_strPrompt);

						if (vItems [i].m_lID == nID) {
							FoxjetIpElements::CTableItem item = vItems [j];

							item.m_strPrompt = vNEXT [j].m_strPrompt;
							item.m_strData = vNEXT [j].m_strData;
							vItems.SetAt (i, item);
						}
					}
				}

				for (int j= 0; j < vItems.GetSize (); j++) 
					vData.Add (vItems [j].ToString ());

				v.Add (_T ("Dynamic data"));
				v.Add (ToString (vData));

				if (CControlView * pView = (CControlView *)theApp.GetActiveView ()) 
					VERIFY (pView->SendMessage (WM_NEXT_SET_SYSTEM_PARAMS, (WPARAM)GetID (), (LPARAM)&v) == 1);
			}
		}
	}

	UpdateImages ();

	if (IsGojo ())
		SetGojoElements (nPeriod);

	COMMIT_TRANS (theApp.m_Database);
	return bResult;
}

void CProdLine::SetGojoElements (int nPeriod)
{
#ifdef __WINPRINTER__
	using namespace FoxjetElements;

	CString strSysID;

	ASSERT (IsGojo ());

	{
		SETTINGSSTRUCT s;

		s.m_strData = _T ("XX");
		GetSettingsRecord (theApp.m_Database, 0, _T ("SYSID:") + ToString (GetPrinterID (theApp.m_Database)), s);
		strSysID = s.m_strData;
		TRACEF (s.m_strData);
	}

	for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {
		CHead * pHead = NULL;
		DWORD dwKey;
		bool bBuild = false;

		m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
		ASSERT (pHead);
		CHeadInfo info = CHead::GetInfo (pHead);

		CElementPtrArray list;
		CHead::GETELEMENTSTRUCT s (&list, USER);

		s.m_nClassID = USER;
		SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);

		for (int nFind = 0; nFind < list.GetSize (); nFind++) {
			if (CUserElement * pFind = DYNAMIC_DOWNCAST (CUserElement, list [nFind])) {
				CString strData = pFind->GetDefaultData ();

				if (!pFind->GetPrompt ().CompareNoCase (CUserElement::m_lpszSysID))
					strData = strSysID;
				else if (!pFind->GetPrompt ().CompareNoCase (CUserElement::m_lpszDate)) {
					CExpDateElement exp (info.m_Config.m_HeadSettings);

					exp.SetPeriod (CExpDateElement::PERIOD_MONTHS);
					exp.SetSpan (nPeriod);
					exp.SetDefaultData (_T ("%m/%Y"));
					exp.Build ();

					try {
						COleDateTime t = COleDateTime::GetCurrentTime ();
						CString str = strData;

						TRACEF (str);
						
						if (!t.ParseDateTime (str)) {
							AfxMessageBox (LoadString (IDS_INVALIDTIME) + str);
							break;//return false;
						}

						CTime tm (t.GetYear (), t.GetMonth (), t.GetDay (), t.GetHour (), t.GetMinute (), t.GetSecond ());
						TRACEF (tm.Format (_T ("%c")));
						exp.Build (INITIAL, false, tm);

						pFind->SetDefaultData (exp.GetImageData ());
						TRACEF (_T ("nPeriod: ") + ToString (nPeriod) + _T (" --> ") + pFind->GetDefaultData ());
					}
					catch (COleException * e)		{ HANDLEEXCEPTION (e); }
					catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
				}
			}
		}
	}
#endif //__WINPRINTER__
}

void CProdLine::SetUserPrompted (CElementPtrArray & list, const CString & strPrompt, const CString & strData, CHead * pHead)
{
	using namespace Foxjet3d;

	ASSERT (pHead);
	CHeadInfo info = CHead::GetInfo (pHead);

#ifdef __WINPRINTER__
	using namespace FoxjetElements;

	USERSTRUCT user;
	FoxjetUtils::CDatabaseStartDlg dlgDB (NULL);
	const CString strKeyValue = theApp.GetProfileString (_T ("sw0858"), _T ("Key value"));

	dlgDB.Load (theApp.m_Database);
			
	TRACEF (dlgDB.m_strDSN);
	TRACEF (dlgDB.m_strTable);
	TRACEF (dlgDB.m_strKeyField);

	GetUserRecord (theApp.m_Database, theApp.m_strUsername, user);

	if (!user.m_strFirstName.GetLength ())	user.m_strFirstName = _T (" ");
	if (!user.m_strLastName.GetLength ())	user.m_strLastName = _T (" ");
	if (!user.m_strUserName.GetLength ())	user.m_strUserName = _T (" ");

	TRACEF (user.m_strFirstName);
	TRACEF (user.m_strLastName);
	TRACEF (user.m_strUserName);
#else
	using namespace FoxjetIpElements;
#endif 

	#ifdef __WINPRINTER__
	for (int nFind = 0; nFind < list.GetSize (); nFind++) {
		if (!Foxjet3d::UserElementDlg::CUserElementDlg::Set (list [nFind], info.m_Config.m_HeadSettings, strPrompt, strData, strKeyValue, user, dlgDB)) {
			if (theApp.IsExpo ()) {
				using namespace FoxjetElements;
				using namespace FoxjetCommon;

				if (CBitmapElement * pFind = DYNAMIC_DOWNCAST (CBitmapElement, list [nFind])) {
					CString strPath = pFind->GetFilePath ();

					strPath.MakeUpper ();
					TRACEF (strPath);

					if (strPath.Find (_T ("PLACEHOLDER.BMP")) != -1) {
						CString strPath = pFind->GetDefPath () + strData + _T (".bmp");

						if (!pFind->SetFilePath (strPath, info.m_Config.m_HeadSettings))
							pFind->SetFilePath (pFind->GetDefPath () + _T ("placeholder.bmp"), info.m_Config.m_HeadSettings);

						CSize size;
					
						SEND_THREAD_MESSAGE (info, TM_GET_PRODUCTAREA, &size);

						pFind->SetPos (CPoint (0, 0), &info.m_Config.m_HeadSettings);
						pFind->SetSize (size, info.m_Config.m_HeadSettings);
					}
				}
			}
		}
	}
	#endif //__WINPRINTER__
}

bool CProdLine::ChangeCounts (bool bTaskStart, __int64 lPalletMax, __int64 lUnitsPerPallet, bool bCountDlg, CHead::CHANGECOUNTSTRUCT * pCount)
{
	bool bResult = false;

	#ifdef __WINPRINTER__
	using namespace FoxjetElements;
	#endif //__WINPRINTER__

	LOCK (m_HeadListSentinel);
	{
		bool bSetCounts = true;

	#ifdef __WINPRINTER__
		NamedCountDlg::CNamedCountDlg dlg (theApp.GetActiveFrame ());

		GetElements (dlg.m_vCounts, COUNT);

		bSetCounts = dlg.m_vCounts.GetSize () > 0;
	#else
		CCountDlg dlg (theApp.GetActiveFrame ());
	#endif

		if (bSetCounts) {
			__int64 lTmp, lMinCount = 0, lMaxCount = 0, lMasterCount = 0;

			GetCounts (NULL, lTmp, lMinCount, lMaxCount, lMasterCount);
			dlg.m_lCount = lMasterCount;

			#ifdef __WINPRINTER__
				for (int i = 0; i < dlg.m_vCounts.GetSize (); i++) {
					if (FoxjetElements::CCountElement * p = DYNAMIC_DOWNCAST (FoxjetElements::CCountElement, dlg.m_vCounts [i])) {
						CCountElement & e = * p;

						if (e.IsPalletCount ()) {
							dlg.m_lPalletMin = e.GetPalMin ();
							dlg.m_lPalletMax = e.GetPalMax ();
							dlg.m_lUnitsPerPallet = e.GetMax ();
						}

						if (e.IsIrregularPalletSize ())
							dlg.m_bIrregularPalletSize = true;
					}
				}

				for (int i = 0; i < dlg.m_vCounts.GetSize (); i++) {
					if (FoxjetElements::CCountElement * p = DYNAMIC_DOWNCAST (FoxjetElements::CCountElement, dlg.m_vCounts [i])) {
						CCountElement & e = * p;

						if (e.IsMaster () && e.IsPalletCount ()) {
							dlg.m_lPalletMin = e.GetPalMin ();
							dlg.m_lPalletMax = e.GetPalMax ();
							dlg.m_lUnitsPerPallet = e.GetMax ();
						}
					}
				}

				if (dlg.m_bIrregularPalletSize) {
					if (pCount) {
						dlg.m_lCount = pCount->m_lCount;
						dlg.m_count = * pCount;
						dlg.m_count.m_bIrregularPalletSize = dlg.m_bIrregularPalletSize;

						if (!dlg.m_count.m_lPalletMax || !dlg.m_count.m_lUnitsPerPallet) {
							dlg.m_lPalletCount = dlg.m_count.m_lPalletCount;
							dlg.m_count.m_lPalletMin		= dlg.m_lPalletMin;
							dlg.m_count.m_lPalletMax		= dlg.m_lPalletMax;
							dlg.m_count.m_lUnitsPerPallet	= dlg.m_lUnitsPerPallet;
						}
					}
				}
			#endif //__WINPRINTER__

			if (theApp.Is0844 ()) {
				#ifdef __WINPRINTER__
					for (int i = 0; i < dlg.m_vCounts.GetSize (); i++) {
						if (FoxjetElements::CCountElement * p = DYNAMIC_DOWNCAST (FoxjetElements::CCountElement, dlg.m_vCounts [i])) {
							CCountElement & e = * p;

							if (lPalletMax > 0)
								e.SetPalMax (lPalletMax); 

							if (e.IsPalletCount () && lUnitsPerPallet > 0)
								e.SetMax (lUnitsPerPallet); 
						}
					}
				#endif //__WINPRINTER__

				if (bTaskStart) 
					dlg.m_lCount = 0;

				if (!bCountDlg) 
					return true;
			}

			if (dlg.DoModal () == IDOK) {
				__int64 lCount = dlg.m_lCount;
				bResult = true;

				::AfxGetMainWnd ()->BeginWaitCursor ();
				const auto state = GetTaskState ();

				if (state == RUNNING)
					IdleTask (false);

				#ifdef __IPPRINTER__
				lCount = dlg.m_lCount - 1;
				#endif

				__int64 lPalletCount = 0, lPalletMin = 0, lPalletMax = 0;
				__int64 lUnitsPerPallet = 0;
	
				#ifdef __WINPRINTER__
				lPalletCount	= dlg.m_lPalletCount;
				lPalletMin		= dlg.m_lPalletMin;
				lPalletMax		= dlg.m_lPalletMax;
				lUnitsPerPallet = dlg.m_lUnitsPerPallet;
				#endif 

				if (dlg.m_bIrregularPalletSize) {
					if (pCount) {
						lCount			= pCount->m_lCount			= dlg.m_lBoxCount;
						lPalletCount	= pCount->m_lPalletCount	= dlg.m_lPalletCount;
						lPalletMin		= pCount->m_lPalletMin		= dlg.m_lPalletMin;
						lPalletMax		= pCount->m_lPalletMax		= dlg.m_lPalletMax;
						lUnitsPerPallet = pCount->m_lUnitsPerPallet	= dlg.m_lUnitsPerPallet;
					}
				}

				//theApp.WriteProfileInt (m_LineRec.m_strName, _T ("Count"), lCount);
				SetCounts (lCount, lPalletMax, lUnitsPerPallet, lPalletCount, lPalletMin);

				if (state == RUNNING)
					ResumeTask (false);

				::AfxGetMainWnd ()->EndWaitCursor ();
			}
		}
	}

	return bResult;
}

void CProdLine::DownloadImages ()
{
#ifdef __WINPRINTER__
	for (POSITION pos = m_listHeads.GetStartPosition(); pos != NULL; ) {
		CHead * pHead = NULL;
		DWORD dw = 0;

		m_listHeads.GetNextAssoc(pos, dw, (void *&)pHead);
		ASSERT (pHead);

		if (CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pHead)) {
			CHeadInfo info = CHead::GetInfo (pHead);

			if (info.IsMaster () || info.m_Config.m_HeadSettings.m_nPhotocell != SHARED_PC) {
				//TRACEF (_T ("CProdLine::DownloadImages: ") + info.GetFriendlyName ());
				POST_THREAD_MESSAGE (info, TM_DOWNLOAD_IMAGES, 0);
			}
		}
	}
#endif //__WINPRINTER__
}

bool CProdLine::SetCounts (__int64 lCount, __int64 lPalletMax, __int64 lUnitsPerPallet, __int64 lPalletCount, __int64 lPalletMin)
{
	bool bResult = true;

#ifdef __WINPRINTER__
	using namespace FoxjetElements;
#endif //__WINPRINTER__

	LOCK (m_HeadListSentinel);
	{
		HANDLE hEvent [MAX_HEADS] = { NULL };
		int nHeads = 0;

		for (POSITION pos = m_listHeads.GetStartPosition(); pos; nHeads++) {
			CHead * pHead = NULL;
			CHead::CHANGECOUNTSTRUCT * pParam = new CHead::CHANGECOUNTSTRUCT;
			DWORD dwKey;

			pParam->m_lCount			= lCount;
			pParam->m_lPalletMax		= lPalletMax;
			pParam->m_lUnitsPerPallet	= lUnitsPerPallet;
			pParam->m_lPalletMin		= lPalletMin;
			pParam->m_lPalletCount		= lPalletCount;

			m_countLast = * pParam;

			m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
			ASSERT (pHead);
			
			CString strEvent;
			strEvent.Format (_T ("CProdLine::SetCounts [%lu]"), pHead->m_nThreadID);
			hEvent [nHeads] = CreateEvent (NULL, true, false, strEvent);

			#ifdef _DEBUG
			CHeadInfo info = CHead::GetInfo (pHead);
			TRACEF (info.GetUID () + _T (": TM_CHANGE_COUNTS: ") + ToString (lCount));
			#endif

			while (!::PostThreadMessage (pHead->m_nThreadID, TM_CHANGE_COUNTS, (WPARAM)pParam, (LPARAM)hEvent [nHeads])) 
				::Sleep (0);
		}

		DWORD dwWait = ::WaitForMultipleObjects (nHeads, hEvent, TRUE, 3000);
		
		if (dwWait == WAIT_TIMEOUT) TRACEF ("WAIT_TIMEOUT");
		else if ((dwWait >= WAIT_ABANDONED_0) && (dwWait <= (WAIT_ABANDONED_0 + nHeads - 1))) TRACEF ("WAIT_ABANDONED_0");

		for (int i = 0; i < nHeads; i++)
			::CloseHandle (hEvent [i]);
	}

	TRACEF (_T ("CProdLine::SetCounts: ") + GetName () + _T (": ") + FoxjetDatabase::ToString (lCount));

	if (bResult) {
		GlobalFuncs::WriteProfileInt (GetName (), GlobalVars::strCount, lCount);
		DownloadImages ();
	}

	return bResult;
}

void CProdLine::GetCounts (CHead * pFind, __int64 & lFind, __int64 & lMinCount, __int64 & lMaxCount, __int64 & lMasterCount)
{
	int nIndex = 0, nCount = 0;

	lFind = lMinCount = lMaxCount = lMasterCount = 0;

	for (POSITION pos = m_listHeads.GetStartPosition(); pos; nIndex++) {
		CHead * pHead = NULL;
		DWORD dwKey;

		m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
		ASSERT (pHead);
		CHeadInfo info = CHead::GetInfo (pHead);

		__int64 lCount = info.m_Status.m_lCount;

		if (pFind == pHead)
			lFind = lCount;

		if (!info.m_bDisabled) {
			if (info.IsMaster ())
				lMasterCount = lCount;

			if (nIndex == 0)
				lMinCount = lCount;

			lMinCount = min (lMinCount, lCount);
			lMaxCount = max (lMaxCount, lCount);
			nCount++;
		}
	}
	
	if (nCount == 1) 
		lFind = lMinCount = lMasterCount = lMaxCount; // TODO: rem: this is a hack
}

__int64 CProdLine::GetCountUntil () const
{
	for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {
		CHead * pHead = NULL;
		DWORD dwKey;

		m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
		ASSERT (pHead);
		CHeadInfo info = CHead::GetInfo (pHead);

		if (info.IsMaster ()) {
			__int64 n = 0;

			SEND_THREAD_MESSAGE (info, TM_GET_COUNT_UNTIL, &n);

			return n;
		}
	}

	return 0;
}

bool CProdLine::SetCountUntil (__int64 lCountUntil)
{
	bool bResult = true;

	LOCK (m_HeadListSentinel);
	{
		__int64 lTmp, lMinCount = 0, lMaxCount = 0, lMasterCount = 0;
		__int64 lOldCountUntil = GetCountUntil ();

		GetCounts (NULL, lTmp, lMinCount, lMaxCount, lMasterCount);

		for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {
			CHead * pHead = NULL;
			DWORD dwKey;

			m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
			ASSERT (pHead);

			SEND_THREAD_MESSAGE (CHead::GetInfo (pHead), TM_SET_COUNT_UNTIL, &lCountUntil);
		}

		if ((/* (lMasterCount == lOldCountUntil) && */ (lMasterCount < lCountUntil) || (lCountUntil == -1)) && (GetTaskState () == IDLE)) 
			ResumeTask ();
	}

	return bResult;
}

void CProdLine::BeginThread ()
{
	CString str;
	CProgressDlg dlg;

	DWORD dw = 0;
	ASSERT (m_hThread == NULL);

	str.Format (LoadString (IDS_STARTINGAMS), GetName ());
	UPDATE_STATUS (dlg, 0, str);

	{ // sw0883
		SETTINGSSTRUCT s;
		bool bContinuousPC = false;

		if (GetSettingsRecord (theApp.m_Database, 0, _T ("sw0883"), s)) {
			CStringArray v;

			FoxjetDatabase::FromString (s.m_strData, v);

			if (v.GetSize () >= 1) bContinuousPC = _ttoi (v [0]) ? true : false;
		}

		theApp.SetContinuousCount (bContinuousPC);
	}

	ASSERT (m_hExit == NULL);

	if (!m_hExit) {
		static int nCall = 0;

		str.Format (_T ("CProdLine::ThreadFunc %d"), nCall++);
		TRACEF (str);
		m_hExit = ::CreateEvent (NULL, true, false, str);
	}
		
	m_hThread = ::CreateThread ((LPSECURITY_ATTRIBUTES)NULL, 0, (LPTHREAD_START_ROUTINE)ThreadFunc, (LPVOID)this, 0, &dw);
}

void CProdLine::EndThread ()
{
	CString str;
	CProgressDlg dlg;

	str.Format (LoadString (IDS_SHUTTINGDOWNAMS), GetName ());
	UPDATE_STATUS (dlg, 0, str);

	if (m_hThread) {
		ASSERT (m_hExit);
		::SetEvent (m_hExit);
		DWORD dw = ::WaitForSingleObject (m_hThread, 2000);
		ASSERT (dw == WAIT_OBJECT_0);
		m_hThread = NULL;
		::CloseHandle (m_hExit);
		m_hExit = NULL;

		if (dw != WAIT_OBJECT_0)
			MsgBox (_T ("Failed to end: CProdLine::ThreadFunc"));
	}
}

void CProdLine::ShutDown()
{
	HANDLE hEventList [255] = { NULL };
	int nEventCnt = 0;
	CProgressDlg Dialog (::AfxGetMainWnd ());
	CString sMsg;

	while (POSITION pos = m_listHeads.GetStartPosition()) {
		DWORD dwThreadID = 0;
		CHead * pHead = NULL;
		CString strEvent;
		
		m_listHeads.GetNextAssoc(pos, dwThreadID, (void *&)pHead);
		m_listHeads.RemoveKey (dwThreadID);

		if (pHead) {
			CHeadInfo info = GetHeadInfo (pHead);
			
			strEvent.Format (_T ("TM_KILLTHREAD %lu"), info.GetThreadID ());
			TRACEF (strEvent);
			HANDLE h = hEventList[nEventCnt++] = ::CreateEvent (NULL, true, false, strEvent);

			sMsg.Format (LoadString (IDS_SHUTTINGDOWN), info.GetUID());
			UPDATE_STATUS (Dialog, nEventCnt + 1, sMsg );

			while (!::PostThreadMessage (pHead->m_nThreadID, TM_KILLTHREAD, (WPARAM)h, 0))
				::Sleep (0);
		}
	}

	if (::WaitForMultipleObjects (nEventCnt, hEventList, TRUE, nEventCnt * 5000) == WAIT_TIMEOUT)
		MsgBox (LoadString (IDS_THREADSFAILEDTOSTOP));

	for (int i = 0; i < ARRAYSIZE (hEventList); i++) 
		if (hEventList [i])
			::CloseHandle (hEventList [i]);

	if (m_pSw0848Thread) {
		while (!::PostThreadMessage (m_pSw0848Thread->m_nThreadID, TM_KILLTHREAD, 0, 0))
			::Sleep (0);

		DWORD dw = ::WaitForSingleObject (m_pSw0848Thread->m_hThread, 2000);
		ASSERT (dw == WAIT_OBJECT_0);
		m_pSw0848Thread = NULL;
	}

#if defined( __NICELABEL__ ) && defined ( __WINPRINTER__ )
	if (FoxjetElements::CLabelElement::IsLabelEditorPresent ()) {
		sMsg.Format (LoadString (IDS_SHUTTINGDOWNNICELABEL), GetName ());
		UPDATE_STATUS (Dialog, 0, sMsg );
		m_appNiceLabel.ReleaseDispatch ();
	}
#endif 

	EndThread ();

	if ( m_AuxBox.IsOpen() )
	{
		m_AuxBox.SetStrobe (1, 1, STROBE_OFF);
		m_AuxBox.SetStrobe (1, 2, STROBE_OFF);
		m_AuxBox.SetDigitalState (false, false); // SW0832
		Sleep ( 125 ); // Give IO Box time to issues last commands.
		m_AuxBox.Close();
	}

	if (m_pSerialData) {
		delete m_pSerialData;
		m_pSerialData = NULL;
	}
}

void CProdLine::UpdateSerialData(LPCTSTR pData, int nLen)
{
	if (nLen > COM_BUF_SIZE)
		nLen = COM_BUF_SIZE;

	ASSERT (m_pSerialData);
	ASSERT (m_pSerialData->Data);

	memset (m_pSerialData->Data, 0, COM_BUF_SIZE);

	_tcsncpy (m_pSerialData->Data, pData, nLen);
	m_pSerialData->m_Len = nLen;

	if (FoxjetUtils::CStartupDlg::IsSerialDataEnabled (theApp.m_Database)) {
		const CString strFile = GetHomeDir () + _T ("\\Serial Data.txt");
		//CString strKey = ListGlobals::defElements.m_strRegSection;
		CString str = FoxjetDatabase::Format (pData, nLen);
		TRACEF (str);
		//FoxjetDatabase::WriteProfileString (strKey, _T ("Serial Data"), str);
		FoxjetFile::WriteDirect (strFile, str);
	}
//	TRACEF ("pData:CDatabaseStartDlg CDatabaseStartDlg" + FoxjetDatabase::Format (pData, nLen));
//	TRACEF ("m_pSerialData: " + FoxjetDatabase::Format (m_pSerialData->Data, nLen));

	UpdateImages ();
}

tagSerialData * CProdLine::GetSerialData()
{
	return m_pSerialData;
}

int CProdLine::GetScannerPort()
{
	return m_nScannerPort;
}

void CProdLine::ProcessVerifierData( LPCTSTR lpszData )
{
	bool bResult = false;
	CString sBarcode = lpszData;

	m_ScanRec.m_lTotalScans++;

	if ( sBarcode.Find (m_LineRec.m_strNoRead) == -1) {
		m_ScanRec.m_lGoodScans++;
		m_nConsecutiveNoReads = 0;
		SetScannerError (false);
	}
	else {
		m_nConsecutiveNoReads++;
		
		if (m_nConsecutiveNoReads >= m_LineRec.m_nMaxConsecutiveNoReads) {
			SetScannerError (true);

//#if __CUSTOM__ == __SW0820__
			if (m_scanner.m_bIdle) {
//#endif
				IdleTask ();
				m_nConsecutiveNoReads = 0;

//#if __CUSTOM__ == __SW0820__
			}
//#endif

		}
	}

	if (m_ScanRec.m_strBarcode.GetLength () == 0 && sBarcode.GetLength ())
		m_ScanRec.m_strBarcode = sBarcode;

	m_ScanRec.m_dtTime = COleDateTime::GetCurrentTime();
	m_ScanRec.m_strBarcode.Remove ('\r');
	m_ScanRec.m_strBarcode.Remove ('\n');

	if (theApp.IsScanReportLoggingEnabled ()) {
		if ( m_ScanRec.m_lID == -1 )
			bResult = FoxjetDatabase::AddScanRecord ( theApp.m_Database, m_ScanRec);
		else
			bResult = FoxjetDatabase::UpdateScanRecord ( theApp.m_Database, m_ScanRec);
	}
}

int CProdLine::ProcessSerialData (const CSerialPacket *pPkt, CControlView * pView)
{
	using namespace Foxjet3d;

	int nResult = 0; 
	bool bProcess = true;

	if (pPkt && pPkt->m_eDeviceType == HANDSCANNER && theApp.Is0835 ()) { 
		LOCK (m_cs0835);
		CString str (' ', pPkt->m_nDataLen);

		for (UINT i = 0; i < pPkt->m_nDataLen; i++)
			str.SetAt (i, (TCHAR)pPkt->m_pData [i]);

		{ CString strTmp; strTmp.Format (_T ("%s [%d]"), str, str.GetLength ()); TRACEF (strTmp); }

		if (!m_pdlg0835) {
			m_pdlg0835 = new CSw0835Dlg (pView, this);
			m_pdlg0835->Create (IDD_BAXTER, pView);
			m_pdlg0835->ShowWindow (SW_SHOW);
		}

		return m_pdlg0835->ProcessSerialData (str);
	}

//#ifdef __IPPRINTER__
//	bProcess = m_LineRec.m_bAuxBoxEnabled ? true : false;
//#endif // __IPPRINTER__
	
	m_strSerialResult.Empty ();

	if (bProcess) {
		switch ( pPkt->m_eDeviceType )
		{
			case SW0858_DBSTART:
			case HANDSCANNER:		// Hand Scanner - Task Start
				{
					ASSERT (pView);
					CControlDoc * pDoc = pView->GetDocument ();
					ASSERT (pDoc);
					CMapStringToString vSet;
					CMapStringToString * pvSet = NULL;
					Foxjet3d::UserElementDlg::CUserElementDlg dlgUser (-1, pView);
			
					const CString strOldLine = pDoc->GetSelectedLine ()->GetName ();
					CStringArray vLines;

					if (m_bRequireLogin && theApp.m_strUsername.IsEmpty ())
						return TASK_START_LOGIN_REQUIRED;
					
					if (theApp.GetCoupledMode ()) 
						pDoc->GetProductLines (vLines);
					else {

						if (pPkt->m_src == SERIALSRC_HUB) 
							vLines.Add (GetName ()); // no prompt if from hub
						else {
							CStringArray vTmp;

							if (pDoc->GetProductLines (vTmp) == 1) 
								pDoc->GetProductLines (vLines);
							else {
								if (pPkt->IsOverrideLine ()) 
									vLines.Add (pPkt->m_sLineName);
								else {
									LinePromptDlg::CLinePromptDlg dlg (theApp.m_Database, pView);

									dlg.m_strTask = a2w (pPkt->m_pData);

									if (dlg.DoModal () == IDOK) {
										TRACEF (dlg.m_line.m_strName);
										(const_cast <CSerialPacket *> (pPkt))->m_sLineName = dlg.m_line.m_strName;
										vLines.Add (dlg.m_line.m_strName);
									}
								}
							}
						}
					}

					CString strTask, strKeyValue;

					for (int i = 0; i < vLines.GetSize (); i++) {
						CProdLine * pLine = pDoc->GetProductionLine (vLines [i]);
						ASSERT (pLine);

						CString strNewLine = vLines [i];

						{
							CString strTemp;

							strTemp.Format (_T ("%s"), a2w (pPkt->m_pData));
							strTemp.Replace (_T ("\r"), _T (""));
							TRACEF (strTemp);

							strTask = pLine->GetSerialData (strTemp);

							if (pPkt->m_eDeviceType == SW0858_DBSTART) {
								FoxjetUtils::CDatabaseStartDlg dlg (NULL);

								pvSet = &vSet;
								dlg.Load (theApp.m_Database);
								CControlApp::LoadDbStartParams ();

								#ifdef __WINPRINTER__
								try {
									using namespace FoxjetElements;

									COdbcCache db = COdbcDatabase::Find (dlg.m_strDSN);
									CString strSQL = COdbcDatabase::CreateSQL (&db.GetDB (), dlg.m_strDSN, dlg.m_strTable, 
										dlg.m_strTaskField, dlg.m_strKeyField, strTask, dlg.m_nSQLType);
									COdbcCache rstCache = COdbcDatabase::Find (db.GetDB (), dlg.m_strDSN, strSQL);
									COdbcRecordset & rst = rstCache.GetRst ();

									strKeyValue = rst.GetFieldValue (dlg.m_strKeyField);
									strTask = rst.GetFieldValue (dlg.m_strTaskField);

									TRACEF (strKeyValue);
									TRACEF (strTask);
								}
								catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
								catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
								#endif //__WINPRINTER__
							}
						}

						bool bSameTask = pLine->GetCurrentTask ().CompareNoCase (strTask) == 0;
						bool bResetCounts = true;
						__int64 lTmp, lMinCount = 0, lMaxCount = 0, lMasterCount = 0;

						pLine->GetCounts (NULL, lTmp, lMinCount, lMaxCount, lMasterCount);

						TRACEF ("StartTask: " + pLine->GetName () + ", " + strTask + ": " + FoxjetDatabase::ToString (lMasterCount));
	
						if (strKeyValue.GetLength ())
							nResult = pLine->StartTask (strTask, true, false, false, pView, 0, 0, false, strKeyValue, pvSet); 
						else
							nResult = pLine->StartTask (strTask, true, false, false, pView, 0, 0, false, _T (""), pvSet);

						pLine->GetUserElements (dlgUser.m_vElements, true);

						if (nResult != TASK_START_SUCCESS)
							pLine->StopTask (false, pView);

						if (bSameTask && !GetSerialResetCounts ())
							bResetCounts = false;
						
						if (nResult == TASK_START_SUCCESS) {
							unsigned __int64 lCount = theApp.IsContinuousCount () ? lMasterCount : 0;

							pLine->SetCounts (lCount);

							if (pPkt->m_eDeviceType == HANDSCANNER) {
								SETTINGSSTRUCT s;
								FoxjetCommon::StdCommDlg::CCommItem comm;
			
								if (GetSettingsRecord (ListGlobals::GetDB (), 0 , pPkt->m_strPort, s)) {
									if (comm.FromString (s.m_strData)) {
										if (comm.m_bPrintOnce) {
											SetCountUntil (lCount + 1);
										}
									}
								}
							}

							if (strTask.CompareNoCase (GlobalVars::strTestPattern) != 0) {
								if (theApp.IsPrintReportLoggingEnabled ()) {
									auto report = GenerateReportRecord();

									if (bResetCounts) 
										report.m_strCounts.Format (_T ("%lu"), 0);

									if (theApp.IsPrintReportLoggingEnabled ()) 
										AddPrinterReportRecord (FoxjetDatabase::REPORT_TASK_START_REMOTE, report);
								}
							}

							theApp.PostViewRefresh (3000);
						}

						for (POSITION pos = pLine->m_listHeads.GetStartPosition (); pos; ) {
							CHead * pHead = NULL;
							DWORD dwKey;

							pLine->m_listHeads.GetNextAssoc (pos, dwKey, (void *&)pHead);

							ASSERT (pHead);
							CHeadInfo info = CHead::GetInfo (pHead);

							if (info.IsMaster ()) {
								while (!PostMessage (GetControlWnd (), WM_HEAD_SERIAL_DATA, 0, NULL)) 
									::Sleep (0);
							
								break;
							}
						}

						switch ( nResult )
						{
							case TASK_START_SUCCESS:
								m_strSerialResult.Format (LoadString (IDS_SERIALSUCCESS), nResult, m_LineRec.m_strName, strTask);

								if (i == 0 && strOldLine.CompareNoCase (strNewLine) != 0) {
									ASSERT (strNewLine.GetLength ());
									pDoc->SetSelectedLine (strNewLine);
									pDoc->UpdateAllViews (NULL, 0, NULL);
									pDoc->UpdateAllViews (NULL, UPDATE_LINE_CHANGE, NULL);
								}

								break;
							case TASK_START_NOT_FOUND:
								m_strSerialResult.Format (LoadString (IDS_SERIALTASKNOTFOUND), nResult, m_LineRec.m_strName, strTask);
								break;
							case TASK_START_NO_ACTIVE_HEADS:
								m_strSerialResult.Format (LoadString (IDS_SERIALNOACTIVEHEADS), nResult, m_LineRec.m_strName, strTask);
								break;
							case TASK_START_NO_MASTER_HEAD:
								m_strSerialResult.Format (LoadString (IDS_SERIALNOMASTER), nResult, m_LineRec.m_strName, strTask);
								break;
							case TASK_START_FAILED:
								m_strSerialResult.Format (LoadString (IDS_SERIALTIMEOUT), nResult, m_LineRec.m_strName, strTask);
								break;
							case TASK_START_CANCELLED:
								break;
						}
					}

					if (pPkt->m_eDeviceType == SW0858_DBSTART) {
						CMapStringToString vUser;
						FoxjetUtils::CDatabaseStartDlg dlgDB (NULL);

						dlgDB.Load (theApp.m_Database);
								
						TRACEF (dlgDB.m_strDSN);
						TRACEF (dlgDB.m_strTable);
						TRACEF (dlgDB.m_strKeyField);
						TRACEF ("");

						for (int i = 0; i < dlgUser.m_vElements.GetSize (); i++) {
							CString strPrompt = dlgUser.m_vElements [i].m_strPrompt;
							CString strData = dlgUser.m_vElements [i].m_strData;

							TRACEF (strPrompt + _T (" --> ") + strData);

							#ifdef __WINPRINTER__

							using namespace FoxjetElements;

							if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, dlgUser.m_vElements [i].m_pElement)) {
								TRACEF (p->GetDSN ());
								TRACEF (p->GetTable ());
								TRACEF (p->GetKeyField ());
								TRACEF ("");

								if (!p->GetDSN ().CompareNoCase (dlgDB.m_strDSN) &&
									!p->GetTable ().CompareNoCase (dlgDB.m_strTable) &&
									!p->GetKeyField ().CompareNoCase (dlgDB.m_strKeyField))
								{
									dlgUser.m_vElements.RemoveAt (i);
								}
							}
							else if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, dlgUser.m_vElements [i].m_pElement)) {
								TRACEF (p->GetDatabaseDSN ());
								TRACEF (p->GetDatabaseTable ());
								TRACEF (p->GetDatabaseKeyField ());
								TRACEF ("");

								if (!p->GetDatabaseDSN ().CompareNoCase (dlgDB.m_strDSN) &&
									!p->GetDatabaseTable ().CompareNoCase (dlgDB.m_strTable) &&
									!p->GetDatabaseKeyField ().CompareNoCase (dlgDB.m_strKeyField))
								{
									dlgUser.m_vElements.RemoveAt (i);
								}
							}
							else if (CExpDateElement * p = DYNAMIC_DOWNCAST (CExpDateElement, dlgUser.m_vElements [i].m_pElement)) { // sw0924
								if (p->GetType () == CExpDateElement::TYPE_DATABASE) {
									TRACEF (p->GetDSN ());
									TRACEF (p->GetTable ());
									TRACEF (p->GetKeyField ());
									TRACEF ("");

									if (!p->GetDSN ().CompareNoCase (dlgDB.m_strDSN) &&
										!p->GetTable ().CompareNoCase (dlgDB.m_strTable) &&
										!p->GetKeyField ().CompareNoCase (dlgDB.m_strKeyField))
									{
										dlgUser.m_vElements.RemoveAt (i);
									}
								}
							}
							#endif //__WINPRINTER__
						}

						if (FoxjetUtils::CStartupDlg::IsUserDataEnabled (theApp.m_Database)) {
							for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
								for (int i = 0; i < dlgUser.m_vElements.GetSize (); i++) {
									CString strPrompt = dlgUser.m_vElements [i].m_strPrompt;
									const CString strFile = GetHomeDir () + _T ("\\") + GlobalVars::strUserPrompted + _T ("\\") + vLines [nLine] + _T ("\\") + strPrompt + _T (".txt");
									CString strData = FoxjetFile::ReadDirect (strFile, dlgUser.m_vElements [i].m_strData);
									dlgUser.m_vElements [i].m_strData = strData;
									TRACEF (strPrompt + _T (" --> ") + dlgUser.m_vElements [i].m_strData);
								}
							}
						}

						if (dlgUser.m_vElements.GetSize ()) {
							if (dlgUser.DoModal () == IDCANCEL) {
								//m_strSerialResult.Format (LoadString (IDS_CANCELLED), nResult, m_LineRec.m_strName, strTask);
							}
							else {
								for (int i = 0; i < dlgUser.m_vElements.GetSize (); i++) {
									CString strPrompt = dlgUser.m_vElements [i].m_strPrompt;
									CString strData = dlgUser.m_vElements [i].m_strData;

									TRACEF (strPrompt + _T (" --> ") + strData);
									vUser.SetAt (strPrompt, strData);

									for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
										const CString strFile = GetHomeDir () + _T ("\\") + GlobalVars::strUserPrompted + _T ("\\") + vLines [nLine] + _T ("\\") + strPrompt + _T (".txt");
										FoxjetFile::WriteDirect (strFile, dlgUser.m_vElements [i].m_strData);
									}
								}
							}

							if (vUser.GetCount ()) {
								for (int i = 0; i < vLines.GetSize (); i++) {
									if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
										CPrintHeadList heads;

										pLine->ChangeUserElementData (false, strKeyValue, &vUser, 0);
										pLine->GetActiveHeads (heads);

										for (POSITION pos = heads.GetStartPosition(); pos; ) {
											ULONG lKey;
											CHead * pHead = NULL;

											heads.GetNextAssoc (pos, lKey, (void *&)pHead);
											ASSERT (pHead);

											#ifdef __WINPRINTER__
											while (!::PostThreadMessage (pHead->m_nThreadID, TM_BUILD_IMAGE, (WPARAM)MAKEIMGPARAMS (IMAGING_REFRESH, 0), 0))
												::Sleep (0);
											#endif //__WINPRINTER__
										}
									}
								}
							}

							for (int i = 0; i < vLines.GetSize (); i++) {
								if (CProdLine * pProdLine = pDoc->GetProductionLine (vLines [i])) {
									pProdLine->ResumeTask (false);
								}
							}
						}
					}
				}
				break;
			case FIXEDSCANNER:	// Fixed Mount Scanner - Barcode Scanner - Log Scan results
				{
					CString str = a2w (pPkt->m_pData);

					ProcessVerifierData (str);
					m_strSerialResult = str;
				}
				break;
			case REMOTEDEVICE:	// Remote Device such as PC or Weight Scale. Remote Protocol or variable data.
				{
					bool bSw0849 = CSerialPacket::IsSw0849 (pPkt->m_pData, pPkt->m_nDataLen);

					if (bSw0849)
						nResult = TASK_START_SUCCESS;

					if (bSw0849 || !GetSerialPending ()) {
						CStringArray vLines;

						ASSERT (pView);
						CControlDoc * pDoc = pView->GetDocument ();
						ASSERT (pDoc);

						SetSerialPending (true);
						OnSw0849 (pPkt, pView);
						UpdateSerialData ( pPkt->m_pData, pPkt->m_nDataLen );
						m_strSerialResult = a2w (pPkt->m_pData);
						pDoc->GetProductLines (vLines);

						if (CSerialPacket::IsSw0849Ignore (pPkt->m_pData, pPkt->m_nDataLen)) // sw0849
							nResult = TASK_START_SUCCESS;

						for (int i = 0; i < vLines.GetSize (); i++) {
							CProdLine * pLine = pDoc->GetProductionLine (vLines [i]);
							ASSERT (pLine);

							for (POSITION pos = pLine->m_listHeads.GetStartPosition (); pos; ) {
								CHead * pHead = NULL;
								DWORD dwKey;

								pLine->m_listHeads.GetNextAssoc (pos, dwKey, (void *&)pHead);
								ASSERT (pHead);

								while (!PostThreadMessage (pHead->m_nThreadID, TM_PROCESS_SERIAL_DATA, (WPARAM)new CSerialPacket (* pPkt), 0))
									::Sleep (0);
							}
						}
					}
					else {
						m_strSerialResult = _T ("Ignored: ") + CString (a2w (pPkt->m_pData));
						TRACEF (m_strSerialResult);
					}

				}
				break;
			case HOSTINTERFACE:
				{
					CString str = a2w (pPkt->m_pData);
					m_strSerialResult = CServerThread::ProcessCommand (str, pView);
				}
				break;
		}
	}

	return nResult;
}

void CProdLine::SetSerialPending (bool bPending)
{
	CPrintHeadList list;
	
	GetActiveHeads (list);
	
	for (POSITION pos = list.GetStartPosition(); pos; ) {
		ULONG lKey; 
		CHead * pHead = NULL;

		list.GetNextAssoc (pos, lKey, (void *&)pHead);

		if (pHead) 
			while (!::PostThreadMessage (pHead->m_nThreadID, TM_SETSERIALPENDING, bPending, 0))
				::Sleep (0);
	}
}

bool CProdLine::GetSerialPending ()
{
	bool bPending = false;
	CPrintHeadList list;
	
	GetActiveHeads (list);
	
	for (POSITION pos = list.GetStartPosition(); pos; ) {
		ULONG lKey; 
		CHead * pHead = NULL;

		list.GetNextAssoc (pos, lKey, (void *&)pHead);

		if (pHead) {
			CHeadInfo info = CHead::GetInfo (pHead);
			bool bPending = false;

			SEND_THREAD_MESSAGE (info, TM_GET_SERIALPENDING, &bPending);

			if (bPending) 
				return true;
		}
	}

	return false;
}

void CProdLine::ResetVerifierData()
{
	m_nConsecutiveNoReads = 0;
	SetScannerError (false);

	m_ScanRec.m_lTotalScans = 0;
	m_ScanRec.m_lGoodScans = 0;
	m_ScanRec.m_lID = -1;
	m_ScanRec.m_strTaskname = m_strTaskname;
	m_ScanRec.m_strBarcode.Empty ();
}

LONG CProdLine::GetAuxBoxHandle()
{
	return m_lAuxBoxID;
}

int CProdLine::OnAuxSerialData(int PortNum, CControlView * pView)
{
	CString sBaud, sParity, sDatabits, sStopbits;
	eSerialDevice DeviceType;
	CString strPort;

	strPort.Format (_T ("AUX%d"), PortNum);

//	m_bAuxBoxConnected = true;

	switch ( PortNum )
	{
		case 1:
			ParseSerialParams ( m_LineRec.m_strSerialParams1, sBaud, sDatabits, sStopbits, sParity, DeviceType );
			break;
		case 2:
			ParseSerialParams ( m_LineRec.m_strSerialParams2, sBaud, sDatabits, sStopbits, sParity, DeviceType );
			break;
		case 3:
			ParseSerialParams ( m_LineRec.m_strSerialParams3, sBaud, sDatabits, sStopbits, sParity, DeviceType );
			break;
	}
	short Len = 1024;
	VARIANT v;
	VariantInit ( &v );
	v.vt = VT_UI1|VT_BYREF;
	v.pbVal = new unsigned char [ Len ];
	memset ( v.pbVal, 0x00, Len);
	int result;
	
	m_AuxBox.GetSerialData (PortNum, &v, &Len);

	while ( Len > 0 )
	{
		CSerialPacket * p = new CSerialPacket (strPort);

		TRACEF ((LPCSTR)v.pbVal);
		p->AssignData (SERIALSRC_HUB, DeviceType, m_LineRec.m_strName, a2w (v.pbVal), Len, 0);

		result = pView->OnHeadSerialData (0, (LPARAM)p);

		Len = 1024;
		memset ( v.pbVal, 0x00, Len);
		m_AuxBox.GetSerialData (PortNum, &v, &Len);
	}
	delete [] v.pbVal;
	return result;
}

void CProdLine::ParseSerialParams(CString sParams, CString &sBaud, CString &sDatabits, CString &sStopbits, CString &sParity, eSerialDevice &DeviceType )
{
	int loc = sParams.Find (',', 0 );
	sBaud = sParams.Left ( loc );

	sParams = sParams.Right ( sParams.GetLength() - (loc + 1) );
	loc = sParams.Find (',', 0 );
	sDatabits = sParams.Left ( loc );
	
	sParams = sParams.Right ( sParams.GetLength() - (loc + 1) );
	loc = sParams.Find (',', 0 );
	sParity.Format (_T ("%c"), sParams.Left ( loc ).GetBuffer(1)[0] );

	sParams = sParams.Right ( sParams.GetLength() - (loc + 1) );
	loc = sParams.Find (',', 0 );
	sStopbits = sParams.Left ( loc );

	sParams = sParams.Right ( sParams.GetLength() - (loc + 1) );
	DeviceType = ( eSerialDevice ) _ttoi ( sParams );
}

void CProdLine::OnAuxBoxConnectStateChange (bool bConnected)
{
	CString sBaud, sParity, sDatabits, sStopbits;
	eSerialDevice DeviceType;
//	m_bAuxBoxConnected = bConnected;

	TRACEF ("OnAuxBoxConnectStateChange: " + CString (bConnected ? "true" : "false"));

//	if ( m_bAuxBoxConnected ) {
	if (bConnected) {
		ParseSerialParams ( m_LineRec.m_strSerialParams1, sBaud, sDatabits, sStopbits, sParity, DeviceType );
//		m_AuxBox.SetSerialPortParams ( 1, FALSE, sBaud, sParity, sDatabits, sStopbits );
		m_AuxBox.SetSerialPortParams ( 2, TRUE, sBaud, sParity, sDatabits, sStopbits );
		TRACEF ("aux B: " + sBaud);

		ParseSerialParams ( m_LineRec.m_strSerialParams2, sBaud, sDatabits, sStopbits, sParity, DeviceType );
		m_AuxBox.SetSerialPortParams ( 3, TRUE, sBaud, sParity, sDatabits, sStopbits );
		TRACEF ("aux C: " + sBaud);

		ParseSerialParams ( m_LineRec.m_strSerialParams3, sBaud, sDatabits, sStopbits, sParity, DeviceType );
		m_AuxBox.SetSerialPortParams ( 4, TRUE, sBaud, sParity, sDatabits, sStopbits );
		TRACEF ("aux D: " + sBaud);

		m_AuxBox.SetStrobe (1, 1, STROBE_OFF);
		m_AuxBox.SetStrobe (1, 2, STROBE_ON);
		m_bStrobeActive = false;
		//m_AuxBox.SetDigitalState (false, true); // SW0832
	}
}

void CProdLine::OnAuxBoxPS2Data()
{
	CString strMsg;
	short n = 256;
	VARIANT v;
	VariantInit ( &v );
	v.vt = VT_UI1|VT_BYREF;
	v.pbVal = new unsigned char [ n ];
	memset ( v.pbVal, 0x00, n);
	m_AuxBox.GetPS2Data (&v, &n);	
//	strMsg.Format ( "PS2 Data\nData[%s]", v.pbVal);
	CString strTask;
	strTask.Format (_T ("%s"), v.pbVal );
	strTask.Replace (_T ("\r"), _T (""));
	delete [] v.pbVal;

	int result = StartTask ( strTask, true, false );
	switch ( result )
	{
		case TASK_START_SUCCESS:
//			strMsg.Format (LoadString (IDS_SERIALSUCCESS), result, sLine, strTask);
			break;
		case TASK_START_NOT_FOUND:
			strMsg.Format (LoadString (IDS_AUXTASKNOTFOUND), m_LineRec.m_strName, strTask);
			break;
		case TASK_START_NO_ACTIVE_HEADS:
			strMsg.Format (LoadString (IDS_AUXNOACTIVEHEADS), m_LineRec.m_strName, strTask);
			break;
		case TASK_START_NO_MASTER_HEAD:
			strMsg.Format (LoadString (IDS_AUXNOMASTER), m_LineRec.m_strName, strTask);
			break;
		case TASK_START_FAILED:
			strMsg.Format (LoadString (IDS_AUXTIMEOUT), m_LineRec.m_strName, strTask);
			break;
		case TASK_START_CANCELLED:
			break;
	}
	if ( !strMsg.IsEmpty() )
		MsgBox ( strMsg );

}

void CProdLine::UpdateHeadErrors (DWORD dwThreadID, eHeadStatus s)
{

	{ // remove any invalid references to CHead::m_hThread (NET issue 0000076)
		CPrintHeadList list;

		GetActiveHeads (list);

		for (int i = 0; i < m_mapHeadErrors.GetCount (); i++) { 
			for (POSITION posError = m_mapHeadErrors.GetStartPosition (); posError; ) {
				eHeadStatus status;
				DWORD dwFind = 0;
				bool bFound = false;

				m_mapHeadErrors.GetNextAssoc (posError, dwFind, status);

				if (dwFind) {
					for (POSITION posHead = list.GetStartPosition(); posHead; ) {
						DWORD dwKey;
						CHead * pHead = NULL;

						list.GetNextAssoc (posHead, dwKey, (void *&)pHead);

						if (pHead->m_nThreadID == dwFind) {
							bFound = true;
							break;
						}
					}

					if (!bFound) 
						m_mapHeadErrors.RemoveKey (dwFind);
				}
			}
		}
	}

	if (CHead * p = GetHeadByThreadID (dwThreadID)) {
		CHeadInfo info = CHead::GetInfo (p);

		if (s == OK || !info.m_Config.m_HeadSettings.m_bEnabled) {
			if (m_mapHeadErrors.Lookup (dwThreadID, s))
				m_mapHeadErrors.RemoveKey (dwThreadID);
		}
		else 
			m_mapHeadErrors.SetAt (dwThreadID, s);
	}
}

bool CProdLine::IsAuxBoxConnected()
{
	return m_AuxBox.IsOpen ();
//	return m_bAuxBoxConnected;
}

DWORD CProdLine::GetErrorState () const 
{ 
	DWORD dw = 0;

	for (POSITION pos = m_mapHeadErrors.GetStartPosition (); pos; ) {
		eHeadStatus s;
		DWORD dwHead = 0;

		m_mapHeadErrors.GetNextAssoc (pos, dwHead, s); 
		dw |= s;
	}

	return dw;
}

bool CProdLine::GetScannerError () const
{
//#if __CUSTOM__ == __SW0820__
	return m_bScannerError;
//#else
//	return false;
//#endif
}

void CProdLine::ClearAllScannerErrors ()
{
//#if __CUSTOM__ == __SW0820__
	
	m_nConsecutiveNoReads = 0;
	SetScannerError (false);
//#endif
}

void CProdLine::SetScannerError (bool bError)
{
//#if __CUSTOM__ == __SW0820__
	STROBESTATE state = bError ? STROBESTATE_FLASH : STROBESTATE_OFF;

	m_bScannerError = bError;

	if (m_bScannerError)
		theApp.SetStrobe ();
//#endif
}

void CProdLine::OnAuxBoxDigitalInputs()
{
	if (m_AuxBox.IsOpen ()) {
		int nLowInk = 0, nOther = 0;

		#ifdef _DEBUG
		{
			VARIANT v;

			::VariantInit (&v);
			
			v.vt	= VT_UI1;
			v.bVal	= 0;

			m_AuxBox.GetDigitalInputs (&v);
			TRACEF (TOBINSTR (v.bVal).Right (8));
		}
		#endif

		// must consider other head errors before calling SetWaiting
		GetLineState (nLowInk, nOther);
		bool bWait = nOther ? true : false;
		SetWaiting (bWait);
	}
}

void CProdLine::GetLineState (int & nLowInk, int & nOther)
{
	for (POSITION pos = m_mapHeadErrors.GetStartPosition (); pos; ) {
		eHeadStatus s;
		DWORD dwHead = 0;

		m_mapHeadErrors.GetNextAssoc (pos, dwHead, s); 

		nLowInk += ((s == LOW_INK || s == IV_DISCONNECTED) ? 1 : 0);
		nOther	+= (s != LOW_INK ? 1 : 0);
	}

	if (m_AuxBox.IsOpen ()) // SW0832
		nOther	+= (m_AuxBox.HasDigitalInputError () ? 1 : 0);
}

int CProdLine::GetDisconnectedCount ()
{
	int nResult = 0;
	CPrintHeadList heads;
	
#ifdef __IPPRINTER__
	for (POSITION pos = heads.GetStartPosition(); pos != NULL; ) {
		CHead * p = NULL;
		ULONG lKey;

		heads.GetNextAssoc (pos, lKey, (void *&)p);

		if (CRemoteHead * pHead = DYNAMIC_DOWNCAST (CRemoteHead, p)) 
			nResult += pHead->IsConnected ();
	}
#endif //__IPPRINTER__

	return nResult;
}

bool CProdLine::IsErrorPresent (HEADERRORTYPE e)
{
	CPrintHeadList list;
	DWORD dw = 0;

	GetActiveHeads (list);

	if (list.GetCount () == 0) 
		dw |= HEADERROR_STOPPED;

	for (POSITION pos = list.GetStartPosition (); pos != NULL;) {
		CHead * pHead = NULL;
		DWORD dwKey = 0;

		list.GetNextAssoc (pos, dwKey, (void *&)pHead);

		if (pHead) {
			CHeadInfo info = CHead::GetInfo (pHead);
			DWORD dwState = 0;

			SEND_THREAD_MESSAGE (info, TM_GET_ERROR_STATE, &dwState);
			dw |= dwState;
		}
	}

	if (e == HEADERROR_READY)
		return (dw == 0) ? true : false;
	else
		return (dw & e) ? true : false;
}

CString CProdLine::FormatHeadErrors (const CHeadInfo & info, DWORD dw)
{
	CString str = _T ("");

	if (dw) {
		struct 
		{
			DWORD m_dw;
			LPCTSTR m_lpsz;
		} 
		static const map [] = 
		{
			{ HEADERROR_LOWTEMP,		_T ("LowTemp"),		},
			{ HEADERROR_HIGHVOLTAGE,	_T ("HV"),			},
			{ HEADERROR_LOWINK,			_T ("LowInk"),		},
			{ HEADERROR_OUTOFINK,		_T ("OutOfInk"),	},
			//{ IVERROR_BROKENLINE,		_T ("BL"),			},
			//{ IVERROR_DISCONNECTED,	_T ("DS"),			},
			//{ HPERROR_COMMPORTFAILURE,_T ("CF"),			},
			//{ HEADERROR_DISCONNECTED,	_T ("DS"),			},
			//{ HUBERROR_IO,			_T ("HB"),			},
			{ HEADERROR_APSCYCLE,		_T ("APS"),			},
			{ HEADERROR_EP8,			_T ("EP8"),			},
		};
		CStringArray v;

		for (int i = 0; i < ARRAYSIZE (map); i++)
			if (map [i].m_dw & dw)
				v.Add (map [i].m_lpsz);

		str = info.GetUID () + _T (":") + Extract (ToString (v), _T ("{"), _T ("}"));
	}

	return str;
}

void CProdLine::UpdateStrobeState(CControlView * pView)
{	
	CPrintHeadList HeadList;
	int nLowInk = 0, nOther = 0;
	STROBESTATE state = STROBESTATE_OFF;

#ifdef __IPPRINTER__
	{
		UINT n [] = 
		{
			IDC_STROBE_RED,
			IDC_STROBE_GREEN,
		};

		for (int i = 0; i < ARRAYSIZE (n); i++)
			if (CWnd * p = pView->GetDlgItem (n [i]))
				p->ShowWindow (SW_HIDE);
	}
#endif

	GetActiveHeads ( HeadList );
	GetLineState (nLowInk, nOther);

//#if __CUSTOM__ == __SW0820__ && defined( _DEBUG )
//	nLowInk = nOther = 0; // TODO: rem
//#endif

	pView->GetLineState (GetID (), nLowInk, nOther);

//#if __CUSTOM__ == __SW0820__
	nOther += GetScannerError () ? 1 : 0;

/*
	#ifdef _DEBUG
	if (GetScannerError ()) 
		TRACEF ("Scanner error");
	#endif
*/

//#endif

	nOther += GetDisconnectedCount ();

	bool bStrobeOn = nLowInk || nOther;

	{ // sw0878
	
		FoxjetCommon::CRawBitmap * pRed		= &m_bmpGray;
		FoxjetCommon::CRawBitmap * pYellow	= &m_bmpGray;
		FoxjetCommon::CRawBitmap * pGreen	= &m_bmpGray;

		HEADERRORTYPE map [] = 
		{
			HEADERROR_READY,
			HEADERROR_LOWTEMP,
			HEADERROR_HIGHVOLTAGE,
			HEADERROR_LOWINK,
			HEADERROR_OUTOFINK,
			IVERROR_BROKENLINE,
			IVERROR_DISCONNECTED,
			HPERROR_COMMPORTFAILURE,
			HEADERROR_DISCONNECTED,
			HUBERROR_IO,
			HEADERROR_APSCYCLE,
			HEADERROR_EP8,
		};
		int nGreen = 0, nYellow = 0, nRedSolid = 0, nRedFlash = 0;
		
		for (int i = 0; i < m_vStates.GetSize (); i++) {
			FoxjetCommon::CStrobeItem & p = m_vStates [i];
			FoxjetDatabase::HEADERRORTYPE e = p.GetErrorType (p.m_strName);
			
			//TRACEF (p.ToString ());

			if (p.m_green != STROBESTATE_OFF) 
				if (IsErrorPresent (e)) 
					nGreen++;

			if (p.m_yellow != STROBESTATE_OFF) 
				if (IsErrorPresent (e)) 
					nYellow++;

			if (p.m_red != STROBESTATE_OFF) {
				if (IsErrorPresent (e)) {
					nRedSolid += (p.m_red == STROBESTATE_ON)	? 1 : 0;
					nRedFlash += (p.m_red == STROBESTATE_FLASH) ? 1 : 0;
				}
			}
		}

		int nRed			= nRedFlash + nRedSolid;
		STROBESTATE green	= nGreen	? STROBESTATE_ON : STROBESTATE_OFF;
		STROBESTATE yellow	= nYellow	? STROBESTATE_ON : STROBESTATE_OFF;
		STROBESTATE red		= nRed		? STROBESTATE_ON : STROBESTATE_OFF;
		m_nTabStrobeColor	= (nGreen ? GREEN_STROBE : 0) | (nYellow ? YELLOW_STROBE : 0) | (nRed ? RED_STROBE : 0);
		int nDriverColor	= m_nTabStrobeColor;

		pGreen	= nGreen	? &m_bmpGreen	: &m_bmpGray;
		pYellow	= nYellow	? &m_bmpYellow	: &m_bmpGray;
		pRed	= nRed		? &m_bmpRed		: &m_bmpGray;


		if (nRedFlash && (m_nUpdateStrobeState % 2)) {
			pRed = &m_bmpGray;
			red = STROBESTATE_FLASH;
			nDriverColor &= ~RED_STROBE;
		}

		if (CStatic * p = (CStatic *)pView->GetDlgItem (IDC_STROBE_RED)) {
			CBitmap bmp;

			pRed->GetBitmap (bmp);
			p->SetBitmap (bmp);
		}
		if (CStatic * p = (CStatic *)pView->GetDlgItem (IDC_STROBE_YELLOW)) {
			CBitmap bmp;

			pYellow->GetBitmap (bmp);
			p->SetBitmap (bmp);
		}
		if (CStatic * p = (CStatic *)pView->GetDlgItem (IDC_STROBE_GREEN)) {
			CBitmap bmp;

			pGreen->GetBitmap (bmp);
			p->SetBitmap (bmp);
		}

		for (POSITION pos = HeadList.GetStartPosition (); pos; ) {
			CHead * pHead = NULL;
			DWORD dwKey = 0;

			HeadList.GetNextAssoc (pos, dwKey, (void *&)pHead);

			if (pHead) 
				while (!PostThreadMessage (pHead->m_nThreadID, TM_UPDATE_STROBE, nDriverColor, 0))
					::Sleep (0);
		}

		{
			CMap <CHead *, CHead *, DWORD, DWORD> v;
			CPrintHeadList list;
			DWORD dwAll = 0;
			CStringArray vError;

			GetActiveHeads (list);

			for (POSITION pos = list.GetStartPosition (); pos != NULL; ) {
				CHead * pHead = NULL;
				DWORD dwKey = 0;

				list.GetNextAssoc (pos, dwKey, (void *&)pHead);

				if (pHead) {
					CHeadInfo info = CHead::GetInfo (pHead);
					DWORD dwHead = 0;
					DWORD dw = 0;

					SEND_THREAD_MESSAGE (info, TM_GET_ERROR_STATE, &dwHead);

					for (int i = 0; i < m_vStates.GetSize (); i++) {
						FoxjetCommon::CStrobeItem & p = m_vStates [i];
						HEADERRORTYPE e = p.GetErrorType (p.m_strName);

						if (dwHead & (DWORD)e) 
							dw |= (DWORD)e;
					}

					dwAll |= dw;
					v.SetAt (pHead, dw);
					vError.Add (FormatHeadErrors (info, dw));
				}
			}

			if (m_dwLastErrorState != dwAll) {
				m_dwLastErrorState = dwAll;

  				while (!pView->PostMessage (WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, NULL))
					::Sleep (0);

				if (dwAll) {
					__int64 lFind = 0, lMin = 0, lMax = 0, lMaster = 0;
					REPORTSTRUCT s;

					GetCounts (NULL, lFind, lMin, lMax, lMaster);

					s.m_strUsername = theApp.m_strUsername;
					s.m_strTaskName = m_strTaskname;
					s.m_strLine		= GetName ();
					s.m_dtTime		= COleDateTime::GetCurrentTime ();
					s.m_strCounts.Format (_T ("%d"), lMaster);
					
					if (vError.GetSize ())
						s.m_strCounts += _T (" [") + Extract (ToString (vError), _T ("{"), _T ("}")) + _T ("]"); 

					if (s.m_strCounts.GetLength () > 50)
						s.m_strCounts = s.m_strCounts.Left (50);

					TRACEF (s.m_strCounts);

					if (theApp.IsPrintReportLoggingEnabled ()) 
						AddPrinterReportRecord (FoxjetDatabase::REPORT_STROBESTATE, s);
				}

				if (CHead::IsError (dwAll)) {
					if (!m_bIdledOnError) {
						m_dwLastTaskState = GetTaskState ();

						if (m_dwLastTaskState == RUNNING) 
							IdleTask (true, true);

						m_bIdledOnError = true;
					}
				}
				else { 
					if (m_bIdledOnError) {
						m_bIdledOnError = false;
						ResumeTask ();
					}
				}
			}
		}

		m_nDriverStrobeColor = nDriverColor;
	}

	m_nLowInk = nLowInk;
	m_nUpdateStrobeState++;
}

void CProdLine::DoAmsCycle ()
{
	CPrintHeadList heads;
	ULONG lLineID = GetID ();
	REPORTSTRUCT rpt;
	
	rpt.m_dtTime	= COleDateTime::GetCurrentTime ();
	rpt.m_strLine	= GetName ();
	rpt.m_lType		= PRINTER;

	AddReportRecord (theApp.m_Database, REPORT_AMS_CYCLE, rpt, GetPrinterID (theApp.m_Database));
	GetActiveHeads (heads);
	
	for (POSITION pos = heads.GetStartPosition(); pos != NULL; ) {
		CHead * p = NULL;
		ULONG lKey;

		heads.GetNextAssoc (pos, lKey, (void *&)p);

#ifdef __IPPRINTER__
		if (CRemoteHead * pHead = DYNAMIC_DOWNCAST (CRemoteHead, p)) {
			while (!PostThreadMessage (pHead->m_nThreadID, WM_HEAD_AMSCYCLE, lLineID, 0)) 
				Sleep (0);
		}
#endif //__IPPRINTER__
	}

	m_dtAms = COleDateTime::GetCurrentTime ();
}

DWORD CProdLine::HasHeadErrors (CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vDisconnected)
{
	DWORD dwErrors = 0;
	CPrintHeadList list;

	GetActiveHeads (list);

	for (POSITION pos = list.GetStartPosition(); pos; ) {
		DWORD dwKey;
		CHead * pHead = NULL;

		list.GetNextAssoc(pos, dwKey, (void *&)pHead);
		ASSERT (pHead);
		CHeadInfo info = CHead::GetInfo (pHead);
		DWORD dwState = 0;

		#ifdef __WINPRINTER__
		if (FoxjetCommon::IsProductionConfig ()) 
			if (CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pHead)) 
				if (!Foxjet3d::GetTestPHC (info.GetIOAddress ()))
					continue;
		#endif //__WINPRINTER__

		SEND_THREAD_MESSAGE (info, TM_GET_ERROR_STATE, &dwState);
		dwErrors |= dwState; 
	}

	if (m_AuxBox.IsOpen ())  // SW0832
		if (m_AuxBox.HasDigitalInputError ())
			dwErrors |= HUBERROR_IO;

	vDisconnected.RemoveAll ();

	#ifdef __IPPRINTER__
	if (GetDisconnectedCount ())
		dwErrors |= HEADERROR_DISCONNECTED;
	#endif //__IPPRINTER__

	return dwErrors;
}

ULONG CALLBACK CProdLine::ThreadFunc (LPVOID lpData) 
{
	ASSERT (lpData);

	CProdLine & line = * (CProdLine *)lpData;
	CString strLine = line.GetName ();
	HANDLE hEvent [1] = { line.m_hExit };
	const HWND hOwnerWnd = line.m_hOwnerWnd;
	const LINESTRUCT lineRec = line.m_LineRec;

	//CDiagnosticSingleLock::RegisterThreadName (_T ("CProdLine::ThreadFunc:") + strLine, ::GetCurrentThreadId ());

	while (1) {
		const DWORD dwSleep = 250;

		//DWORD dwWait = ::MsgWaitForMultipleObjects (ARRAYSIZE (hEvent), hEvent, false, dwSleep, QS_ALLINPUT);

		//switch (dwWait) {
		//	case WAIT_OBJECT_0:
		//		ResetEvent (hEvent);
		//		TRACEF ("ThreadFunc exiting: " + strLine);
		//		return 0;
		//}
		
		if (::WaitForSingleObject (hEvent [0], 0) == WAIT_OBJECT_0) return 0;

		{
			LOCK (line.m_csThread);
			int nHeadErrors = 0;

#ifdef __IPPRINTER__
			int nMinutes = (int)(lineRec.m_dAMS_Interval * 60.0);

			if (nMinutes > 1) {
				const COleDateTime dtWhen = line.m_dtAms + COleDateTimeSpan (0, 0, nMinutes, 0);
				const COleDateTime dtCurrent = COleDateTime::GetCurrentTime ();

				if (dtWhen <= dtCurrent) {
					while (!::PostMessage (hOwnerWnd, WM_LINE_DO_AMS, lineRec.m_lID, 0))
						::Sleep (0);
					* ((DWORD *)(LPVOID)&dwSleep) = 1000;
				}
			}
#endif //__IPPRINTER__

			if (line.m_listHeads.GetCount ()) {
				CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> vDisconnected;
				DWORD dwResult = 0;

				if (::WaitForSingleObject (hEvent [0], 0) == WAIT_OBJECT_0) return 0;

				ULONG lSend = ::SendMessageTimeout (hOwnerWnd, WM_CHECKHEADERRORS, 
					(WPARAM)lineRec.m_lID, (LPARAM)&vDisconnected, 
					SMTO_BLOCK | SMTO_ABORTIFHUNG, 250, &dwResult);
				bool bErrors = dwResult ? true : false;

				if (lSend) {
					if (!bErrors) {
						if (::WaitForSingleObject (hEvent [0], 0) == WAIT_OBJECT_0) return 0;

						dwResult = 0;
						lSend = ::SendMessageTimeout (hOwnerWnd, WM_LINE_IS_WAITING, 
							(WPARAM)lineRec.m_lID, 0, SMTO_BLOCK | SMTO_ABORTIFHUNG, 250, &dwResult);

						if (dwResult) {
							if (::WaitForSingleObject (hEvent [0], 0) == WAIT_OBJECT_0) return 0;

							while (!::PostMessage (hOwnerWnd, WM_LINE_SET_WAITING, lineRec.m_lID, false))
								::Sleep (0);

							if (::WaitForSingleObject (hEvent [0], 0) == WAIT_OBJECT_0) return 0;

							CControlView::SOCKETERRORSTRUCT * p = new CControlView::SOCKETERRORSTRUCT (lineRec.m_lID, line.m_lTaskID, false);
							while (!::PostMessage (hOwnerWnd, WM_SET_SOCKET_ERROR_STATE, (WPARAM)p, 0))
								::Sleep (0);
						}
					}
					else {
						if (theApp.m_bStrobeCancelled) {
							CTimeSpan tm = CTime::GetCurrentTime () - theApp.m_tmStrobeCancelled;
							int nSeconds = tm.GetTotalSeconds ();
							int nTimeout = 2 * 60; // 2 minutes

							//nTimeout = 10; TRACEF ("TODO: rem");

							if (nSeconds >= nTimeout) {
								if (::WaitForSingleObject (hEvent [0], 0) == WAIT_OBJECT_0) return 0;

								while (!::PostMessage (hOwnerWnd, WM_SET_STROBE, 0, 0))
									::Sleep (0);
							}
						}
					}
				}
			}
		}

		::Sleep (dwSleep);
	}

	return 0;
}

void CProdLine::SetWaiting (bool bWait) 
{
	m_bWaiting = bWait;

	if (IsOneToOnePrintEnabled ()) { // __SW0808__
		if (!bWait && !GetSerialPending ()) // heads can print, but no data to print
			return;
	}

#ifdef __IPPRINTER__
	if (!m_bWaiting) {
		__int64 lFind, lMin, lMax, lMaster = 0;

		GetCounts (NULL, lFind, lMin, lMax, lMaster);
		SetCounts (lMaster);
	}
#endif //__IPPRINTER__

	for (POSITION pos = m_listHeads.GetStartPosition(); pos; ) {
		DWORD dwKey;
		CHead * pHead = NULL;

		m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
		
		while (!PostThreadMessage (pHead->m_nThreadID, TM_SET_IGNORE_PHOTOCELL, m_bWaiting, 0L)) 
			::Sleep (0);
	}
}

bool CProdLine::IsWaiting () const
{
	return m_bWaiting;
}

ULONG CProdLine::GetCurrentTaskID () 
{
	CString str = GetCurrentTask ();

	return FoxjetDatabase::GetTaskID (theApp.m_Database, str, m_LineRec.m_strName, GetPrinterID (theApp.m_Database));
}

CString CProdLine::GetCurrentTask (CString * pstrKeyValue) 
{
	CPrintHeadList list;

	GetActiveHeads (list);

	for (POSITION pos = list.GetStartPosition(); pos != NULL; ) {
		CHead * pHead = NULL;
		DWORD dwKey = 0;

		list.GetNextAssoc (pos, dwKey, (void *&)pHead);

		if (pHead) {
			CHeadInfo info = CHead::GetInfo (pHead);
			CString strTask = info.GetTaskName ();

			if (strTask.GetLength ()) {
				if (pstrKeyValue) 
					* pstrKeyValue = info.GetKeyValue ();

				return strTask;
			}
		}
	}

	return _T ("");
}

bool CProdLine::DownloadSerialData (const CString & strDownload, CControlView * pView)
{
	using namespace FoxjetCommon;

	bool bResult = false;
	CStringArray vParams;
	CString strParams;
	CString str = Format ((LPBYTE)(LPCSTR)w2a (strDownload), strDownload.GetLength ());
	SETTINGSSTRUCT s;

	s.m_strData = _T ("COM1");
	GetSettingsRecord (theApp.m_Database, m_LineRec.m_lID, FoxjetDatabase::Globals::m_lpszSerialDownload, s);

	{ 
		CStringArray v;
		CString strPort = _T ("COM1");

		Tokenize (s.m_strData, v);

		if (v.GetSize ())
			strPort = v [0]; // for backward compatability

		vParams.Add (strPort);  // port name
		s.m_strData = _T ("9600,8,0,0,-1");

		bool bGetSettingsRecord = GetSettingsRecord (theApp.m_Database, 0, strPort, s);
		v.RemoveAll ();
		Tokenize (s.m_strData, v);

		if (!bGetSettingsRecord)
			v.InsertAt (0, _T ("")); // default data doesn't include port number

		for (int i = 1; i < v.GetSize (); i++)
			vParams.Add (v [i]);
	}


	if (vParams.GetSize () >= 5) {
		const CString strPort = vParams [0];
		int nHub = 0;

		if (!strPort.CompareNoCase (_T ("HUB A"))) 
			nHub = 1;
		else if (!strPort.CompareNoCase (_T ("HUB B"))) 
			nHub = 2;
		else if (!strPort.CompareNoCase (_T ("HUB C"))) 
			nHub = 3;
		else if (!strPort.CompareNoCase (_T ("HUB D"))) 
			nHub = 4;

		if (nHub) {
			if (m_AuxBox.IsOpen ()) {
				VARIANT v;
				int nFormattedLen = str.GetLength () + 1;

				::VariantInit (&v);
				v.vt = VT_UI1 | VT_BYREF;
				v.pbVal = new unsigned char [nFormattedLen];

				memset (v.pbVal, 0, nFormattedLen);
				int nLen = FoxjetDatabase::Unformat (str, (LPBYTE)v.pbVal);
				bResult = m_AuxBox.PutSerialData (nHub, &v, strlen ((PCHAR)v.pbVal)) == S_OK;

				delete [] v.pbVal;
			}
		}
		else if (!strPort.Mid (0, 3).CompareNoCase (_T ("COM"))) {
			using namespace ControlView;

			int nPort = _ttoi (strPort.Mid (3));
			int nFormattedLen = str.GetLength () + 1;
			char * psz = new char [nFormattedLen];

			memset (psz, 0, nFormattedLen);
			int nLen = FoxjetDatabase::Unformat (str, (LPBYTE)psz);

			if (pView) {
				using namespace ControlView;

				// if there is already a comm thread running, write to it's handle
				if (CComm * p = pView->GetCommHandle (nPort)) {
					LOCK (p->m_cs);

					{
						int nWrite = Write_Comport (p->m_hComm, nLen, psz);
						pView->UpdateSerialData (FoxjetDatabase::Format ((LPBYTE)psz, nLen));
						TRACEF ("Write_Comport: " + FoxjetDatabase::Format ((LPBYTE)psz, nLen));
						bResult = nWrite != 0;
					}
				}
			}

			if (!bResult) {
				// create a temp comm thread
				CComm * p		= new CComm;
				DWORD dwBaud	= _tcstoul (vParams [1], NULL, 10);
				int nByteSize	= _ttoi (vParams [2]);
				int nParity		= _ttoi (vParams [3]);
				int nStopBits	= _ttoi (vParams [4]);
				CString strParams;
				DWORD dwTimeout = 2000;
				CTime tmStart = CTime::GetCurrentTime ();

				strParams.Format (_T ("%d,%d,%d,%d,%d,%d,%d"),
					nPort,
					dwBaud,
					nByteSize,
					nParity,
					nStopBits,
					NOTUSED,
					-1);

				p->m_hControlWnd = pView->GetSafeHwnd();

				if (p->FromString (strParams)) {
					p->m_hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)CControlView::CommThreadFunc, 
						(LPVOID)p, 0, &p->m_dwThreadID);

					// wait for open handle
					while (p->m_hComm == NULL || p->m_hComm == INVALID_HANDLE_VALUE) {
						::Sleep (0);
						CTimeSpan tm = CTime::GetCurrentTime () - tmStart;

						if ((DWORD)(tm.GetTotalSeconds () * 1000) > dwTimeout || tm.GetTotalSeconds () < 0) 
							break;
					}

					if (p->m_hComm != NULL && p->m_hComm != INVALID_HANDLE_VALUE) {
						LOCK (p->m_cs);

						{
							int nWrite = Write_Comport (p->m_hComm, nLen, psz);
							pView->UpdateSerialData (FoxjetDatabase::Format ((LPBYTE)psz, nLen));
							TRACEF ("Write_Comport: " + FoxjetDatabase::Format ((LPBYTE)psz, nLen));
							bResult = (nWrite != 0);

							if (bResult)
								if (CControlView * pView = (CControlView *)theApp.GetActiveView ()) 
									pView->UpdateSerialData (strPort + _T (": ") + FoxjetDatabase::Format ((LPBYTE)psz, nLen));
						}
					}

					p->m_dwThreadID = 0; // signal to die

					VERIFY (::WaitForSingleObject (p->m_hThread, dwTimeout) == WAIT_OBJECT_0);
//
//					// wait for handle to be closed
//					while (p->m_hComm != NULL)
//						::Sleep (0);

					delete p;
				}
			}

			delete [] psz;
		}
	}

	return bResult;
}

CHeadInfo CProdLine::GetHeadInfo (LPVOID lpAddr)
{
	LOCK (m_HeadListSentinel);
	static int nCall = 0;

	#ifdef _DEBUG
	//{ CString str; str.Format (_T ("GetHeadInfo: lpAddr: 0x%p [%d]"), lpAddr, lpAddr); TRACEF (str); }
	#endif

	for (POSITION pos = m_listHeads.GetStartPosition(); pos != NULL; ) {
		ULONG lKey;
		CHead * p= NULL;

		m_listHeads.GetNextAssoc (pos, lKey, (void *&)p);

		if (p && p == lpAddr) 
			return CHeadInfo (CHead::GetInfo ( * p));
	}

	return CHeadInfo ();
}

void CProdLine::UpdateImages ()
{
#ifdef __WINPRINTER__
	CPrintHeadList list;

	GetActiveHeads (list);

	//if (!theApp.GetImagingModeExpire ()) 
	{
	
		FoxjetElements::CDateTimeElement::SetPrintTime (new CTime (CTime::GetCurrentTime ()), GetID ());

		for (POSITION pos = list.GetStartPosition(); pos; ) {
			ULONG lKey; 
			CHead * pHead = NULL;

			list.GetNextAssoc (pos, lKey, (void *&)pHead);

			if (CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pHead)) {
				if (theApp.GetImagingMode () == IMAGE_ON_EOP) {
					CLocalHead::BUILDIMAGESTRUCT * pParam = MAKEIMGPARAMS (IMAGING, pHead->m_nThreadID);
					HANDLE hEvent = pParam->m_hEvent;
	
					while (!::PostThreadMessage (p->m_nThreadID, TM_BUILD_IMAGE, (WPARAM)pParam, 0))
						::Sleep (0);

					DWORD dwWait = ::WaitForSingleObject (hEvent /* pParam->m_hEvent */, 5000);

					//if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
					if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
					if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
					if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");

					//if (dwWait == WAIT_OBJECT_0)
					//	delete pParam;
				}
			}
		}
	}

	DownloadImages ();
#endif //__WINPRINTER__
}

__int64 CProdLine::GetPalletMax () //sw0844
{
	#ifdef __WINPRINTER__
	using namespace FoxjetElements;

	CElementPtrArray v;

	GetElements (v, COUNT);

	for (int i = 0; i < v.GetSize (); i++) {
		if (DYNAMIC_DOWNCAST (CCountElement, v [i])) {
			const CCountElement & e = * (const CCountElement *)v [i];

			if (e.IsMaster ()) 
				return e.GetPalMax ();
		}
	}
	#endif //__WINPRINTER__

	return 0;
}

__int64 CProdLine::GetUnitsPerPallet () //sw0844
{
	#ifdef __WINPRINTER__

	using namespace FoxjetElements;

	CElementPtrArray v;

	GetElements (v, COUNT);

	for (int i = 0; i < v.GetSize (); i++) {
		if (DYNAMIC_DOWNCAST (CCountElement, v [i])) {
			const CCountElement & e = * (const CCountElement *)v [i];

			if (e.IsMaster () && e.IsPalletCount ())
				return e.GetMax (); 
		}
	}

	#endif //__WINPRINTER__

	return 0;
}

void CProdLine::UpdateDynamicData (ULONG lLineID, UINT nIndex, const CString & str, CControlView * pView) // sw0849 // sw0864
{
	CStringArray vLines;

	ASSERT (pView);
	CControlDoc * pDoc = pView->GetDocument ();
	ASSERT (pDoc);

	pDoc->GetProductLines (vLines);

	for (int i = 0; i < vLines.GetSize (); i++) {
		if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
			if (pLine->GetID () == lLineID) {
				CPrintHeadList list;

				pLine->GetActiveHeads (list);

				for (POSITION pos = list.GetStartPosition(); pos != NULL; ) {
					::CHead * pHead = NULL;
					DWORD dwKey = 0;

					list.GetNextAssoc (pos, dwKey, (void *&)pHead);
					CHeadInfo info = CHead::GetInfo (pHead);

					LocalHead::DYNDATA * pData = new LocalHead::DYNDATA;

					pData->m_nIndex = nIndex;
					pData->m_strData = str;

					POST_THREAD_MESSAGE (info, TM_DOWNLOAD_DYNDATA, (WPARAM)pData);
				}
			}
		}
	}
}

void CProdLine::OnSw0849 (const CSerialPacket * pData, CControlView * pView) // sw0849
{
	using namespace FoxjetCommon;

	CControlDoc * pDoc = pView->GetDocument ();
	CString strData;
	ULONG lLineID	= -1;
	UINT nDynIndex	= -1;
	CLongArray vLine;

	for (int i = 0; i < pData->CountSegments (); i++) {
		if (const CSerialPacket * p = pData->GetSegment (i)) {
			TCHAR * pData = p->m_pData;
			ULONG lLen = p->m_nDataLen;

			TRACEF (p->m_sLineName + _T (": ") + FoxjetDatabase::Format (p->m_pData, p->m_nDataLen));

			if (CProdLine * pLine = pDoc->GetProductionLine (p->m_sLineName)) {
				lLineID = pLine->GetID ();

				for (ULONG i = 0; i < lLen; i++) {
					UCHAR c = pData [i];

					switch (c) {
					case CSerialPacket::TOKEN_LINEID:
						{
							if (nDynIndex != -1)
								UpdateDynamicData (lLineID, nDynIndex, strData, pView);

							strData.Empty ();
							nDynIndex = -1;

							if ((i + 2) < lLen) {
								CString strID;
					
								strID += (TCHAR)pData [++i];
								strID += (TCHAR)pData [++i];
	
								int nIndex = _ttoi (strID) - 1;
								lLineID = CSerialPacket::GetLineID (nIndex);

								if (Find (vLine, lLineID) == -1)
									vLine.Add (lLineID);
							}
						}
						break;
					case CSerialPacket::TOKEN_DYNID:
						{
							if (nDynIndex != -1)
								UpdateDynamicData (lLineID, nDynIndex, strData, pView);

							strData.Empty ();
							nDynIndex = -1;

							if ((i + 3) < lLen) {
								CString strID;
					
								strID += (TCHAR)pData [++i];
								strID += (TCHAR)pData [++i];
								i++; // skip <RS>
								nDynIndex = _ttoi (strID) - 1;
							}
						}
						break;
					default:
						strData += c;
						break;
					}
				}

				if (nDynIndex != -1)
					UpdateDynamicData (lLineID, nDynIndex, strData, pView);
			}
		}
	}
}

void CProdLine::EnableDynamicDataDownload (bool bEnable)
{
#ifdef __IPPRINTER__
	for (POSITION pos = m_listHeads.GetStartPosition (); pos; ) {
		DWORD dwKey = 0;
		CHead * pHead = NULL;

		m_listHeads.GetNextAssoc(pos, dwKey, (void *&)pHead);
		ASSERT (pHead);

		if (CRemoteHead * pRemote = DYNAMIC_DOWNCAST (CRemoteHead, pHead)) 
			pRemote->EnableDynamicDataDownload (bEnable);
	}
#endif //__IPPRINTER__
}

CString CProdLine::GetSerialData (const FoxjetDatabase::LINESTRUCT & line, const CString & str)
{
	CString strResult;// = str;
	int nOffset = line.m_nBufferOffset;
	int nLen = line.m_nSignificantChars;
	
	if (str.GetLength () >= nOffset)
		strResult = str.Mid (nOffset, min (str.GetLength () - nOffset, nLen));

	return strResult;
}

CString CProdLine::GetSerialData (const CString & str)
{
	return GetSerialData (m_LineRec, str);
/*
	CString strResult = str;
	int nOffset = m_LineRec.m_nBufferOffset;
	int nLen = m_LineRec.m_nSignificantChars;
	
	if (str.GetLength () >= nOffset + nLen)
		strResult = str.Mid (nOffset, nLen);

	return strResult;
*/
}

bool CProdLine::IsOneToOnePrintEnabled ()
{
	CPrintHeadList list;
	
	GetActiveHeads (list);
	
	for (POSITION pos = list.GetStartPosition(); pos; ) {
		ULONG lKey; 
		CHead * pHead = NULL;

		list.GetNextAssoc (pos, lKey, (void *&)pHead);

		if (pHead) {
			CHeadInfo info = CHead::GetInfo (pHead);
			bool bOneToOne = false;

			SEND_THREAD_MESSAGE (info, TM_IS_ONE_TO_ONE_PRINT_ENABLED, &bOneToOne);

			if (bOneToOne)
				return true;
		}
	}

	return false;
}

CString CProdLine::GetDisplayInfo () 
{
	LPCTSTR lpszState [] = { _T ("DISABLED"), _T ("RUNNING"), _T ("IDLE"), _T ("STOPPED") }; 
	FoxjetDatabase::TASKSTATE eState = GetTaskState ();
	CStringArray v;

	v.Add (GetName ());
	v.Add (GetCurrentTask ());
	v.Add (CString (lpszState [eState]));
	v.Add (ToString (GetTabStrobeColor ()));
	v.Add (GetCount ());

	{
		CPrintHeadList list;

		GetActiveHeads (list);

		for (POSITION pos = list.GetStartPosition(); pos; ) {
			ULONG lKey; 
			CHead * pHead = NULL;

			list.GetNextAssoc (pos, lKey, (void *&)pHead);

			if (pHead) {
				CHeadInfo info = CHead::GetInfo (pHead);

				//str += _T ("\n") + info.GetErrors ();

				if (info.m_Status.m_nInkLevel == LOW_INK)
					v.Add (LoadString (IDS_HEADERROR_LOWINK));
			}
		}
	}

	return ToString (v);
}

CString CProdLine::GetDebugInfo () 
{
	CString str;
	CPrintHeadList list;
	CString strSpeed;
	int nHead = 1;
	CArray <CHead *, CHead *> v;
	
	GetActiveHeads (list);
	
	for (POSITION pos = list.GetStartPosition(); pos; nHead++) {
		ULONG lKey; 
		CHead * pHead = NULL;

		list.GetNextAssoc (pos, lKey, (void *&)pHead);

		if (pHead) {
			CHeadInfo info = CHead::GetInfo (pHead);
			ULONG lInsert = (info.m_Config.m_BoxPanel.m_nNumber << 16) + info.m_Config.m_HeadSettings.m_lRelativeHeight;
			bool bFound = false;

			for (int i = 0; i < v.GetSize (); i++) {
				CHeadInfo cur = CHead::GetInfo (v [i]);
				ULONG lCur = (cur.m_Config.m_BoxPanel.m_nNumber << 16) + cur.m_Config.m_HeadSettings.m_lRelativeHeight;

				if (lCur > lInsert) {
					v.InsertAt (i, pHead);
					bFound = true;
					break;
				}
			}

			if (!bFound)
				v.Add (pHead);
		}
	}

	for (int i = 0; i < v.GetSize (); i++) {
		CHeadInfo info = CHead::GetInfo (v [i]);

		//name	count	enc.sp	hv	temp	ink		printing

		str += info.GetFriendlyName ()	+ _T ("<TAB>");
		str += info.GetCount ()			+ _T ("<RIGHT><TAB>");
		str += info.GetStateString ();

		{
			const double dSpeed = info.m_Status.m_dLineSpeed;
			double d = dSpeed;
			int n = (int)(dSpeed * 12.0 * 1000.0);
			UNITS units = ListGlobals::defElements.GetUnits ();
			CString strUnits = LoadString (IDS_FEETPERMINUTE);

			switch (units) {
			case CENTIMETERS: // really meters
				d = (dSpeed * 2.54 * 12.0) / 100.0;
				strUnits = LoadString (IDS_METERSPERMINUTE);
				break;
			case HEAD_PIXELS:
				d = dSpeed * info.m_Config.m_HeadSettings.GetRes () * 12;
				strUnits = LoadString (IDS_PIXELSPERMINUTE);
				break;
			}

			strSpeed.Format (_T ("%.02lf %s%s<RIGHT><TAB>"), d, strUnits, (dSpeed < 1.0 ? _T ("<RED>") : _T ("")));
			str += strSpeed;
		}

		str += info.IsErrorPresent (HEADERROR_HIGHVOLTAGE) ? _T ("<DOT_FF0000><TAB>") : _T ("<CENTER><GREEN>OK<TAB>");
		str += info.IsErrorPresent (HEADERROR_LOWTEMP) ? _T ("<DOT_FF0000><TAB>") : _T ("<CENTER><GREEN>OK<TAB>");
		
		if (info.IsErrorPresent (HEADERROR_OUTOFINK))
			str += _T ("<DOT_FF0000><TAB>");
		else if (info.IsErrorPresent (HEADERROR_LOWINK))
			str += _T ("<DOT_FFFF00><TAB>");
		else
			str += _T ("<CENTER><GREEN>OK<TAB>");

		str += info.IsPrinting () ? _T ("<DOT_00FF00>") : _T ("");

		//str += (info.IsPrinting () ? _T ("<RED>") : _T ("")) + info.GetFriendlyName () + _T (": ") + info.GetCount () + _T (" ") + info.GetErrors () + _T ("\n");
		//str += _T ("<TAB>") + LoadString (IDS_COUNT) + _T (": ") + info.GetCount () + _T ("\n");
		str += _T ("\n");
	}

	/*
	{
		Foxjet3d::UserElementDlg::CUserElementArray v;

		GetUserElements (v, false);

		if (v.GetSize ())
			str += LoadString (IDS_USER_ELEMENTS) + _T (": \n");

		for (int i = 0; i < v.GetSize (); i++) {
			Foxjet3d::UserElementDlg::CUserItem & user = v [i];

			str += _T ("<TAB>") + user.m_strPrompt + _T (": ") + user.m_strData + _T ("\n");
		}
	}
	*/

	str.Replace (_T ("<TAB>"), _T ("\t"));

	if (m_pSerialData)
		//str += LoadString (IDS_SERIAL_DATA) + _T (": ") + FoxjetDatabase::Format (m_pSerialData->Data, m_pSerialData->m_Len) + _T ("\n");
		str += LoadString (IDS_SERIAL_DATA) + _T (": ") + m_pSerialData->Data + _T ("\n");

	return str;
}

CString CProdLine::GetWarnings (CStringArray & vResult) 
{
	CString str;
	CPrintHeadList list;
	CString strSpeed;
	int nHead = 1;
	CArray <CHead *, CHead *> v;
	
	GetActiveHeads (list);
	
	for (POSITION pos = list.GetStartPosition(); pos; nHead++) {
		ULONG lKey; 
		CHead * pHead = NULL;

		list.GetNextAssoc (pos, lKey, (void *&)pHead);

		if (pHead) {
			CHeadInfo info = CHead::GetInfo (pHead);
			CString str = _T ("<") + info.GetUID () + _T (">") + info.GetFriendlyName () + _T (": ") + info.GetWarnings ();

			if (Find (vResult, str) == -1)
				InsertAscending (vResult, str);
		}
	}

	//if (COdbcDatabase::IsLocal ()) {
	//	//CString str = _T ("<0>") + LoadString (IDS_NETWORK) + _T (": ") + LoadString (IDS_USING_LOCAL_DB);
	//	CString str = _T ("<0>") + LoadString (IDS_DATABASE) + _T (": ") + LoadString (IDS_LOCALDBCONNECTION);

	//	if (Find (vResult, str) == -1)
	//		InsertAscending (vResult, str);
	//}

	if (IsNetworked ()) {
		if (!theApp.IsNetworkConnected ()) {
			CString str = _T ("<0>") + LoadString (IDS_NETWORK) + _T (": ") + LoadString (IDS_DISCONNECTED);

			if (Find (vResult, str) == -1)
				InsertAscending (vResult, str);
		}
		
		if (theApp.GetLocalDSN ().GetLength ()) {
			if (theApp.IsLocalDB ()) {
				CString str = _T ("<0>") + LoadString (IDS_DATABASE) + _T (": ") + LoadString (IDS_LOCALDBCONNECTION);

				if (Find (vResult, str) == -1)
					InsertAscending (vResult, str);
			}
		}
	}

	return ToString (vResult);
}

void CProdLine::AutoExport()
{
	ULONG lPrinterID = GetPrinterID(theApp.m_Database);

	if (theApp.IsScanReportLoggingEnabled()) {
		if (Foxjet3d::ScanReportDlg::CScanReportDlg::IsAutoExport(theApp.m_Database, lPrinterID))
			Foxjet3d::ScanReportDlg::CScanReportDlg::Export(theApp.m_Database, GetName(), lPrinterID);
	}

	if (theApp.IsPrintReportLoggingEnabled()) {
		if (Foxjet3d::PrintReportDlg::CPrintReportDlg::IsAutoExport(theApp.m_Database, lPrinterID)) 
			Foxjet3d::PrintReportDlg::CPrintReportDlg::Export(theApp.m_Database, GetName(), lPrinterID);
	}
}

void CProdLine::AddPrinterReportRecord(FoxjetDatabase::REPORTTYPE type, FoxjetDatabase::REPORTSTRUCT& report)
{
	Foxjet3d::UserElementDlg::CUserElementArray vUser;
	CStringArray vPrompt, vData;
	std::vector<std::wstring> insert;
	bool bUpdateColumns = false;

	GetUserElements(vUser, false);

	switch (type) {
	case FoxjetDatabase::REPORT_TASK_START:
	case FoxjetDatabase::REPORT_TASK_START_REMOTE:
	case FoxjetDatabase::REPORT_TASK_START_POWERUP:
	case FoxjetDatabase::REPORT_KEYFIELDSTART:
	case FoxjetDatabase::REPORT_TASK_IDLE:
	case FoxjetDatabase::REPORT_TASK_RESUME:
		bUpdateColumns = true;
		break;
	}


	if (vUser.GetSize()) {
		auto* pmap = new std::map<std::wstring, std::wstring>();
		auto& map = *pmap;

		report.m_pmapUser.reset(pmap);

		for (int i = 0; i < vUser.GetSize(); i++)
			map[(LPCTSTR)FoxjetDatabase::FormatReportUserColumnName(vUser[i].m_strPrompt)] = vUser[i].m_strData;

		try
		{
			COdbcRecordset rst(theApp.m_Database);
			std::map<std::wstring, int> cols;

			rst.Open(CString(_T("SELECT * FROM ")) + Reports::m_lpszTable);

			for (short i = 0; i < rst.GetODBCFieldCount(); i++)
				cols[((LPCTSTR)rst.GetFieldName(i))]++;

			for (auto i = map.begin(); i != map.end(); i++) {
				std::wstring strFind = _T("[") + (std::wstring)Reports::m_lpszTable + _T("].[") + i->first + _T("]");

				if (cols.find(strFind) == cols.end())
					insert.push_back(i->first);
			}
		}
		catch (CMemoryException * e) { HANDLEEXCEPTION(e); }
		catch (COleException * e) { HANDLEEXCEPTION(e); }

		if (bUpdateColumns) {
			try
			{
				for each (auto col in insert)
				{
					CString str;

					str.Format(_T("ALTER TABLE %s ADD [%s] MEMO;"), Reports::m_lpszTable, col.c_str());
					TRACEF(str);
					theApp.m_Database.ExecuteSQL(str);
				}
			}
			catch (CMemoryException * e) { HANDLEEXCEPTION(e); }
			catch (COleException * e) { HANDLEEXCEPTION(e); }
		}
	}

	FoxjetDatabase::AddPrinterReportRecord(theApp.m_Database, type, report, GetPrinterID(theApp.m_Database));
	AutoExport();
}
