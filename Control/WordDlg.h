#if !defined(AFX_WORDDLG_H__7D4CF180_7067_4861_990C_6822D844ACA2__INCLUDED_)
#define AFX_WORDDLG_H__7D4CF180_7067_4861_990C_6822D844ACA2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WordDlg.h : header file
//

#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CWordDlg dialog

class CWordDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CWordDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CWordDlg)
	enum { IDD = IDD_WORD };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	WORD m_w;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWordDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();

	// Generated message map functions
	//{{AFX_MSG(CWordDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnCheck(UINT nID);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WORDDLG_H__7D4CF180_7067_4861_990C_6822D844ACA2__INCLUDED_)
