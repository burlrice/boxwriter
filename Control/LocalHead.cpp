// LocalHead.cpp : implementation file
//

#include "stdafx.h"
#include "Control.h"
#include "Resource.h"
#include <Math.h>
#include "DebugDlg.h"

#include "Sw0848Thread.h"
#include "ProdLine.h"
#include "Control.h"
#include "Defines.h"
#include "MainFrm.h"
#include "LocalHead.h"
#include "Database.h"
#include "Edit.h"
#include "SerialPacket.h"
#include "Debug.h"
#include "fj_defines.h"
#include "BaseElement.h"
#include "CountElement.h"
#include "TemplExt.h"
#include "AppVer.h"
#include "Registry.h"
#include "DatabaseElement.h"
#include "UserElement.h"
#include "WinMsg.h"
#include "Parse.h"
#include "FileExt.h"
#include "HeadDlg.h"
#include "StrobeDlg.h"
#include "Parse.h"
#include "RawBitmap.h"
#include "ximage.h"
#include "StartTaskDlg.h"

#include "DateTimeElement.h"
#include "fj_dyntext.h"
#include "IpElements.h"



#ifdef _DEBUG
	//#define __DEBUG_BUILDIMAGE__
#endif

#if defined( __DEBUG_LOW_INK_CODE__ ) || defined( _DEBUG )
extern DWORD dwSimError;
#endif

#define UPDATESTATE() \
	if (/*IsAlive () &&*/ m_bShowPrintCycle) { \
		if (m_hControlWnd && ::IsWindow (m_hControlWnd) && ::IsWindowVisible (m_hControlWnd)) { \
			while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this))  \
				::Sleep(0); \
			\
			CHead::SetInfo (* this, &GetInfo ()); \
		} \
	} 

#ifdef _DEBUG
void CLocalHead::Format (USHORT n, CHAR * psz) 
{
	int nIndex= 0;

	for (int i = (sizeof (n) * 8) - 1; i >= 0; i--) 
		psz [nIndex++] = (n & (1 << i)) ? 'X' : '.';
}

void CLocalHead::DbgPrintBuffer (USHORT wColumns, UCHAR cBPC, USHORT wRowWords, PUCHAR pImage) 
{
	CString str;
	PUSHORT pBuffer = (PUSHORT)pImage;

	for (int j = 0; j < wColumns; j++) {
		CHAR szDebug [512];
		int nDebugIndex = 0;

		{
			for (int i = 0; i < ARRAYSIZE (szDebug); i++) 
				szDebug [i] = ' ';
		}

		PUCHAR p = (PUCHAR)pBuffer;

		for (int i = 0; i < cBPC; i += 2) {
			USHORT w = p [i] << 8 | p [i + 1];
			int nReg = IV_REG_DATA_PORT + ((i % 4) ? 2 : 0);

			Format (w, &szDebug [nDebugIndex]);
			nDebugIndex += 17;
		}

		if (cBPC % 2) {
			int nReg = IV_REG_DATA_PORT + 2;
			Format (0x0000, &szDebug [nDebugIndex]);
			nDebugIndex += 17;
		}

		pBuffer += wRowWords;

		szDebug [nDebugIndex] = 0;
		
		str.Format (_T ("%03d: %s"), j, szDebug);
		TRACER (str + _T ("\n"));
	}
}
#endif

#ifdef __WINPRINTER__
	#include "LabelElement.h"
#endif //__WINPRINTER__

using namespace FoxjetDatabase;

#ifdef __WINPRINTER__
	using namespace FoxjetElements;
#endif //__WINPRINTER__

#define DEBUGSERIAL(s) TRACEF (s)

//#define __DEBUGIMAGELEN__

#ifndef _DEBUG
	#ifdef __DEBUGIMAGELEN__
	#undef __DEBUGIMAGELEN__
	#endif
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
using namespace LocalHead;

CError::CError (const CString & str, HEADERRORTYPE dwType, ULONG lLimit)
:	m_str			(str),
	m_dwType		(dwType),
	m_lLimit		(lLimit),
	m_lDuration		(0),
	m_bSignalled	(false)
{
}

CError::CError (const CError & rhs)
:	m_str			(rhs.m_str),
	m_dwType		(rhs.m_dwType),
	m_lLimit		(rhs.m_lLimit),
	m_lDuration		(rhs.m_lDuration),
	m_bSignalled	(rhs.m_bSignalled)
{
	TRACEF ("CError (const CError &)");
}

CError & CError::operator = (const CError & rhs)
{
//	TRACEF ("CError::operator = (const CError &)");

	if (this != &rhs) {
		m_str			= rhs.m_str;
		m_dwType		= rhs.m_dwType;
		m_lLimit		= rhs.m_lLimit;
		m_lDuration		= rhs.m_lDuration;
		m_bSignalled	= rhs.m_bSignalled;
	}

	return * this;
}

CError::~CError ()
{
}

void CError::Clear ()
{
	m_lDuration = 0;
	m_bSignalled = false;
}

void CError::Signal ()
{
	m_bSignalled = true;
}

static CError & GetError (HEADERRORTYPE type, CArray <CError, CError &> & vErrors)
{
	for (int i = 0; i < vErrors.GetSize (); i++)
		if (vErrors [i].m_dwType == type)
			return vErrors [i];

	return vErrors [0];
}

/////////////////////////////////////////////////////////////////////////////
// CLocalHead

IMPLEMENT_DYNCREATE(CLocalHead, CHead)

CLocalHead::CLocalHead()
:	CHead (),
	m_bPhotoEnabled (false),
	m_bAmsActive (false),
	m_bAmsCountDown (false),
	m_nAmsInterval (12),
	m_lastAMSCycle (CTime::GetCurrentTime ()),
	m_hDeviceID (0x00),
	m_hPhotoEvent (NULL),
	m_hEOPEvent (NULL),
	m_hSerialEvent (NULL),
	m_bDDE_Enabled (true),
	m_lMinPhotocellDelay (0),
	m_lMaxPhotocellDelay (0),
	m_wMaxFlush (0),
	m_strobe (STROBESTATE_OFF),
	m_lStartTaskID (-1),
	m_wPrintDly (0),
	m_wProdLen (0),
	m_wImageLen (0),
	m_nFlush (0),
	m_bDebug (false),
	m_bShowPrintCycle (false),
//	pTest (NULL),
#ifdef _DEBUG
	m_pDbgDlg (NULL),
#endif
	m_pUsbDbgDlg (NULL),
	m_bImageBuilt (false),
	m_bLastIgnore (false),
	m_lCardTimeout (30),
	m_tmWDT (CTime::GetCurrentTime ()),
	m_nStrobeColor (0),
	m_dwApsTimeout (0),
	m_nApsDelay (10),
	// TODO: rem //m_pbIsUSB (NULL),
	m_lGetIndicators (0),
	m_lEp8 (0),
	m_tmFPGA (CTime::GetCurrentTime ()),
	m_tmCard (CTime::GetCurrentTime ()),
	m_dwImageRefresh (0),
	m_nInkWarning (INK_CODE_WARNGING_LEVEL_0),
	m_bSerialStart (false)
{
	//m_csImage.m_strName = _T ("CLocalHead::m_csImage:");
	//m_csConfig.m_strName = _T ("CLocalHead::m_csConfig:");

	m_bDisabled					= true;
	m_Status.m_nInkLevel		= -1; 
	m_Status.m_lCountUntil		= -1;

	#if __CUSTOM__ == __SW0828__
	{
		CString str;
		
		str.Format (_T ("Software\\FoxJet\\%s"), FoxjetCommon::GetAppTitle ());
		CString strCount = FoxjetDatabase::GetProfileString (str, _T ("m_lLifetimeCount"), _T ("0"));

		m_Status.m_lLifetimeCount = _tcstoul (strCount, NULL, 16);
		m_Status.m_lTotalBoxCount = 0;
	}
	#endif
	
	memset (&m_registersISA, 0x00, sizeof (m_registersISA));
	::QueryPerformanceFrequency (&m_freq); 

//	m_dRemainingInk = 0.1; TRACEF ("//TODO: rem");

//	QueryPerformanceFrequency ( &m_n64Freq );
	m_clockLast = clock ();

	m_vErrors.Add (CError (_T ("LOWTEMP"),		HEADERROR_LOWTEMP,		30000));
	m_vErrors.Add (CError (_T ("HIGHVOLTAGE"),	HEADERROR_HIGHVOLTAGE,	4000));
	m_vErrors.Add (CError (_T ("LOWINK"),		HEADERROR_LOWINK,		4000));
	m_vErrors.Add (CError (_T ("EP8"),			HEADERROR_EP8,			1));

	m_vClear.Add (CError (_T ("LOWTEMP"),		HEADERROR_LOWTEMP,		2000));

	{ // assume low temp state
		CError & err = GetError (HEADERROR_LOWTEMP, m_vErrors);
		
		err.m_bSignalled = true;
		err.m_lDuration = err.m_lLimit + 1;
	}

	ZeroMemory (&m_eop, sizeof (m_eop));

	{
		long double dPixel = FoxjetDatabase::CalcInkUsage (m_Config.m_HeadSettings.m_nHeadType, FoxjetDatabase::INKTYPE_FIRST, 1);

		//if (m_Config.m_HeadSettings.m_bDoublePulse)
		//	dPixel *= 2;

		m_inkcode.m_bEnabled	= false;
		m_inkcode.m_lMax		= (unsigned __int64)((long double)500.0 / dPixel);
		TRACEF (std::to_string (m_inkcode.m_lMax).c_str ());
		m_inkcode.m_lUsed		= 0;
	}
}

CLocalHead::~CLocalHead()
{
}

BOOL CLocalHead::InitInstance()
{
///*
	CMainFrame * pFrame = (CMainFrame *) theApp.m_pMainWnd;
	CView * pView = pFrame->m_pControlView;
	m_hControlWnd =  pView->GetSafeHwnd();

	if (CWnd * p = CWnd::FromHandlePermanent (m_hControlWnd)) 
		m_pMainWnd = p;

	CString sEvent;
	
	sEvent.Format (_T ("Exit %lu"), m_hThread );
	m_hExit = CreateEvent ( NULL, true, false, sEvent);

	sEvent.Format (_T ("SerialEvent %lu"), m_hThread );
	m_hSerialEvent = CreateEvent ( NULL, true, false, sEvent);

	m_hAmsFinished = CreateEvent ( NULL, true, false, NULL );	// cgh 05e25

	sEvent.Format (_T ("ImageRefresh %lu"), m_hThread );
	m_hImageRefresh = CreateEvent ( NULL, true, false, sEvent);
	
	m_DrawContext = FoxjetCommon::PRINTER;

//	return TRUE;
	return CHead::InitInstance ();
}

int CLocalHead::ExitInstance()
{

	if ( m_hDeviceID )
	{
		if (theApp.IsMPHC ()) {
			if (IsWDTEnabled ()) 
				DisableWDT ();

			if (IsUSB ()) {
				USB::CUsbMphcCtrl * pDriver = theApp.GetUSBDriver ();

				pDriver->SetStrobe ((UINT)m_hDeviceID, STROBE_LIGHT_OFF);
				pDriver->EnablePrint ((UINT)m_hDeviceID, false);
				pDriver->UnRegisterPhotoEvent ((UINT)m_hDeviceID);
				pDriver->UnRegisterEOPEvent ((UINT)m_hDeviceID);
				pDriver->UnRegisterSerialEvent ((UINT)m_hDeviceID);
				//pDriver->UnRegisterAmsEvent ((UINT)m_hDeviceID);
				pDriver->ReleaseAddress ((UINT)m_hDeviceID);
			}
			else {
				CMphc * pDriver = theApp.GetMPHCDriver ();

				pDriver->SetStrobe (m_hDeviceID, STROBE_LIGHT_OFF);
				pDriver->EnablePrint ( m_hDeviceID, false );
				pDriver->UnregisterPhotoEvent ( m_hDeviceID );
				pDriver->UnregisterEOPEvent ( m_hDeviceID );		// cgh 04d12
				pDriver->UnregisterSerialEvent ( m_hDeviceID );
				pDriver->UnregisterAmsEvent ( m_hDeviceID );		// cgh 05e25
				pDriver->ReleaseAddress ( m_hDeviceID );
			}
		}
		else {
			CIvPhc * pDriver = theApp.GetIVDriver ();

			pDriver->SetStrobe (m_hDeviceID, STROBE_LIGHT_OFF);
			pDriver->EnablePrint (m_hDeviceID, false);
			pDriver->UnregisterPhotoEvent (m_hDeviceID);
			pDriver->UnregisterEOPEvent (m_hDeviceID);
			pDriver->UnregisterSerialEvent ( m_hDeviceID );
			pDriver->ReleaseAddress (m_hDeviceID);
		}
	}

	if ( m_pImageBuffer != NULL )
	{
		delete m_pImageBuffer;
		m_pImageBuffer = NULL;
	}
	
#ifdef _DEBUG
	// Begin Debug Stuff
	if ( m_pDbgDlg )
	{
		delete m_pDbgDlg;
		m_pDbgDlg = NULL;
	}
	// End Debug Stuff
#endif

	if (m_pUsbDbgDlg) {
		delete m_pUsbDbgDlg;
		m_pUsbDbgDlg = NULL;
	}

	/* TODO: rem
	if (m_pbIsUSB) {
		delete m_pbIsUSB;
		m_pbIsUSB = NULL;
	}
	*/

	// m_hExit is closed by the sender of TM_KILLTHREAD 
	CloseHandle (m_hSerialEvent);
	CloseHandle (m_hPhotoEvent);
	CloseHandle (m_hAmsFinished);

	m_hExit			= NULL; 
	m_hSerialEvent	= NULL;
	m_hPhotoEvent	= NULL;
	m_hAmsFinished	= NULL;

	return CHead::ExitInstance ();
}

BEGIN_MESSAGE_MAP(CLocalHead, CHead)
	//{{AFX_MSG_MAP(CLocalHead)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_THREAD_MESSAGE (TM_SET_IO_ADDRESS, OnSetIoAddress)
	ON_THREAD_MESSAGE (TM_SHOW_DEBUG_DIALOG, OnShowDebugDialog)
//	ON_THREAD_MESSAGE (TM_ENABLE_PHOTO, OnEnablePhotocell)
	ON_THREAD_MESSAGE (TM_UPDATE_STROBE, OnUpdateStrobe)
	ON_THREAD_MESSAGE (TM_DO_DEBUG_PROCESS, OnDoDebugProcess)
	ON_THREAD_MESSAGE (TM_READ_CONFIG, OnReadConfig)
	ON_THREAD_MESSAGE (TM_CHANGE_COUNTS, OnChangeCounts)
	ON_THREAD_MESSAGE (TM_EVENT_PHOTO, OnEventPhoto)
	ON_THREAD_MESSAGE (TM_EVENT_EOP, OnEventEOP)
	ON_THREAD_MESSAGE (TM_TOGGLE_IMAGE_DEBUG, OnToggleImageDebug)
	ON_THREAD_MESSAGE (TM_BUILD_IMAGE, OnBuildImage)
	ON_THREAD_MESSAGE (TM_DOWNLOAD_IMAGE_SEGMENT, OnDownloadImageSegment)
	ON_THREAD_MESSAGE (TM_INCREMENT, OnIncrement)
	ON_THREAD_MESSAGE (TM_TRIGGER_WDT, OnTriggerWDT)
	ON_THREAD_MESSAGE (TM_BUILD_NEXT_IMAGE, OnBuildNextImage)
	ON_THREAD_MESSAGE (TM_DOWNLOAD_IMAGES, OnDownloadImages)
	ON_THREAD_MESSAGE (TM_BEGIN_APS, OnBeginApsCycle)
	ON_THREAD_MESSAGE (TM_SETSERIALPENDING,	OnSetSerialPending)
	ON_THREAD_MESSAGE (TM_SIGNAL_REFRESH, OnSignalRefresh)
	//ON_THREAD_MESSAGE (TM_HEAD_CONFIG_CHANGED, OnHeadConfigChanged)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLocalHead message handlers

void CLocalHead::OnChangeCounts(UINT wParam, LONG lParam)
{
	CHANGECOUNTSTRUCT * pParam = (CHANGECOUNTSTRUCT *)wParam;

	ASSERT (pParam);

	if (pParam)
		SetCounts (* pParam);

	while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this))
		::Sleep(0);

	delete pParam;

	if (HANDLE h = (HANDLE)lParam)
		::SetEvent (h);
}

void CLocalHead::OnSetIoAddress (UINT wParam, LONG lParam)
{
	ASSERT (m_pdb != NULL);
	ASSERT (m_pdb->IsOpen ()); //CHead::CDbConnection (this, _T (__FILE__), __LINE__);

	TM_SET_IO_ADDRESS_STRUCT * pParam = (TM_SET_IO_ADDRESS_STRUCT *)wParam;

	ASSERT (pParam);

//	SetImagingMode (theApp.GetImagingMode ());
//	SetImagingModeExpire (theApp.GetImagingModeExpire ());
	SetLastError (0);
	m_sDatabaseName.Format (_T ("%X"), pParam->m_lAddress);

	// Load settings here before posting WM_HEAD_PROCESSING because the message handler
	// needs the info for display purposes.
	// cgh 04d06

	LoadStandbySettings ();
	//IsOneToOnePrint (); // cache now, avoid hitting db in OnEventEOP

	/* TODO: rem
	if (m_pbIsUSB) {
		delete m_pbIsUSB;
		m_pbIsUSB = NULL;
	}
	*/

	ASSERT (m_pdb);

	if ( !m_Config.LoadSettings (* m_pdb, m_sDatabaseName ) ) {
		MsgBox (_T ("Load Config Failed"));
		return;
	}

//	while (!::PostMessage ( m_hControlWnd, WM_HEAD_PROCESSING, LOCAL_HEAD_INITIALIZING, (LPARAM) this)) Sleep (0);

	if (IsMaster ()) {
		#if defined( __NICELABEL__ ) && defined( __WINPRINTER__ )
		if (FoxjetElements::CLabelElement::IsLabelEditorPresent ()) {
			while (!::PostMessage ( m_hControlWnd, WM_HEAD_PROCESSING, STARTING_NICELABEL, (LPARAM) this)) 
				::Sleep (0);
			
			pLine->m_appNiceLabel.CreateDispatch (_T ("NICELabel.Application"));
		}
		#endif 
	}

	if (IsUSB ()) 
		m_hDeviceID = (HANDLE)theApp.GetUSBDriver ()->ClaimAddress (pParam->m_lAddress);
	else
		m_hDeviceID = theApp.GetDriver ()->ClaimAddress (pParam->m_lAddress);

	TRACEF (_T ("ClaimAddress: ") + FoxjetDatabase::ToString (pParam->m_lAddress) + _T (": ") + FoxjetDatabase::ToString ((ULONG)m_hDeviceID));

	#ifdef _DEBUG
	if (!m_hDeviceID && theApp.m_bDemo) 
		m_hDeviceID = (HANDLE)1;
	#endif //_DEBUG

	if ( m_hDeviceID ) 
	{
		int nCount = 0;
		DWORD dwError = 1;

		while (dwError && (nCount < 2)) {
			CString strFPGA = FoxjetDatabase::GetHomeDir () + CString (_T ("\\"));
			
			if (theApp.IsMPHC ()) 
				strFPGA += _T ("mphcv9r7.bit"); 
			else
				strFPGA += _T ("2465162g.bit");
				
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, GlobalVars::strDriverRegSection, _T ("FPGA"), strFPGA);
			
			if (IsUSB ()) {
				USB::Registers r;
				
				dwError = 0;//theApp.GetUSBDriver ()->ProgramFPGA ((UINT)m_hDeviceID, w2a (strFPGA));
				//TRACEF (_T ("TODO: ProgramFPGA: ") + strFPGA); dwError = 0;

				if (theApp.GetUSBDriver ()->ReadAll ((UINT)m_hDeviceID, r)) 
					m_wGaVer = r._RsvReg2 [11]._Rsvd;
			}
			else
				dwError = theApp.GetDriver ()->ProgramFPGA (m_hDeviceID, strFPGA);
			
			::Sleep (10);
			nCount++;
		}

		ASSERT (dwError == 0);

		if (dwError == 0) {
			if (IsUSB ()) {
				theApp.GetUSBDriver ()->EnablePrint ((UINT)m_hDeviceID, false);
				theApp.GetUSBDriver ()->ClearPrintBuffer ((UINT)m_hDeviceID, 0, GetImageBufferSize ());
			}
			else {
				theApp.GetDriver ()->EnablePrint (m_hDeviceID, false);
				theApp.GetDriver ()->ClearPrintBuffer (m_hDeviceID);
			}

			OnHeadConfigChanged ( 0, 0 );
			
			//m_pSerialData = (tagSerialData *)SendControlMessage (WM_CONTROL_VIEW_GET_SERIAL_DATA_PTR, (WPARAM)&GetProductionLine ());

			// Allocate Image Buffer;
			try 
			{
				m_pImageBuffer = new unsigned char [ GetImageBufferSize () ];

				memset (m_pImageBuffer, 0x00, GetImageBufferSize ());
			}
			catch ( CMemoryException *e ) { HANDLEEXCEPTION (e); return; }

			if (theApp.IsMPHC ()) {
				if (IsUSB ()) {
					USB::CUsbMphcCtrl * pDriver = theApp.GetUSBDriver ();

					pDriver->RegisterSerialEvent ((UINT)m_hDeviceID, m_hSerialEvent);
					pDriver->RegisterEOPEvent ((UINT)m_hDeviceID, m_hEOPEvent);
				}
				else {
					CMphc * pDriver = theApp.GetMPHCDriver ();

					pDriver->RegisterSerialEvent ( m_hDeviceID, m_hSerialEvent );
					pDriver->RegisterAmsEvent ( m_hDeviceID, m_hAmsFinished );	// cgh 05e25
					pDriver->RegisterEOPEvent ( m_hDeviceID, m_hEOPEvent );	// cgh 04d12
				}
			}
			else { 
				CIvPhc * pDriver = theApp.GetIVDriver ();

				pDriver->RegisterEOPEvent (m_hDeviceID, m_hEOPEvent);
				pDriver->RegisterSerialEvent ( m_hDeviceID, m_hSerialEvent );
			}

 			m_lastAMSCycle = CTime::GetCurrentTime();

			#ifdef _DEBUG
			// Start Debug Stuff
			if ( m_pDbgDlg )
				m_pDbgDlg->SetTitle ( m_sDatabaseName );
			// End Debug Stuff
			#endif
		}
		else
			SetLastError ( FPGA_PROGRAMMING_ERROR );
	}
	else {
		TRACEF (_T ("ClaimAddress failed: ") + ToString (pParam->m_lAddress));
		SetLastError ( CLAIM_ADDRESS_FAILED );
	}

	//{ CString str; str.Format (_T ("%s: pParam->m_hEvent: 0x%p"), GetFriendlyName (), pParam->m_hEvent); TRACEF (str); }

	::SetEvent (pParam->m_hEvent);
	delete pParam;

	while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_ACTIVATION, (LPARAM)this)) ::Sleep (0);
	while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM)this))			::Sleep (0);
}

static DIAGNOSTIC_CRITICAL_SECTION (csSettings);

void CLocalHead::ReadInkCode (const CString & strCode)
{
	const CString strRootSection = FoxjetDatabase::Encrypted::InkCodes::GetRegSection ();
	long double dPixel = FoxjetDatabase::CalcInkUsage (m_Config.m_HeadSettings.m_nHeadType, FoxjetDatabase::INKTYPE_FIRST, 1);

	//if (m_Config.m_HeadSettings.m_bDoublePulse)
	//	dPixel *= 2;

	m_inkcode.m_bEnabled	= FoxjetDatabase::Encrypted::InkCodes::IsEnabled ();
	m_inkcode.m_lMax		= (unsigned __int64)((long double)500.0 / dPixel);
	TRACEF (std::to_string (dPixel).c_str ());
	TRACEF (std::to_string (m_inkcode.m_lMax).c_str ());
	//m_inkcode.m_lUsed		= 0;

	// 500,000,000,000 / 120 = 4166666666 [pixels per 500ml bottle]

	if (m_inkcode.m_bEnabled) {
		CString strKey = m_inkcode.m_strCode;

		if (strCode.GetLength ())
			strKey = strCode;

		if (strKey.Find (_T ("<NULL>")) == -1)
			strKey += _T ("<NULL>");

		unsigned __int64 lUsed = Encrypted::GetProfileInt64 (strRootSection + _T ("\\") + strKey, _T ("Used"), (unsigned __int64)0);

		TRACEF (_T ("[ink code] ") + GetFriendlyName () + _T (" [") + m_inkcode.m_strCode + _T ("] lUsed = ") + ToString (lUsed));

		strKey.Replace (_T ("<NULL>"), _T (""));

		m_inkcode.m_lHeadID		= m_Config.m_HeadSettings.m_lID;
		m_inkcode.m_lUsed		= lUsed;
		m_inkcode.m_strCode		= strKey;
	}
}

void CLocalHead::OnHeadConfigChanged(UINT wParam, LONG lParam)
{
	ASSERT (lParam == 0);

	m_params.m_vLines.RemoveAll  ();
	m_params.m_listHeads.RemoveAll ();

	if (CONFIGCHANGESTRUCT * pConfig = (CONFIGCHANGESTRUCT *)wParam) {
		m_params.m_vLines.Copy (pConfig->m_vLines);
		m_params.m_pSw0848Thread = pConfig->m_pSw0848Thread;

		for (POSITION pos = pConfig->m_listHeads.GetStartPosition(); pos; ) {
			ULONG lKey;
			CHead * pLookup = NULL;

			pConfig->m_listHeads.GetNextAssoc (pos, lKey, (void *&)pLookup);
			m_params.m_listHeads.SetAt (lKey, pLookup);
		}

		m_params.m_serialData.m_Len = pConfig->m_serialData.m_Len;
		memcpy (m_params.m_serialData.Data, pConfig->m_serialData.Data, ARRAYSIZE (m_params.m_serialData.Data) * sizeof (m_params.m_serialData.Data [0]));
	}

	ASSERT (m_pdb != NULL);
	ASSERT (m_pdb->IsOpen ()); //CHead::CDbConnection (this, _T (__FILE__), __LINE__);

	LOCK (::csSettings);
	//DECLARETRACECOUNT (200);

	FreeCommSettings ();
	ReadInkCode ();

	if (m_inkcode.m_strCode.GetLength ()) {
		const CString strRootSection = FoxjetDatabase::Encrypted::InkCodes::GetRegSection ();

		VERIFY (Encrypted::WriteProfileInt64 (strRootSection + _T ("\\") + m_inkcode.m_strCode, _T ("Max"), m_inkcode.m_lMax));
	}

	//MARKTRACECOUNT ();

	{
		ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);
		
		m_vSlaves.RemoveAll ();
		//TRACEF ("m_vSlaves.RemoveAll ();");
	}

	if (!IsUSB ()) {
		SETTINGSSTRUCT s;

		m_motherboard	= MOTHERBOARD_REV3;
		m_lCardTimeout	= 30;
		m_lWDT			= 6;
		m_bWDT			= false;

		ASSERT (m_pdb);

		if (GetSettingsRecord (* m_pdb, 0, _T ("Watchdog"), s)) {
			CStringArray v;

			FoxjetDatabase::FromString (s.m_strData, v);

			m_motherboard	= (MOTHERBOARDTYPE)_ttoi (GetParam (v, 0, FoxjetDatabase::ToString (MOTHERBOARD_REV3)));
			m_lCardTimeout	= _ttoi (GetParam (v, 1, _T ("30")));
			m_lWDT			= _ttoi (GetParam (v, 2, _T ("6")));
			m_bWDT			= _ttoi (GetParam (v, 3, _T ("0"))) ? true : false;
		}

		if (IsWDTEnabled ()) 
			ProgramWDT ();
		else
			DisableWDT ();
	}

	if (FoxjetCommon::IsProductionConfig ()) {
		if (IsUSB ())
			theApp.GetUSBDriver ()->ClearPrintBuffer ((UINT)m_hDeviceID, 0, GetImageBufferSize ());
		else
			theApp.GetDriver ()->ClearPrintBuffer (m_hDeviceID);
	}

	{
		CArray <FoxjetCommon::CStrobeItem, FoxjetCommon::CStrobeItem &> v;

		ASSERT (m_pdb);
		FoxjetCommon::CStrobeDlg::Load (* m_pdb, v);

		for (int i = 0; i < v.GetSize (); i++) {
			FoxjetCommon::CStrobeItem & e = v [i];

			if (e.GetErrorType (e.m_strName) == HEADERROR_APSCYCLE)
				m_nApsDelay = e.m_nDelay;
		}
	}

	CHead::OnHeadConfigChanged (wParam, lParam);

	//TRACEF (GetFriendlyName () + ": OnHeadConfigChanged");

	CString sEvent;
	const FoxjetDatabase::HEADTYPE typeOld = m_Config.m_HeadSettings.m_nHeadType;

	ASSERT (m_pdb);
	m_Config.LoadSettings (* m_pdb, m_sDatabaseName );
	LoadStandbySettings ();

	CDiagnosticSingleLock::RegisterThreadName (_T ("CLocalHead:") + m_Config.m_HeadSettings.m_strUID, m_nThreadID);
	//m_csImage.m_strName		= _T ("CLocalHead::m_csImage:")	+ m_Config.m_HeadSettings.m_strUID;
	//m_csConfig.m_strName	= _T ("CLocalHead::m_csConfig:") + m_Config.m_HeadSettings.m_strUID;
	m_vCache.m_cs.m_strName = _T ("CHead::m_vCache.m_cs:") + m_Config.m_HeadSettings.m_strUID;

	FindSlaves ();

	m_bShowPrintCycle	= theApp.ShowPrintCycle ();

	if (m_Config.m_HeadSettings.m_nHeadType != typeOld)
		m_pImageEng->Free ();

	m_nAmsInterval = (int)(m_Config.m_Line.m_dAMS_Interval * 60.0);

	//MARKTRACECOUNT ();

	if ( m_Config.IsDisabled() )
	{
		m_bDisabled = true;
		m_eTaskState = DISABLED;

		if (IsUSB ())
			theApp.GetUSBDriver ()->EnablePrint ((UINT)m_hDeviceID, false);
		else
			theApp.GetDriver ()->EnablePrint (m_hDeviceID, false);
	}
	else
	{
		m_bDisabled = false;
		if ( m_eTaskState == DISABLED )
			m_eTaskState = STOPPED;
	}

	// Parse and set Serial port parameters
	tagSerialParams params;
	CString sParams = m_Config.GetHeadSettings().m_strSerialParams;

	int loc = sParams.Find (',', 0 );
	params.BaudRate = _ttol ( sParams.Left ( loc ));
	sParams = sParams.Right ( sParams.GetLength() - (loc + 1) );

	loc = sParams.Find (',', 0 );
	params.DataBits = _ttoi ( sParams.Left ( loc ));
	sParams = sParams.Right ( sParams.GetLength() - (loc + 1) );

	loc = sParams.Find (',', 0 );
	switch (sParams.Left ( loc ).GetBuffer(1)[0] )
	{
		case 'N':
			params.Parity = NO_PARITY;
			break;
		case 'O':
			params.Parity = ODD_PARITY;
			break;
		case 'E':
			params.Parity = EVEN_PARITY;
			break;
	}
	sParams = sParams.Right ( sParams.GetLength() - (loc + 1) );
	
	loc = sParams.Find (',', 0 );
	params.StopBits = _ttoi ( sParams.Left ( loc ));

	sParams = sParams.Right ( sParams.GetLength() - (loc + 1) );
	m_SerialDevice = ( eSerialDevice ) _ttoi ( sParams );

	if (theApp.IsMPHC ()) {
		if (IsUSB ()) {
			USB::CUsbMphcCtrl * pDriver = theApp.GetUSBDriver ();
			USB::SetupRegs CfgRegs;
			struct USB::FireVibPulse tPulseRegs;

			memset (&CfgRegs, 0, sizeof (CfgRegs));
			memset (&tPulseRegs, 0, sizeof (tPulseRegs));

			//MARKTRACECOUNT ();
			m_Config.ToRegisters (CfgRegs, tPulseRegs, GetInfo ());
			//MARKTRACECOUNT ();
			bool bWriteSetup = pDriver->WriteSetup ((UINT)m_hDeviceID, CfgRegs, tPulseRegs);
			if (!bWriteSetup) TRACEF (_T ("********** WriteSetup failed: ") + GetFriendlyName ());
			//MARKTRACECOUNT ();
		}
		else {
			CMphc * pDriver = theApp.GetMPHCDriver ();
			tagSetupRegs CfgRegs;
			tagFireVibPulse tPulseRegs;
			tagAMSRegs AmsRegs;
	
			m_Config.ToRegisters (CfgRegs, tPulseRegs, GetInfo ());
			m_Config.GetAMSValues (AmsRegs);
			pDriver->ProgramAmsRegs ( m_hDeviceID, AmsRegs);
			pDriver->WriteSetup ( m_hDeviceID, CfgRegs, tPulseRegs);
			pDriver->SetProductionMode (m_hDeviceID, FoxjetCommon::IsProductionConfig ());
		}

		FoxjetDatabase::HEADSTRUCT HS = m_Config.GetHeadSettings();

		m_wPrintDly = CalcPrintDelay (HS.m_lPhotocellDelay);

		{ 
			CString str; 
			str.Format (_T ("%s(%d): %s: lHeadID: %d, lOffset: %d, wPrintDly: %d\n"), 
				_T (__FILE__), __LINE__, 
				GetFriendlyName (), HS.m_lID, HS.m_lPhotocellDelay, m_wPrintDly); 
			TRACER (str); 
		}

		if (IsUSB ()) {
			USB::tagSerialParams p;
			USB::CUsbMphcCtrl * pDriver = theApp.GetUSBDriver ();

			p.BaudRate	= params.BaudRate;
			p.DataBits	= params.DataBits;
			p.Parity	= params.Parity;
			p.StopBits	= params.StopBits;

			//MARKTRACECOUNT ();
			pDriver->SetSpeedGateWidth ((UINT)m_hDeviceID, 300);
			//MARKTRACECOUNT ();
			pDriver->SetPrintDelay ((UINT)m_hDeviceID, m_wPrintDly);
			//MARKTRACECOUNT ();
			pDriver->SetSerialParams ((UINT)m_hDeviceID, p);
			//MARKTRACECOUNT ();
		}
		else {
			CMphc * pDriver = theApp.GetMPHCDriver ();

			pDriver->SetSpeedGateWidth ( m_hDeviceID, ( HS.m_nHorzRes ) );
			pDriver->SetPrintDelay ( m_hDeviceID, m_wPrintDly);
			pDriver->SetSerialParams ( m_hDeviceID, params );
		}
	}
	else { 
		CIvPhc * pDriver = theApp.GetIVDriver ();
		IVCARDCONFIGSTRUCT config;

		GetConfig (config);
		pDriver->SetConfig (m_hDeviceID, config);

		if (_tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16) == 0x300) {
			pDriver->SetSerialParams (m_hDeviceID, params);
	
			//#ifdef _DEBUG
			pDriver->EnableDebug (m_hDeviceID, m_bDebug = theApp.OutputToDbgView ());
			//#endif
		}
	}

	if (IsUSB ()) {
		//MARKTRACECOUNT ();
		theApp.GetUSBDriver ()->UnRegisterPhotoEvent ((UINT)m_hDeviceID);
		//MARKTRACECOUNT ();
		theApp.GetUSBDriver ()->UnRegisterEOPEvent ((UINT)m_hEOPEvent);
		//MARKTRACECOUNT ();
	}
	else {
		theApp.GetDriver ()->UnregisterPhotoEvent (m_hDeviceID);
		theApp.GetDriver ()->UnregisterEOPEvent (m_hEOPEvent);
	}

	if (m_hPhotoEvent) {
		::CloseHandle (m_hPhotoEvent);
		m_hPhotoEvent = NULL;
	}

	if (m_hEOPEvent) {
		::CloseHandle (m_hEOPEvent);
		m_hEOPEvent = NULL;
	}

	sEvent.Format (_T ("EndOfPrint %lu"), m_hThread );
	m_hEOPEvent = CreateEvent ( NULL, true, false, sEvent);

	switch ( m_Config.m_HeadSettings.m_nPhotocell )
	{
		case EXTERNAL_PC:	// External
			switch ( m_Config.m_HeadSettings.m_nSharePhoto )
			{
				default:
				case 0:	// Not Shared
					if (IsUSB ()) {
						sEvent.Format (_T ("ExtPhoto%d %lu"), m_Config.m_HeadSettings.m_nSharePhoto, m_nThreadID);
						//TRACEF (GetFriendlyName () + _T (": CreateEvent: ") + sEvent);
						m_hPhotoEvent = CreateEvent ( NULL, true, false, sEvent);
					}
					break;
				case 1:	// Shared as A or 1
					sEvent.Format (_T ("ExtPhotoA %lu"), m_nThreadID );
					//TRACEF (GetFriendlyName () + _T (": CreateEvent: ") + sEvent);
					m_hPhotoEvent = CreateEvent ( NULL, true, false, sEvent);
					break;
				case 2:	// Shared as B or 2
					sEvent.Format (_T ("ExtPhotoB %lu"), m_nThreadID );
					//TRACEF (GetFriendlyName () + _T (": CreateEvent: ") + sEvent);
					m_hPhotoEvent = CreateEvent ( NULL, true, false, sEvent);
					break;
			}
			if (theApp.IsMPHC ()) {
				if (IsUSB ()) {
					USB::CUsbMphcCtrl * pDriver = theApp.GetUSBDriver ();

					//MARKTRACECOUNT ();
					pDriver->RegisterPhotoEvent ((UINT)m_hDeviceID, m_hPhotoEvent);
					//MARKTRACECOUNT ();
					pDriver->RegisterEOPEvent ((UINT)m_hDeviceID, m_hEOPEvent);	
					//MARKTRACECOUNT ();
				}
				else {
					CMphc * pDriver = theApp.GetMPHCDriver ();
					
					pDriver->RegisterPhotoEvent (m_hDeviceID, m_hPhotoEvent);
					pDriver->RegisterEOPEvent (m_hDeviceID, m_hEOPEvent);	
				}
			}
			else {
				CIvPhc * pDriver = theApp.GetIVDriver ();
				bool bShared = m_Config.m_HeadSettings.m_nPhotocell == SHARED_PC;

				if (!bShared) {
					pDriver->RegisterPhotoEvent (m_hDeviceID, m_hPhotoEvent);
					pDriver->RegisterEOPEvent (m_hDeviceID, m_hEOPEvent);	
				}
			}
			break;
		case INTERNAL_PC:	// Internal
			switch ( m_Config.m_HeadSettings.m_nSharePhoto )
			{
				default:
				case 0:	// Not Shared
					if (IsUSB ()) {
						sEvent.Format (_T ("IntPhoto%d %lu"), m_Config.m_HeadSettings.m_nSharePhoto, m_nThreadID);
						//TRACEF (GetFriendlyName () + _T (": CreateEvent: ") + sEvent);
						m_hPhotoEvent = CreateEvent ( NULL, true, false, sEvent);
					}
					break;
				case 1:	// Shared as A or 1
					sEvent.Format (_T ("IntPhotoA %lu"), m_nThreadID );
					//TRACEF (GetFriendlyName () + _T (": CreateEvent: ") + sEvent);
					m_hPhotoEvent = CreateEvent ( NULL, true, false, sEvent);
					break;
				case 2:	// Shared as B or 2
					sEvent.Format (_T ("IntPhotoB %lu"), m_nThreadID );
					//TRACEF (GetFriendlyName () + _T (": CreateEvent: ") + sEvent);
					m_hPhotoEvent = CreateEvent ( NULL, true, false, sEvent);
					break;
			}
			if (theApp.IsMPHC ()) {
				if (IsUSB ()) {
					USB::CUsbMphcCtrl * pDriver = theApp.GetUSBDriver ();

					//MARKTRACECOUNT ();
					pDriver->RegisterPhotoEvent ((UINT)m_hDeviceID, m_hPhotoEvent);
					//MARKTRACECOUNT ();
					TRACEF (GetFriendlyName ());
					pDriver->RegisterEOPEvent ((UINT)m_hDeviceID, m_hEOPEvent);	
					//MARKTRACECOUNT ();
				}
				else {
					CMphc * pDriver = theApp.GetMPHCDriver ();
				
					pDriver->RegisterPhotoEvent (m_hDeviceID, m_hPhotoEvent);
					pDriver->RegisterEOPEvent (m_hDeviceID, m_hEOPEvent);	
				}
			}
			else {
				CIvPhc * pDriver = theApp.GetIVDriver ();
				bool bShared = m_Config.m_HeadSettings.m_nPhotocell == SHARED_PC;

				if (!bShared) {
					pDriver->RegisterPhotoEvent (m_hDeviceID, m_hPhotoEvent);
					pDriver->RegisterEOPEvent (m_hDeviceID, m_hEOPEvent);	
				}
			}
			break;
		case SHARED_PC:	// Using a shared photocell
			switch ( m_Config.m_HeadSettings.m_nSharePhoto )
			{
				default:
				case 0:
					if (IsUSB ()) {
						sEvent.Format (_T ("SlavePhoto%d %lu"), m_Config.m_HeadSettings.m_nSharePhoto, m_nThreadID);
						//TRACEF (GetFriendlyName () + _T (": CreateEvent: ") + sEvent);
						m_hPhotoEvent = CreateEvent ( NULL, true, false, sEvent);
					}
					break;
				case 1:	// Shared as A or 1
					sEvent.Format (_T ("SlavePhotoA"));
					//TRACEF (GetFriendlyName () + _T (": CreateEvent: ") + sEvent);
					m_hPhotoEvent = CreateEvent ( NULL, true, false, sEvent);
					break;
				case 2:	// Shared as B or 2
					sEvent.Format (_T ("SlavePhotoB"));
					//TRACEF (GetFriendlyName () + _T (": CreateEvent: ") + sEvent);
					m_hPhotoEvent = CreateEvent ( NULL, true, false, sEvent);
					break;
			}
			if (IsUSB ()) {
				USB::CUsbMphcCtrl * pDriver = theApp.GetUSBDriver ();

				//MARKTRACECOUNT ();
				pDriver->RegisterPhotoEvent ((UINT)m_hDeviceID, m_hPhotoEvent);
				//MARKTRACECOUNT ();
				TRACEF (GetFriendlyName ());
				pDriver->RegisterEOPEvent ((UINT)m_hDeviceID, m_hEOPEvent);	
				//MARKTRACECOUNT ();
			}
			break;
	}

	while (!::PostMessage (m_hControlWnd, WM_CALCPRODUCTLEN, m_Config.m_HeadSettings.m_lID, m_Config.m_Line.m_lID)) 
		::Sleep (0);

	m_dRemainingInk = GetInkLowVolume (m_Config.m_HeadSettings.m_nHeadType, FoxjetDatabase::INKTYPE_FIRST);

	//TRACECOUNTARRAY ();
}

void CLocalHead::OnStartTask(UINT wParam, LONG lParam)
{
	m_vSetUserElements.RemoveAll ();

#if defined( _DEBUG ) && defined( __DEBUG_BUILDIMAGE__ )
	if (IsMaster ()) {
		CStringArray v;

		FoxjetDatabase::GetFiles (_T ("C:\\Temp\\Debug\\"), _T ("*.bmp"), v);

		for (int i = 0; i < v.GetSize (); i++)
			BOOL b = ::DeleteFile (v [i]);
	}
#endif

	ULONG lTaskID = lParam;

	if ((m_lStartTaskID = (m_bPrinting ? lTaskID : -1)) != -1) {
		CHead::OnStartTask (wParam, lParam);

		return; // Run will call OnStartTask again after EOP
	}

	CString sElementData;
	CString sMsgData;
	CString sLabelData;
	FoxjetDatabase::HEADSTRUCT HS = m_Config.GetHeadSettings();
	const ULONG lAddr = _tcstoul (m_Config.m_HeadSettings.m_strUID, 0, 16);

	FindSlaves ();

	if (IsWaxHead ())
		SetStandbyMode (false);

	m_bTestPattern = lTaskID == -1;
	m_sTaskName = _T("");
	m_eTaskState = STOPPED;
	SetLastError ( 0 );

	{
		m_wPrintDly = CalcPrintDelay (HS.m_lPhotocellDelay);

		{ 
			CString str; 
			str.Format (_T ("%s(%d): %s: lHeadID: %d, lOffset: %d, wPrintDly: %d\n"), 
				_T (__FILE__), __LINE__, 
				GetFriendlyName (), HS.m_lID, HS.m_lPhotocellDelay, m_wPrintDly); 
			TRACER (str); 
		}

		if (IsUSB ()) {
			USB::CUsbMphcCtrl * pDriver = theApp.GetUSBDriver ();
			pDriver->SetPrintDelay ((UINT)m_hDeviceID, m_wPrintDly);
		}
		else {
			CMphc * pDriver = theApp.GetMPHCDriver ();

			pDriver->SetPrintDelay ( m_hDeviceID, m_wPrintDly);
		}
	}

	if ( !m_bDisabled )
	{
		if (lTaskID == -1) {
			m_Message.m_strName = m_Task.m_strName = m_sMessageName = m_sTaskName = GlobalVars::strTestPattern;
			m_Task.m_lID = -1;
			SetCounts (0);

			if (!OnPhotocell (INITIAL)) {
				TRACEF (GetFriendlyName () + _T (" !OnPhotocell (INITIAL)"));
				CHead::OnStartTask (wParam, lParam);
				return;
			}
		}
		else {
			const CString strLastTask = GlobalFuncs::GetProfileString (
				m_Config.m_Line.m_strName, GlobalVars::strTask, _T (""));
			bool bForceDownload = false;

			//FoxjetCommon::LoadSystemParams (m_Database, GlobalVars::CurrentVersion);

			m_sMessageName	= m_Message.m_strName;
			m_sTaskName		= m_Task.m_strName;

			if (IsGojo () || theApp.GetProfileInt (_T ("sw0858"), _T ("db start"), 0))
				m_strKeyValue = theApp.GetProfileString (_T ("sw0858"), _T ("Key value"));
			//else
			//	m_strKeyValue = _T ("");

			if (lAddr == 0x310 && m_Config.m_HeadSettings.m_nPhotocell != SHARED_PC)
				bForceDownload = true;
			
			if (!OnPhotocell (INITIAL), bForceDownload) {
				if (!bForceDownload) {
					TRACEF (GetFriendlyName () + _T (" !OnPhotocell (INITIAL)"));
					CHead::OnStartTask (wParam, lParam);
					return;
				}
			}

			if (theApp.GetImagingMode () == IMAGE_ON_EOP && m_Config.m_HeadSettings.m_nPhotocell != SHARED_PC) {
				OnBuildImage ((WPARAM)MAKEIMGPARAMS (IMAGING, 0), BUILDIMAGE_OVERRIDE);
				OnDownloadImageSegment (0, 0);
			}

			BuildDynamicElemList (m_pElementList);
		}
		
		m_nInkWarning = INK_CODE_WARNGING_LEVEL_0;
		CheckInkCode ();
		m_eTaskState = RUNNING;
	}
	else {
		m_pImageEng->Free ();
		m_eTaskState = DISABLED;
	}

	CHead::OnStartTask ( wParam, lParam );
	m_Status.m_lCountUntil = -1;

	while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this)) 
		::Sleep(0);

	if (m_strWDTFile.IsEmpty ())
		m_strWDTFile = GetWDTFile ();
	
	if (IsMaster ())
		theApp.PostViewRefresh (1000);
}

void CLocalHead::SetCountUntil (__int64 lCountUntil)
{
	const CString strFile = GetHomeDir () + _T ("\\XML\\") + m_Config.m_Line.m_strName + _T ("\\_$CountUntil.txt");

	m_Status.m_lCountUntil = lCountUntil;

	if (IsMaster ()) {
		if (lCountUntil > 0)
			FoxjetFile::WriteDirect (strFile, FoxjetCommon::FormatI64 (lCountUntil));
		else
			::DeleteFile (strFile);
	}
}

void CLocalHead::SetCounts (const CHead::CHANGECOUNTSTRUCT & countIn)
{
	Head::CElementPtrArray v;

	m_Status.m_lCount = countIn.m_lCount;
	GetElements (v, COUNT);

	//TRACER (ToString (countIn.m_lCount));

	for (int i = 0; i < v.GetSize (); i++) {
		#ifdef __WINPRINTER__

		if (CCountElement * p = DYNAMIC_DOWNCAST (CCountElement, v [i])) {
			CHead::CHANGECOUNTSTRUCT count = countIn;

			if (p->IsIrregularPalletSize ()) {
				p->IncrementTo (count.m_lCount);

				if (p->IsPalletCount ()) {
					if ((count.m_lPalletCount > count.m_lPalletMax) && count.m_lPalletMax)
						count.m_lPalletCount = p->GetRollover () ? (count.m_lPalletMin - 1) + (count.m_lPalletCount % count.m_lPalletMax) : count.m_lPalletMax;

					if (count.m_lPalletCount > 0) 
						p->SetPalCount (count.m_lPalletCount);

					if (count.m_lPalletMin > 0)
						p->SetPalMin (count.m_lPalletMin);

					if (count.m_lPalletMax > 0)
						p->SetPalMax (count.m_lPalletMax);

					if (count.m_lUnitsPerPallet > 0)
						p->SetMax (count.m_lUnitsPerPallet);
				}
			}
			else {
				if (p->IsPalletCount ()) {
					if (count.m_lPalletCount > 0)
						p->SetPalCount (count.m_lPalletCount);

					if (count.m_lPalletMin > 0)
						p->SetPalMin (count.m_lPalletMin);

					if (count.m_lPalletMax > 0)
						p->SetPalMax (count.m_lPalletMax);
				}

				if (count.m_lPalletMax > 0)
					p->SetPalMax (count.m_lPalletMax); //sw0844

				if (p->IsPalletCount () && count.m_lUnitsPerPallet > 0)
					p->SetMax (count.m_lUnitsPerPallet); //sw0844

				p->IncrementTo (count.m_lCount);
			}

			//TRACER (ToString (p->GetCount ()));
			//int n = 0;
		}
		else if (FoxjetIpElements::CCountElement * p = DYNAMIC_DOWNCAST (FoxjetIpElements::CCountElement, v [i])) {
			p->SetCurValue (p->GetStart ());

			for (__int64 i = 1; i < countIn.m_lCount; i++)
				p->Increment ();

			TRACER (ToString (p->GetStart ()) + _T (": ") + ToString (p->GetCurValue ()));
			//int nBreak = 0;
		}

		
		#endif //__WINPRINTER__
	}

	CHead::SetCounts (countIn);

	if (theApp.GetImagingMode () == IMAGE_ON_EOP) {
		int nFlags = BUILDIMAGE_OVERRIDE;

		AbortImgRefreshMsgs ();

		while (!::PostThreadMessage (m_nThreadID, TM_BUILD_IMAGE, (WPARAM)MAKEIMGPARAMS (IMAGING, 0), nFlags))
			::Sleep (0);
		//OnBuildImage ((WPARAM)MAKEIMGPARAMS (IMAGING, 0), nFlags);
	}
}

void CLocalHead::Stop ()
{
	m_vSetUserElements.RemoveAll ();
	m_sTaskName = _T("");
	m_eTaskState = STOPPED;

	m_bPhotoEnabled = false;

	if ( m_bPrinting == false ) {
		if (IsUSB ()) {
			theApp.GetUSBDriver ()->EnablePrint ((UINT)m_hDeviceID, m_bPhotoEnabled);
			theApp.GetUSBDriver ()->ClearPrintBuffer ((UINT)m_hDeviceID, 0, GetImageBufferSize ());
		}
		else {
			theApp.GetDriver ()->EnablePrint (m_hDeviceID, m_bPhotoEnabled);
			theApp.GetDriver ()->ClearPrintBuffer (m_hDeviceID);
		}
	}
	// else // this will be handled in end-of-print event;
	//	theApp.GetDriver ()->EnablePrint ( m_hDeviceID, m_bPhotoEnabled );

	FreeDynamicElemList ();

	CHead::OnStopTask (m_eop.m_wParam, m_eop.m_lParam);
}

void CLocalHead::OnStopTask(UINT wParam, LONG lParam)
{
	if ( !m_bDisabled )
	{
		m_eop.m_wParam	= wParam;
		m_eop.m_lParam	= lParam;

		if (theApp.IsMPHC ())
			Stop ();
		else {
			if (!m_bPrinting)
				Stop ();
			else {
				m_eop.m_bStop = true;
				return;
			}
		}
	}
	else
	{
		m_sTaskName = _T("");
		m_eTaskState = DISABLED;
	}
}

void CLocalHead::OnIdleTask(UINT wParam, LONG lParam)
{
	m_bPhotoEnabled = false;

	if (m_bPrinting == false) {
		if (IsUSB ())
			theApp.GetUSBDriver ()->EnablePrint ((UINT)m_hDeviceID, m_bPhotoEnabled);
		else
			theApp.GetDriver ()->EnablePrint (m_hDeviceID, m_bPhotoEnabled);
	}
	// else // this will be handled in end-of-print event;
	
	if ( m_eTaskState == RUNNING )
		m_eTaskState = IDLE;
	CHead::OnIdleTask ( wParam, lParam);
}

void CLocalHead::OnResumeTask(UINT wParam, LONG lParam)
{
	bool bEnablePhoto = wParam ? true : false;

	if (IsWaxHead ())
		SetStandbyMode (false);

	if (bEnablePhoto) {
		m_bPhotoEnabled = true;

		if (IsUSB ())
			theApp.GetUSBDriver ()->EnablePrint ((UINT)m_hDeviceID, m_bPhotoEnabled);
		else
			theApp.GetDriver ()->EnablePrint (m_hDeviceID, m_bPhotoEnabled);
	}

	if ( m_eTaskState == IDLE )
		m_eTaskState = RUNNING;
	CHead::OnResumeTask ( wParam, lParam);
}

void CLocalHead::OnEventPhoto (UINT wParam, LONG lParam)
{
DECLARETRACECOUNT (200);
MARKTRACECOUNT ();
	const DWORD dwWait = 20;
	bool bUpdatePreview = false; 

	//TRACEF (_T ("OnEventPhoto: ") + GetFriendlyName ());

	//ASSERT (pLine);

	//if ((m_eTaskState == RUNNING) && (!m_bDisabled)){
	if ((m_eTaskState == RUNNING) && (!m_bDisabled) && m_bPhotoEnabled) {
		if (theApp.GetImagingMode () == IMAGE_ON_PHOTOCELL) {
MARKTRACECOUNT ();
			OnBuildImage ((WPARAM)MAKEIMGPARAMS (IMAGING, 0), 0);
MARKTRACECOUNT ();
			Increment ();
MARKTRACECOUNT ();
		}
		else {
			if (theApp.IsIV ())
				theApp.GetIVDriver ()->EnablePrint (m_hDeviceID, true);
		}

		m_bPrinting = true;
		UPDATESTATE ();
MARKTRACECOUNT ();

		if (m_Status.m_nInkLevel == LOW_INK) {
			long double dUsage = m_pImageEng->GetInkUsage ();

			static const int nAccel = _ttoi (FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), _T ("/accel=")));
					
			if (nAccel > 0)
				dUsage *= nAccel;

			#if defined( __DEBUG_LOW_INK_CODE__ )
			dUsage *= 100.0;
			#endif

			const long double dRemainingInk = m_dRemainingInk - dUsage;
			TRACER (m_Config.m_HeadSettings.m_strName + _T (": ") + a2w (std::to_string (m_dRemainingInk).c_str ()) + _T (" [- ") + a2w (std::to_string (m_dRemainingInk).c_str ()) + _T ("]"));

			/*
			#ifdef _DEBUG
			{
				CString str;
				int n = (int)floor (m_dRemainingInk / dUsage);

				str.Format ("%d remaining [%f / %f]", n, m_dRemainingInk, dUsage);
				TRACEF (str);
			}
			#endif
			*/

			if (dRemainingInk <= 0.0) {
				ULONG lLineID = m_Config.m_Line.m_lID;

				m_Status.m_nInkLevel = INK_OUT;
			}

			m_dRemainingInk = dRemainingInk;
		}

		m_Status.m_lCount++;
		m_countLast.m_lCount = m_Status.m_lCount;
		//TRACEF (GetFriendlyName () + _T (": ") + FoxjetDatabase::ToString (m_Status.m_lCount));

		if (m_inkcode.m_bEnabled) {
			unsigned __int64 lPixels = m_pImageEng->GetPixelCount ();
			//long double dInkUsage = m_pImageEng->GetInkUsage ();

			if (m_Config.m_HeadSettings.m_bDoublePulse)
				lPixels *= 2;

			//#ifdef _DEBUG
			if (lPixels) {
				int nPrints = m_inkcode.m_lMax / lPixels;
				TRACEF (_T ("[ink code] ") + GetFriendlyName () + _T (": ") + ToString ((__int64)(500.0 / 0.001 * 1000000.0)) + _T (" ml, ") + ToString (lPixels) + _T (" pixels, ") + ToString (m_inkcode.m_lMax) + _T (" max, ") + ToString (nPrints) + _T (" prints"));

				// 500,000,000,000 / 120 = 4166666666 [pixels per 500ml bottle]

				// editor, 384 Sample: 23,194 prints, 0.021557 mililiters per print (double pulsed)

				// 500000000000 / 120 = 4166666666	// pixels per bottle // single pulse
				// 500000000000 / 240 = 2083333333	// pixels per bottle // double pulse

				// 89819  pixels

				// 4166666666 / 89819 = 46,389 prints	// single pulse
				// 2083333333 / 89819 = 23,194 prints	// double pulse

				int nBreak = 0;
			}
			//#endif //_DEBUG


			static const int nAccel = _ttoi (FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), _T ("/accel=")));
					
			if (nAccel > 0)
				lPixels *= nAccel;

			m_inkcode.m_lUsed += (unsigned __int64)lPixels;

			bool b = FoxjetDatabase::Encrypted::InkCodes::IsEnabled ();
			CString strCode = m_inkcode.m_strCode;

			strCode.Trim ();
		
			if (strCode.GetLength ()) {
				INKCODE * pCode = new INKCODE;

				pCode->m_lMax		= m_inkcode.m_lMax;
				pCode->m_lUsed		= m_inkcode.m_lUsed;
				pCode->m_lHeadID	= m_Config.m_HeadSettings.m_lID;
				pCode->m_strCode	= strCode;

				//TRACEF (m_inkcode.m_strCode);

				TRACEF (_T ("[ink code] ") + GetFriendlyName () + _T (" [") + m_inkcode.m_strCode + _T ("] lUsed: ") + ToString (m_inkcode.m_lUsed)
					+ _T (": lPixels = ") + ToString ((__int64)lPixels) /* + _T (", dInkUsage: ") + ToString ((double)dInkUsage) */ );

				TRACEF (_T ("[ink code] ") + pCode->m_strCode + _T (", ") + ToString ((__int64)pCode->m_lUsed) + _T (" / ") + ToString ((__int64) + pCode->m_lMax));

				while (!::PostMessage (m_hControlWnd, WM_INK_USAGE, (LPARAM)pCode, 0))
					::Sleep (0);
			}

			CheckInkCode ();
		}

		if (m_Status.m_lCountUntil > 0) {
			if (!m_bPrintDriver) {
				if (m_Status.m_lCount >= m_Status.m_lCountUntil) {
					while (!::PostMessage (m_hControlWnd, WM_IDLE_TASK, m_Config.m_Line.m_lID, (LPARAM)this))
						::Sleep (0);
				}
			}
			else {
				if ((m_Status.m_lCountUntil) < 16000 && (m_Status.m_lCount >= m_Status.m_lCountUntil)) {
					while (!::PostMessage (m_hControlWnd, WM_IDLE_TASK, m_Config.m_Line.m_lID, (LPARAM)this))
						::Sleep (0);
				}
			}
		}

MARKTRACECOUNT ();
		if (IsMaster () && (m_sTaskName.CompareNoCase (GlobalVars::strTestPattern) != 0)) {
			CControlView::CPhotocellCount * pData = new CControlView::CPhotocellCount (m_Status.m_lCount, m_Config.m_Line.m_lID, m_Config.m_Line.m_strName);

			pData->m_count = m_countLast;

			while (!::PostMessage (m_hControlWnd, WM_POSTPHOTOCELL, 0, (LPARAM)pData))
				::Sleep (0);
		}

MARKTRACECOUNT ();

		Sw0848Thread::CSw0848Thread * pSw0848Thread	= m_params.m_pSw0848Thread;

		if (IsMaster () && (m_Config.m_Line.m_lID && pSw0848Thread)) {
			using namespace FoxjetElements;

			Sw0848Thread::CPostPhotocell * pData = new Sw0848Thread::CPostPhotocell ();

			// timestamp
			pData->m_v.Add (Sw0848Thread::CPair (CSw0848FieldsDlg::m_strTimestamp, CTime::GetCurrentTime ().Format (_T ("%c"))));

			for (int i = 0; i < m_vDatabaseElements.GetSize (); i++) {
				CDatabaseElement * p = (CDatabaseElement *)m_vDatabaseElements [i];
				Sw0848Thread::CPair data;

				data.m_strField.Format (_T ("[%s].[%s]"), p->GetTable (), p->GetField ());
				data.m_strData = p->GetImageData ();

				pData->m_v.Add (data);
			}
MARKTRACECOUNT ();

			for (int i = 0; i < m_sw0848Def.m_v.GetSize (); i++) {
				if (Sw0848Thread::CSw0848Thread::FindField (m_sw0848Def.m_v [i].m_strField, pData->m_v) == -1) {
					Sw0848Thread::CPair data;

					data.m_strField = m_sw0848Def.m_v [i].m_strField;
					data.m_strData = m_sw0848Def.m_v [i].m_strData;

					//TRACEF ("DEFAULT: " + data.m_strField + " --> " + data.m_strData);
					pData->m_v.Add (data);
				}
			}
MARKTRACECOUNT ();

			while (!::PostThreadMessage (pSw0848Thread->m_nThreadID, TM_POSTPHOTOCELL, 0, (LPARAM)pData))
				::Sleep (0);
		}

		#if __CUSTOM__ == __SW0828__
		bUpdatePreview = true;
		#endif

		#if __CUSTOM__ == __SW0828__
		{
			CString str, strCount;
			
			m_Status.m_lLifetimeCount++;
			m_Status.m_lTotalBoxCount++;	

			str.Format (_T ("Software\\FoxJet\\%s"), FoxjetCommon::GetAppTitle ());
			strCount.Format (_T ("%p"), m_Status.m_lLifetimeCount);
			FoxjetDatabase::WriteProfileString (str, _T ("m_lLifetimeCount"), strCount);
		}
		#endif
MARKTRACECOUNT ();

  		while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this))
			::Sleep (0);
	}

	if (IsMaster () || m_Config.m_HeadSettings.m_nPhotocell != SHARED_PC) {
		DECLARETRACECOUNT (200);
		ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);
		const DWORD dwTimeout = 2000;
		CHeadPtrArray & v = m_vSlaves;

		{
			bool bSend = true;

			#ifdef _DEBUG
			if (theApp.m_bDemo && m_eTaskState == STOPPED)
				bSend = false;
			#endif

			if (bSend) {
				__int64 * pCount = new __int64;
				* pCount = m_Status.m_lCount;
				WPARAM wParam = m_Config.m_HeadSettings.m_lID;
				LPARAM lParam = (LPARAM)pCount;
			
				while (!::PostMessage (m_hControlWnd, WM_SOCKET_PACKET_PRINTER_SOP, wParam, lParam)) ::Sleep (0);
			}
		}

MARKTRACECOUNT ();
		if (!IsUSB ()) {
			for (int i = 0; i < v.GetSize (); i++) {
				CHead * pSlave = v [i];
				HANDLE hEvent = NULL;

				if (theApp.GetImagingMode () == IMAGE_ON_EOP) {
					CString str;

					str.Format (_T ("TM_EVENT_PHOTO %lu"), pSlave->m_hThread);
					hEvent = ::CreateEvent (NULL, true, false, str);
				}

				while (!::PostThreadMessage (pSlave->m_nThreadID, TM_EVENT_PHOTO, (WPARAM)hEvent, 0))
					::Sleep (0);

				MARKTRACECOUNT ();

				if (hEvent) {
					bool bSignalled = ::WaitForSingleObject (hEvent, dwTimeout) == WAIT_OBJECT_0;
					MARKTRACECOUNT ();
					ASSERT (bSignalled);
					::CloseHandle (hEvent);
				}
			}
		}

MARKTRACECOUNT ();
		if (bUpdatePreview) 
  			while (!::PostMessage (m_hControlWnd, WM_REFRESH_PREVIEW, 0, (LPARAM)this))
				::Sleep (0);

		if (theApp.GetImagingModeExpire ())
			DownloadImages (v);

		//TRACECOUNTARRAYMAX ();
	}

MARKTRACECOUNT ();
	if (theApp.GetImagingMode () == IMAGE_ON_EOP) {// && !GetImagingModeExpire ()) {
		if (IsMaster () || m_Config.m_HeadSettings.m_nPhotocell != SHARED_PC) {
			ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);
			const DWORD dwTimeout = 2000;
			CHeadPtrArray & v = m_vSlaves;

			Increment ();
			OnBuildNextImage (0, 0);

			for (int i = 0; i < v.GetSize (); i++) {
				CHead * pSlave = v [i];
				HANDLE hEvent = NULL;
				CString str;

				str.Format (_T ("BuildImageOnEOP %lu"), pSlave->m_hThread);
				hEvent = ::CreateEvent (NULL, true, false, str);

				while (!::PostThreadMessage (pSlave->m_nThreadID, TM_INCREMENT, (WPARAM)hEvent, 0))
					::Sleep (0);

				bool bSignalled = ::WaitForSingleObject (hEvent, dwTimeout) == WAIT_OBJECT_0;
				ASSERT (bSignalled);

				::ResetEvent (hEvent);

				while (!::PostThreadMessage (pSlave->m_nThreadID, TM_BUILD_NEXT_IMAGE, (WPARAM)hEvent, 0))
					::Sleep (0);

				bSignalled = ::WaitForSingleObject (hEvent, dwTimeout) == WAIT_OBJECT_0;
				ASSERT (bSignalled);

				::CloseHandle (hEvent);
			}
		}
	}

MARKTRACECOUNT ();
	if (HANDLE h = (HANDLE)wParam) 
		::SetEvent (h);
MARKTRACECOUNT ();
//TRACECOUNTARRAYMAX ();
}

void CLocalHead::DownloadImages (CHeadPtrArray & v)
{
	const DWORD dwTimeout = 2000;

	// copy out all images (round-robin time slicing)
	//TRACEF (_T ("CLocalHead::DownloadImages: ") + GetFriendlyName ());

	if (IsMaster () || m_Config.m_HeadSettings.m_nPhotocell != SHARED_PC) {
		if (theApp.GetImagingMode () == IMAGE_ON_EOP) {
			int nFinished = 0;
			int nHeads = v.GetSize () + 1;
			int nSegment = 1;

			while (nFinished < nHeads) {
				if (CanDownloadImageSegment ()) {
					OnDownloadImageSegment (0, 0);
					
					if (!CanDownloadImageSegment ())
						nFinished++;
				}
				else 
					nFinished++;

				for (int i = 0; i < v.GetSize (); i++) {
					CLocalHead * pSlave = DYNAMIC_DOWNCAST (CLocalHead, v [i]);

					ASSERT (pSlave);

					if (pSlave->CanDownloadImageSegment ()) {
						CString str;

						str.Format (_T ("TM_DOWNLOAD_IMAGE_SEGMENT %lu: %d"), pSlave->m_hThread, nSegment);
						HANDLE h = ::CreateEvent (NULL, true, false, str);

						while (!::PostThreadMessage (pSlave->m_nThreadID, TM_DOWNLOAD_IMAGE_SEGMENT, 0, (ULONG)h))
							::Sleep (0);

						bool bSignalled = ::WaitForSingleObject (h, dwTimeout) == WAIT_OBJECT_0;
						ASSERT (bSignalled);
						::CloseHandle (h);

						if (!pSlave->CanDownloadImageSegment ())
							nFinished++;
					}
					else
						nFinished++;
				}

				nSegment++;
			}
		}
	}
}


void CLocalHead::OnBuildNextImage (UINT nParam, LONG lParam)
{
	/* TODO: rem
	if (IsMaster () || m_Config.m_HeadSettings.m_nPhotocell != SHARED_PC) {
		ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);
		CHeadPtrArray & v = m_vSlaves;

		Increment ();

		for (int i = 0; i < v.GetSize (); i++) 
			while (!::PostThreadMessage (v [i]->m_nThreadID, TM_INCREMENT, 0, 0))
				::Sleep (0);
	}

	while (!::PostThreadMessage (m_nThreadID, TM_BUILD_IMAGE, (WPARAM)MAKEIMGPARAMS (IMAGING, 0), 0))
		::Sleep (0);
	*/

	OnBuildImage ((WPARAM)MAKEIMGPARAMS (IMAGING, 0), 0);

	if (HANDLE h = (HANDLE)nParam)
		::SetEvent (h);
}

void CLocalHead::OnEventEOP (UINT wParam, LONG lParam)
{
	CHeadInfo info = GetInfo ();

	// NOTE: this fct may be called twice if not in demo mode (see note below)
	if (m_eop.m_bStop) {
		Stop ();
		ZeroMemory (&m_eop, sizeof (m_eop));
	}

	m_bPrinting = false;
	UPDATESTATE ();

	if (!m_bPhotoEnabled || m_eTaskState == IDLE) {
		if (IsUSB ())
			theApp.GetUSBDriver ()->EnablePrint ((UINT)m_hDeviceID, false);
		else
			theApp.GetDriver ()->EnablePrint (m_hDeviceID, false);

		if (m_eTaskState != IDLE) {
			// note that this will be called repeatedly in demo mode
			// it should only be called once on a live printer

			if (!FoxjetCommon::IsProductionConfig ()) {
				if (IsUSB ()) 
					theApp.GetUSBDriver ()->ClearPrintBuffer ((UINT)m_hDeviceID, 0, GetImageBufferSize ());
				else
					theApp.GetDriver ()->ClearPrintBuffer (m_hDeviceID);
			}
		}
	}

	if (theApp.GetImagingMode () == IMAGE_ON_EOP) {
		if (theApp.GetImagingModeExpire ()) 
			OnBuildNextImage (0, 0);
		else {
			if (IsMaster ())
				DownloadImages (m_vSlaves);
		}
	}

	if (IsMaster () || m_Config.m_HeadSettings.m_nPhotocell != SHARED_PC) {
		ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);
		CHeadPtrArray & v = m_vSlaves;
		bool bSend = true;

		#ifdef _DEBUG
		if (theApp.m_bDemo && m_eTaskState == STOPPED)
			bSend = false;
		#endif

		if (bSend) {
			__int64 * pCount = new __int64;
			* pCount = m_Status.m_lCount;
			WPARAM wParam = m_Config.m_HeadSettings.m_lID;
			LPARAM lParam = (LPARAM)pCount;

			while (!::PostMessage (m_hControlWnd, WM_SOCKET_PACKET_PRINTER_EOP, wParam, lParam)) ::Sleep (0);
		}

		for (int i = 0; i < v.GetSize (); i++)
			while (!::PostThreadMessage (v [i]->m_nThreadID, TM_EVENT_EOP, 0, 0))
				::Sleep (0);
	}

	if (IsOneToOnePrint ())  // __SW0808__
		SetSerialPending (false);

	if (IsMaster ()) {
		if (m_Status.m_lCountUntil > 0) {
			if (IsOneToOnePrint (HANDSCANNER)) {
				CString str = _T ("EOP\r");

				//str = m_Task.m_strName + _T (": ") +  ToString (m_Status.m_lCount) + _T (" [") + ToString (m_Status.m_lCountUntil) + _T ("]\r\n");
				::PostMessage (m_hControlWnd, WM_DOWNLOAD_SERIAL_DATA, (WPARAM)new CString (str), m_Config.m_Line.m_lID);
			}
		}
	}

	if (IsMaster ()) {
		CString strIDLE_AFTER_PRINT = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, NEXT::CNextThread::m_strRegSection, _T ("IDLE_AFTER_PRINT"), _T (""));

		if (!strIDLE_AFTER_PRINT.CompareNoCase (_T ("T"))) 
			IdleAll ();

		{
			CString strSCANNER_USE_ONCE = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, NEXT::CNextThread::m_strRegSection, _T ("SCANNER_USE_ONCE"), _T (""));
			
			if (!strSCANNER_USE_ONCE.CompareNoCase (_T ("T"))) {
				CString strSERIAL_PORT = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, NEXT::CNextThread::m_strRegSection, _T ("SERIAL_PORT"), _T (""));

				if (!strSERIAL_PORT.CompareNoCase (_T ("SCANNER_LABEL"))) {
					if (m_bSerialStart) {
						m_bSerialStart = false;
						StopAll ();
					}
				}
				else if (!strSERIAL_PORT.CompareNoCase (_T ("VARDATA"))) {
					CString strKey;
					ULONG lID = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, NEXT::CNextThread::m_strRegSection, _T ("SerialVarDataID"), 0);

					strKey.Format (_T ("DYN_DATA_ID[%d]"), lID);
					CString strData = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, NEXT::CNextThread::m_strRegSection, strKey, _T (""));
					VERIFY (fj_setWinDynData (lID + 1, ""));
				}
				else if (!strSERIAL_PORT.CompareNoCase (_T ("DYNMEM"))) {
					for (int i = 0; i < FJMESSAGE_DYNAMIC_SEGMENTS; i++) {
						CString strKey;
						ULONG lID = i +  1;

						strKey.Format (_T ("DYN_DATA_ID[%d]"), lID);
						CString strData = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, NEXT::CNextThread::m_strRegSection, strKey, _T (""));
						VERIFY (fj_setWinDynData (lID, ""));
					}
				}
			}
		}

		if (theApp.GetSocketSetUserElements ()) {
			CStringArray v;

			__int64 lCount = m_Status.m_lCount;
			v.Add (BW_HOST_PACKET_PRINTER_EOP);
			v.Add (m_Config.m_Line.m_strName);
			v.Add (FoxjetDatabase::ToString (lCount));

			CString str = ToString (v);
			TRACEF (str);

			::PostMessage (m_hControlWnd, WM_SOCKET_SEND, (WPARAM)new CString (str), 0);
			theApp.SetSocketSetUserElements (false);
		}
	}
}

void CLocalHead::OnIncrement (UINT wParam, LONG lParam)
{
	Increment ();

	if (HANDLE h = (HANDLE)wParam)
		::SetEvent (h);
}

void CLocalHead::OnEventSerial (UINT wParam, LONG lParam)
{
	PUCHAR pData = new UCHAR[COM_BUF_SIZE +1];
	memset ( pData, 0x00, COM_BUF_SIZE +1);
	UINT nBytes = COM_BUF_SIZE;
	bool bRead = false;
	
	if (IsUSB ())
		bRead = theApp.GetUSBDriver ()->ReadSerialData ((UINT)m_hDeviceID, pData, nBytes) ? true : false;
	else
		bRead = theApp.GetDriver ()->ReadSerialData (m_hDeviceID, pData, nBytes) ? true : false;

	if (bRead) {
		pData[nBytes] = 0x00;

		DEBUGSERIAL (GetFriendlyName () + ": " + CString (a2w (pData))); 

		CSerialPacket * p = new CSerialPacket (_T ("AUX"));
	
		p->AssignData (SERIALSRC_MPHC, m_SerialDevice, GetProductionLine(), a2w (pData), nBytes, 0);

		DEBUGSERIAL (GetFriendlyName () + ": PostMessage: " + CString (a2w (pData))); 

		while (!::PostMessage ( m_hControlWnd, WM_HEAD_SERIAL_DATA, 0, (LPARAM) p)) 
			::Sleep (0);
	}

	delete [] pData;
}

DWORD CLocalHead::GetIndicatorState (const tagIndicators & s)
{
	DWORD dw = 0;

	dw |= s.InkLow		? HEADERROR_LOWINK		: 0;
	dw |= !s.AtTemp		? HEADERROR_LOWTEMP		: 0;
	dw |= !s.HVOK		? HEADERROR_HIGHVOLTAGE : 0;

	return dw;
}

void CLocalHead::ClearErrors (DWORD dwError, CArray <CError, CError &> & vErr, CArray <CError, CError &> & vClear)
{
	// clear all errors that:
	//	1) aren't set in dwError
	//	2) aren't set in vClear

	for (int i = 0; i < vErr.GetSize (); i++) {
		CError & err = vErr [i];
		bool bError = (dwError & err.m_dwType) ? true : false;
		
		if (!bError) {
			bool bFound = false;

			for (int j = 0; j < vClear.GetSize () && !bFound; j++) {
				CError & clear = vClear [j];

				if (clear.m_dwType == err.m_dwType) {
					bFound = true;
				}
			}

			if (!bFound)
				err.Clear ();
		}
	}
}

void CLocalHead::CheckErrors (DWORD dwError, CArray <CError, CError &> & v, int nDiff, CArray <CError, CError &> * pvClear)
{
	bool bInvert = pvClear == NULL;

	for (int i = 0; i < v.GetSize (); i++) {
		CError & err = v [i];
		bool bError = (dwError & err.m_dwType) ? true : false;

		if (!bInvert ? bError : !bError) {
			if (!err.m_bSignalled) {
				err.m_lDuration += nDiff;
			
				if (err.m_lDuration > err.m_lLimit) 
					err.Signal ();
			}
		}
		else {  
			bool bClear = true;

			if (pvClear) {
				for (int j = 0; j < pvClear->GetSize (); j++) {
					CError & clear = (* pvClear) [j];

					if (!clear.m_bSignalled) {
						bClear = false;
						break;
					}
				}
			}

			if (bClear)
				err.Clear ();
		}
	}
}

void CLocalHead::ReadStatus ()
{
	const _STATUS lastStatus = m_Status;
	double dSpeed = 0;
	tagIndicators Indicators = 
	{ 
	   true,	// bool HVOK;
	   true,	// bool AtTemp;
	   false,	// bool InkLow;
	   false,	// bool HeaterOn;
	};

	if (theApp.IsMPHC ()) {
		UCHAR cRawStatus = 0;

		if (IsUSB ()) {
			USB::tagIndicators i = 
			{ 
			   true,	// bool HVOK;
			   true,	// bool AtTemp;
			   false,	// bool InkLow;
			   false,	// bool HeaterOn;
			   0.0,		// double dSpeed;
			   0,		// unsigned int nEp8Cnt
			};
			USB::CUsbMphcCtrl * pDriver = theApp.GetUSBDriver ();

			if (!theApp.m_bDemo || (theApp.m_bDemo && (m_lGetIndicators++ % 4) == 0)) {
				pDriver->GetIndicators ((UINT)m_hDeviceID, i); // TODO //, cRawStatus);

				m_lEp8 = (i.nEp8Cnt == m_lastIndicators.nEp8Cnt) ? m_lEp8 + 1 : 0;

				if (m_lEp8 > 3) {
					/*
					CTimeSpan tm = CTime::GetCurrentTime () - m_tmFPGA;

					if (tm.GetTotalSeconds () > (60 * 2)) {
						CString str;

						TRACEF (_T (" ********** ") + GetUID () + _T (": ") + ToString (m_lEp8) + _T (" [") + ToString ((ULONG)i.nEp8Cnt) + _T ("]"));

						str.Format (_T ("%s: Ep8: %d [%d]\n"), GetFriendlyName (), m_lEp8, i.nEp8Cnt);
						TRACER (str);
						
						bool bProgramFPGA = pDriver->ProgramFPGA ((UINT)m_hDeviceID);
						str.Format (_T ("%s: ProgramFPGA (0x%p): %s\n"), GetFriendlyName (), m_hDeviceID, bProgramFPGA ? _T ("true") : _T ("false"));
						TRACER (str);

						m_tmFPGA = CTime::GetCurrentTime ();
					}
					*/
				}

				m_lastIndicators = i;
			}
			else
				i = m_lastIndicators;

			//TRACEF (GetFriendlyName () + _T (": m_nStrobeColor: ") + ToString (m_nStrobeColor));
			//if (!m_Config.m_HeadSettings.m_strUID.CompareNoCase (_T ("3"))) TRACEF (GetFriendlyName () + _T (": m_nStrobeColor: ") + ToString (m_nStrobeColor));

			pDriver->SetStrobe ((UINT)m_hDeviceID, theApp.GetStrobeColor ());

			dSpeed = i.dSpeed;// * 2.0;

			//TRACEF (GetFriendlyName () + _T (": ReadLineSpeed: ") + FoxjetDatabase::ToString (dSpeed) + _T (", SetStrobe: ") + ToString (m_nStrobeColor));

			cRawStatus = 
				((i.AtTemp		? 1 : 0) << 3) |
				((i.HeaterOn	? 1 : 0) << 2) |
				((i.HVOK		? 1 : 0) << 1) |
				((i.InkLow		? 1 : 0) << 0);

			#ifdef _DEBUG
			//{ CString str; str.Format (_T ("GetIndicators: AtTemp: %d, HeaterOn: %d, HVOK: %d, InkLow: %d [0x%04X]"), i.AtTemp, i.HeaterOn, i.HVOK, i.InkLow, cRawStatus); TRACEF (str); }
			#endif

			Indicators.AtTemp	= i.AtTemp;
			Indicators.HeaterOn = i.HeaterOn;
			Indicators.HVOK		= i.HVOK;
			Indicators.InkLow	= i.InkLow;
		}
		else {
			CMphc * pDriver = theApp.GetMPHCDriver ();
			
			pDriver->ReadLineSpeed (m_hDeviceID, dSpeed);
			pDriver->GetIndicators (m_hDeviceID, Indicators, cRawStatus);
			pDriver->SetStrobe (m_hDeviceID, theApp.GetStrobeColor ());
		}

		if (cRawStatus != 0xFF) 
			m_tmCard = CTime::GetCurrentTime ();
		else {
			CTimeSpan tm = CTime::GetCurrentTime () - m_tmCard;

			#ifdef _DEBUG
			CString str;
			str.Format (_T ("WDT timeout: %d [%d]"), tm.GetTotalSeconds (), m_lCardTimeout);
			TRACEF (str);
			#endif

			if (tm.GetTotalSeconds () <= 1) {
				REPORTSTRUCT s;
				ULONG lCard = _tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16);
				CString strMotherboard = (m_motherboard == MOTHERBOARD_REV2) ? _T ("PEAK 603VL") : _T ("PCA-6774");

				s.m_dtTime		= COleDateTime::GetCurrentTime ();
				s.m_strTaskName = s.m_dtTime.Format (_T ("%c"));
				s.m_strLine		= m_Config.m_Line.m_strName;
				s.m_lPrinterID	= GetPrinterID (theApp.m_Database);
				s.m_strTaskName	= m_sTaskName;
				s.m_strCounts	= FoxjetDatabase::ToString (m_Status.m_lCount);
				s.m_strUsername.Format (_T ("WDT: 0x%X (%d, %d, %s)"), 
					lCard, 
					m_lWDT, 
					m_lCardTimeout, 
					strMotherboard);

				if (theApp.IsPrintReportLoggingEnabled ()) FoxjetDatabase::AddPrinterReportRecord (theApp.m_Database, FoxjetDatabase::REPORT_EXECSTOP, s, s.m_lPrinterID);
				TRACEF (s.m_strUsername);
				::Sleep (1000);
			}

			if ((ULONG)tm.GetTotalSeconds () > m_lCardTimeout) {
				TRACEF ("PostMessage: WM_TRIGGER_WDT");

				while (!::PostMessage (m_hControlWnd, WM_TRIGGER_WDT, 0, 0))
					::Sleep (0);
			}
		}

		if (IsWDTEnabled ()) 
			StrokeWDT ();
		
		#ifdef _DEBUG
		if (!IsUSB () && m_pDbgDlg && m_pDbgDlg->IsWindowVisible ()) {
			CMphc * pDriver = theApp.GetMPHCDriver ();
			pDriver->GetDebugFlags (m_hDeviceID, Flags);
			m_pDbgDlg->UpdateFlags (&Flags, this);
		}
		#endif
	}
	else {
		CIvPhc * pDriver = theApp.GetIVDriver ();
		IVCARDSTATESTRUCT state;

		ZeroMemory (&state, sizeof (state));
		state.m_lAddress = _tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16);
		pDriver->GetState (m_hDeviceID, state);
		pDriver->SetStrobe (m_hDeviceID, theApp.GetStrobeColor ());
		dSpeed = (double)state.m_lLineSpeed;
	}

	/*
	#ifdef _DEBUG // TODO: rem
	static const CTime tmStart = CTime::GetCurrentTime ();
	CTimeSpan tm = CTime::GetCurrentTime () - tmStart;
	int n = abs (tm.GetTotalSeconds ()) % 15;

	if (n > 3)	Indicators.AtTemp	= false;	TRACEF ("TODO: rem"); //HEADERROR_LOWTEMP; 
//	if (n > 5)	Indicators.AtTemp	= false;	//HEADERROR_LOWTEMP;
//	if (n > 5)	Indicators.HVOK		= false;	//HEADERROR_HIGHVOLTAGE;
//	if (n > 5)	Indicators.InkLow	= true;		//HEADERROR_LOWINK;
	#endif 
	*/

	/*
	#ifdef _DEBUG 
	if (CMainFrame * pFrame = (CMainFrame *)theApp.m_pMainWnd) {
		Indicators.AtTemp	= !pFrame->m_bLowTemp;	TRACEF ("TODO: rem"); //HEADERROR_LOWTEMP; 
		Indicators.HVOK		= true;	//HEADERROR_HIGHVOLTAGE;
		Indicators.InkLow	= false;//HEADERROR_LOWINK;
	}
	#endif 
	*/

	int nDiff = (int)((double)(clock () - m_clockLast) / CLOCKS_PER_SEC * 1000.0);
	DWORD dwError = GetIndicatorState (Indicators);


	#if defined( __DEBUG_LOW_INK_CODE__ ) || defined( _DEBUG )
	//if (_tcstoul (m_Config.m_HeadSettings.m_strUID, 0, 16) == 0x01)
	{
		dwError				= ::dwSimError;
		Indicators.HVOK		= (dwError & HEADERROR_HIGHVOLTAGE) ? false : true;
		Indicators.AtTemp	= (dwError & HEADERROR_LOWTEMP)		? false : true;
		Indicators.InkLow	= (dwError & HEADERROR_LOWINK)		? true : false;
	}
	#endif

	#ifdef _DEBUG
	if (theApp.m_bDemo && !IsUSB ()) {
		dwError				= m_dwSimulatedError;
		Indicators.HVOK		= (dwError & HEADERROR_HIGHVOLTAGE) ? false : true;
		Indicators.AtTemp	= (dwError & HEADERROR_LOWTEMP)		? false : true;
		Indicators.InkLow	= (dwError & HEADERROR_LOWINK)		? true : false;
	}
	#endif

	ClearErrors (dwError, m_vErrors, m_vClear);
	CheckErrors (dwError, m_vClear, nDiff);
	CheckErrors (dwError, m_vErrors, nDiff, &m_vClear);

	m_clockLast = clock ();

	if (IsValveHead (m_Config.m_HeadSettings)) {
		STATUSSTRUCT s;

		if (CIdsThread * pThread = theApp.GetIdsThread ()) {
			LOCK (pThread->m_cs);
			{
				ULONG lAddr = _tcstoul (m_Config.m_HeadSettings.m_strUID, 0, 16);

				if (lAddr == 0x300) {
					pThread->UpdateStatus ();
					::Sleep (0);
				}

				pThread->GetStatus (s, 2);
			}
		}

		m_Status.m_bAtTemp			= true;
		m_Status.m_bVoltageOk		= true;
		m_Status.m_nInkLevel		= s.m_inkLevel;
		m_Status.m_bBrokenLine		= s.m_bBroken;
		m_Status.m_dLineSpeed		= dSpeed;
		m_Status.m_bIDSConnected	= s.m_bConnected;

		/*
		#ifdef _DEBUG // TODO: rem
		static const CTime tmStart = CTime::GetCurrentTime ();
		CTimeSpan tm = CTime::GetCurrentTime () - tmStart;
		int n = abs (tm.GetTotalSeconds ()) % 15;

		//if (n > 5) { m_Status.m_nInkLevel = LOW_INK; TRACEF ("LOW_INK"); }
		if (n > 5) { m_Status.m_bIDSConnected = false; TRACEF ("IDS disconnected"); } else m_Status.m_bIDSConnected = true;
		#endif 
		*/
	}
	else {
		CError & errLowTemp			= GetError (HEADERROR_LOWTEMP,		m_vErrors);
		CError & errHighVoltage		= GetError (HEADERROR_HIGHVOLTAGE,	m_vErrors);
		CError & errLowInk			= GetError (HEADERROR_LOWINK,		m_vErrors);

		m_Status.m_bAtTemp			= !errLowTemp.m_bSignalled;
		m_Status.m_bVoltageOk		= !errHighVoltage.m_bSignalled;
		m_Status.m_lIntr			= Flags.ISRInt;
		m_Status.m_dLineSpeed		= dSpeed;
		m_Status.m_bIDSConnected	= true; // ignore
		m_Status.m_bEp8Error		= !theApp.m_bDemo ? (m_lEp8 > 3 ? true : false) : false;

		if (m_Status.m_nInkLevel != INK_OUT)
			m_Status.m_nInkLevel	= errLowInk.m_bSignalled ? LOW_INK : OK; 

		//#ifdef _DEBUG
		//{ m_Status.m_nInkLevel = LOW_INK; TRACEF ("TODO: rem: LOW_INK"); }
		//#endif

		if (m_Status.m_nInkLevel == INK_OUT && !errLowInk.m_bSignalled)
			m_Status.m_nInkLevel	= OK; 

		if (m_Status.m_nInkLevel == OK) {
			m_dRemainingInk = GetInkLowVolume (m_Config.m_HeadSettings.m_nHeadType, FoxjetDatabase::INKTYPE_FIRST);

			if (lastStatus.m_nInkLevel == INK_OUT) {
				if (m_eTaskState == IDLE) {
					TRACER (ToString (m_Config.m_Line.m_lID));

					while (!::PostMessage (m_hControlWnd, WM_RESUME_TASK, m_Config.m_Line.m_lID, (LPARAM)this))
						::Sleep (0);
				}
			}
		}
	}

	//{
	//	LPCTSTR lpsz [] = { _T ("DISABLED"), _T ("RUNNING"), _T ("IDLE"), _T ("STOPPED") };
	//	TRACER (m_Config.m_HeadSettings.m_strName + _T (": m_nInkLevel = ") 
	//		+ a2w (std::to_string ((__int64)lastStatus.m_nInkLevel).c_str ()) + _T (", ") 
	//		+ a2w (std::to_string ((__int64)lastStatus.m_nInkLevel).c_str ()) + _T (", ") 
	//		+ CString (lpsz [m_eTaskState]));
	//	//TRACER (m_Config.m_HeadSettings.m_strName + _T (": ") + a2w (std::to_string (m_dRemainingInk).c_str ()) + _T (", m_nInkLevel = ") + a2w (std::to_string ((__int64)m_Status.m_nInkLevel).c_str ()));
	//}
}

void CLocalHead::OnBeginApsCycle (UINT nParam, LONG lParam)
{
	DWORD dwTimeout = 3000;
	
	if (GlobalFuncs::GetPhcChannels (m_Config.m_HeadSettings) == 32) {
		dwTimeout += m_Config.m_Line.m_nAMS32_A1;
		dwTimeout += m_Config.m_Line.m_nAMS32_A2;
		dwTimeout += m_Config.m_Line.m_nAMS32_A3;
		dwTimeout += m_Config.m_Line.m_nAMS32_A4;
		dwTimeout += m_Config.m_Line.m_nAMS32_A5;
		dwTimeout += m_Config.m_Line.m_nAMS32_A6;
		dwTimeout += m_Config.m_Line.m_nAMS32_A7;
	}
	else {
		dwTimeout += m_Config.m_Line.m_nAMS256_A1;
		dwTimeout += m_Config.m_Line.m_nAMS256_A2;
		dwTimeout += m_Config.m_Line.m_nAMS256_A3;
		dwTimeout += m_Config.m_Line.m_nAMS256_A4;
		dwTimeout += m_Config.m_Line.m_nAMS256_A5;
		dwTimeout += m_Config.m_Line.m_nAMS256_A6;
		dwTimeout += m_Config.m_Line.m_nAMS256_A7;
	}

	{ 
		CString str;

		str.Format (_T ("%s: BeginApsCycle %s [%d ms timeout]"),
			GetFriendlyName (),
			CTime::GetCurrentTime ().Format ("%c"),
			dwTimeout);
		TRACEF (str);
	}

	if (theApp.IsMPHC ()) {
		m_bAmsActive = true;
		m_tmApsBegin = CTime::GetCurrentTime ();
		m_dwApsTimeout = dwTimeout;

		if (!IsUSB ()) {
			if (CMphc * pDriver = theApp.GetMPHCDriver ()) {
				pDriver->EnablePrint (m_hDeviceID, false);
				pDriver->SetAmsState (m_hDeviceID, TRUE);
			}
		}

		TRACEF (CTime::GetCurrentTime ().Format (_T ("begin aps:   %c")));
	}
}

void CLocalHead::EndApsCycle ()
{
	{ 
		CString str;

		str.Format (_T ("%s: EndApsCycle %s"),
			GetFriendlyName (),
			CTime::GetCurrentTime ().Format ("%c"));
		TRACEF (str);
	}

	if (theApp.IsMPHC ()) {
		if (!IsUSB ()) 
			theApp.GetMPHCDriver ()->SetAmsState (m_hDeviceID, FALSE);

		m_bAmsActive = false;
		m_bAmsCountDown = false;
	}

	if (m_bPhotoEnabled) {
		if (!IsUSB ())
			theApp.GetDriver ()->EnablePrint (m_hDeviceID, true);
	}

	m_dwApsTimeout = 0;
	m_lastAMSCycle = CTime::GetCurrentTime();
}

bool CLocalHead::IsAmsActive () const
{
	return (m_bAmsCountDown && !m_bPrinting) || m_bAmsActive;
}


int CLocalHead::Find (const CHeadPtrArray & v, const CLocalHead * p)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i] == p)
			return i;

	return -1;
}

void CLocalHead::FindSlaves ()
{
	ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);

	m_vSlaves.RemoveAll ();
	//TRACEF (GetFriendlyName () + ": m_vSlaves.RemoveAll ();");

	if (IsMaster () || m_Config.m_HeadSettings.m_nPhotocell != SHARED_PC) {
		TRACEF ("CLocalHead::FindSlaves [" + GetFriendlyName () + "]");

		GetSlaves (m_vSlaves);

		for (int i = 0; i < m_vSlaves.GetSize (); i++) 
			TRACEF ("\t" + m_vSlaves [i]->GetFriendlyName () + " [slave to " + GetFriendlyName () + "]");
	}
}

void CLocalHead::Insert (CHeadPtrArray & v, CLocalHead * p)
{
	for (int i = 0; i < v.GetSize (); i++) {
		if (p->m_Config.m_HeadSettings.m_lPhotocellDelay <= v [i]->m_Config.m_HeadSettings.m_lPhotocellDelay) {
			v.InsertAt (i, p);
			return;
		}
	}

	v.Add (p);
}

int CLocalHead::GetLineHeads (CHeadPtrArray & v)
{
	v.RemoveAll ();

	if (m_Config.m_Line.m_lID != NOTFOUND) {
		for (POSITION pos = m_params.m_listHeads.GetStartPosition(); pos; ) {
			ULONG lKey;
			CHead * pLookup = NULL;

			m_params.m_listHeads.GetNextAssoc (pos, lKey, (void *&)pLookup);
			ASSERT (pLookup);
			
			if (CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pLookup)) {
				if (Find (v, p) == -1) {
					TRACEF (p->GetFriendlyName ());
					Insert (v, p);
				}
			}
		}
	}

	return v.GetSize ();
}

int CLocalHead::GetSlaves (CHeadPtrArray & v)
{
	int nPhotocell = m_Config.m_HeadSettings.m_nPhotocell;
	const ULONG lAddr = _tcstoul (m_Config.m_HeadSettings.m_strUID, 0, 16);

	v.RemoveAll ();

	if (IsValveHead (m_Config.m_HeadSettings)) {
		if (lAddr == 0x300) {
			for (int i = 0; i < m_params.m_vLines.GetSize (); i++) {
				if (m_Config.m_Line.m_lID != NOTFOUND) {
					for (POSITION pos = m_params.m_listHeads.GetStartPosition(); pos; ) {
						ULONG lKey;
						CHead * pLookup = NULL;

						m_params.m_listHeads.GetNextAssoc (pos, lKey, (void *&)pLookup);
						ASSERT (pLookup);
						CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pLookup);
						ASSERT (p);
						ULONG lLookup = _tcstoul (p->m_Config.m_HeadSettings.m_strUID, 0, 16);

						if (lLookup == 0x320) {
							bool bShared = p->m_Config.m_HeadSettings.m_nPhotocell == SHARED_PC;

							if (bShared) {
								//TRACEF (p->m_Config.m_HeadSettings.m_strUID);
								Insert (v, p);

								return v.GetSize ();
							}
						}
					}
				}
			}
		}
		
		return v.GetSize ();
	}

	if (IsMaster () || m_Config.m_HeadSettings.m_nPhotocell != SHARED_PC) {
		int nPhotocell = m_Config.m_HeadSettings.m_nPhotocell;
		int nFindSharedPhoto = SHARED_PCA;

		if (lAddr == 0x310) 
			nFindSharedPhoto = SHARED_PCB;

		if (IsUSB ())
			nFindSharedPhoto += 3;

		for (int i = 0; i < m_params.m_vLines.GetSize (); i++) {
			if (m_Config.m_Line.m_lID != NOTFOUND) {
				if (lAddr == 0x300) {
					//if (CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pLine->GetHeadByUID (_T ("310")))) {
					if (CLocalHead * p = (CLocalHead *)GetHeadByUID (_T ("310"))) {
						if (p->IsMaster () &&
							p->m_Config.m_HeadSettings.m_nSharePhoto == SHARED_PCA) 
						{
							// special case where card 2 gets it's photocell from card 1

							if (Find (v, p) == -1)
								//if (p->IsAlive ())
									Insert (v, p);
						}
					}
				}

				for (POSITION pos = m_params.m_listHeads.GetStartPosition(); pos; ) {
					ULONG lKey;
					CHead * pLookup = NULL;

					m_params.m_listHeads.GetNextAssoc (pos, lKey, (void *&)pLookup);
					ASSERT (pLookup);
					CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pLookup);

					if (p && p != this && !p->IsMaster ()/* && p->IsAlive ()*/) { 
						if (nFindSharedPhoto == p->m_Config.m_HeadSettings.m_nSharePhoto) 
							if (Find (v, p) == -1)
								Insert (v, p);
					}
				}
			}
		}
	}

	return v.GetSize ();
}

static CString Format (const SYSTEMTIME & tm)
{
	CString str;

	str.Format (_T ("%u:%u:%u:%u"),
		tm.wHour,
		tm.wMinute,
		tm.wSecond,
		tm.wMilliseconds);

	return str;
}


int CLocalHead::Run() 
{
	CString strEvent;
	MSG msg;
	HANDLE hEvent [6] = { NULL };
	CTime tmLast = CTime::GetCurrentTime();
	bool bAmsDisabledPhotocell = false;
	bool bFlashStrobe = false;
	const DWORD dwTimeout = 20;
	bool bEOP = false;
	bool bImageRefresh = false;
	clock_t clock_tLast = clock (), clock_tNow = clock ();
	clock_t clock_tImageRefresh = clock ();
	const double dSampleRate = 0.5;
#if !defined( _DEBUG ) && !defined( __DEBUG_SYNC__ )
	clock_t clock_tPhotoLast = clock ();
#endif

	m_tLastDownload = clock ();
	m_lastAMSCycle = tmLast;

	strEvent.Format (_T ("Initialization Complete %lu"), m_hThread );
	HANDLE hInit = CreateEvent ( NULL, true, false, strEvent );
	hEvent [0] = m_hExit;
	hEvent [1] = hInit;
	
	bool bInitialized = false;

	if (!theApp.m_Database.IsSqlServer ()) 
		m_pdb = &theApp.m_Database;

	while (!bInitialized) {
		switch (::MsgWaitForMultipleObjects (2, hEvent, false, dwTimeout, QS_ALLINPUT)) {
			case WAIT_OBJECT_0:
				return ExitInstance();
			case WAIT_OBJECT_0 + 1:
				//TRACEF (GetFriendlyName () + _T (" Initialized"));
				bInitialized = true;
				break;
		}

		while (::PeekMessage (&msg, NULL, NULL, NULL, PM_NOREMOVE)) 
			if (!PumpMessage ())
				return ExitInstance();
	}
	
	::CloseHandle (hInit);
	::Sleep (dwTimeout);

	if (!IsUSB ())
		SetThreadPriority ( THREAD_PRIORITY_TIME_CRITICAL );

	//TRACEF (GetFriendlyName () + _T (" CLocalHead::Run"));

	if (IsWDTEnabled ()) 
		ProgramWDT ();
	else
		DisableWDT ();

	while ( 1 ) {
		hEvent [0] = m_hExit;
		hEvent [1] = m_hSerialEvent;
		hEvent [2] = m_hPhotoEvent;
		hEvent [3] = m_hEOPEvent;
		hEvent [4] = m_hAmsFinished;
		hEvent [5] = m_hImageRefresh;

		DWORD dwWait = ::MsgWaitForMultipleObjects (ARRAYSIZE (hEvent), hEvent, false, dwTimeout, QS_ALLINPUT);
//		DWORD dwWait = ::WaitForMultipleObjects (ARRAYSIZE (hEvent), hEvent, false, dwTimeout);

		switch (dwWait) {
			case WAIT_OBJECT_0:
				return ExitInstance();
			case WAIT_OBJECT_0 + 1:	
				ResetEvent (m_hSerialEvent);
				OnEventSerial (0, 0);
				break;
			case WAIT_OBJECT_0 + 2:	
				{ 
					ResetEvent (m_hPhotoEvent);

					SYSTEMTIME tm1, tm2;

					::GetSystemTime (&tm1);
					::QueryPerformanceCounter (&m_tmImagingTimePC); 

					#ifdef _DEBUG_PHOTO_TIME
					LARGE_INTEGER a, b;
					CString str;
					
					::QueryPerformanceCounter (&a); 
					#endif //_DEBUG_PHOTO_TIME

					//#if !defined( _DEBUG ) && !defined( __DEBUG_SYNC__ )
					//{
					//	CString str;

					//	double dDiff = (double)(clock () - clock_tPhotoLast) / CLOCKS_PER_SEC;
					//	clock_tPhotoLast = clock ();

					//	str.Format (_T ("[%0.2f s] 0x%p: %s (%s): m_hPhotoEvent"), dDiff, m_hPhotoEvent, GetFriendlyName (), GetUID ());
					//	::PostMessage (m_hControlWnd, WM_DIAGNOSTIC_DATA, (WPARAM)new CString (str), 0);
					//}
					//#endif

					#ifdef __WINPRINTER__
					if (IsMaster ())
						CDateTimeElement::SetPrintTime (new CTime (CTime::GetCurrentTime ()), m_Config.GetLineID ());
					#endif //__WINPRINTER__
					
					//#ifdef _DEBUG
					//if (theApp.m_bDemo && IsOneToOnePrint () && !GetSerialPending ())
					//	break;
					//#endif

					OnEventPhoto (0, 0);

					#ifdef _DEBUG_PHOTO_TIME
					::QueryPerformanceCounter (&b); 

					double d = ((double)b.QuadPart - (double)a.QuadPart) / (double)m_freq.QuadPart; 
					str.Format (_T ("OnEventPhoto [%s]: %.8f seconds"), GetFriendlyName (), d);
					TRACEF (str);

					//((CMainFrame *)theApp.m_pMainWnd)->m_pControlView->UpdateSerialData (str);
					#endif //_DEBUG_PHOTO_TIME

					if (theApp.GetImagingMode () == IMAGE_ON_PHOTOCELL) {
						::GetSystemTime (&tm2);
						PostTimingInfo (m_tmImagingTimePC, GetFriendlyName () + _T (" [IMAGE_ON_PHOTOCELL] [") + ::Format (tm1) + _T (", ") + ::Format (tm2) + _T ("]"));
					}
				}
				break;
			case WAIT_OBJECT_0 + 3:	
				ResetEvent (m_hEOPEvent);

				#if !defined( _DEBUG ) && !defined( __DEBUG_SYNC__ )
				{
					CString str;
					str.Format (_T ("0x%p: %s (%s): m_hEOPEvent"), m_hEOPEvent, GetFriendlyName (), GetUID ());
					TRACER (str);
				}
				#endif

				SYSTEMTIME tm1, tm2;

				::GetSystemTime (&tm1);
				::QueryPerformanceCounter (&m_tmImagingTimeEOP); 

				//#ifdef _DEBUG
				//if (theApp.m_bDemo && IsOneToOnePrint () && !GetSerialPending ())
				//	break;
				//#endif

				OnEventEOP (0, 0);

				if (theApp.GetImagingMode () == IMAGE_ON_EOP) {
					::GetSystemTime (&tm2);
					PostTimingInfo (m_tmImagingTimePC, GetFriendlyName () + _T (" [last photocell]"));
					PostTimingInfo (m_tmImagingTimeEOP, GetFriendlyName () + _T (" [IMAGE_ON_EOP] [") + ::Format (tm1) + _T (", ") + ::Format (tm2) + _T ("]"));
				}
				break;
			case WAIT_OBJECT_0 + 4:	
				::ResetEvent (m_hAmsFinished);
				EndApsCycle ();
				break;

			case WAIT_OBJECT_0 + 5:	
				::ResetEvent (m_hImageRefresh);
				
				if (!bImageRefresh) {
					clock_tImageRefresh = clock ();
					bImageRefresh = true;
				}

				break;
		}

		if (bImageRefresh) {
			double dDiff = (double)(clock () - clock_tImageRefresh) / CLOCKS_PER_SEC;

			if (dDiff > 1.0) {
				while (!::PostThreadMessage (m_nThreadID, TM_BUILD_IMAGE, (WPARAM)MAKEIMGPARAMS (IMAGING_REFRESH, 0), 0))
					::Sleep (0);

				if (IsMaster ()) 
					theApp.PostViewRefresh (m_dwImageRefresh);

				bImageRefresh = false;
			}
		}

		if (m_dwApsTimeout) {
			int nTimeout = m_dwApsTimeout / 1000;
			CTimeSpan tm = CTime::GetCurrentTime () - m_tmApsBegin;

			ASSERT (tm.GetTotalSeconds () >= 0);

			if (tm.GetTotalSeconds () > nTimeout) {
				EndApsCycle ();
				TRACEF (GetFriendlyName () + _T (": APS timed out"));
				//ASSERT (0);				
			}
		}

		#ifdef __WINPRINTER__
		if (theApp.GetImagingMode () == IMAGE_ON_EOP && (m_eTaskState == RUNNING || m_eTaskState == IDLE)) {
			if (theApp.GetImagingModeExpire ()) {
				if (!m_bPrinting) {
					double dDiff = (double)(clock_tNow - m_tLastDownload) / CLOCKS_PER_SEC;

					if (dDiff > 29.0) {
						CTime tmNow = CTime::GetCurrentTime ();

						bool bRolledOver = 
							m_tmImage.GetMinute ()	!= tmNow.GetMinute ()	||
							m_tmImage.GetDay ()		!= tmNow.GetDay ()		||
							m_tmImage.GetHour ()	!= tmNow.GetHour ()		||
							m_tmImage.GetMonth ()	!= tmNow.GetMonth ()	||
							m_tmImage.GetYear ()	!= tmNow.GetYear ();

						if (bRolledOver) {
							CTime tm = CDateTimeElement::GetPrintTime (m_Config.GetLineID ());

							if (IsMaster ())
								CDateTimeElement::SetPrintTime (new CTime (tmNow), m_Config.GetLineID ());

							if (IsMaster ()) {
								TRACEF (GetFriendlyName () + _T (": ") + m_tmImage.Format (_T ("%c")) + tm.Format (_T (" [%c]")));

								OnBuildImage ((WPARAM)MAKEIMGPARAMS (IMAGING, 0), 0);

								for (int i = 0; i < m_vSlaves.GetSize (); i++) {
									if (CLocalHead * pSlave = DYNAMIC_DOWNCAST (CLocalHead, m_vSlaves [i])) {
										CLocalHead::BUILDIMAGESTRUCT * pParam = MAKEIMGPARAMS (IMAGING, pSlave->m_nThreadID);
										HANDLE hEvent = pParam->m_hEvent;

										pSlave->OnBuildImage ((WPARAM)pParam, 0);

										DWORD dwWait = ::WaitForSingleObject (hEvent /* pParam->m_hEvent */, 1000);

										//if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
										if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
										if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
										if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");

										//delete pParam;
									}
								}

								bool bPrintSignalled = ::WaitForSingleObject (m_hPhotoEvent, 0) == WAIT_OBJECT_0;

								if (!bPrintSignalled)
									DownloadImages (m_vSlaves);
							}
						}
					}
				}
			}
		}
		#endif //__WINPRINTER__

		if (!m_bPrinting) {
			if (!m_bAmsActive && theApp.IsMPHC ()) {
				if (IsMaster () && m_nAmsInterval >= 1) {
					CTimeSpan tmAPS = CTime::GetCurrentTime() - m_lastAMSCycle;
					int nAPS = tmAPS.GetTotalSeconds ();

					if (nAPS >= (m_nAmsInterval * 60)) {
						if (WaitForSingleObject (m_hPhotoEvent, 0) == WAIT_TIMEOUT) { // Test the PhotoEvent to insure it hasn't fired again.
							CHeadPtrArray v;

							GetLineHeads (v);
							m_bAmsActive = true;

							/* TODO: rem
							OnBeginApsCycle (0, 0);

							for (int i = 0; i < m_vSlaves.GetSize (); i++)
								while (!::PostThreadMessage (m_vSlaves [i]->m_nThreadID, TM_BEGIN_APS, 0, 0))
									::Sleep (0);
							*/
							for (int i = 0; i < v.GetSize (); i++)
								while (!::PostThreadMessage (v [i]->m_nThreadID, TM_BEGIN_APS, 0, 0))
									::Sleep (0);
						}
					}
					else if ((nAPS + m_nApsDelay) >= (m_nAmsInterval * 60)) {
						if (!m_bAmsCountDown) {
							m_bAmsCountDown = true;
							TRACEF (CTime::GetCurrentTime ().Format (_T ("begin delay: %c")));
						}
					}
				}
			}

			if (m_lStartTaskID != -1) {
				OnStartTask (0, m_lStartTaskID);

				if (m_eTaskState == RUNNING)
					OnEnablePhotocell (true, 0);
			}
		}

		/*
		if (m_pdb && m_pdb->IsSqlServer ()) {
			if (m_pdb->IsOpen () && !theApp.m_bDatabaseInUse) {
				CTimeSpan tm = CTime::GetCurrentTime () - m_tmDatabase;
				int nTotalSeconds = tm.GetTotalSeconds ();
				bool bClose = (UINT)nTotalSeconds > m_nDbTimeout;

				if (tm.GetTotalSeconds () < 0)
					bClose = true;

				if (bClose) {
					TRACEF (GetFriendlyName () + _T (": m_pdb->Close ()"));
					m_pdb->Close ();
					delete m_pdb;
					m_pdb = NULL;
				}
			}
		}
		*/

		clock_tNow = clock ();
		double dDiff = (double)(clock_tNow - clock_tLast) / CLOCKS_PER_SEC;

		if ((int)(dDiff * 1000.0) < 0) 
			clock_tLast = clock ();

		if (dDiff >= dSampleRate) {
			DWORD dwError	= GetErrorState ();
			bool bWarning	= IsWarning (dwError);
			bool bError		= IsError (dwError);

			if (FoxjetCommon::IsProductionConfig () && !Foxjet3d::GetTestPHC (GetIOAddress ()))
				bError = false;

			ReadStatus ();
			CheckState ();
			//TRACER (_T ("."));


			if (bError) {
				while (!::PostMessage (m_hControlWnd, WM_SET_HEAD_ERROR, m_Config.m_Line.m_lID, m_Task.m_lID)) // (LPARAM)this))
					::Sleep (0);
			}
			else {
				////if (theApp.m_bDemo) 
				if (m_eTaskState == RUNNING)
				{
					//DECLARETRACECOUNT (20);
					//MARKTRACECOUNT ();

					//DWORD dwResult = 0;
					//LRESULT lSend = ::SendMessageTimeout (m_hControlWnd, WM_LINE_IS_WAITING, 
					//	(WPARAM)m_Config.m_Line.m_lID, 0, SMTO_BLOCK | SMTO_ABORTIFHUNG, 250, &dwResult);

					while (!::PostMessage (m_hControlWnd, WM_LINE_IS_WAITING, (WPARAM)m_Config.m_Line.m_lID, 1))
						::Sleep (0);

					//MARKTRACECOUNT ();
					//TRACECOUNTARRAYMAX ();

					//if (dwResult) {
					//	while (!::PostMessage (m_hControlWnd, WM_LINE_SET_WAITING, m_Config.m_Line.m_lID, false))
					//		::Sleep (0);
					//}
				}

				if (bWarning) 
					while (!::PostMessage (m_hControlWnd, WM_SET_HEAD_WARNING, m_Config.m_Line.m_lID, (LPARAM)this))
						::Sleep (0);
			}

			tmLast = CTime::GetCurrentTime();
 			clock_tLast = clock ();

			while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this))
				::Sleep (0);
		}

		while (PeekMessage (&msg, NULL, NULL, NULL, PM_NOREMOVE))
			if (!PumpMessage ())
				return ExitInstance ();

		/*
		{
			DEBUG_EVENT DebugEv;
			DWORD dwContinueStatus = DBG_CONTINUE;
			HANDLE hProcess = ::GetCurrentProcess ();
			
			BOOL bWaitForDebugEvent = ::WaitForDebugEvent (&DebugEv, 100); 
 
			switch (DebugEv.dwDebugEventCode) { 
			case OUTPUT_DEBUG_STRING_EVENT: 
				{         
					char * psz = new char [DebugEv.u.DebugString.nDebugStringLength];
					CString str;
					DWORD dwRead;
					
					::ReadProcessMemory (hProcess, DebugEv.u.DebugString.lpDebugStringData, psz, DebugEv.u.DebugString.nDebugStringLength, &dwRead);
					
					str.Format (_T ("[0x%p, 0x%p] %s [%d (%d, %d)]"), 
						DebugEv.dwProcessId, 
						DebugEv.dwThreadId, 
						a2w (psz), 
						dwRead,
						DebugEv.u.DebugString.lpDebugStringData, 
						DebugEv.u.DebugString.nDebugStringLength);

					delete psz;
					break;
				}
			} 
 
			// Resume executing the thread that reported the debugging event. 
 			ContinueDebugEvent(DebugEv.dwProcessId, DebugEv.dwThreadId, dwContinueStatus); 
		}
		*/
	}

	return ExitInstance ();
}

bool CLocalHead::OnPhotocell (BUILDTYPE imgtype, bool bOverrideImagingFlag)
{
	LARGE_INTEGER a, b;

	::QueryPerformanceCounter (&a); 

	bool value = false;
	const int flush = CalcFlushBuffer ();

	if (theApp.GetImagingMode () == IMAGE_ON_EOP)
		FreeImgUpdDesc ();

	#ifdef _DEBUG
	//if (IsUSB ()) { bOverrideImagingFlag = true; TRACEF ("*** TODO: rem: bOverrideImagingFlag = true;"); }
	#endif

	if (BuildImage (imgtype, bOverrideImagingFlag) != IMAGE_SUCCESS) {
		CString str = GetFriendlyName () + " BuildImage failed [1]";
		m_bPhotoEnabled = false;
		
		if (IsUSB ())
			theApp.GetUSBDriver ()->EnablePrint ((UINT)m_hDeviceID, false);
		else
			theApp.GetDriver ()->EnablePrint (m_hDeviceID, false);

		TRACEF (str);
		DisplayDebugMessage (str);

		return false;
	}

//
//	QueryPerformanceCounter ( &m_n64Finish );
//	double dTime = ( double ) ( (double) (m_n64Finish.QuadPart - m_n64Start.QuadPart) /  m_n64Freq.QuadPart );
//	
//	dTime *= 1000;
//
//	sCounter.Format ( "%s Task %s Image Time:%08.4fms Freq:%I64u", GetFriendlyName(), m_sTaskName, dTime, m_n64Freq.QuadPart );
//	Debug.LogEntry ( sCounter );
// Performance

	int N_Factor = m_Config.GetNFactor();
	INT nWordsPerCol	= m_ImageHdr.ibpc / 2;
	DWORD dwImageCols	= (DWORD) ((ULONG) m_ImageHdr.ilen / (ULONG) m_ImageHdr.ibpc);
	bool bProgramSlantRegs = false;

	UCHAR state = (m_Config.GetHeadSettings().m_nDirection == LTOR) ? 1 : 0;
	state |= m_Config.GetHeadSettings().m_bInverted ? 0x02 : 0x00;

	
	//#ifdef _DEBUG
	// 7FFA0
	if (IsUSB ()) { 
		CString str;
		str.Format (_T ("0x%p: [%s] nWordsPerCol: %d, dwImageCols: %d, N_Factor: %d, state: %d"),
			_tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16), 
			m_Config.m_HeadSettings.m_strName, 
			nWordsPerCol,
			dwImageCols,
			N_Factor,
			state);
		//TRACER (str);
	}
	//#endif //_DEBUG

	if (!IsUSB ()) {
		WORD wImageCols	= (WORD)dwImageCols;
		tagSlantRegs SlantRegs;

		switch (state) {
		case 0:	// R->L Normal
		case 3:	// L->R Inverted
			switch ( m_Config.GetHeadSettings().m_nHeadType )
			{
				case FoxjetDatabase::ALPHA_CODER:
				case FoxjetDatabase::UJ2_192_32:
				case FoxjetDatabase::UJ2_192_32NP:
				case FoxjetDatabase::UJ2_96_32:
				case FoxjetDatabase::UJI_96_32:
				case FoxjetDatabase::UJI_192_32:
				case FoxjetDatabase::UJI_256_32:
					//flush = (31 * N_Factor * nWordsPerCol); 
					SlantRegs._ImageStartAddr._Addr1	= (WORD) 0x0000;
					SlantRegs._ImageStartAddr._Addr2	= (WORD) ( -nWordsPerCol * N_Factor );
					SlantRegs._AddrIncFactor._Incr		= (WORD) ( -2 * nWordsPerCol * N_Factor );
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) ( 32 * nWordsPerCol * N_Factor );
					break;
				case FoxjetDatabase::UJ2_352_32:
					//flush = N_Factor * nWordsPerCol; 
					SlantRegs._ImageStartAddr._Addr1	= (WORD) 0x0000;
					SlantRegs._ImageStartAddr._Addr2	= (WORD) (-nWordsPerCol * N_Factor);
					SlantRegs._AddrIncFactor._Incr		= (WORD) 0x0000;
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) 0x0000;
					break;
				case FoxjetDatabase::GRAPHICS_384_128:
				case FoxjetDatabase::GRAPHICS_768_256:
				case FoxjetDatabase::GRAPHICS_768_256_L310:
				case FoxjetDatabase::UR2:
				case FoxjetDatabase::UR4:
					//flush = N_Factor * nWordsPerCol; 
					SlantRegs._ImageStartAddr._Addr2	= (WORD) 0x0000;
					SlantRegs._ImageStartAddr._Addr1	= (WORD) (-nWordsPerCol * N_Factor);
					SlantRegs._AddrIncFactor._Incr		= (WORD) 0x0000;
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) 0x0000;
					break;
			}
			break;
		case 1:	// L->R Normal
		case 2:	// R->L Inverted
			switch ( m_Config.GetHeadSettings().m_nHeadType )
			{
				case FoxjetDatabase::ALPHA_CODER:
				case FoxjetDatabase::UJ2_192_32:
				case FoxjetDatabase::UJ2_192_32NP:
				case FoxjetDatabase::UJ2_96_32:
				case FoxjetDatabase::UJI_96_32:
				case FoxjetDatabase::UJI_192_32:
				case FoxjetDatabase::UJI_256_32:
					//flush = (31 * N_Factor * nWordsPerCol); 
					SlantRegs._ImageStartAddr._Addr1	= (WORD) ( nWordsPerCol * ( wImageCols + flush - 1) );
					SlantRegs._ImageStartAddr._Addr2	= (WORD) ( SlantRegs._ImageStartAddr._Addr1 - ( N_Factor * nWordsPerCol) );
					SlantRegs._AddrIncFactor._Incr		= (WORD) ( -2 * nWordsPerCol * N_Factor );
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) /* max */ (((32 * nWordsPerCol * N_Factor ) - 2 * nWordsPerCol));
					break;
				case FoxjetDatabase::UJ2_352_32:	
					//flush = N_Factor * nWordsPerCol; 
					SlantRegs._ImageStartAddr._Addr1	= (WORD) (  nWordsPerCol * (wImageCols + N_Factor - 1) );
					SlantRegs._ImageStartAddr._Addr2	= (WORD) ( SlantRegs._ImageStartAddr._Addr1 - ( N_Factor * nWordsPerCol) );
					SlantRegs._AddrIncFactor._Incr		= (WORD) 0x0000;
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) -2 * nWordsPerCol;
					break;
				case FoxjetDatabase::GRAPHICS_384_128:
				case FoxjetDatabase::GRAPHICS_768_256:	
				case FoxjetDatabase::GRAPHICS_768_256_L310:
				case FoxjetDatabase::UR2:
				case FoxjetDatabase::UR4:
					//flush = N_Factor * nWordsPerCol; 
					SlantRegs._ImageStartAddr._Addr2	= (WORD) (  nWordsPerCol * (wImageCols + N_Factor - 1) );
					SlantRegs._ImageStartAddr._Addr1	= (WORD) ( SlantRegs._ImageStartAddr._Addr2 - ( N_Factor * nWordsPerCol) );
					SlantRegs._AddrIncFactor._Incr		= (WORD) 0x0000;
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) -2 * nWordsPerCol;
					break;
			}
			break;
		}

		bProgramSlantRegs = theApp.GetMPHCDriver ()->ProgramSlantRegs (m_hDeviceID, SlantRegs);
	}
	else {
		USB::SlantRegs s;
	
		switch (state) {
		case 0:	// R->L Normal
		case 3:	// L->R Inverted
			switch ( m_Config.GetHeadSettings().m_nHeadType )
			{
				case FoxjetDatabase::ALPHA_CODER:
				case FoxjetDatabase::UJ2_192_32:
				case FoxjetDatabase::UJ2_192_32NP:
				case FoxjetDatabase::UJ2_96_32:
				case FoxjetDatabase::UJI_96_32:
				case FoxjetDatabase::UJI_192_32:
				case FoxjetDatabase::UJI_256_32:
					//flush = (31 * N_Factor * nWordsPerCol); 
					s._Addr2	= 0x0000;
					s._Addr1	= (-2 * N_Factor);
					s._Incr		= (-4 * N_Factor);
					s._Adjust	= (64 * N_Factor);
					break;
				case FoxjetDatabase::UJ2_352_32:
				case FoxjetDatabase::GRAPHICS_384_128:
				case FoxjetDatabase::GRAPHICS_768_256:
				case FoxjetDatabase::GRAPHICS_768_256_L310:
				case FoxjetDatabase::UR2:
				case FoxjetDatabase::UR4:
					//flush = N_Factor * nWordsPerCol; 
					s._Addr2	= 0x0000;
					s._Addr1	= (-nWordsPerCol * N_Factor);
					s._Incr		= 0x0000;
					s._Adjust	= 0x0000;
					break;
			}
			break;
		case 1:	// L->R Normal
		case 2:	// R->L Inverted
			switch ( m_Config.GetHeadSettings().m_nHeadType )
			{
				case FoxjetDatabase::ALPHA_CODER:
				case FoxjetDatabase::UJ2_192_32:
				case FoxjetDatabase::UJ2_192_32NP:
				case FoxjetDatabase::UJ2_96_32:
				case FoxjetDatabase::UJI_96_32:
				case FoxjetDatabase::UJI_192_32:
				case FoxjetDatabase::UJI_256_32:
					//flush = (31 * N_Factor * nWordsPerCol); 
					s._Addr2	= (2 * (dwImageCols - 1));
					s._Addr1	= (2 * (dwImageCols - N_Factor - 1));
					s._Incr		= (-4 * N_Factor);
					s._Adjust	= (64 * N_Factor - 4);
					break;
				case FoxjetDatabase::UJ2_352_32:	
				case FoxjetDatabase::GRAPHICS_384_128:
				case FoxjetDatabase::GRAPHICS_768_256:	
				case FoxjetDatabase::GRAPHICS_768_256_L310:
				case FoxjetDatabase::UR2:
				case FoxjetDatabase::UR4:
					//flush = N_Factor * nWordsPerCol; 
					s._Addr2	= (nWordsPerCol * (dwImageCols - 1));
					s._Addr1	= (nWordsPerCol * (dwImageCols - N_Factor - 1));
					s._Incr		=  0x0000;
					s._Adjust	=  -2 * nWordsPerCol;
					break;
			}
			break;
		}

		if ( !m_listImgUpdDesc.IsEmpty() ) {
			if (theApp.IsMPHC()) {
				bool bProgramSlantRegs = false;
				/*
				#ifdef _DEBUG
				// 7FFA0
				{ 
					CString str;
					str.Format (_T ("_Addr1: 0x%p [%d], _Addr2: 0x%p [%d], _Adjust: 0x%p [%d], s._Incr: 0x%p [%d]"), 
						s._Addr1,	s._Addr1,	
						s._Addr2,	s._Addr2,	
						s._Adjust,	s._Adjust,	
						s._Incr,	s._Incr); TRACEF (str);
				}
				#endif //_DEBUG
				*/
			}

			bProgramSlantRegs = theApp.GetUSBDriver ()->ProgramSlantRegs ((UINT)m_hDeviceID, s);

			if (!bProgramSlantRegs) TRACEF (GetFriendlyName () + _T (": ProgramSlantRegs failed"));
		}
	}

	if ( !m_listImgUpdDesc.IsEmpty() ) {
		if (theApp.IsMPHC()) {
		/*
			bool bProgramSlantRegs = false;
			
			if (IsUSB ()) {
				#ifdef _DEBUG
				// 7FFA0
				{ 
					CString str;
					str.Format (_T ("_Addr1: 0x%p [%d], _Addr2: 0x%p [%d], _Adjust: 0x%p [%d], s._Incr: 0x%p [%d]"), 
						s._Addr1,	s._Addr1,	
						s._Addr2,	s._Addr2,	
						s._Adjust,	s._Adjust,	
						s._Incr,	s._Incr); TRACEF (str);
					str.Format (_T ("nWordsPerCol: %d, dwImageCols: %d, N_Factor: %d"),
						nWordsPerCol,
						dwImageCols,
						N_Factor);
					TRACEF (str);
				}
				#endif //_DEBUG

				// bProgramSlantRegs = true; //TRACEF ("*** TODO: ProgramSlantRegs"); //
				bProgramSlantRegs = theApp.GetUSBDriver ()->ProgramSlantRegs ((UINT)m_hDeviceID, s);
			}
			else
				bProgramSlantRegs = theApp.GetMPHCDriver ()->ProgramSlantRegs (m_hDeviceID, SlantRegs);
			*/

			if (bProgramSlantRegs) {
				double dDelay = (double)(m_lMaxPhotocellDelay - m_Config.GetHeadSettings ().m_lPhotocellDelay) / 1000.0;
				WORD wCols = (WORD)(dDelay * m_Config.GetHeadSettings ().GetRes ());

				m_wProdLen = wCols + 1;

				//ASSERT (m_wMaxFlush >= flush);

				/* if (IsMaster ()) */ 
				m_wProdLen += (m_wMaxFlush - flush);

				ASSERT (m_wProdLen >= 1);

				if (IsUSB ())
					theApp.GetUSBDriver ()->SetProductLen ((UINT)m_hDeviceID, m_wProdLen);
				else
					theApp.GetMPHCDriver ()->SetProductLen ( m_hDeviceID, m_wProdLen);

				#if defined( _DEBUG ) && defined( __DEBUGIMAGELEN__ )
				{
					double dProductLen = (double)(m_wProdLen - 1) / ((double)m_Config.GetHeadSettings().m_nHorzRes / (double)m_Config.GetHeadSettings().m_nEncoderDivisor);
					CString str;

					str.Format (_T ("[%d: %s] SetProductLen: %-5d [%-02.02f in] [delay: %.02f in] [%d flush]"),
						m_Config.GetLineID (),
						m_Config.GetHeadSettings ().m_strName,
						m_wProdLen,
						dProductLen,
						dDelay,
						flush);
					TRACEF (str);

					str.Format (
						_T ("\n\tm_ImageHdr.istart = %d,")
						_T ("\n\tm_ImageHdr.ilen   = %d,")
						_T ("\n\tm_ImageHdr.ibpc   = %d,")
						_T ("\n\tm_ImageHdr.ippc   = %d,")
						_T ("\n\tm_ImageHdr.irow   = %d,"),
						m_ImageHdr.istart,
						m_ImageHdr.ilen,
						m_ImageHdr.ibpc,
						m_ImageHdr.ippc,
						m_ImageHdr.irow);
					TRACEF (str);
				}
				#endif 

				m_wImageLen = (WORD)dwImageCols + flush;
				m_nFlush = flush;

				bool bSetImageLen = false;
				
				//TRACEF (_T ("SetImageLen: ") + ToString ((ULONG)(WORD)dwImageCols + flush));

				if (IsUSB ())
					bSetImageLen = theApp.GetUSBDriver ()->SetImageLen ((UINT)m_hDeviceID, (WORD)dwImageCols + flush);
				else
					bSetImageLen = theApp.GetMPHCDriver ()->SetImageLen (m_hDeviceID, (WORD)dwImageCols + flush);

				if (!bSetImageLen) TRACEF (GetFriendlyName () + _T (": SetImageLen failed"));

				if (bSetImageLen) {
					value = true;

					#if defined( _DEBUG ) && defined( __DEBUGIMAGELEN__ )
					{
						CString str;

						str.Format (_T ("[%d: %s] SetImageLen: %-5d [%d: wImageCols, %d: flush]"),
							m_Config.GetLineID (),
							m_Config.GetHeadSettings ().m_strName,
							wImageCols + flush,
							wImageCols,
							flush);
						TRACEF (str);
					}
					#endif 
				}
			}
		}
		else { // IV driver
			value = true;
		}
	}
	else
		value = true;
	
	{
		::QueryPerformanceCounter (&b); 

		double d = ((double)b.QuadPart - (double)a.QuadPart) / (double)m_freq.QuadPart; 
		CString str;
		str.Format (_T ("OnPhotocell [%s]: %.04f seconds"), GetFriendlyName (), d);
		//TRACEF (str);
		//((CMainFrame *)theApp.m_pMainWnd)->m_pControlView->UpdateSerialData (str);

		if (FoxjetDatabase::IsTimingEnabled ())
			while (!::PostMessage (m_hControlWnd, WM_DIAGNOSTIC_DATA, (WPARAM)new CString (str), 0))
				::Sleep (0);
	}

	if (!value) {
		CString str = GetFriendlyName () + _T (" BuildImage failed [2]");
		TRACEF (str);
		DisplayDebugMessage (str);
	}

	return value;
}

bool CLocalHead::SetPhotocellOffsets (ULONG lMin, ULONG lMax, WORD wMaxFlush)
{
	m_lMinPhotocellDelay	= lMin;
	m_lMaxPhotocellDelay	= lMax;
	m_wMaxFlush				= wMaxFlush;

	//CString str; str.Format ("SetPhotocellOffsets: %s: lMin: %d, lMax: %d, wMaxFlush: %d", GetFriendlyName (), lMin, lMax, wMaxFlush); TRACEF (str);

	return true;
}

void CLocalHead::OnShowDebugDialog(UINT wParam, LONG lParam)
{
	if (IsUSB ()) {
		if (m_pUsbDbgDlg) {
			delete m_pUsbDbgDlg;
			m_pUsbDbgDlg = NULL;
		}

		m_pUsbDbgDlg = new UsbDebugDlg::CUsbDebugDlg (this, m_pMainWnd);
		m_pUsbDbgDlg->CenterWindow ();
	}
	else {
#ifdef _DEBUG
		if (m_pDbgDlg) {
			delete m_pDbgDlg;
			m_pDbgDlg = NULL;
		}

		m_pDbgDlg = new CDebugDlg (m_pMainWnd);
		m_pDbgDlg->CenterWindow ();
#endif
	}
}

void CLocalHead::OnEnablePhotocell(UINT wParam, LONG lParam)
{
	bool bPhotoEnabled = wParam ? true : false;
	bool bHandleInEOP = (m_bPrinting && !bPhotoEnabled);

	m_bPhotoEnabled = bPhotoEnabled;

	if (!bHandleInEOP) {
		if (m_eTaskState != RUNNING)
			m_bPhotoEnabled = false;

		if (IsUSB ())
			theApp.GetUSBDriver ()->EnablePrint ((UINT)m_hDeviceID, m_bPhotoEnabled);
		else
			theApp.GetDriver ()->EnablePrint (m_hDeviceID, m_bPhotoEnabled);
	}
	// else // this will be handled in end-of-print event;

	//TRACER (GetFriendlyName () + ": OnEnablePhotocell: " + CString (m_bPhotoEnabled ? "true" : "false") + " " + CString (bHandleInEOP ? "[wait for EOP]" : "") + CString (_T ("\n")));
}

void CLocalHead::OnUpdateStrobe(WPARAM wParam, LONG lParam)
{
	m_nStrobeColor = wParam;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//	Debug Functions
void CLocalHead::OnDoDebugProcess(UINT nParam, LONG lParam)
{
	if (theApp.IsMPHC ()) {
		if (!IsUSB ()) 
			theApp.GetMPHCDriver ()->ClearDebugFlags (m_hDeviceID);
	}
}

void CLocalHead::OnReadConfig(UINT nParam, LONG lParam)
{
	if (theApp.IsMPHC ()) {		
		memset (&m_registersISA, 0x00, sizeof (m_registersISA));

		if (IsUSB ())
			theApp.GetUSBDriver ()->ReadConfiguration ((UINT)m_hDeviceID, m_registersUSB);
		else
			theApp.GetMPHCDriver ()->ReadConfiguration (m_hDeviceID, m_registersISA);
	}
}

void CLocalHead::OnToggleImageDebug(UINT nParam, LONG lParam)
{
	if (theApp.IsIV ()) {
		m_bDebug = !m_bDebug;
		theApp.GetIVDriver ()->EnableDebug (m_hDeviceID, m_bDebug);
	}
}

//	End Debug Functions
///////////////////////////////////////////////////////////////////////////////////////////////

bool CLocalHead::SetStandbyMode (bool bStandby)
{
	TRACEF (bStandby ? "SetStandbyMode: true" : "SetStandbyMode: false");

	bool bResult = CHead::SetStandbyMode (bStandby);

	OnHeadConfigChanged (0, 0);

	return bResult;
}

void CLocalHead::OnSetIgnorePhotocell (UINT wParam, LONG lParam)
{
	bool bIgnore = wParam ? 1 : 0;

	//TRACEF (CString (_T ("OnSetIgnorePhotocell: ")) + CString (bIgnore ? _T ("true") : _T ("false")));

	OnEnablePhotocell (!bIgnore, 0);
	CHead::OnSetIgnorePhotocell (wParam, lParam);

	if (!bIgnore) {
		if ((m_bLastIgnore != bIgnore) && !m_vVarElements.GetSize ()) {
			m_pImageEng->GenImage (m_pElementList, m_Config.GetHeadSettings(), 
				m_pImageBuffer, GetImageBufferSize (), IMAGING_REFRESH, 
				m_listImgUpdDesc, FoxjetCommon::PRINTER, false, this);
			//TRACEF ("re-image");
		}
	}

	m_bLastIgnore = bIgnore;
}

void CLocalHead::SetSerialPending (bool bPending)
{
	TRACEF (CString (_T ("SetSerialPending: ")) + CString (bPending ? _T ("true") : _T ("false")));
	CHead::SetSerialPending (bPending);
	
	if (IsOneToOnePrint ()) // __SW0808__
		OnSetIgnorePhotocell (!GetSerialPending (), 0);
}

void CLocalHead::OnSetSerialPending (WPARAM wParam, LPARAM lParam)
{
	SetSerialPending (wParam ? true : false);
}


void CLocalHead::GetConfig (IVCARDCONFIGSTRUCT & config)
{
	ASSERT (IsValveHead (m_Config.m_HeadSettings));

	ZeroMemory (&config, sizeof (config));

	config.m_lAddress			= _tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16);
	config.m_lLineSpeed			= m_Config.m_HeadSettings.m_nIntTachSpeed;
	config.m_bSharedPhotocell	= m_Config.m_HeadSettings.m_nPhotocell	== SHARED_PC;
	config.m_nEncoderDivisor	= m_Config.m_HeadSettings.m_nEncoderDivisor;
	config.m_bEnabled			= m_Config.m_HeadSettings.m_bEnabled;
	config.m_bExternalEncoder	= m_Config.m_HeadSettings.m_nEncoder	== EXTERNAL_ENC;
	config.m_bSharedEncoder		= m_Config.m_HeadSettings.m_nEncoder	== SHARED_ENC;

	{
		TRACEF (m_Config.m_HeadSettings.m_strUID);
		if (config.m_bSharedPhotocell) TRACEF ("SHARED_PC");
		if (config.m_bExternalEncoder) TRACEF ("EXTERNAL_ENC");
		if (config.m_bSharedEncoder) TRACEF ("SHARED_ENC");
	}

	int nHeads = min (m_Config.m_HeadSettings.m_vValve.GetSize (), ARRAYSIZE (config.m_heads));

	for (int i = 0; i < ARRAYSIZE (config.m_heads); i++) 
		config.m_heads [i].m_nType = IV_NONE;

	for (int i = 0; i < nHeads; i++) {
		VALVESTRUCT & valve = m_Config.m_HeadSettings.m_vValve [i];

		config.m_heads [i].m_nType				= valve.m_type;
		config.m_heads [i].m_lPhotocellDelay	= (USHORT)(valve.m_lPhotocellDelay / 10);
		config.m_heads [i].m_bReversed			= !valve.m_bReversed; // config.m_bReversed;
	}

	{
		int nChannels = m_Config.m_HeadSettings.GetChannels ();
		int n9 = (72 - nChannels) / 9;

		for (int i = 0; i < ARRAYSIZE (config.m_heads) && (nChannels < 72); i++) {
			if (config.m_heads [i].m_nType == (UCHAR)IV_NONE) {
				if (n9 >= 2) 
					config.m_heads [i].m_nType			= IV_10_18;
				else 
					config.m_heads [i].m_nType			= IV_12_9;
					
				config.m_heads [i].m_lPhotocellDelay	= 100;
				config.m_heads [i].m_bReversed			= false;

				int n = GetValveChannels ((VALVETYPE)config.m_heads [i].m_nType);

				nChannels += n;
				n9 -= (n / 9);
				ASSERT (nChannels <= 72);
				ASSERT (n9 >= 0);
			}
		}

		ASSERT (nChannels == 72);

		#ifdef _DEBUG
		for (int i = 0; i < ARRAYSIZE (config.m_heads); i++) {
			if (config.m_heads [i].m_nType != (UCHAR)IV_NONE) {
				CString str;

				str.Format (_T ("0x%X [%d]: %d"), 
					config.m_lAddress,
					i,
					GetValveChannels ((VALVETYPE)config.m_heads [i].m_nType));
				//TRACEF (str);
			}
		}
		#endif //_DEBUG
	}

	config.m_bInternalPhotocell = m_Config.m_HeadSettings.m_nPhotocell == INTERNAL_PC;
	config.m_lAutoPrintCols		= (USHORT)Round (m_Config.m_HeadSettings.m_dPhotocellRate * (double)m_Config.m_HeadSettings.GetRes ());
	config.m_bDPC				= theApp.IsVxDpc ();
	config.m_nImgMode			= theApp.GetImagingMode ();
}

bool CLocalHead::WriteByte (UCHAR nRegID, UCHAR n)
{
	IVREGISTERSTRUCT s;

	ZeroMemory (&s, sizeof (s));
	s.m_lAddress = _tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16);
	s.m_nRegID = nRegID;
	s.m_lSize = sizeof (n);
	s.m_lData = n;

	if (theApp.GetIVDriver ()->WriteRegister (m_hDeviceID, s))
		return true;

	return false;
}

bool CLocalHead::WriteWord (UCHAR nRegID, USHORT n)
{
	IVREGISTERSTRUCT s;

	ZeroMemory (&s, sizeof (s));
	s.m_lAddress = _tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16);
	s.m_nRegID = nRegID;
	s.m_lSize = sizeof (n);
	s.m_lData = n;

	if (theApp.GetIVDriver ()->WriteRegister (m_hDeviceID, s))
		return true;

	return false;
}

UCHAR CLocalHead::ReadByte (UCHAR nRegID)
{
	IVREGISTERSTRUCT s;

	ZeroMemory (&s, sizeof (s));
	s.m_lAddress = _tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16);
	s.m_nRegID = nRegID;
	s.m_lSize = sizeof (UCHAR);

	if (theApp.GetIVDriver ()->ReadRegister (m_hDeviceID, s)) {
		return (UCHAR)s.m_lData;
	}

	return 0;
}

USHORT CLocalHead::ReadWord (UCHAR nRegID)
{
	IVREGISTERSTRUCT s;

	ZeroMemory (&s, sizeof (s));
	s.m_lAddress = _tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16);
	s.m_nRegID = nRegID;
	s.m_lSize = sizeof (USHORT);

	if (theApp.GetIVDriver ()->ReadRegister (m_hDeviceID, s)) {
		return (USHORT)s.m_lData;
	}

	return 0;
}

void CLocalHead::FlipBytes (PUSHORT lpData, ULONG lSize)
{
/*
	PUCHAR p = (PUCHAR)lpData;
	ULONG i, j;
	UCHAR tmp;

	for (i=0, j=lSize-1; i<j; i++, j--) {
		tmp = p [i];
		p [i] = p [j];
		p [j] = tmp;
	}
*/
}

void CLocalHead::SetOffset (int nIndex, USHORT nVal)
{
/*
	FlipBytes (&nVal, sizeof (nVal));
	
	WriteByte (IV_REG_CMD, 0x01); // Page 1
	WriteWord (IV_REG_PH_OFFSETS + (2 * nIndex), nVal);
	WriteByte (IV_REG_CMD, 0x00); // Page 0 
*/
}

void CLocalHead::OnProcessSerialData (UINT wParam, LONG lParam)
{
	using namespace FoxjetElements;

	if (CSerialPacket * pPacket = (CSerialPacket *)wParam) {
		if (pPacket->m_eDeviceType == REMOTEDEVICE) {
			ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);
			FoxjetCommon::StdCommDlg::CCommItem * pComm = NULL;
			CString strData;

			for (int i = 0; i < (int)pPacket->m_nDataLen; i++)
				strData += (TCHAR)pPacket->m_pData [i];

			m_params.m_serialData.m_Len = pPacket->m_nDataLen;
			memcpy (m_params.m_serialData.Data, pPacket->m_pData, ARRAYSIZE (m_params.m_serialData.Data) * sizeof (m_params.m_serialData.Data [0]));

			for (int i = 0; i < m_vDatabaseElements.GetSize (); i++) {
				#ifdef __WINPRINTER__
				try {
					if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, m_vDatabaseElements [i])) {
						if (p->GetSerial ()) {
							CString str;
							
							str.Format (LoadString (IDS_BUILDING_ELEMENT_X_OF_N),
								GetFriendlyName (),
								i + 1,
								m_vDatabaseElements.GetSize ());
							UpdateStatusMsg (str);

							p->SetKeyValue (strData);
							p->Build ();
							TRACEF (p->GetImageData ());
						}
					}
				}
				catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
				catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
				#endif //__WINPRINTER__
			}

			m_vCommSettings.Lookup (pPacket->m_strPort, (void *&)pComm);

			if (pComm) {
				if (pComm->m_bPrintOnce)
					SetSerialPending (true);
			}

			UpdateStatusMsg ();
		}
	}

	CHead::OnProcessSerialData (wParam, lParam);
}

void CLocalHead::Abort ()
{
	m_eop.m_bStop = true;

	if (theApp.IsIV ()) 
		theApp.GetIVDriver ()->Abort (m_hDeviceID);
}

ULONG CLocalHead::AddElement (const CString & str)
{
	ULONG lResult = CHead::AddElement (str);

	if (lResult) 
		while (!::PostThreadMessage (m_nThreadID, TM_BUILD_IMAGE, (WPARAM)MAKEIMGPARAMS (IMAGING_REFRESH, 0), 0))
			::Sleep (0);

	return lResult;
}

bool CLocalHead::UpdateElement (const CString & str)
{
	bool bResult = CHead::UpdateElement (str);

	if (bResult) 
		while (!::PostThreadMessage (m_nThreadID, TM_BUILD_IMAGE, (WPARAM)MAKEIMGPARAMS (IMAGING_REFRESH, 0), 0))
			::Sleep (0);

	return bResult;
}

bool CLocalHead::DeleteElement (ULONG lID)
{
	bool bResult = CHead::DeleteElement (lID);

	if (bResult) 
		while (!::PostThreadMessage (m_nThreadID, TM_BUILD_IMAGE, (WPARAM)MAKEIMGPARAMS (IMAGING_REFRESH, 0), 0))
			::Sleep (0);

	return bResult;
}

void CLocalHead::Increment ()
{
	CString str = GetFriendlyName ();

	if (m_pElementList) {
		for (int i = 0; i < m_pElementList->GetSize (); i++) {
			FoxjetCommon::CBaseElement & e = m_pElementList->GetObject (i);

			e.Increment ();

			#if defined( _DEBUG ) && defined( __DEBUG_BUILDIMAGE__ )
			if (e.GetClassID () == COUNT) str += ", " + e.GetImageData ();
			#endif
		}
	}

	#if defined( _DEBUG ) && defined( __DEBUG_BUILDIMAGE__ )
	TRACEF (str);
	#endif
}

void CLocalHead::OnBuildImage (UINT nParam, LONG lParam)
{
DECLARETRACECOUNT (200);
MARKTRACECOUNT ();

	#ifdef _DEBUG
	const LPCTSTR lpszContext [] = 
	{
		_T ("INITIAL"), 
		_T ("IMAGING"), 
		_T ("IMAGING_REFRESH"),
	};
	#endif //_DEBUG

	ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);

	bool bOverrideImagingFlag	= (lParam & BUILDIMAGE_OVERRIDE)		? true : false;
	bool bConfigChange			= (lParam & BUILDIMAGE_CONFIG_CHANGE)	? true : false;

	AbortImgRefreshMsgs ();
MARKTRACECOUNT ();
	ASSERT (nParam);

	if (BUILDIMAGESTRUCT * p = (BUILDIMAGESTRUCT *)nParam) {
		#if defined( _DEBUG ) && defined( __DEBUG_BUILDIMAGE__ )
		{
			CTime tm = CDateTimeElement::GetPrintTime (m_Config.GetLineID ());
			CString str = p->GetTime ().Format ("built:      %c: TM_BUILD_IMAGE, ") + GetFriendlyName () + CString (", ") + lpszContext [p->GetContext ()];

			str += tm.Format (_T (" [%c]"));
			CDebug::Trace (str, true, p->GetTraceFile (), p->GetTraceLine ());
		}
		#endif //_DEBUG

MARKTRACECOUNT ();
		if (!AbortImgBuild (p)) {
MARKTRACECOUNT ();
			bool bOnPhotocell = OnPhotocell (p->GetContext (), bOverrideImagingFlag);

MARKTRACECOUNT ();
			if (bOnPhotocell) {
MARKTRACECOUNT ();
				if (!AbortImgBuild (p)) {
					m_bImageBuilt = true;

MARKTRACECOUNT ();
					if (theApp.GetImagingMode () == IMAGE_ON_PHOTOCELL)
						DownloadImage (p);
					else if (theApp.GetImagingMode () == IMAGE_ON_EOP && bConfigChange)
						OnDownloadImageSegment (0, 0);
MARKTRACECOUNT ();
				}
			}
		}
		else {
			#if defined( _DEBUG ) && defined( __DEBUG_BUILDIMAGE__ )
			CString str = p->GetTime ().Format ("ABORTED [printing] %c: TM_BUILD_IMAGE, ") + GetFriendlyName () + CString (", ") + lpszContext [p->GetContext ()];

			TRACEF (str);
			#endif //_DEBUG
		}

MARKTRACECOUNT ();
		if (p->m_hEvent)
			::SetEvent (p->m_hEvent);
		//else
			delete p;
	}
MARKTRACECOUNT ();
//TRACECOUNTARRAYMAX ();
}

bool CLocalHead::AbortImgBuild (BUILDIMAGESTRUCT * p)
{
	ASSERT (p);

	if (theApp.GetImagingMode () == IMAGE_ON_PHOTOCELL || p->GetContext () == IMAGING)
		return false;
	
	bool bSignalled = ::WaitForSingleObject (m_hPhotoEvent, 0) == WAIT_OBJECT_0;

	return bSignalled || m_bPrinting;
}

void CLocalHead::AbortImgRefreshMsgs ()
{
	MSG msg;
	
	while (::PeekMessage (&msg, NULL, TM_BUILD_IMAGE, TM_BUILD_IMAGE, PM_REMOVE)) {
		if (BUILDIMAGESTRUCT * pDelete = (BUILDIMAGESTRUCT *)msg.wParam) {
			#ifdef _DEBUG
			const LPCTSTR lpszContext [] = 
			{
				_T ("INITIAL"), 
				_T ("IMAGING"), 
				_T ("IMAGING_REFRESH"),
			};
			CString str = pDelete->GetTime ().Format (_T ("Ignoring: %c: TM_BUILD_IMAGE, ")) + GetFriendlyName () + CString (", ") + lpszContext [pDelete->GetContext ()];

			CDebug::Trace (str, true, pDelete->GetTraceFile (), pDelete->GetTraceLine ());
			#endif //_DEBUG

			delete pDelete;
		}
	}
}

void CLocalHead::DownloadImage (BUILDIMAGESTRUCT * pBuild)
{
	ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);

	if (!m_bPrinting) {
		//if (theApp.GetImagingMode () == IMAGE_ON_EOP) { ASSERT (!m_listImgUpdDesc.IsEmpty ()); }

		if (!m_listImgUpdDesc.IsEmpty ()) {
			tagImgUpdateDesc * pUpdate = NULL;

			//TRACEF (GetFriendlyName ());

			if (theApp.GetImagingMode () == IMAGE_ON_PHOTOCELL) {
				pUpdate = (tagImgUpdateDesc *) m_listImgUpdDesc.RemoveHead ();
				PUCHAR p = m_pImageBuffer;
				p += pUpdate->wStartCol * pUpdate->nBPC;

				/*
				#ifdef _DEBUG
				LPCTSTR lpszFile = _T ("c:\\temp\\Debug\\TxImage.bit");

				if (FILE * fp = _tfopen (lpszFile, _T ("wb"))) {
					CString str (lpszFile);
					int cx = pUpdate->wEndCol;
					int cy = m_Config.m_HeadSettings.GetChannels ();
					CBitmap bmp;
					CxImage img;

					VERIFY (bmp.CreateBitmap (cy, cx, 1, 1, m_pImageBuffer));
					img.CreateFromHBITMAP (bmp);

					str.Replace (_T (".bit"), _T ("_PRINTER.bmp"));
					SAVEBITMAP (CxBitmap (img.MakeBitmap ()), str);

					str.Replace (_T ("_PRINTER.bmp"), _T ("_EDITOR.bmp"));
					img.Flip ();
					img.RotateLeft ();
					img.Negative ();
					SAVEBITMAP (CxBitmap (img.MakeBitmap ()), str);

					TRACEF (lpszFile);
					fwrite (m_pImageBuffer, pUpdate->lLen, 1, fp);
					fclose (fp);
				}
				#endif
				*/

				m_bImageBuilt = false;

				if (theApp.IsMPHC ()) {
					if (IsUSB ()) {
						#ifdef _DEBUG
						//{ CString str; str.Format (_T ("%s, nBPC: %d, start: %d, end: %d"), GetFriendlyName (), pUpdate->nBPC, pUpdate->wStartCol, GetUsbImgLen (pUpdate->wStartCol, pUpdate->wEndCol)); TRACEF (str); }
						#endif

						theApp.GetUSBDriver ()->UpdateImageData ((UINT)m_hDeviceID, p, pUpdate->nBPC, pUpdate->wStartCol, GetUsbImgLen (pUpdate->wStartCol, pUpdate->wEndCol));

						/*
						#ifdef _DEBUG
						{
							int cy = abs (pUpdate->wStartCol - pUpdate->wEndCol);
							ULONG lBytes = 64 * (int)(ceil (pUpdate->nBPC *  cy) / 64.0);
							CxImage img;

							img.CreateFromArray (p, pUpdate->nBPC * 8, cy, 1, pUpdate->nBPC, false);
							SAVEBITMAP (CxBitmap (img.MakeBitmap (*::AfxGetMainWnd ()->GetDC ())), _T ("C:\\Temp\\Debug\\UpdateImageData.bmp"));
						}
						#endif
						*/

						/*
						#ifdef _DEBUG
						{
							int cy = abs (pUpdate->wStartCol - pUpdate->wEndCol);
							ULONG lBytes = 64 * (int)(ceil (pUpdate->nBPC *  cy) / 64.0);
							LPBYTE pBuffer = new UCHAR [lBytes];
							
							ASSERT ((lBytes % 64) == 0);

							if (theApp.GetUSBDriver ()->ReadImage ((UINT)m_hDeviceID, pBuffer, lBytes)) {
								CxImage img;

								img.CreateFromArray (pBuffer, pUpdate->nBPC * 8, cy, 1, pUpdate->nBPC, false);
								SAVEBITMAP (CxBitmap (img.MakeBitmap (*::AfxGetMainWnd ()->GetDC ())), _T ("C:\\Temp\\Debug\\ReadImage.bmp"));
							}
							else
								TRACEF (_T ("*** ReadImage (") + ToString (lBytes) + _T (" bytes) failed"));

							delete [] pBuffer;
						}
						#endif
						*/
					}
					else
						theApp.GetMPHCDriver ()->UpdateImageData (m_hDeviceID, p, pUpdate->nBPC, pUpdate->wStartCol, pUpdate->wEndCol);
				}
				else  
					theApp.GetIVDriver ()->UpdateImageData (m_hDeviceID, m_pImageBuffer, pUpdate->lLen, pUpdate->nBPC, pUpdate->wStartCol, pUpdate->wEndCol);

				m_tLastDownload = clock ();
				m_tmImage = CTime::GetCurrentTime ();
			}
			else 
				pUpdate = (tagImgUpdateDesc *) m_listImgUpdDesc.GetHead ();

			ASSERT (pUpdate);

			#if defined( _DEBUG ) && defined( __DEBUG_BUILDIMAGE__ )
			//#ifdef __SW0875__
			{
				CString strDir = FoxjetDatabase::GetHomeDir () + _T("\\Debug");
				CBitmap bmp;
				CxImage img;
				CString str;
				int cx = pUpdate->wEndCol;
				int cy = m_Config.m_HeadSettings.GetChannels ();
				const LPCTSTR lpszContext [] = 
				{
					_T ("INITIAL"), 
					_T ("IMAGING"), 
					_T ("IMAGING_REFRESH"),
				};
				__int64 lCount = m_Status.m_lCount;

				if (theApp.GetImagingMode () == IMAGE_ON_EOP)
					lCount++;

				str.Format (_T ("%s\\%04I64d [%s, %s].bmp"), 
					strDir, 
					lCount, 
					GetFriendlyName (),
					pBuild ? lpszContext [BindTo ((int)pBuild->GetContext (), 0, (int)ARRAYSIZE (lpszContext))] : _T ("NULL"));
				VERIFY (bmp.CreateBitmap (cy, cx, 1, 1, m_pImageBuffer));

				m_strDebugCount.Format (_T (" [%04d]"), m_Status.m_lCount);

				img.CreateFromHBITMAP (bmp);
				img.RotateLeft ();
				img.Flip ();

				SAVEBITMAP (FoxjetCommon::CxBitmap (img.MakeBitmap ()), str);
				//VERIFY (img.Save (str, CXIMAGE_FORMAT_BMP));
			}
			#endif //_DEBUG

			if (theApp.GetImagingMode () == IMAGE_ON_PHOTOCELL) {
				if (theApp.IsIV ())
					theApp.GetIVDriver ()->EnablePrint (m_hDeviceID, true);

				delete pUpdate;
			}
		}
	}
}

bool CLocalHead::CanDownloadImageSegment ()
{
	ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);
	return m_bImageBuilt && !m_listImgUpdDesc.IsEmpty ();
}

void CLocalHead::FreeImgUpdDesc ()
{
	ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);
	CHead::FreeImgUpdDesc ();
	m_bImageBuilt = false;
}

void CLocalHead::OnDownloadImageSegment (UINT nParam, LONG lParam)
{
	ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);
	bool * pbResult = (bool *)nParam;

	if (pbResult)
		* pbResult = false;

	if (CanDownloadImageSegment ()) {
		tagImgUpdateDesc * pUpdate = (tagImgUpdateDesc *)m_listImgUpdDesc.GetHead ();
		const double dSegmentLen = 1.0;
		const int nRes = IsUSB () ? pUpdate->wEndCol : (int)(m_Config.GetHeadSettings ().GetRes () * dSegmentLen);
		long lStart, lEnd;
		bool bDelete = false;
		int nDirection = m_Config.GetHeadSettings().m_nDirection;

		if (pbResult)
			* pbResult = true;

		//if (m_Config.GetHeadSettings().m_bInverted ^ m_Config.GetHeadSettings().m_bUpsidedown)
		//	nDirection = (nDirection == LTOR) ? RTOL : LTOR;

		if (nDirection == RTOL) {
			lStart					= (long)pUpdate->wCurrentCol;
			lEnd					= (long)pUpdate->wCurrentCol + (int)nRes;// - 1;
			pUpdate->wCurrentCol	+= (UINT)nRes;

			if ((lEnd + 1) >= (long)pUpdate->wEndCol) {
				lEnd = (long)pUpdate->wEndCol;
				bDelete = true;
			}
		}
		else {
			lStart					= (long)pUpdate->wCurrentCol - ((int)nRes);// - 1);
			lEnd					= (long)pUpdate->wCurrentCol;
			pUpdate->wCurrentCol	-= (UINT)nRes;

			if ((lStart - 1) <= (long)pUpdate->wStartCol) {
				lStart = (long)pUpdate->wStartCol;
				bDelete = true;
			}
		}

		ASSERT (lStart < lEnd);

		{
			PUCHAR p = m_pImageBuffer;
			WORD wCols = (WORD)(lEnd - lStart);
		
			#if defined( _DEBUG ) && defined( __DEBUG_BUILDIMAGE__ )
			{ 
				CString str; 
				str.Format (_T ("downloaded: %s [%-5d, %-5d] [%-5d] [%s]"), 
					GetFriendlyName (), lStart, lEnd, wCols, CTime::GetCurrentTime ().Format (_T ("%H:%M:%S"))); 
				TRACEF (str); 
			}
			#endif

			p += lStart * pUpdate->nBPC;

			#ifdef _DEBUG
			{ CString str; str.Format (_T ("%s: nBPC: %d, start: %d, end: %d [%d]"), GetFriendlyName (), pUpdate->nBPC, (WORD)lStart, GetUsbImgLen ((WORD)lStart, wCols), wCols); TRACEF (str); }
			#endif

			if (theApp.IsMPHC ()) {
				if (IsUSB ())
					theApp.GetUSBDriver ()->UpdateImageData ((UINT)m_hDeviceID, p, pUpdate->nBPC, (WORD)lStart, GetUsbImgLen ((WORD)lStart, wCols));
				else
					theApp.GetMPHCDriver ()->UpdateImageData (m_hDeviceID, p, pUpdate->nBPC, (WORD)lStart, wCols);
			}
			else  
				theApp.GetIVDriver ()->UpdateImageData (m_hDeviceID, m_pImageBuffer, pUpdate->lLen, pUpdate->nBPC, (WORD)lStart, wCols);
		}

		/*
		if (bDelete) {
			#ifdef _DEBUG
			{
				LARGE_INTEGER now;
				CString str;

				::QueryPerformanceCounter (&now);
				double dDiff = (double)(now.QuadPart - pUpdate->tm.QuadPart) / (double)m_freq.QuadPart;
				str.Format ("%-10s [%.08f]: [%-5d, %-5d]%s", GetFriendlyName (), dDiff, lStart, lEnd, bDelete ? " [delete]" : "");
				TRACEF (str);
			}
			#endif
		}
		*/

		if (bDelete) {
			FreeImgUpdDesc ();
			m_tLastDownload = clock ();
			m_tmImage = CTime::GetCurrentTime ();
		}
	}

	if (HANDLE h = (HANDLE)lParam)
		::SetEvent (h);
}

bool CLocalHead::Load (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::TASKSTRUCT & task, 
					   const FoxjetDatabase::BOXSTRUCT & box, const CMap <ULONG, ULONG, ULONG, ULONG> & mapHeads)
{
	bool bResult = CHead::Load (db, task, box, mapHeads);

	if (bResult) {
		Sw0848Thread::CSw0848Thread * pSw0848Thread	= m_params.m_pSw0848Thread;

		m_sw0848Def.m_v.RemoveAll ();
		//m_countLast = CHead::CHANGECOUNTSTRUCT (0);

		if (IsMaster () && (/* m_Config.m_Line.m_lID && */ pSw0848Thread)) {
			Sw0848Dlg::CSw0848Map map;

			Sw0848Dlg::CSw0848Dlg::Load (map);

			TRACEF (map.m_dsn.m_strInput);
			TRACEF (map.m_table.m_strInput);

			const CString strSQL = _T ("SELECT * FROM [") + map.m_table.m_strInput + _T ("]");
			COdbcCache db = COdbcDatabase::Find (map.m_dsn.m_strInput);
			COdbcCache rstCache = COdbcDatabase::Find (db.GetDB (), map.m_dsn.m_strInput, strSQL);
			COdbcRecordset & rst = rstCache.GetRst ();

			for (int i = 0; i < map.m_v.GetSize (); i++) {
				if (CSw0848FieldsDlg::m_strTimestamp.CompareNoCase (map.m_v [i].m_strInput) != 0) {
					Sw0848Thread::CPair data;
					CString strField = map.m_v [i].m_strInput;
					const CString strFind = _T ("].[");
					int nIndex = strField.Find (strFind);

					if (nIndex != -1) {
						int nStart = nIndex + strFind.GetLength ();
						
						strField = strField.Mid (nStart);
						strField.Replace (_T ("]"), _T (""));
					}

					data.m_strField = map.m_v [i].m_strInput;
					data.m_strData = rst.GetFieldValue (strField);

					TRACEF (data.m_strField + " --> " + data.m_strData);

					m_sw0848Def.m_v.Add (data);
				}
			}
		}
	}

	return bResult;
}

DWORD CLocalHead::GetErrorMask () const
{
	DWORD dw = HEADERROR_LOWINK;
	CLocalHead * pThis = (CLocalHead *)(LPVOID)this; // const --> non-const

	for (int i = 0; i < m_vErrors.GetSize (); i++)
		dw |= pThis->m_vErrors [i].m_dwType;

	return dw;
}

void CLocalHead::OnTriggerWDT (UINT wParam, LONG lParam)
{
	TRACEF (_T ("OnTriggerWDT: ") + m_Config.m_HeadSettings.m_strUID);

	if (IsWDTEnabled ()) {
		if (theApp.IsMPHC ()) {
			CMphc * pDriver = theApp.GetMPHCDriver ();

			if (m_motherboard == MOTHERBOARD_REV2) {
				tagPeak603VLWatchdog w;

				memset (&w, 0, sizeof (w));

				w._Interval		= 0; 
				w._IntervalType = 0; 
				w._NMIEnable	= 0;
				w._ResetEnable	= 1;
				w._WDTEnable	= 1;
				w._WDTEnable	= 1;

				pDriver->WriteWatchdog (m_hDeviceID, w);
			}
			else {
				UCHAR c = 0;

				pDriver->ReadWatchdog (m_hDeviceID, c);
				pDriver->WriteWatchdog (m_hDeviceID, 0x01);
			}

			::Sleep (5000);
		}
	}
}

bool CLocalHead::IsWDTEnabled ()
{
	if (IsUSB ()) 
		return false;
#ifdef _DEBUG
	return (_tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16) == 0x300) && m_bWDT;
#endif

	return 
		(_tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16) == 0x300) &&
		!theApp.m_bDemo &&
		m_bWDT;
}

void CLocalHead::ProgramWDT ()
{
	const ULONG lAddr = _tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16);

	if (lAddr == 0x300) {
		TRACEF (_T ("ProgramWDT: ") + m_Config.m_HeadSettings.m_strUID);
		if (CMphc * pDriver = theApp.GetMPHCDriver ()) {
			if (m_motherboard == MOTHERBOARD_REV2) {
				tagPeak603VLWatchdog w;

				memset (&w, 0, sizeof (w));
				pDriver->WriteWatchdog (m_hDeviceID, w);

				w._Interval		= (m_lWDT & 0x07);
				w._IntervalType = (m_lWDT & 0x08);
				w._NMIEnable	= 0;
				w._ResetEnable	= 1;
				w._WDTEnable	= 1;
				w._WDTEnable	= 1;

				pDriver->WriteWatchdog (m_hDeviceID, w);
			}
			else {
				UCHAR c = 0;

				pDriver->ReadWatchdog (m_hDeviceID, c);
				
				c = (UCHAR)(m_lWDT & 0x07); 
				pDriver->WriteWatchdog (m_hDeviceID, c);
			}
		}
	}
}

void CLocalHead::DisableWDT ()
{
	const ULONG lAddr = _tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16);

	if (lAddr == 0x300) {
		TRACEF (_T ("DisableWDT: ") + m_Config.m_HeadSettings.m_strUID);

		if (CMphc * pDriver = theApp.GetMPHCDriver ()) {
			if (!IsUSB ()) {
				if (m_motherboard == MOTHERBOARD_REV2) {
					tagPeak603VLWatchdog w;

					memset (&w, 0, sizeof (w));
					pDriver->WriteWatchdog (m_hDeviceID, w);
				}
				else {
					UCHAR c = 0;

					pDriver->ReadWatchdog (m_hDeviceID, c);
				}

				if (m_strWDTFile.GetLength ()) {
					if (FILE * fp = _tfopen (m_strWDTFile, _T ("w"))) {
						CString str = _T ("NORMAL SHUTDOWN");

						fwrite (w2a (str), str.GetLength (), 1, fp);
						fclose (fp);
					}
				}
			}
		}
	}
}

CString CLocalHead::GetWDTFile ()
{
	TCHAR sz [MAX_PATH] = { 0 };

	HMODULE hModule = ::GetModuleHandle (NULL);
	::GetModuleFileName (hModule, sz, ARRAYSIZE (sz) - 1);

	const CString strPath (sz);
	VERIFY (::GetFileTitle (strPath, sz, ARRAYSIZE (sz) - 1) == 0);
	return FoxjetDatabase::GetHomeDir () + _T ("\\") + FoxjetFile::GetFileTitle (sz) + _T ("_WDT.log");
}

void CLocalHead::StrokeWDT ()
{
	const ULONG lAddr = _tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16);

	if (lAddr == 0x300) {
		TRACEF (_T ("StrokeWDT: ") + m_Config.m_HeadSettings.m_strUID);

		if (CMphc * pDriver = theApp.GetMPHCDriver ()) {
			if (!theApp.m_bDemo) {
				if (m_motherboard == MOTHERBOARD_REV2) {
					tagPeak603VLWatchdog w;

					pDriver->ReadWatchdog (m_hDeviceID, w);
				}
				else {
					UCHAR c = (UCHAR)(m_lWDT & 0x07); 

					pDriver->WriteWatchdog (m_hDeviceID, c);
				}
			}

			ULONG lTimeout = max (2, min (m_lCardTimeout, m_lWDT) / 2);
			CTimeSpan tm = CTime::GetCurrentTime () - m_tmWDT;
			ULONG lWDT = (ULONG)tm.GetTotalSeconds ();

			if (lWDT >= lTimeout) {
				if (m_strWDTFile.GetLength ()) {
					if (FILE * fp = _tfopen (m_strWDTFile, _T ("w"))) {
						CString str = CTime::GetCurrentTime ().Format (_T ("%c"));

						fwrite (w2a (str), str.GetLength (), 1, fp);
						TRACEF (str + _T (" [") + ToString (lTimeout) + _T ("]"));
						fclose (fp);
						m_tmWDT = CTime::GetCurrentTime ();
					}
				}
			}
		}
	}
}

void CLocalHead::OnDownloadImages (UINT wParam, LONG lParam)
{
	if (IsMaster () || m_Config.m_HeadSettings.m_nPhotocell != SHARED_PC) {
		TRACEF (_T ("CLocalHead::OnDownloadImages: ") + GetFriendlyName ());
		DownloadImages (m_vSlaves);
	}
}

/* TODO: rem
bool CLocalHead::IsUSB ()
{
	if (!m_pbIsUSB) {
		ULONG lAddr = _tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16);
		CString str = Foxjet3d::CHeadDlg::GetPHC (lAddr);

		m_pbIsUSB = new bool;
		* m_pbIsUSB = str.Find (_T ("USB")) != -1 ? true : false;
	}

	return * m_pbIsUSB;
}
*/

WORD CLocalHead::GetUsbImgLen (WORD wStartCol, WORD wEndCol) const
{
	const int nBoundary = 32;
	int cy = abs (wStartCol - wEndCol);
	WORD wBytes = nBoundary * (int)(ceil (cy / (double)nBoundary));

/*
#ifdef _DEBUG
	CString str;
	str.Format (_T ("GetUsbImgLen (wStartCol: %d, wEndCol: %d): %d"), wStartCol, wEndCol, wBytes);
	TRACEF (str);
#endif
*/

	return wBytes;
}

BOOL CLocalHead::PreTranslateMessage (MSG * pMsg)
{
	bool bHandled = false;

	TraceMessage (pMsg);

	if (pMsg) {
		switch (pMsg->message) {
		case TM_FIND_SLAVES:
			FindSlaves ();
			bHandled = true;
			break;
		case TM_SIM_PHOTOCELL:
			::SetEvent (pMsg->wParam ? m_hEOPEvent : m_hPhotoEvent);
			bHandled = true;
			break;
		case TM_IS_PRINTING:
			* ((bool *)pMsg->wParam) = m_bPrinting;
			bHandled = true;
			break;
		case TM_IS_USB:
			* ((bool *)pMsg->wParam) = IsUSB ();
			bHandled = true;
			break;
		case TM_SET_USERDATA:
			if (CHead::SETUSERDATASTRUCT * p = (CHead::SETUSERDATASTRUCT *)pMsg->wParam)
				m_vSetUserElements.SetAt (p->m_strPrompt, p->m_strData);
			bHandled = TRUE;
			break;
		case TM_IS_DBGVIEW:
			* ((bool *)pMsg->wParam) = m_bDebug;
			bHandled = TRUE;
			break;
		case TM_IS_SHOW_PRINT_CYCLE:
			* ((bool *)pMsg->wParam) = m_bShowPrintCycle;
			bHandled = TRUE;
			break;
		case TM_SET_SHOW_PRINT_CYCLE:
			m_bShowPrintCycle = pMsg->wParam ? 1 : 0;
			bHandled = TRUE;
			break;
		case TM_GET_IMAGE_BUFFER_SIZE:
			* ((DWORD *)pMsg->wParam) = GetImageBufferSize ();
			bHandled = TRUE;
			break;
		case TM_ABORT:
			Abort ();
			bHandled = TRUE;
			break;
		case TM_SET_PHOTOCELL_OFFSETS:
			if (PHOTOCELLOFFSETSTRUCT * p = (PHOTOCELLOFFSETSTRUCT *)pMsg->wParam)
				SetPhotocellOffsets (p->m_lMinPhotocellDelay, p->m_lMaxPhotocellDelay, p->m_nMaxFlush);
			bHandled = TRUE;
			break;
		case TM_IS_PHOTOENABLED:
			* ((bool *)pMsg->wParam) = m_bPhotoEnabled;
			bHandled = TRUE;
			break;
		case TM_IS_ONE_TO_ONE_PRINT_ENABLED:
			* ((bool *)pMsg->wParam) = IsOneToOnePrint ();
			bHandled = TRUE;
			break;
		case TM_ENABLE_PHOTO:
			OnEnablePhotocell (pMsg->wParam, 0);
			bHandled = TRUE;
			break;
		case TM_GET_PHOTO_EVENT_HANDLE:
			* ((HANDLE *)pMsg->wParam) = m_hPhotoEvent;
			//* ((HANDLE *)pMsg->wParam) = m_hEOPEvent;
			bHandled = TRUE;
			break;
		case TM_GET_EOP_EVENT_HANDLE:
			* ((HANDLE *)pMsg->wParam) = m_hEOPEvent;
			bHandled = TRUE;
			break;
		case TM_GET_SERIAL_START:
			* ((bool *)pMsg->wParam) = m_bSerialStart;
			bHandled = TRUE;
			break;
		case TM_SET_SERIAL_START:
			m_bSerialStart =  pMsg->wParam ? true : false;
			bHandled = TRUE;
			break;
		case TM_NEXT_SET_DYNAMIC_INDEX:
			SetDynamicIndex (pMsg->wParam);
			bHandled = TRUE;
			break;
		case TM_SET_COUNT_UNTIL:
			{
				__int64 * p = (__int64 *)pMsg->wParam;
				SetCountUntil (* p);
				bHandled = TRUE;
			}
		case TM_GET_COUNT_UNTIL:
			{
				__int64 * p = (__int64 *)pMsg->wParam;
				* p = m_Status.m_lCountUntil;
				bHandled = TRUE;
			}
			break;
		case TM_SET_PRINT_DRIVER:
			{
				m_bPrintDriver = pMsg->wParam ? true : false;
				bHandled = TRUE;

				/*
				if (m_bPrintDriver) {
					CMainFrame * pFrame = (CMainFrame *)theApp.m_pMainWnd;
					pFrame->m_pControlView->PostMessage (WM_SET_PRINT_DRIVER, m_Config.m_HeadSettings.m_lID, m_bPrintDriver);
				}
				*/
			}
			break;
		case TM_GET_PRINT_DRIVER:
			{
				bool * p = (bool *)pMsg->wParam;
				* p = m_bPrintDriver;
				bHandled = TRUE;
			}
			break;			
		case TM_GET_INK_LEVEL:
			{
				if (pMsg->wParam) {
					INKCODE * p = (INKCODE *)pMsg->wParam;
					* p = m_inkcode;
				}
				else { ASSERT (0); }

				bHandled = TRUE;
			}
			break;
		case TM_CHECK_INK_WARNING_LEVEL:
			{
				int * p = (int *)pMsg->wParam;
				m_nInkWarning = INK_CODE_WARNGING_LEVEL_0;
				* p = GetInkWarningLevel ();
				bHandled = TRUE;
			}
			break;
		case TM_DO_CHECK_INK_CODE:
			{
				CheckInkCode ();
				bHandled = TRUE;
			}
			break;
		case TM_INK_CODE_CHANGE:
			{
				CString strCode = (LPCTSTR)pMsg->wParam;
				ReadInkCode (strCode);
				//m_inkcode.m_lUsed = 0;
				bHandled = TRUE;
			}
			break;
		case TM_HEAD_PRINT_ONCE:
			{
				TRACEF (std::to_string (m_Status.m_lCount).c_str ());
				SetCountUntil (m_Status.m_lCount + 1);
				TRACEF (std::to_string (m_Status.m_lCountUntil).c_str ());
				bHandled = TRUE;
			}
			break;
		case TM_DOWNLOAD_DYNDATA:
			{
				if (LocalHead::DYNDATA * pData = (LocalHead::DYNDATA *)pMsg->wParam) {
					if (pData->m_nIndex >= 0 && pData->m_nIndex < ARRAYSIZE (m_sz0864)) 
						_tcsncpy (m_sz0864 [pData->m_nIndex], pData->m_strData, ARRAYSIZE (m_sz0864 [0]));
					delete pData;
				}
				bHandled = TRUE;
			}
			break;
		case WM_GET_TASK_STATE:
			if (TASKSTATE * p = (TASKSTATE*)pMsg->wParam) 
				*p = m_eTaskState;
			bHandled = TRUE;
			break;
		}
	}

	#ifdef _DEBUG
	if (bHandled)
		ASSERT (ISPOSTMESSAGESTRUCT (pMsg->message));
	#endif

	if (bHandled && ISPOSTMESSAGESTRUCT (pMsg->message)) {
		if (POSTMESSAGESTRUCT * p = (POSTMESSAGESTRUCT *)pMsg->lParam) {
			if (p->m_hEvent)
				::SetEvent (p->m_hEvent);

			delete p;
		}

		return TRUE;
	}

	return CHead::PreTranslateMessage (pMsg);
}

WORD CLocalHead::CalcPrintDelay (ULONG lPhotocellDelay)
{
	WORD w = 0;
	FoxjetDatabase::HEADSTRUCT HS = m_Config.GetHeadSettings();

	switch (HS.m_nEncoderDivisor) {
		case 1:	// 300
		default:
		case 2:	// 150
			w = (WORD)(((double)lPhotocellDelay / (double)1000) * ((double)HS.m_nHorzRes / (double)HS.m_nEncoderDivisor));
			break; 
		case 3:	// 200	
			w = (WORD)(((double)lPhotocellDelay / (double)1000) * (((double)HS.m_nHorzRes * 2.0) / (double)HS.m_nEncoderDivisor));
			break; 
	}

	return w;
}

void CLocalHead::PostTimingInfo (const LARGE_INTEGER & tm, const CString & strDesc)
{
	LARGE_INTEGER counter; 
	CString str;

	::QueryPerformanceCounter (&counter); 
	str.Format (_T ("%s: %.8fs"), strDesc, ((double)counter.QuadPart - (double)tm.QuadPart) / (double)m_freq.QuadPart); 

	if (theApp.IsImageTimingEnabled ()) {
		//TRACEF (str);

		while (!::PostMessage (m_hControlWnd, WM_DIAGNOSTIC_DATA, (WPARAM)new CString (str), 0))
			::Sleep (0);
	}
}

void CLocalHead::OnSignalRefresh (WPARAM wParam, LPARAM lParam)
{
	m_dwImageRefresh = (DWORD)lParam;
	::SetEvent (m_hImageRefresh);
}

void CLocalHead::SetDynamicIndex (UINT nIndex)
{
	Head::CElementPtrArray v;

	GetElements (v, FoxjetIpElements::DYNAMIC_TEXT);
	GetElements (v, FoxjetIpElements::DYNAMIC_BARCODE);

	for (int i = 0; i < v.GetSize (); i++) {
		if (FoxjetIpElements::CDynamicTextElement * p = DYNAMIC_DOWNCAST (FoxjetIpElements::CDynamicTextElement, v [i])) 
			p->SetDynamicID (nIndex);
		if (FoxjetIpElements::CDynamicBarcodeElement * p = DYNAMIC_DOWNCAST (FoxjetIpElements::CDynamicBarcodeElement, v [i])) 
			p->SetDynamicID (nIndex);

		v [i]->SetRedraw ();
	}
}

void CLocalHead::IdleAll ()
{
	m_bSerialStart = false;

	for (POSITION pos = m_params.m_listHeads.GetStartPosition(); pos; ) {
		ULONG lKey;
		CHead * pLookup = NULL;

		m_params.m_listHeads.GetNextAssoc (pos, lKey, (void *&)pLookup);
		ASSERT (pLookup);

		Head::CHeadInfo info = CHead::GetInfo (pLookup);

		while (!::PostThreadMessage (info.GetThreadID (), TM_IDLE_TASK, 0, 0))
			::Sleep (0);
	}
}


void CLocalHead::StopAll ()
{
	for (POSITION pos = m_params.m_listHeads.GetStartPosition(); pos; ) {
		ULONG lKey;
		CHead * pLookup = NULL;

		m_params.m_listHeads.GetNextAssoc (pos, lKey, (void *&)pLookup);
		ASSERT (pLookup);

		Head::CHeadInfo info = CHead::GetInfo (pLookup);

		while (!::PostThreadMessage (info.GetThreadID (), TM_STOP_TASK, 0, 0))
			::Sleep (0);
	}

	::PostMessage (m_hControlWnd, WM_REFRESH_IMAGE, (WPARAM)m_Config.m_Line.m_lID, (LPARAM)1000);
}

int CLocalHead::GetInkWarningLevel () const
{
	int nResult = INK_CODE_WARNGING_LEVEL_0;

	if (m_Config.m_HeadSettings.m_bEnabled && m_inkcode.m_bEnabled) {
		int nPct = (double)m_inkcode.m_lUsed / (double)m_inkcode.m_lMax * 100.0;

		if (nPct >= INK_CODE_THRESHOLD_ERROR) 
			nResult |= INK_CODE_WARNGING_LEVEL_3;
		//else if (nPct >= INK_CODE_THRESHOLD_WARNING)
		//	nResult |= INK_CODE_WARNGING_LEVEL_2;
		else if (nPct >= INK_CODE_THRESHOLD_WARNING) 
			nResult |= INK_CODE_WARNGING_LEVEL_1;

		if (m_Status.m_nInkLevel == LOW_INK)
			nResult |= INK_CODE_WARNGING_LOW_INK;

		if (m_Status.m_nInkLevel == INK_OUT)
			nResult |= INK_CODE_WARNGING_INK_OUT;

		if (m_inkcode.m_strCode.IsEmpty ())
			nResult |= INK_CODE_WARNGING_INVALID_CODE;


		/*
		#ifdef _DEBUG 
		{	// INK_OUT, HEADERROR_OUTOFINK
			static CTime tmStart = CTime::GetCurrentTime ();
			CTimeSpan tmDiff = CTime::GetCurrentTime () - tmStart;
			int n = tmDiff.GetTotalSeconds ();

			if (n > 15)
				nResult |= INK_CODE_WARNGING_LOW_INK;
			else if (n > 30)
				nResult |= INK_CODE_WARNGING_INK_OUT;
		}
		#endif
		*/

	}

	return nResult;
}

DWORD CLocalHead::GetErrorState () const
{
	const DWORD dwLast = m_dwLastState;
	const DWORD dw = CHead::GetErrorState ();

	if ((dw & HEADERROR_LOWINK) && !(dwLast & HEADERROR_LOWINK)) {
		int n = GetInkWarningLevel ();

		if (n & INK_CODE_WARNGING_LEVEL_3)  // will cause cancel button to be enabled if low ink is not removed
			n &= ~INK_CODE_WARNGING_LOW_INK;

		TRACER (CInkSerialNumberDlg::Trace (GetInkWarningLevel ()) + _T (" --> ") + CInkSerialNumberDlg::Trace (n));

		while (!::PostMessage (m_hControlWnd, WM_INK_USAGE_WARNING, m_Config.m_HeadSettings.m_lID, n))
			::Sleep (0);

		//POST_THREAD_MESSAGE (GetInfo (), TM_DO_CHECK_INK_CODE, 0);
	}

	return dw;
}


void CLocalHead::CheckInkCode ()
{
	if (m_Config.m_HeadSettings.m_bEnabled && m_inkcode.m_bEnabled) {
		int nPct = (double)m_inkcode.m_lUsed / (double)m_inkcode.m_lMax * 100.0;
		const int nWarning = GetInkWarningLevel ();
		const int nLevel = nWarning & INK_CODE_WARNGING_LEVEL_MASK;
		const int nState = nWarning & INK_CODE_WARNGING_STATE_MASK;

		TRACER (ToString (m_Config.m_HeadSettings.m_strName) + _T (": ") + 
			+ FoxjetCommon::FormatI64 (m_inkcode.m_lUsed) + _T (" / ") + FoxjetCommon::FormatI64 (m_inkcode.m_lMax) 
			+ _T (" = ") + FoxjetDatabase::ToString (nPct) + _T (" %, ") +
			CInkSerialNumberDlg::Trace (nWarning) + _T (" [") + CInkSerialNumberDlg::Trace (m_nInkWarning) + _T ("]"));

		if (m_nInkWarning != nWarning) {
			TRACER (ToString (m_Config.m_HeadSettings.m_lID) + _T (": ") + CInkSerialNumberDlg::Trace (nWarning) + _T (" [") + CInkSerialNumberDlg::Trace (m_nInkWarning) + _T ("]"));
			int nBreak = 0;
		}

		if (nState & INK_CODE_WARNGING_INVALID_CODE) {
			while (!::PostMessage (m_hControlWnd, WM_INK_USAGE_WARNING, m_Config.m_HeadSettings.m_lID, m_nInkWarning = nWarning))
				::Sleep (0);
			return;
		}

		switch (nLevel) 
		{
		default:
			{
				if (nState) {
					if (m_nInkWarning != nWarning)
						while (!::PostMessage (m_hControlWnd, WM_INK_USAGE_WARNING, m_Config.m_HeadSettings.m_lID, m_nInkWarning = nWarning))
							::Sleep (0);
				}
				else {
					if (m_nInkWarning != nWarning) 
						while (!::PostMessage (m_hControlWnd, WM_INK_USAGE_WARNING, m_Config.m_HeadSettings.m_lID, INK_CODE_WARNGING_LEVEL_0))
							::Sleep (0);

					m_nInkWarning = INK_CODE_WARNGING_LEVEL_0;
				}
			}
			break;
		case INK_CODE_WARNGING_LEVEL_1:
			if (m_nInkWarning < INK_CODE_WARNGING_LEVEL_1)
				while (!::PostMessage (m_hControlWnd, WM_INK_USAGE_WARNING, m_Config.m_HeadSettings.m_lID, m_nInkWarning = nWarning))
					::Sleep (0);
			break;
		case INK_CODE_WARNGING_LEVEL_2:
			if (m_nInkWarning < INK_CODE_WARNGING_LEVEL_2)
				while (!::PostMessage (m_hControlWnd, WM_INK_USAGE_WARNING, m_Config.m_HeadSettings.m_lID, m_nInkWarning = nWarning))
					::Sleep (0);
			break;
		case INK_CODE_WARNGING_LEVEL_3:
			while (!::PostMessage (m_hControlWnd, WM_INK_USAGE_WARNING, m_Config.m_HeadSettings.m_lID, m_nInkWarning = nWarning)) 
				::Sleep (0);
			break;
		}
	}
}