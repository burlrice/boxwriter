// HeadInfoCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "HeadInfoCtrl.h"
#include "Color.h"
#include "Parse.h"
#include "Head.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ItiLibrary;
using namespace Head;

/////////////////////////////////////////////////////////////////////////////
CHeadInfoCtrl::CHeadItem::CHeadItem (CHead * pHead)
:	m_pHead (pHead)
{
}

CHeadInfoCtrl::CHeadItem::~CHeadItem ()
{
}

void CHeadInfoCtrl::CHeadItem::Draw (CDC & dc, const CRect & rcDest, CHeadInfoCtrl & lv)
{
	CRect rc (rcDest);

	if (m_pHead) {
		CHeadInfo info = CHead::GetInfo (m_pHead);

		int nWidth = 0, nPrevWidth = 0;
		const int nOffset = 0;//lv->GetScrollPos (SB_HORZ);
		const int nFlags = DT_END_ELLIPSIS | DT_NOPREFIX | DT_LEFT;

		for (int i = 0; i < lv.GetColumnCount (); i++) {
			CString str; 
			nPrevWidth += nWidth;
			nWidth = lv.GetColumnWidth (i);
			int nCellLeft  = nPrevWidth - nOffset;
			int nCellRight = nPrevWidth + nWidth - nOffset;
			CRect rcCell (nCellLeft, rc.top, nCellRight, rc.bottom);
			CBrush brItem (Color::rgbWhite);
			CRect rcAdjust (rcCell);

			switch (i) {
			case 0:	str = info.m_Config.m_Line.m_strName;					break;
			case 1:	str = info.GetFriendlyName();							break;
			case 2:	str = info.GetCount ();									break;
			case 3:	str.Format (_T ("%d FPM"), (int)info.GetLineSpeed ());	break;
			}

			// there is a slight gap between the bottom of the header
			// & the first item. this fills it
			if (rcAdjust.top < 20) rcAdjust.top -= 5;

			dc.FillRect (&rcAdjust, &brItem);
			dc.SetTextColor (Color::rgbBlack);

			// this keeps text from adjacent columns from mingling
			rcCell.left += 2;
			rcCell.right -= 5;

			if (rcCell.left < rcCell.right) {
				CFont * pFont = dc.SelectObject (lv.GetFont ());
				dc.DrawText (str, rcCell, nFlags);
				dc.SelectObject (pFont);
			}
		}

		//dc.FrameRect (rc, &CBrush (Color::rgbDarkGray));
	}
}

/////////////////////////////////////////////////////////////////////////////
// CHeadInfoCtrl

BEGIN_MESSAGE_MAP(CHeadInfoCtrl, CListCtrl)
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM() 
END_MESSAGE_MAP()

CHeadInfoCtrl::CHeadInfoCtrl()
:	m_nColumns (0),
	CListCtrl ()
{
}

CHeadInfoCtrl::~CHeadInfoCtrl()
{
}


/////////////////////////////////////////////////////////////////////////////
// CHeadInfoCtrl message handlers


void CHeadInfoCtrl::InitHeaders (CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> & vCols, CFont * pFont)
{
	using namespace ListCtrlImp;

	SetExtendedStyle (LVS_OWNERDRAWFIXED | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | GetExtendedStyle ());

	ASSERT (LVS_OWNERDRAWFIXED		& GetExtendedStyle ());
	ASSERT (LVS_EX_FULLROWSELECT	& GetExtendedStyle ());
	ASSERT (LVS_EX_GRIDLINES		& GetExtendedStyle ());

	if (CHeaderCtrl * pHeader = GetHeaderCtrl ()) {
		if (pFont)
			pHeader->SetFont (pFont);

		m_nColumns = vCols.GetSize ();

		for (int i = 0; i < vCols.GetSize (); i++) {
			HDITEM hdItem;
			CColumn col = vCols [i];
		
			memset (&hdItem, 0, sizeof (hdItem));

			switch (col.m_nFormat) {
			case LVCFMT_LEFT:	hdItem.fmt = HDF_LEFT;		break;
			case LVCFMT_CENTER:	hdItem.fmt = HDF_CENTER;	break;
			case LVCFMT_RIGHT:	hdItem.fmt = HDF_RIGHT;		break;
			}

			hdItem.mask = HDI_TEXT | HDI_WIDTH | HDI_FORMAT;
			hdItem.cxy = col.m_nWidth;
			hdItem.fmt = HDF_STRING | HDF_OWNERDRAW;
			hdItem.pszText = col.m_str.GetBuffer(0);

			VERIFY (pHeader->InsertItem (i, &hdItem) != -1);
		}	


		for(int i = 0; i < pHeader->GetItemCount(); i++) {
			HDITEM hdItem;

			hdItem.mask = HDI_FORMAT;
			pHeader->GetItem(i,&hdItem);
			hdItem.fmt|= HDF_OWNERDRAW;
			pHeader->SetItem(i,&hdItem);
		}

		pHeader->EnableWindow (FALSE);
	}
}

void CHeadInfoCtrl::DeleteCtrlData () 
{
	CArray <CHeadItem *, CHeadItem *> v;
	
	for (int i = 0; i < GetItemCount (); i++) 
		v.Add ((CHeadItem *)GetItemData (i));

	DeleteAllItems ();

	for (int i = 0; i < v.GetSize (); i++) 
		delete v [i];
}

bool CHeadInfoCtrl::InsertCtrlData (CHeadItem * pData) 
{
	return InsertCtrlData (pData, GetItemCount ());
}

bool CHeadInfoCtrl::InsertCtrlData (CHeadItem * pData, int nIndex) 
{
	LV_ITEM lvi;

	lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM; 
	lvi.iItem = nIndex; 
	lvi.iSubItem = 0; 
	lvi.iImage = 0;
	lvi.pszText = _T (""); 
	lvi.lParam = (LPARAM)pData;

	if (InsertItem (&lvi) != -1) {
		for (int i = 0; i < GetColumnCount (); i++)
			SetItemText (nIndex, i, FoxjetDatabase::ToString (nIndex) + _T (": ") + FoxjetDatabase::ToString (i));

		return true;//UpdateCtrlData (pData, nIndex);
	}

	return false;
}

void CHeadInfoCtrl::OnDrawItem( int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct )
{
	using namespace Color;
	using namespace FoxjetCommon;

	DRAWITEMSTRUCT dis;
	
	memcpy (&dis, lpDrawItemStruct, sizeof (dis));

	if (nIDCtl == LV_HEADS) {
		CDC dc, dcMem;
		CBitmap bmp;
		CRect rc = dis.rcItem;

		if (CWnd * p = GetDlgItem (nIDCtl)) 
			if (!p->IsWindowVisible ())
				return;

		ASSERT (dis.hDC);
		dc.Attach (dis.hDC);
		dcMem.CreateCompatibleDC (&dc);
		bmp.CreateCompatibleBitmap (&dc, rc.Width (), rc.Height ());
		CBitmap * pBmp = dcMem.SelectObject (&bmp);
		dcMem.FillRect (CRect (0, 0, rc.Width (), rc.Height ()), &CBrush (Color::rgbWhite));
		
		if (CHeadItem * pItem = (CHeadItem *)/*m_lvHeads.*/GetItemData (dis.itemID)) 
			pItem->Draw (dcMem, CRect (0, 0, rc.Width (), rc.Height ()), * this/*m_lvHeads*/);

		dc.BitBlt (rc.left, rc.top, rc.Width (), rc.Height (), &dcMem, 0, 0, SRCCOPY);
		dcMem.SelectObject (pBmp);
		dc.Detach ();
	}
	else {
		CWnd * p = GetDlgItem (dis.CtlID);
		TCHAR sz [128] = { 0 };
		::GetClassName (p->m_hWnd, sz, ARRAYSIZE (sz));
		TRACEF (sz);
		CListCtrl::OnDrawItem(nIDCtl, lpDrawItemStruct);
	}
}

void CHeadInfoCtrl::OnMeasureItem (int nIDCtl, LPMEASUREITEMSTRUCT lpmis)
{
	CWnd * p = GetDlgItem (lpmis->CtlID);
	TCHAR sz [128] = { 0 };
	::GetClassName (p->m_hWnd, sz, ARRAYSIZE (sz));
	TRACEF (_T ("CHeadInfoCtrl::OnMeasureItem: ") + FoxjetDatabase::ToString ((int)lpmis->CtlID) + _T (" ") + CString (sz));
}