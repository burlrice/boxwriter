// ControlDoc.h : interface of the CControlDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONTROLDOC_H__3855D46D_9729_11D5_8F07_00045A47C31C__INCLUDED_)
#define AFX_CONTROLDOC_H__3855D46D_9729_11D5_8F07_00045A47C31C__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxmt.h>
#include <afxcoll.h>

#include "TypeDefs.h"
//#include "ProdLine.h"
//#include "Head.h"

class CProdLine;

namespace Head
{
	class CHead;
	class CHeadInfo;
};


class CControlView;

class CControlDoc : public CDocument
{
protected: // create from serialization only
	CControlDoc();
	DECLARE_DYNCREATE(CControlDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CControlDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL CanCloseFrame(CFrameWnd* pFrame);
	//}}AFX_VIRTUAL

	void SetLocal (bool bLocal) { m_bLocal = bLocal; }
	bool IsLocal () const { return m_bLocal; }

// Implementation
public:
	void DeleteLabels ();
	void SetSelectedLine(int nIndex);
	void SetSelectedLine ( CString sLine );
	int GetLineIndex (CProdLine * pFind);
	CProdLine * GetLine (int nIndex);
	CProdLine * GetProductionLine ( CString sLine );
	CProdLine * GetSelectedLine ( void );
	CString LookupLineAssoc ( CString sAddress );
	bool RemoveActiveHead ( CString sProdLine, Head::CHead *pHead );
	bool AddActiveHead ( CString sProdLine, Head::CHead *pHead );
	bool GetActiveHeads (CString sProdLine, CPrintHeadList &List);
	bool LoadProductLines ();
	int GetProductLines(CStringList &List);
	int GetProductLines(CStringArray & v);
	int	GetProductLines (CMapStringToPtr & v);
	virtual ~CControlDoc();
	CControlView * GetView () const;
	Head::CHead * GetHeadByThreadID (DWORD dwThreadID);

	CProdLine * GetProductionLine (ULONG lLineID);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CProdLine *m_pSelectedLine;
	CMapStringToPtr m_ProductLines;
	bool m_bLocal;

public:
	CDiagnosticCriticalSection m_ConnectionListSentinel;
	CDiagnosticCriticalSection m_csLines;

// Generated message map functions
protected:
	//{{AFX_MSG(CControlDoc)
	afx_msg void OnLinesUpdated();
	afx_msg void OnViewRefreshpreview();
	//}}AFX_MSG

	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyDown (UINT nChar, UINT nRepCnt, UINT nFlags);

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONTROLDOC_H__3855D46D_9729_11D5_8F07_00045A47C31C__INCLUDED_)
