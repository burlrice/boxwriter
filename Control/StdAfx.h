// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__3855D469_9729_11D5_8F07_00045A47C31C__INCLUDED_)
#define AFX_STDAFX_H__3855D469_9729_11D5_8F07_00045A47C31C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h>  
#include <crtdbg.h> 

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <Winsock2.h>
#include "AnsiString.h"
#include "Database.h"
#include "MsgBox.h"
#include "Database.h"

using FoxjetDatabase::IsMatrix;
using FoxjetMessageBox::MsgBox;
using FoxjetDatabase::CAnsiString;

CString LoadString (UINT nID);

namespace ISA { } 
using namespace ISA;

#include "DbTypeDefs.h"
#include <afxcontrolbars.h>
#include "DiagnosticSingleLock.h"

SIZE GetDialogSize(INT nResourceId, BOOL bApproximateCalcMethod = FALSE, LPCTSTR strDllName = NULL);
bool IsSocketDataPending (SOCKET s);

#ifdef _DEBUG
CString GetStyleString (UINT n, UINT nEX);
void CheckDlgSizes ();
#endif

using FoxjetDatabase::HEADERROR_LOWTEMP;
using FoxjetDatabase::HEADERROR_HIGHVOLTAGE;
using FoxjetDatabase::HEADERROR_LOWINK;
using FoxjetDatabase::HEADERROR_OUTOFINK;

void Repaint (CWnd & wnd, LPCTSTR lpszFile, ULONG lLine);
inline void Repaint (CWnd * p, LPCTSTR lpszFile, ULONG lLine) { if (p) Repaint (* p, lpszFile, lLine); }
#define REPAINT(wnd) Repaint ((wnd), _T (__FILE__), __LINE__);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__3855D469_9729_11D5_8F07_00045A47C31C__INCLUDED_)
