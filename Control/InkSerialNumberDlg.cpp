// InkSerialNumberDlg.cpp : implementation file
//

#include "stdafx.h"
#include <vector>
#include <string>
#include "Control.h"
#include "InkSerialNumberDlg.h"
#include "afxdialogex.h"
#include "Extern.h"
#include "Database.h"
#include "Resource.h"
#include "Registry.h"
#include "Parse.h"
#include "Head.h"
#include "Host.h"
#include "Utils.h"
#include "ComPropertiesDlg.h"
#include "Comm32.h"
#include "Defines.h"
#include "StdCommDlg.h"
#include "color.h"
#include "ControlView.h"

using namespace OXMaskedEdit;
using namespace FoxjetDatabase;

struct
{
	UINT m_nLblID;
	UINT m_nTxtID;
	UINT m_nBmpID;
} static const map [] = 
{
	{ LBL_HEAD1, TXT_HEAD1, BMP_STATE1, },
	{ LBL_HEAD2, TXT_HEAD2, BMP_STATE2, },
	{ LBL_HEAD3, TXT_HEAD3, BMP_STATE3, },
	{ LBL_HEAD4, TXT_HEAD4, BMP_STATE4, },
	{ LBL_HEAD5, TXT_HEAD5, BMP_STATE5, },
	{ LBL_HEAD6, TXT_HEAD6, BMP_STATE6, },
};

#define ID_STROKE_COMM_THREAD	100
#define ID_CHECK_INK_CODE_STATE 101
#define ID_CAN_UNLOCK_UI		102

static int Find (const CArray <HEADSTRUCT, HEADSTRUCT &> & v, const HEADSTRUCT & h)
{
	const ULONG lFind = _tcstoul (h.m_strUID, NULL, 16);

	for (int i = 0; i < v.GetSize (); i++) {
		if (_tcstoul (v [i].m_strUID, NULL, 16) == lFind)
			return i;
	}

	return -1;
}

static void InsertAsc (CArray <HEADSTRUCT, HEADSTRUCT &> & v, const HEADSTRUCT & h)
{
	ULONG lInsert = _tcstoul (h.m_strUID, NULL, 16);

	for (int i = 0; i < v.GetSize (); i++) {
		ULONG lCur = _tcstoul (v [i].m_strUID, NULL, 16);
	
		if (lCur > lInsert) {
			v.InsertAt (i, HEADSTRUCT (h));
			return;
		}
	}

	v.Add (HEADSTRUCT (h));
}

static void Sort (CArray <HEADSTRUCT, HEADSTRUCT &> & v)
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vTmp;

	vTmp.Append (v);
	v.RemoveAll ();

	for (int i = 0; i < vTmp.GetSize (); i++) {
		if (Find (v, vTmp [i]) == -1) {
			InsertAsc (v, vTmp [i]);
		}
	}
}

static void GetHeads (CArray <HEADSTRUCT, HEADSTRUCT &> & v)
{
	if (CControlDoc * pDoc = (CControlDoc *)theApp.GetActiveDocument ()) {
		CStringArray vLines;

		ASSERT (pDoc);
		pDoc->GetProductLines (vLines);
		
		for (int i = 0; i < vLines.GetSize (); i++) {
			if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
				CPrintHeadList list;

				pLine->GetActiveHeads (list);

				for (POSITION pos = list.GetStartPosition(); pos != NULL; ) {
					Head::CHead * pHead = NULL;
					DWORD dwKey = 0;
					bool bResult = false;

					list.GetNextAssoc (pos, dwKey, (void *&)pHead);
					Head::CHeadInfo info = Head::CHead::GetInfo (pHead);

					v.Add (info.m_Config.m_HeadSettings);
				}
			}
		}
	}

	Sort (v);
}
const DWORD CInkSerialNumberDlg::m_dwLockoutTimeSeconds = 4 * 60; //30 * 60;

// CInkSerialNumberDlg dialog

IMPLEMENT_DYNAMIC(CInkSerialNumberDlg, CDialogEx)

CInkSerialNumberDlg::CInkSerialNumberDlg(CWnd* pParent, ULONG lHeadID)
:	m_pParent (pParent),
	m_hExit (NULL),
	m_lHeadID (lHeadID),
	m_nErroneousCodes (0),
	m_ptmLockout (NULL),
	CDialogEx(CInkSerialNumberDlg::IDD, pParent)
{
	m_hBmpOK		= (HBITMAP)::LoadImage (::GetModuleHandle (NULL), MAKEINTRESOURCE (IDB_GREEN_CHECK), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE);
	m_hBmpCancel	= (HBITMAP)::LoadImage (::GetModuleHandle (NULL), MAKEINTRESOURCE (IDB_RED_X), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE);

	m_bmpTrueCode.LoadBitmap (IDB_TRUECODE);

	Encrypted::Enumerate (FoxjetDatabase::Encrypted::InkCodes::GetRegSection (), m_vRegistry);
}

CInkSerialNumberDlg::~CInkSerialNumberDlg()
{
	KillThreads ();
	::CloseHandle (m_hExit);

	if (m_ptmLockout) {
		delete m_ptmLockout;
		m_ptmLockout = NULL;
	}

	if (m_hBmpOK) {
		::DeleteObject (m_hBmpOK);
		m_hBmpOK = NULL;
	}

	if (m_hBmpCancel) {
		::DeleteObject (m_hBmpCancel);
		m_hBmpCancel = NULL;
	}
}

void CInkSerialNumberDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);

	ASSERT (ARRAYSIZE (m_txt) == ARRAYSIZE (::map));

	for (int i = 0; i < ARRAYSIZE (m_txt); i++) 
		DDX_Control (pDX, map [i].m_nTxtID, m_txt [i]);
}


BEGIN_MESSAGE_MAP(CInkSerialNumberDlg, CDialogEx)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_MESSAGE (WM_HEAD_SERIAL_DATA, OnSerialData)
	ON_WM_DRAWITEM()
	ON_MESSAGE (WM_INK_CODE_SET_STATE, OnSetInkCodeState)
	ON_MESSAGE (WM_SET_WARNING_MESSAGES, OnSetHeadWarnings)
	ON_MESSAGE (WM_GET_MAX_INK_WARNING, OnGetMaxWarningLevel)
	ON_REGISTERED_MESSAGE (WM_INK_CODE_LOCKOUT, OnInkCodeLockout)
	ON_CONTROL_RANGE(EN_CHANGE, TXT_HEAD1, TXT_HEAD6, OnEnChangeHead)
	ON_MESSAGE (WM_INKCODEDLG_SETHEAD, OnSetHead)
END_MESSAGE_MAP()

CString CInkSerialNumberDlg::Trace (INK_CODE_WARNGING_LEVEL level)
{
	#define MAKEMAPENTRY(s) { _T (#s),  (INK_CODE_WARNGING_##s), }

	struct
	{
		LPCTSTR m_lpsz;
		DWORD m_dw;
	} 
	static const map [] = 
	{
		MAKEMAPENTRY (LEVEL_0),
		MAKEMAPENTRY (LEVEL_1),
		MAKEMAPENTRY (LEVEL_2),
		MAKEMAPENTRY (LEVEL_3),
		MAKEMAPENTRY (LOW_INK),
		MAKEMAPENTRY (INK_OUT),
		MAKEMAPENTRY (INVALID_CODE),
	};

	#undef MAKEMAPENTRY

	std::vector <std::wstring> v;

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		if ((DWORD)level & map [i].m_dw)
			v.push_back (std::wstring (map [i].m_lpsz));
	}

	return implode (v, std::wstring (_T (", "))).c_str ();
}

// CInkSerialNumberDlg message handlers

LRESULT CInkSerialNumberDlg::OnSetHead (WPARAM wParam, LPARAM lParam)
{
	SetHead (wParam, lParam);
	return 0;
}

void CInkSerialNumberDlg::SetHead (ULONG lHeadID, int nMode)
{
	using namespace FoxjetDatabase;

	CArray <HEADSTRUCT, HEADSTRUCT &> v;
	int nIndex = -1;

	//TRACEF (CInkSerialNumberDlg::Trace (nMode));
	//nMode &= INK_CODE_WARNGING_LEVEL_MASK;
	//TRACEF (CInkSerialNumberDlg::Trace (nMode));

	GetHeads (v);

	/*
	{ CString str; str.Format (_T ("[ink code] printerID: %d"), GetPrinterID (ListGlobals::GetDB ())); TRACER (str); }

	for (int i = 0; i < v.GetSize (); i++) {
		CString str;

		str.Format (_T ("[ink code] [%d] %d %s"), i, v [i].m_lID, v [i].m_strUID);
		TRACER (str);
	}
	*/

	for (int i = 0; nIndex == -1 && i < v.GetSize (); i++) {
		HEADSTRUCT & h = v [i];

		if (h.m_lID == lHeadID)
			nIndex = i;
	}

	TRACEF (ToString (lHeadID) + _T (": ") + CInkSerialNumberDlg::Trace (nMode) + _T (" [") + CInkSerialNumberDlg::Trace (OnGetMaxWarningLevel (0, INK_CODE_WARNGING_LEVEL_MASK | INK_CODE_WARNGING_STATE_MASK)) + _T ("]"));
	m_mapHeads.SetAt (lHeadID, nMode);
	//TRACEF (ToString (lHeadID) + _T (": ") + CInkSerialNumberDlg::Trace (nMode) + _T (" [") + CInkSerialNumberDlg::Trace (OnGetMaxWarningLevel (0, INK_CODE_WARNGING_LEVEL_MASK | INK_CODE_WARNGING_STATE_MASK)) + _T ("]"));
	//nMode = max (nMode, OnGetMaxWarningLevel (0, 0));
	//TRACEF (ToString (lHeadID) + _T (": ") + CInkSerialNumberDlg::Trace (nMode) + _T (" [") + CInkSerialNumberDlg::Trace (OnGetMaxWarningLevel (0, INK_CODE_WARNGING_LEVEL_MASK | INK_CODE_WARNGING_STATE_MASK)) + _T ("]"));

	if (CButton * p = (CButton *)GetDlgItem (IDCANCEL)) {
		switch (nMode) {
		//case 0:
		//	EndDialog (IDOK);
		//	break;
		default:
		case INK_CODE_WARNGING_LEVEL_1:
			//if (!p->IsWindowEnabled ()) {
			//	TRACEF (ToString (lHeadID) + _T (": ") + CInkSerialNumberDlg::Trace (nMode) + _T (" [") + CInkSerialNumberDlg::Trace (OnGetMaxWarningLevel (0, 0)) + _T ("]"));
			//	int nBreak = 0;
			//}
			p->EnableWindow (TRUE);
			break;
		case INK_CODE_WARNGING_LEVEL_2:
		case INK_CODE_WARNGING_LEVEL_3:
			//if (p->IsWindowEnabled ()) {
			//	TRACEF (ToString (lHeadID) + _T (": ") + CInkSerialNumberDlg::Trace (nMode) + _T (" [") + CInkSerialNumberDlg::Trace (OnGetMaxWarningLevel (0, 0)) + _T ("]"));
			//	int nBreak = 0;
			//}
			p->EnableWindow (FALSE);
			break;
		}
	}

	//{ CString str; str.Format (_T ("[ink code] SetHead: lHeadID: %d, nMode: %d, nIndex: %d"), lHeadID, nMode, nIndex); TRACER (str); }

	if (nIndex >= 0 && nIndex < ARRAYSIZE (::map)) {
		if (CStatic * p = (CStatic *)GetDlgItem (::map [nIndex].m_nLblID)) {
			p->ShowWindow (SW_SHOW);
			p->SetWindowText (v [nIndex].m_strName);
		}

		if (CWnd * p = GetDlgItem (::map [nIndex].m_nTxtID))
			p->ShowWindow (SW_SHOW);

		//if (CStatic * p = (CStatic *)GetDlgItem (::map [nIndex].m_nBmpID)) 
		//	p->ShowWindow (SW_SHOW);
	}
}



BOOL CInkSerialNumberDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	if (m_lHeadID == -1)
		SetTimer (ID_CHECK_INK_CODE_STATE, 5 * 1000, NULL);

	SetTimer (ID_STROKE_COMM_THREAD, 30 * 1000, NULL);

	OnTimer (ID_STROKE_COMM_THREAD);
	StartThreads ();

	if (CFont * pFont = GetFont ()) {
		LOGFONT lf;

		memset (&lf, 0, sizeof (lf));
		pFont->GetLogFont (&lf);

		lf.lfHeight *= 1.5;
		m_fnt.CreateFontIndirect (&lf);
	}
	else 
		m_fnt.CreatePointFont (200, _T ("Microsoft Sans Serif"));

	m_fntInkCode.CreatePointFont (200, _T ("Courier New"));

	if (CStatic * p = (CStatic *)GetDlgItem (LBL_INKCODE)) 
		p->SetFont (&m_fnt);
	
	for (int nIndex = 0; nIndex < ARRAYSIZE (::map); nIndex++) {
		if (nIndex >= 0 && nIndex < ARRAYSIZE (::map)) {
			if (CStatic * p = (CStatic *)GetDlgItem (::map [nIndex].m_nLblID)) {
				p->ShowWindow (SW_HIDE);
				p->SetFont (&m_fnt);
			}
			
			if (FoxjetUtils::CInkCodeEdit  * p = (FoxjetUtils::CInkCodeEdit *)GetDlgItem (::map [nIndex].m_nTxtID)) {
				p->Init (this);
				p->ShowWindow (SW_HIDE);
				p->SetFont (&m_fntInkCode);
			}

			if (CStatic * p = (CStatic *)GetDlgItem (::map [nIndex].m_nBmpID)) {
				p->ShowWindow (SW_HIDE);
				p->SetBitmap (m_hBmpCancel);
			}
		}
	}

	if (m_lHeadID != -1)
		SetHead (m_lHeadID, 1);

	if (CWnd * p = GetDlgItem (::map [0].m_nTxtID)) 
		p->SetFocus ();

	{
		DWORD dwResult = 0;
		
		LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_INK_CODE_LOCKOUT, 1, (LPARAM)this,  SMTO_NORMAL | SMTO_ABORTIFHUNG, 1, &dwResult);
	}

	if (CStatic * p = (CStatic *)GetDlgItem (LBL_WARNINGS))
		p->ModifyStyle (SS_TYPEMASK, SS_OWNERDRAW);

	if (CStatic * p = (CStatic *)GetDlgItem (IDC_LOGO))
		p->ModifyStyle (SS_TYPEMASK, SS_OWNERDRAW);

	return FALSE;
}

void CInkSerialNumberDlg::OnCancel()
{
	if (m_pParent) {
		for (POSITION pos = m_mapHeads.GetStartPosition (); pos; ) {
			ULONG lHeadID = 0;
			int n = 0;

			m_mapHeads.GetNextAssoc (pos, lHeadID, n);

			if (lHeadID)
				m_pParent->PostMessage (WM_INK_CODE_CHANGE, (WPARAM)new ControlView::WM_INK_CODE_CHANGE_STRUCT (IDCANCEL, lHeadID, false));
		}
	}

	CDialogEx::OnCancel ();
}

static ULONG Find (const CMap <ULONG, ULONG, CString, CString &> & vCodes, const CString & strFind)
{
	for (POSITION posCodes = vCodes.GetStartPosition (); posCodes; ) {
		ULONG lHeadID = 0;
		CString str;

		vCodes.GetNextAssoc (posCodes, lHeadID, str);

		if (!str.CompareNoCase (strFind))
			return lHeadID;
	}

	return -1;
}

LRESULT CInkSerialNumberDlg::OnGetMaxWarningLevel (WPARAM wParam, LPARAM lParam)
{
	int nLevel = INK_CODE_WARNGING_LEVEL_0;
	int nMask = !lParam ? INK_CODE_WARNGING_LEVEL_MASK : 0;
	
	if (lParam & INK_CODE_WARNGING_LEVEL_MASK)
		nMask |= INK_CODE_WARNGING_LEVEL_MASK;

	if (lParam & INK_CODE_WARNGING_STATE_MASK)
		nMask |= INK_CODE_WARNGING_STATE_MASK;

	for (POSITION pos = m_mapHeads.GetStartPosition (); pos; ) {
		ULONG lHeadID = -1;
		int n = INK_CODE_WARNGING_LEVEL_0;

		m_mapHeads.GetNextAssoc (pos, lHeadID, n);

		if (wParam == lHeadID)
			return n & nMask;

		nLevel = max (n & nMask, nLevel);
		//TRACEF (_T ("CControlView::OnCheckInkCodeState: lHeadID: ") + ToString (lHeadID) + _T (", nLevel: ") + ToString (nLevel));
	}

	return nLevel & nMask;
}

void CInkSerialNumberDlg::OnOK()
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
	CMap <ULONG, ULONG, CString, CString &> vCodes;
	bool bAccepted = false;
	CString strInkCode;

	//#ifdef _DEBUG
	//TRACEF (FoxjetDatabase::Encrypted::InkCodes::GetRegSection ());
	//Encrypted::Trace (FoxjetDatabase::Encrypted::InkCodes::GetRegSection ());
	//#endif

	if (m_nErroneousCodes >= 3) 
		return;

	for (int i = 0; i < ARRAYSIZE (::map); i++) {
		if (FoxjetUtils::CInkCodeEdit * p = (FoxjetUtils::CInkCodeEdit *)GetDlgItem (map [i].m_nTxtID)) {
			if (p->IsWindowVisible ()) {
				CString str = p->GetInputData ();
				
				str.Replace (_T ("_"), _T (""));
				str.Trim ();
				//TRACER (_T ("[ink code] entered [") + ToString (i) + _T ("] \"") + str + _T ("\""));
				strInkCode = str;

				if (strInkCode.GetLength ())
					break;
			}
		}
	}

	GetHeads (vHeads);

	/*
	for (int i = 0; i < vHeads.GetSize (); i++) {
		CString str;

		str.Format (_T ("[ink code] [%d] %d %s"), i, vHeads [i].m_lID, vHeads [i].m_strUID);
		TRACER (str);
	}

	TRACER (_T ("[ink code] m_mapHeads: ") + ToString (m_mapHeads.GetSize ()));
	*/

	for (POSITION pos = m_mapHeads.GetStartPosition (); pos; ) {
		ULONG lHeadID;
		int n = 0;

		m_mapHeads.GetNextAssoc (pos, lHeadID, n);

		//TRACER (_T ("[ink code] m_mapHeads: lHeadID: ") + ToString (lHeadID) + _T (", n: ") + ToString (n));

		for (int i = 0; i < vHeads.GetSize (); i++) {
			HEADSTRUCT & h = vHeads [i];

			if (h.m_lID == lHeadID) {
				//TRACER (_T ("[ink code] m_mapHeads: h: ") + h.m_strUID);

				if (i >= 0 && i < ARRAYSIZE (::map)) {
					CString str = strInkCode;

					//if (FoxjetUtils::CInkCodeEdit * p = (FoxjetUtils::CInkCodeEdit *)GetDlgItem (map [i].m_nTxtID)) 
					//	str = p->GetInputData ();

					//str.Replace (_T ("_"), _T (""));

					//TRACEF (ToString (str.GetLength ()) + _T (": ") + str);

					//if (n == 1 && str.IsEmpty ())
					//	continue;

					bool bInvalid = true;
					bool bDemo = theApp.m_bDemo;

					//#ifdef _DEBUG
					//bDemo = false;
					//#endif

					if (!bDemo) {
						//for (int i = 0; i < m_vRegistry.size (); i++) { CString strDebug; strDebug.Format (_T ("[ink code] registry [%d] \"%s\""), i, m_vRegistry [i]); TRACER (strDebug); }
						//{ CString strDebug; strDebug.Format (_T ("[ink code] [%d] [%d] \"%s\""), i, lHeadID, str); TRACER (strDebug); }

						bInvalid = !Encrypted::IsValidInkCode (m_vRegistry, str) || Find (vCodes, str) != -1;
					}
					else {
						if (!(bInvalid = str.GetLength () != 9 ? true : false)) {
							TCHAR * lpsz = _T ("234567ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	
							str.Format (_T ("%09d"), lHeadID);

							for (TCHAR c = '0'; c <= '9'; c++)
								str.Replace (CString (c), CString (lpsz [c - '0' + 6]));
						}
					}

					if (bInvalid) {
						m_nErroneousCodes++;

						if (m_nErroneousCodes >= 3) {
							m_ptmLockout = new CTime (CTime::GetCurrentTime ());
							TRACEF (m_ptmLockout->Format (_T ("%c")));
							Encrypted::InkCodes::SetLockout (m_ptmLockout);
							SetTimer (ID_CAN_UNLOCK_UI, 1000, NULL);
						}

						MsgBox (* this, LoadString (IDS_INVALIDINKCODE), NULL, MB_ICONERROR);

						if (CEdit * p = (CEdit *)GetDlgItem (::map [i].m_nTxtID)) {
							p->SetFocus ();
							p->SetSel (0, p->GetWindowTextLength ());
						}

						return;
					}
					else {
						if (m_ptmLockout) {
							delete m_ptmLockout;
							m_ptmLockout = NULL;
						}

						m_nErroneousCodes = 0;
						Encrypted::InkCodes::SetLockout (NULL);
						bAccepted = true;
					}

					vCodes.SetAt (h.m_lID, str);
				}
			}
		}
	}

	const CString strRootSection = FoxjetDatabase::Encrypted::InkCodes::GetRegSection ();

	for (POSITION posCodes = vCodes.GetStartPosition (); posCodes; ) {
		ULONG lHeadID = 0;
		CString str;

		vCodes.GetNextAssoc (posCodes, lHeadID, str);

		for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
			HEADSTRUCT & h = vHeads [nHead];

			if (h.m_lID == lHeadID) {
				// clear other codes referenced by this head
				for (int i = 0; i < m_vRegistry.size (); i++) {
					CString strSection = strRootSection + _T ("\\") + CString (m_vRegistry [i].c_str ());
					ULONG lID = _tcstoul (Encrypted::GetProfileString (strSection, _T ("lHeadID"), _T ("")), NULL, 10);

					if (lID > 0 && lID == lHeadID) {
						Encrypted::WriteProfileString (strSection, _T ("lHeadID"), _T (""));
						TRACEF (Encrypted::GetProfileString (strSection, _T ("lHeadID"), _T ("[error]")));
					}
				}

				long double dPixel = FoxjetDatabase::CalcInkUsage (h.m_nHeadType, FoxjetDatabase::INKTYPE_FIRST, 1);

				if (h.m_bDoublePulse)
					dPixel *= 2;

				unsigned __int64 nMax = (unsigned __int64)((long double)500.0 / dPixel);
				CString strSection = strRootSection + _T ("\\") + str + _T ("<NULL>");

				if (m_pParent)
					m_pParent->PostMessage (WM_LOG_INK_CODE_REPORT, h.m_lID, (LPARAM)new CString (LoadString (IDS_VALIDCODE))); // (LPARAM)new CString (str));

				VERIFY (Encrypted::WriteProfileString (strSection, _T ("lHeadID"), ToString (h.m_lID)));
				VERIFY (Encrypted::WriteProfileInt64 (strSection, _T ("Used"), (unsigned __int64)0));
				VERIFY (Encrypted::WriteProfileInt64 (strSection, _T ("Max"), (unsigned __int64)nMax));

				TRACEF (_T ("[ink code] [") + ToString (lHeadID) + _T ("]: [") + str + _T ("] lUsed = ") + 
					ToString (Encrypted::GetProfileInt64 (strSection, _T ("Used"), (unsigned __int64)-1)));

				SetHead (lHeadID, 0);
				m_pParent->PostMessage (WM_INK_CODE_CHANGE, (WPARAM)new ControlView::WM_INK_CODE_CHANGE_STRUCT (IDOK, lHeadID, true));
				break;
			}
		}
	}

	if (bAccepted && m_pParent) 
		m_pParent->PostMessage (WM_INK_CODE_ACCEPTED);

	CDialogEx::OnOK();
}

LRESULT CInkSerialNumberDlg::OnSetInkCodeState (WPARAM wParam, LPARAM lParam)
{
	const int nWarning = (int)wParam;
	const int nLevel = nWarning & INK_CODE_WARNGING_LEVEL_MASK;
	const int nState = nWarning & INK_CODE_WARNGING_STATE_MASK;

//	TRACER (Trace (nWarning));

	if (IsWindowVisible ()) {
		if ((nLevel < INK_CODE_WARNGING_LEVEL_1) && nState == 0) {
			OnCancel ();
		}
	}

	return 0;
}


LRESULT CInkSerialNumberDlg::OnSetHeadWarnings (WPARAM wParam, LPARAM lParam)
{
	if (CStringArray * p = (CStringArray *)wParam) {
		//TRACEF (FoxjetDatabase::ToString (* p));

		if (FoxjetDatabase::ToString (* p) != FoxjetDatabase::ToString (m_vHeadErrors)) {
			m_vHeadErrors.RemoveAll ();
			m_vHeadErrors.Append (* p);

			REPAINT (GetDlgItem (LBL_WARNINGS));
			REPAINT (this);
		}

		delete p;
	}

	return 0;
}

void CInkSerialNumberDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent) { 
	case ID_STROKE_COMM_THREAD:
		if (HWND hwndControl = FoxjetCommon::LocateControlWnd ())
			::SendMessage (hwndControl, WM_SUSPEND_COMM_THREADS, 1, (1 * 60) * 1000);
		return;
	case ID_CHECK_INK_CODE_STATE:
		{
			if (m_pParent) 
				m_pParent->PostMessage (WM_INK_CODE_CHECK_STATE);
		}
		break;
	case ID_CAN_UNLOCK_UI:
		if (IsWindow (m_hWnd) && IsWindowVisible ()) {
			if (m_ptmLockout && (m_nErroneousCodes >= 3)) {
				CTimeSpan tmSpan = CTime::GetCurrentTime () - (* m_ptmLockout);

				TRACEF (tmSpan.Format (_T ("%D %H:%M:%S [")) + FoxjetDatabase::ToString (m_dwLockoutTimeSeconds) + _T ("]"));

				if (tmSpan.GetTotalSeconds () >= m_dwLockoutTimeSeconds) {
					DWORD dwResult = 0;
		
					LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_INK_CODE_LOCKOUT, 0, (LPARAM)this,  SMTO_NORMAL | SMTO_ABORTIFHUNG, 1, &dwResult);

					return;
				}

				if (m_vLastUnlockedState.GetSize () == 0) {
					CArray <int, int> v;

					v.Add (IDOK);
					//v.Add (IDCANCEL);

					if (CStatic * p = (CStatic *)GetDlgItem (LBL_INKCODE)) {
						CTime tm = (* m_ptmLockout) + CTimeSpan (0, 0, 0, m_dwLockoutTimeSeconds);

						p->SetWindowText (LoadString (IDS_ENTERINKCODE) + _T (" [") + LoadString (IDS_LOCKEDOUTUNTIL) + tm.Format (_T ("%c")) +  _T (" ]"));
					}

					for (int i = 0; i < ARRAYSIZE (::map); i++) 
						v.Add (::map [i].m_nTxtID);

					for (int i = 0; i < v.GetSize (); i++) {
						if (CWnd * p = GetDlgItem (v [i])) {
							m_vLastUnlockedState.SetAt (v [i], p->IsWindowEnabled ());
							p->EnableWindow (FALSE);
						}
					}

					DWORD dwResult = 0;
					LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_INK_CODE_LOCKOUT, 1, (LPARAM)this,  SMTO_NORMAL | SMTO_ABORTIFHUNG, 1, &dwResult);
				}
			}
		}
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}


void CInkSerialNumberDlg::OnDestroy()
{
	CDialogEx::OnDestroy();
}

typedef struct 
{
	FoxjetCommon::StdCommDlg::CCommItem m_baud;
	HWND								m_hWnd;
	HANDLE								m_hExit;
	DWORD								m_dwWait;
} PORT_FUNC_STRUCT;

static ULONG CALLBACK PortFunc (LPVOID lpData)
{
	PORT_FUNC_STRUCT * pParam = (PORT_FUNC_STRUCT *)lpData;

	ASSERT (pParam);

	::Sleep (pParam->m_dwWait);
	HANDLE hComm = Open_Comport (pParam->m_baud.m_dwPort, pParam->m_baud.m_dwBaudRate, pParam->m_baud.m_nByteSize, pParam->m_baud.m_parity, pParam->m_baud.m_nStopBits);

	if (hComm == INVALID_HANDLE_VALUE || hComm == NULL) {
		TRACEF (_T ("Failed to open: ") + pParam->m_baud.ToString ());
	}

	if (hComm != INVALID_HANDLE_VALUE && hComm != NULL) {
		CString strData;

		while (1) {
			BYTE buffer [1024];
			DWORD dwBytesRead = 0;

			if (::WaitForSingleObject (pParam->m_hExit, 0) == WAIT_OBJECT_0) {
				TRACEF (_T ("Close_Comport: ") + pParam->m_baud.ToString ());
				Close_Comport (hComm);
				break;
			}

			ZeroMemory (buffer, sizeof (buffer));
			Read_Comport (hComm, &dwBytesRead, sizeof (buffer), buffer);

			if (dwBytesRead) {
				for (int i = 0; i < (int)dwBytesRead; i++) {
					if (buffer [i] == 0x0c)
						buffer [i] = '\r';

					strData += (TCHAR)buffer [i];
				}

				if (strData.Find ('\r') != -1) {
					strData.Trim ();

					while (!::PostMessage (pParam->m_hWnd, WM_HEAD_SERIAL_DATA, (WPARAM)pParam->m_baud.m_dwPort, (LPARAM)new CString (strData)))
						::Sleep (0);

					strData.Empty ();
				}
			}

			::Sleep (250);
		}
	}

	delete pParam;

	return 0;
}

void CInkSerialNumberDlg::StartThreads (DWORD dwWait)
{
	using namespace FoxjetDatabase;
	CUIntArray vPorts;

	if (!m_hExit)
		m_hExit = ::CreateEvent (NULL, TRUE, FALSE, _T ("CInkSerialNumberDlg::m_hExit"));
	else
		::ResetEvent (m_hExit);

	FoxjetCommon::EnumerateSerialPorts (vPorts);

	for (int i = 0; i < vPorts.GetSize (); i++) {
		SETTINGSSTRUCT s;
		CString strKey;
		PORT_FUNC_STRUCT * pParam = new PORT_FUNC_STRUCT;
		DWORD dw = 0;

		strKey.Format (_T ("COM%d"), vPorts [i]);

		pParam->m_baud.FromString (_T ("{0,9600,8,0,0,2,1,0,0}"));
		pParam->m_baud.m_dwPort = vPorts [i];

		if (GetSettingsRecord (ListGlobals::GetDB (), 0, strKey, s)) 
			pParam->m_baud.FromString (s.m_strData);

		TRACEF (pParam->m_baud.ToString ());

		pParam->m_hWnd		= m_hWnd;
		pParam->m_hExit		= m_hExit;
		pParam->m_dwWait	= dwWait;
		m_vThreads.Add (::CreateThread (NULL, 0, PortFunc, (LPVOID)pParam, 0, &dw));
	}
}

void CInkSerialNumberDlg::KillThreads ()
{
	::SetEvent (m_hExit);

	for (int i = 0; i < m_vThreads.GetSize (); i++) {
		DWORD dwWait = ::WaitForSingleObject (m_vThreads [i], 2000);

		if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
		if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
		if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
		if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");

		ASSERT (dwWait == WAIT_OBJECT_0);
	}

	m_vThreads.RemoveAll ();
}

LRESULT CInkSerialNumberDlg::OnSerialData (WPARAM wParam, LPARAM lParam)
{
	DWORD dwPort = wParam;
	
	if (CString * pstr = (CString *)lParam) {
		CString str = * pstr;

		delete pstr;

		for (int i = 0; i < ARRAYSIZE (::map); i++) {
			if (CEdit * p = (CEdit *)GetDlgItem (::map [i].m_nTxtID)) {
				if (p->IsWindowVisible ()) {
					CString strTmp;

					GetDlgItemText (::map [i].m_nTxtID, strTmp);

					if (!strTmp.GetLength ()) {
						SetDlgItemText (::map [i].m_nTxtID, str);
						p->SetFocus ();
						p->SetSel (0, p->GetWindowTextLength ());
						return 0;
					}
				}
			}
		}

		
		if (CWnd * p = GetFocus ()) {
			ULONG lID = ::GetWindowLong (p->m_hWnd, GWL_ID);

			for (int i = 0; i < ARRAYSIZE (::map); i++) {
				if (::map [i].m_nTxtID == lID) {
					if (CEdit * pEdit = (CEdit *)GetDlgItem (lID)) {
						pEdit->SetSel (0, p->GetWindowTextLength ());
						pEdit->SetWindowText (str);
						return 0;
					}
				}
			}
		}

		for (int i = 0; i < ARRAYSIZE (::map); i++) {
			if (CEdit * p = (CEdit *)GetDlgItem (::map [i].m_nTxtID)) {
				if (p->IsWindowVisible ()) {
					SetDlgItemText (::map [i].m_nTxtID, str);
					p->SetFocus ();
					p->SetSel (0, p->GetWindowTextLength ());
					return 0;
				}
			}
		}
	}

	return 0;
}

void CInkSerialNumberDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	DRAWITEMSTRUCT dis;
	
	memcpy (&dis, lpDrawItemStruct, sizeof (dis));

	if (nIDCtl == LBL_WARNINGS) {
		CDC dc;
		dc.Attach (dis.hDC);
		CRect rc = dis.rcItem;
		COLORREF rgbBack = FoxjetUtils::GetButtonColor (_T ("background")); // ::GetSysColor (COLOR_3DFACE);

		dc.FillSolidRect (rc, rgbBack);
		
		if (m_vHeadErrors.GetSize ()) {
			CRect rcError = rc;
			CRect rcTmp = rc;
			const int nFlags = DT_CENTER;
			CString str;

			for (int i = 0; i < m_vHeadErrors.GetSize (); i++) {
				CString strHead = FoxjetDatabase::Extract (m_vHeadErrors [i], _T (">"));

				if (FoxjetDatabase::Extract (strHead, _T (": ")).GetLength ())
					str += strHead + _T ("\n");
			}

			str.TrimRight ();
			//TRACEF (str);

			CFont * pFont = dc.SelectObject (&m_fnt);
			int nDrawText = dc.DrawText (str, CRect (rc), nFlags | DT_CALCRECT);

			if (str.GetLength ()) {
				static const CString strOutOfInk = LoadString (IDS_HEADERROR_OUTOFINK);
				CRect rcText = rc;
				COLORREF rgb = str.Find (strOutOfInk) != -1 ? Color::rgbRed : Color::rgbYellow;

				rcText.top += ((nDrawText / m_vHeadErrors.GetSize ()) * (m_vHeadErrors.GetSize () - 1));
				dc.FillSolidRect (rc, rgb);
				dc.DrawText (str, rcText, nFlags);
				dc.DrawEdge (rc, EDGE_SUNKEN, BF_LEFT | BF_RIGHT | BF_TOP | BF_BOTTOM | BF_ADJUST);
			}

			dc.SelectObject (pFont);
		}

		dc.Detach ();
		return;
	}
	else if (nIDCtl == IDC_LOGO) {
		CDC dcMem;
		DIBSECTION ds;
		CDC dc;
		dc.Attach (dis.hDC);
		CRect rc = dis.rcItem;
		COLORREF rgbBack = FoxjetUtils::GetButtonColor (_T ("background")); // ::GetSysColor (COLOR_3DFACE);

		dc.FillSolidRect (rc, rgbBack);

		memset (&ds, 0, sizeof (ds));
		m_bmpTrueCode.GetObject (sizeof (ds), &ds);

		dcMem.CreateCompatibleDC (&dc);
		CBitmap * pBitmap = dcMem.SelectObject (&m_bmpTrueCode);
		COLORREF rgb = dcMem.GetPixel (0, 0);
		::TransparentBlt (dc, rc.left, rc.top, rc.Width (), rc.Height (), dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, rgb);
		dcMem.SelectObject (pBitmap);

		dc.Detach ();
		return;
	}

	CDialogEx::OnDrawItem (nIDCtl, lpDrawItemStruct);
}


LRESULT CInkSerialNumberDlg::OnInkCodeLockout (WPARAM wParam, LPARAM lParam)
{
	bool bLock = wParam ? true : false;
	CInkSerialNumberDlg * pSender = (CInkSerialNumberDlg *)lParam;

	//if (pSender != this) 
	{
		if (m_ptmLockout) {
			delete m_ptmLockout;
			m_ptmLockout = NULL;
		}

		if (bLock) {
			CTime tm = CTime::GetCurrentTime ();

			if (FoxjetDatabase::Encrypted::InkCodes::GetLockout (tm)) {
				CTimeSpan tmSpan = CTime::GetCurrentTime () - tm;

				TRACEF (tm.Format (_T ("%c")));
				TRACEF (tmSpan.Format (_T ("%D %H:%M:%S")));

				if (tmSpan.GetTotalSeconds () < m_dwLockoutTimeSeconds) 
					m_ptmLockout = new CTime (tm);
			}
		}

		if (!m_ptmLockout) {
			m_nErroneousCodes = 0;
			FoxjetDatabase::Encrypted::InkCodes::SetLockout (NULL);
			KillTimer (ID_CAN_UNLOCK_UI);

			for (POSITION pos = m_vLastUnlockedState.GetStartPosition (); pos; ) {
				UINT nID = 1;
				BOOL bEnabled = FALSE;

				m_vLastUnlockedState.GetNextAssoc (pos, nID, bEnabled);
	
				if (CWnd * p = GetDlgItem (nID)) 
					p->EnableWindow (bEnabled);
			}

			m_nErroneousCodes = 0;
			m_vLastUnlockedState.RemoveAll ();

			if (CStatic * p = (CStatic *)GetDlgItem (LBL_INKCODE)) 
				p->SetWindowText (LoadString (IDS_ENTERINKCODE));

		}
		else {
			m_nErroneousCodes = 4;
			SetTimer (ID_CAN_UNLOCK_UI, 1000, NULL);
		}
	}

	return m_ptmLockout ? 1 : 0;
}




void CInkSerialNumberDlg::OnEnChangeHead (UINT nID)
{
	/*
	CStringArray vCodes;

	for (int i = 0; i < ARRAYSIZE (::map); i++) {
		if (nID != ::map [i].m_nTxtID) {
			if (CWnd * p = GetDlgItem (::map [i].m_nTxtID)) {
				if (p->IsWindowVisible ()) {
					CString str;

					GetDlgItemText (::map [i].m_nTxtID, str);
					vCodes.Add (str);
				}
			}
		}
	}

	for (int i = 0; i < ARRAYSIZE (::map); i++) {
		if (::map [i].m_nTxtID == nID) {
			if (CWnd * p = GetDlgItem (::map [i].m_nTxtID)) {
				if (p->IsWindowVisible ()) {
					CString str;

					GetDlgItemText (::map [i].m_nTxtID, str);

					bool bValid = Encrypted::IsValidInkCode (m_vRegistry, str) || (Find (vCodes, str, false) != -1);

					if (CStatic * p = (CStatic *)GetDlgItem (::map [i].m_nBmpID)) 
						p->SetBitmap (bValid ? m_hBmpOK : m_hBmpCancel);
				}
			}

			break;
		}
	}
	*/
}
