// Head.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "Sw0848Thread.h"
#include "ProdLine.h"
#include "Control.h"
#include "Defines.h"
#include "MainFrm.h"
#include "fj_socket.h"
#include "Head.h"
#include "UserElementDlg.h"
#include "Debug.h"
#include "AppVer.h"
#include "Color.h"
#include "Coord.h"
#include "Edit.h"
#include "fj_system.h"
#include "fj_label.h"
#include "WinMsg.h"
#include "HpHead.h"
#include "Parse.h"
#include "..\MphcApi\Master.h"
#include "BaseElement.h"
#include "ComPropertiesDlg.h"
#include "IpElements.h"
#include "DC.h"

#ifdef __WINPRINTER__
	#include "BarcodeElement.h"
	#include "SerialElement.h"
	#include "BitmapElement.h"
	#include "DatabaseElement.h"
	#include "UserElement.h"
	#include "TextElement.h"
	#include "LocalHead.h"
#endif

#ifdef __IPPRINTER__
	#include "RemoteHead.h"
#endif

//#include "SerialElement.h"

//#define __DEBUG_SENDMESSAGE__

using namespace FoxjetDatabase;
using namespace Head;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#if defined( __DEBUG_LOW_INK_CODE__ ) || defined( _DEBUG )
DWORD dwSimError = 0;
#endif

#pragma data_seg( "mapInfo" )
	static DIAGNOSTIC_CRITICAL_SECTION (csInfo);
	static CMap <DWORD, DWORD, CHeadInfo *, CHeadInfo *> mapInfo;
#pragma

#pragma data_seg( "mapStatus" )
	static DIAGNOSTIC_CRITICAL_SECTION (csStatus);
	static CMap <ULONG, ULONG, CString, const CString &> mapStatus;
#pragma




/////////////////////////////////////////////////////////////////////////////

CHeadInfo::CHeadInfo ()
:	m_dwThreadID (0),
	m_eTaskState (FoxjetDatabase::DISABLED),
	m_hControlWnd (NULL),
	m_bDisabled (false),
	m_DrawContext (FoxjetCommon::PRINTER),
	m_bRemoteHead (false),
	m_wGaVer (0),
	m_nErrorIndex (0),
	m_bPrintDriver (false),
	m_nLineSpeed (0),

	#ifdef __IPPRINTER__
	m_dDiskUsage (0.0),
	m_bDigiBoard (false),
	#endif

	#ifdef __WINPRINTER__
	m_dRemainingInk (0.0),
	#endif //__WINPRINTER__

	m_bPrinting	(false)
{
	//TRACEF ("CHeadInfo::CHeadInfo ()");
}

CHeadInfo::CHeadInfo (const CHeadInfo & rhs)
:	m_dwThreadID	(rhs.m_dwThreadID),
	m_hControlWnd	(rhs.m_hControlWnd),
	m_bDisabled		(rhs.m_bDisabled),
	m_ImageHdr		(rhs.m_ImageHdr),
	m_sMessageName	(rhs.m_sMessageName),
	m_Message		(rhs.m_Message),
	m_Task			(rhs.m_Task),
	m_Box			(rhs.m_Box),
	m_sTaskName		(rhs.m_sTaskName),
	m_strKeyValue	(rhs.m_strKeyValue),
	m_sDatabaseName	(rhs.m_sDatabaseName),
	m_Config		(rhs.m_Config),
	m_DrawContext	(rhs.m_DrawContext),
	m_bRemoteHead	(rhs.m_bRemoteHead),
	m_Status		(rhs.m_Status),
	m_eTaskState	(rhs.m_eTaskState),
	m_wGaVer		(rhs.m_wGaVer),
	m_nErrorIndex	(rhs.m_nErrorIndex),
	m_bPrintDriver	(rhs.m_bPrintDriver),
	m_nLineSpeed	(rhs.m_nLineSpeed),


	#ifdef __IPPRINTER__
	m_dDiskUsage	(rhs.m_dDiskUsage),
	m_bDigiBoard	(rhs.m_bDigiBoard),
	#endif

	#ifdef __WINPRINTER__
	m_dRemainingInk (rhs.m_dRemainingInk),
	#endif //__WINPRINTER__

	m_bPrinting		(rhs.m_bPrinting)
{
	//TRACEF ("CHeadInfo::CHeadInfo (const CHeadInfo & rhs)");
}

CHeadInfo & CHeadInfo::operator = (const CHeadInfo & rhs)
{
	//TRACEF ("CHeadInfo & CHeadInfo::operator = (const CHeadInfo & rhs)");

	if (this != &rhs) {
		m_dwThreadID	= rhs.m_dwThreadID;
		m_hControlWnd	= rhs.m_hControlWnd;
		m_bDisabled		= rhs.m_bDisabled;
		m_ImageHdr		= rhs.m_ImageHdr;
		m_sMessageName	= rhs.m_sMessageName;
		m_Message		= rhs.m_Message;
		m_Task			= rhs.m_Task;
		m_Box			= rhs.m_Box;
		m_sTaskName		= rhs.m_sTaskName;
		m_strKeyValue	= rhs.m_strKeyValue;
		m_sDatabaseName	= rhs.m_sDatabaseName;
		m_Config		= rhs.m_Config;
		m_DrawContext	= rhs.m_DrawContext;
		m_bRemoteHead	= rhs.m_bRemoteHead;
		m_Status		= rhs.m_Status;
		m_eTaskState	= rhs.m_eTaskState;
		m_wGaVer		= rhs.m_wGaVer;
		m_nErrorIndex	= rhs.m_nErrorIndex;
		m_bPrinting		= rhs.m_bPrinting;
		m_bPrintDriver	= rhs.m_bPrintDriver;
		m_nLineSpeed	= rhs.m_nLineSpeed;

		#ifdef __IPPRINTER__
		m_dDiskUsage	= rhs.m_dDiskUsage;
		m_bDigiBoard	= rhs.m_bDigiBoard;
		#endif

		#ifdef __WINPRINTER__
		m_dRemainingInk	= rhs.m_dRemainingInk;
		#endif //__WINPRINTER__

	}

	return * this;
}

CHeadInfo::~CHeadInfo ()
{
	//TRACEF ("CHeadInfo::~CHeadInfo");
}

DWORD CHeadInfo::GetThreadID () const
{
	if (const CWinThread * p = dynamic_cast <const CWinThread *> (this)) 
		return p->m_nThreadID;
	
	return m_dwThreadID;
}

eHeadStatus CHeadInfo::GetErrorStatus()
{
	if (!m_Status.m_bVoltageOk)
		return LOW_VOLTAGE;
	if (!m_Status.m_bAtTemp)
		return LOW_TEMP;
	if (m_Status.m_nInkLevel == INK_OUT)
		return INK_OUT;
	if (m_Status.m_bBrokenLine)
		return IV_BROKENLINE;
	
	if (!m_Status.m_bIDSConnected)
		return IV_DISCONNECTED;
	if (m_Status.m_nInkLevel == LOW_INK)
		return LOW_INK;

	return OK;
}

static CString strRunning;
static CString strIdle;
static CString strStopped;
static CString strDisabled;
static CString strLowInk;
static CString strNoInk;
static CString strLowTemp;
static CString strBrokenLine;
static CString strVoltageError;
static CString strIdsDisconnected;
static CString strCommportFailure;
static CString strOffline;
static CString strNoFpga;
static CString strOK;

#if 0 //#ifdef _DEBUG
static std::vector <DWORD> vTraceThreadContext;

inline void TraceThreadContext (LPCTSTR lpszFile, ULONG lLine)
{
	CString str;
	DWORD dw = ::GetCurrentThreadId ();
	static bool bClear = true;
	const CString strLogFile = _T ("C:\\Foxjet\\TraceThreadContext.txt");

	if (Find (::vTraceThreadContext, dw) == -1)
		::vTraceThreadContext.push_back (dw);

	str.Format (_T ("%s(%d): ::GetCurrentThreadId: 0x%p ["), lpszFile, lLine, dw);

	for (int i = 0; i < ::vTraceThreadContext.size (); i++) {
		CString s;

		s.Format (_T ("0x%p"), ::vTraceThreadContext [i]);
		str += s;
		
		if ((i + 1) < ::vTraceThreadContext.size ())
			 str += _T (",");
	}

	str += _T ("]\n");

	TRACER (str);

	//if (FILE * fp = _tfopen (strLogFile, bClear ? _T ("w") : _T ("a"))) {
	//	std::string s = w2a (str);

	//	fwrite (s.c_str (), s.length (), 1, fp);
	//	fclose (fp);
	//	bClear = false;
	//}
}
#endif

CString CHeadInfo::GetDisplayMessage () const
{ 
	CString str; 

	SEND_THREAD_MESSAGE (* this, TM_GET_STATUS_MSG, (WPARAM)&str);

	return str; 
}

CString CHeadInfo::GetStateString() const
{
/*
	CString strState;
	switch ( m_eTaskState )
	{
		case FoxjetDatabase::RUNNING:
			strState.LoadString ( IDS_TASK_RUNNING );
			break;
		case FoxjetDatabase::IDLE:
			strState.LoadString ( IDS_TASK_IDLE );
			break;
		case FoxjetDatabase::STOPPED:
			strState.LoadString ( IDS_TASK_STOPPED );
			break;
		case FoxjetDatabase::DISABLED:
			strState.LoadString ( IDS_TASK_HEAD_DISABLED );
			break;
	}
	return strState;
*/

	if (!::strRunning.GetLength ()) {
		strRunning			= LoadString (IDS_TASK_RUNNING);
		strIdle				= LoadString (IDS_TASK_IDLE);
		strStopped			= LoadString (IDS_TASK_STOPPED);
		strDisabled			= LoadString (IDS_TASK_HEAD_DISABLED);
		strLowInk			= LoadString (IDS_LOWINK);
		strNoInk			= LoadString (IDS_NOINK);
		strLowTemp			= LoadString (IDS_LOWTEMP);
		strBrokenLine		= LoadString (IDS_BROKENLINE);
		strVoltageError		= LoadString (IDS_VOLTAGEERROR);
		strIdsDisconnected	= LoadString (IDS_IDSDISCONNECTED);
		strCommportFailure	= LoadString (IDS_COMMPORTFAILURE);
		strOffline			= LoadString (IDS_OFFLINE);
		strNoFpga			= LoadString (IDS_NOFPGA);
		strOK				= LoadString (IDS_OK);
	}

	switch (m_eTaskState) {
	case FoxjetDatabase::RUNNING:	return ::strRunning;
	case FoxjetDatabase::IDLE:		return ::strIdle;
	case FoxjetDatabase::STOPPED:	return ::strStopped;
	case FoxjetDatabase::DISABLED:	return ::strDisabled;
	}

	return _T ("");
}

CString CHeadInfo::GetStatusMessage()
{
	DWORD dw = 0;

	if (CHead * p = dynamic_cast <CHead *> (this)) {
		ASSERT (0); //dw = p->GetErrorState ();
	}
	else {
		SEND_THREAD_MESSAGE (* this, TM_GET_ERROR_STATE, (WPARAM)&dw);
	}

	struct 
	{
		DWORD m_dwError;
		UINT m_nResID;
	}
	const map [] = 
	{
		{ HEADERROR_LOWINK,			IDS_LOWINK			},
		{ HEADERROR_OUTOFINK,		IDS_NOINK			},
		{ HEADERROR_LOWTEMP,		IDS_LOWTEMP			},
		{ HEADERROR_HIGHVOLTAGE,	IDS_VOLTAGEERROR	},
		{ IVERROR_BROKENLINE,		IDS_BROKENLINE		},
		{ IVERROR_DISCONNECTED,		IDS_IDSDISCONNECTED	},
		{ HPERROR_COMMPORTFAILURE,	IDS_COMMPORTFAILURE },
		{ HEADERROR_DISCONNECTED,	IDS_OFFLINE			},
		{ HEADERROR_NOFPGA,			IDS_NOFPGA			},
		{ HEADERROR_EP8,			IDS_EP8ERROR		},
	};
	UINT nID [ARRAYSIZE (map)] = { 0 };
	int nCount = 0;

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		if (dw & map [i].m_dwError) {
			nID [nCount] = map [i].m_nResID;
			nCount++;
		}
	}

	if (!nCount)
		return LoadString (IDS_OK);

	int nIndex = m_nErrorIndex % nCount;

	return LoadString (nID [nIndex]);

}

CString CHeadInfo::GetWarnings ()
{
	CStringArray v;

	//if (!m_Status.m_bVoltageOk)				v.Add (LoadString (IDS_VOLTAGEERROR));
	//if (!m_Status.m_bAtTemp)					v.Add (LoadString (IDS_HEADERROR_LOWTEMP));
	if (m_Status.m_nInkLevel == INK_OUT)		v.Add (LoadString (IDS_HEADERROR_OUTOFINK));
	if (m_Status.m_nInkLevel == LOW_INK)		v.Add (LoadString (IDS_WARNING_LOWINK));

	//TRACER (GetFriendlyName () + _T (" ") + ToString (v));

	return Extract (ToString (v), _T ("{"), _T ("}"));
}

static CString GetErrorColor (DWORD dw)
{
	switch (dw) {
	case HEADERROR_LOWINK: 
		return _T ("<YELLOW>");
	case HEADERROR_OUTOFINK:
	case HEADERROR_LOWTEMP:
	case HEADERROR_HIGHVOLTAGE:
	case IVERROR_BROKENLINE:
	case IVERROR_DISCONNECTED:
	case HPERROR_COMMPORTFAILURE:
	case HEADERROR_DISCONNECTED:
	case HEADERROR_NOFPGA:
	case HEADERROR_EP8:
		return _T ("<RED>");
	}

	return _T ("");
}

CString CHeadInfo::GetErrors () 
{
	CString str;
	DWORD dw = 0;

	if (CHead * p = dynamic_cast <CHead *> (this)) {
		ASSERT (0); //dw = p->GetErrorState ();
	}
	else {
		SEND_THREAD_MESSAGE (* this, TM_GET_ERROR_STATE, (WPARAM)&dw);
	}

	struct 
	{
		DWORD m_dwError;
		UINT m_nResID;
	}
	const map [] = 
	{
		{ HEADERROR_LOWINK,			IDS_LOWINK			},
		{ HEADERROR_OUTOFINK,		IDS_NOINK			},
		{ HEADERROR_LOWTEMP,		IDS_LOWTEMP			},
		{ HEADERROR_HIGHVOLTAGE,	IDS_VOLTAGEERROR	},
		{ IVERROR_BROKENLINE,		IDS_BROKENLINE		},
		{ IVERROR_DISCONNECTED,		IDS_IDSDISCONNECTED	},
		{ HPERROR_COMMPORTFAILURE,	IDS_COMMPORTFAILURE },
		{ HEADERROR_DISCONNECTED,	IDS_OFFLINE			},
		{ HEADERROR_NOFPGA,			IDS_NOFPGA			},
		{ HEADERROR_EP8,			IDS_EP8ERROR		},
	};
	UINT nID [ARRAYSIZE (map)] = { 0 };
	int nCount = 0;

	for (int i = 0; i < ARRAYSIZE (map); i++) 
		if (dw & map [i].m_dwError) 
			nCount++;

	str.Format (_T ("0x%p\t"), dw);

	if (!nCount) 
		str += _T ("\t") + LoadString (IDS_OK) + _T ("<GREEN>");
	else {
		for (int i = 0; i < ARRAYSIZE (map); i++) 
			if (dw & map [i].m_dwError) 
				str += _T ("\t") + LoadString (map [i].m_nResID) + GetErrorColor (map [i].m_dwError);
	}

	#ifdef _DEBUG
	//str += _T ("\tRED<RED>\tYELLOW<YELLOW>");
	#endif

	return str;
}

bool CHeadInfo::IsErrorPresent (DWORD dwError) 
{
	DWORD dw = 0;

	if (CHead * p = dynamic_cast <CHead *> (this)) {
		ASSERT (0); //dw = p->GetErrorState ();
	}
	else {
		SEND_THREAD_MESSAGE (* this, TM_GET_ERROR_STATE, (WPARAM)&dw);
	}

	return (dw & dwError) ? true : false;
}

UINT CHeadInfo::CalcFlushBuffer () 
{
	UINT nFlush = 0;
	int N_Factor = m_Config.GetNFactor();
	INT nWordsPerCol = GlobalFuncs::GetPhcChannels (m_Config.GetHeadSettings ()) / 16;

	UCHAR state = (m_Config.GetHeadSettings().m_nDirection == LTOR) ? 1 : 0;
	state |= m_Config.GetHeadSettings().m_bInverted ? 0x02 : 0x00;

	switch ( state ) 
	{
		case 0:	// R->L Normal
		case 3:	// L->R Inverted
			switch ( m_Config.GetHeadSettings().m_nHeadType )
			{
				case FoxjetDatabase::ALPHA_CODER:
				case FoxjetDatabase::UJ2_192_32:
				case FoxjetDatabase::UJ2_192_32NP:
				case FoxjetDatabase::UJ2_96_32:
				case FoxjetDatabase::UJI_96_32:
				case FoxjetDatabase::UJI_192_32:
				case FoxjetDatabase::UJI_256_32:
					//nFlush = (31 * N_Factor * nWordsPerCol); 
					nFlush = (31 * N_Factor * nWordsPerCol) / 2; //m_Config.m_HeadSettings.m_nEncoderDivisor; 
					break;
				case FoxjetDatabase::UJ2_352_32:
					nFlush = N_Factor * nWordsPerCol; 
					break;
				case FoxjetDatabase::GRAPHICS_384_128:
				case FoxjetDatabase::GRAPHICS_768_256:
				case FoxjetDatabase::GRAPHICS_768_256_L310:
				case UR2:
				case UR4:
					nFlush = N_Factor * nWordsPerCol; 
					break;
			}
			break;
		case 1:	// L->R Normal
		case 2:	// R->L Inverted
			switch ( m_Config.GetHeadSettings().m_nHeadType )
			{
				case FoxjetDatabase::ALPHA_CODER:
				case FoxjetDatabase::UJ2_192_32:
				case FoxjetDatabase::UJ2_192_32NP:
				case FoxjetDatabase::UJ2_96_32:
				case FoxjetDatabase::UJI_96_32:
				case FoxjetDatabase::UJI_192_32:
				case FoxjetDatabase::UJI_256_32:
					//nFlush = (31 * N_Factor * nWordsPerCol); 
					nFlush = (31 * N_Factor * nWordsPerCol) / 2; //m_Config.m_HeadSettings.m_nEncoderDivisor; 
					break;
				case FoxjetDatabase::UJ2_352_32:	
					nFlush = N_Factor * nWordsPerCol; 
					break;
				case FoxjetDatabase::GRAPHICS_384_128:
				case FoxjetDatabase::GRAPHICS_768_256:	
				case FoxjetDatabase::GRAPHICS_768_256_L310:
				case UR2:
				case UR4:
					nFlush = N_Factor * nWordsPerCol; 
					break;
			}
			break;
	}

/*
{ // TODO: rem
	CString str;

	//AC/42.1/300/300: nFlush: 403, N_Factor: 13, nWordsPerCol: 2, state: 0

	str.Format (_T ("[%s] nFlush: %d, N_Factor: %d, nWordsPerCol: %d, state: %X"), 
		m_Config.GetHeadSettings().m_strName,
		nFlush, 
		N_Factor,
		nWordsPerCol,
		state);
	TRACEF (str);
}
*/

	return nFlush;
}

void CHeadInfo::PostMessage (UINT nMsg, WPARAM wParam, LPCTSTR lpszFile, ULONG lLine) const
{
	DWORD dwID = GetThreadID ();

	if (dwID == 0) {
		#ifdef _DEBUG
		CString str; 
		
		str.Format (_T ("CHeadInfo::PostMessage: thread id: %d"), GetThreadID ()); 
		CDebug::Trace (str, true, lpszFile, lLine);
		#endif
		return;
	}

	#ifdef __DEBUG_SENDMESSAGE__
	{ CString str; str.Format (_T ("CHeadInfo::PostMessage: thread id: %d: %s (wParam: %d)"), GetThreadID (), CHead::GetMessageString (nMsg), wParam); CDebug::Trace (str, true, lpszFile, lLine); }
	#endif

	if (ISPOSTMESSAGESTRUCT (nMsg)) {
		POSTMESSAGESTRUCT * p = new POSTMESSAGESTRUCT (true, lpszFile, lLine, NULL);

		while (!::PostThreadMessage (dwID, nMsg, wParam, (LPARAM)p)) 
			::Sleep (0); 
	}
	else {
		while (!::PostThreadMessage (dwID, nMsg, wParam, 0)) 
			::Sleep (0); 
	}
}

void CHeadInfo::SendMessage (CHead * pThread, UINT nMsg, WPARAM wParam, LPCTSTR lpszFile, ULONG lLine)
{
	int nDiagnostic = nMsg - WM_APP;
	CString str;
	DWORD dwID = GetThreadID ();
	DWORD dwTimeout = INFINITE;//CHead::m_dwExitTimeout;

	if (dwID == 0) {
		#ifdef _DEBUG
		CString str; 
		
		str.Format (_T ("CHeadInfo::SendMessage: thread id: %d"), GetThreadID ()); 
		CDebug::Trace (str, true, lpszFile, lLine);
		#endif
		return;
	}

	#ifdef __DEBUG_SENDMESSAGE__
	{ CString str; str.Format (_T ("CHeadInfo::SendMessage: thread id: %d: %s (wParam: %d)"), pThread->m_nThreadID, CHead::GetMessageString (nMsg), wParam); CDebug::Trace (str, true, lpszFile, lLine); }
	#endif

	/*
	#ifdef _DEBUG
	{
		CString strTmp = _T ("CHeadInfo::SendMessage: ") + CHead::GetMessageString (nMsg);

		strTmp.TrimLeft ();
		strTmp.TrimRight ();
		strTmp += _T ("...");
		MsgBox (NULL, strTmp, 0, 0);
	}
	#endif
	*/

	HANDLE hEvent = ::CreateEvent (NULL, TRUE, FALSE, str);
	POSTMESSAGESTRUCT * p = new POSTMESSAGESTRUCT (false, lpszFile, lLine, hEvent);

	// if it's already signalled, something is wrong
	ASSERT (::WaitForSingleObject (hEvent, 0) != WAIT_OBJECT_0);

	while (!::PostThreadMessage (dwID, nMsg, wParam, (LPARAM)p)) 
		::Sleep (0);
	
	const clock_t clock_tStart = clock ();
	DWORD dwDiff = 0;
	DWORD dwWait = -1;

	while ((dwTimeout == INFINITE || dwDiff < dwTimeout) && (dwWait != WAIT_OBJECT_0)) {
		MSG msg;

		while (::PeekMessage (&msg, NULL, NULL, NULL, PM_NOREMOVE)) 
			if (!pThread->PumpMessage ())
				break;

		dwDiff = (DWORD)((double)(clock () - clock_tStart) / CLOCKS_PER_SEC * 1000.0);
		dwWait = ::WaitForSingleObject (hEvent, 100);
	}

	#ifdef _DEBUG
	{
		CString str;

		str.Format (_T ("SendMessage [%d]: %d (0x%p): "), dwID, nMsg, wParam);

		if (dwWait == WAIT_TIMEOUT)		str += _T ("WAIT_TIMEOUT");
		if (dwWait == WAIT_ABANDONED)	str += _T ("WAIT_ABANDONED");
		if (dwWait == WAIT_OBJECT_0)	str += _T ("WAIT_OBJECT_0");

		if (dwWait != WAIT_OBJECT_0)
			CDebug::Trace (str, true, lpszFile, lLine);
	}
	#endif
	
	::CloseHandle (hEvent);
}

void CHeadInfo::SendMessage (UINT nMsg, WPARAM wParam, LPCTSTR lpszFile, ULONG lLine) const
{
	int nDiagnostic = nMsg - WM_APP;
	CString str;
	DWORD dwID = GetThreadID ();
	DWORD dwTimeout = INFINITE;//CHead::m_dwExitTimeout;

	if (dwID == 0) {
		#ifdef _DEBUG
		CString str; 
		
		str.Format (_T ("CHeadInfo::SendMessage: thread id: %d"), GetThreadID ()); 
		CDebug::Trace (str, true, lpszFile, lLine);
		#endif
		return;
	}

	#ifdef __DEBUG_SENDMESSAGE__
	{ CString str; str.Format (_T ("CHeadInfo::SendMessage: thread id: %d: %s (wParam: %d)"), GetThreadID (), CHead::GetMessageString (nMsg), wParam); CDebug::Trace (str, true, lpszFile, lLine); }
	#endif

	/*
	#ifdef _DEBUG
	{
		switch (nMsg) {
		case TM_GET_ERROR_STATE:
			break;
		default:
			{
				CString strTmp = CTime::GetCurrentTime ().Format (_T ("%c: ")) + _T ("CHeadInfo::SendMessage: ") + CHead::GetMessageString (nMsg);

				strTmp.TrimLeft ();
				strTmp.TrimRight ();
				strTmp += _T ("...");
				TRACEF (strTmp);
			}
			break;
		}
	}
	#endif
	*/

	HANDLE hEvent = ::CreateEvent (NULL, TRUE, FALSE, str);
	POSTMESSAGESTRUCT * p = new POSTMESSAGESTRUCT (false, lpszFile, lLine, hEvent);

	// if it's already signalled, something is wrong
	ASSERT (::WaitForSingleObject (hEvent, 0) != WAIT_OBJECT_0);

	while (!::PostThreadMessage (dwID, nMsg, wParam, (LPARAM)p)) 
		::Sleep (0);
	
	DWORD dwWait = ::WaitForSingleObject (hEvent, dwTimeout);
	
	#ifdef _DEBUG
	{
		CString str;

		str.Format (_T ("SendMessage [%d]: %d (0x%p): "), dwID, nMsg, wParam);

		if (dwWait == WAIT_TIMEOUT)		str += _T ("WAIT_TIMEOUT");
		if (dwWait == WAIT_ABANDONED)	str += _T ("WAIT_ABANDONED");
		if (dwWait == WAIT_OBJECT_0)	str += _T ("WAIT_OBJECT_0");

		if (dwWait != WAIT_OBJECT_0)
			CDebug::Trace (str, true, lpszFile, lLine);
	}
	#endif
	
	::CloseHandle (hEvent);
}

/////////////////////////////////////////////////////////////////////////////
// CHead

const DWORD CHead::m_dwExitTimeout = 5000;

IMPLEMENT_DYNCREATE(CHead, CWinThread)

CHead::CHead() 
:	m_pImageEng (NULL),
	m_SerialDevice (HANDSCANNER),
	m_LabelSize (CSize (0, 0)),
	m_hExit (NULL),
	m_pImageBuffer (NULL),
	m_pElementList (NULL),
	m_bTestPattern (false),
	m_tmLastPrint (CTime::GetCurrentTime ()),
	m_nStandbyTimeout (0),
	m_bStandby (false),
	m_dwIdleFlags (HEADERROR_LOWTEMP | HEADERROR_HIGHVOLTAGE | HEADERROR_OUTOFINK | HEADERROR_EP8),
	m_bModifiedAfterLoad (false),
	m_bSerialPending (false),
	m_tmImage (CTime::GetCurrentTime ()),
#ifdef _DEBUG
	m_dwSimulatedError (0),
	//m_nHeadPtr (0),
#endif
	m_dwLastState (-1),
	m_nImgMode (IMAGE_ON_PHOTOCELL),
	m_bSQLServer (false),
	m_pdb (NULL),
	m_tmDatabase (CTime::GetCurrentTime ()),
	m_nDbTimeout (theApp.m_nDbTimeout),
	m_bImgModeExpire (true)
{
	::QueryPerformanceFrequency (&m_freq);

	m_bSQLServer				= theApp.m_Database.IsSqlServer ();
	m_bAutoDelete				= TRUE;
	m_Status.m_nInkLevel		= INK_OUT; 
	m_Status.m_bAtTemp			= false;
	m_Status.m_bVoltageOk		= false;
	m_Status.m_lCount			= 0L;
	m_Status.m_lLastCount		= -1;
	m_Status.m_dLineSpeed		= 0L;
	m_Status.m_lIntr			= 0L;
	m_Status.m_bBrokenLine		= false;
	m_Status.m_bIDSConnected	= true;
	m_Status.m_bFPGA			= true;

	try {
		m_pImageEng = new CImageEng();
	}
	catch (CMemoryException * e) { throw (e); }

	m_pImageBuffer = NULL;
	m_ImageHdr.ippc = 32;
	m_ImageHdr.ibpc = ( BYTE )(m_ImageHdr.ippc / 8);
	m_ImageHdr.irow = 0;
	m_ImageHdr.istart = 0;
	m_ImageHdr.ilen = 0;
	m_eTaskState = FoxjetDatabase::DISABLED;
	m_Task.m_lID = 0;

	for (int i = 0; i < ARRAYSIZE (m_sz0864); i++) {
		CString str = _T ("Default Serial Data");

		ZeroMemory (m_sz0864 [i], ARRAYSIZE (m_sz0864 [i]));
		memcpy (m_sz0864 [i], (LPVOID)(LPCTSTR)str, str.GetLength ());
	}
}

CHead::~CHead()
{
	if (m_bSQLServer) {
		if (m_pdb) {
			m_pdb->Close ();
			delete m_pdb;
		}
	}
}

LPARAM CHead::SendControlMessage (UINT nMsg, WPARAM wParam, LPARAM lParam)
{
	if (m_hControlWnd)
		return ::SendMessage (m_hControlWnd, nMsg, wParam, lParam);

	return 0;
}

void CHead::FreeImgUpdDesc ()
{
	ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);

	while ( !m_listImgUpdDesc.IsEmpty() )
		delete ( tagImgUpdateDesc * ) m_listImgUpdDesc.RemoveHead();
}

void CHead::SetStrobe ( int Color )
{

}

BEGIN_MESSAGE_MAP(CHead, CWinThread)
	//{{AFX_MSG_MAP(CHead)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_THREAD_MESSAGE (TM_START_TASK, OnStartTask)
	ON_THREAD_MESSAGE (TM_STOP_TASK, OnStopTask)
	ON_THREAD_MESSAGE (TM_IDLE_TASK, OnIdleTask)
	ON_THREAD_MESSAGE (TM_RESUME_TASK, OnResumeTask)
	ON_THREAD_MESSAGE (TM_SET_IGNORE_PHOTOCELL, OnSetIgnorePhotocell)
	ON_THREAD_MESSAGE (TM_BUILD_PREVIEW_IMAGE, OnBuildPreviewImage)
	ON_THREAD_MESSAGE (TM_PROCESS_SERIAL_DATA, OnProcessSerialData) 
	ON_THREAD_MESSAGE (TM_KILLTHREAD, OnKillThread)
	ON_THREAD_MESSAGE (TM_UPDATE_COUNTS, OnUpdateCounts)
	ON_THREAD_MESSAGE (TM_GET_INFO, OnGetInfo)
	ON_THREAD_MESSAGE (TM_INCREMENT_ERROR_INDEX, OnIncrementErrorIndex)
	ON_THREAD_MESSAGE (TM_SET_KEY_VALUE, OnSetKeyValue)	
	ON_THREAD_MESSAGE (TM_SET_EXP_SPAN, OnSetExpSpan)
	//ON_THREAD_MESSAGE (TM_HEAD_CONFIG_CHANGED, OnHeadConfigChanged)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHead message handlers

void CHead::LoadStandbySettings ()
{
	using namespace FoxjetDatabase;

	ASSERT (m_pdb->IsOpen ()); //CHead::CDbConnection (this, _T (__FILE__), __LINE__);
	SETTINGSSTRUCT s;

	s.m_lLineID = 0;
	s.m_strKey.Format (_T ("%d, Standby mode"), 
		m_Config.m_HeadSettings.m_lID);
	s.m_strData = _T ("0");

	ASSERT (m_pdb);
	GetSettingsRecord (* m_pdb, s.m_lLineID, s.m_strKey, s);
	m_nStandbyTimeout = _tcstoul (s.m_strData, NULL, 10);
}

void CHead::Shutdown ()
{
	// NOTE: this should never be called directly by the app, 
	//	only by an instance of this thread (in response to a posted thread message)

	Disconnect (true);
	ExitInstance ();
	::AfxEndThread (0);
}

void CHead::OnKillThread (WPARAM wParam, LPARAM lParam)
{
	//CString strUID = GetUID (); TRACEF (_T ("CHead::OnKillThread: ") + strUID);

	SetThreadPriority (THREAD_PRIORITY_TIME_CRITICAL); TRACEF (_T ("SetThreadPriority: ") + FoxjetDatabase::ToString (GetThreadPriority ()));

	Disconnect (true);

	if (HANDLE h = (HANDLE)wParam)
		::SetEvent (h);

	if (m_hExit)
		::SetEvent (m_hExit);
}


void CHead::Disconnect (bool bShutdown)
{
	if (!bShutdown) {
		while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this))
			::Sleep (0);

		CString str;

		str.Format (_T ("WM_HEAD_DISCONNECT %lu"), m_hThread);
		TRACEF (str);
		HANDLE hEvent = ::CreateEvent (NULL, TRUE, FALSE, str);

		while (!::PostMessage (m_hControlWnd, WM_HEAD_DISCONNECT, (WPARAM)hEvent, (LPARAM)m_Config.m_HeadSettings.m_lID))
			::Sleep (0);

		DWORD dwWait = ::WaitForSingleObject (hEvent, CHead::m_dwExitTimeout);
		
	//	if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
		if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
		if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
		if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");

		::CloseHandle (hEvent);
	}
}

void CHead::OnHeadConfigChanged(UINT wParam, LONG lParam)
{
	ASSERT (lParam == 0);
	ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csConfig);
	TRACEF (_T ("OnHeadConfigChanged: ") + GetFriendlyName ());
	CHead::SetInfo (* this, &GetInfo ());
}

void CHead::OnSetExpSpan (UINT wParam, LONG lParam)
{
	#ifdef __WINPRINTER__
	{
		using namespace FoxjetElements;

		CElementPtrArray v;

		GetElements (v, EXPDATE);

		for (int i = 0; i < v.GetSize (); i++) {
			FoxjetCommon::CBaseElement * pElement = v [i];

			if (CExpDateElement * p = DYNAMIC_DOWNCAST (CExpDateElement, pElement)) { // sw0924
				switch (p->GetType ()) {
				case CExpDateElement::TYPE_DATABASE:
					CDatabaseElement e (p->GetHead ());

					if (m_strKeyValue.IsEmpty ())
						m_strKeyValue = theApp.GetProfileString (_T ("sw0858"), p->GetPrompt ());

					p->SetKeyValue (m_strKeyValue);
					e.Set (* p);
					e.Build ();
					CString str = e.GetImageData ();
					TRACEF (str);
					p->SetSpan (_ttoi (str));
					p->Build (INITIAL, false);
				}
			}
		}
	}
	#endif //__WINPRINTER__
}

void CHead::OnStartTask(UINT wParam, LONG lParam)
{
	if (HANDLE hEvent = (HANDLE)wParam) 
		::SetEvent (hEvent);
	else {
		CString str;
		
		str.Format (_T ("ImageReady %lu"), m_hThread);

		HANDLE h = ::CreateEvent (NULL, TRUE, FALSE, str);
		::SetEvent (h);
		::CloseHandle (h);
	}

	OnSetExpSpan (0, 0);
	UpdateStatusMsg ();

	if (IsMaster ())
		while (!::PostMessage (m_hControlWnd, WM_UPDATE_PREVIEW, 0, (LPARAM)this)) 
			::Sleep (0);

	::Sleep (0);
}

void CHead::OnStopTask(UINT wParam, LONG lParam)
{
	m_strKeyValue.Empty ();

	if (m_pImageEng->m_pPreviewBitmap) {
		delete m_pImageEng->m_pPreviewBitmap;
		m_pImageEng->m_pPreviewBitmap = NULL;
	}

	if (m_pImageEng->m_pImageBitmap) {
		delete m_pImageEng->m_pImageBitmap;
		m_pImageEng->m_pImageBitmap = NULL;
	}

	if (HANDLE hEvent = (HANDLE)lParam)
		SetEvent (hEvent);

	::Sleep (0);
}

void CHead::OnSetKeyValue(UINT wParam, LONG lParam)
{
	if (CString * p = (CString *)wParam) {
		m_strKeyValue = * p;
		delete p;
	}

	if (lParam)
		OnSetExpSpan (0, 0);
}

void CHead::OnIdleTask(UINT wParam, LONG lParam)
{
	CString sEvent;
	sEvent.Format (_T ("IdleTask %lu"), m_nThreadID);
	HANDLE hEvent = CreateEvent (NULL, TRUE, FALSE, sEvent );
	SetEvent ( hEvent );
	CloseHandle ( hEvent );
}

void CHead::OnResumeTask(UINT wParam, LONG lParam)
{
	CString sEvent;
	sEvent.Format (_T ("ResumeTask %lu"), m_nThreadID);
	HANDLE hEvent = CreateEvent (NULL, TRUE, FALSE, sEvent );
	SetEvent ( hEvent );
	CloseHandle ( hEvent );
}

int CHead::BuildImage (FoxjetCommon::BUILDTYPE imgtype, bool bOverrideImagingFlag)
{
DECLARETRACECOUNT (200);
MARKTRACECOUNT ();
	ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);

	int value = IMAGE_SUCCESS;

	PUCHAR pImageBuffer = NULL;

	FoxjetCommon::CElementList empty;
	FoxjetCommon::CElementList * pList = !m_bTestPattern ? (m_pElementList ? m_pElementList : &empty) : NULL;
	//FoxjetCommon::CElementList * pList = !m_bTestPattern ? m_pElementList : NULL;

#ifdef __IPPRINTER__
	if (theApp.GetPreview ())
		value = m_pImageEng->GenImage ( pList, m_Config.GetHeadSettings(), NULL, GetImageBufferSize (), imgtype, m_listImgUpdDesc, FoxjetCommon::EDITOR);

#else	// !__IPPRINTER__
MARKTRACECOUNT ();
	ProcessVariableElements (imgtype);
MARKTRACECOUNT ();

	if (imgtype == INITIAL) { //( bInitialBuild )
MARKTRACECOUNT ();
		if ( value == IMAGE_SUCCESS )
			value = m_pImageEng->GenImage (pList, m_Config.GetHeadSettings(), m_pImageBuffer, GetImageBufferSize (), imgtype, m_listImgUpdDesc, m_DrawContext, false, this);
MARKTRACECOUNT ();

		if (theApp.GetPreview ())
			value = m_pImageEng->GenImage ( pList, m_Config.GetHeadSettings(), NULL, GetImageBufferSize (), imgtype, m_listImgUpdDesc, FoxjetCommon::EDITOR, false, this);
MARKTRACECOUNT ();
	}
	else {
		value = ProcessVariableElements (imgtype);
		bool bBuild = value > 0 || bOverrideImagingFlag;
	
MARKTRACECOUNT ();
		bBuild |= (m_vVarElements.GetSize () > 0);

		//TRACEF (_T ("********** ") + GetFriendlyName () + _T (": bBuild: ") + ToString (bBuild) + _T (", value: ") + ToString (value));

		if (theApp.IsDroppedElementTest ())
			bBuild = true;

MARKTRACECOUNT ();
		if (bBuild)
			value = m_pImageEng->GenImage ( pList, m_Config.GetHeadSettings(), m_pImageBuffer, GetImageBufferSize (), imgtype, m_listImgUpdDesc, m_DrawContext, 0, this);
MARKTRACECOUNT ();
	}
#endif // !__IPPRINTER__

MARKTRACECOUNT ();
	if (value != IMAGE_SUCCESS) {
		CString str = GetFriendlyName () + " BuildImage failed [0]";
		DisplayDebugMessage (str);
	}

	m_ImageHdr.ippc	= GlobalFuncs::GetPhcChannels (m_Config.GetHeadSettings ()); 
	m_ImageHdr.ibpc	= ( BYTE )(m_ImageHdr.ippc / 8);
	m_ImageHdr.irow	= 0;
	m_ImageHdr.istart = 0;
	m_ImageHdr.ilen	= (ULONG)m_pImageEng->GetImageLen();
	
MARKTRACECOUNT ();
	if (imgtype == IMAGING) 
		m_tmImage = CTime::GetCurrentTime ();

MARKTRACECOUNT ();
//TRACECOUNTARRAYMAX ();

	if (m_vRstCacheKey.size ()) {
		for (int i = 0; i < m_vRstCacheKey.size (); i++) {
			//TRACER (m_vRstCacheKey [i].c_str ());
			COdbcDatabase::FreeRstCache (m_vRstCacheKey [i].c_str ());
		}

		m_vRstCacheKey.clear ();
	}

	return value;
}

int CHead::BuildDynamicElemList(FoxjetCommon::CElementList *pElementList)
{
	FreeDynamicElemList ();

#ifdef  __WINPRINTER__

	using namespace FoxjetElements;

	CElementPtrArray vBitmapElements;

	GetElements (m_vVarElements, FoxjetIpElements::COUNT);
	GetElements (m_vVarElements, FoxjetIpElements::DATAMATRIX);
	GetElements (m_vVarElements, FoxjetIpElements::DYNAMIC_TEXT);
	GetElements (m_vVarElements, FoxjetIpElements::DYNAMIC_BARCODE);

	GetElements (m_vVarElements, COUNT);
	GetElements (m_vVarElements, DATETIME);
	GetElements (m_vVarElements, EXPDATE);
	GetElements (m_vVarElements, SHIFTCODE);
	GetElements (m_vVarElements, USER);
	GetElements (m_vVarElements, SERIAL);

	GetElements (m_vUserElements, USER);
	GetElements (m_vSerialElements, SERIAL);
	GetElements (m_vLabelElements, LABEL);
	GetElements (m_vDatabaseElements, DATABASE);

	GetElements (vBitmapElements, BMP);

	for (int i = m_vDatabaseElements.GetSize () - 1; i >= 0; i--) {
		if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, m_vDatabaseElements [i])) 
			/* TODO: rem
			if (/* ! * / p->GetSerial ()) 
				m_vDatabaseElements.RemoveAt (i);
			else
				m_vVarElements.Add (p);
			*/
			if (p->GetSerial ()) 
				m_vVarElements.Add (p);
	}

	for (int i = vBitmapElements.GetSize () - 1; i >= 0; i--) {
		if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, vBitmapElements [i])) {
			if (p->IsDatabaseLookup ())
				m_vVarElements.Add (p);
		}
	}
	
#endif //__WINPRINTER__

	return IMAGE_SUCCESS;
}

void CHead::FreeDynamicElemList ()
{
#ifdef  __WINPRINTER__
	m_vSerialElements.RemoveAll();
	m_vUserElements.RemoveAll();
	m_vVarElements.RemoveAll ();
	m_vLabelElements.RemoveAll ();
	m_vDatabaseElements.RemoveAll ();
#endif //__WINPRINTER__
}


DWORD CHead::GetLastError()
{
	return m_dwLastError;
}

void CHead::SetLastError(DWORD dwError)
{
	m_dwLastError = dwError;
}

#ifndef __IPPRINTER__

int CHead::ProcessDatabaseElements (FoxjetCommon::BUILDTYPE imgtype)
{
	using namespace FoxjetCommon;
	using namespace FoxjetElements;

	int nResult = 0;
	Head::CElementPtrArray v;

	GetElements (v, DATABASE);

	for (int i = 0; i < v.GetSize (); i++) {
		FoxjetCommon::CBaseElement * pElement = v [i];

		if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, pElement)) {
			if (!p->TTL ()) {
				std::wstring strKey = p->GetRstCacheKey ();

				p->Invalidate ();
				
				if (strKey.length ())
					if (Find (m_vRstCacheKey, strKey) == -1)
						m_vRstCacheKey.push_back (strKey);

				nResult++;
			}
		}
	}

	return nResult;
}

int CHead::ProcessSerialElements (FoxjetCommon::BUILDTYPE imgtype)
{
	using namespace FoxjetCommon;

#ifdef __WINPRINTER__
	using namespace FoxjetElements;
#endif //__WINPRINTER__

	const TCHAR * pBuffer = m_params.m_serialData.Data;

	m_vSerialElements.RemoveAll ();
	GetElements (m_vSerialElements, FoxjetIpElements::DYNAMIC_TEXT);
	GetElements (m_vSerialElements, FoxjetIpElements::DYNAMIC_BARCODE);
	GetElements (m_vSerialElements, SERIAL);

	for (int i = 0; i < m_vSerialElements.GetSize (); i++) {
		FoxjetCommon::CBaseElement * pElement = m_vSerialElements [i];

		#ifdef __WINPRINTER__
		if (CSerialElement * p = DYNAMIC_DOWNCAST (CSerialElement, pElement)) {
			p->SetBuffer (pBuffer);

			{ // sw0864
				int nIndex = p->GetTableIndex ();

				if (nIndex >= 0 && ARRAYSIZE (m_sz0864)) {
					const TCHAR * pBuffer = (TCHAR *)&m_sz0864 [nIndex - 1];
		
					//{ CString str; str.Format ("[%d]: %s", nIndex, pBuffer); TRACEF (str); }
					p->SetBuffer (pBuffer);
				}
			}
		}
		#endif //__WINPRINTER__

		if (FoxjetIpElements::CIpBaseElement * p = DYNAMIC_DOWNCAST (FoxjetIpElements::CIpBaseElement, pElement)) 
			p->SetRedraw ();
	}

	return m_vSerialElements.GetSize ();
}

/*
#ifdef _DEBUG
#include "DatabaseElement.h"
#endif //_DEBUG
*/

int CHead::ProcessVariableElements (FoxjetCommon::BUILDTYPE imgtype)
{
	int nResult = m_vVarElements.GetSize ();

	ProcessSerialElements (imgtype);
	nResult += ProcessDatabaseElements (imgtype);
/*
#ifdef _DEBUG
	using namespace FoxjetElements;
	int nCount = 0;

	for (int i = 0; i < m_pElementList->GetSize (); i++) {
		if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, &(* m_pElementList) [i])) {
			p->Build ();
			nCount++;
		}
	}

	return nCount + m_vVarElements.GetSize ();
#endif //_DEBUG
*/

	return nResult;
}


#endif // !__IPPRINTER__

bool CHead::Load (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::TASKSTRUCT & task, 
				  const FoxjetDatabase::BOXSTRUCT & box, const CMap <ULONG, ULONG, ULONG, ULONG> & mapHeads)
{
	using namespace FoxjetDatabase;

	#ifdef __WINPRINTER__						
	using namespace FoxjetElements;
	#endif //__WINPRINTER__						

	const HEADSTRUCT head = m_Config.GetHeadSettings();
	int nCount = 0;

	m_bModifiedAfterLoad = false;

	m_Task = task;
	m_Box = box;

	CCache * p = NULL;
	bool bCached = false;

	{
		LOCK (m_vCache.m_cs);
		bCached = m_vCache.Lookup (task.m_lID, p) ? true : false;
	}

	if (bCached) {
		ASSERT (p);

		m_pElementList = &p->m_list;
	}
	else {
		ULONG lHeadID = head.m_lLinkedTo == NOTFOUND ? head.m_lID : head.m_lLinkedTo;

		lHeadID = GetHeadMapping (db, head, GetMasterPrinterID (db)); // sw0867

		if (!m_pElementList) 
			m_pElementList = &m_defCache.m_list;

		mapHeads.Lookup (lHeadID, lHeadID);

		if (!GetMessageRecord (db, m_Task.m_strName, lHeadID, m_Message)) {
			m_pElementList->DeleteAllElements ();
			m_Message.m_lID		= -1;
			m_Message.m_lHeadID	= head.m_lID;
			m_Message.m_lTaskID = m_Task.m_lID;
			m_Message.m_strName = m_Task.m_strName;
			m_Message.m_strDesc.Empty ();
			m_Message.m_strData.Empty ();
			//return false;
		}

		m_sMessageName	= m_Message.m_strName;
		m_sTaskName		= m_Task.m_strName;

		m_pElementList->SetHead (head);
		m_pElementList->SetSnap (false);
		int nElements = m_pElementList->FromString (m_Message.m_strData, GlobalVars::CurrentVersion, true, m_Task.m_lLineID);

		m_pElementList->SetUserDefined (m_Message.m_strData);

		//TRACEF (_T ("CHead::Load: ") + m_Task.m_strName);
		CHead::SetInfo (* this, &GetInfo ());

		#ifdef __WINPRINTER__						
		{ // force any linked text elements to be the last in the list
			CElementPtrArray v;
			using namespace TextElement;

			for (int i = m_pElementList->GetSize () - 1; i >= 0; i--) {
				if (CTextElement * pElement = DYNAMIC_DOWNCAST (CTextElement, &m_pElementList->GetObject (i))) {
					if (pElement->GetLink ()) {
						v.Add (pElement);
						m_pElementList->RemoveAt (i);
					}
				}
			}

			for (int i = 0; i < v.GetSize (); i++) 
				m_pElementList->Add (v [i]);
		}
		#endif //__WINPRINTER__						

		for (int i = 0; i < m_pElementList->GetSize (); i++) {
			FoxjetCommon::CBaseElement * p = &m_pElementList->GetObject (i);
			CPoint pt = FoxjetCommon::CBaseElement::ThousandthsToLogical (p->GetPos (&m_Config.m_HeadSettings), m_Config.m_HeadSettings);

			if (p->GetClassID () == COUNT)
				nCount++;

			if (pt.y > (int)m_Config.m_HeadSettings.m_nChannels) {
				TRACEF (p->ToString (FoxjetCommon::verApp));
			}
		}

	}

	#ifdef __IPPRINTER__
	LPFJSYSTEM pSystem = FoxjetIpElements::FindSystem (m_Task.m_lLineID);
	ASSERT (pSystem);
	ASSERT (pSystem->pfl);
	memcpy (pSystem->pfl, m_pElementList->GetLabel (), sizeof (FJLABEL));
	#endif

	BuildDynamicElemList (m_pElementList); 

	#ifdef __WINPRINTER__
	if (FoxjetCommon::IsProductionConfig ()) {
		using namespace FoxjetElements;

		CElementPtrArray v;

		GetElements (v, BMP);

		for (int i = 0; i < v.GetSize (); i++) 
			if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, v [i])) 
				Foxjet3d::UserElementDlg::CUserElementDlg::ResetSize (p, m_Config.m_HeadSettings);
	}
	#endif //__WINPRINTER__

	if (theApp.PromptForCountAtTaskStart () && nCount > 0)
		((CMainFrame *)theApp.m_pMainWnd)->PostMessage (WM_COMMAND, MAKEWPARAM (ID_OPERATE_CHANGECOUNTS, 0));

	return true;
}

void CHead::FreeCache ()
{
	LOCK (m_vCache.m_cs);

	for (POSITION pos = m_vCache.GetStartPosition (); pos != NULL; ) {
		CCache * p = NULL;
		ULONG lTaskID = -1;

		m_vCache.GetNextAssoc (pos, lTaskID, p);
		ASSERT (p);

		delete p;
	}

	m_vCache.RemoveAll ();
}

void CHead::Cache (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::TASKSTRUCT & task, 
				  const FoxjetDatabase::BOXSTRUCT & box)
{
	using namespace FoxjetDatabase;

	LOCK (m_vCache.m_cs);

	const HEADSTRUCT & head = m_Config.GetHeadSettings();
	MESSAGESTRUCT msg;
	bool bFound = false;
	CString * pstr = &msg.m_strData;

	for (int i = 0; !bFound && i < task.m_vMsgs.GetSize (); i++) {
		MESSAGESTRUCT & m = const_cast <TASKSTRUCT &> (task).m_vMsgs [i];

		if (m.m_lHeadID == head.m_lID) {
			pstr = & m.m_strData;
			bFound = true;
			break;
		}
	}

	if (!bFound) {
		ULONG lHeadID = GetHeadMapping (db, head, GetMasterPrinterID (db)); // sw0867

		if (!GetMessageRecord (db, task.m_strName, lHeadID, msg)) 
			return;

		msg.m_lHeadID = head.m_lID;		// sw0867
		msg.m_lTaskID = task.m_lID;		// sw0867
		msg.m_strName = task.m_strName;	// sw0867
	}

	CCache * p = new CCache ();
	
	p->m_list.SetHead (head);
	p->m_list.SetSnap (false);
	int nElements = p->m_list.FromString (* pstr, GlobalVars::CurrentVersion);
	p->m_list.SetUserDefined (* pstr);
	
	m_vCache.SetAt (task.m_lID, p);
}

void CHead::GetElements (CElementPtrArray & v, int nClassID)
{
using namespace FoxjetCommon;

#ifdef __WINPRINTER__
	using namespace FoxjetElements;
#endif //__WINPRINTER__

	if (m_pElementList) {
		for (int i = 0; i < m_pElementList->GetSize (); i++) { 
			CBaseElement * p = &m_pElementList->GetObject (i);

			if (FoxjetIpElements::CIpBaseElement * pIP = DYNAMIC_DOWNCAST (FoxjetIpElements::CIpBaseElement, p)) {
				if (p->GetClassID () == nClassID)
					v.Add (p);

				continue;
			}

			if (p->GetClassID () == nClassID)
				v.Add (p);
			else {

#ifdef __WINPRINTER__
				
				if (CBarcodeElement * pBarcode = DYNAMIC_DOWNCAST (CBarcodeElement, p)) {
					for (int i = 0; i < pBarcode->GetSubElementCount (); i++) {
						CSubElement & s = pBarcode->GetSubElement (i);

						if (s.GetElement ()->GetClassID () == nClassID)
							v.Add (s.GetElement ());
					}
				}

#endif //__WINPRINTER__

			}
		}
	}
}

void CHead::GetElements (CElementPtrArray & v)
{
	using namespace FoxjetCommon;

	if (m_pElementList) {
		for (int i = 0; i < m_pElementList->GetSize (); i++) { 
			CBaseElement * p = &m_pElementList->GetObject (i);

			v.Add (p);
		}
	}
}

bool CHead::IsError (const _STATUS & s)
{
	bool bResult = false;

	bResult |= (s.m_nInkLevel	== INK_OUT);
	bResult |= (s.m_bAtTemp		== false);
	bResult |= (s.m_bVoltageOk	== false);
	bResult |= (s.m_bBrokenLine == true);

	return bResult;
}

bool CHead::IsWarning (const _STATUS & s)
{
	bool bResult = false;

	bResult |= (s.m_nInkLevel		== LOW_INK);
	bResult |= (s.m_bIDSConnected	== false);

	return bResult;
}

void CHead::CheckState ()
{
	if (m_Status.m_lCount != m_Status.m_lLastCount) {
		m_tmLastPrint			= CTime::GetCurrentTime ();
		m_Status.m_lLastCount	= m_Status.m_lCount;
	}
	else {
		if (IsWaxHead () && !GetStandbyMode () && m_nStandbyTimeout > 0) {
			CTimeSpan tm = CTime::GetCurrentTime () - m_tmLastPrint;
			int nElapsed = tm.GetTotalMinutes ();
			
			ASSERT (tm.GetTotalSeconds () >= 0);
			ASSERT (tm.GetTotalMinutes () >= 0);

			if (nElapsed > m_nStandbyTimeout) {
				if (!GetStandbyMode ())
					SetStandbyMode (true);

				while (!::PostMessage (m_hControlWnd, WM_IDLE_TASK, m_Config.m_Line.m_lID, (LPARAM)this))
					::Sleep (0);
			}
		}
	}

	m_dwThreadID = m_nThreadID;
	CHead::SetInfo (* this, &GetInfo ());
}

bool CHead::IsWaxHead () const
{
	switch (m_Config.m_HeadSettings.m_nHeadType) {
	case FoxjetDatabase::UJ2_192_32NP:
		return true;
	}

	return false;
}

DWORD CHead::GetIdleFlags () const
{
	return m_dwIdleFlags;
}

bool CHead::SetIdleFlags (DWORD dwFlags)
{
	m_dwIdleFlags = dwFlags;
	return true;
}

void CHead::SetCounts (const CHead::CHANGECOUNTSTRUCT & count)
{
	while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this)) 
		::Sleep(0);
}

bool CHead::SetStandbyMode (bool bStandby)
{
	m_bStandby = bStandby;

	if (!m_bStandby)
		m_tmLastPrint = CTime::GetCurrentTime (); 
		// so CheckState won't attempt to go into standby until 
		// another timeout period has expired

	return true;
}

bool CHead::GetStandbyMode () const
{
	return m_bStandby;
}

bool CHead::IsError (DWORD dw)
{
	const DWORD dwMask = HEADERROR_LOWTEMP | HEADERROR_HIGHVOLTAGE | HEADERROR_OUTOFINK | IVERROR_BROKENLINE | HEADERROR_EP8;

	return (dwMask & dw) ? true : false;
}

bool CHead::IsWarning (DWORD dw)
{
	const DWORD dwMask = HEADERROR_LOWINK | IVERROR_DISCONNECTED;

	return (dwMask & dw) ? true : false;
}

DWORD CHead::GetErrorState () const
{
	DWORD dw = 0;

	if (!m_bDisabled) {
		dw |= (m_Status.m_nInkLevel		== LOW_INK) ? HEADERROR_LOWINK		: 0;
		dw |= (m_Status.m_nInkLevel		== INK_OUT) ? HEADERROR_OUTOFINK	: 0;
		dw |= (m_Status.m_bAtTemp		== false)	? HEADERROR_LOWTEMP		: 0;
		dw |= (m_Status.m_bVoltageOk	== false)	? HEADERROR_HIGHVOLTAGE : 0;
		dw |= (m_Status.m_bBrokenLine	== true)	? IVERROR_BROKENLINE	: 0;
		dw |= (m_Status.m_bIDSConnected	== false)	? IVERROR_DISCONNECTED	: 0;
		dw |= (m_Status.m_bFPGA			== false)	? HEADERROR_NOFPGA		: 0;
		dw |= (m_Status.m_bEp8Error		== true)	? HEADERROR_EP8			: 0;
		dw |= (m_eTaskState				== STOPPED)	? HEADERROR_STOPPED		: 0;
		dw |= (m_eTaskState				== IDLE)	? HEADERROR_PAUSED		: 0;

		if (CHpHead * p = DYNAMIC_DOWNCAST (CHpHead, this))
			dw |= (!p->IsCommportOpen ()) ? HPERROR_COMMPORTFAILURE : 0;

		#ifdef __IPPRINTER__
		if (CRemoteHead * p = DYNAMIC_DOWNCAST (CRemoteHead, this))
			if (!p->IsConnected ())
				dw = HEADERROR_DISCONNECTED; // note that this supercedes all other errors
		#endif
	}

	const int nLineSpeed = (int)(m_Status.m_dLineSpeed * 10);

	if (m_dwLastState != dw || abs (nLineSpeed - m_nLineSpeed) > 1) {
		//{ CString str; str.Format (_T("[%s] state: 0x%X [0x%X], line speed: %d [%d]"), m_Config.m_HeadSettings.m_strUID, dw, m_dwLastState, nLineSpeed, m_nLineSpeed); TRACER (str); }

		while (!::PostMessage (m_hControlWnd, WM_SOCKET_PACKET_PRINTER_HEAD_STATE_CHANGE, m_Config.m_HeadSettings.m_lID, 0)) 
			::Sleep (0);
	}

	#ifdef __WINPRINTER__ // sw0868
	if (m_dwLastState != -1) { 
		if (CLocalHead * pHead = DYNAMIC_DOWNCAST (CLocalHead, this)) {
			DWORD dwTest = pHead->GetErrorMask ();
			bool bNewError = false;
			bool bCleared = false;

			if (dw != m_dwLastState) {
				DWORD dwLast = (m_dwLastState & dwTest);
				DWORD dwCurrent = (dw & dwTest);

				bNewError = (dwLast & dwCurrent) != dwCurrent;
			}

			if (!bNewError) {
				bCleared = 
					((dw			& dwTest) == 0) &&
					((m_dwLastState & dwTest) > 0);

				if (m_dwLastState && (dw == HEADERROR_LOWINK) && (m_dwLastState != HEADERROR_LOWINK)) {
					// special case where all other errors were cleared, but "low ink" remains

					bCleared = false;
					bNewError = true;
				}
			}

			if (bCleared) {
				TRACEF ("WM_UPDATE_IMAGE");

				while (!::PostMessage (m_hControlWnd, WM_HEAD_ERROR_CLEARED, 0, (LPARAM)m_Config.m_Line.m_lID)) 
					::Sleep (0);
	
				while (!::PostMessage (m_hControlWnd, WM_UPDATE_IMAGE, 0, (LPARAM)m_Config.m_Line.m_lID)) 
					::Sleep (0);
			}
			else if (bNewError)
				theApp.m_bStrobeCancelled = false;
		}
	}
	#endif //__WINPRINTER__

	((CHead *)(LPVOID)this)->m_nLineSpeed = nLineSpeed;
	((CHead *)(LPVOID)this)->m_dwLastState = dw;
			
	return dw;
}

void CHead::OnUpdateCounts (UINT wParam, LONG lParam)
{
	if (HANDLE h = (HANDLE)wParam)
		::SetEvent (h);
}

void CHead::OnBuildPreviewImage (UINT wParam, LONG lParam)
{
	ASSERT (m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csImage);

	FoxjetCommon::CElementList empty;
	FoxjetCommon::CElementList * pList = !m_bTestPattern ? (m_pElementList ? m_pElementList : &empty) : NULL;

	if (m_bDisabled) {
		DeleteElement (-1);
		m_pImageEng->Free ();
	}

	m_pImageEng->GenImage (pList, m_Config.GetHeadSettings(), NULL, GetImageBufferSize (), INITIAL, m_listImgUpdDesc, 
		FoxjetCommon::EDITOR, true /* SW0828 */, this);

	if (HANDLE h = (HANDLE)wParam)
		::SetEvent (h);
}

void CHead::OnSetIgnorePhotocell (UINT wParam, LONG lParam)
{
	bool bIgnore = wParam ? 1 : 0;

/*
	CString str;

	str.Format (_T ("%s: OnSetIgnorePhotocell: %s"),
		GetFriendlyName (),
		bIgnore ? _T ("true") : _T ("false"));
	TRACEF (str);
*/
}


void CHead::SetSerialPending (bool bPending)
{
	if (IsOneToOnePrint ()) // __SW0808__
		m_bSerialPending = bPending;
	else
		m_bSerialPending = false;
}

bool CHead::GetSerialPending () const
{
	return m_bSerialPending;
}

BOOL CHead::InitInstance ()
{
	m_dwThreadID = m_nThreadID;
	return TRUE;
}

int CHead::ExitInstance ()
{
	TRACEF (_T ("CHead::ExitInstance: ") + GetFriendlyName ());

	FreeCache ();

	if ( m_pImageEng ) {
		delete m_pImageEng;
		m_pImageEng = NULL;
	}

	FreeImgUpdDesc ();
	FreeCommSettings ();

	if (m_hExit) {
		::CloseHandle (m_hExit);
		m_hExit = NULL;
	}

	CHead::SetInfo (* this, NULL);

	return 0;
}

bool CHead::DeleteElement (ULONG lID)
{
	if (!m_pElementList) 
		m_pElementList = &m_defCache.m_list;

	if (m_pElementList) {
		if (lID == -1) {
			m_pElementList->DeleteAllElements ();

			return true;
		}
			
		for (int i = 0; i < m_pElementList->GetSize (); i++) {
			FoxjetCommon::CBaseElement & e = m_pElementList->GetObject (i);

			if (e.GetID () == lID) {
				m_pElementList->RemoveAt (i);
				m_bModifiedAfterLoad = true;
				delete & e;

				return true;
			}
		}
	}

	return false;
}

ULONG CHead::AddElement (const CString & str)
{
	using namespace FoxjetCommon;

	if (m_pElementList) {
		if (m_pElementList->FromString (str, CVersion (verApp), false)) {
			int nIndex = m_pElementList->GetSize () - 1;
			CBaseElement & e =  m_pElementList->GetObject (nIndex);

			TRACEF (e.ToString (FoxjetCommon::CVersion ()));
			m_bModifiedAfterLoad = true;

			return e.GetID ();
		}
	}

	return 0;
}

bool CHead::UpdateElement (const CString & str)
{
	using namespace FoxjetCommon;
	int nCount = 0;

	if (m_pElementList) {
		CElementList v (false);

		v.SetHead (m_Config.m_HeadSettings);
		v.FromString (str, CVersion (verApp));

		for (int nAdd = 0; nAdd < v.GetSize (); nAdd++) {
			CBaseElement & eAdd = v.GetObject (nAdd);
			bool bFound = false;

			for (int i = 0; i < m_pElementList->GetSize (); i++) {
				CBaseElement & e = m_pElementList->GetObject (i);

				if (e.GetID () == eAdd.GetID ()) {
					m_pElementList->SetAt (i, &eAdd);
					m_bModifiedAfterLoad = true;
					delete &e;
					bFound = true;
					nCount++;
					TRACEF ("update: " + eAdd.ToString (CVersion ()));
					break;
				}
			}

			if (!bFound) {
				TRACEF ("add: " + eAdd.ToString (CVersion ()));
				m_pElementList->Add (&eAdd);
				m_bModifiedAfterLoad = true;
				nCount++;
			}
		}
	}

	return nCount > 0 ? true : false;
}



CString CHead::GetStatusMsg () const
{
	LOCK (::csStatus);
	CString str;

	//::EnterCriticalSection (&::csStatus);
	//TraceThreadContext (_T (__FILE__), __LINE__);
	::mapStatus.Lookup (m_Config.m_HeadSettings.m_lID, str); 
	//::LeaveCriticalSection (&::csStatus);

	return str;
}

void CHead::UpdateStatusMsg (const CString & str) 
{
	LOCK (::csStatus);

	//TraceThreadContext (_T (__FILE__), __LINE__);
	m_strDisplay = str;

	//::EnterCriticalSection (&::csStatus);

	if (m_strDisplay.GetLength ()) {
		::mapStatus.SetAt (m_Config.m_HeadSettings.m_lID, str);

		while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_STATUS_MSG, (LPARAM)this)) 
			::Sleep (0);
	}
	else {
		::mapStatus.RemoveKey (m_Config.m_HeadSettings.m_lID);

		while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM)this)) 
			::Sleep (0);
	}

	//::LeaveCriticalSection (&::csStatus);
}

void CHead::OnProcessSerialData (UINT wParam, LONG lParam)
{
	if (CSerialPacket * pPacket = (CSerialPacket *)wParam) 
		delete pPacket;
}

void CHead::DisplayDebugMessage (const CString & str)
{
#ifdef _DEBUG
	TRACEF (str);
//	((CMainFrame *)theApp.m_pMainWnd)->m_pControlView->OnShowDiag (0, 0);
//	((CMainFrame *)theApp.m_pMainWnd)->m_pControlView->UpdateSerialData (str);

	if (FILE * fp = _tfopen (_T ("Debug.txt"), _T ("a"))) {
		CString s = CTime::GetCurrentTime ().Format ("[%c] ") + str + _T ("\r\n");
		fwrite ((LPCTSTR)s, s.GetLength (), 1, fp);
		fclose (fp);
	}
#endif //_DEBUG
}

CString CHead::GetSerialData ()
{
	CString str;

	if (IsMaster ()) 
		str += m_Task.m_strDownload;

	#ifdef __WINPRINTER__
	{
		using namespace FoxjetElements;

		CElementPtrArray v;

		GetElements (v, DATABASE);

		for (int i = 0; i < v.GetSize (); i++) {
			if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, v [i])) {
				if (p->GetSerialDownload ()) {
					p->Build (FoxjetCommon::INITIAL, false);
					str += p->GetImageData () + _T ("\r");
				}
			}
		}
	}
	#endif

	return str;
}

void CHead::OnGetInfo (UINT wParam, LONG lParam)
{
/* TODO: rem
	if (CHeadInfo * p = (CHeadInfo *)wParam) {

		//p->m_dwThreadID		= m_nThreadID;
		//p->m_nErrorIndex	= m_nErrorIndex;
		//* p = * this;

		if (CLocalHead * pHead = DYNAMIC_DOWNCAST (CLocalHead, this)) {
			//p->m_wGaVer		= pHead->GetGaVersion ();
			{ CString str; str.Format (_T ("0x%p: %s"), pHead->GetGaVersion ()); TRACEF (str); }
		}
	}
*/

	if (HANDLE h = (HANDLE)lParam)
		::SetEvent (h);
}

CHeadInfo CHead::GetInfo (int) const
{
	LOCK (::csInfo);
	return CHead::GetInfo (this);
}

CHeadInfo CHead::GetInfo () const
{
	LOCK (::csInfo);
	return * this;
}

CHeadInfo CHead::GetInfo (const CHead * p) 
{ 
	LOCK (::csInfo);
	ASSERT ((void *)p != (void *)0xdddddddd);

	if (p)
		return GetInfo (* p);
		
	return CHeadInfo (); 
}

void CHead::InitializeCriticalSection ()
{
	//::InitializeCriticalSection (&::csInfo);
	//::InitializeCriticalSection (&::csStatus);
}

void CHead::DeleteCriticalSection ()
{
	//::DeleteCriticalSection (&::csInfo);
	//::DeleteCriticalSection (&::csStatus);
}

void CHead::FreeInfo ()
{
	LOCK (::csInfo); //::EnterCriticalSection (&::csInfo);

	for (POSITION pos = ::mapInfo.GetStartPosition (); pos; ) {
		CHeadInfo * p = NULL;
		DWORD dw = 0;

		::mapInfo.GetNextAssoc (pos, dw, p);

		if (p)
			delete p;
	}

	::mapInfo.RemoveAll ();

	//::LeaveCriticalSection (&::csInfo);
}

void CHead::SetInfo (const CHead & h, const CHeadInfo * pInfo)
{
	LOCK (::csInfo); //::EnterCriticalSection (&::csInfo);

	if (h.m_nThreadID > 0) {
		CHeadInfo * p = NULL;

		if (pInfo) {
			//TRACEF (pInfo->m_Config.m_Line.m_strName);

			if (::mapInfo.Lookup (h.m_nThreadID, p))
				* p = * pInfo;
			else {
				TRACEF (_T ("CHead::SetInfo: ") + pInfo->m_Config.m_HeadSettings.m_strName + _T (": ") + pInfo->m_Config.m_HeadSettings.m_strUID);
				p = new CHeadInfo (* pInfo);
			}

			::mapInfo.SetAt (h.m_nThreadID, p);
		}
		else {
			if (::mapInfo.Lookup (h.m_nThreadID, p)) {
				delete p;
				::mapInfo.RemoveKey (h.m_nThreadID);
			}
		}
	}

	//::LeaveCriticalSection (&::csInfo);
}

CHeadInfo CHead::GetInfo (const CHead & h) 
{
	LOCK (::csInfo); //::EnterCriticalSection (&::csInfo);

	CHeadInfo info;

	CHeadInfo * pInfo = NULL;

	if (::mapInfo.Lookup (h.m_nThreadID, pInfo))
		if (pInfo)
			info = CHeadInfo (* pInfo);

	//::LeaveCriticalSection (&::csInfo);

	return info;
}

void CHead::OnIncrementErrorIndex (UINT wParam, LONG lParam)
{
	m_nErrorIndex++;
}

void CHead::FreeCommSettings ()
{
	for (POSITION pos = m_vCommSettings.GetStartPosition (); pos; ) {
		FoxjetCommon::StdCommDlg::CCommItem * p = NULL;
		CString str;

		m_vCommSettings.GetNextAssoc (pos, str, (void *&)p);

		if (p)
			delete p;
	}

	m_vCommSettings.RemoveAll ();
}

bool CHead::IsOneToOnePrint (eSerialDevice type)
{
	if (m_pdb) {
		CUIntArray v;

		FoxjetCommon::EnumerateSerialPorts (v);

		for (int i = 0; i < v.GetSize (); i++) {
			CString strPort;
			FoxjetCommon::StdCommDlg::CCommItem * pComm = NULL;

			strPort.Format (_T ("COM%d"), v [i]);

			if (!m_vCommSettings.Lookup (strPort, (void *&)pComm)) {
				SETTINGSSTRUCT s;

				pComm = new FoxjetCommon::StdCommDlg::CCommItem;
			
				if (GetSettingsRecord (* m_pdb, 0, strPort, s)) {
					TRACEF (s.m_strData);
					VERIFY (pComm->FromString (s.m_strData));
				}

				m_vCommSettings.SetAt (strPort, pComm);
			}

			if (pComm) 
				if (pComm->m_lLineID == m_Config.m_Line.m_lID)
					if (pComm->m_type == type && pComm->m_bPrintOnce) 
						return true;
		}
	}

	return false;
}

CString CHead::GetMessageString (UINT nMsg)
{
	struct
	{
		UINT m_n;
		LPCTSTR m_lpsz;
	} static const map [] =
	{
		{ TM_STATUS_UPDATE,					_T ("TM_STATUS_UPDATE				"), },
		{ TM_START_TASK,					_T ("TM_START_TASK					"), },
		{ TM_STOP_TASK,						_T ("TM_STOP_TASK					"), },
		{ TM_IDLE_TASK,						_T ("TM_IDLE_TASK					"), },
		{ TM_RESUME_TASK,					_T ("TM_RESUME_TASK					"), },
		{ TM_HEAD_CONFIG_CHANGED,			_T ("TM_HEAD_CONFIG_CHANGED			"), },
		{ TM_CONNECT_COMPLETE,				_T ("TM_CONNECT_COMPLETE			"), },
		{ TM_CREATE_SOCKET,					_T ("TM_CREATE_SOCKET				"), },
		{ TM_RX_SOCKET_DATA,				_T ("TM_RX_SOCKET_DATA				"), },
		{ TM_SET_IO_ADDRESS,				_T ("TM_SET_IO_ADDRESS				"), },
		//{ TM_CHANGE_USER_ELEMENT_DATA,		_T ("TM_CHANGE_USER_ELEMENT_DATA	"), },
		{ TM_DELETE_MEMSTORE,				_T ("TM_DELETE_MEMSTORE				"), },
		{ TM_UPDATE_FONTS,					_T ("TM_UPDATE_FONTS				"), },
		{ TM_GET_MSG_IDS,					_T ("TM_GET_MSG_IDS					"), },
		{ TM_DELETE_LABEL,					_T ("TM_DELETE_LABEL				"), },
		{ TM_ENABLE_PHOTO,					_T ("TM_ENABLE_PHOTO				"), },
		{ TM_SHOW_DEBUG_DIALOG,				_T ("TM_SHOW_DEBUG_DIALOG			"), },
		{ TM_UPDATE_LABELS,					_T ("TM_UPDATE_LABELS				"), },
		{ TM_SOCKET_DISCONNECT,				_T ("TM_SOCKET_DISCONNECT			"), },
		{ TM_DATA_RECIEVED,					_T ("TM_DATA_RECIEVED				"), },
		{ TM_INITIAL_STARTUP,				_T ("TM_INITIAL_STARTUP				"), },
		{ TM_UPDATE_IP_LIST,				_T ("TM_UPDATE_IP_LIST				"), },
		{ TM_SYNC_LABEL_STORE,				_T ("TM_SYNC_LABEL_STORE			"), },
		{ TM_UPDATE_FIRMWARE,				_T ("TM_UPDATE_FIRMWARE				"), },
		{ TM_UPDATE_STROBE,					_T ("TM_UPDATE_STROBE				"), },
		{ TM_CHANGE_COUNTS,					_T ("TM_CHANGE_COUNTS				"), },
		{ TM_EVENT_PHOTO,					_T ("TM_EVENT_PHOTO					"), },
		{ TM_EVENT_EOP,						_T ("TM_EVENT_EOP					"), },
		{ TM_SET_IGNORE_PHOTOCELL,			_T ("TM_SET_IGNORE_PHOTOCELL		"), },
		{ TM_UPDATE_GA,						_T ("TM_UPDATE_GA					"), },
		{ TM_DOWNLOAD_DYNDATA,				_T ("TM_DOWNLOAD_DYNDATA			"), },
		{ TM_BUILD_PREVIEW_IMAGE,			_T ("TM_BUILD_PREVIEW_IMAGE			"), },
		{ TM_PROCESS_SERIAL_DATA,			_T ("TM_PROCESS_SERIAL_DATA			"), },
		{ TM_BUILD_IMAGE,					_T ("TM_BUILD_IMAGE					"), },
		{ TM_DOWNLOAD_IMAGE,				_T ("TM_DOWNLOAD_IMAGE				"), },
		{ TM_DOWNLOAD_IMAGE_SEGMENT,		_T ("TM_DOWNLOAD_IMAGE_SEGMENT		"), },
		{ TM_INCREMENT,						_T ("TM_INCREMENT					"), },
		{ TM_KILLTHREAD,					_T ("TM_KILLTHREAD					"), },
		{ TM_IDS_STATUS,					_T ("TM_IDS_STATUS					"), },
		{ TM_SOCKET_CONNECT,				_T ("TM_SOCKET_CONNECT				"), },
		{ TM_SOCKET_RETRY,					_T ("TM_SOCKET_RETRY				"), },
		{ TM_UPDATE_COUNTS,					_T ("TM_UPDATE_COUNTS				"), },
		{ TM_TRIGGER_WDT,					_T ("TM_TRIGGER_WDT					"), },
		{ TM_BUILD_NEXT_IMAGE,				_T ("TM_BUILD_NEXT_IMAGE			"), },
		{ TM_DOWNLOAD_IMAGES,				_T ("TM_DOWNLOAD_IMAGES				"), },
		{ TM_BEGIN_APS,						_T ("TM_BEGIN_APS					"), },
		{ TM_GET_INFO,						_T ("TM_GET_INFO					"), },
		{ TM_GET_ERROR_STATE,				_T ("TM_GET_ERROR_STATE				"), },
		{ TM_INCREMENT_ERROR_INDEX,			_T ("TM_INCREMENT_ERROR_INDEX		"), },
		{ TM_FIND_SLAVES,					_T ("TM_FIND_SLAVES					"), },
		{ TM_SET_DISPLAY_STRING,			_T ("TM_SET_DISPLAY_STRING			"), },
		{ TM_GET_MISSING_FONTS,				_T ("TM_GET_MISSING_FONTS			"), },
		{ TM_SIM_PHOTOCELL,					_T ("TM_SIM_PHOTOCELL				"), },
		{ TM_IS_PRINTING,					_T ("TM_IS_PRINTING					"), },
		{ TM_IS_USB,						_T ("TM_IS_USB						"), },	
		{ TM_GET_ELEMENTS,					_T ("TM_GET_ELEMENTS				"), },
		{ TM_GET_PRODUCTAREA,				_T ("TM_GET_PRODUCTAREA				"), },
		{ TM_IS_PHOTOENABLED,				_T ("TM_IS_PHOTOENABLED             "), },
		//{ TM_CREATE_CONFIGEVENT,			_T ("TM_CREATE_CONFIGEVENT			"), },
		//{ TM_GET_CONFIGEVENT,				_T ("TM_GET_CONFIGEVENT				"), },
		{ TM_GET_LASTERROR,					_T ("TM_GET_LASTERROR				"), },
		{ TM_SET_USERDATA,					_T ("TM_SET_USERDATA				"), },
		{ TM_ADD_ELEMENT,					_T ("TM_ADD_ELEMENT					"), },
		{ TM_UPDATE_ELEMENT,				_T ("TM_UPDATE_ELEMENT				"), },
		{ TM_DELETE_ELEMENT,				_T ("TM_DELETE_ELEMENT				"), },
		{ TM_DISPLAY_DEBUG_MSG,				_T ("TM_DISPLAY_DEBUG_MSG			"), },
		{ TM_UPDATE_STATUS_MSG,				_T ("TM_UPDATE_STATUS_MSG			"), },
		{ TM_CACHE,							_T ("TM_CACHE						"), },
		{ TM_FREE_CACHE,					_T ("TM_FREE_CACHE					"), },
		{ TM_LOAD,							_T ("TM_LOAD						"), },
		{ TM_IMAGE_FREE,					_T ("TM_IMAGE_FREE					"), },
		{ TM_GET_SERIALDATA,				_T ("TM_GET_SERIALDATA				"), },
		{ TM_GET_SERIALPENDING,				_T ("TM_GET_SERIALPENDING			"), },
		{ TM_GET_PREVIEW_BMP,				_T ("TM_GET_PREVIEW_BMP				"), },
		{ TM_IS_DBGVIEW,					_T ("TM_IS_DBGVIEW					"), },
		{ TM_IS_SHOW_PRINT_CYCLE,			_T ("TM_IS_SHOW_PRINT_CYCLE			"), },
		{ TM_SET_SHOW_PRINT_CYCLE,			_T ("TM_SET_SHOW_PRINT_CYCLE		"), },
		{ TM_GET_IMAGE_BUFFER_SIZE,			_T ("TM_GET_IMAGE_BUFFER_SIZE		"), },
		{ TM_ABORT,							_T ("TM_ABORT						"), },
		{ TM_SET_PHOTOCELL_OFFSETS,			_T ("TM_SET_PHOTOCELL_OFFSETS		"), },
		{ TM_DO_DEBUG_PROCESS,				_T ("TM_DO_DEBUG_PROCESS			"), },
		{ TM_READ_CONFIG,					_T ("TM_READ_CONFIG					"), },
		{ TM_TOGGLE_IMAGE_DEBUG,			_T ("TM_TOGGLE_IMAGE_DEBUG			"), },
		{ TM_SETSERIALPENDING,				_T ("TM_SETSERIALPENDING			"), },
		{ TM_SUSPEND,						_T ("TM_SUSPEND                     "), },
		{ TM_IS_ONE_TO_ONE_PRINT_ENABLED,	_T ("TM_IS_ONE_TO_ONE_PRINT_ENABLED "), },
		{ TM_GET_TIME_DB,					_T ("TM_GET_TIME_DB                 "), },
		{ TM_SET_TIME_DB,					_T ("TM_SET_TIME_DB                 "), },
		{ TM_GET_DB,						_T ("TM_GET_DB                      "), },
		{ TM_HEAD_CONFIG_CHANGED,			_T ("TM_HEAD_CONFIG_CHANGED         "), },
		{ TM_GET_PHOTO_EVENT_HANDLE,		_T ("TM_GET_PHOTO_EVENT_HANDLE      "), },
		{ TM_GET_EOP_EVENT_HANDLE,			_T ("TM_GET_EOP_EVENT_HANDLE        "), },
		{ TM_GET_SERIAL_START,				_T ("TM_GET_SERIAL_START            "), },
		{ TM_SET_SERIAL_START,				_T ("TM_SET_SERIAL_START            "), },
		{ TM_NEXT_SET_DYNAMIC_INDEX,		_T ("TM_NEXT_SET_DYNAMIC_INDEX      "), },
		{ TM_GET_COUNT_UNTIL,				_T ("TM_GET_COUNT_UNTIL             "), },
		{ TM_SET_COUNT_UNTIL,				_T ("TM_SET_COUNT_UNTIL             "), },
		{ TM_SET_PRINT_DRIVER,				_T ("TM_SET_PRINT_DRIVER            "), },
		{ TM_GET_PRINT_DRIVER,				_T ("TM_GET_PRINT_DRIVER            "), },
		{ TM_GET_INK_LEVEL,					_T ("TM_GET_INK_LEVEL               "), },
		{ TM_INK_CODE_CHANGE,				_T ("TM_INK_CODE_CHANGE             "), },
		{ TM_CHECK_INK_WARNING_LEVEL,		_T ("TM_CHECK_INK_WARNING_LEVEL     "), },
		{ TM_DO_CHECK_INK_CODE,				_T ("TM_DO_CHECK_INK_CODE           "), },
		{ TM_HEAD_PRINT_ONCE,				_T ("TM_HEAD_PRINT_ONCE             "), },
		{ TM_DOWNLOAD_DYNDATA,				_T ("TM_DOWNLOAD_DYNDATA            "), },

	};

	for (int i = 0; i < ARRAYSIZE (map); i++)
		if (map [i].m_n == nMsg)
			return map [i].m_lpsz;

	CString str;

	str.Format (_T ("[Unknown] [%d] 0x%p"), nMsg, nMsg);

	return str;
}

CString CHead::TraceMessage (MSG * pMsg) 
{
	CString str;

/*
	if (pMsg) {
		CString strMsg = GetMessageString (pMsg->message);
		int nIndex = strMsg.Find (_T ("[Unknown]"));

		if (ISPOSTMESSAGESTRUCT (pMsg->message)) {
			int nDiagnostic = pMsg->message - WM_APP;
			CString strFile = _T (__FILE__);
			ULONG lLine = __LINE__;

			ASSERT (nIndex == -1);

			if (CHeadInfo::POSTMESSAGESTRUCT * p = (CHeadInfo::POSTMESSAGESTRUCT *)pMsg->lParam) {
				CString strPost = p->m_bPost ? _T ("POST") : _T ("SEND");

				strFile = p->m_strFile;
				lLine = p->m_lLine;
				str.Format (_T ("%s: %s: %s (0x%p, 0x%p)"), CTime::GetCurrentTime ().Format (_T ("%c")), strPost, strMsg, pMsg->wParam, pMsg->lParam);
			}
			else 
				str.Format (_T ("%s: %d [%d] (0x%p, 0x%p)"), CTime::GetCurrentTime ().Format (_T ("%c")), pMsg->message, pMsg->message, pMsg->wParam, pMsg->lParam);

			switch (pMsg->message) {
			case TM_GET_ERROR_STATE:
				break;
			default:
				#ifdef _DEBUG
				CDebug::Trace (str, true, strFile, lLine);
				#endif
				break;
			}
		}
	}
*/

	return str;
}

BOOL CHead::PreTranslateMessage (MSG * pMsg)
{
	bool bHandled = false;

	//{ CString str; str.Format (_T ("CHead::PreTranslateMessage: %s"), TraceMessage (pMsg)); MsgBox (NULL, str, GetFriendlyName (), 0); }

	TraceMessage (pMsg);

	if (pMsg) {
		switch (pMsg->message) {
		case TM_SET_DISPLAY_STRING:
			if (CString * pstr = (CString *)pMsg->wParam) {
				m_strDisplay = * pstr;
				delete pstr;
			}
			bHandled = TRUE;
			break;
		case TM_GET_MISSING_FONTS:
			#ifdef __WINPRINTER__
			if (CStringArray ** pvFonts = (CStringArray **)pMsg->wParam) {
				using namespace FoxjetCommon;

				CStringArray & vFonts = * pvFonts [0];
				CStringArray & vMissing = * pvFonts [1];

				if (m_pElementList) {
					for (int i = 0; i < m_pElementList->GetSize (); i++) {
						CBaseElement & e = m_pElementList->GetObject (i);

						if (CBaseTextElement * p = DYNAMIC_DOWNCAST (CBaseTextElement, &e)) {
							CString strFont = p->GetFontName ();

							if (Find (vFonts, strFont, false) == -1) {
								if (Find (vMissing, strFont, false) == -1) {
									vMissing.Add (strFont);
								}
							}
						}
					}
				}
			}
			#endif
			bHandled = TRUE;
			break;
		case TM_GET_ELEMENTS:
			if (CHead::GETELEMENTSTRUCT * p = (CHead::GETELEMENTSTRUCT *)pMsg->wParam) {
				if (p->m_nClassID == -1)
					GetElements (* p->m_pv);
				else
					GetElements (* p->m_pv, p->m_nClassID);
			}
			bHandled = TRUE;
			break;
		case TM_GET_PRODUCTAREA:
			if (CSize * pSize = (CSize *)pMsg->wParam)
				if (m_pElementList)
					* pSize = m_pElementList->GetProductArea ();
			bHandled = TRUE;
			break;
		case TM_GET_ERROR_STATE:
			* ((DWORD *)pMsg->wParam) = GetErrorState ();
			bHandled = TRUE;
			break;
		case TM_GET_LASTERROR:
			* ((DWORD *)pMsg->wParam) = GetLastError ();
			bHandled = TRUE;
			break;
		case TM_DELETE_ELEMENT:
			if (CHead::UPDATELEMENTSTRUCT * p = (CHead::UPDATELEMENTSTRUCT *)pMsg->wParam) 
				p->m_bResult = DeleteElement (p->m_lID);
			bHandled = TRUE;
			break;
		case TM_ADD_ELEMENT:
			if (CHead::UPDATELEMENTSTRUCT * p = (CHead::UPDATELEMENTSTRUCT *)pMsg->wParam) 
				p->m_lID = AddElement (p->m_strData);
			bHandled = TRUE;
			break;
		case TM_UPDATE_ELEMENT:
			if (CHead::UPDATELEMENTSTRUCT * p = (CHead::UPDATELEMENTSTRUCT *)pMsg->wParam) 
				p->m_bResult = UpdateElement (p->m_strData);
			bHandled = TRUE;
			break;
		case TM_DISPLAY_DEBUG_MSG:
			if (CString * p = (CString *)pMsg->wParam) {
				DisplayDebugMessage (* p);
				delete p;
			}
			bHandled = TRUE;
			break;
		case TM_UPDATE_STATUS_MSG:
			if (CString * p = (CString *)pMsg->wParam) {
				UpdateStatusMsg (* p);
				delete p;
			}
			bHandled = TRUE;
			break;
		case TM_GET_STATUS_MSG:
			if (CString * p = (CString *)pMsg->wParam) 
				* p = GetStatusMsg ();
			bHandled = TRUE;
			break;
		case TM_FREE_CACHE:
			FreeCache ();
			bHandled = TRUE;
			break;
		case TM_CACHE:
			ASSERT (m_pdb);
			if (CACHESTRUCT * p = (CACHESTRUCT *)pMsg->wParam) 
				Cache (* m_pdb, p->m_task, p->m_box);
			bHandled = TRUE;
			break;
		case TM_LOAD:
			ASSERT (m_pdb);
			if (CACHESTRUCT * p = (CACHESTRUCT *)pMsg->wParam) 
				p->m_bResult = Load (* m_pdb, p->m_task, p->m_box, p->m_mapHeads);
			bHandled = TRUE;
			break;
		case TM_IMAGE_FREE:
			if (m_pImageEng)
				m_pImageEng->Free ();
			bHandled = TRUE;
			break;
		case TM_GET_SERIALDATA:
			if (CString * p = (CString *)pMsg->wParam) 
				* p = GetSerialData ();
			bHandled = TRUE;
			break;
		case TM_GET_SERIALPENDING:
			* ((bool *)pMsg->wParam) = GetSerialPending ();
			bHandled = TRUE;
			break;
		case TM_GET_PREVIEW_BMP:
			if (PREVIEWSTRUCT * p = (PREVIEWSTRUCT *)pMsg->wParam) {
				if (m_pImageEng) { 
					if (m_pImageEng->m_pPreviewBitmap) {
						//p->m_pLock = &m_pImageEng->m_csPreview;
						CDC dcMem, dcPreview;
						FoxjetCommon::CTmpDC dc (NULL);
						DIBSECTION ds = { 0 };

						p->m_pBmp = new CBitmap ();
						m_pImageEng->m_pPreviewBitmap->GetObject (sizeof (ds), &ds);
						VERIFY (dcPreview.CreateCompatibleDC (dc));
						VERIFY (dcMem.CreateCompatibleDC (dc));
						VERIFY (p->m_pBmp->CreateCompatibleBitmap (dc, ds.dsBm.bmWidth, ds.dsBm.bmHeight));

						CBitmap * pPreview = dcPreview.SelectObject (m_pImageEng->m_pPreviewBitmap);
						CBitmap * pMem = dcMem.SelectObject (p->m_pBmp);
						
						dcMem.BitBlt (0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, &dcPreview, 0, 0, SRCCOPY);
						
						//SAVEBITMAP (* m_pImageEng->m_pPreviewBitmap, _T ("C:\\Foxjet\\Debug\\m_pPreviewBitmap.bmp"));
						//SAVEBITMAP (* p->m_pBmp, _T ("C:\\Foxjet\\Debug\\m_pBmp.bmp"));

						dcPreview.SelectObject (pPreview);
						dcMem.SelectObject (pMem);
					}
				}
			}
			bHandled = TRUE;
			break;
		case TM_SUSPEND:
			if (POSTMESSAGESTRUCT * p = (POSTMESSAGESTRUCT *)pMsg->lParam) {
				if (p->m_hEvent)
					::SetEvent (p->m_hEvent);
				delete p;
			}
			ASSERT (pMsg->wParam);
			if (SUSPENDSTRUCT * p = (SUSPENDSTRUCT *)pMsg->wParam) {
				TRACER (_T ("WaitForSingleObject: ") + p->m_strSuspend);

				DWORD dwWait = ::WaitForSingleObject (p->m_hSuspend, INFINITE);
				if (dwWait == WAIT_TIMEOUT)		{ TRACER (_T ("WAIT_TIMEOUT: ")	+ p->m_strSuspend);	TRACEF (_T ("WAIT_TIMEOUT"));	}
				if (dwWait == WAIT_ABANDONED)	{ TRACER (_T ("WAIT_ABANDONED: ")	+ p->m_strSuspend);	TRACEF (_T ("WAIT_ABANDONED"));	}
				if (dwWait == WAIT_OBJECT_0)	{ TRACER (_T ("WAIT_OBJECT_0: ")	+ p->m_strSuspend);	TRACEF (_T ("WAIT_OBJECT_0"));	}

				if (dwWait == WAIT_OBJECT_0) {
					TRACER (GetFriendlyName () + _T (": OnHeadConfigChanged\n"));
					OnHeadConfigChanged (0, 0);
					TRACER (_T ("SetEvent: ") + p->m_strLoaded + _T ("\n"));
					::SetEvent (p->m_hLoaded);
				}
			}
			//::Sleep (pMsg->wParam); //SuspendThread ();
			return TRUE;
		case TM_GET_TIME_DB:
			* ((CTime *)pMsg->wParam) = m_tmDatabase;
			bHandled = TRUE;
			break;
		case TM_SET_TIME_DB:
			m_tmDatabase = * ((CTime *)pMsg->wParam);
			bHandled = TRUE;
			break;
		case TM_GET_DB:
			{
				if (!m_pdb)
					m_pdb = new FoxjetDatabase::COdbcDatabase (_T (__FILE__), __LINE__);
				* ((FoxjetDatabase::COdbcDatabase **)pMsg->wParam) = m_pdb;
				bHandled = TRUE;
			}
			break;
		case TM_HEAD_CONFIG_CHANGED:
			OnHeadConfigChanged (pMsg->wParam, 0);
			bHandled = TRUE;
			break;
		}

		if (bHandled && ISPOSTMESSAGESTRUCT (pMsg->message)) {
			if (POSTMESSAGESTRUCT * p = (POSTMESSAGESTRUCT *)pMsg->lParam)
				if (p->m_hEvent)
					::SetEvent (p->m_hEvent);
		}
	}

	#ifdef _DEBUG
	if (pMsg->message == TM_HEAD_CONFIG_CHANGED) {
		ASSERT (bHandled);
		ASSERT (ISPOSTMESSAGESTRUCT (pMsg->message));
	}
	#endif

	if (ISPOSTMESSAGESTRUCT (pMsg->message))
		if (POSTMESSAGESTRUCT * p = (POSTMESSAGESTRUCT *)pMsg->lParam)
			delete p;

	//{ CString str; str.Format (_T ("CWinThread::PreTranslateMessage: %s (0x%p, 0x%p)"), TraceMessage (pMsg)); MsgBox (NULL, str, GetFriendlyName (), 0); }

	return CWinThread::PreTranslateMessage (pMsg);
}

bool CHeadInfo::IsUSB ()
{
	ULONG lAddr = _tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16);

	return (lAddr >= 0x001 && lAddr < 0x300);
}

CHead * CHead::GetHeadByUID (const CString & strUID) 
{
	Head::CHead * pResult = NULL;

	for (POSITION pos = m_params.m_listHeads.GetStartPosition(); pos; ) {
		ULONG lKey;
		Head::CHead *pHead;

		m_params.m_listHeads.GetNextAssoc ( pos, lKey, (void *&)pHead );
		CHeadInfo info = Head::CHead::GetInfo (pHead);

		if (!info.m_Config.m_HeadSettings.m_strUID.CompareNoCase (strUID))
			return pHead;
	}

	return NULL;
}
