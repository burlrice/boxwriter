
#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "PreviewDlg.h"
#include "Database.h"
#include "Extern.h"
#include "Registry.h"
#include "Color.h"
#include "Coord.h"
#include "Element.h"
#include "Extern.h"
#include "Task.h"
#include "Box.h"
#include "Utils.h"
#include "OdbcDatabase.h"
#include "OpenDlg.h"
#include "Head.h"
#include "ProdLine.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace ItiLibrary;
using namespace FoxjetDatabase;
using namespace Color;
using namespace Foxjet3d;
using namespace Foxjet3d::Box;
using namespace Foxjet3d::OpenDlg;
using namespace Foxjet3d::Panel;
using namespace Element;
using namespace PreviewDlg;
using namespace Head;

////////////////////////////////////////////////////////////////////////////////
static CString GetMessageData (CArray <MESSAGESTRUCT, MESSAGESTRUCT &> & v, ULONG lHeadID)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i].m_lHeadID == lHeadID)
			return v [i].m_strData;

	return _T ("");
}
////////////////////////////////////////////////////////////////////////////////

PreviewDlg::CTaskPreview::CTaskPreview (const FoxjetDatabase::TASKSTRUCT & task, CDC & dc, CProdLine * pLine)
:	m_task (task)
{
	COdbcDatabase & database = theApp.m_Database;
	const CVersion ver = GetElementListVersion ();
	CDC dcPanel;
	CTask t (task);
	BOXSTRUCT b;
	CBox box (database);
	const FoxjetDatabase::COrientation orient (HIWORD (task.m_lTransformation) + 1, (ORIENTATION)LOWORD (task.m_lTransformation));
	CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> vBounds;
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
	CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;
	CBoundary workingHead;

	VERIFY (dcPanel.CreateCompatibleDC (&dc));
	VERIFY (GetBoxRecord (database, task.m_lBoxID, b));
	VERIFY (GetPanelRecords (database, task.m_lLineID, vPanels));

	ASSERT (pLine != NULL);

	//VERIFY (GetLineHeads (database, task.m_lLineID, vHeads));
	{
		CPrintHeadList list;

		pLine->GetActiveHeads (list);

		for (POSITION pos = list.GetStartPosition(); pos; ) {
			ULONG lKey; 
			Head::CHead * pHead = NULL;

			list.GetNextAssoc (pos, lKey, (void *&)pHead);

			if (pHead) {
				CHeadInfo info = Head::CHead::GetInfo (pHead);
				vHeads.Add (info.m_Config.m_HeadSettings);
			}
		}
	}


	HeadConfigDlg::CPanel::GetPanels (database, task.m_lLineID, vBounds);

	box.SetBox (b);
	box.SetTransform (orient);
	box.GetWorkingHead (workingHead);

	for (int nPanel = 0; nPanel < vPanels.GetSize (); nPanel++) {
		PANELSTRUCT & panel = vPanels [nPanel];
		FoxjetDatabase::COrientation face = CBox::MapPanel (panel.m_nNumber, orient);
		const CSize size = GetSize (vHeads, box, face);
		const CRect rcPanel (CPoint (0, 0), size);

		for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
			HEADSTRUCT & head = vHeads [nHead];
			const CSize dot = CBaseElement::GetValveDotSize (head);

			if (head.m_lPanelID == panel.m_lID) {
				FoxjetDatabase::COrientation face = CBox::MapPanel (panel.m_nNumber, orient);
				CSize size = GetSize (vHeads, box, face);
				size.cy = head.Height ();
				const CRect rcPanel (CPoint (0, 0), size);
				CEditorElementList list;
				CString str = GetMessageData (t.m_vMsgs, head.m_lID);

				CBitmap * pbmp = new CBitmap ();
				VERIFY (pbmp->CreateCompatibleBitmap (&dc, size.cx, size.cy));
				
				ASSERT (!m_map.Lookup (head.m_lID, pbmp));
				m_map.SetAt (head.m_lID, pbmp);

				CBitmap * pPanelBmp = dcPanel.SelectObject (pbmp);
				::BitBlt (dcPanel, 0, 0, size.cx, size.cy, NULL, 0, 0, WHITENESS);

				list.FromString (str, CProductArea (), INCHES, head, CVersion ());
				list.SetHead (head.m_lID);

				for (int nElement = 0; nElement < list.GetSize (); nElement++) {
					CElement e (list.GetElement (nElement));
					CDC dcElement;
					CBitmap bmpElement;

					e.GetElement ()->Build ();

					CSize size = e.GetElement ()->Draw (dcPanel, head, true, FoxjetCommon::EDITOR);

					dcElement.CreateCompatibleDC (&dc);
					bmpElement.CreateCompatibleBitmap (&dc, size.cx, size.cy);
					CBitmap * pElement = dcElement.SelectObject (&bmpElement);

					CRect rc = e.GetWindowRect (head, head);
					CPoint pt = e.GetPos (head);

					if (IsValveHead (head))
						pt.y += (dot.cy / 2);

					e.Draw (dcElement, head, &list, dcPanel, pt.x, pt.y, rc.Width (), rc.Height (), rc.Width (), rc.Height (), 1.0);

					dcElement.SelectObject (pElement);
				}

				{ CString str; str.Format (_T ("c:\\Temp\\Debug\\%s.bmp"), head.m_strName); SAVEBITMAP (dcPanel, str); }

				dcPanel.SelectObject (pPanelBmp);
			}
		}
	}

	for (int nPanel = 0; nPanel < vBounds.GetSize (); nPanel++) {
		HeadConfigDlg::CPanel & panel = vBounds [nPanel];
		FoxjetDatabase::COrientation face = CBox::MapPanel (panel.m_panel.m_nNumber, orient);
		const CSize size = GetSize (vHeads, box, face);
		const CRect rcPanel (CPoint (0, 0), size);

		{
			int nHeads = 0;

			for (int nCard = 0; nCard < panel.m_vCards.GetSize (); nCard++) {
				HEADSTRUCT & card = panel.m_vCards [nCard];

				if (!IsValveHead (card)) {
					nHeads++;
					break;
				}

				nHeads += card.m_vValve.GetSize ();
			}

			nHeads += panel.m_vValve.GetSize ();

			if (!nHeads) {
				TRACEF ("no heads, skipping: " + panel.m_panel.m_strName);
				continue;
			}
		}

		VERIFY (m_bmp [nPanel].CreateCompatibleBitmap (&dc, size.cx, size.cy));
		CBitmap * pPanelBmp = dcPanel.SelectObject (&m_bmp [nPanel]);
		::BitBlt (dcPanel, 0, 0, size.cx, size.cy, NULL, 0, 0, WHITENESS);

		for (int nCard = 0; nCard < panel.m_vCards.GetSize (); nCard++) {
			HEADSTRUCT & card = panel.m_vCards [nCard];

			if (!IsValveHead (card)) 
				Blt (dcPanel, card);

			for (int nHead = 0; nHead < card.m_vValve.GetSize (); nHead++) 
				Blt (dcPanel, vHeads, card.m_vValve [nHead]);
		}

		for (int nHead = 0; nHead < panel.m_vValve.GetSize (); nHead++) 
			Blt (dcPanel, vHeads, panel.m_vValve [nHead]);

		//SAVEBITMAP (dcPanel, "C:\\Temp\\Debug\\" + panel.m_panel.m_strName + ".bmp");
		dcPanel.SelectObject (pPanelBmp);
	}
}

PreviewDlg::CTaskPreview::~CTaskPreview ()
{
	for (POSITION pos = m_map.GetStartPosition (); pos != NULL; ) {
		CBitmap * pBmp = NULL;
		ULONG lHeadID = 0;

		m_map.GetNextAssoc (pos, lHeadID, pBmp);

		if (pBmp)
			delete pBmp;
	}

	m_map.RemoveAll ();
}


CSize PreviewDlg::CTaskPreview::GetSize (CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vHeads, 
							 const Foxjet3d::Box::CBox & box, const FoxjetDatabase::COrientation & orient)
{
	CDbConnection conn (_T (__FILE__), __LINE__); 
	CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;

	if (!conn.IsOpen ()) 
		return CSize (0, 0);

	VERIFY (GetPanelRecords (theApp.m_Database, m_task.m_lLineID, vPanels));

	for (int i = 0; i < vHeads.GetSize (); i++) {
		HEADSTRUCT & head = vHeads [i];

		CSize size1000s = m_task.GetPanelSize (vPanels, head.m_lPanelID, box.GetBox ());
		CSize sizeLogical = CBaseElement::ThousandthsToLogical (size1000s, head);
	
		return sizeLogical;
	}

	return CSize (0, 0);
}


void PreviewDlg::CTaskPreview::Blt (CDC & dcPanel, 
						CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vHeads, 
						FoxjetDatabase::VALVESTRUCT & valve)
{
	int nHeadIndex = CTask::Find (vHeads, valve.m_lCardID);
	CBitmap * pbmp = NULL;
	CDC dcPrinter;
	int nXSrc = 0, nYSrc = 0;
	int nXDest = 0, nYDest = 0;
	int nWidth = 0, nHeight = 0;

	ASSERT (nHeadIndex != -1);

	HEADSTRUCT & head = vHeads [nHeadIndex];

	if (CBitmap * p = dcPanel.GetCurrentBitmap ()) {
		BITMAP bm;

		ZeroMemory (&bm, sizeof (bm));
		p->GetObject (sizeof (bm), &bm);

		nWidth	= bm.bmWidth;
		nHeight = bm.bmHeight;
	}

	nYDest = nHeight - CBaseElement::ThousandthsToLogical (CPoint (0, valve.m_lHeight), head).y;

	dcPrinter.CreateCompatibleDC (&dcPanel);
	VERIFY (m_map.Lookup (head.m_lID, pbmp));
	ASSERT (pbmp);

	for (int i = 0; i < head.m_vValve.GetSize (); i++) {
		VALVESTRUCT & tmp = head.m_vValve [i];

		nHeight = CBaseElement::ThousandthsToLogical (CPoint (0, GetValveHeight (tmp.m_type)), head).y;

		if (tmp.m_lID == valve.m_lID)
			break;

		nYSrc += nHeight;
	}

	CBitmap * pPrinter = dcPrinter.SelectObject (pbmp);

	dcPanel.BitBlt (nXDest, nYDest, nWidth, nHeight, &dcPrinter, nXSrc, nYSrc, SRCAND);
	//SAVEBITMAP (dcPanel, "C:\\Temp\\Debug\\" + valve.m_strName + ".bmp");

	dcPrinter.SelectObject (pPrinter);
}

void PreviewDlg::CTaskPreview::Blt (CDC & dcPanel, FoxjetDatabase::HEADSTRUCT & head)
{
	CBitmap * pbmp = NULL;
	CDC dcPrinter;
	int nXSrc = 0, nYSrc = 0;
	int nXDest = 0, nYDest = 0;
	int nWidth = 0, nHeight = 0;

	if (CBitmap * p = dcPanel.GetCurrentBitmap ()) {
		BITMAP bm;

		ZeroMemory (&bm, sizeof (bm));
		p->GetObject (sizeof (bm), &bm);

		nWidth	= bm.bmWidth;
		nHeight = bm.bmHeight;
	}

	nYDest = nHeight - CBaseElement::ThousandthsToLogical (CPoint (0, head.m_lRelativeHeight), head).y;
	nYDest -= head.Height ();

	dcPrinter.CreateCompatibleDC (&dcPanel);

	if (m_map.Lookup (head.m_lID, pbmp)) {
		ASSERT (pbmp);

		CBitmap * pPrinter = dcPrinter.SelectObject (pbmp);

		dcPanel.BitBlt (nXDest, nYDest, nWidth, nHeight, &dcPrinter, nXSrc, nYSrc, SRCAND);
		//SAVEBITMAP (dcPanel, "C:\\Temp\\Debug\\" + valve.m_strName + ".bmp");

		dcPrinter.SelectObject (pPrinter);
	}
}
