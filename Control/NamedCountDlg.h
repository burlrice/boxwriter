#if !defined(AFX_NAMEDCOUNTDLG_H__1A0F7333_33A0_4642_8EE0_0B0FDCA202F4__INCLUDED_)
#define AFX_NAMEDCOUNTDLG_H__1A0F7333_33A0_4642_8EE0_0B0FDCA202F4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NamedCountDlg.h : header file
//

#include "ListCtrlImp.h"
#include "List.h"
#include "Database.h"
#include "Resource.h"
#include "CountElement.h"
#include "RoundButtons.h"
#include "Utils.h"
#include "Head.h"

/////////////////////////////////////////////////////////////////////////////
// CNamedCountDlg dialog
namespace NamedCountDlg
{

	class CCountItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CCountItem (FoxjetCommon::CBaseElement * pElement, __int64 lCount);
		virtual ~CCountItem ();

		virtual CString GetDispText (int nColumn) const;
		void SetCount (__int64 lCount, __int64 lPalletMax = 0);
		void SetCount (__int64 lBoxCount, __int64 lPalletCount, __int64 lPalletMin, __int64 lPalletMax, __int64 lUnitsPerPallet);

		const FoxjetElements::CCountElement * GetElement () const { ASSERT (m_pCount); return m_pCount; }

	protected:
		FoxjetElements::CCountElement * m_pCount;
	};

	class CNamedCountDlg : public FoxjetCommon::CEliteDlg
	{

	// Construction
	public:
		CNamedCountDlg(CWnd* pParent = NULL);   // standard constructor

		Head::CElementPtrArray m_vCounts;
		__int64 m_lCount;
		__int64	m_lPalletMin;
		__int64 m_lPalletMax;
		__int64 m_lUnitsPerPallet;
		__int64	m_lPalletCount;
		__int64	m_lBoxCount;
		bool m_bIrregularPalletSize;

		Head::CHead::CHANGECOUNTSTRUCT m_count;
	// Dialog Data
		//{{AFX_DATA(CNamedCountDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA


	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CNamedCountDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:

		FoxjetUtils::CRoundButtons m_buttons;

		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
		__int64 CalcCount ();

		__int64 GetDlgItemInt64 (UINT nID);
		int Find (const CString & strName);

		virtual BOOL OnInitDialog();
		virtual void OnOK();

		afx_msg void OnChangeCount ();
		afx_msg void OnItemchangedElements(NMHDR* pNMHDR, LRESULT* pResult);

		// Generated message map functions
		//{{AFX_MSG(CNamedCountDlg)
			// NOTE: the ClassWizard will add member functions here
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()

		__int64	m_lBoxMin;

		bool	m_bPallet;
		__int64	m_lBoxesPerPallet;
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NAMEDCOUNTDLG_H__1A0F7333_33A0_4642_8EE0_0B0FDCA202F4__INCLUDED_)
