// KeyParse.h: interface for the CKeyParse class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_KEYPARSE_H__153CC6F7_C331_40CF_98FF_63520CD819B2__INCLUDED_)
#define AFX_KEYPARSE_H__153CC6F7_C331_40CF_98FF_63520CD819B2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxcoll.h>
#include <afxmt.h>


class CKeyParse  
{
public:
	static int Parse (CString &sKeyWords, CMapStringToString &mapKeywords);
	CKeyParse();
	virtual ~CKeyParse();
};

#endif // !defined(AFX_KEYPARSE_H__153CC6F7_C331_40CF_98FF_63520CD819B2__INCLUDED_)
