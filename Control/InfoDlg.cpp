// InfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "InfoDlg.h"
#include "AppVer.h"
#include "Version.h"
#include "ControlView.h"
#include "ximage.h"
#include "Color.h"
#include "Parse.h"
#include "Registry.h"
#include "NetworkDlg.h"
#include "ProdLine.h"

using namespace FoxjetCommon;
using namespace Color;
using namespace FoxjetUtils;
using namespace FoxjetDatabase;
using namespace Head;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CSize Fit (int cx1, int cy1, int cx2, int cy2)
{
	double dZoom = min (cx2 / (double)cx1, cy2 / (double)cy1);
	return CSize ((int)floor (cx1 * dZoom), (int)floor (cy1 * dZoom));
}

HANDLE LoadAndFit (UINT nIDBitmap, int cx, int cy)
{
	CxImage img;
	CBitmap bmp;
	DIBSECTION ds;

	memset (&ds, 0, sizeof (ds));
	VERIFY (bmp.LoadBitmap (nIDBitmap));
	::GetObject (bmp.m_hObject, sizeof (ds), &ds);
	CSize size = Fit (ds.dsBm.bmWidth, ds.dsBm.bmHeight, cx, cy);
	
	img.CreateFromHBITMAP (bmp);
	img.Resample (size.cx, size.cy);
	return img.MakeBitmap ();
}

/////////////////////////////////////////////////////////////////////////////
// CInfoDlg dialog

CInfoDlg::CInfoDlg(CControlView * pParent)
:	m_pControlView (pParent),
	FoxjetCommon::CEliteDlg (FoxjetDatabase::IsMatrix () ? IDD_INFO_MATRIX : IDD_INFO, pParent)
{
	CSize size (20, 20);

	//{{AFX_DATA_INIT(CInfoDlg)
	//}}AFX_DATA_INIT

	Create (FoxjetDatabase::IsMatrix () ? IDD_INFO_MATRIX : IDD_INFO, pParent);

	if (m_pControlView) {
		CxImage img;
		CSize size (0, 0);
		CRect rc (0, 0, size.cx, size.cy);
		
		if (CWnd * p = GetDlgItem (TXT_SPEED1)) {

			p->GetWindowRect (rc);
			size = CSize (rc.Height (), rc.Height ());
		}
		const int cx = rc.Height () - ::GetSystemMetrics (SM_CXEDGE);
		const int cy = rc.Height () - ::GetSystemMetrics (SM_CYEDGE);

		img.CreateFromHBITMAP (m_pControlView->m_bmpStrobeGreen);
		size = Fit (img.GetWidth (), img.GetHeight (), cx - 5, cy - 5);
		img.Resample (size.cx, size.cy);
		
		m_bmpPrinting.Attach (img.MakeBitmap ());

		img.GrayScale ();
		m_bmpNotPrinting.Attach (img.MakeBitmap ());

		img.CreateFromHBITMAP (m_pControlView->m_bmpStart);
		size = Fit (img.GetWidth (), img.GetHeight (), cx - 3, cy - 3);
		img.Resample (size.cx, size.cy);
		m_bmpStart.Attach (img.MakeBitmap ());

		img.CreateFromHBITMAP (m_pControlView->m_bmpIdle);
		size = Fit (img.GetWidth (), img.GetHeight (), cx - 3, cy - 3);
		img.Resample (size.cx, size.cy);
		m_bmpPause.Attach (img.MakeBitmap ());

		img.CreateFromHBITMAP (m_pControlView->m_bmpStop);
		size = Fit (img.GetWidth (), img.GetHeight (), cx - 3, cy - 3);
		img.Resample (size.cx, size.cy);
		m_bmpStop.Attach (img.MakeBitmap ());

		img.GrayScale ();
		m_bmpDisabled.Attach (img.MakeBitmap ());

		m_bmpInkGreen.Attach	(LoadAndFit (IDB_INK_GREEN,		cx - 1, cy - 1));
		m_bmpInkYellow.Attach	(LoadAndFit (IDB_INK_YELLOW,	cx - 1, cy - 1));
		m_bmpInkRed.Attach		(LoadAndFit (IDB_INK_RED,		cx - 1, cy - 1));
		m_bmpHVError.Attach		(LoadAndFit (IDB_HV_ERROR,		cx,		cy));
		m_bmpHVOK.Attach		(LoadAndFit (IDB_HV_OK,			cx,		cy));
		m_bmpTempError.Attach	(LoadAndFit (IDB_TEMP_ERROR,	cx,		cy));
		m_bmpTempOK.Attach		(LoadAndFit (IDB_TEMP_OK,		cx,		cy));

		img.Resample (cx, cy);
		m_bmpBlank.Attach (img.MakeBitmap ());
	}

	m_bmpNetwork.LoadBitmap (IDB_NETWORK_LG);
	m_bmpX.LoadBitmap (IDB_X);

	{
		UINT n [] = 
		{
			BMP_STATE1, BMP_STATUS_INK1, BMP_STATUS_HV1, BMP_STATUS_TEMP1, BMP_PRINT1, LBL_INKLEVEL1, 
			BMP_STATE2, BMP_STATUS_INK2, BMP_STATUS_HV2, BMP_STATUS_TEMP2, BMP_PRINT2, LBL_INKLEVEL2, 
			BMP_STATE3, BMP_STATUS_INK3, BMP_STATUS_HV3, BMP_STATUS_TEMP3, BMP_PRINT3, LBL_INKLEVEL3, 
			BMP_STATE4, BMP_STATUS_INK4, BMP_STATUS_HV4, BMP_STATUS_TEMP4, BMP_PRINT4, LBL_INKLEVEL4, 
			BMP_STATE5, BMP_STATUS_INK5, BMP_STATUS_HV5, BMP_STATUS_TEMP5, BMP_PRINT5, LBL_INKLEVEL5, 
			BMP_STATE6, BMP_STATUS_INK6, BMP_STATUS_HV6, BMP_STATUS_TEMP6, BMP_PRINT6, LBL_INKLEVEL6, 
			BMP_NETWORK,
		};

		for (int i = 0; i < ARRAYSIZE (n); i++) 
			if (CStatic * p = (CStatic *)GetDlgItem (n [i])) 
				p->ModifyStyle (SS_TYPEMASK, SS_OWNERDRAW);
	}

	m_strInkCodeKey = FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), _T ("/enum="));


	{
		UINT n [] = 
		{
			LBL_INKLEVEL1, 
			LBL_INKLEVEL2, 
			LBL_INKLEVEL3, 
			LBL_INKLEVEL4, 
			LBL_INKLEVEL5, 
			LBL_INKLEVEL6, 
		};

		if (m_strInkCodeKey.GetLength ()) {
			m_fntInkLevel.CreateFont (11, 0, 0, 0, FW_THIN, FALSE, 0, 0,
				DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
				DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("Microsoft Sans Serif"));

			for (int i = 0; i < ARRAYSIZE (n); i++) {
				if (CStatic * p = (CStatic *)GetDlgItem (n [i])) {
					CRect rc;

					p->GetWindowRect (rc);
					p->SetWindowPos (NULL, 0, 0, rc.Width () * 6, rc.Height (), SWP_NOZORDER | SWP_NOMOVE);
					p->GetWindowRect (rc);
					REPAINT (* p);
				}
			}
		}
		else {
			for (int i = 0; i < ARRAYSIZE (n); i++) 
				if (CStatic * p = (CStatic *)GetDlgItem (n [i])) 
					p->ShowWindow (SW_HIDE);
		}
	}

	{
		COLORREF rgbFont = FoxjetUtils::GetButtonColor (_T ("button font"));
		FoxjetUtils::CRoundButton2 * map [] = 
		{
			&m_btnClipboard,
			&m_btnClear,
		};
		tColorScheme tColor = { Color::rgbLightGray, rgbFont, rgbFont, rgbFont, rgbFont };

		for (int i = 0; i < ARRAYSIZE (map); i++) {
			map [i]->SetRoundButtonStyle(&m_pControlView->m_styleDefault);
			map [i]->SetTextColor (&tColor);
		};

		m_btnClipboard.LoadBitmap (IDB_CLIPBOARD);
		m_btnClear.LoadBitmap (IDB_CLEAR);
	}

	if (CSpinButtonCtrl * p = (CSpinButtonCtrl *)GetDlgItem (SPN_DATA3)) {
		CRect rcSpin (0, 0, 0, 0);
		CRect rcClipboard (0, 0, 0, 0);

		p->GetWindowRect (rcSpin);
		m_btnClipboard.GetWindowRect (rcClipboard);

		rcSpin.right = rcClipboard.left - 10;
		p->SetWindowPos (NULL, 0, 0, rcSpin.Width (), rcSpin.Height (), SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
	}

	OnSystemParamsChanged (0, 0);
}


void CInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInfoDlg)
	DDX_Text(pDX, LBL_VERSION, m_strVersion);

	DDX_Text(pDX, TXT_HEAD1, m_strHead1);
	DDX_Text(pDX, TXT_HEAD2, m_strHead2);
	DDX_Text(pDX, TXT_HEAD3, m_strHead3);
	DDX_Text(pDX, TXT_HEAD4, m_strHead4);
	DDX_Text(pDX, TXT_HEAD5, m_strHead5);
	DDX_Text(pDX, TXT_HEAD6, m_strHead6);

	DDX_Text(pDX, TXT_COUNT1, m_strCount1);
	DDX_Text(pDX, TXT_COUNT2, m_strCount2);
	DDX_Text(pDX, TXT_COUNT3, m_strCount3);
	DDX_Text(pDX, TXT_COUNT4, m_strCount4);
	DDX_Text(pDX, TXT_COUNT5, m_strCount5);
	DDX_Text(pDX, TXT_COUNT6, m_strCount6);
	
	DDX_Text(pDX, TXT_DATA1, m_strData1);
	DDX_Text(pDX, TXT_DATA2, m_strData2);
	
	DDX_Text(pDX, TXT_LINE1, m_strLine1);
	DDX_Text(pDX, TXT_LINE2, m_strLine2);
	DDX_Text(pDX, TXT_LINE3, m_strLine3);
	DDX_Text(pDX, TXT_LINE4, m_strLine4);
	DDX_Text(pDX, TXT_LINE5, m_strLine5);
	DDX_Text(pDX, TXT_LINE6, m_strLine6);
	
	DDX_Text(pDX, TXT_SPEED1, m_strSpeed1);
	DDX_Text(pDX, TXT_SPEED2, m_strSpeed2);
	DDX_Text(pDX, TXT_SPEED3, m_strSpeed3);
	DDX_Text(pDX, TXT_SPEED4, m_strSpeed4);
	DDX_Text(pDX, TXT_SPEED5, m_strSpeed5);
	DDX_Text(pDX, TXT_SPEED6, m_strSpeed6);
	//}}AFX_DATA_MAP

	DDX_Control (pDX, BTN_CLIPBOARD, m_btnClipboard);
	DDX_Control (pDX, BTN_CLEAR, m_btnClear);
}


BEGIN_MESSAGE_MAP(CInfoDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CInfoDlg)
	ON_WM_DRAWITEM()
	ON_NOTIFY(UDN_DELTAPOS, SPN_DATA3, OnDeltaposData3)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(BTN_CLIPBOARD, OnClipboard)
	ON_BN_CLICKED(BTN_CLEAR, OnClear)
	ON_REGISTERED_MESSAGE (WM_SYSTEMPARAMCHANGE, OnSystemParamsChanged)
END_MESSAGE_MAP()

void InsertAsc (CArray <Head::CHead *, Head::CHead *> & v, Head::CHead * p)
{
	CHeadInfo infoInsert = Head::CHead::GetInfo (p);
	ULONG lInsert = _tcstoul (infoInsert.GetUID (), NULL, 16);

	for (int i = 0; i < v.GetSize (); i++) {
		CHeadInfo info = Head::CHead::GetInfo (v [i]);
		ULONG lCur = _tcstoul (info.GetUID (), NULL, 16);
	
		if (lCur > lInsert) {
			v.InsertAt (i, p);
			return;
		}
	}

	v.Add (p);
}

/////////////////////////////////////////////////////////////////////////////
// CInfoDlg message handlers

BOOL CInfoDlg::UpdateData (BOOL bSaveAndValidate)
{
	if (!bSaveAndValidate) {
		m_strVersion = LoadString (IDS_VERSION) + _T (" ") + CVersion (verApp).Format ();

		if (m_pControlView) {
			CControlDoc * pDoc = m_pControlView->GetDocument ();
			CStringArray vLines;
			CArray <Head::CHead *, Head::CHead *> v;
			struct
			{
				CString * m_pstrLine;
				CString * m_pstrHead;
				CString * m_pstrCount;
				CString * m_pstrSpeed;
				UINT m_nIDState;
				UINT m_nIDStatusInk;
				UINT m_nIDStatusHV;
				UINT m_nIDStatusTemp;
				UINT m_nIDInkLevel;
				UINT m_nIDPrint;
			}
			map [] = 
			{
				{ &m_strLine1, &m_strHead1, &m_strCount1, &m_strSpeed1, BMP_STATE1, BMP_STATUS_INK1, BMP_STATUS_HV1, BMP_STATUS_TEMP1, LBL_INKLEVEL1, BMP_PRINT1 },
				{ &m_strLine2, &m_strHead2, &m_strCount2, &m_strSpeed2, BMP_STATE2, BMP_STATUS_INK2, BMP_STATUS_HV2, BMP_STATUS_TEMP2, LBL_INKLEVEL2, BMP_PRINT2 },
				{ &m_strLine3, &m_strHead3, &m_strCount3, &m_strSpeed3, BMP_STATE3, BMP_STATUS_INK3, BMP_STATUS_HV3, BMP_STATUS_TEMP3, LBL_INKLEVEL3, BMP_PRINT3 },
				{ &m_strLine4, &m_strHead4, &m_strCount4, &m_strSpeed4, BMP_STATE4, BMP_STATUS_INK4, BMP_STATUS_HV4, BMP_STATUS_TEMP4, LBL_INKLEVEL4, BMP_PRINT4 },
				{ &m_strLine5, &m_strHead5, &m_strCount5, &m_strSpeed5, BMP_STATE5, BMP_STATUS_INK5, BMP_STATUS_HV5, BMP_STATUS_TEMP5, LBL_INKLEVEL5, BMP_PRINT5 },
				{ &m_strLine6, &m_strHead6, &m_strCount6, &m_strSpeed6, BMP_STATE6, BMP_STATUS_INK6, BMP_STATUS_HV6, BMP_STATUS_TEMP6, LBL_INKLEVEL6, BMP_PRINT6 },
			};

			pDoc->GetProductLines (vLines);

			for (int i = 0; i < vLines.GetSize (); i++) {
				if (CProdLine * p = pDoc->GetProductionLine (vLines [i])) {
					CPrintHeadList l;

					switch (i) {
					case 0:	m_strData1 = FoxjetDatabase::Format (p->GetSerialData ()->Data, p->GetSerialData ()->m_Len);	break;
					case 1:	m_strData2 = FoxjetDatabase::Format (p->GetSerialData ()->Data, p->GetSerialData ()->m_Len);	break;
					}

					p->GetActiveHeads (l);
					
					for (POSITION pos = l.GetStartPosition (); pos != NULL; ) {
						DWORD dw = 0;
						Head::CHead * p = NULL;

						l.GetNextAssoc (pos, dw, (void *&)p);
						ASSERT (p);
						InsertAsc (v, p);
					}
				}
			}

			for (int i = v.GetSize (); i < ARRAYSIZE (map); i++) {
				if (CStatic * p = (CStatic *)GetDlgItem (map [i].m_nIDInkLevel)) 
					p->ShowWindow (SW_HIDE);
			}

			for (int i = 0; i < (int)min (ARRAYSIZE (map), v.GetSize ()); i++) {
				CHeadInfo info = Head::CHead::GetInfo (v [i]);
				DWORD dwError = 0;
				TASKSTATE state = info.GetTaskState ();
				INKCODE code;

				* map [i].m_pstrLine	= info.m_Config.m_Line.m_strName;
				* map [i].m_pstrHead	= info.GetFriendlyName ();
				* map [i].m_pstrCount	= info.GetCount ();
				map [i].m_pstrSpeed->Format (_T ("%.02f fpm"), info.GetLineSpeed ());

				SEND_THREAD_MESSAGE (info, TM_GET_ERROR_STATE, (WPARAM)&dwError);
				SEND_THREAD_MESSAGE (info, TM_GET_INK_LEVEL, (WPARAM)&code);

				if (CStatic * p = (CStatic *)GetDlgItem (map [i].m_nIDInkLevel)) {
					CString str;

					p->GetWindowText (str);
					int nLevel = (int)((double)code.m_lUsed / (double)code.m_lMax * 100.0);

					if (nLevel != _ttoi (str)) {
						p->SetWindowText (ToString (nLevel));
						REPAINT (p);
					}
				}

				if (dwError & HEADERROR_EP8)
					map [i].m_pstrSpeed->LoadString (IDS_EP8ERROR);

				switch (state) {
				case FoxjetDatabase::RUNNING:	m_vBitmap.SetAt (map [i].m_nIDState,		&m_bmpStart);		break;
				case FoxjetDatabase::IDLE:		m_vBitmap.SetAt (map [i].m_nIDState,		&m_bmpPause);		break;
				case FoxjetDatabase::STOPPED:	m_vBitmap.SetAt (map [i].m_nIDState,		&m_bmpStop);		break;	
				case FoxjetDatabase::DISABLED:	m_vBitmap.SetAt (map [i].m_nIDState,		&m_bmpDisabled);	break;
				}

				if (dwError & HEADERROR_OUTOFINK)
					m_vBitmap.SetAt (map [i].m_nIDStatusInk,	&m_bmpInkRed);
				else if (dwError & HEADERROR_LOWINK)
					m_vBitmap.SetAt (map [i].m_nIDStatusInk,	&m_bmpInkYellow);
				else
					m_vBitmap.SetAt (map [i].m_nIDStatusInk,	&m_bmpInkGreen);

				m_vBitmap.SetAt (map [i].m_nIDStatusHV,		(dwError & HEADERROR_HIGHVOLTAGE) ? &m_bmpHVError : &m_bmpHVOK);
				m_vBitmap.SetAt (map [i].m_nIDStatusTemp,	(dwError & HEADERROR_LOWTEMP) ? &m_bmpTempError : &m_bmpTempOK);
				m_vBitmap.SetAt (map [i].m_nIDPrint,		info.IsPrinting () ? &m_bmpPrinting : &m_bmpNotPrinting);

				{
					UINT n [] = 
					{
						map [i].m_nIDState,
						map [i].m_nIDStatusInk,
						map [i].m_nIDStatusHV,
						map [i].m_nIDStatusTemp,
						map [i].m_nIDPrint,
					};

					for (int i = 0; i < ARRAYSIZE (n); i++) { 
						if (CStatic * p = (CStatic *)GetDlgItem (n [i])) {
							DIBSECTION ds;

							memset (&ds, 0, sizeof (ds));
							::GetObject (m_bmpBlank.m_hObject, sizeof (ds), &ds);
							p->SetBitmap (m_bmpBlank);
							p->SetWindowPos (NULL, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
							REPAINT (p);
						}
					}
				}
			}
		}
	}

	return FoxjetCommon::CEliteDlg::UpdateData (bSaveAndValidate);
}

void CInfoDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpdis) 
{
	if (lpdis->CtlType == ODT_STATIC) {
		switch (nIDCtl) {
		case LBL_INKLEVEL1:
		case LBL_INKLEVEL2:
		case LBL_INKLEVEL3:
		case LBL_INKLEVEL4:
		case LBL_INKLEVEL5:
		case LBL_INKLEVEL6:
			{
				CRect rc = lpdis->rcItem;
				CDC dc, dcMem;
				CBitmap bmp;

				dc.Attach (lpdis->hDC);

				dc.FillSolidRect (rc, ::GetSysColor (COLOR_BTNFACE));

				if (m_strInkCodeKey.GetLength ()) 
					rc.right = rc.left + (rc.Width () / 6);

				dc.FrameRect (rc, &CBrush (Color::rgbBlack));
				rc.DeflateRect (1, 1);
				dc.FillSolidRect (rc, Color::rgbWhite);

				if (CStatic * p = (CStatic *)GetDlgItem (nIDCtl)) {
					CString str;

					p->GetWindowText (str);
					int nLevel = _ttoi (str);
					int n = Round ((double)rc.Height () * ((double)nLevel / 100.0));

					if (!n)
						str = _T ("0");

					//str = ToString (nLevel);

					//TRACEF (str + _T (": ") + ToString (n));

					if (n < rc.Height ()) {
						rc.top += n;
						dc.FillSolidRect (rc, Color::rgbBlue);
					}
					else {
						dc.FillSolidRect (rc, (nLevel < INK_CODE_THRESHOLD_WARNING) ? Color::rgbYellow : Color::rgbRed);
					}

					if (m_strInkCodeKey.GetLength ()) {
						CRect rc = lpdis->rcItem;
						
						rc.left += (rc.Width () / 6) + 2;

						CFont * pFont = dc.SelectObject (&m_fntInkLevel);
						//int nSetBkMode = dc.SetBkMode (TRANSPARENT);
						COLORREF rgb = dc.SetTextColor (Color::rgbBlack);
						dc.DrawText (str + _T (" %"), rc, 0);
						dc.SetTextColor (rgb);
						//dc.SetBkMode (nSetBkMode);
						dc.SelectObject (pFont);
					}
				}

				dc.Detach ();
			}
			return;
		}

		CBitmap * pbmpLookup = NULL;

		m_vBitmap.Lookup (lpdis->CtlID, pbmpLookup);

		if (lpdis->CtlID == BMP_NETWORK) 
			pbmpLookup = &m_bmpNetwork;

		if (pbmpLookup) {
			const CString strKey = CControlApp::GetRegKey () + _T ("\\Settings\\Colors");
			COLORREF rgbBack = FoxjetDatabase::GetProfileInt (strKey, _T ("background"), GetDefColor (_T ("background")));
			CRect rc = lpdis->rcItem;
			DIBSECTION ds;
			CDC dcMem;

			dcMem.CreateCompatibleDC (CDC::FromHandle (lpdis->hDC));
			memset (&ds, 0, sizeof (ds));
			::GetObject (pbmpLookup->m_hObject, sizeof (ds), &ds);

			CBitmap * pBmp = dcMem.SelectObject (pbmpLookup);
			COLORREF rgb = dcMem.GetPixel (0, 0);
			CSize size = CSize (ds.dsBm.bmWidth, ds.dsBm.bmHeight);
			::FillRect (lpdis->hDC, rc, CBrush (rgbBack));

			if (lpdis->CtlID == BMP_NETWORK) {
				::TransparentBlt (lpdis->hDC, rc.left, rc.top, rc.Width (), rc.Height (), dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, rgb);

				if (!theApp.IsNetworkConnected ()) {
					CBitmap * pBmp = dcMem.SelectObject (&m_bmpX);
					COLORREF rgb = dcMem.GetPixel (0, 0);
					::GetObject (m_bmpX.m_hObject, sizeof (ds), &ds);
					::TransparentBlt (lpdis->hDC, rc.left, rc.top, rc.Width () / 2, rc.Height () / 2, dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, rgb);
				}
			}
			else
				::TransparentBlt (lpdis->hDC, rc.left, rc.top, size.cx, size.cy, dcMem, 0, 0, size.cx, size.cy, rgb);

			dcMem.SelectObject (pBmp);
		}
	}
	
	FoxjetCommon::CEliteDlg::OnDrawItem(nIDCtl, lpdis);
}

void CInfoDlg::OnClipboard ()
{
	CString str;

	GetDlgItemText (TXT_DATA3, str);

	VERIFY (::OpenClipboard (NULL));
	::EmptyClipboard ();

	int nLen = (str.GetLength () * 2);
	HGLOBAL hClipboardText = ::GlobalAlloc (GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT, nLen);
	LPVOID lpCbTextData = ::GlobalLock (hClipboardText);
	memcpy (lpCbTextData, (LPCTSTR)str, nLen);

	VERIFY (::SetClipboardData (CF_UNICODETEXT, hClipboardText) != NULL);

	if (hClipboardText)
		::GlobalUnlock (hClipboardText);

	::CloseClipboard ();
}

void CInfoDlg::OnClear ()
{
	if (m_pControlView) {
		m_pControlView->m_vDiagnostic.RemoveAll ();
		SetDlgItemText (TXT_DATA3, _T (""));
		UpdateData (FALSE);
	}
}

void CInfoDlg::OnDeltaposData3(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pnmud = (NM_UPDOWN*)pNMHDR;
	
	//TRACEF (_T ("iPos: ") + ToString ((int)pnmud->iPos) + _T (", iDelta: ") + ToString ((int)pnmud->iDelta));

	if (CEdit * pEdit = (CEdit *)GetDlgItem (TXT_DATA3)) {
		int nLine = pEdit->GetFirstVisibleLine ();
		pEdit->LineScroll ((int)pnmud->iDelta);
		pEdit->Invalidate ();
		pEdit->RedrawWindow ();
	}

	if (pResult)
		* pResult = 0;
}

BOOL CInfoDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	/*
	#ifdef _DEBUG
	SetDlgItemText (TXT_DATA3, 
		_T ("1 - 2B030645EUJUN 15150600C123456<CR>\r\n")
		_T ("2 - 0112345610123456<CR>\r\n")
		_T ("3 - 1715060010C123456<CR>\r\n")
		_T ("4 - 2B030645EUJUN 15150600C123456<CR>\r\n")
		_T ("5 - 0112345610123456<CR>\r\n")
		_T ("6 - 1715060010C123456<CR>\r\n")
		_T ("7 - 2B030645EUJUN 15150600C123456<CR>\r\n")
		_T ("8 - 0112345610123456<CR>\r\n")
		_T ("9 - 1715060010C123456<CR>\r\n")
		_T ("10 - 2B030645EUJUN 15150600C123456<CR>\r\n")
		_T ("11- 0112345610123456<CR>\r\n")
		_T ("12 - 0112345610123456<CR>\r\n")
		_T ("13 - 1715060010C123456<CR>\r\n")
		_T ("14 - 2B030645EUJUN 15150600C123456<CR>\r\n")
		_T ("15 - 0112345610123456<CR>\r\n")
		_T ("16 - 1715060010C123456<CR>\r\n")
		_T ("17 - 2B030645EUJUN 15150600C123456<CR>\r\n")
		_T ("18 - 0112345610123456<CR>\r\n")
		_T ("19 - 1715060010C123456<CR>\r\n")
		);
	#endif
	*/
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

HBRUSH CInfoDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = FoxjetCommon::CEliteDlg::OnCtlColor(pDC, pWnd, nCtlColor);

	switch (nCtlColor) {
	case CTLCOLOR_EDIT:
		return hbr;
	case CTLCOLOR_STATIC:
		pDC->SetBkMode(TRANSPARENT);
		break;
	}

	if (m_pControlView)
		return m_pControlView->m_hbrDlg;

	return hbr;
}

LRESULT CInfoDlg::OnSystemParamsChanged (WPARAM wParam, LPARAM lParam)
{
	CDbConnection conn (_T (__FILE__), __LINE__); 

	if (!conn.IsOpen ()) 
		return 0;

	FoxjetUtils::CNetworkDlg dlg;

	dlg.Load (theApp.m_Database);

	if (CWnd * p = GetDlgItem (BMP_NETWORK))
		p->ShowWindow ((IsNetworked () && dlg.m_bEnabled) ? SW_SHOW : SW_HIDE);

	return 0;
}

void CInfoDlg::OnCancel ()
{
}