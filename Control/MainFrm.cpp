// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "Resource.h"
#include <io.h>
#include <WinUser.h>
#include <Portabledevice.h>
#include <PortableDeviceTypes.h>

#include "Database.h"
#include "Edit.h"
#include "DebugDlg.h"

#include "Defines.h"
#include "Control.h"
#include "MainFrm.h"

#include "CfgUsersDlg.h"
#include "GroupOptionsDlg.h"
#include "2LinePromptDlg.h"
#include "ScanReportDlg.h"
#include "PrintReportDlg.h"

#include "FileExt.h"
#include "Debug.h"
#include "HeadConfigDlg.h"
#include "HeadDlg.h"
#include "LineConfigDlg.h"
#include "Splash.h"
#include "WinMsg.h"
#include "List.h"
#include "FieldDefs.h"
#include "OdbcRecordset.h"
#include "Serialize.h"
#include "TemplExt.h"
#include "AppVer.h" 
#include "System.h"
#include "Extern.h"
#include "ScannerPropDlg.h"
#include "DevGuids.h"
#include "Parse.h"
#include "Registry.h"
#include "Sw0848Dlg.h"
#include "DbStartTaskDlg.h"
#include "ZoomDlg.h"
#include "Task.h"
#include "HeadDlg.h"
#include "StrobeDlg.h"
#include "BaxterConfigDlg.h"
#include "TimeChangeDlg.h"
#include "ImportFileDlg.h"
#include "ExportFileDlg.h"
#include "NetworkDlg.h"
#include "ProdLine.h"
#include "IpElements.h"
#include "ComPropertiesDlg.h"
#include "TemplExt.h"
#include "BackupDlg.h"
#include "NewDeviceDlg.h"

#ifdef __IPPRINTER__
	#include "DynamicTableDlg.h"
	#include "DynDataDlg.h"
	#include "SyncDlg.h"
	#include "RemoteHead.h"
#endif //__IPPRINTER__

#ifdef __WINPRINTER__
	#include "DatabaseElement.h"
	#include "DateTimeElement.h"
	#include "LocalHead.h"
#endif

#ifdef _DEBUG
#include "ContentTransfer.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ID_RESUME_COMM_THREADS	1000

using namespace FoxjetDatabase;
using namespace Head;

#if defined( __DEBUG_LOW_INK_CODE__ ) || defined( _DEBUG )
extern DWORD dwSimError;
#endif

/////////////////////////////////////////////////////////////////////////////

CDevice::CDevice ()
{
	memset (&m_dev, 0, sizeof (m_dev));
	memset (&m_interface, 0, sizeof (m_interface));
	memset (&m_interfaceDetail, 0, sizeof (m_interfaceDetail));
	memset (m_szDevicePath, 0, ARRAYSIZE (m_szDevicePath) * sizeof (m_szDevicePath [0]));
}

CDevice::CDevice (const SP_DEVINFO_DATA * pDev, const SP_DEVICE_INTERFACE_DATA * pInterface, const SP_DEVICE_INTERFACE_DETAIL_DATA_W * pInterfaceDetail)
{
	memcpy (&m_dev, pDev, sizeof (m_dev));
	memcpy (&m_interface, pInterface, sizeof (m_interface));
	memcpy (&m_interfaceDetail, pInterfaceDetail, sizeof (m_interfaceDetail));
	memset (m_szDevicePath, 0, ARRAYSIZE (m_szDevicePath) * sizeof (m_szDevicePath [0]));
	_tcsncpy (m_interfaceDetail.DevicePath, pInterfaceDetail->DevicePath, ARRAYSIZE (m_szDevicePath));
}

CDevice::CDevice (const CDevice & rhs)
{
	memcpy (&m_dev, &rhs.m_dev, sizeof (m_dev));
	memcpy (&m_interface, &rhs.m_interface, sizeof (m_interface));
	memcpy (&m_interfaceDetail, &rhs.m_interfaceDetail, sizeof (m_interfaceDetail));
	_tcsncpy (m_interfaceDetail.DevicePath, rhs.m_interfaceDetail.DevicePath, ARRAYSIZE (m_szDevicePath));
	m_strFriendlyName = rhs.m_strFriendlyName;
	m_strDeviceDescription = rhs.m_strDeviceDescription;
	m_strLocationInformation = rhs.m_strLocationInformation;
}

CDevice & CDevice::operator = (const CDevice & rhs)
{
	if (this != &rhs) {
		memcpy (&m_dev, &rhs.m_dev, sizeof (m_dev));
		memcpy (&m_interface, &rhs.m_interface, sizeof (m_interface));
		memcpy (&m_interfaceDetail, &rhs.m_interfaceDetail, sizeof (m_interfaceDetail));
		_tcsncpy (m_interfaceDetail.DevicePath, rhs.m_interfaceDetail.DevicePath, ARRAYSIZE (m_szDevicePath));
		m_strFriendlyName = rhs.m_strFriendlyName;
		m_strDeviceDescription = rhs.m_strDeviceDescription;
		m_strLocationInformation = rhs.m_strLocationInformation;
	}

	return * this;
}

CDevice::~CDevice ()
{
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

#ifdef _DEBUG
const CPoint CMainFrame::m_ptDebug (-1025, 465 - 15);
#endif

#pragma data_seg( "ping" )
	namespace Ping
	{
		extern CRITICAL_SECTION csPing;
		extern TCHAR szPing [MAX_PATH];
		extern FoxjetCommon::CStrobeItem errorPing;
		extern bool bTrigger;
		extern bool bEnabled;
	};
#pragma

const UINT WM_ENABLE_EDIT = ::RegisterWindowMessage (_T ("Foxjet::Control::WM_ENABLE_EDIT"));

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(IDM_CFG_PRINTHEAD, OnCfgPrinthead)
	ON_COMMAND(IDM_CFG_SYS_LINE, OnCfgProductionLines)
	ON_COMMAND(IDM_CFG_USER, OnCfgUser)
	ON_COMMAND(IDM_CFG_GROUP_OPTIONS, OnCfgGroupOptions)
	ON_COMMAND(IDM_VIEW_SCAN_REPORT, OnViewScanReport)
	ON_COMMAND(IDM_CHANGE_USER_ELEMENTS, OnChangeUserElements)
	ON_COMMAND(IDM_CFG_BARCODE_SETTINGS, OnCfgBarcodeSettings)
	ON_COMMAND(IDM_CFG_DATE_TIME_CODES, OnCfgDateCodes)
	ON_COMMAND(IDM_CFG_SHIFTCODES, OnCfgShiftCodes)
	ON_WM_TIMER()
	ON_COMMAND(IDM_VIEW_DIAG, OnViewDiag)
	ON_COMMAND(IDM_VIEW_PRINT_REPORT, OnViewPrintReport)
	ON_COMMAND(ID_OPERATE_CHANGECOUNTS, OnChangecounts)
	ON_COMMAND(ID_HELP_TRANSLATE, OnHelpTranslate)
	ON_COMMAND(IDM_CFG_DYNAMICDATA, OnCfgDynamicdata)
	ON_COMMAND(ID_VIEW_PREVIEW, OnViewPreview)
	ON_WM_CLOSE()
	ON_WM_TIMECHANGE()
	ON_COMMAND(ID_CONFIGURE_SYSTEM_STROBE, OnConfigureSystemStrobe)
	ON_WM_SIZE()
	ON_WM_MOVING()
	ON_WM_MOVE()
	ON_WM_NCHITTEST()
	ON_WM_SHOWWINDOW()
	ON_COMMAND(IDM_LOGOUT, OnLogout)
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP

	ON_COMMAND(ID_CONFIGURE_DATABASE, OnConfigureDatabase)
	ON_COMMAND(IDM_OPER_TEST, OnOperTest)

	ON_COMMAND(IDM_EXPORT, OnExport)
	ON_COMMAND(IDM_IMPORT, OnImport)

	ON_COMMAND(ID_CONFIGURE_SYSTEM_OUTPUTTABLE, OnConfigureSystemOutputtable)

	ON_COMMAND(IDM_CFG_GENERAL, OnCfgGeneral)

	ON_COMMAND(ID_VIEW_ZOOM, OnZoom)

	ON_COMMAND(IDM_OPER_START,	OnOperStart)
	ON_COMMAND(IDM_OPER_STOP,	OnOperStop)
	ON_COMMAND(IDM_OPER_IDLE,	OnOperIdle)
	ON_COMMAND(IDM_OPER_RESUME, OnOperResume)
	
	ON_REGISTERED_MESSAGE (WM_ISPRINTERRUNNING, OnIsAppRunning)
	ON_REGISTERED_MESSAGE (WM_SHUTDOWNAPP, OnShutdown)
	ON_REGISTERED_MESSAGE (WM_SYSTEMPARAMCHANGE, OnSystemParamsChanged)
	ON_REGISTERED_MESSAGE (FoxjetUtils::WM_RESETSCANNERINFO, OnResetScannerInfo)
	ON_REGISTERED_MESSAGE (FoxjetUtils::WM_RESETSCANNERERROR, OnResetScannerError)
	ON_REGISTERED_MESSAGE (WM_GETPRINTERWND, OnGetPrinterWnd)
	ON_REGISTERED_MESSAGE (WM_OPENMAPFILE, OnOpenMapFile)
	ON_REGISTERED_MESSAGE (WM_CLOSEMAPFILE, OnCloseMapFile)
	ON_REGISTERED_MESSAGE (WM_SENDMAPPEDCMD, OnMappedCommand)
	ON_REGISTERED_MESSAGE (WM_DBSTARTCONFIGCHANGED, OnDbStartParamsChanged)
	ON_REGISTERED_MESSAGE (WM_DISPLAY_MSG, OnDisplayMsg)
	ON_REGISTERED_MESSAGE (WM_ENABLE_EDIT, OnEnableEdit)
	ON_REGISTERED_MESSAGE (WM_LOCAL_DB_BACKUP, OnLocalDbBackup)
	ON_REGISTERED_MESSAGE (WM_DATABASECONNECTIONLOST, OnDatabaseConnectionLost)
	ON_REGISTERED_MESSAGE (WM_DEBUG, OnDebug)
	ON_REGISTERED_MESSAGE (WM_KEYBOARDCHANGE, OnKbChange)
	ON_REGISTERED_MESSAGE (WM_SUSPEND_COMM_THREADS, OnSuspendCommThreads)
	ON_REGISTERED_MESSAGE (WM_DEBUG_ENUM_REGISTERED_THREADS, OnDebugEnumRegisteredThreads)

#ifdef __IPPRINTER__
	ON_REGISTERED_MESSAGE (WM_SYNCREMOTEHEAD, OnSyncRemoteHead)
#endif //__IPPRINTER__

	ON_COMMAND(ID_UPDATE_UI, OnUpdateUI)

	ON_COMMAND(ID_CONFIGURE_SERIALDATAFORMAT, OnCfgBaxter)
	ON_WM_TIMER()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_STATUS_PANE,
	ID_INDICATOR_LOGIN,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
:	m_pControlView (NULL),
	m_hMapFile (NULL),
	m_lpMapAddress (NULL),
	m_hCacheThread (NULL),
	m_bClosed (false)
{
	m_vUSB = EnumDevices ();
	m_vPortableDevices = EnumDevices (GUID_DEVINTERFACE_WPD);

	/*
	#ifdef _DEBUG
	const CString strDevicePath = _T ("\\\\?\\usb#vid_8087&pid_0928&mi_00#6&1a979b79&0&0000#{6ac27878-a6fa-4155-ba85-f98f491d4f33}");
	CString strFrom = _T ("C:\\Users\\Burl\\AppData\\Local\\Temp\\backup00000000.zip");
	CString strTo = _T ("s10001/Download/backup00000000.zip");

	CopyToPortableDevice (strDevicePath, strFrom, strTo);
	#endif
	*/
}

CMainFrame::~CMainFrame()
{
	OnCloseMapFile (0, 0);
}

int CMainFrame::GetToolbarResID ()
{
	UINT nToolbar = FoxjetDatabase::IsMatrix () ? IDR_MAINFRAME_MATRIX : IDR_MAINFRAME;

	/*
	#ifdef _DEBUG
	nToolbar = IDR_MAINFRAME_DEBUG;
	#endif
	*/

	if (theApp.m_dbstart.m_strDSN.GetLength () == 0)
		nToolbar = IDR_MAINFRAME_NET;

	#ifdef __IPPRINTER__
	nToolbar = IsMatrix () ? IDR_MAINFRAME_NET_MATRIX : IDR_MAINFRAME_NET;
	#endif

	return nToolbar;
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CSplashWnd::ShowSplashScreen (this);

	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	#ifdef __IPPRINTER__
	UINT nToolbar = GetToolbarResID ();

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(nToolbar))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
	#endif

	SetTitle (GlobalFuncs::GetAppName ());

	//{
	//	CString str = ::GetCommandLine ();

	//	str.MakeLower ();

	//	if (str.Find (_T ("/minimize")) != -1)	
	//		SetWindowLong(this->m_hWnd, GWL_STYLE, GetWindowLong(this->m_hWnd, GWL_STYLE) | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);
	//}

	EnableScrollBarCtrl(SB_BOTH, FALSE);

	return 0;
}

int CMainFrame::Find (ULONG lID, CArray <FoxjetDatabase::OPTIONSSTRUCT, FoxjetDatabase::OPTIONSSTRUCT &> & v)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i].m_lID == lID)
			return i;

	return -1;
}

bool CMainFrame::IsMenuItemSecured (ULONG lID)
{
	static const ULONG lExclude [] = 
	{
		IDM_OPER_RESUME,	// 32795, // Resume
		IDM_OPER_IDLE,		// 32794, // Idle	
		IDM_VIEW_DIAG,		// 32810, // Diagnostic Dialog	
		IDM_LOGIN,			// 32779, // Login	
		IDM_LOGOUT,			// 32792, // Logout	
		ID_APP_ABOUT,		// 57664, // About	
		//IDM_OPER_TEST,	// sw0866

		// these should not be excluded
		//32806, // Shift Codes	
		//32816, // Month/hour codes	
		//32801, // Change User Elements	
		//32814, // Change Counts	
		//32815, // Translate...	
	};

	for (int i = 0; i < ARRAYSIZE (lExclude); i++)
		if (lExclude [i] == lID)
			return false;

	return true;
}

bool CMainFrame::IsMenuItemExcluded (ULONG lID)
{
/*
	static const ULONG lExclude [] = 
	{
		IDM_CHANGE_USER_ELEMENTS,
		ID_OPERATE_CHANGECOUNTS,
	};

	for (int i = 0; i < ARRAYSIZE (lExclude); i++)
		if (lExclude [i] == lID)
			return true;
*/
	return false;
}

CString CMainFrame::GetMenuPrompt (const FoxjetDatabase::OPTIONSSTRUCT & s, const CString & strDef)
{
	CString str (strDef), strPrompt = LoadString (s.m_lID);
	int nIndex;

	if (strPrompt.GetLength ())
		str = strPrompt;

	if ((nIndex = str.Find ('\t')) != -1)
		str = str.Mid (0, nIndex);

	if ((nIndex = str.Find ('\n')) != -1)
		str = str.Mid (0, nIndex);

	str.Remove ('&');

	return str;
}

ULONG CMainFrame::GetAdminID ()
{
	CDbConnection conn (_T (__FILE__), __LINE__); 

	if (!conn.OpenExclusive (this))
		return -1;

	SECURITYGROUPSTRUCT s;

	s.m_lID = -1;

	try {
		COdbcRecordset rst (&theApp.m_Database);
		CString strSQL;

		strSQL.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s';"), 
			SecurityGroups::m_lpszTable,
			SecurityGroups::m_lpszName,
			_T ("Administrator"));
		rst.Open (strSQL, CRecordset::snapshot);
		
		if (!rst.IsEOF ())
			rst >> s;

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return s.m_lID;
}

bool CMainFrame::GetOptionLink (ULONG lGroupID, ULONG lOptionID, FoxjetDatabase::OPTIONSLINKSTRUCT & link)
{
	bool bResult = false;
	CDbConnection conn (_T (__FILE__), __LINE__); 

	if (!conn.OpenExclusive (this))
		return false;

	try {
		COdbcRecordset rst (&theApp.m_Database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%d AND [%s]=%d;"), 
			OptionsToGroup::m_lpszTable,
			OptionsToGroup::m_lpszGroupID,
			lGroupID,
			OptionsToGroup::m_lpszOptionsID,
			lOptionID);
		rst.Open (str);
		
		if (!rst.IsEOF ()) {
			rst >> link;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

void CMainFrame::UpdateOptionsTable (CMenu * pMenu, CArray <FoxjetDatabase::OPTIONSSTRUCT, FoxjetDatabase::OPTIONSSTRUCT &> & v)
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

	if (pMenu) {
		for (UINT i = 0; i < pMenu->GetMenuItemCount (); i++) {
			ULONG lID = pMenu->GetMenuItemID (i);

			if (lID == -1) 
				UpdateOptionsTable (pMenu->GetSubMenu (i), v);
			else if (lID != 0) {
				if (!IsMenuItemExcluded (lID)) {
					if (Find (lID, v) == -1 && IsMenuItemSecured (lID)) {
						OPTIONSSTRUCT option;
						OPTIONSLINKSTRUCT link;
						CString str, strID, strPrompt = LoadString (lID);
				
						option.m_lID = lID;

						pMenu->GetMenuString (i, str, MF_BYPOSITION);
						str = GetMenuPrompt (option, str);
						strID.Format (_T ("%d"), lID);
						option.m_strOptionDesc = str;

						try {
							COdbcRecordset rstOptions (&theApp.m_Database);
							CString strSQL;
							ULONG lAdminID = GetAdminID ();

							strSQL.Format (_T ("SELECT * FROM [%s];"), Options::m_lpszTable);
							rstOptions.Open (strSQL, CRecordset::dynaset);
							
							TRACEF (strID + ": " + str);

							rstOptions.AddNew ();
							rstOptions << option;
							rstOptions.Update ();

							rstOptions.Close ();

							v.Add (option);

							if (!GetOptionLink (lAdminID, option.m_lID, link)) {
								COdbcRecordset rst (&theApp.m_Database);

								link.m_lOptionsID	= lID;
								link.m_lGroupID		= lAdminID;
								strSQL.Format (_T ("SELECT * FROM [%s];"), OptionsToGroup::m_lpszTable);
								rst.Open (strSQL, CRecordset::dynaset);

								rst.AddNew ();
								rst << link;
								rst.Update ();

								rst.Close ();
							}
						}
						catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
						catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
					}
				}
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnLogin() 
{
	DBCONNECTION (conn);

	C2LinePromptDlg Dialog;
	Dialog.m_PromptLine1.LoadString ( IDS_USER_NAME );
	Dialog.m_PromptLine2.LoadString ( IDS_PASSWORD );
	Dialog.m_bLine2PasswordMode = true;

	while (Dialog.DoModal() == IDOK) 
		if (theApp.Login (Dialog.m_sLine1, Dialog.m_sLine2))
			break;

	UpdateAppExitUI ();
}

static CString GetClassName (HWND hWnd)
{
	TCHAR szClass [128] = { 0 };

	::GetClassName (hWnd, szClass, ARRAYSIZE (szClass) - 1);
	
	return szClass;
}

void CMainFrame::BringEditorToTop () 
{
	if (HWND hwnd = FoxjetDatabase::FindEditor ()) {
		#ifdef DEBUG_FINDEDITOR
		{ CString str; str.Format (_T ("%s(%d): SetForegroundWindow\n"), _T (__FILE__), __LINE__); TRACER (str); }
		#endif

		if (!::SetForegroundWindow (hwnd)) {
			DWORD dwResult;

			#ifdef DEBUG_FINDEDITOR
			{ CString str; str.Format (_T ("%s(%d): SetForegroundWindow failed, sending WM_ISEDITORRUNNING\n"), _T (__FILE__), __LINE__); TRACER (str); }
			#endif

			LRESULT lResult = ::SendMessageTimeout (hwnd, WM_ISEDITORRUNNING, 0, 0, SMTO_NORMAL, 1, &dwResult);
		}
	}
}

typedef struct
{
	HWND m_hWnd;
	CString m_strCmdLine;
} LAUNCHSTRUCT;

ULONG CALLBACK CMainFrame::LaunchEditorFunc (LPVOID lpData)
{ 
	PROCESS_INFORMATION pi;
	STARTUPINFO si;
	CString strCmd;
	HWND hWnd;

	{
		ASSERT (lpData);
		LAUNCHSTRUCT * p = (LAUNCHSTRUCT *)lpData;
		strCmd = p->m_strCmdLine;
		hWnd = p->m_hWnd;
		delete p;
	}

	if (HWND hwnd = FoxjetDatabase::FindEditor ()) {
		BringEditorToTop ();
		::PostMessage (hWnd, WM_ENABLE_EDIT, 1, 0);
		return 1;
	}

	/*
	{
		CString strLaunch = FoxjetDatabase::GetHomeDir () + _T ("\\Launch.exe");

		if (::GetFileAttributes (strLaunch) != -1) 
			strCmd = _T ("\"") + strLaunch + _T ("\" ") + strCmd + _T ("");
	}
	*/

	//TRACEF (strCmd);

/*
strCmd = 
	_T ("\"C:\\Program Files (x86)\\Foxjet\\MarksmanELITE\\Launch.exe\" ")
	_T ("\"C:\\Program Files (x86)\\Foxjet\\MarksmanELITE\\MkDraw.exe\" ")
	_T ("\"C:\\FoxJet Database\\LINE0001\\768 Sample.tsk\" ")
	_T ("/demo ")
	_T ("/no_keyboard ")
	_T ("/lowres ")
	_T ("/requireAdministrator ")
	_T ("/nosplash ")
	_T ("/dsn=MarksmanELITE ")
	_T ("/IMAGE_ON_PHOTOCELL ")
	_T ("/DBTIMEOUT=60 ")
	_T ("/user=\"ROOT\" ")
	_T ("/PrinterID=1246576929")
	;
*/

	TCHAR * psz = NULL;

	{
		int nLen = strCmd.GetLength () + 1;
		psz = new TCHAR [nLen];

		ZeroMemory (psz, sizeof (TCHAR) * nLen);
		_tcscpy (psz, strCmd);
	}

	TRACER (CString (psz) + _T ("\n"));

	memset (&pi, 0, sizeof(pi));
	memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	// "C:\FoxJet Database\LINE0001\export.tsk" /capture_image /highres /debug /printcycle /mphc /dsn=MarksmanElite /pass=user /nosplash /IMAGE_ON_PHOTOCELL /user="ROOT"
	// 
	bool bCreateProcess = ::CreateProcess (NULL, psz, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi) ? true : false;

	delete [] psz;

	if (!bCreateProcess) {
		MsgBox (FORMATMESSAGE (::GetLastError ()), MB_OK | MB_ICONINFORMATION );
	}
	else {
		bool bActive = false;

		for (int nTries = 0; !bActive && nTries < 5; nTries++) {
			if (HWND hwnd = FoxjetDatabase::FindEditor ()) {
				DWORD dwResult = 0;

				#ifdef DEBUG_FINDEDITOR
				TCHAR sz [512] = { 0 };
				::GetWindowText (hwnd, sz, ARRAYSIZE (sz));
				{ CString str; str.Format (_T ("%s(%d): WM_ISEDITORINITIALIZED: 0x%p %s, %s\n"), _T (__FILE__), __LINE__, hwnd, sz, GetClassName (hwnd)); TRACER (str); }
				#endif

				LRESULT lResult = ::SendMessageTimeout (hwnd, WM_ISEDITORINITIALIZED, 0, 0, SMTO_BLOCK | SMTO_ABORTIFHUNG, 500, &dwResult);

				bActive = dwResult ? true : false;

				#ifdef DEBUG_FINDEDITOR
				{ CString str; str.Format (_T ("%s(%d): bActive: %d\n"), _T (__FILE__), __LINE__, dwResult); TRACER (str); }
				#endif

				if (!bActive) {
					DWORD dwSleep = 1000;

					#ifdef DEBUG_FINDEDITOR
					{ CString str; str.Format (_T ("%s(%d): Sleep (%d)\n"), _T (__FILE__), __LINE__, dwSleep); TRACER (str); }
					#endif

					::Sleep (dwSleep);
				}
				else {
					BringEditorToTop ();
					::PostMessage (hWnd, WM_ENABLE_EDIT, 1, 0);
					return 1;
				}
			}
		}

		#ifdef DEBUG_FINDEDITOR
		{ CString str; str.Format (_T ("%s(%d): ****** failed ******\n"), _T (__FILE__), __LINE__); TRACER (str); }
		#endif
	}

	::PostMessage (hWnd, WM_ENABLE_EDIT, 1, 0);
	return 0;
}

LRESULT CMainFrame::OnEnableEdit (WPARAM wParam, LPARAM lParam)
{
	bool bEnable = wParam ? true : false;

	if (!bEnable) 
		BeginWaitCursor ();
	else
		EndWaitCursor ();

	m_pControlView->m_btnEdit.EnableWindow (bEnable);

	return 0;
}

void CMainFrame::OnOperEdit() 
{
	if (m_pControlView->m_btnEdit.IsWindowEnabled ()) {
		DBCONNECTION (conn);

		//CString sExeFile = _T ("\"") + FoxjetDatabase::GetHomeDir () + (IsGojo () ? _T ("\\Legacy") : _T ("")) + _T("\\MkDraw.exe") + _T ("\"");
		CString sExeFile = _T ("\"") + FoxjetDatabase::GetHomeDir () + _T("\\MkDraw.exe") + _T ("\"");
		CString strCmd;
		DWORD dw = 0;
		LAUNCHSTRUCT * p = new LAUNCHSTRUCT;

		if (CControlDoc * pDoc = (CControlDoc *)GetActiveDocument ()) {
			if (CProdLine * pLine = pDoc->GetSelectedLine()) {
				CString strTask = pLine->GetCurrentTask ();

				if (strTask.GetLength ()) 
					strCmd += _T (" \"") +
						FoxjetFile::GetRootPath () +
						pLine->GetName () + _T ("\\") + 
						strTask + _T (".") +
						FoxjetFile::GetFileExt (FoxjetFile::TASK) +
						_T ("\" ");
			}
		}

		strCmd += theApp.GetCommandLine ();
		strCmd += _T (" /control");
		strCmd = _T ("") + sExeFile + _T (" ") + strCmd;

		if (theApp.IsLocalDB ())
			strCmd += _T (" /localdb");

		strCmd.Replace (_T ("/nosplash"), _T (""));

		OnEnableEdit  (0, 0);
		p->m_hWnd = m_hWnd;
		p->m_strCmdLine = strCmd;

		::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0, (LPTHREAD_START_ROUTINE)LaunchEditorFunc, (LPVOID)p, 0, &dw);
	}
}

/* TODO: rem
void CMainFrame::OnUpdateOperEdit(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable( theApp.LoginPermits ( pCmdUI->m_nID ) );
}

void CMainFrame::OnOperStart() 
{
	DBCONNECTION (conn);

	CControlView *pView = m_pControlView;
	CControlDoc *pDoc = pView->GetDocument();
	CString sLine = pDoc->GetSelectedLine()->GetName();

	int nStart = pView->StartTask (sLine);

	if (TASK_START_SUCCEEDED (nStart)) {
		pView->PostMessage (WM_COMMAND, MAKEWPARAM (ID_VIEW_REFRESHPREVIEW, 0));
		::Sleep (1);
	}
}

void CMainFrame::OnOperDbStart() 
{
	DBCONNECTION (conn);

#ifdef __WINPRINTER__
	CControlView *pView = m_pControlView;
	CControlDoc *pDoc = pView->GetDocument();

	CDbStartTaskDlg dlg (this);

	if (dlg.DoModal () == IDOK) {
		CString strLine = pDoc->GetSelectedLine ()->GetName ();
		CString strTask = dlg.m_strTaskValue;

		TRACEF (dlg.m_strKeyField + _T (" --> ") + dlg.m_strKeyValue);
		TRACEF (dlg.m_strTaskField + _T (" --> ") + dlg.m_strTaskValue);

		{
			CString strKey = _T ("Software\\FoxJet\\Control\\") + strLine + _T ("\\DbTaskStart");

			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("DSN"),			dlg.m_strDSN); 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("KeyField"),		dlg.m_strKeyField); 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("Table"),		dlg.m_strTable); 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("TaskField"),	dlg.m_strTaskField); 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("KeyValue"),		dlg.m_strKeyValue); 
			FoxjetDatabase::WriteProfileInt	   (HKEY_CURRENT_USER, strKey, _T ("SQLType"),		dlg.m_nSQLType); 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("TaskValue"),	dlg.m_strTaskValue); 
		}

		int nStart = pView->StartTask (strLine, strTask, false, dlg.m_strKeyValue, 0, dlg.m_nOffset);
		
		if (TASK_START_SUCCEEDED (nStart)) {
			CString str = dlg.m_strTaskValue + _T (" [") + dlg.m_strKeyValue + _T ("]");
			int nWidth = max (100, pView->m_HeadCtrl.GetStringWidth (str));

			if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
				using namespace Foxjet3d;

				SETTINGSSTRUCT s;

				GetSettingsRecord (theApp.m_Database, pLine->m_LineRec.m_lID, FoxjetDatabase::Globals::m_lpszResetCounts, s);
				bool bResetCounts = _ttoi (s.m_strData) ? true : false;

				if (bResetCounts) 
					pLine->SetCounts  (0);
			}

			pView->m_HeadCtrl.SetColumnWidth (2, nWidth);
		}
		else {
			CString str;

			str.Format (
				_T ("Failed to start task: %s\n")
				_T ("Key field: %s\n")
				_T ("Key value: %s\n"), 
				strTask, 
				dlg.m_strKeyField, 
				dlg.m_strKeyValue);

			pView->StopTask (strLine, false);
			MsgBox (str, NULL, MB_OK | MB_ICONHAND);
		}
	}
#endif //__WINPRINTER__
}

void CMainFrame::OnUpdateOperStart(CCmdUI* pCmdUI) 
{
	bool bEnable = false;

#ifdef _DEBUG
	if (pCmdUI->m_nID == IDM_OPER_TEST) {
		pCmdUI->Enable (true);
		return;
	}
#endif

#ifdef __IPPRINTER__
	if (pCmdUI->m_nID == IDM_OPER_DBSTART || pCmdUI->m_nID == ID_CONFIGURE_DATABASE) {
		pCmdUI->Enable (false);
		return;
	}
#endif //__IPPRINTER__
	
	if (pCmdUI->m_nID == IDM_OPER_DBSTART) {
		bool bConfigured = theApp.m_dbstart.m_strDSN.GetLength () > 0 ? true : false;

		if (!bConfigured) {
			pCmdUI->Enable (false);
			return;
		}
	}

	if (CControlView * pView = m_pControlView) 
		if ((pView->m_HeadCtrl.GetItemCount () > 0) && theApp.LoginPermits (pCmdUI->m_nID)) // sw0866
			bEnable = true;

	pCmdUI->Enable (bEnable);
}
*/

////////////////////////////////////////////////////////////////////////////////

typedef struct
{
	CMainFrame * m_pWnd;
	CProdLine * m_pLine;
} LOADCACHESTRUCT;

ULONG CALLBACK CMainFrame::LoadCacheFunc (LPVOID lpData)
{
	LOADCACHESTRUCT * pParams = (LOADCACHESTRUCT *)lpData;
	ASSERT (pParams);
	ASSERT (pParams->m_pWnd);
	ASSERT (pParams->m_pLine);

	CMainFrame & frame = * pParams->m_pWnd;
	CProdLine & line = * pParams->m_pLine;

	line.LoadCache (frame); 

	delete pParams;
	frame.m_hCacheThread = NULL;
	return 0;
}

void CMainFrame::LoadCache ()
{
	ASSERT (m_pControlView);
	CControlDoc * pDoc = m_pControlView->GetDocument ();
	ASSERT (pDoc);

	if (CProdLine * pLine = pDoc->GetSelectedLine ()) {
		DWORD dw;
		LOADCACHESTRUCT * p = new LOADCACHESTRUCT;

		p->m_pWnd = this;
		p->m_pLine = pLine;

		m_hCacheThread = ::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0,
			(LPTHREAD_START_ROUTINE)LoadCacheFunc, (LPVOID)p, 0, &dw);

		ASSERT (m_hCacheThread != NULL);
	}
}

////////////////////////////////////////////////////////////////////////////////

static ULONG CALLBACK ParamChangeFunc (LPVOID lpData)
{
	const CTime tmStart = CTime::GetCurrentTime ();
	CTimeSpan tm = CTime::GetCurrentTime () - tmStart;
	LONG lTimeout = (UINT)lpData;
	LONG lTime = 0;

	srand (time (NULL));

	TRACEF (_T ("ParamChangeFunc started: ") + CTime::GetCurrentTime ().Format (_T ("%c")));

	do {

		::PostMessage (HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, 0);
		CString str = tm.Format (_T ("%H:%M:%S")) + CTime::GetCurrentTime ().Format (_T (" [%c]")) + _T (" ") + FoxjetDatabase::ToString (tm.GetTotalSeconds ());

		TRACEF (str);
		TRACER (str + _T ("\n"));

		/* TODO: rem
		#ifndef _DEBUG
		if (CMainFrame * pFrame = theApp.GetActiveFrame ()) 
			pFrame->m_wndStatusBar.SetPaneText (1, str);
		#endif
		*/

		DWORD dwSleep = 7000 + (rand () % 5000);
		::Sleep (dwSleep);
		tm = CTime::GetCurrentTime () - tmStart;
		lTime = tm.GetTotalSeconds ();
	}
	while (lTime < lTimeout);

	TRACEF (_T ("ParamChangeFunc exiting: ") + CTime::GetCurrentTime ().Format (_T ("%c")));

	return 0;
}

static ULONG CALLBACK RefreshFunc (LPVOID lpData)
{
	const CTime tmStart = CTime::GetCurrentTime ();
	CTimeSpan tm;
	LONG lTimeout = (UINT)lpData;

	do {

		if (CControlView * pView = (CControlView *)theApp.GetActiveView ()) 
			pView->PostMessage (WM_COMMAND, MAKEWPARAM (ID_VIEW_REFRESHPREVIEW, 0));

		DWORD dwSleep = 7000 + (rand () % 5000);
		::Sleep (dwSleep);
		tm = CTime::GetCurrentTime () - tmStart;
	}
	while (tm.GetTotalSeconds () < lTimeout);

	return 0;
}

////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
extern BYTE szDebugBuffer [];
extern int nDebugBufferSize;
extern CDiagnosticCriticalSection csDebugBuffer;
#endif


typedef struct
{
	DWORD dwThreadID;
	bool m_bEOP;
	DWORD m_dwSleep;
} DEMOPHOTOCELLSTRUCT;

static int CALLBACK DemoSinglePhotocellFunc (LPARAM lParam)
{
	DEMOPHOTOCELLSTRUCT * p = (DEMOPHOTOCELLSTRUCT *)lParam;
	DWORD dwID = p->dwThreadID;
	CString str;

	str.Format (_T ("0x%p: %s [%d ms]"), p->dwThreadID, p->m_bEOP ? _T ("EOP") : _T ("PC "), p->m_dwSleep);
	TRACEF (str);

	::Sleep (p->m_dwSleep);

	while (1) {
		while (!::PostThreadMessage (dwID, TM_SIM_PHOTOCELL, p->m_bEOP, (LPARAM)new CHeadInfo::POSTMESSAGESTRUCT (true, __FILE__, __LINE__, NULL))) 
			::Sleep (0); 

		DWORD dwSleep = (p->dwThreadID % 1000) + (rand () % (!p->m_bEOP ? 5000 : 2000) );
		str.Format (_T ("0x%p: %s [%d ms]"), p->dwThreadID, p->m_bEOP ? _T ("EOP") : _T ("PC "), p->m_dwSleep);
		TRACEF (str);
		::Sleep (dwSleep);
		p->m_bEOP = !p->m_bEOP;
	}

	delete p;

	return 0;
}

static int CALLBACK DemoPhotocellFunc (LPARAM lParam)
{
	CMainFrame & wnd = * (CMainFrame *)lParam;
	bool bEOP = false;

	srand (time (NULL));

//	while (1) {
		DWORD dwSleep = 4000 + (rand () % 10000);

		if (CControlDoc * pDoc = (CControlDoc *) wnd.m_pControlView->GetDocument ()) {
			if (CProdLine * pLine = pDoc->GetSelectedLine()) {
				CPrintHeadList list;

				pLine->GetActiveHeads (list);

				for (POSITION pos = list.GetStartPosition(); pos; ) {
					DWORD dwKey;
					CHead * pHead = NULL;

					list.GetNextAssoc(pos, dwKey, (void *&)pHead);
					ASSERT (pHead);
					CHeadInfo info = CHead::GetInfo (pHead);
					//int nUID = _ttoi (info.GetUID ());

					//TRACEF (_T ("OnPhotocellSim [") + info.GetUID () + _T ("]: ") + CString (bEOP ? _T ("EopEvent"): _T ("PhotoEvent")));
					//POST_THREAD_MESSAGE (info, TM_SIM_PHOTOCELL, bEOP);

					DEMOPHOTOCELLSTRUCT * p = new DEMOPHOTOCELLSTRUCT;
					DWORD dw = 0;

					p->dwThreadID = info.GetThreadID ();
					p->m_bEOP = bEOP;
					p->m_dwSleep = 100 + (rand () % 2000);

					::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0, (LPTHREAD_START_ROUTINE)DemoSinglePhotocellFunc, (LPVOID)p, 0, &dw);
				}
			}
		}

		TRACEF (_T ("dwSleep: ") + ToString (dwSleep));
		::Sleep (dwSleep);

		bEOP = !bEOP;
//	}

	return 0;
}

void CMainFrame::OnImport () 
{
	DBCONNECTION (conn);

	Foxjet3d::Import (this);
}

void CMainFrame::OnExport () 
{
	DBCONNECTION (conn);

	Foxjet3d::Export (this);
}


//static LRESULT CALLBACK PhotocellTestFunc (LPVOID lpData)
//{
//	CHead * pHead = (CHead *)lpData;
//	CHeadInfo info = CHead::GetInfo (pHead);
//	int n = 0;
//
//	while (1) {
//		while (!::PostThreadMessage (info.GetThreadID (), TM_EVENT_PHOTO, 0, 0)) 
//			::Sleep (1);
//
//		::Sleep (250);
//		
//		while (!::PostThreadMessage (info.GetThreadID (), TM_EVENT_EOP, 0, 0)) 
//			::Sleep (1);
//
//		::Sleep (1000);
//	}
//
//	return 0;
//}


void CMainFrame::OnOperTest() 
{
	#if defined( __DEBUG_LOW_INK_CODE__ )
	static int n = 0;
	static const DWORD map [] = 
	{
		0,
		//HEADERROR_HIGHVOLTAGE,
		HEADERROR_LOWINK,
		//HEADERROR_OUTOFINK, // has to be calculated by head
	};
	int nIndex = ++n % ARRAYSIZE (map);

	if (map [nIndex])
		::dwSimError |= map [nIndex];
	else
		::dwSimError = 0;

	TRACEF ("TODO: rem");
	return;
	#endif

#ifdef _DEBUG

	{
		CControlDoc * pDoc = m_pControlView->GetDocument ();

		if (CProdLine *pLine = pDoc->GetSelectedLine ()) { // TODO: rem 
			#ifdef _DEBUG
			CSerialPacket * p = new CSerialPacket (_T ("COM7"));
			TCHAR sz [512] = { 0 };
			LPCTSTR lpsz [] = 
			{
				//_T ("x.php?fst=1&lst=2&d1=FoxJet&dir=http%3A%2F%2Fburlrice.com%2F_LS_PS5077%2F&image=WC24281_2_lr.jpg"),
				//_T ("<LF>serial.cgi?idx=0&nme=user.prd&fst=1&lst=2&d1=FoxJet&dir=http%3A%2F%2Fburlrice.com%2F_LS_PS5077%2F&image=WC24281_2_lr.jpg<CR><LF>"),
				//_T ("<LF>serial.cgi?idx=0&nme=1.prd<CR><LF>"),
				//_T ("<LF>serial.cgi?idx=0&nme=2.prd<CR><LF>"),
				//_T ("<LF>print.cgi?idx=0&status=Pause<CR><LF>"),
				//_T ("<LF>print.cgi?idx=0&status=Start<CR><LF>"),
				//_T ("<LF>print.cgi?idx=0&status=Cancel<CR><LF>"),
				//_T ("<LF>print.cgi?idx=0&status=ResetCount<CR><LF>"),
				//_T ("{Get Elements,,21}"),
				//_T ("<FS>01<GS>01<RS>ABC<GS>02<RS>123<GS>03<RS>ZZZZZ<CR>"),
				//_T ("<FS>02<GS>01<RS>XXX<CR>"),
				_T ("201512040012600000010962,�������Թؽ���������磨S1133��+1733�꣩,��2011������ҩ֤��35��,�¹�����,+86 10 8455 1497"),
			};
			static int nCalls = 0;
			int nIndex = nCalls % ARRAYSIZE (lpsz);
			int nLen = FoxjetDatabase::Unformat (lpsz [nIndex], sz);
			//TRACEF ("TODO: rem: " + FoxjetDatabase::Format (sz, nLen));

			p->AssignData (SERIALSRC_MPHC, REMOTEDEVICE, pLine->GetName (), sz, nLen, 2312);

			//TRACEF ("TODO: rem: " + FoxjetDatabase::Format (p->m_pData, p->m_nDataLen));

			for (int i = 0; i < p->CountSegments (); i++) {
				if (CSerialPacket * pSeg = p->GetSegment (i))	
					TRACEF ("TODO: rem: " + FoxjetDatabase::Format (pSeg->m_pData, pSeg->m_nDataLen));
			}

			while (!m_pControlView->PostMessage (WM_HEAD_SERIAL_DATA, 0, (LPARAM)p)) 
				::Sleep (0);

			nCalls++;
			return;
			#endif //_DEBUG
		}
	}

	{
		CControlDoc * pDoc = m_pControlView->GetDocument ();

		if (CProdLine *pLine = pDoc->GetSelectedLine ()) { // TODO: rem 
			#ifdef _DEBUG
			CSerialPacket * p = new CSerialPacket (_T ("COM7"));
			LPCTSTR lpsz [] = 
			{
				_T ("384 Sample\r"),
			};
			static int nCalls = 0;
			int nIndex = nCalls % ARRAYSIZE (lpsz);

			TRACEF ("TODO: rem: " + CString (lpsz [nIndex]));
			p->AssignData (SERIALSRC_MPHC, HANDSCANNER, pLine->GetName (), lpsz [nIndex], _tcslen (lpsz [nIndex]), 0);

			while (!m_pControlView->PostMessage (WM_HEAD_SERIAL_DATA, 0, (LPARAM)p)) 
				::Sleep (0);

			nCalls++;
			return;
			#endif //_DEBUG
		}
	}


#endif

	DBCONNECTION (conn);

//#ifdef _DEBUG
//	static int n = 0;
//
//	switch (n++ % 5) {
//	case 0:	::dwSimError |= HEADERROR_LOWINK;			break;
//	case 1:	::dwSimError |= HEADERROR_OUTOFINK;			break;
//	case 2:	::dwSimError |= HEADERROR_HIGHVOLTAGE;		break;
//	case 3:	::dwSimError |= HEADERROR_LOWTEMP;			break;
//	case 4:	::dwSimError = 0;							break;
//	}
//
//	TRACEF ("TODO: rem");
//	return;
//#endif

	ASSERT (m_pControlView);
	CControlDoc * pDoc = m_pControlView->GetDocument ();
	ASSERT (pDoc);

	//{ // TODO: rem
	//	theApp.GetUSBDriver ()->SetDemoPhotocellInterval (128);

	#if 0 //defined( _DEBUG )
	{
		if (CProdLine * pLine = pDoc->GetSelectedLine ()) { 
			CPrintHeadList heads;

			pLine->GetActiveHeads (heads);

			for (POSITION pos = heads.GetStartPosition(); pos; ) {
				DWORD dwKey;
				CHead * pHead = NULL;

				heads.GetNextAssoc(pos, dwKey, (void *&)pHead);
				ASSERT (pHead);
				
				CHeadInfo info = CHead::GetInfo (pHead);

				while (!m_pControlView->PostMessage (WM_SOCKET_PACKET_PRINTER_HEAD_STATE_CHANGE, info.m_Config.m_HeadSettings.m_lID, 0)) 
					::Sleep (0);
			}
		}

		return;
	}
	#endif //_DEBUG


	//	if (CProdLine * pLine = pDoc->GetSelectedLine ()) { 
	//		CPrintHeadList heads;

	//		pLine->GetActiveHeads (heads);

	//		for (POSITION pos = heads.GetStartPosition(); pos; ) {
	//			DWORD dwKey;
	//			CHead * pHead = NULL;

	//			heads.GetNextAssoc(pos, dwKey, (void *&)pHead);
	//			ASSERT (pHead);
	//			
	//			CHeadInfo info = CHead::GetInfo (pHead);

	//			if (info.IsMaster ()) {
	//				DWORD dw = 0;

	//				::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0, (LPTHREAD_START_ROUTINE)PhotocellTestFunc, (LPVOID)pHead, 0, &dw);
	//			}
	//		}
	//	}

	//	return;
	//}

	#ifdef _DEBUG

	/*
	{
		CStringArray v;
		CString strPath = FoxjetDatabase::FormatString (_T ("C:\\Program Files\\Foxjet\\MarksmanELITE\\Logos\\FitnessSnacker.bmp"));

		v.Add (_T ("Add element"));
		v.Add (_T ("LINE0001"));
		v.Add (_T ("21"));
		v.Add (FoxjetDatabase::FormatString (_T ("{Bitmap,9,0,750,5584,1250,0,0,0,") + strPath + _T (",0}")));
		CString str = FoxjetDatabase::ToString (v);

		TRACEF (str);
		CString	strResult = CServerThread::ProcessCommand (str, m_pControlView);
		TRACEF (strResult);
		return;
	}
	*/

	/*
	if (CProdLine *pLine = pDoc->GetSelectedLine ()) { // TODO: rem 		
		CStringArray v, vTmp;
		static int nCalls = 0;

		// TODO: TOKEN_LINEID

		GetFiles (GetHomeDir () + _T ("\\Ship Pack"), _T ("*.*"), v);

		{
			for (int i = 0; i < v.GetSize (); i++)
				InsertAscending (vTmp, v [i]);

			v.RemoveAll ();
			v.Append (vTmp);
		}

		if (v.GetSize ()) {
			DECLARETRACECOUNT (200);
			CString strFile = v [nCalls++ % v.GetSize ()];
			CString strData = FoxjetFile::ReadDirect (strFile);
			LPCTSTR lpszComm [] = { _T ("COM1"), _T ("COM2") };
			
			MARKTRACECOUNT ();

			for (int i = 0; i < ARRAYSIZE (lpszComm); i++) {
				SETTINGSSTRUCT s;

				MARKTRACECOUNT ();

				if (GetSettingsRecord (theApp.m_Database, 0, lpszComm [i], s)) {
					ControlView::CComm comm;

					if (comm.FromString (s.m_strData)) {
						CSerialPacket * p = new CSerialPacket (lpszComm [i]);

						MARKTRACECOUNT ();
						p->AssignData (SERIALSRC_MPHC, comm.m_type, pLine->GetName (), a2w (strData), strData.GetLength (), 0);
						
						MARKTRACECOUNT ();
						m_pControlView->SendMessage (WM_HEAD_SERIAL_DATA, 0, (LPARAM)p);
						MARKTRACECOUNT ();
					}
				}
			}

			TRACEF (strFile + _T (" --> ") + strData);
			TRACECOUNTARRAY ();
		}

		return;
	}
	*/

	if (CProdLine *pLine = pDoc->GetSelectedLine ()) { // TODO: rem 
		CSerialPacket * p = new CSerialPacket (_T ("COM1"));
		LPCTSTR lpsz [] = 
		{
			//_T ("TTTTTT1111112222223333333\r"),
			//_T ("PPPPPPPPPPPPPPPP\r"),
			//_T ("SSSSSSSSSSSS33333\r"),
			_T ("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")
			_T ("	-<asx:abap version=\"1.0\" xmlns:asx=\"http://www.sap.com/abapxml\">\n")
			_T ("	-<asx:values>\n")
			_T ("	-<TAB>\n")
			_T ("	-<item>\n")
			_T ("	<NB_OF_CONTAINER>9</NB_OF_CONTAINER>\n")
			_T ("       <SOC_LOG>96Sample.bmp</SOC_LOG>")
			_T ("       <PARA>")
						_T ("0: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("1: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("2: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("3: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("4: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("5: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("6: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("7: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("8: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("9: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("10: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("11: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("12: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("13: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("14: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("15: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("16: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("17: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("18: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
						_T ("19: 1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ&#13;&#10;")
			_T ("       </PARA>")
			_T ("		<F_LOGO1>Sanofi-Aventis</F_LOGO1>\n")
			_T ("		<F_LOGO2>Farmac�utica Ltda</F_LOGO2>\n")
			_T ("		<F_LOGO3>R. Conde Domingos Papaiz, 413 Suzano-SP</F_LOGO3>\n")
			_T ("		<F_LOGO4>C.N.P.J 02.685.377/0008-23</F_LOGO4>\n")
			_T ("		<F_LOGO5>I n d � s t r i a B r a s i l e i r a</F_LOGO5>\n")
			_T ("		<F_NUMOP>&#60;N� ORDEM DE PROCESSO:&#62;</F_NUMOP>\n")
			_T ("		<F_MATCD>C�DIGO PRODUTO:</F_MATCD>\n")
			_T ("		<F_VENDA>C�DIGO VENDA:</F_VENDA>\n")
			_T ("		<F_LOTES>LOTE:</F_LOTES>\n")
			_T ("		<F_DTFAB>FABRICA��O:</F_DTFAB>\n")
			_T ("		<F_DTVAL>VALIDADE:</F_DTVAL>\n")
			_T ("		<F_PRINC>PRINC�PIO ATIVO:</F_PRINC>\n")
			_T ("		<F_MATDC>PRODUTO:</F_MATDC>\n")
			_T ("		<F_CONCE>TESTE 01</F_CONCE>\n")
			_T ("		<F_CAIXA>CAIXA N�:</F_CAIXA>\n")
			_T ("		<F_QTDCX>QUANTIDADE:</F_QTDCX>\n")
			_T ("		<F_EANCD>EAN:</F_EANCD>\n")
			_T ("		<F_FORMA>FORMA FARMAC�UTICA:</F_FORMA>\n")
			_T ("		<F_OBSER>OBS:</F_OBSER>\n")
			_T ("		<F_PESOL>TESTE 02</F_PESOL>\n")
			_T ("		<F_PESOB>TESTE 03</F_PESOB>\n")
			_T ("		<F_CONSE>CUIDADOS DE CONSERVA��O:</F_CONSE>\n")
			_T ("		<F_EMISS>EMITIDO POR:</F_EMISS>\n")
			_T ("		<F_EAN13>EAN13</F_EAN13>\n")
			_T ("		<F_DUN14>DUN14</F_DUN14>\n")
			_T ("		<V_NUMOP>6005225</V_NUMOP>\n")
			_T ("		<V_MATCD>3192</V_MATCD>\n")
			_T ("		<V_VENDA>DCY001</V_VENDA>\n")
			_T ("		<V_LOTES>024912</V_LOTES>\n")
			_T ("		<V_DTFAB>06.12.2010</V_DTFAB>\n")
			_T ("		<V_DTVAL>30.11.2013</V_DTVAL>\n")
			_T ("		<V_PRINC>acido l�tico + lactoserum</V_PRINC>\n")
			_T ("		<V_MATDC>DERMACYD FEMINA 200ML</V_MATDC>\n")
			_T ("		<V_CONC1>TESTE 04</V_CONC1>\n")
			_T ("		<V_CONC2>TESTE 05</V_CONC2>\n")
			_T ("		<V_CONC3>TESTE 06</V_CONC3>\n")
			_T ("		<V_CONC4>TESTE 07</V_CONC4>\n")
			_T ("		<V_CAIXA>00003</V_CAIXA>\n")
			_T ("		<V_QTDCX>000020 UN</V_QTDCX>\n")
			_T ("		<V_EANCD>7897595901866</V_EANCD>\n")
			_T ("		<V_FORMA>Sabonete L�quido</V_FORMA>\n")
			_T ("		<V_OBSER>EXTRA</V_OBSER>\n")
			_T ("		<V_PESOL>TESTE 08</V_PESOL>\n")
			_T ("		<V_PESOB>TESTE 09</V_PESOB>\n")
			_T ("		<V_CONSE>CONSERVAR EM TEMPERATURA AMBIENTE (ENTRE 15-30�C)</V_CONSE>\n")
			_T ("		<V_EMNOM>E0223868</V_EMNOM>\n")
			_T ("		<V_EMDTH>EM 17.07.2015 AS 14:26:13</V_EMDTH>\n")
			_T ("		<V_LEG12>(01)27897595901866(30)000020(10)024912</V_LEG12>\n")
			_T ("		<V_BAR12>01278975959018663000002010024912</V_BAR12>\n")
			_T ("		<V_LEG13>7897595901866</V_LEG13>\n")
			_T ("		<V_BAR13>7897595901866</V_BAR13>\n")
			_T ("		<V_LEG14>17897595901863</V_LEG14>\n")
			_T ("		<V_BAR14>17897595901863</V_BAR14>\n")
			_T ("		<V_QTDET>30</V_QTDET>\n")
			_T ("	</item>\n")
			_T ("	</TAB>\n")
			_T ("	</asx:values>\n")
			_T ("</asx:abap>\n"),
		};
		static int nCalls = 0;
		int nIndex = nCalls % ARRAYSIZE (lpsz);

		//TRACEF (FoxjetDatabase::Format ((LPBYTE)lpsz [nIndex], _tcslen (lpsz [nIndex])));

		//pLine->SetSerialPending (false);
		TRACEF ("TODO: rem: " + CString (lpsz [nIndex]));
		p->AssignData (SERIALSRC_MPHC, HOSTINTERFACE, pLine->GetName (), lpsz [nIndex], _tcslen (lpsz [nIndex]), 0);
		//p->AssignData (SERIALSRC_MPHC, HANDSCANNER, pLine->GetName (), lpsz [nIndex], _tcslen (lpsz [nIndex]), 0);
		//p->AssignData (SERIALSRC_MPHC, SW0858_DBSTART, pLine->GetName (), (const PUCHAR)(LPCSTR)w2a (lpsz [nIndex]), _tcslen (lpsz [nIndex]));

		while (!m_pControlView->PostMessage (WM_HEAD_SERIAL_DATA, 0, (LPARAM)p)) 
			::Sleep (0);

		nCalls++;
		return;
	}
	#endif

	/*
	#ifdef _DEBUG
	{
		if (USB::CUsbMphcCtrl * pDriver = theApp.GetUSBDriver ()) {
			if (CProdLine * pLine = pDoc->GetSelectedLine ()) { 
				CPrintHeadList heads;

				pLine->GetActiveHeads (heads);

				for (POSITION pos = heads.GetStartPosition(); pos; ) {
					DWORD dwKey;
					CHead * pHead = NULL;

					heads.GetNextAssoc(pos, dwKey, (void *&)pHead);
					ASSERT (pHead);
				
					if (CLocalHead * pLocal = DYNAMIC_DOWNCAST (CLocalHead, pHead)) {
						TRACEF (ToString (pDriver->ProgramFPGA ((UINT)pLocal->GetDeviceID ())));
					}
				}
			}
		}
		return;
	}
	#endif
	*/

	if (CProdLine *pLine = pDoc->GetSelectedLine ()) { // TODO: rem 
		#ifdef _DEBUG
		CSerialPacket * p = new CSerialPacket (_T ("COM1"));
		LPCTSTR lpsz [] = 
		{
		//	_T ("768 Sample\r"),
		//	_T ("384 Sample\r"),
			_T ("56523\r"),
			_T ("56524\r"),
		};
		static int nCalls = 0;
		int nIndex = nCalls % ARRAYSIZE (lpsz);

		TRACEF ("TODO: rem: " + CString (lpsz [nIndex]));
		p->AssignData (SERIALSRC_MPHC, SW0858_DBSTART /* REMOTEDEVICE */, pLine->GetName (), lpsz [nIndex], _tcslen (lpsz [nIndex]), 0);

		while (!m_pControlView->PostMessage (WM_HEAD_SERIAL_DATA, 0, (LPARAM)p)) 
			::Sleep (0);

		nCalls++;
		return;
		#endif //_DEBUG
	}

	/*
	#ifdef _DEBUG
	{
		LOCK (::csDebugBuffer);
		{
			if (FILE * fp = _tfopen (GetHomeDir () + _T ("\\UnicodeComm.txt"), _T ("rb"))) {
				BYTE sz [COM_BUF_SIZE] = { 0 };
				int n = 0;

				while (!feof (fp)) 
					sz [n++] = (BYTE)fgetc (fp);

				memcpy (::szDebugBuffer, sz, n - 1);
				::nDebugBufferSize = n - 1;
				fclose (fp);
				return;
			}
		}
	}
	#endif
	*/

/*
#ifdef _DEBUG
	if (CProdLine * pLine = pDoc->GetSelectedLine ()) {
		LOCK (pLine->m_HeadListSentinel);
		{
			bool bStopped = false;
			FoxjetDatabase::TASKSTATE state = pLine->GetTaskState ();
			CArray <CHead *, CHead *> v;

			for (POSITION pos = pLine->m_ActiveHeadList.GetStartPosition(); pos; ) {
				CHead * pHead = NULL;
				DWORD dwKey;

				pLine->m_ActiveHeadList.GetNextAssoc(pos, dwKey, (void *&)pHead);

				if (pHead) {
					for (int i = 0; i < v.GetSize (); i++) {
						if (_tcstoul (v [i]->m_Config.m_HeadSettings.m_strUID, NULL, 16) < _tcstoul (pHead->m_Config.m_HeadSettings.m_strUID, NULL, 16)) {
							v.InsertAt (i, pHead);
							break;
						}
					}

					v.Add (pHead);
				}
			}

			if (v.GetSize ()) {
				CHead * pHead = v [0];

				if (CControlView * pView = (CControlView *)theApp.GetActiveView ()) {
					TRACEF (pHead->m_Config.m_HeadSettings.m_strName + _T (": ") + pHead->m_Config.m_HeadSettings.m_strUID);
					MsgBox (_T ("Deleting: ") + pHead->m_Config.m_HeadSettings.m_strName + _T (": ") + pHead->m_Config.m_HeadSettings.m_strUID);

					pLine->EndThread ();
					pLine->IdleTask (false, false); 
					bStopped = true;
					pLine->StopTask (false, pView);
					pView->OnHeadDelete (0, pHead->m_Config.m_HeadSettings.m_lID);

					//while (!::PostThreadMessage (pHead->m_nThreadID, TM_HEAD_CONFIG_CHANGED, 0, 0L)) 
					//	::Sleep (0);

					if (!bStopped) {
						if (state == RUNNING)
							pLine->ResumeTask (false);
					}

					pLine->BeginThread ();
				}
			}
		}

		return;
	}
#endif
*/
	

	if (CProdLine *pLine = pDoc->GetSelectedLine ()) {
		/*
		#if defined( __IPPRINTER__ ) && defined( _DEBUG )
		{ 
			FoxjetCommon::CLongArray sel = ItiLibrary::GetSelectedItems (m_pControlView->m_HeadCtrl);
			int nIndex = 0;

			if (sel.GetSize ())
				nIndex = sel [0];

			if (CRemoteHead * p = DYNAMIC_DOWNCAST (CRemoteHead, m_pControlView->m_HeadCtrl.GetSelection (nIndex).m_pThread)) {
				TRACEF (p->GetFriendlyName ());

				p->SetKeepAlive (true);
			}

			return;
		}
		#endif //__IPPRINTER__ && _DEBUG
		*/

/*
#ifdef _DEBUG
{
	static bool bError = true;
	((CControlView *)pDoc->GetView ())->SocketSetErrorState (pLine->m_LineRec.m_lID, -1, bError);
	bError = !bError;
	return;
}
#endif
*/

/*
	{ // TODO: rem 
		#ifdef _DEBUG
		CSerialPacket * p = new CSerialPacket (_T ("COM1"));
		LPCTSTR lpsz [] = 
		{
			_T ("12345\r"),
//			_T ("<FS>02<DC3>0123456789<DC1><FS>016057002<DC1>Untitled1<DC4>{Set count,LINE0002,99}<CR>"),
//			_T ("Untitled1<CR>"),
//			_T ("test\r"),
			//_T ("TTTTTT1111112222223333333\r"),
			//_T ("PPPPPPPPPPPPPPPP\r"),
			//_T ("SSSSSSSSSSSS33333\r"),
			
			//_T ("115818\r"),
//			_T ("red gold\r"),		// Line
//			_T ("171274\r"),		// Line
//			_T ("541593\r"),		// Line
//			_T ("169196\r"),		// Line
//			_T ("15847\r"),			// Line
			//_T ("115816\r"),
//			_T ("073246\r"),		// Line, PRODUCTION LINE #
//			_T ("036593\r"),		// PRODUCTION LINE #
//			_T ("584845\r"),		// Line
//			_T ("571721\r"),		// Line
//			_T ("700710\r"),		// Line
//			_T ("95357\r"),			// Line
//			_T ("81802\r"),			// Line
//			_T ("994628\r"),		// Line
//			_T ("995205\r"),		// Line
//			_T ("163439\r"),		// Line
//			_T ("287820\r"),		// Line
//			_T ("Ruby Tuesday\r"),	// Line
//			_T ("115849\r"),		// Line
//			_T ("0000\r"),			// Line
		};

		static int nCalls = 0;
		int nIndex = nCalls % ARRAYSIZE (lpsz);

		//TRACEF (FoxjetDatabase::Format ((LPBYTE)lpsz [nIndex], _tcslen (lpsz [nIndex])));

		//pLine->SetSerialPending (false);
		TRACEF ("TODO: rem: " + CString (lpsz [nIndex]));
		p->AssignData (SERIALSRC_MPHC, HANDSCANNER, pLine->GetName (), lpsz [nIndex], _tcslen (lpsz [nIndex]), 0);
		//p->AssignData (SERIALSRC_MPHC, SW0858_DBSTART, pLine->GetName (), (const PUCHAR)(LPCSTR)w2a (lpsz [nIndex]), _tcslen (lpsz [nIndex]));

		while (!m_pControlView->PostMessage (WM_HEAD_SERIAL_DATA, 0, (LPARAM)p)) 
			::Sleep (0);

		nCalls++;
		return;
		#endif //_DEBUG
	}
*/

		CString strTask = pLine->GetCurrentTask ();
		int nIndex = 0;

		if (strTask.GetLength ()) {
			if (MsgBox (* this, LoadString (IDS_STOPTASK), NULL, MB_YESNO) == IDNO)
				return;
			else {
				pLine->StopTask (false);
			}
		}

		pLine->StartTask (GlobalVars::strTestPattern, false, false, false, m_pControlView);
	}
}


#ifdef __IPPRINTER__
void CMainFrame::OnCfgPrinthead() 
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

	CControlDoc *pDoc = (CControlDoc *) GetActiveDocument();
	ASSERT (pDoc);
	CProdLine *pLine = pDoc->GetSelectedLine();
	ASSERT (pLine);

	BEGIN_TRANS (theApp.m_Database);

	Foxjet3d::HeadConfigDlg::CHeadConfigDlg dlg (theApp.m_Database, ListGlobals::defElements.GetUnits (), pLine->GetID (), this);

	if (dlg.DoModal() == IDOK) {
		COMMIT_TRANS (theApp.m_Database);
		m_pControlView->HeadCfgUpdated (&dlg.m_vAdded);
	}
	else {
		ROLLBACK_TRANS (theApp.m_Database);
	}
}
#endif //__IPPRINTER__

#ifdef __WINPRINTER__

void CMainFrame::OnCfgPrinthead() 
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

	using namespace Foxjet3d;

	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
	CULongArray vAddr = theApp.GetDriverAddressList ();
	bool bRestricted = false;
	
	if (theApp.m_strUsername.CompareNoCase (_T ("root")) != 0) {
		USERSTRUCT r;

		r.m_lSecurityGroupID = 0;
		GetUserRecord (theApp.m_Database, theApp.m_strUsername, r);
		//bRestricted = (r.m_lSecurityGroupID < 10) ? true : false;
		bRestricted = !theApp.LoginPermits (IDM_CFG_PRINTHEAD);
	}

	FoxjetDatabase::GetPrinterHeads (theApp.m_Database, GetPrinterID (theApp.m_Database), vHeads); //FoxjetDatabase::GetHeadRecords (theApp.m_Database, vHeads);

	/*
	#ifdef _DEBUG
	vAddr.RemoveAll ();

	if (theApp.IsMPHC ()) {
		vAddr.Add (0x300);
		vAddr.Add (0x310);
		vAddr.Add (0x320);
		vAddr.Add (0x330);
		vAddr.Add (0x340);
		vAddr.Add (0x350);
	}
	else {
		vAddr.Add (0x300);
		vAddr.Add (0x320);
	}
	#endif //_DEBUG
	*/

	CControlDoc *pDoc = (CControlDoc *) GetActiveDocument();
	ASSERT (pDoc);
	CProdLine *pLine = pDoc->GetSelectedLine();
	ASSERT (pLine);

	BEGIN_TRANS (theApp.m_Database);

	Foxjet3d::HeadConfigDlg::CHeadConfigDlg dlg (theApp.m_Database, ListGlobals::defElements.GetUnits (), pLine->GetID (), bRestricted, this);

	for (int i = 0; i < vAddr.GetSize (); i++) {
		CString str;
		LONG lAddr = vAddr [i];

		str.Format (_T ("%03X"), lAddr);
		
		dlg.m_vAvailAddressMap [str] = Foxjet3d::CHeadDlg::GetPHC (lAddr);
	}

/* TODO: rem
	if (USB::CUsbMphcCtrl * p = theApp.GetUSBDriver ()) {
		int nHeads = p->GetAddressCount(); 
		TRACEF (_T ("nHeads: ") + FoxjetDatabase::ToString (nHeads));

		for (int i = 0; i < nHeads; i++) {
			CString str;
			ULONG lAddr = i + 1;

			str.Format (_T ("%03X"), lAddr);
			dlg.m_vAvailAddressMap [str] = Foxjet3d::CHeadDlg::GetPHC (lAddr);
		}

	}
*/

	if (dlg.DoModal() == IDOK) {
		COMMIT_TRANS (theApp.m_Database);

		//WM_SYSTEMPARAMCHANGE will call m_pControlView->HeadCfgUpdated();
		::PostMessage (HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, 0);

		if (dlg.m_bRestartTask) 
			::PostMessage (m_pControlView->m_hWnd, WM_RESTART_TASK, 0, 0);
	}
	else {
		ROLLBACK_TRANS (theApp.m_Database);
	}
}
#endif //__WINPRINTER__

void CMainFrame::OnCfgProductionLines() 
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

	BEGIN_TRANS (theApp.m_Database);

	Foxjet3d::LineConfigDlg::CLineConfigDlg dlg (theApp.m_Database, this);

	if (dlg.DoModal() == IDOK) {
		COMMIT_TRANS (theApp.m_Database);
		PostMessage (WM_COMMAND, DOC_CMD_LINES_UPDATED, 0L);
		::PostMessage (HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, 0);
	}
	else {
		ROLLBACK_TRANS (theApp.m_Database);
	}
}

void CMainFrame::OnCfgUser() 
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

	CCfgUsersDlg Dialog;

	BEGIN_TRANS (theApp.m_Database);

	if (Dialog.DoModal() == IDOK) {
		COMMIT_TRANS (theApp.m_Database);
	}
	else {
		ROLLBACK_TRANS (theApp.m_Database);
	}
}

void CMainFrame::OnCfgGroupOptions() 
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

	GroupOptionsDlg::CGroupOptionsDlg Dialog;

	BEGIN_TRANS (theApp.m_Database);

	if (Dialog.DoModal() == IDOK) {
		COMMIT_TRANS (theApp.m_Database);
	}
	else {
		ROLLBACK_TRANS (theApp.m_Database);
	}
}

void CMainFrame::UpdateAppExitUI () 
{
/*
	bool bCanClose = theApp.LoginPermits (ID_APP_EXIT);
	LONG lStyle = GetWindowLong (m_hWnd, GWL_STYLE);
	
	if (!bCanClose && (lStyle | WS_SYSMENU)) 
		lStyle &= ~WS_SYSMENU;
	else if (bCanClose && !(lStyle & WS_SYSMENU)) 
		lStyle |= WS_SYSMENU;

	if (lStyle != GetWindowLong (m_hWnd, GWL_STYLE)) {
//		::SetWindowLong (m_hWnd, GWL_STYLE, lStyle);
//		SendMessage (WM_NCACTIVATE, TRUE);
	}
*/
}

/* TODO: rem
void CMainFrame::OnUpdateCfgGroupOptions(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable( theApp.LoginPermits (pCmdUI->m_nID ) );
}

void CMainFrame::OnUpdateAppExit(CCmdUI* pCmdUI) 
{
	UpdateAppExitUI ();
	pCmdUI->Enable (theApp.LoginPermits (pCmdUI->m_nID));	
}
*/

void CMainFrame::OnUpdateUI () 
{
	using namespace FoxjetUtils;

	struct
	{
		UINT m_nNewID;
		UINT m_nOldID;
		LPCTSTR m_lpsz;
	} map [] = 
	{
		//{ BTN_DBSTART,		IDM_OPER_START,				_T ("BTN_DBSTART"),			},
		{ BTN_START,			IDM_OPER_START,				_T ("BTN_START"),			},
		{ BTN_IDLE,				IDM_OPER_START,				_T ("BTN_IDLE"),			},
		{ BTN_STOP,				IDM_OPER_STOP,				_T ("BTN_STOP"),			},
		{ BTN_EDIT,				IDM_OPER_EDIT,				_T ("BTN_EDIT"),			},
		{ BTN_CONFIG,			IDM_OPER_EDIT,				_T ("BTN_CONFIG"),			},
		//{ BTN_LOGIN,			IDM_LOGIN,					_T ("BTN_LOGIN"),			},
		//{ BTN_INFO,			IDM_INFO,					_T ("BTN_INFO"),			},
		{ BTN_COUNT,			IDM_OPER_START,				_T ("BTN_COUNT"),			}, //{ BTN_COUNT,		ID_OPERATE_CHANGECOUNTS,	_T ("BTN_COUNT"),		},
		{ BTN_USER,				IDM_OPER_START,				_T ("BTN_USER"),			}, //{ BTN_USER,		IDM_CHANGE_USER_ELEMENTS,	_T ("BTN_USER"),		},
		{ BTN_EXIT,				ID_APP_EXIT,				_T ("BTN_EXIT"),			},		
		{ BTN_LIMITED_CONFIG,	IDM_OPER_START,				_T ("BTN_LIMITED_CONFIG"),	}, 
		//{ BTN_BOX,			IDM_OPER_EDIT,				_T ("BTN_BOX"),				},		
		//{ BTN_REPORTS,		IDM_OPER_EDIT,				_T ("BTN_REPORTS"),			},	
		//{ BTN_ZOOMFIT,		IDM_OPER_EDIT,				_T ("BTN_ZOOMFIT"),			},	
		//{ BTN_ZOOMIN,			IDM_OPER_EDIT,				_T ("BTN_ZOOMIN"),			},
		//{ BTN_ZOOMOUT,		IDM_OPER_EDIT,				_T ("BTN_ZOOMOUT"),			},	
	};
	bool bUser = true;
	bool bCount = true;
	/*
	bool bCanClose = theApp.LoginPermits (ID_APP_EXIT);
	LONG lStyle = GetWindowLong (m_hWnd, GWL_STYLE);
	
	if (!bCanClose && (lStyle | WS_SYSMENU)) 
		lStyle &= ~WS_SYSMENU;
	else if (bCanClose && !(lStyle & WS_SYSMENU)) 
		lStyle |= WS_SYSMENU;

	if (lStyle != GetWindowLong (m_hWnd, GWL_STYLE)) {
		//::SetWindowLong (m_hWnd, GWL_STYLE, lStyle);
		//SendMessage (WM_NCACTIVATE, TRUE);
	}
	*/

	/*
	if (CControlDoc * pDoc = (CControlDoc *) GetActiveDocument ()) {
		if (CProdLine * pLine = pDoc->GetSelectedLine()) {
			CElementPtrArray vCount, vUser;

			pLine->GetElements (vCount, COUNT);
			pLine->GetElements (vUser, USER);

			bCount = vCount.GetSize () ? true : false;
			bUser = vUser.GetSize () ? true : false;
		}
	}
	*/

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		if (CRoundButton2 * p = (CRoundButton2 *)m_pControlView->GetDlgItem (map [i].m_nNewID)) {
			bool bEnable = theApp.LoginPermits (map [i].m_nOldID);

			switch (map [i].m_nNewID) {
			case BTN_COUNT: 
				if (!bCount)	
					bEnable = false;	
				break;
			case BTN_USER:	
				if (!bUser)		
					bEnable = false;	
				break;
			}

			//TRACEF (CString (map [i].m_lpsz) + _T (": ") + ToString (bEnable));
			p->EnableWindow (bEnable);
		}
	}

	{
		CUIntArray v;

		v.Add (IDM_VIEW_PRINT_REPORT);
		v.Add (IDM_VIEW_SCAN_REPORT);
		
		for (int i = v.GetSize () - 1; i >= 0; i--) 
			if (!theApp.LoginPermits (v [i])) 
				v.RemoveAt (i);

		if (CWnd * p = m_pControlView->GetDlgItem (BTN_REPORTS))
			p->EnableWindow (v.GetSize () > 0);
	}

	if (CWnd * p = m_pControlView->GetDlgItem (BTN_CONFIG))
		p->EnableWindow (TRUE);
}

void CMainFrame::OnLogout() 
{
	DWORD dwValue;
	bool bValue;
	theApp.m_bRootUser = false;
	
	// Reset Security Table.
	POSITION pos = theApp.m_SecurityTable.GetStartPosition();
	
	while ( pos ) {
		theApp.m_SecurityTable.GetNextAssoc (pos, dwValue, bValue);
		theApp.m_SecurityTable.SetAt (dwValue, false);
	}
	
	theApp.m_strUsername.Empty();
	UpdateAppExitUI ();
}

void CMainFrame::OnOperStart ()
{
	m_pControlView->PostMessage (WM_COMMAND, MAKEWPARAM (BTN_START, BN_CLICKED), NULL);

/* TODO: rem
	DBCONNECTION (conn);

	m_pControlView->StartTask ();
*/
}

void CMainFrame::OnOperStop ()
{
	DBCONNECTION (conn);

	m_pControlView->StopTask ();
}

void CMainFrame::OnOperIdle() 
{
	DBCONNECTION (conn);

	m_pControlView->IdleTask( NULL );
}
	
void CMainFrame::OnOperResume() 
{
	CControlView *pView = m_pControlView;
	VERIFY ( pView->IsKindOf ( RUNTIME_CLASS ( CControlView )) );
	pView->ResumeTask( NULL );
}

void CMainFrame::OnChangeUserElements() 
{
	DBCONNECTION (conn);

	CControlView *pView = m_pControlView;
	pView->ChangeUserElementData();
}

void CMainFrame::OnCfgBarcodeSettings() 
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

	bool bUpdate = false;

#ifdef __IPPRINTER__
	bUpdate = FoxjetIpElements::OnDefineGlobalBarcodes (this, GlobalVars::CurrentVersion);
#else
	bUpdate = FoxjetElements::OnDefineBarcodeParams (this, GlobalVars::CurrentVersion);
#endif
}

void CMainFrame::OnCfgDateCodes() 
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

#ifdef __IPPRINTER__
	FoxjetIpElements::OnDefineDateTimeSettings (this, GlobalVars::CurrentVersion);
#else
	FoxjetElements::OnDefineMonthHourCodes (this, GlobalVars::CurrentVersion);
#endif
}

void CMainFrame::OnCfgShiftCodes() 
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

#ifdef __IPPRINTER__
	FoxjetIpElements::OnDefineShiftCodes (this, GlobalVars::CurrentVersion);
#else
	FoxjetElements::OnDefineShiftCodes (this, GlobalVars::CurrentVersion);
#endif
}

void CMainFrame::OnViewDiag() 
{
	CControlView *pView = m_pControlView;
	if ( pView )
		::SendMessage ( pView->m_hWnd, WM_SHOW_DIAG, 0, 0 );
}

void CMainFrame::OnViewScanReport() 
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

	Foxjet3d::ScanReportDlg::CScanReportDlg dlg (theApp.m_Database);
	CControlView *pView = m_pControlView;
	VERIFY ( pView->IsKindOf ( RUNTIME_CLASS ( CControlView )) );
	CControlDoc *pDoc = pView->GetDocument();

	dlg.m_sLine = pDoc->GetSelectedLine ()->GetName();
	dlg.m_lPrinterID = GetPrinterID (theApp.m_Database);

	if (dlg.DoModal() == IDOK) 
		theApp.EnableScanReportLogging (dlg.m_bEnable); // (Foxjet3d::IsScanReportLoggingEnabled (theApp.m_Database, dlg.m_lPrinterID));
}

void CMainFrame::OnViewPrintReport() 
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

	CControlView *pView = m_pControlView;
	VERIFY ( pView->IsKindOf ( RUNTIME_CLASS ( CControlView )) );
	CControlDoc * pDoc = pView->GetDocument();
	
	for (bool bShow = true; bShow; ) {
		Foxjet3d::PrintReportDlg::CPrintReportDlg dlg(theApp.m_Database);

		dlg.m_sLine = pDoc->GetSelectedLine()->GetName();
		dlg.m_lPrinterID = GetPrinterID(theApp.m_Database);
		int nResult = dlg.DoModal();

		switch (nResult) {
		case IDOK:
			theApp.EnablePrintReportLogging(dlg.m_bEnable);
			bShow = false;
			break;
		case IDCANCEL:
			bShow = false;
			break;
		case Foxjet3d::PrintReportDlg::CPrintReportDlg::RESTART:
			bShow = true;
			continue;
		}
	}
}

#include <Dbt.h>

CString ToString (const GUID & guid)
{
	CString str;

	str.Format (_T ("%04X:%04X:%04X:%04X"), guid.Data1, guid.Data2, guid.Data3, guid.Data4);

	if (IsEqualGUID(WPD_CONTENT_TYPE_FOLDER, guid))
		str = _T ("WPD_CONTENT_TYPE_FOLDER: ") + str;

	return str;
}


::CDeviceList CMainFrame::EnumDevices (const GUID & guid)
{
	::CDeviceList result;
	SP_DEVICE_INTERFACE_DATA         DevIntfData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA DevIntfDetailData;
	SP_DEVINFO_DATA                  DevData;

	DWORD dwSize, dwType, dwMemberIdx;
	HKEY hKey;
	BYTE lpData[1024];

	HDEVINFO hDevInfo = SetupDiGetClassDevs (&guid, NULL, 0, DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);
	
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		DevIntfData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		dwMemberIdx = 0;
		
		SetupDiEnumDeviceInterfaces(hDevInfo, NULL, &guid, dwMemberIdx, &DevIntfData);

		while(GetLastError() != ERROR_NO_MORE_ITEMS)
		{

			DevData.cbSize = sizeof(DevData);
			SetupDiGetDeviceInterfaceDetail(hDevInfo, &DevIntfData, NULL, 0, &dwSize, NULL);

			DevIntfDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwSize);
			DevIntfDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

			if (SetupDiGetDeviceInterfaceDetail(hDevInfo, &DevIntfData, DevIntfDetailData, dwSize, &dwSize, &DevData)) {
				CDevice d (&DevData, &DevIntfData, DevIntfDetailData);
				TCHAR sz [MAX_PATH];
				bool bDuplicate = false;

				memset (sz, 0, ARRAYSIZE (sz) * sizeof (sz [0]));
				::SetupDiGetDeviceRegistryProperty (hDevInfo, &DevData, SPDRP_FRIENDLYNAME, NULL, (PBYTE)sz, ARRAYSIZE (sz), NULL);
				d.m_strFriendlyName = sz;

				memset (sz, 0, ARRAYSIZE (sz) * sizeof (sz [0]));
				::SetupDiGetDeviceRegistryProperty (hDevInfo, &DevData, SPDRP_DEVICEDESC, NULL, (PBYTE)sz, ARRAYSIZE (sz), NULL);
				d.m_strDeviceDescription = sz;

				memset (sz, 0, ARRAYSIZE (sz) * sizeof (sz [0]));
				::SetupDiGetDeviceRegistryProperty (hDevInfo, &DevData, SPDRP_LOCATION_INFORMATION, NULL, (PBYTE)sz, ARRAYSIZE (sz), NULL);
				d.m_strLocationInformation = sz;

				for (int i = 0; !bDuplicate && i < result.GetSize (); i++) 
					bDuplicate = _tcscmp (d.m_interfaceDetail.DevicePath, result [i].m_interfaceDetail.DevicePath) == 0;

				if (!bDuplicate)
					result.Add (d);
			}

			HeapFree(GetProcessHeap(), 0, DevIntfDetailData);

			// Continue looping
			SetupDiEnumDeviceInterfaces (hDevInfo, NULL, &guid, ++dwMemberIdx, &DevIntfData);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}

	#ifdef _DEBUG
	for (int i = 0; i < result.GetSize (); i++) {
		CString str;

		str.Format (_T ("[%03d] %-40s: %s"),
			i,
			result [i].m_strDeviceDescription,
			result [i].m_interfaceDetail.DevicePath);
		TRACEF (str);
	}
	#endif //_DEBUG

	return result;
}

int CMainFrame::Find (const ::CDeviceList & v, const CDevice & find)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i] == find)
			return i;

	return -1;
}

LRESULT CMainFrame::OnDeviceChange (WPARAM wParam, LPARAM lParam)
{
	switch (wParam) {
	case DBT_CONFIGCHANGECANCELED:		TRACEF (_T ("DBT_CONFIGCHANGECANCELED:		"));	break;
	case DBT_CONFIGCHANGED:				TRACEF (_T ("DBT_CONFIGCHANGED:				"));	break;
	case DBT_CUSTOMEVENT:				TRACEF (_T ("DBT_CUSTOMEVENT:				"));	break;
	case DBT_DEVICEARRIVAL:		
		{
			TRACEF (_T ("DBT_DEVICEARRIVAL:				"));	

			bool bLogicalDrive = false;

			if (DEV_BROADCAST_HDR * p = (DEV_BROADCAST_HDR *)lParam) {
				CString str;

				str.Format (_T ("%d [0x%p], %d [0x%p], %d [0x%p]"), 
					p->dbch_devicetype, p->dbch_devicetype, 
					p->dbch_size,		p->dbch_size, 
					p->dbch_reserved,	p->dbch_reserved);
				TRACEF (str);

				switch (p->dbch_devicetype) {
				case DBT_DEVTYP_DEVICEINTERFACE:	
					TRACEF (_T ("DBT_DEVTYP_DEVICEINTERFACE:	"));		
					if (DEV_BROADCAST_DEVICEINTERFACE * pInfo = (DEV_BROADCAST_DEVICEINTERFACE *)p) {
						TRACEF (ToString (pInfo->dbcc_classguid) + _T (": ") + CString (pInfo->dbcc_name));
					}
					break;
				case DBT_DEVTYP_HANDLE:	
					TRACEF (_T ("DBT_DEVTYP_HANDLE:				"));
					if (DEV_BROADCAST_HANDLE * pInfo = (DEV_BROADCAST_HANDLE *)p) {
						str.Format (_T ("0x%p, 0x%p, %s, 0x%p"), 
							pInfo->dbch_handle,
							pInfo->dbch_hdevnotify,
							ToString (pInfo->dbch_eventguid),
							pInfo->dbch_nameoffset);
						TRACEF (str);
					}
					break;
				case DBT_DEVTYP_OEM:
					TRACEF (_T ("DBT_DEVTYP_OEM:				"));	
					if (DEV_BROADCAST_OEM * pInfo = (DEV_BROADCAST_OEM *)p) {
						str.Format (_T ("0x%p, 0x%p"),
							pInfo->dbco_identifier,
							pInfo->dbco_suppfunc);
						TRACEF (str);
					}
					break;
				case DBT_DEVTYP_PORT:	
					TRACEF (_T ("DBT_DEVTYP_PORT:				"));		
					if (DEV_BROADCAST_PORT * pInfo = (DEV_BROADCAST_PORT *)p) {
						TRACEF (pInfo->dbcp_name);
						//m_pControlView->UpdateSerialData (_T ("DEVICE ARRIVAL: ") + CString (pInfo->dbcp_name));
					}
					break;
				case DBT_DEVTYP_VOLUME:	
					TRACEF (_T ("DBT_DEVTYP_VOLUME:				"));
					if (DEV_BROADCAST_VOLUME * pInfo = (DEV_BROADCAST_VOLUME *)p) {
						str.Format (_T ("0x%p, 0x%p"),
							pInfo->dbcv_unitmask,
							pInfo->dbcv_flags);
						TRACEF (str);

						for (int i = 0; i < sizeof (DWORD) * 8; i++) {
							if (pInfo->dbcv_unitmask & (1 << i)) {
								m_pControlView->UpdateSerialData (_T ("DEVICE ARRIVAL: ") + CString ((TCHAR)((int)'A' + i)));
								bLogicalDrive = true;
								m_pControlView->SendMessage (WM_CANCELMODE);
								CNewDeviceDlg (CString ((TCHAR)((int)'A' + i)) + _T (":\\"), _T (""), this).DoModal ();
								break;
							}
						}
					}
					break;
				}
			}

			if (!bLogicalDrive) {
				::CDeviceList v = EnumDevices (GUID_DEVINTERFACE_WPD);

				for (int i = 0; i < v.GetSize (); i++) {
					if (CMainFrame::Find (m_vPortableDevices, v [i]) == -1) {
						CString strDevice = v [i].m_interfaceDetail.DevicePath;
						CString strDesc = v [i].m_strDeviceDescription;

						m_vPortableDevices = v;
						TRACEF (strDesc + _T (": ") + strDevice);
						m_pControlView->UpdateSerialData (_T ("DEVICE ARRIVAL: ") + strDesc);
						m_pControlView->SendMessage (WM_CANCELMODE);
						CNewDeviceDlg (strDesc, strDevice, this).DoModal ();
					}
				}
				
				m_vPortableDevices = v;
			}
		}
		break;
	case DBT_DEVICEQUERYREMOVE:			TRACEF (_T ("DBT_DEVICEQUERYREMOVE:			"));	break;
	case DBT_DEVICEQUERYREMOVEFAILED:	TRACEF (_T ("DBT_DEVICEQUERYREMOVEFAILED:	"));	break;
	case DBT_DEVICEREMOVECOMPLETE:		
		TRACEF (_T ("DBT_DEVICEREMOVECOMPLETE:		"));	
		if (DEV_BROADCAST_HDR * p = (DEV_BROADCAST_HDR *)lParam) {
			m_vPortableDevices = EnumDevices (GUID_DEVINTERFACE_WPD);

			switch (p->dbch_devicetype) {
			case DBT_DEVTYP_PORT:	
				if (DEV_BROADCAST_PORT * pInfo = (DEV_BROADCAST_PORT *)p) {
					TRACEF (pInfo->dbcp_name);
					//m_pControlView->UpdateSerialData (_T ("DEVICE REMOVED: ") + CString (pInfo->dbcp_name));
				}
				break;
			}
		}
		break;
	case DBT_DEVICEREMOVEPENDING:		TRACEF (_T ("DBT_DEVICEREMOVEPENDING:		"));	break;
	case DBT_DEVICETYPESPECIFIC:		TRACEF (_T ("DBT_DEVICETYPESPECIFIC:		"));	break;
	case DBT_DEVNODES_CHANGED:
		TRACEF (_T ("DBT_DEVNODES_CHANGED:			"));	
		{
			::CDeviceList v = EnumDevices ();
			
			for (int i = 0; i < v.GetSize (); i++) {
				if (CMainFrame::Find (m_vPortableDevices, v [i]) == -1) {
					if (!v [i].m_strDeviceDescription.CompareNoCase (_T ("Apple iPhone"))) {
						m_vUSB = v;
						// TODO // CopyToPortableDevice (v [i].m_interfaceDetail.DevicePath, _T ("TODO"), _T ("TODO"));
						break;
					}
				}
			}

			m_vUSB = v;
		}
		break;
	case DBT_QUERYCHANGECONFIG:			TRACEF (_T ("DBT_QUERYCHANGECONFIG:			"));	break;
	case DBT_USERDEFINED:				TRACEF (_T ("DBT_USERDEFINED:				"));	break;
	}

	return TRUE;
}

LRESULT CMainFrame::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	switch (message) {
	case WM_DEVICECHANGE:
		OnDeviceChange (wParam, lParam);
		break;
	case WM_CLOSE:
	case ID_APP_EXIT:
		bool bRegisteredMsg = (wParam == 1) ? true : false;

		if (!bRegisteredMsg) {
			bool bPrinting = false;
			CMapStringToPtr v;

			ASSERT (m_pControlView);
			CControlDoc * pDoc = m_pControlView->GetDocument();
			ASSERT (pDoc);

			pDoc->GetProductLines (v);

			if (!theApp.LoginPermits (ID_APP_EXIT)) {
				CString str;
				OPTIONSSTRUCT os;
				CDbConnection conn (_T (__FILE__), __LINE__); 

				if (!conn.OpenExclusive (this))
					return 1;

				GetOptionsRecord (theApp.m_Database, ID_APP_EXIT, os);

				str.Format (LoadString (IDS_NOLOGOUT), os.m_strOptionDesc);
				MsgBox (* this, str, NULL, MB_ICONINFORMATION);

				return 1;
			}

			for (POSITION pos = v.GetStartPosition(); pos; ) {
				CString str;
				CProdLine * pLine = NULL;

				v.GetNextAssoc (pos, str, (void *&)pLine);

				ASSERT (pLine);
				str = pLine->GetCurrentTask ();

				if (str.GetLength ()) {
					bPrinting = true;
					break;
				}
			}

			#ifndef _DEBUG
			if (!theApp.m_bShutdownTest)
				if (MsgBox (* this, LoadString (bPrinting ? IDS_EXITCONFIRM : IDS_EXITCONFIRM2), NULL, MB_YESNO) == IDNO)
					return 1;
			#endif

		}

		if (CWnd * p = m_pControlView->GetDlgItem (TAB_LINE))
			p->ShowWindow (SW_HIDE);
		
		break;
	}
	
	switch (message) {
	case WM_CLOSE:
		if (theApp.IsPrintReportLoggingEnabled ()) {
			CDbConnection conn (_T (__FILE__), __LINE__); 

			if (conn.OpenExclusive (this)) {
				REPORTSTRUCT s;

				s.m_strUsername.Format (_T ("CMainFrame::WindowProc::WM_CLOSE (%d, %d)"), wParam, lParam);
				s.m_dtTime = COleDateTime::GetCurrentTime ();
				s.m_strTaskName = s.m_dtTime.Format (_T ("%c"));

				FoxjetDatabase::AddPrinterReportRecord (theApp.m_Database, FoxjetDatabase::REPORT_EXECSTOP, s, GetPrinterID (theApp.m_Database));
			}
		}
		break;
	case ID_APP_EXIT:
		if (theApp.IsPrintReportLoggingEnabled ()) {
			CDbConnection conn (_T (__FILE__), __LINE__); 

			if (conn.OpenExclusive (this)) {
				REPORTSTRUCT s;

				s.m_strUsername.Format (_T ("CMainFrame::WindowProc::ID_APP_EXIT (%d, %d)"), wParam, lParam);
				s.m_dtTime = COleDateTime::GetCurrentTime ();
				s.m_strTaskName = s.m_dtTime.Format (_T ("%c"));

				FoxjetDatabase::AddPrinterReportRecord (theApp.m_Database, FoxjetDatabase::REPORT_EXECSTOP, s, GetPrinterID (theApp.m_Database));
			}
		}
		break;
	}

	return CFrameWnd::WindowProc(message, wParam, lParam);
}

LRESULT CMainFrame::OnIsAppRunning (WPARAM wParam, LPARAM lParam)
{
	if (!wParam)
		SetForegroundWindow ();

	return (LRESULT)m_hWnd;
}


LRESULT CMainFrame::OnDisplayMsg (WPARAM wParam, LPARAM lParam)
{
	if (CString * p = (CString *)wParam) {
		MsgBox (* this, * p);
		delete p;
	}

	return WM_DISPLAY_MSG;
}


LRESULT CMainFrame::OnShutdown (WPARAM wParam, LPARAM lParam)
{
	using namespace FoxjetDatabase;

	if ((wParam & SHUTDOWN_CONTROL) || (!wParam)) {
		CStringArray v;
		CString str = ::GetCommandLine ();
		
		TokenizeCmdLine (str, v);

		if (v.GetSize ()) {
			CString strExe = v [0];
			
			str.Delete (0, strExe.GetLength ());
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER,
				_T ("Software\\FoxJet\\Translator\\Cmd line"), 
				_T ("Control"), str); // for Translator
		}

		if (theApp.IsPrintReportLoggingEnabled ()) {
			CDbConnection conn (_T (__FILE__), __LINE__); 

			if (conn.OpenExclusive (this)) {
				REPORTSTRUCT s;

				s.m_strUsername.Format (_T ("CMainFrame::OnShutdown::WM_SHUTDOWNAPP (%d, %d)"), wParam, lParam);
				s.m_dtTime = COleDateTime::GetCurrentTime ();
				s.m_strTaskName = s.m_dtTime.Format (_T ("%c"));

				FoxjetDatabase::AddPrinterReportRecord (theApp.m_Database, FoxjetDatabase::REPORT_EXECSTOP, s, GetPrinterID (theApp.m_Database));
			}
		}

		theApp.m_bRootUser = true;
		PostMessage (WM_CLOSE, 1, 0);
		//exit (0);
	}

	return WM_SHUTDOWNAPP;
}

LRESULT CMainFrame::OnDbStartParamsChanged (WPARAM wParam, LPARAM lParam)
{
	CControlApp::LoadDbStartParams ();

	return WM_DBSTARTCONFIGCHANGED;
}

#pragma data_seg( "ping" )
	namespace Ping
	{
		extern CRITICAL_SECTION csPing;
		extern TCHAR szPing [MAX_PATH];
		extern FoxjetCommon::CStrobeItem errorPing;
		extern bool bTrigger;
		extern bool bEnabled;
	};
#pragma

LRESULT CMainFrame::OnSystemParamsChanged (WPARAM wParam, LPARAM lParam)
{
	CDbConnection conn (_T (__FILE__), __LINE__); 

	if (!conn.IsOpen ()) 
		return WM_SYSTEMPARAMCHANGE;

//	if ((lParam == 0) || (lParam == (LPARAM)::AfxGetMainWnd ()->m_hWnd)) {
		const bool bValve = ISVALVE ();
		FoxjetDatabase::COdbcDatabase & db = theApp.m_Database;
		const FoxjetCommon::CVersion ver = GlobalVars::CurrentVersion;
		const bool bLocalDB = IsNetworked () && theApp.GetLocalDSN ().GetLength () ? true : false;

		ASSERT (m_pControlView);
		CControlDoc * pDoc = m_pControlView->GetDocument();
		ASSERT (pDoc);

		TRACEF ("OnSystemParamsChanged");

		if (FoxjetDatabase::Encrypted::InkCodes::IsEnabled ())
			m_pControlView->SendMessage (WM_INK_CODE_CHANGE, (WPARAM)new ControlView::WM_INK_CODE_CHANGE_STRUCT (IDOK, 0, true));

		FoxjetCommon::LoadSystemParams (db, ver);
		CSerialPacket::Init (); 


		m_pControlView->EndCommThreads ();
		if (bValve)		theApp.EndIdsThread ();

		m_pControlView->BeginCommThreads ();
		if (bValve)		theApp.BeginIdsThread ();

		m_pControlView->HeadCfgUpdated ();

		{
			CStringArray vLines;

			pDoc->GetProductLines (vLines);

			for (int i = 0; i < vLines.GetSize (); i++) {
				if (CProdLine * p = pDoc->GetProductionLine (vLines [i])) {
					p->m_vStates.RemoveAll ();
					FoxjetCommon::CStrobeDlg::Load (theApp.m_Database, p->m_vStates);
				}
			}
		}

		theApp.CacheHubState ();

		{ // sw0883
			SETTINGSSTRUCT s;
			bool bContinuousPC = false;

			if (GetSettingsRecord (theApp.m_Database, 0, _T ("sw0883"), s)) {
				CStringArray v;

				FoxjetDatabase::FromString (s.m_strData, v);

				if (v.GetSize () >= 1) bContinuousPC = _ttoi (v [0]) ? true : false;
			}

			theApp.SetContinuousCount (bContinuousPC);
		}

		{
			FoxjetUtils::CNetworkDlg dlg;

			dlg.Load (db);

			if (IsNetworked ()) {
				::EnterCriticalSection (&Ping::csPing);

				_tcsncpy (Ping::szPing, dlg.m_bEnabled ? dlg.m_strAddr : _T (""), ARRAYSIZE (Ping::szPing));
				Ping::errorPing.m_nDelay = dlg.m_nFreq;

				if (dlg.m_bEnabled) 
					Ping::bTrigger = true;

				::LeaveCriticalSection (&Ping::csPing);
			}
		}
//	}

	_flushall ();

	return WM_SYSTEMPARAMCHANGE;
}

void CMainFrame::OnChangecounts() 
{
	CControlView *pView = m_pControlView;
	pView->ChangeCounts ();
}

void CMainFrame::OnHelpTranslate() 
{
	const CString strApp = _T ("Translator.exe");
	CString strPath = FoxjetDatabase::GetHomeDir () + _T ("\\") +  strApp;
	TCHAR szPath [MAX_PATH] = { 0 };

	HMODULE hModule = ::GetModuleHandle (NULL);
	::GetModuleFileName (hModule, szPath, ARRAYSIZE (szPath) - 1);

	if (::GetFileAttributes (strPath) == -1) {
		CFileDialog dlg (TRUE, strApp, strPath, 
			OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
			_T ("Applications (*.exe)|*.exe|All files (*.*)|*.*||"), this);

		if (dlg.DoModal () == IDOK) 
			strPath = dlg.GetPathName ();
		else
			return;
	}

	CString strCmdLine = strPath + _T (" \"") + szPath + _T ("\"") + CString (FoxjetDatabase::IsMatrix () ? _T (" /matrix") : _T (""));
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [0xFF];

	_tcsncpy (szCmdLine, strCmdLine, 0xFF);
    memset (&pi, 0, sizeof(pi));
    memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	if (!::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
		MsgBox (* this, LoadString (IDS_FAILEDTOLANUCHTRANSLATOR));
}

/* TODO: rem
void CMainFrame::OnUpdateHelpTranslate(CCmdUI* pCmdUI) 
{
	static bool bEnable = false;
	static bool bInit = false;

	if (!bInit) {
		const CString strApp = _T ("Translator.exe");
		CString strPath = FoxjetDatabase::GetHomeDir () + _T ("\\") +  strApp;

		bEnable = ::GetFileAttributes (strPath) != -1;
		bInit = true;
	}
	
	pCmdUI->Enable (bEnable && theApp.LoginPermits (pCmdUI->m_nID)); // sw0866
}

void CMainFrame::OnInitMenu(CMenu* pMenu) 
{
	const bool bValve = ISVALVE ();

	CFrameWnd::OnInitMenu(pMenu);

#ifdef __WINPRINTER__
	pMenu->DeleteMenu (IDM_CFG_DYNAMICDATA, MF_BYCOMMAND);
#endif

//	if (!theApp.Is0848 ())
//		pMenu->DeleteMenu (ID_CONFIGURE_SYSTEM_OUTPUTTABLE, MF_BYCOMMAND);

#ifdef __IPPRINTER__
	pMenu->DeleteMenu (IDM_OPER_DBSTART, MF_BYCOMMAND);
	pMenu->DeleteMenu (ID_CONFIGURE_DATABASE, MF_BYCOMMAND);
	pMenu->DeleteMenu (ID_CONFIGURE_SYSTEM_OUTPUTTABLE, MF_BYCOMMAND);
#endif
	
	if (!theApp.Is0835 ())
		pMenu->DeleteMenu (ID_CONFIGURE_SERIALDATAFORMAT, MF_BYCOMMAND);

	if (bValve) {
		pMenu->DeleteMenu (IDM_CFG_BARCODE_SETTINGS, MF_BYCOMMAND);
	}

	pMenu->DeleteMenu (IDM_SELECT_EFFECTIVITY_DATE, MF_BYCOMMAND);
	pMenu->DeleteMenu (ID_CONFIGURE_PRINT, MF_BYCOMMAND);
}
*/

void CMainFrame::OnCfgDynamicdata() 
{	
#ifdef __IPPRINTER__
	if (FoxjetIpElements::OnDefineDynamic (this, FoxjetCommon::CVersion ())) 
		::PostMessage (HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, 0);
#endif //__IPPRINTER__
}

static void CALLBACK FormatDateTime (CString & str)
{
	#ifdef __WINPRINTER__
	str = FoxjetElements::CDateTimeElement::Format (-1, str, 0, NULL);
	#endif
}

void CMainFrame::OnCfgGeneral ()
{
	using namespace FoxjetCommon;

	CCopyArray <DWORD, DWORD> vCommChanged;

	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

	CString strKey = ListGlobals::defElements.m_strRegSection;

	if (FoxjetCommon::OnSystemConfig (this, strKey, theApp.m_Database, FormatDateTime, vCommChanged)) {
		{
			CUIntArray v;

			FoxjetCommon::EnumerateSerialPorts (v);

			if (v.GetSize () > 0) {
				for (int i = 0; i < vCommChanged.GetSize (); i++) {
					if (vCommChanged [i] == v [0]) {
						NEXT::CNextThread::ApplySw0923CommFix ();
						break;
					}
				}
			}
		}

		PostMessage (WM_COMMAND, DOC_CMD_LINES_UPDATED, 0L);
		::PostMessage (HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, 0);

		#ifdef __WINPRINTER__
		if (CIdsThread * p = theApp.GetIdsThread ())
			p->LoadAddress (theApp.m_Database);
		#endif //__WINPRINTER__
	}
}

LRESULT CMainFrame::OnSyncRemoteHead (WPARAM wParam, LPARAM lParam)
{
	LRESULT lResult = 0;

#ifdef __IPPRINTER__

	using FoxjetIpElements::CSyncDlg;

	ULONG lHeadID = wParam;
	DWORD dwFlags = lParam;
	CStringArray vLines;

	lResult = WM_SYNCREMOTEHEAD;

	if (CControlDoc * pDoc = (CControlDoc *) GetActiveDocument ()) {
		pDoc->GetProductLines (vLines);

		for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
			CString strLine = vLines [nLine];

			if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
				CPrintHeadList list;

				pLine->GetActiveHeads (list);

				for (POSITION pos = list.GetStartPosition(); pos; ) {
					DWORD dwKey;
					CHead * pHead = NULL;

					list.GetNextAssoc(pos, dwKey, (void *&)pHead);
					ASSERT (pHead);
				
					if (CRemoteHead * pRemote = DYNAMIC_DOWNCAST (CRemoteHead, pHead)) {
						CHeadInfo info = CHead::GetInfo (pRemote);
						bool bUpdate = 
							(info.m_Config.m_HeadSettings.m_lID == lHeadID) ||
							(lHeadID == -1);

						if (bUpdate) {						
							while (!PostThreadMessage (pRemote->m_nThreadID, TM_SYNC_LABEL_STORE, 1, dwFlags)) 
								::Sleep (0);
						}
					}
				}
			}
		}
	}

#endif //__IPPRINTER__

	return lResult;
}

LRESULT CMainFrame::OnResetScannerInfo (WPARAM wParam, LPARAM lParam)
{
	CControlView * pView = (CControlView *)GetActiveView (); 

	ASSERT (pView);

	theApp.CancelStrobe ();
	pView->ResetScannerInfo ();

	return FoxjetUtils::WM_RESETSCANNERINFO;
}

LRESULT CMainFrame::OnResetScannerError (WPARAM wParam, LPARAM lParam)
{
	theApp.CancelStrobe ();

	return FoxjetUtils::WM_RESETSCANNERERROR;
}


void CMainFrame::OnViewPreview() 
{
	m_pControlView->SetPreview (!m_pControlView->GetPreview ());
}

void CMainFrame::OnClose() 
{
	m_bClosed = true;

	if (HANDLE h = m_hCacheThread) {
		m_hCacheThread = NULL;
	
		if (::WaitForSingleObject (h, 10000) != WAIT_OBJECT_0) {
			TRACEF ("Timeout waiting for thread");
		}
	}

	CFrameWnd::OnClose();
}


LRESULT CMainFrame::OnGetPrinterWnd (WPARAM wParam, LPARAM lParam)
{
	return (LRESULT)m_hWnd;
}

LRESULT CMainFrame::OnOpenMapFile (WPARAM wParam, LPARAM lParam)
{
	OnCloseMapFile (0, 0);

	m_hMapFile = ::OpenFileMapping (FILE_MAP_WRITE, FALSE, COMMAPPEDFILE);
	
	if (m_hMapFile) {
		m_lpMapAddress = ::MapViewOfFile (m_hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, 0);

		if (m_lpMapAddress) {
			theApp.m_nInterfaceThreadID = (DWORD)lParam;
		
			return TRUE;
		}
	}

	return FALSE;
}

LRESULT CMainFrame::OnCloseMapFile (WPARAM wParam, LPARAM lParam)
{
	if (m_lpMapAddress) {
		VERIFY (::UnmapViewOfFile (m_lpMapAddress));
		m_lpMapAddress = NULL;
	}

	if (m_hMapFile) {
		VERIFY (::CloseHandle (m_hMapFile));
		m_hMapFile = NULL;
	}

	theApp.m_nInterfaceThreadID = 0;

	return TRUE;
}

LRESULT CMainFrame::OnMappedCommand (WPARAM wParam, LPARAM lParam)
{
	CString str (a2w ((LPCSTR)m_lpMapAddress));
	CString	strResult = CServerThread::ProcessCommand (str, m_pControlView);

	if (theApp.m_pMainWnd)
		((CControlView * )(((CMainFrame *)theApp.m_pMainWnd)->m_pControlView))->UpdateSerialData (str + _T (" > ") + strResult, false);

	CopyToMapFile (m_lpMapAddress, w2a (strResult));

	return TRUE;
}


void CMainFrame::OnTimeChange() 
{
	static CTime tmLast = CTime::GetCurrentTime () - CTimeSpan (0, 1, 0, 0);

	CFrameWnd::OnTimeChange();
	
	/*
	CTimeSpan tmDiff = CTime::GetCurrentTime () - tmLast;

	if (tmDiff.GetTotalSeconds ()) {
		tmLast = CTime::GetCurrentTime ();
		SETTINGSSTRUCT s;
		bool bLoggingEnabled = true;

		if (theApp.IsPrintReportLoggingEnabled ()) {
			REPORTSTRUCT s;

			s.m_strUsername = theApp.m_strUsername;
			s.m_dtTime		= COleDateTime::GetCurrentTime ();
			
			FoxjetDatabase::AddPrinterReportRecord (theApp.m_Database, FoxjetDatabase::REPORT_TIMECHANGE, s, GetPrinterID (theApp.m_Database));
		}

		if (FoxjetCommon::CTimeChangeDlg::IsTimeChangeMessageEnabled (theApp.m_Database))
			MsgBox (* this, LoadString (IDS_TIMECHANGE));
	}
	*/
}

/* TODO: rem
void CMainFrame::OnUpdateConfigureSystemOutputtable (CCmdUI* pCmdUI) 
{
	pCmdUI->Enable (theApp.LoginPermits (pCmdUI->m_nID));
}
*/

void CMainFrame::OnConfigureSystemOutputtable() // sw0848
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

	Sw0848Dlg::CSw0848Dlg dlg (this);

	if (dlg.DoModal () == IDOK) {
	}	
}

void CMainFrame::OnZoom ()
{
	using namespace FoxjetCommon;

	double dZoom = m_pControlView->GetZoom ();
	ZOOMSTRUCT zs = { (int)(CImageView::m_dZoomMin * 100.0), (int)(CImageView::m_dZoomMax * 100.0), (int)(dZoom * 100.0), (LPVOID)this };
	CZoomDlg dlg (ZoomFct, &zs, this);

	dlg.m_nZoom = (int)(dZoom * 100.0);

	if (dlg.DoModal () == IDOK) 
		m_pControlView->SetZoom ((double)dlg.m_nZoom / 100.0);
}

bool CALLBACK CMainFrame::ZoomFct (FoxjetCommon::ZOOMSTRUCT & zs)
{
/*
	CBaseView * pView = (CBaseView *)zs.m_lpData;
	ASSERT (pView);

	if (pView->SetZoom ((double)zs.m_nZoom / 100.0), true)
		return true;
	else {
		zs.m_nZoom = (int)(pView->GetZoom () * 100.0);
		return false;
	}
*/

	return true;
}

void CMainFrame::OnConfigureSystemStrobe() 
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

	COdbcDatabase & db = theApp.m_Database;

	CControlView *pView = m_pControlView;
	CControlDoc *pDoc = pView->GetDocument();
	FoxjetCommon::CStrobeDlg dlg (db, ListGlobals::defElements.m_strRegSection, this);

	if (dlg.DoModal () == IDOK) {
		CStringList list;

		pDoc->GetProductLines (list);

		while (!list.IsEmpty ()) {
			if (CProdLine * pLine = pDoc->GetProductionLine (list.RemoveHead ())) {
				dlg.Load (db, pLine->m_vStates);
				//pLine->HeadCfgUpdated (); 
			}
		}

		::PostMessage (HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, 0);
	}
}


void CMainFrame::OnCfgBaxter ()
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

	CBaxterConfigDlg dlg (this);

	dlg.Load ();

	if (dlg.DoModal () == IDOK) 
		dlg.Save ();
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs) 
{
	#ifdef _DEBUG
	if (FoxjetDatabase::IsDebugDisplayAttached ()) {
		cs.x = m_ptDebug.x;
		cs.y = m_ptDebug.y;
	}
	#endif

	cs.hMenu = NULL;	
	cs.style =
		WS_CAPTION |
		WS_DLGFRAME | 
		WS_SYSMENU;

	#ifdef _DEBUG
	TRACEF (GetStyleString (cs.style, cs.dwExStyle));
	#endif

	return CFrameWnd::PreCreateWindow(cs);
}


void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
	CFrameWnd::OnSize(nType, cx, cy);
}

void CMainFrame::OnConfigureDatabase() 
{
	DBCONNECTION (conn);

	if (!conn.OpenExclusive (this))
		return;

	FoxjetUtils::CDatabaseStartDlg dlg (this);

	dlg.Load (theApp.m_Database);

	BEGIN_TRANS (theApp.m_Database);

	if (dlg.DoModal () == IDOK) {
		CMainFrame * pFrame = (CMainFrame *)theApp.m_pMainWnd;

		dlg.Save (theApp.m_Database);

		#ifdef __IPPRINTER__
		pFrame->m_wndToolBar.LoadToolBar (pFrame->GetToolbarResID ());
		#endif

		theApp.UpdateUI ();
		COMMIT_TRANS (theApp.m_Database);
	}
	else
		ROLLBACK_TRANS (theApp.m_Database);
}

void CMainFrame::OnMoving(UINT fwSide, LPRECT pRect) 
{
//	CFrameWnd::OnMoving(fwSide, pRect);
	
	// TODO: Add your message handler code here
	
}

void CMainFrame::OnMove(int x, int y) 
{
	CFrameWnd::OnMove(x, y);

//	if (!theApp.m_bDemo)
//		if (CWnd * p = ::AfxGetMainWnd ())
//			p->SetWindowPos (NULL, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);
}

LRESULT CMainFrame::OnNcHitTest(CPoint point) 
{
	UINT nHit = CFrameWnd::OnNcHitTest(point);

	//TRACEF (ToString ((int)nHit));

	if (!theApp.IsPC () && nHit != HTCLOSE)
		nHit = HTNOWHERE;

	return nHit;
}

BOOL CMainFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle, CWnd* pParentWnd, CCreateContext* pContext) 
{
	BOOL bResult = CFrameWnd::LoadFrame (nIDResource, dwDefaultStyle, pParentWnd, pContext);

	ModifyStyle (WS_CAPTION | WS_DLGFRAME | WS_BORDER, 0, SWP_FRAMECHANGED);

	#ifdef _DEBUG
	if (!FoxjetDatabase::IsDebugDisplayAttached ())
		ModifyStyle (0, WS_CAPTION, SWP_FRAMECHANGED);
	#endif

	return bResult;
}

void CMainFrame::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CFrameWnd::OnShowWindow(bShow, nStatus);
}

void CMainFrame::OnSetFocus(CWnd* pOldWnd) 
{
	CFrameWnd::OnSetFocus(pOldWnd);

	{ 
		CString str, strTmp = _T ("NULL");
		
		if (pOldWnd) {
			TCHAR sz [512] = { 0 };

			pOldWnd->GetWindowText (sz, ARRAYSIZE (sz));
			strTmp.Format (_T ("0x%p [%s] [%s]"), pOldWnd->m_hWnd, sz, GetClassName (pOldWnd->m_hWnd));
		}
		
		str.Format (_T ("%s(%d): OnSetFocus (%s)\n"), _T (__FILE__), __LINE__, strTmp); 
		//TRACER (str); 
	}
}

LRESULT CMainFrame::OnLocalDbBackup (WPARAM wParam, LPARAM lParam)
{
	return m_pControlView->OnLocalDbBackup (wParam, lParam);
}


LRESULT CMainFrame::OnDatabaseConnectionLost (WPARAM wParam, LPARAM lParam)
{
	if (theApp.m_Database.IsOpen ()) {
		TRACEF ("OnDatabaseConnectionLost: m_Database.Close ()");
		theApp.m_Database.Close ();
		COdbcDatabase::FreeDsnCache ();
	}

	theApp.ReconnectDatabase ();

	return theApp.m_Database.IsOpen ();
}


LRESULT CMainFrame::OnDebug (WPARAM wParam, LPARAM lParam)
{
	if (CString * p = (CString *)wParam) {
		TRACER (* p);
		delete p;
	}

	return 0;
}

LRESULT CMainFrame::OnKbChange (WPARAM wParam, LPARAM lParam)
{
	//{ CString str; str.Format (_T ("OnKbChange (%d, %d)"), wParam, lParam); TRACEF (str); }

	if (wParam == KBCHANGETYPE_DOCKSTATE) {
		if (CControlView * pView = (CControlView *)theApp.GetActiveView ()) 
			while (!pView->PostMessage (WM_TIMER, (WPARAM)TIMER_STARTMENU, 0))
				::Sleep (0);
	}

	return 0;
}


LRESULT CMainFrame::OnSuspendCommThreads (WPARAM wParam, LPARAM lParam)
{
	if (wParam) {
		UINT nElapse = BindTo <UINT> (lParam, 1000, 10 * 60 * 1000);

		SetTimer (ID_RESUME_COMM_THREADS, nElapse, NULL);
		m_pControlView->EndCommThreads ();
	}
	else {
		KillTimer (ID_RESUME_COMM_THREADS);
		m_pControlView->EndCommThreads ();
		m_pControlView->BeginCommThreads ();
	}
	
	return 0;
}

void CMainFrame::OnTimer(UINT nIDEvent) 
{
	switch (nIDEvent) {
	case ID_RESUME_COMM_THREADS:
		m_pControlView->EndCommThreads ();
		m_pControlView->BeginCommThreads ();
		KillTimer (ID_RESUME_COMM_THREADS);
		break;
	}

	CFrameWnd::OnTimer(nIDEvent);
}

LRESULT CMainFrame::OnDebugEnumRegisteredThreads (WPARAM wParam, LPARAM lParam)
{
	CDiagnosticSingleLock::EnumRegisteredThreads ();
	return 0;
}

