// DebugPage.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "DebugPage.h"
#include "Debug.h"
#include "AppVer.h"
#include "Sw0848Thread.h"
#include "ProdLine.h"

#ifdef __WINPRINTER__
	#include "LocalHead.h"
#endif //__WINPRINTER__

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using FoxjetDatabase::HEADERROR_LOWTEMP;
using FoxjetDatabase::HEADERROR_HIGHVOLTAGE;
using FoxjetDatabase::HEADERROR_LOWINK;
using FoxjetDatabase::HEADERROR_OUTOFINK;

static bool IsSignaled (HANDLE h)
{
	DWORD dw = ::WaitForSingleObject (h, 0);
	return dw == WAIT_OBJECT_0;
}

/////////////////////////////////////////////////////////////////////////////
// CDebugPage property page

IMPLEMENT_DYNCREATE(CDebugPage, CPropertyPage)

CDebugPage::CDebugPage() 
:	m_hbrDlg (NULL),
	CPropertyPage(CDebugPage::IDD)
{
	//{{AFX_DATA_INIT(CDebugPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CDebugPage::~CDebugPage()
{
	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}
}

void CDebugPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDebugPage)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDebugPage, CPropertyPage)
	//{{AFX_MSG_MAP(CDebugPage)
	ON_BN_CLICKED(BTN_CLIPBOARD, OnClipboard)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDebugPage message handlers

void CDebugPage::OnClipboard() 
{

	CString str;

	GetDlgItemText (TXT_DEBUG, str);

	VERIFY (::OpenClipboard (NULL));
	::EmptyClipboard ();

	HGLOBAL hClipboardText = ::GlobalAlloc (GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT,
		str.GetLength () + 1);
	LPVOID lpCbTextData = ::GlobalLock (hClipboardText);
	memcpy (lpCbTextData, (LPCTSTR)str, str.GetLength ());

	VERIFY (::SetClipboardData (CF_TEXT, hClipboardText) != NULL);

	if (hClipboardText)
		::GlobalUnlock (hClipboardText);

	::CloseClipboard ();
}

BOOL CDebugPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
#if defined( __WINPRINTER__ ) //&& __CUSTOM__ == __SW0819__

	CString strTmp, strDebug = FoxjetCommon::CVersion (FoxjetCommon::verApp).Format () + "\r\n\r\n";

	if (CControlDoc * pDoc = (CControlDoc *)theApp.GetActiveDocument ()) {
		CStringArray vLines;

		pDoc->GetProductLines (vLines);
		
		for (int i = 0; i < vLines.GetSize (); i++) {
			if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
				CPrintHeadList & list = pLine->m_listHeads;

				for (POSITION pos = list.GetStartPosition(); pos != NULL; ) {
					Head::CHead * pHead = NULL;
					DWORD dwKey = 0;

					list.GetNextAssoc (pos, dwKey, (void *&)pHead);

					if (CLocalHead * pLocal = DYNAMIC_DOWNCAST (CLocalHead, pHead)) {
						_DBG_FLAGS * pFlags = &pLocal->Flags;

						strDebug += _T ("----------\r\n");

						strTmp = pLocal->m_Config.m_HeadSettings.m_strUID;
						strDebug += strTmp + _T ("\r\n\r\n");

						strDebug += pLocal->GetFriendlyName () + _T ("\r\n\r\n");

/*
						pDlg->GetDlgItemText (IDC_ISR, strTmp);
						str += _T ("ISR: ") + strTmp + _T ("\r\n");

						pDlg->GetDlgItemText (IDC_PHOTO, strTmp);
						str += _T ("PHOTO: ") + strTmp + _T ("\r\n");

						pDlg->GetDlgItemText (IDC_TACH, strTmp);
						str += _T ("TACH: ") + strTmp + _T ("\r\n");

						pDlg->GetDlgItemText (IDC_OTB, strTmp);
						str += _T ("OTB: ") + strTmp + _T ("\r\n");

						pDlg->GetDlgItemText (IDC_SERIAL, strTmp);
						str += _T ("SERIAL: ") + strTmp + _T ("\r\n");

						pDlg->GetDlgItemText (IDC_RESERVED1, strTmp);
						str += _T ("RESERVED1: ") + strTmp + _T ("\r\n");

						pDlg->GetDlgItemText (IDC_RESERVED2, strTmp);
						str += _T ("RESERVED2: ") + strTmp + _T ("\r\n");

*/
						WORD wPrintDly	= pLocal->GetPrintDly ();
						WORD wImageLen	= pLocal->GetImageLen ();
						WORD wProdLen	= pLocal->GetProdLen ();
						UINT nFlush		= pLocal->GetFlush ();
						CString strFmt = _T ("\t%05d [%03.02f in]\r\n");
						double dFactor = (double)pLocal->m_Config.m_HeadSettings.GetRes (); 

				///*
						strTmp.Format (_T ("Image len:") + strFmt, 
							wImageLen,
							(double)wImageLen / dFactor);
						strDebug += strTmp;

						strTmp.Format (_T ("Prod len:\t") + strFmt,
							wProdLen,
							(double)wProdLen / dFactor);
						strDebug += strTmp;

						strTmp.Format (_T ("Print delay:") + strFmt,
							wPrintDly,
							(double)wPrintDly / dFactor);
						strDebug += strTmp;

						strTmp.Format (_T ("Flush:\t") + strFmt,
							nFlush,
							(double)nFlush / dFactor);
						strDebug += strTmp;

						if (pLocal->m_Config.m_HeadSettings.m_nDirection == FoxjetDatabase::LTOR) 
							strDebug += "Direction:\t\t----->\r\n";
						else
							strDebug += "Direction:\t\t<-----\r\n";

						/*
						strTmp.Format (_T ("Flush+PhotoDly:") + strFmt,
							nFlush + wPrintDly,
							(double)(nFlush + wPrintDly) / dFactor);
						strDebug += strTmp;
						*/

						strDebug += "\r\n";
				//*/

						if (theApp.IsMPHC ()) {
							DWORD dwCtrlReg = 0;
							DWORD dwInterrupt_Reg = 0;
							DWORD dwInterfaceCtrlReg = 0;
							DWORD dwOTBStatusReg = 0;
							DWORD dwInterfaceStatusReg = 0;

							theApp.GetMPHCDriver ()->GetDebugFlags (pLocal->m_hDeviceID, pLocal->Flags);
							theApp.GetMPHCDriver ()->ReadConfiguration (pLocal->m_hDeviceID, pLocal->m_registersISA);

							memcpy (&dwCtrlReg,				&pLocal->m_registersISA._CtrlReg,				sizeof (pLocal->m_registersISA._CtrlReg));
							memcpy (&dwInterrupt_Reg,		&pLocal->m_registersISA._Interrupt_Reg,		sizeof (pLocal->m_registersISA._Interrupt_Reg));
							memcpy (&dwInterfaceCtrlReg,	&pLocal->m_registersISA._InterfaceCtrlReg,		sizeof (pLocal->m_registersISA._InterfaceCtrlReg));
							memcpy (&dwOTBStatusReg,		&pLocal->m_registersISA._OTBStatusReg,			sizeof (pLocal->m_registersISA._OTBStatusReg));
							memcpy (&dwInterfaceStatusReg,	&pLocal->m_registersISA._InterfaceStatusReg,	sizeof (pLocal->m_registersISA._InterfaceStatusReg));
								
							strTmp.Format (_T ("ICR:\t\t0x%p\r\n"), pFlags->uICR);
							strDebug += strTmp;

							strTmp.Format (_T ("IER:\t\t0x%p\r\n"), pFlags->uIER);
							strDebug += strTmp;

							strTmp.Format (
								_T ("CtrlReg:\t\t0x%p\r\n")
								_T ("Interrupt_Reg:\t0x%p\r\n")
								_T ("InterfaceCtrlReg:\t0x%p\r\n")
								_T ("OTBStatusReg:\t0x%p\r\n")
								_T ("InterfaceStatusReg:\t0x%p\r\n"),
								dwCtrlReg,
								dwInterrupt_Reg,
								dwInterfaceCtrlReg,
								dwOTBStatusReg,
								dwInterfaceStatusReg);
							strDebug += strTmp;
						}
						else { // vx
							strTmp.Format (_T ("m_bPrinting: %s\r\n"), pLocal->m_bPrinting ? "true" : "false"); 
							strDebug += strTmp;

							strTmp.Format (_T ("m_eop: %s (0x%p, 0x%p)\r\n"), pLocal->m_eop.m_bStop, pLocal->m_eop.m_wParam, pLocal->m_eop.m_lParam); 
							strDebug += strTmp;

							strTmp.Format (_T ("m_hDeviceID: 0x%p\r\n"), pLocal->m_hDeviceID); 
							strDebug += strTmp;

							strTmp.Format (_T ("m_hPhotoEvent: 0x%p%s\r\n"), pLocal->m_hPhotoEvent, (IsSignaled (pLocal->m_hPhotoEvent) ? " [SIGNALED]" : "")); 
							strDebug += strTmp;

							strTmp.Format (_T ("m_hEOPEvent: 0x%p%s\r\n"), pLocal->m_hEOPEvent, (IsSignaled (pLocal->m_hEOPEvent) ? " [SIGNALED]" : "")); 
							strDebug += strTmp;

							strTmp.Format (_T ("m_listImgUpdDesc: %d\r\n"), pHead->m_listImgUpdDesc.GetCount ()); 
							strDebug += strTmp;
						}

						{
							LocalHead::CHeadPtrArray & v = pLocal->m_vSlaves;

							for (int i = 0; i < v.GetSize (); i++) {
								strDebug += _T ("\r\nMaster\r\n");

								if (CLocalHead * pSlave = DYNAMIC_DOWNCAST (CLocalHead, v [i])) {
									strTmp.Format (_T ("\tSlave: %s\r\n"), 
										pSlave->m_Config.m_HeadSettings.m_strUID);
									strDebug += strTmp;
								}
							}

							strDebug += "\r\n";

							{
								DWORD dwState = pLocal->GetErrorState ();
								CStringArray vState;

								if (dwState & HEADERROR_LOWTEMP)		vState.Add (_T ("LOW TEMP"));
								if (dwState & HEADERROR_HIGHVOLTAGE)	vState.Add (_T ("HIGH VOLTAGE ERROR"));
								if (dwState & HEADERROR_LOWINK)			vState.Add (_T ("LOW INK"));
								if (dwState & HEADERROR_OUTOFINK)		vState.Add (_T ("OUT OF INK"));
								//if (dwState & HEADERROR_EP8)			vState.Add (_T ("EP8"));
								if (pHead->m_bDisabled)					vState.Add (_T ("DISABLED"));

								if (!vState.GetSize ())
									vState.Add (_T ("OK"));

								strDebug += _T ("Head state: ");

								for (int i = 0; i < vState.GetSize (); i++) 
									strDebug += vState [i];

								strDebug += _T ("\r\n");
							}

							{
								CString str [] = 
								{
									"DISABLED",
									"RUNNING",
									"IDLE",
									"STOPPED",
								};

								strDebug += _T ("Image state: ") + str [pLocal->m_eTaskState] + _T ("\r\n");
							}
							 
							strDebug += _T ("Task: ") + pLocal->m_sTaskName + _T ("\r\n");
							
							strTmp.Format (_T ("Count: %d\r\n"), pLocal->m_Status.m_lCount);
							strDebug += strTmp;
						}
					}

					strDebug += _T ("\r\n\r\n");
				}
			}
		}
	}

	SetDlgItemText (TXT_DEBUG, strDebug);

#endif
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

HBRUSH CDebugPage::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = ::GetSysColor (COLOR_BTNFACE);//FoxjetUtils::GetButtonColor (_T ("background"));
	
	if (!m_hbrDlg)	
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}
