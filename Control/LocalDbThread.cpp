#include "stdafx.h"

#include <process.h>

#include "ODBCInst.h"
#include "control.h"
#include "ControlView.h"
#include "Debug.h"
#include "Host.h"
#include "Parse.h"
#include "Registry.h"
#include "LocalDbDlg.h"

using namespace FoxjetDatabase;
using namespace FoxjetUtils;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#pragma data_seg( "local_db" )
	namespace LocalDB
	{
		CRITICAL_SECTION cs;
		bool bEnabled = false;
		CArray <CTime, CTime &> vTrigger;
		TCHAR szLocalBMP	[MAX_PATH] = { 0 };
		TCHAR szNetworkBMP	[MAX_PATH] = { 0 };
		bool bForce = false;
	};
#pragma

#ifdef TRACE
	#undef TRACE
#endif

#ifdef TRACEF
	#undef TRACEF
#endif

#define TRACEF(s) Trace ((s), _T (__FILE__), __LINE__, true)
#define TRACE(s) Trace ((s), _T (__FILE__), __LINE__, false)

static void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine, bool bLog)
{
	CString strLogFile = GetHomeDir () + _T ("\\networked_backup.txt");
	CString str;

	str.Format (_T ("%s(%d): %s\n"), lpszFile, lLine, lpsz);
	TRACER (str);
	
	if (bLog) {
		//int nIndex;

		//for (nIndex = str.Find (_T ("): ")); nIndex >= 0 && nIndex < str.GetLength () && str [nIndex] != '\\'; nIndex--)
		//	;

		//if (nIndex >= 0 && nIndex < str.GetLength ()) 
		{
			CString strLine = CTime::GetCurrentTime ().Format (_T ("[%c] ")) + lpsz + CString (_T ("\n")); //str.Mid (nIndex);
			//strLine.TrimRight ();
			//strLine += _T ("\n");

			if (FILE * fp = _tfopen (strLogFile, _T ("a"))) {
				fwrite (w2a (strLine), strLine.GetLength (), 1, fp);
				fclose (fp);
			}
		}

		if (CControlView * pView = DYNAMIC_DOWNCAST (CControlView, theApp.GetActiveView ())) {
			str = lpsz;
			str.Replace (_T ("\r"), _T (""));
			str.Replace (_T ("\n"), _T (""));
			pView->UpdateSerialData (str);
		}
	}
}

CString GetDBQ (const CString & strDSN)
{
	CString strLocal		= FoxjetDatabase::GetProfileString (HKEY_LOCAL_MACHINE, _T ("SOFTWARE\\ODBC\\ODBC.INI\\") + strDSN, _T ("DBQ"), _T (""));
	CString strCurrentUser	= FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, _T ("SOFTWARE\\ODBC\\ODBC.INI\\") + strDSN, _T ("DBQ"), _T (""));

	TRACEF (strLocal);
	TRACEF (strCurrentUser);

	return strLocal.GetLength () ? strLocal : strCurrentUser;
}

CString GetRootPath (const CString & strFile)
{
	int nIndex = strFile.Find (_T ("\\"));

	if (nIndex != -1) {
		CString str = strFile.Left (nIndex);

		if (str.Find (_T (":")) != -1) 
			return str;
	}

	return _T ("");
}

bool ConnectMappedDrive (const CString & strFile)
{
	bool bResult = false;

	if (::GetFileAttributes (strFile) != -1)
		return true;

	CString strRoot = GetRootPath (strFile);

	if (strRoot.GetLength ()) {
		CArray <CUseInfo2, CUseInfo2 &> v;

		EnumMappedDrives (v);

		for (int i = 0; i < v.GetSize (); i++) {
			CUseInfo2 s = v [i];

			if (!s.m_strLocal.CompareNoCase (strRoot)) {
				NETRESOURCE nrMap = { 0 };
				TCHAR szRoot [MAX_PATH] = { 0 };
				TCHAR szPath [MAX_PATH] = { 0 };

				_tcscpy (szRoot, s.m_strLocal);
				_tcscpy (szPath, s.m_strRemote);

				nrMap.dwType = RESOURCETYPE_DISK;
				nrMap.dwScope = RESOURCE_GLOBALNET;
				nrMap.dwUsage = RESOURCEUSAGE_CONNECTABLE;
				nrMap.dwDisplayType = RESOURCEDISPLAYTYPE_SHARE;
				nrMap.lpLocalName = szRoot;  // "Z:";
				nrMap.lpRemoteName = szPath; // \\ELITE7\foxjet // "\\\\MyServer\\c$";

				TRACEF (CString (szRoot) + _T (" --> ") + CString (szPath));

				DWORD retCode = ::WNetAddConnection2 (&nrMap, _T ("password"), _T ("Administrator"), 0);

				if (!(bResult = (retCode == NO_ERROR) ? true : false)) {
					TRACEF (_T ("WNetAddConnection2 failed: ") + CString (szRoot) + _T (" --> ") + CString (szPath));
					TRACEF (FormatMessage (::GetLastError ()));
				}

				return bResult;
			}
		}
	}

	return bResult;
}

CTime GetFileTime (const CString & strFile)
{
	CTime tm;
	HANDLE h = ::CreateFile (strFile, 0, 0, NULL, OPEN_EXISTING, 0, NULL);

	if (h && h != INVALID_HANDLE_VALUE) {
		FILETIME ft = { 0 };

		::GetFileTime (h, NULL, NULL, &ft);
		::CloseHandle (h);
		tm = ft;
	}

	return tm;
}

IMPLEMENT_DYNCREATE(CLocalDbThread, CWinThread)
IMPLEMENT_DYNCREATE(CLocalLogoThread, CWinThread)

CLocalDbThread::CLocalDbThread ()
:	m_bRun (true),
	m_pos (NULL),
	m_hManualTrigger (NULL),
	m_hManualCancel (NULL),
	m_bInProgress (false),
	m_nRetryLimit (5),
	CWinThread ()
{
	m_bAutoDelete		= TRUE;
	m_hManualTrigger	= ::CreateEvent (NULL, true, false, _T ("CLocalDbThread::ManualTrigger"));
	m_hManualCancel		= ::CreateEvent (NULL, true, false, _T ("CLocalDbThread::ManualCancel"));
}

CLocalDbThread::~CLocalDbThread ()
{
	if (m_hManualTrigger) {
		::CloseHandle (m_hManualTrigger);
		m_hManualTrigger = NULL;
	}

	if (m_hManualCancel) {
		::CloseHandle (m_hManualCancel);
		m_hManualCancel = NULL;
	}
}


BEGIN_MESSAGE_MAP(CLocalDbThread, CWinThread)
	//{{AFX_MSG_MAP(CLocalDbThread)
	ON_THREAD_MESSAGE (TM_KILLTHREAD, OnKillThread)
	//}}AFX_MSG_MAP
	ON_THREAD_MESSAGE (TM_LOCAL_MANUAL, OnManual)
	ON_THREAD_MESSAGE (TM_LOCAL_CANCEL, OnCancel)
	ON_THREAD_MESSAGE (TM_LOCAL_APP_DB_OPEN, OnAppDbOpen)
	ON_THREAD_MESSAGE (TM_LOCAL_APP_DB_CLOSE, OnAppDbClose)
END_MESSAGE_MAP()

BOOL CLocalDbThread::InitInstance ()
{
	CString strLocal = GetHomeDir () + _T ("\\") + theApp.GetLocalDSN () + _T (".mdb");

	CWinThread::InitInstance ();

	if (::GetFileAttributes (strLocal) == -1) {
		CString strNetwork = GetDBQ (theApp.GetDSN ());

		m_mapCopy.SetAt (strNetwork, strLocal);
		m_vDSN.Add (strLocal);
	}

	return TRUE;
}

void CLocalDbThread::OnKillThread (WPARAM wParam, LPARAM lParam)
{
	m_bRun = false;
}

void CLocalDbThread::OnManual (WPARAM wParam, LPARAM lParam)
{
	if (wParam) { // update all
		::EnterCriticalSection (&LocalDB::cs);
		LocalDB::bForce = true;
		::LeaveCriticalSection (&LocalDB::cs);
	}

	m_nRetryLimit = ((int)lParam >= 1) ? (int)lParam : 1;

	::SetEvent (m_hManualTrigger);
}

void CLocalDbThread::OnCancel (WPARAM wParam, LPARAM lParam)
{
	::SetEvent (m_hManualCancel);
}

void CLocalDbThread::OnAppDbOpen (WPARAM wParam, LPARAM lParam)
{
}

void CLocalDbThread::OnAppDbClose (WPARAM wParam, LPARAM lParam)
{
	for (POSITION pos = m_mapDeferredCopy.GetStartPosition (); pos; ) {
		CString strNetwork, strLocal;
		
		m_mapDeferredCopy.GetNextAssoc (pos, strNetwork, strLocal);
		m_mapCopy.SetAt (strNetwork, strLocal);
		TRACEF (_T ("queuing: ") + strNetwork + _T (" --> ") + strLocal);
	}

	m_mapDeferredCopy.RemoveAll ();
}

static int GetMinutes (const CTime & tm)
{
	int h = tm.GetHour ();
	int m = tm.GetMinute ();

	return (h * 60) + m;
}

int CLocalDbThread::Run ()
{
	bool bReset = false;
	CTime * ptmCompare = NULL;
	int nCount = 0;
	CTime timeLast = CTime::GetCurrentTime () - CTimeSpan (1, 0, 0, 0);
	CMap <time_t, time_t, bool, bool> mapTriggered;
	CArray <CTime, CTime &> vTrigger;

	while (m_bRun) {
		bool bCompare = false;
		CArray <CTime, CTime &> v;

		::EnterCriticalSection (&LocalDB::cs);
		CTime tmCurrent = CTime::GetCurrentTime ();
		bool bDayRolledOver = timeLast.GetDay () != tmCurrent.GetDay ();

		if (bDayRolledOver) {
			for (int i = 0; i < LocalDB::vTrigger.GetSize (); i++) {
				CTime tmTrigger = LocalDB::vTrigger [i];

				if (tmTrigger.GetHour () == 0 && tmTrigger.GetMinute () == 0) { // 12:00 AM
					tmTrigger += CTimeSpan (0, 0, 1, 0);
					LocalDB::vTrigger.SetAt (i, tmTrigger);
				}
			}

			mapTriggered.RemoveAll ();
			timeLast = CTime::GetCurrentTime ();
		}
		
		v.Append (LocalDB::vTrigger);
		v.Append (vTrigger);

		for (int i = 0; i < v.GetSize (); i++) {
			CTime tmTrigger = v [i];

			int nTrigger = GetMinutes (tmTrigger);
			int nCurrent = GetMinutes (CTime::GetCurrentTime ());

			if (nCurrent >= nTrigger) {
				time_t t = tmTrigger.GetTime ();
				bool bTriggered = false;

				if (!mapTriggered.Lookup (t, bTriggered)) {
					bCompare = true;
					TRACEF (a2w (GetRuntimeClass ()->m_lpszClassName) + tmTrigger.Format (_T (": triggered: %H:%M %p")));
					nCount = 0;

					if (!vTrigger.GetSize ())
						m_nRetryLimit = 5;
				}

				mapTriggered.SetAt (t, true);
			}
		}
		::LeaveCriticalSection (&LocalDB::cs);

		if (bCompare && vTrigger.GetSize () >= (m_nRetryLimit - 1)) {
			TRACEF (a2w (GetRuntimeClass ()->m_lpszClassName) + CString (_T (" final attempt...")));
			vTrigger.RemoveAll ();
			bReset = true;
		}

		if (!bCompare) {
			if (bCompare = ::WaitForSingleObject (m_hManualTrigger, 1) == WAIT_OBJECT_0 ? true : false) {
				::ResetEvent (m_hManualTrigger);
				nCount = 0;
			}
		}

		if (::WaitForSingleObject (m_hManualCancel, 1) == WAIT_OBJECT_0)
			bReset = true;
		else {
			if (bCompare) {
				if (!ptmCompare)
					ptmCompare = new CTime (CTime::GetCurrentTime ());

				ASSERT (ptmCompare);
				CTimeSpan tmTimeout = CTime::GetCurrentTime () - (* ptmCompare);
				int nTotal = tmTimeout.GetTotalMinutes ();

				if (!Compare (nCount++)) {
					int nInterval = 5 * 60;

					#ifdef _DEBUG
					nInterval = 30;
					#endif

					::EnterCriticalSection (&LocalDB::cs);
					if (!bReset) {
						if (m_nRetryLimit > 1) {
							CTime tmNext = CTime::GetCurrentTime () + CTimeSpan (0, 0, 0, nInterval);
							TRACEF (a2w (GetRuntimeClass ()->m_lpszClassName) + 
								tmNext.Format (_T (" retry at: %H:%M %p [attempt ")) +
								ToString (vTrigger.GetSize () + 1) + _T ("]"));
							vTrigger.Add (tmNext);
						}
					}
					bReset = true;
					::LeaveCriticalSection (&LocalDB::cs);
				}
				else {
					if (nCount == 1 && !m_mapCopy.GetCount ())
						TRACEF (a2w (GetRuntimeClass ()->m_lpszClassName) + _T (": up to update"));

					vTrigger.RemoveAll ();
				}
			}
		}

		if (bReset) {
			::ResetEvent (m_hManualCancel);
			bReset = false;
			m_pos = NULL;
			m_mapCopy.RemoveAll ();
			nCount = 0;

			FoxjetUtils::CLocalDbDlg::SetActive (false, m_nThreadID);
			TRACEF (a2w (GetRuntimeClass ()->m_lpszClassName) + _T (" reset"));

			if (ptmCompare) {
				delete ptmCompare;
				ptmCompare = NULL;
			}
		}

		::Sleep (1000);
		CopyNext ();

		::EnterCriticalSection (&LocalDB::cs);
			if (m_mapCopy.IsEmpty () && LocalDB::bForce)
				LocalDB::bForce = false;
		::LeaveCriticalSection (&LocalDB::cs);

		{
			MSG msg;
		
			while (::PeekMessage (&msg, NULL, NULL, NULL, PM_NOREMOVE)) 
				if (!PumpMessage ())
					return ExitInstance();
		}
	}

	return 0;
}

bool CLocalDbThread::NeedsUpdate (const CString & strNetwork, const CString & strLocal)
{
	FILETIME ftLocal = { 0 };
	FILETIME ftNetwork = { 0 };
	bool bLocal = false;
	bool bNetwork = false;

	HANDLE hLocal	= ::CreateFile (strLocal, 0, 0, NULL, OPEN_EXISTING, 0, NULL);
	HANDLE hNetwork = ::CreateFile (strNetwork, 0, 0, NULL, OPEN_EXISTING, 0, NULL);

	if (hLocal && hLocal != INVALID_HANDLE_VALUE) {
		bLocal = true;
		::GetFileTime (hLocal, NULL, NULL, &ftLocal);
		::CloseHandle (hLocal);
	}

	if (hNetwork && hNetwork != INVALID_HANDLE_VALUE) {
		bNetwork = true;
		::GetFileTime (hNetwork, NULL, NULL, &ftNetwork);
		::CloseHandle (hNetwork);
	}
	else {
		TRACEF (_T ("failed to open: ") + strNetwork);
		TRACEF (FormatMessage (::GetLastError ()));
	}

	ULONG lCompareFileTime = ::CompareFileTime (&ftNetwork, &ftLocal);

	//return (lCompareFileTime > 0 || !bLocal) ? true : false;
	return (lCompareFileTime != 0 || !bLocal) ? true : false;
}

bool CLocalDbThread::Compare (int nCount)
{
	CString strDSN = theApp.GetLocalDSN ();
	CString strLocal = GetHomeDir () + _T ("\\") + strDSN + _T (".mdb");//GetDBQ (theApp.GetLocalDSN ());
	CString strNetwork = GetDBQ (theApp.GetDSN ());

	if (!nCount) {
		TRACEF (a2w (GetRuntimeClass ()->m_lpszClassName) + _T (": checking: ") + strNetwork);
		TRACEF (_T ("/DSN=") + theApp.GetDSN () + _T (": ") + strLocal);
		TRACEF (_T ("/DSN_LOCAL=") + theApp.GetLocalDSN () + _T (": ") + strNetwork);
	}

	if (::GetFileAttributes (strNetwork) == -1) {
		if (!nCount) {
			TRACEF (_T ("file not found: ") + strNetwork);
		}

		/*
		if (!ConnectMappedDrive (strNetwork)) {
			TRACEF (a2w (GetRuntimeClass ()->m_lpszClassName) + _T (" ConnectMappedDrive failed: ") + strNetwork);
		}
		*/

		return false;
	}

	m_bInProgress = true;
	FoxjetUtils::CLocalDbDlg::SetActive (true, m_nThreadID);

	m_vDSN.Add (strLocal);

	m_mapCopy.RemoveAll ();

	if (LocalDB::bForce || /* bDSN || */ NeedsUpdate (strNetwork, strLocal)) {
		TRACEF (GetFileTime (strLocal).Format	(_T ("local:   %c   ")) + strLocal);
		TRACEF (GetFileTime (strNetwork).Format	(_T ("network: %c   ")) + strNetwork);
		m_mapCopy.SetAt (strNetwork, strLocal);
		TRACEF (strNetwork + _T (" --> ") + strLocal);
	}

	m_pos = m_mapCopy.GetStartPosition ();

	return true;
}

void CLocalDbThread::CopyNext ()
{
	::EnterCriticalSection (&LocalDB::cs);

	if (!m_pos) 
		m_pos = m_mapCopy.GetStartPosition ();

	if (m_pos) {
		CString strLocal, strNetwork;

		m_mapCopy.GetNextAssoc (m_pos, strNetwork, strLocal);

		if (!strNetwork.GetLength () || !strLocal.GetLength ()) {
			m_mapCopy.RemoveKey (strNetwork);
			::LeaveCriticalSection (&LocalDB::cs);
			return;
		}

		{
			int nIndex = strLocal.ReverseFind ('\\');

			if (nIndex != -1) {
				CString strDir = strLocal.Left (nIndex);

				if (::GetFileAttributes (strDir) == -1)
					CreateDir (strDir);
			}
		}

		if (::GetFileAttributes (strNetwork) == -1) {
			TRACEF (_T ("GetFileAttributes failed: ") + strNetwork);
			//TRACEF (FormatMessage (::GetLastError ()));
		}
		else {
			if (!strNetwork.CompareNoCase (strLocal)) {
				TRACEF (_T ("skipping: ") + strNetwork + _T (" --> ") + strLocal);
				m_mapCopy.RemoveKey (strNetwork);
			}
			else {
				TRACEF (strNetwork + _T (" --> ") + strLocal);

				if (::CopyFile (strNetwork, strLocal, FALSE)) {
					FILETIME ftNetwork = { 0 };

					HANDLE hLocal	= ::CreateFile (strLocal, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
					HANDLE hNetwork = ::CreateFile (strNetwork, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);

					if (hNetwork && hNetwork != INVALID_HANDLE_VALUE) {
						::GetFileTime (hNetwork, NULL, NULL, &ftNetwork);
						::CloseHandle (hNetwork);
					}

					if (hLocal && hLocal != INVALID_HANDLE_VALUE) {
						if (!::SetFileTime (hLocal, NULL, NULL, &ftNetwork)) {
							TRACEF (_T ("SetFileTime failed: ") + strLocal);
							TRACEF (FormatMessage (::GetLastError ()));
							ASSERT (0);
						}

						::CloseHandle (hLocal);
					}

					{
						int nIndex = Find (m_vDSN, strLocal);

						if (nIndex != -1) {
							CString strFilename;
							CString strDir;
							CString strDSN = theApp.GetLocalDSN ();
							static const LPCTSTR lpszDBDriver = SQL_MDB_DRIVER;
							LPCTSTR lpszFormat = 
								_T ("DSN=%s~ ")					// dsn
								_T ("DBQ=%s\\%s~ ")				// file
								_T ("Description=%s~ ")			// dsn
								_T ("FileType=MicrosoftAccess~ ")
								_T ("DataDirectory=%s~ ")
								_T ("MaxScanRows=20~ ")
								_T ("DefaultDir=%s~ ")
								_T ("FIL=MS Access;~ ")
								_T ("MaxBufferSize=2048~ ")
								_T ("MaxScanRows=8~ ")
								_T ("PageTimeout=5~ ")
								_T ("Threads=3~ ");
							TCHAR sz [512];
							TCHAR szAttr [512];

							{
								int nIndex = strLocal.ReverseFind ('\\');

								if (nIndex != -1) {
									strDir = strLocal.Left (nIndex);
									strFilename = strLocal.Mid (nIndex + 1);
								}
							}

							_stprintf (sz, lpszFormat, 
								strDSN, 
								strDir, strFilename,
								strDSN,
								strDir, 
								strDir);
							int nLen = _tcsclen (sz);

							_tcscpy (szAttr, /* w2a */ (sz));
							TRACEF (/* a2w */ (szAttr));

							for (int i = 0; i < nLen; i++)
								if (szAttr [i] == '~')
									szAttr [i] = '\0';

							if (!::SQLConfigDataSource (NULL, ODBC_REMOVE_SYS_DSN, /* w2a */ (lpszDBDriver), /* CAnsiString */ (_T ("DSN=") + strDSN)))
								TRACE_SQLERROR ();

							if (::SQLConfigDataSource (NULL, ODBC_ADD_SYS_DSN, /* w2a */ (lpszDBDriver), szAttr)) {
								TRACEF (m_vDSN [nIndex]);
								TRACEF (CString (_T ("ODBC_ADD_SYS_DSN: ")) + sz);
								m_vDSN.RemoveAt (nIndex);
							}
							else {
								TRACEF (_T ("SQLConfigDataSource failed: ") + CString (sz) + _T (" --> ") + strLocal);
								TRACE_SQLERROR ();
								TRACEF (FormatMessage (::GetLastError ()));
							}
						}
					}

					m_mapCopy.RemoveKey (strNetwork);
				}
				else {
					TRACEF (_T ("CopyFile failed: ") + strNetwork + _T (" --> ") + strLocal);
					TRACEF (FormatMessage (::GetLastError ()));
				
					if (Find (m_vDSN, strLocal) != -1) {
						TRACEF (_T ("deferring copy until db closes: ") + strNetwork + _T (" --> ") + strLocal);
						m_mapCopy.RemoveKey (strNetwork);
						m_mapDeferredCopy.SetAt (strNetwork, strLocal);
					}
				}
			}
		}

		if (!m_pos)
			TRACEF (a2w (GetRuntimeClass ()->m_lpszClassName) + _T (" finished"));
	}

	if (m_bInProgress) {
		if (m_mapCopy.GetCount ())
			FoxjetUtils::CLocalDbDlg::SetActive (true, m_nThreadID);
		else {
			FoxjetUtils::CLocalDbDlg::SetActive (false, m_nThreadID);
			m_bInProgress = false;
		}
	}

	::LeaveCriticalSection (&LocalDB::cs);
}

//////////////////////////////////////////////////

CLocalLogoThread::CLocalLogoThread ()
:	CLocalDbThread ()
{
	if (m_hManualTrigger)
		::CloseHandle (m_hManualTrigger);
	if (m_hManualCancel)
		::CloseHandle (m_hManualCancel);

	m_hManualTrigger	= ::CreateEvent (NULL, true, false, _T ("CLocalLogoThread::ManualTrigger"));
	m_hManualCancel		= ::CreateEvent (NULL, true, false, _T ("CLocalLogoThread::ManualCancel"));
}

CLocalLogoThread::~CLocalLogoThread ()
{
}

bool CLocalLogoThread::Compare (int nCount)
{
	bool bResult = true;

	::EnterCriticalSection (&LocalDB::cs);

	if (!nCount)
		TRACEF (a2w (GetRuntimeClass ()->m_lpszClassName) + _T (": checking: ") + CString (LocalDB::szNetworkBMP));

	if (::GetFileAttributes (LocalDB::szNetworkBMP) == -1) {
		if (!nCount)
			TRACEF (_T ("directory not found: ") + CString (LocalDB::szNetworkBMP));
		//TRACEF (FormatMessage (::GetLastError ()));
		bResult = false;
	}
	else {
		m_bInProgress = true;
		FoxjetUtils::CLocalDbDlg::SetActive (true, m_nThreadID);

		m_mapCopy.RemoveAll ();
		CStringArray vNetwork, vLocal;

		FoxjetDatabase::GetFiles (LocalDB::szNetworkBMP, _T ("*.*"), vNetwork);
		FoxjetDatabase::GetFiles (LocalDB::szLocalBMP, _T ("*.*"), vLocal);

		for (int i = 0; i < vNetwork.GetSize (); i++) {
			CString strNetwork = vNetwork [i];

			if (::GetFileAttributes (strNetwork) & FILE_ATTRIBUTE_DIRECTORY)
				continue;

			CString strLocal = strNetwork;
			bool bCopy = false;

			strLocal.Replace (LocalDB::szNetworkBMP, LocalDB::szLocalBMP);

			int nIndex = Find (vLocal, strLocal);

			if (nIndex == -1 || LocalDB::bForce || NeedsUpdate (strNetwork, strLocal)) {
				m_mapCopy.SetAt (strNetwork, strLocal);
				//TRACEF (strNetwork + _T (" --> ") + strLocal);
			}
		}

		m_pos = m_mapCopy.GetStartPosition ();
		TRACEF (ToString (m_mapCopy.GetCount ()) + _T (" files to update"));
	}

	::LeaveCriticalSection (&LocalDB::cs);

	return bResult;
}

void CLocalLogoThread::CopyNext ()
{ 
	CLocalDbThread::CopyNext ();
}

BOOL CLocalLogoThread::InitInstance ()
{
	CWinThread::InitInstance ();

	return TRUE;
}
