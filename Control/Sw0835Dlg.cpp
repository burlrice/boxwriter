// Sw0835Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "Sw0835Dlg.h"
#include "ControlView.h"
#include "ProdLine.h"
#include "Resource.h"
#include "Debug.h"
#include "BaxterConfigDlg.h"
#include "Parse.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSw0835Dlg dialog


CSw0835Dlg::CSw0835Dlg(CControlView * pView, CProdLine * pLine)
:	m_pView (pView),
	m_pLine (pLine),
	FoxjetCommon::CEliteDlg(CSw0835Dlg::IDD, pView)
{
	ASSERT (m_pView);
	ASSERT (m_pLine);

	//{{AFX_DATA_INIT(CSw0835Dlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CSw0835Dlg::~CSw0835Dlg ()
{
}

void CSw0835Dlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSw0835Dlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	DDX_Text (pDX, TXT_BARCODE1, m_strBarcode1);
	DDX_Text (pDX, TXT_BARCODE2, m_strBarcode2);
	DDX_Text (pDX, TXT_BARCODE3, m_strBarcode3);

	if (!pDX->m_bSaveAndValidate) {
		CString strTask, strSerial [3], strSubLot, strOut, strOutFormatted;

		GetData (strTask, strSerial [0], strSerial [1], strSerial [2], strSubLot, strOut);

		for (int i = 0; i < strOut.GetLength (); i++) {
			strOutFormatted += strOut [i];

			if (i && (i % 20) == 0) 
				strOutFormatted += _T ("\r\n");
		}

		SetDlgItemText (TXT_PRODUCTCODE, strTask);
		SetDlgItemText (TXT_EXPDATE1, strSerial [0]);
		SetDlgItemText (TXT_EXPDATE2, strSerial [1]);
		SetDlgItemText (TXT_LOTCODE, strSerial [2]);
		SetDlgItemText (TXT_OUTPUT, strOutFormatted);
	}
	else {
		CString strSubLot;

		GetDlgItemText (TXT_SUBLOT, strSubLot);

		if (!IsValidSubLot (strSubLot)) {
			MessageBox (LoadString (IDS_INVALIDSUBLOT));
			pDX->PrepareEditCtrl (TXT_SUBLOT);
			pDX->Fail ();
		}
	}
}


BEGIN_MESSAGE_MAP(CSw0835Dlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CSw0835Dlg)
	ON_WM_TIMER()
	ON_EN_CHANGE(TXT_SUBLOT, OnChangeSublot)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSw0835Dlg message handlers

BOOL CSw0835Dlg::OnInitDialog() 
{
	UINT nID [] = 
	{
		TXT_BARCODE1,
		TXT_BARCODE2,
		TXT_BARCODE3,
		TXT_PRODUCTCODE,
		TXT_EXPDATE1,
		TXT_EXPDATE2,
		TXT_LOTCODE,
		TXT_OUTPUT,
		TXT_SUBLOT,
	};
	CBaxterConfigDlg dlgConfig;

	dlgConfig.Load ();
	FoxjetCommon::CEliteDlg::OnInitDialog();

	if (CWnd * p = GetDlgItem (IDOK))
		p->EnableWindow (FALSE);

	SetTimer (0, 1, NULL);
	m_tmInit = CTime::GetCurrentTime ();

/*
	m_fnt.CreateFont (8, 0, 0, 0, FW_BOLD, FALSE, 0, 0,
		DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("MS Sans Serif"));

	for (int i = 0; i < ARRAYSIZE (nID); i++)
		if (CWnd * p = GetDlgItem (nID [i]))
			p->SetFont (&m_fnt);
*/

	if (CEdit * p = (CEdit *)GetDlgItem (TXT_SUBLOT)) {
		if (dlgConfig.m_nUserLenOpt <= 0)
			p->EnableWindow (FALSE);

		p->SetLimitText (dlgConfig.m_nUserLenOpt);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CSw0835Dlg::ProcessSerialData(const CString &strData)
{
	CBaxterConfigDlg dlgConfig;
	CString str (strData);
	bool bValid = false;

	dlgConfig.Load ();

	if (str.GetLength () && str [str.GetLength () - 1] == '\r')
		str.Delete (str.GetLength () - 1);

	TRACEF (FoxjetDatabase::ToString (str.GetLength ()));
	str.TrimLeft ();
	str.TrimRight ();
	TRACEF (FoxjetDatabase::ToString (str.GetLength ()));

	m_pView->UpdateSerialData (strData);

	if (m_strBarcode1.IsEmpty ()) {
		int nMin = dlgConfig.m_nTaskLen + dlgConfig.m_nOvertureLen1 + dlgConfig.m_nOvertureLen2 + dlgConfig.m_nOvertureLen3;
		int nMax = nMin;

		m_strBarcode1 = str;
		int nLen = str.GetLength ();
		//bValid = str.GetLength () >= 25 && str.GetLength () <= 26;
		bValid = nLen >= nMin && nLen <= nMax;
	}
	else {
		if (dlgConfig.m_bVerify) {
			if (m_strBarcode2.IsEmpty ()) {
				int nMin = dlgConfig.m_nPrimaryLenReq;
				int nMax = nMin + dlgConfig.m_nPrimaryLenOpt;

				str.TrimRight (); // sw0850
				m_strBarcode2 = str;
				int nLen = str.GetLength ();
				//bValid = m_strBarcode2.GetLength () == 16;
				bValid = nLen >= nMin && nLen <= nMax;
			}
			else if (m_strBarcode3.IsEmpty ()) {
				int nMin = dlgConfig.m_nSecondaryLenReq;
				int nMax = nMin + dlgConfig.m_nSecondaryLenOpt;

				str.TrimRight (); // sw0850
				m_strBarcode3 = str;
				int nLen = str.GetLength ();
				//bValid = m_strBarcode3.GetLength () == 17;
				bValid = nLen >= nMin && nLen <= nMax;

				CString strMatch1 = m_strBarcode1.Right (dlgConfig.m_nMatchLen);
				CString strMatch2 = m_strBarcode3.Right (dlgConfig.m_nMatchLen);

				if (strMatch1 != strMatch2) {
					UpdateData (FALSE);
					MessageBox (LoadString (IDS_INVALIDDATA_MISMATCH), NULL, MB_OK | MB_ICONHAND);
					m_strBarcode1 = m_strBarcode2 = m_strBarcode3 = _T ("");
					UpdateData (FALSE);
					return 0;
				}
			}
		}
	}

	UpdateData (FALSE);

	if (!bValid) {
		MessageBox (LoadString (IDS_INVALIDDATA), NULL, MB_OK | MB_ICONHAND);
		m_strBarcode1 = m_strBarcode2 = m_strBarcode3 = _T ("");
		UpdateData (FALSE);
	}
	else {
		if (dlgConfig.m_bVerify) {
			if (m_strBarcode3.GetLength ())
				if (CWnd * p = GetDlgItem (IDOK))
					p->EnableWindow (TRUE);
		}
		else {
			if (m_strBarcode1.GetLength ())
				if (CWnd * p = GetDlgItem (IDOK))
					p->EnableWindow (TRUE);
		}
	}

	return bValid ? 1 : 0;
}

void CSw0835Dlg::Close ()	
{
	LOCK (m_pLine->m_cs0835);
	KillTimer (0);
	m_pLine->m_pdlg0835 = NULL;
	delete this;
}

bool CSw0835Dlg::IsValidSubLot (const CString & str)
{
	CBaxterConfigDlg dlgConfig;

	dlgConfig.Load ();

	if (str.GetLength () <= dlgConfig.m_nUserLenOpt) {
		for (int i = 0; i < str.GetLength (); i++)
			if (str [i] < 'A' && str [i] > 'Z')
				return false;
	}

	return true;
}

CString CSw0835Dlg::MakeAlpha (const CString & str)
{
	CString strResult;
	CBaxterConfigDlg dlgConfig;

	dlgConfig.Load ();

	for (int i = 0; i < str.GetLength (); i++) {
		if (_istalpha (str [i]))
			strResult += (TCHAR)_toupper (str [i]);
	}

	return strResult;
}

void CSw0835Dlg::GetData (CString & strTask, 
						  CString & strSerial1, CString & strSerial2, CString & strSerial3, 
						  CString & strSubLot, 
						  CString & strOut)
{
	CBaxterConfigDlg dlgConfig;

	dlgConfig.Load ();
	GetDlgItemText (TXT_SUBLOT, strSubLot);

	strSubLot = MakeAlpha (strSubLot);

	int nLen = dlgConfig.m_nTaskLen + dlgConfig.m_nOvertureLen1 + dlgConfig.m_nOvertureLen2 + dlgConfig.m_nOvertureLen3;

	if (m_strBarcode1.GetLength () == nLen) {
		int nIndex = 0;

		strTask		= m_strBarcode1.Mid (nIndex, dlgConfig.m_nTaskLen);			nIndex += dlgConfig.m_nTaskLen;
		strSerial1	= m_strBarcode1.Mid (nIndex, dlgConfig.m_nOvertureLen1);	nIndex += dlgConfig.m_nOvertureLen1;
		strSerial2	= m_strBarcode1.Mid (nIndex, dlgConfig.m_nOvertureLen2);	nIndex += dlgConfig.m_nOvertureLen2;
		strSerial3	= m_strBarcode1.Mid (nIndex, dlgConfig.m_nOvertureLen3);	nIndex += dlgConfig.m_nOvertureLen3;
	}

	if (dlgConfig.m_bVerify) {
		using namespace FoxjetCommon;

		strOut = dlgConfig.m_strOuputFormat;
		TRACEF (FoxjetDatabase::Format ((LPBYTE)(LPCSTR)w2a (strOut), strOut.GetLength ()));
		
		/* if (m_strBarcode1.GetLength ())	*/ strOut.Replace (_T ("<PRIMARY>"),	m_strBarcode2);
		/* if (m_strBarcode2.GetLength ())	*/ strOut.Replace (_T ("<SECONDARY>"),	m_strBarcode3);
		/* if (strSubLot.GetLength ())		*/ strOut.Replace (_T ("<SUBLOT>"),		strSubLot);

		TRACEF (FoxjetDatabase::Format ((LPBYTE)(LPCSTR)w2a (strOut), strOut.GetLength ()));
	}
}

void CSw0835Dlg::OnOK()
{
	CString strTask, strSerial [3], strSubLot, strOut;
	CBaxterConfigDlg dlgConfig;

	if (!UpdateData (TRUE))
		return;

	dlgConfig.Load ();
	GetData (strTask, strSerial [0], strSerial [1], strSerial [2], strSubLot, strOut);

	TRACEF (strTask);
	TRACEF (strSerial [0]);
	TRACEF (strSerial [1]);
	TRACEF (strSerial [2]);

	{
		CString str = strSerial [0] + strSerial [1] + strSerial [2] + strSubLot;
		int nLen = str.GetLength () + 2;
		PUCHAR pBuffer = new UCHAR [nLen];
	
		ZeroMemory (pBuffer, sizeof (UCHAR) * nLen);
		memcpy (pBuffer, (LPVOID)(LPCSTR)w2a (str), str.GetLength ());
		TRACEF (FoxjetDatabase::ToString (str.GetLength ()));
		TRACEF ((const char *)pBuffer);

		if (m_pView)
			m_pView->UpdateSerialData (m_pLine->m_LineRec.m_strName + _T (": [serial data]: ") + str);

		m_pLine->UpdateSerialData (a2w (pBuffer), str.GetLength ());

		delete pBuffer;
	}

	int nStart = m_pLine->StartTask (strTask, false, false, false, m_pView);

	if (nStart == TASK_START_SUCCESS) {
		if (dlgConfig.m_bVerify)
			m_pLine->DownloadSerialData (strOut, m_pView); // sw0850
	}
	else {
		CString str;

		str.Format (LoadString (IDS_REMOTESTARTFAILED), m_pLine->GetName (), strTask);
		m_pLine->UpdateSerialData (NULL, 0);
		m_pLine->StopTask (false, m_pView);
		MessageBox (str, NULL, MB_OK | MB_ICONHAND);
	}

	FoxjetCommon::CEliteDlg::OnOK ();
	Close ();
}

void CSw0835Dlg::OnCancel()
{
	FoxjetCommon::CEliteDlg::OnCancel ();
	Close ();
}

void CSw0835Dlg::OnTimer(UINT nIDEvent) 
{
	CTimeSpan tm = CTime::GetCurrentTime () - m_tmInit;
	
	FoxjetCommon::CEliteDlg::OnTimer(nIDEvent);

	if (tm.GetTotalSeconds () > (2 * 60)) {
		KillTimer (nIDEvent);
		MessageBox (LoadString (IDS_0835TIMEOUT));
		Close ();
	}
}

void CSw0835Dlg::OnChangeSublot() 
{
	UpdateData (FALSE);
}


