// Control.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Resource.h"

#include <initguid.h>
#include <WinIoctl.h>
#include <AfxSock.h>
#include <Shlwapi.h>
#include <afxtempl.h>
#include <afxdao.h>
#include <afxpriv.h>
#include <io.h>
#include <iostream>
#include <fcntl.h>

#include "AppVer.h"
#include "AboutDlg.h"
#include "Defines.h"
#include "Control.h"
#include "MainFrm.h"
#include "ControlDoc.h"
#include "ControlView.h"
#include "fj_socket.h"
#include "Database.h"
#include "Extern.h"
#include "SPDevApi.h"
#include "ProgressDlg.h"
#include "Splash.h"
#include "WinMsg.h"
#include "Debug.h"
#include "File.h"
#include "AppVer.h"
#include "DebugPage.h"
#include "FileExt.h"
#include "fj_defines.h"
#include "Parse.h"
#include "IvPhc.h"
#include "DevGuids.h"
#include "Registry.h"
#include "Parse.h"
#include "OdbcDatabase.h"
#include "ReportSettingsDlg.h"
#include "FieldDefs.h"
#include "NetStatDlg.h"
#include "ConnectDlg.h"
#include "Parse.h"
#include "SystemDlg.h"
#include "NetworkDlg.h"
#include "Utils.h"
#include "EventsDlg.h"
#include "LocalDbThread.h"
#include "PrintReportDlg.h"
#include "ScanReportDlg.h"
#include "StartupDlg.h"
#include "StrobeDlg.h"
#include "ProdLine.h"
#include "TemplExt.h"
#include "InkSerialNumberDlg.h"
#include "StartTaskDlg.h"
#include "http.h"

#ifdef __IPPRINTER__
	#include "FirmwareDlg.h"
#endif

#ifdef __WINPRINTER__
	#include "CountElement.h"
	#include "BarcodeElement.h"
	#include "DatabaseElement.h"
	#include "EliteFirmwareDlg.h"
	#include "DateTimeElement.h"
	#include "UserElement.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;
using namespace Head;

static const LPCTSTR lpszSettingsKey  = _T ("BW.");

///////////////////////////////////////////////////////////////////////////////////

static const BYTE nPrivateKeyScrambled [] = 
{
	0xD3, 0x11, 0x28, 0x09, 0x02, 0x8A, 0x71, 0x83, 	0x1F, 0x26, 0x96, 0x09, 0xE8, 0x3A, 0xE7, 0xD0, 
	0x9B, 0x14, 0x0F, 0x18, 0x50, 0x91, 0x25, 0x09, 	0xC1, 0x58, 0x32, 0x9D, 0x6B, 0x46, 0x72, 0xDB, 
	0x2F, 0xE1, 0x37, 0x89, 0x7C, 0xB5, 0x6D, 0x65, 	0xFA, 0x2C, 0xC1, 0x7A, 0x26, 0xD7, 0x95, 0x4F, 
	0xE9, 0x28, 0xB6, 0xEC, 0x5A, 0x48, 0xA6, 0x63, 	0x15, 0xA7, 0xAF, 0xA0, 0x97, 0xF6, 0x43, 0xC6, 
	0x0F, 0x39, 0xC5, 0x17, 0x46, 0xF4, 0x79, 0x5F, 	0x0C, 0x1D, 0x01, 0xE3, 0x66, 0x13, 0xAE, 0xD5, 
	0x48, 0x21, 0x6B, 0xA8, 0x47, 0xA3, 0xB5, 0xBE, 	0x1C, 0x78, 0xDB, 0x51, 0x2D, 0xD5, 0x9D, 0x3D, 
	0x44, 0xF3, 0x43, 0x99, 0x01, 0xF9, 0xB9, 0xC7, 	0x30, 0x0D, 0x42, 0xF9, 0x00, 0xE7, 0x12, 0x4D, 
	0xB8, 0x1B, 0x6C, 0xC1, 0x91, 0xC5, 0xC9, 0x79, 	0x54, 0x47, 0x72, 0x2F, 0x5C, 0xB9, 0x75, 0xD4, 
	0x01, 0x81, 0x40, 0x80, 0xF3, 0x03, 0xDF, 0x85, 	0x9F, 0x62, 0x2A, 0x4E, 0xF9, 0x3C, 0x87, 0x34, 
	0x48, 0xB7, 0x59, 0x56, 0xD5, 0x0B, 0xF2, 0x33, 	0x1A, 0x5F, 0x4E, 0xE7, 0xB8, 0xD1, 0xD5, 0x96, 
	0x22, 0xAF, 0x79, 0xDB, 0x07, 0xE4, 0x1F, 0xEB, 	0x66, 0x53, 0x1E, 0x3F, 0x19, 0x6B, 0x71, 0xC3, 
	0xFF, 0x8F, 0x86, 0x09, 0x1D, 0x43, 0xD6, 0x9F, 	0x1D, 0xB6, 0xF0, 0x6C, 0xC0, 0xCF, 0xCA, 0x1A, 
	0x56, 0x38, 0x62, 0x9E, 0x42, 0x1C, 0x8A, 0x45, 	0xA9, 0x09, 0x22, 0x9A, 0xC3, 0x6A, 0x3E, 0xCF, 
	0xC3, 0xD7, 0x99, 0xBF, 0x97, 0x15, 0x49, 0xBE, 	0x65, 0xEB, 0x03, 0xCA, 0x9E, 0x1A, 0x41, 0x7B, 
	0x8B, 0xB7, 0xE8, 0xC4, 0x6D, 0x09, 0x31, 0xDE, 	0xCC, 0xC0, 0xC2, 0xF7, 0xAE, 0xC1, 0x39, 0xA7, 
	0x8D, 0x06, 0xCF, 0xAF, 0x45, 0xEE, 0xC9, 0x25, 	0x98, 0xD6, 0xCD, 0x2F, 0x4C, 0xAE, 0x5C, 0xAF, 
	0xE9, 0xCD, 0x46, 0x01, 0x81, 0x40, 0x87, 0xA0, 	0xD1, 0xF4, 0x89, 0x08, 0x40, 0x32, 0x4E, 0xBE, 
	0x19, 0x37, 0xDC, 0x22, 0x91, 0xE4, 0xBF, 0xF1, 	0x2B, 0xE3, 0x36, 0xFB, 0x5B, 0x1A, 0x1A, 0x34, 
	0xE9, 0x91, 0xCD, 0x6B, 0x05, 0x42, 0x88, 0x79, 	0x86, 0x43, 0x9D, 0x74, 0xF9, 0xBA, 0xB4, 0x8D, 
	0xF9, 0x2E, 0xE0, 0x6C, 0xCA, 0xEB, 0x22, 0xA3, 	0x4C, 0xB6, 0x6D, 0xD3, 0x4A, 0x1B, 0xA7, 0x9A, 
	0xC5, 0x74, 0x9A, 0x86, 0x19, 0xE7, 0xC8, 0xB7, 	0x12, 0x36, 0xC5, 0x06, 0xE7, 0xFA, 0xFD, 0xC5, 
	0x06, 0x56, 0xD9, 0x17, 0x53, 0x28, 0xD5, 0xAB, 	0x36, 0x7E, 0xD7, 0xCC, 0x88, 0x6A, 0xB2, 0xE9, 
	0x30, 0x28, 0x92, 0xB8, 0xED, 0xE0, 0xD4, 0xAD, 	0xDE, 0x14, 0x58, 0xA9, 0x6E, 0x5A, 0xA5, 0x0F, 
	0xB2, 0xA0, 0xC3, 0x81, 0x5C, 0xBB, 0xF1, 0x4B, 	0xF8, 0xFA, 0x19, 0x10, 0x66, 0x8F, 0x8F, 0x0F, 
	0x63, 0xFE, 0x88, 0x58, 0x3E, 0xB6, 0x01, 0x81, 	0x40, 0xC4, 0x5A, 0x99, 0x03, 0xB8, 0x12, 0xC2, 
	0x13, 0xE7, 0x37, 0xB4, 0xAD, 0xEB, 0x11, 0xBE, 	0x7F, 0x8C, 0xA8, 0x39, 0x71, 0xD3, 0x25, 0x1F, 
	0xAE, 0xA5, 0xD2, 0xF3, 0xD6, 0x7D, 0x17, 0x21, 	0x49, 0x01, 0x92, 0x73, 0x2C, 0x0E, 0xBE, 0x77, 
	0x40, 0xFF, 0x1C, 0x45, 0x9C, 0xAE, 0xD9, 0x22, 	0xEA, 0xEC, 0x52, 0x1F, 0x06, 0xD5, 0xE7, 0x98, 
	0xF7, 0xD2, 0x0F, 0xA9, 0xD1, 0xF4, 0xFA, 0x4D, 	0x0E, 0x8C, 0x13, 0xE5, 0xBC, 0x16, 0xAA, 0x6B, 
	0xE0, 0xE8, 0x2C, 0x77, 0xDB, 0xFA, 0xAF, 0x5B, 	0x36, 0xEE, 0xDD, 0x56, 0x08, 0x38, 0x75, 0x95, 
	0xB9, 0xAE, 0x72, 0xB5, 0x90, 0x06, 0xDA, 0x3F, 	0x5E, 0xF7, 0x92, 0xDA, 0xEA, 0x32, 0xA5, 0xC2, 
	0x69, 0x8F, 0xB2, 0xFC, 0xA3, 0xF4, 0xBF, 0x4A, 	0xDC, 0xE4, 0xE6, 0xB2, 0xD5, 0xDB, 0x05, 0x4D, 
	0x3A, 0x20, 0x0C, 0xF6, 0x30, 0x65, 0xFA, 0x79, 	0x47, 0x00, 0x81, 0x81, 0x40, 0x85, 0x2E, 0xE7, 
	0x10, 0xD8, 0xC8, 0x44, 0x87, 0x49, 0xE3, 0x1E, 	0x0C, 0xDA, 0xB4, 0x26, 0xF0, 0xC2, 0x80, 0xAA, 
	0xFF, 0x23, 0x08, 0x70, 0xE0, 0xA9, 0xBB, 0x59, 	0xD1, 0x32, 0xF9, 0x2A, 0x9E, 0x11, 0x44, 0x37, 
	0x85, 0x60, 0x5E, 0x5E, 0xF0, 0xEA, 0xE9, 0x61, 	0x41, 0x04, 0xB1, 0x80, 0x33, 0x5F, 0x2A, 0x05, 
	0x6F, 0x1F, 0x1D, 0x1A, 0xCC, 0xB1, 0x61, 0x5F, 	0x68, 0xF7, 0x97, 0x3C, 0xAE, 0x5F, 0x4C, 0x49, 
	0x3F, 0x16, 0xAF, 0xB9, 0x35, 0x69, 0x4D, 0xE8, 	0x9A, 0x40, 0x69, 0x42, 0x02, 0xB5, 0x28, 0x3C, 
	0x99, 0xD5, 0x37, 0xD7, 0xEC, 0xC8, 0x8A, 0x1B, 	0x5F, 0x72, 0x8D, 0x7F, 0x7F, 0x3A, 0xD7, 0x81, 
	0xFA, 0x49, 0x36, 0x69, 0xAE, 0xE5, 0x57, 0x6F, 	0x8C, 0x04, 0x1F, 0x59, 0x54, 0xDC, 0xDA, 0xD4, 
	0xD7, 0x9C, 0x7B, 0x61, 0x13, 0x42, 0x26, 0x08, 	0xA9, 0x47, 0x8B, 0x28, 0x8F, 0x00, 0x81, 0x81, 
	0x40, 0x83, 0xFB, 0x20, 0xE1, 0x19, 0x47, 0x44, 	0xB8, 0xA0, 0x55, 0xC6, 0xC7, 0x73, 0xBE, 0xA0, 
	0xBF, 0xCD, 0x84, 0x96, 0xE5, 0x9E, 0x0B, 0x3B, 	0xC9, 0xF5, 0x97, 0x81, 0x2A, 0xB3, 0x39, 0x07, 
	0xA6, 0x5E, 0x5E, 0x64, 0xB7, 0xE9, 0xC1, 0xB6, 	0x50, 0x05, 0xE0, 0x4C, 0x61, 0x3A, 0x6F, 0xBE, 
	0x15, 0x90, 0x49, 0x6A, 0x46, 0x05, 0xBD, 0xA0, 	0xD5, 0x74, 0x73, 0xD0, 0xA5, 0xF8, 0xDE, 0x9A, 
	0x74, 0x21, 0x3D, 0xA2, 0x67, 0x6A, 0x6A, 0x8C, 	0x92, 0x7D, 0x52, 0x11, 0x2A, 0x04, 0xF7, 0xC6, 
	0xCD, 0x1E, 0xE0, 0x37, 0x64, 0xD9, 0x48, 0xC1, 	0x97, 0xC6, 0xBA, 0x19, 0xF7, 0x35, 0x76, 0x9F, 
	0x0E, 0x82, 0x3F, 0x49, 0xF8, 0x43, 0xF2, 0x20, 	0xEA, 0x66, 0x84, 0xBD, 0xAB, 0xEB, 0xD5, 0xAB, 
	0x87, 0xFB, 0x47, 0x1E, 0x44, 0xFA, 0xBC, 0xA9, 	0x6F, 0xFC, 0x45, 0x7D, 0xB6, 0x02, 0x43, 0xD7, 
	0x04, 0x88, 0x1C, 0x8B, 0xF6, 0xBB, 0x80, 0xE3, 	0x57, 0x8C, 0x8F, 0x87, 0x8E, 0x27, 0x89, 0x3E, 
	0xDC, 0xD3, 0xB5, 0xD1, 0x32, 0xCA, 0x2B, 0x9B, 	0x90, 0x33, 0xBE, 0x59, 0x03, 0x0E, 0x8B, 0x21, 
	0x2E, 0xE3, 0xFC, 0xD5, 0x8A, 0x74, 0xB1, 0x05, 	0x33, 0xA3, 0x25, 0x6E, 0x67, 0xCF, 0x28, 0xEA, 
	0x96, 0xC0, 0x5C, 0x67, 0xEC, 0xFE, 0xDC, 0xEC, 	0xE9, 0xFD, 0x9B, 0x27, 0x02, 0xA6, 0x4D, 0x19, 
	0x52, 0xE0, 0xE5, 0xDF, 0x5E, 0x75, 0x4E, 0xD6, 	0x2B, 0xF4, 0x3E, 0x20, 0xC5, 0xED, 0x30, 0xCF, 
	0xD5, 0x01, 0xF0, 0xB2, 0xEB, 0x39, 0x58, 0x3D, 	0xB4, 0x04, 0x16, 0xDD, 0x57, 0x5D, 0xDF, 0xD0, 
	0x01, 0x3B, 0x27, 0x6E, 0x6A, 0xF9, 0xFC, 0x24, 	0x55, 0xB6, 0xCB, 0xED, 0xDA, 0x9E, 0x26, 0x97, 
	0x0E, 0xEB, 0x23, 0xA9, 0x94, 0xB3, 0xAC, 0x4C, 	0xFE, 0x70, 0x48, 0x08, 0x26, 0xC8, 0x0B, 0x4E, 
	0xC0, 0x00, 0x80, 0x41, 0x40, 0x80, 0x00, 0x80, 	0xC0, 0x40, 0xC0, 0x31, 0x7B, 0x50, 0x7B, 0xA1, 
	0x7A, 0xBB, 0xAE, 0xB9, 0x7C, 0x9E, 0xC4, 0x84, 	0x00, 0xB6, 0xA1, 0xFF, 0x22, 0x12, 0x8C, 0xD3, 
	0xA6, 0x66, 0x12, 0xA8, 0x68, 0xA9, 0x37, 0x33, 	0x29, 0x07, 0xA2, 0x1D, 0x60, 0xDD, 0x88, 0x12, 
	0x8A, 0x46, 0xEC, 0xD1, 0x1E, 0x15, 0xAC, 0x69, 	0xB5, 0xDE, 0xB9, 0xCF, 0x9C, 0xD5, 0xC7, 0x7D, 
	0xAD, 0xCC, 0x11, 0x70, 0x79, 0xD5, 0x06, 0x76, 	0x20, 0x36, 0xBA, 0xA2, 0xEC, 0x2D, 0xBB, 0xB4, 
	0x2E, 0x29, 0x95, 0xBE, 0x1C, 0xF0, 0x9B, 0xCA, 	0xD5, 0xAE, 0x5E, 0x19, 0xFA, 0x0B, 0x2C, 0xB5, 
	0xD9, 0x68, 0xE7, 0x56, 0xED, 0x9F, 0x29, 0x5D, 	0x1D, 0xFB, 0x7D, 0x89, 0xF1, 0xAC, 0x41, 0xD2, 
	0x44, 0x06, 0xCC, 0xEF, 0xDA, 0x06, 0x60, 0xD9, 	0x77, 0x7B, 0xB4, 0x31, 0xD6, 0x54, 0x48, 0x9C, 
	0xD4, 0x56, 0xF4, 0xBB, 0x5F, 0xE6, 0xEC, 0x8D, 	0xE7, 0x82, 0x93, 0x06, 0x61, 0x2A, 0xDF, 0x6C, 
	0xD3, 0x30, 0x36, 0xFD, 0x2B, 0x8F, 0x51, 0xEF, 	0xD5, 0x42, 0x3C, 0x0A, 0x9F, 0x8F, 0x49, 0xD3, 
	0x82, 0x5B, 0xAB, 0x8A, 0x3B, 0xA0, 0x8F, 0xF7, 	0xE6, 0x11, 0x10, 0xDD, 0xF4, 0x23, 0x5E, 0x82, 
	0x02, 0x0E, 0x4E, 0x11, 0x1A, 0xFD, 0x88, 0x65, 	0x2D, 0x8B, 0xB8, 0x1D, 0x0A, 0x31, 0xD7, 0xE9, 
	0x6B, 0x96, 0xD2, 0xCF, 0x4B, 0x65, 0x11, 0x54, 	0x5A, 0x1B, 0xD7, 0x6E, 0xBE, 0x95, 0xF5, 0xBE, 
	0x63, 0x63, 0xEB, 0x38, 0xF9, 0xF1, 0x71, 0x6C, 	0xC4, 0xB4, 0xCE, 0xA8, 0xE1, 0x17, 0x1D, 0xE0, 
	0xCF, 0x96, 0x45, 0x49, 0xFB, 0x4B, 0x59, 0x46, 	0xA1, 0x9C, 0xD5, 0xAA, 0xDB, 0x09, 0xAD, 0xB5, 
	0xE0, 0x75, 0x6A, 0xBE, 0xAB, 0x28, 0x97, 0x2B, 	0x66, 0x27, 0x0D, 0x7B, 0x38, 0xB9, 0xCB, 0x3E, 
	0x92, 0xEC, 0x2F, 0xAA, 0x91, 0x55, 0x6C, 0x61, 	0x96, 0xAB, 0x00, 0x80, 0x80, 0x41, 0x40, 0x00, 
	0x80, 0x40, 0x45, 0x20, 0x41, 0x0C, 
};

///////////////////////////////////////////////////////////////////////////////////

#pragma data_seg( "ping" )
	namespace Ping
	{
		CRITICAL_SECTION csPing;
		TCHAR szPing [MAX_PATH] = { 0 };
		FoxjetCommon::CStrobeItem errorPing;
		bool bTrigger = false;
		bool bEnabled = false;
		bool bShutdown = false;
	};
#pragma

#pragma data_seg( "local_db" )
	namespace LocalDB
	{
		extern CRITICAL_SECTION cs;
		extern bool bEnabled;
		extern CArray <CTime, CTime &> vTrigger;
		extern TCHAR szLocalBMP		[MAX_PATH];
		extern TCHAR szNetworkBMP	[MAX_PATH];
	};
#pragma

static LRESULT CALLBACK PingFunc (LPVOID lpData)
{
	HWND hWnd = (HWND)lpData;
	clock_t clock_tLast = clock (), clock_tNow = clock ();
	WSADATA wsaData;

	if (::WSAStartup (MAKEWORD (2,1),&wsaData) != 0){
		TRACEF ("WSAStartup failed: " + FormatMessage (GetLastError ()));
		return 0;
	}

	while (1) {
		clock_tNow = clock ();
		double dDiff = (double)(clock_tNow - clock_tLast) / CLOCKS_PER_SEC;

		::EnterCriticalSection (&Ping::csPing);
		int nDelay = Ping::errorPing.m_nDelay;
		::LeaveCriticalSection (&Ping::csPing);

		//TRACER (_T ("Ping::errorPing.m_nDelay: ") + ToString (nDelay) + _T ("\n"));

		if (dDiff > (double)nDelay || Ping::bTrigger) {
			if (!_tcscmp (Ping::szPing, _T ("shutdown"))) {
				TRACER (_T ("PingFunc exiting\n"));
				TRACEF (_T ("PingFunc exiting"));
				return 0;
			}

			if (_tcslen (Ping::szPing) > 0) {
				::EnterCriticalSection (&Ping::csPing);
				bool bConnected = false;

				for (int nTries = 0; !bConnected && nTries < 3; nTries++) {
					int nPing = FoxjetCommon::Ping (Ping::szPing, 1, theApp.GetActiveFrame ()->m_hWnd) > 0 ? true : false;

					bConnected = (nPing == 1 ? true : false);

					if (!bConnected) 
						::Sleep (500);
				}

				::LeaveCriticalSection (&Ping::csPing);

				TRACEF (CTime::GetCurrentTime ().Format (_T ("%c: ")) + CString (Ping::szPing) + (bConnected ? _T ("") : _T (" **** disconnected **** ")));

				::PostMessage (hWnd, WM_PING_STATUS, bConnected, 0);
			}

			::EnterCriticalSection (&Ping::csPing);
			clock_tLast = clock ();
			Ping::bTrigger = false;
			::LeaveCriticalSection (&Ping::csPing);
		}

		::Sleep (1000);
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////////
CDbConnection::CDbConnection (LPCTSTR lpszFile, ULONG lLine)
:	m_strFile (lpszFile),
	m_lLine (lLine)
{
	theApp.m_bDatabaseInUse = true;

	if (theApp.IsLocalDB ()) {
		CTimeSpan tm = CTime::GetCurrentTime () - theApp.m_tmDatabaseOpened;
		int nSeconds = tm.GetTotalSeconds ();

		if (nSeconds >= 30) {
			if (theApp.m_Database.IsOpen ())
				theApp.m_Database.Close ();
		}
	}

	if (!theApp.m_Database.IsOpen ()) {
		if (theApp.ReconnectDatabase ()) {
			#ifdef _DEBUG
			CDebug::Trace ("theApp.ReconnectDatabase ()", true, m_strFile, m_lLine);
			#endif
		}
	}
}

CDbConnection::~CDbConnection ()
{
	theApp.m_tmDatabase = CTime::GetCurrentTime ();

	if (CControlDoc * pDoc = (CControlDoc *)theApp.GetActiveDocument ()) {
		CStringArray vLines;

		ASSERT (pDoc);
		pDoc->GetProductLines (vLines);
		
		for (int i = 0; i < vLines.GetSize (); i++) {
			if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
				CPrintHeadList list;

				pLine->GetActiveHeads (list);

				for (POSITION pos = list.GetStartPosition(); pos != NULL; ) {
					Head::CHead * pHead = NULL;
					DWORD dwKey = 0;
					bool bResult = false;

					list.GetNextAssoc (pos, dwKey, (void *&)pHead);
					CHeadInfo info = Head::CHead::GetInfo (pHead);

					SEND_THREAD_MESSAGE (info, TM_SET_TIME_DB, &theApp.m_tmDatabase);
				}
			}
		}
	}

	theApp.m_bDatabaseInUse = false;
}

bool CDbConnection::IsOpen () const 
{ 
	return theApp.m_Database.IsOpen () ? true : false; 
}

bool CDbConnection::OpenExclusive (CWnd * pParent)
{
	CString strUser = theApp.m_strUsername;

	if (!IsOpen ()) {
		CString str = LoadString (IDS_FAILEDTOOPENDB);

		if (pParent)
			MsgBox (* pParent, str, NULL, MB_ICONERROR);
		else
			MsgBox (str, (LPCTSTR)NULL, MB_ICONERROR);

		return false;
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////
CDbContext::CDbContext (FoxjetDatabase::COdbcDatabase & db)
:	m_db (db), 
	m_dwThreadID (db.m_dwOwnerThreadID)
{
	db.m_dwOwnerThreadID = ::GetCurrentThreadId ();
}

CDbContext::~CDbContext ()
{
	m_db.m_dwOwnerThreadID = m_dwThreadID;
}

///////////////////////////////////////////////////////////////////////////////////


struct 
{
	FoxjetDatabase::TASKSTATE	m_state;
	UINT						m_nID;
} 
static const mapState [] =
{
	{ FoxjetDatabase::DISABLED,	 IDS_DISABLED	},
	{ FoxjetDatabase::RUNNING,	 IDS_RUNNING	},
	{ FoxjetDatabase::IDLE,		 IDS_IDLE		},
	{ FoxjetDatabase::STOPPED,	 IDS_STOPPED	},
};

///////////////////////////////////////////////////////////////////////////////////
// Global Functions
namespace GlobalVars
{
#ifdef  __IPPRINTER__
	INT StartEditSessionTimeout		= 1000 * 120;
	INT SaveEditSessionTimeout		= 1000 * 120;
	INT HeadResponseTimeout			= 1000 * 30;
#endif

#ifdef __WINPRINTER__
	CString strTestPattern = _T ("[") _T (FJSYS_TEST_PATTERN) _T ("]") ;
#else
	CString strTestPattern = FJSYS_TEST_PATTERN;
#endif
	extern CString strTask				= _T ("Task");
	extern CString strCount				= _T ("Count");
	extern CString strState				= _T ("State");
	extern CString strDriverRegSection	= _T ("SOFTWARE\\FoxJet\\Driver");
	extern CString strUserPrompted		= _T ("User prompted");
};


#ifdef __IPPRINTER__
CMapStringToPtr vEdit;

bool GlobalFuncs::IsEditSession (const FoxjetDatabase::HEADSTRUCT & h)
{
	bool bLock = false;

	::vEdit.Lookup (h.m_strUID, (void *&)bLock);
	
	return bLock;
}

void GlobalFuncs::SetEditSession (const FoxjetDatabase::HEADSTRUCT & h, bool bLock)
{
	TRACEF (h.m_strUID + CString (_T (": ")) + CString (bLock ? _T ("locked") : _T ("unlocked")));
	::vEdit.SetAt (h.m_strUID, (void *&)bLock);
}
#endif //__IPPRINTER__

FoxjetCommon::CVersion GlobalVars::CurrentVersion (FoxjetCommon::verApp); //fj_GetDllVersion

CString LoadString (UINT nID)
{
	return FoxjetDatabase::LoadString (nID, _T ("Control.exe"));
}

int GlobalFuncs::GetPhcChannels (const FoxjetDatabase::HEADSTRUCT & h)
{
	if (_tcstoul (h.m_strUID, NULL, 16) >= 0x300)
		if (h.m_nHeadType == FoxjetDatabase::GRAPHICS_384_128)
			return 256;

	return h.GetChannels (); // h.m_nChannels;
}

bool GlobalFuncs::IsIPAddress (CString &sAddress) {
	bool result = false;
	CString sTemp = sAddress;
	// If this is a valid IP address then it should contain 3 "."  If not then
	// we will assume that it is not valid.  Using replace on
	// a temp var is quick, dirty and simple that is all.
	int n = sTemp.Replace ('.', ' ');

	if ( n == 3 )
	{
		IN_ADDR in_addr;
		in_addr.S_un.S_addr = inet_addr (w2a (sAddress));
		if ( in_addr.S_un.S_addr != INADDR_NONE )
		{
			if ( in_addr.S_un.S_un_b.s_b1 != 127 )
				result = true;
		}
	}
	return result;
}

void GlobalFuncs::StandardizeIPAddress (CString &sAddress) {
	// This functions purpose is to take an IP address and standardize the format.
	// Example 192.168.000.004 changes to 192.168.0.4 (this is typically the 
	// standard method).  Also we must do a little extra work here because numbers
	// starting with a zero (0) are considered to be in octal format.  It is generally
	// expected by the users that all number enter today are in decimal.

	if ( IsIPAddress ( sAddress ) ) {
		TCHAR *pAddress = new TCHAR [sAddress.GetLength() + 1];
		_tcscpy ( pAddress, sAddress);
		IN_ADDR in_addr;
		TCHAR *pToken;
		pToken = _tcstok (pAddress, _T ("."));
		if ( pToken )
			in_addr.S_un.S_un_b.s_b1 = _ttoi ( pToken );

		pToken = _tcstok (NULL, _T ("."));
		if ( pToken )
			in_addr.S_un.S_un_b.s_b2 = _ttoi ( pToken );

		pToken = _tcstok (NULL, _T ("."));
		if ( pToken )
			in_addr.S_un.S_un_b.s_b3 = _ttoi ( pToken );

		pToken = _tcstok (NULL, _T ("."));
		if ( pToken )
			in_addr.S_un.S_un_b.s_b4 = _ttoi ( pToken );

		delete [] pAddress;
		if ( in_addr.S_un.S_addr != INADDR_NONE )
			sAddress = inet_ntoa (in_addr);
	}
}

CString GlobalFuncs::GetAppName ()
{
	return FoxjetCommon::GetAppTitle () + _T (" ") + LoadString (AFX_IDS_APP_TITLE);
}

LONG GlobalFuncs::GetDllVersion(LPCTSTR lpszDllName) {
	HINSTANCE hinstDll;
	DWORD dwVersion = 0;

	hinstDll = LoadLibrary( lpszDllName );
	if( hinstDll ) {
	  DLLGETVERSIONPROC pDllGetVersion;
	  pDllGetVersion = (DLLGETVERSIONPROC) GetProcAddress(hinstDll, "DllGetVersion");
		// Because some DLLs might not implement this function, you must test for it explicitly.
		// Depending on the particular DLL, the lack of a DllGetVersion function can be a useful
		// indicator of the version.
		if( pDllGetVersion ) {
			DLLVERSIONINFO dvi;
			HRESULT hr;
			ZeroMemory(&dvi, sizeof(dvi));
			dvi.cbSize = sizeof(dvi);
			hr = (*pDllGetVersion)(&dvi);
			if( SUCCEEDED ( hr ) ) 
				 dwVersion = PACKVERSION(dvi.dwMajorVersion, dvi.dwMinorVersion);
		}
		FreeLibrary(hinstDll);
	}
	return dwVersion;
}

// Global Functions
/////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
// CControlApp

BEGIN_MESSAGE_MAP(CControlApp, CWinApp)
	//{{AFX_MSG_MAP(CControlApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
//	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CControlApp construction

CControlApp::CControlApp() 
:	m_bCoupledLines (false),
	m_bStrobeCancelled (false),
	m_bSingleMsg (false),
	m_bIsHubConnected (false),
	m_bShowPrintCycle (false),
	m_bShowNetStatDlg (false),
	m_bDbgView (false),
	m_bVxDpc (true),
	m_nInterfaceThreadID (0),
	m_bSW0835 (false),
	m_bSW0844 (false),
	m_bSW0848 (false),
	m_bPrintReport (true),
	m_nStrobe (0),
	m_bNetworkConnected (true),
	m_bNEXTEditSession (false),
	m_bScanReport (true),
	m_bCaptureDebugImage (false),
	m_bDroppedElementTest (false),
	m_bDebug (false),
	m_bIsContinuousCount (false),
	m_tmDatabase (CTime::GetCurrentTime ()),
	m_tmDatabaseOpened (CTime::GetCurrentTime ()),
	m_nDbTimeout (60 * 1),
	m_bDatabaseInUse (false),
	m_Database (_T (__FILE__), __LINE__),
	m_bIsImageTimingEnabled (false),	
	m_bSetSocketSetUserElements (false),
	m_hCallWndProcRet (NULL),
	m_hPingThread (NULL)
{

	#ifdef __WINPRINTER__
	m_pMphcDriver		= NULL;
	m_pIVDriver			= NULL;
	m_pUsbMphcCtrl		= NULL;
	m_pIdsThread		= NULL;
	m_nImgMode			= IMAGE_ON_PHOTOCELL;
	m_bImgModeExpire	= true;
	#endif //__WINPRINTER__

	#ifdef _DEBUG
	using namespace FoxjetDebug;

	CDebugFile * pFile = new CDebugFile ();
	int nFlags = CFile::modeCreate | CFile::modeReadWrite;
	CString str, strFile = _T ("Control.log");

	BOOL bOpen = pFile->Open (strFile, nFlags);

	if (bOpen) {
		::afxDump.m_pFile = pFile;
		str.Format (_T ("\nLog started on: %s\n\n"),
			CTime::GetCurrentTime ().Format (_T ("%c")));
		TRACEF (str);
	}
	else {
		MsgBox (_T ("failed to open log"));
		delete pFile;
	}
	#endif //_DEBUG

	m_bRootUser = false;
	m_strUsername = _T ("");
	m_strDSN	= FoxjetCommon::GetDSN(); 
}

CControlApp::~CControlApp()
{
	int nFlush = _flushall ();

	::DeleteCriticalSection (&Ping::csPing);
	::DeleteCriticalSection (&LocalDB::cs);

	#ifdef _DEBUG
	if (::afxDump.m_pFile) {
		::afxDump.m_pFile->Close ();
		delete (FoxjetDebug::CDebugFile *)::afxDump.m_pFile;
		::afxDump.m_pFile = NULL;
	}
	#endif //_DEBUG
#ifdef _DEBUG
//	CString str;	str.Format (_T ("CControlApp::~CControlApp: nFlush: %d"), nFlush); 	MsgBox (str);
#endif
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CControlApp object

CControlApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CControlApp initialization

/* TODO: rem
static LRESULT CALLBACK UpdateUiFunc (LPVOID lpData)
{
	for (int i = 0; i < 5; i++) {
		theApp.GetActiveView ()->PostMessage (WM_COMMAND, MAKEWPARAM (IDM_LOGIN, 0));
		::Sleep (1000);
	}

	return 0;
}
*/

BOOL CControlApp::InitInstance()
{
	IsInstallerInvoked ();

	if (FoxjetDatabase::IsRunning (ISPRINTERRUNNING)) {	
		DWORD dwResult;

		LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_ISPRINTERRUNNING, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 5000, &dwResult);

		exit (0);
		return TRUE; 
	}

	m_instance.Create (ISPRINTERRUNNING);
	#if defined( _DEBUG ) && defined( __WINPRINTER__ )
	//CheckDlgSizes ();
	#endif

	if (!CInstance::Elevate ())
		return FALSE;

	FoxjetCommon::CheckDllVersions ();

	{
		DWORD dw = 0;
		CDiagnosticSingleLock::Init (60 * 10);
		CDiagnosticSingleLock::RegisterThreadName (_T ("CControlApp"), ::GetCurrentThreadId ());

		//::CreateThread ((LPSECURITY_ATTRIBUTES)NULL, 0, (LPTHREAD_START_ROUTINE)DiagnosticSyncThread, (LPVOID)0, 0, &dw);
	}

	NEXT::CNextThread::ApplySw0923CommFix (true);

	if (FoxjetDatabase::IsDebugDisplayAttached ())
		FoxjetDatabase::SetHighResDisplay (false);

	CHead::InitializeCriticalSection ();

	DECLARETRACECOUNT (200);

	int nPriority = GetThreadPriority ();
	SetThreadPriority (THREAD_PRIORITY_TIME_CRITICAL);
	TRACEF (_T ("SetThreadPriority: ") + FoxjetDatabase::ToString (GetThreadPriority ()));

	CString strCmdLine = ::GetCommandLine ();
	strCmdLine.MakeUpper ();
	FoxjetDatabase::SetNetworked (_tcsstr (strCmdLine, _T ("/NETWORKED")) != NULL);
	FoxjetDatabase::SetGojo (_tcsstr (strCmdLine, _T ("/GOJO")) != NULL);
	FoxjetFile::SetWriteDirect (_tcsstr (strCmdLine, _T ("/WRITE_DIRECT")) != NULL);

	m_pMainWnd = NULL;
	//	DisplaySystemVersion();
	
	#ifdef _DEBUG
	//::afxMemDF = allocMemDF | checkAlwaysMemDF; TRACEF ("TODO: rem");
	#endif

	if( GlobalFuncs::GetDllVersion(_T ("comctl32.dll")) < PACKVERSION(4,71) ) {
		MsgBox (_T ("COMCTL32.DLL version 4.71 or higher is required.\nPlease Install Internet Explorer 4.0 or higher.\nApplication Will Now Exit."));
		return FALSE;
	}

	AfxEnableControlContainer();

	COdbcDatabase::RegisterDisplayFct (CControlView::DisplayStatus);

	if (!AfxOleInit())
	{
		MsgBox (_T ("OLE initialization failed.  Make sure that the OLE libraries are the correct version."));
		return FALSE;
	}

	COleObjectFactory::UpdateRegistryAll ();

	if (!AfxSocketInit() ) {
		MsgBox (LoadString (IDS_SOCKET_INIT_FAILED));
		return FALSE;
	}

	{
		CString str = ::GetCommandLine ();

		str.MakeLower ();

		//if (str.Find (_T ("/http")) != -1) 
		{
			DWORD dw = 0;

			::CreateThread ((LPSECURITY_ATTRIBUTES)NULL, 0, (LPTHREAD_START_ROUTINE)HttpThread, (LPVOID)80, 0, &dw);
			::CreateThread ((LPSECURITY_ATTRIBUTES)NULL, 0, (LPTHREAD_START_ROUTINE)HttpThread, (LPVOID)(BW_HOST_HTTP_PORT), 0, &dw);
			
		}
	}
	/*
	#ifdef __SW0875__
	CString str = FoxjetDatabase::GetHomeDir () + _T("\\Debug");

	if (::GetFileAttributes (str) == -1)
		if (!::CreateDirectory (str, NULL))
			MsgBox (_T ("Failed to create: ") + str, 0, 0);
	#endif
	*/

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
	SetRegistryKey(_T("Foxjet"));
	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	m_bDemo						= false;
	m_bExpo						= false;
	m_bCountPromptAtTaskStart	= false;
	m_bShutdownTest				= false;
	m_bKeyboard					= true;

	::InitializeCriticalSection (&Ping::csPing);
	::InitializeCriticalSection (&LocalDB::cs);

	#ifdef __IPPRINTER__
	FoxjetDatabase::SetHighResDisplay (false);
	#endif

	#ifdef __WINPRINTER__
 	SetImagingMode (IMAGE_ON_PHOTOCELL);
	#endif

	{ // must be before OpenDatabase
		using namespace FoxjetCommon;

		if (strCmdLine.Find (_T ("/EXPO")) != -1) {
			m_bExpo = true;
			m_strFlags += _T (" /EXPO");
		}

		if (strCmdLine.Find (_T ("/COUNT_PROMPT_AT_TASK_START")) != -1) {
			m_bCountPromptAtTaskStart = true;
			m_strFlags += _T (" /COUNT_PROMPT_AT_TASK_START");
		}

 		if (strCmdLine.Find (_T ("/SHUTDOWNTEST")) != -1) {
			m_bShutdownTest = true;
			m_strFlags += _T (" /SHUTDOWNTEST");
		}

 		if (strCmdLine.Find (_T ("/NO_KEYBOARD")) != -1) {
			HWND hwnd;
			int i;

			m_bKeyboard = false;
			m_strFlags += _T (" /NO_KEYBOARD");

			for (i = 0; (hwnd = FindKeyboard ()) && i < 20; i++) {
				DWORD dwResult = 0;
				LRESULT lResult = ::SendMessageTimeout (hwnd, WM_SHUTDOWNAPP, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);
				::Sleep (500);
			}

			for (i = 0; (hwnd = FindKeyboardMonitor ()) && i < 20; i++) {
				DWORD dwResult = 0;
				LRESULT lResult = ::SendMessageTimeout (hwnd, WM_SHUTDOWNAPP, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);
			}
		}
		else 
		{
 			if (strCmdLine.Find (_T (" /KEYBOARD_AUTO_HIDE")) != -1) {
				m_strFlags += _T (" /KEYBOARD_AUTO_HIDE");
				FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, _T ("Software\\Foxjet\\Keyboard"), _T ("auto_hide"), 1);
			}
			else
				FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, _T ("Software\\Foxjet\\Keyboard"), _T ("auto_hide"), 0);
		}

 		if (strCmdLine.Find (_T ("/DEMO")) != -1) {
			m_bDemo = true;
			m_bRootUser = true;
			m_strUsername = _T ("ROOT");
			m_strFlags += _T (" /DEMO");
		}
		else {
			#ifdef __WINPRINTER__
				//#if __CUSTOM__ == __SW0846__
				//	SetImagingMode (IMAGE_ON_EOP);
				//#endif

				if (strCmdLine.Find (_T ("/IMAGE_ON_EOP")) != -1 || strCmdLine.Find (_T ("/IMAGE-ON-EOP")) != -1) 
					SetImagingMode (IMAGE_ON_EOP);
				else if (strCmdLine.Find (_T ("/IMAGE_ON_PHOTOCELL")) != -1 || strCmdLine.Find (_T ("/IMAGE-ON-PHOTOCELL")) != -1) 
					SetImagingMode (IMAGE_ON_PHOTOCELL);

				if (strCmdLine.Find (_T ("/NO_EXPIRE")) != -1 || strCmdLine.Find (_T ("/NO-EXPIRE")) != -1) {
					SetImagingMode (IMAGE_ON_EOP);
					SetImagingModeExpire (false);
				}
			#endif //__WINPRINTER__
		}

		/*
		#if( __CUSTOM__ == __SW0882__ )
			#ifdef _DEBUG
			{
				SetImagingMode (IMAGE_ON_EOP);
				SetImagingModeExpire (false);
			}
			#endif
		#endif
		*/

		#ifdef __WINPRINTER__
		if (GetImagingMode () == IMAGE_ON_EOP)			m_strFlags += _T (" /IMAGE_ON_EOP");
		if (GetImagingMode () == IMAGE_ON_PHOTOCELL)	m_strFlags += _T (" /IMAGE_ON_PHOTOCELL");
		if (!GetImagingModeExpire ())					m_strFlags += _T (" /NO_EXPIRE");
		#endif //__WINPRINTER__

		if (strCmdLine.Find (_T ("/SW0896")) != -1 ||
			strCmdLine.Find (_T ("/SW0835")) != -1 ||
			strCmdLine.Find (_T ("/SW0850")) != -1) 
		{
			Enable0835 (true);
			m_strFlags += _T (" /SW0896");
			m_strFlags += _T (" /SW0850");
			m_strFlags += _T (" /SW0835");
		}

		if (strCmdLine.Find (_T ("/SW0844")) != -1) {
			Enable0844 (true);
			m_strFlags += _T (" /SW0844");
		}

		if (strCmdLine.Find (_T ("/SW0848")) != -1) {
			Enable0848 (true);
			m_strFlags += _T (" /SW0848");
		}

		if (strCmdLine.Find (_T ("/CAPTURE_IMAGE")) != -1) {
			SetCaptureDebugImage (true);
			m_strFlags += _T (" /CAPTURE_IMAGE");
		}

		if (strCmdLine.Find (_T ("/DROP_TEST")) != -1) {
			SetDroppedElementTest (true);
			m_strFlags += _T (" /DROP_TEST");
		}

		if (strCmdLine.Find (_T ("/DEBUG")) != -1) {
			SetDebugMode (true);
			m_strFlags += _T (" /DEBUG");
		}

		if (strCmdLine.Find (_T ("/ULTRARES")) != -1) {
			FoxjetDatabase::EnableUltraRes (true);
			m_strFlags += _T (" /ULTRARES");
		}

		if (strCmdLine.Find (_T ("/IV")) != -1 || strCmdLine.Find (_T ("/VX")) != -1) {
			FoxjetDatabase::SetDriver (GUID_INTERFACE_IVPHC);
			m_strFlags += _T (" /VX");
		}
		else if (strCmdLine.Find (_T ("/MPHC")) != -1) {
			FoxjetDatabase::SetDriver (GUID_INTERFACE_FXMPHC);
			m_strFlags += _T (" /MPHC");
		}
	}

	if (!m_bDemo) {
		CString strSYS = FoxjetDatabase::GetHomeDir () + _T ("\\UsbMphc.dll");
		CString strSPT = _tgetenv (_T ("SystemRoot")) + CString (_T ("\\System32\\Drivers\\Foxjet\\Foxjet.spt"));

		if (::GetFileAttributes (strSYS) == -1) {
			MsgBox  (LoadString (IDS_SYSTEMFILEMISSING) + strSYS, MB_ICONERROR);
			return FALSE;
		}
		else if (::GetFileAttributes (strSPT) == -1) {
			MsgBox (LoadString (IDS_SYSTEMFILEMISSING) + strSPT, MB_ICONERROR);
			return FALSE;
		}
	}

	MARKTRACECOUNT ();

	if (!m_Database.IsOpen ()) 	{
		using namespace FoxjetCommon;

		const CString strDSN = _T ("/DSN=");
		const CString strLocalDSN = _T ("/DSN_LOCAL=");

		if (strCmdLine.Find (_T ("/PRINTCYCLE")) != -1) {
			m_bShowPrintCycle = true;
			m_strFlags += _T (" /PRINTCYCLE");
		}

		if (strCmdLine.Find (_T ("/NETSTAT")) != -1) {
			m_bShowNetStatDlg = true;
			m_strFlags += _T (" /NETSTAT");
		}

		if (strCmdLine.Find (_T ("/DBGVIEW")) != -1) {
			m_bDbgView = true;
			m_strFlags += _T (" /DBGVIEW");
		}

		if (strCmdLine.Find (_T ("/!VXDPC")) != -1) {
			m_bVxDpc = false;
			m_strFlags += _T (" /!VXDPC");
		}

		if (strCmdLine.Find (strDSN) != -1) {
			m_strDSN = GetCmdlineValue (strCmdLine, strDSN);
			m_strFlags += _T (" ") + strDSN + m_strDSN;
		}

		if (strCmdLine.Find (strLocalDSN) != -1) {
			CString strTmp = GetCmdlineValue (strCmdLine, strLocalDSN);

			if (strTmp.CompareNoCase (m_strDSN) != 0) {
				m_strLocalDSN = GetCmdlineValue (strCmdLine, strLocalDSN);
				m_strFlags += _T (" ") + strLocalDSN + m_strLocalDSN;
			}
		}

		sDatabase = FoxjetCommon::GetDatabaseName();

		{
			CString strKey = _T ("Software\\Foxjet\\MarksmanELITE\\ODBC\\") + m_strDSN;
			CString strUID = GetCmdlineValue (::GetCommandLine (), _T ("/UID="));
			CString strPWD = GetCmdlineValue (::GetCommandLine (), _T ("/PWD="));

			if (IsGojo ()) {
				if (!strUID.GetLength ()) strUID = _T ("FoxJet");
				if (!strPWD.GetLength ()) strPWD = _T ("FoxJet");
			}

			TRACEF (strKey);
			TRACEF (_T ("/UID=") + strUID + _T (" /PWD=") + strPWD);

			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("UID"), strUID); 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("PWD"), strPWD); 
		}

		bool bConnected = DoConnectDatabase (m_Database, m_strDSN, m_strLocalDSN, sDatabase);

		if (!bConnected) 
		{
			CString str, strPath, strTitle;

			FoxjetDatabase::GetBackupPath (strPath, strTitle);

			if (!IsNetworked ())
				str = LoadString (IDS_FAILEDTOOPENDB) + _T ("\nCheck: ") + strPath + '\\' + strTitle;
			else
				str = LoadString (IDS_FAILEDTOOPENDB);

			MsgBox (NULL, str, ::AfxGetAppName (), MB_ICONERROR | MB_OK);
			FoxjetCommon::ExitInstance (GlobalVars::CurrentVersion);
			FoxjetUtils::ExitInstance ();
			::AfxAbort ();
		}

		FoxjetDatabase::COdbcDatabase::SetAppDsn (FoxjetDatabase::Extract (m_Database.GetConnect (), _T ("DSN="), _T (";")));
	}

	{
		CString str = ::GetCommandLine ();

		str.MakeLower ();
		m_bIsImageTimingEnabled = str.Find (_T ("/image_timing")) != -1 ? true : false;
	}

	{
		using namespace FoxjetDatabase;

		SETTINGSSTRUCT s;
		
		if (!GetSettingsRecord (m_Database, GetPrinterID (m_Database), _T ("Control::print report"), s))
			EnablePrintReportLogging (false);
		else
			m_bPrintReport = _ttoi (s.m_strData) ? true : false;

		if (!GetSettingsRecord (m_Database, GetPrinterID (m_Database), _T ("Control::scan report"), s))
			EnableScanReportLogging (false);
		else
			m_bScanReport = _ttoi (s.m_strData) ? true : false;

		{
			s.m_lLineID = 0;
			s.m_strKey = _T ("Control: ") + FoxjetCommon::CVersion (FoxjetCommon::verApp).ToString ();
			s.m_strData = CTime::GetCurrentTime ().Format (_T ("%c"));

			if (!UpdateSettingsRecord (m_Database, s))
				VERIFY (AddSettingsRecord (m_Database, s));
		}
	}

	MARKTRACECOUNT ();

	{
		ULONG lPrinterID = GetPrinterID (m_Database);
		CString strPrinterID = GetCmdlineValue (strCmdLine, _T ("/PRINTERID="));
		CString strTimeout = FoxjetDatabase::GetCmdlineValue (strCmdLine, _T ("/DBTIMEOUT="));

		if (strPrinterID.GetLength ())
			lPrinterID = _tcstoul (strPrinterID, NULL, 10);

		SetPrinterID (lPrinterID); 

		if (strTimeout.GetLength ())
			m_nDbTimeout = _ttoi (strTimeout);
	
		m_strFlags += _T (" /DBTIMEOUT=") + ToString ((int)m_nDbTimeout);
	}



	//WriteProfileInt (_T ("sw0858"), _T ("db start"), 0);
	LoadDbStartParams ();

	#ifdef __WINPRINTER__ //#if __CUSTOM__ == __SW0833__
	{
		bool bProductionConfig = strCmdLine.Find (_T ("/PRODUCTION")) != -1;
		UINT lAddr [] = 
		{
			0x300,
			0x310,
			0x320,
			0x330,
			0x340,
			0x350,
		};

		FoxjetCommon::SetProductionConfig (bProductionConfig, m_Database);

		if (bProductionConfig) {
			m_strFlags += _T (" /PRODUCTION");

			for (int i = 0; i < ARRAYSIZE (lAddr); i++) {
				CString str;

				str.Format (_T ("%d"), i);
				bool bUse = FoxjetDatabase::GetProfileInt (CControlApp::GetRegKey () + _T ("\\Settings\\Test"), str, 0) ? true : false;
				Foxjet3d::SetTestPHC (lAddr [i], bUse);
			}
		}
	}

	//if (strCmdLine.Find (_T ("/DEMO")) != -1 || strCmdLine.Find (_T ("/DATAMATRIX")) != -1) 
		FoxjetElements::CBarcodeElement::EnableDataMatrix (true);

	if (strCmdLine.Find (_T ("/ELITE")) != -1)
		FoxjetDatabase::SetHighResDisplay (true);
	else if (strCmdLine.Find (_T ("/MATRIX")) != -1)
		FoxjetDatabase::SetHighResDisplay (false);

	{
		PRINTERSTRUCT printer;
		ULONG lPrinterID = GetPrinterID (m_Database);

		if (GetPrinterRecord (m_Database, lPrinterID, printer)) {
			printer.m_type = FoxjetDatabase::IsMatrix () ? MATRIX : ELITE;
			VERIFY (UpdatePrinterRecord (m_Database, printer));
		}
	}

	#endif

	FoxjetUtils::InitInstance ();

	{
		BYTE n [ARRAYSIZE (::nPrivateKeyScrambled)] = { 0 };

		memcpy (n, ::nPrivateKeyScrambled, ARRAYSIZE (::nPrivateKeyScrambled));
		Unscramble (n, ARRAYSIZE (n));
		FoxjetDatabase::Encrypted::RSA::SetPrivateKey (n, ARRAYSIZE (n));

		#ifdef _DEBUG
		const CString strData = _T ("1234567890-0987654321234567890-");
		const CString strEncrypted = FoxjetDatabase::Encrypted::RSA::Encrypt (strData);
		const CString strDecrypted = FoxjetDatabase::Encrypted::RSA::Decrypt (strEncrypted);

		ASSERT (strData == strDecrypted);
		#endif

		//#ifdef _DEBUG
		//if (!FoxjetDatabase::Encrypted::InkCodes::IsEnabled ()) {
		//	FoxjetDatabase::Encrypted::InkCodes::Enable (true);
		//	TRACEF ("############################################## TODO: rem ##############################################");
		//}
		//#endif

		CString strKey = FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), _T ("/delete="));

		printf (w2a (::GetCommandLine ()));

		{
			CString str = ::GetCommandLine ();

			str.MakeLower ();

			if (str.Find (_T ("/inkcodetest")) != -1) {
				const CString strRootSection = FoxjetDatabase::Encrypted::InkCodes::GetRegSection ();
				std::vector <std::wstring> vCode;
				CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

				FoxjetDatabase::Encrypted::InkCodes::Enable (true);
				FoxjetDatabase::Encrypted::Delete (_T ("\Registry\Machine\SOFTWARE\Foxjet<NULL>"));
				FoxjetDatabase::Encrypted::Delete (_T ("\Registry\User\S-1-5-21-4214632267-836034253-4094818220-1001\SOFTWARE\Foxjet<NULL>"));
				TRACEF (FoxjetDatabase::Encrypted::GetInkCodes ());
			}
		}

		if (strKey.GetLength ()) {
			std::vector <std::wstring> v;

			TRACEF (strKey);

			FoxjetDatabase::Encrypted::Delete (strKey);

			/*
			\Registry\Machine\SOFTWARE\Foxjet<NULL>
			\Registry\User\S-1-5-21-4214632267-836034253-4094818220-1001\SOFTWARE\Foxjet<NULL>
			*/

			strKey = FoxjetDatabase::Encrypted::Enumerate (strKey, v);
			TRACEF (strKey);
			CString str = strKey + _T (":\n");
			
			if (v.size ())
				str += CString (implode (v, std::wstring (_T ("\n"))).c_str ());
			else
				str += _T ("[empty]");

			::MessageBox (NULL, str, _T ("Control"), MB_OK);

			#ifdef _DEBUG
			FoxjetDatabase::Encrypted::Trace (strKey);
			#endif

			exit (0);
		}

		FoxjetDatabase::Encrypted::InkCodes::Restore (GetHomeDir () + _T ("\\data.txt"));
	}

	// Initialize the Element List
	if (!FoxjetCommon::InitInstance (m_Database, GlobalVars::CurrentVersion)) {
		FoxjetCommon::ExitInstance (GlobalVars::CurrentVersion);
		FoxjetUtils::ExitInstance ();
		::AfxAbort ();
	}

	{
		BOOL bEnable = false;
		int nDays = 30;

		Foxjet3d::CReportSettingsDlg::Load (m_Database, _T ("CPrintReportDlg"), bEnable, nDays);
		FoxjetDatabase::EnablePrintReportPurge (bEnable ? true : false, nDays);
	}

	MARKTRACECOUNT ();

	CSerialPacket::Init ();

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CControlDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CControlView));
	AddDocTemplate(pDocTemplate);

	MARKTRACECOUNT ();

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	cmdInfo.m_nShellCommand = CCommandLineInfo::FileNew;
	cmdInfo.m_strFileName.Empty ();

	MARKTRACECOUNT ();

	#ifdef __WINPRINTER__
	{ // init driver
		bool bMpchDriver	= FoxjetDatabase::IsDriverInstalled (GUID_INTERFACE_FXMPHC);
		bool bIvDriver		= FoxjetDatabase::IsDriverInstalled (GUID_INTERFACE_IVPHC);

		if (bIvDriver && bMpchDriver) {
			MsgBox (LoadString (IDS_BOTHDRIVERSINSTALLED));
			return FALSE;
		}

		try
		{
			if (bIvDriver) {
				m_pIVDriver = new CIvPhc (theApp.m_bDemo);

				BeginIdsThread ();
			} 
			else {
				//DECLARETRACECOUNT (20);
				m_pMphcDriver = new CMphc (theApp.m_bDemo);

				if (strCmdLine.Find (_T ("/NO_USB")) == -1) {
					CProgressDlg dlg;
					bool bDemoMemicPC = true;
					
 					if (strCmdLine.Find (_T ("/DEMO")) != -1) {
						bDemoMemicPC = strCmdLine.Find (_T ("/NO_DEMO_PC")) == -1 ? true : false;

						if (!bDemoMemicPC)
							if (strCmdLine.Find (_T ("/IMAGE_ON_EOP")) != -1 || strCmdLine.Find (_T ("/IMAGE-ON-EOP")) != -1) 
								SetImagingMode (IMAGE_ON_EOP);
					}

					CString strFile [] = 
					{
						_T ("usbmphc_2_55.bit"),
						_T ("usbmphc_2_10.bit"),
						_T ("usbmphc_2_02.bit"),
					};
					CStringArray vFirmware;

					for (int i = 0; i < ARRAYSIZE (strFile); i++)
						if (::GetFileAttributes (GetHomeDir () + _T ("\\") + strFile [i]) != -1)
							vFirmware.Add (strFile [i]);

					CString strFirmware = Extract (ToString (vFirmware), _T ("{"), _T ("}"));
					strFirmware.Replace (_T (","), _T (";"));
					TRACEF (strFirmware);

					UPDATE_STATUS (dlg, 0, LoadString (IDS_STARTUSB) + strFirmware + _T ("..."));
					//MARKTRACECOUNT ();
					TRACEF (strFirmware);
					m_pUsbMphcCtrl = new USB::CUsbMphcCtrl (w2a (strFirmware), theApp.m_bDemo, bDemoMemicPC);

					//MARKTRACECOUNT ();
					bool bStart = m_pUsbMphcCtrl->StartService ();
					//MARKTRACECOUNT ();
					TRACEF (_T ("StartService: ") + FoxjetDatabase::ToString (bStart));

					#ifdef _DEBUG
					m_pUsbMphcCtrl->SetDemoPhotocellInterval (4);
					#endif

					//TRACECOUNTARRAYMAX ();
				}
			}
		}
		catch (CMemoryException * e) { HANDLEEXCEPTION (e); return FALSE; }
	}
	#endif //__WINPRINTER__

	if (!ProcessShellCommand(cmdInfo)) {
		MsgBox (_T ("!ProcessShellCommand"));
		return FALSE;
	}

	MARKTRACECOUNT ();

	/*
	#ifndef _DEBUG
	// The one and only window has been initialized, so show and update it.
	m_pMainWnd->ShowWindow( SW_SHOWMAXIMIZED );
	m_pMainWnd->UpdateWindow();
	#endif
	*/

	if (CControlView * pView = DYNAMIC_DOWNCAST (CControlView, GetActiveView ())) 
		pView->SetPreview (FoxjetDatabase::GetProfileInt (CControlApp::GetRegKey () + _T ("\\Settings"), _T ("Preview"), 1) ? true : false);

	FoxjetMessageBox::SetAfxMainWndValid (true);

	if (cmdInfo.m_bShowSplash) {
		CSplashWnd::EnableSplashScreen ();
		CSplashWnd::ShowSplashScreen (::AfxGetMainWnd ());
	}
	
	if (!LoadSecurityOptionsTable ()) {
		MsgBox  (_T ("!LoadSecurityOptionsTable"));
		return FALSE;
	}

	MARKTRACECOUNT ();

	{
		using namespace FoxjetCommon;

		const CString strUser = _T ("/USER=");

		if (strCmdLine.Find (strUser) != -1) {
			CString strUsername = GetCmdlineValue (strCmdLine, strUser);
			CString strPass = GetCmdlineValue (strCmdLine, _T ("/PASS="));

			Login (strUsername, strPass);
			m_strFlags += _T (" /USER=") + theApp.m_strUsername + _T (" /PASS=") + strPass;

			m_bRootUser = !theApp.m_strUsername.CompareNoCase (_T ("ROOT"));

			if (CMainFrame * p = DYNAMIC_DOWNCAST (CMainFrame, GetActiveFrame ())) 
				p->SendMessage (WM_COMMAND, MAKEWPARAM (ID_UPDATE_UI, 0));
		}
	}

	MARKTRACECOUNT ();
	#ifdef _DEBUG
	if (!GetDSN ().CompareNoCase (_T ("sunnyvale"))) 
		TRACECOUNTARRAY ();
	#endif

	SetThreadPriority (nPriority);
	TRACEF (_T ("SetThreadPriority: ") + FoxjetDatabase::ToString (GetThreadPriority ()));

	{
		DWORD dw = 0;
		CString str = _T ("Control::WM_APP_INITIALIZED");
		WPARAM wParam = (WPARAM)::AfxGetMainWnd ()->m_hWnd;
		LPARAM lParam = FoxjetDatabase::NOTFOUND;

		if (CControlDoc * pDoc = (CControlDoc *)GetActiveDocument ()) 
			if (CProdLine * pLine = pDoc->GetSelectedLine ()) 
				lParam = pLine->GetTaskID ();

		::SendMessageTimeout (HWND_BROADCAST, WM_APP_INITIALIZED, wParam, lParam, SMTO_BLOCK | SMTO_ABORTIFHUNG, 100, &dw);
		
		if (HANDLE hEvent = ::CreateEvent (NULL, TRUE, FALSE, str)) {
			::SetEvent (hEvent);
			::CloseHandle (hEvent);
		}
	}

	VERIFY (m_hCallWndProcRet = ::SetWindowsHookEx (WH_CALLWNDPROCRET, (HOOKPROC)CallWndProcRet, ::GetModuleHandle (NULL), ::GetCurrentThreadId ()));

	/* TODO: rem
	{
		DWORD dw = 0;

		::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0, (LPTHREAD_START_ROUTINE)UpdateUiFunc, (LPVOID)0, 0, &dw);
	}
	*/

	if (IsNetworked ()) {
		DWORD dw = 0;
		FoxjetUtils::CNetworkDlg dlg;

		dlg.Load (m_Database);

		Ping::errorPing.m_strName	= FoxjetCommon::CStrobeItem::GetErrorString (ERROR_NETWORK);
		Ping::errorPing.m_green		= STROBESTATE_OFF;
		Ping::errorPing.m_red		= STROBESTATE_OFF;
		Ping::errorPing.m_yellow	= STROBESTATE_ON;
		Ping::errorPing.m_nDelay	= 60;

		LoadPingSettings ();

		_tcsncpy (Ping::szPing, dlg.m_bEnabled ? dlg.m_strAddr : _T (""), ARRAYSIZE (Ping::szPing));
		Ping::errorPing.m_nDelay = dlg.m_nFreq;
		TRACER (_T ("Ping::errorPing.m_nDelay: ") + ToString (Ping::errorPing.m_nDelay) + _T ("\n"));

		m_hPingThread = ::CreateThread ((LPSECURITY_ATTRIBUTES)NULL, 0, (LPTHREAD_START_ROUTINE)PingFunc, (LPVOID)GetActiveView ()->m_hWnd, 0, &dw);

		/*
		#ifdef _DEBUG
		{
			DWORD dwResult = 0;

			LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_LOCAL_DB_BACKUP, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);	
		}
		#endif
		*/
	}


#ifdef __WINPRINTER__
	if (FoxjetUtils::CStartupDlg::IsAutoStartEnabled (theApp.m_Database)) {
		if (CControlDoc * pDoc = DYNAMIC_DOWNCAST (CControlDoc, GetActiveDocument ())) { 
			CControlView * pView = pDoc->GetView ();
			CStringArray lines;

			pDoc->GetProductLines (lines);

			if (theApp.GetCoupledMode ()) {
				if (lines.GetSize () == 2) {
					GlobalFuncs::WriteProfileString (lines [1], GlobalVars::strTask, GlobalFuncs::GetProfileString (lines [0], GlobalVars::strTask, _T ("")));
					GlobalFuncs::WriteProfileString (lines [1], GlobalVars::strState, GlobalFuncs::GetProfileString (lines [0], GlobalVars::strState, GlobalFuncs::GetState (STOPPED)));
					//GlobalFuncs::WriteProfileInt (lines [1], GlobalVars::strCount, GlobalFuncs::GetProfileInt (lines [0], GlobalVars::strCount, 0));
				}
			}
			
			for (int nLine = 0; nLine < lines.GetSize (); nLine++) {
				CString strLine = lines [nLine];
				CString strTask = GlobalFuncs::GetProfileString (strLine, GlobalVars::strTask, "");
				CString strState = GlobalFuncs::GetProfileString (strLine, GlobalVars::strState, GlobalFuncs::GetState (STOPPED));
				__int64 lCount = GlobalFuncs::GetProfileInt (strLine, GlobalVars::strCount, 0);
				CString strKeyValue		= theApp.GetProfileString (_T ("sw0858"), _T ("Key value"));
				int nPeriod				= theApp.GetProfileInt (_T ("sw0858"), _T ("nPeriod"), 0);

				if (!strTask.IsEmpty ()) {
					if (!strState.CompareNoCase (GlobalFuncs::GetState (RUNNING))) {
						int nStart = pView->StartTask (strLine, strTask, true, strKeyValue, -1, nPeriod);

						if (TASK_START_SUCCEEDED (nStart)) {
							const CString strNoData = _T ("[_$no_data]");
							//CString strDebug;

							if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
								pLine->SetCounts (lCount, pLine->m_countLast.m_lPalletMax, pLine->m_countLast.m_lUnitsPerPallet, pLine->m_countLast.m_lPalletCount, pLine->m_countLast.m_lPalletMin);

								{
									const CString strFile = GetHomeDir () + _T ("\\XML\\") + pLine->GetName () + _T ("\\_$CountUntil.txt");
									CString str = FoxjetFile::ReadDirect (strFile, strNoData);

									if (str != strNoData) {
										__int64 n = FoxjetCommon::strtoul64 (str);
										
										if (n > 0)
											pLine->SetCountUntil (n);
									}
								}

								if (FoxjetUtils::CStartupDlg::IsUserDataEnabled (theApp.m_Database)) {
									using namespace FoxjetElements;

									Head::CElementPtrArray v;

									pLine->GetElements (v, USER);
									pLine->GetElements (v, BMP);
									pLine->GetElements (v, COUNT);

									for (int i = 0; i < v.GetSize (); i++) {
										FoxjetCommon::CBaseElement * pBase = v [i];
										CString strPrompt;

										if (CUserElement * pUser = DYNAMIC_DOWNCAST (CUserElement, pBase)) 
											strPrompt = pUser->GetPrompt ();
										else if (CBitmapElement * pBmp = DYNAMIC_DOWNCAST (CBitmapElement, pBase)) 
											strPrompt = pBmp->GetUserPrompt ();
										else if (CCountElement * pCount = DYNAMIC_DOWNCAST (CCountElement, pBase)) 
											strPrompt = pCount->GetName ();

										CString strFile = GetHomeDir () + _T ("\\XML\\") + strPrompt + _T (".txt");
										CString strData = FoxjetFile::ReadDirect (strFile, strNoData);

										//strDebug += strPrompt + _T (" --> ") + strData + _T ("\n");

										if (strData.Compare (strNoData) != 0) {
											if (CUserElement * pUser = DYNAMIC_DOWNCAST (CUserElement, pBase)) {
												pUser->SetDefaultData (strData.GetLength () ? strData : _T (" "));
											}
											else if (CBitmapElement * pBmp = DYNAMIC_DOWNCAST (CBitmapElement, pBase)) {
												pBmp->SetFilePath (strData, pBmp->GetHead ());
												Foxjet3d::UserElementDlg::CUserElementDlg::ResetSize (pBmp, pBmp->GetHead ());
											}
											else if (CCountElement * pCount = DYNAMIC_DOWNCAST (CCountElement, pBase)) {
												__int64 lCount = FoxjetCommon::strtoul64 (strData);

												pCount->SetCount (0);
												pCount->IncrementTo (lCount - 1);
											}
										}
										else {
											//strFile = GetHomeDir () + _T ("\\User prompted\\") + strLine + _T ("\\") + strPrompt + _T (".txt");
											//strData = FoxjetFile::ReadDirect (strFile, strNoData);
											//pLine->SetUserPrompted (v, strPrompt, strData, pLine->GetHeadByID (pBase->GetHead ().m_lID));
										}
									}
								}
							}

							//::MessageBox (NULL, strDebug, NULL, 0);
						}
					}
				}
			}
		}
	}
#endif //__WINPRINTER__

	if (CControlView * pView = DYNAMIC_DOWNCAST (CControlView, GetActiveView ())) {
		CString str = ::GetCommandLine ();

		str.MakeLower ();

		if (str.Find (_T ("/inkcodetest")) != -1) {
			const CString strRootSection = FoxjetDatabase::Encrypted::InkCodes::GetRegSection ();
			std::vector <std::wstring> vCode;
			CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

			FoxjetDatabase::Encrypted::InkCodes::Enable (true);

			vCode.push_back (_T ("LK27C2L52"));
			vCode.push_back (_T ("DK27EKLR7"));
			vCode.push_back (_T ("5S3OCKTB2"));

			//TRACEF (_T ("GetAddressCount: ") + FoxjetDatabase::ToString ((int)nUSB));

			FoxjetDatabase::GetPrinterHeads (theApp.m_Database, GetPrinterID (theApp.m_Database), vHeads);

			for (UINT i = 0; i < vHeads.GetSize (); i++) {
				HEADSTRUCT h = vHeads [i];
				long double dPixel = FoxjetDatabase::CalcInkUsage (h.m_nHeadType, FoxjetDatabase::INKTYPE_FIRST, 1);

				unsigned __int64 nMax = (unsigned __int64)((long double)500.0 / dPixel);
				CString strSection = strRootSection + _T ("\\") + CString (vCode [i].c_str ()) + _T ("<NULL>");

				TRACEF (std::to_string (dPixel).c_str ());
				TRACEF (std::to_string (nMax).c_str ());

				VERIFY (Encrypted::WriteProfileString (strSection, _T ("lHeadID"), ToString (h.m_lID)));
				VERIFY (Encrypted::WriteProfileInt64 (strSection, _T ("Used"), (unsigned __int64)0));
				VERIFY (Encrypted::WriteProfileInt64 (strSection, _T ("Max"), (unsigned __int64)nMax));

				TRACEF (_T ("[ink code] [") + ToString (h.m_lID) + _T ("]: [") + CString (vCode [i].c_str ()) + _T ("] lUsed = ") + 
					ToString (Encrypted::GetProfileInt64 (strSection, _T ("Used"), (unsigned __int64)-1)));

				pView->PostMessage (WM_INK_CODE_CHANGE, (WPARAM)new ControlView::WM_INK_CODE_CHANGE_STRUCT (IDOK, h.m_lID, true));
			}
		}
	}

	if (HWND hwndSkuManager = FoxjetCommon::LocateSkuManager ()) {
		if (HMODULE hLib = GetModuleHandle(_T ("user32.dll"))) {
		   void (__stdcall *SwitchToThisWindow)(HWND, BOOL);

		   (FARPROC &)SwitchToThisWindow = GetProcAddress(hLib, "SwitchToThisWindow");

		   SwitchToThisWindow(hwndSkuManager, TRUE);
		}
	}

	return TRUE;
}

bool CControlApp::Login (const CString & strUser, const CString & strPass)
{
	using namespace FoxjetDatabase;

	USERSTRUCT US;

	// Special case login for ITI only.
	if ( (strUser.CompareNoCase (_T ("Root")) == 0) && (strPass.CompareNoCase (_T ("User")) == 0) ) {
		theApp.m_bRootUser = true;
		theApp.m_strUsername = _T ("ROOT");

		return true;
	}

	if (GetUserRecord (theApp.m_Database, strUser, US) ) {
		if (ComparePasswords (US.m_strPassW, strPass)) {
			CArray <OPTIONSLINKSTRUCT, OPTIONSLINKSTRUCT &> Links;
			if (GetOptionsLinks (theApp.m_Database, US.m_lSecurityGroupID, Links) ) {
				DWORD dwValue;
				bool bValue;
				
				// Reset Security Table.
				{
					CArray <OPTIONSSTRUCT, OPTIONSSTRUCT &> v;

					GetOptionsRecords (theApp.m_Database, v);
					m_SecurityTable.RemoveAll ();

					for (int i = 0; i < v.GetSize (); i++)
						m_SecurityTable.SetAt (v [i].m_lID, false);
				}

				// Assign the new options to the security table.
				OPTIONSLINKSTRUCT OLS;
				for ( int i = 0; i < Links.GetSize(); i++ ) {
					OLS = (OPTIONSLINKSTRUCT) Links.GetAt (i);
					theApp.m_SecurityTable.SetAt (OLS.m_lOptionsID, true);
				}
				
				theApp.m_strUsername = strUser;
				theApp.m_strUsername.MakeUpper();

				theApp.GetActiveView ()->PostMessage (WM_COMMAND, MAKEWPARAM (IDM_LOGIN, 0));
				theApp.UpdateUI ();

				return true;
			}
		}
		else
			MsgBox (LoadString (IDS_INVALID_PASSWORD));
	}
	else
		MsgBox (LoadString (IDS_INVALID_USERNAME));

	return false;
}

// App command to run the dialog
void CControlApp::OnAppAbout()
{
	CStringArray vCmdLine;
	CEventsDlg dlgEvents;

	FoxjetDatabase::TokenizeCmdLine (m_strFlags, vCmdLine);

	{
		bool bFound = false;
		CString strName = GetComputerName ();
		CString strAddr = GetIpAddress ();
		CString strTable;
		ULONG lPrinterID = GetPrinterID (theApp.m_Database);

		if (m_Database.HasTable (Printers::m_lpszTable)) {
			PRINTERSTRUCT p;

			if (GetPrinterRecord (theApp.m_Database, GetPrinterID (theApp.m_Database), p)) {
				strName = p.m_strName;
				strAddr = p.m_strAddress;
				lPrinterID = p.m_lID;
				strTable.Format (_T ("[%s].[%s] "), COdbcDatabase::GetAppDsn (), Printers::m_lpszTable);
				bFound = true;
			}
		}		

		vCmdLine.InsertAt (0, strTable + strName + _T (", ") + strAddr + _T (" [") + ToString (lPrinterID) + _T ("]"));
	}

	vCmdLine.Add (_T ("/DBQ=\"") + Extract (theApp.m_Database.GetConnect (), _T ("DBQ="), _T (";")) + _T ("\""));
	vCmdLine.Add (_T ("/LOCAL_BACKUP_DIR=\"") + GetHomeDir () + _T ("\\") + Extract (theApp.m_Database.GetConnect (), _T ("DSN="), _T (";")) + _T ("\""));
	vCmdLine.Add (_T ("/SELECT=\"") + LoadString (IDS_LOCAL_BACKUP) + _T ("\""));


	{
		CString strKey = FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), _T ("/enum="));

		if (strKey.GetLength ()) {
			std::vector <std::wstring> v;

			TRACEF (FoxjetDatabase::Encrypted::InkCodes::GetRegRootSection ());
			TRACEF (strKey);

			// /enum="\Registry\Machine\SOFTWARE\Foxjet<NULL>\Settings<NULL>\ink codes<NULL>"

			#ifdef _DEBUG
			FoxjetDatabase::Encrypted::Trace (strKey, 10);
			#endif

			FoxjetDatabase::Encrypted::Enumerate (strKey, v);

			if (v.size ()) { 
				CControlDoc * pDoc = (CControlDoc *)GetActiveDocument ();
				CStringArray v;

				vCmdLine.Add (_T ("Ink codes"));
				vCmdLine.Add (CString (' ', 10) + _T ("\tWarning threshold: ") + ToString (INK_CODE_THRESHOLD_WARNING) + _T ("%"));
				vCmdLine.Add (CString (' ', 10) + _T ("Error threshold: ") + ToString (INK_CODE_THRESHOLD_ERROR) + _T ("%"));

				pDoc->GetProductLines (v);

				for (int i = 0; i < v.GetSize (); i++) {
					if (CProdLine * pLine = pDoc->GetProductionLine (v [i])) {
						CPrintHeadList list;

						pLine->GetActiveHeads (list);

						for (POSITION pos = list.GetStartPosition(); pos; ) {
							DWORD dwKey;
							CHead * pHead = NULL;

							list.GetNextAssoc(pos, dwKey, (void *&)pHead);
							ASSERT (pHead);
							CHeadInfo info = CHead::GetInfo (pHead);

							INKCODE code;

							SEND_THREAD_MESSAGE (info, TM_GET_INK_LEVEL, &code);

							int nPct = (double)code.m_lUsed / (double)code.m_lMax * 100.0;

							CString str;
						
							str.Format (_T ("%s%s [%s]: [%s] [%s / %s (%d %%)]"), 
								CString (' ', 10),
								info.m_Config.m_HeadSettings.m_strName,
								ToString (info.m_Config.m_HeadSettings.m_lID), 
								code.m_strCode,
								FoxjetCommon::FormatI64 (code.m_lUsed),
								FoxjetCommon::FormatI64 (code.m_lMax),
								nPct);
							vCmdLine.Add (str);
						}
					}
				}
			}

			for (int i = 0; i < v.size (); i++) {
				std::vector <std::wstring> vSub, vTmp;
				CString strInkCode = CString (v [i].c_str ());
				CString strSubKey = strKey + _T ("\\") + strInkCode;
				unsigned __int64 lMax = 0, lUsed = 0;
				
				strInkCode.Replace (_T ("<NULL>"), _T (""));
				strInkCode = CString (' ', 10) + strInkCode;

				FoxjetDatabase::Encrypted::Enumerate (strSubKey, vSub);

				for (int j = 0; j < vSub.size (); j++) {
					CString strValue = CString (vSub [j].c_str ());
					CString strData = FoxjetDatabase::Encrypted::GetProfileString (strSubKey, strValue, _T (""));

					if (!strValue.CompareNoCase (_T ("Max")))
						lMax = FoxjetCommon::strtoul64 (strData);
					else if (!strValue.CompareNoCase (_T ("Used")))
						lUsed = FoxjetCommon::strtoul64 (strData);

					if (strData.GetLength ())
						vTmp.insert (vTmp.begin (), std::wstring (strValue + _T ("=") + strData));
				}

				if (lMax && lUsed) {
					int nPct = (double)lUsed / (double)lMax * 100.0;

					vTmp.push_back (std::wstring (ToString (nPct) + _T ("%")));
				}

				if (vTmp.size ()) 
					strInkCode += _T (" [") + CString (implode (vTmp, std::wstring (_T (", "))).c_str ()) + _T ("]");

				vCmdLine.Add (strInkCode);
			}
		}
	}

	FoxjetCommon::CAboutDlg dlg (GlobalVars::CurrentVersion, vCmdLine, LoadString (IDS_ABOUTCONTROL));

	CControlDoc * pDoc = (CControlDoc *)theApp.GetActiveDocument ();
	CStringArray vLines;

	ASSERT (pDoc);
	pDoc->GetProductLines (vLines);
	

#ifdef __IPPRINTER__
	FirmwareDlg::CFirmwareDlg dlgFirmware;

	dlgFirmware.m_pParent = &dlg;

	for (int i = 0; i < vLines.GetSize (); i++) {
		if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
			CPrintHeadList list;

			pLine->GetActiveHeads (list);

			for (POSITION pos = list.GetStartPosition(); pos != NULL; ) {
				CHead * pHead = NULL;
				DWORD dwKey = 0;

				list.GetNextAssoc (pos, dwKey, (void *&)pHead);

				if (CRemoteHead * p = DYNAMIC_DOWNCAST (CRemoteHead, pHead)) 
					dlgFirmware.m_vHeads.Add (p);
			}
		}
	}
	
	dlg.AddPage (&dlgFirmware);

#else
	EliteFirmwareDlg::CEliteFirmwareDlg dlgFirmware;
	int nUSB = 0;

	for (int i = 0; i < vLines.GetSize (); i++) {
		if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
			CPrintHeadList list;

			pLine->GetActiveHeads (list);

			for (POSITION pos = list.GetStartPosition(); pos != NULL; ) {
				CHead * pHead = NULL;
				DWORD dwKey = 0;

				list.GetNextAssoc (pos, dwKey, (void *&)pHead);

				if (CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pHead)) {
					CHeadInfo info = CHead::GetInfo (p);
					bool bUSB = false;

					SEND_THREAD_MESSAGE (info, TM_IS_USB, &bUSB);

					if (bUSB)
						nUSB++;

					dlgFirmware.m_vHeads.Add (p);
				}
			}
		}
	}
	
	if (nUSB)
		dlg.AddPage (&dlgFirmware);
#endif 

	::CDebugPage dlgDebug;

	if (theApp.ShowPrintCycle ()) {
		dlg.AddPage (&dlgDebug);
		dlg.SetActivePage (&dlgDebug);
	}

	FoxjetUtils::NetStatDlg::CNetStatDlg dlgNetStat (ListGlobals::defElements.m_strRegSection);

	dlgFirmware.m_psp.dwFlags |= PSH_USECALLBACK;
	dlgFirmware.m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;

	dlgDebug.m_psp.dwFlags |= PSH_USECALLBACK;
	dlgDebug.m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;

	dlgNetStat.m_psp.dwFlags |= PSH_USECALLBACK;
	dlgNetStat.m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;

	if (theApp.ShowNetStatDlg ()) 
		dlg.AddPage (&dlgNetStat);

	{
		CString strCmdLine = ::GetCommandLine ();

		strCmdLine.MakeUpper ();

		if (strCmdLine.Find (_T ("/DEBUG_EVENTS")) != -1) {
			dlg.AddPage (&dlgEvents);
		}
	}

	dlg.DoModal (); 
}

/////////////////////////////////////////////////////////////////////////////
// CControlApp message handlers
#ifndef _AFXDLL
#if _MFC_VER >= 0x0601
#define _AfxDetermineDaoVersion()   (36)
#else
#define _AfxDetermineDaoVersion()   (35)
#endif
#else // dynamically because of DLL
static inline BYTE _AfxDetermineDaoVersion()
{
	BYTE bReturn = 35;

#ifdef _AFXDLL
	AFX_MODULE_STATE* pModuleState = AfxGetModuleState();
	if (pModuleState->m_dwVersion < 0x421)
		bReturn = 30;
	else if (pModuleState->m_dwVersion >= 0x0601)
		bReturn = 36;
#endif // _AFXDLL

	return bReturn;
}
#endif

int CControlApp::ExitInstance() 
{
	FoxjetDatabase::COdbcDatabase & db = m_Database;
	const CString strSrc = FoxjetDatabase::Extract (db.GetConnect (), _T ("DBQ="), _T (";"));
	const CString strDSN = FoxjetDatabase::Extract (db.GetConnect (), _T ("DSN="), _T (";"));
	const bool bOpen = db.IsOpen () ? true : false;
	ULONG lPrinterID = GetPrinterID (db);
	bool bCompact = !db.IsSqlServer ();

	FoxjetMessageBox::SetAfxMainWndValid (false);

	EndIdsThread ();

	if (m_hCallWndProcRet) {
		UnhookWindowsHookEx (m_hCallWndProcRet);
		m_hCallWndProcRet = NULL;
	}

	if (m_hPingThread) {
		_tcscpy (Ping::szPing, _T ("shutdown"));
		Ping::errorPing.m_nDelay = 0;
		VERIFY (::WaitForSingleObject (m_hPingThread, 10000) == WAIT_OBJECT_0);
		m_hPingThread = NULL;
	}

	FoxjetUtils::ExitInstance ();
	VERIFY (FoxjetCommon::ExitInstance (GlobalVars::CurrentVersion));

	if (bOpen)
		db.Close ();

	#ifdef __WINPRINTER__
	if (m_pMphcDriver) {
		delete m_pMphcDriver;
		m_pMphcDriver = NULL;
	}

	if (m_pUsbMphcCtrl) {
		m_pUsbMphcCtrl->StopService ();
		delete m_pUsbMphcCtrl;
		m_pUsbMphcCtrl = NULL;
	}

	if (m_pIVDriver) {
		delete m_pIVDriver;
		m_pIVDriver = NULL;
	}
	#endif //__WINPRINTER__


	{
		CString str = ::GetCommandLine ();

		str.MakeLower ();

		if (str.Find (_T ("/compact")) != -1)
			bCompact = true;
	}

	if (bCompact) {
		if (strSrc.GetLength ()) {
			if (!FoxjetDatabase::IsRunning (ISEDITORRUNNING) && !IsNetworked ()) //(lPrinterID == 0))  
				FoxjetDatabase::Compact (strSrc, strDSN);
		}
	}

	CHead::FreeInfo ();
	CHead::DeleteCriticalSection ();
	Encrypted::Free ();
	CDiagnosticSingleLock::Exit ();

	return CWinApp::ExitInstance();
}

bool CControlApp::LoadSecurityOptionsTable()
{
	using namespace FoxjetDatabase;

	try
	{
		COdbcRecordset rst (m_Database);
		CString str;

		str.Format (_T ("SELECT Count ([%s].[%s]) as expr1 FROM [%s] GROUP BY [%s].[%s] ORDER BY Count([%s].[%s]) DESC;"), 
			Options::m_lpszTable, Options::m_lpszOptionDesc,
			Options::m_lpszTable,
			Options::m_lpszTable, Options::m_lpszOptionDesc, 
			Options::m_lpszTable, Options::m_lpszOptionDesc);
		TRACEF (str);

		rst.Open (str);

		if (!rst.IsEOF ()) {
			int nDuplicates = rst.GetFieldValue ((short)0);

			if (nDuplicates > 1) {
				str.Format (_T ("DELETE * FROM [%s]"), Options::m_lpszTable);
				TRACEF (str);
				m_Database.ExecuteSQL (str);
			}

			rst.Close ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	CArray <OPTIONSSTRUCT, OPTIONSSTRUCT &> v;
	
	GetOptionsRecords (m_Database, v);

	if (CControlView * pView = DYNAMIC_DOWNCAST (CControlView, GetActiveView ())) 
		pView->UpdateOptionsTable (v);

	v.RemoveAll ();

	if (GetOptionsRecords (m_Database, v)) {
		for (int i = 0; i < v.GetSize(); i++) {
			OPTIONSSTRUCT s = v [i];
			CString str = CMainFrame::GetMenuPrompt (s);

			//TRACEF (s.m_strOptionDesc + ": " + str);
			
			if (str.GetLength () == 0) // there is no prompt specified in the menu res editor
				continue;

			s.m_strOptionDesc = str;
			m_SecurityTable.SetAt (s.m_lID, false);
		}

		return true;
	}

	MsgBox (LoadString (IDS_SECURITYTABLEERROR));
	
	return false;
}

bool CControlApp::IsAdmin ()
{
	bool bResult = m_bRootUser;
	USERSTRUCT user;

	if (GetUserRecord (theApp.m_Database, theApp.m_strUsername, user)) {
		SECURITYGROUPSTRUCT group;
			
		if (GetGroupsRecord (theApp.m_Database, user.m_lSecurityGroupID, group)) 
			bResult = group.m_strName.CompareNoCase (_T ("Administrator")) == 0 ? true : false;
	}

	return bResult;
}

bool CControlApp::LoginPermits(unsigned int nCmdID)
{
	bool result = false;
	BOOL bLookup = false;

	if (IsNEXTEditSession ())
		return false;

	//if ( !m_bRootUser ) 
	//	bLookup = m_SecurityTable.Lookup (nCmdID, result);
	//else
	//	result = true;

	if (m_bRootUser) 
		result = true;
	else
		bLookup = m_SecurityTable.Lookup (nCmdID, result);

	return result;
}

void CControlApp::ReportUserActivity(int nActivity)
{

}

BOOL CControlApp::DisplaySystemVersion()
{
	OSVERSIONINFO osvi;
	BOOL bOsVersionInfoEx;
	CString sVersion;
	CString sTemp;

   ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
   osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

   if( !(bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi)) )
         return FALSE;

   switch (osvi.dwPlatformId)
   {
      case VER_PLATFORM_WIN32_NT:

      // Test for the product.

         if ( osvi.dwMajorVersion <= 4 )
            sVersion = _T("Microsoft Windows NT ");

         if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 )
            sVersion = _T("Microsoft Windows 2000 ");

         if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1 )
            sVersion = _T("Microsoft Windows XP ");

        HKEY hKey;
        TCHAR szProductType[80];
        DWORD dwBufLen;

        RegOpenKeyEx( HKEY_LOCAL_MACHINE,
           _T ("SYSTEM\\CurrentControlSet\\Control\\ProductOptions"),
           0, KEY_QUERY_VALUE, &hKey );
        RegQueryValueEx( hKey, _T ("ProductType"), NULL, NULL,
           (LPBYTE) szProductType, &dwBufLen);
        RegCloseKey( hKey );
        if ( lstrcmpi(_T ("WINNT"), szProductType) == 0 )
           sVersion += _T( "Professional " );
        if ( lstrcmpi(_T ("LANMANNT"), szProductType) == 0 )
           sVersion += _T( "Server " );
        if ( lstrcmpi(_T ("SERVERNT"), szProductType) == 0 )
           sVersion += _T( "Advanced Server " );

      // Display version, service pack (if any), and build number.
         if ( osvi.dwMajorVersion <= 4 )
         {
            sTemp.Format(_T ("version %d.%d %s (Build %d)\n"),
               osvi.dwMajorVersion,
               osvi.dwMinorVersion,
               osvi.szCSDVersion,
               osvi.dwBuildNumber & 0xFFFF);
			sVersion += sTemp;
         }
         else
         { 

			 sTemp.Format(_T ("%s (Build %d)\n"), osvi.szCSDVersion, osvi.dwBuildNumber & 0xFFFF);
			 sVersion += sTemp;
         }
         break;

      case VER_PLATFORM_WIN32_WINDOWS:

         if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 0)
         {
             sVersion = _T("Microsoft Windows 95 ");
             if ( osvi.szCSDVersion[1] == 'C' || osvi.szCSDVersion[1] == 'B' )
                sVersion += _T("OSR2 " );
         } 

         if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 10)
         {
             sVersion = _T("Microsoft Windows 98 ");
             if ( osvi.szCSDVersion[1] == 'A' )
                sVersion += _T("SE " );
         } 

         if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 90)
         {
             sVersion = _T("Microsoft Windows Me ");
         } 
         break;

      case VER_PLATFORM_WIN32s:

         sVersion = _T("Microsoft Win32s ");
         break;
   }
   
   MsgBox( sVersion );

   return TRUE; 
}


void CControlApp::ProcessWmChar ()
{
	CControlDoc * pDoc = (CControlDoc *)GetActiveDocument ();
	CControlView * pView = (CControlView *)GetActiveView ();

	CString str;

	for (int i = 0; i < m_vWmChar.size (); i++)
		str += m_vWmChar [i].m_c;

	m_vWmChar.clear ();

	while (!::PostMessage (pView->GetSafeHwnd(), WM_DIAGNOSTIC_DATA, (WPARAM)new CString (str), 0))
		::Sleep (0);

	if (str.GetLength ()) {
		if (CProdLine * pLine = pDoc->GetSelectedLine ()) {
			if (m_dbstart.m_strDSN.GetLength ()) {
				CSerialPacket * p = new CSerialPacket (_T ("COM1"));

				p->AssignData (SERIALSRC_MPHC, SW0858_DBSTART, pLine->GetName (), str, str.GetLength (), 0);

				while (!pView->PostMessage (WM_HEAD_SERIAL_DATA, 0, (LPARAM)p)) 
					::Sleep (0);
			}
			else {
				int n = pLine->StartTask (str, false, false, false, pView);

				if (n != TASK_START_SUCCESS)
					MsgBox (LoadString (IDS_FAILEDTOSTART) + str, MB_ICONERROR, 0);
			}
		}
	}
}

static bool IsChildOf (HWND hChild, HWND hParent)
{
	HWND hLast = NULL;

	for (HWND h = hChild; h; ) {
		//#ifdef _DEBUG
		//TCHAR sz [128] = { 0 };
		//TCHAR szParent [128] = { 0 };
		//CString str;

		//::GetWindowText (h, sz, ARRAYSIZE (sz));
		//::GetWindowText (::GetParent (h), szParent, ARRAYSIZE (szParent));
		//str.Format (_T ("0x%p: %s [0x%p: %s]"), h, sz, ::GetParent (h), szParent);
		//TRACEF (str);
		//#endif

		if (h == hLast)
			break;

		if (h == hParent)
			return true;

		hLast = h;
		h = ::GetParent (hChild);
	}

	return false;
}

BOOL CControlApp::PreTranslateMessage(MSG* pMsg) 
{
	static const int nThresholdMilliseconds = 500;
	bool bChild = false;

	switch (pMsg->message) {
	case WM_CHAR:
		if (bChild = IsChildOf (::GetFocus (), theApp.GetActiveView ()->m_hWnd)) 
		{
			WM_KEY_STRUCT s;

			s.m_c = (TCHAR)pMsg->wParam;
			clock_t last = s.m_clock = clock ();

			if (m_vWmChar.size ()) 
				last = m_vWmChar [m_vWmChar.size () - 1].m_clock;

			double dDiff = ((double)(s.m_clock - last) / CLOCKS_PER_SEC);
			int nDiff = (int)(dDiff * 1000.0);

			//TRACEF (bChild ? _T ("child") : _T ("not child"));
			//TRACEF (CString ((TCHAR)pMsg->wParam) + _T (": ") + ToString (dDiff));

			if (/* (nDiff > nThresholdMilliseconds) || */ (pMsg->wParam == VK_RETURN))
				ProcessWmChar ();
			else
				m_vWmChar.push_back (s);

			return 0;
		}
	case WM_KEYDOWN:
		if (pMsg->wParam == VK_RETURN)
			return 0;
	}

	/*
	if (m_vWmChar.size ()) {
		clock_t last = m_vWmChar [m_vWmChar.size () - 1].m_clock;
		int nDiff = (int)(((double)(clock () - last) / CLOCKS_PER_SEC) * 1000.0);

		if (nDiff > (nThresholdMilliseconds * 2)) 
			ProcessWmChar ();
	}
	*/

	if (CSplashWnd::PreTranslateAppMessage(pMsg))
		return TRUE;
	
	return CWinApp::PreTranslateMessage(pMsg);
}

void CControlApp::SetStatusMessage (const CString & str)
{
}

void CControlApp::SetCoupledMode (bool bCoupled)
{
#ifdef __WINPRINTER__
	m_bCoupledLines = bCoupled;
#else
	m_bCoupledLines = false;
#endif
}

bool CControlApp::GetCoupledMode ()
{
	return m_bCoupledLines;
}

void CControlApp::SetSingleMsgMode (bool bSingle)
{
#ifdef __IPPRINTER__
	m_bSingleMsg = bSingle;
#else
	m_bSingleMsg = false;
#endif
}

bool CControlApp::GetSingleMsgMode ()
{
	return m_bSingleMsg;
}

CMainFrame * CControlApp::GetActiveFrame () const
{
	CMainFrame * pResult = NULL;
	CWinApp * pApp = ::AfxGetApp();
	ASSERT (pApp);
	
	if (pApp->m_pMainWnd) {
		ASSERT_KINDOF (CMainFrame, pApp->m_pMainWnd);
		CMainFrame * pMainWnd = (CMainFrame *)pApp->m_pMainWnd;
		CFrameWnd * pFrame = pMainWnd->GetActiveFrame ();

		pResult = DYNAMIC_DOWNCAST (CMainFrame, pFrame);
	}

	return pResult;
}

CView * CControlApp::GetActiveView() const
{
	CView * pView = NULL;

	if (CMainFrame * pFrame = GetActiveFrame ()) 
		pView = pFrame->GetActiveView();

	return pView;
}

CDocument * CControlApp::GetActiveDocument() const
{
	CView * p = GetActiveView ();
	
	if (p) {
		ASSERT (p->GetDocument ());
		return p->GetDocument ();
	}
	else
		return NULL;
}

bool CControlApp::IsHostRunning () 
{
	return FoxjetDatabase::IsRunning (ISHOSTRUNNING);
}


void CControlApp::CancelStrobe ()
{
	if (CControlView * pView = DYNAMIC_DOWNCAST (CControlView, GetActiveView ())) {
		m_bStrobeCancelled = true;
		m_tmStrobeCancelled = CTime::GetCurrentTime ();
	}
}

bool CControlApp::GetPreview () const
{
	if (CControlView * pView = DYNAMIC_DOWNCAST (CControlView, GetActiveView ())) 
		return pView->GetPreview ();

	return true;
}

void CControlApp::SetStrobe ()
{
	if (CControlView * pView = DYNAMIC_DOWNCAST (CControlView, GetActiveView ())) 
		m_bStrobeCancelled = false;
}

void CControlApp::CacheHubState ()
{
	CControlDoc * pDoc = (CControlDoc *)theApp.GetActiveDocument ();
	CStringArray vLines;

	m_bIsHubConnected = false;
	//TRACEF ("m_bIsHubConnected = false");

	ASSERT (pDoc);
	pDoc->GetProductLines (vLines);
	
	for (int i = 0; i < vLines.GetSize (); i++) {
		if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
			if (pLine->IsAuxBoxConnected ()) {
				m_bIsHubConnected = true;
				TRACEF ("m_bIsHubConnected = true");
				break;
			}
		}
	}
}

bool CControlApp::IsHubConnected ()
{
	return m_bIsHubConnected;
}

BOOL CControlApp::OnIdle(LONG lCount) 
{
	using namespace FoxjetDatabase;

	static CTime tmLast = CTime::GetCurrentTime ();
	static bool bInit = false;
	TCHAR sz [MAX_PATH] = { 0 };

	HMODULE hModule = ::GetModuleHandle (NULL);
	::GetModuleFileName (hModule, sz, ARRAYSIZE (sz) - 1);

	const CString strPath (sz);
	VERIFY (::GetFileTitle (strPath, sz, ARRAYSIZE (sz) - 1) == 0);
	const CString strFile = FoxjetFile::GetFileTitle (sz) + _T ("_exec.log");

	CTime tmNow = CTime::GetCurrentTime ();
	CTimeSpan tmDiff =  tmNow - tmLast;

	if (!m_bDatabaseInUse) {
		CTimeSpan tm = CTime::GetCurrentTime () - m_tmDatabase;
		bool bClose = (UINT)tm.GetTotalSeconds () > m_nDbTimeout;

		if (tm.GetTotalSeconds () < 0)
			bClose = true;

		if (bClose) {
			if (m_Database.IsOpen ()) {
				TRACEF ("m_Database.Close ()");
				m_Database.Close ();
				COdbcDatabase::FreeDsnCache ();

				if (CControlView * pView = DYNAMIC_DOWNCAST (CControlView, GetActiveView ())) 
					pView->OnDbClosed ();
			}
		}
	}

	if (!bInit) {
		const CString strStart = CTime::GetCurrentTime ().Format ("%c");
		const CTime tmTouch = FoxjetCommon::GetFileDate (strFile);
		CString str;

		if (FILE * fp = _tfopen (strFile, _T ("a"))) {
			REPORTSTRUCT rptStart;

			if (::GetFileAttributes (strFile) != -1) {
				REPORTSTRUCT rptTouch;
				
				str = rptTouch.m_strTaskName = tmTouch.Format ("%c");

				if (theApp.IsPrintReportLoggingEnabled ()) AddPrinterReportRecord (theApp.m_Database, FoxjetDatabase::REPORT_EXECSTOP, rptTouch, GetPrinterID (theApp.m_Database));
				str += _T ("\n");
				fwrite (w2a (str), str.GetLength (), 1, fp);
			}

			str = rptStart.m_strTaskName = strStart;
			if (theApp.IsPrintReportLoggingEnabled ()) AddPrinterReportRecord (theApp.m_Database, FoxjetDatabase::REPORT_EXECSTART, rptStart, GetPrinterID (theApp.m_Database));
			str += _T (", ");
			fwrite (w2a (str), str.GetLength (), 1, fp);
			fclose (fp);
		}

		bInit = true;
	}

	if (tmDiff.GetTotalSeconds ()) {
		FoxjetCommon::Touch (strFile);
		tmLast = tmNow;
	}
	
	return CWinApp::OnIdle(lCount);
}

#ifdef __WINPRINTER__
CMphc * CControlApp::GetMPHCDriver ()
{
	if (!m_pMphcDriver) {
		CString str; 
		str.Format (_T ("%s(%d): m_pMphcDriver == NULL"), __FILE__, __LINE__); 
		MsgBox (NULL, str, _T ("Logic error"), 0);
		TRACEF (str);
		ASSERT (0);
	}

	return m_pMphcDriver;
}

USB::CUsbMphcCtrl * CControlApp::GetUSBDriver ()
{
	if (!m_pUsbMphcCtrl) {
		CString str; 
		str.Format (_T ("%s(%d): m_pUsbMphcCtrl == NULL"), __FILE__, __LINE__); 
		MsgBox (NULL, str, _T ("Logic error"), 0);
		TRACEF (str);
		ASSERT (0);
	}

	return m_pUsbMphcCtrl;
}



CIvPhc * CControlApp::GetIVDriver ()
{
	if (!m_pIVDriver) {
		CString str; 
		str.Format (_T ("%s(%d): m_pIVDriver == NULL"), __FILE__, __LINE__); 
		MsgBox (NULL, str, _T ("Logic error"), 0);
		TRACEF (str);
		ASSERT (0);
	}

	return m_pIVDriver;
}

CDriverInterface * CControlApp::GetDriver ()
{
	if (IsMPHC ())
		return GetMPHCDriver ();
	
	return GetIVDriver ();
}
#endif //__WINPRINTER__

void CControlApp::BeginIdsThread ()
{
	#ifdef __WINPRINTER__
	ASSERT (m_pIdsThread == NULL);
	m_pIdsThread = (CIdsThread *)::AfxBeginThread (RUNTIME_CLASS (CIdsThread));
	m_pIdsThread->LoadAddress (m_Database);
	#endif //__WINPRINTER__
}

void CControlApp::EndIdsThread ()
{
	#ifdef __WINPRINTER__
	if (m_pIdsThread) {
		LOCK (m_pIdsThread->m_cs);
		{
			while (!::PostThreadMessage (m_pIdsThread->m_nThreadID, TM_KILLTHREAD, 0, 0))
				::Sleep (0);

			if (m_pIdsThread)
				VERIFY (::WaitForSingleObject (m_pIdsThread->m_hThread, 2000) == WAIT_OBJECT_0);
		}
	}
	#endif //__WINPRINTER__
}

CString CControlApp::GetRegKey ()
{
	return _T ("Software\\Foxjet\\Control");
}

ULONG GlobalFuncs::GetLineID (const CString & strLine)
{
	if (CControlDoc * pDoc = DYNAMIC_DOWNCAST (CControlDoc, theApp.GetActiveDocument ())) 
		if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) 
			return pLine->GetID ();

	return -1;
}

CString GlobalFuncs::GetProfileString (const CString & strLine, const CString & strKey, const CString & strDef)
{
	using namespace FoxjetDatabase;

	ULONG lLineID = GetLineID (strLine);
	SETTINGSSTRUCT s;
	CString strDbKey = lpszSettingsKey + strKey;
	CString strResult = strDef;

	if (GetSettingsRecord (theApp.m_Database, lLineID, strDbKey, s)) 
		strResult = s.m_strData;
	else {
		const CString strRoot = CControlApp::GetRegKey () + _T ("\\") + strLine;
		
		strResult = FoxjetDatabase::GetProfileString (strRoot, strKey, strDef);
	}

	return strResult;
}

DIAGNOSTIC_CRITICAL_SECTION (csCountFile);

__int64 GlobalFuncs::GetProfileInt (const CString & strLine, const CString & strKey, __int64 lDef)
{
	using namespace FoxjetDatabase;

	ULONG lLineID = GetLineID (strLine);
	SETTINGSSTRUCT s;
	CString strDbKey = lpszSettingsKey + strKey;
	__int64 lResult = lDef;

	if (!strKey.CompareNoCase (_T ("Count"))) {
		static const CString strDir = FoxjetDatabase::GetHomeDir ();

		for (int i = 1; i <= 2; i++) {
			CString strFile;
			
			strFile.Format (_T ("%s\\[%d] Count%d.log"), strDir, lLineID, i);

			LOCK (csCountFile);

			if (FILE * fp = _tfopen (strFile, _T ("r"))) {
				char sz [128] = { 0 };

				//TRACEF (strFile);
				fread (sz, ARRAYSIZE (sz) - 1, 1, fp);
				fclose (fp);

				CControlView::CPhotocellCount c;

				if (c.FromString (a2w (sz))) {
					CControlDoc * pDoc = (CControlDoc *)theApp.GetActiveDocument ();

					if (CProdLine * pLine = pDoc->GetProductionLine (c.m_strLine)) 
						pLine->m_countLast = c.m_count;

					return c.m_count.m_lCount;
				}
			}
			else
				TRACEF (_T ("Failed to open: ") + strFile);
		}
	}

	if (GetSettingsRecord (theApp.m_Database, lLineID, strDbKey, s)) 
		lResult = _ttol (s.m_strData);
	else {
		const CString strRoot = CControlApp::GetRegKey () + _T ("\\") + strLine;
		
		lResult = FoxjetCommon::strtoul64 (FoxjetDatabase::GetProfileString (strRoot, strKey, FoxjetDatabase::ToString (lDef)));
	}

	return lResult;
}

void GlobalFuncs::UpdateSettings (const FoxjetDatabase::SETTINGSSTRUCT & s)
{
	CString str, strKey = s.m_strKey, strData = s.m_strData;
	SETTINGSSTRUCT tmp;

	strKey.Replace (_T ("'"), _T (""));
	strData.Replace (_T ("'"), _T (""));

	if (!strData.GetLength ()) 
		strData = _T (" ");

	try
	{
		if (!GetSettingsRecord (theApp.m_Database, s.m_lLineID, strKey, tmp)) {
			str.Format (
				_T ("INSERT INTO [%s] ([%s], [%s], [%s]) VALUES (%d, '%s', '%s');"),
				Settings::m_lpszTable,
				Settings::m_lpszLineID,	Settings::m_lpszKey,	Settings::m_lpszData,	
				s.m_lLineID,			strKey,					strData);
		}
		else {
			str.Format (
				_T ("UPDATE [%s] SET [%s]='%s' WHERE ([%s]=%d AND [%s]='%s');"),
				Settings::m_lpszTable,
				Settings::m_lpszData,	strData,
				Settings::m_lpszLineID,	s.m_lLineID,
				Settings::m_lpszKey,	strKey);
		}

		TRACEF (str);
		theApp.m_Database.ExecuteSQL (str);
	}
	catch (CDBException * e)		{ TRACEF (strKey + _T (": ") + strData); HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ TRACEF (strKey + _T (": ") + strData); HANDLEEXCEPTION (e); }
}

void GlobalFuncs::WriteProfileString (const CString & strLine, const CString & strKey, const CString & str)
{
	using namespace FoxjetDatabase;

	const CString strRoot = CControlApp::GetRegKey () + _T ("\\") + strLine;
	SETTINGSSTRUCT s;

	s.m_lLineID	= GetLineID (strLine);
	s.m_strKey	= lpszSettingsKey + strKey;
	s.m_strData	= str;

	FoxjetDatabase::WriteProfileString (strRoot, strKey, str);

	UpdateSettings (s);
}

void GlobalFuncs::WriteProfileInt (const CString & strLine, const CString & strKey, __int64 lValue)
{
	using namespace FoxjetElements;
	using namespace FoxjetDatabase;

	const CString strRoot = CControlApp::GetRegKey () + _T ("\\") + strLine;
	SETTINGSSTRUCT s;

	s.m_lLineID	= GetLineID (strLine);
	s.m_strKey	= lpszSettingsKey + strKey;
	s.m_strData.Format (_T ("%I64d"), lValue);

	FoxjetDatabase::WriteProfileString (strRoot, strKey, s.m_strData);

	UpdateSettings (s);

	if (!strKey.CompareNoCase (_T ("Count"))) {
		static const CString strDir = FoxjetDatabase::GetHomeDir ();
		CControlDoc * pDoc = (CControlDoc *)theApp.GetActiveDocument ();

		if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
			CControlView::CPhotocellCount c (lValue, pLine->GetID (), pLine->GetName ());
			Head::CElementPtrArray v;

			pLine->GetElements (v, COUNT);

			for (int i = 0; i < v.GetSize (); i++) {
				if (CCountElement * p = DYNAMIC_DOWNCAST (CCountElement, v [i])) {
					if (p->IsIrregularPalletSize ()) {
						if (p->IsPalletCount ()) {
							c.m_count.m_lPalletCount	= p->GetPalCount ();
							c.m_count.m_lPalletMin		= p->GetPalMin ();
							c.m_count.m_lPalletMax		= p->GetPalMax ();
							c.m_count.m_lUnitsPerPallet = p->GetMax ();
						}
					}
				}
			}

			for (int i = 1; i <= 2; i++) {
				CString strFile;
				CString str = c.ToString ();

				strFile.Format (_T ("%s\\[%d] Count%d.log"), strDir, s.m_lLineID, i);

				LOCK (csCountFile);

				if (FoxjetFile::IsWriteDirect ()) {
					//pDoc->GetView ()->UpdateSerialData (_T ("/write_direct: ") + str + _T (" ") + CString (_T (__FILE__)) + _T (" ") + strFile);
					FoxjetFile::WriteDirect (strFile, (LPBYTE)(LPVOID)w2a (str), str.GetLength ());
				}
				else {
					//pDoc->GetView ()->UpdateSerialData (str + _T (" ") + CString (_T (__FILE__)) + _T (" ") + strFile);

					if (FILE * fp = _tfopen (strFile, _T ("w"))) {
						fwrite ((LPVOID)w2a (str), str.GetLength (), 1, fp);
						fflush (fp);
						fclose (fp);
						::Sleep (0);
						// NOTE: file is read by GlobalFuncs::GetProfileInt
					}
					else
						TRACEF (_T ("Failed to open: ") + strFile + _T (" --> ") + FORMATMESSAGE (::GetLastError ()));
				}
			}
		}
	}
}

CString GlobalFuncs::GetState (FoxjetDatabase::TASKSTATE state)
{
	for (int i = 0; i < ARRAYSIZE (::mapState); i++)
		if (::mapState [i].m_state == state)
			return LoadString (::mapState [i].m_nID);

	return _T ("");
}

FoxjetDatabase::TASKSTATE GlobalFuncs::GetState (const CString & str)
{
	for (int i = 0; i < ARRAYSIZE (::mapState); i++)
		if (!str.CompareNoCase (LoadString (::mapState [i].m_nID)))
			return ::mapState [i].m_state;

	return FoxjetDatabase::STOPPED;
}

HWND GlobalFuncs::GetControlWnd ()
{
	if (CMainFrame * pFrame = (CMainFrame *)theApp.m_pMainWnd)
		if (CView * pView = pFrame->m_pControlView)
			return pView->GetSafeHwnd();

	return NULL;
}

bool CControlApp::IsPrintReportLoggingEnabled ()
{
	return m_bPrintReport;
}

bool CControlApp::IsScanReportLoggingEnabled ()
{
	return m_bScanReport;
}

void CControlApp::EnablePrintReportLogging (bool bEnable)
{
	using namespace FoxjetDatabase;

	SETTINGSSTRUCT s;

	s.m_strKey	= _T ("Control::print report");
	s.m_lLineID = GetPrinterID (m_Database);
	s.m_strData = bEnable ? _T ("1") : _T ("0");

	if (!UpdateSettingsRecord (m_Database, s))
		VERIFY (AddSettingsRecord (m_Database, s));

	m_bPrintReport = bEnable;
}

void CControlApp::EnableScanReportLogging (bool bEnable)
{
	using namespace FoxjetDatabase;

	SETTINGSSTRUCT s;

	s.m_strKey	= _T ("Control::scan report");
	s.m_lLineID = GetPrinterID (m_Database);
	s.m_strData = bEnable ? _T ("1") : _T ("0");

	if (!UpdateSettingsRecord (m_Database, s))
		VERIFY (AddSettingsRecord (m_Database, s));

	m_bScanReport = bEnable;
}

void CControlApp::LoadDbStartParams ()
{
	FoxjetUtils::CDatabaseStartDlg dlg (NULL);
	
	dlg.Load (theApp.m_Database);

	theApp.m_dbstart.m_strDSN		= dlg.m_strDSN;
	theApp.m_dbstart.m_strTable		= dlg.m_strTable;
	theApp.m_dbstart.m_strKeyField	= dlg.m_strKeyField;
	theApp.m_dbstart.m_strTaskField	= dlg.m_strTaskField;
	theApp.m_dbstart.m_nSQLType		= dlg.m_nSQLType;
	theApp.m_dbstart.m_bShow		= dlg.m_bShow;
}

CULongArray CControlApp::GetDriverAddressList ()
{

	CULongArray v;
	//unsigned int cnt = 1;

#ifdef __WINPRINTER__
	/*
	if (m_pMphcDriver) 
		cnt = m_pMphcDriver->GetAddressCount();
	else if (m_pIVDriver) 
		cnt = m_pIVDriver->GetAddressCount();
	else {
		CString str; 
		str.Format (_T ("%s(%d): pDriver == NULL"), __FILE__, __LINE__); 
		MsgBox (NULL, str, _T ("Logic error"), 0);
		TRACEF (str);
		ASSERT (0);
	}

	PULONG pAddressList = new ULONG [cnt];

	if (m_pMphcDriver) 
		m_pMphcDriver->GetAddressList (pAddressList, cnt);
	else if (m_pIVDriver) 
		m_pIVDriver->GetAddressList (pAddressList, cnt);

	for (UINT i = 0; i < cnt; i++)
		v.Add (pAddressList [i]);

	delete [] pAddressList;
	*/

	if (USB::CUsbMphcCtrl * pUSB = m_pUsbMphcCtrl) {
		if (UINT nUSB = pUSB->GetAddressCount ()) {
			PULONG pTmp = new ULONG [nUSB];
			CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
			bool bPrompt = false;

			//TRACEF (_T ("GetAddressCount: ") + FoxjetDatabase::ToString ((int)nUSB));

			pUSB->GetAddressList (pTmp, nUSB);
			FoxjetDatabase::GetPrinterHeads (theApp.m_Database, GetPrinterID (theApp.m_Database), vHeads); //FoxjetDatabase::GetHeadRecords (theApp.m_Database, vHeads);

			for (UINT i = 0; i < nUSB; i++) {
				//TRACEF (FoxjetDatabase::ToString ((int)i) + _T (": ") + FoxjetDatabase::ToString ((int)pTmp [i]));
				v.Add (pTmp [i]);
			}

			delete [] pTmp;

			for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
				HEADSTRUCT & h = vHeads [nHead];
				ULONG lAddr = _tcstoul (h.m_strUID, NULL, 16);
				bPrompt |= (lAddr >= 0x300);

				switch (lAddr) {
				case 0x300:		h.m_strUID = _T ("1");		break;
				case 0x310:		h.m_strUID = _T ("2");		break;
				case 0x320:		h.m_strUID = _T ("3");		break;
				case 0x330:		h.m_strUID = _T ("4");		break;
				case 0x340:		h.m_strUID = _T ("5");		break;
				case 0x350:		h.m_strUID = _T ("6");		break;
				case 0x360:		h.m_strUID = _T ("7");		break;
				case 0x370:		h.m_strUID = _T ("8");		break;
				}
			}

			if (bPrompt) {
				CString str = LoadString (IDS_CONVERTTOUSB);

				if (MsgBox (str, MB_YESNO, 0) == IDYES) {
					for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
						HEADSTRUCT & h = vHeads [nHead];

						VERIFY (UpdateHeadRecord (theApp.m_Database, h));
					}
				}
			}
		}
	}

	/* 
	#ifdef _DEBUG
	{
		for (int i = 0; i < v.GetSize (); i++) {
			CString str;
			str.Format (_T ("v [%d] = 0x%p"), i, v [i]);
			TRACEF (str);
		}
	}
	#endif
	*/

#endif //__WINPRINTER__

	return v;
}

static int Replace (CString & str, const CString & strFind, const CString & strReplace = _T ("")) 
{
	CString strUpper (str);
	CString strFindUpper (strFind);
	int nResult = 0;
	int nIndex = 0;

	strUpper.MakeUpper ();
	strFindUpper.MakeUpper ();
	
	while ((nIndex = strUpper.Find (strFindUpper, nIndex)) != -1) {
		strUpper.Delete (nIndex, strFindUpper.GetLength ());
		strUpper.Insert (nIndex, strReplace);
		nIndex += strReplace.GetLength ();

		str.Delete (nIndex, strFindUpper.GetLength ());
		str.Insert (nIndex, strReplace);

		nResult++;
	}

	return nResult;;
}

static CString Concat (const CString & str, const CString & strAdd)
{
	CStringArray v;
	CString s (strAdd);
	CString strResult;

	s.TrimLeft ();
	s.TrimRight ();

	FoxjetDatabase::TokenizeCmdLine (str, v);

	for (int i = 0; i < v.GetSize (); i++) 
		if (!v [i].CompareNoCase (s))
			return str;

	v.Add (s);

	for (int i = 0; i < v.GetSize (); i++) 
		strResult += v [i] + _T (" ");

	strResult.TrimLeft ();
	strResult.TrimRight ();

	return strResult;
}

CString CControlApp::GetCommandLine () const
{
	CDbConnection conn (_T (__FILE__), __LINE__); 
	
	if (!conn.IsOpen ()) 
		return _T ("");

	CString strCmd = ::GetCommandLine ();
	TCHAR szPath [MAX_PATH] = { 0 };

	::GetModuleFileName (::GetModuleHandle (NULL), szPath, sizeof (szPath) - 1);
	CString strPath = szPath;

	{
		int nStart = strCmd.Find (_T ("/user"));

		if (nStart != -1) {
			int nEnd = strCmd.Find (_T ("/"), nStart + 1);

			if (nEnd != -1)
				strCmd.Delete (nStart, nEnd - nStart);
			else
				strCmd = strCmd.Left (nStart);
		}
	}

	if (!Replace (strCmd, _T ("\"") + strPath + _T ("\""), _T (""))) 
		Replace (strCmd, strPath, _T (""));

	if (m_bDemo) 
		strCmd = Concat (strCmd, _T ("/demo "));

	if (theApp.IsExpo ()) 
		strCmd = Concat (strCmd, _T ("/expo "));

	strCmd = Concat (strCmd, _T (" /nosplash"));

	if (!IsNetworked ()) 
		strCmd = Concat (strCmd, _T (" /dsn=") + GetDSN ());
	else {
		CString str = FoxjetDatabase::Extract (m_Database.GetConnect (), _T ("DSN="), _T (";"));
		
		if (!str.CompareNoCase (theApp.GetLocalDSN ()))
			strCmd = Concat (strCmd, _T (" /dsn=") + theApp.GetLocalDSN ());
		else
			strCmd = Concat (strCmd, _T (" /dsn=") + theApp.GetDSN ());
	}

	if (FoxjetDatabase::IsDriverInstalled (GUID_INTERFACE_IVPHC))
		strCmd = Concat (strCmd, _T (" /VX"));
	else if (FoxjetDatabase::IsDriverInstalled (GUID_INTERFACE_FXMPHC))
		strCmd = Concat (strCmd, _T (" /MPHC"));

	#ifdef __WINPRINTER__
	if (FoxjetCommon::IsProductionConfig ())
		strCmd = Concat (strCmd, _T (" /PRODUCTION"));
	#endif //__WINPRINTER__

	{
		CStringArray v;

		FoxjetDatabase::TokenizeCmdLine (m_strFlags, v);

		for (int i = 0; i < v.GetSize (); i++) 
			strCmd = Concat (strCmd, v [i]);
	}

	Replace (strCmd, _T ("/user=") + m_strUsername);
	strCmd = Concat (strCmd, _T (" /user=\"") + m_strUsername + _T ("\" "));

	if (strCmd.Find (_T ("/PrinterID=")) == -1)
		strCmd += _T (" /PrinterID=") + ToString (GetPrinterID (theApp.m_Database)); // sw0867

	TRACEF (strCmd);

	return strCmd;
}

CString CControlApp::GetPreviewDirectory()
{
	return GetHomeDir() + _T("\\Preview");
}

static ULONG CALLBACK PostViewRefreshFunc (LPVOID lpData)
{
	DWORD dwSleep = (DWORD)lpData;

	/*
	#ifdef _DEBUG
	CString str;
	str.Format (_T ("[%d] PostViewRefreshFunc: %d..."), ::GetCurrentThreadId (), dwSleep);
	TRACEF (str);
	#endif
	*/

	::Sleep (dwSleep);

	if (CMainFrame * pFrame = theApp.GetActiveFrame ()) {
		if (CView * pView = pFrame->GetActiveView ()) {
			/*
			#ifdef _DEBUG
			str.Format (_T ("[%d] ID_VIEW_REFRESHPREVIEW"), ::GetCurrentThreadId ());
			TRACEF (str);
			#endif
			*/

			pView->PostMessage (WM_COMMAND, MAKEWPARAM (ID_VIEW_REFRESHPREVIEW, 0));
		}
	}

	return 0;
}

void CControlApp::PostViewRefresh (DWORD dwWait)
{
	DWORD dw = 0;

	::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)PostViewRefreshFunc, (LPVOID)dwWait, 0, &dw);
}

long CALLBACK CControlApp::CallWndProcRet (int nCode, WPARAM wParam, LPARAM lParam)
{
	if (CWPRETSTRUCT * p = (CWPRETSTRUCT *)lParam) {
		switch (p->message) {
		case WM_INITDIALOG:
			FoxjetCommon::SetAutoButtonText (p->hwnd);
			break;
		case WM_COMMAND:
			UINT nMsg = HIWORD (p->wParam);
			UINT nID = LOWORD (p->wParam);

			switch (nID) {
			case IDOK:
			case IDCANCEL:
			case IDABORT:
			case IDRETRY:
			case IDIGNORE:
			case IDYES:
			case IDNO:
			case IDCLOSE:
			case IDHELP:
				break;
			default:
				switch (nMsg) {
				case BN_CLICKED:
					FoxjetCommon::SetAutoButtonText (p->hwnd);
					break;
				}
			}
		}
	}

	return ::CallNextHookEx (theApp.m_hCallWndProcRet, nCode, wParam, lParam);
}


bool CControlApp::IsPC () 
{ 
	return ::GetSystemMetrics (SM_CXHSCROLL) < 20 ? true : false; 
}

bool CControlApp::DoConnectDatabase (FoxjetDatabase::COdbcDatabase & db, const CString & strDSN, const CString & strLocalDSN, const CString & strDatabase)
{
	bool bResult = false;

	if (!db.IsOpen ())  {

		FoxjetDatabase::SetExceptionCount (_T (""));

		if (!IsNetworked ())
			bResult = FoxjetCommon::OpenDatabase (db, strDSN, strDatabase);
		else {
			CString strNetwork = GetDBQ (strDSN);
			bool bNetwork = true;

			if (strNetwork.GetLength ()) {
				//ConnectMappedDrive (strNetwork);

				bNetwork = (::GetFileAttributes (strNetwork) != -1) ? true : false;
			}

			if (bNetwork) 
				bResult = FoxjetCommon::OpenDatabase (db, strDSN, strDatabase, false);

			if (!bResult)
				if (strLocalDSN.GetLength ())
					bResult = FoxjetCommon::OpenDatabase (db, strLocalDSN, strDatabase);
		}

		if (bResult) {
			CString strDSN = Extract (db.GetConnect (), _T ("DSN="), _T (";"));

			m_tmDatabaseOpened = CTime::GetCurrentTime ();
			ListGlobals::SetDB (db);

			if (m_strLastDSN.CompareNoCase (strDSN) != 0) {
				::PostMessage (HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, 0);
				m_strLastDSN = strDSN;

				if (CControlView * pView = DYNAMIC_DOWNCAST (CControlView, GetActiveView ())) 
					pView->OnDbOpened ();
			}
		}
	}
	else
		bResult = true;

	return bResult;
}

bool CControlApp::ReconnectDatabase ()
{
	bool bResult = false;
	CString strDatabase = theApp.GetDSN (); //FoxjetCommon::GetDatabaseName();
	CConnectDlg dlg;

	CControlDoc * pDoc = (CControlDoc *)theApp.GetActiveDocument ();
	CControlView * pView = pDoc->GetView ();
	HINSTANCE hInstance = ::AfxGetInstanceHandle ();
	HRSRC hResource = ::FindResource(hInstance, MAKEINTRESOURCE(IDD_CONNECT), RT_DIALOG);
	HGLOBAL hTemplate = LoadResource(hInstance, hResource);
	CTimeSpan tm = CTime::GetCurrentTime () - m_tmDatabase;
	bool bClose = (UINT)tm.GetTotalSeconds () > m_nDbTimeout;

	if (tm.GetTotalSeconds () < 0)
		bClose = true;

	VERIFY (dlg.CreateIndirect(hTemplate));
	FreeResource(hTemplate);

	dlg.ShowWindow (SW_SHOW);
	dlg.BringWindowToTop ();
	dlg.CenterWindow ();
	dlg.UpdateWindow ();

	dlg.SetDlgItemText (TXT_TEXT, "Closing database: " + GetDSN () + "...");  dlg.UpdateWindow ();

	dlg.SetDlgItemText (TXT_TEXT, "Opening database: " + GetDSN () + "...");  dlg.UpdateWindow ();

	bResult = DoConnectDatabase (m_Database, GetDSN (), GetLocalDSN (), sDatabase);

	if (!bResult)
		MsgBox (NULL, LoadString (IDS_FAILEDTOOPENDB), ::AfxGetAppName (), MB_ICONERROR | MB_OK);
	else
		theApp.m_tmDatabase = CTime::GetCurrentTime ();

	{
		CStringArray vLines;

		ASSERT (pDoc);
		pDoc->GetProductLines (vLines);
		
		for (int i = 0; i < vLines.GetSize (); i++) {
			if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
				CPrintHeadList list;

				pLine->GetActiveHeads (list);

				for (POSITION pos = list.GetStartPosition(); pos != NULL; ) {
					CHead * pHead = NULL;
					DWORD dwKey = 0;
					bool bResult = false;

					list.GetNextAssoc (pos, dwKey, (void *&)pHead);
					CHeadInfo info = Head::CHead::GetInfo (pHead);

					dlg.SetDlgItemText (TXT_TEXT, info.GetFriendlyName () + _T (": Opening database: ") + GetDSN () + _T ("..."));  dlg.UpdateWindow ();
					pView->ReconnectDatabase (pHead);
				}
			}
		}
	}

	FoxjetDatabase::COdbcDatabase::TraceInstances ();

	return bResult;
}

void CControlApp::ColorsChanged ()
{
	COLORREF rgb = FoxjetUtils::GetButtonColor (_T ("background"));

	SetDialogBkColor (rgb);
}

void CControlApp::UpdateUI ()
{
	((CMainFrame *)theApp.m_pMainWnd)->PostMessage (WM_COMMAND, MAKEWPARAM (ID_UPDATE_UI, 0));
}

int CControlApp::DoMessageBox (LPCTSTR lpszPrompt, UINT nType, UINT nIDPrompt)
{
	// catches AfxMessageBox 

	return CWinApp::DoMessageBox (lpszPrompt, nType, nIDPrompt);
}

void CControlApp::LoadPingSettings ()
{
	if (IsNetworked ()) {
		DBCONNECTION (conn);

		if (!conn.OpenExclusive (GetActiveView ()))
			return;

		COdbcDatabase & db = theApp.m_Database;
		CArray <FoxjetCommon::CStrobeItem, FoxjetCommon::CStrobeItem &> v;
		FoxjetUtils::CNetworkDlg dlg;

		dlg.Load (db);
		FoxjetCommon::CStrobeDlg::Load (db, v);

		::EnterCriticalSection (&Ping::csPing);

		Ping::bEnabled = dlg.m_bEnabled ? true : false;

		TRACEF (_T ("Ping::bEnabled: ") + ToString (Ping::bEnabled));

		for (int i = 0; i < v.GetSize (); i++) {
			FoxjetCommon::CStrobeItem & e = v [i];

			if (e.GetErrorType (e.m_strName) == ERROR_NETWORK) {
				Ping::errorPing = e;
				Ping::bTrigger = true;
			}
		}

		::LeaveCriticalSection (&Ping::csPing);
	}
}

bool CControlApp::IsLocalDB ()
{
	const CString strLocal = GetLocalDSN ();
	bool bResult = false;

	if (strLocal.GetLength () && m_Database.IsOpen ()) {
		CString str = Extract (m_Database.GetConnect (), _T ("DSN="), _T (";"));

		bResult = str.CompareNoCase (strLocal) == 0;
	}

	return bResult;
}

CString Count (const FoxjetCommon::CLongArray & v, int nMask)
{
	CString str;

	for (int i = 0; i < v.GetSize (); i++) 
		str += (v [i] & nMask) ? _T ("1") : _T ("0");

	return str;
}

void CControlApp::SetStrobeColor (int nStrobe) 
{ 
	const int n = 4;
	static FoxjetCommon::CLongArray v;
	static const LPCTSTR lpszFlash = _T ("010101010101010101010101010101010101010101010101010101010101");
	static const CString strSolid ('1', n);
	static const CString strFlash1 (lpszFlash, n);
	static const CString strFlash2 (&lpszFlash [1], n);
	

	ASSERT ((n % 2) == 0);

	while (v.GetSize () >= n)
		v.RemoveAt (0);

	v.Add (nStrobe);
	m_nStrobe = nStrobe; 
//	TRACEF (_T ("nStrobe: ") + ToString (m_nStrobe));

	{
		m_vStrobeState.RemoveAll ();
		CString strGreen	= Count (v, GREEN_STROBE);
		CString strYellow	= Count (v, YELLOW_STROBE);
		CString strRed		= Count (v, RED_STROBE);

		if (!strGreen.CompareNoCase (strSolid))
			m_vStrobeState.Add (_T ("GREEN_STROBE"));
		else if (!strGreen.CompareNoCase (strFlash1) || !strGreen.CompareNoCase (strFlash2))
			m_vStrobeState.Add (_T ("GREEN_STROBE_FLASH"));

		if (!strYellow.CompareNoCase (strSolid))
			m_vStrobeState.Add (_T ("YELLOW_STROBE"));
		else if (!strYellow.CompareNoCase (strFlash1) || !strYellow.CompareNoCase (strFlash2))
			m_vStrobeState.Add (_T ("YELLOW_STROBE_FLASH"));

		if (!strRed.CompareNoCase (strSolid))
			m_vStrobeState.Add (_T ("RED_STROBE"));
		else if (!strRed.CompareNoCase (strFlash1) || !strRed.CompareNoCase (strFlash2))
			m_vStrobeState.Add (_T ("RED_STROBE_FLASH"));
	}
}

CString CControlApp::GetStrobeState () const 
{ 
	return ToString (m_vStrobeState); 
}
