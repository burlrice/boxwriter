// DynDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "DynDataDlg.h"
#include "TemplExt.h"
#include "Extern.h"
#include "Registry.h"
#include "Debug.h"
#include "fj_defines.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace ListGlobals;
using namespace ItiLibrary;

using namespace DynDataDlg;

CDataItem::CDataItem (LONG lID, const CString & str)
:	m_lID (lID),
	m_strData (str)
{
}

CDataItem::~CDataItem ()
{
}

CString CDataItem::GetDispText (int nColumn) const
{
	CString str;
	switch (nColumn) {
	case 0:		str = m_strData;				break;
	case 1:		str.Format (_T ("%d"), m_lID);	break;
	}

	return str;
}
/////////////////////////////////////////////////////////////////////////////
// CDynDataDlg dialog


CDynDataDlg::CDynDataDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg (IDD_DYNDATA_MATRIX /* IsMatrix () ? IDD_DYNDATA_MATRIX : IDD_DYNDATA */ , pParent)
{
	//{{AFX_DATA_INIT(CDynDataDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDynDataDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDynDataDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDynDataDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDynDataDlg)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_NOTIFY(NM_DBLCLK, LV_DATA, OnDblclkData)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDynDataDlg message handlers

void CDynDataDlg::OnEdit() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
	}
	else 
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CDynDataDlg::OnDblclkData(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();
	
	if (pResult)
		* pResult = 0;
}


BOOL CDynDataDlg::OnInitDialog() 
{
	using namespace ListCtrlImp;

	CString strSection = defElements.m_strRegSection + _T ("\\CDynDataDlg");
	CArray <CColumn, CColumn> vCols;

	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	vCols.Add (CColumn (LoadString (IDS_ID)));
	vCols.Add (CColumn (LoadString (IDS_DATA)));

	m_lv.Create (LV_DATA, vCols, this, defElements.m_strRegSection);

	for (int i = 0; i < m_vData.GetSize (); i++) 
		m_lv.InsertCtrlData (new CDataItem (i, m_vData [i]));

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
