// BaxterConfigDlg.cpp : implementation file
//

#include "stdafx.h"
#include "control.h"
#include "BaxterConfigDlg.h"
#include "BaxterFormatDlg.h"
#include "Debug.h"
#include "Database.h"
#include "Parse.h"

using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBaxterConfigDlg dialog

/*
IDD_BAXTERCONFIG_ DIALOG DISCARDABLE  0, 0, 260, 236
STYLE DS_MODALFRAME | WS_POPUP | WS_CAPTION | WS_SYSMENU
CAPTION "Serial data configuration"
FONT 12, "Microsoft Sans Serif"
BEGIN
    GROUPBOX        "Overture barcode",IDC_STATIC,7,7,246,65
    LTEXT           "Message length",IDC_STATIC,16,19,59,8
    EDITTEXT        TXT_TASK_LEN,84,19,40,14,ES_AUTOHSCROLL
    LTEXT           "Maximum length",IDC_STATIC,16,44,59,8,NOT WS_VISIBLE
    EDITTEXT        TXT_OVERTURE_MAX,84,44,40,14,ES_AUTOHSCROLL | NOT 
                    WS_VISIBLE
    LTEXT           "Data 1 length",IDC_STATIC,131,19,59,8
    EDITTEXT        TXT_OVERTURE_LEN1,199,19,40,14,ES_AUTOHSCROLL
    LTEXT           "Data 2 length",IDC_STATIC,131,35,59,8
    EDITTEXT        TXT_OVERTURE_LEN2,199,35,40,14,ES_AUTOHSCROLL
    LTEXT           "Data 3 length",IDC_STATIC,131,51,59,8,NOT WS_VISIBLE
    EDITTEXT        TXT_OVERTURE_LEN3,199,51,40,14,ES_AUTOHSCROLL | NOT 
                    WS_VISIBLE
    GROUPBOX        "Primary barcode",IDC_STATIC,7,73,122,33
    EDITTEXT        TXT_PRIMARY_MAX,83,86,40,14,ES_AUTOHSCROLL
    GROUPBOX        "Secondary barcode",IDC_STATIC,131,73,122,33
    LTEXT           "Maximum length",IDC_STATIC,139,86,59,8
    EDITTEXT        TXT_SECONDARY_MAX,207,86,40,14,ES_AUTOHSCROLL
    GROUPBOX        "User data",IDC_STATIC,7,109,122,33
    LTEXT           "Maximum length",IDC_STATIC,15,122,59,8
    EDITTEXT        TXT_USER_LEN_OPTIONAL,83,122,40,14,ES_AUTOHSCROLL
    LTEXT           "Output string format",IDC_STATIC,7,144,121,8
    EDITTEXT        TXT_OUTPUT_FORMAT,7,155,246,14,ES_AUTOHSCROLL
    GROUPBOX        "Data verification",IDC_STATIC,7,174,208,33
    CONTROL         "Enable",CHK_VERIFY,"Button",BS_AUTOCHECKBOX | 
                    WS_TABSTOP,15,188,64,10
    PUSHBUTTON      "Button1",BTN_FORMAT,227,174,26,26,BS_BITMAP
    DEFPUSHBUTTON   "OK",IDOK,7,213,50,14
    PUSHBUTTON      "Cancel",IDCANCEL,61,213,50,14
    LTEXT           "Maximum length",IDC_STATIC,15,86,59,8
END
*/

CBaxterConfigDlg::CBaxterConfigDlg(CWnd* pParent /*=NULL*/)
: FoxjetCommon::CEliteDlg(IDD_BAXTERCONFIG, pParent)
{
	//{{AFX_DATA_INIT(CBaxterConfigDlg)
	//}}AFX_DATA_INIT

	m_bVerify			= FALSE;
	m_strOuputFormat	= _T ("<DC1>ABC<PRIMARY>DEF<DC2><SECONDARY>XYZ<SUBLOT><CR>");
	m_nTaskLen			= 10;
	m_nOvertureLen1		= 6;
	m_nOvertureLen2		= 6;
	//m_nOvertureLen3	= 7;
	m_nPrimaryMax		= 16;
	m_nSecondaryMax		= 17;
	//m_nOvertureMax	= 27;
	m_nUserLenOpt		= 2;

	VERIFY (m_bmpFormat.LoadBitmap (IDB_PLUS));
}


void CBaxterConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBaxterConfigDlg)
	DDX_Check(pDX, CHK_VERIFY, m_bVerify);
	DDX_Text(pDX, TXT_OUTPUT_FORMAT, m_strOuputFormat);
	DDX_Text(pDX, TXT_TASK_LEN, m_nTaskLen);
	//DDX_Text(pDX, TXT_OVERTURE_MAX, m_nOvertureMax);
	DDX_Text(pDX, TXT_OVERTURE_LEN1, m_nOvertureLen1);
	DDX_Text(pDX, TXT_OVERTURE_LEN2, m_nOvertureLen2);
	//DDX_Text(pDX, TXT_OVERTURE_LEN3, m_nOvertureLen3);
	DDX_Text(pDX, TXT_PRIMARY_MAX, m_nPrimaryMax);
	DDX_Text(pDX, TXT_SECONDARY_MAX, m_nSecondaryMax);
	DDX_Text(pDX, TXT_USER_LEN_OPTIONAL, m_nUserLenOpt);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBaxterConfigDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CBaxterConfigDlg)
	ON_BN_CLICKED(BTN_FORMAT, OnFormat)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBaxterConfigDlg message handlers

BOOL CBaxterConfigDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	if (CButton * p = (CButton *)GetDlgItem (BTN_FORMAT))
		p->SetBitmap (m_bmpFormat);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CBaxterConfigDlg::OnFormat() 
{
	CBaxterFormatDlg dlg (this);
	
	if (dlg.DoModal () == IDOK) {
		if (CEdit * p = (CEdit *)GetDlgItem (TXT_OUTPUT_FORMAT)) {
			int nStart = 0, nEnd = 0;
			CString str;

			p->GetWindowText (str);
			p->GetSel (nStart, nEnd);
			str.Insert (nEnd, dlg.m_str);
			p->SetWindowText (str);
			p->SetSel (nEnd + dlg.m_str.GetLength (), nEnd + dlg.m_str.GetLength ());
		}
	}
}

void CBaxterConfigDlg::Load()
{
	SETTINGSSTRUCT s;

	if (GetSettingsRecord (theApp.m_Database, 0, _T ("BaxterConfig"), s)) {
		CStringArray v;
		int nIndex = 0;

		FromString (s.m_strData, v);

		m_bVerify			= _ttoi (GetParam (v, nIndex++)) ? TRUE : FALSE;
		m_strOuputFormat	= UnformatString (GetParam (v, nIndex++));
		m_strOuputFormat.Replace (_T ("<CR_LF>"), _T ("<CR><LF>"));
		m_nTaskLen			= _ttoi (GetParam (v, nIndex++));
		m_nOvertureLen1		= _ttoi (GetParam (v, nIndex++));
		m_nOvertureLen2		= _ttoi (GetParam (v, nIndex++));
		int nOvertureLen3	= _ttoi (GetParam (v, nIndex++));
		m_nPrimaryMax		= _ttoi (GetParam (v, nIndex++));
		m_nSecondaryMax		= _ttoi (GetParam (v, nIndex++));
		int nOvertureMax	= _ttoi (GetParam (v, nIndex++));
		m_nUserLenOpt		= _ttoi (GetParam (v, nIndex++));
	}
}

void CBaxterConfigDlg::Save()
{
	CStringArray v;
	SETTINGSSTRUCT s;
	CString str = m_strOuputFormat;

	str.Replace (_T ("\r\n"), _T (""));
	str.Replace (_T ("<CR><LF>"), _T ("<CR_LF>"));
	str = FormatString (str);

	v.Add (ToString (m_bVerify));
	v.Add (str);
	v.Add (ToString (m_nTaskLen));
	v.Add (ToString (m_nOvertureLen1));
	v.Add (ToString (m_nOvertureLen2));
	v.Add (ToString (8 /*m_nOvertureLen3*/));
	v.Add (ToString (m_nPrimaryMax));
	v.Add (ToString (m_nSecondaryMax));
	v.Add (ToString (0 /*m_nOvertureMax*/));
	v.Add (ToString (m_nUserLenOpt));

	s.m_lLineID = 0;
	s.m_strKey	= _T ("BaxterConfig");
	s.m_strData	= ToString (v);

	if (!UpdateSettingsRecord (theApp.m_Database, s))
		VERIFY (AddSettingsRecord (theApp.m_Database, s));
}

int CBaxterConfigDlg::GetOverture3Len (CString str) 
{ 
	str.TrimLeft (); 
	str.TrimRight (); 
	
	return GetOverture3Len (str.GetLength ()); 
}

int CBaxterConfigDlg::GetOverture3Len (int nOvertureLen)
{
	return max (0, nOvertureLen - (m_nTaskLen + m_nOvertureLen1 + m_nOvertureLen2));
}

