#if !defined(AFX_EVENTSDLG_H__76F6113D_4759_4881_8FC6_6B93FF8F9E77__INCLUDED_)
#define AFX_EVENTSDLG_H__76F6113D_4759_4881_8FC6_6B93FF8F9E77__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EventsDlg.h : header file
//

#include "ListCtrlImp.h"

/////////////////////////////////////////////////////////////////////////////
// CEventsDlg dialog

class CEventsDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CEventsDlg)

	class CEventItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CEventItem ();
		virtual ~CEventItem () { }

		virtual CString GetDispText (int nColumn) const;

		CString m_strAddr;
		CString m_strPcState;
		CString m_strEopState;
		DWORD m_nThreadID;
		HANDLE m_hPC;
		HANDLE m_hEOP;
	};

// Construction
public:
	CEventsDlg();
	~CEventsDlg();

// Dialog Data
	//{{AFX_DATA(CEventsDlg)
	enum { IDD = IDD_EVENTS };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CEventsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

	// Generated message map functions
	//{{AFX_MSG(CEventsDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnRefresh();
	afx_msg void OnSave();
	afx_msg void OnPulse();
	afx_msg void OnReset();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EVENTSDLG_H__76F6113D_4759_4881_8FC6_6B93FF8F9E77__INCLUDED_)
