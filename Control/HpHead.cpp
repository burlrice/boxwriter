// HpHead.cpp : implementation file
//

#include "stdafx.h"
#include "control.h"
#include "HpHead.h"
#include "MainFrm.h"
#include "ControlView.h"
#include "ControlDoc.h"
#include "ProdLine.h"
#include "Database.h"
#include "Comm32.h"
#include "Parse.h"

using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace ControlView;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const LPCTSTR lpszStart	= _T ("~[0>");
static const LPCTSTR lpszEnd	= _T ("~[00<");

/////////////////////////////////////////////////////////////////////////////
// CHpHead

CString CHpHead::Format (const CString & strCmd)
{
	CString str (strCmd);

	str.Replace (::lpszStart, _T ("")); 
	str.Replace (::lpszEnd, _T (""));

	return ::lpszStart + str + ::lpszEnd;
}

IMPLEMENT_DYNCREATE(CHpHead, CHead)

CHpHead::CHpHead()
:	CHead (),
	m_nAmsInterval (12),
	m_lastAMSCycle (CTime::GetCurrentTime ()),
	m_hComm (NULL)
{
}

CHpHead::~CHpHead()
{
}

BOOL CHpHead::InitInstance()
{
	CMainFrame * pFrame = (CMainFrame *) theApp.m_pMainWnd;
	CView * pView = pFrame->m_pControlView;
	m_hControlWnd =  pView->GetSafeHwnd();

	if (CWnd * p = CWnd::FromHandlePermanent (m_hControlWnd)) 
		m_pMainWnd = p;

	CString str;
	
	str.Format (_T ("Exit %lu"), m_hThread);
	m_hExit = CreateEvent (NULL, true, false, str);

	ASSERT (m_pdb);
	ASSERT (m_pdb->IsOpen ());

	return CHead::InitInstance ();
}

int CHpHead::ExitInstance()
{
	CString str;

	str.Format (_T ("CLocalHead %lu ExitInstance"), m_nThreadID);
	TRACEF(str);
	
	return CHead::ExitInstance();
}

BEGIN_MESSAGE_MAP(CHpHead, CHead)
	//{{AFX_MSG_MAP(CHpHead)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_THREAD_MESSAGE (TM_SET_IO_ADDRESS,			OnSetIoAddress)
/*
	ON_THREAD_MESSAGE (TM_SHOW_DEBUG_DIALOG,		OnDoNothing)
	ON_THREAD_MESSAGE (TM_ENABLE_PHOTO,				OnDoNothing)
	ON_THREAD_MESSAGE (TM_UPDATE_STROBE,			OnDoNothing)
	ON_THREAD_MESSAGE (TM_DO_DEBUG_PROCESS,			OnDoNothing)
	ON_THREAD_MESSAGE (TM_READ_CONFIG,				OnDoNothing)
	ON_THREAD_MESSAGE (TM_CHANGE_COUNTS,			OnDoNothing)
	ON_THREAD_MESSAGE (TM_EVENT_PHOTO,				OnDoNothing)
	ON_THREAD_MESSAGE (TM_EVENT_EOP,				OnDoNothing)
	ON_THREAD_MESSAGE (TM_TOGGLE_IMAGE_DEBUG,		OnDoNothing)
	ON_THREAD_MESSAGE (TM_BUILD_IMAGE,				OnDoNothing)
	ON_THREAD_MESSAGE (TM_DOWNLOAD_IMAGE_SEGMENT,	OnDoNothing)
	ON_THREAD_MESSAGE (TM_INCREMENT,				OnDoNothing)
	
	ON_THREAD_MESSAGE (TM_START_TASK,				OnStartTask)
	ON_THREAD_MESSAGE (TM_STOP_TASK,				OnStopTask)
	ON_THREAD_MESSAGE (TM_IDLE_TASK,				OnIdleTask)
	ON_THREAD_MESSAGE (TM_RESUME_TASK,				OnResumeTask)
	ON_THREAD_MESSAGE (TM_HEAD_CONFIG_CHANGED,		OnHeadConfigChanged)
	ON_THREAD_MESSAGE (TM_SET_IGNORE_PHOTOCELL,		OnSetIgnorePhotocell)
	ON_THREAD_MESSAGE (TM_BUILD_PREVIEW_IMAGE,		OnBuildPreviewImage)
	ON_THREAD_MESSAGE (TM_PROCESS_SERIAL_DATA,		OnProcessSerialData) 
*/
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHpHead message handlers

void CHpHead::OnDoNothing (UINT wParam, LONG lParam)
{
}

void CHpHead::OnSetIoAddress (UINT wParam, LONG lParam)
{
	TM_SET_IO_ADDRESS_STRUCT * pParam = (TM_SET_IO_ADDRESS_STRUCT *)wParam;
	CString str;

	ASSERT (pParam);

	ASSERT (m_pdb);
	VERIFY (GetHeadRecord (* m_pdb, pParam->m_lHeadID, m_Config.m_HeadSettings));
	VERIFY (m_Config.LoadSettings (* m_pdb, m_Config.m_HeadSettings.m_strUID));
	::SendMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_ACTIVATION, (LPARAM)this);

	while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, LOCAL_HEAD_INITIALIZING, (LPARAM)this)) 
		::Sleep (0);

	OnHeadConfigChanged (0, 0);
 	m_lastAMSCycle = CTime::GetCurrentTime();

	::SetEvent (pParam->m_hEvent);

	while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM)this)) 
		::Sleep (0);
}

int CHpHead::Run() 
{
	MSG msg;
	const DWORD dwTimeout = 20;
	clock_t clock_tLast = clock (), clock_tNow = clock ();
	const double dSampleRate = 0.5;
	CString strBuffer;
	CTime tmLast = CTime::GetCurrentTime();

	m_lastAMSCycle = tmLast;

	{
		CString strEvent;
		strEvent.Format (_T ("Initialization Complete %lu"), m_hThread);
		HANDLE hInit = ::CreateEvent (NULL, true, false, strEvent);
		HANDLE hEvent [2] = { NULL };

		hEvent [0] = m_hExit;
		hEvent [1] = hInit;
		
		bool bInitialized = false;

		while (!bInitialized) {
			switch (::MsgWaitForMultipleObjects (2, hEvent, false, dwTimeout, QS_ALLINPUT)) {
				case WAIT_OBJECT_0:
					return ExitInstance();
				case WAIT_OBJECT_0 + 1:
					bInitialized = true;
					break;
			}

			while (::PeekMessage (&msg, NULL, NULL, NULL, PM_NOREMOVE)) 
				if (!PumpMessage ())
					return ExitInstance();
		}
		
		::CloseHandle (hInit);
		::Sleep (dwTimeout);
	}

	CMainFrame * pFrame = (CMainFrame *)theApp.m_pMainWnd;
	CControlView * pView = pFrame->m_pControlView;
	CControlDoc * pDoc = pView->GetDocument ();
	CProdLine * pLine = pDoc->GetProductionLine (GetProductionLine());

	ASSERT (pLine);

//	SetThreadPriority (THREAD_PRIORITY_TIME_CRITICAL);

	while (1) {
		HANDLE hEvent [] = { m_hExit, /* m_hSerialEvent, m_hPhotoEvent, m_hEOPEvent, */ };

		DWORD dwWait = ::MsgWaitForMultipleObjects (ARRAYSIZE (hEvent), hEvent, false, dwTimeout, QS_ALLINPUT);

		switch (dwWait) {
			case WAIT_OBJECT_0:
				return ExitInstance();
			case WAIT_OBJECT_0 + 1:	
				//ResetEvent (m_hSerialEvent);
				//OnEventSerial (0, 0);
				break;
			case WAIT_OBJECT_0 + 2:	
				//ResetEvent (m_hPhotoEvent);
				//OnEventPhoto (0, 0);
				break;
			case WAIT_OBJECT_0 + 3:	
				//ResetEvent (m_hEOPEvent);
				//OnEventEOP (0, 0);
				break;
		}

		if (m_nAmsInterval > 1) {
			CTimeSpan tmElapsed = CTime::GetCurrentTime() - m_lastAMSCycle;

			if (tmElapsed.GetTotalMinutes () >= m_nAmsInterval) {
				// Test the PhotoEvent to insure it hasn't fire again.
				//if (WaitForSingleObject ( m_hPhotoEvent, 0) == WAIT_TIMEOUT ) {	
				//	theApp.GetDriver ()->EnablePrint (m_hDeviceID, false);
					
					FireAMS ();
					
				//	if (m_bPhotoEnabled)
				//		theApp.GetDriver ()->EnablePrint (m_hDeviceID, true);

					m_lastAMSCycle = CTime::GetCurrentTime();
				//}
			}
		}

		while (::PeekMessage (&msg, NULL, NULL, NULL, PM_NOREMOVE))
			if (!PumpMessage ())
				return ExitInstance ();

		clock_tNow = clock ();
		double dDiff = (double)(clock_tNow - clock_tLast) / CLOCKS_PER_SEC;

		if (dDiff >= dSampleRate) {
			CString str;

			if (DWORD dwLen = ReadCommport (str)) {
				const TCHAR cDelim = '\r';
				CStringArray v;

				strBuffer += str;
				Tokenize (strBuffer, v, cDelim);

				int nIndex = strBuffer.ReverseFind (cDelim);

				if (nIndex != -1) {
					if (nIndex < strBuffer.GetLength () - 1) {
						// if there was a partial packet, put it back
						strBuffer = strBuffer.Mid (nIndex + 1);
						v.RemoveAt (v.GetSize () - 1);
					}

					for (int i = 0; i < v.GetSize (); i++) 
						TRACEF (v [i]); // TODO: process cmd here

					strBuffer.Empty ();
				}
			}

			m_Status.m_nInkLevel		= OK;
			m_Status.m_bAtTemp			= true;
			m_Status.m_bVoltageOk		= true;
			m_Status.m_bBrokenLine		= false;
			m_Status.m_bIDSConnected	= true;

 			clock_tLast = clock ();
			tmLast = CTime::GetCurrentTime();

			while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this))
				::Sleep (0);
		}
	}

	return ExitInstance ();
}

void CHpHead::OnHeadConfigChanged (UINT wParam, LONG lParam)
{
	LOCK (m_cs);

	{
		if (m_hComm) {
			Close_Comport (m_hComm);
			m_hComm = NULL;
		}


		ASSERT (m_pdb);
		VERIFY (m_Config.LoadSettings (* m_pdb, m_Config.m_HeadSettings.m_strUID));
		CHead::OnHeadConfigChanged (wParam, lParam);
		m_nAmsInterval = (int)(m_Config.m_Line.m_dAMS_Interval * 60.0);

		DWORD dwPort = -1;
		StdCommDlg::CCommItem tmp;

		_stscanf ((LPCTSTR)GetUID (), _T ("COM%d"), &dwPort);
		tmp.FromString (m_Config.m_HeadSettings.m_strSerialParams);

		BYTE nParity = 0;

		switch (tmp.m_parity) {
		default:
		case NO_PARITY:		nParity = NOPARITY;		break;
		case EVEN_PARITY:	nParity = EVENPARITY;	break;
		case ODD_PARITY:	nParity = ODDPARITY;	break;
		}

		m_hComm = Open_Comport (dwPort, tmp.m_dwBaudRate, tmp.m_nByteSize, nParity, tmp.m_nStopBits); 	
		
		if (!IsCommportOpen ()) {
			ASSERT ("Open_Comport failed" == 0);
		}
		else {
			const HEADSTRUCT & h = m_Config.m_HeadSettings;
			
			WriteCommport (Format (h.m_nDirection == LTOR	? _T ("Caml") : _T ("Camr")));
			WriteCommport (Format (!h.m_bInverted			? _T ("Cawi") : _T ("Cawn")));
			WriteCommport (Format (_T ("Tdt") + FoxjetDatabase::ToString (CalcTime (h, h.m_lPhotocellDelay))));

			WriteCommport (Format (_T ("Prr"))); // disable one to one printing ("Pre" - enable)
			
			if (h.m_nEncoder == DUAL_TRIGGER) {
				WriteCommport (Format (_T ("Cttc2")));
				WriteCommport (Format (_T ("Tda5")));
				WriteCommport (Format (_T ("Tdv215")));
			}
			else if (h.m_nEncoder == INTERNAL_ENC) {
				LONG l = (LONG)(5000000.0 / (300.0 * (double)h.m_nIntTachSpeed));

				WriteCommport (Format (_T ("Cttnt")));
				WriteCommport (Format (_T ("Ctci") + FoxjetDatabase::ToString (l)));
			}
		}
	}
}

LONG CHpHead::CalcTime (const FoxjetDatabase::HEADSTRUCT & h, ULONG lThousandths)
{
	LONG lResult = (LONG)((1000.0 / ((double)h.m_nIntTachSpeed / 5.0) * (double)(lThousandths / 1000.0)));

	return lResult;
}

DWORD CHpHead::ReadCommport (CString & str)
{
	DWORD dwBytesRead = 0;
	LOCK (m_cs);
	{
		if (m_hComm) {
			BYTE buffer [COM_BUF_SIZE];

			ZeroMemory (buffer, sizeof (buffer));

			Read_Comport (m_hComm, &dwBytesRead, sizeof (buffer), buffer);

			if (dwBytesRead) {
				for (int i = 0; i < (int)dwBytesRead; i++) 
					str += (TCHAR)buffer [i];

				((CMainFrame *)theApp.m_pMainWnd)->m_pControlView->UpdateSerialData (_T ("> ") + str);
			}
		}
	}

	return dwBytesRead;
}

bool CHpHead::WriteCommport (const CString & str)
{
	bool bResult = false;
	LOCK (m_cs);
	{
		if (m_hComm) {
			int nWrite = Write_Comport (m_hComm, str.GetLength (), (LPVOID)w2a (str));
			TRACEF ("Write_Comport: " + str);
			bResult = nWrite != 0;

			if (bResult)
				((CMainFrame *)theApp.m_pMainWnd)->m_pControlView->UpdateSerialData (str);
		}
	}

	return bResult;
}

void CHpHead::OnStartTask (UINT wParam, LONG lParam)
{
	m_eTaskState = RUNNING;
	m_sTaskName	= m_Task.m_strName;

	CHead::OnStartTask (wParam, lParam);
	OnBuildPreviewImage (0, 0);

	while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this)) 
		::Sleep(0);

	{
		CString str = "Pde";

		if (m_pElementList) {
			for (int i = 0; i < m_pElementList->GetSize (); i++) {
				FoxjetCommon::CBaseElement & e = m_pElementList->GetObject (i);

				e.Build (IMAGING);
				str += e.GetImageData ();
			}
		}

		WriteCommport (Format (str));
	}
}

void CHpHead::OnStopTask (UINT wParam, LONG lParam)
{
//	WriteCommport ("stop  \r");
	m_eTaskState = STOPPED;
	m_sTaskName	= _T ("");

	CHead::OnStopTask (wParam, lParam);

	while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this)) 
		::Sleep(0);
}

void CHpHead::OnIdleTask (UINT wParam, LONG lParam)
{
//	WriteCommport ("idle  \r");
	m_eTaskState = IDLE;

	CHead::OnIdleTask (wParam, lParam);

	while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this)) 
		::Sleep(0);
}

void CHpHead::OnResumeTask (UINT wParam, LONG lParam)
{
//	WriteCommport ("resume\r");
	m_eTaskState = RUNNING;
	
	CHead::OnResumeTask (wParam, lParam);

	while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this)) 
		::Sleep(0);
}

void CHpHead::FireAMS ()
{
	WriteCommport (Format (_T ("Pcqsc10,10,40,60")));
}

eHeadStatus CHpHead::GetErrorStatus()
{
	if (!IsCommportOpen ()) 
		return HP_COMMPORTFAILURE;

	return CHead::GetErrorStatus ();
}
