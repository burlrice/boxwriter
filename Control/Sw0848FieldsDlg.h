#if !defined(AFX_SW0848FIELDSDLG_H__FA1DC9E4_0227_4571_81C1_4B08362C75D7__INCLUDED_)
#define AFX_SW0848FIELDSDLG_H__FA1DC9E4_0227_4571_81C1_4B08362C75D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Sw0848FieldsDlg.h : header file
//


#include "RoundButtons.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CSw0848FieldsDlg dialog

class CSw0848FieldsDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CSw0848FieldsDlg(CWnd* pParent = NULL);   // standard constructor

public:
	virtual void OnOK();

// Dialog Data
	//{{AFX_DATA(CSw0848FieldsDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	CStringArray m_vInput;
	CStringArray m_vOutput;

	CString m_strInput;
	CString m_strOutput;

	static const CString m_strTimestamp;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSw0848FieldsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	FoxjetUtils::CRoundButtons m_buttons;

	// Generated message map functions
	//{{AFX_MSG(CSw0848FieldsDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SW0848FIELDSDLG_H__FA1DC9E4_0227_4571_81C1_4B08362C75D7__INCLUDED_)
