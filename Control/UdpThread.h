#if !defined(AFX_UDPTHREAD_H__FF2CB065_78AD_4210_8CAC_A18CF873B5B3__INCLUDED_)
#define AFX_UDPTHREAD_H__FF2CB065_78AD_4210_8CAC_A18CF873B5B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UdpThread.h : header file
//

#include "host.h"
//#include <winsock.h>

class CControlView;

/////////////////////////////////////////////////////////////////////////////
// CUdpThread thread

class CUdpThread : public CWinThread
{
	DECLARE_DYNCREATE(CUdpThread)
protected:
	CUdpThread();           // protected constructor used by dynamic creation

// Attributes
public:
	virtual ~CUdpThread();

// Operations
public:
	static CString GetError (UINT nError);

	virtual UINT GetPort () const { return BW_HOST_BROADCAST_PORT; }
	virtual UINT GetSockType () const { return SOCK_DGRAM; }
	virtual CString OnCommand (const CString & str, const SOCKADDR_IN * paddr = NULL);	
	virtual bool Open ();
	virtual void Close ();

	static bool Open(SOCKET& sock, UINT nType, int nPort);
	static void Close (SOCKET & sock);

	CString OnDataRecieved (const char * lpsz, int nLen, const SOCKADDR_IN * paddr = NULL);

	static const int m_nBufferSize;

	CControlView * m_pView;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUdpThread)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();
	//}}AFX_VIRTUAL

	bool IsRunning () const { return m_sock != INVALID_SOCKET; }

	static bool CheckForFlush (const CString & str);

// Implementation
protected:
	virtual void ProcessData ();

	void ReportError (UINT Error);
	bool CheckConnection ();
	virtual void SetSocketOptions (SOCKET s) { }

	afx_msg void OnKillThread (WPARAM wParam, LPARAM lParam);
	afx_msg void OnBroadcastInkCode (WPARAM wParam, LPARAM lParam);

	HANDLE m_hExit;
	SOCKET m_sock;
	CTime m_tmLastParamChange;

private:
	typedef enum { ASCII, UTF8, UTF16, UTF16BE } ENCODINGTYPE;

	ENCODINGTYPE m_encoding;
	CString m_strData;

	// Generated message map functions
	//{{AFX_MSG(CUdpThread)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG


	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UDPTHREAD_H__FF2CB065_78AD_4210_8CAC_A18CF873B5B3__INCLUDED_)
