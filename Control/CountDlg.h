#if !defined(AFX_COUNTDLG_H__1F2883D8_7F53_47E5_96E7_582EBEA66A08__INCLUDED_)
#define AFX_COUNTDLG_H__1F2883D8_7F53_47E5_96E7_582EBEA66A08__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CountDlg.h : header file
//

#include "CountElement.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CCountDlg dialog

class CCountDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CCountDlg(CWnd* pParent = NULL);   // standard constructor


// Dialog Data
	//{{AFX_DATA(CCountDlg)
	enum { IDD = IDD_COUNT };
	__int64	m_lCount;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCountDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCountDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COUNTDLG_H__1F2883D8_7F53_47E5_96E7_582EBEA66A08__INCLUDED_)
