#if !defined(AFX_REMOTEHEAD_H__D127FEA8_C5C7_4E80_A5F3_9B68BD932B0E__INCLUDED_)
#define AFX_REMOTEHEAD_H__D127FEA8_C5C7_4E80_A5F3_9B68BD932B0E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RemoteHead.h : header file
//
#include <afxinet.h>

#include "fj_socket.h"
#include "fj_defines.h"
#include "ClientSock.h"		// Added by ClassView
#include "Head.h"
#include "CmdPacket.h"
#include "ProgressDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CRemoteHead thread

namespace RemoteHead
{
	typedef enum 
	{
		UPDATE_FIRMWARE,
		UPDATE_GA,
	} UPDATETYPE;
}; // namespace RemoteHead

class CRemoteHead : public Head::CHead
{
	DECLARE_DYNCREATE(CRemoteHead)
protected:
	CRemoteHead();           // protected constructor used by dynamic creation
public:

	virtual ~CRemoteHead();

	static const TCHAR m_cBreak;

// Attributes
public:

	CDiagnosticCriticalSection m_SequenceSentinel;
	CStringList m_FontTransferList;

	void SetKeepAlive (bool bOn);
	static bool IsNetFirmware (const CString & strFile);
	static bool IsNetFirmware (PUCHAR pData, ULONG lLen);


// Operations
public:
	void GetLocalFontList();
	void GetLocalFontList (CStringArray & vFonts);

	CString m_sEditSessionOwner;
	void CueTxPacket (CCmdPacket *pCmd);
	CStringList m_RemoteLabels;
	ULONG GetNextSequence(void);
	virtual void SetCounts (__int64 lCount, __int64 lPalletMax = 0, __int64 lUnitsPerPallet = 0);
	virtual bool SetStandbyMode (bool bStandby);
	CString GetSwVer () const { return m_strSwVer; }
	CString GetGaVer () const { return m_strGaVer; }
	double GetDiskUsage () const { return m_dDiskUsage; }
	void GetRemoteBitmaps (CStringArray & vBitmaps);

	static int CompareVersions (const FoxjetCommon::CVersion & verIP, const FoxjetCommon::CVersion & verCurrent);

	void TraceMessageState ();

protected:
	bool m_bDynamicDataDownload; 

	class CSuspendDynamicDataDownload
	{
	public:
		CSuspendDynamicDataDownload (CRemoteHead * p) 
		:	m_p (p),
			m_bState (false)
		{
			if (m_p) {
				m_bState = p->IsDynamicDataDownloadEnabled ();
				//m_p->EnableDynamicDataDownload (false);
			}
		}

		~CSuspendDynamicDataDownload ()
		{
			if (m_p) {
				//m_p->EnableDynamicDataDownload (m_bState);
			}
		}

	protected:
		CRemoteHead * m_p;
		bool m_bState;
	};

protected:
	virtual void Shutdown ();
	void QueryDiskUsage ();

public:

	virtual bool IsConnected () const { return m_socket != INVALID_SOCKET; }
	void EnableDynamicDataDownload (bool bEnable); 
	bool IsDynamicDataDownloadEnabled () { return m_bDynamicDataDownload; } 

	CString m_strDynData [FJMESSAGE_DYNAMIC_SEGMENTS + 1];

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRemoteHead)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();
	//}}AFX_VIRTUAL

public:
	static void CloseSocket (SOCKET & s);
	bool IsDigiBoard () const { return m_bDigiBoard; }

protected:
	/* TODO: rem
	class CPacket : public _FJ_SOCKET_MESSAGE 
	{
	public:
		CPacket () 
		{ 
			Header.Cmd		= 0;
			Header.BatchID	= 0;
			Header.Length	= 0;

			memset (Data, _FJ_SOCKET_BUFFER_SIZE, 0);
		}

		~CPacket () { }
	};
	*/
	class CPacket
	{
	public:

		CPacket (int nBufferSize = _FJ_SOCKET_BUFFER_SIZE) 
		:	m_nLen (nBufferSize),
			Data (NULL)
		{ 
			Header.Cmd		= 0;
			Header.BatchID	= 0;
			Header.Length	= 0;

			Data = new unsigned char [m_nLen];
			memset (Data, 0, m_nLen);
		}

		CPacket (const CPacket & rhs) 
		:	m_nLen (rhs.m_nLen),
			Data (NULL)
		{ 
			Header.Cmd		= rhs.Header.Cmd;
			Header.BatchID	= rhs.Header.BatchID;
			Header.Length	= rhs.Header.Length;

			Data = new unsigned char [m_nLen];
			memcpy (Data, rhs.Data, m_nLen);
		}

		CPacket & operator = (const CPacket & rhs)
		{
			if (&rhs != this) {
				memcpy (&Header, &rhs.Header, sizeof (Header));
				ReAlloc (rhs.m_nLen);
				memcpy (Data, rhs.Data, m_nLen);
			}

			return * this;
		}

		void ReAlloc (int nBufferSize = _FJ_SOCKET_BUFFER_SIZE)
		{
			if (Data)
				delete [] Data;

			Data = new unsigned char [m_nLen = nBufferSize];
			memset (Data, 0, m_nLen);
		}

		~CPacket () 
		{ 
			if (Data) {
				delete [] Data;
				Data = NULL;
				m_nLen = 0;
			}
		}

		_FJ_SOCKET_MESSAGE_HEADER	Header;
		unsigned char *				Data;
		UINT						m_nLen;
	};

	class CNotifySocket : public CAsyncSocket
	{
	public:
		CNotifySocket (UINT nOwnerThreadID, HANDLE hOwnerThread, const CString & strAddr);
		virtual ~CNotifySocket ();

		virtual void OnConnect(int nErrorCode);

	protected:
		UINT m_nOwnerThreadID;
		HANDLE m_hOwnerThread;
		CString m_strAddr;
	};

	CNotifySocket * m_pConnect;
	int m_nTimeoutStep;

	void Connect ();
	virtual void Disconnect (bool bShutdown = false);

	bool Send (ULONG lCmd, ULONG lBatchID, const CString & strData = _T (""), CPacket * pReply = NULL, DWORD dwTimeout = GlobalVars::HeadResponseTimeout);
	bool Send (ULONG lCmd, ULONG lBatchID, unsigned char * pData, ULONG lLen, CPacket * pReply = NULL, DWORD dwTimeout = GlobalVars::HeadResponseTimeout);

	bool ReadSocket (CPacket * pReply = NULL);
	CDiagnosticCriticalSection m_csReadSocket;
	CString GetLocalAddress () const;

	void TransferBitmap ( CString sFile );
	void CancelEditSession ( void );
	bool SaveEditSession ( void );
	bool StartEditSession ( void );
	bool CommandSuccessful ( ULONG lSent, const CPacket * pResult);
	void ProcessRxData (const CPacket * pMsg );
	CStringList m_PrinterFontList;
	void SendConfiguration (void);
	CString GetLocalConfiguration ();
	CString GetConfiguration (CString & strBarcode, CString & strDataMatrix, CString & strStrobe);
	bool CompareConfiguration (const CString & str, CString & strBarcode, CString & strDataMatrix, CString & strStrobe);
	bool SendFont ( CString sFont, ULONG batchID );
	void BuildMsgAndElemString(FoxjetCommon::CElementList *pElementList, CString sMsgName, CString &sMsgIDs, CString &sElementData);

	void SetSwVer (const CString & str);
	void SetGaVer (const CString & str);
	void DoUpdate (RemoteHead::UPDATETYPE type, PUCHAR pData, ULONG lLen);
	void GetVersionInfo ();

	void UpdateLabels (CArray <FoxjetDatabase::MESSAGESTRUCT, FoxjetDatabase::MESSAGESTRUCT &> & v, bool bTaskStart = false);
	void DownloadMessage (const FoxjetDatabase::TASKSTRUCT & task,
		const FoxjetDatabase::MESSAGESTRUCT & msg, CSize & sizeLabel, ULONG lBatchID,
		float fBoxHeight, float fBoxWidth, bool bTaskStart);

	class CStatus
	{
	public:
		CTime m_tmLast;
		bool m_bClosed;
		bool m_bShortTimeout;
		bool m_bGetStatus;
	} m_state;
	
	CPtrList m_TxCue;
	ULONG m_nSequence;
	HANDLE m_hLabelListUpdate;
	SOCKET m_socket;
	ULONG m_lControlNum;
	
	// Generated message map functions
	//{{AFX_MSG(CRemoteHead)
	//}}AFX_MSG
	virtual afx_msg void OnDataRecieved (UINT wParam, LONG lParam);
	virtual afx_msg void OnStatusUpdate (UINT wParam, LONG lParam);
	virtual afx_msg void OnStartTask (UINT wParam, LONG lParam);
	virtual afx_msg void OnStopTask (UINT wParam, LONG lParam);
	virtual afx_msg void OnIdleTask (UINT wParam, LONG lParam);
	virtual afx_msg void OnResumeTask (UINT wParam, LONG lParam);
	virtual afx_msg void OnHeadConfigChanged (UINT wParam, LONG lParam);
	virtual afx_msg void OnInitialStartup(UINT wParam, LONG lParam);
	virtual afx_msg void OnSetIgnorePhotocell (UINT wParam, LONG lParam);
	virtual afx_msg void OnBuildPreviewImage (UINT wParam, LONG lParam);
	virtual afx_msg void OnUpdateCounts (UINT wParam, LONG lParam);	

	afx_msg void OnSocketConnect(UINT wParam, LONG lParam);
	afx_msg void OnSocketRetryConnect(UINT wParam, LONG lParam);
	afx_msg void OnSocketDisconnect(UINT wParam, LONG lParam);
	afx_msg void OnUpdateLabels(UINT wParam, LONG lParam);
	afx_msg void OnUpdateFonts(UINT wParam, LONG lParam);
	afx_msg void OnUpdateFirmware(UINT wParam, LONG lParam);
	afx_msg void OnUpdateGa (UINT uParam, LONG lParam);
	afx_msg void OnGetMessageIDs(UINT wParam, LONG lParam);
	afx_msg void OnDeleteLabels(UINT wParam, LONG lParam);
	afx_msg void OnDeleteMemStore(UINT wParam, LONG lParam);
	afx_msg void OnSyncronizeLabelStore(UINT wParam, LONG lParam);
	afx_msg void OnAmsCycle (UINT wParam, LONG lParam);
	afx_msg void OnChangeCounts(UINT wParam, LONG lParam);
	afx_msg void OnDownloadDynData(UINT wParam, LONG lParam);
	afx_msg void OnSetSerialPending(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REMOTEHEAD_H__D127FEA8_C5C7_4E80_A5F3_9B68BD932B0E__INCLUDED_)
