#if !defined(AFX_HEADINFOCTRL_H__77500F64_ADA4_4889_8ED6_DE3C822B9AC9__INCLUDED_)
#define AFX_HEADINFOCTRL_H__77500F64_ADA4_4889_8ED6_DE3C822B9AC9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HeadInfoCtrl.h : header file
//

#include "ListCtrlImp.h"

/////////////////////////////////////////////////////////////////////////////
// CHeadInfoCtrl window

class CHeadInfoCtrl : public CListCtrl
{
// Construction
public:
	class CHeadItem 
	{
	public:
		CHeadItem (Head::CHead * pHead = NULL);
		virtual ~CHeadItem ();

		void Draw (CDC & dc, const CRect & rc, CHeadInfoCtrl & lv);

		Head::CHead * m_pHead;
	};

	CHeadInfoCtrl();

	void InitHeaders (CArray <ItiLibrary::ListCtrlImp::CColumn, ItiLibrary::ListCtrlImp::CColumn> & vCols, CFont * pFont = NULL);

	void DeleteCtrlData ();
	bool InsertCtrlData (CHeadItem * pData);
	bool InsertCtrlData (CHeadItem * pData, int nIndex);

// Attributes
public:
	int GetColumnCount () const { return m_nColumns; }

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHeadInfoCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CHeadInfoCtrl();

	// Generated message map functions
protected:
	int m_nColumns;

	afx_msg void OnDrawItem( int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct );
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT ); 

	//{{AFX_MSG(CHeadInfoCtrl)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEADINFOCTRL_H__77500F64_ADA4_4889_8ED6_DE3C822B9AC9__INCLUDED_)
