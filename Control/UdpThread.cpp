﻿// UdpThread.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include <AfxSock.h>
#include <process.h>

#include "control.h"
#include "UdpThread.h"
#include "Debug.h"
#include "Utils.h"
#include "TemplExt.h"
#include "Host.h"
#include "ControlView.h"
#include "Parse.h"
#include "Registry.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;

const int CUdpThread::m_nBufferSize = 1024;

/////////////////////////////////////////////////////////////////////////////
// CUdpThread

IMPLEMENT_DYNCREATE(CUdpThread, CWinThread)

CUdpThread::CUdpThread()
:	m_sock (INVALID_SOCKET),
	m_hExit (NULL),
	m_encoding (ASCII),
	m_pView (NULL)
{
	m_bAutoDelete = TRUE;
	m_tmLastParamChange = CTime::GetCurrentTime () - CTimeSpan (1);
}

CUdpThread::~CUdpThread()
{
}

BOOL CUdpThread::InitInstance()
{
	CString str;

	str.Format (_T ("Exit %lu"), m_hThread);
	m_hExit = CreateEvent (NULL, true, false, str);

	return Open ();
}

bool CUdpThread::Open(SOCKET& sock, UINT nType, int nPort)
{
	SOCKADDR_IN addr;

	if ((sock = ::socket(AF_INET, nType, 0)) == INVALID_SOCKET) {
		TRACEF(GetError(::WSAGetLastError()));
		return false;
	}

	ZeroMemory(&addr, sizeof(addr));

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(nPort);

	if (::bind(sock, (SOCKADDR*)&addr, sizeof(addr)) == SOCKET_ERROR) {
		TRACEF(GetError(::WSAGetLastError()));
		return false;
	}

	char cFlag;

	cFlag = 1;
	if (::setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &cFlag, sizeof(cFlag)) == SOCKET_ERROR) {
		TRACEF(GetError(::WSAGetLastError()));
		return false;
	}

	cFlag = 1;
	if (::setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &cFlag, sizeof(cFlag)) == SOCKET_ERROR)
		TRACEF(GetError(::WSAGetLastError()));

	return true;
}

bool CUdpThread::Open ()
{
	if (Open(m_sock, GetSockType(), GetPort())) {
		SetSocketOptions(m_sock);

		return true;
	}

	return false;
}

void CUdpThread::Close (SOCKET & sock)
{
	if (sock != INVALID_SOCKET) {
		if (::shutdown (sock, 0) == SOCKET_ERROR) 
			TRACEF (GetError (::WSAGetLastError ()));

		if (::closesocket (sock) == SOCKET_ERROR) 
			TRACEF (GetError (::WSAGetLastError ()));

		sock = INVALID_SOCKET;
	}
}

void CUdpThread::Close ()
{
	TRACEF ("CUdpThread::Close");
	Close (m_sock);
}

int CUdpThread::ExitInstance()
{
	Close ();

	if (m_hExit)
		CloseHandle (m_hExit);
	
	return CWinThread::ExitInstance ();
}

BEGIN_MESSAGE_MAP(CUdpThread, CWinThread)
	//{{AFX_MSG_MAP(CUdpThread)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_THREAD_MESSAGE (TM_KILLTHREAD, OnKillThread)
	ON_THREAD_MESSAGE (TM_BROADCAST_INK_CODE, OnBroadcastInkCode)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CUdpThread message handlers

int CUdpThread::Run() 
{
	MSG Msg;
	HANDLE hEvents [] = { m_hExit };

	while (1) {
		DWORD dwEventID = MsgWaitForMultipleObjects (ARRAYSIZE (hEvents), hEvents, false, 250, QS_ALLINPUT);

		#if 0
		if (CServerThread * pThis = DYNAMIC_DOWNCAST (CServerThread, this)) {
			static int nCalls = 0;

			timeval timeout = { 0, 1 };
			fd_set r, w, e;

			r.fd_count		= w.fd_count		= e.fd_count		= 1;
			r.fd_array [0]	= w.fd_array [0]	= e.fd_array [0]	= pThis->m_sockClient;

			int nRead	= ::select (0, &r, NULL, NULL, &timeout);
			int nWrite	= ::select (0, NULL, &w, NULL, &timeout);
			int nExcept = ::select (0, NULL, NULL, &e, &timeout);

			if (!(nCalls++ % 4)) 
			{
				CString str;

				str.Format (_T ("m_hThread: 0x%d, m_nThreadID: %d, m_sockClient: %d, m_sock: %d, nRead: %d, nWrite: %d, nExcept: %d\n"), m_hThread, m_nThreadID, pThis->m_sockClient, m_sock, nRead, nWrite, nExcept);
				TRACER (str);
				int nBreak = 0;
			}
		}
		#endif

		switch ( dwEventID )
		{
			case WAIT_OBJECT_0:
				TRACEF (GetRuntimeClass ()->m_lpszClassName + CString (_T (": m_hExit")));
				return ExitInstance();
			default:
				while (PeekMessage (&Msg, NULL, NULL, NULL, PM_NOREMOVE))
					if ( !PumpMessage() )
						return ExitInstance();
		}

		ProcessData ();
	}

	return ExitInstance ();
}

CString CUdpThread::OnCommand (const CString & str, const SOCKADDR_IN * paddr)
{
	CStringArray v;
	CString strResult;

	if (!Tokenize (str, v).CompareNoCase (BW_HOST_INK_CODE)) {
		std::vector <std::wstring> vMAC = FoxjetDatabase::GetMacAddresses ();
		bool bSelf = false;

		for (int i = 0; !bSelf && i < v.GetSize (); i++) {
			const CString strParam = GetParam (v, i);

			for (int j = 0; !bSelf && j < vMAC.size (); j++) {
				const CString strMAC = vMAC [j].c_str ();

				if (!strParam.CompareNoCase (strMAC))
					bSelf = true;
			}
		}

		TRACEF (CString (_T ("CUdpThread::OnCommand: ")) + (bSelf ? _T ("[self] ") : _T ("")) + str);

		if (!bSelf) {
			CString strAddress;

			if (paddr) 
				strAddress.Format (_T ("%d.%d.%d.%d"), 
					paddr->sin_addr.S_un.S_un_b.s_b1,
					paddr->sin_addr.S_un.S_un_b.s_b2,
					paddr->sin_addr.S_un.S_un_b.s_b3,
					paddr->sin_addr.S_un.S_un_b.s_b4);

			CString strTmp = strAddress;

			m_pView->SendMessage (WM_HOST_RECIEVE_INK_CODE, (WPARAM)&v, (LPARAM)&strTmp);

			if (paddr) {
				std::vector <std::wstring> v;
				CSocket s;

				m_pView->SendMessage (WM_GET_INK_CODES, (WPARAM)&v, 0);

				if (!s.Create (0, SOCK_DGRAM)) {
					TRACEF ("Create failed: " + strAddress + "\n" + FormatMessage (::GetLastError ()));
				}

				if (!s.Connect (strAddress, BW_HOST_UDP_PORT)) {
					TRACEF ("Connect failed: " + strAddress + "\n" + FormatMessage (::GetLastError ()));
				}

				for (int i = 0; i < v.size (); i++) {
					CString str = v [i].c_str ();

					TRACEF (str);
					int nSend = s.Send (w2a (str), str.GetLength ());
					ASSERT (nSend == str.GetLength ());
				}
			}
		}
	}
	else if (!Tokenize (str, v).CompareNoCase (BW_HOST_PACKET_LOCATE)) {
		IN_ADDR n;
		int nPort = 0;
		CString strAddr, strPort;

		if (paddr) {
			nPort	= ::ntohs (paddr->sin_port);
			strAddr = ::inet_ntoa (paddr->sin_addr);
		}

		strPort.Format (_T ("%d"), nPort);

		v.RemoveAll ();
		v.Add (BW_HOST_PACKET_LOCATE);
		v.Add (strAddr);
		v.Add (strPort);

		DWORD dwAddress = 0;
		n.S_un.S_addr = htonl (dwAddress);

		if (HOSTENT * h = gethostbyaddr ((const char *)&dwAddress, 4, AF_INET)) 
			v.Add (h->h_name);

		strResult = ToString (v);
		TRACEF (strResult);
	}
	else {
		v.RemoveAll ();

		CTimeSpan tm = CTime::GetCurrentTime () - m_tmLastParamChange;

		if (tm.GetTotalSeconds () > 4) {
			if (!Tokenize (str, v).CompareNoCase (BW_HOST_SYSTEMPARAMCHANGE)) {
				if (v.GetSize () >= 3) {
					CString strInstance = FoxjetDatabase::ToString ((ULONG)::AfxGetMainWnd ()->m_hWnd);
					CString strIpAddr = FoxjetDatabase::GetIpAddress ();

					TRACEF (v [1] + _T (" [") + strInstance + _T ("]"));
					TRACEF (v [2] + _T (" [") + strIpAddr + _T ("]"));

					if (strInstance.CompareNoCase (v [1]) != 0 ||
						strIpAddr.CompareNoCase (v [2]) != 0)
					{
						TRACEF (a2w (GetRuntimeClass ()->m_lpszClassName));
						::PostMessage (HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, 0);
						m_tmLastParamChange = CTime::GetCurrentTime ();
					}
				}
			}
		}
	}

	return strResult;
}

bool CUdpThread::CheckConnection ()
{
	DWORD dw = ::GetLastError ();

	switch (dw) {
	case WSAECONNRESET:
	case WSAESHUTDOWN :
		Close ();
		return false;
	}

	return true;
}

bool CUdpThread::CheckForFlush (const CString & str)
{
	const CString strBreak [] =
	{
		_T ("<CR><CR><CR><CR><CR>"),
		_T ("<LF><LF><LF><LF><LF>"),
		_T ("<CR><LF><CR><LF><CR><LF><CR><LF><CR><LF>"),
		_T ("<LF><CR><LF><CR><LF><CR><LF><CR><LF><CR>"),
	};
	const CString strFormatted = Format (str, str.GetLength ());

	for (int i = 0; i < ARRAYSIZE (strBreak); i++) {
		if (strFormatted.GetLength () >= strBreak [i].GetLength ()) {
			CString str = strFormatted.Right (strBreak [i].GetLength ());

			if (!str.CompareNoCase (strBreak [i])) 
				return true;
		}
	}

	return false;
}



CString ByteSwap (const CString & str)
{
	TCHAR * p = new TCHAR [str.GetLength ()];

	for (int i = 0; i < str.GetLength (); i++) {
		TCHAR c = str [i];

		p [i] = MAKEWORD (HIBYTE (c), LOBYTE (c));		
	}

	CString strResult = p;
	delete [] p;
	return strResult;
}

CString CUdpThread::OnDataRecieved (const char * lpsz, int nLen, const SOCKADDR_IN * paddr)
{
	const BYTE nUTF8 []		= { 0xEF, 0xBB, 0xBF };
	const BYTE nUTF16 []	= { 0xFF, 0xFE };
	const BYTE nUTF16BE []	= { 0xFE, 0xFF };
	CString strResult;
	bool bHeader = false;

	TRACER (_T ("[") + ToString (nLen) + _T ("] ") + Format ((LPBYTE)lpsz, strlen (lpsz))); TRACER (_T ("\n"));

	if (m_strData.IsEmpty () && nLen > 4) {
		if (!memcmp (lpsz, nUTF8, ARRAYSIZE (nUTF8))) {
			m_encoding = UTF8;
			m_strData += UTF8ToUnicode (std::string (w2a (&lpsz [ARRAYSIZE (nUTF8) - 1]))).c_str ();
			bHeader = true;
		}
		else if (!memcmp (lpsz, nUTF16, ARRAYSIZE (nUTF16))) {
			m_encoding = UTF16;
			m_strData += std::wstring (&((wchar_t *)lpsz) [ARRAYSIZE (nUTF16) - 1]).c_str ();
			bHeader = true;
		}
		else if (!memcmp (lpsz, nUTF16BE, ARRAYSIZE (nUTF16BE))) {
			CString str = std::wstring (&((wchar_t *)lpsz) [ARRAYSIZE (nUTF16BE) - 1]).c_str ();
			m_encoding = UTF16BE;
			m_strData += ByteSwap (str);
			bHeader = true;
		}
	}

	if (!bHeader) {
		switch (m_encoding) {
		case ASCII:
			m_strData += a2w (lpsz);
			break;
		case UTF8:
			m_strData += UTF8ToUnicode (std::string (w2a (lpsz))).c_str ();
			break;
		case UTF16:
			m_strData += std::wstring ((wchar_t *)lpsz).c_str ();
			break;
		case UTF16BE:
			CString str = std::wstring ((wchar_t *)lpsz).c_str ();
			m_strData += ByteSwap (str);
			break;
		}
	}

	{
		CString strLower = m_strData;

		strLower.MakeLower ();
		
		if (strLower.Find (_T ("<?xml")) != -1) {
			bool bEnd = (strLower.Find (_T ("</xmlscript>")) != -1)? true : false;

			{
				const CString strBegin = CServerThread::GetXmlTag (strLower, 1);
				const CString strEnd = _T ("/") + strBegin;

				int nBegin	= CServerThread::GetXmlTagIndex (m_strData, strBegin);
				int nEnd	= CServerThread::GetXmlTagIndex (m_strData, strEnd);
			
				bEnd |= (nBegin != -1 && nEnd != -1) ? true : false;
			}

			if (bEnd) {
				CString strResult = OnCommand (m_strData, paddr);
				m_strData.Empty ();
				m_encoding = ASCII;
				return strResult;
			}
		}
	}

	if (CheckForFlush (m_strData)) {
		m_strData.Empty ();
		m_encoding = ASCII;
	
		return _T ("{flush}");
	}

	int nLeftDelim	= CountChars (m_strData, '{').GetSize ();
	int nRightDelim = CountChars (m_strData, '}').GetSize ();

	if (nLeftDelim && (nLeftDelim == nRightDelim)) {
		CStringArray vPackets;
		CStringArray  vExempt;
		bool bExempt = false;
		static const CString strKey = FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), _T ("/enum="));
		CString strAddrSelf = GetIpAddress ();
		
		vExempt.Add (BW_HOST_SYSTEMPARAMCHANGE);
		vExempt.Add (BW_HOST_INK_CODE);
		//vExempt.Add (BW_HOST_PACKET_LOCATE);

		TRACEF (m_strData);

		for (int i = 0; !bExempt && i < vExempt.GetSize (); i++)
			bExempt = m_strData.Find (vExempt [i]) != -1 ? true : false;

		if (!bExempt)
			m_pView->UpdateSerialData (m_strData);

		ParsePackets (m_strData, vPackets);

		for (int nPacket = 0; nPacket < vPackets.GetSize (); nPacket++) {
			CStringArray v;
			CString strPacket = vPackets [nPacket];

			strResult = OnCommand (strPacket, paddr);
		}

		m_strData.Empty ();
		m_encoding = ASCII;
	}

	return strResult;
}


void CUdpThread::ProcessData ()
{
	timeval timeout = { 1, 0 };
	fd_set readfds;

	readfds.fd_count = 1;
	readfds.fd_array [0] = m_sock;

	::SetLastError (0);
	int nSelect = ::select (0, &readfds, NULL, NULL, &timeout);
	
	if (!CheckConnection ())
		nSelect = 0;

	if (nSelect > 0) {
		char szBuffer [m_nBufferSize] = { 0 };
		SOCKADDR_IN addr;
		int nSize = sizeof (addr);

		ZeroMemory (&addr, sizeof (addr));

		TRACEF (a2w (GetRuntimeClass ()->m_lpszClassName));
		int nResult = ::recvfrom (m_sock, szBuffer, sizeof (szBuffer), 0, (SOCKADDR *)&addr, &nSize);

		if (nResult != SOCKET_ERROR) {
			CString strSend = OnDataRecieved (szBuffer, nResult, &addr); 

			if (strSend.GetLength ()) {
				if (::sendto (m_sock, w2a (strSend), strSend.GetLength (), 0, (SOCKADDR *)&addr, sizeof (addr)) == SOCKET_ERROR)
					TRACEF (GetError (::WSAGetLastError ()));

				{
					SOCKADDR_IN ex;
					SOCKET sock = socket(AF_INET, SOCK_DGRAM, 0);
					char cFlag;

					memcpy(&ex, &addr, sizeof(ex));
					ex.sin_port = htons(BW_HOST_UDP_PORT_EX);

					cFlag = 1;
					::setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &cFlag, sizeof(cFlag));

					cFlag = 1;
					::setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &cFlag, sizeof(cFlag));

					DWORD dwBytes = ::sendto(sock, w2a(strSend), strSend.GetLength(), 0, (const SOCKADDR*)&ex, sizeof(ex));
					TRACEF(ToString(dwBytes));
				}
			}
		}
	}
}

CString CUdpThread::GetError (UINT nError)
{
	CString str;

	str.Format (LoadString (IDS_UNKNOWNSOCKETERROR), nError);

	switch (nError) {
	case WSAEACCES:		//	(10013) 
			str = LoadString (IDS_WSAEACCES);
			//	An attempt was made to access a socket in a way forbidden by its access permissions.
			//	An example is using a broadcast localess for sendto without broadcast permission being
			//	set using setsockopt(SO_BROADCAST). Another possible reason for the WSAEACCES error 
			//	is that when the bind function is called (on Windows NT 4 SP4 or later), another 
			//	application, service, or kernel mode driver is bound to the same localess with exclusive 
			//	access. Such exclusive access is a new feature of Windows NT 4 SP4 and later, and is 
			//	implemented by using the SO_EXCLUSIVElocalUSE option. 
			break;
		case WSAEADDRINUSE:	//	(10048) 
			str = LoadString (IDS_WSAEADDRINUSE);
			//	Typically, only one usage of each socket localess (protocol/IP localess/port) is permitted. 
			//	This error occurs if an application attempts to bind a socket to an IP localess/port that 
			//	has already been used for an existing socket, or a socket that was not closed properly, 
			//	or one that is still in the process of closing. For server applications that need to bind 
			//	multiple sockets to the same port number, consider using setsockopt(SO_REUSElocal). Client 
			//	applications usually need not call bind at all—connect chooses an unused port automatically.
			// When bind is called with a wildcard localess (involving local_ANY), a WSAElocalINUSE error 
			// could be delayed until the specific localess is committed. This could happen with a call to 
			// another function later, including connect, listen, WSAConnect, or WSAJoinLeaf. 
			break;
		case WSAEADDRNOTAVAIL:	//	(10049) 
			str = LoadString (IDS_WSAEADDRNOTAVAIL);
			//	The requested localess is not valid in its context. This normally results from an attempt 
			//	to bind to an localess that is not valid for the local machine. This can also result from 
			//	connect, sendto, WSAConnect, WSAJoinLeaf, or WSASendTo when the remote localess or port is 
			//	not valid for a remote machine (for example, localess or port 0). 
			break;
		case WSAEAFNOSUPPORT:	//	(10047) 
			str = LoadString (IDS_WSAEAFNOSUPPORT);
			//	An localess incompatible with the requested protocol was used. All sockets are created with 
			//	an associated localess family (that is, AF_INET for Internet Protocols) and a generic 
			//	protocol type (that is, SOCK_STREAM). This error is returned if an incorrect protocol is 
			//	explicitly requested in the socket call, or if an localess of the wrong family is used for 
			//	a socket, for example, in sendto. 
			break;
		case WSAEALREADY:	//	(10037) 
			str = LoadString (IDS_WSAEALREADY);
			//	An operation was attempted on a nonblocking socket with an operation already in progress 
			//	that is, calling connect a second time on a nonblocking socket that is already connecting, 
			//	or canceling an asynchronous request (WSAAsyncGetXbyY) that has already been canceled or 
			//	completed. 
			break;
		case WSAECONNABORTED:	//	(10053) 
			str = LoadString (IDS_WSAECONNABORTED);
			//	An established connection was aborted by the software in your host machine, possibly due 
			//	to a data transmission time-out or protocol error. 
			break;
		case WSAECONNREFUSED:	//	(10061) 
			str = LoadString (IDS_WSAECONNREFUSED);
			//	No connection could be made because the target machine actively refused it. This usually 
			//	results from trying to connect to a service that is inactive on the foreign host that is, 
			//	one with no server application running. 
			break;
		case WSAECONNRESET:	//	(10054) 
			str = LoadString (IDS_WSAECONNRESET);
			//	An existing connection was forcibly closed by the remote host. This normally results if the 
			//	peer application on the remote host is suddenly stopped, the host is rebooted, or the remote 
			//	host uses a hard close (see setsockopt for more information on the SO_LINGER option on the 
			//	remote socket.) This error may also result if a connection was broken due to keep-alive 
			//	activity detecting a failure while one or more operations are in progress. Operations that 
			//	were in progress fail with WSAENETRESET. Subsequent operations fail with WSAECONNRESET. 
			break;
		case WSAEDESTADDRREQ:	//	(10039) 
			str = LoadString (IDS_WSAEDESTADDRREQ);
			//	A required localess was omitted from an operation on a socket. For example, this error is 
			//	returned if sendto is called with the remote localess of local_ANY. 
			break;
		case WSAEFAULT:	//	(10014) 
			str = LoadString (IDS_WSAEFAULT);
			//	The system detected an invalid pointer localess in attempting to use a pointer argument of 
			//	a call. This error occurs if an application passes an invalid pointer value, or if the 
			//	length of the buffer is too small. For instance, if the length of an argument, which is a 
			//	SOCKlocal structure, is smaller than the sizeof(SOCKlocal). 
			break;
		case WSAEHOSTDOWN:	//	(10064) 
			str = LoadString (IDS_WSAEHOSTDOWN);
			//	A socket operation failed because the destination host is down. A socket operation 
			//	encountered a dead host. Networking activity on the local host has not been initiated.
			//	These conditions are more likely to be indicated by the error WSAETIMEDOUT. 
			break;
		case WSAEHOSTUNREACH:	//	(10065) 
			str = LoadString (IDS_WSAEHOSTUNREACH);
			//	A socket operation was attempted to an unreachable host. See WSAENETUNREACH. 
			break;
		case WSAEINPROGRESS:	//	(10036) 
			str = LoadString (IDS_WSAEINPROGRESS);
			//	A blocking operation is currently executing. Windows Sockets only allows a single blocking 
			//	operation per task or thread to be outstanding, and if any other function call is made 
			//	(whether or not it references that or any other socket) the function fails with the 
			//	WSAEINPROGRESS error. 
			break;
		case WSAEINTR:	//	(10004) 
			str = LoadString (IDS_WSAEINTR);
			//	A blocking operation was interrupted by a call to WSACancelBlockingCall. 
			break;
		case WSAEINVAL:	//	(10022) 
			str = LoadString (IDS_WSAEINVAL);
			//	The socket is already bound to an address or  
			//	Some invalid argument was supplied (for example, specifying an invalid level to the 
			//	setsockopt function). In some instances, it also refers to the current state of the 
			//	socket for instance, calling accept on a socket that is not listening. 
			break;
		case WSAEISCONN:	//	(10056)
			str = LoadString (IDS_WSAEISCONN);
			//	A connect request was made on an already-connected socket. Some implementations also return 
			//	this error if sendto is called on a connected SOCK_DGRAM socket (for SOCK_STREAM sockets, 
			//	the to parameter in sendto is ignored) although other implementations treat this as a legal 
			//	occurrence. 
			break;
		case WSAEMFILE:	//	(10024)
			str = LoadString (IDS_WSAEMFILE);
			//	Too many open sockets. Each implementation may have a maximum number of socket handles 
			//	available, either globally, per process, or per thread. 
			break;
		case WSAEMSGSIZE:	//	(10040) 
			str = LoadString (IDS_WSAEMSGSIZE);
			//	A message sent on a datagram socket was larger than the internal message buffer or some 
			//	other network limit, or the buffer used to receive a datagram was smaller than the datagram 
			//	itself. 
			break;
		case WSAENETDOWN:	//	(10050) 
			str = LoadString (IDS_WSAENETDOWN);
			//	A socket operation encountered a dead network. This could indicate a serious failure of 
			//	the network system (that is, the protocol stack that the Windows Sockets DLL runs over), 
			//	the network interface, or the local network itself. 
			break;
		case WSAENETRESET:	//	(10052) 
			str = LoadString (IDS_WSAENETRESET);
			//	The connection has been broken due to keep-alive activity detecting a failure while the 
			//	operation was in progress. It can also be returned by setsockopt if an attempt is made to 
			//	set SO_KEEPALIVE on a connection that has already failed. 
			break;
		case WSAENETUNREACH:	//	(10051)
			str = LoadString (IDS_WSAENETUNREACH);
			//	A socket operation was attempted to an unreachable network. This usually means the local 
			//	software knows no route to reach the remote host. 
			break;
		case WSAENOBUFS:	//	(10055)
			str = LoadString (IDS_WSAENOBUFS);
			//	An operation on a socket could not be performed because the system lacked sufficient buffer 
			//	space or because a queue was full. 
			break;
		case WSAENOPROTOOPT:	//	(10042) 
			str = LoadString (IDS_WSAENOPROTOOPT);
			//	An unknown, invalid or unsupported option or level was specified in a getsockopt or 
			//	setsockopt call. 
			break;
		case WSAENOTCONN:	//	(10057)
			str = LoadString (IDS_WSAENOTCONN);
			//	A request to send or receive data was disallowed because the socket is not connected and 
			//	(when sending on a datagram socket using sendto) no localess was supplied. Any other type 
			//	of operation might also return this error—for example, setsockopt setting SO_KEEPALIVE 
			//	if the connection has been reset. 
			break;
		case WSAENOTSOCK:	//	(10038)
			str = LoadString (IDS_WSAENOTSOCK);
			//	An operation was attempted on something that is not a socket. Either the socket handle 
			//	parameter did not reference a valid socket, or for select, a member of an fd_set was not 
			//	valid. 
			break;
		case WSAEOPNOTSUPP:	//	(10045)
			str = LoadString (IDS_WSAEOPNOTSUPP);
			//	The attempted operation is not supported for the type of object referenced. Usually this 
			//	occurs when a socket descriptor to a socket that cannot support this operation is trying 
			//	to accept a connection on a datagram socket. 
			break;
		case WSAEPFNOSUPPORT:	//	(10046) 
			str = LoadString (IDS_WSAEPFNOSUPPORT);
			//	The protocol family has not been configured into the system or no implementation for it 
			//	exists. This message has a slightly different meaning from WSAEAFNOSUPPORT. However, it 
			//	is interchangeable in most cases, and all Windows Sockets functions that return one of 
			//	these messages also specify WSAEAFNOSUPPORT. 
			break;
		case WSAEPROCLIM:	//	(10067)
			str = LoadString (IDS_WSAEPROCLIM);
			//	A Windows Sockets implementation may have a limit on the number of applications that can 
			//	use it simultaneously. WSAStartup may fail with this error if the limit has been reached. 
			break;
		case WSAEPROTONOSUPPORT:	//	(10043)
			str = LoadString (IDS_WSAEPROTONOSUPPORT);
			//	The requested protocol has not been configured into the system, or no implementation for 
			//	it exists. For example, a socket call requests a SOCK_DGRAM socket, but specifies a stream 
			//	protocol. 
			break;
		case WSAEPROTOTYPE:	//	(10041)
			str = LoadString (IDS_WSAEPROTOTYPE);
			//	A protocol was specified in the socket function call that does not support the semantics of
			//	the socket type requested. For example, the ARPA Internet UDP protocol cannot be specified 
			//	with a socket type of SOCK_STREAM. 
			break;
		case WSAESHUTDOWN:	//	(10058)
			str = LoadString (IDS_WSAESHUTDOWN);
			//	A request to send or receive data was disallowed because the socket had already been shut 
			//	down in that direction with a previous shutdown call. By calling shutdown a partial close 
			//	of a socket is requested, which is a signal that sending or receiving, or both have been 
			//	discontinued. 
			break;
		case WSAESOCKTNOSUPPORT:	//	(10044) 
			str = LoadString (IDS_WSAESOCKTNOSUPPORT);
			//	The support for the specified socket type does not exist in this localess family. For 
			//	example, the optional type SOCK_RAW might be selected in a socket call, and the 
			//	implementation does not support SOCK_RAW sockets at all. 
			break;
		case WSAETIMEDOUT:	//	(10060) 
			str = LoadString (IDS_WSAETIMEDOUT);
			//	A connection attempt failed because the connected party did not properly respond after a 
			//	period of time, or the established connection failed because the connected host has failed 
			//	to respond. 
			break;
		case WSAEWOULDBLOCK:	//	(10035)
			str = LoadString (IDS_WSAEWOULDBLOCK);
			//	This error is returned from operations on nonblocking sockets that cannot be completed 
			//	immediately, for example recv when no data is queued to be read from the socket. It is a 
			//	nonfatal error, and the operation should be retried later. It is normal for WSAEWOULDBLOCK 
			//	to be reported as the result from calling connect on a nonblocking SOCK_STREAM socket, 
			//	since some time must elapse for the connection to be established. 
			break;
		case WSAHOST_NOT_FOUND:	//	(11001) 
			str = LoadString (IDS_WSAHOST_NOT_FOUND);
			//	No such host is known. The name is not an official host name or alias, or it cannot be 
			//	found in the database(s) being queried. This error may also be returned for protocol and 
			//	service queries, and means that the specified name could not be found in the relevant 
			//	database. 
			break;
		case WSANOTINITIALISED:	//	(10093)
			str = LoadString (IDS_WSANOTINITIALISED);
			//	Either the application has not called WSAStartup or WSAStartup failed. The application 
			//	may be accessing a socket that the current active task does not own (that is, trying to 
			//	share a socket between tasks), or WSACleanup has been called too many times. 
			break;
		case WSANO_DATA:	//	(11004)
			str = LoadString (IDS_WSANO_DATA);
			//	The requested name is valid and was found in the database, but it does not have the correct 
			//	associated data being resolved for. The usual example for this is a host name-to-localess 
			//	translation attempt (using gethostbyname or WSAAsyncGetHostByName) which uses the DNS 
			//	(Domain Name Server). An MX record is returned but no A record—indicating the host itself 
			//	exists, but is not directly reachable. 
			break;
		case WSANO_RECOVERY:	//	(11003)
			str = LoadString (IDS_WSANO_RECOVERY);
			//	This indicates some sort of nonrecoverable error occurred during a database lookup. This 
			//	may be because the database files (for example, BSD-compatible HOSTS, SERVICES, or 
			//	PROTOCOLS files) could not be found, or a DNS request was returned by the server with a 
			//	severe error. 
			break;
		case WSASYSNOTREADY:	//	(10091)
			str = LoadString (IDS_WSASYSNOTREADY);
			//	This error is returned by WSAStartup if the Windows Sockets implementation cannot function 
			//	at this time because the underlying system it uses to provide network services is currently 
			//	unavailable. Users should check: 
			//		-That the appropriate Windows Sockets DLL file is in the current path. 
			//		-That they are not trying to use more than one Windows Sockets implementation 
			//			simultaneously. If there is more than one Winsock DLL on your system, be sure the 
			//			first one in the path is appropriate for the network subsystem currently loaded. 
			//		-The Windows Sockets implementation documentation to be sure all necessary components 
			//			are currently installed and configured correctly. 
			break;
		case WSATRY_AGAIN:	//	(11002)
			str = LoadString (IDS_WSATRY_AGAIN);
			//	This is usually a temporary error during host name resolution and means that the local 
			//	server did not receive a response from an authoritative server. A retry at some time later 
			//	may be successful. 
			break;
		case WSAVERNOTSUPPORTED:	//	(10092)
			str = LoadString (IDS_WSAVERNOTSUPPORTED);
			//	The current Windows Sockets implementation does not support the Windows Sockets 
			//	specification version requested by the application. Check that no old Windows Sockets 
			//	DLL files are being accessed. 
			break;
		case WSAEDISCON:	//	(10101)
			str = LoadString (IDS_WSAEDISCON);
			//	Returned by WSARecv and WSARecvFrom to indicate that the remote party has initiated a 
			//	graceful shutdown sequence. 
			break;
	}

	return str;
}

void CUdpThread::ReportError(UINT Error)
{
	CString str = GetError (Error);
	MsgBox (str);
}

void CUdpThread::OnKillThread (WPARAM wParam, LPARAM lParam)
{
	TRACEF ("CUdpThread::OnKillThread");

	::SetEvent (m_hExit);
	ExitInstance ();
	delete this;

	if (HANDLE h = (HANDLE)wParam)
		::SetEvent (h);

	_endthreadex (0);
}

void CUdpThread::OnBroadcastInkCode (WPARAM wParam, LPARAM lParam)
{
	std::vector <std::wstring> v;

	m_pView->SendMessage (WM_GET_INK_CODES, (WPARAM)&v, 0);

	for (int i = 0; i < v.size (); i++) {
		CString str = v [i].c_str ();
		SOCKADDR_IN remoteAddr;
		remoteAddr.sin_family = AF_INET;
		remoteAddr.sin_port = htons (BW_HOST_BROADCAST_PORT);
		remoteAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);

		DWORD dwBytes = ::sendto (m_sock, w2a (str), str.GetLength (), 0, (const SOCKADDR* )&remoteAddr, sizeof (remoteAddr));

		if (dwBytes == SOCKET_ERROR) {
			TRACEF (FormatMessage (::WSAGetLastError ()));
		}
	}

	/* TODO: rem
	if (CStringArray * pvCode = (CStringArray *)wParam) {
		std::vector <std::wstring> vMAC = FoxjetDatabase::GetMacAddresses ();

		while (pvCode->GetSize ()) {
			CStringArray v;

			v.Add (BW_HOST_INK_CODE);
	
			for (int i = 0; i < vMAC.size (); i++)
				v.Add (vMAC [i].c_str ());

			while (pvCode->GetSize ()) {
				v.Add ((* pvCode) [0]);
				pvCode->RemoveAt (0);
			}

			CString str = FoxjetDatabase::ToString (v);

			while (str.GetLength () > m_nBufferSize) {
				int nIndex = v.GetSize () - 1;

				pvCode->Add (v [nIndex]);
				v.RemoveAt (nIndex);
				str = FoxjetDatabase::ToString (v);
			}

			{
				SOCKADDR_IN remoteAddr;
				remoteAddr.sin_family = AF_INET;
				remoteAddr.sin_port = htons (BW_HOST_BROADCAST_PORT);
				remoteAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);

				DWORD dwBytes = ::sendto (m_sock, w2a (str), str.GetLength (), 0, (const SOCKADDR* )&remoteAddr, sizeof (remoteAddr));

				if (dwBytes == SOCKET_ERROR) {
					TRACEF (FormatMessage (::WSAGetLastError ()));
				}
			}
		}

		delete pvCode;
	}
	*/
}
