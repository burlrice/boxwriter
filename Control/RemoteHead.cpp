// RemoteHead.cpp : implementation file
//

#include "stdafx.h"
#include <io.h>
#include <afxinet.h>

#include "Control.h"
#include "Resource.h"
#include "Defines.h"
#include "MainFrm.h"
#include "fj_socket.h"
#include "fj_label.h"
#include "Database.h"
#include "RemoteHead.h"
#include "Utils.h"
#include "ProgressDlg.h"
#include "BitmapElement.h"
#include "Edit.h"
#include "Debug.h"
#include "Utils.h"
#include "TemplExt.h"
#include "DynamicTableDlg.h"
#include "fj_bitmap.h"
#include "FileExt.h"

#include "TextElement.h"
#include "Extern.h"
#include "SyncDlg.h"
#include "HeadDlg.h"
#include "fj_dyntext.h"
#include "CountElement.h"
#include "fj_counter.h"
#include "Parse.h"
#include "Endian.h"

#include "BarcodeElement.h"
#include "DataMatrixElement.h"

#include "..\Marksman\appconf.h"

#ifdef _DEBUG
	#include "..\Custom\IpDemo\Debug.h"

	//#define __TRACE_IPC__
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetIpElements;
using namespace FoxjetDatabase;
using namespace Head;
using namespace RemoteHead;

const TCHAR ::CRemoteHead::m_cBreak = 0x0c;


/////////////////////////////////////////////////////////////////////////////
void TraceDiagnostic (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	CString str;
	TCHAR sz [_MAX_PATH] = { 0 };

	::GetFileTitle (lpszFile, sz, ARRAYSIZE (sz) - 1);

	str.Format (_T ("%s(%d): %s"), sz, lLine, lpsz);
	//((CControlView * )(((CMainFrame *)theApp.m_pMainWnd)->m_pControlView))->UpdateSerialData (str);
}

#define TRACE_DIAG(s) TraceDiagnostic ((s), _T (__FILE__), __LINE__)

/////////////////////////////////////////////////////////////////////////////

::CRemoteHead::CNotifySocket::CNotifySocket (UINT nOwnerThreadID, HANDLE hOwnerThread, const CString & strAddr)
:	m_nOwnerThreadID (nOwnerThreadID),
	m_strAddr (strAddr),
	m_hOwnerThread (hOwnerThread),
	CAsyncSocket ()
{
}

::CRemoteHead::CNotifySocket::~CNotifySocket ()
{
}

void ::CRemoteHead::CNotifySocket::OnConnect (int nErrorCode)
{
	if (!nErrorCode) {
		#ifdef _DEBUG
		{
			CString strAddr;
			UINT nPort;

			GetPeerName (strAddr, nPort);

			TRACEF (_T ("Connected: ") + strAddr);
		}
		#endif

		SOCKET socket = Detach ();

		while (!::PostThreadMessage (m_nOwnerThreadID, TM_SOCKET_CONNECT, (WPARAM)socket, 0))
			::Sleep (0);
	}
	else {
		TRACEF (m_strAddr + _T (": [") + ToString (nErrorCode) + _T ("] ") + CUdpThread::GetError (nErrorCode));

		::Sleep (50);

		/*
		#ifdef _DEBUG
		switch (nErrorCode) {
		case 10065:
		case 10060:
			::Sleep (250);
			break;
		}
		#endif
		*/

		while (!::PostThreadMessage (m_nOwnerThreadID, TM_SOCKET_RETRY, 0, 0))
			::Sleep (0);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CRemoteHead

IMPLEMENT_DYNCREATE(CRemoteHead, CHead)

CRemoteHead::CRemoteHead() 
:	m_hLabelListUpdate (NULL),
	m_bDynamicDataDownload (false),
	m_socket (INVALID_SOCKET),
	m_pConnect (NULL),
	m_nTimeoutStep (0)
{
	CString str;

	m_state.m_tmLast		= CTime::GetCurrentTime ();
	m_state.m_bClosed		= true;
	m_state.m_bShortTimeout = false;
	m_state.m_bGetStatus	= false;

	//TRACEF ("CRemoteHead");
}

CRemoteHead::~CRemoteHead()
{
	TRACEF (_T ("~CRemoteHead: ") + GetFriendlyName ());
}

BOOL CRemoteHead::InitInstance()
{
	//TRACEF ("CRemoteHead::InitInstance");

	if (!AfxSocketInit() ) 
	{
		MsgBox (NULL, IDS_SOCKET_INIT_FAILED);
		return FALSE;
	}
	
	CMainFrame *pFrame = (CMainFrame *) theApp.m_pMainWnd;
	CView *pView = pFrame->m_pControlView;

	m_hControlWnd =  pView->GetSafeHwnd();

	CString sEvent;
	sEvent.Format (_T ("Exit %lu"), m_hThread );
	
	m_hExit = CreateEvent ( NULL, true, false, sEvent);

	m_bRemoteHead = true;
	m_nSequence = 1;
	GetLocalFontList ();
	m_eTaskState = STOPPED;
	m_DrawContext = FoxjetCommon::EDITOR;

	if ( !theApp.m_Database.IsOpen () ) 
	{
		if (!FoxjetCommon::OpenDatabase (theApp.m_Database, theApp.GetDSN (), theApp.sDatabase)) 
		{
			MsgBox (NULL, LoadString (IDS_FAILEDTOOPENDB), ::AfxGetAppName (), MB_ICONERROR | MB_OK);
			FoxjetCommon::ExitInstance (GlobalVars::CurrentVersion);
			::AfxAbort ();
		}
	}

	m_state.m_tmLast = CTime::GetCurrentTime ();

	return CHead::InitInstance ();
}

int CRemoteHead::ExitInstance()
{
	TRACEF ("CRemoteHead::ExitInstance");

	if (m_pConnect) {
		delete m_pConnect;
		m_pConnect = NULL;
	}

	return CHead::ExitInstance ();
}

BEGIN_MESSAGE_MAP(CRemoteHead, CHead)
	//{{AFX_MSG_MAP(CRemoteHead)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_THREAD_MESSAGE (TM_STATUS_UPDATE,			OnStatusUpdate)
	ON_THREAD_MESSAGE (TM_DATA_RECIEVED,			OnDataRecieved)
	ON_THREAD_MESSAGE (TM_UPDATE_LABELS,			OnUpdateLabels)
	ON_THREAD_MESSAGE (TM_UPDATE_FONTS,				OnUpdateFonts)
	ON_THREAD_MESSAGE (TM_UPDATE_FIRMWARE,			OnUpdateFirmware)
	ON_THREAD_MESSAGE (TM_UPDATE_GA,				OnUpdateGa)
	ON_THREAD_MESSAGE (TM_INITIAL_STARTUP,			OnInitialStartup)
	ON_THREAD_MESSAGE (TM_GET_MSG_IDS,				OnGetMessageIDs)
	ON_THREAD_MESSAGE (TM_DELETE_LABEL,				OnDeleteLabels)
	ON_THREAD_MESSAGE (TM_DELETE_MEMSTORE,			OnDeleteMemStore)
	ON_THREAD_MESSAGE (TM_SOCKET_CONNECT,			OnSocketConnect)
	ON_THREAD_MESSAGE (TM_SOCKET_RETRY,				OnSocketRetryConnect)
	ON_THREAD_MESSAGE (TM_SOCKET_DISCONNECT,		OnSocketDisconnect)
	ON_THREAD_MESSAGE (TM_SYNC_LABEL_STORE,			OnSyncronizeLabelStore)
	ON_THREAD_MESSAGE (WM_HEAD_AMSCYCLE,			OnAmsCycle)
	ON_THREAD_MESSAGE (TM_CHANGE_COUNTS,			OnChangeCounts)
	ON_THREAD_MESSAGE (TM_DOWNLOAD_DYNDATA,			OnDownloadDynData)
	ON_THREAD_MESSAGE (TM_SETSERIALPENDING,			OnSetSerialPending)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRemoteHead message handlers

void CRemoteHead::OnSocketConnect (UINT wParam, LONG lParam)
{
	TRACEF ("CRemoteHead::OnSocketConnect");

	if (m_socket != INVALID_SOCKET)
		Disconnect ();

	m_socket = (SOCKET)wParam;
	ASSERT (m_socket != INVALID_SOCKET);
	m_state.m_bClosed = false;

	CString strBarcode, strDatamatrix, strStrobe;
	CString strConfig = GetConfiguration (strBarcode, strDatamatrix, strStrobe);
	m_bDigiBoard = strConfig.Find (_T ("HeadType=")) != -1;

	if (m_Config.m_HeadSettings.m_bDigiNET != m_bDigiBoard) 
		MsgBox (LoadString (m_bDigiBoard ? IDS_HEADCONFIGMISMATCHNEXT : IDS_HEADCONFIGMISMATCHNET), MB_ICONINFORMATION);

	if (IsDigiBoard ()) {
		CString str;
		
		m_Status.m_bFPGA = true;
		QueryDiskUsage ();
		Send (IPC_SET_PRINTER_INFO, GetNextSequence (), _T ("INFO_SOCKET=F;"));

		str.Format (_T ("SYSTEM_TIME=%s;"), CTime::GetCurrentTime ().Format (_T ("%Y/%m/%d %H:%M:%S")));
		//TRACEF (str);
		Send (IPC_SET_SYSTEM_INFO, GetNextSequence (), str);

		/*
		{
			CPacket reply;
			Send (IPC_GET_SYSTEM_TIME_INFO, GetNextSequence (), _T (""), &reply);
			CString str = a2w (reply.Data);
			TRACEF (str);
		}
		*/

		bool bSendConfig = CompareConfiguration (strConfig, strBarcode, strDatamatrix, strStrobe) == false;

		if (bSendConfig) {
			CString str;

			str.Format (LoadString (IDS_SAVEALLPARAMS), m_Config.m_HeadSettings.m_strName, m_Config.m_HeadSettings.m_strUID);

			if (MsgBox (str, m_Config.m_HeadSettings.m_strName, MB_YESNO) == IDYES) 
				SendConfiguration();
		}
	}

	GetVersionInfo ();
	Send (IPC_SET_STATUS_MODE, GetNextSequence(), _T ("STATUS_MODE=ON"));
	Send (IPC_SET_PRINTER_MODE, GetNextSequence (), _T ("APS_MODE=DISABLE;"));
	Send (IPC_SET_PRINTER_INFO, GetNextSequence (), _T ("STANDBY_AUTO=FALSE;"));

	if (m_Config.IsDisabled ()) {
		m_bDisabled = true;
		m_eTaskState = DISABLED;
		while ( !::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this) ) Sleep (0);
	}
	else {
		DWORD dwSync = Foxjet3d::CHeadDlg::GetSyncSettings (theApp.m_Database);

		m_bDisabled = false;
		
		if ( m_eTaskState == DISABLED )
			m_eTaskState = STOPPED;

		if (!IsDigiBoard ())
			SendConfiguration ();

		Send (IPC_GET_LABEL_ID_SELECTED, GetNextSequence());

		while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this)) ::Sleep (0);

		if (dwSync == 0)
			dwSync |= CSyncDlg::SYNC_NO_PROMPT;

		if (!theApp.GetSingleMsgMode ()) 
			while (!PostThreadMessage (TM_SYNC_LABEL_STORE, 0, dwSync)) ::Sleep (0);
	}

	Send (IPC_GET_PRINTER_MODE, GetNextSequence()); // to update 'image state'
}

void CRemoteHead::OnSocketDisconnect (UINT wParam, LONG lParam)
{
	TRACEF ("CRemoteHead::OnSocketDisconnect");
	Disconnect ();
}

void CRemoteHead::OnHeadConfigChanged(UINT wParam, LONG lParam)
{
	const FoxjetDatabase::HEADTYPE typeOld = m_Config.m_HeadSettings.m_nHeadType;

	{ // wait for CControlView::OnHeadConnect to assign m_sDatabaseName
		for (int i = 0; (m_sDatabaseName.GetLength () == 0) && (i < 10); i++) 
			::Sleep (250);

		ASSERT (m_sDatabaseName.GetLength () != 0);
	}

	LoadStandbySettings ();

	if (m_Config.LoadSettings ( theApp.m_Database, m_sDatabaseName)) {
		if (m_Config.IsDisabled ()) {
			if (!m_bDisabled)
				OnStopTask ( 0, 0 );

			m_bDisabled = true;
			m_eTaskState = DISABLED;
		}
		else {
			if (m_bDisabled) {
				m_bDisabled = false;

				if (m_eTaskState == DISABLED)
					m_eTaskState = STOPPED;
			}
		}
	}

	SendConfiguration();
	
	if (m_socket != INVALID_SOCKET) {
		CString strAddr;
		SOCKADDR_IN sockAddr;
		int nSockAddrLen = sizeof (sockAddr);

		memset (&sockAddr, 0, sizeof (sockAddr));

		if (getpeername (m_socket, (SOCKADDR*)&sockAddr, &nSockAddrLen) != SOCKET_ERROR) 
			strAddr = inet_ntoa (sockAddr.sin_addr);

		bool bReconnect = strAddr.CompareNoCase (GetUID ()) != 0;

		//TRACEF (_T ("CRemoteHead::OnHeadConfigChanged: ") + GetFriendlyName () + _T (": ") + strAddr + _T (" --> ") + GetUID ());

		if (bReconnect) 
			Disconnect (); // automatically calls Connect
	}

	if (m_Config.m_HeadSettings.m_nHeadType != typeOld)
		m_pImageEng->Free ();

	CHead::OnHeadConfigChanged (wParam, lParam);
}

void CRemoteHead::OnStatusUpdate(UINT wParam, LONG lParam)
{
	while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this))
		::Sleep (0);
}

void CRemoteHead::ProcessRxData(const CPacket * pMsg)
{
	CString sTemp;
	bool bStatusUpdate = false;

#ifdef _DEBUG
{
	CString str = a2w (pMsg->Data);
	bool bDebug = false;
//	bDebug |= (!m_Config.m_HeadSettings.m_strUID.CompareNoCase ("10.1.2.101"));
	bDebug |= (str.Find (_T ("TODO: rem")) != -1);
	bDebug |= (str.Find (_T (".c")) != -1);
	bDebug |= (str.Find (_T ("C:\\MyPrograms\\")) != -1);
//	bDebug |= (str.Find (_T ("InkUsage")) != -1);
	bDebug |= (str.Find (_T ("DEBUG: ")) != -1);
	//bDebug |= (pMsg->Header.Cmd == IPC_STATUS); 

	if (bDebug) {
		TRACEF (/* m_Config.m_HeadSettings.m_strUID + ": " + */ str);
	}
}
#endif //_DEBUG


	switch ( pMsg->Header.Cmd ) 
	{
		case IPC_SET_GROUP_INFO:	// Set info about printers in system group. group leader is first. 
											//'GROUP_NAME="abc";GROUP_COUNT=n;GROUP_IDS="aaa","bbb","ccc","ddd";
											// GROUP_IPADDRS=n.n.n.n,n.n.n.n,n.n.n.n;' 4 element IP addresses.
			{
				CMapStringToString KeyValue;
				CString sData, sValue;
				sData.Format(_T ("%s"), a2w (pMsg->Data));
				CKeyParse::Parse(sData, KeyValue);
				if ( KeyValue.Lookup(_T ("GROUP_COUNT"), sValue) )
				{
					m_Status.m_lCount = _ttol ( sValue );
					::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this);
				}
			}
			break;
//		case IPC_SET_PRINTER_INFO:
//			{
//				CHeadCfg Temp;
//				Temp.m_HeadSettings = m_Config.m_HeadSettings;
//				CString sData;
//				sData.Format("%s", a2w (pMsg->Data));
//			}
//			break;
		case IPC_SET_PRINTER_MODE:
			{
				CMapStringToString KeyValue;
				CString sData, sValue;
				sData.Format(_T ("%s"), a2w (pMsg->Data));
				CKeyParse::Parse(sData, KeyValue);
				TASKSTATE State = m_eTaskState;
				struct
				{
					LPCTSTR		m_lpszState;
					TASKSTATE	m_nState;
				} static const map [] = 
				{
					{ _T ("IDLE"),		IDLE		},
					{ _T ("STOP"),		STOPPED		},
					{ _T ("RUNNING"),	RUNNING		},
					{ _T ("RESUME"),	RUNNING		},
					{ _T ("STANDBY"),	IDLE		}, // TODO: rem: STANDBY ?
				};

				if (KeyValue.Lookup (_T ("PRINT_MODE"), sValue)) {
					for (int i = 0; i < ARRAYSIZE (map); i++) {
						if (!sValue.CompareNoCase (map [i].m_lpszState)) {
							m_eTaskState = map [i].m_nState;
							break;
						}
					}
				}

				if ( State != m_eTaskState )
					while ( !::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this) ) Sleep (0);
			}
			break;
		case IPC_SET_SYSTEM_INFO:
			{
				/*
				CHeadCfg Temp;
				CMapStringToString KeyValue;
				CString sData, sValue;
				sData.Format (_T ("%s"), a2w (pMsg->Data));

				CKeyParse::Parse(sData, KeyValue);
				if ( KeyValue.Lookup(_T ("CONVEYOR_SPEED"), sValue) )
				{
					if ( m_Status.m_dLineSpeed != FoxjetDatabase::ttof (sValue) )
					{
						m_Status.m_dLineSpeed = FoxjetDatabase::ttof (sValue);
						bStatusUpdate = true;
					}
				}
				*/
			}
			break;
		case IPC_GET_SYSTEM_BARCODES:
//			sTemp.Format ( "%s", a2w (pMsg->Data));
			break;
		case IPC_GET_SYSTEM_TIME_INFO:
//			sTemp.Format ( "%s", a2w (pMsg->Data));
			break;
		case IPC_PHOTO_TRIGGER:
//			m_Status.m_lCount++;
//			bStatusUpdate = true;
			break;
		case IPC_STATUS:	// Device status inforamtion.
			{
				CMapStringToString KeyValue;
				CString sData, sValue;
				sData.Format(_T ("%s"), a2w (pMsg->Data));
				CString strLastStatus = GetStatusMessage ();

				m_state.m_tmLast		= CTime::GetCurrentTime ();
				m_state.m_bClosed		= false;
				m_state.m_bGetStatus	= false;

//				TRACEF (sData);
//				Debug.LogEntry ( sData );

				/*
				#ifdef _DEBUG
				sData.Replace (_T ("InkLevel=OUT;"), _T ("InkLevel=OK;"));
				sData.Replace (_T ("VoltReady=F;"), _T ("VoltReady=T;"));
				sData.Replace (_T ("Heater1Ready=F;"), _T ("Heater1Ready=T;"));
				TRACEF (sData);
				#endif
				*/
				
				CKeyParse::Parse(sData, KeyValue);

				if ( KeyValue.Lookup(_T ("CountSel"), sValue) ) 
				{
					m_Status.m_lCount = _ttol (sValue);
					bStatusUpdate = true;
				}

				if ( KeyValue.Lookup(_T ("InkLevel"), sValue) ) 
				{
					sValue.Replace ('"', ' ');
					sValue.TrimLeft();
					sValue.TrimRight();
					if ( sValue.CompareNoCase (_T ("LOW")) == 0)
					{
							m_Status.m_nInkLevel = LOW_INK; 
							bStatusUpdate = true;
					}
					else
					{
						if ( sValue.CompareNoCase (_T ("EMPTY")) == 0)
						{
								m_Status.m_nInkLevel = INK_OUT; 
								bStatusUpdate = true;
						}
						else
						{
							m_Status.m_nInkLevel = OK; 
							bStatusUpdate = true;
						}
					}
				}
				
				if (KeyValue.Lookup(_T ("FPGA"), sValue)) {
					m_Status.m_bFPGA = false;
				}

				if (KeyValue.Lookup(_T ("CONVEYOR_SPEED"), sValue)) {
					m_Status.m_dLineSpeed = FoxjetDatabase::ttof (sValue);
				}

				if ( KeyValue.Lookup(_T ("VoltReady"), sValue) ) 
				{
					sValue.Replace ('"', ' ');
					sValue.TrimLeft();
					sValue.TrimRight();
					if ( sValue.CompareNoCase (_T ("F")) == 0)
					{
						if ( m_Status.m_bVoltageOk == true )
							bStatusUpdate = true;
						m_Status.m_bVoltageOk = false;
					}
					else
					{
						if ( m_Status.m_bVoltageOk == false )
							bStatusUpdate = true;
						m_Status.m_bVoltageOk = true;
					}
				}
				
				if ( KeyValue.Lookup(_T ("Heater1Ready"), sValue) ) {
					sValue.Replace ('"', ' ');
					sValue.TrimLeft();
					sValue.TrimRight();
					if ( sValue.CompareNoCase (_T ("F")) == 0)
					{
						if ( m_Status.m_bAtTemp == true )
							bStatusUpdate = true;
						m_Status.m_bAtTemp = false;
					}
					else 
					{
						if ( m_Status.m_bAtTemp == false )
							bStatusUpdate = true;
						m_Status.m_bAtTemp = true;
					}
				}

				if (KeyValue.Lookup (_T ("SW_VER"), sValue)) 
					SetSwVer (sValue);
				if (KeyValue.Lookup (_T ("GA_VER"), sValue)) 
					SetGaVer (sValue);

				#ifdef _DEBUG
				{
					CString str = GetStatusMessage ();

					if (strLastStatus.CompareNoCase (str) != 0) 
						TRACEF (_T ("Status change: ") + strLastStatus + _T (" --> ") + str + CTime::GetCurrentTime ().Format (_T (" [%c]")));
				}
				#endif //_DEBUG

			}
			break;
		case IPC_CONNECTION_CLOSE:	// Device is Closing the connection.
			Disconnect ();
			break;
		case IPC_SET_LABEL_ID_SELECTED:
			if (!IsDigiBoard ()) {
				CMapStringToString KeyValue;
				CString sData, sValue;
				if ( !KeyValue.IsEmpty () )
					KeyValue.RemoveAll();
				sData.Format(_T ("%s"), a2w (pMsg->Data));
				CKeyParse::Parse(sData, KeyValue);
				CString sTempTask;
				KeyValue.Lookup(_T ("LABELID"), sTempTask);
				sTempTask.Replace ('"', ' ');
				sTempTask.TrimLeft();
				sTempTask.TrimRight();

				if ( m_sTaskName.Compare ( sTempTask ) != 0 )
				{
					m_sTaskName = sTempTask;
					bStatusUpdate = true;
					if ( m_sTaskName.IsEmpty() )
						m_eTaskState = STOPPED;
					else {
						m_eTaskState = RUNNING;
						EnableDynamicDataDownload (true);
					}
				}

				if (IsError (GetErrorState ()) && m_eTaskState == RUNNING) 
					OnSetIgnorePhotocell (1, 0);
			}
			break;
		case IPC_SET_VARDATA_ID:
			{
				CMapStringToString KeyValue;
				CString strID, strData, str (a2w (pMsg->Data));

				TRACEF (str);
				CKeyParse::Parse (str, KeyValue);

				KeyValue.Lookup (_T ("ID"), strID);
				KeyValue.Lookup (_T ("DATA"), strData);

				LONG lID = _tcstol (strID, NULL, 10) - 1;

				if (lID >= 0 && lID < ARRAYSIZE (m_strDynData)) 
					m_strDynData [lID] = strData;
			}
			break;
		case IPC_EDIT_STATUS:
			break;
		case IPC_INVALID:
			break;
		case IPC_SET_STATUS_MODE:
			break;
		default:
		{
			#ifdef _DEBUG
			CString strCmd = GetIpcCmdName (pMsg->Header.Cmd);

			if (!strCmd.GetLength ()) {
				CString strData;

				strData.Format (_T ("CRemoteHead - Recieved Unknown Command\nCmd:\t[%i] %s\nBatch:\t[%lu]\nData:\t[%s]\n"), 
					pMsg->Header.Cmd, 
					strCmd,
					pMsg->Header.BatchID, 
					a2w (pMsg->Data));
				TRACEF (strData);
			}
			#endif
		}
	}
	
	if (bStatusUpdate) {
		WPARAM wParam = m_Config.m_HeadSettings.m_lID;
		LPARAM lParam = 0;
	
		while (!::PostMessage (m_hControlWnd, WM_SOCKET_PACKET_PRINTER_HEAD_STATE_CHANGE, wParam, 0)) ::Sleep (0);
		while (!::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this)) ::Sleep (0);

		bStatusUpdate = false;
	}
}

int CRemoteHead::CompareVersions (const FoxjetCommon::CVersion & verIP, 
								  const FoxjetCommon::CVersion & verCurrent)
{
	int nVerIP		= (verIP.GetMajor ()		* 100000) + (verIP.GetMinor ()		* 100) + verIP.GetInternal ();
	int nCurrent	= (verCurrent.GetMajor ()	* 100000) + (verCurrent.GetMinor ()	* 100) + verCurrent.GetInternal ();

	return nVerIP - nCurrent;
}

void CRemoteHead::SetSwVer (const CString & str)
{
	using namespace FoxjetCommon;

	const CVersion verApp = GlobalVars::CurrentVersion;
	const CVersion verIP (str);
	bool bObsolete = false;

	m_strSwVer = str;

	if (IsDigiBoard ()) {
		if (verIP.GetMajor () >= 1 && verIP.GetMinor () >= 3)
			Send (IPC_GET_GA_VER_INFO, GetNextSequence(), NULL, 0, NULL); 

		int nDiff = CompareVersions (verIP, verApp);

	//	if (nDiff < -99) {
	//	if (nDiff < -999) {
		if (nDiff < -9999) {
			CString strDebug;

			strDebug.Format (_T ("Obsolete firmware: %d.%02d [need: %d.%02d]"),
				verIP.GetMajor (), verIP.GetMinor (),
				verApp.GetMajor (), verApp.GetMinor ());
			TRACEF (strDebug);
			bObsolete = true;
		}
	}
	else {
		bObsolete = !(verIP.GetMajor () >= 3 && verIP.GetInternal () >= 22);
	}

	if (bObsolete)
		while (!::PostMessage (m_hControlWnd, WM_HEAD_OBSOLETEFIRMWARE, 0, (LPARAM)this)) 
			::Sleep (0);
}

void CRemoteHead::SetGaVer (const CString & str)
{
	using namespace FoxjetCommon;

	m_strGaVer = str;

	if (m_strGaVer.GetLength ()) {
		CVersion verGA (m_strGaVer);

		// PRINT_RESOLUTION special case
		// CHeadCfg::ToString, case IPC_SET_SYSTEM_INFO assumes ga ver >= 1.3 if not set
		if (!IsDigiBoard () && (const VERSIONSTRUCT &)verGA < (const VERSIONSTRUCT &)CVersion (1, 30))
			SendConfiguration ();
	}
}

void CRemoteHead::OnDataRecieved(UINT wParam, LONG lParam)
{
}

void CRemoteHead::DownloadMessage (const FoxjetDatabase::TASKSTRUCT & task,
								  const FoxjetDatabase::MESSAGESTRUCT & msg, 
								  CSize & sizeLabel, ULONG lBatchID,
								  float fBoxHeight, float fBoxWidth,
								  bool bTaskStart)
{
	using namespace FoxjetCommon;
	using namespace FoxjetIpElements;

	try
	{
		CElementList * pList = &m_defCache.m_list; //new CElementList;
		CString strMsgData, strElementData, strLabelData;

		m_pElementList = pList;
		pList->DeleteAllElements ();
		pList->FromString (msg.m_strData, GlobalVars::CurrentVersion);

		LPFJLABEL pLabel = pList->GetLabel();
		TCHAR cRound = pLabel->bExpRoundUp ? 'T' : 'F';

		if (pLabel->bExpRoundDown)
			cRound = 'D';

		strLabelData.Format (_T ("{Label,\"%s\",%02.1f,%02.1f,%d,%d,%c,\"%s\"}"), 
			msg.m_strName, 
			fBoxHeight, 
			fBoxWidth, 
			pLabel->lExpValue, 
			pLabel->lExpUnits, 
			cRound,
			msg.m_strName);
		//TRACEF (strLabelData);

		BuildMsgAndElemString (pList, msg.m_strName, strMsgData, strElementData);

		TRACEF (strLabelData);
		//TRACEF (strMsgData); 
		//TRACEF (strElementData);

		UINT nElemCnt = pList->GetSize();
		CString strFull = strLabelData + strMsgData + strElementData;
		TRACEF (GetFriendlyName () + ": " + msg.m_strName);
		TRACEF (strFull);

		if (!Send (IPC_PUT_PRINTER_ELEMENT, lBatchID, strFull, NULL))
		{
			if (bTaskStart) {
				SetLastError (TRANSFER_FAILED);
				AfxThrowUserException();
			}
		}

		if (bTaskStart) {
			/*
			if (m_pElementList != NULL)
				delete m_pElementList;

			m_pElementList = pList;
			*/
			m_LabelSize = sizeLabel;
			m_Task = task;
			m_Message = msg;
		}
//		else
//			delete pList;
	}
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	catch (CUserException * e)		{ HANDLEEXCEPTION (e); }
	
}

void CRemoteHead::UpdateLabels (CArray <FoxjetDatabase::MESSAGESTRUCT, FoxjetDatabase::MESSAGESTRUCT &> & v,
								bool bTaskStart)
{
	using namespace FoxjetDatabase;

	DBCONNECTION (conn);

	if (!conn.OpenExclusive (theApp.GetActiveFrame ()))
		return;

	if (m_state.m_bShortTimeout)
		return;

	CSuspendDynamicDataDownload (this);

	SetLastError (0);

	if (v.GetSize()) {
		CSize LabelSize;

		ULONG lBatchID = GetNextSequence();
		CString sElementData;
		CString sMsgData;
		CString sLabelData;
		TASKSTRUCT Task;
		HEADSTRUCT HS	= m_Config.GetHeadSettings();
		CString sMsg;
		sMsg.Format (LoadString (IDS_MESSAGEUPDATE), GetFriendlyName(), v.GetSize());
		
		while (!::PostMessage ( m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_MESSAGES, (LPARAM) this )) Sleep (0);
		if ( StartEditSession() )
		{
			if (theApp.GetSingleMsgMode ())
				OnDeleteLabels (0, 0);

			for (long i = 0; i < v.GetSize(); i++ )
			{
				MESSAGESTRUCT Message = v [i];

				if ( FoxjetDatabase::GetTaskRecord ( theApp.m_Database, Message.m_lTaskID, Task ))
				{
					FoxjetDatabase::BOXSTRUCT BS;
					if ( FoxjetDatabase::GetBoxRecord ( theApp.m_Database, Task.m_lBoxID, BS) ) 
					{
						CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;
						float nBoxHeight, nBoxWidth;

						VERIFY (GetPanelRecords (theApp.m_Database, Task.m_lLineID, vPanels));
						LabelSize = Task.GetPanelSize (vPanels, HS.m_lPanelID, BS);
						nBoxHeight	= ((float)LabelSize.cy / (float)1000 );
						nBoxWidth	= ((float)LabelSize.cx / (float)1000 );

						m_strDisplay.Format (LoadString (IDS_UPDATING_MESSAGE_N_OF_X),
							GetFriendlyName (),
							Task.m_strName, 
							i + 1, 
							v.GetSize ());

						while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_MESSAGES, (LPARAM)this)) 
							::Sleep (0);

						QueryDiskUsage ();
						DownloadMessage (Task, Message, LabelSize, lBatchID, nBoxHeight, nBoxWidth, bTaskStart);
					}
					FoxjetDatabase::SetMessageRecordModified (theApp.m_Database, Message.m_lID, false );
				}
				else
				{
					CString sMsg;
					sMsg.Format ( IDS_TASK_LOAD_FAILED, GetFriendlyName());
					MsgBox ( sMsg );
				}
			}
			
			if (!SaveEditSession ()) 
				SetLastError (SAVE_EDIT_SESSION_FAILED);
		}
		else	// Start Edit Session Failed.
		{
//			SetLastError ( START_EDIT_SESSION_FAILED );
			if (bTaskStart)
			{
				while (!::PostMessage ( m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM) this ))Sleep (0);
				AfxThrowUserException();
			}
		}
		while ( !::PostMessage ( m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM) this )) Sleep (0);
	}
	
	QueryDiskUsage ();
	m_strDisplay.Empty ();
}

void CRemoteHead::OnUpdateLabels (UINT wParam, LONG lParam)
{
	SetLastError ( 0 );

	CArray <FoxjetDatabase::MESSAGESTRUCT, FoxjetDatabase::MESSAGESTRUCT &> UpdateList;

	if ( wParam != UPDATE_TASK_START )
	{
		FoxjetDatabase::HEADSTRUCT		HS	= m_Config.GetHeadSettings();
		
		if (!theApp.GetSingleMsgMode ()) {
			CArray <FoxjetDatabase::MESSAGESTRUCT, FoxjetDatabase::MESSAGESTRUCT &> v;
	
			FoxjetDatabase::GetMessageRecords ( theApp.m_Database, HS.m_lID, v);

			for (long i = 0; i < v.GetSize(); i++ )
			{
				FoxjetDatabase::MESSAGESTRUCT &MS = v.ElementAt(i);

				if ( wParam == UPDATE_MODIFIED_ONLY )
				{
					if ( MS.m_bIsUpdated )
						UpdateList.Add ( MS );
				}
				else
					UpdateList.Add ( MS );
			}
		}
		else {
			MESSAGESTRUCT msg;

			if (GetMessageRecord (theApp.m_Database, m_Message.m_lID, msg))
				UpdateList.Add (msg);
		}
	}
	else	// We were called from OnStartTask and lParam should be a vaild FoxjetDatabase::MESSAGESTRUCT *.
	{
		if ( lParam != NULL )
			UpdateList.Add ( *(FoxjetDatabase::MESSAGESTRUCT*)lParam );
		else
		{
			SetLastError ( INVALID_PARAM );
			AfxThrowUserException();
		}
	}

	UpdateLabels (UpdateList, wParam == UPDATE_TASK_START ? true : false);
}

void CRemoteHead::OnStartTask(UINT wParam, LONG lParam)
{
	using namespace FoxjetCommon;

	CSuspendDynamicDataDownload (this);

	ULONG lTaskID = lParam;
	ULONG lBatchID = GetNextSequence();
	bool bTestPattern = lTaskID == -1;

	if (IsWaxHead ())
		SetStandbyMode (false);

	m_Status.m_lCount = 0L;

	if ( !m_bDisabled )
	{
		if (!bTestPattern && !m_bModifiedAfterLoad) {
			try
			{
				FoxjetDatabase::MESSAGESTRUCT Message;
				FoxjetDatabase::TASKSTRUCT Task;
				FoxjetDatabase::HEADSTRUCT HS	= m_Config.GetHeadSettings();
				FoxjetIpElements::LoadBarcodeParams (theApp.m_Database, GlobalVars::CurrentVersion);

				bool bFound = false;
				
				if (lTaskID != -1)
					bFound = FoxjetDatabase::GetTaskRecord (theApp.m_Database, lTaskID, Task);
				else {
					bFound = true;
				}

				if (bFound) {
					bool bDownload = false;

					m_Task = Task;
					FoxjetDatabase::GetBoxRecord ( theApp.m_Database, m_Task.m_lBoxID, m_Box); 
					CPacket reply;

					Send (IPC_GET_LABEL_IDS, GetNextSequence(), NULL, 0, &reply);

					if (CommandSuccessful (IPC_GET_LABEL_IDS, &reply)) {
						CString str = a2w (reply.Data);

						bDownload = str.Find (Task.m_strName) == -1;
					}

					for ( int i = 0; i < Task.m_vMsgs.GetSize(); i++ )
					{
						Message = Task.m_vMsgs.GetAt ( i );

						if ( Message.m_lHeadID == HS.m_lID ) {
							if (bDownload || Message.m_bIsUpdated) 
								OnUpdateLabels (UPDATE_TASK_START, (ULONG) &Message);

							break;
						}
					}
				}
				else
				{
					CString sMsg;
					sMsg.Format ( IDS_TASK_LOAD_FAILED, GetFriendlyName());
					MsgBox ( sMsg );
				}
			}
			catch ( CUserException *e )
			{
				e->Delete();
				CHead::OnStartTask ( wParam, lParam);
				return;
			}
		}
		else { 
			CSize sizeLabel;

			if (bTestPattern) {
				TASKSTRUCT task;
				MESSAGESTRUCT msg;
				int nCount = m_Config.m_HeadSettings.m_nChannels / 32;

				msg.m_strName = task.m_strName = m_sMessageName = m_sTaskName = GlobalVars::strTestPattern;

				msg.m_strData.Format (_T ("{Label,\"%s\",20.0,20.0,0,3,F,\"%s\"}\""),
					GlobalVars::strTestPattern,
					GlobalVars::strTestPattern);

				for (int i = 0; i < nCount; i++) {
					CString strBmp;
					
					strBmp.Format (_T ("{Bitmap,\"%d\",0.000000,%d,D,3,1,F,F,F,\"%s\"}"),
						i + 1, 
						i * 32,
						GlobalVars::strTestPattern);
					TRACEF (strBmp);
					msg.m_strData += strBmp;
				}

				if (StartEditSession ()) {
					DownloadMessage (task, msg, sizeLabel, lBatchID, 20.0, 20.0, true);
					SaveEditSession ();
				}
			}
			else {
				ASSERT (m_pElementList);

				m_Message.m_strData = m_pElementList->ToString (FoxjetCommon::verApp);
				TRACEF (m_Message.m_strData);
				
				// must re-download next time this task is started
				SetMessageRecordModified (theApp.m_Database, m_Message.m_lID, true);

				if (StartEditSession ()) {
					DownloadMessage (m_Task, m_Message, sizeLabel, lBatchID, 20.0, 20.0, true);
					SaveEditSession ();
				}
			}
		}

		while (!::PostMessage ( m_hControlWnd, WM_HEAD_PROCESSING, STARTING_MESSAGE, (LPARAM) this )) 
			Sleep(0);

		UINT nElemCnt = m_pElementList ? m_pElementList->GetSize() : 0;
		CString sLabelName;
		
		sLabelName.Format (_T ("LABELID=\"%s\";"), m_Message.m_strName);
		TRACEF (sLabelName);
		Send ( IPC_SET_LABEL_ID_SELECTED, lBatchID, sLabelName, NULL );

		if (m_pElementList) 
			for (int i = 0; i < m_pElementList->GetSize (); i++) 
				if (CIpBaseElement * p = DYNAMIC_DOWNCAST (CIpBaseElement, &m_pElementList->GetObject (i))) 
					if (p->GetLabel () == NULL)
						p->SetLabel (m_pElementList->GetLabel ());

		BuildImage (INITIAL);
		CHead::OnStartTask ( wParam, lParam);

		m_eTaskState = RUNNING;
		m_sMessageName = m_Message.m_strName;
		Send ( IPC_GET_LABEL_ID_SELECTED, lBatchID, NULL, 0, NULL );

		Send (IPC_GET_SELECTED_COUNT, GetNextSequence (), CString (), NULL);

		while (!::PostMessage ( m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM)this)) ::Sleep (0);
		while (!::PostMessage ( m_hControlWnd, WM_HEAD_STATUS_UPDATE, 0, (LPARAM)this)) ::Sleep (0);
	}
	else
		CHead::OnStartTask ( wParam, lParam);
}

void CRemoteHead::OnStopTask(UINT wParam, LONG lParam)
{
	CString sMode;
	sMode.Format (_T ("PRINT_MODE=%s;APS_MODE=DISABLE;"), _T("STOP"));

	if (IsWaxHead ())
		SetStandbyMode (false);

	//TRACEF (sMode);

// set "PRINT_MODE=RESUME/IDLE/STOP;" Note: Set mode STOP will cancel the label selection
	Send ( IPC_SET_PRINTER_MODE, GetNextSequence(), sMode, NULL );
	Send ( IPC_GET_LABEL_ID_SELECTED, GetNextSequence(), NULL, 0, NULL );
	m_sTaskName = _T("");

	if ( m_bDisabled )
		m_eTaskState = DISABLED;
	else
		m_eTaskState = STOPPED;

	for (int i = 0; i < ARRAYSIZE (m_strDynData); i++)
		m_strDynData [i] = _T ("");

	CHead::OnStopTask ( wParam, lParam);
}

void CRemoteHead::OnIdleTask(UINT wParam, LONG lParam)
{
	CString sMode = _T ("PRINT_MODE=IDLE;APS_MODE=DISABLE;");

/*	can't call this here because timing out into standby mode (which idles) can't reliably set
	standby mode on.  i assume we'll never want to come out of standby to idle anyway.

	if (IsWaxHead ())
		SetStandbyMode (false);
*/

	Send (IPC_SET_PRINTER_MODE, GetNextSequence (), sMode, NULL);
	Send (IPC_GET_STATUS, GetNextSequence ()); 

	if ( m_bDisabled )
		m_eTaskState = DISABLED;
	else
		m_eTaskState = IDLE;

}

void CRemoteHead::OnResumeTask(UINT wParam, LONG lParam)
{
	if ( !m_bDisabled )
	{
		CString sMode = _T ("PRINT_MODE=RESUME;APS_MODE=DISABLE;");

		m_eTaskState = RUNNING;

		if (IsWaxHead ())
			SetStandbyMode (false);

		// set "PRINT_MODE=RESUME/IDLE/STOP;" Note: Set mode STOP will cancel the label selection
		Send ( IPC_SET_PRINTER_MODE, GetNextSequence(), sMode, NULL );
	}
	else
		m_eTaskState = DISABLED;

	CHead::OnResumeTask ( wParam, lParam);
	Send (IPC_GET_STATUS, GetNextSequence()); 
}

void CRemoteHead::OnUpdateFonts(UINT wParam, LONG lParam)
{
	CSuspendDynamicDataDownload (this);

	if (m_state.m_bShortTimeout)
		return;

	GetLocalFontList();
	CCmdPacket *pCmd = NULL;
	SetLastError ( 0 );
	CPacket reply;

	if (wParam == TRANSFER_FONTS)
		while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FONTS, (LPARAM)this)) 
			::Sleep (0);

	if (Send (IPC_GET_FONT_IDS, GetNextSequence(), NULL, 0, &reply)) {
		if (CommandSuccessful (IPC_GET_FONT_IDS, &reply)) {
			CMapStringToString KeyValue;
			CString sData, sValue, sTemp;
			sData.Format(_T ("%s"), a2w (reply.Data));
			CKeyParse::Parse(sData, KeyValue);

			if ( KeyValue.Lookup(_T ("FONTIDS"), sValue) ) 
			{
				TCHAR szBuffer[1024];
				_tcscpy (szBuffer, sValue);
				TCHAR * p = _tcstok (szBuffer, _T (","));
				while ( p )
				{
					sTemp.Format (_T ("%s"), p);
					sTemp.Remove('"');
					sTemp += _T (".fjf");
					sTemp.MakeUpper();
					m_PrinterFontList.AddTail ( sTemp );
					p = _tcstok (NULL, _T (","));
				}
			
				POSITION pos;
				while ( !m_PrinterFontList.IsEmpty() )
					if ( (pos = m_FontTransferList.Find ( m_PrinterFontList.RemoveHead() )) )
						m_FontTransferList.RemoveAt ( pos );

				if ( wParam == TRANSFER_FONTS )
				{
					const int nTotal = m_FontTransferList.GetCount ();
					int nFont = 1;

					if ( !m_FontTransferList.IsEmpty() )
					{
						if ( StartEditSession() )
						{
							while ( !m_FontTransferList.IsEmpty() )
							{
								CString sFont = m_FontTransferList.RemoveHead();

								m_strDisplay.Format (LoadString (IDS_UPDATING_FONT_N_OF_X),
									GetFriendlyName (),
									sFont, 
									nFont++, 
									nTotal);

								while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FONTS, (LPARAM)this)) 
									::Sleep (0);

								if (!SendFont ( sFont, GetNextSequence ())) {
									CancelEditSession();
									SetLastError ( FONT_TRANSFER_FAILED );
									while (!::PostMessage ( m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM) this )) Sleep (0);
									return;
								}
							}

							if (!SaveEditSession ()) 
								SetLastError (SAVE_EDIT_SESSION_FAILED);
						}
					}
				}
			}
		}
	}

	if (wParam == TRANSFER_FONTS )
		while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM)this))
			::Sleep(0);

	m_strDisplay.Empty ();
}

int CRemoteHead::Run() 
{
	const DWORD dwTimeout = 20;
	MSG Msg;
	clock_t clock_tLast = clock (), clock_tNow = clock ();
	const double dSampleRate = 0.5;

	SetThreadPriority (THREAD_PRIORITY_NORMAL);
	//TRACEF (_T ("SetThreadPriority: ") + FoxjetDatabase::ToString (GetThreadPriority ()));

	while ( 1 )
	{
		HANDLE hEvent [1] = { m_hExit };
		DWORD dwWait = ::MsgWaitForMultipleObjects (ARRAYSIZE (hEvent), hEvent, false, dwTimeout, QS_ALLINPUT);

		switch (dwWait) {
			case WAIT_OBJECT_0:
				return ExitInstance();
		}

		::Sleep (1);

		{
			LOCK (m_csReadSocket);		
			ReadSocket ();
		}

		clock_tNow = clock ();
		double dDiff = (double)(clock_tNow - clock_tLast) / CLOCKS_PER_SEC;
	
		if (dDiff > dSampleRate) 
			CheckState ();

		{
			int nTimeoutStep = 0;
			const double dIncrement = 6.0;

			if (!m_state.m_bClosed) {
				CTimeSpan span = CTime::GetCurrentTime () - m_state.m_tmLast;
				int nTimeout = m_state.m_bShortTimeout ? 5 : (int)ceil (GlobalVars::HeadResponseTimeout / 1000.0);

				nTimeoutStep = min ((int)dIncrement, (int)floor ((double)span.GetTotalSeconds () / dIncrement));

				if (!m_state.m_bShortTimeout) {
					if ((span.GetTotalSeconds () * 2) >= nTimeout) {
						if (!m_state.m_bGetStatus) {
							m_state.m_bGetStatus = true;
							TRACEF (GetFriendlyName () + _T (": IPC_GET_STATUS"));
							Send (IPC_GET_STATUS, GetNextSequence()); 
						}
					}
				}
				
				if (span.GetTotalSeconds () >= nTimeout) 
 					Disconnect ();
			}

			if (m_nTimeoutStep != nTimeoutStep) {
				m_nTimeoutStep = nTimeoutStep;

				while ( !::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this)) 
					::Sleep(0);
			}
		}

		if (IsError (GetErrorState ()) && m_eTaskState == RUNNING) {
			ULONG lTaskID = GetTaskID (theApp.m_Database, m_sTaskName, m_Config.m_Line.m_strName, GetPrinterID (theApp.m_Database));

			while (!::PostMessage (m_hControlWnd, WM_SET_HEAD_ERROR, m_Config.m_Line.m_lID, (LPARAM)this))
				::Sleep (0);
		}

		while (PeekMessage (&Msg, NULL, NULL, NULL, PM_NOREMOVE)) {
			if (!PumpMessage ()) {
				TRACEF ("!PumpMessage()");
				return ExitInstance();
			}
		}
	}

	return ExitInstance();
}

void CRemoteHead::OnInitialStartup(UINT wParam, LONG lParam)
{
	m_Config.LoadSettings (theApp.m_Database, (ULONG)lParam);
	m_sDatabaseName = GetUID ();

	if (HANDLE h = (HANDLE)wParam)
		::SetEvent (h); 
	
	LoadStandbySettings ();
	Connect ();
}

void CRemoteHead::OnSocketRetryConnect (UINT wParam, LONG lParam)
{
	if (GetThreadPriority () > THREAD_PRIORITY_NORMAL) { 
		SetThreadPriority (THREAD_PRIORITY_NORMAL);
		TRACEF (_T ("SetThreadPriority: ") + FoxjetDatabase::ToString (GetThreadPriority ()));
	}

	if (m_pConnect) {
		delete m_pConnect;
		m_pConnect = NULL;
	}

	Connect ();
}

void CRemoteHead::Connect ()
{
	//TRACEF (GetUID () + _T (": attempting connection [") + GetFriendlyName () + _T ("]"));

	ASSERT (m_pConnect == NULL);

	m_pConnect = new CNotifySocket (m_nThreadID, m_hThread, GetUID ());
	m_pConnect->Create ();
	m_pConnect->Connect (GetUID (), _FJ_SOCKET_NUMBER);
}

void CRemoteHead::GetVersionInfo ()
{
	CPacket reply;

	Send (IPC_GET_SW_VER_INFO, GetNextSequence (), _T (""), &reply); // version assigned in CRemoteHead::ProcessRxData
	Send (IPC_GET_GA_VER_INFO, GetNextSequence (), _T (""), &reply); // version assigned in CRemoteHead::ProcessRxData
}

ULONG CRemoteHead::GetNextSequence()
{
	ULONG n;
	LOCK (m_SequenceSentinel);
	n = m_nSequence;
	m_nSequence++;
	return n;
}

void CRemoteHead::GetRemoteBitmaps (CStringArray & vBitmaps)
{
	CPacket reply;
	
	Send (IPC_GET_BMP_IDS, GetNextSequence (), _T (""), &reply);
	CString str = Extract (a2w (reply.Data), _T ("BMPIDS="), _T (";"));

	vBitmaps.RemoveAll ();
	Tokenize (str, vBitmaps);
}

void CRemoteHead::BuildMsgAndElemString(FoxjetCommon::CElementList *pElementList, CString strMsgName, CString &strMsgIDs, CString &strElementData)
{
	using namespace FoxjetCommon;

	CString strType;
	CString strID;
	CString strElement;
	UINT nElemCnt = pElementList->GetSize();
	CString strMsgData;
	int nBitmap = 1;
	CStringArray vBitmaps;

	strID.Format (_T (",%i,"), nElemCnt);

	strMsgIDs = _T ("{Message,\"") + strMsgName + _T("\"");
	strMsgIDs += strID;

	GetRemoteBitmaps (vBitmaps);

	if (!nElemCnt) {
		using FoxjetIpElements::CTextElement;

		const CBaseElement & def = ListGlobals::defElements.GetElement (TEXT);
		
		if (CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, CopyElement (&def, &m_Config.m_HeadSettings, true))) {
			p->SetID (1);
			p->SetDefaultData (" ");
			p->SetFontName (((CTextElement *)&def)->GetFontName ());

			strType = _T( "Text-" );
			strElementData += p->ToString (FoxjetCommon::verApp);
			strMsgIDs.Format (_T ("{Message,\"%s\",1,\"%s-%s1\"}"), strMsgName, strMsgName, strType);

			strElement = p->ToString (FoxjetCommon::verApp);
			//TRACEF (strElement);
			
			int nFirstComma = strElement.Find (',', 0);
			CString strTemp = strMsgName + _T("-") + strType;

			strElement.Insert (nFirstComma + 2, strTemp);
			strElementData = strElement;

			//TRACEF (strMsgIDs);
			//TRACEF (strElementData);

			delete p;
		
			return;
		}
	}

	FoxjetCommon::CBaseElement *pBaseElem;
	for ( unsigned int i =0; i < nElemCnt; i++ ) 
	{
		pBaseElem = (FoxjetCommon::CBaseElement *) &pElementList->GetObject ( i );
		strID.Format (_T ("%i"), pBaseElem->GetID());
		switch ( pBaseElem->GetClassID() ) 
		{
			case TEXT:
				strType = _T( "Text-" );
				break;
			case DYNAMIC_TEXT:
				strType = _T( "Dyntext-" );
				break;
			case COUNT:
				strType = _T( "Counter-" );
				break;
			case BARCODE:
				strType = _T( "Barcode-" );
				break;
			case DYNAMIC_BARCODE:
				strType = _T( "Dynbarcode-" );
				break;
			case BMP:
				{
					CBitmapElement *pBitmapElem = (CBitmapElement * ) pBaseElem;
					strType = _T( "Bitmap-" );
					LPFJBITMAP p = pBitmapElem->GetMember ();
					CString strName = p->strName;
					CVersion ver (m_strSwVer);
					bool bSkip = strName.CompareNoCase (GlobalVars::strTestPattern) == 0;

					if ((const VERSIONSTRUCT &)ver < (const VERSIONSTRUCT &)CVersion (1, 20, 0, 0, 18))
						bSkip = false;

					if (IsDigiBoard ()) {
						bSkip = (Find (vBitmaps, pBitmapElem->GetDefaultData ()) != -1) ? true : false;

						if (bSkip) 
							TRACEF (_T ("skipping: ") + pBitmapElem->GetFilePath ());
					}

					if (!bSkip) {
						CString strFile = pBitmapElem->GetFilePath();
						TransferBitmap ( strFile );
						GetRemoteBitmaps (vBitmaps);
					}
				}
				break;
			case DATETIME:
				strType = _T( "Datetime-" );
				break;
			case DATAMATRIX:
				strType = _T( "DataMatrix-" );
				break;
			default:
				ASSERT (0);
				break;
		}
		strElement.Empty();
		strElement.Format(_T ("%s"), pBaseElem->ToString ( GlobalVars::CurrentVersion ));
		//TRACEF (strElement);
		int nFirstComma = strElement.Find (',', 0);
		nFirstComma++;
		CString sTemp = strMsgName + _T("-") + strType;
		strElement.Insert (nFirstComma + 1, sTemp);
		strElementData += strElement;
		strMsgIDs += _T("\"") + sTemp + strID + _T("\"") + _T(",");
	}
	// Drop last appended comma.
	strMsgIDs = strMsgIDs.Left (strMsgIDs.GetLength() - 1);
	// Add final closing brace.
	strMsgIDs += _T( "}" );
}

static void RemoveEmptyStrings (CStringArray & v)
{
	for (int i = v.GetSize () - 1; i >= 0; i--)
		if (v [i].IsEmpty ())
			v.RemoveAt (i);
}

static CString GetKey (const CString & strPair, CString & strValue)
{
	int nIndex = strPair.Find ('=');

	if (nIndex == -1) 
		return _T ("");
	else {
		strValue = strPair.Mid (nIndex + 1);

		return strPair.Left (nIndex);
	}
}

bool CRemoteHead::CompareConfiguration (const CString & strController, CString & strBarcode, CString & strDataMatrix, CString & strStrobe)
{
	CString strDB = GetLocalConfiguration ();
	/*
		_T ("HeadType=3;UNITS=ENGLISH_INCHES;TIME=24;ENCODER=F;ENCODER_COUNT=300.0;ENCODER_WHEEL=300.0;ENCODER_DIVISOR=2;CONVEYOR_SPEED=30.0;")
		_T ("PRINT_RESOLUTION=150.0;INK_SIZE=500.0;INK_COST=230.0;PRINTER_ID=\"FJ\";LONGNAME=\"FoxJet\";SHORTNAME=\"FJ\";SERIAL=;DIRECTION=RIGHT_TO_LEFT;")
		_T ("SLANTANGLE=90.0;SLANTVALUE=0;HEIGHT=0.0;DISTANCE=1.0;AMS_INTERVAL=0.0;AMS_INACTIVE=10;STANDBY_INTERVAL=0.0;STANDBY_AUTO=F;PHOTOCELL_BACK=F;")
		_T ("MSGSTORE_SIZE=0;TABLE_INDEX=3;DATE_MANU=2000/01/19 00:00:00;DATE_SERVICE=2000/01/19 00:00:00;SCANNER_USE_ONCE=F;SERIAL_PORT=VARDATA;")
		_T ("INFO_STATUS=F;INFO_SOCKET=F;INFO_RAM=F;INFO_ROM=F;INFO_NAME=F;INFO_TIME=F;INFO_AD=F;PHOTOCELL_INTERNAL=F;APS_MODE=F;SOP=F;BAUD_RATE=9600;")
		_T ("DISTANCE2=1.0;BIDIRECTIONAL=F;PRINT_MODE=RESUME;APS_MODE=DISABLE;");
	*/
	CStringArray vController, vDB;
	const CString strIgnore [] = 
	{
		_T ("INK_SIZE"),
		_T ("INK_COST"),
		_T ("PRINTER_ID"),
		_T ("LONGNAME"),
		_T ("SHORTNAME"),
		_T ("AMS_INTERVAL"),
		_T ("AMS_INACTIVE"),
		_T ("STANDBY_AUTO"),
		_T ("MSGSTORE_SIZE"),
		_T ("TABLE_INDEX"),
		_T ("DATE_MANU"),
		_T ("DATE_SERVICE"),
		_T ("INFO_STATUS"),
		_T ("INFO_SOCKET"),
		_T ("INFO_RAM"),
		_T ("INFO_ROM"),
		_T ("INFO_NAME"),
		_T ("INFO_TIME"),
		_T ("INFO_AD"),
		_T ("APS_MODE"),
		_T ("SOP"),
		_T ("DISTANCE2"),
		_T ("BIDIRECTIONAL"),
		_T ("PRINT_MODE"),
		_T ("APS_MODE"),
		_T ("OP_NAME"),
		_T ("OP_PHY_TABLE"),
		_T ("OP_PKG_TABLE"),
		_T ("OP_VOLT_TABLE"),
		_T ("OP_VOLTAGE"),
		_T ("OP_FORCEHEADPRESENT"),
		_T ("OP_CURVE1"),
		_T ("OP_CURVE2"),
		_T ("OP_CURVE3"),
		_T ("OP_SUBPULSE"),
		_T ("OP_SUBPULSE_LENGTH"),
		_T ("OP_SUBPULSE_FREQ"),
		_T ("OP_SUBPULSE_ON"),
		_T ("OP_SUBPULSE_OFF"),
		_T ("OP_SUBPULSE_DUR"),
		_T ("OP_SUBPULSE_INT"),
		_T ("OP_CHIP_INVERT"),
		_T ("OP_TEMP_TABLE"),
		_T ("OP_TEMP_RANGE"),
		_T ("OP_TEMP_1"),
		_T ("OP_TEMP_2"),
		_T ("OP_TEMP_3"),
		_T ("OP_TEMP_CAL_1"),
		_T ("OP_TEMP_CAL_2"),
		_T ("OP_TEMP_CAL_3"),
		_T ("SERIAL"),
		_T ("SYSTEM_TIME"), // gets set on connect
	};
	bool bResult = true;

	strDB.Replace (CString (CRemoteHead::m_cBreak), _T (""));
	//TRACEF (strController); TRACEF ("\n");
	//TRACEF (strDB);			TRACEF ("\n");

	Tokenize (strDB, vDB, ';');
	Tokenize (strController, vController, ';');

	RemoveEmptyStrings (vDB);
	RemoveEmptyStrings (vController);

	{
		for (int j = 0; j < ARRAYSIZE (strIgnore); j++) {
			for (int i = vController.GetSize () - 1; i >= 0; i--) {
				CString strValue;
				CString strKey = GetKey (vController [i], strValue);

				if (strKey.Compare (strIgnore [j]) == 0) {
					vController.RemoveAt (i);
					break;
				}
			}
		}
	}

	while (vDB.GetSize ()) {
		const CString strPair = vDB [0];
		CString strValue;
		const CString strKey = GetKey (strPair, strValue);
		bool bFound = false;

		for (int i = vController.GetSize () - 1; i >= 0; i--) {
			const CString strPairCmp = vController [i];
			CString strValueCmp;
			const CString strKeyCmp = GetKey (strPairCmp, strValueCmp);

			if (strPair.Compare (strPairCmp) == 0) {
				bFound = true;
				vController.RemoveAt (i);
				break;
			}
			else if (strKey.Compare (strKeyCmp) == 0) {
				if (strValue.Compare (strValueCmp) != 0) {
					TRACEF (GetUID () + _T (": [db] ") + strPair + _T (" [not matched] ") + strPairCmp + _T (" [controller]"));
					bResult = false;
					//return bResult;
				}

				bFound = true;
				vController.RemoveAt (i);
				break;
			}
		}

		/*
		if (!bFound) 
			TRACEF (_T ("not found: ") + strPair);
		*/

		vDB.RemoveAt (0);
	}

	/*
	{
		TRACEF ("database: ");
		for (int i = 0; i < vDB.GetSize (); i++) 
			TRACEF (vDB [i]);

		TRACEF ("");
		TRACEF ("Controller: ");
		for (i = 0; i < vController.GetSize (); i++) 
			TRACEF (vController [i]);
	}
	*/

	CString strBarcodeDB	= FoxjetIpElements::CBarcodeElement::GetBarcodeSettings (theApp.m_Database, m_Config.m_Line.m_lID, m_Config.m_HeadSettings.m_nHeadType);
	CString strDataMatrixDB = FoxjetIpElements::CDataMatrixElement::GetBarcodeSettings (theApp.m_Database, m_Config.m_Line.m_lID, m_Config.m_HeadSettings.m_nHeadType);
	CString strStrobeDB		= _T ("STROBE_MODE_READY_G") + Extract (strDB, _T ("STROBE_MODE_READY_G"));

	if (strBarcode.Compare (strBarcodeDB) != 0) {
		TRACEF ("barcode settings don't match");
		bResult = false;
	}

	if (strDataMatrix.Compare (strDataMatrixDB) != 0) {
		TRACEF (strDataMatrix);
		TRACEF (strDataMatrixDB);
		TRACEF ("datamatrix settings don't match");
		bResult = false;
	}

	if (strStrobe.Compare (strStrobeDB) != 0) {
		TRACEF (strStrobe);
		TRACEF (strStrobeDB);
		TRACEF ("strobe settings don't match");
		bResult = false;
	}

	return bResult;
}

CString CRemoteHead::GetConfiguration(CString & strBarcode, CString & strDataMatrix, CString & strStrobe)
{
	ULONG lCmd [] = 
	{
		IPC_GET_PH_TYPE, 
		IPC_GET_SYSTEM_INFO,
		IPC_GET_PRINTER_INFO,
		IPC_GET_PRINTER_MODE,
		IPC_GET_SYSTEM_TIME_INFO,
		IPC_GET_SYSTEM_DATE_STRINGS,
		IPC_GET_SYSTEM_BARCODES,
		IPC_GET_SYSTEM_DATAMATRIX,
		//IPC_GET_PRINTER_OP_INFO, //DOUBLE_PULSE=F
	};
	CString strResult;


	for (int i = 0; i < ARRAYSIZE (lCmd); i++) {
		CPacket reply;
		
		//TRACEF (GetIpcCmdName (lCmd [i]));
		Send (lCmd [i], GetNextSequence (), _T (""), &reply);
		CString str = a2w (reply.Data);
		//TRACEF (GetIpcCmdName (lCmd [i]) + _T (" --> ") + str);

		switch (lCmd [i]) {
			case IPC_GET_SYSTEM_BARCODES:
				strBarcode = str;
				break;
			case IPC_GET_SYSTEM_DATAMATRIX:
				strDataMatrix = str;
				break;
			case IPC_GET_PRINTER_MODE:
				strStrobe = _T ("STROBE_MODE_READY_G") + Extract (str, _T ("STROBE_MODE_READY_G"), _T ("STROBE_MODE_DRIVER_G"));
				str.Replace (strStrobe, _T (""));
				strResult += str;
			default:		
				strResult += str;
				break;
		}
	}

	return strResult;
}

CString CRemoteHead::GetLocalConfiguration()
{
	CString str = _T ("HeadType=");
	ULONG lCmd [] = 
	{
		//IPC_SET_PH_TYPE,
		IPC_SET_PRINTER_INFO,
		IPC_SET_SYSTEM_INFO,
		//IPC_GET_PRINTER_OP_INFO,
		IPC_GET_PRINTER_MODE,
	};

	{
		CString strConfig;
		
		m_Config.ToString (IPC_SET_PH_TYPE, strConfig, this);
		str += strConfig + ';';
	}

	for (int i = 0; i < ARRAYSIZE (lCmd); i++) {
		CString strConfig;
		
		m_Config.ToString (lCmd [i], strConfig, this);
		str += strConfig;
	}

	return str;
}

void CRemoteHead::SendConfiguration()
{
	ULONG lCmd [] = 
	{
		IPC_SET_PH_TYPE, 
		IPC_SET_PRINTER_INFO, 
		IPC_SET_SYSTEM_INFO,
		//IPC_SET_PRINTER_OP_INFO,
		IPC_SET_PRINTER_MODE,
	};

	for (int i = 0; i < ARRAYSIZE (lCmd); i++) {
		CString sConfig;
	
		m_Config.ToString (lCmd [i], sConfig, this);
		TRACEF (GetIpcCmdName (lCmd [i]) + _T (": ") + sConfig);
		Send (lCmd [i], GetNextSequence(), sConfig, NULL);
		::Sleep (500);
	}

	if (!IsDigiBoard ())
		Send (IPC_SAVE_ALL_PARAMS, GetNextSequence(), _T (""));
	else {
		CPacket reply;
	
		::AfxGetMainWnd ()->BeginWaitCursor ();
		Send (IPC_SAVE_ALL_PARAMS, GetNextSequence(), _T (""), &reply, GlobalVars::SaveEditSessionTimeout);
		::AfxGetMainWnd ()->EndWaitCursor ();
	}

	#ifdef _DEBUG
	{
		CString strBarcode, strDatamatrix, strStrobe;
		CString strConfig = GetConfiguration (strBarcode, strDatamatrix, strStrobe);
		CString str;

		bool bSendConfig = CompareConfiguration (strConfig, strBarcode, strDatamatrix, strStrobe) == false;

		if (bSendConfig)
			TRACEF ("CompareConfiguration fails");
	}
	#endif

	Send (IPC_SET_STATUS_MODE, GetNextSequence(), _T ("STATUS_MODE=ON"));

	if (IsDigiBoard ()) {
		TRACEF ("UPDATING_FINISHED");
		
		while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM)this)) 
			::Sleep (0);
	}
}


void CRemoteHead::GetLocalFontList (CStringArray & vFonts)
{
	struct _finddata_t c_file;
	CString strDir = FoxjetDatabase::GetHomeDir () + _T ("\\Fonts\\*.fjf");
	long lFile;

	if ((lFile = _findfirst (w2a (strDir), &c_file)) != -1L) {
		CString str = c_file.name;
	
		str.MakeUpper();
		vFonts.Add (str);

		while( _findnext (lFile, &c_file) == 0) {
			CString str = c_file.name;
			
			str.MakeUpper();
			vFonts.Add (str);
		}

		_findclose (lFile);
	}
}

void CRemoteHead::GetLocalFontList()
{
	struct _finddata_t c_file;
	long hFile;
	CString sFile;
	CString strDir = FoxjetDatabase::GetHomeDir () + _T ("\\Fonts\\*.fjf");

	m_FontTransferList.RemoveAll();
	if( (hFile = _findfirst(w2a (strDir), &c_file )) != -1L )
	{
		sFile = c_file.name;
		sFile.MakeUpper();
		m_FontTransferList.AddTail ( sFile );

		while( _findnext( hFile, &c_file ) == 0 )
		{
			sFile = c_file.name;
			sFile.MakeUpper();
			m_FontTransferList.AddTail ( sFile );
		}
		_findclose( hFile );
   }
}

void CRemoteHead::OnGetMessageIDs(UINT wParam, LONG lParam)
{
	m_hLabelListUpdate = (void *)lParam;
	Send ( IPC_GET_LABEL_IDS, GetNextSequence(), NULL, 0, NULL);
}

bool CRemoteHead::SendFont ( CString sFont , ULONG batchID ) 
{
	using namespace FoxjetCommon;

	PUCHAR pBuf = NULL;
	DWORD dwLen;
	CString sFile = FoxjetDatabase::GetHomeDir () + _T("\\Fonts\\") + sFont;
	bool result = true;
	CFile File;

	//TRACEF (sFont);

	QueryDiskUsage ();

	try 
	{
		if ( File.Open ( sFile, CFile::modeRead|CFile::shareDenyWrite) )
		{
			bool bWaitForReply = ((const VERSIONSTRUCT &)CVersion (m_strSwVer) >= (const VERSIONSTRUCT &)CVersion (1, 20, 0, 0, 26));
			DWORD dwTimeout = bWaitForReply ? 5000 : 1000;
			bool bFailed = false;
			CPacket reply;

			dwLen = File.GetLength ();
			pBuf = new UCHAR[dwLen];
			memset ( pBuf, NULL, dwLen);
			DWORD dwActual = File.Read ( pBuf, dwLen );
			File.Close();

			Send (IPC_PUT_FONT, batchID, pBuf, dwLen, &reply);
			delete [] pBuf;

			if (!CommandSuccessful (IPC_PUT_FONT, &reply)) 
				bFailed = true;

			if (bFailed) {
				CString str;
				
				str.Format (LoadString (IDS_SENDFONTFAILED), sFile);
				MsgBox (str);
				return false;
			}
		}
	}
	catch ( CMemoryException *e) { HANDLEEXCEPTION (e); return false; }
	catch ( CFileException *e) { HANDLEEXCEPTION (e); return false; }

	QueryDiskUsage ();

	return result;
}

void CRemoteHead::QueryDiskUsage ()
{
	CPacket reply;
	
	Send (IPC_GET_DISK_USAGE, GetNextSequence (), _T (""), &reply);
	int flash_size = 0, total_blocks = 0, free_blocks = 0, block_size = 0, total_inodes = 0, free_inodes = 0;

	int nScan = sscanf ((const char *)reply.Data, "flash_size=%d;total_blocks=%d;free_blocks=%d;block_size=%d;total_inodes=%d;free_inodes=%d;",
		&flash_size, &total_blocks, &free_blocks, &block_size, &total_inodes, &free_inodes);

	if (nScan == 6) {
		m_dDiskUsage = ((double)(total_blocks - free_blocks) / (double)total_blocks) * 100.0;
		CString strTmp;

		strTmp.Format (_T ("Usage=%.02f%%;"), m_dDiskUsage);
		TRACEF (strTmp + _T (" ") + CString (a2w (reply.Data)));
	}
}

void CRemoteHead::OnSyncronizeLabelStore(UINT wParam, LONG lParam)
{
	using namespace FoxjetDatabase;
	using namespace FoxjetCommon;

	DWORD dwFlags = lParam;
	CArray <MESSAGESTRUCT, MESSAGESTRUCT &> vUpdateMessages, vAllMessages;
	CStringArray vFonts;
	FoxjetDatabase::HEADSTRUCT head= m_Config.GetHeadSettings ();
	const CVersion ver (m_strSwVer);

	TRACEF (ver.ToString ());

	if (ver < CVersion (1, 20))
		return;

	CSuspendDynamicDataDownload (this);

//	if (wParam != 1)
		while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, VERIFYING_IP_DATA, (LPARAM)this)) 
			::Sleep (0);

	FoxjetDatabase::GetMessageRecords (theApp.m_Database, head.m_lID, vAllMessages);
	vUpdateMessages.Append (vAllMessages);
	GetLocalFontList (vFonts);

	if (!(dwFlags & CSyncDlg::SYNC_DELETE_MEMSTORE)) {
		bool bPrompt = false;

		// determine which messages should be downloaded
		/* if (dwFlags & CSyncDlg::SYNC_MESSAGES) */ 
		{
			CPacket reply;

			Send (IPC_GET_LABEL_IDS, GetNextSequence(), NULL, 0, &reply);

			/*
			{
				CStringArray v;

				Tokenize (Extract (a2w (reply.Data), "LABELIDS=", ";"), v);

				TRACEF (FoxjetDatabase::ToString (v.GetSize ()));

				for (int i = 0; i < v.GetSize (); i++) 
					TRACEF (v [i]);
			}
			*/
			
			if (CommandSuccessful (IPC_GET_LABEL_IDS, &reply)) {
				const CString strLabels = a2w (reply.Data);
				
				for (int i = vUpdateMessages.GetSize () - 1; i >= 0; i--) {
					const MESSAGESTRUCT & msg = vUpdateMessages [i];

					if (msg.m_bIsUpdated || strLabels.Find (msg.m_strName) == -1) {
						TRACEF (GetFriendlyName () + ": need to update: " + msg.m_strName);
					}
					else
						vUpdateMessages.RemoveAt (i);
				}

				bPrompt |= vUpdateMessages.GetSize () ? true : false;
			}
		}

		// determine which fonts should be downloaded
		/* if (dwFlags & CSyncDlg::SYNC_FONTS) */
		{
			CPacket reply;

			Send (IPC_GET_FONT_IDS, GetNextSequence (), NULL, 0, &reply);

			if (CommandSuccessful (IPC_GET_FONT_IDS, &reply)) {
				CString strFonts = a2w (reply.Data);

				strFonts.MakeUpper ();

				for (int i = vFonts.GetSize () - 1; i >= 0; i--) {
					CString strFont = vFonts [i];

					strFont.MakeUpper ();
					strFont.Replace (_T (".FJF"), _T (""));

					if (strFonts.Find (strFont) != -1)
						vFonts.RemoveAt (i);
					else {
						TRACEF (GetFriendlyName () + ": need to update: " + strFont);
					}
				}

				bPrompt |= vFonts.GetSize () ? true : false;
			}
		}

		if (bPrompt && !(dwFlags & CSyncDlg::SYNC_NO_PROMPT)) {
			DWORD dw = CSyncDlg::SYNC_SHOW_DELETE, dwSync;

			dwSync |= vFonts.GetSize ()					? CSyncDlg::SYNC_FONTS			: 0;
			dw |= (dwFlags & CSyncDlg::SYNC_FONTS)		? CSyncDlg::SYNC_FONTS			: 0;

			dwSync |= vUpdateMessages.GetSize ()		? CSyncDlg::SYNC_MESSAGES		: 0;
			dw |= (dwFlags & CSyncDlg::SYNC_MESSAGES)	? CSyncDlg::SYNC_MESSAGES		: 0;

			CSyncDlg dlg (dw, dwSync, CWnd::FromHandle (m_hControlWnd));

			dlg.m_strPrompt.Format (LoadString (IDS_SYNCIPCONTROLLER), GetFriendlyName(), GetUID());

			if (dlg.DoModal () == IDOK) {
				dwFlags = 0;
				dwFlags |= dlg.m_bFonts		? CSyncDlg::SYNC_FONTS		: 0;
				dwFlags |= dlg.m_bMessages	? CSyncDlg::SYNC_MESSAGES	: 0;

				if (dlg.m_bDelete)
					dwFlags |= CSyncDlg::SYNC_DELETE_MEMSTORE;

				if (theApp.GetSingleMsgMode () && dlg.m_bMessages)
					MsgBox (LoadString (IDS_SINGLEMESSAGEMODEDOWNLOADERROR), MB_ICONINFORMATION);
			}
			else {
				while (!::PostMessage ( m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM) this )) 
					Sleep (0);
			
				return;
			}
		}
	}

	bool bFonts		= (dwFlags & CSyncDlg::SYNC_FONTS)		? true : false;
	bool bMessages	= (dwFlags & CSyncDlg::SYNC_MESSAGES)	? true : false;

	if (dwFlags & CSyncDlg::SYNC_DELETE_MEMSTORE) {
		bFonts = bMessages = true;
		OnDeleteMemStore (0, 0); // deletes fonts, msgs & bmps

		vUpdateMessages.RemoveAll ();
		vUpdateMessages.Append (vAllMessages);
	}

	if (bFonts) {
		OnUpdateFonts ( GENERATE_FONT_LIST, 0 );
		Sleep( 250 );
		OnUpdateFonts ( TRANSFER_FONTS, 0);
		Sleep ( 500 );
	}

	if (bMessages) {
		if (!theApp.GetSingleMsgMode ()) 
			UpdateLabels (vUpdateMessages);
	}

	if (theApp.GetSingleMsgMode ()) 
		OnDeleteLabels (1, 0);

//	if ( wParam != 1 )
		while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM)this)) 
			::Sleep (0);

	if (dwFlags & CSyncDlg::SYNC_NOTIFY_COMPLETE) {
		DWORD dwResult = 0;
		LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST,
			WM_SYNCREMOTEHEADCOMPLETE, head.m_lID, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dwResult);
	}
}


void CRemoteHead::EnableDynamicDataDownload (bool bEnable) // sw0849
{
	m_bDynamicDataDownload = bEnable;
	TRACEF (GetFriendlyName () + CString (bEnable ? ": enable dyn download" : ": disable dyn download"));
}

void CRemoteHead::OnDeleteMemStore( UINT uParam, LONG lParam )
{
	CSuspendDynamicDataDownload (this);

	while (!::PostMessage ( m_hControlWnd, WM_HEAD_PROCESSING, LABEL_CLEANUP, (LPARAM) this)) 
		::Sleep (0);

	for (int i = 0; i < FJMESSAGE_DYNAMIC_SEGMENTS; i++) {
		LONG lID = i + 1;
		CString str;

		str.Format (_T ("ID=%d;DATA=\"\";"), lID);
		Send (IPC_SET_VARDATA_ID, GetNextSequence (), str, NULL);
		TRACEF (str);

		ASSERT (lID >= 0 && lID < ARRAYSIZE (m_strDynData));
		m_strDynData [lID] = _T ("");
		VERIFY (fj_setWinDynData (lID, ""));
	}

	if (StartEditSession ()) {
		Send (IPC_DELETE_ALL_PRINTER_ELEMENTS, GetNextSequence (), NULL, NULL, NULL);
		SaveEditSession();
	}

	while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM) this)) 
		::Sleep (0);
}

void CRemoteHead::OnDeleteLabels( UINT wParam, LONG lParam )
{
	CSuspendDynamicDataDownload (this);

	ULONG batchID = GetNextSequence();
	bool bEditSession = wParam ? true : false;

	SetLastError ( 0 );
	
	while (!::PostMessage ( m_hControlWnd, WM_HEAD_PROCESSING, LABEL_CLEANUP, (LPARAM) this )) 
		::Sleep (0);

	if (bEditSession) {
		if (!StartEditSession ()) {
			//SetLastError (START_EDIT_SESSION_FAILED);
			return;
		}
	}
	
	if (!Send (IPC_DELETE_ALL_LABELS, batchID, _T (""), NULL)) {
		SetLastError (TRANSFER_FAILED);
		return;
	}

	if (!Send (IPC_DELETE_ALL_BMPS, batchID, _T (""), NULL)) {
		SetLastError (TRANSFER_FAILED);
		return;
	}

	if (bEditSession) 
		if (!SaveEditSession ()) 
			SetLastError (SAVE_EDIT_SESSION_FAILED);

	while (!::PostMessage ( m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM) this )) 
		::Sleep (0);
}

static const CString ToString (const LPBYTE pData, int lLen)
{
	CString str; 
	LPBYTE p = pData;
	
	for (int i = 0; i < lLen; i++) { 
		CString strTmp; 
		strTmp.Format (_T ("%02X"), p++); 
		str += strTmp; 
	}

	return str;
}

bool CRemoteHead::IsNetFirmware (const CString & str)
{
	try
	{
		CFile f;
		PUCHAR pData = NULL;

		f.Open (str, CFile::modeRead | CFile::shareDenyWrite);
		
		ULONG lLen = f.GetLength ();
		pData = new UCHAR [lLen];
		
		f.Read (pData, lLen);
		f.Close();

		return IsNetFirmware (pData, lLen);
	}
	catch ( CFileException *e )  { HANDLEEXCEPTION (e); }

	return false;
}

bool CRemoteHead::IsNetFirmware (PUCHAR pData, ULONG lLen)
{
	/*
	{
		using namespace FoxjetCommon;
		const int nMax = 1024;
		int nSize = nMax;
		BYTE header [nMax] = { 0 };

		CStringArray v;
		
		GetFiles (_T ("C:\\Temp\\Debug"), _T ("ram*.bin"), v);

		if (v.GetSize ()) {
			if (FILE * fp = _tfopen (v [0], _T ("r"))) {
				fread (header, ARRAYSIZE (header), 1, fp);
				fclose (fp);
			}
		}

		for (int nFile = 0; nFile < v.GetSize (); nFile++) {
			TRACEF (v [nFile]);
	
			if (FILE * fp = _tfopen (v [nFile], _T ("r"))) {
				BYTE buffer [nMax] = { 0 };

				fread (buffer, ARRAYSIZE (buffer), 1, fp);

				for (int i = 0; i < nMax; i++) {
					if (buffer [i] != header [i]) {
						nSize = i;
						break;
					}
				}

				fclose (fp);
			}
		}

		{
			CString str;
			
			str.Format (_T ("\n\t[%d]\n\t"), nSize);

			for (int i = 0; i < nSize; i++) {
				CString strTmp;

				strTmp.Format (_T ("0x%02X,%s"), header [i], (!(i % 15)) ? _T ("\n\t") : _T (" "));
				str += strTmp;
			}

			TRACEF (str);
		}
	}
	*/

	static const BYTE nNetHeader [33] = 
	{
		0xE5, 0x9F, 0x00, 0x38, 0xE5, 0x90, 0x20, 0x00, 0xE5, 0x9F, 0x10, 0x34, 0xE0, 0x02, 0x20, 
		0x01, 0xE5, 0x80, 0x20, 0x00, 0xE5, 0x9F, 0xF0, 0x04, 0xE1, 0xA0, 0x00, 0x00, 0xE1, 0xA0, 
		0x00, 0x00, 0x00, 
	};

	if (lLen > ARRAYSIZE (nNetHeader)) {
		int n = memcmp (nNetHeader, pData, ARRAYSIZE (nNetHeader));

		return n == 0 ? true : false;
	}

	return false;
}

void CRemoteHead::DoUpdate (RemoteHead::UPDATETYPE type, PUCHAR pData, ULONG lLen)
{
	CSuspendDynamicDataDownload (this);

	SetLastError ( 0 );

	if ((!IsDigiBoard () && !IsNetFirmware (pData, lLen)) || (IsDigiBoard () && IsNetFirmware (pData, lLen))) {
		CString str;

		str.Format (LoadString (IDS_FIRMWARE_INCOMPATABLE), GetFriendlyName ());
		MsgBox (str, MB_OK | MB_ICONERROR);
		return;
	}

	WORD wParam = (type == UPDATE_FIRMWARE) ? FIRMWARE_UPDATE : GA_UPDATE;
	ULONG batchID = GetNextSequence();

	ASSERT (pData);

	while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, wParam, (LPARAM)this)) 
		::Sleep (0);

	if (StartEditSession ()) {
		int nCmd = (type == UPDATE_FIRMWARE) ? IPC_FIRMWARE_UPGRADE : IPC_GA_UPGRADE;

		Sleep ( 1000 );	// Just so the message can be seen by the user.

		if (nCmd == IPC_GA_UPGRADE && IsDigiBoard ()) {
			CPacket reply;

			Send (nCmd, batchID, pData, lLen, &reply);
			CString str, strReply = a2w (reply.Data);
			bool bResult = strReply.Find (_T ("IPC_GA_UPGRADE complete")) != -1;

			SetLastError (bResult ? REBOOT_PRINTER : GA_UPDATE_FAILED);
			while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM)this)) 
				::Sleep (0);

			Send (IPC_EDIT_SAVE, GetNextSequence(), NULL, 0, NULL);
			return;
		}
		else
			Send (nCmd, batchID, pData, lLen, NULL );


		while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, WAITING_FOR_SAVE_ACK, (LPARAM)this)) 
			::Sleep (0);

		Send (IPC_EDIT_SAVE, GetNextSequence(), NULL, 0, NULL);

		const CTime tmStart = CTime::GetCurrentTime ();

		{
			LOCK (m_csReadSocket);
			bool bTimeout = false;
			double dTimeout = (double)GlobalVars::SaveEditSessionTimeout / 1000.0;

			if (IsDigiBoard ())
				dTimeout *= 2.0;
		
			do {
				CPacket packet;

				if (m_socket == INVALID_SOCKET)
					break;

				if (ReadSocket (&packet)) {
					if (packet.Header.Cmd == IPC_STATUS) {
						CMapStringToString map;
						CString strValue, strData = a2w (packet.Data);

						CKeyParse::Parse (strData, map);

						if (map.Lookup (_T ("HeadStatus"), strValue)) {
							if (strValue.Find (_T ("reboot printer")) != -1 ||
								strValue.Find (_T ("GA has been updated")) != -1)
							{
								GlobalFuncs::SetEditSession (m_Config.m_HeadSettings, false);
								m_state.m_bShortTimeout = true;

								SetLastError (REBOOT_PRINTER);
								
								while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM)this)) 
									::Sleep (0);

								break;
							}
						}
					}
				}

				CTimeSpan tm = CTime::GetCurrentTime () - tmStart;

				bTimeout = tm.GetTotalSeconds () > (LONG)ceil (dTimeout);
				::Sleep (0);
			} 
			while (!bTimeout);

			if (bTimeout) {
				DWORD dwError = type == UPDATE_FIRMWARE ? FIRMWARE_UPDATE_FAILED : GA_UPDATE_FAILED;

				SetLastError (dwError);
				
				while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM) this)) 
					::Sleep (0);
			}
		}
	}
	else {
		DWORD dwError = (type == UPDATE_FIRMWARE) ? FIRMWARE_UPDATE_FAILED : GA_UPDATE_FAILED;
		SetLastError (dwError);

		while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM)this))
			::Sleep (0);
	}
}

void CRemoteHead::OnUpdateGa ( UINT uParam, LONG lParam )
{
	CString * pstrFile = (CString *)lParam;
	PUCHAR pData = NULL;

	ASSERT (pstrFile);

	try
	{
		CFile f;
		PUCHAR pData = NULL;

		f.Open (* pstrFile, CFile::modeRead | CFile::shareDenyWrite);
		
		ULONG lLen = f.GetLength ();
		pData = new UCHAR [lLen];
		
		f.Read (pData, lLen);
		f.Close();

		DoUpdate (UPDATE_GA, pData, lLen);
	}
	catch ( CFileException *e )  { HANDLEEXCEPTION (e); }

	delete [] pData;
	delete pstrFile;
}

void CRemoteHead::OnUpdateFirmware ( UINT uParam, LONG lParam )
{
	CString * pstrFile = (CString *)lParam;
	PUCHAR pData = NULL;

	ASSERT (pstrFile);

	if (IsDigiBoard ()) {
		CString strServer = m_Config.GetHeadSettings ().m_strUID;
		CInternetSession session (_T ("DigiNET firmware upload"));
		const CString strUser = _T ("root");
		const CString strPass = _T (APP_ROOT_PASSWORD);

		while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATE_FIRMWARE, (LPARAM)this)) 
			::Sleep (0);

		if (CFtpConnection * pFTP = session.GetFtpConnection (strServer, strUser, strPass)) {
			const CString strLocal = * pstrFile;
			const CString strRemote = _T ("image.bin");

			TRACEF (_T ("FTP [") + strServer + _T (", ") + strUser + _T (", ") + strPass + _T ("]: ") + * pstrFile + _T (" --> ") + strRemote);

			while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, WAITING_FOR_SAVE_ACK, (LPARAM)this)) 
				::Sleep (0);

			BOOL bPutFile = pFTP->PutFile (strLocal, strRemote, FTP_TRANSFER_TYPE_BINARY);

			pFTP->Close ();

			if (!bPutFile) {
				CString str;

				str.Format (LoadString (IDS_FIRMWARE_UPDATE_FAILED), GetFriendlyName ());
				MsgBox (str, MB_OK | MB_ICONERROR);
			}

			delete pFTP;
		}
		else {
			CString str;

			str.Format (LoadString (IDS_FTPFAILED), GetFriendlyName ());
			MsgBox (str, MB_OK | MB_ICONERROR);
		}

		session.Close();

		while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, UPDATING_FINISHED, (LPARAM)this))
			::Sleep (0);

		return;
	}

	try
	{
		CFile f;

		f.Open (* pstrFile, CFile::modeRead | CFile::shareDenyWrite);
		
		ULONG lLen = f.GetLength ();
		pData = new UCHAR [lLen];
		
		f.Read (pData, lLen);
		f.Close();

		DoUpdate (UPDATE_FIRMWARE, pData, lLen);
	}
	catch ( CFileException *e )  { HANDLEEXCEPTION (e); }

	delete [] pData;
	delete pstrFile;
}

bool CRemoteHead::CommandSuccessful(ULONG lSent, const CPacket * pResult)
{
	bool result = false;
	CString sLog;
	
	m_sEditSessionOwner.Empty();

	if ( pResult->Header.Cmd != IPC_INVALID )
	{
		switch ( lSent )
		{
			case IPC_EDIT_SAVE:
				if ( pResult->Header.Cmd == IPC_EDIT_STATUS )
				{
					CString sData, sValue;
					sData.Format (_T ("%s"), a2w (pResult->Data));
					CMapStringToString mapData;
					CKeyParse::Parse ( sData, mapData );
					mapData.Lookup (_T ("EDIT_LOCK"), sValue );
					if ( sValue.CompareNoCase (_T ("FREE")) == 0)
						result = true;
				}
				break;
			case IPC_GET_FONT_IDS:
				if ( pResult->Header.Cmd == IPC_SET_FONT_IDS )
					result = true;
				break;			
			case IPC_GET_LABEL_IDS:
				if ( pResult->Header.Cmd == IPC_SET_LABEL_IDS )
					result = true;
				break;
			case IPC_GET_LABELS:
				if ( pResult->Header.Cmd == IPC_PUT_LABEL )
					result = true;
				break;
			case IPC_EDIT_START:
			{
				if ( pResult->Header.Cmd == IPC_EDIT_STATUS )
				{
					CString sData, sValue;
					sData.Format (_T ("%s"), a2w (pResult->Data));
					CMapStringToString mapData;
					CKeyParse::Parse ( sData, mapData );
					mapData.Lookup (_T ("EDIT_LOCK"), sValue );
					if ( sValue.CompareNoCase (_T ("SET")) == 0)
					{
						mapData.Lookup (_T ("EDIT_IP"), sValue );
						CString sAddress;
						sAddress = GetLocalAddress ();

						if ( sAddress.CompareNoCase ( sValue ) != 0 )
						{
							CString sMsg;
							mapData.Lookup (_T ("EDIT_IP"), sValue );
//							sMsg.Format ("Edit Session in progress on %s\n\tSession Owner:'%s'\n\tLocked Since: ", GetFriendlyName(), sValue);
							sMsg.Format (_T ("%s:\nSince: "), sValue);
							mapData.Lookup (_T ("EDIT_TIME"), sValue );
							sMsg += sValue;
//							MsgBox ( sMsg );
							m_sEditSessionOwner = sMsg;
							SetLastError ( START_EDIT_SESSION_LOCKED );
						}
						else
							result = true;
					}
					else
					{
						sLog.Format (LoadString (IDS_EDITLOCKINVALIDSTATE), sValue );
						MsgBox ( sLog );
					}
				}
				else
				{
					sLog.Format (LoadString (IDS_INVALIDPRINTHEADRESPONSE), pResult->Header.Cmd );
					MsgBox ( sLog );
				}
			}
			break;
			case IPC_PUT_FONT:
				result = true;
				break;
			default:
				MsgBox (LoadString (IDS_SUCCESSFROMUNKNOWNCMD));
				break;
		}
	}
	return result;
}

bool CRemoteHead::StartEditSession()
{
{ static int nCalls = 0; CString str; str.Format (_T ("StartEditSession [%d]"), nCalls++); TRACEF (str); } // TODO: rem

	bool result = false;
	CString sMsg;
	CCmdPacket *pCmd = NULL;

	if (m_state.m_bClosed)
		return result;

	GlobalFuncs::SetEditSession (m_Config.m_HeadSettings, true);

	CPacket reply;

	if (Send (IPC_EDIT_START, GetNextSequence(), NULL, 0, &reply, GlobalVars::StartEditSessionTimeout)) {
		if  (CommandSuccessful (IPC_EDIT_START, &reply))
			result = true;
		else
			SetLastError (START_EDIT_SESSION_FAILED_NO_RESPONCE);
	}
	else 
		SetLastError (START_EDIT_SESSION_FAILED_SEND);

	if (!result)
		Disconnect ();

	return result;
}

// TODO: rem /////////////////////////////////////////////////////////////////////////////////
#ifndef _DEBUG
struct 
{
	DWORD	m_dw;
	LPCTSTR m_lpsz;
} static const map [] =
{
	{ IPC_COMPLETE,						_T ("IPC_COMPLETE"),					},
	{ IPC_CONNECTION_CLOSE,				_T ("IPC_CONNECTION_CLOSE"),			},
	{ IPC_INVALID,						_T ("IPC_INVALID"),						},
	{ IPC_ECHO,							_T ("IPC_ECHO"),						},
	{ IPC_STATUS,						_T ("IPC_STATUS"),						},
	{ IPC_GET_STATUS,					_T ("IPC_GET_STATUS"),					},
	{ IPC_PHOTO_TRIGGER,				_T ("IPC_PHOTO_TRIGGER"),				},
	{ IPC_EDIT_STATUS,					_T ("IPC_EDIT_STATUS"),					},
	{ IPC_GET_EDIT_STATUS,				_T ("IPC_GET_EDIT_STATUS"),				},
	{ IPC_EDIT_START,					_T ("IPC_EDIT_START"),					},
	{ IPC_EDIT_SAVE,					_T ("IPC_EDIT_SAVE"),					},
	{ IPC_EDIT_CANCEL,					_T ("IPC_EDIT_CANCEL"),					},
	{ IPC_IMAGE_HEADER,					_T ("IPC_IMAGE_HEADER"),				},
	{ IPC_IMAGE_DATA,					_T ("IPC_IMAGE_DATA"),					},
	{ IPC_GET_LABEL_IDS,				_T ("IPC_GET_LABEL_IDS"),				},			
	{ IPC_SET_LABEL_IDS,				_T ("IPC_SET_LABEL_IDS"),				},			
	{ IPC_GET_LABELS,					_T ("IPC_GET_LABELS"),					},			
	{ IPC_GET_LABEL,					_T ("IPC_GET_LABEL"),					},			
	{ IPC_PUT_LABEL,					_T ("IPC_PUT_LABEL"),					},			
	{ IPC_DELETE_LABEL,					_T ("IPC_DELETE_LABEL"),				},			
	{ IPC_SELECT_LABEL,					_T ("IPC_SELECT_LABEL"),				},			
	{ IPC_GET_LABEL_ID_SELECTED,		_T ("IPC_GET_LABEL_ID_SELECTED"),		},			
	{ IPC_SET_LABEL_ID_SELECTED,		_T ("IPC_SET_LABEL_ID_SELECTED"),		},			
	{ IPC_DELETE_ALL_LABELS,			_T ("IPC_DELETE_ALL_LABELS"),			},			
	{ IPC_GET_MSG_IDS,					_T ("IPC_GET_MSG_IDS"),					},			
	{ IPC_SET_MSG_IDS,					_T ("IPC_SET_MSG_IDS"),					},			
	{ IPC_GET_MSGS,						_T ("IPC_GET_MSGS"),					},			
	{ IPC_GET_MSG,						_T ("IPC_GET_MSG"),						},			
	{ IPC_PUT_MSG,						_T ("IPC_PUT_MSG"),						},			
	{ IPC_DELETE_MSG,					_T ("IPC_DELETE_MSG"),					},			
	{ IPC_GET_TEXT_IDS,					_T ("IPC_GET_TEXT_IDS"),				},			
	{ IPC_SET_TEXT_IDS,					_T ("IPC_SET_TEXT_IDS"),				},			
	{ IPC_GET_TEXT_ELEMENTS,			_T ("IPC_GET_TEXT_ELEMENTS"),			},			
	{ IPC_GET_TEXT,						_T ("IPC_GET_TEXT"),					},			
	{ IPC_PUT_TEXT,						_T ("IPC_PUT_TEXT"),					},			
	{ IPC_DELETE_TEXT,					_T ("IPC_DELETE_TEXT"),					},			
	{ IPC_GET_BITMAP_IDS,				_T ("IPC_GET_BITMAP_IDS"),				},			
	{ IPC_SET_BITMAP_IDS,				_T ("IPC_SET_BITMAP_IDS"),				},			
	{ IPC_GET_BITMAP_ELEMENTS,			_T ("IPC_GET_BITMAP_ELEMENTS"),			},			
	{ IPC_GET_BITMAP,					_T ("IPC_GET_BITMAP"),					},			
	{ IPC_PUT_BITMAP,					_T ("IPC_PUT_BITMAP"),					},			
	{ IPC_DELETE_BITMAP,				_T ("IPC_DELETE_BITMAP"),				},			
	{ IPC_GET_DYNIMAGE_IDS,				_T ("IPC_GET_DYNIMAGE_IDS"),			},			
	{ IPC_SET_DYNIMAGE_IDS,				_T ("IPC_SET_DYNIMAGE_IDS"),			},			
	{ IPC_GET_DYNIMAGE_ELEMENTS,		_T ("IPC_GET_DYNIMAGE_ELEMENTS"),		},			
	{ IPC_GET_DYNIMAGE,					_T ("IPC_GET_DYNIMAGE"),				},			
	{ IPC_PUT_DYNIMAGE,					_T ("IPC_PUT_DYNIMAGE"),				},			
	{ IPC_DELETE_DYNIMAGE,				_T ("IPC_DELETE_DYNIMAGE"),				},			
	{ IPC_PUT_DYNIMAGE_DATA,			_T ("IPC_PUT_DYNIMAGE_DATA"),			},			
	{ IPC_GET_DATETIME_IDS,				_T ("IPC_GET_DATETIME_IDS"),			},			
	{ IPC_SET_DATETIME_IDS,				_T ("IPC_SET_DATETIME_IDS"),			},			
	{ IPC_GET_DATETIME_ELEMENTS,		_T ("IPC_GET_DATETIME_ELEMENTS"),		},			
	{ IPC_GET_DATETIME,					_T ("IPC_GET_DATETIME"),				},			
	{ IPC_PUT_DATETIME,					_T ("IPC_PUT_DATETIME"),				},			 
	{ IPC_DELETE_DATETIME,				_T ("IPC_DELETE_DATETIME"),				},			
	{ IPC_GET_COUNTER_IDS,				_T ("IPC_GET_COUNTER_IDS"),				},			
	{ IPC_SET_COUNTER_IDS,				_T ("IPC_SET_COUNTER_IDS"),				},			
	{ IPC_GET_COUNTER_ELEMENTS,			_T ("IPC_GET_COUNTER_ELEMENTS"),		},			
	{ IPC_GET_COUNTER,					_T ("IPC_GET_COUNTER"),					},			
	{ IPC_PUT_COUNTER,					_T ("IPC_PUT_COUNTER"),					},			
	{ IPC_DELETE_COUNTER,				_T ("IPC_DELETE_COUNTER"),				},			
	{ IPC_GET_BARCODE_IDS,				_T ("IPC_GET_BARCODE_IDS"),				},			
	{ IPC_SET_BARCODE_IDS,				_T ("IPC_SET_BARCODE_IDS"),				},			
	{ IPC_GET_BARCODE_ELEMENTS,			_T ("IPC_GET_BARCODE_ELEMENTS"),		},			
	{ IPC_GET_BARCODE,					_T ("IPC_GET_BARCODE"),					},			
	{ IPC_PUT_BARCODE,					_T ("IPC_PUT_BARCODE"),					},			
	{ IPC_DELETE_BARCODE,				_T ("IPC_DELETE_BARCODE"),				},			
	{ IPC_GET_FONT_IDS,					_T ("IPC_GET_FONT_IDS"),				},			
	{ IPC_SET_FONT_IDS,					_T ("IPC_SET_FONT_IDS"),				},			
	{ IPC_GET_FONTS,					_T ("IPC_GET_FONTS"),					},			
	{ IPC_GET_FONT,						_T ("IPC_GET_FONT"),					},			
	{ IPC_PUT_FONT,						_T ("IPC_PUT_FONT"),					},			
	{ IPC_DELETE_FONT,					_T ("IPC_DELETE_FONT"),					},			
	{ IPC_GET_BMP_IDS,					_T ("IPC_GET_BMP_IDS"),					},			
	{ IPC_SET_BMP_IDS,					_T ("IPC_SET_BMP_IDS"),					},			
	{ IPC_GET_BMPS,						_T ("IPC_GET_BMPS"),					},			
	{ IPC_GET_BMP,						_T ("IPC_GET_BMP"),						},			
	{ IPC_PUT_BMP,						_T ("IPC_PUT_BMP"),						},			
	{ IPC_DELETE_BMP,					_T ("IPC_DELETE_BMP"),					},			
	{ IPC_DELETE_ALL_BMPS,				_T ("IPC_DELETE_ALL_BMPS"),				},			
	{ IPC_GET_DYNTEXT_IDS,				_T ("IPC_GET_DYNTEXT_IDS"),				},			
	{ IPC_SET_DYNTEXT_IDS,				_T ("IPC_SET_DYNTEXT_IDS"),				},			
	{ IPC_GET_DYNTEXT_ELEMENTS,			_T ("IPC_GET_DYNTEXT_ELEMENTS"),		},			
	{ IPC_GET_DYNTEXT,					_T ("IPC_GET_DYNTEXT"),					},			
	{ IPC_PUT_DYNTEXT,					_T ("IPC_PUT_DYNTEXT"),					},			
	{ IPC_DELETE_DYNTEXT,				_T ("IPC_DELETE_DYNTEXT"),				},			
	{ IPC_GET_DYNBARCODE_IDS,			_T ("IPC_GET_DYNBARCODE_IDS"),			},			
	{ IPC_SET_DYNBARCODE_IDS,			_T ("IPC_SET_DYNBARCODE_IDS"),			},			
	{ IPC_GET_DYNBARCODE_ELEMENTS,		_T ("IPC_GET_DYNBARCODE_ELEMENTS"),		},			
	{ IPC_GET_DYNBARCODE,				_T ("IPC_GET_DYNBARCODE"),				},			
	{ IPC_PUT_DYNBARCODE,				_T ("IPC_PUT_DYNBARCODE"),				},			
	{ IPC_DELETE_DYNBARCODE,			_T ("IPC_DELETE_DYNBARCODE"),			},			
	{ IPC_GET_GROUP_INFO,				_T ("IPC_GET_GROUP_INFO"),				},			
	{ IPC_SET_GROUP_INFO,				_T ("IPC_SET_GROUP_INFO"),				},			
	{ IPC_GET_SYSTEM_INFO,				_T ("IPC_GET_SYSTEM_INFO"),				},			
	{ IPC_GET_SYSTEM_INTERNET_INFO,		_T ("IPC_GET_SYSTEM_INTERNET_INFO"),	},			
	{ IPC_GET_SYSTEM_TIME_INFO,			_T ("IPC_GET_SYSTEM_TIME_INFO"),		},			
	{ IPC_GET_SYSTEM_DATE_STRINGS,		_T ("IPC_GET_SYSTEM_DATE_STRINGS"),		},			
	{ IPC_GET_SYSTEM_PASSWORDS,			_T ("IPC_GET_SYSTEM_PASSWORDS"),		},			
	{ IPC_GET_SYSTEM_BARCODES,			_T ("IPC_GET_SYSTEM_BARCODES"),			},			
	{ IPC_SET_SYSTEM_INFO,				_T ("IPC_SET_SYSTEM_INFO"),				},			
	{ IPC_GET_PRINTER_ID,				_T ("IPC_GET_PRINTER_ID"),				},			
	{ IPC_GET_PRINTER_INFO,				_T ("IPC_GET_PRINTER_INFO"),			},			
	{ IPC_GET_PRINTER_PKG_INFO,			_T ("IPC_GET_PRINTER_PKG_INFO"),		},			
	{ IPC_GET_PRINTER_PHY_INFO,			_T ("IPC_GET_PRINTER_PHY_INFO"),		},			
	{ IPC_GET_PRINTER_OP_INFO,			_T ("IPC_GET_PRINTER_OP_INFO"),			},			
	{ IPC_SET_PRINTER_INFO,				_T ("IPC_SET_PRINTER_INFO"),			},			
	{ IPC_GET_PRINTER_ELEMENTS,			_T ("IPC_GET_PRINTER_ELEMENTS"),		},			
	{ IPC_PUT_PRINTER_ELEMENT,			_T ("IPC_PUT_PRINTER_ELEMENT"),			},			
	{ IPC_DELETE_ALL_PRINTER_ELEMENTS,	_T ("IPC_DELETE_ALL_PRINTER_ELEMENTS"),	},			
	{ IPC_FIRMWARE_UPGRADE,				_T ("IPC_FIRMWARE_UPGRADE"),			},			
	{ IPC_SET_PH_TYPE,					_T ("IPC_SET_PH_TYPE"),					},			
	{ IPC_SET_PRINTER_MODE,				_T ("IPC_SET_PRINTER_MODE"),			},			
	{ IPC_GET_PRINTER_MODE,				_T ("IPC_GET_PRINTER_MODE"),			},			
	{ IPC_SET_SELECTED_COUNT,			_T ("IPC_SET_SELECTED_COUNT"),			},			
	{ IPC_GET_SELECTED_COUNT,			_T ("IPC_GET_SELECTED_COUNT"),			},			
	{ IPC_SELECT_VARDATA_ID,			_T ("IPC_SELECT_VARDATA_ID"),			},			
	{ IPC_GET_VARDATA_ID,				_T ("IPC_GET_VARDATA_ID"),				},			
	{ IPC_SET_VARDATA_ID,				_T ("IPC_SET_VARDATA_ID"),				},			
	{ IPC_SAVE_ALL_PARAMS,				_T ("IPC_SAVE_ALL_PARAMS"),				},			
	{ IPC_GET_SW_VER_INFO,				_T ("IPC_GET_SW_VER_INFO"),				},			
	{ IPC_GET_GA_VER_INFO,				_T ("IPC_GET_GA_VER_INFO"),				},
	{ IPC_GA_UPGRADE,					_T ("IPC_GA_UPGRADE"),					},
	{ IPC_SET_STATUS_MODE,				_T ("IPC_SET_STATUS_MODE"),				},
	{ IPC_GET_STATUS_MODE,				_T ("IPC_GET_STATUS_MODE"),				},
	{ IPC_GET_DISK_USAGE,				_T ("IPC_GET_DISK_USAGE"),				},			
	{ IPC_GET_PH_TYPE,					_T ("IPC_GET_PH_TYPE"),					},			
	{ IPC_PUT_BMP_DRIVER,				_T ("IPC_PUT_BMP_DRIVER"),				},
};

static CString GetIpcCmdName (DWORD dw)
{
	for (int i = 0; i < ARRAYSIZE (::map); i++)
		if (::map [i].m_dw == dw)
			return ::map [i].m_lpsz;

	return "[not found]";
}

static DWORD GetIpcCmd (const CString & str)
{
	for (int i = 0; i < ARRAYSIZE (::map); i++)
		if (!str.CompareNoCase (::map [i].m_lpsz))
			return ::map [i].m_dw;

	return map [0].m_dw;
}
#endif 
// TODO: rem /////////////////////////////////////////////////////////////////////////////////

bool CRemoteHead::SaveEditSession()
{
{ static int nCalls = 0; CString str; str.Format (_T ("SaveEditSession [%d]"), nCalls++); TRACEF (str); } // TODO: rem

	bool result = false;
	CPacket reply;

	while (!::PostMessage (m_hControlWnd, WM_HEAD_PROCESSING, WAITING_FOR_SAVE_ACK, (LPARAM)this)) 
		::Sleep (0);

	Send (IPC_EDIT_SAVE, GetNextSequence(), NULL, 0, &reply, GlobalVars::SaveEditSessionTimeout);
		
	if  (CommandSuccessful (IPC_EDIT_SAVE, &reply))
		result = true;

	GlobalFuncs::SetEditSession (m_Config.m_HeadSettings, false);
	
	return result;
}

void CRemoteHead::CancelEditSession()
{
{ static int nCalls = 0; CString str; str.Format (_T ("CancelEditSession [%d]"), nCalls++); TRACEF (str); } // TODO: rem

	GlobalFuncs::SetEditSession (m_Config.m_HeadSettings, false);
	Send ( IPC_EDIT_CANCEL, GetNextSequence(), NULL, 0, NULL );
}

void CRemoteHead::TransferBitmap(CString sFile)
{
	CFile file;
	bool bResult = false;
	CString strTitle;

	{
		TCHAR szTitle [MAX_PATH] = { 0 };
		SHELLFLAGSTATE sfs;

		::SHGetSettings (&sfs, SSF_SHOWEXTENSIONS);
		::GetFileTitle (sFile, szTitle, ARRAYSIZE (szTitle) - 1);
		strTitle = szTitle;

		if (sfs.fShowExtensions) {
			int nIndex = strTitle.ReverseFind ('.');

			if (nIndex != -1) 
				strTitle = strTitle.Left (nIndex);
		}
	}

	strTitle.Replace (_T ("."), _T ("_"));
	TRACEF (strTitle);

	if (file.Open (sFile, CFile::modeRead | CFile::shareDenyWrite)) {
		CString sData;
		DWORD dwBitmapLen = file.GetLength();
		PUCHAR pBitmapData = new UCHAR [ dwBitmapLen ];

		memset ( pBitmapData, NULL, dwBitmapLen );
		file.Read ( pBitmapData, dwBitmapLen );
		file.Close();
		sData.Format ( _T ("{bmp,\"%s\",%lu}"), strTitle, dwBitmapLen );
		
		// We must add 1 for NULL byte and we must ensure the NULL and end of Text string is TX'ed.
		DWORD dwTotalLen = sData.GetLength() + dwBitmapLen + 1;
		PUCHAR pData = new UCHAR [ dwTotalLen ];
		memset ( pData, NULL, dwTotalLen );
		PUCHAR p = pData;
		memcpy (p, w2a (sData), sData.GetLength ());
		p+= sData.GetLength() + 1;
		memcpy ( p, pBitmapData, dwBitmapLen );

		ASSERT (fj_bmpCheckFormat (pBitmapData));

		Send (IPC_PUT_BMP, GetNextSequence(), pData, dwTotalLen, NULL);
		bResult = true;
		
		delete [] pData;
		delete [] pBitmapData;
		SetLastError ( 0 );
	}
	
	if (!bResult) {
		CString sMsg;
		sMsg.Format (LoadString (IDS_UNABLETOTRANSFER), sFile );
		MsgBox (sMsg);
		//SetLastError ( BITMAP_TRANSFER_FAILED );
		//AfxThrowUserException();
	}
}

void CRemoteHead::OnAmsCycle (UINT wParam, LONG lParam)
{
	Send (IPC_SET_PRINTER_INFO, GetNextSequence (), _T ("AMS_INACTIVE=0;"), NULL);
	Send (IPC_SET_PRINTER_MODE, GetNextSequence (), _T ("APS_MODE=ENABLE;"), NULL);
	Send (IPC_SET_PRINTER_MODE, GetNextSequence (), _T ("APS_MODE=RUN;"), NULL);
	Send (IPC_SET_PRINTER_MODE, GetNextSequence (), _T ("APS_MODE=DISABLE;"), NULL);
}

void CRemoteHead::SetCounts (__int64 lCount, __int64 lPalletMax, __int64 lUnitsPerPallet)
{
	CHead::SetCounts (lCount, lPalletMax, lUnitsPerPallet);
}

void CRemoteHead::OnChangeCounts(UINT wParam, LONG lParam)
{
	CHead::CHANGECOUNTSTRUCT * pParam = (CHead::CHANGECOUNTSTRUCT *)wParam;
	
	ASSERT (pParam);
	ULONG lCount = (ULONG)pParam->m_lCount;
	CString str;

	str.Format (_T ("CountSel=%d;"), lCount);
	//TRACEF (GetFriendlyName () + _T (": ") + str);
	Send (IPC_SET_SELECTED_COUNT, GetNextSequence (), str, NULL);
	
	while ( !::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this)) 
		::Sleep(0);

	if (m_pElementList) {
		for (int i = 0; i < m_pElementList->GetSize (); i++) {
			if (CCountElement * p = DYNAMIC_DOWNCAST (CCountElement, &m_pElementList->GetObject (i))) {
				TRACEF (GetUID () + _T (": ") + FoxjetDatabase::ToString (lCount));
				p->SetCurValue (lCount);
				p->SetRedraw ();
			}
		}
	}

	// m_Status.m_lCount updated by CRemoteHead::ProcessRxData, case IPC_STATUS

	if (HANDLE h = (HANDLE)lParam)
		::SetEvent (h);

	if (pParam)
		delete pParam;
}

void CRemoteHead::TraceMessageState ()
{ 
#ifdef _DEBUG
/*
	HANDLE hEvent;

	if (Send (IPC_GET_LABEL_IDS, GetNextSequence(), NULL, 0, &hEvent)) {
		if (::WaitForSingleObject (hEvent, GlobalVars::HeadResponseTimeout) != WAIT_TIMEOUT) {
			if (CCmdPacket * pCmd = GetResponse (hEvent)) {
				CStringArray v;
				
				Tokenize (Extract (a2w (pCmd->m_pktInfo.pData), "LABELIDS=", ";"), v);

				for (int i = 0; i < v.GetSize (); i++) 
					TRACEF (v [i]);

				delete pCmd;
			}
		}
	}

	if (Send (IPC_GET_BITMAP_IDS, GetNextSequence(), NULL, 0, &hEvent)) {
		if (::WaitForSingleObject (hEvent, GlobalVars::HeadResponseTimeout) != WAIT_TIMEOUT) {
			if (CCmdPacket * pCmd = GetResponse (hEvent)) {
				CStringArray v;
				
				Tokenize (Extract (a2w (pCmd->m_pktInfo.pData), "BITMAPIDS=", ";"), v);

				for (int i = 0; i < v.GetSize (); i++) 
					TRACEF (v [i]);

				delete pCmd;
			}
		}
	}
*/
#endif //_DEBUG
}

bool CRemoteHead::SetStandbyMode (bool bStandby)
{
	CString str;
	
	str.Format (_T ("PRINT_MODE=%s;APS_MODE=DISABLE;"),
		bStandby ? _T ("STANDBYON") : _T ("STANDBYOFF"));

	TRACEF (str);

	Send ( IPC_SET_PRINTER_MODE, GetNextSequence(), str, NULL );

	return CHead::SetStandbyMode (bStandby);
}

void CRemoteHead::OnSetIgnorePhotocell (UINT wParam, LONG lParam)
{
	bool bIgnore = wParam ? 1 : 0;

	if (bIgnore)
		OnIdleTask (0, 0);
	else
		OnResumeTask (0, 0);

	CHead::OnSetIgnorePhotocell (wParam, lParam);
}

void CRemoteHead::OnDownloadDynData(WPARAM wParam, LPARAM lParam)
{
	using FoxjetIpElements::CItemArray;
	using FoxjetIpElements::CTableItem;

	CItemArray * pvItems = (CItemArray *)lParam;

	ASSERT (pvItems);

	if (!m_bDynamicDataDownload)  // sw0849
		TRACEF (GetFriendlyName () + ": Ignoring: OnDownloadDynData");

	/*
	#ifdef _DEBUG
	{
		CString str;

		for (int i = 0; i < pvItems->GetSize (); i++) {
			CTableItem item = (* pvItems) [i];
			
			str += item.m_strData + ", ";
		}

		TRACEF (str);
	}
	#endif //_DEBUG
	*/

	for (int i = 0; i < pvItems->GetSize (); i++) {
		CTableItem item = (* pvItems) [i];
		LONG lID = item.m_lID;

		if (m_bDynamicDataDownload && item.m_strData.CompareNoCase (CDynamicTableDlg::m_strSkip) != 0) {
			CString str;
	
			str.Format (_T ("ID=%d;DATA=\"%s\";"), lID, item.m_strData);
			Send (IPC_SET_VARDATA_ID, GetNextSequence (), str, NULL);
			TRACEF (str);
		}

		if (lID >= 0 && lID < ARRAYSIZE (m_strDynData)) {
			m_strDynData [lID] = item.m_strData;
			VERIFY (fj_setWinDynData (lID, w2a (item.m_strData)));
		}
	}

	delete pvItems;

	if (HANDLE h = (HANDLE)wParam)
		::SetEvent (h);
}

void CRemoteHead::Shutdown ()
{
	EnableDynamicDataDownload (false);
	CHead::Shutdown ();
}

void CRemoteHead::OnSetSerialPending (WPARAM wParam, LPARAM lParam)
{
	SetSerialPending (wParam ? true : false);
}

void CRemoteHead::OnUpdateCounts (UINT wParam, LONG lParam)
{
	if (m_pElementList) {
		for (int i = 0; i < m_pElementList->GetSize (); i++) {
			if (CCountElement * p = DYNAMIC_DOWNCAST (CCountElement, &m_pElementList->GetObject (i))) {
				TRACEF (GetUID () + _T (": ") + FoxjetDatabase::ToString (m_Status.m_lCount));
				p->SetCurValue (m_Status.m_lCount);
				p->SetRedraw ();
			}
		}
	}

	CHead::OnUpdateCounts (wParam, lParam);
}

void CRemoteHead::OnBuildPreviewImage (UINT wParam, LONG lParam)
{
	if (m_pElementList) {
		for (int i = 0; i < m_pElementList->GetSize (); i++) {
			if (CCountElement * p = DYNAMIC_DOWNCAST (CCountElement, &m_pElementList->GetObject (i))) {
				p->SetRedraw ();
			}
		}
	}

	CHead::OnBuildPreviewImage (wParam, lParam);
}

void CRemoteHead::CloseSocket (SOCKET & s)
{
	if (s != INVALID_SOCKET) {
		if (::shutdown (s, 0) == SOCKET_ERROR) 
			TRACEF (CUdpThread::GetError (::WSAGetLastError ()));

		if (::closesocket (s) == SOCKET_ERROR) 
			TRACEF (CUdpThread::GetError (::WSAGetLastError ()));
		
		s = INVALID_SOCKET;
	}
}

void CRemoteHead::Disconnect (bool bShutdown)
{
	if (m_pConnect) {
		delete m_pConnect;
		m_pConnect = NULL;
	}

	m_state.m_bClosed = true;
	CloseSocket (m_socket);
	CHead::Disconnect (bShutdown);

	if (!bShutdown) {
		m_state.m_bShortTimeout = false;
		Connect ();
	}
}


bool CRemoteHead::Send (ULONG lCmd, ULONG lBatchID, const CString & strData, CPacket * pReply, DWORD dwTimeout)
{
	if (strData.Find (m_cBreak) != -1) {
		bool bResult = true;
		CStringArray v;

		ASSERT (pReply == NULL);
		Tokenize (strData, v, m_cBreak);

		for (int i = 0; i < v.GetSize (); i++) {
			CString str = v [i];

			//TRACEF (str);
			bResult &= Send (lCmd, lBatchID, (PUCHAR)(LPVOID)w2a (str), str.GetLength (), pReply, dwTimeout);
		}

		return bResult;
	}
	else {
		int nLen = strData.GetLength (); if (nLen > 512) TRACEF (strData);
		//ASSERT (strData.GetLength () <= 1024);
		return Send (lCmd, lBatchID, (PUCHAR)(LPVOID)w2a (strData), strData.GetLength (), pReply, dwTimeout);
	}
}

bool CRemoteHead::Send (ULONG lCmd, ULONG lBatchID, unsigned char * pData, ULONG lLen, CPacket * pReply, DWORD dwTimeout)
{
	bool bResult = false;

	if (m_state.m_bShortTimeout) 
		dwTimeout = BindTo ((DWORD)(dwTimeout / 6), (DWORD)(5 * 1000), (DWORD)(20 * 1000));

	if (m_socket != INVALID_SOCKET) {
		const double dTimeout = (double)dwTimeout / 1000.0;
		bool bTimeout = false, bCompleted = false;
		clock_t clock_tLast = clock (), clock_tNow = clock ();
		int nSend = 0;

		while (!bTimeout && !bCompleted) {
			double dDiff = (double)(clock_tNow - clock_tLast) / CLOCKS_PER_SEC;
			_FJ_SOCKET_MESSAGE_HEADER header;
			const ULONG lPacketLen = sizeof (header) + lLen;
			char * pBuffer = new char [lPacketLen];

			header.Cmd		= htonl (lCmd);
			header.BatchID	= htonl (lBatchID);
			header.Length	= htonl (lLen);

			memcpy (pBuffer, &header, sizeof (header));
			memcpy ((LPVOID)(pBuffer + sizeof (header)), pData, lLen);

			nSend = send (m_socket, pBuffer, lPacketLen, 0);

			delete [] pBuffer;

			if (nSend != SOCKET_ERROR) {
				#if defined( _DEBUG ) && defined( __TRACE_IPC__ )
				CString str, strData = (pData ? a2w ((const char *)pData) : _T ("[NULL]"));

				str.Format (_T ("CRemoteHead::Send:       [%d] %s [%d]: %s"),
					lBatchID,
					GetIpcCmdName (lCmd),
					lLen, 
					strData);
				TRACEF (str);
				#endif

				{ // for large packets (like a firmware update) we need to wait till the socket is ready to write again
					CTime tmStart = CTime::GetCurrentTime ();
					bool bTimeout = false;

					do {
						struct timeval timeout;
						fd_set write;

						memset (&write, 0, sizeof (write));
						write.fd_count = 1;
						write.fd_array [0] = m_socket;

						timeout.tv_sec = 1;
						timeout.tv_usec = 0;
						int nSelect = ::select (0, NULL, &write, NULL, &timeout);

						if (nSelect) {
							bCompleted = true;
							break;
						}

						CTimeSpan tm = CTime::GetCurrentTime () - tmStart;

						bTimeout = tm.GetTotalSeconds () > (LONG)ceil ((double)dwTimeout / 1000.0);
						::Sleep (0);
					} 
					while (!bTimeout);

					if (bTimeout) {
						TRACEF (GetUID () + _T (": send timed out"));
						Disconnect ();
					}
				}
			}

			{
				DWORD dwSleep = 0;

				if (nSend == SOCKET_ERROR) {
					switch (::WSAGetLastError ()) {
					case WSAEWOULDBLOCK:
						dwSleep = 100;
						break;
					}
				}

				::Sleep (dwSleep);
				bTimeout = (dDiff > dTimeout);
				clock_tNow = clock ();
			}
		}

		if (nSend == SOCKET_ERROR) {
			#if defined( _DEBUG )
			CString str;

			str.Format (_T ("CRemoteHead::Send:       [%d] %s [%d]"),
				lBatchID,
				GetIpcCmdName (lCmd),
				lLen);
			TRACEF (str);
			TRACEF (CUdpThread::GetError (::WSAGetLastError ()));
			#endif

			//TRACE_DIAG (CUdpThread::GetError (::WSAGetLastError ()));
		}
		else {
			if (!pReply) 
				bResult = true;
			else {
				const double dTimeout = (double)dwTimeout / 1000.0;
				bool bTimeout = false;
				clock_t clock_tLast = clock (), clock_tNow = clock ();
				LOCK (m_csReadSocket);

				while (!bTimeout) {
					double dDiff = (double)(clock_tNow - clock_tLast) / CLOCKS_PER_SEC;

					ASSERT (pReply);

					if (m_socket == INVALID_SOCKET) 
						break;

					if (ReadSocket (pReply)) {
						///*
						#if defined( _DEBUG ) && defined( __TRACE_IPC__ )
						CString str;

						str.Format (_T ("ReadSocket:              [%d] %s [%d]: %s"),
							(pReply->Header.BatchID),
							GetIpcCmdName ( (pReply->Header.Cmd)),
							 (pReply->Header.Length),
							a2w (pReply->Data));
						TRACEF (str);
						#endif
						//*/

						if (pReply->Header.BatchID == lBatchID) {
							bResult = true;
							m_nTimeoutStep = 0;
							break;
						}
					}

					{
						const double dIncrement = 6.0;
						int nTimeoutStep = min ((int)dIncrement, (int)floor (dTimeout / dIncrement));

						if (m_nTimeoutStep != nTimeoutStep) {
							m_nTimeoutStep = nTimeoutStep;

							while ( !::PostMessage (m_hControlWnd, WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)this)) 
								::Sleep(0);
						}
					}

					::Sleep (0);
					bTimeout = (dDiff > dTimeout);
					clock_tNow = clock ();
				}

				if (bTimeout) {
					TRACEF ("CRemoteHead::Send timed out");
					Disconnect ();
				}
			}
		}
	}

	return bResult;
}

bool CRemoteHead::ReadSocket (CPacket * pReply)
{
	bool bResult = false;

	if (m_socket != INVALID_SOCKET) {
		struct timeval timeout;
		fd_set read;

		memset (&read, 0, sizeof (read));
		read.fd_count = 1;
		read.fd_array [0] = m_socket;

		timeout.tv_sec = 1;
		timeout.tv_usec = 0;
		int nSelect = ::select (0, &read, NULL, NULL, &timeout);

		if (nSelect) {
			//_FJ_SOCKET_MESSAGE packet;
			CPacket packet;
			_FJ_SOCKET_MESSAGE_HEADER header;

			memset (&header, 0, sizeof (header));

			int nRecvHeader = ::recv (m_socket, (char *)&header, sizeof (header), 0);

			if (nRecvHeader != SOCKET_ERROR && nRecvHeader > 0) {
				header.Cmd		= ntohl (header.Cmd);
				header.BatchID	= ntohl (header.BatchID);
				header.Length	= ntohl (header.Length);

				memcpy (&packet.Header, &header, sizeof (header));
				
				if (!packet.Header.Length) {
					bResult = true;

					//{ CString str; str.Format (_T ("recv: [%d]"), nRecvHeader); TRACEF (str); } // TODO: rem
				}
				else {
					ULONG lRecvData = 0;

					//{ CString str; str.Format (_T ("Cmd: [%d] %s, BatchID: %d, Length: %d"), packet.Header.Cmd, GetIpcCmdName (packet.Header.Cmd), packet.Header.BatchID, packet.Header.Length); TRACEF (str); }

					if (packet.Header.Length > _FJ_SOCKET_BUFFER_SIZE) 
						packet.ReAlloc (packet.Header.Length);

					for (int i = 0; i < 10; i++) {
						int n = ::recv (m_socket, (char *)&packet.Data [lRecvData], packet.Header.Length - lRecvData, 0);

						if (n != SOCKET_ERROR && n > 0)
							lRecvData += n;

						if (lRecvData == packet.Header.Length)
							break;
						else {
							//TRACEF (a2w (packet.Data));
							::Sleep (50);
						}
					}

					//ULONG lRecvData = ::recv (m_socket, (char *)packet.Data, packet.Header.Length, 0);

					ASSERT ((ULONG)lRecvData == packet.Header.Length);

					if (lRecvData != SOCKET_ERROR && lRecvData > 0) {
						bResult = true;

						#if defined( _DEBUG ) && defined( __TRACE_IPC__ )
						{ CString str; str.Format (_T ("recv: [%d] %d"), nRecvHeader, lRecvData); TRACEF (str); } // TODO: rem
						#endif
					}
					else {
						TRACEF ("error");
						ASSERT (0);
					}
				}

				if (bResult) {
					#if defined( _DEBUG ) && defined( __TRACE_IPC__ )
					CString str;

					str.Format (_T ("CRemoteHead::ReadSocket: [%d] %s [%d]: %s"),
						packet.Header.BatchID,
						GetIpcCmdName (packet.Header.Cmd),
						packet.Header.Length,
						a2w (packet.Data));
					TRACEF (str);
					#endif
					
					ProcessRxData (&packet);

					if (pReply) 
						* pReply = packet;
				}
			}
			/* TODO: rem
			else {
				TRACEF ("error");
				ASSERT (0);
			}
			*/
		}
	}

	return bResult;
}

CString CRemoteHead::GetLocalAddress () const 
{
	CString str;

	if (m_socket != INVALID_SOCKET) {
		SOCKADDR_IN addr;

		memset (&addr, 0, sizeof(addr));
		int nLen = sizeof(addr);

		getsockname (m_socket, (SOCKADDR *)&addr, &nLen);
		str = inet_ntoa (addr.sin_addr);
	}

	return str;
}

void CRemoteHead::SetKeepAlive (bool bOn)
{
	if (bOn)
		Send (IPC_SET_STATUS_MODE, GetNextSequence(), _T ("STATUS_MODE=KEEP_ALIVE"));
	else
		Send (IPC_SET_STATUS_MODE, GetNextSequence(), _T ("STATUS_MODE=OFF"));

	Send (IPC_SET_STATUS_MODE, GetNextSequence(), _T ("STATUS_MODE=ON"));
}
