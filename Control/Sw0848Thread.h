#if !defined(AFX_Sw0848Thread_H__EAAE6A21_F4DF_4AFE_9497_365CD5E3EAC4__INCLUDED_)
#define AFX_Sw0848Thread_H__EAAE6A21_F4DF_4AFE_9497_365CD5E3EAC4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Sw0848Thread.h : header file
//

#include "OdbcDatabase.h"
#include "OdbcRecordset.h"
#include "Sw0848Dlg.h"

#define TM_POSTPHOTOCELL		(WM_APP+3)
#define TM_SW0848INIT			(WM_APP+4)

/////////////////////////////////////////////////////////////////////////////
// CSw0848Thread thread

namespace Sw0848Thread
{
	class CPair
	{
	public:
		CPair (const CString & strField = _T (""), const CString & strData = _T (""))
		:	m_strField (strField),
			m_strData (strData)
		{
		}

		CString m_strField;
		CString m_strData;
	};

	class CPostPhotocell
	{
	public:

		CPostPhotocell () 
		:	m_tm (CTime::GetCurrentTime ())
		{
		}

		CTime m_tm;
		CArray <CPair, CPair &> m_v;

	private:
		CPostPhotocell (const CPostPhotocell & rhs) { }									// no impl
		CPostPhotocell & operator = (const CPostPhotocell & rhs) { return * this;}		// no impl
	};

	class CSw0848Thread : public CWinThread
	{
		DECLARE_DYNCREATE(CSw0848Thread)
	protected:
		CSw0848Thread();           // protected constructor used by dynamic creation

	// Attributes
	public:
	// Operations
	public:

		static int FindField (const CString & str, CArray <CPair, CPair &> & v);

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CSw0848Thread)
		public:
		virtual BOOL InitInstance();
		virtual int ExitInstance();
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual ~CSw0848Thread();

		void Free ();

		FoxjetDatabase::COdbcDatabase * m_pdb;
		FoxjetDatabase::COdbcRecordset * m_prst;
		Sw0848Dlg::CSw0848Map m_map;

		// Generated message map functions
		//{{AFX_MSG(CSw0848Thread)
			// NOTE - the ClassWizard will add and remove member functions here.
		//}}AFX_MSG

		afx_msg void OnKillThread (WPARAM wParam, LPARAM lParam);
		afx_msg void OnPostPhotocell (WPARAM wParam, LPARAM lParam);
		afx_msg void OnInit (WPARAM wParam, LPARAM lParam);

		DECLARE_MESSAGE_MAP()
	};
}; // namespace Sw0848Thread

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sw0848Thread_H__EAAE6A21_F4DF_4AFE_9497_365CD5E3EAC4__INCLUDED_)
