// PreviewDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "control.h"
#include "PreviewDlg.h"
#include "Color.h"
#include "Registry.h"
#include "Coord.h"

#define SS_PREVIEW 100

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Color;
using namespace FoxjetCommon;
using namespace PreviewDlg;

static const CString strRegKey = CControlApp::GetRegKey () + _T ("\\CPreviewDlg");

/////////////////////////////////////////////////////////////////////////////
// CPreviewDlg dialog


CPreviewDlg::CPreviewDlg(COdbcDatabase & db, CProdLine * pLine, CWnd* pParent /*=NULL*/)
:	m_pTask (NULL),
	m_pBmp (NULL),
	m_bTracking (false),
	m_db (db),
	m_sizeDef (0, 0),
	m_ptDef (0, 0),
	m_dZoom (1.0),
	m_pLine (pLine),
	FoxjetCommon::CEliteDlg(CPreviewDlg::IDD, pParent)
{
	ASSERT (m_pLine);

	m_ptDef.x		= FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, ::strRegKey, _T ("x"), 0);
	m_ptDef.y		= FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, ::strRegKey, _T ("y"), 0);
	m_sizeDef.cx	= FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, ::strRegKey, _T ("cx"), 0);
	m_sizeDef.cy	= FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, ::strRegKey, _T ("cy"), 0);

	if (m_ptDef.x == 0 ||
		m_ptDef.y == 0 ||
		m_sizeDef.cx == 0 ||
		m_sizeDef.cy == 0)
	{
		if (pParent) {
			CRect rc (0, 0, 0, 0);

			pParent->GetWindowRect (&rc);
			m_ptDef = rc.TopLeft ();
			m_sizeDef = rc.Size ();
		}
	}
}

CPreviewDlg::~CPreviewDlg ()
{
	if (m_pBmp) {
		delete m_pBmp;
		m_pBmp = NULL;
	}

	if (m_pTask) {
		delete m_pTask;
		m_pTask = NULL;
	}
}

void CPreviewDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPreviewDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPreviewDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CPreviewDlg)
	ON_CBN_SELCHANGE(CB_PANEL, OnSelchangePanel)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_ZOOM, OnReleasedcaptureZoom)
	ON_BN_CLICKED(BTN_PRINT, OnPrint)
	//}}AFX_MSG_MAP
	ON_WM_HSCROLL()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPreviewDlg message handlers

BOOL CPreviewDlg::OnInitDialog() 
{
	DECLARETRACECOUNT (20);

	BeginWaitCursor ();

	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_PANEL);
	CSliderCtrl * pZoom = (CSliderCtrl *)GetDlgItem (IDC_ZOOM);
	UINT nPanels [] = 
	{
		IDS_FRONT,
		IDS_RIGHT,
		IDS_BACK,
		IDS_LEFT,
		IDS_TOP,
		IDS_BOTTOM,
	};
	LINESTRUCT line;
	CDC dcPreview;

	MARKTRACECOUNT ();

	ASSERT (pCB);
	ASSERT (pZoom);

	if (CWnd * p = GetDlgItem (BTN_PRINT)) 
		p->EnableWindow (theApp.LoginPermits (ID_CONFIGURE_PRINT));

	pZoom->SetRange (10, 100);
	pZoom->SetPos (FoxjetDatabase::GetProfileInt (::strRegKey, _T ("Zoom"), 100));

	m_nPanel = FoxjetDatabase::GetProfileInt (::strRegKey, _T ("Panel"), 0);

	for (int i = 0; i < ARRAYSIZE (nPanels); i++) {
		UINT nIndex = pCB->AddString (LoadString (nPanels [i]));

		pCB->SetItemData (nIndex, i);

		if (i == (int)m_nPanel)
			pCB->SetCurSel (i);
	}

	if (pCB->GetCurSel () == CB_ERR)
		pCB->SetCurSel (0);

	MARKTRACECOUNT ();

	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	MARKTRACECOUNT ();

	if (CWnd * p = GetDlgItem (IDC_PREVIEW)) {
		CRect rc;

		p->ShowWindow (SW_HIDE);
		p->GetWindowRect (&rc);
		ScreenToClient (&rc);
		BOOL bSetWindowPos = m_wndPreview.SetWindowPos (NULL, rc.left, rc.top,
			rc.Width (), rc.Height (), SWP_NOZORDER | SWP_SHOWWINDOW);
		ASSERT (bSetWindowPos);
	}

	MARKTRACECOUNT ();

	MARKTRACECOUNT ();

	VERIFY (dcPreview.CreateCompatibleDC (NULL));
	m_pTask = new CTaskPreview (m_task, dcPreview, m_pLine);

	MARKTRACECOUNT ();

	GetLineRecord (m_db, m_task.m_lLineID, line);
	
	MARKTRACECOUNT ();

	SetWindowText (line.m_strName + _T ("\\") + m_task.m_strName);

	if (m_sizeDef.cx > 0 && m_sizeDef.cy > 0) 
		SetWindowPos (NULL, m_ptDef.x, m_ptDef.y, m_sizeDef.cx, m_sizeDef.cy, SWP_NOZORDER | SWP_SHOWWINDOW);

	MARKTRACECOUNT ();

	OnSelchangePanel ();

	MARKTRACECOUNT ();
	
	m_wndPreview.SetScrollPos (SB_HORZ,
		FoxjetDatabase::GetProfileInt (CControlApp::GetRegKey () + _T ("\\CPreviewDlg"), _T ("SB_HORZ"), 0));
	m_wndPreview.SetScrollPos (SB_VERT,
		FoxjetDatabase::GetProfileInt (CControlApp::GetRegKey () + _T ("\\CPreviewDlg"), _T ("SB_VERT"), 0));

	OnHScroll (0, 0, NULL);

	EndWaitCursor ();

	//TRACECOUNTARRAY ();
	
	return TRUE; 
}

void CPreviewDlg::OnSelchangePanel() 
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_PANEL);

	ASSERT (pCB);
	m_nPanel = pCB->GetItemData (pCB->GetCurSel ());

	Invalidate ();
}

int CPreviewDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (FoxjetCommon::CEliteDlg::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	BOOL bCreate = m_wndPreview.Create (this, CRect ());

	ASSERT (bCreate);

	return 0;
}

void CPreviewDlg::OnOK()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_PANEL);
	CSliderCtrl * pZoom = (CSliderCtrl *)GetDlgItem (IDC_ZOOM);

	ASSERT (pZoom);
	ASSERT (pCB);

	m_nPanel = pCB->GetItemData (pCB->GetCurSel ());
	FoxjetDatabase::WriteProfileInt (CControlApp::GetRegKey () + _T ("\\CPreviewDlg"), _T ("Panel"), m_nPanel);
	FoxjetDatabase::WriteProfileInt (CControlApp::GetRegKey () + _T ("\\CPreviewDlg"), _T ("Zoom"), pZoom->GetPos ());
	FoxjetDatabase::WriteProfileInt (CControlApp::GetRegKey () + _T ("\\CPreviewDlg"), _T ("SB_HORZ"), m_wndPreview.GetScrollPos (SB_HORZ));
	FoxjetDatabase::WriteProfileInt (CControlApp::GetRegKey () + _T ("\\CPreviewDlg"), _T ("SB_VERT"), m_wndPreview.GetScrollPos (SB_VERT));

	FoxjetCommon::CEliteDlg::OnOK ();
}

void CPreviewDlg::OnSize(UINT nType, int cx, int cy) 
{
	CSize sizeMargin (0, 0);

	FoxjetCommon::CEliteDlg::OnSize(nType, cx, cy);

	if (CWnd * p = GetDlgItem (CB_PANEL)) {
		CRect rc;
		p->GetWindowRect (&rc);

		ScreenToClient (&rc);
		sizeMargin.cx = rc.left;
		sizeMargin.cy = rc.bottom;
	}
		
	if (m_wndPreview.m_hWnd) {
		CSize size (cx, cy);
		CRect rc;

		m_wndPreview.GetWindowRect (&rc);
		ScreenToClient (&rc);

		size.cx = cx - (sizeMargin.cx * 2);
		size.cy = cy - ((rc.top - sizeMargin.cy) * 6);
		m_wndPreview.SetWindowPos (NULL, 0, 0, size.cx, size.cy, SWP_NOZORDER | SWP_NOMOVE);
	}
}

void CPreviewDlg::OnDestroy() 
{
	CRect rc;

	GetWindowRect (rc);
	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strRegKey, _T ("x"), rc.left);
	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strRegKey, _T ("y"), rc.top);
	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strRegKey, _T ("cx"), rc.Width ());
	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strRegKey, _T ("cy"), rc.Height ());

	FoxjetCommon::CEliteDlg::OnDestroy();
}

void CPreviewDlg::Draw (CDC & dc)
{
	CDC dcMem;
	DIBSECTION ds;

	if (!m_pTask)
		return;

	VERIFY (dcMem.CreateCompatibleDC (&dc));
	ZeroMemory (&ds, sizeof (ds));

	if (m_pBmp) {
		m_pBmp->GetObject (sizeof (ds), &ds);
		
		CBitmap * pBmp = dcMem.SelectObject (m_pBmp);
		
		dc.FillSolidRect (m_wndPreview.GetPageRect (), rgbDarkGray);
		dc.BitBlt (0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, &dcMem, 0, 0, SRCCOPY);

		dcMem.SelectObject (pBmp);
	}
	else if (m_nPanel >= 0 && m_nPanel < ARRAYSIZE (m_pTask->m_bmp)) {
		BeginWaitCursor ();

		if (m_pTask->m_bmp [m_nPanel].m_hObject) {
			CDC dcTmp;

			m_pTask->m_bmp [m_nPanel].GetObject (sizeof (ds), &ds);

			int cx = (int)((double)ds.dsBm.bmWidth * m_dZoom);
			int cy = (int)((double)ds.dsBm.bmHeight * m_dZoom);

			if (m_bTracking) {
				dc.FillSolidRect (m_wndPreview.GetPageRect (), rgbWhite);
				dc.FrameRect (CRect (0, 0, cx, cy), &CBrush (rgbWhite));
			}
			else {
				VERIFY (dcTmp.CreateCompatibleDC (&dc));
				m_pBmp = new CBitmap ();
				VERIFY (m_pBmp->CreateCompatibleBitmap (&dc, cx, cy));

				CBitmap * pMem = dcMem.SelectObject (&m_pTask->m_bmp [m_nPanel]);
				CBitmap * pTmp = dcTmp.SelectObject (m_pBmp);

				dcTmp.FillSolidRect (m_wndPreview.GetPageRect (), rgbWhite);
				dcTmp.StretchBlt (0, 0, cx, cy, &dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, SRCCOPY);

				dc.FillSolidRect (m_wndPreview.GetPageRect (), rgbWhite);
				dc.BitBlt (0, 0, cx, cy, &dcTmp, 0, 0, SRCCOPY);

				dcMem.SelectObject (pMem);
				dcTmp.SelectObject (pTmp);
			}
		}

		EndWaitCursor ();
	}
}

void CPreviewDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CSliderCtrl * pZoom = (CSliderCtrl *)GetDlgItem (IDC_ZOOM);
	CString str;
	CWnd * pCapture = GetCapture ();

	m_bTracking = pCapture == pZoom ? true : false;

	ASSERT (pZoom);
	double dZoom = (double)pZoom->GetPos () / 100.0;

	str.Format (_T ("%d %%"), pZoom->GetPos ());
	SetDlgItemText (LBL_ZOOM, str);

	if (m_dZoom != dZoom) {
		m_dZoom = dZoom;
		Invalidate ();
	}
}

void CPreviewDlg::Invalidate()
{
	if (m_pBmp) {
		delete m_pBmp;
		m_pBmp = NULL;
	}

	if (m_pTask) {
		if (m_nPanel >= 0 && m_nPanel < ARRAYSIZE (m_pTask->m_bmp)) {
			DIBSECTION ds;

			ZeroMemory (&ds, sizeof (ds));
			
			if (m_pTask->m_bmp [m_nPanel].m_hObject)
				m_pTask->m_bmp [m_nPanel].GetObject (sizeof (ds), &ds);

			int cx = max (0, (int)((double)ds.dsBm.bmWidth * m_dZoom));
			int cy = max (0, (int)((double)ds.dsBm.bmHeight * m_dZoom));

			m_wndPreview.SetScrollSizes (MM_TEXT, CSize (cx, cy));
		}
	}

	m_wndPreview.Invalidate ();
	m_wndPreview.RedrawWindow ();
}

void CPreviewDlg::OnReleasedcaptureZoom(NMHDR* pNMHDR, LRESULT* pResult) 
{
	m_bTracking = false;
	Invalidate ();

	if (pResult)
		* pResult = 0;
}

void CPreviewDlg::OnPrint() 
{
	PRINTDLG pd;
	DOCINFO docinfo;
	CDC dc;
	CSize size (0, 0), res (0, 0), screen (0, 0);
	TCHAR szTitle [256] = { 0 };
	CFont fnt;
	UINT nPanels [] = 
	{
		IDS_FRONT,
		IDS_RIGHT,
		IDS_BACK,
		IDS_LEFT,
		IDS_TOP,
		IDS_BOTTOM,
	};

	GetWindowText (szTitle, ARRAYSIZE (szTitle));

	{
		const CString strFile = FoxjetDatabase::GetHomeDir () + _T("\\setup.pd");
		int nPages = 1;
		DWORD dwSize;
		
		ZeroMemory (&pd, sizeof (pd));
		pd.lStructSize = sizeof (pd);

		if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
			if (fread (&dwSize, sizeof (dwSize), 1, fp) == 1) {
				pd.hDevMode = ::GlobalAlloc (GHND, dwSize);
				
				if (LPVOID p = ::GlobalLock (pd.hDevMode)) {
					fread (p, dwSize, 1, fp);
					::GlobalUnlock (pd.hDevMode);
				}
			}

			if (fread (&dwSize, sizeof (dwSize), 1, fp) == 1) {
				pd.hDevNames = ::GlobalAlloc (GHND, dwSize);
				
				if (LPVOID p = ::GlobalLock (pd.hDevNames)) {
					fread (p, dwSize, 1, fp);
					::GlobalUnlock (pd.hDevNames);
				}
			}

			fclose (fp);
		}

		for (int i = 0; i < ARRAYSIZE (m_pTask->m_bmp); i++)
			nPages += (m_pTask->m_bmp [i].m_hObject) ? 1 : 0;

		pd.Flags		= PD_RETURNDC | PD_ALLPAGES | PD_NOPAGENUMS | PD_HIDEPRINTTOFILE | PD_NOSELECTION | PD_USEDEVMODECOPIES;
		pd.nFromPage	= 1; 
		pd.nToPage		= nPages; 
		pd.nMinPage		= 1; 
		pd.nMaxPage		= nPages; 
		pd.nCopies		= 1; 

		if (!::PrintDlg (&pd)) 
			return;

		if (FILE * fp = _tfopen (strFile, _T ("wb"))) {
			if (LPVOID p = ::GlobalLock (pd.hDevMode)) {
				dwSize = ::GlobalSize (pd.hDevMode);
				fwrite (&dwSize, sizeof (dwSize), 1, fp);
				fwrite (p, dwSize, 1, fp);
				::GlobalUnlock (pd.hDevMode);
			}
			if (LPVOID p = ::GlobalLock (pd.hDevNames)) {
				dwSize = ::GlobalSize (pd.hDevNames);
				fwrite (&dwSize, sizeof (dwSize), 1, fp);
				fwrite (p, dwSize, 1, fp);
				::GlobalUnlock (pd.hDevNames);
			}

			fclose (fp);
		}
	}

	HDC hDC = pd.hDC;

	{
		if (pd.hDevMode)
			::GlobalFree (pd.hDevMode);

		if (pd.hDevNames)
			::GlobalFree (pd.hDevNames);
	}

	dc.Attach (hDC);

	memset(&docinfo, 0, sizeof(docinfo));
	docinfo.cbSize = sizeof(docinfo);
	docinfo.lpszDocName = szTitle;

	if (dc.StartDoc (&docinfo) < 0) {
		MsgBox (* this, _T ("StartDoc failed"));
		return;
	}

	size.cx = ::GetDeviceCaps (hDC, HORZRES);
	size.cy = ::GetDeviceCaps (hDC, VERTRES);
	res.cx	= ::GetDeviceCaps (hDC, LOGPIXELSX);
	res.cy	= ::GetDeviceCaps (hDC, LOGPIXELSY);

	screen.cx	= GetDC ()->GetDeviceCaps (LOGPIXELSX);
	screen.cy	= GetDC ()->GetDeviceCaps (LOGPIXELSY);

	double dZoom [] = 
	{
		(double)res.cx / (double)screen.cx,
		(double)res.cy / (double)screen.cy,
	};

	fnt.CreateFont ((int)(18.0 * dZoom [1]), 0, 0, 0, FW_NORMAL, FALSE, 0, 0,
		DEFAULT_CHARSET, OUT_CHARACTER_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("Verdana"));

	CTime tm = CTime::GetCurrentTime ();

	CFont * pFont = dc.SelectObject (&fnt);

	for (int i = 0; i < ARRAYSIZE (m_pTask->m_bmp); i++) {
		if (m_pTask->m_bmp [i].m_hObject) {
			CString str = szTitle + CString (_T (" [")) + LoadString (nPanels [i]) + CString (_T ("] - ")) + tm.GetCurrentTime ().Format (_T ("%c"));
			CDC dcMem;
			DIBSECTION ds;
			double dFactor = 1.0;

			VERIFY (dcMem.CreateCompatibleDC (&dc));
			ZeroMemory (&ds, sizeof (ds));
			m_pTask->m_bmp [i].GetObject (sizeof (ds), &ds);

			CBitmap * pMem = dcMem.SelectObject (&m_pTask->m_bmp [i]);

			if (dc.StartPage () < 0) {
				MsgBox (* this, _T ("StartPage failed"));
				dc.AbortDoc();
				return;
			}

			int cx = (int)((double)ds.dsBm.bmWidth * dZoom [0]);
			int cy = (int)((double)ds.dsBm.bmHeight * dZoom [1]);

			if ((cx > size.cx) || (cy > size.cy)) {
				double d [] = 
				{
					(double)size.cx / (double)cx,
					(double)size.cy / (double)cy,
				};

				dFactor = min (d [0], d [1]);
			}

			cx = (int)((double)cx * dFactor);
			cy = (int)((double)cy * dFactor);

			dc.StretchBlt (0, 0, cx, cy, &dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, SRCCOPY);

			CSize extent = dc.GetTextExtent (str);
			dc.TextOut ((size.cx - extent.cx) / 2, 0, str);

			dcMem.SelectObject (pMem);
			dc.EndPage();
		}
	}

	dc.EndDoc();
	dc.SelectObject (pFont);
}



