#if !defined(AFX_BAXTERCONFIGDLG_H__2AE2A021_FBD5_45C1_9A8B_15016BC2E49C__INCLUDED_)
#define AFX_BAXTERCONFIGDLG_H__2AE2A021_FBD5_45C1_9A8B_15016BC2E49C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BaxterConfigDlg.h : header file
//

#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CBaxterConfigDlg dialog

class CBaxterConfigDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	void Save();
	void Load();
	CBaxterConfigDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBaxterConfigDlg)
	BOOL	m_bVerify;
	CString	m_strOuputFormat;
	int		m_nTaskLen;
	//int	m_nOvertureMax;
	int		m_nOvertureLen1;
	int		m_nOvertureLen2;
	//int	m_nOvertureLen3;
	int		m_nPrimaryMax;
	int		m_nSecondaryMax;
	int		m_nMatchLen;
	int		m_nUserLenOpt;
	//}}AFX_DATA


	int GetOverture3Len (const CString str);
	int GetOverture3Len (int nOvertureLen);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBaxterConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CBitmap m_bmpFormat;

	// Generated message map functions
	//{{AFX_MSG(CBaxterConfigDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnFormat();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BAXTERCONFIGDLG_H__2AE2A021_FBD5_45C1_9A8B_15016BC2E49C__INCLUDED_)
