// ServerThread.cpp : implementation file
//

#include "stdafx.h"
#include <process.h>
#include <shlwapi.h>
#include "control.h"
#include "ServerThread.h"
#include "Debug.h"
#include "TemplExt.h"
#include "Utils.h"
#include "Host.h"
#include "ControlView.h"
#include "Parse.h"
#include "Database.h"
#include "ProdLine.h"
#include "UserElement.h"
#include "BitmapElement.h"
#include "CountElement.h"
#include "Registry.h"
#include "StartupDlg.h"
#include <vector>
#include <string>
#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace std;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

class COdbcDatabaseLocal : public FoxjetDatabase::COdbcDatabase
{
public:
	COdbcDatabaseLocal (LPCTSTR lpszFile, ULONG lLine)
	:	COdbcDatabase (lpszFile, lLine)
	{
		COdbcDatabase::OpenDB (this, theApp.GetDSN ());
	}

	virtual ~COdbcDatabaseLocal  ()
	{
		COdbcDatabase::CloseDB (this);
	}
};

/////////////////////////////////////////////////////////////////////////////
// CServerThread

IMPLEMENT_DYNCREATE(CServerThread, CUdpThread)

CServerThread::CServerThread()
:	m_sockClient (INVALID_SOCKET),
	m_pdb (NULL),
	m_tmDatabase (CTime::GetCurrentTime ()),
	m_nDbTimeout (theApp.m_nDbTimeout)
{
}

CServerThread::~CServerThread()
{
}

UINT CServerThread::GetPort () const 
{ 
	return BW_HOST_TCP_PORT; 
}

UINT CServerThread::GetSockType () const 
{ 
	return SOCK_STREAM; 
}

BOOL CServerThread::InitInstance()
{
	if (!CUdpThread::InitInstance ())
		return FALSE;

	return TRUE;
}

int CServerThread::ExitInstance()
{
	return CUdpThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CServerThread, CUdpThread)
	//{{AFX_MSG_MAP(CServerThread)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_THREAD_MESSAGE (TM_KILLTHREAD, OnKillThread)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CServerThread message handlers

CString CServerThread::GetLineParam (const CStringArray & v, int nIndex, CControlView * pView)
{
	CString strResult;
	bool bUseFirst = false;

	if (v.GetSize () > nIndex) {
		CString strLine = v [nIndex];
	
		strLine.Trim ();

		if (pView) {
			if (CProdLine * p = pView->GetDocument ()->GetProductionLine (strLine))
				strResult = p->GetName ();
			else
				bUseFirst = true;
		}
		else {
			COdbcDatabaseLocal db (_T (__FILE__), __LINE__);

			if (FoxjetDatabase::GetLineID (db, strLine, GetPrinterID (db)) != FoxjetDatabase::NOTFOUND)
				strResult = strLine;
			else
				bUseFirst = true;
		}
	}
	else 
		bUseFirst = true;

	if (bUseFirst) {
		LINESTRUCT line;

		if (pView) {
			if (CProdLine * p = pView->GetDocument ()->GetLine (0))
				strResult = p->GetName ();
		}
		else {
			COdbcDatabaseLocal db (_T (__FILE__), __LINE__);

			GetFirstLineRecord (db, line, GetPrinterID (db));
			strResult = line.m_strName;
		}
	}

	return strResult;
}

CString CServerThread::GetTasks (const CStringArray & vCmd)
{
	CStringArray v;
	CArray <TASKSTRUCT *, TASKSTRUCT *> vTasks;
	const CString strLine = GetLineParam (vCmd);
	COdbcDatabaseLocal db (_T (__FILE__), __LINE__);

	v.Add (BW_HOST_PACKET_GET_TASKS);
	v.Add (strLine);

	ULONG lLineID = FoxjetDatabase::GetLineID (db, strLine, GetPrinterID (db));

	GetTaskRecords (db, lLineID, vTasks);

	for (int nTable = 0; nTable < vTasks.GetSize (); nTable++)
		v.Add (vTasks [nTable]->m_strName);

	Free (vTasks);

	return ToString (v);
}

CString CServerThread::GetLines (const CStringArray & vCmd)
{
	CStringArray v;

	v.Add (BW_HOST_PACKET_GET_LINES);

	if (CControlDoc * pDoc = (CControlDoc *)theApp.GetActiveDocument ())
		pDoc->GetProductLines (v);
	else {
		CArray <LINESTRUCT, LINESTRUCT &> vLines;
		COdbcDatabaseLocal db (_T (__FILE__), __LINE__);

		GetPrinterLines (db, GetPrinterID (db), vLines); //GetLineRecords(db, vLines);

		for (int nTable = 0; nTable < vLines.GetSize (); nTable++)
			v.Add (vLines [nTable].m_strName);
	}

	return ToString (v);
}

CString CServerThread::GetLineID (const CStringArray & vCmd)
{
	CStringArray v;
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CString strID = _T ("-1");
	COdbcDatabaseLocal db (_T (__FILE__), __LINE__);

	GetPrinterLines (db, GetPrinterID (db), vLines); //GetLineRecords(db, vLines);

	v.Add (BW_HOST_PACKET_GET_LINE_ID);

	if (vCmd.GetSize () >= 2) {
		CString strLine = vCmd [1];

		v.Add (strLine);

		for (int nTable = 0; nTable < vLines.GetSize (); nTable++) {
			if (!vLines [nTable].m_strName.CompareNoCase (strLine)) {
				strID = FoxjetDatabase::ToString (vLines [nTable].m_lID);
				break;
			}
		}
	}

	v.Add (strID);

	return ToString (v);
}

CString CServerThread::RegExport (const CStringArray & vCmd)
{
	CStringArray v;
	HKEY hKey = NULL;
	const CString strFile = FoxjetDatabase::GetHomeDir () + _T ("\\FoxJet.registry");

	v.Add (BW_HOST_PACKET_REGISTRY_EXPORT);

	if (::RegCreateKey (HKEY_CURRENT_USER, _T ("Software\\FoxJet"), &hKey) == ERROR_SUCCESS) {
		::DeleteFile (strFile);
		VERIFY (FoxjetDatabase::EnablePrivilege (SE_BACKUP_NAME));
		LRESULT lResult = ::RegSaveKey (hKey, strFile, NULL);
		TRACEF (FormatMessage (lResult));
		::RegCloseKey (hKey);
	}

	const DWORD dwFileSize = FoxjetDatabase::GetFileSize (strFile);

	if (FILE * fpData = _tfopen (strFile, _T ("rb"))) {
		int nMIME = (int)(dwFileSize * 2);
		BYTE * pMIME = new BYTE [nMIME];
		BYTE * p = new BYTE [dwFileSize];

		memset (p, 0, dwFileSize);
		fread (p, dwFileSize, 1, fpData);
		fclose (fpData);

		memset (pMIME, nMIME, 0);
		int nEncoded = EncodeBase64 (p, dwFileSize, pMIME, nMIME);
		pMIME [nEncoded] = 0;
		
		v.Add (CString (pMIME));

		/*
		{
			if (FILE * fp = _tfopen (_T ("C:\\Foxjet\\export.registry"), _T ("w"))) {
				CString str = CString (pMIME);
				fwrite (w2a (str), str.GetLength (), 1, fp);
				fclose (fp);
			}
		}
		*/

		/*
		#ifdef _DEBUG
		{
			CString strMIME = pMIME;

			strMIME.Replace (_T ("\n"), _T (""));
			int n = strMIME.GetLength ();
			BYTE * pDecode = new BYTE [n];
			n = DecodeBase64 ((unsigned char *)(LPSTR)w2a (strMIME), pDecode, n);

			if (FILE * fp = _tfopen (strFile + _T (".decoded"), _T ("wb"))) {
				fwrite (pDecode, n, 1, fp);
				fclose (fp);
			}

			delete [] pDecode;
		}
		#endif
		*/

		delete [] p;
		delete [] pMIME;

	}

	VERIFY (::DeleteFile (strFile));

	return ToString (v);
}

CString CServerThread::RegImport (const CStringArray & vCmd)
{
	CStringArray v;
	bool bResult = false;
	const CString strFile = FoxjetDatabase::GetHomeDir () + _T ("\\FoxJet.registry");
	int nResult = 0;

	::DeleteFile (strFile);

	v.Add (BW_HOST_PACKET_REGISTRY_IMPORT);

	if (vCmd.GetSize () >= 2) {
		if (!vCmd [0].CompareNoCase (BW_HOST_PACKET_REGISTRY_IMPORT)) {
			CString strMIME = vCmd [1];

			strMIME.Replace (_T ("\n"), _T (""));
			strMIME.Replace (_T ("\r"), _T (""));
			int n = strMIME.GetLength ();
			BYTE * p = new BYTE [n];
			n = DecodeBase64 ((unsigned char *)(LPSTR)w2a (strMIME), p, n);

			if (FILE * fp = _tfopen (strFile, _T ("wb"))) {
				fwrite (p, n, 1, fp);
				fclose (fp);
			}

			delete [] p;

			if (::GetFileAttributes (strFile) != -1) {
				HKEY hKey = NULL;
				CString strKey = _T ("Software\\FoxJet");

				VERIFY (EnablePrivilege (SE_RESTORE_NAME));
				VERIFY (EnablePrivilege (SE_BACKUP_NAME));

				LONG lResult = ::SHDeleteKey (HKEY_CURRENT_USER, strKey);

				if (lResult != ERROR_SUCCESS) 
					v.Add (_T ("SHDeleteKey:\n") + FormatMessage (lResult));
				else {
					if (::RegCreateKey (HKEY_CURRENT_USER, strKey, &hKey) != ERROR_SUCCESS) 
						v.Add (_T ("RegCreateKey:\n") + FormatMessage (lResult));
					else {
						lResult = ::RegRestoreKey (hKey, strFile, 0);

						if (lResult != ERROR_SUCCESS) 
							v.Add (_T ("RegRestoreKey:\n") + FormatMessage (lResult));
						
						nResult = lResult == ERROR_SUCCESS ? 1 : 0;
						::RegCloseKey (hKey);
					}
				}
			}
		}
	}

	::DeleteFile (strFile);

	v.InsertAt (1, ToString (nResult));

	return ToString (v);
}

int CServerThread::GetXmlTagIndex (const CString & strXML, const CString & strTag)
{
	CLongArray vLeft, vRight;

	for (int i = 0; i < strXML.GetLength (); i++) {
		TCHAR c = strXML [i];

		switch (c) {
		case '<':	vLeft.Add (i);	break;
		case '>':	vRight.Add (i);	break;
		}

		if (c == '>') {
			if (vLeft.GetSize () > 0 && vRight.GetSize () > 0) {
				int nStart	= vLeft [vLeft.GetSize () - 1];
				int nEnd	= vRight [vRight.GetSize () - 1];
				int nLen = nEnd - nStart + 1;

				if ((nStart >= 0 && nStart < strXML.GetLength ()) &&
					(nEnd >= 0 && nEnd < strXML.GetLength ()) &&
					nLen > 0)
				{
					CString str = strXML.Mid (nStart, nLen);
					TCHAR * psz = new TCHAR [str.GetLength ()];

					memset (psz, 0, str.GetLength ());
					str.Replace (_T ("<"), _T (""));
					str.Replace (_T (">"), _T (""));

					if (_stscanf (str, _T ("%s"), psz) == 1)
						str = psz;

					delete [] psz;

					str.Trim ();

					if (!str.CompareNoCase (strTag))
						return i;
				}
			}
		}
	}

	return -1;
}

CString CServerThread::GetXmlTag (const CString & strInput, int nTag)
{
	CString str (strInput);

	if (LPCTSTR p = str.GetBuffer (str.GetLength ())) {
		CLongArray vLeft, vRight;

		for (int i = 0; i < str.GetLength (); i++) {
			switch (p [i]) {
			case '<': vLeft.Add (i);	break;
			case '>': vRight.Add (i);	break;
			}

			if (vLeft.GetSize () > 1 && vRight.GetSize () > 1) {
				if (vLeft.GetSize () > nTag && vRight.GetSize () > nTag) {
					int nIndex = vLeft [nTag];
					int nLen = vRight [nTag] - nIndex;

					if (nIndex >= 0 && nIndex < str.GetLength () && nLen > 0 && ((nIndex + nLen) < str.GetLength ())) {
						CString strTag = str.Mid (nIndex, nLen + 1);

						for (int j = 0; j < strTag.GetLength (); j++) {
							if (_istspace (strTag [j])) {
								if ((j + 1) < strTag.GetLength ()) {
									strTag = strTag.Left (j + 1);
									break;
								}
								else {
									strTag = strTag.Left (j);
									break;
								}
							}
						}

						strTag.Replace (_T ("<"), _T (""));
						strTag.Replace (_T (">"), _T (""));
						strTag.Trim ();

						return strTag;
					}
				}
			}
		}
	}

	return _T ("");
}

CString CServerThread::UnformatXML (CString str)
{
	for (int i = 0; i < 127; i++) {
		CString strChar;

		strChar.Format (_T ("&#%02d;"), i);
		str.Replace (strChar, CString ((TCHAR)i));
	}

	return str;
}

CString CServerThread::ProcessCommand (const CString & str, CControlView * pView)
{
	if (str.GetLength () > 1) { //4.10.0923.014
		TCHAR c = str [0];
		CProdLine * pLine = NULL;

		switch (c) {
		case 'a': case 'A': pLine = pView->GetDocument ()->GetLine (0); break;
		case 'b': case 'B': pLine = pView->GetDocument ()->GetLine (1); break;
		}				

		if (pLine) {
			CStringArray v;

			v.Add (BW_HOST_PACKET_SETSERIAL);
			v.Add (pLine->GetName ());
			v.Add (str.Right (str.GetLength () - 1));

			return CServerThread::ProcessCommand (ToString (v), pView);
		}
	}

	{
		CString strLower = str;

		strLower.MakeLower ();

		if (strLower.Find (_T ("<?xml version")) != -1 && strLower.Find (_T ("</xmlscript>")) != -1) {
			const CString strStart = _T ("<![CDATA[");
			const CString strEnd = _T ("]]>");
			int nStart = str.Find (strStart);
			int nEnd = str.Find (strEnd);

			if (nStart != -1 && nEnd != -1) {
				CStringArray v;
				CString strCData = str.Mid (nStart + strStart.GetLength (), nEnd - nStart - (strStart.GetLength () + strEnd.GetLength ()));
				int nDataIndex = 0;

				strCData.Replace (_T ("\n"), _T (","));
				strCData.Replace (_T ("\r"), _T (","));

				Tokenize (strCData, v, ',');

				for (int nTable = 0; nTable < v.GetSize (); nTable++) {
					CString strData = Extract (v [nTable], _T ("\""), _T ("\""));

					if (strData.GetLength ()) {
						nDataIndex = nTable;
						break;
					}
				}

				CStringArray vCmd;
				
				vCmd.Add (BW_HOST_PACKET_SET_USER_ELEMENTS);

				if (pView->GetDocument ()->GetSelectedLine ())
					vCmd.Add (pView->GetDocument ()->GetSelectedLine ()->m_LineRec.m_strName);
				else
					vCmd.Add (_T (""));

				for (int nTable = 0; nTable < nDataIndex; nTable++) {
					CString strPrompt = v [nTable];
					CString strData = Extract (v [nTable + nDataIndex], _T ("\""), _T ("\""));

					strPrompt.TrimLeft ();
					strPrompt.TrimRight ();

					vCmd.Add (strPrompt);
					vCmd.Add (strData);

					TRACEF (strPrompt + _T (" --> \"") + strData + _T ("\""));
				}

				{
					CString strResult;
					ULONG lResult = pView->OnSocketSetUserElements ((WPARAM)&vCmd, (LPARAM)&strResult);

					TRACEF (ToString (lResult));
					theApp.SetSocketSetUserElements (true);

					return strResult;
				}
			}

			return _T ("{xml error}");
		}
		else if (strLower.Find (_T ("<?xml")) != -1) {
			const CString strBegin = CServerThread::GetXmlTag (str, 1);
			const CString strEnd = _T ("/") + strBegin;

			int nBegin	= CServerThread::GetXmlTagIndex (str, strBegin);
			int nEnd	= CServerThread::GetXmlTagIndex (str, strEnd);

			bool bEnd = (nBegin != -1 && nEnd != -1) ? true : false;

			if (bEnd) {
				int nParsed = pView->SendMessage (WM_XMLDATA, (WPARAM)new CString (str));

				return _T ("{xml parsed: ") + ToString (nParsed) + _T ("}");
			}

			return _T ("{xml error}");
		}
	}

	CStringArray v;
	struct
	{
		LPCTSTR m_lpsz;
		UINT	m_nMsgID;
	} static const map [] = 
	{
		{ BW_HOST_PACKET_START_TASK,			WM_SOCKET_START_TASK,				},
		{ BW_HOST_PACKET_START_TASK_DATABASE,	WM_SOCKET_START_TASK_DATABASE,		},
		{ BW_HOST_PACKET_LOAD_TASK,				WM_SOCKET_LOAD_TASK,				},
		{ BW_HOST_PACKET_STOP_TASK,				WM_SOCKET_STOP_TASK,				},
		{ BW_HOST_PACKET_IDLE_TASK,				WM_SOCKET_IDLE_TASK,				},
		{ BW_HOST_PACKET_RESUME_TASK,			WM_SOCKET_RESUME_TASK,				},
		{ BW_HOST_PACKET_GET_USER_ELEMENTS,		WM_SOCKET_GET_USER_ELEMENTS,		},
		{ BW_HOST_PACKET_SET_USER_ELEMENTS,		WM_SOCKET_SET_USER_ELEMENTS,		},
		{ BW_HOST_PACKET_SET_ERROR_SOCKET,		WM_SOCKET_SET_ERROR_SOCKET,			},
		{ BW_HOST_PACKET_GET_CURRENT_TASK,		WM_SOCKET_GET_CURRENT_TASK,			},
		{ BW_HOST_PACKET_GET_CURRENT_STATE,		WM_SOCKET_GET_CURRENT_STATE,		},
		{ BW_HOST_PACKET_GET_COUNT,				WM_SOCKET_GET_COUNT,				},
		{ BW_HOST_PACKET_SET_COUNT,				WM_SOCKET_SET_COUNT,				},			
		{ BW_HOST_PACKET_GET_COUNT_MAX,			WM_SOCKET_GET_COUNT_MAX,			},
		{ BW_HOST_PACKET_SET_COUNT_MAX,			WM_SOCKET_SET_COUNT_MAX,			},			
		{ BW_HOST_PACKET_CLOSESOCKET,			WM_SOCKET_CLOSE,					},		
		{ BW_HOST_PACKET_GET_HEADS,				WM_SOCKET_GET_HEADS,				},	
		{ BW_HOST_PACKET_GET_ELEMENTS,			WM_SOCKET_GET_ELEMENTS,				},	
		{ BW_HOST_PACKET_DELETE_ELEMENT,		WM_SOCKET_DELETE_ELEMENT,			},	
		{ BW_HOST_PACKET_ADD_ELEMENT,			WM_SOCKET_ADD_ELEMENT,				},	
		{ BW_HOST_PACKET_UPDATE_ELEMENT,		WM_SOCKET_UPDATE_ELEMENT,			},	
		{ BW_HOST_PACKET_DB_BUILD_STRINGS,		WM_SOCKET_DB_BUILD_STRINGS			},
		{ BW_HOST_PACKET_DB_GET_NEXT_STRING,	WM_SOCKET_DB_GET_NEXT_STRING		},
		{ BW_HOST_PACKET_DB_IS_READY,			WM_SOCKET_DB_IS_READY				},
		{ BW_HOST_PACKET_DB_BEGIN_UPDATE,		WM_SOCKET_DB_BEGIN_UPDATE			},
		{ BW_HOST_PACKET_DB_END_UPDATE,			WM_SOCKET_DB_END_UPDATE				},
		{ BW_HOST_PACKET_REFRESH_PREVIEW,		WM_SOCKET_REFRESH_PREVIEW			},
		{ BW_HOST_PACKET_SETSERIAL,				WM_HOST_PACKET_SETSERIAL			},
		{ BW_HOST_PACKET_GETSERIAL,				WM_HOST_PACKET_GETSERIAL			},
		{ BW_HOST_PACKET_GET_STROBE,			WM_HOST_PACKET_GET_STROBE,			},
		{ BW_HOST_INK_CODE,						WM_HOST_RECIEVE_INK_CODE,			},
	};
	const CString strCmd = Tokenize (str, v);
	CString strResponse = "{" + strCmd + ", Unknown command}";

	if (CUdpThread::CheckForFlush (str))
		return _T ("{Flush}");

	for (int nTable = 0; nTable < v.GetSize (); nTable++) 
		v.SetAt (nTable, UnformatString (v [nTable]));

	{
		DWORD dw = 0;
		
		LRESULT lSendMessageTimeout = ::SendMessageTimeout (pView->m_hWnd, WM_SOCKET_CONNECT_DB, 0, 0, SMTO_BLOCK | SMTO_ABORTIFHUNG, 10000, &dw);

		if (!lSendMessageTimeout) {
			CString str;

			str.Format (_T ("{WM_SOCKET_CONNECT_DB timeout, SendMessageTimeout: %d, dw: %d, %s}"), lSendMessageTimeout, dw, FormatMessage (::GetLastError ()));
			return str;
		}
	}

	if (!strCmd.CompareNoCase (BW_HOST_PACKET_GET_TASKS))				strResponse = GetTasks (v);
	else if (!strCmd.CompareNoCase (BW_HOST_PACKET_GET_LINES))			strResponse = GetLines (v);
	else if (!strCmd.CompareNoCase (BW_HOST_PACKET_GET_LINE_ID))		strResponse = GetLineID (v);
	else if (!strCmd.CompareNoCase (BW_HOST_PACKET_REGISTRY_EXPORT))	strResponse = RegExport (v);
	else if (!strCmd.CompareNoCase (BW_HOST_PACKET_REGISTRY_IMPORT))	strResponse = RegImport (v);
	else {
		for (int nTable = 0; nTable < ARRAYSIZE (map); nTable++) {
			if (!strCmd.CompareNoCase (map [nTable].m_lpsz)) {
				ASSERT (pView);
				CString strResult;
				DWORD dw = 0;

				dw = ::SendMessage (pView->m_hWnd, map [nTable].m_nMsgID, (WPARAM)&v, (LPARAM)&strResult);
				/*
				LRESULT lSendMessageTimeout = ::SendMessageTimeout (pView->m_hWnd, map [nTable].m_nMsgID, (WPARAM)&v, (LPARAM)&strResult, SMTO_BLOCK | SMTO_ABORTIFHUNG, 10000, &dw);

				if (!lSendMessageTimeout) {
					CString str;

					str.Format (_T ("{%s timeout, SendMessageTimeout: %d, dw: %d, %s}"), strCmd, lSendMessageTimeout, dw, FormatMessage (::GetLastError ()));
					return str;
				}
				*/

				strResponse = strResult;
				break;
			}
		}
	}

	TRACEF (strResponse);
//	TRACEF (str);
//	TRACEF ("\t" + strResponse);

	return strResponse;
}


CString CServerThread::OnCommand (const CString & str, const SOCKADDR_IN * paddr)
{
	CString strResult;
	CStringArray v;
	const CString strCmd = Tokenize (str, v);

	if (m_pView) 
		strResult = ProcessCommand (str, m_pView);

	if (!strCmd.CompareNoCase (BW_HOST_PACKET_CLOSESOCKET)) 
		CUdpThread::Close (m_sockClient);

	return strResult;
}

bool CServerThread::Open ()
{
	bool bResult = false;

	if (GetSockType () == SOCK_DGRAM)
		return CUdpThread::Open ();

	TRACEF (GetRuntimeClass ()->m_lpszClassName);

	if (CUdpThread::Open ()) {
		if (::listen (m_sock, SOMAXCONN) == 0) {
			#ifdef _DEBUG
			CString str;

			str.Format (_T ("listen: %s %s, %d"),
				GetLocalAddress (m_sock),
				GetSockType () == SOCK_STREAM ? _T ("SOCK_STREAM") : _T ("SOCK_DGRAM"),
				GetPort ());
			TRACEF (str);
			#endif
			
			bResult = true;
		}
		else 
			Close ();
	}

	if (!bResult) {
		DWORD dw;
		CString * pstr = new CString ();

		pstr->Format (LoadString (IDS_OPENSOCKETFAILED), GetPort (), GetError (::WSAGetLastError ()));
		
		TRACEF (* pstr);
		::SendMessageTimeout (HWND_BROADCAST, WM_DISPLAY_MSG, (WPARAM)pstr, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 100, &dw);
	}

	return bResult;
}

void CServerThread::Close ()
{
	TRACEF ("CServerThread::Close");
	CUdpThread::Close ();
	CUdpThread::Close (m_sockClient);
}


void CServerThread::ProcessData ()
{
	if (GetSockType () == SOCK_DGRAM) {
		CUdpThread::ProcessData ();
		return;
	}

	// NOTE: don't call CUdpThread::ProcessData for SOCK_STREAM

	if (m_sockClient != INVALID_SOCKET) {
		timeval timeout = { 0, 1 }; // (100000 * 1) /* microsecond */ / 100 };
		fd_set readfds;

		readfds.fd_count = 1;
		readfds.fd_array [0] = m_sockClient;

		int nSelect = ::select (0, &readfds, NULL, NULL, &timeout);

		//TRACEF (_T ("nSelect: ") + ToString (nSelect));

		if (nSelect < 0) {
			TRACER (GetError (::WSAGetLastError ()) + _T ("\n"));
			TRACEF (GetError (::WSAGetLastError ()));
		}

		if (nSelect > 0) {
			char szBuffer [1024] = { 0 };
			int nLen = sizeof (szBuffer) - 1;
	
			int nRecv = ::recv (m_sockClient, szBuffer, nLen, 0);

			switch (nRecv) {
			case 0:				// closed gracefully
				CUdpThread::Close (m_sockClient);
				break;
			case SOCKET_ERROR:	// lost connection
				CUdpThread::Close (m_sockClient);
				TRACEF (GetError (::WSAGetLastError ()));
				break;
			default:			// recieved data
				{
					//TRACEF (FoxjetDatabase::Format ((const LPBYTE )szBuffer, nRecv));
					CString strResponse = OnDataRecieved (szBuffer, nRecv); //nLen);
					
					if (int nLen = strResponse.GetLength ()) {
						int nSend = ::send (m_sockClient, w2a (strResponse), nLen, 0);

						TRACER (strResponse + _T ("\n"));

						if (nSend == SOCKET_ERROR)
							TRACEF (GetError (::WSAGetLastError ()));
						else {
							if (m_pView) {
								CString str = ">> " + strResponse;

								TRACEF (str);
								TRACER (str + _T ("\n"));
								m_pView->UpdateSerialData (str);
							}
						}
					}
				}

				break;
			}
		}
	}
	else {
		SOCKADDR_IN sin;
		int nLen = sizeof (sin);
		
		m_sockClient = ::accept (m_sock, (struct sockaddr *)&sin, &nLen);

		if (m_sockClient == INVALID_SOCKET) 
			TRACEF (GetError (::WSAGetLastError ()));
		else {
			m_strAddrFrom = inet_ntoa (sin.sin_addr);

			#ifdef _DEBUG
			CString str;

			TRACEF (m_strAddrFrom);

			str.Format (_T ("accept: %s, %d"),
				GetSockType () == SOCK_STREAM ? _T ("SOCK_STREAM") : _T ("SOCK_DGRAM"),
				GetPort ());
			TRACEF (str);
			#endif
		}
	}
}

void CServerThread::OnKillThread (WPARAM wParam, LPARAM lParam)
{
	TRACEF ("CServerThread::OnKillThread");
	Close ();
	CUdpThread::OnKillThread (wParam, lParam);
}

void CServerThread::Send (const CString & str)
{
	int nSend = ::send (m_sockClient, w2a (str), str.GetLength (), 0);

	if (nSend == SOCKET_ERROR)
		TRACEF (GetError (::WSAGetLastError ()));
}


CString CServerThread::GetLocalAddress (SOCKET sock)
{
	CString str;

	if (sock != INVALID_SOCKET) {
		SOCKADDR_IN addr;

		memset (&addr, 0, sizeof(addr));
		int nLen = sizeof(addr);

		getsockname (sock, (SOCKADDR *)&addr, &nLen);
		str.Format (_T ("%s:%d"),
			CString (a2w (inet_ntoa (addr.sin_addr))),
			ntohs (addr.sin_port));
	}

	return str;
}