// UsbDebugDlg.cpp : implementation file
//

#include "stdafx.h"
#include "control.h"
#include "Sw0848Thread.h"
#include "ProdLine.h"
#include "UsbDebugDlg.h"
#include "Parse.h"
#include "LocalHead.h"
#include "WordDlg.h"
#include "HeadDlg.h"

using namespace FoxjetCommon;
using namespace Foxjet3d;
using namespace ListGlobals;
using namespace ItiLibrary;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
using namespace UsbDebugDlg;

CString CRegisterItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0: return FoxjetDatabase::ToString (m_nIndex);
	case 1: 
		{
			CString str;

			str.Format (_T ("0x%04X"), m_w);

			return str;
		}
	case 2: return FoxjetDatabase::ToString ((int)m_w);
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CUsbDebugDlg dialog


CUsbDebugDlg::CUsbDebugDlg(CLocalHead * pHead, CWnd* pParent /*=NULL*/)
:	m_pHead (pHead),
	FoxjetCommon::CEliteDlg(CUsbDebugDlg::IDD, pParent)
{
	Create (IDD, pParent);

	//{{AFX_DATA_INIT(CUsbDebugDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CUsbDebugDlg::~CUsbDebugDlg ()
{
	m_lv->DeleteAllItems ();
}

void CUsbDebugDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUsbDebugDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUsbDebugDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CUsbDebugDlg)
	ON_NOTIFY(LVN_BEGINLABELEDIT, LV_DATA, OnBeginlabeleditData)
	ON_BN_CLICKED(BTN_READ, OnRead)
	ON_BN_CLICKED(BTN_WRITE, OnWrite)
	ON_NOTIFY(LVN_ENDLABELEDIT, LV_DATA, OnEndlabeleditData)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_NOTIFY(NM_DBLCLK, LV_DATA, OnDblclkData)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUsbDebugDlg message handlers

void CUsbDebugDlg::OnRead() 
{
	USB::Registers r;
	CLongArray sel = GetSelectedItems (m_lv);

	m_lv->DeleteAllItems ();
	
	if (theApp.GetUSBDriver ()->ReadAll ((UINT)m_pHead->m_hDeviceID, r)) {
		LPWORD p = (LPWORD)&r;

		for (int i = 0; i < (sizeof (r) / 2); i++) {
			CRegisterItem * pItem = new CRegisterItem ();

			pItem->m_nIndex = i;
			pItem->m_w = p [i];

			m_lv.InsertCtrlData (pItem);
		}

		if (sel.GetSize ()) {
			m_lv->SetItemState (sel [0], (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);	
			m_lv->EnsureVisible (sel [0], FALSE);
		}
	}
}

void CUsbDebugDlg::OnWrite() 
{
	USB::Registers r;
	LPWORD p = (LPWORD)&r;

	ZeroMemory (&r, sizeof (r));

	for (int i = 0; i < (sizeof (r) / 2); i++) {
		if (CRegisterItem * pItem = (CRegisterItem *)m_lv.GetCtrlData (i)) 
			* p = pItem->m_w;

		p++;
	}

	if (!theApp.GetUSBDriver ()->WriteAll ((UINT)m_pHead->m_hDeviceID, r)) 
		MsgBox (* this, _T ("WriteAll failed"));
}

BOOL CUsbDebugDlg::OnInitDialog() 
{
	using ListCtrlImp::CColumn;

	CString strSection = defElements.m_strRegSection + _T ("\\CUsbDebugDlg");

	CArray <CColumn, CColumn> vCols;

	FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (CColumn (_T ("Index")));
	vCols.Add (CColumn (_T ("Hexidecimal")));
	vCols.Add (CColumn (_T ("Decimal")));

	m_lv.Create (LV_DATA, vCols, this, defElements.m_strRegSection);
	
	ULONG lAddr = _tcstoul (m_pHead->m_Config.m_HeadSettings.m_strUID, NULL, 16);
	CString strTitle = _T ("Debug: ") + m_pHead->GetFriendlyName ();

	for (int i = 0; i < ARRAYSIZE (CHeadDlg::m_mapUSB); i++) {
		if (CHeadDlg::m_mapUSB [i].m_lAddr == lAddr) {
			strTitle.Format (_T ("Debug: %s [%s]"), m_pHead->GetFriendlyName (), CHeadDlg::m_mapUSB [i].m_lpsz);
			break;
		}
	}

	SetWindowText (strTitle);

	OnRead ();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CUsbDebugDlg::OnBeginlabeleditData(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;

	* pResult = 1;
}

void CUsbDebugDlg::OnEndlabeleditData(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	
	if (CRegisterItem * pItem = (CRegisterItem *)m_lv.GetCtrlData (pDispInfo->item.iItem)) {
		CString str;

		if (pDispInfo->item.mask & LVIF_TEXT)
			str = pDispInfo->item.pszText;
		else
			str = m_lv->GetItemText (pDispInfo->item.iItem, pDispInfo->item.iSubItem);

		str.Replace (_T ("0x"), _T (""));
		pItem->m_w = (WORD)_tcstoul (str, NULL, 16);
		m_lv.UpdateCtrlData (pItem);
	}
	
	
	*pResult = 0;
}

void CUsbDebugDlg::OnEdit() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (!sel.GetSize ()) {
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
		return;
	}

	if (CRegisterItem * pItem = (CRegisterItem *)m_lv.GetCtrlData (sel [0])) {
		CWordDlg dlg (this);

		dlg.m_w = pItem->m_w;

		if (dlg.DoModal () == IDOK) {
			pItem->m_w = dlg.m_w;
			m_lv.UpdateCtrlData (pItem);
		}
	}
}

void CUsbDebugDlg::OnDblclkData(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();	
	*pResult = 0;
}
