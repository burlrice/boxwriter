#if !defined(AFX_INFODLG_H__A1542F18_696E_4891_A4FA_16B2E2A16ECD__INCLUDED_)
#define AFX_INFODLG_H__A1542F18_696E_4891_A4FA_16B2E2A16ECD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InfoDlg.h : header file
//

class CControlView;

#include "RoundButton2.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CInfoDlg dialog

class CInfoDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CInfoDlg(CControlView * pParent);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CInfoDlg)
	CString	m_strVersion;
	CString	m_strHead1;
	CString	m_strHead2;
	CString	m_strHead3;
	CString	m_strHead4;
	CString	m_strHead5;
	CString	m_strHead6;

	CString	m_strCount1;
	CString	m_strCount2;
	CString	m_strCount3;
	CString	m_strCount4;
	CString	m_strCount5;
	CString	m_strCount6;

	CString	m_strData1;
	CString	m_strData2;

	CString	m_strLine1;
	CString	m_strLine2;
	CString	m_strLine3;
	CString	m_strLine4;
	CString	m_strLine5;
	CString	m_strLine6;

	CString	m_strSpeed1;
	CString	m_strSpeed2;
	CString	m_strSpeed3;
	CString	m_strSpeed4;
	CString	m_strSpeed5;
	CString	m_strSpeed6;
	//}}AFX_DATA

	BOOL UpdateData( BOOL bSaveAndValidate = TRUE );

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	afx_msg void OnClipboard ();
	afx_msg void OnClear ();
	
	virtual void OnCancel ();

	afx_msg LRESULT OnSystemParamsChanged (WPARAM wParam, LPARAM lParam);

	CControlView * m_pControlView;
	CMap <UINT, UINT, CBitmap *, CBitmap *> m_vBitmap;
	CBitmap m_bmpPrinting;
	CBitmap m_bmpNotPrinting;
	CBitmap m_bmpStart;
	CBitmap m_bmpPause;
	CBitmap m_bmpStop;
	CBitmap m_bmpDisabled;
	CBitmap m_bmpInkGreen;
	CBitmap m_bmpInkYellow;
	CBitmap m_bmpInkRed;
	CBitmap m_bmpHVError;
	CBitmap m_bmpHVOK;
	CBitmap m_bmpTempError;
	CBitmap m_bmpTempOK;
	CBitmap m_bmpBlank;
	CBitmap m_bmpNetwork;
	CBitmap m_bmpX;
	FoxjetUtils::CRoundButton2 m_btnClipboard;
	FoxjetUtils::CRoundButton2 m_btnClear;
	CFont m_fntInkLevel;
	CString m_strInkCodeKey;

	// Generated message map functions
	//{{AFX_MSG(CInfoDlg)
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnDeltaposData3(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INFODLG_H__A1542F18_696E_4891_A4FA_16B2E2A16ECD__INCLUDED_)
