// KeyParse.cpp: implementation of the CKeyParse class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Resource.h"
#include "KeyParse.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CKeyParse::CKeyParse()
{

}

CKeyParse::~CKeyParse()
{

}

int CKeyParse::Parse(CString &sKeyWords, CMapStringToString &mapKeywords)
{
	DIAGNOSTIC_CRITICAL_SECTION (m_Sentinel);
	LOCK (m_Sentinel);
	TCHAR delimiter[2];
	TCHAR *pToken;
	TCHAR *pData = new TCHAR [sKeyWords.GetLength() +1];

	_tcscpy (pData, sKeyWords);
	_tcscpy ( delimiter, _T (";"));
	pToken = _tcstok (pData, delimiter);
	while ( pToken != NULL ) {
		CString sTemp  = pToken;
		sTemp.Replace (';', NULL);
		if ( !sTemp.IsEmpty() ) {
			CString sKey, sValue;
			int index = sTemp.Find ('=');
			sKey = sTemp.Left (index);
			index++;
			sValue = sTemp.Right (sTemp.GetLength() - index);
//			if (( !sValue.IsEmpty() ) && ( !sKey.IsEmpty() ))
			if ( !sKey.IsEmpty() )
				mapKeywords.SetAt (sKey, sValue);
		}
		pToken = _tcstok (NULL, delimiter);
	}
	delete pData;
	return mapKeywords.GetCount();
}
