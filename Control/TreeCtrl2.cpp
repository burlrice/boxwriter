//	TreeCtrl2.cpp : implementation file
//	The purpose of this class is to built in support for Drag and Drop based on the standard CTreeCtrl.


#include "stdafx.h"
#include "Resource.h"
#include "TreeCtrl2.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTreeCtrl2

#define SLOWSCROLL_FREQUENCY 2
#define PARENT_ONLY_DROP_TARGET true	// Define this to be true if Items can only be droped on parents

CTreeCtrl2::CTreeCtrl2()
{
	m_nScrollBarSize = GetSystemMetrics( SM_CYHSCROLL );
	m_bDragging = false;
	m_hitemDrop = NULL;
	
}

CTreeCtrl2::~CTreeCtrl2()
{
}

BEGIN_MESSAGE_MAP(CTreeCtrl2, CTreeCtrl)
	//{{AFX_MSG_MAP(CTreeCtrl2)
	ON_NOTIFY_REFLECT(TVN_BEGINDRAG, OnBegindrag)
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTreeCtrl2 message handlers

////////////////////////////////////////////////////////////////////////
//	CTreeCtrl2::OnBeginDrag
//
//	Purpose:
//		Begins the dragging phase
//		Sets the timer for scrolling
//		Sets up m_nUpperCoor and m_nLowerCoor
//
//	Parameters:    
//		NMHDR* pNMHDR	-  Structure containing information about the
//						   TVN_BEGINDRAG notification
//		LRESULT* pResult - 	Pointer to an LRESULT variable in which to 
//							store the result code if the message is handled.
//
//	Return Value:
//		void
//
//	Comments:
//		None
////////////////////////////////////////////////////////////////////////

void CTreeCtrl2::OnBegindrag(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	CPoint ptAction = pNMTreeView->ptDrag;

	m_bDragging = TRUE;
	m_hitemDrag = pNMTreeView->itemNew.hItem;
	m_hitemDrop = NULL;

	// Create drag image and begin dragging
	m_pImageList = CreateDragImage(m_hitemDrag);  // get the image list for dragging

	ClientToScreen (&ptAction);
	m_pImageList->BeginDrag(0, CPoint(0,0));
	m_pImageList->DragEnter(GetDesktopWindow(), ptAction);

	SetCapture();

	// Set the timer to slow down the scrolling when the dragging cursor
	// is close to the top/bottom border of the tree control
	m_nTimerID = SetTimer(1, 75, NULL);
	CRect rect;
	GetClientRect(&rect); //CGH
	ClientToScreen(&rect);
	m_nUpperYCoor = rect.top;
	m_nLowerYCoor = rect.bottom;
	*pResult = 0;
}

//////////////////////////////////////////////////////////////////////// 
//	CTreeCtrl2::OnLButtonUp
//
//	Purpose:
//		Ends the drag-drop operation
//		Kills scrolling timer
//		Resets all concerned variables
//
//	Parameters:    
//		UINT nFlags	- Indicates whether various virtual keys are down. 
//		CPoint point - Specifies the x- and y-coordinate of the cursor.
//
//	Return Value:
//		void
//
//	Comments:
//		None
//////////////////////////////////////////////////////////////////////// 

void CTreeCtrl2::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// Transfer data if the need be and release capture.
	OnButtonUp();
	
	// Kill the timer and reset all variables
	KillTimer( m_nTimerID );
	m_nUpperYCoor = 0;
	m_nLowerYCoor = 0;
	m_nSlowScrollTimeout = 0;
	CTreeCtrl::OnLButtonUp(nFlags, point);
}

////////////////////////////////////////////////////////////////////////
//	CTreeCtrl2::OnButtonUp
//
//	Purpose:
//		Helper function called from CTreeCtrl2::OnLButtonUp
//
//	Parameters:    
//		None
//
//	Return Value:
//		void
//
//	Comments:
//		None
//////////////////////////////////////////////////////////////////////

void CTreeCtrl2::OnButtonUp()
{
	if (m_bDragging) {
		m_pImageList->DragLeave(this);
		m_pImageList->EndDrag();
		if ( m_hitemDrop != NULL )
		{
			if (m_hitemDrag != m_hitemDrop && !IsChildNodeOf(m_hitemDrop, m_hitemDrag) && GetParentItem(m_hitemDrag) != m_hitemDrop) {
				TransferItem(m_hitemDrag, m_hitemDrop);
				DeleteItem(m_hitemDrag);
			}
			else
				MessageBeep(0);
		}
		else
			MessageBeep(0);

		ReleaseCapture();
		m_bDragging = FALSE;
		SelectDropTarget(NULL);
	}
}

//////////////////////////////////////////////////////////////////////// 
//	CTreeCtrl2::OnMouseMove
//
//	Purpose:
//		Implements routine drag-drop functionality
//
//	Parameters:    
//		UINT nFlags	- Indicates whether various virtual keys are down. 
//		CPoint point - Specifies the x- and y-coordinate of the cursor.
//
//	Return Value:
//		void
//
//	Comments:
//		None
//////////////////////////////////////////////////////////////////////

void CTreeCtrl2::OnMouseMove(UINT nFlags, CPoint point) 
{
	HTREEITEM hItem, hItemRoot, hItemSibling;
	UINT flags;

	if (m_bDragging) {
		CPoint ptScreen(point);
		CPoint ptClient(point);
		ClientToScreen(&ptClient);

		m_pImageList->DragMove(ptClient);
		m_pImageList->DragShowNolock(FALSE);
		
//		ScreenToClient (&ptScreen);
		if ((hItem = HitTest( ptScreen, &flags )) != NULL) {
			if ( PARENT_ONLY_DROP_TARGET ) { // Define this to be true if Items can only be droped on parents
				hItemRoot = GetRootItem();
				hItemSibling = GetNextSiblingItem (hItemRoot);

				while ( hItemSibling ) {
					if ( hItemSibling == hItem )
						break;
					hItemSibling = GetNextSiblingItem(hItemSibling);
				}

				if ( hItemSibling ) {
					SelectDropTarget(hItem);
					m_hitemDrop = hItem;
				}
			}
			else {
				SelectDropTarget(hItem);
				m_hitemDrop = hItem;
			}
		}
		m_pImageList->DragShowNolock(TRUE);
	}
	CTreeCtrl::OnMouseMove(nFlags, point);
}

/*
void CTreeCtrl2::OnMouseMove(UINT nFlags, CPoint point) 
{
	HTREEITEM hitem;
	UINT flags;

	if (m_bDragging) {
		CPoint ptScreen(point);
		ClientToScreen(&ptScreen);

		m_pImageList->DragMove(ptScreen);
		m_pImageList->DragShowNolock(FALSE);
		
		ScreenToClient (&ptScreen);
		if ((hitem = HitTest(ptScreen, &flags)) != NULL) {
			if ( PARENT_ONLY_DROP_TARGET ) { // Define this to be true if Items can only be droped on parents
				if ( ItemHasChildren (hitem) ) {
					SelectDropTarget(hitem);
					m_hitemDrop = hitem;
				}
			}
			else {
				SelectDropTarget(hitem);
				m_hitemDrop = hitem;
			}
		}
		m_pImageList->DragShowNolock(TRUE);
	}
	CTreeCtrl::OnMouseMove(nFlags, point);
}
*/
//////////////////////////////////////////////////////////////////// 
//	CTreeCtrl2::OnTimer
//
//	Purpose:
//		Moves the drag image to the current mouse position
//		Finds out if there is a need to scroll. If so, finds out
//		what kind of scrolling to do - Scroll Up/Down and Scroll
//		Normal/Slow.
//		Selects the drop target
//
//	Parameters:    
//		UINT nIDEvent - The ID of the timer causing the message
//
//	Return Value:
//		void
//
//	Comments:
//		None
//////////////////////////////////////////////////////////////////////

void CTreeCtrl2::OnTimer(UINT nIDEvent) 
{
	if( nIDEvent != m_nTimerID ) {
		CTreeCtrl::OnTimer(nIDEvent);
		return;
	 }

	// Get the "current" mouse position
	const MSG* pMessage;
	CPoint ptMouse;
	pMessage = GetCurrentMessage();
	ASSERT ( pMessage );
	ptMouse = pMessage->pt;

	// Move the ghost image
	m_pImageList->DragMove(ptMouse);

	// Find out if scrolling is needed.
	// Scrolling is not needed for example, if the dragging cursor is 
	// withing the tree control itself
	if( !NeedToScroll( ptMouse ) ) {
		CTreeCtrl::OnTimer(nIDEvent);
		return;
	}

	// Refine the scrolling mode -
	// Scroll Up/Down, Slow/Normal
	int nScrollMode = RefineScrollMode( ptMouse );
	switch( nScrollMode ) {
		case SCROLL_UP_SLOW:
		case SCROLL_DOWN_SLOW:
			if( m_nSlowScrollTimeout == 0)
				SendMessage( WM_VSCROLL, nScrollMode == SCROLL_UP_SLOW ? SB_LINEUP : SB_LINEDOWN);
			m_nSlowScrollTimeout = ++m_nSlowScrollTimeout%SLOWSCROLL_FREQUENCY;
			break;
		case SCROLL_UP_NORMAL:
		case SCROLL_DOWN_NORMAL:
			SendMessage( WM_VSCROLL, nScrollMode == SCROLL_UP_NORMAL ? SB_LINEUP : SB_LINEDOWN);
			break;
		default:
			ASSERT(FALSE);
			return;
			break;
	}

	// Select the drop target
	m_pImageList->DragLeave(this);
	HTREEITEM hitem = GetFirstVisibleItem();

	switch( nScrollMode ) {
		case SCROLL_UP_SLOW:
		case SCROLL_UP_NORMAL:
		{
			SelectDropTarget(hitem);
			m_hitemDrop = hitem;
			break;
		}
		case SCROLL_DOWN_SLOW:
		case SCROLL_DOWN_NORMAL:
		{
			// Get the last visible item in the control
			int nCount = GetVisibleCount();
			for ( int i=0; i<nCount-1; ++i )
				hitem = GetNextVisibleItem(hitem);
			SelectDropTarget(hitem);
			m_hitemDrop = hitem;
			break;
		}
		default:
			ASSERT(FALSE);
			return;
			break;
	}

	m_pImageList->DragEnter(this, ptMouse);
	CTreeCtrl::OnTimer(nIDEvent);
}

/////////////////////////////////////////////////////////////////////
//  CTreeCtrl2::TransferItem
//
//	Purpose:
//		Transfers items during a drop operation
//
//	Parameters:    
//		HTREEITEM hitemDrag - identifies item that is being dragged
//		HTREEITEM hItemDrop - identifies the drop target
//
//	Return Value:
//		BOOL : TRUE if successful
//	
//	Comments:
//		None
//////////////////////////////////////////////////////////////////////

BOOL CTreeCtrl2::TransferItem(HTREEITEM hitemDrag, HTREEITEM hitemDrop)
{
	TV_INSERTSTRUCT		tvstruct;
	TCHAR				sztBuffer[50];
	HTREEITEM			hNewItem;
	HTREEITEM			hFirstChild;

	// avoid an infinite recursion situation
	tvstruct.item.hItem = hitemDrag;
	tvstruct.item.cchTextMax = 49;
	tvstruct.item.pszText = sztBuffer;
	tvstruct.item.mask = TVIF_CHILDREN | TVIF_HANDLE | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;
	GetItem(&tvstruct.item);  // get information of the dragged element
	tvstruct.hParent = hitemDrop;
	tvstruct.hInsertAfter = TVI_SORT;
	tvstruct.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT |TVIF_PARAM;
	hNewItem = InsertItem(&tvstruct);

	while ((hFirstChild = GetChildItem(hitemDrag)) != NULL) {
		// recursively transfer all the items
		TransferItem(hFirstChild, hNewItem);  
		// delete the first child and all its children
		DeleteItem(hFirstChild);		
	}
	return TRUE;
}


//////////////////////////////////////////////////////////////////////// 
//	CTreeCtrl2::IsChildNodeOf
//
//	Purpose:
//		Finds out if hitemChild is a child of hitemSuspectedParent
//		Called by OnButtonUp to prevent the case where an item is attempted
//		to be made a child of its own child.
//
//	Parameters:    
//		HTREEITEM hitemChild	- The item being dragged
//		HTREEITEM hitemSuspectedParent - The suspected parent
//
//	Return Value:
//		BOOL:	TRUE if hItemChild is a child of hitemSuspectedParent,
//				FALSE otherwise.
//	
//	Comments:
//		None
/////////////////////////////////////////////////////////////////////////////

BOOL CTreeCtrl2::IsChildNodeOf(HTREEITEM hitemChild, HTREEITEM hitemSuspectedParent)
{
	do {
		if (hitemChild == hitemSuspectedParent)
			break;
	} while ((hitemChild = GetParentItem(hitemChild)) != NULL);
	return (hitemChild != NULL);
}

///////////////////////////////////////////////////////////////////////
//
//	CTreeCtrl2::NeedToScroll
//
//	Purpose:
//		Finds out if there is a need to scroll. Called during dragging
//
//	Parameters:    
//		CPoint ptMouse - The current mouse position
//
//	Return Value:
//		BOOL : TRUE if scrolling is necessary, FALSE otherwise.
//
//	Comments:
//		None
///////////////////////////////////////////////////////////////////////

BOOL CTreeCtrl2::NeedToScroll( CPoint ptMouse )
{
	return ptMouse.y < m_nUpperYCoor || ptMouse.y > m_nLowerYCoor;
}


//////////////////////////////////////////////////////////////////////// 
//
//	CTreeCtrl2::RefineScrollMode
//
//	Purpose:
//		Refines the scrolling mode. Called if scrolling is necessary
//
//	Parameters:    
//		CPoint ptMouse - the Current Mouse position
//
//	Return Value:
//		SCROLLMODE: 
//			SCROLL_DOWN_NORMAL : Need normal down scrolling
//			SCROLL_DOWN_SLOW : Need slow down scrolling
//			SCROLL_UP_NORMAL : Need normal up scrolling
//			SCROLL_UP_SLOW : Need slow up scrolling
//
//	Comments:
//		None
//////////////////////////////////////////////////////////////////////// 

SCROLLMODE CTreeCtrl2::RefineScrollMode( CPoint ptMouse )
{
	int nYCoor = ptMouse.y;
	SCROLLMODE nScrollMode;

	nScrollMode = nYCoor > m_nLowerYCoor + m_nScrollBarSize ? SCROLL_DOWN_NORMAL :
					nYCoor > m_nLowerYCoor ? SCROLL_DOWN_SLOW :
					nYCoor < m_nUpperYCoor - m_nScrollBarSize ? SCROLL_UP_NORMAL :
					SCROLL_UP_SLOW;

	return nScrollMode;
}

BOOL CTreeCtrl2::PreTranslateMessage(MSG* pMsg) 
{
	return CTreeCtrl::PreTranslateMessage(pMsg);
}
