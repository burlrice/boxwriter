// PreviewCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "PreviewCtrl.h"
#include "PreviewDlg.h"
#include "Color.h"

using namespace Color;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPreviewCtrl

IMPLEMENT_DYNCREATE(CPreviewCtrl, CScrollView)

CPreviewCtrl::CPreviewCtrl()
:	m_pParent (NULL)
{
}

CPreviewCtrl::~CPreviewCtrl()
{
}


BEGIN_MESSAGE_MAP(CPreviewCtrl, CScrollView)
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEACTIVATE()
	//{{AFX_MSG_MAP(CPreviewCtrl)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPreviewCtrl drawing

void CPreviewCtrl::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
}

/////////////////////////////////////////////////////////////////////////////
// CPreviewCtrl diagnostics

#ifdef _DEBUG
void CPreviewCtrl::AssertValid() const
{
	CScrollView::AssertValid();
}

void CPreviewCtrl::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPreviewCtrl message handlers

BOOL CPreviewCtrl::OnEraseBkgnd(CDC* pDC) 
{
	CRect rc;

	GetClientRect (rc);
	pDC->FillSolidRect (rc, Color::rgbDarkGray);

	return TRUE;
}

void CPreviewCtrl::PostNcDestroy() 
{
	CWnd::PostNcDestroy();
}

int CPreviewCtrl::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message) 
{
	// NOTE: do NOT call CScrollView::OnMouseActivate
	return CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

bool CPreviewCtrl::Create (PreviewDlg::CPreviewDlg * pParent, const CRect & rc)
{
	if (CScrollView::Create (NULL, NULL, 
		WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL,
		rc, pParent, 0))
	{
		if (CDC * pDC = GetDC ()) {
			CSize size (0, 0), sizePage (0, 0), sizeRes (0, 0);
			CPrintDialog dlg (FALSE);

			dlg.GetDefaults ();

			sizePage.cx = ::GetDeviceCaps (dlg.m_pd.hDC, HORZRES);
			sizePage.cy = ::GetDeviceCaps (dlg.m_pd.hDC, VERTRES);
			sizeRes.cx	= ::GetDeviceCaps (dlg.m_pd.hDC, LOGPIXELSX);
			sizeRes.cy	= ::GetDeviceCaps (dlg.m_pd.hDC, LOGPIXELSY);

			sizePage.cx = max (0, sizePage.cx);
			sizePage.cy = max (0, sizePage.cy);
			sizeRes.cx	= max (0, sizeRes.cx);
			sizeRes.cy	= max (0, sizeRes.cy);

			size.cx = (int)((double)sizePage.cx * ((double)pDC->GetDeviceCaps (LOGPIXELSX) / (double)sizeRes.cx));
			size.cy = (int)((double)sizePage.cy * ((double)pDC->GetDeviceCaps (LOGPIXELSY) / (double)sizeRes.cy));

			SetScrollSizes (MM_TEXT, size);
		}

		m_pParent = pParent;

		return true;
	}

	return false;
}

void CPreviewCtrl::OnDraw(CDC* pDC)
{
	if (m_pParent)
		m_pParent->Draw (* pDC);
	else
		pDC->FillSolidRect (GetPageRect (), Color::rgbDarkGray);
}

CRect CPreviewCtrl::GetPageRect()
{
	CRect rc;
	CSize size = GetTotalSize ();

	GetClientRect (rc);
	rc.right = rc.left + size.cx;
	rc.bottom = rc.top + size.cy;

	return rc;
}



