#if !defined(AFX_CONNECTDLG_H__B1984B6B_51DC_4902_BA91_0B8F21BA60D8__INCLUDED_)
#define AFX_CONNECTDLG_H__B1984B6B_51DC_4902_BA91_0B8F21BA60D8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ConnectDlg.h : header file
//

#include "Resource.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CConnectDlg dialog

class CConnectDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CConnectDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CConnectDlg)
	enum { IDD = IDD_CONNECT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CConnectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CConnectDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONNECTDLG_H__B1984B6B_51DC_4902_BA91_0B8F21BA60D8__INCLUDED_)
