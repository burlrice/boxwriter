#if !defined(AFX_FIRMWAREDLG_H__95B4F6F2_D4C2_457F_8A28_AB3EC0C38994__INCLUDED_)
#define AFX_FIRMWAREDLG_H__95B4F6F2_D4C2_457F_8A28_AB3EC0C38994__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FirmwareDlg.h : header file
//

#ifdef __IPPRINTER__
	#include "RemoteHead.h"
#endif

#include "ListCtrlImp.h"

/////////////////////////////////////////////////////////////////////////////
// CFirmwareDlg dialog

namespace FirmwareDlg
{
	class CHeadItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CHeadItem (CRemoteHead * pHead);
		virtual ~CHeadItem ();

		virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
		virtual CString GetDispText (int nColumn) const;

		CString m_strName;
		CString m_strIpAddr;
		CString m_strSwVer;
		CString m_strGaVer;
		CString m_strDiskUsage;
		CRemoteHead * m_pHead;
	};

	class CFirmwareDlg : public CPropertyPage
	{
		DECLARE_DYNCREATE(CFirmwareDlg)

		CArray <CRemoteHead *, CRemoteHead *> m_vHeads;

	// Construction
	public:
		CFirmwareDlg();
		~CFirmwareDlg();

		CPropertySheet * m_pParent;

	protected:

		CString GetMostRecentFile (RemoteHead::UPDATETYPE type);
		bool DoUpdate (RemoteHead::UPDATETYPE type);

		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

	// Dialog Data
		//{{AFX_DATA(CFirmwareDlg)
		CString	m_strFirmwarePath;
		CString	m_strGaPath;
		//}}AFX_DATA


	// Overrides
		// ClassWizard generate virtual function overrides
		//{{AFX_VIRTUAL(CFirmwareDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		HBRUSH m_hbrDlg;

		// Generated message map functions
		//{{AFX_MSG(CFirmwareDlg)
		afx_msg void OnBrowse();
		afx_msg void OnUpdate();
		virtual BOOL OnInitDialog();
		afx_msg void OnBrowseGa();
		afx_msg void OnUpdateGa();
		afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()

	};
}; // namespace FirmwareDlg

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIRMWAREDLG_H__95B4F6F2_D4C2_457F_8A28_AB3EC0C38994__INCLUDED_)
