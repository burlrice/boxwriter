#ifndef __CONTROL_EXPORTIMPL_H__
#define __CONTROL_EXPORTIMPL_H__

#include <afxtempl.h>
#include "Database.h"

class CExport
{
public:
	CExport ();
	virtual ~CExport ();

	void Build (FoxjetDatabase::COdbcDatabase & db);
	CString GetNext ();
	bool IsFinished () const;
	int GetSize ();

private:
	static ULONG CALLBACK ThreadFunc (LPVOID lpData);

	FoxjetDatabase::COdbcDatabase * m_pDB;
	bool m_bFinished;
	CStringArray m_v;
	CDiagnosticCriticalSection m_cs;
	HANDLE m_hThread;
};

#endif //__CONTROL_EXPORTIMPL_H__