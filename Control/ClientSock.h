#if !defined(AFX_CCLIENTSOCK_H__BFDC1BE2_73D4_11D2_99E8_006097CA12A0__INCLUDED_)
#define AFX_CCLIENTSOCK_H__BFDC1BE2_73D4_11D2_99E8_006097CA12A0__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <afxmt.h>
#include <afxsock.h>
#include "Defines.h"
#include "typedefs.h"

/////////////////////////////////////////////////////////////////////////////
// CClientSock command target
#define RX_BUFFER_SIZE 1024 * 32
class CClientSock : public CSocket
{

// Attributes
public:

// Operations
public:
	CClientSock(DWORD dwThreadID);
	virtual ~CClientSock();
	HANDLE hPhotoEvent;
	CTimeSpan GetConnectedTime(void);
	unsigned int nSocketId;
	bool m_bConnected;
	int SendPacket (unsigned int Cmd, long BatchID, const void* data, unsigned long Len);

// Overrides
public:
	void Initialize (void);
	_SOCKET_MESSAGE *GetRxData();
	unsigned int GetSocketID(void);
	CString & GetAddress (void);
	CString & GetNetworkName (void);

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClientSock)
	public:
	virtual void OnReceive(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CClientSock)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
	void CueRxData (_SOCKET_MESSAGE* pMsg);
	unsigned char *m_pRxBuffer;
	bool m_bMessageTransfered;
	CDiagnosticCriticalSection m_csSendLock;
	CDiagnosticCriticalSection m_RxCueSentinel;
	DWORD m_nThreadID;
	UINT m_nPort;
	CString m_sAddress;
	CString m_sNetworkName;
	int m_nPacketState;
	_SOCKET_MESSAGE tmpSm;
	void *holdBuf;
	unsigned long holdBufLen;
	CTime ConnectTime;
	CPtrList MessageCue;
	CPtrList m_RxCue;
	void Extract_Message(unsigned char *pMsg, unsigned pMsgLength);
	void ProcessMessages(void);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCLIENTSOCK_H__BFDC1BE2_73D4_11D2_99E8_006097CA12A0__INCLUDED_)
