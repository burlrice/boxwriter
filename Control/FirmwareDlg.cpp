// FirmwareDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FirmwareDlg.h"
#include "Head.h"
#include "control.h"
#include "Extern.h"
#include "Registry.h"
#include "Resource.h"
#include "Debug.h"
#include "FileExt.h"
#include "EnTabCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace ListGlobals;
using namespace ItiLibrary;
using namespace FirmwareDlg;

using RemoteHead::UPDATE_FIRMWARE;
using RemoteHead::UPDATE_GA;

static LPCTSTR lpszFirmwareKey	= _T ("Firmware");
static LPCTSTR lpszGaKey		= _T ("GA");

static LPCTSTR lpszFirmwareExt	= _T ("*.bin");
static LPCTSTR lpszGaExt		= _T ("*.ga");

/////////////////////////////////////////////////////////////////////////////

using FirmwareDlg::CHeadItem;

CHeadItem::CHeadItem (CRemoteHead * pHead)
:	m_pHead (pHead)
{
	Head::CHeadInfo info = Head::CHead::GetInfo (pHead);

	m_strName	= info.GetFriendlyName ();
	m_strIpAddr	= info.m_Config.m_HeadSettings.m_strUID;
	m_strSwVer	= info.GetSwVer ();
	m_strGaVer	= info.GetGaVer ();

	if (info.IsDigiBoard ())
		m_strDiskUsage.Format (_T ("%.02f%%"), info.GetDiskUsage ());
}

CHeadItem::~CHeadItem ()
{
}

int CHeadItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	return CItem::Compare (rhs, nColumn);
}

CString CHeadItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:		return m_strName;
	case 1:		return m_strIpAddr;
	case 2:		return m_strSwVer;
	case 3:		return m_strGaVer;
	case 4:		return m_strDiskUsage;
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CFirmwareDlg property page

IMPLEMENT_DYNCREATE(CFirmwareDlg, CPropertyPage)

CFirmwareDlg::CFirmwareDlg() 
:	m_pParent (NULL),
	m_hbrDlg (NULL),
	CPropertyPage (IDD_FIRMWARE_NEXT_MATRIX /* IsMatrix () ? IDD_FIRMWARE_NEXT_MATRIX : IDD_FIRMWARE_NEXT */ )
{
	//{{AFX_DATA_INIT(CFirmwareDlg)
	//}}AFX_DATA_INIT
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CFirmwareDlg::~CFirmwareDlg()
{
}

void CFirmwareDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFirmwareDlg)
	DDX_Text(pDX, TXT_FILENAME, m_strFirmwarePath);
	DDX_Text(pDX, TXT_GAFILENAME, m_strGaPath);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFirmwareDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CFirmwareDlg)
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_BN_CLICKED(BTN_UPDATE, OnUpdate)
	ON_BN_CLICKED(BTN_BROWSEGA, OnBrowseGa)
	ON_BN_CLICKED(BTN_UPDATEGA, OnUpdateGa)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFirmwareDlg message handlers

void CFirmwareDlg::OnBrowse() 
{
	CFileDialog dlg (true, ::lpszFirmwareExt, m_strFirmwarePath, 
		OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST|OFN_READONLY|OFN_HIDEREADONLY, 
		"Firmware files (" + CString (::lpszFirmwareExt) + ")|" + CString (::lpszFirmwareExt) + "|");

	if (dlg.DoModal () == IDOK) {
		CString strSection = defElements.m_strRegSection + _T ("\\CFirmwareDlg");

		if (FoxjetFile::GetFileExt (dlg.GetPathName ()).CompareNoCase (_T ("bin")) != 0) {
			MsgBox (* this, LoadString (IDS_INVALIDFILETYPE), NULL, MB_ICONERROR);
			return;
		}

		m_strFirmwarePath = dlg.GetPathName ();
		FoxjetDatabase::WriteProfileString (strSection, lpszFirmwareKey, m_strFirmwarePath);
		UpdateData (FALSE);
	}
}

void CFirmwareDlg::OnBrowseGa() 
{
	CFileDialog dlg (true, ::lpszGaExt, m_strGaPath, 
		OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST|OFN_READONLY|OFN_HIDEREADONLY, 
		"Gate array files (" + CString (::lpszGaExt) + ")|" + CString (::lpszGaExt) + "|");

	if (dlg.DoModal () == IDOK) {
		CString strSection = defElements.m_strRegSection + _T ("\\CFirmwareDlg");
		
		if (FoxjetFile::GetFileExt (dlg.GetPathName ()).CompareNoCase (_T ("ga")) != 0) {
			MsgBox (* this, LoadString (IDS_INVALIDFILETYPE), NULL, MB_ICONERROR);
			return;
		}

		m_strGaPath = dlg.GetPathName ();
		FoxjetDatabase::WriteProfileString (strSection, lpszGaKey, m_strGaPath);
		UpdateData (FALSE);
	}
}


bool CFirmwareDlg::DoUpdate (RemoteHead::UPDATETYPE type) 
{
	bool bResult = false;
	ULONG lLen = 0;
	const CString strPath = type == UPDATE_FIRMWARE ? m_strFirmwarePath : m_strGaPath;
	CString strNewer;
	bool bSkipMoreRecent = false;
	CArray <CHeadItem *, CHeadItem *> v;
	CArray <CRemoteHead *, CRemoteHead *> vMismatch;
	const bool bNetFirmware = CRemoteHead::IsNetFirmware (m_strFirmwarePath);

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (m_lv->GetCheck (i)) {
			CHeadItem * p = (CHeadItem *)m_lv.GetCtrlData (i);

			ASSERT (p);
			v.Add (p);

			if (type == UPDATE_FIRMWARE) {
				CVersion verIP (p->m_strSwVer);
				CVersion verApp (GlobalVars::CurrentVersion);
				int nCmp = CRemoteHead::CompareVersions (verIP, verApp);

				if (nCmp >= -99) 
					strNewer += _T ("\n") + p->m_strName + _T (": ") + p->m_strSwVer;

				if (type == UPDATE_FIRMWARE) { 
					if ((p->m_pHead->IsDigiBoard () && bNetFirmware) || !p->m_pHead->IsDigiBoard () && !bNetFirmware) {
						vMismatch.Add (p->m_pHead);
					}
				}
			}
		}
	}

	if (vMismatch.GetSize ()) {
		CString str;
		
		str.Format (LoadString (IDS_FIRMWARE_INCOMPATABLE), _T ("\n"));

		for (int i = 0; i < vMismatch.GetSize (); i++) {
			Head::CHeadInfo info = Head::CHead::GetInfo (vMismatch [i]);

			str += info.GetFriendlyName () + _T ("\n");
		}

		MsgBox (* this, str, NULL, MB_OK | MB_ICONERROR);
		return false;
	}

	if (strNewer.GetLength ()) {
		CString str;
		
		str.Format (LoadString (IDS_NEWVERSION), strNewer);
		int nResult = MsgBox (* this, str, NULL, MB_YESNOCANCEL | MB_DEFBUTTON2);

		if (nResult == IDCANCEL) 
			return false;
		else if (nResult == IDNO)
			bSkipMoreRecent = true;
	}

	const UINT nMsg = type == UPDATE_FIRMWARE ? TM_UPDATE_FIRMWARE : TM_UPDATE_GA;

	for (int i = 0; i < v.GetSize (); i++) {
		CHeadItem * p = v [i];

		ASSERT (p);

		CVersion verIP (type == UPDATE_FIRMWARE ? p->m_strSwVer : p->m_strGaVer);
		CVersion verApp (GlobalVars::CurrentVersion);
		int nCmp = CRemoteHead::CompareVersions (verIP, verApp);

		if (bSkipMoreRecent && nCmp >= -99)
			continue;

		TRACEF ("Update: " + p->m_strIpAddr + " SW ver: " + p->m_strSwVer + " GA ver: " + p->m_strGaVer);

		while (!PostThreadMessage (p->m_pHead->m_nThreadID, nMsg, 0, (LPARAM)new CString (strPath))) 
			::Sleep(0);

		// TM_UPDATE_x deletes pTemp

		bResult = true;
	}

	return bResult;
}

void CFirmwareDlg::OnUpdate() 
{
	if (!m_strFirmwarePath.GetLength ()) {
		MsgBox (* this, LoadString (IDS_NOFILENAME));
		return;
	}

	if (DoUpdate (UPDATE_FIRMWARE))
		if (m_pParent)
			m_pParent->EndDialog (IDOK);
}

void CFirmwareDlg::OnUpdateGa() 
{
	if (!m_strGaPath.GetLength ()) {
		MsgBox (* this, LoadString (IDS_NOFILENAME));
		return;
	}

	if (DoUpdate (UPDATE_GA)) {
		if (m_pParent)
			m_pParent->EndDialog (IDOK);
	}
}

CString CFirmwareDlg::GetMostRecentFile (RemoteHead::UPDATETYPE type) 
{
	CString strResult;
	WIN32_FIND_DATA fd;
	const CString strDir = FoxjetDatabase::GetHomeDir ();
	const CString str = strDir + _T ("\\") + CString (type == UPDATE_FIRMWARE ? ::lpszFirmwareExt : ::lpszGaExt);
	HANDLE hFind = ::FindFirstFile (str, &fd);
	bool bMore = hFind != INVALID_HANDLE_VALUE;
	const CTime tmNow = CTime::GetCurrentTime ();
	CTimeSpan tmSpan = tmNow - CTime (fd.ftLastWriteTime);
	LONG lSeconds = abs (tmSpan.GetTotalSeconds ());

	while (bMore) {
		tmSpan = tmNow - CTime (fd.ftLastWriteTime);
		
		if (abs (tmSpan.GetTotalSeconds ()) <= lSeconds) {
			lSeconds = abs (tmSpan.GetTotalSeconds ());
			strResult = fd.cFileName;
		}

		ZeroMemory (&fd, sizeof (fd));
		bMore = ::FindNextFile (hFind, &fd) ? true : false;
	}

	::FindClose (hFind);

	if (strResult.GetLength ())
		return strDir + _T ("\\") + strResult;
	
	return _T ("");
}

BOOL CFirmwareDlg::OnInitDialog() 
{	
	using ListCtrlImp::CColumn;

	CString strSection = defElements.m_strRegSection + _T ("\\CFirmwareDlg");
	CArray <CColumn, CColumn> vCols;

	m_strFirmwarePath = FoxjetDatabase::GetProfileString (strSection, lpszFirmwareKey, GetMostRecentFile (UPDATE_FIRMWARE));
	m_strGaPath = FoxjetDatabase::GetProfileString (strSection, lpszGaKey, GetMostRecentFile (UPDATE_GA));

	
	CPropertyPage::OnInitDialog (); //FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_HEAD)));
	vCols.Add (CColumn (LoadString (IDS_ADDRESS)));
	vCols.Add (CColumn (LoadString (IDS_SWVER)));
	vCols.Add (CColumn (LoadString (IDS_GAVER)));
	vCols.Add (CColumn (LoadString (IDS_DISKUSAGE)));

	m_lv.Create (LV_HEADS, vCols, this, defElements.m_strRegSection);
	m_lv->SetExtendedStyle (LVS_EX_CHECKBOXES | m_lv->GetExtendedStyle ());

	for (int i = 0; i < m_vHeads.GetSize (); i++) {
		CHeadItem * p = new CHeadItem (m_vHeads [i]);

		m_lv.InsertCtrlData (p);

		int nCmp = CRemoteHead::CompareVersions (CVersion (p->m_strSwVer), FoxjetCommon::verApp);

		m_lv->SetCheck (m_lv.GetDataIndex (p), (nCmp < -99));
	}


	ASSERT (GetDlgItem (BTN_UPDATE));
	ASSERT (GetDlgItem (BTN_UPDATEGA));

	bool bEnabled = theApp.LoginPermits (IDM_CFG_PRINTHEAD);

	bEnabled &= (m_vHeads.GetSize () > 0);

	GetDlgItem (BTN_UPDATE)->EnableWindow (bEnabled);
	GetDlgItem (BTN_UPDATEGA)->EnableWindow (bEnabled);

	return TRUE;
}


HBRUSH CFirmwareDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = ::GetSysColor (COLOR_BTNFACE);//FoxjetUtils::GetButtonColor (_T ("background"));
	
	if (FoxjetDatabase::IsControl (false)) 
		rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	if (!m_hbrDlg)
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}
