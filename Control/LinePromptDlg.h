#if !defined(AFX_LINEPROMPTDLG_H__B64CCFC1_A734_4503_9B01_FB47869CAB9A__INCLUDED_)
#define AFX_LINEPROMPTDLG_H__B64CCFC1_A734_4503_9B01_FB47869CAB9A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LinePromptDlg.h : header file
//

#include "OdbcDatabase.h"
#include "Database.h"
#include "ListCtrlImp.h"
#include "RoundButtons.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CLinePromptDlg dialog

namespace LinePromptDlg
{
	class CLineItem : 
		public ItiLibrary::ListCtrlImp::CItem,
		public FoxjetDatabase::LINESTRUCT
	{
	public:
		CLineItem (FoxjetDatabase::LINESTRUCT & line);
		virtual ~CLineItem ();

		virtual CString GetDispText (int nColumn) const;
	};

	class CLinePromptDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:

	public:
		CLinePromptDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CLinePromptDlg)
		//}}AFX_DATA

		FoxjetDatabase::LINESTRUCT m_line;
		CString m_strTask;
		FoxjetUtils::CRoundButtons m_buttons;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CLinePromptDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:

		FoxjetDatabase::COdbcDatabase & m_db;
		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

		// Generated message map functions
		//{{AFX_MSG(CLinePromptDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnItemchangedLine(NMHDR* pNMHDR, LRESULT* pResult);
		//}}AFX_MSG

		afx_msg void OnDblclkLine(NMHDR* pNMHDR, LRESULT* pResult);
		virtual void OnOK ();

		DECLARE_MESSAGE_MAP()
	};
}; // namespace LinePromptDlg
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LINEPROMPT_H__B64CCFC1_A734_4503_9B01_FB47869CAB9A__INCLUDED_)
