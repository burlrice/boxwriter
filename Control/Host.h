#ifndef __HOST_H__
#define __HOST_H__

#include "WinMsg.h"

#define BW_HOST_BROADCAST_PORT				2200
#define BW_HOST_UDP_PORT					2201
#define BW_HOST_TCP_PORT					2202
#define BW_HOST_HTTP_PORT					2204
#define BW_HOST_UDP_PORT_EX					2205

#define IV_IDS_PORT_SENDTO					1025
#define IV_IDS_PORT_REVCFROM				2203

#define BW_HOST_PACKET_LOCATE						_T ("Locate BoxWriter")
#define BW_HOST_INK_CODE							_T ("Ink code")
#define BW_HOST_SYSTEMPARAMCHANGE						SYSTEMPARAMCHANGE
													
#define BW_HOST_PACKET_GET_TASKS					_T ("Get tasks")
#define BW_HOST_PACKET_GET_LINES					_T ("Get lines")
#define BW_HOST_PACKET_GET_LINE_ID					_T ("Get line ID")
#define BW_HOST_PACKET_START_TASK					_T ("Start task")
#define BW_HOST_PACKET_START_TASK_DATABASE			_T ("Database start task")
#define BW_HOST_PACKET_LOAD_TASK					_T ("Load task")
#define BW_HOST_PACKET_STOP_TASK					_T ("Stop task")
#define BW_HOST_PACKET_IDLE_TASK					_T ("Idle task")
#define BW_HOST_PACKET_RESUME_TASK					_T ("Resume task")
#define BW_HOST_PACKET_GET_USER_ELEMENTS			_T ("Get user elements")
#define BW_HOST_PACKET_SET_USER_ELEMENTS			_T ("Set user elements")
#define BW_HOST_PACKET_SET_ERROR_SOCKET				_T ("Set error socket")
#define BW_HOST_PACKET_GET_CURRENT_TASK				_T ("Get current task")
#define BW_HOST_PACKET_GET_CURRENT_STATE			_T ("Get current state")
#define BW_HOST_PACKET_GET_COUNT					_T ("Get count")
#define BW_HOST_PACKET_SET_COUNT					_T ("Set count")
#define BW_HOST_PACKET_GET_COUNT_MAX				_T ("Get max count")
#define BW_HOST_PACKET_SET_COUNT_MAX				_T ("Set max count")
#define BW_HOST_PACKET_CLOSESOCKET					_T ("Close")
#define BW_HOST_PACKET_GET_HEADS					_T ("Get heads")
#define BW_HOST_PACKET_GET_ELEMENTS					_T ("Get elements")
#define BW_HOST_PACKET_DELETE_ELEMENT				_T ("Delete element")
#define BW_HOST_PACKET_ADD_ELEMENT					_T ("Add element")
#define BW_HOST_PACKET_UPDATE_ELEMENT				_T ("Update element")
#define BW_HOST_PACKET_REFRESH_PREVIEW				_T ("Refresh preview")
#define BW_HOST_PACKET_CLEAR_ERROR					_T ("Clear error")
#define BW_HOST_PACKET_DB_BUILD_STRINGS				_T ("Database::ToString")
#define BW_HOST_PACKET_DB_GET_NEXT_STRING			_T ("Next")
#define BW_HOST_PACKET_DB_IS_READY					_T ("Database::IsReady")
#define BW_HOST_PACKET_DB_BEGIN_UPDATE				_T ("Database::FromString begin")
#define BW_HOST_PACKET_DB_END_UPDATE				_T ("Database::FromString end")
#define BW_HOST_PACKET_PRINTER_SOP					_T ("Start of print")
#define BW_HOST_PACKET_PRINTER_EOP					_T ("End of print")
#define BW_HOST_PACKET_PRINTER_HEAD_STATE_CHANGE	_T ("Head state change")
#define BW_HOST_PACKET_REGISTRY_EXPORT				_T ("RegExport")
#define BW_HOST_PACKET_REGISTRY_IMPORT				_T ("RegImport")
#define BW_HOST_PACKET_SETSERIAL					_T ("Set serial data")
#define BW_HOST_PACKET_GETSERIAL					_T ("Get serial data")
#define BW_HOST_PACKET_GET_STROBE					_T ("Get strobe")

#endif //__HOST_H__
