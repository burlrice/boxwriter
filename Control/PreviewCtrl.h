#if !defined(AFX_PREVIEWCTRL_H__4F17C6A6_9DCC_4955_A780_050879014ECB__INCLUDED_)
#define AFX_PREVIEWCTRL_H__4F17C6A6_9DCC_4955_A780_050879014ECB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PreviewCtrl.h : header file
//

namespace PreviewDlg
{
	class CPreviewDlg;
};

/////////////////////////////////////////////////////////////////////////////
// CPreviewCtrl view

class CPreviewCtrl : public CScrollView
{
protected:
	DECLARE_DYNCREATE(CPreviewCtrl)
public:
	CPreviewCtrl();
	virtual ~CPreviewCtrl ();

	virtual bool Create (PreviewDlg::CPreviewDlg * pParent, const CRect & rc);
	CRect GetPageRect();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPreviewCtrl)
	protected:
	virtual void OnInitialUpdate();     // first time after construct
	//}}AFX_VIRTUAL

// Implementation
protected:

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	PreviewDlg::CPreviewDlg * m_pParent;


	virtual void PostNcDestroy(); 
	virtual void OnDraw(CDC* pDC);

	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	// Generated message map functions
	//{{AFX_MSG(CPreviewCtrl)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PREVIEWCTRL_H__4F17C6A6_9DCC_4955_A780_050879014ECB__INCLUDED_)
