// DebugDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "Control.h"
#include "DebugDlg.h"
#include "Debug.h"
#include "AppVer.h"
#include "Parse.h"
#include "Sw0848Thread.h"
#include "ProdLine.h"

#ifdef __WINPRINTER__
	#include "LocalHead.h"
#endif //__WINPRINTER__

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDebugDlg dialog


CDebugDlg::CDebugDlg(CWnd* pParent /*=NULL*/)
:	m_pHead (NULL),
	FoxjetCommon::CEliteDlg(CDebugDlg::IDD, pParent)
{
	Create (IDD, pParent);
	//{{AFX_DATA_INIT(CDebugDlg)
	m_sISR = _T("");
	m_sOTB = _T("");
	m_sPhoto = _T("");
	m_sSerial = _T("");
	m_sTach = _T("");
	m_sReserved1 = _T("");
	m_sReserved2 = _T("");
	//}}AFX_DATA_INIT
	m_sTitle.Format (_T ("Debug Dlg"));
}


void CDebugDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDebugDlg)
	DDX_Text(pDX, IDC_ISR, m_sISR);
	DDX_Text(pDX, IDC_OTB, m_sOTB);
	DDX_Text(pDX, IDC_PHOTO, m_sPhoto);
	DDX_Text(pDX, IDC_SERIAL, m_sSerial);
	DDX_Text(pDX, IDC_TACH, m_sTach);
	DDX_Text(pDX, IDC_RESERVED1, m_sReserved1);
	DDX_Text(pDX, IDC_RESERVED2, m_sReserved2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDebugDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDebugDlg)
	ON_BN_CLICKED(BTN_CLIPBOARD, OnClipboard)
	ON_BN_CLICKED(CHK_HIGHVOLTAGE, OnHighvoltage)
	ON_BN_CLICKED(CHK_LOWINK, OnLowink)
	ON_BN_CLICKED(CHK_LOWTEMP, OnLowtemp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDebugDlg message handlers

void CDebugDlg::UpdateFlags(_DBG_FLAGS *pFlags, Head::CHead * pHead)
{
	CString str, strTmp, strDebug;

#ifdef __WINPRINTER__
	if (!m_pHead) {
		if (CLocalHead * pNewHead = DYNAMIC_DOWNCAST (CLocalHead, pHead)) {
		#ifdef _DEBUG
			if (CButton * p = (CButton *)GetDlgItem (CHK_LOWTEMP))
				p->SetCheck (pNewHead->m_dwSimulatedError & FoxjetDatabase::HEADERROR_LOWTEMP ? 1 : 0);
			if (CButton * p = (CButton *)GetDlgItem (CHK_LOWINK))
				p->SetCheck (pNewHead->m_dwSimulatedError & FoxjetDatabase::HEADERROR_LOWINK ? 1 : 0);
			if (CButton * p = (CButton *)GetDlgItem (CHK_HIGHVOLTAGE))
				p->SetCheck (pNewHead->m_dwSimulatedError & FoxjetDatabase::HEADERROR_HIGHVOLTAGE ? 1 : 0);
		#endif
		}
	}
#endif

	m_pHead = pHead;
	UpdateUI ();

	m_sISR.Format		(_T ("%lu"), pFlags->ISRInt );
	m_sOTB.Format		(_T ("%lu"), pFlags->OTBInt );
	m_sPhoto.Format		(_T ("%lu"), pFlags->PhotoInt );
	m_sSerial.Format	(_T ("%lu"), pFlags->SerialInt );
	m_sTach.Format		(_T ("%lu"), pFlags->TachInt );
	m_sReserved1.Format	(_T ("%lu"), pFlags->Reserved1 );
	m_sReserved2.Format	(_T ("%lu"), pFlags->Reserved2 );

#ifdef __WINPRINTER__

	if (CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pHead)) {
		WORD wPrintDly	= p->GetPrintDly ();
		WORD wImageLen	= p->GetImageLen ();
		WORD wProdLen	= p->GetProdLen ();
		UINT nFlush		= p->GetFlush ();
		CString str, strFmt = _T ("\t%05d [%03.02f in]\r\n");
		double dFactor = (double)p->m_Config.m_HeadSettings.GetRes ();

///*
		str.Format (_T ("Image len:") + strFmt, 
			wImageLen,
			(double)wImageLen / dFactor);
		strDebug += str;

		str.Format (_T ("Prod len:\t") + strFmt,
			wProdLen,
			(double)wProdLen / dFactor);
		strDebug += str;

		str.Format (_T ("Print delay:") + strFmt,
			wPrintDly,
			(double)wPrintDly / dFactor);
		strDebug += str;

		str.Format (_T ("Flush:\t") + strFmt,
			nFlush,
			(double)nFlush / dFactor);
		strDebug += str;

		if (p->m_Config.m_HeadSettings.m_nDirection == FoxjetDatabase::LTOR) 
			strDebug += "Direction:\t\t----->\r\n";
		else
			strDebug += "Direction:\t\t<-----\r\n";

		/*
		str.Format (_T ("Flush+PhotoDly:") + strFmt,
			nFlush + wPrintDly,
			(double)(nFlush + wPrintDly) / dFactor);
		strDebug += str;
		*/

		strDebug += "\r\n";
//*/

		DWORD dwCtrlReg = 0;
		DWORD dwInterrupt_Reg = 0;
		DWORD dwInterfaceCtrlReg = 0;
		DWORD dwOTBStatusReg = 0;
		DWORD dwInterfaceStatusReg = 0;

		memcpy (&dwCtrlReg,				&p->m_registersISA._CtrlReg,				sizeof (p->m_registersISA._CtrlReg));
		memcpy (&dwInterrupt_Reg,		&p->m_registersISA._Interrupt_Reg,			sizeof (p->m_registersISA._Interrupt_Reg));
		memcpy (&dwInterfaceCtrlReg,	&p->m_registersISA._InterfaceCtrlReg,		sizeof (p->m_registersISA._InterfaceCtrlReg));
		memcpy (&dwOTBStatusReg,		&p->m_registersISA._OTBStatusReg,			sizeof (p->m_registersISA._OTBStatusReg));
		memcpy (&dwInterfaceStatusReg,	&p->m_registersISA._InterfaceStatusReg,	sizeof (p->m_registersISA._InterfaceStatusReg));
			
		str.Format (_T ("ICR:\t\t0x%p\r\n"), pFlags->uICR);
		strDebug += str;

		str.Format (_T ("IER:\t\t0x%p\r\n"), pFlags->uIER);
		strDebug += str;

		str.Format (
			_T ("CtrlReg:\t\t0x%p\r\n")
			_T ("Interrupt_Reg:\t0x%p\r\n")
			_T ("InterfaceCtrlReg:\t0x%p\r\n")
			_T ("OTBStatusReg:\t0x%p\r\n")
			_T ("InterfaceStatusReg:\t0x%p\r\n"),
			dwCtrlReg,
			dwInterrupt_Reg,
			dwInterfaceCtrlReg,
			dwOTBStatusReg,
			dwInterfaceStatusReg);
		strDebug += str;

		{
			LocalHead::CHeadPtrArray & v = p->m_vSlaves;

			for (int i = 0; i < v.GetSize (); i++) {
				strDebug += _T ("\r\nMaster\r\n");

				if (CLocalHead * pSlave = DYNAMIC_DOWNCAST (CLocalHead, v [i])) {
					str.Format (_T ("\tSlave: %s\r\n"), 
						pSlave->m_Config.m_HeadSettings.m_strUID);
					strDebug += str;
				}
			}

			strDebug += "\r\n";

			{
				DWORD dwState = p->GetErrorState ();
				CStringArray vState;

				if (dwState & FoxjetDatabase::HEADERROR_LOWTEMP)		vState.Add (_T ("LOW TEMP"));
				if (dwState & FoxjetDatabase::HEADERROR_HIGHVOLTAGE)	vState.Add (_T ("HIGH VOLTAGE ERROR"));
				if (dwState & FoxjetDatabase::HEADERROR_LOWINK)			vState.Add (_T ("LOW INK"));
				if (dwState & FoxjetDatabase::HEADERROR_OUTOFINK)		vState.Add (_T ("OUT OF INK"));
				if (dwState & FoxjetDatabase::HEADERROR_EP8)			vState.Add (_T ("EP8"));
				if (pHead->m_bDisabled)									vState.Add (_T ("DISABLED"));

				if (!vState.GetSize ())
					vState.Add (_T ("OK"));

				strDebug += _T ("Head state: ");

				for (int i = 0; i < vState.GetSize (); i++) 
					strDebug += vState [i];

				strDebug += _T ("\r\n");
			}

			{
				CString str [] = 
				{
					"DISABLED",
					"RUNNING",
					"IDLE",
					"STOPPED",
				};

				strDebug += _T ("Image state: ") + str [p->m_eTaskState] + _T ("\r\n");
			}
			 
			strDebug += _T ("Task: ") + p->m_sTaskName + _T ("\r\n");
			
			str.Format (_T ("Count: %d\r\n"), p->m_Status.m_lCount);
			strDebug += str;
		}
	}
#endif //__WINPRINTER__

	GetDlgItemText (TXT_DEBUG, strTmp);

	if (strTmp != strDebug) {
		//TRACEF (strDebug);
		SetDlgItemText (TXT_DEBUG, strDebug);
	}

	UpdateData ( FALSE );
}

BOOL CDebugDlg::OnInitDialog() 
{
	m_tmHighVoltage = m_tmLowTemp = m_tmLowInk = CTime::GetCurrentTime ();
	FoxjetCommon::CEliteDlg::OnInitDialog();
	SetWindowText ( m_sTitle );
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDebugDlg::SetTitle(CString sTitle)
{
	m_sTitle.Format (_T ("Debug: %s"), sTitle );
	if ( m_hWnd != NULL )
		SetWindowText ( m_sTitle );
}

void CDebugDlg::OnClipboard() 
{
#ifdef __WINPRINTER__
	CString strTmp, str;

	if (CControlDoc * pDoc = (CControlDoc *)theApp.GetActiveDocument ()) {
		CStringArray vLines;

		pDoc->GetProductLines (vLines);
		
		for (int i = 0; i < vLines.GetSize (); i++) {
			if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
				CPrintHeadList & list = pLine->m_listHeads;

				for (POSITION pos = list.GetStartPosition(); pos != NULL; ) {
					Head::CHead * pHead = NULL;
					DWORD dwKey = 0;

					list.GetNextAssoc (pos, dwKey, (void *&)pHead);

					if (CLocalHead * pLocal = DYNAMIC_DOWNCAST (CLocalHead, pHead)) {
						/*
						if (CDebugDlg * pDlg = pLocal->m_pDbgDlg) {
							str += _T ("----------\r\n");

							pDlg->GetWindowText (strTmp);
							str += strTmp + _T ("\r\n\r\n");

							str += pLocal->GetFriendlyName () + _T ("\r\n\r\n");

							pDlg->GetDlgItemText (IDC_ISR, strTmp);
							str += _T ("ISR: ") + strTmp + _T ("\r\n");

							pDlg->GetDlgItemText (IDC_PHOTO, strTmp);
							str += _T ("PHOTO: ") + strTmp + _T ("\r\n");

							pDlg->GetDlgItemText (IDC_TACH, strTmp);
							str += _T ("TACH: ") + strTmp + _T ("\r\n");

							pDlg->GetDlgItemText (IDC_OTB, strTmp);
							str += _T ("OTB: ") + strTmp + _T ("\r\n");

							pDlg->GetDlgItemText (IDC_SERIAL, strTmp);
							str += _T ("SERIAL: ") + strTmp + _T ("\r\n");

							pDlg->GetDlgItemText (IDC_RESERVED1, strTmp);
							str += _T ("RESERVED1: ") + strTmp + _T ("\r\n");

							pDlg->GetDlgItemText (IDC_RESERVED2, strTmp);
							str += _T ("RESERVED2: ") + strTmp + _T ("\r\n");

							pDlg->GetDlgItemText (TXT_DEBUG, strTmp);
							str += _T ("\r\n") + strTmp + _T ("\r\n");

							str += _T ("\r\n\r\n");
						}
						*/
					}
				}
			}
		}
	}
		

	VERIFY (::OpenClipboard (NULL));
	::EmptyClipboard ();

	HGLOBAL hClipboardText = ::GlobalAlloc (GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT,
		str.GetLength () + 1);
	LPVOID lpCbTextData = ::GlobalLock (hClipboardText);
	memcpy (lpCbTextData, (LPCTSTR)str, str.GetLength ());

	VERIFY (::SetClipboardData (CF_TEXT, hClipboardText) != NULL);

	if (hClipboardText)
		::GlobalUnlock (hClipboardText);

	::CloseClipboard ();
#endif
}

void CDebugDlg::OnHighvoltage() 
{
#ifdef __WINPRINTER__
	#ifdef _DEBUG
	m_tmHighVoltage = CTime::GetCurrentTime ();

	if (CLocalHead * pHead = DYNAMIC_DOWNCAST (CLocalHead, m_pHead)) 
		if (CButton * p = (CButton *)GetDlgItem (CHK_HIGHVOLTAGE))
			if (p->GetCheck () == 1)
				pHead->m_dwSimulatedError |= FoxjetDatabase::HEADERROR_HIGHVOLTAGE;
			else
				pHead->m_dwSimulatedError &= ~FoxjetDatabase::HEADERROR_HIGHVOLTAGE;

	UpdateUI ();
	#endif
#endif
}

void CDebugDlg::OnLowink() 
{
#ifdef __WINPRINTER__
	m_tmLowInk = CTime::GetCurrentTime ();

	#ifdef _DEBUG
	if (CLocalHead * pHead = DYNAMIC_DOWNCAST (CLocalHead, m_pHead)) 
		if (CButton * p = (CButton *)GetDlgItem (CHK_LOWINK))
			if (p->GetCheck () == 1)
				pHead->m_dwSimulatedError |= FoxjetDatabase::HEADERROR_LOWINK;
			else
				pHead->m_dwSimulatedError &= ~FoxjetDatabase::HEADERROR_LOWINK;

	UpdateUI ();
	#endif
#endif
}

void CDebugDlg::OnLowtemp() 
{
#ifdef __WINPRINTER__
	m_tmLowTemp = CTime::GetCurrentTime ();

	#ifdef _DEBUG
	if (CLocalHead * pHead = DYNAMIC_DOWNCAST (CLocalHead, m_pHead)) 
		if (CButton * p = (CButton *)GetDlgItem (CHK_LOWTEMP))
			if (p->GetCheck () == 1)
				pHead->m_dwSimulatedError |= HEADERROR_LOWTEMP;
			else
				pHead->m_dwSimulatedError &= ~HEADERROR_LOWTEMP;

	UpdateUI ();
	#endif
#endif
}

void CDebugDlg::UpdateUI ()
{
	using namespace FoxjetDatabase;

	CTime tmNow				= CTime::GetCurrentTime ();
	CTimeSpan tmHighVoltage = tmNow - m_tmHighVoltage;
	CTimeSpan tmLowTemp		= tmNow - m_tmLowTemp;
	CTimeSpan tmLowInk		= tmNow - m_tmLowInk;

	if (CButton * p = (CButton *)GetDlgItem (CHK_HIGHVOLTAGE))
		p->SetWindowText ("High voltage [" + ToString (tmHighVoltage.GetTotalSeconds ()) + "]");

	if (CButton * p = (CButton *)GetDlgItem (CHK_LOWTEMP))
		p->SetWindowText ("Low temp [" + ToString (tmLowTemp.GetTotalSeconds ()) + "]");

	if (CButton * p = (CButton *)GetDlgItem (CHK_LOWINK))
		p->SetWindowText ("Low ink [" + ToString (tmLowInk.GetTotalSeconds ()) + "]");
}