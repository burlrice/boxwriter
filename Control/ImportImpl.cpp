#include "stdafx.h"
#include "ImportImpl.h"
#include "Export.h"
#include "Debug.h"
#include "FieldDefs.h"
#include "Serialize.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;

CImport::CImport ()
:	m_bFinished (false),
	m_hThread (NULL)
{
}

CImport::~CImport ()
{

	if (m_hThread)
		::WaitForSingleObject (m_hThread, INFINITE);

	LOCK (m_cs);
	m_v.RemoveAll ();
}

void CImport::Build (COdbcDatabase & db)
{
	if (!m_hThread) {
		DWORD dw = 0;

		m_bFinished = false;
		m_pDB = &db;

		m_hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)ThreadFunc, (LPVOID)this, 0, &dw);
		VERIFY (m_hThread);
	}
}

bool CImport::IsFinished () const
{
	return m_bFinished;
}

void CImport::Add (const CString & str)
{
	LOCK (m_cs);
	m_v.Add (str);
}

ULONG CALLBACK CImport::ThreadFunc (LPVOID lpData)
{
	TRACEF ("CImport::ThreadFunc starting");

	ASSERT (lpData);
	CImport & e = * (CImport *)lpData;
	COdbcDatabase & db = * e.m_pDB;
	CStringArray & v = e.m_v;

	LOCK (e.m_cs);

	e.m_bFinished = false;

	{
		DBMANAGETHREADSTATE (db);

		int nResult = FoxjetCommon::Import (db, v, IMPORT_ALL);

		ASSERT (nResult == v.GetSize ());

		e.m_bFinished = true;
	}

	e.m_hThread = NULL;
	TRACEF ("CImport::ThreadFunc exiting");

	return 0;
}

