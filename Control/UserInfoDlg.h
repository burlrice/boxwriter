#if !defined(AFX_USERINFODLG_H__39D404A6_2287_4AD0_9BB9_568292530BBF__INCLUDED_)
#define AFX_USERINFODLG_H__39D404A6_2287_4AD0_9BB9_568292530BBF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserInfoDlg.h : header file
//

#include "Database.h"
#include "RoundButtons.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CUserInfoDlg dialog

class CUserInfoDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CUserInfoDlg(CWnd* pParent = NULL);   // standard constructor
	FoxjetDatabase::USERSTRUCT m_user;

// Dialog Data
	//{{AFX_DATA(CUserInfoDlg)
	CListBox	m_RightsList;
	CString	m_sFirstname;
	CString	m_sLastname;
	CString	m_sPassword1;
	CString	m_sPassword2;
	CString	m_sUsername;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	FoxjetUtils::CRoundButtons m_buttons;

	void DeleteObjects (void);
	bool Validate(void);

	// Generated message map functions
	//{{AFX_MSG(CUserInfoDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERINFODLG_H__39D404A6_2287_4AD0_9BB9_568292530BBF__INCLUDED_)
