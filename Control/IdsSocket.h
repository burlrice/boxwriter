#if !defined(AFX_IDSSOCKET_H__1DF6A43C_2CA7_432D_92C6_3AF87D8B2364__INCLUDED_)
#define AFX_IDSSOCKET_H__1DF6A43C_2CA7_432D_92C6_3AF87D8B2364__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IdsSocket.h : header file
//

#include <afxsock.h>

/////////////////////////////////////////////////////////////////////////////
// CIdsSocket command target

class CIdsSocket : public CAsyncSocket
{
	friend class CIdsThread;

// Attributes
public:

// Operations
public:
	CIdsSocket();
	virtual ~CIdsSocket();

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIdsSocket)
	public:
	virtual void OnReceive(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CIdsSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
	CIdsThread * m_pThread;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IDSSOCKET_H__1DF6A43C_2CA7_432D_92C6_3AF87D8B2364__INCLUDED_)
