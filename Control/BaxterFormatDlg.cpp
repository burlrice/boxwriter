// BaxterFormatDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "BaxterFormatDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBaxterFormatDlg dialog


CBaxterFormatDlg::CBaxterFormatDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(CBaxterFormatDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBaxterFormatDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CBaxterFormatDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBaxterFormatDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBaxterFormatDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CBaxterFormatDlg)
	ON_LBN_DBLCLK(LB_FORMAT, OnDblclkFormat)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBaxterFormatDlg message handlers

BOOL CBaxterFormatDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();

	if (CListBox * pLB = (CListBox *)GetDlgItem (LB_FORMAT)) {
		pLB->AddString (_T ("<PRIMARY>"));
		pLB->AddString (_T ("<SECONDARY>"));
		pLB->AddString (_T ("<SUBLOT>"));

		for (int i = 0; i <= 31; i++) {
			BYTE b [] = { i, 0 };

			pLB->AddString (FoxjetDatabase::Format (b, 1));
		}
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CBaxterFormatDlg::OnDblclkFormat() 
{
	OnOK ();	
}

void CBaxterFormatDlg::OnOK()
{
	if (CListBox * pLB = (CListBox *)GetDlgItem (LB_FORMAT)) {
		int nIndex = pLB->GetCurSel ();

		if (nIndex == CB_ERR) {
			MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
			return;
		}

		pLB->GetText (nIndex, m_str);
		FoxjetCommon::CEliteDlg::OnOK ();
	}
}
