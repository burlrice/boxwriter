// DbStartTaskDlg.cpp : implementation file
//

#include "stdafx.h"
#include "control.h"
#include "Resource.h"
#include "DbStartTaskDlg.h"
#include "DbFieldDlg.h"
#include "Extern.h"
#include "Registry.h"
#include "Database.h"
#include "Parse.h"
#include "DatabaseElement.h"
#include "DatabaseStartDlg.h"
#include "ControlView.h"

using namespace ListGlobals;
using namespace ItiLibrary;
using namespace FoxjetUtils;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetElements;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CDbStartTaskDlg dialog


CDbStartTaskDlg::CDbStartTaskDlg(const CString & strLine, CControlView * pParent)
:	m_nSQLType (0),
	m_pControlView (pParent),
	m_nOffset (0),
	m_bResetCounts (FALSE),
	m_strLine (strLine),
	m_txtKeyValue (this),
	FoxjetCommon::CEliteDlg (IDD_SW0858START_TASK_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CDbStartTaskDlg)
	m_strDSN = _T("");
	m_strKeyField = _T("");
	m_strTable = _T("");
	m_strTaskField = _T("");
	m_strKeyValue = _T("");
	//}}AFX_DATA_INIT
}


void CDbStartTaskDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDbStartTaskDlg)
	DDX_Text(pDX, TXT_DSN, m_strDSN);
	DDX_Text(pDX, TXT_KEYFIELD, m_strKeyField);
	DDX_Text(pDX, TXT_TABLE, m_strTable);
	DDX_Text(pDX, TXT_KEYVALUE, m_strKeyValue);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, CHK_RESETCOUNTS, m_bResetCounts);

	//if (FoxjetDatabase::IsMatrix ()) 
	{
		m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
		m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
		m_buttons.DoDataExchange (pDX, BTN_SELECT);
		m_buttons.DoDataExchange (pDX, BTN_OTHER,	IDB_START);
		m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);
	}

	m_txtKeyValue.DoDataExchange (pDX, TXT_KEYVALUE);
}


BEGIN_MESSAGE_MAP(CDbStartTaskDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDbStartTaskDlg)
	ON_BN_CLICKED(BTN_SELECT, OnSelect)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(BTN_OTHER, OnOther)
	ON_EN_CHANGE(TXT_KEYVALUE, OnKeyValueChanged)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CDbStartTaskDlg message handlers

void CDbStartTaskDlg::OnOther ()
{
	EndDialog (IDCANCEL);
	m_pControlView->DoStart ();
}

void CDbStartTaskDlg::OnSelect() 
{
	CDbFieldDlg dlg (defElements.m_strRegSection, this);

	GetDlgItemText (TXT_DSN, dlg.m_strDSN);
	GetDlgItemText (TXT_TABLE, dlg.m_strTable);
	GetDlgItemText (TXT_KEYFIELD, dlg.m_strField);
	dlg.m_nMaxRows		= 100;
	dlg.m_dwFlags		= ListCtrlImp::CELLCTRL_NOCHANGECOLSEL;

	if (dlg.DoModal () == IDOK) {
		m_strKeyValue = dlg.m_strSelected;
		SetDlgItemText (TXT_KEYVALUE, m_strKeyValue);
	}
}

BOOL CDbStartTaskDlg::OnInitDialog() 
{
	using namespace FoxjetUtils;

	COdbcDatabase::FreeDsnCache ();

	CDatabaseStartDlg dlg (NULL);

	dlg.Load (theApp.m_Database);
	m_strDSN				= dlg.m_strDSN;
	m_strTable				= dlg.m_strTable;
	m_strKeyField			= dlg.m_strKeyField;
	m_strTaskField			= dlg.m_strTaskField;
	m_nSQLType				= dlg.m_nSQLType;
	m_strStart				= dlg.m_strStartField;
	m_strEnd				= dlg.m_strEndField;
	m_strOffsetField		= dlg.m_strOffset;

	FoxjetCommon::CEliteDlg::OnInitDialog();

	SetDlgItemText (TXT_KEYVALUE, theApp.GetProfileString (_T ("sw0858"), _T ("Key value")));

	if (theApp.IsContinuousCount()) {
		m_bResetCounts = FALSE;

		if (CButton* p = (CButton*)GetDlgItem(CHK_RESETCOUNTS)) {
			p->EnableWindow(FALSE);
			p->SetCheck(0);
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CDbStartTaskDlg::OnOK()
{
	if (!UpdateData ())
		return;

	BeginWaitCursor ();

	try {
		COdbcCache db = COdbcDatabase::Find (m_strDSN);
		CString str = COdbcDatabase::CreateSQL (&db.GetDB (), m_strDSN, m_strTable, m_strTaskField, 
			m_strKeyField, m_strKeyValue, m_nSQLType);

		if (IsGojo ()) {
			CString strDate; // sw0867
			CTime tmNow = CTime::GetCurrentTime ();

			bool bPrompt = theApp.LoginPermits (IDM_SELECT_EFFECTIVITY_DATE);

			if (!bPrompt) {
				FoxjetDatabase::SetSelectEffectivityDate (false);

				if (!db.GetDB ().IsSqlServer ())
					strDate.Format (_T (" AND [%s].[%s]<=#%s# AND [%s].[%s]>=#%s#"),
						m_strTable, m_strStart, tmNow.Format (_T ("%m/%d/%Y")),
						m_strTable, m_strEnd, tmNow.Format (_T ("%m/%d/%Y")));
				else
					strDate.Format (_T (" AND [%s].[%s]<='%s' AND [%s].[%s]>='%s'"),
						m_strTable, m_strStart, tmNow.Format (_T ("%m/%d/%Y")),
						m_strTable, m_strEnd, tmNow.Format (_T ("%m/%d/%Y")));

				///*
				#ifdef _DEBUG
				if (!db.GetDB ().IsSqlServer ())
					strDate.Format (_T (" AND [%s].[%s]<=#%s# AND [%s].[%s]>=#%s#"),
						m_strTable, m_strStart, tmNow.Format (_T ("%m/%d/%Y")),
						m_strTable, m_strEnd, tmNow.Format (_T ("%m/%d/%Y"))); 
				else
					strDate.Format (_T (" AND [%s].[%s]<='%s' AND [%s].[%s]>='%s'"),
						m_strTable, m_strStart, tmNow.Format (_T ("%m/%d/%Y")),
						m_strTable, m_strEnd, tmNow.Format (_T ("%m/%d/%Y"))); 

				TRACEF ("***************** TODO: rem");
				#endif
				//*/
			}
			else {
				CDbFieldDlg dlg (defElements.m_strRegSection, this);

				dlg.m_strTitle		= LoadString (IDS_EFFECTIVITYDATE);
				dlg.m_strDSN		= m_strDSN;
				dlg.m_strTable		= str;
				dlg.m_strField		= m_strTaskField;
				dlg.m_nMaxRows		= 100;
				dlg.m_dwFlags		= ListCtrlImp::CELLCTRL_NOCHANGECOLSEL;

				if (dlg.DoModal () == IDOK) {
					COleDateTime dt = COleDateTime::GetCurrentTime ();
					CTime tm = CTime::GetCurrentTime ();
					TRACEF (dlg.m_strSelected);

					if (dlg.m_strSelected.GetLength ()) {
						COdbcCache db = COdbcDatabase::Find (m_strDSN);
						COdbcCache rst = COdbcDatabase::Find (db.GetDB (), dlg.m_strTable);
						CString strDate = rst.GetRst ().GetFieldValue (m_strStart); // = dlg.m_pSelected->GetDispText (nIndex);

						TRACEF (strDate);
						dt.ParseDateTime (strDate);
						tm = CTime (dt.GetYear (), dt.GetMonth (), dt.GetDay (), dt.GetHour (), dt.GetMinute (), dt.GetSecond ());
						TRACEF (dt.Format (_T ("%c")));
						TRACEF (tm.Format (_T ("%c")));
					}
					
					FoxjetDatabase::SetSelectEffectivityDate (true, tm);

					if (IsGojo ()) {
						m_strKeyValue = dlg.m_mapResult [m_strKeyField];
						m_strTaskValue = dlg.m_mapResult [m_strTaskField];
						m_nOffset = _ttoi (dlg.m_mapResult [m_strOffsetField]);
						EndWaitCursor ();
						FoxjetCommon::CEliteDlg::OnOK ();

						{
							CString strKey = CControlApp::GetRegKey () + _T ("\\Settings\\CStartTaskDlg");
							ULONG lTaskID = GetTaskID (ListGlobals::GetDB (), m_strTaskValue, m_strLine, GetPrinterID (ListGlobals::GetDB ()));

							FoxjetDatabase::WriteProfileInt (strKey, _T ("m_lTaskID"), lTaskID);
							FoxjetDatabase::WriteProfileString (strKey, _T ("strTask"), m_strTaskValue);
							FoxjetDatabase::WriteProfileInt (strKey, _T ("m_bResetCounts"), m_bResetCounts);
						}

						return;
					}


					str = COdbcDatabase::CreateSQL (&db.GetDB (), m_strDSN, m_strTable, m_strTaskField, m_strTaskField, dlg.m_strSelected, m_nSQLType, false);
					TRACEF (str);
					strDate = _T ("");
				}
				else {
					FoxjetCommon::CEliteDlg::OnCancel ();
					return;
				}
			}
		}

		TRACEF (m_strDSN + _T (": ") + str);
		COdbcCache rstCache = COdbcDatabase::Find (db.GetDB (), m_strDSN, str);
		COdbcRecordset & rst = rstCache.GetRst ();

		if (!rst.IsEOF ()) {
			m_strKeyValue = rst.GetFieldValue (m_strKeyField);
			m_strTaskValue = rst.GetFieldValue (m_strTaskField);

			if (IsGojo ()) 
				m_nOffset = _ttoi ((CString)rst.GetFieldValue (m_strOffsetField));
		}
		else {
			if (IsGojo ()) {
				COdbcRecordset rstTmp  (&db.GetDB ());

				m_strKeyValue = _T ("");
				m_strTaskValue = _T ("");
				m_nOffset = 0;

				TRACEF (str);
				rstTmp.Open (str);
		
				if (!rstTmp.IsEOF ()) 
					MsgBox (* this, LoadString (IDS_EFFECIVITYNOTMET));
				else
					MsgBox (* this, LoadString (IDS_SKUNOTFOUND));

				if (CEdit * p = (CEdit *)GetDlgItem (TXT_KEYVALUE)) 
					p->SetSel (0, -1);

				return;
			}
		}

		{
			CString strKey = CControlApp::GetRegKey () + _T ("\\Settings\\CStartTaskDlg");
			ULONG lTaskID = GetTaskID (ListGlobals::GetDB (), m_strTaskValue, m_strLine, GetPrinterID (ListGlobals::GetDB ()));

			FoxjetDatabase::WriteProfileInt (strKey, _T ("m_lTaskID"), lTaskID);
			FoxjetDatabase::WriteProfileString (strKey, _T ("strTask"), m_strTaskValue);
			FoxjetDatabase::WriteProfileInt (strKey, _T ("m_bResetCounts"), m_bResetCounts);
		}

		FoxjetCommon::CEliteDlg::OnOK ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	EndWaitCursor ();
}

std::vector <std::wstring> CDbStartTaskDlg::CEditEx::OnTyping (const CString & str)
{
	std::vector <std::wstring> v;
	const int nRecords = 10;

	try {
		CString strSQL;
		COdbcCache db = COdbcDatabase::Find (m_pParent->m_strDSN);

		strSQL.Format (_T ("SELECT TOP %d [%s] FROM [%s] WHERE [%s] LIKE '%s%%' ORDER BY [%s] ASC"), 
			nRecords + 1, 
			m_pParent->m_strKeyField, 
			m_pParent->m_strTable, 
			m_pParent->m_strKeyField, 
			str, 
			m_pParent->m_strKeyField);
		//TRACEF (strSQL);

		COdbcRecordset rst (db.GetDB ());

		if (rst.Open (strSQL)) {
			while (!rst.IsEOF ()) {
				CString str = (CString)rst.GetFieldValue ((short)0);
				v.push_back (std::wstring (str));
				//TRACEF (FoxjetDatabase::ToString ((int)v.size ()) + _T (": ") + str);
				rst.MoveNext ();
			}
		}
	}
	catch (CDBException * e)		{ Enable (false); HANDLEEXCEPTION_TRACEONLY (e); }
	catch (CMemoryException * e)	{ Enable (false); HANDLEEXCEPTION_TRACEONLY (e); }

	if (v.size () >= nRecords) {
		v.pop_back ();
		v.push_back (std::wstring ((LPCTSTR)CEditEx::m_strMore));
	}
	
	return v;
}

void CDbStartTaskDlg::OnKeyValueChanged ()
{
	if (!theApp.IsContinuousCount()) {
		if (CButton* pReset = (CButton*)GetDlgItem(CHK_RESETCOUNTS))
			pReset->SetCheck(1);
	}
}
