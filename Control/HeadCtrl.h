#if !defined(AFX_HEADCTRL_H__E5E17B37_9FDF_4E20_964B_45FC2204FDB0__INCLUDED_)
#define AFX_HEADCTRL_H__E5E17B37_9FDF_4E20_964B_45FC2204FDB0__INCLUDED_

#include "Database.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HeadCtrl.h : header file
//

#include "Database.h"
#include "Head.h"

/////////////////////////////////////////////////////////////////////////////
// CHeadCtrl window
namespace HeadCtrl
{
	class CSelection
	{
	public:
		CSelection ();
		CSelection (const CSelection & rhs);
		CSelection & operator = (const CSelection & rhs);
		virtual ~CSelection ();

		Head::CHead * m_pThread;
		ULONG	m_lPanelID;
	};

	class CHeadItem
	{
	public:
		CHeadItem (DWORD dwThreadID, ULONG lPanelID = -1);
		CHeadItem (const FoxjetDatabase::HEADSTRUCT & h, ULONG lPanelID = -1);
		virtual ~CHeadItem ();

		ULONG GetHeight () const;
		ULONG GetPanelID () const;
		ULONG GetHeadID () const;

		bool GetRedraw ();

		DWORD							m_dwThreadID;
		Head::CHeadInfo					m_info;
		FoxjetDatabase::HEADSTRUCT *	m_pHead;
		CString							m_strPanel;
		ULONG							m_lPanelID;
		bool							m_bRedraw;

	protected:
		CString ToString ();

		CString m_strLastState;
	};

	class CHeadCtrl : public CListCtrl
	{
	public:
	// Construction
	public:
		CHeadCtrl();

	// Attributes
	public:

	// Operations
	public:
		bool Create (CWnd * pParent);
	
	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CHeadCtrl)
		public:
		virtual BOOL DestroyWindow();
		//}}AFX_VIRTUAL

	// Implementation
	public:
		virtual ~CHeadCtrl();

		COLORREF GetIconColor (const CHeadItem & h);
		COLORREF GetStateColor (int nIndex, DWORD dwState);

		int Insert (Head::CHead * p, ULONG lPanelID = -1);
		int Insert (const FoxjetDatabase::HEADSTRUCT & h, ULONG lPanelID = -1);

		int GetIndex (DWORD dwThreadID) const;
		int GetSelectedIndex () const;

		bool Delete (int nIndex);
		void Select (int nIndex);
		void Redraw (int nIndex);

		CSelection GetSelection (int nIndex);

		void ResetContent ();

		int Find (ULONG lHeadID);
		void Resize ();

		CFont	m_fnt;

		// Generated message map functions
	protected:
		virtual void DrawItem( LPDRAWITEMSTRUCT lpDrawItemStruct );
		int GetFirst (ULONG lPanelID);
		const FoxjetDatabase::HEADSTRUCT * GetStruct (int nIndex);
		int FindStruct (ULONG lHeadID);
		int Insert (CHeadItem * pInsert);

		//{{AFX_MSG(CHeadCtrl)
		afx_msg void OnSize(UINT nType, int cx, int cy);
		//}}AFX_MSG
		afx_msg void MeasureItem ( LPMEASUREITEMSTRUCT lpMeasureItemStruct );

		DECLARE_MESSAGE_MAP()
	};
}; //namespace HeadCtrl

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEADCTRL_H__E5E17B37_9FDF_4E20_964B_45FC2204FDB0__INCLUDED_)
