#ifndef __CONTROL_IMPORTIMPL_H__
#define __CONTROL_IMPORTIMPL_H__

#include <afxtempl.h>
#include "Database.h"

class CImport
{
public:
	CImport ();
	virtual ~CImport ();

	void Build (FoxjetDatabase::COdbcDatabase & db);
	void Add (const CString & str);
	bool IsFinished () const;

private:
	static ULONG CALLBACK ThreadFunc (LPVOID lpData);

	FoxjetDatabase::COdbcDatabase * m_pDB;
	bool m_bFinished;
	CStringArray m_v;
	CDiagnosticCriticalSection m_cs;
	HANDLE m_hThread;
};

#endif //__CONTROL_IMPORTIMPL_H__