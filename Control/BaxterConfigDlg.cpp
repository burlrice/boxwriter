// BaxterConfigDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "control.h"
#include "BaxterConfigDlg.h"
#include "BaxterFormatDlg.h"
#include "Debug.h"
#include "Database.h"
#include "Parse.h"

using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBaxterConfigDlg dialog


CBaxterConfigDlg::CBaxterConfigDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(CBaxterConfigDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBaxterConfigDlg)
	m_nMaxLen = 0;
	//}}AFX_DATA_INIT

	m_bVerify			= FALSE;
	m_strOuputFormat	= _T ("<DC1>ABC<PRIMARY>DEF<DC2><SECONDARY>XYZ<SUBLOT><CR>");
	m_nTaskLen			= 7;
	m_nOvertureLen1		= 6;
	m_nOvertureLen2		= 6;
	m_nOvertureLen3		= 7;
	m_nPrimaryLenReq	= 16;
	m_nPrimaryLenOpt	= 0;
	m_nSecondaryLenReq	= 17;
	m_nSecondaryLenOpt	= 0;
	m_nUserLenOpt		= 0;
	m_nMatchLen			= 5;

	VERIFY (m_bmpFormat.LoadBitmap (IDB_PLUS));
}


void CBaxterConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBaxterConfigDlg)
	DDX_Check(pDX, CHK_VERIFY, m_bVerify);
	DDX_Text(pDX, TXT_OUTPUT_FORMAT, m_strOuputFormat);
	DDX_Text(pDX, TXT_TASK_LEN, m_nTaskLen);
	DDX_Text(pDX, TXT_OVERTURE_LEN1, m_nOvertureLen1);
	DDX_Text(pDX, TXT_OVERTURE_LEN2, m_nOvertureLen2);
	DDX_Text(pDX, TXT_OVERTURE_LEN3, m_nOvertureLen3);
	DDX_Text(pDX, TXT_PRIMARY_LEN_REQUIRED, m_nPrimaryLenReq);
	DDX_Text(pDX, TXT_PRIMARY_LEN_OPTIONAL, m_nPrimaryLenOpt);
	DDX_Text(pDX, TXT_SECONDARY_LEN_REQUIRED, m_nSecondaryLenReq);
	DDX_Text(pDX, TXT_SECONDARY_LEN_OPTIONAL, m_nSecondaryLenOpt);
	DDX_Text(pDX, TXT_USER_LEN_OPTIONAL, m_nUserLenOpt);
	DDX_Text(pDX, TXT_MATCH_LEN, m_nMatchLen);
	DDX_Text(pDX, TXT_MAX_LEN, m_nMaxLen);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBaxterConfigDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CBaxterConfigDlg)
	ON_BN_CLICKED(BTN_FORMAT, OnFormat)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBaxterConfigDlg message handlers

BOOL CBaxterConfigDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	if (CButton * p = (CButton *)GetDlgItem (BTN_FORMAT))
		p->SetBitmap (m_bmpFormat);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CBaxterConfigDlg::OnFormat() 
{
	CBaxterFormatDlg dlg (this);
	
	if (dlg.DoModal () == IDOK) {
		if (CEdit * p = (CEdit *)GetDlgItem (TXT_OUTPUT_FORMAT)) {
			int nStart = 0, nEnd = 0;
			CString str;

			p->GetWindowText (str);
			p->GetSel (nStart, nEnd);
			str.Insert (nEnd, dlg.m_str);
			p->SetWindowText (str);
			p->SetSel (nEnd + dlg.m_str.GetLength (), nEnd + dlg.m_str.GetLength ());
		}
	}
}

void CBaxterConfigDlg::Load()
{
	SETTINGSSTRUCT s;

	if (GetSettingsRecord (theApp.m_Database, 0, _T ("BaxterConfig"), s)) {
		CStringArray v;
		int nIndex = 0;

		FromString (s.m_strData, v);

		m_bVerify			= _ttoi (GetParam (v, nIndex++)) ? TRUE : FALSE;
		m_strOuputFormat	= UnformatString (GetParam (v, nIndex++));
		m_nTaskLen			= _ttoi (GetParam (v, nIndex++));
		m_nOvertureLen1		= _ttoi (GetParam (v, nIndex++));
		m_nOvertureLen2		= _ttoi (GetParam (v, nIndex++));
		m_nOvertureLen3		= _ttoi (GetParam (v, nIndex++));
		m_nPrimaryLenOpt	= _ttoi (GetParam (v, nIndex++));
		m_nPrimaryLenReq	= _ttoi (GetParam (v, nIndex++));
		m_nSecondaryLenOpt	= _ttoi (GetParam (v, nIndex++));
		m_nSecondaryLenReq	= _ttoi (GetParam (v, nIndex++));
		m_nUserLenOpt		= _ttoi (GetParam (v, nIndex++));

		TRACEF (FoxjetDatabase::Format ((LPBYTE)(LPCSTR)w2a (m_strOuputFormat), m_strOuputFormat.GetLength ()));
		m_strOuputFormat.Replace (_T ("<_CR>"), _T ("\r"));
		TRACEF (m_strOuputFormat);
		m_strOuputFormat = FoxjetDatabase::Format ((LPBYTE)(LPCSTR)w2a (m_strOuputFormat), m_strOuputFormat.GetLength ());
		TRACEF (m_strOuputFormat);
	}
}

void CBaxterConfigDlg::Save()
{
	CStringArray v;
	SETTINGSSTRUCT s;
	CString strOuputFormat = m_strOuputFormat;

	strOuputFormat = FormatString (FoxjetDatabase::Format ((LPBYTE)(LPCSTR)w2a (m_strOuputFormat), m_strOuputFormat.GetLength ()));
	strOuputFormat.Replace (_T ("<CR>"), _T ("<_CR>"));
	TRACEF (strOuputFormat);

	v.Add (ToString (m_bVerify));
	v.Add (strOuputFormat);
	v.Add (ToString (m_nTaskLen));
	v.Add (ToString (m_nOvertureLen1));
	v.Add (ToString (m_nOvertureLen2));
	v.Add (ToString (m_nOvertureLen3));
	v.Add (ToString (m_nPrimaryLenOpt));
	v.Add (ToString (m_nPrimaryLenReq));
	v.Add (ToString (m_nSecondaryLenOpt));
	v.Add (ToString (m_nSecondaryLenReq));
	v.Add (ToString (m_nUserLenOpt));

	s.m_lLineID = 0;
	s.m_strKey	= _T ("BaxterConfig");
	s.m_strData	= ToString (v);

	if (!UpdateSettingsRecord (theApp.m_Database, s))
		VERIFY (AddSettingsRecord (theApp.m_Database, s));
}
