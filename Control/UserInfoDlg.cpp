// UserInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "Control.h"
#include "UserInfoDlg.h"
#include "CfgUsersDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////
// CUserInfoDlg dialog


CUserInfoDlg::CUserInfoDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg (IDD_USER_INFO_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CUserInfoDlg)
	m_sFirstname = _T("");
	m_sLastname = _T("");
	m_sPassword1 = _T("");
	m_sPassword2 = _T("");
	m_sUsername = _T("");
	//}}AFX_DATA_INIT
}


void CUserInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserInfoDlg)
	DDX_Control(pDX, IDC_RIGHTSLIST, m_RightsList);
	DDX_Text(pDX, IDC_FIRSTNAME, m_sFirstname);
	DDV_MaxChars(pDX, m_sFirstname, 25);
	DDX_Text(pDX, IDC_LASTNAME, m_sLastname);
	DDV_MaxChars(pDX, m_sLastname, 25);
	DDX_Text(pDX, IDC_PASSWORD1, m_sPassword1);
	DDV_MaxChars(pDX, m_sPassword1, 25);
	DDX_Text(pDX, IDC_PASSWORD2, m_sPassword2);
	DDV_MaxChars(pDX, m_sPassword2, 25);
	DDX_Text(pDX, IDC_USERNAME, m_sUsername);
	DDV_MaxChars(pDX, m_sUsername, 25);
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);
}


BEGIN_MESSAGE_MAP(CUserInfoDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CUserInfoDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserInfoDlg message handlers

void CUserInfoDlg::OnOK() 
{
	SECURITYGROUPSTRUCT *pRS;

	if ( !Validate () )
		return;
	
	m_user.m_strFirstName = m_sFirstname;
	m_user.m_strLastName = m_sLastname;
	m_user.m_strUserName = m_sUsername;
	m_user.m_strPassW = FoxjetDatabase::Encrypt (m_sPassword1);

	int iSel = m_RightsList.GetCurSel();
	pRS = (	SECURITYGROUPSTRUCT * ) m_RightsList.GetItemData( iSel );
	m_user.m_lSecurityGroupID = pRS->m_lID;

	DeleteObjects();
	FoxjetCommon::CEliteDlg::OnOK();
}

BOOL CUserInfoDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	SECURITYGROUPSTRUCT *pRS;
	CArray <SECURITYGROUPSTRUCT, SECURITYGROUPSTRUCT &> v;
	int index, iSel = -1;

	ASSERT (GetDlgItem (IDC_USERNAME));
	ASSERT (GetDlgItem (IDC_FIRSTNAME));

	GetGroupsRecords (theApp.m_Database, v);
	for ( int i= 0; i < v.GetSize(); i++) {
		pRS = new SECURITYGROUPSTRUCT;
		*pRS = ( SECURITYGROUPSTRUCT )v.GetAt(i);
		index = m_RightsList.AddString ( pRS->m_strName );
		if ( m_user.m_lSecurityGroupID == pRS->m_lID )
			m_RightsList.SetCurSel ( index );
		m_RightsList.SetItemData ( index, (DWORD)pRS );
	}
	m_sPassword1 = m_sPassword2 = m_user.m_strPassW;
	m_sFirstname = m_user.m_strFirstName;
	m_sLastname = m_user.m_strLastName;
	m_sUsername = m_user.m_strUserName;

	UpdateData (false);
	
	m_RightsList.SetItemHeight (0, FoxjetDatabase::GetListCtrlItemHeight ());

	if (!m_sUsername.IsEmpty())
		GetDlgItem (IDC_USERNAME)->EnableWindow (false);
	
	if (!m_sUsername.CompareNoCase (CCfgUsersDlg::m_strAdmin))
		if (CWnd * p = GetDlgItem (IDC_RIGHTSLIST))
			p->EnableWindow (FALSE);

	GetDlgItem (IDC_FIRSTNAME)->SetFocus();

	return FALSE;
}

bool CUserInfoDlg::Validate()
{
	UpdateData (true);
	if ( m_sPassword2.CompareNoCase (m_sPassword1) != 0 ) {
		MsgBox (LoadString (IDS_PASSWORDS_DONT_MATCH));
		m_user.m_strPassW.Empty();
		m_sPassword2.Empty();
		UpdateData (false);
		return false;
	}
	
	if (( m_sFirstname.IsEmpty() ) || ( m_sLastname.IsEmpty() ) || ( m_sUsername.IsEmpty() )) {
		MsgBox (LoadString (IDS_FIELD_CANNOT_BE_BLANK));
		return false;
	}

	if ( m_RightsList.GetCurSel() == -1) {
		MsgBox (LoadString (IDS_INVALID_ACCESSRIGHT_SELECTION));
		return false;
	}
	return true;
}

void CUserInfoDlg::DeleteObjects()
{
	SECURITYGROUPSTRUCT *pRS;
	int nItems = m_RightsList.GetCount();
	for ( int i=0; i < nItems; i++ ) { 
		pRS = ( SECURITYGROUPSTRUCT * ) m_RightsList.GetItemData(i);
		delete pRS;
	}
}

void CUserInfoDlg::OnCancel() 
{
	DeleteObjects();	
	FoxjetCommon::CEliteDlg::OnCancel();
}
