#pragma once

#include "Utils.h"
#include "resource.h"
#include "BackupDlg.h"
#include "ListCtrlImp.h"

// CNewDeviceDlg dialog

class CNewDeviceDlg : public FoxjetCommon::CEliteDlg
{
	class CImportItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CImportItem (const CString & strFile);

		virtual CString GetDispText (int nColumn) const;

		CString m_strLine;
		CString m_strName;
		const CString m_strFile;
	};

	DECLARE_DYNAMIC(CNewDeviceDlg)

public:
	CNewDeviceDlg(const CString & strDevice, const CString & strUID, CWnd* pParent = NULL);   // standard constructor
	virtual ~CNewDeviceDlg();

// Dialog Data
	enum { IDD = IDD_NEW_DEVICE };

protected:
	FoxjetUtils::CRoundButtons m_buttons;
	CString m_strDevice;
	CString m_strUID;
	CMapStringToString m_map;
	CBackupDlg * m_pdlgBackup;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBackup();
	afx_msg void OnBnClickedRestore();
	afx_msg void OnBnClickedInstall();
	afx_msg void OnBnClickedImport ();
	afx_msg void OnItemchangedImport(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnAll ();

	afx_msg LRESULT OnDeviceChange (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnQueryCancelAutoplay (WPARAM wParam, LPARAM lParam);

	static ULONG CALLBACK GetFilesFunc(LPVOID lpData);

	virtual BOOL OnInitDialog();
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
	DWORD m_dwRot;
};
