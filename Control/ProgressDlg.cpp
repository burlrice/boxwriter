// ProgressDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "Control.h"
#include "Mainfrm.h"
#include "ProgressDlg.h"
#include "Debug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProgressDlg dialog


CProgressDlg::CProgressDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg (IDD_PROGRESS_DLG_MATRIX /* IsMatrix () ? IDD_PROGRESS_DLG_MATRIX : IDD_PROGRESS_DLG */ , pParent) 
{
	Create (IDD_PROGRESS_DLG_MATRIX /* IsMatrix () ? IDD_PROGRESS_DLG_MATRIX : IDD_PROGRESS_DLG */ , pParent );
	//{{AFX_DATA_INIT(CProgressDlg)
	m_sMessage = _T("");
	//}}AFX_DATA_INIT
}

CProgressDlg::~CProgressDlg ()
{
	DestroyWindow ();
	//TRACEF (_T ("CProgressDlg::~CProgressDlg ()"));
}

void CProgressDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProgressDlg)
	DDX_Text(pDX, IDC_MESSAGE, m_sMessage);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CProgressDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CProgressDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProgressDlg message handlers

int CProgressDlg::AddProcess(ULONG lID, CString sMsg, LPCTSTR lpszFile, ULONG lLine)
{
//	if (sMsg.GetLength ()) {
		CString sData = sMsg + _T("\r\n");
		m_mapMsg.SetAt ( lID, sData );
		m_sMessage.Empty();

		#ifdef _DEBUG
		{ CString str; str.Format (_T ("AddProcess: [%d]: %s"), lID, sMsg); CDebug::Trace (str, true, lpszFile, lLine); }
		#endif

		ULONG Key;
		POSITION pos = m_mapMsg.GetStartPosition();
		
		while ( pos )
		{
			m_mapMsg.GetNextAssoc ( pos, Key, sData );
			m_sMessage += sData;
		}
		UpdateData ( false );
		UpdateWindow();
//	}

	return m_mapMsg.GetCount();
}

int CProgressDlg::RemoveProcess (ULONG lID, LPCTSTR lpszFile, ULONG lLine)
{
	m_sMessage.Empty();
	m_mapMsg.RemoveKey ( lID );

	#ifdef _DEBUG
	{ CString str; str.Format (_T ("RemoveProcess: [%d]"), lID); CDebug::Trace (str, true, lpszFile, lLine); }
	#endif

	CString sData;
	ULONG Key;
	POSITION pos = m_mapMsg.GetStartPosition();
	
	while ( pos )
	{
		m_mapMsg.GetNextAssoc ( pos, Key, sData );
		m_sMessage += sData;
	}
	UpdateData ( false );
	return m_mapMsg.GetCount();
}
