// HeadCfg.cpp: implementation of the CHeadCfg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Resource.h"
#include <math.h>
#include "Control.h"
#include "HeadCfg.h"
#include "fj_socket.h"
#include "fj_printhead.h"
#include "BarcodeElement.h"
#include "Edit.h"
#include "Debug.h"
#include "Version.h"
#include "HeadDlg.h"
#include "Utils.h"
#include "NetCommDlg.h"
#include "Parse.h"
#include "List.h"
#include "Database.h"
#include "Head.h"

using namespace FoxjetDatabase;
using namespace Head;

#ifdef __IPPRINTER__
	#include "DynamicTableDlg.h"
	#include "RemoteHead.h"
	#include "DataMatrixElement.h"
#endif

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHeadCfg::CHeadCfg()
{
}

CHeadCfg::~CHeadCfg()
{

}

const FoxjetDatabase::HEADSTRUCT & CHeadCfg::GetHeadSettings() const
{
	return m_HeadSettings;
}

bool CHeadCfg::FromString(CString sConfig)
{
	CString sValue;
	CMapStringToString mapKeywords;

	Parse (sConfig, mapKeywords);
	
	if ( mapKeywords.Lookup (_T ("PRINTER_RESOLUTION"), sValue) )
		m_HeadSettings.m_nHorzRes = _ttoi (sValue);

	if ( mapKeywords.Lookup (_T ("DIRECTION"), sValue) )
	{
		if ( sValue.CompareNoCase (_T ("Left_To_Right")) == 0)
			m_HeadSettings.m_nDirection = FoxjetDatabase::LTOR;
		else
			m_HeadSettings.m_nDirection = FoxjetDatabase::RTOL;
	}

	if ( mapKeywords.Lookup (_T ("SLANTANGLE"), sValue) )
		m_HeadSettings.m_dHeadAngle = FoxjetDatabase::_ttof (sValue);

	if ( mapKeywords.Lookup (_T ("DISTANCE"), sValue) ) 
		m_HeadSettings.m_lPhotocellDelay = (ULONG)FoxjetDatabase::_ttof (sValue) * 1000;

	if ( mapKeywords.Lookup (_T ("HEIGHT"), sValue) ) 
		m_HeadSettings.m_lRelativeHeight = (ULONG)FoxjetDatabase::_ttof ( sValue ) * 1000;
	
	if ( mapKeywords.Lookup (_T ("PHOTOCELL_BACK"), sValue) )
	{
		if ( sValue.CompareNoCase (_T ("T")) )
			m_HeadSettings.m_nSharePhoto = 1;
		else
			m_HeadSettings.m_nSharePhoto = 0;
	}
	
//	sBuffer += m_TimeCodeSettings.m_strData;
//	sBuffer += m_DateCodeSettings.m_strData;
//	sBuffer += m_BarCodeSettings.m_strData;

	if ( mapKeywords.Lookup (_T ("ENCODER"), sValue) )
	{
		m_HeadSettings.m_nEncoder = 0;
		if ( sValue.CompareNoCase (_T ("T")) )
			m_HeadSettings.m_nEncoder = 1;
	}

	if ( mapKeywords.Lookup (_T ("ENCODER_COUNT"), sValue) )
		m_HeadSettings.m_nHorzRes = (ULONG)FoxjetDatabase::_ttof ( sValue );
	if ( mapKeywords.Lookup (_T ("ENCODER_WHEEL"), sValue) )
		m_HeadSettings.m_nHorzRes = (ULONG)FoxjetDatabase::_ttof ( sValue );
	if ( mapKeywords.Lookup (_T ("CONVEYOR_SPEED"), sValue) )
		m_HeadSettings.m_nIntTachSpeed = (ULONG)FoxjetDatabase::_ttof ( sValue );

	return false;
}

bool CHeadCfg::LoadSettings( FoxjetDatabase::COdbcDatabase & Database, CString sUid )
{
	bool result = true;
	CString sMsg;

	LONG headID = FoxjetDatabase::GetHeadID (Database, sUid, FoxjetDatabase::GetPrinterID (theApp.m_Database));

	return LoadSettings (Database, headID);
}

bool CHeadCfg::LoadSettings( FoxjetDatabase::COdbcDatabase & Database, ULONG lHeadID)
{
	bool result = true;
	CString sMsg;

	if ( lHeadID > 0 )
	{
		if ( !FoxjetDatabase::GetHeadRecord( Database, lHeadID, m_HeadSettings ) )
		{
			result = false;
			sMsg.Format ( IDS_LOAD_HEAD_RECORD_FAILED, FoxjetDatabase::ToString (lHeadID));
			MsgBox ( sMsg, MB_ICONSTOP);
		}

		if ( !FoxjetDatabase::GetPanelRecord (Database, m_HeadSettings.m_lPanelID, m_BoxPanel) )
		{
			result = false;
			sMsg.Format ( IDS_LOAD_PANEL_RECORD_FAILED, FoxjetDatabase::ToString (lHeadID));
			MsgBox ( sMsg, MB_ICONSTOP);
		}

		if ( !FoxjetDatabase::GetLineRecord (Database, m_BoxPanel.m_lLineID, m_Line) )
		{
			result = false;
			sMsg.Format ( IDS_LOAD_LINE_RECORD_FAILED, FoxjetDatabase::ToString (lHeadID));
			MsgBox ( sMsg, MB_ICONSTOP);
		}

#if defined ( __IPPRINTER__ )
		FoxjetIpElements::LoadBarcodeParams (Database, GlobalVars::CurrentVersion);
		m_BarCodeSettings	= FoxjetIpElements::CBarcodeElement::GetBarcodeSettings( Database, m_Line.m_lID, m_HeadSettings.m_nHeadType );
		m_BarCodeSettings	+= CRemoteHead::m_cBreak;
		m_BarCodeSettings	+= FoxjetIpElements::CDataMatrixElement::GetBarcodeSettings( Database, m_Line.m_lID, m_HeadSettings.m_nHeadType );
		FoxjetDatabase::GetSettingsRecord (Database, m_Line.m_lID, ListGlobals::Keys::m_strTime, m_TimeCodeSettings);
		FoxjetDatabase::GetSettingsRecord (Database, m_Line.m_lID, ListGlobals::Keys::m_strDays, m_DateCodeSettings);
#else
		//FoxjetCommon::LoadSystemParams (Database, GlobalVars::CurrentVersion);
		FoxjetElements::LoadBarcodeParams (Database, GlobalVars::CurrentVersion);
		FoxjetElements::LoadShiftcodes (Database, GlobalVars::CurrentVersion);
#endif
	}
	else
	{
		// Either we have been deleted of our UID has changed.
		if ( !FoxjetDatabase::GetHeadRecord( Database, m_HeadSettings.m_lID, m_HeadSettings ) )
		{
			// We have been deleted
			result = false;
//			sMsg.Format ( IDS_LOAD_HEAD_RECORD_FAILED, sUid );
//			MsgBox ( sMsg, MB_ICONSTOP);
		}
		else
		{
			result = false;
//			sMsg.Format (LoadString (IDS_ADDRCHANGE), sUid);
//			MsgBox ( sMsg );
		}
	}
	return result;
}

// N-factor is the number of encoder pulse required to cover the 
// distance from one channel to the next channel of the print head.
int CHeadCfg::CalculateNFactor(double angle, double res)
{
	
#if defined (__IPPRINTER__)
	int t = Foxjet3d::CHeadDlg::CalcNFactor (angle, res);

#else
	double NFactor;

	if (angle >= 90.0) {
		switch (m_HeadSettings.m_nHeadType) {
		case FoxjetDatabase::UJ2_352_32:
		case FoxjetDatabase::GRAPHICS_384_128:
		case FoxjetDatabase::GRAPHICS_768_256: 
		case UR2:
		case UR4:
		{
			if (m_HeadSettings.m_nHorzRes == 426)
				return 8;

			if (FoxjetCommon::IsProductionConfig () || m_HeadSettings.IsUSB ()) {
				switch (m_HeadSettings.m_nEncoderDivisor) {
				case 2:	return 6;	// 150
				case 3:	return 8;	// 200
				case 1:	return 12;	// 300
				}
			}

			return 6;
		}
		case FoxjetDatabase::GRAPHICS_768_256_L310:
			return 46;

		case FoxjetDatabase::IV_72:
		{
			// 0.192" is channel separation for valve heads (0.040" for others?)
			int n = (int)(0.192 * res); // 0.192 / (1 / res)

			return n;
		}
		default:
			return 0;
		}
	}

	NFactor = 0.05848 * cos ( DEG2RAD(angle) );
//	NFactor *= res / 2;
	NFactor *= res;
	NFactor += .5;

	int n = Foxjet3d::CHeadDlg::CalcNFactor (angle, res);
	int t = ( int ) floor(NFactor);

	{ CString str; str.Format (_T ("CalculateNFactor (angle: %f, res: %f): %d"), angle, res, t); TRACEF (str); }

#endif

	return t;
}

void CHeadCfg::ToString(int Section, CString &sBuffer, const Head::CHead * pHead)
{
#ifdef __IPPRINTER__
	FoxjetCommon::ToString (theApp.m_Database, m_HeadSettings, &m_Line, Section, sBuffer);
#endif //__IPPRINTER__
}

bool CHeadCfg::IsMaster() const
{
	bool result = false;
	if ( m_HeadSettings.m_bMasterHead )
		result = true;
	return result;
}

#ifdef __WINPRINTER__
void CHeadCfg::ToRegisters (USB::SetupRegs &tConfig, struct USB::FireVibPulse &tPulseRegs, const Head::CHeadInfo & info)
{
	tagSetupRegs c;
	tagFireVibPulse t;

	ToRegisters (c, t, info);

	tPulseRegs._DampPulseDelay	= t._DampPulseDelay;
	tPulseRegs._DampPulseWidth	= t._DampPulseWidth;
	tPulseRegs._FirePulseDelay	= t._FirePulseDelay;
	tPulseRegs._FirePulseWidth	= t._FirePulseWidth;
	tPulseRegs._v5				= t._v5;
	tPulseRegs._v6				= t._v6;
	tPulseRegs._v7				= t._v7;
	tPulseRegs._v8				= t._v8;

	/*
	{ 
		CString str;
	
		str.Format (_T ("tagFireVibPulse: [%d %d %d %d] [%d %d %d %d]"), 
			t._DampPulseDelay,
			t._DampPulseWidth,
			t._FirePulseDelay,
			t._FirePulseWidth,
			t._v5,			
			t._v6,			
			t._v7,			
			t._v8);			
		TRACEF (str);
	}
	*/

	tConfig._CtrlReg._PrintMode			= 0;
	tConfig._CtrlReg._PrintBuffer		= 0;
	//tConfig._DividerCtrlReg._SClkRate	= 1;
	//tConfig._CtrlReg._FireAMS			= 0x00;
	tConfig._CtrlReg._DoublePulse		= m_HeadSettings.m_bDoublePulse;

	ULONG lAddr = _tcstoul (info.m_Config.m_HeadSettings.m_strUID, NULL, 16);
	ULONG lLinkedTo = info.m_Config.m_HeadSettings.m_lLinkedTo;

	if (lLinkedTo) {
		HEADSTRUCT link;

		TRACEF (info.GetFriendlyName () + _T (": linked to: ") + FoxjetDatabase::ToString ((int)lLinkedTo));

		if (GetHeadRecord (theApp.m_Database, lLinkedTo, link)) {
			ULONG lLinkAddr = _tcstoul (link.m_strUID, NULL, 16);

			bool bHwBuffer = 
				(lAddr == 1 && lLinkAddr == 2) ||
				(lAddr == 2 && lLinkAddr == 1) ||
				(lAddr == 3 && lLinkAddr == 4) ||
				(lAddr == 4 && lLinkAddr == 3);

			if (bHwBuffer)
				tConfig._CtrlReg._PrintBuffer = 1;
		}
	}


	//TRACEF (pHead->GetFriendlyName () + _T (": _PrintMode:   ") + FoxjetDatabase::ToString ((int)tConfig._CtrlReg._PrintMode));
	//TRACEF (pHead->GetFriendlyName () + _T (": _PrintBuffer: ") + FoxjetDatabase::ToString ((int)tConfig._CtrlReg._PrintBuffer));

	tConfig._IFStatusReg._EnPhotoEvent		= info.m_bDisabled ? 0 : 1;
	tConfig._IFStatusReg._EnEndPrintEvent	= info.m_bDisabled ? 0 : 1;
	tConfig._IFStatusReg._EnStartPrintEvent = 0; //pHead->m_bDisabled ? 0 : 1;
	tConfig._IFStatusReg._EnHVEvent			= 0; //pHead->m_bDisabled ? 0 : 1;
	tConfig._IFStatusReg._EnInkLowEvent		= 0; //pHead->m_bDisabled ? 0 : 1;
	tConfig._IFStatusReg._EnAtTempEvent		= 0; //pHead->m_bDisabled ? 0 : 1;
	tConfig._IFStatusReg._EnHeatOnEvent		= 0; //pHead->m_bDisabled ? 0 : 1;

	//tConfig._MiscCtrlReg._Rsvd = pHead->GetStandbyMode () ? 0 : 1;

	switch (info.m_Config.m_HeadSettings.m_nEncoderDivisor) {
	default:
	case 2:	tConfig._MiscCtrlReg._TachRes = 0x00; /* 150 dpi */	break;
	case 3: tConfig._MiscCtrlReg._TachRes = 0x01; /* 200 dpi */	break; 
	case 1: tConfig._MiscCtrlReg._TachRes = 0x02; /* 300 dpi */	break; 
	case 4: tConfig._MiscCtrlReg._TachRes = 0x03; /* 120 dpi */	break; 
	}

	if (!m_HeadSettings.m_bEnabled)
		tConfig._CtrlReg._HeadType = 0;

	switch ( m_HeadSettings.m_nHeadType )
	{
		case FoxjetDatabase::UJ2_192_32:
		case FoxjetDatabase::UJ2_192_32NP:
		case FoxjetDatabase::UJ2_96_32:
		case FoxjetDatabase::UJI_96_32: 
		case FoxjetDatabase::UJI_192_32: 
		case FoxjetDatabase::UJI_256_32: 
		case FoxjetDatabase::ALPHA_CODER:	
			tConfig._CtrlReg._HeadType			= 1;		
			break;
		case FoxjetDatabase::UJ2_352_32:	
			tConfig._CtrlReg._HeadType			= 2;		
			break;
		case FoxjetDatabase::GRAPHICS_384_128:
			tConfig._CtrlReg._HeadType			= 3; 
			tConfig._CtrlReg._GraphicHeadType	= 1;
			break;
		case FoxjetDatabase::GRAPHICS_768_256:
		case FoxjetDatabase::GRAPHICS_768_256_L310:	
		case UR2:
			tConfig._CtrlReg._HeadType			= 3; 
			tConfig._CtrlReg._GraphicHeadType	= 0;
			break;
		case UR4:
			tConfig._CtrlReg._HeadType			= 3; 
			tConfig._CtrlReg._GraphicHeadType	= 0;
			tConfig._CtrlReg._Rsvd				= 8;
			break;
	}

	tConfig._CtrlReg._Inverted = m_HeadSettings.m_bInverted;
	//TRACEF (_T ("_Inverted: ") + FoxjetDatabase::ToString ((int)tConfig._CtrlReg._Inverted));

	{
		CString str;

		str.Format (_T ("0x%04X: tConfig._CtrlReg: 0x%04X")
			_T ("\n\t_HeadType:        %d")
			_T ("\n\t_GraphicHeadType: %d")
			_T ("\n\t_DoublePulse:     %d")
			_T ("\n\t_PrintMode:       %d")
			_T ("\n\t_PrintBuffer:     %d")
			_T ("\n\t_Inverted:        %d")
			_T ("\n\t_PrintEnable:     %d")
			_T ("\n\t_Rsvd:            %d")
			_T ("\n\t_ErrDrv:          %d"),
			_tcstoul (info.m_Config.m_HeadSettings.m_strUID, NULL, 16),
			* (WORD *)&tConfig._CtrlReg,
			tConfig._CtrlReg._HeadType,
			tConfig._CtrlReg._GraphicHeadType,
			tConfig._CtrlReg._DoublePulse,
			tConfig._CtrlReg._PrintMode,
			tConfig._CtrlReg._PrintBuffer,
			tConfig._CtrlReg._Inverted,
			tConfig._CtrlReg._PrintEnable,
			tConfig._CtrlReg._Rsvd,
			tConfig._CtrlReg._ErrDrv);
		TRACER (str);
	}

	tConfig._IFCtrlReg._OTBTach		= 1;
	tConfig._IFCtrlReg._OTBEdge		= 1;
	tConfig._IFCtrlReg._TachSrc		= m_HeadSettings.m_nEncoder == FoxjetDatabase::EXTERNAL_ENC ? 0 : 1;
	tConfig._IFCtrlReg._PhotoSrc	= m_HeadSettings.m_nPhotocell == FoxjetDatabase::EXTERNAL_PC ? 0 : 1;

	if (m_HeadSettings.m_nShareEnc >= 4) {
		tConfig._IFCtrlReg._TachSrc = m_HeadSettings.m_nShareEnc;

		if (lAddr >= 0x05)
			tConfig._IFCtrlReg._TachSrc = m_HeadSettings.m_nShareEnc - 4; 
	}

	if (m_HeadSettings.m_nSharePhoto >= 4) {
		tConfig._IFCtrlReg._PhotoSrc = m_HeadSettings.m_nSharePhoto;

		if (lAddr >= 0x05)
			tConfig._IFCtrlReg._PhotoSrc = m_HeadSettings.m_nSharePhoto - 4; 
	}

	double dPrintRes = m_HeadSettings.m_nHorzRes;

	switch (m_HeadSettings.m_nEncoderDivisor) {
	default:
	case 1:	dPrintRes = (double)m_HeadSettings.m_nHorzRes * 1.0; break;
	case 3:	dPrintRes = (double)m_HeadSettings.m_nHorzRes * 1.5; break;
	case 2:	dPrintRes = (double)m_HeadSettings.m_nHorzRes * 2.0; break;
	}

	//TRACEF (pHead->GetFriendlyName () + _T (": m_nIntTachSpeed: ") + FoxjetDatabase::ToString ((double)m_HeadSettings.m_nIntTachSpeed));

	double dTemp = ((double)m_HeadSettings.m_nIntTachSpeed / 5.0);
	dTemp *= 300.0;
	//dTemp *= m_HeadSettings.GetRes ();
	dTemp = (dTemp / (double)1843200) * (double)65536;

	tConfig._IntTachReg._TachGenFreg = (unsigned short)dTemp;
	tConfig._IntPhotoReg._PhotoGenFreg = (USHORT ) ( ( m_HeadSettings.m_dPhotocellRate * (dPrintRes) / 2.0));

	//{ // TODO: rem
	//	CString str; 
	//	
	//	str.Format (_T ("%s: _PhotoGenFreg: %d [%f], _TachGenFreg; %d, print res: %f"), m_HeadSettings.m_strName,
	//		tConfig._IntPhotoReg._PhotoGenFreg,
	//		m_HeadSettings.m_dPhotocellRate,
	//		tConfig._IntTachReg._TachGenFreg,
	//		m_HeadSettings.GetRes ()); 
	//	TRACER (str); 
	//	::PostMessage (info.m_hControlWnd, WM_DIAGNOSTIC_DATA, (WPARAM)new CString (str), 0);
	//}

	switch (m_HeadSettings.m_nEncoderDivisor) {
	default:
	case 1:																break;  // 300 dpi
	case 3:	tConfig._IntPhotoReg._PhotoGenFreg /= (3.0 * 1.1235);		break;	// 200 dpi
	case 2:	tConfig._IntPhotoReg._PhotoGenFreg /= 2.0;					break;	// 150 dpi
	}

	//{ 
	//	CString str; 
	//	
	//	str.Format (_T ("%s: _PhotoGenFreg: %d [%f], _TachGenFreg; %d, print res: %f"), m_HeadSettings.m_strName,
	//		tConfig._IntPhotoReg._PhotoGenFreg,
	//		m_HeadSettings.m_dPhotocellRate,
	//		tConfig._IntTachReg._TachGenFreg,
	//		m_HeadSettings.GetRes ()); 
	//	TRACER (str); 
	//	::PostMessage (info.m_hControlWnd, WM_DIAGNOSTIC_DATA, (WPARAM)new CString (str), 0);
	//}

	#ifdef _DEBUG
	//{ CString str; str.Format (_T ("[%d dpi]: _TachGenFreg: %f [%lu], m_nEncoderDivisor: %d,   _PhotoGenFreg: %d"), (int)GetPrintRes (), dTemp, tConfig._IntTachReg._TachGenFreg, m_HeadSettings.m_nEncoderDivisor, tConfig._IntPhotoReg._PhotoGenFreg); TRACEF (str); }
	#endif //_DEBUG

	//#ifdef _DEBUG
	{ 
		CString str; 
		WORD wCtrlReg = 0;
		
		memcpy (&wCtrlReg, &tConfig._CtrlReg, sizeof (wCtrlReg));
		str.Format (_T ("0x%p: [%s] _TachSrc: %d, _PhotoSrc: %d, _CtrlReg: 0x%04X [%s]"), 
			lAddr, 
			info.GetFriendlyName (), 
			tConfig._IFCtrlReg._TachSrc, 
			tConfig._IFCtrlReg._PhotoSrc,
			wCtrlReg, ToBinString (wCtrlReg));
		TRACER (str); 
	}
	//#endif
}

void CHeadCfg::ToRegisters (tagSetupRegs &tConfig, tagFireVibPulse &tPulseRegs, const Head::CHeadInfo & info)
{
	// These 2 work in conjuction with one another.  Setting _SingleBuffer=0 causes the 
	// the printer to print from one buffer while we fill the other buffer with image.
	// This also means that you must switch the _PrintBuffer flag depending on whether
	// you are writing or reading.  The print head card will use the one selected at the
	// time of the photocell trigger - photocell delay.
	tConfig._CtrlReg._SingleBuffer		= 1;
	tConfig._CtrlReg._PrintBuffer		= 0;
	tConfig._DividerCtrlReg._SClkRate	= 1;
	tConfig._CtrlReg._FireAMS			= 0x00;
	
	tConfig._CtrlReg._DoublePulse		= m_HeadSettings.m_bDoublePulse;

	tConfig._DividerCtrlReg._Reserved	= 0; //= pHead->GetStandbyMode () ? 0 : 1;

	switch (info.m_Config.m_HeadSettings.m_nEncoderDivisor) {
	default:
	case 2:
		tConfig._DividerCtrlReg._TachDiv = 0x00; // 150 dpi
		break;
	case 3:
		tConfig._DividerCtrlReg._TachDiv = 0x01; // 200 dpi 
		break;
	case 1:
		tConfig._DividerCtrlReg._TachDiv = 0x02; // 300 dpi
		break;
	}

	if (!m_HeadSettings.m_bEnabled)
		tConfig._CtrlReg._HeadType = 0;

	// Note: 29.4912 MHz is the clock rate with which pulse widths are base off of.
	// Thus every 29.4912 clock cycles is 1 Micro-Second.
	switch ( m_HeadSettings.m_nHeadType )
	{
		case FoxjetDatabase::ALPHA_CODER: // 17/5/6
			tPulseRegs._DampPulseDelay = (WORD) (6 * 29.4912);
			tPulseRegs._DampPulseWidth = (WORD) (5 * 29.4912);
			tPulseRegs._FirePulseDelay = (WORD) (0 * 29.4912);
			tPulseRegs._FirePulseWidth	= (WORD) (17 * 29.4912);
			tConfig._CtrlReg._HeadType = 1; //2;
			break;
		case FoxjetDatabase::UJ2_192_32:
		case FoxjetDatabase::UJ2_192_32NP:
	        tPulseRegs._DampPulseDelay = (WORD) (5 * 29.4912);
			tPulseRegs._DampPulseWidth = (WORD) (6 * 29.4912);
			tPulseRegs._FirePulseDelay = (WORD) (1 * 29.4912);
			tPulseRegs._FirePulseWidth	= (WORD) (13.5 * 29.4912);
			tConfig._CtrlReg._HeadType = 1;
			break;
		case FoxjetDatabase::UJ2_96_32:
			tPulseRegs._DampPulseDelay = (WORD) (0 * 29.4912);
			tPulseRegs._DampPulseWidth = (WORD) (0 * 29.4912);
			tPulseRegs._FirePulseDelay = (WORD) (0 * 29.4912);
			tPulseRegs._FirePulseWidth	= (WORD) (17 * 29.4912);
			tConfig._CtrlReg._HeadType = 1;
			break;
		case FoxjetDatabase::UJ2_352_32:
			tPulseRegs._DampPulseDelay = (WORD) (5 * 29.4912);
			tPulseRegs._DampPulseWidth = (WORD) (6 * 29.4912);
			tPulseRegs._FirePulseDelay = (WORD) (1 * 29.4912);
			tPulseRegs._FirePulseWidth	= (WORD) (13.5 * 29.4912);
			tConfig._CtrlReg._HeadType = 2;
			break;
		case FoxjetDatabase::GRAPHICS_384_128:
		case FoxjetDatabase::GRAPHICS_768_256:
		case FoxjetDatabase::GRAPHICS_768_256_L310:
		case FoxjetDatabase::UR2:
		case FoxjetDatabase::UR4:
			tPulseRegs._DampPulseDelay = (WORD) (0 * 29.4912);
			tPulseRegs._DampPulseWidth = (WORD) (0 * 29.4912);
			tPulseRegs._FirePulseDelay = (WORD) (0 * 29.4912);
			tPulseRegs._FirePulseWidth	= (WORD) (5 * 29.4912);
			tConfig._CtrlReg._HeadType = 3;
			break;
		case FoxjetDatabase::UJI_96_32: // normal 17 microsecond pulse
			tPulseRegs._DampPulseDelay = (WORD) (0 * 29.4912);
			tPulseRegs._DampPulseWidth = (WORD) (0 * 29.4912);
			tPulseRegs._FirePulseDelay = (WORD) (0 * 29.4912);
			tPulseRegs._FirePulseWidth	= (WORD) (17 * 29.4912);
			tConfig._CtrlReg._HeadType = 1;
			break;
		case FoxjetDatabase::UJI_192_32: // 17/4/4 
	        tPulseRegs._DampPulseDelay = (WORD) (4 * 29.4912);
			tPulseRegs._DampPulseWidth = (WORD) (4 * 29.4912);
			tPulseRegs._FirePulseDelay = (WORD) (0 * 29.4912);
			tPulseRegs._FirePulseWidth	= (WORD) (17 * 29.4912);
			tConfig._CtrlReg._HeadType = 1;
			break;
		case FoxjetDatabase::UJI_256_32: // 13.5/5/7 
	        tPulseRegs._DampPulseDelay = (WORD) (7 * 29.4912);
			tPulseRegs._DampPulseWidth = (WORD) (5 * 29.4912);
			tPulseRegs._FirePulseDelay = (WORD) (0 * 29.4912);
			tPulseRegs._FirePulseWidth	= (WORD) (13.5 * 29.4912);
			tConfig._CtrlReg._HeadType = 1;
			break;
	}
	tConfig._CtrlReg._Inverted = m_HeadSettings.m_bInverted;

	switch ( m_HeadSettings.m_nEncoder )
	{
		case FoxjetDatabase::EXTERNAL_ENC:	// External
			switch ( m_HeadSettings.m_nShareEnc )
			{
				case 0:	// Not Shared
					tConfig._InterfaceCtrlReg._TachSrc = 0;
					tConfig._InterfaceCtrlReg._OTBTach1 = 0;
					tConfig._InterfaceCtrlReg._OTBTach2 = 0;
					break;
				case 1:	// Shared as A or 1
					tConfig._InterfaceCtrlReg._TachSrc = 0;
					tConfig._InterfaceCtrlReg._OTBTach1 = 1;
					tConfig._InterfaceCtrlReg._OTBTach2 = 0;
					break;
				case 2:	// Shared as B or 2
					tConfig._InterfaceCtrlReg._TachSrc = 0;
					tConfig._InterfaceCtrlReg._OTBTach1 = 0;
					tConfig._InterfaceCtrlReg._OTBTach2 = 1;
					break;
			}
			break;
		case FoxjetDatabase::INTERNAL_ENC:	// Internal
			switch ( m_HeadSettings.m_nShareEnc )
			{
				case 0:	// Not Shared
					tConfig._InterfaceCtrlReg._TachSrc = 1;
					tConfig._InterfaceCtrlReg._OTBTach1 = 0;
					tConfig._InterfaceCtrlReg._OTBTach2 = 0;
					break;
				case 1:	// Shared as A or 1
					tConfig._InterfaceCtrlReg._TachSrc = 1;
					tConfig._InterfaceCtrlReg._OTBTach1 = 1;
					tConfig._InterfaceCtrlReg._OTBTach2 = 0;
					break;
				case 2:	// Shared as B or 2
					tConfig._InterfaceCtrlReg._TachSrc = 1;
					tConfig._InterfaceCtrlReg._OTBTach1 = 0;
					tConfig._InterfaceCtrlReg._OTBTach2 = 1;
					break;
			}
			break;
		case FoxjetDatabase::SHARED_ENC:	// Shared Encoder
			switch ( m_HeadSettings.m_nShareEnc )
			{
				case 0:	// Not Shared  This is a invalid state.  We will force to OTB1
					tConfig._InterfaceCtrlReg._TachSrc = 2;
					tConfig._InterfaceCtrlReg._OTBTach1 = 0;
					tConfig._InterfaceCtrlReg._OTBTach2 = 0;
					break;
				case 1:	// Shared as A or 1
					tConfig._InterfaceCtrlReg._TachSrc = 2;
					tConfig._InterfaceCtrlReg._OTBTach1 = 0;
					tConfig._InterfaceCtrlReg._OTBTach2 = 0;
					break;
				case 2:	// Shared as B or 2
					tConfig._InterfaceCtrlReg._TachSrc = 3;
					tConfig._InterfaceCtrlReg._OTBTach1 = 0;
					tConfig._InterfaceCtrlReg._OTBTach2 = 0;
					break;
			}
			break;
	}

	switch ( m_HeadSettings.m_nPhotocell )
	{
		case FoxjetDatabase::EXTERNAL_PC:	// External
			switch ( m_HeadSettings.m_nSharePhoto )
			{
				case 0:	// Not Shared
					tConfig._InterfaceCtrlReg._PhotoSrc = 0;
					tConfig._InterfaceCtrlReg._OTBPhoto1 = 0;
					tConfig._InterfaceCtrlReg._OTBPhoto2 = 0;
					break;
				case 1:	// Shared as A or 1
					tConfig._InterfaceCtrlReg._PhotoSrc = 0;
					tConfig._InterfaceCtrlReg._OTBPhoto1 = 1;
					tConfig._InterfaceCtrlReg._OTBPhoto2 = 0;
					break;
				case 2:	// Shared as B or 2
					tConfig._InterfaceCtrlReg._PhotoSrc = 0;
					tConfig._InterfaceCtrlReg._OTBPhoto1 = 0;
					tConfig._InterfaceCtrlReg._OTBPhoto2 = 1;
					break;
			}
			break;
		case FoxjetDatabase::INTERNAL_PC:	// Internal
			switch ( m_HeadSettings.m_nSharePhoto )
			{
				case 0:	// Not Shared
					tConfig._InterfaceCtrlReg._PhotoSrc		= 1;
					tConfig._InterfaceCtrlReg._OTBPhoto1	= 0;
					tConfig._InterfaceCtrlReg._OTBPhoto2	= 0;
					break;
				case 1:	// Shared as A or 1
					tConfig._InterfaceCtrlReg._PhotoSrc		= 1;
					tConfig._InterfaceCtrlReg._OTBPhoto1	= 1;
					tConfig._InterfaceCtrlReg._OTBPhoto2	= 0;
					break;
				case 2:	// Shared as B or 2
					tConfig._InterfaceCtrlReg._PhotoSrc		= 1;
					tConfig._InterfaceCtrlReg._OTBPhoto1	= 0;
					tConfig._InterfaceCtrlReg._OTBPhoto2	= 1;
					break;
			}
			break;
		case FoxjetDatabase::SHARED_PC:	// Using a shared photocell
			switch ( m_HeadSettings.m_nSharePhoto )
			{
				case 0:	// Not Shared This is an invalid state  We will force to OTB1
					tConfig._InterfaceCtrlReg._PhotoSrc = 2;
					tConfig._InterfaceCtrlReg._OTBPhoto1 = 0;
					tConfig._InterfaceCtrlReg._OTBPhoto2 = 0;
					break;
				case 1:	// Shared as A or 1
					tConfig._InterfaceCtrlReg._PhotoSrc = 2;
					tConfig._InterfaceCtrlReg._OTBPhoto1 = 0;
					tConfig._InterfaceCtrlReg._OTBPhoto2 = 0;
					break;
				case 2:	// Shared as B or 2
					tConfig._InterfaceCtrlReg._PhotoSrc = 3;
					tConfig._InterfaceCtrlReg._OTBPhoto1 = 0;
					tConfig._InterfaceCtrlReg._OTBPhoto2 = 0;
					break;
			}
			break;
	}

	double dTemp =  ( (double) m_HeadSettings.m_nIntTachSpeed / 5.0 );
	dTemp *= m_HeadSettings.m_nHorzRes;
	dTemp = (dTemp / (double)1843200) * (double)65536;

	if (m_HeadSettings.IsUSB ())
		dTemp *= 2;

	tConfig._IntTachGenReg._TachGenFreg = (unsigned short ) dTemp;

/*
#ifdef _DEBUG
	{
		float fEncoderWheel = (float)m_HeadSettings.m_nHorzRes;
		int nEncoderDivisor = m_HeadSettings.m_nEncoderDivisor;
		float fPrintRes = (float)((fEncoderWheel * 2.0) / (float)nEncoderDivisor);
		long lAutoPrint = (long)(fPrintRes * (float)m_HeadSettings.m_dPhotocellRate);
		int nBreak = 0;
	}
#endif
*/

	// Note:
	// Internal Photocell is ( Inches desired * (Encoder Resolution / 2))  / 2 
	// Encoder is specified in real resolution.  This is always divided by .
	// The final divide by 2 is because the hardware duty-cycles this by 2, thus
	// its minimum period is 60us.
	tConfig._IntPhotoGenReg._PhotoGenFreg = (USHORT ) ( ( m_HeadSettings.m_dPhotocellRate * (m_HeadSettings.m_nHorzRes / 2.0) ) /  2.0 );
	//tConfig._IntPhotoGenReg._PhotoGenFreg = (USHORT ) ( ( m_HeadSettings.m_dPhotocellRate * (m_HeadSettings.m_nHorzRes / m_HeadSettings.m_nEncoderDivisor) ) /  2.0 );

	{ 
		CString str; 
		
		str.Format (_T ("%s: _PhotoGenFreg: %d [%f], print res: %f"), m_HeadSettings.m_strName,
			tConfig._IntPhotoGenReg._PhotoGenFreg, m_HeadSettings.m_dPhotocellRate,
			m_HeadSettings.GetRes ()); 
		TRACER (str); 

		str.Format (_T ("%s: m_lPhotocellDelay: %d [%f in]"), m_HeadSettings.m_strName,
			m_HeadSettings.m_lPhotocellDelay, m_HeadSettings.m_lPhotocellDelay / 1000.0); 
		//TRACEF (str); 
	}
}
#endif //__WINPRINTER__

int CHeadCfg::GetNFactor()
{
	return CalculateNFactor ( m_HeadSettings.m_dHeadAngle, (m_HeadSettings.m_nHorzRes / m_HeadSettings.m_nEncoderDivisor) );
//	return FoxjetCommon::CalculateNFactor (m_HeadSettings.m_dHeadAngle, m_HeadSettings.GetRes ());
}

ULONG CHeadCfg::GetLineID()
{
	return m_Line.m_lID;
}

#ifdef __WINPRINTER__
void CHeadCfg::GetAMSValues(tagAMSRegs &AmsRegs)
{
	switch ( m_HeadSettings.m_nHeadType )
	{
		case FoxjetDatabase::UJ2_192_32:
		case FoxjetDatabase::UJ2_192_32NP:
		case FoxjetDatabase::UJ2_96_32:
		case FoxjetDatabase::UJ2_352_32:
		case FoxjetDatabase::ALPHA_CODER:
		case FoxjetDatabase::UJI_96_32:
		case FoxjetDatabase::UJI_192_32:
		case FoxjetDatabase::UJI_256_32:
			AmsRegs.A1 = m_Line.m_nAMS32_A1;
			AmsRegs.A2 = m_Line.m_nAMS32_A2;
			AmsRegs.A3 = m_Line.m_nAMS32_A3;
			AmsRegs.A4 = m_Line.m_nAMS32_A4;
			AmsRegs.A5 = m_Line.m_nAMS32_A5;
			AmsRegs.A6 = m_Line.m_nAMS32_A6;
			AmsRegs.A7 = m_Line.m_nAMS32_A7;
			break;
		case FoxjetDatabase::GRAPHICS_384_128:
		case FoxjetDatabase::GRAPHICS_768_256:
		case FoxjetDatabase::GRAPHICS_768_256_L310:
		case FoxjetDatabase::UR2:
		case FoxjetDatabase::UR4:
			AmsRegs.A1 = m_Line.m_nAMS256_A1;
			AmsRegs.A2 = m_Line.m_nAMS256_A2;
			AmsRegs.A3 = m_Line.m_nAMS256_A3;
			AmsRegs.A4 = m_Line.m_nAMS256_A4;
			AmsRegs.A5 = m_Line.m_nAMS256_A5;
			AmsRegs.A6 = m_Line.m_nAMS256_A6;
			AmsRegs.A7 = m_Line.m_nAMS256_A7;
			break;
		default:
			TRACEF ("TODO: rem");
			break;
	}
//	m_nInterval			= m_pLS->m_nAMS_Interval;
}
#endif //__WINPRINTER__

bool CHeadCfg::IsDisabled()
{
	return !m_HeadSettings.m_bEnabled ? true : false;
}

double CHeadCfg::GetPrintRes () const
{
	double dResult = 0;

	switch (m_HeadSettings.m_nEncoderDivisor) {
		case 1:	dResult = (double)m_HeadSettings.m_nHorzRes;		break; // 300
		default:
		case 3:	dResult = (double)m_HeadSettings.m_nHorzRes / 1.5;	break; // 200
		case 2:	dResult = (double)m_HeadSettings.m_nHorzRes / 2.0;	break; // 150
	}

	return dResult;
}