#if !defined(AFX_DBSTARTTASKDLG_H__F419A6BE_F16A_46EC_913A_F53C2355B6F6__INCLUDED_)
#define AFX_DBSTARTTASKDLG_H__F419A6BE_F16A_46EC_913A_F53C2355B6F6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DbStartTaskDlg.h : header file
//

#include "RoundButtons.h"
#include "Utils.h"
#include <string>
#include "AutoComplete.h"

/////////////////////////////////////////////////////////////////////////////
// CDbStartTaskDlg dialog

class CControlView;

class CDbStartTaskDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CDbStartTaskDlg(const CString & strLine, CControlView * pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDbStartTaskDlg)
	CString	m_strDSN;
	CString	m_strKeyField;
	CString	m_strTable;
	CString	m_strTaskField;
	CString	m_strKeyValue;
	//}}AFX_DATA

	CString m_strStart;			// sw0867
	CString m_strEnd;			// sw0867
	CString m_strOffsetField;	// sw0867
	int m_nOffset;				// sw0867
	BOOL m_bResetCounts;
	CString m_strLine;

	int m_nSQLType;
	CString m_strTaskValue;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDbStartTaskDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	class CEditEx : public FoxjetUtils::CAutoEdit
	{
	public:
		CEditEx (CDbStartTaskDlg * pParent) : m_pParent (pParent), FoxjetUtils::CAutoEdit (pParent) { }

		virtual std::vector <std::wstring> OnTyping (const CString & str);

		CDbStartTaskDlg * m_pParent;
	};

	CEditEx m_txtKeyValue;

	FoxjetUtils::CRoundButtons m_buttons;

	virtual void OnOK();

	CControlView * m_pControlView;

	// Generated message map functions
	//{{AFX_MSG(CDbStartTaskDlg)
	afx_msg void OnSelect();
	afx_msg void OnOther();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	afx_msg void OnKeyValueChanged ();
	afx_msg LRESULT OnKeyValueTypingChange (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnKeyValueShortcut (WPARAM wParam, LPARAM lParam);	

	std::vector <std::wstring> m_vKeyValues;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DBSTARTTASKDLG_H__F419A6BE_F16A_46EC_913A_F53C2355B6F6__INCLUDED_)
