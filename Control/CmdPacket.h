// CmdPacket.h: interface for the CCmdPacket class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CMDPACKET_H__F8CD5750_2536_4F90_88AB_0BD0D875FB6B__INCLUDED_)
#define AFX_CMDPACKET_H__F8CD5750_2536_4F90_88AB_0BD0D875FB6B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "TypeDefs.h"

class CCmdPacket  
{
public:
	public:
		// Methods
		CCmdPacket ();
		~CCmdPacket ();
		 void operator << ( const _SOCKET_MESSAGE *pMsg );
		// Attributes
		CTime TimeStamp;
		HANDLE m_hEvent;
		_SOCKET_MESSAGE m_pktInfo;
		ULONG m_lControlNum;
};

#endif // !defined(AFX_CMDPACKET_H__F8CD5750_2536_4F90_88AB_0BD0D875FB6B__INCLUDED_)
