#if !defined(AFX_BAXTERFORMATDLG_H__28095A58_073E_4B50_B1C1_B9BEFE9035A2__INCLUDED_)
#define AFX_BAXTERFORMATDLG_H__28095A58_073E_4B50_B1C1_B9BEFE9035A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BaxterFormatDlg.h : header file
//
#include "Utils.h"


/////////////////////////////////////////////////////////////////////////////
// CBaxterFormatDlg dialog

class CBaxterFormatDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CBaxterFormatDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBaxterFormatDlg)
	enum { IDD = IDD_BAXTER_FORMAT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	CString m_str;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBaxterFormatDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();

	// Generated message map functions
	//{{AFX_MSG(CBaxterFormatDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkFormat();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BAXTERFORMATDLG_H__28095A58_073E_4B50_B1C1_B9BEFE9035A2__INCLUDED_)
