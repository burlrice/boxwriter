// NamedCountDlg.cpp : implementation file
//

#include "stdafx.h"
#include "control.h"
#include "NamedCountDlg.h"
#include "Debug.h"
#include "Parse.h"
#include "Registry.h"
#include "Color.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ItiLibrary;
using namespace FoxjetCommon;
using namespace FoxjetElements;


/////////////////////////////////////////////////////////////////////////////

using namespace NamedCountDlg;

CCountItem::CCountItem (FoxjetCommon::CBaseElement * pElement, __int64 lCount)
:	m_pCount (NULL)
{
	ASSERT (pElement);
	ASSERT (pElement->IsKindOf (RUNTIME_CLASS (CCountElement)));
	m_pCount = (CCountElement *)CopyElement (pElement, NULL, false);

	if (m_pCount->IsIrregularPalletSize ()) 
		SetCount (lCount, m_pCount->GetPalCount (), m_pCount->GetPalMin (), m_pCount->GetPalMax (), m_pCount->GetMax ());
	else {
		if (m_pCount->IsPalletCount ())
			SetCount (lCount, m_pCount->GetPalMax ());
		else
			SetCount (lCount);
	}
}

CCountItem::~CCountItem ()
{
	if (m_pCount) {
		delete m_pCount;
		m_pCount = NULL;
	}
}

CString CCountItem::GetDispText (int nColumn) const
{
	CString str;

	ASSERT (m_pCount);

	switch (nColumn) {
	case 0:		
		str = m_pCount->GetName (); 
		break;
	case 1:	
		str = m_pCount->GetImageData ();
		break;
	}

	return str;
}

void CCountItem::SetCount (__int64 lCount, __int64 lPalletMax)
{
	ASSERT (m_pCount);
	ASSERT (!m_pCount->IsIrregularPalletSize ());
	//TRACEF (FormatI64 (lCount) + _T (": ") + FormatI64 (lPalletMax));

	if (m_pCount->IsPalletCount ())
		m_pCount->SetPalMax (lPalletMax);

	m_pCount->Build (FoxjetCommon::INITIAL);

	if (lCount >= m_pCount->GetMin ()) 
		m_pCount->IncrementTo (lCount);

	m_pCount->Build (FoxjetCommon::IMAGING);
	//TRACEF (m_pCount->GetImageData ());
}

void CCountItem::SetCount (__int64 lBoxCount, __int64 lPalletCount, __int64 lPalletMin, __int64 lPalletMax, __int64 lUnitsPerPallet)
{
	ASSERT (m_pCount);
	ASSERT (m_pCount->IsIrregularPalletSize ());

	//TRACEF (FormatI64 (lCount) + _T (": ") + FormatI64 (lPalletMax));

	m_pCount->IncrementTo (lBoxCount);

	if (m_pCount->IsPalletCount ()) {
		if ((lPalletCount > lPalletMax) && lPalletMax)
			lPalletCount = m_pCount->GetRollover () ? (lPalletMin - 1) + (lPalletCount % lPalletMax) : lPalletMax;

		m_pCount->SetPalCount (lPalletCount);
		m_pCount->SetPalMin (lPalletMin);
		m_pCount->SetPalMax (lPalletMax);
		m_pCount->SetMax (max (1, lUnitsPerPallet));
	}

	//m_pCount->Build (FoxjetCommon::INITIAL);
	m_pCount->Build (FoxjetCommon::IMAGING);
	//TRACEF (m_pCount->GetImageData ());
}

/////////////////////////////////////////////////////////////////////////////
// CNamedCountDlg dialog


CNamedCountDlg::CNamedCountDlg(CWnd* pParent /*=NULL*/)
:	m_lCount (0),
	m_lBoxCount (0),
	m_lBoxMin (0),
	m_lPalletCount (0),
	m_lPalletMin (0),
	m_lPalletMax (0),
	m_lBoxesPerPallet (0),
	m_lUnitsPerPallet (0),
	m_bPallet (false),
	m_bIrregularPalletSize (false),
	FoxjetCommon::CEliteDlg(IDD_NAMEDCOUNT_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CNamedCountDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CNamedCountDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNamedCountDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	//if (IsMatrix ()) 
	{
		m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
		m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
		m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);
	}

	FoxjetCommon::DDX_Text (pDX, TXT_COUNT, m_lCount);
	FoxjetCommon::DDX_Text (pDX, TXT_PALCOUNT, m_lPalletCount);
	FoxjetCommon::DDX_Text (pDX, TXT_UNITSPERPALLET, m_lUnitsPerPallet);
	FoxjetCommon::DDX_Text (pDX, TXT_MINPALCOUNT, m_lPalletMin);
	FoxjetCommon::DDX_Text (pDX, TXT_MAXPALCOUNT, m_lPalletMax);
}


BEGIN_MESSAGE_MAP(CNamedCountDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CNamedCountDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_NOTIFY(LVN_ITEMCHANGED, LV_COUNT, OnItemchangedElements)
	ON_EN_CHANGE (TXT_COUNT,			OnChangeCount)
	ON_EN_CHANGE (TXT_PALCOUNT,			OnChangeCount)
	ON_EN_CHANGE (TXT_MINPALCOUNT,		OnChangeCount)
	ON_EN_CHANGE (TXT_MAXPALCOUNT,		OnChangeCount)
	ON_EN_CHANGE (TXT_UNITSPERPALLET,	OnChangeCount)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNamedCountDlg message handlers

int CNamedCountDlg::Find (const CString & strName)
{
	for (int i = 0; i < m_lv->GetItemCount (); i++) 
		if (CCountItem * p = (CCountItem *)m_lv.GetCtrlData (i)) 
			if (!p->GetDispText (0).CompareNoCase (strName))
				return i;

	return -1;
}

BOOL CNamedCountDlg::OnInitDialog() 
{	
	using namespace ListCtrlImp;

	CArray <CColumn, CColumn> vCols;

	vCols.Add (CColumn (LoadString (IDS_NAME), 400));
	vCols.Add (CColumn (LoadString (IDS_COUNT), 400));
	
	FoxjetCommon::CEliteDlg::OnInitDialog();
	m_lv.Create (LV_COUNT, vCols, this, _T ("Software\\FoxJet\\Control"));

	for (int i = 0; i < m_vCounts.GetSize (); i++) {
		if (CCountElement * p = DYNAMIC_DOWNCAST (CCountElement, m_vCounts [i])) {
			if (Find (p->GetName ()) == -1) {
				CCountItem * pItem = new CCountItem (p, m_lCount);
				
				m_lv.InsertCtrlData (pItem);

				if (p->IsMaster ()) {
					int nIndex = m_lv.GetDataIndex (pItem);

					m_lv->SetItemState (nIndex, LVIS_FOCUSED | LVIS_SELECTED, 0xFFFF);
					m_lv->SetExtendedStyle (LVS_EX_CHECKBOXES | m_lv->GetExtendedStyle ());
					m_lv->SetCheck (nIndex, TRUE);
					m_lv->EnableWindow (FALSE);

					if (p->IsPalletCount ()) {
						m_lPalletMax = p->GetPalMax ();
						UpdateData (FALSE);
					}
				}
			}
		}
	}

	if (GetSelectedItems (m_lv).GetSize () == 0) {
		if (CCountItem * p = (CCountItem *)m_lv.GetCtrlData (0)) { 
			if (p->GetElement ()->IsPalletCount ()) {
				m_lPalletMax = p->GetElement ()->GetPalMax ();
				UpdateData (FALSE);
			}
		}

		m_lv->SetItemState (0, LVIS_FOCUSED | LVIS_SELECTED, 0xFFFF);
	}

	m_lv->SortItems (ItiLibrary::ListCtrlImp::CListCtrlImp::CompareFunc, 0);
	OnItemchangedElements (NULL, NULL);

	GotoDlgCtrl (GetDlgItem (TXT_COUNT)); 
		
	return FALSE;
}

void CNamedCountDlg::OnItemchangedElements(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMLISTVIEW * pNM = (NMLISTVIEW *)pNMHDR;
	bool bUpdate = pNM ? ((pNM->uNewState & LVIS_SELECTED) ? true : false) : true;
	CLongArray sel = GetSelectedItems (m_lv);

	for (int i = 0; i < m_lv->GetItemCount (); i++) 
		if (CCountItem * p = (CCountItem *)m_lv.GetCtrlData (i)) 
			m_lv->SetCheck (i, p->GetElement ()->IsMaster ());

	if (sel.GetSize ()) {
		int nIndex = sel [0];

		if (CCountItem * p = (CCountItem *)m_lv.GetCtrlData (nIndex)) {
			const CCountElement * pCount = p->GetElement ();

			if (bUpdate) {
				m_bPallet			= pCount->IsPalletCount ();
				m_lBoxMin			= pCount->GetMin ();
				m_lBoxCount			= pCount->GetCount ();
				m_lPalletCount		= 0;
				m_lPalletMin		= 0;
				m_lPalletMax		= 0;
				m_lBoxesPerPallet	= 0;
				m_lUnitsPerPallet	= 0;

				if (m_bPallet) {
					m_lBoxCount			= ((pCount->GetPalCount () - pCount->GetPalMin ()) * pCount->GetMax ()) + pCount->GetCount ();
					m_lPalletCount		= pCount->GetPalMin ();
					m_lPalletMin		= pCount->GetPalMin ();
					m_lPalletMax		= pCount->GetPalMax ();
					m_lBoxesPerPallet	= pCount->GetMax ();
					m_lUnitsPerPallet	= pCount->GetMax ();

					TRACEF (_T ("m_lBoxCount:       ") + FormatI64 (m_lBoxCount));
					TRACEF (_T ("m_lPalletCount:    ") + FormatI64 (m_lPalletCount));
					TRACEF (_T ("m_lUnitsPerPallet: ") + FormatI64 (m_lUnitsPerPallet));
					TRACEF (_T ("m_lPalletMin:      ") + FormatI64 (m_lPalletMin));
					TRACEF (_T ("m_lPalletMax:      ") + FormatI64 (m_lPalletMax));

					{
						__int64 lBox	= m_lCount;
						__int64 lPallet	= pCount->GetPalMin ();
						__int64 lPalletMin, lPalletMax, lUnitsPerPallet;

						if (m_lCount <= 0) {
							lBox = 0;
							lPallet = 1;
						}
						else {
							__int64 lRolledOver = (__int64)(m_lCount / pCount->GetMax ());
							
							lPallet = pCount->GetPalMin () + (lRolledOver * pCount->GetIncrement ());
							lBox = m_lCount - (lRolledOver * pCount->GetMax ());
						}

						if (m_bIrregularPalletSize) {
							lBox				= m_lBoxCount			= m_count.m_lCount;
							lPallet				= m_lPalletCount		= m_count.m_lPalletCount;
							m_lPalletMin		= m_count.m_lPalletMin;
							m_lPalletMax		= m_count.m_lPalletMax;
							m_lBoxesPerPallet	= 
							m_lUnitsPerPallet	= m_count.m_lUnitsPerPallet;

							lPalletMin		= m_lPalletMin;
							lPalletMax		= m_lPalletMax;
							lUnitsPerPallet = m_lUnitsPerPallet;
						}
						else {
							//lPalletMin		= pCount->GetMax ();
							//lPalletMax		= pCount->GetPalMin ();
							//lUnitsPerPallet = pCount->GetPalMax ();
							lPalletMin		= m_lPalletMin;
							lPalletMax		= m_lPalletMax;
							lUnitsPerPallet = m_lUnitsPerPallet;
						}

						TRACEF (_T ("TXT_COUNT:          ") + FormatI64 (lBox));
						TRACEF (_T ("TXT_PALCOUNT:       ") + FormatI64 (lPallet));
						TRACEF (_T ("TXT_UNITSPERPALLET: ") + FormatI64 (lPalletMin));
						TRACEF (_T ("TXT_MINPALCOUNT:    ") + FormatI64 (lPalletMax));
						TRACEF (_T ("TXT_MAXPALCOUNT:    ") + FormatI64 (lUnitsPerPallet));

						SetDlgItemText (TXT_COUNT,			FormatI64 (lBox));
						SetDlgItemText (TXT_PALCOUNT,		FormatI64 (lPallet));
						SetDlgItemText (TXT_UNITSPERPALLET,	FormatI64 (lUnitsPerPallet));
						SetDlgItemText (TXT_MINPALCOUNT,	FormatI64 (lPalletMin));
						SetDlgItemText (TXT_MAXPALCOUNT,	FormatI64 (lPalletMax));

						//SetDlgItemText (TXT_COUNT,			FormatI64 (lBox));
						//SetDlgItemText (TXT_PALCOUNT,		FormatI64 (lPallet));
						//SetDlgItemText (TXT_UNITSPERPALLET,	FormatI64 (lPalletMin));
						//SetDlgItemText (TXT_MINPALCOUNT,	FormatI64 (lPalletMax));
						//SetDlgItemText (TXT_MAXPALCOUNT,	FormatI64 (lUnitsPerPallet));
					}
					/*
					TXT_COUNT [m_lCount]		TXT_PALCOUNT		[m_lPalletCount]
												TXT_UNITSPERPALLET	[m_lUnitsPerPallet]
												TXT_MAXPALCOUNT		[m_lPalletMax]
					*/
				}

				GetDlgItem (TXT_PALCOUNT)->EnableWindow (m_bPallet);
				GetDlgItem (TXT_MINPALCOUNT)->EnableWindow (m_bPallet);
				GetDlgItem (TXT_MAXPALCOUNT)->EnableWindow (m_bPallet);
				GetDlgItem (TXT_UNITSPERPALLET)->EnableWindow (m_bPallet);
				//UpdateData (FALSE);
				OnChangeCount ();
			}
		}
	}

	if (pResult)
		* pResult = 0;
}

__int64 CNamedCountDlg::CalcCount () 
{
	__int64 lCount = m_lBoxCount - m_lBoxMin;

	if (m_bPallet) {
		if (m_lBoxCount < m_lBoxMin)
			lCount = m_lBoxMin;

		if (m_lPalletCount < m_lPalletMin)
			m_lPalletCount = m_lPalletMin;

		lCount = ((m_lPalletCount - m_lPalletMin) * m_lBoxesPerPallet) + lCount;
	}
	else 
		lCount = m_lBoxCount;

	return lCount;
}

void CNamedCountDlg::OnOK()
{
	FoxjetCommon::CEliteDlg::OnOK ();
	m_lCount = CalcCount ();
}

__int64 CNamedCountDlg::GetDlgItemInt64 (UINT nID)
{
	CString str;

	GetDlgItemText (nID, str);	
	return strtoul64 (str);
}

void CNamedCountDlg::OnChangeCount ()
{
	BeginWaitCursor ();

	m_lBoxCount			= GetDlgItemInt64 (TXT_COUNT);
	m_lPalletCount		= GetDlgItemInt64 (TXT_PALCOUNT);
	m_lUnitsPerPallet	= GetDlgItemInt64 (TXT_UNITSPERPALLET);
	m_lPalletMin		= GetDlgItemInt64 (TXT_MINPALCOUNT);
	m_lPalletMax		= GetDlgItemInt64 (TXT_MAXPALCOUNT);

	TRACEF (_T ("m_lBoxCount:       ") + FormatI64 (m_lBoxCount));
	TRACEF (_T ("m_lPalletCount:    ") + FormatI64 (m_lPalletCount));
	TRACEF (_T ("m_lUnitsPerPallet: ") + FormatI64 (m_lUnitsPerPallet));
	TRACEF (_T ("m_lPalletMin:      ") + FormatI64 (m_lPalletMin));
	TRACEF (_T ("m_lPalletMax:      ") + FormatI64 (m_lPalletMax));

	__int64 lCount = CalcCount ();

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (CCountItem * p = (CCountItem *)m_lv.GetCtrlData (i)) {
			const CCountElement * pCount = p->GetElement ();

			if (pCount->IsIrregularPalletSize ()) 
				p->SetCount (m_lBoxCount, m_lPalletCount, m_lPalletMin, m_lPalletMax, m_lUnitsPerPallet);
			else
				p->SetCount (lCount, m_lPalletMax);

			m_lv.UpdateCtrlData (p);
		}
	}

	EndWaitCursor ();
}
