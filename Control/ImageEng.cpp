// ImageEng.cpp: implementation of the CImageEng class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Control.h"
#include "Resource.h"
#include "Sw0848Thread.h"
#include "ProdLine.h"
#include "ImageEng.h"
#include "BaseElement.h"
#include "TypeDefs.h"
#include "Master.h"
#include "Resource.h"
#include "Debug.h"
#include "Utils.h"
#include "Color.h"
#include "TemplExt.h"
#include "Coord.h"
#include "ximage.h"
#include "Parse.h"
#include "Registry.h"
#include "Head.h"
#include "IpElements.h"
#include "CountElement.h"
#include "StartTaskDlg.h"
#include "Registry.h"

#ifdef __WINPRINTER__
	#include "DatabaseElement.h"
	#include "TextElement.h"
	#include "BitmapElement.h"
	#include "LocalHead.h"
#endif

#ifdef _DEBUG
	#include "Parse.h"
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////

CImageEng::CImageData::CImageData (ULONG lBmpHandle, const CRect & rc, ULONG lPixels, const CString & strData, FoxjetCommon::DRAWCONTEXT context) 
:	m_tm (CTime::GetCurrentTime ()) ,
	m_lBmpHandle (lBmpHandle),
	m_rc (rc),
	m_lPixels (lPixels),
	m_strData (strData), 
	m_context (context)
{ 
}

////////////////////////////////////////////////////////////////////////////////////////////////////

/*
void Format (USHORT n, CHAR * psz) // TODO: rem
{
	int nIndex= 0;

	for (int i = (sizeof (n) * 8) - 1; i >= 0; i--) 
		psz [nIndex++] = (n & (1 << i)) ? 'X' : '.';
}

{
	USHORT wWidthBits = (USHORT)bm.bmWidth;
	USHORT wColumns = (USHORT)bm.bmHeight;

	UCHAR cBPC = wWidthBits / 8;
//	USHORT wRowLen = (int)ceil ((long double)bm.bmWidth / 16.0); // win bitmap is word aligned
	USHORT wRowLen = (wWidthBits / 16) + ((wWidthBits % 16) ? 1 : 0); // win bitmap is word aligned
	PUSHORT pBuffer = (PUSHORT )pImageBuffer;

	for (int j = 0; j < wColumns; j++) {
		CHAR szDebug [512];
		int nDebugIndex = 0;

		{
			for (int i = 0; i < ARRAYSIZE (szDebug); i++) 
				szDebug [i] = ' ';
		}

		PUCHAR p = (PUCHAR)pBuffer;

		for (int i = 0; i < cBPC; i += 2) {
			USHORT w = p [i] << 8 | p [i + 1];
			int nReg = IV_REG_DATA_PORT + ((i % 4) ? 2 : 0);

			Format (w, &szDebug [nDebugIndex]);
			nDebugIndex += 17;
		}

		if (cBPC % 2) {
			int nReg = IV_REG_DATA_PORT + 2;
			Format (0x0000, &szDebug [nDebugIndex]);
			nDebugIndex += 17;
		}

		pBuffer += wRowLen;

		CString str;
		str.Format ("%03d: %s", 
			j + 1, 
			szDebug);
		str += "\n";
		TRACER (str);
	}
}
int nBreak = 0;
*/

#ifdef __WINPRINTER__
	#include "LabelElement.h"
#endif //__WINPRINTER__

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace Color;

typedef struct
{
	int							m_nStart;
	int							m_nLen;
	FoxjetDatabase::VALVESTRUCT m_valve;
} VALVEBOUNDARYSTRUCT;

//////////////////////////////////////////////////////////////////////

static int CalcValveYPos (const FoxjetDatabase::HEADSTRUCT & head, ULONG y1000ths)
{
	using namespace FoxjetDatabase;

	VALVETYPE type = IV_12_9;

	if (head.m_vValve.GetSize ())
		type = head.m_vValve [0].m_type;

	ULONG y = 0;
	int nChannels = 0;

	for (int i = 0; i < head.m_vValve.GetSize (); i++) {
		type = head.m_vValve [i].m_type;

		ULONG lHeight = GetValveHeight (type);
		ULONG cy = y + lHeight;

		if (y1000ths >= y && y1000ths < cy) 
			break;

		y			+= lHeight;
		nChannels	+= GetValveChannels (type);
	}

	ULONG lOffset = y1000ths - y;
	double dDot = (double)GetValveHeight (type) / (double)GetValveChannels (type);
	int nOffset = DivideAndRound (lOffset, dDot);

	return nChannels + nOffset;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImageEng::CImageEng()
:	m_pImageBitmap (NULL),
	m_pPreviewBitmap (NULL),
	m_nImageLen (0),
	m_productArea (0, 0),
	m_dInkUsage (0.0),
	m_lPixelCount (0),
	m_lEditorPixels (-1),
	m_lPrinterPixels (-1)
{
	VERIFY (m_dcImage.CreateCompatibleDC (NULL));
	VERIFY (m_dcTemp.CreateCompatibleDC (&m_dcImage));
	
	m_dcImage.SetStretchBltMode (COLORONCOLOR);
	m_dcTemp.SetStretchBltMode (COLORONCOLOR);
}

CImageEng::~CImageEng()
{
	Free ();
}

void CImageEng::Free ()
{
	//LOCK (m_csPreview);

	if (m_pImageBitmap) {
		delete m_pImageBitmap;
		m_pImageBitmap = NULL;
	}

	if (m_pPreviewBitmap) {
		delete m_pPreviewBitmap;
		m_pPreviewBitmap = NULL;
	}
}


int CImageEng::GenImage(FoxjetCommon::CElementList *pElementList, 
						const FoxjetDatabase::HEADSTRUCT & HeadCfg,
						void *pImageBuffer,
						unsigned long nImageBufferLen,
						BUILDTYPE imgtype,
						CPtrList &UpdateList, 
						FoxjetCommon::DRAWCONTEXT Context,
						bool bPreviewOnly /* SW0828 */,
						Head::CHead * pThread)
{
	using namespace FoxjetCommon;

	CArray <CImageEng::CImageData *, CImageEng::CImageData *> * pvDebug = NULL;
	Head::CHeadInfo info = Head::CHead::GetInfo (pThread);

	//m_csPreview.m_strName = _T ("CImageEng::m_csPreview:") + pThread->m_Config.m_HeadSettings.m_strUID;
	ASSERT (pThread->m_nThreadID == ::GetCurrentThreadId ()); //LOCK (m_csPreview);	

	bool bCapture = false;
	bool bBlackBox = false;

	if (pThread && theApp.IsDroppedElementTest ())
		pvDebug = new CArray <CImageEng::CImageData *, CImageEng::CImageData *> ();

	#ifdef __WINPRINTER__						
	using namespace FoxjetElements;
	#endif

	if (FoxjetDatabase::IsValveHead (HeadCfg) && Context == EDITOR) {
		CxImage img;

		if (!m_pImageBitmap) {
			CPtrList tmp;

			GenImage (pElementList, HeadCfg, pImageBuffer, nImageBufferLen, imgtype, tmp, PRINTER, bPreviewOnly);

			while (!tmp.IsEmpty ())
				delete (tagImgUpdateDesc *)tmp.RemoveHead();
		}

		ASSERT (m_pImageBitmap);

		if (m_pPreviewBitmap) {
			delete m_pPreviewBitmap;
			m_pPreviewBitmap = NULL;
		}
		
		img.CreateFromHBITMAP (* m_pImageBitmap);
		img.RotateLeft ();
		img.Negative ();
		img.Flip ();

		//{ HBITMAP h = img.MakeBitmap (m_dcImage); SAVEBITMAP (h, "C:\\Temp\\Debug\\img.bmp"); ::DeleteObject (h); }

		m_pPreviewBitmap = new CBitmap;
		m_pPreviewBitmap->Attach (img.MakeBitmap (m_dcImage));

		return IMAGE_SUCCESS;
	}

	//TRACEF ("GenImage: " + HeadCfg.m_strName + " [PRINTER]"); 

	const int iAlignment = sizeof(DWORD) * 8;		// Scan lines lengths must be aligned on DWORD boundries.
	CBitmap *pOldBitmap;
	HBITMAP hBmpTest = NULL;
	int nChannels = (Context == PRINTER) ? GlobalFuncs::GetPhcChannels (HeadCfg) : HeadCfg.m_nChannels;
	const int nShift = FoxjetDatabase::IsValveHead (HeadCfg) ? 0 : nChannels - HeadCfg.m_nChannels;

	#ifdef __WINPRINTER__
	if (Context == PRINTER) {
		if (CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pThread)) {
			Head::CHeadInfo info = Head::CHead::GetInfo (p);

			if (info.IsUSB ())
				nChannels = HeadCfg.m_nChannels;
		}
	}
	#endif //__WINPRINTER__

#ifdef __WYSIWYGFIX__
	const double dScreenRes = FoxjetCommon::CCoord::GetPixelsPerXUnit (INCHES, HeadCfg);
	const double dPrintRes = CBaseElement::CalcPrintRes (HeadCfg);
	const double dFactor = dScreenRes / dPrintRes;
	double dStretch [2] = { 0 };

	CBaseElement::CalcStretch (HeadCfg, dStretch);
#endif

	if (imgtype == INITIAL) { //( bInitialImage )
		if ( Context == EDITOR )
		{
			if ( m_pPreviewBitmap )
			{
				delete m_pPreviewBitmap;
				m_pPreviewBitmap = NULL;
			}
		}
		else {
			m_dInkUsage = 0.0;
			m_lPixelCount = 0;
		}
	}

	if (Context == PRINTER)
		while ( !UpdateList.IsEmpty() )
			delete ( tagImgUpdateDesc *) UpdateList.RemoveHead();

	m_dcTemp.SetBkColor (RGB(255,255,255));
	m_dcTemp.SetTextColor (RGB(0,0,0));

	if (pElementList == NULL) {
		UINT nID = IDB_TESTVALVE;
		
		if (!FoxjetDatabase::IsValveHead (HeadCfg)) {
			switch (HeadCfg.GetChannels ()) {
			case 32:	nID = IDB_TEST32;	break;
			case 128:	nID = IDB_TEST128;	break;
			case 256:	nID = IDB_TEST256;	break;
			}
		}

		hBmpTest = (HBITMAP)::LoadImage (::GetModuleHandle (NULL), 
			MAKEINTRESOURCE (nID), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_MONOCHROME);
		DIBSECTION ds;

		ASSERT (hBmpTest);

		CBitmap * pDef = CBitmap::FromHandle (hBmpTest);
		::ZeroMemory (&ds, sizeof (ds));
		VERIFY (pDef->GetObject (sizeof (ds), &ds));
		const CSize size (ds.dsBm.bmWidth, ds.dsBm.bmHeight);

		m_productArea.cx = size.cx;
		m_productArea.cy = HeadCfg.GetChannels ();
	}
	else {
		m_productArea = pElementList->GetProductArea();
		m_productArea = CBaseElement::ThousandthsToLogical(m_productArea, HeadCfg);
		m_productArea.cy = HeadCfg.m_nChannels;
	}
	
	if (Context == PRINTER)
		swap (m_productArea.cx, m_productArea.cy);

	if (imgtype == INITIAL) { //( bInitialImage )
		if (pElementList && m_pImageBitmap) {
			delete m_pImageBitmap;
			m_pImageBitmap = NULL;
		}
	}

	if ( Context == PRINTER )
	{

#if defined( __WYSIWYGFIX__ ) && defined( __WINPRINTER__ )
		bool bValveTestPattern = (pElementList == NULL) && FoxjetDatabase::IsValveHead (HeadCfg);

		if (!bValveTestPattern)
			m_productArea.cy = (int)((double)m_productArea.cy * (1.0 / dFactor));
#endif

		if ( m_pImageBitmap == NULL )
		{
			m_productArea.cx = nChannels;
			//m_productArea.cx = FoxjetDatabase::IsValveHead (HeadCfg) ? 72 : nChannels;

			m_pImageBitmap = new CBitmap;
			m_pImageBitmap->CreateCompatibleBitmap (&m_dcImage, m_productArea.cx, m_productArea.cy);
			m_pImageBitmap->SetBitmapDimension ( m_productArea.cx, m_productArea.cy);
		}
		pOldBitmap = m_dcImage.SelectObject (m_pImageBitmap);
	}
	else
	{
		m_pPreviewBitmap = new CBitmap;
		m_pPreviewBitmap->CreateCompatibleBitmap (&m_dcImage, m_productArea.cx, m_productArea.cy);
		m_pPreviewBitmap->SetBitmapDimension ( m_productArea.cx, m_productArea.cy);
		pOldBitmap = m_dcImage.SelectObject (m_pPreviewBitmap);
	}

	{
		CSize size = m_productArea;
		
		if (CBitmap * p = m_dcImage.GetCurrentBitmap ()) {
			BITMAP bm;

			p->GetObject (sizeof (bm), &bm);
			size.cx = bm.bmWidth;
			size.cy = bm.bmHeight;
		}

		::BitBlt (m_dcImage, 0, 0, size.cx, size.cy, NULL, 0, 0, Context == PRINTER ? BLACKNESS : WHITENESS);
	}


	ULONG lAddress = _tcstoul (HeadCfg.m_strUID, NULL, 16);
	bool bBuildImage = true;

	#ifdef __WINPRINTER__
	if (FoxjetCommon::IsProductionConfig ()) 
		if (!Foxjet3d::GetTestPHC (lAddress))
			bBuildImage = false;
	#endif //__WINPRINTER__

	if (bBuildImage) {
		if (pElementList == NULL) {
			if (!HeadCfg.m_bRemoteHead) {
				CBitmap * pTest = CBitmap::FromHandle (hBmpTest);
				CBitmap bmp;

				ASSERT (pTest);

				CDC dcTest, dcMem;
				DIBSECTION ds;
				::ZeroMemory (&ds, sizeof (ds));
				VERIFY (pTest->GetObject (sizeof (ds), &ds));
				const CSize size (ds.dsBm.bmWidth, ds.dsBm.bmHeight);
				CRect rcText (10, 0, size.cx, HeadCfg.m_nChannels);
				CFont fnt;

				BOOL bCreateFont = fnt.CreateFont (32, 0, 0, 0, FW_BOLD, FALSE, 0, 0,
					DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
					DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("MK TIMES")); 

				dcTest.CreateCompatibleDC (NULL);
				dcMem.CreateCompatibleDC (NULL);
				bmp.CreateCompatibleBitmap (&m_dcImage, size.cx, HeadCfg.m_nChannels);

				CBitmap * pOldTest = dcTest.SelectObject (pTest);
				CBitmap * pOldMem = dcMem.SelectObject (&bmp);
				CFont * pOldFont = dcMem.SelectObject (&fnt);
				COLORREF rgbSetTextColor = dcMem.SetTextColor (Color::rgbBlack);
				int nBkMode = dcMem.SetBkMode (OPAQUE);

				dcMem.StretchBlt (0, 0, size.cx, HeadCfg.m_nChannels, &dcTest, 0, 0, size.cx, size.cy, SRCCOPY);

				if (!FoxjetDatabase::IsValveHead (HeadCfg)) {
					dcMem.DrawText (HeadCfg.m_strName, &rcText, 0);
					dcMem.FrameRect (CRect (0, 0, size.cx, HeadCfg.m_nChannels), &CBrush (Color::rgbBlack));
				}

				if (Context == PRINTER) {
					CBitmap bmpPrinter;
					int x = nShift, y = 0, cx = HeadCfg.m_nChannels, cy = size.cx;

					::BitBlt (dcMem, 0, 0, cy, cx, dcMem, 0, 0, DSTINVERT);

					// flip h
					x += (cx - 1);
					cx *= -1;

					dcMem.SelectObject (pOldMem);
					RotateRight90 (dcMem, bmp, bmpPrinter); 
					dcMem.SelectObject (&bmp);

					//SAVEBITMAP (bmpPrinter, _T ("C:\\Temp\\Debug\\bmpPrinter.bmp"));

					CBitmap * pOld = dcMem.SelectObject (&bmpPrinter);
					CBitmap * pOldImage = m_dcImage.SelectObject (m_pImageBitmap);
					int nStretchBltMode = m_dcImage.SetStretchBltMode (HALFTONE);

					m_dcImage.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, HeadCfg.m_nChannels, size.cx, SRCCOPY);
					m_dcImage.SelectObject (pOldImage);
					dcMem.SelectObject (pOld);
					m_dcImage.SetStretchBltMode (nStretchBltMode);
				}
				else {
					CBitmap * pOldPreview = m_dcImage.SelectObject (m_pPreviewBitmap);
					m_dcImage.BitBlt (0, 0, size.cx, HeadCfg.m_nChannels, &dcMem, 0, 0, SRCCOPY);
					m_dcImage.SelectObject (pOldPreview);
				}

				dcTest.SelectObject (pOldTest);
				dcMem.SelectObject (pOldMem);
				dcMem.SelectObject (pOldFont);
				dcMem.SetTextColor (rgbSetTextColor);
				dcMem.SetBkMode (nBkMode);
			}

			if (hBmpTest)
				::DeleteObject (hBmpTest);
		}
		else {
			HBITMAP hbmp = NULL;
			HDC hdc = NULL;
			const int nItems = pElementList->GetSize ();
			const CSize sizeMax = GetMaxElementSize (pElementList, HeadCfg, pImageBuffer, nImageBufferLen, imgtype, UpdateList, 
				Context, bPreviewOnly, pThread, info);

			VERIFY (hdc = ::CreateCompatibleDC (m_dcImage));

			for (int i = 0; i < nItems; i++) {
				FoxjetCommon::CBaseElement & element = pElementList->GetObject (i);

				if (!element.IsPrintable ()) // && Context == PRINTER) 
					continue;

				#ifdef __WINPRINTER__						
				{
					using namespace FoxjetElements;

					if (element.GetClassID () == BARCODE) 		
						element.SetRedraw ();

					if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, &element))
						if (p->GetSerialDownload ())
							continue;
				}
				#endif										

				CSize elemImgSize = element.Draw (* CDC::FromHandle (hdc), HeadCfg, true, Context);  

				#ifdef __WINPRINTER__						
				{
					using namespace TextElement;

					// note that this depends on CHead::Load placing the linked text elements last
					if (CTextElement * pElement = DYNAMIC_DOWNCAST (CTextElement, &element)) 
						if (pElement->GetLink ()) 
							pElement->Build (* pElementList); 
				}
				#endif //__WINPRINTER__						

				if (Context == EDITOR) {
					elemImgSize = CBaseElement::ThousandthsToLogical (elemImgSize, HeadCfg);

					#ifdef __WYSIWYGFIX__
					elemImgSize.cx = (int)((double)elemImgSize.cx / dStretch [0]);
					#endif
				}

				CSize size = Context == PRINTER ? CSize ((int)(elemImgSize.cy / dStretch [0]), elemImgSize.cx) : elemImgSize;
				CPoint pt1000ths = element.GetPosInternal (FoxjetCommon::ALIGNMENT_ADJUST_NONE);

				{
					using namespace FoxjetIpElements;

					if (CIpBaseElement * pIpBase = DYNAMIC_DOWNCAST (CIpBaseElement, &element)) 
						pt1000ths = pIpBase->GetPos (&HeadCfg);
				}

				const CPoint ptLogical = CBaseElement::ThousandthsToLogical (pt1000ths, HeadCfg);
				const CPoint ptInternal = element.GetPosInternal (ptLogical, size, ALIGNMENT_ADJUST_SUBTRACT);
				CPoint Pos = ptInternal;

				double dStretch [2] = { 1.0, 1.0 };

				element.CalcStretch (HeadCfg, dStretch);

				Pos.y = (int)((double)Pos.y / dStretch [1]);

				if (Context == PRINTER) {

					#if defined( __WYSIWYGFIX__ ) && defined( __WINPRINTER__ )
					Pos.x = (int)((double)Pos.x * (1.0 / dFactor));
					#endif

					if (nShift && Pos.y) 
						Pos.y--; // got no explanation for this, but it prints ok

					swap (Pos.x, Pos.y);
				}

				if (FoxjetDatabase::IsValveHead (HeadCfg))
					Pos.x = CalcValveYPos (HeadCfg, pt1000ths.y);

				if (elemImgSize.cx > 0 && elemImgSize.cy > 0) {
					const ULONG lTotal = elemImgSize.cx * elemImgSize.cy;
					const int nMax = 3;
					bool bReimage = true;
					int nTries = 0;
					ULONG lPixels = 0;

					CString strImageData = element.GetImageData ();

					strImageData.TrimLeft ();
					strImageData.TrimRight ();

					do {
						const CPoint pt1000ths = element.GetPos (&HeadCfg);
						const CRect rc (CRect (element.ThousandthsToLogical (pt1000ths, HeadCfg), elemImgSize));

						if (bReimage) {
							if (!hbmp)
								VERIFY (hbmp = ::CreateCompatibleBitmap (hdc, sizeMax.cx, sizeMax.cy));
							
							HGDIOBJ hbmpOld = ::SelectObject (hdc, hbmp);
							::BitBlt (hdc, 0, 0, sizeMax.cx, sizeMax.cy, NULL, 0, 0, Context == PRINTER ? BLACKNESS : WHITENESS);

							element.Draw (* CDC::FromHandle (hdc), HeadCfg, false, Context);

							#ifdef _DEBUG
							/*
							if (Context == PRINTER) {
								if (element.GetClassID () == BMP) {
									CString str;
									static int nCount = 1;
									CxImage img;

									str.Format (_T ("C:\\Foxjet\\Debug\\%s [%03d] [%03d].bmp"), HeadCfg.m_strName, element.GetID (), nCount++);
									img.CreateFromHBITMAP (hbmp);
									img.Mirror ();
									img.Negative ();
									img.Rotate (270);

									if (HBITMAP h = img.MakeBitmap (NULL)) {
										SAVEBITMAP (h, str);
										::DeleteObject (h);
									}

									if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, &element)) {
										str.Format (_T ("C:\\Foxjet\\Debug\\%s [%03d] [%03d] [ORIGINAL].bmp"), HeadCfg.m_strName, element.GetID (), nCount++);
										::CopyFile (p->GetFilePath (), str, FALSE);
									}
								}
							}
							*/

							//if (Context == PRINTER) {
							//	if (element.GetClassID() == FoxjetIpElements::COUNT) {
							//		TRACEF(element.GetImageData());
							//		int n = 0;
							//	}
							//}
							//if (Context == PRINTER && element.GetClassID () == DATABASE) {
							//	TRACEF (element.GetImageData ());
							//	int n = 0;
							//}
							//if (element.GetClassID () == BMP) {
							//	CSize size = element.GetSize (HeadCfg);
							//	TRACEF (FoxjetDatabase::ToString (size.cx) + _T (", ") + FoxjetDatabase::ToString (size.cy));
							//	int n = 0;
							//}
							#endif

							if (pThread) {
								using namespace FoxjetElements;

								if (element.GetClassID () == COUNT) {
									if (CCountElement * p = DYNAMIC_DOWNCAST (CCountElement, &element)) {
										if (p->IsIrregularPalletSize ()) {
											if (!p->IsPalletCount ()) 
												pThread->m_countLast.m_lCount			= p->GetCount ();
											else {
												pThread->m_countLast.m_lPalletCount		= p->GetPalCount ();
												pThread->m_countLast.m_lPalletMin		= p->GetPalMin ();
												pThread->m_countLast.m_lPalletMax		= p->GetPalMax ();
												pThread->m_countLast.m_lUnitsPerPallet	= p->GetMax ();
											}
										}
									}
								}
							}

							lPixels = CountPixels (hbmp);
							/* TODO: rem
							ULONG lElementPixels = Context == EDITOR ? (((ULONG)(sizeMax.cx * sizeMax.cy)) - lPixels) : lPixels;
							bReimage = (lElementPixels == 0) && (strImageData.GetLength () > 0);
							*/
							if (strImageData.GetLength () > 0) {
								if (Context == EDITOR) 
									bReimage = (lPixels == 0);
								else
									bReimage = (lPixels == (ULONG)(sizeMax.cx * sizeMax.cy));
							}

							if (!bReimage) {
								int x = Pos.x, y = Pos.y, cx = elemImgSize.cx, cy = elemImgSize.cy;

								::BitBlt (m_dcImage, x, y, cx, cy, hdc, 0, 0, Context == PRINTER ? SRCPAINT : SRCAND);
							}
							else {
								bBlackBox = true;
								::SelectObject (hdc, hbmpOld);
								::DeleteObject (hbmp);
								hbmp = NULL;
							}

							if (pvDebug) {
								CString str;

								str.Format (_T ("[%-3d]  [%d] %s"), element.GetID (), nTries, element.GetImageData ());
								pvDebug->Add (new CImageData ((ULONG)hbmp, rc, lPixels, str, Context));
							}

							::SelectObject (hdc, hbmpOld);
						}

						nTries++;
					} while (bReimage && (nTries <= nMax));
				}
			}

			if (hbmp) {
				::DeleteObject (hbmp);
				hbmp = NULL;
			}
			if (hdc) {
				::DeleteDC (hdc);
				hdc = NULL;
			}
		}
	}

/*
	if (Context == PRINTER) { // http
		DECLARETRACECOUNT (10);
		MARKTRACECOUNT ();

		const CString strDir = FoxjetDatabase::GetHomeDir () + _T ("\\Debug");
		CString str;

		if (::GetFileAttributes (strDir) == -1) {
			::SetCurrentDirectory (FoxjetDatabase::GetHomeDir ());
	
			if (!::CreateDirectory (_T ("Debug"), NULL)) {
				TRACEF (FormatMessage (GetLastError ()));
			}

			ASSERT (::GetFileAttributes (strDir) != -1);
		}

		str.Format (_T ("%s\\%s.bmp"), strDir, HeadCfg.m_strUID);

		if (CBitmap * p = m_dcImage.GetCurrentBitmap ()) {
			TRACEF (str);
			SaveBitmap (* p, str);
		}

		MARKTRACECOUNT ();
		TRACECOUNTARRAYMAX ();
		int n = 0;
	}
*/

	if (theApp.IsCaptureDebugImage () && Context == PRINTER) {
		const CString strDir = FoxjetDatabase::GetHomeDir () + _T ("\\Debug");
		CString str;

		if (::GetFileAttributes (strDir) == -1) {
			::SetCurrentDirectory (FoxjetDatabase::GetHomeDir ());
	
			if (!::CreateDirectory (_T ("Debug"), NULL)) {
				TRACEF (FormatMessage (GetLastError ()));
			}

			ASSERT (::GetFileAttributes (strDir) != -1);
		}

		str.Format (_T ("%s\\[%s] %s%s.bmp"),
			strDir, 
			pThread ? info.GetCount () : _T ("x"),
			Context == EDITOR ? _T (" [EDITOR]") : _T (" [PRINTER]"),
			HeadCfg.m_strName);

		if (CBitmap * p = m_dcImage.GetCurrentBitmap ()) {
			TRACEF (str);
			SaveBitmap (* p, str);
		}
	}

	if (nShift && pElementList && Context == PRINTER)
		m_dcImage.BitBlt (0, 0, nChannels, m_productArea.cy, &m_dcImage, -nShift, 0, SRCCOPY);

#ifndef __IPPRINTER__

	if ( Context == PRINTER )
	{
		if (pImageBuffer == NULL) {
			if (pThread) { POST_THREAD_MESSAGE (info, TM_DISPLAY_DEBUG_MSG, new CString (_T ("return -3"))); }
			return -3;
		}

		ZeroMemory ( pImageBuffer, nImageBufferLen );

		BITMAP bm;
		m_pImageBitmap->GetBitmap (&bm);

		if (FoxjetDatabase::IsValveHead (HeadCfg)) {
			CDC dcPrinter, dcValve;
			CBitmap bmpValve;
			int x = 0, cy = bm.bmHeight;
			const int nBufferHeight = 72;
			CArray <VALVEBOUNDARYSTRUCT, VALVEBOUNDARYSTRUCT &> vBoundary;
			int i;

			for (i = 0; i < HeadCfg.m_vValve.GetSize (); i++) {
				int nChannels = FoxjetDatabase::GetValveChannels (HeadCfg.m_vValve [i].m_type);
				VALVEBOUNDARYSTRUCT bounds;

				bounds.m_nStart = x;
				bounds.m_nLen	= nChannels;
				bounds.m_valve	= HeadCfg.m_vValve [i]; 

				x += nChannels;

				vBoundary.Add (bounds);
			}

			VERIFY (dcPrinter.CreateCompatibleDC (NULL));
			VERIFY (dcValve.CreateCompatibleDC (NULL));
			VERIFY (bmpValve.CreateCompatibleBitmap (&dcValve, nBufferHeight, cy));

			CBitmap * pImage = m_dcImage.SelectObject (pOldBitmap);
			CBitmap * pPrinter = dcPrinter.SelectObject (m_pImageBitmap);
			CBitmap * pValve = dcValve.SelectObject (&bmpValve);

			::BitBlt (dcValve, 0, 0, nBufferHeight, cy, NULL, 0, 0, BLACKNESS);

			for (x = 0, i = 0; i < vBoundary.GetSize (); i++) {
				VALVEBOUNDARYSTRUCT bounds = vBoundary [i];
				int nStart		= bounds.m_nStart;
				int nLen		= bounds.m_nLen;

				int nXDest		= nStart;
				int nYDest		= 0;
				int nWidthDest	= nLen;
				int nHeightDest = cy;

				int nXSrc		= x;
				int nYSrc		= 0;

				bool bReversed = bounds.m_valve.m_bReversed ? true : false;

				if (bReversed) {
					int nWidthSrc = nWidthDest;
					int nHeightSrc = nHeightDest;

					nYDest += (nHeightDest - 1);
					nHeightDest *= -1;

					VERIFY (dcValve.StretchBlt (nXDest, nYDest, nWidthDest, nHeightDest, &dcPrinter, 
						nXSrc, nYSrc, nWidthSrc, nHeightSrc, SRCCOPY));
				}
				else 
					VERIFY (dcValve.BitBlt (nXDest, nYDest, nWidthDest, nHeightDest, &dcPrinter, nXSrc, nYSrc, SRCCOPY));

				if (bounds.m_valve.m_bUpsidedown) {
					int x = nXSrc, y = nYSrc, cx = nWidthDest, cy = nHeightDest;

					x += (cx - 1);
					cx *= -1;

					y += (cy - 1);
					cy *= -1;

					VERIFY (dcValve.StretchBlt (nXSrc, nYSrc, nWidthDest, nHeightDest, &dcValve, 
						x, y, cx, cy, SRCCOPY));
				}

				x += nLen;
			}

			{ 
				CBitmap bmpIV;
				CxImage img;

				img.CreateFromHBITMAP (bmpValve);
				img.Negative ();
				img.Flip ();
				
				bmpIV.Attach (img.MakeBitmap (dcValve));
	
				bmpIV.GetObject (sizeof (bm), &bm);
				m_nImageLen = bm.bmWidthBytes * bm.bmHeight;

				//SAVEBITMAP (bmpIV, "C:\\Temp\\Debug\\" + HeadCfg.m_strName + " [Rotatate180].bmp");
				DWORD dwBits = bmpIV.GetBitmapBits (m_nImageLen, pImageBuffer);

				/*
				CLocalHead::DbgPrintBuffer ((USHORT)bm.bmHeight, 
					(USHORT)bm.bmWidth / 8, (USHORT)bm.bmWidthBytes / 2, 
					(PUCHAR)pImageBuffer);
				*/
			}

			m_lPixelCount = CountPixels (bm.bmWidthBytes, bm.bmHeight, (LPBYTE)pImageBuffer);
			m_dInkUsage = FoxjetDatabase::CalcInkUsage (HeadCfg.m_nHeadType, FoxjetDatabase::INKTYPE_FIRST, m_lPixelCount); 

			dcPrinter.SelectObject (pPrinter);
			dcValve.SelectObject (pValve);
			m_dcImage.SelectObject (pImage);
		}
		else {
			m_nImageLen = bm.bmWidthBytes * bm.bmHeight;
			
			if (CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pThread)) {
				Head::CHeadInfo info = Head::CHead::GetInfo (p);
				DWORD dwSize = info.GetImageBufferSize ();
	
				if ( m_nImageLen > dwSize) {
					CString str = HeadCfg.m_strName + ": image is too long for MPHC";
					TRACEF (str);
					if (pThread) { POST_THREAD_MESSAGE (info, TM_DISPLAY_DEBUG_MSG, new CString (_T ("[return -4]"))); }
					return -4;
				}
			}

			if (HeadCfg.m_bUpsidedown) 
				Rotate180 (m_dcImage, CSize (bm.bmWidth, bm.bmHeight));

			if (m_pImageBitmap)
				m_pImageBitmap->GetBitmapBits (m_nImageLen, pImageBuffer);

			m_lPixelCount = CountPixels (bm.bmWidthBytes, bm.bmHeight, (LPBYTE)pImageBuffer);
			m_dInkUsage = FoxjetDatabase::CalcInkUsage (HeadCfg.m_nHeadType, FoxjetDatabase::INKTYPE_FIRST, m_lPixelCount); 
		}
		
		if (pvDebug || (pvDebug && bBlackBox)) {
			ULONG lPixels = Context == EDITOR ? (((ULONG)(bm.bmWidthBytes * 8 * bm.bmHeight)) - m_lPixelCount) : m_lPixelCount;
			const CString strDir = FoxjetDatabase::GetHomeDir () + _T ("\\Debug");
			CString str;

			ULONG lCapture = FoxjetDatabase::GetProfileInt (CControlApp::GetRegKey () + _T ("\\Settings\\") + HeadCfg.m_strName, _T ("SerialNumber"), 0); 
			FoxjetDatabase::WriteProfileInt (CControlApp::GetRegKey () + _T ("\\Settings\\") + HeadCfg.m_strName, _T ("SerialNumber"), lCapture + 1); 

			str.Format (_T ("%s\\%s %s%s(%06d).bmp"),
				strDir, 
				HeadCfg.m_strName,
				pThread ? info.m_sTaskName : _T (""),
				Context == EDITOR ? _T (" [EDITOR]") : _T (" [PRINTER]"),
				lCapture);

			if (bBlackBox) {
				TRACER (_T ("[") + FoxjetDatabase::ToString (lPixels) + _T (" pixels]: ") + str + _T ("\n"));
				SaveBitmap (* m_pImageBitmap, str);
			}

			if ((m_lPrinterPixels != (ULONG)m_lPixelCount) || bCapture) {
				bCapture = true;
				m_lPrinterPixels = m_lPixelCount;
			}

			if (pvDebug)
				pvDebug->Add (new CImageData ((ULONG)m_pImageBitmap->m_hObject, CRect (0, 0, bm.bmWidthBytes * 8, bm.bmHeight), lPixels, str, Context));
		}


		if (HeadCfg.m_bDoublePulse) 
			m_dInkUsage *= 2;

		bool bForceDownload = pThread ? (theApp.GetImagingMode () == IMAGE_ON_PHOTOCELL) : false;

		if (imgtype != INITIAL || bForceDownload) { 
			long remaining = m_nImageLen;
			WORD Loc = 0;
			int nBPC = GlobalFuncs::GetPhcChannels (HeadCfg) / 8;

			WORD NumCols		= m_nImageLen / nBPC;
			WORD ColsRemaing	= m_nImageLen / nBPC;

			if (FoxjetDatabase::IsValveHead (HeadCfg)) {
				ColsRemaing = NumCols = (WORD)bm.bmHeight;
				nBPC = bm.bmWidth / 8;
				
				ASSERT (bm.bmWidth == 72); // IV driver expects 72 pixel wide image
				ASSERT (nBPC == 9); 
			}

			WORD TxCols = NumCols;//256;
			WORD CurCol;
			int nDirection = HeadCfg.m_nDirection;

			//if (HeadCfg.m_bInverted ^ HeadCfg.m_bUpsidedown)
			//	nDirection = (nDirection == FoxjetDatabase::LTOR) ? FoxjetDatabase::RTOL : FoxjetDatabase::LTOR;

			if (nDirection == FoxjetDatabase::LTOR)
				CurCol = 0;
			else
				CurCol = NumCols - TxCols;

			{ // http
				DECLARETRACECOUNT (20);
				MARKTRACECOUNT ();

				const CString strDir = CControlApp::GetPreviewDirectory();
				CString str;

				if (::GetFileAttributes (strDir) == -1) {
					FoxjetDatabase::CreateDir(strDir);
					ASSERT (::GetFileAttributes (strDir) != -1);
				}

				str.Format (_T ("%s\\%s.bmp"), strDir, HeadCfg.m_strUID);
				CxImage img;

				MARKTRACECOUNT ();
				if (img.CreateFromArray ((BYTE *)pImageBuffer, bm.bmWidth, bm.bmHeight, bm.bmBitsPixel, bm.bmWidthBytes, false)) {
					MARKTRACECOUNT ();

					img.Flip ();

					if (img.Save (str, CXIMAGE_FORMAT_BMP)) {

						MARKTRACECOUNT ();
						CString strHash = FoxjetDatabase::Encrypted::GetSha256 ((BYTE *)pImageBuffer, m_nImageLen);
						MARKTRACECOUNT ();

						CString strFile = str;

						//TRACEF (str + _T (": ") + strHash);

						strFile.Replace (_T (".bmp"), _T (".hash.txt"));

						MARKTRACECOUNT ();
						if (FILE * fp = _tfopen (strFile, _T ("w"))) {
							fwrite (w2a (strHash), strHash.GetLength (), 1, fp);
							fclose (fp);
						}
						MARKTRACECOUNT ();
					}
					MARKTRACECOUNT ();
				}

				MARKTRACECOUNT ();
				//TRACECOUNTARRAYMAX ();
				int n = 0;
			}

			while ( ColsRemaing != 0 )
			{
				try 
				{
					tagImgUpdateDesc *ImageUpdate = new tagImgUpdateDesc;
					ImageUpdate->nBPC			= nBPC;
					ImageUpdate->wStartCol		= CurCol;
					ImageUpdate->wEndCol		= TxCols;
					ImageUpdate->lLen			= m_nImageLen;
					ImageUpdate->wCurrentCol	= (nDirection == FoxjetDatabase::RTOL) ? 0 : TxCols;
					::QueryPerformanceCounter (&ImageUpdate->tm);

					UpdateList.AddTail ( ImageUpdate );

					/*
					#ifdef _DEBUG
						CString str;
						str.Format (_T ("[%s] ImageUpdate %s [nBPC: %d, wStartCol: %d, wEndCol: %d, lLen: %d, wCurrentCol: %d]"),
							HeadCfg.m_strName,
							nDirection == FoxjetDatabase::LTOR ? _T ("-->") : _T ("<--"),
							ImageUpdate->nBPC,
							ImageUpdate->wStartCol,
							ImageUpdate->wEndCol,
							ImageUpdate->lLen,
							ImageUpdate->wCurrentCol);
						TRACEF (str);
					#endif //_DEBUG
					*/
				}
				catch ( CMemoryException *e)  { HANDLEEXCEPTION (e); }

				ColsRemaing -= TxCols;
				if (nDirection == FoxjetDatabase::LTOR)
				{
					CurCol += TxCols;
					if ( ColsRemaing < TxCols )
						TxCols = ColsRemaing;
				}
				else
				{
					if ( ColsRemaing < TxCols )
						TxCols = ColsRemaing;
					CurCol -= TxCols;
				}
			}
		}
	}
	else {
		CString str;
		const CString strDir = FoxjetDatabase::GetHomeDir () + _T ("\\Debug");

		ULONG lCapture = FoxjetDatabase::GetProfileInt (CControlApp::GetRegKey () + _T ("\\Settings\\") + HeadCfg.m_strName, _T ("SerialNumber"), 0); 
		FoxjetDatabase::WriteProfileInt (CControlApp::GetRegKey () + _T ("\\Settings\\") + HeadCfg.m_strName, _T ("SerialNumber"), lCapture + 1); 

		str.Format (_T ("%s\\%s %s%s(%06d).bmp"),
			strDir, 
			HeadCfg.m_strName,
			pThread ? info.m_sTaskName : _T (""),
			Context == EDITOR ? _T (" [EDITOR]") : _T (" [PRINTER]"),
			lCapture);

		if (pvDebug && m_pPreviewBitmap) {
			BITMAP bm;

			ZeroMemory (&bm, sizeof (bm));
			m_pPreviewBitmap->GetObject (sizeof (bm), &bm);

			ULONG lPixels = Context == EDITOR ? (((ULONG)(bm.bmWidthBytes * 8 * bm.bmHeight)) - CountPixels (* m_pPreviewBitmap)) : CountPixels (* m_pPreviewBitmap);

			if ((m_lEditorPixels != (ULONG)lPixels) || bCapture) {
				bCapture = true;
				m_lEditorPixels = (ULONG)lPixels;
			}

			pvDebug->Add (new CImageData ((ULONG)m_pPreviewBitmap->m_hObject, CRect (0, 0, bm.bmWidthBytes * 8, bm.bmHeight), lPixels, str, Context));
			//{ CString strDebug; strDebug.Format (_T ("[%s]: %d px: %s"), CTime::GetCurrentTime ().Format (_T ("%c")), m_lEditorPixels, str); TRACEF (strDebug); }
		}

		if (bCapture && m_pPreviewBitmap) {
			//TRACEF (str);
			SaveBitmap (* m_pPreviewBitmap, str);
		}
	}

#endif
	m_dcImage.SelectObject ( pOldBitmap );

//{ CString str; str.Format ("   %s: GenImage [IMAGE_SUCCESS]: %s [0x%X]", CTime::GetCurrentTime ().Format ("%c"), HeadCfg.m_strName, ::GetCurrentThreadId ()); TRACEF (str); }

	if (pvDebug) {
		if (!bCapture || pvDebug->GetSize () <= 1) {
			while (pvDebug->GetSize ()) { 
				if (CImageEng::CImageData * p = pvDebug->GetAt (0))
					delete p;

				pvDebug->RemoveAt (0);
			}
				
			delete pvDebug;
		}
		else
			::PostMessage (GlobalFuncs::GetControlWnd (), WM_GENIMAGE_DEBUG, 0, (LPARAM)pvDebug);
	}

	return IMAGE_SUCCESS;
}

unsigned long CImageEng::GetImageLen()
{
	return m_nImageLen;
}


CSize CImageEng::GetMaxElementSize (FoxjetCommon::CElementList *pElementList, 
									const FoxjetDatabase::HEADSTRUCT & HeadCfg,
									void *pImageBuffer,
									unsigned long nImageBufferLen,
									BUILDTYPE imgtype,						
									CPtrList &UpdateList, 
									FoxjetCommon::DRAWCONTEXT Context,
									bool bPreviewOnly /* SW0828 */,
									Head::CHead * pThread, const Head::CHeadInfo & info)
{
	using namespace FoxjetCommon;

	#ifdef __WINPRINTER__						
	using namespace FoxjetElements;
	#endif

	#ifdef __WYSIWYGFIX__
	double dStretch [2] = { 0 };

	CBaseElement::CalcStretch (HeadCfg, dStretch);
	#endif

	int nItems = pElementList->GetSize ();
	CSize sizeMax (1, 1);

	for (int i = 0; i < nItems; i++) {
		FoxjetCommon::CBaseElement & element = pElementList->GetObject (i);

		if (!element.IsPrintable ())// && Context == PRINTER) 
			continue;

		#ifdef __WINPRINTER__						
		{
			using namespace FoxjetElements;

			if (element.GetClassID () == BARCODE) 		
				element.SetRedraw ();

			if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, &element))
				if (p->GetSerialDownload ())
					continue;
		}
		#endif										

		#ifdef __WINPRINTER__						
		if (element.GetClassID () == BARCODE) 		
			element.SetRedraw ();					
		#endif										

		//#ifdef _DEBUG
		////if (Context == PRINTER) 
		//{
		//	switch (element.GetClassID ()) {
		//	case FoxjetIpElements::COUNT:
		//	case FoxjetIpElements::DYNAMIC_TEXT:
		//	case FoxjetIpElements::DYNAMIC_BARCODE:
		//		element.SetRedraw ();
		//		TRACEF ("TODO: rem");
		//		break;
		//	}
		//}
		//#endif										

		if (FoxjetDatabase::IsHpHead (HeadCfg.m_nHeadType))
			element.Build (imgtype, true);
		else {
			if (!bPreviewOnly) { // SW0828
				try 
				{
					if (pThread && imgtype == INITIAL) {
						CString str;
						str.Format (LoadString (IDS_BUILDING_ELEMENT_X_OF_N),
							info.GetFriendlyName (),
							i + 1,
							nItems);
						POST_THREAD_MESSAGE (info, TM_UPDATE_STATUS_MSG, new CString (str));
					}

					//TRACER (info.GetFriendlyName () + _T (": [") + FoxjetDatabase::ToString (i) + _T ("] ") + element.ToString (::verApp));
					element.Build (imgtype, true);
				}
				catch (CElementException *e)
				{
					CString str = LoadString (IDS_ERROR) + HeadCfg.m_strName + e->GetErrorMessage();
					if (pThread) { POST_THREAD_MESSAGE (info, TM_DISPLAY_DEBUG_MSG, new CString (_T ("return -2"))); }
					if (pThread) { POST_THREAD_MESSAGE (info, TM_UPDATE_STATUS_MSG, new CString (str)); }

					#ifdef _DEBUG
					TRACEF (str);

					if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, &element)) {
						TRACEF (p->ToString (::verApp));
						TRACEF (p->GetSQL ());
					}
					#endif

					HANDLEEXCEPTION_TRACEONLY (e);

					//MsgBox (str);
					//return CSize (0, 0);
				}
			}
		}

		POST_THREAD_MESSAGE (info, TM_UPDATE_STATUS_MSG, new CString ());

		CSize size = element.Draw (m_dcTemp, HeadCfg, true, Context);  

		#ifdef __WINPRINTER__						
		{
			using namespace TextElement;

			// note that this depends on CHead::Load placing the linked text elements last
			if (CTextElement * pElement = DYNAMIC_DOWNCAST (CTextElement, &element)) 
				if (pElement->GetLink ()) 
					pElement->Build (* pElementList); 
		}
		#endif //__WINPRINTER__						

		if (Context == EDITOR) {
			size = CBaseElement::ThousandthsToLogical (size, HeadCfg);

			#ifdef __WYSIWYGFIX__
			size.cx = (int)((double)size.cx / dStretch [0]);
			//size.cy = (int)((double)size.cy / dStretch [1]);
			#endif
		}

		sizeMax.cx = max (sizeMax.cx, size.cx);
		sizeMax.cy = max (sizeMax.cy, size.cy);
	}

	return sizeMax;
}
