// CmdPacket.cpp: implementation of the CCmdPacket class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Resource.h"
#include "Control.h"
#include "CmdPacket.h"
#include "Debug.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCmdPacket::CCmdPacket()
{
	m_lControlNum = 0;
	TimeStamp = CTime::GetCurrentTime(); 
	memset (&m_pktInfo, 0x00, sizeof ( _SOCKET_MESSAGE) );
	m_hEvent = NULL;
}

/*
CCmdPacket::CCmdPacket( CCmdPacket *pCmd)
{
	m_lControlNum = pCmd->m_lControlNum;
	TimeStamp = CTime::GetCurrentTime();
	m_hEvent = pCmd->m_hEvent;
	memcpy ( &m_pktInfo.Header, &pCmd->m_pktInfo.Header, sizeof ( _SOCKET_MESSAGE_HEADER ) );
	if ( pCmd->m_pktInfo.pData != NULL )
	{
		try
		{
			m_pktInfo.pData = new UCHAR[m_pktInfo.Header.Length];
			memcpy ( m_pktInfo.pData, pCmd->m_pktInfo.pData, m_pktInfo.Header.Length );
		}
		catch ( CMemoryException *e) { HANDLEEXCEPTION (e); }
	}
}
*/

CCmdPacket::~CCmdPacket()
{
	if (m_pktInfo.pData) {
		delete [] m_pktInfo.pData;
		m_pktInfo.pData = NULL;
	}

	if (m_hEvent) {
		CloseHandle (m_hEvent);
		m_hEvent = NULL;
	}
}

void CCmdPacket::operator <<( const _SOCKET_MESSAGE *pMsg)
{
	m_pktInfo.Header.BatchID	= pMsg->Header.BatchID;
	m_pktInfo.Header.Cmd			= pMsg->Header.Cmd;
	m_pktInfo.Header.Length		= pMsg->Header.Length;
	m_pktInfo.pData = NULL;

	if ( pMsg->Header.Length )
	{
		try
		{
			m_pktInfo.pData = new UCHAR [m_pktInfo.Header.Length+1];
		}
		catch ( CMemoryException *e) { HANDLEEXCEPTION (e); }

		memset ( m_pktInfo.pData, 0x00, m_pktInfo.Header.Length+1);
		memcpy ( m_pktInfo.pData, pMsg->pData, m_pktInfo.Header.Length);
	}
}
