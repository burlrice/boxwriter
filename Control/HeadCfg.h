// HeadCfg.h: interface for the CHeadCfg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HEADCFG_H__E75776A8_72E6_4792_89E4_465BF4C02435__INCLUDED_)
#define AFX_HEADCFG_H__E75776A8_72E6_4792_89E4_465BF4C02435__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Database.h"
#include "KeyParse.h"
#include "Extern.h"

#ifdef __WINPRINTER__
	#include "..\MphcApi\Master.h"
	#include "..\UsbMphc\TypeDefs.h"
#endif

namespace Head
{
	class CHead;
	class CHeadInfo;
};

class CHeadCfg : public CKeyParse
{

protected:

public:
	bool IsDisabled (void );

	#ifdef __WINPRINTER__
	void GetAMSValues (tagAMSRegs &AmsRegs);
	void ToRegisters (tagSetupRegs & tConfig, tagFireVibPulse &tPulseRegs, const Head::CHeadInfo & info);
	void ToRegisters (USB::SetupRegs &tConfig, struct USB::FireVibPulse &tPulseRegs, const Head::CHeadInfo & info);
	#endif

	ULONG GetLineID ( void );
	int GetNFactor ( void );
	FoxjetDatabase::HEADSTRUCT m_HeadSettings;
	FoxjetDatabase::PANELSTRUCT m_BoxPanel;
	FoxjetDatabase::LINESTRUCT m_Line;
//	FoxjetDatabase::SETTINGSSTRUCT m_BarCodeSettings;
	CString m_BarCodeSettings;
	FoxjetDatabase::SETTINGSSTRUCT m_TimeCodeSettings;
	FoxjetDatabase::SETTINGSSTRUCT m_DateCodeSettings;
	double slantAngle;
	bool IsMaster() const;
	void ToString (int Section, CString &sBuffer, const Head::CHead * pHead);
	bool LoadSettings (	FoxjetDatabase::COdbcDatabase &Database, CString sUid);
	bool LoadSettings( FoxjetDatabase::COdbcDatabase & Database, ULONG lHeadID);
	bool FromString (CString sConfig);
	const FoxjetDatabase::HEADSTRUCT & GetHeadSettings () const;
//	void Set (FoxjetDatabase::HEADSTRUCT cfg);
	int CalculateNFactor(double angle, double res);
	CHeadCfg();
	virtual ~CHeadCfg();

	double GetPrintRes () const;
};

#endif // !defined(AFX_HEADCFG_H__E75776A8_72E6_4792_89E4_465BF4C02435__INCLUDED_)
