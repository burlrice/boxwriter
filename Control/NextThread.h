
#pragma once
#include "serverthread.h"
#include "fj_socket.h"
#include "fj_defines.h"
#include "ProdLine.h"
#include "Head.h"
#include <vector>

namespace NEXT
{
	typedef enum 
	{
		NOTUSED = 0,
		VARDATA,
		DYNMEM,
		SCANNER_LABEL,
		INFO,
	} PORT_TYPE;

	typedef struct tagPORTSETTINGS
	{
		tagPORTSETTINGS () : m_nPort (0), m_lBaud (9600), m_type (VARDATA) { } 

		UINT m_nPort;
		ULONG m_lBaud;
		PORT_TYPE m_type;
	} PORTSETTINGS;

	typedef struct tagBCPARAMS
	{
		tagBCPARAMS (ULONG lLineID, FoxjetDatabase::HEADTYPE type, const CString & str = _T ("")) : m_lLineID (lLineID), m_type (type), m_str (str) { }

		ULONG m_lLineID;
		FoxjetDatabase::HEADTYPE m_type;
		CString m_str;
	} BCPARAMS;

	CString fj_socketCommandString (LONG lCmd);

	#define DECLARE_FJ_GETSET(s) \
		CString Get##s (); \
		void Set##s (const CString & str); 

	class CNextThread : public CWinThread //CServerThread
	{
		DECLARE_DYNCREATE(CNextThread)
	public:
		CNextThread(void);
		virtual ~CNextThread(void);

		virtual UINT GetPort () const { return _FJ_SOCKET_NUMBER; }
		virtual UINT GetSockType () const { return SOCK_STREAM; }
		virtual void ProcessData ();
		static CString GetLocalAddress (SOCKET socket, bool bPort = true);

		virtual BOOL InitInstance ();
		virtual int Run (); 

		static int sizeofBITMAPFILEHEADER (BITMAPFILEHEADER * p);
		static int sizeofBITMAPINFOHEADER (BITMAPINFOHEADER * p);
		static void ApplySw0923CommFix (bool bInit = false);
		static const CString m_strRegSection;
		static BOOL fj_IsPrintDriverCmd (ULONG lCmd);
		static CString GetIpcCmdName (DWORD dw);
		static DWORD GetIpcCmd (const CString & str);

		CControlView * m_pView;

		DECLARE_FJ_GETSET (DIRECTION);           
		DECLARE_FJ_GETSET (SLANTANGLE);          
		DECLARE_FJ_GETSET (SLANTVALUE);          
		DECLARE_FJ_GETSET (HEIGHT);              
		DECLARE_FJ_GETSET (DISTANCE);           
		DECLARE_FJ_GETSET (STANDBY_AUTO);      
		DECLARE_FJ_GETSET (PHOTOCELL_BACK);      
		DECLARE_FJ_GETSET (SCANNER_USE_ONCE);    
		DECLARE_FJ_GETSET (SERIAL_PORT);         
		DECLARE_FJ_GETSET (INFO_STATUS);         
		DECLARE_FJ_GETSET (INFO_SOCKET);         
		DECLARE_FJ_GETSET (PHOTOCELL_INTERNAL);  
		DECLARE_FJ_GETSET (APS_MODE);          
		DECLARE_FJ_GETSET (BAUD_RATE);
		DECLARE_FJ_GETSET (OP_AUTOPRINT);    
		DECLARE_FJ_GETSET (IDLE_AFTER_PRINT);
		DECLARE_FJ_GETSET (SAVE_DYN_DATA);
		DECLARE_FJ_GETSET (SAVE_SERIAL_DATA);

		DECLARE_FJ_GETSET (UNITS);
		DECLARE_FJ_GETSET (TIME);
		DECLARE_FJ_GETSET (ENCODER);
		DECLARE_FJ_GETSET (ENCODER_COUNT);
		DECLARE_FJ_GETSET (ENCODER_WHEEL);
		DECLARE_FJ_GETSET (ENCODER_DIVISOR);
		DECLARE_FJ_GETSET (CONVEYOR_SPEED);
		DECLARE_FJ_GETSET (PRINT_RESOLUTION);
		DECLARE_FJ_GETSET (INK_SIZE);
		DECLARE_FJ_GETSET (INK_COST);
		DECLARE_FJ_GETSET (SYSTEM_TIME);
		DECLARE_FJ_GETSET (HeadType);

		DECLARE_FJ_GETSET (INTERNAL_CLOCK);     
		DECLARE_FJ_GETSET (TIME_ZONE);          
		DECLARE_FJ_GETSET (OBSERVE_USA_DST);    
		DECLARE_FJ_GETSET (FIRST_DAY);          
		DECLARE_FJ_GETSET (DAYS_WEEK1);         
		DECLARE_FJ_GETSET (WEEK53);             
		DECLARE_FJ_GETSET (ROLLOVER_HOURS);     
		DECLARE_FJ_GETSET (SHIFT1);             
		DECLARE_FJ_GETSET (SHIFT2);             
		DECLARE_FJ_GETSET (SHIFT3);             
		DECLARE_FJ_GETSET (SHIFTCODE1);         
		DECLARE_FJ_GETSET (SHIFTCODE2);         
		DECLARE_FJ_GETSET (SHIFTCODE3);
		DECLARE_FJ_GETSET (DAYS);               
		DECLARE_FJ_GETSET (MONTHS);             
		DECLARE_FJ_GETSET (DAYS_SPECIAL);       
		DECLARE_FJ_GETSET (MONTHS_SPECIAL);     
		DECLARE_FJ_GETSET (HOURS);              
		DECLARE_FJ_GETSET (AMPM); 

		DECLARE_FJ_GETSET (BC_N);
		DECLARE_FJ_GETSET (BC_NAME);
		DECLARE_FJ_GETSET (BC_TY);
		DECLARE_FJ_GETSET (BC_H);
		DECLARE_FJ_GETSET (BC_CD);
		DECLARE_FJ_GETSET (BC_HB);
		DECLARE_FJ_GETSET (BC_VB);
		DECLARE_FJ_GETSET (BC_QZ);
		DECLARE_FJ_GETSET (BC_BASE);
		DECLARE_FJ_GETSET (BC_B1);
		DECLARE_FJ_GETSET (BC_B2);
		DECLARE_FJ_GETSET (BC_B3);
		DECLARE_FJ_GETSET (BC_B4);

		DECLARE_FJ_GETSET (DM_N);
		DECLARE_FJ_GETSET (DM_NAME);
		DECLARE_FJ_GETSET (DM_H);
		DECLARE_FJ_GETSET (DM_HB);
		DECLARE_FJ_GETSET (DM_W);
		DECLARE_FJ_GETSET (DM_WB);

		DECLARE_FJ_GETSET (STATUS_MODE);

		DECLARE_FJ_GETSET (PRINT_MODE);             
		DECLARE_FJ_GETSET (STROBE_MODE_READY_G);		
		DECLARE_FJ_GETSET (STROBE_MODE_READY_Y);		
		DECLARE_FJ_GETSET (STROBE_MODE_READY_R);		
		DECLARE_FJ_GETSET (STROBE_MODE_APS_G);		
		DECLARE_FJ_GETSET (STROBE_MODE_APS_Y);		
		DECLARE_FJ_GETSET (STROBE_MODE_APS_R);		
		DECLARE_FJ_GETSET (STROBE_MODE_LOWTEMP_G);	
		DECLARE_FJ_GETSET (STROBE_MODE_LOWTEMP_Y);	
		DECLARE_FJ_GETSET (STROBE_MODE_LOWTEMP_R);	
		DECLARE_FJ_GETSET (STROBE_MODE_HV_G);			
		DECLARE_FJ_GETSET (STROBE_MODE_HV_Y);			
		DECLARE_FJ_GETSET (STROBE_MODE_HV_R);			
		DECLARE_FJ_GETSET (STROBE_MODE_LOWINK_G);		
		DECLARE_FJ_GETSET (STROBE_MODE_LOWINK_Y);		
		DECLARE_FJ_GETSET (STROBE_MODE_LOWINK_R);		
		DECLARE_FJ_GETSET (STROBE_MODE_OUTOFINK_G);
		DECLARE_FJ_GETSET (STROBE_MODE_OUTOFINK_Y);
		DECLARE_FJ_GETSET (STROBE_MODE_OUTOFINK_R);
		DECLARE_FJ_GETSET (STROBE_MODE_DRIVER_G);		
		DECLARE_FJ_GETSET (STROBE_MODE_DRIVER_Y);		
		DECLARE_FJ_GETSET (STROBE_MODE_DRIVER_R);		
		DECLARE_FJ_GETSET (PRINT_DRIVER);		
		DECLARE_FJ_GETSET (PRINT_DRIVER_COUNT);		

	protected:
		class CSocketHandler
		{
		public:
			CSocketHandler () : m_sock (INVALID_SOCKET), m_hThread (NULL), m_pParentThread (NULL), m_tmLastStatus (CTime::GetCurrentTime ()) { }

			CString ToString () const { return GetLocalAddress (m_sock); }
			operator SOCKET () const { return m_sock; }
			operator SOCKET & () { return m_sock; }

			SOCKET			m_sock;
			HANDLE			m_hThread;
			CString			m_strAddrFrom;
			CNextThread *	m_pParentThread;
			CTime			m_tmLastStatus;
		};

		static ULONG CALLBACK SocketHandlerFct (LPVOID lpData);
		static CString GetTrace (_FJ_SOCKET_MESSAGE * pmsg, ULONG lLength, LPCTSTR lpszFile, ULONG lLine);

		virtual void SetSocketOptions (SOCKET s);
		virtual void Close (SOCKET & sock);
		virtual bool Open ();

		CProdLine * CNextThread::GetLine (int nLine = 0);
		Head::CHeadInfo CNextThread::GetHead (int nLine = 0);

		bool Send (CNextThread::CSocketHandler & s, _FJ_SOCKET_MESSAGE * pmsg, ULONG lLength, LPCTSTR lpszFile, ULONG lLine);
		ULONG Process_FJ_SOCKET_MESSAGE (CNextThread::CSocketHandler & s, _FJ_SOCKET_MESSAGE * psmsg);
		void ProcessCmd (CNextThread::CSocketHandler & s, _FJ_SOCKET_MESSAGE & msg);
		void SaveParams (bool bPostChange = true);

		void fj_print_head_SetStatus ();
		void fj_print_head_SendInfoAll (CNextThread::CSocketHandler & s);
		void fj_print_head_SendSelectedCNT (CNextThread::CSocketHandler & s);
		void fj_SystemToString (char * psz);
		void fj_SystemFromString (char * psz);
		void fj_PrintHeadToString (char * psz);
		void fj_PrintHeadFromString (char * psz);
		void fj_PrintModeToString (char * psz);
		void fj_PrintModeFromString (char * psz);
		void fj_SystemTimeToString (char * psz);
		void fj_SystemDateStringsToString (char * psz);
		void fj_SystemBarCodesToString (char * psz);
		void fj_SystemDataMatrixToString (char * psz);
		void fj_StatusModeToString (char * psz);

		std::vector <std::wstring> fj_getLabelIDs ();
		std::vector <std::wstring> fj_getBmpIDs ();
		std::vector <std::wstring> CNextThread::fj_getFontIDs ();
		std::vector <std::wstring> CNextThread::GetFileTitles (const CString & strPath, const CString & strExt);
		void Diagnostic (const CString & strDebug, LPCTSTR lpszFile, ULONG lLine);

		ULONG GetBoxID (const CString & strName, const CSize & size);

		afx_msg void OnKillThread (WPARAM wParam, LPARAM lParam);
		afx_msg void OnSystemParamsChanged (WPARAM wParam, LPARAM lParam);
		afx_msg void OnTaskStart (WPARAM wParam, LPARAM lParam);
		afx_msg void OnSendStatus (WPARAM wParam, LPARAM lParam);

	protected:
		class CSQL
		{
		public:
			CSQL (const CString & str = _T (""), const CString & strFile = _T (""), ULONG lLine = 0) 
				: m_str (str), m_strFile (strFile), m_lLine (lLine) { }

			operator LPCTSTR () const { return m_str; }

			CString	m_str;
			CString	m_strFile;
			ULONG	m_lLine;
		};

		CArray <CSQL, CSQL &> m_vSQL;

		static int Find (CArray <CSQL, CSQL &> & v, const CString & strSQL);
		CString GetStrobe (const CString & strNEXTState);
		void LoadSettings ();
		void WriteCache (ULONG lCmd, LPSTR pData);

		class CEditLock 
		{
		public:
			CEditLock () : m_pView (NULL), m_tm (CTime::GetCurrentTime ()) { }

			bool IsLocked () const { return m_strAddress.GetLength () ? true : false; }
			bool IsFree () const { return !IsLocked (); }
			bool Lock (const CString & str, CControlView * pView);
			bool Free ();

			CString GetAddress () const { return m_strAddress; }
			CTime GetTime () const { return m_tm; }

		protected:
			CString m_strAddress;
			CTime m_tm;
			CControlView * m_pView;
		} m_editSession;

		CString m_strHeadStatus;
		CSocketHandler m_sockClients [FJSYS_NUMBER_SOCKETS];
		CSocketHandler m_sockTCP, m_sockUDP;
		HANDLE m_hExit;
		CArray <FoxjetCommon::CStrobeItem, FoxjetCommon::CStrobeItem &> m_vStrobe;
		bool m_bSendStatusNow;

		DECLARE_MESSAGE_MAP()
	};
};
