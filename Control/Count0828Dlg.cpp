// Count0828Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "control.h"
#include "Count0828Dlg.h"
#include "CountElement.h"


#if __CUSTOM__ == __SW0828__

using namespace FoxjetElements;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCount0828Dlg dialog


CCount0828Dlg::CCount0828Dlg(CProdLine & line, CWnd* pParent /*=NULL*/)
:	m_line (line),
	FoxjetCommon::CEliteDlg(IDD_COUNT_0828_MATRIX /* IsMatrix () ? IDD_COUNT_0828_MATRIX : IDD_COUNT_0828 */ , pParent)
{
	//{{AFX_DATA_INIT(CCount0828Dlg)
	m_lBoxCurrent = 0;
	m_lBoxIncrement = 0;
	m_lBoxRollover = 0;
	m_lBoxStart = 0;
	m_lPalletCurrent = 0;
	m_lPalletIncrement = 0;
	m_lPalletRollover = 0;
	m_lPalletStart = 0;
	//}}AFX_DATA_INIT
}


void CCount0828Dlg::DoDataExchange(CDataExchange* pDX)
{
	const DWORD dwMin = CCountElement::m_lmtStart.m_dwMin;
	const DWORD dwMax = CCountElement::m_lmtStart.m_dwMax;

	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCount0828Dlg)
	//}}AFX_DATA_MAP

	// box count
	DDX_Text(pDX, TXT_BOXSTART, m_lBoxStart);					// min
	DDV_MinMaxDWord(pDX, m_lBoxStart, dwMin, dwMax - 1);

	DDX_Text(pDX, TXT_BOXROLLOVER, m_lBoxRollover);				// max
	DDV_MinMaxDWord(pDX, m_lBoxRollover, m_lBoxStart + 1, dwMax);

	DDX_Text(pDX, TXT_BOXINCREMENT, m_lBoxIncrement);			// inc
	DDV_MinMaxDWord(pDX, m_lBoxIncrement, dwMin + 1, dwMax);

	DDX_Text(pDX, TXT_BOXCURRENT, m_lBoxCurrent);				// start
	DDV_MinMaxDWord(pDX, m_lBoxCurrent, m_lBoxStart, m_lBoxRollover);
	
	// pallet count
	DDX_Text(pDX, TXT_PALLETSTART, m_lPalletStart);				// min
	DDV_MinMaxDWord(pDX, m_lPalletStart, dwMin, dwMax - 1);

	DDX_Text(pDX, TXT_PALLETROLLOVER, m_lPalletRollover);		// max
	DDV_MinMaxDWord(pDX, m_lPalletRollover, m_lPalletStart + 1, dwMax);

	DDX_Text(pDX, TXT_PALLETINCREMENT, m_lPalletIncrement);		// inc
	DDV_MinMaxDWord(pDX, m_lPalletIncrement, dwMin + 1, dwMax);

	DDX_Text(pDX, TXT_PALLETCURRENT, m_lPalletCurrent);			// start
	DDV_MinMaxDWord(pDX, m_lPalletCurrent, m_lPalletStart, m_lPalletRollover);

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_RESET);
}


BEGIN_MESSAGE_MAP(CCount0828Dlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CCount0828Dlg)
	ON_BN_CLICKED(BTN_RESET, OnReset)
	ON_EN_CHANGE(TXT_BOXCURRENT, OnChangeBoxcurrent)
	ON_EN_CHANGE(TXT_BOXROLLOVER, OnChangeBoxrollover)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCount0828Dlg message handlers

void CCount0828Dlg::OnReset() 
{
	BeginWaitCursor ();
	
	m_line.IdleTask (false);
	m_line.SetTotalBoxCount (0);
	m_line.ResumeTask (false);

	EndWaitCursor ();
}

void CCount0828Dlg::OnChangeBoxcurrent() 
{
	SetDlgItemInt (TXT_PALLETCURRENTBOXCOUNT, GetDlgItemInt (TXT_BOXCURRENT));
}

void CCount0828Dlg::OnChangeBoxrollover() 
{
	SetDlgItemInt (TXT_PALLETUNITS, GetDlgItemInt (TXT_BOXROLLOVER));
}

BOOL CCount0828Dlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	SetTimer (0, 500, NULL);
	OnChangeBoxcurrent ();
	OnChangeBoxrollover (); 
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCount0828Dlg::OnTimer(UINT nIDEvent) 
{
	SetDlgItemInt (TXT_TOTALBOXCOUNT, m_line.GetTotalBoxCount ());
	SetDlgItemInt (TXT_LIFETIMEBOXCOUNT, m_line.GetLifetimeCount ());
}


#endif //__CUSTOM__ == __SW0828__
