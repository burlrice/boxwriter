#pragma once

#include "resource.h"
#include "RoundButtons.h"
#include "maskededit_src\OXMaskedEdit.h"
#include "DbTypeDefs.h"

// CInkSerialNumberDlg dialog

class CInkSerialNumberDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CInkSerialNumberDlg)

public:
	CInkSerialNumberDlg(CWnd* pParent = NULL, ULONG lHeadID = -1);   // standard constructor
	virtual ~CInkSerialNumberDlg();


	static CString Trace (FoxjetDatabase::INK_CODE_WARNGING_LEVEL level);
	static CString Trace (int nLevel) { return Trace ((FoxjetDatabase::INK_CODE_WARNGING_LEVEL)nLevel); }

// Dialog Data
	enum { IDD = IDD_INK_SERIAL_NUMBER };


protected:
	void SetHead (ULONG lHeadID, int nMode);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	FoxjetUtils::CInkCodeEdit m_txt [6];
	CMap <ULONG, ULONG, int, int> m_mapHeads;

	DECLARE_MESSAGE_MAP()

	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();

	FoxjetUtils::CRoundButtons m_buttons;
	CWnd * m_pParent;
	ULONG m_lHeadID;
	int m_nErroneousCodes;
	CTime * m_ptmLockout;
	CMap <UINT, UINT, BOOL, BOOL> m_vLastUnlockedState;
	CStringArray m_vHeadErrors;


	static const DWORD m_dwLockoutTimeSeconds;

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnSerialData (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetInkCodeState (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetHeadWarnings (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnInkCodeLockout (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGetMaxWarningLevel (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetHead (WPARAM wParam, LPARAM lParam);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnEnChangeHead (UINT nID);

	void StartThreads (DWORD dwWait = 1000);
	void KillThreads ();

	CFont m_fnt, m_fntInkCode;
	HANDLE m_hExit;
	HBITMAP m_hBmpOK;
	HBITMAP m_hBmpCancel;
	CBitmap m_bmpTrueCode;

	CArray <HANDLE, HANDLE> m_vThreads;
	std::vector <std::wstring> m_vRegistry;
};
