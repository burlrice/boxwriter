// NewDeviceDlg.cpp : implementation file
//

#include "stdafx.h"
#include <Shobjidl.h>
#include "Control.h"
#include "NewDeviceDlg.h"
#include "afxdialogex.h"
#include "Parse.h"
#include "BackupDlg.h"
#include "MainFrm.h"
#include <Dbt.h>
#include <Portabledevice.h>
#include <PortableDeviceTypes.h>
#include "ImportFileDlg.h"
#include "ProgressDlg.h"

const int WM_QUERY_CANCEL_AUTPLAY = ::RegisterWindowMessage (_T ("QueryCancelAutoPlay"));

using namespace FoxjetDatabase;
using namespace FoxjetCommon;

//////////////////////////////////////////////////////////////////////////////////////////

class CQueryCancelAutoplay : public IQueryCancelAutoPlay
{
public:
	STDMETHODIMP QueryInterface(REFIID riid, void** ppv);
	STDMETHODIMP_(ULONG) AddRef();
	STDMETHODIMP_(ULONG) Release();

	STDMETHODIMP AllowAutoPlay(LPCWSTR pszPath, DWORD dwContentType, LPCWSTR pszLabel, DWORD dwSerialNumber);

	CString m_strDevice;

public:
	CQueryCancelAutoplay() : m_cRef(1){}
	~CQueryCancelAutoplay(){}
private:
	ULONG               m_cRef;
};


static HRESULT GetCancelAutoPlayMoniker(IMoniker** ppmoniker)
{
 // Create the moniker that we'll put in the ROT 
 return CreateClassMoniker(CLSID_QueryCancelAutoPlay, ppmoniker);
}

HRESULT _RegisterForIQueryCancelAutoplay (DWORD* pdwRegisterROT, const CString & strDevice)
{
 IMoniker* pmoniker =NULL;

 HRESULT hr = GetCancelAutoPlayMoniker( &pmoniker);

 if (SUCCEEDED(hr))
 {
  IRunningObjectTable* prot = NULL;
  hr = GetRunningObjectTable(0, &prot);

  if (SUCCEEDED(hr))
  {
   CQueryCancelAutoplay* pQCA = new CQueryCancelAutoplay();

   if ( NULL != pQCA )
   {
     IUnknown* punk=NULL;

    pQCA->m_strDevice = strDevice;
    hr = pQCA->QueryInterface(IID_IUnknown, (void**)&punk);

     if (SUCCEEDED(hr))
     {
      // Register...
      hr = prot->Register(ROTFLAGS_REGISTRATIONKEEPSALIVE,
       punk, pmoniker, pdwRegisterROT);

      punk->Release();
     }
    pQCA->Release();    
   }
   else
   {
    hr = E_OUTOFMEMORY;
   }
   prot->Release();
  }
  pmoniker->Release();
 }
 return hr;
}

HRESULT _UnregisterForIQueryCancelAutoplay(DWORD dwRegisterROT)
{
 IRunningObjectTable *prot;
 if (SUCCEEDED(GetRunningObjectTable(0, &prot)))
 {
  // Remove our instance from the ROT
  prot->Revoke(dwRegisterROT);
  prot->Release();
 }
 return S_OK;
}


STDMETHODIMP CQueryCancelAutoplay::QueryInterface(REFIID riid, void** ppv)
{
 IUnknown* punk = NULL;
 HRESULT hr = S_OK;

 if (IID_IUnknown == riid)
 {
  punk = static_cast<IUnknown*>(this);
  punk->AddRef();
 }
 else
 {
  if (IID_IQueryCancelAutoPlay == riid)
  {
   punk = static_cast<IQueryCancelAutoPlay*>(this);
   punk->AddRef();
  }
  else
  {
   hr = E_NOINTERFACE;
  }
 }

 *ppv = punk;

 return hr;
}

STDMETHODIMP_(ULONG) CQueryCancelAutoplay::AddRef()
{
 return ::InterlockedIncrement((LONG*)&m_cRef);
}

STDMETHODIMP_(ULONG) CQueryCancelAutoplay::Release()
{
 ULONG cRef = ::InterlockedDecrement((LONG*)&m_cRef);

 if (!cRef)
 {
  delete this;
 }

 return cRef;
}

STDMETHODIMP CQueryCancelAutoplay::AllowAutoPlay(LPCWSTR pszPath, DWORD dwContentType, LPCWSTR pszLabel, DWORD dwSerialNumber)
{
	HRESULT hr = S_OK;
	CString strPath (pszPath);

	return (strPath.Find (m_strDevice) == -1) ? S_OK : S_FALSE;

	//return S_FALSE to cancel the autoplay
	//return S_OK to do autoplay
	return hr;
}

//////////////////////////////////////////////////////////////////////////////////////////



CNewDeviceDlg::CImportItem::CImportItem (const CString & strFile)
:	m_strFile (strFile)
{
	CStringArray v;
	CString str (strFile);

	str.Replace (_T ("\\"), _T ("/"));
	Tokenize (str , v, '/');

	if (v.GetSize () > 2) {
		m_strLine = v [v.GetSize () - 2];
		m_strName = v [v.GetSize () - 1];
	}
	else if (v.GetSize () == 1) 
		m_strName = v [v.GetSize () - 1];
	else
		m_strName = strFile;

	int nIndex = FindNoCase (m_strName, _T (".bwmsg"));

	if (nIndex != -1)
		m_strName = m_strName.Left (nIndex);
}

CString CNewDeviceDlg::CImportItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:	return m_strLine;
	case 1: return m_strName;
	}

	return _T ("");
}

// CNewDeviceDlg dialog

IMPLEMENT_DYNAMIC(CNewDeviceDlg, FoxjetCommon::CEliteDlg)

CNewDeviceDlg::CNewDeviceDlg(const CString & strDevice, const CString & strUID, CWnd* pParent /*=NULL*/)
:	m_strDevice (strDevice),
	m_strUID (strUID),
	m_pdlgBackup (NULL),
	m_dwRot (0),
	FoxjetCommon::CEliteDlg(CNewDeviceDlg::IDD, pParent)
{
	_RegisterForIQueryCancelAutoplay (&m_dwRot, strDevice);
}

CNewDeviceDlg::~CNewDeviceDlg()
{
	HRESULT hr = _UnregisterForIQueryCancelAutoplay (m_dwRot);

	if (SUCCEEDED (hr))
		m_dwRot = 0;
}

void CNewDeviceDlg::DoDataExchange(CDataExchange* pDX)
{
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_RESTORE,	IDB_RESTORE);
	m_buttons.DoDataExchange (pDX, BTN_BACKUP,	IDB_BACKUP);
	m_buttons.DoDataExchange (pDX, BTN_INSTALL,	IDB_INSTALL);
	m_buttons.DoDataExchange (pDX, BTN_IMPORT,	IDB_IMPORT);

	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CNewDeviceDlg, FoxjetCommon::CEliteDlg)
	ON_BN_CLICKED(BTN_BACKUP, &CNewDeviceDlg::OnBnClickedBackup)
	ON_BN_CLICKED(BTN_RESTORE, &CNewDeviceDlg::OnBnClickedRestore)
	ON_BN_CLICKED(BTN_INSTALL, &CNewDeviceDlg::OnBnClickedInstall)
	ON_BN_CLICKED(BTN_IMPORT, OnBnClickedImport)
	ON_BN_CLICKED(CHK_IMPORT_ALL, OnAll)
	ON_NOTIFY(LVN_ITEMCHANGED, LV_IMPORT, OnItemchangedImport)
	ON_REGISTERED_MESSAGE (WM_QUERY_CANCEL_AUTPLAY, OnQueryCancelAutoplay)
END_MESSAGE_MAP()


// CNewDeviceDlg message handlers


BOOL CNewDeviceDlg::OnInitDialog()
{
	using namespace FoxjetCommon;
	using namespace ItiLibrary::ListCtrlImp;

	CArray <CColumn, CColumn> vCols;
	DWORD dw = ::GetFileAttributes (m_strDevice);
	bool bInstall = false;

	CEliteDlg::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_LINE), 120));
	vCols.Add (CColumn (LoadString (IDS_NAME), 140));
	m_lv.Create (LV_IMPORT, vCols, this, ListGlobals::defElements.m_strRegSection);
	m_lv->SetExtendedStyle (LVS_EX_CHECKBOXES | m_lv->GetExtendedStyle ());

	SetDlgItemText (LBL_DEVICE, m_strDevice);

	if ((dw & FILE_ATTRIBUTE_DIRECTORY) && dw != -1) {
		CStringArray v;

		GetFiles (m_strDevice, _T ("*.exe"), v);

		if (CListBox * p = (CListBox *)GetDlgItem (LB_INSTALL)) {
			CVersion verMax = CVersion (0, 0);
			CString strSelect;

			p->ResetContent ();
			p->SetItemHeight (0, 32);

			for (int i = 0; i < v.GetSize (); i++) {
				CString strNetworked = Extract (v [i], _T ("BwSetupNetworked_"), _T (".exe"));
				CString strStd = Extract (v [i], _T ("BwSetup_"), _T (".exe"));
				CString strVer = strNetworked.GetLength () ? strNetworked : strStd;

				if (strVer.GetLength ()) {
					VERSIONSTRUCT ver = { 0 };

					if (_stscanf (strVer, _T ("%d.%d.%d.%d"), &ver.m_nMajor, &ver.m_nMinor, &ver.m_nCustomMajor, &ver.m_nInternal) != 4)
						_stscanf (strVer, _T ("%d.%d"), &ver.m_nMajor, &ver.m_nMinor);

					TRACEF (CVersion (ver).ToString ());

					if (strNetworked.GetLength ()) {
						CString str = _T ("Networked ") + strNetworked;
						int nIndex = p->AddString (str);

						m_map.SetAt (str, v [i]);

						if (verMax < ver) {
							verMax = ver;
							strSelect = str;
						}
					}
					else if (strStd.GetLength ()) {
						CString str = strStd;
						int nIndex = p->AddString (str);

						m_map.SetAt (str, v [i]);

						if (verMax < ver) {
							verMax = ver;
							strSelect = str;
						}
					}
				}
			}

			p->SetCurSel (p->FindString(-1, strSelect));
			bInstall = p->GetCount () > 0 ? true : false;
		}
	}

	if (CWnd * pwnd = GetDlgItem (BTN_INSTALL))
		pwnd->ShowWindow (!bInstall ? SW_HIDE : SW_SHOW);
	if (CWnd * pwnd = GetDlgItem (LB_INSTALL))
		pwnd->ShowWindow (!bInstall ? SW_HIDE : SW_SHOW);

	if (!theApp.LoginPermits (IDM_IMPORT)) {
		UINT n [] = { BTN_RESTORE, BTN_IMPORT, LV_IMPORT, CHK_IMPORT_ALL };

		if (!m_lv->GetItemCount ()) {
			for (int i = 0; i < ARRAYSIZE (n); i++)
				if (CWnd * pwnd = GetDlgItem (n [i]))
					pwnd->EnableWindow (FALSE);
		}
	}
	else {
		DWORD dw = 0;

		HANDLE h = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)GetFilesFunc, this, 0, &dw);
	}

	if (!theApp.LoginPermits (IDM_EXPORT)) {
		if (CWnd * pwnd = GetDlgItem (BTN_BACKUP))
			pwnd->EnableWindow (FALSE);
	}

	if (!theApp.LoginPermits (ID_APP_EXIT)) {
	//if (!theApp.IsAdmin ()) {
		if (CWnd * pwnd = GetDlgItem (BTN_INSTALL))
			pwnd->EnableWindow (FALSE);
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CNewDeviceDlg::OnBnClickedBackup()
{
	CBackupDlg dlg (this);
	DWORD dw = ::GetFileAttributes (m_strDevice);
	m_pdlgBackup = &dlg;

	if ((dw & FILE_ATTRIBUTE_DIRECTORY) && dw != -1) {
		for (int j = 0; j < 256; j++) {
			dlg.m_strPath.Format (_T ("%s%s_%s%s.zip"), 
				m_strDevice,
				GetComputerName (),
				CTime::GetCurrentTime ().Format (_T ("%m_%d_%Y")),
				j > 0 ? (_T (" (") + ToString (j) + _T (")")) : _T (""));

			if (::GetFileAttributes (dlg.m_strPath) == -1)
				break;
		}

		if (dlg.DoModal () == IDOK) {
			BeginWaitCursor ();
			TRACER (_T ("CopyFile: ") + dlg.m_strTempFile + _T (" --> ") + dlg.m_strPath);
			BOOL bCopy = ::CopyFile (dlg.m_strTempFile, dlg.m_strPath, FALSE);
			TRACER (_T ("CopyFile: ") + ToString ((int)bCopy));
			TRACER (FORMATMESSAGE (::GetLastError ()));

			EndWaitCursor ();
									
			if (!bCopy)
				MsgBox (FORMATMESSAGE (::GetLastError ()), MB_OK | MB_ICONINFORMATION);
			else {
				CString str;

				TRACER (_T ("DeleteFile: ") + dlg.m_strTempFile);
				::DeleteFile (dlg.m_strTempFile);
				str.Format (LoadString (IDS_BACKUPFILECOPIEDTO), dlg.m_strPath);
				MsgBox (str, MB_OK | MB_ICONINFORMATION);
			}

			EndDialog (IDOK);
		}
	}
	else {
		CComPtr <IPortableDevice> device = CBackupDlg::OpenDevice (m_strUID);

		if (device) {
			CComPtr<IPortableDeviceContent> content;
							
			if (SUCCEEDED (device->Content (&content))) {
				DEVICE_FILE_STRUCT root;

				CBackupDlg::Enumerate (content, WPD_DEVICE_OBJECT_ID, dlg.m_vRoot, 3);
				CBackupDlg::Trace (dlg.m_vRoot);

				FoxjetCommon::CCopyArray <CString, CString &> vFiles = CBackupDlg::ToString (dlg.m_vRoot, false, true); 

				for (int j = 0; j < 256; j++) {
					bool bFound = false;

					dlg.m_strPath.Format (_T ("%s_%s%s"), 
						GetComputerName (),
						CTime::GetCurrentTime ().Format (_T ("%m_%d_%Y")),
						j > 0 ? (_T (" (") + ToString (j) + _T (")")) : _T (""));

					CString strTmp = dlg.m_strPath;
					strTmp.MakeLower ();
					dlg.m_strPath += _T (".zip");

					for (int k = 0; !bFound && (k < vFiles.GetSize ()); k++) {
						CString str = vFiles [k];

						str.MakeLower ();
						bFound = str.Find (strTmp) != -1 ? true : false;
					}

					if (!bFound)
						break;
				}
			}

			if (dlg.DoModal () == IDOK) {
				CString strPath = dlg.m_strPath;
				BOOL bCopy;

				BeginWaitCursor ();
				{
					CString strFrom = dlg.m_strTempFile;
					CString strTo = strPath;

					strFrom.Replace ('\\', '/');
					strTo.Replace ('\\', '/');

					std::vector <std::wstring> vFrom = explode (std::wstring (strFrom), '/');
					std::vector <std::wstring> vTo = explode (std::wstring (strTo), '/');

					if (vFrom.size () && vTo.size ()) {
						CString strTitleFrom = vFrom [vFrom.size () - 1].c_str ();
						CString strTitleTo = vTo [vTo.size () - 1].c_str ();
						CString strTempFile = dlg.m_strTempFile;

						strTempFile.Replace (strTitleFrom, strTitleTo);

						if (::CopyFile (dlg.m_strTempFile, strTempFile, FALSE)) {
							::DeleteFile (dlg.m_strTempFile);
							dlg.m_strTempFile = strTempFile;
						}
					}

					CString strID = CBackupDlg::GetID (dlg.m_vRoot, dlg.m_strTitle);
					bool bOverwrite = true;

					if (strID.GetLength ()) {
						CString str, strTitle = dlg.m_strTitle;
						int nIndex = strTitle.ReverseFind ('/');

						if (nIndex != -1)
							strTitle = strTitle.Mid (nIndex + 1);

						str.Format (LoadString (IDS_OVERWRITEFILE), strTitle);
						bOverwrite = MsgBox (str, LoadString (IDS_CONFIRM), MB_YESNO) == IDYES ? true : false;

						if (bOverwrite) 
							if (!CBackupDlg::Delete (device, strID))
								TRACER (_T ("Delete failed: [") + strID + _T ("] ") + strPath);
					}

					if (bOverwrite)
						bCopy = CBackupDlg::CopyToPortableDevice (device, dlg.m_strTempFile, strPath, dlg.m_vRoot);
				}
				EndWaitCursor ();
									
				if (!bCopy)
					MsgBox (LoadString (IDS_COPYFAILED) + _T ("\n") + FORMATMESSAGE (::GetLastError ()), MB_OK | MB_ICONINFORMATION);
				else {
					CString str;

					str.Format (LoadString (IDS_BACKUPFILECOPIEDTO), dlg.m_strPath);
					MsgBox (str, MB_OK | MB_ICONINFORMATION);
				}

				::DeleteFile (dlg.m_strTempFile);
			}

			EndDialog (IDOK);
		}
	}

	m_pdlgBackup = NULL;
}


void CNewDeviceDlg::OnBnClickedRestore()
{
	CString str;

	str += _T ("/DBQ=\"") + FoxjetDatabase::Extract (theApp.m_Database.GetConnect (), _T ("DBQ="), _T (";")) + _T ("\" ");
	str += _T ("/LOCAL_BACKUP_DIR=\"") + GetHomeDir () + _T ("\\") + FoxjetDatabase::Extract (theApp.m_Database.GetConnect (), _T ("DSN="), _T (";")) + _T ("\" ");
	
	DWORD dw = ::GetFileAttributes (m_strDevice);

	if ((dw & FILE_ATTRIBUTE_DIRECTORY) && dw != -1) {
		if (m_strDevice.GetLength ())
			str += _T ("/SELECT=\"(") + m_strDevice.Left (1) + _T (":)\" ");
	}
	else {
		::CDeviceList v = CMainFrame::EnumDevices (GUID_DEVINTERFACE_WPD);

		for (int i = 0; i < v.GetSize (); i++) {
			CDevice & d = v [i];

			if (!d.m_strDeviceDescription.CompareNoCase (m_strDevice)) {
				str += _T ("/SELECT=\"\\\\") + m_strDevice + _T ("\\\" ");
				break;
			}
		}
	}

	FoxjetDatabase::Restore (this, str);
}


void CNewDeviceDlg::OnBnClickedInstall()
{
	if (CListBox * p = (CListBox *)GetDlgItem (LB_INSTALL)) {
		CString str, strFile;

		p->GetText (p->GetCurSel (), str);
		
		if (m_map.Lookup (str, strFile)) {
			STARTUPINFO si;     
			PROCESS_INFORMATION pi; 
			TCHAR szCmdLine [0xFF];
			bool bMore = true;
			CTime tmStart = CTime::GetCurrentTime ();

			_tcsncpy (szCmdLine, strFile, 0xFF);
			memset (&pi, 0, sizeof(pi));
			memset (&si, 0, sizeof(si));
			si.cb = sizeof(si);     
			si.dwFlags = STARTF_USESHOWWINDOW;     
			si.wShowWindow = SW_SHOW;     

			if (::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
				EndDialog (IDCANCEL);
		}
	}
}


LRESULT CNewDeviceDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
	case WM_DEVICECHANGE:
		OnDeviceChange (wParam, lParam);
		break;
	}

	return CEliteDlg::WindowProc(message, wParam, lParam);
}

LRESULT CNewDeviceDlg::OnDeviceChange (WPARAM wParam, LPARAM lParam)
{
	bool bDismiss = false;

	switch (wParam) {
	case DBT_DEVNODES_CHANGED:
		CString str = Extract (_T ("$") + m_strDevice, _T ("$"), _T (":\\"));

		if (str.GetLength () == 1) {
			DWORD dw = GetLogicalDrives ();
			int i = str [0] - 'A';

			if (!(dw & (1 << i)))
				bDismiss = true;
		}
		else {
			::CDeviceList v = CMainFrame::EnumDevices (GUID_DEVINTERFACE_WPD);
			bool bFound = false;

			for (int i = 0; !bFound && i < v.GetSize (); i++) {
				CDevice & d = v [i];

				if (!d.m_strDeviceDescription.CompareNoCase (m_strDevice))
					bFound = true;
			}

			if (!bFound)
				bDismiss = true;
		}

		break;
	}


	if (bDismiss) {
		if (m_pdlgBackup) {
			m_pdlgBackup->EndDialog (IDCANCEL);
			m_pdlgBackup = NULL;
		}

		EndDialog (IDCANCEL);
	}

	return 0;
}

void CNewDeviceDlg::OnBnClickedImport ()
{

	CLongArray sel = ItiLibrary::GetCheckedItems (m_lv);
	std::vector <std::wstring> vError;

	if (!sel.GetSize ()) {
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
		return;
	}

	//Foxjet3d::CProgressDlg dlg (this);
	//VERIFY (dlg.Create (this));
	//dlg.ShowWindow (SW_SHOW);
	//dlg.BringWindowToTop ();

	BeginWaitCursor ();

	for (int i = 0; i < sel.GetSize (); i++) {
		if (CImportItem * p = (CImportItem *)m_lv.GetCtrlData (sel [i])) {
			Foxjet3d::CImportFileDlg::IMPORTTHREADSTRUCT params;
			bool bError = false;

			TRACEF (p->m_strLine + _T ("\\") + p->m_strName);
			
			params.m_pDlg		= NULL; //&dlg;
			params.m_pDB		= &theApp.m_Database;
			params.m_strFile	= p->m_strFile;
			params.m_dwFlags	= IMPORT_TASKS;

			ULONG lResult = Foxjet3d::CImportFileDlg::Import ((LPVOID)&params);

			for (int i = 0; i < params.m_vResult.GetSize (); i++) {
				//TRACEF (params.m_vResult [i].m_strStruct + _T (": ") + ToString (params.m_vResult [i].m_nParsed) + _T (": ") + ToString (params.m_vResult [i].m_nImported));

				if (!params.m_vResult [i].m_strStruct.CompareNoCase (_T ("TASKSTRUCT"))) {
					if (params.m_vResult [i].m_nImported == 0)
						bError = true;

					break;
				}
			}

			if (lResult != Foxjet3d::CImportFileDlg::IMPORTTHREAD_SUCCESS)
				bError = true;

			if (bError)
				vError.push_back (std::wstring (p->m_strLine + _T ("\\") + p->m_strName));
		}
	}

	EndWaitCursor ();

	if (vError.size ()) {
		CString str;

		str.Format (LoadString (IDS_IMPORTFAILED), CString (_T ("\n")) + implode (vError, std::wstring (_T ("\n"))).c_str ());
		MsgBox (* this, str, MB_ICONERROR);
	}
	else
		EndDialog (IDOK);
}

void CNewDeviceDlg::OnItemchangedImport(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW * pnm = (NM_LISTVIEW*)pNMHDR;

	if (pResult)
		* pResult = 0;
}

void CNewDeviceDlg::OnAll ()
{
	if (CButton * pAll = (CButton *)GetDlgItem (CHK_IMPORT_ALL)) {
		BOOL b = pAll->GetCheck () == 1;

		if (CWnd * p = GetDlgItem (LV_IMPORT))
			p->EnableWindow (!b);

		for (int i = 0; i < m_lv->GetItemCount (); i++) {
			if (CImportItem * p = (CImportItem *)m_lv.GetCtrlData (i)) {
				m_lv->SetCheck (i, b);
			}
		}
	}
}

LRESULT CNewDeviceDlg::OnQueryCancelAutoplay (WPARAM wParam, LPARAM lParam)
{
	TRACER (_T ("CNewDeviceDlg::OnQueryCancelAutoplay"));
	return TRUE;
}

#define CHECK_FOR_EXIT(d) \
	if (!::IsWindowVisible (d.m_hWnd)) \
		return 0;

ULONG CALLBACK CNewDeviceDlg::GetFilesFunc(LPVOID lpData) 
{
	CNewDeviceDlg & dlg = * (CNewDeviceDlg *)lpData;
	CStringArray vFiles;
	CString strDevice (dlg.m_strDevice);

	while (!::IsWindowVisible (dlg.m_hWnd))
		::Sleep (100);

	//GetPrinterLines (theApp.m_Database, GetPrinterID (theApp.m_Database), vLines);

	while (strDevice.GetLength () && strDevice [strDevice.GetLength () - 1] == '\\')
		strDevice.Delete (strDevice.GetLength () - 1);

	CHECK_FOR_EXIT (dlg)

	{
		WIN32_FIND_DATA fd;
		const CString str = strDevice + _T ("\\*.*");
		HANDLE hFind = ::FindFirstFile (str, &fd);
		bool bMore = hFind != INVALID_HANDLE_VALUE;

		while (bMore) {
			CString str = strDevice + _T ("\\") + CString (fd.cFileName);

			CHECK_FOR_EXIT (dlg)
		
			if (::GetFileAttributes (str) & FILE_ATTRIBUTE_DIRECTORY) {
				CStringArray v;
				TRACEF (str);

				GetFiles (str, _T ("*.bwmsg"), v);

				for (int i = 0; i < v.GetSize (); i++)
					InsertAscending (vFiles, v [i]);
			}

			ZeroMemory (&fd, sizeof (fd));
			bMore = ::FindNextFile (hFind, &fd) ? true : false;
		}

		::FindClose (hFind);
	}

	CHECK_FOR_EXIT (dlg)

	for (int i = 0; i < vFiles.GetSize (); i++) {
		CHECK_FOR_EXIT (dlg)
			
		if (vFiles [i].Find (_T (".bwmsg")) != -1) {
			CImportItem * p = new CImportItem (vFiles [i]);

			int nIndex = dlg.m_lv.InsertCtrlData (p);
			dlg.m_lv->SetCheck (nIndex, FALSE);
		}
	}

	for (int i = 0; i < dlg.m_lv->GetItemCount (); i++) {
		CHECK_FOR_EXIT (dlg)

		if (CImportItem * p = (CImportItem *)dlg.m_lv.GetCtrlData (i)) {
			ULONG lTaskID = GetTaskID (theApp.m_Database, p->m_strName, p->m_strLine, GetPrinterID (theApp.m_Database));
			BOOL bCheck = lTaskID == NOTFOUND;

			dlg.m_lv->SetCheck (dlg.m_lv.GetDataIndex (p), bCheck);

			if (bCheck)
				dlg.m_lv->EnsureVisible (dlg.m_lv.GetDataIndex (p), FALSE);
		}
	}

	UINT n [] = { BTN_IMPORT, LV_IMPORT, CHK_IMPORT_ALL };

	CHECK_FOR_EXIT (dlg)

	if (!dlg.m_lv->GetItemCount ()) {
		for (int i = 0; i < ARRAYSIZE (n); i++)
			if (CWnd * pwnd = dlg.GetDlgItem (n [i]))
				pwnd->ShowWindow (SW_HIDE);
	}

	return 0;
}

