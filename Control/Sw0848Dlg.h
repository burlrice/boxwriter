#if !defined(AFX_SW0848DLG_H__FB13D608_11F9_4E2A_A077_5C8E442D7932__INCLUDED_)
#define AFX_SW0848DLG_H__FB13D608_11F9_4E2A_A077_5C8E442D7932__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Sw0848Dlg.h : header file
//

#include "ListCtrlImp.h"
#include "Sw0848FieldsDlg.h"
#include "RoundButtons.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CSw0848Dlg dialog

namespace Sw0848Dlg
{

	class CPair : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CPair () { }
		CPair (const CPair & rhs);
		CPair & operator = (const CPair & rhs);
		virtual ~CPair () { }

		virtual CString GetDispText (int nColumn) const;

		CString m_strInput;
		CString m_strOutput;
	};

	class CSw0848Map
	{
	public:
		CPair m_dsn;
		CPair m_table;

		CArray <CPair, CPair &> m_v;
	};

	class CSw0848Dlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CSw0848Dlg(CWnd* pParent = NULL);   // standard constructor

	public:

		CSw0848Map m_map;

		virtual void OnOK();

	// Dialog Data
		//{{AFX_DATA(CSw0848Dlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA


		static void Load (CSw0848Map & map);
		static void Save (CSw0848Map & map);
		static int FindTable (FoxjetDatabase::COdbcDatabase & db, const CString & strTable);

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CSw0848Dlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:

		FoxjetUtils::CRoundButtons m_buttons;
		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
		void MatchFields ();
		bool IsMapped (const CString & strOutput);
		void Init (CSw0848FieldsDlg & dlg);

		// Generated message map functions
		//{{AFX_MSG(CSw0848Dlg)
		afx_msg void OnInputdsn();
		afx_msg void OnOutputdsn();
		afx_msg void OnAdd();
		afx_msg void OnDelete();
		afx_msg void OnEdit();
		virtual BOOL OnInitDialog();
		afx_msg void OnSelchangeInputtable();
		afx_msg void OnSelchangeOutputtable();
		afx_msg void OnDblclkFields(NMHDR* pNMHDR, LRESULT* pResult);
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; // namespace Sw0848Dlg

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SW0848DLG_H__FB13D608_11F9_4E2A_A077_5C8E442D7932__INCLUDED_)
