#if !defined(AFX_LABELBATCHQTYDLG_H__2E91DFA9_791C_4322_A58A_9F74C5149426__INCLUDED_)
#define AFX_LABELBATCHQTYDLG_H__2E91DFA9_791C_4322_A58A_9F74C5149426__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LabelBatchQtyDlg.h : header file
//
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CLabelBatchQtyDlg dialog

class CLabelBatchQtyDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CLabelBatchQtyDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLabelBatchQtyDlg)
	DWORD	m_dwQuantity;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLabelBatchQtyDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLabelBatchQtyDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LABELBATCHQTYDLG_H__2E91DFA9_791C_4322_A58A_9F74C5149426__INCLUDED_)
