// LicenseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Control.h"
#include "LicenseDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLicenseDlg dialog


CLicenseDlg::CLicenseDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(CLicenseDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLicenseDlg)
	m_sHashPart = _T("");
	m_sSerialNo = _T("");
	m_sVer = _T("");
	//}}AFX_DATA_INIT
}


void CLicenseDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLicenseDlg)
	DDX_Text(pDX, IDC_HASHPART, m_sHashPart);
	DDV_MaxChars(pDX, m_sHashPart, 8);
	DDX_Text(pDX, IDC_SERIALNO, m_sSerialNo);
	DDV_MaxChars(pDX, m_sSerialNo, 8);
	DDX_Text(pDX, IDC_VER, m_sVer);
	DDV_MaxChars(pDX, m_sVer, 4);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLicenseDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CLicenseDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLicenseDlg message handlers

BOOL CLicenseDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();
	TCHAR sVolume[255];
	TCHAR sFileSystem[255];
	DWORD dwMaxFileNameLen;
	DWORD dwFileSysFlags;

	GetVolumeInformation ( "\\", sVolume, 255, &m_dwSerial, &dwMaxFileNameLen, &dwFileSysFlags, sFileSystem, 255);
	m_Hash = (HIWORD(m_dwSerial) + LOWORD(m_dwSerial) + AppVer);
	m_sSerialNo.Format ("%08X", m_dwSerial);
	m_sVer.Format ( "%04X", AppVer);
	UpdateData (false);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLicenseDlg::OnOK() 
{
	UpdateData (true);
	CString sLicKey;
	CString sTemp;
	sLicKey.Format ( "%08X-%04X-%08X", m_dwSerial, AppVer, m_Hash);
	sTemp.Format ( "%08X-%04X-%s", m_dwSerial, AppVer, m_sHashPart);
	if ( sLicKey.CompareNoCase ( sTemp ) != 0)
		MsgBox (LoadString (IDS_INVALIDLICENSEKEY));
	else {
		WriteProfileString ("License", "Key", sTemp);
		FoxjetCommon::CEliteDlg::OnOK();
	}
}
