// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__3855D46B_9729_11D5_8F07_00045A47C31C__INCLUDED_)
#define AFX_MAINFRM_H__3855D46B_9729_11D5_8F07_00045A47C31C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ControlView.h"
#include "ZoomDlg.h"
#include <setupapi.h>
#include <initguid.h>
#include "Parse.h"

// This is the GUID for the USB device class
DEFINE_GUID(GUID_DEVINTERFACE_USB_DEVICE,
			0xA5DCBF10L, 0x6530, 0x11D2, 0x90, 0x1F, 0x00, 0xC0, 0x4F, 0xB9, 0x51, 0xED);
// (A5DCBF10-6530-11D2-901F-00C04FB951ED)


class CDevice
{
public:
	CDevice ();
	CDevice (const CDevice & rhs);
	CDevice (const SP_DEVINFO_DATA * pDev, const SP_DEVICE_INTERFACE_DATA * pInterface, const SP_DEVICE_INTERFACE_DETAIL_DATA_W * pInterfaceDetail);
	CDevice & operator = (const CDevice & rhs);
	virtual ~CDevice ();

	friend bool operator == (const CDevice & lhs, const CDevice & rhs) { return !_tcscmp (lhs.m_interfaceDetail.DevicePath, rhs.m_interfaceDetail.DevicePath); }
	friend bool operator != (const CDevice & lhs, const CDevice & rhs) { return !(lhs == rhs); }

	SP_DEVINFO_DATA						m_dev;
	SP_DEVICE_INTERFACE_DATA			m_interface;
	SP_DEVICE_INTERFACE_DETAIL_DATA_W	m_interfaceDetail;
	TCHAR								m_szDevicePath [MAX_PATH];
	
	CString m_strFriendlyName;
	CString m_strDeviceDescription;
	CString m_strLocationInformation;

	WORD GetVID () const { return _tcstoul (FoxjetDatabase::Extract (m_interfaceDetail.DevicePath, _T ("vid_"), _T ("&")), NULL, 16); } 
	WORD GetPID () const { return _tcstoul (FoxjetDatabase::Extract (m_interfaceDetail.DevicePath, _T ("pid_"), _T ("#")), NULL, 16); } 
};

typedef struct FoxjetCommon::CCopyArray <CDevice, CDevice &> CDeviceList;


class CMainFrame : public CFrameWnd
{
friend class CControlDoc;

	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:
	CControlView *m_pControlView;

	static CString GetMenuPrompt (const FoxjetDatabase::OPTIONSSTRUCT & s, const CString & strDef = _T (""));
	int GetToolbarResID ();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL LoadFrame(UINT nIDResource, DWORD dwDefaultStyle = WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, CWnd* pParentWnd = NULL, CCreateContext* pContext = NULL);
	protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();

	static int Find (const ::CDeviceList & v, const CDevice & find);
	static ::CDeviceList EnumDevices (const GUID & guid = GUID_DEVINTERFACE_USB_DEVICE);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	STARTUPINFO *m_psiStartInfo;
	
	#ifdef __IPPRINTER__
	CToolBar    m_wndToolBar;
	#endif

	static ULONG CALLBACK LoadCacheFunc (LPVOID lpData);
	void LoadCache ();


	HANDLE m_hCacheThread;
	::CDeviceList m_vUSB;
	::CDeviceList m_vPortableDevices;

public:
	bool IsCacheThreadAlive () const { return m_hCacheThread != NULL; }
	bool IsClosing () const { return m_bClosed; }

	#ifdef _DEBUG
	static const CPoint m_ptDebug;
	#endif

// Generated message map functions
protected:
	void OnUpdateUI ();
	
	afx_msg LRESULT OnIsAppRunning (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnShutdown (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSystemParamsChanged (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSyncRemoteHead (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDbStartParamsChanged (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDisplayMsg (WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnResetScannerInfo (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnResetScannerError (WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnGetPrinterWnd (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnOpenMapFile (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnCloseMapFile (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMappedCommand (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEnableEdit (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnLocalDbBackup (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDatabaseConnectionLost (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDebug (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnKbChange (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDeviceChange (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSuspendCommThreads (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDebugEnumRegisteredThreads (WPARAM wParam, LPARAM lParam);

	HANDLE m_hMapFile;
	LPVOID m_lpMapAddress;

	afx_msg void OnConfigureDatabase();
	afx_msg void OnCfgGeneral ();
	afx_msg void OnZoom ();

	static bool CALLBACK ZoomFct (FoxjetCommon::ZOOMSTRUCT & zs);

	afx_msg void OnConfigureSystemOutputtable(); // sw0848

public:

	afx_msg void OnOperEdit();
	afx_msg void OnLogin();
	afx_msg void OnLogout();
	afx_msg void OnOperTest();
	afx_msg void OnImport ();
	afx_msg void OnExport ();


protected:
	static void BringEditorToTop ();
	static ULONG CALLBACK LaunchEditorFunc (LPVOID lpData);

	ULONG GetAdminID ();
	bool GetOptionLink (ULONG lGroupID, ULONG lOptionID, FoxjetDatabase::OPTIONSLINKSTRUCT & link);
	void UpdateOptionsTable (CMenu * pMenu, CArray <FoxjetDatabase::OPTIONSSTRUCT, FoxjetDatabase::OPTIONSSTRUCT &> & v);
	int Find (ULONG lID, CArray <FoxjetDatabase::OPTIONSSTRUCT, FoxjetDatabase::OPTIONSSTRUCT &> & v);
	bool IsMenuItemSecured (ULONG lID);
	bool IsMenuItemExcluded (ULONG lID);

	void UpdateAppExitUI ();


	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnCfgPrinthead();
	afx_msg void OnCfgProductionLines();
	afx_msg void OnCfgUser();
	afx_msg void OnCfgGroupOptions();
	afx_msg void OnOperStart();
	afx_msg void OnOperStop();
	afx_msg void OnOperIdle();
	afx_msg void OnOperResume();
	afx_msg void OnViewScanReport();
	afx_msg void OnChangeUserElements();
	afx_msg void OnCfgBarcodeSettings();
	afx_msg void OnCfgDateCodes();
	afx_msg void OnCfgShiftCodes();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnViewDiag();
	afx_msg void OnViewPrintReport();
	afx_msg void OnChangecounts();
	afx_msg void OnHelpTranslate();
	afx_msg void OnCfgDynamicdata();
	afx_msg void OnViewPreview();
	afx_msg void OnClose();
	afx_msg void OnTimeChange();
	afx_msg void OnConfigureSystemStrobe();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMoving(UINT fwSide, LPRECT pRect);
	afx_msg void OnMove(int x, int y);
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	
	afx_msg void OnCfgBaxter ();

private:
	bool m_bClosed;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__3855D46B_9729_11D5_8F07_00045A47C31C__INCLUDED_)
