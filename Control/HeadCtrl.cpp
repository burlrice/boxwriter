// HeadCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "Sw0848Thread.h"
#include "ProdLine.h"
#include "control.h"
#include "HeadCtrl.h"
#include "Color.h"
#include "Head.h"
#include "ListCtrlImp.h"
#include "Debug.h"
#include "Parse.h"
#include "head.h"
#include "Color.h"

#ifdef __WINPRINTER__
	#include "LocalHead.h"
#endif

using namespace Color;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace ItiLibrary;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////

using namespace HeadCtrl;

CSelection::CSelection ()
:	m_pThread (NULL),
	m_lPanelID (-1)
{
}

CSelection::CSelection (const CSelection & rhs)
:	m_pThread (rhs.m_pThread),
	m_lPanelID (rhs.m_lPanelID)
{
}

CSelection & CSelection::operator = (const CSelection & rhs)
{
	if (this != &rhs) {
		m_pThread = rhs.m_pThread;
		m_lPanelID = rhs.m_lPanelID;
	}

	return * this;
}

CSelection::~CSelection ()
{
}

/////////////////////////////////////////////////////////////////////////////

CHeadItem::CHeadItem (DWORD dwThreadID, ULONG lPanelID)
:	m_dwThreadID (dwThreadID),
	m_pHead (NULL),
	m_bRedraw (true),
	m_lPanelID (lPanelID)
{
	PANELSTRUCT panel;

	if (CControlDoc * pDoc = (CControlDoc *)theApp.GetActiveDocument ()) 
		if (Head::CHead * pHead = pDoc->GetHeadByThreadID (dwThreadID))
			m_info = Head::CHead::GetInfo (pHead);

	if (lPanelID == -1)
		lPanelID = m_info.m_Config.m_HeadSettings.m_lPanelID;

	if (GetPanelRecord (theApp.m_Database, lPanelID, panel)) 
		m_strPanel = panel.m_strName;
}

CHeadItem::CHeadItem (const FoxjetDatabase::HEADSTRUCT & h, ULONG lPanelID)
:	m_dwThreadID (0),
	m_pHead (NULL),
	m_bRedraw (true),
	m_lPanelID (lPanelID)
{
	PANELSTRUCT panel;

	m_pHead = new HEADSTRUCT (h);

	if (lPanelID == -1)
		lPanelID = h.m_lPanelID;

	if (GetPanelRecord (theApp.m_Database, lPanelID, panel)) 
		m_strPanel = panel.m_strName;
}

CHeadItem::~CHeadItem ()
{
	if (m_pHead) {
		delete m_pHead;
		m_pHead = NULL;
	}
}

ULONG CHeadItem::GetHeight () const
{
	return m_info.m_Config.m_HeadSettings.m_lRelativeHeight;
}

ULONG CHeadItem::GetHeadID () const
{
	return m_info.m_Config.m_HeadSettings.m_lID;
}

ULONG CHeadItem::GetPanelID () const
{
	return m_info.m_Config.m_HeadSettings.m_lPanelID;

	return 0;
}

CString CHeadItem::ToString ()  
{
	CString str =
		m_info.GetPanelName ()		+ _T (", ") + 
		m_info.GetFriendlyName ()	+ _T (", ") + 
		m_info.GetCount ()			+ _T (", ") + 
		m_info.GetStateString ()	+ _T (", ") + 
		m_info.GetStatusMessage ()	+ _T (", ") + 
		m_info.GetTaskName ()		+ _T (", ") + 
		m_info.GetKeyValue ()		+ _T (", ");
	
	if (theApp.ShowPrintCycle ())
		str += FoxjetDatabase::ToString (m_info.IsPrinting ());

	return str;
}

bool CHeadItem::GetRedraw ()  
{ 
	CString str = ToString ();

	if (m_strLastState != str) {
		m_strLastState = str;
		return true;
	}

	return false; 
}

/////////////////////////////////////////////////////////////////////////////
// CHeadCtrl

CHeadCtrl::CHeadCtrl()
{
}

CHeadCtrl::~CHeadCtrl()
{
}


BEGIN_MESSAGE_MAP(CHeadCtrl, CListCtrl)
	//{{AFX_MSG_MAP(CHeadCtrl)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_WM_MEASUREITEM_REFLECT()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHeadCtrl message handlers

COLORREF CHeadCtrl::GetIconColor (const CHeadItem & h)
{
	if (h.m_dwThreadID == 0)
		return Color::rgbLightGray;
	else { 
		bool bConnected = true;

		{
			DWORD dw = 0;

			SEND_THREAD_MESSAGE (h.m_info, TM_GET_ERROR_STATE, (WPARAM)&dw);
		}

		if (!bConnected)
			return Color::rgbLightGray;
		else if (h.m_info.m_bDisabled)
			return Color::rgbLightGray;
		else if (Head::CHead::IsError (h.m_info.GetStatus ()))
			return Color::rgbRed;
		else if (Head::CHead::IsWarning (h.m_info.GetStatus ()))
			return Color::rgbYellow;
	}

	return Color::rgbGreen;
}

void CHeadCtrl::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	ASSERT (lpDrawItemStruct);

	DRAWITEMSTRUCT item;
	
	memcpy (&item, lpDrawItemStruct, sizeof (item));

	{
		const int nOffset = GetScrollPos (SB_HORZ);
		COLORREF rgbBack = Color::rgbCOLOR_WINDOW;
		COLORREF rgbFore = Color::rgbCOLOR_WINDOWTEXT;
		COLORREF rgbHighlight = Color::rgbCOLOR_HIGHLIGHT;
		static const CSize sizeIcon (::GetSystemMetrics (SM_CXSMICON), ::GetSystemMetrics (SM_CYSMICON)); 
		LOGBRUSH lbBack = { BS_SOLID, rgbBack, 0};
		LOGBRUSH lbFore = { BS_SOLID, rgbFore, 0};
		LOGBRUSH lbHighlight = { BS_SOLID, rgbHighlight, 0};
		HBRUSH brBack = ::CreateBrushIndirect (&lbBack);
		HBRUSH brFore = ::CreateBrushIndirect (&lbFore);
		HBRUSH brHighlight = ::CreateBrushIndirect (&lbHighlight);

		ASSERT (item.hDC);
		ASSERT (brBack);
		ASSERT (brFore);
		ASSERT (brHighlight);

		::FillRect (item.hDC, &item.rcItem, brBack);
		
		int nWidth = 0, nPrevWidth = 0;

		if (CHeaderCtrl * pHeader = GetHeaderCtrl ()) {
			int nColumns = pHeader->GetItemCount ();
			int nFlags = DT_END_ELLIPSIS | DT_NOPREFIX;
			bool bPrinting = false;

			// draw the individual cells
			for (int i = 0; i < nColumns; i++) {
				HDITEM hdi;
				CString str;

				hdi.mask = HDI_FORMAT;

				if (pHeader->GetItem (i, &hdi)) {
					if (hdi.fmt & HDF_CENTER)	nFlags |= DT_CENTER;
					if (hdi.fmt & HDF_LEFT)		nFlags |= DT_LEFT;
					if (hdi.fmt & HDF_RIGHT)	nFlags |= DT_RIGHT;
				}

				nPrevWidth += nWidth;
				nWidth = GetColumnWidth (i);
				int nCellLeft  = nPrevWidth - nOffset;
				int nCellRight = nPrevWidth + nWidth - nOffset;
				CRect rcCell (nCellLeft, item.rcItem.top, nCellRight, item.rcItem.bottom);

				if (CHeadItem * pItem = (CHeadItem *)GetItemData (item.itemID)) {
					if (theApp.ShowPrintCycle ())
						bPrinting = pItem->m_info.IsPrinting ();

					switch (i) {
					case 0:
						{
							CRect rc = CRect (0, 0, sizeIcon.cx, sizeIcon.cy) + rcCell.TopLeft ();
							COLORREF rgb = GetIconColor (* pItem);
							LOGBRUSH lbIcon = { BS_SOLID, rgb, 0};
							HBRUSH brIcon = ::CreateBrushIndirect (&lbIcon);

							ASSERT (brIcon);

							str = pItem->m_info.GetPanelName ();
							rcCell.left += sizeIcon.cx; 

							HGDIOBJ hOld = ::SelectObject (item.hDC, brIcon);
							::Ellipse (item.hDC, rc.left, rc.top, rc.right, rc.bottom);
							::SelectObject (item.hDC, hOld);
							::DeleteObject (brIcon);
						}
						break;
					case 1: str = pItem->m_info.GetFriendlyName ();		break;
					case 3: str = pItem->m_info.GetCount ();			break;
					case 4: str = pItem->m_info.GetStateString ();		break;
					case 5: str = pItem->m_info.GetStatusMessage ();	break;
					case 2: //str = pItem->m_info.GetTaskName ();			break;
						{
							CString strKey = pItem->m_info.GetKeyValue ();

							str = pItem->m_info.GetTaskName ();

							if (strKey.GetLength ())
								str += _T (" [") + strKey + _T ("]");

						}
						break;
					}
				}

				int nTextWidth;
				
				{
					CSize size (0, 0);

					::GetTextExtentPoint32 (item.hDC, str, str.GetLength(), &size);
					nTextWidth = size.cx;
				}
				
				// only want to write over the background if 
				// this item is NOT selected
				if (item.itemState & ODS_SELECTED) {
					::FillRect (item.hDC, &rcCell, brHighlight);
					::SetTextColor (item.hDC, !bPrinting ? rgbBack : rgbRed);
				}
				else { // invert the text color
					LOGBRUSH lb = { BS_SOLID, !bPrinting ? rgbWhite : rgbRed, 0};
					HBRUSH br = ::CreateBrushIndirect (&lb);
					CRect rcAdjust (rcCell);

					ASSERT (br);

					// there is a slight gap between the bottom of the header
					// & the first item. this fills it
					if (rcAdjust.top < 20) rcAdjust.top -= 5;

					::FillRect (item.hDC, &rcAdjust, br);
					::SetTextColor (item.hDC, rgbFore);
					::DeleteObject (br);
				}

				// this keeps text from adjacent columns from mingling
				rcCell.left += 2;
				rcCell.right -= 5;

				if (rcCell.left < rcCell.right) 
					::DrawText (item.hDC, str, str.GetLength (), &rcCell, nFlags);
			}

			if (CHeadItem * pItem = (CHeadItem *)GetItemData (item.itemID)) {
				nWidth = nPrevWidth = 0;
				DWORD dw = 0;

				SEND_THREAD_MESSAGE (pItem->m_info, TM_GET_ERROR_STATE, (WPARAM)&dw);

				for (int i = 0; i < nColumns; i++) {
					nPrevWidth += nWidth;
					nWidth = GetColumnWidth (i);
					int nCellLeft  = nPrevWidth - nOffset;
					int nCellRight = nPrevWidth + nWidth - nOffset;
					CRect rcCell (nCellLeft, item.rcItem.top, nCellRight, item.rcItem.bottom);

					switch (i) {
					case 6:
					case 7:
					case 8:
						int n = min (rcCell.Width (), rcCell.Height ()) * 2 / 3;
						int nDiff [] = { rcCell.Width () - n, rcCell.Height () - n };
						CRect rc = CRect (0, 0, n, n) + rcCell.TopLeft () + CPoint (nDiff [0] / 2, nDiff [1] / 2);
						HBRUSH hbr = ::CreateSolidBrush (GetStateColor (i, dw));
						::FillRect (item.hDC, rc, hbr);
						::DrawEdge (item.hDC, rc, EDGE_SUNKEN, BF_LEFT | BF_RIGHT | BF_TOP | BF_BOTTOM | BF_ADJUST);
						::DeleteObject (hbr);
						break;
					}
				}
			}
		}
		
		::DeleteObject (brBack);
		::DeleteObject (brFore);
		::DeleteObject (brHighlight);

		return;
	}
}

COLORREF CHeadCtrl::GetStateColor (int nIndex, DWORD dwState)
{
	COLORREF rgb = Color::rgbGreen;

	switch (nIndex) {
	case 6: // temp
		if (dwState & HEADERROR_LOWTEMP)
			return Color::rgbRed;
		break;
	case 7:	// hv
		if (dwState & HEADERROR_HIGHVOLTAGE)
			return Color::rgbRed;
		break;
	case 8:	// ink
		if (dwState & HEADERROR_OUTOFINK)
			return Color::rgbRed;
		else if (dwState & HEADERROR_LOWINK)
			return Color::rgbYellow;
		break;
	}

	return rgb;
}

void CHeadCtrl::ResetContent ()
{
	for (int i = 0; i < GetItemCount (); i++) 
		if (CHeadItem * p = (CHeadItem *)GetItemData (i))
			delete p;

	DeleteAllItems ();
}

const UINT nDefWidth [] = 
{
	100,
	100,
	100,
	80,
	100,
	0, //100, // status message
	50, 40, 40
};

bool CHeadCtrl::Create (CWnd * pParent)
{
	BOOL bResult = CListCtrl::Create (
		LVS_REPORT | LVS_SINGLESEL | LVS_ALIGNLEFT | LVS_NOSORTHEADER | LVS_OWNERDRAWFIXED |
		WS_CHILD | WS_VISIBLE | WS_BORDER |  WS_TABSTOP, 
		CRect (), pParent, IDC_HEAD_LIST);

	if (bResult) {
		DWORD dwStyle = GetExtendedStyle () | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;
		int nIndex = 0;

		SetExtendedStyle (dwStyle);

		InsertColumn (nIndex, LoadString (IDS_PANEL),			LVCFMT_CENTER, nDefWidth [nIndex++]);
		InsertColumn (nIndex, LoadString (IDS_HEAD),			LVCFMT_CENTER, nDefWidth [nIndex++]);
		InsertColumn (nIndex, LoadString (IDS_TASK),			LVCFMT_CENTER, nDefWidth [nIndex++]);
		InsertColumn (nIndex, LoadString (IDS_COUNT),			LVCFMT_CENTER, nDefWidth [nIndex++]);
		InsertColumn (nIndex, LoadString (IDS_IMAGESTATE),		LVCFMT_CENTER, nDefWidth [nIndex++]);
		InsertColumn (nIndex, LoadString (IDS_STATUSMSG),		LVCFMT_CENTER, nDefWidth [nIndex++]);
		InsertColumn (nIndex, LoadString (IDS_TEMP),			LVCFMT_CENTER, nDefWidth [nIndex++]);
		InsertColumn (nIndex, LoadString (IDS_HV),				LVCFMT_CENTER, nDefWidth [nIndex++]);
		InsertColumn (nIndex, LoadString (IDS_INK),				LVCFMT_CENTER, nDefWidth [nIndex++]);
	}

	if (!FoxjetDatabase::IsMatrix ()) {
		if (CFont * p = GetFont ()) {		
			LOGFONT lf;

			p->GetLogFont (&lf);

			lf.lfHeight = (int)(1.5 * (double)lf.lfHeight);
			VERIFY (m_fnt.CreateFontIndirect (&lf));
			SetFont (&m_fnt);

			#ifdef _DEBUG
			{ CString str; str.Format (_T ("%s(%d)"), lf.lfFaceName, lf.lfHeight); TRACEF (str); }
			#endif
		}
	}

	return bResult ? true : false;
}

void CHeadCtrl::MeasureItem( LPMEASUREITEMSTRUCT lpMeasureItemStruct )
{
	if (!FoxjetDatabase::IsMatrix ()) {
		LOGFONT lf;
		GetFont()->GetLogFont( &lf );
		double dFactor = 1.5;

		lpMeasureItemStruct->itemHeight = abs ((int)((double)lf.lfHeight * dFactor)); 
	}
}

void CHeadCtrl::Select (int nIndex)
{
	SetItemState (nIndex, LVIS_SELECTED, LVIS_SELECTED);
}

int CHeadCtrl::GetIndex (DWORD dwThreadID) const
{
	CListCtrl * pList = (CListCtrl *)(LPVOID)this;

	for (int i = 0; i < pList->GetItemCount (); i++)
		if (CHeadItem * p = (CHeadItem *)GetItemData (i))
			if (p->m_dwThreadID == dwThreadID)
				return i;

	return -1;
}

int CHeadCtrl::GetSelectedIndex () const
{
	CLongArray sel = GetSelectedItems (* (CListCtrl *)(LPVOID)this);

	if (sel.GetSize ())
		return sel [0];

	return -1;
}

void CHeadCtrl::Redraw (int nIndex)
{
	if (CHeadItem * p = (CHeadItem *)GetItemData (nIndex)) {
		ULONG lCardID = p->GetHeadID ();

		for (int i = 0; i < GetItemCount (); i++) 
			if (CHeadItem * pTmp = (CHeadItem *)GetItemData (i)) 
				if (pTmp->GetHeadID () == lCardID) 
					RedrawItems (i, i);
	}
}

BOOL CHeadCtrl::DestroyWindow() 
{
	ResetContent ();
	
	return CListCtrl::DestroyWindow();
}


CSelection CHeadCtrl::GetSelection (int nIndex)
{
	CSelection result;

	if (CHeadItem * p = (CHeadItem *)GetItemData (nIndex)) {
		if (CControlDoc * pDoc = (CControlDoc *)theApp.GetActiveDocument ()) {
			result.m_pThread = pDoc->GetHeadByThreadID (p->m_dwThreadID);
			result.m_lPanelID = p->GetPanelID ();
		}
	}

	return result;
}

const FoxjetDatabase::HEADSTRUCT * CHeadCtrl::GetStruct (int nIndex)
{
	if (CHeadItem * p = (CHeadItem *)GetItemData (nIndex))
		return p->m_pHead;

	return NULL;
}

int CHeadCtrl::GetFirst (ULONG lPanelID)
{
	int i;

	for (i = 0; i < GetItemCount (); i++) 
		if (CHeadItem * p = (CHeadItem *)GetItemData (i)) 
			if (p->GetPanelID () > lPanelID) 
				break;

	return i;
}

int CHeadCtrl::Find (ULONG lHeadID)
{
	for (int i = 0; i < GetItemCount (); i++) 
		if (CHeadItem * p = (CHeadItem *)GetItemData (i)) 
			if (p->GetHeadID () == lHeadID)
				return i;

	return -1;
}

int CHeadCtrl::Insert (CHeadItem * pInsert)
{
	bool bCheckDuplicate = true;
	
	#ifdef __WINPRINTER__
	bCheckDuplicate = !ISVALVE ();
	#endif 

	if (bCheckDuplicate) {
		int nFind = Find (pInsert->GetHeadID ());

		if (nFind != -1) {
			CHeadItem * p = (CHeadItem *)GetItemData (nFind);
			//ASSERT (p->m_pThread);
			//ASSERT (pInsert->m_pThread);
			//TRACEF (FoxjetDatabase::ToString (nFind) + _T (": ") + p->m_pThread->GetInfo ().GetUID ());
			//TRACEF (pInsert->m_pThread->GetInfo ().GetUID ());
			return -1;
		}
	}

	int nItems = GetItemCount ();
	int i;

	for (i = GetFirst (pInsert->GetPanelID ()); i < nItems; i++) 
		if (CHeadItem * p = (CHeadItem *)GetItemData (i)) 
			if (pInsert->GetHeight () > p->GetHeight () || p->GetPanelID () != pInsert->GetPanelID ()) 
				break;

	int nIndex = InsertItem (i, pInsert->m_info.GetFriendlyName ());

	VERIFY (SetItemData (nIndex, (DWORD)pInsert));

	if (pInsert->m_dwThreadID) {
		int nOld = FindStruct (pInsert->GetHeadID ());

		if (nOld != -1)
			VERIFY (Delete (nOld));
	}

	return nIndex;
}

int CHeadCtrl::Insert (Head::CHead * pInsert, ULONG lPanelID)
{
	CHeadItem * p = new CHeadItem (pInsert->m_nThreadID, lPanelID);
	int nResult = Insert (p);

	if (nResult == -1)
		delete p;

	return nResult;
}

int CHeadCtrl::Insert (const FoxjetDatabase::HEADSTRUCT & h, ULONG lPanelID)
{
	CHeadItem * p = new CHeadItem (h, lPanelID);
	int nResult = Insert (p);

	if (nResult == -1)
		delete p;

	return nResult;
}

bool CHeadCtrl::Delete (int nIndex)
{
	bool bResult = false;

	if (CHeadItem * p = (CHeadItem *)GetItemData (nIndex)) {
		if (DeleteItem (nIndex)) {
			delete p;
			bResult = true;
		}
	}

	return bResult;
}

int CHeadCtrl::FindStruct (ULONG lHeadID)
{
	for (int i = 0; i < GetItemCount (); i++)
		if (const HEADSTRUCT * p = GetStruct (i)) 
			if (p->m_lID == lHeadID)
				return i;

	return -1;
}

void CHeadCtrl::OnSize(UINT nType, int cx, int cy) 
{
	int nTotal = 0;

	CListCtrl::OnSize(nType, cx, cy);
	
	for (int i = 0; i < ARRAYSIZE (::nDefWidth); i++) 
		nTotal += ::nDefWidth [i];

	double d = max (1.0, (double)cx / (double)nTotal);

	for (int i = 0; i < ARRAYSIZE (::nDefWidth); i++) {
		int n = (int)(d * (double)::nDefWidth [i]);

		SetColumnWidth (i, n);
	}
}
