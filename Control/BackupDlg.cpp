// BackupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Control.h"
#include "BackupDlg.h"
#include "afxdialogex.h"
#include "Database.h"
#include "TemplExt.h"
#include "Parse.h"
#include "BitmapElement.h"
#include "Registry.h"
#include "Extern.h"
#include "Database.h"
#include "List.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;


HRESULT GetProperties(IPortableDeviceProperties* pProperties, LPCWSTR pwszObjectID, DEVICE_FILE_STRUCT & file);
void GetClientInformation(IPortableDeviceValues** clientInformation);
bool TransferContentToDevice (IPortableDevice* device, REFGUID contentType, const CString & strFrom, const CString & strID);


// CBackupDlg dialog

IMPLEMENT_DYNAMIC(CBackupDlg, CEliteDlg)

CBackupDlg::CBackupDlg(CWnd* pParent /*=NULL*/)
:	CEliteDlg(CBackupDlg::IDD, pParent)
{

	m_strPath = _T("");
}

CBackupDlg::~CBackupDlg()
{
}

void CBackupDlg::DoDataExchange(CDataExchange* pDX)
{
	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_BROWSE,	IDB_BROWSE);

	CEliteDlg::DoDataExchange(pDX);
	
	DDX_Text(pDX, TXT_PATH, m_strPath);
}


BEGIN_MESSAGE_MAP(CBackupDlg, CEliteDlg)
	ON_BN_CLICKED(BTN_BROWSE, &CBackupDlg::OnBnClickedBrowse)
	ON_BN_CLICKED(CHK_DATABASE, &CBackupDlg::OnBnClickedDatabase)
	ON_BN_CLICKED(CHK_ALL, &CBackupDlg::OnBnClickedAll)
	ON_BN_CLICKED(BTN_RESTORE, &CBackupDlg::OnBnClickedRestore)
END_MESSAGE_MAP()


// CBackupDlg message handlers


void CBackupDlg::OnBnClickedBrowse()
{
	CFileDialog dlg (FALSE, _T ("*.zip"), m_strPath, 
		OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("Zip files (*.zip)|*.zip||"), this);

	if (dlg.DoModal () == IDOK) {
		m_strPath = dlg.GetPathName ();
		SetDlgItemText (TXT_PATH, m_strPath);
	}
}


void CBackupDlg::OnBnClickedDatabase()
{
	// TODO: Add your control notification handler code here
}


BOOL CBackupDlg::OnInitDialog()
{
	CEliteDlg::OnInitDialog();

	if (CButton * p = (CButton *)GetDlgItem (CHK_DATABASE)) {
		p->SetCheck (1);
		p->EnableWindow (FALSE);
	}

	if (!IsLogicalDrive ()) {
		if (CButton * p = (CButton *)GetDlgItem (BTN_BROWSE)) {
			p->EnableWindow (FALSE);
		}

		if (CComboBox * p = (CComboBox *)GetDlgItem (CB_DIR)) {
			FoxjetCommon::CCopyArray <CString, CString &> v = ToString (m_vRoot, true, false); 

			for (int i = 0; i < v.GetSize (); i++) {
				CString str = v [i];
				int nIndex = p->AddString (str);

				str.MakeLower ();
				
				if (str.Find (_T ("\\download")) != -1)
					p->SetCurSel (nIndex);
			}

			if (p->GetCurSel () == CB_ERR)
				p->SetCurSel (0);
		}
	}
	else {
		if (CWnd * p = GetDlgItem (CB_DIR)) 
			p->ShowWindow (SW_HIDE);
	}

	m_strTempFile = CreateTempFilename (GetTempPath (), _T ("backup"), _T ("zip"));

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CBackupDlg::OnBnClickedAll()
{
	UINT n [] = { CHK_DATABASE, CHK_LOGOS, CHK_DIAGNOSTICS };

	if (CButton * pAll = (CButton *)GetDlgItem (CHK_ALL)) {
		for (int i = 0; i < ARRAYSIZE (n); i++) {
			if (CButton * p = (CButton *)GetDlgItem (n [i])) {
				p->SetCheck (pAll->GetCheck () == 1 ? 1 : 0);
				p->EnableWindow (pAll->GetCheck () == 1 ? FALSE : TRUE);
			}
		}
	}

	if (CButton * p = (CButton *)GetDlgItem (CHK_DATABASE)) {
		p->SetCheck (1);
		p->EnableWindow (FALSE);
	}
}


CString CBackupDlg::GetTempPath ()
{
	TCHAR szTemp [MAX_PATH] = { 0 };
	
	::GetTempPath (ARRAYSIZE (szTemp) - 1, szTemp);
	::GetLongPathName (szTemp, szTemp, ARRAYSIZE (szTemp) - 1);
	CString strPath = szTemp;
//	const CString strKey = _T ("Software\\Foxjet\\Database");
//	CString strPath = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("BackupPath"), GetHomeDir ());

	if (strPath.GetLength ())
		if (strPath [strPath.GetLength () - 1] != '\\')
			strPath += '\\';

	return strPath;
}

bool CBackupDlg::Extract (UINT nID, const CString & strPath, const CString & strType, const CString & strModule)
{
	bool bResult = false; //::GetFileAttributes (strPath) != -1;

	if (!bResult) {
		HMODULE hModule = ::GetModuleHandle (strModule);

		if (HRSRC hRSRC = ::FindResource (hModule, MAKEINTRESOURCE (nID), strType)) {
			DWORD dwSize = ::SizeofResource (hModule, hRSRC);

			if (HGLOBAL hGlobal = ::LoadResource (hModule, hRSRC)) {
				if (LPVOID lpData = ::LockResource (hGlobal)) {
					if (FILE * fp = _tfopen (strPath, _T ("wb"))) {
						fwrite (lpData, dwSize, 1, fp);
						fclose (fp);
						TRACER (_T ("wrote: ") + strPath);
						bResult = true;
					}
				}
			}
		}
	}

	return bResult;
}

void RemoveDuplicates (CStringArray & v)
{
	for (int i = v.GetSize () - 1; i >= 0; i--) {
		for (int j = v.GetSize () - 1; j >= 0; j--) {
			if (i != j) {
				if (!v [i].CompareNoCase (v [j])) {
					v.RemoveAt (j);
					break;
				}
			}
		}
	}
}

static void Enum (const CString & strKey, std::vector <std::wstring> & v, DWORD dwType)
{
	static const CString strDefault = _T ("[$_error]");
	std::vector <std::wstring> vTmp;

	#ifdef _DEBUG
	//TRACEF (strKey);
	//FoxjetDatabase::Encrypted::Trace (strKey, 10);
	//\Registry\User\S-1-5-21-4214632267-836034253-4094818220-1001\SOFTWARE\Foxjet<NULL>\Settings<NULL>
	#endif

	FoxjetDatabase::Encrypted::Enumerate (strKey, vTmp, dwType);

	for (int i = 0; i < vTmp.size (); i++) {
		CString s = FoxjetDatabase::Encrypted::GetProfileString (strKey, vTmp [i].c_str (), strDefault);
		CStringArray vValue;

		if (s != strDefault) {
			vValue.Add (strKey + _T ("\\") + CString (vTmp [i].c_str ()));
			vValue.Add (s);
		}
		else  {
			vValue.Add (strKey + _T ("\\") + CString (vTmp [i].c_str ()));
		}

		//TRACEF (ToString (vValue));
		std::wstring strValue = std::wstring (ToString (vValue));

		if (strValue.length () > 2 && Find (v, strValue) == -1)
			v.push_back (strValue);

		Enum (strKey + _T ("\\") + CString (vTmp [i].c_str ()), v, dwType);
	}
}

static CString GetPath (const CString & strFile)
{
	CString str;
	int nIndex = strFile.ReverseFind ('\\');

	if (nIndex != -1)
		str = strFile.Left (nIndex);

	return str;
}

void CBackupDlg::LocateLogosFolder (CDatabase & db, CStringArray & vResult) 
{
	CMapStringToString vPath;
	CArray <RESULTSTRUCT, RESULTSTRUCT &> v;

	if (db.IsOpen ()) {
		try 
		{
			CRecordset rst (&db);
			CString strSQL;

			strSQL.Format (_T ("SELECT [Data] FROM [Messages]"));

			if (rst.Open (CRecordset::snapshot, strSQL, CRecordset::readOnly)) {
				
				while (!rst.IsEOF ()) {
					CString strData;
					CStringArray vParams;
					
					rst.GetFieldValue ((short)0, strData);
					//TRACEF (strData);
					ParsePackets (strData, vParams);

					for (int nParam = 0; nParam < vParams.GetSize (); nParam++) {
						CString strParam = _T ("{") +  vParams [nParam] + _T ("}");
						const int nIndex = 9;
						const CString strBMP = _T ("{Bitmap,");
						CLongArray v = CountChars (strParam, ',');

						if (strParam.Find (strBMP) != -1) {
							CStringArray v;

							Tokenize (strParam, v);

							CString strFile = UnformatString (v [nIndex]);
							CString strPath = GetPath (strFile) + _T ("\\");

							{
								CString str;
								vPath.Lookup (strPath, str);
								str.Format (_T ("%d"), _ttoi (str) + 1);
								vPath.SetAt (strPath, str);
							}
						}
					}
				
					rst.MoveNext ();
				}
			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	for (POSITION pos = vPath.GetStartPosition (); pos; ) {
		CString strPath, strCount;
		bool bFound = false;
		RESULTSTRUCT s;

		vPath.GetNextAssoc (pos, strPath, strCount);
		s.m_nCount = _ttoi (strCount);
		s.m_strPath = strPath;

		for (int i = 0; !bFound && i < v.GetSize (); i++) {
			if (v [i].m_nCount < s.m_nCount) {
				v.InsertAt (i, s);
				bFound = true;
			}
		}

		if (!bFound)
			v.Add (s);
	}
	
	vResult.RemoveAll ();

	for (int i = 0; i < v.GetSize (); i++) {
		RESULTSTRUCT & s = v [i];

		/*
		#ifdef _DEBUG
		CString str;
		str.Format (_T ("%d: %s"), s.m_nCount, s.m_strPath);
		TRACEF (str);
		#endif
		*/

		//vResult.Add (s.m_strPath);
		if (Find (vResult, s.m_strPath) == -1)
			InsertAscending (vResult, s.m_strPath);
	}
}

void CBackupDlg::OnOK()
{
	DBCONNECTION (conn);

	const CString strDBQ = FoxjetDatabase::Extract (theApp.m_Database.GetConnect (), _T ("DBQ="), _T (";"));
	CString str, strMdb;
	bool bAll = false;
	
	UpdateData ();

	if (IsLogicalDrive ()) {
		if (::GetFileAttributes (m_strPath) != -1) {
			CString str;

			str.Format (LoadString (IDS_OVERWRITEFILE), m_strPath);

			if (MsgBox (str, LoadString (IDS_CONFIRM), MB_YESNO) == IDNO)
				return;
		}
	}

	const CString strZip = GetTempPath () + CString (_T ("7z.exe"));
	CStringArray v;

	#define IDR_7Z_EXE                      136
	#define IDR_7Z_DLL                      135
	#define IDR_7ZIP_DLL                    137

	VERIFY (Extract (IDR_7Z_EXE, strZip));
	VERIFY (Extract (IDR_7Z_DLL, GetTempPath () + _T ("7z.dll")));
	VERIFY (Extract (IDR_7ZIP_DLL, GetTempPath () + _T ("7-zip.dll")));

	if (GetTempPath ().GetLength ()) {
		int nIndex = strDBQ.ReverseFind ('\\');

		if (nIndex != -1) {
			CString strTitle = strDBQ.Mid (nIndex + 1);
			strMdb = GetTempPath () + strTitle;

			if (strMdb.CompareNoCase (strDBQ) != 0) {
				TRACER (_T ("DeleteFile: ") + strMdb);
				::DeleteFile (strMdb);
				TRACER (_T ("CopyFile: ") + strDBQ + _T (" --> ") + strMdb);
				::CopyFile (strDBQ, strMdb, FALSE);
			}
		}
	}

	{
		std::vector <std::wstring> vMAC = GetMacAddresses ();
		std::vector <std::wstring> vData, vRegistry;
		CString str, strFile = GetTempPath () + _T ("data.txt");
		const CString strKey = FoxjetDatabase::Encrypted::InkCodes::GetRegRootSection ();


		//#ifdef _DEBUG
		//TRACEF (strKey);
		//FoxjetDatabase::Encrypted::Trace (strKey, 20);
		//#endif

		Enum (strKey, vRegistry, ENUMERATE_VALUES | ENUMERATE_KEYS);

		for (int i = 0; i < vMAC.size (); i++) 
			vData.push_back (std::wstring (_T ("MAC=") + CString (vMAC [i].c_str ())));

		for (int i = 0; i < vRegistry.size (); i++) {
			CString str = vRegistry [i].c_str ();
			CStringArray v;

			FromString (str, v);

			if (v.GetSize ()) {
				CString s = v [0];

				s.Replace (strKey, _T (""));
				v.SetAt (0, s);
			}

			str = FoxjetDatabase::ToString (v);
			//TRACEF (str);
			vData.push_back (std::wstring (str));
		}

		if (FILE * fp = _tfopen (strFile, _T ("w"))) {
			for (int i = 0; i < vData.size (); i++) {
				CString str = vData [i].c_str ();

				//TRACEF (str);

				/*
				{
					CStringArray v;
					FromString (str, v);
					for (int i = 0; i < v.GetSize (); i++)
						TRACEF (v [i]);
				}
				*/

				CString strEncrypted = FoxjetDatabase::Encrypted::RSA::Encrypt (str);
				//TRACEF (strEncrypted);
				ASSERT (str != strEncrypted);
				strEncrypted += _T ("\r\n");
				fwrite (w2a (strEncrypted), strEncrypted.GetLength (), 1, fp);
			}

			fclose (fp);
			v.Add (strFile);
		}
	}

	if (CButton * p = (CButton *)GetDlgItem (CHK_LOGOS)) {
		if (p->GetCheck () == 1) {
			CStringArray vLogos;
			
			LocateLogosFolder (theApp.m_Database, vLogos);

			if (Find (vLogos, FoxjetElements::CBitmapElement::GetDefPath (), false) == -1)
				vLogos.Add (FoxjetElements::CBitmapElement::GetDefPath ());

			#ifdef _DEBUG
			for (int i = 0; i < vLogos.GetSize (); i++) 
				TRACEF (vLogos [i]);
			#endif // _DEBUG

			v.Append (vLogos);
		}
	}

	if (CButton * p = (CButton *)GetDlgItem (CHK_DIAGNOSTICS)) {
		if (p->GetCheck () == 1) {
			CStringArray vTmp;
			CString strCmdLine = _T ("\"") + FoxjetDatabase::GetHomeDir () + _T ("\\Diagnostics.exe\" /runas_elevated /no_notepad ") + ::GetCommandLine ();
			
			strCmdLine = FoxjetDatabase::Encrypted::GetCommandLine (strCmdLine);
			
			BeginWaitCursor ();
			TRACER (strCmdLine);
			Tokenize (execute (strCmdLine), vTmp, '\n');
			EndWaitCursor ();

			if (vTmp.GetSize ()) {
				CString strFile = vTmp [0];

				strFile.Trim ();
				v.Add (strFile);

				{
					CString str = Encrypted::GetInkCodes ();
		
					if (str.GetLength ()) {
						if (FILE * fp = _tfopen (strFile, _T ("a"))) {
							fwrite (w2a (str), str.GetLength (), 1, fp);
							fclose (fp);
						}
					}
				}
			}
		}
	}

	if (CButton * p = (CButton *)GetDlgItem (CHK_ALL)) {
		if (p->GetCheck () == 1) {
			v.RemoveAll ();
			bAll = true;
		}
	}

	{
		CStringArray vDSN;

		try 
		{
			CString strDSN = FoxjetDatabase::Extract (theApp.m_Database.GetConnect (), _T ("DSN="), _T (";"));
			
			FoxjetCommon::EnumDSNs (vDSN, theApp.m_Database);

			if (Find (vDSN, strDSN) == -1)
				vDSN.Add (strDSN);
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
		
		for (int i = 0; i < vDSN.GetSize (); i++) {
			HKEY hKey = NULL;
			CString strKey = _T ("SOFTWARE\\ODBC\\ODBC.INI\\") + vDSN [i];
			const CString strDSN = GetTempPath () + vDSN [i] + _T (".reg");
			const CString strDBQ = GetTempPath () + vDSN [i] + _T (".dbq");

			::DeleteFile (strDSN);
			::DeleteFile (strDBQ);

			if (::RegCreateKey (HKEY_CURRENT_USER, strKey, &hKey) == ERROR_SUCCESS) {
				VERIFY (FoxjetDatabase::EnablePrivilege (SE_BACKUP_NAME));
				LRESULT lResult = ::RegSaveKey (hKey, strDSN, NULL);
				TRACEF (FormatMessage (lResult));
				::RegCloseKey (hKey);

				if (Find (v, strDSN, false) == -1) {
					TRACER (strDSN);
					v.Add (strDSN);
				}
			}

			try 
			{
				CDatabase db;

				str = _T ("DSN=") + vDSN [i];
				TRACEF (_T ("Testing connection: ") + str);
					
				if (db.OpenEx (str, CDatabase::noOdbcDialog)) {				
					str = db.GetConnect ();

					if (FILE * fp = _tfopen (strDBQ, _T ("w"))) {
						const BYTE nUTF16 [] = { 0xFF, 0xFE };
						fwrite (nUTF16, ARRAYSIZE (nUTF16), 1, fp);
						fwrite ((LPCTSTR)str, str.GetLength () * sizeof (TCHAR), 1, fp);
						fclose (fp);

						if (Find (v, strDBQ, false) == -1) {
							TRACER (strDBQ);
							v.Add (strDBQ);
						}
					}
				}
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }
		}
	}

	// Usage: 7z <command> [<switches>...] <archive_name> [<file_names>...] [<@listfiles...>]

	if (v.GetSize ()) {
		CString strDBQ = FoxjetDatabase::Extract (theApp.m_Database.GetConnect (), _T ("DBQ="), _T (";"));

		for (int nIndex = Find (v, strDBQ, false); nIndex != -1; ) {
			v.RemoveAt (nIndex);
			nIndex = Find (v, strDBQ, false);
		}

		RemoveDuplicates (v);

		for (int i = 0; i < v.GetSize (); i++)
			TRACEF (v [i]);

		for (int i = 0; i < v.GetSize (); i++) {
			//str += _T (" \"") + v [i] + _T ("\"");
			str.Format (_T ("\"%s\" a \"%s\" \"%s\""), strZip, m_strTempFile, v[i]);
			TRACER (str);
			str = execute (str);
			TRACER (str);
		}
	}

	TRACER (str);

	//if (!bAll) {
	//	str = execute (str);
	//	TRACER (str);
	//}
	//else {
	if (bAll) {
		STARTUPINFO si;     
		PROCESS_INFORMATION pi; 
		TCHAR * pszCmdLine = new TCHAR [str.GetLength () + 1];
		bool bMore = true;
		CTime tmStart = CTime::GetCurrentTime ();

		str.Format (_T ("\"%s\" a \"%s\" \"%s\""), strZip, m_strTempFile, GetHomeDir ());

		ZeroMemory (pszCmdLine, str.GetLength () + 1);
		_tcsncpy (pszCmdLine, str, 0xFF);
		memset (&pi, 0, sizeof(pi));
		memset (&si, 0, sizeof(si));
		si.cb = sizeof(si);     
		si.dwFlags = STARTF_USESHOWWINDOW;     
		si.wShowWindow = SW_SHOW;     

		::CreateProcess (NULL, pszCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);

		do {
			DWORD dwExitCode = STILL_ACTIVE;
		
			if (!GetExitCodeProcess (pi.hProcess, &dwExitCode))
				bMore = false;
			else {
				bMore = (dwExitCode == STILL_ACTIVE);
				::WaitForSingleObjectEx (pi.hProcess, 3000, TRUE);
				TRACEF (_T ("still waiting"));
			}

			CTimeSpan tm = CTime::GetCurrentTime () - tmStart;

			if (tm.GetTotalSeconds () > (5 * 60))
				break;
		}
		while (bMore);

		delete [] pszCmdLine;
	}

	str.Format (_T ("\"%s\" d \"%s\" \"%s\""), strZip, m_strTempFile, strDBQ);
	TRACER (str);
	str = execute (str);
	TRACER (_T ("execute: ") + str);

	str.Format (_T ("\"%s\" a \"%s\" \"%s\""), strZip, m_strTempFile, strMdb);
	TRACER (str);
	str = execute (str);
	TRACER (_T ("execute: ") + str);

	CEliteDlg::OnOK();

	if (!IsLogicalDrive ()) {
		if (CComboBox * p = (CComboBox *)GetDlgItem (CB_DIR)) {
			CString strDir;

			p->GetLBText (p->GetCurSel (), strDir);
			m_strPath = strDir + _T ("\\") + m_strPath;
			m_strTitle = m_strPath;
			int nIndex = m_strTitle.ReverseFind ('.');

			m_strTitle.Replace (_T ("\\"), _T ("/"));

			if (nIndex != -1) 
				m_strTitle = m_strTitle.Left (nIndex);
		}
	}
}

FoxjetCommon::CCopyArray <CString, CString &> CBackupDlg::ToString (const DEVICE_FILE_STRUCT & d, bool bDirectory, bool bFile, const CString & strDir)
{
	FoxjetCommon::CCopyArray <CString, CString &> result;

	if ((bDirectory && !d.m_lSize) || (bFile && d.m_lSize)) {
		CString str;

		str.Format (_T ("%s\\%s"), strDir, d.m_strName);
		result.Add (str);
	}

	for (int i = 0; i < d.m_v.GetSize (); i++) 
		result.Append (ToString (d.m_v [i], bDirectory, bFile, strDir + _T ("\\") + d.m_strName));

	return result;
}

CString CBackupDlg::GetID (const DEVICE_FILE_STRUCT & d, const CString & strName, const CString & strDir)
{
	if (!strName.CompareNoCase (strDir + '/' + d.m_strName))
		return d.m_strID;

	for (int i = 0; i < d.m_v.GetSize (); i++) {
		CString str = GetID (d.m_v [i], strName, strDir + '/' + d.m_strName);

		if (str.GetLength ())
			return str;
	}

	return _T ("");
}

#include "ContentTransfer.h"


void GetClientInformation(IPortableDeviceValues** clientInformation)
{
    // Client information is optional.  The client can choose to identify itself, or
    // to remain unknown to the driver.  It is beneficial to identify yourself because
    // drivers may be able to optimize their behavior for known clients. (e.g. An
    // IHV may want their bundled driver to perform differently when connected to their
    // bundled software.)

    // CoCreate an IPortableDeviceValues interface to hold the client information.
    //<SnippetDeviceEnum7>
    HRESULT hr = CoCreateInstance(CLSID_PortableDeviceValues,
                                  nullptr,
                                  CLSCTX_INPROC_SERVER,
                                  IID_PPV_ARGS(clientInformation));
    //</SnippetDeviceEnum7>
    //<SnippetDeviceEnum8>
    if (SUCCEEDED(hr))
    {
        // Attempt to set all bits of client information
        hr = (*clientInformation)->SetStringValue(WPD_CLIENT_NAME, _T ("Box Writer"));
        if (FAILED(hr))
        {
            TRACEF (_T ("! Failed to set WPD_CLIENT_NAME, hr = ") + FormatMessage (hr));
        }

        hr = (*clientInformation)->SetUnsignedIntegerValue(WPD_CLIENT_MAJOR_VERSION, FoxjetCommon::verApp.m_nMajor);
        if (FAILED(hr))
        {
            TRACEF (_T ("! Failed to set WPD_CLIENT_MAJOR_VERSION, hr = ") + FormatMessage (hr));
        }

        hr = (*clientInformation)->SetUnsignedIntegerValue(WPD_CLIENT_MINOR_VERSION, FoxjetCommon::verApp.m_nMinor);
        if (FAILED(hr))
        {
            TRACEF (_T ("! Failed to set WPD_CLIENT_MINOR_VERSION, hr = ") + FormatMessage (hr));
        }

		hr = (*clientInformation)->SetUnsignedIntegerValue(WPD_CLIENT_REVISION, FoxjetCommon::verApp.m_nInternal);
        if (FAILED(hr))
        {
            TRACEF (_T ("! Failed to set WPD_CLIENT_REVISION, hr = ") + FormatMessage (hr));
        }

        //  Some device drivers need to impersonate the caller in order to function correctly.  Since our application does not
        //  need to restrict its identity, specify SECURITY_IMPERSONATION so that we work with all devices.
        hr = (*clientInformation)->SetUnsignedIntegerValue(WPD_CLIENT_SECURITY_QUALITY_OF_SERVICE, SECURITY_IMPERSONATION);
        if (FAILED(hr))
        {
            TRACEF (_T ("! Failed to set WPD_CLIENT_SECURITY_QUALITY_OF_SERVICE, hr = ") + FormatMessage (hr));
        }
    }
    else
    {
        TRACEF (_T ("! Failed to CoCreateInstance CLSID_PortableDeviceValues, hr = ") + FormatMessage (hr));
    }
    //</SnippetDeviceEnum8>
}

void CBackupDlg::Trace (const DEVICE_FILE_STRUCT & dir, int nCalls)
{
	CString str;

	str.Format (_T ("%s[%s] %s (%d bytes)"), CString (' ', nCalls * 4), dir.m_strID, dir.m_strName, dir.m_lSize);
	TRACEF (str);

	for (int i = 0; i < dir.m_v.GetSize (); i++)
		Trace (dir.m_v [i], nCalls + 1);
}

HRESULT GetProperties(IPortableDeviceProperties* pProperties, LPCWSTR pwszObjectID, DEVICE_FILE_STRUCT & file)
{
    HRESULT hr = S_OK;
    // Specify the properties we are interested in spPropertyKeys
    CComPtr<IPortableDeviceKeyCollection> spPropertyKeys;

    if (hr == S_OK)

    {
        hr = CoCreateInstance(CLSID_PortableDeviceKeyCollection,
                              NULL,
                              CLSCTX_INPROC_SERVER,
                              IID_IPortableDeviceKeyCollection,
                              (VOID**)&spPropertyKeys);
    }

    if (hr == S_OK)
    {
        hr = spPropertyKeys->Add(WPD_OBJECT_NAME);
    }

    if (hr == S_OK)
    {
        hr = spPropertyKeys->Add(WPD_OBJECT_SIZE);
    }

    // Use the GetValues API to get the desired values
    CComPtr<IPortableDeviceValues> spPropertyValues;

    if (hr == S_OK)
    {
        hr = pProperties->GetValues(pwszObjectID, spPropertyKeys, &spPropertyValues);
        // GetValues may return S_FALSE if one or more properties could not be retrieved

        if (hr == S_FALSE)
        {
            hr = S_OK;
        }
    }

    // Get value of each requested property
    LPWSTR pwszName = NULL;

    if (hr == S_OK)
    {
        hr = spPropertyValues->GetStringValue(WPD_OBJECT_NAME, &pwszName);
    }

    ULONGLONG ullSize = 0;

    if (hr == S_OK)
    {
        hr = spPropertyValues->GetUnsignedLargeIntegerValue(WPD_OBJECT_SIZE, &ullSize);
        // WPD_OBJECT_SIZE may not be supported some objects

        if (FAILED(hr))
        {
            hr = S_OK;
        }
    }

    // Display object properties
	if (hr == S_OK)
    {
		file.m_strID = pwszObjectID;
		file.m_strName = pwszName;
		file.m_lSize = ullSize;
    }

    // Free any memory allocated by GetStringValue
    if (pwszName != NULL)
    {
        CoTaskMemFree(pwszName);
    }

    return hr;
}

HRESULT CBackupDlg::Enumerate (IPortableDeviceContent* pContent, LPCWSTR pwszParentObjectId, DEVICE_FILE_STRUCT & dir, int nDepth)
{
    HRESULT hr = S_OK;
    // Display properties of supplied object
    CComPtr<IPortableDeviceProperties> spProperties;
    hr = pContent->Properties(&spProperties);

    if (hr == S_OK)
    {
        hr = GetProperties(spProperties, pwszParentObjectId, dir);
    }

    // Enumerate children (if any) of provided object
    CComPtr <IEnumPortableDeviceObjectIDs> spEnum;

    if (hr == S_OK)
    {
        hr = pContent->EnumObjects(0, pwszParentObjectId, NULL, &spEnum);
    }

    while (hr == S_OK)
    {
        // We�ll enumerate one object at a time, but be aware that an array can
        // be supplied to the Next API to optimize enumeration
        LPWSTR pwszObjectId = NULL;
        ULONG celtFetched = 0;

        hr = spEnum->Next(1, &pwszObjectId, &celtFetched);

		//TRACER (FoxjetDatabase::ToString ((int)celtFetched)  + _T (": ") + (CString)pwszObjectId);

        // Try enumerating children of this object
        if (hr == S_OK)
        {
			if (nDepth >= 0) {
				DEVICE_FILE_STRUCT d;
				hr = Enumerate(pContent, pwszObjectId, d, nDepth - 1);
				dir.m_v.Add (d);
			}
        }
    }

    // Once no more children are available, S_FALSE is returned which we promote to S_OK
    if (hr == S_FALSE)
    {
        hr = S_OK;
    }

    return hr;
}

CComPtr<IPortableDevice> CBackupDlg::OpenDevice (const CString & strDevicePath)
{
	bool bResult = false;
	HRESULT hr = S_OK;
	CComPtr<IPortableDevice>			device;
	CComPtr<IPortableDeviceValues>		clientInformation;
	CComPtr<IPortableDeviceContent>     content;

    GetClientInformation(&clientInformation);

	if (SUCCEEDED (CoCreateInstance (CLSID_PortableDeviceFTM, nullptr, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&device)))) {
		hr = device->Open (strDevicePath, clientInformation);
		                        
		if (hr == E_ACCESSDENIED) {
			TRACEF ("E_ACCESSDENIED");
			return NULL;
		}
		else if (FAILED (hr)) {
			TRACEF ("FAILED");
			return NULL;
		}
		else 
			return device;
	}

	return NULL;
}

bool CBackupDlg::CopyToPortableDevice (CComPtr<IPortableDevice> & device, const CString & strFrom, const CString & strTo, const DEVICE_FILE_STRUCT & root)
{
	bool bResult = false;
	CComPtr <IPortableDeviceContent> content;

	if (SUCCEEDED (device->Content (&content))) {
		#ifdef _DEBUG
		DEVICE_FILE_STRUCT d;
		Enumerate (content, WPD_DEVICE_OBJECT_ID, d, 4);
		Trace (d);
		#endif

		CString str = strTo;
		str.Replace ('\\', '/');
		std::vector <std::wstring> v = explode (std::wstring (str), '/');

		if (v.size () > 2) {
			v.erase (v.end () - 1);
			CString strDir = implode (v, std::wstring (_T ("/"))).c_str (); // v [v.size () - 2].c_str ();
			CString strID = CBackupDlg::GetID (root, strDir);

			if (strDir.GetLength () && strID.GetLength ())
				bResult = TransferContentToDevice (device, WPD_CONTENT_TYPE_DOCUMENT, strFrom, strID);
		}

		#ifdef _DEBUG
		Enumerate (content, WPD_DEVICE_OBJECT_ID, d, 4);
		Trace (d);
		#endif
	}

	return bResult;
}

bool TransferContentToDevice (IPortableDevice* device, REFGUID contentType, const CString & strFrom, const CString & strID)
{
	bool bResult = false;
    HRESULT                             hr                       = S_OK;
    DWORD                               optimalTransferSizeBytes = 0;
    CComPtr<IStream>                     fileStream;
    CComPtr<IPortableDeviceDataStream>   finalObjectDataStream;
    CComPtr<IPortableDeviceValues>       finalObjectProperties;
    CComPtr<IPortableDeviceContent>      content;
    CComPtr<IStream>                     tempStream;  // Temporary IStream which we use to QI for IPortableDeviceDataStream

    // Prompt user to enter an object identifier for the parent object on the device to transfer.
    //wprintf(L"Enter the identifer of the parent object which the file will be transferred under.\n>");
    //hr = StringCchGetsW(selection, ARRAYSIZE(selection));
	
    if (FAILED(hr))
    {
        TRACEF (L"An invalid object identifier was specified, aborting content transfer");
    }
    //</SnippetContentTransfer1>
    // 1) Get an IPortableDeviceContent interface from the IPortableDevice interface to
    // access the content-specific methods.
    //<SnippetContentTransfer2>
    if (SUCCEEDED(hr))
    {
        hr = device->Content(&content);
        if (FAILED(hr))
        {
            TRACEF (_T ("! Failed to get IPortableDeviceContent from IPortableDevice, hr = ") + FormatMessage (hr));
        }
    }

    if (SUCCEEDED(hr))
    {
        // Open the selected file as an IStream.  This will simplify reading the
        // data and writing to the device.
        hr = SHCreateStreamOnFileEx(strFrom, STGM_READ, FILE_ATTRIBUTE_NORMAL, FALSE, nullptr, &fileStream);
        if (SUCCEEDED(hr))
        {
            // Get the required properties needed to properly describe the data being
            // transferred to the device.
            hr = GetRequiredPropertiesForContentType(contentType,              // Content type of the data
                                                     strID,                // Parent to transfer the data under
                                                     strFrom,                 // Full file path to the data file
                                                     fileStream,         // Open IStream that contains the data
                                                     &finalObjectProperties);  // Returned properties describing the data
            if (FAILED(hr))
            {
                TRACEF (_T ("! Failed to get required properties needed to transfer a file to the device, hr = ") + FormatMessage (hr));
            }
        }

        if (FAILED(hr))
        {
            TRACEF (_T ("! Failed to open file named (") + strFrom + _T (") to transfer to device, hr = ") + FormatMessage (hr));
        }
    }
    //</SnippetContentTransfer4>
    //<SnippetContentTransfer5>
    // 4) Transfer for the content to the device
    if (SUCCEEDED(hr))
    {
        hr = content->CreateObjectWithPropertiesAndData(finalObjectProperties,    // Properties describing the object data
                                                        &tempStream,                    // Returned object data stream (to transfer the data to)
                                                        &optimalTransferSizeBytes,      // Returned optimal buffer size to use during transfer
                                                        nullptr);

        // Once we have a the IStream returned from CreateObjectWithPropertiesAndData,
        // QI for IPortableDeviceDataStream so we can use the additional methods
        // to get more information about the object (i.e. The newly created object
        // identifier on the device)
        if (SUCCEEDED(hr))
        {
            //hr = tempStream.As(&finalObjectDataStream);
            hr = tempStream.QueryInterface (&finalObjectDataStream);
            if (FAILED(hr))
            {
                TRACEF (_T ("! Failed to QueryInterface for IPortableDeviceDataStream, hr =  ") + FormatMessage (hr));
            }
        }

        // Since we have IStream-compatible interfaces, call our helper function
        // that copies the contents of a source stream into a destination stream.
        if (SUCCEEDED(hr))
        {
            DWORD totalBytesWritten = 0;

            hr = StreamCopy(finalObjectDataStream,    // Destination (The Object to transfer to)
                            fileStream,               // Source (The File data to transfer from)
                            optimalTransferSizeBytes,       // The driver specified optimal transfer buffer size
                            &totalBytesWritten);            // The total number of bytes transferred from file to the device
            if (FAILED(hr))
            {
                TRACEF(_T ("! Failed to transfer object to device, hr = ") + FormatMessage (hr));
            }
        }
        else
        {
            TRACEF(_T ("! Failed to get IStream (representing destination object data on the device) from IPortableDeviceContent, hr =  ") + FormatMessage (hr));
        }

        // After transferring content to the device, the client is responsible for letting the
        // driver know that the transfer is complete by calling the Commit() method
        // on the IPortableDeviceDataStream interface.
        if (SUCCEEDED(hr))
        {
            hr = finalObjectDataStream->Commit(STGC_DEFAULT);
            if (FAILED(hr))
            {
                TRACEF(_T ("! Failed to commit object to device, hr = ") + FormatMessage (hr));
            }
        }

        // Some clients may want to know the object identifier of the newly created
        // object.  This is done by calling GetObjectID() method on the
        // IPortableDeviceDataStream interface.
        if (SUCCEEDED(hr))
        {
            PWSTR newlyCreatedObject = nullptr;
            hr = finalObjectDataStream->GetObjectID(&newlyCreatedObject);
            if (SUCCEEDED(hr))
            {
				bResult = true;
            }
            else
            {
                TRACEF (_T ("! Failed to get the newly transferred object's identifier from the device, hr ") + FormatMessage (hr));
            }

            // Free the object identifier string returned from the GetObjectID() method.
            CoTaskMemFree(newlyCreatedObject);
            newlyCreatedObject = nullptr;
        }
    }
    //</SnippetContentTransfer5>

	return bResult;
}

bool CBackupDlg::Delete (CComPtr<IPortableDevice> & device, const CString & strID)
{
	bool bResult = false;
    HRESULT                                       hr               = S_OK;
    CComPtr<IPortableDeviceContent>               pContent;
    CComPtr<IPortableDevicePropVariantCollection> pObjectsToDelete;
    CComPtr<IPortableDevicePropVariantCollection> pObjectsFailedToDelete;

    if (device == NULL)
    {
        TRACER (_T ("! A NULL IPortableDevice interface pointer was received"));
        return bResult;
    }

    if (SUCCEEDED(hr))
    {
        hr = device->Content(&pContent);

		if (FAILED(hr))
            TRACER (_T ("! Failed to get IPortableDeviceContent from IPortableDevice, hr = ") + FormatMessage (hr));
    }

    if (SUCCEEDED(hr))
    {
        hr = CoCreateInstance (CLSID_PortableDevicePropVariantCollection, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pObjectsToDelete));
 
		if (SUCCEEDED(hr))
        {
            if (pObjectsToDelete != NULL)
            {
                PROPVARIANT pv = {0};
                PropVariantInit(&pv);

                pv.vt      = VT_LPWSTR;
                pv.pwszVal = AtlAllocTaskWideString(strID);
                if (pv.pwszVal != NULL)
                {
                    // Add the object identifier to the objects-to-delete list
                    // (We are only deleting 1 in this example)
                    hr = pObjectsToDelete->Add(&pv);
                    if (SUCCEEDED(hr))
                    {
                        // Attempt to delete the object from the device
                        hr = pContent->Delete(PORTABLE_DEVICE_DELETE_NO_RECURSION,  // Deleting with no recursion
                                              pObjectsToDelete,                     // Object(s) to delete
                                              NULL);                                // Object(s) that failed to delete (we are only deleting 1, so we can pass NULL here)
                        if (SUCCEEDED(hr))
                        {
                            // An S_OK return lets the caller know that the deletion was successful
                            if (hr == S_OK)
                                bResult = true;

                            // An S_FALSE return lets the caller know that the deletion failed.
                            // The caller should check the returned IPortableDevicePropVariantCollection
                            // for a list of object identifiers that failed to be deleted.
                            else
                                TRACER (_T ("The object '") + strID + _T ("' failed to be deleted from the device."));
                        }
                        else
                            TRACER (_T ("! Failed to delete an object from the device, hr = ") + FormatMessage (hr));
                    }
                    else
                    {
                        TRACER (_T ("! Failed to delete an object from the device because we could no add the object identifier string to the IPortableDevicePropVariantCollection, hr = ") + FormatMessage (hr));
                    }
                }
                else
                {
                    hr = E_OUTOFMEMORY;
                    TRACER (_T ("! Failed to delete an object from the device because we could no allocate memory for the object identifier string, hr = ") + FormatMessage (hr));
                }

                // Free any allocated values in the PROPVARIANT before exiting
                PropVariantClear(&pv);
            }
            else
            {
                TRACER (_T ("! Failed to delete an object from the device because we were returned a NULL IPortableDevicePropVariantCollection interface pointer, hr = ") + FormatMessage (hr));
            }
		}
        else
        {
            TRACER (_T ("! Failed to CoCreateInstance CLSID_PortableDevicePropVariantCollection, hr = ") + FormatMessage (hr));
        }
	}

	return bResult;
 }


void CBackupDlg::OnBnClickedRestore()
{
	CString str;

	str += _T ("/DBQ=\"") + FoxjetDatabase::Extract (theApp.m_Database.GetConnect (), _T ("DBQ="), _T (";")) + _T ("\" ");
	str += _T ("/LOCAL_BACKUP_DIR=\"") + GetHomeDir () + _T ("\\") + FoxjetDatabase::Extract (theApp.m_Database.GetConnect (), _T ("DSN="), _T (";")) + _T ("\" ");

	if (IsLogicalDrive ()) {
		if (m_strPath.GetLength ())
			str += _T ("/SELECT=\"(") + m_strPath.Left (1) + _T (":)\" ");
	}
	else {
		if (CComboBox * p = (CComboBox *)GetDlgItem (CB_DIR)) {
			CString strPath;
			p->GetLBText (p->GetCurSel (), strPath);
			int nIndex = strPath.Find ('\\', 2);

			if (nIndex != -1)
				str += _T ("/SELECT=\"") + strPath.Left (nIndex) + _T ("\" ");
		}
	}

	FoxjetDatabase::Restore (this, str);
}
