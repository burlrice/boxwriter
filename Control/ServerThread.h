#if !defined(AFX_SERVERTHREAD_H__CDB7D455_30E9_4148_8107_A66D0AAB65E2__INCLUDED_)
#define AFX_SERVERTHREAD_H__CDB7D455_30E9_4148_8107_A66D0AAB65E2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ServerThread.h : header file
//

#include "UdpThread.h"


/////////////////////////////////////////////////////////////////////////////
// CServerThread thread

class CServerThread : public CUdpThread
{
	friend class CUdpThread;

	DECLARE_DYNCREATE(CServerThread)
protected:
	CServerThread();           // protected constructor used by dynamic creation


// Attributes
public:

	virtual ~CServerThread ();

	virtual UINT GetPort () const;
	virtual UINT GetSockType () const;
	virtual CString OnCommand (const CString & str, const SOCKADDR_IN * paddr = NULL);

	virtual bool Open ();
	virtual void Close ();

	void Send (const CString & str);

// Operations
public:

	static CString GetXmlTag (const CString & str, int nTag);
	static int GetXmlTagIndex (const CString & strXML, const CString & strTag);
	static CString CServerThread::UnformatXML (CString str);
	static CString ProcessCommand (const CString & str, CControlView * pView);
	static CString GetLineParam (const CStringArray & v, int nIndex = 1, CControlView * pView = NULL);
	static CString GetLocalAddress (SOCKET socket);

	CString GetConnectedAddr () const { return m_strAddrFrom; }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServerThread)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void ProcessData ();

	static CString GetLines (const CStringArray & vCmd);
	static CString GetTasks (const CStringArray & vCmd);
	static CString GetLineID (const CStringArray & vCmd);
	static CString RegExport (const CStringArray & vCmd);
	static CString RegImport (const CStringArray & vCmd);


	afx_msg void OnKillThread (WPARAM wParam, LPARAM lParam);

	SOCKET m_sockClient;
	CString m_strAddrFrom;

	// Generated message map functions
	//{{AFX_MSG(CServerThread)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	FoxjetDatabase::COdbcDatabase * m_pdb;
	UINT							m_nDbTimeout;
	CTime							m_tmDatabase;

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVERTHREAD_H__CDB7D455_30E9_4148_8107_A66D0AAB65E2__INCLUDED_)
