// SerialPacket.cpp: implementation of the CSerialPacket class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Resource.h"
#include "control.h"
#include "SerialPacket.h"
#include "Parse.h"
#include "StdCommDlg.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define SEG1		17	// DC1
#define SEG2		18	// DC2
#define SEG3		19	// DC3
#define SEG4		20	// DC4
#define SEG5		21	// NAK

static CArray <LINESTRUCT, LINESTRUCT &> vLines;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSerialPacket::CSerialPacket(const CString & strPort)
:	m_strPort (strPort)
{
	::QueryPerformanceCounter (&m_counter);
	m_eDeviceType = (eSerialDevice ) 0;
	m_nDataLen = 0;
	m_bOverride = false;
	try 
	{
		m_pData = new TCHAR [COM_BUF_SIZE];
		memset ( m_pData, 0x00, COM_BUF_SIZE );
	}
	catch ( CMemoryException *e ) { HANDLEEXCEPTION (e); }
}

CSerialPacket::CSerialPacket (const CSerialPacket & rhs)
:	m_strPort (rhs.m_strPort),
	m_counter (rhs.m_counter)
{
	m_eDeviceType	= rhs.m_eDeviceType;
	m_nDataLen		= rhs.m_nDataLen;
	m_bOverride		= rhs.m_bOverride;

	try 
	{
		m_nDataLen = max (rhs.m_nDataLen, COM_BUF_SIZE);
		m_pData = new TCHAR [m_nDataLen];
		memset (m_pData, 0x00, m_nDataLen);
		_tcsncpy (m_pData, rhs.m_pData, m_nDataLen);
	}
	catch ( CMemoryException *e ) { HANDLEEXCEPTION (e); }
}

CSerialPacket & CSerialPacket::operator = (const CSerialPacket & rhs)
{
	if (this != &rhs) {
		m_counter		= rhs.m_counter;
		m_eDeviceType	= rhs.m_eDeviceType;
		m_nDataLen		= rhs.m_nDataLen;
		m_bOverride		= rhs.m_bOverride;
		m_strPort		= rhs.m_strPort;

		try 
		{
			m_nDataLen = max (rhs.m_nDataLen, COM_BUF_SIZE);
			m_pData = new TCHAR [m_nDataLen];
			memset (m_pData, 0x00, m_nDataLen);
			_tcsncpy (m_pData, rhs.m_pData, m_nDataLen);
		}
		catch ( CMemoryException *e ) { HANDLEEXCEPTION (e); }
	}

	return * this;
}

CSerialPacket::~CSerialPacket()
{
	if ( m_pData )
	{
		delete [] m_pData;
		m_pData = NULL;
		m_nDataLen = 0;
	}
}

bool CSerialPacket::AssignData(eSerialSource src, eSerialDevice Device, const CString sLineName, const TCHAR * pData, UINT Len, int nEncoding)
{
	bool value = false;
	if ( m_nDataLen <= COM_BUF_SIZE )
	{
		m_eDeviceType = Device;
		m_src = src;
		try 
		{
			CString str = StdCommDlg::CCommItem::Encode (CString (pData), nEncoding);

			m_nDataLen = str.GetLength (); //Len;

			if (m_nDataLen > COM_BUF_SIZE) {
				delete [] m_pData;
				m_pData = new TCHAR [m_nDataLen + 1];
				memset (m_pData, 0, m_nDataLen + 1);
			}

			_tcsncpy (m_pData, str, str.GetLength ());

			//TRACEF (str);
			//TRACEF (FoxjetDatabase::Format (pData, Len));
			//TRACEF (FoxjetDatabase::Format (m_pData, m_nDataLen));

			m_sLineName = sLineName;
			value = true;

			//if (!nEncoding) 
			{ 
				// re-assign line, if specified
				CLongArray v = Count (m_pData, m_nDataLen, TOKEN_LINEID);

				if (v.GetSize ()) {
					CString str;
					int i = v [0] + 1;

					str += (TCHAR)m_pData [i++];
					str += (TCHAR)m_pData [i++];

					int nLine = _ttoi (str) - 1;

					if (nLine >= 0 && nLine < ::vLines.GetSize ()) {
						m_sLineName = ::vLines [nLine].m_strName;
						m_bOverride = true;
					}

					_tcsncpy (m_pData, &m_pData [i], m_nDataLen - i);
					m_pData [m_nDataLen - i] = 0;

					if (m_pData [m_nDataLen - i - 1] == '\r') {
						m_pData [m_nDataLen - i - 1] = 0;
						m_nDataLen--;
					}
				}

				m_vSegments.RemoveAll ();

				m_vSegments = MergeAcending (m_vSegments, Count (m_pData, m_nDataLen, SEG1));
				m_vSegments = MergeAcending (m_vSegments, Count (m_pData, m_nDataLen, SEG2));
				m_vSegments = MergeAcending (m_vSegments, Count (m_pData, m_nDataLen, SEG3));
				m_vSegments = MergeAcending (m_vSegments, Count (m_pData, m_nDataLen, SEG4));
				m_vSegments = MergeAcending (m_vSegments, Count (m_pData, m_nDataLen, SEG5));
			}

			if (m_vSegments.GetSize ()) {
				if (m_vSegments [0] != 0)
					m_vSegments.InsertAt (0, (ULONG)0);
			}
			else
				m_vSegments.InsertAt (0, (ULONG)0);
		}
		catch ( CMemoryException *e ) { HANDLEEXCEPTION (e); }
	}
	return value;
}

CLongArray CSerialPacket::Count (LPCTSTR pData, ULONG lLen, TCHAR c)
{
	CLongArray v;

	for (ULONG i = 0; i < lLen; i++) {
		TCHAR cCur = pData [i];

		if (cCur == c) 
			v.Add (i);
	}

	return v;
}

int CSerialPacket::CountSegments () const
{
	return m_vSegments.GetSize ();
}

CSerialPacket * CSerialPacket::GetSegment (int nIndex) const
{
	if (nIndex < m_vSegments.GetSize ()) {
		int nStart = m_vSegments [nIndex];
		int nEnd = nStart;
		CSerialPacket * p = new CSerialPacket (m_strPort);
		eSerialDevice dev = NOTUSED;
		CString strLine = m_sLineName;
		int nDiff = 0;

		if ((nIndex + 1) < m_vSegments.GetSize ()) 
			nEnd = m_vSegments [nIndex + 1];
		else 
			nEnd = m_nDataLen;

		switch (m_pData [nStart]) {
		case SEG1:	dev = HANDSCANNER;		break;
		case SEG2:	dev = FIXEDSCANNER;		break;
		case SEG3:	dev = REMOTEDEVICE;		break;
		case SEG4:	dev = HOSTINTERFACE;	break;
		case SEG5:	dev = SW0858_DBSTART;	break;
		}

		if (dev != NOTUSED)
			nStart++;
		else
			dev = m_eDeviceType;

		{ // assign line, if specified
			LPCTSTR pData = &m_pData [nStart];
			ULONG lLen = nEnd - nStart;
			CLongArray v = Count (pData, lLen, TOKEN_LINEID);

			if (v.GetSize ()) {
				CString str;
				int i = v [0] + 1;

				str += (TCHAR)pData [i++];
				str += (TCHAR)pData [i++];

				int nLine = _ttoi (str) - 1;
				nDiff = (ULONG)&pData [i] - (ULONG)pData;

				//nStart	+= nDiff;
				//lLen	-= nDiff;

				if (nLine >= 0 && nLine < ::vLines.GetSize ()) 
					strLine = ::vLines [nLine].m_strName;
			}
		}

		LPCTSTR pData = &m_pData [nStart + nDiff];
		ULONG lLen = (nEnd - nStart) - nDiff;

		p->AssignData (m_src, dev, strLine, pData, lLen, 0);
		p->m_bOverride = m_bOverride;

		return p;
	}

	return NULL;
}

void CSerialPacket::Init ()
{
	::vLines.RemoveAll ();
	GetPrinterLines (theApp.m_Database, GetPrinterID (theApp.m_Database), ::vLines); //GetLineRecords (theApp.m_Database, ::vLines);
}

ULONG CSerialPacket::GetLineID (UINT nIndex) // sw0849
{
	if (nIndex >= 0 && nIndex < (UINT)::vLines.GetSize ()) 
		return ::vLines [nIndex].m_lID;

	return FoxjetDatabase::NOTFOUND;
}

bool CSerialPacket::IsSw0849 (LPCTSTR pData, ULONG lLen) // sw0849
{
	return 
		Find (pData, lLen, 28) != NULL ||
		Find (pData, lLen, 29) != NULL ||
		Find (pData, lLen, 30) != NULL;
}

bool CSerialPacket::IsSw0849Ignore (LPCTSTR pData, ULONG lLen) // sw0849
{
	for (ULONG i = 0; i < lLen; i++)
		if (pData [i] != '\r')
			return false;

	return true;
}

bool CSerialPacket::IsUrl () const
{
	CString str (m_pData);

	return (str.Find (_T ("?")) != -1) && (str.Find (_T ("=")) != -1);// && (str.Find (_T ("&")) != -1);
}
