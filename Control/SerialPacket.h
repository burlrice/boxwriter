// SerialPacket.h: interface for the CSerialPacket class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERIALPACKET_H__2DE2FEB7_52C3_4FF2_9E20_3DA625461E63__INCLUDED_)
#define AFX_SERIALPACKET_H__2DE2FEB7_52C3_4FF2_9E20_3DA625461E63__INCLUDED_

#include "TypeDefs.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CopyArray.h"

class CSerialPacket  
{
public:
	typedef enum 
	{
		TOKEN_LINEID	= 28,
		TOKEN_DYNID		= 29,
		TOKEN_DATA		= 30,
	} TOKEN;

	CSerialPacket (const CString & strPort);
	CSerialPacket (const CSerialPacket & rhs);
	CSerialPacket & operator = (const CSerialPacket & rhs);
	virtual ~CSerialPacket();

	bool IsUrl () const;

	static bool IsSw0849 (LPCTSTR pData, ULONG lLen);
	static bool IsSw0849Ignore (LPCTSTR pData, ULONG lLen);

	bool AssignData (eSerialSource src, eSerialDevice Device, const CString sLineName, LPCTSTR pData, UINT Len, int nEncoding);

	int CountSegments () const;
	CSerialPacket * GetSegment (int nIndex) const;

	bool IsOverrideLine () const { return m_bOverride; }
	
	static FoxjetCommon::CLongArray Count (LPCTSTR pData, ULONG lLen, TCHAR c);
	static void Init ();
	static ULONG GetLineID (UINT nIndex);

	static bool IsToken (TCHAR c) { return c == TOKEN_LINEID || c == TOKEN_DYNID || c == TOKEN_DATA; }


	TCHAR * m_pData;
	UINT m_nDataLen;
	eSerialDevice m_eDeviceType;
	eSerialSource m_src;
	CString m_sLineName;
	LARGE_INTEGER m_counter;
	CString m_strPort;

protected:
	bool m_bOverride;
	FoxjetCommon::CLongArray m_vSegments;
};

#endif // !defined(AFX_SERIALPACKET_H__2DE2FEB7_52C3_4FF2_9E20_3DA625461E63__INCLUDED_)
