// ControlView.cpp : implementation of the CControlView class
//

#include "stdafx.h"
#include <Afxtempl.h>
#include <AFXPRIV.H>
#include "Resource.h"
#include <Windows.h>
#include <string>
#include <algorithm>

#ifdef __WINPRINTER__
//	#include "WINABLE.H"
	#include "DbStartTaskDlg.h"
	#include "DateTimeElement.h"
#endif
 
#include "Defines.h"
#include "TypeDefs.h"
#include "ControlDoc.h"
#include "Control.h"
#include "ImageView.h"
#include "ControlView.h"
#include "Head.h"
#include "HpHead.h"
#include "MainFrm.h"
#include "StartTaskDlg.h"

#include "LinePromptDlg.h"
#include "ProgressDlg.h"
#include "ProdLine.h"
#include "Debug.h"
#include "Color.h"
#include "Utils.h"
#include "ListCtrlImp.h"
#include "Utils.h"
//#include "UserElement.h"
#include "AppVer.h"
#include "WinMsg.h"
#include "Export.h"
#include "Parse.h"
#include "FieldDefs.h"
#include "Serialize.h"
#include "ComPropertiesDlg.h"
#include "Comm32.h"
#include "Export.h"
#include "IvPhc.h"
#include "DevGuids.h"
#include "HeadConfigDlg.h"
#include "TemplExt.h"
#include "Registry.h"
#include "Coord.h"
#include "ScanReportDlg.h"
#include "PrintReportDlg.h"
#include "StartupDlg.h"
#include "ximage.h"
#include "Parse.h"
#include "StrobeDlg.h"
#include "HeadLimitedConfigDlg.h"
#include "System.h"
#include "TemplExt.h"
#include "StartupDlg.h"
#include "LocalDbDlg.h"
#include "Extern.h"
#include "IpElements.h"
#include "fj_system.h"
#include "fj_dyntext.h"
#include "fj_barcode.h"
#include "Registry.h"
#include "FileExt.h"

#ifdef __WINPRINTER__
	#include "UserElement.h"
	#include "CountElement.h"
	#include "DatabaseElement.h"
	#include "LocalHead.h"
	#include "BitmapElement.h"
#endif

#ifdef __IPPRINTER__
	#include "SyncDlg.h"
	#include "RemoteHead.h"
	#include "FirmwareDlg.h"
#endif

#if __CUSTOM__ == __SW0816__
	#include "OdbcDatabase.h"
	#include "OdbcRecordset.h"
	#include "DatabaseElement.h"
#endif

#define LBL_LINESPEED 100

using namespace FoxjetDatabase;
using namespace Foxjet3d;
using Head::CHead;
using Head::CHeadInfo;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#pragma data_seg( "local_db" )
	namespace LocalDB
	{
		extern CRITICAL_SECTION cs;
		extern bool bEnabled;
		extern CArray <CTime, CTime &> vTrigger;
		extern TCHAR szLocalBMP		[MAX_PATH];
		extern TCHAR szNetworkBMP	[MAX_PATH];
	};
#pragma

static NEXT::PORT_TYPE * g_pNEXTPortType = NULL;

////////////////////////////////////////////////////////////////////////////////
const int TIMER_REFRESH	= 99;

////////////////////////////////////////////////////////////////////////////////

#define MENUITEM_STD		(0)
#define MENUITEM_NOSECURITY	(-1)
#define MENUITEM_SUBMENU	(-2)

struct
{
	UINT	m_nResID;
	UINT	m_nCmdID;
	UINT	m_nBitmapID;
	int		m_nType;
}
static const mapMenu [] = 
{
	{ IDS_CONFIGURE_DATABASE,				ID_CONFIGURE_DATABASE,				IDB_DBSTART,			MENUITEM_STD,			},
	{ IDS_CFG_PRINTHEAD,					IDM_CFG_PRINTHEAD,					-1,						MENUITEM_STD,			},
	{ IDS_CFG_SYS_LINE,						IDM_CFG_SYS_LINE,					-1,						MENUITEM_STD,			},
	{ IDS_CONFIGURE_SERIALDATAFORMAT,		ID_CONFIGURE_SERIALDATAFORMAT,		-1,						MENUITEM_STD,			},
	{ IDS_SYSTEM,							IDM_SYSTEM,							-1,						MENUITEM_SUBMENU,		},
	//{ IDS_CFG_BARCODE_SETTINGS,			IDM_CFG_BARCODE_SETTINGS,			-1,						MENUITEM_STD,			},
	//{ IDS_CFG_DATE_TIME_CODES,			IDM_CFG_DATE_TIME_CODES,			-1,						MENUITEM_STD,			},
	//{ IDS_CFG_SHIFTCODES,					IDM_CFG_SHIFTCODES,					-1,						MENUITEM_STD,			},
	
	#ifdef __IPPRINTER__
	{ IDS_CFG_DYNAMICDATA,					IDM_CFG_DYNAMICDATA,				-1,						MENUITEM_STD,			},
	#endif

	{ IDS_CFG_GENERAL,						IDM_CFG_GENERAL,					-1,						MENUITEM_STD,			},
	{ IDS_CONFIGURE_SYSTEM_OUTPUTTABLE,		ID_CONFIGURE_SYSTEM_OUTPUTTABLE,	-1,						MENUITEM_STD,			},
	{ IDS_CONFIGURE_SYSTEM_STROBE,			ID_CONFIGURE_SYSTEM_STROBE,			-1,						MENUITEM_STD,			},
	{ IDS_VIEW,								IDM_VIEW,							-1,						MENUITEM_SUBMENU,		},
	{ IDS_VIEW_PRINT_REPORT,				IDM_VIEW_PRINT_REPORT,				-1,						MENUITEM_STD,			},
	{ IDS_VIEW_SCAN_REPORT,					IDM_VIEW_SCAN_REPORT,				-1,						MENUITEM_STD,			},
	{ IDS_SECURITY,							IDM_SECURITY,						-1,						MENUITEM_SUBMENU,		},
	{ IDS_CFG_USER,							IDM_CFG_USER,						-1,						MENUITEM_STD,			},
	{ IDS_CFG_GROUP_OPTIONS,				IDM_CFG_GROUP_OPTIONS,				-1,						MENUITEM_STD,			},
	{ IDS_HELP,								IDM_HELP,							-1,						MENUITEM_NOSECURITY,	},
	{ IDS_APP_EXIT,							ID_APP_EXIT,						-1,						MENUITEM_STD,			},
	{ IDS_APP_ABOUT,						ID_APP_ABOUT,						-1,						MENUITEM_NOSECURITY,	},
	{ IDS_HELP_TRANSLATE,					ID_HELP_TRANSLATE,					-1,						MENUITEM_STD,			},
	{ IDS_OPER_TEST,						IDM_OPER_TEST,						IDB_TESTPATTERN,		IDM_OPER_START,			},

	{ IDS_IMPORT,							IDM_IMPORT,							-1,						MENUITEM_STD,			},
	{ IDS_EXPORT,							IDM_EXPORT,							-1,						MENUITEM_STD,			},

	{ IDS_OPER_START,						IDM_OPER_START,						-1,						MENUITEM_STD,			},
	{ IDS_OPER_STOP,						IDM_OPER_STOP,						-1,						MENUITEM_STD,			},
	{ IDS_OPER_EDIT,						IDM_OPER_EDIT,						-1,						MENUITEM_STD,			},
};

static int Find (ULONG lID, CArray <FoxjetDatabase::OPTIONSSTRUCT, FoxjetDatabase::OPTIONSSTRUCT &> & v)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i].m_lID == lID)
			return i;

	return -1;
}

////////////////////////////////////////////////////////////////////////////////

static CArray <LINESTRUCT, LINESTRUCT &> vLines;

#pragma data_seg( "ping" )
	namespace Ping
	{
		extern CRITICAL_SECTION csPing;
		extern TCHAR szPing [MAX_PATH];
		extern FoxjetCommon::CStrobeItem errorPing;
		extern bool bTrigger;
		extern bool bEnabled;
	};
#pragma

////////////////////////////////////////////////////////////////////////////////

CString CControlView::CPhotocellCount::ToString ()
{
	CStringArray v;

	v.Add (FoxjetDatabase::ToString (m_count.m_lCount));
	v.Add (FoxjetDatabase::ToString (m_count.m_lPalletCount));
	v.Add (FoxjetDatabase::ToString (m_count.m_lPalletMin));
	v.Add (FoxjetDatabase::ToString (m_count.m_lPalletMax));
	v.Add (FoxjetDatabase::ToString (m_count.m_lUnitsPerPallet));
	v.Add (m_tm.Format (_T ("%c")));
	v.Add (FoxjetDatabase::ToString (m_lLineID));
	v.Add (m_strLine);
	v.Add (FoxjetDatabase::ToString (CalcChecksum (v)));

	return FoxjetDatabase::ToString (v);
}

bool CControlView::CPhotocellCount::FromString (const CString & str)
{
	CStringArray v;

	FoxjetDatabase::FromString (str, v);

	if (v.GetSize () == 5) {
		int i = 0;

		m_count.m_lCount			= FoxjetCommon::strtoul64 (v [i++]);
		m_count.m_lPalletCount		= 0;
		m_count.m_lPalletMin		= 0;
		m_count.m_lPalletMax		= 0;
		m_count.m_lUnitsPerPallet	= 0;
		
		if (!m_tm.ParseDateTime (v [i++]))
			return false;

		m_lLineID	= _ttol (v [i++]);
		m_strLine	= v [i++];

		ULONG lParse = _ttol (v [i++]);

		v.RemoveAt (i - 1);
		ULONG lCalc = CalcChecksum (v);

		return (lCalc == lParse) ? true : false;
	}
	else if (v.GetSize () == 9) {
		int i = 0;

		m_count.m_lCount			= FoxjetCommon::strtoul64 (v [i++]);
		m_count.m_lPalletCount		= FoxjetCommon::strtoul64 (v [i++]);
		m_count.m_lPalletMin		= FoxjetCommon::strtoul64 (v [i++]);
		m_count.m_lPalletMax		= FoxjetCommon::strtoul64 (v [i++]);
		m_count.m_lUnitsPerPallet	= FoxjetCommon::strtoul64 (v [i++]);
		
		if (!m_tm.ParseDateTime (v [i++]))
			return false;

		m_lLineID	= _ttol (v [i++]);
		m_strLine	= v [i++];

		ULONG lParse = _ttol (v [i++]);

		v.RemoveAt (i - 1);
		ULONG lCalc = CalcChecksum (v);

		return (lCalc == lParse) ? true : false;
	}

	return false;
}

ULONG CControlView::CPhotocellCount::CalcChecksum (const CStringArray & v)
{
	ULONG lChecksum = 0;

	for (int i = 0; i < v.GetSize (); i++) {
		const CString & str = v [i];

		for (int j = 0; j < str.GetLength (); j++) {
			TCHAR c = str [j];

			lChecksum += (c - ' ');
		}
	}

	lChecksum = lChecksum % 47;

	return lChecksum;
}

////////////////////////////////////////////////////////////////////////////////
void InsertAscendingByCard (CArray <HEADSTRUCT, HEADSTRUCT &> & v, HEADSTRUCT & h)
{
	ULONG lInsert = _tcstoul (h.m_strUID, NULL, 16);

	for (int i = 0; i < v.GetSize (); i++) {
		ULONG lCur = _tcstoul (v [i].m_strUID, NULL, 16);
	
		if (lCur > lInsert) {
			v.InsertAt (i, h);
			return;
		}
	}

	v.Add (h);
}

////////////////////////////////////////////////////////////////////////////////
using namespace ControlView;

const DWORD CComm::m_dwTimeout = 1000;
int CComm::m_nInstances = 0;

CComm::CComm ()
:	m_hThread (NULL),
	m_hComm (NULL),
	m_dwThreadID (0),
	m_hControlWnd (NULL),
	m_cs (_T ("CComm::CComm:") + FoxjetDatabase::ToString (++m_nInstances)),
	FoxjetCommon::StdCommDlg::CCommItem ()
{
}

CComm::CComm (const CComm & rhs)
:	m_hThread		(rhs.m_hThread),
	m_hComm			(rhs.m_hComm),
	m_dwThreadID	(rhs.m_dwThreadID),
	m_hControlWnd	(rhs.m_hControlWnd),
	m_cs (_T ("CComm::CComm:") + FoxjetDatabase::ToString (++m_nInstances)),
	FoxjetCommon::StdCommDlg::CCommItem (rhs)
{
}

CComm & CComm::operator = (const CComm & rhs)
{
	this->FoxjetCommon::StdCommDlg::CCommItem::operator = (rhs);

	if (this != &rhs) {
		m_hThread		= rhs.m_hThread;
		m_hComm			= rhs.m_hComm;
		m_dwThreadID	= rhs.m_dwThreadID;
		m_hControlWnd	= rhs.m_hControlWnd;
	}

	return * this;
}

CComm::~CComm ()
{
}

////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CControlView

IMPLEMENT_DYNCREATE(CControlView, CFormView)

BEGIN_MESSAGE_MAP(CControlView, CFormView)
	//{{AFX_MSG_MAP(CControlView)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_WM_DRAWITEM()
	ON_BN_CLICKED(BTN_STROBE, OnCancelStrobe)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_COMMAND(ID_VIEW_REFRESHPREVIEW, OnViewRefreshpreview)
	ON_WM_MEASUREITEM()
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP

	ON_MESSAGE (WM_HEAD_CONNECT, OnHeadConnect)
	ON_MESSAGE (WM_HEAD_DISCONNECT, OnHeadDisconnect)
	ON_MESSAGE (WM_HEAD_DELETE, OnHeadDelete)
	ON_MESSAGE (WM_HEAD_STATUS_UPDATE, OnHeadStatusUpdate)
	ON_MESSAGE (WM_SYNC_HEAD_DATA, OnSyncHeadData)
//	ON_COMMAND (IDM_HEAD_UPDATE_FONTS, OnUpdateFonts)
	ON_MESSAGE (WM_INITIAL_STARTUP, OnInitialStartup)
	ON_MESSAGE (WM_HEAD_PROCESSING, OnHeadProcessing)
	ON_MESSAGE (WM_HEAD_SERIAL_DATA, OnHeadSerialData)
	ON_MESSAGE (WM_AUX_SERIAL_DATA,	OnAuxSerialData)
	ON_MESSAGE (WM_AUX_PS2_DATA, OnAuxPS2Data)
	ON_MESSAGE (WM_AUX_CONNECT_STATE_CHANGED, OnAuxConnectStateChanged)
	ON_MESSAGE (WM_AUX_DIGITAL_INPUT_DATA, OnAuxDigitalInputs)
	ON_MESSAGE (WM_IDLE_TASK, OnIdleTask)
	ON_MESSAGE (WM_RESUME_TASK, OnResumeTask)
	ON_MESSAGE (WM_CALCPRODUCTLEN, OnCalcProductLen)
	ON_MESSAGE (WM_HEAD_OBSOLETEFIRMWARE, OnHeadObsoleteFirmware)
	ON_MESSAGE (WM_UPDATE_PREVIEW, OnUpdatePreview)
	ON_MESSAGE (WM_NEXT_TASK_START, OnNEXTTaskStart)
	ON_MESSAGE (WM_NEXT_SET_DYNAMIC_INDEX, OnNEXTSetDynamicIndex)

	ON_MESSAGE (WM_SOCKET_START_TASK,			OnSocketStartTask)
	ON_MESSAGE (WM_SOCKET_START_TASK_DATABASE,	OnSocketStartTaskDatabase)
	ON_MESSAGE (WM_SOCKET_LOAD_TASK,			OnSocketLoadTask)
	ON_MESSAGE (WM_SOCKET_STOP_TASK,			OnSocketStopTask)
	ON_MESSAGE (WM_SOCKET_IDLE_TASK,			OnSocketIdleTask)
	ON_MESSAGE (WM_SOCKET_RESUME_TASK,			OnSocketResumeTask)
	ON_MESSAGE (WM_SOCKET_GET_USER_ELEMENTS,	OnSocketGetUserElements)
	ON_MESSAGE (WM_SOCKET_SET_USER_ELEMENTS,	OnSocketSetUserElements)
	ON_MESSAGE (WM_SOCKET_SET_ERROR_SOCKET,		OnSocketSetErrorSocket)
	ON_MESSAGE (WM_SOCKET_GET_CURRENT_TASK,		OnSocketGetCurrentTask)
	ON_MESSAGE (WM_SOCKET_GET_CURRENT_STATE,	OnSocketGetCurrentState)
	ON_MESSAGE (WM_SOCKET_GET_COUNT,			OnSocketGetCount)
	ON_MESSAGE (WM_SOCKET_SET_COUNT,			OnSocketSetCount)
	ON_MESSAGE (WM_SOCKET_CLOSE,				OnSocketClose)
	ON_MESSAGE (WM_SOCKET_GET_HEADS,			OnSocketGetHeads)
	ON_MESSAGE (WM_SOCKET_GET_ELEMENTS,			OnSocketGetElements)
	ON_MESSAGE (WM_SOCKET_DELETE_ELEMENT,		OnSocketDeleteElement)
	ON_MESSAGE (WM_SOCKET_ADD_ELEMENT,			OnSocketAddElement)
	ON_MESSAGE (WM_SOCKET_UPDATE_ELEMENT,		OnSocketUpdateElement)
	ON_MESSAGE (WM_SOCKET_DB_BUILD_STRINGS,		OnSocketDbBuild)
	ON_MESSAGE (WM_SOCKET_DB_GET_NEXT_STRING,	OnSocketDbGetNext)
	ON_MESSAGE (WM_SOCKET_DB_IS_READY,			OnSocketDbIsReady)
	ON_MESSAGE (WM_SOCKET_DB_BEGIN_UPDATE,		OnSocketDbBeginUpdate)
	ON_MESSAGE (WM_SOCKET_DB_END_UPDATE,		OnSocketDbEndUpdate)
	ON_MESSAGE (WM_SET_SOCKET_ERROR_STATE,		OnSocketSetErrorState)
	ON_MESSAGE (WM_CHECKHEADERRORS,				OnCheckHeadErrors)
	ON_MESSAGE (WM_LINE_DO_AMS,					OnDoAms)
	ON_MESSAGE (WM_LINE_IS_WAITING,				OnIsWaiting)
	ON_MESSAGE (WM_LINE_SET_WAITING,			OnSetWaiting)
	ON_MESSAGE (WM_UPDATE_IMAGE,				OnUpdateImage) 
	ON_MESSAGE (WM_HEAD_ERROR_CLEARED,			OnHeadErrorCleared) 
	ON_MESSAGE (WM_SOCKET_REFRESH_PREVIEW,		OnSocketRefreshPreview)
	ON_MESSAGE (WM_SOCKET_CONNECT_DB,			OnSocketConnectDb)
	ON_MESSAGE (WM_HOST_PACKET_SETSERIAL,		OnSocketSetSerial)
	ON_MESSAGE (WM_HOST_PACKET_GETSERIAL,		OnSocketGetSerial)
	ON_MESSAGE (WM_HOST_PACKET_GET_STROBE,		OnSocketGetStrobe)
	ON_MESSAGE (WM_SOCKET_GET_COUNT_MAX,		OnSocketGetCountMax)
	ON_MESSAGE (WM_SOCKET_SET_COUNT_MAX,		OnSocketSetCountMax)
	ON_MESSAGE (WM_HOST_RECIEVE_INK_CODE,		OnSocketRecieveInkCode)

	ON_MESSAGE (WM_SET_HEAD_ERROR,				OnSetHeadError)
	ON_MESSAGE (WM_SET_HEAD_WARNING,			OnSetHeadWarning)
	ON_MESSAGE (WM_SET_STROBE,					OnSetStrobe)
	ON_MESSAGE (WM_REFRESH_PREVIEW,				OnRefreshPreview)
	ON_MESSAGE (WM_AUX_KEEP_ALIVE,				OnKeepAlive)

	ON_MESSAGE (WM_SOCKET_PACKET_PRINTER_EOP,					OnSocketPrinterEOP)
	ON_MESSAGE (WM_SOCKET_PACKET_PRINTER_SOP,					OnSocketPrinterSOP)
	ON_MESSAGE (WM_SOCKET_PACKET_PRINTER_HEAD_STATE_CHANGE,		OnSocketPrinterStateChange)

	ON_MESSAGE (WM_HEAD_CONNECTION,				OnHeadConnectionStateChanged)
	ON_MESSAGE (WM_RESTART_TASK,				OnRestartTask)

	ON_MESSAGE (WM_TRIGGER_WDT,					OnTriggerWDT)
	ON_MESSAGE (WM_POSTPHOTOCELL,				OnPostPhotocell)
	ON_MESSAGE (WM_XMLDATA,						OnXmlData)
	ON_MESSAGE (WM_SAVEXMLDATA,					OnSaveXmlData)

	ON_MESSAGE (WM_PING_STATUS,					OnPingStatus)

	ON_BN_CLICKED(CHK_PRINT1,					OnPhotocellSim1)
	ON_BN_CLICKED(CHK_PRINT2,					OnPhotocellSim2)

	ON_MESSAGE (WM_GENIMAGE_DEBUG,				OnGenImageDebug)

	ON_MESSAGE (WM_COLORS_CHANGED,				OnColorsChanged)

	ON_MESSAGE (WM_DIAGNOSTIC_DATA,				OnDiagnosticData)
	ON_MESSAGE (WM_SOCKET_SEND,					OnSocketSend)

	ON_MESSAGE (WM_SQL,							OnSql)
	ON_MESSAGE (WM_NEXT_SET_BCPARAMS,			OnSetNEXTBcParams)
	ON_MESSAGE (WM_NEXT_GET_BCPARAMS,			OnGetNEXTBcParams)
	ON_MESSAGE (WM_NEXT_SET_DMPARAMS,			OnSetNEXTDmParams)
	ON_MESSAGE (WM_NEXT_GET_DMPARAMS,			OnGetNEXTDmParams)
	ON_MESSAGE (WM_NEXT_GET_TASK_NAMES,			OnGetTaskNames)
	ON_MESSAGE (WM_NEXT_GET_STROBE,				OnGetNextStrobe)
	ON_MESSAGE (WM_NEXT_SET_STROBE,				OnSetNextStrobe)
	ON_MESSAGE (WM_NEXT_SET_SYSTEM_PARAMS,		OnSetNEXTSystemParams)
	ON_MESSAGE (WM_NEXT_GET_SYSTEM_PARAMS,		OnGetNEXTSystemParams)
	ON_MESSAGE (WM_REFRESH_IMAGE,				OnRefreshImage)
	ON_MESSAGE (WM_NEXT_ADD_TASK,				OnAddTask)
	ON_MESSAGE (WM_NEXT_DELETE_TASK,			OnDeleteTask)
	ON_MESSAGE (WM_GET_BOX_ID,					OnGetBoxID)
	ON_MESSAGE (WM_NEXT_SET_DYNAMIC_DATA,		OnSetNextDynamicData)
	ON_MESSAGE (WM_NEXT_SET_SERIAL_PORT,		OnSetNextPort)
	ON_MESSAGE (WM_NEXT_REFINE_CONFIG,			OnNEXTRefineConfig)
	ON_MESSAGE (WM_NEXT_INIT_FONTS,				OnNEXTInitFonts)
	ON_MESSAGE (WM_NEXT_EDIT_SESSION,			OnNEXTEditSession)
	ON_MESSAGE (WM_SET_PRINT_DRIVER,			OnNEXTSetPrintDriver)
	ON_MESSAGE (WM_INK_USAGE,					OnInkUsage)
	ON_MESSAGE (WM_INK_USAGE_WARNING,			OnInkUsageWarning)
	ON_MESSAGE (WM_INK_CODE_CHANGE,				OnInkCodeChange)
	ON_MESSAGE (WM_INK_CODE_CHECK_STATE,		OnCheckInkCodeState)
	ON_MESSAGE (WM_GET_INK_CODES,				OnGetInkCodes)
	ON_MESSAGE (WM_LOG_INK_CODE_REPORT,			OnLogInkCodeReport)
	ON_MESSAGE (WM_SET_WARNING_MESSAGES,		OnSetHeadWarnings)
	ON_MESSAGE (WM_INK_CODE_ACCEPTED,			OnInkCodeAccepted)
	ON_MESSAGE (WM_DOWNLOAD_SERIAL_DATA,		OnDownloadSerialData)

	ON_BN_CLICKED(BTN_START, OnStart)
	ON_BN_CLICKED(BTN_IDLE, OnIdle)
	ON_BN_CLICKED(BTN_STOP, OnStop)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_BN_CLICKED(BTN_LOGIN, OnLogin)
	ON_BN_CLICKED(BTN_CONFIG, OnConfig)
	ON_BN_CLICKED(BTN_INFO, OnInfo)
	ON_BN_CLICKED(BTN_LIMITED_CONFIG, OnLimitedConfig)
	ON_BN_CLICKED(BTN_COUNT, OnCounts)
	ON_BN_CLICKED(BTN_USER, OnUserData)
	ON_BN_CLICKED(BTN_EXIT, OnExit)
	ON_BN_CLICKED(BTN_BOX, OnBox)
	ON_BN_CLICKED(BTN_ZOOMFIT, OnZoomFit)
	ON_BN_CLICKED(BTN_ZOOMIN, OnZoomIn)
	ON_BN_CLICKED(BTN_ZOOMOUT, OnZoomOut)
	ON_BN_CLICKED(BTN_REPORTS, OnReports)

	ON_COMMAND (IDM_LOGIN, OnUpdateLogin)

	//ON_CONTROL_RANGE(BN_CLICKED, BTN_LINE1, BTN_LINE4, OnLine)

	ON_NOTIFY(TCN_SELCHANGE, TAB_PREVIEW, OnChangePanelTab)
	ON_NOTIFY(TCN_SELCHANGE, TAB_LINE, OnChangeLineTab)
	ON_NOTIFY(NM_CLICK, IDC_GROUP_TAB, OnClickProductLineTab)

	//ON_MESSAGE (WM_CONTROL_VIEW_GET_PROD_LINES,				OnGetProdLines)
	//ON_MESSAGE (WM_CONTROL_VIEW_GET_ACTIVE_HEADS,			OnGetActiveHeads)
	//ON_MESSAGE (WM_CONTROL_VIEW_GET_PROD_LINE_ID,			OnGetProdLineID)
	//ON_MESSAGE (WM_CONTROL_VIEW_GET_THREAD_BY_UID,			OnGetThreadByUID)
	//ON_MESSAGE (WM_CONTROL_VIEW_GET_PROD_LINE_BY_HEAD_ID,	OnGetProdLineIdByHeadId)
	//ON_MESSAGE (WM_CONTROL_VIEW_LINE_NAME,					OnGetLineName)
	//ON_MESSAGE (WM_CONTROL_VIEW_GET_SERIAL_DATA_PTR,		OnGetSerialDataPtr)
	//ON_MESSAGE (WM_CONTROL_VIEW_GET_SW048_THREAD,			OnGetSw0848Thread)

END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CControlView, CFormView)
    //{{AFX_EVENTSINK_MAP(CControlView)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

/////////////////////////////////////////////////////////////////////////////
// CControlView construction/destruction

CControlView::CControlView()
: 	CFormView(FoxjetDatabase::IsMatrix () ? IDD_CONTROL_FORM_MATRIX : IDD_CONTROL_FORM), 
	m_pServerThread (NULL),
	m_nGreenIndex (-1),
	m_nRedIndex (-1),
	m_nYellowIndex (-1),
	m_nGrayIndex (-1),
	m_bPreview (true),
	m_nTimerStatus (-1),
	m_nTimerTab (-1),
	m_nTimerRefresh (-1),
	m_nTimerTime (-1),
	m_nTimerStartMenu (-1),
	m_pExport (NULL),
	m_pImport (NULL),
	m_lTaskID (0),
	m_bKeyboard (false),
	m_nLineIndex (BTN_LINE1),
	m_nLastLineIndex (-1),
	m_bShowInfo (false),
	m_pdlgInfo (NULL),
	m_hbrDlg (NULL),
	m_hbrDlgDisabled (NULL),
	m_bNetworkConnected (false),
	m_lTimerTab (0),
	m_sizeInit (0, 0),
	m_tmPing (CTime::GetCurrentTime ()),
	m_pNextThread (NULL)
{
	m_CtrlSentinel.m_strName = _T ("CControlView::m_CtrlSentinel");

	//{{AFX_DATA_INIT(CControlView)
	//}}AFX_DATA_INIT

	m_pUdpThread = NULL;
	m_eTaskState = STOPPED;
	m_nDataSyncInterval = 1000 * 5;
	m_bNetIODevConnected = false;
	m_pPreview = NULL;
	m_pProgressDlg = NULL;

	m_fntLarge.CreateFont (45, 0, 0, 0, FW_NORMAL, FALSE, 0, 0,
		DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("Microsoft Sans Serif"));
	m_fntMedium.CreateFont (22, 0, 0, 0, FW_NORMAL, FALSE, 0, 0,
		DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("Microsoft Sans Serif"));
	m_fntSmall.CreateFont (16, 0, 0, 0, FW_BOLD, FALSE, 0, 0,
		DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("Microsoft Sans Serif"));

	::QueryPerformanceFrequency (&m_freq); 
	::QueryPerformanceCounter (&m_counter); 
}

CControlView::~CControlView()
{
	if (m_pExport) {
		delete m_pExport;
		m_pExport = NULL;
	}

	if (m_pImport) {
		delete m_pImport;
		m_pImport = NULL;
	}

	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}

	if (m_hbrDlgDisabled) {
		::DeleteObject (m_hbrDlgDisabled);
		m_hbrDlgDisabled = NULL;
	}
}

void CControlView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CControlView)
	//}}AFX_DATA_MAP

	DDX_Control(pDX, BTN_START,				m_btnStart);
	DDX_Control(pDX, BTN_IDLE,				m_btnIdle);
	DDX_Control(pDX, BTN_STOP,				m_btnStop);
	DDX_Control(pDX, BTN_LOGIN,				m_btnLogin);
	DDX_Control(pDX, BTN_EDIT,				m_btnEdit);
	DDX_Control(pDX, BTN_STOP,				m_btnStop);
	DDX_Control(pDX, BTN_CONFIG,			m_btnConfig);
	DDX_Control(pDX, BTN_INFO,				m_btnInfo);
	DDX_Control(pDX, BTN_COUNT,				m_btnCount);
	DDX_Control(pDX, BTN_USER,				m_btnUser);
	DDX_Control(pDX, BTN_LIMITED_CONFIG,	m_btnLimitedConfig);


	DDX_Control(pDX, BTN_BOX,		m_btnBox);
	DDX_Control(pDX, BTN_EXIT,		m_btnExit);
	DDX_Control(pDX, BTN_REPORTS,	m_btnReport);
	DDX_Control(pDX, BTN_ZOOMFIT,	m_btnZoomFit);
	DDX_Control(pDX, BTN_ZOOMIN,	m_btnZoomIn);
	DDX_Control(pDX, BTN_ZOOMOUT,	m_btnZoomOut);

	DDX_Control(pDX, TAB_LINE,		m_tabLine);
}

/////////////////////////////////////////////////////////////////////////////
// CControlView diagnostics

#ifdef _DEBUG
void CControlView::AssertValid() const
{
	CFormView::AssertValid();
}

void CControlView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CControlDoc* CControlView::GetDocument() // non-debug version is inline
{
	VERIFY(m_pDocument->IsKindOf(RUNTIME_CLASS(CControlDoc)));
	return (CControlDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CControlView message handlers

void CControlView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	LOCK (m_CtrlSentinel);

	CString strStatus = LoadString (IDS_STATUS);
	CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> vDisconnected;

	if ( pSender == this )
		return;

	CControlDoc * pDoc = GetDocument ();
	
	LOCK (pDoc->m_ConnectionListSentinel);

	CMainFrame * pFrame = (CMainFrame *)theApp.m_pMainWnd;

	if (pFrame->IsClosing ())
		return;

	CProdLine * pSelectedLine = pDoc->GetSelectedLine ();

	if (!pSelectedLine)
		return;

	LOCK (pSelectedLine->m_HeadListSentinel);
	
	DWORD dwError = pSelectedLine->HasHeadErrors (vDisconnected);

	dwError &= ~HEADERROR_LOWINK; // sw8686 // don't consider low ink

	if (dwError) 
		strStatus = LoadString (IDS_HEADERRORS);

#ifdef __IPPRINTER__
	if (vDisconnected.GetSize ()) {
		static CTime tmStart = CTime::GetCurrentTime ();
		CTimeSpan tm = CTime::GetCurrentTime () - tmStart;
		const int nPeriod = 2;
		bool bDisplay = (abs (tm.GetTotalSeconds ()) % (nPeriod * 2)) >= nPeriod;

		if (!dwError || bDisplay) {
			strStatus = LoadString (IDS_OFFLINE) + _T (": ");

			for (int i = 0; i < vDisconnected.GetSize (); i++) {
				strStatus += vDisconnected [i].m_strName;

				if ((i + 1) < vDisconnected.GetSize ())
					 strStatus += ", ";
			}
		}
	}
#endif

	#if !defined( _DEBUG )
	if (strStatus != LoadString (IDS_STATUS) && theApp.m_bDemo)
	#endif

	SetDlgItemText (IDC_STATUS, strStatus);

	switch (lHint) 
	{
		case 0:	// Update Group (Production Line) List
		{
			//break;	// Fall through is desired.
		}
		case UPDATE_PREVIEW:
			{
				CPrintHeadList HeadList;
				bool bSet = false;

				pDoc->GetActiveHeads (pSelectedLine->GetName(), HeadList);
				
				POSITION pos = HeadList.GetStartPosition();

				while (pos)  {
					CHead * pHead = NULL;
					DWORD dwKey = 0;
	
					HeadList.GetNextAssoc(pos, dwKey, (void *&)pHead);
					ASSERT (pHead);

					CHeadInfo info = CHead::GetInfo (pHead);

					if (info.IsMaster ()) {
						break;
					}
				}
			}
			break;
		case UPDATE_LINE_CHANGE:	// Based on Group Selected, Update Head List
			{
				/*
				CArray <LINESTRUCT, LINESTRUCT &> v;
				CRect rcLabel [4];
				CButton * pButton [] = 
				{
					(CButton *)GetDlgItem (BTN_LINE1),
					(CButton *)GetDlgItem (BTN_LINE2),
					(CButton *)GetDlgItem (BTN_LINE3),
					(CButton *)GetDlgItem (BTN_LINE4),
				};
				CStatic * pLabel [] = 
				{
					(CStatic *)GetDlgItem (LBL_LINE1),
					(CStatic *)GetDlgItem (LBL_LINE2),
					(CStatic *)GetDlgItem (LBL_LINE3),
					(CStatic *)GetDlgItem (LBL_LINE4),
				};

				ASSERT (pButton [0]);
				ASSERT (pButton [1]);
				ASSERT (pButton [2]);
				ASSERT (pButton [3]);

				ASSERT (pLabel [0]);
				ASSERT (pLabel [1]);
				ASSERT (pLabel [2]);
				ASSERT (pLabel [3]);

				GetPrinterLines (theApp.m_Database, GetPrinterID (theApp.m_Database), v); 

				m_nFontSmall	= 20;
				m_nFontLarge	= 40;

				for (int i = 0; i < ARRAYSIZE (pButton); i++) {
					pLabel [i]->GetWindowRect (&rcLabel [i]);
					pButton [i]->ShowWindow (SW_SHOW);
					pButton [i]->SetWindowPos (NULL, rcLabel [i].left, rcLabel [i].top, rcLabel [i].Width (), rcLabel [i].Height (), SWP_NOZORDER | SWP_SHOWWINDOW);
				}

				if (v.GetSize () == 1) {
					CRect rc = rcLabel [0];

					rc.UnionRect (rc, rcLabel [3]);

					m_nLineIndex = BTN_LINE1;
					pButton [0]->SetWindowPos (NULL, rc.left, rc.top, rc.Width (), rc.Height (), SWP_NOZORDER | SWP_SHOWWINDOW);
					pButton [1]->ShowWindow (SW_HIDE);
					pButton [2]->ShowWindow (SW_HIDE);
					pButton [3]->ShowWindow (SW_HIDE);
				}
				else if (v.GetSize () == 2) {
					CRect rc = rcLabel [0];

					m_nLineIndex = BindTo (m_nLineIndex, BTN_LINE1, BTN_LINE2);
					rc.UnionRect (rc, rcLabel [1]);

					pButton [0]->SetWindowPos (NULL, rc.left, rc.top, rc.Width (), rc.Height (), SWP_NOZORDER | SWP_SHOWWINDOW);

					rc = rcLabel [2];
					rc.UnionRect (rc, rcLabel [3]);
					pButton [1]->SetWindowPos (NULL, rc.left, rc.top, rc.Width (), rc.Height (), SWP_NOZORDER | SWP_SHOWWINDOW);

					pButton [2]->ShowWindow (SW_HIDE);
					pButton [3]->ShowWindow (SW_HIDE);
				}
				else if (v.GetSize () == 3) {
					m_nLineIndex = BindTo (m_nLineIndex, BTN_LINE1, BTN_LINE3);
					pButton [3]->ShowWindow (SW_HIDE);
				}

				GetDocument ()->SetSelectedLine (m_nLineIndex - BTN_LINE1);
				
				for (i = 0; i < ARRAYSIZE (pButton); i++) {
					REPAINT (pButton [i]);
				}
				*/

				Resize ();
			}
			break;
		case UPDATE_HEAD_DISCONNECT:
			{
				CHeadInfo info = pSelectedLine->GetHeadInfo (pHint);
				eHeadStatus s;

				if (m_mapHeadError.Lookup (info.GetThreadID (), s))
					m_mapHeadError.RemoveKey (info.GetThreadID ());

				Resize ();
			}
			break;
		// When the head initializes it should send a WM_HEAD_STATUS_UPDATE with this value as the paramater.
		case UPDATE_HEAD_ACTIVATION:
			{
				CHeadInfo info = pSelectedLine->GetHeadInfo (pHint); 

				Resize ();
				OnSocketPrinterStateChange (info.m_Config.m_HeadSettings.m_lID, 0);
			}
//			break;	// Fall through intended.
		case UPDATE_HEAD_STATUS:
			{
				CHeadInfo info = pSelectedLine->GetHeadInfo (pHint); 

				{
					CStringArray v;
		
					pDoc->GetProductLines (v);
					
					{
						CString strInfoCache, strInfo;
						
						/*
						strInfo.Format (
							_T ("\t\t<CENTER>%s\t\t<CENTER>%s\t\t%s") /* _T ("\t%s") * / _T ("\n")
							_T ("\t<RIGHT>%s\t<CENTER>%s\t<CENTER>%s\t<CENTER>%s\t<CENTER>%s\n"),
							LoadString (IDS_ENCODER_),
							LoadString (IDS_ERRORS),
							LoadString (IDS_PRINTING),
							//LoadString (IDS_STATE),
							LoadString (IDS_COUNT),
							LoadString (IDS_SPEED),
							LoadString (IDS_HV),
							LoadString (IDS_TEMP),
							LoadString (IDS_INK));
						*/

						for (int i = 0; i < v.GetSize (); i++) {
							if (CProdLine * pLine = pDoc->GetProductionLine (v [i])) {
								//strInfo += pLine->GetName () + _T ("\n") + pLine->GetDebugInfo () + _T ("\n");
								strInfo += pLine->GetName () + _T ("\t") + pLine->GetDebugInfo () + _T ("\t");
							}
						}

						//strInfo += LoadString (IDS_BWVER) + _T (": ") + FoxjetCommon::CVersion (FoxjetCommon::verApp).Format ();

						//{ for (int i = 0; i < 10; i++) { CString str; str.Format (_T ("\n%s(%d): %d, TODO: rem"), _T (__FILE__), __LINE__, i); strInfo += str; } }

						m_vLineDebugInfo.Lookup		(-1, strInfoCache);
						m_vLineDebugInfo.SetAt		(-1, strInfo);

						if (strInfo != strInfoCache) {
							if (m_pdlgInfo) 
								m_pdlgInfo->UpdateData (FALSE); //REPAINT (m_pdlgInfo);
						}
					}

					for (int i = 0; i < v.GetSize (); i++) {
						if (CProdLine * pLine = pDoc->GetProductionLine (v [i])) {
							CString strDispCache, strDisp = pLine->GetDisplayInfo ();
							CString strDebugCache, strDebug = pLine->GetDebugInfo ();
							CString strWarningsCache, strWarnings = pLine->GetWarnings (CStringArray ());

							m_vLineDisplayInfo.Lookup	(pLine->GetID (), strDispCache);
							m_vLineDebugInfo.Lookup		(pLine->GetID (), strDebugCache);
							m_vLineWarningsInfo.Lookup	(pLine->GetID (), strWarningsCache);
							
							m_vLineDisplayInfo.SetAt	(pLine->GetID (), strDisp);
							m_vLineDebugInfo.SetAt		(pLine->GetID (), strDebug);
							m_vLineWarningsInfo.SetAt	(pLine->GetID (), strWarnings);

							if (strWarningsCache != strWarnings) { // || strDebugCache != strDebug) {
								/*
								//TRACEF (strDebugCache);
								//TRACEF (strDebug);
								TRACEF (strWarningsCache);
								TRACEF (strWarnings);
								*/
								REPAINT (m_pPreview);
							}
							
							if (strDispCache != strDisp || !pHint) {
								//TRACEF (strDisp);
								//TRACEF (strDebug);

								if (CLineTab * p = (CLineTab *)GetDlgItem (TAB_LINE)) {
									CPrintHeadList list;

									pLine->GetActiveHeads (list);

									for (POSITION pos = list.GetStartPosition(); pos; ) {
										ULONG lKey; 
										CHead * pHead = NULL;

										list.GetNextAssoc (pos, lKey, (void *&)pHead);

										if (pHead) {
											CHeadInfo info = pSelectedLine->GetHeadInfo (pHead); 
											pLine->UpdateHeadErrors (info.GetThreadID (), info.GetErrorStatus ());
										}
									}

									OnChangeProductLine ();
									REPAINT (p);
								}
							}
						}
					}
				}

				if (pSelectedLine->GetName().CompareNoCase (info.GetProductionLine()) == 0) {
					if (info.IsMaster ()) {
						CString str;

						#ifdef __IPPRINTER__
						if (info.m_bRemoteHead)
							if (!info.m_Config.m_HeadSettings.m_bDigiNET)
								str += _T ("[NET]");
						#endif
					}
				}
			}
			break;
	}

	if (m_vObsolete.GetSize ()) {
		CString str = LoadString (IDS_OBSOLETEFIRMWARE);

		for (int i = 0; i < m_vObsolete.GetSize (); i++) {
			CHead * pHead = m_vObsolete [i];

			str += _T ("\n") + CHead::GetInfo (pHead).GetFriendlyName ();
		}

		m_vObsolete.RemoveAll ();
		MsgBox (* this, str, LoadString (IDS_WARNING), MB_ICONERROR);
	}
}

void CControlView::OnChangeLineTab (NMHDR* pNMHDR, LRESULT* pResult) 
{
	TRACEF (_T ("CControlView::OnChangeLineTab"));

	if (CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (TAB_LINE)) {
		CControlDoc * pDoc = GetDocument ();
		int nIndex = pTab->GetCurSel ();

		pDoc->SetSelectedLine (nIndex);
		OnChangeProductLine ();
		OnViewRefreshpreview ();
	}
}

void CControlView::OnChangePanelTab (NMHDR* pNMHDR, LRESULT* pResult) 
{
	CControlDoc * pDoc = GetDocument ();
	CHead * pSelectedHead	= NULL;
	int nSelectedPanel		= -1;
	ULONG lTaskID			= -1;
	CString strPanel;

	m_btnBox.GetWindowText (strPanel);

	TRACEF (_T ("CControlView::OnChangePanelTab: ") + strPanel);

	if (strPanel.GetLength ()) {
		if (CProdLine * pLine = pDoc->GetSelectedLine ()) {
			CPrintHeadList list;

			pLine->GetActiveHeads (list);

			for (POSITION pos = list.GetStartPosition(); pos; ) {
				DWORD dwKey;
				CHead * pHead = NULL;

				list.GetNextAssoc (pos, dwKey, (void *&)pHead);
				ASSERT (pHead);
				CHeadInfo info = CHead::GetInfo (pHead);

				if (info.GetPanelName () == strPanel) {
					pSelectedHead	= pHead;
					nSelectedPanel	= info.GetBoxPanel ();
					lTaskID			= pLine->GetTaskID ();
					break;
				}
			}
		}
	}

	TRACEF (_T ("lTaskID: ") + ToString (lTaskID) + _T (", nSelectedPanel: ") + ToString (nSelectedPanel));
	m_pPreview->SetPreviewPanel (pSelectedHead, lTaskID, nSelectedPanel);

	if (pResult)
		* pResult = 0;
}


void CControlView::OnClickProductLineTab (NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnViewRefreshpreview ();

	if (pResult)
		* pResult = 0;
}

void CControlView::OnChangeProductLine ()
{
	CControlDoc * pDoc = GetDocument();

	if (CProdLine * pLine = pDoc->GetSelectedLine ()) {
		int nIndex = pDoc->GetLineIndex (pLine);
		FoxjetDatabase::WriteProfileString (CControlApp::GetRegKey () + _T ("\\Settings"), _T ("Selected Line"), pLine->GetName ());
		CString strTitle = pLine->GetName ();
		
		if (theApp.m_bDemo)
			strTitle += _T (" ") + LoadString (IDS_DEMO);
	
		pDoc->SetTitle (strTitle);

		{
			TASKSTATE state = pLine->GetTaskState ();
			bool bEnable = state == RUNNING || state == IDLE;

			if (theApp.m_strUsername.GetLength ()) {
				m_btnIdle.EnableWindow (bEnable);
				m_btnStop.EnableWindow (bEnable);
			}

			if (state == IDLE) {
				m_btnIdle.SetBitmap (m_bmpResume);
				m_btnIdle.SetWindowText (LoadString (IDS_RESUME));
			}
			else {
				m_btnIdle.SetBitmap (m_bmpIdle);
				m_btnIdle.SetWindowText (LoadString (IDS_TASK_IDLE));
			}

			if (m_nLastLineIndex != nIndex) {
				CString strPanel;
				CStringArray v;
				CArray <PANELSTRUCT, PANELSTRUCT &> & vPanels = pLine->m_vPanels;
				DBCONNECTION (conn);

				for (int i = vPanels.GetSize () - 1; i >= 0; i--) {
					CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
					
					GetPanelHeads (theApp.m_Database, vPanels [i].m_lID, vHeads);

					if (!vHeads.GetSize ()) 
						vPanels.RemoveAt (i);
				}

				if (vPanels.GetSize ())
					strPanel = vPanels [pLine->m_nPreviewPanel % vPanels.GetSize ()].m_strName; 

				m_btnBox.SetWindowText (strPanel);
				OnChangePanelTab (NULL, NULL);
			}
		}

		m_nLastLineIndex = nIndex;
	}

	theApp.UpdateUI ();
}

void CControlView::OnTimer(UINT nIDEvent) 
{
	static bool flag = false;
	CMainFrame *pFrame = ( CMainFrame *) theApp.m_pMainWnd;

	switch (nIDEvent) {
	case TIMER_TIME:
		{
			
			UINT n [] = 
			{
				LBL_TIME1,
				LBL_TIME2,
				LBL_TIME3,
			};

			for (int i = 0; i < ARRAYSIZE (n); i++) {
				CString strOld, strNew;
			
				#ifdef __WINPRINTER__
				strNew = FoxjetElements::CDateTimeElement::Format (-1, m_strTimeFormat [i], 0, NULL);
				#else
				strNew = CTime::GetCurrentTime ().Format (m_strTimeFormat [i]);
				#endif

				GetDlgItemText (n [i], strOld);

				if (strOld != strNew) {
					if (CWnd * p = GetDlgItem (n [i])) {
						p->SetWindowText (strNew);
						REPAINT (p);
					}
				}
			}

		}
		break;
	case TIMER_TAB:
		{
			m_lTimerTab++;
			
			if (CLineTab * p = (CLineTab *)GetDlgItem (TAB_LINE)) { 
				REPAINT (p);
			}
		}
		break;
	case TIMER_STATUS:
		{
			static UINT nLastBitmap = -1;
			UINT nBitmap = IDB_GRAY;

			if (CButton * p = (CButton *)GetDlgItem (CHK_DBCONNECTION)) {
				CString str;
				CString strFmt = LoadString (IDS_DBCONNECTION) + _T (" (%d)");
				int n = 0;
				int nCount = COdbcDatabase::GetInstanceCount ();

				p->GetWindowText (str);
				_stscanf (str, strFmt, &n);				

				if (n != nCount) {
					COdbcDatabase::TraceInstances ();
					str.Format (strFmt, nCount);

					p->SetWindowText (str);
					p->SetCheck (nCount > 0);
				}
			}

			CProdLine * pLine = GetDocument()->GetSelectedLine();
			if ( pLine ) 
			{
				if (!pLine->m_LineRec.m_bAuxBoxEnabled)
					nBitmap = IDB_GRAY;
				else {
					if ( pLine->IsAuxBoxConnected() )
						nBitmap = IDB_GREEN;
					else
						nBitmap = IDB_RED;
				}
			}

			if (nLastBitmap != nBitmap) {
				theApp.CacheHubState ();
				nLastBitmap = nBitmap;
			}

			CStringList LineList;
			int nStrobe = 0;

			GetDocument()->GetProductLines ( LineList );
			
			while ( !LineList.IsEmpty () )
			{
				pLine = GetDocument()->GetProductionLine (LineList.RemoveHead());
				pLine->UpdateStrobeState (this);
				nStrobe |= pLine->GetDriverStrobeColor ();
			}

			if (IsNetworked ()) {
				static int nLastRed = 0;

				if (Ping::bEnabled) {
					if (!theApp.IsNetworkConnected ()) {
						if (Ping::errorPing.m_green		== STROBESTATE_ON)		
							nStrobe |= GREEN_STROBE;
						if (Ping::errorPing.m_yellow	== STROBESTATE_ON)		
							nStrobe |= YELLOW_STROBE;
						if (Ping::errorPing.m_red		== STROBESTATE_ON)		
							nStrobe |= RED_STROBE;
						if (Ping::errorPing.m_red		== STROBESTATE_FLASH)	
							nStrobe |= ((nLastRed++ % 2) ? RED_STROBE : STROBE_LIGHT_OFF);
					}
				}

			}

			theApp.SetStrobeColor (nStrobe);

			if (CProdLine * pLine = GetDocument ()->GetSelectedLine ()) {
				CPrintHeadList list;

				pLine->GetActiveHeads (list);

				if (!list.GetCount ())
					OnUpdate (NULL, UPDATE_PREVIEW, NULL);

				for (POSITION pos = list.GetStartPosition(); pos; ) {
					DWORD dwKey;
					CHead * pHead = NULL;

					list.GetNextAssoc (pos, dwKey, (void *&)pHead);
					ASSERT (pHead);
					CHeadInfo info = CHead::GetInfo (pHead);
					DWORD dwState = 0;

					SEND_THREAD_MESSAGE (info, TM_GET_ERROR_STATE, &dwState);

					if (dwState) {
						POST_THREAD_MESSAGE (info, TM_INCREMENT_ERROR_INDEX, 0);

	  					while (!PostMessage (WM_HEAD_STATUS_UPDATE, UPDATE_HEAD_STATUS, (LPARAM)pHead))
							::Sleep (0);
					}
				}
			}

		}
		break;
	case TIMER_REFRESH:
		#ifdef __WINPRINTER__
		if (theApp.m_pMainWnd->m_hWnd == ::GetForegroundWindow ()) {
			INPUT key [2] = { 0 };

			key [0].type		= 1;
			key [0].ki.wVk		= 116;
			key [1].type		= 1;
			key [1].ki.wVk		= 116;
			key [1].ki.dwFlags	= 2;

			::SendInput (ARRAYSIZE (key), key, sizeof (INPUT));
			TRACEF ("TIMER_REFRESH");
		}
		break;
		#endif
	case TIMER_STARTMENU:
			{
				if (!theApp.m_bDemo && !FoxjetDatabase::IsMatrix ()) {
					CRect rcWin (0, 0, 0, 0);

					::AfxGetMainWnd ()->GetWindowRect (rcWin);

					if (HWND hwndKeyboard = FoxjetCommon::FindKeyboard ()) {
						if (::IsWindowVisible (hwndKeyboard)) {
							CRect rcKeyboard (0, 0, 0, 0);

							::GetWindowRect (hwndKeyboard, rcKeyboard);

							int nDiff = rcWin.bottom - rcKeyboard.top;//rcKeyboard.top - rcWin.bottom;

							if (nDiff > 1) {
								CSize size (rcWin.Width (), rcKeyboard.top - 1);

								SetWindowPos (NULL, 0, 0, size.cx, size.cy, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
								TRACER (_T ("rcWin.bottom: ") + ToString (rcWin.bottom));
								TRACER (_T ("rcKeyboard.top: ") + ToString (rcKeyboard.top));
								TRACER (_T ("nDiff: ") + ToString (nDiff));
								TRACER (_T ("SetWindowPos: ") + ToString (size.cx) + _T (", ") + ToString (size.cy));

								if (CMainFrame * pFrame = (CMainFrame *)GetParentFrame ()) {
									pFrame->SetWindowPos (NULL, 0, 0, size.cx, size.cy, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);

									//if (!theApp.m_bDemo) 
									{
										pFrame->RecalcLayout ();
										SetScaleToFitSize (size);
										ResizeParentToFit (FALSE);
									}
								}
							}
						}
						else {
							if (m_sizeInit.cy > rcWin.Height ()) {
								pFrame->SetWindowPos (NULL, 0, 0, m_sizeInit.cx, m_sizeInit.cy, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
								pFrame->RecalcLayout ();
								SetScaleToFitSize (m_sizeInit);
								ResizeParentToFit (FALSE);
							}
						}
					}
				}
			}
			break;
	default:
		CFormView::OnTimer(nIDEvent);
	}
}

LRESULT CControlView::OnHeadConnect(WPARAM wParam, LPARAM lParam)
{
#ifdef __IPPRINTER__
	MsgBox (* this, _T ("TODO: rem: CControlView::OnHeadConnect"));
#endif //__IPPRINTER__

	return 1;
}

LRESULT CControlView::OnHeadDelete (WPARAM wParam, LPARAM lParam)
{
	CControlDoc *pDoc = GetDocument();
	CStringArray vLines;
	ULONG lID = (ULONG)lParam;
	bool bResult = false;

	pDoc->GetProductLines (vLines);

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		if (CProdLine * pLine = pDoc->GetProductionLine (vLines [nLine])) {
			CHead * pHead = pLine->GetHeadByID (lID);

			if (pHead) {
				CHeadInfo info = CHead::GetInfo (pHead);
				CEvent event (false, true, _T ("TM_KILLTHREAD: ") + ToString (pHead->m_nThreadID), NULL);

				#ifdef __IPPRINTER__
				pHead->m_bAutoDelete = true;
				#endif //__IPPRINTER__

				if (!pLine->GetName ().CompareNoCase (pDoc->GetSelectedLine()->GetName ())) {
					OnUpdate (NULL, UPDATE_HEAD_DISCONNECT, pHead);
					pDoc->UpdateAllViews (this, UPDATE_HEAD_DISCONNECT);
				}

				//{ CString str; str.Format (_T ("RemoveActiveHead: %s, 0x%p [%s]"), pLine->GetName (), pHead, pHead->GetFriendlyName ()); TRACEF (str); }
				pDoc->RemoveActiveHead (pLine->GetName (), pHead);

				while (!::PostThreadMessage (pHead->m_nThreadID, TM_KILLTHREAD, (WPARAM)(HANDLE)event, 0))
					::Sleep (0);

				DWORD dwWait = ::WaitForSingleObject (event, 5000);

				//if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
				if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
				if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
				if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");

				bResult = dwWait == WAIT_OBJECT_0 ? true : false;
				ASSERT (dwWait == WAIT_OBJECT_0);
			}
		}
	}

	if (HANDLE h = (HANDLE)wParam)
		::SetEvent (h);

	return bResult;
}

LRESULT CControlView::OnHeadDisconnect ( WPARAM wParam, LPARAM lParam )
{
#ifdef _DEBUG
	{
		CString str;
		str.Format (_T ("CControlView::OnHeadDisconnect (%d, %d)"), wParam, lParam);
		TRACEF (str);
	}
#endif

	CControlDoc *pDoc = GetDocument();
	CStringArray vLines;
	ULONG lID = (ULONG)lParam;

	pDoc->GetProductLines (vLines);

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		if (CProdLine * pLine = pDoc->GetProductionLine (vLines [nLine])) {
			CHead * pHead = pLine->GetHeadByID (lID);

			if (pHead) {
				CHeadInfo info = CHead::GetInfo (pHead);
				HEADSTRUCT h = info.m_Config.m_HeadSettings;
				CString strHead = info.GetFriendlyName ();

				OnSocketPrinterStateChange (lID, 0);

				if (HANDLE h = (HANDLE)wParam)
					::SetEvent (h);
			}
		}
	}

	return 0;
}

LRESULT CControlView::OnHeadStatusUpdate ( WPARAM wParam, LPARAM lParam )
{
	OnUpdate (NULL, wParam, (CObject *)lParam);
	return 0;
}

void CControlView::OnDestroy() 
{
	if (m_bKeyboard) {
		DWORD dw;
		
		::SendMessageTimeout (HWND_BROADCAST, WM_SHUTDOWNKEYBOARD, 1, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 100, &dw);
	}

	EndCommThreads ();
	EndLocalDbThreads ();

	if (m_nTimerStatus != -1)		KillTimer (m_nTimerStatus);
	if (m_nTimerTab != -1)			KillTimer (m_nTimerTab);
	if (m_nTimerRefresh != -1)		KillTimer (m_nTimerRefresh);
	if (m_nTimerTime != -1)			KillTimer (m_nTimerTime);
	if (m_nTimerStartMenu != -1)	KillTimer (m_nTimerStartMenu);

	KillThread ((CWinThread **)&m_pServerThread);
	KillThread ((CWinThread **)&m_pUdpThread);
	KillThread ((CWinThread **)&m_pNextThread);

///////////////////////////////////////////////////////////////////////////////////////////////////
//	CPrintHeadList HeadList;
	CHead *pHead = NULL;
	CStringList List;
	CControlDoc *pDoc = GetDocument();


	::SendMessage ( m_pPreview->m_hWnd, WM_DESTROY, 0, 0L );
	CFormView::OnDestroy();
	
	if (m_pdlgInfo) {
		delete m_pdlgInfo;
		m_pdlgInfo = NULL;
	}

	if (m_pPreview) {
		delete m_pPreview;
		m_pPreview = NULL;
	}
}

void CControlView::KillThread (CWinThread ** pThread)
{
	DECLARETRACECOUNT (20);

	if (pThread && (* pThread)) {
		HANDLE hThread = (* pThread)->m_hThread;
		UINT nThreadID = (* pThread)->m_nThreadID;
		CString strClass = a2w ((* pThread)->GetRuntimeClass ()->m_lpszClassName);

		if (CServerThread * p = DYNAMIC_DOWNCAST (CServerThread, * pThread)) {
			p->Close (); // otherwise, it's blocked
		}

		MARKTRACECOUNT ();

		HANDLE hEvent = CreateEvent (NULL, true, false, strClass + _T ("::TM_KILLTHREAD"));

		while (!::PostThreadMessage (nThreadID, TM_KILLTHREAD, (WPARAM)hEvent, 0))
			::Sleep (0);

		MARKTRACECOUNT ();

		DWORD dwWait = ::WaitForSingleObject (hEvent, 2000);

		::CloseHandle (hEvent);

		MARKTRACECOUNT ();

		//if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
		if (dwWait == WAIT_TIMEOUT)		{ TRACEF (_T ("WAIT_TIMEOUT: ")		+ strClass + _T (": ") + FormatMessage (::GetLastError ())); }
		if (dwWait == WAIT_ABANDONED)	{ TRACEF (_T ("WAIT_ABANDONED: ")	+ strClass + _T (": ") + FormatMessage (::GetLastError ())); }
		if (dwWait == WAIT_FAILED)		{ TRACEF (_T ("WAIT_FAILED: ")		+ strClass + _T (": ") + FormatMessage (::GetLastError ())); }

		//ASSERT (dwWait == WAIT_OBJECT_0);
		ASSERT (dwWait != WAIT_ABANDONED && dwWait != WAIT_TIMEOUT);

		(* pThread) = NULL;
	}

	TRACECOUNTARRAYMAX ();
}

int CControlView::StartTask(LPCTSTR pLine, LPCTSTR pTask, bool bStartup, const CString & strKeyValue, int nRemote, int nPeriod, CHead::CHANGECOUNTSTRUCT * pCount)
{
	CDbConnection conn (_T (__FILE__), __LINE__); 

	if (!conn.IsOpen ()) 
		return 0;

	bool bRemoteStart = false;
	bool bResetCounts = false;
	int result = -2;
	ULONG lTaskID;
	CControlDoc *pDoc = GetDocument();
	CString strLine = (pDoc->GetSelectedLine())->GetName();
	CString strTask;
	bool bCoupled = theApp.GetCoupledMode () && !bStartup;
	CStringArray vLines, vFailed;
	CString strLineAlt;

	if (!bCoupled && pLine == NULL)
	{
		LinePromptDlg::CLinePromptDlg dlg (theApp.m_Database, this);
		
		bRemoteStart = true;
		dlg.m_line = pDoc->GetSelectedLine()->m_LineRec;

		if (dlg.DoModal() == IDOK) {
			strLine = dlg.m_line.m_strName;
			pDoc->SetSelectedLine (strLine);
		}
		else
			return 0;
	}
	else
		strLine = pLine;

	if (!bCoupled) 
		vLines.Add (strLine);
	else 
		pDoc->GetProductLines (vLines);

	if (pTask == NULL) {
		CStartTaskDlg dlg;

		dlg.m_strRegKey = CControlApp::GetRegKey ();
		dlg.m_bContinuousCount = theApp.IsContinuousCount ();

		BeginWaitCursor ();
		dlg.m_strTaskName = pDoc->GetSelectedLine()->GetCurrentTask ();
		dlg.m_sLineName = strLine;
		int nResult = dlg.DoModal ();
		strLineAlt = dlg.m_sLineName;
		EndWaitCursor ();
		
		if (theApp.IsContinuousCount ())
			dlg.m_bResetCounts = false;

		bResetCounts = dlg.m_bResetCounts;

		if (dlg.m_bResetCounts) {
			if (pCount) {
				pCount->m_lCount = 0;
				pCount->m_lPalletCount = pCount->m_lPalletMin;
			}
		}
		else {
			if (pCount) {
				if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
					pCount->m_lCount		= pLine->m_countLast.m_lCount;
					pCount->m_lPalletCount	= pLine->m_countLast.m_lPalletCount;
				}
			}
		}

		if (nResult == IDOK) {
			lTaskID			= m_lTaskID = dlg.m_lTaskID;
			strTask			= dlg.m_strTaskName;

			if (dlg.m_bResetCounts) {
				for (int i = 0; i < vLines.GetSize (); i++) 
					GlobalFuncs::WriteProfileInt (vLines [i], GlobalVars::strCount, 0);
			}
		}
		else
			return 0;
	}
	else {
		CStringArray v;

		strTask.Format (_T ("%s"), pTask);

		if (theApp.GetCoupledMode ()) {
			CStringArray vTmp;
			
			v.Add (strLine);

			pDoc->GetProductLines (vTmp);

			for (int i = 0; i < vTmp.GetSize (); i++) 
				if (Find (v, vTmp [i]) == -1)
					v.Add (vTmp [i]);

			#ifdef _DEBUG
			{
				for (int i = 0; i < v.GetSize (); i++) { TRACEF (v [i]); }
				int nBreak = 0;
			}
			#endif
		}
		else 
			v.Add (strLine);

		for (int i = 0; i < v.GetSize (); i++) {
			lTaskID = FoxjetDatabase::GetTaskID (theApp.m_Database, strTask, v [i], GetMasterPrinterID (theApp.m_Database));

			if (lTaskID > 0) {
				if (strLine != v [i])
					strLineAlt = v [i];
				break;
			}
		}

		if (lTaskID <= 0)
			return -1;

		//if ((lTaskID = FoxjetDatabase::GetTaskID (theApp.m_Database, strTask, strLine, GetMasterPrinterID (theApp.m_Database))) == 0) 
		//	return -1;
		
		if (nRemote != -1)
			bRemoteStart = nRemote ? true : false;
		else 
			bRemoteStart = true;
	}

	DECLARETRACECOUNT (200);
	MARKTRACECOUNT ();

	::QueryPerformanceCounter (&m_counter); 

	bool bCountDlg			= bStartup ? true : (bResetCounts > 0 ? true : false);			// sw0844

	if (pCount)
		bCountDlg = true;

	m_lTaskID = lTaskID;

	Foxjet3d::UserElementDlg::CUserElementDlg dlgUser (-1, this);
	CMapStringToString vSet;

	if (!bStartup) {
		for (int i = 0; i < vLines.GetSize (); i++) {
			CString strLine = vLines [i];
			if (!StopTask (strLine, false))
				return false;

			GlobalFuncs::WriteProfileInt (strLine, GlobalVars::strCount, 0);
			GlobalFuncs::WriteProfileString (strLine, GlobalVars::strTask, _T (""));
		}

		{
			CStringArray v;

			GetFiles (GetHomeDir () + _T ("\\XML\\"), _T ("*.*"), v);
			//GetFiles (GetHomeDir () + _T ("\\") + GlobalVars::strUserPrompted + _T ("\\") + strLine, _T ("*.*"), v);

			for (int i = 0; i < v.GetSize (); i++)
				::DeleteFile (v [i]);
		}
	}

	for (int i = 0; i < vLines.GetSize (); i++) {
		CString str = vLines [i];
		CProdLine *pProdLine = pDoc->GetProductionLine (str);
		bool bSameTask = strTask.CompareNoCase (pProdLine->GetCurrentTask ()) == 0;
		__int64 lPalletMax		= 0;
		__int64 lUnitsPerPallet	= 0;

		if (!strTask.CompareNoCase (pProdLine->GetCurrentTask ())) {
			lPalletMax		= pProdLine->GetPalletMax ();
			lUnitsPerPallet = pProdLine->GetUnitsPerPallet ();
		}

		result = pProdLine->StartTask (strTask, bRemoteStart, bStartup, false, this, 
			lPalletMax, lUnitsPerPallet, bCountDlg, strKeyValue, &vSet, nPeriod, strLineAlt, pCount); 
		pDoc->UpdateAllViews (this);

		if (bResetCounts) {
			using namespace FoxjetElements;

			Head::CElementPtrArray v;
			bool bIrregular = false;

			pProdLine->GetElements (v, COUNT);

			for (int i = 0; i < v.GetSize (); i++) {
				if (CCountElement * p = DYNAMIC_DOWNCAST (CCountElement, v [i])) {
					if (p->IsIrregularPalletSize ()) {
						if (p->IsPalletCount ()) {
							bIrregular = true;
						}
					}
				}
			}

			if (!bIrregular) {
				if (pCount) {
					pCount->m_lCount = 0;
					pCount->m_lPalletCount = pCount->m_lPalletMin;
				}
			}
		}

		TRACEF (pProdLine->GetName () + _T (": ") + strTask + _T (": ") + ToString (result));
		pProdLine->GetUserElements (dlgUser.m_vElements, true);

		if (!TASK_START_SUCCEEDED (result)) {
			result = 0;
			vFailed.Add (str);
		}
		else {
			pProdLine->IdleTask (false);
		}
	}

	if (!vFailed.GetSize ()) {
		CMapStringToString vUser;
		FoxjetUtils::CDatabaseStartDlg dlgDB (NULL);

		dlgDB.Load (theApp.m_Database);
				
		TRACEF (dlgDB.m_strDSN);
		TRACEF (dlgDB.m_strTable);
		TRACEF (dlgDB.m_strKeyField);
		TRACEF ("");

		for (int i = 0; i < dlgUser.m_vElements.GetSize (); i++) {
			CString strPrompt = dlgUser.m_vElements [i].m_strPrompt;
			CString strData = dlgUser.m_vElements [i].m_strData;

			TRACEF (strPrompt + _T (" --> ") + strData);

			#ifdef __WINPRINTER__

			using namespace FoxjetElements;

			if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, dlgUser.m_vElements [i].m_pElement)) {
				TRACEF (p->GetDSN ());
				TRACEF (p->GetTable ());
				TRACEF (p->GetKeyField ());
				TRACEF ("");

				if (!p->GetDSN ().CompareNoCase (dlgDB.m_strDSN) &&
					!p->GetTable ().CompareNoCase (dlgDB.m_strTable) &&
					!p->GetKeyField ().CompareNoCase (dlgDB.m_strKeyField))
				{
					if (strKeyValue.GetLength ()) 
						dlgUser.m_vElements.RemoveAt (i);
				}
			}
			else if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, dlgUser.m_vElements [i].m_pElement)) {
				TRACEF (p->GetDatabaseDSN ());
				TRACEF (p->GetDatabaseTable ());
				TRACEF (p->GetDatabaseKeyField ());
				TRACEF ("");

				if (!p->GetDatabaseDSN ().CompareNoCase (dlgDB.m_strDSN) &&
					!p->GetDatabaseTable ().CompareNoCase (dlgDB.m_strTable) &&
					!p->GetDatabaseKeyField ().CompareNoCase (dlgDB.m_strKeyField))
				{
					if (strKeyValue.GetLength ()) 
						dlgUser.m_vElements.RemoveAt (i);
				}
			}
			else if (CExpDateElement * p = DYNAMIC_DOWNCAST (CExpDateElement, dlgUser.m_vElements [i].m_pElement)) { // sw0924
				if (p->GetType () == CExpDateElement::TYPE_DATABASE) {
					TRACEF (p->GetDSN ());
					TRACEF (p->GetTable ());
					TRACEF (p->GetKeyField ());
					TRACEF ("");

					if (!p->GetDSN ().CompareNoCase (dlgDB.m_strDSN) &&
						!p->GetTable ().CompareNoCase (dlgDB.m_strTable) &&
						!p->GetKeyField ().CompareNoCase (dlgDB.m_strKeyField))
					{
						if (strKeyValue.GetLength ()) 
							dlgUser.m_vElements.RemoveAt (i);
					}
				}
			}
			#endif //__WINPRINTER__
		}

		if (FoxjetUtils::CStartupDlg::IsUserDataEnabled (theApp.m_Database)) {
			for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
				for (int i = 0; i < dlgUser.m_vElements.GetSize (); i++) {
					CString strPrompt = dlgUser.m_vElements [i].m_strPrompt;
					const CString strFile = GetHomeDir () + _T ("\\") + GlobalVars::strUserPrompted + _T ("\\") + vLines [nLine] + _T ("\\") + strPrompt + _T (".txt");
					const CString strData = FoxjetFile::ReadDirect (strFile, dlgUser.m_vElements [i].m_strData);
					TRACEF (strPrompt + _T (" --> ") + dlgUser.m_vElements [i].m_strData);
					dlgUser.m_vElements [i].m_strData = strData;
				}
			}
		}

		if (dlgUser.m_vElements.GetSize ()) {
			if (!bStartup) {
				if (dlgUser.DoModal () == IDCANCEL) {
					for (int i = 0; i < vLines.GetSize (); i++) 
						vFailed.Add (vLines [i]);

					result = 0;
				}
				else {
					for (int i = 0; i < dlgUser.m_vElements.GetSize (); i++) {
						CString strPrompt = dlgUser.m_vElements [i].m_strPrompt;
						CString strData = dlgUser.m_vElements [i].m_strData;

						TRACEF (strPrompt + _T (" --> ") + strData);
						vUser.SetAt (strPrompt, strData);

						for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
							const CString strFile = GetHomeDir () + _T ("\\") + GlobalVars::strUserPrompted + _T ("\\") + vLines [nLine] + _T ("\\") + strPrompt + _T (".txt");
							FoxjetFile::WriteDirect (strFile, dlgUser.m_vElements [i].m_strData);
						}
					}
				}
			}
			else {
				if (FoxjetUtils::CStartupDlg::IsUserDataEnabled (theApp.m_Database)) {
					for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
						for (int i = 0; i < dlgUser.m_vElements.GetSize (); i++) {
							CString strPrompt = dlgUser.m_vElements [i].m_strPrompt;
							const CString strFile = GetHomeDir () + _T ("\\") + GlobalVars::strUserPrompted + _T ("\\") + vLines [nLine] + _T ("\\") + strPrompt + _T (".txt");
							CString strData = FoxjetFile::ReadDirect (strFile,  dlgUser.m_vElements [i].m_strData);
							TRACEF (strPrompt + _T (" --> ") + strData);
							vUser.SetAt (strPrompt, strData);
						}
					}
				}
			}
		}

		if (vUser.GetCount()) {
			for (int i = 0; i < vLines.GetSize (); i++) {
				if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
					CPrintHeadList list;
					CString strKeyValueSet (strKeyValue);

					if (strKeyValueSet.IsEmpty ()) {
						CString strKeyPrompt;

						strKeyPrompt.Format (_T ("[%s].[%s].[%s]"),
							dlgDB.m_strDSN,
							dlgDB.m_strTable,
							dlgDB.m_strKeyField);
					
						for (POSITION pos = vUser.GetStartPosition (); pos; ) {
							CString strPrompt, strData;

							vUser.GetNextAssoc (pos, strPrompt, strData);
							TRACEF (strPrompt + _T (" --> ") + strData);

							if (!strKeyPrompt.CompareNoCase (strPrompt)) {
								strKeyValueSet = strData;
								break;
							}
						}
					}

					pLine->ChangeUserElementData(bStartup, strKeyValueSet, &vUser, nPeriod);
					pLine->GetActiveHeads (list);

					for (POSITION pos = list.GetStartPosition(); pos; ) {
						DWORD dwKey;
						CHead * pHead = NULL;

						list.GetNextAssoc (pos, dwKey, (void *&)pHead);
						ASSERT (pHead);

						#ifdef __WINPRINTER__
						CLocalHead::BUILDIMAGESTRUCT * pParam = MAKEIMGPARAMS (IMAGING_REFRESH, pHead->m_nThreadID);
						HANDLE hEvent = pParam->m_hEvent;

						while (!::PostThreadMessage (pHead->m_nThreadID, TM_BUILD_IMAGE, (WPARAM)pParam, CLocalHead::BUILDIMAGE_OVERRIDE))
							::Sleep (0);

						DWORD dwWait = ::WaitForSingleObject (hEvent /* pParam->m_hEvent */, 5000);

						//if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
						if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
						if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
						if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");

						//delete pParam;
						#endif //__WINPRINTER__
					}
				}
			}
		}

		for (int i = 0; i < vLines.GetSize (); i++) {
			if (CProdLine * pProdLine = pDoc->GetProductionLine (vLines [i])) {
				pProdLine->ResumeTask (false);
			}
		}
	}

	if (vFailed.GetSize () && !bStartup) {
		CString str;
		
		str.Format (LoadString (IDS_FAILEDTOSTARTLINE), strTask);

		for (int i = 0; i < vFailed.GetSize (); i++)
			str += _T ("\n") + vFailed [i];

		TRACEF (str);
		MsgBox (* this, str, NULL, MB_OK | MB_ICONINFORMATION);
	}

	pDoc->UpdateAllViews (NULL, UPDATE_PREVIEW, NULL);

	theApp.PostViewRefresh (3000);
	pDoc->SetLocal (COdbcDatabase::IsLocal ());
	COdbcDatabase::FreeDsnCache ();

	if (IsSunnyvale ())
		Sunnyvale::Init ();

	#ifdef _DEBUG
	//if (m_pDiagDlg) 
	{
		CString str;
		LARGE_INTEGER counter;
		
		::QueryPerformanceCounter (&counter); 

		double d = ((double)counter.QuadPart - (double)m_counter.QuadPart) / (double)m_freq.QuadPart; 
		str.Format (_T ("CControlView::StartTask: %.8f seconds"), d);

		//UpdateSerialData (str); 
	}
	#endif //_DEBUG

	MARKTRACECOUNT ();
	//TRACECOUNTARRAY ();

	OnUpdateLogin ();

	return result;
}

LRESULT CControlView::OnRestartTask (WPARAM wParam, LPARAM lParam)
{
	CDbConnection conn (_T (__FILE__), __LINE__); 

	if (!conn.IsOpen ()) 
		return 0;

	CControlDoc * pDoc = GetDocument();
	CStringArray vLines, vFailed;

	pDoc->GetProductLines (vLines);

	bool bPromptForCountAtTaskStart = theApp.PromptForCountAtTaskStart ();
	theApp.SetPromptForCountAtTaskStart (false);

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		CString strLine = vLines [nLine];
		
		if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
			TASKSTATE state = pLine->GetTaskState ();

			if ((state == RUNNING) || (state == IDLE)) {
				CString strKeyValue;
				CString strTask			= pLine->GetCurrentTask ();
				__int64 lPalletMax		= pLine->GetPalletMax ();
				__int64 lUnitsPerPallet	= pLine->GetUnitsPerPallet ();
				__int64 lFind = 0, lMin = 0, lMax = 0, lMaster = 0;
				Foxjet3d::UserElementDlg::CUserElementArray vUser;
				CMapStringToString vUserMap;
				bool bSerialPending = pLine->GetSerialPending ();

				pLine->GetUserElements (vUser, false);

				for (int i = 0; i < vUser.GetSize (); i++) {
					TRACEF (vUser [i].m_strPrompt + _T (" --> ") + vUser [i].m_strData);
					vUserMap.SetAt (vUser [i].m_strPrompt, vUser [i].m_strData);
				}

				if (theApp.GetProfileInt (_T ("sw0858"), _T ("db start"), 0)) 
					strKeyValue	= theApp.GetProfileString (_T ("sw0858"), _T ("Key value"));

				pLine->GetCounts (NULL, lFind, lMin, lMax, lMaster);

				#ifdef __WINPRINTER__
				pLine->SetCounts (lMaster);
				#endif

				ULONG lResult = pLine->StartTask (strTask, false, false, false, this, lPalletMax, lUnitsPerPallet, false, strKeyValue, &vUserMap); 

				#ifdef __IPPRINTER__
				pLine->SetCounts (lMaster);
				#endif

				if (state == IDLE)
					pLine->IdleTask (false, false);

				if (!TASK_START_SUCCEEDED (lResult)) 
					vFailed.Add (strLine);
				else {
					if (pLine->IsOneToOnePrintEnabled ())
						pLine->SetSerialPending (bSerialPending);
				}
			}
		}
	}

	pDoc->UpdateAllViews (this);
	pDoc->UpdateAllViews (NULL, UPDATE_PREVIEW, NULL);

	if (vFailed.GetSize ()) {
		CString str;
		
		str.Format (LoadString (IDS_FAILEDTORESTARTLINE));

		for (int i = 0; i < vFailed.GetSize (); i++)
			str += _T ("\n") + vFailed [i];

		TRACEF (str);
		MsgBox (* this, str, NULL, MB_OK | MB_ICONINFORMATION);
	}

	theApp.SetPromptForCountAtTaskStart (bPromptForCountAtTaskStart);
	theApp.PostViewRefresh (3000);

	return 0;
}

bool CControlView::StopTask(LPCTSTR pLine, bool bPrompt)
{
	bool bResult = true;
	CDbConnection conn (_T (__FILE__), __LINE__); 

	if (!conn.IsOpen ()) 
		return bResult;

	CControlDoc *pDoc = GetDocument();
	CString strLine;
	CStringArray vLines;
	
	m_pPreview->SaveScrollPos ();

	if ( pLine != NULL )
		strLine.Format (_T ("%s"), pLine);
	else
		strLine = pDoc->GetSelectedLine()->GetName();
	
	if (!theApp.GetCoupledMode ()) 
		vLines.Add (strLine);
	else {
		pDoc->GetProductLines (vLines);

		if (bPrompt)
			if (MsgBox (* this, LoadString (IDS_STOPTASK), LoadString (IDS_STOP), MB_YESNO) == IDNO)
				return 0;

		bPrompt = false;
	}

	for (int i = 0; i < vLines.GetSize (); i++) {
		CProdLine *pProdLine = pDoc->GetProductionLine (vLines [i]);
		
		if (!pProdLine->StopTask (bPrompt, this))
			bResult = false;

		pDoc->UpdateAllViews (NULL, UPDATE_PREVIEW, NULL);
		OnUpdate ( NULL, UPDATE_PREVIEW, NULL );
	}

	theApp.PostViewRefresh (1000);

	return bResult;
}

int CControlView::IdleTask(const CString * pLine)
{
	int nResult = 0;
	CDbConnection conn (_T (__FILE__), __LINE__); 

	if (!conn.IsOpen ()) 
		return nResult;

	CStringArray vLines;
	CControlDoc *pDoc = GetDocument();
	CString strLine, strSelectedLine, strMsg;
	bool bIdle = true;

	strLine = strSelectedLine = (pDoc->GetSelectedLine ())->GetName();
	
	if (!theApp.GetCoupledMode ()) 
		vLines.Add (strLine);
	else 
		pDoc->GetProductLines (vLines);

	if ( pLine != NULL )
		strLine = * pLine;

	if (pLine == NULL) 
		bIdle = MsgBox (* this, LoadString (IDS_IDLETASK), LoadString (IDS_CONFIRM), MB_YESNO | MB_ICONQUESTION) == IDYES;

	for (int i = 0; i < vLines.GetSize (); i++) {
		strLine = vLines [i];

		CProdLine * pProdLine = pDoc->GetProductionLine (strLine);
		ASSERT (pProdLine);

		if (bIdle) {
			bool bHeadError = false;
			CPrintHeadList list;

			pProdLine->GetActiveHeads (list);

			for (POSITION pos = list.GetStartPosition (); pos; ) {
				CHead * pHead = NULL;
				DWORD dw;

				list.GetNextAssoc (pos, dw, (void *&)pHead);
				ASSERT (pHead);
				CHeadInfo info = CHead::GetInfo (pHead);
				DWORD dwState = 0;

				SEND_THREAD_MESSAGE (info, TM_GET_ERROR_STATE, &dwState);

				if (pHead->IsError (dwState)) {
					bHeadError = true;
					break;
				}
			}

			pProdLine->IdleTask (true, bHeadError);
			m_eTaskState = IDLE;
			GetDocument()->UpdateAllViews (this, UPDATE_PREVIEW, NULL);
			nResult = 1;
		}
	}

	return nResult;
}

int CControlView::ResumeTask(const CString *pLine)
{
	int result = 0;

	CDbConnection conn (_T (__FILE__), __LINE__); 

	if (!conn.IsOpen ()) 
		return result;

	CControlDoc *pDoc = GetDocument();
	CStringArray vLines;
	CString sSelectedLine, strLine;
	strLine = sSelectedLine = (pDoc->GetSelectedLine ())->GetName();
	
	if (pLine)
		strLine = * pLine;

	if (!theApp.GetCoupledMode ()) 
		vLines.Add (strLine);
	else
		pDoc->GetProductLines (vLines);

	for (int i = 0; i < vLines.GetSize (); i++) {
		strLine = vLines [i];
		
		if (CProdLine *pProdLine = pDoc->GetProductionLine (strLine)) {
			if (!pProdLine->IsIdledOnError ()) {
				pProdLine->ResumeTask ();
				result = 1;
				m_eTaskState = RUNNING;
			}
		}
	}

	GetDocument()->UpdateAllViews (this, UPDATE_PREVIEW, NULL);
	return result;
}

void CControlView::DoAmsCycle ()
{
#ifdef __IPPRINTER__
	CControlDoc * pDoc = GetDocument();
	CPrintHeadList heads;

	ASSERT (pDoc);

	const CString strLine = pDoc->GetSelectedLine()->GetName();
	ULONG lLineID = pDoc->GetSelectedLine()->GetID ();
	
	pDoc->GetActiveHeads (strLine, heads);
	
	for (POSITION pos = heads.GetStartPosition(); pos != NULL; ) {
		CHead * p;
		ULONG key;

		heads.GetNextAssoc (pos, key, (void *&)p);

		if (CRemoteHead * pHead = DYNAMIC_DOWNCAST (CRemoteHead, p)) {

			while (!PostThreadMessage (pHead->m_nThreadID, WM_HEAD_AMSCYCLE, lLineID, 0)) 
				Sleep (0);
		}
	}
#endif //__IPPRINTER__
}

void CControlView::HeadCfgUpdated(const FoxjetCommon::CLongArray * pvNew)
{
	CDbConnection conn (_T (__FILE__), __LINE__); 

	CControlDoc *pDoc = GetDocument();
	CStringArray vLines;
	CArray <CHead *, CHead *> vHeads;
	CMap <CHead *, CHead *, bool, bool> mapPhotoEnabled;
	int nSuspend = 0, nResume = 0;
	CMap <DWORD, DWORD, FILETIME, FILETIME &> mapUser;
	CMap <DWORD, DWORD, FILETIME, FILETIME &> mapKernel;

	BeginWaitCursor ();

	m_vTask.RemoveAll ();
	m_vKeyValue.RemoveAll ();

	m_strTimeFormat [0] = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, ListGlobals::defElements.m_strRegSection + _T ("\\time"), _T ("format1"), FoxjetCommon::GetDefTimeFormat (0)); 
	m_strTimeFormat [1] = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, ListGlobals::defElements.m_strRegSection + _T ("\\time"), _T ("format2"), FoxjetCommon::GetDefTimeFormat (1)); 
	m_strTimeFormat [2] = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, ListGlobals::defElements.m_strRegSection + _T ("\\time"), _T ("format3"), FoxjetCommon::GetDefTimeFormat (2)); 

	pDoc->GetProductLines (vLines);
	FillLineCtrl ();

	#if defined(__IPPRINTER__)
	if (pvNew) {
		LOCK (pDoc->m_ConnectionListSentinel);

		for (int i = 0; i < pvNew->GetSize (); i++) {
			FoxjetDatabase::HEADSTRUCT head;
			ULONG lHeadID = (* pvNew) [i];

			if (FoxjetDatabase::GetHeadRecord (theApp.m_Database, lHeadID, head)) {
				CString str;
				CHead * p = StartRemoteHead (head);

				vHeads.Add (p);
				pDoc->UpdateAllViews (NULL, UPDATE_HEAD_ACTIVATION, p);
			}
		}
	}
	#endif

	for (int i = 0; i < vLines.GetSize (); i++) {
		if (CProdLine * p = pDoc->GetProductionLine (vLines [i])) {
			CPrintHeadList l;

			p->GetActiveHeads (l);

			p->m_vPanels.RemoveAll ();
			GetPanelRecords (theApp.m_Database, p->GetID (), p->m_vPanels);
			ASSERT (p->m_vPanels.GetSize () == 6);
			
			for (POSITION pos = l.GetStartPosition (); pos != NULL; ) {
				DWORD dw = 0;
				CHead * p = NULL;

				l.GetNextAssoc (pos, dw, (void *&)p);
				ASSERT (p);
				CHeadInfo info = CHead::GetInfo (p);
				vHeads.Add (p);

				#ifdef __IPPRINTER__
				if (CRemoteHead * pRemote = DYNAMIC_DOWNCAST (CRemoteHead, p)) {
					if (pRemote->IsDigiBoard ()) {
						OnHeadProcessing (UPDATING_CONFIG, (LPARAM)pRemote);
						::Sleep (1);
					}
				}
				#endif //__IPPRINTER__
			}
		}
	}

	TRACEF (_T ("CControlView::HeadCfgUpdated"));

	// check for deleted heads 
	for (int i = vHeads.GetSize () - 1; i >= 0; i--) {
		CHead * p = vHeads [i];
		CHeadInfo info = CHead::GetInfo (p);
		HEADSTRUCT h;
		ULONG lID = info.m_Config.m_HeadSettings.m_lID;
		bool bPhotoEnabled = false;

		if (!GetHeadRecord (theApp.m_Database, lID, h)) { 
			CString str, strEvent;
			bool bDeleted = false;
			CProgressDlg dlg (this);

			POST_THREAD_MESSAGE (info, TM_ENABLE_PHOTO, 0);
	
			strEvent.Format (_T ("TM_KILLTHREAD %lu"), info.GetThreadID ());
			TRACEF (strEvent);
			HANDLE hEvent = ::CreateEvent (NULL, true, false, strEvent);

			str.Format (LoadString (IDS_SHUTTINGDOWN), info.GetFriendlyName ());
			UPDATE_STATUS (dlg, lID, str);
			//::Sleep (1000);
			bDeleted = OnHeadDelete (0, lID) ? true : false;
			CLEAR_STATUS (dlg, lID);
			vHeads.RemoveAt (i);
			ASSERT (bDeleted);
		}
	}

	#ifdef _DEBUG
	for (int i = 0; i < vHeads.GetSize (); i++) {
		CHead * p = vHeads [i];
		CHeadInfo info = CHead::GetInfo (p);
		TRACEF (info.GetUID () + _T (": ") + info.GetFriendlyName ());
	}
	#endif


	{
		CArray <CHead::SUSPENDSTRUCT *, CHead::SUSPENDSTRUCT *> v;

		for (int i = 0; i < vHeads.GetSize (); i++) {
			CHead * p = vHeads [i];
			CHeadInfo info = CHead::GetInfo (p);
			bool bPhotoEnabled = false;
			CHead::SUSPENDSTRUCT * pSuspend = new CHead::SUSPENDSTRUCT;
			ULONG lTime = time (NULL);

			SEND_THREAD_MESSAGE (info, TM_IS_PHOTOENABLED, &bPhotoEnabled);
			mapPhotoEnabled.SetAt (p, bPhotoEnabled);

			pSuspend->m_strSuspend.Format (_T ("SUSPENDSTRUCT::m_hSuspend::%d::%d"), info.GetThreadID (), lTime);
			TRACER (pSuspend->m_strSuspend + _T ("\n"));
			pSuspend->m_hSuspend = ::CreateEvent (NULL, true, false, pSuspend->m_strSuspend);
			
			pSuspend->m_strLoaded.Format (_T ("SUSPENDSTRUCT::m_hLoaded::%d::%d"), info.GetThreadID (), lTime);
			TRACER (pSuspend->m_strLoaded + _T ("\n"));
			pSuspend->m_hLoaded = ::CreateEvent (NULL, true, false, pSuspend->m_strLoaded);

			SEND_THREAD_MESSAGE (info, TM_ENABLE_PHOTO, 0);
			SEND_THREAD_MESSAGE (info, TM_SUSPEND, pSuspend);
			v.Add (pSuspend);
		}

		for (int i = 0; i < v.GetSize (); i++) {
			if (CHead::SUSPENDSTRUCT * pSuspend = v [i]) {
				TRACER (_T ("SetEvent: ") + pSuspend->m_strSuspend);
				::SetEvent (pSuspend->m_hSuspend);

				TRACER (_T ("WaitForSingleObject: ") + pSuspend->m_strLoaded);
				DWORD dwWait = ::WaitForSingleObject (pSuspend->m_hLoaded, INFINITE);
				if (dwWait == WAIT_TIMEOUT)		{ TRACER (_T ("WAIT_TIMEOUT: ")	+ pSuspend->m_strLoaded);	TRACEF (_T ("WAIT_TIMEOUT"));	}
				if (dwWait == WAIT_ABANDONED)	{ TRACER (_T ("WAIT_ABANDONED: ")	+ pSuspend->m_strLoaded);	TRACEF (_T ("WAIT_ABANDONED"));	}
				if (dwWait == WAIT_OBJECT_0)	{ TRACER (_T ("WAIT_OBJECT_0: ")	+ pSuspend->m_strLoaded);	TRACEF (_T ("WAIT_OBJECT_0"));	}

				delete pSuspend;
			}
		}
	}

	for (int i = 0; i < vHeads.GetSize (); i++) {
		CHead * p = vHeads [i];
		bool bPhotoEnabled = false;

		#ifdef __WINPRINTER__
		if (CLocalHead * pLocal = DYNAMIC_DOWNCAST (CLocalHead, p)) {
			CHeadInfo info = CHead::GetInfo (p);

			if (CControlDoc * pDoc = GetDocument ()) {
				CHead::CONFIGCHANGESTRUCT config;

				pDoc->GetProductLines (config.m_vLines);

				for (int i = 0; i < config.m_vLines.GetSize (); i++) {
					if (CProdLine * pLine = pDoc->GetProductionLine (config.m_vLines [i])) {
						if (pLine->GetID () == info.m_Config.m_Line.m_lID) {
							pLine->GetActiveHeads (config.m_listHeads);
							//config.m_pSerialData = pLine->GetSerialData ();
							config.m_serialData.m_Len = pLine->GetSerialData ()->m_Len;
							memcpy (config.m_serialData.Data, pLine->GetSerialData ()->Data, ARRAYSIZE (config.m_serialData.Data) * sizeof (config.m_serialData.Data [0]));
							config.m_pSw0848Thread = pLine->m_pSw0848Thread;
							SEND_THREAD_MESSAGE (info, TM_HEAD_CONFIG_CHANGED, &config);
							break;
						}
					}
				}
			}

			CLocalHead::BUILDIMAGESTRUCT * pParam = MAKEIMGPARAMS (IMAGING, 0);
			CString str;

			str.Format (_T ("TM_BUILD_IMAGE %lu"), p->m_hThread);
			HANDLE hEvent = pParam->m_hEvent = ::CreateEvent (NULL, true, false, str);

			while (!::PostThreadMessage (p->m_nThreadID, TM_BUILD_IMAGE, (WPARAM)pParam, CLocalHead::BUILDIMAGE_OVERRIDE | CLocalHead::BUILDIMAGE_CONFIG_CHANGE))
				::Sleep (0);

			DWORD dwWait = ::WaitForSingleObject (hEvent /* pParam->m_hEvent */, 5000);

			//if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
			if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
			if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
			if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");

			//if (dwWait == WAIT_OBJECT_0)
			//	delete pParam;

			ASSERT (dwWait == WAIT_OBJECT_0);
		}
		#endif //__WINPRINTER__

		if (mapPhotoEnabled.Lookup (p, bPhotoEnabled))
			if (bPhotoEnabled)
				while (!::PostThreadMessage (p->m_nThreadID, TM_ENABLE_PHOTO, bPhotoEnabled, 0))
					::Sleep (0);
	}

	OnViewRefreshpreview ();
	OnChangeProductLine ();
	theApp.LoadPingSettings ();
	LoadLocalDbSettings ();

	if (m_pdlgInfo)
		m_pdlgInfo->PostMessage (WM_SYSTEMPARAMCHANGE);
	
	if (m_pNextThread) 
		while (!m_pNextThread->PostThreadMessage (TM_SYSTEMPARAMCHANGE, 0, 0))
			::Sleep (1);

	{ // update registry settings for driver
		CString strSection;
		CArray <HEADSTRUCT, HEADSTRUCT &> v;
		int nCard = 1;

		//FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, GlobalVars::strDriverRegSection, _T ("WorkingDir"), FoxjetDatabase::GetHomeDir ());

		{ // delete any old data
			for (int i = 1; i <= 6; i++) {
				CString str;

				str.Format (_T ("%s\\Head%d"), GlobalVars::strDriverRegSection, i);
				::RegDeleteKey (HKEY_CURRENT_USER, str);
			}
		}

		for (int i = 0; i < vLines.GetSize (); i++) {
			if (CProdLine * p = pDoc->GetProductionLine (vLines [i])) {
				CArray <HEADSTRUCT, HEADSTRUCT &> vSorted;

				GetLineHeads (theApp.m_Database, p->m_LineRec.m_lID, v);

				for (int nHead = 0; nHead < v.GetSize (); nHead++)
					InsertAscendingByCard (vSorted, v [nHead]);

				for (int nHead = 0; nHead < vSorted.GetSize (); nHead++) {
					HEADSTRUCT & h = vSorted [nHead];

					strSection.Format (_T ("%s\\Head%d"), GlobalVars::strDriverRegSection, nCard++);
					h.WriteProfile (HKEY_CURRENT_USER, strSection, p->m_LineRec.m_lID);
				}
			}
		}
	}

	if (m_pPreview)
		m_pPreview->OnHeadConfigUpdated();

	EndWaitCursor ();
}

// Used for IP Printer Only
LRESULT CControlView::OnSyncHeadData (WPARAM wParam, LPARAM lParam)
{
	LRESULT result = 0;

#ifdef __IPPRINTER__

	DWORD Key;
	CControlDoc *pDoc = GetDocument();
	CPrintHeadList HeadList;
	CRemoteHead *pHead;
	CString sSelectedLine = pDoc->GetSelectedLine()->GetName();

	LOCK (pDoc->m_ConnectionListSentinel);
	// Sync All Heads
	if ( wParam  == 0xFFFFFFFF )
	{
		pDoc->GetActiveHeads (sSelectedLine, HeadList);
		if ( HeadList.GetCount() )
		{
			POSITION pos = HeadList.GetStartPosition();
			while ( pos ) {
				CHead *p;
				HeadList.GetNextAssoc(pos, Key, (void *&)p);
				if ( p->IsKindOf ( RUNTIME_CLASS(CRemoteHead) ) )
				{
					pHead = (CRemoteHead *) p;
					while ( !PostThreadMessage (pHead->m_nThreadID, TM_UPDATE_LABELS, UPDATE_MODIFIED_ONLY, 0)) Sleep (0);
					// Added the following line to force Date/Time, Barcode, and Shift code updates CGH 0.0.3
					//while ( !PostThreadMessage (pHead->m_nThreadID, TM_HEAD_CONFIG_CHANGED, 0, 0L)) Sleep (0);
				}
			}
		}
	}

	result = 1;
	sLock.Unlock();
#endif
	return result;
}

void CControlView::ChangeUserElementData()
{
	CControlDoc *pDoc = GetDocument();
	CString sLine = pDoc->GetSelectedLine()->GetName();
	CStringArray vLines;
	Foxjet3d::UserElementDlg::CUserElementDlg dlgUser (-1, this);

	if (!theApp.GetCoupledMode ()) {
		if (CProdLine * pLine = pDoc->GetProductionLine (sLine)) 
			vLines.Add (pLine->GetName ());
	}
	else 
		pDoc->GetProductLines (vLines);


	for (int i = 0; i < vLines.GetSize (); i++) 
		if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i]))
			pLine->GetUserElements (dlgUser.m_vElements, false);

	{
		FoxjetUtils::CDatabaseStartDlg dlgDB (NULL);

		dlgDB.Load (theApp.m_Database);
				
		TRACEF (dlgDB.m_strDSN);
		TRACEF (dlgDB.m_strTable);
		TRACEF (dlgDB.m_strKeyField);
		TRACEF ("");

		for (int i = 0; i < dlgUser.m_vElements.GetSize (); i++) {
			CString strPrompt = dlgUser.m_vElements [i].m_strPrompt;
			CString strData = dlgUser.m_vElements [i].m_strData;

			TRACEF (strPrompt + _T (" --> ") + strData);

			#ifdef __WINPRINTER__

			using namespace FoxjetElements;

			if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, dlgUser.m_vElements [i].m_pElement)) {
				TRACEF (p->GetDSN ());
				TRACEF (p->GetTable ());
				TRACEF (p->GetKeyField ());
				TRACEF ("");

				if (!p->GetDSN ().CompareNoCase (dlgDB.m_strDSN) &&
					!p->GetTable ().CompareNoCase (dlgDB.m_strTable) &&
					!p->GetKeyField ().CompareNoCase (dlgDB.m_strKeyField))
				{
					dlgUser.m_vElements.RemoveAt (i);
				}
			}
			else if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, dlgUser.m_vElements [i].m_pElement)) {
				TRACEF (p->GetDatabaseDSN ());
				TRACEF (p->GetDatabaseTable ());
				TRACEF (p->GetDatabaseKeyField ());
				TRACEF ("");

				if (p->IsDatabaseLookup ()) {
					if (!p->GetDatabaseDSN ().CompareNoCase (dlgDB.m_strDSN) &&
						!p->GetDatabaseTable ().CompareNoCase (dlgDB.m_strTable) &&
						!p->GetDatabaseKeyField ().CompareNoCase (dlgDB.m_strKeyField))
					{
						dlgUser.m_vElements.RemoveAt (i);
					}
				}
			}
			else if (CExpDateElement * p = DYNAMIC_DOWNCAST (CExpDateElement, dlgUser.m_vElements [i].m_pElement)) { // sw0924
				if (p->GetType () == CExpDateElement::TYPE_DATABASE) {
					TRACEF (p->GetDSN ());
					TRACEF (p->GetTable ());
					TRACEF (p->GetKeyField ());
					TRACEF ("");

					if (!p->GetDSN ().CompareNoCase (dlgDB.m_strDSN) &&
						!p->GetTable ().CompareNoCase (dlgDB.m_strTable) &&
						!p->GetKeyField ().CompareNoCase (dlgDB.m_strKeyField))
					{
						dlgUser.m_vElements.RemoveAt (i);
					}
				}
			}
			#endif //__WINPRINTER__
		}
	}

	if (!dlgUser.m_vElements.GetSize ())
		MsgBox (* this, LoadString (IDS_NO_USER_ELEMENTS));
	else {
		if (dlgUser.DoModal () == IDOK) {
			CMapStringToString vUser;
			CString strKeyValue;

			if (theApp.GetProfileInt (_T ("sw0858"), _T ("db start"), 0)) 
				strKeyValue	= theApp.GetProfileString (_T ("sw0858"), _T ("Key value"));

			for (int i = 0; i < dlgUser.m_vElements.GetSize (); i++) {
				CString strPrompt = dlgUser.m_vElements [i].m_strPrompt;
				CString strData = dlgUser.m_vElements [i].m_strData;

				TRACEF (strPrompt + _T (" --> ") + strData);
				vUser.SetAt (strPrompt, strData);
				theApp.WriteProfileString (_T ("sw0858"), strPrompt, strData);
			}

			for (int i = 0; i < vLines.GetSize (); i++) {
				if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
					CPrintHeadList list;

					pLine->ChangeUserElementData (false, strKeyValue, &vUser);
					pLine->GetActiveHeads (list);

					for (POSITION pos = list.GetStartPosition(); pos; ) {
						DWORD dwKey;
						CHead * pHead = NULL;

						list.GetNextAssoc (pos, dwKey, (void *&)pHead);
						ASSERT (pHead);

						while (!pHead->PostThreadMessage (TM_SET_KEY_VALUE, (WPARAM)new CString (strKeyValue), 1)) 
							::Sleep (1);
					}
				}
			}

			OnViewRefreshpreview ();
		}
	}
}

void CControlView::ChangeCounts ()
{
	CControlDoc *pDoc = GetDocument();
	CString sLine = pDoc->GetSelectedLine()->GetName();
	
	if (CProdLine * pLine = pDoc->GetProductionLine (sLine)) {

#ifdef __WINPRINTER__
		using namespace FoxjetElements;

		Head::CElementPtrArray v;
		CHead::CHANGECOUNTSTRUCT * pCount = NULL;

		pLine->GetElements (v, COUNT);

		for (int i = 0; i < v.GetSize (); i++) {
			if (CCountElement * p = DYNAMIC_DOWNCAST (CCountElement, v [i])) {
				pCount = new CHead::CHANGECOUNTSTRUCT ();
				break;
			}
		}

		if (pCount) {
			__int64 l;

			pCount->m_bIrregularPalletSize = true;
			
			pLine->GetCounts (NULL, l, l, l, pCount->m_lCount);

			for (int i = 0; i < v.GetSize (); i++) {
				if (CCountElement * p = DYNAMIC_DOWNCAST (CCountElement, v [i])) {
					if (p->IsPalletCount ()) {
						pCount->m_lPalletCount			= p->GetPalCount ();
						pCount->m_lPalletMin			= p->GetPalMin ();
						pCount->m_lPalletMax			= p->GetPalMax ();
						pCount->m_lUnitsPerPallet		= p->GetMax ();
					}
				}
			}
		}

		if (!v.GetSize ())
			MsgBox (* this, LoadString (IDS_NO_COUNT_ELEMENTS));
		else 
			pLine->ChangeCounts (false, 0, 0, true, pCount);
#else
			pLine->ChangeCounts (false, 0, 0, true);
#endif
			
	}
}


BOOL CControlView::Create (LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, 
						   const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{
	CRect rc;

	BOOL bResult = CFormView::Create (lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	GetClientRect (&rc);

	ASSERT (m_pPreview == NULL);
	m_pPreview = (CImageView *)RUNTIME_CLASS (CImageView)->CreateObject ();
	ASSERT (m_pPreview != NULL);

	if (!m_pPreview->Create (NULL, NULL, dwStyle, rect, this, 12000, pContext)) {
		TRACEF ("Create failed");
		ASSERT (0);
	}

	m_pPreview->SendMessage (WM_INITIALUPDATE);

	CControlDoc * pDoc = GetDocument ();

	ASSERT (pDoc);
	m_pPreview->SetActiveDocument (pDoc);

	return bResult;
}

void CControlView::OnInitialUpdate()
{
	using namespace FoxjetCommon;

	CMainFrame *pFrame = (CMainFrame *)GetParentFrame();
	CPoint pt (0, 0);

	CFormView::OnInitialUpdate();

	#ifdef _DEBUG
	if (FoxjetDatabase::IsDebugDisplayAttached ()) 
		pt = CMainFrame::m_ptDebug;
	#endif

	pFrame->SetWindowPos (NULL, pt.x, pt.y, 0, 0, SWP_NOZORDER | SWP_NOSIZE | SWP_SHOWWINDOW);

	m_strTimeFormat [0] = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, ListGlobals::defElements.m_strRegSection + _T ("\\time"), _T ("format1"), GetDefTimeFormat (0));
	m_strTimeFormat [1] = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, ListGlobals::defElements.m_strRegSection + _T ("\\time"), _T ("format2"), GetDefTimeFormat (1));
	m_strTimeFormat [2] = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, ListGlobals::defElements.m_strRegSection + _T ("\\time"), _T ("format3"), GetDefTimeFormat (2));

	#ifdef _DEBUG
	{
		CString strCmdLine = ::GetCommandLine ();
		bool bDemoMemicPC = strCmdLine.Find (_T ("/NO_DEMO_PC")) == -1 ? true : false;
		int nShow = SW_SHOW;

		if (bDemoMemicPC)
			nShow = SW_HIDE;
	
		if (CWnd * p = GetDlgItem (CHK_PRINT1))
			p->ShowWindow (nShow);
		if (CWnd * p = GetDlgItem (CHK_PRINT2))
			p->ShowWindow (nShow);
	}
	#endif

	pFrame->m_pControlView = this;

	{
		DWORD dwStyle = ::GetWindowLong (((CMainFrame *)theApp.m_pMainWnd)->m_hWnd, GWL_STYLE);
		CSize size = GetDialogSize (FoxjetDatabase::IsMatrix () ? IDD_CONTROL_FORM_MATRIX : IDD_CONTROL_FORM);

		size.cx += ::GetSystemMetrics (SM_CXEDGE) * 2;
		size.cx += ::GetSystemMetrics (SM_CXFRAME) * 2;
			
		size.cy += ::GetSystemMetrics (SM_CYEDGE) * 2;
		size.cy += ::GetSystemMetrics (SM_CYFRAME) * 2;

		if (dwStyle & WS_CAPTION) {
			size.cy += ::GetSystemMetrics (SM_CYSIZE);
		}
		
		if (size.cy > ::GetSystemMetrics (SM_CYMAXIMIZED)) 
			size.cy = ::GetSystemMetrics (SM_CYMAXIMIZED);

		if (!FoxjetDatabase::IsMatrix ()) {
			if (HWND hwnd = FoxjetCommon::FindKeyboard ()) {
				CRect rc (0, 0, 0, 0);

				if (::GetWindowRect (hwnd, &rc)) {
					//size.cx = rc.Width () - ((::GetSystemMetrics (SM_CXEDGE) + ::GetSystemMetrics (SM_CXFRAME)) * 2);
					size.cy = rc.top;
				}
			}
		}

		if (!theApp.m_bDemo) 
			size.cx = 1024;
		else {
			CRect rcExit (0, 0, 0, 0);
			CRect rcStart (0, 0, 0, 0);
			CRect rcWin (0, 0, 0, 0);

			if (CWnd * p = GetDlgItem (BTN_START)) 
				p->GetWindowRect (&rcStart);

			if (CWnd * p = GetDlgItem (BTN_EXIT)) 
				p->GetWindowRect (&rcExit);

			GetWindowRect (&rcWin);

			CSize size = rcWin.Size ();

			size.cx = rcExit.right + rcStart.left;
		}

		SetWindowPos (NULL, 0, 0, size.cx, size.cy, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
		pFrame->SetWindowPos (NULL, 0, 0, size.cx, size.cy, SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
		m_sizeInit = size;

		if (!theApp.m_bDemo) {
			GetParentFrame()->RecalcLayout ();
			SetScaleToFitSize (size);
			ResizeParentToFit (FALSE);
		}
	}

	m_StrobeResetTime = CTime::GetCurrentTime();

	m_nTimerStatus		= SetTimer (TIMER_STATUS, 1000, NULL);
	m_nTimerTab			= SetTimer (TIMER_TAB, 2000, NULL);
	m_nTimerTime		= SetTimer (TIMER_TIME, 500, NULL);
	m_nTimerStartMenu	= SetTimer (TIMER_STARTMENU, 5000, NULL);

	m_fntTimer.CreateFont (25, 0, 0, 0, FW_NORMAL, FALSE, 0, 0,
		DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("Microsoft Sans Serif"));

	{
		UINT n [] = 
		{
			LBL_TIME1,
			LBL_TIME2,
			LBL_TIME3,
		};

		for (int i = 0; i < ARRAYSIZE (n); i++) {
			if (CWnd * p = GetDlgItem (n [i])) {
				p->SetFont (&m_fntTimer);
				p->ModifyStyle (SS_TYPEMASK, SS_OWNERDRAW);
			}
		}
	}

	if (theApp.IsDroppedElementTest ())
		m_nTimerRefresh	= SetTimer (TIMER_REFRESH, 1000, NULL);

	OnInitialStartup (0, 0); 
	//while (!PostMessage (WM_INITIAL_STARTUP)) ::Sleep(0);

	BeginCommThreads ();
	if (IsNetworked () && theApp.GetLocalDSN ().GetLength ()) 
		BeginLocalDbThreads ();

	if (m_pUdpThread = (CUdpThread *)AfxBeginThread (RUNTIME_CLASS (CUdpThread)))
		m_pUdpThread->m_pView = this;
	#ifdef _DEBUG
	else { MsgBox (_T ("failed to start CUdpThread")); }
	#endif

	InitInkCodes ();

	if (m_pServerThread = (CServerThread *)AfxBeginThread (RUNTIME_CLASS (CServerThread)))
		m_pServerThread->m_pView = this;
	#ifdef _DEBUG
	else { MsgBox (_T ("failed to start CServerThread")); }
	#endif

	const CString strCmdLine = ::GetCommandLine ();

	{
		CString str = strCmdLine;
		
		str.MakeLower ();

		//if (str.Find (_T ("/next")) != -1) 
		{
			using namespace NEXT;
	
			if (m_pNextThread = (CNextThread *)AfxBeginThread (RUNTIME_CLASS (CNextThread)))
				m_pNextThread->m_pView = this;
			#ifdef _DEBUG
			else { MsgBox (_T ("failed to start CNextThread")); }
			#endif
		}
	}


	// TODO: rem //FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, _T ("Software\\FoxJet\\BoxWriter ELITE"), _T ("InstallerRunning"), 0);

	if (theApp.IsKeyboardEnabled ()) {
		if (!FoxjetDatabase::IsRunning (ISKEYBOARDRUNNING)) {
			CString strCmdLine = FoxjetDatabase::GetHomeDir () + _T("\\Keyboard.exe");
			STARTUPINFO si;     
			PROCESS_INFORMATION pi; 
			TCHAR szCmdLine [0xFF];

			strCmdLine += _T (" ") + CString (FoxjetDatabase::IsMatrix () ? _T ("/matrix") : _T ("/elite"));

			_tcsncpy (szCmdLine, strCmdLine, 0xFF);
			memset (&pi, 0, sizeof(pi));
			memset (&si, 0, sizeof(si));
			si.cb = sizeof(si);     
			si.dwFlags = STARTF_USESHOWWINDOW;     
			si.wShowWindow = SW_SHOW;     

			m_bKeyboard = !FoxjetDatabase::IsRunning (ISKEYBOARDRUNNING);
			::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
		}
	}

	if (!IsGojo ()) {
		if (CButton * p = (CButton *)GetDlgItem (CHK_DBCONNECTION)) 
			p->ShowWindow (SW_HIDE);
	}

	theApp.UpdateUI ();
	OnUpdateLogin ();
	OnChangeProductLine ();
	pFrame->SetWindowPos (NULL, pt.x, pt.y, 0, 0, SWP_NOZORDER | SWP_NOSIZE | SWP_SHOWWINDOW);
	SetFocus ();
}

CHead * CControlView::StartRemoteHead (FoxjetDatabase::HEADSTRUCT & h)
{
	CHead * pResult = NULL;

#ifdef __IPPRINTER__
	if (!h.m_bRemoteHead) 
		return NULL;

	CControlDoc * pDoc = GetDocument();

	GlobalFuncs::StandardizeIPAddress (h.m_strUID);
	h.m_strUID.MakeUpper();
	ULONG lAddr = inet_addr (w2a (h.m_strUID));

	//TRACEF (h.m_strUID);
	CString strOwner = pDoc->LookupLineAssoc (h.m_strUID);

	if (strOwner.IsEmpty ()) 
		return NULL;

	if (CRemoteHead * pHead = (CRemoteHead *)AfxBeginThread (RUNTIME_CLASS(CRemoteHead), THREAD_PRIORITY_TIME_CRITICAL)) {
		CEvent init (false, true, _T ("TM_INITIAL_STARTUP: ") + ToString (pHead->m_nThreadID), NULL);

		TRACEF ("************************* set CHeadInfo::m_dwThreadID");

		while (!::PostThreadMessage (pHead->m_nThreadID, TM_INITIAL_STARTUP, (WPARAM)(HANDLE)init, h.m_lID)) 
			Sleep (0);

		DWORD dwWait = ::WaitForSingleObject (init, 5000);

		//if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
		if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
		if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
		if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");

		ASSERT (dwWait == WAIT_OBJECT_0);

		pDoc->AddActiveHead (strOwner, pHead);
		pResult = pHead;
	}

	pDoc->UpdateAllViews (NULL, UPDATE_LINE_CHANGE, NULL);

#endif //__IPPRINTER__

	return pResult;
}

bool CControlView::ReconnectDatabase (CHead * pHead)
{
	bool bResult = false;
	CHeadInfo info = CHead::GetInfo (pHead);

	if (theApp.m_Database.IsSqlServer ()) {
		FoxjetDatabase::COdbcDatabase * pdb = NULL;
	
		SEND_THREAD_MESSAGE (info, TM_GET_DB, &pdb);

		if (pdb) {
			CDbContext tmp (* pdb);

			if (!pdb->IsOpen ()) {
				if (FoxjetCommon::OpenDatabase (* pdb, theApp.GetDSN (), FoxjetCommon::GetDatabaseName())) {
					pdb->m_dwOwnerThreadID = info.GetThreadID ();
					SEND_THREAD_MESSAGE (info, TM_SET_TIME_DB, &theApp.m_tmDatabase);
					bResult = true;
				}
			}
		}
	}

	return bResult;
}

bool CControlView::BeginThread (CHead::TM_SET_IO_ADDRESS_STRUCT * pParam)
{
	CControlDoc *pDoc = GetDocument();
	bool bResult = false;
	const DWORD dwTimeout = 20 * 1000;
	CHead * pHead = pParam->m_pHead; // pParam will be invalid (deleted) after TM_SET_IO_ADDRESS returns
	HANDLE hEvent = pParam->m_hEvent;

	while (!PostThreadMessage (pHead->m_nThreadID, TM_SET_IO_ADDRESS, (WPARAM)pParam, 0))
		::Sleep (0);

	DWORD dwWait = ::WaitForSingleObject (hEvent, dwTimeout);

	::CloseHandle (hEvent);

	if (dwWait != WAIT_OBJECT_0) {
		CString str = CDiagnosticSingleLock::TraceAll (_T (__FILE__), __LINE__);
		TRACEF (str);
	}

	ASSERT (dwWait == WAIT_OBJECT_0);

	CHeadInfo info = CHead::GetInfo (pHead);

	if (dwWait == WAIT_OBJECT_0) {
		CString strLine = pDoc->LookupLineAssoc (info.GetUID ());

		pDoc->AddActiveHead (strLine, pHead);
		bResult = true;
	}

	//if (dwWait == WAIT_OBJECT_0)	TRACEF (info.GetFriendlyName () + ": WAIT_OBJECT_0");
	if (dwWait == WAIT_TIMEOUT)		TRACEF (info.GetFriendlyName () + ": WAIT_TIMEOUT");
	if (dwWait == WAIT_ABANDONED)	TRACEF (info.GetFriendlyName () + ": WAIT_ABANDONED");
	if (dwWait == WAIT_FAILED)		TRACEF (info.GetFriendlyName () + ": WAIT_FAILED");

	return bResult;
}


LRESULT CControlView::OnColorsChanged (WPARAM wParam, LPARAM lParam)
{
	using namespace FoxjetUtils;

	COLORREF rgbFont			= FoxjetUtils::GetButtonColor (_T ("button font"));
	COLORREF rgbFace			= FoxjetUtils::GetButtonColor (_T ("button face"));
	COLORREF rgbBorder			= FoxjetUtils::GetButtonColor (_T ("button border"));
	COLORREF rgbBack			= FoxjetUtils::GetButtonColor (_T ("background"));
	COLORREF rgbShaded			= FoxjetUtils::GetButtonColor (_T ("shaded"));
	COLORREF rgbClickedFace		= FoxjetUtils::GetButtonColor (_T ("button face clicked"));
	COLORREF rgbClickedBorder	= FoxjetUtils::GetButtonColor (_T ("button border clicked"));

	theApp.ColorsChanged ();

	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}
	m_hbrDlg = ::CreateSolidBrush (rgbBack);

	if (m_hbrDlgDisabled) {
		::DeleteObject (m_hbrDlgDisabled);
		m_hbrDlgDisabled = NULL;
	}
	m_hbrDlgDisabled = ::CreateSolidBrush (rgbShaded);

	{
		tButtonStyle tStyle;
		//LOGBRUSH lb = { 0 };

		//::GetObject (m_hbrDlg, sizeof (lb), &lb); 

		m_styleDefault.GetButtonStyle (&tStyle);

		tStyle.m_tColorFace.m_tEnabled		= rgbFace;
		tStyle.m_tColorBorder.m_tEnabled	= rgbBorder;

		tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
		tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);

		tStyle.m_tColorFace.m_tPressed		= rgbClickedFace;
		tStyle.m_tColorBorder.m_tPressed	= rgbClickedBorder;

		tStyle.m_tColorBack.m_tEnabled		= 
		tStyle.m_tColorBack.m_tClicked		= 
		tStyle.m_tColorBack.m_tPressed		= 
		tStyle.m_tColorBack.m_tDisabled		= 
		tStyle.m_tColorBack.m_tHot			= rgbBack;//lb.lbColor;//::GetSysColor (COLOR_BTNFACE);

		m_styleDefault.SetButtonStyle (&tStyle);
	}

	{
		FoxjetUtils::CRoundButton2 * map [] = 
		{
			&m_btnStart,
			&m_btnIdle,
			&m_btnIdle,
			&m_btnStop,
			&m_btnEdit,
			&m_btnLogin,
			&m_btnConfig,
			&m_btnUser,
			&m_btnCount,
			&m_btnInfo,
			&m_btnBox,
			&m_btnExit,
			&m_btnReport,
			&m_btnZoomFit,
			&m_btnZoomIn,
			&m_btnZoomOut,
			&m_btnLimitedConfig,
		};
		tColorScheme tColor = { Color::rgbLightGray, rgbFont, rgbFont, rgbFont, rgbFont };

		for (int i = 0; i < ARRAYSIZE (map); i++) {
			map [i]->SetRoundButtonStyle(&m_styleDefault);
			map [i]->SetTextColor (&tColor);
		};

		Invalidate ();
		RedrawWindow ();
	}

	return 0;
}

LRESULT CControlView::OnInitialStartup(WPARAM wParam, LPARAM lParam)
{
	using namespace FoxjetUtils;

	CControlDoc *pDoc = GetDocument();
	CArray <CHead *, CHead *> waitingHeadList;
	CArray <CHead::TM_SET_IO_ADDRESS_STRUCT *, CHead::TM_SET_IO_ADDRESS_STRUCT *> v;

	m_bmpStrobe.LoadBitmap (IDB_STROBE);
	{
		CxImage imgRed, imgYellow, imgGreen, imgGray;

		imgRed.CreateFromHBITMAP (m_bmpStrobe);
		imgYellow.CreateFromHBITMAP (m_bmpStrobe);
		imgGreen.CreateFromHBITMAP (m_bmpStrobe);
		imgGray.CreateFromHBITMAP (m_bmpStrobe);
		
		imgRed.GrayScale ();
		imgYellow.GrayScale ();
		imgGreen.GrayScale ();
		imgGray.GrayScale ();

		imgRed.Colorize		(0,		255, 1.0);
		imgYellow.Colorize	(44,	255, 1.0);
		imgGreen.Colorize	(66,	255, 1.0);

		m_bmpStrobeRed.Attach (imgRed.MakeBitmap ());
		m_bmpStrobeYellow.Attach (imgYellow.MakeBitmap ());
		m_bmpStrobeGreen.Attach (imgGreen.MakeBitmap ());
		m_bmpStrobeGray.Attach (imgGray.MakeBitmap ());
	}

	m_bmpStart.LoadBitmap	(IDB_START);
	m_bmpIdle.LoadBitmap	(IDB_IDLE);
	m_bmpResume.LoadBitmap	(IDB_RESUME);
	m_bmpStop.LoadBitmap	(IDB_STOP);
	m_bmpLogin.LoadBitmap	(IDB_LOGIN);
	m_bmpLogout.LoadBitmap	(IDB_LOGOUT);

	m_btnStart.SetBitmap	(m_bmpStart);
	m_btnIdle.SetBitmap		(m_bmpIdle);
	m_btnStop.SetBitmap		(m_bmpStop);
	m_btnEdit.LoadBitmap	(IDB_EDIT);
	m_btnLogin.SetBitmap	(m_bmpLogin);
	m_btnConfig.LoadBitmap	(IDB_CONFIG);
	m_btnUser.LoadBitmap	(IDB_USER);
	m_btnCount.LoadBitmap	(IDB_COUNT);
	m_btnInfo.LoadBitmap	(IDB_INFO);
	m_btnBox.LoadBitmap		(IDB_BOX);
	m_btnExit.LoadBitmap	(IDB_EXIT);
	m_btnReport.LoadBitmap	(IDB_REPORT);
	m_btnZoomFit.LoadBitmap	(IDB_ZOOMFIT);
	m_btnZoomIn.LoadBitmap	(IDB_ZOOMIN);
	m_btnZoomOut.LoadBitmap	(IDB_ZOOMOUT);
	m_btnLimitedConfig.LoadBitmap (IDB_LIMITED_CONFIG);

	OnColorsChanged (0, 0);

	if (theApp.IsDroppedElementTest ())
		OnGenImageDebug (1, 0);

	FillLineCtrl ();

#ifdef __IPPRINTER__
	{
		CMapStringToPtr v;

		pDoc->GetProductLines (v);

		for (POSITION pos = v.GetStartPosition(); pos; ) {
			CProdLine * pLine;
			CString strLine;
		
			v.GetNextAssoc (pos, strLine, (void *&)pLine);
			
			if (FoxjetDatabase::GetLineHeads (theApp.m_Database, pLine->GetID(), pLine->m_vHeads)) {
				for ( int i= 0; i < pLine->m_vHeads.GetSize(); i++) {
					FoxjetDatabase::HEADSTRUCT h = pLine->m_vHeads [i];

					StartRemoteHead (h);
				}
			}
		}
	}
#else
	CULongArray vAddr = theApp.GetDriverAddressList ();
	CString sAddress;

	if (!m_pProgressDlg)
		m_pProgressDlg = new CProgressDlg (this);

	for (int i = 0; i < vAddr.GetSize (); i++) {
		ULONG headID; 
		ULONG lAddress = vAddr [i];

		sAddress.Format (_T ("%X"), lAddress );
		TRACEF (sAddress);
		
		{
			MSG msg;

			while (::PeekMessage (&msg, NULL, 0, 0, PM_REMOVE)) {
				if (msg.message == WM_QUIT)
					break;

				::TranslateMessage (&msg);
				::DispatchMessage (&msg);
			}
		}

		if ((headID = FoxjetDatabase::GetHeadID (theApp.m_Database, sAddress, GetPrinterID (theApp.m_Database))) != 0)
		{
			CString sProdLine = pDoc->LookupLineAssoc (sAddress);
			CProdLine * pLine = pDoc->GetProductionLine (sProdLine);

			if ( !sProdLine.IsEmpty() )
			{
				FoxjetDatabase::HEADSTRUCT HS;
				if (FoxjetDatabase::GetHeadRecord( theApp.m_Database, headID, HS)) {
					if (!IsHpHead (HS)) {
						if (CLocalHead * pHead = (CLocalHead *)AfxBeginThread (RUNTIME_CLASS (CLocalHead))) {
							CHead::TM_SET_IO_ADDRESS_STRUCT * pParam = new CHead::TM_SET_IO_ADDRESS_STRUCT;
							CString str;

							::Sleep (0);
							str.Format (_T ("Initialization Complete %lu"), pHead->m_hThread);

							pParam->m_pHead		= pHead;
							pParam->m_hEvent	= CreateEvent (NULL, true, false, str);
							pParam->m_lLineID	= pLine->GetID ();
							pParam->m_lAddress	= lAddress;
							pParam->m_lHeadID	= HS.m_lID;

							str.Format (LoadString (IDS_LOCAL_HEAD_INITIALIZING), HS.m_strName);
							UPDATE_STATUS (* m_pProgressDlg, pHead->m_nThreadID, str);

							v.Add (pParam);
						}
					}
				}
			}
			else
			{
				CString sMsg;
				sMsg.Format (LoadString (IDS_HEADHASNOLINE), sAddress);
				MsgBox (* this, sMsg );
			}
		}
	}

#endif

	ConnectDatabases (v);

	if (theApp.m_dbstart.m_strDSN.GetLength ()) {
		CString strSection = _T ("SOFTWARE\\ODBC\\ODBC.INI\\") + theApp.m_dbstart.m_strDSN;
		CString strDriver = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strSection, _T ("Driver"), _T (""));

		COdbcCache db = COdbcDatabase::Find (theApp.m_dbstart.m_strDSN);
		TRACEF (db.GetDB ().GetConnect ());
	}

	#ifdef __HPHEAD__
	{
		CMapStringToPtr v;

		pDoc->GetProductLines (v);

		for (POSITION pos = v.GetStartPosition(); pos; ) { 
			CProdLine * pLine = NULL;
			CString str;

			v.GetNextAssoc (pos, str, (void *&)pLine);

			ASSERT (pLine);

			CArray <HEADSTRUCT, HEADSTRUCT &> v;

			GetLineHeads (theApp.m_Database, pLine->GetID (), v);

			for (int i = 0; i < v.GetSize (); i++) {
				HEADSTRUCT h = v [i];

				if (IsHpHead (h.m_nHeadType)) {
					if (CHpHead * pHead = (CHpHead *)AfxBeginThread (RUNTIME_CLASS (CHpHead))) {
						CHead::TM_SET_IO_ADDRESS_STRUCT * pParam = new CHead::TM_SET_IO_ADDRESS_STRUCT;
						CString str;

						::Sleep (0);
						str.Format (_T ("Initialization Complete %lu"), pHead->m_hThread );

						pParam->m_pHead		= pHead;
						pParam->m_hEvent	= CreateEvent (NULL, true, false, str);
						pParam->m_pLine		= pLine;
						pParam->m_lAddress	= lAddress;
						pParam->m_lHeadID	= h.m_lID;

						str.Format (LoadString (IDS_LOCAL_HEAD_INITIALIZING), HS.m_strName);
						UPDATE_STATUS (* m_pProgressDlg, pHead->m_nThreadID, str);

						v.Add (pParam);
					}
				}
			}
		}
	}
	#endif //__HPHEAD__

	{
		for (int i = 0; i < v.GetSize (); i++) {
			CHead::TM_SET_IO_ADDRESS_STRUCT * pParam = v [i];

			if (!BeginThread (pParam)) 
				waitingHeadList.Add (pParam->m_pHead);
		}

		for (int i = 0; i < v.GetSize (); i++) {
			CStringArray vLines;

			pDoc->GetProductLines (vLines);

			for (int i = 0; i < vLines.GetSize (); i++) {
				if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
					CPrintHeadList listHeads;

					pLine->GetActiveHeads (listHeads);

					for (POSITION pos = listHeads.GetStartPosition(); pos; ) {
						ULONG lKey;
						CHead * pHead = NULL;

						listHeads.GetNextAssoc (pos, lKey, (void *&)pHead);
						
						if (pHead) {
							CHead::CONFIGCHANGESTRUCT config;

							pDoc->GetProductLines (config.m_vLines);
							pLine->GetActiveHeads (config.m_listHeads);
							CHeadInfo info = CHead::GetInfo (pHead);

							//config.m_pSerialData = pLine->GetSerialData ();
							config.m_serialData.m_Len = pLine->GetSerialData ()->m_Len;
							memcpy (config.m_serialData.Data, pLine->GetSerialData ()->Data, ARRAYSIZE (config.m_serialData.Data) * sizeof (config.m_serialData.Data [0]));
							config.m_pSw0848Thread = pLine->m_pSw0848Thread;
							SEND_THREAD_MESSAGE (info, TM_HEAD_CONFIG_CHANGED, (WPARAM)&config);
						}
					}
				}
			}
		}
	}

#ifdef __WINPRINTER__

	if (waitingHeadList.GetSize () != 0) {
		CString str = LoadString (IDS_ADDRESSESFAILEDINIT);

		for (int i = 0; i < waitingHeadList.GetSize (); i++ ) {
			if (CHead * pHead = waitingHeadList [i]) {

				str += _T ("\n") + CHead::GetInfo (pHead).GetUID ();
				
				while (!::PostThreadMessage (pHead->m_nThreadID, TM_KILLTHREAD, 0, 0))
					::Sleep (0);
			}
		}

		MsgBox (* this, str, LoadString (IDS_ERROR), MB_ICONERROR);
	}

	SetFocus();
#endif

	if (m_pPreview)
		m_pPreview->OnHeadConfigUpdated();

	pDoc->UpdateAllViews(NULL, UPDATE_LINE_CHANGE, NULL);

	{
		CString str;
	
		if (!DoHeadConfigCheck (str)) {
			str = LoadString (IDS_HEADCONFIGCHECKFAILED) + str;
			TRACEF (str);
			MsgBox (* this, str);
			((CMainFrame *)theApp.m_pMainWnd)->PostMessage (WM_COMMAND, MAKEWPARAM (IDM_CFG_PRINTHEAD, 0));
		}

		TRACEF (str);
	}

	//{
	//	DWORD dw = 0;
	//	HANDLE hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)ColorFunc, (LPVOID)this, 0, &dw);
	//}

	return 0L;
}

bool CControlView::DoHeadConfigCheck (CString & str)
{
#ifdef _DEBUG
	TRACEF (" **** TODO: rem *****");
	return true;
#endif

	CULongArray vAddr = theApp.GetDriverAddressList ();
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
	bool bResult = true;
	ULONG lPrinterID = GetPrinterID (theApp.m_Database);

	FoxjetDatabase::GetPrinterHeads (theApp.m_Database, lPrinterID, vHeads); //GetHeadRecords (theApp.m_Database, vHeads);

	for (int i = 0; i < vHeads.GetSize (); i++) {
		HEADSTRUCT & h = vHeads [i];
		CString strHead;
		
		ULONG lAddr = _tcstoul (h.m_strUID, NULL, 16);
		int nIndex = Find (vAddr, lAddr);

		strHead.Format (_T ("%s\t[0x%03X]\t"), h.m_strName, lAddr);
		str += strHead;

		TRACEF (strHead);

		if (nIndex != -1) {
			vAddr.RemoveAt (nIndex);
			strHead.Format (_T ("[0x%03X]"), lAddr);
			str += strHead;
		}
		else {
			if (h.m_bEnabled) { 
				{ CString str;  str.Format (_T ("lAddr: 0x%03X not availible"), lAddr); TRACEF (str); }
				strHead.Format (_T ("[NOT FOUND]"), lAddr);
				str += strHead;
				bResult = false;
			}
		}

		str += _T ("\n");
	}

	return bResult;
}

void CControlView::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CFormView::OnShowWindow(bShow, nStatus);
}

void CControlView::Resize ()
{
	/*
	if (m_hWnd) {
		CRect rc;

		UINT n [] = 
		{
			BTN_START,
			BTN_IDLE,
			BTN_STOP,
			BTN_COUNT,
			BTN_USER,
			BTN_EDIT,
			BTN_CONFIG,
			BTN_REPORTS,
			BTN_EXIT,
			//BTN_INFO,
			//BTN_LOGIN,
		};

		GetWindowRect (rc);

		int nIncrement	= (int)floor (rc.Width () / (double)ARRAYSIZE (n)) - 1;
		int cx			= (int)(floor (rc.Width () / (double)ARRAYSIZE (n)) * 0.9) - 1;
		int x			= rc.left;

		for (int i = 0; i < ARRAYSIZE (n); i++) {
			if (CWnd * p = GetDlgItem (n [i])) {
				if (::IsWindow (p->m_hWnd)) {
					{
						CRect rcTmp;

						p->GetWindowRect (rcTmp);
						int cx = rcTmp.Width ();
						int cy = rcTmp.Height ();

						// 112 x 122 --> 56 x 56 --> 99 x 99
					}

					p->SetWindowPos (NULL, x + ((nIncrement - cx) / 2), 0, cx, cx, SWP_NOZORDER | SWP_SHOWWINDOW);
					x += nIncrement + 1;
				}
			}
		}
	}
	*/

	if (m_pPreview && m_pPreview->m_hWnd) 
		m_pPreview->SetRedraw (FALSE);

	if (m_pPreview && m_pPreview->m_hWnd) {
		m_pPreview->ShowWindow (m_bPreview ? SW_SHOW : SW_HIDE);

		if (CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (TAB_LINE)) {
			CRect rc, rcItem;

			pTab->GetItemRect (0, rcItem);
			pTab->GetWindowRect (&rc);

			rc.right -= 30;
			rc.bottom -= 50;

			if (CWnd * p = GetDlgItem (BTN_BOX)) {
				CRect rcTmp;

				p->GetWindowRect (rcTmp);
				rc.left = rcTmp.right + 10;
				rc.top = rcTmp.top;
			}

			if (FoxjetDatabase::IsMatrix ()) {
				if (CWnd * p = GetDlgItem (BTN_ZOOMOUT)) {
					CRect rcTmp;

					p->GetWindowRect (rcTmp);
					rc.bottom = rcTmp.bottom;
				}
			}

			ScreenToClient (rc);
			m_pPreview->SetWindowPos (&CWnd::wndTop, rc.left, rc.top, rc.Width (), rc.Height (), SWP_SHOWWINDOW);
		}

		m_pPreview->SetRedraw (TRUE);
	}
}

void CControlView::OnSize(UINT nType, int cx, int cy) 
{
//	CFormView::OnSize(nType, cx, cy);
	Resize ();
}

#ifdef _DEBUG
CString GetHeadProcessingWParam (WPARAM wParam)
{
	struct
	{
		WPARAM m_wParam;
		LPCTSTR m_lpsz;
	} static const map [] =
	{
		{ STARTING_NICELABEL,		_T ("STARTING_NICELABEL		") },
		{ FIRMWARE_UPDATE,			_T ("FIRMWARE_UPDATE		") },
		{ GA_UPDATE,				_T ("GA_UPDATE				") },
		{ LOCAL_HEAD_INITIALIZING,	_T ("LOCAL_HEAD_INITIALIZING") },
		{ VERIFYING_IP_DATA,		_T ("VERIFYING_IP_DATA		") },
		{ WAITING_FOR_SAVE_ACK,		_T ("WAITING_FOR_SAVE_ACK	") },
		{ LABEL_CLEANUP,			_T ("LABEL_CLEANUP			") },
		{ STARTING_MESSAGE,			_T ("STARTING_MESSAGE		") },
		{ UPDATING_MESSAGES,		_T ("UPDATING_MESSAGES		") },
		{ UPDATING_FONTS,			_T ("UPDATING_FONTS			") },
		{ UPDATING_STATUS_MSG,		_T ("UPDATING_STATUS_MSG	") },
		{ UPDATING_FINISHED,		_T ("UPDATING_FINISHED		") },
	};

	for (int i = 0; i < ARRAYSIZE (map); i++) 
		if (map [i].m_wParam == wParam)
			return map [i].m_lpsz;

	return _T ("");
}
#endif

void CALLBACK CControlView::DisplayStatus (const CString & strDisplay, bool bDisplay)
{
	WPARAM wParam = bDisplay ? ODBC_STATUS_DISPLAY : ODBC_STATUS_FINISHED;
	HWND hWnd = NULL;
	
	if (CMainFrame * pFrame = (CMainFrame *)theApp.m_pMainWnd) 
		if (CControlView * pView = (CControlView * )pFrame->m_pControlView) 
			hWnd = pView->m_hWnd;

	if (hWnd) {
		DWORD dw = 0;

		LRESULT lResult = ::SendMessageTimeout (hWnd, WM_HEAD_PROCESSING, wParam, (LPARAM)&strDisplay, SMTO_NORMAL, 500, &dw);
	}
}

LRESULT CControlView::OnHeadProcessing(WPARAM wParam, LPARAM lParam)
{
	//CCriticalSection csLock;
	//LOCK (csLock);

	CHead *pHead = (CHead *) lParam;
	CControlDoc *pDoc = GetDocument();
	CString sLine;
	CMainFrame *pFrame = (CMainFrame *) theApp.m_pMainWnd;

	if (CProdLine * pLine = pDoc->GetSelectedLine ())
		sLine = pLine->GetName();

	//TRACEF (pHead->GetFriendlyName () + ": " + GetHeadProcessingWParam (wParam));

	CString sMsg;
	switch ( wParam )
	{
		case ODBC_STATUS_DISPLAY:
			if (CString * pstrMsg = (CString *)lParam) {
				if (m_pProgressDlg == NULL)
					m_pProgressDlg = new CProgressDlg (this);
				SetDlgItemText (IDC_STATUS, * pstrMsg);
				UPDATE_STATUS (* m_pProgressDlg, -1, * pstrMsg);
			}
			break;
		case ODBC_STATUS_FINISHED:
			if (m_pProgressDlg) {
				if (CLEAR_STATUS (* m_pProgressDlg, -1) == 0) {
					delete m_pProgressDlg;
					m_pProgressDlg = NULL;
				}
			}
			SetDlgItemText (IDC_STATUS, LoadString (IDS_STATUS));
			break;
		case STARTING_NICELABEL:
			if ( m_pProgressDlg == NULL )
				m_pProgressDlg = new CProgressDlg(this);
			sMsg.Format (LoadString (IDS_STARTINGNICELABEL), CHead::GetInfo (pHead).m_Config.m_Line.m_strName );
			SetDlgItemText (IDC_STATUS, sMsg);
			UPDATE_STATUS (* m_pProgressDlg, pHead->m_nThreadID, sMsg );
			break;
		case FIRMWARE_UPDATE:
			if ( m_pProgressDlg == NULL )
				m_pProgressDlg = new CProgressDlg(this);
			sMsg.Format (LoadString (IDS_FIRMWARE_UPDATE), CHead::GetInfo (pHead).GetFriendlyName() );
			SetDlgItemText (IDC_STATUS, sMsg);
			UPDATE_STATUS (* m_pProgressDlg, pHead->m_nThreadID, sMsg );
			break;
		case GA_UPDATE:
			if ( m_pProgressDlg == NULL )
				m_pProgressDlg = new CProgressDlg(this);
			sMsg.Format (LoadString (IDS_GA_UPDATE), CHead::GetInfo (pHead).GetFriendlyName() );
			SetDlgItemText (IDC_STATUS, sMsg);
			UPDATE_STATUS (* m_pProgressDlg, pHead->m_nThreadID, sMsg );
			break;
		case LOCAL_HEAD_INITIALIZING:
			if ( m_pProgressDlg == NULL )
				m_pProgressDlg = new CProgressDlg(this);
			sMsg.Format (LoadString (IDS_LOCAL_HEAD_INITIALIZING), CHead::GetInfo (pHead).GetFriendlyName() );
			SetDlgItemText (IDC_STATUS, sMsg);
			UPDATE_STATUS (* m_pProgressDlg, pHead->m_nThreadID, sMsg );
			break;
		case VERIFYING_IP_DATA:
			if ( m_pProgressDlg == NULL )
				m_pProgressDlg = new CProgressDlg(this);
			sMsg.Format (LoadString (IDS_VERIFYING_IP_DATA), CHead::GetInfo (pHead).GetFriendlyName() );
			SetDlgItemText (IDC_STATUS, sMsg);
			UPDATE_STATUS (* m_pProgressDlg, pHead->m_nThreadID, sMsg );
			break;
		case WAITING_FOR_SAVE_ACK:
			if ( m_pProgressDlg == NULL )
				m_pProgressDlg = new CProgressDlg(this);
			sMsg.Format (LoadString (IDS_WAITING_FOR_SAVE_ACK), CHead::GetInfo (pHead).GetFriendlyName() );
			SetDlgItemText (IDC_STATUS, sMsg);
			UPDATE_STATUS (* m_pProgressDlg, pHead->m_nThreadID, sMsg );
			break;
		case LABEL_CLEANUP:
			if ( m_pProgressDlg == NULL )
				m_pProgressDlg = new CProgressDlg(this);
			sMsg.Format (LoadString (IDS_LABEL_CLEANUP), CHead::GetInfo (pHead).GetFriendlyName() );
			SetDlgItemText (IDC_STATUS, sMsg);
			UPDATE_STATUS (* m_pProgressDlg, pHead->m_nThreadID, sMsg );
			break;
		case STARTING_MESSAGE:
//				if ( m_pProgressDlg == NULL )
//					m_pProgressDlg = new CProgressDlg;
			sMsg.Format (LoadString (IDS_STARTING_MESSAGE), CHead::GetInfo (pHead).GetFriendlyName() );
			SetDlgItemText (IDC_STATUS, sMsg);
//				UPDATE_STATUS (* m_pProgressDlg, pHead->m_nThreadID, sMsg );
			break;
		case UPDATING_MESSAGES:
			sMsg = CHead::GetInfo (pHead).GetDisplayMessage ();

			if (!sMsg.GetLength ())
				sMsg.Format (LoadString (IDS_UPDATING_MESSAGES), CHead::GetInfo (pHead).GetFriendlyName() );

			if (sMsg.GetLength ()) {
				if ( m_pProgressDlg == NULL )
					m_pProgressDlg = new CProgressDlg(this);

				SetDlgItemText (IDC_STATUS, sMsg);
				UPDATE_STATUS (* m_pProgressDlg, pHead->m_nThreadID, sMsg);
			}
			break;
		case UPDATING_FONTS:
			sMsg = CHead::GetInfo (pHead).GetDisplayMessage ();

			if (!sMsg.GetLength ())
				sMsg.Format (LoadString (IDS_UPDATING_FONTS), CHead::GetInfo (pHead).GetFriendlyName() );

			if (sMsg.GetLength ()) {
				if (m_pProgressDlg == NULL)
					m_pProgressDlg = new CProgressDlg(this);

				SetDlgItemText (IDC_STATUS, sMsg);
				UPDATE_STATUS (* m_pProgressDlg, pHead->m_nThreadID, sMsg);
			}
			break;
		case UPDATING_STATUS_MSG:
			sMsg = CHead::GetInfo (pHead).GetDisplayMessage ();

			if (sMsg.GetLength ()) {
				if ( m_pProgressDlg == NULL )
					m_pProgressDlg = new CProgressDlg(this);

				SetDlgItemText (IDC_STATUS, sMsg);
				UPDATE_STATUS (* m_pProgressDlg, pHead->m_nThreadID, sMsg);
			}
			break;
		case UPDATING_CONFIG:
			if ( m_pProgressDlg == NULL )
				m_pProgressDlg = new CProgressDlg(this);

			sMsg.Format (LoadString (IDS_UPDATING_CONFIG), CHead::GetInfo (pHead).GetFriendlyName() );
			SetDlgItemText (IDC_STATUS, sMsg);
			UPDATE_STATUS (* m_pProgressDlg, pHead->m_nThreadID, sMsg);
			break;
		case UPDATING_FINISHED:
			if (m_pProgressDlg) {
				if (CLEAR_STATUS (* m_pProgressDlg, pHead->m_nThreadID) == 0) {
					delete m_pProgressDlg;
					m_pProgressDlg = NULL;
				}
			}
			SetDlgItemText (IDC_STATUS, LoadString (IDS_STATUS));

			CString sTitle;
			DWORD dwLastError = 0;
			CHeadInfo info = CHead::GetInfo (pHead);

			SEND_THREAD_MESSAGE (info, TM_GET_LASTERROR, &dwLastError);

			switch (dwLastError) 
			{
				case START_EDIT_SESSION_LOCKED:
					#ifdef __IPPRINTER__
					{
						CString strOwner = ((CRemoteHead* )pHead)->m_sEditSessionOwner;
						sTitle = LoadString (IDS_ERROR);
						sMsg.Format (LoadString (IDS_START_EDIT_SESSION_LOCKED), info.GetFriendlyName() , strOwner);
					}
					#endif
					break;
				case START_EDIT_SESSION_FAILED_NO_RESPONCE:
					sTitle = LoadString (IDS_ERROR);
					sMsg.Format (LoadString (IDS_START_EDIT_SESSION_FAILED_NO_RESPONCE), CHead::GetInfo (pHead).GetFriendlyName() );
					break;
				case START_EDIT_SESSION_FAILED_TIMEOUT:
					sTitle = LoadString (IDS_ERROR);
					sMsg.Format (LoadString (IDS_START_EDIT_SESSION_FAILED_TIMEOUT), CHead::GetInfo (pHead).GetFriendlyName() );
					break;
				case START_EDIT_SESSION_FAILED_SEND:
					sTitle = LoadString (IDS_ERROR);
					sMsg.Format (LoadString (IDS_START_EDIT_SESSION_FAILED_SEND), CHead::GetInfo (pHead).GetFriendlyName());
					break;
				case RETREIVE_LABELNAMES_FALIED:
					sTitle = LoadString (IDS_ERROR);
					sMsg.Format (LoadString (IDS_RETREIVE_LABELNAMES_FALIED), CHead::GetInfo (pHead).GetFriendlyName() );
					break;
				case REBOOT_PRINTER:
					sTitle = LoadString (IDS_NOTICE);
					sMsg.Format (LoadString (IDS_REBOOT_PRINTER), CHead::GetInfo (pHead).GetFriendlyName() );
					break;
				case FIRMWARE_UPDATE_FAILED:
					sTitle = LoadString (IDS_ERROR);
					sMsg.Format (LoadString (IDS_FIRMWARE_UPDATE_FAILED), CHead::GetInfo (pHead).GetFriendlyName() );
					break;
				case GA_UPDATE_FAILED:
					sTitle = LoadString (IDS_ERROR);
					sMsg.Format (LoadString (IDS_GA_UPDATE_FAILED), CHead::GetInfo (pHead).GetFriendlyName() );
					break;
				case FPGA_PROGRAMMING_ERROR:
					sTitle = LoadString (IDS_ERROR);
					sMsg.Format (LoadString (IDS_FPGA_PROGRAMMING_ERROR), CHead::GetInfo (pHead).GetFriendlyName() );
					break;
				case CLAIM_ADDRESS_FAILED:
					sTitle = LoadString (IDS_ERROR);
					sMsg.Format (LoadString (IDS_CLAIM_ADDRESS_FAILED), CHead::GetInfo (pHead).GetFriendlyName() );
					break;
				case SAVE_EDIT_SESSION_FAILED:
					sTitle = LoadString (IDS_ERROR);
					sMsg.Format (LoadString (IDS_SAVE_EDIT_SESSION_FAILED), CHead::GetInfo (pHead).GetFriendlyName() );
					break;
				case TRANSFER_FAILED:
					sTitle = LoadString (IDS_ERROR);
					sMsg.Format (LoadString (IDS_TRANSFER_FAILED), CHead::GetInfo (pHead).GetFriendlyName() );
					break;
				case BITMAP_TRANSFER_FAILED:
					sTitle = LoadString (IDS_ERROR);
					sMsg.Format (LoadString (IDS_BITMAP_TRANSFER_FAILED), CHead::GetInfo (pHead).GetFriendlyName() );
					break;
				case FONT_TRANSFER_FAILED:
					sTitle = LoadString (IDS_ERROR);
					sMsg.Format (LoadString (IDS_FONT_TRANSFER_FAILED), CHead::GetInfo (pHead).GetFriendlyName() );
					break;
				case INVALID_PARAM:
					sTitle = LoadString (IDS_ERROR);
					sMsg.Format (LoadString (IDS_INVALID_PARAM), CHead::GetInfo (pHead).GetFriendlyName() );
					break;
			}
			if ( !sMsg.IsEmpty() )
			{
				if (!sTitle.CompareNoCase (LoadString (IDS_ERROR)))
					MsgBox (* this, sMsg, sTitle, MB_OK|MB_ICONERROR);
				else
					MsgBox (* this, sMsg, sTitle, MB_OK|MB_ICONINFORMATION);
			}
			break;
	}

	return 0L;
}


#if __CUSTOM__ == __SW0816__

#define __SW0816_DSN__			"sw0816"
#define __SW0816_TASK__			"Task"
#define __SW0816_TABLE__		"ShopOrders"
#define __SW0816_SHOPORDERS__	"ShopOrder"

int CControlView::OnSerialShopOrder (CSerialPacket & packet, CProdLine & line)
{
	DECLARETRACECOUNT (20);

	using namespace FoxjetDatabase;
	using namespace FoxjetElements;

	int nResult = 0;

	if (packet.m_eDeviceType == HOSTINTERFACE) {
		CString strData = packet.m_pData;

		if (strData.GetLength () > 2) {
			if (strData [0] == DC1) {
				CString strOrder = strData.Mid (1);
				CString strTask;
				CString strException;

				TRACEF (strOrder);

				try
				{
					COdbcDatabase db;

					MARKTRACECOUNT ();

					if (db.Open (__SW0816_DSN__, FALSE, TRUE)) {
						COdbcRecordset rst (db);
						CString strSQL;

						strSQL.Format ("SELECT * FROM [%s] WHERE [%s]=%s;",
							__SW0816_TABLE__,
							__SW0816_SHOPORDERS__,
							strOrder);
						TRACEF (strSQL);
					
						MARKTRACECOUNT ();

						if (rst.Open (strSQL, CRecordset::snapshot, CRecordset::readOnly | CRecordset::noDirtyFieldCheck)) {
							MARKTRACECOUNT ();

							if (!rst.IsEOF ())
								strTask = rst.GetFieldValue (__SW0816_TASK__);
							
							MARKTRACECOUNT ();

							rst.Close ();
						}

						db.Close ();
					}
				}
				catch (CDBException * e)		{ strException = FoxjetDatabase::FormatException (e, __FILE__, __LINE__); HANDLEEXCEPTION_TRACEONLY (e); }
				catch (CMemoryException * e)	{ strException = FoxjetDatabase::FormatException (e, __FILE__, __LINE__); HANDLEEXCEPTION_TRACEONLY (e); }

				MARKTRACECOUNT ();

				if (strException.GetLength () || !strTask.GetLength ()) {
					CString str;

					str.Format (
						"Failed to find record\n\n"
						"DSN: [%s]\n"
						"Key field: [%s]\n"
						"Search string: [%s]",
						__SW0816_DSN__,
						__SW0816_SHOPORDERS__,
						strOrder);

					if (strException.GetLength ())
						str += "\n\n" + strException;

					line.StopTask (false, this);
					MsgBox (* this, str, NULL, MB_ICONERROR);
					nResult = -1;
				}
				else {
					if (!strTask.CompareNoCase (line.GetCurrentTask ())) { // same task: do not restart, just update
						CElementPtrArray v;
						CString strPrompt;

						strPrompt.Format ("[%s].[%s].[%s]",
							__SW0816_DSN__, 
							__SW0816_TABLE__, 
							__SW0816_SHOPORDERS__);

						line.GetElements (v, DATABASE);
						
						for (int i = 0; i < v.GetSize (); i++) {
							if (CDatabaseElement * pFind = DYNAMIC_DOWNCAST (CDatabaseElement, v [i])) {
								if (!pFind->GetPrompt ().CompareNoCase (strPrompt)) {
									pFind->SetKeyValue (strOrder);
									pFind->Build (CDatabaseElement::INITIAL, true);

									TRACEF (pFind->GetPrompt () + " --> " + pFind->GetKeyValue ());
									TRACEF (pFind->GetDefaultData ());
								}
							}
						}

						nResult = 1;
					}
					else {
						TCHAR sz [COM_BUF_SIZE] = { 0 };

						_tcsncpy ((LPSTR)sz, strTask, COM_BUF_SIZE);
						packet.AssignData (SERIALSRC_MPHC, HANDSCANNER, line.GetName (), 
							(PUCHAR)(LPSTR)w2a (sz), strTask.GetLength () + 1);
						line.SetSerialPending (false); 
					}
				}
			}
		}
	}

	MARKTRACECOUNT ();

	TRACECOUNTARRAY ();

	return nResult;
}
#endif // __SW0816__

LRESULT CControlView::OnNEXTTaskStart (WPARAM wParam, LPARAM lParam)
{
	if (m_pNextThread) {
		while (!m_pNextThread->PostThreadMessage (TM_NEXT_TASK_START, wParam, lParam))
			::Sleep (0);
	}

	return 0;
}

CString CControlView::GetLine (int nIndex, const CSerialPacket & packet)
{
	if (CProdLine * pLine = GetDocument ()->GetLine (nIndex))
		return pLine->GetName ();
	
	return packet.m_sLineName;
}

LRESULT CControlView::OnHeadSerialData (WPARAM wParam, LPARAM lParam)
{
	int nResult = 0;
	CControlDoc * pDoc = GetDocument ();
	LARGE_INTEGER counter;

	//::QueryPerformanceCounter (&m_counter); 
	::QueryPerformanceCounter (&counter); 

	ASSERT (pDoc);

	if (CSerialPacket * p = (CSerialPacket *)lParam) {
		CDbConnection conn (_T (__FILE__), __LINE__); 

		if (!conn.IsOpen ()) 
			return 0;

		counter = p->m_counter;

		if (p->IsUrl ()) {
			std::vector <std::wstring> vURL = FoxjetDatabase::UrlDecode (std::wstring (p->m_pData));

			if (vURL.size () > 1) {
				CString strURL = vURL [0].c_str ();

				if (strURL.Find (_T ("serial.cgi")) != -1) {
					CStringArray vUser;
					CString strResult;
					int idx = 0;

					vUser.Add (BW_HOST_PACKET_SET_USER_ELEMENTS);
					vUser.Add (GetLine (idx, * p));

					for (int i = 0; i < vURL.size (); i++) {
						std::vector <std::wstring> vParam = explode (vURL [i], '=');

						if (vParam.size () >= 2) {
							if (!_tcsicmp (vParam [0].c_str (), _T ("nme"))) {
								CString strTask = vParam [1].c_str ();
								CStringArray v;

								strTask.Replace (_T (".prd"), _T (""));

								v.Add (BW_HOST_PACKET_START_TASK);
								v.Add (GetLine (idx, * p));
								v.Add (strTask);

								TRACEF (ToString (v));
								OnSocketStartTask ((WPARAM)&v, (LPARAM)&strResult);
								TRACEF (strResult);
								strResult.Empty ();
							}
							else if (!_tcsicmp (vParam [0].c_str (), _T ("idx"))) {
								idx = _ttoi (vParam [1].c_str ());
								vUser.SetAt (1, GetLine (idx, * p));
							}
							else {
								if (vParam.size () >= 2) {
									vUser.Add (UrlDecode ((CString)vParam [0].c_str ()));
									vUser.Add (UrlDecode ((CString)vParam [1].c_str ()));
								}
							}
						}
					}

					if (vUser.GetSize () > 2) {
						TRACEF (ToString (vUser));
						OnSocketSetUserElements ((WPARAM)&vUser, (LPARAM)&strResult);
						TRACEF (strResult);
					}
				}
				else if (strURL.Find (_T ("print.cgi")) != -1) {
					CString strResult;
					int idx = 0;

					for (int i = 0; i < vURL.size (); i++) {
						std::vector <std::wstring> vParam = explode (vURL [i], '=');

						if (vParam.size () >= 2) {
							//TRACEF (vParam [0].c_str () + CString (_T (" --> ")) + vParam [1].c_str ());

							if (!_tcsicmp (vParam [0].c_str (), _T ("idx"))) 
								idx = _ttoi (vParam [1].c_str ());
							else if (!_tcsicmp (vParam [0].c_str (), _T ("status"))) {
								CString str = vParam [1].c_str ();
								CStringArray v;

								if (!str.CompareNoCase (_T ("Pause"))) {
									v.Add (BW_HOST_PACKET_IDLE_TASK);
									v.Add (GetLine (idx, * p));
									OnSocketIdleTask ((WPARAM)&v, (LPARAM)&strResult);
									TRACEF (strResult);
									strResult.Empty ();
								}
								else if (!str.CompareNoCase (_T ("Start"))) {
									v.Add (BW_HOST_PACKET_RESUME_TASK);
									v.Add (GetLine (idx, * p));
									OnSocketResumeTask ((WPARAM)&v, (LPARAM)&strResult);
									TRACEF (strResult);
									strResult.Empty ();
								}
								else if (!str.CompareNoCase (_T ("Cancel"))) {
									v.Add (BW_HOST_PACKET_STOP_TASK);
									v.Add (GetLine (idx, * p));
									OnSocketStopTask ((WPARAM)&v, (LPARAM)&strResult);
									TRACEF (strResult);
									strResult.Empty ();
								}
								else if (!str.CompareNoCase (_T ("ResetCount"))) {
									v.Add (BW_HOST_PACKET_SET_COUNT);
									v.Add (GetLine (idx, * p));
									v.Add (_T ("0"));
									OnSocketSetCount ((WPARAM)&v, (LPARAM)&strResult);
									TRACEF (strResult);
									strResult.Empty ();
								}
							}
						}
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////////////
		int nSegments = p->CountSegments ();

		if (nSegments > 1) {
			for (int i = 0; i < nSegments; i++) {
				CSerialPacket * pNext = p->GetSegment (i);

				TRACEF (pNext->m_sLineName + _T (": ") + FoxjetDatabase::Format (pNext->m_pData, pNext->m_nDataLen));

				nResult += OnHeadSerialData (wParam, (LPARAM)pNext);
			}

			return nResult;
		}

		////////////////////////////////////////////////////////////////////////////////

		if (p->m_eDeviceType == HOSTINTERFACE) {
			UpdateSerialData (a2w (p->m_pData)); 

			CString strResult = CServerThread::ProcessCommand (a2w (p->m_pData), this);

			if (CComm * pComm = (CComm *)wParam) {
				LOCK (pComm->m_cs);
				VERIFY (Write_Comport (pComm->m_hComm, strResult.GetLength (), (LPVOID)(LPCSTR)w2a (strResult)));
				UpdateSerialData (_T (">> ") + strResult); 
			}
			else {
				UpdateSerialData (_T ("x> [") + strResult + _T ("]")); 
			}
		}
		else {
			CString strNewLine = p->m_sLineName;
			const CString strOldLine = p->IsOverrideLine () ? strNewLine : pDoc->GetSelectedLine ()->GetName ();
			bool bProcess = true;
			CStringArray vLines;
			CString strData = /* a2w */ (p->m_pData);

			//TRACEF (FoxjetDatabase::Format (strData, strData.GetLength ()));

			if (p->m_eDeviceType == FIXEDSCANNER && !p->m_sLineName.IsEmpty ())
				vLines.Add (p->m_sLineName);
			else 
				vLines.Add (strOldLine);

			if (bProcess) {
				for (int i = 0; i < vLines.GetSize (); i++) {
					CString strLine = vLines [i];

					if (CProdLine * pProdLine = pDoc->GetProductionLine (strLine)) {
						p->m_sLineName = strLine;

					#if __CUSTOM__ == __SW0816__
						int nShopOrderResult = OnSerialShopOrder (* p, * pProdLine);

						//strData = a2w (p->m_pData);

						if (nShopOrderResult == 1) 
							nResult = 1;
						else if (nShopOrderResult == -1)
							return 0;
						else {
					#endif

						if (((p->m_eDeviceType == HANDSCANNER) || (p->m_eDeviceType == SW0858_DBSTART)) && pProdLine->GetSerialPending ()) {
							#if __CUSTOM__ == __SW0816__
							strData = CString (_T ("Ignored: ")) + strData;
							#endif
						}

						const clock_t tmStart = clock ();

						int nStart = pProdLine->ProcessSerialData (p, this);

						if (TASK_START_SUCCEEDED (nStart)) {
							nResult = TASK_START_SUCCESS;

							if (FoxjetDatabase::IsTimingEnabled ()) {
								double dDiff = (double)(clock () - tmStart) / CLOCKS_PER_SEC;
								CString str;

								str.Format (_T ("Serial start time: %.04f seconds"), dDiff);
								UpdateSerialData (str);
							}
						}
						else 
							nResult = nStart;



					#if __CUSTOM__ == __SW0816__
						}
					#endif
					}
				}


				if ((p->m_eDeviceType == HANDSCANNER) || (p->m_eDeviceType == SW0858_DBSTART)) {
					if (nResult > 0) {
						strNewLine = p->m_sLineName;

						bool bSw0849 = CSerialPacket::IsSw0849 (p->m_pData, p->m_nDataLen) && (p->m_eDeviceType == REMOTEDEVICE);

						if (!bSw0849) {
							//if (strNewLine.GetLength () && strOldLine.CompareNoCase (strNewLine) != 0) {
								pDoc->SetSelectedLine (strNewLine);
								pDoc->UpdateAllViews (NULL, 0, NULL);
								pDoc->UpdateAllViews (NULL, UPDATE_LINE_CHANGE, NULL);
							//}
						}
					}
					else {
						if (!theApp.Is0835 ()) {
							CString str, strTask = a2w (p->m_pData);

							if (CProdLine * pLine = pDoc->GetProductionLine (strNewLine)) 
								strTask = pLine->GetSerialData (strTask);

							str.Format (LoadString (IDS_REMOTESTARTFAILED), strNewLine, strTask);

							if (nResult == TASK_START_LOGIN_REQUIRED)
								str += _T ("\n") + LoadString (IDS_LOGINREQUIRED);

							MsgBox (* this, str, NULL, MB_ICONERROR);
						}
					}
				}
			}
		
			//if (m_pDiagDlg) 
			{
				TRACEF (FoxjetDatabase::Format (strData, strData.GetLength ()));
				UpdateSerialData (strData); 
			}
		}

		delete p;
	}

	return nResult;
}

LRESULT CControlView::OnUpdateImage (WPARAM wParam, LPARAM lParam) // sw0868
{
#ifdef __WINPRINTER__
	CControlDoc * pDoc = GetDocument ();
	ULONG lLineID = lParam;

	if (CProdLine * pLine = pDoc->GetProductionLine (lLineID)) {
		CPrintHeadList list;

		pLine->GetActiveHeads (list);

		for (POSITION pos = list.GetStartPosition(); pos; ) {
			DWORD dwKey;
			CHead * pHead = NULL;

			list.GetNextAssoc (pos, dwKey, (void *&)pHead);
			ASSERT (pHead);

			CLocalHead::BUILDIMAGESTRUCT * pParam = MAKEIMGPARAMS (IMAGING, pHead->m_nThreadID);
			HANDLE hEvent = pParam->m_hEvent;

			while (!::PostThreadMessage (pHead->m_nThreadID, TM_BUILD_IMAGE, (WPARAM)pParam, CLocalHead::BUILDIMAGE_OVERRIDE))
				::Sleep (0);

			DWORD dwWait = ::WaitForSingleObject (pParam->m_hEvent, 5000);

			//if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
			if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
			if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
			if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");

			//delete pParam;
		}
	}
#endif //__WINPRINTER__

	return 0;
}

LRESULT CControlView::OnHeadErrorCleared (WPARAM wParam, LPARAM lParam) // sw0868
{
	CControlDoc * pDoc = GetDocument ();
	ULONG lLineID = lParam;

	if (CProdLine * pLine = pDoc->GetProductionLine (lLineID)) {
		CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> vDisconnected;
			DWORD dwError = pLine->HasHeadErrors (vDisconnected);		
	
			if (!dwError)
				theApp.CancelStrobe ();
	}

	return 0;
}

LRESULT CControlView::OnUpdatePreview (WPARAM wParam, LPARAM lParam)
{
	CControlDoc * pDoc = GetDocument ();

	if (HANDLE h = (HANDLE)wParam)
		::SetEvent (h);

	return 0;
}

LRESULT CControlView::OnAuxSerialData ( WPARAM wParam, LPARAM lParam )
{
	CControlDoc *pDoc = GetDocument();
	CStringList LineList;
	CProdLine * pLine;
	pDoc->GetProductLines ( LineList );
	while ( !LineList.IsEmpty() )
	{
		CString sLine = LineList.RemoveHead();
		pLine = pDoc->GetProductionLine ( sLine );
		if ( pLine->GetAuxBoxHandle() == lParam )
		{
			int result = pLine->OnAuxSerialData (wParam, this);

			UpdateSerialData (pLine->m_strSerialResult);
			
			break;
		}
	}
	return 0L;
}

LRESULT CControlView::OnAuxPS2Data ( WPARAM wParam, LPARAM lParam )
{
	CControlDoc *pDoc = GetDocument();
	CStringList LineList;
	CProdLine * pLine;
	pDoc->GetProductLines ( LineList );
	while ( !LineList.IsEmpty() )
	{
		CString sLine = LineList.RemoveHead();
		pLine = pDoc->GetProductionLine ( sLine );
		if ( pLine->GetAuxBoxHandle() == lParam )
		{
			pLine->OnAuxBoxPS2Data();
			break;
		}
	}
	return 0L;
}

LRESULT CControlView::OnAuxConnectStateChanged ( WPARAM wParam, LPARAM lParam )
{
	CControlDoc *pDoc = GetDocument();
	CStringList LineList;
	CProdLine * pLine;
	pDoc->GetProductLines ( LineList );
	while ( !LineList.IsEmpty() )
	{
		CString sLine = LineList.RemoveHead();
		pLine = pDoc->GetProductionLine ( sLine );
		if ( pLine->GetAuxBoxHandle() == lParam )
		{
			pLine->OnAuxBoxConnectStateChange ( wParam ? true : false );
			break;
		}
	}
	m_bNetIODevConnected = wParam ? true : false;
	return 0L;
}

LRESULT CControlView::OnAuxDigitalInputs ( WPARAM wParam, LPARAM lParam )
{
	BYTE nDigital		= (BYTE)wParam;
	HANDLE hAuxThread	= (HANDLE)lParam;

	CControlDoc *pDoc = GetDocument();
	CStringList LineList;
	CProdLine * pLine;
	pDoc->GetProductLines ( LineList );

	while ( !LineList.IsEmpty() )
	{
		CString sLine = LineList.RemoveHead();
		pLine = pDoc->GetProductionLine ( sLine );
		if ( pLine->GetAuxBoxHandle () == (LONG)hAuxThread)
		{
			pLine->OnAuxBoxDigitalInputs();
			break;
		}
	}
	return 0L;
}


void CControlView::UpdateOptionsTable (CArray <FoxjetDatabase::OPTIONSSTRUCT, FoxjetDatabase::OPTIONSSTRUCT &> & v)
{
	for (int i = 0; i < ARRAYSIZE (::mapMenu); i++) {
		ULONG lID = ::mapMenu [i].m_nCmdID;
		int nType = ::mapMenu [i].m_nType;

		if (Find (lID, v) == -1) {
			if (nType >= MENUITEM_STD) {
				OPTIONSSTRUCT option;
				OPTIONSLINKSTRUCT link;
				CString str, strID, strPrompt = LoadString (::mapMenu [i].m_nResID);
		
				option.m_lID = lID;

				str = LoadString (::mapMenu [i].m_nResID);
				//str = GetMenuPrompt (option, str);
				strID.Format (_T ("%d"), lID);
				option.m_strOptionDesc = str;

				try {
					COdbcRecordset rstOptions (&theApp.m_Database);
					CString strSQL;
					ULONG lAdminID = GetAdminID ();

					strSQL.Format (_T ("SELECT * FROM [%s];"), Options::m_lpszTable);
					rstOptions.Open (strSQL, CRecordset::dynaset);
					rstOptions.SetEditMode (COdbcRecordset::addnew);
					
					TRACEF (strID + ": " + str);

					//rstOptions.AddNew ();
					rstOptions << option;
					//rstOptions.Update ();

					rstOptions.Close ();

					v.Add (option);

					if (!GetOptionLink (lAdminID, option.m_lID, link)) {
						COdbcRecordset rst (&theApp.m_Database);

						link.m_lOptionsID	= lID;
						link.m_lGroupID		= lAdminID;
						strSQL.Format (_T ("SELECT * FROM [%s];"), OptionsToGroup::m_lpszTable);
						rst.Open (strSQL, CRecordset::dynaset);

						//rst.AddNew ();
						rst << link;
						//rst.Update ();

						rst.Close ();
					}
				}
				catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
				catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
			}
		}
	}

	for (int j = v.GetSize () - 1; j >= 0; j--) {
		bool bFound = false;
		ULONG lID = v [j].m_lID;

		for (int i = 0; !bFound && i < ARRAYSIZE (::mapMenu); i++) 
			bFound = ::mapMenu [i].m_nCmdID == lID;

		if (!bFound) {
			TRACEF (_T ("obsolete: ") + v [j].m_strOptionDesc);
			theApp.m_SecurityTable.RemoveKey (lID);
			v.RemoveAt (j);

			try {
				CString str;

				str.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), OptionsToGroup::m_lpszTable, OptionsToGroup::m_lpszOptionsID, lID);
				TRACEF (str);
				theApp.m_Database.ExecuteSQL (str);

				str.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), Options::m_lpszTable, Options::m_lpszID, lID);
				TRACEF (str);
				theApp.m_Database.ExecuteSQL (str);
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		}
	}
}

ULONG CControlView::GetAdminID ()
{
	SECURITYGROUPSTRUCT s;

	s.m_lID = -1;

	try {
		COdbcRecordset rst (&theApp.m_Database);
		CString strSQL;

		strSQL.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s';"), 
			SecurityGroups::m_lpszTable,
			SecurityGroups::m_lpszName,
			_T ("Administrator"));
		rst.Open (strSQL, CRecordset::snapshot);
		
		if (!rst.IsEOF ())
			rst >> s;

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return s.m_lID;
}

bool CControlView::GetOptionLink (ULONG lGroupID, ULONG lOptionID, FoxjetDatabase::OPTIONSLINKSTRUCT & link)
{
	bool bResult = false;

	try {
		COdbcRecordset rst (&theApp.m_Database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%d AND [%s]=%d;"), 
			OptionsToGroup::m_lpszTable,
			OptionsToGroup::m_lpszGroupID,
			lGroupID,
			OptionsToGroup::m_lpszOptionsID,
			lOptionID);
		rst.Open (str);
		
		if (!rst.IsEOF ()) {
			rst >> link;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

static void InsertAscByID(CArray <CProdLine *, CProdLine *> & v, CProdLine * p) 
{
	for (int i = 0; i < v.GetSize (); i++) {
		if (p->GetID () <= v [i]->GetID ()) {
			v.InsertAt (i, p);
			return;
		}
	}

	v.Add (p);
}

void CControlView::FillLineCtrl ()
{
	CControlDoc * pDoc = GetDocument ();

	if (CTabCtrl * p = (CTabCtrl *)GetDlgItem (TAB_LINE)) { 
		CMapStringToPtr v;
		CRect rc, rcTop;
		int nIndex = p->GetCurSel ();
		CArray <CProdLine *, CProdLine *> vSorted;

		if (CWnd * pTop = GetDlgItem (BTN_BOX))
			pTop->GetWindowRect (rcTop);

		p->DeleteAllItems ();
		pDoc->GetProductLines (v);
		p->GetWindowRect (rc);

		{
			for (POSITION pos = v.GetStartPosition(); pos; ) {
				CProdLine * pLine = NULL;
				CString strLine;
			
				v.GetNextAssoc (pos, strLine, (void *&)pLine);
				InsertAscByID (vSorted, pLine);
			}
		}

		if (v.GetCount ()) {
			int cy = rcTop.top - rc.top - 10;
			CSize size ((rc.Width () / v.GetCount ()) - (min (2, v.GetCount ()) * 1), cy);

			if (v.GetCount () <= 1)
				size.cx -= 2;

			p->SetItemSize (size);
			p->SetMinTabWidth (size.cx);

			for (int i = 0; i < vSorted.GetSize (); i++) {
				CProdLine * pLine = vSorted [i];
				CString strLine = pLine->GetName ();

				ASSERT (pLine);
				TRACEF (pLine->GetName ());
				//strLine.Format (_T ("%d"), pLine->GetID ());
				int n = p->InsertItem (i, strLine);
				ASSERT (n == i);
			}

			if (nIndex == -1)
				nIndex = 0;

			p->SetCurSel (nIndex);
		}
	}

	OnChangeLineTab (NULL, NULL);
}

void CControlView::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	using namespace Color;
	using namespace FoxjetCommon;

	DRAWITEMSTRUCT dis;
	
	memcpy (&dis, lpDrawItemStruct, sizeof (dis));

	if (dis.CtlType == ODT_MENU) {
		bool bSelected = (dis.itemState & (ODS_SELECTED | ODS_FOCUS)) ? false : true;
		CString str;
		UINT nBitmapID = -1;
		CRect rc = dis.rcItem;
		CDC dc;
		MEASUREITEMSTRUCT m;
		const CSize sizeBitmap (32, 32);

		memset (&m, 0, sizeof (m));

		for (int i = 0; i < ARRAYSIZE (::mapMenu); i++) {
			if (::mapMenu [i].m_nCmdID == dis.itemID) {
				str			= LoadString (::mapMenu [i].m_nResID);
				nBitmapID	= ::mapMenu [i].m_nBitmapID;
				//TRACEF (str + _T (": ") + FoxjetDatabase::ToString ((int)nBitmapID));
				break;
			}
		}

		dc.Attach (dis.hDC);
		COLORREF rgbSetTextColor = dc.SetTextColor (bSelected ? Color::rgbCOLOR_WINDOWTEXT : Color::rgbCOLOR_HIGHLIGHTTEXT);
		int nSetBkMode = dc.SetBkMode (TRANSPARENT);
		dc.FillRect (rc, &CBrush (bSelected ? Color::rgbCOLOR_MENU : Color::rgbCOLOR_HIGHLIGHT));
		dc.DrawEdge (rc, EDGE_SUNKEN, BF_LEFT | BF_RIGHT | BF_TOP | BF_BOTTOM | BF_ADJUST);
		rc.left += sizeBitmap.cy + 5; 

		if (nBitmapID != -1) {
			CBitmap bmp;

			if (bmp.LoadBitmap (nBitmapID)) {
				CDC dcMem;
				DIBSECTION ds;

				memset (&ds, 0, sizeof (ds));
				bmp.GetObject (sizeof (ds), &ds);
				int x = 0;
				int y = rc.top + (rc.Height () - sizeBitmap.cy) / 2;

				dcMem.CreateCompatibleDC (&dc);
				CBitmap * pBitmap = dcMem.SelectObject (&bmp);
				COLORREF rgb = dcMem.GetPixel (0, 0);
				::TransparentBlt (dc, x, y, sizeBitmap.cx, sizeBitmap.cy, dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, rgb);
				dcMem.SelectObject (pBitmap);
			}
		}

		//{ CString strDebug; strDebug.Format (_T ("[%d] %s"), dis.itemID, str); TRACEF (strDebug); }
		dc.DrawText (str, rc, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_EXPANDTABS);
		dc.SetTextColor (rgbSetTextColor);
		dc.SetBkMode (nSetBkMode);
		dc.Detach ();
	}
	else if (nIDCtl == TAB_LINE) {
		CDC dc, dcMem;
		CBitmap bmp;
		CRect rc = dis.rcItem;
		
		dc.Attach (dis.hDC);

		//TRACEF (_T ("OnDrawItem [") + ToString (nIDCtl) + _T ("]: ") + ToString ((int)dis.itemID));

		if (dis.itemID == -1) 
			dc.FillRect (rc, CBrush::FromHandle (m_hbrDlg));
		else {
			dcMem.CreateCompatibleDC (&dc);
			bmp.CreateCompatibleBitmap (&dc, rc.Width (), rc.Height ());
			CBitmap * pBmp = dcMem.SelectObject (&bmp);
			DrawLineDisplay (dcMem, nIDCtl, dis);
//{ CString str; str.Format (_T ("%d, rc: %d, %d, [%d], %d"), dis.itemID, rc.left, rc.top, rc.Width (), rc.Height ()); TRACEF (str); }
			::TransparentBlt (dc, rc.left, rc.top, rc.Width (), rc.Height (), dcMem, 0, 0, rc.Width (), rc.Height (), dcMem.GetPixel (0, 0));
			dcMem.SelectObject (pBmp);
		}

		dc.Detach ();
	}
	else if (nIDCtl == LBL_TIME1 || nIDCtl == LBL_TIME2 || nIDCtl == LBL_TIME3) {
		CRect rc = dis.rcItem;
		CDC dc, dcMem;
		CBitmap bmp;

		dc.Attach (dis.hDC);
		dcMem.CreateCompatibleDC (&dc);
		bmp.CreateCompatibleBitmap (&dc, rc.Width (), rc.Height ());
		CBitmap * pBmp = dcMem.SelectObject (&bmp);
		::BitBlt (dcMem, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
		DrawTimeDisplay (dcMem, rc, nIDCtl - LBL_TIME1);
		dc.BitBlt (0, 0, rc.Width (), rc.Height (), &dcMem, 0, 0, SRCCOPY);
		dcMem.SelectObject (pBmp);
		dc.Detach ();
	}
	else 
		CFormView::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CControlView::DrawTimeDisplay (CDC & dc, CRect rc, int nIndex)
{
	UINT n [] = 
	{
		LBL_TIME1,
		LBL_TIME2,
		LBL_TIME3,
	};

	ASSERT (nIndex >= 0 && nIndex < ARRAYSIZE (n));

	if (CWnd * p = GetDlgItem (n [nIndex])) {
		CString str;

		#ifdef __WINPRINTER__
		str = FoxjetElements::CDateTimeElement::Format (-1, m_strTimeFormat [nIndex], 0, NULL);
		#else
		str = CTime::GetCurrentTime ().Format (m_strTimeFormat [nIndex]);
		#endif

		dc.FillRect (rc, CBrush::FromHandle (m_hbrDlg));
		CFont * pFont = dc.SelectObject (&m_fntTimer);
		int nBkMode = dc.SetBkMode (TRANSPARENT);
		COLORREF rgb = dc.SetTextColor (Color::rgbBlack);
		dc.DrawText (str, rc, DT_CENTER);
		dc.SetBkMode (nBkMode);
		dc.SelectObject (pFont);
		dc.SetTextColor (rgb);
	}
}

void CControlView::DrawLineDisplay (CDC & dc, UINT nIDCtl, const DRAWITEMSTRUCT & dis)
{
	if (CLineTab * p = (CLineTab *)GetDlgItem (nIDCtl)) { 
		const CRect rc = dis.rcItem;
		CRect rcText = dis.rcItem;
//		const int nMargin = 20;
		const int nMargin = 10;
		int nLeft = nMargin;

		if (p->GetCurSel () == (int)dis.itemID)
			dc.FillRect (CRect (0, 0, rc.Width (), rc.Height ()), CBrush::FromHandle (m_hbrDlg));
		else 
			dc.FillRect (CRect (0, 0, rc.Width (), rc.Height ()), CBrush::FromHandle (m_hbrDlgDisabled));

		ASSERT (GetDocument ()->GetLine (dis.itemID));

		if (CProdLine * pLine = GetDocument ()->GetLine (dis.itemID)) {
			CSize sizeIcon (30, 30);
			const FoxjetDatabase::TASKSTATE eState = pLine->GetTaskState ();

			{
				CBitmap * pbmp = &m_bmpStrobeGray; // &m_bmpStrobeGreen;
				DIBSECTION ds;
				COLORREF rgbState = Color::rgbBlack;
				CDC dcStrobe;

				memset (&ds, 0, sizeof (ds));
				dcStrobe.CreateCompatibleDC (&dc);

				if (pLine->GetTabStrobeColor () & RED_STROBE)
					pbmp = &m_bmpStrobeRed;
				if (pLine->GetTabStrobeColor () & YELLOW_STROBE)
					pbmp = &m_bmpStrobeYellow;
				if (pLine->GetTabStrobeColor () & GREEN_STROBE)
					pbmp = &m_bmpStrobeGreen;

				pbmp->GetObject (sizeof (ds), &ds);
				int cx = ds.dsBm.bmWidth;
				int cy = ds.dsBm.bmHeight;
				CBitmap * pBmp = dcStrobe.SelectObject (pbmp);
				COLORREF rgb = dcStrobe.GetPixel (0, 0);
				double dZoom = 0.9;//min ((rc.Width () - (nMargin * 2)) / (double)cx, (rc.Height () - (nMargin * 2)) / (double)cy);
				sizeIcon = CSize ((int)floor (cx * dZoom), (int)floor (cy * dZoom));
//				::TransparentBlt (dc, 5, nMargin, sizeIcon.cx, sizeIcon.cy, dcStrobe, 0, 0, cx, cy, rgb);
				::TransparentBlt (dc, nMargin, rc.bottom - (sizeIcon.cy + nMargin), sizeIcon.cx, sizeIcon.cy, dcStrobe, 0, 0, cx, cy, rgb);
				dcStrobe.SelectObject (pBmp);

				rcText.left += (int)floor (cx * dZoom) + nMargin;
				rcText.right -= nMargin;
				rcText.top += 5;
				rcText.bottom -= 5;

				rcText.right /= (dis.itemID + 1);
			}

			{
				CDC dcState;
				DIBSECTION ds;
				CBitmap * pbmp = &m_bmpStop;
				CxImage img;

				dcState.CreateCompatibleDC (&dc);

				switch (pLine->GetTaskState ()) {
				case RUNNING:	pbmp = &m_bmpStart;	break;
				case IDLE:		pbmp = &m_bmpIdle;	break;
				case STOPPED:	pbmp = &m_bmpStop;	break;
				}

				memset (&ds, 0, sizeof (ds));
				pbmp->GetObject (sizeof (ds), &ds);
				img.CreateFromHBITMAP (* pbmp);

				/*
				{
					if (p->GetCurSel () == (int)dis.itemID)
						img.Light (100, 0);

					img.GrayScale ();
				}
				*/

				{
					DIBSECTION ds;

					memset (&ds, 0, sizeof (ds));
					m_bmpStrobe.GetObject (sizeof (ds), &ds);
					nLeft += ds.dsBm.bmWidth;
				}

				HBITMAP hbmp = img.MakeBitmap ();
				CBitmap * pBmp = dcState.SelectObject (CBitmap::FromHandle (hbmp));
				//RGBQUAD q = img.GetPixelColor (0, 0);
				COLORREF rgb = dcState.GetPixel (0, 0); //RGB (q.rgbRed, q.rgbBlue, q.rgbGreen);
				double dZoom = 0.55;//BindTo (min (sizeIcon.cx / (double)ds.dsBm.bmWidth, sizeIcon.cy / (double)ds.dsBm.bmHeight) - 0.10, 0.25, 1.00);
				sizeIcon = CSize ((int)floor (ds.dsBm.bmWidth * dZoom), (int)floor (ds.dsBm.bmHeight * dZoom));
				::TransparentBlt (
					dc, 
					nLeft /*(rc.Width () - sizeIcon.cx) / 2*/, 
//					(rc.Height () - sizeIcon.cy) / 2, 
					rc.bottom - (sizeIcon.cy + nMargin), 
					sizeIcon.cx, sizeIcon.cy, 
					dcState,
					0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, rgb);
				dcState.SelectObject (pBmp);
				::DeleteObject (hbmp);
				nLeft += sizeIcon.cx + nMargin;
				rcText.left = nLeft;
			}

			{
				int n = (int)(nMargin / 2.0);
			
				//rcText.InflateRect (0, n, 0, n);
			}

			if (p->GetCurSel () != (int)dis.itemID)
				rcText -= CPoint (0, 2);

			CRect rcTextLeft [] = { rcText, rcText, };
			CRect rcTextRight [] = { rcText, rcText };

			{

				CRect rcTmp [] = { rcTextRight, rcTextRight };
				CFont * pFont = dc.SelectObject (&m_fntSmall);
				CString strMsg = LoadString (IDS_MESSAGE) + _T (":");
				CString strCount = LoadString (IDS_COUNT) + _T (":");

				strMsg.MakeUpper ();
				strCount.MakeUpper ();

				dc.DrawText (strMsg, rcTmp [0], DT_SINGLELINE | DT_TOP | DT_LEFT | DT_CALCRECT);
				dc.DrawText (strCount, rcTmp [1], DT_SINGLELINE | DT_TOP | DT_LEFT | DT_CALCRECT);
				int cx = max (rcTmp [0].Width (), rcTmp [1].Width ());
				

				dc.SelectObject (&m_fntMedium);
				const int cy = dc.DrawText (strMsg, CRect (rcTextRight [0]), DT_SINGLELINE | DT_TOP | DT_LEFT | DT_CALCRECT);
				dc.SelectObject (pFont);

				rcTextLeft [0].right = rcTextLeft [0].left + cx;
				rcTextLeft [0].bottom = rcTextLeft [0].top + (rcText.Height () / 2);
				
				rcTextLeft [1].right = rcTextLeft [1].left + cx;
				rcTextLeft [1].top += (rcText.Height () / 2);

				rcTextRight [0].left += cx + (nMargin / 2);
				rcTextRight [0].bottom = rcTextRight [0].top + (rcText.Height () / 2);

				rcTextRight [1].left += cx + (nMargin / 2);
				rcTextRight [1].top += (rcText.Height () / 2);

				dc.SelectObject (&m_fntSmall);
				dc.DrawText (strMsg, rcTextLeft [0], DT_SINGLELINE | DT_VCENTER | DT_RIGHT);
				dc.DrawText (strCount, rcTextLeft [1], DT_SINGLELINE | DT_VCENTER | DT_RIGHT);
				dc.SelectObject (pFont);
			}

			/*
			{ 
				LPCTSTR lpsz [] = { _T ("DISABLED"), _T ("RUNNING"), _T ("IDLE"), _T ("STOPPED"), };
				TRACEF (pLine->GetName () + _T (": ") + lpsz [eState]);
			}
			*/

			if (eState == RUNNING || eState == IDLE) {
				int nBkMode = dc.SetBkMode (TRANSPARENT);
				CFont * pFont = dc.SelectObject (&m_fntLarge);
				rcTextRight [0].InflateRect (0, 0, 0, (int)(rcTextRight [0].Height () * 0.1));
				
				{
					int nFormat = DT_SINGLELINE | DT_TOP | DT_LEFT;

					CString strKey;
					CString strTask = pLine->GetCurrentTask (&strKey);
					CSize size = dc.GetTextExtent (strTask);
					bool bKeyValue = (strKey.GetLength ()) && !(m_lTimerTab % 3);
					bool bLookup = false;

					if (bKeyValue) {
						strTask = _T ("[") + strKey + _T ("]");
						bLookup = m_vKeyValue.Lookup (strKey, strTask) ? true : false;
					}
					else
						bLookup = m_vTask.Lookup (strTask, strTask) ? true : false;

					if (!bLookup) {
						for (int i = 0; i < strTask.GetLength (); i++) {
							CString str = strTask;

							str.Delete (0, i);
							str = CString ('.', min (i, 2)) + str;
							size = dc.GetTextExtent (str);

							if (size.cx <= rcTextRight [0].Width ()) {
								if (bKeyValue)
									m_vKeyValue.SetAt (strKey, str);
								else
									m_vTask.SetAt (strTask, str);

								strTask = str;
								break;
							}
							else {
								nFormat &= ~DT_LEFT;
								nFormat |= DT_RIGHT;
							}
						}
					}

					dc.DrawText (strTask, rcTextRight [0], nFormat);
				}

				dc.DrawText (pLine->GetCount (), rcTextRight [1], DT_SINGLELINE | DT_TOP | DT_LEFT);
				dc.SelectObject (pFont);
				dc.SetBkMode (nBkMode);
			}

			{
				CFont * pFont = dc.SelectObject (&m_fntMedium);
				dc.DrawText (pLine->GetName (), rcText - CPoint (nLeft - nMargin, 0), DT_SINGLELINE | DT_TOP | DT_LEFT);
				dc.SelectObject (pFont);
			}

			#ifdef _DEBUG
			/*
			dc.FrameRect (rcText, &CBrush (Color::rgbBlack));
			dc.FrameRect (rcTextLeft  [0], &CBrush (Color::rgbRed));
			dc.FrameRect (rcTextLeft  [1], &CBrush (Color::rgbGreen));
			dc.FrameRect (rcTextRight [0], &CBrush (Color::rgbBlue));
			dc.FrameRect (rcTextRight [1], &CBrush (Color::rgbYellow));
			*/
			#endif //_DEBUG

			{
				CPen pen (PS_SOLID, p->GetPenSize (), p->GetLineColor ());
				CPen * pPen = dc.SelectObject (&pen);
				int x = rcText.left - (nMargin / 2);
				dc.MoveTo (x, rcText.top + nMargin);
				dc.LineTo (x, rcText.bottom - nMargin);
				dc.SelectObject (pPen);
			}

			{
				CDC dcMem;
				CBitmap bmp, bmpMask;
				CRect rcTmp = rc - rc.TopLeft ();

				dcMem.CreateCompatibleDC (&dc);
				bmp.CreateCompatibleBitmap (&dc, rc.Width (), rc.Height ());

				CBitmap * pBmp = dcMem.SelectObject (&bmp);
				::BitBlt (dcMem, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
				m_tabLine.DrawTab (dcMem, m_tabLine.GetLineColor (), rcTmp, 1, m_tabLine.GetRadius (), false, true);
				::TransparentBlt (dc, 0, 0, rc.Width (), rc.Height (), dcMem, 0, 0, rc.Width (), rc.Height (), m_tabLine.GetLineColor ());

				dcMem.SelectObject (pBmp);
			}
		}
	}

	/*
	{
		pLine->m_nErrorIndex++;
	}
	*/
}

static void ExpandTab (CUIntArray & vTab, int nIndex, double dStretchPct)
{
	if (vTab.GetSize () > 1) {
		const int nDiff = vTab [nIndex + 1] - vTab [nIndex];
		const int nShift = (int)floor ((double)nDiff * dStretchPct);
		const int nDiv = vTab.GetSize () - nIndex - 1;
		const int nRemainder = (nDiff - nShift) / nDiv;
		int x = 1;
		
		for (int j = nIndex + 1; j < vTab.GetSize (); j++) {
			for (int y = j; y < vTab.GetSize (); y++) {
				vTab.SetAt (j, vTab [j] - nRemainder);
			}
		//{ CString str; for (int i = 0; i < vTab.GetSize (); i++) str += ToString ((int)vTab [i]) + _T (","); TRACEF (str); }
		}

	/*
		for (i = 3; i <= 5; i++) 
		{
			//{ CString str; for (int i = 0; i < vTab.GetSize (); i++) str += ToString ((int)vTab [i]) + _T (","); TRACEF (str); }

			//int i = 4;
			const int nDiff = vTab [i + 1] - vTab [i];
			const int nShift = (int)floor ((double)nDiff * 0.66);
			const int nDiv = vTab.GetSize () - i - 1;
			const int nRemainder = (nDiff - nShift) / nDiv;
			int x = 1;
			
			for (int j = i + 1; j < vTab.GetSize (); j++) {
				for (int y = j; y < vTab.GetSize (); y++) {
					vTab.SetAt (j, vTab [j] - nRemainder);
				}
			//{ CString str; for (int i = 0; i < vTab.GetSize (); i++) str += ToString ((int)vTab [i]) + _T (","); TRACEF (str); }
			}

			//{ CString str; for (int i = 0; i < vTab.GetSize (); i++) str += ToString ((int)vTab [i]) + ','; TRACEF (str); }
		}
	*/
	}
}


LRESULT CControlView::OnResumeTask(WPARAM wParam, LPARAM lParam)
{
	ULONG lLineID = wParam;
	CHead * pHead = (CHead *)lParam;
	CControlDoc * pDoc = GetDocument ();
	CMapStringToPtr v;

	ASSERT (pDoc);
	pDoc->GetProductLines (v);

	for (POSITION pos = v.GetStartPosition (); pos; ) {
		CProdLine * pLine = NULL;
		CString strLine;

		v.GetNextAssoc (pos, strLine, (void *&)pLine);
		
		if (pLine) {
			if (pLine->m_LineRec.m_lID == lLineID) {
				CHeadInfo info = CHead::GetInfo (pHead);

				//TRACEF ("ResumeTask: " + pLine->m_LineRec.m_strName);
				ResumeTask (&pLine->m_LineRec.m_strName);
				//TRACER (ToString (info.m_Status.m_nInkLevel));

				if (info.m_Status.m_nInkLevel == OK) {
					for (POSITION pos = m_mapInkSerialNumber.GetStartPosition (); pos; ) {
						CInkSerialNumberDlg * pdlg = NULL;
						ULONG lHeadID = -1;

						m_mapInkSerialNumber.GetNextAssoc (pos, lHeadID, pdlg);

						if (pdlg) {
							delete pdlg;
							m_mapInkSerialNumber.SetAt (lHeadID, NULL);
						}
					}
				}

				break;
			}
		}
	}

	return 0;
}

LRESULT CControlView::OnIdleTask(WPARAM wParam, LPARAM lParam)
{
	ULONG lLineID = wParam;
	CControlDoc * pDoc = GetDocument ();
	CMapStringToPtr v;

	ASSERT (pDoc);
	pDoc->GetProductLines (v);

	for (POSITION pos = v.GetStartPosition (); pos; ) {
		CProdLine * pLine = NULL;
		CString strLine;

		v.GetNextAssoc (pos, strLine, (void *&)pLine);
		
		if (pLine) {
			if (pLine->m_LineRec.m_lID == lLineID) {
				CPrintHeadList list;

				pLine->GetActiveHeads (list);
				
				for (POSITION pos = list.GetStartPosition(); pos; ) {
					DWORD Key;
					CHead * pHead = NULL;

					list.GetNextAssoc(pos, Key, (void *&)pHead);
					ASSERT (pHead);
					CHeadInfo info = CHead::GetInfo (pHead);
					bool bPrintDriver = false;

					SEND_THREAD_MESSAGE (info, TM_GET_PRINT_DRIVER, &bPrintDriver);

					if (bPrintDriver) {
						if ((info.m_Status.m_lCountUntil) < 16000 && (info.m_Status.m_lCount >= info.m_Status.m_lCountUntil)) {
							SEND_THREAD_MESSAGE (info, TM_SET_PRINT_DRIVER, false);

							if (m_pNextThread) {
								while (!::PostThreadMessage (m_pNextThread->m_nThreadID, TM_NEXT_SEND_STATUS, 0, 0))
									::Sleep (1);
							}
						}
					}
				}

				TRACEF ("IdleTask: " + pLine->m_LineRec.m_strName);
				IdleTask (&pLine->m_LineRec.m_strName);
				break;
			}
		}
	}

	return 0;
}

LRESULT CControlView::OnSocketSetErrorState (WPARAM wParam, LPARAM lParam)
{
	using namespace FoxjetCommon;

	CControlDoc * pDoc = GetDocument ();
	SOCKETERRORSTRUCT * pError = (SOCKETERRORSTRUCT *)wParam;
	CMapStringToPtr v;

	ASSERT (pDoc);

	if (!pError)
		return 0;

	ULONG lLineID = pError->m_lLineID;
	ULONG lTaskID = pError->m_lTaskID;
	bool bError = pError->m_bError;

	pDoc->GetProductLines (v);

	for (POSITION pos = v.GetStartPosition(); pos; ) {
		CProdLine * pLine = NULL;
		CString strLine;

		v.GetNextAssoc (pos, strLine, (void *&)pLine);

		if (pLine) {
			if (pLine->m_LineRec.m_lID == lLineID) {
				CStringArray v;
				CString strSend;

				if (bError) {
					v.Add (BW_HOST_PACKET_GET_CURRENT_STATE);
					v.Add (strLine);

					OnSocketGetCurrentState ((WPARAM)&v, (LPARAM)&strSend);
				}
				else {
					v.Add (BW_HOST_PACKET_CLEAR_ERROR);
					v.Add (strLine);
					strSend = ToString (v);
				}

				//TRACEF (strSend);

				if (theApp.m_nInterfaceThreadID) {
					DWORD dw = 0;
					DWORD dwMsg = bError ? TM_ERROR_RAISED : TM_ERROR_CLEARED;

					while (!::PostThreadMessage (theApp.m_nInterfaceThreadID, dwMsg, lLineID, -1))
						::Sleep (0);
				}

				InterfaceNotify (strSend);
				/* TODO: rem
				for (POSITION pos = theApp.m_vErrSock.GetStartPosition (); pos != NULL; ) {
					CString strKey, strValue;
					CStringArray vSocket;

					theApp.m_vErrSock.GetNextAssoc (pos, strKey, strValue);
					Tokenize (strKey, vSocket);

					if (vSocket.GetSize () >= 2) {
						CString strDebug, strAddr;// = vSocket [0];
						UINT nPort = _ttoi (vSocket [1]);
						CSocket s;
						int nFlag;

						ASSERT (m_pServerThread);
						strAddr = m_pServerThread->GetConnectedAddr ();

						VERIFY (s.Create (0, SOCK_DGRAM));

						nFlag = 1;
						VERIFY (s.SetSockOpt (SO_REUSEADDR, &nFlag, sizeof (int)));

						#ifdef _DEBUG
						strDebug.Format ("%s:%d", strAddr, nPort);
						TRACEF ("error socket: " + strDebug);
						#endif //_DEBUG

						if (s.Connect (strAddr, nPort)) {
							int nLen = strSend.GetLength ();

							DWORD dwSend = s.Send (strSend.GetBuffer (nLen), nLen);
							
							if (dwSend == SOCKET_ERROR)
								TRACEF ("SOCKET_ERROR: " + strDebug);
						}
						else
							TRACEF ("Failed to connect: " + strDebug);

						s.Close ();
					}
				}
				*/
			}
		}
	}

	delete pError;

	return 0;
}

LRESULT CControlView::OnSetStrobe (WPARAM wParam, LPARAM lParam)
{
	theApp.SetStrobe ();

	return 0;
}

LRESULT CControlView::OnSetHeadWarning (WPARAM wParam, LPARAM lParam)
{
	ULONG lLineID = wParam;
	ULONG lTaskID = lParam;

	OnSocketSetErrorState ((WPARAM)new SOCKETERRORSTRUCT (lLineID, lTaskID, true), 0);

	return 0;
}


LRESULT CControlView::OnTriggerWDT (WPARAM wParam, LPARAM lParam)
{
/*
#ifdef __WINPRINTER__
	CMphc * pDriver = theApp.GetMPHCDriver ();
	CControlDoc * pDoc = GetDocument ();
	CMapStringToPtr v;

	ASSERT (pDoc);
	pDoc->GetProductLines (v);

	TRACEF ("OnTriggerWDT");

	for (POSITION pos = v.GetStartPosition(); pos; ) {
		CProdLine * pLine = NULL;
		CString strLine;

		v.GetNextAssoc (pos, strLine, (void *&)pLine);

		if (pLine) {
			if (CLocalHead * p = (CLocalHead *)pLine->GetHeadByUID (_T ("300"))) {
				TRACEF ("PostThreadMessage: TM_TRIGGER_WDT");

				while (!::PostThreadMessage (p->m_nThreadID, TM_TRIGGER_WDT, lParam, wParam))
					::Sleep (0);

				return 1;
			}
		}
	}
#endif //__WINPRINTER__
*/
	return 0;
}

LRESULT CControlView::OnSetHeadError (WPARAM wParam, LPARAM lParam)
{
	ULONG lLineID = wParam;
	ULONG lTaskID = lParam;
	CControlDoc * pDoc = GetDocument ();
	CMapStringToPtr v;

	ASSERT (pDoc);
	pDoc->GetProductLines (v);

	for (POSITION pos = v.GetStartPosition(); pos; ) {
		CProdLine * pLine = NULL;
		CString strLine;

		v.GetNextAssoc (pos, strLine, (void *&)pLine);

		if (pLine) {
			if (pLine->m_LineRec.m_lID == lLineID) {
				bool bWait = !pLine->IsWaiting ();

				#ifdef __WINPRINTER__
				if (FoxjetCommon::IsProductionConfig ())
					bWait = true;
				#endif //__WINPRINTER__

				if (bWait) {
					pLine->SetWaiting (true);

					OnSocketSetErrorState ((WPARAM)new SOCKETERRORSTRUCT (lLineID, lTaskID, true), 0);
				}
			}
		}
	}

	return 0;
}

LRESULT CControlView::OnCalcProductLen (WPARAM wParam, LPARAM lParam)
{
	ULONG lHeadID = wParam;
	ULONG lLineID = lParam;
	CControlDoc * pDoc = GetDocument ();
	CMapStringToPtr v;

	ASSERT (pDoc);
	pDoc->GetProductLines (v);

	for (POSITION pos = v.GetStartPosition(); pos; ) {
		CProdLine * pLine = NULL;
		CString strLine;

		v.GetNextAssoc (pos, strLine, (void *&)pLine);
		
		if (pLine) {
			if (pLine->m_LineRec.m_lID == lLineID) {
				pLine->CalcProductLen ();
				break;
			}
		}
	}
	
	return 0;
}


LRESULT CControlView::OnHeadObsoleteFirmware (WPARAM wParam, LPARAM lParam)
{
	ASSERT (lParam);

#ifdef __IPPRINTER__
	ASSERT (((CHead *)lParam)->IsKindOf (RUNTIME_CLASS (CRemoteHead)));
	CRemoteHead * pHead = (CRemoteHead *)lParam;

	m_vObsolete.Add (pHead);
#endif //__IPPRINTER__

	return 0;
}


LRESULT CControlView::OnSocketSetErrorSocket (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	bool bResult = false;

	if (vCmd.GetSize () >= 4) {
		bool bSet = _ttoi (vCmd [1]) ? true : false;
		CString strAddr = vCmd [2];
		UINT nPort = (UINT)_tcstoul (vCmd [3], NULL, 10);
		CStringArray vTmp;
		CString strPort;

		strPort.Format (_T ("%d"), nPort);

		vTmp.Add (strAddr);
		vTmp.Add (strPort);

		CString strKey = FoxjetDatabase::ToString (vTmp);

		if (bSet)
			theApp.m_vErrSock.SetAt (strKey, _T (""));
		else
			theApp.m_vErrSock.RemoveKey (strKey);

#ifdef _DEBUG
		for (POSITION pos = theApp.m_vErrSock.GetStartPosition (); pos != NULL; ) {
			CString strKey, strValue;

			theApp.m_vErrSock.GetNextAssoc (pos, strKey, strValue);
			TRACEF (strKey);
		}
#endif //_DEBUG

		bResult = true;
	}

	v.Add (BW_HOST_PACKET_SET_ERROR_SOCKET);
	v.Add (bResult ? _T ("1") : _T ("0"));

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketStopTask (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	bool bStop = false;
	CControlDoc * pDoc = GetDocument ();
	const CString strLine = CServerThread::GetLineParam (vCmd);

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
		pLine->StopTask (false, this);
		//pDoc->UpdateAllViews (this);
		pDoc->UpdateAllViews (NULL, UPDATE_PREVIEW, NULL);
		OnUpdate ( NULL, UPDATE_PREVIEW, NULL );
		bStop = true;
	}

	v.Add (BW_HOST_PACKET_STOP_TASK);
	v.Add (strLine);
	v.Add (bStop ? _T ("1") : _T ("0"));

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketIdleTask (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	bool bResult = false;
	CControlDoc * pDoc = GetDocument ();
	const CString strLine = CServerThread::GetLineParam (vCmd);

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
		pLine->IdleTask (true, false);
		pDoc->UpdateAllViews (this);
		bResult = true;
	}

	v.Add (BW_HOST_PACKET_IDLE_TASK);
	v.Add (strLine);
	v.Add (bResult ? _T ("1") : _T ("0"));

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketResumeTask (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	bool bResult = false;
	const CString strLine = CServerThread::GetLineParam (vCmd);
	CControlDoc * pDoc = GetDocument ();

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
		pLine->ResumeTask (true);
		pDoc->UpdateAllViews (this);
		bResult = true;
	}

	v.Add (BW_HOST_PACKET_RESUME_TASK);
	v.Add (strLine);
	v.Add (bResult ? _T ("1") : _T ("0"));

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketGetUserElements (WPARAM wParam, LPARAM lParam)
{
#ifdef __WINPRINTER__
	using namespace FoxjetElements;
	using namespace Foxjet3d;
#endif //__WINPRINTER__

	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	CControlDoc * pDoc = GetDocument();
	const CString strLine = CServerThread::GetLineParam (vCmd);
	Foxjet3d::UserElementDlg::CUserElementArray vUser;
	
	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
		Head::CElementPtrArray v;

		pLine->GetUserElements (vUser, false);
		//pLine->GetElements (v, BMP);
		pLine->GetElements (v, COUNT);

		for (int i = 0; i < v.GetSize (); i++) {
			if (CBitmapElement * pBmp = DYNAMIC_DOWNCAST (CBitmapElement, v [i])) {
				if (pBmp->IsUserPrompted ())
					vUser.Add (Foxjet3d::UserElementDlg::CUserItem (pBmp));
			}
			else if (CCountElement * pCount = DYNAMIC_DOWNCAST (CCountElement, v [i])) {
				vUser.Add (Foxjet3d::UserElementDlg::CUserItem (pCount));
			}
		}
	}

	v.Add (BW_HOST_PACKET_GET_USER_ELEMENTS);
	v.Add (strLine);

	for (int i = 0; i < vUser.GetSize (); i++) {
#ifdef __WINPRINTER__
		if (CUserElement * pElement = DYNAMIC_DOWNCAST (CUserElement, vUser [i].m_pElement)) {
			v.Add (pElement->GetPrompt ());
			v.Add (pElement->GetImageData ());
		}
		else if (CBitmapElement * pBmp = DYNAMIC_DOWNCAST (CBitmapElement, vUser [i].m_pElement)) {
			v.Add (pBmp->GetUserPrompt ());
			v.Add (pBmp->GetImageData ());
		}
		else if (CCountElement * pCount = DYNAMIC_DOWNCAST (CCountElement, vUser [i].m_pElement)) {
			v.Add (pCount->GetName ());
			v.Add (pCount->GetImageData ());
		}
#else
		UserElementDlg::CUserItem item = vUser [i];

		v.Add (item.m_strPrompt);
		v.Add (item.m_strData);
#endif
	}

	strResult = FoxjetDatabase::ToString (v);

	TRACEF (strResult);

	return 0;
}

LRESULT CControlView::OnSocketSetUserElements (WPARAM wParam, LPARAM lParam)
{
#ifdef __WINPRINTER__
	using namespace FoxjetElements;
#endif //__WINPRINTER__

	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	const CString strLine = CServerThread::GetLineParam (vCmd);
	CString str;
	int nCount = 0;
	CControlDoc * pDoc = GetDocument ();

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
		CPrintHeadList heads;

#ifdef __IPPRINTER__
		UserElementDlg::CUserElementArray vElements;
	
		pLine->GetUserElements (vElements, false);
#endif //__IPPRINTER__

		pLine->GetActiveHeads (heads);

		for (int i = 2; i < vCmd.GetSize (); i += 2) {
			CString strPrompt = vCmd [i];
			CString strData;

			if ((i + 1) < vCmd.GetSize ())
				strData = vCmd [i + 1];

			TRACEF (strPrompt + ": " + strData);

			#ifdef __IPPRINTER__
				for (int nFind = 0; nFind < vElements.GetSize (); nFind++) {
					UserElementDlg::CUserItem & user = vElements [nFind];

					if (!user.m_strPrompt.CompareNoCase (strPrompt)) 
						user.m_strData = strData;
				}
			#else //__WINPRINTER__
				for (POSITION pos = heads.GetStartPosition(); pos; ) {
					CHead * pHead = NULL;
					DWORD dwKey;

					heads.GetNextAssoc(pos, dwKey, (void *&)pHead);
					ASSERT (pHead);
					CHeadInfo info = CHead::GetInfo (pHead);
					Head::CElementPtrArray list;
					CHead::GETELEMENTSTRUCT elements (&list, USER);
					CHead::SETUSERDATASTRUCT data (strPrompt, strData);

					SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &elements);
					SEND_THREAD_MESSAGE (info, TM_SET_USERDATA, &data);

					elements.m_nClassID = COUNT;
					SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &elements);

					elements.m_nClassID = BMP;
					SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &elements);

					if (FoxjetUtils::CStartupDlg::IsUserDataEnabled (theApp.m_Database)) {
						const CString strFile = GetHomeDir () + _T ("\\") + GlobalVars::strUserPrompted + _T ("\\") + strLine + _T ("\\") + strPrompt + _T (".txt");
		
						FoxjetFile::WriteDirect (strFile, strData);
					}

					for (int nFind = 0; nFind < list.GetSize (); nFind++) {
						if (CUserElement * pFind = DYNAMIC_DOWNCAST (CUserElement, list [nFind])) {
							if (!pFind->GetPrompt ().CompareNoCase (strPrompt)) {
								pFind->SetDefaultData (strData);
								nCount++;
								TRACEF (pFind->GetPrompt () + " --> " + strData);
							}
						}
						else if (CBitmapElement * pFind = DYNAMIC_DOWNCAST (CBitmapElement, list [nFind])) {
							if (pFind->IsUserPrompted () && !strPrompt.CompareNoCase (pFind->GetUserPrompt ())) {
								pFind->SetFilePath (strData, pFind->GetHead ());
								Foxjet3d::UserElementDlg::CUserElementDlg::ResetSize (pFind, pFind->GetHead ());
								nCount++;
								TRACEF (pFind->GetUserPrompt () + " --> " + strData);
							}
						}
						else if (CCountElement * pFind = DYNAMIC_DOWNCAST (CCountElement, list [nFind])) {
							if (!pFind->GetName ().CompareNoCase (strPrompt)) {
								__int64 lCount = FoxjetCommon::strtoul64 (strData);

								pFind->SetCount (0);
								pFind->IncrementTo (lCount - 1);
								nCount++;
								TRACEF (pFind->GetName () + " --> " + strData);
							}
						}
					}
				}
			#endif
		}

		#ifdef __IPPRINTER__
		for (POSITION pos = heads.GetStartPosition(); pos; ) {
			CHead * pHead = NULL;
			DWORD dwKey;

			heads.GetNextAssoc(pos, dwKey, (void *&)pHead);
			ASSERT (pHead);
			nCount += pLine->UpdateUserElements (vElements, pHead, false);
		}
		#endif //__IPPRINTER__

		pLine->UpdateImages ();
	}

	str.Format (_T ("%d"), nCount);

	v.Add (BW_HOST_PACKET_SET_USER_ELEMENTS);
	v.Add (strLine);
	v.Add (str);

	strResult = FoxjetDatabase::ToString (v);

	return nCount;
}

LRESULT CControlView::OnSocketGetCurrentTask (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	const CString strLine = CServerThread::GetLineParam (vCmd);
	CControlDoc * pDoc = GetDocument ();
	CString strTask;

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) 
		strTask = pLine->GetCurrentTask ();

	v.Add (BW_HOST_PACKET_GET_CURRENT_TASK);
	v.Add (strLine);
	v.Add (strTask);

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketGetCurrentState (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	CControlDoc * pDoc = GetDocument ();
	const CString strLine = CServerThread::GetLineParam (vCmd, 1, this);
	ULONG lHeadID = -1;

	v.Add (BW_HOST_PACKET_GET_CURRENT_STATE);
	v.Add (strLine);

	if (vCmd.GetSize () >= 3)
		lHeadID = _ttol (vCmd [2]);

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
		CPrintHeadList heads;
		CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> vDisconnected;

		pLine->HasHeadErrors (vDisconnected);
		pLine->GetActiveHeads (heads);

		for (POSITION pos = heads.GetStartPosition(); pos; ) {
			CHead * pHead = NULL;
			DWORD dwKey;
			CStringArray vState;

			heads.GetNextAssoc (pos, dwKey, (void *&)pHead);
			ASSERT (pHead);
			CHeadInfo info = CHead::GetInfo (pHead);

			if (lHeadID == -1 || info.m_Config.m_HeadSettings.m_lID == lHeadID) {
				DWORD dwState = 0;

				SEND_THREAD_MESSAGE (info, TM_GET_ERROR_STATE, &dwState);

				if (dwState & HEADERROR_LOWTEMP)		vState.Add (_T ("LOW TEMP"));
				if (dwState & HEADERROR_HIGHVOLTAGE)	vState.Add (_T ("HIGH VOLTAGE ERROR"));
				if (dwState & HEADERROR_LOWINK)			vState.Add (_T ("LOW INK"));
				if (dwState & HEADERROR_OUTOFINK)		vState.Add (_T ("OUT OF INK"));
				if (dwState & HEADERROR_EP8)			vState.Add (_T ("EP8"));
				if (info.m_bDisabled)					vState.Add (_T ("DISABLED"));

				if (!vState.GetSize ())
					vState.Add (_T ("OK"));

				v.Add (FoxjetDatabase::ToString (info.m_Config.m_HeadSettings.m_lID));
				v.Add (FoxjetDatabase::ToString ((int)info.m_eTaskState));
				v.Add (FoxjetDatabase::ToString (vState));
				//TRACEF (ToString (info.GetLineSpeed ()));
				v.Add (FoxjetDatabase::ToString (info.GetLineSpeed ()));
			}
		}

		for (int i = 0; i < vDisconnected.GetSize (); i++) {
			CStringArray vState;

			vState.Add (_T ("OFFLINE"));

			v.Add (FoxjetDatabase::ToString (vDisconnected [i].m_lID));
			v.Add (FoxjetDatabase::ToString (DISABLED)); 
			v.Add (FoxjetDatabase::ToString (vState));
		}
	}

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketGetCount (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	unsigned __int64 lCount = 0;
	CString str;
	CControlDoc * pDoc = GetDocument ();
	const CString strLine = CServerThread::GetLineParam (vCmd);

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
		__int64 lTmp, lMinCount = 0, lMaxCount = 0, lMasterCount = 0;

		pLine->GetCounts (NULL, lTmp, lMinCount, lMaxCount, lMasterCount);
		lCount = lMasterCount;
	}

	str.Format (_T ("%I64u"), lCount);

	v.Add (BW_HOST_PACKET_GET_COUNT);
	v.Add (strLine);
	v.Add (str);

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}


LRESULT CControlView::OnSocketSetCount (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	bool bSet = false;
	CControlDoc * pDoc = GetDocument ();
	const CString strLine = CServerThread::GetLineParam (vCmd);

	if (vCmd.GetSize () > 2) {
		CString str = vCmd [2];
		unsigned __int64 lCount = _tcstoui64 (str, NULL, 10);

		if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) 
			bSet = pLine->SetCounts (lCount);
	}

	v.Add (BW_HOST_PACKET_SET_COUNT);
	v.Add (strLine);
	v.Add (bSet ? _T ("1") : _T ("0"));

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketGetCountMax (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	unsigned __int64 lCount = 0;
	CString str;
	CControlDoc * pDoc = GetDocument ();
	const CString strLine = CServerThread::GetLineParam (vCmd);

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) 
		lCount = pLine->GetCountUntil ();

	str.Format (_T ("%I64u"), lCount);

	v.Add (BW_HOST_PACKET_GET_COUNT_MAX);
	v.Add (strLine);
	v.Add (str);

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}


LRESULT CControlView::OnSocketSetCountMax (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	bool bSet = false;
	CControlDoc * pDoc = GetDocument ();
	const CString strLine = CServerThread::GetLineParam (vCmd);

	if (vCmd.GetSize () > 2) {
		CString str = vCmd [2];
		unsigned __int64 lCount = FoxjetCommon::strtoul64 (str);

		if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) 
			bSet = pLine->SetCountUntil (lCount);
	}

	v.Add (BW_HOST_PACKET_SET_COUNT_MAX);
	v.Add (strLine);
	v.Add (bSet ? _T ("1") : _T ("0"));

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketClose (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	CControlDoc * pDoc = GetDocument ();


	return 0;
}

void CControlView::UpdateSerialData (CString str, bool bFormat)
{
	if (m_pdlgInfo) {
		if (!m_pdlgInfo->IsWindowVisible ())
			return;

		if (!bFormat) {
			for (int i = str.GetLength () - 1; i >= 0; i--) {
				if (str [i] == '\0') {
					str.Delete (i);
					str.Insert (i, _T ("<NULL>"));
				}
			}
		}

		CString strTemp = !bFormat ? str : FoxjetDatabase::Format (str, str.GetLength ());
		CString str;

		m_vDiagnostic.Add (strTemp);

		while (m_vDiagnostic.GetSize () > 200)
			m_vDiagnostic.RemoveAt (0);

		for (int i = 0; i < m_vDiagnostic.GetSize (); i++)
			str += m_vDiagnostic [i] + _T ("\r\n");

		//m_pdlgInfo->m_strPortData = str;

		if (CEdit * p = (CEdit *)m_pdlgInfo->GetDlgItem (TXT_DATA3)) {
			p->SetWindowText (str);
			p->LineScroll (m_vDiagnostic.GetSize ());
			p->Invalidate ();
			p->RedrawWindow ();
		}

		/*
		if (lpszLogfile) {
			if (FILE * fp = _tfopen (lpszLogfile, _T ("a"))) {
				sTemp += '\n';
				fwrite ((LPVOID)(LPCTSTR)sTemp, sTemp.GetLength (), 1, fp);
				fclose (fp);
			}
		}
		*/
	}
}

void CControlView::OnCancelStrobe() 
{
	theApp.CancelStrobe ();
}


int CControlView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}

void CControlView::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	
	CFormView::OnClose();
}

void CControlView::BeginLocalDbThreads ()
{
	LoadLocalDbSettings ();

	if (m_vLocalDbThreads.GetSize ())
		EndLocalDbThreads ();

	m_vLocalDbThreads.Add ((CLocalDbThread *)::AfxBeginThread (RUNTIME_CLASS (CLocalDbThread)));
	m_vLocalDbThreads.Add ((CLocalDbThread *)::AfxBeginThread (RUNTIME_CLASS (CLocalLogoThread)));

	for (int i = 0; i < m_vLocalDbThreads.GetSize (); i++) {
		if (CLocalDbThread * pThread = (CLocalDbThread *)m_vLocalDbThreads [i]) {
			VERIFY (pThread->SetThreadPriority (THREAD_PRIORITY_IDLE));

			//if (!pThread->IsKindOf (RUNTIME_CLASS (CLocalLogoThread)))
				while (!::PostThreadMessage (pThread->m_nThreadID, TM_LOCAL_MANUAL, 0, 1))
					::Sleep (0);
		}
	}
}

LRESULT CControlView::OnLocalDbBackup (WPARAM wParam, LPARAM lParam)
{
	UINT nMsg = TM_LOCAL_MANUAL;
	WPARAM w = 0;
	LPARAM l = 0;

	switch ((int)wParam) {
	case 0: // backup
		nMsg = TM_LOCAL_MANUAL;
		break;
	case -1: // backup all
		nMsg = TM_LOCAL_MANUAL;
		w = 1;
		break;
	case 1: // cancel
		nMsg = TM_LOCAL_CANCEL;
		break;
	}

	for (int i = 0; i < m_vLocalDbThreads.GetSize (); i++) {
		if (CLocalDbThread * pThread = (CLocalDbThread *)m_vLocalDbThreads [i]) {
			while (!::PostThreadMessage (pThread->m_nThreadID, nMsg, w, l))
				::Sleep (0);
		}
	}

	return 0;
}

void CControlView::LoadLocalDbSettings ()
{
	#ifdef __WINPRINTER__
	using namespace FoxjetUtils;
	using namespace FoxjetElements;
	using namespace ListGlobals;

	CLocalDbDlg dlg;
	dlg.Load (theApp.m_Database);

	::EnterCriticalSection (&LocalDB::cs);
	
	LocalDB::bEnabled = dlg.m_bEnabled ? true : false;
	LocalDB::vTrigger.RemoveAll ();
	LocalDB::vTrigger.Append (dlg.m_vTrigger);

	{
		CString strNetworkedBMP = CBitmapElement::GetNetworkFilePath ();
		CString strLocalBMP = CBitmapElement::GetLocalFilePath ();

		TRACEF (_T ("local:   ") + strLocalBMP);
		TRACEF (_T ("network: ") + strNetworkedBMP);
		
		_tcsncpy (LocalDB::szLocalBMP,		strLocalBMP,		MAX_PATH);
		_tcsncpy (LocalDB::szNetworkBMP,	strNetworkedBMP,	MAX_PATH);
	}

	::LeaveCriticalSection (&LocalDB::cs);

	if (!LocalDB::bEnabled)
		EndLocalDbThreads ();
	#endif //__WINPRINTER__
}

void CControlView::EndLocalDbThreads ()
{
	while (m_vLocalDbThreads.GetSize ()) {
		if (CLocalDbThread * p = m_vLocalDbThreads [0]) {
			while (!::PostThreadMessage (p->m_nThreadID, TM_LOCAL_CANCEL, 0, 0))
				::Sleep (0);

			while (!::PostThreadMessage (p->m_nThreadID, TM_KILLTHREAD, 0, 0))
				::Sleep (0);

			VERIFY (::WaitForSingleObject (p->m_hThread, 5000) == WAIT_OBJECT_0);
			m_vLocalDbThreads.RemoveAt (0);
		}
	}
}

void CControlView::BeginCommThreads ()
{
	CUIntArray v;
	
	if (m_vCommThreads.GetSize ())
		EndCommThreads ();

	FoxjetCommon::EnumerateSerialPorts (v);

	for (int i = 0; i < v.GetSize (); i++) {
		SETTINGSSTRUCT s;
		CString strKey;

		strKey.Format (_T ("COM%d"), v [i]);

		if (GetSettingsRecord (theApp.m_Database, 0, strKey, s)) {
			bool bStarted = false;
			CComm * p = new CComm;

			p->m_hControlWnd = GetSafeHwnd();

			if (p->FromString (s.m_strData)) {
				if (p->m_type != NOTUSED) {
					p->m_hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)CommThreadFunc, 
						(LPVOID)p, 0, &p->m_dwThreadID);

					m_vCommThreads.Add (p);
					bStarted = true;
				}
			}

			if (!bStarted)
				delete p;
		}
	}
}

void CControlView::EndCommThreads ()
{
	while (m_vCommThreads.GetSize ()) {
		if (CComm * p = m_vCommThreads [0]) {
			TRACEF ("delete: " + p->ToString ());

			p->m_dwThreadID = 0; // signal to die

			if (::WaitForSingleObject (p->m_hThread, 1000) == WAIT_OBJECT_0) {
				m_vCommThreads.RemoveAt (0);
				delete p;
			}
			else {
				TRACEF ("WaitForSingleObject failed");
				ASSERT (0);
			}
		}
	}
}

ControlView::CComm * CControlView::GetCommHandle (int nPort)
{
	for (int i = 0; i < m_vCommThreads.GetSize (); i++) 
		if (CComm * p = m_vCommThreads [i]) 
			if (p->m_dwPort == (DWORD)nPort)
				return p;

	return NULL;
}

#ifdef _DEBUG
BYTE szDebugBuffer [COM_BUF_SIZE] = { 0 };
int nDebugBufferSize = 0;
DIAGNOSTIC_CRITICAL_SECTION (csDebugBuffer);
#endif

static int ReverseFind (const CString & str, TCHAR cFind)
{
	for (int i = str.GetLength () - 1; i >= 0; i--)
		if (str [i] == cFind)
			return i;

	return -1;
}

ULONG CALLBACK CControlView::ColorFunc (LPVOID lpData) 
{
	CControlView * pView = (CControlView *)lpData;

	const CString strKey = CControlApp::GetRegKey () + _T ("\\Settings\\Colors");

	static COLORREF rgbBackLast				= FoxjetDatabase::GetProfileInt (strKey, _T ("background"),				-1);
	static COLORREF rgbFontLast				= FoxjetDatabase::GetProfileInt (strKey, _T ("button font"),			-1);
	static COLORREF rgbBorderLast			= FoxjetDatabase::GetProfileInt (strKey, _T ("button border"),			-1);
	static COLORREF rgbFaceLast				= FoxjetDatabase::GetProfileInt (strKey, _T ("button face"),			-1);
	static COLORREF rgbShadedLast 			= FoxjetDatabase::GetProfileInt (strKey, _T ("shaded"),					-1);
	static COLORREF rgbClickedFaceLast		= FoxjetDatabase::GetProfileInt (strKey, _T ("button face clicked"),	-1);
	static COLORREF rgbClickedBorderLast	= FoxjetDatabase::GetProfileInt (strKey, _T ("button border clicked"),	-1);

	while (pView->IsWindowVisible ()) {
		COLORREF rgbBack			= FoxjetDatabase::GetProfileInt (strKey, _T ("background"),				-1);
		COLORREF rgbFont			= FoxjetDatabase::GetProfileInt (strKey, _T ("button font"),			-1);
		COLORREF rgbBorder			= FoxjetDatabase::GetProfileInt (strKey, _T ("button border"),			-1);
		COLORREF rgbFace 			= FoxjetDatabase::GetProfileInt (strKey, _T ("button face"),			-1);
		COLORREF rgbShaded			= FoxjetDatabase::GetProfileInt (strKey, _T ("shaded"),					-1);
		COLORREF rgbClickedFace		= FoxjetDatabase::GetProfileInt (strKey, _T ("button face clicked"),	-1);
		COLORREF rgbClickedBorder	= FoxjetDatabase::GetProfileInt (strKey, _T ("button border clicked"),	-1);

		bool bChanged = 
			rgbBack				!= rgbBackLast ||
			rgbFont				!= rgbFontLast ||
			rgbBorder			!= rgbBorderLast ||
			rgbFace				!= rgbFaceLast ||
			rgbShaded			!= rgbShadedLast ||
			rgbClickedFace		!= rgbClickedFaceLast ||
			rgbClickedBorder	!= rgbClickedBorderLast;

		if (bChanged) 
			pView->PostMessage (WM_COLORS_CHANGED, 1, 0);

		rgbBackLast				= rgbBack;
		rgbFontLast				= rgbFont;
		rgbBorderLast			= rgbBorder;
		rgbFaceLast				= rgbFace;
		rgbShadedLast			= rgbShaded;
		rgbClickedFaceLast		= rgbClickedFace;
		rgbClickedBorderLast	= rgbClickedBorder;

		::Sleep (500);
	}

	TRACEF ("CControlView::ColorFunc exiting");

	return 0;
}

static void SaveDynamicData (ULONG lLineID, ULONG lID, const CString & str, CControlView * pView)
{
	using FoxjetIpElements::CTableItem;

	CArray <CTableItem, CTableItem &> vItems;
	CTableItem item;

	item.m_lID = lID;
	item.m_strData = str;

	FoxjetIpElements::GetDynamicData (lLineID, vItems);

	for (int i = 0; i < vItems.GetSize (); i++) {
		if (vItems [i].m_lID == lID) {
			CStringArray v, vData;

			vItems.SetAt (i, item);
			FoxjetIpElements::SetDynamicData (lLineID, vItems);

			for (int j= 0; j < vItems.GetSize (); j++) 
				vData.Add (vItems [j].ToString ());

			v.Add (_T ("Dynamic data"));
			v.Add (ToString (vData));
			VERIFY (pView->SendMessage (WM_NEXT_SET_SYSTEM_PARAMS, (WPARAM)lLineID, (LPARAM)&v) == 1);

			break;
		}
	}
}

ULONG CALLBACK CControlView::CommThreadFunc (LPVOID lpData) 
{
	using namespace FoxjetDatabase;
	using namespace FoxjetCommon;

	ASSERT (lpData);

	CComm & c = * (CComm *)lpData;
	BYTE nParity = 0;
	//CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CString strData;
	CString strPort;

	strPort.Format (_T ("COM%d"), c.m_dwPort);
	//GetPrinterLines (theApp.m_Database, GetPrinterID (theApp.m_Database), vLines); //GetLineRecords (theApp.m_Database, vLines);

	TRACEF (_T ("CommThreadFunc: ") + c.ToString ());

	switch (c.m_parity) {
	case NO_PARITY:		nParity = NOPARITY;		break;
	case EVEN_PARITY:	nParity = EVENPARITY;	break;
	case ODD_PARITY:	nParity = ODDPARITY;	break;
	}

	c.m_hComm = Open_Comport (c.m_dwPort, c.m_dwBaudRate, c.m_nByteSize, nParity, c.m_nStopBits); 

	if (!c.m_hComm || c.m_hComm == INVALID_HANDLE_VALUE) {
		TRACEF ("Open_Comport failed");
		ASSERT (0);
		return 0;
	}

	while (c.m_dwThreadID) {
		BYTE buffer [COM_BUF_SIZE];
		DWORD dwBytesRead = 0;

		ZeroMemory (buffer, sizeof (buffer));

		{
			LOCK (c.m_cs);

			Read_Comport (c.m_hComm, &dwBytesRead, sizeof (buffer), buffer);

			#ifdef _DEBUG
			LOCK (::csDebugBuffer);
			{
				if (::nDebugBufferSize) {
					memcpy (buffer, ::szDebugBuffer, ARRAYSIZE (::szDebugBuffer));
					memset (::szDebugBuffer, 0, ARRAYSIZE (::szDebugBuffer));
					dwBytesRead = ::nDebugBufferSize;
					::nDebugBufferSize = 0;

					if (FILE * fp = _tfopen (GetHomeDir () + _T ("\\comm.txt"), _T ("ab"))) {
						fwrite (&c, 1, 1, fp);
						fclose (fp);
					}
				}
			}
			#endif //_DEBUG
		}

		for (int i = 0; i < (int)dwBytesRead; i++) {
			if (buffer [i] == 0x0c)
				buffer [i] = '\r';

			strData += (TCHAR)buffer [i];
		}

		if (dwBytesRead) {
			/*
			#ifdef _DEBUG
			int nLen = strData.GetLength ();
			TRACEF (Format (strData, nLen));
			TRACEF (strData);
			//((CControlView * )(((CMainFrame *)theApp.m_pMainWnd)->m_pControlView))->UpdateSerialData (strData, false);
			//((CControlView * )(((CMainFrame *)theApp.m_pMainWnd)->m_pControlView))->UpdateSerialData (strData, true);

			for (int nEncoding = 1251; nEncoding <= 1252; nEncoding++) {
				CString str = ToString (nEncoding) + _T (": ") + CStdCommDlg::CCommItem::Encode (strData, nEncoding);

				TRACEF (Format (str, str.GetLength ()));
				((CControlView * )(((CMainFrame *)theApp.m_pMainWnd)->m_pControlView))->UpdateSerialData (str, false);
			}
			#endif
			*/

			if (::g_pNEXTPortType) {
				static const auto strRegSection = NEXT::CNextThread::m_strRegSection;

				if (strData.Find ('\r') != -1) {
					CMainFrame *pFrame = (CMainFrame *)theApp.m_pMainWnd;
					CControlView *pView = pFrame->m_pControlView;
					CControlDoc * pDoc = pView->GetDocument ();
					ULONG lLineID = 0;
					CString str = strData;

					if (CProdLine * pLine = pDoc->GetLine (0))
						lLineID = pLine->GetID ();

					strData.Empty ();
					str.Replace (_T ("\r"), _T (""));
					TRACEF (str);
					((CControlView * )(((CMainFrame *)theApp.m_pMainWnd)->m_pControlView))->UpdateSerialData (str);

					switch (* ::g_pNEXTPortType)
					{
					case NEXT::VARDATA:
						{
							CString strSAVE_SERIAL_DATA = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strRegSection, _T ("SAVE_SERIAL_DATA"), _T (""));
							bool bSAVE_SERIAL_DATA = !strSAVE_SERIAL_DATA.CompareNoCase (_T ("T"));

							ULONG lID = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strRegSection, _T ("SerialVarDataID"), 0);
							VERIFY (fj_setWinDynData (lID + 1, w2a (str)));

							if (bSAVE_SERIAL_DATA) 
								SaveDynamicData (lLineID, lID + 1, str, pView);
						}
						break;
					case NEXT::DYNMEM:
						if (str.GetLength () >= 2) {
							ULONG lID = 0;

							if (_stscanf (str.Left (2), _T ("%d"), &lID) == 1) {
								if (str.GetLength () > 2) {
									CString strSAVE_DYN_DATA = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strRegSection, _T ("SAVE_DYN_DATA"), _T (""));
									bool bSAVE_DYN_DATA = !strSAVE_DYN_DATA.CompareNoCase (_T ("T"));

									str.Delete (0, 2);
									VERIFY (fj_setWinDynData (lID, w2a (str)));

									if (bSAVE_DYN_DATA) 
										SaveDynamicData (lLineID, lID, str, pView);
								}
								else if (str.GetLength () == 2) {
									while (!::PostMessage (c.m_hControlWnd, WM_NEXT_SET_DYNAMIC_INDEX, (WPARAM)_ttoi (str), (LPARAM)0))
										::Sleep (0);
								}
							}
						}
						break;
					case NEXT::SCANNER_LABEL:
						while (!::PostMessage (c.m_hControlWnd, WM_NEXT_TASK_START, (WPARAM)new CString (str), (LPARAM)1))
							::Sleep (0);
						break;
					}
				}

				continue;
			}

			if (c.m_type == HOSTINTERFACE) {
				{ // 4.10.0923.014
					CStringArray v;
					bool bProcessed = false;

					for (int nIndex = 0; (nIndex = strData.Find ('\r', 0)) != -1; ) {
						v.Add (strData.Left (nIndex + 1));
						strData.Delete (0, nIndex + 1);
					}

					for (int i = v.GetSize () - 1; i >= 0; i--) {
						CString str = v [i];
						if (str.GetLength () >= 2) { 

							switch (str [0]) {
							case 'a': case 'A':
							case 'b': case 'B':
								{
									if (str.Find (_T ("\r")) != -1) {
										CSerialPacket * p = new CSerialPacket (strPort);

										p->AssignData (SERIALSRC_WIN32COMM, c.m_type, _T (""), str, str.GetLength (), c.m_nEncoding);

										while (!::PostMessage (c.m_hControlWnd, WM_HEAD_SERIAL_DATA, (WPARAM)&c, (LPARAM)p)) 
											::Sleep (0);

										strData.Empty ();
										bProcessed = true;
									}
									break;
								}
							}

							if (bProcessed)
								break;
						}
					}

					if (bProcessed)
						continue;
				}


				if (CUdpThread::CheckForFlush (strData)) {
					CSerialPacket * p = new CSerialPacket (strPort);
					CString str ('\r', 5);

					strData.Empty ();
					p->AssignData (SERIALSRC_WIN32COMM, c.m_type, _T (""), str, str.GetLength (), c.m_nEncoding);

					while (!::PostMessage (c.m_hControlWnd, WM_HEAD_SERIAL_DATA, (WPARAM)&c, (LPARAM)p)) 
						::Sleep (0);

					continue;
				}

				int nLeft = CountChars (strData, '{').GetSize ();
				int nRight = CountChars (strData, '}').GetSize ();

				if (nLeft && nRight == nLeft) {
					CStringArray v;

					ParsePackets (strData, v);
					strData.Empty ();

					for (int i = 0; i < v.GetSize (); i++) {
						const CString str = '{' + v [i] + '}';
						CSerialPacket * p = new CSerialPacket (strPort);

						p->AssignData (SERIALSRC_WIN32COMM, c.m_type, _T (""), str, str.GetLength (), c.m_nEncoding);

						while (!::PostMessage (c.m_hControlWnd, WM_HEAD_SERIAL_DATA, (WPARAM)&c, (LPARAM)p)) 
							::Sleep (0);
					}
				}
			}
			else {
				#ifdef _DEBUG
				if (::nDebugBufferSize > 0) {
					strData.Replace (_T ("\r"), _T ("")); 
					strData += _T ("\r"); 
					TRACEF ("**************************** TODO: rem");
				}
				#endif


				const TCHAR cFind = '\r';
				int nIndex = ReverseFind (strData, cFind);
				CString strLine;

				if (nIndex != -1) {
					CSerialPacket * p = new CSerialPacket (strPort);
					const CString str = strData.Left (nIndex);
					strData = strData.Mid (nIndex);
					strData.Remove (cFind);

					for (int nLine = 0; nLine < ::vLines.GetSize (); nLine++) {
						LINESTRUCT line = ::vLines [nLine];

						if (line.m_lID == c.m_lLineID) {
							strLine = line.m_strName;
							break;
						}
					}

					/*
					#ifdef _DEBUG
					for (int i = 0; i < str.GetLength (); i++) {
						TCHAR c = str [i];
						CString strChar, strTmp;

						if (FILE * fp = _tfopen (GetHomeDir () + _T ("\\comm.txt"), _T ("ab"))) {
							fwrite (&c, 1, 1, fp);
							fclose (fp);
						}

						strTmp.Format (_T ("%c"), c);
						strChar.Format (_T ("[%d]: %s [%d]"), i, strTmp, c);
						//TRACEF (strChar); ((CControlView * )(((CMainFrame *)theApp.m_pMainWnd)->m_pControlView))->UpdateSerialData (strChar, false);
					}
					#endif //_DEBUG
					*/

					TRACEF (str);
					p->AssignData (SERIALSRC_WIN32COMM, c.m_type, strLine, str, str.GetLength (), c.m_nEncoding);
					TRACEF (FoxjetDatabase::Format (p->m_pData, p->m_nDataLen));

					while (!::PostMessage (c.m_hControlWnd, WM_HEAD_SERIAL_DATA, (WPARAM)&c, (LPARAM)p)) 
						::Sleep (0);
				}
			}
		}

		::Sleep (100);
	}

	Close_Comport (c.m_hComm);
	c.m_hComm = NULL;

	return 0;
}

void CControlView::ResetScannerInfo ()
{
	CControlDoc * pDoc = (CControlDoc *)GetDocument (); 
	CStringArray vLines;

	ASSERT (pDoc);

	pDoc->GetProductLines (vLines);

	for (int nThread = 0; nThread < m_vCommThreads.GetSize (); nThread++) {
		CComm * p = m_vCommThreads [nThread];

		ASSERT (p);
		
		for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
			CString strLine = vLines [nLine];

			if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
				if (pLine->GetID () == p->m_lLineID) {
					pLine->ResetVerifierData ();
				}
			}
		}
	}
}

bool CControlView::GetLineState (ULONG lExcludeID, int & nLowInk, int & nOther)
{
	CControlDoc * pDoc = (CControlDoc *)GetDocument (); 
	CStringArray vLines;
	bool bResult = false;

	ASSERT (pDoc);

	pDoc->GetProductLines (vLines);

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		CString strLine = vLines [nLine];

		if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
			if (pLine->GetID () != lExcludeID) {
				pLine->GetLineState (nLowInk, nOther);
				bResult = true;
			}
		}
	}

	return bResult;
}

void CControlView::SetPreview (bool bPreview)
{
	CRect rc;

	m_bPreview = bPreview;
	FoxjetDatabase::WriteProfileInt (CControlApp::GetRegKey () + _T ("\\Settings"), _T ("Preview"), m_bPreview);

	if (!m_bPreview) {
		if (m_pPreview) {
			m_pPreview->SetPreviewPanel (NULL, -1);
		}
	}

	Invalidate ();
	Resize ();
}

LRESULT CControlView::OnRefreshPreview (WPARAM wParam, LPARAM lParam)
{

//#if __CUSTOM__ == __SW0828__

	CHead * pHead = (CHead *)lParam;
	CControlDoc * pDoc = GetDocument();

	ASSERT (pHead);

	if (CProdLine * pLine = pDoc->GetSelectedLine ()) 
		if (CHead::GetInfo (pHead).m_Config.m_Line.m_lID == pLine->m_LineRec.m_lID) 
			OnViewRefreshpreview ();

//#endif

	return 0;
}

void CControlView::OnViewRefreshpreview() 
{
	ViewRefreshpreview ();
}

void CControlView::ViewRefreshpreview()
{
	using namespace ItiLibrary;

	//CHead * pSelectedHead = NULL;
	CStringArray vLines;
	CControlDoc *pDoc = GetDocument();

	pDoc->GetProductLines (vLines);

	for (int i = 0; i < vLines.GetSize (); i++) {
		CString strLine = vLines [i];
		HANDLE hEvent [MAX_HEADS] = { NULL };
		int nHeads = 0;

		CProdLine * pProdLine = pDoc->GetProductionLine (strLine);
		ASSERT (pProdLine);

		if (pProdLine->GetCurrentTask ().GetLength () && pProdLine->GetTaskState () != STOPPED) {
			CPrintHeadList list;

			pProdLine->GetActiveHeads (list);

			for (POSITION pos = list.GetStartPosition (); pos; nHeads++) {
				CHead * pHead = NULL;
				DWORD dw;
				CString str;
			
				list.GetNextAssoc (pos, dw, (void *&)pHead);
				ASSERT (pHead);

				str.Format (_T ("BuildImage %lu"), pHead->m_hThread);
				hEvent [nHeads] = ::CreateEvent (NULL, TRUE, FALSE, str);

				TRACEF (CHead::GetInfo (pHead).GetUID () + _T (": TM_BUILD_PREVIEW_IMAGE"));

				while (!::PostThreadMessage (pHead->m_nThreadID, TM_BUILD_PREVIEW_IMAGE, (WPARAM)hEvent [nHeads], 0))
					::Sleep (0);
			}
			
			DWORD dwWait = ::WaitForMultipleObjects (nHeads, hEvent, TRUE, 3000);
			
			if (dwWait == WAIT_TIMEOUT) TRACEF ("WAIT_TIMEOUT");
			else if ((dwWait >= WAIT_ABANDONED_0) && (dwWait <= (WAIT_ABANDONED_0 + nHeads - 1))) TRACEF ("WAIT_ABANDONED_0");

			for (int i = 0; i < nHeads; i++)
				::CloseHandle (hEvent [i]);
		}
	}	
	
	OnChangePanelTab (NULL, NULL);
	REPAINT (m_pPreview); 
}

LRESULT CControlView::OnSocketGetHeads (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	const CString strLine = CServerThread::GetLineParam (vCmd);
	CControlDoc * pDoc = GetDocument ();

	v.Add (BW_HOST_PACKET_GET_HEADS);
	v.Add (strLine);

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
		CPrintHeadList list;

		pLine->GetActiveHeads (list);

		for (POSITION pos = list.GetStartPosition(); pos; ) {
			DWORD dwKey;
			CHead * pHead = NULL;

			list.GetNextAssoc(pos, dwKey, (void *&)pHead);
			ASSERT (pHead);

			v.Add (FoxjetDatabase::ToString (CHead::GetInfo (pHead).m_Config.m_HeadSettings));
		}
	}

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketLoadTask (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	bool bLoad = false; 
	const CString strLine = CServerThread::GetLineParam (vCmd);

	if (vCmd.GetSize () > 2) {
		CString strTask = vCmd [2];
		CControlDoc * pDoc = GetDocument ();

		if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
			ULONG lTaskID = FoxjetDatabase::GetTaskID (theApp.m_Database, strTask, strLine, GetPrinterID (theApp.m_Database));

			if (!strTask.CompareNoCase (_T ("PrintDriver"))) {
				using namespace FoxjetElements;

				TASKSTRUCT task;
				BOXSTRUCT box;
				CPrintHeadList list;
				const CString strFile = FoxjetIpElements::CBitmapElement::GetDefPath () + _T ("IPC_PUT_BMP_DRIVER.bmp");
				int n = 11;

				//#ifdef _DEBUG
				//* ((CString *)(LPVOID)&strFile) = _T ("C:\\Foxjet\\IP\\Logos\\Arkady.bmp");
				//#endif

				box.m_lID = GetBoxID (theApp.m_Database, strTask);
				GetBoxRecord (theApp.m_Database, box.m_lID, box);
				pLine->GetActiveHeads (list);

				for (POSITION pos = list.GetStartPosition(); pos; ) {
					DWORD dwKey;
					CHead * pHead = NULL;
					const FoxjetCommon::CBaseElement & def = ListGlobals::defElements.GetElement (BMP);

					list.GetNextAssoc(pos, dwKey, (void *&)pHead);
					ASSERT (pHead);
					CHeadInfo info = CHead::GetInfo (pHead);

					const double dDpiX = info.m_Config.m_HeadSettings.GetRes ();
					const double dDpiY = info.m_Config.m_HeadSettings.GetChannels () / (double)info.m_Config.m_HeadSettings.GetSpan ();
					CSize sizePixels = CBitmapElement::GetSizePixels (strFile, info.m_Config.m_HeadSettings);
					CSize size (sizePixels.cx / dDpiX * 1000.0, sizePixels.cy / dDpiY * 1000.0); 

					n = max (n, (int)max (ceil (size.cx / 1000.0), (int)ceil (size.cy / 1000.0)));
					break;
				}

				if (box.m_lID == NOTFOUND) {
					box.m_strName = strTask;
					box.m_lHeight = box.m_lWidth = box.m_lLength = n * 1000;
					AddBoxRecord (theApp.m_Database, box);
				}

				{
					const int n = box.m_lLength;
					
					box.m_lLength = std::min <long> ((n * 1000), MAX_PRODLEN);

					if (n != box.m_lLength)
						VERIFY (UpdateBoxRecord (theApp.m_Database, box));
				}

				task.m_lBoxID = box.m_lID;
				task.m_lID = -1;
				task.m_lLineID = pLine->GetID ();
				task.m_strName = strTask;

				for (POSITION pos = list.GetStartPosition(); pos; ) {
					DWORD dwKey;
					CHead * pHead = NULL;
					const FoxjetCommon::CBaseElement & def = ListGlobals::defElements.GetElement (BMP);

					list.GetNextAssoc(pos, dwKey, (void *&)pHead);
					ASSERT (pHead);
					CHeadInfo info = CHead::GetInfo (pHead);
					MESSAGESTRUCT msg;

					msg.m_lHeadID		= info.m_Config.m_HeadSettings.m_lID;
					msg.m_lTaskID		= task.m_lID;
					msg.m_strName		= task.m_strName;
					
					if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, CopyElement (&def, &info.m_Config.m_HeadSettings, true))) {
						const double dDpiX = info.m_Config.m_HeadSettings.GetRes ();
						const double dDpiY = info.m_Config.m_HeadSettings.GetChannels () / (double)info.m_Config.m_HeadSettings.GetSpan ();
						CString strPA;

						p->SetPos (CPoint (0, 0), &info.m_Config.m_HeadSettings);
						p->SetFilePath (strFile, info.m_Config.m_HeadSettings);
		
						const CSize sizePixels = CBitmapElement::GetSizePixels (strFile, info.m_Config.m_HeadSettings);
						const CSize size (sizePixels.cx / dDpiX * 1000.0, sizePixels.cy / dDpiY * 1000.0); 
						int nHeight = FoxjetCommon::CBaseElement::LogicalToThousandths (CPoint (0, info.m_Config.m_HeadSettings.Height ()), info.m_Config.m_HeadSettings).y;

						strPA.Format (_T ("{ProdArea,%d,%d,%d,%d}"), n * 1000, nHeight, 0, info.m_Config.m_HeadSettings.m_lID);
						p->SetSize (size, info.m_Config.m_HeadSettings, true);

						//#ifdef _DEBUG
						//{ CString s; s.Format (_T ("(%d, %d)"), sizePixels.cx, sizePixels.cy); TRACEF (s); }
						//#endif

						msg.m_strData = strPA + p->ToString (FoxjetCommon::verApp);
						TRACEF (msg.m_strData);

						delete p;
					}

					task.m_vMsgs.Add (msg);
				}

				#ifdef _DEBUG
				{
					CString str;

					str.Format (_T ("n: %d"), n);
					TRACEF (str);

					// {ProdArea,38542,4000,0,21}

					for (int i = 0; i < task.m_vMsgs.GetSize (); i++) {
						str.Format (_T ("%d: %s"), task.m_vMsgs [i].m_lHeadID,  task.m_vMsgs [i].m_strData);
						TRACEF (str);
					}

					int nBreak = 0;
				}
				#endif

				OnAddTask ((WPARAM)&task, 0);
			}

			pLine->StopTask (false, this);
			bLoad = pLine->LoadTask (lTaskID, true, false, false, this);
			pDoc->UpdateAllViews (this);
		}
	}

	v.Add (BW_HOST_PACKET_LOAD_TASK);
	v.Add (strLine);
	v.Add (bLoad ? _T ("1") : _T ("0"));

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}


LRESULT CControlView::OnSocketStartTask (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	bool bStart = false; 
	const CString strLine = CServerThread::GetLineParam (vCmd);
	CControlDoc * pDoc = GetDocument ();
	CString strTask;

	if (vCmd.GetSize () > 2) 
		strTask = vCmd [2];

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
		int nStart;
		
		if (strTask.GetLength ()) {
			pLine->StopTask (false, this);
			nStart = pLine->StartTask (strTask, true, false, false, this);
		}
		else {
			nStart = pLine->StartTask (true, false, false, this);
		}


		bStart = TASK_START_SUCCEEDED (nStart);

		OnUpdate ( NULL, UPDATE_PREVIEW, NULL );
		pDoc->UpdateAllViews (NULL, UPDATE_PREVIEW, NULL);
	}

	v.Add (BW_HOST_PACKET_START_TASK);
	v.Add (strLine);
	v.Add (bStart ? _T ("1") : _T ("0"));

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketStartTaskDatabase (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	bool bStart = false; 
	const CString strLine = CServerThread::GetLineParam (vCmd);
	CControlDoc * pDoc = GetDocument ();
	CString strTask, strKeyValue;
	FoxjetUtils::CDatabaseStartDlg dlg (NULL);

	if (vCmd.GetSize () > 2) 
		strKeyValue = vCmd [2];

	dlg.Load (theApp.m_Database);

	TRACEF (dlg.m_strDSN);
	TRACEF (dlg.m_strTable);
	TRACEF (dlg.m_strKeyField);
	TRACEF (dlg.m_strTaskField);
	TRACEF (ToString (dlg.m_nSQLType));
	TRACEF (ToString (dlg.m_bShow));

	if (dlg.m_strDSN.GetLength ()) {
		#ifdef __WINPRINTER__
		try {
			using namespace FoxjetElements;

			COdbcCache db = COdbcDatabase::Find (dlg.m_strDSN);
			CString strSQL = COdbcDatabase::CreateSQL (&db.GetDB (), dlg.m_strDSN, dlg.m_strTable, 
				dlg.m_strTaskField, dlg.m_strKeyField, strKeyValue, dlg.m_nSQLType);
			TRACEF (strSQL);
			COdbcCache rstCache = COdbcDatabase::Find (db.GetDB (), dlg.m_strDSN, strSQL);
			COdbcRecordset & rst = rstCache.GetRst ();

			TRACEF ((CString)rst.GetFieldValue (dlg.m_strKeyField));
			TRACEF ((CString)rst.GetFieldValue (dlg.m_strTaskField));

			//strKeyValue = rst.GetFieldValue (dlg.m_strKeyField);
			strTask = rst.GetFieldValue (dlg.m_strTaskField);

			TRACEF (strKeyValue);
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
		#endif //__WINPRINTER__

		TRACEF (strTask + _T (" --> ") + strKeyValue);

		if (strTask.GetLength ()) {
			if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
				int nStart;

				if (strTask.GetLength ()) {
					pLine->StopTask (false, this);
					nStart = pLine->StartTask (strTask, true, false, false, this, 0, 0, false, strKeyValue);
				}
				else {
					nStart = pLine->StartTask (true, false, false, this);
				}


				bStart = TASK_START_SUCCEEDED (nStart);

				OnUpdate ( NULL, UPDATE_PREVIEW, NULL );
				pDoc->UpdateAllViews (NULL, UPDATE_PREVIEW, NULL);
			}
		}
	}

	v.Add (BW_HOST_PACKET_START_TASK_DATABASE);
	v.Add (strLine);
	v.Add (bStart ? _T ("1") : _T ("0"));

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketGetElements (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	bool bStart = false; 
	const CString strLine = CServerThread::GetLineParam (vCmd);
	CControlDoc * pDoc = GetDocument ();
	ULONG lHeadID = -1;

	if (vCmd.GetSize () >= 3) 
		lHeadID = _ttoi (vCmd [2]);

	v.Add (BW_HOST_PACKET_GET_ELEMENTS);
	v.Add (strLine);
	v.Add (FoxjetDatabase::ToString (lHeadID));

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
		CPrintHeadList list;

		pLine->GetActiveHeads (list);

		for (POSITION pos = list.GetStartPosition(); pos; ) {
			DWORD dwKey;
			CHead * pHead = NULL;

			list.GetNextAssoc(pos, dwKey, (void *&)pHead);
			ASSERT (pHead);
			CHeadInfo info = CHead::GetInfo (pHead);

			if (CHead::GetInfo (pHead).m_Config.m_HeadSettings.m_lID == lHeadID) {
				Head::CElementPtrArray vElements;
				CHead::GETELEMENTSTRUCT s (&vElements, -1);

				SEND_THREAD_MESSAGE (info, TM_GET_ELEMENTS, &s);
				
				for (int i = 0; i < vElements.GetSize (); i++) 
					v.Add (vElements [i]->ToString (FoxjetCommon::verApp));

				break;
			}
		}
	}

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketDeleteElement (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	bool bStart = false; 
	const CString strLine = CServerThread::GetLineParam (vCmd);
	CControlDoc * pDoc = GetDocument ();
	ULONG lHeadID = -1;

	if (vCmd.GetSize () >= 3) 
		lHeadID = _ttoi (vCmd [2]);

	v.Add (BW_HOST_PACKET_DELETE_ELEMENT);
	v.Add (strLine);
	v.Add (FoxjetDatabase::ToString (lHeadID));

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
		CPrintHeadList list;

		pLine->GetActiveHeads (list);

		for (POSITION pos = list.GetStartPosition(); pos; ) {
			DWORD dwKey;
			CHead * pHead = NULL;

			list.GetNextAssoc(pos, dwKey, (void *&)pHead);
			ASSERT (pHead);
			CHeadInfo info = CHead::GetInfo (pHead);

			if (info.m_Config.m_HeadSettings.m_lID == lHeadID) {
				for (int i = 3; i < vCmd.GetSize (); i++) {
					ULONG lID = _ttol (vCmd [i]);
					CHead::UPDATELEMENTSTRUCT s (lID);
					
					SEND_THREAD_MESSAGE (info, TM_DELETE_ELEMENT, &s);
					v.Add (FoxjetDatabase::ToString (s.m_bResult));
				}

				break;
			}
		}
	}

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketAddElement (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	bool bStart = false; 
	const CString strLine = CServerThread::GetLineParam (vCmd);
	CControlDoc * pDoc = GetDocument ();
	ULONG lHeadID = -1;

	if (vCmd.GetSize () >= 3) 
		lHeadID = _ttoi (vCmd [2]);

	v.Add (BW_HOST_PACKET_ADD_ELEMENT);
	v.Add (strLine);
	v.Add (FoxjetDatabase::ToString (lHeadID));

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
		CPrintHeadList list;

		pLine->GetActiveHeads (list);

		for (POSITION pos = list.GetStartPosition(); pos; ) {
			DWORD dwKey;
			CHead * pHead = NULL;

			list.GetNextAssoc(pos, dwKey, (void *&)pHead);
			ASSERT (pHead);
			CHeadInfo info = CHead::GetInfo (pHead);

			if (info.m_Config.m_HeadSettings.m_lID == lHeadID) {
				for (int i = 3; i < vCmd.GetSize (); i++) {
					CString strCmd = vCmd [i];
					CString str = FoxjetDatabase::UnformatString (strCmd);
					CHead::UPDATELEMENTSTRUCT s (str);
					
					SEND_THREAD_MESSAGE (info, TM_ADD_ELEMENT, &s);
					v.Add (FoxjetDatabase::ToString (s.m_lID));
				}

				break;
			}
		}
	}

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}


LRESULT CControlView::OnSocketUpdateElement (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	const CString strLine = CServerThread::GetLineParam (vCmd);
	CControlDoc * pDoc = GetDocument ();
	ULONG lHeadID = -1;

	if (vCmd.GetSize () >= 3) 
		lHeadID = _ttoi (vCmd [2]);

	v.Add (BW_HOST_PACKET_UPDATE_ELEMENT);
	v.Add (strLine);
	v.Add (FoxjetDatabase::ToString (lHeadID));

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
		CPrintHeadList list;

		pLine->GetActiveHeads (list);

		for (POSITION pos = list.GetStartPosition(); pos; ) {
			DWORD dwKey;
			CHead * pHead = NULL;

			list.GetNextAssoc(pos, dwKey, (void *&)pHead);
			ASSERT (pHead);
			CHeadInfo info = CHead::GetInfo (pHead);

			if (info.m_Config.m_HeadSettings.m_lID == lHeadID) {				
				for (int i = 3; i < vCmd.GetSize (); i++) {
					CString strCmd = vCmd [i];
					CString str = FoxjetDatabase::UnformatString (strCmd);
					CHead::UPDATELEMENTSTRUCT s (str);
					
					SEND_THREAD_MESSAGE (info, TM_UPDATE_ELEMENT, &s);
					v.Add (FoxjetDatabase::ToString ((int)s.m_bResult));
				}

				break;
			}
		}
	}

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketRefreshPreview (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	
	OnViewRefreshpreview ();
	v.Add (BW_HOST_PACKET_UPDATE_ELEMENT);
	v.Add (FoxjetDatabase::ToString (1));
	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketDbBuild (WPARAM wParam, LPARAM lParam)
{
	using namespace FoxjetDatabase;

	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	COdbcDatabase & db = theApp.m_Database;
	bool bResult = false;

	v.Add (vCmd [0]);

	if (!m_pExport) {
		m_pExport = new CExport ();
		m_pExport->Build (db);
		bResult = true;
	}

	v.Add (FoxjetDatabase::ToString (bResult));

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketDbIsReady (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	bool bResult = false;

	v.Add (vCmd [0]);

	if (m_pExport) {
		if (m_pExport->IsFinished ()) {
			v.Add (FoxjetDatabase::ToString (m_pExport->GetSize ()));
			TRACEF ("CControlView::OnSocketDbIsReady: " + FoxjetDatabase::ToString (m_pExport->GetSize ()));
		}
	}
	else {
		if (m_pImport) {
			if (m_pImport->IsFinished ()) {
				bResult = true;
				delete m_pImport;
				m_pImport = NULL;
			}
		}
		else
			bResult = true;

		v.Add (FoxjetDatabase::ToString ((int)bResult));
	}

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketDbGetNext (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;

	v.Add (vCmd [0]);
	
	if (m_pExport) {
		v.Add (m_pExport->GetNext ());

		if (m_pExport->GetSize () == 0) {
			TRACEF ("delete m_pExport");
			delete m_pExport;
			m_pExport = NULL;
		}
	}
	else if (m_pImport) {
		for (int i = 1; i < vCmd.GetSize (); i++)
			m_pImport->Add (vCmd [i]);
	}

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}


LRESULT CControlView::OnSocketDbBeginUpdate (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	CControlDoc * pDoc = GetDocument ();
	bool bResult = false;

	v.Add (vCmd [0]);
	
	if (m_pImport) {
		delete m_pImport;
		m_pImport = NULL;
	}

	m_pImport = new CImport ();
	bResult = true;

	v.Add (FoxjetDatabase::ToString (bResult));
	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketDbEndUpdate (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	CControlDoc * pDoc = GetDocument ();
	COdbcDatabase & db = theApp.m_Database;
	bool bResult = false;

	v.Add (vCmd [0]);
	
	if (m_pImport) {
		m_pImport->Build (db);
		bResult = true;
	}

	v.Add (FoxjetDatabase::ToString (bResult));
	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

void CControlView::GetPanelIDs (ULONG lLineID, ULONG lCardID, CArray <UINT, UINT> & vPanelIDs)
{
	using namespace Foxjet3d;
	CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> vPanels;

	HeadConfigDlg::CPanel::GetPanels (theApp.m_Database, lLineID, vPanels);

	for (int nPanel = 0; nPanel < vPanels.GetSize (); nPanel++) {
		HeadConfigDlg::CPanel & p = vPanels [nPanel];

		for (int nCard = 0; nCard < p.m_vCards.GetSize (); nCard++) {
			HEADSTRUCT card = p.m_vCards [nCard];

			for (int nValve = 0; nValve < card.m_vValve.GetSize (); nValve++) {
				VALVESTRUCT & valve = card.m_vValve [nValve];

				if (valve.m_lCardID == lCardID) {
 					if (Find <UINT, UINT> (vPanelIDs, p.m_panel.m_lID) == -1)
						vPanelIDs.Add (p.m_panel.m_lID);
				}
			}
		}

		for (int nValve = 0; nValve < p.m_vValve.GetSize (); nValve++) {
			VALVESTRUCT & valve = p.m_vValve [nValve];

			if (valve.m_lCardID == lCardID) {
				if (Find <UINT, UINT> (vPanelIDs, p.m_panel.m_lID) == -1)
					vPanelIDs.Add (p.m_panel.m_lID);
			}
		}
	}
}

LRESULT CControlView::OnCheckHeadErrors (WPARAM wParam, LPARAM lParam)
{
	typedef CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> CHeadArray;

	CHeadArray * pvDisconnected = (CHeadArray *)lParam;
	ULONG lLineID = (ULONG)wParam;
	CControlDoc * pDoc = GetDocument ();
	CMapStringToPtr v;

	ASSERT (pDoc);
	ASSERT (pvDisconnected);
	pDoc->GetProductLines (v);

	for (POSITION pos = v.GetStartPosition(); pos; ) {
		CProdLine * pLine = NULL;
		CString strLine;

		v.GetNextAssoc (pos, strLine, (void *&)pLine);

		if (pLine) {
			if (pLine->m_LineRec.m_lID == lLineID) {
				return pLine->HasHeadErrors (* pvDisconnected) & ~HEADERROR_LOWINK;
			}
		}
	}

	return 0;
}

LRESULT CControlView::OnDoAms (WPARAM wParam, LPARAM lParam)
{
	CControlDoc * pDoc = GetDocument ();
	CMapStringToPtr v;

	ULONG lLineID = (ULONG)wParam;

	ASSERT (pDoc);
	pDoc->GetProductLines (v);

	for (POSITION pos = v.GetStartPosition(); pos; ) {
		CProdLine * pLine = NULL;
		CString strLine;

		v.GetNextAssoc (pos, strLine, (void *&)pLine);

		if (pLine) 
			if (pLine->m_LineRec.m_lID == lLineID) 
				pLine->DoAmsCycle ();
	}

	return 0;
}

LRESULT CControlView::OnIsWaiting (WPARAM wParam, LPARAM lParam)
{
	//DECLARETRACECOUNT (20);
	//MARKTRACECOUNT ();

	CControlDoc * pDoc = GetDocument ();
	CMapStringToPtr v;
	ULONG lLineID = (ULONG)wParam;
	bool bReply = lParam ? true : false;
	LRESULT lResult = 0;

	ASSERT (pDoc);
	pDoc->GetProductLines (v);

	for (POSITION pos = v.GetStartPosition(); pos; ) {
		CProdLine * pLine = NULL;
		CString strLine;

		v.GetNextAssoc (pos, strLine, (void *&)pLine);

		if (pLine) {
			if (pLine->m_LineRec.m_lID == lLineID) {
				if (pLine->IsWaiting ()) {
					lResult = 1;
					break;
				}
			}
		}
	}

	//MARKTRACECOUNT ();

	if (bReply) {
		if (lResult) {
			while (!PostMessage (WM_LINE_SET_WAITING, lLineID, false))
				::Sleep (0);
		}
	}

	//MARKTRACECOUNT ();
	//TRACECOUNTARRAYMAX ();

	return lResult;
}

LRESULT CControlView::OnSetWaiting (WPARAM wParam, LPARAM lParam)
{
	CControlDoc * pDoc = GetDocument ();
	CMapStringToPtr v;
	ULONG lLineID = (ULONG)wParam;

	ASSERT (pDoc);
	pDoc->GetProductLines (v);

	for (POSITION pos = v.GetStartPosition(); pos; ) {
		CProdLine * pLine = NULL;
		CString strLine;

		v.GetNextAssoc (pos, strLine, (void *&)pLine);

		if (pLine) 
			if (pLine->m_LineRec.m_lID == lLineID) 
				pLine->SetWaiting (lParam ? true : false);
	}

	return 0;
}

LRESULT CControlView::OnKeepAlive (WPARAM wParam, LPARAM lParam)
{
	return 1;
}

LRESULT CControlView::OnSocketPrinterSOP (WPARAM wParam, LPARAM lParam)
{
	DWORD dw = 0;
	CStringArray v;
	__int64 * pCount = (__int64 *)lParam;

	v.Add (BW_HOST_PACKET_PRINTER_SOP);
	v.Add (FoxjetDatabase::ToString ((long)wParam));
	v.Add (FoxjetDatabase::ToString (* pCount));

	if (m_pNextThread) {
		while (!m_pNextThread->PostThreadMessage (TM_NEXT_SEND_STATUS, wParam, lParam))
			::Sleep (0);
	}

	InterfaceNotify (ToString (v));
	::SendMessageTimeout (HWND_BROADCAST, WM_PRINTER_SOP, wParam, lParam, SMTO_NORMAL | SMTO_ABORTIFHUNG, 100, &dw);

	delete pCount;

	return 0;
}

LRESULT CControlView::OnSocketPrinterEOP (WPARAM wParam, LPARAM lParam)
{
	DWORD dw = 0;
	CStringArray v;
	__int64 * pCount = (__int64 *)lParam;
	v.Add (BW_HOST_PACKET_PRINTER_EOP);
	v.Add (FoxjetDatabase::ToString ((long)wParam));
	v.Add (FoxjetDatabase::ToString (* pCount));

//TRACEF (ToString (v));
	InterfaceNotify (ToString (v));
	::SendMessageTimeout (HWND_BROADCAST, WM_PRINTER_EOP, wParam, lParam, SMTO_NORMAL | SMTO_ABORTIFHUNG, 100, &dw);
	
	delete pCount;

	return 0;
}


LRESULT CControlView::OnSocketPrinterStateChange (WPARAM wParam, LPARAM lParam)
{
	DWORD dw = 0;
	CStringArray v;
	CString strState;

	if (m_pNextThread) {
		while (!m_pNextThread->PostThreadMessage (TM_NEXT_SEND_STATUS, wParam, lParam))
			::Sleep (0);
	}

	{
		v.Add (BW_HOST_PACKET_GET_CURRENT_STATE);
		v.Add (""); // line
		v.Add (FoxjetDatabase::ToString ((long)wParam));
		OnSocketGetCurrentState ((WPARAM)&v, (LPARAM)&strState);
		v.RemoveAll ();
	}

	v.Add (BW_HOST_PACKET_PRINTER_HEAD_STATE_CHANGE);
	v.Add (FoxjetDatabase::ToString ((long)wParam));
	v.Add (strState);

	InterfaceNotify (ToString (v));
	::SendMessageTimeout (HWND_BROADCAST, WM_PRINTER_HEAD_STATE_CHANGE, wParam, lParam,
		SMTO_NORMAL | SMTO_ABORTIFHUNG, 100, &dw);

	return 0;
}


void CControlView::InterfaceNotify (const CString & strSend)
{
	CString strCmd = strSend;
	bool bDisplay = false;

	for (int i = 0; i < m_vCommThreads.GetSize (); i++) {
		if (CComm * p = m_vCommThreads [i]) {
			if (p->m_type == HOSTINTERFACE) {
				LOCK (p->m_cs);

				BOOL bWrite = Write_Comport (p->m_hComm, strSend.GetLength (), (LPVOID)(LPCSTR)w2a (strSend));

				ASSERT (bWrite);
				bDisplay = true;
			}
		}
	}

#ifdef _DEBUG
	{
		for (POSITION pos = theApp.m_vErrSock.GetStartPosition (); pos != NULL; ) {
			CString strKey, strValue;
			CStringArray vSocket;

			theApp.m_vErrSock.GetNextAssoc (pos, strKey, strValue);
			TRACEF (strKey);
		}
	}
#endif //_DEBUG

	for (POSITION pos = theApp.m_vErrSock.GetStartPosition (); pos != NULL; ) {
		CString strKey, strValue;
		CStringArray vSocket;

		theApp.m_vErrSock.GetNextAssoc (pos, strKey, strValue);
		Tokenize (strKey, vSocket);

		if (vSocket.GetSize () >= 2) {
			CString strDebug, strAddr = vSocket [0];
			UINT nPort = _ttoi (vSocket [1]);
			CSocket s;
			int nFlag;

			//ASSERT (m_pServerThread);
			//strAddr = m_pServerThread->GetConnectedAddr ();

			VERIFY (s.Create (0, SOCK_DGRAM));

			nFlag = 1;
			VERIFY (s.SetSockOpt (SO_REUSEADDR, &nFlag, sizeof (int)));

			#ifdef _DEBUG
			strDebug.Format (_T ("%s:%d"), strAddr, nPort);
			//TRACEF (_T ("error socket: ") + strDebug);
			#endif //_DEBUG

			if (s.Connect (strAddr, nPort)) {
				int nLen = strCmd.GetLength ();

				bDisplay = true;
				DWORD dwSend = s.Send (w2a (strCmd), nLen);
				
				if (dwSend == SOCKET_ERROR)
					TRACEF (_T ("SOCKET_ERROR: ") + strDebug);
			}
			else
				TRACEF (_T ("Failed to connect: ") + strDebug);

			s.Close ();
		}
	}

	if (bDisplay)
		UpdateSerialData (_T (">> ") + strSend);
}

LRESULT CControlView::OnHeadConnectionStateChanged (WPARAM wParam, LPARAM lParam)
{
/* TODO: rem
	if (CString * pstrAddr = (CString *)wParam) {
		bool bConnected = lParam ? true : false;
		CControlDoc *pDoc = GetDocument();
		CStringArray vLines;

		TRACEF (CString (bConnected ? "connected: " : "disconnected: ") + * pstrAddr);

		pDoc->GetProductLines (vLines);

		for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
			if (CProdLine * pLine = pDoc->GetProductionLine (vLines [nLine])) {
				LOCK (pLine->m_csDisconnected);
				bool bFound = false;

				for (int i = 0; !bFound && (i < pLine->m_vHeads.GetSize ()); i++)
					if (!pLine->m_vHeads [i].m_strUID.CompareNoCase (* pstrAddr))
						bFound = true;

				if (bFound) {
					int nIndex = Find (pLine->m_vDisconnected, * pstrAddr, false);

					if (bConnected) {
						if (nIndex != -1)
							pLine->m_vDisconnected.RemoveAt (nIndex);
					}
					else {
						if (nIndex == -1)
							pLine->m_vDisconnected.Add (* pstrAddr);
					}
				}
			}
		}

		delete pstrAddr;
	}
	else { TRACEF ("CControlView::OnHeadConnectionStateChanged"); }
*/

	return 0;
}

double CControlView::GetZoom () const
{
	return m_pPreview ? m_pPreview->GetZoom () : 1.0;
}

bool CControlView::SetZoom (double dZoom)
{
	bool bResult = false;

	if (m_pPreview) {
		bResult = m_pPreview->SetZoom (dZoom);
		ViewRefreshpreview ();
	}

	return bResult;
}

LRESULT CControlView::OnXmlData(WPARAM wParam, LPARAM lParam)
{
	int nParsed = 0;

	if (CString * pstr = (CString *)wParam) {
		CString & str = * pstr;

		if (CProdLine * pLine = GetDocument ()->GetLine (0)) {
			using namespace FoxjetElements;

			Head::CElementPtrArray v;

			pLine->GetElements (v, USER);
			pLine->GetElements (v, BMP);
			pLine->GetElements (v, COUNT);

			{
				const CString strMaxCount = _T ("NB_OF_CONTAINER");
				int nBegin	= CServerThread::GetXmlTagIndex (str, strMaxCount);
				int nEnd	= CServerThread::GetXmlTagIndex (str, _T ("/") + strMaxCount);

				if (nBegin >= 0 && nEnd > 1 && nBegin < str.GetLength () && nEnd < str.GetLength ()) {
					CString strData = CServerThread::UnformatXML (Extract (str.Mid (nBegin, nEnd - nBegin + 1), _T (">"), _T ("<")));

					strData.Trim ();
					pLine->SetCountUntil (strData.GetLength () ? FoxjetCommon::strtoul64 (strData) : -1);
					nParsed++;
				}
			}

			for (int i = 0; i < v.GetSize (); i++) {
				CString strPrompt, strData, strKey = _T ("Software\\Foxjet\\Control\\") + pLine->GetName ();

				if (CUserElement * pUser = DYNAMIC_DOWNCAST (CUserElement, v [i])) {
					int nBegin	= CServerThread::GetXmlTagIndex (str, pUser->GetPrompt ());
					int nEnd	= CServerThread::GetXmlTagIndex (str, _T ("/") + pUser->GetPrompt ());

					if (nBegin >= 0 && nEnd > 1 && nBegin < str.GetLength () && nEnd < str.GetLength ()) {
						strData = CServerThread::UnformatXML (Extract (str.Mid (nBegin, nEnd - nBegin + 1), _T (">"), _T ("<")));
						nParsed++;
						pUser->SetDefaultData (strData.GetLength () ? strData : _T (" "));

						strPrompt = pUser->GetPrompt ();
						strData = pUser->GetDefaultData ();
					}
				}
				else if (CBitmapElement * pBmp = DYNAMIC_DOWNCAST (CBitmapElement, v [i])) {
					int nBegin	= CServerThread::GetXmlTagIndex (str, pBmp->GetUserPrompt ());
					int nEnd	= CServerThread::GetXmlTagIndex (str, _T ("/") + pBmp->GetUserPrompt ());

					if (nBegin >= 0 && nEnd > 1 && nBegin < str.GetLength () && nEnd < str.GetLength ()) {
						strPrompt = pBmp->GetUserPrompt ();
						strData = CServerThread::UnformatXML (Extract (str.Mid (nBegin, nEnd - nBegin + 1), _T (">"), _T ("<")));
						nParsed++;
						pBmp->SetFilePath (strData, pBmp->GetHead ());
						Foxjet3d::UserElementDlg::CUserElementDlg::ResetSize (pBmp, pBmp->GetHead ());
					}
				}
				else if (CCountElement * pCount = DYNAMIC_DOWNCAST (CCountElement, v [i])) {
					int nBegin	= CServerThread::GetXmlTagIndex (str, pCount->GetName ());
					int nEnd	= CServerThread::GetXmlTagIndex (str, _T ("/") + pCount->GetName ());

					if (nBegin >= 0 && nEnd > 1 && nBegin < str.GetLength () && nEnd < str.GetLength ()) {
						strPrompt = pCount->GetName ();
						strData = CServerThread::UnformatXML (Extract (str.Mid (nBegin, nEnd - nBegin + 1), _T (">"), _T ("<")));
						__int64 lCount = FoxjetCommon::strtoul64 (strData);

						nParsed++;
						pCount->SetCount (0);
						pCount->IncrementTo (lCount - 1);
					}
				}

				if (FoxjetUtils::CStartupDlg::IsUserDataEnabled (theApp.m_Database)) {
					if (strPrompt.GetLength ()) {
						const CString strFile = GetHomeDir () + _T ("\\XML\\") + strPrompt + _T (".txt");

						OnSaveXmlData ((WPARAM)new CString (strFile), (LPARAM)new CString (strData));
					}
				}
			}

			OnSaveXmlData (0, 0);
		}

		delete pstr;
	}

	return nParsed;
}

LRESULT CControlView::OnSaveXmlData(WPARAM wParam, LPARAM lParam)
{
	if (CString * pstrFile = (CString *)wParam) {
		if (CString * pstrData = (CString *)lParam) {
			const BYTE nUTF16 [] = { 0xFF, 0xFE };
			const TCHAR cNULL = 0;
			const int nLen = (pstrData->GetLength () * sizeof (TCHAR)) + ARRAYSIZE (nUTF16) + sizeof (cNULL);
			int n;

			BYTE * pData = new BYTE [nLen + 2];
			BYTE * p = pData;

			n = ARRAYSIZE (nUTF16);
			memcpy (p, nUTF16, n); 
			p += n;

			n = pstrData->GetLength () * sizeof (TCHAR);
			memcpy (p, (LPVOID)(LPCTSTR)(* pstrData), n); 
			p += n;

			n = sizeof (cNULL);
			memcpy (p, &cNULL, n); 
			p += n;

			VERIFY (FoxjetFile::WriteDirect (* pstrFile, pData, nLen));

			delete [] pData;
			delete pstrData;
		}

		delete pstrFile;
	}


	return 0;
}

//extern CDiagnosticCriticalSection csCountFile;

LRESULT CControlView::OnPostPhotocell (WPARAM wParam, LPARAM lParam)
{
	//DECLARETRACECOUNT (20);

	if (CPhotocellCount * p = (CPhotocellCount *)lParam) {
		static const CString strDir = FoxjetDatabase::GetHomeDir ();

		if (CProdLine * pLine = GetDocument ()->GetProductionLine (p->m_lLineID))
			pLine->m_countLast = p->m_count;
		//MARKTRACECOUNT ();

		//GlobalFuncs::WriteProfileInt (p->m_strLine, GlobalVars::strCount, p->m_lCount);

		//MARKTRACECOUNT ();

		CString str = p->ToString ();

		for (int i = 1; i <= 2; i++) {
			CString strFile;
			
			strFile.Format (_T ("%s\\[%d] Count%d.log"), strDir, p->m_lLineID, i);

			//MARKTRACECOUNT ();

			//LOCK (csCountFile);

			if (FoxjetFile::IsWriteDirect ()) {
				//UpdateSerialData (_T ("/write_direct: ") + str + _T (" ") + CString (_T (__FILE__)) + _T (" ") + strFile);
				FoxjetFile::WriteDirect (strFile, (LPBYTE)(LPVOID)w2a (str), str.GetLength ());
			}
			else {
				//UpdateSerialData (str + _T (" ") + CString (_T (__FILE__)) + _T (" ") + strFile);

				if (FILE * fp = _tfopen (strFile, _T ("w"))) {
					fwrite ((LPVOID)w2a (str), str.GetLength (), 1, fp);
					fflush (fp);
					fclose (fp);
					::Sleep (0);
					//TRACEF (str);

					//MARKTRACECOUNT ();

					/*
					DWORD dw = ::GetFileAttributes (strFile);
				
					//MARKTRACECOUNT ();

					if (!(dw & FILE_ATTRIBUTE_HIDDEN))
						::SetFileAttributes (strFile, dw | FILE_ATTRIBUTE_HIDDEN);
					*/

					// NOTE: file is read by GlobalFuncs::GetProfileInt
				}
				else
					TRACEF (_T ("Failed to open: ") + strFile + _T (" --> ") + FORMATMESSAGE (::GetLastError ()));
			}
		}

		//MARKTRACECOUNT ();

		delete p;
	}

	//TRACECOUNTARRAY ();

	return 0;
}

void CControlView::OnPhotocellSim1 ()
{
#if defined( _DEBUG ) && defined( __WINPRINTER__ )
	CButton * pState = (CButton *)GetDlgItem (CHK_PRINT1);

	ASSERT (pState);
	bool bEOP = pState->GetCheck () == 0 ? true : false;

	if (CControlDoc * pDoc = (CControlDoc *) GetDocument ()) {
		if (CProdLine * pLine = pDoc->GetSelectedLine()) {
			CPrintHeadList list;

			pLine->GetActiveHeads (list);

			for (POSITION pos = list.GetStartPosition(); pos; ) {
				DWORD dwKey;
				CHead * pHead = NULL;

				list.GetNextAssoc(pos, dwKey, (void *&)pHead);
				ASSERT (pHead);
				CHeadInfo info = CHead::GetInfo (pHead);
				int nUID = _ttoi (info.GetUID ());

				if (nUID == 1) {
					//TRACEF (_T ("OnPhotocellSim [") + info.GetUID () + _T ("]: ") + CString (bEOP ? _T ("EopEvent"): _T ("PhotoEvent")));
					POST_THREAD_MESSAGE (info, TM_SIM_PHOTOCELL, bEOP);
					return;
				}
			}
		}
	}
#endif
}

void CControlView::OnPhotocellSim2 ()
{
#if defined( _DEBUG ) && defined( __WINPRINTER__ )
	CButton * pState = (CButton *)GetDlgItem (CHK_PRINT2);

	ASSERT (pState);
	bool bEOP = pState->GetCheck () == 0 ? true : false;

	if (CControlDoc * pDoc = (CControlDoc *) GetDocument ()) {
		if (CProdLine * pLine = pDoc->GetSelectedLine()) {
			CPrintHeadList list;

			pLine->GetActiveHeads (list);

			for (POSITION pos = list.GetStartPosition(); pos; ) {
				DWORD dwKey;
				CHead * pHead = NULL;

				list.GetNextAssoc(pos, dwKey, (void *&)pHead);
				ASSERT (pHead);
				CHeadInfo info = CHead::GetInfo (pHead);
				int nUID = _ttoi (info.GetUID ());

				if (nUID == 2) {
					//TRACEF (_T ("OnPhotocellSim [") + info.GetUID () + _T ("]: ") + CString (bEOP ? _T ("EopEvent"): _T ("PhotoEvent")));
					POST_THREAD_MESSAGE (info, TM_SIM_PHOTOCELL, bEOP);
					return;
				}
			}
		}
	}
#endif
}

LRESULT CControlView::OnGenImageDebug (WPARAM wParam, LPARAM lParam)
{

	const CString strDir = FoxjetDatabase::GetHomeDir () + _T ("\\Debug");
	const CString strFile = strDir + _T ("\\GenImage.txt");

	if (::GetFileAttributes (strDir) == -1) {
		::SetCurrentDirectory (FoxjetDatabase::GetHomeDir ());

		if (!::CreateDirectory (_T ("Debug"), NULL)) {
			TRACEF (FormatMessage (GetLastError ()));
		}

		ASSERT (::GetFileAttributes (strDir) != -1);
	}

	if (wParam) {
		if (FILE * fp = _tfopen (strFile, _T ("w"))) {
			fclose (fp);
		}
	}

	if (CArray <CImageEng::CImageData *, CImageEng::CImageData *> * pv = (CArray <CImageEng::CImageData *, CImageEng::CImageData *> *)lParam) {
		if (FILE * fp = _tfopen (strFile, _T ("a"))) {
			if (pv->GetSize ()) {
				CImageEng::CImageData * p = pv->GetAt (pv->GetSize () - 1);
				CString str;
				
				str.Format (_T ("[%s] %s (%dx%d) %d px"),
					p->m_tm.Format (_T ("%c")),
					p->m_strData,
					p->m_rc.Width (),
					p->m_rc.Height (),
					p->m_lPixels);
				
				TRACEF (str);
				fwrite (w2a (str), str.GetLength (), 1, fp);
				fwrite ("\n", 1, 1, fp);
			}

			for (int i = 0; i < pv->GetSize () - 1; i++) {
				CImageEng::CImageData * p = pv->GetAt (i);
				CString str;
				
				str.Format (_T ("0x%p, %-4d, %-4d (%-4d, %-4d), %08d px, %s"),
					p->m_lBmpHandle,
					p->m_rc.left, p->m_rc.top, p->m_rc.Width (), p->m_rc.Height (),
					p->m_lPixels,
					p->m_strData);
				//TRACEF (str);
				fwrite (w2a (str), str.GetLength (), 1, fp);
				fwrite ("\n", 1, 1, fp);
			}

			if (pv->GetSize ()) {
				CString str = _T ("\n\n");
				fwrite (w2a (str), str.GetLength (), 1, fp);
			}

			fclose (fp);
		}

		{
			while (pv->GetSize ()) {
				if (CImageEng::CImageData * p = pv->GetAt (0))
					delete p;
				
				pv->RemoveAt (0);
			}
				
			delete pv;
		}
	}

	return 0;
}

void CControlView::ConnectDatabases (CArray <CHead::TM_SET_IO_ADDRESS_STRUCT *, CHead::TM_SET_IO_ADDRESS_STRUCT *> & v)
{
	for (int i = 0; i < v.GetSize (); i++) {
		CHead::TM_SET_IO_ADDRESS_STRUCT * pParam = v [i];
		CHeadInfo info = CHead::GetInfo (pParam->m_pHead);

		if (theApp.m_Database.IsSqlServer ()) {
			FoxjetDatabase::COdbcDatabase * pdb = NULL;
			CString str;

			str.Format (_T ("TM_GET_DB:%lu"), pParam->m_pHead->m_nThreadID);
		
			HANDLE hEvent = ::CreateEvent (NULL, TRUE, FALSE, str);
			CHeadInfo::POSTMESSAGESTRUCT * p = new CHeadInfo::POSTMESSAGESTRUCT (false, _T (__FILE__), __LINE__, hEvent);

			// if it's already signalled, something is wrong
			ASSERT (::WaitForSingleObject (hEvent, 0) != WAIT_OBJECT_0);

			while (!::PostThreadMessage (pParam->m_pHead->m_nThreadID, TM_GET_DB, (WPARAM)&pdb, (LPARAM)p)) 
				::Sleep (0);
			
			DWORD dwWait = ::WaitForSingleObject (hEvent, INFINITE);
			
			#ifdef _DEBUG
			{
				CString str;

				str.Format (_T ("SendMessage [%d]: %d (0x%p): "), pParam->m_pHead->m_nThreadID, TM_GET_DB, &pdb);

				if (dwWait == WAIT_TIMEOUT)		str += _T ("WAIT_TIMEOUT");
				if (dwWait == WAIT_ABANDONED)	str += _T ("WAIT_ABANDONED");
				if (dwWait == WAIT_OBJECT_0)	str += _T ("WAIT_OBJECT_0");

				if (dwWait != WAIT_OBJECT_0)
					TRACEF (str);
			}
			#endif
			
			::CloseHandle (hEvent);

			if (pdb) {
				CDbContext tmp (* pdb);

				if (!pdb->IsOpen ()) {
					if (FoxjetCommon::OpenDatabase (* pdb, theApp.GetDSN (), FoxjetCommon::GetDatabaseName())) {
						pdb->m_dwOwnerThreadID = info.GetThreadID ();
						//SEND_THREAD_MESSAGE (info, TM_SET_TIME_DB, &theApp.m_tmDatabase);
					}
				}
			}
		}
	}
}

void CControlView::OnExit ()
{
	((CMainFrame *)theApp.m_pMainWnd)->PostMessage (WM_CLOSE, 0, 0);
}

void CControlView::DoStart ()
{
	DBCONNECTION (conn);

	CControlDoc * pDoc = GetDocument();
	CString strLine = pDoc->GetSelectedLine()->GetName();
	CStringArray vLines;
	CHead::CHANGECOUNTSTRUCT count;

	if (!theApp.GetCoupledMode ()) 
		vLines.Add (strLine);
	else 
		pDoc->GetProductLines (vLines);

	int nStart = StartTask (strLine, NULL, false, _T (""), -1, 0, &count);

	if (TASK_START_SUCCEEDED (nStart)) {
		const auto mapCount = GetCounts();

		for (int i = 0; i < vLines.GetSize (); i++) {
			if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
				__int64 lCount = 0;

				if (count.m_lCount < 0) {
					auto find = mapCount.find(pLine->GetID());

					if (find != mapCount.end())
						lCount = find->second;
				}

				pLine->SetCounts(lCount);

				if (theApp.IsPrintReportLoggingEnabled()) {
					auto report = pLine->GenerateReportRecord();

					report.m_strTaskName = pLine->GetCurrentTask();
					report.m_strCounts.Format (_T ("%lu"), lCount);
					pLine->AddPrinterReportRecord(FoxjetDatabase::REPORT_TASK_START, report);
				}
			}
		}

		OnViewRefreshpreview ();
	}
}

void CControlView::OnStart ()
{
	DBCONNECTION (conn);

	if (!theApp.m_dbstart.m_strDSN.GetLength ()) 
		DoStart ();
	else
		CControlView::OnDbStart ();

	theApp.UpdateUI ();
}

std::map<ULONG, __int64> CControlView::GetCounts() 
{
	std::map<ULONG, __int64> result;

	if (CControlDoc* pDoc = GetDocument()) {
		CStringArray vLines;

		pDoc->GetProductLines(vLines);

		for (int i = 0; i < vLines.GetSize(); i++) {
			if (CProdLine* pLine = pDoc->GetProductionLine(vLines[i])) {
				__int64 lTmp, lMinCount = 0, lMaxCount = 0, lMasterCount = 0;

				pLine->GetCounts(NULL, lTmp, lMinCount, lMaxCount, lMasterCount);
				result[pLine->GetID()] = lMasterCount;
			}
		}
	}

	return result;
}

void CControlView::OnDbStart ()
{
	DBCONNECTION (conn);

#ifdef __WINPRINTER__
	CControlDoc *pDoc = GetDocument();

	CDbStartTaskDlg dlg (pDoc->GetSelectedLine ()->GetName (), this);

	if (dlg.DoModal () == IDOK) {
		CString strLine = pDoc->GetSelectedLine ()->GetName ();
		CString strTask = dlg.m_strTaskValue;

		TRACEF (dlg.m_strKeyField + _T (" --> ") + dlg.m_strKeyValue);
		TRACEF (dlg.m_strTaskField + _T (" --> ") + dlg.m_strTaskValue);

		{
			CString strKey = _T ("Software\\FoxJet\\Control\\") + strLine + _T ("\\DbTaskStart");

			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("DSN"),			dlg.m_strDSN); 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("KeyField"),		dlg.m_strKeyField); 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("Table"),		dlg.m_strTable); 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("TaskField"),	dlg.m_strTaskField); 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("KeyValue"),		dlg.m_strKeyValue); 
			FoxjetDatabase::WriteProfileInt	   (HKEY_CURRENT_USER, strKey, _T ("SQLType"),		dlg.m_nSQLType); 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("TaskValue"),	dlg.m_strTaskValue); 
		}

		CStringArray vLines;

		if (!theApp.GetCoupledMode ()) 
			vLines.Add (strLine);
		else 
			pDoc->GetProductLines (vLines);

		int nStart = StartTask (strLine, strTask, false, dlg.m_strKeyValue, 0, dlg.m_nOffset);
		
		if (TASK_START_SUCCEEDED (nStart)) {
			CString str = dlg.m_strTaskValue + _T (" [") + dlg.m_strKeyValue + _T ("]");
			const auto mapCount = GetCounts();

			if (theApp.IsContinuousCount())
				dlg.m_bResetCounts = FALSE;

			for (int i = 0; i < vLines.GetSize (); i++) {
				if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
					__int64 lCount = 0;

					if (!dlg.m_bResetCounts) {
						auto find = mapCount.find(pLine->GetID());

						if (find != mapCount.end())
							lCount = find->second;
					}

					pLine->SetCounts(lCount);

					if (theApp.IsPrintReportLoggingEnabled()) {
						auto report = pLine->GenerateReportRecord();

						report.m_strTaskName = strTask;
						report.m_strCounts.Format(_T("%lu"), lCount);

						if (dlg.m_strKeyValue.GetLength ())	
							report.m_strTaskName.Format(_T("%s: %s"), strTask, dlg.m_strKeyValue);
						else
							report.m_strTaskName = strTask;

						pLine->AddPrinterReportRecord(FoxjetDatabase::REPORT_KEYFIELDSTART, report);
					}
				}
			}
		}
		else {
			CString str, strFmt;
			
			strFmt.Format (
				_T ("%s: %%s\n")
				_T ("%s: %%s\n")
				_T ("%s: %%s\n"), 
				LoadString (IDS_TASKSTARTFAILED), 
				LoadString (IDS_KEYFIELD), 
				LoadString (IDS_KEYVALUE));

			str.Format (strFmt, 
				strTask, 
				dlg.m_strKeyField, 
				dlg.m_strKeyValue);

			StopTask (strLine, false);
			MsgBox (* this, str, NULL, MB_OK | MB_ICONHAND);
		}
	}
#endif //__WINPRINTER__
}


void CControlView::OnIdle ()
{
	DBCONNECTION (conn);

	CString str;

	m_btnIdle.GetWindowText (str);

	if (str == LoadString (IDS_TASK_IDLE)) 
		IdleTask (NULL);
	else 
		ResumeTask (NULL);
}

void CControlView::OnStop ()
{
	DBCONNECTION (conn);

	StopTask();
}

void CControlView::OnEdit ()
{
	((CMainFrame *)theApp.m_pMainWnd)->OnOperEdit ();
}

void CControlView::OnLogin ()
{
	DBCONNECTION (conn);

	CString str, strLogin (LoadString (IDS_LOGIN));

	m_btnLogin.GetWindowText (str);

	if (str == strLogin) 
		((CMainFrame *)theApp.m_pMainWnd)->OnLogin ();
	else 
		((CMainFrame *)theApp.m_pMainWnd)->OnLogout ();

	OnUpdateLogin ();
}

void CControlView::OnUpdateLogin ()
{
	if (theApp.m_strUsername.GetLength ()) {
		m_btnLogin.SetBitmap (m_bmpLogout);
		m_btnLogin.SetWindowText (theApp.m_strUsername + _T ("\n") + LoadString (IDS_LOGOUT));
	}
	else {
		m_btnLogin.SetBitmap (m_bmpLogin);
		m_btnLogin.SetWindowText (LoadString (IDS_LOGIN));
	}

	theApp.UpdateUI ();
}

void CALLBACK CControlView::InkCodeCallback (ULONG lHeadID)
{
	CInkSerialNumberDlg dlg (theApp.GetActiveView (), lHeadID);

	if (dlg.DoModal () == IDOK) {
		CControlView * pView = (CControlView *)theApp.GetActiveView ();
		int nLevel = INK_CODE_WARNGING_LEVEL_0;

		for (POSITION pos = pView->m_mapInkSerialNumber.GetStartPosition (); pos; ) {
			CInkSerialNumberDlg * pdlg = NULL;
			ULONG lHeadID = -1;

			pView->m_mapInkSerialNumber.GetNextAssoc (pos, lHeadID, pdlg);

			if (pdlg) {
				nLevel = pdlg->SendMessage (WM_GET_MAX_INK_WARNING);
				TRACER (CInkSerialNumberDlg::Trace (nLevel));
				pdlg->PostMessage (WM_INK_CODE_SET_STATE, nLevel);
			}
		}
	}
}

void CALLBACK CControlView::HeadConfigCallback (ULONG lHeadID, ULONG lOffset)
{
	#ifdef __WINPRINTER__

	if (USB::CUsbMphcCtrl * pDriver = theApp.GetUSBDriver ()) {
		CStringArray vLines;

		if (CControlDoc * pDoc = (CControlDoc *) theApp.GetActiveDocument ()) {
			pDoc->GetProductLines (vLines);
		
			for (int i = 0; i < vLines.GetSize (); i++) {
				if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
					if (CLocalHead * pHead = (CLocalHead *)pLine->GetHeadByID (lHeadID)) {
						CHeadInfo info = CHead::GetInfo (pHead);
						WORD wPrintDly = pHead->CalcPrintDelay (lOffset);
						
						pDriver->SetPrintDelay ((UINT)pHead->GetDeviceID (), wPrintDly);
						
						{ 
							CString str; 
							str.Format (_T ("%s(%d): %s: lHeadID: %d, lOffset: %d, wPrintDly: %d\n"), 
								_T (__FILE__), __LINE__, 
								info.GetFriendlyName (), lHeadID, lOffset, wPrintDly); 
							TRACER (str); 
						}

						break;
					}
				}
			}
		}
	}

	#endif //__WINPRINTER__
}

void CControlView::OnLimitedConfig ()
{
	DBCONNECTION (conn);

	if (CControlDoc * pDoc = GetDocument ()) {
		if (CProdLine * pLine = pDoc->GetSelectedLine ()) {
			Foxjet3d::CHeadLimitedConfigDlg dlg (theApp.m_Database, ListGlobals::defElements.GetUnits (), pLine->GetID (), this);

			dlg.m_lpfct = HeadConfigCallback;
			dlg.m_lpfctInkCode = InkCodeCallback;

			dlg.DoModal ();
		}
	}
}

void CControlView::OnInfo ()
{
	m_bShowInfo = !m_bShowInfo;

	if (m_bShowInfo) {
		if (!m_pdlgInfo)
			m_pdlgInfo = new CInfoDlg (this);
	
		m_btnInfo.SetWindowText (LoadString (IDS_PREVIEW));
		m_btnInfo.LoadBitmap (IDB_PREVIEW);
		m_pdlgInfo->ShowWindow (SW_SHOW);
		m_vLineDebugInfo.SetAt (-1, CString ());

		if (CWnd * pTab = GetDlgItem (TAB_LINE)) {
			CRect rc;

			pTab->GetWindowRect (rc);
			m_pdlgInfo->SetWindowPos (NULL, rc.left, rc.top, rc.Width (), rc.Height (), SWP_NOZORDER);
		}
	}
	else {
		m_btnInfo.SetWindowText (LoadString (IDS_INFO));
		m_btnInfo.LoadBitmap (IDB_INFO);

		if (m_pdlgInfo) {
			m_pdlgInfo->ShowWindow (SW_HIDE);
			//m_pdlgInfo->EndDialog (IDOK);
			//delete m_pdlgInfo;
			//m_pdlgInfo = NULL;
		}
	}

	for (POSITION pos = m_mapInkSerialNumber.GetStartPosition (); pos; ) {
		CInkSerialNumberDlg * pdlg = NULL;
		ULONG lHeadID = -1;

		m_mapInkSerialNumber.GetNextAssoc (pos, lHeadID, pdlg);
	
		if (pdlg)
			pdlg->BringWindowToTop ();
	}
}

void CControlView::OnCounts ()
{
	ChangeCounts ();
}

void CControlView::OnUserData ()
{
	ChangeUserElementData ();
}

void CControlView::OnConfig ()
{
	CMenu menu, menuSystem, menuView, menuSecurity, menuHelp;
	CRect rc;

	m_btnConfig.GetWindowRect (rc);
	menu.CreatePopupMenu ();
	menuSystem.CreatePopupMenu ();
	menuView.CreatePopupMenu ();
	menuSecurity.CreatePopupMenu ();
	menuHelp.CreatePopupMenu ();

	UINT nMenu [] = 
	{
		IDM_OPER_TEST,
		ID_CONFIGURE_DATABASE,				
		IDM_CFG_PRINTHEAD,					
		IDM_CFG_SYS_LINE,					
		ID_CONFIGURE_SERIALDATAFORMAT,		
		IDM_SYSTEM,
		IDM_VIEW,
		IDM_SECURITY,
		IDM_IMPORT,
		IDM_EXPORT,
		IDM_HELP,
	};
	UINT nSystemMenu [] = 
	{
		//IDM_CFG_BARCODE_SETTINGS,			
		//IDM_CFG_DATE_TIME_CODES,			
		//IDM_CFG_SHIFTCODES,					
		IDM_CFG_DYNAMICDATA,				
		IDM_CFG_GENERAL,					
		ID_CONFIGURE_SYSTEM_OUTPUTTABLE,	
		ID_CONFIGURE_SYSTEM_STROBE,			
	};
	/*
	UINT nViewMenu [] = 
	{
        IDM_VIEW_PRINT_REPORT,
        IDM_VIEW_SCAN_REPORT,
	};
	*/
	UINT nSecurityMenu [] = 
	{
        IDM_CFG_USER,
        IDM_CFG_GROUP_OPTIONS,
	};
	UINT nHelpMenu [] = 
	{
        ID_APP_ABOUT,
        ID_HELP_TRANSLATE,
	};
	struct
	{
		CMenu * m_pMenu;
		UINT * m_nID;
		int m_nItems;
	}
	static const map [] = 
	{
		{ &menuSystem,		nSystemMenu,	ARRAYSIZE (nSystemMenu)		},
		//{ &menuView,		nViewMenu,		ARRAYSIZE (nViewMenu)		},
		{ &menuSecurity,	nSecurityMenu,	ARRAYSIZE (nSecurityMenu)	},
		{ &menuHelp,		nHelpMenu,		ARRAYSIZE (nHelpMenu)		},
	};

	for (int j = 0; j < ARRAYSIZE (map); j++) {
		for (int i = 0; i < map [j].m_nItems; i++) {
			MENUITEMINFO info;

			memset (&info, 0, sizeof (info));

			info.cbSize		= sizeof (info);
			info.fMask		= MIIM_TYPE | MIIM_ID | MIIM_DATA | MIIM_SUBMENU;
			info.fType		= MFT_OWNERDRAW;
			info.wID		= map [j].m_nID [i];
		
			::InsertMenuItem (* map [j].m_pMenu, i, TRUE, &info);
		}
	}

	for (int i = 0; i < ARRAYSIZE (nMenu); i++) {
		MENUITEMINFO info;

		memset (&info, 0, sizeof (info));

		info.cbSize		= sizeof (info);
		info.fMask		= MIIM_TYPE | MIIM_ID | MIIM_DATA | MIIM_SUBMENU;
		info.fType		= MFT_OWNERDRAW;
		info.wID		= nMenu [i];
	
		switch (info.wID) {
		case IDM_SYSTEM:
			info.hSubMenu = menuSystem;
			break;
		case IDM_VIEW:
			info.hSubMenu = menuView;
			break;
		case IDM_SECURITY:
			info.hSubMenu = menuSecurity;
			break;
		case IDM_HELP:
			info.hSubMenu = menuHelp;
			break;
		}

		::InsertMenuItem (menu, i, TRUE, &info);
	}

	{
		const bool bValve = ISVALVE ();

		#ifdef __WINPRINTER__
		VERIFY (menu.DeleteMenu (IDM_CFG_DYNAMICDATA, MF_BYCOMMAND));
		#endif

		if (!theApp.Is0835 ())
			VERIFY (menu.DeleteMenu (ID_CONFIGURE_SERIALDATAFORMAT, MF_BYCOMMAND));

		#ifdef __IPPRINTER__
		VERIFY (menu.DeleteMenu (IDM_OPER_DBSTART, MF_BYCOMMAND));
		VERIFY (menu.DeleteMenu (ID_CONFIGURE_DATABASE, MF_BYCOMMAND));
		VERIFY (menu.DeleteMenu (ID_CONFIGURE_SYSTEM_OUTPUTTABLE, MF_BYCOMMAND));
		#endif
		
		if (bValve) {
			/* VERIFY */ (menu.DeleteMenu (IDM_CFG_BARCODE_SETTINGS, MF_BYCOMMAND));
		}
	}

	{
		if (::GetFileAttributes (FoxjetDatabase::GetHomeDir () + _T ("\\") +  _T ("Translator.exe")) == -1)
			menu.RemoveMenu (ID_HELP_TRANSLATE, MF_BYCOMMAND);

		for (int i = 0; i < ARRAYSIZE (::mapMenu); i++) {
			UINT nID = ::mapMenu [i].m_nCmdID;
			int nType = ::mapMenu [i].m_nType;

			if (nType == MENUITEM_NOSECURITY || nType == MENUITEM_SUBMENU) {
				// intentionally empty
			}
			else if (nType == MENUITEM_STD) {
				if (!theApp.LoginPermits (nID)) {
					TRACEF (_T ("remove: ") + LoadString (::mapMenu [i].m_nResID));
					menu.RemoveMenu (nID, MF_BYCOMMAND);
				}
			}
			else if (nType > 0) {
				if (!theApp.LoginPermits (nType)) {
					menu.RemoveMenu (nID, MF_BYCOMMAND);
					TRACEF (_T ("remove: ") + LoadString (::mapMenu [i].m_nResID));
				}
			}
		}

		for (int i = menu.GetMenuItemCount () - 1; i >= 0; i--) 
			if (CMenu * p = menu.GetSubMenu (i)) 
				if (!p->GetMenuItemCount ()) 
					menu.RemoveMenu (i, MF_BYPOSITION);
	}

	menu.TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON, rc.left, rc.bottom, this);
}

void CControlView::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpmis) 
{	
	//TRACEF (_T ("CControlView::OnMeasureItem: ") + FoxjetDatabase::ToString ((int)lpmis->CtlID));

	if (lpmis->CtlType == ODT_MENU) {
		lpmis->itemWidth = 200;
		lpmis->itemHeight = 50;
	}

	CFormView::OnMeasureItem (nIDCtl, lpmis);
}


BOOL CControlView::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	ULONG lCmdID = LOWORD (wParam);
	
	for (int i = 0; i < ARRAYSIZE (::mapMenu); i++) {
		if (lCmdID == ::mapMenu [i].m_nCmdID) {
			((CMainFrame *)theApp.m_pMainWnd)->PostMessage (WM_COMMAND, wParam, lParam);
			break;
		}
	}

	return CFormView::OnCommand(wParam, lParam);
}

void CControlView::OnBox ()
{
	CControlDoc * pDoc = GetDocument ();
	CHead * pSelectedHead	= NULL;
	int nSelectedPanel		= -1;
	ULONG lTaskID			= -1;
	CString strPanel;

	if (CProdLine * pLine = pDoc->GetSelectedLine ()) {
		CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;
		CPrintHeadList list;

		vPanels.Append (pLine->m_vPanels);
		pLine->GetActiveHeads (list);

		for (int i = vPanels.GetSize () - 1; i >= 0; i--) {
			PANELSTRUCT panel = vPanels [i];
			CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

			GetPanelHeads (theApp.m_Database, vPanels [i].m_lID, vHeads);

			if (!vHeads.GetSize ()) 
				vPanels.RemoveAt (i);
		}

		if (int nPanels = vPanels.GetSize ()) {
			int nIndex = ++pLine->m_nPreviewPanel % nPanels;
			strPanel = vPanels [nIndex].m_strName; 
		}

		for (int i = vPanels.GetSize () - 1; i >= 0; i--) {
			PANELSTRUCT panel = vPanels [i];
			CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

			GetPanelHeads (theApp.m_Database, panel.m_lID, vHeads);

			if (panel.m_strName == strPanel) {
				if (vHeads.GetSize ()) 
					pSelectedHead = pLine->GetHeadByUID (vHeads [0].m_strUID); 

				nSelectedPanel	= panel.m_nNumber;
				lTaskID			= pLine->GetTaskID ();
				break;
			}
		}

		//for (POSITION pos = list.GetStartPosition(); pos; ) {
		//	DWORD dwKey;
		//	CHead * pHead = NULL;

		//	list.GetNextAssoc (pos, dwKey, (void *&)pHead);
		//	ASSERT (pHead);
		//	CHeadInfo info = CHead::GetInfo (pHead);
		//	
		//	if (info.GetPanelName () == strPanel) {
		//		pSelectedHead	= pHead;
		//		nSelectedPanel	= info.GetBoxPanel ();
		//		lTaskID			= pLine->GetTaskID ();
		//		break;
		//	}
		//}
	}

	m_btnBox.SetWindowText (strPanel);
	m_pPreview->SetPreviewPanel (pSelectedHead, lTaskID, nSelectedPanel);
	OnViewRefreshpreview ();
}

void CControlView::OnZoomFit ()
{
	m_pPreview->SetZoom (m_pPreview->CalcZoomFit (), false);
	m_pPreview->ScrollToPosition (CPoint (0, 0));
	m_pPreview->m_bZoomToFit = true;
	OnViewRefreshpreview ();
}

void CControlView::OnZoomIn ()
{
	m_pPreview->SetZoom (m_pPreview->GetZoom () + CImageView::m_dZoomIncrement);
	OnViewRefreshpreview ();
}

void CControlView::OnZoomOut ()
{
	m_pPreview->SetZoom (m_pPreview->GetZoom () - CImageView::m_dZoomIncrement);
	OnViewRefreshpreview ();
}

void CControlView::OnReports ()
{
	CMenu menu;

	UINT nViewMenu [] = 
	{
        IDM_VIEW_PRINT_REPORT,
        IDM_VIEW_SCAN_REPORT,
	};

	menu.CreatePopupMenu ();

	for (int i = 0; i < ARRAYSIZE (nViewMenu); i++) {
		if (theApp.LoginPermits (nViewMenu [i])) {
			MENUITEMINFO info;

			memset (&info, 0, sizeof (info));

			info.cbSize		= sizeof (info);
			info.fMask		= MIIM_TYPE | MIIM_ID | MIIM_DATA;
			info.fType		= MFT_OWNERDRAW;
			info.wID		= nViewMenu [i];

			::InsertMenuItem (menu, 0, TRUE, &info);
		}
	}

	if (menu.GetMenuItemCount ()) {
		CRect rc;

		m_btnReport.GetWindowRect (rc);
		menu.TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON, rc.left, rc.bottom, this);
	}
}

HBRUSH CControlView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	if (nCtlColor == CTLCOLOR_DLG) 
		return m_hbrDlg;

	return CFormView::OnCtlColor(pDC, pWnd, nCtlColor);
}

LRESULT CControlView::OnDiagnosticData (WPARAM wParam, LPARAM lParam)
{
	if (CString * p = (CString *)wParam) {
		UpdateSerialData (* p);
		delete p;
	}

	return 0;
}

LRESULT CControlView::OnPingStatus (WPARAM wParam, LPARAM lParam)
{
	bool bNetworkConnected = wParam ? true : false;
	bool bStateChange = bNetworkConnected != theApp.IsNetworkConnected ();

	theApp.SetNetworkConnected (bNetworkConnected);

	if (bStateChange)
		m_tmPing = CTime::GetCurrentTime ();

	if (m_pdlgInfo) {
		CString str;
		
		str.Format (_T ("%s: %s [%s] %s"),
			LoadString (IDS_PING), 
			CString (Ping::szPing),
			CTime::GetCurrentTime ().Format (_T ("%c")),
			bNetworkConnected ? _T ("") : _T ("[") + LoadString (IDS_DISCONNECTED) + _T ("]"));
		UpdateSerialData (str, false);

		//if (bStateChange)
			if (CWnd * p = m_pdlgInfo->GetDlgItem (BMP_NETWORK))
				REPAINT (p);
	}

	return 0;
}

LRESULT CControlView::OnSocketConnectDb (WPARAM wParam, LPARAM lParam)
{
	CDbConnection conn (_T (__FILE__), __LINE__);

	if (!conn.IsOpen ()) 
		return 0;

	return 1;
}

LRESULT CControlView::OnSocketSend (WPARAM wParam, LPARAM lParam)
{
	if (CString * p = (CString *)wParam) {
		m_pServerThread->Send (* p);
		delete p;
	}

	return 0;
}

LRESULT CControlView::OnSocketSetSerial (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	const CString strLine = CServerThread::GetLineParam (vCmd);
	CControlDoc * pDoc = GetDocument ();
	int nResult = 0;

	if (vCmd.GetSize () >= 3) {
		if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
			CString str = vCmd [2];
			int nFormattedLen = str.GetLength () + 1;
			char * psz = new char [nFormattedLen];

			memset (psz, 0, nFormattedLen);
			nResult = FoxjetDatabase::Unformat (str, (LPBYTE)psz);
			pLine->UpdateSerialData (a2w (psz), nResult);

			delete [] psz;
		}
	}

	v.Add (BW_HOST_PACKET_SETSERIAL);
	v.Add (strLine);
	v.Add (ToString (nResult));

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}

LRESULT CControlView::OnSocketGetSerial (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	const CString strLine = CServerThread::GetLineParam (vCmd);
	CControlDoc * pDoc = GetDocument ();
	int nResult = 0;


	v.Add (BW_HOST_PACKET_SETSERIAL);
	v.Add (strLine);

	if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
		if (tagSerialData * pSerial = pLine->GetSerialData ()) {
			v.Add (FoxjetDatabase::Format (pSerial->Data, pSerial->m_Len));
		}
	}

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}


LRESULT CControlView::OnSocketGetStrobe (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray v;
	CControlDoc * pDoc = GetDocument ();

	v.Add (BW_HOST_PACKET_GET_STROBE);
	v.Add (theApp.GetStrobeState ());

	strResult = FoxjetDatabase::ToString (v);

	return 0;
}


void CControlView::OnDbOpened ()
{
	for (int i = 0; i < m_vLocalDbThreads.GetSize (); i++) {
		UINT nThreadID = m_vLocalDbThreads [i]->m_nThreadID;
		
		while (!::PostThreadMessage (nThreadID, TM_LOCAL_APP_DB_OPEN, 0, 0))
			::Sleep (0);
	}
}

void CControlView::OnDbClosed ()
{
	for (int i = 0; i < m_vLocalDbThreads.GetSize (); i++) {
		UINT nThreadID = m_vLocalDbThreads [i]->m_nThreadID;
		
		while (!::PostThreadMessage (nThreadID, TM_LOCAL_APP_DB_CLOSE, 0, 0))
			::Sleep (0);
	}
}

LRESULT CControlView::OnSql (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	if (CString * p = (CString *)wParam) {
		CDbConnection conn (_T (__FILE__), __LINE__); 
		CString strError;

		if (!conn.IsOpen ()) 
			return 0;
		
		try
		{
			TRACEF (* p);
			int nUpdated = theApp.m_Database.ExecuteSQL (* p);
			VERIFY (nUpdated >= 1);
			return 1;
		}
		catch (CDBException * e)		{ strError = FoxjetDatabase::FormatException (e, _T (__FILE__), __LINE__); HANDLEEXCEPTION_TRACEONLY (e); }
		catch (CMemoryException * e)	{ strError = FoxjetDatabase::FormatException (e, _T (__FILE__), __LINE__); HANDLEEXCEPTION_TRACEONLY (e); }

		if (CString * pstrError = (CString *)lParam) 
			* pstrError = strError;
	}

	return 0;
}

LRESULT CControlView::OnSetNEXTBcParams (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	if (NEXT::BCPARAMS * p = (NEXT::BCPARAMS *)wParam) {
		if (FoxjetIpElements::CBarcodeElement::SetBarcodeSettings (theApp.m_Database, p->m_lLineID, p->m_type, p->m_str)) {
			if (LPFJSYSTEM pSystem = FoxjetIpElements::GetSystem (p->m_lLineID)) 
				fj_SystemFromString (pSystem, w2a (p->m_str));

			return 1;
		}
		else {
			if (CString * pstrError = (CString *)lParam) 
				* pstrError = _T ("FoxjetIpElements::CBarcodeElement::SetBarcodeSettings failed");
		}
	}

	return 0;
}

LRESULT CControlView::OnGetNEXTBcParams (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	if (NEXT::BCPARAMS * pParams = (NEXT::BCPARAMS *)wParam) {
		if (FJSYSBARCODE * pbcArray = (FJSYSBARCODE * )lParam) {
			CString str = FoxjetIpElements::CBarcodeElement::GetBarcodeSettings (theApp.m_Database, pParams->m_lLineID, pParams->m_type);
			
			if (LPFJSYSTEM pSystem = FoxjetIpElements::GetSystem (pParams->m_lLineID)) {
				if (fj_SystemFromString (pSystem, w2a (str))) {
					memcpy (pbcArray, pSystem->bcArray, sizeof (pSystem->bcArray));
					return 1;
				}
			}
		}
	}

	return 0;
}

LRESULT CControlView::OnSetNEXTDmParams (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	if (NEXT::BCPARAMS * p = (NEXT::BCPARAMS *)wParam) {
		if (FoxjetIpElements::CDataMatrixElement::SetBarcodeSettings (theApp.m_Database, p->m_lLineID, p->m_type, p->m_str)) {
			if (LPFJSYSTEM pSystem = FoxjetIpElements::GetSystem (p->m_lLineID)) 
				fj_SystemFromString (pSystem, w2a (p->m_str));
			
			return 1;
		}
		else {
			if (CString * pstrError = (CString *)lParam) 
				* pstrError = _T ("FoxjetIpElements::CDataMatrixElement::SetBarcodeSettings failed");
		}
	}

	return 0;
}

LRESULT CControlView::OnGetNEXTDmParams (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	if (NEXT::BCPARAMS * pParams = (NEXT::BCPARAMS *)wParam) {
		if (FJSYSDATAMATRIX * pbcDataMatrixArray = (FJSYSDATAMATRIX * )lParam) {
			CString str = FoxjetIpElements::CDataMatrixElement::GetBarcodeSettings (theApp.m_Database, pParams->m_lLineID, pParams->m_type);
			
			if (LPFJSYSTEM pSystem = FoxjetIpElements::GetSystem (pParams->m_lLineID)) {
				if (fj_SystemFromString (pSystem, w2a (str))) {
					memcpy (pbcDataMatrixArray, pSystem->bcDataMatrixArray, sizeof (pSystem->bcDataMatrixArray));
					return 1;
				}
			}
		}
	}

	return 0;
}

LRESULT CControlView::OnGetTaskNames (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	ULONG lLineID = (ULONG)wParam;

	if (std::vector <std::wstring> * pv = (std::vector <std::wstring> *)lParam) {
		try
		{
			CString strSQL;
			COdbcRecordset rst (theApp.m_Database);

			strSQL.Format (_T ("SELECT [%s] FROM [%s] ORDER BY [%s] ASC;"), 
				Tasks::m_lpszName, Tasks::m_lpszTable, Tasks::m_lpszName);
			TRACEF (strSQL);

			if (rst.Open (strSQL)) {
				while (!rst.IsEOF ()) {
					pv->push_back (std::wstring ((CString)rst.GetFieldValue ((short)0)));
					rst.MoveNext ();
				}
			}

			return 1;
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }
	}

	return 0;
}

LRESULT CControlView::OnAddTask (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	if (TASKSTRUCT * pTask = (TASKSTRUCT *)wParam) {
		LINESTRUCT line;

		if (GetLineRecord (theApp.m_Database, pTask->m_lLineID, line)) {
			ULONG lID = GetTaskID (theApp.m_Database, pTask->m_strName, line.m_strName, ::GetPrinterID (theApp.m_Database));

			if (lID != NOTFOUND) 
				DeleteTaskRecord (theApp.m_Database, lID);

			if (AddTaskRecord (theApp.m_Database, * pTask))
				return pTask->m_lID;
		}
	}

	return 0;
}

LRESULT CControlView::OnDeleteTask (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	ULONG lResult = 0;

	if (ULONG lLineID = (ULONG)wParam) {
		LINESTRUCT line;
		CArray <ULONG> v;

		GetLineRecord (theApp.m_Database, lLineID, line);

		if (CString * pstr = (CString *)lParam) 
			v.Add (GetTaskID (theApp.m_Database, * pstr, line.m_strName, GetPrinterID (theApp.m_Database)));
		else {
			CArray <TASKSTRUCT *, TASKSTRUCT *> vTasks;

			GetTaskRecords (theApp.m_Database, lLineID, vTasks);

			for (int i = 0; i < vTasks.GetSize (); i++)
				v.Add (vTasks [i]->m_lID);
		}

		for (int i = 0; i < v.GetSize (); i++) 
			lResult += DeleteTaskRecord (theApp.m_Database, v [i]);
	}

	return lResult;
}

LRESULT CControlView::OnGetNextStrobe (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	if (CArray <FoxjetCommon::CStrobeItem, FoxjetCommon::CStrobeItem &> * pv = (CArray <FoxjetCommon::CStrobeItem, FoxjetCommon::CStrobeItem &> *)wParam) {
		FoxjetCommon::CStrobeDlg::Load (theApp.m_Database, * pv);
		return 1;
	}

	return 0;
}

LRESULT CControlView::OnSetNextStrobe (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	if (CArray <FoxjetCommon::CStrobeItem, FoxjetCommon::CStrobeItem &> * pv = (CArray <FoxjetCommon::CStrobeItem, FoxjetCommon::CStrobeItem &> *)wParam) {
		FoxjetCommon::CStrobeDlg::Save (theApp.m_Database, * pv);
		return 1;
	}

	return 0;
}


LRESULT CControlView::OnSetNEXTSystemParams (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	LRESULT lResult = 0;

	if (ULONG lLineID = (ULONG)wParam) {
		if (CStringArray * pv = (CStringArray *)lParam) {
			if (pv->GetSize () >= 2) {
				SETTINGSSTRUCT s;

				s.m_lLineID = lLineID;
				s.m_strKey	= (* pv) [0];
				s.m_strData = (* pv) [1];

				if (LPFJSYSTEM pSystem = FoxjetIpElements::GetSystem (lLineID)) {
					if (!s.m_strKey.CompareNoCase (ListGlobals::Keys::NEXT::m_strTime) ||
						!s.m_strKey.CompareNoCase (ListGlobals::Keys::NEXT::m_strDays)) 
					{
						fj_SystemFromString (pSystem, w2a (s.m_strData));
					}
				}

				if (!(lResult = UpdateSettingsRecord (theApp.m_Database, s)))
					VERIFY (lResult = AddSettingsRecord (theApp.m_Database, s));
			}
		}
	}

	return lResult;
}

LRESULT CControlView::OnGetNEXTSystemParams (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	if (ULONG lLineID = (ULONG)wParam) {
		SETTINGSSTRUCT s;

		if (CString * pstr = (CString *)lParam) {
			CString strKey = * pstr;

			pstr->Empty ();

			if (LPFJSYSTEM pSystem = FoxjetIpElements::GetSystem (lLineID)) {
				if (GetSettingsRecord (theApp.m_Database, lLineID, strKey, s)) {
					* pstr = s.m_strData;
					return 1;
				}
				else {
					if (!strKey.Compare (ListGlobals::Keys::NEXT::m_strTime)) {
						char sz [1024] = { 0 };

						fj_SystemTimeToString (pSystem, sz);
						TRACEF (sz);
						* pstr = a2w (sz);
						return 1;
					}
					else if (!strKey.Compare (ListGlobals::Keys::NEXT::m_strDays)) {
						char sz [1024] = { 0 };

						fj_SystemDateStringsToString (pSystem, sz);
						TRACEF (sz);
						* pstr = a2w (sz);
						return 1;
					}
					//else { ASSERT (0); }
				}
			}
		}
	}

	return 0;
}

LRESULT CControlView::OnRefreshImage (WPARAM wParam, LPARAM lParam)
{
	ULONG lLineID = (ULONG)wParam;
	CControlDoc * pDoc = GetDocument ();
	CStringArray v;
	MSG msg;

	pDoc->GetProductLines (v);

	for (int i = 0; i < v.GetSize (); i++) {
		if (CProdLine * pLine = pDoc->GetProductionLine (v [i])) {
			if (pLine->GetID () == lLineID) {
				CPrintHeadList heads;

				pLine->GetActiveHeads (heads);

				for (POSITION pos = heads.GetStartPosition(); pos; ) {
					ULONG lKey;
					CHead * pHead = NULL;

					heads.GetNextAssoc (pos, lKey, (void *&)pHead);
					ASSERT (pHead);

					while (!::PostThreadMessage (pHead->m_nThreadID, TM_SIGNAL_REFRESH, 0, lParam))
						::Sleep (0);
				}
			}
		}
	}

	return 0;
}


LRESULT CControlView::OnGetBoxID (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	ULONG lResult = FoxjetDatabase::NOTFOUND;

	if (CString * pstr = (CString *)wParam) {
		if (CSize * psize = (CSize *)lParam) {
			if ((lResult = GetBoxID (theApp.m_Database, * pstr)) == FoxjetDatabase::NOTFOUND) {
				try {
					COdbcRecordset rst (theApp.m_Database);
					CString strSQL;
					BOXSTRUCT box;

					strSQL.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%d AND [%s]=%d;"), 
						Boxes::m_lpszTable, 
						Boxes::m_lpszWidth, psize->cx, 
						Boxes::m_lpszHeight, psize->cy);

					TRACEF (strSQL);
					rst.Open (strSQL);

					if (!rst.IsEOF ()) {
						rst >> box;
						return box.m_lID;
					}

					box.m_strName = * pstr;
					box.m_lWidth = box.m_lLength = psize->cx;
					box.m_lHeight = psize->cy;
					AddBoxRecord (theApp.m_Database, box);
					lResult = box.m_lID;
				}
				catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
				catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
			}
		}
	}

	return lResult;
}

LRESULT CControlView::OnSetNextDynamicData (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	using namespace FoxjetIpElements;

	WORD wIndex = LOWORD (wParam);
	ULONG lLineID = HIWORD (wParam);
	CString str;
	CItemArray v;

	if (CString * pstr = (CString *)lParam) {
		str = * pstr;
		delete pstr;
	}

	FoxjetIpElements::GetDynamicData (lLineID, v);

	if (wIndex == LOWORD (MAKEWPARAM (-1, 0))) {
		for (int i = 0; i < v.GetSize (); i++) 
			v [i].m_strData = str;

		return v.GetSize ();
	}
	else if (wIndex >= 0 && wIndex < v.GetSize ()) {
		v [wIndex].m_strData = str;
		return 1;
	}

	return 0;
}


LRESULT CControlView::OnSetNextPort (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	CString strSCANNER_USE_ONCE = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, NEXT::CNextThread::m_strRegSection, _T ("SCANNER_USE_ONCE"), _T (""));
	bool bSCANNER_USE_ONCE = !strSCANNER_USE_ONCE.CompareNoCase (_T ("T"));
	DWORD dwBaudRate = _ttoi (FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, NEXT::CNextThread::m_strRegSection, _T ("BAUD_RATE"), _T ("")));

	if (!wParam) {
		if (::g_pNEXTPortType) {
			delete ::g_pNEXTPortType;
			::g_pNEXTPortType = NULL;
		}
	}
	else {
		if (!::g_pNEXTPortType)
			::g_pNEXTPortType = new NEXT::PORT_TYPE;

		* ::g_pNEXTPortType = (NEXT::PORT_TYPE)wParam;


		{
			CUIntArray v;

			FoxjetCommon::EnumerateSerialPorts (v);

			for (int i = 0; i < v.GetSize (); i++) {
				CString strPort;
				FoxjetDatabase::SETTINGSSTRUCT s;
				FoxjetCommon::StdCommDlg::CCommItem c;

				strPort.Format (_T ("COM%d"), v [i]);
				//s.m_strData = c.ToString ();

				if (!GetSettingsRecord (theApp.m_Database, 0, strPort, s))
					c.m_dwBaudRate = 0;

				TRACEF (s.m_strData);
				c.FromString (s.m_strData);

				if (c.m_dwBaudRate != dwBaudRate || c.m_bPrintOnce != bSCANNER_USE_ONCE) {
					c.m_dwPort		= v [i];
					c.m_bPrintOnce	= bSCANNER_USE_ONCE;
					c.m_dwBaudRate	= dwBaudRate;
					c.m_type		= HANDSCANNER;

					s.m_lLineID = 0;
					s.m_strKey = strPort;
					s.m_strData = c.ToString ();

					if (!UpdateSettingsRecord (theApp.m_Database, s)) 
						VERIFY (AddSettingsRecord (theApp.m_Database, s));

					EndCommThreads ();
					BeginCommThreads ();
				}
			}
		}
	}

	return 0;
}

LRESULT CControlView::OnNEXTSetDynamicIndex (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	CStringArray v;
	CControlDoc * pDoc = GetDocument ();
		
	pDoc->GetProductLines (v);

	for (int i = 0; i < v.GetSize (); i++) {
		CPrintHeadList HeadList;

		pDoc->GetActiveHeads (v [i], HeadList);
				
		for (POSITION pos = HeadList.GetStartPosition(); pos; )  {
			CHead * pHead = NULL;
			DWORD dwKey = 0;
	
			HeadList.GetNextAssoc(pos, dwKey, (void *&)pHead);
			ASSERT (pHead);

			CHeadInfo info = CHead::GetInfo (pHead);
			SEND_THREAD_MESSAGE (info, TM_NEXT_SET_DYNAMIC_INDEX, wParam);
		}
	}

	return 0;
}

LRESULT CControlView::OnNEXTRefineConfig (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	bool bReload = false;

	GetLineRecords (theApp.m_Database, vLines);

	{
		struct
		{
			UINT m_nID;
			CString m_str;
		} static const map [] = 
		{
			{ IDR_BARCODE,		FoxjetIpElements::CTextElement::GetFontDir () + a2w (BARCODE_FONT)		+ _T (".fjf"),	},
			{ IDR_BARCODE256,	FoxjetIpElements::CTextElement::GetFontDir () + a2w (BARCODE_FONT256)	+ _T (".fjf"),	},
		};

		for (int i = 0; i < ARRAYSIZE (map); i++) {
			if (::GetFileAttributes (map [i].m_str) == -1) {
				HMODULE hModule = ::GetModuleHandle (NULL);

				if (HRSRC hRSRC = ::FindResource (hModule, MAKEINTRESOURCE (map [i].m_nID), _T ("FJF"))) {
					DWORD dwSize = ::SizeofResource (hModule, hRSRC);

					if (HGLOBAL hGlobal = ::LoadResource (hModule, hRSRC)) {
						if (LPVOID lpData = ::LockResource (hGlobal)) {
							if (FILE * fp = _tfopen (map [i].m_str, _T ("wb"))) {
								fwrite (lpData, dwSize, 1, fp);
								fclose (fp);
							}
						}
					}
				}
			}
		}
	}

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		LINESTRUCT & line = vLines [nLine];
		CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

		GetHeadRecords (theApp.m_Database, vHeads);

		while (vHeads.GetSize () > 1) {
			int nIndex = vHeads.GetSize () - 1;

			bReload |= DeleteHeadRecord (theApp.m_Database, vHeads [nIndex].m_lID);
			vHeads.RemoveAt (nIndex);
		}
	}

	if (bReload)
		::PostMessage (HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, 0);

	return 0;
}

LRESULT CControlView::OnNEXTInitFonts (WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	FoxjetIpElements::CHead::InitFonts ();

	return 0;
}

LRESULT CControlView::OnNEXTEditSession(WPARAM wParam, LPARAM lParam)
{
	DBCONNECTION_WITH_RETURN (conn, 0);

	theApp.SetNEXTEditSession (wParam ? true : false);

	return 0;
}

LRESULT CControlView::OnNEXTSetPrintDriver(WPARAM wParam, LPARAM lParam)
{
	ULONG lHeadID = (ULONG)wParam;

	return 0;
}

LRESULT CControlView::OnInkUsage(WPARAM wParam, LPARAM lParam)
{
	INKCODE used, * pUsed = (INKCODE *)wParam;
	CControlDoc * pDoc = GetDocument ();
	CStringArray v;

	ASSERT (pUsed);
	used = * pUsed;
	delete pUsed;

	if (used.m_strCode.GetLength ()) {
		const CString strRootKey = FoxjetDatabase::Encrypted::InkCodes::GetRegSection ();
		const CString strSection = strRootKey + _T ("\\") + used.m_strCode + _T ("<NULL>");
		ULONG lHeadID = _tcstoul (Encrypted::GetProfileString (strSection, _T ("lHeadID"), ToString (used.m_lHeadID)), NULL, 10);

		TRACEF (_T ("[ink code] [") + ToString (lHeadID) + _T ("] ") + ToString (used.m_lHeadID) + _T (": [") + used.m_strCode + _T ("] lUsed = ") +
			ToString (used.m_lUsed));

		if (lHeadID == used.m_lHeadID) 
			VERIFY (Encrypted::WriteProfileInt64 (strSection, _T ("Used"), used.m_lUsed));
		else
			TRACEF (_T ("[ink code] ") + ToString (lHeadID) + _T (" != ") + ToString (used.m_lHeadID));
	}

	return 0;
}

LRESULT CControlView::OnInkUsageWarning (WPARAM wParam, LPARAM lParam)
{
	ULONG lHeadID = (ULONG)wParam;
	const int nWarning = (int)lParam;
	const int nLevel = nWarning & INK_CODE_WARNGING_LEVEL_MASK;
	const int nState = nWarning & INK_CODE_WARNGING_STATE_MASK;
	CInkSerialNumberDlg * pdlg = NULL;
	CControlDoc * pDoc = GetDocument ();
	CStringArray vLines;

	TRACER (ToString (lHeadID) + _T (": ") + CInkSerialNumberDlg::Trace (nWarning));

	pDoc->GetProductLines (vLines);

	for (POSITION pos = m_mapInkSerialNumber.GetStartPosition (); pos; ) {
		CInkSerialNumberDlg * p = NULL;
		ULONG lHeadID = -1;

		m_mapInkSerialNumber.GetNextAssoc (pos, lHeadID, p);

		if (p) {
			bool bDelete = false;

			for (int i = 0; i < vLines.GetSize (); i++) {
				if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
					CPrintHeadList list;

					pLine->GetActiveHeads (list);

					for (POSITION pos = list.GetStartPosition(); pos; ) {
						DWORD dwKey;
						CHead * pHead = NULL;

						list.GetNextAssoc(pos, dwKey, (void *&)pHead);
						ASSERT (pHead);
						CHeadInfo info = CHead::GetInfo (pHead);

						if (info.m_Config.m_HeadSettings.m_lID == lHeadID) {
							if (!info.m_Config.m_HeadSettings.m_bEnabled) {
								bDelete = true;
								break;
							}
						}
					}
				}
			}

			if (!::IsWindow (p->m_hWnd) || !p->IsWindowVisible () || !p->m_hBmpOK) 
				bDelete = true;

			if (bDelete) {
				delete p;
				m_mapInkSerialNumber.RemoveKey (lHeadID);
			}
		}
	}
	
	if (FoxjetDatabase::Encrypted::InkCodes::IsEnabled ()) {
		if (nWarning == 0) {
			if (m_mapInkSerialNumber.Lookup (lHeadID, pdlg)) {
				delete pdlg;
				pdlg = NULL;
				m_mapInkSerialNumber.RemoveKey (lHeadID);
			}
		}

		if ((nLevel > INK_CODE_WARNGING_LEVEL_0) || (nState & (INK_CODE_WARNGING_LOW_INK | INK_CODE_WARNGING_INK_OUT | INK_CODE_WARNGING_INVALID_CODE))) {
			m_mapInkSerialNumber.Lookup (lHeadID, pdlg);

			//if (pdlg) {
			//	if (!::IsWindow (pdlg->m_hWnd) || !pdlg->IsWindowVisible ()) {
			//		delete pdlg;
			//		pdlg = NULL;
			//		m_mapInkSerialNumber.RemoveKey (lHeadID);
			//	}
			//}

			if (!pdlg) {
				pdlg = new CInkSerialNumberDlg (this);
				pdlg->Create (CInkSerialNumberDlg::IDD);
				pdlg->ShowWindow (SW_SHOW);
				pdlg->CenterWindow (this);

				TRACEF (ToString (lHeadID) + _T (": ") + CInkSerialNumberDlg::Trace (nWarning));

				m_mapInkSerialNumber.SetAt (lHeadID, pdlg);

				if (m_mapInkSerialNumber.GetCount () > 1) {
					CRect rc (0, 0, 0, 0);
					int nIndex = 0;

					pdlg->GetWindowRect (&rc);
					
					int nIncrement = rc.top / m_mapInkSerialNumber.GetCount ();
					const CPoint ptIncrement (nIncrement, nIncrement);

					for (int i = 0; i < (m_mapInkSerialNumber.GetCount () / 2); i++)
						rc -= ptIncrement;

					for (POSITION pos = m_mapInkSerialNumber.GetStartPosition (); pos; ) {
						CInkSerialNumberDlg * p = NULL;
						ULONG lHeadID = -1;

						m_mapInkSerialNumber.GetNextAssoc (pos, lHeadID, p);

						if (p && p->IsWindowVisible ()) {
							p->MoveWindow (rc);
							rc += CPoint (nIncrement, nIncrement);
						}
					}
				}
			}
		}
	}

	if (pdlg) {
		CStringArray vErrors;
		
		for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
			if (CProdLine * pLine = pDoc->GetProductionLine (vLines [nLine])) {
				pLine->GetWarnings (vErrors);
			}
		}

		//TRACEF (ToString (lHeadID) + _T (": ") + CInkSerialNumberDlg::Trace (nWarning));

		pdlg->PostMessage (WM_INKCODEDLG_SETHEAD, (WPARAM)lHeadID, (LPARAM)nWarning); //pdlg->SetHead (lHeadID, nWarning);

		{
			CStringArray * p = new CStringArray ();
			p->Append (vErrors);
			pdlg->PostMessage (WM_SET_WARNING_MESSAGES, (WPARAM)p, 0);
		}
	}

	/*
	{ // check all
		for (int i = 0; i < vLines.GetSize (); i++) {
			if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
				CPrintHeadList list;

				pLine->GetActiveHeads (list);

				for (POSITION pos = list.GetStartPosition(); pos; ) {
					DWORD dwKey;
					CHead * pHead = NULL;

					list.GetNextAssoc(pos, dwKey, (void *&)pHead);
					ASSERT (pHead);
					CHeadInfo info = CHead::GetInfo (pHead);

					if (info.m_Config.m_HeadSettings.m_lID != lHeadID) {
						int nWarning = 0;

						SEND_THREAD_MESSAGE (info, TM_CHECK_INK_WARNING_LEVEL, &nWarning);

						if (nWarning && pdlg) {
							int nLevel = nWarning & INK_CODE_WARNGING_LEVEL_MASK;
							int nState = nWarning & INK_CODE_WARNGING_STATE_MASK;

							nLevel = max (nLevel, pdlg->SendMessage (WM_GET_MAX_INK_WARNING, lHeadID, INK_CODE_WARNGING_LEVEL_MASK));
							int n = nState | nLevel;

							//TRACER (ToString (lHeadID) + _T (": ") + CInkSerialNumberDlg::Trace (n));
							pdlg->SetHead (lHeadID, n);
						}
					}
				}
			}
		}
	}
	*/

	if (nLevel >= INK_CODE_WARNGING_LEVEL_3 || (nState & (INK_CODE_WARNGING_INK_OUT | INK_CODE_WARNGING_INVALID_CODE))) {
		CControlDoc * pDoc = GetDocument ();
		CStringArray v;

		pDoc->GetProductLines (v);

		for (int i = 0; i < v.GetSize (); i++) {
			if (CProdLine * pLine = pDoc->GetProductionLine (v [i])) {
				CPrintHeadList list;

				pLine->GetActiveHeads (list);

				for (POSITION pos = list.GetStartPosition(); pos; ) {
					DWORD dwKey;
					CHead * pHead = NULL;

					list.GetNextAssoc(pos, dwKey, (void *&)pHead);
					ASSERT (pHead);
					CHeadInfo info = CHead::GetInfo (pHead);

					if (info.m_Config.m_HeadSettings.m_lID == lHeadID) {
						pLine->IdleTask (false);
						break;
					}
				}
			}
		}
	}

	return 0;
}



LRESULT CControlView::OnDownloadSerialData (WPARAM wParam, LPARAM lParam)
{
	if (CString * p = (CString *)wParam) {
		CStringArray vLines;
		CControlDoc * pDoc = GetDocument ();
		CString str = * p;

		TRACEF (ToString (lParam) + _T (": ") + str);

		delete p;

		pDoc->GetProductLines (vLines);

		for (int i = 0; i < vLines.GetSize (); i++) {
			if (CProdLine * pLine = pDoc->GetProductionLine (vLines [i])) {
				if (pLine->GetID () == lParam) {
					pLine->DownloadSerialData (str, this);
				}
			}
		}
	}

	return 0;
}

LRESULT CControlView::OnInkCodeAccepted (WPARAM wParam, LPARAM lParam)
{
	MsgBox (LoadString (IDS_VALID_CODE_ENTERED), MB_ICONINFORMATION | MB_OK);
	return 0;
}



LRESULT CControlView::OnInkCodeChange (WPARAM wParam, LPARAM lParam)
{
	if (WM_INK_CODE_CHANGE_STRUCT * pParam = (WM_INK_CODE_CHANGE_STRUCT *)wParam) {
		WM_INK_CODE_CHANGE_STRUCT params = * pParam;

		delete pParam;

		if (params.m_bDismiss) {
			CInkSerialNumberDlg * pdlg = NULL;

			if (m_mapInkSerialNumber.Lookup (params.m_lHeadID, pdlg)) {
				m_mapInkSerialNumber.RemoveKey (params.m_lHeadID);
				delete pdlg;
			}
		}

		switch (params.m_nDoModalResult) {
		case IDOK:
			{
				InitInkCodes ();

				if (params.m_bDismiss) {
					CStringArray vLines;
					CControlDoc * pDoc = GetDocument ();

					pDoc->GetProductLines (vLines);

					for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
						CString strLine = vLines [nLine];

						if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
							if (pLine->GetTaskState () == IDLE)
								pLine->ResumeTask ();
						}
					}
				}
			}
			break;
		case IDCANCEL:
			break;
		}
	}

	return 0;
}


LRESULT CControlView::OnCheckInkCodeState (WPARAM wParam, LPARAM lParam)
{
	int nLevel = INK_CODE_WARNGING_LEVEL_0;

	for (POSITION pos = m_mapInkSerialNumber.GetStartPosition (); pos; ) {
		CInkSerialNumberDlg * pdlg = NULL;
		ULONG lHeadID = -1;

		m_mapInkSerialNumber.GetNextAssoc (pos, lHeadID, pdlg);

		if (pdlg) {
			nLevel = pdlg->SendMessage (WM_GET_MAX_INK_WARNING, 0, INK_CODE_WARNGING_LEVEL_MASK | INK_CODE_WARNGING_STATE_MASK);
			//TRACER (CInkSerialNumberDlg::Trace (nLevel));
			pdlg->PostMessage (WM_INK_CODE_SET_STATE, nLevel);
		}
	}

	return nLevel;
}


LRESULT CControlView::OnGetInkCodes (WPARAM wParam, LPARAM lParam)
{
	std::vector <std::wstring> v;
	std::vector <std::wstring> & vResult = * (std::vector <std::wstring> *)wParam;\
	CControlDoc * pDoc = GetDocument ();
	const CString strKey = FoxjetDatabase::Encrypted::InkCodes::GetRegSection ();
	std::vector <std::wstring> vMAC = FoxjetDatabase::GetMacAddresses ();
	CStringArray vLines, vCode;

	pDoc->GetProductLines (vLines);
	FoxjetDatabase::Encrypted::Enumerate (strKey, v);

	for (int i = 0; i < v.size (); i++) {
		CString strInkCode = CString (v [i].c_str ());
		CString strSubKey = strKey + _T ("\\") + strInkCode;
		ULONG lHeadID = _tcstoul (FoxjetDatabase::Encrypted::GetProfileString (strSubKey, _T ("lHeadID"), _T ("")), NULL, 10);
		CString str = strInkCode;

		str.Replace (_T ("<NULL>"), _T (""));
		vCode.Add (str);
	}

	while (vCode.GetSize ()) {
		CStringArray v;

		v.Add (BW_HOST_INK_CODE);
	
		for (int i = 0; i < vMAC.size (); i++)
			v.Add (vMAC [i].c_str ());

		while (vCode.GetSize ()) {
			v.Add (vCode [0]);
			vCode.RemoveAt (0);
		}

		CString str = FoxjetDatabase::ToString (v);

		while (str.GetLength () > CUdpThread::m_nBufferSize) {
			int nIndex = v.GetSize () - 1;

			vCode.Add (v [nIndex]);
			v.RemoveAt (nIndex);
			str = FoxjetDatabase::ToString (v);
		}

		vResult.push_back (std::wstring (str));
	}

	return vResult.size ();
}

void CControlView::InitInkCodes ()
{
	CStringArray vLines;
	CControlDoc * pDoc = GetDocument ();
	std::vector <std::wstring> v;
	const CString strKey = FoxjetDatabase::Encrypted::InkCodes::GetRegSection ();

	pDoc->GetProductLines (vLines);
	FoxjetDatabase::Encrypted::Enumerate (strKey, v);


	for (int i = 0; i < v.size (); i++) {
		CString strInkCode = CString (v [i].c_str ());
		CString strSubKey = strKey + _T ("\\") + strInkCode;
		ULONG lHeadID = _tcstoul (FoxjetDatabase::Encrypted::GetProfileString (strSubKey, _T ("lHeadID"), _T ("0")), NULL, 10);

		for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
			CString strLine = vLines [nLine];

			if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
				CPrintHeadList list;

				pLine->GetActiveHeads (list);

				for (POSITION pos = list.GetStartPosition(); pos; ) {
					DWORD dwKey;
					CHead * pHead = NULL;

					list.GetNextAssoc (pos, dwKey, (void *&)pHead);
					ASSERT (pHead);
					CHeadInfo info = CHead::GetInfo (pHead);

					if (lHeadID == info.m_Config.m_HeadSettings.m_lID) {
						CString str = strInkCode;

						str.Replace (_T ("<NULL>"), _T (""));
						str.Trim ();

						bool bValid = Encrypted::IsValidInkCode (std::vector <std::wstring> (), str);

						if (theApp.m_bDemo)
							bValid = str.GetLength () == 9 ? true : false;

						if (bValid) {

							/*
							#ifdef _DEBUG
							{
								static CMap <ULONG, ULONG, ULONG, ULONG> map;
								ULONG lTmp;

								if (!map.Lookup (lHeadID, lTmp)) {
									long double dPixel	= FoxjetDatabase::CalcInkUsage (info.m_Config.m_HeadSettings.m_nHeadType, FoxjetDatabase::INKTYPE_FIRST, 1);
									__int64 lMax		= (unsigned __int64)((long double)500.0 / dPixel);

									const CString strRootKey = FoxjetDatabase::Encrypted::InkCodes::GetRegSection ();
									const CString strSection = strRootKey + _T ("\\") + str + _T ("<NULL>");
									//__int64 lUsed = FoxjetDatabase::Encrypted::GetProfileInt64 (strSection, _T ("Used"), lMax);
									__int64 lUsed = (__int64)(lMax * 0.98);

									TRACEF ("############################################## TODO: rem ##############################################");
									VERIFY (Encrypted::WriteProfileInt64 (strSection, _T ("Used"), lUsed));
									map.SetAt (lHeadID, 0);
								}
							}
							#endif
							*/

							SEND_THREAD_MESSAGE (info, TM_INK_CODE_CHANGE, (WPARAM)(LPCTSTR)strInkCode);
						}
					}
				}
			}
		}
	}

	if (m_pUdpThread) {
		while (!::PostThreadMessage (m_pUdpThread->m_nThreadID, TM_BROADCAST_INK_CODE, 0, 0))
			::Sleep (1);
	}
}



LRESULT CControlView::OnSocketRecieveInkCode (WPARAM wParam, LPARAM lParam)
{
	ASSERT (wParam);
	ASSERT (lParam);

	const CStringArray & vCmd = * (const CStringArray *)wParam;
	CString & strResult = * (CString *)lParam;
	CStringArray vResult;
	const CString strKey = FoxjetDatabase::Encrypted::InkCodes::GetRegSection ();
	std::vector <std::wstring> v;
	const CString strDefault = _T ("[not found]");

	FoxjetDatabase::Encrypted::Enumerate (strKey, v);

	for (int i = 1; i < vCmd.GetSize (); i++) {
		CString strInkCode = vCmd [i]; //CString (v [i].c_str ());

		strInkCode.Replace (_T ("<NULL>"), _T (""));

		if (strInkCode.Find (_T (":")) == -1 && Find (v, std::wstring (strInkCode)) == -1) {
			CString strSection = FoxjetDatabase::Encrypted::InkCodes::GetRegSection () + _T ("\\") + strInkCode + _T ("<NULL>");
			CString strHeadID = FoxjetDatabase::Encrypted::GetProfileString (strSection, _T ("lHeadID"), strDefault);

			if (strHeadID == strDefault) {
				TRACEF (strInkCode);
				VERIFY (FoxjetDatabase::Encrypted::WriteProfileString (strSection, _T ("lHeadID"), _T ("")));
				VERIFY (FoxjetDatabase::Encrypted::WriteProfileString (strSection, _T ("IP"), strResult));
				//VERIFY (FoxjetDatabase::Encrypted::WriteProfileInt64 (strSection, _T ("Used"), (unsigned __int64)0));
			}
		}
	}

	vResult.Add (BW_HOST_INK_CODE);
	strResult = FoxjetDatabase::ToString (vResult);

	return 0;
}

LRESULT CControlView::OnLogInkCodeReport (WPARAM wParam, LPARAM lParam)
{
	if (CString * pstrCode = (CString *)lParam) {
		CString strCode = * pstrCode;
		ULONG lHeadID = (ULONG)wParam;

		delete pstrCode;

		if (strCode.GetLength () == 9) {
			strCode.Insert (6, '-');
			strCode.Insert (3, '-');
		}

		if (theApp.IsPrintReportLoggingEnabled ()) {
			CStringArray vLines;
			CControlDoc * pDoc = GetDocument ();
			REPORTSTRUCT s;
			bool bFound = false;

			s.m_dtTime		= COleDateTime::GetCurrentTime ();
			s.m_strTaskName = strCode;
			s.m_lPrinterID	= GetPrinterID (theApp.m_Database);
			s.m_strUsername	= theApp.m_strUsername;

			pDoc->GetProductLines (vLines);

			for (int nLine = 0; !bFound && nLine < vLines.GetSize (); nLine++) {
				CString strLine = vLines [nLine];

				if (CProdLine * pLine = pDoc->GetProductionLine (strLine)) {
					CPrintHeadList list;

					pLine->GetActiveHeads (list);

					for (POSITION pos = list.GetStartPosition(); !bFound && pos; ) {
						DWORD dwKey;
						CHead * pHead = NULL;

						list.GetNextAssoc (pos, dwKey, (void *&)pHead);
						ASSERT (pHead);
						CHeadInfo info = CHead::GetInfo (pHead);

						if (info.m_Config.m_HeadSettings.m_lID == lHeadID) {
							s.m_strLine		= info.m_Config.m_Line.m_strName;
							s.m_strCounts	= FoxjetDatabase::ToString (info.m_Status.m_lCount);
							bFound			= true;
							break;
						}
					}
				}
			}

			FoxjetDatabase::AddPrinterReportRecord (theApp.m_Database, FoxjetDatabase::REPORT_INKCODE, s, s.m_lPrinterID);
		}
	}

	return 0;
}

LRESULT CControlView::OnSetHeadWarnings (WPARAM wParam, LPARAM lParam)
{
	if (CStringArray * pwParam = (CStringArray *)wParam) {
		for (POSITION pos = m_mapInkSerialNumber.GetStartPosition (); pos; ) {
			CInkSerialNumberDlg * pdlg = NULL;
			ULONG lHeadID = -1;

			m_mapInkSerialNumber.GetNextAssoc (pos, lHeadID, pdlg);

			if (pdlg) {
				CStringArray * p = new CStringArray;
				p->Append (* pwParam);
				pdlg->PostMessage (WM_SET_WARNING_MESSAGES, (WPARAM)p, lParam);
			}
		}

		delete pwParam;
	}

	return 0;
}

//LRESULT CControlView::OnGetProdLines (WPARAM wParam, LPARAM lParam)
//{
//	if (CStringArray * pv = (CStringArray *)wParam) {
//		if (CControlDoc * pDoc = GetDocument ()) 
//			pDoc->GetProductLines (* pv);
//	}
//
//	return 0;
//}
//
//
//LRESULT CControlView::OnGetProdLineID (WPARAM wParam, LPARAM lParam)
//{
//	CStringArray v;
//
//	if (CString * pstrFind = (CString *)wParam) {
//		if (CControlDoc * pDoc = GetDocument ()) {
//			pDoc->GetProductLines (v);
//
//			for (int i = 0; i < v.GetSize (); i++)
//				if (CProdLine * pLine = pDoc->GetProductionLine (v [i]))
//					if (!pLine->GetName ().CompareNoCase (* pstrFind))
//						return pLine->m_LineRec.m_lID;
//		}
//	}
//
//	return NOTFOUND;
//}
//
//LRESULT CControlView::OnGetActiveHeads (WPARAM wParam, LPARAM lParam)
//{
//	if (CControlDoc * pDoc = GetDocument ()) {
//		if (CString * pstrLine = (CString *)wParam) {
//			if (CPrintHeadList * pList = (CPrintHeadList *)lParam) {
//				if (CProdLine * pLine = pDoc->GetProductionLine (* pstrLine)) {
//					pLine->GetActiveHeads (* pList);
//
//					return pList->GetSize ();
//				}
//			}
//		}
//	}
//
//	return 0;
//}
//
//LRESULT CControlView::OnGetThreadByUID (WPARAM wParam, LPARAM lParam)
//{
//	if (CControlDoc * pDoc = GetDocument ()) {
//		if (CString * pstrUID = (CString *)wParam) {
//			if (CControlDoc * pDoc = GetDocument ()) {
//				CStringArray v;
//
//				pDoc->GetProductLines (v);
//
//				for (int i = 0; i < v.GetSize (); i++) {
//					if (CProdLine * pLine = pDoc->GetProductionLine (v [i])) {
//						if (CLocalHead * p = DYNAMIC_DOWNCAST (CLocalHead, pLine->GetHeadByUID (* pstrUID))) 
//							return (LPARAM)p;
//					}
//				}
//			}
//		}
//	}
//
//	return NULL;
//}
//
//LRESULT CControlView::OnGetProdLineIdByHeadId (WPARAM wParam, LPARAM lParam)
//{
//	ULONG lHeadID = (ULONG)wParam;
//
//	if (CControlDoc * pDoc = GetDocument ()) {
//		if (CProdLine * pLine = pDoc->GetProductionLine (wParam)) {
//			CStringArray v;
//
//			pDoc->GetProductLines (v);
//
//			for (int i = 0; i < v.GetSize (); i++) {
//				if (CProdLine * p = pDoc->GetProductionLine (v [i])) {
//					CPrintHeadList list;
//
//					pLine->GetActiveHeads (list);
//
//
//					for (POSITION pos = list.GetStartPosition(); pos; ) {
//						CHead * pHead = NULL;
//						DWORD dwKey = 0;
//	
//						list.GetNextAssoc(pos, dwKey, (void *&)pHead);
//						ASSERT (pHead);
//
//						CHeadInfo info = CHead::GetInfo (pHead);
//
//						if (info.m_Config.m_HeadSettings.m_lID == lHeadID)
//							return pLine->GetID ();
//					}
//				}
//			}
//		}
//	}
//
//	return 0;
//}
//
//
//LRESULT CControlView::OnGetLineName (WPARAM wParam, LPARAM lParam)
//{
//	if (CControlDoc * pDoc = GetDocument ()) {
//		if (ULONG lProdLineID = (ULONG)wParam) {
//			if (CString * pstr = (CString *)lParam) {
//				CStringArray v;
//
//				pDoc->GetProductLines (v);
//
//				for (int i = 0; i < v.GetSize (); i++) {
//					if (CProdLine * pLine = pDoc->GetProductionLine (v [i])) {
//						if (pLine->m_LineRec.m_lID == lProdLineID) {
//							* pstr = pLine->m_LineRec.m_strName;
//	
//							return 1;
//						}
//					}
//				}
//			}
//		}
//	}
//
//	return 0;
//}
//
//LRESULT CControlView::OnGetSerialDataPtr (WPARAM wParam, LPARAM lParam)
//{
//	if (CControlDoc * pDoc = GetDocument ()) {
//		if (CString * pstrLine = (CString *)wParam) {
//			if (CProdLine * pLine = pDoc->GetProductionLine (* pstrLine)) {
//				return (LPARAM)pLine->GetSerialData ();
//			}
//		}
//	}
//
//	return NULL;
//}
//
//
//LRESULT CControlView::OnGetSw0848Thread (WPARAM wParam, LPARAM lParam)
//{
//	if (CControlDoc * pDoc = GetDocument ()) {
//		if (ULONG lProdLineID = (ULONG)wParam) {
//			CStringArray v;
//
//			pDoc->GetProductLines (v);
//
//			for (int i = 0; i < v.GetSize (); i++) {
//				if (CProdLine * pLine = pDoc->GetProductionLine (v [i])) {
//					if (pLine->m_LineRec.m_lID == lProdLineID)
//						return (LRESULT)pLine->m_pSw0848Thread;
//				}
//			}
//		}
//	}
//
//	return NULL;
//}