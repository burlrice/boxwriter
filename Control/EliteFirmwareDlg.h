#if !defined(AFX_ELITEFIRMWAREDLG_H__F8C27D9F_13FD_4A4E_B162_09EE2D9695EC__INCLUDED_)
#define AFX_ELITEFIRMWAREDLG_H__F8C27D9F_13FD_4A4E_B162_09EE2D9695EC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EliteFirmwareDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CEliteFirmwareDlg dialog

#ifndef __WINPRINTER__
#error can't build with the current configuration
#endif

#include "LocalHead.h"
#include "ListCtrlImp.h"

namespace EliteFirmwareDlg
{
	class CHeadItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CHeadItem (CLocalHead * pHead);
		virtual ~CHeadItem ();

		virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
		virtual CString GetDispText (int nColumn) const;

		CString m_strName;
		CString m_strAddr;
		CString m_strGaVer;
		CLocalHead * m_pHead;
	};

	class CEliteFirmwareDlg : public CPropertyPage
	{
		DECLARE_DYNCREATE(CEliteFirmwareDlg)

	// Construction
	public:

		CArray <CLocalHead *, CLocalHead *> m_vHeads;

		CEliteFirmwareDlg();
		~CEliteFirmwareDlg();

	// Dialog Data
		//{{AFX_DATA(CEliteFirmwareDlg)
			// NOTE - ClassWizard will add data members here.
			//    DO NOT EDIT what you see in these blocks of generated code !
		//}}AFX_DATA


	// Overrides
		// ClassWizard generate virtual function overrides
		//{{AFX_VIRTUAL(CEliteFirmwareDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:

		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
		HBRUSH m_hbrDlg;

		// Generated message map functions
		//{{AFX_MSG(CEliteFirmwareDlg)
		virtual BOOL OnInitDialog();
		afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()

	};
}; //namespace EliteFirmwareDlg

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ELITEFIRMWAREDLG_H__F8C27D9F_13FD_4A4E_B162_09EE2D9695EC__INCLUDED_)
