#if !defined(AFX_HEAD_H__D127FEA8_C5C7_4E80_A5F3_9B68BD932B0E__INCLUDED_)
#define AFX_HEAD_H__D127FEA8_C5C7_4E80_A5F3_9B68BD932B0E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Head.h : header file

#include "Control.h"
#include "KeyParse.h"
#include "HeadCfg.h"
#include "List.h"
#include "ImageEng.h"
#include "TypeDefs.h"	// Added by ClassView
#include "IvRegs.h"
#include "Sw0848Thread.h"

class CLocalHead;
class CRemoteHead;

#ifdef _DEBUG
	//#define __DEBUG_LOW_INK_CODE__ 
#endif

namespace Head
{
	typedef CArray <FoxjetCommon::CBaseElement *, FoxjetCommon::CBaseElement *> CElementPtrArray;

	////////////////////////////////////////////////////////////////////////////

	template<class KEY, class ARG_KEY, class VALUE, class ARG_VALUE>
	class CSyncMap : public CMap <KEY, ARG_KEY, VALUE, ARG_VALUE>
	{
	public:
		CSyncMap (int nBlockSize = 10) : CMap <KEY, ARG_KEY, VALUE, ARG_VALUE> (nBlockSize) { }

		CDiagnosticCriticalSection m_cs;
	};

	using FoxjetDatabase::MESSAGESTRUCT;
	using FoxjetDatabase::TASKSTRUCT;
	using FoxjetDatabase::BOXSTRUCT;
	using FoxjetDatabase::TASKSTATE;

	class CHead;

	class CHeadInfo
	{
		friend class CHead;
		friend class ::CLocalHead;
		friend class ::CRemoteHead;

	public:

		typedef struct tagPOSTMESSAGESTRUCT
		{
			tagPOSTMESSAGESTRUCT (bool bPost, const CString & strFile, ULONG lLine, HANDLE hEvent) : m_bPost (bPost), m_strFile (strFile), m_lLine (lLine), m_hEvent (hEvent) { }

			bool m_bPost;
			CString m_strFile;
			ULONG m_lLine;
			HANDLE m_hEvent;
		} POSTMESSAGESTRUCT;

		typedef struct tagERRORSCOLORSTRUCT
		{
			COLORREF m_rgb;
			FoxjetDatabase::HEADERRORTYPE m_e;
		} ERRORSCOLORSTRUCT;

		CHeadInfo ();
		CHeadInfo (const CHeadInfo & rhs);
		CHeadInfo & operator = (const CHeadInfo & rhs);
		virtual ~CHeadInfo ();

		HWND						m_hControlWnd;
		bool						m_bDisabled;
		_IMAGE_HDR_PKT				m_ImageHdr;
		CString						m_sMessageName;
		MESSAGESTRUCT				m_Message;
		TASKSTRUCT					m_Task;
		BOXSTRUCT					m_Box;
		CString						m_sTaskName;
		CString						m_strKeyValue;
		CString						m_sDatabaseName;

		CHeadCfg					m_Config;
		FoxjetCommon::DRAWCONTEXT	m_DrawContext;
		bool						m_bRemoteHead;
		_STATUS						m_Status;
		TASKSTATE					m_eTaskState;
		WORD						m_wGaVer;
		int							m_nErrorIndex;
		bool						m_bPrintDriver;
		INKCODE						m_inkcode;

		#ifdef __IPPRINTER__
		CString						m_strSwVer;
		CString						m_strGaVer;
		double						m_dDiskUsage;
		bool						m_bDigiBoard;
		#endif //__IPPRINTER__

		#ifdef __WINPRINTER__
		long double m_dRemainingInk;
		#endif //__WINPRINTER__

	protected:
		DWORD						m_dwThreadID;
		CString						m_strDisplay;
		bool						m_bPrinting;
		int							m_nLineSpeed;

	public:
		DWORD				GetThreadID () const;
		CString				GetDisplayMessage () const;
		CString				GetProductionLine () const	{ return m_Config.m_Line.m_strName; }
		bool				IsMaster ()	const			{ return m_Config.IsMaster (); }
		double				GetLineSpeed () const		{ return m_Status.m_dLineSpeed; }
		CString				GetUID () const				{ return m_Config.m_HeadSettings.m_strUID; } 
		CString				GetFriendlyName () const	{ return m_Config.GetHeadSettings().m_strName; }
		DWORD				GetGaVersion () const		{ return m_wGaVer; }
		CString				GetTaskName () const		{ return m_sTaskName; }
		CString				GetKeyValue () const		{ return m_strKeyValue; }
		CString				GetPanelName () const		{ return m_Config.m_BoxPanel.m_strName; }
		ULONG				GetBoxPanel () const		{ return m_Config.m_BoxPanel.m_lID; }
		CString				GetCount () const			{ return FoxjetCommon::FormatI64 (m_Status.m_lCount, 6); }
		CString				GetMessageName () const		{ return m_sMessageName; }
		TASKSTATE			GetTaskState () const		{ return m_eTaskState; }
		_STATUS				GetStatus () const			{ return m_Status; }
		virtual bool		IsConnected () const		{ return true; }
		ULONG				GetIOAddress () const		{ return _tcstoul (m_Config.m_HeadSettings.m_strUID, NULL, 16); }
		bool				IsPrinting () const			{ return m_bPrinting; }
		CString				GetStateString () const;
		CString				GetErrors ();
		CString				GetWarnings ();
		bool				IsErrorPresent (DWORD dwError);
		virtual CString		GetStatusMessage ();
		virtual eHeadStatus GetErrorStatus ();
		UINT				CalcFlushBuffer (); 
		bool				IsUSB ();
		DWORD				GetImageBufferSize ()		
		{ 
			#ifdef __WINPRINTER__
				return IsUSB () ? 0x00080000 : IMAGE_BUFFER_SIZE; 
			#else
				return 0x00080000;
			#endif
		}

		#ifdef __IPPRINTER__
		CString GetSwVer () const		{ return m_strSwVer; }
		CString GetGaVer () const		{ return m_strSwVer; }
		bool IsDigiBoard () const		{ return m_bDigiBoard; }
		double GetDiskUsage () const	{ return m_dDiskUsage; }
		#endif

		void PostMessage (UINT nMsg, WPARAM wParam, LPCTSTR lpszFile, ULONG lLine) const;
		void SendMessage (UINT nMsg, WPARAM wParam, LPCTSTR lpszFile, ULONG lLine) const;
		void SendMessage (CHead * pThread, UINT nMsg, WPARAM wParam, LPCTSTR lpszFile, ULONG lLine);
	};
}; // namespace Head;

#define POST_THREAD_MESSAGE(info,msg,wParam)			(info).PostMessage ((msg), (WPARAM)(wParam), _T (__FILE__), __LINE__);
#define SEND_THREAD_MESSAGE(info,msg,wParam)			(info).SendMessage ((msg), (WPARAM)(wParam), _T (__FILE__), __LINE__);

class CProdLine;
class CDebugDlg;
class CDebugPage;

namespace Head
{
	////////////////////////////////////////////////////////////////////////////
	// CHead thread

	class CHead : public CWinThread, protected CHeadInfo
	{
		friend class ::CDebugDlg;
		friend class ::CDebugPage;
		friend class CConfigUpdater;

	public:
		typedef struct tagCHANGECOUNTSTRUCT 
		{
			tagCHANGECOUNTSTRUCT (__int64 lCount = 0) : m_lCount (lCount), m_lPalletCount (0), m_lPalletMin (0), m_lPalletMax (0), m_lUnitsPerPallet (0), m_bIrregularPalletSize (false) { }

			__int64 m_lCount;
			__int64 m_lPalletCount;
			__int64 m_lPalletMin;
			__int64 m_lPalletMax;
			__int64 m_lUnitsPerPallet;
			bool	m_bIrregularPalletSize;
		} CHANGECOUNTSTRUCT;

		typedef struct
		{
			CHead *		m_pHead;
			ULONG		m_lLineID;
			ULONG		m_lAddress;
			HANDLE		m_hEvent;
			ULONG		m_lHeadID;
		} TM_SET_IO_ADDRESS_STRUCT;

		typedef struct tagGETELEMENTSTRUCT
		{
			tagGETELEMENTSTRUCT (CElementPtrArray * pv, int nClassID) : m_pv (pv), m_nClassID (nClassID) { } 

			CElementPtrArray * m_pv;
			int m_nClassID;
		} GETELEMENTSTRUCT;

		typedef struct tagSETUSERDATASTRUCT
		{
			tagSETUSERDATASTRUCT (const CString & strPrompt, const CString & strData) : m_strPrompt (strPrompt), m_strData (strData) { }

			CString m_strPrompt;
			CString m_strData;
		} SETUSERDATASTRUCT;

		typedef struct tagUPDATELEMENTSTRUCT
		{
			tagUPDATELEMENTSTRUCT (ULONG lID)			: m_bResult (false), m_lID (lID) { }
			tagUPDATELEMENTSTRUCT (const CString & str) : m_bResult (false), m_lID (-1), m_strData (str) { }

			ULONG m_lID;
			CString m_strData;
			bool m_bResult;
		} UPDATELEMENTSTRUCT;

		typedef struct tagCACHESTRUCT
		{
			tagCACHESTRUCT (const FoxjetDatabase::TASKSTRUCT & task, const FoxjetDatabase::BOXSTRUCT & box, CMap <ULONG, ULONG, ULONG, ULONG> & mapHeads) 
			:	m_task (task), 
				m_box (box), 
				m_bResult (false) 
			{
				for (POSITION pos = mapHeads.GetStartPosition (); pos; ) {
					ULONG lKey = 0, lValue = 0;

					mapHeads.GetNextAssoc (pos, lKey, lValue);
					m_mapHeads.SetAt (lKey, lValue);
				}
			}

			FoxjetDatabase::TASKSTRUCT m_task;
			FoxjetDatabase::BOXSTRUCT m_box;
			CMap <ULONG, ULONG, ULONG, ULONG> m_mapHeads;
			bool m_bResult;
		} CACHESTRUCT;

		typedef struct tagPREVIEWSTRUCT
		{
			tagPREVIEWSTRUCT () : /* m_pLock (NULL), */ m_pBmp (NULL) { }

			//CCriticalSection *	m_pLock;
			CBitmap *			m_pBmp;
		} PREVIEWSTRUCT;

		typedef struct tagPHOTOCELLOFFSETSTRUCT
		{
			tagPHOTOCELLOFFSETSTRUCT (ULONG lMinPhotocellDelay, ULONG lMaxPhotocellDelay, UINT nMaxFlush) 
				:	m_lMinPhotocellDelay (lMinPhotocellDelay), 
					m_lMaxPhotocellDelay (lMaxPhotocellDelay),
					m_nMaxFlush (nMaxFlush)
			{ 
			}

			ULONG m_lMinPhotocellDelay;
			ULONG m_lMaxPhotocellDelay;
			UINT m_nMaxFlush;
		} PHOTOCELLOFFSETSTRUCT;

		typedef struct tagSUSPENDSTRUCT
		{
			tagSUSPENDSTRUCT () : m_hSuspend (NULL), m_hLoaded (NULL) { }

			HANDLE m_hSuspend;
			HANDLE m_hLoaded;
			CString m_strSuspend;
			CString m_strLoaded;
		} SUSPENDSTRUCT;

		typedef struct tagCONFIGCHANGESTRUCT
		{
			tagCONFIGCHANGESTRUCT () 
			{
			}
			CStringArray					m_vLines;
			CPrintHeadList					m_listHeads;
			tagSerialData 					m_serialData;
			Sw0848Thread::CSw0848Thread *	m_pSw0848Thread;
		} CONFIGCHANGESTRUCT;

		DECLARE_DYNCREATE(CHead)

		Head::CHead::CHANGECOUNTSTRUCT	m_countLast;

	protected:
		CHead();           // protected constructor used by dynamic creation

		CHeadInfo GetInfo () const;
		operator CHeadInfo () const { return GetInfo (); }

		virtual BOOL PreTranslateMessage (MSG * pMsg);

	// Attributes
	public:
		virtual ~CHead();

		LPARAM SendControlMessage (UINT nMsg, WPARAM wParam = 0, LPARAM lParam = 0);

		static void SetInfo (const CHead & h, const CHeadInfo * pInfo);
		static CHeadInfo GetInfo (const CHead & h);
		static CHeadInfo GetInfo (const CHead * p);
		static void FreeInfo ();
		static CString TraceMessage (MSG * pMsg);
		static void InitializeCriticalSection ();
		static void DeleteCriticalSection ();

		static const DWORD m_dwExitTimeout;

		CHeadInfo GetInfo (int) const;

		static bool IsWarning (const _STATUS & s);
		static bool IsError (const _STATUS & s);
		static bool IsError (DWORD dw);
		static bool IsWarning (DWORD dw);
		static CString GetMessageString (UINT nMsg);
	
	protected:
		CImageEng *		m_pImageEng;
		eSerialDevice	m_SerialDevice;
		CMapStringToPtr m_vCommSettings;
		CONFIGCHANGESTRUCT m_params;

		void FreeCommSettings ();
		bool IsOneToOnePrint (eSerialDevice type = REMOTEDEVICE);

		// TODO: rem //virtual DWORD GetImageBufferSize () { return 0; };

		virtual bool DeleteElement (ULONG lID);
		virtual ULONG AddElement (const CString & str);
		virtual bool UpdateElement (const CString & str);

		void DisplayDebugMessage (const CString & str);
		CString GetSerialData ();

	//	void SetImagingMode (IMAGINGMODE mode) { m_nImgMode = mode; }
	//	IMAGINGMODE GetImagingMode () const { return m_nImgMode; }

	//	void SetImagingModeExpire (bool bExpire) { m_bImgModeExpire = bExpire; }
	//	bool GetImagingModeExpire () const { return m_bImgModeExpire; }

		DWORD GetIdleFlags () const;
		bool SetIdleFlags (DWORD dwFlags);
		virtual DWORD GetErrorState () const;

		bool IsWaxHead () const;
		void LoadStandbySettings ();

		virtual bool SetStandbyMode (bool bStandby);
		bool GetStandbyMode () const;

		virtual void SetCounts (const CHead::CHANGECOUNTSTRUCT & count);
		virtual void FreeImgUpdDesc ();

		void GetElements (CElementPtrArray & v, int nClassID);
		void GetElements (CElementPtrArray & v);
		virtual void SetStrobe ( int Color );

		void SetLastError ( DWORD dwError );
		DWORD GetLastError ( void );
		CSize m_LabelSize;
		bool HasUserElements ( void );

		virtual bool Load (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::TASKSTRUCT & task, 
			const FoxjetDatabase::BOXSTRUCT & box, const CMap <ULONG, ULONG, ULONG, ULONG> & mapHeads);

		bool GetSerialPending () const;
		virtual afx_msg void OnProcessSerialData (WPARAM wParam, LPARAM lParam);

		virtual BOOL InitInstance();
		virtual int ExitInstance();

		int GetStandbyTimeout () const { return m_nStandbyTimeout; }

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CHead)
		//}}AFX_VIRTUAL

	protected:
		virtual void Shutdown (void);

	// Implementation
	protected:
		virtual void SetSerialPending (bool bPending);
		virtual void CheckState ();
		virtual void Disconnect (bool bShutdown = false);


	#ifndef __IPPRINTER__
		int ProcessSerialElements (FoxjetCommon::BUILDTYPE imgtype);
		int ProcessVariableElements (FoxjetCommon::BUILDTYPE imgtype);
		int ProcessAutomationElements (FoxjetCommon::BUILDTYPE imgtype);
		int ProcessDatabaseElements (FoxjetCommon::BUILDTYPE imgtype);

	#endif
		DWORD m_dwLastError;
		CElementPtrArray m_vSerialElements;
		CElementPtrArray m_vUserElements;
		CElementPtrArray m_vVarElements;
		CElementPtrArray m_vLabelElements;
		CElementPtrArray m_vDatabaseElements;

		std::vector <std::wstring> m_vRstCacheKey;

		int BuildDynamicElemList ( FoxjetCommon::CElementList *pElementList );
		void FreeDynamicElemList ();
		virtual int BuildImage (FoxjetCommon::BUILDTYPE imgtype, bool bOverrideImagingFlag = false);
		int BuildTestImage ();
		CHead * GetHeadByUID (const CString & strUID); 

		HANDLE m_hExit;
		PUCHAR m_pImageBuffer;
		FoxjetCommon::CElementList *m_pElementList;
		CString sFriendlyName;
		CKeyParse KeyParser;
		CString sTrace;
		CPtrList m_listImgUpdDesc;
		bool m_bTestPattern;
		CTime m_tmLastPrint;
		int m_nStandbyTimeout;
		bool m_bStandby;
		bool m_bModifiedAfterLoad;
		DWORD m_dwLastState;
		//CDiagnosticCriticalSection m_csConfig;
		CString m_strKeyValue;
		TCHAR m_sz0864 [30][256]; // sw0864

		// Generated message map functions
		//{{AFX_MSG(CHead)
		//}}AFX_MSG
		virtual afx_msg void OnStartTask (WPARAM wParam, LPARAM lParam);
		virtual afx_msg void OnStopTask (WPARAM wParam, LPARAM lParam);
		virtual afx_msg void OnIdleTask (WPARAM wParam, LPARAM lParam);
		virtual afx_msg void OnResumeTask (WPARAM wParam, LPARAM lParam);
		virtual afx_msg void OnHeadConfigChanged (WPARAM wParam, LPARAM lParam);
		virtual afx_msg void OnSetIgnorePhotocell (WPARAM wParam, LPARAM lParam);
		virtual afx_msg void OnBuildPreviewImage (WPARAM wParam, LPARAM lParam);
		virtual afx_msg void OnKillThread (WPARAM wParam, LPARAM lParam);	
		virtual afx_msg void OnUpdateCounts (WPARAM wParam, LPARAM lParam);	
		virtual afx_msg void OnGetInfo (WPARAM wParam, LPARAM lParam);
		virtual afx_msg void OnIncrementErrorIndex (WPARAM wParam, LPARAM lParam);
		virtual afx_msg void OnSetKeyValue (UINT wParam, LONG lParam);
		virtual afx_msg void OnSetExpSpan (UINT wParam, LONG lParam);

		DECLARE_MESSAGE_MAP()

	protected:

		class CCache
		{
		public:
			CCache () { }
			virtual ~CCache () { }

			FoxjetCommon::CElementList m_list;

		private:
			CCache (const CCache & rhs);
			CCache & operator = (const CCache & rhs);
		};

		CSyncMap <ULONG, ULONG, CCache *, CCache *> m_vCache;
		CCache m_defCache;

		IMAGINGMODE						m_nImgMode;
		bool							m_bImgModeExpire;
		DWORD							m_dwIdleFlags;
		bool							m_bSerialPending;
		LARGE_INTEGER					m_freq;
		//CDiagnosticCriticalSection		m_csImage;
		CTime							m_tmImage;
		bool							m_bSQLServer;
		FoxjetDatabase::COdbcDatabase * m_pdb;
		UINT							m_nDbTimeout;
		CTime							m_tmDatabase;

	#ifdef _DEBUG
	//#ifdef __SW0875__
		CString m_strDebugCount;
	#endif

	#ifdef _DEBUG
		DWORD m_dwSimulatedError;
	#endif

		void Cache (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::TASKSTRUCT & task, const FoxjetDatabase::BOXSTRUCT & box);
		void FreeCache ();
		void UpdateStatusMsg (const CString & str = _T (""));
		CString GetStatusMsg () const;
	};
}; // namespace Head

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEAD_H__D127FEA8_C5C7_4E80_A5F3_9B68BD932B0E__INCLUDED_)
