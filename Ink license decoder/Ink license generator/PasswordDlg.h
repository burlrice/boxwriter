#pragma once

#include "resource.h"
#include "maskededit_src\OXMaskedEdit.h"


// CPasswordDlg dialog

class CPasswordDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CPasswordDlg)

public:
	CPasswordDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPasswordDlg();

// Dialog Data
	enum { IDD = IDD_PASSWORDS };

protected:
	FoxjetUtils::CInkCodeEdit m_txt;
	CFont m_fnt;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedGenerate();
	afx_msg void OnDtnDatetimechangeExp(NMHDR *pNMHDR, LRESULT *pResult);
	CDateTimeCtrl m_dt;
	afx_msg void OnBnClickedGenerateDisable();
};
