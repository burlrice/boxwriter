
// InkDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Ink license generator.h"
#include "InkDlg.h"
#include "afxdialogex.h"
#include "zlib.h"
#include <math.h>
#include <string>
#include <vector>
#include "Base32.h"
#include "Registry.h"
#include "TemplExt.h"
#include "GenerateListDlg.h"
#include "PasswordDlg.h"

#undef OCSP_REQUEST
#undef X509_NAME
#undef OCSP_RESPONSE
#include <openssl/des.h>	


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

static const BYTE nPrivateKeyScrambled [] = 
{
	0xD3, 0x11, 0x28, 0x09, 0x02, 0x8A, 0x71, 0x83, 	0x1F, 0x26, 0x96, 0x09, 0xE8, 0x3A, 0xE7, 0xD0, 
	0x9B, 0x14, 0x0F, 0x18, 0x50, 0x91, 0x25, 0x09, 	0xC1, 0x58, 0x32, 0x9D, 0x6B, 0x46, 0x72, 0xDB, 
	0x2F, 0xE1, 0x37, 0x89, 0x7C, 0xB5, 0x6D, 0x65, 	0xFA, 0x2C, 0xC1, 0x7A, 0x26, 0xD7, 0x95, 0x4F, 
	0xE9, 0x28, 0xB6, 0xEC, 0x5A, 0x48, 0xA6, 0x63, 	0x15, 0xA7, 0xAF, 0xA0, 0x97, 0xF6, 0x43, 0xC6, 
	0x0F, 0x39, 0xC5, 0x17, 0x46, 0xF4, 0x79, 0x5F, 	0x0C, 0x1D, 0x01, 0xE3, 0x66, 0x13, 0xAE, 0xD5, 
	0x48, 0x21, 0x6B, 0xA8, 0x47, 0xA3, 0xB5, 0xBE, 	0x1C, 0x78, 0xDB, 0x51, 0x2D, 0xD5, 0x9D, 0x3D, 
	0x44, 0xF3, 0x43, 0x99, 0x01, 0xF9, 0xB9, 0xC7, 	0x30, 0x0D, 0x42, 0xF9, 0x00, 0xE7, 0x12, 0x4D, 
	0xB8, 0x1B, 0x6C, 0xC1, 0x91, 0xC5, 0xC9, 0x79, 	0x54, 0x47, 0x72, 0x2F, 0x5C, 0xB9, 0x75, 0xD4, 
	0x01, 0x81, 0x40, 0x80, 0xF3, 0x03, 0xDF, 0x85, 	0x9F, 0x62, 0x2A, 0x4E, 0xF9, 0x3C, 0x87, 0x34, 
	0x48, 0xB7, 0x59, 0x56, 0xD5, 0x0B, 0xF2, 0x33, 	0x1A, 0x5F, 0x4E, 0xE7, 0xB8, 0xD1, 0xD5, 0x96, 
	0x22, 0xAF, 0x79, 0xDB, 0x07, 0xE4, 0x1F, 0xEB, 	0x66, 0x53, 0x1E, 0x3F, 0x19, 0x6B, 0x71, 0xC3, 
	0xFF, 0x8F, 0x86, 0x09, 0x1D, 0x43, 0xD6, 0x9F, 	0x1D, 0xB6, 0xF0, 0x6C, 0xC0, 0xCF, 0xCA, 0x1A, 
	0x56, 0x38, 0x62, 0x9E, 0x42, 0x1C, 0x8A, 0x45, 	0xA9, 0x09, 0x22, 0x9A, 0xC3, 0x6A, 0x3E, 0xCF, 
	0xC3, 0xD7, 0x99, 0xBF, 0x97, 0x15, 0x49, 0xBE, 	0x65, 0xEB, 0x03, 0xCA, 0x9E, 0x1A, 0x41, 0x7B, 
	0x8B, 0xB7, 0xE8, 0xC4, 0x6D, 0x09, 0x31, 0xDE, 	0xCC, 0xC0, 0xC2, 0xF7, 0xAE, 0xC1, 0x39, 0xA7, 
	0x8D, 0x06, 0xCF, 0xAF, 0x45, 0xEE, 0xC9, 0x25, 	0x98, 0xD6, 0xCD, 0x2F, 0x4C, 0xAE, 0x5C, 0xAF, 
	0xE9, 0xCD, 0x46, 0x01, 0x81, 0x40, 0x87, 0xA0, 	0xD1, 0xF4, 0x89, 0x08, 0x40, 0x32, 0x4E, 0xBE, 
	0x19, 0x37, 0xDC, 0x22, 0x91, 0xE4, 0xBF, 0xF1, 	0x2B, 0xE3, 0x36, 0xFB, 0x5B, 0x1A, 0x1A, 0x34, 
	0xE9, 0x91, 0xCD, 0x6B, 0x05, 0x42, 0x88, 0x79, 	0x86, 0x43, 0x9D, 0x74, 0xF9, 0xBA, 0xB4, 0x8D, 
	0xF9, 0x2E, 0xE0, 0x6C, 0xCA, 0xEB, 0x22, 0xA3, 	0x4C, 0xB6, 0x6D, 0xD3, 0x4A, 0x1B, 0xA7, 0x9A, 
	0xC5, 0x74, 0x9A, 0x86, 0x19, 0xE7, 0xC8, 0xB7, 	0x12, 0x36, 0xC5, 0x06, 0xE7, 0xFA, 0xFD, 0xC5, 
	0x06, 0x56, 0xD9, 0x17, 0x53, 0x28, 0xD5, 0xAB, 	0x36, 0x7E, 0xD7, 0xCC, 0x88, 0x6A, 0xB2, 0xE9, 
	0x30, 0x28, 0x92, 0xB8, 0xED, 0xE0, 0xD4, 0xAD, 	0xDE, 0x14, 0x58, 0xA9, 0x6E, 0x5A, 0xA5, 0x0F, 
	0xB2, 0xA0, 0xC3, 0x81, 0x5C, 0xBB, 0xF1, 0x4B, 	0xF8, 0xFA, 0x19, 0x10, 0x66, 0x8F, 0x8F, 0x0F, 
	0x63, 0xFE, 0x88, 0x58, 0x3E, 0xB6, 0x01, 0x81, 	0x40, 0xC4, 0x5A, 0x99, 0x03, 0xB8, 0x12, 0xC2, 
	0x13, 0xE7, 0x37, 0xB4, 0xAD, 0xEB, 0x11, 0xBE, 	0x7F, 0x8C, 0xA8, 0x39, 0x71, 0xD3, 0x25, 0x1F, 
	0xAE, 0xA5, 0xD2, 0xF3, 0xD6, 0x7D, 0x17, 0x21, 	0x49, 0x01, 0x92, 0x73, 0x2C, 0x0E, 0xBE, 0x77, 
	0x40, 0xFF, 0x1C, 0x45, 0x9C, 0xAE, 0xD9, 0x22, 	0xEA, 0xEC, 0x52, 0x1F, 0x06, 0xD5, 0xE7, 0x98, 
	0xF7, 0xD2, 0x0F, 0xA9, 0xD1, 0xF4, 0xFA, 0x4D, 	0x0E, 0x8C, 0x13, 0xE5, 0xBC, 0x16, 0xAA, 0x6B, 
	0xE0, 0xE8, 0x2C, 0x77, 0xDB, 0xFA, 0xAF, 0x5B, 	0x36, 0xEE, 0xDD, 0x56, 0x08, 0x38, 0x75, 0x95, 
	0xB9, 0xAE, 0x72, 0xB5, 0x90, 0x06, 0xDA, 0x3F, 	0x5E, 0xF7, 0x92, 0xDA, 0xEA, 0x32, 0xA5, 0xC2, 
	0x69, 0x8F, 0xB2, 0xFC, 0xA3, 0xF4, 0xBF, 0x4A, 	0xDC, 0xE4, 0xE6, 0xB2, 0xD5, 0xDB, 0x05, 0x4D, 
	0x3A, 0x20, 0x0C, 0xF6, 0x30, 0x65, 0xFA, 0x79, 	0x47, 0x00, 0x81, 0x81, 0x40, 0x85, 0x2E, 0xE7, 
	0x10, 0xD8, 0xC8, 0x44, 0x87, 0x49, 0xE3, 0x1E, 	0x0C, 0xDA, 0xB4, 0x26, 0xF0, 0xC2, 0x80, 0xAA, 
	0xFF, 0x23, 0x08, 0x70, 0xE0, 0xA9, 0xBB, 0x59, 	0xD1, 0x32, 0xF9, 0x2A, 0x9E, 0x11, 0x44, 0x37, 
	0x85, 0x60, 0x5E, 0x5E, 0xF0, 0xEA, 0xE9, 0x61, 	0x41, 0x04, 0xB1, 0x80, 0x33, 0x5F, 0x2A, 0x05, 
	0x6F, 0x1F, 0x1D, 0x1A, 0xCC, 0xB1, 0x61, 0x5F, 	0x68, 0xF7, 0x97, 0x3C, 0xAE, 0x5F, 0x4C, 0x49, 
	0x3F, 0x16, 0xAF, 0xB9, 0x35, 0x69, 0x4D, 0xE8, 	0x9A, 0x40, 0x69, 0x42, 0x02, 0xB5, 0x28, 0x3C, 
	0x99, 0xD5, 0x37, 0xD7, 0xEC, 0xC8, 0x8A, 0x1B, 	0x5F, 0x72, 0x8D, 0x7F, 0x7F, 0x3A, 0xD7, 0x81, 
	0xFA, 0x49, 0x36, 0x69, 0xAE, 0xE5, 0x57, 0x6F, 	0x8C, 0x04, 0x1F, 0x59, 0x54, 0xDC, 0xDA, 0xD4, 
	0xD7, 0x9C, 0x7B, 0x61, 0x13, 0x42, 0x26, 0x08, 	0xA9, 0x47, 0x8B, 0x28, 0x8F, 0x00, 0x81, 0x81, 
	0x40, 0x83, 0xFB, 0x20, 0xE1, 0x19, 0x47, 0x44, 	0xB8, 0xA0, 0x55, 0xC6, 0xC7, 0x73, 0xBE, 0xA0, 
	0xBF, 0xCD, 0x84, 0x96, 0xE5, 0x9E, 0x0B, 0x3B, 	0xC9, 0xF5, 0x97, 0x81, 0x2A, 0xB3, 0x39, 0x07, 
	0xA6, 0x5E, 0x5E, 0x64, 0xB7, 0xE9, 0xC1, 0xB6, 	0x50, 0x05, 0xE0, 0x4C, 0x61, 0x3A, 0x6F, 0xBE, 
	0x15, 0x90, 0x49, 0x6A, 0x46, 0x05, 0xBD, 0xA0, 	0xD5, 0x74, 0x73, 0xD0, 0xA5, 0xF8, 0xDE, 0x9A, 
	0x74, 0x21, 0x3D, 0xA2, 0x67, 0x6A, 0x6A, 0x8C, 	0x92, 0x7D, 0x52, 0x11, 0x2A, 0x04, 0xF7, 0xC6, 
	0xCD, 0x1E, 0xE0, 0x37, 0x64, 0xD9, 0x48, 0xC1, 	0x97, 0xC6, 0xBA, 0x19, 0xF7, 0x35, 0x76, 0x9F, 
	0x0E, 0x82, 0x3F, 0x49, 0xF8, 0x43, 0xF2, 0x20, 	0xEA, 0x66, 0x84, 0xBD, 0xAB, 0xEB, 0xD5, 0xAB, 
	0x87, 0xFB, 0x47, 0x1E, 0x44, 0xFA, 0xBC, 0xA9, 	0x6F, 0xFC, 0x45, 0x7D, 0xB6, 0x02, 0x43, 0xD7, 
	0x04, 0x88, 0x1C, 0x8B, 0xF6, 0xBB, 0x80, 0xE3, 	0x57, 0x8C, 0x8F, 0x87, 0x8E, 0x27, 0x89, 0x3E, 
	0xDC, 0xD3, 0xB5, 0xD1, 0x32, 0xCA, 0x2B, 0x9B, 	0x90, 0x33, 0xBE, 0x59, 0x03, 0x0E, 0x8B, 0x21, 
	0x2E, 0xE3, 0xFC, 0xD5, 0x8A, 0x74, 0xB1, 0x05, 	0x33, 0xA3, 0x25, 0x6E, 0x67, 0xCF, 0x28, 0xEA, 
	0x96, 0xC0, 0x5C, 0x67, 0xEC, 0xFE, 0xDC, 0xEC, 	0xE9, 0xFD, 0x9B, 0x27, 0x02, 0xA6, 0x4D, 0x19, 
	0x52, 0xE0, 0xE5, 0xDF, 0x5E, 0x75, 0x4E, 0xD6, 	0x2B, 0xF4, 0x3E, 0x20, 0xC5, 0xED, 0x30, 0xCF, 
	0xD5, 0x01, 0xF0, 0xB2, 0xEB, 0x39, 0x58, 0x3D, 	0xB4, 0x04, 0x16, 0xDD, 0x57, 0x5D, 0xDF, 0xD0, 
	0x01, 0x3B, 0x27, 0x6E, 0x6A, 0xF9, 0xFC, 0x24, 	0x55, 0xB6, 0xCB, 0xED, 0xDA, 0x9E, 0x26, 0x97, 
	0x0E, 0xEB, 0x23, 0xA9, 0x94, 0xB3, 0xAC, 0x4C, 	0xFE, 0x70, 0x48, 0x08, 0x26, 0xC8, 0x0B, 0x4E, 
	0xC0, 0x00, 0x80, 0x41, 0x40, 0x80, 0x00, 0x80, 	0xC0, 0x40, 0xC0, 0x31, 0x7B, 0x50, 0x7B, 0xA1, 
	0x7A, 0xBB, 0xAE, 0xB9, 0x7C, 0x9E, 0xC4, 0x84, 	0x00, 0xB6, 0xA1, 0xFF, 0x22, 0x12, 0x8C, 0xD3, 
	0xA6, 0x66, 0x12, 0xA8, 0x68, 0xA9, 0x37, 0x33, 	0x29, 0x07, 0xA2, 0x1D, 0x60, 0xDD, 0x88, 0x12, 
	0x8A, 0x46, 0xEC, 0xD1, 0x1E, 0x15, 0xAC, 0x69, 	0xB5, 0xDE, 0xB9, 0xCF, 0x9C, 0xD5, 0xC7, 0x7D, 
	0xAD, 0xCC, 0x11, 0x70, 0x79, 0xD5, 0x06, 0x76, 	0x20, 0x36, 0xBA, 0xA2, 0xEC, 0x2D, 0xBB, 0xB4, 
	0x2E, 0x29, 0x95, 0xBE, 0x1C, 0xF0, 0x9B, 0xCA, 	0xD5, 0xAE, 0x5E, 0x19, 0xFA, 0x0B, 0x2C, 0xB5, 
	0xD9, 0x68, 0xE7, 0x56, 0xED, 0x9F, 0x29, 0x5D, 	0x1D, 0xFB, 0x7D, 0x89, 0xF1, 0xAC, 0x41, 0xD2, 
	0x44, 0x06, 0xCC, 0xEF, 0xDA, 0x06, 0x60, 0xD9, 	0x77, 0x7B, 0xB4, 0x31, 0xD6, 0x54, 0x48, 0x9C, 
	0xD4, 0x56, 0xF4, 0xBB, 0x5F, 0xE6, 0xEC, 0x8D, 	0xE7, 0x82, 0x93, 0x06, 0x61, 0x2A, 0xDF, 0x6C, 
	0xD3, 0x30, 0x36, 0xFD, 0x2B, 0x8F, 0x51, 0xEF, 	0xD5, 0x42, 0x3C, 0x0A, 0x9F, 0x8F, 0x49, 0xD3, 
	0x82, 0x5B, 0xAB, 0x8A, 0x3B, 0xA0, 0x8F, 0xF7, 	0xE6, 0x11, 0x10, 0xDD, 0xF4, 0x23, 0x5E, 0x82, 
	0x02, 0x0E, 0x4E, 0x11, 0x1A, 0xFD, 0x88, 0x65, 	0x2D, 0x8B, 0xB8, 0x1D, 0x0A, 0x31, 0xD7, 0xE9, 
	0x6B, 0x96, 0xD2, 0xCF, 0x4B, 0x65, 0x11, 0x54, 	0x5A, 0x1B, 0xD7, 0x6E, 0xBE, 0x95, 0xF5, 0xBE, 
	0x63, 0x63, 0xEB, 0x38, 0xF9, 0xF1, 0x71, 0x6C, 	0xC4, 0xB4, 0xCE, 0xA8, 0xE1, 0x17, 0x1D, 0xE0, 
	0xCF, 0x96, 0x45, 0x49, 0xFB, 0x4B, 0x59, 0x46, 	0xA1, 0x9C, 0xD5, 0xAA, 0xDB, 0x09, 0xAD, 0xB5, 
	0xE0, 0x75, 0x6A, 0xBE, 0xAB, 0x28, 0x97, 0x2B, 	0x66, 0x27, 0x0D, 0x7B, 0x38, 0xB9, 0xCB, 0x3E, 
	0x92, 0xEC, 0x2F, 0xAA, 0x91, 0x55, 0x6C, 0x61, 	0x96, 0xAB, 0x00, 0x80, 0x80, 0x41, 0x40, 0x00, 
	0x80, 0x40, 0x45, 0x20, 0x41, 0x0C, 
};

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

#if 0

	bool IsLeapYear (int nYear)
	{
		/*
		if (year is not divisible by 4) then (it is a common year)
		else if (year is not divisible by 100) then (it is a leap year)
		else if (year is not divisible by 400) then (it is a common year)
		else (it is a leap year)
		*/

		if ((nYear % 4) != 0) 
			return false;
		else if ((nYear % 100) != 0) 
			return true;
		else if ((nYear % 400) != 0) 
			return true;
		
		return true;
	}

	int GetDaysInMonth (int nMonth, int nYear)
	{
		int n [12] = 
		{
			31,
			!IsLeapYear (nYear) ? 28 : 29,
			31,
			30,
			31,
			30,
			31,
			31,
			30,
			31,
			30,
			31,
		};

		ASSERT (nMonth >= 1 && nMonth <= 12);

		return n [nMonth - 1];
	}



template <class T>
T sum (const std::vector <T> & v)
{
	T result = 0;

	for (UINT i = 0; i < v.size (); i++)
		result += v [i];

	return result;
}

template <class T>
T _min (const std::vector <T> & v)
{
	T result = v.size () ? v [0] : 0;

	for (UINT i = 0; i < v.size (); i++)
		result = min (result, v [i]);

	return result;
}

template <class T>
T _max (const std::vector <T> & v)
{
	T result = 0;

	for (UINT i = 0; i < v.size (); i++)
		result = max (result, v [i]);

	return result;
}

template <class T>
T avg (const std::vector <T> & v)
{
	return (T)sum (v) / (T)v.size ();
}
#endif

/*
static unsigned __int64 BreakCRC (const CInkCode & test)
{
	unsigned __int64 n = 0;
	unsigned __int64 lCount = 0;

	for (unsigned __int32 nManufacturer = 0; nManufacturer < (unsigned __int32)pow (2.0, (double)test.m_range.m_nManufacturer); nManufacturer++) {
		for (unsigned __int32 nDay = 0; nDay < (unsigned __int32)pow (2.0, (double)test.m_range.m_nDay); nDay++) {
			for (unsigned __int32 nSerialNumber = 0; nSerialNumber < (unsigned __int32)pow (2.0, (double)test.m_range.m_nSerialNumber); nSerialNumber++) {
				for (unsigned __int32 nExpPeriod = 0; nExpPeriod < (unsigned __int32)pow (2.0, (double)test.m_range.m_nExpPeriod); nExpPeriod++) {
					CInkCode c;

					c.SetManufacturer (nManufacturer);
					c.SetDay (nDay);
					c.SetSerialNumber (nSerialNumber);
					c.SetExpPeriod (nExpPeriod);
					VERIFY (c.SetCRC ());
					lCount++;

					if (test.GetCRC () == c.GetCRC ()) {
						bool bSelf = (test.Encode () == c.Encode ()) ? true : false;

						if (!bSelf) {
							n++;
							TRACEF (_T ("\t\t") + CString (bSelf ? _T ("[SELF] ") : _T ("       ")) + ToString (n) + _T (" [of ") + ToString (lCount) + _T ("]: ") + test.ToString () + _T (" [") + test.Encode () + _T ("] == ") + c.ToString () + _T (" [") + c.Encode () + _T ("]"));
						}
					}
				}
			}
		}
	}

	double nPct = (int)((double)n / (double)lCount * 100.0);
	TRACEF (ToString (n) + _T (" / ") + ToString (lCount) + _T (" = ") + ToString (nPct) + _T (" %"));

	return n;
}
*/

/*
	{
		unsigned __int64 nCollision = 0;
		unsigned __int64 lCount = 0;
		CInkCode c;

		TRACEF (c.ToDebugString ());

		/*
		for (unsigned __int32 nManufacturer = 0; nManufacturer < (unsigned __int32)pow (2.0, (double)c.m_range.m_nManufacturer); nManufacturer++) {
			for (unsigned __int32 nDay = 0; nDay < (unsigned __int32)pow (2.0, (double)c.m_range.m_nDay); nDay++) {
				for (unsigned __int32 nSerialNumber = 0; nSerialNumber < (unsigned __int32)pow (2.0, (double)c.m_range.m_nSerialNumber); nSerialNumber++) {
					for (unsigned __int32 nExpPeriod = 0; nExpPeriod < (unsigned __int32)pow (2.0, (double)c.m_range.m_nExpPeriod); nExpPeriod++) {
						c.SetManufacturer (nManufacturer);
						c.SetDay (nDay);
						c.SetSerialNumber (nSerialNumber);
						c.SetExpPeriod (nExpPeriod);
						VERIFY (c.SetCRC ());
						lCount++;

						TRACEF (c.ToString () + _T (": calculated CRC: ") + ToString (c.GetCRC ()) + _T (", testing: 0 to ") + ToString ((unsigned __int32)pow (2.0, (double)c.m_range.m_nCRC) - 1));

						nCollision += BreakCRC (c);
					}
				}
			}
		}

		TRACEF (_T ("lCount: ") + ToString (lCount));
		TRACEF (_T ("nCollision: ") + ToString (nCollision));
		int nBreak = 0;
		* /

		{
			unsigned __int64 lCount = 0;
			const CInkCode c;


			for (unsigned __int32 nManufacturer = 0; nManufacturer < (unsigned __int32)pow (2.0, (double)c.m_range.m_nManufacturer); nManufacturer++) {
				for (unsigned __int32 nDay = 0; nDay < (unsigned __int32)pow (2.0, (double)c.m_range.m_nDay); nDay++) {
					for (unsigned __int32 nSerialNumber = 0; nSerialNumber < (unsigned __int32)pow (2.0, (double)c.m_range.m_nSerialNumber); nSerialNumber++) {
						for (unsigned __int32 nExpPeriod = 0; nExpPeriod < (unsigned __int32)pow (2.0, (double)c.m_range.m_nExpPeriod); nExpPeriod++) {

			//lCount = 16383999;
			// 26656767

			//for (unsigned __int32 nManufacturer = 0; nManufacturer < (unsigned __int32)pow (2.0, (double)c.m_range.m_nManufacturer); nManufacturer++) {
			//	for (unsigned __int32 nDay = 3999; nDay < (unsigned __int32)pow (2.0, (double)c.m_range.m_nDay); nDay++) {
			//		for (unsigned __int32 nSerialNumber = 0; nSerialNumber < (unsigned __int32)pow (2.0, (double)c.m_range.m_nSerialNumber); nSerialNumber++) {
			//			for (unsigned __int32 nExpPeriod = 0; nExpPeriod < (unsigned __int32)pow (2.0, (double)c.m_range.m_nExpPeriod); nExpPeriod++) {
							CInkCode a, b;

							a.SetManufacturer (nManufacturer);
							a.SetDay (nDay);
							a.SetSerialNumber (nSerialNumber);
							a.SetExpPeriod (nExpPeriod);
							//VERIFY (a.SetCRC (262139));
							VERIFY (a.SetCRC ());

							CString strEncoded = a.Encode ();
							ASSERT (strEncoded.GetLength () == 11);
							VERIFY (b.Decode (strEncoded));
							ASSERT (a == b);
							ASSERT (strEncoded == b.Encode ());
//							if (((lCount + 1) % 16384) == 0) 
								TRACEF (_T ("[") + ToString (strEncoded.GetLength ()) + _T ("] ") + strEncoded + _T (": ") + a.ToString () + _T ("\t") + ToHexString (lCount) + _T (" ") + ToString (lCount));
							lCount++;
						}
					}
				}
			}

			TRACEF (_T ("encoded: ") + ToString (lCount)); // 134217728
 			int nBreak = 0;
		}

		/*
		int nBytes = 8;

		for (; Base32::GetEncode32Length (nBytes) >= 9; nBytes--)
			TRACEF (_T ("nBytes: ") + ToString (nBytes) + _T (" --> ") + ToString (Base32::GetEncode32Length (nBytes)));

		TRACEF (_T ("nBytes: ") + ToString (nBytes) + _T (" --> ") + ToString (Base32::GetEncode32Length (nBytes)));

		const int nBits = 45;
		unsigned __int64 lCount = 0;

		for (unsigned __int64 i = 0; i < (unsigned __int64)pow (2.0, (double)nBits); i++) {
			CString str = ToBinString (i);
			CInkCode a, b;

			str.Replace (_T (" "), _T (""));
			str = str.Right (nBits);
			VERIFY (a.FromBinString (str));
			CString strEncoded = a.Encode ();
			ASSERT (strEncoded.GetLength () == 9);
			VERIFY (b.Decode (strEncoded));
			ASSERT (a == b);
			if (((i + 1) % 16384) == 0) TRACEF (_T ("[") + ToString (strEncoded.GetLength ()) + _T ("] ") + strEncoded + _T (": ") + a.ToString ());
			b = a;
			ASSERT (a == b);
			CString strBin = b.ToBinString ();
			ASSERT (strBin == str);
			lCount++;
		}

		TRACEF (_T ("encoded: ") + ToString (lCount));
	}
	* /
	}
*/

/*
void Test ()
{
	const CTime tm (2020, 9, 30, 0, 0, 0);
	CInkCode c;
	unsigned __int64 lCount = 0;
	int nLastValid = -1;

	for (unsigned __int32 nDay = 0; nDay < (unsigned __int32)pow (2.0, (double)c.m_range.m_nDay); nDay++) {
		c.SetManufacturer (CInkCode::MFG_FOXJET);
		c.SetDay (nDay);
		c.SetSerialNumber (0);
		c.SetExpPeriod (CInkCode::EXP_PERIOD_24);
		VERIFY (c.SetCRC ());

		int nValid = c.IsValid (&tm) ? 1 : 0;

		if (nLastValid != nValid) {
			TRACEF (CString (nValid ? _T ("  valid: ") : _T ("invalid: ")) + tm.Format (CInkCode::m_strFormat) + _T (": ") + c.ToString ());
			nLastValid = nValid;
		}

		lCount++;
	}


	TRACEF (ToString (lCount));
	lCount = 0;
	nLastValid = -1;

	c.SetManufacturer (CInkCode::MFG_FOXJET);
	c.SetDay (1368);
	c.SetSerialNumber (0);
	c.SetExpPeriod (CInkCode::EXP_PERIOD_24);
	VERIFY (c.SetCRC ());
	TRACEF (c.ToString ());

	CTime tmTest = CInkCode::m_tmBase;

	for (unsigned __int32 nDay = 0; nDay < (unsigned __int32)pow (2.0, (double)c.m_range.m_nDay); nDay++) {

		int nValid = c.IsValid (&tmTest) ? 1 : 0;

		if (nLastValid != nValid) {
			TRACEF (CString (nValid ? _T ("  valid: ") : _T ("invalid: ")) + tmTest.Format (CInkCode::m_strFormat) + _T (": ") + c.ToString ());
			nLastValid = nValid;
		}

		lCount++;
		tmTest += CTimeSpan (1, 0, 0, 0);
	}

	TRACEF (ToString (lCount));
}
*/

// CInkDlg dialog

CInkDlg::CInkDlg(CWnd* pParent /*=NULL*/)
:	m_nSerialNumber (0),
	m_tmDay (CTime::GetCurrentTime ()),
	CDialogEx(CInkDlg::IDD, pParent)
{
	{
		BYTE n [ARRAYSIZE (::nPrivateKeyScrambled)] = { 0 };

		memcpy (n, ::nPrivateKeyScrambled, ARRAYSIZE (::nPrivateKeyScrambled));
		Unscramble (n, ARRAYSIZE (n));
		FoxjetDatabase::Encrypted::RSA::SetPrivateKey (n, ARRAYSIZE (n));

		#ifdef _DEBUG
		const CString strData = _T ("1234567890-0987654321234567890-");
		const CString strEncrypted = FoxjetDatabase::Encrypted::RSA::Encrypt (strData);
		const CString strDecrypted = FoxjetDatabase::Encrypted::RSA::Decrypt (strEncrypted);

		TRACEF (strEncrypted);

		ASSERT (strData == strDecrypted);
		#endif
	}

	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CInkDlg::~CInkDlg ()
{
}

void CInkDlg::DoDataExchange(CDataExchange* pDX)
{
	static const unsigned __int32 lSerialNumberMax = (unsigned __int32)pow (2.0, (double)CInkCode::m_range.m_nSerialNumber) - 1;
	static const unsigned __int32 lDayMax = (unsigned __int32)pow (2.0, (double)CInkCode::m_range.m_nDay) - 1;
	static const CTime tmMin = CInkCode::m_tmBase;
	static const CTime tmMax = CInkCode::m_tmBase + CTimeSpan (lDayMax, 23, 23, 59);

	CDialogEx::DoDataExchange(pDX);
	
	DDX_Control (pDX, TXT_INKCODE, m_txt);

	TRACEF (tmMin.Format (_T ("[") + CInkCode::m_strFormat + _T (" - ")) + tmMax.Format (CInkCode::m_strFormat + _T ("]")));

	DDX_DateTimeCtrl (pDX, DT_DAY, m_tmDay);
	DDV_MinMaxDateTime (pDX, m_tmDay, &tmMin, &tmMax);
	DDX_Text (pDX, TXT_SERIAL, m_nSerialNumber);
	DDV_MinMaxInt (pDX, m_nSerialNumber, 0, lSerialNumberMax);
}

BEGIN_MESSAGE_MAP(CInkDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_DECODE, &CInkDlg::OnBnClickedDecode)
	ON_BN_CLICKED(BTN_ENCODE, &CInkDlg::OnBnClickedEncode)
	ON_BN_CLICKED(BTN_LIST, &CInkDlg::OnBnClickedList)
	ON_COMMAND(ID_FILE_PASSWORDS, &CInkDlg::OnFilePasswords)
END_MESSAGE_MAP()


// CInkDlg message handlers

BOOL CInkDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	if (FoxjetUtils::CInkCodeEdit  * p = (FoxjetUtils::CInkCodeEdit *)GetDlgItem (TXT_INKCODE)) 
		p->Init (this, 9);

	if (CWnd * p = GetDlgItem (LBL_DAY))
		p->ShowWindow (SW_HIDE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CInkDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CInkDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CInkDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CInkDlg::OnBnClickedDecode()
{
	if (FoxjetUtils::CInkCodeEdit  * p = (FoxjetUtils::CInkCodeEdit *)GetDlgItem (TXT_INKCODE)) {
		CString str = p->GetInputData ();
		CInkCode c;

		if (!c.Decode (str))
			MessageBox (_T ("Failed to decode"));
		else {
			int n [] = 
			{ 
				RDO_FOXJET,
				RDO_DIAGRAPH,
				RDO_EXP_12,
				RDO_EXP_18,
				RDO_EXP_24,
			};

			for (int i = 0; i < ARRAYSIZE (n); i++)
				if (CButton * p = (CButton *)GetDlgItem (n [i]))
					p->SetCheck (0);

			SetManufacturer (c.GetManufacturer ());
			m_nSerialNumber = c.GetSerialNumber ();
			SetExpPeriod (c.GetExpPeriod ());
			m_tmDay = CInkCode::m_tmBase + CTimeSpan (c.GetDay (), 0, 0, 0);

			if (CWnd * p = GetDlgItem (LBL_DAY))
				p->ShowWindow (!c.IsValid (&CTime::GetCurrentTime ()) ? SW_SHOW : SW_HIDE);

			UpdateData (FALSE);
		}
	}
}

int CInkDlg::GetManufacturer () const
{
	if (CButton * p = (CButton *)GetDlgItem (RDO_FOXJET))
		if (p->GetCheck () == 1)
			return CInkCode::MFG_FOXJET;

	if (CButton * p = (CButton *)GetDlgItem (RDO_DIAGRAPH))
		if (p->GetCheck () == 1)
			return CInkCode::MFG_DIAGRAPH;

	return CInkCode::MFG_FOXJET;
}

void CInkDlg::SetManufacturer (int n)
{
	if (CButton * p = (CButton *)GetDlgItem (RDO_FOXJET))
		p->SetCheck (n == CInkCode::MFG_FOXJET ? 1 : 0);

	if (CButton * p = (CButton *)GetDlgItem (RDO_DIAGRAPH))
		p->SetCheck (n == CInkCode::MFG_DIAGRAPH ? 1 : 0);
}

int CInkDlg::GetExpPeriod () const
{
	if (CButton * p = (CButton *)GetDlgItem (RDO_EXP_12))
		if (p->GetCheck () == 1)
			return CInkCode::EXP_PERIOD_12;

	if (CButton * p = (CButton *)GetDlgItem (RDO_EXP_18))
		if (p->GetCheck () == 1)
			return CInkCode::EXP_PERIOD_18;

	if (CButton * p = (CButton *)GetDlgItem (RDO_EXP_24))
		if (p->GetCheck () == 1)
			return CInkCode::EXP_PERIOD_24;

	return CInkCode::EXP_PERIOD_12;
}

void CInkDlg::SetExpPeriod (int n)
{
	if (CButton * p = (CButton *)GetDlgItem (RDO_EXP_12))
		p->SetCheck (n == CInkCode::EXP_PERIOD_12? 1 : 0);

	if (CButton * p = (CButton *)GetDlgItem (RDO_EXP_18))
		p->SetCheck (n == CInkCode::EXP_PERIOD_18 ? 1 : 0);

	if (CButton * p = (CButton *)GetDlgItem (RDO_EXP_24))
		p->SetCheck (n == CInkCode::EXP_PERIOD_24 ? 1 : 0);
}


void CInkDlg::OnBnClickedEncode()
{
	UpdateData (TRUE);

	CInkCode c;
	CTimeSpan tm = m_tmDay - CInkCode::m_tmBase;
	bool bSet = true;

	bSet &= c.SetManufacturer (GetManufacturer ());
	bSet &= c.SetDay (tm.GetDays ());
	bSet &= c.SetSerialNumber (m_nSerialNumber);
	bSet &= c.SetExpPeriod (GetExpPeriod ());

	if (bSet) {
		CString str = c.Encode ();

		SetDlgItemText (TXT_INKCODE, str);

		if (CWnd * p = GetDlgItem (LBL_DAY))
			p->ShowWindow (!c.IsValid (&CTime::GetCurrentTime ()) ? SW_SHOW : SW_HIDE);

		if (::OpenClipboard (NULL)) {
			::EmptyClipboard ();
			int nLen = str.GetLength () + 1;

			#ifdef _UNICODE
			nLen = (str.GetLength () * 2) + 1;
			#endif
	
			HGLOBAL hClipboardText = ::GlobalAlloc (GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT, nLen);
			LPVOID lpCbTextData = ::GlobalLock (hClipboardText);
			memcpy (lpCbTextData, (LPCTSTR)str, nLen);

			if (::SetClipboardData (CF_UNICODETEXT, hClipboardText) == NULL)
				TRACEF (_T ("SetClipboardData failed"));

			if (hClipboardText) {
				::GlobalUnlock (hClipboardText);
				hClipboardText = NULL;
			}

			::CloseClipboard ();
		}
	}
}

void CInkDlg::OnBnClickedList()
{
	CInkCode c;
	CGenerateListDlg dlg (this);

	UpdateData (TRUE);

	CTimeSpan tm = m_tmDay - CInkCode::m_tmBase;

	dlg.m_nManufacturer	= GetManufacturer ();
	dlg.m_nDay			= tm.GetDays ();
	dlg.m_nExpPeriod	= GetExpPeriod ();

	if (dlg.DoModal () == IDOK) {
	}
}


void CInkDlg::OnFilePasswords()
{
	CPasswordDlg dlg (this);

	if (dlg.DoModal () == IDOK) {
	}
}
