// GenerateListDlg.cpp : implementation file
//

#include "stdafx.h"
#include <Winspool.h>
#include <shlwapi.h>
#include "Ink license generator.h"
#include "GenerateListDlg.h"
#include "afxdialogex.h"
#include "Parse.h"
#include "TemplExt.h"
#include "Debug.h"
#include <vector>
#include <string>
#include <array>
#include "resource.h"
#include "OdbcDatabase.h"
#include "ODBCInst.h"
#include "ExcelFormat.h"

using namespace FoxjetDatabase;

// CGenerateListDlg dialog

IMPLEMENT_DYNAMIC(CGenerateListDlg, CDialogEx)

CGenerateListDlg::CGenerateListDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CGenerateListDlg::IDD, pParent)
{

}

CGenerateListDlg::~CGenerateListDlg()
{
}

void CGenerateListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CGenerateListDlg, CDialogEx)
	ON_BN_CLICKED(BTN_GENERATE, &CGenerateListDlg::OnBnClickedGenerate)
	ON_BN_CLICKED(BTN_SAVE, &CGenerateListDlg::OnBnClickedSave)
END_MESSAGE_MAP()


// CGenerateListDlg message handlers


void CGenerateListDlg::OnBnClickedGenerate()
{
	using namespace FoxjetDatabase;

	CString str;

	GetDlgItemText (TXT_RANGE, str);

	std::vector <std::wstring> vCodes;
	std::vector <std::wstring> v = explode (std::wstring (str), ',');
	std::vector <int> vSerial;

	for (int i = 0; i < v.size (); i++) {
		str = v [i].c_str ();

		if (str.Find (_T ("-")) != -1) {
			std::vector <std::wstring> vRange = explode (v [i], '-');

			if (vRange.size () == 2) {
				for (int j = _ttoi (vRange [0].c_str ()); j <= _ttoi (vRange [1].c_str ()); j++)
					vSerial.push_back (j);
			}
		}
		else
			vSerial.push_back (_ttoi (str));
	}

	for (int i = 0; i < vSerial.size (); i++) {
		CInkCode c;

		c.SetManufacturer (m_nManufacturer);
		c.SetDay (m_nDay);
		c.SetSerialNumber (vSerial [i]);
		c.SetExpPeriod (m_nExpPeriod);
		vCodes.push_back (std::wstring (c.Encode ()));
	}

	SetDlgItemText (TXT_CODES, implode (vCodes, std::wstring (_T ("\r\n"))).c_str ());
}


BOOL CGenerateListDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	SetDlgItemText (TXT_RANGE, _T ("0,1,2,3,100-103"));

	m_fnt.CreatePointFont (150, _T ("Courier New"));

	if (CWnd * p = GetDlgItem (TXT_CODES))
		p->SetFont (&m_fnt);

	InitPrinters ();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

std::vector <std::wstring> EnumRegistryEntries (HKEY hKeyRoot, const CString & strKey, const CString & strEntry)
{
	std::vector <std::wstring> result;
	static const int MAX_KEY_LENGTH			= 255;
	TCHAR    szClass [MAX_PATH]				= { 0 };
    DWORD    dwClassName					= MAX_PATH; 
    DWORD    dwSubKeys						= 0;
    DWORD    dwMaxSubKey					= 0;
	DWORD    dwMaxClass						= 0;
    DWORD    dwValues						= 0;
    DWORD    dwMaxValue						= 0;
    DWORD    dwMaxValueData					= 0;
	HKEY hKey								= NULL;

	if (::RegOpenKeyEx (hKeyRoot, strKey, 0, KEY_READ, &hKey) == ERROR_SUCCESS) {
		LSTATUS retCode = ::RegQueryInfoKey (hKey, szClass, &dwClassName, NULL, &dwSubKeys, &dwMaxSubKey, &dwMaxClass, &dwValues, &dwMaxValue, &dwMaxValueData, NULL, NULL);

		//TRACEF (strKey);
	
		for (int i = 0; i < dwSubKeys; i++) { 
			TCHAR szKey [MAX_KEY_LENGTH]	= { 0 };
			DWORD dwName					= MAX_KEY_LENGTH;
		
			retCode = RegEnumKeyEx (hKey, i, szKey, &dwName, NULL, NULL, NULL, NULL);
		
			if (retCode == ERROR_SUCCESS) {
				//TRACEF (szKey);

				std::vector <std::wstring> v = EnumRegistryEntries (hKeyRoot, strKey + _T ("\\") + CString (szKey), strEntry);

				for (int i = 0; i < v.size (); i++)
					result.push_back (v [i]);			
			}
		}

		BYTE * buffer = new BYTE [dwMaxValueData];
		ZeroMemory (buffer, dwMaxValueData);

		for (int i = 0; i < dwValues; i++) { 
			TCHAR szValue [MAX_KEY_LENGTH] = { 0 }; 
			DWORD dwValue = ARRAYSIZE (szValue); 
			DWORD dwType = -1;

			retCode = ::RegEnumValue (hKey, i, szValue, &dwValue, NULL, &dwType, NULL, NULL);

			if (retCode == ERROR_SUCCESS ) { 
				DWORD lpData = dwMaxValueData;
				buffer[0] = '\0';
				LONG dwRes = RegQueryValueEx (hKey, szValue, 0, NULL, buffer, &lpData);
				//TRACEF (szValue + CString (_T (": ")) + (LPCTSTR)buffer); 

				if (strEntry == szValue) {
					CString str;

					switch (dwType) {
					case REG_DWORD:
						str = FoxjetDatabase::ToString ((int)FoxjetDatabase::GetProfileInt (hKeyRoot, strKey, strEntry, 0));
						break;
					case REG_SZ:
						str = FoxjetDatabase::GetProfileString (hKeyRoot, strKey, strEntry, _T (""));
						break;
					}

					result.push_back (std::wstring (str));
				}
			} 
		}

		delete [] buffer;
		::RegCloseKey (hKey);
	}

	return result;
}

void CGenerateListDlg::InitPrinters ()
{

	/*
	if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_PORT)) {
		DWORD dwNeeded = 0;
		DWORD dwReturned = 0;
		bool bResult = false;
		const CString strLast = FoxjetDatabase::GetProfileString (theApp.GetRegSection (), _T ("Port"), _T ("")); 

		pCB->ResetContent ();
		::EnumMonitors (NULL, 2, NULL, 0, &dwNeeded, &dwReturned);
		MONITOR_INFO_2 * p = (MONITOR_INFO_2 *)new BYTE [dwNeeded];
					
		if (!::EnumMonitors (NULL, 2, (LPBYTE)p, dwNeeded, &dwNeeded, &dwReturned))
			TRACEF (FormatMessage (::GetLastError ()));

		for (DWORD n = 0; n < dwReturned; n++) {
			const HKEY hKeyRoot = HKEY_LOCAL_MACHINE;
			const CString strRegKey = _T ("SYSTEM\\CurrentControlSet\\Control\\Print\\Monitors");
			std::vector <std::wstring> vPort = EnumRegistryEntries (hKeyRoot, strRegKey + _T ("\\") + CString (p [n].pName), _T ("PortNumber"));
			std::vector <std::wstring> vAddr = EnumRegistryEntries (hKeyRoot, strRegKey + _T ("\\") + CString (p [n].pName), _T ("IPAddress"));

			//TRACEF (p [n].pName);

			for (int i = 0; i < vAddr.size (); i++) {
				TRACEF (vAddr [i].c_str ());
			}

			for (int i = 0; i < vPort.size (); i++) {
				CString str;

				//TRACEF (ToString (v [i].c_str ()));
				str.Format (_T ("%d [%s]"), vPort [i].c_str (), p [n].pName);
				int nIndex = pCB->AddString (str);
				pCB->SetItemData (nIndex, _ttoi (vPort [i].c_str ()));
			}

			for (int i = 0; i < pCB->GetCurSel (); i++) {
				CString str;

				pCB->GetLBText (i, str);

				if (!str.CompareNoCase (strLast)) {
					pCB->SetCurSel (i);
					break;
				}
			}

			if (pCB->GetCurSel () == CB_ERR)
				pCB->SetCurSel (0);
		}

		delete p;
	}

	if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_PRINTER)) {
		DWORD dwNeeded = 0, dwReturned = 0;

		pCB->ResetContent ();
		::EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 2, NULL, 0, &dwNeeded, &dwReturned);

			//	/zpl=6101,203


			//ZDesigner 105SL 203DPI: ZDesigner 105SL 203DPI: LAN_ZPL
			//Microsoft XPS Document Writer v4: Microsoft XPS Document Writer: PORTPROMPT:
			//Microsoft Print To PDF: Microsoft Print to PDF: PORTPROMPT:
			//HP Color LaserJet CP1510 Series PCL6 Class Driver: HP Color LaserJet CP1510 Series PCL6 Class Driver UPD PCL 6: USB001
			//Foxjet Marksman NEXT: Foxjet Marksman NEXT.8: 127.0.0.1
			//Microsoft Shared Fax Driver: Fax: SHRFAX:

			//HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Print\Monitors
			//HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Print\Monitors\ZDesigner Port Monitor\Ports\LAN_ZPL

			//HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Print\Printers\ZDesigner 105SL 203DPI
			//HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Windows NT\CurrentVersion\Print\Printers\ZDesigner 105SL 203DPI

			//HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\Print\Monitors\ZDesigner Port Monitor\Ports\LAN_ZPL
			//HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\Print\Printers\ZDesigner 105SL 203DPI
			//HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Print\Monitors\ZDesigner Port Monitor\Ports\LAN_ZPL
			//HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Print\Printers\ZDesigner 105SL 203DPI

		if (dwNeeded > 0) {
			PRINTER_INFO_2 * p = (PRINTER_INFO_2 *)new BYTE [dwNeeded];

			if (::EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 2, (LPBYTE)p, dwNeeded, &dwNeeded, &dwReturned)) {
				const CString strLast = FoxjetDatabase::GetProfileString (theApp.GetRegSection (), _T ("Printer"), _T ("")); 

				for (DWORD i = 0; i < dwReturned; i++) {
					CString strDriver = p [i].pDriverName;

					TRACEF (p [i].pDriverName + CString (_T (": ")) + p [i].pPrinterName + CString (_T (": ")) + p [i].pPortName);
					strDriver.MakeLower ();

					int nIndex = pCB->AddString (p [i].pPrinterName);

					if (strDriver.Find (_T ("zdesigner")) != -1) {
						pCB->SetCurSel (nIndex);
					}
				}

				for (int i = 0; i < pCB->GetCurSel (); i++) {
					CString str;

					pCB->GetLBText (i, str);

					if (!str.CompareNoCase (strLast)) {
						pCB->SetCurSel (i);
						break;
					}

				}

				if (pCB->GetCurSel () == CB_ERR)
					pCB->SetCurSel (0);
			}

			delete [] p;
		}
	}
	*/
}

void CGenerateListDlg::OnBnClickedSave()
{
	using namespace ExcelFormat;

	CString str, strBatch, strFile = FoxjetDatabase::GetHomeDir () + _T ("\\InkCodes.xls");

	CFileDialog dlg (FALSE, _T ("*.xls"), strFile, 
		OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		_T ("Excel files (*.xls)|*.xls||"), this);

	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();

		GetDlgItemText (TXT_BATCH_NUMBER, strBatch);
		GetDlgItemText (TXT_CODES, str);
		str.Replace (_T ("\r\n"), _T ("\r"));
		std::vector <std::wstring> vCodes = explode (std::wstring (str), '\r');

		BasicExcel xls;
		XLSFormatManager fmt_mgr(xls);
		CellFormat fmtNumeric (fmt_mgr);
		CellFormat fmtText (fmt_mgr);
		fmtNumeric.set_format_string(XLS_FORMAT_DECIMAL);
		fmtText.set_format_string(XLS_FORMAT_TEXT);

		xls.New(1);
		BasicExcelWorksheet* sheet = xls.GetWorksheet(0);

		std::vector <std::vector <std::wstring> > v;
		std::vector <std::wstring> vHeader;

		vHeader.push_back (_T ("ID"));
		vHeader.push_back (_T ("Ink Code"));
		vHeader.push_back (_T ("Batchcode"));
		vHeader.push_back (_T ("Manufacturer"));
		vHeader.push_back (_T ("Date stamp"));
		vHeader.push_back (_T ("Expiration"));
		vHeader.push_back (_T ("Serial number"));
		v.push_back (vHeader);

		for (int i = 0; i < vHeader.size (); i++)
			sheet->SetColWidth (i, 5000);

		ExcelFont fnt = fmtNumeric.get_font ();

		fnt._name = _T ("Courier New");
		fmtNumeric.set_font (fnt);
		fmtText.set_font (fnt);

		for (int i = 0; i < vCodes.size (); i++) {
			std::vector <std::wstring> vRow;
			CString strCode = vCodes [i].c_str ();
			CInkCode c;
			CString strExp, strSerial;

			strCode.Insert (3, '-');
			strCode.Insert (7, '-');

			VERIFY (c.Decode (strCode));

			CTime tm = CInkCode::m_tmBase + CTimeSpan (c.GetDay (), 0, 0, 0);

			switch (c.GetExpPeriod ()) {
			case CInkCode::EXP_PERIOD_12:	strExp = _T ("12 months");	break;
			case CInkCode::EXP_PERIOD_18:	strExp = _T ("18 months");	break;
			case CInkCode::EXP_PERIOD_24:	strExp = _T ("24 months");	break;
			}

			strSerial.Format (_T ("%04d"), c.GetSerialNumber ());

			vRow.push_back (std::wstring (strCode));
			vRow.push_back (vCodes [i]);
			vRow.push_back (std::wstring (strBatch));
			vRow.push_back (std::wstring (c.GetManufacturer () == CInkCode::MFG_FOXJET ? _T ("Foxjet") : _T ("Diagraph")));
			vRow.push_back (std::wstring (tm.Format (CInkCode::m_strFormatDay)));
			vRow.push_back (std::wstring (strExp));
			vRow.push_back (std::wstring (strSerial));


			v.push_back (vRow);
		}


		for (int y = 0; y < v.size (); y++) {
			std::vector <std::wstring> vRow = v [y];

			for (int x = 0; x < vRow.size (); x++) {
				CString str = vRow [x].c_str ();

				sheet->Cell(y, x)->Set (str);
				sheet->Cell(y, x)->SetFormat (fmtText);

				//if (isnumeric (str))
				//	sheet->Cell(y, x)->SetFormat (fmtNumeric);
			}
		}

		if (!xls.SaveAs (strFile))
			MessageBox (_T ("Failed to write: ") + strFile, NULL, MB_ICONERROR);
	}
}


void CGenerateListDlg::OnBnClickedBrowse()
{
	// TODO: Add your control notification handler code here
}
