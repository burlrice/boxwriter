
// InkDlg.h : header file
//

#pragma once

#include "maskededit_src\OXMaskedEdit.h"

// CInkDlg dialog
class CInkDlg : public CDialogEx
{
// Construction
public:
	CInkDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CInkDlg ();

// Dialog Data
	enum { IDD = IDD_INKLICENSEGENERATOR_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	int GetManufacturer () const;
	void SetManufacturer (int n);

	int GetExpPeriod () const;
	void SetExpPeriod (int n);

// Implementation
protected:
	HICON m_hIcon;
	FoxjetUtils::CInkCodeEdit m_txt;

	CTime m_tmDay;
	int m_nSerialNumber;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedDecode();
	afx_msg void OnBnClickedEncode();
	afx_msg void OnBnClickedList();
	afx_msg void OnFilePasswords();
};
