// PasswordDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Ink license generator.h"
#include "PasswordDlg.h"
#include "afxdialogex.h"
#include "Database.h"
#include "Registry.h"
#include "Parse.h"
#include "TemplExt.h"

using namespace FoxjetDatabase;

// CPasswordDlg dialog

IMPLEMENT_DYNAMIC(CPasswordDlg, CDialogEx)

CPasswordDlg::CPasswordDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CPasswordDlg::IDD, pParent)
{

}

CPasswordDlg::~CPasswordDlg()
{
}

void CPasswordDlg::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control (pDX, TXT_MACADDRESS, m_txt);
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, DT_EXP, m_dt);
}


BEGIN_MESSAGE_MAP(CPasswordDlg, CDialogEx)
	ON_BN_CLICKED(BTN_GENERATE, &CPasswordDlg::OnBnClickedGenerate)
	ON_NOTIFY(DTN_DATETIMECHANGE, DT_EXP, &CPasswordDlg::OnDtnDatetimechangeExp)
	ON_BN_CLICKED(BTN_GENERATE_DISABLE, &CPasswordDlg::OnBnClickedGenerateDisable)
END_MESSAGE_MAP()


// CPasswordDlg message handlers


BOOL CPasswordDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_txt.Init (this, 10);
	m_txt.SetMask (_T ("AA:AA:AA:AA:AA:AA"));
	m_fnt.CreatePointFont (150, _T ("Courier New"));

	#ifdef _DEBUG
	std::vector <std::wstring> vMAC = GetMacAddresses ();

	if (vMAC.size ()) {
		TRACEF (vMAC [0].c_str ());
		m_txt.SetInputData (vMAC [0].c_str ());
	}
	#endif

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_ENABLE)) {
		for (int i = 1; i <= 10; i++) 
			p->SetItemData (p->AddString (FoxjetDatabase::ToString (i)), i);

		p->SetCurSel (2);
	}

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_DISABLE)) {
		for (int i = 1; i <= 20; i++) 
			p->SetItemData (p->AddString (FoxjetDatabase::ToString (i)), i);

		p->SetCurSel (9);
	}

	if (CWnd * p = GetDlgItem (TXT_ENABLE))
		p->SetFont (&m_fnt);

	if (CWnd * p = GetDlgItem (TXT_DISABLE))
		p->SetFont (&m_fnt);

	m_dt.SetFormat (_T ("M-yyyy"));
	m_dt.SetTime (&CTime::GetCurrentTime ());

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CPasswordDlg::OnBnClickedGenerate()
{
	//using namespace FoxjetDatabase::Encrypted::InkCodes;

	//int nEnable = 1;

	//if (CComboBox * p = (CComboBox *)GetDlgItem (CB_ENABLE)) 
	//	nEnable = p->GetItemData (p->GetCurSel ());

	//std::vector <std::wstring> vDisable, vEnable = CEnablePassword::Generate (m_txt.GetInputData (), nEnable);
	//SetDlgItemText (TXT_ENABLE, implode (vEnable, std::wstring (_T ("\r\n"))).c_str ());
	//OnBnClickedGenerateDisable ();
}


void CPasswordDlg::OnDtnDatetimechangeExp(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMDATETIMECHANGE pDTChange = reinterpret_cast<LPNMDATETIMECHANGE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}


void CPasswordDlg::OnBnClickedGenerateDisable()
{
/*
	CString str;
	int nDisable = 1;
	std::vector <std::wstring> vDisable;

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_DISABLE)) 
		nDisable = p->GetItemData (p->GetCurSel ());

	GetDlgItemText (TXT_ENABLE, str);
	str.Replace (_T ("\r\n"), _T ("\r"));
	std::vector <std::wstring> vEnable = explode (std::wstring (str), '\r');

	for (int i = 0; i < vEnable.size (); i++) {
		CEnablePassword enable;
		CString strTest = vEnable [i].c_str ();

		if (!enable.Decode (strTest)) {
			vDisable.push_back (std::wstring (_T ("Invalid")));
		}
		else {
			CString strEncode = enable.Encode ();
			TRACEF (strTest + _T (" ") + strEncode);
			ASSERT (strEncode == strTest);

			std::vector <std::wstring> v = CDisablePassword::Generate (enable, nDisable);

			vDisable.push_back (std::wstring (_T ("[") + enable.Encode () + _T ("]")));

			for (int j = 0; j < v.size (); j++) {
				CDisablePassword disable;
				CTime tm = CInkCode::m_tmBase;

				m_dt.GetTime (tm);
				TRACEF (tm.Format (_T ("%c")));
				disable.Decode (v [j].c_str (), enable);
				disable.SetExpPeriod (tm);

				VERIFY (disable.Decode (v [j].c_str (), enable, &tm));

				/*
				#ifdef _DEBUG
				{
					CTime tmTest = CInkCode::m_tmBase;
					int nLastValid = -1;

					for (int nDay = 0; nDay < (366 * 44); nDay++) {
						int nValid = disable.Decode (v [j].c_str (), enable, &tmTest) ? 1 : 0;

						if (nLastValid != nValid) {
							TRACEF (CString (nValid ? _T ("  valid: ") : _T ("invalid: ")) + tmTest.Format (_T ("%c")));
							nLastValid = nValid;
						}

						tmTest += CTimeSpan (1, 0, 0, 0);
					}
				}
				#endif
				* /

				vDisable.push_back (std::wstring (_T ("\t") + CString (disable.Encode ())));
			}
		}	
	}

	SetDlgItemText (TXT_DISABLE, implode (vDisable, std::wstring (_T ("\r\n"))).c_str ());
*/
}
