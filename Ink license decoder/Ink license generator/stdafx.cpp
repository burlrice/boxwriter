
// stdafx.cpp : source file that includes just the standard includes
// Ink license generator.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include <math.h>
#include <string>
#include "Base32.h"
#include "zlib.h"
#include "TemplExt.h"
#include "Database.h"



void Trace (LPCSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	Trace (a2w (lpsz), lpszFile, lLine);
}

void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	CString str;

	str.Format (_T ("%s(%d): %s\n"), lpszFile, lLine, lpsz);
	::OutputDebugString (str);
	::Sleep (1);
}

CString ToString (__int8 n)					{ return ToString ((__int64)n); }
CString ToString (__int16 n)				{ return ToString ((__int64)n); }
CString ToString (__int32 n)				{ return ToString ((__int64)n); }
CString ToString (__int64 n)				{ CString str; str.Format (_T ("%I64d"), n); return str; }
CString ToString (long n)					{ return ToString ((__int64)n); }

CString ToString (unsigned __int8 n)		{ return ToString ((__int64)n); }
CString ToString (unsigned __int16 n)		{ return ToString ((__int64)n); }
CString ToString (unsigned __int32 n)		{ return ToString ((__int64)n); }
CString ToString (unsigned __int64 n)		{ CString str; str.Format (_T ("%I64u"), n); return str; }
CString ToString (unsigned long n)			{ return ToString ((unsigned __int64)n); }

CString ToString (double d)			{ CString str; str.Format (_T ("%f"), d); return str; }
CString ToString (long double d)	{ CString str; str.Format (_T ("%Lf"), d); return str; }
CString ToString (float f)			{ CString str; str.Format (_T ("%f"), f); return str; }

ULONG CalcPrime (int nExponent)
{
	ULONG lResult = 0;
	ULONG lMax = pow (2.0, nExponent);
	ULONG lMaxSqrt = sqrt (pow (2.0, nExponent));

	for (ULONG l = 2; l <= lMax; l++) {
		bool bPrime = true;

		for (ULONG i = 2; bPrime && i <= lMaxSqrt; i++) {
			if ((l % i) == 0)
				bPrime = false;
		}

		if (bPrime) 
			lResult = l;
	}

	return lResult;
}

CString GetHomeDir ()
{
	TCHAR sz [MAX_PATH] = { 0 };

	HMODULE hModule = ::GetModuleHandle (NULL);
	::GetModuleFileName (hModule, sz, ARRAYSIZE (sz) - 1);

	const CString strPath (sz);
	CString strPathLower (sz);
	strPathLower.MakeLower ();

	VERIFY (::GetFileTitle (strPath, sz, ARRAYSIZE (sz) - 1) == 0);
	
	CString strTitle (sz);
	strTitle.MakeLower ();

	int nIndex = strPathLower.Find (strTitle);

	ASSERT (nIndex != -1);
	CString str = strPath.Left (nIndex - 1);

	return str;
}

DWORD GetFileSize (const CString & strFile)
{
	DWORD dw = 0;

    if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
		fseek (fp, 0, SEEK_END);
		dw = ftell (fp);
		fclose (fp);
	}

	return dw;
}
