#pragma once


// CGenerateListDlg dialog

class CGenerateListDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CGenerateListDlg)

public:
	CGenerateListDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGenerateListDlg();

// Dialog Data
	enum { IDD = IDD_LIST };

	int m_nManufacturer;
	int m_nDay;
	int m_nExpPeriod;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	CFont m_fnt;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedGenerate();
	virtual BOOL OnInitDialog();
	void InitPrinters(void);
	afx_msg void OnBnClickedSave();
	afx_msg void OnBnClickedBrowse();
};
