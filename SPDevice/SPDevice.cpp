// SPDevice.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include <shlwapi.h>
#include "SPDevApi.h"
#include "SPDevice.h"
#include "LicenseDlg.h"
#include "AppVer.h"
#include "Database.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

/////////////////////////////////////////////////////////////////////////////
// CSPDeviceApp

BEGIN_MESSAGE_MAP(CSPDeviceApp, CWinApp)
	//{{AFX_MSG_MAP(CSPDeviceApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSPDeviceApp construction

CSPDeviceApp::CSPDeviceApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CSPDeviceApp object

CSPDeviceApp theApp;
//extern DWORD ComputeRegCode ( DWORD dwSerial, BYTE VerMajor, BYTE VerMinor, BYTE Level );

/////////////////////////////////////////////////////////////////////////////
//
//	Function:
//		CheckLicense
//
//	Purpose:
//		Check System for present and validity of Software License Key.
//
// Parameters:
//		CWinApp *pApp, BYTE VerMajor, BYTE VerMinor
//
//	Return
//		INVALID	- License Key Present but does not match system
//		NONE		- No License Key;
//		LEVEL_1	- License Key Present and Matches system for Level 1 access.
//		
//
/////////////////////////////////////////////////////////////////////////////
SPDEVICE_API eLicenseType CheckLicense( LPCTSTR lpszAppName, PCHAR pProduct, BYTE VerMajor, BYTE VerMinor )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	CString sRegKey;
	sRegKey = theApp.GetProfileString (lpszAppName, _T ("RegKey"));
	if ( !sRegKey.IsEmpty() )
	{
		CString sCheck, sTemp;
		sCheck.Format (_T ("%s%i%02i"), pProduct, VerMajor, VerMinor);
		sTemp = sRegKey.Left ( 5 );
		if ( sCheck.CompareNoCase ( sTemp ) == 0 )
			return LEVEL_1;
		else
			return INVALID;
	}
	else
		return PromptForLicense ( lpszAppName, pProduct, VerMajor, VerMinor );
	return NONE;
}

/*
SPDEVICE_API DWORD ComputeRegCode ( DWORD dwSerial, BYTE VerMajor, BYTE VerMinor, BYTE Level )
{
	DWORD dwKey = 0x80;
	dwKey <<=8;
	dwKey |= VerMajor;
	dwKey <<=8;
	dwKey |= VerMinor;
	dwKey <<=8;
	dwKey |= Level;
	return ( dwSerial ^ dwKey );
}
*/

/////////////////////////////////////////////////////////////////////////////
//
//	Function:
//		PromptForLicense
//
//	Purpose:
//		Prompt user to enter License Key and store Key if valid.
//
// Parameters:
//		LPCTSTR lpszAppName, BYTE VerMajor, BYTE VerMinor
//
//	Return
//		INVALID	- License Key Present but does not match system of has expired
//		NONE		- No License Key;
//		LEVEL_1	- License Key Present and Matches system for Level 1 access.
//		LEVEL_2	- License Key Present and Matches system for Level 2 access.
//		
//
/////////////////////////////////////////////////////////////////////////////
SPDEVICE_API eLicenseType PromptForLicense ( LPCTSTR lpszAppName, PCHAR pProduct, BYTE VerMajor, BYTE VerMinor )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	eLicenseType value = NONE;
	CLicenseDlg Dialog;
	Dialog.m_VerMajor = VerMajor;
	Dialog.m_VerMinor = VerMinor;
	Dialog.m_sAppName = lpszAppName;
	Dialog.m_sProduct.Format (_T ("%s"), pProduct);
	if ( Dialog.DoModal() == IDOK )
		value = CheckLicense( lpszAppName, pProduct, VerMajor, VerMinor );
	return value;
}

/////////////////////////////////////////////////////////////////////////////
//
//	Function:
//		GetLicenseKey
//
//	Purpose:
//		Return License Key and Registeration date for display puproses.
//
// Parameters:
//		LPCTSTR *, DWORD &dwRegKey, CTime &RegDate
//
//	Return
//		NONE
//
/////////////////////////////////////////////////////////////////////////////
SPDEVICE_API void GetLicenseKey ( LPCTSTR lpszAppName, PCHAR pRegKey, CTime &RegDate )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	CString sRegKey;
	time_t Date_t;
	sRegKey = theApp.GetProfileString ( lpszAppName, _T ("RegKey"));
	Date_t = theApp.GetProfileInt (lpszAppName, _T ("Activation"), 0);
	strcpy ( pRegKey, w2a (sRegKey));
		
	if ( !sRegKey.IsEmpty() )
	{
		strcpy ( pRegKey, w2a (sRegKey));
		CTime Date ( Date_t );
		RegDate = Date;
	}
	else
		RegDate = CTime::GetCurrentTime();
}


BOOL CSPDeviceApp::InitInstance() 
{
	SetRegistryKey (_T ("FoxJet"));
	return CWinApp::InitInstance();
}

void CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer)
{
	if (lpVer) 
		* lpVer = FoxjetCommon::verApp; // memcpy (lpVer, &FoxjetCommon::verApp, sizeof (FoxjetCommon::VERSIONSTRUCT));
}

HRESULT CALLBACK DllGetVersion (DLLVERSIONINFO * pdvi)
{
	FoxjetCommon::VERSIONSTRUCT ver;

	fj_GetDllVersion (&ver);

	if (pdvi) {
		pdvi->dwMajorVersion	= ver.m_nMajor;
		pdvi->dwMinorVersion	= ver.m_nMinor;
		pdvi->dwBuildNumber		= ver.m_nInternal;
		pdvi->dwPlatformID		= DLLVER_PLATFORM_WINDOWS;
	}

	return NOERROR;
}

HINSTANCE GetInstanceHandle ()
{
	return ::GetModuleHandle (_T ("SPDevice.dll"));
}

CString LoadString (UINT nID)
{
	return FoxjetDatabase::LoadString (nID, _T ("SPDevice.dll"));
}
