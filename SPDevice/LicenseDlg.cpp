// LicenseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LicenseDlg.h"
#include "SPDevApi.h"
#include "SPDevice.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLicenseDlg dialog


CLicenseDlg::CLicenseDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLicenseDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLicenseDlg)
	m_sRegCode = _T("");
	//}}AFX_DATA_INIT
}


void CLicenseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLicenseDlg)
	DDX_Text(pDX, IDC_REG_CODE, m_sRegCode);
	DDV_MaxChars(pDX, m_sRegCode, 14);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLicenseDlg, CDialog)
	//{{AFX_MSG_MAP(CLicenseDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLicenseDlg message handlers

BOOL CLicenseDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLicenseDlg::OnOK() 
{
	UpdateData (true);
	CTime RegDate = CTime::GetCurrentTime();
	time_t Now;
	time ( &Now );

	CString sCheck;
	CString sTemp;
	sTemp = m_sRegCode.Left (5);

	sCheck.Format (_T ("%s%i%02i"), m_sProduct, m_VerMajor, m_VerMinor );
	if ( sCheck.CompareNoCase ( sTemp ) == 0 )
	{
		theApp.WriteProfileString ( m_sAppName, _T ("RegKey"), m_sRegCode);
		theApp.WriteProfileInt ( m_sAppName, _T ("Activation"), Now);
		CDialog::OnOK();
	}
	else
		AfxMessageBox (IDS_INVALIDREGCODE);
}
