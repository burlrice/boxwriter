#if !defined(AFX_LICENSEDLG_H__EB265E72_BF02_45BE_8AB1_6A0AC2FAF531__INCLUDED_)
#define AFX_LICENSEDLG_H__EB265E72_BF02_45BE_8AB1_6A0AC2FAF531__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LicenseDlg.h : header file
//
#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////
// CLicenseDlg dialog

class CLicenseDlg : public CDialog
{
// Construction
public:
	CLicenseDlg(CWnd* pParent = NULL);   // standard constructor

	CString m_sProduct;
	BYTE m_VerMajor;
	BYTE m_VerMinor;
//	CWinApp *m_pApp;
	CString m_sAppName;

// Dialog Data
	//{{AFX_DATA(CLicenseDlg)
	enum { IDD = IDD_LICENSE_KEY };
	CString	m_sRegCode;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLicenseDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	DWORD m_dwVolSerial;

	// Generated message map functions
	//{{AFX_MSG(CLicenseDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LICENSEDLG_H__EB265E72_BF02_45BE_8AB1_6A0AC2FAF531__INCLUDED_)
