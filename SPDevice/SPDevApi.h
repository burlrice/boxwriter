#if !defined (__SPDEV_API_H__)
#define __SPDEV_API_H__

#if defined ( DLL_EXPORT )
#define SPDEVICE_API __declspec( dllexport )
#else 
#define SPDEVICE_API __declspec( dllimport )
#endif

/////////////////////////////////////////////////////////////////////////////
// CSPDeviceApp
// See SPDevice.cpp for the implementation of this class
//
enum eLicenseType{
	NONE,
	INVALID,
	LEVEL_1,
	LEVEL_2
};


SPDEVICE_API eLicenseType CheckLicense( LPCTSTR lpszAppName, PCHAR pProduct, BYTE VerMajor, BYTE VerMinor );
SPDEVICE_API void GetLicenseKey ( LPCTSTR lpszAppName, PCHAR pRegKey, CTime &RegDate );
SPDEVICE_API eLicenseType PromptForLicense ( LPCTSTR lpszAppName, PCHAR pProduct, BYTE VerMajor, BYTE VerMinor );
//SPDEVICE_API DWORD ComputeRegCode ( DWORD dwSerial, BYTE VerMajor, BYTE VerMinor, BYTE Level );
#endif
