; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CConfigDlg
LastTemplate=CListCtrl
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Config.h"

ClassCount=9
Class1=CConfigApp
Class2=CConfigDlg
Class3=CAboutDlg

ResourceCount=15
Resource1=IDR_MAINFRAME
Resource2=IDD_LOCK
Resource3=IDD_ADD_PRINTER_MATRIX
Resource4=IDD_CONFIG_DIALOG
Resource5=IDD_ADD_PRINTER
Class4=CPrinterDlg
Resource6=IDD_COMPACT
Resource7=IDM_LINE
Resource8=IDM_CONTEXT
Resource9=IDD_PRINTER_TYPE
Resource10=IDM_HEAD
Resource11=IDM_ADD
Class5=CLockDlg
Resource12=IDM_PRINTER
Class6=CAddPrinterDlg
Resource13=IDD_PRINTER
Resource14=IDM_MAINMENU
Class7=CPrinterTypeDlg
Class8=CPrinterThread
Class9=CPrinterListCtrl
Resource15=IDA_MAIN

[CLS:CConfigApp]
Type=0
HeaderFile=Config.h
ImplementationFile=Config.cpp
Filter=N
BaseClass=CWinApp
VirtualFilter=AC

[CLS:CConfigDlg]
Type=0
HeaderFile=ConfigDlg.h
ImplementationFile=ConfigDlg.cpp
Filter=W
BaseClass=CDialog
VirtualFilter=dWC
LastObject=BTN_ADD

[CLS:CAboutDlg]
Type=0
HeaderFile=ConfigDlg.h
ImplementationFile=ConfigDlg.cpp
Filter=D

[DLG:IDD_CONFIG_DIALOG]
Type=1
Class=CConfigDlg
ControlCount=6
Control1=BTN_ADD,button,1342242816
Control2=BTN_EDIT,button,1342242816
Control3=BTN_DELETE,button,1342242816
Control4=LV_TASKS,SysListView32,1350664201
Control5=BTN_CLOSE,button,1342242816
Control6=LV_PRINTERS,SysListView32,1350632460

[MNU:IDM_MAINMENU]
Type=1
Class=?
Command1=ID_FILE_COMPACTDATABASE
Command2=ID_FILE_VIEWLOCKS
Command3=ID_APP_EXIT
Command4=ID_HELP_ABOUT
CommandCount=4

[ACL:IDA_MAIN]
Type=1
Class=?
Command1=ID_DO_NOTHING
Command2=ID_HELP_ABOUT
Command3=ID_APP_EXIT
CommandCount=3

[DLG:IDD_COMPACT]
Type=1
Class=?
ControlCount=1
Control1=TXT_TEXT,edit,1342244996

[DLG:IDD_PRINTER]
Type=1
Class=CPrinterDlg
ControlCount=10
Control1=IDC_STATIC,static,1342308352
Control2=TXT_NAME,edit,1350631552
Control3=LBL_ADDR,static,1342308352
Control4=IDC_ADDR,SysIPAddress32,1342242816
Control5=CHK_MASTER,button,1342246915
Control6=BTN_LINES,button,1342242816
Control7=BTN_HEADS,button,1342242816
Control8=IDCANCEL,button,1342242816
Control9=IDOK,button,1342242817
Control10=BTN_REPORTS,button,1342242816

[CLS:CPrinterDlg]
Type=0
HeaderFile=PrinterDlg.h
ImplementationFile=PrinterDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CHK_MASTER

[MNU:IDM_PRINTER]
Type=1
Class=?
Command1=ID_PRINTER_EDIT
Command2=ID_PRINTER_DELETE
Command3=ID_LINE_ADD
Command4=ID_HEAD_ADD
CommandCount=4

[MNU:IDM_LINE]
Type=1
Class=?
Command1=ID_LINE_EDIT
Command2=ID_LINE_DELETE
Command3=ID_HEAD_ADD
CommandCount=3

[MNU:IDM_HEAD]
Type=1
Class=?
Command1=ID_HEAD_EDIT
Command2=ID_HEAD_DELETE
CommandCount=2

[MNU:IDM_CONTEXT]
Type=1
Class=?
Command1=ID_PRINTER_ADD
CommandCount=1

[MNU:IDM_ADD]
Type=1
Class=?
Command1=ID_PRINTER_ADD
Command2=ID_LINE_ADD
Command3=ID_HEAD_ADD
CommandCount=3

[DLG:IDD_LOCK]
Type=1
Class=CLockDlg
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=LV_LOCK,SysListView32,1350631433
Control4=BTN_DELETE,button,1342242816

[CLS:CLockDlg]
Type=0
HeaderFile=LockDlg.h
ImplementationFile=LockDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=BTN_DELETE
VirtualFilter=dWC

[DLG:IDD_ADD_PRINTER]
Type=1
Class=CAddPrinterDlg
ControlCount=8
Control1=IDC_STATIC,static,1342308352
Control2=TXT_NAME,edit,1350631552
Control3=IDC_STATIC,static,1342308352
Control4=TXT_ADDR,edit,1350631552
Control5=IDC_STATIC,static,1342308352
Control6=LV_PRINTERS,SysListView32,1350631437
Control7=IDOK,button,1342242817
Control8=IDCANCEL,button,1342242816

[CLS:CAddPrinterDlg]
Type=0
HeaderFile=AddPrinterDlg.h
ImplementationFile=AddPrinterDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=TXT_NAME

[DLG:IDD_ADD_PRINTER_MATRIX]
Type=1
Class=?
ControlCount=8
Control1=IDC_STATIC,static,1342308352
Control2=TXT_NAME,edit,1350631552
Control3=IDC_STATIC,static,1342308352
Control4=TXT_ADDR,edit,1350631552
Control5=IDC_STATIC,static,1342308352
Control6=LV_PRINTERS,SysListView32,1350631437
Control7=IDOK,button,1342242817
Control8=IDCANCEL,button,1342242816

[DLG:IDD_PRINTER_TYPE]
Type=1
Class=CPrinterTypeDlg
ControlCount=6
Control1=RDO_MATRIX,button,1342312585
Control2=RDO_ELITE,button,1342181513
Control3=RDO_MATRIX_TXT,button,1342312457
Control4=RDO_ELITE_TXT,button,1342181385
Control5=IDCANCEL,button,1342242816
Control6=IDOK,button,1342242817

[CLS:CPrinterTypeDlg]
Type=0
HeaderFile=PrinterTypeDlg.h
ImplementationFile=PrinterTypeDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CPrinterTypeDlg
VirtualFilter=dWC

[CLS:CPrinterThread]
Type=0
HeaderFile=PrinterThread.h
ImplementationFile=PrinterThread.cpp
BaseClass=CWinThread
Filter=N
VirtualFilter=TC

[CLS:CPrinterListCtrl]
Type=0
HeaderFile=PrinterListCtrl.h
ImplementationFile=PrinterListCtrl.cpp
BaseClass=CListCtrl
Filter=W
LastObject=CPrinterListCtrl
VirtualFilter=FWC

