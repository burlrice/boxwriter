// LockDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Config.h"
#include "Resource.h"
#include "LockDlg.h"
#include "Extern.h"
#include "OdbcRecordset.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define TIMER_DATE		1
#define TIMER_LOCKCOUNT 2

CString CLockDlg::CLockItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0: return m_strIpAddr;
	case 1: return m_strName;
	case 2: return m_strType;
	case 3: return m_strUser;
	case 4: { CString str; str.Format (_T ("%d"), m_lRecordID); return str; }
	case 5: { CString str; str.Format (_T ("%d"), m_lPrinterID); return str; }
	case 6: return m_tmModified.Format (_T ("%c"));
	case 7: 
		{
			COleDateTimeSpan tm = COleDateTime::GetCurrentTime () - m_tmModified;
			return tm.Format (_T ("%D days, %H:%M"));
		}
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CLockDlg dialog


CLockDlg::CLockDlg(CWnd* pParent /*=NULL*/)
:	CDialog(CLockDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLockDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CLockDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLockDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLockDlg, CDialog)
	//{{AFX_MSG_MAP(CLockDlg)
	ON_BN_CLICKED(BTN_DELETE, OnDelete)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLockDlg message handlers

void CLockDlg::OnDelete() 
{
	CLongArray sel = ItiLibrary::GetSelectedItems (m_lv);

	if (!sel.GetSize ()) {
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
		return;
	}

	if (MsgBox (* this, LoadString (IDS_DELETELOCKWARN), LoadString (IDS_WARNING), MB_ICONINFORMATION | MB_YESNO) == IDNO)
		return;

	try
	{
		for (int i = 0; i < sel.GetSize (); i++) {
			if (CLockItem * p = (CLockItem *)m_lv.GetCtrlData (sel [i])) {
				CString str;

				str.Format (_T ("DELETE * FROM [Lock] WHERE IpAddr='%s' AND Name='%s' AND Type='%s' AND [Lock].[User]='%s' AND PrinterID=%d AND RecordID=%d"),
					p->m_strIpAddr,
					p->m_strName,
					p->m_strType,
					p->m_strUser,
					p->m_lPrinterID,
					p->m_lRecordID);
				TRACEF (theApp.m_db.FormatSQL (str));
				theApp.m_db.ExecuteSQL (str);
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	FillListCtrl ();
}


BOOL CLockDlg::OnInitDialog() 
{
	using namespace ItiLibrary;
	using ItiLibrary::CListCtrlImp::CColumn;
	
	CArray <CColumn, CColumn> vCols;

	CDialog::OnInitDialog();
	SetTimer (TIMER_DATE, 1000, NULL);
	SetTimer (TIMER_LOCKCOUNT, 5000, NULL);
	
	vCols.Add (CColumn (LoadString (IDS_ADDRESS)));
	vCols.Add (CColumn (LoadString (IDS_COMPUTERNAME), 100));
	vCols.Add (CColumn (LoadString (IDS_TYPE)));
	vCols.Add (CColumn (LoadString (IDS_USER)));
	vCols.Add (CColumn (LoadString (IDS_PRINTERID)));
	vCols.Add (CColumn (LoadString (IDS_RECORDID)));
	vCols.Add (CColumn (LoadString (IDS_MODIFIED), 120));
	vCols.Add (CColumn (LoadString (IDS_MODIFIEDLEN), 120));

	m_lv.Create (LV_LOCK, vCols, this, ListGlobals::defElements.m_strRegSection);

	FillListCtrl ();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLockDlg::FillListCtrl()
{
	try
	{
		using namespace FoxjetDatabase;

		COdbcRecordset rst (theApp.m_db);

		rst.Open (_T ("SELECT * FROM [Lock];"));
		m_lv.DeleteCtrlData ();

		while (!rst.IsEOF ()) {
			CLockItem * p = new CLockItem;
			
			rst >> (* p);
			m_lv.InsertCtrlData (p);
			rst.MoveNext ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
}

void CLockDlg::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent == TIMER_DATE) {
		for (int i = 0; i < m_lv->GetItemCount (); i++) {
			if (CLockItem * p = (CLockItem *)m_lv.GetCtrlData (i)) {
				CString str = m_lv->GetItemText (i, 7);

				if (p->GetDispText (7) != str)
					m_lv.UpdateCtrlData (p);
			}
		}
	}
	else if (nIDEvent == TIMER_LOCKCOUNT) {
		CStringArray v;

		try
		{
			using namespace FoxjetDatabase;

			COdbcRecordset rst (theApp.m_db);

			rst.Open (_T ("SELECT * FROM [Lock];"));
			
			while (!rst.IsEOF ()) {
				CString str;

				rst >> str;
				v.Add (str);
				rst.MoveNext ();
			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		CString str = ToString (v);

		if (m_strRecords != str) {
			m_strRecords = str;
			FillListCtrl ();
		}
	}	
	CDialog::OnTimer(nIDEvent);
}

void CLockDlg::OnDestroy() 
{
	KillTimer (TIMER_DATE);	
	KillTimer (TIMER_LOCKCOUNT);
	CDialog::OnDestroy();
}


