// Config.h : main header file for the CONFIG application
//

#if !defined(AFX_CONFIG_H__050978B3_8E4C_4A56_B7A4_95E571DDEEA9__INCLUDED_)
#define AFX_CONFIG_H__050978B3_8E4C_4A56_B7A4_95E571DDEEA9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "OdbcDatabase.h"
#include "Version.h"
#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CConfigApp:
// See Config.cpp for the implementation of this class
//

class CConfigApp : public CWinApp
{
public:
	CConfigApp();
	virtual ~CConfigApp ();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CConfigApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	FoxjetCommon::CVersion GetVersion() const;

	FoxjetDatabase::COdbcDatabase m_db;
	bool m_bCompact;

protected:

// Implementation

	//{{AFX_MSG(CConfigApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONFIG_H__050978B3_8E4C_4A56_B7A4_95E571DDEEA9__INCLUDED_)
