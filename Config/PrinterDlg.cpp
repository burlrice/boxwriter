// PrinterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "Color.h"
#include <Winsock2.h>
#include "Config.h"
#include "PrinterDlg.h"
#include "LineConfigDlg.h"
#include "HeadConfigDlg.h"
#include "AnsiString.h"
#include "PrintReportDlg.h"
#include "ScanReportDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrinterDlg dialog


CPrinterDlg::CPrinterDlg(CWnd* pParent /*=NULL*/)
:	m_lID (0),
	m_bAddLine (false),
	m_bAddHead (false),
	m_lLineID (-1),
	CDialog(CPrinterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPrinterDlg)
	m_bMaster = FALSE;
	m_strName = _T("");
	//}}AFX_DATA_INIT
}


void CPrinterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrinterDlg)
	DDX_Control(pDX, IDC_ADDR, m_ctrlAddress);
	DDX_Check(pDX, CHK_MASTER, m_bMaster);
	DDX_Text(pDX, TXT_NAME, m_strName);
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_LINES,	IDI_LINE);
	m_buttons.DoDataExchange (pDX, BTN_HEADS,	IDI_HEAD);
	m_buttons.DoDataExchange (pDX, BTN_REPORTS,	IDB_REPORT);
}


BEGIN_MESSAGE_MAP(CPrinterDlg, CDialog)
	//{{AFX_MSG_MAP(CPrinterDlg)
	ON_BN_CLICKED(BTN_LINES, OnLines)
	ON_BN_CLICKED(BTN_HEADS, OnHeads)
	ON_BN_CLICKED(BTN_REPORTS, OnReports)
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
	//}}AFX_MSG_MAP
	ON_COMMAND_RANGE(IDM_REPORT, IDM_REPORT+10, OnSelectReport) 
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrinterDlg message handlers

BOOL CPrinterDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_ctrlAddress.SetAddress (htonl (inet_addr (w2a (m_strAddress))));
	
	if (CWnd * p = GetDlgItem (CHK_MASTER))
		p->ShowWindow (IsMasterMode ());

	if (m_bAddLine) 
		OnLines ();
	else if (m_bAddHead)
		OnHeads ();

	if (!m_bMaster) {
		if (!IsDebug ()) {
			if (CWnd * p = GetDlgItem (BTN_LINES)) p->ShowWindow (SW_HIDE);
			if (CWnd * p = GetDlgItem (BTN_HEADS)) p->ShowWindow (SW_HIDE);
		}
	}
	else {
		if (CWnd * p = GetDlgItem (TXT_NAME)) p->EnableWindow (FALSE);
		if (CWnd * p = GetDlgItem (LBL_ADDR)) p->ShowWindow (SW_HIDE);
		if (CWnd * p = GetDlgItem (IDC_ADDR)) p->ShowWindow (SW_HIDE);
		if (CWnd * p = GetDlgItem (BTN_REPORTS)) p->ShowWindow (SW_HIDE);
	}

	m_vReports.Add (LoadString (IDS_SCAN_REPORT));
	m_vReports.Add (LoadString (IDS_PRINT_REPORT));

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPrinterDlg::OnOK()
{
	ULONG lAddress = 0;
	in_addr addr;

	m_ctrlAddress.GetAddress (lAddress); 
	addr.S_un.S_addr = ntohl (lAddress);
	m_strAddress = inet_ntoa (addr);

	CDialog::OnOK ();
}

void CPrinterDlg::OnLines() 
{
	Foxjet3d::LineConfigDlg::CLineConfigDlg dlg (theApp.m_db, this);
	dlg.m_lPrinterID = m_lID;

	int nResult = dlg.DoModal ();

	if (m_bAddLine) {
		if (nResult == IDOK)
			CDialog::OnOK ();
		else 
			CDialog::OnCancel ();
	}
}

void CPrinterDlg::OnHeads() 
{
	Foxjet3d::HeadConfigDlg::CHeadConfigDlg dlg (theApp.m_db, INCHES, m_lLineID, false, this);
	dlg.m_lPrinterID = m_lID;

	int nResult = dlg.DoModal ();

	if (m_bAddHead) {
		if (nResult == IDOK)
			CDialog::OnOK ();
		else 
			CDialog::OnCancel ();
	}
}

void CPrinterDlg::OnReports() 
{
	if (CWnd * p = GetDlgItem (BTN_REPORTS)) {
		CMenu menu;
		CRect rc;

		p->GetWindowRect (rc);
		menu.CreatePopupMenu ();

		for (int i = 0; i < m_vReports.GetSize (); i++) {
			MENUITEMINFO info;

			memset (&info, 0, sizeof (info));

			info.cbSize		= sizeof (info);
			info.fMask		= MIIM_TYPE | MIIM_ID | MIIM_DATA;
			info.fType		= MFT_OWNERDRAW;
			info.wID		= IDM_REPORT + i;

			::InsertMenuItem (menu, i, TRUE, &info);
		}

		menu.TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON, rc.left, rc.top, this);
	}
}

void CPrinterDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	using namespace Color;
	using namespace FoxjetCommon;

	DRAWITEMSTRUCT dis;
	
	memcpy (&dis, lpDrawItemStruct, sizeof (dis));

	if (dis.CtlType == ODT_MENU) {
		bool bSelected = (dis.itemState & (ODS_SELECTED | ODS_FOCUS)) ? false : true;
		CString str;
		CRect rc = dis.rcItem;
		CDC dc;

		int nIndex = dis.itemID - IDM_REPORT;

		if (nIndex >= 0 && nIndex < m_vReports.GetSize ())
			str = m_vReports [nIndex];

		dc.Attach (dis.hDC);
		COLORREF rgbSetTextColor = dc.SetTextColor (bSelected ? Color::rgbCOLOR_WINDOWTEXT : Color::rgbCOLOR_HIGHLIGHTTEXT);
		int nSetBkMode = dc.SetBkMode (TRANSPARENT);
		dc.FillRect (rc, &CBrush (bSelected ? Color::rgbCOLOR_MENU : Color::rgbCOLOR_HIGHLIGHT));
		dc.DrawEdge (rc, EDGE_SUNKEN, BF_LEFT | BF_RIGHT | BF_TOP | BF_BOTTOM | BF_ADJUST);

		dc.DrawText (str, rc, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_EXPANDTABS);
		dc.SetTextColor (rgbSetTextColor);
		dc.SetBkMode (nSetBkMode);
		dc.Detach ();
	}
	else 
		CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CPrinterDlg::OnSelectReport (UINT nID)
{
	switch (nID - IDM_REPORT) { 
	case 0:
		{
			Foxjet3d::ScanReportDlg::CScanReportDlg dlg (theApp.m_db);
			//dlg.m_sLine = ?
			dlg.m_lPrinterID = m_lID;

			if (dlg.DoModal () == IDOK) {
				SETTINGSSTRUCT s;

				s.m_strKey	= _T ("Control::scan report");
				s.m_lLineID = dlg.m_lPrinterID;
				s.m_strData = dlg.m_bEnable ? _T ("1") : _T ("0");

				if (!UpdateSettingsRecord (theApp.m_db, s))
					VERIFY (AddSettingsRecord (theApp.m_db, s));
			}
		}
		return;
	case 1:
		{
			Foxjet3d::PrintReportDlg::CPrintReportDlg dlg (theApp.m_db);
			//dlg.m_sLine = ?
			dlg.m_lPrinterID = m_lID;
			
			if (dlg.DoModal () == IDOK) {
				SETTINGSSTRUCT s;

				s.m_strKey	= _T ("Control::print report");
				s.m_lLineID = dlg.m_lPrinterID;
				s.m_strData = dlg.m_bEnable ? _T ("1") : _T ("0");

				if (!UpdateSettingsRecord (theApp.m_db, s))
					VERIFY (AddSettingsRecord (theApp.m_db, s));
			}
		}
		return;
	}
}

void CPrinterDlg::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpmis) 
{
	if (lpmis->CtlType == ODT_MENU) {
		lpmis->itemWidth = 200;
		lpmis->itemHeight = 50;
	}
	
	CDialog::OnMeasureItem(nIDCtl, lpmis);
}
