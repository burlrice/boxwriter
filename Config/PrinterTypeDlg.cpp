// PrinterTypeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Config.h"
#include "PrinterTypeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrinterTypeDlg dialog


CPrinterTypeDlg::CPrinterTypeDlg(CWnd* pParent /*=NULL*/)
:	m_type (MATRIX),
	m_nType ((int)MATRIX),
	CDialog(CPrinterTypeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPrinterTypeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	VERIFY (m_bmpElite.LoadBitmap (IDB_ELITE));
	VERIFY (m_bmpMatrix.LoadBitmap (IDB_MATRIX));

	const int nSize = 256;

	Resize (m_bmpElite, nSize);
	Resize (m_bmpMatrix, nSize);
}


void CPrinterTypeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Radio (pDX, RDO_MATRIX, (int &)m_type);
	DDX_Radio (pDX, RDO_MATRIX_TXT, m_nType);
	//{{AFX_DATA_MAP(CPrinterTypeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
}


BEGIN_MESSAGE_MAP(CPrinterTypeDlg, CDialog)
	//{{AFX_MSG_MAP(CPrinterTypeDlg)
	ON_BN_CLICKED(RDO_MATRIX, OnType)
	ON_BN_CLICKED(RDO_MATRIX_TXT, OnTypeTxt)
	ON_BN_CLICKED(RDO_ELITE, OnType)
	ON_BN_CLICKED(RDO_ELITE_TXT, OnTypeTxt)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrinterTypeDlg message handlers

BOOL CPrinterTypeDlg::OnInitDialog() 
{
	if (CButton * p = (CButton *)GetDlgItem (RDO_MATRIX))
		p->SetBitmap (m_bmpMatrix);

	if (CButton * p = (CButton *)GetDlgItem (RDO_ELITE))
		p->SetBitmap (m_bmpElite);

	CDialog::OnInitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPrinterTypeDlg::OnType () 
{
	UpdateData ();
	m_nType = m_type;
	UpdateData (FALSE);
}

void CPrinterTypeDlg::OnTypeTxt() 
{
	UpdateData ();
	m_type = (PRINTERTYPE)m_nType;
	UpdateData (FALSE);
}
