// stdafx.cpp : source file that includes just the standard includes
//	Config.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include "color.h"
#include "xImage.h"

CString LoadString (UINT nID)
{
	static HINSTANCE hDLL = theApp.m_hInstance;
	HINSTANCE hInst = ::AfxGetResourceHandle ();
	CString str;

	ASSERT (hDLL);
	::AfxSetResourceHandle (hDLL);

	BOOL bLoad = str.LoadString (nID);
	ASSERT (bLoad);
	
	::AfxSetResourceHandle (hInst);

	return str;
}


bool IsSocketDataPending (SOCKET s)
{
	struct timeval timeout = { 1, 0 };
	fd_set read;

	memset (&read, 0, sizeof (read));
	read.fd_count = 1;
	read.fd_array [0] = s;

	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

	int nSelect = ::select (0, &read, NULL, NULL, &timeout);

	return (nSelect != SOCKET_ERROR && nSelect > 0) ? true : false;
}


bool IsMasterMode ()
{
	CString str = ::GetCommandLine ();

	str.MakeLower ();

	return str.Find (_T ("/master")) != -1 ? true : false;
}

COLORREF Resize (CBitmap & bmp, int nSize)
{
	CxImage img;

	img.CreateFromHBITMAP (bmp);
	bmp.Detach ();

	CSize size = CSize (img.GetWidth (), img.GetHeight ());
	double dZoom = min (nSize / (double)size.cx, nSize / (double)size.cy);
	img.Resample ((int)(size.cx * dZoom), (int)(size.cy * dZoom));
	bmp.Attach (img.MakeBitmap ());

	return Color::rgbBlack;
}

bool IsDebug ()
{
	static bool bDebug = false;
	static bool bInit = false;

	if (!bInit) {
		CString str = ::GetCommandLine ();

		str.MakeLower ();
		bDebug = str.Find (_T ("/debug")) != -1 ? true : false;
		bInit = true;
	}

	return bDebug;
}
