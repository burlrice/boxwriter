// Config.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Config.h"
#include "ConfigDlg.h"
#include "WinMsg.h"
#include "FileExt.h"
#include "Registry.h"
#include "File.h"
#include "WinMsg.h"
#include "AddPrinterDlg.h"
#include "Database.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"

using namespace FoxjetDebug;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CConfigApp

BEGIN_MESSAGE_MAP(CConfigApp, CWinApp)
	//{{AFX_MSG_MAP(CConfigApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConfigApp construction

CConfigApp::CConfigApp()
:	m_db (_T (__FILE__), __LINE__),
	m_bCompact (false)
{
	#ifdef _DEBUG
	using namespace FoxjetDebug;

	CDebugFile * pFile = new CDebugFile ();
	int nFlags = CFile::modeCreate | CFile::modeReadWrite;
	CString str, strFile = _T ("Config.log");

	BOOL bOpen = pFile->Open (strFile, nFlags);

	if (bOpen) {
		::afxDump.m_pFile = pFile;
		str.Format (_T ("\nLog started on: %s\n\n"),
			CTime::GetCurrentTime ().Format (_T ("%c")));
		TRACEF (str);
	}
	else {
		::AfxMessageBox (_T ("failed to open log"));
		delete pFile;
	}
	#endif //_DEBUG
}

CConfigApp::~CConfigApp()
{
	#ifdef _DEBUG
	if (::afxDump.m_pFile) {
		::afxDump.m_pFile->Close ();
		delete (FoxjetDebug::CDebugFile *)::afxDump.m_pFile;
		::afxDump.m_pFile = NULL;
	}
	#endif //_DEBUG
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CConfigApp object

CConfigApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CConfigApp initialization

BOOL CConfigApp::InitInstance()
{
	IsInstallerInvoked ();

	if (!CInstance::Elevate ())
		return FALSE;

	FoxjetCommon::CheckDllVersions ();

	#ifdef _DEBUG
	{
		UINT n [] = 
		{
			WM_ISEDITORRUNNING,					// 0
			WM_ISPRINTERRUNNING,				// 1
			WM_ISCONFIGRUNNING,					// 2
			WM_ISHOSTRUNNING,					// 3	
			WM_SHUTDOWNAPP,						// 4
			WM_SYSTEMPARAMCHANGE,				// 5
			WM_GETPRINTERWND,					// 6
			WM_OPENMAPFILE,						// 7
			WM_CLOSEMAPFILE,					// 8
			WM_SENDMAPPEDCMD,					// 9
			WM_TASKUPDATED,						// 10
			WM_PRINTER_SOP,						// 11
			WM_PRINTER_EOP,						// 12
			WM_EDITOR_REDRAW,					// 13
			WM_EDITOR_NEW_TASK,					// 14
			WM_EDITOR_TASK_CHANGE,				// 15
			WM_EDITOR_OPEN_TASK,				// 16
			WM_DATABASE_CLOSED,					// 17
			WM_PRINTER_HEAD_STATE_CHANGE,		// 18
			//WM_SYNCREMOTEHEAD,
		};

		for (int i = 0; i < ARRAYSIZE (n); i++) {
			ASSERT (n [i] >= 0xC000 && n [i] <= 0xFFFF);

			for (int j = 0; j < ARRAYSIZE (n); j++) {
				if (i != j)
					ASSERT (n [i] != n [j]);
			}
		}
	}
	#endif

	HANDLE hMutexOneInstance = ::CreateMutex (NULL, FALSE, ISCONFIGRUNNING);
	bool bAlreadyRunning = 
		GetLastError () == ERROR_ALREADY_EXISTS || 
		GetLastError () == ERROR_ACCESS_DENIED;
	bool bExisits = GetLastError () == ERROR_ALREADY_EXISTS;
	bool bDenied = GetLastError () == ERROR_ACCESS_DENIED;

	{
		WSADATA wsaData;

		if (::WSAStartup (MAKEWORD (2,1),&wsaData) != 0){
			TRACEF ("WSAStartup failed: " + FormatMessage (GetLastError ()));
			return 0;
		}
	}

	if (bAlreadyRunning) {	
		DWORD dwResult;

		LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_ISCONFIGRUNNING, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 5000, &dwResult);
		
		::CloseHandle (hMutexOneInstance);
		return FALSE; 
	}

	AfxEnableControlContainer();
	SetDialogBkColor (FoxjetUtils::GetButtonColor (_T ("background")));

	// Standard initialization

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	//FoxjetCommon::SetDriver (GUID_INTERFACE_FXMPHC);

	CString strDSN = GetDSN ();
	CString strName = GetDatabaseName ();
	CString strCmdLine = ::GetCommandLine ();
	strCmdLine.MakeUpper ();
	FoxjetDatabase::SetNetworked (true); //(_tcsstr (strCmdLine, _T ("/NETWORKED")) != NULL);
	FoxjetDatabase::SetGojo (_tcsstr (strCmdLine, _T ("/GOJO")) != NULL);

	{
		WSADATA wsaData;

		if (::WSAStartup (MAKEWORD (2,1),&wsaData) != 0){
			TRACEF ("WSAStartup failed: " + FormatMessage (GetLastError ()));
			return 0;
		}
	}

	CPrinterThread::InitializeCriticalSection ();

	{
		CString str = ::GetCommandLine ();

		str.MakeLower ();

		if (str.Find (_T ("/matrix")) != -1) {
			FoxjetDatabase::SetHighResDisplay (false);
			SetDialogBkColor (FoxjetUtils::GetButtonColor (_T ("background")));
		}

		CString strExtract = Extract (str, _T ("/dsn="), _T (" "));

		if (!strExtract.GetLength ())
			strExtract = Extract (str, _T ("/dsn="));

		if (strExtract.GetLength ())
			strDSN = strExtract;
	}

	{
		CString str = ::GetCommandLine ();

		str.MakeUpper ();
		FoxjetDatabase::SetGojo (str.Find (_T ("/GOJO")) != -1);
	}

	{
		CString strKey = _T ("Software\\Foxjet\\MarksmanELITE\\ODBC\\") + strDSN;
		CString strUID = FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), _T ("/UID="));
		CString strPWD = FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), _T ("/PWD="));

		if (IsGojo ()) {
			if (!strUID.GetLength ()) strUID = _T ("FoxJet");
			if (!strPWD.GetLength ()) strPWD = _T ("FoxJet");
		}

		TRACEF (strKey);
		TRACEF (_T ("/UID=") + strUID + _T (" /PWD=") + strPWD);

		FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("UID"), strUID); 
		FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("PWD"), strPWD); 
	}

	if (!FoxjetCommon::OpenDatabase (m_db, strDSN, strName)) {
		::MessageBox (NULL, LoadString (IDS_FAILEDTOINITIALIZEDB), 
			::AfxGetAppName (), MB_ICONERROR | MB_OK);
		FoxjetCommon::ExitInstance (GetElementListVersion ());
		::AfxAbort ();
	}

	COdbcDatabase::SetAppDsn (FoxjetDatabase::Extract (m_db.GetConnect (), _T ("DSN="), _T (";")));

	if (!FoxjetCommon::InitInstance (m_db, GetElementListVersion ())) {
		::MessageBox (NULL, LoadString (IDS_FAILEDTOINITIALIZEDLL), 
			::AfxGetAppName (), MB_ICONERROR | MB_OK);
		FoxjetCommon::ExitInstance (GetElementListVersion ());
		::AfxAbort ();
		return FALSE;
	}

	{
		CString str = ::GetCommandLine ();

		str.MakeLower ();

		if (str.Find (_T ("/addnew")) != -1) {
			CAddPrinterDlg dlg;
			CString strName;

			dlg.m_strAddr = FoxjetDatabase::GetIpAddress ();
			strName = dlg.m_strName = FoxjetDatabase::GetComputerName ();
			m_pMainWnd = &dlg;

			try
			{
				COdbcRecordset rst (m_db);
				CString str;

				str.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s' AND [%s]='%s'"), 
					Printers::m_lpszTable,
					Printers::m_lpszName, strName,
					Printers::m_lpszAddress, dlg.m_strAddr);
				TRACEF (str);
				rst.Open (str);
				
				if (rst.GetRecordCount () == 0) {
					int nIndex = dlg.m_strAddr.ReverseFind ('.');

					if (nIndex != -1)
						strName = dlg.m_strName + dlg.m_strAddr.Mid (nIndex);
				}
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

			dlg.m_strName = strName;
			dlg.DoModal ();

			return FALSE;
		}
	}

	CConfigDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
	}
	else if (nResponse == IDCANCEL)
	{
	}


	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int CConfigApp::ExitInstance() 
{
	if (m_db.IsOpen ()) {
		m_db.Close ();
		::MessageBox (NULL, _T ("m_db.Close ()"), 0, 0);
	}

	CPrinterThread::DeleteCriticalSection ();

	return CWinApp::ExitInstance();
}

FoxjetCommon::CVersion CConfigApp::GetVersion() const
{
//	void CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer)
	return CVersion (FoxjetCommon::verApp);
}

