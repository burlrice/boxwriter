#if !defined(AFX_PRINTERLISTCTRL_H__3F809F58_64FA_4E6A_8558_A7A7E84DD75D__INCLUDED_)
#define AFX_PRINTERLISTCTRL_H__3F809F58_64FA_4E6A_8558_A7A7E84DD75D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrinterListCtrl.h : header file
//

#include "DbTypeDefs.h"

#define PRINTERCTRL_MASTER			_T ("Master")
#define PRINTERCTRL_MATRIX			_T ("Matrix")
#define PRINTERCTRL_ELITE			_T ("Elite")
#define PRINTERCTRL_CONNECTED		_T ("Connected")
#define PRINTERCTRL_DISCONNECTED	_T ("Disconnected")

/////////////////////////////////////////////////////////////////////////////
// CPrinterListCtrl window

class CPrinterListCtrl : public CListCtrl
{
// Construction
public:
	CPrinterListCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrinterListCtrl)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	void Init();
	int GetIndex (ULONG lPrinterID);
	virtual ~CPrinterListCtrl();
	void DrawIcon (CDC * pDC, int nItem);
	void DrawText (CDC * pDC, int nItem);
	void Draw (CDC * pDC, int nItem);

	CImageList m_imglist;
	CBitmap m_bmpMaster;
	CBitmap m_bmpElite;
	CBitmap m_bmpMatrix;
	CBitmap m_bmpEliteDisabled;
	CBitmap m_bmpMatrixDisabled;
	CBitmap m_bmpState;
	CBitmap m_bmpStateRed;
	CBitmap m_bmpStateYellow;
	CBitmap m_bmpStateGreen;
	CBitmap m_bmpStateGray;
	CBitmap m_bmpRunning;
	CBitmap m_bmpIdle;
	CBitmap m_bmpStopped;
	CBitmap m_bmpStrobeRed;
	CBitmap m_bmpStrobeYellow;
	CBitmap m_bmpStrobeGreen;
	CBitmap m_bmpStrobeGray;

	int m_nMasterIndex;
	int m_nEliteIndex;
	int m_nEliteDisabledIndex;
	int m_nMatrixIndex;
	int m_nMatrixDisabledIndex;
	CFont m_fntStatus;
	CMapStringToString m_mapData;

	// Generated message map functions
protected:
	int GetState (ULONG lPrinterID, CString & strConnected, CString & strType, 
		int nLine = 0, CString & strTask = CString (), CString & strCount = CString (), CString & strState = CString ());

	//{{AFX_MSG(CPrinterListCtrl)
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnCustomDraw(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnRightClick(NMHDR* pNMHDR, LRESULT* pResult);

	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTERLISTCTRL_H__3F809F58_64FA_4E6A_8558_A7A7E84DD75D__INCLUDED_)
