# Microsoft Developer Studio Project File - Name="Config" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Config - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Config.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Config.mak" CFG="Config - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Config - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Config - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Config - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\Editor\DLL\Debug" /I "..\CxImage\CxImage\\" /I "..\Editor\DLL\ElementList\Win" /I "..\Editor\DLL\Utils" /I "..\Editor\DLL\3d\DirectXExt" /I "..\Editor\DLL\Database" /I "..\Editor\DLL\Common" /I "..\Editor\DLL\3d" /D "NDEBUG" /D "_UNICODE" /D "__WINPRINTER__" /D "_AFXDLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 MSIMG32.LIB shlwapi.lib Ws2_32.lib ElementList.lib Document.lib Utils.lib Database.lib 3d.lib /nologo /entry:"wWinMainCRTStartup " /subsystem:windows /machine:I386 /libpath:"..\Editor\DLL\ElementList\Win\Release" /libpath:"..\Editor\DLL\Document\Release" /libpath:"..\Editor\DLL\3d\Release" /libpath:"..\Editor\DLL\Database\Release" /libpath:"..\Editor\DLL\Utils\Release"
# Begin Custom Build - Updating \FoxJet\$(TargetName).exe
TargetPath=.\Release\Config.exe
TargetName=Config
InputPath=.\Release\Config.exe
SOURCE="$(InputPath)"

"\FoxJet\$(TargetName).exe" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(TargetPath) \FoxJet

# End Custom Build

!ELSEIF  "$(CFG)" == "Config - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\CxImage\CxImage\\" /I "..\Editor\DLL\Utils" /I "..\Editor\DLL\ElementList\Win" /I "..\Editor\DLL\3d\DirectXExt" /I "..\Editor\DLL\Debug" /I "..\Editor\DLL\Database" /I "..\Editor\DLL\Common" /I "..\Editor\DLL\3d" /D "_DEBUG" /D "_UNICODE" /D "__WINPRINTER__" /D "_AFXDLL" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 MSIMG32.LIB shlwapi.lib Ws2_32.lib Debug.lib ElementList.lib Document.lib Utils.lib Database.lib 3d.lib /nologo /entry:"wWinMainCRTStartup " /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"..\Editor\DLL\ElementList\Win\Debug" /libpath:"..\Editor\DLL\Debug\Debug" /libpath:"..\Editor\DLL\3d\Debug" /libpath:"..\Editor\DLL\Document\Debug" /libpath:"..\Editor\DLL\Database\Debug" /libpath:"..\Editor\DLL\Utils\Debug"
# Begin Custom Build - Updating \FoxJet\$(TargetName).exe
TargetPath=.\Debug\Config.exe
TargetName=Config
InputPath=.\Debug\Config.exe
SOURCE="$(InputPath)"

"\FoxJet\$(TargetName).exe" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(TargetPath) \FoxJet

# End Custom Build

!ENDIF 

# Begin Target

# Name "Config - Win32 Release"
# Name "Config - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AddPrinterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Config.cpp
# End Source File
# Begin Source File

SOURCE=.\Config.rc
# End Source File
# Begin Source File

SOURCE=.\ConfigDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PrinterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PrinterListCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\PrinterThread.cpp
# End Source File
# Begin Source File

SOURCE=.\PrinterTypeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AddPrinterDlg.h
# End Source File
# Begin Source File

SOURCE=.\Config.h
# End Source File
# Begin Source File

SOURCE=.\ConfigDlg.h
# End Source File
# Begin Source File

SOURCE=.\PrinterDlg.h
# End Source File
# Begin Source File

SOURCE=.\PrinterListCtrl.h
# End Source File
# Begin Source File

SOURCE=.\PrinterThread.h
# End Source File
# Begin Source File

SOURCE=.\PrinterTypeDlg.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\cancel.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Config.ico
# End Source File
# Begin Source File

SOURCE=.\res\Config.rc2
# End Source File
# Begin Source File

SOURCE=.\res\Configure.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Editor.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ELITE.bmp
# End Source File
# Begin Source File

SOURCE=.\res\head.ico
# End Source File
# Begin Source File

SOURCE=.\res\headdisa.ico
# End Source File
# Begin Source File

SOURCE=.\res\headmast.ico
# End Source File
# Begin Source File

SOURCE=.\res\line.ico
# End Source File
# Begin Source File

SOURCE=.\res\master.bmp
# End Source File
# Begin Source File

SOURCE=.\res\matrix.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ok.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Pause.bmp
# End Source File
# Begin Source File

SOURCE=.\res\printer.ico
# End Source File
# Begin Source File

SOURCE=.\res\printer1.ico
# End Source File
# Begin Source File

SOURCE=.\res\report.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Start.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Stop.bmp
# End Source File
# Begin Source File

SOURCE=.\res\strobe_body.bmp
# End Source File
# Begin Source File

SOURCE=.\res\strobe_green.bmp
# End Source File
# Begin Source File

SOURCE=.\res\strobe_red.bmp
# End Source File
# Begin Source File

SOURCE=.\res\strobe_round.bmp
# End Source File
# Begin Source File

SOURCE=.\res\strobe_yellow.bmp
# End Source File
# Begin Source File

SOURCE=.\res\task.ico
# End Source File
# End Group
# Begin Source File

SOURCE=.\Changes.txt
# End Source File
# End Target
# End Project
