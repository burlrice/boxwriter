// ConfigDlg.h : header file
//

#if !defined(AFX_CONFIGDLG_H__DB425786_126F_4A50_B4D7_42019BD45578__INCLUDED_)
#define AFX_CONFIGDLG_H__DB425786_126F_4A50_B4D7_42019BD45578__INCLUDED_

#include "RoundButtons.h"
#include "PrinterThread.h"
#include "PrinterListCtrl.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

int Find (CArray <CPrinterThread *, CPrinterThread *> & v, ULONG lPrinterID);
int Find (CArray <PRINTERSTRUCT, PRINTERSTRUCT &> & v, ULONG lPrinterID);

/////////////////////////////////////////////////////////////////////////////
// CConfigDlg dialog

class CTreeItem 
{
public:
	CTreeItem (const FoxjetDatabase::PRINTERSTRUCT & printer, ULONG lPrinterID);
	CTreeItem (const FoxjetDatabase::LINESTRUCT & line, ULONG lPrinterID);
	CTreeItem (const FoxjetDatabase::HEADSTRUCT & head, ULONG lPrinterID);
	virtual ~CTreeItem ();
			
	typedef enum { PRINTER = 0, LINE, HEAD } TYPE;

	const TYPE m_type;
	const ULONG m_lPrinterID;
	CArray <ULONG, ULONG> m_vLineIDs;

	union 
	{
		FoxjetDatabase::PRINTERSTRUCT *	m_pPrinter;
		FoxjetDatabase::LINESTRUCT *	m_pLine;
		FoxjetDatabase::HEADSTRUCT *	m_pHead;
	};

	UINT GetMenuID () const;
	UINT GetDefMenuID () const;
};

class CConfigDlg : public CDialog
{
// Construction
public:
	CConfigDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CConfigDlg ();

// Dialog Data
	//{{AFX_DATA(CConfigDlg)
	enum { IDD = IDD_CONFIG_DIALOG };
	CPrinterListCtrl	m_lv;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CConfigDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

public:

	static ULONG GetPanelID (CArray <PANELSTRUCT, PANELSTRUCT &> & v, UINT nNumber);

// Implementation
protected:
	CArray <CPrinterThread *, CPrinterThread *> m_vThreads;

	virtual void OnCancel();
	virtual void OnOK();


	protected:

	struct 
	{
		ULONG m_lID;
		ULONG m_lType;
	} m_sel;

	HICON m_hIcon;
	int m_nSelected;

	HACCEL m_hAccel;
	
	//HANDLE m_hLockThread;
	//DWORD m_dwLockThreadID;


	bool m_bPrinterAddLine;
	bool m_bPrinterAddHead;
	ULONG m_lPrinterAddHead;
	ULONG m_lLineID;
	bool m_bFillingTreeCtrl;
	UINT m_nTimerID;

	//void FillTreeCtrl ();
	void FillListCtrl ();
	void DeleteTreeItem (CTreeCtrl & tree, HTREEITEM hItem);

	bool Edit (PRINTERSTRUCT & p);
	bool Edit (LINESTRUCT & line, ULONG lPrinterID);
	bool Edit (HEADSTRUCT & head, ULONG lPrinterID);

	bool LineNamesMatch ();

	afx_msg void OnAppAbout();
	afx_msg LRESULT OnTasksChanged (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnPrinterStateChange (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnColorsChanged (WPARAM wParam, LPARAM lParam);
	afx_msg void OnDoNothing ();
	afx_msg void OnFileViewlocks ();
	afx_msg void OnEdit ();
	afx_msg LRESULT OnIsAppRunning (WPARAM wParam, LPARAM lParam);

	FoxjetUtils::CRoundButtons m_buttons;

	afx_msg void OnRclickPrinters(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangedPrinters(NMHDR* pNMHDR, LRESULT* pResult);

	// Generated message map functions
	//{{AFX_MSG(CConfigDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnTimer( UINT nIDEvent );
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnHelpAbout();
	afx_msg void OnAppExit();
	afx_msg void OnFileCompactdatabase();
	afx_msg void OnDblclkPrinters(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnPrinterEdit();
	afx_msg void OnPrinterAdd();
	afx_msg void OnPrinterDelete();
	afx_msg void OnHeadEdit();
	afx_msg void OnHeadDelete();
	afx_msg void OnLineDelete();
	afx_msg void OnLineEdit();
	afx_msg void OnDelete();
	afx_msg void OnAdd();
	afx_msg void OnHeadAdd();
	afx_msg void OnLineAdd();
	afx_msg void OnEditTask();
	afx_msg void OnAddTask();
	afx_msg void OnDeleteTask();
	afx_msg void OnItemdblclickTasks(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnInitMenu(CMenu* pMenu);
	afx_msg void OnClose();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnStart (UINT nCmdID);
	afx_msg void OnStop (UINT nCmdID);
	afx_msg void OnIdle (UINT nCmdID);
	afx_msg void OnResume (UINT nCmdID);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONFIGDLG_H__DB425786_126F_4A50_B4D7_42019BD45578__INCLUDED_)
