#if !defined(AFX_PRINTERTHREAD_H__A2670CBE_7548_44EA_BD5B_8330A8C0D1DF__INCLUDED_)
#define AFX_PRINTERTHREAD_H__A2670CBE_7548_44EA_BD5B_8330A8C0D1DF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrinterThread.h : header file
//

#include "Database.h"

using namespace FoxjetDatabase;

#define TM_SET_PRINTER				(WM_APP+1)
#define TM_CLOSE					(WM_APP+2)
#define TM_SET_HWND					(WM_APP+3)

#define TM_POSTMESSAGESTRUCT_FIRST	TM_SET_PRINTER
#define TM_POSTMESSAGESTRUCT_LAST	TM_SET_HWND

#define WM_PRINTERSTATECHANGE		(WM_USER+1)

#define ISPOSTMESSAGESTRUCT(s) ((s) >= TM_POSTMESSAGESTRUCT_FIRST && (s) <= TM_POSTMESSAGESTRUCT_LAST)


/////////////////////////////////////////////////////////////////////////////
// CPrinterThread thread

#define POST_THREAD_MESSAGE(info,msg,wParam)			(info).PostMessage ((msg), (WPARAM)(wParam), _T (__FILE__), __LINE__);
#define SEND_THREAD_MESSAGE(info,msg,wParam)			(info).SendMessage ((msg), (WPARAM)(wParam), _T (__FILE__), __LINE__);

class CPrinterThread : public CWinThread
{
	DECLARE_DYNCREATE(CPrinterThread)
protected:
	CPrinterThread();           // protected constructor used by dynamic creation

public:
	typedef struct tagPOSTMESSAGESTRUCT
	{
		tagPOSTMESSAGESTRUCT (bool bPost, const CString & strFile, ULONG lLine, HANDLE hEvent) : m_bPost (bPost), m_strFile (strFile), m_lLine (lLine), m_hEvent (hEvent) { }

		bool m_bPost;
		CString m_strFile;
		ULONG m_lLine;
		HANDLE m_hEvent;
	} POSTMESSAGESTRUCT;

	static void InitializeCriticalSection ();
	static void DeleteCriticalSection ();
	static bool GetPrinter (ULONG lPrinterID, PRINTERSTRUCT & printer);
	static void SetPrinter (ULONG lPrinterID, const PRINTERSTRUCT & printer);

	DWORD GetThreadID ();
	void PostMessage (UINT nMsg, WPARAM wParam, LPCTSTR lpszFile, ULONG lLine);
	void SendMessage (UINT nMsg, WPARAM wParam, LPCTSTR lpszFile, ULONG lLine);

	ULONG m_lPrinterID;
	DWORD m_dwThreadID;
	HANDLE m_hInit;

protected:
	static bool SendCmd (SOCKET & socket, CStringArray & vCmd, CStringArray & vResponse);
	bool SendCmd (CStringArray & vCmd, CStringArray & vResponse);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrinterThread)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual int Run();
	//}}AFX_VIRTUAL

// Implementation
protected:
	void DoDisconnect();
	void DoConnect();
	void DoUpdate();
	virtual ~CPrinterThread();

	PRINTERSTRUCT m_printer;
	SOCKET m_socket;
	HWND m_hPrinterWnd;
	bool m_bRun;

	// Generated message map functions
	//{{AFX_MSG(CPrinterThread)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTERTHREAD_H__A2670CBE_7548_44EA_BD5B_8330A8C0D1DF__INCLUDED_)
