// AddPrinterDlg.cpp : implementation file
//

#include "stdafx.h"
#include <shlwapi.h>
#include "Config.h"
#include "AddPrinterDlg.h"
#include "Extern.h"
#include "ConfigDlg.h"
#include "Export.h"
#include "Registry.h"
#include "AnsiString.h"
#include "..\Control\Host.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ItiLibrary;
using namespace FoxjetDatabase;
using namespace ListGlobals;

static bool IsNoPrompt ()
{
	CString str = ::GetCommandLine ();
		
	str.MakeLower ();

	return (str.Find (_T ("/no_prompt")) != -1) ? true : false;
}

CPrinterItem::CPrinterItem (FoxjetDatabase::PRINTERSTRUCT & printer)
:	m_printer (printer)
{
}

CString CPrinterItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0: return m_printer.m_strName;
	case 1: return m_printer.m_strAddress;
	case 2: return m_printer.m_bMaster ? LoadString (IDS_MASTER) : _T ("");
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CAddPrinterDlg dialog


CAddPrinterDlg::CAddPrinterDlg(CWnd* pParent /*=NULL*/)
:	CDialog(IDD_ADD_PRINTER_MATRIX /* IsMatrix () ? IDD_ADD_PRINTER_MATRIX : IDD_ADD_PRINTER */, pParent)
{
	//{{AFX_DATA_INIT(CAddPrinterDlg)
	//}}AFX_DATA_INIT
}


void CAddPrinterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CAddPrinterDlg)
	DDX_Text(pDX, TXT_NAME, m_strName);
	DDX_Text(pDX, TXT_ADDR, m_strAddr);
	//}}AFX_DATA_MAP
	
	//if (IsMatrix ()) 
	{
		m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
		m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	}
}


BEGIN_MESSAGE_MAP(CAddPrinterDlg, CDialog)
	//{{AFX_MSG_MAP(CAddPrinterDlg)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAddPrinterDlg message handlers

BOOL CAddPrinterDlg::OnInitDialog() 
{
	using namespace ListCtrlImp;
	BYTE n [4] = { 0 };

	CArray <CColumn, CColumn> vCols;

	CDialog::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_NAME),			120));
	vCols.Add (CColumn (LoadString (IDS_ADDRESS),		150));

	if (IsMasterMode ())
		vCols.Add (CColumn (LoadString (IDS_MASTER),	80));

	m_lv.Create (LV_PRINTERS, vCols, this, defElements.m_strRegSection);

	try 
	{
		CArray <PRINTERSTRUCT, PRINTERSTRUCT &> v;

		GetPrinterRecords (theApp.m_db, v);

		for (int i = 0; i < v.GetSize (); i++) {
			CPrinterItem * p = new CPrinterItem (v [i]);

			m_lv.InsertCtrlData (p);

			if (p->m_printer.m_bMaster)
				m_lv->SetItemState (m_lv.GetDataIndex (p), (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);	
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAddPrinterDlg::OnOK()
{
	if (CopySettings ())
		CDialog::OnOK ();
}

static int Find (CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> & v, const CString & strKey)
{
	for (int i = 0; i < v.GetSize (); i++) 
		if (!v [i].m_strKey.CompareNoCase (strKey))
			return i;

	return -1;
}

bool CAddPrinterDlg::CopySettings ()
{
	CLongArray sel = GetSelectedItems (m_lv);

	ASSERT (m_lv->GetItemCount () != 0);

	if (sel.GetSize () != 1) {
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
		return false;
	}

	if (CPrinterItem * pCopy = (CPrinterItem *)m_lv.GetCtrlData (sel [0])) {
		PRINTERSTRUCT printer, copy = pCopy->m_printer;
		CArray <LINESTRUCT, LINESTRUCT &> vLines;
		CString strCmdLine = ::GetCommandLine ();
		CArray <CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> *, CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> *>  vSettings;

		strCmdLine.MakeLower ();
		BeginWaitCursor ();

		if (CWnd * p = GetDlgItem (IDOK))
			p->EnableWindow (FALSE);
		//if (CWnd * p = GetDlgItem (IDCANCEL))
		//	p->EnableWindow (FALSE);

		printer.m_strName		= m_strName;
		printer.m_strAddress	= m_strAddr;
		printer.m_bMaster		= false;
		printer.m_type			= strCmdLine.Find (_T ("/matrix")) != -1 ? MATRIX : ELITE;

		{
			bool bFound = false;
			CArray <PRINTERSTRUCT, PRINTERSTRUCT &> vPrinters;

			GetPrinterRecords (theApp.m_db, vPrinters);

			for (int i = 0; i < vPrinters.GetSize (); i++) {
				PRINTERSTRUCT & p = vPrinters [i];

				if (!p.m_strAddress.CompareNoCase (m_strAddr)) {
					if (vPrinters.GetSize () == 1 && p.m_bMaster)
						return false;

					{
						CArray <LINESTRUCT, LINESTRUCT &> vLines;
				
						GetPrinterLines (theApp.m_db, p.m_lID, vLines);

						for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
							LINESTRUCT line = vLines [nLine];
							CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> * pv = new CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> ();

							GetSettingsRecords (theApp.m_db, line.m_lID, * pv);

							#ifdef _DEBUG
							for (int i = 0; i < pv->GetSize (); i++) {
								SETTINGSSTRUCT & s = pv->GetAt (i);

								TRACEF (ToString (s.m_lLineID) + _T (": ") + s.m_strKey + _T (": ") + s.m_strData);
							}
							#endif

							vSettings.Add (pv);
						}
					}

					DeletePrinterRecord (theApp.m_db, p.m_lID);
					AddPrinterRecord (theApp.m_db, printer);
					bFound = true;
					break;
				}
			}

			if (!bFound) 
				VERIFY (AddPrinterRecord (theApp.m_db, printer));
		}

		GetPrinterLines (theApp.m_db, copy.m_lID, vLines);

		for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
			LINESTRUCT line = vLines [nLine];
			CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
			CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;

			GetLineHeads (theApp.m_db, line.m_lID, vHeads);
			line.m_lPrinterID = printer.m_lID;

			VERIFY (AddLineRecord (theApp.m_db, line)); 
			TRACEF (ToString (line));
			GetPanelRecords (theApp.m_db, line.m_lID, vPanels);

			if (!nLine && strCmdLine.Find (_T ("/dsn_local")) != -1) {
				CStringArray v;
				SETTINGSSTRUCT s;
				CTime tm (2014, 1, 1, 0, 0, 0);

				v.Add (_T ("1"));
				v.Add (tm.Format (_T ("%c")));

				s.m_strKey	= _T ("LocalDB");
				s.m_lLineID = line.m_lID;
				s.m_strData = FoxjetDatabase::ToString (v);

				if (vSettings.GetSize ()) 
					if (Find (* vSettings [nLine], s.m_strKey) == -1) 
						vSettings [nLine]->Add (s);

				{
					CString strDSN = Extract (strCmdLine, _T ("/dsn="), _T (" "));
					CString strPath = FoxjetDatabase::GetProfileString (HKEY_LOCAL_MACHINE, _T ("SOFTWARE\\ODBC\\ODBC.INI\\") + strDSN, _T ("DBQ"), _T (""));
					int nDrive = ::PathGetDriveNumber (strPath);

					if (nDrive != -1) {
						CString strDrive;
						CArray <CUseInfo2, CUseInfo2 &> v;

						strDrive.Format (_T ("%c:"), 'A' + nDrive);
						EnumMappedDrives (v);

						for (int i = 0; i < v.GetSize (); i++) {
							CUseInfo2 & mapped = v [i];

							if (!mapped.m_strLocal.CompareNoCase (strDrive)) {
								strPath = mapped.m_strRemote + _T ("\\");
								break;
							}
						}
					}

					if (::PathIsUNC (strPath)) {
						CString str = Extract (strPath, _T ("\\\\"), _T ("\\"));
						
						if (struct hostent * hp = ::gethostbyname (w2a (str))) {
							struct sockaddr_in dest;
							memcpy(&(dest.sin_addr),hp->h_addr,hp->h_length);
							CString strAddr = a2w (inet_ntoa (dest.sin_addr));
							SETTINGSSTRUCT s;
							CStringArray v;

							v.Add (strAddr);
							v.Add (_T ("1"));
							v.Add (ToString ((int)(5 * 60)));

							s.m_strKey	= _T ("Network");
							s.m_lLineID = line.m_lID;
							s.m_strData = FoxjetDatabase::ToString (v);

							if (Find (* vSettings [nLine], s.m_strKey) == -1)
								vSettings [nLine]->Add (s);
						}
					}
				}
			}

			if (vSettings.GetSize () > nLine) {
				for (int i = 0; i < vSettings [nLine]->GetSize (); i++) {
					SETTINGSSTRUCT s = vSettings [nLine]->GetAt (i);

					TRACEF (ToString (s.m_lLineID) + _T (": ") + s.m_strKey + _T (": ") + s.m_strData);
					s.m_lLineID = line.m_lID;
					TRACEF (ToString (s.m_lLineID) + _T (": ") + s.m_strKey + _T (": ") + s.m_strData);

					if (!UpdateSettingsRecord (theApp.m_db, s))
						VERIFY (AddSettingsRecord (theApp.m_db, s));
				}
			}

			for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
				HEADSTRUCT head = vHeads [nHead];
				ULONG lAddr = _tcstoul (head.m_strUID, NULL, 16);
				bool bUSB = lAddr < 0x300;
				PANELSTRUCT panel;

				VERIFY (GetPanelRecord (theApp.m_db, head.m_lPanelID, panel));
				head.m_lPanelID = CConfigDlg::GetPanelID (vPanels, panel.m_nNumber);
				head.m_strUID.Format (_T ("%X"), bUSB ? GetSynonymUsbAddr (lAddr) : GetSynonymPhcAddr (lAddr));
				VERIFY (AddHeadRecord (theApp.m_db, head));
				TRACEF (ToString (head));
			}
		}

		/*
		{ // copy reg settings from first real printer
			CString strName, strAddr;
			CArray <PRINTERSTRUCT, PRINTERSTRUCT &> v;

			GetPrinterRecords (theApp.m_db, v);

			for (int i = 0; i < v.GetSize (); i++) {
				if (!v [i].m_bMaster) {
					if (v [i].m_strAddress.CompareNoCase (FoxjetDatabase::GetIpAddress ()) != 0) {
						strName = v [i].m_strName;
						strAddr = v [i].m_strAddress;
						break;
					}
				}
			}

			if (strAddr.GetLength ()) {
				CString str;

				str.Format (LoadString (IDS_COPYREGSETTINGSFROM), strName, strAddr);

				int nResult = IsNoPrompt () ? IDYES : MsgBox (* this, str, MB_YESNO);

				if (nResult == IDYES) {
					bool bImported = false;

					while (!bImported) {
						bImported = DoRegistryImport (strAddr);

						if (!bImported) {
							CString str;

							str.Format (LoadString (IDS_REGIMPORTFAILED), strName, strAddr);

							if (MsgBox (* this, str, MB_YESNO) == IDNO)
								return false;
						}
					}
				}
			}
		}
		*/

		for (int i = 0; i < vSettings.GetSize (); i++) {
			if (CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> * pv = vSettings [i])
				delete pv;
		}

		EndWaitCursor ();
	}

	return true;
}

bool CAddPrinterDlg::DoRegistryImport (const CString & strAddr)
{
	bool bResult = false;
	SOCKADDR_IN target;

	target.sin_family = AF_INET; 
	target.sin_port = htons (BW_HOST_TCP_PORT); 
	target.sin_addr.s_addr = inet_addr (w2a (strAddr));

	SOCKET s = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP); 

	if (connect(s, (SOCKADDR *)&target, sizeof(target)) != SOCKET_ERROR) {
		int iOption = 1;
		ULONG lRet;
		lRet = setsockopt (s, SOL_SOCKET, SO_KEEPALIVE, (char*)&iOption, sizeof (iOption));
		lRet = setsockopt (s, SOL_SOCKET, SO_REUSEADDR, (char*)&iOption, sizeof (iOption));
		CString strReply;
		CString str = _T ("{RegExport}");
		const CTime tmStart = CTime::GetCurrentTime ();

		while (IsSocketDataPending (s)) { // clear any pending data
			char sz [128] = { 0 };

			memset (&sz, 0, sizeof (sz));
			recv (s, sz, sizeof (sz), 0);
			
			#ifdef _DEBUG
			::OutputDebugStringA (sz);
			#endif
		}

		DWORD dwSend = send (s, (LPCSTR)w2a (str), str.GetLength (), 0);

		while (1) { 
			CTimeSpan tm = CTime::GetCurrentTime () - tmStart;
			char sz [128] = { 0 };

			if (tm.GetTotalMinutes () >= 3) {
				MsgBox (* this, LoadString (IDS_TIMEOUT), MB_ICONERROR);
				return false;
			}

			memset (&sz, 0, sizeof (sz));
			recv (s, sz, sizeof (sz) - 1, 0);
			//ntohl (sz);
			
			#ifdef _DEBUG
			::OutputDebugStringA (sz);
			#endif

			strReply += a2w (sz);

			FoxjetCommon::CLongArray left = CountChars (strReply, '{');
			FoxjetCommon::CLongArray right = CountChars (strReply, '}');

			if (left.GetSize () == 1 && right.GetSize () == 1)
				break;
		}

		const CString strFile = FoxjetDatabase::GetHomeDir () + _T ("\\FoxJet.registry");
		CStringArray vCmd;
		
		Tokenize (strReply, vCmd);

		if (vCmd.GetSize () > 1) {
			CString strMIME = vCmd [1];

			int nReplace1 = strMIME.Replace (_T ("\n"), _T (""));
			int nReplace2 = strMIME.Replace (_T ("\r"), _T (""));
			int n = strMIME.GetLength ();
			BYTE * p = new BYTE [n];
			n = FoxjetDatabase::DecodeBase64 ((unsigned char *)(LPSTR)w2a (strMIME), p, n);

			::DeleteFile (strFile);

			if (FILE * fp = _tfopen (strFile, _T ("wb"))) {
				fwrite (p, n, 1, fp);
				fclose (fp);
			}

			delete [] p;

			if (::GetFileAttributes (strFile) != -1) {
				HKEY hKey = NULL;
				CString strKey = _T ("Software\\FoxJet");

				VERIFY (EnablePrivilege (SE_RESTORE_NAME));
				VERIFY (EnablePrivilege (SE_BACKUP_NAME));

				LONG lResult = ::SHDeleteKey (HKEY_CURRENT_USER, strKey);

				if (lResult != ERROR_SUCCESS) 
					MsgBox (* this, _T ("SHDeleteKey:\n") + FormatMessage (lResult), MB_ICONERROR);
				else {
					if (::RegCreateKey (HKEY_CURRENT_USER, strKey, &hKey) != ERROR_SUCCESS) 
						MsgBox (* this, _T ("RegCreateKey:\n") + FormatMessage (lResult), MB_ICONERROR);
					else {
						lResult = ::RegRestoreKey (hKey, strFile, 0);

						if (lResult != ERROR_SUCCESS) 
							MsgBox (* this, _T ("RegRestoreKey:\n") + FormatMessage (lResult), MB_ICONERROR);
						
						bResult = lResult == ERROR_SUCCESS ? 1 : 0;
						::RegCloseKey (hKey);
					}
				}
			}
		}

		::DeleteFile (strFile);
		closesocket (s);
	}

	return bResult;
}

void CAddPrinterDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	if (bShow) {
		if (IsNoPrompt ()) {
			OnOK ();
			theApp.m_db.Close ();
			exit (0);
		}
	}
}
