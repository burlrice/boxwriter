// PrinterListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "Config.h"
#include "PrinterListCtrl.h"
#include "Color.h"
#include "xImage.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrinterListCtrl

CPrinterListCtrl::CPrinterListCtrl()
:	CListCtrl ()
{
	const int nSize = 160;//128;
	CxImage imgMatrix, imgElite;

	m_imglist.Create (nSize, nSize, ILC_COLOR32, 0, 6);

	m_bmpMaster.LoadBitmap (IDB_MASTER);
	m_bmpMatrix.LoadBitmap (IDB_MATRIX);
	m_bmpElite.LoadBitmap (IDB_ELITE);

	COLORREF rgb =	Resize (m_bmpMaster, nSize);
					Resize (m_bmpMatrix,	nSize);
					Resize (m_bmpElite,	nSize);

	m_nMasterIndex	= m_imglist.Add (&m_bmpMaster,	rgb); //(m_hPrinterMaster);
	m_nMatrixIndex	= m_imglist.Add (&m_bmpMatrix,	rgb); //(m_hPrinter);
	m_nEliteIndex	= m_imglist.Add (&m_bmpElite,	rgb); //(m_hPrinter);


	imgMatrix.CreateFromHBITMAP (m_bmpMatrix);
	imgMatrix.GrayScale ();
	imgMatrix.Light (100);
	m_bmpMatrixDisabled.Attach (imgMatrix.MakeBitmap ());

	imgElite.CreateFromHBITMAP (m_bmpElite);
	imgElite.GrayScale ();
	imgElite.Light (75);
	m_bmpEliteDisabled.Attach (imgElite.MakeBitmap ());

	m_nMatrixDisabledIndex	= m_imglist.Add (&m_bmpMatrixDisabled,	rgb); 
	m_nEliteDisabledIndex	= m_imglist.Add (&m_bmpEliteDisabled,	rgb); 

	m_bmpState.LoadBitmap (IDB_STROBE);
	{
		CxImage imgRed, imgYellow, imgGreen, imgGray;

		imgRed.CreateFromHBITMAP (m_bmpState);
		imgYellow.CreateFromHBITMAP (m_bmpState);
		imgGreen.CreateFromHBITMAP (m_bmpState);
		imgGray.CreateFromHBITMAP (m_bmpState);
		
		imgRed.GrayScale ();
		imgYellow.GrayScale ();
		imgGreen.GrayScale ();
		imgGray.GrayScale ();

		imgRed.Colorize		(0,		255, 1.0);
		imgYellow.Colorize	(44,	255, 1.0);
		imgGreen.Colorize	(66,	255, 1.0);

		m_bmpStateRed.Attach (imgRed.MakeBitmap ());
		m_bmpStateYellow.Attach (imgYellow.MakeBitmap ());
		m_bmpStateGreen.Attach (imgGreen.MakeBitmap ());
		m_bmpStateGray.Attach (imgGray.MakeBitmap ());
	}

	m_bmpRunning.LoadBitmap (IDB_RUNNING);
	m_bmpIdle.LoadBitmap (IDB_PAUSED);
	m_bmpStopped.LoadBitmap (IDB_STOPPED);
	m_bmpStrobeGray.LoadBitmap (IDB_STROBE_GRAY);
	m_bmpStrobeRed.LoadBitmap (IDB_STROBE_RED);
	m_bmpStrobeYellow.LoadBitmap (IDB_STROBE_YELLOW);
	m_bmpStrobeGreen.LoadBitmap (IDB_STROBE_GREEN);
}

CPrinterListCtrl::~CPrinterListCtrl()
{
}


BEGIN_MESSAGE_MAP(CPrinterListCtrl, CListCtrl)
	//{{AFX_MSG_MAP(CPrinterListCtrl)
	ON_WM_DRAWITEM()
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnCustomDraw)
	ON_NOTIFY_REFLECT(NM_RCLICK, OnRightClick)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrinterListCtrl message handlers

void CPrinterListCtrl::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{	
	CListCtrl::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

LRESULT CPrinterListCtrl::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	return CListCtrl::WindowProc(message, wParam, lParam);
}

void DrawOutlineText(CDC & dc, CRect & rc, const CString& str, CFont * pFont)
{
	const int nRestorePoint = dc.SaveDC();
	LOGBRUSH lf = { 0 };
	CPen pen;

	lf.lbColor = FoxjetUtils::GetButtonColor (_T ("config::text::outline")); //Color::rgbLightGray;
	lf.lbHatch = HS_CROSS;
	lf.lbStyle = BS_SOLID;

	dc.SelectObject (pFont);
	dc.SetBkMode (TRANSPARENT);
	dc.SetTextColor (FoxjetUtils::GetButtonColor (_T ("config::text::fill"))); //(Color::rgbBlue);

	pen.CreatePen (PS_GEOMETRIC | PS_SOLID, 2, &lf, 0, 0);
	dc.SelectObject (&pen);

	dc.BeginPath();
	dc.DrawText (str, rc, 0);
	dc.EndPath();
	dc.StrokePath();

	dc.DrawText (str, rc, 0);
	dc.RestoreDC (nRestorePoint);
}

void CPrinterListCtrl::DrawIcon (CDC * pDC, int nItem)
{
	LVITEM   rItem;
    //BOOL     bListHasFocus;
    CRect    rcItem;
    CRect    rcText;
    CString  sText;
    UINT     uFormat;
	const ULONG lPrinterID = GetItemData (nItem);

    //bListHasFocus = ( GetSafeHwnd() == ::GetFocus() );
    
    // Get the image index and selected/focused state of the
    // item being drawn.
    ZeroMemory ( &rItem, sizeof(LVITEM) );
    rItem.mask  = LVIF_IMAGE | LVIF_STATE;
    rItem.iItem = nItem;
    rItem.stateMask = LVIS_SELECTED | LVIS_FOCUSED;
    GetItem ( &rItem );

    // Get the rect that holds the item's icon.
    GetItemRect (nItem, &rcItem, LVIR_ICON);
	rcItem -= rcItem.TopLeft ();

    // Draw the icon.
    uFormat = ILD_TRANSPARENT;

    //if ((rItem.state & LVIS_SELECTED) && bListHasFocus)
	//	uFormat |= ILD_FOCUS;

	//int nImage = rItem.iImage;
	CString strTask, strCount, strState, strType, strConnected;
	CStringArray vStrobe;
	bool bFlash = false;

	int nImage = GetState (lPrinterID, strConnected, strType);
	m_imglist.Draw (pDC, nImage, rcItem.TopLeft(), uFormat);

	pDC->FrameRect (rcItem, &CBrush (::GetSysColor ((rItem.state & LVIS_SELECTED) ? COLOR_HIGHLIGHT : COLOR_WINDOW)));
	
	if (!strType.CompareNoCase (PRINTERCTRL_MATRIX) ||
		!strType.CompareNoCase (PRINTERCTRL_ELITE)) 
	{
		for (int nLine = 0; nLine < 2; nLine++) {
			LOGFONT lf;
			CString strKey;

			m_fntStatus.GetLogFont (&lf);
			int cy = abs (lf.lfHeight) * 3;

			strKey.Format (_T ("%d:task[%d]"), lPrinterID, nLine);
			strState.Empty ();
			m_mapData.Lookup (strKey, strTask);

			strKey.Format (_T ("%d:count[%d]"), lPrinterID, nLine);
			strState.Empty ();
			m_mapData.Lookup (strKey, strCount);

			strKey.Format (_T ("%d:state[%d]"), lPrinterID, nLine);
			strState.Empty ();
			m_mapData.Lookup (strKey, strState);

			{
				CString str;
				strKey.Format (_T ("%d:strobe"), lPrinterID);
				m_mapData.Lookup (strKey, str);
				FromString (str, vStrobe);
			}

			{
				CString str;
				strKey.Format (_T ("%d:strobe_current"), lPrinterID);
				m_mapData.Lookup (strKey, str);
				bFlash = _ttoi (str) ? true : false;
			}

			if (!strConnected.CompareNoCase (PRINTERCTRL_DISCONNECTED)) {
				strTask = LoadString (IDS_DISCONNECTED);
				strCount.Empty ();

				if (nLine > 0)
					break;
			}

			if (strTask.IsEmpty ())
				strCount.Empty ();

			CRect rc = rcItem;

			rc += CPoint (32, 32 + (nLine * cy));
			DrawOutlineText (* pDC, rc, strTask + _T ("\n") + strCount, &m_fntStatus);


			if (strTask.CompareNoCase (PRINTERCTRL_DISCONNECTED) != 0) 
			{
				CDC dcMem;
				CArray <CBitmap *, CBitmap *> vBmp;

				vBmp.Add (&m_bmpStrobeGray);

				dcMem.CreateCompatibleDC (pDC);

				for (int nStrobe = 0; nStrobe < vStrobe.GetSize (); nStrobe++) {
					CString str = vStrobe [nStrobe];

					if (!str.CompareNoCase (_T ("Unknown command"))) {
						vBmp.RemoveAll ();
						break;
					}

					if (!str.CompareNoCase (_T ("GREEN_STROBE")))
						vBmp.Add (&m_bmpStrobeGreen);
					else if (!str.CompareNoCase (_T ("YELLOW_STROBE")))
						vBmp.Add (&m_bmpStrobeYellow);
					else if (!str.CompareNoCase (_T ("RED_STROBE")))
						vBmp.Add (&m_bmpStrobeRed);

					if (bFlash) {
						if (!str.CompareNoCase (_T ("GREEN_STROBE_FLASH")))
							vBmp.Add (&m_bmpStrobeGreen);
						else if (!str.CompareNoCase (_T ("YELLOW_STROBE_FLASH")))
							vBmp.Add (&m_bmpStrobeYellow);
						else if (!str.CompareNoCase (_T ("RED_STROBE_FLASH")))
							vBmp.Add (&m_bmpStrobeRed);
					}
				}

				for (int nStrobe = 0; nStrobe < vBmp.GetSize (); nStrobe++) {
					DIBSECTION ds;
					CBitmap * pbmp = vBmp [nStrobe];
					CRect rc = rcItem;
					int n = 32;

					rc.left = rc.right - n;
					rc.top = rc.bottom - n;
					memset (&ds, 0, sizeof (ds));
					pbmp->GetObject (sizeof (ds), &ds);

					CBitmap * pOld = dcMem.SelectObject (pbmp);
					COLORREF rgb = dcMem.GetPixel (0, 0);
					::TransparentBlt (pDC->m_hDC, rc.left, rc.top, rc.Width (), rc.Height (), dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, rgb);
					dcMem.SelectObject (pOld);
				}

				{
					CBitmap * pbmp = &m_bmpStateGray;
					DIBSECTION ds;
					int n = 24;
					
					strKey.Format (_T ("%d:HEADSTATE[%d]"), lPrinterID, nLine);
					strState.Empty ();
					m_mapData.Lookup (strKey, strState);
					//TRACEF (strKey + _T (": ") + strState);

					memset (&ds, 0, sizeof (ds));

					if (!strState.CompareNoCase (_T ("{OK}")))
						pbmp = &m_bmpStateGreen;
					else if (
						strState.Find (_T ("LOW TEMP"))				!= -1 ||
						strState.Find (_T ("HIGH VOLTAGE ERROR"))	!= -1 ||
						strState.Find (_T ("OUT OF INK"))			!= -1 ||
						strState.Find (_T ("EP8"))					!= -1)
					{
						pbmp = &m_bmpStateRed;
					}
					else if (strState.Find (_T ("LOW INK")) != -1) 
						pbmp = &m_bmpStateYellow;

					pbmp->GetObject (sizeof (ds), &ds);
					int cx = ds.dsBm.bmWidth;
					int cy = ds.dsBm.bmHeight;
					CBitmap * pOld = dcMem.SelectObject (pbmp);
					COLORREF rgb = dcMem.GetPixel (0, 0);
					::TransparentBlt (pDC->m_hDC, 0, rc.top, n, n, dcMem, 0, 0, cx, cy, rgb);
					dcMem.SelectObject (pOld);
				}

				{
					CBitmap * pbmp = NULL;

					strKey.Format (_T ("%d:TASKSTATE[%d]"), lPrinterID, nLine);
					strState.Empty ();
					m_mapData.Lookup (strKey, strState);
					//TRACEF (strKey + _T (": ") + strState);

					TASKSTATE eState = (TASKSTATE)_tcstoul (strState, NULL, 0);

					switch (eState) {
					case RUNNING:	pbmp = &m_bmpRunning;		break;
					case IDLE:		pbmp = &m_bmpIdle;			break;
					case STOPPED:	pbmp = &m_bmpStopped;		break;
					}

					if (pbmp) {
						DIBSECTION ds;
						int n = 24;

						memset (&ds, 0, sizeof (ds));
						pbmp->GetObject (sizeof (ds), &ds);
						int cx = ds.dsBm.bmWidth;
						int cy = ds.dsBm.bmHeight;
						CBitmap * pOld = dcMem.SelectObject (pbmp);
						COLORREF rgb = dcMem.GetPixel (0, 0);
						::TransparentBlt (pDC->m_hDC, 0, rc.top + n, n, n, dcMem, 0, 0, cx, cy, rgb);
						dcMem.SelectObject (pOld);
					}
				}
			}
		}
	}
}

void CPrinterListCtrl::DrawText (CDC * pDC, int nItem)
{
    COLORREF crBkgnd;
	LVITEM   rItem;
    CRect    rcItem;
    CRect    rcText;
    CString  sText;
    BOOL bListHasFocus = ( GetSafeHwnd() == ::GetFocus() );

    ZeroMemory ( &rItem, sizeof(LVITEM) );
    rItem.mask  = LVIF_IMAGE | LVIF_STATE;
    rItem.iItem = nItem;
    rItem.stateMask = LVIS_SELECTED | LVIS_FOCUSED;
    GetItem ( &rItem );

    // Get the rect that bounds the text label.
    GetItemRect ( nItem, rcItem, LVIR_LABEL );
	rcItem -= rcItem.TopLeft ();


    // Draw the background of the list item.  Colors are selected 
    // according to the item's state.

    if (rItem.state & LVIS_SELECTED) {
        if (bListHasFocus) {
            crBkgnd = GetSysColor (COLOR_HIGHLIGHT);
            pDC->SetTextColor (GetSysColor (COLOR_HIGHLIGHTTEXT));
        }
		else {
            crBkgnd = GetSysColor (COLOR_BTNFACE);
            pDC->SetTextColor (GetSysColor (COLOR_BTNTEXT));
		}
	}
    else {
        crBkgnd = GetSysColor (COLOR_WINDOW);
        pDC->SetTextColor (GetSysColor (COLOR_BTNTEXT));
	}

    // Draw the background & prep the DC for the text drawing.  Note
    // that the entire item RECT is filled in, so this emulates the full-
    // row selection style of normal lists.
    pDC->FillSolidRect (rcItem, crBkgnd);
    pDC->SetBkMode (TRANSPARENT);


    // Tweak the rect a bit for nicer-looking text alignment.
    rcText = rcItem;
    rcText.left += 3;
    rcText.top++;

    // Draw the text.
    sText = GetItemText (nItem, 0);

    pDC->DrawText (sText, rcText, DT_VCENTER | DT_SINGLELINE);
    
    // Draw a focus rect around the item if necessary.
    if (bListHasFocus && (rItem.state & LVIS_FOCUSED)) 
        pDC->DrawFocusRect (rcItem);
}

void CPrinterListCtrl::Draw (CDC * pDC, int nItem)
{
	CRect rc, rcIcon, rcLabel;
	CDC dcMem;
	CBitmap bmpMem;

	GetItemRect (nItem, &rc, LVIR_BOUNDS);
	GetItemRect (nItem, &rcIcon, LVIR_ICON);
	GetItemRect (nItem, rcLabel, LVIR_LABEL);

	CPoint ptOffset = rc.TopLeft ();

	rc -= ptOffset;
	rcIcon -= ptOffset;
	rcLabel -= ptOffset;

	dcMem.CreateCompatibleDC (pDC);
	bmpMem.CreateCompatibleBitmap (pDC, rc.Width (), rc.Height ());

	CBitmap * pBitmap = dcMem.SelectObject (&bmpMem);

	::BitBlt (dcMem, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
	DrawIcon (&dcMem, nItem);
	pDC->BitBlt (rcIcon.left, rcIcon.top, rcIcon.Width (), rcIcon.Height (), &dcMem, 0, 0, SRCCOPY);

	::BitBlt (dcMem, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
	DrawText (&dcMem, nItem);
	pDC->BitBlt (rcLabel.left, rcLabel.top, rcLabel.Width (), rcLabel.Height (), &dcMem, 0, 0, SRCCOPY);

	dcMem.SelectObject (pBitmap);
}

void CPrinterListCtrl::OnCustomDraw(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );
    
    * pResult = 0;

    if (pLVCD->nmcd.dwDrawStage == CDDS_PREPAINT)
        * pResult = CDRF_NOTIFYITEMDRAW;
    else if (pLVCD->nmcd.dwDrawStage == CDDS_ITEMPREPAINT) {
        // This is the beginning of an item's paint cycle.
        int      nItem = static_cast<int>( pLVCD->nmcd.dwItemSpec );
        CDC*     pDC   = CDC::FromHandle ( pLVCD->nmcd.hdc );
		CRect rc;
		CDC dcMem;
		CBitmap bmpMem;

		GetItemRect (nItem, &rc, LVIR_BOUNDS);

		dcMem.CreateCompatibleDC (pDC);
		bmpMem.CreateCompatibleBitmap (pDC, rc.Width (), rc.Height ());

		CBitmap * pBitmap = dcMem.SelectObject (&bmpMem);

		::BitBlt (dcMem, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
		Draw (&dcMem, nItem);
		pDC->BitBlt (rc.left, rc.top, rc.Width (), rc.Height (), &dcMem, 0, 0, SRCCOPY);

		dcMem.SelectObject (pBitmap);

        * pResult = CDRF_SKIPDEFAULT;    // We've painted everything.
	}
}

BOOL CPrinterListCtrl::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	BOOL bResult = CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	return bResult;
}

int CPrinterListCtrl::GetIndex(ULONG lPrinterID)
{
	for (int i = 0; i < GetItemCount (); i++)
		if (GetItemData (i) == lPrinterID)
			return i;

	return -1;
}

int CPrinterListCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}

void CPrinterListCtrl::Init()
{
}

int CPrinterListCtrl::GetState (ULONG lPrinterID, CString & strConnected, CString & strType, int nLine, CString & strTask, CString & strCount, CString & strState)
{
	int nImage = 0;
	CString strKey;

	strKey.Format (_T ("%d:type"), lPrinterID);
	m_mapData.Lookup (strKey, strType);

	strKey.Format (_T ("%d:connected"), lPrinterID);
	m_mapData.Lookup (strKey, strConnected);

	if (strConnected.Find (PRINTERCTRL_CONNECTED) == -1)
		strConnected = PRINTERCTRL_DISCONNECTED;

	if (!strType.CompareNoCase (PRINTERCTRL_MASTER))
		nImage = m_nMasterIndex;
	else if (!strType.CompareNoCase (PRINTERCTRL_MATRIX))
		nImage = !strConnected.CompareNoCase (PRINTERCTRL_DISCONNECTED) ? m_nMatrixDisabledIndex : m_nMatrixIndex;
	else if (!strType.CompareNoCase (PRINTERCTRL_ELITE))
		nImage = !strConnected.CompareNoCase (PRINTERCTRL_DISCONNECTED) ? m_nEliteDisabledIndex : m_nEliteIndex;
			
	//TRACEF (ToString (lPrinterID) + _T (": ") + strType + _T (", ") + strConnected);
	//TRACEF (ToString (lPrinterID) + _T (": ") + ToString (nImage));

	strKey.Format (_T ("%d:task[%d]"), lPrinterID, nLine);
	m_mapData.Lookup (strKey, strTask);

	strKey.Format (_T ("%d:count[%d]"), lPrinterID, nLine);
	m_mapData.Lookup (strKey, strCount);

	strKey.Format (_T ("%d:state[%d]"), lPrinterID, nLine);
	m_mapData.Lookup (strKey, strState);

	return nImage;
}

void CPrinterListCtrl::OnRightClick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CLongArray sel = ItiLibrary::GetSelectedItems (* this);
	const int nStart []		= { ID_LINE001_START,	ID_LINE002_START };
	const int nStop []		= { ID_LINE001_STOP,	ID_LINE002_STOP };
	const int nIdle []		= { ID_LINE001_IDLE,	ID_LINE002_IDLE };
	const int nResume []	= { ID_LINE001_RESUME,	ID_LINE002_RESUME };

	if (sel.GetSize ()) {
		CMenu menu;

		if (menu.LoadMenu (IDM_START)) {
			CString strLine [2];
			CString strType, strConnected;
			const ULONG lPrinterID = GetItemData (sel [0]);
			CPoint pt;

			::GetCursorPos (&pt);

			m_mapData.Lookup (ToString (lPrinterID) + _T (":line[0]"), strLine [0]);
			m_mapData.Lookup (ToString (lPrinterID) + _T (":line[1]"), strLine [1]);

			int nImage = GetState (lPrinterID, strConnected, strType);

//if (strLine [1].IsEmpty ()) strLine [1] = _T ("TODO");

			//if (nImage == m_nMatrixIndex || nImage == m_nEliteIndex) 
			{
				if (CMenu * pMenu = menu.GetSubMenu (0)) {

					pMenu->ModifyMenu (0, MF_BYPOSITION | MF_STRING, 0, strLine [0]); 
					pMenu->ModifyMenu (1, MF_BYPOSITION | MF_STRING, 1, strLine [1]); 

					for (int nLine = 0; nLine < 2; nLine++) {
						CString strKey, strTask, strCount, strState;

						GetState (lPrinterID, strConnected, strType, nLine, strTask, strCount, strState);
						//TRACEF (ToString (nLine) + _T (": ") + strTask + _T (", ") + strCount + _T (", ") + strState);

						if (CMenu * pSubMenu = pMenu->GetSubMenu (nLine)) {
							strKey.Format (_T ("%d:TASKSTATE[%d]"), lPrinterID, nLine);
							m_mapData.Lookup (strKey, strState);
							TASKSTATE eState = (TASKSTATE)_tcstoul (strState, NULL, 0);

							switch (eState) {
							case RUNNING:	
								pSubMenu->EnableMenuItem (nResume	[nLine], MF_GRAYED | MF_BYCOMMAND);
								break;
							case IDLE:		
								pSubMenu->EnableMenuItem (nIdle		[nLine], MF_GRAYED | MF_BYCOMMAND);
								break;
							case STOPPED:	
								pSubMenu->EnableMenuItem (nIdle		[nLine], MF_GRAYED | MF_BYCOMMAND);
								pSubMenu->EnableMenuItem (nResume	[nLine], MF_GRAYED | MF_BYCOMMAND);
								pSubMenu->EnableMenuItem (nStop		[nLine], MF_GRAYED | MF_BYCOMMAND);
								break;
							default:
								pSubMenu->EnableMenuItem (nStart	[nLine], MF_GRAYED | MF_BYCOMMAND);
								pSubMenu->EnableMenuItem (nIdle		[nLine], MF_GRAYED | MF_BYCOMMAND);
								pSubMenu->EnableMenuItem (nResume	[nLine], MF_GRAYED | MF_BYCOMMAND);
								pSubMenu->EnableMenuItem (nStop		[nLine], MF_GRAYED | MF_BYCOMMAND);
								break;
							}
						}
					}

					if (strLine [1].IsEmpty ())
						pMenu = pMenu->GetSubMenu (0);

					if (pMenu)
						pMenu->TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON, pt.x, pt.y, GetParent ());
				}
			}
		}
	}

	if (pResult)
		* pResult = 0;
}