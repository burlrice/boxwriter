#if !defined(AFX_PRINTERTYPEDLG_H__1D1C4BB4_B379_4CB6_9ECC_4CB99838265B__INCLUDED_)
#define AFX_PRINTERTYPEDLG_H__1D1C4BB4_B379_4CB6_9ECC_4CB99838265B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrinterTypeDlg.h : header file
//

#include "resource.h"
#include "RoundButtons.h"

/////////////////////////////////////////////////////////////////////////////
// CPrinterTypeDlg dialog

class CPrinterTypeDlg : public CDialog
{
// Construction
public:
	CPrinterTypeDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPrinterTypeDlg)
	enum { IDD = IDD_PRINTER_TYPE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	PRINTERTYPE m_type;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrinterTypeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	CBitmap m_bmpElite;
	CBitmap m_bmpMatrix;
	FoxjetUtils::CRoundButtons m_buttons;

// Implementation
protected:

	int m_nType;

	// Generated message map functions
	//{{AFX_MSG(CPrinterTypeDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnType ();
	afx_msg void OnTypeTxt();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTERTYPEDLG_H__1D1C4BB4_B379_4CB6_9ECC_4CB99838265B__INCLUDED_)
