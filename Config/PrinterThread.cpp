// PrinterThread.cpp : implementation file
//

#include "stdafx.h"
#include "Config.h"
#include "PrinterThread.h"
#include "..\..\Control\Host.h"
#include "AnsiString.h"
#include "PrinterListCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//////////////////////////////////////////////////////////////////////////////////////////

#pragma data_seg( "mapPrinters" )
	static CRITICAL_SECTION csPrinters;
	CMap <ULONG, ULONG, PRINTERSTRUCT, PRINTERSTRUCT> mapPrinters;
#pragma

void CPrinterThread::InitializeCriticalSection ()
{
	::InitializeCriticalSection (&::csPrinters);
}

void CPrinterThread::DeleteCriticalSection ()
{
	::DeleteCriticalSection (&::csPrinters);
}

bool CPrinterThread::GetPrinter (ULONG lPrinterID, PRINTERSTRUCT & printer) 
{
	bool bResult = false;

	::EnterCriticalSection (&::csPrinters);
	bResult = ::mapPrinters.Lookup (lPrinterID, printer) ? true : false;
	::LeaveCriticalSection (&::csPrinters);

	return bResult;
}

void CPrinterThread::SetPrinter (ULONG lPrinterID, const PRINTERSTRUCT & printer) 
{
	::EnterCriticalSection (&::csPrinters);
	::mapPrinters.SetAt (lPrinterID, printer);
	::LeaveCriticalSection (&::csPrinters);
}

//////////////////////////////////////////////////////////////////////////////////////////

int Find (CArray <CPrinterThread *, CPrinterThread *> & v, ULONG lPrinterID)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i]->m_lPrinterID == lPrinterID)
			return i;

	return -1;
}

int Find (CArray <PRINTERSTRUCT, PRINTERSTRUCT &> & v, ULONG lPrinterID)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i].m_lID == lPrinterID)
			return i;

	return -1;
}

static ULONG lCall = 0;
static CCriticalSection csCall;

//////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CPrinterThread

IMPLEMENT_DYNCREATE(CPrinterThread, CWinThread)

CPrinterThread::CPrinterThread()
:	m_lPrinterID (-1),
	m_hInit (NULL),
	m_socket (INVALID_SOCKET),
	m_hPrinterWnd (NULL),
	m_bRun (true)
{
	static DWORD dw = 0;
	CString str;

	m_bAutoDelete = TRUE;
	str.Format (_T ("CPrinterThread::m_hInit%d"), dw++);
	m_hInit = ::CreateEvent (NULL, true, false, str);
}

CPrinterThread::~CPrinterThread()
{
}

BOOL CPrinterThread::InitInstance()
{
	m_dwThreadID = m_nThreadID;
	::SetEvent (m_hInit);
	return TRUE;
}

int CPrinterThread::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CPrinterThread, CWinThread)
	//{{AFX_MSG_MAP(CPrinterThread)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrinterThread message handlers

DWORD CPrinterThread::GetThreadID ()
{
	if (const CWinThread * p = dynamic_cast <const CWinThread *> (this)) 
		return p->m_nThreadID;
	
	return m_dwThreadID;
}

void CPrinterThread::PostMessage (UINT nMsg, WPARAM wParam, LPCTSTR lpszFile, ULONG lLine)
{
	DWORD dwID = GetThreadID ();

	if (dwID == 0) {
		#ifdef _DEBUG
		CString str; 
		
		str.Format (_T ("CHeadInfo::PostMessage: thread id: %d"), GetThreadID ()); 
		CDebug::Trace (str, true, lpszFile, lLine);
		#endif
		return;
	}

	#ifdef __DEBUG_SENDMESSAGE__
	{ CString str; str.Format (_T ("CHeadInfo::PostMessage: thread id: %d: %s (wParam: %d)"), GetThreadID (), CHead::GetMessageString (nMsg), wParam); CDebug::Trace (str, true, lpszFile, lLine); }
	#endif

	if (ISPOSTMESSAGESTRUCT (nMsg)) {
		POSTMESSAGESTRUCT * p = new POSTMESSAGESTRUCT (true, lpszFile, lLine, NULL);

		while (!::PostThreadMessage (dwID, nMsg, wParam, (LPARAM)p)) 
			::Sleep (0); 
	}
	else {
		while (!::PostThreadMessage (dwID, nMsg, wParam, 0)) 
			::Sleep (0); 
	}
}

void CPrinterThread::SendMessage (UINT nMsg, WPARAM wParam, LPCTSTR lpszFile, ULONG lLine)
{
	int nDiagnostic = nMsg - WM_APP;
	CString str;
	DWORD dwID = GetThreadID ();
	DWORD dwTimeout = INFINITE;//CHead::m_dwExitTimeout;

	if (dwID == 0) {
		#ifdef _DEBUG
		CString str; 
		
		str.Format (_T ("CHeadInfo::SendMessage: thread id: %d"), GetThreadID ()); 
		CDebug::Trace (str, true, lpszFile, lLine);
		#endif
		return;
	}

	#ifdef __DEBUG_SENDMESSAGE__
	{ CString str; str.Format (_T ("CHeadInfo::SendMessage: thread id: %d: %s (wParam: %d)"), GetThreadID (), CHead::GetMessageString (nMsg), wParam); CDebug::Trace (str, true, lpszFile, lLine); }
	#endif

	/*
	#ifdef _DEBUG
	{
		switch (nMsg) {
		case TM_GET_ERROR_STATE:
			break;
		default:
			{
				CString strTmp = CTime::GetCurrentTime ().Format (_T ("%c: ")) + _T ("CHeadInfo::SendMessage: ") + CHead::GetMessageString (nMsg);

				strTmp.TrimLeft ();
				strTmp.TrimRight ();
				strTmp += _T ("...");
				TRACEF (strTmp);
			}
			break;
		}
	}
	#endif
	*/

	{
		VERIFY (CSingleLock (&::csCall).Lock ());
		str.Format (_T ("%lu:%X:%X:%X"), dwID, nMsg, wParam, ::lCall++);
		//TRACEF (str);
	}

	HANDLE hEvent = ::CreateEvent (NULL, TRUE, FALSE, str);
	POSTMESSAGESTRUCT * p = new POSTMESSAGESTRUCT (false, lpszFile, lLine, hEvent);

	// if it's already signalled, something is wrong
	ASSERT (::WaitForSingleObject (hEvent, 0) != WAIT_OBJECT_0);

	while (!::PostThreadMessage (dwID, nMsg, wParam, (LPARAM)p)) 
		::Sleep (0);
	
	DWORD dwWait = ::WaitForSingleObject (hEvent, dwTimeout);
	
	#ifdef _DEBUG
	{
		CString str;

		str.Format (_T ("SendMessage [%d]: %d (0x%p): "), dwID, nMsg, wParam);

		if (dwWait == WAIT_TIMEOUT)		str += _T ("WAIT_TIMEOUT");
		if (dwWait == WAIT_ABANDONED)	str += _T ("WAIT_ABANDONED");
		if (dwWait == WAIT_OBJECT_0)	str += _T ("WAIT_OBJECT_0");

		if (dwWait != WAIT_OBJECT_0)
			CDebug::Trace (str, true, lpszFile, lLine);
	}
	#endif
	
	::CloseHandle (hEvent);
}


BOOL CPrinterThread::PreTranslateMessage(MSG* pMsg) 
{
	bool bHandled = false;

	if (pMsg) {
		#ifdef _DEBUG
		{
			CString str;
			str.Format (_T ("CPrinterThread::PreTranslateMessage: pMsg: 0x%p, 0x%p, 0x%p, 0x%p"), pMsg->message, pMsg->wParam, pMsg->lParam, pMsg->hwnd);
			TRACEF (str);
		}
		#endif

		switch (pMsg->message) {
		case TM_CLOSE:
			m_bRun = false;
			DoDisconnect ();
			::ExitThread (0);
			bHandled = TRUE;
			break;
		case TM_SET_PRINTER:
			{
				bool bDisconnect = false;

				m_lPrinterID = -1;

				if (PRINTERSTRUCT * p = (PRINTERSTRUCT *)pMsg->wParam) {
					bDisconnect = m_printer.m_strAddress != p->m_strAddress;
					m_printer = * p;
					m_lPrinterID = m_printer.m_lID;
					SetPrinter (m_printer.m_lID, m_printer);
					delete p;
				}
				else 
					bDisconnect = true;

				if (bDisconnect) {
					TRACEF (_T ("disconnect: ") + m_printer.m_strName);
					DoDisconnect ();
				}
			}
			bHandled = TRUE;
			break;
		case TM_SET_HWND:
			m_hPrinterWnd = (HWND)pMsg->wParam;
			DoUpdate ();
			bHandled = TRUE;
			break;
		}

		if (bHandled && ISPOSTMESSAGESTRUCT (pMsg->message)) {
			if (POSTMESSAGESTRUCT * p = (POSTMESSAGESTRUCT *)pMsg->lParam)
				if (p->m_hEvent)
					::SetEvent (p->m_hEvent);
		}
	}

	if (ISPOSTMESSAGESTRUCT (pMsg->message))
		if (POSTMESSAGESTRUCT * p = (POSTMESSAGESTRUCT *)pMsg->lParam)
			delete p;

	return CWinThread::PreTranslateMessage(pMsg);
}

bool CPrinterThread::SendCmd (CStringArray & vCmd, CStringArray & vResponse)
{
	if (m_socket != INVALID_SOCKET) {
		if (SendCmd (m_socket, vCmd, vResponse)) 
			return true;
	}

	DoDisconnect ();
	return false;
}

bool CPrinterThread::SendCmd (SOCKET & socket, CStringArray & vCmd, CStringArray & vResponse)
{
	if (socket != INVALID_SOCKET) {
		CString strCmd = ToString (vCmd);
		int nLen = strCmd.GetLength ();

		vResponse.RemoveAll ();

		ASSERT (strCmd.Find (BW_HOST_PACKET_CLOSESOCKET) == -1);

		bool bSend = ::send (socket, (const char *)(LPCSTR)w2a (strCmd), nLen, 0) != SOCKET_ERROR;

		if (bSend) {
			bool bMore = true;
			CString strBuffer;
			int nTries = 0;
			CTime tmStart = CTime::GetCurrentTime ();
			bool bTimeout = false;

			do {
				CTimeSpan tm = CTime::GetCurrentTime () - tmStart;
				bTimeout = tm.GetTotalSeconds () > 5;
			}
			while (!bTimeout && !IsSocketDataPending (socket));


			if (!bTimeout) {
				do {
					char szBuffer [1024];
					ZeroMemory (szBuffer, sizeof (szBuffer));

					int nRevc = ::recv (socket, szBuffer, sizeof (szBuffer), 0);

					if (nRevc == SOCKET_ERROR) {
						TRACEF (_T ("Receive failed: \n") + FormatMessage (::GetLastError ()));
						return false;
					}
					
					strBuffer += a2w (szBuffer);

					int nLeft = CountChars (strBuffer, '{').GetSize ();
					int nRight = CountChars (strBuffer, '}').GetSize ();

					if (nLeft && nRight == nLeft)
						bMore = false;
					else {
						if (nTries++ > 20) {
							TRACEF (_T ("No TCP/IP response from printer"));
							ASSERT (0);
							return false;
						}
					}
				} 
				while (bMore);

				FromString (strBuffer, vResponse);

				#ifdef _DEBUG
				ASSERT (vCmd.GetSize ());
				ASSERT (vResponse.GetSize ());
				CString strCmd = vCmd [0];
				CString strResponse = vResponse [0];
				ASSERT (!strCmd.CompareNoCase (strResponse)); // cmd/response mismatched
				#endif //_DEBUG

				return true;
			}
		}
	}

	return false;
}

int CPrinterThread::Run () 
{
	clock_t clock_tLast = clock (), clock_tNow = clock ();
	const double dSampleRate = 5.0;

	VERIFY (::WaitForSingleObject (m_hInit, INFINITE) == WAIT_OBJECT_0);

	while (m_bRun) {
		{
			MSG msg;
		
			while (PeekMessage (&msg, NULL, NULL, NULL, PM_NOREMOVE))
				if (!PumpMessage ())
					return ExitInstance ();
		}

		clock_tNow = clock ();
		double dDiff = (double)(clock_tNow - clock_tLast) / CLOCKS_PER_SEC;

		if ((int)(dDiff * 1000.0) < 0) 
			clock_tLast = clock ();

		if (dDiff >= dSampleRate) {
			if (m_socket == INVALID_SOCKET)
				DoConnect ();

			DoUpdate ();

 			clock_tLast = clock ();
		}

		::Sleep (1);
	}

	TRACEF (_T ("CPrinterThread::Run exiting: ") + m_printer.m_strAddress);

	return 0;
}

void CPrinterThread::DoConnect()
{
	if (m_printer.m_strAddress.CompareNoCase (_T ("0.0.0.0")) != 0) {
		//TRACEF (m_printer.m_strAddress + _T (": ") + m_printer.m_strName + _T (": DoConnect: ") + CTime::GetCurrentTime ().Format (_T ("%c")));

		if (m_socket == INVALID_SOCKET) {
			SOCKADDR_IN target;
			/*
			TIMEVAL timeout;
			unsigned long iMode;
			int iResult;
			
			timeout.tv_sec = 5;
			timeout.tv_usec = 0;
			*/

			target.sin_family = AF_INET; 
			target.sin_port = htons (BW_HOST_TCP_PORT); 
			target.sin_addr.s_addr = inet_addr (w2a (m_printer.m_strAddress));

			TRACEF (m_printer.m_strAddress + _T (": ") + m_printer.m_strName + _T (": Attempting to connect..."));
			m_socket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP); 

			/*
			iMode = 1;
			iResult = ioctlsocket (m_socket, FIONBIO, &iMode); // set to non-blocking

			if (iResult != NO_ERROR)
				TRACEF (_T ("ioctlsocket failed with error: ") + ToString (iResult));
			*/

			if (connect (m_socket, (SOCKADDR *)&target, sizeof(target)) == SOCKET_ERROR) {
				TRACEF (m_printer.m_strAddress + _T (": ") + m_printer.m_strName + _T (": Failed to connect"));
				m_socket = INVALID_SOCKET;
			}
			else {
				/*
				fd_set write, err;

				TRACEF (m_printer.m_strAddress + _T (": ") + m_printer.m_strName + _T (": Connected"));

				iMode = 0;
				iResult = ioctlsocket (m_socket, FIONBIO, &iMode);

				if (iResult != NO_ERROR)
					TRACEF (_T ("ioctlsocket failed with error: ") + ToString (iResult));

				FD_ZERO (&write);
				FD_ZERO (&err);
				FD_SET (m_socket, &write);
				FD_SET (m_socket, &err);

				// check if the socket is ready
				select (0, NULL, &write, &err, &timeout);			

				if (!FD_ISSET (m_socket, &write)) {
					TRACEF (m_printer.m_strAddress + _T (": ") + m_printer.m_strName + _T (": FD_ISSET failed"));
					closesocket (m_socket);
					m_socket = INVALID_SOCKET;
				}
				*/
			}
		}
	}
}

void CPrinterThread::DoUpdate()
{
	const CString strConnected		= _T ("{") + CString (PRINTERCTRL_CONNECTED)	+ _T ("}");
	const CString strDisconnected	= _T ("{") + CString (PRINTERCTRL_DISCONNECTED) + _T ("}");

	if (m_printer.m_strAddress.CompareNoCase (_T ("0.0.0.0")) != 0) {
		//TRACEF (m_printer.m_strAddress + _T (": ") + m_printer.m_strName + _T (": DoUpdate: ") + CTime::GetCurrentTime ().Format (_T ("%c")));

		CString strState = strDisconnected;

		strState += (m_printer.m_bMaster) ? _T ("{Master}") : (m_printer.m_type == MATRIX ? _T ("{Matrix}") : _T ("{Elite}"));

		if (m_socket != INVALID_SOCKET) {
			CStringArray vCmd, vResponse, vLines;
			
			strState = strConnected;
			strState += (m_printer.m_bMaster) ? _T ("{Master}") : (m_printer.m_type == MATRIX ? _T ("{Matrix}") : _T ("{Elite}"));

			vCmd.Add (BW_HOST_PACKET_GET_LINES);
			if (!SendCmd (vCmd, vResponse)) {
				strState.Replace (strConnected, strDisconnected);
			}
			strState += ToString (vResponse);
			vCmd.RemoveAll ();

			for (int i = 1; i < vResponse.GetSize (); i++)
				vLines.Add (vResponse [i]);

			for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
				vCmd.Add (BW_HOST_PACKET_GET_CURRENT_TASK);
				vCmd.Add (vLines [nLine]);
				if (!SendCmd (vCmd, vResponse)) {
					strState.Replace (strConnected, strDisconnected);
					break;
				}
				//TRACEF (ToString (vResponse));
				strState += ToString (vResponse);
				vCmd.RemoveAll ();

				vCmd.Add (BW_HOST_PACKET_GET_CURRENT_STATE);
				vCmd.Add (vLines [nLine]);
				if (!SendCmd (vCmd, vResponse)) {
					strState.Replace (strConnected, strDisconnected);
					break;
				}
				//TRACEF (ToString (vResponse));
				strState += ToString (vResponse);
				vCmd.RemoveAll ();

				vCmd.Add (BW_HOST_PACKET_GET_COUNT);
				vCmd.Add (vLines [nLine]);
				if (!SendCmd (vCmd, vResponse)) {
					strState.Replace (strConnected, strDisconnected);
					break;
				}
				//TRACEF (ToString (vResponse));
				strState += ToString (vResponse);
				vCmd.RemoveAll ();

				vCmd.Add (BW_HOST_PACKET_GET_STROBE);
				if (!SendCmd (vCmd, vResponse)) {
					strState.Replace (strConnected, strDisconnected);
					break;
				}
				//TRACEF (ToString (vResponse));
				/*
				#ifdef _DEBUG
				{
					CStringArray v, vStrobe;
					//vStrobe.Add (_T ("GREEN_STROBE"));
					//vStrobe.Add (_T ("RED_STROBE_FLASH"));
					v.Add (BW_HOST_PACKET_GET_STROBE);
					//v.Add (ToString (vStrobe));
					v.Add (_T ("Unknown command"));
					strState += ToString (v); TRACEF (_T (" ***** TODO ***** ") + ToString (v)); 
				}
				#else
				*/
				strState += ToString (vResponse);
				//#endif 
				vCmd.RemoveAll ();
			}
		}
 
		//TRACEF (m_printer.m_strAddress + _T (": ") + m_printer.m_strName + _T (": ") + strState);

		if (m_hPrinterWnd) 
			while (!::PostMessage (m_hPrinterWnd, WM_PRINTERSTATECHANGE, (WPARAM)m_printer.m_lID, (LPARAM)(LPVOID)new CString (strState)))
				::Sleep (0);
	}
}


void CPrinterThread::DoDisconnect()
{
	if (m_socket != INVALID_SOCKET)
		closesocket (m_socket);

	m_socket = INVALID_SOCKET;

	if (m_hPrinterWnd) {
		CString strState = CString (_T ("{") + CString (PRINTERCTRL_DISCONNECTED) + _T ("}")) + (m_printer.m_type == MATRIX ? _T ("{Matrix}") : _T ("{Elite}"));

		while (!::PostMessage (m_hPrinterWnd, WM_PRINTERSTATECHANGE, (WPARAM)m_printer.m_lID, (LPARAM)(LPVOID)new CString (strState)))
			::Sleep (0);
	}
}
