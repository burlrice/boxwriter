#if !defined(AFX_LOCKDLG_H__CABC9B6F_DD42_4705_9F16_042046596B2F__INCLUDED_)
#define AFX_LOCKDLG_H__CABC9B6F_DD42_4705_9F16_042046596B2F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LockDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLockDlg dialog

#include "ListCtrlImp.h"
#include "Lock.h"

class CLockDlg : public CDialog
{
// Construction
public:
	CLockDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLockDlg)
	enum { IDD = IDD_LOCK };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLockDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void FillListCtrl();
	class CLockItem : public ItiLibrary::CListCtrlImp::CItem, public FoxjetDatabase::LOCKRECORDSTRUCT
	{
	public:
		virtual CString GetDispText (int nColumn) const;
	};

	ItiLibrary::CListCtrlImp m_lv;

	// Generated message map functions
	//{{AFX_MSG(CLockDlg)
	afx_msg void OnDelete();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CString m_strRecords;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOCKDLG_H__CABC9B6F_DD42_4705_9F16_042046596B2F__INCLUDED_)
