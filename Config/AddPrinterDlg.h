#if !defined(AFX_ADDPRINTERDLG_H__9A23C0E5_358C_48B7_8FEA_7E51FEA7487D__INCLUDED_)
#define AFX_ADDPRINTERDLG_H__9A23C0E5_358C_48B7_8FEA_7E51FEA7487D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddPrinterDlg.h : header file
//

#include "RoundButtons.h"
#include "Database.h"
#include "Resource.h"
#include "ListCtrlImp.h"

/////////////////////////////////////////////////////////////////////////////
// CAddPrinterDlg dialog

class CPrinterItem : public ItiLibrary::ListCtrlImp::CItem
{
public:
	CPrinterItem (FoxjetDatabase::PRINTERSTRUCT & printer);

	virtual CString GetDispText (int nColumn) const;

	FoxjetDatabase::PRINTERSTRUCT m_printer;
};

class CAddPrinterDlg : public CDialog
{
// Construction
public:
	CAddPrinterDlg(CWnd* pParent = NULL);   // standard constructor


// Dialog Data
	//{{AFX_DATA(CAddPrinterDlg)
	CString	m_strName;
	CString	m_strAddr;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddPrinterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	FoxjetUtils::CRoundButtons m_buttons;
	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

	virtual void OnOK();

	bool CopySettings ();
	bool DoRegistryImport (const CString & strAddr);

	// Generated message map functions
	//{{AFX_MSG(CAddPrinterDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDPRINTERDLG_H__9A23C0E5_358C_48B7_8FEA_7E51FEA7487D__INCLUDED_)
