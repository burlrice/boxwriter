// ConfigDlg.cpp : implementation file
//

#include "stdafx.h"
#include <afxdao.h>   
#include "Config.h"
#include "ConfigDlg.h"
#include "AboutDlg.h"
#include "Resource.h"
#include "FileExt.h"
#include "Registry.h"
#include "HeadDlg.h"
#include "PrinterDlg.h"
#include "LineDlg.h"
#include "ListCtrlImp.h"
#include "WinMsg.h"
#include "OdbcRecordset.h"
#include "Color.h"
#include "FieldDefs.h"
#include "CardTypeDlg.h"
#include "ximage.h"
#include "ListCtrlImp.h"
#include "PrinterTypeDlg.h"
#include "..\..\Control\Host.h"
#include "StartTaskDlg.h"

#define ID_STROBE 1000

using namespace ItiLibrary;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static int Find (CArray <HEADSTRUCT, HEADSTRUCT &> & vHeads, ULONG lHeadID)
{
	for (int i = 0; i < vHeads.GetSize (); i++) {
		HEADSTRUCT & head = vHeads [i];

		if (head.m_lID == lHeadID) 
			return i;
	}

	return -1;
}

static void DeleteLine (COdbcDatabase & db, ULONG lLineID)
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

	GetLineHeads (db, lLineID, vHeads);

	DeleteLineRecord (db, lLineID);
	
	for (int i = 0; i < vHeads.GetSize (); i++)
		DeleteHeadRecord (db, vHeads [i].m_lID);
}

ULONG CConfigDlg::GetPanelID (CArray <PANELSTRUCT, PANELSTRUCT &> & v, UINT nNumber)
{
	for (int i = 0; i < v.GetSize (); i++) 
		if (v [i].m_nNumber == nNumber)
			return v [i].m_lID;

	return -1;
}

/*
class CLock 
{
public:
	CLock (DWORD dwThreadID, ULONG lRecordID, FoxjetDatabase::LockType type = FoxjetDatabase::LOCK_CONFIG)
	:	m_dwThreadID (dwThreadID),
		m_lRecordID (lRecordID),
		m_type (type),
		m_bLocked (false)
	{
	}

	~CLock ()
	{
		if (m_bLocked)
			VERIFY (Unlock (0, m_dwThreadID, m_lRecordID, m_type));
	}

	bool Lock (CWnd * pWnd)
	{
		LOCKRECORDSTRUCT lock;

		if (FoxjetDatabase::Lock (0, m_dwThreadID, m_lRecordID, m_type, _T ("root"), lock)) 
			m_bLocked = true;
		else {
			CString str, strDiff;
			COleDateTimeSpan tm = COleDateTime::GetCurrentTime () - lock.m_tmModified;

			strDiff.Format (_T ("%s %s"), 
				lock.m_tmModified.Format (_T ("%c")),
				tm.Format (_T ("[%D days, %H:%M:%S ago]")));
			str.Format (LoadString (IDS_LOCKFAILED), 
				lock.m_strIpAddr, 
				lock.m_strName, 
				lock.m_strUser, 
				strDiff);

			if (pWnd)
				MsgBox (* pWnd, str, _T (""), MB_ICONINFORMATION);
			else
				MsgBox (str, MB_ICONINFORMATION);
		}

		return m_bLocked;
	}

protected:
	DWORD m_dwThreadID;
	ULONG m_lRecordID;
	FoxjetDatabase::LockType m_type;
	bool m_bLocked;
};
*/

////////////////////////////////////////////////////////////////////////////////
class CCompactDlg : public CDialog
{
// Construction
public:
	CCompactDlg(CWnd* pParent = NULL)	
	:	CDialog (IDD_COMPACT, pParent)
	{
	}

protected:
	virtual void DoDataExchange(CDataExchange* pDX)
	{
		CDialog::DoDataExchange(pDX);
	}

protected:
	DECLARE_MESSAGE_MAP()
};

BEGIN_MESSAGE_MAP(CCompactDlg, CDialog)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////


CTreeItem::CTreeItem (const FoxjetDatabase::PRINTERSTRUCT & printer, ULONG lPrinterID)
:	m_type (PRINTER),
	m_lPrinterID (lPrinterID)
{
	m_pPrinter = new PRINTERSTRUCT (printer);
}

CTreeItem::CTreeItem (const FoxjetDatabase::LINESTRUCT & line, ULONG lPrinterID)
:	m_type (LINE),
	m_lPrinterID (lPrinterID)
{
	m_pLine = new LINESTRUCT (line);
}

CTreeItem::CTreeItem (const FoxjetDatabase::HEADSTRUCT & head, ULONG lPrinterID)
:	m_type (HEAD),
	m_lPrinterID (lPrinterID)
{
	m_pHead = new HEADSTRUCT (head);
}

CTreeItem::~CTreeItem ()
{
	switch (m_type) {
	case PRINTER:	if (m_pPrinter) delete m_pPrinter;		m_pPrinter	= NULL;		break;
	case LINE:		if (m_pLine)	delete m_pLine;			m_pLine		= NULL;		break;
	case HEAD:		if (m_pHead)	delete m_pHead;			m_pHead		= NULL;		break;
	}
}

UINT CTreeItem::GetMenuID () const
{
	switch (m_type) {
	case PRINTER:	return IDM_PRINTER;
	case LINE:		return IDM_LINE;
	case HEAD:		return IDM_HEAD;
	}

	return -1;
}

UINT CTreeItem::GetDefMenuID () const
{
	switch (m_type) {
	case PRINTER:	return ID_PRINTER_EDIT;
	case LINE:		return ID_LINE_EDIT;
	case HEAD:		return ID_HEAD_EDIT;
	}

	return -1;
}


/////////////////////////////////////////////////////////////////////////////
// CConfigDlg dialog

CConfigDlg::CConfigDlg(CWnd* pParent /*=NULL*/)
:	/*
	m_hPrinter (NULL),
	m_hPrinterMaster (NULL),
	m_hLine (NULL),
	m_hHead (NULL),
	m_hTask (NULL),
	m_hHeadMaster (NULL),
	m_hHeadDisabled (NULL),
	*/
	m_hAccel (NULL),
	m_bPrinterAddLine (false),
	m_bPrinterAddHead (false),
	m_lPrinterAddHead (-1),
	m_lLineID (FoxjetDatabase::NOTFOUND),
	m_bFillingTreeCtrl (false),
	m_nSelected (-1),
	//m_hLockThread (NULL),
	//m_dwLockThreadID (NULL),
	m_nTimerID (-1),
	CDialog(CConfigDlg::IDD, pParent)
{
	HINSTANCE hInst = theApp.m_hInstance;

	m_sel.m_lID = -1;
	m_sel.m_lType = CTreeItem::PRINTER;

	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CConfigDlg::~CConfigDlg ()
{
	/*
	if (m_hPrinter)			::DestroyIcon (m_hPrinter);
	if (m_hPrinterMaster)	::DestroyIcon (m_hPrinterMaster);
	if (m_hLine)			::DestroyIcon (m_hLine);
	if (m_hTask)			::DestroyIcon (m_hTask);
	if (m_hHead)			::DestroyIcon (m_hHead);
	if (m_hHeadMaster)		::DestroyIcon (m_hHeadMaster);
	if (m_hHeadDisabled)	::DestroyIcon (m_hHeadDisabled);
	*/
}

void CConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConfigDlg)
	DDX_Control(pDX, LV_PRINTERS, m_lv);
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, BTN_ADD);
	m_buttons.DoDataExchange (pDX, BTN_EDIT);
	m_buttons.DoDataExchange (pDX, BTN_DELETE);
	m_buttons.DoDataExchange (pDX, BTN_CLOSE);
}

BEGIN_MESSAGE_MAP(CConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CConfigDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_COMMAND(ID_HELP_ABOUT, OnHelpAbout)
	ON_COMMAND(ID_APP_EXIT, OnAppExit)
	ON_COMMAND(ID_FILE_COMPACTDATABASE, OnFileCompactdatabase)
	ON_NOTIFY(NM_DBLCLK, LV_PRINTERS, OnDblclkPrinters)
	ON_COMMAND(ID_PRINTER_EDIT, OnPrinterEdit)
	ON_COMMAND(ID_PRINTER_ADD, OnPrinterAdd)
	ON_COMMAND(ID_PRINTER_DELETE, OnPrinterDelete)
	ON_COMMAND(ID_HEAD_EDIT, OnHeadEdit)
	ON_COMMAND(ID_HEAD_DELETE, OnHeadDelete)
	ON_COMMAND(ID_LINE_DELETE, OnLineDelete)
	ON_COMMAND(ID_LINE_EDIT, OnLineEdit)
	ON_BN_CLICKED(BTN_DELETE, OnDelete)
	ON_BN_CLICKED(BTN_ADD, OnAdd)
	ON_COMMAND(ID_HEAD_ADD, OnHeadAdd)
	ON_COMMAND(ID_LINE_ADD, OnLineAdd)
	ON_BN_CLICKED(BTN_EDITTASK, OnEditTask)
	ON_BN_CLICKED(BTN_ADDTASK, OnAddTask)
	ON_BN_CLICKED(BTN_DELETETASK, OnDeleteTask)
	ON_NOTIFY(NM_DBLCLK, LV_TASKS, OnItemdblclickTasks)
	ON_WM_INITMENU()
	ON_BN_CLICKED(BTN_CLOSE, OnClose)
	ON_WM_DRAWITEM()
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_FILE_VIEWLOCKS, OnFileViewlocks)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(BTN_EDIT, OnEdit)
	ON_COMMAND(ID_DO_NOTHING, OnDoNothing)

	ON_COMMAND_RANGE(ID_LINE001_START, ID_LINE002_START, OnStart)
	ON_COMMAND_RANGE(ID_LINE001_STOP, ID_LINE002_STOP, OnStop)
	ON_COMMAND_RANGE(ID_LINE001_IDLE, ID_LINE002_IDLE, OnIdle)
	ON_COMMAND_RANGE(ID_LINE001_RESUME, ID_LINE002_RESUME, OnResume)

	ON_MESSAGE (WM_EDITOR_TASK_CHANGE, OnTasksChanged)
	ON_REGISTERED_MESSAGE (WM_EDITOR_TASK_CHANGE, OnTasksChanged)
	ON_MESSAGE (WM_PRINTERSTATECHANGE, OnPrinterStateChange)
	ON_REGISTERED_MESSAGE (WM_SYSTEM_COLOR_CHANGE, OnColorsChanged)
	ON_REGISTERED_MESSAGE (WM_ISCONFIGRUNNING, OnIsAppRunning)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConfigDlg message handlers

BOOL CConfigDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	/*
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);
	tree.SetImageList (&m_ilTreeView, TVSIL_NORMAL);
	FillTreeCtrl ();

	CListCtrl & lv = * (CListCtrl *)GetDlgItem (LV_TASKS);

	lv.SetExtendedStyle (lv.GetExtendedStyle () | LVS_EX_FULLROWSELECT);
	lv.InsertColumn (0, LoadString (IDS_NAME), LVCFMT_LEFT, 80);
	lv.InsertColumn (1, LoadString (IDS_DESCRIPTION), LVCFMT_LEFT, 120);
	lv.InsertColumn (2, LoadString (IDS_ID), LVCFMT_LEFT, 0);

	ItiLibrary::LoadSettings (lv, _T ("Software\\FoxJet\\Config"), _T ("CConfigDlg"));
	lv.SetColumnWidth (2, 0);
	*/

	m_lv.SetImageList (&m_lv.m_imglist, TVSIL_NORMAL);
	m_lv.InsertColumn (0, _T (""));
	m_lv.Init ();
	
	if (CFont * p = GetFont ()) {
		LOGFONT lf;

		p->GetLogFont (&lf);
		lf.lfHeight = (int)(lf.lfHeight * 1.25);
		lf.lfWeight = FW_BOLD;
		VERIFY (m_lv.m_fntStatus.CreateFontIndirect (&lf));
	}

	FillListCtrl ();

	{
		UINT n [] = 
		{
			LV_TASKS,
			BTN_ADDTASK,
			BTN_EDITTASK,
			BTN_DELETETASK,
		};

		for (int i = 0; i < ARRAYSIZE (n); i++)
			if (CWnd * p = GetDlgItem (n [i]))
				p->ShowWindow (FALSE);
	}

	/*
	{
		VERIFY (m_hLockThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)FoxjetDatabase::LockFunc, (LPVOID)&theApp.m_db, 0, &m_dwLockThreadID));

		while (!::PostThreadMessage (m_dwLockThreadID, TM_LOCK_INIT, (WPARAM)GetPrinterID (theApp.m_db), (LPARAM)LOCK_MESSAGE))
			::Sleep (0);
	}
	*/

	m_nTimerID = SetTimer (ID_STROBE, 1000, NULL);


	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CConfigDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		OnAppAbout ();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CConfigDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CConfigDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CConfigDlg::OnAppAbout ()
{
	CString strTitle = 
		LoadString (IDS_ABOUT) + _T (" ") + 
		GetAppTitle () + _T (" ") + 
		LoadString (AFX_IDS_APP_TITLE);
	CStringArray vCmdLine;

	TokenizeCmdLine (::GetCommandLine (), vCmdLine);

	FoxjetCommon::CAboutDlg dlg (theApp.GetVersion (), vCmdLine, strTitle);

	dlg.m_strApp = GetAppTitle ();
		
	dlg.DoModal ();
}

void CConfigDlg::DeleteTreeItem (CTreeCtrl & tree, HTREEITEM hItem)
{
	while (hItem) {
		const HTREEITEM hCurrent = hItem;
		CTreeItem * p = (CTreeItem *)tree.GetItemData (hItem);

		if (HTREEITEM hChild = tree.GetChildItem (hItem)) {
			while (hChild != NULL) {
				DeleteTreeItem (tree, hChild);
				hChild = tree.GetNextSiblingItem (hChild);
			}
		}

		hItem = tree.GetNextItem (hItem, TVGN_NEXT);
		tree.DeleteItem (hCurrent);

		if (p) {
			if (p->m_type == CTreeItem::PRINTER)	TRACEF (_T ("delete: \t")		+ p->m_pPrinter->m_strName);
			if (p->m_type == CTreeItem::LINE)		TRACEF (_T ("delete: \t\t")		+ p->m_pLine->m_strName);
			if (p->m_type == CTreeItem::HEAD)		TRACEF (_T ("delete: \t\t\t")	+ p->m_pHead->m_strName);
				
			delete p;
		}
	}
}

/*
void CConfigDlg::FillTreeCtrl ()
{
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);
	CArray <PRINTERSTRUCT, PRINTERSTRUCT &> v;
	int nVertPos = tree.GetScrollPos (SB_VERT);
	int nHorzPos = tree.GetScrollPos (SB_HORZ);
	CArray <HEADSTRUCT, HEADSTRUCT &> vMasterHeads;
	bool bConfigError = false;

	GetPrinterHeads (theApp.m_db, GetMasterPrinterID (theApp.m_db), vMasterHeads);

	BeginWaitCursor ();

	m_bFillingTreeCtrl = true;
	tree.SetRedraw (FALSE);
	DeleteTreeItem (tree, tree.GetRootItem ());
	GetPrinterRecords (theApp.m_db, v);

	for (int nPrinter = 0; nPrinter < v.GetSize (); nPrinter++) {
		CArray <LINESTRUCT, LINESTRUCT &> vLines;
		PRINTERSTRUCT p = v [nPrinter];
		int nIndex = IsMasterMode () ? (p.m_bMaster ? m_nPrinterMasterIndex : m_nPrinterIndex) : m_nPrinterIndex;
		CString strName = p.m_strName;

		TRACEF (p.m_strName);

		if (!p.m_bMaster) {
			if (!CompareHeadConfigs (theApp.m_db, vMasterHeads, p.m_lID)) {
				strName += _T (" [") + LoadString (IDS_CONFIGERROR) + _T ("]");
				bConfigError = true;
			}
		}

		HTREEITEM hPrinter = tree.InsertItem (strName, nIndex, nIndex);

		if (m_sel.m_lType == CTreeItem::PRINTER && m_sel.m_lID == p.m_lID)
			tree.SelectItem (hPrinter);

		GetPrinterLines (theApp.m_db, p.m_lID, vLines);
		
		{
			CTreeItem * pItem = new CTreeItem (p, p.m_lID);
			
			for (int i = 0; i < vLines.GetSize (); i++)
				pItem->m_vLineIDs.Add (vLines [i].m_lID);

			tree.SetItemData (hPrinter, (DWORD)pItem);
		}

		for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
			CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
			CArray <TASKSTRUCT, TASKSTRUCT &> vTasks;
			LINESTRUCT line = vLines [nLine];
			HTREEITEM hLine = hPrinter;

			//if (vLines.GetSize () > 1) 
			{
				CString str = line.m_strName;

				if (line.m_strDesc.GetLength ())
					str += _T (" [") + line.m_strDesc + _T ("]");

				hLine = tree.InsertItem (str, m_nLineIndex, m_nLineIndex, hPrinter);
				CTreeItem * pItem = new CTreeItem (line, p.m_lID);

				pItem->m_vLineIDs.Add (line.m_lID);
				tree.SetItemData (hLine, (DWORD)pItem);

				if (m_sel.m_lType == CTreeItem::LINE && m_sel.m_lID == line.m_lID)
					tree.SelectItem (hLine);
			}

			GetLineHeads (theApp.m_db, line.m_lID, vHeads);

			for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
				HEADSTRUCT head = vHeads [nHead];
				int nIndex = m_nHeadIndex;

				if (head.m_bMasterHead) 
					nIndex = m_nHeadMasterIndex;
				else if (!head.m_bEnabled)
					nIndex = m_nHeadDisabledIndex;

				HTREEITEM hHead = tree.InsertItem (head.m_strName, nIndex, nIndex, hLine);

				{
					CTreeItem * pItem = new CTreeItem (head, p.m_lID);

					pItem->m_vLineIDs.Add (line.m_lID);
					tree.SetItemData (hHead, (DWORD)pItem);
				}

				if (m_sel.m_lType == CTreeItem::HEAD && m_sel.m_lID == head.m_lID)
					tree.SelectItem (hHead);
			}

			tree.Expand (hLine, TVE_EXPAND);
		}

		tree.Expand (hPrinter, TVE_EXPAND);
	}

	if (bConfigError) {
		for (HTREEITEM hItem = tree.GetRootItem (); hItem; ) {
			tree.Expand (hItem, TVE_COLLAPSE);
			hItem = tree.GetNextItem (hItem, TVGN_NEXT);
		}
	}

	tree.SetRedraw (TRUE);
	tree.SetScrollPos (SB_VERT, nVertPos);
	tree.SetScrollPos (SB_HORZ, nHorzPos);
	EndWaitCursor ();
	m_bFillingTreeCtrl = false;
}
*/

void CConfigDlg::OnDestroy() 
{
	const CString strSrc = FoxjetDatabase::Extract (theApp.m_db.GetConnect (), _T ("DBQ="), _T (";"));
	const CString strDSN = FoxjetDatabase::Extract (theApp.m_db.GetConnect (), _T ("DSN="), _T (";"));
	const CString strKey = _T ("Software\\Foxjet\\Database\\") + strDSN;
	DWORD dwFileSize = 0;
	const bool bOpen = theApp.m_db.IsOpen () ? true : false;

	if (m_nTimerID != -1)
		KillTimer (m_nTimerID);

	/*
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);
	CListCtrl & lv = * (CListCtrl *)GetDlgItem (LV_TASKS);

	ItiLibrary::SaveSettings (lv, 2, _T ("Software\\FoxJet\\Control"), _T ("CConfigDlg"));
	tree.SetRedraw (FALSE);
	lv.SetRedraw (FALSE);
	m_bFillingTreeCtrl = true;
	DeleteTreeItem (tree, tree.GetRootItem ());
	*/

	/*
	if (m_hLockThread) {
		while (!::PostThreadMessage (m_dwLockThreadID, TM_LOCK_KILL, 0, 0))
			::Sleep (0);

		VERIFY (::WaitForSingleObject (m_hLockThread, 5000) == WAIT_OBJECT_0);
		m_hLockThread = NULL;
	}
	*/

	if (bOpen) {
		VERIFY (FoxjetCommon::ExitInstance (GetElementListVersion ()));

		theApp.m_db.Close ();
	}

	if (strSrc.GetLength () && theApp.m_bCompact) {
		TCHAR szTitle [MAX_PATH] = { 0 };
		CString strDest = strSrc;
		CString strBackup = strSrc;
		CString strExt = _T (".") + FoxjetFile::GetFileExt (strSrc);
		CCompactDlg dlg;
		CString strProgress = LoadString (IDS_COMPACT);

		HRSRC hResource = ::FindResource(theApp.m_hInstance, MAKEINTRESOURCE(IDD_COMPACT), RT_DIALOG);
		HGLOBAL hTemplate = LoadResource(theApp.m_hInstance, hResource);
		VERIFY (dlg.CreateIndirect(hTemplate));
		FreeResource(hTemplate);
		
		dlg.ShowWindow (SW_SHOW);
		dlg.BringWindowToTop ();
		dlg.CenterWindow ();
		dlg.UpdateWindow ();

		dlg.SetDlgItemText (TXT_TEXT, strProgress);  dlg.UpdateWindow ();

		::GetFileTitle (strSrc, szTitle, sizeof (szTitle));
		CString strTitle (szTitle);
		
		if (strTitle.Find (strExt) == -1)
			strTitle += strExt;

		strBackup.Replace (strTitle, _T ("backup_") + strTitle);
		strDest.Replace (strTitle, _T (""));
		strTitle.Replace (strExt, _T (""));
		strExt.Replace (_T ("."), _T (""));
		strDest = CreateTempFilename (strDest, strTitle, strExt);

		#ifdef _DEBUG
		int n = ::afxTraceFlags;
		::afxTraceFlags |= ::traceDatabase;
		#endif //_DEBUG

		try 
		{
			CString strStatus;

			strStatus =  _T ("CopyFile: ") + strSrc + _T (" --> ") + strBackup;
			TRACEF (strStatus);
			strProgress += _T ("\r\n") + strStatus; dlg.SetDlgItemText (TXT_TEXT, strProgress); dlg.UpdateWindow ();
			VERIFY (::CopyFile (strSrc, strBackup, FALSE));
			::DeleteFile (strDest);
			
			strStatus = _T ("CompactDatabase: ") + strSrc + _T (" --> ") + strDest;
			TRACEF (strStatus);
			strProgress += _T ("\r\n") + strStatus; dlg.SetDlgItemText (TXT_TEXT, strProgress); dlg.UpdateWindow ();
			CDaoWorkspace::CompactDatabase (strSrc, strDest);
						
			strStatus = _T ("CopyFile: ") + strDest + " --> " + strSrc;
			TRACEF (strStatus);								
			strProgress += _T ("\r\n") + strStatus; dlg.SetDlgItemText (TXT_TEXT, strProgress); dlg.UpdateWindow ();
			VERIFY (::CopyFile (strDest, strSrc, FALSE));

			strStatus = _T ("DeleteFile: ") + strDest;
			TRACEF (strStatus);													
			strProgress += _T ("\r\n") + strStatus; dlg.SetDlgItemText (TXT_TEXT, strProgress); dlg.UpdateWindow ();
			VERIFY (::DeleteFile (strDest));

			{
				HANDLE hFile = ::CreateFile (strSrc, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);

				if (hFile && hFile != INVALID_HANDLE_VALUE) {
					DWORD dwHighSize = 0;
					CString str;
					
					dwFileSize = GetFileSize (hFile, &dwHighSize);
					str.Format (_T ("%p"), dwFileSize);
					FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("size"), str);
					::CloseHandle (hFile);
				}
			}

			CString str;

			str.Format (LoadString (IDS_COMPACTSUCCEEDED), strBackup);
			TRACEF (str);
			MsgBox (* this, str, NULL, MB_OK | MB_SYSTEMMODAL | MB_ICONINFORMATION);
		}
		catch (CDaoException * e)		{ HANDLEEXCEPTION (e); }
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		#ifdef _DEBUG
		::afxTraceFlags = n;
		#endif //_DEBUG
	}

	CDialog::OnDestroy();
}

void CConfigDlg::OnHelpAbout() 
{
	OnAppAbout ();
}

BOOL CConfigDlg::PreTranslateMessage(MSG* pMsg) 
{
	if (!m_hAccel)
		VERIFY (m_hAccel = ::LoadAccelerators (::AfxGetInstanceHandle (), MAKEINTRESOURCE (IDA_MAIN)));

	if (::TranslateAccelerator (m_hWnd, m_hAccel, pMsg))
		return TRUE;

	return FALSE; //return CDialog::PreTranslateMessage(pMsg);
}

void CConfigDlg::OnOK()
{

}

void CConfigDlg::OnCancel()
{
	CDialog::OnOK ();	
}

void CConfigDlg::OnAppExit() 
{
	CDialog::OnOK ();	
}

void CConfigDlg::OnDoNothing ()
{
}

void CConfigDlg::OnFileViewlocks() 
{
/*
	CLockDlg dlg (this);

	BEGIN_TRANS (theApp.m_db);

	if (dlg.DoModal () == IDOK) 
		COMMIT_TRANS (theApp.m_db);
	else
		ROLLBACK_TRANS (theApp.m_db);
*/
}

void CConfigDlg::OnFileCompactdatabase() 
{
	CString str;

	str.Format (LoadString (IDS_COMPACTATSHUTDOWN));

	theApp.m_bCompact = MsgBox (str, MB_YESNO | MB_ICONINFORMATION) == IDYES;
}

void CConfigDlg::OnDblclkPrinters(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();

	if (pResult)
		* pResult = 0;
}

bool CConfigDlg::Edit (LINESTRUCT & line, ULONG lPrinterID)
{
	/*
	CLock lock (m_dwLockThreadID, lPrinterID);

	if (!lock.Lock (this))
		return false;
	*/

	const bool bTransactionPending = theApp.m_db.IsTransactionPending ();
	CArray <LINESTRUCT, LINESTRUCT &> vLines;

	if (!bTransactionPending)
		BEGIN_TRANS (theApp.m_db);


	GetPrinterLines (theApp.m_db, line.m_lPrinterID, vLines);
	bool bMore = true;

	for (int i = 0; i < vLines.GetSize (); i++)
		TRACEF (vLines [i].m_strName);

	while (bMore) {
		SETTINGSSTRUCT s;
		CLineDlg dlg (theApp.m_db, line, this);

		{
			s.m_strData = _T ("COM1");
			GetSettingsRecord (theApp.m_db, line.m_lID, FoxjetDatabase::Globals::m_lpszSerialDownload, s);
			dlg.m_strSerialDownload = s.m_strData;
		}

		{
			s.m_strData = _T ("1");
			GetSettingsRecord (theApp.m_db, line.m_lID, FoxjetDatabase::Globals::m_lpszResetCounts, s);
			dlg.m_bResetCountsOnRestart = _ttoi (s.m_strData);
		}

		{
			s.m_strData = _T ("0");
			GetSettingsRecord (theApp.m_db, line.m_lID, FoxjetDatabase::Globals::m_lpszRequireLogin, s);
			dlg.m_bRequireLogin = _ttoi (s.m_strData);
		}

		if (dlg.DoModal () == IDOK) {

			line = dlg.m_line;

			bool bUnique = true;

			for (int i = 0; i < vLines.GetSize (); i++) {
				LINESTRUCT l = vLines [i];

				if (l.m_lID != line.m_lID && !l.m_strName.CompareNoCase (line.m_strName))
					bUnique = false;
			}

			if (bUnique) {
				VERIFY (UpdateLineRecord (theApp.m_db, line)); 
				LineConfigDlg::CLineConfigDlg::UpdateSettingsTable (theApp.m_db, line, dlg);

				if (!Foxjet3d::LineConfigDlg::CLineConfigDlg::LineNamesMatch (theApp.m_db)) {
					CString str = _T ("Line names must match the master printer's configuration.");
					MsgBox (* this, str, _T ("Warning"), MB_OK | MB_ICONWARNING);
				}

				if (!bTransactionPending)
					COMMIT_TRANS (theApp.m_db);
				return true;
			}
			else {
				CString str;

				str.Format (LoadString (IDS_LINENAMENOTEUNIQUE), line.m_strName);
				MsgBox (* this, str, NULL, MB_ICONINFORMATION);
			}
		}
		else
			bMore = false;
	}

	if (!bTransactionPending)
		ROLLBACK_TRANS (theApp.m_db);

	return false;
}

bool CConfigDlg::Edit (HEADSTRUCT & head, ULONG lPrinterID)
{
	/*
	CLock lock (m_dwLockThreadID, lPrinterID);

	if (!lock.Lock (this))
		return false;
	*/

	CMapStringToString v;

	if (head.IsUSB ()) {
		v [_T ("001")] = _T ("USB_1a");
		v [_T ("002")] = _T ("USB_1b");
		v [_T ("003")] = _T ("USB_2a");
		v [_T ("004")] = _T ("USB_2b");
	}
	else {
		v [_T ("300")] = _T ("PHC_1");
		v [_T ("310")] = _T ("PHC_2");
		v [_T ("320")] = _T ("PHC_3");
		v [_T ("330")] = _T ("PHC_4");
		v [_T ("340")] = _T ("PHC_5");
		v [_T ("350")] = _T ("PHC_6");
	}

	CHeadDlg dlgUSB (theApp.m_db, head, INCHES, v, false, this);
	CHeadPhcDlg dlgPHC (theApp.m_db, head, INCHES, v, false, this);
	const bool bTransactionPending = theApp.m_db.IsTransactionPending ();

	dlgPHC.m_lLineID = dlgUSB.m_lLineID = m_lLineID;

	if (!bTransactionPending)
		BEGIN_TRANS (theApp.m_db);

	int nResult = !head.IsUSB () ? dlgPHC.DoModal () : dlgUSB.DoModal ();

	if (nResult == IDOK) {
		HEADSTRUCT h = !head.IsUSB () ? dlgPHC.GetHead () : dlgUSB.GetHead ();

		UpdateNetworkedHeads (theApp.m_db, h);

		if (!bTransactionPending)
			COMMIT_TRANS (theApp.m_db);

		return true;
	}
	else {
		if (!bTransactionPending)
			ROLLBACK_TRANS (theApp.m_db);
	}

	return false;
}

void CConfigDlg::OnEdit() 
{
	DECLARETRACECOUNT (100);

	MARKTRACECOUNT ();
	CListCtrl & lv = m_lv;//* (CListCtrl *)GetDlgItem (LV_PRINTERS);
	CLongArray sel = GetSelectedItems (lv);

	if (!sel.GetSize ()) {
		MsgBox (LoadString (IDS_NOTHINGSELECTED));
		return;
	}

	ULONG lPrinterID = lv.GetItemData (sel [0]);
	PRINTERSTRUCT printer;

	MARKTRACECOUNT ();
	GetPrinterRecord (theApp.m_db, lPrinterID, printer);

	MARKTRACECOUNT ();

	if (Edit (printer)) {
		m_nSelected = sel [0];
		MARKTRACECOUNT ();
		FillListCtrl ();
		MARKTRACECOUNT ();
	}

	MARKTRACECOUNT ();
	TRACECOUNTARRAYMAX ();
}

void CConfigDlg::OnDelete() 
{
	CListCtrl & lv = m_lv;//* (CListCtrl *)GetDlgItem (LV_PRINTERS);
	CLongArray sel = GetSelectedItems (lv);

	if (!sel.GetSize ()) {
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
		return;
	}

	ULONG lPrinterID = lv.GetItemData (sel [0]);
	PRINTERSTRUCT printer;
	CString str;

	GetPrinterRecord (theApp.m_db, lPrinterID, printer);

	if (printer.m_bMaster) 
		return;

	str.Format (LoadString (IDS_CONFIRMPRINTERDELETE), printer.m_strName);

	if (MsgBox (* this, str, NULL, MB_ICONEXCLAMATION | MB_YESNO) == IDYES) { 
		if (DeletePrinterRecord (theApp.m_db, lPrinterID)) {
			m_nSelected = -1;
			FillListCtrl ();
		}
	}

/*
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);
	bool bCommit = false;

	if (CTreeItem * p = (CTreeItem *)tree.GetItemData (tree.GetSelectedItem ())) {
		CString str;

		switch (p->m_type) {
		case CTreeItem::PRINTER:
			str.Format (LoadString (IDS_CONFIRMPRINTERDELETE), p->m_pPrinter->m_strName);

			if (MsgBox (* this, str, NULL, MB_ICONEXCLAMATION | MB_YESNO) == IDYES) { 
				if (DeletePrinterRecord (theApp.m_db, p->m_pPrinter->m_lID)) {
					m_sel.m_lID = -1;
					FillTreeCtrl ();
				}
			}
			break;
		case CTreeItem::LINE:
			str.Format (LoadString (IDS_DELETELINE), p->m_pLine->m_strName);

			if (MsgBox (* this, str, NULL, MB_ICONWARNING | MB_YESNO) == IDYES) {
				DeleteLine (theApp.m_db, p->m_pLine->m_lID);
				m_sel.m_lID = -1;
				m_sel.m_lType = p->m_type;
				FillTreeCtrl ();
			}
			break;
		case CTreeItem::HEAD:
			str.Format (LoadString (IDS_DELETEHEAD), p->m_pHead->m_strName);

			if (MsgBox (* this, str, NULL, MB_ICONWARNING | MB_YESNO) == IDYES) {
				DeleteHeadRecord (theApp.m_db, p->m_pHead->m_lID);
				m_sel.m_lID = -1;
				m_sel.m_lType = p->m_type;
				FillTreeCtrl ();
			}
			break;
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
*/
}

bool CConfigDlg::Edit (PRINTERSTRUCT & p)
{
	DECLARETRACECOUNT (100);
	MARKTRACECOUNT ();
	/*
	CLock lock (m_dwLockThreadID, p.m_lID);

	if (!lock.Lock (this))
		return false;
	*/

	const bool bTransactionPending = theApp.m_db.IsTransactionPending ();
	CArray <PRINTERSTRUCT, PRINTERSTRUCT &> v;

	if (!bTransactionPending)
		BEGIN_TRANS (theApp.m_db);

	MARKTRACECOUNT ();
	GetPrinterRecords (theApp.m_db, v);

	while (1) {
		CPrinterDlg dlg (this);
		CArray <HEADSTRUCT, HEADSTRUCT &> vOldHeads, vNewHeads;

		if (p.m_bMaster) {
			CArray <LINESTRUCT, LINESTRUCT &> vLines;

			GetPrinterLines (theApp.m_db, p.m_lID, vLines);

			for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
				LINESTRUCT line = vLines [nLine];
				CArray <HEADSTRUCT, HEADSTRUCT &> v;

				GetLineHeads (theApp.m_db, line.m_lID, v);
				vOldHeads.Append (v);
			}
		}

		dlg.m_strName		= p.m_strName;
		dlg.m_bMaster		= p.m_bMaster;
		dlg.m_strAddress	= p.m_strAddress;
		dlg.m_lID			= p.m_lID;
		dlg.m_bAddLine		= m_bPrinterAddLine;
		dlg.m_bAddHead		= m_bPrinterAddHead;
		dlg.m_lLineID		= m_lPrinterAddHead;

		if (dlg.DoModal () == IDOK) {
			bool bValid = true;

			MARKTRACECOUNT ();

			p.m_strName		= dlg.m_strName;
			p.m_bMaster		= dlg.m_bMaster ? true : false;
			p.m_strAddress	= dlg.m_strAddress;

			MARKTRACECOUNT ();

			{ // verify no name/addr conflict exists
				for (int i = 0; bValid && (i < v.GetSize ()); i++) {
					if (v [i].m_lID != p.m_lID) {
						bool bNameConflict = !p.m_strName.CompareNoCase (v [i].m_strName);
						bool bAddrConflict = !p.m_strAddress.CompareNoCase (v [i].m_strAddress);

						bValid = !bNameConflict && !bAddrConflict;

						if (bNameConflict)
							MsgBox (* this, LoadString (IDS_NAMECONFLICT), NULL, MB_ICONERROR);

						if (bAddrConflict) 
							MsgBox (* this, LoadString (IDS_ADDRESSCONFLICT), NULL, MB_ICONERROR);
					}
				}
			}

			MARKTRACECOUNT ();

			if (bValid) {
				if (p.m_bMaster) {
					bool bNewHead = false;

					{
						CArray <LINESTRUCT, LINESTRUCT &> vLines;

						GetPrinterLines (theApp.m_db, p.m_lID, vLines);

						for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
							LINESTRUCT line = vLines [nLine];
							CArray <HEADSTRUCT, HEADSTRUCT &> v;

							GetLineHeads (theApp.m_db, line.m_lID, v);
							vNewHeads.Append (v);
						}
					}

					{	
						for (int i = 0; i < vNewHeads.GetSize (); i++) {
							HEADSTRUCT & head = vNewHeads [i];
							
							if (Find (vOldHeads, head.m_lID) == -1 && Find (vNewHeads, head.m_lID) != -1) {
								bNewHead = true;
								break;
							}
						}
					}

					if (bNewHead && MsgBox (* this, LoadString (IDS_ADDHEADTOALLPRINTERS), NULL, MB_YESNO) == IDYES) {
						for (int i = 0; i < vNewHeads.GetSize (); i++) {
							HEADSTRUCT h = vNewHeads [i];
							int nIndex = Find (vOldHeads, h.m_lID);

							if (nIndex == -1 && Find (vNewHeads, h.m_lID) != -1) {
								for (int nPrinter = 0; nPrinter < v.GetSize (); nPrinter++) {
									PRINTERSTRUCT & printer = v [nPrinter];

									if (!printer.m_bMaster) {
										CArray <LINESTRUCT, LINESTRUCT &> vLines;

										GetPrinterLines (theApp.m_db, printer.m_lID, vLines);

										for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
											LINESTRUCT line = vLines [nLine];
											CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;
											PANELSTRUCT panel;

											GetPanelRecords (theApp.m_db, line.m_lID, vPanels);

											ULONG lAddr = _tcstoul (h.m_strUID, NULL, 16);

											VERIFY (GetPanelRecord (theApp.m_db, h.m_lPanelID, panel));
											h.m_lPanelID = GetPanelID (vPanels, panel.m_nNumber);
											h.m_bEnabled = false;
											h.m_strUID.Format (_T ("%X"), h.IsUSB () ? GetSynonymUsbAddr (lAddr) : GetSynonymPhcAddr (lAddr));
											TRACEF (printer.m_strName + _T (", ") + printer.m_strAddress + _T ("\t") + line.m_strName + _T ("\t") + h.m_strName + _T (", ") + h.m_strUID);
											VERIFY (AddHeadRecord (theApp.m_db, h));
										}
									}
								}
							}
						}
					}


					for (int i = 0; i < v.GetSize (); i++) {
						if (v [i].m_lID != p.m_lID) {
							if (v [i].m_bMaster) {
								v [i].m_bMaster = false;
								VERIFY (UpdatePrinterRecord (theApp.m_db, v [i]));
							}
						}
					}
				}

				MARKTRACECOUNT ();

				if (!UpdatePrinterRecord (theApp.m_db, p))
					VERIFY (AddPrinterRecord (theApp.m_db, p));

				MARKTRACECOUNT ();

				if (!bTransactionPending)
					COMMIT_TRANS (theApp.m_db);

				MARKTRACECOUNT ();

				// update if addr changed
				for (int i = 0; i < m_vThreads.GetSize (); i++) {
					if (CPrinterThread * pThread = m_vThreads [i]) {
						PRINTERSTRUCT old;

						MARKTRACECOUNT ();
						CPrinterThread::GetPrinter (pThread->m_lPrinterID, old);
						MARKTRACECOUNT ();

						if (p.m_lID == old.m_lID) {
							if (p.m_strAddress != old.m_strAddress) {
								BeginWaitCursor ();
								POST_THREAD_MESSAGE (* pThread, TM_SET_PRINTER, new PRINTERSTRUCT (p));
								EndWaitCursor ();
							}
						}

						MARKTRACECOUNT ();
					}
				}

				MARKTRACECOUNT ();
				TRACECOUNTARRAY ();

				return true;
			}
		}
		else {
			if (!bTransactionPending)
				ROLLBACK_TRANS (theApp.m_db);

			return false;
		}
	}

	if (!bTransactionPending)
		ROLLBACK_TRANS (theApp.m_db);

	return false;
}

void CConfigDlg::OnPrinterEdit() 
{
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);

	HTREEITEM hItem = tree.GetSelectedItem ();

	while (hItem) {
		if (CTreeItem * p = (CTreeItem *)tree.GetItemData (hItem)) {
			if (p->m_type == CTreeItem::PRINTER) {

				ULONG lSelID = p->m_pPrinter->m_lID;
				ULONG lSelType = p->m_type;;

				if (Edit (* p->m_pPrinter)) {
					m_sel.m_lID = p->m_pPrinter->m_lID;
					m_sel.m_lType = p->m_type;
					FillListCtrl (); //FillTreeCtrl ();
				}

				break;
			}
			else
				hItem = tree.GetParentItem (hItem);
		}
	}
}

void CConfigDlg::OnPrinterAdd() 
{
	CPrinterTypeDlg dlgType (this);
	PRINTERSTRUCT printer;
	CArray <PRINTERSTRUCT, PRINTERSTRUCT &> v;

	if (dlgType.DoModal () == IDCANCEL)
		return;

	GetPrinterRecords (theApp.m_db, v);

	BEGIN_TRANS (theApp.m_db);

	printer.m_strName.Format (_T ("%s%03d"), GetDSN (), v.GetSize ());
	printer.m_strAddress = _T ("10.2.1.255");
	printer.m_bMaster = false;//v.GetSize () > 0 ? false : true;
	printer.m_type = dlgType.m_type;
	printer.m_lID = -1;

	if (Edit (printer)) {
		if (v.GetSize () > 0) {
			for (int i = 0; i < v.GetSize (); i++) {
				PRINTERSTRUCT & copy = v [i];

				if (copy.m_bMaster) { // copy config from master printer
					CArray <LINESTRUCT, LINESTRUCT &> vLines;

					GetPrinterLines (theApp.m_db, copy.m_lID, vLines);

					for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
						LINESTRUCT line = vLines [nLine];
						CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
						CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;

						GetLineHeads (theApp.m_db, line.m_lID, vHeads);
						line.m_lPrinterID = printer.m_lID;

						VERIFY (AddLineRecord (theApp.m_db, line)); 
						GetPanelRecords (theApp.m_db, line.m_lID, vPanels);

						for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
							HEADSTRUCT head = vHeads [nHead];
							ULONG lAddr = _tcstoul (head.m_strUID, NULL, 16);
							PANELSTRUCT panel;

							VERIFY (GetPanelRecord (theApp.m_db, head.m_lPanelID, panel));
							head.m_lPanelID = GetPanelID (vPanels, panel.m_nNumber);
							head.m_strUID.Format (_T ("%X"), GetSynonymUsbAddr (lAddr)); //dlgCardType.m_nType == CCardTypeDlg::CARD_USB ? GetSynonymUsbAddr (lAddr) : GetSynonymPhcAddr (lAddr));
							VERIFY (AddHeadRecord (theApp.m_db, head));
						}
					}
				}
			}
		}
		else
			printer.m_bMaster = true;

		m_sel.m_lID		= printer.m_lID;
		m_sel.m_lType	= CTreeItem::PRINTER;
		FillListCtrl (); //FillTreeCtrl ();
		COMMIT_TRANS (theApp.m_db);
	}
	else
		ROLLBACK_TRANS (theApp.m_db);
}

void CConfigDlg::OnPrinterDelete() 
{
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);
	HTREEITEM hItem = tree.GetSelectedItem ();

	while (hItem) {
		if (CTreeItem * p = (CTreeItem *)tree.GetItemData (hItem)) {
			if (p->m_type == CTreeItem::PRINTER) {
				CString str;

				str.Format (LoadString (IDS_CONFIRMPRINTERDELETE), p->m_pPrinter->m_strName);

				if (MsgBox (* this, str, NULL, MB_ICONEXCLAMATION | MB_YESNO) == IDYES) { 
					if (DeletePrinterRecord (theApp.m_db, p->m_pPrinter->m_lID)) {
						m_sel.m_lID = -1;
						FillListCtrl (); //FillTreeCtrl ();
					}
				}

				return;
			}
			else
				hItem = tree.GetParentItem (hItem);
		}
	}
}

void CConfigDlg::OnRclickPrinters(NMHDR* pNMHDR, LRESULT* pResult) 
{
	UINT nMenuID = IDM_CONTEXT;
	UINT nDefID = -1;
	CMenu menu;
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);
	CPoint ptScreen, ptClient;

	::GetCursorPos (&ptScreen);
	ptClient = ptScreen;
	tree.ScreenToClient (&ptClient);

	if (HTREEITEM hHit = tree.HitTest (ptClient)) {
		tree.SelectItem (hHit);

		if (CTreeItem * p = (CTreeItem *)tree.GetItemData (hHit)) {
			nMenuID = p->GetMenuID ();
			nDefID = p->GetDefMenuID ();
		}
	}

	if (menu.LoadMenu (nMenuID)) {
		if (CMenu * pMenu = menu.GetSubMenu (0)) {
	
			if (nDefID != -1)
				pMenu->SetDefaultItem (nDefID);

			pMenu->TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON, ptScreen.x, ptScreen.y, this);
		}
	}
	
	if (pResult)
		* pResult = 0;
}

void CConfigDlg::OnHeadEdit() 
{
/*
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);

	HTREEITEM hItem = tree.GetSelectedItem ();

	while (hItem) {
		if (CTreeItem * p = (CTreeItem *)tree.GetItemData (hItem)) {
			if (p->m_type == CTreeItem::HEAD) {
				if (Edit (* p->m_pHead, p->m_pLine->m_lID)) {
					m_sel.m_lID = p->m_pHead->m_lID;
					m_sel.m_lType = p->m_type;
					FillTreeCtrl ();
				}

				break;
			}
			else
				hItem = tree.GetParentItem (hItem);
		}
	}
*/
}

void CConfigDlg::OnHeadDelete() 
{
/*
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);

	HTREEITEM hItem = tree.GetSelectedItem ();

	while (hItem) {
		if (CTreeItem * p = (CTreeItem *)tree.GetItemData (hItem)) {
			if (p->m_type == CTreeItem::HEAD) {
				CString str;

				str.Format (LoadString (IDS_DELETEHEAD), p->m_pHead->m_strName);

				if (MsgBox (* this, str, NULL, MB_ICONWARNING | MB_YESNO) == IDYES) {
					DeleteHeadRecord (theApp.m_db, p->m_pHead->m_lID);
					m_sel.m_lID = -1;
					m_sel.m_lType = p->m_type;
					FillTreeCtrl ();
				}

				break;
			}
			else
				hItem = tree.GetParentItem (hItem);
		}
	}
*/
}

void CConfigDlg::OnLineDelete() 
{
/*
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);

	HTREEITEM hItem = tree.GetSelectedItem ();

	while (hItem) {
		if (CTreeItem * p = (CTreeItem *)tree.GetItemData (hItem)) {
			if (p->m_type == CTreeItem::LINE) {
				CString str;

				str.Format (LoadString (IDS_DELETELINE), p->m_pLine->m_strName);

				if (MsgBox (* this, str, NULL, MB_ICONWARNING | MB_YESNO) == IDYES) {
					DeleteLine (theApp.m_db, p->m_pLine->m_lID);
					m_sel.m_lID = -1;
					m_sel.m_lType = p->m_type;
					FillTreeCtrl ();
				}

				break;
			}
			else
				hItem = tree.GetParentItem (hItem);
		}
	}
*/
}

void CConfigDlg::OnLineEdit() 
{
/*
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);
	HTREEITEM hItem = tree.GetSelectedItem ();

	while (hItem) {
		if (CTreeItem * p = (CTreeItem *)tree.GetItemData (hItem)) {
			if (p->m_type == CTreeItem::LINE) {
				BEGIN_TRANS (theApp.m_db);

				if (Edit (* p->m_pLine, p->m_pLine->m_lID)) {
					m_sel.m_lID = p->m_pLine->m_lID;
					m_sel.m_lType = p->m_type;
					COMMIT_TRANS (theApp.m_db);
					FillTreeCtrl ();
				}
				else
					ROLLBACK_TRANS (theApp.m_db);

				break;
			}
			else
				hItem = tree.GetParentItem (hItem);
		}
	}
*/
}

void CConfigDlg::OnAdd() 
{
	OnPrinterAdd ();

	/*
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);

	if (CWnd * p = GetDlgItem (BTN_ADD)) {
		CMenu menu;
		CRect rc;

		p->GetWindowRect (&rc);

		if (menu.LoadMenu (IDM_ADD)) {
			if (CMenu * pMenu = menu.GetSubMenu (0)) {
				CPoint pt (rc.right, rc.top);
				bool bPrinter = false;
				bool bLine = false;
		
				if (HTREEITEM hItem = tree.GetSelectedItem ()) {
					if (CTreeItem * pSel = (CTreeItem *)tree.GetItemData (hItem)) {
						bPrinter	= pSel->m_type == CTreeItem::PRINTER;
						bLine		= pSel->m_type == CTreeItem::LINE;
					}
				}

				//pMenu->EnableMenuItem (ID_LINE_ADD, MF_BYCOMMAND | (bPrinter	? MF_ENABLED : MF_GRAYED));
				//pMenu->EnableMenuItem (ID_HEAD_ADD, MF_BYCOMMAND | (bLine		? MF_ENABLED : MF_GRAYED));

				pMenu->TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON, pt.x, pt.y, this);
			}
		}
	}
	*/
}

void CConfigDlg::OnHeadAdd() 
{
/*
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);
	HTREEITEM hItem = tree.GetSelectedItem ();
	const HTREEITEM hOriginal = hItem;
	ULONG lLineID = -1;

	while (hItem) {
		if (CTreeItem * p = (CTreeItem *)tree.GetItemData (hItem)) {
			if (p->m_type == CTreeItem::LINE) {
				tree.SelectItem (hItem);
				lLineID = p->m_pLine->m_lID;
				break;
			}
			else
				hItem = tree.GetParentItem (hItem);
		}
	}

	m_bPrinterAddHead = true;
	m_lPrinterAddHead = lLineID;
	OnPrinterEdit ();
	m_bPrinterAddHead = false;

	tree.SelectItem (hOriginal);
*/
}

void CConfigDlg::OnLineAdd() 
{
/*
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);
	HTREEITEM hItem = tree.GetSelectedItem ();
	const HTREEITEM hOriginal = hItem;

	while (hItem) {
		if (CTreeItem * p = (CTreeItem *)tree.GetItemData (hItem)) {
			if (p->m_type == CTreeItem::PRINTER) {
				tree.SelectItem (hItem);
				break;
			}
			else
				hItem = tree.GetParentItem (hItem);
		}
	}

	m_bPrinterAddLine = true;
	OnPrinterEdit ();
	m_bPrinterAddLine = false;

	tree.SelectItem (hOriginal);
*/
}

void CConfigDlg::OnSelchangedPrinters(NMHDR* pNMHDR, LRESULT* pResult) 
{
	ULONG lLineID = m_lLineID;

	if (!m_bFillingTreeCtrl) {
		if (NM_TREEVIEW * pNMTreeView = (NM_TREEVIEW*)pNMHDR) {
			if (pNMTreeView->itemNew.mask & TVIF_PARAM) {
				CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (IDC_PRINTERS);
				HTREEITEM hItem = (HTREEITEM)pNMTreeView->itemNew.lParam;
			
				if (CTreeItem * p = (CTreeItem *)hItem) {
					switch (p->m_type) {
					case CTreeItem::LINE:		
						lLineID = p->m_pLine->m_lID;
						break;
					case CTreeItem::PRINTER:	
					case CTreeItem::HEAD:	
						if (p->m_vLineIDs.GetSize ())
							lLineID = p->m_vLineIDs [0];
						break;
					}
				}
			}
		}

		if (m_lLineID != lLineID) {
			m_lLineID = lLineID;
			FillListCtrl ();
		}
	}

	if (pResult)
		* pResult = 0;
}

void CConfigDlg::FillListCtrl ()
{
	DECLARETRACECOUNT (100);

	CListCtrl & lv = m_lv;//* (CListCtrl *)GetDlgItem (LV_PRINTERS);
	CArray <PRINTERSTRUCT, PRINTERSTRUCT &> v;
	bool bConfigError = false;
	CSize sizeItem (0, 0);
	CRect rcCtrl (0, 0, 0, 0);

	BeginWaitCursor ();
	lv.DeleteAllItems ();
	lv.GetWindowRect (rcCtrl);

	MARKTRACECOUNT ();
	GetPrinterRecords (theApp.m_db, v);

	for (int nPrinter = 0; nPrinter < v.GetSize (); nPrinter++) {
		MARKTRACECOUNT ();

		CRect rc (0, 0, 0, 0);
		PRINTERSTRUCT & printer = v [nPrinter];
		int nImage = printer.m_bMaster ? m_lv.m_nMasterIndex : (printer.m_type == ELITE ? m_lv.m_nEliteIndex : m_lv.m_nMatrixIndex);
		int nIndex = lv.InsertItem (nPrinter, printer.m_strName, nImage);
		lv.SetItemData (nIndex, printer.m_lID);

		MARKTRACECOUNT ();
		if (Find (m_vThreads, printer.m_lID) == -1) {
			if (CPrinterThread * pThread = (CPrinterThread *)::AfxBeginThread (RUNTIME_CLASS (CPrinterThread))) {
				::WaitForSingleObject (pThread->m_hInit, INFINITE);
				MARKTRACECOUNT ();
				SEND_THREAD_MESSAGE (* pThread, TM_SET_PRINTER, new PRINTERSTRUCT (printer));
				SEND_THREAD_MESSAGE (* pThread, TM_SET_HWND, m_hWnd);
				MARKTRACECOUNT ();
				m_vThreads.Add (pThread);
			}
		}

		MARKTRACECOUNT ();
		lv.GetItemRect (nIndex, rc, LVIR_BOUNDS);
		sizeItem.cx = max (sizeItem.cx, rc.Width ());
		sizeItem.cy = max (sizeItem.cy, rc.Height ());
	}

	MARKTRACECOUNT ();
	{
		for (int i = m_vThreads.GetSize () - 1; i >= 0; i--) {
			if (CPrinterThread * pThread = m_vThreads [i]) {
				if (Find (v, pThread->m_lPrinterID) == -1) {
					MARKTRACECOUNT ();
					POST_THREAD_MESSAGE (* pThread, TM_CLOSE, 0);
					MARKTRACECOUNT ();
					VERIFY (::WaitForSingleObject (pThread->m_hThread, 6000) == WAIT_OBJECT_0);
					//delete pThread;
					m_vThreads.RemoveAt (i);
					MARKTRACECOUNT ();
				}
			}
		}
	}

	MARKTRACECOUNT ();

	{
		CPoint pt (0, 0);

		lv.SetItemPosition (0, pt);

		pt.y += sizeItem.cy;

		for (int i = 1; i < lv.GetItemCount (); i++) {
			if ((pt.x + sizeItem.cx) >= rcCtrl.Width ()) {
				pt.x = 0;
				pt.y += sizeItem.cy;
			}

			lv.SetItemPosition (i, pt);
			pt.x += sizeItem.cx;
		}
	}

	lv.SetItemState (m_nSelected, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);

	EndWaitCursor ();

	TRACECOUNTARRAY ();
}

void CConfigDlg::OnDeleteTask() 
{
	CListCtrl & lv = * (CListCtrl *)GetDlgItem (LV_TASKS);
	CLongArray sel = ItiLibrary::GetSelectedItems (lv);

	if (sel.GetSize ()) {
		CString str;

		str.Format (LoadString (IDS_CONFIRMTASKDELETE));

		if (MsgBox (* this, str, NULL, MB_ICONWARNING | MB_YESNO) == IDYES) {
			for (int i = 0; i < sel.GetSize (); i++) {
				CString strTask = lv.GetItemText (sel [i], 0);
				ULONG lTaskID = _ttol (lv.GetItemText (sel [i], 2));
			
				VERIFY (DeleteTaskRecord (theApp.m_db, lTaskID));
			}

			FillListCtrl ();
		}
	}
	else 
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CConfigDlg::OnEditTask() 
{
	ULONG lPrinterID = GetMasterPrinterID (theApp.m_db);
	ULONG lTaskID = -1;

	HANDLE hMutex = ::CreateMutex (NULL, FALSE, ISEDITORRUNNING);
	bool bRunning = 
		GetLastError () == ERROR_ALREADY_EXISTS || 
		GetLastError () == ERROR_ACCESS_DENIED;

	if (hMutex)
		::CloseHandle (hMutex);

	CString strApp = FoxjetDatabase::GetHomeDir () + (IsGojo () ? _T ("\\Legacy") : _T ("")) + _T("\\MkDraw.exe");
	CString strCmd = strApp;
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [0xFF];

	/*
	{
		CListCtrl & lv = * (CListCtrl *)GetDlgItem (LV_TASKS);
		CLongArray sel = ItiLibrary::GetSelectedItems (lv);

		if (sel.GetSize ()) {
			CString strTask = lv.GetItemText (sel [0], 0);
			CString strLine;
			CArray <LINESTRUCT, LINESTRUCT &> vLines;

			lTaskID = _ttol (lv.GetItemText (sel [0], 2));

			GetPrinterLines (theApp.m_db, lPrinterID, vLines);

			for (int i = 0; i < vLines.GetSize (); i++) {
				if (vLines [i].m_lID == m_lLineID) {
					strLine = vLines [i].m_strName;
					break;
				}
			}

			strCmd += _T (" \"") +
				FoxjetFile::GetRootPath () +
				strLine + _T ("\\") + 
				strTask + _T (".") +
				FoxjetFile::GetFileExt (FoxjetFile::TASK) +
				_T ("\" ");
		}
	}
	*/

	strCmd += _T (" /nosplash");
	//strCmd += _T (" /dsn=") + GetDSN ();
	strCmd += _T (" /MPHC");
	strCmd += _T (" /PrinterID=") + ToString (lPrinterID);
	strCmd += _T (" /LockID=0");

	{
		TRACEF (::GetCommandLine ());
		CString strDSN = FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), _T ("/dsn="));
		CString strKey = _T ("Software\\Foxjet\\MarksmanELITE\\ODBC\\") + strDSN;
		CString strUID = FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), _T ("/UID="));
		CString strPWD = FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), _T ("/PWD="));

		if (strDSN.GetLength ()) 
			strCmd += _T (" /dsn=") + strDSN;

		if (IsGojo ()) {
			if (!strUID.GetLength ()) strUID = _T ("FoxJet");
			if (!strPWD.GetLength ()) strPWD = _T ("FoxJet");
		}

		TRACEF (strKey);
		TRACEF (_T ("/UID=") + strUID + _T (" /PWD=") + strPWD);
		strCmd += _T (" /UID=") + strUID + _T (" /PWD=") + strPWD;
	}

	{
		CString str = ::GetCommandLine ();

		str.MakeUpper ();

		if (str.Find (_T ("/WRITE_TEST")) != -1)
			strCmd += _T (" /WRITE_TEST");
	}

	if (bRunning) {
		DWORD dwResult = 0;

		LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_ISEDITORRUNNING, 0, 0,  SMTO_NORMAL | SMTO_ABORTIFHUNG, 1, &dwResult);

		lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_EDITOR_OPEN_TASK, lPrinterID, lTaskID,  SMTO_NORMAL | SMTO_ABORTIFHUNG, 1, &dwResult);

		return;
	}


	TRACEF (strApp);
	TRACEF (strCmd);

	_tcsncpy (szCmdLine, strCmd, 0xFF);
    memset (&pi, 0, sizeof(pi));
    memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	BOOL bResult = ::CreateProcess (NULL, szCmdLine, NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi); 

	if (!bResult) 
		MsgBox (* this, LoadString (IDS_FAILEDTOLANUCHEDITOR));
}

void CConfigDlg::OnAddTask() 
{
	DWORD dwResult = 0;

	BeginWaitCursor ();

	HANDLE hMutex = ::CreateMutex (NULL, FALSE, ISEDITORRUNNING);
	bool bRunning = 
		GetLastError () == ERROR_ALREADY_EXISTS || 
		GetLastError () == ERROR_ACCESS_DENIED;

	if (hMutex)
		::CloseHandle (hMutex);

	if (!bRunning) {
		OnEditTask ();
		//::Sleep (2000);
	}

	CEvent eventIsRunning (false, true, _T ("WM_ISEDITORRUNNING"), NULL);
	CEvent eventNewTask (false, true, _T ("WM_EDITOR_NEW_TASK"), NULL);

	bRunning = false;

	for (int i = 0; i < 20; i++) {
		LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_ISEDITORRUNNING, 0, 0,  SMTO_NORMAL | SMTO_ABORTIFHUNG, 1, &dwResult);

		DWORD dwWait = ::WaitForSingleObject (eventIsRunning, 250);

		if (dwWait == WAIT_OBJECT_0)	{ 
			TRACEF ("WAIT_OBJECT_0"); 
			bRunning = true;
			break; 
		}
		if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
		if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
		if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");
	}

	if (bRunning) {
		LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_EDITOR_NEW_TASK, 0, 0,  SMTO_NORMAL | SMTO_ABORTIFHUNG, 1, &dwResult);

		DWORD dwWait = ::WaitForSingleObject (eventNewTask, 5000);

		if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
		if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
		if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
		if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");

		ASSERT (dwWait == WAIT_OBJECT_0);
	}

	EndWaitCursor ();
}

LRESULT CConfigDlg::OnTasksChanged (WPARAM wParam, LPARAM lParam)
{
	FillListCtrl ();
	return WM_EDITOR_TASK_CHANGE;
}

LRESULT CConfigDlg::OnPrinterStateChange (WPARAM wParam, LPARAM lParam)
{
	ULONG lPrinterID = (ULONG)wParam;
	int nIndex = m_lv.GetIndex (lPrinterID);
	CArray <int, int> vRedraw;
	const CString strConnected		= _T ("{") + CString (PRINTERCTRL_CONNECTED)	+ _T ("}");
	const CString strDisconnected	= _T ("{") + CString (PRINTERCTRL_DISCONNECTED) + _T ("}");
	const CString strMatrix			= _T ("{") + CString (PRINTERCTRL_MATRIX)		+ _T ("}");
	const CString strElite			= _T ("{") + CString (PRINTERCTRL_ELITE)		+ _T ("}");
	const LPCTSTR lpszState [] = { _T ("DISABLED"), _T ("RUNNING"), _T ("IDLE"), _T ("STOPPED") }; 

	/*
		{Get current task,LINE0001,768 Sample}
		{Get current state,LINE0001,21,1,\{LOW TEMP\,HIGH VOLTAGE ERROR\}}
		{Get count,LINE0001,0}
		
		{Get current task,LINE0002,384 Sample}
		{Get current state,LINE0002,22,1,\{LOW TEMP\,HIGH VOLTAGE ERROR\}}
		{Get count,LINE0002,0}
	*/

	if (CString * pstr = (CString *)lParam) {
		CStringArray v, vLines;

		ParsePackets (* pstr, v);

		//TRACEF (ToString (lPrinterID) + _T (": ") + * pstr);

		for (int i = 0; i < v.GetSize (); i++) {
			CStringArray vPacket;

			//TRACEF (ToString (lPrinterID) + _T (": ") + v [i]);
			Tokenize (v [i], vPacket);

			if (vPacket.GetSize ()) {
				if (!vPacket [0].CompareNoCase (BW_HOST_PACKET_GET_LINES)) {
					for (int i = 1; i < vPacket.GetSize (); i++) {
						vLines.Add (vPacket [i]);
						m_lv.m_mapData.SetAt (ToString (lPrinterID) + _T (":line[") + ToString (i - 1) + _T ("]"), vPacket [i]);
					}
				}
			}
		}

		for (int i = 0; i < v.GetSize (); i++) {
			CStringArray vPacket;

			//TRACEF (v [i]);
			//TRACEF (ToString (lPrinterID) + _T (": ") + v [i]);
			Tokenize (v [i], vPacket);

			if (vPacket.GetSize ()) {
				CString strKey, strData, strOldData;
				CArray <TASKSTATE, TASKSTATE> vLineState;
				CStringArray vHeadState;
				int nLine = Find (vLines, GetParam (vPacket, 1));

				if (!vPacket [0].CompareNoCase (PRINTERCTRL_MASTER) ||
					!vPacket [0].CompareNoCase (PRINTERCTRL_MATRIX) ||
					!vPacket [0].CompareNoCase (PRINTERCTRL_ELITE))
				{
					strKey	= ToString (lPrinterID) + _T (":type");
					strData = vPacket [0];
				}
				else if (!vPacket [0].CompareNoCase (PRINTERCTRL_DISCONNECTED)) {
					m_lv.m_mapData.SetAt (ToString (lPrinterID) + _T (":task[0]"), _T (""));
					m_lv.m_mapData.SetAt (ToString (lPrinterID) + _T (":task[1]"), _T (""));
					m_lv.m_mapData.SetAt (ToString (lPrinterID) + _T (":count[0]"), _T (""));
					m_lv.m_mapData.SetAt (ToString (lPrinterID) + _T (":count[1]"), _T (""));
					m_lv.m_mapData.SetAt (ToString (lPrinterID) + _T (":state[0]"), _T (""));
					m_lv.m_mapData.SetAt (ToString (lPrinterID) + _T (":state[1]"), _T (""));
					m_lv.m_mapData.SetAt (ToString (lPrinterID) + _T (":connected"), PRINTERCTRL_DISCONNECTED);
					//m_lv.m_mapData.SetAt (ToString (lPrinterID) + _T (":type"), _T (""));

					if (Find (vRedraw, nIndex) == -1)
						vRedraw.Add (nIndex);

					//strKey	= ToString (lPrinterID) + _T (":connected");
					//strData = vPacket [0];
				}
				else if (!vPacket [0].CompareNoCase (PRINTERCTRL_CONNECTED)) {
					strKey	= ToString (lPrinterID) + _T (":connected");
					strData = vPacket [0];
				}
				else if (!vPacket [0].CompareNoCase (BW_HOST_PACKET_GET_STROBE)) {
					strKey	= ToString (lPrinterID) + _T (":strobe");
					strData = UnformatString (GetParam (vPacket, 1));
				}

				if (nLine != -1) {
					if (!vPacket [0].CompareNoCase (BW_HOST_PACKET_GET_CURRENT_TASK)) {
						strKey	= ToString (lPrinterID) + _T (":task[") + ToString (nLine) + _T ("]");
						strData = GetParam (vPacket, 2);
					}
					else if (!vPacket [0].CompareNoCase (BW_HOST_PACKET_GET_COUNT)) {
						strKey	= ToString (lPrinterID) + _T (":count[") + ToString (nLine) + _T ("]");
						__int64 lCount = FoxjetCommon::strtoul64 (GetParam (vPacket, 2));
						strData = FormatI64 (lCount);
					}
					else if (!vPacket [0].CompareNoCase (BW_HOST_PACKET_GET_CURRENT_STATE)) {
						CStringArray vTmp;

						strKey	= ToString (lPrinterID) + _T (":state[") + ToString (nLine)   + _T ("]");

						for (int j = 2; j < vPacket.GetSize (); j++) 
							vTmp.Add (vPacket [j]);

						for (int nParam = 2; nParam < vPacket.GetSize (); ) {
							CStringArray vState;
							ULONG lHeadID = _tcstoul (GetParam (vPacket, nParam++), NULL, 10);
							TASKSTATE eState = (TASKSTATE)_tcstoul (GetParam (vPacket, nParam++), NULL, 10);
							ParsePackets (UnformatString (GetParam (vPacket, nParam++)), vState);

							if (eState != DISABLED)
								if (Find (vLineState, eState) == -1)
									vLineState.Add (eState);

							for (int nState = 0; nState < vState.GetSize (); nState++) {
								//TRACEF (vState [nState]);
							
								if (Find (vHeadState, vState [nState]) == -1)
									vHeadState.Add (vState [nState]);
							}
						}

						strData = ToString (vTmp);
					}
				}

				{
					if (vLineState.GetSize ()) 
						m_lv.m_mapData.SetAt (ToString (lPrinterID) + _T (":TASKSTATE[") + ToString (nLine) + _T ("]"), ToString ((int)vLineState [0]));

					if (vHeadState.GetSize ()) 
						m_lv.m_mapData.SetAt (ToString (lPrinterID) + _T (":HEADSTATE[") + ToString (nLine) + _T ("]"), ToString (vHeadState));
				}

				m_lv.m_mapData.Lookup (strKey, strOldData);

				if (strOldData != strData) {
					if (Find (vRedraw, nIndex) == -1)
						vRedraw.Add (nIndex);

					m_lv.m_mapData.SetAt (strKey, strData);
				}
			}
		}

		delete pstr;
	}

	for (int i = 0; i < vRedraw.GetSize (); i++)
		m_lv.RedrawItems (vRedraw [i], vRedraw [i]);

	return 0;
}

void CConfigDlg::OnItemdblclickTasks(NMHDR* pNMHDR, LRESULT* pResult) 
{
	HD_NOTIFY *phdn = (HD_NOTIFY *) pNMHDR;
	CListCtrl & lv = * (CListCtrl *)GetDlgItem (LV_TASKS);
	CLongArray sel = ItiLibrary::GetSelectedItems (lv);

	if (sel.GetSize ())
		OnEditTask ();

	if (pResult)
		* pResult = 0;
}

void CConfigDlg::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent == ID_STROBE) {
		for (int nItem = 0; nItem < m_lv.GetItemCount (); nItem++) {
			const ULONG lPrinterID = m_lv.GetItemData (nItem);
			CString strKey, strState;

			strKey.Format (_T ("%d:strobe"), lPrinterID);
			strState.Empty ();
			m_lv.m_mapData.Lookup (strKey, strState);

			if (strState.Find (_T ("FLASH")) != -1) {
				strKey.Format (_T ("%d:strobe_current"), lPrinterID);
				strState.Empty ();
				m_lv.m_mapData.Lookup (strKey, strState);
				m_lv.m_mapData.SetAt (strKey, _ttoi (strState) ? _T ("0") : _T ("1"));
				m_lv.RedrawItems (nItem, nItem);
			}
		}
	}
	
	CDialog::OnTimer(nIDEvent);
}

void CConfigDlg::OnInitMenu(CMenu* pMenu) 
{
	CDialog::OnInitMenu(pMenu);
	
	pMenu->DeleteMenu (ID_FILE_VIEWLOCKS, MF_BYCOMMAND);

	if (theApp.m_db.IsSqlServer ())
		pMenu->DeleteMenu (ID_FILE_COMPACTDATABASE, MF_BYCOMMAND);
}

void CConfigDlg::OnClose() 
{
	HANDLE * pHandles = new HANDLE [m_vThreads.GetSize ()];

	BeginWaitCursor ();
	m_lv.SetRedraw (FALSE);

	for (int i = 0; i < m_vThreads.GetSize (); i++) {
		if (CPrinterThread * pThread = m_vThreads [i]) {
			pHandles [i] = pThread->m_hThread;
			POST_THREAD_MESSAGE (* pThread, TM_CLOSE, 0);
		}
	}

	VERIFY (::WaitForMultipleObjects (m_vThreads.GetSize (), pHandles, TRUE, 6000) == WAIT_OBJECT_0);
	delete [] pHandles;

	m_vThreads.RemoveAll ();
	EndWaitCursor ();
	EndDialog (IDOK);	
}

void CConfigDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if (nIDCtl == LV_PRINTERS) {
		TRACEF ("LV_PRINTERS");
	}

	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

LRESULT CConfigDlg::OnColorsChanged (WPARAM wParam, LPARAM lParam)
{
	m_lv.RedrawItems (0, m_lv.GetItemCount ());
	return 0;
}

LRESULT CConfigDlg::OnIsAppRunning (WPARAM wParam, LPARAM lParam)
{
	::AfxGetMainWnd ()->SetForegroundWindow ();

	return WM_ISCONFIGRUNNING;
}

CString GetLineParam (int nIndex)
{
	CArray <LINESTRUCT, LINESTRUCT &> vLines;

	GetPrinterLines (theApp.m_db, GetMasterPrinterID (theApp.m_db), vLines);

	if (vLines.GetSize () > nIndex)
		return vLines [nIndex].m_strName;

	return _T ("");
}

void CConfigDlg::OnStart (UINT nCmdID)
{
	CStartTaskDlg dlg;
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CLongArray sel = GetSelectedItems (m_lv);

	if (!sel.GetSize ())
		return;

	GetPrinterLines (theApp.m_db, GetMasterPrinterID (theApp.m_db), vLines);

	dlg.m_strRegKey = _T ("Software\\Foxjet\\Config");
	//dlg.m_bContinuousCount = theApp.IsContinuousCount ();

	//dlg.m_strTaskName = pDoc->GetSelectedLine()->GetCurrentTask ();

	dlg.m_sLineName = GetLineParam (nCmdID - ID_LINE001_START);

	if (dlg.m_sLineName.IsEmpty ())
		return;


	TRACEF (_T ("OnStart: ") + ToString ((int)nCmdID));

	if (dlg.DoModal () == IDOK) {
		ULONG lPrinterID = m_lv.GetItemData (sel [0]);

		TRACEF (dlg.m_strTaskName);

		for (int i = m_vThreads.GetSize () - 1; i >= 0; i--) {
			if (CPrinterThread * pThread = m_vThreads [i]) {
				if (pThread->m_lPrinterID == lPrinterID) {
				}
			}
		}

		//dlg.m_bResetCounts
		//dlg.m_lTaskID;
		//dlg.m_strTaskName;
	}
}

void CConfigDlg::OnStop (UINT nCmdID)
{
	CString strLine = GetLineParam (nCmdID - ID_LINE001_STOP);

	if (strLine.IsEmpty ())
		return;

	if (MsgBox (* this, LoadString (IDS_STOPMESSAGE), NULL, MB_YESNO) == IDYES) {
	}
}

void CConfigDlg::OnIdle (UINT nCmdID)
{
	CString strLine = GetLineParam (nCmdID - ID_LINE001_IDLE);

	if (strLine.IsEmpty ())
		return;

	if (MsgBox (* this, LoadString (IDS_IDLEMESSAGE), NULL, MB_YESNO) == IDYES) {
	}
}

void CConfigDlg::OnResume (UINT nCmdID)
{
	CString strLine = GetLineParam (nCmdID - ID_LINE001_RESUME);

	if (strLine.IsEmpty ())
		return;

	TRACEF (strLine);
}