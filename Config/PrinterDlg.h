#if !defined(AFX_PRINTERDLG_H__FEFBEA06_4DE9_47E8_9A5D_8FF069C7B45F__INCLUDED_)
#define AFX_PRINTERDLG_H__FEFBEA06_4DE9_47E8_9A5D_8FF069C7B45F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrinterDlg.h : header file
//

#include "RoundButtons.h"

/////////////////////////////////////////////////////////////////////////////
// CPrinterDlg dialog

class CPrinterDlg : public CDialog
{
// Construction
public:
	CPrinterDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPrinterDlg)
	enum { IDD = IDD_PRINTER };
	CIPAddressCtrl	m_ctrlAddress;
	CString m_strAddress;
	BOOL	m_bMaster;
	CString	m_strName;
	//}}AFX_DATA

	ULONG m_lID;
	bool m_bAddLine;
	bool m_bAddHead;
	ULONG m_lLineID;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrinterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	FoxjetUtils::CRoundButtons m_buttons;

// Implementation
protected:
	virtual void OnOK();
	CStringArray m_vReports;

	// Generated message map functions
	//{{AFX_MSG(CPrinterDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnLines();
	afx_msg void OnHeads();
	afx_msg void OnReports();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	//}}AFX_MSG
	afx_msg void OnSelectReport (UINT nID);

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTERDLG_H__FEFBEA06_4DE9_47E8_9A5D_8FF069C7B45F__INCLUDED_)
