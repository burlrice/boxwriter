// ElLstDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MkDraw.h"
#include "ElLstDlg.h"
#include "List.h"
#include "Coord.h"
#include "ItiLibrary.h"
#include "TaskDoc.h"
#include "TaskView.h"
#include "Extern.h"

using namespace EditorGlobals;
using namespace FoxjetCommon;
using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace FoxjetDocument;
using namespace TaskDoc;
using namespace TaskView;
using namespace Panel;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CElementListDlg::CElementItem::CElementItem (Element::CElement & e, CEditorElementList & list, UNITS units)
:	m_element (e),
	m_list (list),
	m_units (units)
{
}

CElementListDlg::CElementItem::~CElementItem ()
{
}

int CElementListDlg::CElementItem::Compare (const ListCtrlImp::CItem & rhs, int nColumn) const
{
	const CElementItem & eRhs = (CElementItem &)rhs;

	switch (nColumn) {
	case 0:
		return m_element.GetElement ()->GetID () - eRhs.m_element.GetElement ()->GetID ();
	case 4:
		return (int)m_element.IsSelected () - (int)eRhs.m_element.IsSelected ();
	}

	return CItem::Compare (rhs, nColumn);
}

CString CElementListDlg::CElementItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0:
		str.Format (_T ("%d"), m_element.GetElement ()->GetID ());
		break;
	case 1:
		str = GetElementResStr (m_element.GetElement ()->GetClassID ());
		break;
	case 2:
		str = m_element.GetElement ()->GetImageData ();
		break;
	case 3:
		{
			const FoxjetDatabase::HEADSTRUCT & head = m_list.GetHead ();
			CCoord c (m_element.GetPos (head), m_units, head);
			str = c.Format ();
			break;
		}
	case 4:
		str = LoadString (m_element.IsSelected () ? IDS_YES : IDS_NO);
		break;
	case 5:
		str = m_list.GetHead ().m_strName;
		break;
	}

	return str;
}

/////////////////////////////////////////////////////////////////////////////
// CElementListDlg dialog

CElementListDlg::CElementListDlg (TaskView::CElementListPtrArray & v, CTaskDoc * pDoc, CWnd* pParent /*=NULL*/)
:	m_vLists (v),
	m_pDoc (pDoc),
	FoxjetCommon::CEliteDlg(IDD_ELEMENTLIST, pParent)
{
	ASSERT (pDoc);

	//{{AFX_DATA_INIT(CElementListDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CElementListDlg::~CElementListDlg()
{
}

void CElementListDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CElementListDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CElementListDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CElementListDlg)
	ON_BN_CLICKED(BTN_DELETE, OnDelete)
	ON_BN_CLICKED(BTN_SELECT, OnSelect)
	ON_BN_CLICKED(BTN_DESELECT, OnDeselect)
	ON_COMMAND(ID_EDIT_SELECTALL, OnEditSelectall)
	//}}AFX_MSG_MAP
	ON_NOTIFY(NM_DBLCLK, LV_ELEMENTS, OnDblclkListCtrl)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CElementListDlg message handlers

void CElementListDlg::OnDelete() 
{
	CTaskDoc & doc = * m_pDoc;
	CLongArray sel = GetSelectedItems (m_lv);
	CTaskView * pView = (CTaskView *)doc.GetView ();
	Box::CBoxParams & params = pView->GetParams ();
	const double dZoom = pView->GetZoom ();
	const CPoint ptScroll = pView->GetScrollPosition ();
	CBoundary workingHead;

	doc.GetBox ().GetWorkingHead (params, workingHead);

	for (int i = sel.GetSize () - 1; i >= 0; i--) {
		const CElementItem * pItem = (CElementItem *)m_lv.GetCtrlData (sel [i]);
	
		if (doc.m_box.IsLinked (pItem->m_list.m_bounds)) 
			sel.RemoveAt (i);
	}

	if (sel.GetSize ()) {
		int nResult = MsgBox (* this, LoadString (IDS_CONFIRMELEMENTDELETE), 
			LoadString (IDS_CONFIRM), MB_YESNO | MB_ICONQUESTION);
		
		if (nResult == IDYES) {
			BeginWaitCursor ();

			TaskDoc::CSnapshot * pSnapshot = (TaskDoc::CSnapshot *)doc.GetSnapshot ();
			Element::CPairArray vErase;

			for (int i = 0; i < sel.GetSize (); i++) {
				const CElementItem * pItem = (CElementItem *)m_lv.GetCtrlData (sel [i]);
			
				pItem->m_element.SetSelected (false, pView, pItem->m_list, workingHead.m_card);
				vErase.Add (Element::CPair (&pItem->m_element, &pItem->m_list, &workingHead.m_card));
			}

			pSnapshot->SetElementsChanged (vErase, TaskDoc::ELEMENT_DELETE);
			doc.Erase (vErase, pView);

			for (int i = sel.GetSize () - 1; i >= 0; i--) {
				const CElementItem * pItem = (CElementItem *)m_lv.GetCtrlData (sel [i]);
				CEditorElementList & v = pItem->m_list;
				int nIndex = v.GetIndex (&(pItem->m_element));
				Element::CElement * p = & v.GetElement (nIndex); //(sel [i]);

				v.RemoveAt (nIndex);
				delete p;
			}

			RefreshElements ();
			doc.SetModifiedFlag (pSnapshot);
			EndWaitCursor ();
		}
	}
}

void CElementListDlg::OnSelect() 
{
	CTaskDoc & doc = * m_pDoc;
	CLongArray sel = GetSelectedItems (m_lv);
	CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, doc.GetView ());
	CBoundary workingHead;

	doc.GetBox ().GetWorkingHead (pView->GetParams (), workingHead);

	ASSERT (pView);

	for (int i = 0; i < sel.GetSize (); i++) {
		CElementItem * pItem = (CElementItem *)m_lv.GetCtrlData (sel [i]);
		int nIndex = m_lv.GetDataIndex (pItem);
		Element::CElement & e = pItem->m_element;

		if (!doc.m_box.IsLinked (pItem->m_list.m_bounds))
			pItem->m_element.SetSelected (true, pView, pItem->m_list, workingHead.m_card);

		m_lv.UpdateCtrlData (pItem);
	}
	
	doc.Invalidate (pView->GetSelected (0), pView);
	doc.UpdateUIStructs ();
}

void CElementListDlg::OnDeselect() 
{
	CTaskDoc & doc = * m_pDoc;
	CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, doc.GetView ());
	CLongArray sel = GetSelectedItems (m_lv);
	Element::CPairArray vInvalidate = pView->GetSelected (0);
	CBoundary workingHead;

	ASSERT (pView);
	doc.GetBox ().GetWorkingHead (pView->GetParams (), workingHead);

	for (int i = 0; i < sel.GetSize (); i++) {
		CElementItem * pItem = (CElementItem *)m_lv.GetCtrlData (sel [i]);
		CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, doc.GetView ());
		Element::CElement & e = pItem->m_element;

		pItem->m_element.SetSelected (false, pView, pItem->m_list, workingHead.m_card);
		m_lv.UpdateCtrlData (pItem);
	}

	doc.Invalidate (vInvalidate, pView);
	doc.UpdateUIStructs ();
}

BOOL CElementListDlg::OnInitDialog() 
{
	using ListCtrlImp::CColumn;

	CArray <CColumn, CColumn> vCols;

	FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_ID), 20));
	vCols.Add (CColumn (LoadString (IDS_TYPE), 80));
	vCols.Add (CColumn (LoadString (IDS_DATA), 125));
	vCols.Add (CColumn (LoadString (IDS_LOCATION), 105));
	vCols.Add (CColumn (LoadString (IDS_SELECTED), 55));
	vCols.Add (CColumn (LoadString (IDS_HEAD), 80));

	m_lv.Create (LV_ELEMENTS, vCols, this, defElements.m_strRegSection);

	RefreshElements ();
	
	GetDlgItem (BTN_DELETE)->EnableWindow (!m_pDoc->GetReadOnly ());

	return TRUE;
}

void CElementListDlg::RefreshElements()
{
	UNITS units = m_pDoc->GetUnits ();
	CString strCount;
	int nCount = 0;

	m_lv.DeleteCtrlData ();

	for (int i = 0; i < m_vLists.GetSize (); i++) {
		CEditorElementList & list = * m_vLists [i];

		if (!m_pDoc->m_box.IsLinked (list.m_bounds)) {
			for (int j = 0; j < list.GetSize (); j++) {
				m_lv.InsertCtrlData (new CElementItem (list [j], list, units));
				nCount++;
			}
		}
	}

	strCount.Format (LoadString (IDS_ELEMENTCOUNT), nCount);
	SetDlgItemText (TXT_COUNT, strCount);
}

CString CElementListDlg::GetElementType(const Element::CElement &e)
{
	return GetElementResStr (e.GetElement ()->GetClassID ());
}

CString CElementListDlg::GetElementData (const Element::CElement & e)
{
	return e.GetElement ()->GetImageData ();
}

void CElementListDlg::OnDblclkListCtrl(NMHDR* pNMHDR, LRESULT* pResult)
{
	OnSelect ();
	* pResult = 0;
}

BOOL CElementListDlg::PreTranslateMessage(MSG* pMsg) 
{
	static HACCEL hAccel = NULL;
	
	if (!hAccel)
		VERIFY (hAccel = ::LoadAccelerators (::AfxGetInstanceHandle (), MAKEINTRESOURCE (IDA_LISTCTRL)));

	if (::TranslateAccelerator (m_hWnd, hAccel, pMsg))
		return TRUE;
	
	return FoxjetCommon::CEliteDlg::PreTranslateMessage(pMsg);
}

void CElementListDlg::OnEditSelectall() 
{
	m_lv.SelectAll ();
}

