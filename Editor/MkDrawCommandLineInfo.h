// MkDrawCommandLineInfo.h: interface for the CMkDrawCommandLineInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MKDRAWCOMMANDLINEINFO_H__52FBDD08_32DF_4C3B_855E_F4A6AC391A24__INCLUDED_)
#define AFX_MKDRAWCOMMANDLINEINFO_H__52FBDD08_32DF_4C3B_855E_F4A6AC391A24__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMkDrawCommandLineInfo : public CCommandLineInfo  
{
public:
	CMkDrawCommandLineInfo();
	virtual ~CMkDrawCommandLineInfo();

	virtual void ParseParam( LPCTSTR lpszParam, BOOL bFlag, BOOL bLast );

	CString ToString () const;

	bool m_bReadOnly;
	bool m_bResetDSN;
	bool m_bSplash;
	bool m_bCleanRegistry;
	CString m_strUser;

	bool m_bDelete;
	CString m_strDelete;

	bool m_bExport;
	CString m_strExport;

	bool m_bImport;
	CString m_strImport;

	CString m_strDSN;
	CString m_strDSNLocal;
	CString m_strHostAddress;

	bool m_bIV;
	bool m_bMPHC;
	bool m_bProductionConfig;
	bool m_bSw0883;
	bool m_bShowNetStatDlg;
	bool m_bKill;
	bool m_bGojo;
	bool m_bNetworked;
	bool m_bDemo;

	CString m_strUID;
	CString m_strPWD;
};

#endif // !defined(AFX_MKDRAWCOMMANDLINEINFO_H__52FBDD08_32DF_4C3B_855E_F4A6AC391A24__INCLUDED_)
