#include "stdafx.h"
#include "extern.h"
#include "alias.h"
#include "ListGlobals.h"

FoxjetCommon::CElementDefaults & EditorGlobals::defElements = ListGlobals::defElements;
CMkDrawApp EditorGlobals::theApp;
FoxjetDatabase::COdbcDatabase & EditorGlobals::database = EditorGlobals::theApp.GetDB ();
