#include "stdafx.h"
#include "mkdraw.h"
#include "TaskDoc.h"
#include "Debug.h"
#include "Database.h"
#include "Compare.h"
#include "Panel.h"
#include "TemplExt.h"


using namespace FoxjetDatabase;
using namespace FoxjetDocument;
using namespace Foxjet3d;
using namespace TaskDoc;
using namespace Element;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//IMPLEMENT_DYNAMIC (TaskDoc::CHint, FoxjetDocument::CHint)
#ifdef _AFXDLL
	CRuntimeClass* PASCAL TaskDoc::CHint::_GetBaseClass() 
	{ 
		return RUNTIME_CLASS(FoxjetDocument::CHint); 
	}
 
	AFX_COMDAT const AFX_DATADEF CRuntimeClass TaskDoc::CHint::classCHint = 
	{ 
		"TaskDoc::CHint", sizeof(class TaskDoc::CHint), 0xFFFF, NULL,
		&TaskDoc::CHint::_GetBaseClass, NULL 
	}; 

	CRuntimeClass * TaskDoc::CHint::GetRuntimeClass() const 
	{ 
		return RUNTIME_CLASS(TaskDoc::CHint); 
	}

	CRuntimeClass* PASCAL TaskDoc::CHint::GetThisClass() 
	{ 
		//#define _RUNTIME_CLASS(class_name) ((CRuntimeClass*)(&class_name::class##class_name))
		return (CRuntimeClass*)(&TaskDoc::CHint::classCHint);//_RUNTIME_CLASS(TaskDoc::CHint); 
	} 

#else //!_AFXDLL
	AFX_COMDAT const AFX_DATADEF CRuntimeClass TaskDoc::CHint::classCHint = 
	{ 
		"TaskDoc::CHint", sizeof(class TaskDoc::CHint), 0xFFFF, NULL, 
		NESTED_RUNTIME_CLASS (CBaseDoc, CHint), NULL 
	}; 

	CRuntimeClass* TaskDoc::CHint::GetRuntimeClass() const 
	{
		return RUNTIME_CLASS(TaskDoc::CHint); 
	} 
#endif //_AFXDLL


/*
#define IMPLEMENT_DYNAMIC(class_name, base_class_name) \
	IMPLEMENT_RUNTIMECLASS(class_name, base_class_name, 0xFFFF, NULL, NULL)
/////////////////////////////////////////////////////////////////////////////
// 
#ifdef _AFXDLL
	CRuntimeClass* PASCAL CTaskDoc::CHint::_GetBaseClass() 
	{ 
		return NESTED_RUNTIME_CLASS (CBaseDoc, CHint); 
	}
 
	AFX_COMDAT const AFX_DATADEF CRuntimeClass CTaskDoc::CHint::classCHint = 
	{ 
		"CTaskDoc::CHint", sizeof(class CTaskDoc::CHint), 0xFFFF, NULL,
		&CTaskDoc::CHint::_GetBaseClass, NULL 
	}; 

	CRuntimeClass * CTaskDoc::CHint::GetRuntimeClass() const 
	{ 
		return NESTED_RUNTIME_CLASS (CTaskDoc, CHint); 
	}
#else //!_AFXDLL
	AFX_COMDAT const AFX_DATADEF CRuntimeClass CTaskDoc::CHint::classCHint = 
	{ 
		"CTaskDoc::CHint", sizeof(class CTaskDoc::CHint), 0xFFFF, NULL, 
		NESTED_RUNTIME_CLASS (CBaseDoc, CHint), NULL 
	}; 

	CRuntimeClass* CTaskDoc::CHint::GetRuntimeClass() const 
	{
		return NESTED_RUNTIME_CLASS (CTaskDoc, CHint); 
	} 
#endif //_AFXDLL
*/

TaskDoc::CHint::CHint (const CTaskMembers * pCurrent, const CSnapshot * pOld, const Element::CPair & pair)
:	FoxjetDocument::CHint (pCurrent, pOld)
{
	m_vChanged.Add (CPair (pair));

	CHint::CHint (pCurrent, pOld, NULL);
}

TaskDoc::CHint::CHint (const CTaskMembers * pCurrent, const CSnapshot * pOld, const Element::CPairArray * pvChanged)
:	FoxjetDocument::CHint (pCurrent, pOld)
{
	ASSERT (pCurrent);

	if (pvChanged) 
		m_vChanged.Append (* pvChanged);

	for (int i = 0; i < 6; i++)
		m_bUpdatedPanels [i] = false;

	if (pCurrent == pOld)
		return;

	if (!pOld) {
		// assume everything changed
		CTaskMembers::operator = (* pCurrent);
		m_lType = ALL;

		ASSERT (0 == "Warning: constructing CTaskDoc::CHint with a NULL snapshot is not recommended");

		for (int i = 0; i < 6; i++)
			m_bUpdatedPanels [i] = true;
	}
	else {
		m_lType |= SetChangedBox	(pCurrent, pOld);
		m_lType |= SetChangedPanel	(pCurrent, pOld);
	}
}

TaskDoc::CHint::~CHint ()
{
}

ULONG TaskDoc::CHint::SetChangedBox (const CTaskMembers * pCurrent, const CSnapshot * pOld)
{
	ULONG lResult = NONE;
	ASSERT (pCurrent);
	ASSERT (pOld);

	CString strCurrent = pCurrent->m_box.ToString (pCurrent->m_version);
	CString strOld = pOld->m_strBox;

	if (strCurrent != strOld) {
		//TRACEF (pCurrent->m_box.GetBox ().m_strName);
		//TRACEF (strCurrent);
		ASSERT (strCurrent.Find (pCurrent->m_box.GetBox ().m_strName) != -1);

		m_box.SetBox (pCurrent->m_box.GetBox ()); // updates images automatically
		
		lResult |= Hint::BOX;
	}

	return lResult;
}

ULONG TaskDoc::CHint::SetChangedPanel (const CTaskMembers * pCurrent, const CSnapshot * pOld)
{
	ULONG lResult = NONE;
	const Panel::CPanel * pCurrentPanels [6] = { NULL };
	const Panel::CPanel * pOldPanels [6] = { NULL };
	CTask oldTask;

	ASSERT (pCurrent);
	ASSERT (pOld);

	oldTask.FromString (pOld->m_strTask, pOld->m_version);

	// compare transforms
	if (pCurrent->m_task.m_lTransformation != oldTask.m_lTransformation) {
		for (int i = 0; i < 6; i++)
			m_bUpdatedPanels [i] = true;

		lResult |= Hint::PANEL;
		return lResult;
	}

	return lResult;
}

bool TaskDoc::CHint::SetBox (const FoxjetDatabase::BOXSTRUCT & box)
{
	m_box.SetBox (box);
	m_lType |= Hint::BOX;
	return true;
}
