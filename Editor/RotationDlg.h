#if !defined(AFX_ROTATIONDLG_H__B7DC0A3D_5D36_427B_8DDB_2626E272AF7D__INCLUDED_)
#define AFX_ROTATIONDLG_H__B7DC0A3D_5D36_427B_8DDB_2626E272AF7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RotationDlg.h : header file
//

#include "utils.h"

/////////////////////////////////////////////////////////////////////////////
// CRotationDlg dialog

class CRotationDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CRotationDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRotationDlg)
	double	m_dXAxis;
	double	m_dYAxis;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRotationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRotationDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONDLG_H__B7DC0A3D_5D36_427B_8DDB_2626E272AF7D__INCLUDED_)
