#include "stdafx.h"
#include "mkdraw.h"
#include "NewTaskDocType.h"
#include "TaskDoc.h"
#include "TaskView.h"
#include "TaskFrame.h"
#include "Extern.h"
#include "Debug.h"
#include "Box.h"
#include "Parse.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace EditorGlobals;
using namespace FoxjetDocument;
using namespace Foxjet3d;
using namespace FoxjetFile;
using namespace TaskDoc;
using namespace TaskView;


IMPLEMENT_DYNAMIC (CTaskDocTemplate, CBaseDocTemplate)

CTaskDocTemplate::CTaskDocTemplate ()
:	FoxjetDocument::CBaseDocTemplate (IDR_TASK,
									  RUNTIME_CLASS(CTaskDoc), 
									  RUNTIME_CLASS(CTaskFrame), 
									  RUNTIME_CLASS(CTaskView),
									  FoxjetFile::TASK)
{
	SetContainerInfo (IDR_MESSAGE);
}

CTaskDocTemplate::~CTaskDocTemplate ()
{
}

void CTaskDocTemplate::LoadTemplate()
{
	HINSTANCE hInst = ::AfxGetResourceHandle ();
	UINT nMenuID = IDR_TASK;

#ifdef _DEBUG
	nMenuID = IDR_TASKDEBUG;
#endif _DEBUG

	if (m_hMenuShared) {
		::DestroyMenu (m_hMenuShared);
		m_hMenuShared = NULL;
	}

	VERIFY (m_hMenuShared = ::LoadMenu (hInst, MAKEINTRESOURCE (nMenuID)));
}

CDocument * CTaskDocTemplate::CreateNewDocument ()
{
	CDocument * p = CBaseDocTemplate::CreateNewDocument ();
	ASSERT (p);
	return p;
}

CFrameWnd * CTaskDocTemplate::CreateNewFrame (CDocument * pDoc, CFrameWnd * pOther) 
{
	CFrameWnd * pResult = CBaseDocTemplate::CreateNewFrame (pDoc, pOther);

	if (pResult && pOther) {
		if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, pResult->GetActiveDocument ())) {
			pResult->InitialUpdateFrame (pDoc, TRUE);

			CTaskMembers & m = * (CTaskMembers *)(LPVOID)&pDoc->GetMembers ();
			CTaskView * pView = (CTaskView *)pDoc->GetView ();
			Box::CBoxParams & params = pView->GetParams ();

			pDoc->m_box.Invalidate (-1);
		}
	}

	return pResult;
}

bool CTaskDocTemplate::ParseTaskFromPath (const CString & strPath, CString & strLine, CString & strTask)
{
	strLine = GetFileHead (strPath);
	strTask = GetFileTitle (strPath);

	if (FindNoCase (strPath, FoxjetFile::GetRootPath ()) != -1) {
		CLongArray v;

		for (int i = 0; i < strPath.GetLength (); i++)
			if (strPath [i] == '\\')
				v.Add (i);

		if (v.GetSize () > 2) {
			int n1 = v [v.GetSize () - 2];
			int n2 = v [v.GetSize () - 1];
			
			strLine = strPath.Mid (n1 + 1, (n2 - n1) - 1);
			strTask = strPath.Mid (n2 + 1);

			int n3 = strTask.ReverseFind ('.');

			if (n3 != -1)
				strTask.Delete (n3, strTask.GetLength () - n3);

//			strLine = v [v.GetSize () - 2];
//			strTask = GetFileTitle (v [v.GetSize () - 1]);

			return true;
		}
	}

	return strLine.GetLength () && strTask.GetLength ();
}

CDocument * CTaskDocTemplate::OpenDocumentFile(LPCTSTR lpszPathName, BOOL bMakeVisible)
{
	CString strPath (lpszPathName);
	COdbcDatabase & db = theApp.GetDB ();
	ULONG lPrinterID = GetPrinterID (db);
	ULONG lMasterID = GetMasterPrinterID (db); 

	if (lpszPathName) {
		const CString strName = GetRelativeName (lpszPathName);
		CString strLine, strTask;
		CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

		ParseTaskFromPath (strPath, strLine, strTask);

		ULONG lLineID = GetLineID (db, strLine, lPrinterID);
		GetLineHeads (db, lLineID, vHeads);

		if (!FoxjetCommon::IsValidFileTitle (strTask)) {
			CString str;
			
			str.Format (LoadString (IDS_INVALIDTASKNAME), strTask);
			MsgBox (str, MB_ICONINFORMATION);
			return NULL;
		}
		else if (GetTaskID (database, strTask, strLine, lMasterID) == NOTFOUND) {
			CString str;
			
			str.Format (LoadString (IDS_TASKNOTFOUND), strName);
			MsgBox (str, MB_ICONINFORMATION);
			return NULL;
		}
		else if (vHeads.GetSize () == 0) {
			CString str;
			
			str.Format (LoadString (IDS_LINEHASNOHEADS), strName, strLine);
			MsgBox (str, MB_ICONINFORMATION);
			return NULL;
		}
	}

	CDocument * p = CBaseDocTemplate::OpenDocumentFile (strPath, bMakeVisible);

	if (p) {
		if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, p)) {
			if (!lpszPathName) {
				ULONG lLineID = GetLineID (m_strPrefix);
				CTaskView * pView = (CTaskView *)pDoc->GetView ();

				ASSERT (pView);

				if (!GetLineRecord (lLineID, m_line)) {
					bool bInit = GetFirstLineRecord (m_line);
					ASSERT (bInit);
				}

				pDoc->SetLine (m_line);
				pDoc->SetBox (m_box);
				pDoc->m_box.Create (pView->m_3d);
				pDoc->m_box.LoadPanels (db, m_line.m_lID);

				pDoc->m_task.m_strDesc = m_strDesc;
				bool bCreate = pDoc->m_task.Create (pDoc->m_box, m_line, database);

				pDoc->UpdateAllViews (pView);
				theApp.GetFontBar ().ResetContent (pView->GetActiveHead ());
			}
		}

		if (p->IsModified ()) {
			CString str = p->GetTitle ();

			if (str.Find (FoxjetFile::GetModifiedSuffix ()) == -1)
				p->SetTitle (str + FoxjetFile::GetModifiedSuffix ());
		}
	}

	return p;
}

bool CTaskDocTemplate::IsExistingDoc (const CString & str) const
{
	const CString strTitle = FoxjetFile::GetRelativeName (str);
	const CString strFile = FoxjetFile::GetFileTitle (strTitle);
	ULONG lPrinterID = GetMasterPrinterID (theApp.GetDB ()); //GetPrinterID (db);

	if (CBaseDocTemplate::IsExistingDoc (str)) 
		return true;

	const CString strLine = FoxjetFile::GetFileHead (strTitle);
	TASKSTRUCT task;

	return FoxjetDatabase::GetTaskRecord (database, strFile, strLine, lPrinterID, task);
}

CString CTaskDocTemplate::GetDefPrefix () const
{
	CString str;
	BOOL bGetDocString = CMultiDocTemplate::GetDocString (str, 
		CMultiDocTemplate::docName);
	CString strPrefix = FoxjetFile::GetFileHead (str);
	COdbcDatabase & db = EditorGlobals::theApp.GetDB ();

	ASSERT (bGetDocString);

	// if the head doesn't exist, grab the first one that does
	if (FoxjetCommon::GetLineID (strPrefix) == NOTFOUND) {
		LINESTRUCT line;

		FoxjetCommon::GetFirstLineRecord (line);
		strPrefix = line.m_strName;
	}

	return strPrefix;
}
