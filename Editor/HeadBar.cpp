// HeadBar.cpp : implementation file
//

#include "stdafx.h"
#include "mkdraw.h"
#include "HeadBar.h"
#include "Debug.h"
#include "Parse.h"
#include "Extern.h"
#include "HeadView.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ID_RESETCONTENT 100

using namespace EditorGlobals;
using namespace Foxjet3d;
using namespace EditorGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDocument;

using namespace Panel;
using namespace HeadView;
using namespace TaskDoc;
using namespace TaskView;

/////////////////////////////////////////////////////////////////////////////
// CHeadBar dialog

CHeadBar::CHeadBar()
:	m_bSetSelection (true),
	m_nPanel (-1),
	m_lHeadID (-1),
	CDialogBar()
{
	//{{AFX_DATA_INIT(CHeadBar)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


CHeadBar::~CHeadBar()
{
}

void CHeadBar::DoDataExchange(CDataExchange* pDX)
{
	CDialogBar::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHeadBar)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHeadBar, CDialogBar)
	//{{AFX_MSG_MAP(CHeadBar)
	ON_WM_DRAWITEM()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_COMMAND(BTN_CLOSE, OnClose)
	ON_UPDATE_COMMAND_UI(BTN_CLOSE, OnUpdateClose)
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE(CB_HEAD, OnSelchangeHead)
	ON_UPDATE_COMMAND_UI(CB_HEAD, OnUpdateHead)
	ON_WM_TIMER ()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHeadBar message handlers

void CHeadBar::Free ()
{
	if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_HEAD)) {
		pCB->SetRedraw (FALSE);

		for (int i = 0; i < pCB->GetCount (); i++) {
			if (HeadView::CTreeItem * p = (HeadView::CTreeItem *)pCB->GetItemData (i)) {
				//if (p->m_type == HeadView::HEAD)		TRACEF (p->m_pHead->m_card.m_strName);
				//else if (p->m_type == HeadView::PANEL)	TRACEF (p->m_pPanel->m_strName);
				//else if (p->m_type == CHeadView::CItem::BOX)	TRACEF (p->m_pBox->m_strName);

				delete p;
			}
		}

		pCB->ResetContent ();
		pCB->SetRedraw (TRUE);
	}
}

//static ULONG CALLBACK TryAgainFunc (LPVOID lpData)
//{
//	ASSERT (lpData);
//	CHeadBar & bar = * (CHeadBar *)lpData;
//	DWORD dw = 250;
//
//	for (int i = 0; i < 10; i++) {
//		if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) {
//			if (CTaskView * pView = (CTaskView *)pDoc->GetView ()) {
//				TRACEF (_T ("bar.ResetContent"));
//				bar.ResetContent ();
//				return 0;
//			}
//		}
//
//		TRACEF (_T ("Sleep: ") + ToString (dw));
//		::Sleep (dw);
//	}
//
//	return 0;
//}

void CHeadBar::ResetContent (CDocument * pDocument)
{
	bool bSucceeded = false;

	Free ();

	if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_HEAD)) {
		pCB->SetRedraw (FALSE);

		if (!pDocument)
			pDocument = theApp.GetActiveDocument ();

		if (pDocument) {
			if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, pDocument)) {
				CTaskView * pView = (CTaskView *)pDoc->GetView ();

				if (!pView)
					pView = DYNAMIC_DOWNCAST (CTaskView, theApp.GetActiveView ());

				if (pView) {
					const CPanel * pPanels [6] = { NULL };
					const bool bValve = ISVALVE ();
					Box::CBoxParams & params = pView->GetParams ();

					if (pDoc->m_box.GetPanels (pPanels)) {
						ULONG lSelHead = params.m_lHeadID;
						UINT nSelPanel = params.m_nPanel;
											
						for (int nPanel = 0; nPanel < 6; nPanel++) {
							ASSERT (pPanels [nPanel]);
							const CPanel & panel = * pPanels [nPanel];

								pCB->SetItemData (pCB->AddString (panel.GetMembers ().m_strName), (DWORD)new HeadView::CTreeItem (panel.GetMembers ()));

							for (int nCard = 0; nCard < panel.m_vBounds.GetSize (); nCard++) {
								const CBoundary & head = panel.m_vBounds [nCard];

								pCB->SetItemData (pCB->AddString (head.GetName ()), (DWORD)new HeadView::CTreeItem (head));
							}
						}

						for (int i = 0; (i < pCB->GetCount ()) && (pCB->GetCurSel () == CB_ERR); i++) 
							if (HeadView::CTreeItem * p = (HeadView::CTreeItem *)pCB->GetItemData (i)) 
								if (p->m_type == HeadView::HEAD && p->m_pHead->GetID () == params.m_lHeadID)
									pCB->SetCurSel (i);

						for (int i = 0; (i < pCB->GetCount ()) && (pCB->GetCurSel () == CB_ERR); i++) 
							if (HeadView::CTreeItem * p = (HeadView::CTreeItem *)pCB->GetItemData (i)) 
								if (p->m_type == HeadView::PANEL && p->m_pPanel->m_nNumber == params.m_nPanel)
									pCB->SetCurSel (i);

						bSucceeded = true;
					}
					else
						TRACEF (_T ("!GetPanels"));
				}
				else
					TRACEF (_T ("!GetView ()"));
			}
			else
				TRACEF (_T ("DYNAMIC_DOWNCAST failed"));
		}
		else
			TRACEF (_T ("!pDocument"));

		pCB->SetRedraw (TRUE);
	}

	//if (!bSucceeded) {
	//	DWORD dw = 0;

	//	::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)TryAgainFunc, (LPVOID)this, 0, &dw);
	//}
}


void CHeadBar::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if (nIDCtl == CB_HEAD) {
		const bool bValve = ISVALVE ();
		DRAWITEMSTRUCT & dis = * lpDrawItemStruct;
		CDC dc;
		CRect rc = dis.rcItem;

		dc.Attach (dis.hDC);

		if (dis.itemID != CB_ERR) {
			CDC dcMem;

			VERIFY (dcMem.CreateCompatibleDC (&dc));

			COLORREF crOldTextColor = dc.GetTextColor();
			COLORREF crOldBkColor = dc.GetBkColor();

			if ((dis.itemAction | ODA_SELECT) && (dis.itemState & ODS_SELECTED)) {
				dc.SetTextColor (::GetSysColor (COLOR_HIGHLIGHTTEXT));
				dc.SetBkColor (::GetSysColor (COLOR_HIGHLIGHT));
				dc.FillSolidRect (rc, ::GetSysColor (COLOR_HIGHLIGHT));
			}
			else
				dc.FillSolidRect (rc, crOldBkColor);

			if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_HEAD)) {
				if (HeadView::CTreeItem * p = (HeadView::CTreeItem *)pCB->GetItemData (dis.itemID)) {
					CString str;
					CxImage * pImage = NULL;

					switch (p->m_type) {
					case HeadView::HEAD:	
						str = p->m_pHead->m_card.m_strName;		
						pImage = p->m_pHead->m_card.m_bMasterHead ? &m_imgHeadMaster : &m_imgHead;

						if (bValve)
							pImage = &m_imgHead;

						if (!p->m_pHead->m_card.m_bEnabled)
							pImage = &m_imgHeadDisabled;

						if (pCB->GetDroppedState ())
							rc.left += (m_imgHead.GetWidth () + 1);

						break;
					case HeadView::PANEL:	
						str = p->m_pPanel->m_strName;			
						pImage = &m_imgPanel;
						break;
					}

					if (pImage) {
						CxBitmap bmp (pImage->MakeBitmap (dc));
						HGDIOBJ hSelectObject = ::SelectObject (dcMem, bmp);
						dc.BitBlt (rc.left, rc.top, pImage->GetWidth (), pImage->GetHeight (), &dcMem, 0, 0, SRCCOPY);
						::SelectObject (dcMem, hSelectObject);
						rc.left += pImage->GetWidth () + 1;
					}

					dc.DrawText (str, rc, DT_VCENTER | DT_SINGLELINE);
				}
			}


			if (dis.itemAction & ODS_FOCUS) 
				dc.DrawFocusRect (rc);

			dc.SetTextColor(crOldTextColor);
			dc.SetBkColor(crOldBkColor);
		}

		dc.Detach ();
	}
	else
		CDialogBar::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

BOOL CHeadBar::Create (CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle, UINT nID)
{
	HINSTANCE hInst = ::AfxGetInstanceHandle ();
	HICON hPanel		= NULL;
	HICON hTask			= NULL;
	HICON hHead			= NULL;
	HICON hHeadMaster	= NULL;
	HICON hHeadDisabled	= NULL;

	BOOL bResult = CDialogBar::Create (pParentWnd, nIDTemplate, nStyle, nID);

	VERIFY (hPanel			= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_PANEL),			IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
	VERIFY (hTask			= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDR_TASK),			IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
	VERIFY (hHead			= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_HEAD),			IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
	VERIFY (hHeadMaster		= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_HEADMASTER),		IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
	VERIFY (hHeadDisabled	= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_HEADDISABLED),	IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));

	VERIFY (m_imgPanel.CreateFromHICON			(hPanel));
	VERIFY (m_imgTask.CreateFromHICON			(hTask));
	VERIFY (m_imgHead.CreateFromHICON			(hHead));
	VERIFY (m_imgHeadMaster.CreateFromHICON		(hHeadMaster));
	VERIFY (m_imgHeadDisabled.CreateFromHICON	(hHeadDisabled));

	VERIFY (m_bmpClose.LoadBitmap (IDB_CLOSE));

	::DestroyIcon (hPanel);
	::DestroyIcon (hTask);
	::DestroyIcon (hHead);
	::DestroyIcon (hHeadMaster);
	::DestroyIcon (hHeadDisabled);

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_HEAD)) {
		p->SetItemHeight (-1, m_imgPanel.GetHeight ());
		p->SetItemHeight (0, m_imgPanel.GetHeight ());
		p->ResetContent ();
	}

	if (CButton * p = (CButton *)GetDlgItem (BTN_CLOSE))
		p->SetBitmap (m_bmpClose);

	return bResult;
}

void CHeadBar::OnSize(UINT nType, int cx, int cy) 
{
	CDialogBar::OnSize(nType, cx, cy);
}

void CHeadBar::OnDestroy() 
{
	Free ();
	CDialogBar::OnDestroy();
}

void CHeadBar::OnSelchangeHead ()
{
	if (m_bSetSelection) {
		if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_HEAD)) {
			int nIndex = pCB->GetCurSel ();

			if (nIndex != CB_ERR) {
				if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) {
					if (CTaskView * pView = (CTaskView *)pDoc->GetView ()) {
						if (HeadView::CTreeItem * p = (HeadView::CTreeItem *)pCB->GetItemData (nIndex)) {
							if (p->m_type == HeadView::HEAD) {
								ULONG lHeadID = p->m_pHead->GetID ();

								if (CHeadView * pHead = (CHeadView *)pDoc->GetView (TaskDoc::HEAD)) 
									VERIFY (pHead->SetSelection (HeadView::CItem (lHeadID)));
							}
							else if (p->m_type == HeadView::PANEL) {
								UINT nPanel = p->m_pPanel->m_nNumber;

								if (CHeadView * pHead = (CHeadView *)pDoc->GetView (TaskDoc::HEAD)) 
									VERIFY (pHead->SetSelection (HeadView::CItem (nPanel)));
							}
						}
					}
				}
			}
		}
	}
}

void CHeadBar::SetActiveHead (ULONG lHeadID)
{
	m_lHeadID = lHeadID;
	SetTimer (ID_RESETCONTENT, 500, NULL);
}

void CHeadBar::SetActivePanel (UINT nNumber)
{
	m_nPanel = nNumber;
	SetTimer (ID_RESETCONTENT, 500, NULL);
}

void CHeadBar::OnUpdateHead (CCmdUI * pCmdUI)
{
	bool bEnable = theApp.GetActiveDocument () != NULL;

	if (!bEnable) 
		if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_HEAD)) 
			if (pCB->GetCount () > 0)
				Free ();

	pCmdUI->Enable (bEnable);
}


void CHeadBar::OnClose() 
{
	if (CMainFrame * pMain = DYNAMIC_DOWNCAST (CMainFrame, theApp.m_pMainWnd)) 
		pMain->PostMessage (WM_CLOSE, 0, 0);
}

void CHeadBar::OnUpdateClose(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable ();
}

void CHeadBar::OnTimer(UINT nIDEvent)
{
	if (nIDEvent == ID_RESETCONTENT) {
		bool bSet = false;

		if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_HEAD)) {
			for (int i = 0; i < pCB->GetCount (); i++) {
				if (HeadView::CTreeItem * p = (HeadView::CTreeItem *)pCB->GetItemData (i)) {
					if (p->m_type == HeadView::PANEL) {
						if (p->m_pPanel->m_nNumber == m_nPanel) {
							m_bSetSelection = false;
							pCB->SetCurSel (i);
							bSet = m_bSetSelection = true;
						}
					}
				}
			}
		}

		if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_HEAD)) {
			for (int i = 0; i < pCB->GetCount (); i++) {
				if (HeadView::CTreeItem * p = (HeadView::CTreeItem *)pCB->GetItemData (i)) {
					if (p->m_type == HeadView::HEAD) { 
						if (p->m_pHead->GetID () == m_lHeadID) {
							m_bSetSelection = false;
							pCB->SetCurSel (i);
							bSet = m_bSetSelection = true;
						}
					}
				}
			}
		}

		if (bSet) 
			KillTimer (nIDEvent);
		else {
			CDocument * pDoc = theApp.GetActiveDocument ();

			if (pDoc)
				ResetContent (pDoc);
			else 
				KillTimer (nIDEvent);
		}
	}
}
