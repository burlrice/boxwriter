// MkDrawDocManager.cpp: implementation of the CMkDrawDocManager class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "mkdraw.h"
#include "MkDrawDocManager.h"
#include "NewDocTypeDlg.h"

//#include "MessageDoc.h"
//#include "MessageView.h"
#include "MessageFrame.h"
#include "NewMessageDocType.h"

#include "TaskDoc.h"
#include "TaskView.h"
#include "TaskFrame.h"
#include "NewTaskDocType.h"

#include "Debug.h"
#include "Database.h"
#include "Extern.h"
#include "OpenDlg.h"
#include "Registry.h"
#include "Extern.h"
#include "Database.h"
#include "Parse.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetDocument;
using namespace EditorGlobals;
using namespace TaskDoc;

IMPLEMENT_DYNAMIC (CNewDocType, CObject)

CNewDocType::CNewDocType (FoxjetFile::DOCTYPE type)
:	m_pDocTemplate (NULL), 
	m_hIcon (NULL),
	m_type (type)
{
}

CNewDocType::~CNewDocType ()
{
	if (!m_pDocTemplate->m_bAutoDelete)
		delete m_pDocTemplate;

	if (m_hIcon)
		::DestroyIcon (m_hIcon);
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC (CMkDrawDocManager, CDocManager);

CMkDrawDocManager::CMkDrawDocManager()
:	CDocManager ()
{
	m_vTemplates.Add (new CNewTaskDocType);
// TODO //	m_vTemplates.Add (new CNewMessageDocType);

	for (int i = 0; i < m_vTemplates.GetSize (); i++)
		AddDocTemplate (m_vTemplates [i]->m_pDocTemplate);
}

CMkDrawDocManager::~CMkDrawDocManager()
{
	for (int i = 0; i < m_vTemplates.GetSize (); i++) 
		delete (CNewDocType * )m_vTemplates [i];

	m_vTemplates.RemoveAll ();
}

BOOL CMkDrawDocManager::DoPromptFileName (CString& fileName, 
										  UINT nIDSTitle, DWORD lFlags, 
										  BOOL bOpenFileDialog, 
										  CDocTemplate* pTemplate)
{
	const CString strPrefix = FoxjetFile::GetFileHead (fileName);
	const CString strTitle = FoxjetFile::GetFileTitle (fileName);
	FoxjetFile::DOCTYPE type = FoxjetFile::GetFileType (fileName);

	// DoPromptFileName should never be called to open a file because
	// OnFileOpen is overridden to handle this case
	// Should only get called for FileSaveAs
	ASSERT (!bOpenFileDialog);

	for (POSITION pos = pTemplate->GetFirstDocPosition (); pos != NULL; ) {
		CBaseDoc * pDoc = DYNAMIC_DOWNCAST (CBaseDoc, pTemplate->GetNextDoc (pos));
		ASSERT (pDoc);
		const CString strPath = pDoc->GetPathName ();
		const CString strCmpPrefix = FoxjetFile::GetFileHead (strPath);
		const CString strCmpTitle = FoxjetFile::GetFileTitle (strPath);

		if (!pDoc->PromptSaveModified ()) {
			// fileName assumed to be already set, no need to prompt
			return TRUE;
		}

		if (pDoc->GetReadOnly ()) {
			CString str;

			str.Format (LoadString (IDS_READONLYFILE), pDoc->GetTitle ());
			MsgBox (::AfxGetMainWnd ()->m_hWnd, str, LoadString (IDS_ERROR), MB_OK);
			return FALSE;
		}

		switch (type) {
/* TODO
		case FoxjetFile::MESSAGE:
			ASSERT (pDoc->IsKindOf (RUNTIME_CLASS (CMessageDoc)));
			break;
*/
		case FoxjetFile::TASK:
			ASSERT (pDoc->IsKindOf (RUNTIME_CLASS (CTaskDoc)));
			break;
		default:
			ASSERT (0); 
			break;
		}

		if (!strCmpPrefix.CompareNoCase (strPrefix) &&
			!strCmpTitle.CompareNoCase (strTitle))
		{
			Foxjet3d::CSaveDlg dlg ((FoxjetDocument::CBaseDocTemplate *)pTemplate, ::AfxGetMainWnd ());
			
			dlg.m_type = type;
			dlg.m_vFiles.Add (fileName);

			if (dlg.DoModal () == IDOK) {
				REPORTSTRUCT r;
				const CString strRoot = FoxjetFile::GetRootPath ();
				const CString strOldFile = fileName;
				const CString strLine = FoxjetFile::GetFileHead (strOldFile);

				ASSERT (dlg.m_vFiles.GetSize ());
				fileName = dlg.m_vFiles [0];

				TRACEF (dlg.m_ver.Format (true)); 
				pDoc->SetVersion (dlg.m_ver);
				pDoc->SetDesc (dlg.m_strDesc);

				if (fileName.Find (strRoot, 0) == -1)
					fileName = strRoot + fileName;

				r.m_dtTime = COleDateTime::GetCurrentTime ();
				r.m_strTaskName = FoxjetFile::GetFileTitle (fileName);
				r.m_strAction = FoxjetFile::GetFileTitle (strOldFile) +
					_T (" --> ") + r.m_strTaskName;
				r.m_strLine = strLine;
				r.m_strUsername = theApp.GetUsername ();

				VERIFY (AddReportRecord (theApp.GetDB (), REPORT_TASK_MODIFY, r, GetPrinterID (theApp.GetDB ())));

				return TRUE;
			}
		}
	}

	return FALSE;
}

void CMkDrawDocManager::OnFileOpen()
{
	using namespace Foxjet3d;

	OpenDlg::COpenDlg dlg (::AfxGetMainWnd ());
	CWinApp * pApp = ::AfxGetApp ();
	const CString strSection = _T ("Settings\\LastOpen");
	BOOL bReadOnly = FALSE;// pApp->GetProfileInt (strSection, _T ("bReadOnly"), FALSE);
	BOOL bPreview = pApp->GetProfileInt (strSection, _T ("bPreview"), FALSE);
	FoxjetFile::DOCTYPE type = (FoxjetFile::DOCTYPE)pApp->GetProfileInt (strSection, _T ("type"), (int)FoxjetFile::MESSAGE);
	int nPanel = pApp->GetProfileInt (strSection, _T ("nPanel"), 1);
	CStringArray vFailed;

	ASSERT (pApp);
	dlg.m_type = type;
	dlg.m_bReadOnly = bReadOnly;
	dlg.m_bPreview = bPreview;
	dlg.m_nPanel = nPanel - 1;
	dlg.m_lLineID = pApp->GetProfileInt (strSection, _T ("LineID"), -1);

	if (dlg.DoModal () == IDOK) {
		int nFiles = dlg.m_vFiles.GetSize ();
		const CString strMsg = LoadString (IDS_OPENING);

		theApp.BeginProgressBar (strMsg, nFiles);

		for (int i = 0; i < nFiles; i++) {
			const CString strFile = FoxjetFile::GetRootPath () + dlg.m_vFiles [i];

			theApp.StepProgressBar (strMsg + _T (": ") + strFile + _T ("..."));

			if (OpenDocumentFile (strFile, dlg.m_bReadOnly ? true : false)) {
				pApp->WriteProfileInt (strSection, _T ("type"), dlg.m_type);
				pApp->WriteProfileInt (strSection, _T ("bReadOnly"), dlg.m_bReadOnly);
				pApp->WriteProfileInt (strSection, _T ("bPreview"), dlg.m_bPreview);
				pApp->WriteProfileInt (strSection, _T ("nPanel"), dlg.m_nPanel + 1);
				pApp->WriteProfileString (strSection, _T ("Filename"), strFile);
				pApp->WriteProfileInt (strSection, _T ("LineID"), dlg.m_lLineID);
			}
			else 
				vFailed.Add (strFile);
		}

		theApp.EndProgressBar ();

		if (vFailed.GetSize ()) {
			CString str = LoadString (IDS_FAILEDTOOPEN);

			for (int i = 0; i < vFailed.GetSize (); i++)
				str += _T ("\n") + vFailed [i];

			MsgBox (::AfxGetMainWnd ()->m_hWnd, str, LoadString (IDS_ERROR), MB_ICONERROR);
		}
	}
}

CDocument * CMkDrawDocManager::OpenDocumentFile(LPCTSTR lpszFilename, bool bReadOnly)
{
	CDocument * p = CDocManager::OpenDocumentFile (lpszFilename);

	if (p && p->IsKindOf (RUNTIME_CLASS (CBaseDoc))) {
		CBaseDoc * pDoc = (CBaseDoc *)p;
		pDoc->SetReadOnly (bReadOnly);
	}

	return p;
}

void CMkDrawDocManager::OnFileNew()
{
	CWnd * pMain = ::AfxGetMainWnd ();
	CNewDocType * pTemplate = NULL;
	CDocument * pDocExisting = NULL;
	bool bPrompted = false;

	ASSERT (m_vTemplates.GetSize ());

	if (pMain->IsWindowVisible ()) {
		CWinApp * pApp = ::AfxGetApp ();
		CArray <CNewDocType *, CNewDocType *> vTemplates;
		
		vTemplates.Append (m_vTemplates);

		try
		{
			using namespace FoxjetDatabase;

			CArray <LINESTRUCT, LINESTRUCT &> vLines;
			GetPrinterLines (theApp.GetDB (), GetPrinterID (theApp.GetDB ()), vLines); //GetLineRecords (theApp.GetDB (), vLines);

			for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
				LINESTRUCT & line = vLines [nLine];
				CStringArray v;
				SETTINGSSTRUCT s;

				GetSettingsRecord (theApp.GetDB (), line.m_lID, _T ("Templates"), s);
				FromString (s.m_strData, v);

				for (int i = 0; i < v.GetSize (); i++) {
					CNewDocType * p = new CNewTaskDocType ();
					p->m_pDocTemplate->UpdateDocStrings (line.m_strName, v [i]);

					if (CTaskDocTemplate * pTask = DYNAMIC_DOWNCAST (CTaskDocTemplate, p->m_pDocTemplate)) {
						TASKSTRUCT task;

						pTask->m_strTemplate = pTask->m_strTitle;
						
						if (!GetTaskRecord (theApp.GetDB (), pTask->m_strTitle, line.m_lID, task)) {
							delete p;
							continue;
						}

						GetBoxRecord (theApp.GetDB (), task.m_lBoxID, pTask->m_box);
					}

					vTemplates.Add (p);
				}
			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }

		CNewDocTypeDlg dlg (vTemplates, pMain);
		const CString strSection = _T ("Settings\\LastOpen");
		const CString strDoctype = _T ("type");
		
		ASSERT (pApp);
		dlg.m_type = (FoxjetFile::DOCTYPE)pApp->GetProfileInt (strSection, strDoctype, 
			FoxjetFile::MESSAGE);

		if (dlg.DoModal() == IDOK) {
			pApp->WriteProfileInt (strSection, strDoctype, dlg.m_type);
			pTemplate = dlg.m_pSelectedTemplate;
			ASSERT (pTemplate);
			pDocExisting = dlg.m_pDocExisting;
			TRACEF ("pTemplate->m_strDesc = " + pTemplate->m_strDesc);
			bPrompted = true;
		}
	}
	else if (m_vTemplates.GetSize ()) 
		pTemplate = m_vTemplates [0];

	if (pTemplate) {
		CDocument * pDoc = pDocExisting;
		const CString strPath = FoxjetFile::GetRootPath ();
		CString strTitle = pTemplate->m_strLibrary 
			+ _T ("\\") + pTemplate->m_strTitle 
			+ _T (".") + FoxjetFile::GetFileExt (pTemplate->m_type);

		if (!bPrompted) 
			strTitle = pTemplate->m_pDocTemplate->GetNextDefaultName ();

		// user could have chosen to delete the contents of an existing file
		if (!pDoc) {
			const CString strDoc = strPath + strTitle;

			if (pTemplate->m_type == FoxjetFile::TASK) {
				using namespace FoxjetDatabase;

				CString strLine, strTask;
				CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
				COdbcDatabase & db = theApp.GetDB ();

				CTaskDocTemplate::ParseTaskFromPath (strDoc, strLine, strTask);

				ULONG lLineID = FoxjetDatabase::GetLineID (db, strLine, GetPrinterID (db));
				GetLineHeads (db, lLineID, vHeads);

				if (vHeads.GetSize () == 0) {
					CString str;
					
					str.Format (LoadString (IDS_CREATETASKNOHEADS), strTitle, strLine);
					MsgBox (* pMain, str, MB_ICONINFORMATION);
					
					return;
				}
			}

			if (pDoc = pTemplate->m_pDocTemplate->OpenDocumentFile (NULL)) {
				// note that we must be in "prompted" mode (ie, the main
				// window is visible) for SetPathName to update the window titles

				if (CTaskDocTemplate * pTaskTemplate = DYNAMIC_DOWNCAST (CTaskDocTemplate, pTemplate->m_pDocTemplate)) {
					if (pTaskTemplate->m_strTemplate.GetLength ()) {
						TRACEF (pTaskTemplate->m_line.m_strName);
						TRACEF (pTaskTemplate->m_strTemplate);

						if (CTaskDoc * pTaskDoc = DYNAMIC_DOWNCAST (CTaskDoc, pDoc)) {
							pTaskDoc->LoadFromFile (pTaskTemplate->m_line.m_strName + _T ("\\") + pTaskTemplate->m_strTemplate, pTaskDoc->GetVersion ());
							pTaskDoc->SetTemplate (false);
						}
					}
				}

				theApp.AddToRecentFileList (strDoc);
				
				if (CBaseDoc * pBaseDoc = DYNAMIC_DOWNCAST (CBaseDoc, pDoc)) {
					pBaseDoc->SetPathName (strDoc, FALSE);
					pBaseDoc->SetModifiedFlag (NULL);

					if (m_vTemplates.GetSize ()) {
						if (CBaseDocTemplate * p = m_vTemplates [0]->m_pDocTemplate) {
							p->UpdateDocument (pDoc);
							pBaseDoc->UpdateTemplate (p);
						}
					}
				}

				TRACEF ("pDoc->GetPathName () = " + pDoc->GetPathName ());
				TRACEF ("pDoc->GetTitle ()    = " + pDoc->GetTitle ());
			}
		}
	}
}

CString CMkDrawDocManager::GetFilePath (const CNewDocType & type)
{
	CString str;

	if (type.m_pDocTemplate) 
		type.m_pDocTemplate->GetDocString (str, CDocTemplate::docName);

	return str;
}

CString CMkDrawDocManager::GetFileTitle (const CNewDocType & type)
{
	CString str;

	if (type.m_pDocTemplate) 
		type.m_pDocTemplate->GetDocString (str, CDocTemplate::docName);

	return FoxjetFile::GetFileTitle (str);
}

CString CMkDrawDocManager::GetFileLibrary (const CNewDocType & type)
{
	CString str;

	if (type.m_pDocTemplate) 
		type.m_pDocTemplate->GetDocString (str, CDocTemplate::docName);

	return FoxjetFile::GetFileHead (str);
}

CString CMkDrawDocManager::GetFileType (const CNewDocType & type)
{
	CString str;

	if (type.m_pDocTemplate) 
		type.m_pDocTemplate->GetDocString (str, CDocTemplate::regFileTypeName);

	return str;
}

CNewDocType * CMkDrawDocManager::GetFileType (FoxjetFile::DOCTYPE type)
{
	for (int i = 0; i < m_vTemplates.GetSize (); i++)
		if (m_vTemplates [i]->m_type == type)
			return m_vTemplates [i];

	return NULL;
}

