#if !defined(AFX_NEWDOCTYPEDLG_H__619B204B_D293_440D_88DE_63071A278328__INCLUDED_)
#define AFX_NEWDOCTYPEDLG_H__619B204B_D293_440D_88DE_63071A278328__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewDocTypeDlg.h : header file
//

#include "MkDrawDocManager.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CNewDocTypeDlg dialog

class CNewDocTypeDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	ULONG GetSelectedLine () const;
	CNewDocTypeDlg (const CArray <CNewDocType *, CNewDocType *> & vTemplates, 
		CWnd* pParent = NULL);
	virtual ~CNewDocTypeDlg ();

	CNewDocType * m_pSelectedTemplate;
	CDocument * m_pDocExisting;

// Dialog Data
	//{{AFX_DATA(CNewDocTypeDlg)
	//}}AFX_DATA

	FoxjetFile::DOCTYPE	m_type;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewDocTypeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();

	CImageList m_ilMsg;
	CImageList m_ilTask;
	CArray <CNewDocType *, CNewDocType *> m_vTemplates;
	CArray <FoxjetDatabase::BOXSTRUCT, FoxjetDatabase::BOXSTRUCT &> m_vBoxes;

	// Generated message map functions
	//{{AFX_MSG(CNewDocTypeDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeLine();
	afx_msg void OnSelchangeBox();
	//}}AFX_MSG
	
	afx_msg void OnSelchangeDoctype(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedDoctypes(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkDoctypes(NMHDR* pNMHDR, LRESULT* pResult);
	CNewDocType * ValidateMessage ();
	CNewDocType * ValidateTask ();

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWDOCTYPEDLG_H__619B204B_D293_440D_88DE_63071A278328__INCLUDED_)
