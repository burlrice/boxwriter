#if !defined(AFX_TASKDOC_H__0DCAA080_A2F8_46E3_BFAE_41C20E3C4EAD__INCLUDED_)
#define AFX_TASKDOC_H__0DCAA080_A2F8_46E3_BFAE_41C20E3C4EAD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TaskDoc.h : header file
//

#include "BaseDoc.h"
#include "Task.h"
#include "Database.h"
#include "Image.h"
#include "Box.h"
#include "UiStruct.h"
#include "Element.h"

class CTaskMembers : public virtual FoxjetDocument::CBaseMembers
{
protected:
	CTaskMembers ();
	CTaskMembers (const CTaskMembers & rhs);
	CTaskMembers & operator = (const CTaskMembers & rhs);
	virtual ~CTaskMembers ();


public:
	Foxjet3d::CTask			m_task;
	Foxjet3d::Box::CBox		m_box;
};

bool operator == (const CTaskMembers & lhs, const CTaskMembers & rhs);
bool operator != (const CTaskMembers & lhs, const CTaskMembers & rhs);

/////////////////////////////////////////////////////////////////////////////
// CTaskDoc document

namespace TaskView
{
	class CTaskView;
};

namespace TaskDoc
{
	class CTaskDoc;

	typedef enum INVALIDATETYPE 
	{	
		INVALIDATERECT		= 0x0001,
		CACHE_ERASE			= 0x0002, 
		CACHE_REDRAW		= 0x0004,
	};

	typedef enum { TASK, HEAD } VIEWTYPE;

	typedef struct 
	{
		CString		m_str;
		ULONG		m_lHeadID;
	} MESSAGEPAIRSTRUCT;

	typedef enum 
	{ 
		ELEMENT_NONE,
		ELEMENT_ADD, 
		ELEMENT_MODIFY, 
		ELEMENT_DELETE,
	} SNAPSHOTTYPE;

	class CSnapshot : public FoxjetDocument::CSnapshot, public CTaskMembers
	{
	public:
		CSnapshot (const CTaskMembers & rhs);
		CSnapshot & operator = (const CTaskMembers & rhs);
		virtual ~CSnapshot ();

		bool GetMessageByHead (ULONG lHeadID, MESSAGEPAIRSTRUCT & msg) const;
		void UpdateLinkedSlaves (CTaskDoc & doc);

		CString												m_strTask;
		CString												m_strBox;
		CArray <MESSAGEPAIRSTRUCT, MESSAGEPAIRSTRUCT &>		m_vMsgs;

		void SetElementsChanged (const Element::CPair & pair, SNAPSHOTTYPE type = ELEMENT_MODIFY);
		void SetElementsChanged (const Element::CPairArray & v, SNAPSHOTTYPE type = ELEMENT_MODIFY);
		void SetPropertiesChanged (const Foxjet3d::CTask & task, const Foxjet3d::Box::CBox & box);

		const Element::CPairArray & GetElementsChanged () const;
		SNAPSHOTTYPE GetElementChangeType () const;
		bool GetPropertiesChanged () const;

	protected:
		Element::CPairArray								m_vChanged;
		SNAPSHOTTYPE										m_type;
		bool												m_bProperties;

	private:
		CSnapshot ();								// no impl
		CSnapshot (const CSnapshot & rhs);			// no impl
		CSnapshot & operator = (CSnapshot & rhs);	// no impl
	};

	namespace Hint
	{
		typedef enum
		{ 
			BOX			= (1 << (FoxjetDocument::Hint::ENUM_END + 1)), 
			PANEL		= (1 << (FoxjetDocument::Hint::ENUM_END + 3)), 
		} HINT_TYPE;
	};

	class CHint : public FoxjetDocument::CHint, protected virtual CTaskMembers
	{
		DECLARE_DYNAMIC (CHint)
	public:

		CHint (const CTaskMembers * pCurrent, const CSnapshot * pOld, const Element::CPairArray * pvChanged); 
		CHint (const CTaskMembers * pCurrent, const CSnapshot * pOld, const Element::CPair & pair); 
		virtual ~CHint ();

		DECLARE_CONSTBASEACCESS(CTaskMembers)

		bool SetBox (const FoxjetDatabase::BOXSTRUCT & box);

		bool						m_bUpdatedPanels [6]; 
		Element::CPairArray			m_vChanged;

	protected:
		ULONG SetChangedBox (const CTaskMembers * pCurrent, const CSnapshot * pOld);
		ULONG SetChangedPanel (const CTaskMembers * pCurrent, const CSnapshot * pOld);

	};

	class CTaskDoc : public FoxjetDocument::CBaseDoc, protected CTaskMembers 
	{
	public:

	protected:
		CTaskDoc();           // protected constructor used by dynamic creation

		DECLARE_DYNCREATE(CTaskDoc)

	public:
		virtual ~CTaskDoc();

		virtual FoxjetDocument::CBaseDoc & Copy (const FoxjetDocument::CSnapshot * pSnapshot,
			FoxjetDocument::CSnapshot * pNext = NULL);
		virtual FoxjetDocument::CSnapshot * GetSnapshot();
		virtual bool LoadFromFile(LPCTSTR pszName, FoxjetCommon::CVersion & ver);
		virtual bool StoreToFile(LPCTSTR pszName);
		virtual const FoxjetDatabase::HEADSTRUCT * GetSelectedHead () const;
		virtual void SetModifiedFlag (bool bModified, const FoxjetDocument::CSnapshot * pPreModify);
		virtual void SetModifiedFlag (const FoxjetDocument::CSnapshot * pPreModify);
		virtual bool SetVersion (const FoxjetCommon::CVersion & ver);
		virtual CString GetDesc () const;
		virtual bool SetDesc (const CString & str);
		virtual void SetPathName(LPCTSTR lpszPathName, BOOL bAddToMRU);
		virtual bool GetReadOnly () const;

		bool HasOverlappingElements () const;

		bool CopyChanged (const TaskDoc::CSnapshot * pSnapshot, TaskDoc::CSnapshot * pNext);
		bool CheckElementCount () const;

		const Foxjet3d::Box::CBox & GetBox () const;
		bool SetBox (const FoxjetDatabase::BOXSTRUCT & box, bool bUndoable = true);
		bool SetLine (FoxjetDatabase::LINESTRUCT & line);

		void GetMessages (CArray <Foxjet3d::CMessage *, Foxjet3d::CMessage *> & v, UINT nPanel);
		CSize GetProductArea (const CEditorElementList & list) const;
		int CountInvalidElements (Element::CPairArray * pvInvalid = NULL);
		bool CheckState (const Element::CPairArray * pvConfined = NULL);
		ULONG GetLineID () const { return m_task.m_lLineID; }

		void GetHeads (Foxjet3d::Panel::CBoundaryArray & v) const;
		bool GetHead (ULONG lHeadID, Foxjet3d::Panel::CBoundary & head) const;
		bool GetHead (ULONG lHeadID, Foxjet3d::Panel::CBoundaryArray & v, Foxjet3d::Panel::CBoundary & head) const;

		bool GetPasteToCrosshairs () const;
		void SetPasteToCrosshairs (bool bFlag);

	//	void Invalidate (Element::CPairArray & v, DWORD dwFlags = INVALIDATERECT, const TaskView::CTaskView * pCaller = NULL);	// TODO: rem
	//	CRect Invalidate (Element::CPair & pair, DWORD dwFlags = INVALIDATERECT, const TaskView::CTaskView * pCaller = NULL);	// TODO: rem
	//	void Invalidate (const CRect & rc, Element::CPair & pair, DWORD dwFlags, const TaskView::CTaskView * pCaller = NULL);	// TODO: rem
		void Invalidate (LPCRECT lpRect = NULL, const TaskView::CTaskView * pCaller = NULL);										// TODO: rem
		void Invalidate (Element::CPairArray & v, TaskView::CTaskView * pCaller = NULL);
		void Erase (Element::CPairArray & vInvalidate, TaskView::CTaskView * pCaller = NULL);

		bool IsMoveLocked () const { return m_bMoveLocked; }
		void SetMoveLock (bool bLock = true) { m_bMoveLocked = bLock; }

		bool IsRubberBand () const { return m_bRubberBand; }
		void SetRubberBand (bool bSet = true) { m_bRubberBand = bSet; }
		
		void SetTemplate (bool bSet) { m_bTemplate = bSet; };
		bool IsTemplate () const  { return m_bTemplate; }

		CRect GetBoundingRect (const Element::CPairArray & v) const;
		CRect GetBoundingRect (const CEditorElementList & v) const;

		using CTaskMembers::m_task;
		using CTaskMembers::m_box;
		bool m_bInitFromHeads;
		bool m_bPasteToCrosshairs;
	
		DECLARE_CONSTBASEACCESS(CTaskMembers)

	protected:
		bool m_bMoveLocked;
		bool m_bRubberBand;
		bool m_bTemplate;
		const CString m_strRegKey;

		bool IsTemplate (ULONG lLineID, const CString & strTask);
		bool Parse (Foxjet3d::CTask & task, const FoxjetCommon::CVersion & ver, bool bValidate);
		static void CALLBACK ProgressFct (int nType, const CString & str, int nMax);

	// Operations
	public:

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CTaskDoc)
		public:
		virtual void Serialize(CArchive& ar);   // overridden for document i/o
		virtual void DeleteContents();
		virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
		protected:
		virtual BOOL OnNewDocument();
		//}}AFX_VIRTUAL

	// Implementation
	public:

		CView * GetView (VIEWTYPE type = TASK) const;
		CView * GetView (VIEWTYPE type, int) const;
		void UpdateProductAreas ();
		void SelectAll (bool bSelect);
		CEditorElementList * GetActiveList () const;
		int CountMDIFrames () const;

	#ifdef _DEBUG
		virtual void AssertValid() const;
		virtual void Dump(CDumpContext& dc) const;
	#endif

		// Generated message map functions
	protected:
		CDC * GetDC () const;

	public:
		Element::CPairArray PasteFromString (LPCTSTR pszData, CEditorElementList & list);

		void OnFontNameChanged(LPCTSTR pszName);
		void OnFontSizeChanged(int nSize);
		void OnFontWidthChanged(int nSize);

		void UpdateAttrUIStruct(CMDUISTRUCT &flipH, CMDUISTRUCT &flipV, CMDUISTRUCT &inverse, const Element::CPairArray & v);
		void UpdateFontUIStruct(FONTUISTRUCT &f, const Element::CPairArray & v);
		void UpdateAlignUIStruct(CMDUISTRUCT &c, const Element::CPairArray & v);
		void UpdateUIStructs();

		afx_msg void OnCreateCaption ();

		Element::CPairArray Confine (bool bSetPos = true);

	public:
		afx_msg void UpdateAlignmentUI (CCmdUI * pCmdUI);
		afx_msg void OnElementsDelete();

	protected:
		static void InsertAscendingXPos (Element::CPairArray & v, const Element::CPair & p); 
		static void InsertAscendingYPos (Element::CPairArray & v, const Element::CPair & p);
		static Element::CPairArray SortByXPos (const Element::CPairArray & v); 
		static Element::CPairArray SortByYPos (const Element::CPairArray & v); 

		afx_msg void OnAlignBottom();
		afx_msg void OnAlignHcenter();
		afx_msg void OnAlignLeft();
		afx_msg void OnAlignRight();
		afx_msg void OnAlignTop();
		afx_msg void OnAlignVcenter();
		afx_msg void OnFontBold();
		afx_msg void OnFontItalic();
		afx_msg void OnAlignProdcenter();
		afx_msg void OnUpdateAlignProdcenter(CCmdUI* pCmdUI);
		afx_msg void OnAlignFliphorz();
		afx_msg void OnUpdateAlignFliphorz(CCmdUI* pCmdUI);
		afx_msg void OnAlignFlipvert();
		afx_msg void OnUpdateAlignFlipvert(CCmdUI* pCmdUI);
		afx_msg void OnAlignInverse();
		afx_msg void OnUpdateAlignInverse(CCmdUI* pCmdUI);
		afx_msg void OnUpdateFontBold(CCmdUI* pCmdUI);
		afx_msg void OnUpdateFontItalic(CCmdUI* pCmdUI);
		afx_msg void OnEditCut();
		afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
		afx_msg void OnEditCopy();
		afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
		afx_msg void OnEditPaste();
		afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
		afx_msg void OnEditSelectall(); 
		afx_msg void OnUpdateEditSelectall(CCmdUI* pCmdUI);
		afx_msg void OnAlignVDistribute ();
		afx_msg void OnAlignHDistribute ();
		afx_msg void OnUpdateAlignVDistribute (CCmdUI * pCmdUI);
		afx_msg void OnUpdateAlignHDistribute (CCmdUI * pCmdUI);
		afx_msg void OnSaveToFile ();
		afx_msg void OnUpdateSaveToFile (CCmdUI* pCmdUI);

		afx_msg void OnAlignLockMove ();
		afx_msg void OnUpdateAlignLockMove (CCmdUI * pCmdUI);
		afx_msg void OnAlignRubberband();
		afx_msg void OnUpdateAlignRubberband (CCmdUI * pCmdUI);

		afx_msg void OnViewZoomin();
		afx_msg void OnViewZoomout();
		afx_msg void OnViewZoomnormal100();
		afx_msg void OnViewZoomMax();
		afx_msg void OnUpdateZoom (CCmdUI * pCmdUI);
		afx_msg void OnViewZoomCustom();

		afx_msg void OnApiElementCommand(UINT nID);
		afx_msg void OnUpdateApiElementCommand(CCmdUI *pCmdUI);
		afx_msg void OnElementEdit();
		afx_msg void OnUpdateElementsDelete(CCmdUI* pCmdUI);
		afx_msg void OnElementsList();
		afx_msg void OnPropertiesMovable();
		afx_msg void OnPropertiesResizable(); 
		afx_msg void OnPropertiesFliph(); 
		afx_msg void OnPropertiesFlipv();
		afx_msg void OnPropertiesInverse();
		afx_msg void OnUpdateElementsEdit(CCmdUI* pCmdUI);
		afx_msg void OnBringToFront();
		afx_msg void OnSendToBack();
		afx_msg void OnBringForward ();
		afx_msg void OnSendBackward ();

		//{{AFX_MSG(CTaskDoc)
		afx_msg void OnBoxChange();
		afx_msg void OnUpdateBoxChange(CCmdUI* pCmdUI);
		afx_msg void OnBoxProperties();
		afx_msg void OnPanelProperties();
		afx_msg void OnHeadProperties();
		afx_msg void OnViewZoomFittoscreen();
		afx_msg void OnFileProperties();
		afx_msg void OnDebuggingstuffConfining();
		afx_msg void OnUpdateDebuggingstuffConfining(CCmdUI* pCmdUI);
		afx_msg void OnDebuggingstuffDrawinprintercontext();
		afx_msg void OnDebuggingstuffExceptions();
		afx_msg void OnToolsInkusage();
		//}}AFX_MSG
	
		afx_msg void OnViewTransparent();
		afx_msg void OnUpdateViewTransparent(CCmdUI* pCmdUI);
		afx_msg void OnViewChangeperspective();
		afx_msg void OnRotateOrientationCcl();
		afx_msg void OnRotateOrientationCl();
		afx_msg void OnRotateViewDown();
		afx_msg void OnRotateViewUp();
		afx_msg void OnRotateViewLeft();
		afx_msg void OnRotateViewRight(); 
		afx_msg void OnRotateOrientation(Foxjet3d::Box::ROTATION r);
		afx_msg void OnViewShowheadboundaries();
		afx_msg void OnUpdateViewShowheadboundaries(CCmdUI* pCmdUI);
		afx_msg void OnViewShowdirection();
		afx_msg void OnViewShowCrosshairs();
		afx_msg void OnUpdateViewShowCrosshairs(CCmdUI* pCmdUI);
		afx_msg void OnUpdateViewShowdirection(CCmdUI* pCmdUI);
		afx_msg void OnDebugHeadconfig ();
		afx_msg void OnDebugLineconfig ();

		afx_msg void OnViewRulersHorizonal ();
		afx_msg void OnViewRulersVertical ();
		afx_msg void OnUpdateViewRulersHorizonal (CCmdUI* pCmdUI);
		afx_msg void OnUpdateViewRulersVertical (CCmdUI* pCmdUI);

		afx_msg void OnConfine();

	#ifdef _DEBUG
		void OnEditUndo(); 
		void OnEditRedo(); 
	#endif
		void TraceVectors ();

		DECLARE_MESSAGE_MAP()
	};
}; //namespace TaskDoc

#define TRACESNAPSHOT(p,s) CTaskDoc::Trace(p, s, __FILE__, __LINE__)

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TASKDOC_H__0DCAA080_A2F8_46E3_BFAE_41C20E3C4EAD__INCLUDED_)
