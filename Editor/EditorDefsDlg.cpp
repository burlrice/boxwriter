// EditorDefsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "mkdraw.h"
#include "EditorDefsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditorDefsDlg dialog


CEditorDefsDlg::CEditorDefsDlg(CWnd* pParent /*=NULL*/)
:	m_nResizeHandle (0),
	m_bOverlapped (FALSE),
	FoxjetCommon::CEliteDlg(IDD_EDITORDEFS, pParent)
{
	//{{AFX_DATA_INIT(CEditorDefsDlg)
	m_nUnits = -1;
	//}}AFX_DATA_INIT
}


void CEditorDefsDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditorDefsDlg)
	DDX_Radio(pDX, RDO_INCHES, m_nUnits);
	//}}AFX_DATA_MAP
	
	DDX_Text (pDX, TXT_HANDLE, m_nResizeHandle);
	DDV_MinMaxInt (pDX, m_nResizeHandle, 
		Element::CElementTracker::m_lmtHandle.m_dwMin,
		Element::CElementTracker::m_lmtHandle.m_dwMax);
	DDX_Check (pDX, CHK_OVERLAP, m_bOverlapped);
}


BEGIN_MESSAGE_MAP(CEditorDefsDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CEditorDefsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditorDefsDlg message handlers

BOOL CEditorDefsDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();
	
#ifdef __IPPRINTER__
	ASSERT (GetDlgItem (LBL_HANDLE));
	ASSERT (GetDlgItem (TXT_HANDLE));

	GetDlgItem (LBL_HANDLE)->ShowWindow (SW_HIDE);
	GetDlgItem (TXT_HANDLE)->ShowWindow (SW_HIDE);
#endif //__IPPRINTER__
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

