USE [MarksmanElite_FTO]
GO
/****** Object:  Table [dbo].[BarcodeScans]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BarcodeScans](
	[ID] [int] NOT NULL,
	[Time] [datetime] NULL,
	[LineID] [int] NULL,
	[Scanner] [nvarchar](max) NULL,
	[Taskname] [nvarchar](max) NULL,
	[Barcode] [nvarchar](max) NULL,
	[TotalScans] [int] NULL,
	[GoodScans] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Boxes]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Boxes](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Desc] [nvarchar](max) NULL,
	[Width] [int] NULL,
	[Length] [int] NULL,
	[Height] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BoxToLine]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoxToLine](
	[LineID] [int] NULL,
	[BoxID] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ClassOfService]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClassOfService](
	[Service Type] [nvarchar](255) NULL,
	[Service Indicator] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CountryCodes]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CountryCodes](
	[Country] [nvarchar](255) NULL,
	[A2] [nvarchar](255) NULL,
	[A3] [nvarchar](255) NULL,
	[Number] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Deleted]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Deleted](
	[ID] [int] NOT NULL,
	[LineID] [int] NULL,
	[TaskName] [nvarchar](max) NULL,
	[Data] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Heads]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Heads](
	[ID] [int] NOT NULL,
	[PanelID] [int] NULL,
	[Name] [nvarchar](max) NULL,
	[UID] [nvarchar](15) NOT NULL,
	[RelativeHeight] [int] NULL,
	[Channels] [smallint] NULL,
	[HorzRes] [int] NOT NULL,
	[PhotocellDelay] [int] NULL,
	[Encoder] [smallint] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[Direction] [tinyint] NOT NULL,
	[Angle] [float] NOT NULL,
	[Photocell] [int] NULL,
	[SharePhotocell] [smallint] NOT NULL,
	[ShareEncoder] [smallint] NULL,
	[Inverted] [bit] NOT NULL,
	[HeadType] [smallint] NULL,
	[RemoteHead] [bit] NOT NULL,
	[MasterHead] [bit] NOT NULL,
	[IntTachSpeed] [smallint] NOT NULL,
	[NozzleSpan] [float] NOT NULL,
	[PhotocellRate] [float] NULL,
	[SerialParams] [nvarchar](50) NULL,
	[EncoderDivisor] [int] NULL,
	[DoublePulse] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Images]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Images](
	[BoxID] [int] NULL,
	[Panel] [int] NULL,
	[Image] [nvarchar](max) NULL,
	[Attributes] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Lines]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lines](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Desc] [nvarchar](max) NULL,
	[Maintenance] [datetime] NULL,
	[NoRead] [nvarchar](max) NULL,
	[ResetScanInfo] [bit] NOT NULL,
	[MaxConsecutiveNoReads] [smallint] NULL,
	[BufferOffset] [smallint] NULL,
	[SignificantChars] [smallint] NULL,
	[AuxBoxIp] [nvarchar](15) NOT NULL,
	[AuxBoxEnabled] [bit] NOT NULL,
	[SerialParams1] [nvarchar](15) NULL,
	[SerialParams2] [nvarchar](15) NULL,
	[SerialParams3] [nvarchar](15) NULL,
	[AMS32_A1] [smallint] NULL,
	[AMS32_A2] [smallint] NULL,
	[AMS32_A3] [smallint] NULL,
	[AMS32_A4] [smallint] NULL,
	[AMS32_A5] [smallint] NULL,
	[AMS32_A6] [smallint] NULL,
	[AMS32_A7] [smallint] NULL,
	[AMS256_A1] [smallint] NULL,
	[AMS256_A2] [smallint] NULL,
	[AMS256_A3] [smallint] NULL,
	[AMS256_A4] [smallint] NULL,
	[AMS256_A5] [smallint] NULL,
	[AMS256_A6] [smallint] NULL,
	[AMS256_A7] [smallint] NULL,
	[AMS_Interval] [float] NULL,
	[PrinterID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Messages]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Messages](
	[ID] [int] NOT NULL,
	[HeadID] [int] NULL,
	[TaskID] [int] NULL,
	[Name] [nvarchar](max) NULL,
	[Desc] [nvarchar](max) NULL,
	[Data] [nvarchar](max) NULL,
	[Height] [int] NULL,
	[IsUpdated] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Options]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Options](
	[ID] [int] NOT NULL,
	[OptionDesc] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OptionsToGroup]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OptionsToGroup](
	[GroupsID] [int] NULL,
	[OptionsID] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Panels]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Panels](
	[ID] [int] NOT NULL,
	[LineID] [int] NULL,
	[Number] [int] NULL,
	[Name] [nvarchar](max) NULL,
	[Direction] [tinyint] NULL,
	[Orientation] [tinyint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Printers]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Printers](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[Master] [bit] NOT NULL,
	[Type] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Reports]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reports](
	[Time] [datetime] NULL,
	[Action] [nvarchar](max) NULL,
	[Username] [nvarchar](max) NULL,
	[TaskName] [nvarchar](max) NULL,
	[Counts] [nvarchar](max) NULL,
	[Line] [nvarchar](max) NULL,
	[Type] [int] NULL,
	[PrinterID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SecurityGroups]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SecurityGroups](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Settings]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[LineID] [int] NULL,
	[Key] [nvarchar](max) NULL,
	[Data] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Shifts]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shifts](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[tm] [datetime] NULL,
	[Code] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tasks]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tasks](
	[ID] [int] NOT NULL,
	[LineID] [int] NULL,
	[BoxID] [int] NULL,
	[Name] [nvarchar](max) NULL,
	[Desc] [nvarchar](max) NULL,
	[DownloadString] [nvarchar](max) NULL,
	[Transformation] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[temp]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp](
	[Text] [nvarchar](50) NULL,
	[Memo] [nvarchar](max) NULL,
	[Number Byte] [tinyint] NULL,
	[Number Integer] [smallint] NULL,
	[Number Long Integer] [int] NULL,
	[Number Single] [real] NULL,
	[Number Double] [float] NULL,
	[NUmber Replication ID] [uniqueidentifier] NULL,
	[Date/Time] [datetime] NULL,
	[Currency General Number] [money] NULL,
	[Currency Currency] [money] NULL,
	[Currency Fixed] [money] NULL,
	[Currency Standard] [money] NULL,
	[Currency Percent] [money] NULL,
	[Currency Scientific] [money] NULL,
	[AutoNumber Long Integer] [int] NOT NULL,
	[AutoNumber Replication ID] [uniqueidentifier] NULL,
	[Yes/No] [bit] NOT NULL,
	[OLE Object] [image] NULL,
	[Hyperlink] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[test]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[test](
	[DBVT_BOOL] [bit] NOT NULL,
	[DBVT_UCHAR] [tinyint] NULL,
	[DBVT_SHORT] [smallint] NULL,
	[DBVT_LONG] [int] NULL,
	[DBVT_SINGLE] [real] NULL,
	[DBVT_DOUBLE] [float] NULL,
	[DBVT_STRING] [nvarchar](50) NULL,
	[DBVT_DATE] [datetime] NULL,
	[DBVT_BINARY] [uniqueidentifier] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 1/11/2019 12:56:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] NOT NULL,
	[FirstName] [nvarchar](max) NOT NULL,
	[LastName] [nvarchar](max) NOT NULL,
	[UserName] [nvarchar](max) NOT NULL,
	[PassW] [nvarchar](max) NOT NULL,
	[SecurityGroupID] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
