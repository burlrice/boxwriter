#ifndef __EXTERN_H__
#define __EXTERN_H__

#include "MkDraw.h"
#include "Alias.h"
#include "ElementDefaults.h"
#include "OdbcDatabase.h"

namespace EditorGlobals
{
	extern FoxjetCommon::CElementDefaults & defElements;
	extern CMkDrawApp theApp;
	extern FoxjetDatabase::COdbcDatabase & database;
}; //namespace EditorGlobals

#endif //__EXTERN_H__
