#pragma once


// CDraftSliderDlg dialog

class CDraftSliderDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CDraftSliderDlg)

public:
	CDraftSliderDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDraftSliderDlg();

// Dialog Data
	enum { IDD = IDD_DRAFT_SLIDER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();

	afx_msg void OnActivate (UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnSetfocus();
	afx_msg void OnKillfocus();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

};
