#if !defined(AFX_FONTBAR_H__407C285F_B9CD_481E_A944_689F3AA805EA__INCLUDED_)
#define AFX_FONTBAR_H__407C285F_B9CD_481E_A944_689F3AA805EA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FontBar.h : header file
//

#include "DraftSliderDlg.h"

#include <afxext.h>

namespace HeadView
{
	class CHeadView;
}

const UINT WM_DRAFTSLIDERCLOSED = ::RegisterWindowMessage (_T ("WM_DRAFTSLIDERCLOSED"));
const UINT WM_DRAFTSLIDERCHANGED = ::RegisterWindowMessage (_T ("WM_DRAFTSLIDERCHANGED"));

/////////////////////////////////////////////////////////////////////////////
// CFontBar dialog

class CFontBar : public CDialogBar
{
	friend class HeadView::CHeadView;

// Construction
public:
	CFontBar();   // standard constructor

	CString GetFontName () const;
	bool SetFontName (const CString & str);

	int GetFontSize () const;
	bool SetFontSize (int nSize);

	bool SetFontWidth (int nSize);

// Dialog Data
	//{{AFX_DATA(CFontBar)
	enum { IDD = IDD_FONTBAR_MATRIX };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFontBar)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	bool SetFontData (int nSize, UINT nCtrlID);
	int FindFontData (int nIndexStart, int nSize, UINT nCtrlID) const;
	int FindFontData (int nIndexStart, const CString & strSize, UINT nCtrlID) const;
	int AddFontData (const CString & strSize, UINT nCtrlID);
	int AddFontData (int nSize, UINT nCtrlID);
	int GetFontData (int nIndex, UINT nCtrlID) const;

public:
	void ResetContent (ULONG lHeadID);

	CDraftSliderDlg * m_pwndDraftSlider;

protected:
	ULONG m_lHeadID;
	int m_nMin;
	int m_nMax;
	bool m_bEditFont;
	bool m_bEditWidth;
	int m_nLastWidth;
	HACCEL m_hAccel;

	// Generated message map functions
	//{{AFX_MSG(CFontBar)
	afx_msg void OnEndeditFontsize(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBegineditFontsize(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangeFontsize();
	afx_msg void OnSelchangeFontname();
	afx_msg void OnUpdateName(CCmdUI* pCmdUI);
	afx_msg void OnFontsizeChange();
	//}}AFX_MSG
	
	afx_msg void OnUpdateSize (CCmdUI * p);

	afx_msg void OnEndeditFontwidth(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBegineditFontwidth(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangeFontwidth();
	afx_msg void OnUpdateWidth (CCmdUI * p);
	afx_msg void OnSetfocusFontwidth ();
	afx_msg void OnKillfocusFontwidth ();
	afx_msg LRESULT OnKbEnChange (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDraftSliderClosed (WPARAM wParam, LPARAM lParam);
	afx_msg void OnMore ();
	afx_msg int OnCreate (LPCREATESTRUCT lpCreateStruct);

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FONTBAR_H__407C285F_B9CD_481E_A944_689F3AA805EA__INCLUDED_)
