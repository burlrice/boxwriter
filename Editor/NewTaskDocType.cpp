// NewTaskDocType.cpp: implementation of the CNewTaskDocType class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "mkdraw.h"
#include "NewTaskDocType.h"
#include "TaskDoc.h"
#include "TaskView.h"
#include "TaskFrame.h"
#include "Extern.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace EditorGlobals;
using namespace FoxjetDocument;

IMPLEMENT_DYNAMIC (CNewTaskDocType, CNewDocType);

CNewTaskDocType::CNewTaskDocType()
:	CNewDocType (FoxjetFile::TASK)
{
	m_hIcon = (HICON)::LoadImage (::AfxGetInstanceHandle (), 
		MAKEINTRESOURCE (IDR_TASK), IMAGE_ICON, 0, 0, LR_DEFAULTSIZE);
	ASSERT (m_hIcon);
	CTaskDocTemplate * pTemplate = new CTaskDocTemplate ();

	m_pDocTemplate  = pTemplate;
	m_strLibrary = CMkDrawDocManager::GetFileLibrary (* this);
	m_strTitle = CMkDrawDocManager::GetFileTitle (* this);
	GetFirstLineRecord (pTemplate->m_line);
	GetFirstBoxRecord (database, pTemplate->m_line.m_lID, pTemplate->m_box);
}

CNewTaskDocType::~CNewTaskDocType()
{
}

