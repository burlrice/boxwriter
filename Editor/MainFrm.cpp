// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "mkdraw.h"
#include <Dbt.h>

#include <wingdi.h>
#include <afxdlgs.h>
#include <afxext.h>
#include <AFXPRIV.H>

#include "MainFrm.h"
#include "Splash.h"
#include "DefsDlg.h"
#include "List.h"
#include "ElementDefaults.h"
#include "Debug.h"
#include "ZoomDlg.h"
#include "extern.h"
#include "Database.h"
#include "BoxConfigDlg.h"
#include "BoxUsageDlg.h"
#include "CopyDlg.h"
#include "BaseView.h"
#include "TaskView.h"
#include "DxResDlg.h"
#include "Database.h"
#include "Compare.h"
#include "Utils.h"
#include "DeleteDlg.h"
#include "ExportDlg.h"
#include "TemplExt.h"
#include "EditorDefsDlg.h"
#include "ExportFileDlg.h"
#include "ImportFileDlg.h"
#include "WinMsg.h"
#include "Edit.h"
#include "System.h"
#include "Parse.h"
#include "Registry.h"
#include "TaskFrame.h"
#include "TimeChangeDlg.h"
#include "Database.h"
#include "MsgBox.h"

#ifdef __WINPRINTER__
	#include "TextElement.h"
#endif //__WINPRINTER__

#ifdef _DEBUG
	#include "..\PrinterDriver\NEXT\src\Broadcast.h"
#endif _DEBUG

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


using namespace EditorGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDocument;
using namespace FoxjetDatabase;
using namespace Foxjet3d;

using namespace Panel;
using namespace TaskView;
using namespace TaskDoc;
using namespace Element;

#ifdef _DEBUG
	const UINT WM_PRINTDRIVER_NOTICE = ::RegisterWindowMessage (_T ("Foxjet::Driver::Notice"));
#endif

#define TIMER_DATABASE_MONITOR 1

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_COMMAND(ID_DEFINE_ELEMENTDEFAULTS, OnDefineElementdefaults)
	ON_WM_INITMENU()
	ON_WM_SHOWWINDOW()
	ON_COMMAND(ID_FILE_SAVEALL, OnFileSaveall)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVEALL, OnUpdateFileSaveall)
	ON_COMMAND(ID_DEFINE_BOXES, OnDefineBoxes)
	ON_COMMAND(ID_DEFINE_BOXUSAGE, OnDefineBoxusage)
	ON_COMMAND(ID_DEBUGGINGSTUFF_SHOWCOPYDIALOG, OnDebuggingstuffShowcopydialog)
	ON_COMMAND(ID_VIEW_TOOLBAR, OnViewToolbar)
	ON_COMMAND(ID_VIEW_STATUS_BAR, OnViewStatusbar)
	ON_COMMAND(ID_VIEW_ROTATEBAR, OnViewRotatebar)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ROTATEBAR, OnUpdateViewRotatebar)
	ON_WM_INITMENUPOPUP()
	ON_COMMAND(ID_DEBUGGINGSTUFF_SETDXRESOLUTION, OnDebuggingstuffSetdxresolution)
	ON_COMMAND(ID_FILE_DELETE, OnFileDelete)
	ON_COMMAND(ID_FILE_EXPORT, OnFileExport)
	ON_COMMAND(ID_DEBUGGINGSTUFF_RUNTESTTHREAD, OnDebuggingstuffRuntestthread)
	ON_COMMAND(ID_DEBUGGINGSTUFF_EXPDATETEST, OnDebuggingstuffExpTest)
	ON_COMMAND(ID_FILE_COPY, OnFileCopy)
	ON_COMMAND(ID_FILE_IMPORT, OnFileImport)
	ON_COMMAND(ID_HELP_TRANSLATE, OnHelpTranslate)
	ON_UPDATE_COMMAND_UI(ID_HELP_TRANSLATE, OnUpdateHelpTranslate)
	ON_WM_TIMECHANGE()
	//}}AFX_MSG_MAP

	ON_WM_TIMER ()
	ON_COMMAND_RANGE(ID_DEFINE_01, ID_DEFINE_30, OnApiDefineCommand)

	ON_UPDATE_COMMAND_UI(ID_VIEW_TOOLBAR, OnUpdateViewToolbar)
	ON_UPDATE_COMMAND_UI(ID_VIEW_STATUS_BAR, OnUpdateViewStatusbar)
	ON_UPDATE_COMMAND_UI_RANGE(ID_DEFINE_01, ID_DEFINE_30, OnUpdateApiDefineCommand)
	ON_COMMAND(ID_VIEW_FONTBAR, OnViewFontbar)
	ON_UPDATE_COMMAND_UI(ID_VIEW_FONTBAR, OnUpdateViewFontbar)
	ON_COMMAND(ID_DEFINE_EDITORDEFAULTS, OnDefineEditordefaults)
	ON_COMMAND(ID_VIEW_ALIGNMENTBAR, OnViewAlignmentbar)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ALIGNMENTBAR, OnUpdateViewAlignmentbar)
	ON_COMMAND(ID_VIEW_ELEMENTBAR, OnViewElementbar)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ELEMENTBAR, OnUpdateViewElementbar)

	ON_COMMAND(ID_VIEW_HEADBAR, OnViewHeadbar)
	ON_UPDATE_COMMAND_UI(ID_VIEW_HEADBAR, OnUpdateViewHeadbar)

	ON_COMMAND(ID_DEBUGGINGSTUFF_SYSTEMCONFIG, OnDebuggingstuffSystemConfig)
	ON_COMMAND(ID_DEFINE_BACKUPPATH, OnDefineBackupPath)
	ON_COMMAND(ID_ELEMENTS_DELETE, OnElementsDelete)
	ON_UPDATE_COMMAND_UI(ID_ELEMENTS_DELETE, OnUpdateElementsDelete)

	ON_REGISTERED_MESSAGE (WM_ISEDITORRUNNING, OnIsAppRunning)
	ON_REGISTERED_MESSAGE (WM_ISEDITORINITIALIZED, OnIsAppInitialized)
	ON_REGISTERED_MESSAGE (WM_SHUTDOWNAPP, OnShutdown)
	ON_REGISTERED_MESSAGE (WM_SYSTEMPARAMCHANGE, OnSystemParamsChanged)
	ON_REGISTERED_MESSAGE (WM_CREATE_CAPTION, OnCreateCaptionBroadcast)
	ON_REGISTERED_MESSAGE (WM_EDITOR_NEW_TASK, OnNewTask)  // sw0867
	ON_REGISTERED_MESSAGE (WM_EDITOR_OPEN_TASK, OnOpenTask) // sw0867
	ON_REGISTERED_MESSAGE (WM_KEYBOARDCHANGE, OnKbChange)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_strTitle = GetAppTitle () + _T (" ") + LoadString (AFX_IDS_APP_TITLE);
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	UINT nStatusBar [] = {
		ID_SEPARATOR,		// default
		ID_SEPARATOR,		// zoom
		ID_SEPARATOR,		// photo
		ID_SEPARATOR,		// location
	};

	// CG: The following line was added by the Splash Screen component.
	CSplashWnd::ShowSplashScreen(this);

	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
			| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC, 
			CRect(0, 0, 0, 0), AFX_IDW_TOOLBAR) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME_MATRIX))
	{
		TRACEF(_T ("Failed to create toolbar\n"));
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(nStatusBar,
		  sizeof(nStatusBar)/sizeof(UINT)))
	{
		TRACEF(_T ("Failed to create status bar\n"));
		return -1;      // fail to create
	}

	if (!m_wndElementBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_BOTTOM
			| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC, 
			CRect(0, 0, 0, 0), BAR_EDIT) ||
		!FoxjetCommon::LoadToolBar(m_wndElementBar))
	{
		TRACEF (_T ("Failed to create toolbar\n"));
		return -1;      // fail to create
	}

	if (!m_wndRotateBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
			| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC, 
			CRect(0, 0, 0, 0), BAR_ROTATE) ||
		!m_wndRotateBar.LoadToolBar(IDR_ROTATION_MATRIX))
	{
		TRACEF(_T ("Failed to create toolbar\n"));
		return -1;      // fail to create
	}

	if (!m_wndAlignBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
			| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC, 
			CRect(0, 0, 0, 0), BAR_ALIGN) ||
		!m_wndAlignBar.LoadToolBar(IDR_ALIGNBAR_MATRIX))
	{
		TRACEF (_T ("Failed to create toolbar\n"));
		return -1;      // fail to create
	}

	EnableDocking(CBRS_ALIGN_ANY);

	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	//EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);
	InitStatusBar ();

	m_wndElementBar.EnableDocking(CBRS_ALIGN_ANY);
	//EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndElementBar);

	m_wndRotateBar.EnableDocking(CBRS_ALIGN_ANY);
	//EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndRotateBar);

	m_wndAlignBar.EnableDocking(CBRS_ALIGN_ANY);
	//EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar (&m_wndAlignBar);

	m_wndFontBar.Create (this, IDD_FONTBAR_MATRIX, CBRS_TOP | CBRS_GRIPPER, BAR_FONT);
	m_wndFontBar.EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndFontBar);

	m_wndHeadBar.Create (this, IDD_HEADBAR_MATRIX, CBRS_TOP | CBRS_GRIPPER, BAR_HEAD);
	m_wndHeadBar.EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndHeadBar);

	m_wndToolBar.SetWindowText (LoadString (IDS_TOOL_BAR));
	m_wndRotateBar.SetWindowText (LoadString (IDS_ROTATION_BAR));
	m_wndAlignBar.SetWindowText (LoadString (IDS_ALIGN_BAR));
	m_wndFontBar.SetWindowText (LoadString (IDS_FONTBAR));
	m_wndElementBar.SetWindowText (LoadString (IDS_ELEMENTBAR));
	m_wndHeadBar.SetWindowText (LoadString (IDS_HEADBAR));

	LoadBarState (_T ("Settings\\Toolbars\\Elite"));

	{
		const int nMinutes = 1;

		SetTimer (TIMER_DATABASE_MONITOR, 1000 * 60 * nMinutes, NULL);
	}

	#ifndef _DEBUG
	if (CWnd * p = m_wndFontBar.GetDlgItem (BTN_MORE))
		p->ShowWindow (SW_HIDE);
	#endif

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	//if (IsMatrix ()) 
	{
		cs.style &= ~WS_MINIMIZEBOX;
	}

	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


void CMainFrame::InitStatusBar()
{
	int nPane = 1;
	m_wndStatusBar.SetPaneInfo (nPane++, ID_STATUS_ZOOM, SBPS_NORMAL, 70);
	m_wndStatusBar.SetPaneInfo (nPane++, ID_STATUS_PHOTO, SBPS_NORMAL, 100);
	m_wndStatusBar.SetPaneInfo (nPane++, ID_STATUS_LOCATION, SBPS_NORMAL, 100);
}

void CMainFrame::OnClose() 
{
	BeginWaitCursor ();

	const CMkDrawDocManager & manager = theApp.GetDocManager ();
	bool bValidBarState = true;

	for (POSITION pos = manager.GetFirstDocTemplatePosition (); pos && bValidBarState;  ) {
		CDocTemplate * pTemplate = manager.GetNextDocTemplate (pos);

		for (POSITION posDoc = pTemplate->GetFirstDocPosition (); posDoc && bValidBarState; ) {
			if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, pTemplate->GetNextDoc (posDoc)))  {
					CTaskView * pView = (CTaskView *)pDoc->GetView ();

					if (pView->IsPrintPreviewOpen ()) 
						bValidBarState = false;
			}
		}
	}

	if (bValidBarState) {
		if (CTaskFrame * pFrame = DYNAMIC_DOWNCAST (CTaskFrame, theApp.GetActiveFrame ()))
			pFrame->SaveSettings ();

		SaveBarState (_T ("Settings\\Toolbars\\Elite"));
	}

	{
		CString strCmdLine = ::GetCommandLine ();

		strCmdLine.MakeLower ();

		if (strCmdLine.Find (_T ("/savebarstate")) != -1) {
			HKEY hKey = NULL;

			if (::RegCreateKey (HKEY_CURRENT_USER, _T ("Software\\FoxJet\\BoxWriter ELITE\\Settings\\Toolbars"), &hKey) == ERROR_SUCCESS) {
				const CString strFile = FoxjetDatabase::GetHomeDir () + _T ("\\toolbar.reg");

				::DeleteFile (strFile);
				VERIFY (FoxjetDatabase::EnablePrivilege (SE_BACKUP_NAME));
				LRESULT lResult = ::RegSaveKey (hKey, strFile, NULL);
				TRACEF (FormatMessage (lResult));
				MsgBox (* this, strFile + _T ("\n") + FormatMessage (lResult));
				::RegCloseKey (hKey);
			}
		}
	}

	SaveState (this);
	CMDIFrameWnd::OnClose();
	EndWaitCursor ();
}

void CMainFrame::OnViewStatusbar() 
{
	ShowControlBar (&m_wndStatusBar, (m_wndStatusBar.GetStyle () & WS_VISIBLE) == 0, FALSE);
}

void CMainFrame::OnUpdateViewStatusbar(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck ((m_wndStatusBar.GetStyle () & WS_VISIBLE) ? 1 : 0);
}

void CMainFrame::OnViewToolbar() 
{
	ShowControlBar (&m_wndToolBar, (m_wndToolBar.GetStyle () & WS_VISIBLE) == 0, FALSE);
}

void CMainFrame::OnUpdateViewToolbar(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck ((m_wndToolBar.GetStyle () & WS_VISIBLE) ? 1 : 0);
}

void CMainFrame::OnViewRotatebar() 
{
	ShowControlBar (&m_wndRotateBar, (m_wndRotateBar.GetStyle () & WS_VISIBLE) == 0, FALSE);
}

void CMainFrame::OnUpdateViewRotatebar(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck ((m_wndRotateBar.GetStyle () & WS_VISIBLE) ? 1 : 0);
}

void CMainFrame::OnDefineElementdefaults() 
{
	CElementDefsDlg dlg (this);
	const CVersion ver = GetElementListVersion ();
	defElements.LoadRegDefs (ver);

	if (dlg.DoModal () == IDOK) {
		for (int i = 0; i < GetElementTypeCount (); i++) {
			if (IsValidElementType (i, FoxjetDatabase::GetDefaultHead ())) {
				ASSERT (i < dlg.m_vDefs.GetSize ());
				bool bResult = defElements.SetElement (i, dlg.m_vDefs [i], ver);
				ASSERT (bResult);
			}
		}
	}
}


BOOL CMainFrame::DestroyWindow() 
{
	return CMDIFrameWnd::DestroyWindow();
}

void CMainFrame::OnInitMenu(CMenu* pMenu) 
{
	CMDIFrameWnd::OnInitMenu(pMenu);
	CView * pView = theApp.GetActiveView ();

	if (pView && pView->IsKindOf (RUNTIME_CLASS (CBaseView))) {
		CBaseView * p = (CBaseView *)pView;
		p->InitMenu (pMenu);
		return;
	}

	InitDefineMenu (pMenu);
}


void CMainFrame::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CMDIFrameWnd::OnShowWindow(bShow, nStatus);
}

void CMainFrame::OnUpdateApiDefineCommand(CCmdUI *pCmdUI)
{
	bool bEnabled = false;
	UINT nID = pCmdUI->m_nID;
	
	if (nID >= ID_DEFINE_01 && nID <= ID_DEFINE_30) 
		bEnabled = IsDefineCmdEnabled (nID);

	pCmdUI->Enable (bEnabled);
}

void CMainFrame::OnApiDefineCommand(UINT nID)
{
	using namespace FoxjetDatabase;

	typedef struct 
	{
		LINESTRUCT									m_line;
		CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &>	m_vBefore;
		CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &>	m_vAfter;
	} UPDATESTRUCT;

	if (nID < ID_DEFINE_01 || nID > ID_DEFINE_30) 
		return;

	COdbcDatabase & db = theApp.GetDB ();
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CArray <LPVOID, LPVOID> vTmp;

	GetPrinterLines (db, GetPrinterID (db), vLines); //GetLineRecords (db, vLines);

	for (int i = 0; i < vLines.GetSize (); i++) {
		UPDATESTRUCT * p = new UPDATESTRUCT;

		p->m_line = vLines [i];
		GetSettingsRecords (db, p->m_line.m_lID, p->m_vBefore);

		vTmp.Add (p);
	}

	OnDefineCmd (nID, this, GetElementListVersion ());

	for (int i = 0; i < vTmp.GetSize (); i++) {
		UPDATESTRUCT * p = (UPDATESTRUCT *)vTmp [i];
		ULONG lLineID = p->m_line.m_lID;

		GetSettingsRecords (db, lLineID, p->m_vAfter);

		if (p->m_vBefore != p->m_vAfter) // something changed, invalidate 
			InvalidateDocs (lLineID);
	}

	for (int i = 0; i < vTmp.GetSize (); i++) {
		UPDATESTRUCT * p = (UPDATESTRUCT *)vTmp [i];
		
		delete p;
	}
}

void CMainFrame::GetMessageString(UINT nID, CString& rMessage) const
{
	// if nID specifies an element, then ElementList.dll should
	// have the corresponding string table entry.  thus,
	// the default implementation of this function handles it
	if (IsDefineCmdEnabled (nID)) 
		rMessage = GetDefineCmdStatusText (nID);
	else
		CFrameWnd::GetMessageString (nID, rMessage);
}

CDocPtrArray CMainFrame::GetOpenDocs () const
{
	CDocManager * pMgr = theApp.m_pDocManager;
	CDocPtrArray vResult;

	for (POSITION posMgr = pMgr->GetFirstDocTemplatePosition (); posMgr != NULL; ) {
		CDocTemplate * pTemplate = pMgr->GetNextDocTemplate (posMgr);
		
		for (POSITION pos = pTemplate->GetFirstDocPosition (); pos != NULL; ) 
			if (CDocument * pDoc = pTemplate->GetNextDoc (pos))
				vResult.Add (pDoc);
	}

	return vResult;
}

void CMainFrame::OnFileSaveall() 
{
	CDocManager * pMgr = theApp.m_pDocManager;
	CStringArray vFail;
	CDocPtrArray v = GetOpenDocs ();
	const CString strMsg = LoadString (IDS_SAVING);

	theApp.BeginProgressBar (strMsg, v.GetSize ());

	for (int i = 0; i < v.GetSize (); i++) {
		CString strFile = v [i]->GetTitle ();

		theApp.StepProgressBar (strMsg + _T (": ") + strFile + _T ("..."));

		if (CBaseDoc * pBase = DYNAMIC_DOWNCAST (CBaseDoc, v [i])) {
			if (!pBase->Save ())
				vFail.Add (pBase->GetPathName ());
		}
	}

	theApp.EndProgressBar ();

	if (vFail.GetSize ()) {
		CString str = LoadString (IDS_FAILEDTOSAVE);

		for (int i = 0; i < vFail.GetSize (); i++)
			str += _T ("\n") + vFail [i];

		MsgBox (* this, str, LoadString (IDS_ERROR), MB_ICONERROR);
	}
}

void CMainFrame::OnUpdateFileSaveall(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable (theApp.GetActiveDocument () != NULL);
}

void CMainFrame::OnElementsDelete() 
{
	if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) 
		pDoc->OnElementsDelete ();
}

void CMainFrame::OnUpdateElementsDelete(CCmdUI* pCmdUI) 
{
	if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) 
		pCmdUI->Enable (theApp.m_uiSel.m_nSelCount > 0 && !pDoc->GetReadOnly ());
}

void CMainFrame::InvalidateDocs (ULONG lLineID) 
{
	// causes every element in every open doc to be redrawn

	const CMkDrawDocManager & manager = theApp.GetDocManager ();

	for (POSITION pos = manager.GetFirstDocTemplatePosition (); pos;  ) {
		CDocTemplate * pTemplate = manager.GetNextDocTemplate (pos);

		for (POSITION posDoc = pTemplate->GetFirstDocPosition (); posDoc; ) {
			if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, pTemplate->GetNextDoc (posDoc)))  {
				if (pDoc->GetLineID () == lLineID || lLineID == FoxjetDatabase::ALL) {
					CTaskView * pView = (CTaskView *)pDoc->GetView ();
					FoxjetCommon::CElementListArray allLists = pView->GetLists (0);
					Element::CPairArray vInvalidate;
					CDC dcMem;
					CBoundary workingHead;

					pDoc->GetBox ().GetWorkingHead (pView->GetParams (), workingHead);

					dcMem.CreateCompatibleDC (pView->GetDC ());

					for (int nMessage = 0; nMessage < pDoc->m_task.GetMessageCount (); nMessage++) {
						if (Foxjet3d::CMessage * pMsg = pDoc->m_task.GetMessage (nMessage)) {
							CArray <CElement *, CElement *> v;

							for (int nElement = 0; nElement < pMsg->m_list.GetSize (); nElement++) {
								CElement & e = pMsg->m_list [nElement];

								if (e.GetElement ()->GetClassID () != TEXT)
									v.Add (&e);
							}
							
							for (int nElement = 0; nElement < pMsg->m_list.GetSize (); nElement++) {
								CElement & e = pMsg->m_list [nElement];

								if (e.GetElement ()->GetClassID () == TEXT)
									v.Add (&e);
							}

							for (int nElement = 0; nElement < v.GetSize (); nElement++) {
								CElement & e = * v [nElement];
								const FoxjetDatabase::HEADSTRUCT & head = pMsg->m_list.GetHead ();
								Element::CPair pair (&e, &pMsg->m_list, &workingHead.m_card);
								CRect rc;

								vInvalidate.Add (pair);

								#ifdef __WINPRINTER__
								{
									using namespace FoxjetElements;
								
									if (TextElement::CTextElement * p = DYNAMIC_DOWNCAST (TextElement::CTextElement, e.GetElement ())) 
										if (p->GetLink ()) 
											p->Build (&allLists);
								}
								#endif //__WINPRINTER__

								e.GetElement ()->SetRedraw (true);
								e.GetElement ()->Build ();
								e.GetElement ()->Draw (dcMem, head, true);
							}
						}
					}

					if (pDoc->Confine ().GetSize ()) {
						pDoc->SetModifiedFlag (true, NULL);
						pDoc->UpdateAllViews (NULL, 0, NULL);
					}
					else {
						pDoc->Invalidate (vInvalidate, pView);
					}
				}
			}
		}
	}
}

void CMainFrame::UpdateDocBoxes (ULONG lBoxID) 
{
	using namespace FoxjetDatabase;

	const CMkDrawDocManager & manager = theApp.GetDocManager ();

	for (POSITION pos = manager.GetFirstDocTemplatePosition (); pos;  ) {
		CDocTemplate * pTemplate = manager.GetNextDocTemplate (pos);

		for (POSITION posDoc = pTemplate->GetFirstDocPosition (); posDoc; ) {
			if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, pTemplate->GetNextDoc (posDoc)))  {
				const BOXSTRUCT & box = pDoc->GetMembers ().m_box.GetBox ();

				if (box.m_lID == lBoxID) {
					BOXSTRUCT newBox;

					// NOTE: this relies on CBoxConfigDlg to prevent deleting a box that is in use by a task
					VERIFY (GetBoxRecord (database, box.m_lID, newBox));
					VERIFY (pDoc->SetBox (newBox, false));
					pDoc->UpdateAllViews (NULL, 0, NULL);
				}
			}
		}
	}
}

void CMainFrame::OnDefineBoxes() 
{
	// dlg uses a db transaction to commit or cancel changes
	CWaitCursor wait;
	Foxjet3d::CBoxConfigDlg dlg (database, this);
	dlg.m_units = defElements.GetUnits ();
	int nResult = dlg.DoModal ();
	wait.Restore ();

	if (nResult == IDOK)
		for (int i = 0; i < dlg.m_vIDUpdate.GetSize (); i++) 
			UpdateDocBoxes (dlg.m_vIDUpdate [i]);
}

void CMainFrame::OnDefineBoxusage() 
{
	// dlg uses a db transaction to commit or cancel changes
	CWaitCursor wait;
	Foxjet3d::BoxUsageDlg::CBoxUsageDlg dlg (database, this);
	const CString strSection =  _T ("Settings\\CBoxUsageDlg");
	
	dlg.m_units = defElements.GetUnits ();
	dlg.m_lLineID = theApp.GetProfileInt (strSection, _T ("LineID"), -1);

	if (dlg.DoModal () == IDOK) {
		theApp.WriteProfileInt (strSection, _T ("LineID"),	dlg.m_lLineID);
	}

	wait.Restore ();
}

void CMainFrame::OnDebuggingstuffShowcopydialog() 
{
	Foxjet3d::CCopyDlg (this).DoModal ();	
}

void CMainFrame::OnDebuggingstuffSystemConfig() 
{
	CCopyArray <DWORD, DWORD> v;

	FoxjetCommon::OnSystemConfig (this, defElements.m_strRegSection, theApp.GetDB (), NULL, v);
}

void CMainFrame::OnDefineBackupPath() 
{
	FoxjetDatabase::OnDefineBackupPath (this);
}

void CMainFrame::InitDefineMenu(CMenu *pMenu)
{
	for (int i = 0; CBaseView::m_nDefineMenuIDs [i] != -1; i++) {
		if (!IsDefineCmdEnabled (CBaseView::m_nDefineMenuIDs [i]))
			pMenu->DeleteMenu (CBaseView::m_nDefineMenuIDs [i], MF_BYCOMMAND);
		else {
			CString str = GetDefineMenuStr (CBaseView::m_nDefineMenuIDs [i]);

			pMenu->ModifyMenu (CBaseView::m_nDefineMenuIDs [i], MF_BYCOMMAND, 
				CBaseView::m_nDefineMenuIDs [i], str);
		}
	}
}

void CMainFrame::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu) 
{
	CMDIFrameWnd::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
	InitDefineMenu (pPopupMenu);
}


void CMainFrame::OnDebuggingstuffSetdxresolution() 
{
	using namespace Foxjet3d;

	CDxResDlg dlg (this);	
	const CString strSection = _T ("Settings\\DirectX");

	dlg.m_nRes = theApp.GetProfileInt (strSection, _T ("res"), 7);

	if (dlg.DoModal () == IDOK) 
		theApp.WriteProfileInt (strSection, _T ("res"), dlg.m_nRes);
}

void CMainFrame::OnViewFontbar() 
{
	ShowControlBar (&m_wndFontBar, (m_wndFontBar.GetStyle () & WS_VISIBLE) == 0, FALSE);
}

void CMainFrame::OnUpdateViewFontbar(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck ((m_wndFontBar.GetStyle () & WS_VISIBLE) ? 1 : 0);
}

void CMainFrame::OnViewHeadbar() 
{
	ShowControlBar (&m_wndHeadBar, (m_wndHeadBar.GetStyle () & WS_VISIBLE) == 0, FALSE);
}

void CMainFrame::OnUpdateViewHeadbar(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck ((m_wndHeadBar.GetStyle () & WS_VISIBLE) ? 1 : 0);
}

void CMainFrame::OnViewAlignmentbar() 
{
	ShowControlBar (&m_wndAlignBar, (m_wndAlignBar.GetStyle () & WS_VISIBLE) == 0, FALSE);
}

void CMainFrame::OnUpdateViewAlignmentbar(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck ((m_wndAlignBar.GetStyle () & WS_VISIBLE) ? 1 : 0);
}

void CMainFrame::OnViewElementbar() 
{
	ShowControlBar (&m_wndElementBar, (m_wndElementBar.GetStyle () & WS_VISIBLE) == 0, FALSE);
}

void CMainFrame::OnUpdateViewElementbar(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck ((m_wndElementBar.GetStyle () & WS_VISIBLE) ? 1 : 0);
}

bool CALLBACK IsTaskOpen (ULONG lID)
{
	return theApp.IsTaskOpen (lID);
}

void CALLBACK BeginProgressBar (const CString & str, int nMax)
{
	theApp.BeginProgressBar (str, nMax);
}

void CALLBACK StepProgressBar (const CString & str)
{
	theApp.StepProgressBar (str);
}

void CALLBACK EndProgressBar ()
{
	theApp.EndProgressBar ();
}

void CMainFrame::OnFileDelete() 
{
	Foxjet3d::CDeleteDlg dlg (this);
	const CString strSection = _T ("Settings\\LastDelete");

	dlg.m_lpfctIsTaskOpen		= IsTaskOpen;
	dlg.m_lpfctBeginProgressBar	= BeginProgressBar;
	dlg.m_lpfctStepProgressBar	= StepProgressBar;
	dlg.m_lpfctEndProgressBar	= EndProgressBar;
	dlg.m_bPreview				= theApp.GetProfileInt (strSection, _T ("bPreview"), FALSE);

	if (dlg.DoModal () == IDOK) {
		DWORD dw = 0;

		theApp.WriteProfileInt (strSection, _T ("bPreview"), dlg.m_bPreview);
		::SendMessageTimeout (HWND_BROADCAST, WM_EDITOR_TASK_CHANGE, 0, 0, SMTO_NORMAL | SMTO_ABORTIFHUNG, 1, &dw);
	}
}


void CMainFrame::OnDefineEditordefaults ()
{
	CEditorDefsDlg dlg (this);
	CSize resize = CElementTracker::GetHandleSize ();

	dlg.m_nUnits = defElements.GetUnits ();
	dlg.m_nResizeHandle = max (resize.cx, resize.cy);
	dlg.m_bOverlapped = defElements.GetOverlappedWarning ();

	if (dlg.DoModal () == IDOK) {
		const CMkDrawDocManager & manager = theApp.GetDocManager ();

		defElements.SetUnits ((UNITS)dlg.m_nUnits);
		CElementTracker::SetHandleSize (CSize (dlg.m_nResizeHandle, dlg.m_nResizeHandle));
		
		theApp.WriteProfileInt (theApp.m_lpszSettings, theApp.m_lpszOverlapped, dlg.m_bOverlapped);
		defElements.SetOverlappedWarning (dlg.m_bOverlapped ? true : false);

		for (POSITION pos = manager.GetFirstDocTemplatePosition (); pos;  ) {
			CDocTemplate * pTemplate = manager.GetNextDocTemplate (pos);

			for (POSITION posDoc = pTemplate->GetFirstDocPosition (); posDoc; ) {
				if (CBaseDoc * pDoc = DYNAMIC_DOWNCAST (CBaseDoc, pTemplate->GetNextDoc (posDoc)))  {
					pDoc->SetUnits (defElements.GetUnits ());

					if (CTaskDoc * pTask = DYNAMIC_DOWNCAST (CTaskDoc, pDoc)) {
						if (CTaskView * pView = (CTaskView *)pTask->GetView ()) 
							NCINVALIDATE (pView);
					}
				}
			}
		}

		InvalidateDocs (FoxjetDatabase::ALL);
	}
}

static ULONG CALLBACK ThreadTestFunc (LPVOID lpData) 
{

	using namespace FoxjetDatabase;

	COdbcDatabase & db = * (COdbcDatabase *)lpData;
	static ULONG lThreadCount = 0;
	ULONG lThreadID = lThreadCount++;

	CString str;
	str.Format (_T ("ThreadTestFunc [%d] begin"), lThreadID);
	TRACEF (str);

	::srand (time (NULL));

	for (int i = 0; i < 3; i++) {
		CArray <LINESTRUCT, LINESTRUCT &> v;

		GetPrinterLines (db, GetPrinterID (db), v); //GetLineRecords (db, v);

		for (int nIndex = 0; nIndex < v.GetSize (); nIndex++) {
			const LINESTRUCT & line = v[nIndex];
			CString str;

			str.Format (_T ("%d: %s"), lThreadID, line.m_strName);

			TRACEF (str);
		}

		::Sleep (::rand () % (1000 * (6 - lThreadID)));
	}

	str.Format (_T ("ThreadTestFunc [%d] end"), lThreadID);
	TRACEF (str);
	return 0;
}

#ifdef _DEBUG
#include "DateTimeElement.h"
#endif

void CMainFrame::OnDebuggingstuffExpTest ()
{
#ifdef _DEBUG
	using namespace FoxjetElements;
	using namespace Foxjet3d;

	/*
	for (int nYear = 2015; nYear < 2050; nYear++) {
		//const CTimeSpan tmIncrement (1, 0, 0, 0);
		const CTime tmStart (nYear, 1, 1, 0, 0, 0);
		const bool bLeapYear = IsLeapYear (nYear);
		const int nDays = bLeapYear ? 366 : 365;

		TRACEF (tmStart.Format (_T ("%c")));

		for (int nDay = 0; nDay < nDays; nDay++) {
			const CTime tm = tmStart + CTimeSpan (nDay, 0, 0, 0);

			int nUS = _ttoi (FoxjetElements::CDateTimeElement::Format (-1, _T ("%j"), 0, &tm));
			int nEU = _ttoi (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &tm));

			TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%j"), 0, &tm) + _T (" ") + FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &tm));

			if (!bLeapYear)
				ASSERT (nUS == nEU);
			else {
				if (nUS < 60) 
					ASSERT (nUS == nEU);
				else if (nUS == 60) 
					ASSERT (nEU == 366);
				else
					ASSERT (nUS == (nEU + 1));
			}

		}

		int nBreak = 0;
	}

	int nBreak = 0;

	*/

	/*
	if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) {
		CTaskView * pView = (CTaskView *)pDoc->GetView ();
		Box::CBoxParams & params = pView->GetParams ();

		for (int nMessage = 0; nMessage < params.m_task.GetMessageCount (); nMessage++) {
			if (CMessage * pMsg = params.m_task.GetMessage (nMessage)) {
				for (int i = 0; i < pMsg->m_list.GetSize (); i++) {
					CElement & e = pMsg->m_list.GetElement (i);
						
					if (e.GetElement ()->GetClassID () == EXPDATE) {
						CDateTimeElement * p = (CDateTimeElement *)e.GetElement ();

						const CTimeSpan tmIncrement (1, 0, 0, 0);
						const CTime tmStart (2015, 1, 1, 0, 0, 0);
						CTime tm = tmStart;

						TRACEF (tm.Format (_T ("%m/%d/%Y")));

						for (int nDay = 0; nDay < 65535; nDay++) {
							bool bLeapYear = IsLeapYear (tm.GetYear ());
							CDateTimeElement::SetPrintTime (new CTime (tm), p->GetLineID ());
							
							p->SetDefaultData (_T ("%j"));
							p->Build ();
							int nUS = _ttoi (p->GetImageData ());

							p->SetDefaultData (_T ("%J"));
							p->Build ();
							int nEU = _ttoi (p->GetImageData ());


							TRACEF (tm.Format (_T ("%m/%d/%Y: ")) + ToString (nUS) + _T (" ") + ToString (nEU));

							if (!bLeapYear)
								ASSERT (nUS == nEU);
							else {
								if (nUS < 60) 
									ASSERT (nUS == nEU);
								else if (nUS == 60) 
									ASSERT (nEU == 366);
								else
									ASSERT (nUS == (nEU + 1));
							}

							tm += tmIncrement;
						}
					}
				}
			}
		}
	}
	*/

	if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) {
		CTaskView * pView = (CTaskView *)pDoc->GetView ();
		Box::CBoxParams & params = pView->GetParams ();

		for (int nMessage = 0; nMessage < params.m_task.GetMessageCount (); nMessage++) {
			if (CMessage * pMsg = params.m_task.GetMessage (nMessage)) {
				for (int i = 0; i < pMsg->m_list.GetSize (); i++) {
					CElement & e = pMsg->m_list.GetElement (i);
						
					switch (e.GetElement ()->GetClassID ()) { 
					case EXPDATE:
					case DATETIME:
						CDateTimeElement * p = (CDateTimeElement *)e.GetElement ();

						const CTimeSpan tmIncrement (0, 0, 0, 15); // (0, 1, 0, 0);
						const CTime tmStart (2015, 1, 1, 0, 0, 59);
						CTime tm = tmStart;
						bool bIsDst = false;
						CTimeSpan tmDiff = CTime::GetCurrentTime () - tmStart;
						CString strLast;

						TRACEF (tm.Format (_T ("%m/%d/%Y")));

						//for (int i = 0; i < 65535; i++) {
						while (tmDiff.GetDays () < (365 * 12)) {
							struct tm t;

							tm.GetLocalTm (&t);
							mktime (&t);
							
							if (bIsDst != t.tm_isdst) {
								bIsDst = t.tm_isdst;
							}

							CDateTimeElement::SetPrintTime (new CTime (tm), p->GetLineID ());

							p->SetDefaultData (_T ("%m/%d/%Y"));
							p->Build ();
							CString str = p->GetImageData ();

							//TRACEF (tm.Format (_T ("%c: ")) + p->GetImageData ());

							if (strLast != str) {
								TRACEF (tm.Format (_T ("%c: ")) + str);

								if (strLast.GetLength ()) {
									int h = _ttoi (tm.Format (_T ("%H")));
									int m = _ttoi (tm.Format (_T ("%M")));

									ASSERT (h == 6);
									ASSERT (m == 0);
								}
	
								strLast = str;
							}

							tm += tmIncrement;
							tmDiff = CTime::GetCurrentTime () - tmStart;
						}

						break;
					}
				}
			}
		}
	}

#endif //__WINPRINTER__
}

void CMainFrame::OnDebuggingstuffRuntestthread() 
{
	/*
	using namespace FoxjetDatabase;

	for (int i = 0; i < 5; i++) {
		DWORD dw = 0;

		HANDLE hThread = ::CreateThread ((LPSECURITY_ATTRIBUTES)NULL, 0,
			(LPTHREAD_START_ROUTINE)ThreadTestFunc, (LPVOID)&theApp.GetDB (), 0, &dw);
	
		::Sleep (0);
	}

	for (int i = 0; i < 3; i++) {
		CSingleLock lock (&theApp.GetDB ().m_mutex);

		TRACEF (_T ("Lock"));
		lock.Lock ();
		::Sleep (0);

		TRACEF (_T ("Unlock"));
		lock.Unlock ();
		::Sleep (0);
	}
	*/
}

void CMainFrame::OnFileExport() 
{
	Foxjet3d::Export (this);
}

void CMainFrame::OnFileImport() 
{
	Foxjet3d::Import (this);
}

void CMainFrame::OnFileCopy() 
{
	Foxjet3d::Copy (this);
}

LRESULT CMainFrame::OnIsAppInitialized (WPARAM wParam, LPARAM lParam)
{
	return theApp.IsInitialized ();
}

LRESULT CMainFrame::OnIsAppRunning (WPARAM wParam, LPARAM lParam)
{
	if (!wParam) {
		if (!SetForegroundWindow ()) {
		}
	}
	
	return WM_ISEDITORRUNNING;
}

LRESULT CMainFrame::OnShutdown (WPARAM wParam, LPARAM lParam)
{
	using namespace FoxjetDatabase;

	if ((wParam & SHUTDOWN_EDITOR) || (!wParam)) {
		CStringArray v;
		CString str = ::GetCommandLine ();

		TRACEF (str);
		TokenizeCmdLine (str, v);

		if (v.GetSize ()) {
			CString strExe = v [0];

			str.Delete (0, strExe.GetLength ());
			TRACEF (str);
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER,
				_T ("Software\\FoxJet\\Translator\\Cmd line"), 
				_T ("Editor"), str); // for Translator
		}

		SendMessage (WM_CLOSE, 1, 0);
	}

	return WM_SHUTDOWNAPP;
}

LRESULT CMainFrame::OnSystemParamsChanged (WPARAM wParam, LPARAM lParam)
{
	FoxjetDatabase::COdbcDatabase & db = theApp.GetDB ();
	const CVersion ver = GetElementListVersion ();

	TRACEF (_T ("OnSystemParamsChanged"));

	LoadSystemParams (db, ver);
	InvalidateDocs ();
	_flushall ();

	return WM_SYSTEMPARAMCHANGE;
}

void CMainFrame::OnHelpTranslate() 
{
	const CString strApp = _T ("Translator.exe");
	CString strPath = FoxjetDatabase::GetHomeDir () + _T ("\\") +  strApp;
	TCHAR szPath [MAX_PATH] = { 0 };

	HMODULE hModule = ::GetModuleHandle (NULL);
	::GetModuleFileName (hModule, szPath, ARRAYSIZE (szPath) - 1);

	if (::GetFileAttributes (strPath) == -1) {
		CFileDialog dlg (TRUE, strApp, strPath, 
			OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
			_T ("Applications (*.exe)|*.exe|All files (*.*)|*.*||"), this);

		if (dlg.DoModal () == IDOK) 
			strPath = dlg.GetPathName ();
		else
			return;
	}

	CString strCmdLine = strPath + _T (" \"") + szPath + _T ("\"") + CString (IsMatrix () ? _T (" /matrix") : _T (""));
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [0xFF];

	_tcsncpy (szCmdLine, strCmdLine, 0xFF);
    memset (&pi, 0, sizeof(pi));
    memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	if (!::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) 
		MsgBox (* this, LoadString (IDS_FAILEDTOLANUCHTRANSLATOR));
}

void CMainFrame::OnUpdateHelpTranslate(CCmdUI* pCmdUI) 
{
	static bool bEnable = false;
	static bool bInit = false;

	if (!bInit) {
		const CString strApp = _T ("Translator.exe");
		CString strPath = FoxjetDatabase::GetHomeDir () + _T ("\\") +  strApp;

		bEnable = ::GetFileAttributes (strPath) != -1;
		bInit = true;
	}
	
	pCmdUI->Enable (bEnable);
}

LRESULT CMainFrame::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	switch (message) {
	case WM_DEVICECHANGE:
		OnDeviceChange (wParam, lParam);
		break;
	case WM_SETTEXT:
		CMDIFrameWnd::DefWindowProc (WM_SETTEXT, 0, (LPARAM)(LPCTSTR)m_strTitle);
		return TRUE;
	case WM_CLOSE:
	case ID_APP_EXIT:
		{
			bool bRegisteredMsg = wParam ? true : false;

			if (bRegisteredMsg) {
				const CMkDrawDocManager & manager = theApp.GetDocManager ();

				// clear all modified flags so there's no prompt when the app closes

				for (POSITION pos = manager.GetFirstDocTemplatePosition (); pos;  ) {
					CDocTemplate * pTemplate = manager.GetNextDocTemplate (pos);

					for (POSITION posDoc = pTemplate->GetFirstDocPosition (); posDoc; ) {
						if (CBaseDoc * pDoc = DYNAMIC_DOWNCAST (CBaseDoc, pTemplate->GetNextDoc (posDoc)))  {
							pDoc->SetModifiedFlag (false, NULL);
						}
					}
				}
			}
		}

		break;
	}
	
	return CMDIFrameWnd::WindowProc(message, wParam, lParam);
}


void CMainFrame::SetTitle(const CString &strTitle)
{
	DWORD dwStyle = ::GetWindowLong (m_hWnd, GWL_STYLE);

	if (dwStyle & WS_VISIBLE)
		::SetWindowLong (m_hWnd, GWL_STYLE, dwStyle & ~WS_VISIBLE);

	DefWindowProc (WM_SETTEXT, 0, (LPARAM)(LPCTSTR)strTitle);

	if (dwStyle & WS_VISIBLE)
		::SetWindowLong (m_hWnd, GWL_STYLE, dwStyle);

	SetWindowPos (NULL, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOSIZE | SWP_NOMOVE | SWP_NOACTIVATE | SWP_DRAWFRAME);

	if (&m_strTitle != &strTitle)
		m_strTitle = strTitle;

	TRACEF (m_strTitle);
}


void CMainFrame::OnTimeChange() 
{
	using namespace FoxjetDatabase;

	static CTime tmLast = CTime::GetCurrentTime () - CTimeSpan (0, 1, 0, 0);

	CMDIFrameWnd::OnTimeChange();
	
	/*
	CTimeSpan tmDiff = CTime::GetCurrentTime () - tmLast;

	if (tmDiff.GetTotalSeconds ()) {
		tmLast = CTime::GetCurrentTime ();
		SETTINGSSTRUCT s;
		bool bLoggingEnabled = true;

		if (GetSettingsRecord (theApp.GetDB (), GetPrinterID (theApp.GetDB ()), _T ("Control::print report"), s))
			bLoggingEnabled = _ttoi (s.m_strData) ? true : false;

		if (bLoggingEnabled) {
			REPORTSTRUCT s;

			s.m_strUsername = theApp.GetUsername ();
			s.m_dtTime		= COleDateTime::GetCurrentTime ();

			FoxjetDatabase::AddPrinterReportRecord (theApp.GetDB (), FoxjetDatabase::REPORT_TIMECHANGE, s, GetPrinterID (theApp.GetDB ()));
		}

		if (CTimeChangeDlg::IsTimeChangeMessageEnabled (theApp.GetDB ()))
			MsgBox (* this, LoadString (IDS_TIMECHANGE));
	}
	*/
}

LRESULT CMainFrame::OnCreateCaptionBroadcast (WPARAM wParam, LPARAM lParam)
{
#ifdef __WINPRINTER__
	using namespace FoxjetElements;
	using namespace Foxjet3d;

	ULONG lHeadID = wParam;
	UINT nElementID = lParam;

	if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) {
		CTaskView * pView = (CTaskView *)pDoc->GetView ();
		Box::CBoxParams & params = pView->GetParams ();
		CBoundary workingHead;

		pDoc->m_box.GetWorkingHead (pView->GetParams (), workingHead);

		for (int nMessage = 0; nMessage < params.m_task.GetMessageCount (); nMessage++) {
			if (CMessage * pMsg = params.m_task.GetMessage (nMessage)) {
				if (pMsg->m_list.GetHead ().m_lID == lHeadID) {
					const CBoundary & head = pMsg->m_list.m_bounds;

					for (int i = 0; i < pMsg->m_list.GetSize (); i++) {
						if (TextElement::CTextElement * pText = DYNAMIC_DOWNCAST (TextElement::CTextElement, pMsg->m_list.GetElement (i).GetElement ())) {
							if (pText->GetLink () == nElementID)
								return 0;
						}
					}

					pDoc->OnCreateCaption ();
					pDoc->SelectAll (false);

					for (int i = 0; i < pMsg->m_list.GetSize (); i++) {
						CElement & e = pMsg->m_list.GetElement (i);
						
						if (e.GetElement ()->GetID () == nElementID) {
							Element::CPairArray v;

							v.Add (Element::CPair (&e, &pMsg->m_list, &head.m_card));
							e.SetSelected (true, pView, pMsg->m_list, head);
							pDoc->Invalidate (v);
							break;
						}
					}

					return WM_CREATE_CAPTION;
				}
			}
		}
	}
#endif //__WINPRINTER__

	return 0;
}

LRESULT CMainFrame::OnNewTask (WPARAM wParam, LPARAM lParam) // sw0867 
{
	CEvent event (false, true, _T ("WM_EDITOR_NEW_TASK"), NULL);

	event.SetEvent ();

	theApp.OnFileNew ();

	return WM_EDITOR_NEW_TASK;
}

LRESULT CMainFrame::OnOpenTask (WPARAM wParam, LPARAM lParam) // sw0867 
{
	using namespace FoxjetDatabase;

	ULONG lPrinterID = wParam;
	ULONG lTaskID = lParam;
	TASKSTRUCT task;
	LINESTRUCT line;

	VERIFY (GetTaskRecord (theApp.GetDB (), lTaskID, task));
	VERIFY (GetLineRecord (theApp.GetDB (), task.m_lLineID, line));

	CString strPath = 
		FoxjetFile::GetRootPath () +
		line.m_strName + _T ("\\") + 
		task.m_strName + _T (".") +
		FoxjetFile::GetFileExt (FoxjetFile::TASK);

	theApp.OpenDocumentFile (strPath);

	return WM_EDITOR_OPEN_TASK;
}

LRESULT CMainFrame::OnKbChange (WPARAM wParam, LPARAM lParam) 
{
	if (CView * p = theApp.GetActiveView ()) 
		p->PostMessage (WM_KEYBOARDCHANGE, wParam, lParam);

	return 0;
}

LRESULT CMainFrame::OnDeviceChange (WPARAM wParam, LPARAM lParam)
{
	switch (wParam) {
	case DBT_CONFIGCHANGECANCELED:		TRACEF (_T ("DBT_CONFIGCHANGECANCELED:		"));	break;
	case DBT_CONFIGCHANGED:				TRACEF (_T ("DBT_CONFIGCHANGED:				"));	break;
	case DBT_CUSTOMEVENT:				TRACEF (_T ("DBT_CUSTOMEVENT:				"));	break;
	case DBT_DEVICEARRIVAL:		
		{
			//TRACEF (_T ("DBT_DEVICEARRIVAL:				"));	
			if (DEV_BROADCAST_HDR * p = (DEV_BROADCAST_HDR *)lParam) {
				switch (p->dbch_devicetype) {
				case DBT_DEVTYP_DEVICEINTERFACE:	TRACEF (_T ("DBT_DEVTYP_DEVICEINTERFACE:	"));	break;
				case DBT_DEVTYP_HANDLE:				TRACEF (_T ("DBT_DEVTYP_HANDLE:				"));	break;
				case DBT_DEVTYP_OEM:				TRACEF (_T ("DBT_DEVTYP_OEM:				"));	break;
				case DBT_DEVTYP_PORT:				TRACEF (_T ("DBT_DEVTYP_PORT:				"));	break;
				case DBT_DEVTYP_VOLUME:	
					TRACEF (_T ("DBT_DEVTYP_VOLUME:				"));
					if (DEV_BROADCAST_VOLUME * pInfo = (DEV_BROADCAST_VOLUME *)p) {
						for (int i = 0; i < sizeof (DWORD) * 8; i++) {
							if (pInfo->dbcv_unitmask & (1 << i)) {
								m_vDevices.Add (CString ((TCHAR)((int)'A' + i)) + _T (":\\"));
								break;
							}
						}
					}
					break;
				}
			}
		}
		break;
	case DBT_DEVICEQUERYREMOVE:			TRACEF (_T ("DBT_DEVICEQUERYREMOVE:			"));		break;
	case DBT_DEVICEQUERYREMOVEFAILED:	TRACEF (_T ("DBT_DEVICEQUERYREMOVEFAILED:	"));		break;
	case DBT_DEVICEREMOVECOMPLETE:		
		{
			//TRACEF (_T ("DBT_DEVICEREMOVECOMPLETE:		"));	
			if (DEV_BROADCAST_HDR * p = (DEV_BROADCAST_HDR *)lParam) {
				switch (p->dbch_devicetype) {
				case DBT_DEVTYP_DEVICEINTERFACE:	TRACEF (_T ("DBT_DEVTYP_DEVICEINTERFACE:	"));	break;
				case DBT_DEVTYP_HANDLE:				TRACEF (_T ("DBT_DEVTYP_HANDLE:				"));	break;
				case DBT_DEVTYP_OEM:				TRACEF (_T ("DBT_DEVTYP_OEM:				"));	break;
				case DBT_DEVTYP_PORT:				TRACEF (_T ("DBT_DEVTYP_PORT:				"));	break;
				case DBT_DEVTYP_VOLUME:	
					TRACEF (_T ("DBT_DEVTYP_VOLUME:				"));
					if (DEV_BROADCAST_VOLUME * pInfo = (DEV_BROADCAST_VOLUME *)p) {
						for (int i = 0; i < sizeof (DWORD) * 8; i++) {
							if (pInfo->dbcv_unitmask & (1 << i)) {
								CString str = CString ((TCHAR)((int)'A' + i)) + _T (":\\");
								int nIndex = Find (m_vDevices, str, false);

								if (nIndex != -1)
									m_vDevices.RemoveAt (nIndex);

								break;
							}
						}
					}
					break;
				}
			}
		}
		break;
	case DBT_DEVICEREMOVEPENDING:		TRACEF (_T ("DBT_DEVICEREMOVEPENDING:		"));		break;
	case DBT_DEVICETYPESPECIFIC:		TRACEF (_T ("DBT_DEVICETYPESPECIFIC:		"));		break;
	case DBT_DEVNODES_CHANGED:			/* TRACEF (_T ("DBT_DEVNODES_CHANGED:          "));	*/	break;
	case DBT_QUERYCHANGECONFIG:			TRACEF (_T ("DBT_QUERYCHANGECONFIG:			"));		break;
	case DBT_USERDEFINED:				TRACEF (_T ("DBT_USERDEFINED:				"));		break;
	}

	return TRUE;
}

void CMainFrame::OnTimer(UINT nIDEvent)
{
	if (nIDEvent == TIMER_DATABASE_MONITOR) {
		//TRACEF ("TIMER_DATABASE_MONITOR");
		COdbcDatabase::PurgeDsnCache (theApp.GetDSN ());
		return;
	}

	CMDIFrameWnd::OnTimer (nIDEvent);	
}