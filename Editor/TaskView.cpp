// TaskView.cpp : implementation file
//

#include "stdafx.h"
#include "mkdraw.h"
#include "TaskView.h"
#include "HeadView.h"
#include "TaskDoc.h"
#include "Color.h"
#include "Database.h"
#include "List.h"
#include "Extern.h"
#include "math.h"
#include "Debug.h"
#include "Coord.h"
#include "DxResDlg.h"
#include "HeadView.h"
#include "MainFrm.h"
#include "PanelDlg.h"
#include "ElLstDlg.h"
#include "Element.h"
#include "Utils.h"
#include "TemplExt.h"
#include "WinMsg.h"
#include "Parse.h"
#include "ximage.h"
#include "UserElement.h"
#include "BitmapElement.h"
#include "UserElementDlg.h"
#include "StrobeDlg.h"
#include "DbWizard.h"
#include "DbWizardDsnDlg.h"
#include "DbWizardFieldDlg.h"
#include "DbWizardKeyFieldDlg.h"
#include "DbWizardDbStartDlg.h"
#include "DbWizardFinishDlg.h"
#include "DatabaseStartDlg.h"
#include "BarcodeElement.h"
#include "SerialWizardPortsDlg.h"
#include "..\..\Control\Host.h"
#include "Parse.h"
#include "DC.h"
#include <stdexcept>

#ifdef __WINPRINTER__
	#include "DatabaseElement.h"
	#include "SerialElement.h"
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace EditorGlobals;
using namespace Foxjet3d;
using namespace FoxjetDocument;
using namespace Color;
using namespace Box;

using namespace TaskView;
using namespace TaskDoc;
using namespace Element;
using namespace Panel;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const UINT WM_SETUNFOLDED = ::RegisterWindowMessage (_T ("WM_SETUNFOLDED"));

/////////////////////////////////////////////////////////////////////////////

static CPair GetMinID (CPairArray & v)
{
	UINT nMin = UINT_MAX;

	for (int i = 0; i < v.GetSize (); i++)
		if (CElement * p = v [i].m_pElement)
			nMin = min (nMin, p->GetElement ()->GetID ());

	for (int i = 0; i < v.GetSize (); i++)
		if (CElement * p = v [i].m_pElement)
			if (p->GetElement ()->GetID () == nMin)
				return v [i];

	return CPair (NULL, NULL, NULL);
}

static CPair GetMaxID (CPairArray & v)
{
	UINT nMax = 0;

	for (int i = 0; i < v.GetSize (); i++)
		if (CElement * p = v [i].m_pElement)
			nMax = max (nMax, p->GetElement ()->GetID ());

	for (int i = 0; i < v.GetSize (); i++)
		if (CElement * p = v [i].m_pElement)
			if (p->GetElement ()->GetID () == nMax)
				return v [i];

	return CPair (NULL, NULL, NULL);
}

/////////////////////////////////////////////////////////////////////////////
// CTaskView

// map the hardcoded command (menu/toolbar) ids to
// the dynamic element types
struct 
{
	UINT m_nCmdID;
	int m_nClassID;
} static MENUMAP [] =
{
	{ ID_ELEMENT_01,	-1 },
	{ ID_ELEMENT_02,	-1 },
	{ ID_ELEMENT_03,	-1 },
	{ ID_ELEMENT_04,	-1 },
	{ ID_ELEMENT_05,	-1 },
	{ ID_ELEMENT_06,	-1 },
	{ ID_ELEMENT_07,	-1 },
	{ ID_ELEMENT_08,	-1 },
	{ ID_ELEMENT_09,	-1 },
	{ ID_ELEMENT_10,	-1 },
	{ ID_ELEMENT_11,	-1 },
	{ ID_ELEMENT_12,	-1 },
	{ ID_ELEMENT_13,	-1 },
	{ ID_ELEMENT_14,	-1 },
	{ ID_ELEMENT_15,	-1 },
	{ ID_ELEMENT_16,	-1 },
	{ ID_ELEMENT_17,	-1 },
	{ ID_ELEMENT_18,	-1 },
	{ ID_ELEMENT_19,	-1 },
	{ ID_ELEMENT_20,	-1 },
	{ ID_ELEMENT_21,	-1 },
	{ ID_ELEMENT_22,	-1 },
	{ ID_ELEMENT_23,	-1 },
	{ ID_ELEMENT_24,	-1 },
	{ ID_ELEMENT_25,	-1 },
	{ ID_ELEMENT_26,	-1 },
	{ ID_ELEMENT_27,	-1 },
	{ ID_ELEMENT_28,	-1 },
	{ ID_ELEMENT_29,	-1 },
	{ ID_ELEMENT_30,	-1 },
	{ -1,				-1 },
};

const int CTaskView::m_nMinorMove = 50;		// in 1000ths of an inch
const int CTaskView::m_nMajorMove = 250;	// in 1000ths of an inch
const int CTaskView::m_nTickWidth [3] = { 5, 10, 15 };

IMPLEMENT_DYNCREATE(CTaskView, CBaseView)

CTaskView::CTaskView()
:	m_pLastParams (NULL),
	m_sizeRuler (30, 30),
	m_bShowHRuler (true),
	m_bShowVRuler (true),
	m_bTrackingPopupMenu (false),
	m_bTrackingElement (false),
	m_bInitUpdate (false),
	m_hAccel (NULL),
	m_pPreview (NULL),
	m_bUnfolded (false),
	m_ptDrag (NULL),
	m_ptScroll (NULL),
	m_pHit (NULL),
	m_tmKbChange (CTime::GetCurrentTime	() - CTimeSpan (1)),
	m_sizePref (0, 0),
	CBaseView ()
{
	m_strRegSection.Format (_T ("Settings\\%s\\CBoxParams"), a2w (GetRuntimeClass ()->m_lpszClassName));

	for (int i = 0; MENUMAP [i].m_nCmdID != -1; i++) 
		if (i < GetElementTypeCount ())
			MENUMAP [i].m_nClassID = i;

	CString str = m_strRegSection;

	str.Replace (_T ("CBoxParams"), _T ("Rulers"));

	m_bShowHRuler = theApp.GetProfileInt (str, _T ("Horizontal"), 1) ? true : false;
	m_bShowVRuler = theApp.GetProfileInt (str, _T ("Vertical"), 1) ? true : false;
}

CTaskView::~CTaskView()
{
	if (m_pLastParams) {
		delete m_pLastParams;
		m_pLastParams = NULL;
	}

	if (m_pPreview) {
		delete m_pPreview;
		m_pPreview = NULL;
	}
}

BEGIN_MESSAGE_MAP(CTaskView, CBaseView)
	//{{AFX_MSG_MAP(CTaskView)
	ON_WM_ERASEBKGND()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_WM_LBUTTONUP()
	ON_WM_CLOSE()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_SETCURSOR()
	ON_WM_RBUTTONDOWN()
	ON_WM_INITMENU()
	ON_WM_INITMENUPOPUP()
	ON_WM_NCCALCSIZE()
	ON_WM_NCPAINT()
	ON_COMMAND(ID_DEBUGGINGSTUFF_STROBE, OnDebuggingstuffStrobe)
	ON_COMMAND(ID_VIEW_RULERS_HORIZONAL, OnViewRulersHorizonal)
	ON_UPDATE_COMMAND_UI(ID_VIEW_RULERS_HORIZONAL, OnUpdateViewRulersHorizonal)
	ON_COMMAND(ID_VIEW_RULERS_VERTICAL, OnViewRulersVertical)
	ON_UPDATE_COMMAND_UI(ID_VIEW_RULERS_VERTICAL, OnUpdateViewRulersVertical)
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP

	ON_WM_PAINT()

	ON_COMMAND(ID_ELEMENT_EDIT, OnElementEdit)
	ON_COMMAND(ID_ELEMENTS_DELETE, OnElementsDelete)
	ON_COMMAND(ID_ELEMENTS_LIST, OnElementsList)
	ON_COMMAND(ID_PROPERTIES_MOVABLE, OnPropertiesMovable)
	ON_COMMAND(ID_PROPERTIES_RESIZABLE, OnPropertiesResizable)
	ON_COMMAND(ID_PROPERTIES_FLIPH, OnPropertiesFliph)
	ON_COMMAND(ID_PROPERTIES_FLIPV, OnPropertiesFlipv)
	ON_COMMAND(ID_PROPERTIES_INVERSE, OnPropertiesInverse)

	ON_UPDATE_COMMAND_UI(ID_ELEMENT_EDIT, OnUpdateElementsEdit)
	ON_UPDATE_COMMAND_UI(ID_ELEMENTS_DELETE, OnUpdateElementsDelete)

	ON_UPDATE_COMMAND_UI(ID_PROPERTIES_SENDTOFRONT, OnUpdateElementsEdit)
	ON_UPDATE_COMMAND_UI(ID_PROPERTIES_SENDTOBACK, OnUpdateElementsEdit)
	ON_UPDATE_COMMAND_UI(ID_PROPERTIES_BRINGFORWARD, OnUpdateElementsEdit)
	ON_UPDATE_COMMAND_UI(ID_PROPERTIES_SENDBACKWARD, OnUpdateElementsEdit)

	ON_COMMAND_RANGE(ID_ELEMENT_01, ID_ELEMENT_30, OnApiElementCommand)
	ON_UPDATE_COMMAND_UI_RANGE(ID_ELEMENT_01, ID_ELEMENT_30, OnUpdateApiElementCommand)
	/*
	note: for whatever reason, ON_UPDATE_COMMAND_UI doesn't work 
	with a context menu, so these have to do the same thing as the 
	InitMenu (CMenu *) fct.
	*/

	ON_COMMAND(ID_JUMPDOWN, OnJumpdown)
	ON_COMMAND(ID_JUMPLEFT, OnJumpleft)
	ON_COMMAND(ID_JUMPRIGHT, OnJumpright)
	ON_COMMAND(ID_JUMPUP, OnJumpup)
	ON_COMMAND(ID_PAGEDOWN, OnPagedown)
	ON_COMMAND(ID_PAGELEFT, OnPageleft)
	ON_COMMAND(ID_PAGERIGHT, OnPageright)
	ON_COMMAND(ID_PAGEUP, OnPageup)
	ON_COMMAND(ID_HOME, OnHome)
	ON_COMMAND(ID_END, OnEnd)
	ON_COMMAND(ID_BOTTOM, OnBottom)
	ON_COMMAND(ID_TOP, OnTop)
	ON_COMMAND(ID_MOVEDOWN, OnMovedown)
	ON_COMMAND(ID_MOVELEFT, OnMoveleft)
	ON_COMMAND(ID_MOVERIGHT, OnMoveright)
	ON_COMMAND(ID_MOVEUP, OnMoveup)

	ON_COMMAND(ID_MOVECROSSHAIRSUP,		MoveCrosshairsUp)
	ON_COMMAND(ID_MOVECROSSHAIRSDOWN,	MoveCrosshairsDown)
	ON_COMMAND(ID_MOVECROSSHAIRSLEFT,	MoveCrosshairsLeft)
	ON_COMMAND(ID_MOVECROSSHAIRSRIGHT,	MoveCrosshairsRight)

	ON_COMMAND(ID_JUMPCROSSHAIRSUP,		JumpCrosshairsUp)
	ON_COMMAND(ID_JUMPCROSSHAIRSDOWN,	JumpCrosshairsDown)
	ON_COMMAND(ID_JUMPCROSSHAIRSLEFT,	JumpCrosshairsLeft)
	ON_COMMAND(ID_JUMPCROSSHAIRSRIGHT,	JumpCrosshairsRight)

	ON_UPDATE_COMMAND_UI(ID_STATUS_PHOTO, OnUpdateStatusCrosshairs)
	ON_UPDATE_COMMAND_UI (ID_STATUS_LOCATION, OnUpdateStatusLocation)
	ON_COMMAND_RANGE(ID_EDIT_BRINGTOFRONT1, ID_EDIT_BRINGTOFRONT6, OnBringToFront)
	ON_COMMAND_RANGE(ID_PANEL_PROPERTIES1, ID_PANEL_PROPERTIES6, OnPanelProperties)

	ON_COMMAND(ID_VIEW_ZOOM_CUSTOM, OnViewZoomCustom)

	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	ON_COMMAND(ID_NEXT_ELEMENT, OnElementNext)
	ON_COMMAND(ID_PREV_ELEMENT, OnElementPrev)

	ON_REGISTERED_MESSAGE (WM_EDITOR_REDRAW, OnRedraw)
	ON_REGISTERED_MESSAGE (WM_KEYBOARDCHANGE, OnKbChange)
	ON_REGISTERED_MESSAGE (WM_SETUNFOLDED, OnSetUnfolded)
END_MESSAGE_MAP()

CTaskDoc * CTaskView::GetDocument () const
{
	CDocument * pDoc = CBaseView::GetDocument ();

	if (pDoc) {
		ASSERT (pDoc->IsKindOf (RUNTIME_CLASS (CTaskDoc)));
	}

	return (CTaskDoc *)pDoc;
}

/////////////////////////////////////////////////////////////////////////////
// CTaskView drawing

void CTaskView::OnDraw (CDC * pDC)
{
	CRect rc;
	CTaskDoc * pDoc = GetDocument ();
	const CTaskMembers & doc = pDoc->GetMembers ();
	CBoxParams & p = GetParams ();
	bool bUpdate = false;
	
	if (!m_pLastParams) {
		m_pLastParams = new CBoxParams (p);
		bUpdate = true;
	}
	else {
		p.m_lChange = Compare (p, * m_pLastParams);
		bUpdate = p.m_lChange ? true : false;

		if (bUpdate) 
			(* m_pLastParams) = p;
	}

	//SetScrollSizes (MM_TEXT, GetPageSize ());

	if (bUpdate && doc.m_box.IsCreated ()) {
		Element::CPairArray v = pDoc->m_box.GetElements (p.m_nPanel, p); 
		m_3d.Update ();
		pDoc->Invalidate (v, this);
		ScrollToCrosshairs ();
	}

	CPoint pt = GetScrollPosition ();

	pDC->SetWindowOrg (pt);
	pDC->SetViewportOrg (pt);

	GetClientRect (rc);
	rc += pt;

	m_3d.Draw (pDC, rc, GetZoom ());
}

BOOL CTaskView::OnEraseBkgnd(CDC* pDC) 
{
/*
	CRect rc;

	GetClientRect (&rc);
	::BitBlt (* pDC, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);

	return TRUE;
*/
    CRect rc;
    CBrush * pOld = pDC->SelectObject (&CBrush (Color::rgbWhite));

    pDC->GetClipBox (&rc);
    pDC->PatBlt (rc.left, rc.top, rc.Width(), rc.Height(), PATCOPY);
    pDC->SelectObject (pOld); 

    return TRUE;
}
/////////////////////////////////////////////////////////////////////////////
// CTaskView diagnostics

#ifdef _DEBUG
void CTaskView::AssertValid() const
{
	CBaseView::AssertValid();
}

void CTaskView::Dump(CDumpContext& dc) const
{
	CBaseView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTaskView message handlers

CBitmap * CTaskView::CreateBitmap(CDC &dc)
{
	ASSERT (0);
	return NULL;
}


CSize CTaskView::CalcMaxLogicalSize () const
{
	const CTaskMembers & doc = GetDocument ()->GetMembers ();
	const CPanel * pPanels [6];
	int nSize = 0;
	CBoundary head;

	GetParams ().GetActiveHead (doc.m_box, head);

	if (doc.m_box.GetPanels (pPanels)) {
		for (int i = 0; i < 6; i++) {
			CSize size = pPanels [i]->GetSize (head);
			nSize = max (max (size.cx, size.cy), nSize);
		}
	}

	return CSize (nSize, nSize);
}

void CTaskView::OnInitialUpdate() 
{
	TRACEF (_T ("CTaskView::OnInitialUpdate"));
	GetDocument ()->UpdateAllViews (this);
	CBaseView::OnInitialUpdate();
	m_bInitUpdate = true;
	SetScrollSizes (MM_TEXT, GetPageSize ());
	TRACEF (_T ("CTaskView::OnInitialUpdate exiting"));
}

void CTaskView::OnUpdateStatusCrosshairs(CCmdUI *pCmdUI)
{
	CString str;
	CBoxParams & p = GetParams ();
	static HEADSTRUCT head = FoxjetDatabase::GetDefaultHead ();

	if (head.m_lID != p.m_lHeadID) {
		CBoundary b;

		p.GetActiveHead (GetDocument ()->m_box, b);
		head = b;
	}

	if (p.m_crosshairs.m_bShow) {
		UNITS units = GetDocument ()->GetUnits ();
		double dZoom = GetZoom ();
		CCoord c (p.m_crosshairs.m_pt, units, head, dZoom); 

		str = c.Format ();
	}

	pCmdUI->SetText (str);
}

void CTaskView::OnUpdateStatusOrient(CCmdUI *pCmdUI)
{
	CString str;
	const CTaskMembers & doc = GetDocument ()->GetMembers ();
	CBoxParams & p = GetParams ();
	UINT nPanel = p.m_nPanel;
	UINT nFace = doc.m_box.GetTransformFace (); 
	ORIENTATION orient = doc.m_box.GetTransformOrientation ();
	CString strRot [] = 
	{
		_T ("0"),
		_T ("90"),
		_T ("180"),
		_T ("270"),
	};

	str.Format (_T ("%d [%d, %s]"), nPanel, nFace, strRot [orient]);
	pCmdUI->SetText (str);
}

int CTaskView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CBaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CTaskDoc * pDoc = GetDocument ();
	const CTaskMembers & doc = pDoc->GetMembers ();
	CBoxParams * pParams = new CBoxParams (m_3d, pDoc->m_task);
	int nRes = theApp.GetProfileInt (_T ("Settings\\DirectX"), _T ("res"), 7);

	VERIFY (m_3d.SetResolution (nRes));
	pParams->LoadSettings (theApp, m_strRegSection);

	if (!m_3d.Create (this, pParams)) {
		TRACEF ("Failed to create 3d");
		ASSERT (0);
		return -1;
	}

	return 0;
}

void CTaskView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	CTaskDoc * pDoc = GetDocument ();
	CBox & box = pDoc->m_box;
	CTask & task = pDoc->m_task;
	CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, pSender);

	if (pSender && pView != this) { // multiple frame wnd's for this doc
		ULONG lPanelSent = pView->GetActivePanel ();
		ULONG lPanelThis = GetActivePanel ();

		if (lPanelSent != lPanelThis) // only update if on the same panel
			return;
	}
	
	CBaseView::OnUpdate (pSender, lHint, pHint);
	NCINVALIDATE (this);

	if (!box.IsCreated ()) {
		CBoxParams & p = GetParams ();
		CTaskMembers & m = * (CTaskMembers *)(LPVOID)&pDoc->GetMembers ();

		box.Create (m_3d);
		box.LoadPanels (theApp.GetDB (), task.m_lLineID);
		GetDocument ()->UpdateAllViews (this, lHint, pHint);
	}

	if (pHint) {
		using namespace TaskDoc;
		
		//if (TaskDoc::CHint * pTask = NESTED_DYNAMIC_DOWNCAST (TaskDoc, CHint, pHint))
		if (TaskDoc::CHint * pTask = DYNAMIC_DOWNCAST (TaskDoc::CHint, pHint))
		{
			if (pTask->GetType ())
				Invalidate ();
		}
	}
	else {
		Invalidate ();

		if (!m_3d.ContainsObject (&box)) 
			m_3d.AddObject (&box); 

		box.SetPos (m_sizeRuler);
	}

	pDoc->UpdateUIStructs ();
}

bool CTaskView::SetActivePanel(UINT nPanel)
{
	CBoxParams & p = GetParams ();

	if (p.m_nPanel != nPanel &&
		nPanel >= 1 && nPanel <= 6) 
	{
		p.m_nPanel = nPanel;
		return true;
	}

	return false;
}

UINT CTaskView::GetActivePanel() const
{
	return GetParams ().m_nPanel;
}

bool CTaskView::SetActiveHead(ULONG lHeadID, bool bScrollToCrosshairs)
{
	using namespace FoxjetDatabase;

	CTaskDoc * pDoc = GetDocument ();
	const CTaskMembers & doc = GetDocument ()->GetMembers ();
	CBoxParams & p = GetParams ();

	if (p.m_lHeadID != lHeadID) {
		ULONG lPanelID = GetActivePanel ();
		CBoundaryArray vHeads;

		CPanel::GetHeads (pDoc->m_box, lPanelID, vHeads);
		p.m_lHeadID = lHeadID;

		if (CTask::Find (vHeads, lHeadID) == -1) {  // changing panels
			CBoundary head;
			const CPanel * pPanel = NULL;
			
			VERIFY (p.GetHead (pDoc->m_box, lHeadID, head));
			VERIFY (doc.m_box.GetPanelByID (head.GetPanelID (), &pPanel));

			SetActivePanel (pPanel->GetMembers ().m_nNumber);

			if (bScrollToCrosshairs)
				ScrollToCrosshairs ();

			RedrawWindow ();

			if (HeadView::CHeadView * pHead = (HeadView::CHeadView *)pDoc->GetView (TaskDoc::HEAD)) 
				VERIFY (pHead->SetSelection (HeadView::CItem (p.m_lHeadID)));

		}

		return true;
	}

	return false;
}

ULONG CTaskView::GetActiveHead() const
{
	CBoxParams & p = GetParams ();
	ULONG lHeadID = p.m_lHeadID;
	CTaskDoc & doc = * GetDocument ();
	const CPanel * pPanel = NULL;

	if (doc.m_box.GetPanel (GetActivePanel (), &pPanel)) {
		if (CTask::Find (const_cast <CBoundaryArray &> (pPanel->m_vBounds), lHeadID) == -1) {
			lHeadID = -1;
		
			if (pPanel->m_vBounds.GetSize ()) {
				lHeadID = pPanel->m_vBounds [0].GetID ();
				const_cast <CTaskView *> (this)->SetActiveHead (lHeadID);
				doc.m_box.Invalidate (-1);
			}
		}
	}

	return lHeadID;
}

bool CTaskView::SetZoom (double dZoom, bool bAutoScroll)
{
	bool bResult = false;

	if (m_3d.m_pParams) {
		CBoxParams & p = GetParams ();
		CBoundary head;

		if (p.GetActiveHead (GetDocument ()->m_box, head)) {
			if (CBaseView::SetZoom (dZoom, bAutoScroll)) {
				double dOldZoom = m_3d.m_pParams->m_rZoom;
				UNITS units = GetDocument ()->GetUnits ();

				CCoord cOld (p.m_crosshairs.m_pt, units, head, dOldZoom); 
				CPoint ptOld = CCoord::CoordToPoint (cOld, units, head);
				
				CCoord cNew (ptOld, units, head, 1.0 / dZoom); 
				CPoint ptNew = CCoord::CoordToPoint (cNew, units, head);

				p.m_crosshairs.m_pt = ptNew;

				m_3d.m_pParams->m_rZoom = dZoom;
				m_3d.m_pParams->SaveSettings (theApp, m_strRegSection);

				if (bAutoScroll)
					ScrollToCrosshairs ();

				SetScrollSizes (MM_TEXT, GetPageSize ());
				bResult =  true;
			}
		}
	}

	return bResult;
}

void CTaskView::ScrollToCrosshairs ()
{
	CTaskDoc & doc = * GetDocument ();
	CBoxParams & p = GetParams ();
	CBoundary head;

	if (p.GetActiveHead (doc.m_box, head)) {
		// center scroll pos based on crosshair position
		const CRect rcHead = doc.m_box.GetHeadRect (head, p);
		const CPoint ptScroll = GetScrollPosition ();
		const CPoint ptOffset = rcHead.TopLeft ();
		const CPoint ptCrosshairs = CPoint (p.m_crosshairs.m_pt) + ptOffset;
		CRect rcClient;

		GetClientRect (rcClient);
		CPoint ptDiff (
			ptCrosshairs.x - (rcClient.Width () / 2), 
			ptCrosshairs.y - (rcClient.Height () / 2));
		ScrollToPosition (ptDiff);
		NCINVALIDATE (this);
	}
}

bool CTaskView::GetCrosshairsVisible () const
{
	CBoxParams & p = GetParams ();
	CBoundary head;
	CTaskDoc & doc = * GetDocument ();

	if (p.GetActiveHead (doc.m_box, head)) {
		const CRect rcHead = doc.m_box.GetHeadRect (head, p);
		const CPoint ptScroll = GetScrollPosition ();
		const CPoint ptOffset = rcHead.TopLeft ();
		const CPoint ptCrosshairs = CPoint (p.m_crosshairs.m_pt) + ptOffset;
		CRect rcClient;

		GetClientRect (rcClient);
		rcClient += ptScroll;

		return rcClient.PtInRect (ptCrosshairs) ? true : false;
	}

	return false;
}

void CTaskView::MoveCrosshairsUp ()
{
	MoveCrosshairs (CPoint (0, -m_nMinorMove));
}

void CTaskView::MoveCrosshairsDown ()
{
	MoveCrosshairs (CPoint (0, m_nMinorMove));
}

void CTaskView::MoveCrosshairsLeft ()
{
	MoveCrosshairs (CPoint (-m_nMinorMove, 0));
}

void CTaskView::MoveCrosshairsRight ()
{
	MoveCrosshairs (CPoint (m_nMinorMove, 0));
}

void CTaskView::JumpCrosshairsUp ()
{
	MoveCrosshairs (CPoint (0, -m_nMajorMove));
}

void CTaskView::JumpCrosshairsDown ()
{
	MoveCrosshairs (CPoint (0, m_nMajorMove));
}

void CTaskView::JumpCrosshairsLeft ()
{
	MoveCrosshairs (CPoint (-m_nMajorMove, 0));
}

void CTaskView::JumpCrosshairsRight ()
{
	MoveCrosshairs (CPoint (m_nMajorMove, 0));
}

void CTaskView::MoveCrosshairs (const CPoint & ptOffset)
{
	CBoxParams & p = GetParams ();
	CBoundary head;
	CTaskDoc & doc = * GetDocument ();

	if (p.GetActiveHead (doc.m_box, head)) {
		CRect rcHead = doc.m_box.GetHeadRect (head, p);
		CPoint pt = CPoint (p.m_crosshairs.m_pt) + CBaseElement::ThousandthsToLogical (ptOffset, head);

		rcHead -= rcHead.TopLeft ();

		if (rcHead.PtInRect (pt)) {
			CRect rcInvalid (0, 0, 0, 0);
			
			rcInvalid.UnionRect (rcInvalid, InvalidateCrosshairs ());
			p.m_crosshairs.m_pt = pt;
			rcInvalid.UnionRect (rcInvalid, InvalidateCrosshairs ());

			InvalidateRect (rcInvalid);

			if (!GetCrosshairsVisible ())
				ScrollToCrosshairs ();
		}
	}
}

double CTaskView::GetZoom () const
{
	if (m_3d.m_pParams) 
		return m_3d.m_pParams->m_rZoom;

	return 1.0;
}

void CTaskView::OnDestroy() 
{
	bool bDestroy = m_3d.Destroy ();	
	ASSERT (bDestroy);
	CBaseView::OnDestroy();
}

void CTaskView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	using namespace HeadView;

	CBaseView::OnLButtonDblClk(nFlags, point);
	
	CTaskDoc * pDoc = GetDocument ();
	const CTaskMembers & doc = pDoc->GetMembers ();
	CBoxParams & p = GetParams ();
	HeadView::CHeadView * pHead = (HeadView::CHeadView *)pDoc->GetView (TaskDoc::HEAD);
	CPoint pt = point + GetScrollPosition ();
	Box::CHit hit = doc.m_box.HitTest (pt, p);

	if (hit.m_nPanel >= 1 && hit.m_nPanel <= 6) {
		if (hit.m_nPanel != p.m_nPanel) {
			pHead->SetSelection (CItem (hit.m_nPanel));
			p.m_nPanel = hit.m_nPanel;
		}
	
		// only change selection if ONE head was hit
		if (hit.m_vHeadID.GetSize () == 1) {
			ULONG lHeadID = hit.m_vHeadID [0];

			pHead->SetSelection (CItem (lHeadID, HeadView::HEAD));
			p.m_nPanel = hit.m_nPanel;
			p.m_lHeadID = lHeadID;
		}

		if (hit.m_element.m_pElement) 
			OnElementEdit ();
	}
}

Foxjet3d::Box::CBoxParams & CTaskView::GetParams() const
{
 	ASSERT (m_3d.m_pParams);
	ASSERT (m_3d.m_pParams->IsKindOf (RUNTIME_CLASS (CBoxParams)));
	return * (CBoxParams *)m_3d.m_pParams;
}

BOOL CTaskView::IsSelected(const CObject* pDocItem) const
{
// TODO: check whether the item passed to it is currently selected.
	return CBaseView::IsSelected(pDocItem);
}

void CTaskView::OnSetFocus(CWnd* pOldWnd) 
{
    CBaseView::OnSetFocus(pOldWnd);
}

void CTaskView::OnSize(UINT nType, int cx, int cy) 
{
	CTimeSpan tm = CTime::GetCurrentTime () - m_tmKbChange;

	EnableScrollBarCtrl (SB_BOTH, FALSE);
	CBaseView::OnSize(nType, cx, cy);
// TODO:	inform an OLE embedded item that it needs to change its rectangle 
//			to reflect the change in size of its containing view. 

	if (tm.GetTotalSeconds () > 2) {
		if (CWnd * p = ::AfxGetMainWnd ()) { 
			CRect rc (0, 0, 0, 0);

			p->GetWindowRect (rc);

			if (rc.Width () > 0 && rc.Height () > 0) {
				m_sizePref = rc.Size ();

				//#ifdef _DEBUG
				//{ 
				//	CString str; 

				//	str.Format (_T ("OnSize: (%d, %d) "), m_sizePref.cx, m_sizePref.cy); 

				//	if (nType == SIZE_MAXIMIZED)	str += _T ("SIZE_MAXIMIZED");
				//	if (nType == SIZE_MINIMIZED)	str += _T ("SIZE_MINIMIZED");
				//	if (nType == SIZE_RESTORED)		str += _T ("SIZE_RESTORED");
				//	if (nType == SIZE_MAXHIDE)		str += _T ("SIZE_MAXHIDE");
				//	if (nType == SIZE_MAXSHOW)		str += _T ("SIZE_MAXSHOW");
				//	
				//	TRACEF (str); 
				//}
				//#endif //_DEBUG
			}
		}
	}
}

void CTaskView::OnClose() 
{
	CBaseView::OnClose();
}

void CTaskView::SaveState()
{
	CBoxParams & p = GetParams ();
	p.SaveSettings (theApp, m_strRegSection);
}

BOOL CTaskView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	BOOL bResult = CBaseView::OnScrollBy(sizeScroll, bDoScroll);

	NCINVALIDATE (this);

	return bResult;
}

CSize CTaskView::GetPageSize () const
{
	const CTaskDoc * pDoc = GetDocument ();
	CSize size (100, 100);
	
	if (CBoxParams * p = DYNAMIC_DOWNCAST (CBoxParams, m_3d.m_pParams)) 
		size = pDoc->m_box.CalcViewSize (* p) + CSize (100, 100);

	return size;
}

void CTaskView::OnMouseMove(UINT nFlags, CPoint point) 
{
	CBaseView::OnMouseMove(nFlags, point);

	if (m_ptDrag && m_ptScroll) {
		CPoint pt = point - (* m_ptDrag);
		CPoint ptScroll = (* m_ptScroll) - pt;

		ptScroll.x = BindTo ((int)ptScroll.x, 0, GetScrollLimit (SB_HORZ));
		ptScroll.y = BindTo ((int)ptScroll.y, 0, GetScrollLimit (SB_VERT));

		if (ptScroll.x < 0 || ptScroll.x > GetScrollLimit (SB_HORZ) || ptScroll.y < 0 || ptScroll.y > GetScrollLimit (SB_VERT)) {
			TRACEF ("out of bounds");
		}
		else {
			if (GetScrollPosition () != ptScroll) {
				ScrollToPosition (ptScroll);
				NCINVALIDATE (this); 
				REPAINT (this);
			}
		}
	}
}

void CTaskView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	CBoxParams & p = GetParams ();
	CTaskDoc & doc = * GetDocument ();
	CPoint ptOffset = GetOffset ();
	CPoint pt = point + CPoint (ptOffset.x, -ptOffset.y);
	CPoint ptScreen;
	Box::CHit hit = doc->m_box.HitTest (pt, p);
	CMenu mnu, * pMenu = NULL;
	UINT nMenuID = IDM_TASKCONTEXT;
	int nDefItem = 0;

	CBaseView::OnRButtonUp(nFlags, point);

	if (hit.m_element.m_pElement) {
		VERIFY (mnu.LoadMenu (IDM_ELEMENTCONTEXT));
		pMenu = mnu.GetSubMenu (0);

		#ifdef __WINPRINTER__
		if (hit.m_element.m_pElement->GetElement ()->GetClassID () == BARCODE) {
			pMenu->InsertMenu (0, MF_BYPOSITION | MF_SEPARATOR); 
			pMenu->InsertMenu (0, MF_BYPOSITION, ID_CREATE_CAPTION, LoadString (IDS_CREATE_CAPTION)); 
			nDefItem = 2;
		}
		#endif //__WINPRINTER__
	}
	else {
		VERIFY (mnu.LoadMenu (nMenuID));
		const UINT nID [6][2] = 
		{
			{ ID_EDIT_BRINGTOFRONT1, ID_PANEL_PROPERTIES1 },
			{ ID_EDIT_BRINGTOFRONT2, ID_PANEL_PROPERTIES2 },
			{ ID_EDIT_BRINGTOFRONT3, ID_PANEL_PROPERTIES3 },
			{ ID_EDIT_BRINGTOFRONT4, ID_PANEL_PROPERTIES4 },
			{ ID_EDIT_BRINGTOFRONT5, ID_PANEL_PROPERTIES5 },
			{ ID_EDIT_BRINGTOFRONT6, ID_PANEL_PROPERTIES6 },
		};

		pMenu = mnu.GetSubMenu (0);
		InitElementMenu (pMenu);

		// remove all menu items not related to the hit panel
		for (UINT i = 0; i < 6; i++) {
			if ((i + 1) != hit.m_nPanel || p.m_nPanel == hit.m_nPanel) 
				pMenu->DeleteMenu (nID [i][0], MF_BYCOMMAND);

			if ((i + 1) != hit.m_nPanel)
				pMenu->DeleteMenu (nID [i][1], MF_BYCOMMAND);
		}

		if (hit.m_vHeadID.GetSize () == 0) 
			pMenu->DeleteMenu (ID_HEAD_PROPERTIES, MF_BYCOMMAND);

		if (hit.m_nPanel == -1) 
			pMenu->DeleteMenu (ID_PANEL_PROPERTIES, MF_BYCOMMAND);

	}

	::GetCursorPos (&ptScreen);
	ASSERT (pMenu);
	InitContextMenu (pMenu);
	pMenu->SetDefaultItem (nDefItem, TRUE);
	m_bTrackingPopupMenu = true;
	pMenu->TrackPopupMenu (
		TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON,
		ptScreen.x, ptScreen.y, this);
	m_bTrackingPopupMenu = false;
}

void CTaskView::OnBringToFront (UINT nCommandID)
{
	const UINT nID [6] = 
	{
		ID_EDIT_BRINGTOFRONT1, 
		ID_EDIT_BRINGTOFRONT2, 
		ID_EDIT_BRINGTOFRONT3, 
		ID_EDIT_BRINGTOFRONT4, 
		ID_EDIT_BRINGTOFRONT5, 
		ID_EDIT_BRINGTOFRONT6, 
	};

	for (int i = 0; i < 6; i++) {
		if (nID [i] == nCommandID) {
			CBoxParams & p = GetParams ();
			p.m_nPanel = i + 1;

			if (HeadView::CHeadView * pView = (HeadView::CHeadView *)GetDocument ()->GetView (TaskDoc::HEAD)) 
				VERIFY (pView->SetSelection (HeadView::CItem (p.m_nPanel)));

			RedrawWindow ();
		}
	}
}

void CTaskView::OnPanelProperties (UINT nCommandID)
{
	const UINT nID [6] = 
	{
		ID_PANEL_PROPERTIES1, 
		ID_PANEL_PROPERTIES2, 
		ID_PANEL_PROPERTIES3, 
		ID_PANEL_PROPERTIES4, 
		ID_PANEL_PROPERTIES5, 
		ID_PANEL_PROPERTIES6, 
	};

	for (int i = 0; i < 6; i++) {
		if (nID [i] == nCommandID) {
			using namespace Foxjet3d;

			int nPanel = i + 1;
			const PANELSTRUCT * pPanel = NULL;
			const CTaskMembers & doc = GetDocument ()->GetMembers ();

			if (doc.m_box.GetPanel (nPanel, &pPanel)) {
				CPanelDlg dlg (database, * pPanel, this);
				dlg.m_bReadOnly = true;
				dlg.DoModal ();
			}
		}
	}
}

Foxjet3d::Box::CHit CTaskView::DoMouseSelect(UINT nFlags, const Foxjet3d::Box::CHit & hit, const CPoint & pt)
{
	// NOTE: if invalidation is performed in this function, but sure to test with scroll

	using namespace Box::BoxParams;

	CTaskDoc & doc = * GetDocument ();
	CBoxParams & params = GetParams ();
	CRect rcInvalid (0, 0, 0, 0);

	CPairArray v = GetSelected (0);
	bool bCtrl = (nFlags & MK_CONTROL) ? true : false;
	bool bShift = (nFlags & MK_SHIFT) ? true : false;
	bool bDeselect = false;
	bool bNcInvalidate = false;
	CHit htResult (hit);
	const CPoint ptScroll = GetScrollPosition ();
	const double dZoom = GetZoom ();
	CBoundary workingHead;

	doc.GetBox ().GetWorkingHead (params, workingHead);

	if (!hit.m_element.m_pElement && v.GetSize () > 0 && !bCtrl)
		bDeselect = true;
	else {
		if (hit.m_element.m_pElement)
			if (!hit.m_element.m_pElement->IsSelected () && !bCtrl)
				bDeselect = true;
	}

	if (bDeselect) {
		for (int i = 0; i < v.GetSize (); i++) {
			CElement & e = v [i];
			bool bInvalidate = e.IsSelected ();
			CEditorElementList & list = v [i];
			const CBoundary & head = list.m_bounds;
			const CRect rcHead = doc.GetBox ().GetHeadRect (head, params) - ptScroll;

			e.SetSelected (false, this, * v [i].m_pList, workingHead);
			bNcInvalidate = true;

			CRect rcElement = (e.GetWindowRect (head, workingHead) * dZoom) + rcHead.TopLeft ();

			rcElement.InflateRect (1, 1);	
			rcInvalid.UnionRect (rcInvalid, rcElement);
		}
	}

	if (hit.m_element.m_pElement) { // clicked on an element
		CElement & e = * hit.m_element.m_pElement;
		CEditorElementList & list = * hit.m_element.m_pList;
		const CBoundary & head = list.m_bounds;
		const CRect rcHead = doc.GetBox ().GetHeadRect (head, params) - ptScroll;

		ASSERT (hit.m_element.m_pList);
		e.SetSelected (bCtrl ? !e.IsSelected () : true, this, * hit.m_element.m_pList, workingHead);

		CRect rcElement = (e.GetWindowRect (head, workingHead) * dZoom) + rcHead.TopLeft ();

		rcElement.InflateRect (1, 1);	
		rcInvalid.UnionRect (rcInvalid, rcElement);
		
		bNcInvalidate = true;

		if (e.IsSelected ()) {
			const FoxjetDatabase::HEADSTRUCT & head = hit.m_element.m_pList->GetHead ();
			CPoint pt = e.GetPos (head);
			CCoord coord (pt, doc.GetUnits (), head);
			theApp.SetStatusText (coord.Format (), CMainFrame::LOCATION);
		}
	}
	
	// only consider this case if ONE head was hit (heads can overlap)
	if (htResult.m_vHeadID.GetSize () == 1) {
		ULONG lHeadID = htResult.m_vHeadID [0];

		// hit a different head, change selection
		if (lHeadID != params.m_lHeadID) {
			CBoundary oldHead, newHead;
				
			doc.GetHead (params.m_lHeadID, oldHead);
			doc.GetHead (lHeadID, newHead);

			CRect rcOld = doc.GetBox ().GetHeadRect (oldHead, params) - ptScroll;
			CRect rcNew = doc.GetBox ().GetHeadRect (newHead, params) - ptScroll;

			rcOld.InflateRect (5, 5);
			rcNew.InflateRect (5, 5);

			rcInvalid.UnionRect (rcInvalid, rcOld);
			rcInvalid.UnionRect (rcInvalid, rcNew);

			SetActiveHead (lHeadID, false);

			if (HeadView::CHeadView * pHead = (HeadView::CHeadView *)doc.GetView (TaskDoc::HEAD)) 
				VERIFY (pHead->SetSelection (HeadView::CItem (params.m_lHeadID)));
		}
	}

	if (GetSelected (0) != v) 
		htResult = doc.m_box.HitTest (pt, params, HT_DEFAULT | (params.m_crosshairs.m_bShow ? HT_SETCROSSHAIRS : 0));

	doc.UpdateUIStructs ();

	InvalidateRect (rcInvalid);
	UpdateWindow ();

	return htResult;
}

void CTaskView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	using namespace Box::BoxParams;

	CBaseView::OnLButtonDown(nFlags, point);

	CTaskDoc & doc = * GetDocument ();
	CBoxParams & p = GetParams ();
	const CPoint ptCrosshairs = p.m_crosshairs.m_pt;
	const ULONG lInitHeadID = p.m_lHeadID;
	CRect rcInvalid (0, 0, 0, 0);
	const double dZoom = GetZoom ();
	const CPoint ptScroll = GetScrollPosition ();
	const CPoint pt = point + ptScroll;

	const CHit hitFirst = doc.m_box.HitTest (pt, p, HT_DEFAULT | (p.m_crosshairs.m_bShow ? HT_SETCROSSHAIRS : 0));
	bool bRepaint = false;
	TaskDoc::CSnapshot * pSnapshot = NULL;
	const UINT nPanel = p.m_nPanel;
	const ULONG lHeadID = p.m_lHeadID;
	CBoundary workingHead;

	doc.GetBox ().GetWorkingHead (p, workingHead);

	CHit hit = DoMouseSelect (nFlags, hitFirst, pt);
	SetCursor (hit);
	CPairArray vSel = GetSelected (0);
	const bool bScrollLocked = false;

	//TRACEF (_T ("doc.IsMoveLocked (): ") + ToString (doc.IsMoveLocked ()) + _T (", doc.IsScrollLocked (): ") + ToString (doc.IsScrollLocked ()) + _T (", vSel.GetSize (): ") + ToString (vSel.GetSize ()));

	bool bTrack = (!doc.IsMoveLocked () && vSel.GetSize () > 0) && !bScrollLocked || (bScrollLocked && vSel.GetSize () == 0);

	if (doc.IsRubberBand ())
		bTrack = true;

	m_pHit = NULL;

	if (!bTrack) {
		CRect rc (0, 0, 0, 0);

		if (m_ptDrag) 
			delete m_ptDrag;

		if (m_ptScroll)
			delete m_ptScroll;

		m_ptDrag = new CPoint (point);
		m_ptScroll = new CPoint (GetScrollPosition ());
		m_pHit = hit.m_element.m_pElement;

		// restore crosshairs
		p.m_crosshairs.m_pt = ptCrosshairs;

		GetClientRect (rc);
		ClientToScreen (rc);
		::ClipCursor (rc);
	}
	else {
		if (hit.m_element.m_pElement) {
			if (!doc.GetReadOnly () && !doc.IsMoveLocked ()) {
				if (theApp.IsConfiningOn ()) {
					////////////////////////////////////
					// TODO: this is a temporary fix.  it prevents an element from being dragged 
					// out of bounds before the real clipping rect is set
					CPoint ptCursor;
					::GetCursorPos (&ptCursor);
					::ClipCursor (CRect (0, 0, 1, 1) + ptCursor);
					::Sleep (0);
					// TODO
					////////////////////////////////////
				}

				CElement & e = * hit.m_element.m_pElement;

				if (e.IsSelected () && GetSelMovableUnified (vSel) == ALL) 
				{
					pSnapshot = (TaskDoc::CSnapshot *)doc.GetSnapshot ();
					pSnapshot->SetElementsChanged (vSel);

					ASSERT (hit.m_element.m_pList);
					CEditorElementList & list = * hit.m_element.m_pList;
					CPoint ptCursor, ptElement = hit.m_element.m_ptTracker;
					CRect rcTracker;
					const CBoundary  & head = hit.m_element.m_pList->m_bounds;
					CBoundary workingHead;
					const CPoint ptHead = doc.m_box.GetHeadRect (head, p).TopLeft () - ptScroll;
					const CRect rcElement = (e.GetWindowRect (head, workingHead) * dZoom) + ptHead;
					CRect rcHead [] = 
					{
						doc.m_box.GetHeadRect (head, p, true),
						doc.m_box.GetHeadRect (head, p, false),
					};

					doc.GetBox ().GetWorkingHead (p, workingHead);

					VERIFY (e.GetTrackerRect (rcTracker));
					rcTracker = CRect (CPoint (0, 0), rcTracker.Size ()) + point - ptElement;
					VERIFY (e.SetTrackerRect (rcTracker));
					ptElement += rcTracker.TopLeft ();
					doc.UpdateProductAreas ();

					m_bTrackingElement = true;

					//////////////////////////////////////////////////////////////////////////////////////////
					// NOTE:	if invalidation is performed before the call to CElementTracker::Track,
					//			be sure to test with scroll
					//////////////////////////////////////////////////////////////////////////////////////////

					if (e.Track (this, ptElement, rcHead, vSel, SetLocationFct, workingHead)) {
						for (int i = 0; i < vSel.GetSize (); i++)
							vSel [i].m_pElement->GetElement ()->ClipTo (vSel [i].m_pList->GetHead ());

						doc.SetModifiedFlag (pSnapshot);

						if (theApp.IsConfiningOn ())
							doc.Confine ();

						doc.Invalidate (vSel, this);
						//doc.SetMoveLock (true);
					}
					else {
 						delete pSnapshot;
						pSnapshot = NULL;
					}

					m_bTrackingElement = false;
				}

				////////////////////////////////////
				// TODO
				::ClipCursor (NULL);
				::Sleep (0);
				// TODO
				////////////////////////////////////
			}
		}
		else {
			ULONG lHeadID = GetActiveHead ();
			CBoundary head;
			
			head.m_card = GetDefaultHead ();

			doc.GetHead (lHeadID, head);

			CElementTracker t (head, workingHead);
			CPoint ptTrack (point);
			
			t.m_sizeMin = CSize (0, 0);

			if (t.TrackRubberBand (this, ptTrack)) { 
				CRect rc = t.m_rect;
				CArray <CMessage *, CMessage *> v;
				CArray <HEADSTRUCT, HEADSTRUCT &> vHeadsHit;
				const CPoint ptOffset = GetScrollPosition ();
				
				doc.GetMessages (v, GetActivePanel ());

				rc.NormalizeRect ();

				CRect rcTracker (rc);
				rcTracker.InflateRect (1, 1);	
				rcInvalid.UnionRect (rcInvalid, rcTracker);

				rc += ptOffset;

				for (int i = 0; i < v.GetSize (); i++) {
					CEditorElementList & list = v [i]->m_list;
					const CBoundary & h = list.m_bounds;
					CRect rcHead = doc.GetMembers ().m_box.GetHeadRect (h, p);

					if (rcHead.PtInRect (rc.TopLeft ()) || rcHead.PtInRect (rc.BottomRight ()))
						if (CTask::Find (vHeadsHit, h.GetID ()) == -1)
							vHeadsHit.Add (HEADSTRUCT (h));

					if (!doc.m_box.IsLinked (h)) {
						for (int j = 0; j < list.GetSize (); j++) {
							CElement & e = list [j];
							CRect rcElement = (e.GetWindowRect (list.GetHead (), workingHead) * dZoom) + rcHead.TopLeft ();

							if (CRect ().IntersectRect (rc, rcElement)) {
								e.SetSelected (true, this, list, workingHead);

								CPoint pt = rcHead.TopLeft () - ptScroll;
								CRect rc = (e.GetWindowRect (h, workingHead) * dZoom) + pt;

								rc.InflateRect (1, 1);	
								rcInvalid.UnionRect (rcInvalid, rc);
							}
						}
					}
				}

				// restore crosshairs
				p.m_crosshairs.m_pt = ptCrosshairs;

				if (vHeadsHit.GetSize () > 1)
					p.m_lHeadID = lInitHeadID;

				doc.Invalidate (rcInvalid, this);
			}
		}
	}

	bRepaint |= nPanel != p.m_nPanel ? true : false;

	if (ptScroll != GetScrollPosition ()) {
		CRect rc;

		GetClientRect (rcInvalid);
	}

	if (!bRepaint) {
		CBoundaryArray v;

		doc.GetHeads (v);

		int nHeadIndex = CTask::Find (v, p.m_lHeadID);

		if (nHeadIndex != -1) {
			CBoundary b = v [nHeadIndex];;
			HEADSTRUCT & head = b.m_card;
			const CRect rcHead = doc.m_box.GetHeadRect (b, p);
			const CPoint ptOffset = rcHead.TopLeft () - ptScroll;

			if (ptCrosshairs != p.m_crosshairs.m_pt) { 
				const CSize size = (CSize (p.m_crosshairs.m_size) + CSize (1, 1)) * 2;
				CRect rc;
			
				rc = CRect (ptCrosshairs, ptCrosshairs) + ptOffset;
				rc.InflateRect (size.cx, size.cy);
				rcInvalid.UnionRect (rcInvalid, rc);

				rc = CRect (p.m_crosshairs.m_pt, p.m_crosshairs.m_pt) + ptOffset;
				rc.InflateRect (size.cx, size.cy);
				rcInvalid.UnionRect (rcInvalid, rc);
			}
		}
	}

	if (bRepaint) {
		Invalidate ();
		RedrawWindow ();

		if (pSnapshot) {
			doc.UpdateAllViews (this, 0, &TaskDoc::CHint (&doc.GetMembers (), pSnapshot, &vSel));
			NCINVALIDATE (this); 
		}
	}
	else {
		doc.Invalidate (rcInvalid, this);
	}

	doc.UpdateUIStructs ();
	SetCursor (hit);
}

void CTaskView::OnPaint ()
{
	CRect rcUpdate;
	CPaintDC dc(this);
	OnPrepareDC(&dc);

	if (GetUpdateRect (rcUpdate)) {
		CRect rcClient;

		GetClientRect (rcClient);

		if (rcClient.Size () != rcUpdate.Size ()) {
			m_3d.Draw (&dc, rcUpdate);
			return;
		}
	}

	OnDraw(&dc);
}

void CTaskView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if (m_ptDrag || m_ptScroll) {
		::ClipCursor (NULL);
		//REPAINT (this);
	}

	if (m_ptDrag) {
		CPoint ptOffset = point - (* m_ptDrag);
		CSize resize = CElementTracker::GetHandleSize ();

		if (!m_pHit && abs (ptOffset.x) < resize.cx && abs (ptOffset.y) < resize.cy) {
			if (m_ptScroll) {
				CBoxParams & p = GetParams ();
				CBoundary head;
				CTaskDoc & doc = * GetDocument ();

				if (p.GetActiveHead (doc.m_box, head)) {
					double dZoom = GetZoom ();
					CRect rcHead = doc.m_box.GetHeadRect (head, p);
					CPoint pt = point;
					CPoint ptScroll = GetScrollPosition ();

					pt.x -= (rcHead.left - ptScroll.x);
					pt.y -= (rcHead.top - ptScroll.y);

					rcHead -= rcHead.TopLeft ();

					if (rcHead.PtInRect (pt)) {
						CRect rcInvalid (0, 0, 0, 0);
						
						rcInvalid.UnionRect (rcInvalid, InvalidateCrosshairs ());
						p.m_crosshairs.m_pt = pt;
						rcInvalid.UnionRect (rcInvalid, InvalidateCrosshairs ());

						//CString str; str.Format (_T ("m_ptDrag: %d, %d, pt: %d, %d, ptScroll: %d, %d "), m_ptDrag->x, m_ptDrag->y, pt.x, pt.y, ptScroll.x, ptScroll.y); TRACEF (str); 

						InvalidateRect (rcInvalid);
					}
				}
			}
		}

		delete m_ptDrag;
		m_ptDrag = NULL;
	}

	if (m_ptScroll) {
		delete m_ptScroll;
		m_ptScroll = NULL;
	}

/*
	CBoxParams & p = GetParams ();
	CTaskDoc & doc = * GetDocument ();
	CPoint ptOffset = GetOffset ();
	CPoint pt = point + CPoint (ptOffset.x, -ptOffset.y);
	Box::CHit hit = doc->m_box.HitTest (pt, p);
*/
	CBaseView::OnLButtonUp(nFlags, point);
/*
	if (hit.m_lHeadID != -1 && hit.m_lHeadID != p.m_lHeadID) {
		p.m_lHeadID = hit.m_lHeadID;

		if (CHeadView * pHead = (CHeadView *)doc.GetView (CTaskDoc::HEAD)) 
			VERIFY (pHead->SetSelection (CHeadView::CItem (p.m_lHeadID)));
	}
*/
}

void CTaskView::SetCursor(const CPoint &pt)
{
	CBoxParams & p = GetParams ();
	CTaskDoc & doc = * GetDocument ();
	Box::CHit hit = doc.m_box.HitTest (pt + GetScrollPosition (), p);
	SetCursor (hit);
}

void CTaskView::SetCursor(const Foxjet3d::Box::CHit & hit)
{
	HCURSOR hCursor = ::AfxGetApp ()->LoadStandardCursor (IDC_ARROW);

	if (!GetDocument ()->IsMoveLocked ())
		if (!m_bTrackingPopupMenu)
			if (theApp.m_selMovableUnified == ALL) 
				if (hit.m_element.m_nType != CRectTracker::hitNothing) 
					hCursor = CElement::GetCursor (hit.m_element.m_nType);

	if (hCursor != ::GetCursor ())
		::SetCursor (hCursor);
}

BOOL CTaskView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	CBoxParams & p = GetParams ();
	CTaskDoc & doc = * GetDocument ();
	CPoint pt; 
	CString str;

	::GetCursorPos (&pt);
	pt += GetScrollPosition ();
	ScreenToClient (&pt);

	Box::CHit hit = doc->m_box.HitTest (pt, p, Box::HT_ELEMENT);

	SetCursor (hit);
	
/*
	str.Format ("[%d,%d,%d,%d]", 
		hit.m_nPanel,
		hit.m_lHeadID,
		hit.m_element.m_lID,
		hit.m_element.m_nType);
	theApp.SetStatusText (str, 3);
*/

	return TRUE;
}

bool CTaskView::SelHasFont (const CPairArray & v) const
{
	bool bHasFont = v.GetSize () > 0;

	for (int i = 0; i < v.GetSize (); i++) {
		const CBaseElement * p = v [i].m_pElement->GetElement ();

		if (!p->HasFont ()) {
			bHasFont = false;
			break;
		}
	}

	return bHasFont;
}

TaskView::SELECTIONTYPE CTaskView::GetSelFontSizeUnified (const CPairArray & v) const
{
	bool bUnified = true;
	SELECTIONTYPE nType = NA;

	for (int i = 0; i < v.GetSize (); i++) {
		const CBaseElement * pBaseCur = v [i].m_pElement->GetElement ();

		if (!pBaseCur->HasFont ()) {
			bUnified = false;
			break;
		}
		else if (i < (v.GetSize () - 1)) {
			const CBaseElement * pBaseNext = v [i + 1].m_pElement->GetElement ();

			if (!pBaseNext->HasFont ()) {
				bUnified = false;
				break;
			}
			else {
				const CBaseTextElement * pTextCur = (CBaseTextElement *)pBaseCur;
				const CBaseTextElement * pTextNext = (CBaseTextElement *)pBaseNext;

				if (pTextCur->GetFontSize () != pTextNext->GetFontSize ()) {
					bUnified = false;
					break;
				}
			}
		}
	}

	if (bUnified && v.GetSize () > 0)
		nType = ALL;

	return nType;
}

TaskView::SELECTIONTYPE CTaskView::GetSelFontWidthUnified (const CPairArray & v) const
{
	bool bUnified = true;
	SELECTIONTYPE nType = NA;

	for (int i = 0; i < v.GetSize (); i++) {
		const CBaseElement * pBaseCur = v [i].m_pElement->GetElement ();

		if (pBaseCur->GetWidth () == -1) 
			return NONE;

		else if (i < (v.GetSize () - 1)) {
			const CBaseElement * pBaseNext = v [i + 1].m_pElement->GetElement ();

			if (pBaseCur->GetWidth () != pBaseNext->GetWidth ()) {
				bUnified = false;
				break;
			}
		}
	}

	if (bUnified && v.GetSize () > 0)
		nType = ALL;

	return nType;
}

TaskView::SELECTIONTYPE CTaskView::GetSelFontNameUnified (const CPairArray & v) const
{
	SELECTIONTYPE nType = NA;
	bool bUnified = true;

	for (int i = 0; i < v.GetSize (); i++) {
		const CBaseElement * pBaseCur = v [i].m_pElement->GetElement ();

		if (!pBaseCur->HasFont ()) {
			bUnified = false;
			break;
		}
		else if (i < (v.GetSize () - 1)) {
			const CBaseElement * pBaseNext = v [i + 1].m_pElement->GetElement ();

			if (!pBaseNext->HasFont ()) {
				bUnified = false;
				break;
			}
			else {
				const CBaseTextElement * pTextCur = (CBaseTextElement *)pBaseCur;
				const CBaseTextElement * pTextNext = (CBaseTextElement *)pBaseNext;

				if (pTextCur->GetFontName () != pTextNext->GetFontName ()) {
					bUnified = false;
					break;
				}
			}
		}
	}

	if (bUnified && v.GetSize () > 0)
		nType = ALL;

	return nType;
}

TaskView::SELECTIONTYPE CTaskView::GetSelFontItalicUnified (const CPairArray & v) const
{
	SELECTIONTYPE nType = NA;
	bool bUnified = true;

	for (int i = 0; i < v.GetSize (); i++) {
		const CBaseElement * pBaseCur = v [i].m_pElement->GetElement ();

		if (!pBaseCur->HasFont ()) {
			bUnified = false;
			break;
		}
		else if (i < (v.GetSize () - 1)) {
			const CBaseElement * pBaseNext = v [i + 1].m_pElement->GetElement ();

			if (!pBaseNext->HasFont ()) {
				bUnified = false;
				break;
			}
			else {
				const CBaseTextElement * pTextCur = (CBaseTextElement *)pBaseCur;
				const CBaseTextElement * pTextNext = (CBaseTextElement *)pBaseNext;

				if (pTextCur->GetFontItalic () != pTextNext->GetFontItalic ()) {
					bUnified = false;
					break;
				}
			}
		}
	}

	if (bUnified && v.GetSize () > 0) {
		const CBaseElement * pBase = v [0].m_pElement->GetElement ();

		if (pBase->HasFont ()) {
			const CBaseTextElement * pText = (CBaseTextElement *)pBase;
			nType = pText->GetFontItalic () ? ALL : NONE;
		}
	}

	return nType;
}

TaskView::SELECTIONTYPE CTaskView::GetSelFontBoldUnified (const CPairArray & v) const
{
	SELECTIONTYPE nType = NA;
	bool bUnified = true;

	for (int i = 0; i < v.GetSize (); i++) {
		const CBaseElement * pBaseCur = v [i].m_pElement->GetElement ();

		if (!pBaseCur->HasFont ()) {
			bUnified = false;
			break;
		}
		else if (i < (v.GetSize () - 1)) {
			const CBaseElement * pBaseNext = v [i + 1].m_pElement->GetElement ();

			if (!pBaseNext->HasFont ()) {
				bUnified = false;
				break;
			}
			else {
				const CBaseTextElement * pTextCur = (CBaseTextElement *)pBaseCur;
				const CBaseTextElement * pTextNext = (CBaseTextElement *)pBaseNext;

				if (pTextCur->GetFontBold () != pTextNext->GetFontBold ()) {
					bUnified = false;
					break;
				}
			}
		}
	}

	if (bUnified && v.GetSize () > 0) {
		const CBaseElement * pBase = v [0].m_pElement->GetElement ();

		if (pBase->HasFont ()) {
			const CBaseTextElement * pText = (CBaseTextElement *)pBase;
			nType = pText->GetFontBold () ? ALL : NONE;
		}
	}

	return nType;
}

TaskView::SELECTIONTYPE CTaskView::GetSelInverseUnified (const CPairArray & sel) const
{
	SELECTIONTYPE nType = NA;
	bool bUnified = true;

	for (int i = 0; i < sel.GetSize (); i++) {
		const CBaseElement * pCur = sel [i].m_pElement->GetElement ();

		if (i < (sel.GetSize () - 1)) {
			const CBaseElement * pNext = sel [i + 1].m_pElement->GetElement ();

			if (pCur->IsInverse () != pNext->IsInverse ()) {
				bUnified = false;
				break;
			}
		}
	}

	if (bUnified && sel.GetSize () > 0)
		nType = sel [0].m_pElement->GetElement ()->IsInverse () ? ALL : NONE;

	return nType;
}

TaskView::SELECTIONTYPE CTaskView::GetSelFlipVUnified (const CPairArray & sel) const
{
	SELECTIONTYPE nType = NA;
	bool bUnified = true;

	for (int i = 0; i < sel.GetSize (); i++) {
		const CBaseElement * pCur = sel [i].m_pElement->GetElement ();

		if (i < (sel.GetSize () - 1)) {
			const CBaseElement * pNext = sel [i + 1].m_pElement->GetElement ();

			if (pCur->IsFlippedV () != pNext->IsFlippedV ()) {
				bUnified = false;
				break;
			}
		}
	}

	if (bUnified && sel.GetSize () > 0)
		nType = sel [0].m_pElement->GetElement ()->IsFlippedV () ? ALL : NONE;

	return nType;
}

TaskView::SELECTIONTYPE CTaskView::GetSelFlipHUnified (const CPairArray & sel) const
{
	SELECTIONTYPE nType = NA;
	bool bUnified = true;

	for (int i = 0; i < sel.GetSize (); i++) {
		const CBaseElement * pCur = sel [i].m_pElement->GetElement ();

		if (i < (sel.GetSize () - 1)) {
			const CBaseElement * pNext = sel [i + 1].m_pElement->GetElement ();

			if (pCur->IsFlippedH () != pNext->IsFlippedH ()) {
				bUnified = false;
				break;
			}
		}
	}

	if (bUnified && sel.GetSize () > 0)
		nType = sel [0].m_pElement->GetElement ()->IsFlippedH () ? ALL : NONE;

	return nType;
}

TaskView::SELECTIONTYPE CTaskView::GetSelResizableUnified(const CPairArray & sel) const
{
	SELECTIONTYPE nType = NA;
	bool bUnified = true;

	for (int i = 0; i < sel.GetSize (); i++) {
		const CBaseElement * pCur = sel [i].m_pElement->GetElement ();

		if (i < (sel.GetSize () - 1)) {
			const CBaseElement * pNext = sel [i + 1].m_pElement->GetElement ();

			if (pCur->IsResizable () != pNext->IsResizable ()) {
				bUnified = false;
				break;
			}
		}
	}

	if (bUnified && sel.GetSize () > 0)
		nType = sel [0].m_pElement->GetElement ()->IsResizable () ? ALL : NONE;

	return nType;
}

TaskView::SELECTIONTYPE CTaskView::GetSelMovableUnified (const CPairArray & sel) const
{
	SELECTIONTYPE nType = NA;
	bool bUnified = true;

	for (int i = 0; i < sel.GetSize (); i++) {
		const CBaseElement * pCur = sel [i].m_pElement->GetElement ();

		if (i < (sel.GetSize () - 1)) {
			const CBaseElement * pNext = sel [i + 1].m_pElement->GetElement ();

			if (pCur->IsMovable () != pNext->IsMovable ()) {
				bUnified = false;
				break;
			}
		}
	}

	if (bUnified && sel.GetSize () > 0)
		nType = sel [0].m_pElement->GetElement ()->IsMovable () ? ALL : NONE;

	return nType;
}

TaskView::CElementPtrArray CTaskView::GetSelected () const
{
	CElementListPtrArray vLists = GetLists ();
	CElementPtrArray v;

	for (int i = 0; i < vLists.GetSize (); i++) {
		ASSERT (vLists [i]);
		CEditorElementList & list = * vLists [i];

		for (int j = 0; j < list.GetSize (); j++) {
			CElement & e = list [j];

			if (e.IsSelected ())
				v.Add (&e);
		}
	}

	return v;
}

TaskView::CElementListPtrArray CTaskView::GetLists () const
{
	CElementListPtrArray v;
	const CTaskMembers & doc = GetDocument ()->GetMembers ();
	CBoxParams & p = GetParams ();

	for (int i = 0; i < p.m_task.GetMessageCount (); i++) {
		if (CMessage * pMsg = p.m_task.GetMessage (i)) {
			ULONG lPanelID = pMsg->m_list.m_bounds.GetPanelID ();
			const CPanel * pFace = NULL;
			
			VERIFY (doc.m_box.GetPanel (p.m_nPanel, &pFace));

			if (lPanelID == pFace->GetMembers ().m_lID) 
				v.Add (&pMsg->m_list);
		}
	}

	return v;
}

FoxjetCommon::CElementListArray CTaskView::GetLists (int) const
{
	FoxjetCommon::CElementListArray v;
	const CTaskMembers & doc = GetDocument ()->GetMembers ();
	CBoxParams & p = GetParams ();

	for (int i = 0; i < p.m_task.GetMessageCount (); i++) {
		if (CMessage * pMsg = p.m_task.GetMessage (i)) {
			ULONG lPanelID = pMsg->m_list.m_bounds.GetPanelID ();
			const CPanel * pFace = NULL;
			
			VERIFY (doc.m_box.GetPanel (p.m_nPanel, &pFace));

			if (lPanelID == pFace->GetMembers ().m_lID) 
				v.Add (&pMsg->m_list);
		}
	}

	return v;
}

CEditorElementList * CTaskView::GetList (ULONG lHeadID) const
{
	CElementListPtrArray v;
	const CTaskMembers & doc = GetDocument ()->GetMembers ();
	CBoxParams & p = GetParams ();

	for (int i = 0; i < p.m_task.GetMessageCount (); i++) {
		if (CMessage * pMsg = p.m_task.GetMessage (i)) {
			if (pMsg->m_list.m_bounds.GetID () == lHeadID) 
				return &pMsg->m_list;
		}
	}

	return NULL;
}


Element::CPairArray CTaskView::GetSelected (bool bAll, ULONG lHeadID) const
{
	CElementListPtrArray vLists = GetLists ();
	CPairArray v;
	CBoundary workingHead;
	CBoxParams & params = GetParams ();

	GetDocument ()->GetBox ().GetWorkingHead (params, workingHead);

	for (int i = 0; i < vLists.GetSize (); i++) {
		ASSERT (vLists [i]);
		CEditorElementList & list = * vLists [i];

		if (lHeadID == -1 || lHeadID == list.GetHead ().m_lID) {
			for (int j = 0; j < list.GetSize (); j++) {
				CElement & e = list [j];

				if (bAll || e.IsSelected ())
					v.Add (CPair (&e, &list, &workingHead.m_card));
			}
		}
	}

	return v;
}

bool CTaskView::GetSelHeadUnified (FoxjetDatabase::HEADSTRUCT & head) const
{
	CElementListPtrArray v = GetLists ();
	ULONG lLastHeadID = -1;

	for (int i = 0; i < v.GetSize (); i++) {
		const CEditorElementList & list = * v [i];

		for (int j = 0; j < list.GetSize (); j++) {
			const CElement & e = list [j];

			if (e.IsSelected ()) {
				const HEADSTRUCT & h = list.GetHead ();

				if (lLastHeadID != -1 && lLastHeadID != h.m_lID)
					return false;

				if (lLastHeadID != h.m_lID)
					head = h;

				lLastHeadID = h.m_lID;
			}
		}
	}

	return true;
}

HBITMAP CTaskView::CreateBitmap (CDC & dc, Element::CPairArray & vDraw) const
{
	HBITMAP hBmp = NULL;

	if (vDraw.GetSize () == 1) {
		CElement & e = vDraw [0];

		if (CRawBitmap * pBmp = e.GetCache ()) {
			hBmp = pBmp->GetHBITMAP ();
			//hBmp = (HBITMAP)pBmp->GetSafeHandle ();
		}
	}

	return hBmp;
}


void CTaskView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	using namespace Box;

	CRect rcInvalid (0, 0, 0, 0);
	CTaskDoc & doc = * GetDocument ();
	CPoint pt = point + GetScrollPosition ();
	CBoxParams & p = GetParams ();
	
	if (p.m_crosshairs.m_bShow) 
		rcInvalid.UnionRect (rcInvalid, InvalidateCrosshairs (false));

	CHit hit = GetDocument ()->m_box.HitTest (pt, p, HT_DEFAULT | (p.m_crosshairs.m_bShow ? HT_SETCROSSHAIRS : 0));

	CBaseView::OnRButtonDown(nFlags, point);
	hit = DoMouseSelect (nFlags, hit, pt);

	if (p.m_crosshairs.m_bShow) 
		rcInvalid.UnionRect (rcInvalid, InvalidateCrosshairs (false));

	GetDocument ()->Invalidate (rcInvalid, this);
}

void CTaskView::OnUpdateStatusLocation(CCmdUI *pCmdUI)
{
	// TODO

	/* 
	the coords are actually displayed by: 
	- CElementTracker::DrawTrackerRect (LPCRECT, CWnd *, CDC *, CWnd *)
		this displays the coords of the lead object during a mouse move/resize

	- CTaskView::DoMouseSelect (...)
		this displays the coords of the lead object when it is selected

	- CTaskView::MoveSelection(int cx, int cy)
		this displays the coords of the lead object during a keyboard move
	*/

	CString str;

	if (theApp.m_uiSel.m_nSelCount) {
		CPairArray v = GetSelected (0);

		if (v.GetSize ()) {
			const CElement & e = v [0];
			const FoxjetDatabase::HEADSTRUCT & head = v [0].m_pList->GetHead ();
			CCoord c (e.GetPos (head), GetDocument ()->GetUnits (), head);
			str = c.Format ();
		}
	}
	
	pCmdUI->SetText (str);
}

void CALLBACK CTaskView::SetLocationFct (const CString & str)
{
	theApp.SetStatusText (str, 3);
}

void CTaskView::OnApiElementCommand(UINT nID)
{
	CTaskDoc & doc = * GetDocument ();
	CBoundary head;

	doc.GetHead (GetActiveHead (), head);

	if (nID >= ID_ELEMENT_01 && nID <= ID_ELEMENT_30) {
		for (int i = 0; MENUMAP [i].m_nCmdID != -1; i++) {
			if (MENUMAP [i].m_nCmdID == nID) {
				if (IsValidElementType (MENUMAP [i].m_nClassID, head)) {
					TRACEF (FoxjetDatabase::ToString (MENUMAP [i].m_nClassID) + _T (": ") + GetElementResStr (MENUMAP [i].m_nClassID));
					OnCreateElement (MENUMAP [i].m_nClassID);
					return;
				}
			}
		}
	}
}

bool CTaskView::CanCreateElement (Foxjet3d::Box::CBoxParams & p) const
{
	UINT nPanel = GetActivePanel ();
	ULONG lHeadID = GetActiveHead ();
	const CPanel * pPanel = NULL;
	CTaskDoc & doc = * GetDocument ();
	
	if (!doc.m_box.GetPanel (nPanel, &pPanel)) 
		return false;

	ASSERT (pPanel);
	
	return pPanel->ContainsHead (lHeadID);
}

void CTaskView::OnUpdateApiElementCommand(CCmdUI *pCmdUI)
{
	bool bEnabled = false;
	UINT nID = pCmdUI->m_nID;
	CTaskDoc & doc = * GetDocument ();
	CBoundary head;

	doc.GetHead (GetActiveHead (), head);

	if (!doc.m_box.IsLinked (head)) {
		if (CanCreateElement (GetParams ()) && !doc.GetReadOnly ()) {
			if (nID >= ID_ELEMENT_01 && nID <= ID_ELEMENT_30) {
				for (int i = 0; MENUMAP [i].m_nCmdID != -1; i++) {
					
					if (MENUMAP [i].m_nCmdID == nID) 
						bEnabled = IsValidElementType (MENUMAP [i].m_nClassID, head);
				}
			}
		}
	}

	pCmdUI->Enable (bEnabled);
}

void CTaskView::Update (CEditorElementList & list, CElement * p, const CPoint & pt1000ths, const CBoundary & head, const CBoundary & workingHead, CPairArray & all,
	CBoxParams & params, CElementArray & vUpdate, TaskDoc::CSnapshot * pSnapshot)
{
	CTaskDoc & doc = * GetDocument ();
	double dZoom = GetZoom ();
//	CPoint ptReturned = p->GetPos (head);
	/* CSize size = */ p->GetElement ()->Draw (* GetDC (), head, true);
//	const double dXStretch = 1.0 / CElement::CalcXStretch (head, workingHead);
			
//	ptReturned.x = (int)((double)ptReturned.x * dXStretch);
	p->GetElement ()->Draw (CDC (), head, true);
	p->GetElement ()->GetWindowRect (head);
//	CPoint pt1000ths = CBaseElement::LogicalToThousandths (p->GetElement ()->GetPosInternal (ptReturned), head);
	p->GetElement ()->SetPos (pt1000ths, &head.m_card, ALIGNMENT_ADJUST_SUBTRACT);

	const CRect rcHead [] = 
	{
		doc.m_box.GetHeadRect (head, params, true),
		doc.m_box.GetHeadRect (head, params, false),
	};
	CRect rcInvalid (0, 0, 0, 0);

	p->Confine (* GetDC (), rcHead, dZoom, head, workingHead);
	list.Add (p);
	doc.SelectAll (false);
	p->SetSelected (true, this, list, workingHead);

	CRect rc = p->GetWindowRect (head, workingHead);

	rcInvalid.UnionRect (rcInvalid, InvalidateCrosshairs (false));
	params.m_crosshairs.m_pt = CPoint (rc.right, rc.top) * dZoom;
	rcInvalid.UnionRect (rcInvalid, InvalidateCrosshairs (false));

	CPairArray vInvalidate;
	CPair pair (p, &list, &workingHead.m_card);

	vInvalidate.Add (pair);
	pSnapshot->SetElementsChanged (pair, TaskDoc::ELEMENT_ADD); 

	// can't mix ELEMENT_MODIFY with ELEMENT_ADD, need separate snapshot
	if (vUpdate.GetSize () > 1) {
		TaskDoc::CSnapshot * pSnapshot2 = (TaskDoc::CSnapshot *)doc.GetSnapshot ();

		for (int i = 0; i < vUpdate.GetSize (); i++) {
			CBaseElement * p = vUpdate [i];

			if (Find (p, vInvalidate) == -1) {
				int nIndex = Find (p, all);

				if (nIndex != -1) {
					CPair update = all [nIndex];

					vInvalidate.Add (update);
					pSnapshot2->SetElementsChanged (update);
				}
			}
		}

		doc.SetModifiedFlag (pSnapshot2);
	}

	doc.Invalidate (vInvalidate, this);
	doc.Invalidate (rcInvalid, this);
}

#ifdef _DEBUG
	#include "Comm32.h"
#endif



void CTaskView::OnCreateElement(int nClassID)
{
	CTaskDoc & doc = * GetDocument ();
	CBoundary head;

	doc.GetHead (GetActiveHead (), head);

	if (FoxjetCommon::IsValidElementType (nClassID, head)) {
		CTaskDoc & doc = * GetDocument ();

		if (CMessage * pMsg = doc.m_task.GetMessageByHead (GetActiveHead ())) {
			if (!pMsg->m_list.CanAdd ()) {
				CString str;

				str.Format (LoadString (IDS_ELEMENTMAX), pMsg->m_list.GetMaxElements ());
				MsgBox (* this, str, MB_ICONASTERISK);
				return;
			}

			TaskDoc::CSnapshot * pSnapshot = (TaskDoc::CSnapshot *)doc.GetSnapshot ();
			CEditorElementList & list = pMsg->m_list;
			const CBoundary & head = list.m_bounds;
			defElements.LoadRegElementDefs (nClassID, GetElementListVersion ());
			const CBaseElement & def = defElements.GetElement (nClassID);
			CPairArray all = GetSelected (true);
			CElement * p = new CElement (CopyElement (&def, &head.m_card, true));

			CBoxParams & params = GetParams ();
			CSize pa = doc.GetProductArea (list);
			UNITS units = doc.GetUnits ();
			double dZoom = GetZoom ();
			CCoord c (params.m_crosshairs.m_pt, units, head, dZoom); 
			CPoint pt = CCoord::CoordToPoint (c, units, head);
			CBoundary workingHead;
			CElementArray vUpdate;

			ASSERT (p);

			doc.GetBox ().GetWorkingHead (params, workingHead);

			if (nClassID == SERIAL) {
				bool bWizard = true;

				for (int i = all.GetCount () - 1; i >= 0; i--) {
					if (CElement * e = all [i].m_pElement) {
						if (CBaseElement * pBase = e->GetElement ()) {
							if (pBase->GetClassID () == SERIAL) {
								delete p;
								p = new CElement (CopyElement (pBase, &head.m_card, true));
								bWizard = false;
								break;
							}
						}
					}
				}

				if (bWizard) {
					using namespace FoxjetElements;

					CSerialWizardSheet dlg (this);

					dlg.m_lLineID = doc.m_task.m_lLineID;

					if (dlg.DoModal () == IDOK) {
						CPoint pt1000ths = CBaseElement::LogicalToThousandths (params.m_crosshairs.m_pt, head);
						double dStretch [2] = { 1, 1 };
						int cy = 0;
						CSize sizeHead = CBaseElement::LogicalToThousandths (doc.GetMembers ().m_box.GetHeadRect (head, params, false).Size (), head);

						CBaseElement::CalcStretch (head, dStretch);

						if (p)
							p = new CElement (CopyElement (&defElements.GetElement (SERIAL), &head.m_card, true));

						TRACEF (dlg.m_pdlgIndex->m_strSampleData);
						TRACEF (ToString (dlg.m_pdlgIndex->m_nIndex));
						TRACEF (ToString (dlg.m_pdlgIndex->m_nLength));

						if (dlg.m_pdlgIndex->m_strSampleData.GetLength ()) {
							LINESTRUCT line;

							GetLineRecord (theApp.GetDB (), dlg.m_lLineID, line);

							line.m_nBufferOffset = 0;
							line.m_nSignificantChars = dlg.m_pdlgIndex->m_strSampleData.GetLength ();
							VERIFY (UpdateLineRecord (theApp.GetDB (), line));
						}

						if (CSerialElement * pElement = DYNAMIC_DOWNCAST (CSerialElement, p->GetElement ())) {
							pElement->SetIndex (dlg.m_pdlgIndex->m_nIndex);
							pElement->SetLength (dlg.m_pdlgIndex->m_nLength);

							if (dlg.m_pdlgIndex->m_bSetDefaultData) {
								std::wstring str (dlg.m_pdlgIndex->m_strSampleData);

								try { str = str.substr (dlg.m_pdlgIndex->m_nIndex, dlg.m_pdlgIndex->m_nLength); } catch (const std::out_of_range & oor) {  }
								pElement->SetDefault (str.c_str ());
							}
						}

						p->GetElement ()->Build ();
						p->SetPos (pt, head);
						p->GetElement ()->SetID (list.GetNextID ());
						p->GetElement ()->SetLocalParamUID ((ULONG)&doc);
						p->GetElement ()->OnCreateNew (head, doc.m_task.m_strName);


						{
							TRACEF (dlg.m_pdlgPort->m_port.ToString ());

							FoxjetCommon::StdCommDlg::CCommItem comm;
							SETTINGSSTRUCT s;

							s.m_strKey.Format (_T ("COM%d"), dlg.m_pdlgPort->m_port.m_dwPort);

							GetSettingsRecord (theApp.GetDB (), 0, s.m_strKey, s);
							comm.FromString (s.m_strData);
							
							comm.m_bPrintOnce	= dlg.m_pdlgPrintOnce->m_bPrintOnce;
							comm.m_dwPort		= dlg.m_pdlgPort->m_port.m_dwPort;
							comm.m_dwBaudRate	= dlg.m_pdlgPort->m_port.m_dwBaudRate;
							comm.m_lLineID		= dlg.m_lLineID;
							comm.m_nByteSize	= dlg.m_pdlgPort->m_port.m_nByteSize;
							comm.m_nStopBits	= dlg.m_pdlgPort->m_port.m_nStopBits;
							comm.m_parity		= dlg.m_pdlgPort->m_port.m_parity;
							comm.m_strLine		= dlg.m_pdlgPort->m_port.m_strLine;
							comm.m_type			= REMOTEDEVICE;
							comm.m_nEncoding	= 0;

							if (dlg.m_pdlgEncoding)
								comm.m_nEncoding = dlg.m_pdlgEncoding->m_nEncoding;

							s.m_lLineID = 0;
							s.m_strData = comm.ToString ();

							if (!UpdateSettingsRecord (theApp.GetDB (), s))
								VERIFY (AddSettingsRecord (theApp.GetDB (), s));
						}

						p->GetElement ()->Build ();
						CSize size = p->GetElement ()->Draw (* GetDC (), head, true);
						
						defElements.SetElement (SERIAL, * p->GetElement (), ::verApp);

						TRACEF (ToString ((double)size.cx) + _T (", ") + ToString (pt1000ths.x / 1000.0));

						Update (list, p, pt1000ths, head, workingHead, all, params, vUpdate, pSnapshot);

						pt1000ths.x += (size.cx / dStretch [0]) + 500; 
						cy = max (cy, size.cy);

						if (pt1000ths.x >= sizeHead.cx) {
							pt1000ths.x = 0;
							pt1000ths.y += (cy + 250);
							cy = 0;
						}

						doc.SetModifiedFlag (pSnapshot);
						doc.SetMoveLock (false);
						doc.SetRubberBand (false);
						doc.SetTemplate (dlg.m_pdlgFinish->m_bTemplate);

						return;
					}
				}
			}

			if (nClassID == DATABASE) {
				bool bWizard = true;

				for (int i = all.GetCount () - 1; i >= 0; i--) {
					if (CElement * e = all [i].m_pElement) {
						if (CBaseElement * pBase = e->GetElement ()) {
							if (pBase->GetClassID () == DATABASE) {
								delete p;
								p = new CElement (CopyElement (pBase, &head.m_card, true));
								bWizard = false;
								break;
							}
						}
					}
				}

				if (bWizard) {
					using namespace FoxjetElements;

					FoxjetElements::CDbWizardDlg dlg (this);

					if (dlg.DoModal () == IDOK) {				
						CPoint pt1000ths = CBaseElement::LogicalToThousandths (params.m_crosshairs.m_pt, head);
						double dStretch [2] = { 1, 1 };
						int cy = 0;
						CSize sizeHead = CBaseElement::LogicalToThousandths (doc.GetMembers ().m_box.GetHeadRect (head, params, false).Size (), head);

						CBaseElement::CalcStretch (head, dStretch);

						TRACEF (dlg.m_pdlgDSN->m_strDSN);
						TRACEF (dlg.m_pdlgField->m_strTable);
						TRACEF (dlg.m_pdlgKeyField->m_strKeyField);
						TRACEF (dlg.m_pdlgKeyField->m_strKeyValue);

						FoxjetCommon::CCopyArray <FoxjetElements::CFieldItem, FoxjetElements::CFieldItem &> v = dlg.m_pdlgField->m_vFields;
						
						if (dlg.m_pdlgDbStart->m_bEnabled) {
							FoxjetUtils::CDatabaseStartDlg dlgDbStart (this);

							dlgDbStart.m_strDSN			= dlg.m_pdlgDbStart->m_params.m_strDSN;
							dlgDbStart.m_strTable		= dlg.m_pdlgDbStart->m_params.m_strTable;
							dlgDbStart.m_strKeyField	= dlg.m_pdlgDbStart->m_params.m_strKeyField;
							dlgDbStart.m_strTaskField	= dlg.m_pdlgDbStart->m_params.m_strTaskField;
							dlgDbStart.m_nSQLType		= dlg.m_pdlgDbStart->m_params.m_nSQLType;

							TRACEF (dlgDbStart.m_strDSN);
							TRACEF (dlgDbStart.m_strTable);
							TRACEF (dlgDbStart.m_strKeyField);
							TRACEF (dlgDbStart.m_strTaskField);

							dlgDbStart.Save (theApp.GetDB ());
						}

						delete p;

						for (int i = 0; i < v.GetSize (); i++) {
							FoxjetElements::CFieldItem & e = v [i];
							int nClassID = e.m_nClassID; 

							TRACEF (ToString (i) + _T (": ") + e.GetDispText (0) + _T (", ") + e.GetDispText (1)  + _T (", ") + e.GetDispText (2));

							switch (nClassID) {
							default:
							case TEXT:
								p = new CElement (CopyElement (&defElements.GetElement (DATABASE), &head.m_card, true));
								break;
							case BMP:
							case BARCODE:
								p = new CElement (CopyElement (&defElements.GetElement (nClassID), &head.m_card, true));
								break;
							}

							p->GetElement ()->Build ();
							p->SetPos (pt, head);
							p->GetElement ()->SetID (list.GetNextID ());
							p->GetElement ()->SetLocalParamUID ((ULONG)&doc);
							p->GetElement ()->OnCreateNew (head, doc.m_task.m_strName);

							switch (nClassID) {
							default:
							case TEXT:
								// 1: Flavor, Text, Bitmap
								// 1: [field], [sql type], [element type]
								{
									if (CDatabaseElement * pElement = DYNAMIC_DOWNCAST (CDatabaseElement, p->GetElement ())) {
										pElement->SetDSN (dlg.m_pdlgDSN->m_strDSN);
										pElement->SetTable (dlg.m_pdlgField->m_strTable);
										pElement->SetField (e.GetDispText (0));

										if (dlg.m_pdlgKeyField->m_strKeyField.GetLength ()) {
											pElement->SetPromptAtTaskStart (true);
											pElement->SetKeyField (dlg.m_pdlgKeyField->m_strKeyField);
											pElement->SetKeyValue (dlg.m_pdlgKeyField->m_strKeyValue);
										}
									}
								}
								break;
							case BMP:
								{
									if (CBitmapElement * pElement = DYNAMIC_DOWNCAST (CBitmapElement, p->GetElement ())) {
										pElement->SetDatabaseLookup (true);
										pElement->SetDatabaseDSN (dlg.m_pdlgDSN->m_strDSN);
										pElement->SetDatabaseTable (dlg.m_pdlgField->m_strTable);
										pElement->SetDatabaseField (e.GetDispText (0));
										pElement->SetDatabaseSQLType (e.m_nSQLType);

										if (dlg.m_pdlgKeyField->m_strKeyField.GetLength ()) {
											pElement->SetDatabasePromptAtTaskStart (true);
											pElement->SetDatabaseKeyField (dlg.m_pdlgKeyField->m_strKeyField);
											pElement->SetDatabaseKeyValue (dlg.m_pdlgKeyField->m_strKeyValue);
										}
									}
								}
								break;
							case BARCODE:
								{
									if (CBarcodeElement * pElement = DYNAMIC_DOWNCAST (CBarcodeElement, p->GetElement ())) {
										pElement->SetSymbology (e.m_nSymbology);
										pElement->DeleteAllSubElements ();

										CSubElement * pSub = new CSubElement (DATABASE);

										if (CDatabaseElement * pDatabase = DYNAMIC_DOWNCAST (CDatabaseElement, pSub->GetElement ())) {
											pDatabase->SetDSN (dlg.m_pdlgDSN->m_strDSN);
											pDatabase->SetTable (dlg.m_pdlgField->m_strTable);
											pDatabase->SetField (e.GetDispText (0));

											if (dlg.m_pdlgKeyField->m_strKeyField.GetLength ()) {
												pDatabase->SetPromptAtTaskStart (true);
												pDatabase->SetKeyField (dlg.m_pdlgKeyField->m_strKeyField);
												pDatabase->SetKeyValue (dlg.m_pdlgKeyField->m_strKeyValue);
											}

											pDatabase->Build ();
											CString str = pDatabase->GetImageData ();
											TRACEF (str);
											pSub->m_strDataMask = CString (CSubElement::m_cDef, str.GetLength ());
										}

										pElement->InsertSubElement (0, * pSub);
									}
								}
								break;
							}

							p->GetElement ()->Build ();
							CSize size = p->GetElement ()->Draw (* GetDC (), head, true);


							VERIFY (defElements.SetElement (DATABASE, * p->GetElement (), ::verApp));
							//VERIFY (defElements.SaveRegElementDefs (DATABASE, ::verApp));

							TRACEF (ToString ((double)size.cx) + _T (", ") + ToString (pt1000ths.x / 1000.0));

							Update (list, p, pt1000ths, head, workingHead, all, params, vUpdate, pSnapshot);

							pt1000ths.x += (size.cx / dStretch [0]) + 500; 
							cy = max (cy, size.cy);

							if (pt1000ths.x >= sizeHead.cx) {
								pt1000ths.x = 0;
								pt1000ths.y += (cy + 250);
								cy = 0;
							}
						}

						doc.SetModifiedFlag (pSnapshot);
						doc.SetMoveLock (false);
						doc.SetRubberBand (false);
						doc.SetTemplate (dlg.m_pdlgFinish->m_bTemplate);

						return;
					}
				}
			}

			p->GetElement ()->Build ();
			p->SetPos (pt, head);
			p->GetElement ()->SetID (list.GetNextID ());
			p->GetElement ()->SetLocalParamUID ((ULONG)&doc);
			p->GetElement ()->OnCreateNew (head, doc.m_task.m_strName);
			
			if (FoxjetCommon::OnEditElement (* p->GetElement (), this, pa, 
				doc.GetUnits (), &list, false, false, &GetLists (0), &vUpdate))
			{
				CPoint pt1000ths = CBaseElement::LogicalToThousandths (p->GetElement ()->GetPosInternal (p->GetPos (head)), head);

				Update (list, p, pt1000ths, head, workingHead, all, params, vUpdate, pSnapshot);

				doc.SetModifiedFlag (pSnapshot);
				doc.SetMoveLock (false);
				doc.SetRubberBand (false);
			}
			else {
				p->GetElement ()->OnDestroyNew ();

				delete pSnapshot;
				delete p;
			}
		}
	}
}

void CTaskView::InitElementMenu(CMenu *pMenu)
{
	int i;
	bool bReadOnly = GetDocument ()->GetReadOnly () || !CanCreateElement (GetParams ());
	CTaskDoc & doc = * GetDocument ();
	CBoundary head;

	doc.GetHead (GetActiveHead (), head);

	for (i = GetElementTypeCount (); MENUMAP [i].m_nCmdID != -1; i++) 
		pMenu->DeleteMenu (MENUMAP [i].m_nCmdID, MF_BYCOMMAND);

	if (doc.m_box.IsLinked (head)) {
		for (i = 0; MENUMAP [i].m_nCmdID != -1; i++) 
			pMenu->DeleteMenu (MENUMAP [i].m_nCmdID, MF_BYCOMMAND);

		pMenu->EnableMenuItem (ID_EDIT_PASTE, MF_GRAYED | MF_BYCOMMAND);
		pMenu->EnableMenuItem (ID_EDIT_SELECTALL, MF_GRAYED | MF_BYCOMMAND);
	}

	for (i = 0; i < GetElementTypeCount (); i++) {
		UINT nID = MENUMAP [i].m_nCmdID;
		CString strItem, strAccel;
		
		strItem = GetElementMenuStr (i);

		if (i < 11)
			strAccel.Format (_T ("\tF%d"), i + 2);

		pMenu->ModifyMenu (nID, MF_BYCOMMAND, nID, strItem + strAccel);

		if (bReadOnly || !IsValidElementType (MENUMAP [i].m_nClassID, head))
			pMenu->EnableMenuItem (nID, MF_GRAYED | MF_BYCOMMAND);
	}
}

void CTaskView::InitContextMenu(CMenu *pMenu)
{
	CTaskDoc & doc = * GetDocument ();
	CPairArray sel = GetSelected (0);
	
	doc.UpdateUIStructs ();

	int nMovableUnified		= theApp.m_selMovableUnified; 
	int nResizableUnified	= theApp.m_selResizableUnified;
	int nFlipHUnified		= theApp.m_selFlipHUnified;
	int nFlipVUnified		= theApp.m_selFlipVUnified;
	int nInverseUnified		= theApp.m_selInverseUnified;

	bool bMovable			= (nMovableUnified == ALL);
	bool bResizable			= (nResizableUnified == ALL);
	bool bFlippedH			= (nFlipHUnified == ALL);
	bool bFlippedV			= (nFlipVUnified == ALL);
	bool bInverse			= (nInverseUnified == ALL);
	bool bReadOnly			= GetDocument ()->GetReadOnly ();

	CBaseView::InitContextMenu (pMenu);
	
	pMenu->EnableMenuItem (ID_EDIT_CUT, (bReadOnly ? MF_ENABLED : MF_GRAYED) | MF_BYCOMMAND);
	pMenu->EnableMenuItem (ID_PROPERTIES_MOVABLE, ((nMovableUnified == NA) ? MF_GRAYED : MF_ENABLED) | MF_BYCOMMAND);
	pMenu->EnableMenuItem (ID_PROPERTIES_RESIZABLE, ((nResizableUnified == NA) ? MF_GRAYED : MF_ENABLED) | MF_BYCOMMAND);
	pMenu->EnableMenuItem (ID_PROPERTIES_FLIPH, ((nFlipHUnified == NA) ? MF_GRAYED : MF_ENABLED) | MF_BYCOMMAND);
	pMenu->EnableMenuItem (ID_PROPERTIES_FLIPV, ((nFlipVUnified == NA) ? MF_GRAYED : MF_ENABLED) | MF_BYCOMMAND);
	pMenu->EnableMenuItem (ID_PROPERTIES_INVERSE, ((nInverseUnified == NA) ? MF_GRAYED : MF_ENABLED) | MF_BYCOMMAND);
	
	pMenu->EnableMenuItem (ID_ELEMENTS_DELETE, ((sel.GetSize () && !bReadOnly) ? MF_ENABLED : MF_GRAYED) | MF_BYCOMMAND);
	pMenu->EnableMenuItem (ID_EDIT_CUT, ((sel.GetSize () && !bReadOnly) ? MF_ENABLED : MF_GRAYED) | MF_BYCOMMAND);
	pMenu->EnableMenuItem (ID_EDIT_COPY, (sel.GetSize () ? MF_ENABLED : MF_GRAYED) | MF_BYCOMMAND);

	pMenu->EnableMenuItem (ID_ELEMENT_EDIT, (sel.GetSize () == 1 ? MF_ENABLED : MF_GRAYED) | MF_BYCOMMAND);

	pMenu->CheckMenuItem (ID_PROPERTIES_MOVABLE, (bMovable ? MF_CHECKED : MF_UNCHECKED) | MF_BYCOMMAND);
	pMenu->CheckMenuItem (ID_PROPERTIES_RESIZABLE, (bResizable ? MF_CHECKED : MF_UNCHECKED) | MF_BYCOMMAND);
	pMenu->CheckMenuItem (ID_PROPERTIES_FLIPH, (bFlippedH ? MF_CHECKED : MF_UNCHECKED) | MF_BYCOMMAND);
	pMenu->CheckMenuItem (ID_PROPERTIES_FLIPV, (bFlippedV ? MF_CHECKED : MF_UNCHECKED) | MF_BYCOMMAND);
	pMenu->CheckMenuItem (ID_PROPERTIES_INVERSE, (bInverse ? MF_CHECKED : MF_UNCHECKED) | MF_BYCOMMAND);

	pMenu->EnableMenuItem (ID_PROPERTIES_MOVABLE, (!bReadOnly ? MF_ENABLED : MF_GRAYED) | MF_BYCOMMAND);
	pMenu->EnableMenuItem (ID_PROPERTIES_RESIZABLE, (!bReadOnly ? MF_ENABLED : MF_GRAYED) | MF_BYCOMMAND);
	pMenu->EnableMenuItem (ID_PROPERTIES_FLIPH, (!bReadOnly ? MF_ENABLED : MF_GRAYED) | MF_BYCOMMAND);
	pMenu->EnableMenuItem (ID_PROPERTIES_FLIPV, (!bReadOnly ? MF_ENABLED : MF_GRAYED) | MF_BYCOMMAND);
	pMenu->EnableMenuItem (ID_PROPERTIES_INVERSE, (!bReadOnly ? MF_ENABLED : MF_GRAYED) | MF_BYCOMMAND);

	// this must match CTaskDoc::OnUpdateEditPaste
	bool bCanPaste = 
		(::IsClipboardFormatAvailable (CF_TEXT) || ::IsClipboardFormatAvailable (CF_BITMAP)) && 
		doc.GetActiveList () &&
		!bReadOnly;

	pMenu->EnableMenuItem (ID_EDIT_PASTE, (bCanPaste ? MF_ENABLED : MF_GRAYED) | MF_BYCOMMAND);
	pMenu->EnableMenuItem (ID_EDIT_SELECTALL, (GetElementCount () ? MF_ENABLED : MF_GRAYED) | MF_BYCOMMAND);

	pMenu->EnableMenuItem (ID_BOX_CHANGE, (!bReadOnly ? MF_ENABLED : MF_GRAYED) | MF_BYCOMMAND);

	{
		UINT nState = sel.GetSize () == 1 ? MF_ENABLED : MF_GRAYED;

		pMenu->EnableMenuItem (ID_PROPERTIES_SENDTOFRONT,	nState | MF_BYCOMMAND);
		pMenu->EnableMenuItem (ID_PROPERTIES_SENDTOBACK,	nState | MF_BYCOMMAND);
		pMenu->EnableMenuItem (ID_PROPERTIES_BRINGFORWARD,	nState | MF_BYCOMMAND);
		pMenu->EnableMenuItem (ID_PROPERTIES_SENDBACKWARD,	nState | MF_BYCOMMAND);
	}
}

int CTaskView::GetElementCount () const
{
	const CTaskDoc & doc = * GetDocument ();
	CBoundaryArray vHeads;
	ULONG lPanelID = GetActivePanel ();
	int nCount = 0;

	CPanel::GetHeads (doc.m_box, lPanelID, vHeads);

	for (int i = 0; i < vHeads.GetSize (); i++) {
		ULONG lHeadID = vHeads [i].GetID ();

		if (const CMessage * pMsg = doc.m_task.GetMessageByHead (lHeadID)) 
			nCount += pMsg->m_list.GetSize ();
	}

	return nCount;
}

void CTaskView::InitMenu(CMenu *pMenu)
{
	CBaseView::InitMenu (pMenu);
	InitContextMenu (pMenu);
	InitElementMenu (pMenu);
}

void CTaskView::OnInitMenu(CMenu* pMenu) 
{
	CBaseView::OnInitMenu(pMenu);
	InitMenu (pMenu);
}

void CTaskView::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu) 
{
	CBaseView::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
	InitMenu (pPopupMenu);
}

/* TODO: rem
Element::CPair CTaskView::FindElement (const FoxjetCommon::CBaseElement * pFind) const
{
	CElementListPtrArray v = GetLists ();
	
	for (int nList = 0; nList < v.GetSize (); nList++) {
		CEditorElementList * pList = v [nList];

		for (int nElement = 0; nElement < pList->GetSize (); nElement++) {
			CElement & e = pList->GetElement (nElement);

			if (e.GetElement () == pFind)
				return CPair (&e, pList, &e.GetElement ()->GetHead ());
		}
	}

	ASSERT (0);
	return CPair (NULL, NULL, NULL);
}
*/

int CTaskView::Find (const FoxjetCommon::CBaseElement * pFind, const Element::CPairArray & sel)
{
	for (int i = 0; i < sel.GetSize (); i++)
		if (sel [i].m_pElement) 
			if (sel [i].m_pElement->GetElement () == pFind)
				return i;

	return -1;
}

void CTaskView::OnElementEdit() 
{
	CTaskDoc & doc = * GetDocument ();
	CPairArray sel = GetSelected (0);
	CPairArray all = GetSelected (true);
	CBoundary workingHead;
	CBoxParams & params = GetParams ();

	doc.GetBox ().GetWorkingHead (params, workingHead);

	if (sel.GetSize () == 1) {
		CElement & e = sel [0];
		TaskDoc::CSnapshot * pSnapshot = (TaskDoc::CSnapshot *)doc.GetSnapshot ();
		const CEditorElementList & list = * sel [0].m_pList;
		const CBoundary & head = list.m_bounds;
		const CPoint ptScroll = GetScrollPosition ();
		CSize pa = doc.GetProductArea (list);
		double dZoom = GetZoom ();
		CPair pair (&e, sel [0].m_pList, &workingHead.m_card);
		CElementArray vUpdate;
		bool bReadOnly = doc.GetReadOnly ();

		e.GetElement ()->SetLocalParamUID ((ULONG)&doc);

		if (doc.m_box.IsLinked (head))
			bReadOnly = true;

		pSnapshot->SetElementsChanged (pair);

		bool bUpdated = FoxjetCommon::OnEditElement (
			* e.GetElement (), this, pa, doc.GetUnits (), 
			&list, bReadOnly, !e.IsMovable (),
			&GetLists (0), &vUpdate);

		if (bUpdated) {
			BeginWaitCursor ();

			CBoxParams & params = GetParams ();
			const CRect rcHead [] = 
			{
				doc.m_box.GetHeadRect (head, params, true) - ptScroll,
				doc.m_box.GetHeadRect (head, params, false) - ptScroll,
			};

			e.Confine (* GetDC (), rcHead, dZoom, head, workingHead);

			for (int i = 0; i < vUpdate.GetSize (); i++) {
				CBaseElement * p = vUpdate [i];

				if (Find (p, sel) == -1) {
					int nIndex = Find (p, all);

					if (nIndex != -1) {
						CPair update = all [nIndex];

						update.m_pElement->RecalcSize (* GetDC (), update.m_pList->GetHead ());

						sel.Add (update);
						pSnapshot->SetElementsChanged (update);
					}
				}
			}

			doc.SetModifiedFlag (pSnapshot);
			doc.Invalidate (sel, this);
			EndWaitCursor ();
		}
		else
			delete pSnapshot;
	}
}

void CTaskView::OnUpdateElementsEdit(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable (theApp.m_uiSel.m_nSelCount == 1);	
}

void CTaskView::OnElementsDelete() 
{
	ASSERT (!GetDocument ()->GetReadOnly ());
	CTaskDoc * pDoc = GetDocument ();
	const CPoint ptScroll = GetScrollPosition ();
	const double dZoom = GetZoom ();

	if (pDoc) {
		CPairArray sel = GetSelected (0);
		int nResult = IDNO;

		if (sel.GetSize ()) 
			nResult = MsgBox (* this, LoadString (IDS_CONFIRMELEMENTDELETE), 
				LoadString (IDS_CONFIRM), MB_YESNO | MB_ICONQUESTION);

		if (nResult == IDYES) {
			BeginWaitCursor ();

			TaskDoc::CSnapshot * pSnapshot = (TaskDoc::CSnapshot *)pDoc->GetSnapshot ();
			const CPoint ptScroll = GetScrollPosition ();
			CBoxParams & params = GetParams ();
			CBoundary workingHead;

			pSnapshot->SetElementsChanged (sel, TaskDoc::ELEMENT_DELETE);

			pDoc->GetBox ().GetWorkingHead (params, workingHead);

			for (int i = 0; i < sel.GetSize (); i++) 
				sel [i].m_pElement->SetSelected (false, this, sel [i], workingHead);
			
			pDoc->Erase (sel, this);

			for (int i = sel.GetSize () - 1; i >= 0; i--) {
				CElement * pElement = sel [i].m_pElement;
				CEditorElementList * pList = sel [i].m_pList;
				int nIndex = pList->GetIndex (pElement);

				pList->RemoveAt (nIndex);
				delete pElement;
			}

			pDoc->SetModifiedFlag (pSnapshot);
			EndWaitCursor ();
		}
	}
}

void CTaskView::OnUpdateElementsDelete(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable (theApp.m_uiSel.m_nSelCount > 0 && !GetDocument ()->GetReadOnly ());	
}

void CTaskView::OnElementsList() 
{
	CElementListDlg (GetLists (), GetDocument (), this).DoModal ();
}

void CTaskView::OnPropertiesMovable() 
{

	CTaskDoc & doc = * GetDocument ();
	CPairArray sel = GetSelected (0);

	BeginWaitCursor ();

	ASSERT (!doc.GetReadOnly ());	

	if (sel.GetSize ()) {
		TaskDoc::CSnapshot * pSnapshot = (TaskDoc::CSnapshot *)doc.GetSnapshot ();

		pSnapshot->SetElementsChanged (sel);

		for (int i = 0; i < sel.GetSize (); i++) {
			CElement & e = sel [i];

			e.SetMovable (!e.IsMovable ());
		}

		doc.SetModifiedFlag (pSnapshot);
		doc.Invalidate (sel, this);
	}

	EndWaitCursor ();
}

void CTaskView::OnPropertiesResizable() 
{

	CTaskDoc & doc = * GetDocument ();
	CPairArray sel = GetSelected (0);

	BeginWaitCursor ();

	ASSERT (!doc.GetReadOnly ());	

	if (sel.GetSize ()) {
		TaskDoc::CSnapshot * pSnapshot = (TaskDoc::CSnapshot *)doc.GetSnapshot ();

		pSnapshot->SetElementsChanged (sel);

		for (int i = 0; i < sel.GetSize (); i++) {
			CElement & e = sel [i];

			e.SetResizable (!e.IsResizable ());
		}

		doc.SetModifiedFlag (pSnapshot);
		doc.Invalidate (sel, this);
	}

	EndWaitCursor ();
}

void CTaskView::OnPropertiesFliph() 
{
	CTaskDoc & doc = * GetDocument ();

	BeginWaitCursor ();

	ASSERT (!doc.GetReadOnly ());	
	CPairArray sel = GetSelected (0);

	if (sel.GetSize ()) {
		TaskDoc::CSnapshot * pSnapshot = (TaskDoc::CSnapshot *)doc.GetSnapshot ();

		pSnapshot->SetElementsChanged (sel);

		for (int i = 0; i < sel.GetSize (); i++) {
			CElement & e = sel [i];

			e.GetElement ()->SetFlippedH (!e.GetElement ()->IsFlippedH ());
		}

		doc.SetModifiedFlag (pSnapshot);
		doc.Invalidate (sel, this);
	}

	EndWaitCursor ();
}

void CTaskView::OnPropertiesFlipv() 
{
	CTaskDoc & doc = * GetDocument ();

	BeginWaitCursor ();

	ASSERT (!doc.GetReadOnly ());	
	CPairArray sel = GetSelected (0);

	if (sel.GetSize ()) {
		TaskDoc::CSnapshot * pSnapshot = (TaskDoc::CSnapshot *)doc.GetSnapshot ();

		pSnapshot->SetElementsChanged (sel);

		for (int i = 0; i < sel.GetSize (); i++) {
			CElement & e = sel [i];

			e.GetElement ()->SetFlippedV (!e.GetElement ()->IsFlippedV ());
		}

		doc.SetModifiedFlag (pSnapshot);
		doc.Invalidate (sel, this);
	}

	EndWaitCursor ();
}

void CTaskView::OnPropertiesInverse() 
{
	CTaskDoc & doc = * GetDocument ();

	BeginWaitCursor ();

	ASSERT (!doc.GetReadOnly ());	
	CPairArray sel = GetSelected (0);

	if (sel.GetSize ()) {
		TaskDoc::CSnapshot * pSnapshot = (TaskDoc::CSnapshot *)doc.GetSnapshot ();

		pSnapshot->SetElementsChanged (sel);

		for (int i = 0; i < sel.GetSize (); i++) {
			CElement & e = sel [i];
		
			e.GetElement ()->SetInverse (!e.GetElement ()->IsInverse ());
		}

		doc.SetModifiedFlag (pSnapshot);
		doc.Invalidate (sel, this);
	}

	EndWaitCursor ();
}

void CTaskView::OnJumpdown() 
{
	MoveSelection (0, m_nMajorMove);
}

void CTaskView::OnJumpleft() 
{
	MoveSelection (-m_nMajorMove, 0);
}

void CTaskView::OnJumpright() 
{
	MoveSelection (m_nMajorMove, 0);
}

void CTaskView::OnJumpup() 
{
	MoveSelection (0, -m_nMajorMove);
}

void CTaskView::OnDown() 
{
	if (::GetFocus () == m_hWnd)
		OnVScroll (SB_LINEDOWN, 0, NULL);
}

void CTaskView::OnUp() 
{
	if (::GetFocus () == m_hWnd)
		OnVScroll (SB_LINEUP, 0, NULL);
}

void CTaskView::OnLeft() 
{
	if (::GetFocus () == m_hWnd)
		OnHScroll (SB_LINELEFT, 0, NULL);
}

void CTaskView::OnRight() 
{
	if (::GetFocus () == m_hWnd)
		OnHScroll (SB_LINERIGHT, 0, NULL);
}

void CTaskView::OnPagedown() 
{
	if (::GetFocus () == m_hWnd)
		OnVScroll (SB_PAGEDOWN, 0, NULL);
}

void CTaskView::OnPageleft() 
{
	if (::GetFocus () == m_hWnd)
		OnHScroll (SB_PAGELEFT, 0, NULL);
}

void CTaskView::OnPageright() 
{
	if (::GetFocus () == m_hWnd)
		OnHScroll (SB_PAGERIGHT, 0, NULL);
}

void CTaskView::OnPageup() 
{
	if (::GetFocus () == m_hWnd)
		OnVScroll (SB_PAGEUP, 0, NULL);
}

void CTaskView::OnHome() 
{
	if (::GetFocus () == m_hWnd)
		OnHScroll (SB_LEFT, 0, NULL);
}

void CTaskView::OnEnd() 
{
	if (::GetFocus () == m_hWnd)
		OnHScroll (SB_RIGHT, 0, NULL);
}

void CTaskView::OnBottom() 
{
	if (::GetFocus () == m_hWnd)
		OnVScroll (SB_BOTTOM, 0, NULL);
}

void CTaskView::OnTop() 
{
	if (::GetFocus () == m_hWnd)
		OnVScroll (SB_TOP, 0, NULL);
}

void CTaskView::OnMovedown() 
{
	MoveSelection (0, m_nMinorMove);
}

void CTaskView::OnMoveleft() 
{
	MoveSelection (-m_nMinorMove, 0);
}

void CTaskView::OnMoveright() 
{
	MoveSelection (m_nMinorMove, 0);
}

void CTaskView::OnMoveup() 
{
	MoveSelection (0, -m_nMinorMove);
}

void CTaskView::MoveSelection(int cx, int cy)
{
	CTaskDoc & doc = * GetDocument ();

	BeginWaitCursor ();

	if (!doc.GetReadOnly ()) {
		CPairArray sel = GetSelected (0);
		CString strLoc = theApp.GetStatusText (CMainFrame::LOCATION);

		if (sel.GetSize ()) {
			TaskDoc::CSnapshot * pSnapshot = (TaskDoc::CSnapshot *)doc.GetSnapshot ();
			const CPoint ptScroll = GetScrollPosition ();
			CBoundary workingHead;
			CBoxParams & params = GetParams ();

			pSnapshot->SetElementsChanged (sel);
			doc.GetBox ().GetWorkingHead (params, workingHead);

			for (int i = 0; i < sel.GetSize (); i++) {
				CElement & e = sel [i];
				CEditorElementList & list = sel [i];
				const CBoundary & head = list.m_bounds;
				const CPoint ptMove = CPoint (cx, cy);
				const CPoint pt = e.GetElement ()->GetPos (NULL);
				Element::CPair pair (&e, &list, &workingHead.m_card);

				CCoord coord (e.GetPos (head), doc.GetUnits (), head);
				bool bDisplayCoords = (strLoc == coord.Format ());
				CBoxParams & params = GetParams ();
				double dZoom = GetZoom ();
				const CRect rcHead [] = 
				{
					doc.m_box.GetHeadRect (head, params, true) + ptScroll,
					doc.m_box.GetHeadRect (head, params, false) + ptScroll,
				};

				e.GetElement ()->Move (ptMove, head);
				e.Confine (* GetDC (), rcHead, dZoom, head, workingHead);

				if (bDisplayCoords) {
					CCoord c (e.GetPos (head), doc.GetUnits (), head);
					theApp.SetStatusText (c.Format (), CMainFrame::LOCATION);
				}
			}

			doc.Invalidate (sel, this);
			doc.SetModifiedFlag (pSnapshot);
		}
	}

	EndWaitCursor ();
}

void CTaskView::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp) 
{
	if (m_bShowVRuler)
		lpncsp->rgrc [0].left += m_sizeRuler.cx;
	
	if (m_bShowHRuler)
		lpncsp->rgrc [0].top  += m_sizeRuler.cy;

	CBaseView::OnNcCalcSize(bCalcValidRects, lpncsp);
}

void CTaskView::OnNcPaint() 
{
	CWindowDC dc (this); 
	CTaskDoc & doc = * GetDocument ();
	CRect rcWin;
	CBoxParams & p = GetParams ();
	const CRect rcPanel = doc.m_box.GetPanelRect (p);
	const CPoint ptScroll = GetScrollPosition ();
	CPoint ptTL = rcPanel.TopLeft () - CPoint (1, 1);
	CPairArray vSel = GetSelected (0);
	CArray <CRect, CRect &> vRects;
	double dZoom = GetZoom ();
	CBoundary workingHead;

	doc.GetBox ().GetWorkingHead (p, workingHead);

	ptTL -= ptScroll;

	GetWindowRect (rcWin);
	rcWin.InflateRect (0, 0, m_sizeRuler.cx, m_sizeRuler.cy);
	ScreenToClient (rcWin);

	CRect rcHRuler = CRect (0, 0, rcWin.Width (), m_sizeRuler.cy);
	CRect rcVRuler = CRect (0, 0, m_sizeRuler.cx, rcWin.Width ());
	CRect rcIntersection = CRect (0, 0, rcVRuler.Width (), rcHRuler.Height ());

	rcHRuler.NormalizeRect ();
	rcVRuler.NormalizeRect ();

	CBaseView::OnNcPaint ();

	for (int i = 0; i < vSel.GetSize (); i++) {
		const CElement & e = vSel [i];
		CRect rc;

		if (m_bTrackingElement) 
			e.GetTrackerRect (rc);
		else {
			const CBoundary & hs = vSel [i].m_pList->m_bounds;
			CRect rcHead = doc.m_box.GetHeadRect (hs, p);

			rc = e.GetWindowRect (hs, workingHead) * dZoom;
			rc += ptTL;
			rc += CPoint (0, rcHead.top - rcPanel.top);
		}

		if (m_bShowHRuler)
			rc += CPoint (0, m_sizeRuler.cy);
		if (m_bShowVRuler)
			rc += CPoint (m_sizeRuler.cx, 0);

		vRects.Add (rc);
	}

	ULONG lHeadID = GetActiveHead ();
	CBoundary head; 

	if (!doc.GetHead (lHeadID, head))
		head.m_card = GetDefaultHead ();

	head = workingHead;

	if (m_bShowHRuler) {
		if (m_bShowVRuler) {
			rcHRuler += CPoint (m_sizeRuler.cx, 0);
			rcHRuler.DeflateRect (0, 0, m_sizeRuler.cx, 0);
		}

		dc.FillSolidRect (rcHRuler, ::GetSysColor (COLOR_INACTIVEBORDER));
		rcHRuler.DeflateRect (0, 0, 0, 1);
		DrawHRuler (dc, rcHRuler, ptTL, vRects, head);
	}

	if (m_bShowVRuler) {
		if (m_bShowHRuler) {
			rcVRuler += CPoint (0, m_sizeRuler.cy);
			rcVRuler.DeflateRect (0, 0, 0, m_sizeRuler.cy);
		}

		dc.FillSolidRect (rcVRuler, ::GetSysColor (COLOR_INACTIVEBORDER));
		rcVRuler.DeflateRect (0, 0, 1, 0);
		DrawVRuler (dc, rcVRuler, ptTL, vRects, head);
	}

	if (m_bShowVRuler && m_bShowHRuler)
		dc.FillSolidRect (rcIntersection, GetSysColor (COLOR_INACTIVEBORDER));
}

void CTaskView::OnViewRulersHorizonal() 
{
	CString str = m_strRegSection;

	str.Replace (_T ("CBoxParams"), _T ("Rulers"));
	m_bShowHRuler = !m_bShowHRuler;
	theApp.WriteProfileInt (str, _T ("Horizontal"), m_bShowHRuler);

	NCINVALIDATE (this);
}

void CTaskView::OnUpdateViewRulersHorizonal(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck (m_bShowHRuler ? 1 : 0);
}

void CTaskView::OnViewRulersVertical() 
{
	CString str = m_strRegSection;

	str.Replace (_T ("CBoxParams"), _T ("Rulers"));
	m_bShowVRuler = !m_bShowVRuler;
	theApp.WriteProfileInt (str, _T ("Vertical"), m_bShowVRuler);

	NCINVALIDATE (this);
}

void CTaskView::OnUpdateViewRulersVertical(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck (m_bShowVRuler ? 1 : 0);
}

void CTaskView::DrawHRuler (CDC & dc, const CRect & rc, const CPoint & ptOffset, 
							CArray <CRect, CRect &> & v, const FoxjetDatabase::HEADSTRUCT & head) const
{
	double dZoom = GetZoom ();
	UNITS units = GetDocument ()->GetUnits ();

	int nBkMode = dc.SetBkMode (TRANSPARENT);
	int nMapMode = dc.SetMapMode (MM_TEXT);
	COLORREF rgb = ::GetSysColor (COLOR_3DSHADOW);

	for (int i = 0; i < v.GetSize (); i++) {
		CRect rcElement = v [i];
		CRect rcShadow (rcElement.left, rc.bottom - m_sizeRuler.cy, rcElement.right, rc.bottom);
		
		dc.FillSolidRect (rcShadow, rgb);
	}

	double dUnitWidth = CCoord::GetPixelsPerXUnit (units, head); 

	if (units == HEAD_PIXELS) 
		DrawHorzUnits (dc, rc, ptOffset, dUnitWidth, 0, 10, dZoom, 200);
	else if (units == INCHES)
		DrawHorzUnits (dc, rc, ptOffset, dUnitWidth, 4, 8, dZoom);
	else if (units == CENTIMETERS) 
		DrawHorzUnits (dc, rc, ptOffset, dUnitWidth, 0, 5, dZoom);

	dc.SetBkMode (nBkMode);
	dc.SetMapMode (nMapMode);
}

void CTaskView::DrawVRuler (CDC & dc, const CRect & rc, const CPoint & ptOffset,
							CArray <CRect, CRect &> & v, const FoxjetDatabase::HEADSTRUCT & head) const
{
	double dZoom = GetZoom ();
	UNITS units = GetDocument ()->GetUnits ();
	COLORREF rgb = ::GetSysColor (COLOR_3DSHADOW);

	int nBkMode = dc.SetBkMode (TRANSPARENT);
	int nMapMode = dc.SetMapMode (MM_TEXT);

	for (int i = 0; i < v.GetSize (); i++) {
		CRect rcElement = v [i];
		CRect rcShadow (rc.right - m_sizeRuler.cx, rcElement.top, rc.right, rcElement.bottom);
		
		dc.FillSolidRect (rcShadow, rgb);
	}

	double dUnitWidth = CCoord::GetPixelsPerYUnit (units, head);

	if (units == HEAD_PIXELS)
		DrawVertUnits (dc, rc, ptOffset, dUnitWidth, 0, 10, dZoom, 200);
	else if (units == INCHES)
		DrawVertUnits (dc, rc, ptOffset, dUnitWidth, 4, 8, dZoom);
	else if (units == CENTIMETERS)
		DrawVertUnits (dc, rc, ptOffset, dUnitWidth, 0, 5, dZoom);

	dc.SetBkMode (nBkMode);
	dc.SetMapMode (nMapMode);
}

void CTaskView::DrawHorzUnits (CDC & dc, const CRect & rc, const CPoint & ptOffset, 
							   double dUnitWidth, int nMediumTicksPerUnit, int nMinorTicksPerUnit, 
							   double dZoom, int nFactor) const
{
	const double dZoomUnitWidth = dUnitWidth * dZoom; 
	double dInc = dZoomUnitWidth / nMinorTicksPerUnit;
	CPoint pt = CPoint (rc.left, rc.bottom);
	double x, y; 
	bool bMore = true;
	int nTicks = 0, nUnits = 0, nSkipUnits;
	TEXTMETRIC tm;

	if (dZoom <= 0.10) 
		nSkipUnits = 8;
	else if (dZoom <= 0.25) 
		nSkipUnits = 4;
	else if (dZoom <= 0.75)
		nSkipUnits = 2;
	else
		nSkipUnits = 1;

	dc.GetTextMetrics (&tm);
	x = (double)pt.x + ptOffset.x;
	y = (double)pt.y;

	while (bMore) {
		dc.MoveTo ((int)x, (int)y);

		if (nMediumTicksPerUnit && !(nTicks % nMediumTicksPerUnit)) {
			if (dZoom >= 0.25)
				dc.LineTo ((int)x, (int)(y - (double)m_nTickWidth [MEDIUM]));
		}

		if (nMinorTicksPerUnit && (nTicks % nMinorTicksPerUnit)) {
			if (dZoom >= 0.5)
				dc.LineTo ((int)x, (int)(y - (double)m_nTickWidth [MINOR]));
		}
		else {
			if (dZoom >= 0.25 || (!(nUnits % 2)))
				dc.LineTo ((int)x, (int)(y - (double)m_nTickWidth [MAJOR]));

			if (!(nUnits % nSkipUnits)) {
				CString str;
				int cx = (int)(x + ((double)tm.tmAveCharWidth / 2));
				int cy = (int)(y - ((double)tm.tmHeight / 1.5) - (double)m_nTickWidth [MAJOR]);
				str.Format (_T ("%d"), nUnits * nFactor);
				CSize extent = dc.GetTextExtent (str);
				CRect rcClip (cx, cy, cx + extent.cx, cy + extent.cy);
				rcClip.IntersectRect (rcClip, rc);
				dc.DrawText (str, &rcClip, DT_LEFT | DT_SINGLELINE);
			}

			nUnits++;
		}

		x += dInc;
		nTicks++;
		bMore = (x <= rc.right);
	}
}

void CTaskView::DrawVertUnits (CDC & dc, const CRect & rc, const CPoint & ptOffset, 
							   double dUnitWidth, int nMediumTicksPerUnit, int nMinorTicksPerUnit, 
							   double dZoom, int nFactor) const
{
	const double dZoomUnitWidth = dUnitWidth * dZoom; 
	double dInc = dZoomUnitWidth / nMinorTicksPerUnit;
	CPoint pt (rc.right, rc.top);
	double x, y; 
	bool bMore = true;
	int nTicks = 0, nUnits = 0, nSkipUnits;
	TEXTMETRIC tm;

	if (dZoom <= 0.10) 
		nSkipUnits = 8;
	else if (dZoom <= 0.25) 
		nSkipUnits = 4;
	else if (dZoom <= 0.75)
		nSkipUnits = 2;
	else
		nSkipUnits = 1;

	dc.GetTextMetrics (&tm);
	x = (double)pt.x;
	y = (double)pt.y + ptOffset.y;

	while (bMore) {
		dc.MoveTo ((int)x, (int)y);

		if (nMediumTicksPerUnit && !(nTicks % nMediumTicksPerUnit)) {
			if (dZoom >= 0.25)
				dc.LineTo ((int)(x - (double)m_nTickWidth [MEDIUM]), (int)y);
		}

		if (nMinorTicksPerUnit && (nTicks % nMinorTicksPerUnit)) {
			if (dZoom >= 0.5)
				dc.LineTo ((int)(x - (double)m_nTickWidth [MINOR]), (int)y);
		}
		else {
			if (dZoom >= 0.25 || (!(nUnits % 2)))
				dc.LineTo ((int)(x - (double)m_nTickWidth [MAJOR]), (int)y);

			if (!(nUnits % nSkipUnits)) {
				CString str;
				int cx = (int)(x - (double)(tm.tmAveCharWidth * 1.75) - (double)m_nTickWidth [MAJOR]);
				int cy = (int)(y + ((double)tm.tmHeight / 4));
				str.Format (_T ("%d"), nUnits * nFactor);
				CSize extent = dc.GetTextExtent (str);
				CRect rcClip (cx, cy, cx + extent.cx, cy + extent.cy);
				rcClip.IntersectRect (rcClip, rc);
				dc.DrawText (str, &rcClip, DT_LEFT | DT_SINGLELINE);
			}

			nUnits++;
		}

		y += dInc;
		nTicks++;
		bMore = (y <= rc.bottom);
	}
}

void CTaskView::OnDrawTrackerRect (CDC * pDC, const CRect & rc, DWORD dwElement)
{
	SendMessage (WM_NCPAINT);
}

void CTaskView::OnActivateFrame (UINT nState, CFrameWnd * pFrameWnd) 
{
}

void CTaskView::Invalidate (BOOL bErase)
{
	if (m_pLastParams) {
		delete m_pLastParams;
		m_pLastParams = NULL;
	}

	CBaseView::Invalidate (bErase);
	NCINVALIDATE (this);
}

BOOL CTaskView::RedrawWindow (LPCRECT lpRectUpdate, CRgn * prgnUpdate, UINT flags)
{
	if (!m_bInitUpdate)
		return FALSE;

	CTaskDoc * pDoc = GetDocument ();
	bool bInvalidate = pDoc->CountMDIFrames () > 1;

	BOOL bResult = CBaseView::RedrawWindow (lpRectUpdate, prgnUpdate, flags);
	
	if (bInvalidate) 
		Invalidate (); 
	else
		NCINVALIDATE (this);
	
	return bResult;
}

#ifdef _DEBUG
typedef struct tagVK_DEBUG_STRUCT
{
	tagVK_DEBUG_STRUCT (UINT nID, LPCTSTR lpsz) : m_nID (nID), m_lpsz (lpsz) { }

	UINT m_nID;
	LPCTSTR m_lpsz;
} VK_DEBUG_STRUCT;

#define VK_DEBUG_STRING(s) VK_DEBUG_STRUCT (s, _T (#s))

static const VK_DEBUG_STRUCT vkDebug [] =
{
	VK_DEBUG_STRING (VK_LBUTTON),        
	VK_DEBUG_STRING (VK_RBUTTON),        
	VK_DEBUG_STRING (VK_CANCEL),
	VK_DEBUG_STRING (VK_MBUTTON),
	VK_DEBUG_STRING (VK_BACK),
	VK_DEBUG_STRING (VK_TAB),
	VK_DEBUG_STRING (VK_CLEAR),
	VK_DEBUG_STRING (VK_RETURN),
	VK_DEBUG_STRING (VK_SHIFT),
	VK_DEBUG_STRING (VK_CONTROL),
	VK_DEBUG_STRING (VK_MENU),
	VK_DEBUG_STRING (VK_PAUSE),
	VK_DEBUG_STRING (VK_CAPITAL),
	VK_DEBUG_STRING (VK_KANA),
	VK_DEBUG_STRING (VK_HANGEUL),
	VK_DEBUG_STRING (VK_HANGUL),
	VK_DEBUG_STRING (VK_JUNJA),
	VK_DEBUG_STRING (VK_FINAL),
	VK_DEBUG_STRING (VK_HANJA),
	VK_DEBUG_STRING (VK_KANJI),
	VK_DEBUG_STRING (VK_ESCAPE),
	VK_DEBUG_STRING (VK_CONVERT),
	VK_DEBUG_STRING (VK_NONCONVERT),
	VK_DEBUG_STRING (VK_ACCEPT),
	VK_DEBUG_STRING (VK_MODECHANGE),
	VK_DEBUG_STRING (VK_SPACE),
	VK_DEBUG_STRING (VK_PRIOR),
	VK_DEBUG_STRING (VK_NEXT),
	VK_DEBUG_STRING (VK_END),
	VK_DEBUG_STRING (VK_HOME),
	VK_DEBUG_STRING (VK_LEFT),
	VK_DEBUG_STRING (VK_UP),
	VK_DEBUG_STRING (VK_RIGHT),
	VK_DEBUG_STRING (VK_DOWN),
	VK_DEBUG_STRING (VK_SELECT),
	VK_DEBUG_STRING (VK_PRINT),
	VK_DEBUG_STRING (VK_EXECUTE),
	VK_DEBUG_STRING (VK_SNAPSHOT),
	VK_DEBUG_STRING (VK_INSERT),
	VK_DEBUG_STRING (VK_DELETE),
	VK_DEBUG_STRING (VK_HELP),
	VK_DEBUG_STRING (VK_LWIN),
	VK_DEBUG_STRING (VK_RWIN),
	VK_DEBUG_STRING (VK_APPS),
	VK_DEBUG_STRING (VK_NUMPAD0),
	VK_DEBUG_STRING (VK_NUMPAD1),
	VK_DEBUG_STRING (VK_NUMPAD2),
	VK_DEBUG_STRING (VK_NUMPAD3),
	VK_DEBUG_STRING (VK_NUMPAD4),
	VK_DEBUG_STRING (VK_NUMPAD5),
	VK_DEBUG_STRING (VK_NUMPAD6),
	VK_DEBUG_STRING (VK_NUMPAD7),
	VK_DEBUG_STRING (VK_NUMPAD8),
	VK_DEBUG_STRING (VK_NUMPAD9),
	VK_DEBUG_STRING (VK_MULTIPLY),
	VK_DEBUG_STRING (VK_ADD),
	VK_DEBUG_STRING (VK_SEPARATOR),
	VK_DEBUG_STRING (VK_SUBTRACT),
	VK_DEBUG_STRING (VK_DECIMAL),
	VK_DEBUG_STRING (VK_DIVIDE),
	VK_DEBUG_STRING (VK_F1),
	VK_DEBUG_STRING (VK_F2),
	VK_DEBUG_STRING (VK_F3),
	VK_DEBUG_STRING (VK_F4),
	VK_DEBUG_STRING (VK_F5),
	VK_DEBUG_STRING (VK_F6),
	VK_DEBUG_STRING (VK_F7),
	VK_DEBUG_STRING (VK_F8),
	VK_DEBUG_STRING (VK_F9),
	VK_DEBUG_STRING (VK_F10),
	VK_DEBUG_STRING (VK_F11),
	VK_DEBUG_STRING (VK_F12),
	VK_DEBUG_STRING (VK_F13),
	VK_DEBUG_STRING (VK_F14),
	VK_DEBUG_STRING (VK_F15),
	VK_DEBUG_STRING (VK_F16),
	VK_DEBUG_STRING (VK_F17),
	VK_DEBUG_STRING (VK_F18),
	VK_DEBUG_STRING (VK_F19),
	VK_DEBUG_STRING (VK_F20),
	VK_DEBUG_STRING (VK_F21),
	VK_DEBUG_STRING (VK_F22),
	VK_DEBUG_STRING (VK_F23),
	VK_DEBUG_STRING (VK_F24),
	VK_DEBUG_STRING (VK_NUMLOCK),
	VK_DEBUG_STRING (VK_SCROLL),
	VK_DEBUG_STRING (VK_LSHIFT),
	VK_DEBUG_STRING (VK_RSHIFT),
	VK_DEBUG_STRING (VK_LCONTROL),
	VK_DEBUG_STRING (VK_RCONTROL),
	VK_DEBUG_STRING (VK_LMENU),
	VK_DEBUG_STRING (VK_RMENU),
	VK_DEBUG_STRING (VK_ATTN),
	VK_DEBUG_STRING (VK_CRSEL),
	VK_DEBUG_STRING (VK_EXSEL),
	VK_DEBUG_STRING (VK_EREOF),
	VK_DEBUG_STRING (VK_PLAY),
	VK_DEBUG_STRING (VK_ZOOM),
	VK_DEBUG_STRING (VK_NONAME),
	VK_DEBUG_STRING (VK_PA1),
	VK_DEBUG_STRING (VK_OEM_CLEAR),
};

static CString GetVkString (UINT nID)
{
	CString str;

	for (int i = 0; i < ARRAYSIZE (::vkDebug); i++) 
		if (::vkDebug [i].m_nID == nID)
			return ::vkDebug [i].m_lpsz;

	str.Format (_T ("%c"), (TCHAR)nID);

	return str;
}

#endif

BOOL CTaskView::PreTranslateMessage (MSG *pMsg)
{
	if (!m_hAccel)
		VERIFY (m_hAccel = ::LoadAccelerators (::AfxGetInstanceHandle (), MAKEINTRESOURCE (IDA_TASKVIEW)));

	#ifdef _DEBUG
	/*
	0�15	Specifies the repeat count for the current message. The value is the number of times the keystroke is autorepeated as a result of the user holding down the key. 
			The repeat count is always one for a WM_KEYUP message. 
	16�23	Specifies the scan code. The value depends on the original equipment manufacturer (OEM). 
	24		Specifies whether the key is an extended key, such as the right-hand ALT and CTRL keys that appear on an enhanced 101- or 102-key keyboard. 
			The value is 1 if it is an extended key; otherwise, it is 0. 
	25�28	Reserved; do not use. 
	29		Specifies the context code. The value is always 0 for a WM_KEYUP message. 
	30		Specifies the previous key state. The value is always 1 for a WM_KEYUP message. 
	31		Specifies the transition state. The value is always 1 for a WM_KEYUP message. 
	*/

	#pragma pack(1)
	typedef struct
	{
		unsigned short m_nCount			: 16;
		unsigned short m_nScanCode		: 8;
		unsigned short m_nExtendedKey	: 1;
		unsigned short m_nReserved		: 4;
		unsigned short m_nContext		: 1;
		unsigned short m_nPrevious		: 1;
		unsigned short m_nTransition	: 1;
	} KEYSTRUCT;
	#pragma pack()

	switch (pMsg->message) {
	case WM_KEYUP:
	case WM_KEYDOWN:
		switch (pMsg->wParam) {
		default:
		case VK_LEFT:
		case VK_RIGHT:
		case VK_UP:
		case VK_DOWN:
			CString str;
			KEYSTRUCT k;

			memcpy (&k, &pMsg->lParam, sizeof (KEYSTRUCT));

			/*
			str.Format (_T ("%d: %d (%-20s), 0x%p\t\tcount: %-4d, scan: %-4d, ext: %-4d, res: %-4d, context: %-4d, prev: %-4d, trans: %-4d"), 
				pMsg->message, pMsg->wParam, GetVkString (pMsg->wParam), pMsg->lParam,
				k.m_nCount,
				k.m_nScanCode,
				k.m_nExtendedKey,
				k.m_nReserved,
				k.m_nContext,
				k.m_nPrevious,
				k.m_nTransition);
			*/
			str.Format (_T ("%-15s %s"), GetVkString (pMsg->wParam), k.m_nPrevious ? _T ("UP") : _T ("DOWN"));
			//TRACEF (str);
		}
		break;
	}
	#endif

	if (::TranslateAccelerator (m_hWnd, m_hAccel, pMsg))
		return TRUE;
	
	return CBaseView::PreTranslateMessage(pMsg);
}

CRect CTaskView::InvalidateCrosshairs (bool bRedraw)
{
	CRect rc (0, 0, 0, 0);
	const CTaskDoc & doc = * GetDocument ();
	CBoxParams & p = GetParams ();
	CBoundaryArray vHeads;

	doc.GetHeads (vHeads);

	int nHeadIndex = CTask::Find (vHeads, p.m_lHeadID);

	if (nHeadIndex != -1) {
		CBoundary b = vHeads [nHeadIndex];
		HEADSTRUCT & head = b.m_card;
		const CRect rcHead = doc.m_box.GetHeadRect (b, p);
		const CPoint ptScroll = GetScrollPosition ();
		const CPoint ptOffset = rcHead.TopLeft () - ptScroll;
		const CSize size = (CSize (p.m_crosshairs.m_size) + CSize (1, 1)) * 2;
		const CPoint ptCrosshairs = p.m_crosshairs.m_pt;
	
		rc = CRect (ptCrosshairs, ptCrosshairs) + ptOffset;
		rc.InflateRect (size.cx, size.cy);

		if (bRedraw) {
			InvalidateRect (rc);
			UpdateWindow ();
		}
	}

	return rc;
}

void CTaskView::OnViewZoomCustom() 
{
	CBoxParams & p = GetParams ();
	double dZoom = GetZoom ();

	p.m_bOutlineOnly = true;
	CBaseView::OnViewZoomCustom (); 
	p.m_bOutlineOnly = false;

	if (GetZoom () != dZoom) { 
		p.m_lChange |= DxParams::ZOOM; // force update
		Invalidate ();
	}
	else
		RedrawWindow ();
}

BOOL CTaskView::OnPreparePrinting (CPrintInfo* pInfo)
{
	CTaskDoc & doc = * GetDocument ();
	const CPanel * pPanels [6] = { NULL };
	int nPages = 0;

	if (m_bUnfolded) 
		nPages = 1;
	else {
		if (doc.m_box.GetPanels (pPanels)) 
			for (int i = 0; i < 6; i++) 
				if (pPanels [i]->GetHeads ().GetSize ())
					nPages++;
	}

	pInfo->SetMaxPage (nPages);

	BOOL bResult = DoPreparePrinting (pInfo);

	return bResult;
}

void CTaskView::GetUserElements (CBaseElement & e, Foxjet3d::UserElementDlg::CUserElementArray & v, FoxjetDatabase::HEADSTRUCT & head, bool bTaskStart)
{
	#ifdef __WINPRINTER__
	using namespace FoxjetElements;
	#endif

	#ifdef __WINPRINTER__
	if (CUserElement * p = DYNAMIC_DOWNCAST (CUserElement, &e)) {
		if ((bTaskStart && p->m_bPromptAtTaskStart) || !bTaskStart)
			if (!Foxjet3d::UserElementDlg::CUserElementDlg::Find (p->GetPrompt (), v))
				v.Add (Foxjet3d::UserElementDlg::CUserItem (p, &head));
	}
	else if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, &e)) {
		if ((bTaskStart && p->GetPromptAtTaskStart ()) || !bTaskStart)
			if (!Foxjet3d::UserElementDlg::CUserElementDlg::Find (p->GetPrompt (), v))
				v.Add (Foxjet3d::UserElementDlg::CUserItem (p, &head));
	}
	else if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, &e)) {
		if (p->IsDatabaseLookup ()) {
			if ((bTaskStart && p->GetDatabasePromptAtTaskStart ()) || !bTaskStart)
				if (!Foxjet3d::UserElementDlg::CUserElementDlg::Find (p->GetDatabasePrompt (), v))
					v.Add (Foxjet3d::UserElementDlg::CUserItem (p, &head));
		}
		else if (p->IsUserPrompted ()) {
			if ((bTaskStart && p->IsPromptAtTaskStart ()) || !bTaskStart)
				if (!Foxjet3d::UserElementDlg::CUserElementDlg::Find (p->GetUserPrompt (), v))
					v.Add (Foxjet3d::UserElementDlg::CUserItem (p, &head));
		}
	}
	else if (CExpDateElement * p = DYNAMIC_DOWNCAST (CExpDateElement, &e)) { // sw0924
		switch (p->GetType ()) {
		case CExpDateElement::TYPE_DATABASE:
			if (p->GetKeyField ().GetLength () > 0) 
				if ((bTaskStart && p->GetPromptAtTaskStart ()) || !bTaskStart)
					if (!Foxjet3d::UserElementDlg::CUserElementDlg::Find (p->GetPrompt (), v))
						v.Add (Foxjet3d::UserElementDlg::CUserItem (p, &head));
			break;
		case CExpDateElement::TYPE_USER:
			if ((bTaskStart && p->GetPromptAtTaskStart ()) || !bTaskStart)
				if (!Foxjet3d::UserElementDlg::CUserElementDlg::Find (p->GetPrompt (), v))
					v.Add (Foxjet3d::UserElementDlg::CUserItem (p, &head));
			break;
		}
	}
	else if (CBarcodeElement * p = DYNAMIC_DOWNCAST (CBarcodeElement, &e)) {
		for (int i = 0; i < p->GetSubElementCount (); i++)
			GetUserElements (* p->GetSubElement (i).GetElement (), v, head, bTaskStart);
	}
	//else {
	//	if (FoxjetCommon::IsProductionConfig ()) //#if __CUSTOM__ == __SW0833__ 
	//		if (CLocalHead * pLocalHead = DYNAMIC_DOWNCAST (CLocalHead, pHead)) 
	//			if (GlobalFuncs::GetTestPHC (info.GetIOAddress ())) 
	//				if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, &e)) 
	//					v.Add (Foxjet3d::UserElementDlg::CUserItem (p, &info.m_Config.m_HeadSettings));
	//}

	#endif //__WINPRINTER__
}

void CTaskView::GetUserElements (CEditorElementList & list, Foxjet3d::UserElementDlg::CUserElementArray & v, FoxjetDatabase::HEADSTRUCT & head)
{
	#ifdef __WINPRINTER__
	using namespace FoxjetElements;
	bool bTaskStart = true;
	#endif

	using namespace UserElementDlg;

	for (int nElement = 0; nElement < list.GetSize (); nElement++) {
		CElement & e = list.GetElement (nElement);

		GetUserElements (* e.GetElement (), v, head, bTaskStart);
	}
}

bool CTaskView::DoUserPrompt ()
{
	bool bResult = false;

	#ifdef __WINPRINTER__
	using namespace FoxjetElements;
	#endif

	using namespace UserElementDlg;

	CTaskDoc & doc = * GetDocument ();
	CUserElementDlg dlg (doc.m_task.m_lLineID, ::AfxGetMainWnd ());
	const FoxjetDatabase::COrientation orient (HIWORD (doc.m_task.m_lTransformation) + 1, (ORIENTATION)LOWORD (doc.m_task.m_lTransformation));
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
	CArray <CEditorElementList *, CEditorElementList *> vLists;

	BeginWaitCursor ();
	m_mapPreview.RemoveAll ();

	//VERIFY (GetLineHeads (theApp.GetDB (), doc.m_task.m_lLineID, vHeads));
	VERIFY (GetPrinterHeads (theApp.GetDB (), GetPrinterID (theApp.GetDB ()), vHeads));

	for (int nPanel = 0; nPanel < 6; nPanel++) {
		const CPanel * pPanel = NULL;
		
		if (doc.m_box.GetPanel (nPanel, &pPanel)) {
			const PANELSTRUCT & panel = pPanel->GetMembers ();
			CBoundaryArray vBounds;

			pPanel->GetHeads (doc.m_box, panel.m_lID, vBounds);

			for (int nBoundary = 0; nBoundary < vBounds.GetSize (); nBoundary++) {
				int nHeadIndex = CTask::Find (vHeads, vBounds [nBoundary].m_card.m_lID);
				ASSERT (nHeadIndex != -1);

				if (nHeadIndex != -1) {
					HEADSTRUCT & head = vHeads [nHeadIndex];

					TRACEF (vBounds [nBoundary].m_card.m_strName);

					if (CEditorElementList * pList = GetList (vBounds [nBoundary].GetID ())) {
						CEditorElementList  * pTmpList = new CEditorElementList ();
						CString str = pList->ToString (verApp, CProductArea ());

						vLists.Add (pTmpList);
						pTmpList->FromString (str, CProductArea (), INCHES, head, CVersion ());
						pTmpList->SetHead (head.m_lID);

						GetUserElements (* pTmpList, dlg.m_vElements, head);
					}
				}
			}
		}
	}

	if (!dlg.m_vElements.GetSize ()) 
		bResult = true;
	else {
		CString strKeyValue;
		FoxjetDatabase::USERSTRUCT user;
		FoxjetUtils::CDatabaseStartDlg dlgDB (NULL);

		dlgDB.Load (theApp.GetDB ());

		if (dlg.DoModal () == IDOK) {
			bResult = true;

			for (int i = 0; i < dlg.m_vElements.GetSize (); i++) {
				Foxjet3d::UserElementDlg::CUserElementArray vPrompt;
				Foxjet3d::UserElementDlg::CUserElementDlg::Get (dlg.m_vElements [i].m_pElement, dlg.m_vElements [i].m_head, vPrompt, true);

				for (int j = 0; j < vPrompt.GetSize (); j++) {
					TRACEF (vPrompt [j].m_strPrompt + _T (" --> ") + vPrompt [j].m_strData);
					m_mapPreview.SetAt (vPrompt [j].m_strPrompt, vPrompt [j].m_strData);
					Foxjet3d::UserElementDlg::CUserElementDlg::Set (dlg.m_vElements [i].m_pElement, dlg.m_vElements [i].m_head, vPrompt [j].m_strPrompt, vPrompt [j].m_strData, strKeyValue, user, dlgDB);
				}
			}
		}
	}

	{ 
		while (vLists.GetSize () > 0) {
			CEditorElementList * p = vLists [0];

			vLists.RemoveAt (0);
			delete p;
		}
	}

	EndWaitCursor ();

	return bResult;
}

#include "afxpriv.h"

class CTaskPreviewView : public CPreviewView 
{
public:
	DECLARE_DYNCREATE(CTaskPreviewView)

	afx_msg void OnUnfolded ();

	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);

	DECLARE_MESSAGE_MAP()
};

IMPLEMENT_DYNCREATE(CTaskPreviewView, CPreviewView)

BEGIN_MESSAGE_MAP(CTaskPreviewView, CPreviewView)
	ON_BN_CLICKED(BTN_UNFOLDED, OnUnfolded)
END_MESSAGE_MAP()

void CTaskPreviewView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView)
{
	if (bActivate && m_pToolBar->IsWindowVisible ()) {
		if (CButton * p = (CButton *)m_pToolBar->GetDlgItem (BTN_UNFOLDED)) {
			bool bUnfolded = theApp.GetProfileInt (_T ("Settings\\CTaskPreviewView"), _T ("unfolded"), 0) ? true : false;
			
			m_pPrintView->SendMessage (WM_SETUNFOLDED, bUnfolded);
			p->SetCheck (bUnfolded ? 1 : 0);
		}
	}

	CPreviewView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}

void CTaskPreviewView::OnUnfolded ()
{
	if (CButton * pUnfolded = (CButton *)m_pToolBar->GetDlgItem (BTN_UNFOLDED)) {
		bool bUnfolded = pUnfolded->GetCheck () == 1 ? true : false;

		TRACEF (bUnfolded ? _T ("checked") : _T ("unchecked"));
		m_pPrintView->SendMessage (WM_SETUNFOLDED, bUnfolded);
		m_dcPrint.Detach ();
		SetPrintView (m_pPrintView);
	}

	REPAINT (this);
}

LRESULT CTaskView::OnSetUnfolded (WPARAM wParam, LPARAM lParam) 
{
	m_bUnfolded = wParam ? true : false;
	theApp.WriteProfileInt (_T ("Settings\\CTaskPreviewView"), _T ("unfolded"), m_bUnfolded);

	return 0;
}


void CTaskView::OnFilePrintPreview ()
{
	if (DoUserPrompt ()) {
		//CBaseView::OnFilePrintPreview ();

		// In derived classes, implement special window handling here
		// Be sure to Unhook Frame Window close if hooked.

		// must not create this on the frame.  Must outlive this function
		CPrintPreviewState* pState = new CPrintPreviewState;

		TRY
		{
			// DoPrintPreview's return value does not necessarily indicate that
			// Print preview succeeded or failed, but rather what actions are necessary
			// at this point.  If DoPrintPreview returns TRUE, it means that
			// OnEndPrintPreview will be (or has already been) called and the
			// pState structure will be/has been deleted.
			// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
			// WILL NOT be called and that cleanup, including deleting pState
			// must be done here.

			if (!DoPrintPreview(IDD_PRINT_PREVIEW /* AFX_IDD_PREVIEW_TOOLBAR */, this, RUNTIME_CLASS(CTaskPreviewView), pState))
			{
				// In derived classes, reverse special window handling here for
				// Preview failure case

				TRACE(traceAppMsg, 0, "Error: DoPrintPreview failed.\n");
				AfxMessageBox(AFX_IDP_COMMAND_FAILURE);
				delete pState;      // preview failed to initialize, delete State now
			}
		}
		CATCH_ALL(e)
		{
			delete pState;
			THROW_LAST();
		}
		END_CATCH_ALL
	}
}

void CTaskView::OnFilePrint ()
{
	if (!m_mapPreview.GetCount ())
		if (!DoUserPrompt ())
			return;

	CBaseView::OnFilePrint ();
	m_mapPreview.RemoveAll ();
}

void CTaskView::OnPrint (CDC* pDC, CPrintInfo* pInfo)
{
	BeginWaitCursor ();

	CTaskDoc & doc = * GetDocument ();
	const CPanel * pPanels [6] = { NULL };
	CFont fntHeader;
	int nPages = 0;
	const int nFontSize = pDC->GetDeviceCaps (LOGPIXELSY) / 8;
	const int nBorder = 10;
	CDC dcMem;
	CBitmap bmpMem;

	bmpMem.CreateBitmap (1, 1, 1, 32, NULL); // dc will be monochrome otherwise
	dcMem.CreateCompatibleDC (CTmpDC (NULL)); 
	CBitmap * pMem = dcMem.SelectObject (&bmpMem);

	fntHeader.CreateFont (nFontSize, 0, 0, 0, FW_BOLD, FALSE, 0, 0,
		DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("Verdana")); 
	CFont * pFont = pDC->SelectObject (&fntHeader);
//	int nMapMode = pDC->SetMapMode (MM_TEXT);

	//if (CButton * p = (CButton *)m_pToolBar->GetDlgItem (BTN_UNFOLDED))
	//	TRACEF (p->GetCheck () ? _T ("checked") : _T ("unchecked"));

	if (m_bUnfolded) {
		if (doc.m_box.GetPanels (pPanels)) {
			CRect rcFront, rcLeft, rcRight, rcBack, rcTop, rcBottom, rcUnion (0, 0, 0, 0);
			CRect rc [6];
			CRect rectDraw = pInfo->m_rectDraw;

			rectDraw.DeflateRect (0, 0, 0, pDC->GetTextExtent (_T ("X")).cy);

			for (int i = 0; i < 6; i++) {
				if (const CPanel * pPanel = pPanels [i]) {
					CBoundary h;
					doc.m_box.GetWorkingHead (h);
					CSize3 size = doc.m_box.GetSize (pPanel->GetMembers ().m_nNumber, h, pPanel->GetMembers ().m_orientation);
					int cx = (int)size.GetZ (), cy = (int)size.GetY ();

					rc [pPanel->m_face] = CRect (0, 0, cx, cy);
					
					#ifdef _DEBUG
					LPCTSTR lpsz [] = {	_T ("FRONT"), _T ("RIGHT"), _T ("BACK"), _T ("LEFT"), _T ("TOP"), _T ("BOTTOM"),  };
					CString str;
					BITMAP bm = { 0 };

					//m_pPreview->m_bmp [pPanel->m_face].GetBitmap (bm);

					str.Format (_T ("%s: %f, %f, %f (%d, %d [%f]), bm: %d x %d"), 
						lpsz [pPanel->m_face], 
						size.GetX (), size.GetY (), size.GetZ (),
						rc [pPanel->m_face].Width (), rc [pPanel->m_face].Height (),
						rc [pPanel->m_face].Height () / (double)rc [pPanel->m_face].Width (),
						bm.bmWidth, bm.bmHeight);
					TRACEF (str);
					int nBreak = 0;
					#endif
				}
			}

			for (int i = 0; i < ARRAYSIZE (rc); i++) 
				rc [i] += rectDraw.TopLeft ();

			rc [FRONT] += CPoint ((rectDraw.Width () - rc [FRONT].Width ()) / 2, (rectDraw.Height () - rc [FRONT].Height ()) / 2);
			rc [FRONT] += CPoint (rc [FRONT].Width () / 2, 0);

			rc [BOTTOM] += CPoint (rc [FRONT].left - rc [BOTTOM].Width (), rc [FRONT].top);
			rc [BACK] += CPoint (rc [BOTTOM].left - rc [BACK].Width (), rc [BOTTOM].top);
			rc [TOP] += CPoint (rc [FRONT].right, rc [FRONT].top);

			rc [Foxjet3d::RIGHT] += CPoint (rc [FRONT].left, rc [FRONT].bottom);
			rc [Foxjet3d::LEFT] += CPoint (rc [FRONT].left, rc [FRONT].top - rc [Foxjet3d::LEFT].Height ());

			for (int i = 0; i < ARRAYSIZE (rc); i++) 
				rcUnion.UnionRect (rcUnion, rc [i]);

			double dScale = min ((double)rectDraw.Width () / (double)rcUnion.Width (), (double)rectDraw.Height () / (double)rcUnion.Height ());

			for (int i = 0; i < ARRAYSIZE (rc); i++) {
				rc [i].left		*= dScale;
				rc [i].top		*= dScale;
				rc [i].right	*= dScale;
				rc [i].bottom	*= dScale;

				rc [i] -= CPoint (rcUnion.left * dScale, rcUnion.top * dScale);
				rc [i] += CPoint (0, pInfo->m_rectDraw.bottom - rectDraw.bottom);
				rc [i] += CPoint ((rectDraw.Width () - (rcUnion.Width () * dScale)) / 2, 0);
			}

			rc [BACK]				+= CPoint (nBorder * 2, 0);
			rc [BOTTOM]				+= CPoint (nBorder, 0);
			rc [TOP]				-= CPoint (nBorder, 0);
			rc [Foxjet3d::RIGHT]	-= CPoint (0, nBorder);
			rc [Foxjet3d::LEFT]		+= CPoint (0, nBorder);

			if (!m_pPreview) {
				TASKSTRUCT t = doc.m_task;

				t.m_vMsgs.RemoveAll ();

				for (int i = 0; i < doc.m_task.GetMessageCount (); i++) {
					if (CMessage * pMsg = doc.m_task.GetMessage (i)) {
						MESSAGESTRUCT msg (pMsg->GetMembers ());

						msg.m_strData = pMsg->m_list.ToString (CVersion (), CProductArea ());
						//TRACEF (msg.m_strData);
						t.m_vMsgs.Add (msg);
					}
				}

				m_pPreview = new OpenDlg::CTaskPreview (t, dcMem /* * pDC */, rectDraw, m_mapPreview); 
			}

			for (int f = Foxjet3d::FRONT; f <= Foxjet3d::BOTTOM; f++) {
				CRect r = rc [f];
				CxImage imgPanel;
				CString strFile = pPanels [f]->m_img.GetMembers ().m_strImage;
				CBoundary h;

				doc.m_box.GetWorkingHead (h);

				if (strFile.GetLength ()) {
					imgPanel.Load (strFile);
					imgPanel.Rotate (90);

					switch (f) {
					case Foxjet3d::BACK:
						imgPanel.Rotate180 ();
						break;
					}

					imgPanel.Stretch (pDC->m_hDC, rc [f]);
				}

				if (HBITMAP hbmp = m_pPreview->m_bmp [f].GetHBITMAP ()) {
					CxImage img;
					const CSize3 size = doc.m_box.GetSize (pPanels [f]->GetMembers ().m_nNumber, h, pPanels [f]->GetMembers ().m_orientation);

					img.CreateFromHBITMAP (hbmp);
					img.Crop (0, 0, (int)size.GetY (), img.GetHeight ());
					img.Rotate (90);

					switch (f) {
					case Foxjet3d::BACK:
						img.Rotate180 ();
						break;
					}

					img.Stretch (pDC->m_hDC, rc [f], SRCAND);
				}

				for (int j = 0; j < nBorder; j++, r.DeflateRect (1, 1))
					pDC->FrameRect (r, &CBrush (rgbLightGray));
			}

			#if 0 // #ifdef _DEBUG
			{ 
				COLORREF rgb [6] = 
				{
						Color::rgbRed,
						Color::rgbGreen,
						Color::rgbBlue,
						Color::rgbYellow,
						Color::rgbLightGray,
						Color::rgbBlack,
				};

				for (int i = 0; i < ARRAYSIZE (rc); i++) {
					CRect r = rc [i];

					pDC->TextOut (r.left, r.top, ToString (i));

					for (int j = 0; j < nBorder; j++) {
						pDC->FrameRect (r, &CBrush (rgb [i]));
						r.DeflateRect (1, 1);
					}
				}
			}
			#endif

			{
				CString str, strTitle = doc.GetTitle ();
				CRect rcHeader (rectDraw);
				strTitle.Replace (FoxjetFile::GetModifiedSuffix (), _T (""));
				CSize size;

				HDC hdcScreen = ::GetDC (NULL);
				double dScreen = (double)pDC->GetDeviceCaps (HORZRES) / (double)pDC->GetDeviceCaps (LOGPIXELSX);
				double dPrint = (double)rcUnion.Width () / ::GetDeviceCaps (hdcScreen, LOGPIXELSX);
				double d = (dScreen / dPrint) * 100.0;
				::ReleaseDC (NULL, hdcScreen);

				const int nSetBkMode = pDC->SetBkMode (TRANSPARENT);

				str.Format (_T ("%s [%.02f %%]"), strTitle, d);
				pDC->TextOut (rcHeader.left, rcHeader.top, str);

				str = doc.GetVersion ().Format ();
				size = pDC->GetTextExtent (str);
				pDC->TextOut (rcHeader.left + ((rcHeader.Width () - size.cx) / 2), rcHeader.top, str);

				str = CTime::GetCurrentTime ().Format (_T ("%c"));
				size = pDC->GetTextExtent (str);
				pDC->TextOut (rcHeader.left + (rcHeader.Width () - size.cx), rcHeader.top, str);

				pDC->SetBkMode (nSetBkMode);
			}
		}
	}
	else {
		if (doc.m_box.GetPanels (pPanels)) {
			for (int i = 0; i < 6; i++) {
				const CPanel * pPanel = pPanels [i];

				if (pPanel->GetHeads ().GetSize ()) {
					nPages++;
				
					if (pInfo->m_nCurPage == (UINT)nPages) {
						CString str;
						CSize size;
						CRect rcHeader (pInfo->m_rectDraw);
						CRect rcPanel (pInfo->m_rectDraw);
						CBitmap bmp;

						rcHeader.bottom = rcHeader.top + nFontSize;
						rcPanel.top = rcHeader.bottom;
					
						int nIndex = pPanel->GetMembers ().m_nNumber - 1;

						ASSERT (nIndex >= 0 && nIndex < 6);

						if (!m_pPreview) {
							TASKSTRUCT t = doc.m_task;

							t.m_vMsgs.RemoveAll ();

							for (int i = 0; i < doc.m_task.GetMessageCount (); i++) {
								if (CMessage * pMsg = doc.m_task.GetMessage (i)) {
									MESSAGESTRUCT msg (pMsg->GetMembers ());

									msg.m_strData = pMsg->m_list.ToString (CVersion (), CProductArea ());
									//TRACEF (msg.m_strData);
									t.m_vMsgs.Add (msg);
								}
							}

							m_pPreview = new OpenDlg::CTaskPreview (t, dcMem /* * pDC */, rcPanel, m_mapPreview);
						}

						m_pPreview->m_bmp [nIndex].GetBitmap (bmp);

						if (bmp.m_hObject) {
							DIBSECTION ds;

							::ZeroMemory (&ds, sizeof (ds));

							if (bmp.GetObject (sizeof (ds), &ds)) {
								CSize size (ds.dsBm.bmWidth, ds.dsBm.bmHeight);

								if (size.cx > 0 && size.cy > 0) {
									CDC dcMem;
									int cx = 0, cy = 0;
									BITMAP bm;

									VERIFY (dcMem.CreateCompatibleDC (pDC));
									CGdiObject * pOldObject = dcMem.SelectObject (&bmp);
									bmp.GetObject (sizeof (bm), &bm);

									{
										double d [] = 
										{
											(double)rcPanel.Width () / (double)bm.bmWidth,
											(double)rcPanel.Height () / (double)bm.bmHeight,
										};
										double dZoom = min (d [0], d [1]);

										cx = (int)((double)bm.bmWidth * dZoom);
										cy = (int)((double)bm.bmHeight * dZoom);
									}

									{
										CString strTitle = doc.GetTitle ();
										strTitle.Replace (FoxjetFile::GetModifiedSuffix (), _T (""));

										HDC hdcScreen = ::GetDC (NULL);
										double dScreen = (double)pDC->GetDeviceCaps (HORZRES) / (double)pDC->GetDeviceCaps (LOGPIXELSX);
										double dPrint = (double)size.cx / ::GetDeviceCaps (hdcScreen, LOGPIXELSX);
										double d = (dScreen / dPrint) * 100.0;
										::ReleaseDC (NULL, hdcScreen);

										str.Format (_T ("%s, %s [%.02f %%]"), strTitle, pPanel->GetMembers ().m_strName, d);
										pDC->TextOut (rcHeader.left, rcHeader.top, str);

										str = doc.GetVersion ().Format ();
										size = pDC->GetTextExtent (str);
										pDC->TextOut (rcHeader.left + ((rcHeader.Width () - size.cx) / 2), rcHeader.top, str);

										str = CTime::GetCurrentTime ().Format (_T ("%c"));
										size = pDC->GetTextExtent (str);
										pDC->TextOut (rcHeader.left + (rcHeader.Width () - size.cx), rcHeader.top, str);
									}

									if (pInfo->m_bPreview) {	
										CRect rc = CRect (0, 0, cx, cy) + rcPanel.TopLeft ();
										CxImage img;
										CString strImage = pPanel->m_img.GetMembers ().m_strImage;
										DWORD dwType = GetCxImageType (strImage);
									
										if (strImage.GetLength ()) {
											TRACEF (strImage);

											if (img.Load (strImage, dwType)) {
												CDC dcTmp;
												int cx = img.GetWidth ();
												int cy = img.GetHeight ();

												CxBitmap hBmp (img.MakeBitmap (NULL));
												VERIFY (dcTmp.CreateCompatibleDC (pDC));
							
												CGdiObject * pOld = dcTmp.SelectObject (CBitmap::FromHandle (hBmp));

												::StretchBlt (pDC->m_hDC, rc.left, rc.top, rc.Width (), rc.Height (), dcTmp, 0, 0, cx, cy, SRCCOPY);

												dcTmp.SelectObject (pOld);
											}
										}
									}

									pDC->StretchBlt (rcPanel.left, rcPanel.top, cx, cy, &dcMem, 0, 0, bm.bmWidth, bm.bmHeight, SRCAND);

									if (pInfo->m_bPreview) {	
										CRect rc = CRect (0, 0, cx, cy) + rcPanel.TopLeft ();

										for (int i = 0; i < nBorder; i++) {
											pDC->FrameRect (rc, &CBrush (rgbDarkGray));
											rc.DeflateRect (1, 1);
										}
									}

									dcMem.SelectObject (pOldObject);
								}
							}
						}
					}
				}
			}
		}
	}


	if (m_bUnfolded) {
		CString str = doc.m_task.m_strNotes;

		str.Trim ();

		if (str.GetLength ()) {
			CPoint pt = pInfo->m_rectDraw.TopLeft ();
			CSize size = pDC->GetTextExtent (str);
			CRect rc = pInfo->m_rectDraw;
			int nBorder = pDC->GetTextExtent ("W").cy * 1;
			CBrush brush (rgbBlack);

			rc.left += nBorder * 3;
			rc.top += nBorder * 3;

			int nSetBkMode = pDC->SetBkMode (TRANSPARENT);
			pDC->DrawText (str, &rc, 0);
			pDC->DrawText (str, &rc, DT_CALCRECT);

			rc.InflateRect (nBorder, nBorder);

			for (int i = 0; i < 8; i++) {
				pDC->FrameRect (rc, &brush);
				rc.InflateRect (1, 1);
			}

			pDC->SetBkMode (nSetBkMode);
		}
	}

	pDC->SelectObject (pFont);
//	pDC->SetMapMode (nMapMode);
	dcMem.SelectObject (pMem);

	EndWaitCursor ();
}

void CTaskView::OnEndPrinting (CDC* pDC, CPrintInfo* pInfo) 
{
	if (m_pPreview) {
		delete m_pPreview;
		m_pPreview = NULL;
	}
}

void CTaskView::OnElementNext ()
{
	CBoundary head;

	GetParams ().GetActiveHead (GetDocument ()->m_box, head);

	CPairArray sel = GetSelected (false, head.m_card.m_lID);
	CPairArray all = GetSelected (true, head.m_card.m_lID);
	CTaskDoc * pDoc = GetDocument ();

	if (CWnd * p = CWnd::GetFocus ())
		TRACEF (p->GetRuntimeClass ()->m_lpszClassName);
	else
		TRACEF ("CWnd::GetFocus: NULL");

	if (sel.GetSize () == 0) {
		UpdateSelection (sel, GetMinID (all));
		return;
	}

	if (CElement * pMax = GetMaxID (sel).m_pElement) {
		CPairArray next;

		for (int i = 0; i < all.GetSize (); i++) 
			if (CElement * p = all [i].m_pElement) 
				if (p->GetElement ()->GetID () > pMax->GetElement ()->GetID ())
					next.Add (all [i]);

		CPair tmp = GetMinID (next);

		if (!tmp.m_pElement) // end of list, get first
			tmp = GetMinID (all);

		UpdateSelection (sel, tmp);
	}
}

void CTaskView::OnElementPrev ()
{
	CBoundary head;

	GetParams ().GetActiveHead (GetDocument ()->m_box, head);

	CPairArray sel = GetSelected (false, head.m_card.m_lID);
	CPairArray all = GetSelected (true, head.m_card.m_lID);
	CTaskDoc * pDoc = GetDocument ();

	if (sel.GetSize () == 0) {
		UpdateSelection (sel, GetMaxID (all));
		return;
	}

	if (CElement * pMin = GetMinID (sel).m_pElement) {
		CPairArray next;

		for (int i = 0; i < all.GetSize (); i++) 
			if (CElement * p = all [i].m_pElement) 
				if (p->GetElement ()->GetID () < pMin->GetElement ()->GetID ())
					next.Add (all [i]);

		CPair tmp = GetMaxID (next);

		if (!tmp.m_pElement) // end of list, get first
			tmp = GetMaxID (all);

		UpdateSelection (sel, tmp);
	}
}

void CTaskView::UpdateSelection (Element::CPairArray & sel, Element::CPair & pair)
{
	CBoxParams & params = GetParams ();
	const CPoint ptScroll = GetScrollPosition ();
	const double dZoom = GetZoom ();
	CTaskDoc * pDoc = GetDocument ();
	CBoundary workingHead;

	pDoc->m_box.GetWorkingHead (params, workingHead);

	if (CElement * pNext = pair.m_pElement) {
		CRect rcInvalid;

		sel.Add (pair); // calc invalid rect for pair too

		for (int i = 0; i < sel.GetSize (); i++) {
			HEADSTRUCT head = sel [i].m_pList->m_bounds;
			CBoundary bounds = sel [i].m_pList->m_bounds;
			CElement & e = * sel [i].m_pElement;
			const CRect rcHead = pDoc->GetBox ().GetHeadRect (bounds, params) - ptScroll;
			CRect rcElement = (e.GetWindowRect (head, workingHead) * dZoom) + rcHead.TopLeft ();

			rcElement.InflateRect (1, 1);	
			rcInvalid.UnionRect (rcInvalid, rcElement);
		}

		pDoc->SelectAll (false);

		if (!pDoc->m_box.IsLinked (pair.m_pList->m_bounds))
			pNext->SetSelected (true, this, * pair.m_pList, workingHead);

		pDoc->UpdateUIStructs ();
		InvalidateRect (rcInvalid);
		UpdateWindow ();
	}
}

LRESULT CTaskView::OnRedraw(WPARAM wParam, LPARAM lParam)
{
	BeginWaitCursor ();

	CBoxParams & p = GetParams ();
	const int nPanel = p.m_nPanel;

	p.m_nPanel = ((p.m_nPanel + 1) % 6) + 1;
	OnPaint ();

	p.m_nPanel = nPanel;
	OnPaint ();

	EndWaitCursor ();

	return 0;
}

void CTaskView::OnBringToFront()
{
	CElementPtrArray v;
	CTaskDoc * pDoc = GetDocument ();
	CBoundary workingHead;

	pDoc->m_box.GetWorkingHead (workingHead);

	if (CEditorElementList * pList = GetList (GetActiveHead ())) {
		TaskDoc::CSnapshot * pSnapshot = (TaskDoc::CSnapshot *)pDoc->GetSnapshot ();

		for (int i = 0; i < pList->GetSize (); i++) {
			CElement * pElement = pList->GetElementPtr (i);

			if (pElement->IsSelected ()) {
				v.Add (pElement);
				pList->RemoveAt (i);
				pSnapshot->SetElementsChanged (Element::CPair (pElement, pList, &workingHead.m_card), TaskDoc::ELEMENT_NONE);
			}
		}

		if (v.GetSize ()) {
			for (int i = v.GetSize () - 1; i >= 0; i--)
				if (CElement * pElement = v [i]) 
					pList->InsertAt (0, pElement);

			pDoc->SetModifiedFlag (pSnapshot);
		}
		else 
			delete pSnapshot;
	}
}

void CTaskView::OnBringForward ()
{
	CElementPtrArray v;
	CTaskDoc * pDoc = GetDocument ();
	CBoundary workingHead;

	pDoc->m_box.GetWorkingHead (workingHead);

	if (CEditorElementList * pList = GetList (GetActiveHead ())) {
		TaskDoc::CSnapshot * pSnapshot = (TaskDoc::CSnapshot *)pDoc->GetSnapshot ();

		for (int i = 0; i < pList->GetSize (); i++) {
			CElement * pElement = pList->GetElementPtr (i);

			if (i > 0 && pElement->IsSelected ()) {
				v.Add (pElement);
				pList->RemoveAt (i);
				pList->InsertAt (i - 1, pElement);
				pSnapshot->SetElementsChanged (Element::CPair (pElement, pList, &workingHead.m_card), TaskDoc::ELEMENT_NONE);
			}
		}

		if (pSnapshot->GetElementsChanged ().GetSize ()) 
			pDoc->SetModifiedFlag (pSnapshot);
		else 
			delete pSnapshot;
	}
}

void CTaskView::OnSendToBack()
{
	CElementPtrArray v;
	CTaskDoc * pDoc = GetDocument ();
	CBoundary workingHead;

	pDoc->m_box.GetWorkingHead (workingHead);

	if (CEditorElementList * pList = GetList (GetActiveHead ())) {
		TaskDoc::CSnapshot * pSnapshot = (TaskDoc::CSnapshot *)pDoc->GetSnapshot ();
	
		for (int i = pList->GetSize () - 1; i >= 0; i--) {
			CElement * pElement = pList->GetElementPtr (i);

			if (pElement->IsSelected ()) {
				v.Add (pElement);
				pList->RemoveAt (i);
				pSnapshot->SetElementsChanged (Element::CPair (pElement, pList, &workingHead.m_card), TaskDoc::ELEMENT_NONE);
			}
		}

		if (v.GetSize ()) {
			for (int i = v.GetSize () - 1; i >= 0; i--)
				if (CElement * pElement = v [i]) 
					pList->Add (pElement);

			pDoc->SetModifiedFlag (pSnapshot);
		}
		else 
			delete pSnapshot;
	}
}

void CTaskView::OnSendBackward ()
{
	CElementPtrArray v;
	CTaskDoc * pDoc = GetDocument ();
	CBoundary workingHead;

	pDoc->m_box.GetWorkingHead (workingHead);

	if (CEditorElementList * pList = GetList (GetActiveHead ())) {
		TaskDoc::CSnapshot * pSnapshot = (TaskDoc::CSnapshot *)pDoc->GetSnapshot ();
	
		for (int i = pList->GetSize () - 1; i >= 0; i--) {
			CElement * pElement = pList->GetElementPtr (i);

			if ((i < (pList->GetSize () - 1)) && pElement->IsSelected ()) {
				v.Add (pElement);
				pList->RemoveAt (i);
				pList->InsertAt (i + 1, pElement);
				pSnapshot->SetElementsChanged (Element::CPair (pElement, pList, &workingHead.m_card), TaskDoc::ELEMENT_NONE);
			}
		}

		if (v.GetSize ()) 
			pDoc->SetModifiedFlag (pSnapshot);
		else 
			delete pSnapshot;
	}
}

void CTaskView::OnDebuggingstuffStrobe() 
{
	CStrobeDlg (theApp.GetDB (), defElements.m_strRegSection, this).DoModal ();	
}

void CTaskView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CBaseView::OnVScroll(nSBCode, nPos, pScrollBar);
	REPAINT (this); 
}

void CTaskView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{

	CBaseView::OnHScroll(nSBCode, nPos, pScrollBar);
	REPAINT (this); 
}


LRESULT CTaskView::OnKbChange (WPARAM wParam, LPARAM lParam) 
{
	#ifdef _DEBUG
	{ 
		LPCTSTR lpsz [] = 
		{
			_T ("KBCHANGETYPE_DOCKSTATE"),
			_T ("KBCHANGETYPE_SHOWSTATE"),
			_T ("KBCHANGETYPE_DOCKSTATE_DOCK"),
			_T ("KBCHANGETYPE_DOCKSTATE_UNDOCK"),
			_T ("KBCHANGETYPE_SHOWSTATE_SHOW"),
			_T ("KBCHANGETYPE_SHOWSTATE_HIDE"),
		};
		CString str; 
		
		str.Format (_T ("OnKbChange (%s, %s)"), lpsz [wParam], lpsz [lParam]); 
		TRACEF (str); 
	}
	#endif //_DEBUG

	/*
		show
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_DOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_DOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_DOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_DOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_DOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_DOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_DOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_DOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_DOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_SHOWSTATE, KBCHANGETYPE_SHOWSTATE_SHOW)

		hide
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_UNDOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_UNDOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_UNDOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_UNDOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_UNDOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_UNDOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_UNDOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_UNDOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_UNDOCK)
			C:\Dev\sw0922\Editor\TaskView.cpp(4048): OnKbChange (KBCHANGETYPE_SHOWSTATE, KBCHANGETYPE_SHOWSTATE_HIDE)
	*/

	if (wParam == KBCHANGETYPE_DOCKSTATE) 
		m_tmKbChange = CTime::GetCurrentTime ();
	else if (wParam == KBCHANGETYPE_SHOWSTATE) {
		if (lParam == KBCHANGETYPE_SHOWSTATE_HIDE) {
			HWND hwndRestore = m_hWnd;

			for (HWND h = m_hWnd; h != NULL; h = ::GetParent (h)) {
				#ifdef _DEBUG
				TCHAR sz [512] = { 0 };
				TCHAR szClass [512] = { 0 };
				CString str;

				::GetWindowText (h, sz, ARRAYSIZE (sz));
				::GetClassName (h, szClass, ARRAYSIZE (szClass));
				str.Format (_T ("0x%p: [%s] [%s]"), h, sz, szClass);
				TRACEF (str);
				#endif //_DEBUG

				hwndRestore = h;
			}

			if (m_sizePref.cx > 0 && m_sizePref.cy > 0) {
				#ifdef _DEBUG
				{ CString str; str.Format (_T ("SetWindowPos: 0x%p: [%d] [%d]"), hwndRestore, m_sizePref.cx, m_sizePref.cy); TRACEF (str); }
				#endif //_DEBUG

				VERIFY (::SetWindowPos (hwndRestore, NULL, 0, 0, m_sizePref.cx, m_sizePref.cy, SWP_NOMOVE | SWP_NOZORDER));
			}
		}
	}

	return 0;
}
