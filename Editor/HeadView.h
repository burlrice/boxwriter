#if !defined(AFX_HEADVIEW_H__9647698F_3303_4107_939D_640E211ECD5F__INCLUDED_)
#define AFX_HEADVIEW_H__9647698F_3303_4107_939D_640E211ECD5F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HeadView.h : header file
//

#include "Alias.h"
#include "TaskDoc.h"
#include "TaskView.h"
#include "Database.h"
#include "Panel.h"

#include <afxcview.h>

/////////////////////////////////////////////////////////////////////////////
// CHeadView view

namespace HeadView
{
	typedef enum { BOX = 0, PANEL, HEAD } TYPE;

	class CItem
	{ 
	public:

		CItem (UINT nPanel);
		CItem (ULONG lID, TYPE type = HEAD);
		virtual ~CItem ();

		const TYPE m_type;

		union 
		{
			UINT	m_nPanel;
			ULONG	m_lID;
		};

	protected:
		CItem (TYPE type);
	};

	class CTreeItem : protected CItem
	{
	public:
		CTreeItem (const FoxjetDatabase::BOXSTRUCT & s);
		CTreeItem (const FoxjetDatabase::PANELSTRUCT & s);
		CTreeItem (const Foxjet3d::Panel::CBoundary & s);
		virtual ~CTreeItem ();

		UINT GetMenuID () const;
		UINT GetDefMenuItemID () const;

		using CItem::m_type;

		union 
		{
			const FoxjetDatabase::BOXSTRUCT *			m_pBox;
			const FoxjetDatabase::PANELSTRUCT *			m_pPanel;
			const const Foxjet3d::Panel::CBoundary *	m_pHead;
		};
	};

	class CHeadView : public CTreeView
	{

	protected:
		CHeadView();           // protected constructor used by dynamic creation
		DECLARE_DYNCREATE(CHeadView)

	// Attributes
	public:

	// Operations
	public:
		TaskDoc::CTaskDoc * GetDocument ();
		const TaskDoc::CTaskDoc * GetDocument () const;

		bool SetSelection (const CItem & sel);

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CHeadView)
		public:
		virtual void OnInitialUpdate();
		protected:
		virtual void OnDraw(CDC* pDC);      // overridden to draw this view
		virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual ~CHeadView();
	#ifdef _DEBUG
		virtual void AssertValid() const;
		virtual void Dump(CDumpContext& dc) const;
	#endif

		// Generated message map functions
	protected:
		void FillCtrl ();
		void OnSelect ();
		TaskView::CTaskView * GetTaskView();

		virtual BOOL OnCmdMsg (UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	
		CImageList m_ilTreeView;
		HICON m_hTask;
		HICON m_hPanel;
		HICON m_hHead;
		HICON m_hHeadMaster;
		HICON m_hHeadDisabled;

		int m_nTaskIndex;
		int m_nPanelIndex;
		int m_nHeadIndex;
		int m_nHeadMasterIndex;
		int m_nHeadDisabledIndex;

		bool m_bInitUpdate;
		Foxjet3d::Panel::CBoundaryArray m_vHeads;

		//{{AFX_MSG(CHeadView)
		afx_msg void OnDestroy();
		afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnDeleteitem(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnInitMenu(CMenu* pMenu);
		afx_msg void OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnItemexpanded(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
		afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
		afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
		//}}AFX_MSG

		afx_msg void OnCopyToHead (UINT nCmdID);
		afx_msg void OnLinkedToHead (UINT nCmdID);
		afx_msg void OnFilePrintPreview ();
		afx_msg void OnFilePrint ();

		DECLARE_MESSAGE_MAP()
	};
}; //namespace HeadView;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEADVIEW_H__9647698F_3303_4107_939D_640E211ECD5F__INCLUDED_)
