// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__40AC74C9_3604_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_MAINFRM_H__40AC74C9_3604_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FontBar.h"
#include "HeadBar.h"
#include "TemplExt.h"

typedef FoxjetCommon::CCopyArray <CDocument *, CDocument *> CDocPtrArray;

class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)

public:
	CMainFrame();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL DestroyWindow();
	protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members

public:
	static enum Panes { DEFAULT = 0, ZOOM, PHOTO, LOCATION };

	
	CDocPtrArray GetOpenDocs () const;
	void InitDefineMenu (CMenu * pMenu);
	void SetTitle (const CString & strTitle);
	CTime GetKbChangeTime () const { return m_tmKbChange; }

	CToolBar    m_wndToolBar;
	CStatusBar  m_wndStatusBar;
	CToolBar	m_wndRotateBar;
	CToolBar	m_wndElementBar;
	CToolBar	m_wndAlignBar;
	CFontBar	m_wndFontBar;
	CHeadBar	m_wndHeadBar;

	CStringArray m_vDevices;

// Generated message map functions
protected:
	CString m_strTitle;
	CTime m_tmKbChange;

	virtual void GetMessageString(UINT nID, CString& rMessage) const;
	void GetBarState (LPCTSTR pszRegKey, bool & bFloating, int & nDockState, int nDefDockState, CRect & rc) const;
	void InitStatusBar ();
	void UpdateDocBoxes (ULONG lBoxID);
	void InvalidateDocs (ULONG lLineID = FoxjetDatabase::ALL);

	afx_msg void OnViewHeadbar ();
	afx_msg void OnUpdateViewHeadbar (CCmdUI* pCmdUI);

	afx_msg void OnApiDefineCommand(UINT nID);
	afx_msg void OnUpdateApiDefineCommand(CCmdUI *pCmdUI);
	afx_msg void OnViewFontbar();
	afx_msg void OnUpdateViewFontbar(CCmdUI* pCmdUI);
	afx_msg void OnDefineEditordefaults ();
	afx_msg void OnViewAlignmentbar();
	afx_msg void OnUpdateViewAlignmentbar(CCmdUI* pCmdUI);
	afx_msg void OnViewElementbar();
	afx_msg void OnUpdateViewElementbar(CCmdUI* pCmdUI);
	afx_msg LRESULT OnIsAppRunning (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnIsAppInitialized (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnShutdown (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSystemParamsChanged (WPARAM wParam, LPARAM lParam);
	afx_msg void OnDebuggingstuffSystemConfig();
	afx_msg void OnDefineBackupPath();
	afx_msg LRESULT OnCreateCaptionBroadcast (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnNewTask (WPARAM wParam, LPARAM lParam); // sw0867 
	afx_msg LRESULT OnOpenTask (WPARAM wParam, LPARAM lParam); // sw0867 
	afx_msg LRESULT OnKbChange (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDeviceChange (WPARAM wParam, LPARAM lParam);


	afx_msg void OnElementsDelete(); 
	afx_msg void OnUpdateElementsDelete(CCmdUI* pCmdUI);

	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDefineElementdefaults();
	afx_msg void OnInitMenu(CMenu* pMenu);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnFileSaveall();
	afx_msg void OnUpdateFileSaveall(CCmdUI* pCmdUI);
	afx_msg void OnDefineBoxes();
	afx_msg void OnDefineBoxusage();
	afx_msg void OnDebuggingstuffShowcopydialog();
	afx_msg void OnViewToolbar();
	afx_msg void OnUpdateViewToolbar(CCmdUI* pCmdUI);
	afx_msg void OnViewStatusbar();
	afx_msg void OnUpdateViewStatusbar(CCmdUI* pCmdUI);
	afx_msg void OnViewRotatebar();
	afx_msg void OnUpdateViewRotatebar(CCmdUI* pCmdUI);
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg void OnDebuggingstuffSetdxresolution();
	afx_msg void OnFileDelete();
	afx_msg void OnFileExport();
	afx_msg void OnDebuggingstuffRuntestthread();
	afx_msg void OnDebuggingstuffExpTest ();
	afx_msg void OnFileCopy();
	afx_msg void OnFileImport();
	afx_msg void OnHelpTranslate();
	afx_msg void OnUpdateHelpTranslate(CCmdUI* pCmdUI);
	afx_msg void OnTimeChange();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__40AC74C9_3604_11D4_8FC6_006067662794__INCLUDED_)
