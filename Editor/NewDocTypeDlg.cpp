// NewDocTypeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "mkdraw.h"
#include "alias.h"
#include "NewDocTypeDlg.h"
#include "Debug.h"
#include "ItiLibrary.h"
#include "Database.h"
#include "Extern.h"
#include "NewMessageDocType.h"
#include "NewTaskDocType.h"
#include "ListCtrlImp.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


using namespace EditorGlobals;
using namespace ItiLibrary;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

using FoxjetDatabase::LINESTRUCT;
using FoxjetDatabase::HEADSTRUCT;
using FoxjetDatabase::TASKSTRUCT;
using FoxjetDatabase::MESSAGESTRUCT;

typedef struct 
{
	CWnd * m_pCtrl;
	int m_nTab;
} TABINDEXSTRUCT;

static const int nTaskTab		= 0;
static const int nMsgTab		= 1;

/////////////////////////////////////////////////////////////////////////////
// CNewDocTypeDlg dialog


CNewDocTypeDlg::CNewDocTypeDlg(const CArray <CNewDocType *, CNewDocType *> & vTemplates, 
							   CWnd* pParent)
:	m_pSelectedTemplate (NULL),
	m_pDocExisting (NULL),
	m_type (FoxjetFile::MESSAGE),
	FoxjetCommon::CEliteDlg(IDD_NEWDOC_MATRIX, pParent)
{
	for (int i = 0; i < vTemplates.GetSize (); i++) 
		m_vTemplates.Add (vTemplates [i]);
}

CNewDocTypeDlg::~CNewDocTypeDlg ()
{
	m_ilMsg.DeleteImageList ();
	m_ilTask.DeleteImageList ();
}

void CNewDocTypeDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CNewDocTypeDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CNewDocTypeDlg)
	ON_CBN_SELCHANGE(CB_LINE, OnSelchangeLine)
	ON_CBN_SELCHANGE(CB_BOX, OnSelchangeBox)
	//}}AFX_MSG_MAP
	ON_NOTIFY(TCN_SELCHANGE, TAB_DOCTYPE, OnSelchangeDoctype)
	ON_NOTIFY(LVN_ITEMCHANGED, LV_MESSAGETYPES, OnItemchangedDoctypes)
	ON_NOTIFY(LVN_ITEMCHANGED, LV_TASKTYPES, OnItemchangedDoctypes) // TODO: need separate handler
	ON_NOTIFY(NM_DBLCLK, LV_MESSAGETYPES, OnDblclkDoctypes)
	ON_NOTIFY(NM_DBLCLK, LV_TASKTYPES, OnDblclkDoctypes) // TODO: need separate handler
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewDocTypeDlg message handlers


BOOL CNewDocTypeDlg::OnInitDialog() 
{
	typedef struct
	{
		int			m_nTab;
		CListCtrl * m_pLV;
		bool		m_bShowTab;
		int			m_nSel;
	} SELECTSTRUCT;
	
	ASSERT (GetDlgItem (TAB_DOCTYPE));
	ASSERT (GetDlgItem (LV_MESSAGETYPES));
	ASSERT (GetDlgItem (LV_TASKTYPES));
	ASSERT (GetDlgItem (CB_LINE));
	ASSERT (GetDlgItem (CB_HEAD));

	FoxjetCommon::CEliteDlg::OnInitDialog();
	CTabCtrl & tab = * (CTabCtrl *)GetDlgItem (TAB_DOCTYPE);
	CListCtrl & lvMsg = * (CListCtrl *)GetDlgItem (LV_MESSAGETYPES);
	CListCtrl & lvTask = * (CListCtrl *)GetDlgItem (LV_TASKTYPES);
	CComboBox & cbHead = * (CComboBox *)GetDlgItem (CB_HEAD);
	CComboBox & cbLine = * (CComboBox *)GetDlgItem (CB_LINE);
	CArray <FoxjetDatabase::LINESTRUCT, FoxjetDatabase::LINESTRUCT &> vLine;
	CArray <HEADSTRUCT, HEADSTRUCT &> vHead;
//	ULONG lDefHeadID = EditorGlobals::defElements.GetHeadID ();
	SELECTSTRUCT sel [] = 
	{
		{ ::nTaskTab,	&lvTask,	(m_type == FoxjetFile::TASK),		0 },
		{ ::nMsgTab,	&lvMsg,		(m_type == FoxjetFile::MESSAGE),	0 },
		{ 0,			NULL,		false,								0 }
	};
	int nIndex = 0;

	FoxjetCommon::GetLineRecords (vLine);
	FoxjetCommon::GetHeadRecords (vHead);

	for (int i = 0; i < vLine.GetSize (); i++) {
		CArray <HEADSTRUCT, HEADSTRUCT &> v;
		LINESTRUCT line = vLine [i];

		FoxjetCommon::GetLineHeads (line.m_lID, v);

		if (v.GetSize ()) {
			int nIndex = cbLine.AddString (line.m_strName);
			cbLine.SetItemData (nIndex, line.m_lID);
		}
	}

	for (int i = 0; i < vHead.GetSize (); i++) {
		int nIndex = cbHead.AddString (vHead [i].m_strName);
		cbHead.SetItemData (nIndex, vHead [i].m_lID);
	}

	tab.InsertItem (::nTaskTab, LoadString (IDS_MESSAGE));
	
	#ifdef __MESSAGE__
	tab.InsertItem (::nMsgTab, LoadString (IDS_MESSAGE));
	#endif //__MESSAGE__
	
	m_ilMsg.Create (32, 32, ILC_COLOR, 4, 4);
	m_ilTask.Create (32, 32, ILC_COLOR, 4, 4);

	{
		DWORD dw = ::GetWindowLong (lvTask.m_hWnd, GWL_STYLE);
	
		dw &= ~(LVS_ICON | LVS_SMALLICON | LVS_LIST | LVS_REPORT);
		dw |= LVS_REPORT;

		::SetWindowLong (lvTask.m_hWnd, GWL_STYLE, dw);

		lvTask.SetColumnWidth (0, 300);
		lvTask.InsertColumn (0, LoadString (IDS_TYPE), 0, 200);
	}

	for (int i = 0; i < m_vTemplates.GetSize (); i++) {
		const CNewDocType * pType = m_vTemplates [i];
		CString str = CMkDrawDocManager::GetFileType (* pType);
	
		if (pType->m_type == FoxjetFile::MESSAGE) {
			int nImage = m_ilMsg.GetImageCount ();
			int nItem = lvMsg.GetItemCount ();

			m_ilMsg.Add (pType->m_hIcon);
			int nIndex = lvMsg.InsertItem (nItem, str, nImage);
			ASSERT (nIndex != -1);
			lvMsg.SetItemData (nIndex, (DWORD)pType);
		}
		else if (pType->m_type == FoxjetFile::TASK) {
			int nImage = m_ilTask.GetImageCount ();
			int nItem = lvTask.GetItemCount ();

			m_ilTask.Add (pType->m_hIcon);
			int nIndex = lvTask.InsertItem (nItem, str, nImage);
			ASSERT (nIndex != -1);
			lvTask.SetItemData (nIndex, (DWORD)pType);
		}
		else {
			ASSERT (0); // invalid doc type
		}
	}
	
	lvMsg.SetImageList (&m_ilMsg, LVSIL_NORMAL);
	lvTask.SetImageList (&m_ilTask, LVSIL_NORMAL);

	for (int i = 0; sel [i].m_pLV; i++) {
		tab.SetCurSel (sel [i].m_nTab);
		sel [i].m_pLV->SetItemState (sel [i].m_nSel, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
		OnSelchangeDoctype (NULL, NULL);
		OnItemchangedDoctypes (NULL, NULL);

		if (sel [i].m_bShowTab) 
			nIndex = i;
	}

	tab.SetCurSel (sel [nIndex].m_nTab);
	sel [nIndex].m_pLV->SetItemState (sel [nIndex].m_nSel, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
	OnSelchangeDoctype (NULL, NULL);
	OnItemchangedDoctypes (NULL, NULL);
	OnSelchangeBox ();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CNewDocTypeDlg::OnSelchangeDoctype(NMHDR* pNMHDR, LRESULT* pResult)
{
	ASSERT (GetDlgItem (TAB_DOCTYPE));
	TABINDEXSTRUCT ctrls [] = {
		{ GetDlgItem (LV_MESSAGETYPES),	::nMsgTab	},
		{ GetDlgItem (LBL_HEAD),		::nMsgTab	},
		{ GetDlgItem (CB_HEAD),			::nMsgTab	},
		{ GetDlgItem (LBL_MESSAGENAME),	::nMsgTab	},
		{ GetDlgItem (TXT_MESSAGENAME),	::nMsgTab	},
		{ GetDlgItem (LBL_MESSAGEDESC),	::nMsgTab	},
		{ GetDlgItem (TXT_MESSAGEDESC),	::nMsgTab	},
		{ GetDlgItem (CB_HEAD),			::nMsgTab	},
		{ GetDlgItem (LBL_HEAD),		::nMsgTab	},
		{ GetDlgItem (LV_TASKTYPES),	::nTaskTab	},
		{ GetDlgItem (LBL_LINE),		::nTaskTab	},
		{ GetDlgItem (CB_LINE),			::nTaskTab	},
		{ GetDlgItem (LBL_TASKNAME),	::nTaskTab	},
		{ GetDlgItem (TXT_TASKNAME),	::nTaskTab	},
		{ GetDlgItem (LBL_TASKDESC),	::nTaskTab	},
		{ GetDlgItem (TXT_TASKDESC),	::nTaskTab	},
		{ GetDlgItem (LBL_BOX),			::nTaskTab	},
		{ GetDlgItem (CB_BOX),			::nTaskTab	},
		{ GetDlgItem (TXT_BOXDESC),		::nTaskTab	},
		{ NULL,							-1			},
	};
	CTabCtrl & tab = * (CTabCtrl *)GetDlgItem (TAB_DOCTYPE);
	int nTab = tab.GetCurSel ();

	for (int i = 0; ctrls [i].m_nTab != -1 && ctrls [i].m_pCtrl; i++) {
		ASSERT (ctrls [i].m_pCtrl);
		ctrls [i].m_pCtrl->ShowWindow (nTab == ctrls [i].m_nTab ? 
			SW_SHOW : SW_HIDE);
	}

	if (pResult)
		* pResult = 0;
}

void CNewDocTypeDlg::OnItemchangedDoctypes (NMHDR* pNMHDR, LRESULT* pResult) 
{
	ASSERT (GetDlgItem (TAB_DOCTYPE));

	CTabCtrl & tab = * (CTabCtrl *)GetDlgItem (TAB_DOCTYPE);
	int nTab = tab.GetCurSel ();
	int nCBCtrl = CB_HEAD;
	int nNameCtrl = TXT_MESSAGENAME;
	int nDescCtrl = TXT_MESSAGEDESC;
	CNewDocType * pTemplate = NULL;
	CString strLib, strDoc, strFirstRecord;

	if (nTab == ::nMsgTab) {
		ASSERT (GetDlgItem (LV_MESSAGETYPES));
		CListCtrl & lv = * (CListCtrl *)GetDlgItem (LV_MESSAGETYPES);
		CLongArray sel = GetSelectedItems (lv); 

		nCBCtrl = CB_HEAD;
		nNameCtrl = TXT_MESSAGENAME;
		nDescCtrl = TXT_MESSAGEDESC;

		if (sel.GetSize ()) {
			if (sel [0] >= 0 && sel [0] < (DWORD)lv.GetItemCount ()) { 
				pTemplate = (CNewDocType *)lv.GetItemData (sel [0]);
				CString str = CMkDrawDocManager::GetFileLibrary (* pTemplate);

				// if the def head is not in the database, use the 
				// first one that is there
				if (FoxjetCommon::GetHeadID (str) == NOTFOUND) {
					HEADSTRUCT head;
					
					FoxjetCommon::GetFirstHeadRecord (head);
					strFirstRecord = head.m_strName;
				}
			}

/* TODO 
			ASSERT (pTemplate->IsKindOf (RUNTIME_CLASS (CNewMessageDocType)));
			ASSERT (GetDlgItem (CB_HEAD));
			CComboBox & cbHead = * (CComboBox *)GetDlgItem (CB_HEAD);
			CNewMessageDocType * p = (CNewMessageDocType *)pTemplate;
			int nSelect = 0;

			for (int i = 0; i < cbHead.GetCount (); i++) {
				if (cbHead.GetItemData (i) == p->m_head.m_lID) {
					nSelect = i;
					break;
				}
			}

			cbHead.SetCurSel (nSelect);
*/
		}
	}
	else if (nTab == ::nTaskTab) {
		ASSERT (GetDlgItem (LV_TASKTYPES));
		ASSERT (GetDlgItem (CB_BOX));

		CListCtrl & lv = * (CListCtrl *)GetDlgItem (LV_TASKTYPES);
		CComboBox & cbBox = * (CComboBox *)GetDlgItem (CB_BOX);
		CLongArray sel = GetSelectedItems (lv); 

		nCBCtrl = CB_LINE;
		nNameCtrl = TXT_TASKNAME;
		nDescCtrl = TXT_TASKDESC;

		if (sel.GetSize ()) {
			if (sel [0] >= 0 && sel [0] < (DWORD)lv.GetItemCount ()) { 
				pTemplate = (CNewDocType *)lv.GetItemData (sel [0]);
				CTaskDocTemplate * pTask = (CTaskDocTemplate *)pTemplate->m_pDocTemplate;
				CString str = CMkDrawDocManager::GetFileLibrary (* pTemplate);

				// if the def product line is not in the database, use the 
				// first one that is there
				if (FoxjetCommon::GetLineID (str) == NOTFOUND) {
					LINESTRUCT line;

					FoxjetCommon::GetFirstLineRecord (line);
					strFirstRecord = line.m_strName;
				}
			}
		}
	}

	CComboBox * pCB = (CComboBox *)GetDlgItem (nCBCtrl);
	CEdit * pEdit = (CEdit *)GetDlgItem (nNameCtrl);

	ASSERT (pEdit);
	ASSERT (pCB);

	if (pTemplate) {
		TRACEF (
			CString ("\n\tpTemplate                 = ") + pTemplate->GetRuntimeClass ()->m_lpszClassName +
			CString ("\n\tpTemplate->m_pDocTemplate = ") + pTemplate->m_pDocTemplate->GetRuntimeClass ()->m_lpszClassName);
		CString strPath = pTemplate->m_pDocTemplate->GetNextDefaultName ();
		strDoc = FoxjetFile::GetFileTitle (strPath);
		strLib = CMkDrawDocManager::GetFileLibrary (* pTemplate);
		TRACEF (
			CString ("\n\tdoc                       = ") + strDoc +
			CString ("\n\tlib                       = ") + strLib);
	}
	
	int nSel = pCB->FindStringExact (-1, strLib);
	
	if (nSel == CB_ERR)
		nSel = pCB->FindStringExact (-1, strFirstRecord);

	pCB->SetCurSel (nSel);
	pEdit->SetWindowText (strDoc);

	if (pCB->GetCurSel () == CB_ERR)
		pCB->SetCurSel (0);

	if (nTab == ::nTaskTab)
		OnSelchangeLine ();

	if (pResult)
		* pResult = 0;
}

CNewDocType * CNewDocTypeDlg::ValidateTask ()
{
	ASSERT (GetDlgItem (LV_TASKTYPES));
	ASSERT (GetDlgItem (CB_LINE));
	ASSERT (GetDlgItem (CB_BOX));

	CListCtrl & lv = * (CListCtrl *)GetDlgItem (LV_TASKTYPES);
	CComboBox & cbLine = * (CComboBox *)GetDlgItem (CB_LINE);
	CComboBox & cbBox = * (CComboBox *)GetDlgItem (CB_BOX);
	CLongArray sel = GetSelectedItems (lv); 
	TASKSTRUCT task;
	CString strTitle, strLine, strDesc;
	ULONG lBoxID = -1;

	if (sel.GetSize () == 0) {
		MsgBox (* this, LoadString (IDS_NODOCTYPESELECTED));
		return NULL;
	}

	ASSERT (sel [0] >= 0 && sel [0] < (DWORD)lv.GetItemCount ());
	int nIndex = cbLine.GetCurSel ();
	CNewDocType * pTemplate = (CNewDocType *)lv.GetItemData (sel [0]);
	ASSERT (pTemplate->m_pDocTemplate->IsKindOf (RUNTIME_CLASS (CTaskDocTemplate)));
	CTaskDocTemplate * pTask = (CTaskDocTemplate *)pTemplate->m_pDocTemplate;

	if (nIndex == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOLINESELECTED));
		return NULL;
	}

	nIndex = cbBox.GetCurSel ();

	if (nIndex == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOBOXSELECTED));
		return NULL;
	}

	lBoxID = cbBox.GetItemData (nIndex);

	GetDlgItemText (TXT_TASKNAME, strTitle);
	GetDlgItemText (TXT_TASKDESC, strDesc);

	if (!FoxjetCommon::IsValidFileTitle (strTitle)) {
		MsgBox (* this, LoadString (IDS_INVALIDFILETITLE));
		return NULL;
	}

	GetDlgItemText (CB_LINE, strLine);

	POSITION pos = pTemplate->m_pDocTemplate->GetFirstDocPosition ();

	while (pos != NULL) {
		CDocument * pDoc = pTemplate->m_pDocTemplate->GetNextDoc (pos);
		const CString & strExistingTitle = FoxjetFile::GetRelativeName (pDoc->GetTitle ());
		const CString & strTmpTitle = strLine + _T ("\\") + strTitle;
		
		if (!strTmpTitle.CompareNoCase (strExistingTitle)) {
			CString str;
			str.Format (LoadString (IDS_FILEOPENALREADYEXISTS), strTitle, strLine);
			int nResult = MsgBox (* this, str, LoadString (IDS_CONFIRM), MB_YESNO);

			if (nResult == IDNO)
				return NULL;
			else if (nResult == IDYES) { 
				pDoc->DeleteContents ();
				m_pDocExisting = pDoc;
			}
		}
	}

	if (FoxjetDatabase::GetTaskRecord (theApp.GetDB (), strTitle, strLine, GetPrinterID (theApp.GetDB ()), task)) 
	{
		CString str;
		str.Format (LoadString (IDS_FILECLOSEDALREADYEXISTS), strTitle, strLine);
		int nResult = MsgBox (* this, str, LoadString (IDS_CONFIRM), MB_YESNO);
	
		if (nResult == IDNO)
			return NULL;
	}

	pTemplate->m_strLibrary = strLine;
	pTemplate->m_strTitle = strTitle;
	pTemplate->m_strDesc = strDesc;
	
	bool bGetBox = GetBoxRecord (database, lBoxID, pTask->m_box);
	ASSERT (bGetBox);

	return pTemplate;
}

CNewDocType * CNewDocTypeDlg::ValidateMessage ()
{
	CListCtrl & lv = * (CListCtrl *)GetDlgItem (LV_MESSAGETYPES);
	CComboBox & cb = * (CComboBox *)GetDlgItem (CB_HEAD);
	CLongArray sel = GetSelectedItems (lv); 
	CString strTitle, strHead, strDesc;
	MESSAGESTRUCT msg;

	if (sel.GetSize () == 0) {
		MsgBox (* this, LoadString (IDS_NODOCTYPESELECTED));
		return NULL;
	}

	ASSERT (sel [0] >= 0 && sel [0] < (DWORD)lv.GetItemCount ());
	int nIndex = cb.GetCurSel ();
	CNewDocType * pTemplate = (CNewDocType *)lv.GetItemData (sel [0]);

	if (nIndex == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOHEADSELECTED));
		return NULL;
	}

	GetDlgItemText (TXT_MESSAGENAME, strTitle);
	GetDlgItemText (TXT_MESSAGEDESC, strDesc);

	if (!FoxjetCommon::IsValidFileTitle (strTitle)) {
		MsgBox (* this, LoadString (IDS_INVALIDFILETITLE));
		return NULL;
	}

	GetDlgItemText (CB_HEAD, strHead);
	POSITION pos = pTemplate->m_pDocTemplate->GetFirstDocPosition ();

	while (pos != NULL) {
		CDocument * pDoc = pTemplate->m_pDocTemplate->GetNextDoc (pos);
		const CString & strExistingTitle = FoxjetFile::GetRelativeName (pDoc->GetTitle ());
		const CString & strTmpTitle = strHead + _T ("\\") + strTitle;
		
		if (!strTmpTitle.CompareNoCase (strExistingTitle)) {
			CString str;
			str.Format (LoadString (IDS_FILEOPENALREADYEXISTS), strTitle, strHead);
			int nResult = MsgBox (* this, str, LoadString (IDS_CONFIRM), MB_YESNO);

			if (nResult == IDNO)
				return NULL;
			else if (nResult == IDYES) {
				pDoc->DeleteContents ();
				m_pDocExisting = pDoc;
			}
		}
	}

	MsgBox (* this, _T ("need a head uid or line id"));
	return NULL;

	if (FoxjetDatabase::GetMessageRecord (theApp.GetDB (), strTitle, 
		strHead, msg)) 
	{
		CString str;
		str.Format (LoadString (IDS_FILECLOSEDALREADYEXISTS), strTitle, strHead);
		int nResult = MsgBox (* this, str, LoadString (IDS_CONFIRM), MB_YESNO);

		if (nResult == IDNO)
			return NULL;
	}

	pTemplate->m_strLibrary = strHead;
	pTemplate->m_strTitle = strTitle;
	pTemplate->m_strDesc = strDesc;

	return pTemplate;
}

void CNewDocTypeDlg::OnOK()
{
	ASSERT (GetDlgItem (TAB_DOCTYPE));
	CTabCtrl & tab = * (CTabCtrl *)GetDlgItem (TAB_DOCTYPE);
	int nCBID, nNameID, nDescID, nTab = tab.GetCurSel ();
	FoxjetFile::DOCTYPE type;

	if (nTab == ::nMsgTab) {
		type = FoxjetFile::MESSAGE;
		nCBID = CB_HEAD;
		nNameID = TXT_MESSAGENAME;
		nDescID = TXT_MESSAGEDESC;
		m_pSelectedTemplate = ValidateMessage ();
	}
	else if (nTab == ::nTaskTab) {
		type = FoxjetFile::TASK;
		nCBID = CB_LINE;
		nNameID = TXT_TASKNAME;
		nDescID = TXT_TASKDESC;
		m_pSelectedTemplate = ValidateTask ();
	}
	
	if (m_pSelectedTemplate) {
		CString strPrefix, strTitle, strDesc;

		m_type = type;
		GetDlgItemText (nCBID, strPrefix);
		GetDlgItemText (nNameID, strTitle);
		GetDlgItemText (nDescID, strDesc);

		m_pSelectedTemplate->m_pDocTemplate->m_strPrefix = 
			m_pSelectedTemplate->m_strLibrary = strPrefix;
		m_pSelectedTemplate->m_pDocTemplate->m_strTitle = 
			m_pSelectedTemplate->m_strTitle = strTitle;
		m_pSelectedTemplate->m_pDocTemplate->m_strDesc = 
			m_pSelectedTemplate->m_strDesc = strDesc;

		FoxjetCommon::CEliteDlg::OnOK ();
	}
}

void CNewDocTypeDlg::OnDblclkDoctypes(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnOK ();
	*pResult = 0;
}

void CNewDocTypeDlg::OnSelchangeLine() 
{
	using namespace FoxjetCommon;
	using namespace EditorGlobals;

	ASSERT (GetDlgItem (CB_BOX));
	ASSERT (GetDlgItem (LV_TASKTYPES));

	CListCtrl & lv = * (CListCtrl *)GetDlgItem (LV_TASKTYPES);
	CComboBox & cb = * (CComboBox *)GetDlgItem (CB_BOX);
	CLongArray sel = GetSelectedItems (lv); 

	m_vBoxes.RemoveAll ();

	if (IsNetworked ())
		GetNetworkedBoxRecords (database, GetSelectedLine (), m_vBoxes);
	else
		GetBoxRecords (database, GetSelectedLine (), m_vBoxes);

	cb.ResetContent ();
	
	for (int i = 0; i < m_vBoxes.GetSize (); i++) {
		int nIndex = cb.AddString (m_vBoxes [i].m_strName);
		cb.SetItemData (nIndex, (DWORD)m_vBoxes [i].m_lID);
	}

	if (sel.GetSize ()) {
		CNewDocType * pTemplate = (CNewDocType *)lv.GetItemData (sel [0]);
		CTaskDocTemplate * pTask = (CTaskDocTemplate *)pTemplate->m_pDocTemplate;
		
		if (cb.SelectString (-1, pTask->m_box.m_strName) == CB_ERR)
			cb.SetCurSel (0);
	}
	else if (cb.GetCount ())
		cb.SetCurSel (0);

	OnSelchangeBox ();
}

ULONG CNewDocTypeDlg::GetSelectedLine() const
{
	ASSERT (GetDlgItem (CB_LINE));
	CComboBox & cb = * (CComboBox *)GetDlgItem (CB_LINE);
	int nIndex = cb.GetCurSel ();

	if (nIndex != CB_ERR) 
		return cb.GetItemData (nIndex);

	return -1;
}

void CNewDocTypeDlg::OnSelchangeBox() 
{
	CComboBox * pBox = (CComboBox *)GetDlgItem (CB_BOX);
	CString str;

	ASSERT (pBox);

	int nIndex = pBox->GetCurSel ();

	if (nIndex != CB_ERR) {
		ULONG lBoxID = pBox->GetItemData (nIndex);
		int nFind = Foxjet3d::CTask::Find (m_vBoxes, lBoxID);

		if (nFind != -1)
			str = m_vBoxes [nFind].m_strDesc;
	}

	SetDlgItemText (TXT_BOXDESC, str);
}
