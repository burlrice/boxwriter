// TaskFrame.cpp : implementation file
//

#include "stdafx.h"
#include "mkdraw.h"
#include "TaskFrame.h"
#include "MainFrm.h"
#include "TaskView.h"
#include "HeadView.h"
#include "Debug.h"
#include "Extern.h"

//#include "MessageView.h"

using namespace EditorGlobals;
using namespace TaskView;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTaskFrame

IMPLEMENT_DYNCREATE(CTaskFrame, CMessageFrame)

CTaskFrame::CTaskFrame()
{
}

CTaskFrame::~CTaskFrame()
{
}


BEGIN_MESSAGE_MAP(CTaskFrame, CMessageFrame)
	//{{AFX_MSG_MAP(CTaskFrame)
	ON_WM_CLOSE()
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
	ON_WM_MDIACTIVATE()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTaskFrame message handlers

BOOL CTaskFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	const int nPanes = 2;
	const CString strClass = a2w (GetRuntimeClass ()->m_lpszClassName);
	const CString strPane [3] =
	{
		strClass + _T ("\\WindowState\\m_wndSplitter [0]\\Pane [0, 0]"),
		strClass + _T ("\\WindowState\\m_wndSplitter [1]\\Pane [0, 0]"),
		strClass + _T ("\\WindowState\\m_wndSplitter [1]\\Pane [1, 0]"),
	};
	const CString strDef [3] = 
	{
		_T ("0, 540"), //_T ("140, 260"),
		_T ("440, 80"),
		_T ("440, 170"),
	};
	CSize size [3];

	for (int i = 0; i < nPanes; i++) {
		CString str = theApp.GetProfileString (strPane [i], _T ("size"), strDef [i]);
		_stscanf (str, _T ("%d, %d"), &size [i].cx, &size [i].cy);
		TRACEF (str);
	}

	if (nPanes == 3) {
		if (!m_wndSplitter [0].CreateStatic (this, 1, 2)) {
			TRACEF ("CreateStatic failed");
			return FALSE;
		}

		if (!m_wndSplitter [0].CreateView (0, 0,
			RUNTIME_CLASS (HeadView::CHeadView), size [0], pContext)) 
		{
			TRACEF ("CreateView failed");
			return FALSE;
		}

		if (!m_wndSplitter [1].CreateStatic (
			&m_wndSplitter [0],     // our parent window is the first splitter
			2, 1,					// the new splitter is 2 rows, 1 column
			WS_CHILD | WS_VISIBLE | WS_BORDER,  // style, WS_BORDER is needed
			m_wndSplitter [0].IdFromRowCol(0, 1)))
		{
			TRACEF ("CreateStatic failed");
			return FALSE;
		}

		if (!m_wndSplitter [1].CreateView (0, 0,
			pContext->m_pNewViewClass, size [1], pContext))
		{
			TRACEF ("CreateView failed");
			return FALSE;
		}

		if (!m_wndSplitter [1].CreateView (1, 0,
			pContext->m_pNewViewClass, size [2], pContext))
		{
			TRACEF ("CreateView failed");
			return FALSE;
		}

		CWnd * pWnd = m_wndSplitter [1].GetPane (1, 0);
		
		if (pWnd->IsKindOf (RUNTIME_CLASS (CView)))
			SetActiveView ((CView *)pWnd);

		return TRUE;
	}
	else if (nPanes == 2) {
		if (m_wndSplitter [0].CreateStatic (this, 1, 2) &&
			m_wndSplitter [0].CreateView (0, 0,
				RUNTIME_CLASS (HeadView::CHeadView), size [0] /* CSize (130, 50) */, pContext) &&
			m_wndSplitter [0].CreateView (0, 1,
				pContext->m_pNewViewClass, CSize (0, 0), pContext))
		{
			SetActiveView ((CView *)m_wndSplitter [0].GetPane (0, 1));
			return TRUE;
		}
	}

	return FALSE;
}

void CTaskFrame::OnClose() 
{
	SaveSettings ();
	CMessageFrame::OnClose();
}

void CTaskFrame::SaveSettings() const
{
	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < m_wndSplitter [i].GetRowCount (); j++) {
			for (int k = 0; k < m_wndSplitter [i].GetColumnCount (); k++) {
				CWnd * pWnd = m_wndSplitter [i].GetPane (j, k);

				if (pWnd) {
					CString str, strSection;
					CRect rc;

					pWnd->GetWindowRect (&rc);
					strSection.Format (
						_T ("%s\\WindowState\\m_wndSplitter [%d]\\Pane [%d, %d]"),
						a2w (GetRuntimeClass ()->m_lpszClassName), i, j, k);
					str.Format (_T ("%d, %d"), rc.Width (), rc.Height ());
					theApp.WriteProfileString (strSection, _T ("size"), str);
					theApp.WriteProfileString (strSection, _T ("class"), 
						a2w (pWnd->GetRuntimeClass ()->m_lpszClassName));
				}
			}
		}
	}
}

void CTaskFrame::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd) 
{
	using namespace TaskDoc;

	CMessageFrame::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);

	if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) {
		if (CTaskView * pView = (CTaskView *)pDoc->GetView ()) {
			theApp.GetHeadBar ().SetActivePanel (pView->GetActivePanel ());
			theApp.GetHeadBar ().SetActiveHead (pView->GetActiveHead ());
		}
	}
}


void CTaskFrame::OnSetFocus(CWnd* pOldWnd) 
{
	TaskView::CTaskView * pView = DYNAMIC_DOWNCAST (TaskView::CTaskView, GetActiveView ());
	TaskDoc::CTaskDoc * pDoc = DYNAMIC_DOWNCAST (TaskDoc::CTaskDoc, GetActiveDocument ());

	//theApp.GetHeadBar ().ResetContent (pDoc);
	TRACEF ("CTaskFrame::OnSetFocus");
	CMessageFrame::OnSetFocus (pOldWnd);
}

