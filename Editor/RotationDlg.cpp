// RotationDlg.cpp : implementation file
//

#include "stdafx.h"
#include "mkdraw.h"
#include "RotationDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRotationDlg dialog


CRotationDlg::CRotationDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(IDD_ROTATION, pParent)
{
	//{{AFX_DATA_INIT(CRotationDlg)
	m_dXAxis = 0.0;
	m_dYAxis = 0.0;
	//}}AFX_DATA_INIT
}


void CRotationDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRotationDlg)
	DDX_Text(pDX, TXT_XAXIS, m_dXAxis);
	DDV_MinMaxDouble(pDX, m_dXAxis, -90., 90.);
	DDX_Text(pDX, TXT_YAXIS, m_dYAxis);
	DDV_MinMaxDouble(pDX, m_dYAxis, -90., 90.);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRotationDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CRotationDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRotationDlg message handlers
