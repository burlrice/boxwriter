#if !defined(AFX_EDITORDEFSDLG_H__2792A7B1_E52C_4CFA_890C_222F7E7BC3DF__INCLUDED_)
#define AFX_EDITORDEFSDLG_H__2792A7B1_E52C_4CFA_890C_222F7E7BC3DF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EditorDefsDlg.h : header file
//

#include "utils.h"

/////////////////////////////////////////////////////////////////////////////
// CEditorDefsDlg dialog

class CEditorDefsDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CEditorDefsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEditorDefsDlg)
	int		m_nUnits;
	//}}AFX_DATA
	int		m_nResizeHandle;
	BOOL m_bOverlapped;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditorDefsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditorDefsDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITORDEFSDLG_H__2792A7B1_E52C_4CFA_890C_222F7E7BC3DF__INCLUDED_)
