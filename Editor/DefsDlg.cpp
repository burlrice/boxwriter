// DefsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "mkdraw.h"
#include "DefsDlg.h"
#include "BaseElement.h"
#include "ElementDefaults.h"
#include "Coord.h"
#include "Debug.h"
#include "Database.h"
#include "extern.h"
#include "ElmntLst.h"
#include "Utils.h"

#ifdef __WINPRINTER__
	#include "TextElement.h"
#endif //__WINPRINTER__

#include <CDErr.h>

using namespace EditorGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CElementDefsDlg dialog


CElementDefsDlg::CElementDefsDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg(IDD_ELEMENTDEFS, pParent)
{
}

CElementDefsDlg::~CElementDefsDlg ()
{
	m_vElements.DeleteAllElements ();
}

void CElementDefsDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CElementDefsDlg)
	//}}AFX_DATA_MAP

	if (!pDX->m_bSaveAndValidate) 
		OnSelchangeElements ();
}


BEGIN_MESSAGE_MAP(CElementDefsDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CElementDefsDlg)
	ON_BN_CLICKED(BTN_FONT, OnFont)
	ON_BN_CLICKED(BTN_PROPERTIES, OnProperties)
	ON_LBN_DBLCLK(LB_ELEMENTS, OnDblclkElements)
	ON_LBN_SELCHANGE(LB_ELEMENTS, OnSelchangeElements)
	ON_BN_CLICKED(BTN_RESET, OnReset)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(CHK_FLIPH, OnAttributesChanged)
	ON_BN_CLICKED(CHK_FLIPV, OnAttributesChanged)
	ON_BN_CLICKED(CHK_INVERSE, OnAttributesChanged)
END_MESSAGE_MAP()

BOOL CElementDefsDlg::OnInitDialog() 
{
	BeginWaitCursor ();
	FoxjetCommon::CEliteDlg::OnInitDialog();
	CListBox * pLB = (CListBox *)GetDlgItem (LB_ELEMENTS);

	for (int i = 0; i < GetElementTypeCount (); i++) { 
		if (IsValidElementType (i, GetDefaultHead ())) {
			int nIndex = pLB->AddString (GetElementResStr (i));

			if (nIndex != LB_ERR) 
				pLB->SetItemData (nIndex, i);
		}
	}

	InitElements ();
	pLB->SetCurSel (0);
	OnSelchangeElements ();

	EndWaitCursor ();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CElementDefsDlg::InitElements()
{
	const CVersion ver = GetElementListVersion ();

	m_vDefs.RemoveAll ();
	m_vElements.DeleteAllElements ();

	for (int i = 0; i < GetElementTypeCount (); i++) {
		defElements.LoadRegElementDefs (i, ver);
		const CBaseElement & def = defElements.GetElement (i);
		CBaseElement * p = CopyElement (&def, NULL, false);
		
		m_vDefs.Add (defElements.GetRegElementDefs (i, ver));
		m_vElements.Add (p);

		const CString str = GetString (i);

		if (str.GetLength ()) {
			bool bResult = p->FromString (str, GetElementListVersion ());

			if (IsValidElementType (i, GetDefaultHead ())) {
				#ifdef _DEBUG
				if (!bResult) {
					CString str;
					str.Format (_T ("FromString failed: %s"), GetString (i));
					TRACEF (str);
					MsgBox (* this, str);
				}
				#endif //_DEBUG

				ASSERT (bResult);
			}
		}
	}
}

CBaseElement & CElementDefsDlg::GetElement(int nType) 
{
	ASSERT (nType >= 0 && nType < m_vElements.GetSize ());
	return m_vElements [nType];
}

void CElementDefsDlg::OnFont() 
{
	CListBox * pLB = (CListBox *)GetDlgItem (LB_ELEMENTS);
	int nIndex = pLB->GetCurSel ();

	if (nIndex != LB_ERR) {
		int nType = pLB->GetItemData (nIndex);
		CBaseElement & obj = GetElement (nType);

		if (obj.HasFont ()) {
			CBaseTextElement & e = * (CBaseTextElement *)&obj;
			CPrinterFont f;
			int nWidth = 0;

#ifdef __WINPRINTER__
			using namespace FoxjetElements;
			using namespace TextElement;

			if (CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, &e))
				nWidth = p->GetWidth ();
#endif //__WINPRINTER__

			e.Build ();

			bool bGetFont = e.GetFont (f);
			ASSERT (bGetFont);

			if (DoFontDialog (f, nWidth, e.GetImageData (), this)) {
				VERIFY (e.SetFont (f));

#ifdef __WINPRINTER__
				if (CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, &e))
					p->SetWidth (nWidth);
#endif //__WINPRINTER__

				TRACEF (e.ToString (CVersion ()));
				OnSelchangeElements ();
			}
		}
	}
}

void CElementDefsDlg::OnProperties() 
{
	CListBox * pLB = (CListBox *)GetDlgItem (LB_ELEMENTS);
	int nIndex = pLB->GetCurSel ();
	CEditorElementList list;

	list.CElementList::SetHead (GetDefaultHead ());

	if (nIndex != CB_ERR) {
		int nType = pLB->GetItemData (nIndex);
		CBaseElement & e = GetElement (nType);

		e.SetPos (CPoint (0, 0), NULL);
		OnEditElement (e, this, CSize (MAX_PRODLEN, MAX_PRODHEIGHT), INCHES, &list, false, true);
		OnSelchangeElements ();
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CElementDefsDlg::OnDblclkElements() 
{
	OnProperties ();	
}

void CElementDefsDlg::OnSelchangeElements() 
{
	CListBox * pLB = (CListBox *)GetDlgItem (LB_ELEMENTS);
	CButton * pFlipH = (CButton *)GetDlgItem (CHK_FLIPH);
	CButton * pFlipV = (CButton *)GetDlgItem (CHK_FLIPV);
	CButton * pInverse = (CButton *)GetDlgItem (CHK_INVERSE);
	CString strFont;
	int nIndex = pLB->GetCurSel ();
	bool bFlipH = false;
	bool bFlipV = false;
	bool bInverse = false;

	if (nIndex != CB_ERR) {
		int nType = pLB->GetItemData (nIndex);
		const CBaseElement & base = GetElement (nType);

		bFlipH = base.IsFlippedH ();
		bFlipV = base.IsFlippedV ();
		bInverse = base.IsInverse ();

		if (CBaseTextElement * p = DYNAMIC_DOWNCAST (CBaseTextElement, &base)) {
			if (p->HasFont ()) {
				const CBaseTextElement & e = * p;
				CString strTmp;
				CPrinterFont f;

				bool bGetFont = e.GetFont (f);
				ASSERT (bGetFont);

				strFont.Format (_T ("%s, %d, "), f.m_strName, f.m_nSize);

				if (f.m_bBold)
					strTmp += LoadString (IDS_BOLD);

				if (f.m_bItalic) {
					if (strTmp.GetLength ())
						strTmp += _T (", ");

					strTmp += LoadString (IDS_ITALIC);
				}

				if (strTmp.GetLength ())
					strFont += strTmp;
				else
					strFont += LoadString (IDS_REGULAR);


#ifdef __WINPRINTER__
				using namespace FoxjetElements;
				using namespace TextElement;

				if (CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, &e)) {
					int nWidth = p->GetWidth ();
					strTmp.Format (_T (", %d"), p->GetWidth ());
					strFont += strTmp;
				}
#endif //__WINPRINTER__

			}
		}
	}

	pFlipH->SetCheck (bFlipH ? 1 : 0);
	pFlipV->SetCheck (bFlipV ? 1 : 0);
	pInverse->SetCheck (bInverse ? 1 : 0);

	SetDlgItemText (TXT_FONTDEF, strFont);
	GetDlgItem (BTN_FONT)->EnableWindow (strFont.GetLength ());
}


CString & CElementDefsDlg::GetString(int nType) 
{
	ASSERT (nType >= 0 && nType < m_vDefs.GetSize ());
	return m_vDefs [nType];
}

void CElementDefsDlg::OnOK()
{
	for (int i = 0; i < GetElementTypeCount (); i++) {
		const CBaseElement & e = GetElement (i);
		CString str = e.ToString (GetElementListVersion ());
		
		GetString (i) = str;
	}

	FoxjetCommon::CEliteDlg::OnOK ();
}

void CElementDefsDlg::OnAttributesChanged()
{
	CListBox * pLB = (CListBox *)GetDlgItem (LB_ELEMENTS);
	CButton * pFlipH = (CButton *)GetDlgItem (CHK_FLIPH);
	CButton * pFlipV = (CButton *)GetDlgItem (CHK_FLIPV);
	CButton * pInverse = (CButton *)GetDlgItem (CHK_INVERSE);
	int nIndex = pLB->GetCurSel ();
	bool bFlipH = false;
	bool bFlipV = false;
	bool bInverse = false;

	if (nIndex != CB_ERR) {
		int nType = pLB->GetItemData (nIndex);
		CBaseElement & e = GetElement (nType);

		e.SetFlippedH (pFlipH->GetCheck () == 1);
		e.SetFlippedV (pFlipV->GetCheck () == 1);
		e.SetInverse (pInverse->GetCheck () == 1);
	}
}

void CElementDefsDlg::OnReset() 
{	
	if (MsgBox (* this, LoadString (IDS_CONFIRMRESET), NULL, MB_YESNO) == IDYES) {
		if (defElements.Reset (theApp.GetVersion ())) {
			
			InitElements ();

			if (CWnd * p = GetDlgItem (IDCANCEL))
				p->EnableWindow (FALSE);
		}
	}
}
