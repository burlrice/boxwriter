// NewMessageDocType.h: interface for the CNewMessageDocType class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NEWMESSAGEDOCTYPE_H__E076C4E8_559D_45B4_9052_F622E8992553__INCLUDED_)
#define AFX_NEWMESSAGEDOCTYPE_H__E076C4E8_559D_45B4_9052_F622E8992553__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MkDrawDocManager.h"
#include "Alias.h"
#include "Database.h"
#include "BaseDoc.h"

/* TODO
class CNewMessageDocType : public CNewDocType  
{
	DECLARE_DYNAMIC (CNewMessageDocType)
public:
	CNewMessageDocType ();
	virtual ~CNewMessageDocType ();

	FoxjetDatabase::HEADSTRUCT m_head;

private:
	CNewMessageDocType (const CNewMessageDocType & rhs);
	CNewMessageDocType & operator = (const CNewMessageDocType & rhs);
};

class CMessageDocTemplate : public FoxjetDocument::CBaseDocTemplate
{
	DECLARE_DYNAMIC (CMessageDocTemplate)
public:
	CMessageDocTemplate ();
	virtual ~CMessageDocTemplate ();
	
	virtual CDocument * CreateNewDocument ();
	virtual CString GetDefPrefix () const;
	virtual bool IsExistingDoc (const CString & strTitle) const;

	FoxjetDatabase::HEADSTRUCT m_head;

protected:
	CMessageDocTemplate (const CMessageDocTemplate & rhs);
	CMessageDocTemplate & operator = (const CMessageDocTemplate & rhs);
};
*/

#endif // !defined(AFX_NEWMESSAGEDOCTYPE_H__E076C4E8_559D_45B4_9052_F622E8992553__INCLUDED_)
