#include "stdafx.h"
#include "Parse.h"
#include <string>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CString Decode (const CString & str)
{
	CString strResult;

	for (int i = 0; i < str.GetLength (); i++) {
		TCHAR c = str [i];

		if (c < 256)
			strResult += c;
		else {
			CString s;

			s.Format (_T ("[0x%04X]"), c);
			strResult += s;
		}
	}

	return strResult;
}

void KanjiTest ()
{
	using namespace FoxjetDatabase;

	//const CString str = _T ("201701230023202927540001,猪圆环病毒2型杆状病毒载体灭活疫苗,（2014）外兽药证字27号,BI,+49-6132-77-0");
	const CString str = _T ("猪圆环病");

	TRACEF (str);
	TRACEF (Decode (str));

	CString strFormatted = FoxjetDatabase::FormatString (str);
	TRACEF (strFormatted);
	CString strUnformatted = FoxjetDatabase::UnformatString (strFormatted);
	TRACEF (strUnformatted);
	TRACEF (Decode (strUnformatted));

	ASSERT (str == strUnformatted);
	ASSERT (Decode (str) == Decode (strUnformatted));
	/*
	CString str = _T ("{MESSAGESTRUCT,2663,21,90,qr,,\\{Ver\\,4\\,13\\,925\\,0\\,47\\}\\{Timestamp\\,12/28/17 17:10:00\\}\\{ProdArea\\,15000\\,4000\\,0\\,21\\}\\{Barcode\\,1\\,0\\,0\\,0\\,0\\,0\\,21\\,0\\,1\\,\\,000000000\\,840\\,003\\,0\\,2\\,0\\,14\\,14\\,0\\{TextObj\\,DEF\\,Default barcode sub-element\\,zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz\\,201701230023202927540001\\\\\\,\\u732A\\u5706\\u73AF\\u75C5\\u6BD22\\u578B\\u6746\\u72B6\\u75C5\\u6BD2\\u8F7D\\u4F53\\u706D\\u6D3B\\u75AB\\u82D7\\\\\\,\\uFF082014\\uFF09\\u5916\\u517D\\u836F\\u8BC1\\u5B5727\\u53F7\\\\\\,BI\\\\\\,+49-6132-77-0\\,0\\,0\\,0\\,0\\,0\\,0\\,0\\,0\\}\\},0,1}");
	::OutputDebugStringW (_T ("猪\n"));
	::OutputDebugStringW (_T ("201701230023202927540001,猪圆环病毒2型杆状病毒载体灭活疫苗,（2014）外兽药证字27号,BI,+49-6132-77-0\n"));
	TRACEF (_T ("201701230023202927540001,猪圆环病毒2型杆状病毒载体灭活疫苗,（2014）外兽药证字27号,BI,+49-6132-77-0"));
	TRACEF (str);
	str = FoxjetDatabase::UnformatString (str);
	TRACEF ((std::string (w2a (str))).c_str ());
	TRACEF (UTF8ToUnicode (std::string (w2a (str))).c_str ());
	//TRACEF (UTF8ToUnicode (std::string (w2a (str))).c_str ());
	*/

	int n = 0;
}