// ChildFrm.cpp : implementation of the CMessageFrame class
//

#include "stdafx.h"
#include "mkdraw.h"

#include "List.h"
#include "Extern.h"

#include "MessageFrame.h"
#include "BaseView.h"
#include "BaseDoc.h"

using namespace EditorGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDocument;
using namespace TaskView;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMessageFrame

IMPLEMENT_DYNCREATE(CMessageFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMessageFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMessageFrame)
	ON_WM_MDIACTIVATE()
	ON_WM_NCACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMessageFrame construction/destruction

CMessageFrame::CMessageFrame()
{
}

CMessageFrame::~CMessageFrame()
{
}

BOOL CMessageFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CMDIChildWnd::PreCreateWindow(cs))
		return FALSE;

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMessageFrame diagnostics

#ifdef _DEBUG
void CMessageFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMessageFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMessageFrame message handlers

void CMessageFrame::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd) 
{
	CMDIChildWnd::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);

	if (bActivate) 
		UpdateDocView ();
}

BOOL CMessageFrame::OnNcActivate(BOOL bActive) 
{
	if (bActive) 
		UpdateDocView ();

	return CMDIChildWnd::OnNcActivate(bActive);
}

void CMessageFrame::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CMDIChildWnd::OnShowWindow(bShow, nStatus);
	
	if (bShow) {
		CWinApp * pApp = ::AfxGetApp ();

		if (pApp) {
			WINDOWPLACEMENT wp;

			wp.length = sizeof(WINDOWPLACEMENT);
			GetWindowPlacement (&wp);
			wp.showCmd = pApp->GetProfileInt (
				_T ("Settings\\CMessageFrame\\WindowState\\"),
				_T ("showCmd"), SW_SHOWMAXIMIZED);

			if (wp.showCmd == SW_SHOWMAXIMIZED) {
				wp.flags = WPF_RESTORETOMAXIMIZED;
				wp.showCmd = SW_SHOWMAXIMIZED;
				SetWindowPlacement (&wp);
			}
		}
	}
}

void CMessageFrame::UpdateDocView()
{
	CView * pActiveView = GetActiveView ();

	if (CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, GetActiveView ())) {
		pView->SetZoomStatus ();

		if (CBaseDoc * pDoc = pView->GetDocument ())
			pDoc->OnUpdateDocView ();
	}
}

BOOL CMessageFrame::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CMDIFrameWnd* pParentWnd, CCreateContext* pContext) 
{
	if (!CMDIChildWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, pContext))
		return FALSE;

	return TRUE;
}

void CMessageFrame::OnClose() 
{
	WINDOWPLACEMENT wp;

	if (GetWindowPlacement (&wp)) {
		::AfxGetApp ()->WriteProfileInt (
			_T ("Settings\\CMessageFrame\\WindowState\\"),
			_T ("showCmd"), wp.showCmd);
	}

	CMDIChildWnd::OnClose();
}
