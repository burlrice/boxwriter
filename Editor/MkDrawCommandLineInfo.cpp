// MkDrawCommandLineInfo.cpp: implementation of the CMkDrawCommandLineInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "mkdraw.h"
#include "MkDrawCommandLineInfo.h"
#include "Debug.h"
#include "Utils.h"
#include "Extern.h"
#include "Parse.h"
#include "BarcodeElement.h"
#include "System.h"

#ifdef __IPPRINTER__
	#include "DataMatrixElement.h"
#endif

using namespace EditorGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

static const CString strReadOnly		= _T ("READONLY");
static const CString strUser			= _T ("USER");
static const CString strDeleteDSN		= _T ("RESETDSN");
static const CString strNoSplash		= _T ("NOSPLASH");
static const CString strDelete			= _T ("DELETE");
static const CString strExport			= _T ("EXPORT");
static const CString strImport			= _T ("IMPORT");
static const CString strCleanRegistry	= _T ("CLEANREGISTRY");
static const CString strDSN				= _T ("DSN");
static const CString strDSNLocal		= _T ("DSN_LOCAL");
static const CString strHostAddress		= _T ("HOST_ADDRESS");
static const CString strIV				= _T ("IV");
static const CString strVX				= _T ("VX");
static const CString strMPHC			= _T ("MPHC");
static const CString strProduction		= _T ("PRODUCTION");
static const CString strDataMatrix		= _T ("DATAMATRIX");
static const CString strDemo			= _T ("DEMO");
static const CString strSW0883			= _T ("SW0883");
static const CString strNETSTAT			= _T ("NETSTAT");
static const CString strKill			= _T ("KILL");
static const CString strUID				= _T ("UID");
static const CString strPWD				= _T ("PWD");
static const CString strGojo			= _T ("GOJO");
static const CString strNetworked		= _T ("NETWORKED");


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMkDrawCommandLineInfo::CMkDrawCommandLineInfo()
:	m_bReadOnly (false),
	m_bResetDSN (false),
	m_bSplash (true),
	m_bDelete (false),
	m_bExport (false),
	m_bImport (false),
	m_bCleanRegistry (false),
	m_strUser (_T ("Editor")),
	m_bIV (false),
	m_bMPHC (false),
	m_bProductionConfig (false),
	m_bSw0883 (false),
	m_bShowNetStatDlg (false),
	m_bKill (false),
	m_bGojo (false),
	m_bNetworked (false),
	m_strDSN (FoxjetCommon::GetDSN ()),
	m_bDemo (false),
	CCommandLineInfo ()
{
	FoxjetDatabase::SetUsername (m_strUser);
}

CMkDrawCommandLineInfo::~CMkDrawCommandLineInfo()
{

}

void CMkDrawCommandLineInfo::ParseParam( LPCTSTR lpszParam, BOOL bFlag, BOOL bLast )
{
	CString strUpper (lpszParam);
	const CString str (lpszParam);

	strUpper.MakeUpper ();

	if (strUpper == ::strDemo) {
		#ifdef __WINPRINTER__
		FoxjetElements::CBarcodeElement::EnableDataMatrix (true);
		#else
		FoxjetIpElements::CDataMatrixElement::EnableDataMatrix (true);
		#endif
		m_bDemo = true;
	}

	if (strUpper == ::strReadOnly)
		m_bReadOnly = true;
	else if (strUpper == ::strNoSplash)
		m_bSplash = false;
	else if (strUpper == strIV || strUpper == strVX)
		m_bIV = true;
	else if (strUpper == ::strMPHC)
		m_bMPHC = true;
	else if (strUpper == ::strProduction)
		m_bProductionConfig = true;
	else if (strUpper == ::strDeleteDSN) {
		m_bResetDSN = true;
		m_bSplash = false;
	}
	else if (strUpper == ::strCleanRegistry) {
		m_bCleanRegistry = true;
	}
	else if (strUpper == ::strDataMatrix) {
		#ifdef __WINPRINTER__
		FoxjetElements::CBarcodeElement::EnableDataMatrix (true);
		#else
		FoxjetIpElements::CDataMatrixElement::EnableDataMatrix (true);
		#endif
	}
	else if (strUpper == _T ("ELITE")) {
		FoxjetDatabase::SetHighResDisplay (true);
	}
	else if (strUpper == _T ("MATRIX")) {
		FoxjetDatabase::SetHighResDisplay (false);
	}
	else if (strUpper == strNETSTAT) {
		m_bShowNetStatDlg = true;
	}
	else if (strUpper == strKill) {
		m_bKill = true;
	}
	else if (strUpper == strGojo) {
		m_bGojo = true;
	}
	else if (strUpper == strNetworked) {
		m_bNetworked = true;
	}
	else {
		CStringArray v;
		const TCHAR cDelim = '=';

		Tokenize (strUpper, v, cDelim);

		if (v.GetSize ()) {
			strUpper = v [0];

			if (strUpper == ::strUser) {//if (strUpper.Find (::strUser) != -1) {
				m_strUser = Extract (str, CString (cDelim));
				FoxjetDatabase::SetUsername (m_strUser);
				TRACEF (::strUser + ": " + m_strUser);
			}
			else if (strUpper == ::strDelete) {//else if (strUpper.Find (::strDelete) != -1) {
				m_bDelete = true;
				m_bSplash = false;
				m_strDelete = Extract (str, CString (cDelim));
				TRACEF (::strDelete + ": " + m_strDelete);
			}
			else if (strUpper == ::strExport) { //else if (strUpper.Find (::strExport) != -1) {
				m_bExport = true;
				m_bSplash = false;
				m_strExport = Extract (str, CString (cDelim));
				TRACEF (::strExport + ": " + m_strExport);
			}
			else if (strUpper == ::strImport) {//else if (strUpper.Find (::strImport) != -1) {
				m_bImport = true;
				m_bSplash = false;
				m_strImport = Extract (str, CString (cDelim));
				TRACEF (::strImport + ": " + m_strImport);
			}
			else if (strUpper == ::strDSN) {//else if (strUpper.Find (::strDSN) != -1) {
				m_strDSN = Extract (str, CString (cDelim));
				TRACEF (::strDSN + ": " + m_strDSN);
			}
			else if (strUpper == ::strDSNLocal) {
				m_strDSNLocal = Extract (str, CString (cDelim));
				TRACEF (::strDSNLocal + ": " + m_strDSNLocal);
			}
			else if (strUpper == ::strHostAddress) {//else if (strUpper.Find (::strHostAddress) != -1) {
				m_strHostAddress = Extract (str, CString (cDelim));
				TRACEF (::strHostAddress + ": " + m_strHostAddress);
			}
			else if (strUpper == ::strUID) {
				m_strUID = Extract (str, CString (cDelim));
			}
			else if (strUpper == ::strPWD) {
				m_strPWD = Extract (str, CString (cDelim));
			}
		}
	}

	CCommandLineInfo::ParseParam (lpszParam, bFlag, bLast);
}

CString CMkDrawCommandLineInfo::ToString () const
{
	CString str; 

	if (m_bReadOnly)					str += _T (" /") + ::strReadOnly;
	if (m_bSplash)						str += _T (" /") + ::strNoSplash;
	if (m_bIV)							str += _T (" /") + ::strVX;
	if (m_bMPHC)						str += _T (" /") + ::strMPHC;
	if (m_bProductionConfig)			str += _T (" /") + ::strProduction;
	if (!m_bSplash)						str += _T (" /") + ::strNoSplash;
#ifdef __WINPRINTER__
	if (FoxjetElements::CBarcodeElement::IsDataMatrixEnabled ())
										str += _T (" /") + ::strDataMatrix;
#else
	if (FoxjetIpElements::CDataMatrixElement::IsDataMatrixEnabled ())
										str += _T (" /") + ::strDataMatrix;
#endif

	if (m_strUser.GetLength ())			str += _T (" /") + ::strUser +			_T ("=") + m_strUser;
	if (m_strExport.GetLength ())		str += _T (" /") + ::strExport +		_T ("=") + m_strExport;
	if (m_strImport.GetLength ())		str += _T (" /") + ::strImport +		_T ("=") + m_strImport;
	if (m_strDSN.GetLength ())			str += _T (" /") + ::strDSN +			_T ("=") + m_strDSN;
	if (m_strHostAddress.GetLength ())	str += _T (" /") + ::strHostAddress +	_T ("=") + m_strHostAddress;
	if (m_strDSNLocal.GetLength ())		str += _T (" /") + ::strDSNLocal +		_T ("=") + m_strDSNLocal;

	return str;
}
