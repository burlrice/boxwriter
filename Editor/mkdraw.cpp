// mkdraw.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "mkdraw.h"

#include <afxadv.h>
#include <shlwapi.h>
#include <Tlhelp32.h>
#include <atlbase.h>
#include <afxsock.h>

#include "MainFrm.h"
#include "MessageFrame.h"
#include "Splash.h"
#include "MkDrawDocManager.h"
#include "List.h"
#include "coord.h"
#include "Debug.h"
#include "Database.h"
#include "DllVer.h"
#include "AboutDlg.h"
#include "Extern.h"
#include "File.h"
#include "BaseDoc.h"
#include "BaseView.h"
#include "Utils.h"
#include "MkDrawCommandLineInfo.h"
#include "WinMsg.h"
#include "TaskDoc.h"
#include "NewTaskDocType.h"
#include "AppVer.h"
#include "Export.h"
#include "Registry.h"
#include "OdbcRecordset.h"
#include "Documentation.h"
#include "DevGuids.h"
#include "Parse.h"
#include "ListGlobals.h"
#include "FieldDefs.h"
#include "NetStatDlg.h"
#include "ChangeDsnDlg.h"
#include "ProgressDlg.h"

#ifdef __WINPRINTER__
	#include "DatabaseElement.h"
	#include "UserElement.h"
	#include "DatabaseElement.h"
	#include "BitmapElement.h"
#endif

using namespace EditorGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDocument;
using namespace FoxjetDatabase;
using namespace TaskView;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const BYTE nPrivateKeyScrambled [] = 
{
	0xD3, 0x11, 0x28, 0x09, 0x02, 0x8A, 0x71, 0x83, 	0x1F, 0x26, 0x96, 0x09, 0xE8, 0x3A, 0xE7, 0xD0, 
	0x9B, 0x14, 0x0F, 0x18, 0x50, 0x91, 0x25, 0x09, 	0xC1, 0x58, 0x32, 0x9D, 0x6B, 0x46, 0x72, 0xDB, 
	0x2F, 0xE1, 0x37, 0x89, 0x7C, 0xB5, 0x6D, 0x65, 	0xFA, 0x2C, 0xC1, 0x7A, 0x26, 0xD7, 0x95, 0x4F, 
	0xE9, 0x28, 0xB6, 0xEC, 0x5A, 0x48, 0xA6, 0x63, 	0x15, 0xA7, 0xAF, 0xA0, 0x97, 0xF6, 0x43, 0xC6, 
	0x0F, 0x39, 0xC5, 0x17, 0x46, 0xF4, 0x79, 0x5F, 	0x0C, 0x1D, 0x01, 0xE3, 0x66, 0x13, 0xAE, 0xD5, 
	0x48, 0x21, 0x6B, 0xA8, 0x47, 0xA3, 0xB5, 0xBE, 	0x1C, 0x78, 0xDB, 0x51, 0x2D, 0xD5, 0x9D, 0x3D, 
	0x44, 0xF3, 0x43, 0x99, 0x01, 0xF9, 0xB9, 0xC7, 	0x30, 0x0D, 0x42, 0xF9, 0x00, 0xE7, 0x12, 0x4D, 
	0xB8, 0x1B, 0x6C, 0xC1, 0x91, 0xC5, 0xC9, 0x79, 	0x54, 0x47, 0x72, 0x2F, 0x5C, 0xB9, 0x75, 0xD4, 
	0x01, 0x81, 0x40, 0x80, 0xF3, 0x03, 0xDF, 0x85, 	0x9F, 0x62, 0x2A, 0x4E, 0xF9, 0x3C, 0x87, 0x34, 
	0x48, 0xB7, 0x59, 0x56, 0xD5, 0x0B, 0xF2, 0x33, 	0x1A, 0x5F, 0x4E, 0xE7, 0xB8, 0xD1, 0xD5, 0x96, 
	0x22, 0xAF, 0x79, 0xDB, 0x07, 0xE4, 0x1F, 0xEB, 	0x66, 0x53, 0x1E, 0x3F, 0x19, 0x6B, 0x71, 0xC3, 
	0xFF, 0x8F, 0x86, 0x09, 0x1D, 0x43, 0xD6, 0x9F, 	0x1D, 0xB6, 0xF0, 0x6C, 0xC0, 0xCF, 0xCA, 0x1A, 
	0x56, 0x38, 0x62, 0x9E, 0x42, 0x1C, 0x8A, 0x45, 	0xA9, 0x09, 0x22, 0x9A, 0xC3, 0x6A, 0x3E, 0xCF, 
	0xC3, 0xD7, 0x99, 0xBF, 0x97, 0x15, 0x49, 0xBE, 	0x65, 0xEB, 0x03, 0xCA, 0x9E, 0x1A, 0x41, 0x7B, 
	0x8B, 0xB7, 0xE8, 0xC4, 0x6D, 0x09, 0x31, 0xDE, 	0xCC, 0xC0, 0xC2, 0xF7, 0xAE, 0xC1, 0x39, 0xA7, 
	0x8D, 0x06, 0xCF, 0xAF, 0x45, 0xEE, 0xC9, 0x25, 	0x98, 0xD6, 0xCD, 0x2F, 0x4C, 0xAE, 0x5C, 0xAF, 
	0xE9, 0xCD, 0x46, 0x01, 0x81, 0x40, 0x87, 0xA0, 	0xD1, 0xF4, 0x89, 0x08, 0x40, 0x32, 0x4E, 0xBE, 
	0x19, 0x37, 0xDC, 0x22, 0x91, 0xE4, 0xBF, 0xF1, 	0x2B, 0xE3, 0x36, 0xFB, 0x5B, 0x1A, 0x1A, 0x34, 
	0xE9, 0x91, 0xCD, 0x6B, 0x05, 0x42, 0x88, 0x79, 	0x86, 0x43, 0x9D, 0x74, 0xF9, 0xBA, 0xB4, 0x8D, 
	0xF9, 0x2E, 0xE0, 0x6C, 0xCA, 0xEB, 0x22, 0xA3, 	0x4C, 0xB6, 0x6D, 0xD3, 0x4A, 0x1B, 0xA7, 0x9A, 
	0xC5, 0x74, 0x9A, 0x86, 0x19, 0xE7, 0xC8, 0xB7, 	0x12, 0x36, 0xC5, 0x06, 0xE7, 0xFA, 0xFD, 0xC5, 
	0x06, 0x56, 0xD9, 0x17, 0x53, 0x28, 0xD5, 0xAB, 	0x36, 0x7E, 0xD7, 0xCC, 0x88, 0x6A, 0xB2, 0xE9, 
	0x30, 0x28, 0x92, 0xB8, 0xED, 0xE0, 0xD4, 0xAD, 	0xDE, 0x14, 0x58, 0xA9, 0x6E, 0x5A, 0xA5, 0x0F, 
	0xB2, 0xA0, 0xC3, 0x81, 0x5C, 0xBB, 0xF1, 0x4B, 	0xF8, 0xFA, 0x19, 0x10, 0x66, 0x8F, 0x8F, 0x0F, 
	0x63, 0xFE, 0x88, 0x58, 0x3E, 0xB6, 0x01, 0x81, 	0x40, 0xC4, 0x5A, 0x99, 0x03, 0xB8, 0x12, 0xC2, 
	0x13, 0xE7, 0x37, 0xB4, 0xAD, 0xEB, 0x11, 0xBE, 	0x7F, 0x8C, 0xA8, 0x39, 0x71, 0xD3, 0x25, 0x1F, 
	0xAE, 0xA5, 0xD2, 0xF3, 0xD6, 0x7D, 0x17, 0x21, 	0x49, 0x01, 0x92, 0x73, 0x2C, 0x0E, 0xBE, 0x77, 
	0x40, 0xFF, 0x1C, 0x45, 0x9C, 0xAE, 0xD9, 0x22, 	0xEA, 0xEC, 0x52, 0x1F, 0x06, 0xD5, 0xE7, 0x98, 
	0xF7, 0xD2, 0x0F, 0xA9, 0xD1, 0xF4, 0xFA, 0x4D, 	0x0E, 0x8C, 0x13, 0xE5, 0xBC, 0x16, 0xAA, 0x6B, 
	0xE0, 0xE8, 0x2C, 0x77, 0xDB, 0xFA, 0xAF, 0x5B, 	0x36, 0xEE, 0xDD, 0x56, 0x08, 0x38, 0x75, 0x95, 
	0xB9, 0xAE, 0x72, 0xB5, 0x90, 0x06, 0xDA, 0x3F, 	0x5E, 0xF7, 0x92, 0xDA, 0xEA, 0x32, 0xA5, 0xC2, 
	0x69, 0x8F, 0xB2, 0xFC, 0xA3, 0xF4, 0xBF, 0x4A, 	0xDC, 0xE4, 0xE6, 0xB2, 0xD5, 0xDB, 0x05, 0x4D, 
	0x3A, 0x20, 0x0C, 0xF6, 0x30, 0x65, 0xFA, 0x79, 	0x47, 0x00, 0x81, 0x81, 0x40, 0x85, 0x2E, 0xE7, 
	0x10, 0xD8, 0xC8, 0x44, 0x87, 0x49, 0xE3, 0x1E, 	0x0C, 0xDA, 0xB4, 0x26, 0xF0, 0xC2, 0x80, 0xAA, 
	0xFF, 0x23, 0x08, 0x70, 0xE0, 0xA9, 0xBB, 0x59, 	0xD1, 0x32, 0xF9, 0x2A, 0x9E, 0x11, 0x44, 0x37, 
	0x85, 0x60, 0x5E, 0x5E, 0xF0, 0xEA, 0xE9, 0x61, 	0x41, 0x04, 0xB1, 0x80, 0x33, 0x5F, 0x2A, 0x05, 
	0x6F, 0x1F, 0x1D, 0x1A, 0xCC, 0xB1, 0x61, 0x5F, 	0x68, 0xF7, 0x97, 0x3C, 0xAE, 0x5F, 0x4C, 0x49, 
	0x3F, 0x16, 0xAF, 0xB9, 0x35, 0x69, 0x4D, 0xE8, 	0x9A, 0x40, 0x69, 0x42, 0x02, 0xB5, 0x28, 0x3C, 
	0x99, 0xD5, 0x37, 0xD7, 0xEC, 0xC8, 0x8A, 0x1B, 	0x5F, 0x72, 0x8D, 0x7F, 0x7F, 0x3A, 0xD7, 0x81, 
	0xFA, 0x49, 0x36, 0x69, 0xAE, 0xE5, 0x57, 0x6F, 	0x8C, 0x04, 0x1F, 0x59, 0x54, 0xDC, 0xDA, 0xD4, 
	0xD7, 0x9C, 0x7B, 0x61, 0x13, 0x42, 0x26, 0x08, 	0xA9, 0x47, 0x8B, 0x28, 0x8F, 0x00, 0x81, 0x81, 
	0x40, 0x83, 0xFB, 0x20, 0xE1, 0x19, 0x47, 0x44, 	0xB8, 0xA0, 0x55, 0xC6, 0xC7, 0x73, 0xBE, 0xA0, 
	0xBF, 0xCD, 0x84, 0x96, 0xE5, 0x9E, 0x0B, 0x3B, 	0xC9, 0xF5, 0x97, 0x81, 0x2A, 0xB3, 0x39, 0x07, 
	0xA6, 0x5E, 0x5E, 0x64, 0xB7, 0xE9, 0xC1, 0xB6, 	0x50, 0x05, 0xE0, 0x4C, 0x61, 0x3A, 0x6F, 0xBE, 
	0x15, 0x90, 0x49, 0x6A, 0x46, 0x05, 0xBD, 0xA0, 	0xD5, 0x74, 0x73, 0xD0, 0xA5, 0xF8, 0xDE, 0x9A, 
	0x74, 0x21, 0x3D, 0xA2, 0x67, 0x6A, 0x6A, 0x8C, 	0x92, 0x7D, 0x52, 0x11, 0x2A, 0x04, 0xF7, 0xC6, 
	0xCD, 0x1E, 0xE0, 0x37, 0x64, 0xD9, 0x48, 0xC1, 	0x97, 0xC6, 0xBA, 0x19, 0xF7, 0x35, 0x76, 0x9F, 
	0x0E, 0x82, 0x3F, 0x49, 0xF8, 0x43, 0xF2, 0x20, 	0xEA, 0x66, 0x84, 0xBD, 0xAB, 0xEB, 0xD5, 0xAB, 
	0x87, 0xFB, 0x47, 0x1E, 0x44, 0xFA, 0xBC, 0xA9, 	0x6F, 0xFC, 0x45, 0x7D, 0xB6, 0x02, 0x43, 0xD7, 
	0x04, 0x88, 0x1C, 0x8B, 0xF6, 0xBB, 0x80, 0xE3, 	0x57, 0x8C, 0x8F, 0x87, 0x8E, 0x27, 0x89, 0x3E, 
	0xDC, 0xD3, 0xB5, 0xD1, 0x32, 0xCA, 0x2B, 0x9B, 	0x90, 0x33, 0xBE, 0x59, 0x03, 0x0E, 0x8B, 0x21, 
	0x2E, 0xE3, 0xFC, 0xD5, 0x8A, 0x74, 0xB1, 0x05, 	0x33, 0xA3, 0x25, 0x6E, 0x67, 0xCF, 0x28, 0xEA, 
	0x96, 0xC0, 0x5C, 0x67, 0xEC, 0xFE, 0xDC, 0xEC, 	0xE9, 0xFD, 0x9B, 0x27, 0x02, 0xA6, 0x4D, 0x19, 
	0x52, 0xE0, 0xE5, 0xDF, 0x5E, 0x75, 0x4E, 0xD6, 	0x2B, 0xF4, 0x3E, 0x20, 0xC5, 0xED, 0x30, 0xCF, 
	0xD5, 0x01, 0xF0, 0xB2, 0xEB, 0x39, 0x58, 0x3D, 	0xB4, 0x04, 0x16, 0xDD, 0x57, 0x5D, 0xDF, 0xD0, 
	0x01, 0x3B, 0x27, 0x6E, 0x6A, 0xF9, 0xFC, 0x24, 	0x55, 0xB6, 0xCB, 0xED, 0xDA, 0x9E, 0x26, 0x97, 
	0x0E, 0xEB, 0x23, 0xA9, 0x94, 0xB3, 0xAC, 0x4C, 	0xFE, 0x70, 0x48, 0x08, 0x26, 0xC8, 0x0B, 0x4E, 
	0xC0, 0x00, 0x80, 0x41, 0x40, 0x80, 0x00, 0x80, 	0xC0, 0x40, 0xC0, 0x31, 0x7B, 0x50, 0x7B, 0xA1, 
	0x7A, 0xBB, 0xAE, 0xB9, 0x7C, 0x9E, 0xC4, 0x84, 	0x00, 0xB6, 0xA1, 0xFF, 0x22, 0x12, 0x8C, 0xD3, 
	0xA6, 0x66, 0x12, 0xA8, 0x68, 0xA9, 0x37, 0x33, 	0x29, 0x07, 0xA2, 0x1D, 0x60, 0xDD, 0x88, 0x12, 
	0x8A, 0x46, 0xEC, 0xD1, 0x1E, 0x15, 0xAC, 0x69, 	0xB5, 0xDE, 0xB9, 0xCF, 0x9C, 0xD5, 0xC7, 0x7D, 
	0xAD, 0xCC, 0x11, 0x70, 0x79, 0xD5, 0x06, 0x76, 	0x20, 0x36, 0xBA, 0xA2, 0xEC, 0x2D, 0xBB, 0xB4, 
	0x2E, 0x29, 0x95, 0xBE, 0x1C, 0xF0, 0x9B, 0xCA, 	0xD5, 0xAE, 0x5E, 0x19, 0xFA, 0x0B, 0x2C, 0xB5, 
	0xD9, 0x68, 0xE7, 0x56, 0xED, 0x9F, 0x29, 0x5D, 	0x1D, 0xFB, 0x7D, 0x89, 0xF1, 0xAC, 0x41, 0xD2, 
	0x44, 0x06, 0xCC, 0xEF, 0xDA, 0x06, 0x60, 0xD9, 	0x77, 0x7B, 0xB4, 0x31, 0xD6, 0x54, 0x48, 0x9C, 
	0xD4, 0x56, 0xF4, 0xBB, 0x5F, 0xE6, 0xEC, 0x8D, 	0xE7, 0x82, 0x93, 0x06, 0x61, 0x2A, 0xDF, 0x6C, 
	0xD3, 0x30, 0x36, 0xFD, 0x2B, 0x8F, 0x51, 0xEF, 	0xD5, 0x42, 0x3C, 0x0A, 0x9F, 0x8F, 0x49, 0xD3, 
	0x82, 0x5B, 0xAB, 0x8A, 0x3B, 0xA0, 0x8F, 0xF7, 	0xE6, 0x11, 0x10, 0xDD, 0xF4, 0x23, 0x5E, 0x82, 
	0x02, 0x0E, 0x4E, 0x11, 0x1A, 0xFD, 0x88, 0x65, 	0x2D, 0x8B, 0xB8, 0x1D, 0x0A, 0x31, 0xD7, 0xE9, 
	0x6B, 0x96, 0xD2, 0xCF, 0x4B, 0x65, 0x11, 0x54, 	0x5A, 0x1B, 0xD7, 0x6E, 0xBE, 0x95, 0xF5, 0xBE, 
	0x63, 0x63, 0xEB, 0x38, 0xF9, 0xF1, 0x71, 0x6C, 	0xC4, 0xB4, 0xCE, 0xA8, 0xE1, 0x17, 0x1D, 0xE0, 
	0xCF, 0x96, 0x45, 0x49, 0xFB, 0x4B, 0x59, 0x46, 	0xA1, 0x9C, 0xD5, 0xAA, 0xDB, 0x09, 0xAD, 0xB5, 
	0xE0, 0x75, 0x6A, 0xBE, 0xAB, 0x28, 0x97, 0x2B, 	0x66, 0x27, 0x0D, 0x7B, 0x38, 0xB9, 0xCB, 0x3E, 
	0x92, 0xEC, 0x2F, 0xAA, 0x91, 0x55, 0x6C, 0x61, 	0x96, 0xAB, 0x00, 0x80, 0x80, 0x41, 0x40, 0x00, 
	0x80, 0x40, 0x45, 0x20, 0x41, 0x0C, 
};


namespace ToolbarSettings
{
	static LPCTSTR lpszSummary	= _T ("Settings\\Toolbars\\Elite-Summary");
}; //namespace ToolbarSettings

CString LoadString (UINT nID)
{
	return FoxjetDatabase::LoadString (nID, _T ("mkdraw.exe"));
}

/////////////////////////////////////////////////////////////////////////////
// CMkDrawApp

BEGIN_MESSAGE_MAP(CMkDrawApp, CWinApp)
	//{{AFX_MSG_MAP(CMkDrawApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateFileOpen)
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
	ON_COMMAND(ID_DEBUGGINGSTUFF_UPGRADETASKS, OnDebuggingstuffUpgradetasks)
	ON_COMMAND(ID_FILE_CHANGEDSN, OnFileChangedsn)
	ON_COMMAND(ID_APP_CLOSE, OnAppClose)
	//}}AFX_MSG_MAP
	// Standard file based document commands
//	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	ON_COMMAND(ID_FILE_PAGE_SETUP, OnFilePrintSetup)
	ON_COMMAND(ID_FILE_BATCHPRINT, OnFileBatchPrint)

	#ifdef _DEBUG
	ON_COMMAND(ID_FILE_SAVEAS10, OnFileSave10000)
	#endif

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMkDrawApp construction

const LPCTSTR CMkDrawApp::m_lpszSettings = _T ("Settings");
const LPCTSTR CMkDrawApp::m_lpszOverlapped = _T ("Overlap warning");

CMkDrawApp::CMkDrawApp()
:	m_db (_T (__FILE__), __LINE__),
	m_bServer (false),
	m_strUser (_T ("Editor")),
	m_bShowNetStatDlg (false),
	m_bGojo (false),
	m_bDemo (false),
	m_bInitialized (false),
	m_strDSN (FoxjetCommon::GetDSN ()),
	m_hCallWndProcRet (NULL)
{
	#ifdef _DEBUG
	using namespace FoxjetDebug;

	if (!FoxjetDatabase::IsRunning (ISEDITORRUNNING)) {
		CDebugFile * pFile = new CDebugFile ();
		int nFlags = CFile::modeCreate | CFile::modeReadWrite;
		CString str, strFile = GetAppTitle () + _T (".log");

		BOOL bOpen = pFile->Open (strFile, nFlags);

		if (bOpen) {
			::afxDump.m_pFile = pFile;
			
			str.Format (_T ("\nLog started on: %s\n\n"), CTime::GetCurrentTime ().Format (_T ("%c"))); 
			TRACEF (str);
		}
		else {
			MsgBox (_T ("failed to open log"));
			delete pFile;
		}
	}

	#endif //_DEBUG
}

CMkDrawApp::~CMkDrawApp()
{
	#ifdef _DEBUG
	if (::afxDump.m_pFile) {
		::afxDump.m_pFile->Close ();
		delete (FoxjetDebug::CDebugFile *)::afxDump.m_pFile;
		::afxDump.m_pFile = NULL;
	}
	#endif //_DEBUG
}


static void CALLBACK StatusFct (const CString & str)
{
	theApp.SetStatusText (str);
}

#ifdef _DEBUG
static CString MsgBoxResult (int n)
{
	switch (n) {
		case IDABORT:		return _T ("IDABORT	");
//		case IDCONTINUE:	return _T ("IDCONTINUE");
		case IDIGNORE:		return _T ("IDIGNORE	");
		case IDNO:			return _T ("IDNO		");
		case IDRETRY:		return _T ("IDRETRY	");
//		case IDTRYAGAIN:	return _T ("IDTRYAGAIN");
		case IDYES:			return _T ("IDYES		");
		case IDCANCEL:		return _T ("IDCANCEL	");
		case IDOK:			return _T ("IDOK		");
	}

	return _T ("[unknown]");
}

#endif //_DEBUG


/////////////////////////////////////////////////////////////////////////////
// CMkDrawApp initialization

#ifdef _DEBUG
#include "ImportFileDlg.h"
#include <string>
void KanjiTest ();
int CALLBACK InitializeLynxPreview ();
int CALLBACK DrawLynxPreview (const CString & strFile, const CMapStringToString & map, DIBSECTION * pds, BYTE ** pData);
int CALLBACK ReleaseLynxPreview ();
#endif

BOOL CMkDrawApp::InitInstance()
{	
#ifdef _DEBUG
	#if 0
	{
		DIBSECTION ds = { 0 };
		BYTE * pData = NULL;
		CMapStringToString map; 
		CString strFile = _T ("C:\\Dev\\Diagraph\\Unified\\logo.yml");
		
		strFile = FoxjetFile::ReadDirect (strFile);

		map.SetAt (_T ("1"), _T ("one"));
		map.SetAt (_T ("2"), _T ("two"));
		map.SetAt (_T ("3"), _T ("three"));

		VERIFY (InitializeLynxPreview ());
		VERIFY (DrawLynxPreview (strFile, map, &ds, &pData));

		if (pData) {
			CxImage img;

			img.CreateFromArray (pData, ds.dsBm.bmWidth, ds.dsBm.bmHeight, ds.dsBm.bmBitsPixel, ds.dsBm.bmWidthBytes, true);
			//img.Save (GetHomeDir () + _T ("\\Debug\\test.bmp"), CXIMAGE_FORMAT_BMP);
			HBITMAP h = img.MakeBitmap ();
			SAVEBITMAP (h, GetHomeDir () + _T ("\\Debug\\test.bmp"));
			::DeleteObject (h);
			delete [] pData;
		}

		VERIFY (ReleaseLynxPreview ());
	}
	#endif
#endif

	IsInstallerInvoked ();

	if (!CInstance::Elevate ())
		return FALSE;

	FoxjetCommon::CheckDllVersions ();

	VERIFY (EnablePrivilege (SE_RESTORE_NAME));

	{
		WSADATA wsaData;

		if (::WSAStartup (MAKEWORD (2,1),&wsaData) != 0){
			TRACEF ("WSAStartup failed: " + FormatMessage (GetLastError ()));
			return 0;
		}
	}

	const CString strExportSection = _T ("Software\\FoxJet\\") + GetAppTitle () + _T ("\\Export");

	Foxjet3d::CProgressDlg::SetStatusFct (StatusFct);

	// Parse command line for standard shell commands, DDE, file open
	CMkDrawCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
	FoxjetDatabase::SetNetworked (cmdInfo.m_bNetworked);
	FoxjetDatabase::SetGojo (m_bGojo = cmdInfo.m_bGojo);
	m_bDemo = cmdInfo.m_bDemo;

	if (cmdInfo.m_strDSNLocal.CompareNoCase (cmdInfo.m_strDSN) != 0)
		m_strDSNLocal = cmdInfo.m_strDSNLocal;
	else
		cmdInfo.m_strDSNLocal.Empty ();

	m_strFlags = cmdInfo.ToString ();

	#ifdef _DEBUG
	ASSERT (::GetFileAttributes (_T ("C:\\Dev\\BoxWriter\\Installation\\MarksmanELITE\\Runtime Files\\Printer Files\\MarksmanElite.mdb")) == -1);
	ASSERT (::GetFileAttributes (_T ("C:\\Dev\\BoxWriter\\Installation\\MarksmanELITE\\Runtime Files\\Printer Files\\MarksmanVX.mdb")) == -1);
	ASSERT (::GetFileAttributes (_T ("C:\\Dev\\BoxWriter\\Installation\\MarksmanNET\\Runtime Files\\Printer Files\\MarksmanNET.mdb")) == -1);
	#endif

	if (cmdInfo.m_bIV) 
		FoxjetDatabase::SetDriver (GUID_INTERFACE_IVPHC);
	else if (cmdInfo.m_bMPHC) 
		FoxjetDatabase::SetDriver (GUID_INTERFACE_FXMPHC);

	if (cmdInfo.m_bDelete) {
		DWORD dwSize = 256;
		TCHAR szUser [256] = { 0 };

		::DeleteFile (cmdInfo.m_strDelete);

		// this is a hack.  installer creates shortcuts under
		// "all users", uninstall.exe attempts to delete using current username

		::GetUserName (szUser, &dwSize);
		cmdInfo.m_strDelete.Replace (szUser, _T ("All Users"));

		::DeleteFile (cmdInfo.m_strDelete);

		return TRUE;
	}

	if (cmdInfo.m_bKill) {
		HWND hwnd = NULL;
		const int nMax = 20;
		int i;
		DWORD dwResult = 0;

		for (i = 0; (i < nMax) && (hwnd = FindKeyboard ()); i++) {
			LRESULT lResult = ::SendMessageTimeout (hwnd, WM_SHUTDOWNAPP, SHUTDOWN_ALL, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);
			::PostMessage (hwnd, WM_COMMAND, 32778, 0);
			::Sleep (250);
		}

		LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_SHUTDOWNAPP, SHUTDOWN_ALL, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 10000, &dwResult);

		if (i >= nMax || IsRunning (ISKEYBOARDRUNNING) || IsRunning (ISKEYBOARDMONITORRUNNING))	
			MsgBox (LoadString (IDS_KILLKBFAILED), MB_ICONERROR, 0);

		return TRUE;
	}

	if (cmdInfo.m_bResetDSN) {
		FoxjetDatabase::DeleteDSN (GetDSN ());
		return TRUE;
	}

	if (cmdInfo.m_bCleanRegistry) {
		LPCTSTR lpsz = _T ("Software\\FoxJet");

		/* TODO: rem
		const CString strSection = defElements.m_strRegSection + _T ("\\Elements");
		CMapStringToString v;

		for (int i = 0; i < GetElementTypeCount (); i++) {
			CString strKey = GetElementResStr (i);
			CString strData = FoxjetDatabase::GetProfileString (strSection, strKey, _T (""));

			v.SetAt (strKey, strData);
			TRACEF (strData);
		}
		*/

		DWORD dwCU = ::SHDeleteKey (HKEY_CURRENT_USER, lpsz);
		DWORD dwLM = ::SHDeleteKey (HKEY_LOCAL_MACHINE, lpsz);

		/* TODO: rem
		TRACEF (strSection);

		for (POSITION pos = v.GetStartPosition (); pos; ) {
			CString strKey, strData;

			v.GetNextAssoc (pos, strKey, strData);
			TRACEF (strData);

			if (strData)
				FoxjetDatabase::WriteProfileString (strSection, strKey, strData);
		}
		*/

		return TRUE;
	}

	if (FoxjetDatabase::IsRunning (ISEDITORRUNNING)) {
		DWORD dwResult;

		LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_ISEDITORRUNNING, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 5000, &dwResult);
		
		return FALSE; 
	}

	m_instance.Create (ISEDITORRUNNING);

	AfxEnableControlContainer();
	VERIFY (::AfxOleInit ());

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	SetRegistryKey (_T ("FoxJet"));

	if (m_pszProfileName) {
		free ((void*)m_pszProfileName);
		m_pszProfileName = _tcsdup (GetAppTitle ());
	}

	COdbcDatabase::RegisterDisplayFct (COdbcDatabase::DisplayStatus);

	defElements.SetOverlappedWarning (GetProfileInt (m_lpszSettings, m_lpszOverlapped, 0) ? true : false);

	//HKEY_CURRENT_USER\Software\FoxJet\BoxWriter ELITE\Settings\Toolbars\Elite-Summary
	TRACEF (ToolbarSettings::lpszSummary);

	int nBars = GetProfileInt (ToolbarSettings::lpszSummary, _T ("Bars"), -1);

	if (nBars == -1) {
	
		// assume the toolbar state has never been saved, default it

		HINSTANCE hModule = ::AfxGetInstanceHandle (); 
		bool bResult = false;
		bool bDelete = false;
		const CString strFilepath = GetHomeDir () + _T ("\\toolbar.reg");

		if (HRSRC hRSRC = ::FindResource (hModule, MAKEINTRESOURCE (IDR_TOOLBAR_REG), _T ("REGISTRY"))) {
			DWORD dwSize = ::SizeofResource (hModule, hRSRC);

			if (HGLOBAL hGlobal = ::LoadResource (hModule, hRSRC)) {
				if (LPVOID lpData = ::LockResource (hGlobal)) {
					if (FILE * fp = _tfopen (strFilepath, _T ("wb"))) {
						fwrite (lpData, dwSize, 1, fp);
						fclose (fp);
						bDelete = true;
					}
				}
			}
		}

		// file created by: /savebarstate

		if (::GetFileAttributes (strFilepath) != -1) {
			HKEY hKey = NULL;
	
			VERIFY (EnablePrivilege (SE_RESTORE_NAME));

			if (::RegCreateKey (HKEY_CURRENT_USER, _T ("Software\\FoxJet\\BoxWriter ELITE\\Settings\\Toolbars"), &hKey) == ERROR_SUCCESS) {
				const CString strFile = GetHomeDir () + _T ("\\toolbar.reg");

				LONG lResult = ::RegRestoreKey (hKey, strFilepath, 0);
				TRACEF (FormatMessage (lResult)); 

				::RegCloseKey (hKey);

				if (lResult == ERROR_SUCCESS && bDelete)
					::DeleteFile (strFilepath);
			}
		}
		/*
		using namespace ToolbarSettings;

		const int n = sizeof (map) / sizeof (map [0]);

		for (int i = 0; i < n; i++)
			WriteProfileInt (map [i].m_lpszSection, map [i].m_lpszEntry, map [i].m_dwValue);
		*/
	}

	if (cmdInfo.m_bSplash) {
		CSplashWnd::EnableSplashScreen ();
		CSplashWnd::ShowSplashScreen (::AfxGetMainWnd ());
		::Sleep (CSplashWnd::m_nMinDisplayTime);
	}

	FoxjetDatabase::COdbcDatabase & db = GetDB ();

	#if 0 //#ifdef _DEBUG
	try
	{
		COdbcCache db = COdbcDatabase::Find (_T ("chempax"));

		{
			COdbcRecordset rst (db.GetDB ());
			CString str = _T ("SELECT TOP 10 * FROM [PUB].[PROD-PKG]");

			if (rst.Open (str, CRecordset::forwardOnly)) {
				for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
					CODBCFieldInfo info;

					rst.GetODBCFieldInfo (i, info);
					TRACEF (FoxjetDatabase::ToString (i) + _T (": ") + info.m_strName);
				}

				while (!rst.IsEOF ()) {
					TRACEF ((CString)rst.GetFieldValue (_T ("Prod-Pkg-code")));
					rst.MoveNext ();
				}
			}
		}

		{
			COdbcRecordset rst (db.GetDB ());
			CString str = _T ("SELECT TOP 10 * FROM [PUB].[Product]");

			if (rst.Open (str, CRecordset::forwardOnly)) {
				for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
					CODBCFieldInfo info;

					rst.GetODBCFieldInfo (i, info);
					TRACEF (FoxjetDatabase::ToString (i) + _T (": ") + info.m_strName);
				}

				while (!rst.IsEOF ()) {
					TRACEF ((CString)rst.GetFieldValue (_T ("Product-Code")));
					rst.MoveNext ();
				}
			}
		}

		{
			COdbcRecordset rst (db.GetDB ());
			CString str = 
				_T ("SELECT ") 
				// _T ("TOP 10 ")
				_T ("* ")
//				_T ("*, ")
//				_T ("'Net Weight ' & [PUB].[Prod-Pkg].[weight] & [PUB].[Prod-Pkg].[weight-um] & ' (' & [PUB].[Prod-Pkg].[weight]*0.4536 & ' kg)' AS weight_calc")
//				_T ("[PUB].[Prod-Pkg].[weight] AS weight_calc")
				_T (" FROM [PUB].[Product] INNER JOIN [PUB].[Prod-Pkg] ON [PUB].[Product].[Product-Code] = Left ([PUB].[Prod-Pkg].[Prod-Pkg-code], 7)")
				_T (" WHERE [PUB].[Product].[Product-Code]='C000006'");
				;

			/*
			SELECT *, 'Net Weight ' & [Prod-Pkg].[weight] & [Prod-Pkg].[weight-um] & ' (' & [Prod-Pkg].[weight]*0.4536 & ' kg)' AS weight_calc
			FROM Product INNER JOIN [Prod-Pkg] ON Product.[Product-Code] = Left ([Prod-Pkg].[Prod-Pkg-code], 7);
			*/

			if (rst.Open (str, CRecordset::forwardOnly)) {

				for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
					CODBCFieldInfo info;

					rst.GetODBCFieldInfo (i, info);
					TRACEF (FoxjetDatabase::ToString (i) + _T (": ") + info.m_strName);
				}

				while (!rst.IsEOF ()) {
					CString s;

					for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
						CODBCFieldInfo info;

						s += (CString)rst.GetFieldValue ((short)i) + _T (", ");

						//if (i > 10)
						//	break;
					}

					TRACEF (s);
					rst.MoveNext ();
				}

				int n = 0;
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	#endif

	if (!db.IsOpen ()) {
		CString strDSN = GetDSN ();
		CString strName = GetDatabaseName ();

		if (cmdInfo.m_strDSN.GetLength ()) {
			m_strDSN = strDSN = cmdInfo.m_strDSN;
			//strName = _T (""); // causes OpenDatabase to fail to create if file missing
			m_strHostAddress = cmdInfo.m_strHostAddress;
		}

		{
			CString strKey = _T ("Software\\Foxjet\\MarksmanELITE\\ODBC\\") + m_strDSN;
			
			cmdInfo.m_strUID = FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), _T ("/UID="));
			cmdInfo.m_strPWD = FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), _T ("/PWD="));

			if (theApp.IsGojo ()) {
				if (!cmdInfo.m_strUID.GetLength ()) cmdInfo.m_strUID = _T ("FoxJet");
				if (!cmdInfo.m_strPWD.GetLength ()) cmdInfo.m_strPWD = _T ("FoxJet");
			}

			TRACEF (strKey);
			TRACEF (_T ("/UID=") + cmdInfo.m_strUID + _T (" /PWD=") + cmdInfo.m_strPWD);

			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("UID"), cmdInfo.m_strUID); 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("PWD"), cmdInfo.m_strPWD); 
		}

		bool bConnected = false;

		if (!IsNetworked ())
			bConnected = FoxjetCommon::OpenDatabase (db, strDSN, strName);
		else {
			if (!(bConnected = FoxjetCommon::OpenDatabase (db, strDSN, strName, false)))
				if (m_strDSNLocal.GetLength ())
					bConnected = FoxjetCommon::OpenDatabase (db, m_strDSNLocal, strName);
		}

		if (!bConnected) {
			CString str, strPath, strTitle;

			GetBackupPath (strPath, strTitle);

			if (!IsNetworked ())
				str = LoadString (IDS_FAILEDTOINITIALIZEDB) + _T ("\nCheck: ") + strPath + '\\' + strTitle;
			else
				str = LoadString (IDS_FAILEDTOINITIALIZEDB);

			::MessageBox (NULL, str, ::AfxGetAppName (), MB_ICONERROR | MB_OK);
			FoxjetCommon::ExitInstance (GetElementListVersion ());
			::AfxAbort ();
		}

		COdbcDatabase::SetAppDsn (FoxjetDatabase::Extract (db.GetConnect (), _T ("DSN="), _T (";")));
	}


	/* TODO: rem
	{
		try {
			COdbcDatabase db (_T (__FILE__), __LINE__);

			db.OpenEx (_T ("DSN=MarksmanELITE"), CDatabase::noOdbcDialog | CDatabase::useCursorLib);
			CString str;
			PRINTERSTRUCT cs;

			cs.m_lID = 0;
			cs.m_strName = _T ("\"M\"a\"s\"t\"e\"r\"");
			cs.m_strAddress = _T ("'0'.0.0.0'");
			cs.m_bMaster = true;
			cs.m_type = ELITE;

			{
				CString str;
				
				/*
				CString FormatSQL (const CString & str)
				{
					CString strResult = str;

					//strResult.Replace (_T ("\""), _T ("\"\""));
					strResult.Replace (_T ("'"), _T ("''"));

					return strResult;
				}
				* /

				str.Format ( 
					_T ("INSERT INTO Printers (Name,Address,Master,Type) VALUES (")
					_T ("\n\t'%s',")
					_T ("\n\t'%s',")
					_T ("\n\t'%d',")
					_T ("\n\t'%d'")
					_T ("\n\t)"),
					FormatSQL (cs.m_strName),	
					FormatSQL (cs.m_strAddress),
					cs.m_bMaster,
					(long)cs.m_type);

				TRACEF (str);
				db.ExecuteSQL (str);
			}

			{
				COdbcRecordset rst (&db);

				rst.Open (str);
				ASSERT (!rst.IsEOF ());
				cs.m_lID = (long)rst.GetFieldValue (Printers::m_lpszID);
				TRACEF (ToString (cs.m_lID));
			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}
	*/

	m_bShowNetStatDlg = cmdInfo.m_bShowNetStatDlg;

	{
		SETTINGSSTRUCT s;

		s.m_lLineID = 0;
		s.m_strKey = _T ("Editor: ") + FoxjetCommon::CVersion (FoxjetCommon::verApp).ToString ();
		s.m_strData = CTime::GetCurrentTime ().Format (_T ("%c"));

		if (!UpdateSettingsRecord (db, s))
			VERIFY (AddSettingsRecord (db, s));
	}


	{
		CString strCmdLine = ::GetCommandLine ();

		strCmdLine.MakeUpper ();
	
		if (strCmdLine.Find (_T ("/ULTRARES")) != -1) {
			FoxjetDatabase::EnableUltraRes (true);
			m_strFlags += _T (" /ULTRARES");
		}
	}		

	{
		ULONG lPrinterID = GetPrinterID (db);
		CString strCmdLine = ::GetCommandLine ();

		strCmdLine.MakeUpper ();

		FoxjetFile::SetWriteDirect (_tcsstr (strCmdLine, _T ("/WRITE_DIRECT")) != NULL);

		CString strPrinterID = GetCmdlineValue (strCmdLine, _T ("/PRINTERID="));

		if (FoxjetDatabase::IsNetworked ())
			lPrinterID = GetMasterPrinterID (db);

		if (strPrinterID.GetLength ())
			lPrinterID = _tcstoul (strPrinterID, NULL, 10);

		SetPrinterID (lPrinterID); 
	}

	#ifdef __WINPRINTER__
	FoxjetCommon::SetProductionConfig (cmdInfo.m_bProductionConfig, db);
	#endif

	/*
	if (cmdInfo.m_bExport) {
		CString str = cmdInfo.m_strExport;
		CLongArray ids;
		CArray <LINESTRUCT, LINESTRUCT &> vLines;

		GetPrinterLines (db, GetPrinterID (db), vLines); //GetLineRecords (db, vLines);

		for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {	
			CArray <TASKSTRUCT *, TASKSTRUCT *> v;
			ULONG lLineID = vLines [nLine].m_lID;

			GetTaskRecords (db, lLineID, v);

			for (int i = 0; i < v.GetSize (); i++)
				ids.Add (v [i]->m_lID);

			Free (v);
		}
		
		BeginWaitCursor ();

		if (!str.GetLength ()) 
			str = CreateTempFilename (_T ("."), _T ("Export"), _T ("txt"));

		bool bExport = FoxjetCommon::Export (db, str, ids, IMPORT_ALL);

		if (!bExport) {
			CString strMsg;

			strMsg.Format (LoadString (IDS_FAILEDTOEXPORT), str);
			MsgBox (strMsg);
		}
		else 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strExportSection, _T ("Last"), str);
		
		EndWaitCursor ();
		
		return bExport;
	}
	*/

	{
		BYTE n [ARRAYSIZE (::nPrivateKeyScrambled)] = { 0 };

		memcpy (n, ::nPrivateKeyScrambled, ARRAYSIZE (::nPrivateKeyScrambled));
		Unscramble (n, ARRAYSIZE (n));
		FoxjetDatabase::Encrypted::RSA::SetPrivateKey (n, ARRAYSIZE (n));

		#ifdef _DEBUG
		const CString strData = _T ("1234567890-0987654321234567890-");
		const CString strEncrypted = FoxjetDatabase::Encrypted::RSA::Encrypt (strData);
		const CString strDecrypted = FoxjetDatabase::Encrypted::RSA::Decrypt (strEncrypted);

		ASSERT (strData == strDecrypted);

		TRACEF (FoxjetDatabase::Encrypted::InkCodes::GetRegSection ());
		TRACEF (FoxjetDatabase::Encrypted::InkCodes::GetRegRootSection ());
		FoxjetDatabase::Encrypted::Trace (FoxjetDatabase::Encrypted::InkCodes::GetRegRootSection (), 20);

		TRACEF (ToString (Encrypted::InkCodes::IsEnabled ()));

		FoxjetDatabase::Encrypted::InkCodes::Restore (GetHomeDir () + _T ("\\data.txt"));

		#endif
	}

	if (!FoxjetCommon::InitInstance (db, GetElementListVersion ())) {
		MsgBox (LoadString (IDS_FAILEDTOINITIALIZEDLL), ::AfxGetAppName (), MB_ICONERROR | MB_OK);
		FoxjetCommon::ExitInstance (GetElementListVersion ());
		::AfxAbort ();
	}

#ifdef _DEBUG
	#if 0
	{
		using namespace Foxjet3d;

		CImportFileDlg::IMPORTTHREADSTRUCT p;

		p.m_strFile = _T ("C:\\Dev\\Diagraph\\Unified\\CHINA5-4900.yml");
		int n = CImportFileDlg::Import (&p);
		TRACEF (ToString (n));
	}
	#endif
#endif

#ifdef _DEBUG
	#if 0
	{
		Foxjet3d::CImportFileDlg::GetMessages (_T ("192.168.1.100"), 29044, _T ("ADVANCED"), _T ("12345"));
		//Foxjet3d::CImportFileDlg::RciGetMessages (_T ("192.168.1.100"), 29043, _T ("ADVANCED"), _T ("12345"));
	}
	#endif
#endif

	LoadStdProfileSettings(IsMatrix () ? 2 : 5);  // Load standard INI file options (including MRU)
	UpdateRecentFileList ();
	
	if (m_pDocManager)
		delete m_pDocManager;

	m_pDocManager = new CMkDrawDocManager ();
	// End changes CGH

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;

	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;

	m_pMainWnd = pMainFrame;

	if (!RestoreState (pMainFrame)) 
		pMainFrame->ShowWindow (m_nCmdShow);

	if (!theApp.IsDemo ()) {
		WINDOWPLACEMENT wp;

		ZeroMemory (&wp, sizeof (wp));
		wp.length = sizeof (WINDOWPLACEMENT);
		pMainFrame->GetWindowPlacement (&wp);
		wp.showCmd = SW_SHOWMAXIMIZED;
		pMainFrame->SetWindowPlacement (&wp);
	}

	FoxjetMessageBox::SetAfxMainWndValid (true);

	{
		CString str = ::GetCommandLine ();

		str.MakeLower ();

		if (str.Find (_T ("/dbcheck")) != -1) {
			DoDbIntegerityCheck ();
		}
	}

	CString strTitle = GetAppTitle () + _T (" ") + LoadString (AFX_IDS_APP_TITLE);

	if (cmdInfo.m_strDSN.GetLength () && cmdInfo.m_strHostAddress.GetLength ()) 
		strTitle += _T (" [") + cmdInfo.m_strHostAddress + _T ("]"); 

	pMainFrame->SetTitle (strTitle);
	pMainFrame->UpdateWindow();

	if (!cmdInfo.m_strFileName.GetLength ()) {
		cmdInfo.m_strFileName = GetMRUFilename (0);
		cmdInfo.m_nShellCommand = CCommandLineInfo::FileOpen;
	}

	if (!TaskExists (cmdInfo.m_strFileName)) 
		cmdInfo.m_nShellCommand = CCommandLineInfo::FileNothing;

	#ifdef __WINPRINTER__
	BeginWaitCursor ();
	CheckFonts ();
	EndWaitCursor ();
	#endif //__WINPRINTER__

//	ProcessShellCommand (cmdInfo);
	m_strUser = cmdInfo.m_strUser;

	LoadDesktopPrinterDefaults ();

	VERIFY (m_hCallWndProcRet = ::SetWindowsHookEx (WH_CALLWNDPROCRET, (HOOKPROC)CallWndProcRet, ::GetModuleHandle (NULL), ::GetCurrentThreadId ()));
	TRACEF (::GetCommandLine ());

	ProcessShellCommand (cmdInfo);

	m_bInitialized = true;

	return TRUE;
}

int CMkDrawApp::ExitInstance() 
{
	using namespace FoxjetDatabase;

	FoxjetMessageBox::SetAfxMainWndValid (false);

	COdbcDatabase & db = GetDB ();
	const CString strSrc = FoxjetDatabase::Extract (db.GetConnect (), _T ("DBQ="), _T (";"));
	const CString strDSN = FoxjetDatabase::Extract (db.GetConnect (), _T ("DSN="), _T (";"));
	const bool bOpen = db.IsOpen () ? true : false;
	ULONG lPrinterID = GetPrinterID (db);
	CArray <PRINTERSTRUCT, PRINTERSTRUCT &> vPrinters;

	GetPrinterRecords (database, vPrinters);

	if (m_hCallWndProcRet) {
		UnhookWindowsHookEx (m_hCallWndProcRet);
		m_hCallWndProcRet = NULL;
	}

	if (bOpen) {

		VERIFY (FoxjetCommon::ExitInstance (GetElementListVersion ()));

		db.Close ();
	}

	if (!db.IsSqlServer ()) {
		bool bCompact = false;

		if (strSrc.GetLength ()) {
			if (!FoxjetDatabase::IsRunning (ISPRINTERRUNNING)) {
				if (!IsNetworked ())
					bCompact = true;
				else if (!IsControl (true)) // not started from Control.exe
					bCompact = true;
			}
		}

		if (bCompact) {
			bool bForce = false;

			#ifdef _DEBUG
			//bForce = true;
			#endif

			FoxjetDatabase::Compact (strSrc, strDSN, bForce);
		}
	}

	Encrypted::Free ();

	return CWinApp::ExitInstance ();
}

bool CMkDrawApp::TaskExists (const CString & strPath)
{
	CString strLine, strTask;

	if (CTaskDocTemplate::ParseTaskFromPath (strPath, strLine, strTask))
		return GetTaskID (GetDB (), strTask, strLine, GetMasterPrinterID (GetDB ())) != NOTFOUND;

	return false;
}

BOOL CMkDrawApp::ProcessShellCommand (CMkDrawCommandLineInfo & cmdInfo)
{
	if (cmdInfo.m_nShellCommand == CCommandLineInfo::FileNew)
		cmdInfo.m_nShellCommand = CCommandLineInfo::FileNothing;
	else if (cmdInfo.m_nShellCommand == CCommandLineInfo::FileOpen) {
		CArray <LINESTRUCT, LINESTRUCT &> vLines;
		const CString strLine = FoxjetFile::GetFileHead (cmdInfo.m_strFileName);
		const CString strTask = FoxjetFile::GetFileTitle (cmdInfo.m_strFileName);

		GetLineRecords (vLines);

		for (int i = 0; i < vLines.GetSize (); i++) {
			LINESTRUCT & line = vLines [i];

			if (!strLine.CompareNoCase (line.m_strName)) {
				// parse out working directory & insert GetRootPath ()
				CString str = FoxjetFile::GetRootPath () + strLine + _T ("\\") + strTask + _T (".") + FoxjetFile::GetFileExt (cmdInfo.m_strFileName);

				if (FoxjetFile::GetFileType (str) == FoxjetFile::UNKNOWNDOCTYPE)
					str += _T (".") + FoxjetFile::GetFileExt (FoxjetFile::TASK);

				cmdInfo.m_strFileName = str;

				break;
			}
		}
	}

	CWinApp::ProcessShellCommand (cmdInfo);

	TRACEF (_T ("CMkDrawApp::ProcessShellCommand"));

	if (cmdInfo.m_bReadOnly && cmdInfo.m_strFileName.GetLength ()) {
		CString strFind (cmdInfo.m_strFileName);
		
		strFind.MakeUpper ();

		for (POSITION posMgr = m_pDocManager->GetFirstDocTemplatePosition (); posMgr != NULL; ) {
			CDocTemplate * pTemplate = m_pDocManager->GetNextDocTemplate (posMgr);
			
			for (POSITION pos = pTemplate->GetFirstDocPosition (); pos != NULL; ) {
				if (CBaseDoc * pDoc = DYNAMIC_DOWNCAST (CBaseDoc, pTemplate->GetNextDoc (pos))) {
					CString str = pDoc->GetPathName ();

					str.MakeUpper ();

					if (str == strFind) {
						pDoc->SetReadOnly (true);
						break;
					}
				}
			}
		}
	}

	TRACEF (_T ("CMkDrawApp::ProcessShellCommand"));

	return TRUE;
}

// App command to run the dialog
void CMkDrawApp::OnAppAbout()
{
	CString strTitle = 
		LoadString (IDS_ABOUT) + _T (" ") + 
		GetAppTitle () + _T (" ") + 
		LoadString (AFX_IDS_APP_TITLE);
	CStringArray vCmdLine;

	TokenizeCmdLine (m_strFlags, vCmdLine);

	for (int i = vCmdLine.GetSize () - 1; i >= 0; i--) {
		if (FindNoCase (_T ("/user="), vCmdLine [i]) != -1 ||
			FindNoCase (_T ("/pass="), vCmdLine [i]) != -1)
		{
			vCmdLine.RemoveAt (i);
		}
	}

	if (m_db.HasTable (Printers::m_lpszTable)) {
		PRINTERSTRUCT p;

		if (GetPrinterRecord (m_db, GetPrinterID (m_db), p)) 
			vCmdLine.InsertAt (0, p.m_strName + _T (", ") + p.m_strAddress + _T (" [") + ToString (p.m_lID) + _T ("]"));
	}

	vCmdLine.Add (_T ("/DBQ=\"") + Extract (m_db.GetConnect (), _T ("DBQ="), _T (";")) + _T ("\""));
	vCmdLine.Add (_T ("/LOCAL_BACKUP_DIR=\"") + GetHomeDir () + _T ("\\") + Extract (m_db.GetConnect (), _T ("DSN="), _T (";")) + _T ("\""));
	vCmdLine.Add (_T ("/SELECT=\"") + LoadString (IDS_LOCAL_BACKUP) + _T ("\""));

	Foxjet3d::CDocumentationPage dlgDoc;
	FoxjetCommon::CAboutDlg dlg (theApp.GetVersion (), vCmdLine, strTitle);

	dlg.m_strApp = GetAppTitle ();
	
#ifdef __IPPRINTER__
	if (verApp >= CVersion (1, 30))
#endif 
		dlg.AddPage (&dlgDoc);
	
	FoxjetUtils::NetStatDlg::CNetStatDlg dlgNetStat (defElements.m_strRegSection);
	
	if (theApp.ShowNetStatDlg ()) 
		dlg.AddPage (&dlgNetStat);

	dlg.DoModal ();
}

/////////////////////////////////////////////////////////////////////////////
// CMkDrawApp message handlers


CString CMkDrawApp::GetUsername () const 
{
	return FoxjetDatabase::GetUsername ();
}

void CMkDrawApp::SetStatusText(LPCTSTR psz, int nPane)
{
	CMainFrame & wnd = * (CMainFrame *)m_pMainWnd;
	wnd.m_wndStatusBar.SetPaneText (nPane, psz);
}


CString CMkDrawApp::GetStatusText(int nPane)
{
	CMainFrame & wnd = * (CMainFrame *)m_pMainWnd;
	return wnd.m_wndStatusBar.GetPaneText (nPane);
}

void CMkDrawApp::BeginProgressBar (const CString & str, int nMax)
{
	ASSERT (nMax != 0);
	m_nProgressMax = nMax;
	m_nProgress = 0;
}

void CMkDrawApp::StepProgressBar (const CString & str)
{
	if (CMainFrame * pMain = DYNAMIC_DOWNCAST (CMainFrame, m_pMainWnd)) {
		CStatusBar & bar = pMain->m_wndStatusBar;
		CRect rc, rcBar;
		CDC * pDC = bar.GetDC ();

		if (!pDC)
			return;

		if (m_nProgress < m_nProgressMax)
			m_nProgress++;

		ASSERT (m_nProgressMax != 0);

		bar.GetItemRect (0, &rc);
		rcBar = rc;
		
		CFont * pFont = pDC->SelectObject (bar.GetFont ());
		pDC->FillSolidRect (rc, ::GetSysColor (COLOR_ACTIVEBORDER));
		pDC->DrawText (str, rc, 0);

		rcBar.right = rcBar.left + (int)((double)rcBar.Width () * ((double)m_nProgress / (double)m_nProgressMax));
		pDC->FillSolidRect (rcBar, ::GetSysColor (COLOR_HIGHLIGHT));
		COLORREF rgbTextColor = pDC->SetTextColor (Color::rgbWhite);
		pDC->DrawText (str, rcBar, 0);
		pDC->SetTextColor (rgbTextColor);
		pDC->SelectObject (pFont);
	}
}

void CMkDrawApp::EndProgressBar ()
{
	if (CMainFrame * pMain = DYNAMIC_DOWNCAST (CMainFrame, m_pMainWnd)) {
		CStatusBar & bar = pMain->m_wndStatusBar;

		bar.Invalidate ();
		bar.RedrawWindow ();
	}
}

CMDIChildWnd * CMkDrawApp::GetActiveFrame () const
{
	CMDIChildWnd * pResult = NULL;
	CWinApp * pApp = ::AfxGetApp();
	ASSERT (pApp);
	
	if (pApp->m_pMainWnd) {
		ASSERT_KINDOF (CMDIFrameWnd, pApp->m_pMainWnd);
		CMDIFrameWnd * pMainWnd = (CMDIFrameWnd *)pApp->m_pMainWnd;
		CFrameWnd * pFrame = pMainWnd->GetActiveFrame ();

		pResult = DYNAMIC_DOWNCAST (CMDIChildWnd, pFrame);
	}

	return pResult;
}

CView * CMkDrawApp::GetActiveView() const
{
	CView * pView = NULL;

	if (CMDIChildWnd * pFrame = GetActiveFrame ()) 
		pView = (CBaseView *)pFrame->GetActiveView();

	return pView;
}

CDocument * CMkDrawApp::GetActiveDocument() const
{
	CView * p = GetActiveView ();
	
	if (p) {
		ASSERT (p->GetDocument ());
		return p->GetDocument ();
	}
	else
		return NULL;
}

BOOL CMkDrawApp::OnIdle(LONG lCount) 
{
	if (CBaseDoc * pDoc = DYNAMIC_DOWNCAST (CBaseDoc, GetActiveDocument ())) 
		pDoc->OnUpdateDocView ();

	return CWinApp::OnIdle(lCount);
}

BOOL CMkDrawApp::PreTranslateMessage(MSG* pMsg)
{
	// CG: The following lines were added by the Splash Screen component.
	if (CSplashWnd::PreTranslateAppMessage(pMsg))
		return TRUE;

	return CWinApp::PreTranslateMessage(pMsg);
}

BOOL CMkDrawApp::OnDDECommand(LPTSTR lpszCommand) 
{
	return CWinApp::OnDDECommand(lpszCommand);
}

bool CMkDrawApp::IsConfiningOn() const
{
	return FoxjetCommon::IsConfiningOn ();
}

bool CMkDrawApp::SetConfiningOn(bool bOn)
{
	return FoxjetCommon::SetConfiningOn (bOn);
}

void CMkDrawApp::OnUpdateFileOpen(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable (true);
}

void CMkDrawApp::AddToRecentFileList (LPCTSTR lpszPathName)
{
//	CWinApp::AddToRecentFileList (lpszPathName);
	
	if (m_pRecentFileList)
		m_pRecentFileList->Add (lpszPathName);

	UpdateRecentFileList ();
}

void CMkDrawApp::UpdateRecentFileList()
{
	using namespace FoxjetFile;

	if (m_pRecentFileList) {
		CRecentFileList & list = * m_pRecentFileList;

		list.WriteList ();
	}
}

void CMkDrawApp::OnFileNew() 
{
	CWinApp::OnFileNew ();
}

FoxjetCommon::CVersion CMkDrawApp::GetVersion() const
{
//	void CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer)
	return CVersion (FoxjetCommon::verApp);
}


CString CMkDrawApp::GetDdeAppName()
{
	return LoadString (IDR_MAINFRAME);
}

const CMkDrawDocManager & CMkDrawApp::GetDocManager() const
{
	ASSERT_KINDOF (CMkDrawDocManager, m_pDocManager);
	return * (CMkDrawDocManager *)m_pDocManager;
}


CString CMkDrawApp::GetMRUFilename(int nIndex) const
{
	CString str;

	if (theApp.m_pRecentFileList) {
		CRecentFileList & list = * m_pRecentFileList;

		if (nIndex >= 0 && nIndex < list.GetSize ()) 
			str = list [nIndex];
	}

	return str;
}

CFontBar & CMkDrawApp::GetFontBar() const
{
	CMainFrame & wnd = * (CMainFrame *)m_pMainWnd;
	return wnd.m_wndFontBar;
}

CHeadBar & CMkDrawApp::GetHeadBar() const
{
	CMainFrame & wnd = * (CMainFrame *)m_pMainWnd;
	return wnd.m_wndHeadBar;
}

bool CMkDrawApp::IsTaskOpen (ULONG lTaskID) const
{
	using namespace TaskDoc;

	const CMkDrawDocManager & manager = theApp.GetDocManager ();

	for (POSITION pos = manager.GetFirstDocTemplatePosition (); pos;  ) {
		CDocTemplate * pTemplate = manager.GetNextDocTemplate (pos);

		for (POSITION posDoc = pTemplate->GetFirstDocPosition (); posDoc; ) {
			if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, pTemplate->GetNextDoc (posDoc))) {
				if (pDoc->m_task->m_lID == lTaskID)
					return true;
			}
		}
	}

	return false;
}

CString CMkDrawApp::GetUser () const
{
	return m_strUser;
}


void CMkDrawApp::OnDebuggingstuffUpgradetasks() 
{
	using namespace FoxjetDatabase;

	BeginWaitCursor ();

	CMkDrawDocManager & manager = * (CMkDrawDocManager *)m_pDocManager;
	COdbcDatabase & db = GetDB ();
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	int nTotal = 0;
	CString str;
	const CString strVer = theApp.GetVersion ().ToString ();
	CStringArray vDocs;

	CloseAllDocuments (FALSE);
	::Sleep (0);

	GetPrinterLines (db, GetPrinterID (db), vLines); //GetLineRecords (db, vLines);

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		const LINESTRUCT line = vLines [nLine];
		CArray <TASKSTRUCT, TASKSTRUCT &> vTasks;

		try
		{
			CString strSQL;
			COdbcRecordset rst (&db);

			strSQL.Format (
				_T ("SELECT Messages.Name, Messages.Data ")
				_T ("FROM Tasks INNER JOIN Messages ON Tasks.ID = Messages.TaskID ")
				_T ("WHERE Tasks.LineID=%d ")
				_T ("ORDER BY Messages.Name; "),
				line.m_lID);
			TRACEF (strSQL);

			rst.Open (strSQL);

			while (!rst.IsEOF ()) {
				const CString strData = rst.GetFieldValue (_T ("Data"));

				if (strData.Find (strVer) == -1) {
					const CString strName = rst.GetFieldValue (_T ("Name"));
					const CString strDoc = 
						FoxjetFile::GetRootPath () +
						line.m_strName + 
						_T ("\\") + 
						strName +
						_T (".") + 
						FoxjetFile::GetFileExt (FoxjetFile::TASK);

					TRACEF (strDoc);
					vDocs.Add (strDoc);
				}

				rst.MoveNext ();
			}

			rst.Close ();
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		if (vDocs.GetSize ()) {
			str.Format (_T ("line %s: %d tasks remain"), line.m_strName, vDocs.GetSize ());
			TRACEF (str);
		}

		for (int nDoc = 0; nDoc < vDocs.GetSize (); nDoc++) {
			CString strDoc = vDocs [nDoc];

			TRACEF (strDoc);

			if (manager.OpenDocumentFile (strDoc, false)) {
				CBaseDoc * pDoc = DYNAMIC_DOWNCAST (CBaseDoc, GetActiveDocument ());

				ASSERT (pDoc);
				VERIFY (pDoc->Save ());
				CloseAllDocuments (FALSE);
				nTotal++;
				//::Sleep ((((nTotal) % 25) == 0) ? 3000 : 0);
			}
		}

		TRACEF (_T (""));
	}

	str.Format (_T ("%d tasks saved"), nTotal);
	TRACEF (str);

	EndWaitCursor ();
}

class CPrintDlg : public Foxjet3d::OpenDlg::COpenDlg
{
// Construction
public:
	CPrintDlg (CWnd* pParent = NULL) : Foxjet3d::OpenDlg::COpenDlg (pParent) { }

protected:
	virtual BOOL OnInitDialog() 
	{
		UINT n [] = 
		{
			CHK_READONLY,
			//CHK_PREVIEW,
		};

		Foxjet3d::OpenDlg::COpenDlg::OnInitDialog ();

		for (int i = 0; i < ARRAYSIZE (n); i++)
			if (CWnd * p = GetDlgItem (n [i]))
				p->ShowWindow (SW_HIDE);

		SetWindowText (LoadString (IDS_BATCHPRINT));

		return FALSE;
	}
};

#ifdef _DEBUG
void CMkDrawApp::OnFileSave10000 ()
{
	TaskDoc::CTaskDoc * pDoc = (TaskDoc::CTaskDoc *)theApp.GetActiveDocument ();
	LINESTRUCT line;
	const CString strName = pDoc->m_task.m_strName;

	GetLineRecord (theApp.m_db, pDoc->m_task.m_lLineID, line);

	//for (int i = 1; i <= 100000; i++) {
	for (int i = 9999; i <= 100000; i++) {
		
		TASKSTRUCT task = pDoc->m_task;

		task.m_strName.Format (_T ("%s_%06d"), strName, i);

		for (int nMsg = 0; nMsg < task.m_vMsgs.GetSize (); nMsg++) {
			MESSAGESTRUCT msg = task.m_vMsgs [nMsg];

			msg.m_strName = task.m_strName;
			task.m_vMsgs.SetAt (nMsg, msg);
		}

		DeleteTaskRecord (theApp.m_db, GetTaskID (theApp.m_db, task.m_strName, line.m_strName, GetPrinterID (theApp.m_db)));
		VERIFY (AddTaskRecord (theApp.m_db, task));
		TRACEF (task.m_strName);
	}
}
#endif

typedef struct
{
	TASKSTRUCT m_task;
	CArray <CEditorElementList *, CEditorElementList *> m_vLists;
} BATCHPRINTSTRUCT;

void CMkDrawApp::OnFileBatchPrint ()
{
	using namespace Foxjet3d;

	#ifdef __WINPRINTER__
	using namespace FoxjetElements;
	#endif //__WINPRINTER__

	COdbcDatabase & db = theApp.GetDB ();
	const CString strSection = _T ("Settings\\LastOpen");
	CPrintDlg dlgPrint (::AfxGetMainWnd ());
	CArray <BATCHPRINTSTRUCT *, BATCHPRINTSTRUCT *> vLists;
	CMapStringToString mapUser;

	int nPanel		= GetProfileInt (strSection, _T ("nPanel"), 1);
	dlgPrint.m_nPanel	= nPanel - 1;
	dlgPrint.m_bPreview	= GetProfileInt (strSection, _T ("bPreview"), FALSE);
	dlgPrint.m_lLineID	= GetProfileInt (strSection, _T ("LineID"), -1);

	if (dlgPrint.DoModal () == IDOK) {
		int nFiles = dlgPrint.m_vFiles.GetSize ();
		const CString strMsg = LoadString (IDS_OPENING);
		UserElementDlg::CUserElementDlg dlgUser (dlgPrint.m_lLineID, ::AfxGetMainWnd ());

		BeginWaitCursor ();
		theApp.BeginProgressBar (strMsg, nFiles);

		for (int i = 0; i < nFiles; i++) {
			const CString strFile = FoxjetFile::GetRootPath () + dlgPrint.m_vFiles [i];
			TASKSTRUCT task;

			theApp.StepProgressBar (strMsg + _T (": ") + strFile + _T ("..."));

			const CString strName = FoxjetFile::GetRelativeName (strFile);
			CString strLine, strTask;

			CTaskDocTemplate::ParseTaskFromPath (strFile, strLine, strTask);

			ULONG lLineID = GetLineID (db, strLine, GetPrinterID (db));
			
			if (FoxjetDatabase::GetTaskRecord (db, strTask, lLineID, task)) {
				BATCHPRINTSTRUCT * pBatch = new BATCHPRINTSTRUCT;

				pBatch->m_task = task;

				for (int i = 0; i < task.m_vMsgs.GetSize (); i++) {
					CEditorElementList * pList = new CEditorElementList ();
					HEADSTRUCT head;

					pBatch->m_vLists.Add (pList);
					VERIFY (GetHeadRecord (db, task.m_vMsgs [i].m_lHeadID, head));

					TRACEF (task.m_vMsgs [i].m_strData);
					pList->FromString (task.m_vMsgs [i].m_strData, CProductArea (), INCHES, head, CVersion ());
					pList->SetHead (head.m_lID);

					CTaskView::GetUserElements (* pList, dlgUser.m_vElements, head);
				}

				vLists.Add (pBatch);
			}
		}

		if (dlgUser.m_vElements.GetSize ()) {
			if (dlgUser.DoModal () == IDOK) {
				for (int i = 0; i < dlgUser.m_vElements.GetSize (); i++) {
					Foxjet3d::UserElementDlg::CUserElementArray vPrompt;
					Foxjet3d::UserElementDlg::CUserElementDlg::Get (dlgUser.m_vElements [i].m_pElement, dlgUser.m_vElements [i].m_head, vPrompt, true);

					for (int j = 0; j < vPrompt.GetSize (); j++) {
						TRACEF (vPrompt [j].m_strPrompt + _T (" --> ") + vPrompt [j].m_strData);
						mapUser.SetAt (vPrompt [j].m_strPrompt, vPrompt [j].m_strData);
					}

					/* TODO: rem
					UserElementDlg::CUserItem & item = dlgUser.m_vElements [i];
					CString strKey = dlgUser.m_vElements [i].m_strPrompt;
					CString strData;

					#ifdef __WINPRINTER__
					if (CUserElement * pUser = DYNAMIC_DOWNCAST (CUserElement, item.m_pElement)) 
						strData = pUser->GetDefaultData ();
					if (CDatabaseElement * pUser = DYNAMIC_DOWNCAST (CDatabaseElement, item.m_pElement)) 
						strData = pUser->GetKeyValue ();
					if (CBitmapElement * pUser = DYNAMIC_DOWNCAST (CBitmapElement, item.m_pElement)) 
						strData = pUser->GetFilePath ();
					#endif //__WINPRINTER__

					//TRACEF (strKey + _T (" --> ") + strData);
					mapUser.SetAt (strKey, strData);
					*/
				}
			}
		}

		for (POSITION pos = mapUser.GetStartPosition (); pos; ) {
			CString strKey, strData;

			mapUser.GetNextAssoc (pos, strKey, strData);
			TRACEF (strKey + _T (" --> ") + strData);
		}

		if (vLists.GetSize ()) {
			CFont fnt;
			CPrintDialog dlg (FALSE);

			theApp.GetPrinterDeviceDefaults (&dlg.m_pd);

			if (theApp.DoPrintDialog (&dlg) == IDOK) {
				HDC hDC = dlg.CreatePrinterDC ();
				ASSERT (hDC);

				CDC & dc = * CDC::FromHandle (hDC);
				CDC dcMem;
				DOCINFO doc;
				const CRect rcPage (
					CPoint (::GetDeviceCaps (hDC, PHYSICALOFFSETX), ::GetDeviceCaps (hDC, PHYSICALOFFSETY)), 
					CSize (::GetDeviceCaps (hDC, HORZRES), ::GetDeviceCaps (hDC, VERTRES)));

				const int nFontSize = ::GetDeviceCaps (hDC, LOGPIXELSY) / 8;
				fnt.CreateFont (nFontSize, 0, 0, 0, FW_BOLD, FALSE, 0, 0,
					DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
					DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("Verdana"));

				VERIFY (dcMem.CreateCompatibleDC (&dc));

				memset (&doc, 0, sizeof (doc));
				doc.cbSize			= sizeof (doc);
				doc.lpszDocName		= vLists [0]->m_task.m_strName;
				
				VERIFY (dc.StartDoc (&doc) > 0);
				
				for (int i = 0; i < vLists.GetSize (); i++) {
					BATCHPRINTSTRUCT * pBatch = vLists [i];
					CBitmap bmp;
					CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;
					LINESTRUCT line;

					GetLineRecord (db, pBatch->m_task.m_lLineID, line);
					GetPanelRecords (db, pBatch->m_task.m_lLineID, vPanels);

					VERIFY (bmp.CreateCompatibleBitmap (&dcMem, rcPage.Width (), rcPage.Height ()));
					CBitmap * pBitmap = dcMem.SelectObject (&bmp);

					TRACEF (pBatch->m_task.m_strName);
					UINT nFormat = DT_NOPREFIX | DT_CENTER;
					CString str;

					OpenDlg::CTaskPreview preview (pBatch->m_task, dcMem, rcPage, mapUser);

					for (int j = 0; j < ARRAYSIZE (preview.m_bmp); j++) {
						CSize size = preview.m_bmp [j].GetSize ();

						if (size.cx > 0 && size.cy > 0) {
							CRect rc (rcPage);
							BITMAP bm;
							CBitmap bmp;
							CString strTask		= line.m_strName + _T ("\\") +  pBatch->m_task.m_strName; 
							CString strPanel	= vPanels [j].m_strName;

							preview.m_bmp [j].GetBitmap (bmp);
							ZeroMemory (&bm, sizeof (bm));
							bmp.GetObject (sizeof (bm), &bm);

							ULONG lTotal = bm.bmWidth * bm.bmHeight;
							ULONG lPixels = lTotal - CountPixels (bmp);
							
							TRACEF (strTask + _T ("\\") + strPanel + _T (": ") + ToString (lPixels) + _T (" pixels"));

							if (lPixels > 0) {

								rc.top += nFontSize;

								double d [2] = { (double)rc.Width () / (double)bm.bmWidth, (double)rc.Height () / (double)bm.bmHeight };
								double dZoom = min (d [0], d [1]);
								int x = rc.left;
								int y = rc.top;
								int cx = (int)(dZoom * (double)bm.bmWidth);
								int cy = (int)(dZoom * (double)bm.bmHeight);

								VERIFY (dc.StartPage () > 0);
								
								CBitmap * pBitmap = dcMem.SelectObject (&bmp);
								CFont * pFont = dcMem.SelectObject (&fnt);

								dc.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, bm.bmWidth, bm.bmHeight, SRCCOPY);
								//dc.FrameRect (CRect (rcPage), &CBrush (Color::rgbDarkGray));
								dc.SetTextColor (Color::rgbBlack);
								
								{
									HDC hdcScreen = ::GetDC (NULL);
									double dScreen = (double)dc.GetDeviceCaps (HORZRES) / (double)dc.GetDeviceCaps (LOGPIXELSX);
									double dPrint = (double)bm.bmWidth / ::GetDeviceCaps (hdcScreen, LOGPIXELSX);
									double d = (dScreen / dPrint) * 100.0;
									::ReleaseDC (NULL, hdcScreen);

									//dc.DrawText (str, CRect (rcPage), nFormat);
									str.Format (_T ("%s, %s [%.02f %%]"), strTask, strPanel, d);
									dc.TextOut (rcPage.left, rcPage.top, str);

									str = pBatch->m_vLists [i]->GetVersion ().Format (); 
									CSize size = dc.GetTextExtent (str);
									dc.TextOut (rcPage.left + ((rcPage.Width () - size.cx) / 2), rcPage.top, str);

									str = CTime::GetCurrentTime ().Format (_T ("%c"));
									size = dc.GetTextExtent (str);
									dc.TextOut (rcPage.left + (rcPage.Width () - size.cx), rcPage.top, str);
								}

								dcMem.SelectObject (pFont);
								dcMem.SelectObject (pBitmap);
								
								SAVEBITMAP (bmp, _T ("C:\\Temp\\Debug\\") + strPanel + _T (".bmp"));

								dc.EndPage ();
							}
						}
					}


					/* TODO: rem
					for (int j = 0; j < pBatch->m_vLists.GetSize (); j++) {
						CRawBitmap * pbmp = NULL;
						ULONG lHeadID = pBatch->m_vLists [j]->GetHead ().m_lID;

						str.Format (_T ("%s [%s]"), pBatch->m_task.m_strName, pBatch->m_vLists [j]->GetHead ().m_strName);
						ASSERT (preview.m_map.Lookup (lHeadID, pbmp));

						if (pbmp) {
							CBitmap bmp;
							CRect rc (rcPage);
							BITMAP bm;

							pbmp->GetBitmap (bmp);
							ZeroMemory (&bm, sizeof (bm));
							bmp.GetObject (sizeof (bm), &bm);

							rc.top += nFontSize;

							double d [2] = { (double)rc.Width () / (double)bm.bmWidth, (double)rc.Height () / (double)bm.bmHeight };
							double dZoom = min (d [0], d [1]);
							int x = rc.left;
							int y = rc.top;
							int cx = (int)(dZoom * (double)bm.bmWidth);
							int cy = (int)(dZoom * (double)bm.bmHeight);

							VERIFY (dc.StartPage () > 0);
							
							//SAVEBITMAP (bmp, _T ("C:\\Temp\\Debug\\") + str + _T (".bmp"));
							CBitmap * pBitmap = dcMem.SelectObject (&bmp);
							CFont * pFont = dcMem.SelectObject (&fnt);

							dc.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, bm.bmWidth, bm.bmHeight, SRCCOPY);
							dc.SetTextColor (Color::rgbBlack);
							
							{
								HDC hdcScreen = ::GetDC (NULL);
								double dScreen = (double)dc.GetDeviceCaps (HORZRES) / (double)dc.GetDeviceCaps (LOGPIXELSX);
								double dPrint = (double)bm.bmWidth / ::GetDeviceCaps (hdcScreen, LOGPIXELSX);
								double d = (dScreen / dPrint) * 100.0;
								::ReleaseDC (NULL, hdcScreen);

								CString strTask		= /* TODO: line name + _T ("\\") + * / pBatch->m_task.m_strName; 
								CString strPanel	= _T (""); // TODO // doc.GetTitle ();

								//dc.DrawText (str, CRect (rcPage), nFormat);
								str.Format (_T ("%s, %s [%.02f %%]"), strTask, strPanel, d);
								dc.TextOut (rcPage.left, rcPage.top, str);

								str = pBatch->m_vLists [i]->GetVersion ().Format (); 
								CSize size = dc.GetTextExtent (str);
								dc.TextOut (rcPage.left + ((rcPage.Width () - size.cx) / 2), rcPage.top, str);

								str = CTime::GetCurrentTime ().Format (_T ("%c"));
								size = dc.GetTextExtent (str);
								dc.TextOut (rcPage.left + (rcPage.Width () - size.cx), rcPage.top, str);
							}

							dcMem.SelectObject (pFont);
							dcMem.SelectObject (pBitmap);
							
							dc.EndPage ();
						}
					}
					*/

					dcMem.SelectObject (pBitmap);
				}

				dc.EndDoc ();
			}
		}
	}

	{
		for (int i = 0; i < vLists.GetSize (); i++) {
			BATCHPRINTSTRUCT * pBatch = vLists [i];

			for (int j = 0; j < pBatch->m_vLists.GetSize (); j++)
				if (CEditorElementList * p = pBatch->m_vLists [j])
					delete p;

			delete pBatch;
		}

		vLists.RemoveAll ();
	}

	theApp.EndProgressBar ();
	EndWaitCursor ();
}

void CMkDrawApp::OnFilePrintSetup ()
{
	const CString strSection = defElements.m_strRegSection;
	CPrintDialog pd (TRUE);

	if (DoPrintDialog (&pd) == IDOK) {
		if (pd.m_pd.hDevMode) {
			if (LPVOID pSrc = ::GlobalLock (pd.m_pd.hDevMode)) {
				HKEY hKey;

				if (::RegCreateKey (HKEY_CURRENT_USER, strSection, &hKey) == ERROR_SUCCESS) {
					DWORD dwSize = ::GlobalSize (pd.m_pd.hDevMode);

					LONG lStatus = ::RegSetValueEx (hKey, _T ("hDevMode"), 0, REG_BINARY, 
						(const BYTE *)pSrc, dwSize);
					::RegCloseKey (hKey);
					ASSERT (lStatus == ERROR_SUCCESS);
				}

				if (m_hDevMode) {
					LPVOID pDest = ::GlobalLock (m_hDevMode);

					if (pDest && pSrc) {
						DWORD dwSize = ::GlobalSize (m_hDevMode);
						
						memcpy (pSrc, pDest, dwSize);
					}

					::GlobalUnlock (m_hDevMode);
				}

				::GlobalUnlock (pd.m_pd.hDevMode);
			}
		}

		if (pd.m_pd.hDevNames) {
			if (LPVOID pSrc = ::GlobalLock (pd.m_pd.hDevNames)) {
				HKEY hKey;

				if (::RegCreateKey (HKEY_CURRENT_USER, strSection, &hKey) == ERROR_SUCCESS) {
					DWORD dwSize = ::GlobalSize (pd.m_pd.hDevNames);

					LONG lStatus = ::RegSetValueEx (hKey, _T ("hDevNames"), 0, REG_BINARY, 
						(const BYTE *)pSrc, dwSize);
					::RegCloseKey (hKey);
					ASSERT (lStatus == ERROR_SUCCESS);
				}

				::GlobalUnlock (pd.m_pd.hDevNames);
			}
		}
	}
}

void CMkDrawApp::LoadDesktopPrinterDefaults ()
{
	const CString strSection = defElements.m_strRegSection;
	
	{
		HKEY hKey;

		if (::RegCreateKey (HKEY_CURRENT_USER, strSection, &hKey) == ERROR_SUCCESS) {
			DWORD dwMaxValueLen = 2048, dwType = REG_BINARY;

			if (::RegQueryInfoKey (hKey, NULL, NULL, NULL, NULL, NULL, 
				NULL, NULL, NULL, &dwMaxValueLen, NULL, NULL) != ERROR_SUCCESS)
			{
				dwMaxValueLen = 2048;
			}

			m_hDevMode = ::GlobalAlloc (GHND, dwMaxValueLen);
			LPVOID p = ::GlobalLock (m_hDevMode);

			if (::RegQueryValueEx (hKey, _T ("hDevMode"), NULL, &dwType, 
				(LPBYTE)p, &dwMaxValueLen) != ERROR_SUCCESS)
			{
				::GlobalFree (m_hDevMode);
				m_hDevMode = NULL;
			}

			BOOL bGlobalUnlock = ::GlobalUnlock (m_hDevMode);
			::RegCloseKey (hKey);
		}
	}

	{
		HKEY hKey;

		if (::RegCreateKey (HKEY_CURRENT_USER, strSection, &hKey) == ERROR_SUCCESS) {
			DWORD dwMaxValueLen = 2048, dwType = REG_BINARY;

			if (::RegQueryInfoKey (hKey, NULL, NULL, NULL, NULL, NULL, 
				NULL, NULL, NULL, &dwMaxValueLen, NULL, NULL) != ERROR_SUCCESS)
			{
				dwMaxValueLen = 2048;
			}

			m_hDevNames = ::GlobalAlloc (GHND, dwMaxValueLen);
			LPVOID p = ::GlobalLock (m_hDevNames);

			if (::RegQueryValueEx (hKey, _T ("hDevNames"), NULL, &dwType, 
				(LPBYTE)p, &dwMaxValueLen) != ERROR_SUCCESS)
			{
				::GlobalFree (m_hDevNames);
				m_hDevNames = NULL;
			}

			BOOL bGlobalUnlock = ::GlobalUnlock (m_hDevNames);
			::RegCloseKey (hKey);
		}
	}
}

long CALLBACK CMkDrawApp::CallWndProcRet (int nCode, WPARAM wParam, LPARAM lParam)
{
	if (CWPRETSTRUCT * p = (CWPRETSTRUCT *)lParam) {
		switch (p->message) {
		case WM_INITDIALOG:
			FoxjetCommon::SetAutoButtonText (p->hwnd);
			break;
		case WM_COMMAND:
			UINT nMsg = HIWORD (p->wParam);
			UINT nID = LOWORD (p->wParam);

			switch (nID) {
			case IDOK:
			case IDCANCEL:
			case IDABORT:
			case IDRETRY:
			case IDIGNORE:
			case IDYES:
			case IDNO:
			case IDCLOSE:
			case IDHELP:
				break;
			default:
				switch (nMsg) {
				case BN_CLICKED:
					FoxjetCommon::SetAutoButtonText (p->hwnd);
					break;
				}
			}
		}
	}

	return ::CallNextHookEx (theApp.m_hCallWndProcRet, nCode, wParam, lParam);
}

void CMkDrawApp::OnFileChangedsn() 
{
#ifdef __WINPRINTER__
	using namespace FoxjetElements;
	using namespace TaskDoc;

	CChangeDsnDlg dlg (::AfxGetMainWnd ());

	dlg.m_strFrom = theApp.GetProfileString (_T ("Settings\\CChangeDsnDlg"), _T ("from"));
	dlg.m_strTo = theApp.GetProfileString (_T ("Settings\\CChangeDsnDlg"), _T ("to"));

	if (dlg.DoModal () == IDOK) {
		using namespace FoxjetDatabase;
		
		theApp.WriteProfileString (_T ("Settings\\CChangeDsnDlg"), _T ("from"), dlg.m_strFrom);
		theApp.WriteProfileString (_T ("Settings\\CChangeDsnDlg"), _T ("to"), dlg.m_strTo);

		BeginWaitCursor ();

		CMkDrawDocManager & manager = * (CMkDrawDocManager *)m_pDocManager;
		COdbcDatabase & db = GetDB ();
//		CArray <LINESTRUCT, LINESTRUCT &> vLines;
		int nTotal = 0;
		CString str;
		const CString strVer = theApp.GetVersion ().ToString ();
		CStringArray vDocs;

		CloseAllDocuments (FALSE);
		::Sleep (0);

		CArray <TASKSTRUCT, TASKSTRUCT &> vTasks;
		LINESTRUCT line;

		GetTaskRecords (db, FoxjetDatabase::ALL, vTasks);
		
		for (int i = 0; i < vTasks.GetSize (); i++) {
			LINESTRUCT line;

			GetLineRecord (db, vTasks [i].m_lLineID, line);

			const CString strName = vTasks [i].m_strName;
			const CString strDoc = 
				FoxjetFile::GetRootPath () +
				line.m_strName + 
				_T ("\\") + 
				strName +
				_T (".") + 
				FoxjetFile::GetFileExt (FoxjetFile::TASK);

			TRACEF (strDoc);
			vDocs.Add (strDoc);
		}

		if (vDocs.GetSize ()) {
			str.Format (_T ("line %s: %d tasks remain"), line.m_strName, vDocs.GetSize ());
			TRACEF (str);
		}

		for (int nDoc = 0; nDoc < vDocs.GetSize (); nDoc++) {
			CString strDoc = vDocs [nDoc];

			TRACEF (strDoc);

			if (manager.OpenDocumentFile (strDoc, false)) {
				if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, GetActiveDocument ())) {
					int nSet = 0;

					for (int i = 0; i < pDoc->m_task.GetMessageCount (); i++) {
						MESSAGESTRUCT msg = * pDoc->m_task.GetMessageStruct (i);
						CEditorElementList & list = pDoc->m_task.GetMessage (i)->m_list;

						for (int nElement = 0; nElement < list.GetSize (); nElement++) {
							if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, list [nElement].GetElement ())) {
								if (!p->GetDSN ().CompareNoCase (dlg.m_strFrom)) {
									p->SetDSN (dlg.m_strTo);
									nSet++;
								}
							}
						}
					}

					if (nSet) {
						VERIFY (pDoc->Save ());
						nTotal++;
					}

					CloseAllDocuments (FALSE);
					::Sleep (1);
				}
			}
		}


		str.Format (LoadString (IDS_TASKSUPDATED), nTotal);
		TRACEF (str);
		MsgBox (str);

		EndWaitCursor ();
	}
#endif
}

void CMkDrawApp::OnAppClose() 
{
	if (CMainFrame * pMain = DYNAMIC_DOWNCAST (CMainFrame, m_pMainWnd)) 
		pMain->PostMessage (WM_CLOSE, 0, 0);
}

bool CMkDrawApp::IsLocalDB ()
{
	const CString strLocal = GetLocalDSN ();
	bool bResult = false;

	if (strLocal.GetLength ()) {
		CString str = Extract (m_db.GetConnect (), _T ("DSN="), _T (";"));

		bResult = str.CompareNoCase (strLocal) == 0;
	}

	return bResult;
}

void CMkDrawApp::DoDbIntegerityCheck ()
{
	DECLARETRACECOUNT (100);

	MARKTRACECOUNT ();
	FoxjetDatabase::COdbcDatabase & db = GetDB ();
	std::vector <std::wstring> v;
	CString str;

	defElements.SetOverlappedWarning (true);

	if (IsNetworked ()) {
		try
		{
			CString str;
			COdbcRecordset rst (db);

			str.Format (
				_T ("SELECT Count([%s]) AS Expr1, [%s] ")
				_T ("FROM [%s] ")
				_T ("GROUP BY [%s], [%s] ")
				_T ("HAVING (((Count([%s]))>1)) ")
				_T ("ORDER BY Count([%s]) DESC; "),
				Tasks::m_lpszName,
				Tasks::m_lpszName,
				Tasks::m_lpszTable,
				Tasks::m_lpszName,
				Tasks::m_lpszName,
				Tasks::m_lpszName,
				Tasks::m_lpszName);
			TRACEF (str);

			for (rst.Open (str); !rst.IsEOF (); rst.MoveNext ()) {
				//COdbcRecordset rstTask (db);

				TRACEF ((CString)rst.GetFieldValue ((short)0) + _T (": ") + (CString)rst.GetFieldValue ((short)1));

				v.push_back ((LPCTSTR)(CString)rst.GetFieldValue ((short)1));
				//str.Format (_T ("SELECT Tasks.ID, Tasks.Name FROM Tasks WHERE (((Tasks.Name)='%s'));"), (CString)rst.GetFieldValue ((short)1));
				//rstTask.Open (str);

				//while (!rstTask.IsEOF ()) {
				//	ULONG lTaskID = (ULONG)(long)rstTask.GetFieldValue ((short)0);
				//	TASKSTRUCT task;

				//	TRACEF (_T ("\t") + (CString)rstTask.GetFieldValue ((short)0) + _T (": ") + (CString)rstTask.GetFieldValue ((short)1));
				//	
				//	rstTask.MoveNext ();
				//}

			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		try {
			if (v.size ()) {
				CString str = LoadString (IDS_DUPLICATETASKNAMES) + _T (":\n") + (CString)implode (v, std::wstring (_T ("\n"))).c_str ();

				TRACEF (str);

				MsgBox (str, MB_ICONEXCLAMATION, 0);
				
				/*
				str += _T ("\n\n") + LoadString (IDS_DELETEDUPLICATES);

				if (MsgBox (str, MB_ICONEXCLAMATION | MB_YESNO, 0) == IDYES) {
					for (int i = 0; i < v.size (); i++) {
						COdbcRecordset rst (db);

						str.Format (_T ("SELECT [%s], [%s] FROM [%s] WHERE [%s]='%s' ORDER BY [%s] DESC"), 
							Tasks::m_lpszID, 
							Tasks::m_lpszLineID, 
							Tasks::m_lpszTable,
							Tasks::m_lpszName, 
							v [i].c_str (),
							Tasks::m_lpszID);
						TRACEF (str);

						rst.Open (str); 

						{
							ULONG lTaskID = (ULONG)(long)rst.GetFieldValue ((short)0);
							ULONG lLineID = (ULONG)(long)rst.GetFieldValue ((short)1);
							CArray <LINESTRUCT, LINESTRUCT &> vLines;

							GetPrinterLines (db, GetMasterPrinterID (db), vLines);

							if (vLines.GetSize ()) {
								ULONG lMasterLineID = vLines [0].m_lID;

								str.Format (_T ("UPDATE [%s] SET [%s]=%d WHERE [%s]=%d"), 
									Tasks::m_lpszTable, 
									Tasks::m_lpszLineID,
									lMasterLineID,
									Tasks::m_lpszID,
									lTaskID);
								TRACEF (str);
								db.ExecuteSQL (str);
							}
						}

						rst.MoveNext ();

						for (; !rst.IsEOF (); rst.MoveNext ()) {
							ULONG lTaskID = (ULONG)(long)rst.GetFieldValue ((short)0);
							DeleteTaskRecord (db, lTaskID);
						}
					}
				}
				*/
			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		v.clear ();
	}

	try 
	{
		COdbcRecordset rst (db);

		str.Format (
			_T ("SELECT Count([%s]) AS CountOfHeadID, [%s], [%s] ")
			_T ("FROM [%s] ")
			_T ("GROUP BY [%s], [%s] ")
			_T ("HAVING (((Count([%s]))>1)) ")
			_T ("ORDER BY Count([%s]) DESC"),
			Messages::m_lpszHeadID,
			Messages::m_lpszHeadID,
			Messages::m_lpszTaskID,
			Messages::m_lpszTable,
			Messages::m_lpszHeadID,
			Messages::m_lpszTaskID,
			Messages::m_lpszHeadID,
			Messages::m_lpszHeadID);
		TRACEF (str);

		for (rst.Open (str); !rst.IsEOF (); rst.MoveNext ()) {
			COdbcRecordset rstTask (db);
			ULONG lTaskID = (ULONG)(long)rst.GetFieldValue ((short)2);

			TRACEF ((CString)rst.GetFieldValue ((short)0) + _T (": ") + (CString)rst.GetFieldValue ((short)1) + _T (", ") + (CString)rst.GetFieldValue ((short)2));

			str.Format (_T ("SELECT [%s] FROM [%s] WHERE [%s]=%d"), 
				Tasks::m_lpszName, Tasks::m_lpszTable, Tasks::m_lpszID, 
				lTaskID);
			TRACEF (str);
			rstTask.Open (str);

			if (!rstTask.IsEOF ()) {
				std::wstring s = (LPCTSTR)(CString)rstTask.GetFieldValue ((short)0);

				if (Find (v, s) == -1)
					v.push_back (s);
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	try 
	{
		COdbcRecordset rst (db);
		CString str;

		/*
		valid head IDs:		SELECT Heads.ID FROM (Heads INNER JOIN Panels ON Heads.PanelID = Panels.ID) INNER JOIN Lines ON Panels.LineID = Lines.ID WHERE (((Lines.ID)=2));

		message records:	SELECT Messages.ID, Messages.HeadID FROM Messages WHERE (((Messages.TaskID)=712));

		SELECT Messages.ID, Messages.HeadID, Messages.TaskID
		FROM Messages
		WHERE (((Messages.TaskID)=712) AND (((([Messages].[TaskID])=712)) AND Messages.HeadID 
		Not In (SELECT Heads.ID FROM (Heads INNER JOIN Panels ON Heads.PanelID = Panels.ID) INNER JOIN Lines ON Panels.LineID = Lines.ID WHERE (((Lines.ID)=2)))));


		all heads, by line:		SELECT Heads.*, Lines.* FROM (Heads INNER JOIN Panels ON Heads.PanelID = Panels.ID) INNER JOIN Lines ON Panels.LineID = Lines.ID;

		SELECT column_name(s)
		FROM table1
		INNER JOIN table2 ON table1.column_name = table2.column_name;


		SELECT Messages.ID, Messages.HeadID, Tasks.ID, Tasks.Name, Tasks.LineID
		FROM Tasks INNER JOIN Messages ON Tasks.ID = Messages.TaskID
		WHERE (((Tasks.LineID)=2));

		SELECT Messages.ID, Messages.HeadID, Tasks.ID, Tasks.Name, Tasks.LineID
		FROM Tasks INNER JOIN Messages ON Tasks.ID = Messages.TaskID
		WHERE (((Tasks.LineID)=2)) AND NOT Messages.HeadID IN (SELECT Heads.ID FROM Heads)

		SELECT Messages.ID, Messages.HeadID, Tasks.ID, Tasks.Name, Tasks.LineID
		FROM Tasks INNER JOIN Messages ON Tasks.ID = Messages.TaskID
		WHERE (((Tasks.LineID)=2)) AND 
			NOT Messages.HeadID IN (SELECT Heads.ID FROM (Heads INNER JOIN Panels ON Heads.PanelID = Panels.ID) INNER JOIN Lines ON Panels.LineID = Lines.ID WHERE (((Lines.ID)=2)))

		SELECT Messages.ID, Messages.HeadID, Tasks.ID, Tasks.Name, Tasks.LineID
		FROM Tasks INNER JOIN Messages ON Tasks.ID = Messages.TaskID
		WHERE 
			NOT Messages.HeadID IN (SELECT Heads.ID FROM (Heads INNER JOIN Panels ON Heads.PanelID = Panels.ID) INNER JOIN Lines ON Panels.LineID = Lines.ID WHERE (((Lines.ID)=Tasks.LineID)))
		*/

		str.Format (
			_T ("SELECT Messages.ID, Messages.HeadID, Tasks.ID, Tasks.Name, Tasks.LineID ")
			_T ("FROM Tasks INNER JOIN Messages ON Tasks.ID = Messages.TaskID ")
			_T ("WHERE NOT Messages.HeadID IN (SELECT Heads.ID FROM (Heads INNER JOIN Panels ON Heads.PanelID = Panels.ID) ")
			_T ("INNER JOIN Lines ON Panels.LineID = Lines.ID WHERE (((Lines.ID)=Tasks.LineID)))"),		
			Messages::m_lpszTable, Messages::m_lpszID, 
			Messages::m_lpszTable, Messages::m_lpszHeadID, 
			Tasks::m_lpszTable, Tasks::m_lpszID, 
			Tasks::m_lpszTable, Tasks::m_lpszName, 
			Tasks::m_lpszTable, Tasks::m_lpszLineID,
			Tasks::m_lpszTable,
			Messages::m_lpszTable,
			Tasks::m_lpszTable, Tasks::m_lpszID,
			Messages::m_lpszTable, Messages::m_lpszTaskID,
			Messages::m_lpszTable, Messages::m_lpszHeadID,
			Heads::m_lpszTable, Heads::m_lpszID,
			Heads::m_lpszTable, 
			Panels::m_lpszTable, 
			Heads::m_lpszTable, Heads::m_lpszPanelID,
			Panels::m_lpszTable, Panels::m_lpszID,
			Lines::m_lpszTable,
			Panels::m_lpszTable, Panels::m_lpszLineID,
			Lines::m_lpszTable, Lines::m_lpszID,
			Lines::m_lpszTable, Lines::m_lpszID,
			Tasks::m_lpszTable, Tasks::m_lpszLineID);
		TRACEF (str);

		for (rst.Open (str); !rst.IsEOF (); rst.MoveNext ()) {
			std::wstring strTask = (LPCTSTR)(CString)rst.GetFieldValue ((short)3);

			TRACEF ((CString)rst.GetFieldValue ((short)0) 
				+ _T (": ") + (CString)rst.GetFieldValue ((short)1) 
				+ _T (", ") + (CString)rst.GetFieldValue ((short)2)
				+ _T (", ") + (CString)rst.GetFieldValue ((short)3));

			if (Find (v, strTask) == -1)
				v.push_back (strTask);
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	MARKTRACECOUNT ();
	TRACECOUNTARRAY ();

	if (v.size ()) {
		CString str = LoadString (IDS_DUPLICATEELEMENTS) + _T (":\n") + (CString)implode (v, std::wstring (_T ("\n"))).c_str ();

		TRACEF (str);
		MsgBox (str, MB_ICONEXCLAMATION, 0);
	}
}
