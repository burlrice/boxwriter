// ChangeDsnDlg.cpp : implementation file
//

#include "stdafx.h"

#ifdef __WINPRINTER__

#include "mkdraw.h"
#include "ChangeDsnDlg.h"
#include "Database.h"
#include "OdbcDatabase.h"
#include "extern.h"
#include "DatabaseElement.h"
#include "Parse.h"

using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace EditorGlobals;
using namespace FoxjetElements;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CChangeDsnDlg dialog


CChangeDsnDlg::CChangeDsnDlg(CWnd* pParent /*=NULL*/)
: FoxjetCommon::CEliteDlg(IDD_CHANGE_DSN, pParent)
{
	//{{AFX_DATA_INIT(CChangeDsnDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CChangeDsnDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChangeDsnDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChangeDsnDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CChangeDsnDlg)
	ON_BN_CLICKED(BTN_FIND, OnFind)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChangeDsnDlg message handlers

void CChangeDsnDlg::OnOK()
{
	GetDlgItemText (CB_FROM, m_strFrom);
	GetDlgItemText (CB_TO, m_strTo);
	FoxjetCommon::CEliteDlg::OnOK ();
}

BOOL CChangeDsnDlg::OnInitDialog() 
{
	CStringArray v;

	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	COdbcDatabase::GetDsnEntries (v);

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_TO)) {
		for (int i = 0; i < v.GetSize (); i++) {
			p->AddString (v [i]);
		}
	}

	SetDlgItemText (CB_FROM, m_strFrom);
	SetDlgItemText (CB_TO, m_strTo);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CChangeDsnDlg::OnFind() 
{
	COdbcDatabase & db = theApp.GetDB ();	
	CStringArray vDSN;

	BeginWaitCursor ();

	try 
	{
		EnumDSNs (vDSN, db);
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_FROM)) {
		p->ResetContent ();

		for (int i = 0; i < vDSN.GetSize (); i++) 
			p->AddString (vDSN [i]);
	}

	EndWaitCursor ();
}

#endif //__WINPRINTER__
