#include "stdafx.h"
#include "mkdraw.h"
#include "NewMessageDocType.h"
#include "Extern.h"
#include "Debug.h"
#include "MessageFrame.h"

//#include "MessageDoc.h"
//#include "MessageView.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;
using namespace EditorGlobals;
using namespace FoxjetDocument;

