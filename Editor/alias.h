#ifndef __ALIAS_H__
#define __ALIAS_H__

// Common headers from the ElementList.dll project.

#if defined(  __IPPRINTER__ )
	#pragma include_alias( "ListGlobals.h",			"DLL\ElementList\IP\Extern.h" )
#elif defined( __WINPRINTER__ )
	#pragma include_alias( "ListGlobals.h",			"DLL\ElementList\Win\Extern.h" )
#else
	#error Document\Extern.h: Incorrect configuration
#endif 

#endif //__ALIAS_H__