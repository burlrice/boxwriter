#if !defined(AFX_TASKVIEW_H__FF8578A4_A29C_4452_9ED8_E1EFAB065723__INCLUDED_)
#define AFX_TASKVIEW_H__FF8578A4_A29C_4452_9ED8_E1EFAB065723__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TaskView.h : header file
//

#include "BaseView.h"
#include "TaskDoc.h"
#include "Image.h"
#include "DxView.h"
#include "CopyArray.h"
#include "OpenDlg.h"
#include "UserElementDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CTaskView view
namespace HeadView
{
	class CHeadView;
}

namespace TaskView
{
	typedef enum { NA, ALL, NONE } SELECTIONTYPE;

	typedef FoxjetCommon::CCopyArray <Element::CElement *, Element::CElement *> CElementPtrArray;
	typedef FoxjetCommon::CCopyArray <CEditorElementList *, CEditorElementList *> CElementListPtrArray;

	class CTaskView : public FoxjetDocument::CBaseView
	{
		friend class HeadView::CHeadView;

	protected:
		CTaskView();           // protected constructor used by dynamic creation
		DECLARE_DYNCREATE(CTaskView)

	// Attributes
	public:
		Foxjet3d::DxParams::CDxView m_3d;

	public:
		CElementPtrArray GetSelected () const;
		CElementListPtrArray GetLists () const;
		FoxjetCommon::CElementListArray GetLists (int) const;
		CEditorElementList * GetList (ULONG lHeadID) const;
		Element::CPairArray GetSelected (bool bAll, ULONG lHeadID = -1) const;
		int Find (const FoxjetCommon::CBaseElement * pFind, const Element::CPairArray & sel);

		bool GetSelHeadUnified (FoxjetDatabase::HEADSTRUCT & head) const;
		void OnDrawTrackerRect (CDC * pDC, const CRect & rc, DWORD dwElement);

		bool SelHasFont (const Element::CPairArray & sel) const;
		SELECTIONTYPE GetSelFontSizeUnified (const Element::CPairArray & sel) const;
		SELECTIONTYPE GetSelFontWidthUnified (const Element::CPairArray & v) const;
		SELECTIONTYPE GetSelFontNameUnified (const Element::CPairArray & sel) const;
		SELECTIONTYPE GetSelFontItalicUnified (const Element::CPairArray & sel) const;
		SELECTIONTYPE GetSelFontBoldUnified (const Element::CPairArray & sel) const;
		SELECTIONTYPE GetSelInverseUnified (const Element::CPairArray & sel) const;
		SELECTIONTYPE GetSelFlipVUnified (const Element::CPairArray & sel) const;
		SELECTIONTYPE GetSelFlipHUnified (const Element::CPairArray & sel) const;
		SELECTIONTYPE GetSelResizableUnified (const Element::CPairArray & sel) const;
		SELECTIONTYPE GetSelMovableUnified (const Element::CPairArray & sel) const;

		int GetSelType () const;
		void InitElementMenu(CMenu *pMenu);
		int GetElementCount () const;
		virtual void InitMenu (CMenu * pMenu);
		virtual void InitContextMenu(CMenu *pMenu);
	
		HBITMAP CreateBitmap (CDC & dc, Element::CPairArray & vDraw) const;
		static void CALLBACK SetLocationFct (const CString & str);

	// Operations
	public:
		void SaveState();
		Foxjet3d::Box::CBoxParams & GetParams () const;
		ULONG GetActiveHead () const;
		bool SetActiveHead (ULONG lHeadID, bool bScrollToCrosshairs = true);
		UINT GetActivePanel () const;
		bool SetActivePanel (UINT nPanel);
		void OnRotateOrientation(Foxjet3d::Box::ROTATION r);
		CSize CalcMaxLogicalSize () const;
		CBitmap * CreateBitmap(CDC &dc);

		void ScrollToCrosshairs ();
		bool GetCrosshairsVisible () const;
		void MoveCrosshairs (const CPoint & ptOffset);
	
		afx_msg void MoveCrosshairsUp ();
		afx_msg void MoveCrosshairsDown ();
		afx_msg void MoveCrosshairsLeft ();
		afx_msg void MoveCrosshairsRight ();

		afx_msg void JumpCrosshairsUp ();
		afx_msg void JumpCrosshairsDown ();
		afx_msg void JumpCrosshairsLeft ();
		afx_msg void JumpCrosshairsRight ();

		afx_msg void OnViewZoomCustom();

		virtual bool SetZoom (double dZoom, bool bAutoScroll = false);
		virtual double GetZoom () const;
		virtual CSize GetPageSize () const;

		TaskDoc::CTaskDoc * GetDocument () const;
		void Invalidate (BOOL bErase = TRUE);
		CRect InvalidateCrosshairs (bool bRedraw = true);

		bool IsPrintPreviewOpen () const { return m_pPreview != NULL; }

		CString m_strRegSection;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CTaskView)
		public:
		virtual void OnDraw (CDC * pDC);
		virtual void OnInitialUpdate();
		virtual BOOL IsSelected(const CObject* pDocItem) const;
		protected:
		virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
		virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
		//}}AFX_VIRTUAL

		virtual void OnActivateFrame( UINT nState, CFrameWnd* pFrameWnd );

	// Implementation
	protected:
		virtual ~CTaskView();

		static enum Tick { MINOR = 0, MEDIUM, MAJOR };

		void DrawHRuler (CDC & dc, const CRect & rc, const CPoint & ptOffset, 
			CArray <CRect, CRect &> & v, const FoxjetDatabase::HEADSTRUCT & head) const;
		void DrawVRuler (CDC & dc, const CRect & rc, const CPoint & ptOffset, 
			CArray <CRect, CRect &> & v, const FoxjetDatabase::HEADSTRUCT & head) const;
		void DrawHorzUnits (CDC & dc, const CRect & rc, const CPoint & ptOffset, double dUnitWidth, 
			int nMediumTicksPerUnit, int nMinorTicksPerUnit, double dZoom, int nFactor = 1) const;
		void DrawVertUnits (CDC & dc, const CRect & rc, const CPoint & ptOffset, double dUnitWidth, 
			int nMediumTicksPerUnit, int nMinorTicksPerUnit, double dZoom, int nFactor = 1) const;

		bool CanCreateElement (Foxjet3d::Box::CBoxParams & p) const;

		//CCriticalSection m_csServer;
		Foxjet3d::Box::CBoxParams * m_pLastParams;
		CSize m_sizeRuler;
		bool m_bShowHRuler;
		bool m_bShowVRuler;
		bool m_bTrackingElement;
		static const int m_nTickWidth [3];
		HACCEL m_hAccel;
		CPoint * m_ptDrag;
		CPoint * m_ptScroll;
		Element::CElement * m_pHit;
		CTime m_tmKbChange;
		CSize m_sizePref;

	#ifdef _DEBUG
		virtual void AssertValid() const;
		virtual void Dump(CDumpContext& dc) const;
	#endif

	protected:

		void SetCursor (const CPoint & pt);
		void SetCursor (const Foxjet3d::Box::CHit & hit);
		Foxjet3d::Box::CHit DoMouseSelect(UINT nFlags, const Foxjet3d::Box::CHit & hit, const CPoint & pt);
		void OnCreateElement (int nClassID);
		void Update (CEditorElementList & list, Element::CElement * p, const CPoint & pt1000ths, const Foxjet3d::Panel::CBoundary & head, const Foxjet3d::Panel::CBoundary & workingHead, Element::CPairArray & all,
			Foxjet3d::Box::CBoxParams & params, FoxjetCommon::CElementArray & vUpdate, TaskDoc::CSnapshot * pSnapshot);

		bool m_bInitUpdate;
		bool m_bTrackingPopupMenu;
		static const int m_nMinorMove;
		static const int m_nMajorMove;

	public:
		static void GetUserElements (FoxjetCommon::CBaseElement & e, Foxjet3d::UserElementDlg::CUserElementArray & v, FoxjetDatabase::HEADSTRUCT & head, bool bTaskStart);
		static void GetUserElements (CEditorElementList & list, Foxjet3d::UserElementDlg::CUserElementArray & v, FoxjetDatabase::HEADSTRUCT & head);

		afx_msg void OnApiElementCommand(UINT nID);
		afx_msg void OnUpdateApiElementCommand (CCmdUI * pCmdUI);
		afx_msg void OnElementEdit();
		afx_msg void OnElementsDelete();
		afx_msg void OnUpdateElementsDelete(CCmdUI* pCmdUI);
		afx_msg void OnElementsList();
		afx_msg void OnPropertiesMovable();
		afx_msg void OnPropertiesResizable(); 
		afx_msg void OnPropertiesFliph(); 
		afx_msg void OnPropertiesFlipv();
		afx_msg void OnPropertiesInverse();
		afx_msg void OnUpdateElementsEdit(CCmdUI* pCmdUI);
		afx_msg void OnViewRulersHorizonal();
		afx_msg void OnUpdateViewRulersHorizonal(CCmdUI* pCmdUI);
		afx_msg void OnViewRulersVertical();
		afx_msg void OnUpdateViewRulersVertical(CCmdUI* pCmdUI);
		afx_msg void OnElementNext ();
		afx_msg void OnElementPrev ();
		afx_msg void OnBringToFront();
		afx_msg void OnSendToBack();
		afx_msg void OnBringForward ();
		afx_msg void OnSendBackward ();
	
		afx_msg LRESULT OnRedraw(WPARAM wParam, LPARAM lParam);
		afx_msg LRESULT OnKbChange (WPARAM wParam, LPARAM lParam);
		afx_msg LRESULT OnSetUnfolded (WPARAM wParam, LPARAM lParam);

		void UpdateSelection (Element::CPairArray & sel, Element::CPair & pair);

		BOOL RedrawWindow (LPCRECT lpRectUpdate = NULL, CRgn * prgnUpdate = NULL, UINT flags = RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE); 

	protected:
		CMapStringToString m_mapPreview;

		virtual BOOL PreTranslateMessage( MSG *pMsg );

		afx_msg void OnUpdateStatusOrient (CCmdUI * pCmdUI);
		afx_msg void OnUpdateStatusCrosshairs(CCmdUI *pCmdUI);
		afx_msg void OnUpdateStatusLocation (CCmdUI * pCmdUI);
		afx_msg void OnBringToFront (UINT nID);
		afx_msg void OnPanelProperties (UINT nID);
		afx_msg void OnJumpdown(); 
		afx_msg void OnJumpleft();
		afx_msg void OnJumpright(); 
		afx_msg void OnJumpup(); 
		afx_msg void OnDown(); 
		afx_msg void OnUp(); 
		afx_msg void OnLeft(); 
		afx_msg void OnRight(); 
		afx_msg void OnPagedown(); 
		afx_msg void OnPageleft();
		afx_msg void OnPageright(); 
		afx_msg void OnPageup(); 
		afx_msg void OnHome(); 
		afx_msg void OnEnd(); 
		afx_msg void OnBottom(); 
		afx_msg void OnTop(); 
		afx_msg void OnMovedown(); 
		afx_msg void OnMoveleft(); 
		afx_msg void OnMoveright(); 
		afx_msg void OnMoveup(); 
		afx_msg void MoveSelection(int cx, int cy);
		afx_msg void OnPaint ();
		afx_msg void OnFilePrintPreview();
		afx_msg void OnFilePrint ();

		virtual BOOL OnPreparePrinting( CPrintInfo* pInfo );
		virtual void OnPrint( CDC* pDC, CPrintInfo* pInfo );
		virtual void OnEndPrinting (CDC* pDC, CPrintInfo* pInfo);
		
		bool DoUserPrompt ();

		Foxjet3d::OpenDlg::CTaskPreview * m_pPreview;
		bool m_bUnfolded;

		//{{AFX_MSG(CTaskView)
		afx_msg BOOL OnEraseBkgnd(CDC* pDC);
		afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
		afx_msg void OnDestroy();
		afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
		afx_msg void OnSetFocus(CWnd* pOldWnd);
		afx_msg void OnSize(UINT nType, int cx, int cy);
		afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
		afx_msg void OnClose();
		afx_msg void OnMouseMove(UINT nFlags, CPoint point);
		afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
		afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
		afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
		afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
		afx_msg void OnInitMenu(CMenu* pMenu);
		afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
		afx_msg void OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp);
		afx_msg void OnNcPaint();
		afx_msg void OnDebuggingstuffStrobe();
		afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
		afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace TaskView
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TASKVIEW_H__FF8578A4_A29C_4452_9ED8_E1EFAB065723__INCLUDED_)
