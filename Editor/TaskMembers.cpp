#include "stdafx.h"
#include "mkdraw.h"
#include "TaskDoc.h"
#include "Debug.h"
#include "Compare.h"
#include "CopyArray.h"
#include "TemplExt.h"
#include "Extern.h"
#include "Message.h"
#include "Version.h"

using namespace Foxjet3d;
using namespace FoxjetCommon;
using namespace EditorGlobals;
using namespace FoxjetDocument;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

bool operator == (const CTaskMembers & lhs, const CTaskMembers & rhs)
{
	CTaskMembers * pLhs = (CTaskMembers *)(LPVOID)&lhs;
	CTaskMembers * pRhs = (CTaskMembers *)(LPVOID)&rhs;

	return 
		lhs.m_task		== rhs.m_task &&
		lhs.m_box		== rhs.m_box;// &&
		//pLhs->m_vHeads	== pRhs->m_vHeads;
}

bool operator != (const CTaskMembers & lhs, const CTaskMembers & rhs)
{
	return !(lhs == rhs);
}

CTaskMembers::CTaskMembers ()
:	m_box (database),
	CBaseMembers ()
{
}

CTaskMembers::CTaskMembers (const CTaskMembers & rhs)
:	m_box (rhs.m_box),
	CBaseMembers (rhs)
{
	CTaskMembers * pRhs = (CTaskMembers *)(LPVOID)&rhs;
	
	m_task			= rhs.m_task;
	m_box			= rhs.m_box;
}

CTaskMembers & CTaskMembers::operator = (const CTaskMembers & rhs)
{
	CBaseMembers::operator = (rhs);

	if (this != &rhs) {
		CTaskMembers * pRhs = (CTaskMembers *)(LPVOID)&rhs;
	
		m_task			= rhs.m_task;
		m_box			= rhs.m_box;
	}

	return * this;
}

CTaskMembers::~CTaskMembers ()
{
}
