// NewTaskDocType.h: interface for the CNewTaskDocType class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NEWTASKDOCTYPE_H__A78C15C7_82F8_44BE_B331_6033CBA6FC51__INCLUDED_)
#define AFX_NEWTASKDOCTYPE_H__A78C15C7_82F8_44BE_B331_6033CBA6FC51__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "NewMessageDocType.h"
#include "MkDrawDocManager.h"
#include "BaseDoc.h"

class CNewTaskDocType : public CNewDocType  
{
	DECLARE_DYNAMIC (CNewTaskDocType)
public:
	CNewTaskDocType();
	virtual ~CNewTaskDocType();

protected:
	CNewTaskDocType (const CNewTaskDocType & rhs);
	CNewTaskDocType & operator = (const CNewTaskDocType & rhs);
};

class CTaskDocTemplate : public FoxjetDocument::CBaseDocTemplate
{
	DECLARE_DYNAMIC (CTaskDocTemplate)
public:
	virtual CDocument * OpenDocumentFile( LPCTSTR lpszPathName, BOOL bMakeVisible = TRUE );
	CTaskDocTemplate ();
	virtual ~CTaskDocTemplate ();

	virtual CDocument * CreateNewDocument ();
	virtual CFrameWnd * CreateNewFrame (CDocument * pDoc, CFrameWnd * pOther);
	virtual CString GetDefPrefix () const;
	virtual bool IsExistingDoc (const CString & strTitle) const;
	virtual void LoadTemplate();

	static bool ParseTaskFromPath (const CString & strPath, CString & strLine, CString & strTask);

	FoxjetDatabase::LINESTRUCT m_line;
	FoxjetDatabase::BOXSTRUCT m_box;
	CString m_strTemplate;
	
protected:
	CTaskDocTemplate (const CTaskDocTemplate & rhs);
	CTaskDocTemplate & operator = (const CTaskDocTemplate & rhs);
};

#endif // !defined(AFX_NEWTASKDOCTYPE_H__A78C15C7_82F8_44BE_B331_6033CBA6FC51__INCLUDED_)
