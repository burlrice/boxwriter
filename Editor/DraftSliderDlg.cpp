// DraftSliderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "mkdraw.h"
#include "DraftSliderDlg.h"
#include "afxdialogex.h"
#include "Debug.h"
#include "Parse.h"

// CDraftSliderDlg dialog

IMPLEMENT_DYNAMIC(CDraftSliderDlg, CDialogEx)

CDraftSliderDlg::CDraftSliderDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDraftSliderDlg::IDD, pParent)
{

}

CDraftSliderDlg::~CDraftSliderDlg()
{
}

void CDraftSliderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDraftSliderDlg, CDialogEx)
	ON_WM_ACTIVATE()
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
	ON_WM_HSCROLL()
END_MESSAGE_MAP()


// CDraftSliderDlg message handlers


BOOL CDraftSliderDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	if (CSliderCtrl * p = (CSliderCtrl *)GetDlgItem (SLD_DRAFT)) {
		p->SetRangeMin (0);
		p->SetRangeMax (100);
		p->SetTicFreq (20);

		for (int i = 0; i < 100; i += 10)
			p->SetTic (i);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDraftSliderDlg::OnActivate (UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	if (nState == WA_INACTIVE) {
		GetParent ()->PostMessage (WM_DRAFTSLIDERCLOSED);
		EndDialog (IDOK);
	}
}

void CDraftSliderDlg::OnSetfocus()
{
}

void CDraftSliderDlg::OnKillfocus()
{
}

void CDraftSliderDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CSliderCtrl * pSlide = (CSliderCtrl *)GetDlgItem (SLD_DRAFT);

	SetDlgItemText (TXT_PCT, FoxjetDatabase::ToString (pSlide->GetPos ()));
	GetParent ()->PostMessage (WM_DRAFTSLIDERCHANGED, pSlide->GetPos ());
}