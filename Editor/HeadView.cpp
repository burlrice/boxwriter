// HeadView.cpp : implementation file
//

#include "stdafx.h"
#include "mkdraw.h"
#include "HeadView.h"
#include "MainFrm.h"
#include "Database.h"
#include "Extern.h"
#include "TaskView.h"
#include "Debug.h"
#include "PanelDlg.h"
#include "Coord.h"
#include "MsgBox.h"

#include "HeadConfigDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ID_COPYTO_HEAD_RANGE_START		101
#define ID_COPYTO_HEAD_RANGE_END		110
#define ID_LINKEDTO_HEAD_RANGE_START	111
#define ID_LINKEDTO_HEAD_RANGE_END		120

using namespace Foxjet3d;
using namespace EditorGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDocument;
using namespace FoxjetDatabase;

using namespace HeadView;
using namespace TaskDoc;
using namespace TaskView;
using namespace Panel;

/////////////////////////////////////////////////////////////////////
//
CItem::CItem (TYPE type)
:	m_type (type),
	m_lID (-1)
{
}

CItem::CItem (UINT nPanel)
:	m_type (PANEL),
	m_nPanel (nPanel)
{
}

CItem::CItem (ULONG lID, TYPE type)
:	m_type (type),
	m_nPanel (lID)
{
}

CItem::~CItem ()
{
}

/////////////////////////////////////////////////////////////////////
//

CTreeItem::CTreeItem (const FoxjetDatabase::BOXSTRUCT & s)
:	HeadView::CItem (BOX)
{
	using namespace FoxjetDatabase;

	m_pBox = new BOXSTRUCT (s);
}

CTreeItem::CTreeItem (const FoxjetDatabase::PANELSTRUCT & s)
:	HeadView::CItem (PANEL)
{
	using namespace FoxjetDatabase;

	m_pPanel = new PANELSTRUCT (s);
}

CTreeItem::CTreeItem (const Panel::CBoundary & s)
:	HeadView::CItem (HEAD)
{
	m_pHead = new Panel::CBoundary (s);
}


CTreeItem::~CTreeItem ()
{
	switch (m_type) {
	case BOX:		delete m_pBox;			break;
	case PANEL:		delete m_pPanel;		break;
	case HEAD:		delete m_pHead;			break;
	}

	m_pBox = NULL;
	m_pPanel = NULL;
	m_pHead = NULL;
}

UINT CTreeItem::GetMenuID () const
{
	switch (m_type) {
	case BOX:		return IDM_BOXCONTEXT;
	case PANEL:		return IDM_PANELCONTEXT;
	case HEAD:		return IDM_HEADCONTEXT;
	}

	return IDM_TASKCONTEXT;
}

UINT CTreeItem::GetDefMenuItemID () const
{
	switch (m_type) {
	case BOX:		return ID_BOX_CHANGE;
	case PANEL:		return ID_PANEL_PROPERTIES;
	case HEAD:		return ID_HEAD_PROPERTIES;
	}

	return -1;
}

/////////////////////////////////////////////////////////////////////////////
// CHeadView

IMPLEMENT_DYNCREATE(CHeadView, CTreeView)

HeadView::CHeadView::CHeadView()
:	m_hPanel (NULL),
	m_hTask (NULL),
	m_hHead (NULL),
	m_hHeadMaster (NULL),
	m_hHeadDisabled (NULL),
	m_nTaskIndex (0),
	m_nPanelIndex (0),
	m_nHeadIndex (0),
	m_nHeadMasterIndex (0),
	m_nHeadDisabledIndex (0),
	m_bInitUpdate (false)
{
}

HeadView::CHeadView::~CHeadView()
{
	::DestroyIcon (m_hPanel);
	::DestroyIcon (m_hTask);
	::DestroyIcon (m_hHead);
	::DestroyIcon (m_hHeadMaster);
	::DestroyIcon (m_hHeadDisabled);
}


BEGIN_MESSAGE_MAP(CHeadView, CTreeView)
	//{{AFX_MSG_MAP(CHeadView)
	ON_WM_DESTROY()
	ON_NOTIFY_REFLECT(NM_RCLICK, OnRclick)
	ON_NOTIFY_REFLECT(TVN_DELETEITEM, OnDeleteitem)
	ON_NOTIFY_REFLECT(NM_DBLCLK, OnDblclk)
	ON_WM_INITMENU()
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, OnSelchanged)
	ON_NOTIFY_REFLECT(TVN_ITEMEXPANDED, OnItemexpanded)
	ON_WM_LBUTTONUP()
	ON_WM_MBUTTONUP()
	ON_WM_RBUTTONUP()
	//}}AFX_MSG_MAP

	ON_COMMAND_RANGE(ID_COPYTO_HEAD_RANGE_START, ID_COPYTO_HEAD_RANGE_END, OnCopyToHead)
	ON_COMMAND_RANGE(ID_LINKEDTO_HEAD_RANGE_START, ID_LINKEDTO_HEAD_RANGE_END, OnLinkedToHead)

	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHeadView drawing

void HeadView::CHeadView::OnDraw(CDC* pDC)
{
	CTreeView::OnDraw (pDC);
}

/////////////////////////////////////////////////////////////////////////////
// CHeadView diagnostics

#ifdef _DEBUG
void HeadView::CHeadView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CHeadView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CHeadView message handlers

void HeadView::CHeadView::OnInitialUpdate() 
{
	TRACEF (_T ("HeadView::CHeadView::OnInitialUpdate"));

	CTreeView::OnInitialUpdate();

	if (m_ilTreeView.m_hImageList == NULL) {
		CTreeCtrl & tree = GetTreeCtrl ();
		HINSTANCE hInst = ::AfxGetInstanceHandle ();
		const int nSize = FoxjetDatabase::IsHighResDisplay () ? 32 : 16;

		m_ilTreeView.Create (nSize, nSize, ILC_COLOR, 0, 6);

		VERIFY (m_hPanel		= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_PANEL),			IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
		VERIFY (m_hTask			= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDR_TASK),			IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
		VERIFY (m_hHead			= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_HEAD),			IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
		VERIFY (m_hHeadMaster	= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_HEADMASTER),		IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));
		VERIFY (m_hHeadDisabled	= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_HEADDISABLED),	IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED));

		m_nTaskIndex			= m_ilTreeView.Add (m_hTask);
		m_nPanelIndex			= m_ilTreeView.Add (m_hPanel);
		m_nHeadIndex			= m_ilTreeView.Add (m_hHead);
		m_nHeadMasterIndex		= m_ilTreeView.Add (m_hHeadMaster);
		m_nHeadDisabledIndex	= m_ilTreeView.Add (m_hHeadDisabled);

		tree.ModifyStyle (0, TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS | 
			TVS_SHOWSELALWAYS | TVS_DISABLEDRAGDROP);
		tree.SetImageList (&m_ilTreeView, TVSIL_NORMAL);

		FillCtrl ();
	}

	m_bInitUpdate = true;
	TRACEF (_T ("HeadView::CHeadView::OnInitialUpdate exiting"));
}

void HeadView::CHeadView::OnDestroy() 
{
	CImageList * pil = GetTreeCtrl ().GetImageList (TVSIL_NORMAL);

	if (pil) 
		pil->DeleteImageList ();

	CTreeView::OnDestroy();
}

CTaskDoc * HeadView::CHeadView::GetDocument()
{
	CDocument * pDocument = CView::GetDocument ();
	ASSERT(pDocument);
	ASSERT(pDocument->IsKindOf(RUNTIME_CLASS(CTaskDoc)));
	return (CTaskDoc *)pDocument;
}

const CTaskDoc * HeadView::CHeadView::GetDocument() const
{
	CDocument * pDocument = CView::GetDocument ();
	ASSERT(pDocument);
	ASSERT(pDocument->IsKindOf(RUNTIME_CLASS(CTaskDoc)));
	return (CTaskDoc *)pDocument;
}

static BOOL operator <= (const FoxjetDatabase::HEADSTRUCT & hLhs, const FoxjetDatabase::HEADSTRUCT & hRhs)
{
	return hLhs.m_lRelativeHeight <= hRhs.m_lRelativeHeight;
}


void HeadView::CHeadView::OnRclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CPoint pt;
	CMenu mnu;
	UINT nMenuID = IDM_TASKCONTEXT;
	UINT nDefID = -1;
	UINT nFlags;
	const CTaskDoc & doc = * GetDocument ();
	CTaskView * pView = (CTaskView *)doc.GetView ();
	ULONG lActiveHeadID = pView->GetActiveHead ();
	ULONG lActiveCardID = 0, lLinkedTo = 0;

	::GetCursorPos (&pt);
	ScreenToClient (&pt);
	HTREEITEM hItem = GetTreeCtrl ().HitTest (pt, &nFlags);

	if (hItem && (nFlags & TVHT_ONITEM)) {
		CTreeItem * pItem = (CTreeItem *)GetTreeCtrl ().GetItemData (hItem);

		GetTreeCtrl ().SelectItem (hItem);
		nMenuID = pItem->GetMenuID ();
		nDefID = pItem->GetDefMenuItemID ();

		if (pItem->m_type == HeadView::HEAD) {
			lActiveHeadID = pItem->m_pHead->GetID ();
			lActiveCardID = pItem->m_pHead->m_card.m_lID;
			lLinkedTo = pItem->m_pHead->m_card.m_lLinkedTo;
		}
	}

	if (mnu.LoadMenu (nMenuID)) {
		CPoint ptScreen;
		CMenu * pMenu = mnu.GetSubMenu (0);
		CTaskView * pTaskView = GetTaskView ();
		static const UINT nID [] = 
		{
			ID_PANEL_PROPERTIES1,
			ID_PANEL_PROPERTIES2,
			ID_PANEL_PROPERTIES3,
			ID_PANEL_PROPERTIES4,
			ID_PANEL_PROPERTIES5,
			ID_PANEL_PROPERTIES6,
			ID_EDIT_BRINGTOFRONT1,
			ID_EDIT_BRINGTOFRONT2,
			ID_EDIT_BRINGTOFRONT3,
			ID_EDIT_BRINGTOFRONT4,
			ID_EDIT_BRINGTOFRONT5,
			ID_EDIT_BRINGTOFRONT6,
			-1
		};

		ASSERT (pMenu);
		//ClientToScreen (&ptScreen);
		::GetCursorPos (&ptScreen);

		for (int i = 0; nID [i] != -1; i++) 
			if (nDefID != nID [i])
				pMenu->DeleteMenu (nID [i], MF_BYCOMMAND);

		if (pTaskView)
			pTaskView->InitContextMenu (pMenu);

		if (nDefID != -1)
			pMenu->SetDefaultItem (nDefID);

		if (nMenuID == IDM_HEADCONTEXT) {
			UINT nBase [] = { ID_COPYTO_HEAD_RANGE_START /*, ID_LINKEDTO_HEAD_RANGE_START */ };
			int nMenu [] = { 2 /* , 4 */ };

			for (int i = 0; i < ARRAYSIZE (nMenu); i++) {
				if (CMenu * pSub = pMenu->GetSubMenu (nMenu [i])) {
					pSub->RemoveMenu (0, MF_BYPOSITION);

					m_vHeads.RemoveAll ();
					doc.GetHeads (m_vHeads);

					for (int nHead = 0; nHead < m_vHeads.GetSize (); nHead++) {
						CBoundary & h = m_vHeads [nHead];

						if (h.GetID () != lActiveHeadID) {
							UINT nID = nBase [i] + nHead;

							pSub->AppendMenu (MF_STRING, nID, h.GetName ());

							if (nBase [i] == ID_LINKEDTO_HEAD_RANGE_START) 
								pSub->CheckMenuItem (nID, lLinkedTo == h.m_card.m_lID ? MF_CHECKED : MF_UNCHECKED);

							{
								bool bEnabled = (h.m_lLinkedTo == 0);

								if (bEnabled) {
									CBoundary link;

									if (doc.m_box.GetLinkedSlave (h, link)) 
										bEnabled = false;
								}

								if (!bEnabled)
									pSub->EnableMenuItem (nID, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
							}
						}
					}
				}
			}
		}

		pMenu->TrackPopupMenu (
			TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON,
			ptScreen.x, ptScreen.y, this);

	}

	if (pResult)
		* pResult = 0;
}

void HeadView::CHeadView::OnFilePrintPreview ()
{
	((CTaskView *)GetDocument ()->GetView ())->OnFilePrintPreview ();
}

void HeadView::CHeadView::OnFilePrint ()
{
	((CTaskView *)GetDocument ()->GetView ())->OnFilePrint ();
}

void HeadView::CHeadView::OnLinkedToHead (UINT nCmdID)
{
}

void HeadView::CHeadView::OnCopyToHead (UINT nCmdID)
{
	int nIndex = nCmdID - ID_COPYTO_HEAD_RANGE_START;
	CBoundary headFrom, headTo = m_vHeads [nIndex];
	CTaskDoc & doc = * GetDocument ();
	CTaskView * pView = (CTaskView *)doc.GetView ();
	Box::CBoxParams & params = pView->GetParams ();
	const double dZoom = pView->GetZoom ();
	const bool bPasteToCrosshairs = doc.GetPasteToCrosshairs ();

	doc.SetPasteToCrosshairs (false);
	doc.GetHead (pView->GetActiveHead (), headFrom);

	CMessage * pMsgFrom = doc.m_task.GetMessageByHead (headFrom.GetID ());
	CMessage * pMsgTo = doc.m_task.GetMessageByHead (headTo.GetID ());

	ASSERT (pMsgFrom);
	ASSERT (pMsgTo);

	TRACEF ("From: " + headFrom.GetName () + " to: " + headTo.GetName ());

	if (pMsgFrom->m_list.GetSize ()) {
		CEditorElementList & listFrom = pMsgFrom->m_list;
		CEditorElementList & listTo = pMsgTo->m_list;
		CString strRestore;

		if (!::OpenClipboard (NULL)) {
			TRACEF (_T ("OpenClipboard failed"));
			ASSERT (0);
			return;
		}
		
		if (::IsClipboardFormatAvailable (CF_UNICODETEXT)) 
			strRestore = (LPCTSTR)::GetClipboardData (CF_UNICODETEXT);

		::EmptyClipboard ();

		CString strData;
		TCHAR szCrLf [] = { 0x0D, 0x0A, 0x00 };
		HGLOBAL hClipboardText = NULL;

		for (int i = 0; i < listFrom.GetSize (); i++) {
			const CBaseElement & e = * listFrom [i].GetElement ();
			strData += e.ToString (GetElementListVersion ()) + szCrLf;
		}

		int nLen = strData.GetLength () + 1;

		#ifdef _UNICODE
		nLen = (strData.GetLength () * 2) + 1;
		#endif
	
		hClipboardText = ::GlobalAlloc (GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT, nLen);
		LPVOID lpCbTextData = ::GlobalLock (hClipboardText);
		memcpy (lpCbTextData, (LPCTSTR)strData, nLen);

		if (::SetClipboardData (CF_UNICODETEXT, hClipboardText) == NULL)
			MsgBox (_T ("SetClipboardData failed"));

		if (hClipboardText) {
			::GlobalUnlock (hClipboardText);
			hClipboardText = NULL;
		}

		if (!::CloseClipboard ())
			TRACEF (_T ("CloseClipboard failed"));

		TRACEF (headTo.GetName ());
		pView->SetActiveHead (headTo.GetID ());
		doc.OnCmdMsg (ID_EDIT_PASTE, 0, NULL, NULL);

		if (strRestore.GetLength ()) {
			::OpenClipboard (NULL);

			hClipboardText = ::GlobalAlloc (GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT,
				strRestore.GetLength () + 1);
			lpCbTextData = ::GlobalLock (hClipboardText);
			memcpy (lpCbTextData, (LPCTSTR)strRestore, strRestore.GetLength ());

			if (::SetClipboardData (CF_UNICODETEXT, hClipboardText) == NULL)
				MsgBox (_T ("SetClipboardData failed"));

			if (hClipboardText)
				::GlobalUnlock (hClipboardText);

			::CloseClipboard ();
		}
	}
	else {
		CString str;

		str.Format (LoadString (IDS_NOELEMENTSTOCOPY), headFrom.GetName ());
		MsgBox (* this, str);
	}

	doc.SetPasteToCrosshairs (bPasteToCrosshairs);
}

CTaskView * HeadView::CHeadView::GetTaskView()
{
	if (CTaskDoc * pDoc = GetDocument ())
		return DYNAMIC_DOWNCAST (CTaskView, pDoc->GetView ());

	return NULL;
}

void HeadView::CHeadView::OnDblclk(NMHDR* pNMHDR, LRESULT* pResult) 
{
	HTREEITEM hItem = GetTreeCtrl ().GetSelectedItem ();

	if (hItem) {
		CTreeItem * pItem = (CTreeItem *)GetTreeCtrl ().GetItemData (hItem);
		WPARAM wParam = MAKEWPARAM (pItem->GetDefMenuItemID (), 0);
		CWnd * pWnd = ::AfxGetMainWnd ();

		ASSERT (pWnd);
		::PostMessage (pWnd->m_hWnd, WM_COMMAND, wParam, NULL);
		::Sleep (0);
	}

	if (pResult)
		* pResult = 0;
}

void HeadView::CHeadView::OnDeleteitem(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW * p = (NM_TREEVIEW*)pNMHDR;

	CTreeItem * pItem = (CTreeItem *)GetTreeCtrl ().GetItemData (p->itemOld.hItem);
	
	if (pItem) 
		delete pItem;

	if (pResult)
		* pResult = 0;
}

void HeadView::CHeadView::OnSelect()
{
	using namespace TaskDoc;

	if (!m_bInitUpdate)
		return;

	HTREEITEM hItem = GetTreeCtrl ().GetSelectedItem ();

	if (hItem) {
		CTreeItem * pItem = (CTreeItem *)GetTreeCtrl ().GetItemData (hItem);
		CTaskDoc * pDoc = GetDocument ();
		const CTaskMembers & doc = pDoc->GetMembers ();
		CTaskView * pView = (CTaskView *)pDoc->GetView ();
		ASSERT (pView);
		CFrameWnd * pFrame = pView->GetParentFrame ();
		ASSERT (pFrame);

		if (pItem) {
			if (pItem->m_type == HEAD) {
				const FoxjetDatabase::PANELSTRUCT * pPanel = NULL;
				bool bUpdate = false;

				VERIFY (pDoc->m_box.GetPanelByID (pItem->m_pHead->GetPanelID (), &pPanel));
				bUpdate |= pView->SetActivePanel (pPanel->m_nNumber);
				bUpdate |= pView->SetActiveHead (pItem->m_pHead->GetID (), true);

				theApp.GetHeadBar ().SetActiveHead (pItem->m_pHead->GetID ());

				if (bUpdate)
					if (CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, pDoc->GetView ()))
						pView->RedrawWindow ();
			}
			else if (pItem->m_type == PANEL) {
				bool bUpdate = pView->SetActivePanel (pItem->m_pPanel->m_nNumber);

				theApp.GetHeadBar ().SetActivePanel (pItem->m_pPanel->m_nNumber);

				if (bUpdate)
					if (CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, pDoc->GetView ()))
						pView->RedrawWindow ();
			}

			if (CTaskView * pView = (CTaskView *)pDoc->GetView ())
				pView->SaveState ();
		}
	}
}

void HeadView::CHeadView::OnInitMenu(CMenu* pMenu) 
{
	CTreeView::OnInitMenu(pMenu);
	CTaskView * pView = GetTaskView ();

	if (pView)
		pView->InitMenu (pMenu);
}

void HeadView::CHeadView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	if (pHint) {
		if (FoxjetDocument::CHint * pBase = DYNAMIC_DOWNCAST (FoxjetDocument::CHint, pHint)) //NESTED_DYNAMIC_DOWNCAST (CBaseDoc, CHint, pHint))
		{
			if (TaskDoc::CHint * pTask = DYNAMIC_DOWNCAST (TaskDoc::CHint, pHint)) //NESTED_DYNAMIC_DOWNCAST (CTaskDoc, CHint, pHint))
			{
				CTreeCtrl & tree = GetTreeCtrl ();
				HTREEITEM hRoot = tree.GetRootItem ();
				const CTaskMembers & m = pTask->GetMembers ();

				if (pTask->IsType (TaskDoc::Hint::BOX)) {
					CTreeItem * pItem = (CTreeItem *)tree.GetItemData (hRoot);

					ASSERT (hRoot);
					ASSERT (pItem);

					delete pItem;
					tree.SetItemText (hRoot, m.m_box.GetBox ().m_strName);
					tree.SetItemData (hRoot, (DWORD)new CTreeItem (m.m_box.GetBox ()));
				}
				else if (pTask->IsType (TaskDoc::Hint::PANEL)) {
					int nIndex = 0;
					const CPanel * pPanels [6];
					const CTaskDoc & doc = * GetDocument ();

					if (doc.m_box.GetPanels (pPanels)) {
						for (HTREEITEM hPanel = tree.GetChildItem (hRoot); hPanel != NULL; nIndex++) {
							if (pTask->m_bUpdatedPanels [nIndex]) {
								const FoxjetDatabase::PANELSTRUCT & panel = pPanels [nIndex]->GetMembers ();
								CTreeItem * pItem = (CTreeItem *)tree.GetItemData (hPanel);

								delete pItem;

								tree.SetItemData (hPanel, (DWORD)new CTreeItem (panel));
								tree.SetItemText (hPanel, panel.m_strName);
							}

							hPanel = tree.GetNextItem (hPanel, TVGN_NEXT);
						}
					}
				}
			}
		}
	}
	else if (pSender && !pSender->IsKindOf (RUNTIME_CLASS (CHeadView)))
		FillCtrl ();
}

void HeadView::CHeadView::FillCtrl()
{
	using namespace Panel;

	CTreeCtrl & tree = GetTreeCtrl ();
	const CTaskDoc * pDoc = GetDocument ();
	const CTaskMembers & doc = pDoc->GetMembers ();
	const CTaskView * pView = (CTaskView *)pDoc->GetView ();
	//Box::CBoxParams * pParams = NULL;
	CString strSection;
	HTREEITEM hSel = NULL;
	const CPanel * pPanels [6] = { NULL };
	const bool bValve = ISVALVE ();

	if (!doc.m_box.HasPanels ())
		return;

	VERIFY (doc.m_box.GetPanels (pPanels));

	ASSERT (pView);
	Box::CBoxParams & params = pView->GetParams ();

	ULONG lSelHead = params.m_lHeadID;
	UINT nSelPanel = params.m_nPanel;

	tree.DeleteAllItems ();

	HTREEITEM hRoot = tree.InsertItem (doc.m_box.GetBox ().m_strName, 
		m_nTaskIndex, m_nTaskIndex, NULL, TVI_SORT);
	
	tree.SetItemData (hRoot, (DWORD)new CTreeItem (doc.m_box.GetBox ()));
	strSection.Format (_T ("Settings\\%s"), a2w (GetRuntimeClass ()->m_lpszClassName)); 

	for (int nPanel = 0; nPanel < 6; nPanel++) {
		ASSERT (pPanels [nPanel]);
		const CPanel & panel = * pPanels [nPanel];
		HTREEITEM hPanel = tree.InsertItem (panel.GetMembers ().m_strName, m_nPanelIndex, m_nPanelIndex, hRoot, TVI_LAST);

		tree.SetItemData (hPanel, (DWORD)new CTreeItem (panel.GetMembers ()));

		if (panel.GetMembers ().m_nNumber == nSelPanel) 
			hSel = hPanel;

		for (int nCard = 0; nCard < panel.m_vBounds.GetSize (); nCard++) {
			const CBoundary & head = panel.m_vBounds [nCard];
			int nHeadIndex = head.m_card.m_bMasterHead ? m_nHeadMasterIndex : m_nHeadIndex;

			if (bValve)
				nHeadIndex = m_nHeadIndex;

			if (!head.m_card.m_bEnabled)
				nHeadIndex = m_nHeadDisabledIndex;

			HTREEITEM hHead = tree.InsertItem (head.GetName (), nHeadIndex, nHeadIndex, hPanel, TVI_LAST);

			tree.SetItemData (hHead, (DWORD)new CTreeItem (head));

			if (head.GetID () == lSelHead) 
				hSel = hHead;

			tree.Expand (hHead, TVE_EXPAND);
		}

		if (theApp.GetProfileInt (strSection, panel.GetMembers ().m_strName, true))
			tree.Expand (hPanel, TVE_EXPAND);
	}

	if (hSel)
		tree.SelectItem (hSel);
}

void HeadView::CHeadView::OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult) 
{
 	OnSelect ();
 
 	if (pResult)
 		* pResult = 0;
}

bool HeadView::CHeadView::SetSelection (const HeadView::CItem & sel)
{
	CTreeCtrl & tree = GetTreeCtrl ();

	if (sel.m_type == PANEL) {
		HTREEITEM hBox = tree.GetNextItem (tree.GetRootItem (), TVGN_ROOT);
		HTREEITEM hItem = tree.GetNextItem (hBox, TVGN_CHILD);

		while (hItem) {
			CTreeItem * pItem = (CTreeItem *)tree.GetItemData (hItem);

			ASSERT (pItem);
			ASSERT (pItem->m_type == PANEL);

			if (pItem->m_pPanel->m_nNumber == sel.m_nPanel) {
				tree.Expand (hBox, TVE_EXPAND);
				tree.Select (hItem, TVGN_CARET);
				return true;
			}

			hItem = tree.GetNextItem (hItem, TVGN_NEXT);
		}
	}
	else if (sel.m_type == HEAD) {
		HTREEITEM hPanel = tree.GetNextItem (tree.GetRootItem (), TVGN_ROOT);
		hPanel = tree.GetNextItem (hPanel, TVGN_CHILD);

		while (hPanel) {
			HTREEITEM hHead = tree.GetNextItem (hPanel, TVGN_CHILD);

			while (hHead) {
				CTreeItem * pItem = (CTreeItem *)tree.GetItemData (hHead);

				ASSERT (pItem);
				ASSERT (pItem->m_type == HEAD);

				if (pItem->m_pHead->GetID () == sel.m_lID) {
					tree.Expand (hPanel, TVE_EXPAND);
					tree.Select (hHead, TVGN_CARET);
					return true;
				}

				hHead = tree.GetNextItem (hHead, TVGN_NEXT);
			}

			hPanel = tree.GetNextItem (hPanel, TVGN_NEXT);
		}
	}

	return false;
}

void HeadView::CHeadView::OnItemexpanded(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CTreeCtrl & tree = GetTreeCtrl ();
	NMTREEVIEW * pInfo = (NMTREEVIEW *)pNMHDR;
	HTREEITEM hItem = pInfo->itemNew.hItem;

	if (hItem) {
		bool bExpanded = pInfo->itemNew.state & TVIS_EXPANDED ? true : false;//(tree.GetItemState (hItem, TVIF_STATE) & TVIS_EXPANDED) ? true : false;
		CTreeItem * pItem = (CTreeItem *)tree.GetItemData (hItem);
		CString strSection;

		ASSERT (pItem);
		strSection.Format (_T ("Settings\\%s"), a2w (GetRuntimeClass ()->m_lpszClassName)); 

		switch (pItem->m_type) {
		case BOX:
			theApp.WriteProfileInt (strSection, _T ("Box"), bExpanded);
			break;
		case PANEL:
			theApp.WriteProfileInt (strSection, pItem->m_pPanel->m_strName, bExpanded);
			break;
		}
	}

	if (pResult)
		* pResult = 0;
}

void HeadView::CHeadView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CTreeView::OnLButtonUp(nFlags, point);
}

void CHeadView::OnMButtonUp(UINT nFlags, CPoint point) 
{
	CTreeView::OnMButtonUp(nFlags, point);
}

void HeadView::CHeadView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	CTreeView::OnRButtonUp(nFlags, point);
}

BOOL HeadView::CHeadView::OnCmdMsg (UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	BOOL bResult = CTreeView::OnCmdMsg (nID, nCode, pExtra, pHandlerInfo);

	if (!bResult) { 
		if (CTaskView * pView = (CTaskView *)GetDocument ()->GetView ()) {
			UINT n [] = 
			{
				ID_NEXT_ELEMENT,
				ID_PREV_ELEMENT,
			};

			for (int i = 0; i < ARRAYSIZE (n); i++) {
				if (n [i] == nID) {
					if (bResult = pView->OnCmdMsg (nID, nCode, pExtra, pHandlerInfo))
						pView->SetFocus ();
				}
			}
		}
	}

	return bResult;
}
