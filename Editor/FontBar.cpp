// FontBar.cpp : implementation file
//

#include "stdafx.h"
#include "mkdraw.h"
#include "FontBar.h"
#include "TaskDoc.h"
#include "TaskView.h"
#include "Extern.h"
#include "Debug.h"
#include "Database.h"
#include "PrinterFont.h"
#include "TextElement.h"
#include "WinMsg.h"
#include "Parse.h"
#include "resource.h"

using namespace EditorGlobals;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace TaskDoc;
using namespace TaskView;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFontBar dialog


CFontBar::CFontBar()
:	m_lHeadID (-1),
	m_nMin (5),
	m_nMax (5),
	m_bEditFont (false),
	m_nLastWidth (0),
	m_hAccel (NULL),
	m_bEditWidth (false), 
	m_pwndDraftSlider (NULL),
	CDialogBar()
{
	//{{AFX_DATA_INIT(CFontBar)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CFontBar::DoDataExchange(CDataExchange* pDX)
{
	CDialogBar::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFontBar)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CFontBar, CDialogBar)
	//{{AFX_MSG_MAP(CFontBar)
	ON_CBN_SELCHANGE(CB_FONTNAME, OnSelchangeFontname)
	ON_UPDATE_COMMAND_UI(CB_FONTNAME, OnUpdateName)
	ON_COMMAND(ID_FONTSIZE_CHANGE, OnFontsizeChange)
	//}}AFX_MSG_MAP

	ON_NOTIFY(CBEN_BEGINEDIT, CB_FONTSIZE, OnBegineditFontsize)
	ON_NOTIFY(CBEN_ENDEDIT, CB_FONTSIZE, OnEndeditFontsize)
	ON_CBN_SELCHANGE(CB_FONTSIZE, OnSelchangeFontsize)
	ON_UPDATE_COMMAND_UI(CB_FONTSIZE, OnUpdateSize)

	ON_NOTIFY(CBEN_BEGINEDIT, CB_FONTWIDTH, OnBegineditFontwidth)
	ON_NOTIFY(CBEN_ENDEDIT, CB_FONTWIDTH, OnEndeditFontwidth)
	ON_CBN_SELCHANGE(CB_FONTWIDTH, OnSelchangeFontwidth)
	ON_UPDATE_COMMAND_UI(CB_FONTWIDTH, OnUpdateWidth)

	ON_CBN_SETFOCUS(CB_FONTWIDTH, OnSetfocusFontwidth)
	ON_CBN_KILLFOCUS(CB_FONTWIDTH, OnKillfocusFontwidth)

	ON_REGISTERED_MESSAGE(WM_KB_ON_EN_CHANGE, OnKbEnChange)

	ON_BN_CLICKED (BTN_MORE, OnMore)
	ON_UPDATE_COMMAND_UI(BTN_MORE, OnUpdateWidth)
	ON_REGISTERED_MESSAGE(WM_DRAFTSLIDERCLOSED, OnDraftSliderClosed)
	ON_WM_CREATE ()
	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFontBar message handlers

int CFontBar::OnCreate (LPCREATESTRUCT lpCreateStruct)
{
	int nResult = CDialogBar::OnCreate (lpCreateStruct);

	return nResult;
}

CString CFontBar::GetFontName () const
{
	CString str;

	GetDlgItemText (CB_FONTNAME, str);

	return str;
}

int CFontBar::GetFontSize () const
{
	CString str;

	GetDlgItemText (CB_FONTSIZE, str);

	return _ttoi (str);
}

void CFontBar::OnSelchangeFontsize() 
{
	BeginWaitCursor ();

	if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) {
		CString str;

		GetDlgItemText (CB_FONTSIZE, str);
		pDoc->OnFontSizeChanged (_ttoi (str));
	}

	EndWaitCursor ();
}

void CFontBar::OnSelchangeFontname() 
{
	BeginWaitCursor ();

	if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) {
		CString str;

		GetDlgItemText (CB_FONTNAME, str);
		pDoc->OnFontNameChanged (str);
	}

	EndWaitCursor ();
}

bool CFontBar::SetFontName (const CString & strName)
{
	CComboBox * pName = (CComboBox *)GetDlgItem (CB_FONTNAME);

	ASSERT (pName);

	if (pName->GetCount () == 0) 
		if (CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, theApp.GetActiveView ())) 
			ResetContent (pView->GetActiveHead ());

	for (int i = 0; i < pName->GetCount (); i++) {
		CString str;

		if (pName->GetLBTextLen (i) > 0)
			pName->GetLBText (i, str);
		
		if (!str.CompareNoCase (strName)) {
			pName->SetCurSel (i);
			theApp.m_uiFont.m_strName = strName;
			return true;
		}
	}

	pName->SetCurSel (-1);
	theApp.m_uiFont.m_strName = _T ("");

	return false;
}

void CFontBar::ResetContent (ULONG lHeadID)
{
#ifdef __WINPRINTER__
	using namespace FoxjetElements;
	using namespace TextElement;
#else
	using namespace FoxjetIpElements;
#endif

	CComboBoxEx * pSize = (CComboBoxEx *)GetDlgItem (CB_FONTSIZE);
	CComboBox * pName = (CComboBox *)GetDlgItem (CB_FONTNAME);
	CArray <int, int> vSizes;

	m_nMin = CTextElement::m_lmtFontSize.m_dwMin;
	int nSize = m_nMax = CTextElement::m_lmtFontSize.m_dwMax;

	if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) {
		m_nMin = pDoc->GetBox ().GetMinFontSize (lHeadID);
		nSize = m_nMax = pDoc->GetBox ().GetMaxFontSize (lHeadID);
	}

	while (nSize > m_nMin) {
		vSizes.Add (nSize);
		nSize /= 2;
	}

	vSizes.Add (m_nMin);

	ASSERT (pSize);
	ASSERT (pName);
	pSize->ResetContent ();
	pName->ResetContent ();

	if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) {
		if (CEditorElementList * pList = pDoc->GetActiveList ()) {
			CArray <CPrinterFont, CPrinterFont &> v;

			pList->GetFonts (v);
			
			for (int i = 0; i < v.GetSize (); i++)
				pName->AddString (v [i].m_strName);
		}
	}

	for (int i = 0; i < vSizes.GetSize (); i++) 
		VERIFY (AddFontData (vSizes [i], CB_FONTSIZE) != CB_ERR);

	m_bEditFont = false;
}

BOOL CFontBar::PreTranslateMessage(MSG* pMsg) 
{
	if (!m_hAccel)
		VERIFY (m_hAccel = ::LoadAccelerators (::AfxGetInstanceHandle (), MAKEINTRESOURCE (IDA_FONTBAR)));

	if (::TranslateAccelerator (m_hWnd, m_hAccel, pMsg))
		return TRUE;

	return CDialogBar::PreTranslateMessage(pMsg);
}

void CFontBar::OnUpdateName (CCmdUI* pCmdUI) 
{
	bool bEnable = false;

	if (CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, theApp.GetActiveView ())) {
		CTaskDoc * pDoc = pView->GetDocument ();
		ULONG lHeadID = pView->GetActiveHead ();

		bEnable = !pDoc->GetReadOnly () && theApp.m_uiFont.m_name.m_bEnabled;

		if (m_lHeadID != lHeadID)
			ResetContent (lHeadID);

		m_lHeadID = lHeadID;
	}

	// if CTaskDoc::UpdateFontUIStruct doesn't have enough time to update the font bar
	// then this will do it
	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_FONTNAME)) 
		if (!p->GetDroppedState () && theApp.m_uiFont.m_strName != GetFontName ()) 
			SetFontName (theApp.m_uiFont.m_strName);

	if (CComboBoxEx * p = (CComboBoxEx *)GetDlgItem (CB_FONTSIZE)) {
		if (!p->GetDroppedState () && !m_bEditFont) {
			if (theApp.m_uiFont.m_nSize != GetFontSize ()) 
				SetFontSize (theApp.m_uiFont.m_nSize);
		}
	}

	pCmdUI->Enable (bEnable);
}

void CFontBar::OnBegineditFontsize(NMHDR* pNMHDR, LRESULT* pResult) 
{
	m_bEditFont = true;

	if (pResult)
		* pResult = 0;
}

void CFontBar::OnEndeditFontsize(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMCBEENDEDIT * p = (NMCBEENDEDIT *)pNMHDR;

	if (p && p->fChanged && (p->iWhy != CBENF_DROPDOWN && p->iWhy != CBENF_ESCAPE)) {
		CString str;// = p->szText;
		GetDlgItemText (CB_FONTSIZE, str);

		if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) {
			int n = 0;

			bool bNumeric = _stscanf (str, _T ("%d"), &n) == 1;

			if (bNumeric) {
				int nIndex = FindFontData (-1, str, CB_FONTSIZE);

				if (nIndex == CB_ERR) 
					nIndex = AddFontData (str, CB_FONTSIZE);


				if (nIndex != CB_ERR) {
					CComboBoxEx * pSize = (CComboBoxEx *)GetDlgItem (CB_FONTSIZE);
		
					ASSERT (pSize);
					pSize->SetCurSel (nIndex);
					OnSelchangeFontsize ();
				}
			}
		}

		m_bEditFont = false;
	}

	if (pResult)
		* pResult = 0;
}


LRESULT CFontBar::OnKbEnChange (WPARAM wParam, LPARAM lParam)
{
	#ifdef _DEBUG
	//#define CB_FONTSIZE                     1512
	CString str; str.Format (_T ("OnKbEnChange: %d, %d"), wParam, lParam); TRACEF (str); 
	#endif

	HWND hwnd = (HWND)lParam;
	ULONG lID = ::GetWindowLong (hwnd, GWL_ID);

	if (wParam == CB_FONTSIZE) {
		LRESULT lResult = 0;
		NMCBEENDEDIT nm;

		ZeroMemory (&nm, sizeof (nm));

		nm.fChanged = TRUE;
		nm.iWhy = CBENF_KILLFOCUS;

		OnEndeditFontsize ((NMHDR *)&nm, &lResult);
	}
	else if (wParam == CB_FONTWIDTH) {
		LRESULT lResult = 0;
		NMCBEENDEDIT nm;

		ZeroMemory (&nm, sizeof (nm));

		nm.fChanged = TRUE;
		nm.iWhy = CBENF_KILLFOCUS;

		OnEndeditFontwidth ((NMHDR *)&nm, &lResult);
	}

	return 1;
}

bool CFontBar::SetFontWidth (int nSize)
{
	if (m_bEditWidth) 
		return false;
	else {
		CComboBoxEx * pWidth = (CComboBoxEx *)GetDlgItem (CB_FONTWIDTH);
		int nIndex = CB_ERR;

		ASSERT (pWidth);

		if (nSize >= 0) {
			nIndex = FindFontData (-1, nSize, CB_FONTWIDTH);

			if (nIndex == CB_ERR)
				nIndex = AddFontData (nSize, CB_FONTWIDTH);
		}

		pWidth->SetCurSel (nIndex);

		return nIndex != CB_ERR;
	}
}

void CFontBar::OnUpdateSize (CCmdUI * pCmdUI)
{
	bool bEnable = false;

#ifdef __WINPRINTER__

	if (CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, theApp.GetActiveView ())) {
		CTaskDoc * pDoc = pView->GetDocument ();

		bEnable = !pDoc->GetReadOnly () && theApp.m_uiFont.m_size.m_bEnabled;
	}

#endif //__WINPRINTER__

	pCmdUI->Enable (bEnable);
}


void CFontBar::OnFontsizeChange() 
{
	if (CWnd * p = GetNextDlgTabItem (GetDlgItem (CB_FONTSIZE))) {
		::SendMessage (m_hWnd, WM_NEXTDLGCTL, (WPARAM)p->m_hWnd, 1L);
		::SendMessage (m_hWnd, WM_NEXTDLGCTL, 1, 0);
	}

	if (CWnd * p = GetNextDlgTabItem (GetDlgItem (CB_FONTWIDTH))) {
		::SendMessage (m_hWnd, WM_NEXTDLGCTL, (WPARAM)p->m_hWnd, 1L);
		::SendMessage (m_hWnd, WM_NEXTDLGCTL, 1, 0);
	}
}

void CFontBar::OnBegineditFontwidth(NMHDR* pNMHDR, LRESULT* pResult) 
{
	TRACEF (_T ("CFontBar::OnBegineditFontwidth"));

	if (pResult)
		* pResult = 0;
}

void CFontBar::OnEndeditFontwidth(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMCBEENDEDIT * p = (NMCBEENDEDIT *)pNMHDR;

	TRACEF (_T ("CFontBar::OnEndeditFontwidth"));

	if (p && p->fChanged && (p->iWhy != CBENF_DROPDOWN && p->iWhy != CBENF_ESCAPE)) {
		CString str;// = p->szText;
		CComboBoxEx * pWidth = (CComboBoxEx *)GetDlgItem (CB_FONTWIDTH);

		ASSERT (pWidth);

		GetDlgItemText (CB_FONTWIDTH, str);

		if (str.IsEmpty ())
			pWidth->GetComboBoxCtrl ()->GetWindowText (str);

		if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) {
			int n = 0;

			bool bNumeric = _stscanf (str, _T ("%d"), &n) == 1;

			if (bNumeric) {
				int nIndex = FindFontData (-1, str, CB_FONTWIDTH);

				if (nIndex == CB_ERR) 
					nIndex = AddFontData (str, CB_FONTWIDTH);

				if (nIndex != CB_ERR) {
					pWidth->SetCurSel (nIndex);
					SetDlgItemText (CB_FONTWIDTH, str);
					//OnSelchangeFontwidth ();
					pDoc->OnFontWidthChanged (m_nLastWidth = n);
				}
			}
			else {
				int nIndex = pWidth->GetCurSel ();

				if (nIndex != CB_ERR) {
					int nSize = pWidth->GetItemData (nIndex);

					if (nSize != CB_ERR && nSize != m_nLastWidth) 
						pDoc->OnFontWidthChanged (m_nLastWidth = nSize);
				}
			}
		}
	}

	if (pResult)
		* pResult = 0;
}

void CFontBar::OnSelchangeFontwidth() 
{
	TRACEF (_T ("CFontBar::OnSelchangeFontwidth"));

	BeginWaitCursor ();

	if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) {
		CComboBoxEx * pWidth = (CComboBoxEx *)GetDlgItem (CB_FONTWIDTH);

		ASSERT (pWidth);

		int nCurSel = pWidth->GetCurSel ();
		int nWidth = pWidth->GetItemData (nCurSel);

		pDoc->OnFontWidthChanged (nWidth);
	}

	EndWaitCursor ();
}

void CFontBar::OnUpdateWidth(CCmdUI * pCmdUI)
{
	bool bEnable = false;

	if (CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, theApp.GetActiveView ())) {
		CTaskDoc * pDoc = pView->GetDocument ();

		bEnable = !pDoc->GetReadOnly () && theApp.m_uiFont.m_size.m_bEnabled;
	}

	pCmdUI->Enable (bEnable);
}



//////////////////////////////////////////////////////////////////////////////////////////////////
bool CFontBar::SetFontSize (int nSize)
{
	return SetFontData (nSize, CB_FONTSIZE);
}

bool CFontBar::SetFontData (int nSize, UINT nCtrlID)
{
	CComboBoxEx * pCtrl = (CComboBoxEx *)GetDlgItem (nCtrlID);
	int nIndex = CB_ERR;

	ASSERT (pCtrl);

	if (nSize > 0) {
		nIndex = FindFontData (-1, nSize, nCtrlID);

		if (nIndex == CB_ERR)
			nIndex = AddFontData (nSize, nCtrlID);
	}

	pCtrl->SetCurSel (nIndex);

	if (nCtrlID == CB_FONTSIZE)
		theApp.m_uiFont.m_nSize = nSize;

	return nIndex != CB_ERR;
}

int CFontBar::GetFontData (int nIndex, UINT nCtrlID) const
{
	COMBOBOXEXITEM cbi;
	TCHAR sz [MAX_PATH];
	CComboBoxEx * pCtrl = (CComboBoxEx *)GetDlgItem (nCtrlID);

	ASSERT (pCtrl);

	cbi.mask = CBEIF_TEXT | CBEIF_LPARAM;
	cbi.iItem = nIndex;
	cbi.pszText = sz;
	cbi.cchTextMax = MAX_PATH;

	if (pCtrl->GetItem (&cbi))
		return (int)cbi.lParam;

	return -1;
}

int CFontBar::FindFontData (int nIndexStart, int nSize, UINT nCtrlID) const
{
	CComboBoxEx * pCtrl = (CComboBoxEx *)GetDlgItem (nCtrlID);

	ASSERT (pCtrl);

	for (int i = 0; i < pCtrl->GetCount (); i++) 
		if (GetFontData (i, nCtrlID) == nSize)
			return i;

	return CB_ERR;
}

int CFontBar::FindFontData (int nIndexStart, const CString & strSize, UINT nCtrlID) const
{
	return FindFontData (nIndexStart, _ttoi (strSize), nCtrlID);
}

int CFontBar::AddFontData (int nSize, UINT nCtrlID)
{
	CString str;
	str.Format (_T ("%d"), nSize);
	return AddFontData (str, nCtrlID);
}

int CFontBar::AddFontData (const CString & strSize, UINT nCtrlID)
{
	int nSize = _ttoi (strSize);
	int nIndex = CB_ERR;

	CComboBoxEx * pCtrl = (CComboBoxEx *)GetDlgItem (nCtrlID);
	nIndex = FindFontData (-1, strSize, nCtrlID);

	ASSERT (pCtrl);

	if (nIndex == CB_ERR) {
		COMBOBOXEXITEM cbi;
		int nInsert;

		for (nInsert = 0; nInsert < pCtrl->GetCount (); nInsert++) 
			if (GetFontData (nInsert, nCtrlID) > nSize)
				break;

		cbi.mask =	CBEIF_TEXT | CBEIF_LPARAM;
		cbi.iItem = nInsert;
		cbi.pszText = (LPTSTR)(LPCTSTR)strSize;
		cbi.cchTextMax = strSize.GetLength();
		cbi.lParam = nSize;

		nIndex = pCtrl->InsertItem (&cbi);
	}

	return nIndex;
}

void CFontBar::OnSetfocusFontwidth ()
{
	TRACEF (_T ("CFontBar::OnSetfocusFontwidth "));
}

void CFontBar::OnKillfocusFontwidth ()
{
	TRACEF (_T ("CFontBar::OnKillfocusFontwidth "));

	CString str;
	CComboBoxEx * pWidth = (CComboBoxEx *)GetDlgItem (CB_FONTWIDTH);

	m_bEditWidth = true;

	ASSERT (pWidth);
	GetDlgItemText (CB_FONTWIDTH, str);

	if (CTaskDoc * pDoc = DYNAMIC_DOWNCAST (CTaskDoc, theApp.GetActiveDocument ())) {
		int nSize = 0;

		if (str.IsEmpty ())
			pWidth->GetComboBoxCtrl ()->GetWindowText (str);

		bool bNumeric = _stscanf (str, _T ("%d"), &nSize) == 1;

		if (bNumeric) { // && nSize != m_nLastWidth) {
			int nIndex = FindFontData (-1, str, CB_FONTWIDTH);

			if (nIndex == CB_ERR) 
				nIndex = AddFontData (str, CB_FONTWIDTH);

			if (nIndex != CB_ERR) {
				pWidth->SetCurSel (nIndex);
				SetDlgItemText (CB_FONTWIDTH, str);
				//OnSelchangeFontwidth ();

				pDoc->OnFontWidthChanged (m_nLastWidth = nSize);
			}
		}
		else {
			int nIndex = pWidth->GetCurSel ();

			if (nIndex != CB_ERR) {
				int nSize = pWidth->GetItemData (nIndex);

				if (nSize != CB_ERR && nSize != m_nLastWidth) 
					pDoc->OnFontWidthChanged (m_nLastWidth = nSize);
			}
		}
	}

	m_bEditWidth = false;
}


void CFontBar::OnMore ()
{
	CRect rc;

	if (m_pwndDraftSlider)
		delete m_pwndDraftSlider;

	m_pwndDraftSlider = new CDraftSliderDlg (this);

	VERIFY (m_pwndDraftSlider->Create (IDD_DRAFT_SLIDER, this));
	m_pwndDraftSlider->ShowWindow (SW_SHOW);
	m_pwndDraftSlider->BringWindowToTop ();

	if (CWnd * p = GetDlgItem (BTN_MORE)) {
		p->GetWindowRect (rc);
		m_pwndDraftSlider->SetWindowPos (NULL, rc.left, rc.bottom, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
	}
}

LRESULT CFontBar::OnDraftSliderClosed (WPARAM wParam, LPARAM lParam)
{
	if (m_pwndDraftSlider) {
		delete m_pwndDraftSlider;
		m_pwndDraftSlider = NULL;
	}

	return 0;
}

