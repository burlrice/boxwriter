#if !defined(AFX_TASKFRAME_H__6619EEC6_5520_4952_8FEF_B13485701068__INCLUDED_)
#define AFX_TASKFRAME_H__6619EEC6_5520_4952_8FEF_B13485701068__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TaskFrame.h : header file
//

#include "MessageFrame.h"

/////////////////////////////////////////////////////////////////////////////
// CTaskFrame frame

class CTaskFrame : public CMessageFrame
{
	DECLARE_DYNCREATE(CTaskFrame)
protected:
	CTaskFrame();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:
	void SaveSettings () const;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTaskFrame)
	public:
	protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	//}}AFX_VIRTUAL

// Implementation
protected:
	CSplitterWnd m_wndSplitter [2];
	virtual ~CTaskFrame();

	// Generated message map functions
	//{{AFX_MSG(CTaskFrame)
	afx_msg void OnClose();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	//}}AFX_MSG

	afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TASKFRAME_H__6619EEC6_5520_4952_8FEF_B13485701068__INCLUDED_)
