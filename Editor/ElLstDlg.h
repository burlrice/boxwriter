#if !defined(AFX_ELLSTDLG_H__AE5AA661_5C81_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_ELLSTDLG_H__AE5AA661_5C81_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ElLstDlg.h : header file
//

#include "List.h"
#include "ListCtrlImp.h"
#include "Resource.h"
#include "ElmntLst.h"
#include "TaskView.h"
#include "TaskDoc.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CElementListDlg dialog

class CElementListDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CElementListDlg (TaskView::CElementListPtrArray & v, TaskDoc::CTaskDoc * pDoc, CWnd * pParent = NULL);   // standard constructor
	virtual ~CElementListDlg ();

// Dialog Data
	//{{AFX_DATA(CElementListDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CElementListDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	TaskView::CElementListPtrArray m_vLists;
	TaskDoc::CTaskDoc * m_pDoc;

	class CElementItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CElementItem (Element::CElement & e, CEditorElementList & list, UNITS units);
		virtual ~CElementItem ();

		virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
		virtual CString GetDispText (int nColumn) const;

		Element::CElement &		m_element;
		CEditorElementList &	m_list;
		const UNITS				m_units;
	};

	static CString GetElementData (const Element::CElement & e);
	static CString GetElementType (const Element::CElement & e);

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

	void RefreshElements ();

	// Generated message map functions
	//{{AFX_MSG(CElementListDlg)
	afx_msg void OnDelete();
	afx_msg void OnSelect();
	virtual BOOL OnInitDialog();
	afx_msg void OnDeselect();
	afx_msg void OnEditSelectall();
	//}}AFX_MSG

	afx_msg void OnDblclkListCtrl(NMHDR* pNMHDR, LRESULT* pResult);
	
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ELLSTDLG_H__AE5AA661_5C81_11D4_8FC6_006067662794__INCLUDED_)
