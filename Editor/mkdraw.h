// mkdraw.h : main header file for the MKDRAW application
//

#if !defined(AFX_MKDRAW_H__40AC74C5_3604_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_MKDRAW_H__40AC74C5_3604_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

/////////////////////////////////////////////////////////////////////////////
// CMkDrawApp:
// See mkdraw.cpp for the implementation of this class
//

#include <ddeml.h>
#include <afxdisp.h>

#include "alias.h"
#include "resource.h"       // main symbols
#include "Version.h"
#include "MkDrawDocManager.h"
#include "OdbcDatabase.h"
#include "UiStruct.h"
#include "FontBar.h"
#include "HeadBar.h"
#include "MkDrawCommandLineInfo.h"
#include "TaskView.h"

CString LoadString (UINT nID);

class CMkDrawApp : public CWinApp
{
public:
	CMkDrawApp();
	virtual ~CMkDrawApp ();
public:
	void CloseServer();
	void StartServer();
	static DWORD GetServerPID ();
	const CMkDrawDocManager & GetDocManager () const;
	bool TaskExists (const CString & str);

	static CString GetDdeAppName();

	FoxjetCommon::CVersion GetVersion() const;
	bool SetConfiningOn (bool bOn = true);
	bool IsConfiningOn () const;
	CDocument * GetActiveDocument () const;
	CView * GetActiveView () const;
	CMDIChildWnd * GetActiveFrame () const;
	CString GetMRUFilename (int nIndex) const;
	CFontBar & GetFontBar() const;
	CHeadBar & GetHeadBar() const;
	CString GetUsername () const;

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void AddToRecentFileList( LPCTSTR lpszPathName );
	void UpdateRecentFileList();
	bool IsGojo () const { return m_bGojo; }
	bool IsDemo () const { return m_bDemo; }

	FONTUISTRUCT	m_uiFont;
	CMDUISTRUCT		m_uiAlign;
	CMDUISTRUCT		m_uiFlipH;
	CMDUISTRUCT		m_uiFlipV;
	CMDUISTRUCT		m_uiInverse;
	SELUISTRUCT		m_uiSel;
	ULONG			m_lCurrentHead;

	bool					m_selHasFont;
	bool					m_selMultipleHeads;
	TaskView::SELECTIONTYPE	m_selFontSizeUnified;
	TaskView::SELECTIONTYPE	m_selFontWidthUnified;
	TaskView::SELECTIONTYPE	m_selFontNameUnified;
	TaskView::SELECTIONTYPE	m_selFontItalicUnified;
	TaskView::SELECTIONTYPE	m_selFontBoldUnified;
	TaskView::SELECTIONTYPE	m_selInverseUnified;
	TaskView::SELECTIONTYPE	m_selFlipVUnified;
	TaskView::SELECTIONTYPE	m_selFlipHUnified;
	TaskView::SELECTIONTYPE	m_selResizableUnified;
	TaskView::SELECTIONTYPE	m_selMovableUnified;

	static const LPCTSTR m_lpszSettings;
	static const LPCTSTR m_lpszOverlapped;

	void SetStatusText (LPCTSTR psz, int nPane = 0);
	CString GetStatusText (int nPane = 0);

	void BeginProgressBar (const CString & str, int nMax);
	void StepProgressBar (const CString & str);
	void EndProgressBar ();

	inline FoxjetDatabase::COdbcDatabase & GetDB ();

	bool IsTaskOpen (ULONG lTaskID) const;

	CString GetUser () const;
	void GetCmdLine (CStringArray & v);

	bool ShowNetStatDlg () const { return m_bShowNetStatDlg; }
	bool IsInitialized () { return m_bInitialized; }

	bool IsLocalDB ();
	CString GetDSN () const { return m_strDSN; }
	CString GetLocalDSN () const { return m_strDSNLocal; }

protected:

	FoxjetDatabase::COdbcDatabase m_db;
	bool m_bServer;
	int m_nProgressMax;
	int m_nProgress;
	CString m_strUser;
	CString m_strDSN;
	CString m_strDSNLocal;
	CString m_strHostAddress;
	CString m_strFlags;
	bool m_bShowNetStatDlg;
	HHOOK m_hCallWndProcRet;
	FoxjetDatabase::CInstance m_instance;
	bool m_bGojo;
	bool m_bInitialized;
	bool m_bDemo;

	static long CALLBACK CallWndProcRet (int nCode, WPARAM wParam, LPARAM lParam);

	afx_msg void OnFilePrintSetup ();
	afx_msg void OnFileBatchPrint ();

	#ifdef _DEBUG
	afx_msg void OnFileSave10000 ();
	#endif

	void LoadDesktopPrinterDefaults ();
	void DoDbIntegerityCheck ();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMkDrawApp)
	public:
	virtual BOOL InitInstance();
	virtual BOOL OnIdle(LONG lCount);
	virtual int ExitInstance();
	virtual BOOL OnDDECommand(LPTSTR lpszCommand);
	//}}AFX_VIRTUAL

	BOOL ProcessShellCommand (CMkDrawCommandLineInfo & cmdInfo);

// Implementation
	//{{AFX_MSG(CMkDrawApp)
	afx_msg void OnAppAbout();
	afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnFileNew();
	afx_msg void OnDebuggingstuffUpgradetasks();
	afx_msg void OnFileChangedsn();
	afx_msg void OnAppClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

inline FoxjetDatabase::COdbcDatabase & CMkDrawApp::GetDB ()
{
	return m_db;
}
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MKDRAW_H__40AC74C5_3604_11D4_8FC6_006067662794__INCLUDED_)
