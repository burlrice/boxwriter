#if !defined(AFX_HEADBAR_H__09BD347F_B75B_4D99_A133_B9EAA7DEA805__INCLUDED_)
#define AFX_HEADBAR_H__09BD347F_B75B_4D99_A133_B9EAA7DEA805__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HeadBar.h : header file
//

#include "ximage.h"

/////////////////////////////////////////////////////////////////////////////
// CHeadBar dialog

class CHeadBar : public CDialogBar
{
// Construction
public:
	CHeadBar();   // standard constructor
	virtual ~CHeadBar ();

	BOOL Create( CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle, UINT nID );

	void ResetContent (CDocument * pDoc = NULL);

	void SetActiveHead (ULONG lHeadID);
	void SetActivePanel (UINT nNumber);

// Dialog Data
	//{{AFX_DATA(CHeadBar)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHeadBar)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void Free ();

	CxImage m_imgPanel;
	CxImage m_imgTask;
	CxImage m_imgHead;
	CxImage m_imgHeadMaster;
	CxImage m_imgHeadDisabled;
	CBitmap m_bmpClose;
	bool	m_bSetSelection;

	// Generated message map functions
	//{{AFX_MSG(CHeadBar)
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnUpdateClose(CCmdUI* pCmdUI);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG

	afx_msg void OnUpdateHead (CCmdUI * pCmdUI);
	afx_msg void OnSelchangeHead ();

	UINT m_nPanel;
	ULONG m_lHeadID;

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEADBAR_H__09BD347F_B75B_4D99_A133_B9EAA7DEA805__INCLUDED_)
