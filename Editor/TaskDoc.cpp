// TaskDoc.cpp : implementation file
//

#include "stdafx.h"

#include <math.h>

#include "mkdraw.h"
#include "TaskDoc.h"
#include "Extern.h"
#include "Debug.h"
#include "SelectBoxDlg.h"
#include "BoxDimDlg.h"
#include "PanelDlg.h"
#include "HeadDlg.h"
#include "Compare.h"
#include "CopyArray.h"
#include "TemplExt.h"
#include "Color.h"
#include "TaskView.h"
#include "HeadView.h"
#include "RotationDlg.h"
#include "NewMessageDocType.h"
#include "TaskView.h"
#include "Coord.h"
#include "Utils.h"
#include "InvalidElementsDlg.h"
#include "TemplExt.h"
#include "MainFrm.h"
#include "NewTaskDocType.h"
#include "InkUsageDlg.h"
#include "WinMsg.h"
#include "Parse.h"
#include "HpHeadDlg.h"
#include "Registry.h"
#include "FieldDefs.h"
#include "OdbcRecordset.h"
#include "zlib.h"

#include "HeadConfigDlg.h" // TODO: rem
#include "LineConfigDlg.h" // TODO: rem

#ifdef __WINPRINTER__
	#include "BarcodeElement.h" // TODO: rem
	#include "SerialElement.h"	// TODO: rem
	#include "ExpDateElement.h"	// TODO: rem
	#include "CountElement.h"	// TODO: rem
#endif //__WINPRINTER__

#ifdef __IPPRINTER__
	#include "IpBaseElement.h"
#endif

#ifdef _DEBUG
	#include "HeadLimitedConfigDlg.h"
#endif

using namespace EditorGlobals;
using namespace FoxjetDatabase;
using namespace Foxjet3d;
using namespace FoxjetCommon;
using namespace FoxjetDocument;

using namespace Element;
using namespace Panel;
using namespace TaskDoc;
using namespace TaskView;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static CString GenRegKey ()
{
	CString str = defElements.m_strRegSection;

	str.Replace (_T ("\\Defaults"), _T ("\\Settings\\CTaskDoc"));

	return str;
}
////////////////////////////////////////////////////////////////////////////
// CTaskDoc

IMPLEMENT_DYNCREATE(CTaskDoc, CBaseDoc) 

CTaskDoc::CTaskDoc()
:	m_bInitFromHeads (false),
	m_bPasteToCrosshairs (true),
	CTaskMembers (),
	m_bMoveLocked (true), 
	m_bRubberBand (false), 
	m_bTemplate (false),
	m_strRegKey (GenRegKey ()),
	CBaseDoc (FoxjetFile::TASK)
{
	m_bMoveLocked = FoxjetDatabase::GetProfileInt (m_strRegKey, _T ("m_bMoveLocked"), m_bMoveLocked);
	m_bRubberBand = FoxjetDatabase::GetProfileInt (m_strRegKey, _T ("m_bRubberBand"), m_bRubberBand);

	SetVersion (GetElementListVersion ());
	SetUnits (defElements.GetUnits ());
}

CTaskDoc::~CTaskDoc()
{
	FoxjetElements::CBarcodeElement::ClearLocalParams ((ULONG)this);
	COdbcDatabase::PurgeDsnCache (theApp.GetDSN ());
}

bool CTaskDoc::CopyChanged (const TaskDoc::CSnapshot * pSnapshot, TaskDoc::CSnapshot * pNext)
{
	ASSERT (pSnapshot);

	const CVersion & ver = pSnapshot->m_version;
	CPairArray vChanged = pSnapshot->GetElementsChanged ();
	CPairArray vInvalidate;
	CBoundary workingHead;
	TaskView::CTaskView * pView = (TaskView::CTaskView *)GetView ();
	Box::CBoxParams & params = pView->GetParams ();
	const double dZoom = pView->GetZoom ();
	CDC dcMem;

	if (pSnapshot->GetElementChangeType () == ELEMENT_NONE && !pSnapshot->GetPropertiesChanged ())
		return false;

	dcMem.CreateCompatibleDC (pView->GetDC ());

	m_box.GetWorkingHead (workingHead);

	switch (pSnapshot->GetElementChangeType ()) {
	case ELEMENT_ADD:
		{
			CRect rcInvalid (0, 0, 0, 0);
			bool bScreen = false; // this is a hack to account for selection rect not being erased

			for (int i = 0; i < vChanged.GetSize (); i++) {
				CPair & pair = vChanged [i];
				const CBoundary & head = pair.m_pList->m_bounds;
				ULONG lHeadID = head.GetID ();
				CPairArray vErase; 
				CRect rcHead = m_box.GetHeadRect (head, params, false);
				CMessage * pMsg = m_task.GetMessageByHead (lHeadID);
				CRect rcElement = (pair.GetWindowRect (workingHead.m_card) * dZoom) + rcHead.TopLeft ();

				ASSERT (pMsg);

				int nIndex = pMsg->m_list.FindID (pair.GetElementID ());

				ASSERT (nIndex != -1);

				CElement * pElement = pMsg->m_list.GetElementPtr (nIndex);

				ASSERT (pElement);

				if (pElement->IsSelected ()) 
					bScreen = true;

				CPair pairDelete (pElement, &pMsg->m_list, &workingHead.m_card);

				vErase.Add (pairDelete);
				Erase (vErase, pView);
				rcInvalid.UnionRect (rcInvalid, rcElement);

				pMsg->m_list.RemoveAt (nIndex);
				delete pElement;

 				if (pNext) 
					pNext->SetElementsChanged (pairDelete, ELEMENT_DELETE);
			}

			rcInvalid.InflateRect (1, 1);

			if (!bScreen)
				Invalidate (rcInvalid, pView);
			else
				Invalidate (NULL, NULL); 
		}
		break;
	case ELEMENT_MODIFY:
		{
			CPairArray vErase;

			for (int i = 0; i < vChanged.GetSize (); i++) {
				CPair & pair = vChanged [i];
				const CBoundary & head = pair.m_pList->m_bounds;
				ULONG lHeadID = head.GetID ();
				CMessage * pMsg = m_task.GetMessageByHead (lHeadID);
				CRect rcInvalid (0, 0, 0, 0);
				CRect rcHead = m_box.GetHeadRect (head, params, false);
				CPoint * pptRestore = NULL;

				ASSERT (pMsg);

				int nIndex = pMsg->m_list.FindID (pair.GetElementID ());
				ASSERT (nIndex != -1);

				CElement * pElement = pMsg->m_list.GetElementPtr (nIndex);
				ASSERT (pElement);

				CPair pairPreModify (pElement, &pMsg->m_list, &workingHead.m_card);
	
				pElement->GetElement ()->FromString (pair.GetString (), ver);

				#ifdef __IPPRINTER__
				{ 
					using namespace FoxjetIpElements;

					if (CIpBaseElement * pBase = DYNAMIC_DOWNCAST (CIpBaseElement, pElement->GetElement ())) 
						pptRestore = new CPoint (pBase->GetPos (&pMsg->m_list.GetHead ()));
				}

				CPoint ptOld = pElement->GetElement ()->GetPos (NULL);
				CPoint ptOld1000ths = CBaseElement::LogicalToThousandths (ptOld, pMsg->m_list.GetHead ());
				#endif

				pElement->GetElement ()->Build ();
				pElement->GetElement ()->Draw (dcMem, pMsg->m_list.GetHead (), true);

				pElement->GetElement ()->AdjustPos (pMsg->m_list.GetHead ());

				if (pptRestore) {
					pElement->GetElement ()->SetPos (* pptRestore, &pMsg->m_list.GetHead ());
					delete pptRestore;
				}

				CPair pairPostModify (pElement, &pMsg->m_list, &workingHead.m_card);

				vErase.Add (pairPostModify);
				vErase.Add (pairPreModify);

				vInvalidate.Add (pairPostModify);

				if (pNext) 
					pNext->SetElementsChanged (pairPreModify, ELEMENT_MODIFY);
			}

			Erase (vErase, pView);
		}
		break;
	case ELEMENT_DELETE: 
		{
			for (int i = 0; i < vChanged.GetSize (); i++) {
				CPair & pair = vChanged [i];
				CElementList listTmp (false);
				CMessage * pMsg = m_task.GetMessageByHead (pair.m_pList->m_bounds.GetID ());

				ASSERT (pMsg);

				listTmp.SetHead (pMsg->m_list.GetHead ());
				listTmp.FromString (pair.GetString (), CVersion (), false);

				for (int i = 0; i < listTmp.GetSize (); i++) {
					CElement * pElement = new CElement (&listTmp [i]);
					CPair pairAdd (pElement, &pMsg->m_list, &workingHead.m_card);
					
					pElement->GetElement ()->Build ();
					pElement->GetElement ()->Draw (dcMem, pMsg->m_list.GetHead (), true);

					pMsg->m_list.Add (pElement);
					vInvalidate.Add (pairAdd);

					if (pNext) 
						pNext->SetElementsChanged (pairAdd, ELEMENT_ADD);
				}
			}
		}
		break;
	}

	if (pSnapshot->GetPropertiesChanged ()) {
		CHint hint (this, pSnapshot, NULL);

		if (pNext) 
			pNext->SetPropertiesChanged (m_task, m_box);

		m_box.Invalidate (-1);

		if (m_box.ToString (ver) != pSnapshot->m_strBox) { 
			VERIFY (m_box.FromString (pSnapshot->m_strBox, ver));
			VERIFY (hint.SetBox (m_box.GetBox ())); 
			m_box.Create (pView->m_3d);
			m_box.LoadPanels (theApp.GetDB (), m_task.m_lLineID);
		}

		VERIFY (m_task.FromString (pSnapshot->m_strTask, ver));

		UINT nPanel = HIWORD (m_task.m_lTransformation) + 1;
		ORIENTATION orient = (ORIENTATION)LOWORD (m_task.m_lTransformation);
		VERIFY (m_box.SetTransform (FoxjetDatabase::COrientation (nPanel, orient)));

		UpdateProductAreas ();

		UpdateAllViews (NULL, 0, &hint);
		pView->Invalidate ();
		NCINVALIDATE (pView);
	}

	Invalidate (vInvalidate, pView);

	return true;
}

void CTaskDoc::TraceVectors ()
{
#ifdef _DEBUG
	for (int i = m_vUndo.GetSize () - 1; i >= 0; i--) {
		const CSnapshot * p = (const CSnapshot *)m_vUndo [i];
		CString str;

		str.Format (_T ("m_vUndo [%d]: %s"), i, p->m_strBox);
		TRACEF (str);
	}
	for (int i = m_vRedo.GetSize () - 1; i >= 0; i--) {
		const CSnapshot * p = (const CSnapshot *)m_vRedo [i];
		CString str;

		str.Format (_T ("m_vRedo [%d]: %s"), i, p->m_strBox);
		TRACEF (str);
	}
#endif
}

#ifdef _DEBUG
void CTaskDoc::OnEditUndo()
{
	TraceVectors (); TRACEF (_T (""));
	CBaseDoc::OnEditUndo ();
	TraceVectors (); TRACEF (_T (""));
}

void CTaskDoc::OnEditRedo()
{
	TraceVectors (); TRACEF (_T (""));
	CBaseDoc::OnEditRedo ();
	TraceVectors (); TRACEF (_T (""));
}
#endif //_DEBUG

FoxjetDocument::CBaseDoc & CTaskDoc::Copy (const FoxjetDocument::CSnapshot * pSnapshot,
										   FoxjetDocument::CSnapshot * pNext)
{
	BeginWaitCursor ();
	ASSERT (pSnapshot);

	const TaskDoc::CSnapshot * p = (TaskDoc::CSnapshot *)pSnapshot;
	const CVersion & ver = p->m_version;
	const CTaskMembers & src = * (CTaskMembers *)pSnapshot;
	const CTaskMembers & dest = GetMembers ();
	CString strBox;

	if (!CopyChanged ((TaskDoc::CSnapshot *)pSnapshot, (TaskDoc::CSnapshot *)pNext) && (src != dest)) {
		CHint hint (this, p, NULL);
		// CTaskMembers::operator = (* p); // intentionally not called

		if (m_box.ToString (ver) != p->m_strBox) { 
			// TODO: should 'hint' be created after this?

			VERIFY (m_box.FromString (p->m_strBox, ver));
			VERIFY (hint.SetBox (m_box.GetBox ())); 
		}

		VERIFY (m_task.FromString (p->m_strTask, ver));

		UINT nPanel = HIWORD (m_task.m_lTransformation) + 1;
		ORIENTATION orient = (ORIENTATION)LOWORD (m_task.m_lTransformation);
		VERIFY (m_box.SetTransform (FoxjetDatabase::COrientation (nPanel, orient)));

		for (int i = 0; i < p->m_vMsgs.GetSize (); i++) {
			const TaskDoc::MESSAGEPAIRSTRUCT & msg = p->m_vMsgs [i];
			ULONG lHeadID = msg.m_lHeadID;
			CMessage * pMsg = m_task.GetMessageByHead (lHeadID);

			ASSERT (pMsg);

			pMsg->m_list.DeleteAllElements ();
			pMsg->m_list.FromString (msg.m_str, pMsg->m_list.GetProductArea (0), 
				GetUnits (), pMsg->m_list.m_bounds, CVersion ());
		}

		CBaseDoc::Copy (p, pNext);
		UpdateProductAreas ();
		m_box.Invalidate (-1);

		UpdateAllViews (NULL, 0, &hint);

		if (CTaskView * pView = (CTaskView *)GetView ()) {
			pView->Invalidate ();
			NCINVALIDATE (pView);
		}
	}

	UpdateUIStructs ();
	EndWaitCursor ();

	return * this;
}

BOOL CTaskDoc::OnNewDocument()
{
	if (!CBaseDoc::OnNewDocument())
		return FALSE;
	
	CString str = GetTitle ();

	m_task.m_strName = FoxjetFile::GetFileTitle (str);
	m_bInitFromHeads = true;
	UpdateProductAreas ();
	UpdateUIStructs ();

	theApp.AddToRecentFileList (str);

	return TRUE;
}

BOOL CTaskDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	TRACEF (_T ("CTaskDoc::OnOpenDocument: ") + CString (lpszPathName));

	BOOL bResult = CBaseDoc::OnOpenDocument (lpszPathName);

	TRACEF (ToString (bResult));

	if (bResult) {
		if (CTaskView * pView = (CTaskView *)GetView ()) {
			theApp.GetFontBar ().ResetContent (pView->GetActiveHead ());
			theApp.GetHeadBar ().ResetContent (this);
		}

		{
			using namespace FoxjetElements;

			for (int nSymbology = kSymbologyFirst; nSymbology <= kSymbologyLast; nSymbology++) {
				if (CBarcodeElement::IsValidSymbology (nSymbology)) {
					CBarcodeParams param = GetDefParam (nSymbology, 1, GRAPHICS_768_256, 150, EditorGlobals::database);
					CBarcodeParams b = param;

					b.SetID (GetParamsPerSymbology ());
					CBarcodeElement::SetLocalParam ((ULONG)this, nSymbology, b);
				}
			}

			for (int nMsg = 0; nMsg < m_task.GetMessageCount (); nMsg++) {
				if (CMessage * pMsg = m_task.GetMessage (nMsg)) {
					CEditorElementList & list = pMsg->m_list;
					CStringArray v;

					ParsePackets (list.m_strLocalParams, v);

					for (int i = 0; i < v.GetSize (); i++) {
						CBarcodeParams b;

						b.FromString (v [i], ::verApp);
						b.SetID (GetParamsPerSymbology ());
						TRACEF (b.ToString (::verApp));
						CBarcodeElement::SetLocalParam ((ULONG)this, b.GetSymbology (), b);
					}
				}
			}
		}
	}

	Element::CPairArray vConfine = Confine ();

	m_box.Invalidate (-1);

	if (vConfine.GetSize ()) 
		SetModifiedFlag (true, NULL);

	//COdbcDatabase::PurgeDsnCache (theApp.GetDSN ());

	TRACEF (_T ("CTaskDoc::OnOpenDocument exiting"));

	return bResult;
}

BEGIN_MESSAGE_MAP(CTaskDoc, CBaseDoc)
	//{{AFX_MSG_MAP(CTaskDoc)
	ON_COMMAND(ID_BOX_CHANGE, OnBoxChange)
	ON_UPDATE_COMMAND_UI(ID_BOX_CHANGE, OnUpdateBoxChange)
	ON_COMMAND(ID_BOX_PROPERTIES, OnBoxProperties)
	ON_COMMAND(ID_PANEL_PROPERTIES, OnPanelProperties)
	ON_COMMAND(ID_HEAD_PROPERTIES, OnHeadProperties)
	ON_COMMAND(ID_FILE_PROPERTIES, OnFileProperties)
	ON_COMMAND(ID_DEBUGGINGSTUFF_CONFINING, OnDebuggingstuffConfining)
	ON_UPDATE_COMMAND_UI(ID_DEBUGGINGSTUFF_CONFINING, OnUpdateDebuggingstuffConfining)
	ON_COMMAND(ID_DEBUGGINGSTUFF_DRAWINPRINTERCONTEXT, OnDebuggingstuffDrawinprintercontext)
	ON_COMMAND(ID_DEBUGGINGSTUFF_EXCEPTIONS, OnDebuggingstuffExceptions)
	ON_COMMAND(ID_TOOLS_INKUSAGE, OnToolsInkusage)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_VIEW_TRANSPARENT, OnViewTransparent)
	ON_COMMAND(ID_VIEW_SHOWHEADBOUNDARIES, OnViewShowheadboundaries)
	ON_COMMAND(ID_VIEW_SHOWDIRECTION, OnViewShowdirection)
	ON_COMMAND(ID_VIEW_CHANGEPERSPECTIVE, OnViewChangeperspective)
	ON_COMMAND(ID_ROTATE_ORIENTATION_CCL, OnRotateOrientationCcl)
	ON_COMMAND(ID_ROTATE_ORIENTATION_CL, OnRotateOrientationCl)
	ON_COMMAND(ID_ROTATE_VIEW_DOWN, OnRotateViewDown)
	ON_COMMAND(ID_ROTATE_VIEW_LEFT, OnRotateViewLeft)
	ON_COMMAND(ID_ROTATE_VIEW_RIGHT, OnRotateViewRight)
	ON_COMMAND(ID_ROTATE_VIEW_UP, OnRotateViewUp)
	ON_UPDATE_COMMAND_UI(ID_VIEW_TRANSPARENT, OnUpdateViewTransparent)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SHOWHEADBOUNDARIES, OnUpdateViewShowheadboundaries)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SHOWDIRECTION, OnUpdateViewShowdirection)
	ON_COMMAND(ID_CONFINE, OnConfine)
	
	ON_COMMAND(ID_VIEW_ZOOMIN, OnViewZoomin)
	ON_COMMAND(ID_VIEW_ZOOMOUT, OnViewZoomout)
	ON_COMMAND(ID_VIEW_ZOOMNORMAL100, OnViewZoomnormal100)
	ON_COMMAND(ID_VIEW_ZOOM_MAX, OnViewZoomMax)
	ON_COMMAND(ID_VIEW_ZOOM_FITTOSCREEN, OnViewZoomMax)
	ON_COMMAND(ID_VIEW_ZOOM_CUSTOM, OnViewZoomCustom)

	ON_COMMAND(ID_ALIGN_BOTTOM, OnAlignBottom)
	ON_COMMAND(ID_ALIGN_HCENTER, OnAlignHcenter)
	ON_COMMAND(ID_ALIGN_LEFT, OnAlignLeft)
	ON_COMMAND(ID_ALIGN_RIGHT, OnAlignRight)
	ON_COMMAND(ID_ALIGN_TOP, OnAlignTop)
	ON_COMMAND(ID_ALIGN_VCENTER, OnAlignVcenter)
	ON_COMMAND(ID_FONT_BOLD, OnFontBold)
	ON_COMMAND(ID_FONT_ITALIC, OnFontItalic)
	ON_COMMAND(ID_ALIGN_PRODCENTER, OnAlignProdcenter)
	ON_COMMAND(ID_ALIGN_INVERSE, OnAlignInverse)
	ON_COMMAND(ID_ALIGN_FLIPHORZ, OnAlignFliphorz)
	ON_COMMAND(ID_ALIGN_FLIPVERT, OnAlignFlipvert)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_COMMAND(ID_EDIT_SELECTALL, OnEditSelectall)
	ON_COMMAND(ID_VIEW_SHOWCROSSHAIRS, OnViewShowCrosshairs)
	ON_COMMAND(ID_ALIGN_VDISTRIBUTE, OnAlignVDistribute)
	ON_COMMAND(ID_ALIGN_HDISTRIBUTE, OnAlignHDistribute)
	ON_COMMAND(ID_DEBUGGINGSTUFF_HEADCONFIG, OnDebugHeadconfig)
	ON_COMMAND(ID_DEBUGGINGSTUFF_LINECONFIG, OnDebugLineconfig)
	ON_COMMAND(ID_CREATE_CAPTION, OnCreateCaption)
	ON_COMMAND(ID_ALIGN_LOCKPOS, OnAlignLockMove)
	ON_COMMAND(ID_ALIGN_RUBBERBAND, OnAlignRubberband)

	ON_UPDATE_COMMAND_UI(ID_ALIGN_VDISTRIBUTE, OnUpdateAlignVDistribute)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_HDISTRIBUTE, OnUpdateAlignHDistribute)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SHOWCROSSHAIRS, OnUpdateViewShowCrosshairs)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_FLIPHORZ, OnUpdateAlignFliphorz)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_PRODCENTER, OnUpdateAlignProdcenter)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_FLIPVERT, OnUpdateAlignFlipvert)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_INVERSE, OnUpdateAlignInverse)
	ON_UPDATE_COMMAND_UI(ID_FONT_BOLD, OnUpdateFontBold)
	ON_UPDATE_COMMAND_UI(ID_FONT_ITALIC, OnUpdateFontItalic)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_SELECTALL, OnUpdateEditSelectall)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_BOTTOM, UpdateAlignmentUI)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_HCENTER, UpdateAlignmentUI)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_LEFT, UpdateAlignmentUI)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_RIGHT, UpdateAlignmentUI)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_TOP, UpdateAlignmentUI)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_VCENTER, UpdateAlignmentUI)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_LOCKPOS, OnUpdateAlignLockMove)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_RUBBERBAND, OnUpdateAlignRubberband)

	ON_COMMAND_RANGE(ID_ELEMENT_01, ID_ELEMENT_30, OnApiElementCommand)
	ON_UPDATE_COMMAND_UI_RANGE(ID_ELEMENT_01, ID_ELEMENT_30, OnUpdateApiElementCommand)
	/*
	note: for whatever reason, ON_UPDATE_COMMAND_UI doesn't work 
	with a context menu, so these have to do the same thing as the 
	InitMenu (CMenu *) fct.
	*/

	ON_COMMAND(ID_ELEMENT_EDIT, OnElementEdit)
	ON_COMMAND(ID_ELEMENTS_DELETE, OnElementsDelete)
	ON_COMMAND(ID_ELEMENTS_LIST, OnElementsList)
	ON_COMMAND(ID_PROPERTIES_MOVABLE, OnPropertiesMovable)
	ON_COMMAND(ID_PROPERTIES_RESIZABLE, OnPropertiesResizable)
	ON_COMMAND(ID_PROPERTIES_FLIPH, OnPropertiesFliph)
	ON_COMMAND(ID_PROPERTIES_FLIPV, OnPropertiesFlipv)
	ON_COMMAND(ID_PROPERTIES_INVERSE, OnPropertiesInverse)

	ON_UPDATE_COMMAND_UI(ID_ELEMENT_EDIT, OnUpdateElementsEdit)
	ON_UPDATE_COMMAND_UI(ID_ELEMENTS_DELETE, OnUpdateElementsDelete)

	ON_COMMAND(ID_VIEW_RULERS_HORIZONAL, OnViewRulersHorizonal)
	ON_COMMAND(ID_VIEW_RULERS_VERTICAL, OnViewRulersVertical)
	ON_UPDATE_COMMAND_UI(ID_VIEW_RULERS_HORIZONAL, OnUpdateViewRulersHorizonal)
	ON_UPDATE_COMMAND_UI(ID_VIEW_RULERS_VERTICAL, OnUpdateViewRulersVertical)

#ifdef _DEBUG
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_COMMAND(ID_EDIT_REDO, OnEditRedo)
#endif

	ON_COMMAND(ID_PROPERTIES_SENDTOFRONT, OnBringToFront)
	ON_COMMAND(ID_PROPERTIES_SENDTOBACK, OnSendToBack)
	ON_COMMAND(ID_PROPERTIES_BRINGFORWARD, OnBringForward)
	ON_COMMAND(ID_PROPERTIES_SENDBACKWARD, OnSendBackward)

	ON_COMMAND(ID_FILE_SAVETOFILE, OnSaveToFile) 
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVETOFILE, OnUpdateSaveToFile)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTaskDoc diagnostics


#ifdef _DEBUG
void CTaskDoc::AssertValid() const
{
	CBaseDoc::AssertValid();
}

void CTaskDoc::Dump(CDumpContext& dc) const
{
	CBaseDoc::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTaskDoc serialization

void CTaskDoc::Serialize(CArchive& ar)
{
	CBaseDoc::Serialize (ar);
}

/////////////////////////////////////////////////////////////////////////////
// CTaskDoc commands
void CTaskDoc::DeleteContents()
{
	CBaseDoc::DeleteContents ();

	m_box.Free ();
}

bool CTaskDoc::SetLine (FoxjetDatabase::LINESTRUCT & line)
{ 
	CArray <HEADSTRUCT, HEADSTRUCT &> v;

	m_box.SetLine (line);
	m_task.m_lLineID = line.m_lID;

	bool bGetHeads = GetLineHeads (line.m_lID, v);

	if (!bGetHeads) {
		#ifdef _DEBUG
			CString str;
			str.Format (_T ("Line [%s] has no heads\n"), line.m_strName);
			TRACEF (str);
		#endif //_DEBUG

		return false;
	}

	return true; 
}

bool CTaskDoc::SetBox (const FoxjetDatabase::BOXSTRUCT & box, bool bUndoable)
{ 
	m_box.SetBox (box);
	m_task.m_lBoxID = m_box.GetBox ().m_lID;

	if (!bUndoable) {
		// changes to the box can't be undone by the document
		for (int i = 0; i < m_vUndo.GetSize (); i++) {
			CSnapshot * p = (CSnapshot *)m_vUndo [i];

			ASSERT (p);
			p->m_strBox = m_box.ToString (m_version);
			p->m_box = m_box;
		}

		for (int i = 0; i < m_vRedo.GetSize (); i++) {
			CSnapshot * p = (CSnapshot *)m_vRedo [i];

			ASSERT (p);
			p->m_strBox = m_box.ToString (m_version);
			p->m_box = m_box;
		}
	}

	if (CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, GetView ())) {
		Box::CBoxParams & p = pView->GetParams ();

		p.m_lChange |= DxParams::ZOOM; // force update
		pView->Invalidate ();
		pView->SetScrollSizes (MM_TEXT, pView->GetPageSize ());
	}

	UpdateProductAreas ();
	return true;
}

const Foxjet3d::Box::CBox & CTaskDoc::GetBox () const 
{ 
	return m_box; 
}

FoxjetDocument::CSnapshot * CTaskDoc::GetSnapshot()
{
	return new TaskDoc::CSnapshot (* this);
}

void CTaskDoc::SetModifiedFlag (bool bModified, const FoxjetDocument::CSnapshot * pPreModify)
{
	if (FoxjetDocument::CSnapshot * pNonConst = (FoxjetDocument::CSnapshot *)(LPVOID)pPreModify)
		if (TaskDoc::CSnapshot * p = dynamic_cast <TaskDoc::CSnapshot *> (pNonConst))
			p->UpdateLinkedSlaves (* this);

//	if (pPreModify) TRACESNAPSHOT (* (CTaskMembers *)pPreModify);

	CBaseDoc::SetModifiedFlag (bModified, pPreModify);
	UpdateUIStructs ();
}

void CTaskDoc::SetModifiedFlag (const FoxjetDocument::CSnapshot * pPreModify)
{
//	if (pPreModify) TRACESNAPSHOT (* (CTaskMembers *)pPreModify);

	CTaskDoc::SetModifiedFlag (true, pPreModify); //CBaseDoc::SetModifiedFlag (pPreModify);
}

bool CTaskDoc::CheckElementCount () const
{
	CBoundaryArray v, vMax;

	GetHeads (v);

	for (int i = 0; i < v.GetSize (); i++) {
		CBoundary & h = v [i];

		if (const CMessage * pMsg = m_task.GetMessageByHead (h.GetID ())) 
			if (pMsg->m_list.IsMaxedOut ()) 
				vMax.Add (CBoundary (h));
	}

	if (vMax.GetSize ()) {
		CString str;

		str.Format (LoadString (IDS_TASKMAXELEMENTS), CElementList::GetMaxElements ());

		for (int i = 0; i < vMax.GetSize (); i++)
			str += vMax [i].GetName () + _T ("\n");

		MsgBox (str, MB_ICONWARNING);

		return false;
	}

	return true;
}

bool CTaskDoc::HasOverlappingElements () const
{
	CTaskView * pView = (CTaskView *)GetView ();
	Box::CBoxParams & params = pView->GetParams ();
	CBoundary workingHead;

	m_box.GetWorkingHead (params, workingHead);

	for (int nPanel = 1; nPanel <= 6; nPanel++) {
		const CPanel * pPanel = NULL;

		VERIFY (m_box.GetPanel (nPanel, &pPanel));
		ASSERT (pPanel);

		for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) {
			const CBoundary & head = pPanel->m_vBounds [i];
			CMessage * pMsg = params.m_task.GetMessageByHead (head.GetID ());

			ASSERT (pMsg); 

			for (int j = 0; j < pMsg->m_list.GetSize (); j++) {
				CEditorElementList & list = pMsg->m_list;
				CElement & e = list [j];
				Element::CPairArray vOverlap = list.GetOverlapping (&e, workingHead);

				if (vOverlap.GetSize ()) 
					return true;
			}
		}
	}

	return false;
}

bool CTaskDoc::StoreToFile(LPCTSTR pszName)
{
	bool bResult = false;
	CString strLine = FoxjetFile::GetFileHead (pszName);
	CString strTask = FoxjetFile::GetFileTitle (pszName);
	ULONG lLineID = FoxjetCommon::GetLineID (strLine);

	TRACEF (ToString (GetMembers ().m_task.m_lID) + _T (": ") + GetMembers ().m_task.m_strName);

	BeginWaitCursor ();
	FoxjetDatabase::DoBackup (database);

	ASSERT (FoxjetCommon::IsValidFileTitle (strTask));

	bool bModified = IsModified () ? true : false;

	if (IsTemplate ()) {
		CString str = LoadString (IDS_TEMPLATE);

		if (MsgBox (str, MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
			return false;
	}

	UpdateProductAreas ();

	if (!CheckElementCount ())
		return false;

	if (defElements.GetOverlappedWarning () && HasOverlappingElements ()) {
		CString str = LoadString (IDS_OVERLAPPPEDELEMENTS);

		if (MsgBox (str, MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
			return false;
	}

	if (lLineID != -1) {
		const FoxjetDatabase::COrientation transform = m_box.GetTransform ();
		const ULONG lPrinterID = GetMasterPrinterID (theApp.GetDB ());
		const ULONG lSavedID = m_task.m_lID; 
		const ULONG lExistingID = GetTaskID (database, strTask, strLine, lPrinterID);

		//ASSERT (GetMembers ().m_task.m_lID != lExistingID);

		// different file name or task doesn't exist yet
		// -1 will cause m_task.Save to call AddTaskRecord 
		// instead of UpdateTaskRecord
		if (lExistingID == NOTFOUND) 
			m_task.m_lID = -1;
		else if (lExistingID != m_task.m_lID) {
			// write over
			VERIFY (DeleteTaskRecord (database, lExistingID));
			m_task.m_lID = -1;
		}
		
		for (int i = 0; i < m_task.m_vMsgs.GetSize (); i++) 
			m_task.m_vMsgs [i].m_lID = -1; 

		m_task.m_lLineID = lLineID;
		m_task.m_lBoxID = m_box.GetBox ().m_lID;
		m_task.m_strName = strTask;
		m_task.m_lTransformation = MAKELPARAM (transform.m_orient, transform.m_nPanel - 1);

		if (CMessage * pMsg0 = m_task.GetMessage (0)) {
			using namespace FoxjetElements;

			for (int nMsg = 0; nMsg < m_task.GetMessageCount (); nMsg++) {
				if (CMessage * pMsg = m_task.GetMessage (nMsg)) {
					CEditorElementList & list = pMsg->m_list;

					for (int i = 0; i < list.GetSize (); i++) {
						if (CBarcodeElement * pBarcode = DYNAMIC_DOWNCAST (CBarcodeElement, list.GetElementPtr (i)->GetElement ())) {
							if (pBarcode->GetMagnification () == GetParamsPerSymbology ()) {
								CBarcodeParams param = CBarcodeElement::GetLocalParam ((ULONG)this, pBarcode->GetSymbology ());
								CString str = param.ToString (::verApp);

								if (list.m_strLocalParams.Find (str) == -1)
									list.m_strLocalParams += str;
							}
						}
					}
				}
			}
		}

		if (!(bResult = m_task.Save (database, GetElementListVersion ()))) 
			m_task.m_lID = lSavedID;
		else {
			if (lExistingID == NOTFOUND)
				SetPathName (GetPathName (), TRUE); // add to MRU

			if (IsNetworked ())
			{
				CString str = ::GetCommandLine ();

				str.MakeLower ();

				if (str.Find (_T ("/dbcheck")) != -1) {
					try {
						CString str;
						COdbcRecordset rst (database);

						str.Format (_T ("SELECT [%s] FROM [%s] WHERE [%s]='%s' AND NOT [%s]=%d"),
							Tasks::m_lpszID,
							Tasks::m_lpszTable,
							Tasks::m_lpszName,
							m_task.m_strName,
							Tasks::m_lpszID,
							m_task.m_lID);
						rst.Open (str);

						if (!rst.IsEOF () && MsgBox (LoadString (IDS_DELETEDUPLICATES), MB_ICONEXCLAMATION | MB_YESNO, 0) == IDYES) {
							for (; !rst.IsEOF (); rst.MoveNext ()) {
								ULONG lID = (ULONG)(long)rst.GetFieldValue ((short)0);

								DeleteTaskRecord (database, lID);
							}
						}
					}
					catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
					catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
				}
			}

			if (bModified || lExistingID == NOTFOUND) {
				REPORTSTRUCT r;

				r.m_dtTime = COleDateTime::GetCurrentTime ();
				r.m_strTaskName = strTask;
				r.m_strLine = strLine;
				r.m_strUsername = theApp.GetUsername ();

				VERIFY (AddReportRecord (theApp.GetDB (), bModified ? REPORT_TASK_MODIFY : REPORT_TASK_CREATE, r, GetPrinterID (theApp.GetDB ())));
			}

			/*
			{
				DWORD dw;
			
				::SendMessageTimeout (HWND_BROADCAST, WM_TASKUPDATED, m_task.m_lID, 0, 
					SMTO_NORMAL, 0, &dw);
			}
			*/
		}

		{
			SETTINGSSTRUCT s;
			CStringArray v;
			bool bUpdate = false;

			GetSettingsRecord (theApp.GetDB (), lLineID, _T ("Templates"), s);
			FromString (s.m_strData, v);
			int nIndex = Find (v, strTask, false);
			
			if (m_bTemplate) {
				if (nIndex == -1) {
					InsertAscending (v, strTask);
					bUpdate = true;
				}
			}
			else {
				if (nIndex != -1) {
					v.RemoveAt (nIndex);
					bUpdate = true;
				}
			}

			if (bUpdate) {
				s.m_strKey = _T ("Templates");
				s.m_lLineID = lLineID;
				s.m_strData = ToString (v);

				if (!UpdateSettingsRecord (theApp.GetDB (), s))
					VERIFY (AddSettingsRecord (theApp.GetDB (), s));
			}
		}
	}

	if (bResult) {
		DWORD dw = 0;
		
		::SendMessageTimeout (HWND_BROADCAST, WM_EDITOR_TASK_CHANGE, 0, 0, SMTO_NORMAL | SMTO_ABORTIFHUNG, 1, &dw);
	}

	EndWaitCursor ();

	return bResult;
}

bool CTaskDoc::Parse (Foxjet3d::CTask & task, const FoxjetCommon::CVersion & ver, bool bValidate)
{
	using namespace InvalidElementsDlg;

	CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;
	CArray <CItem, CItem &> vErrors;
	CArray <CItem, CItem &> * pvErrors = &vErrors;
	CTaskView * pView = (CTaskView *)GetView ();
	FoxjetDatabase::BOXSTRUCT box; 
	FoxjetDatabase::LINESTRUCT line;
	CDC dcMem;
	int nProgressMax = 0;
	const CString strLoading = LoadString (IDS_LOADING);
	CString strProgress;
	bool bModifiedAtLoad = false;

	ASSERT (pView);

	Box::CBoxParams & params = pView->GetParams ();
	const double dZoom = pView->GetZoom ();

	bool bGetLine = GetLineRecord (task.m_lLineID, line);
	ASSERT (bGetLine);
	m_box.SetLine (line);

	if (!GetPanelRecords (database, task.m_lLineID, vPanels)) {
		CString str;

		str.Format (LoadString (IDS_TASKLOADPANELSMISSING), 
			task.m_strName, line.m_strName);
		MsgBox (str);

		return false;
	}

	dcMem.CreateCompatibleDC (GetDC ());	

	m_task.SetVersion (GetElementListVersion ());

	if (GetBoxRecord (database, task.m_lBoxID, box)) {
		m_box.SetBox (box);
	}
	else {
		CString str;

		str.Format (LoadString (IDS_TASKLOADBOXMISSING), 
			task.m_strName, task.m_lBoxID);
		MsgBox (str);

		return false;
	}

	UINT nPanel = HIWORD (task.m_lTransformation) + 1;
	ORIENTATION orient = (ORIENTATION)LOWORD (task.m_lTransformation);
	bool bSetTransform = m_box.SetTransform (FoxjetDatabase::COrientation (nPanel, orient));
	ASSERT (bSetTransform);

	UpdateProductAreas ();

	m_box.Create (pView->m_3d);
	m_box.LoadPanels (theApp.GetDB (), task.m_lLineID);

	if (bValidate) { // TODO: rem ?
	}

	pvErrors = NULL; // TODO: rem ?

	int nLoad = task.Load (m_box, task, theApp.GetDB (), CVersion (), pvErrors, ProgressFct, &bModifiedAtLoad);

	for (int i = 0; i < task.GetMessageCount (); i++) {
		if (const CMessage * p = task.GetMessage (i)) {
			if (p->m_list.IsNEXT ()) {
				MsgBox (LoadString (IDS_ISNEXTTASK), LoadString (IDS_ERROR));
				return false;
			}
		}
	}

	if (!nLoad)
		return false;
	else if (nLoad > 1) {
		SetModifiedFlag (true, NULL);
		UpdateAllViews (NULL, 0, NULL);
	}

	if (vErrors.GetSize ()) {
		int nResult = MsgBox (LoadString (IDS_INVALIDELEMENTS), 
			LoadString (IDS_ERROR), MB_YESNOCANCEL | MB_ICONEXCLAMATION);

		if (nResult == IDCANCEL)
			return false;

		if (nResult == IDYES) {
			CInvalidElementsDlg dlg (::AfxGetMainWnd ());
			LINESTRUCT line;

			GetLineRecord (task.m_lLineID, line);

			dlg.m_strLine = line.m_strName;
			dlg.m_strTask = task.m_strName;
			::Copy (dlg.m_vErrors, vErrors);

			if (dlg.DoModal () == IDCANCEL)
				return false;
		}
	}

	if (bModifiedAtLoad) 
		SetModifiedFlag (true, NULL);

	theApp.GetDB ().ClearStatus ();

	return true;
}

void CTaskDoc::ProgressFct (int nType, const CString & str, int nMax)
{
	switch (nType) {
	case PROGRESS_SET:
		theApp.BeginProgressBar (str, nMax);
		break;
	case PROGRESS_STEP:
		theApp.StepProgressBar (str);
		break;
	case PROGRESS_END:
		theApp.EndProgressBar ();
		break;
	}
}

bool CTaskDoc::LoadFromFile(LPCTSTR pszName, FoxjetCommon::CVersion & ver)
{
	//if (IsGojo ()) {
	//	if (MsgBox (LoadString (IDS_PROEDITORONLY), MB_OKCANCEL | MB_ICONINFORMATION | MB_DEFBUTTON2) == IDCANCEL)
	//		return false;
	//}

LARGE_INTEGER start = STARTTRACECOUNT (); 

	CString strLine = FoxjetFile::GetFileHead (pszName);
	CString strTask = FoxjetFile::GetFileTitle (pszName);
	TASKSTRUCT task;
	CVersion verActual (ver);
	bool bDifferentVersion = false;
	ULONG lMasterID = GetMasterPrinterID (theApp.GetDB ());
	ULONG lPrinterID = GetPrinterID (theApp.GetDB ());

	ver = GetElementListVersion ();

	if (!GetTaskRecord (database, strTask, strLine, lMasterID, task)) 
		return false;
	else {		
		Remap (database, task, lPrinterID, lMasterID);

		for (int i = 0; i < task.m_vMsgs.GetSize () && !bDifferentVersion; i++) {
			CStringArray v;

			ParsePackets (task.m_vMsgs [i].m_strData, v);

			for (int j = 0; j < v.GetSize (); j++) {
				CStringArray vPacket;
				CString str = v [j];

				Tokenize (str, vPacket);

				if (vPacket.GetSize () && !vPacket [0].CompareNoCase (_T ("Ver"))) {
					CVersion verParse;

					if (verParse.FromString (str)) {
						if (!ver.IsSynonym (verParse)) {
							bDifferentVersion = true;
							break;
						}
						else
							verActual = verParse;
					}
				}
			}
		}

		if (bDifferentVersion) {
			if (MsgBox (LoadString (IDS_OPENDIFFERENTVERSION), MB_OKCANCEL) == IDCANCEL)
				return false;
		}

		((TASKSTRUCT &)m_task) = task;

TRACECOUNT (start); start = STARTTRACECOUNT (); 
/* TODO: rem
		// don't bother with a validity check if opening a different version
		if (!bDifferentVersion && !Parse (m_task, verActual))
			return false;
*/
		if (!Parse (m_task, verActual, bDifferentVersion))
			return false;
TRACECOUNT (start); start = STARTTRACECOUNT (); 

		{
			CString str = ::GetCommandLine ();
			str.MakeUpper ();
			bool bLocal = IsNetworked () && theApp.IsLocalDB () || str.Find (_T ("/LOCALDB")) != -1;
		
			if (bLocal) {
				SetReadOnly (true);
				MsgBox (LoadString (IDS_LOCALDBONLY), MB_ICONINFORMATION);
			}
		}

		m_task.SetVersion (ver);
		SetVersion (ver);

		{ // to set 'file properties'
			for (int i = 0; i < task.m_vMsgs.GetSize (); i++) {
				CString str = task.m_vMsgs [i].m_strData;

				m_task.UpdateUserDefined (task.m_vMsgs [i]);
			}
		}

		//Element::CPairArray vConfine = Confine ();

		//m_box.Invalidate (-1);

		//if (vConfine.GetSize ()) 
		//	SetModifiedFlag (true, NULL);
	}

	{
		for (int nMsg = 0; nMsg < m_task.m_vMsgs.GetSize (); nMsg++) {
			if (CMessage * pMsg = m_task.GetMessage (nMsg)) {
				CEditorElementList & list = pMsg->m_list;

				for (int nElement = 0; nElement < list.GetSize (); nElement++) {
					CElement & e = list [nElement];

					e.GetElement ()->SetLocalParamUID ((ULONG)this);
				}
			}
		}
	}

	#ifdef __WINPRINTER__
	{
		CStringArray vFonts, vMissing;
	
		FoxjetCommon::CHead::GetWinFontNames (vFonts);

		if (ISVALVE ()) 
			FoxjetCommon::GetBitmappedFonts (vFonts);

		for (int nMsg = 0; nMsg < m_task.m_vMsgs.GetSize (); nMsg++) {
			if (CMessage * pMsg = m_task.GetMessage (nMsg)) {
				CEditorElementList & list = pMsg->m_list;

				for (int nElement = 0; nElement < list.GetSize (); nElement++) {
					CElement & e = list [nElement];
					CString strFont = e.GetFontName ();

					if (strFont.GetLength () && Find (vFonts, strFont, false) == -1) {
						if (Find (vMissing, strFont, false) == -1) {
							vMissing.Add (strFont);
						}
					}
				}
			}
		}

		if (vMissing.GetSize ()) {
			CString str = LoadString (IDS_MISSINGFONTS);

			for (int i = 0; i < vMissing.GetSize (); i++)
				str += _T ("\r\n") + vMissing [i];

			TRACEF (str);
			MsgBox (str);
		}
	}
	#endif

	m_bTemplate = IsTemplate (GetLineID (), m_task.m_strName);

	return true;
}

bool CTaskDoc::IsTemplate (ULONG lLineID, const CString & strTask)
{
	bool bResult = false;
	SETTINGSSTRUCT s;
	CStringArray v;

	GetSettingsRecord (theApp.GetDB (), lLineID, _T ("Templates"), s);
	FromString (s.m_strData, v);

	return Find (v, strTask, false) != -1 ? true : false;
}

void CTaskDoc::OnBoxChange() 
{
	CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();
	ULONG lLineID = m_task.m_lLineID; 
	Foxjet3d::CSelectBoxDlg dlg (database, GetUnits (), lLineID, ::AfxGetMainWnd ());
	CTaskView * pView = (CTaskView *)GetView ();

	ASSERT (pView);
	dlg.m_vSelection.Add (m_box.GetBox ().m_lID);

	if (dlg.DoModal () == IDOK) {
		BOXSTRUCT boxNew, boxOld = m_box.GetBox ();

		BeginWaitCursor ();
		pSnapshot->SetPropertiesChanged (m_task, m_box);

		ASSERT (dlg.m_vSelection.GetSize ());
		ULONG lID = dlg.m_vSelection [0];

		bool bGetBox = GetBoxRecord (database, lID, boxNew);
		ASSERT (bGetBox);

		bool bSetBox = SetBox (boxNew);
		ASSERT (bSetBox);
	
		const Element::CPairArray vPreConfine = Confine (false);
		const Element::CPairArray vConfine = Confine ();

		pSnapshot->SetElementsChanged (vPreConfine, ELEMENT_MODIFY);

		if (!CheckState (&vConfine)) { 
			Copy (pSnapshot);
			UpdateAllViews (NULL, 0, NULL);
			delete pSnapshot;
		}
		else {
			TraceVectors ();
			SetModifiedFlag (pSnapshot);
			TraceVectors ();
			pView->Invalidate ();
			UpdateAllViews (NULL, 0, &CHint (&GetMembers (), pSnapshot, NULL));
		}

		EndWaitCursor ();
	}
	else
		delete pSnapshot;
}

void CTaskDoc::OnUpdateBoxChange(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable (!GetReadOnly ());
}

void CTaskDoc::OnBoxProperties() 
{
	Foxjet3d::CBoxDimDlg dlg (theApp.GetDB (), ::AfxGetMainWnd ());
	
	dlg.m_lLineID = GetLineID ();
	dlg.m_box = m_box.GetBox ();
	dlg.m_bReadOnly = GetReadOnly ();
	dlg.m_bPreview = theApp.GetProfileInt (_T ("Settings\\CBoxDimDlg"), _T ("Preview"), 0) ? true : false;
	
	if (dlg.DoModal () == IDOK) {
		BeginWaitCursor ();

		BOXSTRUCT newBox, oldBox = m_box.GetBox ();
		CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();

		pSnapshot->SetPropertiesChanged (m_task, m_box);
		VERIFY (GetBoxRecord (database, m_box.GetBox ().m_lID, newBox));
		VERIFY (SetBox (newBox, false));

		const Element::CPairArray vPreConfine = Confine (false);
		const Element::CPairArray vConfine = Confine ();
		
		pSnapshot->SetElementsChanged (vPreConfine, ELEMENT_MODIFY);

		if (!CheckState (&vConfine)) {
			pSnapshot->SetPropertiesChanged (m_task, m_box);

			VERIFY (UpdateBoxRecord (database, oldBox));
			VERIFY (SetBox (oldBox, false));
			Copy (pSnapshot);
			
			delete pSnapshot;
		}
		else {
			if (vConfine.GetSize ()) 
				SetModifiedFlag (pSnapshot);
			else
				delete pSnapshot;
		}

		VERIFY (SetBox (m_box.GetBox (), false)); 
		UpdateAllViews (NULL, 0, NULL);
		theApp.WriteProfileInt (_T ("Settings\\CBoxDimDlg"), _T ("Preview"), dlg.m_bPreview);

		EndWaitCursor ();
	}
}

void CTaskDoc::OnPanelProperties() 
{
	using namespace Foxjet3d;
	const PANELSTRUCT * pPanels [6];

	if (m_box.GetPanels (pPanels)) {
		CTaskView * pView = (CTaskView *)GetView ();
		int nIndex = pView->GetActivePanel () - 1;
		ASSERT (nIndex >= 0 && nIndex < 6);
		const FoxjetDatabase::PANELSTRUCT & p = * pPanels [nIndex];

		CPanelDlg dlg (database, p, ::AfxGetMainWnd ());
		dlg.m_bReadOnly = true;
		dlg.DoModal ();
	}
}

void CTaskDoc::OnHeadProperties() 
{
	CTaskView * pView = (CTaskView *)GetView ();
	CBoundaryArray v;

	GetHeads (v);

	for (int i = 0; i < v.GetSize (); i++) {
		if (v [i].GetID () == pView->GetActiveHead ()) {
			HEADSTRUCT & h = v [i];

			if (IsHpHead (h.m_nHeadType)) {
				Foxjet3d::CHpHeadDlg dlg (database, h, defElements.GetUnits (), CMapStringToString (), ::AfxGetMainWnd ());
				dlg.m_bReadOnly = true;

				dlg.DoModal ();
			}
			else {
				if (h.IsUSB ()) {
					Foxjet3d::CHeadDlg dlg (database, h, defElements.GetUnits (), CMapStringToString (), false, ::AfxGetMainWnd ());
					dlg.m_bReadOnly = true;
					dlg.DoModal ();
				}
				else {
					Foxjet3d::CHeadPhcDlg dlg (database, h, defElements.GetUnits (), CMapStringToString (), false, ::AfxGetMainWnd ());
					dlg.m_bReadOnly = true;
					dlg.DoModal ();
				}
			}

			return;
		}
	}
}

void CTaskDoc::OnDebugHeadconfig ()
{
	CArray <LINESTRUCT, LINESTRUCT &> v;

	GetPrinterLines (theApp.GetDB (), GetPrinterID (theApp.GetDB ()), v); //GetLineRecords (theApp.GetDB (), v);

	if (v.GetSize ()) {
		Foxjet3d::HeadConfigDlg::CHeadConfigDlg dlg (theApp.GetDB (), defElements.GetUnits (), v [0].m_lID, false, GetView ());
		//Foxjet3d::CHeadLimitedConfigDlg dlg (theApp.GetDB (), defElements.GetUnits (), -1, GetView ());

		BEGIN_TRANS (theApp.GetDB ());

		int nResult = dlg.DoModal ();

		/*
		for (int i = 0; i < dlg.m_vAdded.GetSize (); i++) {
			CString str;
			
			str.Format ("add: %d", dlg.m_vAdded [i]);
			TRACEF (str);
		}

		for (i = 0; i < dlg.m_vEdited.GetSize (); i++) {
			CString str;
			
			str.Format ("edit: %d", dlg.m_vEdited [i]);
			TRACEF (str);
		}

		for (i = 0; i < dlg.m_vDeleted.GetSize (); i++) {
			CString str;
			
			str.Format ("delete: %d", dlg.m_vDeleted [i]);
			TRACEF (str);
		}
		*/

		/*
		#ifdef _DEBUG
		if (nResult == IDOK) {
			COMMIT_TRANS (theApp.GetDB ());
			return;
		}
		#endif
		*/

		ROLLBACK_TRANS (theApp.GetDB ());
	}
}

void CTaskDoc::OnDebugLineconfig ()
{
	Foxjet3d::LineConfigDlg::CLineConfigDlg dlg (theApp.GetDB (), GetView ());

	BEGIN_TRANS (theApp.GetDB ());

	dlg.DoModal ();

	ROLLBACK_TRANS (theApp.GetDB ());
}

void CTaskDoc::OnConfine() 
{
	Confine ();
}

Element::CPairArray CTaskDoc::Confine (bool bSetPos) 
{
	BeginWaitCursor ();

	Element::CPairArray vConfine;
	CDC & dc = * GetDC ();
	bool bConfining = theApp.IsConfiningOn ();
	CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();
	CTaskView * pView = (CTaskView *)GetView ();
	CRect rcInvalid (0, 0, 0, 0);
	CBoundary workingHead;

	ASSERT (pView);

	double dZoom = pView->GetZoom ();
	Box::CBoxParams & p = pView->GetParams ();
	int nPanel = p.m_nPanel;
	ULONG lHeadID = p.m_lHeadID;

	theApp.SetConfiningOn (true);
	m_box.GetWorkingHead (p, workingHead);


	for (int i = 0; i < m_task.GetMessageCount (); i++) {
		CMessage * pMsg = m_task.GetMessage (i);
		const CBoundary & head = pMsg->m_list.m_bounds;

		p.m_nPanel = m_box.GetPanelNumber (head);
		p.m_lHeadID = head.GetID ();
		m_box.Invalidate (-1); // clear rect cache

		const CRect rcHead [] = 
		{
			m_box.GetHeadRect (head, p, true),
			m_box.GetHeadRect (head, p, false),
		};

		for (int j = 0; j < pMsg->m_list.GetSize (); j++) {
			const CBoundary & head = pMsg->m_list.m_bounds;
			CElement & e = pMsg->m_list.GetElement (j);
			Element::CPair pair (&e, &pMsg->m_list, &workingHead.m_card);
			const CPoint pt = e.GetElement ()->GetPos (&head.m_card);

			if (e.Confine (dc, rcHead, dZoom, head, workingHead)) 
				vConfine.Add (pair);

			if (!bSetPos) 
				e.GetElement ()->SetPos (pt, &head.m_card);
		}
	}

	p.m_nPanel = nPanel;
	p.m_lHeadID = lHeadID;

	if (vConfine.GetSize ()) 
		Invalidate (vConfine, pView);

	delete pSnapshot;
	theApp.SetConfiningOn (bConfining);

	EndWaitCursor ();

	return vConfine;
}

CView * CTaskDoc::GetView (VIEWTYPE type, int) const
{
	const CRuntimeClass * pFirstClass = NULL;
	const CFrameWnd * pFrame = theApp.GetActiveFrame ();
	int nFrame = 1, nCount = 0;

	if (pFrame)
		nCount = pFrame->m_nWindow;

	for (POSITION pos = GetFirstViewPosition (); pos != NULL; ) {
		CView * pView = GetNextView (pos);

		if (!pFirstClass)
			pFirstClass = pView->GetRuntimeClass ();
		else {
			const CString strClass = a2w (pView->GetRuntimeClass ()->m_lpszClassName);

			if (!strClass.CompareNoCase (a2w (pFirstClass->m_lpszClassName)))
				nFrame++;
		}

		if (!nCount || nFrame == nCount) {
			switch (type) {
			case TASK:
				if (pView->IsKindOf (RUNTIME_CLASS (CTaskView)))
					return pView;
				break;
			case HEAD:
				if (pView->IsKindOf (RUNTIME_CLASS (HeadView::CHeadView)))
					return pView;
				break;
			}
		}
	}

	return NULL;
}

CView * CTaskDoc::GetView (VIEWTYPE type) const
{
	CView * pView = GetView (type, 0);

	// ASSERT (pView);
	
	return pView;
}

void CTaskDoc::OnViewTransparent() 
{
	CTaskView & view = * (CTaskView *)GetView ();
	Box::CBoxParams & p = view.GetParams ();
	p.m_uBlend = p.m_uBlend == 255 ? 100 : 255;
	view.SaveState ();
	m_box.Invalidate (-1);
	view.RedrawWindow ();
}

void CTaskDoc::OnUpdateViewTransparent(CCmdUI* pCmdUI) 
{
	CTaskView & view = * (CTaskView *)GetView ();
	Box::CBoxParams & p = view.GetParams ();
	pCmdUI->SetCheck (p.m_uBlend != 255 ? 1 : 0);
	pCmdUI->Enable (false); // TODO
}

void CTaskDoc::OnViewChangeperspective() 
{
	CTaskView & view = * (CTaskView *)GetView ();
	CRotationDlg dlg (&view);
	Box::CBoxParams & p = view.GetParams ();

	dlg.m_dXAxis = p.m_dRotation [0];
	dlg.m_dYAxis = p.m_dRotation [1];

	if (dlg.DoModal () == IDOK) {
		p.m_dRotation [0] = dlg.m_dXAxis;
		p.m_dRotation [1] = dlg.m_dYAxis;
		view.SaveState ();
//		m_box.Invalidate ();
		view.RedrawWindow ();

		// causes the WM_NCCALCSIZE message to be sent
		view.SetWindowPos (NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER);
	}
}

void CTaskDoc::OnRotateOrientationCcl() 
{
	OnRotateOrientation (Foxjet3d::Box::CCL);
}

void CTaskDoc::OnRotateOrientationCl() 
{
	OnRotateOrientation (Foxjet3d::Box::CL);
}

void CTaskDoc::OnRotateViewDown() 
{
	OnRotateOrientation (Foxjet3d::Box::DOWN);
}

void CTaskDoc::OnRotateViewUp() 
{
	OnRotateOrientation (Foxjet3d::Box::UP);
}

void CTaskDoc::OnRotateViewLeft() 
{
	OnRotateOrientation (Foxjet3d::Box::LEFT);
}

void CTaskDoc::OnRotateViewRight() 
{
	OnRotateOrientation (Foxjet3d::Box::RIGHT);
}

void CTaskDoc::OnRotateOrientation(Foxjet3d::Box::ROTATION r) 
{
	BeginWaitCursor ();

	CTaskView & view = * (CTaskView *)GetView ();
	CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();
	Box::CBoxParams & p = view.GetParams ();
	UINT nFace = p.m_nPanel;

	if (m_box.RotateTransform (nFace, r)) {
		const FoxjetDatabase::COrientation transform = m_box.GetTransform ();

		pSnapshot->SetPropertiesChanged (m_task, m_box);

		m_task.m_lTransformation = MAKELPARAM (transform.m_orient, transform.m_nPanel - 1);
		UpdateProductAreas ();

		const Element::CPairArray vPreConfine = Confine (false);
		const Element::CPairArray vConfine = Confine ();

		pSnapshot->SetElementsChanged (vPreConfine);

		view.SaveState ();
		view.RedrawWindow ();

		if (!CheckState (&vConfine)) { 
			Copy (pSnapshot);
			delete pSnapshot;
		}
		else {
			UpdateAllViews (NULL, 0, &CHint (&GetMembers (), pSnapshot, NULL));
			SetModifiedFlag (pSnapshot);
		}
	}
	else
		delete pSnapshot;

	EndWaitCursor ();
}

void CTaskDoc::OnViewShowheadboundaries() 
{
	CTaskView & view = * (CTaskView *)GetView ();
	Box::CBoxParams & p = view.GetParams ();
	
	view.SaveState ();
	p.m_bHeads = !p.m_bHeads;
	Invalidate (NULL);
}

void CTaskDoc::OnUpdateViewShowheadboundaries(CCmdUI* pCmdUI) 
{
	CTaskView & view = * (CTaskView *)GetView ();
	Box::CBoxParams & p = view.GetParams ();
	pCmdUI->SetCheck (p.m_bHeads ? 1 : 0);
}

void CTaskDoc::OnViewShowdirection() 
{
	CTaskView & view = * (CTaskView *)GetView ();
	Box::CBoxParams & p = view.GetParams ();

	p.m_bDirection = !p.m_bDirection;
	view.SaveState ();
	Invalidate (NULL);
}

void CTaskDoc::OnUpdateViewShowdirection(CCmdUI* pCmdUI) 
{
	CTaskView & view = * (CTaskView *)GetView ();
	Box::CBoxParams & p = view.GetParams ();
	pCmdUI->SetCheck (p.m_bDirection ? 1 : 0);
	pCmdUI->Enable (false); // TODO
}

void CTaskDoc::OnViewShowCrosshairs() 
{
	CTaskView & view = * (CTaskView *)GetView ();
	Box::CBoxParams & p = view.GetParams ();

	p.m_crosshairs.m_bShow = !p.m_crosshairs.m_bShow;
	view.SaveState ();
	Invalidate (NULL);
}

void CTaskDoc::OnUpdateViewShowCrosshairs(CCmdUI* pCmdUI) 
{
	CTaskView & view = * (CTaskView *)GetView ();
	Box::CBoxParams & p = view.GetParams ();
	pCmdUI->SetCheck (p.m_crosshairs.m_bShow ? 1 : 0);
}

void CTaskDoc::OnViewZoomFittoscreen() 
{
	CTaskView & view = * (CTaskView *)GetView ();
	CRect rc;
//	const CTaskMembers & doc = GetDocument ()->GetMembers ();
//	ULONG lSize = max (max (doc.m_box.m_lHeight, doc.m_box.m_lWidth), doc.m_box.m_lLength);
//	CSize size = CBaseElement::ThousandthsToLogical (CPoint (lSize, 0));
	CSize size = view.GetPageSize ();

	view.GetClientRect (rc);

	double dZoom [2] = 
	{
		(double)rc.Width () / (double)size.cx,
		(double)rc.Height () / (double)size.cx,
	};

	view.SetZoom (min (dZoom [0], dZoom [1]) * 0.80, true);
}

CSize CTaskDoc::GetProductArea (const CEditorElementList & list) const
{
	CTaskView * pView = (CTaskView *)GetView ();
	double dZoom = pView->GetZoom ();
	Box::CBoxParams & p = pView->GetParams ();
	CSize result = m_box.GetHeadRect (list.m_bounds, p).Size ();

	result.cx = DivideAndRound (result.cx, dZoom);
	result.cy = DivideAndRound (result.cy, dZoom);

	//CString str; str.Format (_T ("result: %d, %d"), result.cx, result.cy); TRACEF (str);

	return result;
}

void CTaskDoc::UpdateProductAreas()
{
	CTaskView * pView = (CTaskView *)GetView ();
	ASSERT (pView);
	Box::CBoxParams & p = pView->GetParams ();
	const CPanel * pPanel = NULL;
	double dZoom = pView->GetZoom ();
	CDC dcMem;
	bool bUpdate = false;

	dcMem.CreateCompatibleDC (GetDC ());

	if (m_box.GetPanel (p.m_nPanel, &pPanel)) {
		for (int i = 0; i < m_task.GetMessageCount (); i++) {
			CMessage * pMsg = m_task.GetMessage (i);

			ASSERT (pMsg);

			CEditorElementList & list = pMsg->m_list;
			const CBoundary & head = list.m_bounds;

			// only update heads on this panel
			if (head.GetPanelID () == pPanel->GetMembers ().m_lID) {
				CRect rcHead = m_box.GetHeadRect (head, p);

				list.SetProductArea (rcHead);
			}
		}
	}

	if (!m_box.IsCreated ())
		return;

	for (int nPanel = 1; nPanel <= 6; nPanel++) {
		FoxjetDatabase::COrientation boxOrient = m_box.GetTransform ();
		FoxjetDatabase::COrientation orient = m_box.MapPanel (nPanel, boxOrient);
		const CPanel * pRelative = NULL;
		const CPanel * pAbs = NULL;
	
		VERIFY (m_box.GetPanel (orient.m_nPanel, &pRelative));
		VERIFY (m_box.GetPanel (nPanel, &pAbs));

		for (int i = 0; i < m_task.GetMessageCount (); i++) {
			CMessage * pMsg = m_task.GetMessage (i);
			
			ASSERT (pMsg);

			CEditorElementList & list = pMsg->m_list;
			const CBoundary & head = list.m_bounds;

			// only update heads on this panel
			if (head.GetPanelID () == pAbs->GetMembers ().m_lID) {
				const CSize sizeLogical = pRelative->GetSize (head, orient.m_orient, m_box.GetBox (), pRelative->GetMembers ());
				const CSize sizePanel = CBaseElement::LogicalToThousandths (sizeLogical, head);
				CSize sizePA = CBaseElement::LogicalToThousandths (CSize (sizeLogical.cx, head.Height ()), head);

				list.CElementList::SetProductArea (sizePA, sizePanel);
			}
		}
	}
}

void CTaskDoc::OnViewZoomin()
{
	if (CTaskView * p = DYNAMIC_DOWNCAST (CTaskView, GetView ()))
		p->OnViewZoomin ();
}

void CTaskDoc::OnViewZoomout()
{
	if (CTaskView * p = DYNAMIC_DOWNCAST (CTaskView, GetView ()))
		p->OnViewZoomout ();
}

void CTaskDoc::OnViewZoomnormal100()
{
	if (CTaskView * p = DYNAMIC_DOWNCAST (CTaskView, GetView ()))
		p->OnViewZoomnormal100 ();
}

void CTaskDoc::OnViewZoomMax()
{
	if (CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, GetView ())) {
		CRect rcClient;

		pView->GetClientRect (&rcClient);
		pView->SetZoom (1.0); // CalcViewSize is dependant on zoom

		CSize size = m_box.CalcViewSize (pView->GetParams ());

		double dRatio [] = 
		{
			(double)rcClient.Width () / (double)size.cx,
			(double)rcClient.Height () / (double)size.cy,
		};
		double dZoom = min (dRatio [0], dRatio [1]);

		pView->SetZoom (dZoom, true);
	}
}

void CTaskDoc::OnViewZoomCustom()
{
	if (CTaskView * p = DYNAMIC_DOWNCAST (CTaskView, GetView ()))
		p->OnViewZoomCustom();
}

void CTaskDoc::OnUpdateZoom (CCmdUI * pCmdUI)
{
	if (CTaskView * p = DYNAMIC_DOWNCAST (CTaskView, GetView ()))
		p->OnUpdateZoom (pCmdUI);
}

void CTaskDoc::OnFileProperties() 
{
	CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();
	BOXSTRUCT box = m_box.GetBox ();
	TASKSTRUCT task = m_task.GetTaskStruct ();

	// make sure task.m_vMsgs is up to date
	task.m_vMsgs.RemoveAll (); 

	for (int i = 0; i < m_task.GetMessageCount (); i++) {
		MESSAGESTRUCT msg = * m_task.GetMessageStruct (i);
		CEditorElementList & list = m_task.GetMessage (i)->m_list;

		msg.m_strData = list.CElementList::ToString (list.GetVersion ());
		task.m_vMsgs.Add (msg);
	}

	if (OnProperties (task, box, ::AfxGetMainWnd (), GetReadOnly ())) {
		CBoundaryArray v;

		BeginWaitCursor ();

		GetHeads (v);
		m_task.m_strDesc		= task.m_strDesc;
		m_task.m_strDownload	= task.m_strDownload;
		m_task.m_strNotes		= task.m_strNotes;

		pSnapshot->SetPropertiesChanged (m_task, m_box); 

		VERIFY (GetBoxRecord (database, box.m_lID, box));
		VERIFY (SetBox (box));
		
		for (int i = 0; i < task.m_vMsgs.GetSize (); i++) {
			CString str = task.m_vMsgs [i].m_strData;

			VERIFY (m_task.UpdateUserDefined (task.m_vMsgs [i]));
		}

		for (int nHead = 0; nHead < v.GetSize (); nHead++) {
			if (CMessage * pMsg = m_task.GetMessageByHead (v [nHead].GetID ())) {
				for (int nElement = 0; nElement < pMsg->m_list.GetSize (); nElement++) {
					CElement & e = pMsg->m_list [nElement];

					e.GetElement ()->SetRedraw (true);
				}
			}
		}

		const Element::CPairArray vPreConfine = Confine (false);
		const Element::CPairArray vConfine = Confine ();

		pSnapshot->SetElementsChanged (vPreConfine);

		if (!CheckState (&vConfine)) { 
			Copy (pSnapshot);
			delete pSnapshot;
		}
		else {
			UpdateAllViews (NULL, 0, &CHint (&GetMembers (), pSnapshot, NULL));
			SetModifiedFlag (pSnapshot);
		}

		EndWaitCursor ();
	}
	else {
		delete pSnapshot;
	}
}

void CTaskDoc::GetMessages (CArray <Foxjet3d::CMessage *, Foxjet3d::CMessage *> & v, UINT nPanel)
{
	const CPanel * pPanel = NULL;
	CBoundaryArray vHeads;

	GetHeads (vHeads);

	v.RemoveAll (); 
	VERIFY (m_box.GetPanel (nPanel, &pPanel));

	for (int i = 0; i < vHeads.GetSize (); i++) {
		if (vHeads [i].GetPanelID () == pPanel->GetMembers ().m_lID) {
			CMessage * pMsg = m_task.GetMessageByHead (vHeads [i].GetID ());

			ASSERT (pMsg);
			v.Add (pMsg);
		}
	}
}

void CTaskDoc::OnAlignTop()
{
	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	HEADSTRUCT head;

	// not safe to do this operation unless all elements are on the same head
	if (!pView->GetSelHeadUnified (head))
		return;

	BeginWaitCursor ();

	CPairArray sel = pView->GetSelected (0);
	CRect rcInvalid (0, 0, 0, 0);
	CBoundary workingHead;

	m_box.GetWorkingHead (pView->GetParams (), workingHead);

	if (sel.GetSize ()) {
		CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();
		int i, nTop = sel [0].m_pElement->GetWindowRect (head, workingHead).top;
		
		pSnapshot->SetElementsChanged (sel);

		for (i = 0; i < sel.GetSize (); i++) {
			nTop = min (nTop, sel [i].m_pElement->GetWindowRect (head, workingHead).top);
		}

		for (i = 0; i < sel.GetSize (); i++) {
			CElement & e = sel [i];
			CPoint ptOffset (0, nTop - e.GetWindowRect (head, workingHead).top);

			e.SetYPos (e.GetPos (head) + ptOffset, head);
		}

		Invalidate (sel, pView);
		SetModifiedFlag (pSnapshot);
	}

	EndWaitCursor ();
}

void CTaskDoc::OnAlignVcenter() 
{
	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	HEADSTRUCT head;

	// not safe to do this operation unless all elements are on the same head
	if (!pView->GetSelHeadUnified (head))
		return;

	BeginWaitCursor ();

	CPairArray sel = pView->GetSelected (0);
	CBoundary workingHead;

	m_box.GetWorkingHead (pView->GetParams (), workingHead);

	if (sel.GetSize ()) {
		int i;
		CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();
		CRect rcMax = sel [0].m_pElement->GetWindowRect (head, workingHead);
		CRect rcInvalid (0, 0, 0, 0);

		pSnapshot->SetElementsChanged (sel);

		for (i = 0; i < sel.GetSize (); i++) {
			CRect rc = sel [i].m_pElement->GetWindowRect (head, workingHead);
			
			if (rc.Width () > rcMax.Width ())
				rcMax = rc;
		}

		int nMiddle = rcMax.CenterPoint ().x;

		for (i = 0; i < sel.GetSize (); i++) {
			CElement & e = sel [i];
			CPoint ptOffset (nMiddle - e.GetWindowRect (head, workingHead).CenterPoint ().x, 0);
			double dXStretch = 1.0 / CElement::CalcXStretch (head, workingHead);

			ptOffset.x = (int)((double)ptOffset.x * dXStretch);
			e.SetXPos (e.GetPos (head) + ptOffset, head);
		}

		Invalidate (sel, pView);
		SetModifiedFlag (pSnapshot);
	}

	EndWaitCursor ();
}

void CTaskDoc::OnAlignBottom() 
{
	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	HEADSTRUCT head;

	// not safe to do this operation unless all elements are on the same head
	if (!pView->GetSelHeadUnified (head))
		return;

	BeginWaitCursor ();

	CPairArray sel = pView->GetSelected (0);
	CBoundary workingHead;

	m_box.GetWorkingHead (pView->GetParams (), workingHead);

	if (sel.GetSize ()) {
		CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();
		int i, nBottom = sel [0].m_pElement->GetWindowRect (head, workingHead).bottom;
		CRect rcInvalid (0, 0, 0, 0);

		pSnapshot->SetElementsChanged (sel);

		for (i = 0; i < sel.GetSize (); i++) {
			nBottom = max (nBottom, sel [i].m_pElement->GetWindowRect (head, workingHead).bottom);
		}

		for (i = 0; i < sel.GetSize (); i++) {
			CElement & e = sel [i];
			CPoint ptOffset (0, nBottom - e.GetWindowRect (head, workingHead).bottom);

			e.SetYPos (e.GetPos (head) + ptOffset, head);
		}

		Invalidate (sel, pView);
		SetModifiedFlag (pSnapshot);
	}

	EndWaitCursor ();
}

void CTaskDoc::OnAlignHcenter() 
{
	BeginWaitCursor (); 

	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	CPairArray sel = pView->GetSelected (0);
	CBoundary workingHead;

	m_box.GetWorkingHead (pView->GetParams (), workingHead);
	
	if (sel.GetSize ()) {
		CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();
		int i;
		CRect rcMax = sel [0].m_pElement->GetWindowRect (sel [0].m_pList->GetHead (), workingHead);
		CRect rcInvalid (0, 0, 0, 0);

		pSnapshot->SetElementsChanged (sel);

		for (i = 0; i < sel.GetSize (); i++) {
			CRect rc = sel [i].m_pElement->GetWindowRect (sel [i].m_pList->GetHead (), workingHead);
			
			if (rc.Height () > rcMax.Height ())
				rcMax = rc;
		}

		int nMiddle = rcMax.CenterPoint ().y;

		for (i = 0; i < sel.GetSize (); i++) {
			CElement & e = * (sel [i].m_pElement);
			const FoxjetDatabase::HEADSTRUCT & head = sel [i].m_pList->GetHead ();
			CPoint ptOffset (0, nMiddle - e.GetWindowRect (head, workingHead).CenterPoint ().y);

			e.SetYPos (e.GetPos (head) + ptOffset, head);
		}

		Invalidate (sel, pView);
		SetModifiedFlag (pSnapshot);
	}

	EndWaitCursor ();
}

void CTaskDoc::OnAlignLeft()
{
	BeginWaitCursor ();

	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	CPairArray sel = pView->GetSelected (0);
	CBoundary workingHead;

	m_box.GetWorkingHead (pView->GetParams (), workingHead);
	
	if (sel.GetSize ()) {
		CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();
		int i, nLeft = sel [0].m_pElement->GetWindowRect (sel [0].m_pList->GetHead (), workingHead).left;
		CRect rcInvalid (0, 0, 0, 0);

		pSnapshot->SetElementsChanged (sel);

		for (i = 0; i < sel.GetSize (); i++)
			nLeft = min (nLeft, sel [i].m_pElement->GetWindowRect (sel [i].m_pList->GetHead (), workingHead).left);

		for (i = 0; i < sel.GetSize (); i++) {
			CElement & e = * sel [i].m_pElement;
			const FoxjetDatabase::HEADSTRUCT & head = sel [i].m_pList->GetHead ();
			CPoint ptOffset (nLeft - e.GetWindowRect (head, workingHead).left, 0);
			double dXStretch = 1.0 / CElement::CalcXStretch (head, workingHead);
			CPoint pt = e.GetPos (head);

			ptOffset.x = (int)((double)ptOffset.x * dXStretch);
			e.SetXPos (pt + ptOffset, head);
		}

		Invalidate (sel, pView);
		SetModifiedFlag (pSnapshot);
	}

	EndWaitCursor ();
}

void CTaskDoc::OnAlignRight() 
{
	BeginWaitCursor ();

	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	CPairArray sel = pView->GetSelected (0);
	CBoundary workingHead;

	m_box.GetWorkingHead (pView->GetParams (), workingHead);
	
	if (sel.GetSize ()) {
		CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();
		int i, nRight = sel [0].m_pElement->GetWindowRect (sel [0].m_pList->GetHead (), workingHead).right;
		CRect rcInvalid (0, 0, 0, 0);

		pSnapshot->SetElementsChanged (sel);

		for (i = 0; i < sel.GetSize (); i++) 
			nRight = max (nRight, sel [i].m_pElement->GetWindowRect (sel [i].m_pList->GetHead (), workingHead).right);

		for (i = 0; i < sel.GetSize (); i++) {
			CElement & e = * sel [i].m_pElement;
			const FoxjetDatabase::HEADSTRUCT & head = sel [i].m_pList->GetHead ();
			CPoint ptOffset (nRight - e.GetWindowRect (head, workingHead).right, 0);
			double dXStretch = 1.0 / CElement::CalcXStretch (head, workingHead);
			CPoint pt = e.GetPos (head);

			ptOffset.x = (int)((double)ptOffset.x * dXStretch);
			e.SetXPos (pt + ptOffset, head);
		}

		Invalidate (sel, pView);
		SetModifiedFlag (pSnapshot);
	}

	EndWaitCursor ();
}

void CTaskDoc::InsertAscendingXPos (Element::CPairArray & v, const Element::CPair & p) 
{
	const CPoint ptElement = p.m_pElement->GetPos (p.m_pList->GetHead ());

	for (int i = 0; i < v.GetSize (); i++) {
		const CEditorElementList & list = v [i];
		const CElement & e = v [i];
		const HEADSTRUCT & head = list.GetHead ();
		const CPoint pt = e.GetPos (head);

		if (pt.x > ptElement.x) {
			v.InsertAt (i, Element::CPair (p));
			return;
		}
	}

	v.Add (Element::CPair (p));
}

void CTaskDoc::InsertAscendingYPos (Element::CPairArray & v, const Element::CPair & p) 
{
	const CPoint ptElement = p.m_pElement->GetPos (p.m_pList->GetHead ());

	for (int i = 0; i < v.GetSize (); i++) {
		const CEditorElementList & list = v [i];
		const CElement & e = v [i];
		const HEADSTRUCT & head = list.GetHead ();
		const CPoint pt = e.GetPos (head);

		if (pt.y > ptElement.y) {
			v.InsertAt (i, Element::CPair (p));
			return;
		}
	}

	v.Add (Element::CPair (p));
}

Element::CPairArray CTaskDoc::SortByXPos (const Element::CPairArray & v) 
{
	Element::CPairArray vResult;

	for (int i = 0; i < v.GetSize (); i++) 
		InsertAscendingXPos (vResult, v [i]);

	return vResult;
}

Element::CPairArray CTaskDoc::SortByYPos (const Element::CPairArray & v) 
{
	Element::CPairArray vResult;

	for (int i = 0; i < v.GetSize (); i++) 
		InsertAscendingYPos (vResult, v [i]);

	return vResult;
}

void CTaskDoc::OnAlignVDistribute ()
{
	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	HEADSTRUCT head;

	// not safe to do this operation unless all elements are on the same head
	if (!pView->GetSelHeadUnified (head))
		return;

	BeginWaitCursor (); 

	CPairArray sel = pView->GetSelected (0);
	CBoundary workingHead;

	m_box.GetWorkingHead (pView->GetParams (), workingHead);

	if (sel.GetSize () > 2) {
		CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();

		sel = SortByYPos (sel);

		ASSERT (sel.GetSize () > 2);

		pSnapshot->SetElementsChanged (sel);

		const int nCount = sel.GetSize ();
		const CPoint ptTop = sel [0].m_pElement->GetWindowRect (sel [0].m_pList->GetHead (), workingHead).CenterPoint ();
		const CPoint ptBottom = sel [nCount - 1].m_pElement->GetWindowRect (sel [nCount - 1].m_pList->GetHead (), workingHead).CenterPoint ();
		const int nDistance = ptBottom.y - ptTop.y;
		const int nStep = nDistance / (nCount - 1);
		int y = ptTop.y + nStep;
		CRect rcInvalid (0, 0, 0, 0);

		for (int i = 1; i < sel.GetSize () - 1; i++) {
			const HEADSTRUCT & head = sel [i].m_pList->GetHead ();
			const CRect rc = sel [i].m_pElement->GetWindowRect (head, workingHead);
			const CPoint pt = rc.TopLeft ();
			const CPoint ptCenter = rc.CenterPoint ();
			const int nOffset = ptCenter.y - pt.y;
			
			sel [i].m_pElement->SetYPos (CPoint (pt.x, y - nOffset), head);

			y += nStep;
		}

		Invalidate (sel, pView);
		SetModifiedFlag (pSnapshot);
	}

	EndWaitCursor ();
}

void CTaskDoc::OnAlignHDistribute ()
{
	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	HEADSTRUCT head;

	// not safe to do this operation unless all elements are on the same head
	if (!pView->GetSelHeadUnified (head))
		return;

	BeginWaitCursor ();

	CPairArray sel = pView->GetSelected (0);
	CBoundary workingHead;

	m_box.GetWorkingHead (pView->GetParams (), workingHead);

	if (sel.GetSize () > 2) {
		CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();

		sel = SortByXPos (sel);

		ASSERT (sel.GetSize () > 2);

		pSnapshot->SetElementsChanged (sel);

		const int nCount = sel.GetSize ();
		const CPoint ptLeft = sel [0].m_pElement->GetWindowRect (sel [0].m_pList->GetHead (), workingHead).CenterPoint ();
		const CPoint ptRight = sel [nCount - 1].m_pElement->GetWindowRect (sel [nCount - 1].m_pList->GetHead (), workingHead).CenterPoint ();
		const int nDistance = ptRight.x - ptLeft.x;
		double dXStretch = 1.0 / CElement::CalcXStretch (sel [0].m_pList->GetHead (), workingHead);
		const int nStep = (int)((double)nDistance * dXStretch) / (nCount - 1);
		int x = ptLeft.x + nStep;
		CRect rcInvalid (0, 0, 0, 0);

		for (int i = 1; i < sel.GetSize () - 1; i++) {
			CElement & e = sel [i];
			const HEADSTRUCT & head = sel [i].m_pList->GetHead ();
			const CRect rc = e.GetWindowRect (head, workingHead);
			const CPoint pt = rc.TopLeft ();
			const CPoint ptCenter = rc.CenterPoint ();
			const int nOffset = ptCenter.x - pt.x;

			e.SetXPos (CPoint (x - nOffset, pt.y), head);

			x += nStep;
		}

		Invalidate (sel, pView);
		SetModifiedFlag (pSnapshot);
	}

	EndWaitCursor ();
}

void CTaskDoc::OnUpdateAlignVDistribute (CCmdUI * pCmdUI)
{
	pCmdUI->Enable (theApp.m_uiSel.m_nSelCount > 2 && !GetReadOnly ());	
}

void CTaskDoc::OnUpdateAlignHDistribute (CCmdUI * pCmdUI)
{
	pCmdUI->Enable (theApp.m_uiSel.m_nSelCount > 2 && !GetReadOnly ());	
}


void CTaskDoc::OnFontBold()
{
/* TODO: rem
const LARGE_INTEGER total = STARTTRACECOUNT (); 
LARGE_INTEGER start = STARTTRACECOUNT (); 

TRACECOUNT (start); start = STARTTRACECOUNT (); 
*/
	BeginWaitCursor ();

	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	const CPoint ptScroll = pView->GetScrollPosition ();
	CPairArray sel = pView->GetSelected (0);
	bool bBold = false;
	CSnapshot * pSnapshot = NULL;
	double dZoom = pView->GetZoom ();
	CArray <CRect, CRect> vrcInvalid; 
	CBoundary workingHead;

	m_box.GetWorkingHead (workingHead);
	
	if (sel.GetSize ()) {
		pSnapshot = (CSnapshot *)GetSnapshot ();
		pSnapshot->SetElementsChanged (sel);
		bBold = !sel [0].m_pElement->IsFontBold ();
	}

	for (int i = 0; i < sel.GetSize (); i++) {
		CElement & e = sel [i];
		CEditorElementList & list = * sel [i].m_pList;
		Box::CBoxParams & p = pView->GetParams ();
		const CBoundary & head = list.m_bounds;
		const CRect rcHead [] = 
		{
			m_box.GetHeadRect (head, p, true),
			m_box.GetHeadRect (head, p, false),
		};

		e.SetFontBold (bBold);
		e.Confine (* GetDC (), rcHead, dZoom, head, workingHead);
	}


	if (pSnapshot) {
		if (!CheckState ()) { 
			Copy (pSnapshot);
			delete pSnapshot;
		}
		else {
			Invalidate (sel, pView);
			SetModifiedFlag (pSnapshot);
		}
	}

	EndWaitCursor ();
}

void CTaskDoc::OnFontItalic()
{
	BeginWaitCursor ();

	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	CPairArray sel = pView->GetSelected (0);
	bool bItalic = false;
	CSnapshot * pSnapshot = NULL;
	double dZoom = pView->GetZoom ();
	CArray <CRect, CRect> vrcInvalid; 
	CBoundary workingHead;

	m_box.GetWorkingHead (workingHead);

	if (sel.GetSize ()) {
		pSnapshot = (CSnapshot *)GetSnapshot ();
		pSnapshot->SetElementsChanged (sel);
		bItalic = !sel [0].m_pElement->IsFontItalic ();
	}

	for (int i = 0; i < sel.GetSize (); i++) {
		CElement & e = sel [i];
		CEditorElementList & list = * sel [i].m_pList;
		Box::CBoxParams & p = pView->GetParams ();
		const CBoundary & head = list.m_bounds;
		const CRect rcHead [] = 
		{
			m_box.GetHeadRect (head, p, true),
			m_box.GetHeadRect (head, p, false),
		};

		e.SetFontItalic (bItalic);
		e.Confine (* GetDC (), rcHead, dZoom, head, workingHead);
	}


	if (pSnapshot) {
		if (!CheckState ()) { 
			Copy (pSnapshot);
			delete pSnapshot;
		}
		else {
			Invalidate (sel, pView);
			SetModifiedFlag (pSnapshot);
		}
	}

	EndWaitCursor ();
}

void CTaskDoc::UpdateAlignmentUI (CCmdUI * pCmdUI)
{
	pCmdUI->Enable (theApp.m_uiAlign.m_bEnabled && !GetReadOnly ());
}

int CTaskDoc::CountInvalidElements (Element::CPairArray * pvInvalid) 
{
	CTaskView * pView = (CTaskView *)GetView ();
	ASSERT (pView);
	Box::CBoxParams & p = pView->GetParams ();
	const CPanel * pPanel = NULL;
	double dZoom = pView->GetZoom ();
	CDC dcMem;
	bool bUpdate = false;
	int nResult = 0;
	CBoundary workingHead;

	m_box.GetWorkingHead (p, workingHead);
	dcMem.CreateCompatibleDC (GetDC ());

	if (!m_box.IsCreated ())
		return 0;

	for (int nPanel = 1; nPanel <= 6; nPanel++) {
		FoxjetDatabase::COrientation boxOrient = m_box.GetTransform ();
		FoxjetDatabase::COrientation orient = m_box.MapPanel (nPanel, boxOrient);
		const CPanel * pRelative = NULL;
		const CPanel * pAbs = NULL;
	
		VERIFY (m_box.GetPanel (orient.m_nPanel, &pRelative));
		VERIFY (m_box.GetPanel (nPanel, &pAbs));

		for (int i = 0; i < m_task.GetMessageCount (); i++) {
			CMessage * pMsg = m_task.GetMessage (i);
			double dStretch [2] = { 0 };
			
			ASSERT (pMsg);

			CEditorElementList & list = pMsg->m_list;
			const CBoundary & head = list.m_bounds;

			CBaseElement::CalcStretch (head, dStretch);
			dStretch [0] *= CElement::CalcXStretch (head, workingHead);

			// only check heads on this panel
			if (head.GetPanelID () == pAbs->GetMembers ().m_lID) {
				const CSize sizeLogical = pRelative->GetSize (head, orient.m_orient, m_box.GetBox (), pRelative->GetMembers ());
				const CSize sizePanel = CBaseElement::LogicalToThousandths (sizeLogical, head);
				CSize sizePA = CBaseElement::LogicalToThousandths (CSize (sizeLogical.cx, head.Height ()), head);
				int nClip = sizePanel.cy - (sizePA.cy + (int)head.GetY ());

				sizePA.cx = (int)((double)sizePA.cx * dStretch [0]);
				sizePA += CSize (CElement::m_ptError);

				if (nClip < 0) 
					sizePA.cy -= abs (nClip);

				for (int nElement = 0; nElement < list.GetSize (); nElement++) {
					CElement & e = list [nElement];
					CSize sizeElement = e.GetElement ()->Draw (dcMem, head, true);
					
					sizeElement.cx = (int)floor ((double)sizeElement.cx * dStretch [0]);

#ifdef __WYSIWYGFIX__
					const double dDPI = CCoord::GetPixelsPerXUnit (INCHES, head);
					const double dFactor = dDPI / CBaseElement::CalcPrintRes (head); //((double)head.m_nHorzRes * 2.0);
					
					sizeElement.cx = (int)((double)sizeElement.cx * dFactor);
					sizeElement.cy = (int)floor ((double)sizeElement.cy * dStretch [1]);
#else
					sizeElement.cy = (int)floor ((double)sizeElement.cy * dStretch [1]);
#endif
					if (sizeElement.cx > sizePA.cx || sizeElement.cy > sizePA.cy) {
						nResult++;

						if (pvInvalid) 
							pvInvalid->Add (Element::CPair (&e, &list, &workingHead.m_card));
					}
				}
			}
		}
	}

	return nResult;
}

void CTaskDoc::OnFontNameChanged(LPCTSTR pszName)
{
	BeginWaitCursor ();

	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	CPairArray sel = pView->GetSelected (0);
	CSnapshot * pSnapshot = NULL;
	double dZoom = pView->GetZoom ();
	CArray <CRect, CRect> vrcInvalid; 
	CBoundary workingHead;

	m_box.GetWorkingHead (workingHead);

	if (sel.GetSize ()) {
		pSnapshot = (CSnapshot *)GetSnapshot ();
		pSnapshot->SetElementsChanged (sel);
	}

	for (int i = 0; i < sel.GetSize (); i++) {
		CElement & e = sel [i];
		CEditorElementList & list = * sel [i].m_pList;
		Box::CBoxParams & p = pView->GetParams ();
		const CBoundary  & head = list.m_bounds;
		const CRect rcHead [] = 
		{
			m_box.GetHeadRect (head, p, true),
			m_box.GetHeadRect (head, p, false),
		};

		e.SetFontName (pszName);
		e.Confine (* GetDC (), rcHead, dZoom, head, workingHead);
	}


	if (pSnapshot) {
		if (!CheckState ()) { 
			Copy (pSnapshot);
			delete pSnapshot;
		}
		else {
			Invalidate (sel, pView);
			SetModifiedFlag (pSnapshot);
		}
	}

	EndWaitCursor ();
}

void CTaskDoc::OnFontSizeChanged(int nSize)
{
	BeginWaitCursor ();

	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	CPairArray sel = pView->GetSelected (0);
	CSnapshot * pSnapshot = NULL;
	double dZoom = pView->GetZoom ();
	CArray <CRect, CRect> vrcInvalid; 
	CBoundary workingHead;

	m_box.GetWorkingHead (workingHead);

	if (sel.GetSize ()) {
		pSnapshot = (CSnapshot *)GetSnapshot ();
		pSnapshot->SetElementsChanged (sel);
	}

	for (int i = 0; i < sel.GetSize (); i++) {
		CElement & e = sel [i];
		CEditorElementList & list = * sel [i].m_pList;
		Box::CBoxParams & p = pView->GetParams ();
		const CBoundary & head = list.m_bounds;
		const CRect rcHead [] = 
		{
			m_box.GetHeadRect (head, p, true),
			m_box.GetHeadRect (head, p, false),
		};

		if (nSize > head.m_card.GetChannels ()) {
			Copy (pSnapshot);
			delete pSnapshot;
			pSnapshot = NULL;
			break;
		}

		e.SetFontSize (nSize);
		e.Confine (* GetDC (), rcHead, dZoom, head, workingHead);
	}

	if (pSnapshot) {
		if (!CheckState ()) { 
			Copy (pSnapshot);
			delete pSnapshot;
		}
		else {
			Invalidate (sel, pView);
			SetModifiedFlag (pSnapshot);
		}
	}

	EndWaitCursor ();
}

void CTaskDoc::OnFontWidthChanged(int nSize)
{
	BeginWaitCursor ();

	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	CPairArray sel = pView->GetSelected (0);
	CSnapshot * pSnapshot = NULL;
	double dZoom = pView->GetZoom ();
	Element::CPairArray v;
	CArray <CRect, CRect> vrcInvalid; 
	CBoundary workingHead;

	m_box.GetWorkingHead (pView->GetParams (), workingHead);

	//TRACEF (ToString (nSize));

	if (sel.GetSize ()) {
		pSnapshot = (CSnapshot *)GetSnapshot ();
		pSnapshot->SetElementsChanged (sel);
	}

	for (int i = 0; i < sel.GetSize (); i++) {
		CElement & e = sel [i];
		CEditorElementList & list = * sel [i].m_pList;
		Box::CBoxParams & p = pView->GetParams ();
		const CBoundary & head = list.m_bounds;
		Element::CPair pair (&e, &list, &workingHead.m_card);
		const CRect rcHead [] = 
		{
			m_box.GetHeadRect (head, p, true),
			m_box.GetHeadRect (head, p, false),
		};

		//TRACEF (ToString (nSize) + _T (": ") + e.GetElement ()->ToString (::verApp));

		e.SetWidth (nSize);
		e.Confine (* GetDC (), rcHead, dZoom, head, workingHead);

		v.Add (pair);
	}

	if (pSnapshot) {
		if (!CheckState ()) { 
			Copy (pSnapshot);
			delete pSnapshot;
		}
		else {
			Invalidate (sel, pView);
			SetModifiedFlag (pSnapshot);
		}
	}

	EndWaitCursor ();
}

void CTaskDoc::OnAlignProdcenter()
{
	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	CPairArray sel = pView->GetSelected (0);
	CArray <CRect, CRect> vrcInvalid; 
	
	if (sel.GetSize ()) {
		CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();
		CBoundary workingHead;

		pSnapshot->SetElementsChanged (sel);
		m_box.GetWorkingHead (pView->GetParams (), workingHead);

		for (int i = 0; i < sel.GetSize (); i++) {
			CElement & e = sel [i];
			CEditorElementList & list = * sel [i].m_pList;
			const FoxjetDatabase::HEADSTRUCT & head = list.GetHead ();

			CRect rcMiddle (CPoint (0, 0), GetProductArea (list));
			int nMiddle = rcMiddle.CenterPoint ().x;
			CPoint ptOffset (nMiddle - e.GetWindowRect (head, workingHead).CenterPoint ().x, 0);
			double dXStretch = 1.0 / CElement::CalcXStretch (head, workingHead);

			ptOffset.x = (int)((double)ptOffset.x * dXStretch);
			e.SetXPos (e.GetPos (head) + ptOffset, head);
		}

		Invalidate (sel, pView);
		SetModifiedFlag (pSnapshot);
	}
}

void CTaskDoc::OnUpdateAlignProdcenter(CCmdUI* pCmdUI)
{
	pCmdUI->Enable (theApp.m_uiSel.m_nSelCount > 0 && !GetReadOnly ());	
}

void CTaskDoc::OnAlignFliphorz()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnPropertiesFliph ();
}

void CTaskDoc::OnUpdateAlignFliphorz(CCmdUI* pCmdUI) 
{
	//m_bEnabled and m_bChecked not used
	ASSERT (pCmdUI);
	pCmdUI->Enable (theApp.m_uiSel.m_nSelCount > 0 && !GetReadOnly ());
	pCmdUI->SetCheck (theApp.m_uiFlipH.m_nState);
}

void CTaskDoc::OnAlignFlipvert()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnPropertiesFlipv ();
}

void CTaskDoc::OnUpdateAlignFlipvert(CCmdUI* pCmdUI)
{
	//m_bEnabled and m_bChecked not used
	ASSERT (pCmdUI);
	pCmdUI->Enable (theApp.m_uiSel.m_nSelCount > 0 && !GetReadOnly ());	
	pCmdUI->SetCheck (theApp.m_uiFlipV.m_nState);
}

void CTaskDoc::OnAlignInverse()
{
	//DECLARETRACECOUNT (20);
	//MARKTRACECOUNT ();

	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnPropertiesInverse ();

	//MARKTRACECOUNT ();
	//TRACECOUNTARRAY (); 
}

void CTaskDoc::OnUpdateAlignInverse(CCmdUI* pCmdUI)
{
	//m_bEnabled and m_bChecked not used
	ASSERT (pCmdUI);
	pCmdUI->Enable (theApp.m_uiSel.m_nSelCount > 0 && !GetReadOnly ());
	pCmdUI->SetCheck (theApp.m_uiInverse.m_nState);
}

void CTaskDoc::OnUpdateFontBold(CCmdUI* pCmdUI)
{
	pCmdUI->Enable (theApp.m_uiFont.m_bold.m_bEnabled && !GetReadOnly ());
	pCmdUI->SetCheck (theApp.m_uiFont.m_bold.m_bChecked);
}

void CTaskDoc::OnUpdateFontItalic(CCmdUI* pCmdUI)
{
#ifndef __IPPRINTER__
	pCmdUI->Enable (theApp.m_uiFont.m_italic.m_bEnabled && !GetReadOnly ());
	pCmdUI->SetCheck (theApp.m_uiFont.m_italic.m_bChecked);
#else //!__IPPRINTER__
	pCmdUI->Enable (FALSE);
#endif //!__IPPRINTER__

}

void CTaskDoc::UpdateFontUIStruct(FONTUISTRUCT &f, const Element::CPairArray & v)
{
	using namespace TaskView;

	CTaskView * pView	= (CTaskView *)GetView ();
	SELECTIONTYPE nUnifiedName		= theApp.m_selFontNameUnified		= pView->GetSelFontNameUnified (v);
	SELECTIONTYPE nUnifiedSize		= theApp.m_selFontSizeUnified		= pView->GetSelFontSizeUnified (v);
	SELECTIONTYPE nUnifiedWidth		= theApp.m_selFontWidthUnified		= pView->GetSelFontWidthUnified (v);
	SELECTIONTYPE nUnifiedBold		= theApp.m_selFontBoldUnified		= pView->GetSelFontBoldUnified (v);
	SELECTIONTYPE nUnifiedItalic	= theApp.m_selFontItalicUnified		= pView->GetSelFontItalicUnified (v);
	bool bSelHasFont				= theApp.m_selHasFont				= pView->SelHasFont (v);
	bool bMultipleHeads				= theApp.m_selMultipleHeads			= CElement::SpansMultipleHeads (v, pView->GetParams ().m_lHeadID);

	f.m_name.m_bEnabled		= false;
	f.m_size.m_bEnabled		= false;
	f.m_bold.m_bEnabled		= false;
	f.m_italic.m_bEnabled	= false;

	f.m_strName				= _T ("");
	f.m_name.m_bEnabled		= bSelHasFont;
	f.m_name.m_nState		= nUnifiedName;

	f.m_nWidth				= -1;
	f.m_width.m_nState		= nUnifiedWidth;
	f.m_width.m_bEnabled	= false;

	f.m_size.m_nState		= nUnifiedSize;
	f.m_size.m_bEnabled		= bSelHasFont && !bMultipleHeads;
	f.m_nSize				= 0;

	f.m_bold.m_bEnabled		= bSelHasFont;
	f.m_italic.m_bEnabled	= bSelHasFont;
	f.m_bold.m_bChecked		= false;
	f.m_italic.m_bChecked	= false;


	if (v.GetSize ()) {
		if (f.m_name.m_bEnabled && nUnifiedName != TaskView::NA) {
			const CElement & e = v [0];
			f.m_strName = e.GetFontName ();
		}

		if (f.m_size.m_bEnabled && nUnifiedSize != TaskView::NA) {
			const CElement & e = v [0];
			f.m_nSize = e.GetFontSize ();
		}

		if (nUnifiedWidth != TaskView::NONE) {
			const CElement & e = v [0];
			f.m_nWidth = nUnifiedWidth == TaskView::NA ? -1 : e.GetWidth ();
			f.m_width.m_bEnabled = true;
		}

		if (f.m_bold.m_bEnabled) 
			f.m_bold.m_bChecked = (nUnifiedBold == TaskView::ALL);

		if (f.m_italic.m_bEnabled)
			f.m_italic.m_bChecked = (nUnifiedItalic == TaskView::ALL);

		if (bSelHasFont) {
			CBaseTextElement * pElement = DYNAMIC_DOWNCAST (CBaseTextElement, v [0].m_pElement->GetElement ());

			ASSERT (pElement);

			if (nUnifiedName == TaskView::ALL)
				f.m_strName	= pElement->GetFontName ();
			if (nUnifiedSize == TaskView::ALL) 
				f.m_nSize = pElement->GetFontSize ();
		}
	}

	CFontBar & bar = theApp.GetFontBar ();

	bar.SetFontName (f.m_strName);
	bar.SetFontSize (f.m_nSize);
	bar.SetFontWidth (f.m_nWidth);
}

void CTaskDoc::UpdateAlignUIStruct(CMDUISTRUCT &c, const Element::CPairArray & v)
{
	CTaskView * pView = (CTaskView *)GetView ();
	
	theApp.m_selMovableUnified = pView->GetSelMovableUnified (v);

	c.m_bEnabled = 
		!GetReadOnly () &&
		v.GetSize () > 1 &&
		theApp.m_selMovableUnified == TaskView::ALL;
}

void CTaskDoc::UpdateUIStructs() 
{
	if (CTaskView * pView = (CTaskView *)GetView (TASK, 0)) {
		CPairArray v = pView->GetSelected (0);

		UpdateAlignUIStruct (theApp.m_uiAlign, v);
		UpdateFontUIStruct (theApp.m_uiFont, v);
		UpdateAttrUIStruct (theApp.m_uiFlipH, theApp.m_uiFlipV, theApp.m_uiInverse, v);
		theApp.m_uiSel.m_nSelCount = v.GetSize ();
		theApp.m_lCurrentHead = pView->GetActiveHead ();
	}
}

void CTaskDoc::OnEditCut()
{
	BeginWaitCursor ();

	ASSERT (!GetReadOnly ());	
	OnEditCopy ();

	CTaskView * pView = (CTaskView *)GetView ();
	CPairArray sel = pView->GetSelected (0);
	CArray <CRect, CRect> vrcInvalid; 
	Box::CBoxParams & params = pView->GetParams ();
	const CPoint ptScroll = pView->GetScrollPosition ();
	const double dZoom = pView->GetZoom ();

	if (sel.GetSize ()) {
		CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();
		CBoundary workingHead;

		pSnapshot->SetElementsChanged (sel, ELEMENT_DELETE);
		m_box.GetWorkingHead (params, workingHead);

		for (int i = 0; i < sel.GetSize (); i++) 
			sel [i].m_pElement->SetSelected (false, pView, sel [i], workingHead);
		
		Erase (sel, pView);

		for (int i = sel.GetSize () - 1; i >= 0; i--) {
			CEditorElementList & list = sel [i];
			CElement * pElement = sel [i].m_pElement;

			int nIndex = list.GetIndex (pElement);
			list.RemoveAt (nIndex);
			delete pElement;
		}

		SetModifiedFlag (pSnapshot);
	}

	EndWaitCursor ();
}

void CTaskDoc::OnUpdateEditCut(CCmdUI* pCmdUI) 
{
	if (GetReadOnly ())
		pCmdUI->Enable (false);
	else
		OnUpdateEditCopy (pCmdUI);
}

void CTaskDoc::OnEditCopy() 
{
	if (!::OpenClipboard (NULL)) {
		TRACEF (_T ("OpenClipboard failed"));
		ASSERT (0);
		return;
	}

	::EmptyClipboard ();

	CTaskView * pView = (CTaskView *)GetView ();
	CPairArray sel = pView->GetSelected (0);
	CString strData;
	TCHAR szCrLf [] = { 0x0D, 0x0A, 0x00 };
	HGLOBAL hClipboardText = NULL;

	for (int i = 0; i < sel.GetSize (); i++) {
		const CBaseElement & e = * sel [i].m_pElement->GetElement ();
		strData += e.ToString (GetElementListVersion ()) + szCrLf;

		#ifdef __WINPRINTER__
		{
			using namespace FoxjetElements;

			if (CBarcodeElement * p = DYNAMIC_DOWNCAST (CBarcodeElement, &e)) {
				if (p->GetMagnification () == GetParamsPerSymbology ()) {
					CBarcodeParams b = CBarcodeElement::GetLocalParam (p->GetLocalParamUID (), p->GetSymbology ());

					strData += b.ToString (::verApp);
				}
			}
		}
		#endif

	}

	TRACEF (strData);

	int nLen = strData.GetLength () + 1;

	#ifdef _UNICODE
	nLen = (strData.GetLength () + 1) * 2;
	#endif

	hClipboardText = ::GlobalAlloc (GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT, nLen);
	LPVOID lpCbTextData = ::GlobalLock (hClipboardText);
	memcpy (lpCbTextData, (LPCTSTR)strData, nLen - 1);

	if (::SetClipboardData (CF_UNICODETEXT, hClipboardText) == NULL)
		MsgBox (_T ("SetClipboardData failed"));

	if (HBITMAP hBmp = pView->CreateBitmap (* GetDC (), sel)) 
		::SetClipboardData (CF_BITMAP, hBmp);

	if (hClipboardText)
		::GlobalUnlock (hClipboardText);

	if (!::CloseClipboard ())
		TRACEF (_T ("CloseClipboard failed"));
}

void CTaskDoc::OnUpdateEditCopy(CCmdUI* pCmdUI)
{
	pCmdUI->Enable (theApp.m_uiSel.m_nSelCount > 0);
}

CEditorElementList * CTaskDoc::GetActiveList () const
{
	CTaskView * p = (CTaskView *)GetView ();
	TaskView::CElementListPtrArray v = p->GetLists ();
	ULONG lHeadID = p->GetActiveHead ();

	for (int i = 0; i < v.GetSize (); i++) 
		if (v [i]->m_bounds.GetID () == lHeadID)
			return v [i];

	return NULL;
}

CRect CTaskDoc::GetBoundingRect (const Element::CPairArray & v) const
{
	CRect rc (0, 0, 0, 0);

	if (v.GetSize ()) {
		const CElement & eFirst = v [0];
		const CEditorElementList & listFirst = v [0];
		CBoundary workingHead;

		m_box.GetWorkingHead (((CTaskView *)GetView ())->GetParams (), workingHead);
		
		rc = eFirst.GetWindowRect (listFirst.GetHead (), workingHead);

		for (int i = 0; i < v.GetSize (); i++) {
			const CElement & e = v [i];
			const CEditorElementList & list = v [i];
			const CRect rcElement = e.GetWindowRect (list.GetHead (), workingHead);

			rc.UnionRect (rc, rcElement);
		}
	}

	return rc;
}

CRect CTaskDoc::GetBoundingRect (const CEditorElementList & v) const
{
	CRect rc (0, 0, 0, 0);

	if (v.GetSize ()) {
		const CElement & eFirst = v [0];
		CBoundary workingHead;

		m_box.GetWorkingHead (((CTaskView *)GetView ())->GetParams (), workingHead);
		
		rc = eFirst.GetWindowRect (v.GetHead (), workingHead);

		for (int i = 0; i < v.GetSize (); i++) {
			const CElement & e = v [i];
			const CRect rcElement = e.GetWindowRect (v.GetHead (), workingHead);

			rc.UnionRect (rc, rcElement);
		}
	}

	return rc;
}

void CTaskDoc::OnCreateCaption ()
{
#ifdef __WINPRINTER__
	using namespace FoxjetElements;

	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	Box::CBoxParams & params = pView->GetParams ();
	double dZoom = pView->GetZoom ();
	UNITS units = GetUnits ();
	CPairArray vCreated;
	CBoundary workingHead;
	ULONG lLink = 0;
	CRect rcBarcode (0, 0, 0, 0);

	m_box.GetWorkingHead (pView->GetParams (), workingHead);

 	CEditorElementList * pList = pView->GetList (pView->GetActiveHead ());

	if (!pList)
		return;

	const CBoundary & head = pList->m_bounds;

	{ // get the selected barcode
		CPairArray sel = pView->GetSelected (0);
		
		if (!sel.GetSize ())
			return;

		if (CBarcodeElement * pBarcode = DYNAMIC_DOWNCAST (CBarcodeElement, sel [0].m_pElement->GetElement ())) {
			lLink = pBarcode->GetID ();
			rcBarcode = pBarcode->GetWindowRect (head);
		}
	}

	// create a new text element & set it's link
	{
		CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();
		CTextElement * pText = (CTextElement *)CreateElement (TEXT, head);
		CElement * pElement = new CElement (pText);
		CPairArray v;
		const CRect rcHead [] = 
		{
			m_box.GetHeadRect (head, params, true),
			m_box.GetHeadRect (head, params, false),
		};
		CSize sizeHead = CBaseElement::LogicalToThousandths (rcHead [0].Size () / dZoom, head);
		CPoint pt = FoxjetCommon::CBaseElement::ThousandthsToLogical (CPoint (rcBarcode.left, rcBarcode.bottom), head);
		CDC & dc = * GetDC ();

		pList->Add (pElement);

		pText->SetID (pList->GetNextID ());
		pText->SetLink (lLink);
		pText->OnCreateNew (head, m_task.m_strName);
		pText->Build ();
		pText->Build (* pList);

		pElement->SetSelected (true, pView, * pList, head);

		pElement->SetPos (pt, head);
		SelectAll (false);
		pElement->Confine (dc, rcHead, dZoom, head, workingHead);

		v.Add (Element::CPair (pElement, pList, &head.m_card));
		pSnapshot->SetElementsChanged (v, ELEMENT_ADD);
		Invalidate (v, pView);
		SetModifiedFlag (pSnapshot);
	}
#endif //__WINPRINTER__
}

void CTaskDoc::OnEditPaste()
{
	ASSERT (!GetReadOnly ());	
	CTaskView * pView = (CTaskView *)GetView ();
	Box::CBoxParams & params = pView->GetParams ();
	double dZoom = pView->GetZoom ();
	UNITS units = GetUnits ();
	CPairArray vCreated;
	CBoundary workingHead;

	m_box.GetWorkingHead (pView->GetParams (), workingHead);

 	CEditorElementList * pList = pView->GetList (pView->GetActiveHead ());

	if (!pList)
		return;

	TRACEF (pList->GetHead ().m_strName);

	BeginWaitCursor ();

	CSnapshot * pSnapshot = (CSnapshot *)GetSnapshot ();

	if (!::OpenClipboard (NULL))
		MsgBox (_T ("OpenClipboard failed"));

	// try plain text first
	if (::IsClipboardFormatAvailable (CF_UNICODETEXT)) {
		CString strData = (LPCTSTR)::GetClipboardData (CF_UNICODETEXT);
		TRACEF (_T ("CF_UNICODETEXT: ") + strData);
		vCreated = PasteFromString (strData, * pList); 

		#ifdef _DEBUG
		CString str;
		str.Format (_T ("%d elements parsed"), vCreated.GetSize ());
		TRACEF (str);
		#endif //_DEBUG
	}
	else if (::IsClipboardFormatAvailable (CF_TEXT)) {
		CString strData = (LPTSTR)::GetClipboardData (CF_TEXT);
		TRACEF (_T ("CF_TEXT: ") + strData);
		vCreated = PasteFromString (strData, * pList); 

		#ifdef _DEBUG
		CString str;
		str.Format (_T ("%d elements parsed"), vCreated.GetSize ());
		TRACEF (str);
		#endif //_DEBUG
	}
		
	// still nothing pasted, try a bitmap
	if (!vCreated.GetSize () && ::IsClipboardFormatAvailable (CF_BITMAP) && pList->CanAdd ()) {
		HBITMAP hBmp = (HBITMAP)::GetClipboardData (CF_BITMAP);
		const FoxjetDatabase::HEADSTRUCT & head = pList->GetHead ();
		FoxjetDatabase::LINESTRUCT line;
		CString strName;

		#ifdef _DEBUG
		CBitmap & bmp = * CBitmap::FromHandle (hBmp);
		DIBSECTION ds;
		CString str;

		::ZeroMemory (&ds, sizeof (ds));
		VERIFY (bmp.GetObject (sizeof (DIBSECTION), &ds));

		str.Format (_T ("CF_BITMAP:")
			_T ("\n\tbmType = %u")
			_T ("\n\tbmWidth = %u")
			_T ("\n\tbmHeight = %u")
			_T ("\n\tbmWidthBytes = %u")
			_T ("\n\tbmPlanes = %u")
			_T ("\n\tbmBitsPixel = %u")
			_T ("\n\tbmBits = %p"),
			ds.dsBm.bmType,
			ds.dsBm.bmWidth,
			ds.dsBm.bmHeight,
			ds.dsBm.bmWidthBytes,
			ds.dsBm.bmPlanes,
			ds.dsBm.bmBitsPixel,
			ds.dsBm.bmBits); 
		TRACEF (str);
		#endif //_DEBUG

		GetLineRecord (m_task.m_lLineID, line);
		strName.Format (_T ("%s_%s_%s_"), line.m_strName, m_task.m_strName, head.m_strName);
		CBaseElement * pBase = CreateElement (hBmp, head, strName); 

		if (pBase) {
			CElement * p = new CElement (pBase);
			const CBoundary & head = pList->m_bounds;
			const CRect rcHead [] = 
			{
				m_box.GetHeadRect (head, params, true),
				m_box.GetHeadRect (head, params, false),
			};
			CSize sizeHead = CBaseElement::LogicalToThousandths (rcHead [0].Size () / dZoom, head);
			CDC dcMem;

			pBase->Build ();

			dcMem.CreateCompatibleDC (GetDC ());
			CSize size = pBase->Draw (dcMem, head, true);

			if (size.cx > sizeHead.cx) {
				MsgBox (LoadString (IDS_CLIPBOARDELEMENTTOOLONG));
				delete p;
				p = NULL;
				pBase = NULL;
			}
			else if (size.cy > sizeHead.cy) {
				MsgBox (LoadString (IDS_CLIPBOARDELEMENTTOOTALL));
				delete p;
				p = NULL;
				pBase = NULL;
			}
			
			if (p) {
				vCreated.Add (Element::CPair (p, pList, &workingHead.m_card));
				pBase->SetID (pList->GetNextID ());
				p->Confine (* GetDC (), rcHead, dZoom, head, workingHead);
				pList->Add (p);
			}
		}
	}

	if (!::CloseClipboard ())
		MsgBox (_T ("CloseClipboard failed"));

	// something pasted? 
	if (vCreated.GetSize ()) {
		CDC & dc = * GetDC ();
		CPoint ptAnchor = GetBoundingRect (vCreated).TopLeft ();
		CPoint ptCrosshairs = params.m_crosshairs.m_pt / dZoom;
		const CPoint ptScroll = pView->GetScrollPosition ();
		CPairArray v;
		CArray <CRect, CRect> vrcInvalid; 
		const double dXStretch = CElement::CalcXStretch (pList->GetHead (), workingHead);

		SelectAll (false);

		if (!m_bPasteToCrosshairs) {
			ptAnchor = CPoint (0, 0);
			ptCrosshairs = CPoint (0, 0);
		}

		for (int i = 0; i < vCreated.GetSize (); i++) {
			CElement & e = vCreated [i];
			const CEditorElementList & list = vCreated [i];
			const CBoundary & head = list.m_bounds;
			CPoint ptPos = e.GetPos (head);

			ptPos.x = (int)((double)ptPos.x * dXStretch);
			
			const CPoint ptOffset = ptPos - ptAnchor;
			CPoint pt = ptCrosshairs + ptOffset;
			const CRect rcHead [] = 
			{
				m_box.GetHeadRect (head, params, true),
				m_box.GetHeadRect (head, params, false),
			};

			pt.x = (int)((double)pt.x / dXStretch);
			e.SetPos (pt, head);
			e.Confine (dc, rcHead, dZoom, head, workingHead);

			e.SetSelected (true, pView, list, workingHead);

			CRect rcElement = (e.GetWindowRect (head, workingHead) * dZoom) + rcHead [1].TopLeft () - ptScroll;

			Element::CPair pair (&e, vCreated [i].m_pList, &workingHead.m_card);

			v.Add (pair);
		}

		pSnapshot->SetElementsChanged (v, ELEMENT_ADD);

		if (!pView->GetCrosshairsVisible ()) 
			pView->ScrollToCrosshairs ();

		Invalidate (v, pView);
		SetModifiedFlag (pSnapshot);
	}
	else 
		delete pSnapshot;

	EndWaitCursor ();
}

// returns the number of elements parsed
Element::CPairArray CTaskDoc::PasteFromString (LPCTSTR pszData, CEditorElementList & list) 
{
	CPairArray vResult;
	CProductArea pa = GetProductArea (list);
	int nElements = list.GetSize ();
	int nTokens = list.FromString (pszData, pa, GetUnits (), 
		 list.GetHead (), GetElementListVersion (), 
		false, (ULONG)this);
	const CBoundary & head = list.m_bounds;
	CTaskView * pView = (CTaskView *)GetView ();
	Box::CBoxParams & params = pView->GetParams ();
	double dZoom = pView->GetZoom ();
	UNITS units = GetUnits ();
	const CRect rcHead [] = 
	{
		m_box.GetHeadRect (head, params, true),
		m_box.GetHeadRect (head, params, false),
	};
	CBoundary workingHead;

	m_box.GetWorkingHead (params, workingHead);

	for (int i = nElements; i < list.GetSize (); i++) {
		CElement & e = list [i];
		CBaseElement * pBase = e.GetElement ();
		CPoint pt = e.GetPos (head);

/* TODO: rem
		CPoint pt = e.GetPos (head) + pView->GetOffset ();
		e.SetPos (list.GetNextOpenPos (pt), head); // TODO: rem
*/

		e.SetPos (pt, head); 
		pBase->Build ();
		pBase->SetID (list.GetNextID ());
		vResult.Add (Element::CPair (&e, &list, &workingHead.m_card));
	}

	if (!nTokens && _tcslen (pszData) && list.CanAdd ()) {
		CBaseElement * pBase = CreateElement (CString (pszData), head);

		if (pBase) {
			CDC dcMem;
			CElement * p = new CElement (pBase);
			CSize sizeHead = CBaseElement::LogicalToThousandths (rcHead [0].Size () / dZoom, head);

			dcMem.CreateCompatibleDC (GetDC ());

			pBase->Build ();
			pBase->SetID (list.GetNextID ());

			CSize size = pBase->Draw (dcMem, head, true);

			if (size.cx > sizeHead.cx) {
				MsgBox (LoadString (IDS_CLIPBOARDTEXTTOOLONG));
				delete p;
				return vResult;
			}
			else if (size.cy > sizeHead.cy) {
				MsgBox (LoadString (IDS_CLIPBOARDELEMENTTOOTALL));
				delete p;
				return vResult;
			}

			SelectAll (false);
			p->SetSelected (true, pView, * GetActiveList (), workingHead);
			NCINVALIDATE (pView);
			
			list.Add (p);
			vResult.Add (Element::CPair (p, &list, &workingHead.m_card));
		}
	}

	return vResult;
}

void CTaskDoc::OnUpdateEditPaste(CCmdUI* pCmdUI)
{
	bool bClipboardData = 
		(::IsClipboardFormatAvailable (CF_TEXT) || ::IsClipboardFormatAvailable (CF_BITMAP)) &&
		GetActiveList ();

	pCmdUI->Enable (bClipboardData && !GetReadOnly ());
}

void CTaskDoc::SelectAll (bool bSelect)
{
	CTaskView * pView = (CTaskView *)GetView ();
	TaskView::CElementListPtrArray v = pView->GetLists ();
	Box::CBoxParams & params = pView->GetParams ();
	const CPoint ptScroll = pView->GetScrollPosition ();
	const double dZoom = pView->GetZoom ();
	CRect rcInvalid (0, 0, 0, 0);
	CBoundary workingHead;

	m_box.GetWorkingHead (pView->GetParams (), workingHead);

	for (int i = 0; i < v.GetSize (); i++) {
		CEditorElementList & list = * v [i];

		if (!bSelect || !m_box.IsLinked (list.m_bounds)) {
			for (int j = 0; j < list.GetSize (); j++) {
				CElement & e = list [j];
				CRect rcHead = m_box.GetHeadRect (list.m_bounds, params);

				if (e.IsSelected () != bSelect) {
					// only "select all" on the active head
					// "deselect all" doesn't care
					if ((bSelect && list.m_bounds.GetID () == pView->GetActiveHead ()) || !bSelect) {
						CRect rcElement = (e.GetWindowRect (list.GetHead (), workingHead) * dZoom) + rcHead.TopLeft () - ptScroll;

						rcElement.InflateRect (1, 1);
						e.SetSelected (bSelect, pView, list, workingHead);
						rcInvalid.UnionRect (rcInvalid, rcElement); 
					}
				}
			}
		}
	}

	Invalidate (rcInvalid, pView);
	UpdateUIStructs ();
}

void CTaskDoc::OnEditSelectall() 
{
	SelectAll (true);
}

void CTaskDoc::OnUpdateEditSelectall(CCmdUI* pCmdUI) 
{
	CTaskView * pView = (CTaskView *)GetView ();
	TaskView::CElementListPtrArray v = pView->GetLists ();
	bool bEnable = false;

	for (int i = 0; i < v.GetSize () && !bEnable; i++) 
		bEnable = (v [i]->GetSize () > 0);

	pCmdUI->Enable (bEnable);
}

void CTaskDoc::UpdateAttrUIStruct(CMDUISTRUCT &flipH, CMDUISTRUCT &flipV, CMDUISTRUCT &inverse, 
								  const Element::CPairArray & v) 
{
	CTaskView * pView = (CTaskView *)GetView ();
	int nUnifiedFlipH	= theApp.m_selFlipHUnified		= pView->GetSelFlipHUnified (v);
	int nUnifiedFlipV	= theApp.m_selFlipVUnified		= pView->GetSelFlipVUnified (v);
	int nUnifiedInverse	= theApp.m_selInverseUnified	= pView->GetSelInverseUnified (v);

	//m_bEnabled and m_bChecked not used

	flipH.m_nState		= (nUnifiedFlipH	== TaskView::ALL) ? 1 : 0;
	flipV.m_nState		= (nUnifiedFlipV	== TaskView::ALL) ? 1 : 0;
	inverse.m_nState	= (nUnifiedInverse	== TaskView::ALL) ? 1 : 0;
}

const FoxjetDatabase::HEADSTRUCT * CTaskDoc::GetSelectedHead () const
{
	CTaskView * pView = (CTaskView *)GetView ();
	const CMessage * pMsg = m_task.GetMessageByHead (pView->GetActiveHead ());
	ASSERT (pMsg);

	return &pMsg->m_list.GetHead ();
}

void CTaskDoc::OnDebuggingstuffConfining() 
{
	theApp.SetConfiningOn (!theApp.IsConfiningOn ());	
}

void CTaskDoc::OnUpdateDebuggingstuffConfining(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck (theApp.IsConfiningOn () ? 1 : 0);
}

void CTaskDoc::OnApiElementCommand(UINT nID)
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnApiElementCommand (nID);
}

void CTaskDoc::OnUpdateApiElementCommand(CCmdUI *pCmdUI)
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnUpdateApiElementCommand (pCmdUI);
}

void CTaskDoc::OnElementEdit()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnElementEdit ();
}

void CTaskDoc::OnElementsDelete()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnElementsDelete ();
}

void CTaskDoc::OnUpdateElementsDelete(CCmdUI* pCmdUI)
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnUpdateElementsDelete (pCmdUI);
}

void CTaskDoc::OnElementsList()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnElementsList ();
}

void CTaskDoc::OnPropertiesMovable()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnPropertiesMovable ();
}

void CTaskDoc::OnPropertiesResizable()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnPropertiesResizable ();
}

void CTaskDoc::OnPropertiesFliph()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnPropertiesFliph ();
}

void CTaskDoc::OnPropertiesFlipv()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnPropertiesFlipv ();
}

void CTaskDoc::OnPropertiesInverse()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnPropertiesInverse ();
}

void CTaskDoc::OnBringToFront()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnBringToFront ();
}

void CTaskDoc::OnSendToBack()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnSendToBack ();
}

void CTaskDoc::OnBringForward ()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnBringForward ();
}

void CTaskDoc::OnSendBackward ()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnSendBackward ();
}

void CTaskDoc::OnUpdateElementsEdit(CCmdUI* pCmdUI)
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnUpdateElementsEdit (pCmdUI);
}

bool CTaskDoc::SetVersion (const FoxjetCommon::CVersion & ver)
{
	ASSERT (ver.IsSynonym (GetElementListVersion ()));

	m_task.SetVersion (ver);

	return CBaseDoc::SetVersion (ver);
}

CDC * CTaskDoc::GetDC () const
{
	CTaskView * pView = (CTaskView *)GetView ();
	ASSERT (pView);

	CDC * pDC = pView->GetDC ();
	ASSERT (pDC);
	
	return pDC;
}

#ifdef _DEBUG
CTime Add (const CTime & tm, int nYears)
{
	CTime tmExp = tm;

	for (int n = 0; n < nYears; n++)
		tmExp += CTimeSpan (IsLeapYear (tm.GetYear () + n) ? 366 : 365, 0, 0, 0);
		//tmExp += CTimeSpan (365, 0, 0, 0);

	return tmExp;
}
#endif

void CTaskDoc::OnDebuggingstuffDrawinprintercontext() 
{
	#if 0
	{
		HDC hdc = ::GetDC (NULL);
		CDC dcMem, & dc = * CDC::FromHandle (hdc);
		COdbcDatabase & db = theApp.GetDB ();

		TASKSTRUCT task;
		BOXSTRUCT box;
		LINESTRUCT line;
		CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
		CArray <VALVESTRUCT, VALVESTRUCT &> vValves;
		CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;
		CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> vBounds;
		SETTINGSSTRUCT settings;
			

		VERIFY (dcMem.CreateCompatibleDC (&dc));

		VERIFY (GetTaskRecord (db, m_task.m_lID, task));
		VERIFY (GetBoxRecord (db, m_task.m_lBoxID, box));
		VERIFY (GetLineRecord (db, m_task.m_lLineID, line));
		VERIFY (GetLineHeads (db, m_task.m_lLineID, vHeads));
		VERIFY (GetPanelRecords (db, m_task.m_lLineID, vPanels));

		if (vHeads.GetSize ()) {
			CString strKey;
	
			strKey.Format (_T ("Valve head:%d"), vHeads [0].m_lID);
			VERIFY (GetSettingsRecord (db, 0, strKey, settings));
		}

		for (int i = 0; i < vHeads.GetSize (); i++) 
			vValves.Append (vHeads [i].m_vValve);

		HeadConfigDlg::CPanel::GetPanels (m_task.m_lLineID, vBounds, vHeads, vValves, vPanels);

		/*
			{TASKSTRUCT,2,1,44,text,,,0,{MESSAGESTRUCT,3,38,2,text,,\{Ver\,4\,13\,925\,0\,58\}\{Timestamp\,03/16/18 19:07:54\}\{ProdArea\,12000\,1750\,0\,38\}\{Text\,1\,0\,0\,0\,0\,0\,Vx 18b\,18\,0\,0\,9X130001\,0\,0\,0\,0\,0\,0\,0\,0\},0,1}}
			{BOXSTRUCT,44,lynx,,12000,12000,5000}
			{LINESTRUCT,1,LINE0001,Test line 1,12:00,NO READ,1,3,0,5,10.1.2.50,0,9600\,8\,N\,1\,0,9600\,8\,N\,1\,0,9600\,8\,N\,1\,0,3,1000,10,1000,2500,3,20,3,1000,28,1000,5500,2,100,24.0,1}
			{HEADSTRUCT,38,1,CARD 1,300,0,18,300,1000,0,1,1,0,90.0,0,1,1,7,0,1,60,1.813,36.0,9600\,8\,N\,1\,0,4,0}
			{PANELSTRUCT,1,1,Front,1,0,0}
			{PANELSTRUCT,2,1,Right,2,1,0}
			{PANELSTRUCT,3,1,Back,3,1,0}
			{PANELSTRUCT,4,1,Left,4,0,0}
			{PANELSTRUCT,5,1,Top,5,1,0}
			{PANELSTRUCT,6,1,Bottom,6,0,0}
			{SETTINGSSTRUCT,0,Valve head:38,\{VALVESTRUCT\,1\,38\,2000\,1\,0\,1\,4000\,1\,FJ0001\,1\,4294967295\}\{VALVESTRUCT\,2\,38\,2000\,1\,0\,1\,3125\,1\,FJ0002\,2\,1\}}
		*/

		TRACEF (ToString (task));
		TRACEF (ToString (box));
		TRACEF (ToString (line));

		for (int i = 0; i < vHeads.GetSize (); i++)
			TRACEF (ToString (vHeads [i]));

		for (int i = 0; i < vPanels.GetSize (); i++)
			TRACEF (ToString (vPanels [i]));

		TRACEF (ToString (settings));

		Foxjet3d::OpenDlg::CTaskPreview preview (task, dcMem, CRect (0, 0, 1, 1), CMapStringToString (), box, line, vHeads, vPanels, vBounds);

		for (int i = 0; i < ARRAYSIZE (preview.m_bmp); i++) 
			if (preview.m_bmp [i].GetHBITMAP ())
				preview.m_bmp [i].Save (GetHomeDir () + _T ("\\Debug\\preview") + ToString (i) + _T (".bmp"));

		::ReleaseDC (NULL, hdc);
	}
	#endif

	/*
	{
		CArray <TASKSTRUCT, TASKSTRUCT &> v;
		ULONG lLineID = m_task.m_lLineID; 

		GetTaskRecords (theApp.GetDB (), lLineID, v);

		for (int i = 0; i < 1000; i++) {
			TASKSTRUCT t = v [0];

			t.m_strName = _T ("x") + v [0].m_strName + _T ("_") + ToString (v.GetSize () + i);
			TRACEF (t.m_strName);

			for (int j = 0; j < t.m_vMsgs.GetSize (); j++)
				t.m_vMsgs [j].m_strName = t.m_strName;

			VERIFY (AddTaskRecord (theApp.GetDB (), t));
		}

		TRACEF ("********* TODO: rem ***********");
	}
	*/

	/*
	{
		const CTimeSpan tmIncrement (1, 0, 0, 0);
		const CTime tmStart (2012, 1, 1, 0, 0, 0);
		CTime tmNow = tmStart;
		int nChange;
		CString strLast;
		int nLastYear = 0;

		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%j"), 0, &CTime (2013, 3, 1, 0, 0, 0, 0)));
		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &CTime (2013, 3, 1, 0, 0, 0, 0)));

		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &CTime (2012, 3, 1, 0, 0, 0, 0)));
		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%j"), 0, &CTime (2012, 3, 1, 0, 0, 0, 0)));

		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%j"), 0, &CTime (2012, 3, 1, 0, 0, 0, 0)));
		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &CTime (2012, 2, 29, 0, 0, 0, 0)));

		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &CTime (2012, 3, 1, 0, 0, 0, 0)));
		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &CTime (2013, 3, 1, 0, 0, 0, 0)));
		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &CTime (2014, 3, 1, 0, 0, 0, 0)));
		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &CTime (2015, 3, 1, 0, 0, 0, 0)));
		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &CTime (2016, 3, 1, 0, 0, 0, 0)));
		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &CTime (2017, 3, 1, 0, 0, 0, 0)));
		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &CTime (2018, 3, 1, 0, 0, 0, 0)));
		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &CTime (2019, 3, 1, 0, 0, 0, 0)));
		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &CTime (2020, 3, 1, 0, 0, 0, 0)));
		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &CTime (2021, 3, 1, 0, 0, 0, 0)));
		TRACEF (FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &CTime (2022, 3, 1, 0, 0, 0, 0)));

		do 
		{
			tmNow += tmIncrement;

			CTimeSpan diff = tmNow - tmStart;

			CString strUS	= FoxjetElements::CDateTimeElement::Format (-1, _T ("%j"), 0, &tmNow);
			CString strEuro = FoxjetElements::CDateTimeElement::Format (-1, _T ("%J"), 0, &tmNow);

			if (strUS != strEuro) {
				CString str = strUS + _T (" ") + strEuro;

				if (strLast != str) {
					TRACEF (tmNow.Format (_T ("[%c] ")) + str);
					strLast = str;

					if (nLastYear != tmNow.GetYear ())
						nLastYear = tmNow.GetYear ();
				}
			}

			nChange = diff.GetDays ();
		} 
		while (nChange < ((365 * 15) + 1));

		int nBreak = 0;
	}
	*/

/* TODO: rem
	{
		DECLARETRACECOUNT (20);
		const ULONG lSize = 65536;
		BYTE buffer1 [lSize] = { 0 };
		BYTE buffer2 [lSize] = { 0xFF };
		
		MARKTRACECOUNT ();
		for (int i = 0; i < lSize; i++)
			buffer1 [i] = buffer2 [i];
		MARKTRACECOUNT ();

		MARKTRACECOUNT ();
		memcpy (buffer1, buffer2, lSize);
		MARKTRACECOUNT ();

		TRACECOUNTARRAY (); 
		TRACEF ("");
	}
*/
/* TODO: rem
{ 
	const DWORD dwTimeout = 2000;
	
	if (HWND hwndControl = LocateControlWnd ()) {
		HANDLE hMapFile = NULL;
		HANDLE hFile = ::CreateFile (COMMAPPEDFILE, GENERIC_READ | GENERIC_WRITE, 
			FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, TRUNCATE_EXISTING, FILE_ATTRIBUTE_NORMAL, (HANDLE)0);

		if (hFile != INVALID_HANDLE_VALUE) {
			hMapFile = ::CreateFileMapping (hFile, NULL, 
				PAGE_READWRITE, 0, COMMAPPEDFILESIZE, COMMAPPEDFILE);
		}
		else {
			hMapFile = ::OpenFileMapping (FILE_MAP_WRITE, TRUE, COMMAPPEDFILE);
		}

		if (!hMapFile)
			TRACEF (CDebug::FormatMessage (GetLastError ()));

		if (hMapFile) {
			LPVOID lpMapAddress = ::MapViewOfFile (hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, 0);

			if (lpMapAddress) {
				DWORD dw;

				dw = 0;
				::SendMessageTimeout (hwndControl, WM_OPENMAPFILE, 0, 0,  
					SMTO_BLOCK | SMTO_ABORTIFHUNG, dwTimeout, &dw);
				ASSERT (dw);

				{
					CStringArray v;

					v.Add (BW_HOST_PACKET_GET_COUNT);
					v.Add ("");
					CString str = ToString (v);
					CopyToMapFile (lpMapAddress, w2a (str));

					dw = 0;
					::SendMessageTimeout (hwndControl, WM_SENDMAPPEDCMD, 0, 0,
						SMTO_BLOCK | SMTO_ABORTIFHUNG, dwTimeout, &dw);
					ASSERT (dw);

					TRACEF (CString ((LPCTSTR)lpMapAddress));
				}

				dw = 0;
				::SendMessageTimeout (hwndControl, WM_CLOSEMAPFILE, 0, 0,  
					SMTO_BLOCK | SMTO_ABORTIFHUNG, dwTimeout, &dw);
				ASSERT (dw);

				VERIFY (::UnmapViewOfFile (lpMapAddress));
			}

			VERIFY (::CloseHandle (hMapFile));

			if (hFile != INVALID_HANDLE_VALUE) 
				VERIFY (::CloseHandle (hFile));
		}
	}

	return;
}
*/

#ifdef __WINPRINTER__
	using namespace FoxjetElements;
#endif //__WINPRINTER__

	CDC * pDC = GetDC ();
	CTaskView * pView = (CTaskView *)GetView ();
	const UCHAR szSerial [] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 0, 0, 0, 0, 0, 0, 0, 0, 0, };

	if (CMessage * pMsg = m_task.GetMessageByHead (pView->GetActiveHead ())) {
		CEditorElementList & list = pMsg->m_list;
		long double dCount = 0;
		ULONG lTotalPixels = 0;

		for (int i = 0; i < list.GetSize (); i++) {
			DECLARETRACECOUNT (20);
			const HEADSTRUCT & head = list.GetHead ();
			CElement & e = list.GetElement (i);
			CBaseElement * p = e.GetElement ();

			CDC dcMem;
			CBitmap bmp;
			CString strPath, str;

			MARKTRACECOUNT ();
			p->SetRedraw ();

			/*
			if (p->GetClassID () == COUNT) {
				if (CCountElement * pCount = DYNAMIC_DOWNCAST (CCountElement, p)) {
					bool bFailed = false;
					__int64 nMax = pCount->IsPalletCount () ? pCount->GetMax () * pCount->GetPalMax () : pCount->GetMax ();
					CString strIncrementLast;

					for (__int64 j = 0; j < (nMax * 3) + 1; j++) {
						pCount->Reset ();
						pCount->Build (FoxjetCommon::CBaseElement::INITIAL);

						for (__int64 i = 0; i < j; i++) 
							pCount->Increment ();

						pCount->Build (FoxjetCommon::CBaseElement::IMAGING);

						CString strIncrement = pCount->GetImageData ();
						
						{
							//if (pCount->IsPalletCount ()) { // && pCount->GetPalIncrement () < 0) 
								if (strIncrementLast != strIncrement) {
									CString str;
									str.Format (_T ("[%9I64d]: %s"), j, strIncrement);
									TRACEF (str);
								}
							//}
							
							strIncrementLast = strIncrement;
						}

						pCount->IncrementTo (i);
						pCount->Build (FoxjetCommon::CBaseElement::IMAGING);
						CString strIncrementTo = pCount->GetImageData ();

						if (strIncrement != strIncrementTo) {
							CString str;

							str.Format (_T ("[%I64d] %s --> %s"), j, strIncrement, strIncrementTo);

							if (pCount->IsPalletCount ()) 
								str += _T (" [pal]");

							TRACEF (str);
							bFailed = true;
							pCount->IncrementTo (i);
							pCount->Build (FoxjetCommon::CBaseElement::IMAGING);
							CString strFailed = pCount->GetImageData ();
						}
					}

					for (j = 0; j < (nMax * 3) + 1; j++) {
						const int nOffset = 2;

						if (j > nOffset) {
							pCount->IncrementTo (j - nOffset);

							for (__int64 i = j - nOffset; i < j; i++) 
								pCount->Increment ();

							pCount->Build (FoxjetCommon::CBaseElement::IMAGING);

							CString strIncrement = pCount->GetImageData ();

							pCount->IncrementTo (i);
							pCount->Build (FoxjetCommon::CBaseElement::IMAGING);
							CString strIncrementTo = pCount->GetImageData ();

							if (strIncrement != strIncrementTo) {
								CString str;

								str.Format (_T ("[%I64d] %s --> %s"), j, strIncrement, strIncrementTo);

								if (pCount->IsPalletCount ()) 
									str += _T (" [pal]");

								TRACEF (str);
								bFailed = true;
								pCount->IncrementTo (i);
								pCount->Build (FoxjetCommon::CBaseElement::IMAGING);
								CString strFailed = pCount->GetImageData ();
							}
						}
					}

					if (bFailed) {
						CString str;
						str.Format (_T ("FAILED [%9I64d]: %20s, %s"), j, pCount->GetName (), pCount->ToString (verApp));
						TRACEF (str);
						int nBreak = 0;
					}
					else {
						CString str;
						str.Format (_T ("passed [%9I64d]: %20s, %s"), j, pCount->GetName (), pCount->ToString (verApp));
						TRACEF (str);
					}
				}

				continue;
			}
			else { continue; }
			*/

			if (p->GetClassID () == EXPDATE && 0) {
				/*
				if (CExpDateElement * pExp = DYNAMIC_DOWNCAST (CExpDateElement, p)) {
					const CTime tmStart (2018, 1, 1, 0, 0, 0);
					CTime tm = tmStart;
					CTimeSpan tmSpan = tm - tmStart;
					int nLast = -1;

					while (tmSpan.GetDays () <= (366 * 9)) {
						pExp->Build (FoxjetCommon::INITIAL, false, tm);
						CString str = pExp->GetImageData ();
						int n = _ttoi (tm.Format (_T ("%d")));

						if (nLast != n) {
							TRACEF (tm.Format (_T ("%c --> ")) + str);
							nLast = n;

							ASSERT (_ttoi (str) == n);

							if (_ttoi (tm.Format (_T ("%m"))) == 2 && _ttoi (tm.Format (_T ("%d"))) == 29) {
								int nBreak = 0;
							}
						}

						tm += CTimeSpan (0, 0, 30, 0);
						tmSpan = tm - tmStart;
					}

					int nBreak = 0;
				}
				*/

//				/* TODO: rem
				if (CExpDateElement * pExp = DYNAMIC_DOWNCAST (CExpDateElement, p)) {
					CTime tmNow = CTime::GetCurrentTime ();
					//CTime tmActual = CTime (tmNow.GetYear (), tmNow.GetMonth (), tmNow.GetDay (), 0, 0, 0);
					CTime tmActual (2018, 1, 1, 0, 0, 0);
					CTime tm = tmActual;
					CTimeSpan tmSpan = tm - tmNow;

					TRACEF (tm.Format (_T ("%c")));

					{ CTime tm (2018, 3, 1, 0, 0, 0);   TRACEF (tm.Format (_T ("%d %b %Y + 2 years = ")) + Add (tm, 2).Format (_T ("%d %b %Y ")) + CTimeSpan (Add (tm, 2) - tm).Format (_T ("[%D %H:%M:%S]"))); }
					{ CTime t (2018, 3, 1, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 2 years = ")) + pExp->GetImageData ()); }

					{ CTime tm (2018, 2, 28, 0, 0, 0);  TRACEF (tm.Format (_T ("%d %b %Y + 2 years = ")) + Add (tm, 2).Format (_T ("%d %b %Y ")) + CTimeSpan (Add (tm, 2) - tm).Format (_T ("[%D %H:%M:%S]"))); }
					{ CTime tm (2018, 3, 1, 0, 0, 0);   TRACEF (tm.Format (_T ("%d %b %Y + 2 years = ")) + Add (tm, 2).Format (_T ("%d %b %Y ")) + CTimeSpan (Add (tm, 2) - tm).Format (_T ("[%D %H:%M:%S]"))); }
					{ CTime tm (2018, 3, 2, 0, 0, 0);   TRACEF (tm.Format (_T ("%d %b %Y + 2 years = ")) + Add (tm, 2).Format (_T ("%d %b %Y ")) + CTimeSpan (Add (tm, 2) - tm).Format (_T ("[%D %H:%M:%S]"))); }
					TRACEF ("");

					{ CTime tm (2019, 2, 28, 0, 0, 0);  TRACEF (tm.Format (_T ("%d %b %Y + 2 years = ")) + Add (tm, 2).Format (_T ("%d %b %Y ")) + CTimeSpan (Add (tm, 2) - tm).Format (_T ("[%D %H:%M:%S]"))); }
					{ CTime tm (2019, 3, 1, 0, 0, 0);   TRACEF (tm.Format (_T ("%d %b %Y + 2 years = ")) + Add (tm, 2).Format (_T ("%d %b %Y ")) + CTimeSpan (Add (tm, 2) - tm).Format (_T ("[%D %H:%M:%S]"))); }
					{ CTime tm (2019, 3, 2, 0, 0, 0);   TRACEF (tm.Format (_T ("%d %b %Y + 2 years = ")) + Add (tm, 2).Format (_T ("%d %b %Y ")) + CTimeSpan (Add (tm, 2) - tm).Format (_T ("[%D %H:%M:%S]"))); }
					TRACEF ("");

					{ CTime tm (2020, 2, 28, 0, 0, 0);  TRACEF (tm.Format (_T ("%d %b %Y + 2 years = ")) + Add (tm, 2).Format (_T ("%d %b %Y ")) + CTimeSpan (Add (tm, 2) - tm).Format (_T ("[%D %H:%M:%S]"))); }
					{ CTime tm (2020, 2, 29, 0, 0, 0);  TRACEF (tm.Format (_T ("%d %b %Y + 2 years = ")) + Add (tm, 2).Format (_T ("%d %b %Y ")) + CTimeSpan (Add (tm, 2) - tm).Format (_T ("[%D %H:%M:%S]"))); }
					{ CTime tm (2020, 3, 1, 0, 0, 0);   TRACEF (tm.Format (_T ("%d %b %Y + 2 years = ")) + Add (tm, 2).Format (_T ("%d %b %Y ")) + CTimeSpan (Add (tm, 2) - tm).Format (_T ("[%D %H:%M:%S]"))); }
					{ CTime tm (2020, 3, 2, 0, 0, 0);   TRACEF (tm.Format (_T ("%d %b %Y + 2 years = ")) + Add (tm, 2).Format (_T ("%d %b %Y ")) + CTimeSpan (Add (tm, 2) - tm).Format (_T ("[%D %H:%M:%S]"))); }
					TRACEF ("");

					{ CTime tm (2021, 2, 28, 0, 0, 0);  TRACEF (tm.Format (_T ("%d %b %Y + 2 years = ")) + Add (tm, 2).Format (_T ("%d %b %Y ")) + CTimeSpan (Add (tm, 2) - tm).Format (_T ("[%D %H:%M:%S]"))); }
					{ CTime tm (2021, 3, 1, 0, 0, 0);   TRACEF (tm.Format (_T ("%d %b %Y + 2 years = ")) + Add (tm, 2).Format (_T ("%d %b %Y ")) + CTimeSpan (Add (tm, 2) - tm).Format (_T ("[%D %H:%M:%S]"))); }
					{ CTime tm (2021, 3, 2, 0, 0, 0);   TRACEF (tm.Format (_T ("%d %b %Y + 2 years = ")) + Add (tm, 2).Format (_T ("%d %b %Y ")) + CTimeSpan (Add (tm, 2) - tm).Format (_T ("[%D %H:%M:%S]"))); }
					TRACEF ("");


					{ CTime t (2018, 2, 28, 0, 0, 0);   pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 2 years = ")) + pExp->GetImageData ()); }
					{ CTime t (2018, 3, 1, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 2 years = ")) + pExp->GetImageData ()); }
					{ CTime t (2018, 3, 2, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 2 years = ")) + pExp->GetImageData ()); }
					TRACEF ("");

					{ CTime t (2019, 2, 28, 0, 0, 0);   pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 2 years = ")) + pExp->GetImageData ()); }
					{ CTime t (2019, 3, 1, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 2 years = ")) + pExp->GetImageData ()); }
					{ CTime t (2019, 3, 2, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 2 years = ")) + pExp->GetImageData ()); }
					TRACEF ("");

					{ CTime t (2020, 2, 28, 0, 0, 0);   pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 2 years = ")) + pExp->GetImageData ()); }
					{ CTime t (2020, 2, 29, 0, 0, 0);   pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 2 years = ")) + pExp->GetImageData ()); }
					{ CTime t (2020, 3, 1, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 2 years = ")) + pExp->GetImageData ()); }
					{ CTime t (2020, 3, 2, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 2 years = ")) + pExp->GetImageData ()); }
					TRACEF ("");

					{ CTime t (2021, 2, 28, 0, 0, 0);   pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 2 years = ")) + pExp->GetImageData ()); }
					{ CTime t (2021, 3, 1, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 2 years = ")) + pExp->GetImageData ()); }
					{ CTime t (2021, 3, 2, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 2 years = ")) + pExp->GetImageData ()); }
					TRACEF ("");

					pExp->SetRollover (false);
					pExp->SetPeriod (CExpDateElement::PERIOD_DAYS);
					pExp->SetSpan (730, CExpDateElement::SPAN_DAYS);

					{ CTime t (2018, 2, 28, 0, 0, 0);   pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					{ CTime t (2018, 3, 1, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					{ CTime t (2018, 3, 2, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					TRACEF ("");

					{ CTime t (2019, 1, 1, 0, 0, 0);   pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					{ CTime t (2019, 2, 28, 0, 0, 0);   pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					{ CTime t (2019, 3, 1, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					{ CTime t (2019, 3, 2, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					TRACEF ("");

					{ CTime t (2020, 1, 1, 0, 0, 0);   pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					{ CTime t (2020, 2, 28, 0, 0, 0);   pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					{ CTime t (2020, 2, 29, 0, 0, 0);   pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					{ CTime t (2020, 3, 1, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					{ CTime t (2020, 3, 2, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					TRACEF ("");

					{ CTime t (2021, 1, 1, 0, 0, 0);   pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					{ CTime t (2021, 2, 28, 0, 0, 0);   pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					{ CTime t (2021, 3, 1, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					{ CTime t (2021, 3, 2, 0, 0, 0);    pExp->Build (FoxjetCommon::INITIAL, false, t); TRACEF (t.Format (_T ("%d %b %Y + 730 days = ")) + pExp->GetImageData ()); }
					TRACEF ("");


					pExp->Build (FoxjetCommon::INITIAL, false, tm);
					CString strLast = pExp->GetImageData ();
					int nLast = -1;

					while (tmSpan.GetDays () <= (366 * 9)) {
						CTime tmExp = tm;
						
						for (int n = 0; n < pExp->GetSpan (); n++) 
							tmExp += CTimeSpan (IsLeapYear (tm.GetYear ()) ? 366 : 365, 0, 0, 0);

						//TRACEF (tmExp.Format (_T ("%c")));

						int nMax = GetDaysInMonth (tm.GetMonth (), tm.GetYear ());
						int nMaxExp = GetDaysInMonth (tmExp.GetMonth (), tmExp.GetYear ());
						pExp->Build (FoxjetCommon::INITIAL, false, tm);
						CString str = pExp->GetImageData ();
						int n = _ttoi (str);

						if (strLast.CompareNoCase (str) != 0) {
							TRACEF (str + tm.Format (_T (" [%c]")));
							strLast = str;
							pExp->Build (FoxjetCommon::INITIAL, false, tm);
						}

						//if ((nLast != -1) && !(nLast == 365 && n == 1) && !(nLast == 366 && n == 1))
						if ((nLast != -1) && !(nLast == nMax && n == 1))
							ASSERT (abs (nLast - n) <= 1);

						tm += CTimeSpan (0, 0, 30, 0);
						tmSpan = tm - tmNow;
						nLast = n;
					}
				}
//				*/
			}
			int nBreak = 0;

			/* TODO: rem // sw0879 test
			if (p->GetClassID () == EXPDATE) {
				if (CExpDateElement * pExp = DYNAMIC_DOWNCAST (CExpDateElement, p)) {
					CTime tmNow = CTime::GetCurrentTime ();
					CTime tmActual = CTime (tmNow.GetYear (), tmNow.GetMonth (), tmNow.GetDay (), 0, 0, 0);
					//CTime tmActual (2007, 4, 1, 0, 0, 0);
					CTime tm = tmActual;
					CTimeSpan tmSpan = tm - tmNow;

					pExp->Build (FoxjetCommon::INITIAL, false, tm);
					CString strLast = pExp->GetImageData ();

					while (tmSpan.GetDays () <= 2) {
						pExp->Build (FoxjetCommon::INITIAL, false, tm);
						CString str = pExp->GetImageData ();

						if (strLast.CompareNoCase (str) != 0) {
							TRACEF (str + tm.Format (_T (" [%c]")));
							strLast = str;
							pExp->Build (FoxjetCommon::INITIAL, false, tm);
						}

						tm += CTimeSpan (0, 0, 1, 0);
						tmSpan = tm - tmNow;
					}
				}
			}
			int nBreak = 0;
			*/

			/* TODO: rem
			if (p->GetClassID () == DATETIME) {
				CDateTimeElement * pElement = DYNAMIC_DOWNCAST (CDateTimeElement, p);
				CTime tm (2008, 8, 12, 0, 0, 0);

				for (int nQuarter = 0; nQuarter < (24 * 4); nQuarter++) {
					CString str = CDateTimeElement::Format (p->GetLineID (), p->GetDefaultData (), pElement->GetRolloverMinutes (), &tm);

					TRACEF (tm.Format (_T ("%H:%M : ")) + str);
					if (nQuarter && (((nQuarter + 1) % 4) == 0)) TRACEF ("");
					tm += CTimeSpan (0, 0, 15, 0);
				}

				TRACEF ("************* TODO: rem **********************"); return;
			}
			*/

			/*
			int nErrors = 0;

			if (p->GetClassID () == EXPDATE) {
				CTime tmNow = CTime::GetCurrentTime ();
				CTime tmActual = CTime (tmNow.GetYear (), tmNow.GetMonth (), tmNow.GetDay (), 0, 0, 0);
				//CTime tmActual (2007, 4, 1, 0, 0, 0);
				CExpDateElement * pExp = DYNAMIC_DOWNCAST (CExpDateElement, p);
				int nChange = 0;
				const CString strFormat = "%m/%d/%Y %I:%M %p";

				ASSERT (pExp);

				/*
				{ // find dst
					const CTimeSpan span (0, 0, 5, 0);
					CTime tmStart = CTime::GetCurrentTime ();
					
					tmStart = CTime (tmStart.GetYear (), tmStart.GetMonth (), tmStart.GetDay (), 
						tmStart.GetHour (), 0, 0);
					
					CTime tmNow = tmStart;
					int nOffset = 0;
					int nCount = 0;

					while (nCount < 4) {
						tmNow += span;
						int n = pExp->GetDstOffset (tmStart, tmNow);

						if (nOffset != n) {
							CTime tm = tmNow;// - span;
							TRACEF (tm.Format ("DST change: %c"));
							nOffset = n;
							nCount++;
						}
					}
				}
				* /

				CTime tm = tmActual;
	
				{
					int nRollover = pExp->GetRolloverMinutes ();
					CTime tmRollover = tmActual - CTimeSpan (0, 0, nRollover, 0);
					int nDay = pExp->GetFirstDayOfWeek ();
					LPCTSTR lpszDay [] = 
					{
						"Sunday",
						"Monday",
						"Tuesday",
						"Wednesday",
						"Thursday",
						"Friday",
						"Saturday",
					};
					LPCTSTR lpszPeriod [] = 
					{
						"DAYS",
						"WEEKS",
						"MONTHS",
						"YEARS"
					};
					CString str;

					str.Format ("[%d %s] starting at: ",
						pExp->GetSpan (),
						lpszPeriod [pExp->GetPeriod ()]);
					str += tmActual.Format (strFormat);
					str += tmRollover.Format (" [%I:%M %p, " + CString (lpszDay [nDay]) + "]");
					TRACEF (str);
				}

				pExp->Build (CBaseElement::IMAGING, false, tm);
				CString strBuilt = pExp->GetImageData ();
				int nBreak = 0;

				CTime tmStart (tmActual);
				
				{
					int n [3] = { 0 };
					
					int nScanned = sscanf (strBuilt, "%d/%d/%d", &n [0], &n [1], &n [2]);

					ASSERT (nScanned == 3);
					tmStart = CTime (n [2], n [0], n [1], 0, 0, 0);
					//TRACEF (tmStart.Format ("%c"));
				}

				CTime tmLast (tm);
				tmLast -= CTimeSpan (1, 0, 0, 0);

				do 
				{
					int nInc = 15;

					tmActual	+= CTimeSpan (0, 0, nInc, 0);
					tm			+= CTimeSpan (0, 0, nInc, 0);
					pExp->Build (CBaseElement::IMAGING, false, tm);
					
					if (strBuilt != p->GetImageData ()) {
						int n [3] = { 0 };
						int nRollover	= pExp->GetRolloverMinutes ();
						int nHour		= tmActual.GetHour ();
						int nMinute		= tmActual.GetMinute ();
						//bool bMidnight	= (nHour == 0 && nMinute == 0);
						bool bMidnight	= (nRollover * -1) == ((nHour * 60) + nMinute);

						if (!bMidnight) 
							nErrors++;

						TRACEF (tmActual.Format (strFormat + " --> ") + p->GetImageData () + (!bMidnight ? " [*** error ***]" : ""));
						strBuilt = p->GetImageData ();

						if (!bMidnight) {
							CTime tmPrev = tm - CTimeSpan (0, 0, nInc, 0);

							pExp->Build (CBaseElement::IMAGING, false, tmPrev);
							TRACEF (tmPrev.Format ("%c") + " --> " + pExp->GetImageData ());

							pExp->Build (CBaseElement::IMAGING, false, tm);
							TRACEF (tm.Format ("%c") + " --> " + pExp->GetImageData ());
						}
						
						int nScanned = sscanf (strBuilt, "%d/%d/%d", &n [0], &n [1], &n [2]);

						ASSERT (nScanned == 3);	
						CTime tmNow (n [2], n [0], n [1], tmStart.GetHour (), tmStart.GetMinute (), tmStart.GetSecond ());
						CTimeSpan diff = tmNow - tmStart;

						nChange = diff.GetDays ();

						/* TODO: buggy
						{
							CTime tm1 (tm.GetYear (), tm.GetMonth (), tm.GetDay (), 0, 0, 0);
							CTime tm2 (tmLast.GetYear (), tmLast.GetMonth (), tmLast.GetDay (), 0, 0, 0);
							CTimeSpan diff2 = tm1 - tm2;
							
							if (diff2.GetDays () != 1) {
								CString str;

								str.Format ("*** skiped: %s [%s --> %s]", 
									diff2.Format ("%D days, %H:%M:%S"),
									tmLast.Format ("%c"),
									tm.Format ("%c"));
								TRACEF (str);
								int nBreak = 0;
							}

							tmLast = tm1;
						}
						* /
					}
				} 
				while (nChange < ((365 * 2) + 2));
			}

			str.Format ("%d errors", nErrors);
			TRACEF (str);
			*/

			p->Build (FoxjetCommon::IMAGING);

			/* TODO: rem
			if (CBarcodeElement * pBC = DYNAMIC_DOWNCAST (CBarcodeElement, p)) { // TODO: rem
				TRACEF (pBC->GetImageData ());
			}
			*/

			MARKTRACECOUNT ();
			// calc size
			CSize sizePrinter = p->Draw (* pDC, head, true, FoxjetCommon::PRINTER);
//			CSize sizeEditor = p->Draw (* pDC, head, true, FoxjetCommon::EDITOR);

//			sizePrinter = CBaseElement::LogicalToThousandths (sizePrinter, head);
//			sizeEditor = CBaseElement::ThousandthsToLogical (sizeEditor, head);


//			VERIFY (dcMem.CreateCompatibleDC (pDC));
//			VERIFY (bmp.CreateCompatibleBitmap (pDC, sizePrinter.cx, sizePrinter.cy));
			VERIFY (dcMem.CreateCompatibleDC (NULL));
			VERIFY (bmp.CreateCompatibleBitmap (&dcMem, sizePrinter.cx, sizePrinter.cy));

			CBitmap * pOld = dcMem.SelectObject (&bmp);

			::BitBlt (dcMem, 0, 0, sizePrinter.cx, sizePrinter.cy, NULL, 0, 0, WHITENESS);

			MARKTRACECOUNT ();
			p->Draw (dcMem, head, false, FoxjetCommon::PRINTER);
			MARKTRACECOUNT ();
			TRACECOUNTARRAY (); TRACEF (_T (""));

/*
			{
				HBITMAP hbmp = FoxjetElements::CBitmapElement::LoadBitmap ("C:\\Foxjet\\Logos\\128x128.bmp");
				CBitmap & bmp = * CBitmap::FromHandle (hbmp);
				DIBSECTION ds;

				ZeroMemory (&ds, sizeof (ds));
				bmp.GetObject (sizeof (ds), &ds);

				DWORD dwCount = ds.dsBm.bmWidth * ds.dsBm.bmHeight;
				BYTE bits [0x1000] = { 0 };
				CString str, strTmp;

				bmp.GetBitmapBits (dwCount, &bits);

				for (int i = 0; i < dwCount; i++) {
					BYTE n = bits [i];
					
					strTmp.Format ("0x%02X, ", n);
					str += strTmp;

					if ((i % 16) == 0)
						str += "\n";
				}

				TRACEF (str);
				::DeleteObject (hbmp);
			}
*/
/*
////////////////////////////////////////////////////////////////////////////////
			{ // TODO: rem
				CString str;
			
				str.Format ("[%s] %s [%d]", 
					head.m_strName, a2w (p->GetRuntimeClass ()->m_lpszClassName), p->GetID ());

				CBitmap * pTemp = dcMem.SelectObject (pOld);
				TestImgFcts (bmp, sizePrinter, str);
				dcMem.SelectObject (pTemp);
			}
////////////////////////////////////////////////////////////////////////////////
*/

			if (p->GetClassID () == COUNT) 
				TRACEF (p->GetImageData ());

			LONG lPixels = CountPixels (bmp);
			lTotalPixels += lPixels;
			dCount += CalcInkUsage (head.m_nHeadType, INKTYPE_FIRST, lPixels);

			strPath.Format (GetHomeDir () + _T ("\\Debug\\[%s] %s [%d] [PRINTER].bmp"), 
				head.m_strName, a2w (p->GetRuntimeClass ()->m_lpszClassName), p->GetID ());
			SAVEBITMAP (dcMem, strPath);

			str.Format (_T ("sizePrinter: %d, %d [lPixels: %d]"), 
				sizePrinter.cx, sizePrinter.cy, lPixels);
			TRACEF (str);
//			str.Format ("sizeEditor:  %d, %d\n", sizeEditor.cx, sizeEditor.cy);
//			TRACEF (str);

			// restore internal size
			p->SetRedraw ();
			p->Draw (* pDC, head, true, FoxjetCommon::EDITOR);

/*
			{
				CSize sizeEditor = p->Draw (* pDC, head, true, FoxjetCommon::EDITOR);
				CBitmap bmp;

				sizeEditor = CBaseElement::ThousandthsToLogical (sizeEditor, head);

				bmp.CreateCompatibleBitmap (pDC, sizeEditor.cx, sizeEditor.cy);
				CBitmap * pMem = dcMem.SelectObject (&bmp);
				::BitBlt (dcMem, 0, 0, sizeEditor.cx, sizeEditor.cy, NULL, 0, 0, WHITENESS);
				p->Draw (dcMem, head, false, FoxjetCommon::EDITOR);
				strPath.Format ("C:\\Temp\\Debug\\[%s] %s [%d] [EDITOR].bmp", 
					head.m_strName, a2w (p->GetRuntimeClass ()->m_lpszClassName), p->GetID ());
				SAVEBITMAP (dcMem, strPath);
				dcMem.SelectObject (pMem);
			}
*/

			dcMem.SelectObject (pOld);
		}

		CString str;
		str.Format (_T ("Ink usage [%d pixels] %.12lf"), lTotalPixels, dCount);
		TRACEF (str);
	}
}

void CTaskDoc::OnViewRulersHorizonal ()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnViewRulersHorizonal ();
}

void CTaskDoc::OnViewRulersVertical ()
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnViewRulersVertical ();
}

void CTaskDoc::OnUpdateViewRulersHorizonal (CCmdUI* pCmdUI)
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnUpdateViewRulersHorizonal (pCmdUI);
}

void CTaskDoc::OnUpdateViewRulersVertical (CCmdUI* pCmdUI) 
{
	if (CTaskView * p = (CTaskView *)GetView ())
		p->OnUpdateViewRulersVertical (pCmdUI);
}

CString CTaskDoc::GetDesc () const
{
	return m_task.m_strDesc;
}

bool CTaskDoc::SetDesc (const CString & str)
{
	m_task.m_strDesc = str;
	return true;
}

bool CTaskDoc::CheckState (const Element::CPairArray * pvConfined)
{
	CPairArray v;

	if (CountInvalidElements (&v)) { 
		CString str = LoadString (IDS_INVALIDELEMENTSIZE);

		int nResult = MsgBox (* GetView (), str, NULL, MB_YESNO | MB_ICONEXCLAMATION | MB_DEFBUTTON2);

		return nResult == IDYES;
	}
	else {
		if (pvConfined && pvConfined->GetSize ()) {
			CString str = LoadString (IDS_ELEMENTSCONFINED);

			int nResult = MsgBox (* GetView (), str, NULL, MB_YESNO | MB_ICONINFORMATION | MB_DEFBUTTON2);

			return nResult == IDYES;
		}
	}

	return true;
}

void CTaskDoc::SetPathName(LPCTSTR lpszPathName, BOOL bAddToMRU)
{
	CBaseDoc::SetPathName (lpszPathName, bAddToMRU);
	m_task.m_strName = FoxjetFile::GetFileName (GetTitle ());
}

int CTaskDoc::CountMDIFrames () const
{
	const CRuntimeClass * pFirstClass = NULL;
	int nFrames = 1;

	for (POSITION pos = GetFirstViewPosition (); pos != NULL; ) {
		CView * pView = GetNextView (pos);

		if (!pFirstClass)
			pFirstClass = pView->GetRuntimeClass ();
		else {
			const CString strClass = a2w (pView->GetRuntimeClass ()->m_lpszClassName);

			if (!strClass.CompareNoCase (a2w (pFirstClass->m_lpszClassName)))
				nFrames++;
		}
	}

	return nFrames;
}

void CTaskDoc::OnDebuggingstuffExceptions() 
{
/* TODO: rem
#include "SerialElement.h"
#include "DatabaseElement.h"

	using namespace FoxjetElements;
	UCHAR szBuffer [] = _T ("Serial buffer");

	CDC * pDC = GetDC ();
	CTaskView * pView = (CTaskView *)GetView ();

	if (CMessage * pMsg = m_task.GetMessageByHead (pView->GetActiveHead ())) {
		CEditorElementList & list = pMsg->m_list;

		for (int i = 0; i < list.GetSize (); i++) {
			const HEADSTRUCT & head = list.GetHead ();
			CElement & e = list.GetElement (i);
			CBaseElement * p = e.GetElement ();

			if (p->GetClassID () == SERIAL) {
				((CSerialElement *)p)->SetBuffer (szBuffer);
				((CSerialElement *)p)->SetIndex (4);
				((CSerialElement *)p)->SetLength (10);
			}
			else if (p->GetClassID () == DATABASE) {
				CString strSQL = "SELECT IDS From Boxes;";

				((CDatabaseElement *)p)->SetSQL (true, strSQL);
			}

			try {
				p->Build (CBaseElement::INITIAL, true);
				p->Build (CBaseElement::IMAGING, true);
				TRACEF (p->GetImageData ());
			}
			catch (CElementException * e) {
				TRACEF (e->GetErrorMessage ());
				e->ReportError (MB_OK | MB_ICONWARNING);
				e->Delete ();
			}
		}
	}
*/
}


void CTaskDoc::Invalidate (LPCRECT lpRect, const CTaskView * pCaller)
{
	for (POSITION pos = GetFirstViewPosition (); pos != NULL; ) {
		if (CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, GetNextView (pos))) {
			pView->InvalidateRect (pView == pCaller ? lpRect : NULL);
			pView->UpdateWindow ();
			NCINVALIDATE (pView);
		}
	}
}

void CTaskDoc::Erase (Element::CPairArray & vInvalidate, CTaskView * pCaller)
{
	Box::CBoxParams & params = ((CTaskView *)GetView ())->GetParams ();
	CRect rcInvalid = m_box.Erase (params, vInvalidate);

	for (POSITION pos = GetFirstViewPosition (); pos != NULL; ) {
		if (CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, GetNextView (pos))) {
			if (pView == pCaller) 
				pView->InvalidateRect (rcInvalid - pView->GetScrollPosition ());
			else
				pView->InvalidateRect (NULL);
	
			pView->UpdateWindow ();
			NCINVALIDATE (pView);
		}
	}
} 

void CTaskDoc::Invalidate (Element::CPairArray & vInvalidate, CTaskView * pCaller)
{
	Element::CPairArray vRedraw;

	CRect rcInvalid = m_box.Invalidate (((CTaskView *)GetView ())->GetParams (), vInvalidate, vRedraw);

	for (POSITION pos = GetFirstViewPosition (); pos != NULL; ) {
		if (CTaskView * pView = DYNAMIC_DOWNCAST (CTaskView, GetNextView (pos))) {
//			if (pView == pCaller) 
//				pView->InvalidateRect (rcInvalid - pView->GetScrollPosition ());
//			else
				pView->InvalidateRect (NULL);
	
			pView->UpdateWindow ();
			NCINVALIDATE (pView);
		}
	}
} 

bool CTaskDoc::GetPasteToCrosshairs () const
{
	return m_bPasteToCrosshairs;
}

void CTaskDoc::SetPasteToCrosshairs (bool bFlag)
{
	m_bPasteToCrosshairs = bFlag;
}

bool CTaskDoc::GetReadOnly () const
{
/* TODO: rem
	if (CTaskView * pView = (CTaskView *)GetView ()) {
		HEADSTRUCT h;

		if (GetHead (pView->GetActiveHead (), h))
			if (!h.m_bEnabled)
				return true;
	}
*/

	return CBaseDoc::GetReadOnly ();
}

void CTaskDoc::OnToolsInkusage() 
{
	InkUsageDlg::CInkUsageDlg (m_task, GetView ()).DoModal ();
}

void CTaskDoc::GetHeads (CBoundaryArray & v) const
{
	for (int nPanel = 1; nPanel <= 6; nPanel++) {
		const CPanel * pPanel = NULL;

		if (m_box.GetPanel (nPanel, &pPanel))
			v.Append (pPanel->m_vBounds);
	}
}

bool CTaskDoc::GetHead (ULONG lHeadID, CBoundaryArray & v, Panel::CBoundary & head) const
{
	for (int i = 0; i < v.GetSize (); i++) {
		if (v [i].GetID () == lHeadID) {
			head = v [i];
			return true;
		}
	}

	return false;
}

bool CTaskDoc::GetHead (ULONG lHeadID, CBoundary & head) const
{
	CBoundaryArray v;

	GetHeads (v);

	for (int i = 0; i < v.GetSize (); i++) {
		CBoundary & hs = v [i];

		if (hs.GetID () == lHeadID) {
			head = hs;
			return true;
		}
	}

	return false;
}

void CTaskDoc::OnAlignLockMove ()
{
	m_bMoveLocked = !m_bMoveLocked;
	FoxjetDatabase::WriteProfileInt (m_strRegKey, _T ("m_bMoveLocked"), m_bMoveLocked);
}

void CTaskDoc::OnUpdateAlignLockMove (CCmdUI * pCmdUI)
{
	pCmdUI->SetCheck (!IsMoveLocked () ? 1 : 0);
}

void CTaskDoc::OnAlignRubberband ()
{
	m_bRubberBand = !m_bRubberBand;
	FoxjetDatabase::WriteProfileInt (m_strRegKey, _T ("m_bRubberBand"), m_bRubberBand);
}

void CTaskDoc::OnUpdateAlignRubberband (CCmdUI * pCmdUI)
{
	pCmdUI->SetCheck (IsRubberBand () ? 1 : 0);
}

static TCHAR szBrowse [MAX_PATH] = { 0 };

static int CALLBACK BrowseCallbackProc(HWND hwnd,UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	// If the BFFM_INITIALIZED message is received
	// set the path to the start path.
	switch (uMsg)
	{
		case BFFM_INITIALIZED:
		{
			if (NULL != lpData)
			{
				SendMessage(hwnd, BFFM_SETSELECTION, TRUE, (LPARAM)::szBrowse);
			}
		}
	}

	return 0; // The function should always return 0.
}


void CTaskDoc::OnSaveToFile () 
{
	#define BIF_NEWDIALOGSTYLE (0x00000040)

	using namespace FoxjetElements;

	BROWSEINFO info;
	TCHAR szTitle [MAX_PATH] = { 0 };
	CString strFolder = theApp.GetProfileString (_T ("Settings"), _T ("SaveToFile"), GetHomeDir ());

	ZeroMemory (&info, sizeof (info));
	_stprintf (szTitle, _T ("%s"), LoadString (IDS_FOLDER));
	ZeroMemory (::szBrowse, ARRAYSIZE (::szBrowse));
	_tcsncpy (::szBrowse, strFolder, ARRAYSIZE (::szBrowse));

	if (CMainFrame * pMain = DYNAMIC_DOWNCAST (CMainFrame, theApp.m_pMainWnd)) 
		if (pMain->m_vDevices.GetSize ()) 
			_tcsncpy (::szBrowse, pMain->m_vDevices [pMain->m_vDevices.GetSize () - 1], ARRAYSIZE (::szBrowse));


    info.hwndOwner		= GetView ()->m_hWnd; 
    info.pidlRoot		= NULL; 
    info.pszDisplayName	= ::szBrowse; 
    info.lpszTitle		= szTitle; 
	info.lParam			= (LPARAM)::szBrowse;
	info.lpfn			= BrowseCallbackProc;
	info.ulFlags        = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;

	if (LPITEMIDLIST lpItem = ::SHBrowseForFolder (&info)) {
		CBoundaryArray v;
		TASKSTRUCT task;
		LINESTRUCT line;
		BOXSTRUCT box;

		::SHGetPathFromIDList (lpItem, ::szBrowse);
		theApp.WriteProfileString (_T ("Settings"), _T ("SaveToFile"), ::szBrowse);

		GetLineRecord (theApp.GetDB (), m_task.m_lLineID, line);
		_tcscat (::szBrowse, _T ("\\") + line.m_strName);

		CString strFile = ::szBrowse + CString (_T ("\\")) + m_task.m_strName + _T (".bwmsg");
		
		TRACEF (strFile);

		if (::GetFileAttributes (strFile) != -1) {
			CString str;

			str.Format (LoadString (IDS_FILECLOSEDALREADYEXISTS), m_task.m_strName, ::szBrowse);

			if (MsgBox (str, LoadString (IDS_WARNING), MB_ICONINFORMATION | MB_YESNO) == IDNO)
				return;
		}

		BeginWaitCursor ();
		CreateDir (::szBrowse);
		OnSave ();
		GetHeads (v);

		GetTaskRecord (theApp.GetDB (), m_task.m_lID, task);
		GetBoxRecord (theApp.GetDB (), m_task.m_lBoxID, box);

		CString strData = ToString (line) + _T ("\n\n") + ToString (box) + _T ("\n\n") + ToString (task);

		for (int nHead = 0; nHead < v.GetSize (); nHead++) {
			CBoundary & h = v [nHead];

			if (const CMessage * pMsg = m_task.GetMessageByHead (h.GetID ())) {
				for (int i = 0; i < pMsg->m_list.GetCount (); i++) {
					if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, pMsg->m_list.GetElementPtr (i)->GetElement ())) {
						CxImage img;

						if (img.Load (p->GetFilePath ())) {
							DWORD dw = img.GetSize ();
							DWORD dwCompressed = dw;
							BYTE * pData = new BYTE [dw];
							BYTE * pCompressed = new BYTE [dw];

							memcpy (pData, img.GetBits (), dw);

							int nCompress = compress (pCompressed, &dwCompressed, pData, dw);

							if (nCompress == Z_OK) {
								CString strEncoded;
								CString strBITMAP;
								DIBSECTION ds;

								HBITMAP h = img.MakeBitmap ();
								memset (&ds, 0, sizeof (ds));
								::GetObject (h, sizeof (ds), &ds);
								int nBytes = ds.dsBm.bmHeight * ds.dsBm.bmWidthBytes;
								int nBytesPerRow = ds.dsBm.bmWidthBytes;

								memset (pData, 0, dw);
								int nEncoded = EncodeBase64 (pCompressed, dwCompressed, pData, dw);
								strEncoded.Format (_T ("%d,%d,%d,%d,:Z64:%s:"), ds.dsBm.bmWidth, ds.dsBm.bmHeight, ds.dsBm.bmWidthBytes, dw, a2w (pData));

								strBITMAP.Format (_T ("{BITMAP,%s,\n%s}"), FormatString (p->GetFilePath ()), FormatString (strEncoded));
								TRACEF (_T ("\n") + strBITMAP);
								strData += _T ("\n\n") + strBITMAP;

								::DeleteObject (h);
							}

							delete [] pData;
							delete [] pCompressed;
						}
					}
				}
			}
		}

		strData.Replace (_T ("\n"), _T ("\r\n"));

		if (FILE * fp = _tfopen (strFile, _T ("w"))) {
			fwrite (w2a (strData), strData.GetLength (), 1, fp);
			fclose (fp);
		}

		EndWaitCursor ();
	}
}

void CTaskDoc::OnUpdateSaveToFile (CCmdUI* pCmdUI) 
{
	pCmdUI->Enable (TRUE);
}

