// MkDrawDocManager.h: interface for the CMkDrawDocManager class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MKDRAWDOCMANAGER_H__7066C0C1_B0AD_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_MKDRAWDOCMANAGER_H__7066C0C1_B0AD_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxwin.h>
#include <afxtempl.h>
#include "FileExt.h"
#include "BaseDoc.h"
#include "BaseDocTemplate.h"

class CNewDocType : public CObject
{
	DECLARE_DYNAMIC (CNewDocType)
public:
	CNewDocType (FoxjetFile::DOCTYPE type);
	virtual ~CNewDocType ();

	FoxjetDocument::CBaseDocTemplate *	m_pDocTemplate;
	HICON								m_hIcon;
	const FoxjetFile::DOCTYPE			m_type;

	// to be filled in by CNewDocTypeDlg
	CString						m_strLibrary;
	CString						m_strTitle;
	CString						m_strDesc;

protected:
	CNewDocType (const CNewDocType & rhs);
	CNewDocType & operator = (const CNewDocType & rhs);
};

class CMkDrawDocManager : public CDocManager  
{
	DECLARE_DYNAMIC (CMkDrawDocManager);

public:
	
	CMkDrawDocManager();
	virtual ~CMkDrawDocManager();

public:
	virtual void OnFileNew();
	virtual BOOL DoPromptFileName(CString& fileName, 
		UINT nIDSTitle, DWORD lFlags, BOOL bOpenFileDialog, 
		CDocTemplate* pTemplate);
	virtual void OnFileOpen ();
	virtual CDocument * OpenDocumentFile (LPCTSTR lpszFilename, bool bReadOnly);

	CNewDocType * GetFileType (FoxjetFile::DOCTYPE type);

	static CString GetFilePath (const CNewDocType & type);
	static CString GetFileTitle (const CNewDocType & type);
	static CString GetFileLibrary (const CNewDocType & type);
	static CString GetFileType (const CNewDocType & type);

protected:
	CArray <CNewDocType *, CNewDocType *> m_vTemplates;
};

#endif // !defined(AFX_MKDRAWDOCMANAGER_H__7066C0C1_B0AD_11D4_8FC6_006067662794__INCLUDED_)
