#if !defined(AFX_DEFSDLG_H__AE7789C6_48E4_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_DEFSDLG_H__AE7789C6_48E4_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DefsDlg.h : header file
//

#include "BaseElement.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CElementDefsDlg dialog

class CElementDefsDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CElementDefsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CElementDefsDlg ();

// Dialog Data
	//{{AFX_DATA(CElementDefsDlg)
	//}}AFX_DATA

/*
	CString m_strFontName;
	int m_nFontSize;
	bool m_bFontBold;
	bool m_bFontItalic;
*/

	CStringArray m_vDefs;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CElementDefsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void OnAttributesChanged ();
	virtual void OnOK();
	CString & GetString (int nType);
	FoxjetCommon::CBaseElement & GetElement (int nType);
	void InitElements ();

	// Generated message map functions
	//{{AFX_MSG(CElementDefsDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnFont();
	afx_msg void OnProperties();
	afx_msg void OnDblclkElements();
	afx_msg void OnSelchangeElements();
	afx_msg void OnReset();
	//}}AFX_MSG

	FoxjetCommon::CElementList m_vElements;

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEFSDLG_H__AE7789C6_48E4_11D4_8FC6_006067662794__INCLUDED_)
