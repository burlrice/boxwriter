#include "stdafx.h"
#include "mkdraw.h"
#include "TaskDoc.h"
#include "Debug.h"
#include "Message.h"
#include "Utils.h"

using namespace FoxjetCommon;
using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace TaskDoc;
using namespace TaskView;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CSnapshot::CSnapshot (const CTaskMembers & rhs)
:	m_type (ELEMENT_NONE),
	m_bProperties (false),
	FoxjetDocument::CSnapshot (rhs)
{
	const CVersion ver = GetElementListVersion ();

	// note that CTaskMembers constructor IS NOT called here
	
	m_strBox = rhs.m_box.ToString (ver);
	m_strTask = rhs.m_task.ToString (ver);

	for (int i = 0; i < rhs.m_task.GetMessageCount (); i++) {
		MESSAGEPAIRSTRUCT msg;
		const CMessage * p = rhs.m_task.GetMessage (i);

		ASSERT (p);

		msg.m_str = p->m_list.CElementList::ToString (p->m_list.GetVersion (), false, rhs.m_task.m_strName);
		msg.m_lHeadID = p->m_list.m_bounds.GetID (); 
		m_vMsgs.Add (msg);
	}

//	TRACEF (m_strBox);
}

CSnapshot & TaskDoc::CSnapshot::operator = (const CTaskMembers & rhs)
{
	CTaskMembers::operator = (rhs);
	
	m_type			= ELEMENT_NONE;
	m_bProperties	= false;

	return * this;
}

CSnapshot::~CSnapshot ()
{
}


bool CSnapshot::GetMessageByHead (ULONG lHeadID, TaskDoc::MESSAGEPAIRSTRUCT & msg) const 
{
	for (int i = 0; i < m_vMsgs.GetSize (); i++) {
		if (m_vMsgs [i].m_lHeadID == lHeadID) {
			msg = m_vMsgs [i];
			return true;
		}
	}

	return false;
}

void CSnapshot::SetElementsChanged (const Element::CPair & pair, TaskDoc::SNAPSHOTTYPE type)
{
	m_type = type;
	m_vChanged.Add (Element::CPair (pair));
}

void CSnapshot::SetElementsChanged (const Element::CPairArray & v, TaskDoc::SNAPSHOTTYPE type)
{
	m_type = type;
	m_vChanged.Append (v);
}

const Element::CPairArray & CSnapshot::GetElementsChanged () const
{
	return m_vChanged;
}

TaskDoc::SNAPSHOTTYPE CSnapshot::GetElementChangeType () const
{
	return m_type;
}

void CSnapshot::SetPropertiesChanged (const Foxjet3d::CTask & task, const Foxjet3d::Box::CBox & box)
{
	CVersion ver;

	m_bProperties = true;
	m_strTask = task.ToString (ver);
	m_strBox = box.ToString (ver);

//	TRACEF (m_strBox);
}

bool CSnapshot::GetPropertiesChanged () const
{
	return m_bProperties;
}

void CSnapshot::UpdateLinkedSlaves (CTaskDoc & doc)
{
	CTaskView * pView = (CTaskView *)doc.GetView ();
	Element::CPairArray v;
	Panel::CBoundary workingHead;
	Box::CBoxParams & params = pView->GetParams ();

	doc.GetBox ().GetWorkingHead (params, workingHead);

	for (int nPair = 0; nPair < m_vChanged.GetSize (); nPair++) {
		Element::CPair & pair = m_vChanged [nPair];
		Panel::CBoundary link;

		if (doc.m_box.GetLinkedSlave (pair.m_pList->m_bounds, link)) {
			CEditorElementList * pList = pView->GetList (link.GetID ());

			ASSERT (pList);

			if (pList) {
				switch (m_type) {
				case ELEMENT_ADD:
					{
						VERIFY (pList->FromString (pair.GetString (), pList->GetProductArea (0), pList->GetUnits (), pList->m_bounds, CVersion (verApp), false)== 1);

						for (int i = 0; i < pList->GetSize (); i++) {
							Element::CElement & e = pList->GetElement (i);

							if (e.GetElement ()->GetID () == pair.GetElementID ()) {
								v.Add (Element::CPair (&e, pList, &pList->m_bounds.m_card));
								break;
							}
						}
					}
					break;
				case ELEMENT_MODIFY: 
					{
						for (int i = 0; i < pList->GetSize (); i++) {
							Element::CElement & e = pList->GetElement (i);

							if (e.GetElement ()->GetID () == pair.GetElementID ()) {
								CRect rc = e.GetWindowRect (pList->m_bounds, workingHead);
								
								doc.Invalidate (rc + doc.m_box.GetHeadRect (pList->m_bounds, params), pView);

								v.Add (Element::CPair (&e, pList, &pList->m_bounds.m_card));

								CString str = pair.m_pElement->GetElement ()->ToString (CVersion (verApp));
								VERIFY (e.GetElement ()->FromString (str, verApp));
								e.GetElement ()->Build ();
								TRACEF (e.GetElement ()->GetDefaultData ());
								TRACEF (e.GetElement ()->GetImageData ());
								
								break;
							}
						}
					}
					break;
				case ELEMENT_DELETE:
					{
						for (int i = 0; i < pList->GetSize (); i++) {
							Element::CElement & e = pList->GetElement (i);

							if (e.GetElement ()->GetID () == pair.GetElementID ()) {
								v.Add (Element::CPair (&e, pList, &pList->m_bounds.m_card));
								break;
							}
						}
					}
					break;
				}
			}
		}
	}

	m_vChanged.Append (v);

	switch (m_type) {
	case ELEMENT_ADD:
	case ELEMENT_MODIFY: 
		doc.Invalidate (v, NULL);
		break;
	case ELEMENT_DELETE:
		{
			doc.Erase (v);

			for (int nPair = 0; nPair < v.GetSize (); nPair++) {
				Element::CPair & pair = v [nPair];
				CEditorElementList * pList = pair.m_pList;

				ASSERT (pList);

				if (pList) {
					for (int i = 0; i < pList->GetSize (); i++) {
						Element::CElement & e = pList->GetElement (i);

						if (e.GetElement ()->GetID () == pair.GetElementID ()) {
							pList->RemoveAt (i);
							delete &e;
							break;
						}
					}
				}
			}
		}
		break;
	}
}