//
// Define below GUIDs
//
#include <initguid.h>

//
// Device Interface GUID.
// Used by all WinUsb devices that this application talks to.
// Must match "DeviceInterfaceGUIDs" registry value specified in the INF file.
// 31c2e696-b586-4490-9733-45cc21747be4
//
DEFINE_GUID(GUID_DEVINTERFACE_AndroidAccessoryMode,
	0xa5dcbf10, 0x6530, 0x11d2, 0x90, 0x1f, 0x00, 0xc0, 0x4f, 0xb9, 0x51, 0xed);
//    0x31c2e696,0xb586,0x4490,0x97,0x33,0x45,0xcc,0x21,0x74,0x7b,0xe4);

// \\?\usb#vid_8087&pid_08f4#0123456789012345678901234567890#{a5dcbf10-6530-11d2-901f-00c04fb951ed}

typedef struct _DEVICE_DATA {

    BOOL                    HandlesOpen;
    WINUSB_INTERFACE_HANDLE WinusbHandle;
    HANDLE                  DeviceHandle;
    TCHAR                   DevicePath[MAX_PATH];

} DEVICE_DATA, *PDEVICE_DATA;

HRESULT
OpenDevice(
    _Out_     PDEVICE_DATA DeviceData,
    _Out_opt_ PBOOL        FailureDeviceNotFound
    );

VOID
CloseDevice(
    _Inout_ PDEVICE_DATA DeviceData
    );
