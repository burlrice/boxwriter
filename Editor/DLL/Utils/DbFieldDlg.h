#if !defined(AFX_DBFIELDDLG_H__B824CD3C_D9E6_4E99_82D8_998127A6F965__INCLUDED_)
#define AFX_DBFIELDDLG_H__B824CD3C_D9E6_4E99_82D8_998127A6F965__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DbFieldDlg.h : header file
//

#include "UtilApi.h"
#include "CellCtrlImp.h"
#include "RoundButtons.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CDbFieldDlg dialog

namespace FoxjetUtils
{
	class UTIL_API CDbFieldDlg : public FoxjetCommon::CEliteDlg
	{ 
	// Construction
	public:
		CDbFieldDlg(const CString & strRegSection, CWnd* pParent = NULL);   // standard constructor
		virtual ~CDbFieldDlg ();

	// Dialog Data
		//{{AFX_DATA(CDbFieldDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		CString m_strDSN;
		CString m_strTable;
		CString m_strField;
		int		m_nRow;
		DWORD	m_dwFlags; //see CCellCtrlImp::CELLCTRL_FLAGS
		int		m_nMaxRows;

		CString m_strSelected;

		CString m_strRegSection;
		CString m_strTitle;

		CMapStringToString m_mapResult;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CDbFieldDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		CRoundButtons m_buttons;
		CImageList m_imglist;

		virtual void OnOK();
		ItiLibrary::ListCtrlImp::CCellCtrlImp m_lv;

		// Generated message map functions
		//{{AFX_MSG(CDbFieldDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnDblclkRecords(NMHDR* pNMHDR, LRESULT* pResult);
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace FoxjetElements

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DBFIELDDLG_H__B824CD3C_D9E6_4E99_82D8_998127A6F965__INCLUDED_)
