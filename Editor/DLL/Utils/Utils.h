#ifndef __UTILS_H__
#define __UTILS_H__

#include <afx.h>
#include <math.h>
#include <string>

#include "UtilApi.h"
#include "CopyArray.h"
#include "Version.h"

#define DC1			17
#define DC2			18
#define DC3			19
#define DC4			20
#define I64_MAX		0x7FFFFFFFFFFFFFFF
#define U64_MAX		0xFFFFFFFFFFFFFFFF

typedef enum { INCHES = 0, CENTIMETERS, HEAD_PIXELS, FEET, METERS } UNITS;
// this enum always needs to start at 0

#define	UTILSPROC_HOOK		"Hook"
#define	UTILSPROC_UNHOOK	"Unhook"
#define	UTILSPROC_SWAP		"Swap"

namespace FoxjetUtils
{
	UTIL_API void InitInstance ();
	UTIL_API void ExitInstance ();
};

namespace FoxjetCommon 
{
	typedef int (CALLBACK * LPHOOKPROC) (HWND hwnd, ULONG lMsgClick, ULONG lMsgRClick, ULONG lMsgFocus);
	typedef int (CALLBACK * LPUNHOOKPROC) (HWND hwnd);
	typedef int (CALLBACK * LPSWAPPROC) ();

	typedef struct
	{
		DWORD m_dwMin;
		DWORD m_dwMax;
	} LIMITSTRUCT;

	namespace ElementFields 
	{
		typedef struct UTIL_API tagFIELDSTRUCT
		{
			tagFIELDSTRUCT (LPCTSTR lpszField = NULL, LPCTSTR lpszDef = NULL) 
				: m_strField (lpszField), m_strDef (lpszDef)
			{ 
			}

			CString	m_strField;
			CString	m_strDef;
		} FIELDSTRUCT;

		extern UTIL_API LPCTSTR m_lpszElementType;
		extern UTIL_API LPCTSTR m_lpszID;
		extern UTIL_API LPCTSTR m_lpszX;
		extern UTIL_API LPCTSTR m_lpszY;
		extern UTIL_API LPCTSTR m_lpszFlipH;
		extern UTIL_API LPCTSTR m_lpszFlipV;
		extern UTIL_API LPCTSTR m_lpszInverse;
		extern UTIL_API LPCTSTR m_lpszFontName;
		extern UTIL_API LPCTSTR m_lpszFontSize;
		extern UTIL_API LPCTSTR m_lpszBold;
		extern UTIL_API LPCTSTR m_lpszItalic;
		extern UTIL_API LPCTSTR m_lpszOrient;
		extern UTIL_API LPCTSTR m_lpszData;
		extern UTIL_API LPCTSTR m_lpszFileName;
		extern UTIL_API LPCTSTR m_lpszMaxChars;
		extern UTIL_API LPCTSTR m_lpszPrompt;
		extern UTIL_API LPCTSTR m_lpszPromptAtStart;
		extern UTIL_API LPCTSTR m_lpszCount;
		extern UTIL_API LPCTSTR m_lpszCountMin;
		extern UTIL_API LPCTSTR m_lpszCountMax;
		extern UTIL_API LPCTSTR m_lpszCountInc;
		extern UTIL_API LPCTSTR m_lpszWidth;
		extern UTIL_API LPCTSTR m_lpszHeight;
		extern UTIL_API LPCTSTR m_lpszPalCount;
		extern UTIL_API LPCTSTR m_lpszPalCountMin;
		extern UTIL_API LPCTSTR m_lpszPalCountMax;
		extern UTIL_API LPCTSTR m_lpszPalCountInc;
		extern UTIL_API LPCTSTR m_lpszLeadingZeros;
		extern UTIL_API LPCTSTR m_lpszDigits;
		extern UTIL_API LPCTSTR m_lpszPalCountEnabled;
		extern UTIL_API LPCTSTR m_lpszDays;
		extern UTIL_API LPCTSTR m_lpszHours;
		extern UTIL_API LPCTSTR m_lpszMinutes;
		extern UTIL_API LPCTSTR m_lpszSeconds;
		extern UTIL_API LPCTSTR m_lpszDSN;
		extern UTIL_API LPCTSTR m_lpszTable;
		extern UTIL_API LPCTSTR m_lpszField;
		extern UTIL_API LPCTSTR m_lpszKeyField;
		extern UTIL_API LPCTSTR m_lpszKeyValue;
		extern UTIL_API LPCTSTR m_lpszRow;
		extern UTIL_API LPCTSTR m_lpszSQLb;
		extern UTIL_API LPCTSTR m_lpszSQLstr;
		extern UTIL_API LPCTSTR m_lpszIndex;
		extern UTIL_API LPCTSTR m_lpszLength;
		extern UTIL_API LPCTSTR m_lpszTag;
		extern UTIL_API LPCTSTR m_lpszPeriod;
		extern UTIL_API LPCTSTR m_lpszRound;
		extern UTIL_API LPCTSTR m_lpszBatchQuantity;
		extern UTIL_API LPCTSTR m_lpszRollover;
		extern UTIL_API LPCTSTR m_lpszParaAlign;
		extern UTIL_API LPCTSTR m_lpszParaGap;
		extern UTIL_API LPCTSTR m_lpszParaWidth;
		extern UTIL_API LPCTSTR m_lpszResizable;
		extern UTIL_API LPCTSTR m_lpszDefault;
		extern UTIL_API LPCTSTR m_lpszSQLType;
		extern UTIL_API LPCTSTR m_lpszSerial;
		extern UTIL_API LPCTSTR m_lpszPort;
		extern UTIL_API LPCTSTR m_lpszMaster;
		extern UTIL_API LPCTSTR m_lpszLink;
		extern UTIL_API LPCTSTR m_lpszAlign;
		extern UTIL_API LPCTSTR m_lpszUserPrompt;
		extern UTIL_API LPCTSTR m_lpszPromptAtTaskStart;
		extern UTIL_API LPCTSTR m_lpszTableIndex;
		extern UTIL_API LPCTSTR m_lpszDither;
		extern UTIL_API LPCTSTR m_lpszLockAspectRatio;
		extern UTIL_API LPCTSTR m_lpszColor;
		extern UTIL_API LPCTSTR m_lpszHoldStartDate;

		extern UTIL_API LPCTSTR m_lpszIPWidth;
		extern UTIL_API LPCTSTR m_lpszIPGap;
		extern UTIL_API LPCTSTR m_lpszPosition;
		extern UTIL_API LPCTSTR m_lpszSysBCIndex;
		extern UTIL_API LPCTSTR m_lpszHROutput;
		extern UTIL_API LPCTSTR m_lpszHRAlign;
		extern UTIL_API LPCTSTR m_lpszHRBold;
		extern UTIL_API LPCTSTR m_lpszChange;
		extern UTIL_API LPCTSTR m_lpszRepeat;
		extern UTIL_API LPCTSTR m_lpszStart;
		extern UTIL_API LPCTSTR m_lpszLimit;
		extern UTIL_API LPCTSTR m_lpszAlignChar;
		extern UTIL_API LPCTSTR m_lpszFillChar;
		extern UTIL_API LPCTSTR m_lpszFormat;
		extern UTIL_API LPCTSTR m_lpszFormatType;
		extern UTIL_API LPCTSTR m_lpszDelimChar;
		extern UTIL_API LPCTSTR m_lpszStringNormal;
		extern UTIL_API LPCTSTR m_lpszHourACount;
		extern UTIL_API LPCTSTR m_lpszTimeType;
		extern UTIL_API LPCTSTR m_lpszName;
		extern UTIL_API LPCTSTR m_lpszDynFirstChar;
		extern UTIL_API LPCTSTR m_lpszDynNumChar;
		extern UTIL_API LPCTSTR m_lpszDynIndex;
		extern UTIL_API LPCTSTR m_lpszEmptyString;
		extern UTIL_API LPCTSTR m_lpszThicknessCX;
		extern UTIL_API LPCTSTR m_lpszThicknessCY;
		extern UTIL_API LPCTSTR m_lpszType;
		extern UTIL_API LPCTSTR m_lpszMaxWidthEnable;
		extern UTIL_API LPCTSTR m_lpszMaxWidth;
		extern UTIL_API LPCTSTR m_lpszDatabaseLookup;
		extern UTIL_API LPCTSTR m_lpszBmpWidth;
		extern UTIL_API LPCTSTR m_lpszBmpHeight;
	}; // namespace ElementFields 

	UTIL_API bool IsValid (DWORD dw, const LIMITSTRUCT & lmt);
	UTIL_API bool IsValid (int n, const LIMITSTRUCT & lmt);
	UTIL_API void DDV_MinMaxChars (CDataExchange* pDX, CString & value, UINT nCtrlID, const LIMITSTRUCT & lmt);
	UTIL_API void DDV_MinMaxChars (CDataExchange* pDX, CString & value, UINT nCtrlID, int nMinChars, int nMaxChars);

	UTIL_API unsigned __int64 strtoul64 (const CString & str);
	UTIL_API CString FormatI64 (__int64 iValue, int nLeadingZeroes = 0);
	UTIL_API void DDX_Text (CDataExchange* pDX, int nIDC, __int64 & iValue);
	UTIL_API void DDX_RadioOwnerDrawn (CDataExchange* pDX, int nIDC, int& value);
	UTIL_API void DDV_MinMaxInt64 (CDataExchange* pDX, int nIDC, __int64 iValue, __int64 iMin, __int64 iMax);
	UTIL_API void DDV_MinMaxUnsignedInt64 (CDataExchange* pDX, int nIDC, unsigned __int64 iValue, unsigned __int64 iMin, unsigned __int64 iMax);

	UTIL_API CString Convert (const CString & str, const FoxjetCommon::CVersion & ver);
	UTIL_API CString Convert (const CString & str, const FoxjetCommon::CVersion & verFrom,
		const FoxjetCommon::CVersion & verTo, bool bMakePacket = true);

	UTIL_API void EnumVersions (CArray <FoxjetCommon::CVersion, const FoxjetCommon::CVersion &> & v);
	UTIL_API FoxjetCommon::CVersion GetElementListVersion ();

	UTIL_API void Rotate180 (CDC & dc, const CSize & size);
	UTIL_API void RotateLeft90 (CDC & dc, CBitmap & bmpSrc, CBitmap & bmpDest);
	UTIL_API void RotateRight90 (CDC & dc, CBitmap & bmpSrc, CBitmap & bmpDest);
	UTIL_API void RotateLeft90AndFlip (CDC & dc, CBitmap & bmpSrc, CBitmap & bmpDest);

	inline bool GetPixel (LONG lBit, LPBYTE lpBits);
	inline void SetPixel (LONG lBit, LPBYTE lpBits, bool bBit);
	UTIL_API ULONG CountPixels (int nWidthBytes, int nHeight, LPBYTE lpBits);
	UTIL_API ULONG CountPixels (CBitmap & bmp);
	UTIL_API ULONG CountPixels (HBITMAP hbmp);
	UTIL_API UINT CountBits (BYTE nValue);
	UTIL_API UINT CountBits (DWORD dw);

	UTIL_API void DisableDlgCtrls (CDialog * pWnd);

	UTIL_API DWORD GetDependencies (const CString & strFilename, CStringArray & v, bool bFjOnly);
	UTIL_API CTime GetFileDate (const CString & strPath);
	UTIL_API CVersion GetDllVersion (const CString & strPath);
	UTIL_API bool Touch (const CString & strPath);

	UTIL_API CString GetCxImageExt (DWORD dwType);
	UTIL_API DWORD GetCxImageType (const CString & strFile);
	UTIL_API CString GetFileDlgList ();

	
	UTIL_API void TestImgFcts (CBitmap & bmp, CSize size, const CString & strName); // TODO: rem
	UTIL_API void Rotate180Plg (CDC & dc, const CSize & size);
	UTIL_API void RotateLeft90Plg (CDC & dc, CBitmap & bmpPrinter, CBitmap & bmpEditor);
	UTIL_API void RotateRight90Plg (CDC & dc, CBitmap & bmpEditor, CBitmap & bmpPrinter);
	UTIL_API void RotateLeft90AndFlipPlg (CDC & dc, CBitmap & bmpPrinter, CBitmap & bmpEditor);

	UTIL_API int GetNumberOfDigits (__int64 iMax);

	UTIL_API int Ping (LPCTSTR lpszAddr, int nTries, HWND hWndDebug = NULL);

	UTIL_API bool RestoreState (CWnd * pWnd, const CString & strRegKey = _T (""));
	UTIL_API void SaveState (CWnd * pWnd, const CString & strRegKey = _T (""));

	UTIL_API int CalculateNFactor (double dAngle, double dRes);

	UTIL_API BOOL CALLBACK EnumWindowsProc (HWND hWnd, LPARAM lParam);
	UTIL_API void SendKey (int nVK, bool bDown);
	UTIL_API void ActivateProcess (DWORD dwProcessID);

	UTIL_API HWND LocateControlWnd ();
	UTIL_API HWND LocateSkuManager();
	UTIL_API HWND FindKeyboard ();
	UTIL_API HWND FindKeyboardMonitor ();

	UTIL_API void AlignToKeyboard (CWnd * pwnd);
	
	UTIL_API void SetAutoButtonText (HWND hWnd);

	UTIL_API CString execute (const CString & str);
	UTIL_API CString GetPrintDriverDirectory ();

	UTIL_API std::wstring gb2312(const std::string& str);

	typedef struct 
	{
		int m_nPage;
		int m_nCount;
	} PDFSEARCHSTRUCT;

	UTIL_API CCopyArray <PDFSEARCHSTRUCT, PDFSEARCHSTRUCT &> PdfSearch (const CString & strFile, const CString & strFind, const CString & strLangExt);
	
	CString UTIL_API GetCSIDLName (const KNOWNFOLDERID & k);
	CString UTIL_API GetCSIDLName (int csidl);
	int UTIL_API GetCSIDL (const KNOWNFOLDERID & k);
	CString UTIL_API GetKnownFolderPath (const KNOWNFOLDERID & k);

	void UTIL_API CheckDllVersions ();

	class UTIL_API CEliteDlg : public CDialog
	{
		DECLARE_DYNCREATE(CEliteDlg)

	public:
		CEliteDlg (LPCTSTR lpszTemplateName, CWnd* pParentWnd = NULL);
		CEliteDlg (UINT nIDTemplate, CWnd* pParentWnd = NULL);
		CEliteDlg ();

		static void OnHelp (CWnd * pWnd);

	protected:
		COLORREF m_rgbBack;
		CBrush m_brushBackground;

		afx_msg HBRUSH OnCtlColor (CDC* pDC, CWnd* pWnd, UINT nCtlColor);
		afx_msg void OnHelp ();

		DECLARE_MESSAGE_MAP()

	private:
		void Init ();
	};
}; //namespace FoxjetCommon

inline bool FoxjetCommon::GetPixel (LONG lBit, LPBYTE lpBits)
{
	int nByte = (int)floor ((long double)lBit / 8.0);	// byte of data which lBit lies in
	int nBit = (int)(lBit - (LONG)(nByte * 8));			// bit within lpBits [nByte]

	return (lpBits [nByte] & (1 << (7 - nBit))) ? true : false;
}

inline void FoxjetCommon::SetPixel (LONG lBit, LPBYTE lpBits, bool bBit)
{
	int nByte = (int)floor ((long double)lBit / 8.0);	// byte of data which lBit lies in
	int nBit = (int)(lBit - (LONG)(nByte * 8));			// bit within lpBits [nByte]

	if (!bBit)
		lpBits [nByte] |= (1 << (7 - nBit));
	else
		lpBits [nByte] &= ~(1 << (7 - nBit));
}

#endif //__UTILS_H__