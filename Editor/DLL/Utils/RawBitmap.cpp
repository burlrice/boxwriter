// RawBitmap.cpp: implementation of the CRawBitmap class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "utils.h"
#include "RawBitmap.h"
#include "Debug.h"
#include "Resource.h"
#include "Parse.h"
#include "ximage.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////

CxBitmap::CxBitmap (HBITMAP h)
:	m_hBitmap (h) 
{ 
}

CxBitmap::~CxBitmap ()
{
	if (m_hBitmap) {
		::DeleteObject (m_hBitmap);
		m_hBitmap = NULL;
	}
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRawBitmap::CRawBitmap()
:	m_lLen (0),
	m_pBuffer (NULL)
{
	memset (&m_bm, 0, sizeof (m_bm));
}

CRawBitmap::CRawBitmap (HBITMAP hbmp)
:	m_lLen (0),
	m_pBuffer (NULL)
{
	memset (&m_bm, 0, sizeof (m_bm));
	VERIFY (Load (hbmp));
}

CRawBitmap::CRawBitmap (const CRawBitmap & rhs)
:	m_lLen (rhs.m_lLen),
	m_pBuffer (NULL)
{
	memcpy (&m_bm, &rhs.m_bm, sizeof (m_bm));
	m_pBuffer = new UCHAR [m_lLen];
	memcpy (m_pBuffer, rhs.m_pBuffer, m_lLen);
}

CRawBitmap & CRawBitmap::operator = (const CRawBitmap & rhs)
{
	Free ();

	if (this != &rhs) {
		m_lLen = rhs.m_lLen;
		memcpy (&m_bm, &rhs.m_bm, sizeof (m_bm));
		m_pBuffer = new UCHAR [m_lLen];
		memcpy (m_pBuffer, rhs.m_pBuffer, m_lLen);
	}

	return * this;
}

CRawBitmap::~CRawBitmap()
{
	Free ();
}

void CRawBitmap::Free ()
{
	if (m_pBuffer) {
		delete m_pBuffer;
		m_pBuffer = NULL;
	}

	m_lLen = 0;
	memset (&m_bm, 0, sizeof (m_bm));
}

bool CRawBitmap::Load (HBITMAP hbmp)
{
	if (::GetObject (hbmp, sizeof (m_bm), &m_bm)) {

		/*
		TRACEF (_T ("bmBitsPixel:  ") + ToString (m_bm.bmBitsPixel));
		TRACEF (_T ("bmBits:       ") + ToString ((long)m_bm.bmBits));
		TRACEF (_T ("bmHeight:     ") + ToString (m_bm.bmHeight));
		TRACEF (_T ("bmPlanes:     ") + ToString (m_bm.bmPlanes));
		TRACEF (_T ("bmType:       ") + ToString (m_bm.bmType));
		TRACEF (_T ("bmWidth:      ") + ToString (m_bm.bmWidth));
		TRACEF (_T ("bmWidthBytes: ") + ToString (m_bm.bmWidthBytes));
		*/

		if (m_pBuffer)
			delete [] m_pBuffer;

		m_lLen = m_bm.bmHeight * m_bm.bmWidthBytes;
		m_pBuffer = new UCHAR [m_lLen];
		memset (m_pBuffer, 0, m_lLen);

		if (::GetBitmapBits (hbmp, m_lLen, m_pBuffer))
			return true;
	}
	
	return false;
}

bool CRawBitmap::Save (LPCTSTR lpszFilePath, LPCSTR lpszSrcFile, ULONG lSrcLine)
{
	CBitmap bmp;
	CString strSrcFile = a2w (lpszSrcFile);
	
	if (bmp.CreateBitmap (m_bm.bmWidth, m_bm.bmHeight, m_bm.bmPlanes, m_bm.bmBitsPixel, m_pBuffer)) {
		CxImage img;

		if (img.CreateFromHBITMAP (bmp)) {
			if (img.Save (lpszFilePath, CXIMAGE_FORMAT_BMP)) {
				#ifdef _DEBUG
				CString str;
				str.Format (_T ("%s (%d, %d)"), lpszFilePath, m_bm.bmWidth, m_bm.bmHeight);
				CDebug::Trace (str, true, strSrcFile, lSrcLine);
				#endif //_DEBUG

				return true;
			}
		}
	}

	return false;
}

bool CRawBitmap::GetBitmap (CBitmap & bmp)
{
	if (m_pBuffer && m_lLen)
		if (bmp.CreateBitmap (m_bm.bmWidth, m_bm.bmHeight, m_bm.bmPlanes, m_bm.bmBitsPixel, m_pBuffer)) 
			return true;

	return false;
}

bool CRawBitmap::GetBitmap (BITMAP & bm)
{
	if (m_pBuffer) {
		memcpy (&bm, &m_bm, sizeof (bm));
		return true;
	}

	return false;
}

ULONG CRawBitmap::CountPixels () const
{
	ULONG lResult = FoxjetCommon::CountPixels (m_bm.bmWidth, m_bm.bmHeight, m_pBuffer);

	return lResult;
}

HBITMAP CRawBitmap::GetHBITMAP () const
{
	if (m_pBuffer)
		return ::CreateBitmap (m_bm.bmWidth, m_bm.bmHeight, m_bm.bmPlanes, m_bm.bmBitsPixel, m_pBuffer);

	return NULL;
}

CSize CRawBitmap::GetSize () const 
{ 
	return CSize (m_bm.bmWidth, m_bm.bmHeight); 
}
