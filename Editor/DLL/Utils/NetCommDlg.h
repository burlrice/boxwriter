#if !defined(AFX_NETCOMMDLG_H__5164EBA1_DFD9_4FFA_9826_73AB55CFD64F__INCLUDED_)
#define AFX_NETCOMMDLG_H__5164EBA1_DFD9_4FFA_9826_73AB55CFD64F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NetCommDlg.h : header file
//

#include "StdCommDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CNetCommDlg dialog

namespace FoxjetCommon
{
	bool UTIL_API EditNetSerialParams (CString & str);

	namespace NetCommDlg
	{
		class UTIL_API CCommItem : public StdCommDlg::CCommItem
		{
		public:
			CCommItem ();

			typedef enum 
			{ 
				NETSERIAL_NONE, 
				NETSERIAL_VAR_DATA, 
				NETSERIAL_DYNMEM, 
				NETSERIAL_SCANNER_LABEL, 
				NETSERIAL_INFO, 
			} USE;

			CString ToString () const;
			bool FromString (const CString & str);

			virtual CString GetDispText (int nColumn) const;

			USE m_use;
			bool m_bScannerUseOnce;
			bool m_bSaveSerial;
		};

		class UTIL_API CNetCommDlg : public CPropertyPage
		{
			DECLARE_DYNCREATE(CNetCommDlg)

		public:
			CNetCommDlg();
			~CNetCommDlg();

			CString m_strBaud;
			int m_lUse;
			BOOL m_bScannerUseOnce;
			BOOL m_bSaveSerial;

		// Dialog Data
			//{{AFX_DATA(CNetCommDlg)
				// NOTE - ClassWizard will add data members here.
				//    DO NOT EDIT what you see in these blocks of generated code !
			//}}AFX_DATA


		// Overrides
			// ClassWizard generate virtual function overrides
			//{{AFX_VIRTUAL(CNetCommDlg)
			protected:
			virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
			//}}AFX_VIRTUAL

		// Implementation
		protected:
			afx_msg void OnUseSelchange ();
			virtual BOOL OnInitDialog();
			// Generated message map functions
			//{{AFX_MSG(CNetCommDlg)
				// NOTE: the ClassWizard will add member functions here
			//}}AFX_MSG
			DECLARE_MESSAGE_MAP()

		};
	}; //namespace NetCommDlg
}; //namespace FoxjetIpElements

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NETCOMMDLG_H__5164EBA1_DFD9_4FFA_9826_73AB55CFD64F__INCLUDED_)
