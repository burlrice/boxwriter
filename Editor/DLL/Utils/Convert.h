#ifndef __CONVERT_H__
#define __CONVERT_H__

#include "CopyArray.h"
#include "Version.h"

typedef FoxjetCommon::CCopyArray <FoxjetCommon::CVersion, FoxjetCommon::CVersion &> CVersionArray;

namespace Conversion
{
	class CField
	{
	public:
		typedef enum 
		{ 
			FT_STRING,
			FT_STRING_DELIMITED,
			FT_LONG,
			FT_BOOL,
			FT_BOOL_IP,
			FT_FONT,
			FT_FONT_IP,
		} FIELDTYPE;

		CField (const CString & strName = _T (""), FIELDTYPE type = FT_STRING, const CString & strDefault = _T (""));
		CField (const CField & rhs);
		CField (const CField & rhs, const CString & strDefault);
		CField & operator = (const CField & rhs);
		virtual ~CField ();

		const CString & GetName () const;
		FIELDTYPE GetType () const;
		const CString & GetDefault () const;

		operator CString () const { return GetName (); }
		operator FIELDTYPE () const { return GetType (); }

		CString Convert (const CString & str, const CField & to) const;

	protected:
		CString		m_strName;
		FIELDTYPE	m_type;
		CString		m_strDefault;
	};

	typedef CArray <CField, CField &> CFieldArray;

	class CConversion
	{
	public:


		CConversion (const CFieldArray & vFields, const CVersionArray & vVer);
		CConversion (const CConversion & rhs);
		CConversion & operator = (const CConversion & rhs);
		virtual ~CConversion ();

		bool Contains (const FoxjetCommon::CVersion & ver) const;
		CString GetElementType () const;
		CString Convert (const CStringArray & v, const CConversion & to, bool bMakePacket) const;
		int Find (const CString & strField) const;
		CField GetField (int nIndex) const;
		const CFieldArray & GetFields () const;

		static bool IsSynonym (const FoxjetCommon::CVersion & ver1, const FoxjetCommon::CVersion & ver2);

	protected:
	
		CFieldArray			m_vFields;
		CVersionArray		m_vVer;
	};
}; //namespace Conversion
#endif //__CONVERT_H__
