// MsgBoxDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Color.h"
#include "MsgBoxDlg.h"
#include "TemplExt.h"
#include "Parse.h"
#include "Debug.h"
#include "Version.h"
#include "AppVer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

	/*
	FoxjetDatabase::SetHighResDisplay (false);
	TRACEF (ToString (MsgBox (_T ("MB_ABORTRETRYIGNORE\n\nabort\nretry\nignore"), _T (""), MB_ABORTRETRYIGNORE)));
	TRACEF (ToString (MsgBox (_T ("MB_OK"), _T (""), MB_OK)));
	TRACEF (ToString (MsgBox (_T ("MB_OKCANCEL"), _T (""), MB_OKCANCEL)));
	TRACEF (ToString (MsgBox (_T ("MB_RETRYCANCEL"), _T (""), MB_RETRYCANCEL)));
	TRACEF (ToString (MsgBox (_T ("MB_YESNO"), _T (""), MB_YESNO)));
	TRACEF (ToString (MsgBox (_T ("MB_YESNOCANCEL"), _T (""), MB_YESNOCANCEL)));

	TRACEF (ToString (MsgBox (_T ("MB_ICONEXCLAMATION"), _T (""), MB_OK | MB_ICONEXCLAMATION)));
	TRACEF (ToString (MsgBox (_T ("MB_ICONINFORMATION"), _T (""), MB_OK | MB_ICONINFORMATION)));
	TRACEF (ToString (MsgBox (_T ("MB_ICONQUESTION"), _T (""), MB_OK | MB_ICONQUESTION)));
	TRACEF (ToString (MsgBox (_T ("MB_ICONSTOP"), _T (""), MB_OK | MB_ICONSTOP)));
	*/


int CALLBACK DoMsgBox (HWND hWnd, const CString & strMessage, const CString & strTitle, UINT nType)
{
	return CMsgBoxDlg (strMessage, strTitle, nType, (hWnd ? CWnd::FromHandle (hWnd) : NULL)).DoModal ();
}

static const int nDrawTextFlags = DT_LEFT | DT_WORDBREAK | DT_VCENTER | DT_END_ELLIPSIS;


/////////////////////////////////////////////////////////////////////////////
// CMsgBoxDlg dialog


CMsgBoxDlg::CMsgBoxDlg(const CString & strMessage, const CString & strTitle, UINT nType, CWnd* pParent /*=NULL*/)
:	m_strMessage (strMessage), 
	m_strTitle (strTitle),
	m_nType (nType),
	m_hbrDlg (NULL),
	FoxjetCommon::CEliteDlg(CMsgBoxDlg::IDD, pParent)
{
	ZeroMemory (&m_msgKeyDown, sizeof (m_msgKeyDown));

	//{{AFX_DATA_INIT(CMsgBoxDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

}

CMsgBoxDlg::~CMsgBoxDlg ()
{
	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg); 
		m_hbrDlg = NULL;
	}
}


void CMsgBoxDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);


	//{{AFX_DATA_MAP(CMsgBoxDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,			IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,		IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, IDYES,			IDB_OK);
	m_buttons.DoDataExchange (pDX, IDNO,			IDB_NO);
	m_buttons.DoDataExchange (pDX, IDRETRY,			IDB_RESUME);
	m_buttons.DoDataExchange (pDX, IDIGNORE,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDABORT,			IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_CLIPBOARD,	IDB_CLIPBOARD);
}


BEGIN_MESSAGE_MAP(CMsgBoxDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CMsgBoxDlg)
	ON_BN_CLICKED(BTN_CLIPBOARD, OnClipboard)
	ON_BN_CLICKED(IDABORT, OnAbort)
	ON_BN_CLICKED(IDIGNORE, OnIgnore)
	ON_BN_CLICKED(IDNO, OnNo)
	ON_BN_CLICKED(IDRETRY, OnRetry)
	ON_BN_CLICKED(IDYES, OnYes)
	//}}AFX_MSG_MAP
	ON_WM_DRAWITEM()
	ON_WM_CTLCOLOR()
	ON_WM_KEYUP()
END_MESSAGE_MAP()

static void Add (CArray <UINT, UINT> & v, UINT n)
{
	if (Find (v, n) == -1)
		v.Add (n);
}

BOOL CMsgBoxDlg::OnInitDialog()
{
	CArray <UINT, UINT> v;
	UINT nType = m_nType & 0x0F;

	{
		UINT n [] =
		{
			IDCANCEL,
			IDNO,
			IDABORT,
			IDRETRY,
			IDIGNORE,
			IDYES,
			IDOK,
		};

		for (int i = 0; i < ARRAYSIZE (n); i++) 
			if (CWnd * p = GetDlgItem (n [i]))
				p->ShowWindow (SW_HIDE);
	}

	if		(nType == MB_ABORTRETRYIGNORE)	{ Add (v, IDABORT);		Add (v, IDIGNORE);	Add (v, IDRETRY);	}
	else if (nType == MB_HELP)				{																}
	else if (nType == MB_OKCANCEL)			{ Add (v, IDCANCEL);	Add (v, IDOK);							}
	else if (nType == MB_RETRYCANCEL)		{ Add (v, IDCANCEL);	Add (v, IDRETRY);						}
	else if (nType == MB_YESNO)				{ Add (v, IDNO);		Add (v, IDYES);							}
	else if (nType == MB_YESNOCANCEL)		{ Add (v, IDCANCEL);	Add (v, IDNO);		Add (v, IDYES);		}
	else if (nType == MB_OK)				{ Add (v, IDOK);												}
	else									{ Add (v, IDOK);												}

	for (int i = 0; i < v.GetSize (); i++) {
		if (CWnd * p = GetDlgItem (v [i]))
			p->ShowWindow (SW_SHOW);
	}

	{
		CRect rcLeft, rcRight;

		if (CWnd * p = GetDlgItem (IDCANCEL))
			p->GetWindowRect (rcLeft);
		if (CWnd * p = GetDlgItem (IDOK))
			p->GetWindowRect (rcRight);

		ScreenToClient (rcLeft);
		ScreenToClient (rcRight);

		TRACEF (_T ("v.GetSize (): ") + FoxjetDatabase::ToString (v.GetSize ()));

		if (v.GetSize () > 1) {
			const int nCount = v.GetSize ();
			const CPoint ptLeft = rcLeft.CenterPoint ();
			const CPoint ptRight = rcRight.CenterPoint ();
			const int nDistance = ptRight.x - ptLeft.x;
			const int nStep = (int)(double)nDistance / (nCount - 1);
			int x = ptLeft.x + nStep;

			if (v.GetSize () > 0) {
				if (CWnd * p = GetDlgItem (v [0])) 
					p->SetWindowPos (NULL, rcLeft.left, rcLeft.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
			}

			if (v.GetSize () > 1) {
				if (CWnd * p = GetDlgItem (v [v.GetSize () - 1])) 
					p->SetWindowPos (NULL, rcRight.left, rcRight.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
			}

			for (int i = 1; i < v.GetSize () - 1; i++) {
				if (CWnd * p = GetDlgItem (v [i])) {
					CRect rc;

					p->GetWindowRect (rc);

					const CPoint pt = rc.TopLeft ();
					const CPoint ptCenter = rc.CenterPoint ();
					const int nOffset = ptCenter.x - pt.x;

					rc.left = x - nOffset;
					ScreenToClient (rc);
					p->SetWindowPos (NULL, rc.left, rc.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

					x += nStep;
				}
			}
		}
	}

	SetWindowText (m_strTitle);

	if (CStatic * p = (CStatic *)GetDlgItem (LBL_TEXT))
		p->ModifyStyle (0, SS_OWNERDRAW, 0);

	if (CStatic * p = (CStatic *)GetDlgItem (LBL_BMP))
		p->ModifyStyle (0, SS_OWNERDRAW, 0);

	FoxjetCommon::CEliteDlg::OnInitDialog ();

	if (CFont * pFont = GetFont ()) {
		LOGFONT lf;

		ZeroMemory (&lf, sizeof (lf));
		pFont->GetLogFont (&lf);

		lf.lfHeight = (int)(lf.lfHeight * 1.5);
		m_fnt.CreateFontIndirect (&lf);
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMsgBoxDlg message handlers

static HBITMAP GetDesktopBmp ()
{
    int cx = ::GetSystemMetrics (SM_CXSCREEN);
    int cy = ::GetSystemMetrics (SM_CYSCREEN);

    HWND hwnd = ::GetDesktopWindow ();
    HDC hdc = ::GetDC (hwnd);
    HDC hdcMem = ::CreateCompatibleDC (hdc);
    HBITMAP hbmp = ::CreateCompatibleBitmap (hdc, cx, cy);
    
	::SelectObject (hdcMem, hbmp); 
    ::BitBlt (hdcMem, 0, 0, cx, cy, hdc, 0, 0, SRCCOPY); 

    ::ReleaseDC (hwnd, hdc);
    ::DeleteDC (hdcMem);
    
	return hbmp;
}

void CMsgBoxDlg::OnClipboard() 
{
	CString str = m_strMessage;

	str.Replace (_T ("\r\n"), _T ("\n"));
	str.Replace (_T ("\n\r"), _T ("\n"));

	str.Replace (_T ("\n"), _T ("\r\n"));

	{
		OSVERSIONINFO osver = { sizeof (osver) };
		CString strTmp;

		::GetVersionEx (&osver);

		strTmp.Format (
			_T ("dwOSVersionInfoSize: %d\n")
			_T ("dwMajorVersion:      %d\n")
			_T ("dwMinorVersion:      %d\n")
			_T ("dwBuildNumber:       %d\n")
			_T ("dwPlatformId:        %d\n")
			_T ("szCSDVersion:        %s\n")
			_T ("\n"),
			osver.dwOSVersionInfoSize,
			osver.dwMajorVersion,
			osver.dwMinorVersion,
			osver.dwBuildNumber,
			osver.dwPlatformId,
			osver.szCSDVersion);

		str += _T ("\n----------------------------------------\n");
		str += FoxjetCommon::CVersion (FoxjetCommon::verApp).Format () + _T ("\n");
		str += CString (::GetCommandLine ()) + _T ("\n");
		str += strTmp;		
	}


	HGLOBAL hClipboardText = ::GlobalAlloc (GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT, (str.GetLength () + 1) * sizeof (TCHAR));
	LPVOID lpCbTextData = ::GlobalLock (hClipboardText);
	memset (lpCbTextData, 0, (str.GetLength () + 1) * sizeof (TCHAR));
	memcpy (lpCbTextData, (LPCTSTR)str, (str.GetLength () * sizeof (TCHAR)));

	VERIFY (::OpenClipboard (NULL));
	VERIFY (::EmptyClipboard ());
	VERIFY (::SetClipboardData (CF_UNICODETEXT, hClipboardText));
	VERIFY (::SetClipboardData (CF_BITMAP, GetDesktopBmp ()));

	if (hClipboardText) 
		::GlobalUnlock (hClipboardText);

	{
		hClipboardText = ::GlobalAlloc (GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT, str.GetLength () + 1);
		lpCbTextData = ::GlobalLock (hClipboardText);
		memset (lpCbTextData, 0, str.GetLength ());
		memcpy (lpCbTextData, w2a (str), str.GetLength ());
		HBITMAP hBmp = NULL;

		VERIFY (::OpenClipboard (NULL));
		VERIFY (::SetClipboardData (CF_TEXT, hClipboardText));

		if (hClipboardText) 
			::GlobalUnlock (hClipboardText);
	}

	VERIFY (::CloseClipboard ());
}

void CMsgBoxDlg::OnAbort() 
{
	EndDialog (IDABORT);
}

void CMsgBoxDlg::OnIgnore() 
{
	EndDialog (IDIGNORE);
}

void CMsgBoxDlg::OnNo() 
{
	EndDialog (IDNO);
}

void CMsgBoxDlg::OnRetry() 
{
	EndDialog (IDRETRY);
}

void CMsgBoxDlg::OnYes() 
{
	EndDialog (IDYES);
}

void CMsgBoxDlg::OnOK() 
{
	if (CWnd * p = GetDlgItem (IDOK))
		if (p->IsWindowVisible ())
			FoxjetCommon::CEliteDlg::OnOK ();
}

void CMsgBoxDlg::OnCancel() 
{
	if (CWnd * p = GetDlgItem (IDCANCEL))
		if (p->IsWindowVisible ())
			FoxjetCommon::CEliteDlg::OnCancel();
}

void CMsgBoxDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	using namespace Color;
	using namespace FoxjetCommon;

	DRAWITEMSTRUCT dis;
	
	memcpy (&dis, lpDrawItemStruct, sizeof (dis));

	if (dis.CtlID == LBL_BMP) {
		CDC dc;
		int nID = -1;
		UINT nType = m_nType & 0xF0;

		dc.Attach (dis.hDC);

		switch (nType) {
		case MB_ICONEXCLAMATION:
		//case MB_ICONWARNING:
			nID = IDB_EXCLAMATION;
			break;
		case MB_ICONINFORMATION:
		//case MB_ICONASTERISK:
			nID = IDB_INFORMATION;
			break;
		case MB_ICONQUESTION:
			nID = IDB_QUESTION;
			break;
		case MB_ICONSTOP:
		//case MB_ICONERROR:
		//case MB_ICONHAND:
			nID = IDB_ERROR;
			break;
		}

		dc.FillSolidRect (&dis.rcItem, GetDefColor (_T ("background")));

		if (nID != -1) {
			if (!m_bmp.m_hObject)
				m_bmp.LoadBitmap (nID);

			if (m_bmp.m_hObject) {
				CDC dcMem;
				DIBSECTION ds;
				CRect rc = dis.rcItem;

				memset (&ds, 0, sizeof (ds));
				m_bmp.GetObject (sizeof (ds), &ds);

				dcMem.CreateCompatibleDC (&dc);
				CBitmap * pBitmap = dcMem.SelectObject (&m_bmp);
				
				double dZoom = min (1.0, min (rc.Width () / (double)ds.dsBm.bmWidth, rc.Height () / (double)ds.dsBm.bmHeight));
				int cx = (int)floor (dZoom * (double)ds.dsBm.bmWidth);
				int cy = (int)floor (dZoom * (double)ds.dsBm.bmHeight);

				COLORREF rgb = dcMem.GetPixel (0, 0);
				::TransparentBlt (dc, 0, 0, cx, cy, dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, rgb);

				dcMem.SelectObject (pBitmap);
			}
		}

		dc.Detach ();
	}
	else if (dis.CtlID == LBL_TEXT) {
		CDC dc;
		int nID = -1;
		const CRect rc = dis.rcItem;
		CRect rcCalc = dis.rcItem;

		dc.Attach (dis.hDC);
		CFont * pFont = dc.SelectObject (&m_fnt);

		dc.FillSolidRect (&dis.rcItem, GetDefColor (_T ("background")));
		dc.SetBkMode (TRANSPARENT);

		if (!m_strMessageFormatted.GetLength () && rcCalc.Height ()) {
			LOGFONT lf;
			CStringArray vParse, vFormatted;

			m_strMessage.Replace (_T ("\r\n"), _T ("\n"));
			m_strMessage.Replace (_T ("\n\r"), _T ("\n"));
			FoxjetDatabase::Tokenize (m_strMessage, vParse, '\n');

			m_fnt.GetLogFont (&lf);
			const int n = (rc.Height () / abs (lf.lfHeight)) - 4;
			const int nHalf = (int)floor (n / 2.0);

			if (vParse.GetSize () <= n) 
				m_strMessageFormatted = m_strMessage;
			else {
				for (int i = 0; i < nHalf; i++) {
					if (i >= 0 && i < vParse.GetSize ())
						vFormatted.Add (vParse [i]);
				}

				for (int i = 0; i < nHalf; i++) {
					int nIndex = vParse.GetSize () - i - 1;

					if (nIndex >= 0 && nIndex < vParse.GetSize ())
						vFormatted.InsertAt (nHalf, vParse [nIndex]);
				}

				vFormatted.InsertAt (nHalf, _T ("..."));

				for (int i = 0; i < vFormatted.GetSize (); i++) 
					m_strMessageFormatted += vFormatted [i] + _T ("\n");

				TRACEF (m_strMessageFormatted);

				m_strMessageFormatted.TrimRight ();
			}
		}

		dc.DrawText (m_strMessageFormatted, &rcCalc, nDrawTextFlags | DT_CALCRECT);

		rcCalc += CPoint (0, (rc.Height () - rcCalc.Height ()) / 2);
		dc.DrawText (m_strMessageFormatted, rcCalc, nDrawTextFlags);

		dc.SelectObject (pFont);
		dc.Detach ();
	}
	else 
		FoxjetCommon::CEliteDlg::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

HBRUSH CMsgBoxDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = FoxjetCommon::CEliteDlg::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));
	
	if (FoxjetDatabase::IsControl (false)) 
		rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	if (!m_hbrDlg)	
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}

BOOL CMsgBoxDlg::PreTranslateMessage(MSG* pMsg)
{
	struct 
	{
		TCHAR m_c;
		UINT m_nID;
	}
	const mapChar [] =
	{
		{ 'A',		IDABORT,		},
		{ 'I',		IDIGNORE,		},
		{ 'N',		IDNO,			},
		{ 'R',		IDRETRY,		},
		{ 'Y',		IDYES,			},
		{ 0x0d,		IDYES,			},
		{ 0x0d,		IDOK,			},
		{ 0x1b,		IDNO,			},
	//	{ 'C',		IDCONTINUE,		},
	//	{ 'T',		IDTRYAGAIN,		},
	//	{ VK_ESC,	IDCANCEL,		},
	//	{ VK_ENTER,	IDOK,			},
	};

	switch (pMsg->message) {
		case WM_KEYDOWN: 
			m_msgKeyDown = * pMsg;
			break;
		case WM_KEYUP: 
		{
			const TCHAR c = (TCHAR)pMsg->wParam;
			const TCHAR cUpper = _toupper (c);

			TRACEF (CString (c) + _T (" [") + CString (cUpper) + _T ("]"));

			for (int i = 0; i < ARRAYSIZE (mapChar); i++) {
				if (mapChar [i].m_c == c || mapChar [i].m_c == cUpper) {
					if (CWnd * p = GetDlgItem (mapChar [i].m_nID)) {
						if (p->IsWindowVisible ()) {
							if ((mapChar [i].m_nID == IDOK) || ((m_msgKeyDown.wParam == c || m_msgKeyDown.wParam == cUpper)))
								EndDialog (mapChar [i].m_nID);
						}
					}
				}
			}

			/*
			if (c == 0x0d) {
				if (CButton * p = (CButton *)GetDlgItem (IDYES)) {
					if (p->IsWindowVisible ())
						EndDialog (IDYES);
				}
			}
			*/
		}
		break;
	}

	return FoxjetCommon::CEliteDlg::PreTranslateMessage(pMsg);
}