#if !defined(AFX_STROBEEDITDLG_H__AD1AFBF3_FC7E_467C_99A1_FD0C5D9AE6E5__INCLUDED_)
#define AFX_STROBEEDITDLG_H__AD1AFBF3_FC7E_467C_99A1_FD0C5D9AE6E5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StrobeEditDlg.h : header file
//

#include "UtilApi.h"
#include "Database.h"
#include "RoundButtons.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CStrobeEditDlg dialog

namespace FoxjetCommon
{
	class UTIL_API CStrobeEditDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CStrobeEditDlg (FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection, CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CStrobeEditDlg)
		CString	m_strName;
		int		m_nRed;
		int		m_nGreen;
		int		m_nYellow;
		//}}AFX_DATA
		int		m_nDelay;

		bool m_bAPS;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CStrobeEditDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		FoxjetUtils::CRoundButtons m_buttons;
		FoxjetDatabase::COdbcDatabase & m_db;
		const CString m_strRegSection;

		virtual BOOL OnInitDialog();
		// Generated message map functions
		//{{AFX_MSG(CStrobeEditDlg)
			// NOTE: the ClassWizard will add member functions here
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; // namespace FoxjetCommon

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STROBEEDITDLG_H__AD1AFBF3_FC7E_467C_99A1_FD0C5D9AE6E5__INCLUDED_)
