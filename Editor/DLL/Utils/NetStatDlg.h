#if !defined(AFX_NETSTATDLG_H__92D0D47F_515F_4AF1_B7EC_89AA7020F506__INCLUDED_)
#define AFX_NETSTATDLG_H__92D0D47F_515F_4AF1_B7EC_89AA7020F506__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NetStatDlg.h : header file
//

#include "UtilApi.h"
#include "ListCtrlImp.h"

/////////////////////////////////////////////////////////////////////////////
// CNetStatDlg dialog

namespace FoxjetUtils
{
	namespace NetStatDlg
	{
		class CPortItem : public ItiLibrary::ListCtrlImp::CItem
		{ 
		public:
			CPortItem ();
			virtual ~CPortItem ();

			virtual CString GetDispText (int nColumn) const;
			virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;

			CString m_strProtocol;
			CString m_strLocalAddress;
			CString m_strRemoteAddress;
			CString m_strState;
			DWORD m_dwLocalPort;
			DWORD m_dwRemotePort;
			DWORD m_dwOwningPid;
			CString m_strOwner;
		};

		class UTIL_API CNetStatDlg : public CPropertyPage
		{
			//DECLARE_DYNCREATE(CNetStatDlg)


		// Construction
		public:
			CNetStatDlg(const CString & strRegSection);
			~CNetStatDlg();

		// Dialog Data
			//{{AFX_DATA(CNetStatDlg)
				// NOTE - ClassWizard will add data members here.
				//    DO NOT EDIT what you see in these blocks of generated code !
			//}}AFX_DATA

			const CString m_strRegSection;

		// Overrides
			// ClassWizard generate virtual function overrides
			//{{AFX_VIRTUAL(CNetStatDlg)
			protected:
			virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
			//}}AFX_VIRTUAL

		// Implementation
		protected:
			ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
			HBRUSH m_hbrDlg;

			void EnumTcpConnections (UINT nType);
			void EnumUdpConnections (UINT nType);

			// Generated message map functions
			//{{AFX_MSG(CNetStatDlg)
			afx_msg void OnRefresh();
			virtual BOOL OnInitDialog();
		afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
		//}}AFX_MSG
			DECLARE_MESSAGE_MAP()

			afx_msg void OnSave();

		};
	}; //namespace NetStatDlg
}; //namespace FoxjetUtils

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NETSTATDLG_H__92D0D47F_515F_4AF1_B7EC_89AA7020F506__INCLUDED_)
