// ListCtrlImp.cpp: implementation of the CListCtrlImp class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <commctrl.h>
#include "ListCtrlImp.h"
#include "Debug.h"
#include "Registry.h"
#include "Resource.h"
#include "Parse.h"

#define GET_X_LPARAM(lp) ((int)(short)LOWORD(lp))
#define GET_Y_LPARAM(lp) ((int)(short)HIWORD(lp))

/*
#define TreeView_SetItemState(hwndTV, hti, data, _mask) \
{ TVITEM _ms_TVi;\
  _ms_TVi.mask = TVIF_STATE; \
  _ms_TVi.hItem = hti; \
  _ms_TVi.stateMask = _mask;\
  _ms_TVi.state = data;\
  SNDMSG((hwndTV), TVM_SETITEM, 0, (LPARAM)(TV_ITEM *)&_ms_TVi);\
}

#define TreeView_SetCheckState(hwndTV, hti, fCheck) \
  TreeView_SetItemState(hwndTV, hti, INDEXTOSTATEIMAGEMASK((fCheck)?2:1), TVIS_STATEIMAGEMASK)

#define TVM_GETITEMSTATE        (TV_FIRST + 39)
#define TreeView_GetItemState(hwndTV, hti, mask) \
   (UINT)SNDMSG((hwndTV), TVM_GETITEMSTATE, (WPARAM)(hti), (LPARAM)(mask))

#define TreeView_GetCheckState(hwndTV, hti) \
   ((((UINT)(SNDMSG((hwndTV), TVM_GETITEMSTATE, (WPARAM)(hti), TVIS_STATEIMAGEMASK))) >> 12) -1)
*/

using namespace FoxjetCommon;
using namespace ItiLibrary;
using namespace ItiLibrary::ListCtrlImp;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CMapPtrToPtr CListCtrlImp::m_vWndMap (10);

///////////////////////////////////////////////////////////////////////////
CString ToString (const CLongArray & v)
{
	CString str;

	for (int i = 0; i < v.GetSize (); i++)
		str += FoxjetDatabase::ToString (v [i]) + _T (", ");
	return str;
}

class CSelection
{
public:
	CSelection () { Init (); }

	ULONG m_lCtrlID;
	CMap <int, int, int, int> m_map;

	void Init ()
	{
		m_map.RemoveAll ();
		m_lCtrlID = 0;
	}

	void Add (int nItem)
	{
		int nHit = 0;

		m_map.Lookup (nItem, nHit);
		m_map.SetAt (nItem, nHit);
	}

	void Remove (int nItem)
	{
		m_map.RemoveKey (nItem);
	}

	void Hit (int nItem)
	{
		if (nItem != -1) {
			int nHit = 0;

			m_map.Lookup (nItem, nHit);
			m_map.SetAt (nItem, nHit + 1);
		}
	}

	int GetHitCount (int nItem)
	{
		int nHit = 0;

		m_map.Lookup (nItem, nHit);

		return nHit;
	}

	bool IsSelected (int nItem) 
	{ 
		if (int nHit = GetHitCount (nItem))
			return (nHit % 2) ? true : false; 

		return false;
	}

	CLongArray GetSelected (bool bSelected = true)
	{
		CLongArray v;

		for (POSITION pos = m_map.GetStartPosition (); pos; ) {
			CString str;
			int nItem = 0, nHit = 0;

			m_map.GetNextAssoc (pos, nItem, nHit);
			
			bool b = (!(nHit % 2)) ? true : false;

			if (bSelected) {
				if (!b)
					v.Add (nItem);
			}
			else {
				if (b)
					v.Add (nItem);
			}
		}

		return v;
	}

	CString ToString ()
	{
		CStringArray v;

		for (POSITION pos = m_map.GetStartPosition (); pos; ) {
			CString str;
			int nItem = 0, nHit = 0;

			m_map.GetNextAssoc (pos, nItem, nHit);
			str.Format (_T ("%d(%d)"), nItem, nHit);
			v.Add (str);
		}

		CString str = FoxjetDatabase::ToString (v);

		str.Replace (_T ("{"), _T (""));
		str.Replace (_T ("}"), _T (""));
		str.Replace (_T (","), _T (", "));

		return str;
	}

	void Select (CListCtrl & lv)
	{
		/*
		#ifdef _DEBUG
		{
			for (int nItem = 0; nItem < lv.GetItemCount (); nItem++) 
				TRACEF (FoxjetDatabase::ToString (nItem) + (IsSelected (nItem) ? _T (": SELECTED") : _T (": not selected")));
		}
		#endif //_DEBUG
		*/

		lv.SetRedraw (FALSE);
		
		for (int nItem = 0; nItem < lv.GetItemCount (); nItem++) {
			bool bNew = IsSelected (nItem);
			bool bOld = lv.GetItemState (nItem, LVIS_SELECTED) ? true : false;

			if (bNew ^ bOld)
				lv.SetItemState (nItem, LVIS_SELECTED, bNew ? LVIS_SELECTED : 0);
		}

		lv.SetRedraw (TRUE);
	}

	bool IsEnabled () const { return true; }
};

static CArray <CSelection *, CSelection *> vSel;

static CSelection * GetSelectedInternal (ULONG lCtrlID) 
{
	for (int i = 0; i < ::vSel.GetSize (); i++)
		if (::vSel [i]->m_lCtrlID == lCtrlID)
			return ::vSel [i];

	CSelection * p = new CSelection;
	p->m_lCtrlID = lCtrlID;
	::vSel.Add (p);
	return p;
}

#define TRACEITEM(s) Trace ((s), _T (__FILE__), __LINE__)

CString GetState (UINT nState)
{

	struct
	{
		UINT m_n;
		LPCTSTR m_lpsz;
	} static const map [] =
	{
		{ LVIS_CUT,			_T ("LVIS_CUT"),			},
		{ LVIS_DROPHILITED,	_T ("LVIS_DROPHILITED"),	},
		{ LVIS_FOCUSED,		_T ("LVIS_FOCUSED"),		},
		{ LVIS_SELECTED,	_T ("LVIS_SELECTED"),		},
	};
	CStringArray v;

	for (int i = 0; i < ARRAYSIZE (map); i++)
		if (nState & map [i].m_n)
			v.Add (map [i].m_lpsz);

	CString str = FoxjetDatabase::ToString (v);

	str.Replace (_T ("{"), _T (""));
	str.Replace (_T ("}"), _T (""));

	return str;
}

CString GetChanged (UINT nState)
{

	struct
	{
		UINT m_n;
		LPCTSTR m_lpsz;
	} static const map [] =
	{
		{ LVIF_DI_SETITEM,	_T ("LVIF_DI_SETITEM"),		},
		{ LVIF_IMAGE,		_T ("LVIF_IMAGE"),			},
		{ LVIF_INDENT,		_T ("LVIF_INDENT"),			},
		{ LVIF_NORECOMPUTE,	_T ("LVIF_NORECOMPUTE"),	},
		{ LVIF_PARAM,		_T ("LVIF_PARAM"),			},
		{ LVIF_STATE,		_T ("LVIF_STATE"),			},
		{ LVIF_TEXT,		_T ("LVIF_TEXT"),			},
	};
	CStringArray v;

	for (int i = 0; i < ARRAYSIZE (map); i++)
		if (nState & map [i].m_n)
			v.Add (map [i].m_lpsz);

	CString str = FoxjetDatabase::ToString (v);

	str.Replace (_T ("{"), _T (""));
	str.Replace (_T ("}"), _T (""));

	return str;
}

void Trace (LPNMLISTVIEW lp, LPCTSTR lpszFile, ULONG lLine)
{
	CString str;

	str.Format (
		_T ("%s(%d): %d\n") 
		_T ("\thdr.hwndFrom = 0x%p\n")
		_T ("\thdr.idFrom   = %d\n")
		_T ("\thdr.code     = %d\n")
		_T ("\tiItem        = %d\n")
		_T ("\tiSubItem     = %d\n")
		_T ("\tuNewState    = 0x%p (%s)\n")
		_T ("\tuOldState    = 0x%p (%s)\n")
		_T ("\tuChanged     = 0x%p (%s)\n")
		_T ("\tptAction     = (%d, %d)\n")
		_T ("\tlParam       = 0x%p\n"),
		lpszFile, lLine, lp->iItem,
		lp->hdr.hwndFrom,
		lp->hdr.idFrom,
		lp->hdr.code,
		lp->iItem,
		lp->iSubItem,
		lp->uNewState,	GetState (lp->uNewState),
		lp->uOldState,	GetState (lp->uOldState),
		lp->uChanged,	GetChanged (lp->uChanged),
		lp->ptAction.x, lp->ptAction.y,
		lp->lParam);

	::OutputDebugString (str);
}

///////////////////////////////////////////////////////////////////////////
UTIL_API FoxjetCommon::CLongArray ItiLibrary::GetSelectedItems (CListCtrl & lv)
{
	FoxjetCommon::CLongArray sel;

	for (int i = 0; i < lv.GetItemCount (); i++) 
		if (lv.GetItemState (i, LVIS_SELECTED))
			sel.Add (i);

	return sel;
}

UTIL_API FoxjetCommon::CLongArray ItiLibrary::GetCheckedItems (CListCtrl & lv)
{
	FoxjetCommon::CLongArray sel;

	for (int i = 0; i < lv.GetItemCount (); i++) 
		if (ListView_GetCheckState (lv.m_hWnd, i))
			sel.Add (i);

	return sel;
}

UTIL_API FoxjetCommon::CLongArray ItiLibrary::GetSelectedItems (CListBox & lb)
{
	FoxjetCommon::CLongArray sel;

	for (int i = 0; i < lb.GetCount (); i++) 
		if (lb.GetSel (i))
			sel.Add (i);

	return sel;
}

UTIL_API void ItiLibrary::SelectAll (CListCtrl & lv, bool bSelect)
{
	if (CWnd * p = lv.GetParent ()) 
		p->BeginWaitCursor ();

	lv.SetRedraw (false);

	int nCount = lv.GetItemCount();

	for (int i = 0; i < nCount; i++)
		lv.SetItemState (i, bSelect ? (LVIS_SELECTED | LVIS_FOCUSED) : 0, 0xFFFF);

	if (nCount > 0)
		lv.EnsureVisible (nCount - 1, FALSE);

	lv.SetRedraw (true);

	if (CWnd * p = lv.GetParent ()) 
		p->EndWaitCursor ();
}

UTIL_API void ItiLibrary::Select (CListCtrl & lv, int nIndex)
{
	lv.SetItemState (nIndex, lv.GetItemState (nIndex, 0xFFFF) | (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
	lv.EnsureVisible (nIndex, FALSE);
}

UTIL_API bool ItiLibrary::SelectItemData(CComboBox *pCB, DWORD dwData)
{
	if (pCB) {
		for (int i = 0; i < pCB->GetCount (); i++)
			if (pCB->GetItemData (i) == dwData) {
				pCB->SetCurSel (i);
				return true;
			}
	}

	return false;
}

UTIL_API void ItiLibrary::SaveSettings(CListCtrl & lv, int nCols, const CString & strRegSection, const CString & strName) 
{
	const CString strSubkey = _T ("CListCtrl\\");

	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, 
		strRegSection + _T ("\\") + strSubkey + strName,
		_T ("nColumns"), nCols);

	for (int i = 0; i < nCols; i++) {
		CString strColInfo;
		int nWidth = lv.GetColumnWidth (i);

		strColInfo.Format (_T ("nColWidth%d"), i);
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER,
			strRegSection + _T ("\\") + strSubkey + strName,
			strColInfo, nWidth);
	}
}

UTIL_API void ItiLibrary::LoadSettings(CListCtrl & lv, const CString & strRegSection, const CString & strName) 
{
	const CString strSubkey = _T ("CListCtrl\\");
	int nCols = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER,
		strRegSection + _T ("\\") + strSubkey + strName, 
		_T ("nColumns"), 0);

	for (int i = 0; i < nCols; i++) {
		CString strColInfo;
		strColInfo.Format (_T ("nColWidth%d"), i);

		int nWidth = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER,
			strRegSection + _T ("\\") + strSubkey + strName, 
			strColInfo, 80);

		nWidth = max (nWidth, 15);

		lv.SetColumnWidth (i, nWidth);
	}
}

UTIL_API bool ItiLibrary::Export (ItiLibrary::ListCtrlImp::CListCtrlImp & lv, const CString & strFile)
{
	const CString strCrLf = _T ("\r\n");
	const int nCols = lv.GetColumnCount ();	

	if (FILE * fp = _tfopen (strFile, _T ("wb"))) {
		CString str;

		for (int nCol = 0; nCol < nCols; nCol++) { 
			LVCOLUMN col;
			TCHAR sz [256] = { 0 };

			col.mask = LVCF_TEXT;
			col.pszText = sz;
			col.cchTextMax = sizeof (sz);

			if (lv->GetColumn (nCol, &col)) 
				str += _T ("\"") + CString (col.pszText) + _T ("\"");
			
			if (nCol < (nCols - 1))
				str += _T (",");
		}

		str += strCrLf;
		//fwrite (w2a (str), str.GetLength (), 1, fp);
		fwrite ((LPVOID)(LPCTSTR)str, str.GetLength () * sizeof (TCHAR), 1, fp);

		for (int nRow = 0; nRow < lv->GetItemCount (); nRow++) {
			str.Empty ();

			for (int nCol = 0; nCol < nCols; nCol++) {
				bool bNumeric = false;
				
				if (ListCtrlImp::CItem * p = lv.GetCtrlData (nCol))
					bNumeric = p->IsNumeric (nCol);

				if (bNumeric)
					str += lv->GetItemText (nRow, nCol);
				else
					str += _T ("\"") + lv->GetItemText (nRow, nCol) + _T ("\"");

				if (nCol < (nCols - 1))
					str += _T (",");
			}

			str += strCrLf;
			//fwrite (w2a (str), str.GetLength (), 1, fp);
			fwrite ((LPVOID)(LPCTSTR)str, str.GetLength () * sizeof (TCHAR), 1, fp);
		}

		fclose (fp);
		return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////////////

int UTIL_API ItiLibrary::ListCtrlImp::Find(const CArray <CColumn, CColumn>& v, const CString& str)
{
	for (int i = 0; i < v.GetSize(); i++)
		if (v[i].m_str == str)
			return i;

	return -1;
}

///////////////////////////////////////////////////////////////////////////////////////

CItem::CItem ()
{
}

CItem::~CItem ()
{
}

int CItem::Compare (const CItem & rhs, int nColumn) const
{
	return GetDispText (nColumn).Compare (rhs.GetDispText (nColumn));
}

CListCtrlImp::CListCtrlImp () 
:	m_nID (0),
	m_nListCtrlID (0), 
	m_lpParentWndProc (NULL), 
	m_pParent (NULL), 
	m_nColumns (0),
	m_strRegSection (_T ("Software\\FoxJet\\Utils\\Defaults")),
	m_bMultiSelect (false)
{
}

CListCtrlImp::~CListCtrlImp() 
{
}

void CListCtrlImp::Create (UINT nListCtrlID, 
						   CArray <CColumn, CColumn> & vCols, 
						   CWnd * pParent, const CString & strRegSection)
{
	Create (nListCtrlID, vCols, pParent, strRegSection, WindowProc);
}

void CListCtrlImp::Create (UINT nListCtrlID, CArray <CColumn, CColumn> & vCols, CWnd * pParent,
						   const CString & strRegSection, LISTCTRLIMP_WNDPROC lpWndProc)
{
	ASSERT (lpWndProc);
	ASSERT (pParent);
	ASSERT (pParent->GetDlgItem (nListCtrlID));
	ASSERT (vCols.GetSize () > 0);

	if (CSelection * p = GetSelectedInternal (nListCtrlID)) 
		p->Init ();

	HWND hWnd = pParent->m_hWnd;
	CArray <CListCtrlImp *, CListCtrlImp *> v;

	m_pParent = pParent;
	m_nColumns = vCols.GetSize (); 
	m_nListCtrlID = nListCtrlID;
	m_strRegSection = strRegSection;
	
	GetDispatch (hWnd, v);

	// no dispatch array means hWnd's wndproc hasn't been overriden yet
	if (v.GetSize () == 0) 
		m_lpParentWndProc = (WNDPROC)::SetWindowLong (hWnd, 
			GWL_WNDPROC, (LONG)lpWndProc);
	else // multiple list ctrls, only override the wndproc once
		m_lpParentWndProc = v [0]->m_lpParentWndProc;

	ASSERT (m_lpParentWndProc);

	m_nID = v.GetSize ();
	m_vWndMap.SetAt (this, hWnd);

	CListCtrl & lv = GetListCtrl ();
	lv.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	InitHeaders (vCols);
	RefreshCtrlData ();

	m_imglistBlank.Create (1, FoxjetDatabase::GetListCtrlItemHeight (), ILC_COLOR4, 1, 1);
	lv.SetImageList (&m_imglistBlank, LVSIL_SMALL);
}

void CListCtrlImp::Destroy ()
{
	CString strName, strTitle;

	GetListCtrl().SetImageList(NULL, LVSIL_SMALL);

	if (m_lpParentWndProc) {
		HWND hWnd = GetParent ()->m_hWnd;
		LONG lResult = ::SetWindowLong (hWnd, GWL_WNDPROC, 
			(LONG)m_lpParentWndProc);

		ASSERT (lResult);
		m_lpParentWndProc = NULL;
	}

	GetParent ()->GetWindowText (strTitle);
	strName.Format (_T ("%s\\%d"), strTitle, m_nID);
	SaveSettings (GetListCtrl (), GetColumnCount (), m_strRegSection, strName);
	DeleteCtrlData ();
}

void CListCtrlImp::GetDispatch (HWND hWnd, CArray <CListCtrlImp *, CListCtrlImp *> & v)
{
	v.RemoveAll ();

	for (POSITION pos = m_vWndMap.GetStartPosition (); pos != NULL; ) {
		CListCtrlImp * pLCI = NULL;
		HWND hParent = NULL;//hWnd;

		m_vWndMap.GetNextAssoc (pos, (void *&)pLCI, (void *&)hParent);
		ASSERT (pLCI);

		if (hParent == hWnd) 
			v.Add (pLCI);
	}
}


LRESULT CALLBACK CListCtrlImp::WindowProc (HWND hWnd, UINT nMsg, 
										   WPARAM wParam, 
										   LPARAM lParam)
{
	CArray <CListCtrlImp *, CListCtrlImp *> v;

	GetDispatch (hWnd, v);
	ASSERT (v.GetSize ());

	LRESULT lResult = ::CallWindowProc ((WNDPROC)v [0]->m_lpParentWndProc, hWnd, nMsg, wParam, lParam);

	switch (nMsg) {
	case WM_NOTIFY:
		{
			for (int i = 0; i < v.GetSize (); i++) {
				UINT nCtrl = (UINT)wParam;
				CListCtrlImp * pCtrl = v [i];

				if (nCtrl == pCtrl->GetListCtrlID ()) {
					LPNMHDR pnmh = (LPNMHDR)lParam;

					if (pnmh->code == LVN_COLUMNCLICK) {
						LONG lResult = 0;
						pCtrl->OnColumnClick (pnmh, &lResult);
						return lResult;
					}

					if (pCtrl->IsTouchScreenMultiSelect ()) {
						if (pnmh->code == LVN_ITEMCHANGING) {
							if (!(pCtrl->GetListCtrl ().GetStyle () & LVS_SINGLESEL)) {
								if (LPNMLISTVIEW lp = (LPNMLISTVIEW)lParam) {
									if (lp->uChanged & LVIF_STATE) {
										if (CSelection * p = GetSelectedInternal (nCtrl)) {
											if (p->IsEnabled ()) {
												//TRACEITEM (lp);
												//TRACEF (_T ("selected:    ") + ToString (p->GetSelected (true)));

												if (p->IsSelected (lp->iItem)) {
													if (!(lp->uNewState & LVIS_SELECTED) && (lp->uOldState & LVIS_SELECTED)) {
														return TRUE;
													}
												}
												else {
													if ((lp->uNewState & LVIS_SELECTED) && !(lp->uOldState & LVIS_SELECTED)) {
														return TRUE;
													}
												}
											}
										}
									}
								}
							}

							return FALSE;
						}
					}

					if (pnmh->code == LVN_ITEMCHANGED) {
						if (pCtrl->IsTouchScreenMultiSelect ()) {
							if (!(pCtrl->GetListCtrl ().GetStyle () & LVS_SINGLESEL)) {
								if (LPNMLISTVIEW lp = (LPNMLISTVIEW)lParam) {
									if (CSelection * p = GetSelectedInternal (nCtrl)) {
										if (p->IsEnabled ()) {
											p->Select (pCtrl->GetListCtrl ());
										}
									}
								}
							}
						}

						if (pCtrl->GetListCtrl ().GetExtendedStyle () & LVS_EX_CHECKBOXES) {
							if (!pCtrl->m_bmpUnchecked.m_hObject) {
								DIBSECTION ds;

								pCtrl->GetListCtrl ().SetExtendedStyle (pCtrl->GetListCtrl ().GetExtendedStyle () & ~LVS_EX_CHECKBOXES);

								ZeroMemory (&ds, sizeof (ds));
								VERIFY (pCtrl->m_bmpUnchecked.LoadBitmap (IDB_UNCHECKED));
								VERIFY (pCtrl->m_bmpChecked.LoadBitmap (IDB_CHECKED));
								pCtrl->m_bmpChecked.GetObject (sizeof (ds), &ds);

								pCtrl->m_imglist.Create (ds.dsBm.bmWidth, ds.dsBm.bmHeight, ILC_COLOR, 0, 1);
								pCtrl->m_nUncheckedIndex = pCtrl->m_imglist.Add (&pCtrl->m_bmpUnchecked, RGB (0, 0, 0));
								pCtrl->m_nCheckedIndex = pCtrl->m_imglist.Add (&pCtrl->m_bmpChecked, RGB (0, 0, 0));
								pCtrl->GetListCtrl ().SetImageList (&pCtrl->m_imglist, LVSIL_SMALL);
							}
						}

						if (NM_LISTVIEW * pnmlv = (NM_LISTVIEW *)pnmh) {
							/*
							TRACEF (_T ("iItem :    ") + FoxjetDatabase::ToString (pnmlv->iItem ));
							TRACEF (_T ("iSubItem : ") + FoxjetDatabase::ToString (pnmlv->iSubItem ));
							TRACEF (_T ("uNewState: ") + FoxjetDatabase::ToString ((int)pnmlv->uNewState));
							TRACEF (_T ("uOldState: ") + FoxjetDatabase::ToString ((int)pnmlv->uOldState));
							TRACEF (_T ("uChanged:  ") + FoxjetDatabase::ToString ((int)pnmlv->uChanged));
							*/

							if (pnmlv->uNewState & LVIS_STATEIMAGEMASK) {
								int nCheck = ListView_GetCheckState (pnmh->hwndFrom, pnmlv->iItem);
								int nImage = nCheck > 0 ? pCtrl->m_nCheckedIndex : pCtrl->m_nUncheckedIndex;
								LVITEM lvi;

								ZeroMemory (&lvi, sizeof (lvi));
								lvi.mask = LVIF_IMAGE;
								lvi.iItem = pnmlv->iItem;
								lvi.iSubItem = 0;
								lvi.iImage = nImage;

								pCtrl->GetListCtrl ().SetItem (&lvi);
								//TRACEF (_T ("LVIS_STATEIMAGEMASK: ") + FoxjetDatabase::ToString (nCheck));
							}
						}

						return 0;
					}					

					if (pnmh->code == NM_CLICK) {
						LVHITTESTINFO ht = {0};
						DWORD dwpos = GetMessagePos();

						ht.pt.x = GET_X_LPARAM(dwpos);
						ht.pt.y = GET_Y_LPARAM(dwpos);
						::MapWindowPoints (HWND_DESKTOP, pnmh->hwndFrom, &ht.pt, 1);

						ListView_HitTest (pnmh->hwndFrom, &ht);

						if (!(ht.flags & ~LVHT_ONITEMICON)) {
							int nCheck = ListView_GetCheckState (pnmh->hwndFrom, ht.iItem);
							bool bCheck = nCheck > 0 ? false : true;
							int nImage = bCheck ? pCtrl->m_nCheckedIndex : pCtrl->m_nUncheckedIndex;
							LVITEM lvi;

							pCtrl->GetListCtrl ().SetCheck (ht.iItem, bCheck ? 1 : 0);
							nCheck = ListView_GetCheckState (pnmh->hwndFrom, ht.iItem);

							ZeroMemory (&lvi, sizeof (lvi));
							lvi.mask = LVIF_IMAGE;
							lvi.iItem = ht.iItem;
							lvi.iSubItem = 0;
							lvi.iImage = nImage;

							pCtrl->GetListCtrl ().SetItem (&lvi);
						}

						if (pCtrl->IsTouchScreenMultiSelect ()) {
							if (!(pCtrl->GetListCtrl ().GetStyle () & LVS_SINGLESEL)) {
								if (CSelection * p = GetSelectedInternal (nCtrl)) {
									if (p->IsEnabled ()) {
										p->Hit (ht.iItem);

										/*
										#ifdef _DEBUG
										TRACEF (_T ("hit: ") + FoxjetDatabase::ToString (ht.iItem));
										TRACEF (p->ToString ());

										CLongArray vSelected = p->GetSelected (true);
										//CLongArray vUnselected = p->GetSelected (false);

										TRACEF (_T ("selected:    ") + ToString (vSelected));
										//TRACEF (_T ("un-selected: ") + ToString (vUnselected));
										#endif //_DEBUG
										*/

										p->Select (pCtrl->GetListCtrl ());
									}
								}
							}
						}
					}
				}
			}
		}
		break;
	case WM_CLOSE:
	case WM_DESTROY:
		{
			for (int i = 0; i < v.GetSize (); i++) {
				if (i) // only unhook the first one 
					v [i]->m_lpParentWndProc = NULL;

				v [i]->Destroy ();
				m_vWndMap.RemoveKey (v [i]);
			}
		}
		break;
	}

	return lResult;
}

CWnd * CListCtrlImp::GetParent () const
{
	ASSERT (m_pParent);
	return m_pParent;
}

CListCtrl & CListCtrlImp::GetListCtrl () const 
{
	CWnd * pWnd = GetParent ()->GetDlgItem (GetListCtrlID ());
	ASSERT (pWnd);
	return * (CListCtrl *)pWnd;
}

UINT CListCtrlImp::GetListCtrlID () const  
{
	return m_nListCtrlID;
}

int CListCtrlImp::GetColumnCount () const  
{
	return m_nColumns;
}

int CListCtrlImp::GetDataIndex (const CItem * pData) const 
{
	for (int i = 0; i < m_vCtrlData.GetSize (); i++) 
		if (m_vCtrlData [i] == pData)
			return i;

	return -1;
}

void CListCtrlImp::RefreshCtrlData () 
{
	ASSERT (::IsWindow (GetParent ()->m_hWnd));
	CListCtrl & lv = GetListCtrl ();
	CLongArray sel = GetSelectedItems (lv);
	CArray <CItem *, CItem *> v;
	int i;

	for (i = 0; i < m_vCtrlData.GetSize (); i++)
		v.Add (m_vCtrlData [i]);

	lv.DeleteAllItems ();
	m_vCtrlData.RemoveAll ();

	for (i = 0; i < v.GetSize (); i++) 
		InsertCtrlData (v [i], i);

	for (i = 0; i < sel.GetSize (); i++) 
		lv.SetItemState (sel [i], (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
}

bool CListCtrlImp::InsertCtrlData (CItem * pData) 
{
	return InsertCtrlData (pData, GetListCtrl ().GetItemCount ());
}

bool CListCtrlImp::InsertCtrlData (CItem * pData, int nIndex) 
{
	ASSERT (::IsWindow (GetParent ()->m_hWnd));
	CListCtrl & lv = GetListCtrl ();
	LV_ITEM lvi;

	lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM; 
	lvi.iItem = nIndex; 
	lvi.iSubItem = 0; 
	lvi.iImage = 0;
	lvi.pszText = _T (""); 
	lvi.lParam = (LPARAM)pData;

	if (lv.InsertItem (&lvi) != -1) {
		m_vCtrlData.Add (pData);
		return UpdateCtrlData (pData, nIndex);
	}

	return false;
}

bool CListCtrlImp::UpdateCtrlData (CItem * pData, int nIndex)  
{
	ASSERT (::IsWindow (GetParent ()->m_hWnd));
	CListCtrl & lv = GetListCtrl ();

	if (nIndex >= 0 && nIndex < lv.GetItemCount ()) {
		int nItem = GetDataIndex (pData);
		ASSERT (nIndex >= 0 && nIndex < m_vCtrlData.GetSize ());
		ASSERT (nItem >= 0 && nItem < m_vCtrlData.GetSize ());

		for (int i = 0; i < GetColumnCount (); i++)
			lv.SetItemText (nIndex, i, pData->GetDispText (i));

		m_vCtrlData [nItem] = pData;

		return lv.SetItemData (nIndex, (DWORD)pData) ? true : false;
	}

	return false;
}

bool CListCtrlImp::UpdateCtrlData (CItem * pData) 
{
	CListCtrl & lv = GetListCtrl ();

	for (int i = 0; i < lv.GetItemCount (); i++) {
		if (lv.GetItemData (i) == (DWORD)pData) {
			return UpdateCtrlData (pData, i);
		}
	}

	return false;
}

int CALLBACK CListCtrlImp::CompareFunc (LPARAM lParam1, LPARAM lParam2, 
										LPARAM lParam3)
{
	if (lParam1 && lParam2) {
		CItem * pItem1 = (CItem *)lParam1;
		CItem * pItem2 = (CItem *)lParam2;
		int nColumn = (int)lParam3;
		
		return pItem1->Compare (* pItem2, nColumn);
	}

	return 0;
}

int CALLBACK CListCtrlImp::DyslexicCompareFunc(LPARAM lParam1, LPARAM lParam2, 
											   LPARAM lParam3) 
{
	if (lParam1 && lParam2) {
		// note: 1 & 2 are switched because we're cixelsyd
		CItem * pItem1 = (CItem *)lParam2; 
		CItem * pItem2 = (CItem *)lParam1;
		int nColumn = (int)lParam3;
		
		return pItem1->Compare (* pItem2, nColumn);
	}

	return 0;
}

void CListCtrlImp::OnColumnClick (NMHDR* pNMHDR, LRESULT* pResult) 
{
	CListCtrl & lv = GetListCtrl ();

	NM_LISTVIEW * pnmlv = (NM_LISTVIEW *) pNMHDR;
	ASSERT (pnmlv);

	// "pnmlv->iSubItem" is the column to sort on
	int nCol = pnmlv->iSubItem;

	if (nCol == m_sort.m_nLastCol)
		// if the user clicks the same column several times in a row,
		// flip-flop the sort order
		m_sort.m_bReverseOrder = !m_sort.m_bReverseOrder;
	else
		// otherwise, normal sort order
		m_sort.m_bReverseOrder = false;

	if (!m_sort.m_bReverseOrder)
		lv.SortItems (CompareFunc, (DWORD)nCol);
	else
		lv.SortItems (DyslexicCompareFunc, (DWORD)nCol);

	m_sort.m_nLastCol = pnmlv->iSubItem;
	* pResult = 0;
}

void CListCtrlImp::InitHeaders (CArray <CColumn, CColumn> & cols)  
{
	ASSERT (::IsWindow (GetParent ()->m_hWnd));
	CString strName, strTitle;
	CListCtrl & lv = GetListCtrl ();

	for (int i = 0; i < cols.GetSize (); i++) 
		lv.InsertColumn (i, cols [i].m_str, cols [i].m_nFormat, cols [i].m_nWidth);

	GetParent ()->GetWindowText (strTitle);
	strName.Format (_T ("%s\\%d"), strTitle, m_nID);
	LoadSettings (lv, m_strRegSection, strName);
}

CItem * CListCtrlImp::GetCtrlData (int nIndex) const
{
	CListCtrl & lv = GetListCtrl ();

	if (nIndex >= 0 && nIndex < lv.GetItemCount ()) {
		int nItem = GetDataIndex ((CItem *)lv.GetItemData (nIndex));

		if (nItem >= 0 && nItem < m_vCtrlData.GetSize ())
			return m_vCtrlData [nItem];
	}

	return NULL;
}

bool CListCtrlImp::DeleteCtrlData (int nIndex)  
{
	if (::IsWindow (GetParent ()->m_hWnd))  {
		// not a window if being called from the destructor
		CListCtrl & lv = GetListCtrl ();
		CItem * pData = (CItem *)lv.GetItemData (nIndex);

		m_vCtrlData.RemoveAt (GetDataIndex (pData));
		delete pData;

		return lv.DeleteItem (nIndex) ? true : false;
	}

	return false;
}

void CListCtrlImp::DeleteCtrlData ()  
{
	CListCtrl & lv = GetListCtrl ();

	for (int i = 0; i < m_vCtrlData.GetSize (); i++) {
		CItem * p = m_vCtrlData [i];
		delete p;
	}

	m_vCtrlData.RemoveAll ();
	lv.DeleteAllItems ();
}
