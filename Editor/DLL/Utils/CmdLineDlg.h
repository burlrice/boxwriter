#if !defined(AFX_CMDLINEDLG_H__5A72BD03_38B2_4415_AC10_245DDC903FD8__INCLUDED_)
#define AFX_CMDLINEDLG_H__5A72BD03_38B2_4415_AC10_245DDC903FD8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CmdLineDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCmdLineDlg dialog

class CCmdLineDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CCmdLineDlg)

// Construction
public:
	CCmdLineDlg();
	~CCmdLineDlg();

// Dialog Data
	//{{AFX_DATA(CCmdLineDlg)
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

	CStringArray m_v;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CCmdLineDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HBRUSH m_hbrDlg;

	// Generated message map functions
	//{{AFX_MSG(CCmdLineDlg)
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	afx_msg void OnDiagnostics ();

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CMDLINEDLG_H__5A72BD03_38B2_4415_AC10_245DDC903FD8__INCLUDED_)
