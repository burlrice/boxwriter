// SystemDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Utils.h"
#include "SystemDlg.h"
#include "Database.h"
#include "Resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;

/////////////////////////////////////////////////////////////////////////////
// CSystemDlg

IMPLEMENT_DYNAMIC(CSystemDlg, CEnPropertySheet)

CSystemDlg::CSystemDlg(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
:	CEnPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
}

CSystemDlg::CSystemDlg(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
:	CEnPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
}

CSystemDlg::~CSystemDlg()
{
}


BEGIN_MESSAGE_MAP(CSystemDlg, CEnPropertySheet)
	//{{AFX_MSG_MAP(CSystemDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSystemDlg message handlers

BOOL CSystemDlg::OnInitDialog() 
{
	CEnPropertySheet::OnInitDialog();
	
	if (CWnd * p = GetDlgItem (IDHELP))
		p->ShowWindow (SW_SHOW);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

