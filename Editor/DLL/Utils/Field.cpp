#include "stdafx.h"
#include "Utils.h"
#include "Debug.h"
#include "Convert.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetUtils;
using namespace FoxjetCommon;
using namespace Conversion;

////////////////////////////////////////////////////////////////////////////////
typedef CString ( * LPFCTCONVERT) (const CString & str);

static bool IsTrue (const CString & strBoolIp);
static CString StringToLong (const CString & strFrom);
static CString StringToDelimited (const CString & strFrom);
static CString StringToBool (const CString & strFrom);
static CString StringToBoolIp (const CString & strFrom);
static CString DelimitedToString (const CString & strFrom);
static CString DelimitedToLong (const CString & strFrom);
static CString DelimitedToBool (const CString & strFrom);
static CString DelimitedToBoolIp (const CString & strFrom);
static CString LongToString (const CString & strFrom);
static CString LongToDelimited (const CString & strFrom);
static CString LongToBool (const CString & strFrom);
static CString LongToBoolIp (const CString & strFrom);
static CString BoolToString (const CString & strFrom);
static CString BoolToDelimited (const CString & strFrom);
static CString BoolToBoolIp (const CString & strFrom);
static CString BoolIpToDelimited (const CString & strFrom);
static CString BoolIpToLong (const CString & strFrom);
static CString BoolIpToBool (const CString & strFrom);
static CString FontToFontIp (const CString & strFrom);
static CString FontIpToFont (const CString & strFrom);

static bool IsTrue (const CString & strBoolIp)
{
	if (strBoolIp.GetLength ()) 
		if (strBoolIp [0] == 'T' || strBoolIp [0] == 't')
			return true;

	return false;
}

static CString StringToLong (const CString & strFrom)
{
	CString str (strFrom);
	long l = _tcstol (strFrom, NULL, 10);

	str.Format (_T ("%d"), l);

	return str;
}

static CString StringToDelimited (const CString & strFrom)
{
	return _T ("\"") + strFrom + _T ("\"");
}

static CString StringToBool (const CString & strFrom)
{
	long l = _tcstol (strFrom, NULL, 10);

	return l ? _T ("1") : _T ("0");
}

static CString StringToBoolIp (const CString & strFrom)
{
	long l = _tcstol (strFrom, NULL, 10);

	return l ? _T ("T") : _T ("F");
}

static CString DelimitedToString (const CString & strFrom)
{
	CString str;
	int nLen = strFrom.GetLength ();

	if (strFrom [0] == '\"' && strFrom [nLen - 1] == '\"')
		str = strFrom.Mid (1, nLen - 2); 

	return str;
}

static CString DelimitedToLong (const CString & strFrom)
{
	return StringToLong (DelimitedToString (strFrom));
}

static CString DelimitedToBool (const CString & strFrom)
{
	return StringToBool (DelimitedToString (strFrom));
}

static CString DelimitedToBoolIp (const CString & strFrom)
{
	return StringToBoolIp (DelimitedToString (strFrom));
}

static CString LongToString (const CString & strFrom)
{
	CString str;
	long l = _tcstol (strFrom, NULL, 10);

	str.Format (_T ("%d"), l);

	return str;
}

static CString LongToDelimited (const CString & strFrom)
{
	CString str;
	long l = _tcstol (strFrom, NULL, 10);

	str.Format (_T ("%d"), l);

	return StringToDelimited (str);
}

static CString LongToBool (const CString & strFrom)
{
	return StringToBool (LongToString (strFrom));
}

static CString LongToBoolIp (const CString & strFrom)
{
	return StringToBoolIp (LongToString (strFrom));
}

static CString BoolToString (const CString & strFrom)
{
	long l = _tcstol (strFrom, NULL, 10);

	return l ? _T ("1") : _T ("0");
}

static CString BoolToDelimited (const CString & strFrom)
{
	return StringToDelimited (BoolToString (strFrom));
}

static CString BoolToBoolIp (const CString & strFrom)
{
	long l = _tcstol (strFrom, NULL, 10);

	return l ? _T ("T") : _T ("F");
}

static CString BoolIpToDelimited (const CString & strFrom)
{
	return StringToDelimited (IsTrue (strFrom) ? _T ("T") : _T ("F"));
}

static CString BoolIpToLong (const CString & strFrom)
{
	return IsTrue (strFrom) ? _T ("1") : _T ("0");
}

static CString BoolIpToBool (const CString & strFrom)
{
	return IsTrue (strFrom) ? _T ("1") : _T ("0");
}

static CString FontToFontIp (const CString & strFrom)
{
	return _T ("us32x32");
}

static CString FontIpToFont (const CString & strFrom)
{
	return _T ("Courier New");
}

////////////////////////////////////////////////////////////////////////////////

CField::CField (const CString & strName, FIELDTYPE type, const CString & strDefault)
:	m_strName (strName),
	m_type (type),
	m_strDefault (strDefault)
{
}

CField::CField (const CField & rhs)
:	m_strName (rhs.m_strName),
	m_type (rhs.m_type),
	m_strDefault (rhs.m_strDefault)
{
}

CField::CField (const CField & rhs, const CString & strDefault)
:	m_strName (rhs.m_strName),
	m_type (rhs.m_type),
	m_strDefault (strDefault)
{
}

CField & CField::operator = (const CField & rhs)
{
	if (this != &rhs) {
		m_strName		= rhs.m_strName;
		m_type			= rhs.m_type;
		m_strDefault	= rhs.m_strDefault;
	}

	return * this;
}

CField::~CField ()
{
}

const CString & CField::GetName () const
{
	return m_strName;
}

CField::FIELDTYPE CField::GetType () const
{
	return m_type;
}

const CString & CField::GetDefault () const
{
	return m_strDefault;
}

CString CField::Convert (const CString & str, const CField & to) const
{
/*
	FT_STRING,
	FT_STRING_DELIMITED,
	FT_LONG,
	FT_BOOL,
	FT_BOOL_IP,
	FT_FONT,
	FT_FONT_IP,
*/

	if (GetType () != to.GetType ()) {
		struct 
		{
			FIELDTYPE		m_from;
			FIELDTYPE		m_to;
			LPFCTCONVERT	m_pfct;
		} static const map [] = 
		{
			{ FT_STRING,			FT_LONG,				StringToLong		},
			{ FT_STRING,			FT_STRING_DELIMITED,	StringToDelimited	},
			{ FT_STRING,			FT_BOOL,				StringToBool		},
			{ FT_STRING,			FT_BOOL_IP,				StringToBoolIp		},
			{ FT_STRING,			FT_FONT,				NULL				},
			{ FT_STRING,			FT_FONT_IP,				NULL				},
																		
			{ FT_STRING_DELIMITED,	FT_STRING,				DelimitedToString	},
			{ FT_STRING_DELIMITED,	FT_LONG,				DelimitedToLong		},
			{ FT_STRING_DELIMITED,	FT_BOOL,				DelimitedToBool		},
			{ FT_STRING_DELIMITED,	FT_BOOL_IP,				DelimitedToBoolIp	},
			{ FT_STRING_DELIMITED,	FT_FONT,				NULL				},
			{ FT_STRING_DELIMITED,	FT_FONT_IP,				NULL				},

			{ FT_LONG,				FT_STRING,				LongToString		},
			{ FT_LONG,				FT_STRING_DELIMITED,	LongToDelimited		},
			{ FT_LONG,				FT_BOOL,				LongToBool			},
			{ FT_LONG,				FT_BOOL_IP,				LongToBoolIp		},
																				
			{ FT_BOOL,				FT_STRING,				BoolToString		},
			{ FT_BOOL,				FT_STRING_DELIMITED,	BoolToDelimited		},
			{ FT_BOOL,				FT_LONG,				NULL				},
			{ FT_BOOL,				FT_BOOL_IP,				BoolToBoolIp		},
																				
			{ FT_BOOL_IP,			FT_STRING,				NULL				},
			{ FT_BOOL_IP,			FT_STRING_DELIMITED,	BoolIpToDelimited	},
			{ FT_BOOL_IP,			FT_LONG,				BoolIpToLong		},
			{ FT_BOOL_IP,			FT_BOOL,				BoolIpToBool		},
																				
			{ FT_FONT,				FT_STRING,				NULL				},
			{ FT_FONT,				FT_STRING_DELIMITED,	StringToDelimited	},
			{ FT_FONT,				FT_FONT_IP,				FontToFontIp		},
																				
			{ FT_FONT_IP,			FT_STRING,				NULL				},
			{ FT_FONT_IP,			FT_STRING_DELIMITED,	NULL				},
			{ FT_FONT_IP,			FT_FONT,				FontIpToFont		},
		};
		int nCount = sizeof (map) / sizeof (map [0]);
		// NOTE: a NULL entry denotes the conversion is possible, but no change to 'str' is needed

		for (int i = 0; i < nCount; i++) 
			if (map [i].m_from == GetType () && map [i].m_to == to.GetType ())
				if (map [i].m_pfct)
					return (* map [i].m_pfct) (str);
				else
					return str;
	}

	return str;
}
