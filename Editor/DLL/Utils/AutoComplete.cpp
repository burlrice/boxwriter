#include "StdAfx.h"
#include "AutoComplete.h"
#include "Debug.h"
#include "color.h"
#include "Database.h"
#include "Parse.h"

using namespace FoxjetUtils;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define LB_VALUE				100
#define AWM_CHANGE_SEL			WM_APP+1

const CString FoxjetUtils::CAutoEdit::m_strMore = _T ("...");

FoxjetUtils::CAutoEdit::CAutoEdit (CWnd * pParent) 
:	m_pParent (pParent),
	m_bEnabled (true),
	m_nCtrlID (-1),
	m_pAutoList (NULL)
{ 
}

LRESULT FoxjetUtils::CAutoEdit::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_pAutoList) {
		CRect rc, rcAuto;

		GetWindowRect (rc);
		m_pAutoList->GetWindowRect (rcAuto);
		rc.top += rc.Height ();			

		if (rc != rcAuto) {
			//{ CString str; str.Format (_T ("%d, %d (%d, %d)"), rc.left, rc.top, rc.Width (), rc.Height ()); TRACER (str); }
			//m_pAutoList->MoveWindow (rc.left, rc.top, rcAuto.Width (), rcAuto.Height ());
			m_pAutoList->SetWindowPos (NULL, rc.left, rc.top, rcAuto.Width (), rcAuto.Height (), SWP_NOACTIVATE | SWP_NOZORDER | SWP_NOZORDER);
		}
	}

	switch (message) {
	case WM_KEYDOWN:
		m_wLastKey = wParam;
		break;
	case WM_GETDLGCODE:
		if (m_pAutoList) {
			if (m_pAutoList->IsWindowVisible ()) 
				return DLGC_WANTALLKEYS;
		}
		break;
	}

	if (m_pAutoList) {
		switch (message) {
		case WM_CHAR:
			#ifdef _DEBUG
			{ CString str; str.Format (_T ("WM_CHAR: %d, %d"), wParam, lParam); TRACEF (str); }
			#endif

			switch (wParam) {
			case VK_UP:		
			case VK_DOWN:
			case VK_RETURN:
			case VK_TAB:
			case VK_ESCAPE:
				return 0;
			}
			break;
		case WM_KEYDOWN:
			m_wLastKey = wParam;

			switch (wParam) {
			case VK_UP:		
				m_pAutoList->PostMessage (AWM_CHANGE_SEL, -1);
				return 0;
			case VK_DOWN:
				m_pAutoList->PostMessage (AWM_CHANGE_SEL, 1);
				return 0;
			case VK_RETURN:
			case VK_TAB:
				m_pAutoList->SendMessage (AWM_CHANGE_SEL, 0);
				return 0;
			case VK_ESCAPE:
				Dismiss ();
				return 0;
			}
			break;
		}
	}

	LRESULT lResult = CEdit::WindowProc(message, wParam, lParam);

	switch (message) {
	case WM_CHAR:
		if (m_wLastKey != VK_ESCAPE)
			OnValueChanged ();
		break;
	case WM_SETFOCUS:			
		if (!m_pAutoList)
			OnValueChanged ();
		break;
	case WM_KILLFOCUS:
		if (m_pAutoList)
			if (m_pAutoList->m_hWnd != (HWND)wParam)
				Dismiss ();
		break;
	case WM_DESTROY:
		Dismiss ();
		break;
	}

	/*
	switch (message) {
	case WM_CHAR:
	case WM_PAINT:
	case WM_NCPAINT:
	case WM_GETDLGCODE:
	case WM_IME_SETCONTEXT:
	case WM_IME_NOTIFY:
	case WM_GETTEXTLENGTH:
	case EM_GETRECT:
	case EM_GETSEL:
	case EM_CHARFROMPOS:
	case EM_LINEFROMCHAR:
	case EM_LINELENGTH:
	case EM_SETSEL:
	case WM_SETFOCUS:
	case WM_KILLFOCUS:
	case EM_POSFROMCHAR:
	case WM_NCMOUSEMOVE:
	case SPI_SETPENWINDOWS:
		break;
	default:
		{ CString str; str.Format (_T ("CAutoEdit::WindowProc: 0x%p, 0x%p, 0x%p"), message, wParam, lParam); TRACEF (str); }
		break;
	}
	*/

	return lResult;
}

void FoxjetUtils::CAutoEdit::Dismiss()
{
	if (m_pAutoList) {
		delete m_pAutoList;
		m_pAutoList = NULL;
	}
}

void FoxjetUtils::CAutoEdit::Enable (bool bEnable)
{
	m_bEnabled = bEnable;

	if (!m_bEnabled) 
		Dismiss ();
}

void FoxjetUtils::CAutoEdit::OnValueChanged()
{
	if (!m_bEnabled) 
		Dismiss ();
	else {
		CString str;

		GetWindowText (str);

		if (!str.GetLength ()) {
			Dismiss ();
		}
		else {
			std::vector <std::wstring> v;
			m_pParent->BeginWaitCursor ();
			
			try {
				v = OnTyping (str);
			}
			catch (CDBException * e)		{ Enable (false); HANDLEEXCEPTION_TRACEONLY (e); }
			catch (CMemoryException * e)	{ Enable (false); HANDLEEXCEPTION_TRACEONLY (e); }

			m_pParent->EndWaitCursor ();

			if (Find (v, std::wstring (str)) != -1 || v.size () <= 1) 
				Dismiss ();
			else {
				CRect rc;

				GetWindowRect (rc);
				rc.top += rc.Height ();

				Dismiss ();

				m_pAutoList = new CListBoxWnd (this, v, rc);
			}
		}
	}
}

void FoxjetUtils::CAutoEdit::DoDataExchange (CDataExchange * pDX, UINT nID)
{
	DDX_Control (pDX, nID, * this);
	m_nCtrlID = nID;
}

void FoxjetUtils::CAutoEdit::OnAutoComplete (const CString & str)
{
	m_pParent->SetDlgItemText (m_nCtrlID, str);

	if (CEdit * pEdit = (CEdit *)m_pParent->GetDlgItem (m_nCtrlID))
		pEdit->SetSel (str.GetLength (), str.GetLength ());
}

////////////////////////////////////////////////////////////////////////////////////////////////////

FoxjetUtils::CAutoEdit::CListBoxWnd::CListBoxEx::CListBoxEx (const std::vector <std::wstring> & v, CAutoEdit * pParent)
:	m_v (v),
	m_pParent (pParent),
	CListBox ()
{
}

LRESULT FoxjetUtils::CAutoEdit::CListBoxWnd::CListBoxEx::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	LRESULT lResult = CListBox::WindowProc (message, wParam, lParam);

	switch (message) {
	case WM_SHOWWINDOW:
		if (wParam) {
			if (GetCount () != m_v.size ()) {
				ResetContent ();
				
				for (int i = 0; i < (int)m_v.size (); i++) 
					SetItemData (AddString(m_v [i].c_str ()), i);
			}
		}
		break;
	}


	return lResult;
}

void FoxjetUtils::CAutoEdit::CListBoxWnd::CListBoxEx::DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	ASSERT(lpDrawItemStruct->CtlType == ODT_LISTBOX);
	DRAWITEMSTRUCT dis = { 0 };

	if (lpDrawItemStruct) {
		CDC dc;

		memcpy (&dis, lpDrawItemStruct, sizeof (dis)); 
		dc.Attach(dis.hDC);

		if (dis.itemData >= 0 && dis.itemData < m_v.size ()) {
			CString strText = m_v [dis.itemData].c_str ();

			COLORREF crOldTextColor = dc.GetTextColor();
			COLORREF crOldBkColor = dc.GetBkColor();

			if ((dis.itemAction | ODA_SELECT) && (dis.itemState & ODS_SELECTED))
			{
				dc.SetTextColor (::GetSysColor (COLOR_HIGHLIGHTTEXT));
				dc.SetBkColor (::GetSysColor (COLOR_HIGHLIGHT));
				dc.FillSolidRect (&dis.rcItem, ::GetSysColor (COLOR_HIGHLIGHT));
			}
			else
				dc.FillSolidRect(&dis.rcItem, crOldBkColor);

			if ((dis.itemAction | ODA_FOCUS) && (dis.itemState & ODS_FOCUS)) 
				dc.DrawFocusRect (&dis.rcItem);

			dc.DrawText(strText, &dis.rcItem, DT_SINGLELINE | DT_VCENTER);

			dc.SetTextColor(crOldTextColor);
			dc.SetBkColor(crOldBkColor);

		}

		dc.Detach();
	}
}


void FoxjetUtils::CAutoEdit::CListBoxWnd::CListBoxEx::MeasureItem (LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (lpMeasureItemStruct) {
		lpMeasureItemStruct->itemHeight = 32;

		if (m_pParent) {
			if (CFont * p = m_pParent->GetFont()) {
				LOGFONT lf = { 0 };

				p->GetLogFont (&lf);
				lpMeasureItemStruct->itemHeight = abs (lf.lfHeight) * m_pParent->GetHeightFactor ();
			}
		}
	}

	//TRACEF (_T ("lpMeasureItemStruct->itemHeight: ") + FoxjetDatabase::ToString ((int)lpMeasureItemStruct->itemHeight));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

FoxjetUtils::CAutoEdit::CListBoxWnd::CListBoxWnd (CAutoEdit * pParent, const std::vector <std::wstring> & v, const CRect & rc)
:	m_pLB (NULL),
	m_pParent (pParent)
{
	// WS_POPUPWINDOW = WS_POPUP | WS_BORDER | WS_SYSMENU
    //Create (NULL, NULL, WS_POPUPWINDOW /* | WS_CAPTION | WS_SIZEBOX */, rc, NULL, NULL, WS_EX_TOOLWINDOW);
    Create (NULL, NULL, WS_POPUP, rc, NULL, NULL, WS_EX_TOOLWINDOW);
	SetFont (pParent->GetFont ());

    CRect rect;
    GetClientRect(&rect);
	rect.right = rect.left + rc.Width ();

    m_pLB = new CListBoxEx (v, pParent);
    m_pLB->Create (WS_CHILD | WS_VISIBLE | LBS_NOTIFY | LBS_NOINTEGRALHEIGHT | LBS_OWNERDRAWVARIABLE | WS_VSCROLL, rect, this, LB_VALUE);
	m_pLB->SetFont (pParent->GetFont ());

	rect.bottom = rect.top + (m_pLB->GetItemHeight (0) * (v.size () + 1));
	SetWindowPos (&CWnd::wndTopMost, 0, 0, rect.Width (), rect.Height (), SWP_NOMOVE | SWP_NOACTIVATE);
	ShowWindow(SW_SHOWNOACTIVATE);
}

FoxjetUtils::CAutoEdit::CListBoxWnd::~CListBoxWnd ()
{
	if (m_pLB) {
		delete m_pLB;
		m_pLB = NULL;
	}
}

BEGIN_MESSAGE_MAP(FoxjetUtils::CAutoEdit::CListBoxWnd, CFrameWnd)
   //{{AFX_MSG_MAP(CMainFrame)
	ON_WM_SIZE()
	ON_LBN_SELCHANGE (LB_VALUE, OnSelChange)
	ON_WM_NCPAINT()
	ON_MESSAGE (AWM_CHANGE_SEL, OnArrowKey)
   //}}AFX_MSG_MAP
END_MESSAGE_MAP()

void FoxjetUtils::CAutoEdit::CListBoxWnd::OnSize(UINT nType, int cx, int cy)
{   
    CFrameWnd::OnSize(nType, cx, cy);
    
	if (m_pLB) 
		m_pLB->MoveWindow (0, 0, cx, cy);
}

LRESULT FoxjetUtils::CAutoEdit::CListBoxWnd::OnArrowKey (WPARAM wParam, LPARAM lParam)
{
	if (CListBox * p = (CListBox *)GetDlgItem (LB_VALUE)) {
		if (!wParam) 
			OnSelChange ();
		else {
			int nIndex = p->GetCurSel ();

			if (nIndex == LB_ERR) 
				p->SetCurSel (wParam > 0 ? 0 : (p->GetCount () - 1));
			else {
				nIndex += (int)wParam;
				p->SetCurSel (BindTo(nIndex, 0, (p->GetCount () - 1)));
			}
		}
	}

	return 0;
}

int FoxjetUtils::CAutoEdit::CListBoxWnd::CListBoxEx::GetText (int nIndex, LPTSTR lpszBuffer) const
{
	CString str = GetText (nIndex);

	_tcscpy (lpszBuffer, (LPCTSTR)str);

	return str.GetLength ();
}

void FoxjetUtils::CAutoEdit::CListBoxWnd::CListBoxEx::GetText (int nIndex, CString& rString) const
{
	rString = GetText (nIndex);
}

CString FoxjetUtils::CAutoEdit::CListBoxWnd::CListBoxEx::GetText (int nIndex) const
{
	nIndex = GetItemData (nIndex);;

	if (nIndex >= 0 && nIndex < m_v.size ()) 
		return m_v [nIndex].c_str ();

	return _T ("");
}

void FoxjetUtils::CAutoEdit::CListBoxWnd::OnSelChange ()
{
	if (m_pLB) {
		CString str;
		int nIndex = m_pLB->GetCurSel ();

		if (nIndex != LB_ERR) {
			str = m_pLB->GetText (nIndex);

			if (str != CAutoEdit::m_strMore) {
				m_pParent->OnAutoComplete (str);
				ShowWindow (SW_HIDE);
			}
		}
	}
}

void FoxjetUtils::CAutoEdit::CListBoxWnd::OnNcPaint() 
{
	CWindowDC dc (this); 
	CRect rc;

	GetWindowRect (rc);
	rc -= rc.TopLeft ();
	dc.FrameRect (rc, &CBrush (Color::rgbBlack));
}
