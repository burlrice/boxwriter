// CellCtrlImp.cpp: implementation of the CCellCtrlImp class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CellCtrlImp.h"
#include "Debug.h"
#include "OdbcDatabase.h"
#include "OdbcRecordset.h"
#include "Database.h"
#include "Color.h"
#include "Parse.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace ItiLibrary::ListCtrlImp;
using namespace Color;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
CRecordItem::CRecordItem (FoxjetDatabase::COdbcRecordset & rst, int nRow)
:	m_nRow (nRow)
{
	for (int i = 0; i < rst.GetODBCFieldCount (); i++)
		m_vData.Add ((CString)rst.GetFieldValue (i));
}

CRecordItem::~CRecordItem ()
{
}

CString CRecordItem::GetDispText (int nColumn) const 
{
	if (nColumn >= 0 && nColumn < m_vData.GetSize ())
		return m_vData [nColumn];

	return _T ("");
}

int CRecordItem::GetRow () const 
{
	return m_nRow;
}

//////////////////////////////////////////////////////////////////////

CCellCtrlImp::CCellCtrlImp()
:	m_dwFlags (0),
	m_nMaxRows (100),
	m_prst (NULL),
	m_pdb (NULL),
	m_bEOF (false),
	m_nRow (0)
{
	m_selection.m_nRow = -1;
	m_selection.m_nColumn = -1;
}

CCellCtrlImp::~CCellCtrlImp()
{
}

void CCellCtrlImp::Create (UINT nListCtrlID, const CString & strDSN, const CString & strTable,
						   CWnd * pParent, const CString & strRegSection,
						   const CString & strField, int nRow, DWORD dwFlags, int nMaxRows)
{
	m_dwFlags	= dwFlags;
	m_nMaxRows	= nMaxRows;
	m_nRow		= nRow;
	m_strField	= strField;

	if (strDSN.IsEmpty ())
		return;

	try {
//		const CString strSQL = _T ("SELECT * FROM [") + strTable + _T ("]");
		CString strSQL = _T ("SELECT * FROM [") + strTable + _T ("]");

		if (strTable.Find (_T ("SELECT ")) != -1)
			strSQL = strTable;

		m_pdb = new COdbcCache (COdbcDatabase::Find (strDSN));
		m_prst = new COdbcCache (COdbcDatabase::Find (m_pdb->GetDB (), strSQL));

		for (int i = 0; i < m_prst->GetRst ().GetODBCFieldCount (); i++) {
			CODBCFieldInfo info;

			m_prst->GetRst ().GetODBCFieldInfo (i, info);
			m_vCols.Add (CColumn (info.m_strName));
		}

		if (!m_vCols.GetSize ())
			m_vCols.Add (CColumn (_T ("")));

		CListCtrlImp::Create (nListCtrlID, m_vCols, pParent, strRegSection, CCellCtrlImp::WindowProc);
		Fetch ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	ASSERT (GetListCtrl ().GetStyle () & LVS_OWNERDRAWFIXED);
	ASSERT (GetListCtrl ().GetStyle () & LVS_SINGLESEL);
}

void CCellCtrlImp::Fetch ()
{
	GetListCtrl ().BeginWaitCursor ();

	try
	{
		if (m_prst) {
			const int nMaxRows = m_nMaxRows + GetListCtrl ().GetItemCount ();

			if (!(m_prst->GetRst ().GetOpenType () & CRecordset::forwardOnly))
				m_prst->GetRst ().MoveFirst ();

			for (int i = 0; i < GetListCtrl ().GetItemCount (); i++) {
				if (!m_prst->GetRst ().IsEOF ())
					m_prst->GetRst ().MoveNext ();
			}

			for (int i = GetListCtrl ().GetItemCount (); !m_prst->GetRst ().IsEOF (); i++) {
				CRecordItem * pItem = new CRecordItem (m_prst->GetRst (), i);
				
				InsertCtrlData (pItem);

				if (m_nMaxRows > 0 && i >= (nMaxRows - 1))
					break;

				if (i == m_nRow) {
					for (int j = 0; j < m_vCols.GetSize (); j++) {
						CString strCmp = m_vCols [j].m_str;

						if (!m_strField.Compare (strCmp)) {
							m_selection.m_nRow		= m_nRow;
							m_selection.m_nColumn	= j;
							break;
						}
					}
				}

				m_prst->GetRst ().MoveNext ();
			}

			m_bEOF = m_prst->GetRst ().IsEOF () ? true : false;
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	GetListCtrl ().EndWaitCursor ();
}

LRESULT CALLBACK CCellCtrlImp::WindowProc (HWND hWnd, UINT nMsg, WPARAM wParam, LPARAM lParam)
{
	LRESULT lResult = CListCtrlImp::WindowProc (hWnd, nMsg, wParam, lParam);

	CArray <CListCtrlImp *, CListCtrlImp *> v;

	GetDispatch (hWnd, v);

	switch (nMsg) {
	case WM_NOTIFY:
		{
			UINT nCtrl = (UINT)wParam;

			for (int i = 0; i < v.GetSize (); i++) {
				if (nCtrl == v [i]->GetListCtrlID ()) {
					LPNMHDR pnmh = (LPNMHDR)lParam;

					switch (pnmh->code) {
					case LVN_ITEMCHANGED:
						((CCellCtrlImp *)v [i])->OnItemChanged ((LPNMLISTVIEW)lParam);
						break;
					case NM_CLICK:
						((CCellCtrlImp *)v [i])->OnClick ((LPNMITEMACTIVATE)lParam);
						break;
					}
				}
			}
		}
		break;
		case WM_DRAWITEM:
			{
				UINT nCtrl = (UINT)wParam;

				for (int i = 0; i < v.GetSize (); i++) {
					if (nCtrl == v [i]->GetListCtrlID ()) {
						LPDRAWITEMSTRUCT lpdis = (LPDRAWITEMSTRUCT)lParam;

						for (int i = 0; i < v.GetSize (); i++)
							((CCellCtrlImp *)v [i])->DrawItem (lpdis);
					}
				}
			}
			break;
	}

	return lResult;
}

void CCellCtrlImp::OnItemChanged (LPNMLISTVIEW lpnmlv)
{
	if (lpnmlv->uNewState & LVIS_SELECTED) {
		int nOldIndex = m_selection.m_nRow;
		int nNewIndex = lpnmlv->iItem;

		m_selection.m_nRow = lpnmlv->iItem;
		GetListCtrl ().Update (nNewIndex); // repaint
		GetListCtrl ().Update (nOldIndex);
		
		if ((lpnmlv->iItem == (GetListCtrl ().GetItemCount () - 1)) && !m_bEOF)
			Fetch ();
	}
}

void CCellCtrlImp::OnClick (LPNMITEMACTIVATE lpnma)
{
	if (!(m_dwFlags & CELLCTRL_NOCHANGECOLSEL))
		m_selection.m_nColumn = lpnma->iSubItem;
	
	GetListCtrl ().Update (lpnma->iItem); // repaint
}

void CCellCtrlImp::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	ASSERT (lpDrawItemStruct);
	CListCtrl & lv = GetListCtrl ();

	DRAWITEMSTRUCT & item = * lpDrawItemStruct;
	CDC dc;
	const int nOffset = lv.GetScrollPos (SB_HORZ);

	ASSERT (item.hDC);
	dc.Attach (item.hDC);

	dc.FillRect (&item.rcItem, &CBrush (COLOR_WINDOW));
	
	int nWidth = 0, nPrevWidth = 0;
	COLORREF crBack = ::GetSysColor (COLOR_WINDOW);
	COLORREF crFore = ::GetSysColor (COLOR_WINDOWTEXT);
	CBrush brBack (crBack);
	CBrush brFore (crFore);

	// draw the individual cells
	for (int i = 0; i < GetColumnCount (); i++) {
		nPrevWidth += nWidth;
		nWidth = lv.GetColumnWidth (i);
		int nCellLeft  = nPrevWidth - nOffset;
		int nCellRight = nPrevWidth + nWidth - nOffset;
		CRect rcCell (nCellLeft, item.rcItem.top, nCellRight, item.rcItem.bottom);

		if (item.itemID != -1) { // need to draw the item's text
			if (CRecordItem * pData = (CRecordItem *)item.itemData) {
				CRecordItem & data = * pData;
				int nTextWidth = dc.GetTextExtent (data.GetDispText (i)).cx;
				int nFlags = DT_END_ELLIPSIS | DT_NOPREFIX | DT_RIGHT;

				// only want to write over the background if 
				// this item is NOT selected
				if (m_selection.m_nColumn == i && m_selection.m_nRow == data.GetRow ()) {
					dc.FillRect (rcCell, &CBrush (COLOR_HIGHLIGHT));
					dc.SetTextColor (crBack);
				}
				else { // invert the text color
					CBrush brItem (rgbWhite);
					CRect rcAdjust (rcCell);

					// there is a slight gap between the bottom of the header
					// & the first item. this fills it
					if (rcAdjust.top < 20) rcAdjust.top -= 5;

					dc.FillRect (&rcAdjust, &brItem);
					dc.SetTextColor (crFore);
				}

				// this keeps text from adjacent columns from mingling
				rcCell.left += 2;
				rcCell.right -= 5;

				if (rcCell.left < rcCell.right) 
					dc.DrawText (data.GetDispText (i), rcCell, nFlags);
			}
		}
		else
			dc.FillRect (&rcCell, &brBack);
	}

	dc.Detach ();

	if (!m_bEOF) {
		int nMax = GetListCtrl ().GetScrollLimit (SB_VERT);
		int nPos = GetListCtrl ().GetScrollPos (SB_VERT);

		//TRACEF (_T ("SB_VERT: ") + ToString (nPos) + _T (" [") + ToString (nMax) + _T ("]"));

		if (nPos >= nMax)
			Fetch ();
	}
}

void CCellCtrlImp::Destroy ()
{
	CListCtrlImp::Destroy ();

	try 
	{
		if (m_prst) {
			delete m_prst;
			m_prst = NULL;
		}

		if (m_pdb) {
			delete m_pdb;
			m_pdb = NULL;
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
}

