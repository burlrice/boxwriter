#ifndef __EXPORT_H__
#define __EXPORT_H__

#include "Database.h"
#include "OdbcRecordset.h"
#include "CopyArray.h"
#include "UtilApi.h"
#include "FieldDefs.h"

namespace FoxjetCommon
{
	using FoxjetDatabase::COdbcRecordset;

	#define IMPORT_TASKSDELETEEXISTING		0x80000000

	typedef struct
	{
		CString m_strStruct;
		int		m_nParsed;
		int		m_nImported;
	} IMPORTSTRUCT;

	typedef FoxjetCommon::CCopyArray <IMPORTSTRUCT, IMPORTSTRUCT &> CImportArray;

	#ifdef __IPPRINTER__
	void UTIL_API SetMkNextParams (const CString & strLogosDir, const CString & strBmpExt);
	#endif

	bool UTIL_API Export (FoxjetDatabase::COdbcDatabase & database, const CString & strFile, const FoxjetCommon::CLongArray & vTaskIDs, DWORD dwFlags);
	bool UTIL_API Import (FoxjetDatabase::COdbcDatabase & database, const CString & strFile, DWORD dwFlags, CImportArray & vResult);
	int UTIL_API Import (FoxjetDatabase::COdbcDatabase & db, const CStringArray & v, DWORD /* INTERFACE_TABLE_TYPES */ dwFlags);
	UTIL_API void GetBitmapSummary (CArray <FoxjetDatabase::TASKSTRUCT, FoxjetDatabase::TASKSTRUCT &> & vTasks, CMapStringToString & vFiles, CMapStringToString & vDirs);


	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::LINESTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::BOXSTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::BOXLINKSTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::HEADSTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::PANELSTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::OPTIONSSTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::OPTIONSLINKSTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::USERSTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::SECURITYGROUPSTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::SHIFTSTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::REPORTSTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::BCSCANSTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::DELETEDSTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::SETTINGSSTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::IMAGESTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::TASKSTRUCT & s);
	void UTIL_API UpdateRecord (COdbcRecordset & rst, FoxjetDatabase::PRINTERSTRUCT & s); 

	int UTIL_API OverwriteRecord (FoxjetDatabase::COdbcDatabase & db, CArray <FoxjetDatabase::TASKSTRUCT, FoxjetDatabase::TASKSTRUCT &> & v, FoxjetCommon::CImportArray & vImport);
	int UTIL_API OverwriteRecord (FoxjetDatabase::COdbcDatabase & db, CArray <FoxjetDatabase::BOXSTRUCT, FoxjetDatabase::BOXSTRUCT &> & v, FoxjetCommon::CImportArray & vImport);

	int UTIL_API Find (const CString & strStruct, const CImportArray & v); 



	bool UTIL_API SqlInsert (FoxjetDatabase::COdbcDatabase & db, LPCTSTR lpszTable, const CString & strID, ULONG lID);
}; // namespace FoxjetCommon


template <class TYPE>
inline int UpdateRecords (FoxjetDatabase::COdbcDatabase & db, 
						  CArray <TYPE, TYPE &> & v, LPCTSTR lpszTable,
						  bool bDeleteAllExisting, FoxjetCommon::CImportArray * pvImport = NULL)
{
	using namespace FoxjetCommon;

	int nResult = 0;

	//TRACEF (CString ("UpdateRecords: ") + lpszTable + CString (", ") + (bDeleteAllExisting ? "true" : "false"));

	try {
		CString str;

		if (bDeleteAllExisting) {
			COdbcRecordset rst (db);

			str.Format (_T ("DELETE * FROM [%s]"), lpszTable);
			db.ExecuteSQL (str);

			if (!_tcscmp (lpszTable, Tasks::m_lpszTable)) {
				str.Format (_T ("DELETE * FROM [%s]"), Messages::m_lpszTable);
				db.ExecuteSQL (str);
			}

			str.Format (_T ("SELECT * FROM [%s]"), lpszTable);
			rst.Open (str);

			for (int i = 0; i < v.GetSize (); i++) {
				TYPE & obj = v [i];
				const CString strID = GetIDField (obj);
				const ULONG lID = GetID (obj);

				if (strID.GetLength ()) 
					VERIFY (SqlInsert (db, lpszTable, strID, lID));

				UpdateRecord (rst, obj);
				nResult++;
			}
		}
		else {
			for (int i = 0; i < v.GetSize (); i++) {
				TYPE & obj = v [i];
				const CString strID = GetIDField (obj);
				COdbcRecordset rst (db);
				const long lID = GetID (obj);

				if (strID.GetLength () && lID != -1) {
					str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), lpszTable, strID, lID);
					
					if (strID.GetLength ()) {
						CString strDelete;

						strDelete.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), lpszTable, strID, lID);
						db.ExecuteSQL (strDelete);

						VERIFY (SqlInsert (db, lpszTable, strID, lID));
					}

					UpdateRecord (rst, obj);
				}
				else {
					str.Format (_T ("SELECT * FROM [%s]"), lpszTable);
					rst.Open (str, CRecordset::dynaset);
					UpdateRecord (rst, obj);
				}

				nResult++;
			}
		}

		if (!_tcscmp (lpszTable, Settings::m_lpszTable)) {
			CString strSQL;
			CArray <TYPE, TYPE &> vUnique;
			CStringArray vstr;

			{
				COdbcRecordset rst (db);
				strSQL.Format (_T ("SELECT * FROM [%s];"), lpszTable);
				rst.Open (strSQL, CRecordset::dynaset);

				while (!rst.IsEOF ()) {
					TYPE t;

					rst >> t;

					CString str = ToString (t);

					if (Find (vstr, str) == -1) {
						vstr.Add (str);
						vUnique.Add (t);
					}

					rst.MoveNext ();
				}
			}

			strSQL.Format (_T ("DELETE * FROM [%s];"), lpszTable);
			db.ExecuteSQL (strSQL);

			v.RemoveAll ();
			v.Append (vUnique);
			nResult = vUnique.GetSize ();

			{
				COdbcRecordset rst (db);
				strSQL.Format (_T ("SELECT * FROM [%s];"), lpszTable);
				
				rst.SetEditMode (COdbcRecordset::addnew);

				for (int i = 0; i < vUnique.GetSize (); i++) {
					rst << vUnique [i];
				}
			}
		}
	}
	catch (CDBException * e)		
	{ 
		if (pvImport) {
			bool bFound = false;
			CString strKey;
			
			strKey.Format (_T ("%s, %s, %s, %s"),
				a2w (e->GetRuntimeClass ()->m_lpszClassName),
				lpszTable,
				e->m_strError,
				e->m_strStateNativeOrigin);

			for (int i = 0; i < pvImport->GetSize (); i++) {
				IMPORTSTRUCT & s = pvImport->GetAt (i);

				if (s.m_strStruct == strKey) {
					s.m_nParsed++;
					bFound = true;
					pvImport->SetAt (i, s);
					break;
				}
			}

			if (!bFound) {
				IMPORTSTRUCT s;

				s.m_strStruct = strKey;
				s.m_nParsed = 1;
				pvImport->Add (s);
			}
		}

		#ifdef _DEBUG
		HANDLEEXCEPTION (e); 
		int nBreak = 0;
		#else
		HANDLEEXCEPTION_TRACEONLY (e); 
		#endif
	}
	catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }
	
	return nResult;
}

template <class TYPE>
inline bool Parse (CArray <TYPE, TYPE &> & v, const CString & strSrc)
{
	using namespace FoxjetCommon;

	TYPE obj; 

	if (FromString (strSrc, obj)) {
		v.Add (obj); 
		return true;
	}

	return false;
}

template <class TYPE>
inline void Update (CArray <TYPE, TYPE &> & v, int nUpdate, LPCTSTR lpszStruct, FoxjetCommon::CImportArray & vImport)
{
	using namespace FoxjetCommon;

	int nFind = Find (lpszStruct, vImport); 
	
	if (nFind != -1) { 
		IMPORTSTRUCT & i = vImport [nFind]; 
					
		i.m_nImported = nUpdate; 
		
		if (i.m_nParsed != i.m_nImported) 
			TRACEF ("possible import failure on " + CString (lpszStruct));
	} 
}

template <class TYPE>
static bool GetRecords (FoxjetDatabase::COdbcDatabase & database, CArray <TYPE, TYPE &> & v, 
						LPCTSTR lpszTable)
{
	bool bResult = false;

	try
	{
		COdbcRecordset rst (database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s]"), lpszTable);

		rst.Open (str);

		while (!rst.IsEOF ()) {
			TYPE t;

			rst >> t;
			v.Add (t);
			rst.MoveNext ();
		}

		rst.Close ();
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}


#define DECLAREIMPORT(t) CArray < ##t , ##t &> v##t; CMap <ULONG, ULONG, ULONG, ULONG> vID##t;

#define COUNTPARSED(t, vImport) \
	{ \
		using namespace FoxjetCommon; \
		IMPORTSTRUCT i; \
		i.m_strStruct = #t; \
		i.m_nImported = 0; \
		i.m_nParsed = v##t.GetSize (); \
		vImport.Add (i); \
	}
#define PARSERECORD(t, lpszSrc) \
	Parse < ##t > (v##t, lpszSrc);

#define IMPORTRECORDS(db, t, lpszTable, vImport) \
	{ \
		int nUpdate = UpdateRecords < ##t > (db, v##t, lpszTable, true, &vImport); \
		Update < ##t > (v##t, nUpdate, _T (#t), vImport); \
	}

#define ADDRECORD(db, t, lpszTable, vImport) \
	{ \
		int nUpdate = UpdateRecords < ##t > (db, v##t, lpszTable, false, &vImport); \
		Update < ##t > (v##t, nUpdate, _T (#t), vImport); \
	}

#define OVERWRITERECORDS(db, t, vImport) \
	{ \
		int nUpdate = OverwriteRecord (db, v##t, vImport); \
		Update < ##t > (v##t, nUpdate, _T (#t), vImport); \
	}

#define GETRECORDS(db, t, lpszTable, v) \
	{ \
		TRACEF (_T ("GETRECORDS: ")  _T (#t) _T (", ") _T (#lpszTable)); \
		CArray < ##t , ##t &> v##t ; \
		GetRecords < ##t > ((db), v##t , ##lpszTable ); \
		for (int i = 0; i < ( v##t ).GetSize (); i++) \
			v.Add (FoxjetDatabase::ToString (( v##t ) [i])); \
	}

#endif //__EXPORT_H__
