// StrobeEditDlg.cpp : implementation file
//

#include "stdafx.h"
#include "control.h"
#include "StrobeEditDlg.h"
#include "resource.h"
#include "DbTypeDefs.h"
#include "Debug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetUtils;

using FoxjetDatabase::STROBESTATE;
using FoxjetDatabase::HEADERRORTYPE;

/////////////////////////////////////////////////////////////////////////////
// CStrobeEditDlg dialog


CStrobeEditDlg::CStrobeEditDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection, CWnd* pParent /*=NULL*/)
:	m_db (db),
	m_strRegSection (strRegSection),
	#ifdef __WINPRINTER__
		FoxjetCommon::CEliteDlg (IDD_STROBE_EDIT_MATRIX, pParent)
	#else
		FoxjetCommon::CEliteDlg (IDD_STROBE_EDIT_IP, pParent)
	#endif
{
	//{{AFX_DATA_INIT(CStrobeEditDlg)
	m_strName = _T("");
	m_nRed = -1;
	m_nGreen = -1;
	m_nYellow = -1;
	//}}AFX_DATA_INIT
	m_nDelay = 0;
}


void CStrobeEditDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStrobeEditDlg)
	DDX_Text(pDX, TXT_NAME, m_strName);
	DDX_Radio(pDX, RDO_RED_OFF, m_nRed);
	DDX_Radio(pDX, RDO_GREEN_OFF, m_nGreen);
	DDX_Radio(pDX, RDO_YELLOW_OFF, m_nYellow);
	//}}AFX_DATA_MAP

	#ifdef __WINPRINTER__
	if (CWnd * p = GetDlgItem (TXT_DELAY)) {
		if (m_bAPS) {
			DDX_Text(pDX, TXT_DELAY, m_nDelay);
			DDV_MinMaxInt(pDX, m_nDelay, 0, 60);
		}
	}
	#endif
	
	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
}


BEGIN_MESSAGE_MAP(CStrobeEditDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CStrobeEditDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStrobeEditDlg message handlers

BOOL CStrobeEditDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	#ifdef __WINPRINTER__
	if (!m_bAPS) {
		UINT nID [] = 
		{
			LBL_DELAY,
			LBL_DELAY_SECONDS,
			TXT_DELAY,
		};

		for (int i = 0; i < ARRAYSIZE (nID); i++) {
			if (CWnd * p = GetDlgItem (nID [i]))
				p->ShowWindow (SW_HIDE);
		}
	}
	#endif

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
