// Sw0833Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "utils.h"
#include "Sw0833Dlg.h"
#include "Resource.h"
#include "Database.h"
#include "SystemDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetUtils;

/////////////////////////////////////////////////////////////////////////////
// CSw0833Dlg property page

//IMPLEMENT_DYNCREATE(CSw0833Dlg, CPropertyPage)

CSw0833Dlg::CSw0833Dlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection) 
:	m_db (db),
	m_strRegSection (strRegSection),
	m_bContinuousPC (false),
	m_hbrDlg (NULL),
	CPropertyPage(IDD_SW0883_MATRIX)
{
	//{{AFX_DATA_INIT(CSw0833Dlg)
	m_bContinuousPC = FALSE;
	//}}AFX_DATA_INIT

	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CSw0833Dlg::~CSw0833Dlg()
{
	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}
}

void CSw0833Dlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSw0833Dlg)
	DDX_Check(pDX, CHK_CONTINUOUSPHOTOCELL, m_bContinuousPC);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSw0833Dlg, CPropertyPage)
	//{{AFX_MSG_MAP(CSw0833Dlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSw0833Dlg message handlers

BOOL CSw0833Dlg::OnInitDialog ()
{
	CPropertyPage::OnInitDialog ();

	return FALSE;
}

void CSw0833Dlg::OnReset ()
{
}

HBRUSH CSw0833Dlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = ::GetSysColor (COLOR_BTNFACE);//FoxjetUtils::GetButtonColor (_T ("background"));
	
	if (FoxjetDatabase::IsControl (false)) 
		rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	if (!m_hbrDlg)	
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}
