#ifndef __UTILAPI_H__
#define __UTILAPI_H__

#ifdef __BUILD_UTILS__
	#define UTIL_API __declspec(dllexport)
#else
	#define UTIL_API __declspec(dllimport)
#endif //__BUILD_UTILS__

#endif //#ifndef __UTILAPI_H__
