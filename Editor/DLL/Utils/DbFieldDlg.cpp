// DbFieldDlg.cpp : implementation file
//

#include "stdafx.h"
//#include "list.h"
#include "DbFieldDlg.h"
#include "Debug.h"
#include "Resource.h"
#include "Database.h"

using namespace FoxjetUtils;
using namespace FoxjetCommon;
using namespace ItiLibrary;
using namespace ItiLibrary::ListCtrlImp;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDbFieldDlg dialog


CDbFieldDlg::CDbFieldDlg(const CString & strRegSection, CWnd* pParent /*=NULL*/)
:	m_nRow (0),
	m_strRegSection (strRegSection),
	m_dwFlags (0),
	m_nMaxRows (-1),
	FoxjetCommon::CEliteDlg(IDD_DBFIELD_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CDbFieldDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CDbFieldDlg::~CDbFieldDlg ()
{
}

void CDbFieldDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDbFieldDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
}


BEGIN_MESSAGE_MAP(CDbFieldDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDbFieldDlg)
	ON_NOTIFY(NM_DBLCLK, LV_RECORDS, OnDblclkRecords)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDbFieldDlg message handlers

BOOL CDbFieldDlg::OnInitDialog() 
{
	CArray <CColumn, CColumn> vCols;

	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();

	BeginWaitCursor ();
	m_lv.Create (LV_RECORDS, m_strDSN, m_strTable, this, m_strRegSection, m_strField, m_nRow, m_dwFlags, m_nMaxRows); 
	
	//if (IsMatrix ()) 
	{
		if (CFont * p = GetFont ()) {
			LOGFONT lf = { 0 };
	
			p->GetLogFont (&lf);
			int cy = (int)((double)abs (lf.lfHeight) * 2);
			m_imglist.Create (cy, cy, ILC_COLOR4, 10, 10);
			m_lv->SetImageList(&m_imglist, LVSIL_SMALL);
		}
	}
	
	EndWaitCursor ();
	
	if (m_strTitle.GetLength ())
		SetWindowText (m_strTitle);

	return bResult;
}

void CDbFieldDlg::OnDblclkRecords(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnOK ();

	if (pResult)
		*pResult = 0;
}

void CDbFieldDlg::OnOK()
{
	if (m_lv.m_selection.m_nRow != -1 && m_lv.m_selection.m_nColumn != -1) {
		LVCOLUMN col;
		TCHAR sz [MAX_PATH] = { 0 };

		col.mask = LVCF_TEXT;
		col.pszText = sz;
		col.cchTextMax = sizeof (sz);

		m_lv->GetColumn (m_lv.m_selection.m_nColumn, &col);

		m_nRow = m_lv.m_selection.m_nRow;
		m_strField = col.pszText;

		if (CRecordItem * pItem = (CRecordItem *)m_lv.GetCtrlData (m_lv.m_selection.m_nRow)) {
			for (int i = 0; i < m_lv.GetColumnCount (); i++) {
				LVCOLUMN lvi = { 0 };
				TCHAR sz [256] = { 0 };

				lvi.mask = LVCF_TEXT;
				lvi.pszText = sz;
				lvi.cchTextMax = ARRAYSIZE (sz);

				m_lv->GetColumn (i, &lvi);

				//TRACEF (CString (sz) + _T (" --> ") + pItem->GetDispText (i));
				m_mapResult.SetAt (sz, pItem->GetDispText (i));
			}

			m_strSelected = pItem->GetDispText (m_lv.m_selection.m_nColumn);
		}

		FoxjetCommon::CEliteDlg::OnOK ();
	}
	else 
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}
