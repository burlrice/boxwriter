#if !defined(AFX_COMPONENTSDLG_H__A8F6B30A_B6EE_4377_A9F6_F64C54C9CFE7__INCLUDED_)
#define AFX_COMPONENTSDLG_H__A8F6B30A_B6EE_4377_A9F6_F64C54C9CFE7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ComponentsDlg.h : header file
//

#include "Version.h"
#include "RoundButtons.h"
#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////
// CComponentsDlg dialog
namespace ComponentsDlg
{
	class CComponent
	{
	public:
		CComponent ();
		CComponent (const CComponent & rhs);
		virtual ~CComponent ();

		CComponent & operator = (const CComponent &rhs);

		CString					m_strTitle;
		CString					m_strPath;
		CTime					m_tmModified;
		CString					m_strVer;
	};

	class CComponentsDlg : public CPropertyPage
	{
		DECLARE_DYNCREATE(CComponentsDlg)

	// Construction
	public:
		CComponentsDlg();
		~CComponentsDlg();

	// Dialog Data
		//{{AFX_DATA(CComponentsDlg)
			// NOTE - ClassWizard will add data members here.
			//    DO NOT EDIT what you see in these blocks of generated code !
		//}}AFX_DATA


		FoxjetCommon::CVersion	m_ver;
		CString					m_strApp;
		CStringArray			m_vCmdLine;

	// Overrides
		// ClassWizard generate virtual function overrides
		//{{AFX_VIRTUAL(CComponentsDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	protected:
		CImageList m_imgList;

		HTREEITEM InsertItem (const CString & strComponent, HTREEITEM hParent = NULL, int nDepth = 1);
		void EnumDependencies ();
		void Free ();
		void InsertDriver (HTREEITEM hRoot, const CString & strDriver);
		bool GetFixedFileInfo (const CString & strComponent, CComponent * p);


		// Generated message map functions
		//{{AFX_MSG(CAboutDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnSelchangedComponents(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnItemexpandingComponents(NMHDR* pNMHDR, LRESULT* pResult);
		//}}AFX_MSG
		afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnFjComponentOnly ();
		afx_msg void OnRestore ();

		FoxjetUtils::CRoundButtons m_buttons;
		CArray <CComponent *, CComponent *> m_vComponents;
		bool m_bFjOnly;
		HBRUSH m_hbrDlg;

	// Implementation
	protected:
		// Generated message map functions
		//{{AFX_MSG(CComponentsDlg)
		afx_msg void OnDropFiles(HDROP hDropInfo);
		afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()

	};
}; //namespace ComponentsDlg
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMPONENTSDLG_H__A8F6B30A_B6EE_4377_A9F6_F64C54C9CFE7__INCLUDED_)
