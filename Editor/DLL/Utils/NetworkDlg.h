#if !defined(AFX_NETWORKDLG_H__79608A81_A859_4B99_AC7D_47302A786F2B__INCLUDED_)
#define AFX_NETWORKDLG_H__79608A81_A859_4B99_AC7D_47302A786F2B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NetworkDlg.h : header file
//

#include "RoundButtons.h"
#include "OdbcDatabase.h"
#include "UtilApi.h"

namespace FoxjetUtils
{
	/////////////////////////////////////////////////////////////////////////////
	// CNetworkDlg dialog

	class UTIL_API CNetworkDlg : public CPropertyPage
	{
		DECLARE_DYNCREATE(CNetworkDlg)

	// Construction
	public:
		CNetworkDlg();
		~CNetworkDlg();

	// Dialog Data
		//{{AFX_DATA(CNetworkDlg)
		CString	m_strAddr;
		BOOL	m_bEnabled;
		int m_nFreq;
		//}}AFX_DATA

		bool Load (FoxjetDatabase::COdbcDatabase & db);
		bool Save (FoxjetDatabase::COdbcDatabase & db);

	// Overrides
		// ClassWizard generate virtual function overrides
		//{{AFX_VIRTUAL(CNetworkDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:

		CRoundButtons m_buttons;
		int m_nPing;
		CBitmap m_bmpNetwork;
		CBitmap m_bmpOK;
		CBitmap m_bmpNo;

		// Generated message map functions
		//{{AFX_MSG(CNetworkDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnEnabled();
		afx_msg void OnChangeAddr();
		afx_msg void OnTest();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_MSG
		DECLARE_MESSAGE_MAP()

	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NETWORKDLG_H__79608A81_A859_4B99_AC7D_47302A786F2B__INCLUDED_)
