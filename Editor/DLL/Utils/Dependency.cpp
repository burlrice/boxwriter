// Dependency.cpp: implementation of the CDependency class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Debug.h"
#include "Utils.h"
#include "TemplExt.h"
#include "Depends\DependencyList.h"
#include "Depends\PeExe.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetUtils;
using namespace FoxjetCommon;

UTIL_API DWORD FoxjetCommon::GetDependencies (const CString & strFilename, CStringArray & v, bool bFjOnly) 
{
	CString strFile (strFilename);
    PE_EXE peFile( w2a (strFile.GetBuffer (strFile.GetLength ())));       // Get easy access to the executable
     
     if ( FALSE == peFile.IsValid() )    // A valid PE file???
         return (errModuleDependencyList)peFile.GetErrorType();
 
     //
     // Now see if this module imports any other modules.  If so, we need
     // to recurse and add them as well.
     //
     if (0 == peFile.GetDataDirectoryEntrySize( IMAGE_DIRECTORY_ENTRY_IMPORT ))
         return errMDL_NO_ERROR;
 
     // Make a pointer to the imports table
     PIMAGE_IMPORT_DESCRIPTOR pImportDir;
     pImportDir = (PIMAGE_IMPORT_DESCRIPTOR)
         peFile.GetDataDirectoryEntryPointer(IMAGE_DIRECTORY_ENTRY_IMPORT);
     if ( !pImportDir )
         return errMDL_NO_ERROR;
 
     // While there are still non-null IMAGE_IMPORT_DESCRIPTORs...
     while ( pImportDir->Name )
     {
         // Get a pointer to the imported module's base name
         PSTR pszBaseName = (PSTR)peFile.GetReadablePointerFromRVA(pImportDir->Name);

         pImportDir++;   // Advance to next imported module

         if ( !pszBaseName )
             break;
 
		 CString str = pszBaseName;

		 if (bFjOnly) 
			 if (HMODULE hComponent = ::GetModuleHandle (str)) 
				 if (!::GetProcAddress (hComponent, w2a (FJGETDLLVERSIONPROCADDRESS)))
					 continue;

		 if (Find (v, str) == -1)
			 v.Add (str);
     }

	 return errMDL_NO_ERROR;
} 
