// NetStatDlg.cpp : implementation file
//

#include "stdafx.h"
#include "utils.h"

#include <WINSOCK2.H>

#include "NetStatDlg.h"
#include "Resource.h"
#include "CmdLineDlg.h"
#include "Database.h"
#include "Debug.h"
#include "Parse.h"
#include "..\..\Control\Host.h"
#include "Registry.h"
#include "Database.h"
#include "RoundButtons.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetUtils;
using namespace FoxjetCommon;
using namespace ItiLibrary;

using namespace FoxjetUtils::NetStatDlg;

CPortItem::CPortItem ()
{
}

CPortItem::~CPortItem ()
{
}

CString CPortItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0: return m_strOwner;
	case 1:	return m_strProtocol;
	case 2:	return m_strLocalAddress;
	case 3: return m_dwLocalPort == 0xCDCDCDCD ? _T ("") : FoxjetDatabase::ToString (m_dwLocalPort);
	case 4:	return m_strRemoteAddress;
	case 5: return m_dwRemotePort == 0xCDCDCDCD ? _T ("") : FoxjetDatabase::ToString (m_dwRemotePort);
	case 6:	return m_strState;
	}

	return _T ("");
}

int CPortItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	const CPortItem & cmp = (CPortItem &)rhs;

	switch (nColumn) {
	case 3: return m_dwLocalPort - cmp.m_dwLocalPort;
	case 5: return m_dwRemotePort - cmp.m_dwRemotePort;
	}

	return CItem::Compare (rhs, nColumn);
}


/////////////////////////////////////////////////////////////////////////////
// CNetStatDlg property page

//IMPLEMENT_DYNCREATE(CNetStatDlg, CPropertyPage)

CNetStatDlg::CNetStatDlg(const CString & strRegSection) 
:	m_hbrDlg (NULL),
	m_strRegSection (strRegSection),
	CPropertyPage (IDD_NETSTAT)
{
	//{{AFX_DATA_INIT(CNetStatDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	{ // for OpenProcess
		HANDLE hToken;
		LUID sedebugnameValue;
		TOKEN_PRIVILEGES tkp;

		OpenProcessToken (GetCurrentProcess (), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken);
		LookupPrivilegeValue (NULL, SE_DEBUG_NAME, &sedebugnameValue);

		tkp.PrivilegeCount = 1;
		tkp.Privileges[0].Luid = sedebugnameValue;
		tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

		AdjustTokenPrivileges (hToken, FALSE, &tkp, sizeof tkp, NULL, NULL);
		::CloseHandle (hToken);
	}

}

CNetStatDlg::~CNetStatDlg()
{
	if (m_hbrDlg) { 
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}

	TRACEF ("********** TODO: restore from // for OpenProcess **********");
}

void CNetStatDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNetStatDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNetStatDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CNetStatDlg)
	ON_BN_CLICKED(BTN_REFRESH, OnRefresh)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(BTN_SAVE, OnSave)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNetStatDlg message handlers

BOOL CNetStatDlg::OnInitDialog() 
{
	using namespace ItiLibrary::ListCtrlImp;

	CString strSection = m_strRegSection + _T ("\\CNetStatDlg");
	CArray <CColumn, CColumn> vCols;

	CPropertyPage::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_OWNER), 120));
	vCols.Add (CColumn (LoadString (IDS_PROTOCOL), 60));
	vCols.Add (CColumn (LoadString (IDS_LOCALADDRESS), 100));
	vCols.Add (CColumn (LoadString (IDS_LOCALPORT), 60));
	vCols.Add (CColumn (LoadString (IDS_REMOTEADDRESS), 100));
	vCols.Add (CColumn (LoadString (IDS_REMOTEPORT), 60));
	vCols.Add (CColumn (LoadString (IDS_STATE)));
		
	m_lv.Create (LV_ADDR, vCols, this, strSection);	

	OnRefresh ();	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

static CString FormatIpAddr (DWORD dwLocalAddr)
{
	IN_ADDR addr;

	addr.S_un.S_addr = dwLocalAddr;
	return a2w (inet_ntoa (addr));
}

static CString GetProcName (DWORD dwProcessID)
{
	typedef DWORD (CALLBACK * LPGETMODULEBASENAME) (HANDLE hProcess, HMODULE hModule, LPTSTR lpBaseName, DWORD nSize);
	typedef DWORD (CALLBACK * LPGENUMPROCESSMODULES) (HANDLE hProcess, HMODULE *lphModule, DWORD cb, LPDWORD lpcbNeeded);

	TCHAR sz [_MAX_PATH] = { 0 };

	switch (dwProcessID) {
	case 0:	return _T ("System Idle Process");
	case 4: return _T ("System");
	}

	_stprintf (sz, _T ("[%d]"), dwProcessID);

	if (HANDLE h = ::OpenProcess (PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, dwProcessID)) {
		if (HMODULE hlib = ::LoadLibrary (_T ("psapi.dll"))) {
			HMODULE hMod;
			DWORD cbNeeded;

			if (LPGENUMPROCESSMODULES lpEnumProcessModules = (LPGENUMPROCESSMODULES)::GetProcAddress (hlib, ("EnumProcessModules"))) 
				if (lpEnumProcessModules (h, &hMod, sizeof(hMod), &cbNeeded)) 
					if (LPGETMODULEBASENAME lpf = (LPGETMODULEBASENAME)::GetProcAddress (hlib, ("GetModuleBaseNameW"))) 
						(* lpf) (h, hMod, sz, ARRAYSIZE (sz));

			::FreeLibrary (hlib);
		}

		::CloseHandle (h);
	}
	//else TRACEF (FORMATMESSAGE (::GetLastError ()));

	return sz;
}

void CNetStatDlg::OnRefresh() 
{
	m_lv.DeleteCtrlData ();

	EnumTcpConnections (AF_INET);
	EnumTcpConnections (AF_INET6);
	EnumUdpConnections (AF_INET);
	EnumUdpConnections (AF_INET6);

	m_lv->SortItems (m_lv.CompareFunc, (DWORD)3);

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		CPortItem * p = (CPortItem *)m_lv.GetCtrlData (i); 

		switch (p->m_dwLocalPort) {
		case BW_HOST_BROADCAST_PORT:
		case BW_HOST_UDP_PORT:
		case BW_HOST_TCP_PORT:
			TRACEF (_T (" [") + FoxjetDatabase::ToString (ntohs ((USHORT)p->m_dwLocalPort)) + _T ("]"));
			m_lv->SetItemState (i, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);	
			break;
		}
		switch (htons ((USHORT)p->m_dwLocalPort)) {
		case BW_HOST_BROADCAST_PORT:
		case BW_HOST_UDP_PORT:
		case BW_HOST_TCP_PORT:
			TRACEF (FoxjetDatabase::ToString (p->m_dwLocalPort) + _T (" [") + FoxjetDatabase::ToString (ntohs ((USHORT)p->m_dwLocalPort)) + _T ("]"));
			m_lv->SetItemState (i, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);	
			break;
		}
	}
}

void CNetStatDlg::EnumTcpConnections (UINT nType)
{
	typedef enum  {
		TCP_TABLE_BASIC_LISTENER,
		TCP_TABLE_BASIC_CONNECTIONS,
		TCP_TABLE_BASIC_ALL,
		TCP_TABLE_OWNER_PID_LISTENER,
		TCP_TABLE_OWNER_PID_CONNECTIONS,
		TCP_TABLE_OWNER_PID_ALL,
		TCP_TABLE_OWNER_MODULE_LISTENER,
		TCP_TABLE_OWNER_MODULE_CONNECTIONS,
		TCP_TABLE_OWNER_MODULE_ALL 
	} TCP_TABLE_CLASS, *PTCP_TABLE_CLASS;

	typedef struct _MIB_TCPROW_OWNER_PID {
		DWORD dwState;
		DWORD dwLocalAddr;
		DWORD dwLocalPort;
		DWORD dwRemoteAddr;
		DWORD dwRemotePort;
		DWORD dwOwningPid;
	} MIB_TCPROW_OWNER_PID, *PMIB_TCPROW_OWNER_PID;
	
	typedef struct {
		DWORD                dwNumEntries;
		MIB_TCPROW_OWNER_PID table [1];
	} MIB_TCPTABLE_OWNER_PID, *PMIB_TCPTABLE_OWNER_PID;
	
	typedef DWORD (CALLBACK * LPGETEXTCPTABLE) (PVOID pTcpTable, PDWORD pdwSize, BOOL bOrder, ULONG ulAf, TCP_TABLE_CLASS TableClass, ULONG Reserved);

	if (HMODULE hlib = ::LoadLibrary (_T ("iphlpapi.dll"))) {
		if (LPGETEXTCPTABLE lpf = (LPGETEXTCPTABLE)::GetProcAddress (hlib, ("GetExtendedTcpTable"))) {
			DWORD dwSize = 0;
			DWORD dwResult = (* lpf) (NULL, &dwSize, TRUE, nType, TCP_TABLE_OWNER_PID_ALL, 0);

			if (dwResult == ERROR_INSUFFICIENT_BUFFER) {
				if (MIB_TCPTABLE_OWNER_PID * pTable = (MIB_TCPTABLE_OWNER_PID *)malloc (dwSize)) {
					memset (pTable, 0, dwSize);
					dwResult = (* lpf) (pTable, &dwSize, TRUE, nType, TCP_TABLE_OWNER_PID_ALL, 0);

					if (dwResult == NO_ERROR) {
						for (DWORD i = 0; i < pTable->dwNumEntries; i++) {
							CPortItem * p			= new CPortItem ();
						
							p->m_strProtocol		= nType == AF_INET ? _T ("TCP") : _T ("TCPv6");
							p->m_strLocalAddress	= FormatIpAddr (pTable->table [i].dwLocalAddr);
							p->m_dwLocalPort		= pTable->table [i].dwLocalPort;
							p->m_strRemoteAddress	= FormatIpAddr (pTable->table [i].dwRemoteAddr);
							p->m_dwRemotePort		= pTable->table [i].dwRemotePort;
							p->m_dwOwningPid		= pTable->table [i].dwOwningPid;
							p->m_strOwner			= GetProcName (pTable->table [i].dwOwningPid);

							switch (pTable->table [i].dwState) {
							case 1:		p->m_strState = /* _T ("MIB_TCP_STATE_ */ _T ("CLOSED");		break;
							case 2:		p->m_strState = /* _T ("MIB_TCP_STATE_ */ _T ("LISTEN");		break;
							case 3:		p->m_strState = /* _T ("MIB_TCP_STATE_ */ _T ("SYN_SENT");		break;
							case 4:		p->m_strState = /* _T ("MIB_TCP_STATE_ */ _T ("SYN_RCVD");		break;
							case 5:		p->m_strState = /* _T ("MIB_TCP_STATE_ */ _T ("ESTAB");			break;
							case 6:		p->m_strState = /* _T ("MIB_TCP_STATE_ */ _T ("FIN_WAIT1");		break;
							case 7:		p->m_strState = /* _T ("MIB_TCP_STATE_ */ _T ("FIN_WAIT2");		break;
							case 8:		p->m_strState = /* _T ("MIB_TCP_STATE_ */ _T ("CLOSE_WAIT");	break;
							case 9:		p->m_strState = /* _T ("MIB_TCP_STATE_ */ _T ("CLOSING");		break;
							case 10:	p->m_strState = /* _T ("MIB_TCP_STATE_ */ _T ("LAST_ACK");		break;
							case 11:	p->m_strState = /* _T ("MIB_TCP_STATE_ */ _T ("TIME_WAIT");		break;
							case 12:	p->m_strState = /* _T ("MIB_TCP_STATE_ */ _T ("DELETE_TCB");	break;
							}

							m_lv.InsertCtrlData (p);
						}
					}

					free (pTable);
				}
			}
		}

		::FreeLibrary (hlib);
	}
}

void CNetStatDlg::EnumUdpConnections (UINT nType)
{
	typedef enum  {
		UDP_TABLE_BASIC,
		UDP_TABLE_OWNER_PID,
		UDP_TABLE_OWNER_MODULE 
	} UDP_TABLE_CLASS, *PUDP_TABLE_CLASS;

	typedef struct _MIB_UDPROW_OWNER_PID {
		DWORD dwLocalAddr;
		DWORD dwLocalPort;
		DWORD dwOwningPid;
	} MIB_UDPROW_OWNER_PID, *PMIB_UDPROW_OWNER_PID;

	typedef struct _MIB_UDPTABLE_OWNER_PID {
		DWORD                dwNumEntries;
		MIB_UDPROW_OWNER_PID table[1];
	} MIB_UDPTABLE_OWNER_PID, *PMIB_UDPTABLE_OWNER_PID;

	typedef DWORD (CALLBACK * LPGETEXUDPTABLE) (PVOID pTcpTable, PDWORD pdwSize, BOOL bOrder, ULONG ulAf, UDP_TABLE_CLASS TableClass, ULONG Reserved);

	if (HMODULE hlib = ::LoadLibrary (_T ("iphlpapi.dll"))) {
		if (LPGETEXUDPTABLE lpf = (LPGETEXUDPTABLE)::GetProcAddress (hlib, ("GetExtendedUdpTable"))) {
			DWORD dwSize = 0;
			DWORD dwResult = (* lpf) (NULL, &dwSize, TRUE, nType, UDP_TABLE_OWNER_PID, 0);

			if (dwResult == ERROR_INSUFFICIENT_BUFFER) {
				if (MIB_UDPTABLE_OWNER_PID * pTable = (MIB_UDPTABLE_OWNER_PID *)malloc (dwSize)) {
					memset (pTable, 0, dwSize);
					dwResult = (* lpf) (pTable, &dwSize, TRUE, nType, UDP_TABLE_OWNER_PID, 0);

					if (dwResult == NO_ERROR) {
						for (DWORD i = 0; i < pTable->dwNumEntries; i++) {
							CPortItem * p			= new CPortItem ();
						
							p->m_strProtocol		= nType == AF_INET ? _T ("UDP") : _T ("UDPv6");
							p->m_strLocalAddress	= FormatIpAddr (pTable->table [i].dwLocalAddr);
							p->m_dwLocalPort		= pTable->table [i].dwLocalPort;
							p->m_dwOwningPid		= pTable->table [i].dwOwningPid;
							p->m_strOwner			= GetProcName (pTable->table [i].dwOwningPid);

							m_lv.InsertCtrlData (p);
						}
					}

					free (pTable);
				}
			}
		}

		::FreeLibrary (hlib);
	}
}

void CNetStatDlg::OnSave ()
{
	CString strFile = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection + _T ("\\CNetStatDlg"), 
		_T ("Export"), FoxjetDatabase::GetHomeDir () + _T ("\\NETSTAT.txt"));
	CFileDialog dlg (FALSE, _T ("*.txt"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
		_T ("Text files (*.txt)|*.txt|All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();
		FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, m_strRegSection + _T ("\\CNetStatDlg"), 
			_T ("Export"), strFile);
		VERIFY (ItiLibrary::Export (m_lv, strFile));
	}
}

HBRUSH CNetStatDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = ::GetSysColor (COLOR_BTNFACE);//FoxjetUtils::GetButtonColor (_T ("background"));
	
	if (FoxjetDatabase::IsControl (false)) 
		rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	if (!m_hbrDlg)	
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}
