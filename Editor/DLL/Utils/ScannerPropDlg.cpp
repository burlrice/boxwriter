// ScannerPropDlg.cpp : implementation file
//

#include "stdafx.h"
#include "utils.h"
#include "ScannerPropDlg.h"
#include "Parse.h"
#include "Debug.h"
#include "Utils.h"
#include "Resource.h"
#include "Parse.h"
#include "SystemDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetUtils;

/////////////////////////////////////////////////////////////////////////////
// CScannerPropDlg property page

//IMPLEMENT_DYNCREATE(CScannerPropDlg, CPropertyPage)

CScannerPropDlg::CScannerPropDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection) 
:	m_db (db),
	m_strRegSection (strRegSection),
	m_hbrDlg (NULL),
	CPropertyPage(IDD_FIXEDSCANNER_MATRIX)
{
	//{{AFX_DATA_INIT(CScannerPropDlg)
	//}}AFX_DATA_INIT
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
	Init (m_scanner);
}

CScannerPropDlg::~CScannerPropDlg()
{
	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}
}

void CScannerPropDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CScannerPropDlg)
	DDX_Check(pDX, CHK_IDLE, m_scanner.m_bIdle);
	DDX_Check(pDX, CHK_RESET, m_scanner.m_bReset);
	DDX_Text(pDX, TXT_NOREADS, m_scanner.m_nNoReads);
	DDV_MinMaxInt(pDX, m_scanner.m_nNoReads, 0, 99);
	DDX_Text(pDX, TXT_NOREADSTRING, m_scanner.m_strNoRead);
	//}}AFX_DATA_MAP

	if (!m_buttons.m_prgbBack)
		m_buttons.m_prgbBack = new COLORREF (::GetSysColor (COLOR_BTNFACE)); //FoxjetUtils::GetButtonColor (_T ("background")));

	if (FoxjetDatabase::IsControl (false)) 
		* m_buttons.m_prgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	m_buttons.DoDataExchange (pDX, BTN_RESETSCANNER);
	m_buttons.DoDataExchange (pDX, BTN_RESETINFO);
}


BEGIN_MESSAGE_MAP(CScannerPropDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CScannerPropDlg)
	ON_BN_CLICKED(BTN_RESETINFO, OnResetinfo)
	ON_BN_CLICKED(BTN_RESETSCANNER, OnResetscanner)
	//}}AFX_MSG_MAP
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CScannerPropDlg message handlers

void CScannerPropDlg::Init (FIXEDSCANNERSTRUCT & s)
{
	s.m_strNoRead	= _T("NO READ");
	s.m_nNoReads	= 3;
	s.m_bIdle		= TRUE;
	s.m_bReset		= TRUE;
}

CString CScannerPropDlg::ToString (const FIXEDSCANNERSTRUCT & s) 
{
	CStringArray v;

	v.Add (s.m_strNoRead);
	v.Add (FoxjetDatabase::ToString (s.m_nNoReads));
	v.Add (FoxjetDatabase::ToString ((int)s.m_bIdle));
	v.Add (FoxjetDatabase::ToString ((int)s.m_bReset));

	return FoxjetDatabase::ToString (v);
}

bool CScannerPropDlg::FromString (const CString & str, FIXEDSCANNERSTRUCT & s)
{
	CStringArray v;

 	FoxjetDatabase::FromString (str, v);

	if (v.GetSize () >= 4) {
		int nIndex = 0;

		s.m_strNoRead		= v [nIndex++];
		s.m_nNoReads		= _ttoi (v [nIndex++]);
		s.m_bIdle			= _ttoi (v [nIndex++]);
		s.m_bReset		= _ttoi (v [nIndex++]);

		return true;
	}

	return false;
}

void CScannerPropDlg::OnResetinfo() 
{
	DWORD dwResult = 0;

	LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST,
		WM_RESETSCANNERINFO, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 10000, &dwResult);
}

void CScannerPropDlg::OnResetscanner() 
{
	DWORD dwResult = 0;

	LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST,
		WM_RESETSCANNERERROR, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 10000, &dwResult);
}

HBRUSH CScannerPropDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = ::GetSysColor (COLOR_BTNFACE);//FoxjetUtils::GetButtonColor (_T ("background"));
	
	if (FoxjetDatabase::IsControl (false)) 
		rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	if (!m_hbrDlg)	
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}
