// NetworkDlg.cpp : implementation file
//

#include "stdafx.h"
#include "utils.h"
#include "NetworkDlg.h"
#include "Resource.h"
#include "TemplExt.h"
#include "Database.h"
#include "Parse.h"
#include "SystemDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;
using namespace FoxjetUtils;

/////////////////////////////////////////////////////////////////////////////
// CNetworkDlg property page

IMPLEMENT_DYNCREATE(CNetworkDlg, CPropertyPage)

CNetworkDlg::CNetworkDlg() 
:	m_nPing (-1),
	CPropertyPage(IDD_NETWORK)
{
	m_psp.dwFlags |= PSH_USECALLBACK;
	m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;

	//{{AFX_DATA_INIT(CNetworkDlg)
	m_strAddr = _T("");
	m_bEnabled = FALSE;
	m_nFreq = 5 * 60;
	//}}AFX_DATA_INIT

	m_bmpNetwork.LoadBitmap (IDB_NETWORK);
	m_bmpOK.LoadBitmap (IDB_OK);
	m_bmpNo.LoadBitmap (IDB_NO);
}

CNetworkDlg::~CNetworkDlg()
{
}

void CNetworkDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNetworkDlg)
	DDX_Text(pDX, TXT_ADDR, m_strAddr);
	DDX_Check(pDX, CHK_ENABLED, m_bEnabled);
	//}}AFX_DATA_MAP

	//if (IsMatrix ())
		m_buttons.DoDataExchange (pDX, BTN_TEST);

	if (pDX->m_bSaveAndValidate) {
		if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_FREQ)) 
			m_nFreq = pCB->GetItemData (pCB->GetCurSel ());

		if (m_strAddr.IsEmpty ())
			m_bEnabled = FALSE;
	}
}


BEGIN_MESSAGE_MAP(CNetworkDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CNetworkDlg)
	ON_BN_CLICKED(CHK_ENABLED, OnEnabled)
	ON_EN_CHANGE(TXT_ADDR, OnChangeAddr)
	ON_BN_CLICKED(BTN_TEST, OnTest)
	ON_WM_DRAWITEM()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNetworkDlg message handlers

BOOL CNetworkDlg::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_FREQ)) {
		int n [] =
		{
		//	5,
			15,
			30,
			60,
			5 * 60,
			10 * 60,
			15 * 60,
			20 * 60,
			30 * 60,
			60 * 60,
		};

		for (int i = 0; i < ARRAYSIZE (n); i++) {
			CTimeSpan tm (0, 0, 0, n [i]);
			CString str = tm.Format (_T ("%H:%M:%S"));
			int nIndex = pCB->AddString (str);
			pCB->SetItemData (nIndex, n [i]);

			if (m_nFreq == n [i])
				pCB->SetCurSel (nIndex);
		}

		if (pCB->GetCurSel () == -1)
			pCB->SetCurSel (4);
	}
	
	if (m_strAddr.IsEmpty ())
		m_bEnabled = FALSE;

	if (CStatic * p = (CStatic *)GetDlgItem (LBL_STATE))
		p->ModifyStyle (0, SS_OWNERDRAW, 0);

	OnEnabled ();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CNetworkDlg::OnEnabled() 
{
	if (CButton * pEnabled = (CButton *)GetDlgItem (CHK_ENABLED)) {
		UINT n [] = { TXT_ADDR, CB_FREQ, BTN_TEST, LBL_STATE };

		for (int i = 0; i < ARRAYSIZE (n); i++)
			if (CWnd * p = GetDlgItem (n [i]))
				p->EnableWindow (pEnabled->GetCheck () == 1);
	}	
}

bool CNetworkDlg::Load (FoxjetDatabase::COdbcDatabase & db)
{
	SETTINGSSTRUCT s;

	if (GetSettingsRecord (db, GetFirstLineID (db, GetPrinterID (db)), _T ("Network"), s)) {
		CStringArray v;
		int i = 0;

		FoxjetDatabase::FromString (s.m_strData, v);

		m_strAddr	= GetParam (v, i++);
		m_bEnabled	= _ttoi (GetParam (v, i++)) ? true : false;
		m_nFreq		= _tcstoul (GetParam (v, i++), NULL, 10);

		return true;
	}

	return false;
}

bool CNetworkDlg::Save (FoxjetDatabase::COdbcDatabase & db)
{
	SETTINGSSTRUCT s;
	CStringArray v;

	v.Add (m_strAddr);
	v.Add (ToString ((int)m_bEnabled));
	v.Add (ToString ((int)m_nFreq));

	s.m_strKey	= _T ("Network");
	s.m_lLineID = GetFirstLineID (db, GetPrinterID (db));
	s.m_strData = FoxjetDatabase::ToString (v);

	if (!UpdateSettingsRecord (db, s))
		return AddSettingsRecord (db, s);

	return true;
}

void CNetworkDlg::OnChangeAddr() 
{
}

void CNetworkDlg::OnTest() 
{
	CString str;
	
	GetDlgItemText (TXT_ADDR, str);

	BeginWaitCursor ();
	m_nPing = FoxjetCommon::Ping (str, 1) ? 1 : 0;
	
	if (CWnd * p = GetDlgItem (LBL_STATE)) 
		REPAINT (p);

	EndWaitCursor ();
}

void CNetworkDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpdis) 
{
	if (nIDCtl == LBL_STATE) {
		COLORREF rgbBack = IsControl (false) ? FoxjetUtils::GetButtonColor (_T ("background")) : ::GetSysColor (COLOR_BTNFACE);
		CRect rc = lpdis->rcItem;
		DIBSECTION ds;
		CDC dcMem;

		dcMem.CreateCompatibleDC (CDC::FromHandle (lpdis->hDC));
		memset (&ds, 0, sizeof (ds));
		::GetObject (m_bmpNetwork.m_hObject, sizeof (ds), &ds);

		CBitmap * pBmp = dcMem.SelectObject (&m_bmpNetwork);
		COLORREF rgb = dcMem.GetPixel (0, 0);
		CSize size = CSize (ds.dsBm.bmWidth, ds.dsBm.bmHeight);
		::FillRect (lpdis->hDC, rc, CBrush (rgbBack));

		if (CWnd * p = GetDlgItem (nIDCtl)) {
			if (p->IsWindowEnabled ()) {
				if (m_nPing >= 0) {
					double dZoom = min (1.0, min (rc.Width () / (double)ds.dsBm.bmWidth, rc.Height () / (double)ds.dsBm.bmHeight));
					int cx = (int)floor (dZoom * (double)ds.dsBm.bmWidth);
					int cy = (int)floor (dZoom * (double)ds.dsBm.bmHeight);

					::TransparentBlt (lpdis->hDC, rc.left, rc.top, cx, cy, dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, rgb);
					
					CBitmap * p = NULL;
					
					if (m_nPing == 1)
						p = &m_bmpOK;
					else if (m_nPing == 0)
						p = &m_bmpNo;

					if (p) {
						::GetObject (p->m_hObject, sizeof (ds), &ds);

						dZoom = min (1.0, min (rc.Width () / (double)ds.dsBm.bmWidth, rc.Height () / (double)ds.dsBm.bmHeight));
						cx = (int)floor (dZoom * (double)ds.dsBm.bmWidth);
						cy = (int)floor (dZoom * (double)ds.dsBm.bmHeight);

						dcMem.SelectObject (p);
						COLORREF rgb = dcMem.GetPixel (0, 0);
						::TransparentBlt (lpdis->hDC, rc.left, rc.top, cx / 2, cy / 2, dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, rgb);
					}
				}
			}
		}

		dcMem.SelectObject (pBmp);
	}

	CPropertyPage::OnDrawItem(nIDCtl, lpdis);
}
