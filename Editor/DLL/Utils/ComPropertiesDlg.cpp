// ComProperitesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ComPropertiesDlg.h"
#include "Resource.h"
#include "Utils.h"
#include "Debug.h"
#include "..\..\..\Control\TypeDefs.h"
#include "TemplExt.h"
#include "Parse.h"
#include "SystemDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetUtils;
using namespace FoxjetDatabase;

bool UTIL_API FoxjetCommon::EditSerialParams (CString & str, bool bWin32) 
{
	CPropertySheet sheet;
	CComPropertiesDlg dlg;
	CStringArray v;
	CString strValues [] = 
	{
		_T ("9600"),
		_T ("8"),
		_T ("N"),
		_T ("1"),
		_T ("0"),
		_T ("0"),
	};

	Tokenize (str, v, ',');

	for (int i = 0; i < min (v.GetSize (), (int)(ARRAYSIZE (strValues))); i++) 
		if (v [i].GetLength ())
			strValues [i] = v [i];

	dlg.m_bWin32		= bWin32;
	dlg.m_lBaud			= atol (CAnsiString (strValues [0]));
	dlg.m_nDatabits		= atoi (CAnsiString (strValues [1]));
	dlg.m_cParity		= (char)strValues [2].GetAt (0);
	dlg.m_nStopBits		= atoi (CAnsiString (strValues [3]));
	dlg.m_nDeviceType	= atoi (CAnsiString (strValues [4]));
	dlg.m_bPrintOnce	= atoi (CAnsiString (strValues [5])) ? true : false;
	
	sheet.AddPage (&dlg);
	sheet.SetTitle (LoadString (IDS_SERIALSETTINGS));

	if (sheet.DoModal() == IDOK) {
		str.Format (_T ("%i,%i,%c,%i,%i,%i"), 
			dlg.m_lBaud, dlg.m_nDatabits, dlg.m_cParity, dlg.m_nStopBits, dlg.m_nDeviceType, dlg.m_bPrintOnce);
		return true;
	}

	return false;
}

void UTIL_API FoxjetCommon::EnumerateSerialPorts(CUIntArray& ports)
{
	//const int nCom2 = 2; // COM2 is touchscreen

  //Make sure we clear out any elements which may already be in the array
  ports.RemoveAll();

  //Determine what OS we are running on
  OSVERSIONINFO osvi;
  osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
  BOOL bGetVer = GetVersionEx(&osvi);

  //On NT use the QueryDosDevice API
  if (bGetVer && (osvi.dwPlatformId == VER_PLATFORM_WIN32_NT))
  {
    //Use QueryDosDevice to look for all devices of the form COMx. This is a better
    //solution as it means that no ports have to be opened at all.
    TCHAR szDevices[65535];
    DWORD dwChars = QueryDosDevice(NULL, szDevices, 65535);
    if (dwChars)
    {
      int i=0;

      for (;;)
      {
        //Get the current device name
        TCHAR* pszCurrentDevice = &szDevices[i];

        //If it looks like "COMX" then
        //add it to the array which will be returned
        int nLen = _tcslen(pszCurrentDevice);
        if (nLen > 3 && _tcsnicmp(pszCurrentDevice, _T("COM"), 3) == 0)
        {
          //Work out the port number
          if (int nPort = _ttoi(&pszCurrentDevice[3]))
			  //if (nPort != nCom2)
		          ports.Add(nPort);
        }

        // Go to next NULL character
        while(szDevices[i] != _T('\0'))
          i++;

        // Bump pointer to the next string
        i++;

        // The list is double-NULL terminated, so if the character is
        // now NULL, we're at the end
        if (szDevices[i] == _T('\0'))
          break;
      }
    }
    else
      TRACEF(_T("Failed in call to QueryDosDevice, GetLastError:%d\n"));//, GetLastError());
  }
  else
  {
    //On 95/98 open up each port to determine their existence

    //Up to 255 COM ports are supported so we iterate through all of them seeing
    //if we can open them or if we fail to open them, get an access denied or general error error.
    //Both of these cases indicate that there is a COM port at that number. 
    for (UINT i=1; i<256; i++)
    {
      //Form the Raw device name
      CString sPort;
      sPort.Format(_T("\\\\.\\COM%d"), i);

      //Try to open the port
      BOOL bSuccess = FALSE;
      HANDLE hPort = ::CreateFile(sPort, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
      if (hPort == INVALID_HANDLE_VALUE)
      {
        DWORD dwError = GetLastError();

        //Check to see if the error was because some other app had the port open or a general failure
        if (dwError == ERROR_ACCESS_DENIED || dwError == ERROR_GEN_FAILURE)
          bSuccess = TRUE;
      }
      else
      {
        //The port was opened successfully
        bSuccess = TRUE;

        //Don't forget to close the port, since we are going to do nothing with it anyway
        CloseHandle(hPort);
      }

      //Add the port number to the array which will be returned
      if (bSuccess)
		  //if (i != nCom2)
			  ports.Add (i);
    }
  }
}

/////////////////////////////////////////////////////////////////////////////
// CComPropertiesDlg property page

IMPLEMENT_DYNCREATE(CComPropertiesDlg, CPropertyPage)

CComPropertiesDlg::CComPropertiesDlg() 
:	m_bWin32 (false),
	m_lLineID (-1),
	m_bShowAssignment (true),
	m_bPrintOnce (false),
	m_nEncoding (0),
	m_hbrDlg (NULL),
	m_bBaudRateOnly (false),
	CPropertyPage (IDD_COM_PROPERTIES_MATRIX)
{
	//{{AFX_DATA_INIT(CComPropertiesDlg)
	m_strBaud = _T("9600");
	m_strDatabits = _T("8");
	m_strParity = _T("NONE");
	m_strStopbits = _T("1");
	m_nDeviceType = 0;
	//}}AFX_DATA_INIT

	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CComPropertiesDlg::~CComPropertiesDlg()
{
	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}
}

void CComPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CComPropertiesDlg)
	DDX_CBString(pDX, IDC_BAUD, m_strBaud);
	DDX_CBString(pDX, IDC_DATABITS, m_strDatabits);
	DDX_CBString(pDX, IDC_PARITY, m_strParity);
	DDX_CBString(pDX, IDC_STOPBITS, m_strStopbits);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CComPropertiesDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CComPropertiesDlg)
	ON_CBN_SELENDOK(IDC_BAUD, OnSelendokBaud)
	ON_CBN_SELENDOK(IDC_DATABITS, OnSelendokDatabits)
	ON_CBN_SELENDOK(IDC_DEVICETYPE, OnSelendokDevicetype)
	ON_CBN_SELENDOK(IDC_PARITY, OnSelendokParity)
	ON_CBN_SELENDOK(IDC_STOPBITS, OnSelendokStopbits)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CComPropertiesDlg message handlers

BOOL CComPropertiesDlg::OnInitDialog() 
{
	struct 
	{
		eSerialDevice	m_type;
		UINT			m_nID;
	} static const map [] = 
	{
		HANDSCANNER,		IDS_HANDSCANNER,
		SW0858_DBSTART,		IDS_DBSTART,
		FIXEDSCANNER,		IDS_FIXEDSCANNER,
		REMOTEDEVICE,		IDS_REMOTEDEVICE,
		HOSTINTERFACE,		IDS_HOSTINTERFACE,
		NOTUSED,			IDS_NOTUSED,
	};
	CComboBox * pType = (CComboBox *)GetDlgItem (IDC_DEVICETYPE);
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CComboBox * pStopBits = (CComboBox *)GetDlgItem (IDC_STOPBITS);

	ASSERT (pType);
	ASSERT (pLine);
	ASSERT (pStopBits);

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		if (!m_bWin32 && (map [i].m_type == NOTUSED))
			continue;

		int nIndex = pType->AddString (LoadString (map [i].m_nID));

		pType->SetItemData (nIndex, (DWORD)map [i].m_type);

		if (m_nDeviceType == map [i].m_type)
			pType->SetCurSel (nIndex);
	}

	if (!m_bWin32) {
		UINT nID [] =
		{
			LBL_LINE,
			CB_LINE,
		};

		for (int i = 0; i < ARRAYSIZE (nID); i++)
			if (CWnd * p = GetDlgItem (nID [i]))
				p->ShowWindow (SW_HIDE);
	}

	CPropertyPage::OnInitDialog();
	
	if (pType->GetCurSel () == CB_ERR)
		pType->SetCurSel (0);

	for (int i = 0; i < m_vLines.GetSize (); i++) {
		LINESTRUCT line = m_vLines [i];

		int nIndex = pLine->AddString (line.m_strName);
		pLine->SetItemData (nIndex, line.m_lID);

		if (m_lLineID == line.m_lID)
			pLine->SetCurSel (nIndex);
	}

	if (pLine->GetCurSel () == CB_ERR)
		pLine->SetCurSel (0);

	
	if (m_bWin32) {
		pStopBits->SetItemData (pStopBits->AddString (_T ("1")),	ONESTOPBIT);
		pStopBits->SetItemData (pStopBits->AddString (_T ("1.5")),	ONE5STOPBITS);
		pStopBits->SetItemData (pStopBits->AddString (_T ("2")),	TWOSTOPBITS);
	}
	else {
		pStopBits->SetItemData (pStopBits->AddString (_T ("1")),	1);
		pStopBits->SetItemData (pStopBits->AddString (_T ("2")),	2);
	}

	if (CButton * p = (CButton *)GetDlgItem (CHK_USEONCE))
		p->SetCheck (m_bPrintOnce);

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_ENCODING)) {
		p->SetItemData (p->AddString (LoadString (IDS_NONE)), 0);
		p->SetItemData (p->AddString (_T ("1251")), 1251);
		p->SetItemData (p->AddString (_T ("1252")), 1252);
		p->SetItemData (p->AddString (_T ("GB2312")), 2312);

		for (int i = 0; i < p->GetCount (); i++) {
			if (p->GetItemData (i) == (DWORD)m_nEncoding) {
				p->SetCurSel (i);
				break;
			}
		}

		if (p->GetCurSel () == CB_ERR)
			p->SetCurSel (0);
	}

	AssignControls();
	UpdateUI ();
	UpdateData (false);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CComPropertiesDlg::AssignControls()
{
	m_strBaud.Format (_T ("%lu"), m_lBaud);
	m_strDatabits.Format (_T ("%i"), m_nDatabits);

	if (m_bWin32) {
		switch (m_nStopBits) {
		default:
		case ONESTOPBIT:		m_strStopbits = _T ("1");		break;
		case ONE5STOPBITS:		m_strStopbits = _T ("1.5");		break;
		case TWOSTOPBITS:		m_strStopbits = _T ("2");		break;
		}
	}
	else {
		switch (m_nStopBits) {
		default:
		case 1:		m_strStopbits = _T ("1");		break;
		case 2:		m_strStopbits = _T ("2");		break;
		}
	}

	switch ( m_cParity )
	{
	case 'N':
		m_strParity.Format (_T ("NONE"));
		break;
	case 'O':
		m_strParity.Format (_T ("ODD"));
		break;
	case 'E':
		m_strParity.Format (_T ("EVEN"));
		break;
	}
}

void CComPropertiesDlg::UpdateSettings()
{
	CComboBox * pType = (CComboBox *)GetDlgItem (IDC_DEVICETYPE);
	CComboBox * pStopBits = (CComboBox *)GetDlgItem (IDC_STOPBITS);

	ASSERT (pType);
	ASSERT (pStopBits);

	m_cParity		= (char)m_strParity.GetBuffer(1)[0];
	m_lBaud			= atol (w2a (m_strBaud));
	m_nDatabits		= atoi (w2a (m_strDatabits));
	m_nDeviceType	= pType->GetItemData (pType->GetCurSel ());
	m_nStopBits		= pStopBits->GetItemData (pStopBits->GetCurSel ());

	if (CButton * p = (CButton *)GetDlgItem (CHK_USEONCE))
		m_bPrintOnce = p->GetCheck () == 1 ? true : false;
}


void CComPropertiesDlg::OnOK() 
{
	if ( UpdateData ( true ) )
	{
		CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);

		UpdateSettings();
		CPropertyPage::OnOK();

		ASSERT (pLine);
		
		m_lLineID = pLine->GetItemData (pLine->GetCurSel ());
	
		if (CComboBox * p = (CComboBox *)GetDlgItem (CB_ENCODING)) 
			m_nEncoding = p->GetItemData (p->GetCurSel  ());
	}
}

BOOL CComPropertiesDlg::OnApply() 
{
	if ( UpdateData ( true ) )
	{
		UpdateSettings();
		SetModified ( false );
	}
	return CPropertyPage::OnApply();
}

void CComPropertiesDlg::OnSelendokBaud() 
{
	SetModified ( true );
}

void CComPropertiesDlg::OnSelendokDatabits() 
{
	SetModified ( true );
}

void CComPropertiesDlg::OnSelendokDevicetype() 
{
	SetModified ( true );
	UpdateUI ();
}

void CComPropertiesDlg::UpdateUI () 
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CComboBox * pType = (CComboBox *)GetDlgItem (IDC_DEVICETYPE);

	ASSERT (pType);
	ASSERT (pLine);
	
	eSerialDevice type = (eSerialDevice)pType->GetItemData (pType->GetCurSel ());

	pLine->EnableWindow (type == FIXEDSCANNER);

	{
		UINT nID [] = 
		{
			LBL_DEVICETYPE,
			IDC_DEVICETYPE,
			CB_LINE,
			LBL_LINE,
		};

		for (int i = 0; i < ARRAYSIZE (nID); i++) 
			if (CWnd * p = GetDlgItem (nID [i]))
				p->ShowWindow (m_bShowAssignment ? SW_SHOW : SW_HIDE);
	}

	if (m_bBaudRateOnly) {
		UINT nID [] = 
		{
			LBL_DEVICETYPE,
			IDC_DEVICETYPE,
			CB_LINE,
			LBL_LINE,
			CHK_USEONCE,
			LBL_ENCODING,
			CB_ENCODING,
		};

		for (int i = 0; i < ARRAYSIZE (nID); i++) 
			if (CWnd * p = GetDlgItem (nID [i]))
				p->ShowWindow (SW_HIDE);
	}

	if (CWnd * p = GetDlgItem (CHK_USEONCE)) {
		if (CComboBox * pType = (CComboBox *)GetDlgItem (IDC_DEVICETYPE)) {
			int n = pType->GetItemData (pType->GetCurSel ());
			bool bEnabled = (n == REMOTEDEVICE) || (n == HANDSCANNER);

			p->EnableWindow (bEnabled); 
		}
	}
}

void CComPropertiesDlg::OnSelendokParity() 
{
	SetModified ( true );
}

void CComPropertiesDlg::OnSelendokStopbits() 
{
	SetModified ( true );
}


HBRUSH CComPropertiesDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = ::GetSysColor (COLOR_BTNFACE);//FoxjetUtils::GetButtonColor (_T ("background"));
	
	if (IsControl ()) 
		rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	if (!m_hbrDlg)	
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}
