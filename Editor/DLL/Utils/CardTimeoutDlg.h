#if !defined(AFX_CARDTIMEOUTDLG_H__22F69B60_5279_4312_B6DF_951D5FDB271A__INCLUDED_)
#define AFX_CARDTIMEOUTDLG_H__22F69B60_5279_4312_B6DF_951D5FDB271A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CardTimeoutDlg.h : header file
//

#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CCardTimeoutDlg dialog

class CCardTimeoutDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CCardTimeoutDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCardTimeoutDlg)
	int		m_nTimeout;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCardTimeoutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCardTimeoutDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CARDTIMEOUTDLG_H__22F69B60_5279_4312_B6DF_951D5FDB271A__INCLUDED_)
