// ComponentsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "utils.h"
#include "ComponentsDlg.h"
#include "SystemDlg.h"

#include <afxtempl.h>
#include <shlwapi.h>

#include "Resource.h"
#include "AboutDlg.h"
#include "Utils.h"
#include "Debug.h"
#include "Registry.h"
#include "DevGuids.h"
#include "Database.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const CTime tmUninit (1970, 1, 1, 0, 0, 0);
static const FoxjetCommon::CVersion verUninit (-1, -1);
static const LPCTSTR lpszDAO = _T ("DAO");

using namespace ComponentsDlg;

/////////////////////////////////////////////////////////////////////////////

CComponent::CComponent () 
:	m_tmModified (::tmUninit), 
	m_strVer (::verUninit.Format ()) 
{ 
}

CComponent::CComponent (const CComponent & rhs)
:	m_strTitle (rhs.m_strTitle),
	m_strPath (rhs.m_strPath),
	m_tmModified (rhs.m_tmModified),
	m_strVer (rhs.m_strVer)
{
}

CComponent & CComponent::operator = (const CComponent &rhs)
{
	if (this != &rhs) {
		m_strTitle		= rhs.m_strTitle;
		m_strPath		= rhs.m_strPath;
		m_tmModified	= rhs.m_tmModified;
		m_strVer		= rhs.m_strVer;
	}

	return * this;
}

CComponent::~CComponent ()
{
}

/////////////////////////////////////////////////////////////////////////////
// CComponentsDlg property page

IMPLEMENT_DYNCREATE(CComponentsDlg, CPropertyPage)

CComponentsDlg::CComponentsDlg() 
:	m_hbrDlg (NULL),
	m_bFjOnly (true),
	CPropertyPage(IDD_COMPONENTS_MATRIX)
{
	m_psp.dwFlags |= PSH_USECALLBACK;
	m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;

	//{{AFX_DATA_INIT(CComponentsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CComponentsDlg::~CComponentsDlg()
{
	Free ();
}

void CComponentsDlg::Free ()
{
	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}

	for (int i = 0; i < m_vComponents.GetSize (); i++) 
		if (CComponent * p = m_vComponents [i])
			delete p;

	m_vComponents.RemoveAll ();
}

extern HINSTANCE hInstance;

void CComponentsDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CComponentsDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	HINSTANCE hRestore = ::AfxGetResourceHandle ();
	::AfxSetResourceHandle (::hInstance);
		m_buttons.DoDataExchange (pDX, BTN_RESTORE,	IDB_RESTORE);
	::AfxSetResourceHandle (hRestore);
}


BEGIN_MESSAGE_MAP(CComponentsDlg, CPropertyPage)
	ON_NOTIFY(TVN_SELCHANGED, TV_COMPONENTS, OnSelchangedComponents)
	ON_NOTIFY(TVN_ITEMEXPANDING, TV_COMPONENTS, OnItemexpandingComponents)
	ON_COMMAND (ID_ABOUT_FOXJETCOMPONENTSONLY, OnFjComponentOnly)
	ON_NOTIFY(NM_RCLICK, TV_COMPONENTS, OnRclick)
	//{{AFX_MSG_MAP(CComponentsDlg)
	ON_WM_DROPFILES()
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
	ON_COMMAND (BTN_RESTORE, OnRestore)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CComponentsDlg message handlers

BOOL CComponentsDlg::OnInitDialog() 
{
	ASSERT (GetDlgItem (TV_COMPONENTS));
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (TV_COMPONENTS);

	CPropertyPage::OnInitDialog();
	EnumDependencies ();
	DragAcceptFiles  (TRUE);

	m_imgList.Create (GetTreeCtrlItemHeight (), GetTreeCtrlItemHeight (), ILC_COLOR, 0, 1);
	tree.SetImageList (&m_imgList, LVSIL_SMALL);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CComponentsDlg::InsertDriver (HTREEITEM hRoot, const CString & strDriver)
{
	TCHAR sz [MAX_PATH] = { 0 };
	CStringArray v;

	::GetSystemDirectory (sz, sizeof (sz));
	GetFiles (CString (sz) + _T ("\\drivers"), strDriver, v);

	for (int i = 0; i < v.GetSize (); i++) 
		InsertItem (v [i], hRoot, 0);
}

void CComponentsDlg::EnumDependencies ()
{
	ASSERT (GetDlgItem (TV_COMPONENTS));

	WORD wIcon = 0;
	CStringArray v; 
	TCHAR sz [MAX_PATH] = { 0 };
	TCHAR szTitle [MAX_PATH] = { 0 };
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (TV_COMPONENTS);
	CComponent * pRoot = new CComponent ();

	Free ();
	tree.DeleteAllItems ();

	::ZeroMemory (sz, sizeof (TCHAR) * MAX_PATH);
	::ZeroMemory (szTitle, sizeof (TCHAR) * MAX_PATH);
	::GetModuleFileName (NULL, sz, ARRAYSIZE (sz) - 1);
	::GetFileTitle (sz, szTitle, ARRAYSIZE (szTitle) - 1);
	
	FoxjetCommon::GetDependencies (sz, v, m_bFjOnly);

	if (CStatic * pIcon = (CStatic *)GetDlgItem (IDI_ICON))
		pIcon->SetIcon (::ExtractAssociatedIcon (NULL, sz, &wIcon));

	pRoot->m_strTitle		= szTitle;
	pRoot->m_strPath		= sz;
	pRoot->m_tmModified		= GetFileDate (sz);
	pRoot->m_strVer			= m_ver.Format ();
	
	HTREEITEM hRoot = tree.InsertItem (pRoot->m_strTitle, 0, 1, NULL, TVI_SORT);
	tree.SetItemData (hRoot, (DWORD)pRoot);
	m_vComponents.Add (pRoot);
	
	for (int i = 0; i < v.GetSize (); i++) 
		InsertItem (v [i], hRoot, 0);

	tree.Expand (hRoot, TVE_EXPAND);
	tree.Select (hRoot, TVGN_CARET);

#ifdef __WINPRINTER__
	if (IsDriverInstalled (GUID_INTERFACE_IVPHC))
		InsertDriver (hRoot, _T ("IvPhc.sys"));
	
	if (IsDriverInstalled (GUID_INTERFACE_FXMPHC))
		InsertDriver (hRoot, _T ("FxMphc.sys"));

	if (::GetFileAttributes (GetHomeDir () + _T ("\\UsbMphc.dll")) != -1)
		InsertItem (_T ("UsbMphc.dll"), hRoot, 1);

	{ // ActiveX interface
		CString strInterface = _T ("Interface.ocx");
		CString strPath = FoxjetDatabase::GetHomeDir () + _T ("\\") + strInterface;

		if (HMODULE hComponent = ::LoadLibrary (strPath)) {
			CComponent * pInterface = new CComponent ();
			CStringArray v;

			pInterface->m_strTitle			= strInterface;
			pInterface->m_strPath			= strPath;
			pInterface->m_tmModified		= GetFileDate (strPath);
			GetFixedFileInfo (strPath, pInterface);
		
			HTREEITEM h = tree.InsertItem (pInterface->m_strTitle, 0, 1, NULL, TVI_LAST);
			tree.SetItemData (h, (DWORD)pInterface);
			m_vComponents.Add (pInterface);

			FoxjetCommon::GetDependencies (strInterface, v, m_bFjOnly);

			for (int i = 0; i < v.GetSize (); i++) 
				InsertItem (v [i], h, 0);


			::FreeLibrary (hComponent);
		}
	}
#endif

	InsertItem (lpszDAO, hRoot, 0);

	if (IsPrintMonitor ()) 
		InsertItem (GetPrintDriverDirectory () + _T ("\\fjmknext.dll"), hRoot, 1);
}

HTREEITEM CComponentsDlg::InsertItem(const CString &strComponent, HTREEITEM hParent, int nDepth)
{
	ASSERT (GetDlgItem (TV_COMPONENTS));

	TCHAR sz [MAX_PATH] = { 0 };
	TCHAR szTitle [MAX_PATH] = { 0 };
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (TV_COMPONENTS);
	CComponent * p = new CComponent;
	HTREEITEM hItem = NULL;

	BeginWaitCursor ();
	::ZeroMemory (szTitle, sizeof (TCHAR) * MAX_PATH);
	
	if (!::GetFileTitle (strComponent, szTitle, ARRAYSIZE (szTitle) - 1)) {
		p->m_strTitle = szTitle;

		if (p->m_strTitle == strComponent)
			p->m_strPath = _T ("");
	}
	else
		p->m_strTitle = strComponent;

	if (!hParent)
		hParent = tree.GetRootItem ();

	hItem = tree.InsertItem (p->m_strTitle, hParent, TVI_SORT);
	tree.SetItemData (hItem, (DWORD)p);
	m_vComponents.Add (p);


/*
	#ifdef _DEBUG
	CString str;
	str.Format ("InsertItem (%s, 0x%p, %u)", strComponent, hParent, nDepth);
	TRACEF (str);
	#endif //_DEBUG
*/

	if (strComponent.CompareNoCase (_T ("mfc90.dll")) != 0 &&
		strComponent.CompareNoCase (_T ("MSVCR90.dll")) != 0) 
	{
		bool bFreeLibrary = false;
		HMODULE hComponent = ::GetModuleHandle (strComponent);

		if (!hComponent) 
			bFreeLibrary = (hComponent = ::LoadLibrary (strComponent)) != NULL;

		if (!hComponent) {
			TRACEF (strComponent);
			TRACEF (FormatMessage (::GetLastError ()));
		}

		if (hComponent) {
			LPGETDLLVERSION lpfGetVer = (LPGETDLLVERSION)
				::GetProcAddress (hComponent, CAnsiString (FJGETDLLVERSIONPROCADDRESS));

			if (lpfGetVer) {
				CVersion ver;
				(* lpfGetVer) (ver);
				p->m_strVer = ver.Format ();
			}
			else {
				TRACEF (strComponent);

				DLLGETVERSIONPROC lpDllGetVersion = (DLLGETVERSIONPROC)
					GetProcAddress(hComponent, CAnsiString (_T ("DllGetVersion")));

				if (lpDllGetVersion) {
					DLLVERSIONINFO dvi;
					HRESULT hr;

					ZeroMemory(&dvi, sizeof(dvi));
					dvi.cbSize = sizeof(dvi);
					hr = (* lpDllGetVersion)(&dvi);
					
					if (SUCCEEDED (hr)) 
						p->m_strVer = CVersion (dvi.dwMajorVersion, dvi.dwMinorVersion, 0, 0, dvi.dwBuildNumber).Format ();
				}
				else 
					GetFixedFileInfo (strComponent, p);
			}

			::ZeroMemory (sz, sizeof (TCHAR) * MAX_PATH);
			::ZeroMemory (szTitle, sizeof (TCHAR) * MAX_PATH);
			::GetModuleFileName (hComponent, sz, ARRAYSIZE (sz) - 1);
			
			if (!::GetFileTitle (sz, szTitle, ARRAYSIZE (szTitle) - 1)) {
				p->m_strTitle = szTitle;
				p->m_strPath = sz;
			}
			else
				p->m_strTitle = sz;

			p->m_tmModified = GetFileDate (sz);

			// check for sub-components
			if (nDepth > 0) {
				CStringArray v;

				GetDependencies (strComponent, v, m_bFjOnly);

				for (int i = 0; i < v.GetSize (); i++) {
					TRACEF (v [i]);
					InsertItem (v [i], hItem, nDepth - 1);
				}
			}

			if (bFreeLibrary)
				VERIFY (FreeLibrary (hComponent));
		}
	}
	else 
		GetFixedFileInfo (strComponent, p);

	EndWaitCursor ();

	return hItem;
}

bool CComponentsDlg::GetFixedFileInfo (const CString & strComponent, CComponent * p)
{
	bool bResult = false;
	DWORD dw;
	CString str (strComponent);
	DWORD dwSize = ::GetFileVersionInfoSize (str.GetBuffer (str.GetLength ()), &dw);
	BYTE * pVer = new BYTE [dwSize];
	TCHAR szTitle [MAX_PATH] = { 0 };

	if (!::GetFileTitle (str, szTitle, ARRAYSIZE (szTitle) - 1)) {
		p->m_strTitle = szTitle;
		p->m_strPath = str;
	}
	else
		p->m_strTitle = str;

	p->m_tmModified = GetFileDate (str);

	if (::GetFileVersionInfo (str.GetBuffer (str.GetLength ()), 0, dwSize, pVer)) {
		LPVOID pData = NULL;
		UINT nLen = 0;

		if (::VerQueryValue (pVer, _T ("\\"), &pData, &nLen)) {
			if (VS_FIXEDFILEINFO * pInfo = (VS_FIXEDFILEINFO *)pData) {
				CString str;

				str.Format (_T ("%d.%d.%d.%d"),
					HIWORD (pInfo->dwFileVersionMS),
					LOWORD (pInfo->dwFileVersionMS),
					HIWORD (pInfo->dwFileVersionLS),
					LOWORD (pInfo->dwFileVersionLS));

				p->m_strVer = str;
				bResult = true;
 			}
		}
	}

	delete [] pVer;

	return bResult;
}

void CComponentsDlg::OnSelchangedComponents(NMHDR* pNMHDR, LRESULT* pResult) 
{
	ASSERT (GetDlgItem (TV_COMPONENTS));
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (TV_COMPONENTS);
	NM_TREEVIEW * pNMTV = (NM_TREEVIEW*)pNMHDR;

	CComponent * p = (CComponent *)tree.GetItemData (pNMTV->itemNew.hItem);

	if (p) {
		CString strModified, strVer;

		if (p->m_tmModified != ::tmUninit)
			strModified = p->m_tmModified.Format (_T ("%c"));
		if (p->m_strVer != ::verUninit.Format ())
			strVer = p->m_strVer;

		if (p->m_strTitle == lpszDAO) {
			strVer = GetDAOVersion ();

			if (!strVer.GetLength ())
				strVer = FoxjetUtils::LoadString (IDS_NOTINSTALLED);
		}

		SetDlgItemText (TXT_TITLE, p->m_strTitle);
		SetDlgItemText (TXT_PATH, p->m_strPath);
		SetDlgItemText (TXT_MODIFIED, strModified);
		SetDlgItemText (TXT_VERSION, strVer);
	}

	*pResult = 0;
}


void CComponentsDlg::OnItemexpandingComponents(NMHDR* pNMHDR, LRESULT* pResult) 
{
	ASSERT (GetDlgItem (TV_COMPONENTS));
	CTreeCtrl & tree = * (CTreeCtrl *)GetDlgItem (TV_COMPONENTS);
	NM_TREEVIEW* pNMTV = (NM_TREEVIEW*)pNMHDR;
	HTREEITEM hItem = pNMTV->itemNew.hItem;
	CComponent * pItem = (CComponent *)tree.GetItemData (hItem);

	if (pItem && tree.ItemHasChildren (hItem)) {
		HTREEITEM hChildItem = tree.GetChildItem (hItem);

		while (hChildItem != NULL) {
			CComponent * pChild = (CComponent *)tree.GetItemData (hChildItem);

			if (pChild && !tree.ItemHasChildren (hChildItem)) {
				CStringArray v;

				GetDependencies (pChild->m_strPath, v, m_bFjOnly);

				for (int i = 0; i < v.GetSize (); i++)
					InsertItem (v [i], hChildItem);
			}

			hChildItem = tree.GetNextItem (hChildItem, TVGN_NEXT);
		}
	}

	*pResult = 0;
}


void CComponentsDlg::OnFjComponentOnly ()
{
	m_bFjOnly = !m_bFjOnly;
	EnumDependencies ();
}

void CComponentsDlg::OnRclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CMenu mnu;
	CPoint ptScreen;

	mnu.LoadMenu (IDM_ABOUT);
	CMenu * pMenu = mnu.GetSubMenu (0);

	::GetCursorPos (&ptScreen);
	ASSERT (pMenu);
	pMenu->CheckMenuItem (ID_ABOUT_FOXJETCOMPONENTSONLY, 
		MF_BYCOMMAND | (m_bFjOnly ? MF_CHECKED : MF_UNCHECKED));
	pMenu->TrackPopupMenu (
		TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON,
		ptScreen.x, ptScreen.y, this);

	* pResult = 0;
}


void CComponentsDlg::OnDropFiles(HDROP hDropInfo) 
{
	CPropertyPage::OnDropFiles(hDropInfo);

    if (hDropInfo) {
        UINT nFiles = ::DragQueryFile (hDropInfo, (UINT)-1, NULL, 0);

        for (UINT i = 0; i < nFiles; i++) {
			TCHAR sz [MAX_PATH] = { 0 };

            ::DragQueryFile (hDropInfo, i, sz, ARRAYSIZE (sz));
            TRACEF (sz);
			InsertItem (sz);
        }
	}
}

HBRUSH CComponentsDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = ::GetSysColor (COLOR_BTNFACE);//FoxjetUtils::GetButtonColor (_T ("background"));

	if (IsControl (false)) 
		rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	if (!m_hbrDlg)	
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}

void CComponentsDlg::OnRestore ()
{
	CString str;

	for (int i = 0; i < m_vCmdLine.GetSize (); i++)
		str += m_vCmdLine [i] + _T (" ");

	str += _T ("/select=\"") + FoxjetUtils::LoadString (IDS_LOCAL_BACKUP) + _T ("\\");

	FoxjetDatabase::Restore (this, str);
}


