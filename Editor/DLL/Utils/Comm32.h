//
// COMM32.H
// -----------
// WIN32 communications library definitions 
//
//
//
// Written By:
// ------------
// Anthony A. Kempka
// Integrity Instruments
// 
// Tel: 8000-450-2001
// Web: www.rs-485.com
//
// Note:
// -----
// Please read the following license
//


/*  LICENSE AGREEMENT FOR INTEGRITY INSTRUMENTS SOFTWARE

    This legal agreement between you, the customer or end user, and 
    INTEGRITY INSTRUMENTS gives you certain rights, benefits, and 
    obligations.  By breaking any sealed package or by using the product, 
    you agree to be bound by the terms of this agreement and indicate that 
    you have read, understood, and will comply with these terms.

1.  Product - The term product refers to the software supplied 
    regardless of the media or means by which it is supplied.

2.  License - The license agreement that INTEGRITY INSTRUMENTS grants 
    is to install, use, and modify the product in whatever manner you choose.

3.  You may not sell, lease, distribute, or release the product, in 
    whole or in part, without explicit written permission from 
    INTEGRITY INSTRUMENTS.  

4.  INTEGRITY INSTRUMENTS makes no warranties to the product's contents, 
    disclaims any implied warranties of any kind, except that the 
    product will perform functionally in accordance with the written 
    documentation supplied.

5.  INTEGRITY INSTRUMENTS shall not be liable to any person or entity for 
    any direct, indirect, special, incidental or consequential damages 
    resulting from the use or misuse of its product, including but not 
    limited to loss of business, business profits, or business credibility.  */

//
// Function definitions
//

#include "UtilApi.h"

HANDLE UTIL_API Open_Comport (DWORD Comport, DWORD BaudRate, BYTE nByteSize, BYTE nParity, BYTE nStopBits, DWORD dwCreateFlags = 0);
int UTIL_API Close_Comport (HANDLE DriverHandle);
int UTIL_API Write_Comport (HANDLE DriverHandle, DWORD NumBytes, void *Buffer);
int UTIL_API Read_Comport (HANDLE DriverHandle, DWORD *BytesRead, DWORD BufferSize, void *Buffer);
