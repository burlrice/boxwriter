#if !defined(AFX_WDTTIMEOUTDLG_H__C1FDB1EE_BAF1_4911_8387_63078FD2215D__INCLUDED_)
#define AFX_WDTTIMEOUTDLG_H__C1FDB1EE_BAF1_4911_8387_63078FD2215D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WdtTimeoutDlg.h : header file
//

#include "DbTypeDefs.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CWdtTimeoutDlg dialog

class CWdtTimeoutDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CWdtTimeoutDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CWdtTimeoutDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	FoxjetDatabase::MOTHERBOARDTYPE m_motherboard;
	ULONG m_lWDT;

	static CString FormatWDT (FoxjetDatabase::MOTHERBOARDTYPE b, ULONG lWDT);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWdtTimeoutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	virtual void OnOK ();

	// Generated message map functions
	//{{AFX_MSG(CWdtTimeoutDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkTimeout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WDTTIMEOUTDLG_H__C1FDB1EE_BAF1_4911_8387_63078FD2215D__INCLUDED_)
