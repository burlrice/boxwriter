// AboutDlg1.cpp : implementation file
//

#include "stdafx.h"
#include "utils.h"
#include "AboutDlg.h"
#include "LicenseDlg.h"
#include "ComponentsDlg.h"
#include "Parse.h"
#include "Debug.h"
#include "CmdLineDlg.h"
#include "Parse.h"
#include "SystemDlg.h"
#include "RoundButtons.h"
#include "Registry.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace ComponentsDlg;

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg

IMPLEMENT_DYNAMIC(CAboutDlg, CEnPropertySheet)

CAboutDlg::CAboutDlg(const CVersion & ver, const CStringArray & vCmdLine, UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
:	m_ver (ver),
	m_pPage1 (NULL),
	m_pPage2 (NULL),
	m_pPage3 (NULL),
	CEnPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
	m_vCmdLine.Copy (vCmdLine);
	Init ();
}

CAboutDlg::CAboutDlg(const CVersion & ver, const CStringArray & vCmdLine, LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
:	m_ver (ver),
	m_pPage1 (NULL),
	m_pPage2 (NULL),
	m_pPage3 (NULL),
	CEnPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	m_vCmdLine.Copy (vCmdLine);
	Init ();
}

CAboutDlg::~CAboutDlg()
{
	if (m_pPage1) {
		delete m_pPage1;
		m_pPage1 = NULL;
	}

	if (m_pPage2) {
		delete m_pPage2;
		m_pPage2 = NULL;
	}

	if (m_pPage3) {
		delete m_pPage3;
		m_pPage3 = NULL;
	}
}


BEGIN_MESSAGE_MAP(CAboutDlg, CEnPropertySheet)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg message handlers

void CAboutDlg::Init()
{
	m_psh.dwFlags |= PSH_NOAPPLYNOW;
	m_psh.dwFlags |= PSH_USECALLBACK;
	m_psh.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetProc;

	//m_pPage1 = new CLicenseDlg ();
	m_pPage2 = new CComponentsDlg ();

	{
		using namespace FoxjetDatabase;

		//if (m_vCmdLine.GetSize () >= 1) {
			CCmdLineDlg * p = new CCmdLineDlg ();
		
			m_pPage3 = p;

			p->m_v.Copy (m_vCmdLine);

			{
				CString strTmp, str = FoxjetDatabase::Encrypted::GetCommandLine (::GetCommandLine ());
				CStringArray v;

				Tokenize (str, v, '/');

				for (int i = 0; i < m_vCmdLine.GetSize (); i++)
					strTmp += m_vCmdLine [i] + _T (" ");

				for (int i = 1; i < v.GetSize (); i++) {
					CString strOption = _T ("/") + v [i];

					if (FindNoCase (strTmp, strOption) == -1)
						p->m_v.Add (strOption);
				}
			}

			for (int i = 0; i < p->m_v.GetSize (); i++) {
				if (FindNoCase (p->m_v [i], _T ("pass=")) != -1)
					p->m_v.SetAt (i, _T ("/PASS=******"));

				TRACEF (p->m_v [i]);
			}
		//}
	}

	if (CComponentsDlg * p = DYNAMIC_DOWNCAST (CComponentsDlg, m_pPage1)) {
		p->m_ver	= m_ver;
		p->m_strApp = m_strApp;
		p->m_vCmdLine.Append (m_vCmdLine);
	}

	if (CComponentsDlg * p = DYNAMIC_DOWNCAST (CComponentsDlg, m_pPage2)) {
		p->m_ver	= m_ver;
		p->m_strApp = m_strApp;
		p->m_vCmdLine.Append (m_vCmdLine);
	}

	if (m_pPage1) AddPage (m_pPage1);
	if (m_pPage2) AddPage (m_pPage2);
	if (m_pPage3) AddPage (m_pPage3);

	if (m_pPage1) SetActivePage (m_pPage2);

}

BOOL CAboutDlg::OnInitDialog() 
{
	CEnPropertySheet::OnInitDialog();

	if (CWnd * p = GetDlgItem (IDCANCEL))
		p->ShowWindow (SW_HIDE);
	if (CWnd * p = GetDlgItem (IDHELP))
		p->ShowWindow (SW_SHOW);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

