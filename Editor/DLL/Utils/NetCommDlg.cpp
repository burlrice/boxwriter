// NetCommDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
//#include "list.h"
#include "NetCommDlg.h"
#include "Parse.h"
#include "TemplExt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetUtils;
using namespace FoxjetCommon;
using namespace FoxjetCommon::NetCommDlg;
using namespace FoxjetDatabase;
using namespace ItiLibrary;

////////////////////////////////////////////////////////////////////////////////

bool UTIL_API FoxjetCommon::EditNetSerialParams (CString & str) 
{
	CPropertySheet sheet;
	CNetCommDlg dlg;
	CStringArray v;
	CString strValues [] = 
	{
		_T ("38400"),
		_T ("0"),
		_T ("0"),
		_T ("0"),
	};

	Tokenize (str, v, ',');

	for (int i = 0; i < min (v.GetSize (), (int)ARRAYSIZE (strValues)); i++) 
		if (v [i].GetLength ())
			strValues [i] = v [i];

	dlg.m_strBaud			= strValues [0];
	dlg.m_lUse				= _ttol (strValues [1]);
	dlg.m_bScannerUseOnce	= _ttol (strValues [2]);
	dlg.m_bSaveSerial		= _ttoi (strValues [3]);
	
	sheet.AddPage (&dlg);
	sheet.SetTitle (LoadString (IDS_SERIALSETTINGS));

	if (sheet.DoModal() == IDOK) {
		str.Format (_T ("%s,%i,%d,%d"), dlg.m_strBaud, dlg.m_lUse, dlg.m_bScannerUseOnce, dlg.m_bSaveSerial);
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////


CCommItem::CCommItem ()
:	m_use (NETSERIAL_NONE)
{
	m_dwBaudRate = 38400;
	m_bScannerUseOnce = false;
	m_bSaveSerial = false;
}

CString CCommItem::ToString () const
{
	CStringArray v;

	v.Add (FoxjetDatabase::ToString (m_dwBaudRate));
	v.Add (FoxjetDatabase::ToString ((long)m_use));
	v.Add (FoxjetDatabase::ToString ((long)m_bScannerUseOnce));
	v.Add (FoxjetDatabase::ToString ((long)m_bSaveSerial));

	return FoxjetDatabase::ToString (v);
}

bool CCommItem::FromString (const CString & str)
{
	CStringArray v;

	FoxjetDatabase::FromString (str, v);

	if (v.GetSize () >= 2) {
		int nIndex = 0;

		m_dwBaudRate		= _tcstoul (v [nIndex++], NULL, 10);
		m_use				= (USE)BindTo (_tcstoul (v [nIndex++], NULL, 10), (ULONG)NETSERIAL_NONE, (ULONG)NETSERIAL_INFO);
		m_bScannerUseOnce	= _ttoi (GetParam (v, nIndex++, _T ("0"))) ? true : false;
		m_bSaveSerial		= _ttoi (GetParam (v, nIndex++, _T ("0"))) ? true : false;
		return true;
	}

	return false;
}
				
CString CCommItem::GetDispText (int nColumn) const
{
	CString str;
	LPCTSTR lpsz [] = // note: must correspond to CNetCommDlg::CCommItem::USE
	{
		_T ("NONE"), 
		_T ("VAR_DATA"), 
		_T ("DYNMEM"), 
		_T ("SCANNER_LABEL"), 
		_T ("INFO"), 
	};

	str.Format (_T ("%d, %s"), m_dwBaudRate, lpsz [m_use % ARRAYSIZE (lpsz)]);

	return str;
}

/////////////////////////////////////////////////////////////////////////////
// CNetCommDlg property page

IMPLEMENT_DYNCREATE(CNetCommDlg, CPropertyPage)

CNetCommDlg::CNetCommDlg() 
:	CPropertyPage(IDD_NET_COM_PROPERTIES)
{
	//{{AFX_DATA_INIT(CNetCommDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CNetCommDlg::~CNetCommDlg()
{
}

void CNetCommDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNetCommDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	DDX_CBString(pDX, CB_BAUD, m_strBaud);
	DDX_CBIndex(pDX, CB_USE, m_lUse);
	DDX_Check (pDX, CHK_SCANNERUSEONCE, m_bScannerUseOnce);
	DDX_Check (pDX, CHK_SAVESERIALDATA, m_bSaveSerial);
}


BEGIN_MESSAGE_MAP(CNetCommDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CNetCommDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE (CB_USE, OnUseSelchange)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNetCommDlg message handlers

BOOL CNetCommDlg::OnInitDialog()
{
	CPropertyPage::OnInitDialog ();

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_BAUD)) 
		if (p->GetCurSel () == CB_ERR)
			p->SetCurSel (3);

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_USE)) 
		if (p->GetCurSel () == CB_ERR)
			p->SetCurSel (0);

	OnUseSelchange ();
	
	return FALSE;
}

void CNetCommDlg::OnUseSelchange ()
{
	bool bEnableOnce = false;
	bool bEnableSave = false;

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_USE)) {
		bEnableOnce = (p->GetCurSel () == 1) || (p->GetCurSel () == 3);
		bEnableSave = (p->GetCurSel () == 1);
	}

	if (CButton * p = (CButton *)GetDlgItem (CHK_SCANNERUSEONCE)) {
		p->EnableWindow (bEnableOnce);
		p->SetCheck (bEnableOnce ? m_bScannerUseOnce : 0);
	}

	if (CButton * p = (CButton *)GetDlgItem (CHK_SAVESERIALDATA)) {
		p->EnableWindow (bEnableSave);
		p->SetCheck (bEnableSave ? m_bSaveSerial : 0);
	}
}