// Tracker.cpp: implementation of the CElementTracker class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "System.h"
#include "SystemDlg.h"
#include "resource.h"
#include "Database.h"
#include "StdCommDlg.h"
#include "TemplExt.h"
#include "Debug.h"
#include "ScannerPropDlg.h"
#include "AppVer.h"
#include "IdsDlg.h"
#include "DevGuids.h"
#include "Utils.h"
#include "Parse.h"
#include "StartupDlg.h"
#include "WatchdogDlg.h"
#include "Sw0833Dlg.h"
#include "TimeChangeDlg.h"
#include "SysIdDlg.h"
#include "Registry.h"
#include "NetworkDlg.h"
#include "LocalDbDlg.h"
#include "InkCodePage.h"

using namespace FoxjetCommon;
using namespace FoxjetCommon::StdCommDlg;
using namespace FoxjetUtils;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

static bool bSw0883 = false;

CString UTIL_API FoxjetCommon::GetDefTimeFormat (int nRow)
{
	switch (nRow) {
	case 0: return LoadString (IDS_DATETIME);
	case 1: return _T ("%b %d"); 
	case 2: return _T ("%H:%M"); 
	}

	return _T ("");
}

bool UTIL_API CALLBACK FoxjetCommon::OnSystemConfig (CWnd * pParent, const CString & strRegSection, FoxjetDatabase::COdbcDatabase & db, 
	LPFORMATDATETIME lpfct, CCopyArray <DWORD, DWORD> & vCommChanged)
{
	const bool bValve = ISVALVE ();
	CSystemDlg sheet (LoadString (IDS_SYSTEMCONFIG), pParent);
	CStdCommDlg dlgComm (db, strRegSection);
	CScannerPropDlg dlgScanner (db, strRegSection);
	CIdsDlg dlgIDS (db, strRegSection);
	CSw0833Dlg dlgSW0883 (db, strRegSection);
	CTimeChangeDlg dlgTimeChange;
	CSysIdDlg dlgSysID; // sw0867
	CInkCodePage dlgInkCode (db);
	CNetworkDlg dlgNetwork;
	CLocalDbDlg dlgLocalDb;
	bool bResult = false;
	SETTINGSSTRUCT s;

	dlgInkCode.m_bEnabled = _ttoi (FoxjetDatabase::Encrypted::GetProfileString (FoxjetDatabase::Encrypted::InkCodes::GetRegSection (), _T ("Enabled"), _T ("1"))) ? TRUE : FALSE; 

	#ifdef __WINPRINTER__
	CStartupDlg dlgStartup (db, strRegSection);
	//CWatchdogDlg dlgWDT (db, strRegSection);
	#endif

	dlgTimeChange.m_strTimeFormat [0] = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strRegSection + _T ("\\time"), _T ("format1"), GetDefTimeFormat (0)); 
	dlgTimeChange.m_strTimeFormat [1] = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strRegSection + _T ("\\time"), _T ("format2"), GetDefTimeFormat (1)); 
	dlgTimeChange.m_strTimeFormat [2] = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strRegSection + _T ("\\time"), _T ("format3"), GetDefTimeFormat (2)); 
	dlgTimeChange.m_lpFormatFct = lpfct;

	if (GetSettingsRecord (db, 0, lpszScannerKey, s)) {
		TRACEF (s.m_strData);
		dlgScanner.FromString (s.m_strData, dlgScanner.m_scanner);
	}

	sheet.AddPage (&dlgComm);

	if (IsGojo ()) {
		s.m_strData = _T ("XX");

		if (GetSettingsRecord (db, 0, _T ("SYSID:") + ToString (GetPrinterID (db)), s)) {
			TRACEF (s.m_strData);
			dlgSysID.m_strSysID = s.m_strData;
		}

		sheet.AddPage (&dlgSysID);
	}

	#ifdef __WINPRINTER__
	{
		sheet.AddPage (&dlgStartup);
	
		dlgStartup.m_bAutoStart = CStartupDlg::IsAutoStartEnabled (db);
		dlgStartup.m_bUserData		= CStartupDlg::IsUserDataEnabled (db);
		dlgStartup.m_bSerialData	= CStartupDlg::IsSerialDataEnabled (db);
	}

	/*
	{
		sheet.AddPage (&dlgWDT);

		if (GetSettingsRecord (db, 0, _T ("Watchdog"), s)) {
			CStringArray v;

			FoxjetDatabase::FromString (s.m_strData, v);

			if (v.GetSize () >= 1) dlgWDT.m_motherboard		= (MOTHERBOARDTYPE)_ttoi (v [0]);
			if (v.GetSize () >= 2) dlgWDT.m_lCardTimeout	= _ttoi (v [1]);
			if (v.GetSize () >= 3) dlgWDT.m_lWDT			= _ttoi (v [2]);
			if (v.GetSize () >= 4) dlgWDT.m_bWDT			= _ttoi (v [3]) ? true : false;
		}
	}
	*/
	#endif //__WINPRINTER__
	
	{
		sheet.AddPage (&dlgSW0883);

		if (GetSettingsRecord (db, 0, _T ("sw0883"), s)) {
			CStringArray v;

			FoxjetDatabase::FromString (s.m_strData, v);

			if (v.GetSize () >= 1) dlgSW0883.m_bContinuousPC = _ttoi (v [0]) ? true : false;
		}
	}

	if (bValve) {
		sheet.AddPage (&dlgIDS);

		if (GetSettingsRecord (db, 0, lpszIDSKey, s)) {
			CStringArray v;

			FoxjetDatabase::FromString (s.m_strData, v);

			if (v.GetSize () >= 1) dlgIDS.m_dwAddress = _tcstoul (v [0], NULL, 16);
			if (v.GetSize () >= 2) dlgIDS.m_strMacAddr = v [1];
		}
	}

	{
		dlgTimeChange.m_bSuppress = !CTimeChangeDlg::IsTimeChangeMessageEnabled (db);
		sheet.AddPage (&dlgTimeChange);
	}

	if (IsNetworked ()) {
		CString str = ::GetCommandLine ();

		str.MakeUpper ();

		CString strLocalDSN = Extract (str, _T ("/DSN_LOCAL="), _T (" "), false);

		dlgNetwork.Load (db);
		sheet.AddPage (&dlgNetwork);

		if (strLocalDSN.GetLength ()) {
			dlgLocalDb.Load (db);
			sheet.AddPage (&dlgLocalDb);
		}
	}

	sheet.AddPage (&dlgInkCode);

	BEGIN_TRANS (db);

	int nResult = sheet.DoModal ();

	if (nResult == IDOK) {
		CString str;

		vCommChanged = dlgComm.m_vDirty;

		{
			s.m_lLineID = 0;
			s.m_strKey = lpszScannerKey;
			s.m_strData = dlgScanner.ToString (dlgScanner.m_scanner);
			TRACEF (s.m_strData);

			if (!UpdateSettingsRecord (db, s))
				VERIFY (AddSettingsRecord (db, s));
		}

		if (bValve) {
			CStringArray v;

			str.Format (_T ("%p"), dlgIDS.m_dwAddress);
			
			v.Add (str);
			v.Add (dlgIDS.m_strMacAddr);

			s.m_lLineID = 0;
			s.m_strKey = lpszIDSKey;
			s.m_strData = FoxjetDatabase::ToString (v);
			TRACEF (s.m_strData);

			if (!UpdateSettingsRecord (db, s))
				VERIFY (AddSettingsRecord (db, s));
		}

		#ifdef __WINPRINTER__
		{
			CStringArray v;

			v.Add (ToString (dlgStartup.m_bAutoStart));
			v.Add (ToString (dlgStartup.m_bUserData));
			v.Add (ToString (dlgStartup.m_bSerialData));
			
			s.m_lLineID = 0;
			s.m_strKey = lpszStartupKey;
			s.m_strData = FoxjetDatabase::ToString (v);
			TRACEF (s.m_strData);

			if (!UpdateSettingsRecord (db, s))
				VERIFY (AddSettingsRecord (db, s));
		}

		{
			CTimeChangeDlg::EnableTimeChangeMessage (db, dlgTimeChange.m_bSuppress ? false : true);

			#ifdef __WINPRINTER__
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strRegSection + _T ("\\time"), _T ("format1"), dlgTimeChange.m_strTimeFormat [0]); 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strRegSection + _T ("\\time"), _T ("format2"), dlgTimeChange.m_strTimeFormat [1]); 
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strRegSection + _T ("\\time"), _T ("format3"), dlgTimeChange.m_strTimeFormat [2]); 
			#endif //__WINPRINTER__
		}

		/*
		{
			CStringArray v;

			v.Add (FoxjetDatabase::ToString ((int)dlgWDT.m_motherboard));
			v.Add (FoxjetDatabase::ToString ((int)dlgWDT.m_lCardTimeout));
			v.Add (FoxjetDatabase::ToString ((int)dlgWDT.m_lWDT));
			v.Add (FoxjetDatabase::ToString ((int)dlgWDT.m_bWDT));

			s.m_lLineID = 0;
			s.m_strKey = _T ("Watchdog");
			s.m_strData = FoxjetDatabase::ToString (v);
			TRACEF (s.m_strData);

			if (!UpdateSettingsRecord (db, s))
				VERIFY (AddSettingsRecord (db, s));
		}
		*/
		#endif //__WINPRINTER__

		{
			CStringArray v;

			v.Add (FoxjetDatabase::ToString ((int)dlgSW0883.m_bContinuousPC));

			s.m_lLineID = 0;
			s.m_strKey = _T ("sw0883");
			s.m_strData = FoxjetDatabase::ToString (v);
			TRACEF (s.m_strData);

			if (!UpdateSettingsRecord (db, s))
				VERIFY (AddSettingsRecord (db, s));
		}

		if (IsGojo ()) {
			s.m_lLineID = 0;
			s.m_strKey = _T ("SYSID:") + ToString (GetPrinterID (db));
			s.m_strData = dlgSysID.m_strSysID;
			TRACEF (s.m_strData);

			if (!UpdateSettingsRecord (db, s))
				VERIFY (AddSettingsRecord (db, s));
		}

		if (IsNetworked ()) {
			dlgNetwork.Save (db);
			dlgLocalDb.Save (db);
		}

		FoxjetDatabase::Encrypted::WriteProfileString (FoxjetDatabase::Encrypted::InkCodes::GetRegSection (), _T ("Enabled"), ToString (dlgInkCode.m_bEnabled));

		bResult = true;
		COMMIT_TRANS (db);
	}
	else {
		ROLLBACK_TRANS (db);
	}

	return bResult;
}
