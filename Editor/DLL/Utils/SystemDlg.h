#if !defined(AFX_SYSTEMDLG_H__178BD0B5_3711_41B5_B523_FC65B0B4F75B__INCLUDED_)
#define AFX_SYSTEMDLG_H__178BD0B5_3711_41B5_B523_FC65B0B4F75B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SystemDlg.h : header file
//

#include "UtilApi.h"
#include "RoundButtons.h"
#include "EnTabCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CSystemDlg

namespace FoxjetCommon
{
	class UTIL_API CSystemDlg : public CEnPropertySheet
	{
		DECLARE_DYNAMIC(CSystemDlg)

	// Construction
	public:
		CSystemDlg(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
		CSystemDlg(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

	// Attributes
	public:

	// Operations
	public:

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CSystemDlg)
		//}}AFX_VIRTUAL

	// Implementation
	public:
		virtual ~CSystemDlg();


		// Generated message map functions
	protected:
		CBitmap m_bmpOK;
		CBitmap m_bmpCancel;
		FoxjetUtils::CRoundButton2 m_btnOK;
		FoxjetUtils::CRoundButton2 m_btnCancel;

		//{{AFX_MSG(CSystemDlg)
		virtual BOOL OnInitDialog();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace FoxjetCommon

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SYSTEMDLG_H__178BD0B5_3711_41B5_B523_FC65B0B4F75B__INCLUDED_)
