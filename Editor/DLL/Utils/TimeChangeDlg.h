#if !defined(AFX_TIMECHANGEPAGE_H__AE148588_E2CA_4728_9102_37B5369947A2__INCLUDED_)
#define AFX_TIMECHANGEPAGE_H__AE148588_E2CA_4728_9102_37B5369947A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TimeChangePage.h : header file
//

#include "UtilApi.h"
#include "Database.h"
#include "System.h"

/////////////////////////////////////////////////////////////////////////////
// CTimeChangeDlg dialog
namespace FoxjetCommon
{
	class UTIL_API CTimeChangeDlg : public CPropertyPage
	{
		DECLARE_DYNCREATE(CTimeChangeDlg)

	// Construction
	public:
		CTimeChangeDlg();
		~CTimeChangeDlg();

		CString m_strTimeFormat [3];
		LPFORMATDATETIME m_lpFormatFct;

	// Dialog Data
		//{{AFX_DATA(CTimeChangeDlg)
		BOOL	m_bSuppress;
		//}}AFX_DATA

		static bool IsTimeChangeMessageEnabled (FoxjetDatabase::COdbcDatabase & db);
		static void EnableTimeChangeMessage (FoxjetDatabase::COdbcDatabase & db, bool bEnable);

	// Overrides
		// ClassWizard generate virtual function overrides
		//{{AFX_VIRTUAL(CTimeChangeDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual BOOL OnInitDialog();

		// Generated message map functions
		//{{AFX_MSG(CTimeChangeDlg)
			// NOTE: the ClassWizard will add member functions here
		//}}AFX_MSG

		afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
		afx_msg void OnUpdateUI ();
		afx_msg void OnTimeEdited ();
		afx_msg void OnTimer( UINT nIDEvent );

		bool m_bSetTime;
		HBRUSH m_hbrDlg;

		DECLARE_MESSAGE_MAP()

	};
}; //namespace FoxjetCommon

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TIMECHANGEPAGE_H__AE148588_E2CA_4728_9102_37B5369947A2__INCLUDED_)
