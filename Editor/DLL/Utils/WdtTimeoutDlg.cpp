// WdtTimeoutDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "TemplExt.h"
#include "utils.h"
#include "WdtTimeoutDlg.h"
#include "Debug.h"
#include "Database.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;
using namespace FoxjetUtils;

/////////////////////////////////////////////////////////////////////////////
// CWdtTimeoutDlg dialog


CWdtTimeoutDlg::CWdtTimeoutDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg(IDD_WATCHDOGTIMEOUT, pParent)
{
	//{{AFX_DATA_INIT(CWdtTimeoutDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CWdtTimeoutDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWdtTimeoutDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWdtTimeoutDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CWdtTimeoutDlg)
	ON_LBN_DBLCLK(CB_TIMEOUT, OnDblclkTimeout)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWdtTimeoutDlg message handlers

CString CWdtTimeoutDlg::FormatWDT (MOTHERBOARDTYPE b, ULONG lWDT)
{
	CString str;

	if (b == MOTHERBOARD_REV2) {
		CString strUnits = LoadString ((lWDT & 0x08) ? IDS_MIN : IDS_SEC);

		switch (lWDT & 0x07) {
		case 0:	return _T ("1 ")	+ strUnits;
		case 1:	return _T ("2 ")	+ strUnits;
		case 2:	return _T ("4 ")	+ strUnits;
		case 3:	return _T ("8 ")	+ strUnits;
		case 4:	return _T ("16 ")	+ strUnits;
		case 5:	return _T ("32 ")	+ strUnits;
		case 6:	return _T ("64 ")	+ strUnits;
		case 7:	return _T ("128 ")	+ strUnits;
		}
	}
	else {
		str.Format (_T ("%d %s"), lWDT, LoadString (IDS_SEC));
	}

	return str;
}

BOOL CWdtTimeoutDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	if (CListBox * p = (CListBox *)GetDlgItem (CB_TIMEOUT)) {
		if (m_motherboard == MOTHERBOARD_REV2) {
			struct
			{
				LPCTSTR m_lpsz;
				ULONG	m_n;
			} 
			map [] =
			{
				{ _T ("1 "),	0	},
				{ _T ("2 "),	1	},
				{ _T ("4 "),	2	},
				{ _T ("8 "),	3	},
				{ _T ("16 "),	4	},
				{ _T ("32 "),	5	},
				{ _T ("64 "),	6	},
				{ _T ("128 "),	7	},	
			};

			for (int i = 0; i < ARRAYSIZE (map); i++) {
				int nIndex = p->AddString (map [i].m_lpsz + LoadString (IDS_SEC));

				p->SetItemData (nIndex, map [i].m_n);
				
				if (m_lWDT == map [i].m_n) 
					p->SetCurSel (nIndex);
			}
		}
		else {
			for (ULONG i = 1; i <= 0x3E; i++) {
				CString str;

				str.Format (_T ("%d %s"), i, LoadString (IDS_SEC));
				int nIndex = p->AddString (str);

				p->SetItemData (nIndex, i);
				
				if (m_lWDT == i)
					p->SetCurSel (nIndex);
			}
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CWdtTimeoutDlg::OnOK ()
{
	if (CListBox * p = (CListBox *)GetDlgItem (CB_TIMEOUT)) {
		int nIndex = p->GetCurSel ();

		if (nIndex == LB_ERR) {
			MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
			return;
		}

		m_lWDT = p->GetItemData (nIndex);
		FoxjetCommon::CEliteDlg::OnOK ();
	}
}

void CWdtTimeoutDlg::OnDblclkTimeout() 
{
	OnOK ();	
}
