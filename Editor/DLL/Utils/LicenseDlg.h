#if !defined(AFX_LICENSEDLG_H__8B3081E7_B766_4BDB_90A1_04B9110F11B0__INCLUDED_)
#define AFX_LICENSEDLG_H__8B3081E7_B766_4BDB_90A1_04B9110F11B0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LicenseDlg.h : header file
//

#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////
// CLicenseDlg dialog

class CLicenseDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CLicenseDlg)

// Construction
public:
	CLicenseDlg();
	~CLicenseDlg();

// Dialog Data
	//{{AFX_DATA(CLicenseDlg)
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

	FoxjetCommon::CVersion	m_ver;
	CString					m_strApp;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CLicenseDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CLicenseDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LICENSEDLG_H__8B3081E7_B766_4BDB_90A1_04B9110F11B0__INCLUDED_)
