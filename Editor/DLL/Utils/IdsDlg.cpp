// IdsDlg.cpp : implementation file
//

#include "stdafx.h"
#include <Winsock2.h>
#include "utils.h"
#include "IdsDlg.h"
#include "resource.h"
#include "Utils.h"
#include "Debug.h"
#include "SystemDlg.h"
#include "MsgBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetUtils;
using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////

#include "bootp.h"

#define BOOTP_REPLY_PORT 68
#define SockClose(x)    closesocket(x)

static const LPCTSTR lpszMacFmt [] = 
{
	_T ("%02x:%02x:%02x:%02x:%02x:%02x"),
	_T ("%d.%d.%d.%d.%d.%d"),
};

int SendBootpReply(
     int times,
     int Socket,
     unsigned long serverIP,
     unsigned long broadcastIP,
     unsigned long clientIP,
     unsigned char *clientMAC
) {
     int i;
     char udpbuf[sizeof(BOOTP_PACKET)];
     int udpbuflen = sizeof(udpbuf);
     BOOTP_PACKET *ppkt = (BOOTP_PACKET *)udpbuf;
     struct sockaddr dst;

     //  typedef struct  {
     //      unsigned char optype;
     //      unsigned char hwtype;
     //      unsigned char hwlen;
     //      unsigned char hops;
     //      unsigned long xid;
     //      unsigned short secs;
     //      unsigned short unused;
     //      unsigned long ciaddr;
     //      unsigned long yiaddr;
     //      unsigned long siaddr;
     //      unsigned long giaddr;
     //      unsigned char chaddr[16];
     //      unsigned char sname[64];
     //      unsigned char bootfile[128];
     //      unsigned char vendor[64];
     //  } BOOTP_PACKET;

     /*
	 if (bDebug) {
          fflush(stdout);
          fprintf(stderr,"SendBootpReply(%d, %d, "
            "%d.%d.%d.%d, "
            "%d.%d.%d.%d, "
            "%d.%d.%d.%d:%d, "
            "%02x:%02x:%02x:%02x:%02x:%02x)\n",
            times,
            (int)Socket,
            (int)(serverIP>>24)&255, (int)(serverIP>>16)&255, (int)(serverIP>>8)&255, (int)serverIP&255,
            (int)(broadcastIP>>24)&255, (int)(broadcastIP>>16)&255, (int)(broadcastIP>>8)&255, (int)broadcastIP&255,
            (int)(clientIP>>24)&255, (int)(clientIP>>16)&255, (int)(clientIP>>8)&255, (int)clientIP&255,
            BOOTP_REPLY_PORT,
            clientMAC[0],clientMAC[1],clientMAC[2],clientMAC[3],clientMAC[4],clientMAC[5]);
          fflush(stderr);
     }
	 */

     ((struct sockaddr_in *)(&dst))->sin_family = AF_INET;
     ((struct sockaddr_in *)(&dst))->sin_addr.s_addr = htonl(broadcastIP);
     ((struct sockaddr_in *)(&dst))->sin_port = htons(BOOTP_REPLY_PORT);

     memset(ppkt, 0, udpbuflen);      /* clear BOOTP packet */

     ppkt->optype = 2;                /* BOOTREPLY */
     ppkt->hwtype = 1;                /* 10mb ethernet */
     ppkt->hwlen = 6;
     ppkt->hops = 0;
     ppkt->xid = 0;                   /* transaction ID */
     ppkt->secs = 0;                  /* client's secs since boot */
     ppkt->ciaddr = ntohl(0);         /* client IP (from client) */
     ppkt->yiaddr = ntohl(clientIP);  /* client IP (from server) */
     ppkt->siaddr = ntohl(serverIP);  /* server IP (from server) */
     ppkt->giaddr = ntohl(0);         /* gateway IP (optional) */
     ppkt->chaddr[0] = clientMAC[0];  /* client Ethernet address */
     ppkt->chaddr[1] = clientMAC[1];
     ppkt->chaddr[2] = clientMAC[2];
     ppkt->chaddr[3] = clientMAC[3];
     ppkt->chaddr[4] = clientMAC[4];
     ppkt->chaddr[5] = clientMAC[5];

     for (i = 0; i < times; ++i) {
          if (sendto(Socket, udpbuf, udpbuflen, 0, &dst, sizeof(struct sockaddr)) != udpbuflen) {
               // TODO // Err(0,"sendto");
               return(0);         /* error exit */
          }
     }
     return 1;     /* no error exit */
}

/////////////////////////////////////////////////////////////////////////////
// CIdsDlg property page

//IMPLEMENT_DYNCREATE(CIdsDlg, CPropertyPage)

CIdsDlg::CIdsDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection) 
:	m_db (db),
	m_strRegSection (strRegSection),
	m_dwAddress (0),
	m_strMacAddr (_T ("00:00:00:00:00:00")),
	CPropertyPage(IDD_IDSADDR)
{
	//{{AFX_DATA_INIT(CIdsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CIdsDlg::~CIdsDlg()
{
}

void CIdsDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIdsDlg)
	DDX_Control(pDX, IDC_ADDR, m_ctrlAddr);
	//}}AFX_DATA_MAP

	DDX_Text (pDX, TXT_MACADDR, m_strMacAddr);

	if (pDX->m_bSaveAndValidate) {
		int n [6] = { 0 };
		bool bValid = false;

		for (int i = 0; i < ARRAYSIZE (lpszMacFmt); i++) {
			int nResult = _stscanf (m_strMacAddr, lpszMacFmt [i], &n [0], &n [1], &n [2], &n [3], &n [4], &n [5]);

			if (nResult == ARRAYSIZE (n)) {
				bValid = true;
				break;
			}
		}

		if (!bValid) {
			MsgBox (* this, LoadString (IDS_INVALIDMACADDR));
			pDX->PrepareEditCtrl (TXT_MACADDR);
			pDX->Fail ();
		}
	}
}


BEGIN_MESSAGE_MAP(CIdsDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CIdsDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (BTN_SET, OnSetClick)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIdsDlg message handlers

DWORD CIdsDlg::GetDefAddr () 
{
	return htonl (inet_addr ("10.1.2.2"));
}

BOOL CIdsDlg::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	m_ctrlAddr.SetAddress (GetDefAddr ());

	if (m_dwAddress)
		m_ctrlAddr.SetAddress (m_dwAddress);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CIdsDlg::OnOK()
{
	m_ctrlAddr.GetAddress (m_dwAddress);
	CPropertyPage::OnOK ();
}

void CIdsDlg::OnSetClick ()
{
	if (UpdateData ()) {
		int bBroad = 1;
		unsigned long lClientIP = 0;
		unsigned long lBroadcastIP = INADDR_BROADCAST;
		unsigned long lServerIP = 0;
		unsigned char cMAC[6];
		struct sockaddr_in addr;
		SOCKET sockReply = socket (AF_INET,SOCK_DGRAM,IPPROTO_UDP);
		CString strClientIP;
		
		{
			BYTE n [4] = { 0 };

			m_ctrlAddr.GetAddress (n [0], n [1], n [2], n [3]); 
			strClientIP.Format (_T ("%d.%d.%d.%d"), n [0], n [1], n [2], n [3]);
			lClientIP = htonl(inet_addr (w2a (strClientIP)));
		}

		if (sockReply < 0) {
			MsgBox (* this, LoadString (IDS_FAILEDTOCREATESOCKET));
			return;
		}

		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = INADDR_ANY;
		addr.sin_port = 0 ;  // use any old port to reply 

		if (bind(sockReply,(struct sockaddr *)&addr, sizeof addr) != 0)  {
			MsgBox (* this, LoadString (IDS_FAILEDTOBINDSOCKET));
			return;
		}

		if (setsockopt(sockReply,SOL_SOCKET,SO_BROADCAST, (char *)&bBroad,sizeof bBroad) != 0)  {
			MsgBox (* this, LoadString (IDS_SETSOCKOPTFAILED));
			return;
		}

		if ((lClientIP == 0) || (lClientIP == ((unsigned long)-1))) {
			if (struct hostent *pHostEnt = gethostbyname (w2a (strClientIP))) {
				struct  sockaddr_in addr;
		
				addr.sin_addr = *((struct in_addr *)pHostEnt->h_addr_list[0]);
				lClientIP = htonl(addr.sin_addr.s_addr);
			}
		}

		{
			bool bValid = false;

			for (int i = 0; i < ARRAYSIZE (lpszMacFmt); i++) {
				int nResult = _stscanf (m_strMacAddr, lpszMacFmt [i], 
					&cMAC [0], &cMAC [1], &cMAC [2], 
					&cMAC [3], &cMAC [4], &cMAC [5]);

				if (nResult == ARRAYSIZE (cMAC)) {
					bValid = true;
					break;
				}
			}

			if (!bValid) {
				MsgBox (* this, LoadString (IDS_INVALIDMACADDR));
				return;
			}
		}

		SendBootpReply (3, sockReply, lServerIP, lBroadcastIP, lClientIP, cMAC);
		SockClose(sockReply);

		{
			BeginWaitCursor ();
			::Sleep (1000);

			int nTries = 4;
			int nPing = FoxjetCommon::Ping (strClientIP, nTries);
			CString str;

			str.Format (LoadString (IDS_PINGRESULT), 
				strClientIP, nPing, nTries, 
				(int)((double)(nTries - nPing) / (double)nTries * 100.0)); 
			TRACEF (str);
			MsgBox (* this, str);
			EndWaitCursor ();
		}
	}
}