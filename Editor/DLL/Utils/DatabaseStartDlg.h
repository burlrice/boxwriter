#if !defined(AFX_DATABASEDLG_H__1F1D52DD_F22F_464D_B87A_F55BADBEAE8B__INCLUDED_)
#define AFX_DATABASEDLG_H__1F1D52DD_F22F_464D_B87A_F55BADBEAE8B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DatabaseDlg.h : header file
//

#include "UtilApi.h"
#include "OdbcDatabase.h"
#include "RoundButtons.h"
#include "Utils.h"

namespace FoxjetUtils
{
	class UTIL_API CDatabaseStartParams
	{
	public:
		CDatabaseStartParams () : m_nSQLType (0), m_bShow (false) { }

		CString	m_strDSN;
		CString m_strTable;
		CString m_strKeyField;
		CString m_strTaskField;
		int m_nSQLType;
		bool m_bShow;
		CString m_strStartField;	// sw0867
		CString m_strEndField;		// sw0867
		CString m_strOffset;		// sw0867
	};

	/////////////////////////////////////////////////////////////////////////////
	// CDatabaseStartDlg dialog

	class UTIL_API CDatabaseStartDlg : public CDatabaseStartParams, public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		void Save(FoxjetDatabase::COdbcDatabase & db);
		void Load(FoxjetDatabase::COdbcDatabase & db);
		CDatabaseStartDlg(CWnd* pParent = NULL);   // standard constructor
		virtual ~CDatabaseStartDlg ();
		
	// Dialog Data
		//{{AFX_DATA(CDatabaseStartDlg)
		//}}AFX_DATA

		const CString m_strDisabled;
		bool m_bReadOnly;
		bool m_bFreeCache;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CDatabaseStartDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		CRoundButtons m_buttons;
		CString GetCurSelDSN ();
		CString GetCurSelTable ();
		CString GetCurSelKeyField ();
		CString GetCurSelTaskField ();

		CPtrArray m_vTables;

		virtual void OnOK();
		void InitTables (const CString & strDSN, const CString & strTable = _T (""), const CString & strKeyField = _T (""),
			const CString & strTaskField = _T (""),
			const CString & strStartField = _T (""), const CString & strEndField = _T (""),
			const CString & strOffset = _T ("")); 
		void InitFields (const CString & strDSN, const CString & strTable, 
			const CString & strKeyField = _T (""), const CString & strTaskField = _T (""),
			const CString & strStartField = _T (""), const CString & strEndField = _T (""),
			const CString & strOffset = _T (""));

		// Generated message map functions
		//{{AFX_MSG(CDatabaseStartDlg)
		afx_msg void OnDsn();
		afx_msg void OnSelchangeTable();
		virtual BOOL OnInitDialog();
		//}}AFX_MSG
		afx_msg void OnSelchangeDsn();
		
		afx_msg void UpdateKeyField();
		afx_msg void UpdateUI();
		afx_msg void OnFilter ();

		DECLARE_MESSAGE_MAP()
	};
}; //namespace FoxjetUtils

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATABASEDLG_H__1F1D52DD_F22F_464D_B87A_F55BADBEAE8B__INCLUDED_)
