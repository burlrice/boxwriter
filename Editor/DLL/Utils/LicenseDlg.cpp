// LicenseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "utils.h"
#include "LicenseDlg.h"

#include "Resource.h"
#include "AboutDlg.h"
#include "Utils.h"
#include "Debug.h"
#include "Registry.h"
#include "Database.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLicenseDlg property page

IMPLEMENT_DYNCREATE(CLicenseDlg, CPropertyPage)

CLicenseDlg::CLicenseDlg() 
:	CPropertyPage(IDD_LICENSE)
{
	//{{AFX_DATA_INIT(CLicenseDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CLicenseDlg::~CLicenseDlg()
{
}

void CLicenseDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLicenseDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLicenseDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CLicenseDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLicenseDlg message handlers

BOOL CLicenseDlg::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	CString strSection = _T ("Software\\Foxjet\\") + m_strApp;
	CString strKey = FoxjetDatabase::GetProfileString (strSection, _T ("RegKey"), _T (""));
	time_t lActivate = FoxjetDatabase::GetProfileInt (strSection, _T ("Activation"), 0);
	CTime tmActivate (lActivate); 
	TCHAR sz [MAX_PATH] = { 0 };
	TCHAR szTitle [MAX_PATH] = { 0 };
	WORD wIcon = 0;

	if (strKey.GetLength ()) {
		SetDlgItemText (TXT_KEY, strKey);
		SetDlgItemText (TXT_DATE, tmActivate.Format (_T ("%c")));
	}

	::ZeroMemory (sz, sizeof (TCHAR) * MAX_PATH);
	::ZeroMemory (szTitle, sizeof (TCHAR) * MAX_PATH);
	::GetModuleFileName (NULL, sz, ARRAYSIZE (sz) - 1);
	::GetFileTitle (sz, szTitle, ARRAYSIZE (szTitle) - 1);

	if (CStatic * pIcon = (CStatic *)GetDlgItem (IDI_ICON))
		pIcon->SetIcon (::ExtractAssociatedIcon (NULL, sz, &wIcon));

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
