#if !defined(AFX_COMPROPERITESDLG_H__8E8D4627_BE7C_42D5_9FC7_F7E384174D95__INCLUDED_)
#define AFX_COMPROPERITESDLG_H__8E8D4627_BE7C_42D5_9FC7_F7E384174D95__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ComProperitesDlg.h : header file
//

#include "UtilApi.h"
#include "Database.h"

/////////////////////////////////////////////////////////////////////////////
// CComPropertiesDlg dialog

namespace FoxjetCommon
{
	bool UTIL_API EditSerialParams (CString & str, bool bWin32 = false);
	void UTIL_API EnumerateSerialPorts (CUIntArray & v);

	class UTIL_API CComPropertiesDlg : public CPropertyPage
	{
		DECLARE_DYNCREATE(CComPropertiesDlg)

	// Construction
	public:
		CComPropertiesDlg();
		virtual ~CComPropertiesDlg();

	public:
		LONG	m_lBaud;
		int		m_nDatabits;
		int		m_nStopBits;
		char	m_cParity;
		ULONG	m_lLineID;
		bool	m_bPrintOnce;
		int		m_nEncoding;
		bool	m_bBaudRateOnly;

		CArray <FoxjetDatabase::LINESTRUCT, FoxjetDatabase::LINESTRUCT &> m_vLines;

	// Dialog Data
		//{{AFX_DATA(CComPropertiesDlg)
		CString	m_strBaud;
		CString	m_strDatabits;
		CString	m_strParity;
		CString	m_strStopbits;
		int		m_nDeviceType;
		//}}AFX_DATA

		bool m_bWin32;
		bool m_bShowAssignment;

	// Overrides
		// ClassWizard generate virtual function overrides
		//{{AFX_VIRTUAL(CComPropertiesDlg)
		public:
		virtual void OnOK();
		virtual BOOL OnApply();
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		void UpdateSettings ( void );
		void AssignControls ( void );
		void UpdateUI ();

		HBRUSH m_hbrDlg;

		// Generated message map functions
		//{{AFX_MSG(CComPropertiesDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnSelendokBaud();
		afx_msg void OnSelendokDatabits();
		afx_msg void OnSelendokDevicetype();
		afx_msg void OnSelendokParity();
		afx_msg void OnSelendokStopbits();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG

		DECLARE_MESSAGE_MAP()

	};
}; //namespace FoxjetCommon

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMPROPERITESDLG_H__8E8D4627_BE7C_42D5_9FC7_F7E384174D95__INCLUDED_)
