// CardTimeoutDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "utils.h"
#include "CardTimeoutDlg.h"
#include "Database.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCardTimeoutDlg dialog


CCardTimeoutDlg::CCardTimeoutDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg(IDD_HEADTIMEOUT, pParent)
{
	//{{AFX_DATA_INIT(CCardTimeoutDlg)
	m_nTimeout = 0;
	//}}AFX_DATA_INIT
}


void CCardTimeoutDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCardTimeoutDlg)
	DDX_Text(pDX, TXT_TIMEOOUT, m_nTimeout);
	DDV_MinMaxInt(pDX, m_nTimeout, 3, 60);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCardTimeoutDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CCardTimeoutDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCardTimeoutDlg message handlers
