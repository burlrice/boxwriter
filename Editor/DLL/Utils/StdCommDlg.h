#if !defined(AFX_STDCOMMDLG1_H__F82BA64B_89BE_4505_B06B_FFE89B7607A0__INCLUDED_)
#define AFX_STDCOMMDLG1_H__F82BA64B_89BE_4505_B06B_FFE89B7607A0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StdCommDlg.h : header file
//

#include "UtilApi.h"
#include "ListCtrlImp.h"
#include "OdbcDatabase.h"
#include "..\..\..\Control\TypeDefs.h"
#include "..\..\..\FxMphc\Src\TypeDefs.h"
#include "RoundButtons.h"

/////////////////////////////////////////////////////////////////////////////
// CStdCommDlg dialog

namespace FoxjetCommon
{
	namespace StdCommDlg
	{
		class UTIL_API CCommItem : public ItiLibrary::ListCtrlImp::CItem
		{
		public:
			CCommItem (bool bWin32 = true);
			CCommItem (const CCommItem & rhs);
			CCommItem & operator = (const CCommItem & rhs);
			virtual ~CCommItem ();

			DWORD			m_dwPort;
			DWORD			m_dwBaudRate;
			BYTE			m_nByteSize;
			PARITY			m_parity;
			BYTE			m_nStopBits;
			eSerialDevice	m_type;
			ULONG			m_lLineID;
			bool			m_bPrintOnce;
			int				m_nEncoding;

			CString			m_strLine; // for display purposes only
			const bool		m_bWin32;

			CString ToString () const;
			bool FromString (const CString & str);

			virtual CString GetDispText (int nColumn) const;

			static CString ToString (PARITY p);
			static PARITY GetParity (const CString & str);

			static CString Encode (const CString & str, int nEncoding);
		};

		class UTIL_API CStdCommDlg : public CPropertyPage
		{
		//	DECLARE_DYNCREATE(CStdCommDlg)

		// Construction
		public:
			CStdCommDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection);
			~CStdCommDlg();

		// Dialog Data
			//{{AFX_DATA(CStdCommDlg)
				// NOTE - ClassWizard will add data members here.
				//    DO NOT EDIT what you see in these blocks of generated code !
			//}}AFX_DATA

			const CString m_strRegSection;
			FoxjetDatabase::COdbcDatabase & m_db;
			CCopyArray <DWORD, DWORD> m_vDirty;

		// Overrides
			// ClassWizard generate virtual function overrides
			//{{AFX_VIRTUAL(CStdCommDlg)
			protected:
			virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
			//}}AFX_VIRTUAL
		
			public:
		protected:

				ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
				FoxjetUtils::CRoundButtons m_buttons;
				HBRUSH m_hbrDlg;

		// Implementation
		protected:
			// Generated message map functions
			//{{AFX_MSG(CStdCommDlg)
			afx_msg void OnEdit();
			virtual BOOL OnInitDialog();
			//}}AFX_MSG

			afx_msg void OnDblclkPorts(NMHDR* pNMHDR, LRESULT* pResult);
		
			DECLARE_MESSAGE_MAP()

		};
	}; //namespace StdCommDlg

}; //namespace FoxjetCommon

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDCOMMDLG1_H__F82BA64B_89BE_4505_B06B_FFE89B7607A0__INCLUDED_)
