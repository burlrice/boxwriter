// ZoomDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ZoomDlg.h"
#include "Resource.h"
#include "Debug.h"
#include "Database.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;

static const ZOOMSTRUCT defZoom = { 10, 400, 100, NULL };

/////////////////////////////////////////////////////////////////////////////
// CZoomDlg dialog

CZoomDlg::CZoomDlg(LPZOOMFCT lpZoomFunction, const ZOOMSTRUCT * pZoom, 
				   CWnd* pParent)
:	m_lpZoomFct (lpZoomFunction), m_zoom (defZoom),
	FoxjetCommon::CEliteDlg(IDD_ZOOM, pParent)
{
	ASSERT (m_lpZoomFct);

	//{{AFX_DATA_INIT(CZoomDlg)
	m_nZoom = 0;
	//}}AFX_DATA_INIT

	if (pZoom) 
		m_zoom = * pZoom;
}


void CZoomDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CZoomDlg)
	DDX_Text(pDX, TXT_ZOOM, m_nZoom);
	//}}AFX_DATA_MAP
	DDV_MinMaxInt(pDX, m_nZoom, m_zoom.m_nMin, m_zoom.m_nMax);
}


BEGIN_MESSAGE_MAP(CZoomDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CZoomDlg)
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CZoomDlg message handlers

BOOL CZoomDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();

	CSliderCtrl * pSlide = (CSliderCtrl *)GetDlgItem (IDC_ZOOMSLIDE);
	CEdit * pZoom = (CEdit *)GetDlgItem (TXT_ZOOM);
	CString strMin, strMax;

	strMin.Format (_T ("%d %%"), m_zoom.m_nMin);
	strMax.Format (_T ("%d %%"), m_zoom.m_nMax);

	ASSERT (GetDlgItem (LBL_MIN));
	ASSERT (GetDlgItem (LBL_MAX));

	GetDlgItem (LBL_MIN)->SetWindowText (strMin);
	GetDlgItem (LBL_MAX)->SetWindowText (strMax);

	pSlide->SetRange (m_zoom.m_nMin, m_zoom.m_nMax);
	pSlide->SetPos (m_nZoom);
	m_nOriginalZoom = m_nZoom;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CZoomDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CSliderCtrl * pSlide = (CSliderCtrl *)GetDlgItem (IDC_ZOOMSLIDE);
	CEdit * pZoom = (CEdit *)GetDlgItem (TXT_ZOOM);
	ZOOMSTRUCT zs (m_zoom);
	CString str;

	zs.m_nZoom = pSlide->GetPos ();

	bool bSetZoom = (* m_lpZoomFct) (zs);

	if (!bSetZoom) {
		if (zs.m_nZoom < zs.m_nMin)
			zs.m_nZoom = zs.m_nMin;
		else if (zs.m_nZoom > zs.m_nMax)
			zs.m_nZoom = zs.m_nMax;

		pSlide->SetPos (zs.m_nZoom);
	}

	m_nZoom = zs.m_nZoom;
	str.Format (_T ("%d"), m_nZoom);
	pZoom->SetWindowText (str);

	FoxjetCommon::CEliteDlg::OnHScroll(nSBCode, nPos, pScrollBar);
}


BOOL CZoomDlg::DestroyWindow() 
{
	ZOOMSTRUCT zoom (m_zoom);
	(* m_lpZoomFct) (zoom);
	return FoxjetCommon::CEliteDlg::DestroyWindow();
}

void CZoomDlg::OnCancel()
{
	m_nZoom = m_nOriginalZoom;
	FoxjetCommon::CEliteDlg::OnCancel ();
}
