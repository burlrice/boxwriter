#if !defined(AFX_SCANNERPROPDLG_H__C51392F8_E8C4_41AF_9C1C_545995BDDCA0__INCLUDED_)
#define AFX_SCANNERPROPDLG_H__C51392F8_E8C4_41AF_9C1C_545995BDDCA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScannerPropDlg.h : header file
//

#include "OdbcDatabase.h"
#include "UtilApi.h"
#include "RoundButtons.h"

namespace FoxjetUtils
{
	const UINT WM_RESETSCANNERERROR	= ::RegisterWindowMessage (_T ("Foxjet::ResetScannerError"));
	const UINT WM_RESETSCANNERINFO	= ::RegisterWindowMessage (_T ("Foxjet::ResetScannerInfo"));

	const LPCTSTR lpszScannerKey	= _T ("Scanner settings");
	const LPCTSTR lpszIDSKey		= _T ("IDS Address");
	const LPCTSTR lpszStartupKey	= _T ("Startup");

	typedef struct
	{
		BOOL	m_bIdle;
		BOOL	m_bReset;
		int		m_nNoReads;
		CString	m_strNoRead;

	} FIXEDSCANNERSTRUCT;

	/////////////////////////////////////////////////////////////////////////////
	// CScannerPropDlg dialog

	class UTIL_API CScannerPropDlg : public CPropertyPage
	{
	//	DECLARE_DYNCREATE(CScannerPropDlg)

	// Construction
	public:
		CScannerPropDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection);
		~CScannerPropDlg();

	// Dialog Data
		//{{AFX_DATA(CScannerPropDlg)
		//}}AFX_DATA
		FIXEDSCANNERSTRUCT m_scanner;

		const CString m_strRegSection;
		FoxjetDatabase::COdbcDatabase & m_db;

		static CString ToString (const FIXEDSCANNERSTRUCT & s);
		static bool FromString (const CString & str, FIXEDSCANNERSTRUCT & s);
		static void Init (FIXEDSCANNERSTRUCT & s);

	// Overrides
		// ClassWizard generate virtual function overrides
		//{{AFX_VIRTUAL(CScannerPropDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		FoxjetUtils::CRoundButtons m_buttons;
		HBRUSH m_hbrDlg;

		// Generated message map functions
		//{{AFX_MSG(CScannerPropDlg)
		afx_msg void OnResetinfo();
		afx_msg void OnResetscanner();
		//}}AFX_MSG
		afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

		DECLARE_MESSAGE_MAP()

	};
}; //namespace FoxjetUtils

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCANNERPROPDLG_H__C51392F8_E8C4_41AF_9C1C_545995BDDCA0__INCLUDED_)
