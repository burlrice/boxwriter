// InkCodePage.cpp : implementation file
//

#include "stdafx.h"
#include "Utils.h"
#include "InkCodePage.h"
#include "afxdialogex.h"
#include "EnTabCtrl.h"
#include "Registry.h"
#include "resource.h"
#include "Database.h"
#include "Debug.h"
#include "WinMsg.h"
#include "Parse.h"

using namespace FoxjetUtils;

// CInkCodePage dialog

IMPLEMENT_DYNAMIC(CInkCodePage, CPropertyPage)

CInkCodePage::CInkCodePage(FoxjetDatabase::COdbcDatabase & db)
:	m_db (db),
	CPropertyPage(CInkCodePage::IDD)
{
	m_psp.dwFlags |= PSH_NOAPPLYNOW | PSH_USECALLBACK;
	m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CInkCodePage::~CInkCodePage()
{
}

void CInkCodePage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control (pDX, TXT_PASSWORD, m_txt);
}


BEGIN_MESSAGE_MAP(CInkCodePage, CPropertyPage)
	ON_BN_CLICKED(BTN_ENABLE, &CInkCodePage::OnBnClickedEnable)
	ON_BN_CLICKED(BTN_DISABLE, &CInkCodePage::OnBnClickedDisable)
END_MESSAGE_MAP()


void CInkCodePage::Init ()
{
	if (CStatic * p = (CStatic *)GetDlgItem (LBL_ENABLED)) {
		bool bEnabled = FoxjetDatabase::Encrypted::InkCodes::IsEnabled ();

		p->SetWindowText (LoadString (bEnabled ? IDS_INKCODESENABLED : IDS_INKCODESDISABLED));
	}
}


// CInkCodePage message handlers

BOOL CInkCodePage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	if (FoxjetUtils::CInkCodeEdit  * p = (FoxjetUtils::CInkCodeEdit *)GetDlgItem (TXT_PASSWORD)) 
		p->Init (this, 6);

	Init ();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CInkCodePage::OnBnClickedEnable()
{
	if (FoxjetUtils::CInkCodeEdit  * p = (FoxjetUtils::CInkCodeEdit *)GetDlgItem (TXT_PASSWORD)) {
		CString str = p->GetInputData ();

		if (!str.CompareNoCase (FoxjetDatabase::Encrypted::InkCodes::GetEnablePassword ())) {
			FoxjetDatabase::Encrypted::InkCodes::Enable (true);
			TRACEF (FoxjetDatabase::ToString (FoxjetDatabase::Encrypted::InkCodes::IsEnabled ()));
			Init ();
			::PostMessage (HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, 0);
			//::PostMessage (HWND_BROADCAST, WM_INKCODE_ENABLE, 1, 0);
			EndDialog (IDCANCEL);
		}
	}
}


void CInkCodePage::OnBnClickedDisable()
{
	if (FoxjetUtils::CInkCodeEdit  * p = (FoxjetUtils::CInkCodeEdit *)GetDlgItem (TXT_PASSWORD)) {
		CString str = p->GetInputData ();

		if (!str.CompareNoCase (FoxjetDatabase::Encrypted::InkCodes::GetDisablePassword ())) {
			FoxjetDatabase::Encrypted::InkCodes::Enable (false);
			TRACEF (FoxjetDatabase::ToString (FoxjetDatabase::Encrypted::InkCodes::IsEnabled ()));
			Init ();
			::PostMessage (HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, 0);
			//::PostMessage (HWND_BROADCAST, WM_INKCODE_ENABLE, 0, 0);
			EndDialog (IDCANCEL);
		}
	}
}
