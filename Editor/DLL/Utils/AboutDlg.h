#if !defined(AFX_ABOUTDLG1_H__2EB999EF_A528_4DBC_AC8E_5B71289568DB__INCLUDED_)
#define AFX_ABOUTDLG1_H__2EB999EF_A528_4DBC_AC8E_5B71289568DB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AboutDlg1.h : header file
//

#include "Version.h"
#include "UtilApi.h"
#include "EnTabCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg

namespace FoxjetCommon
{
	class UTIL_API CAboutDlg : public CEnPropertySheet
	{
		DECLARE_DYNAMIC(CAboutDlg)

	// Construction
	public:
		CAboutDlg(const CVersion & ver, const CStringArray & vCmdLine, UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
		CAboutDlg(const CVersion & ver, const CStringArray & vCmdLine, LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

	// Attributes
	public:

	// Operations
	public:

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CAboutDlg)
		//}}AFX_VIRTUAL

	// Implementation
	public:
		virtual ~CAboutDlg();

		CString			m_strTitle;
		CString			m_strApp;
		CVersion		m_ver;
		CStringArray	m_vCmdLine;

		// Generated message map functions
	protected:
		void Init();

		CPropertyPage * m_pPage1;
		CPropertyPage * m_pPage2;
		CPropertyPage * m_pPage3;

		//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace FoxjetCommon

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ABOUTDLG1_H__2EB999EF_A528_4DBC_AC8E_5B71289568DB__INCLUDED_)
