﻿// StdCommDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "Utils.h"
#include "StdCommDlg.h"
#include "Parse.h"
#include "TemplExt.h"
#include "ComPropertiesDlg.h"
#include "Database.h"
#include "AppVer.h"
#include "Debug.h"
#include "Parse.h"
#include "SystemDlg.h"

using namespace FoxjetUtils;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////

using namespace FoxjetCommon::StdCommDlg;

CCommItem::CCommItem (bool bWin32)
:	m_bWin32 (bWin32),
	m_dwBaudRate (9600),
	m_parity (NO_PARITY),
	m_nByteSize (8),
	m_nStopBits (0),
	m_type (NOTUSED),
	m_lLineID (-1),
	m_bPrintOnce (false),
	m_nEncoding (0)
{
}

CCommItem::CCommItem (const CCommItem & rhs)
:	m_bWin32		(rhs.m_bWin32),
	m_dwBaudRate	(rhs.m_dwBaudRate),
	m_parity		(rhs.m_parity),
	m_nByteSize		(rhs.m_nByteSize),
	m_nStopBits		(rhs.m_nStopBits),
	m_type			(rhs.m_type),
	m_lLineID		(rhs.m_lLineID),
	m_strLine		(rhs.m_strLine),
	m_bPrintOnce	(rhs.m_bPrintOnce),
	m_nEncoding		(rhs.m_nEncoding)
{
}

CCommItem & CCommItem::operator = (const CCommItem & rhs)
{
	if (this != &rhs) {
		m_dwBaudRate	= rhs.m_dwBaudRate;
		m_parity		= rhs.m_parity;
		m_nByteSize		= rhs.m_nByteSize;
		m_nStopBits		= rhs.m_nStopBits;
		m_type			= rhs.m_type;
		m_lLineID		= rhs.m_lLineID;
		m_strLine		= rhs.m_strLine;
		m_bPrintOnce	= rhs.m_bPrintOnce;
		m_nEncoding		= rhs.m_nEncoding;
	}

	return * this;
}

CCommItem::~CCommItem ()
{
}

CString CCommItem::ToString () const
{
	CStringArray v;
	bool bPrintOnce = false;

	switch (m_type) {
	case HANDSCANNER:
	case REMOTEDEVICE:
		bPrintOnce = m_bPrintOnce;
		break;
	}

	v.Add (FoxjetDatabase::ToString (m_dwPort));
	v.Add (FoxjetDatabase::ToString (m_dwBaudRate));
	v.Add (FoxjetDatabase::ToString (m_nByteSize));
	v.Add (FoxjetDatabase::ToString (m_parity));
	v.Add (FoxjetDatabase::ToString (m_nStopBits));
	v.Add (FoxjetDatabase::ToString ((int)m_type));
	v.Add (FoxjetDatabase::ToString ((long)m_lLineID));
	v.Add (FoxjetDatabase::ToString ((long)bPrintOnce));
	v.Add (FoxjetDatabase::ToString (m_nEncoding));

	return FoxjetDatabase::ToString (v);
}

bool CCommItem::FromString (const CString & str)
{
	CStringArray v;

	FoxjetDatabase::FromString (str, v);

	if (v.GetSize () >= 6) {
		int nIndex = 0;

		m_dwPort		=					_tcstoul (v [nIndex++], NULL, 10);
		m_dwBaudRate	=					_tcstoul (v [nIndex++], NULL, 10);
		m_nByteSize		= (BYTE)			_tcstoul (v [nIndex++], NULL, 10);
		m_parity		= (PARITY)			_tcstoul (v [nIndex++], NULL, 10);
		m_nStopBits		= (BYTE)			_tcstoul (v [nIndex++], NULL, 10);
		m_type			= (eSerialDevice)	_tcstoul (v [nIndex++], NULL, 10);

		if (v.GetSize () >= 7) 
			m_lLineID	=					_tcstoul (v [nIndex++], NULL, 10);

		if (v.GetSize () >= 8) 
			m_bPrintOnce =					_tcstoul (v [nIndex++], NULL, 10) ? true : false;

		if (v.GetSize () >= 9) 
			m_nEncoding =					_tcstoul (v [nIndex++], NULL, 10);

		return true;
	}

	return false;
}

CString CCommItem::ToString (PARITY p)
{
	switch (p) {
	default:
	case NO_PARITY:		return _T ("N");
	case EVEN_PARITY:	return _T ("E");
	case ODD_PARITY:	return _T ("O");
	}
}

PARITY CCommItem::GetParity (const CString & str)
{
	if (str.GetLength ()) {
		switch (str [0]) {
		default:
		case 'N':	return NO_PARITY;
		case 'E':	return EVEN_PARITY;
		case 'O':	return ODD_PARITY;
		}
	}

	return NO_PARITY;
}

CString CCommItem::GetDispText (int nColumn) const
{
	CString str;

	struct 
	{
		eSerialDevice	m_type;
		UINT			m_nID;
	} static const map [] = 
	{
		HANDSCANNER,		IDS_HANDSCANNER,
		SW0858_DBSTART,		IDS_DBSTART,
		FIXEDSCANNER,		IDS_FIXEDSCANNER,
		REMOTEDEVICE,		IDS_REMOTEDEVICE,
		HOSTINTERFACE,		IDS_HOSTINTERFACE,
		NOTUSED,			IDS_NOTUSED,
	};

	switch (nColumn) {
	case 0:
		str.Format (_T ("COM%d"), m_dwPort);
		break;
	case 1:
		str.Format (_T ("%d, %s, %d, "), 
			m_dwBaudRate,
			ToString (m_parity),
			m_nByteSize);
		
		if (m_bWin32) {
			switch (m_nStopBits) {
			default:
			case 0:		str += _T ("1, ");		break;
			case 1:		str += _T ("1.5, ");	break;
			case 2:		str += _T ("2, ");		break;
			}
		}
		else {
			CString strTmp;

			strTmp.Format (_T ("%d, "), m_nStopBits);
			str += strTmp;
		}

		str += FoxjetDatabase::ToString ((int)m_type);
		break;
	case 2:
		{
			for (int i = 0; i < ARRAYSIZE (map); i++) 
				if (map [i].m_type == m_type)
					return LoadString (map [i].m_nID);
		}
		break;
	case 3:
		if (m_type == FIXEDSCANNER)
			return m_strLine;
	}

	return str;
}

CString CCommItem::Encode (const CString & str, int nEncoding)
{
	CString strData = str;

	switch (nEncoding) {
	case 1251:
	case 1252:
		for (int i = strData.GetLength () - 1; i >= 0; i--) {
			BYTE n = (char)strData [i];
			TCHAR sz [2] = { 0 };

			int nMultiByteToWideChar = ::MultiByteToWideChar (nEncoding, 0, (const char *)&n, 1, sz, ARRAYSIZE (sz));

			if (nMultiByteToWideChar) {
				strData.Delete (i);
				strData.Insert (i, sz);
			}
		}
		break;
	case 2312:
		strData = FoxjetCommon::gb2312 ((std::string)w2a (str)).c_str ();
		break;
	}

	return strData;
}

/////////////////////////////////////////////////////////////////////////////
// CStdCommDlg property page

//IMPLEMENT_DYNCREATE(CStdCommDlg, CPropertyPage)

CStdCommDlg::CStdCommDlg (FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection) 
:	m_db (db),
	m_strRegSection (strRegSection),
	m_hbrDlg (NULL),
	CPropertyPage (IDD_WIN32COMM_MATRIX)
{
	//{{AFX_DATA_INIT(CStdCommDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_psp.dwFlags |= PSH_NOAPPLYNOW | PSH_USECALLBACK;
	m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CStdCommDlg::~CStdCommDlg()
{
	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}
}

void CStdCommDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStdCommDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	if (!m_buttons.m_prgbBack)
		m_buttons.m_prgbBack = new COLORREF (IsControl (false) ? FoxjetUtils::GetButtonColor (_T ("background")) : ::GetSysColor (COLOR_BTNFACE)); 

	m_buttons.DoDataExchange (pDX, BTN_EDIT);
}


BEGIN_MESSAGE_MAP(CStdCommDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CStdCommDlg)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	//}}AFX_MSG_MAP
	ON_NOTIFY(NM_DBLCLK, LV_PORTS, OnDblclkPorts)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStdCommDlg message handlers

void CStdCommDlg::OnDblclkPorts(NMHDR* pNMHDR, LRESULT* pResult)
{
	OnEdit ();

	if (pResult)
		* pResult = 0;
}

void CStdCommDlg::OnEdit() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		SETTINGSSTRUCT s;
		CString strKey;
		CEnPropertySheet sheet;
		CComPropertiesDlg dlg;
		CCommItem * p = (CCommItem *)m_lv.GetCtrlData (sel [0]);

		sheet.m_psh.dwFlags |= PSH_USECALLBACK;
		sheet.m_psh.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetProc;

		ASSERT (p);

		GetPrinterLines (m_db, GetPrinterID (m_db), dlg.m_vLines); //GetLineRecords (m_db, dlg.m_vLines);

		dlg.m_bWin32		= true;
		dlg.m_lBaud			= p->m_dwBaudRate;
		dlg.m_nDatabits		= p->m_nByteSize;
		dlg.m_cParity		= (char)CCommItem::ToString (p->m_parity).GetAt (0);
		dlg.m_nStopBits		= p->m_nStopBits;
		dlg.m_nDeviceType	= p->m_type;
		dlg.m_lLineID		= p->m_lLineID;
		dlg.m_bPrintOnce	= p->m_bPrintOnce;
		dlg.m_nEncoding		= p->m_nEncoding;

		sheet.AddPage (&dlg);
		sheet.SetTitle (LoadString (IDS_SERIALSETTINGS));

		if (sheet.DoModal () == IDOK) {
			LINESTRUCT line;
			CUIntArray v;

			FoxjetCommon::EnumerateSerialPorts (v);
			GetLineRecord (m_db, dlg.m_lLineID, line);

			p->m_dwBaudRate		= dlg.m_lBaud;
			p->m_nByteSize		= dlg.m_nDatabits;
			p->m_parity			= CCommItem::GetParity (dlg.m_cParity);
			p->m_nStopBits		= dlg.m_nStopBits;
			p->m_type			= (eSerialDevice)dlg.m_nDeviceType; 
			p->m_lLineID		= dlg.m_lLineID;
			p->m_strLine		= line.m_strName;
			p->m_bPrintOnce		= dlg.m_bPrintOnce;
			p->m_nEncoding		= dlg.m_nEncoding;

			s.m_lLineID = 0;
			s.m_strKey.Format (_T ("COM%d"), p->m_dwPort);
			s.m_strData = p->ToString ();

			m_vDirty.Add (p->m_dwPort);

			if (!UpdateSettingsRecord (m_db, s)) 
				VERIFY (AddSettingsRecord (m_db, s));

			m_lv.UpdateCtrlData (p);
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

BOOL CStdCommDlg::OnInitDialog() 
{
	using namespace ItiLibrary::ListCtrlImp;

	CArray <CColumn, CColumn> vCols;
	CUIntArray v;

	CPropertyPage::OnInitDialog();
	CString strPort;

	strPort.LoadString (IDS_PORT);
	
	vCols.Add (CColumn (LoadString (IDS_PORT), 150));
	vCols.Add (CColumn (LoadString (IDS_SETTINGS), 220));
	vCols.Add (CColumn (LoadString (IDS_USAGE), 150));
	vCols.Add (CColumn (LoadString (IDS_LINE), 150));

	m_lv.Create (LV_PORTS, vCols, this, m_strRegSection);	
	
	EnumerateSerialPorts (v);

	for (int i = 0; i < v.GetSize (); i++) {
		CCommItem * p = new CCommItem;
		SETTINGSSTRUCT s;
		CString strKey;

		p->m_dwPort = v [i];
		strKey.Format (_T ("COM%d"), p->m_dwPort);

		if (GetSettingsRecord (m_db, 0, strKey, s)) {
			LINESTRUCT line;

			p->FromString (s.m_strData);
			GetLineRecord (m_db, p->m_lLineID, line);
			p->m_strLine = line.m_strName;
		}

		m_lv.InsertCtrlData (p);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


