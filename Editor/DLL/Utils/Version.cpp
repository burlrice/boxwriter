// Version.cpp: implementation of the CVersion class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Version.h"
#include "Utils.h"
#include "Debug.h"
#include "Parse.h"

using namespace FoxjetUtils;
using namespace FoxjetCommon;

void CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer);

//////////////////////////////////////////////////////////////////////

static CString Format (const FoxjetCommon::VERSIONSTRUCT & v)
{
	CString str;

	str.Format (_T ("%010d.%010d.%010d.%010d.%010d"), 
		v.m_nMajor,			v.m_nMinor, 
		v.m_nCustomMajor,	v.m_nCustomMinor, 
		v.m_nInternal);

	return str;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL (CVersion, CObject, 1)

CVersion::CVersion ()
{
	fj_GetDllVersion (&m_ver);
}

CVersion::CVersion(UINT nMajor, UINT nMinor, UINT nCustomMajor, UINT nCustomMinor, UINT nInternal)
{
	m_ver.m_nMajor			= nMajor;
	m_ver.m_nMinor			= nMinor;
	m_ver.m_nCustomMajor	= nCustomMajor;
	m_ver.m_nCustomMinor	= nCustomMinor;
	m_ver.m_nInternal		= nInternal;
}

CVersion::CVersion(const FoxjetCommon::VERSIONSTRUCT & rhs)
{
	m_ver.m_nMajor			= rhs.m_nMajor;
	m_ver.m_nMinor			= rhs.m_nMinor; 
	m_ver.m_nCustomMajor	= rhs.m_nCustomMajor;
	m_ver.m_nCustomMinor	= rhs.m_nCustomMinor;
	m_ver.m_nInternal		= rhs.m_nInternal;
}

CVersion::CVersion (const CVersion & rhs)
{
	m_ver.m_nMajor			= rhs.m_ver.m_nMajor;
	m_ver.m_nMinor			= rhs.m_ver.m_nMinor; 
	m_ver.m_nCustomMajor	= rhs.m_ver.m_nCustomMajor;
	m_ver.m_nCustomMinor	= rhs.m_ver.m_nCustomMinor;
	m_ver.m_nInternal		= rhs.m_ver.m_nInternal;
}

CVersion::CVersion (const CString & strIpVer)
{
	int nMajor = 0, nMinor = 0, nBuild = 0;
	CStringArray v;

	FoxjetDatabase::Tokenize (strIpVer, v, '.');

	if (v.GetSize () >= 2) {
		CString strMinor = v [1];

		if (strMinor.GetLength () >= 2) {
			nBuild = _ttoi (strMinor.Mid (2));
			strMinor = strMinor.Mid (0, 2);
		}

		nMajor = _ttoi (v [0]);
		nMinor = _ttoi (strMinor);

		if (strMinor.GetLength () == 1)
			nMinor *= 10;
	}

	m_ver.m_nMajor			= nMajor;
	m_ver.m_nMinor			= nMinor; 
	m_ver.m_nCustomMajor	= 0;
	m_ver.m_nCustomMinor	= 0;
	m_ver.m_nInternal		= nBuild;
}

CVersion & CVersion::operator = (const CVersion & rhs)
{
	if (&m_ver != &(rhs.m_ver)) {
		m_ver.m_nMajor			= rhs.m_ver.m_nMajor;
		m_ver.m_nMinor			= rhs.m_ver.m_nMinor;
		m_ver.m_nCustomMajor	= rhs.m_ver.m_nCustomMajor;
		m_ver.m_nCustomMinor	= rhs.m_ver.m_nCustomMinor;
		m_ver.m_nInternal		= rhs.m_ver.m_nInternal;
	}

	return * this;
}

CVersion::~CVersion()
{
}

void CVersion::Serialize(CArchive &ar) const 
{
	if (ar.IsStoring ()) 
		ar	<< m_ver.m_nMajor << m_ver.m_nMinor 
			<< m_ver.m_nCustomMajor << m_ver.m_nCustomMinor 
			<< m_ver.m_nInternal;
}

void CVersion::Serialize(CArchive &ar)
{
	CObject::Serialize (ar);

	if (ar.IsStoring ()) 
		ar	<< m_ver.m_nMajor << m_ver.m_nMinor 
			<< m_ver.m_nCustomMajor << m_ver.m_nCustomMinor 
			<< m_ver.m_nInternal;
	else 
		ar	>> m_ver.m_nMajor >> m_ver.m_nMinor 
			>> m_ver.m_nCustomMajor >> m_ver.m_nCustomMinor 
			>> m_ver.m_nInternal;
}

CString CVersion::Format(bool bShowInternal) const
{
	CString str;

	if (m_ver.m_nCustomMajor || m_ver.m_nCustomMinor) {
		/*
		str.Format (_T ("%d.%02d.%02d-%04d [%d]"),
			m_ver.m_nMajor, m_ver.m_nMinor,
			m_ver.m_nCustomMinor, m_ver.m_nCustomMajor, m_ver.m_nInternal);
		*/

		str.Format (_T ("%d.%02d.%04d.%03d"),
			m_ver.m_nMajor, m_ver.m_nMinor, m_ver.m_nCustomMajor, m_ver.m_nInternal);
	}
	else {
		if (m_ver.m_nInternal)
			str.Format (_T ("%d.%02d [%d]"),
				m_ver.m_nMajor, m_ver.m_nMinor, m_ver.m_nInternal);
		else
			str.Format (_T ("%d.%02d"),
				m_ver.m_nMajor, m_ver.m_nMinor);
	}

	return str;
}

CString CVersion::ToString() const
{
	CString str;
	
	str.Format (_T ("{Ver,%u,%u,%u,%u,%u}"), 
		m_ver.m_nMajor,
		m_ver.m_nMinor,
		m_ver.m_nCustomMajor,
		m_ver.m_nCustomMinor,
		m_ver.m_nInternal);

	return str;
}

bool CVersion::FromString(const CString &str)
{
	CStringArray v;
	UINT n [] = { 1, 0, 0, 0, 0 };

	FoxjetDatabase::Tokenize (str, v);

	if (v.GetSize () && v [0].CompareNoCase (_T ("Ver")) != 0)
		return false;

	for (int i = 1; i < min (v.GetSize (), 6); i++) 
		n [i - 1] = (UINT)atol (w2a (v [i]));

	if (v.GetSize ()) {
		m_ver.m_nMajor			= n [0];
		m_ver.m_nMinor			= n [1];
		m_ver.m_nCustomMajor	= n [2];
		m_ver.m_nCustomMinor	= n [3];
		m_ver.m_nInternal		= n [4];

		return true;
	}

	return false;
}

UTIL_API bool operator == (const FoxjetCommon::VERSIONSTRUCT &lhs, const FoxjetCommon::VERSIONSTRUCT &rhs)
{
	return Format (lhs) == Format (rhs);
}

UTIL_API bool operator != (const FoxjetCommon::VERSIONSTRUCT &lhs, const FoxjetCommon::VERSIONSTRUCT &rhs)
{
	return (!(lhs == rhs));
}

UTIL_API bool operator >  (const FoxjetCommon::VERSIONSTRUCT &lhs, const FoxjetCommon::VERSIONSTRUCT &rhs)
{
	return Format (lhs) > Format (rhs);
}

UTIL_API bool operator >= (const FoxjetCommon::VERSIONSTRUCT &lhs, const FoxjetCommon::VERSIONSTRUCT &rhs)
{
	return lhs > rhs || lhs == rhs;
}

UTIL_API bool operator <  (const FoxjetCommon::VERSIONSTRUCT &lhs, const FoxjetCommon::VERSIONSTRUCT &rhs)
{
	return Format (lhs) < Format (rhs);
}

UTIL_API bool operator <= (const FoxjetCommon::VERSIONSTRUCT &lhs, const FoxjetCommon::VERSIONSTRUCT &rhs)
{
	return lhs < rhs || lhs == rhs;
}

bool CVersion::IsSynonym (const FoxjetCommon::CVersion & ver) const
{
//	TRACEF (ToString () + CString (" [") + ver.ToString () + CString ("]"));

	bool bResult = m_ver >= ver.m_ver;

	return bResult;
}

bool CVersion::IsCustom (const FoxjetCommon::CVersion & ver)
{
	return GetCustomMajor () == ver.GetCustomMajor ();
}
