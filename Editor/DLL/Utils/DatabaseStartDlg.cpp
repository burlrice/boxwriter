// DatabaseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "control.h"
#include "Resource.h"
#include "DatabaseStartDlg.h"
#include "OdbcDatabase.h"
#include "OdbcRecordset.h"
#include "OdbcTable.h"
#include "Parse.h"
#include "TableTypeDlg.h"
#include "TemplExt.h"
#include "WinMsg.h"

using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace FoxjetUtils;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDatabaseStartDlg dialog


CDatabaseStartDlg::CDatabaseStartDlg(CWnd* pParent /*=NULL*/)
:	m_strDisabled (_T ("[") + LoadString (IDS_NOTUSED) + _T ("]")),
	m_bReadOnly (false),
	m_bFreeCache (true),
	FoxjetCommon::CEliteDlg(IDD_SW0858DATABASE_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CDatabaseStartDlg)
	m_strDSN = _T("");
	//}}AFX_DATA_INIT
}

CDatabaseStartDlg::~CDatabaseStartDlg ()
{
	COdbcDatabase::FreeTables (m_vTables);
}

void CDatabaseStartDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_DSN);
	m_buttons.DoDataExchange (pDX, BTN_FILTER);
	m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFORMATION);

	if (pDX->m_bSaveAndValidate)
		UpdateKeyField ();
	
	//{{AFX_DATA_MAP(CDatabaseStartDlg)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDatabaseStartDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDatabaseStartDlg)
	ON_BN_CLICKED(BTN_DSN, OnDsn)
	ON_CBN_SELCHANGE(CB_TABLE, OnSelchangeTable)
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE(CB_KEYFIELD, UpdateKeyField)
	ON_CBN_SELCHANGE(CB_DSN, OnSelchangeDsn)
	ON_BN_CLICKED(BTN_FILTER, OnFilter)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDatabaseStartDlg message handlers

void CDatabaseStartDlg::OnSelchangeDsn ()
{
	CString strDSN = GetCurSelDSN ();

	if (strDSN.CompareNoCase (m_strDisabled) != 0) {
		COdbcCache db = COdbcDatabase::Find (strDSN);
		
		BeginWaitCursor ();
		COdbcDatabase::FreeTables (m_vTables);
		db.GetDB ().CopyTables (m_vTables);
		EndWaitCursor ();

		InitTables (strDSN, GetCurSelTable (), GetCurSelKeyField (), GetCurSelTaskField ());
		UpdateUI ();
	}
}

void CDatabaseStartDlg::OnDsn() 
{
	COdbcDatabase db (_T (__FILE__), __LINE__);

	try {
		if (db.Open (NULL)) {
			CString strDSN = Extract (db.GetConnect (), _T ("DSN="), _T (";"));

			int nTables = db.GetTableCount ();

			db.Close ();

			if (!nTables) {
				CString str;

				str.Format (LoadString (IDS_DSNHASNOTABLES), strDSN);
				MsgBox (* this, str);
			}
			else {
				if (CComboBox * p = (CComboBox *)GetDlgItem (CB_DSN)) {
					int nIndex = p->FindString (-1, strDSN);

					if (nIndex == CB_ERR) 
						nIndex = p->AddString (strDSN);

					p->SetCurSel (nIndex);
				}

				OnSelchangeDsn (); //InitTables (strDSN);
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	UpdateUI ();
}

void CDatabaseStartDlg::InitTables (const CString & strDSN, const CString & strTable, const CString & strKeyField, 
							   const CString & strTaskField, const CString & strStartField, const CString & strEndField,
							   const CString & strOffset) 
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	CString strTableTmp (strTable);

	BeginWaitCursor ();

	ASSERT (pTable);
	pTable->ResetContent ();

	if (strDSN.GetLength () && strDSN.CompareNoCase (m_strDisabled) != 0) {
		COdbcCache db = COdbcDatabase::Find (strDSN);

		try {
			CString strType = CTableTypeDlg::GetParams (_T ("Software\\Foxjet\\Control\\Database"));

			for (int i = 0; i < m_vTables.GetSize (); i++) {
				if (COdbcTable * p = (COdbcTable *)m_vTables [i]) {
					CString strFind = _T ("'") + p->GetType () + _T ("'");

					if (strType.Find (strFind) != -1) {
						CString strName = p->GetName ();
						int nIndex = pTable->AddString (strName);

						if (!strName.CompareNoCase (strTable)) 
							pTable->SetCurSel (nIndex);
					}
				}
			}

			if (pTable->GetCurSel () == CB_ERR) {
				pTable->SetCurSel (0);

				if (pTable->GetLBTextLen (0) > 0)
					pTable->GetLBText (0, strTableTmp);
			}

			InitFields (strDSN, strTableTmp, strKeyField, strTaskField, strStartField, strEndField, strOffset);
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	EndWaitCursor ();
	UpdateUI ();
}

void CDatabaseStartDlg::InitFields (const CString & strDSN, const CString & strTable, 
							   const CString & strKeyField, const CString & strTaskField, 
							   const CString & strStartField, const CString & strEndField,
							   const CString & strOffset) 
{
	CComboBox * pTaskField = (CComboBox *)GetDlgItem (CB_TASKFIELD);
	CComboBox * pKeyField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CComboBox * pStartField = (CComboBox *)GetDlgItem (CB_START);
	CComboBox * pEndField = (CComboBox *)GetDlgItem (CB_END);
	CComboBox * pOffset = (CComboBox *)GetDlgItem (CB_OFFSET);

	ASSERT (pTaskField);
	ASSERT (pKeyField);
	ASSERT (pStartField);
	ASSERT (pEndField);
	ASSERT (pOffset);

	pStartField->ResetContent ();
	pEndField->ResetContent ();
	pOffset->ResetContent ();
	pTaskField->ResetContent ();
	pKeyField->ResetContent ();

	if (strDSN.GetLength () && strDSN.CompareNoCase (m_strDisabled) != 0) {
		COdbcCache db = COdbcDatabase::Find (strDSN);
		
		try {
			COdbcRecordset rst (db.GetDB ());
			CString strSQL;

			if (strTable.GetLength ()) {
				strSQL.Format (_T ("SELECT * FROM [%s]"), strTable);

				if (rst.Open (strSQL, CRecordset::forwardOnly)) {
					for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
						CODBCFieldInfo info;

						rst.GetODBCFieldInfo (i, info);
						CString strName = info.m_strName;
						int nIndex = pTaskField->AddString (strName);

						if (!strName.CompareNoCase (strTaskField))
							pTaskField->SetCurSel (nIndex);

						nIndex = pKeyField->AddString (strName);
						pKeyField->SetItemData (nIndex, info.m_nSQLType);

						if (!strName.CompareNoCase (strKeyField))
							pKeyField->SetCurSel (nIndex);

						if (IsGojo ()) {
							nIndex = pStartField->AddString (strName);
							pStartField->SetItemData (nIndex, info.m_nSQLType);

							if (!strName.CompareNoCase (strStartField))
								pStartField->SetCurSel (nIndex);

							nIndex = pEndField->AddString (strName);
							pEndField->SetItemData (nIndex, info.m_nSQLType);

							if (!strName.CompareNoCase (strEndField))
								pEndField->SetCurSel (nIndex);

							nIndex = pOffset->AddString (strName);
							pOffset->SetItemData (nIndex, info.m_nSQLType);

							if (!strName.CompareNoCase (strOffset))
								pOffset->SetCurSel (nIndex);
						}
					}

					rst.Close ();

					if (pTaskField->GetCurSel () == CB_ERR) 
						pTaskField->SetCurSel (0);

					if (pKeyField->GetCurSel () == CB_ERR) 
						pKeyField->SetCurSel (0);

					if (IsGojo ()) {
						if (pStartField->GetCurSel () == CB_ERR) 
							pStartField->SetCurSel (0);

						if (pEndField->GetCurSel () == CB_ERR) 
							pEndField->SetCurSel (0);

						if (pOffset->GetCurSel () == CB_ERR) 
							pOffset->SetCurSel (0);
					}

					UpdateKeyField ();
				}
			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	UpdateUI ();
}

void CDatabaseStartDlg::OnSelchangeTable() 
{
	CString strDSN, strTable;	
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);

	ASSERT (pTable);

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_DSN)) 
		if (p->GetLBTextLen (p->GetCurSel ()) > 0)
			p->GetLBText (p->GetCurSel (), strDSN);

	if (pTable->GetLBTextLen (pTable->GetCurSel ()) > 0)
		pTable->GetLBText (pTable->GetCurSel (), strTable);

	InitFields (strDSN, strTable);
}

void CDatabaseStartDlg::OnOK()
{
	if (!UpdateData ())
		return;

	CComboBox * pDSN = (CComboBox *)GetDlgItem (CB_DSN);
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	CComboBox * pTaskField = (CComboBox *)GetDlgItem (CB_TASKFIELD);
	CComboBox * pKeyField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CComboBox * pStartField = (CComboBox *)GetDlgItem (CB_START);
	CComboBox * pEndField = (CComboBox *)GetDlgItem (CB_END);
	CComboBox * pOffset = (CComboBox *)GetDlgItem (CB_OFFSET);

	ASSERT (pDSN);
	ASSERT (pTable);
	ASSERT (pTaskField);
	ASSERT (pKeyField);
	ASSERT (pStartField);
	ASSERT (pEndField);
	ASSERT (pOffset);

	if (pDSN->GetLBTextLen (pDSN->GetCurSel ()) > 0)
		pDSN->GetLBText (pDSN->GetCurSel (), m_strDSN);
	
	if (m_strDSN.CompareNoCase (m_strDisabled) == 0) {
		this->CDatabaseStartParams::operator = (CDatabaseStartParams ());
		FoxjetCommon::CEliteDlg::OnOK ();
		return;
	}
	
	int nIndex = pTable->GetCurSel ();

	if (nIndex == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOTABLESELECTED));
		return;
	}

	pTable->GetLBText (nIndex, m_strTable);

	nIndex = pTaskField->GetCurSel ();

	if (nIndex == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOFIELDSELECTED));
		return;
	}

	pTaskField->GetLBText (nIndex, m_strTaskField);

	nIndex = pKeyField->GetCurSel ();

	if (nIndex != CB_ERR) 
		pKeyField->GetLBText (nIndex, m_strKeyField);

	UpdateKeyField ();

	nIndex = pStartField->GetCurSel ();
	if (nIndex != CB_ERR) 
		pStartField->GetLBText (nIndex, m_strStartField);

	nIndex = pEndField->GetCurSel ();
	if (nIndex != CB_ERR) 
		pEndField->GetLBText (nIndex, m_strEndField);

	nIndex = pOffset->GetCurSel ();
	if (nIndex != CB_ERR) 
		pOffset->GetLBText (nIndex, m_strOffset);

	FoxjetCommon::CEliteDlg::OnOK ();
}

void CDatabaseStartDlg::UpdateKeyField() 
{
	CComboBox * pKeyField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CString strKeyField, strKeyValue;

	ASSERT (pKeyField);

//	GetDlgItemText (TXT_KEYVALUE, strKeyValue);

//	if (!strKeyValue.GetLength ()) 
//		return;

	int nIndex = pKeyField->GetCurSel ();
	
	if (nIndex != CB_ERR) {
		pKeyField->GetLBText (nIndex, strKeyField);
		m_nSQLType = pKeyField->GetItemData (nIndex);
	}
}

BOOL CDatabaseStartDlg::OnInitDialog() 
{
	if (m_bFreeCache)
		COdbcDatabase::FreeDsnCache ();

	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_DSN)) {
		CStringArray v;

		COdbcDatabase::GetDsnEntries (v);
		p->AddString (m_strDisabled);

		for (int i = 0; i < v.GetSize (); i++) 
			if (p->FindString (-1, v [i]) == CB_ERR)
				p->AddString (v [i]);

		int nIndex = p->FindString (-1, m_strDSN);

		if (nIndex == CB_ERR)
			nIndex = p->FindString (-1, m_strDisabled);

		p->SetCurSel (nIndex);
	}

	if (m_strDSN.GetLength ()) {
		COdbcCache db = COdbcDatabase::Find (m_strDSN);

		db.GetDB ().CopyTables (m_vTables);
	}

	InitTables (m_strDSN, m_strTable, m_strKeyField, m_strTaskField, m_strStartField, m_strEndField, m_strOffset);

	if (m_bReadOnly)
		if (CWnd * p = GetDlgItem (IDOK))
			p->EnableWindow (false);

	if (!FoxjetDatabase::IsGojo ()) {
		UINT n [] = 
		{
			CB_START,
			CB_END,
			CB_OFFSET,
			LBL_START,
			LBL_END,
			LBL_OFFSET,
		};

		for (int i = 0; i < ARRAYSIZE (n); i++)
			if (CWnd * p = GetDlgItem (n [i]))
				p->ShowWindow (SW_HIDE);

	}

	return TRUE;
}

void CDatabaseStartDlg::Load(FoxjetDatabase::COdbcDatabase & db)
{
	SETTINGSSTRUCT s;
	CStringArray v;

	GetSettingsRecord (db, 0, _T ("sw0858"), s);
	FromString (s.m_strData, v);

	{
		int i = 0;
	
		m_strDSN		= GetParam (v, i++);
		m_strTable		= GetParam (v, i++);
		m_strKeyField	= GetParam (v, i++);
		m_strTaskField	= GetParam (v, i++);
		m_nSQLType		= _ttoi (GetParam (v, i++));
		m_strStartField	= GetParam (v, i++);
		m_strEndField	= GetParam (v, i++);
		m_strOffset		= GetParam (v, i++);
	}
}

void CDatabaseStartDlg::Save(FoxjetDatabase::COdbcDatabase & db)
{
	SETTINGSSTRUCT s;
	CStringArray v;
	DWORD dwResult = 0;

	v.Add (m_strDSN);
	v.Add (m_strTable);
	v.Add (m_strKeyField);
	v.Add (m_strTaskField);
	v.Add (ToString (m_nSQLType));
	v.Add (m_strStartField);
	v.Add (m_strEndField);
	v.Add (m_strOffset);

	s.m_strKey	= _T ("sw0858");
	s.m_lLineID = 0;
	s.m_strData = ToString (v);

	if (!UpdateSettingsRecord (db, s))
		VERIFY (AddSettingsRecord (db, s));

	LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST,
		WM_DBSTARTCONFIGCHANGED, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 10000, &dwResult);
}

void CDatabaseStartDlg::UpdateUI ()
{
	bool bEnable = GetCurSelDSN ().CompareNoCase (m_strDisabled) != 0;
	UINT nID [] = 
	{
		CB_TABLE,
		BTN_FILTER,
		CB_KEYFIELD,
		CB_KEYFIELD,
		CB_TASKFIELD,
	};

	for (int i = 0; i < ARRAYSIZE (nID); i++) 
		if (CWnd * p = GetDlgItem (nID [i]))
			p->EnableWindow (bEnable);
}

void CDatabaseStartDlg::OnFilter ()
{
	CTableTypeDlg dlg (this);

	dlg.m_strKey = _T ("Software\\Foxjet\\Control\\Database");

	if (dlg.DoModal () == IDOK) 
		InitTables (GetCurSelDSN (), GetCurSelTable (), GetCurSelKeyField (), GetCurSelTaskField ());
}

CString CDatabaseStartDlg::GetCurSelDSN ()
{
	CString str;

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_DSN)) 
		if (p->GetLBTextLen (p->GetCurSel ()) > 0)
			p->GetLBText (p->GetCurSel (), str);

	return str;
}

CString CDatabaseStartDlg::GetCurSelTable ()
{
	CString str;

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_TABLE)) 
		if (p->GetLBTextLen (p->GetCurSel ()) > 0)
			p->GetLBText (p->GetCurSel (), str);

	return str;
}

CString CDatabaseStartDlg::GetCurSelKeyField ()
{
	CString str;

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_KEYFIELD)) 
		if (p->GetLBTextLen (p->GetCurSel ()) > 0)
			p->GetLBText (p->GetCurSel (), str);

	return str;
}

CString CDatabaseStartDlg::GetCurSelTaskField ()
{
	CString str;

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_TASKFIELD)) 
		if (p->GetLBTextLen (p->GetCurSel ()) > 0)
			p->GetLBText (p->GetCurSel (), str);

	return str;
}
