// CmdLineDlg.cpp : implementation file
//

#include "stdafx.h"
#include "utils.h"
#include "Resource.h"
#include "CmdLineDlg.h"
#include "Database.h"
#include "SystemDlg.h"
#include "Parse.h"
#include "Utils.h"
#include "Debug.h"
#include "Registry.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCmdLineDlg property page

IMPLEMENT_DYNCREATE(CCmdLineDlg, CPropertyPage)

CCmdLineDlg::CCmdLineDlg() 
:	m_hbrDlg (NULL),
	CPropertyPage(IDD_CMDLINE_MATRIX)
{
	m_psp.dwFlags |= PSH_USECALLBACK;
	m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;

	//{{AFX_DATA_INIT(CCmdLineDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CCmdLineDlg::~CCmdLineDlg()
{
	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}
}

void CCmdLineDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCmdLineDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCmdLineDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CCmdLineDlg)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(BTN_DIAGNOSTICS, OnDiagnostics)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCmdLineDlg message handlers

BOOL CCmdLineDlg::OnInitDialog() 
{
	CListBox * pLB = (CListBox *)GetDlgItem (CB_ARGS);
	const CString strPass = _T ("/PASS=");

	ASSERT (pLB);

	CPropertyPage::OnInitDialog();
	
	for (int i = 0; i < m_v.GetSize (); i++) {
		CString str = m_v [i];

		str.MakeUpper ();

		if (str.Find (strPass) != -1) {
			str = strPass + CString ('*', str.GetLength () - strPass.GetLength ());
			pLB->AddString (str);
		}
		else
			pLB->AddString (m_v [i]);
	}
		
	if (CWnd * p = GetDlgItem (BTN_DIAGNOSTICS))
		p->ShowWindow (::GetFileAttributes (FoxjetDatabase::GetHomeDir () + _T ("\\Diagnostics.exe")) != -1 ? SW_SHOW : SW_HIDE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

HBRUSH CCmdLineDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = ::GetSysColor (COLOR_BTNFACE);//FoxjetUtils::GetButtonColor (_T ("background"));
	
	if (FoxjetDatabase::IsControl (false)) 
		rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	if (!m_hbrDlg)	
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}

void CCmdLineDlg::OnDiagnostics ()
{
	using namespace FoxjetDatabase;

	CStringArray vTmp;
	CString strCmdLine = _T ("\"") + FoxjetDatabase::GetHomeDir () + _T ("\\Diagnostics.exe\" /runas_elevated /no_notepad ") + ::GetCommandLine ();
	CString strHide [] = 
	{
		_T ("/enum="),
		_T ("/delete="),
	};

	for (int i = 0; i < ARRAYSIZE (strHide); i++) 
		strCmdLine.Replace (strHide [i] + _T ("\"") + FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), strHide [i]) + _T ("\""), _T (""));

	BeginWaitCursor ();
	TRACEF (strCmdLine);
	Tokenize (FoxjetCommon::execute (strCmdLine), vTmp, '\n');
	EndWaitCursor ();

	if (vTmp.GetSize ()) {
		CString strFile = vTmp [0];

		strFile.Trim ();

		{
			CString str = Encrypted::GetInkCodes ();
		
			if (str.GetLength ()) {
				if (FILE * fp = _tfopen (strFile, _T ("a"))) {
					fwrite (w2a (str), str.GetLength (), 1, fp);
					fclose (fp);
				}
			}
		}

		strCmdLine = _T ("notepad.exe \"") + strFile + _T ("\"");
		STARTUPINFO si;     
		PROCESS_INFORMATION pi; 
		TCHAR szCmdLine [0xFF];

		_tcsncpy (szCmdLine, strCmdLine, 0xFF);
		memset (&pi, 0, sizeof(pi));
		memset (&si, 0, sizeof(si));
		si.cb = sizeof(si);     
		si.dwFlags = STARTF_USESHOWWINDOW;     
		si.wShowWindow = SW_SHOW;     

		TRACEF (szCmdLine);
		::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
	}

	/*
	CString strCmdLine = _T ("\"") + FoxjetDatabase::GetHomeDir () + _T ("\\Diagnostics.exe\" ") + ::GetCommandLine ();
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [0xFF];

	_tcsncpy (szCmdLine, strCmdLine, 0xFF);
	memset (&pi, 0, sizeof(pi));
	memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
	*/
}