#pragma once

#include "resource.h"
#include "maskededit_src\OXMaskedEdit.h"
#include "OdbcDatabase.h"

// CInkCodePage dialog

class CInkCodePage : public CPropertyPage
{
	DECLARE_DYNAMIC(CInkCodePage)

public:
	CInkCodePage(FoxjetDatabase::COdbcDatabase & db);
	virtual ~CInkCodePage();

// Dialog Data
	enum { IDD = IDD_INK_CODE_ENABLE };

	BOOL m_bEnabled;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

protected:
	void Init ();

	virtual BOOL OnInitDialog();

	afx_msg void OnBnClickedEnable();
	afx_msg void OnBnClickedDisable();

	FoxjetUtils::CInkCodeEdit m_txt;
	FoxjetDatabase::COdbcDatabase & m_db;
};
