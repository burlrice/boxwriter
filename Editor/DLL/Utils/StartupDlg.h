#if !defined(AFX_STARTUPDLG_H__93D89748_9892_4E9E_8050_1AF5C18E7F2B__INCLUDED_)
#define AFX_STARTUPDLG_H__93D89748_9892_4E9E_8050_1AF5C18E7F2B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StartupDlg.h : header file
//

#include "OdbcDatabase.h"

/////////////////////////////////////////////////////////////////////////////
// CStartupDlg dialog

namespace FoxjetUtils
{
	class UTIL_API CStartupDlg : public CPropertyPage
	{
		//DECLARE_DYNCREATE(CStartupDlg)

	// Construction
	public:
		CStartupDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection);
		~CStartupDlg();

		const CString m_strRegSection;
		FoxjetDatabase::COdbcDatabase & m_db;

		static bool IsAutoStartEnabled (FoxjetDatabase::COdbcDatabase & db);
		static bool IsUserDataEnabled (FoxjetDatabase::COdbcDatabase & db);
		static bool IsSerialDataEnabled (FoxjetDatabase::COdbcDatabase & db);

	// Dialog Data
		//{{AFX_DATA(CStartupDlg)
		BOOL	m_bAutoStart;
		//}}AFX_DATA
		BOOL m_bUserData;
		BOOL m_bSerialData;


	// Overrides
		// ClassWizard generate virtual function overrides
		//{{AFX_VIRTUAL(CStartupDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		HBRUSH m_hbrDlg;

		// Generated message map functions
		//{{AFX_MSG(CStartupDlg)
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG
		DECLARE_MESSAGE_MAP()

	};
}

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STARTUPDLG_H__93D89748_9892_4E9E_8050_1AF5C18E7F2B__INCLUDED_)
