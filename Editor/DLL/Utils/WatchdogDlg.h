#if !defined(AFX_WATCHDOGDLG_H__AC4BA65D_87FA_484F_BA59_5AD75F0E9742__INCLUDED_)
#define AFX_WATCHDOGDLG_H__AC4BA65D_87FA_484F_BA59_5AD75F0E9742__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WatchdogDlg.h : header file
//

#include "OdbcDatabase.h"
#include "DbTypeDefs.h"

/////////////////////////////////////////////////////////////////////////////
// CWatchdogDlg dialog

namespace FoxjetUtils
{
	class UTIL_API CWatchdogDlg : public CPropertyPage
	{
		//DECLARE_DYNCREATE(CWatchdogDlg)

	// Construction
	public:
		CWatchdogDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection);
		~CWatchdogDlg();

		const CString m_strRegSection;
		FoxjetDatabase::COdbcDatabase & m_db;

	// Dialog Data
		//{{AFX_DATA(CWatchdogDlg)
			// NOTE - ClassWizard will add data members here.
			//    DO NOT EDIT what you see in these blocks of generated code !
		//}}AFX_DATA

		FoxjetDatabase::MOTHERBOARDTYPE m_motherboard;
		ULONG m_lCardTimeout;
		ULONG m_lWDT;
		bool m_bWDT;

	// Overrides
		// ClassWizard generate virtual function overrides
		//{{AFX_VIRTUAL(CWatchdogDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual void OnOK();
		void InitWDT ();
		void InitCard ();

		// Generated message map functions
		//{{AFX_MSG(CWatchdogDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnWatchdogtimeout();
	afx_msg void OnHeadtimeout();
	afx_msg void OnMotherboard();
	afx_msg void OnEnabled();
	//}}AFX_MSG
		DECLARE_MESSAGE_MAP()

	};

}; //namespace FoxjetUtils

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WATCHDOGDLG_H__AC4BA65D_87FA_484F_BA59_5AD75F0E9742__INCLUDED_)
