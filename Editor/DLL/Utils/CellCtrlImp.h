// CellCtrlImp.h: interface for the CCellCtrlImp class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CELLCTRLIMP_H__9BD9D7B7_81D3_47B0_B779_EBF232B713EA__INCLUDED_)
#define AFX_CELLCTRLIMP_H__9BD9D7B7_81D3_47B0_B779_EBF232B713EA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ListCtrlImp.h"
#include "OdbcRecordset.h"

namespace ItiLibrary 
{
	namespace ListCtrlImp
	{
		class UTIL_API CRecordItem : public ListCtrlImp::CItem
		{
		public:
			CRecordItem (FoxjetDatabase::COdbcRecordset & rst, int m_nRow);
			virtual ~CRecordItem ();

			virtual CString GetDispText (int nColumn) const;
			int GetRow () const;
			int Find (const CString & str) const;

		protected:
			CStringArray	m_vData;
			int				m_nRow;
		};

		typedef enum 
		{ 
			CELLCTRL_NOCHANGECOLSEL = 0x0001
		} CELLCTRL_FLAGS;  // SW0830

		class UTIL_API CCellCtrlImp : public CListCtrlImp  
		{
		public:
			CCellCtrlImp();
			virtual ~CCellCtrlImp();

			virtual void Create (UINT nListCtrlID, const CString & strDSN, const CString & strTable, CWnd * pParent,
				const CString & strRegSection, const CString & strField = _T (""), int nRow = 0,
				DWORD dwFlags = 0, int nMaxRows = -1);

			struct
			{
				int m_nRow;
				int m_nColumn;
			} m_selection;

		protected:
			virtual void Destroy ();

			static LRESULT CALLBACK WindowProc (HWND hWnd, UINT nMsg, WPARAM wParam, LPARAM lParam);

			void OnClick (LPNMITEMACTIVATE lpnma);
			void OnItemChanged (LPNMLISTVIEW lpnmlv);
			void DrawItem (LPDRAWITEMSTRUCT lpDrawItemStruct);
			void Fetch ();

			DWORD m_dwFlags;
			int m_nMaxRows;
			int m_nRow;
			FoxjetDatabase::COdbcCache * m_prst;
			FoxjetDatabase::COdbcCache * m_pdb;
			bool m_bEOF;
			CArray <CColumn, CColumn> m_vCols;
			CString m_strField;

		private:
			CCellCtrlImp (const CListCtrlImp & rhs);					// no impl
			CCellCtrlImp & operator = (const CListCtrlImp & rhs);		// no impl
		};
	}; //namespace ListCtrlImp
}; //namespace ItiLibrary 

#endif // !defined(AFX_CELLCTRLIMP_H__9BD9D7B7_81D3_47B0_B779_EBF232B713EA__INCLUDED_)
