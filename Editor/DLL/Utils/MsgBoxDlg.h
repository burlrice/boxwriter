#if !defined(AFX_MSGBOXDLG_H__193189F1_C3D7_4EA7_A08A_98389CA1840F__INCLUDED_)
#define AFX_MSGBOXDLG_H__193189F1_C3D7_4EA7_A08A_98389CA1840F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MsgBoxDlg.h : header file
//

#include "Resource.h"
#include "RoundButtons.h"
#include "Utils.h"

using namespace FoxjetUtils;

/////////////////////////////////////////////////////////////////////////////
// CMsgBoxDlg dialog

class CMsgBoxDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CMsgBoxDlg(const CString & strMessage, const CString & strTitle, UINT nType, CWnd* pParent = NULL);   // standard constructor
	virtual ~CMsgBoxDlg ();

// Dialog Data
	//{{AFX_DATA(CMsgBoxDlg)
	enum { IDD = IDD_MSGBOX };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMsgBoxDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	virtual BOOL OnInitDialog();

// Implementation
protected:

	CRoundButtons m_buttons;
	CString m_strMessage;
	CString m_strMessageFormatted;
	CString m_strTitle;
	UINT m_nType;
	HBRUSH m_hbrDlg;
	CBitmap m_bmp;
	CFont m_fnt;

	// Generated message map functions
	//{{AFX_MSG(CMsgBoxDlg)
	afx_msg void OnClipboard();
	afx_msg void OnAbort();
	afx_msg void OnIgnore();
	afx_msg void OnNo();
	afx_msg void OnRetry();
	afx_msg void OnYes();
	//}}AFX_MSG

	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	MSG m_msgKeyDown;

	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MSGBOXDLG_H__193189F1_C3D7_4EA7_A08A_98389CA1840F__INCLUDED_)
