// EnTabCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "EnTabCtrl.h"
#include "RoundButtons.h"
#include "Database.h"
#include "SystemDlg.h"
#include "Debug.h"
#include "Parse.h"
#include "TemplExt.h"
#include "Utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC(CEnPropertySheet, CPropertySheet)

BEGIN_MESSAGE_MAP(CEnPropertySheet, CPropertySheet)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED (IDHELP, OnHelp)
END_MESSAGE_MAP()

CEnPropertySheet::CEnPropertySheet () 
:	m_hbrDlg (NULL),
	CPropertySheet ()
{ 
	m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_USECALLBACK;
	m_psh.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetProc;
}

CEnPropertySheet::CEnPropertySheet (UINT nIDCaption, CWnd *pParentWnd, UINT iSelectPage)
:	m_hbrDlg (NULL),
	CPropertySheet (nIDCaption, pParentWnd, iSelectPage)
{ 
	m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_USECALLBACK;
	m_psh.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetProc;
}

CEnPropertySheet::CEnPropertySheet (LPCTSTR pszCaption, CWnd *pParentWnd, UINT iSelectPage)
:	m_hbrDlg (NULL),
	CPropertySheet (pszCaption, pParentWnd, iSelectPage)
{ 
	m_psh.dwFlags |= PSH_NOAPPLYNOW | PSH_USECALLBACK;
	m_psh.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetProc;
}

CEnPropertySheet::~CEnPropertySheet () 
{ 
	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}
}

void CEnPropertySheet::OnHelp ()
{
	CEliteDlg::OnHelp (this);
}

BOOL CEnPropertySheet::OnInitDialog () 
{
	CPropertySheet::OnInitDialog();

	if (CTabCtrl * p = CPropertySheet::GetTabControl ()) {
		m_tab.SubclassDlgItem (p->GetDlgCtrlID (), this);

		//if (FoxjetDatabase::IsControl (false)) 
		{
			CRect rc (0, 0, 0, 0);

			p->GetItemRect (0, rc);

			if (rc.Width () > 0 && rc.Height () > 0) {
				p->SetItemSize (CSize (rc.Width (), (int)(rc.Height () * 1.5)));
				SetActivePage (GetActivePage ());
			}
		}	
	}

	if (IsControl (false)) 
		ModifyStyle (WS_SYSMENU, 0);

	UpdateData (FALSE);

	return TRUE;
}

HBRUSH CEnPropertySheet::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertySheet::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = ::GetSysColor (COLOR_BTNFACE);//FoxjetUtils::GetButtonColor (_T ("background"));
	
	if (FoxjetDatabase::IsControl (false)) 
		rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	if (!m_hbrDlg)	
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}

#include <AFXPRIV.H>

class CSystemDlgTemplate : public CDialogTemplate
{
public:
    void Attach (LPDLGTEMPLATE pTemplate)
	{
		m_hTemplate      = ::GlobalHandle( pTemplate );
		m_dwTemplateSize = GetTemplateSize( pTemplate );
		m_bSystemFont    = false;
	}
};

static CString GetFont () { return _T ("Microsoft Sans Serif"); }
static int GetFontSize () { return 12; }

UINT CALLBACK CEnPropertySheet::PropSheetPageProc (HWND hwnd, UINT uMsg, LPPROPSHEETPAGE ppsp)
{
	extern UINT CALLBACK AfxPropPageCallback(HWND, UINT message, LPPROPSHEETPAGE pPropPage);

	int nRes = AfxPropPageCallback(hwnd, uMsg, ppsp);

    switch (uMsg)
    {
	case PSPCB_CREATE:
        if (LPDLGTEMPLATE pTemplate = (LPDLGTEMPLATE)ppsp->pResource) {
			CSystemDlgTemplate dlgTemplate;

			dlgTemplate.Attach (pTemplate);
			dlgTemplate.SetFont (::GetFont (), GetFontSize ());
			dlgTemplate.Detach ();
		}
		break;
	}

	return nRes;
}

int CALLBACK CEnPropertySheet::PropSheetProc (HWND hwndDlg, UINT uMsg, LPARAM lParam)
{
    switch (uMsg)
    {
        case PSCB_PRECREATE:
        {
            LPDLGTEMPLATE pTemplate = (LPDLGTEMPLATE)lParam;
            CSystemDlgTemplate dlgTemplate;

            dlgTemplate.Attach (pTemplate);
			dlgTemplate.SetFont (::GetFont (), GetFontSize ());
            dlgTemplate.Detach ();
            break;
        }
        default:
            break;
    }

    return 0;
}

static ULONG GetBmpID (ULONG lID)
{
	switch (lID) {
	case IDOK:		return IDB_OK;
	case IDCANCEL:	return IDB_CANCEL;
	case IDHELP:	return IDB_INFORMATION;
	}

	return 0;
}

void CEnPropertySheet::DoDataExchange (CDataExchange* pDX)
{
	CPropertySheet::DoDataExchange (pDX);

	{
		bool bHelp = false;
		HWND hwnd = ::GetWindow (m_hWnd, GW_CHILD);
		CRect rcWin (0, 0, 0, 0);
		bool bResize = true;

		::GetWindowRect (m_hWnd, &rcWin);

		do
		{
			LONG lID = ::GetWindowLong (hwnd, GWL_ID);
			switch (lID) {

			case IDHELP:
				if (CWnd * p = GetDlgItem (IDOK)) {
					CRect rc (0, 0, 0, 0);

					p->GetWindowRect (rc);
					ScreenToClient (rc);
					rc -= CPoint (rc.Width () + 10, 0);
					::SetWindowPos (hwnd, NULL, rc.left, rc.top, rc.Width (), rc.Height (), SWP_NOZORDER);
					::ShowWindow (hwnd, SW_SHOW);
					bHelp = true;
				}
				break;
			}

			hwnd = ::GetWindow (hwnd, GW_HWNDNEXT);
		}
		while (!bHelp && hwnd != NULL);

		ASSERT (bHelp); 
	}

#ifdef _DEBUG
	if (IsControl (true)) 
#else
	if (IsControl (false)) 
#endif
	{
		HWND hwnd = ::GetWindow (m_hWnd, GW_CHILD);
		CRect rcWin (0, 0, 0, 0);
		bool bResize = true;

		::GetWindowRect (m_hWnd, &rcWin);

		do
		{
			LONG lID = ::GetWindowLong (hwnd, GWL_ID);
			CRect rcButton (0, 0, 0, 0);

			::GetWindowRect (hwnd, &rcButton);

			int x = 0;

			switch (lID) {
			case IDOK:
			case IDCANCEL:
			case IDABORT:
			case IDRETRY:
			case IDIGNORE:
			case IDYES:
			case IDNO:
			case IDCLOSE:
			case IDHELP:
				#ifdef _DEBUG
				TCHAR sz [128] = { 0 };

				::GetWindowText (hwnd, sz, ARRAYSIZE (sz) - 1);
				TRACEF (ToString (lID) + _T (": ") + CString (sz) + _T (" [") + ToString (rcButton.Width ()) + _T (", ") + ToString (rcButton.Height ()) + _T ("]"));
				#endif

				m_buttons.DoDataExchange (pDX, lID, GetBmpID (lID));

				if (bResize) {
					int cy = rcButton.Width () - rcButton.Height ();

					bResize = false;

					::SetWindowPos (m_hWnd, NULL, 0, 0, rcWin.Width (), rcWin.Height () + cy, SWP_NOZORDER | SWP_NOMOVE);

					if (CWnd * pThis = CWnd::FromHandle (m_hWnd)) {
						if (CWnd * pParent = CWnd::FromHandle (::GetParent (m_hWnd))) {
							pThis->CenterWindow (pParent);
							::GetWindowRect (m_hWnd, &rcWin);
							::GetWindowRect (hwnd, &rcButton);
						}
					}
				}

				break;
			}


			ScreenToClient (rcButton);

			switch (lID) {
			case IDOK:
				x = rcWin.Width () - (rcButton.Width () + 10);
				break;
			case IDCANCEL:
				x = 10;
				break;
			case IDHELP:
				x = rcButton.left;
				break;
			}

			if (x)
				::MoveWindow (hwnd, x, rcButton.top, rcButton.Width (), rcButton.Width (), TRUE);

			hwnd = ::GetWindow (hwnd, GW_HWNDNEXT);
		}
		while (hwnd != NULL);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CEnTabCtrl

DWORD CEnTabCtrl::s_dwCustomLook = 0;

enum { PADDING = 3, EDGE = 20};

//////////////////////////////////////////////////////////////////////////////
// helpers
COLORREF Darker(COLORREF crBase, float fFactor)
{
	ASSERT (fFactor < 1.0f && fFactor > 0.0f);

	fFactor = min(fFactor, 1.0f);
	fFactor = max(fFactor, 0.0f);

	BYTE bRed, bBlue, bGreen;
	BYTE bRedShadow, bBlueShadow, bGreenShadow;

	bRed = GetRValue(crBase);
	bBlue = GetBValue(crBase);
	bGreen = GetGValue(crBase);

	bRedShadow = (BYTE)(bRed * fFactor);
	bBlueShadow = (BYTE)(bBlue * fFactor);
	bGreenShadow = (BYTE)(bGreen * fFactor);

	return RGB(bRedShadow, bGreenShadow, bBlueShadow);
}

COLORREF Lighter(COLORREF crBase, float fFactor) 
{
	ASSERT (fFactor > 1.0f);

	fFactor = max(fFactor, 1.0f);

	BYTE bRed, bBlue, bGreen;
	BYTE bRedHilite, bBlueHilite, bGreenHilite;

	bRed = GetRValue(crBase);
	bBlue = GetBValue(crBase);
	bGreen = GetGValue(crBase);

	bRedHilite = (BYTE)min((int)(bRed * fFactor), 255);
	bBlueHilite = (BYTE)min((int)(bBlue * fFactor), 255);
	bGreenHilite = (BYTE)min((int)(bGreen * fFactor), 255);

	return RGB(bRedHilite, bGreenHilite, bBlueHilite);
}

CSize FormatText(CString& sText, CDC* pDC, int nWidth)
{
	CRect rect(0, 0, nWidth, 20);
	UINT uFlags = DT_CALCRECT | DT_SINGLELINE | DT_MODIFYSTRING | DT_END_ELLIPSIS;

	::DrawText(pDC->GetSafeHdc(), sText.GetBuffer(sText.GetLength() + 4), -1, rect, uFlags);
	sText.ReleaseBuffer();

	return pDC->GetTextExtent(sText);
}

////////////////////////////////////////////////////////////////////////////////////////

CEnTabCtrl::CEnTabCtrl()
{
}

CEnTabCtrl::~CEnTabCtrl()
{
}

/////////////////////////////////////////////////////////////////////////////
// CEnTabCtrl message handlers

void CEnTabCtrl::DrawItem(LPDRAWITEMSTRUCT lpdis)
{
	TC_ITEM     tci;
	CDC* pDC = CDC::FromHandle(lpdis->hDC);
	HIMAGELIST hilTabs = (HIMAGELIST)TabCtrl_GetImageList(GetSafeHwnd());

	BOOL bSelected = (lpdis->itemID == (UINT)GetCurSel());
	BOOL bColor = (s_dwCustomLook & ETC_COLOR);

	CRect rItem(lpdis->rcItem);

	if (bSelected)
		rItem.bottom -= 1;
	else
		rItem.bottom += 2;

	// tab
	// blend from back color to COLOR_3DFACE if 16 bit mode or better
	COLORREF crFrom = GetTabColor(bSelected);
	
	if (s_dwCustomLook & ETC_GRADIENT && pDC->GetDeviceCaps(BITSPIXEL) >= 16)
	{
		COLORREF crTo = bSelected ? (IsControl (false) ? FoxjetUtils::GetButtonColor (_T ("background")) : ::GetSysColor(COLOR_3DFACE)) : Darker(!bColor || m_crBack == -1 ? (IsControl (false) ? FoxjetUtils::GetButtonColor (_T ("background")) : ::GetSysColor(COLOR_3DFACE)) : m_crBack, 0.7f);

		int nROrg = GetRValue(crFrom);
		int nGOrg = GetGValue(crFrom);
		int nBOrg = GetBValue(crFrom);
		int nRDiff = GetRValue(crTo) - nROrg;
		int nGDiff = GetGValue(crTo) - nGOrg;
		int nBDiff = GetBValue(crTo) - nBOrg;
		
		int nHeight = rItem.Height();

		for (int nLine = 0; nLine < nHeight; nLine += 2)
		{
			int nRed = nROrg + (nLine * nRDiff) / nHeight;
			int nGreen = nGOrg + (nLine * nGDiff) / nHeight;
			int nBlue = nBOrg + (nLine * nBDiff) / nHeight;
			
			pDC->FillSolidRect(CRect(rItem.left, rItem.top + nLine, rItem.right, rItem.top + nLine + 2), 
								RGB(nRed, nGreen, nBlue));
		}
	}
	else // simple solid fill
		pDC->FillSolidRect(rItem, crFrom);

	// text & icon
	rItem.left += PADDING;
	rItem.top += PADDING + (bSelected ? 1 : 0);

	pDC->SetBkMode(TRANSPARENT);

	CString sTemp;
	tci.mask        = TCIF_TEXT | TCIF_IMAGE;
	tci.pszText     = sTemp.GetBuffer(100);
	tci.cchTextMax  = 99;
	GetItem(lpdis->itemID, &tci);
	sTemp.ReleaseBuffer();

	// icon
	if (hilTabs)
	{
		ImageList_Draw(hilTabs, tci.iImage, *pDC, rItem.left, rItem.top, ILD_TRANSPARENT);
		rItem.left += 16 + PADDING;
	}

	// text
	rItem.right -= PADDING;
	FormatText(sTemp, pDC, rItem.Width());

	pDC->SetTextColor(GetTabTextColor(bSelected));
	pDC->DrawText(sTemp, rItem, DT_NOPREFIX | DT_CENTER);
}

void CEnTabCtrl::DrawItemBorder(LPDRAWITEMSTRUCT lpdis)
{
	ASSERT (s_dwCustomLook & ETC_FLAT);

	BOOL bSelected = (lpdis->itemID == (UINT)GetCurSel());
	BOOL bBackTabs = (s_dwCustomLook & ETC_BACKTABS);

	CRect rItem(lpdis->rcItem);
	CDC* pDC = CDC::FromHandle(lpdis->hDC); 

	COLORREF crTab = GetTabColor(bSelected);
	COLORREF crHighlight = Lighter(crTab, 1.5f);
	COLORREF crShadow = Darker(crTab, 0.75f);

	if (bSelected || bBackTabs)
	{
		rItem.bottom += bSelected ? -1 : 1;

		// edges
		pDC->FillSolidRect(CRect(rItem.left, rItem.top, rItem.left + 1, rItem.bottom), crHighlight);
		pDC->FillSolidRect(CRect(rItem.left, rItem.top, rItem.right, rItem.top + 1), crHighlight);
		pDC->FillSolidRect(CRect(rItem.right - 1, rItem.top, rItem.right, rItem.bottom), crShadow);
	}
	else // draw simple dividers
	{
		pDC->FillSolidRect(CRect(rItem.left - 1, rItem.top, rItem.left, rItem.bottom), crShadow);
		pDC->FillSolidRect(CRect(rItem.right - 1, rItem.top, rItem.right, rItem.bottom), crShadow);
	}
}

void CEnTabCtrl::DrawMainBorder(LPDRAWITEMSTRUCT lpdis)
{
	CRect rBorder(lpdis->rcItem);
	CDC* pDC = CDC::FromHandle(lpdis->hDC); 

	COLORREF crTab = GetTabColor();
	COLORREF crHighlight = Lighter(crTab, 1.5f);
	COLORREF crShadow = Darker(crTab, 0.75f);

	pDC->Draw3dRect(rBorder, crHighlight, crShadow);
}

COLORREF CEnTabCtrl::GetTabColor(BOOL bSelected)
{
	BOOL bColor = (s_dwCustomLook & ETC_COLOR);
	BOOL bHiliteSel = (s_dwCustomLook & ETC_SELECTION);
	BOOL bBackTabs = (s_dwCustomLook & ETC_BACKTABS);
	BOOL bFlat = (s_dwCustomLook & ETC_FLAT);

	if (bSelected && bHiliteSel)
	{
		if (bColor)
			return Lighter((m_crBack == -1) ? (IsControl (false) ? FoxjetUtils::GetButtonColor (_T ("background")) :  ::GetSysColor(COLOR_3DFACE)) : m_crBack, 1.4f);
		else
			return Lighter((IsControl (false) ? FoxjetUtils::GetButtonColor (_T ("background")) :  ::GetSysColor(COLOR_3DFACE)), 1.4f);
	}
	else if (!bSelected)
	{
		if (bBackTabs || !bFlat)
		{
			if (bColor)
				return Darker((m_crBack == -1) ? (IsControl (false) ? FoxjetUtils::GetButtonColor (_T ("background")) : ::GetSysColor(COLOR_3DFACE)) : m_crBack, 0.9f);
			else
				return Darker((IsControl (false) ? FoxjetUtils::GetButtonColor (_T ("background")) : ::GetSysColor(COLOR_3DFACE)), 0.9f);
		}
		else
			return (m_crBack == -1) ? (IsControl (false) ? FoxjetUtils::GetButtonColor (_T ("background")) : ::GetSysColor(COLOR_3DFACE)) : m_crBack;
	}

	// else
	return IsControl (false) ? FoxjetUtils::GetButtonColor (_T ("background")) : ::GetSysColor(COLOR_3DFACE);
}

COLORREF CEnTabCtrl::GetTabTextColor(BOOL bSelected)
{
	BOOL bColor = (s_dwCustomLook & ETC_COLOR);
	BOOL bFlat = (s_dwCustomLook & ETC_FLAT);

	if (bSelected)
	{
		return IsControl (false) ? FoxjetUtils::GetButtonColor (_T ("button font")) : ::GetSysColor(COLOR_WINDOWTEXT);
	}
	else
	{
		if (bColor || bFlat)
			return Darker((m_crBack == -1) ? ::GetSysColor(COLOR_3DFACE) : m_crBack, 0.5f);
		else
			return Darker(IsControl (false) ? FoxjetUtils::GetButtonColor (_T ("background")) : ::GetSysColor(COLOR_3DFACE), 0.5f);
	}

	// else
	return Darker(IsControl (false) ? FoxjetUtils::GetButtonColor (_T ("background")) : ::GetSysColor(COLOR_3DFACE), 0.5f);
}

void CEnTabCtrl::EnableCustomLook(BOOL bEnable, DWORD dwStyle)
{
	if (!bEnable)
		dwStyle = 0;

	s_dwCustomLook = dwStyle;
}

void CEnTabCtrl::PreSubclassWindow() 
{
	CBaseTabCtrl::PreSubclassWindow();

	if (s_dwCustomLook)
		ModifyStyle(0, TCS_OWNERDRAWFIXED);
}


