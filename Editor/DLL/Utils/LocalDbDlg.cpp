// LocalDbDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "utils.h"
#include "LocalDbDlg.h"
#include "EnTabCtrl.h"
#include "Database.h"
#include "Parse.h"
#include "TemplExt.h"
#include "WinMsg.h"
#include "Debug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ID_UPDATEUI 100

using namespace FoxjetUtils;
using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////
// CLocalDbDlg property page

IMPLEMENT_DYNCREATE(CLocalDbDlg, CPropertyPage)

CLocalDbDlg::CLocalDbDlg() 
:	m_nTimer (0),
	CPropertyPage(IDD_LOCALDB)
{
	//{{AFX_DATA_INIT(CLocalDbDlg)
	m_bEnabled = FALSE;
	//}}AFX_DATA_INIT
	m_psp.dwFlags |= PSH_USECALLBACK;
	m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CLocalDbDlg::~CLocalDbDlg()
{
}

void CLocalDbDlg::DoDataExchange(CDataExchange* pDX)
{
	CString strTime = _T ("12:00 AM");

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLocalDbDlg)
	DDX_Check(pDX, CHK_LOCALDB_ENABLED, m_bEnabled);
	//}}AFX_DATA_MAP

	if (!pDX->m_bSaveAndValidate) {
		if (m_vTrigger.GetSize ()) 
			strTime =  m_vTrigger [0].Format (_T ("%I:%M %p"));
	}

	m_buttons.DoDataExchange (pDX, BTN_BACKUP);
	m_buttons.DoDataExchange (pDX, BTN_BACKUP_ALL);
	m_buttons.DoDataExchange (pDX, BTN_STOP);
	m_buttons.DoDataExchange (pDX, BTN_VIEW);
	DDX_Text (pDX, TXT_TIME, strTime);

	if (pDX->m_bSaveAndValidate) {
		bool bFail = true;

		m_vTrigger.RemoveAll ();

		try {
			COleDateTime tmParse;

			if (tmParse.ParseDateTime (strTime, VAR_TIMEVALUEONLY)) {
				CTime t (2014, 1, 1, tmParse.GetHour (), tmParse.GetMinute (), 0);

				TRACEF (strTime);
				TRACEF (t.Format (_T ("%c [%H:%M %p]")));

				m_vTrigger.Add (t);
				bFail = false;
			}
		}
		catch (COleException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		if (bFail) {
			MsgBox (LoadString (IDS_INVALIDTIME));
			pDX->PrepareCtrl (TXT_TIME);
			pDX->Fail ();
		}

		#ifdef _DEBUG
		{
			for (int i = 0; i < m_vTrigger.GetSize (); i++)
				TRACEF (m_vTrigger [i].Format (_T ("%H:%M %p")));
		}
		#endif
	}
}


BEGIN_MESSAGE_MAP(CLocalDbDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CLocalDbDlg)
	ON_BN_CLICKED(BTN_BACKUP, OnBackup)
	ON_BN_CLICKED(CHK_LOCALDB_ENABLED, OnLocaldbEnabled)
	ON_BN_CLICKED(BTN_STOP, OnStop)
	ON_BN_CLICKED(BTN_BACKUP_ALL, OnBackupAll)
	ON_BN_CLICKED(BTN_VIEW, OnView)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLocalDbDlg message handlers

bool CLocalDbDlg::Load (FoxjetDatabase::COdbcDatabase & db)
{
	SETTINGSSTRUCT s;

	m_vTrigger.RemoveAll ();

	if (GetSettingsRecord (db, GetFirstLineID (db, GetPrinterID (db)), _T ("LocalDB"), s)) {
		CStringArray v;
		int nParam = 0;

		FoxjetDatabase::FromString (s.m_strData, v);

		m_bEnabled	= _ttoi (GetParam (v, nParam++)) ? true : false;

		for (int i = nParam; i < v.GetSize (); i++) {
			CString str = UnformatString (v [i]);

			try {
				COleDateTime t;

				if (t.ParseDateTime (str)) {
					SYSTEMTIME sysTime;

					t.GetAsSystemTime (sysTime);
					TRACEF (str);
					TRACEF (CTime (sysTime).Format (_T ("%c [%H:%M %p]")));

					m_vTrigger.Add (CTime (sysTime));
				}
			}
			catch (COleException * e)		{ HANDLEEXCEPTION (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
		}

		if (!m_vTrigger.GetSize ()) 
			m_vTrigger.Add (CTime (2014, 1, 1, 23, 59, 59));

		#ifdef _DEBUG
		{
			for (int i = 0; i < m_vTrigger.GetSize (); i++)
				TRACEF (m_vTrigger [i].Format (_T ("%c [%H:%M %p]")));
		}
		#endif

		return true;
	}

	return false;
}

bool CLocalDbDlg::Save (FoxjetDatabase::COdbcDatabase & db)
{
	SETTINGSSTRUCT s;
	CStringArray v;

	v.Add (ToString ((int)m_bEnabled));

	for (int i = 0; i < m_vTrigger.GetSize (); i++)
		v.Add (FormatString (m_vTrigger [i].Format (_T ("%c"))));

	s.m_strKey	= _T ("LocalDB");
	s.m_lLineID = GetFirstLineID (db, GetPrinterID (db));
	s.m_strData = FoxjetDatabase::ToString (v);
	TRACEF (s.m_strData);

	if (!UpdateSettingsRecord (db, s))
		return AddSettingsRecord (db, s);

	return true;
}

BOOL CLocalDbDlg::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	OnTimer (ID_UPDATEUI);
	m_nTimer = SetTimer (ID_UPDATEUI, 1000, NULL);
	OnLocaldbEnabled ();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLocalDbDlg::OnLocaldbEnabled() 
{
	if (CButton * pEnabled = (CButton *)GetDlgItem (CHK_LOCALDB_ENABLED)) {
		if (!pEnabled->GetCheck ()) {
			UINT n [] = 
			{
				TXT_TIME,
				BTN_STOP,
				BTN_BACKUP,
				BTN_BACKUP_ALL,
			};

			for (int i = 0; i < ARRAYSIZE (n); i++) {
				if (CWnd * p = GetDlgItem (n [i]))
					p->EnableWindow (pEnabled->GetCheck () == 1);
			}
		}
		else
			if (CWnd * p = GetDlgItem (TXT_TIME))
				p->EnableWindow (pEnabled->GetCheck () == 1);
	}
}

void CLocalDbDlg::OnBackup() 
{
	DWORD dwResult = 0;

	LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_LOCAL_DB_BACKUP, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);	
}

void CLocalDbDlg::OnStop() 
{
	DWORD dwResult = 0;

	LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_LOCAL_DB_BACKUP, 1, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);	
}

void CLocalDbDlg::OnBackupAll() 
{
	DWORD dwResult = 0;

	LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_LOCAL_DB_BACKUP, -1, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);	
}

static CMap <ULONG, ULONG, bool, bool> vActive;

void CLocalDbDlg::SetActive (bool bActive, ULONG lThreadID)
{
	TRACEF (_T ("SetActive: ") + ToString (lThreadID) + _T (": ") + ToString (bActive));

	if (!bActive)
		::vActive.RemoveKey (lThreadID);
	else
		::vActive.SetAt (lThreadID, true);
}

bool CLocalDbDlg::GetActive ()
{
	return ::vActive.GetCount () ? true : false;
}

void CLocalDbDlg::OnTimer(UINT nIDEvent) 
{
	switch (nIDEvent) {
	case ID_UPDATEUI:
		if (CButton * pEnabled = (CButton *)GetDlgItem (CHK_LOCALDB_ENABLED)) {
			if (pEnabled->GetCheck ()) {
				bool bActive = GetActive ();

				if (CWnd * p = GetDlgItem (BTN_STOP)) 
					p->EnableWindow (bActive);
				if (CWnd * p = GetDlgItem (BTN_BACKUP)) 
					p->EnableWindow (!bActive);
				if (CWnd * p = GetDlgItem (BTN_BACKUP_ALL)) 
					p->EnableWindow (!bActive);
			}
		}
		return;
	}

	CPropertyPage::OnTimer(nIDEvent);
}

void CLocalDbDlg::OnClose() 
{
	if (m_nTimer)
		KillTimer (m_nTimer);
	
	CPropertyPage::OnClose();
}

void CLocalDbDlg::OnView ()
{
	CString strCmdLine = _T ("notepad \"") + GetHomeDir () + _T ("\\networked_backup.txt\"");
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [0xFF];

	TRACEF (szCmdLine);
	_tcsncpy (szCmdLine, strCmdLine, 0xFF);
	memset (&pi, 0, sizeof(pi));
	memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
}

