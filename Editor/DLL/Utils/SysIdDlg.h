#if !defined(AFX_SYSIDDLG_H__3A1E2E48_2F44_4ABA_A878_D3ABF0BD8B3A__INCLUDED_)
#define AFX_SYSIDDLG_H__3A1E2E48_2F44_4ABA_A878_D3ABF0BD8B3A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SysIdDlg.h : header file
//

#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// CSysIdDlg dialog

class CSysIdDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CSysIdDlg)

// Construction
public:
	CSysIdDlg();
	~CSysIdDlg();

// Dialog Data
	//{{AFX_DATA(CSysIdDlg)
	CString	m_strSysID;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSysIdDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

	HBRUSH m_hbrDlg;

	// Generated message map functions
	//{{AFX_MSG(CSysIdDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SYSIDDLG_H__3A1E2E48_2F44_4ABA_A878_D3ABF0BD8B3A__INCLUDED_)
