﻿// Utils.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"

#include <math.h>
#include <shlwapi.h>
#include <Windows.h>
//#include "WINABLE.H"
#include <string>
#include <Winspool.h>
#include <stack>
#include <iostream>
#include <sstream>

#include "Utils.h"
#include "Debug.h"
#include "DllVer.h"
#include "CopyArray.h"
#include "FileExt.h"
#include "Resource.h"
#include "TemplExt.h"
#include "AppVer.h"
#include "ximacfg.h"
#include "ximage.h"
#include "Setupapi.h"
#include "DevGuids.h"
#include "Database.h"
#include "WinMsg.h"
#include "..\..\..\Control\Host.h"
#include "Parse.h"
#include "MsgBox.h"
#include "RoundButtons.h"
#include "Resource.h"
#include "Registry.h"
#include "OdbcRecordset.h"
#include "OdbcTable.h"
#include "ExcelFormat.h"


using namespace FoxjetUtils;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;

//#define __CXIMAGE__

#pragma data_seg(".HKT")
HWND ghwndTarget = NULL;
HINSTANCE hInstance = NULL;
HHOOK ghCallWndProc = NULL;
HHOOK ghHook = NULL;
ULONG glWinMsg = 0;
bool gbSwap = false;
#pragma data_seg()

#ifdef _DEBUG
CString GetStyleString (HWND hwnd, LONG lStyle, LONG lExStyle);
CString GetButtonState (HWND hWndCtrl);
CString GetDlgCode (HWND hWndCtrl);
#endif


static void SetAutoButtonText (HWND hWnd);
static CString GetSynonym (const CString & str, const CString & strLangExt);

static AFX_EXTENSION_MODULE UtilsDLL = { NULL, NULL };



////////////////////////////////////////////////////////////////////////////////////////////////////
IMPLEMENT_DYNCREATE(CEliteDlg, CDialog)

BEGIN_MESSAGE_MAP(CEliteDlg, CDialog)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED (IDHELP, OnHelp)
END_MESSAGE_MAP()

CEliteDlg::CEliteDlg (LPCTSTR lpszTemplateName, CWnd* pParentWnd)
:	CDialog (lpszTemplateName, pParentWnd)
{
	Init ();
}

CEliteDlg::CEliteDlg (UINT nIDTemplate, CWnd* pParentWnd)
:	CDialog (nIDTemplate, pParentWnd)
{
	Init ();
}

CEliteDlg::CEliteDlg ()
:	CDialog ()
{
	Init ();
}

void CEliteDlg::Init ()
{
	m_rgbBack = FoxjetDatabase::IsControl (false) ? FoxjetUtils::GetButtonColor (_T ("background")) : ::GetSysColor (COLOR_BTNFACE);
	m_brushBackground.CreateSolidBrush (m_rgbBack);
}

HBRUSH CEliteDlg::OnCtlColor (CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	
	switch (nCtlColor) {
	case CTLCOLOR_DLG:
	case CTLCOLOR_STATIC:
		pDC->SetBkColor (m_rgbBack);
		hbr = (HBRUSH)m_brushBackground;
		break;
	}

	return hbr;
}

static CString GetMostRecentFile (const CStringArray & v)
{
	if (v.GetSize () > 0) {
		CTime tmFirst = GetFileDate (v [0]);
		int nFirst = 0;

		for (int i = 1; i < v.GetSize (); i++) {
			CTime tm = GetFileDate (v [i]);

			if (tm > tmFirst) {
				tmFirst = tm;
				nFirst = i;
			}
		}

		return v [nFirst];
	}

	return _T ("");
}

void CEliteDlg::OnHelp ()
{
	CEliteDlg::OnHelp (this);
}

#include <odbcinst.h>

#define DO_SQL_CALL_BOOL(f) DoSqlCall ((f) ? true : false, _T (__FILE__), __LINE__)

bool DoSqlCall (bool bResult, LPCTSTR lpszFile, ULONG lLine)
{
	if (!bResult) {
		WORD w = 1, wLen = 0;
		DWORD dwError = 0;
		TCHAR sz[256];

		::SQLInstallerError(w, &dwError, sz, ARRAYSIZE(sz), &wLen);

		#ifdef _DEBUG
		CDebug::Trace (sz, true, lpszFile, lLine);
		#endif 
	}

	return bResult;
}

typedef struct 
{
	std::vector <std::wstring> m_v;
} ROW_STRUCT;

void InsertAscending (CArray <ROW_STRUCT, ROW_STRUCT &> & v, ROW_STRUCT value) 
{
	for (int i = 0; i < v.GetSize (); i++) {
		if (value.m_v.size () > 0 && v [i].m_v [0].size () > 0) {
			if (value.m_v [0] <= v [i].m_v [0]) {
				v.InsertAt (i, value);
				return;
			}
		}
	}

	v.Add (value);
}


static void PopulateXls (FoxjetDatabase::COdbcDatabase & db)
{
	CStringArray vFiles, vTitle;

	//GetFiles (GetHomeDir () + _T ("\\Language\\") + GetLangDir (_T ("en")), _T ("*.dll"), vFiles);
	GetFiles (GetHomeDir (), _T ("*.exe"), vFiles);
	GetFiles (GetHomeDir (), _T ("*.dll"), vFiles);


	for (int i = vFiles.GetSize () - 1; i >= 0; i--) {
		CString str = vFiles [i];

		str.Replace (GetHomeDir () + _T ("\\"), _T (""));

		if (str.Find ('\\') != -1)
			vFiles.RemoveAt (i);
	}

	#ifdef _DEBUG
	for (int i = 0; i < vFiles.GetSize (); i++) 
		TRACEF (vFiles [i]);
	#endif //_DEBUG

	for (int i = 0; i < vFiles.GetSize (); i++) {
		CString strModule = vFiles [i];
		FoxjetCommon::CCopyArray <UINT, UINT> v = FoxjetDatabase::GetDialogResourceIDs (strModule);
		
		TRACEF (strModule);

		if (HMODULE hModule = ::LoadLibrary (strModule)) {
			for (int j = 0; j < v.GetSize (); j++) {
				UINT nDialogID = v [j];

				if (HRSRC hrsrc = ::FindResourceEx (hModule, RT_DIALOG, MAKEINTRESOURCE (nDialogID), MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL))) {
					if (HGLOBAL hglob = ::LoadResource (hModule, hrsrc)) {
						if (LPCDLGTEMPLATE lpDialogTemplate = (LPCDLGTEMPLATE)::LockResource(hglob)) {
							if (HWND hwnd = ::CreateDialogIndirect (::AfxGetInstanceHandle (), lpDialogTemplate, ::AfxGetMainWnd ()->m_hWnd, HiddenDialogProc)) {
								/*
								#ifdef _DEBUG
								{
									TCHAR sz [512] = { 0 };

									::GetWindowText (hwnd, sz, ARRAYSIZE (sz) - 1);
									TRACEF (ToString ((int)nDialogID) + _T (": ") + CString (sz));
								}
								#endif //_DEBUG
								*/
								int i = 0;
								TCHAR sz [512] = { 0 };

								::GetWindowText (hwnd, sz, ARRAYSIZE (sz) - 1);

								for (HWND h = ::GetTopWindow (hwnd); h != NULL; h = ::GetNextWindow (h, GW_HWNDNEXT), i++) {
									ULONG lID = ::GetWindowLong (h, GWL_ID);

									/*
									#ifdef _DEBUG
									{
										TCHAR sz [512] = { 0 };

										::GetWindowText (h, sz, ARRAYSIZE (sz) - 1);
										TRACEF (_T ("\t\t") + ToString ((int)lID) + _T (": ") + CString (sz));
									}
									#endif //_DEBUG
									*/

									if (i == 0) {
										if (!::IsWindowVisible (h)) {
											TCHAR sz [32] = { 0 };
											ULONG lID = ::GetWindowLong (h, GWL_ID);

											::GetClassName (h, sz, ARRAYSIZE (sz) - 1);
	
											if (lID == IDC_STATIC && !_tcscmp (sz, _T ("Static")))
												::GetWindowText (h, sz, ARRAYSIZE (sz) - 1);
										}
									}

									if (lID == IDHELP) {
										InsertAscending (vTitle, sz);
										break;
									}
								}

								::DestroyWindow (hwnd); 
							}

							::UnlockResource (hglob);
						}

						::FreeResource(hglob);
					}
				}
			}

			::FreeLibrary (hModule);
		}
	}

	try {
		if (db.GetTableCount () > 0) {
			const CString strTable = db.GetTable (0)->GetName ();
			CString strName;
			int nFields = 0;

			{
				COdbcRecordset rst (db);
				CString strSQL;

				strSQL.Format (_T ("SELECT * FROM [%s]"), strTable);
				rst.Open (strSQL);
				strName = rst.GetFieldName ((short)0);
				strName.Replace (_T ("[") + strTable + _T ("]."), _T (""));
				nFields = rst.GetODBCFieldCount ();
			}

			for (int i = vTitle.GetSize () - 1; i >= 0; i--) {
				COdbcRecordset rst (db);
				CString strSQL;

				strSQL.Format (_T ("SELECT * FROM [%s] WHERE [Dialog title]='%s'"), strTable, vTitle [i]);
				rst.Open (strSQL);

				if (!rst.IsEOF ()) 
					vTitle.RemoveAt (i);
			}

			for (int i = 0; i < vTitle.GetSize (); i++) {
				CString strSQL;

				strSQL.Format (_T ("INSERT INTO [%s] (%s) VALUES ('%s');"), strTable, strName, vTitle [i]);
				TRACEF (strSQL);
				db.ExecuteSQL (strSQL);
			}

			if (nFields > 0) {
				COdbcRecordset rst (db);
				CString strSQL;
				CArray <ROW_STRUCT, ROW_STRUCT &> v;
				std::vector <std::wstring> vNames, vValues;
				ROW_STRUCT rowHeader;

				strSQL.Format (_T ("SELECT * FROM [%s]"), strTable);
				rst.Open (strSQL);

				for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
					CString str = rst.GetFieldName ((short)i);

					str.Replace (_T ("[") + strTable + _T ("]."), _T (""));
					str.Replace (_T ("["), _T (""));
					str.Replace (_T ("]"), _T (""));
					rowHeader.m_v.push_back (std::wstring ((CString)str));
				}

				while (!rst.IsEOF ()) {
					ROW_STRUCT r;

					for (int i = 0; i < rst.GetODBCFieldCount (); i++) 
						r.m_v.push_back (std::wstring ((CString)rst.GetFieldValue ((short)i)));

					InsertAscending (v, r);
					rst.MoveNext ();
				}

				v.InsertAt (0, rowHeader);

				//ODBC;DSN=MarksmanELITE_Manual;DBQ=c:\foxjet\Language\ManualIndices.xls;DriverId=790;MaxBufferSize=2048;PageTimeout=5;
				const CString strDSN = Extract (db.GetConnect (), _T ("DSN="), _T (";"));
				const CString strFile = Extract (db.GetConnect (), _T ("DBQ="), _T (";"));
				db.Close ();

				//if (::DeleteFile (strFile)) 
				{
					using namespace ExcelFormat;
					using namespace YExcel;

					BasicExcel xls;
					XLSFormatManager fmt_mgr(xls);
					CellFormat fmtText (fmt_mgr);

					fmtText.set_format_string(XLS_FORMAT_TEXT);

					xls.New(1);
					BasicExcelWorksheet* sheet = xls.GetWorksheet(0);

					for (int y = 0; y < v.GetSize (); y++) {
						for (int x = 0; x < v [y].m_v.size (); x++) {
							CString str = v [y].m_v [x].c_str ();

							sheet->Cell(y, x)->Set (str);
							sheet->Cell(y, x)->SetFormat (fmtText);
						}
					}

					VERIFY (xls.SaveAs (strFile));
				}

				db.Open (strDSN, 0, 0, _T ("ODBC;"), 0);
			}
		}
	}
	catch (CMemoryException * e) { HANDLEEXCEPTION (e); }
	catch (CDBException * e) { HANDLEEXCEPTION (e); }
}


static bool CreateExcelDsn (const CString & strFilepath, const CString & strDSN)
{
	bool bResult = false;

	int nIndex = strFilepath.ReverseFind ('\\');

	FoxjetDatabase::DeleteDSN (strDSN, SQL_XLS_DRIVER);

	if (nIndex != -1) {
		const CString strFileTitle = strFilepath.Mid (nIndex + 1);
		const CString strDir = strFilepath.Mid (0, nIndex);
		LPCTSTR lpszDBDriver = SQL_XLS_DRIVER;

		if (::GetFileAttributes(strFilepath) == -1) {
			HMODULE hModule = ::GetModuleHandle (_T ("Utils.dll"));

			if (HRSRC hRSRC = ::FindResource(hModule, MAKEINTRESOURCE(IDR_MANUAL_XLS), _T("EXCEL"))) {
				DWORD dwSize = ::SizeofResource (hModule, hRSRC);

				if (HGLOBAL hGlobal = ::LoadResource(hModule, hRSRC)) {
					if (LPVOID lpData = ::LockResource(hGlobal)) {
						if (FILE * fp = _tfopen(strFilepath, _T("wb"))) {
							if (fwrite(lpData, dwSize, 1, fp) == 1)
								TRACEF(_T("wrote: ") + strFilepath);

							fclose(fp);

							{
								TCHAR sz[512];
								TCHAR szAttr[512];
								LPCTSTR lpszFormat =
									_T("DSN=%s~ ")					// dsn
									_T("DBQ=%s\\%s~ ");				// file

								_stprintf(sz, lpszFormat,
									strDSN,
									strDir, strFileTitle,
									strDir);

								int nLen = _tcsclen(sz);

								_tcscpy(szAttr, (sz));
								TRACEF(szAttr);

								for (int i = 0; i < nLen; i++)
									if (szAttr[i] == '~')
										szAttr[i] = '\0';

								DO_SQL_CALL_BOOL(::SQLConfigDataSource(NULL, ODBC_REMOVE_SYS_DSN, lpszDBDriver, _T("DSN=") + strDSN));

								if (DO_SQL_CALL_BOOL(SQLConfigDataSource(NULL, ODBC_ADD_SYS_DSN, lpszDBDriver, szAttr))) {
									try
									{
										COdbcDatabase db (_T (__FILE__), __LINE__);
										
										db.Open (strDSN, 0, 0, _T ("ODBC;"), 0);
										bResult = true;
										PopulateXls (db);
									}
									catch (CMemoryException * e) { HANDLEEXCEPTION (e); }
									catch (CDBException * e) { HANDLEEXCEPTION (e); }
								}
							}
						}
					}
				}
			}
			else
				TRACEF(FormatMessage(::GetLastError()));
		}
	}

	return bResult;
}

static CTime GetFileTime (const CString & strPath)
{
	CTime result = CTime (2000, 1, 1, 0, 0, 0);
	HANDLE hFile = ::CreateFile (strPath, 
		GENERIC_WRITE, 
		FILE_SHARE_READ | FILE_SHARE_WRITE, 
		NULL, 
		OPEN_EXISTING, 
		FILE_ATTRIBUTE_NORMAL, 
		0);

	if (hFile) {
		FILETIME ft;
		SYSTEMTIME sys;

		::GetFileTime (hFile, NULL, NULL, &ft);

		::GetSystemTime (&sys);
		::SystemTimeToFileTime (&sys, &ft);
		result = CTime (ft);

		::CloseHandle (hFile);
	}

	return result;
}

static int GetMostRecent (const CStringArray & v)
{
	CTime tmMax = CTime (2000, 1, 1, 0, 0, 0);
	int nIndex = -1;

	for (int i = 0; i < v.GetSize (); i++) {
		CTime tm = GetFileTime (v [i]);

		if (tm > tmMax) {
			nIndex = i;
			tmMax = tm;
		}
	}

	return nIndex;
}

static CString GetSynonym (const CString & str, const CString & strLangExt)
{
	const CString strDSN = _T ("MarksmanELITE_Manual");
	CString strResult = str;
	CString strFind = RemovePunctuation (str);
	CString strXLS = _T ("ManualIndices.xls");

	if (str.Find (_T ("Head properties ")) != -1)
		strFind = RemovePunctuation (_T ("Head configuration"));

	{
		CStringArray v;

		GetFiles (GetHomeDir () + _T ("\\Language\\") + GetLangDir (_T ("en")), _T ("*.pdf"), v);
		int nIndex = GetMostRecent (v);

		if (nIndex != -1) {
			CString s = v [nIndex];

			s.Replace (_T ("\\"), _T ("/"));
			std::vector <std::wstring> vFile = explode (s, (TCHAR)'/');

			if (vFile.size ()) {
				strXLS = vFile [vFile.size () - 1].c_str ();
				strXLS.Replace (_T (".pdf"), _T (".xls"));
			}
		}
	}

	{
		const CString strManualXls = GetHomeDir () + _T ("\\Language\\") + strXLS;
		bool bCreate = true;

		if (::GetFileAttributes (strManualXls) != -1) {
			try
			{
				using namespace FoxjetDatabase;

				COdbcDatabase db (_T (__FILE__), __LINE__);
				CString strSQL;

				if (db.Open (strDSN, FALSE, TRUE, _T ("ODBC;"), FALSE)) {
					for (int i = 0; bCreate && (i < db.GetTableCount ()); i++) {
						const COdbcTable * pTable = db.GetTable (i);
						COdbcRecordset rst (&db);
						CString strSQL = _T ("SELECT * FROM [") + pTable->GetName () + _T ("]");
						
						TRACEF (strSQL);
						rst.Open (strSQL, CRecordset::snapshot);

						for (short j = 0; bCreate && (j < rst.GetODBCFieldCount ()); j++) {
							CODBCFieldInfo info;

							rst.GetODBCFieldInfo (j, info);
							TRACEF (info.m_strName);

							if (!info.m_strName.CompareNoCase (_T ("Dialog title"))) 
								bCreate = false;
						}
					}
				}
			}
			catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }
			catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
		}

		if (bCreate) {
			CStringArray v;

			::DeleteFile (strManualXls);
			GetFiles (GetHomeDir () + _T ("\\Language"), _T ("*.*"), v);

			for (int i = 0; i < v.GetSize (); i++) {
				if (::GetFileAttributes (v [i]) & FILE_ATTRIBUTE_DIRECTORY) {
					CStringArray vPDF;

					TRACEF (v [i]);
					GetFiles (v [i], _T ("*.*"), vPDF);

					for (int j = 0; j < vPDF.GetSize (); j++) {
						CString str = vPDF [j];

						str.MakeLower ();

						if (str.Replace (_T (".pdf"), _T (".txt")))
							::DeleteFile (str);
					}
				}
			}

			VERIFY (CreateExcelDsn (strManualXls, strDSN));
		}
	}


	try
	{
		using namespace FoxjetDatabase;

		COdbcDatabase db (_T (__FILE__), __LINE__);
		CString strSQL;

		db.Open(strDSN, FALSE, TRUE, _T ("ODBC;"), FALSE);
		COdbcRecordset rst (db);

		if (db.GetTableCount () > 0) {
			strSQL.Format (_T ("SELECT * FROM [%s] WHERE [Dialog title]='%s'"), db.GetTable (0)->GetName (), strFind);
			TRACEF (strSQL);
			rst.Open (strSQL);

			if (!rst.IsEOF ()) {
				CString strXls = (CString)rst.GetFieldValue (FoxjetDatabase::GetLangDir (GetLangExt ()));

				if (strXls.GetLength ()) {
					TRACEF (strXls);
					strResult = strXls;
				}
				else {
					if (!strLangExt.CompareNoCase (_T ("en"))) {
						strXls = (CString)rst.GetFieldValue (GetLangDir (_T ("en")));
						TRACEF (strXls);
						strResult = strXls;
					}
				}
			}
		}
	}
	catch (CMemoryException * e) { HANDLEEXCEPTION (e); }
	catch (CDBException * e) { HANDLEEXCEPTION (e); }

	{
		int n = 0;
		int nscanf = _stscanf (strResult, _T ("[%d]"), &n);

		if (nscanf != 1)
			strResult = RemovePunctuation (strResult);
		else 
			strResult.Format (_T ("[%d]"), n - 1);
	}
	
	TRACEF (str + _T (" --> ") + strResult); 

	return strResult;

	/*
	struct 
	{
		LPCTSTR m_lpszSoftware;
		LPCTSTR m_lpszManual;
	}
	static const map [] =
	{
		{ _T ("User element data"),				_T ("[28]"),	},
		{ _T ("Set counts"),					_T ("[28]"),	},
		{ _T ("Database properties"),			_T ("[105]"),	},
		{ _T ("Head configuration"),			_T ("[30]"),	},
		{ _T ("Line configuration"),			_T ("[32]"),	},
		{ _T ("Line properties"),				_T ("[32]"),	},
		{ _T ("System configuration"),			_T ("[33]"),	},
		{ _T ("Configure Users"),				_T ("[34]"),	},
		{ _T ("User Info"),						_T ("[34]"),	},
		{ _T ("Import"),						_T ("[57]"),	},
		{ _T ("Export"),						_T ("[58]"),	},
		{ _T ("Print report"),					_T ("[37]"),	},
		{ _T ("Scan report"),					_T ("[37]"),	},
		{ _T ("Head configuration - delays"),	_T ("[38]"),	},
	};

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		CString strTmp = RemoveWhitespace (RemovePunctuation (map [i].m_lpszSoftware));

		if (!strTmp.CompareNoCase (strFind)) {
			CString strResult = map [i].m_lpszManual;
			int n = 0;
			int nscanf = _stscanf (strResult, _T ("[%d]"), &n);

			if (nscanf != 1)
				strResult = RemovePunctuation (strResult);

			TRACEF (str + _T (" --> ") + strResult); 

			return strResult;
		}
	}
	*/

	return str;
}

void CEliteDlg::OnHelp (CWnd * pWnd)
{
	CString str, strEXE, strCmdLine, strPDF, strEnglishPDF;
	const CString strLangExt = GetLangExt ().MakeLower ();
	
	pWnd->GetWindowText (str);

	{
		CWnd * p = pWnd->GetTopWindow ();

		if (p) {
			if (!p->IsWindowVisible ()) {
				TCHAR sz [32] = { 0 };
				ULONG lID = ::GetWindowLong (p->m_hWnd, GWL_ID);

				::GetClassName (p->m_hWnd, sz, ARRAYSIZE (sz) - 1);
	
				if (lID == IDC_STATIC && !_tcscmp (sz, _T ("Static")))
					p->GetWindowText (str);
			}
		}
	}

	#ifdef _DEBUG
	{
		/*
		English				Spanish									Portuguese			Dutch				Russian						Hungarian
		Element defaults	Valores predeterminados del elemento	Padrões de elemento	Element defaults	Компоненты по умолчанию		Alapértelmezett elemek		
		*/

		/*	manual											lang db
			Configuración por Defecto de los Elementos		Valores predeterminados del elemento
		*/

		/*
		if (!str.CompareNoCase (_T ("Element defaults"))) {
			str = _T ("Valores predeterminados del elemento");
			(* (CString *)(LPVOID)&strLangExt) = _T ("sp");
			//str = _T ("Alapértelmezett elemek");
			//(* (CString *)(LPVOID)&strLangExt) = _T ("hu");
		}
		*/

		//str = _T ("Acerca de Control...");

		//if (!str.CompareNoCase (_T ("Message name")))
		//	str = _T ("Название сообщения");
	}
	#endif

	{
		const CString strRegKey = _T ("Software\\Foxjet\\Control\\Settings");
		CString strRegistry = FoxjetDatabase::GetProfileString (strRegKey, _T ("PDF reader"), _T (""));

		if (::GetFileAttributes (strRegistry) == -1)
			strRegistry.Empty ();

		if (strRegistry.GetLength ())
			strEXE = strRegistry;
		else {
			const CString strReader = _T ("SumatraPDF.exe");
			CStringArray v;

			GetFiles (GetHomeDir () + _T ("\\SumatraPDF"), strReader, v);

			if (v.GetSize () == 0)
				GetFiles (GetKnownFolderPath (FOLDERID_ProgramFiles) + _T ("\\SumatraPDF"), strReader, v);

			if (v.GetSize () == 0)
				GetFiles (GetKnownFolderPath (FOLDERID_ProgramFilesX86) + _T ("\\SumatraPDF"), strReader, v);

			if (v.GetSize () == 0) {
				MsgBox (LoadString (IDS_READERMISSING) + _T (": ") + strReader, _T (""), MB_ICONERROR);
				return;
			}
			else {
				strEXE = v [0];
				FoxjetDatabase::WriteProfileString (strRegKey, _T ("PDF reader"), strEXE);
			}
		}
	}


	{
		CStringArray v, vEnglish;
		CString strDir = GetHomeDir () + _T ("\\Language\\") + GetLangDir (strLangExt);

		TRACEF (_T ("checking: ") + strDir);
		GetFiles (strDir, _T ("*.pdf"), v);
		GetFiles (GetHomeDir () + _T ("\\Language\\") + GetLangDir (_T ("en")), _T ("*.pdf"), vEnglish);

		if (v.GetSize () == 0 && vEnglish.GetSize () == 0) {
			MsgBox (LoadString (IDS_HELPFILEMISSING), _T (""), MB_ICONERROR);
			return;
		}

		strPDF = GetMostRecentFile (v);
		strEnglishPDF = GetMostRecentFile (vEnglish);
	}


	pWnd->BeginWaitCursor ();

	CCopyArray <PDFSEARCHSTRUCT, PDFSEARCHSTRUCT &> vIndex = FoxjetCommon::PdfSearch (strPDF, str, GetLangExt ());

	#ifdef _DEBUG
	TRACEF (str + _T (" [") + strPDF + _T ("]"));

	for (int i = 0; i < vIndex.GetSize (); i++) {
		TRACEF (ToString (vIndex [i].m_nPage) + _T (" [") + ToString (vIndex [i].m_nCount) + _T (" hits]"));
	}
	#endif

	if (strPDF.IsEmpty () || !vIndex.GetSize ()) {
		CStringArray vLang;

		GetFiles (GetHomeDir () + _T ("\\Language\\") + GetLangDir (strLangExt), _T ("*.dll"), vLang);

		for (int i = 0; !vIndex.GetSize () && i < vLang.GetSize (); i++) {
			CString strDLL = vLang [i];
			CString strEnDLL = strDLL;

			TRACEF (strDLL);
			strEnDLL.MakeLower ();
			strEnDLL.Replace (GetLangDir (strLangExt).MakeLower (), GetLangDir (_T ("en")).MakeLower ());
			strEnDLL.Replace (strLangExt + _T (".dll"), _T ("en.dll"));

			UINT nID = GetIdFromStringResource (str, strDLL, true);

			if (nID) {
				const CString strTranslated = LoadString (nID, strEnDLL);

				#ifdef _DEBUG
				TRACEF (ToString ((int)nID) + _T (": ") + strTranslated);
				TRACEF (ToString ((int)nID) + _T (": ") + LoadString (nID, strDLL));
				#endif

				vIndex = FoxjetCommon::PdfSearch (strEnglishPDF, strTranslated, _T ("en"));
				
				if (vIndex.GetSize () && strPDF.IsEmpty ())
					strPDF = strEnglishPDF;
			}

			if (!vIndex.GetSize ()) {
				DIALOG_SEARCH_STRUCT find = GetIdFromDialogResource (str, strDLL);

				if (find.m_nDialogID != -1) {
					CString strEnglish = GetStringFromDialogResource (find, strEnDLL);

					vIndex = FoxjetCommon::PdfSearch (strEnglishPDF, strEnglish, _T ("en"));

					if (vIndex.GetSize () && strPDF.IsEmpty ()) 
						strPDF = strEnglishPDF;
				}
			}
		}
	}

	TRACEF (str);

	if (vIndex.GetSize ()) {
		strCmdLine.Format (_T ("\"%s\" -page %d -reuse-instance \"%s\""), strEXE, vIndex [0].m_nPage, strPDF);
		TRACEF (strCmdLine);
		int n = 0;
	}
	else {
		strCmdLine.Format (_T ("\"%s\" -page 4 -reuse-instance \"%s\""), strEXE, strPDF);
		TRACEF (strCmdLine);
		ASSERT (0);
	}

	for (int i = 0; i < 2; i++) {
		STARTUPINFO si;     
		PROCESS_INFORMATION pi; 
		TCHAR szCmdLine [MAX_PATH];

		_tcsncpy (szCmdLine, strCmdLine, MAX_PATH);
		memset (&pi, 0, sizeof(pi));
		memset (&si, 0, sizeof(si));
		si.cb = sizeof(si);     
		si.dwFlags = STARTF_USESHOWWINDOW;     
		si.wShowWindow = SW_SHOW;

		TRACEF (szCmdLine);
		BOOL bResult = ::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi); 
		::Sleep (100);
	}

	pWnd->EndWaitCursor ();
}


////////////////////////////////////////////////////////////////////////////////////////////////////

UTIL_API void FoxjetCommon::SetAutoButtonText (HWND hWnd)
{
	HWND hwnd = ::GetWindow (hWnd, GW_CHILD);

	do
	{
		LONG lID = ::GetWindowLong (hwnd, GWL_ID);

		switch (lID) {
		case IDOK:
		case IDCANCEL:
		case IDABORT:
		case IDRETRY:
		case IDIGNORE:
		case IDYES:
		case IDNO:
		case IDCLOSE:
		case IDHELP:
			break;
		default:
			const LRESULT lCode = ::SendMessage (hwnd, WM_GETDLGCODE, 0, 0);
			const LONG lStyle = ::GetWindowLong (hwnd, GWL_STYLE);
			//const LONG lExStyle = ::GetWindowLong (hwnd, GWL_EXSTYLE);

			if (((lCode & DLGC_RADIOBUTTON) && (lStyle & BS_AUTORADIOBUTTON)) ||
				((lCode & DLGC_BUTTON)		&& (lStyle & BS_AUTOCHECKBOX))) 
			{

				if (lStyle & BS_PUSHLIKE)  {
					static const TCHAR szChecked []		= { 0x25cf, 0x00 };
					static const TCHAR szUnchecked []	= { 0x25cc, 0x00 };
					TCHAR sz [128] = { 0 };
					const LRESULT lCheck	= ::SendMessage (hwnd, BM_GETCHECK, 0, 0);
					const LONG lJustify		= (lStyle & BS_CENTER) >> 8;

					::GetWindowText (hwnd, sz, ARRAYSIZE (sz) - 1);
					CString str = sz;

					//str.Replace (_T ("["), _T (""));
					//str.Replace (_T ("]"), _T (""));
					str.Replace (szChecked, _T (""));
					str.Replace (szUnchecked, _T (""));
					str.TrimLeft ();
					str.TrimRight ();

					//TRACEF (str + _T (" ") + GetDlgCode (hwnd) + _T (" ") + GetStyleString (hwnd, lStyle, lExStyle));

					switch (lJustify) {
					default:
					case 0:		// center
					case 1:		// left
						::SetWindowText (hwnd, (lCheck ? szChecked : szUnchecked) + CString (_T (" ")) + str);
						break;
					case 2:		// right
						::SetWindowText (hwnd, str + _T (" ") + (lCheck ? szChecked : szUnchecked));
						break;
					}

					/*
					if (lCheck)
						::SetWindowText (hwnd, _T ("[") + str + _T ("]"));
					else
						::SetWindowText (hwnd, str);
					*/
				}
			}
		}

		hwnd = ::GetWindow (hwnd, GW_HWNDNEXT);
	}
	while (hwnd != NULL);
}


extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UTILS.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UtilsDLL, hInstance)) {
			MESSAGEBOX (_T ("AfxInitExtensionModule failed"));
			return 0;
		}

		// Insert this DLL into the resource chain
		// NOTE: If this Extension DLL is being implicitly linked to by
		//  an MFC Regular DLL (such as an ActiveX Control)
		//  instead of an MFC application, then you will want to
		//  remove this line from DllMain and put it in a separate
		//  function exported from this Extension DLL.  The Regular DLL
		//  that uses this Extension DLL should then explicitly call that
		//  function to initialize this Extension DLL.  Otherwise,
		//  the CDynLinkLibrary object will not be attached to the
		//  Regular DLL's resource chain, and serious problems will
		//  result.

		::hInstance = hInstance;
		new CDynLinkLibrary(UtilsDLL);
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		if (::ghCallWndProc) {
			UnhookWindowsHookEx (::ghCallWndProc);
			::ghCallWndProc = NULL;
		}

		TRACE0("UTILS.DLL Terminating!\n");
		// Terminate the library before destructors are called
		AfxTermExtensionModule(UtilsDLL);
	}
	return 1;   // ok
}

CString FoxjetUtils::LoadString (UINT nID)
{
	return FoxjetDatabase::LoadString (nID, _T ("Utils.dll"));
}

void CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer)
{
	if (lpVer) 
		* lpVer = FoxjetCommon::verApp; // memcpy (lpVer, &FoxjetCommon::verApp, sizeof (FoxjetCommon::VERSIONSTRUCT));
}

HRESULT CALLBACK DllGetVersion (DLLVERSIONINFO * pdvi)
{
	FoxjetCommon::VERSIONSTRUCT ver;

	fj_GetDllVersion (&ver);

	if (pdvi) {
		pdvi->dwMajorVersion	= ver.m_nMajor;
		pdvi->dwMinorVersion	= ver.m_nMinor;
		pdvi->dwBuildNumber		= ver.m_nInternal;
		pdvi->dwPlatformID		= DLLVER_PLATFORM_WINDOWS;
	}

	return NOERROR;
}

UTIL_API void FoxjetUtils::InitInstance ()
{
/*
	CString str = ::GetCommandLine ();

	str.MakeLower ();

	if (str.Find (_T (" /no_checkboxes")) == -1)
		if (!::ghCallWndProc)
			::ghCallWndProc = SetWindowsHookEx (WH_CALLWNDPROCRET, (HOOKPROC)CallWndRetProc, ::hInstance, 0);
*/
}

UTIL_API void FoxjetUtils::ExitInstance ()
{
	if (::ghCallWndProc) {
		UnhookWindowsHookEx (::ghCallWndProc);
		::ghCallWndProc = NULL;
	}
}

UTIL_API FoxjetCommon::CVersion FoxjetCommon::GetElementListVersion ()
{
	const CVersion verNotFound = CVersion (0, 0, 0, 0, 0);
	CVersion ver;

#if defined( __WINPRINTER__ )
	ver = GetDllVersion (_T ("ElementList.dll"));
#elif defined( __IPPRINTER__ )
	ver = GetDllVersion (_T ("IpElementList.dll"));
#else
	#error __WINPRINTER__ or __IPPRINTER__ must be defined 
#endif

	// TODO: this only works if every implementation of ElementList has a different dll name and working dir.
	// it should probably be removed before a major release
	if (ver == verNotFound)
		ver = GetDllVersion (_T ("ElementList.dll"));
	if (ver == verNotFound)
		ver = GetDllVersion (_T ("IpElementList.dll"));

	return ver;
}

////////////////////////////////////////////////////////////////////////////////
UTIL_API void FoxjetCommon::Rotate180 (CDC & dc, const CSize & size) 
{
	Rotate180Plg (dc, size); // this is more efficient

/*
	CxImage img;
	CDC dcMem;
	CBitmap * pbmpSrc = dc.GetCurrentBitmap ();

	ASSERT (pbmpSrc);

	dcMem.CreateCompatibleDC (&dc);
	img.CreateFromHBITMAP (* pbmpSrc);
	img.Rotate180 ();

	HBITMAP hBmp = img.MakeBitmap (dcMem);
	ASSERT (hBmp);

	pbmpSrc->Detach ();
	VERIFY (pbmpSrc->Attach (hBmp));
*/
}

UTIL_API void FoxjetCommon::RotateLeft90 (CDC & dc, CBitmap & bmpSrc, CBitmap & bmpDest) 
{
#ifndef __CXIMAGE__
	RotateLeft90Plg (dc, bmpSrc, bmpDest); 
	return;
#endif 

	CxImage img;

	VERIFY (img.CreateFromHBITMAP (bmpSrc));
	VERIFY (img.RotateLeft ());

	HBITMAP hBmp = img.MakeBitmap (dc);
	ASSERT (hBmp);
	
	VERIFY (bmpDest.Attach (hBmp));
}

UTIL_API void FoxjetCommon::RotateRight90 (CDC & dc, CBitmap & bmpSrc, CBitmap & bmpDest)
{
#ifndef __CXIMAGE__
	RotateRight90Plg (dc, bmpSrc, bmpDest);
	return;
#endif

	CxImage img;

	VERIFY (img.CreateFromHBITMAP (bmpSrc));
	VERIFY (img.RotateRight ());

	HBITMAP hBmp = img.MakeBitmap (dc);
	ASSERT (hBmp);
	
	VERIFY (bmpDest.Attach (hBmp));
}

UTIL_API void FoxjetCommon::RotateLeft90AndFlip (CDC & dc, CBitmap & bmpSrc, CBitmap & bmpDest)
{
#ifndef __CXIMAGE__
	RotateLeft90AndFlipPlg (dc, bmpSrc, bmpDest);
	return;
#endif

	CxImage img;

	VERIFY (img.CreateFromHBITMAP (bmpSrc));
	VERIFY (img.RotateLeft ());
	VERIFY (img.Flip ());

	HBITMAP hBmp = img.MakeBitmap (dc);
	ASSERT (hBmp);
	
	VERIFY (bmpDest.Attach (hBmp));
}
////////////////////////////////////////////////////////////////////////////////


UTIL_API void FoxjetCommon::Rotate180Plg (CDC & dc, const CSize & size)
{
	CDC dcMem;
	CBitmap bmp;
	int x = size.cx - 1, y = size.cy - 1, cx = -size.cx, cy = -size.cy;

	if (!dcMem.CreateCompatibleDC (&dc))
		TRACEF (FORMATMESSAGE (::GetLastError ()));
	
	if (!bmp.CreateCompatibleBitmap (&dc, size.cx, size.cy))
		TRACEF (FORMATMESSAGE (::GetLastError ()));

	CBitmap * pMem = dcMem.SelectObject (&bmp);

	if (!dcMem.StretchBlt (x, y, cx, cy, &dc, 0, 0, size.cx, size.cy, SRCCOPY))
		TRACEF (FORMATMESSAGE (::GetLastError ()));

	if (!dc.BitBlt (0, 0, size.cx, size.cy, &dcMem, 0, 0, SRCCOPY))
		TRACEF (FORMATMESSAGE (::GetLastError ()));

	dcMem.SelectObject (pMem);
}

UTIL_API void FoxjetCommon::RotateLeft90AndFlipPlg (CDC & dc, CBitmap & bmpPrinter, CBitmap & bmpEditor)
{
	CDC dcPrinter, dcEditor;
	DIBSECTION ds;

	::ZeroMemory (&ds, sizeof (ds));
	VERIFY (bmpPrinter.GetObject (sizeof (ds), &ds));
	
	const int cx = ds.dsBm.bmHeight;
	const int cy = ds.dsBm.bmWidth;
	const CPoint ppt [3] = 
	{ 
		CPoint (0,	0), 		//	[0]		[2]
		CPoint (0,	cy),		// 
		CPoint (cx,	0), 		//	[1]		[ ]
	};

	VERIFY (dcEditor.CreateCompatibleDC (&dc));
	VERIFY (dcPrinter.CreateCompatibleDC (&dc));
	VERIFY (bmpEditor.CreateCompatibleBitmap (&dc, cx, cy));

	CBitmap * pEditor = dcEditor.SelectObject (&bmpEditor);
	CBitmap * pPrinter = dcPrinter.SelectObject (&bmpPrinter);

	::BitBlt (dcEditor, 0, 0, cx, cy, NULL, 0, 0, WHITENESS);

	int nSetStretchBltMode = dcPrinter.SetStretchBltMode (HALFTONE);
	
	if (!::PlgBlt (dcEditor, ppt, dcPrinter, 0, 0, cy, cx, NULL, 0, 0))
		TRACEF (FORMATMESSAGE (::GetLastError ()));

	dcPrinter.SetStretchBltMode (nSetStretchBltMode);

	dcEditor.SelectObject (pEditor);
	dcPrinter.SelectObject (pPrinter);
}

UTIL_API void FoxjetCommon::RotateLeft90Plg (CDC & dc, CBitmap & bmpPrinter, CBitmap & bmpEditor)
{
	CDC dcPrinter, dcEditor;
	DIBSECTION ds;

	::ZeroMemory (&ds, sizeof (ds));
	VERIFY (bmpPrinter.GetObject (sizeof (ds), &ds));
	
	const int cx = ds.dsBm.bmHeight;
	const int cy = ds.dsBm.bmWidth;
	const CPoint ppt [3] = 
	{ 
		CPoint (0,	cy),		//	[3]		[0] 
		CPoint (0,  0),			//
		CPoint (cx, cy),		//	[ ]		[2]
	};

	VERIFY (dcEditor.CreateCompatibleDC (&dc));
	VERIFY (dcPrinter.CreateCompatibleDC (&dc));
	VERIFY (bmpEditor.CreateCompatibleBitmap (&dc, cx, cy));

	CBitmap * pEditor = dcEditor.SelectObject (&bmpEditor);
	CBitmap * pPrinter = dcPrinter.SelectObject (&bmpPrinter);

	::BitBlt (dcEditor, 0, 0, cx, cy, NULL, 0, 0, WHITENESS);

	int nSetStretchBltMode = dcPrinter.SetStretchBltMode (HALFTONE);
	
	if (!::PlgBlt (dcEditor, ppt, dcPrinter, 0, 0, cy, cx, NULL, 0, 0))
		TRACEF (FORMATMESSAGE (::GetLastError ()));

	dcPrinter.SetStretchBltMode (nSetStretchBltMode);

	dcEditor.SelectObject (pEditor);
	dcPrinter.SelectObject (pPrinter);
}

UTIL_API void FoxjetCommon::RotateRight90Plg (CDC & dc, CBitmap & bmpEditor, CBitmap & bmpPrinter)
{
	CDC dcPrinter, dcEditor;
	DIBSECTION ds;

	::ZeroMemory (&ds, sizeof (ds));
	VERIFY (bmpEditor.GetObject (sizeof (ds), &ds));
	
	const int cx = ds.dsBm.bmHeight;
	const int cy = ds.dsBm.bmWidth;
	const CPoint ppt [3] = 
	{ 
		CPoint (cx,	0),			//	[2]		[0] 
		CPoint (cx,	cy),		//
		CPoint (0,	0),			//	[ ]		[1]
	};

	VERIFY (dcEditor.CreateCompatibleDC (&dc));
	VERIFY (dcPrinter.CreateCompatibleDC (&dc));
	VERIFY (bmpPrinter.CreateCompatibleBitmap (&dc, cx, cy));

	CBitmap * pEditor = dcEditor.SelectObject (&bmpEditor);
	CBitmap * pPrinter = dcPrinter.SelectObject (&bmpPrinter);

	::BitBlt (dcPrinter, 0, 0, cx, cy, NULL, 0, 0, WHITENESS);

	int nSetStretchBltMode = dcPrinter.SetStretchBltMode (HALFTONE);

	if (!::PlgBlt (dcPrinter, ppt, dcEditor, 0, 0, cy, cx, NULL, 0, 0))
		TRACEF (FORMATMESSAGE (::GetLastError ()));

	dcPrinter.SetStretchBltMode (nSetStretchBltMode);

	dcEditor.SelectObject (pEditor);
	dcPrinter.SelectObject (pPrinter);
}

////////////////////////////////////////////////////////////////////////////////

UTIL_API void FoxjetCommon::DDV_MinMaxChars (CDataExchange* pDX, CString & value, UINT nCtrlID, 
											 int nMinChars, int nMaxChars)
{
	if (pDX->m_bSaveAndValidate) {
		//value.TrimLeft ();
		//value.TrimRight ();

		int nLen = value.GetLength ();

		if (nLen < nMinChars || nLen > nMaxChars) {
			CString str;
			
			ASSERT (pDX->m_pDlgWnd);

			str.Format (LoadString (IDS_MINMAXCHARS), nMinChars, nMaxChars);
			MsgBox (* pDX->m_pDlgWnd, str);
			pDX->PrepareEditCtrl (nCtrlID);
			pDX->Fail ();
		}
	}
	else {
		if (CEdit * p = (CEdit *)pDX->m_pDlgWnd->GetDlgItem (nCtrlID)) 
			p->SetLimitText (nMaxChars);
	}
}

void FoxjetCommon::DDV_MinMaxChars (CDataExchange* pDX, CString & value, UINT nCtrlID, 
									const FoxjetCommon::LIMITSTRUCT & lmt)
{
	FoxjetCommon::DDV_MinMaxChars (pDX, value, nCtrlID, lmt.m_dwMin, lmt.m_dwMax);
}

bool FoxjetCommon::IsValid (int n, const FoxjetCommon::LIMITSTRUCT & lmt) 
{ 
	return (n >= (int)lmt.m_dwMin && n <= (int)lmt.m_dwMax); 
}

bool FoxjetCommon::IsValid (DWORD dw, const FoxjetCommon::LIMITSTRUCT & lmt) 
{ 
	return (dw >= lmt.m_dwMin && dw <= lmt.m_dwMax); 
}

UTIL_API void FoxjetCommon::DisableDlgCtrls (CDialog * pWnd)
{
	CWnd * pStart = pWnd->GetNextDlgTabItem (NULL); 
	CWnd * pNext = pStart, * pLast = NULL;

	do {
		if (pNext) {
			UINT nID = pNext->GetDlgCtrlID ();

			if (nID > 0 && nID != IDCANCEL)
				pNext->EnableWindow (FALSE);
		}

		pLast = pNext;
		pNext = pWnd->GetNextDlgTabItem (pStart);
	}
	while (pNext != pStart && pLast != pNext);
}

UTIL_API UINT FoxjetCommon::CountBits (DWORD dw)
{
	UINT nBits = 0;
	BYTE * p = (BYTE *)&dw;

	for (int i = 0; i < sizeof (dw); i++)
		nBits += FoxjetCommon::CountBits (p [i]);

	return nBits;
}

// pixel count is actually white pixels (true dot in PRINTER context)
UTIL_API UINT FoxjetCommon::CountBits (BYTE nValue) 
{
	static const UCHAR nBitsPerByte [256] = 
	{
		0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, //   0.. 15
		1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, //  16.. 31
		1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, //  32.. 47
		2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, //  48.. 63
		1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, //  64.. 79
		2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, //  80.. 95
		2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, //  96..111
		3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, // 112..127
		1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, // 128..143
		2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, // 144..159
		2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, // 160..175
		3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, // 176..191
		2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, // 192..207
		3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, // 208..223
		3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, // 224..239
		4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8, // 240..255
	};
	UCHAR nIndex [] = 
	{
		(nValue & 0x000F) >> 0,
		(nValue & 0x00F0) >> 4,
	};
	UINT nResult = 0;

	for (int i = 0; i < ARRAYSIZE (nIndex); i++)
		nResult += nBitsPerByte [nIndex [i]];

	return nResult;
}

// pixel count is actually white pixels (true dot in PRINTER context)
UTIL_API ULONG FoxjetCommon::CountPixels (int nWidthBytes, int nHeight, LPBYTE lpBits)
{
	ULONG lResult = 0;
	DWORD dwLen = nWidthBytes * nHeight;

	for (DWORD dw = 0; dw < dwLen; dw++) 
		lResult += CountBits (lpBits [dw]);
	
	return lResult;
}

// pixel count is actually white pixels (true dot in PRINTER context)
UTIL_API ULONG FoxjetCommon::CountPixels (HBITMAP hbmp)
{
	ULONG lResult = 0;
	BITMAP bm;

	::ZeroMemory (&bm, sizeof (bm)); 
	VERIFY (::GetObject (hbmp, sizeof (bm), &bm));

	const DWORD dwSize = bm.bmWidthBytes * bm.bmHeight;

	if (dwSize) {
		try
		{
			LPBYTE lpBits = new BYTE [dwSize];

			::ZeroMemory (lpBits, sizeof (BYTE) * dwSize);
			::GetBitmapBits (hbmp, dwSize, lpBits);

			lResult = CountPixels (bm.bmWidthBytes, bm.bmHeight, lpBits);

			delete [] lpBits;
		}
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	return lResult;
}

// pixel count is actually white pixels (true dot in PRINTER context)
UTIL_API ULONG FoxjetCommon::CountPixels (CBitmap & bmp)
{
	ULONG lResult = 0;
	BITMAP bm;

	::ZeroMemory (&bm, sizeof (bm)); 
	VERIFY (bmp.GetObject (sizeof (bm), &bm));

	const DWORD dwSize = bm.bmWidthBytes * bm.bmHeight;

	if (dwSize) {
		try
		{
			LPBYTE lpBits = new BYTE [dwSize];

			::ZeroMemory (lpBits, sizeof (BYTE) * dwSize);
			bmp.GetBitmapBits (dwSize, lpBits);

			lResult = CountPixels (bm.bmWidthBytes, bm.bmHeight, lpBits);

			delete [] lpBits;
		}
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	return lResult;
}

UTIL_API CTime FoxjetCommon::GetFileDate (const CString & strPath)
{
	WIN32_FIND_DATA wfd;
	HANDLE hFile = ::FindFirstFile (strPath, &wfd);

	if (hFile != INVALID_HANDLE_VALUE) {
		::FindClose (hFile);
		return CTime (wfd.ftLastWriteTime);
	}

	return CTime ();
}

UTIL_API bool FoxjetCommon::Touch (const CString & strPath)
{
	HANDLE hFile = ::CreateFile (strPath, 
		GENERIC_WRITE, 
		FILE_SHARE_READ | FILE_SHARE_WRITE, 
		NULL, 
		OPEN_EXISTING, 
		FILE_ATTRIBUTE_NORMAL, 
		0);

	if (hFile) {
		FILETIME ft;
		SYSTEMTIME sys;

		::GetFileTime (hFile, NULL, NULL, &ft);

		::GetSystemTime (&sys);
		::SystemTimeToFileTime (&sys, &ft);
		::SetFileTime (hFile, NULL, NULL, &ft);

		::CloseHandle (hFile);
		return true;
	}

	return false;
}

UTIL_API FoxjetCommon::CVersion FoxjetCommon::GetDllVersion (const CString & strPath)
{
	CVersion version (0, 0, 0, 0, 0);
	HMODULE hDLL = ::LoadLibrary (strPath);

	if (hDLL) {
		LPGETDLLVERSION lpf = (LPGETDLLVERSION)::GetProcAddress (hDLL, 
			w2a (FJGETDLLVERSIONPROCADDRESS));
		
		if (lpf) 
			(* lpf) (version);

		::FreeLibrary (hDLL);
	}

	return version;
}

UTIL_API CString FoxjetCommon::GetPrintDriverDirectory ()
{
	TCHAR szDir [MAX_PATH] = { 0 };
	DWORD dwNeeded = 0;
	CStringArray v;

	::GetPrinterDriverDirectory (NULL, NULL, 1, (LPBYTE)szDir, ARRAYSIZE (szDir), &dwNeeded);
	
	GetFiles (szDir, _T ("*.*"), v);

	for (int i = 0; i < v.GetSize (); i++) {
		CString str = v [i];

		str.MakeLower ();

		if (str.Replace (_T ("\\fjmknext.dll"), _T (""))) {
			return str;
		}
	}

	return szDir;
}


struct
{
	LPCTSTR m_lpsz;
	DWORD	m_dw;
	UINT	m_nID;
} static const mapCxImage [] = 
{
	#if CXIMAGE_SUPPORT_BMP
		{ _T ("bmp"),		CXIMAGE_FORMAT_BMP,		IDS_FILESBMP		},
	#endif
	#if CXIMAGE_SUPPORT_GIF
		{ _T ("gif"),		CXIMAGE_FORMAT_GIF,		IDS_FILESGIF		},
	#endif
	#if CXIMAGE_SUPPORT_JPG
		{ _T ("jpg"),		CXIMAGE_FORMAT_JPG,		IDS_FILESJPG		},
		{ _T ("jpe"),		CXIMAGE_FORMAT_JPG,		IDS_FILESJPG		},
		{ _T ("jpeg"),		CXIMAGE_FORMAT_JPG,		IDS_FILESJPG		},
	#endif
	#if CXIMAGE_SUPPORT_PNG
		{ _T ("png"),		CXIMAGE_FORMAT_PNG,		IDS_FILESPNG		},
	#endif
	#if CXIMAGE_SUPPORT_MNG
		{ _T ("mng"),		CXIMAGE_FORMAT_MNG,		IDS_FILESMNG		},
	#endif
	#if CXIMAGE_SUPPORT_ICO
		{ _T ("ico"),		CXIMAGE_FORMAT_ICO,		IDS_FILESICO		},
	#endif
	#if CXIMAGE_SUPPORT_TIF
		{ _T ("tif"),		CXIMAGE_FORMAT_TIF,		IDS_FILESTIF		},
		{ _T ("tiff"),		CXIMAGE_FORMAT_TIF,		IDS_FILESTIFF		},
	#endif
	#if CXIMAGE_SUPPORT_TGA
		{ _T ("tga"),		CXIMAGE_FORMAT_TGA,		IDS_FILESTGA		},
	#endif
	#if CXIMAGE_SUPPORT_PCX
		{ _T ("pcx"),		CXIMAGE_FORMAT_PCX,		IDS_FILESPCX		},
	#endif
	#if CXIMAGE_SUPPORT_WBMP
		{ _T ("wbmp"),		CXIMAGE_FORMAT_WBMP,	IDS_FILESWBMP		},
	#endif
	#if CXIMAGE_SUPPORT_WMF
		{ _T ("wmf"),		CXIMAGE_FORMAT_WMF,		IDS_FILESWMF		},
	#endif
	#if CXIMAGE_SUPPORT_J2K
		{ _T (""),			CXIMAGE_FORMAT_J2K,		IDS_FILESJ2K		},
	#endif
	#if CXIMAGE_SUPPORT_JBG
		{ _T (""),			CXIMAGE_FORMAT_JBG,		IDS_FILESJBG		},
	#endif
	#if CXIMAGE_SUPPORT_JP2
		{ _T (""),			CXIMAGE_FORMAT_JP2,		IDS_FILESJP2		},
	#endif
	#if CXIMAGE_SUPPORT_JPC
		{ _T (""),			CXIMAGE_FORMAT_JPC,		IDS_FILESJPC		},
	#endif
	#if CXIMAGE_SUPPORT_PGX
		{ _T (""),			CXIMAGE_FORMAT_PGX,		IDS_FILESPGX		},
	#endif
	#if CXIMAGE_SUPPORT_PNM
		{ _T (""),			CXIMAGE_FORMAT_PNM,		IDS_FILESPNM		},
	#endif
	#if CXIMAGE_SUPPORT_RAS
		{ _T (""),			CXIMAGE_FORMAT_RAS,		IDS_FILESRAS		},
	#endif
};

UTIL_API CString FoxjetCommon::GetCxImageExt (DWORD dwType)
{
	for (int i = 0; i < ARRAYSIZE (::mapCxImage); i++) 
		if (::mapCxImage [i].m_dw == dwType)
			return ::mapCxImage [i].m_lpsz;

	return _T ("");
}

UTIL_API DWORD FoxjetCommon::GetCxImageType (const CString & strFile)
{
	CString str = FoxjetFile::GetFileExt (strFile);

	for (int i = 0; i < ARRAYSIZE (::mapCxImage); i++) 
		if (!str.CompareNoCase (::mapCxImage [i].m_lpsz))
			return ::mapCxImage [i].m_dw;

	return CXIMAGE_FORMAT_UNKNOWN;
}

UTIL_API CString FoxjetCommon::GetFileDlgList ()
{
	if (FoxjetCommon::verApp < CVersion (1, 20))  // TODO: rem
		return LoadString (IDS_BITMAPS) + _T (" (*.bmp)|*.bmp||");

	CString strAll, str;

	for (int i = 0; i < ARRAYSIZE (::mapCxImage); i++) {
		CString strExt = CString (::mapCxImage [i].m_lpsz);

		if (strExt.GetLength ()) {
			CString strTmp, strType = LoadString (::mapCxImage [i].m_nID);

			strTmp.Format (_T ("%s (*.%s)|*.%s|"),
				strType,
				::mapCxImage [i].m_lpsz, 
				::mapCxImage [i].m_lpsz);

			strAll += _T ("*.") + strExt + _T (";");
			str += strTmp;
		}
	}


	CString strResult = 
		LoadString (IDS_IMAGEFILES) + _T ("|") + strAll + _T ("|") +
		str + _T ("||");

	return strResult;
}


UTIL_API void FoxjetCommon::TestImgFcts (CBitmap & bmp, CSize size, const CString & strName)
{
	#define TESTIMGFCTS_SAVEBMP

	DECLARETRACECOUNT (20);

	{ // RotateLeft90
		DECLARETRACECOUNT (20);
		CDC dcTest;
		CBitmap bmpTest;
		CString str;

		dcTest.CreateCompatibleDC (NULL);								MARKTRACECOUNT ();

		RotateLeft90Plg (dcTest, bmp, bmpTest);							MARKTRACECOUNT ();

		#ifdef TESTIMGFCTS_SAVEBMP
		str.Format (_T ("C:\\Temp\\Debug\\RotateLeft90 %s.bmp"), strName);
		SAVEBITMAP (bmpTest, str);
		#endif //TESTIMGFCTS_SAVEBMP
		
		MARKTRACECOUNT ();
//		TRACECOUNTARRAY ();
	}

	MARKTRACECOUNT ();

	{ // RotateRight90
		DECLARETRACECOUNT (20);
		CDC dcTest;
		CBitmap bmpTest;
		CString str;

		dcTest.CreateCompatibleDC (NULL);								MARKTRACECOUNT ();

		RotateRight90Plg (dcTest, bmp, bmpTest);						MARKTRACECOUNT ();

		#ifdef TESTIMGFCTS_SAVEBMP
		str.Format (_T ("C:\\Temp\\Debug\\RotateRight90 %s.bmp"), strName);
		SAVEBITMAP (bmpTest, str);
		#endif //TESTIMGFCTS_SAVEBMP

		MARKTRACECOUNT ();
//		TRACECOUNTARRAY ();
	}

	MARKTRACECOUNT ();
 	
	{ // Rotate180
		DECLARETRACECOUNT (20);
		CDC dcTest;
		CBitmap bmpTest;
		CString str;
		DIBSECTION ds;

		::ZeroMemory (&ds, sizeof (ds));
		VERIFY (bmp.GetObject (sizeof (ds), &ds));										MARKTRACECOUNT ();
		CSize size (ds.dsBm.bmWidth, ds.dsBm.bmHeight);
		HBITMAP hTest = (HBITMAP)::CopyImage (bmp.m_hObject, IMAGE_BITMAP, 0, 0, 0);	MARKTRACECOUNT ();

		ASSERT (hTest);
		bmpTest.Attach (hTest);															MARKTRACECOUNT ();

		dcTest.CreateCompatibleDC (NULL);												MARKTRACECOUNT ();
		CBitmap * pOld = dcTest.SelectObject (&bmpTest);

		Rotate180Plg (dcTest, size);													MARKTRACECOUNT ();

		#ifdef TESTIMGFCTS_SAVEBMP
		str.Format (_T ("C:\\Temp\\Debug\\Rotate180 %s.bmp"), strName);
		SAVEBITMAP (bmpTest, str);
		dcTest.SelectObject (pOld);
		#endif //TESTIMGFCTS_SAVEBMP

		MARKTRACECOUNT ();
		TRACECOUNTARRAY ();
	}

	MARKTRACECOUNT ();

	{ // RotateLeft90AndFlip
		DECLARETRACECOUNT (20);
		CDC dcTest;
		CBitmap bmpTest;
		CString str;

		dcTest.CreateCompatibleDC (NULL);								MARKTRACECOUNT ();

		RotateLeft90AndFlipPlg (dcTest, bmp, bmpTest);					MARKTRACECOUNT ();

		#ifdef TESTIMGFCTS_SAVEBMP
		str.Format (_T ("C:\\Temp\\Debug\\RotateLeft90AndFlip %s.bmp"), strName);
		SAVEBITMAP (bmpTest, str);
		#endif //TESTIMGFCTS_SAVEBMP

		MARKTRACECOUNT ();
//		TRACECOUNTARRAY ();
	}


// CxImage //////////////////////////////////////////////////////////////////////

	MARKTRACECOUNT ();

	{ // RotateLeft90
		DECLARETRACECOUNT (20);
		CDC dcTest;
		CBitmap bmpTest;
		CString str;

		dcTest.CreateCompatibleDC (NULL);								MARKTRACECOUNT ();

		RotateLeft90 (dcTest, bmp, bmpTest);							MARKTRACECOUNT ();

		#ifdef TESTIMGFCTS_SAVEBMP
		str.Format (_T ("C:\\Temp\\Debug\\RotateLeft90 [CxImage] %s.bmp"), strName);
		SAVEBITMAP (bmpTest, str);
		#endif //TESTIMGFCTS_SAVEBMP
		
		MARKTRACECOUNT ();
//		TRACECOUNTARRAY ();
	}

	MARKTRACECOUNT ();

	{ // RotateRight90
		DECLARETRACECOUNT (20);
		CDC dcTest;
		CBitmap bmpTest;
		CString str;

		dcTest.CreateCompatibleDC (NULL);							MARKTRACECOUNT ();

		RotateRight90 (dcTest, bmp, bmpTest);						MARKTRACECOUNT ();

		#ifdef TESTIMGFCTS_SAVEBMP
		str.Format (_T ("C:\\Temp\\Debug\\RotateRight90 [CxImage] %s.bmp"), strName);
		SAVEBITMAP (bmpTest, str);
		#endif //TESTIMGFCTS_SAVEBMP

		MARKTRACECOUNT ();
//		TRACECOUNTARRAY ();
	}

	MARKTRACECOUNT ();
 	
	{ // Rotate180
		DECLARETRACECOUNT (20);
		CDC dcTest;
		CBitmap bmpTest;
		CString str;
		DIBSECTION ds;

		::ZeroMemory (&ds, sizeof (ds));
		VERIFY (bmp.GetObject (sizeof (ds), &ds));										MARKTRACECOUNT ();
		CSize size (ds.dsBm.bmWidth, ds.dsBm.bmHeight);
		HBITMAP hTest = (HBITMAP)::CopyImage (bmp.m_hObject, IMAGE_BITMAP, 0, 0, 0);	MARKTRACECOUNT ();

		ASSERT (hTest);
		bmpTest.Attach (hTest);															MARKTRACECOUNT ();

		dcTest.CreateCompatibleDC (NULL);												MARKTRACECOUNT ();
		CBitmap * pOld = dcTest.SelectObject (&bmpTest);

		Rotate180 (dcTest, size);														MARKTRACECOUNT ();

		#ifdef TESTIMGFCTS_SAVEBMP
		str.Format (_T ("C:\\Temp\\Debug\\Rotate180 [CxImage] %s.bmp"), strName);
		SAVEBITMAP (bmpTest, str);
		dcTest.SelectObject (pOld);
		#endif //TESTIMGFCTS_SAVEBMP

		MARKTRACECOUNT ();
		TRACECOUNTARRAY ();
	}

	MARKTRACECOUNT ();

	{ // RotateLeft90AndFlip
		DECLARETRACECOUNT (20);
		CDC dcTest;
		CBitmap bmpTest;
		CString str;

		dcTest.CreateCompatibleDC (NULL);								MARKTRACECOUNT ();

		RotateLeft90AndFlip (dcTest, bmp, bmpTest);						MARKTRACECOUNT ();

		#ifdef TESTIMGFCTS_SAVEBMP
		str.Format (_T ("C:\\Temp\\Debug\\RotateLeft90AndFlip [CxImage] %s.bmp"), strName);
		SAVEBITMAP (bmpTest, str);
		#endif //TESTIMGFCTS_SAVEBMP

		MARKTRACECOUNT ();
//		TRACECOUNTARRAY ();
	}

	MARKTRACECOUNT ();

	TRACECOUNTARRAY ();
}


UTIL_API int FoxjetCommon::GetNumberOfDigits (__int64 iMax)
{
	CString str;

	str.Format (_T ("%I64d"), iMax);
	str.Replace (_T ("-"), _T (""));

	return str.GetLength ();
}

UTIL_API bool FoxjetCommon::RestoreState(CWnd * pWnd, const CString & strRegKey)
{
	CWinApp * pApp = ::AfxGetApp ();

	if (pApp) {
		WINDOWPLACEMENT wp;
		CString str = strRegKey.GetLength () ? 
			strRegKey + _T ("\\WindowState") : _T ("WindowState");

		ZeroMemory (&wp, sizeof (wp));
		wp.length = sizeof (WINDOWPLACEMENT);
		pWnd->GetWindowPlacement (&wp);

		if (((wp.flags =
			pApp->GetProfileInt (str, _T ("flags"), -1)) != -1) &&
			((wp.showCmd =
				pApp->GetProfileInt (str, _T ("showCmd"), -1)) != -1) &&
			((wp.rcNormalPosition.left =
				pApp->GetProfileInt (str, _T ("left"), -1)) != -1) &&
			((wp.rcNormalPosition.top =
				pApp->GetProfileInt (str, _T ("top"), -1)) != -1) &&
			((wp.rcNormalPosition.right =
				pApp->GetProfileInt (str, _T ("right"), -1)) != -1) &&
			((wp.rcNormalPosition.bottom =
				pApp->GetProfileInt (str, _T ("bottom"), -1)) != -1))
		{
			wp.rcNormalPosition.left = min (wp.rcNormalPosition.left,
				::GetSystemMetrics (SM_CXSCREEN) - ::GetSystemMetrics (SM_CXICON));
			wp.rcNormalPosition.top = min (wp.rcNormalPosition.top,
				::GetSystemMetrics (SM_CYSCREEN) - ::GetSystemMetrics (SM_CYICON));
			pWnd->SetWindowPlacement (&wp);

			return true;
		}
	}

	return false;
}

UTIL_API void FoxjetCommon::SaveState(CWnd * pWnd, const CString & strRegKey)
{
	CWinApp * pApp = ::AfxGetApp ();

	if (pApp) {
		WINDOWPLACEMENT wp;
		CString str = strRegKey.GetLength () ? 
			strRegKey + _T ("\\WindowState") : _T ("WindowState");

		wp.length = sizeof (WINDOWPLACEMENT);
		pWnd->GetWindowPlacement (&wp);

		pApp->WriteProfileInt (str, _T ("flags"), wp.flags);
		pApp->WriteProfileInt (str, _T ("showCmd"), wp.showCmd);
		pApp->WriteProfileInt (str, _T ("left"), wp.rcNormalPosition.left);
		pApp->WriteProfileInt (str, _T ("top"), wp.rcNormalPosition.top);
		pApp->WriteProfileInt (str, _T ("right"), wp.rcNormalPosition.right);
		pApp->WriteProfileInt (str, _T ("bottom"), wp.rcNormalPosition.bottom); 
	}
}


long CALLBACK MouseProc (int nCode, WPARAM wParam, LPARAM lParam)
{
	switch (wParam) {
	case WM_RBUTTONUP:
		if (gbSwap) {
			SwapMouseButton (FALSE);
			gbSwap = false;

			if (ghwndTarget != NULL && glWinMsg != 0)
				::PostMessage (ghwndTarget, glWinMsg, 0, 0);
		}

		break;
	}

	return CallNextHookEx (ghHook, nCode, wParam, lParam);
}

LRESULT CALLBACK Hook (HWND hwnd, ULONG lMsg)
{
	ghwndTarget = hwnd;
	glWinMsg = lMsg;
	ghHook = SetWindowsHookEx (WH_MOUSE, (HOOKPROC)MouseProc, hInstance, 0);
	return (ULONG)ghHook;
}

LRESULT CALLBACK Unhook (HWND hwnd)
{
	LRESULT lResult = UnhookWindowsHookEx (ghHook);
	ghwndTarget = NULL;
	glWinMsg = 0;
	ghHook = NULL;
	return lResult;
}

LRESULT CALLBACK Swap ()
{
	gbSwap = true;
	SwapMouseButton (TRUE);
	return 1;
}

static const double dChannelSeparation = 0.05848; // Trident Heads
UTIL_API int FoxjetCommon::CalculateNFactor (double dAngle, double dRes)
{
	
#if defined (__IPPRINTER__)

	double x = 0.0;
	double dNFactor = 0;
	bool bMore = true;
	const int nAngle = (int)(dAngle * 10.0);

	while (bMore) {
		x = dNFactor / (dRes * ::dChannelSeparation);
		
		if (bMore = (x >= 0.0) && (x < 0.97)) {
			double dCalcAngle = RAD2DEG (acos (x));
			int nCalcAngle = (int)(dCalcAngle * 10.0);

			if (nAngle == nCalcAngle)
				return (int)dNFactor;
		}
		
		dNFactor++;
	}

	return 0;

#else
	ASSERT (0);

	/*
	double NFactor;

	if (angle >= 90.0) {
		switch (m_HeadSettings.m_nHeadType) {
		case FoxjetDatabase::GRAPHICS_768_256: 
		case FoxjetDatabase::GRAPHICS_384_128:
		case FoxjetDatabase::UJ2_352_32:
		{
			if (m_HeadSettings.m_nHorzRes == 426)
				return 8;

			if (FoxjetCommon::IsProductionConfig ()) {
				switch (m_HeadSettings.m_nEncoderDivisor) {
				case 2:	return 6;	// 150
				case 3:	return 8;	// 200
				case 1:	return 12;	// 300
				}
			}

			return 6;
		}
		case FoxjetDatabase::GRAPHICS_768_256_L310:
			return 46;

		case FoxjetDatabase::IV_72:
		{
			// 0.192" is channel separation for valve heads (0.040" for others?)
			int n = (int)(0.192 * res); // 0.192 / (1 / res)

			return n;
		}
		default:
			return 0;
		}
	}

	NFactor = 0.05848 * cos ( DEG2RAD(angle) );
//	NFactor *= res / 2;
	NFactor *= res;
	NFactor += .5;

	int n = Foxjet3d::CHeadDlg::CalcNFactor (angle, res);
	int t = ( int ) floor(NFactor);

	//{ CString str; str.Format (_T ("CalculateNFactor (angle: %f, res: %f): %d"), angle, res, t); TRACEF (str); }
	*/

	return 0; // TODO: return t;

#endif

}


UTIL_API unsigned __int64 FoxjetCommon::strtoul64 (const CString & str)
{
	unsigned __int64 iResult = 0;
	CString strTmp (str);
	
	strTmp.Replace (_T (","), _T (""));
	_stscanf (strTmp, _T ("%I64d"), &iResult);

	return iResult;
}


UTIL_API CString FoxjetCommon::FormatI64 (__int64 iValue, int nLeadingZeroes)
{
	CString str, strFmt = _T ("%I64d");
	CArray <TCHAR, TCHAR> v;

	if (nLeadingZeroes) 
		strFmt.Format (_T ("%%0%dI64d"), nLeadingZeroes);

	str.Format (strFmt, iValue);

	for (int i = str.GetLength () - 1; i >= 0; i--) 
		v.Add (str [i]);

	//TRACEF (str);
	str.Empty ();

	for (int i = 0; i < v.GetSize (); i++) {
		str += v [i];

		if (((i + 1) % 3) == 0 && ((i + 1) > 0) && (i + 1) < v.GetSize ())
			str += _T (",");
	}

	str.MakeReverse ();
	//TRACEF (str);

	return str;
}

UTIL_API void FoxjetCommon::DDV_MinMaxUnsignedInt64 (CDataExchange* pDX, int nIDC, unsigned __int64 iValue, unsigned __int64 iMin, unsigned __int64 iMax)
{
	if (pDX->m_bSaveAndValidate) {
		if (iValue < iMin) {
			CString str;

			str.Format (LoadString (IDS_MININT64), FormatI64 (iMin));
			MsgBox (* pDX->m_pDlgWnd, str);
			pDX->PrepareEditCtrl (nIDC);
			pDX->Fail ();
		}
		else if (iValue > iMax) {
			CString str;

			str.Format (LoadString (IDS_MAXINT64), FormatI64 (iMax));
			MsgBox (* pDX->m_pDlgWnd, str);
			pDX->PrepareEditCtrl (nIDC);
			pDX->Fail ();
		}
	}
}

UTIL_API void FoxjetCommon::DDV_MinMaxInt64 (CDataExchange* pDX, int nIDC, __int64 iValue, __int64 iMin, __int64 iMax)
{
	if (pDX->m_bSaveAndValidate) {
		if (iValue < iMin) {
			CString str;

			str.Format (LoadString (IDS_MININT64), FormatI64 (iMin));
			MsgBox (* pDX->m_pDlgWnd, str);
			pDX->PrepareEditCtrl (nIDC);
			pDX->Fail ();
		}
		else if (iValue > iMax) {
			CString str;

			str.Format (LoadString (IDS_MAXINT64), FormatI64 (iMax));
			MsgBox (* pDX->m_pDlgWnd, str);
			pDX->PrepareEditCtrl (nIDC);
			pDX->Fail ();
		}
	}
}

UTIL_API void FoxjetCommon::DDX_Text (CDataExchange* pDX, int nIDC, __int64 & iValue)
{
	if (pDX->m_bSaveAndValidate) {
		CString str;

		pDX->m_pDlgWnd->GetDlgItemText (nIDC, str);
		str.Replace (_T (","), _T (""));
		
		if (_stscanf (str, _T ("%I64d"), &iValue) != 1) {
			pDX->PrepareEditCtrl (nIDC);
			MsgBox (pDX->m_pDlgWnd->m_hWnd, LoadString (IDS_INVALIDDATA), LoadString (IDS_ERROR), MB_ICONERROR);
			pDX->Fail ();
		}
	}
	else 
		pDX->m_pDlgWnd->SetDlgItemText (nIDC, FormatI64 (iValue));
}

UTIL_API BOOL CALLBACK FoxjetCommon::EnumWindowsProc (HWND hWnd, LPARAM lParam)
{
	CArray <HWND, HWND> & v = * (CArray <HWND, HWND> *)lParam;

	if (::IsWindowVisible (hWnd)) {
		DWORD dwStyle = ::GetWindowLong (hWnd, GWL_STYLE);
		DWORD dwExStyle = ::GetWindowLong (hWnd, GWL_EXSTYLE);

		//if ((dwStyle & WS_MINIMIZEBOX) || (dwStyle & WS_MAXIMIZEBOX)) 
			v.Add (hWnd);
	}

	return TRUE;
}

UTIL_API void FoxjetCommon::SendKey (int nVK, bool bDown)
{
	INPUT key;

	memset (&key, 0, sizeof (INPUT));

	key.ki.dwFlags	= bDown ? 0 : KEYEVENTF_KEYUP;

	key.type		= INPUT_KEYBOARD;
	key.ki.wVk		= nVK;
	key.ki.wScan	= nVK;

	::SendInput (1, &key, sizeof (INPUT));
	::Sleep (1);
}

UTIL_API void FoxjetCommon::ActivateProcess (DWORD dwProcessID)
{
	CArray <HWND, HWND> v;

	::EnumWindows (FoxjetCommon::EnumWindowsProc, (LPARAM)&v);

	for (int i = 0; i < v.GetSize (); i++) {
		DWORD dw = 0;
		HWND hwnd = v [i];

		GetWindowThreadProcessId (hwnd, &dw);
		
		if (dwProcessID == dw) {
			::BringWindowToTop (hwnd);
			::SetActiveWindow (hwnd);
			break;
		}
	}
}

static BOOL CALLBACK EnumHiddenWindowsProc (HWND hWnd, LPARAM lParam)
{
	CArray <HWND, HWND> & v = * (CArray <HWND, HWND> *)lParam;
	
	v.Add (hWnd);

	return TRUE;
}

UTIL_API HWND FoxjetCommon::LocateControlWnd ()
{
	CArray <HWND, HWND> v;

	::EnumWindows (EnumWindowsProc, (LPARAM)&v);

	for (int i = 0; i < v.GetSize (); i++) {
		DWORD dwResult = 0;
		int n = 0;

		HWND hwnd = v [i];
		LRESULT lResult = ::SendMessageTimeout (hwnd, WM_ISPRINTERRUNNING, 1, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 100, &dwResult);
		
		if (dwResult == (DWORD)hwnd) {
			#ifdef _DEBUG
			TCHAR sz [256] = { 0 };

			::GetWindowText (hwnd, sz, ARRAYSIZE (sz));
			TRACEF (sz);
			#endif

			return hwnd;
		}
	}

	return NULL;
}

UTIL_API HWND FoxjetCommon::LocateSkuManager()
{
	CArray <HWND, HWND> v;

	::EnumWindows (EnumWindowsProc, (LPARAM)&v);

	for (int i = 0; i < v.GetSize (); i++) {
		DWORD dwResult = 0;
		int n = 0;

		HWND hwnd = v [i];
		LRESULT lResult = ::SendMessageTimeout (hwnd, WM_ISSKUMGRRUNNING, 1, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 100, &dwResult);
		
		if (dwResult == (DWORD)hwnd) {
			#ifdef _DEBUG
			TCHAR sz [256] = { 0 };

			::GetWindowText (hwnd, sz, ARRAYSIZE (sz));
			TRACEF (sz);
			#endif

			return hwnd;
		}
	}

	return NULL;
}

UTIL_API HWND FoxjetCommon::FindKeyboard ()
{
	CArray <HWND, HWND> v;

	::EnumWindows (EnumHiddenWindowsProc, (LPARAM)&v);

	for (int i = 0; i < v.GetSize (); i++) {
		DWORD dwResult = 0;
		HWND hwnd = v [i];

		#ifdef _DEBUG
		TCHAR sz [256] = { 0 };

		::GetWindowText (hwnd, sz, ARRAYSIZE (sz));
		#endif

		LRESULT lResult = ::SendMessageTimeout (hwnd, WM_ISKEYBOARD, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 100, &dwResult);
		
		//TRACEF (_T ("WM_ISKEYBOARD: ") + FoxjetDatabase::ToString ((int)lResult) + _T (" :") + FoxjetDatabase::ToString ((int)dwResult));

		if (dwResult == BW_HOST_TCP_PORT) {
			#ifdef _DEBUG
			TRACEF (sz);
			#endif

			return hwnd;
		}
	}

	return NULL;
}

UTIL_API HWND FoxjetCommon::FindKeyboardMonitor ()
{
	CArray <HWND, HWND> v;

	::EnumWindows (EnumHiddenWindowsProc, (LPARAM)&v);

	for (int i = 0; i < v.GetSize (); i++) {
		DWORD dwResult = 0;
		HWND hwnd = v [i];

		#ifdef _DEBUG
		TCHAR sz [256] = { 0 };

		::GetWindowText (hwnd, sz, ARRAYSIZE (sz));
		#endif

		LRESULT lResult = ::SendMessageTimeout (hwnd, WM_ISKEYBOARDMONITORRUNNING, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 100, &dwResult);
		
		//TRACEF (_T ("WM_ISKEYBOARD: ") + FoxjetDatabase::ToString ((int)lResult) + _T (" :") + FoxjetDatabase::ToString ((int)dwResult));

		if (dwResult == WM_ISKEYBOARDMONITORRUNNING) {
			#ifdef _DEBUG
			TRACEF (sz);
			#endif

			return hwnd;
		}
	}

	return NULL;
}

#ifdef _DEBUG
CString GetDlgCode (HWND hWndCtrl)
{
	CString str;
	DWORD dw = ::SendMessage (hWndCtrl, WM_GETDLGCODE, 0, 0L);

	if (dw & DLGC_BUTTON)				str += _T ("DLGC_BUTTON ");
	if (dw & DLGC_DEFPUSHBUTTON)		str += _T ("DLGC_DEFPUSHBUTTON ");
	if (dw & DLGC_HASSETSEL)			str += _T ("DLGC_HASSETSEL ");
	if (dw & DLGC_RADIOBUTTON)			str += _T ("DLGC_RADIOBUTTON ");
	if (dw & DLGC_STATIC)				str += _T ("DLGC_STATIC ");
	if (dw & DLGC_UNDEFPUSHBUTTON)		str += _T ("DLGC_UNDEFPUSHBUTTON ");
	if (dw & DLGC_WANTALLKEYS)			str += _T ("DLGC_WANTALLKEYS ");
	if (dw & DLGC_WANTARROWS)			str += _T ("DLGC_WANTARROWS ");
	if (dw & DLGC_WANTCHARS)			str += _T ("DLGC_WANTCHARS ");
	if (dw & DLGC_WANTMESSAGE)			str += _T ("DLGC_WANTMESSAGE ");
	if (dw & DLGC_WANTTAB)				str += _T ("DLGC_WANTTAB ");

	return str;
}

CString GetButtonState (HWND hWndCtrl)
{
	CString str;
	LRESULT lCheck = ::SendMessage (hWndCtrl, BM_GETSTATE, 0, 0);

	if (lCheck & 0x0003)			str += (_T ("0x0003 "));
	if (lCheck & BST_CHECKED)		str += (_T ("BST_CHECKED "));
	if (lCheck & BST_FOCUS)			str += (_T ("BST_FOCUS "));
	if (lCheck & BST_INDETERMINATE)	str += (_T ("BST_INDETERMINATE "));
	if (lCheck & BST_PUSHED)		str += (_T ("BST_PUSHED "));		
	if (lCheck & BST_UNCHECKED)		str += (_T ("BST_UNCHECKED "));		

	return str;
}

CString GetStyleString (HWND hwnd, LONG lStyle, LONG lExStyle)
{
	CString str;
	TCHAR sz [128] = { 0 };

	::GetClassName (hwnd, sz, ARRAYSIZE (sz) - 1);
	str.Format (_T ("[0x%p] [%s] 0x%p ex: 0x%p "), hwnd, sz, lStyle, lExStyle);

	if (lStyle & BS_DEFPUSHBUTTON)    str += _T ("BS_DEFPUSHBUTTON ");   
	if (lStyle & BS_CHECKBOX)         str += _T ("BS_CHECKBOX ");        
	if (lStyle & BS_AUTOCHECKBOX)     str += _T ("BS_AUTOCHECKBOX ");    
	if (lStyle & BS_RADIOBUTTON)      str += _T ("BS_RADIOBUTTON ");     
	if (lStyle & BS_3STATE)           str += _T ("BS_3STATE ");          
	if (lStyle & BS_AUTO3STATE)       str += _T ("BS_AUTO3STATE ");      
	if (lStyle & BS_GROUPBOX)         str += _T ("BS_GROUPBOX ");        
	if (lStyle & BS_USERBUTTON)       str += _T ("BS_USERBUTTON ");      
	if (lStyle & BS_AUTORADIOBUTTON)  str += _T ("BS_AUTORADIOBUTTON "); 
	if (lStyle & BS_OWNERDRAW)        str += _T ("BS_OWNERDRAW ");       
	if (lStyle & BS_LEFTTEXT)         str += _T ("BS_LEFTTEXT ");        
	if (lStyle & BS_TEXT)             str += _T ("BS_TEXT ");            
	if (lStyle & BS_ICON)             str += _T ("BS_ICON ");            
	if (lStyle & BS_BITMAP)           str += _T ("BS_BITMAP ");          
	if (lStyle & BS_LEFT)             str += _T ("BS_LEFT ");            
	if (lStyle & BS_RIGHT)            str += _T ("BS_RIGHT ");           
	if (lStyle & BS_CENTER)           str += _T ("BS_CENTER ");          
	if (lStyle & BS_TOP)              str += _T ("BS_TOP ");             
	if (lStyle & BS_BOTTOM)           str += _T ("BS_BOTTOM ");          
	if (lStyle & BS_VCENTER)          str += _T ("BS_VCENTER ");         
	if (lStyle & BS_PUSHLIKE)         str += _T ("BS_PUSHLIKE ");        
	if (lStyle & BS_MULTILINE)        str += _T ("BS_MULTILINE ");       
	if (lStyle & BS_NOTIFY)           str += _T ("BS_NOTIFY ");          
	if (lStyle & BS_FLAT)             str += _T ("BS_FLAT ");            

	return str;
}
#endif

UTIL_API CString FoxjetCommon::execute (const CString & str)
{
	CString strResult;
	HANDLE hOutputReadTmp,hOutputRead,hOutputWrite;
	HANDLE hInputWriteTmp,hInputRead,hInputWrite;
	HANDLE hErrorWrite;
	SECURITY_ATTRIBUTES sa;
	HANDLE hStdIn = NULL; // Handle to parents std input.

	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;

	if (!::CreatePipe (&hOutputReadTmp, &hOutputWrite, &sa, 0))
		TRACEF (FormatMessage (::GetLastError ()));

	// Create a duplicate of the output write handle for the std error
	// write handle. This is necessary in case the child application
	// closes one of its std output handles.
	if (!::DuplicateHandle (::GetCurrentProcess (), hOutputWrite, GetCurrentProcess(), &hErrorWrite, 0, TRUE, DUPLICATE_SAME_ACCESS))
		TRACEF (FormatMessage (::GetLastError ()));


	// Create the child input pipe.
	if (!::CreatePipe (&hInputRead, &hInputWriteTmp, &sa, 0))
		TRACEF (FormatMessage (::GetLastError ()));


	// Create new output read handle and the input write handles. Set
	// the Properties to FALSE. Otherwise, the child inherits the
	// properties and, as a result, non-closeable handles to the pipes
	// are created.
	if (!::DuplicateHandle (::GetCurrentProcess (), hOutputReadTmp, ::GetCurrentProcess (), &hOutputRead, 0, FALSE, DUPLICATE_SAME_ACCESS))
		TRACEF (FormatMessage (::GetLastError ()));

	if (!::DuplicateHandle (::GetCurrentProcess (), hInputWriteTmp, ::GetCurrentProcess (), &hInputWrite, 0, FALSE, DUPLICATE_SAME_ACCESS))
		TRACEF (FormatMessage (::GetLastError ()));


	// Close inheritable copies of the handles you do not want to be inherited.
	if (!::CloseHandle (hOutputReadTmp)) TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hInputWriteTmp)) TRACEF (FormatMessage (::GetLastError ()));


	// Get std input handle so you can close it and force the ReadFile to
	// fail when you want the input thread to exit.
	if ((hStdIn = GetStdHandle(STD_INPUT_HANDLE)) == INVALID_HANDLE_VALUE)
		TRACEF (FormatMessage (::GetLastError ()));

	{
		PROCESS_INFORMATION pi;
		STARTUPINFO si;
		TCHAR * psz = new TCHAR [str.GetLength () + 1];

		
		ZeroMemory (psz, str.GetLength () + 1);
		ZeroMemory (&si, sizeof (STARTUPINFO));

		si.cb			= sizeof (STARTUPINFO);
		si.dwFlags		= STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
		si.wShowWindow	= SW_HIDE;
		si.hStdOutput	= hOutputWrite;
		si.hStdInput	= hInputRead;
		si.hStdError	= hErrorWrite;

		_tcscpy (psz, str);
		TRACEF (psz);

		if (!::CreateProcess (NULL, psz, NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi))
			TRACEF (FormatMessage (::GetLastError ()));

		if (!::CloseHandle (pi.hThread)) 
			TRACEF (FormatMessage (::GetLastError ()));

		delete [] psz;
	}


	if (!::CloseHandle (hOutputWrite))	TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hInputRead ))	TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hErrorWrite))	TRACEF (FormatMessage (::GetLastError ()));

	while (1) {
		const int nSize = 256;
		CHAR lpBuffer [nSize + 1] = { 0 };
		DWORD nBytesRead = 0;
		DWORD nCharsWritten = 0;

		if (!::ReadFile (hOutputRead, lpBuffer, nSize, &nBytesRead, NULL) || !nBytesRead) {
			if (::GetLastError() == ERROR_BROKEN_PIPE)
				break; // pipe done - normal exit path.
			else
				TRACEF (FormatMessage (::GetLastError ()));
		}

		strResult += a2w (lpBuffer);
		//::OutputDebugString (a2w (lpBuffer)); ::Sleep (1);
	}

	if (!::CloseHandle (hOutputRead))	TRACEF (FormatMessage (::GetLastError ()));
	if (!::CloseHandle (hInputWrite))	TRACEF (FormatMessage (::GetLastError ()));

	return strResult;
}

UTIL_API void FoxjetCommon::AlignToKeyboard (CWnd * pwnd)
{
	if (pwnd) {
		if (CWnd * pMain = ::AfxGetMainWnd ())
			pwnd->CenterWindow (pMain);

		if (!IsMatrix ()) {
			if (HWND hwnd = FindKeyboard ()) {
				CRect rc (0, 0, 0, 0);
				CRect rcKeyboard (0, 0, 0, 0);

				::GetWindowRect (pwnd->m_hWnd, rc);
				::GetWindowRect (hwnd, rcKeyboard);

				int nDiff = rc.bottom - rcKeyboard.top;

				if (nDiff > 0) 
					::SetWindowPos (pwnd->m_hWnd, NULL, rc.left, rc.top - nDiff, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
			}
		}
	}
}

static void InsertDescending (CCopyArray <PDFSEARCHSTRUCT, PDFSEARCHSTRUCT &> & v, PDFSEARCHSTRUCT & s)
{
	for (int i = 0; i < v.GetSize (); i++) {
		if (s.m_nCount >= v [i].m_nCount) {
			v.InsertAt (i, s);
			return;
		}
	}

	v.Add (s);
}

#include <podofo.h>
#include "src\doc\PdfDocument.h"
#include "src\doc\PdfMemDocument.h"
#include "src\base\PdfContentsTokenizer.h"

static CString GetPdfIndexFileName (const CString & strFile)
{
	int nIndex = strFile.ReverseFind ('.');

	if (nIndex != -1) {
		CString strSuffix = strFile.Mid (nIndex);
		CString strTxt (strFile);

		strTxt.Replace (strSuffix, _T (".txt"));

		return strTxt;
	}

	return _T ("");
}

static CString GetTempPath ()
{
	TCHAR szTemp [MAX_PATH] = { 0 };
	::GetTempPath (ARRAYSIZE (szTemp), szTemp);
	::GetLongPathName (szTemp, szTemp, ARRAYSIZE (szTemp));
	szTemp [_tcslen (szTemp) - 1] = '\0';

	TRACEF (szTemp);

	return szTemp;
}

static CString GetPdfToExePath ()
{
	CString strFilepath = GetHomeDir () + _T ("\\Language\\") + GetLangDir (_T ("en")) + _T ("\\pdftotxt.exe"); // ::GetTempPath () + _T ("\\pdftotxt.exe");

	if (::GetFileAttributes (strFilepath) == -1) {
		HINSTANCE hModule =  ::hInstance; // ::GetModuleHandle (NULL);
		bool bResult = false;
		UINT nID = IDR_PDF_TO_EXE;

		if (HRSRC hRSRC = ::FindResource (hModule, MAKEINTRESOURCE (nID), _T ("EXE"))) {
			DWORD dwSize = ::SizeofResource (hModule, hRSRC);

			if (HGLOBAL hGlobal = ::LoadResource (hModule, hRSRC)) {
				if (LPVOID lpData = ::LockResource (hGlobal)) {
					if (FILE * fp = _tfopen (strFilepath, _T ("wb"))) {
						if (fwrite (lpData, dwSize, 1, fp) == 1) {
							TRACEF (_T ("wrote: ") + strFilepath);
						}

						fclose (fp);
					}
				}
			}
		}
	}

	return strFilepath;
}

static void BuildPdfIndexFile (const CString & strPDF, const CString & strTxt)
{
	using namespace std;
	using namespace PoDoFo;

	if (FILE * fp = _tfopen (strTxt, _T ("wb"))) {
		const BYTE nUTF16 [] = { 0xFF, 0xFE };

		fwrite (nUTF16, ARRAYSIZE (nUTF16), 1, fp);

		{
			CString strExe = GetPdfToExePath ();
			CString str, strTmp = strTxt;
			
			strTmp.Replace (_T (".txt"), _T (".text"));
			str.Format (_T ("\"%s\" \"%s\" \"%s\""), strExe, strPDF, strTmp);
			TRACEF (str);
			str = execute (str);
			TRACEF (str);

			str = FoxjetFile::ReadDirect (strTmp);
			//TRACEF (str);

			std::vector <std::wstring> v = explode (str, (TCHAR)0x0C);

			for (int i = 0; i < v.size (); i++) {
				CString strLine = _T ("[") + ToString (i) + _T ("] ") + RemovePunctuation (v [i].c_str ());
				//TRACEF (strLine);
				strLine += _T ("\r\n");
				fwrite ((LPVOID)(LPCTSTR)strLine, strLine.GetLength () * sizeof (TCHAR), 1, fp);
			}

			#ifndef _DEBUG
			::DeleteFile (strExe);
			::DeleteFile (strTmp);
			#endif
		}

		#if 0
		std::vector <std::wstring> vLines;
		PdfMemDocument doc ((const char *)w2a (strPDF));
		PdfDocument & document = (PdfDocument &)doc;
		int nPage = 1;


		int nCount = document.GetPageCount ();

		for (int i = 0; i < nCount; i++) {
			std::stack<PdfVariant> stack;
			PdfVariant var;
			EPdfContentsType eType;
			const char * pszToken	= NULL;
			PdfPage * pPage			= document.GetPage (i);
			double dCurPosX			= 0.0;
			double dCurPosY			= 0.0;
			bool bTextBlock			= false;
			//PdfFont * pCurFont		= NULL;

			if (pPage) {
				std::vector <std::wstring> v;
				///*
				PdfContentsTokenizer tokenizer ((PdfCanvas *)pPage);

				while( tokenizer.ReadNext( eType, pszToken, var ) )
				{
					if( eType == ePdfContentsType_Keyword )
					{
						// support 'l' and 'm' tokens
						if( strcmp( pszToken, "l" ) == 0 || 
							strcmp( pszToken, "m" ) == 0 )
						{
							dCurPosX = stack.top().GetReal();
							stack.pop();
							dCurPosY = stack.top().GetReal();
							stack.pop();
						}
						else if( strcmp( pszToken, "BT" ) == 0 ) 
						{
							bTextBlock   = true;     
							// BT does not reset font
							// pCurFont     = NULL;
						}
						else if( strcmp( pszToken, "ET" ) == 0 ) 
						{
							if( !bTextBlock ) 
								fprintf( stderr, "WARNING: Found ET without BT!\n" );
						}

						if( bTextBlock ) 
						{
							if( strcmp( pszToken, "Tf" ) == 0 ) 
							{
								stack.pop();
								PdfName fontName = stack.top().GetName();
								PdfObject* pFont = pPage->GetFromResources( PdfName("Font"), fontName );
								if( !pFont ) 
								{
									PODOFO_RAISE_ERROR_INFO( ePdfError_InvalidHandle, "Cannot create font!" );
								}

								//pCurFont = document.GetFont( pFont );
								//if( !pCurFont ) 
								//{
								//	fprintf( stderr, "WARNING: Unable to create font for object %i %i R\n",
								//			 pFont->Reference().ObjectNumber(),
								//			 pFont->Reference().GenerationNumber() );
								//}
							}
							else if( strcmp( pszToken, "Tj" ) == 0 ||
										strcmp( pszToken, "'" ) == 0 ) 
							{
								v.push_back (stack.top().GetString().GetStringW ().c_str ());
								stack.pop();
							}
							else if( strcmp( pszToken, "\"" ) == 0 )
							{
								v.push_back (stack.top().GetString().GetStringW ().c_str ());
								stack.pop();
								stack.pop(); // remove char spacing from stack
								stack.pop(); // remove word spacing from stack
							}
							else if( strcmp( pszToken, "TJ" ) == 0 ) 
							{
								std::wstring str;
								PdfArray array = stack.top().GetArray();
								stack.pop();
                    
								for( int i=0; i<static_cast<int>(array.GetSize()); i++ ) 
								{
									if( array[i].IsString() || array[i].IsHexString() ) 
										str.append (array[i].GetString ().GetStringW ());
								}

								v.push_back (str.c_str ());
							}
						}
					}
					else if ( eType == ePdfContentsType_Variant )
					{
						stack.push( var );
					}
					else
					{
						// Impossible; type must be keyword or variant
						PODOFO_RAISE_ERROR( ePdfError_InternalLogic );
					}
				}

				{
					std::wstring str;
					CString strLine;

					for (int i = 0; i < v.size (); i++) {
						str.append (v [i].c_str ());
						str.append (_T (" "));
					}

					wstringstream iss(str);
					wstring word;
				
					while(iss >> word) {
						strLine += word.c_str ();
						strLine += _T (" ");
					}

					strLine = _T ("[") + ToString (nPage++) + _T ("] ") + RemovePunctuation (strLine);
					TRACEF (strLine);
					strLine += _T ("\r\n");
					fwrite ((LPVOID)(LPCTSTR)strLine, strLine.GetLength () * sizeof (TCHAR), 1, fp);
				}
			}
		}
		#endif

		fclose (fp);
	}
}


UTIL_API CCopyArray <PDFSEARCHSTRUCT, PDFSEARCHSTRUCT &> FoxjetCommon::PdfSearch (const CString & strFile, const CString & strFindIn, const CString & strLangExt)
{
	using namespace PoDoFo;
	using namespace std;

	CCopyArray <PDFSEARCHSTRUCT, PDFSEARCHSTRUCT &> result;

	if (::GetFileAttributes (strFile) != -1) {
		const CString strFind = GetSynonym (RemoveWhitespace (RemovePunctuation (strFindIn)), strLangExt);
		CString strTxt = GetPdfIndexFileName (strFile);

		if (::GetFileAttributes (strTxt) == -1) {
			if (MsgBox (LoadString (IDS_BUILDHELPINDEX), _T (""), MB_YESNO) == IDNO) {
				PDFSEARCHSTRUCT f;
				
				f.m_nCount = f.m_nPage = -1;
				result.Add (f);

				return result;
			}
			else {
				if (CWnd * p = ::AfxGetMainWnd ())
					p->BeginWaitCursor ();

				{
					CString str = strFile;

					str.MakeLower ();

					if (str.Find (_T ("language\\english\\")) == -1) {
						CStringArray v;

						GetFiles (GetHomeDir () + _T ("\\Language\\") + GetLangDir (_T ("en")), _T ("*.pdf"), v);
						CString strEnglishPDF = GetMostRecentFile (v);
						CString strEnglishTxt = GetPdfIndexFileName (strEnglishPDF);

						if (::GetFileAttributes (strEnglishTxt) == -1)
							BuildPdfIndexFile (strEnglishPDF, strEnglishTxt);
					}
				}

				BuildPdfIndexFile (strFile, strTxt);

				if (CWnd * p = ::AfxGetMainWnd ())
					p->EndWaitCursor ();
			}
		}

		std::vector <std::wstring> v = explode (std::wstring (FoxjetFile::ReadDirect (strTxt)), '\n');
		CString strFindLower (strFind);

		strFindLower.MakeLower ();

		if (strFindLower.GetLength ()) {
			for (int i = 0; i < v.size (); i++) {
				CString str = v [i].c_str ();
				int nIndex = -1;
				PDFSEARCHSTRUCT s;

				s.m_nPage =	i + 1;
				s.m_nCount = 0;
				str.MakeLower ();

				while ((nIndex = str.Find (strFindLower)) != -1) {
					str.Delete (nIndex, strFindLower.GetLength ());
					s.m_nCount++;
				}

				if (s.m_nCount > 0) 
					InsertDescending (result, s);
			}
		}
	}

	return result;
}

#include <shlobj.h>

struct
{
	int m_csidl;
	const KNOWNFOLDERID * m_prfid;
	LPCTSTR m_lpsz;
} 
static const foldermap [] = 
{
	{ CSIDL_ADMINTOOLS,					&FOLDERID_AdminTools,				_T ("CSIDL_ADMINTOOLS"),				},
	{ CSIDL_ALTSTARTUP,					&FOLDERID_Startup,					_T ("CSIDL_ALTSTARTUP"),				},
	{ CSIDL_APPDATA,					&FOLDERID_RoamingAppData,			_T ("CSIDL_APPDATA"),					},
	{ CSIDL_BITBUCKET,					&FOLDERID_RecycleBinFolder,			_T ("CSIDL_BITBUCKET"),					},
	{ CSIDL_CDBURN_AREA,				&FOLDERID_CDBurning,				_T ("CSIDL_CDBURN_AREA"),				},
	{ CSIDL_COMMON_ADMINTOOLS,			&FOLDERID_CommonAdminTools,			_T ("CSIDL_COMMON_ADMINTOOLS"),			},
	{ CSIDL_COMMON_ALTSTARTUP,			&FOLDERID_CommonStartup,			_T ("CSIDL_COMMON_ALTSTARTUP"),			},
	{ CSIDL_COMMON_APPDATA,				&FOLDERID_ProgramData,				_T ("CSIDL_COMMON_APPDATA"),			},
	{ CSIDL_COMMON_DESKTOPDIRECTORY,	&FOLDERID_PublicDesktop,			_T ("CSIDL_COMMON_DESKTOPDIRECTORY"),	},
	{ CSIDL_COMMON_DOCUMENTS,			&FOLDERID_PublicDocuments,			_T ("CSIDL_COMMON_DOCUMENTS"),			},
	{ CSIDL_COMMON_FAVORITES,			&FOLDERID_Favorites,				_T ("CSIDL_COMMON_FAVORITES"),			},
	{ CSIDL_COMMON_MUSIC,				&FOLDERID_PublicMusic,				_T ("CSIDL_COMMON_MUSIC"),				},
	{ CSIDL_COMMON_OEM_LINKS,			&FOLDERID_CommonOEMLinks,			_T ("CSIDL_COMMON_OEM_LINKS"),			},
	{ CSIDL_COMMON_PICTURES,			&FOLDERID_PublicPictures,			_T ("CSIDL_COMMON_PICTURES"),			},
	{ CSIDL_COMMON_PROGRAMS,			&FOLDERID_CommonPrograms,			_T ("CSIDL_COMMON_PROGRAMS"),			},
	{ CSIDL_COMMON_STARTMENU,			&FOLDERID_CommonStartMenu,			_T ("CSIDL_COMMON_STARTMENU"),			},
	{ CSIDL_COMMON_STARTUP,				&FOLDERID_CommonStartup,			_T ("CSIDL_COMMON_STARTUP"),			},
	{ CSIDL_COMMON_TEMPLATES,			&FOLDERID_CommonTemplates,			_T ("CSIDL_COMMON_TEMPLATES"),			},
	{ CSIDL_COMMON_VIDEO,				&FOLDERID_PublicVideos,				_T ("CSIDL_COMMON_VIDEO"),				},
	{ CSIDL_COMPUTERSNEARME,			&FOLDERID_NetworkFolder,			_T ("CSIDL_COMPUTERSNEARME"),			},
	{ CSIDL_CONNECTIONS,				&FOLDERID_ConnectionsFolder,		_T ("CSIDL_CONNECTIONS"),				},
	{ CSIDL_CONTROLS,					&FOLDERID_ControlPanelFolder,		_T ("CSIDL_CONTROLS"),					},
	{ CSIDL_COOKIES,					&FOLDERID_Cookies,					_T ("CSIDL_COOKIES"),					},
	{ CSIDL_DESKTOP,					&FOLDERID_Desktop,					_T ("CSIDL_DESKTOP"),					},
	{ CSIDL_DESKTOPDIRECTORY,			&FOLDERID_Desktop,					_T ("CSIDL_DESKTOPDIRECTORY"),			},
	{ CSIDL_DRIVES,						&FOLDERID_ComputerFolder,			_T ("CSIDL_DRIVES"),					},
	{ CSIDL_FAVORITES,					&FOLDERID_Favorites,				_T ("CSIDL_FAVORITES"),					},
	{ CSIDL_FONTS,						&FOLDERID_Fonts,					_T ("CSIDL_FONTS"),						},
	{ CSIDL_HISTORY,					&FOLDERID_History,					_T ("CSIDL_HISTORY"),					},
	{ CSIDL_INTERNET,					&FOLDERID_InternetFolder,			_T ("CSIDL_INTERNET"),					},
	{ CSIDL_INTERNET_CACHE,				&FOLDERID_InternetCache,			_T ("CSIDL_INTERNET_CACHE"),			},
	{ CSIDL_LOCAL_APPDATA,				&FOLDERID_LocalAppData,				_T ("CSIDL_LOCAL_APPDATA"),				},
	{ CSIDL_MYDOCUMENTS,				&FOLDERID_Documents,				_T ("CSIDL_MYDOCUMENTS"),				},
	{ CSIDL_MYMUSIC,					&FOLDERID_Music,					_T ("CSIDL_MYMUSIC"),					},
	{ CSIDL_MYPICTURES,					&FOLDERID_Pictures,					_T ("CSIDL_MYPICTURES"),				},
	{ CSIDL_MYVIDEO,					&FOLDERID_Videos,					_T ("CSIDL_MYVIDEO"),					},
	{ CSIDL_NETHOOD,					&FOLDERID_NetHood,					_T ("CSIDL_NETHOOD"),					},
	{ CSIDL_NETWORK,					&FOLDERID_NetworkFolder,			_T ("CSIDL_NETWORK"),					},
	{ CSIDL_PERSONAL,					&FOLDERID_Documents,				_T ("CSIDL_PERSONAL"),					},
	{ CSIDL_PRINTERS,					&FOLDERID_PrintersFolder,			_T ("CSIDL_PRINTERS"),					},
	{ CSIDL_PRINTHOOD,					&FOLDERID_PrintHood,				_T ("CSIDL_PRINTHOOD"),					},
	{ CSIDL_PROFILE,					&FOLDERID_Profile,					_T ("CSIDL_PROFILE"),					},
	{ CSIDL_PROGRAM_FILES,				&FOLDERID_ProgramFiles,				_T ("CSIDL_PROGRAM_FILES"),				},
	{ CSIDL_PROGRAM_FILESX86,			&FOLDERID_ProgramFilesX86,			_T ("CSIDL_PROGRAM_FILESX86"),			},
	{ CSIDL_PROGRAM_FILES_COMMON,		&FOLDERID_ProgramFilesCommon,		_T ("CSIDL_PROGRAM_FILES_COMMON"),		},
	{ CSIDL_PROGRAM_FILES_COMMONX86,	&FOLDERID_ProgramFilesCommonX86,	_T ("CSIDL_PROGRAM_FILES_COMMONX86"),	},
	{ CSIDL_PROGRAMS,					&FOLDERID_Programs,					_T ("CSIDL_PROGRAMS"),					},
	{ CSIDL_RECENT,						&FOLDERID_Recent,					_T ("CSIDL_RECENT"),					},
	{ CSIDL_RESOURCES,					&FOLDERID_ResourceDir,				_T ("CSIDL_RESOURCES"),					},
	{ CSIDL_RESOURCES_LOCALIZED,		&FOLDERID_LocalizedResourcesDir,	_T ("CSIDL_RESOURCES_LOCALIZED"),		},
	{ CSIDL_SENDTO,						&FOLDERID_SendTo,					_T ("CSIDL_SENDTO"),					},
	{ CSIDL_STARTMENU,					&FOLDERID_StartMenu,				_T ("CSIDL_STARTMENU"),					},
	{ CSIDL_STARTUP,					&FOLDERID_Startup,					_T ("CSIDL_STARTUP"),					},
	{ CSIDL_SYSTEM,						&FOLDERID_System,					_T ("CSIDL_SYSTEM"),					},
	{ CSIDL_SYSTEMX86,					&FOLDERID_SystemX86,				_T ("CSIDL_SYSTEMX86"),					},
	{ CSIDL_TEMPLATES,					&FOLDERID_Templates,				_T ("CSIDL_TEMPLATES"),					},
	{ CSIDL_WINDOWS,					&FOLDERID_Windows,					_T ("CSIDL_WINDOWS"),					}, 
};

CString UTIL_API FoxjetCommon::GetCSIDLName (const KNOWNFOLDERID & k)
{
	for (int i = 0; i < ARRAYSIZE (foldermap); i++)
		if ((* foldermap [i].m_prfid) == k)
			return foldermap [i].m_lpsz;

	return _T ("[not found]");
}

CString UTIL_API FoxjetCommon::GetCSIDLName (int csidl)
{
	for (int i = 0; i < ARRAYSIZE (foldermap); i++)
		if (foldermap [i].m_csidl == csidl)
			return foldermap [i].m_lpsz;

	return _T ("[not found]");
}

int UTIL_API FoxjetCommon::GetCSIDL (const KNOWNFOLDERID & k)
{
	for (int i = 0; i < ARRAYSIZE (foldermap); i++)
		if ((* foldermap [i].m_prfid) == k)
			return foldermap [i].m_csidl;

	return 0;
}

CString UTIL_API FoxjetCommon::GetKnownFolderPath (const KNOWNFOLDERID & k)
{
	typedef HRESULT (WINAPI* lpSHGetKnownFolderPath)(REFKNOWNFOLDERID rfid,DWORD dwFlags,HANDLE hToken,PWSTR *ppszPath);

	CString strResult;

	{
		wchar_t * pwsz = 0;

		HMODULE hModule = LoadLibrary (_T ("shell32.dll"));
    
		if (lpSHGetKnownFolderPath pFct = (lpSHGetKnownFolderPath)GetProcAddress (hModule, "SHGetKnownFolderPath")) {
			HRESULT hResult = (* pFct) (k, 0, NULL, &pwsz);

			if (hResult != S_OK) {
				TRACEF (GetCSIDLName (k));
				TRACEF (_T ("SHGetKnownFolderPath: ") + ToString (hResult));
				TRACEF (FormatMessage (::GetLastError ()));
			}
		}

		strResult = pwsz;

		if (pwsz)
			::CoTaskMemFree (static_cast<void*>(pwsz));
	}

	if (strResult.IsEmpty ()) {
		TCHAR sz [MAX_PATH] = { 0 };
		int csidl = GetCSIDL (k);

		if (::SHGetSpecialFolderPath (NULL, sz, csidl, TRUE))
			strResult = sz;
	}

	if (strResult.IsEmpty ()) {
		if (k == FOLDERID_CommonStartup) 
			strResult = GetKnownFolderPath (FOLDERID_CommonPrograms) + _T ("\\Startup");
	}

	return strResult;
}

static FoxjetCommon::CVersion GetFileVersion (const CString & szVersionFile)
{
	FoxjetCommon::CVersion result (0, 0, 0, 0);
	DWORD  verHandle = 0;
	UINT   size      = 0;
	LPBYTE lpBuffer  = NULL;
	DWORD  verSize   = GetFileVersionInfoSize( szVersionFile, &verHandle);

	if (verSize != NULL)
	{
		LPSTR verData = new char[verSize];

		if (GetFileVersionInfo( szVersionFile, verHandle, verSize, verData))
		{
			if (VerQueryValue(verData,_T ("\\"),(VOID FAR* FAR*)&lpBuffer,&size))
			{
				if (size)
				{
					VS_FIXEDFILEINFO *verInfo = (VS_FIXEDFILEINFO *)lpBuffer;
					if (verInfo->dwSignature == 0xfeef04bd)
					{
						result = FoxjetCommon::CVersion (
							( verInfo->dwFileVersionMS >> 16 ) & 0xffff,
							( verInfo->dwFileVersionMS >>  0 ) & 0xffff,
							( verInfo->dwFileVersionLS >> 16 ) & 0xffff,
							0,
							( verInfo->dwFileVersionLS >>  0 ) & 0xffff);
					}
				}
			}
		}

		delete[] verData;
	}

	return result;
}

void UTIL_API FoxjetCommon::CheckDllVersions ()
{
	std::wstring strDLL = 
		_T ("3d.dll;")
		//_T ("Auxiliary.dll;")
		_T ("Database.dll;")
		_T ("Debug.dll;")
		_T ("Document.dll;")
		_T ("ElementList.dll;")
		_T ("IpElementList.dll;")
		_T ("IvPhcApi.dll;")
		_T ("kb.dll;")
		_T ("MphcApi.dll;")
		//_T ("NiceLabel.dll;")
		_T ("Raster.dll;")
		//_T ("SPDevice.dll;")
		_T ("Utils.dll;");
	std::vector <std::wstring> vDLL = explode (strDLL, ';');
	TCHAR szExePath [MAX_PATH] = { 0 };
	CString strHomeDir = GetHomeDir ();
	CStringArray vDependencies;

	::GetModuleFileName (NULL, szExePath, ARRAYSIZE (szExePath));

	//_tcscpy (szExePath, _T ("C:\\Program Files (x86)\\Foxjet\\MarksmanELITE\\control.exe"));
	TRACEF (szExePath);

	GetDependencies (szExePath, vDependencies, false);

	for (int i = 0; i < vDependencies.GetSize (); i++)
		vDependencies [i] = strHomeDir + _T ("\\") + vDependencies [i];

	for (int i = 0; i < vDLL.size (); i++)
		vDLL [i] = std::wstring (strHomeDir + _T ("\\")) + vDLL [i];

	TRACEF (_T ("dependencies: \n\t") + implode (vDependencies, _T ("\n\t")));
	TRACEF (_T ("dlls: \n\t") + implode (vDLL, (std::wstring)_T ("\n\t")));
	
	for (int i = 0; i < vDependencies.GetSize (); i++) {
		if (Find (vDLL, (std::wstring)(LPCTSTR)vDependencies [i], false) != -1) {
			CVersion ver = GetFileVersion (vDependencies [i]);

			TRACEF (ver.Format () + _T (": ") + vDependencies [i]);

			if (ver != ::verApp) {
				CString str = LoadString (IDS_MISMATCHEDVERSIONS) + _T (":\n") +
					CVersion (::verApp).Format () + _T (": ") + (CString)szExePath + _T ("\n") +
					ver.Format () + _T (": ") + vDependencies [i] + _T ("\n\n") +
					LoadString (IDS_REINSTALL);
				MsgBox (str, _T (""), MB_ICONERROR);
				exit (0);
			}
		}
	}
}

UTIL_API std::wstring FoxjetCommon::gb2312(const std::string& str)
{
	std::wstring convertedString;
	int requiredSize = MultiByteToWideChar(936, 0, str.c_str(), -1, 0, 0);
	if (requiredSize > 0)
	{
		std::vector<wchar_t> buffer(requiredSize);
		MultiByteToWideChar(936, 0, str.c_str(), -1, &buffer[0], requiredSize);
		convertedString.assign(buffer.begin(), buffer.end() - 1);
	}

	return convertedString;
}
