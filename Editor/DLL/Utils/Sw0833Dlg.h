#if !defined(AFX_SW0833DLG_H__0FBBBFC4_117B_49D0_BE49_519AA2A93719__INCLUDED_)
#define AFX_SW0833DLG_H__0FBBBFC4_117B_49D0_BE49_519AA2A93719__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Sw0833Dlg.h : header file
//

#include "OdbcDatabase.h"
#include "DbTypeDefs.h"

/////////////////////////////////////////////////////////////////////////////
// CSw0833Dlg dialog

namespace FoxjetUtils
{
	class UTIL_API CSw0833Dlg : public CPropertyPage
	{
		//DECLARE_DYNCREATE(CSw0833Dlg)

	// Construction
	public:
		CSw0833Dlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection);
		~CSw0833Dlg();

		const CString m_strRegSection;
		FoxjetDatabase::COdbcDatabase & m_db;

	// Dialog Data
		//{{AFX_DATA(CSw0833Dlg)
		BOOL	m_bContinuousPC;
		//}}AFX_DATA


	// Overrides
		// ClassWizard generate virtual function overrides
		//{{AFX_VIRTUAL(CSw0833Dlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		// Generated message map functions
		//{{AFX_MSG(CSw0833Dlg)
			// NOTE: the ClassWizard will add member functions here
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()

		virtual BOOL OnInitDialog ();

		afx_msg void OnReset ();
		afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

		HBRUSH m_hbrDlg;

	};
}; // namespace FoxjetUtils

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SW0833DLG_H__0FBBBFC4_117B_49D0_BE49_519AA2A93719__INCLUDED_)
