#ifndef __BWSYSTEM_H__
#define __BWSYSTEM_H__

//#include "List.h"
#include "UtilApi.h"
#include "Version.h"
#include "OdbcDatabase.h"
#include "TemplExt.h"

namespace FoxjetCommon
{
	typedef void (CALLBACK * LPFORMATDATETIME) (CString & str);

	CString UTIL_API GetDefTimeFormat (int nRow);
	bool UTIL_API CALLBACK OnSystemConfig (CWnd * pParent, const CString & strRegSection, FoxjetDatabase::COdbcDatabase & db, LPFORMATDATETIME lpfct, CCopyArray <DWORD, DWORD> & vCommChanged);
}; //namespace Foxjet3d

#endif //__BWSYSTEM_H__
