// RawBitmap.h: interface for the CRawBitmap class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RAWBITMAP_H__057929C9_7918_4D52_96A6_97EA6E74CC29__INCLUDED_)
#define AFX_RAWBITMAP_H__057929C9_7918_4D52_96A6_97EA6E74CC29__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "UtilApi.h"

namespace FoxjetCommon 
{
	class UTIL_API CxBitmap  
	{
	public:
		CxBitmap (HBITMAP h);
		~CxBitmap ();

		operator HBITMAP () { return m_hBitmap; }
		operator HGDIOBJ () { return (HGDIOBJ)m_hBitmap; }

		HBITMAP m_hBitmap;
	};

	class UTIL_API CRawBitmap  
	{
	public:
		CRawBitmap ();
		CRawBitmap (HBITMAP hbmp);
		CRawBitmap (const CRawBitmap & rhs);
		CRawBitmap & operator = (const CRawBitmap & rhs);
		virtual ~CRawBitmap();

		void Free ();
		bool Load (HBITMAP hbmp);
		bool Save (LPCTSTR lpszFilePath, LPCSTR lpszSrcFile = __FILE__, ULONG lSrcLine = __LINE__);
		bool GetBitmap (CBitmap & bmp);
		bool GetBitmap (BITMAP & bm);
		ULONG CountPixels () const;
		HBITMAP GetHBITMAP () const;
		CSize GetSize () const;
		const PUCHAR GetBuffer () { return m_pBuffer; }
		ULONG GetLength () const { return m_lLen; }

	protected:
		PUCHAR m_pBuffer;
		ULONG m_lLen;
		BITMAP m_bm;
	};
}; //namespace FoxjetCommon

#define SAVERAWBITMAP(p,s)		((p).Save (s, _T (__FILE__), __LINE__))

#endif // !defined(AFX_RAWBITMAP_H__057929C9_7918_4D52_96A6_97EA6E74CC29__INCLUDED_)
