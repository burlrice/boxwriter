#include "stdafx.h"
#include "Utils.h"
#include "Debug.h"
#include "Convert.h"
#include "TemplExt.h"
#include "Database.h"
#include "Parse.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetUtils;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace Conversion;

static const CVersion verDLL = GetElementListVersion ();

// when new element formats are introduced, the 'major' version number should change
//	void CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer)
static const CVersion versions [] = 
{
	CVersion (0, 3, 0, 0, 0),	// Win
	CVersion (0, 3, 0, 0, 1),	// IP
};
static const CVersion & ver03000 = versions [0]; // Win
static const CVersion & ver03001 = versions [1];  // IP

UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszElementType		= _T ("ElementType");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszID				= _T ("ID"); 
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszX				= _T ("x");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszY				= _T ("y");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszFlipH			= _T ("flip h");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszFlipV			= _T ("flip v");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszInverse			= _T ("inverse");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszFontName		= _T ("font name");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszFontSize		= _T ("font size");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszBold			= _T ("bold");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszItalic			= _T ("italic");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszOrient			= _T ("orientation");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszData			= _T ("data");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszFileName		= _T ("file name");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszMaxChars		= _T ("max chars");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszPrompt			= _T ("prompt");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszPromptAtStart	= _T ("prompt at task start");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszCount			= _T ("count");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszCountMin		= _T ("count min");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszCountMax		= _T ("count max");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszCountInc		= _T ("count increment");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszWidth			= _T ("width");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszHeight			= _T ("height");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszPalCount		= _T ("pal count");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszPalCountMin		= _T ("pal count min");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszPalCountMax		= _T ("pal count max");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszPalCountInc		= _T ("pal count inc");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszLeadingZeros	= _T ("leading zeros");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszDigits			= _T ("digits");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszPalCountEnabled	= _T ("pal count enabled");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszDays			= _T ("days");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszHours			= _T ("hours");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszMinutes			= _T ("minutes");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszSeconds			= _T ("seconds");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszDSN				= _T ("DSN");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszTable			= _T ("Table");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszField			= _T ("Field");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszKeyField		= _T ("KeyField");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszKeyValue		= _T ("KeyValue");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszRow				= _T ("Row");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszSQLb			= _T ("bSQL");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszSQLstr			= _T ("strSQL");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszIndex			= _T ("Index");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszLength			= _T ("Length");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszTag				= _T ("Tag");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszPeriod			= _T ("Period");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszRound			= _T ("Round");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszBatchQuantity	= _T ("Batch quantity");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszRollover		= _T ("Rollover");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszParaAlign		= _T ("Paragraph alignment");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszParaGap			= _T ("Paragraph gap");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszParaWidth		= _T ("Paragraph width");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszResizable		= _T ("Resizable");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszDefault			= _T ("default");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszSQLType			= _T ("SQL type");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszSerial			= _T ("Serial lookup");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszPort			= _T ("Serial download port");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszMaster			= _T ("Master");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszLink			= _T ("Linked to (barcode)");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszAlign			= _T ("Alignment");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszUserPrompt		= _T ("user prompted");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszPromptAtTaskStart = _T ("task start");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszTableIndex		= _T ("table index");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszDither			= _T ("Dither");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszLockAspectRatio	= _T ("Lock aspect ratio");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszColor			= _T ("Color");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszHoldStartDate	= _T ("Hold start date");


UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszIPWidth			= _T ("width (ip)");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszIPGap			= _T ("Gap (ip)");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszPosition		= _T ("Position");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszSysBCIndex		= _T ("SysBCIndex");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszHROutput		= _T ("HROutput");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszHRAlign			= _T ("HRAlign");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszHRBold			= _T ("HRBold");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszChange			= _T ("Change");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszRepeat			= _T ("Repeat");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszStart			= _T ("Start");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszLimit			= _T ("Limit");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszAlignChar		= _T ("AlignChar");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszFillChar		= _T ("FillChar");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszFormat			= _T ("Format");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszFormatType		= _T ("FormatType");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszDelimChar		= _T ("DelimChar");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszStringNormal	= _T ("StringNormal");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszHourACount		= _T ("HourACount");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszTimeType		= _T ("TimeType");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszName			= _T ("Name");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszDynFirstChar	= _T ("DynFirstChar");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszDynNumChar		= _T ("DynNumChar");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszDynIndex		= _T ("DynIndex");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszEmptyString		= _T ("Empty string");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszThicknessCX		= _T ("thickness.x");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszThicknessCY		= _T ("thickness.y");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszType			= _T ("Type");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszMaxWidthEnable	= _T ("MaxWidthEnable");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszMaxWidth		= _T ("MaxWidth");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszDatabaseLookup	= _T ("DB Lookup");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszBmpWidth		= _T ("Bitmap width");
UTIL_API LPCTSTR FoxjetCommon::ElementFields::m_lpszBmpHeight		= _T ("Bitmap height");

static LPCTSTR lpszTypes [][2] =
{	// win						IP
	{	_T ("Text"),			_T ("Text"),		},	// [0]
	{	_T ("Bitmap"),			_T ("Bitmap"),		},	// [1]
	{	_T ("Count"),			_T ("Counter"),		},	// [2]
	{	_T ("Datetime"),		_T ("Datetime"),	},	// [3]
	{	_T ("ExpDate"),			NULL,				},	// [4]
	{	_T ("Shift"),			NULL,				},	// [5]
	{	_T ("User"),			_T ("User"),		},	// [6]
	{	_T ("Barcode"),			_T ("Barcode"),		},	// [7]
	{	_T ("Database"),		NULL,				},	// [8]
	{	_T ("Serial"),			NULL,				},	// [9]
};
static const int nTypeCount	= sizeof (lpszTypes) / sizeof (lpszTypes [0]);
static const int nWinIndex	= 0;
static const int nIpIndex	= 1;

typedef enum 
{
	INDEX_TEXT = 0,
	INDEX_BITMAP,
	INDEX_COUNT,
	INDEX_DATETIME,
	INDEX_EXPDATE,
	INDEX_SHIFT,
	INDEX_USER,
	INDEX_BARCODE,
	INDEX_DATABASE,
	INDEX_SERIAL,
} TYPEINDEX;
	
struct
{
	LPCTSTR				m_lpszType;
	const CVersion *	m_lpverFrom;
	const CVersion *	m_lpverTo;
} static const exclude [] = 
{
	// table of elements to be specifically excluded from conversion
	{ ::lpszTypes [INDEX_BARCODE][0],		&ver03000,	&ver03001 },
};

static class CConversionMap
{
public:
	CConversionMap ();
	virtual ~CConversionMap ();

	static int GetTypeIndex (const CVersion & ver);
	static bool HasSynonym (const CVersion & verFrom, const CVersion & verTo);
	static CString GetSynonym (const CString & strFrom, const CVersion & verFrom, const CVersion & verTo);
	static bool IsExcluded (const CString & strType, const CVersion & verFrom, const CVersion & verTo);

	const CConversion * GetConversion (const CString & str, const FoxjetCommon::CVersion & ver) const;

	CArray <CConversion * , CConversion *> m_vMap;
} const map;

CConversionMap::CConversionMap ()
{
	using namespace FoxjetCommon::ElementFields;

	const CField fldX				(m_lpszX,						CField::FT_LONG,	_T ("0"));
	const CField fldY				(m_lpszY,						CField::FT_LONG,	_T ("0"));
	const CField fldMaxChars		(m_lpszMaxChars,				CField::FT_LONG,	_T ("15"));
	const CField fldPromptString	(m_lpszPrompt,					CField::FT_STRING,	_T ("Enter user data")); 
	const CField fldPromptFlag		(m_lpszPromptAtStart,			CField::FT_BOOL,	_T ("1")); 
	const CField fldCount			(m_lpszCount,					CField::FT_LONG,	_T ("1"));
	const CField fldCountMin		(m_lpszCountMin,				CField::FT_LONG,	_T ("0"));
	const CField fldCountMax		(m_lpszCountMax,				CField::FT_LONG,	_T ("999999"));
	const CField fldCountInc		(m_lpszCountInc,				CField::FT_LONG,	_T ("1"));

	{ // Win 
		CVersionArray vVer;
		CFieldArray vFields;
		const CField fldID			(m_lpszID,			CField::FT_LONG,	_T ("0"));
		const CField fldFilename	(m_lpszFileName,	CField::FT_STRING,	_T ("Def.bmp"));
		const CField fldData		(m_lpszData,		CField::FT_STRING);			
		const CField fldFlipH		(m_lpszFlipH,		CField::FT_BOOL,	_T ("0"));
		const CField fldFlipV		(m_lpszFlipV,		CField::FT_BOOL,	_T ("0"));
		const CField fldInverse		(m_lpszInverse,		CField::FT_BOOL,	_T ("0"));
		const CField fldFontName	(m_lpszFontName,	CField::FT_FONT,	_T ("Courier New"));
		const CField fldFontSize	(m_lpszFontSize,	CField::FT_LONG,	_T ("30"));
		const CField fldBold		(m_lpszBold,		CField::FT_BOOL,	_T ("0"));
		const CField fldItalic		(m_lpszItalic,		CField::FT_BOOL,	_T ("0"));
		const CField fldOrient		(m_lpszOrient,		CField::FT_LONG,	_T ("0"));
		const CField fldFontWidth	(m_lpszWidth,		CField::FT_LONG,	_T ("0"));
	
		vVer.Add (CVersion (::ver03000));

		// CBarcodeElement is intentionally excluded (too complicated)

		vFields.RemoveAll (); 
		vFields.Add (CField (lpszTypes [INDEX_BITMAP][::nWinIndex]));
		vFields.Add (CField (fldID));								
		vFields.Add (CField (fldX));									
		vFields.Add (CField (fldY));									
		vFields.Add (CField (m_lpszWidth,								CField::FT_LONG,	_T ("0")));
		vFields.Add (CField (m_lpszHeight,								CField::FT_LONG,	_T ("0")));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (fldFilename));
		m_vMap.Add (new CConversion (vFields, vVer));

		vFields.RemoveAll ();
		vFields.Add (CField (lpszTypes [INDEX_COUNT][::nWinIndex]));
		vFields.Add (CField (fldID));
		vFields.Add (CField (fldX));
		vFields.Add (CField (fldY));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (fldFontName));
		vFields.Add (CField (fldFontSize));
		vFields.Add (CField (fldBold));
		vFields.Add (CField (fldItalic));
		vFields.Add (CField (fldCount));
		vFields.Add (CField (fldCountMin));
		vFields.Add (CField (fldCountMax));
		vFields.Add (CField (fldCountInc));
		vFields.Add (CField (m_lpszPalCount,							CField::FT_LONG,	_T ("1")));
		vFields.Add (CField (m_lpszPalCountMin,							CField::FT_LONG,	_T ("0")));
		vFields.Add (CField (m_lpszPalCountMax,							CField::FT_LONG,	_T ("999")));
		vFields.Add (CField (m_lpszPalCountInc,							CField::FT_LONG,	_T ("1")));
		vFields.Add (CField (m_lpszLeadingZeros,						CField::FT_BOOL,	_T ("1")));
		vFields.Add (CField (m_lpszDigits,								CField::FT_LONG,	_T ("6")));
		vFields.Add (CField (m_lpszPalCountEnabled,						CField::FT_BOOL,	_T ("0")));
		vFields.Add (CField (fldOrient));
		vFields.Add (CField (fldFontWidth));
		m_vMap.Add (new CConversion (vFields, vVer));

		vFields.RemoveAll ();
		vFields.Add (CField (lpszTypes [INDEX_DATETIME][::nWinIndex]));
		vFields.Add (CField (fldID));
		vFields.Add (CField (fldX));
		vFields.Add (CField (fldY));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (fldFontName));
		vFields.Add (CField (fldFontSize));
		vFields.Add (CField (fldBold));
		vFields.Add (CField (fldItalic));
		vFields.Add (CField (fldData, _T ("%H:%M:%S")));
		vFields.Add (CField (fldOrient));
		vFields.Add (CField (fldFontWidth));
		m_vMap.Add (new CConversion (vFields, vVer));

		vFields.RemoveAll ();
		vFields.Add (CField (lpszTypes [INDEX_EXPDATE][::nWinIndex]));
		vFields.Add (CField (fldID));
		vFields.Add (CField (fldX));
		vFields.Add (CField (fldY));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (fldFontName));
		vFields.Add (CField (fldFontSize));
		vFields.Add (CField (fldBold));
		vFields.Add (CField (fldItalic));
		vFields.Add (CField (fldData, _T ("%H:%M:%S")));
		vFields.Add (CField (m_lpszDays,								CField::FT_LONG,	_T ("30")));
		vFields.Add (CField (m_lpszHours,								CField::FT_LONG,	_T ("0")));
		vFields.Add (CField (m_lpszMinutes,								CField::FT_LONG,	_T ("0")));
		vFields.Add (CField (m_lpszSeconds,								CField::FT_LONG,	_T ("0")));
		vFields.Add (CField (fldOrient));
		vFields.Add (CField (fldFontWidth));
		m_vMap.Add (new CConversion (vFields, vVer));

		vFields.RemoveAll ();
		vFields.Add (CField (lpszTypes [INDEX_SHIFT][::nWinIndex]));
		vFields.Add (CField (fldID));
		vFields.Add (CField (fldX));
		vFields.Add (CField (fldY));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (fldFontName));
		vFields.Add (CField (fldFontSize));
		vFields.Add (CField (fldBold));
		vFields.Add (CField (fldItalic));
		vFields.Add (CField (fldOrient));
		vFields.Add (CField (fldFontWidth));
		m_vMap.Add (new CConversion (vFields, vVer));

		vFields.RemoveAll ();
		vFields.Add (CField (lpszTypes [INDEX_TEXT][::nWinIndex]));
		vFields.Add (CField (fldID));
		vFields.Add (CField (fldX));
		vFields.Add (CField (fldY));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (fldFontName));
		vFields.Add (CField (fldFontSize));
		vFields.Add (CField (fldBold));
		vFields.Add (CField (fldItalic));
		vFields.Add (CField (fldData, _T ("Text Element")));
		vFields.Add (CField (m_lpszWidth,						CField::FT_LONG,	_T ("0")));
		vFields.Add (CField (fldOrient));
		vFields.Add (CField (fldFontWidth));
		m_vMap.Add (new CConversion (vFields, vVer));

		vFields.RemoveAll ();
		vFields.Add (CField (lpszTypes [INDEX_USER][::nWinIndex]));
		vFields.Add (CField (fldID));
		vFields.Add (CField (fldX));
		vFields.Add (CField (fldY));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (fldFontName));
		vFields.Add (CField (fldFontSize));
		vFields.Add (CField (fldBold));
		vFields.Add (CField (fldItalic));
		vFields.Add (CField (fldData, _T ("User element")));
		vFields.Add (CField (fldMaxChars));
		vFields.Add (CField (fldPromptString));
		vFields.Add (CField (fldPromptFlag));
		vFields.Add (CField (fldOrient));
		vFields.Add (CField (fldFontWidth));
		m_vMap.Add (new CConversion (vFields, vVer));

		vFields.RemoveAll ();
		vFields.Add (CField (lpszTypes [INDEX_DATABASE][::nWinIndex]));
		vFields.Add (CField (fldID));
		vFields.Add (CField (fldX));
		vFields.Add (CField (fldY));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (fldFontName));
		vFields.Add (CField (fldFontSize));
		vFields.Add (CField (fldBold));
		vFields.Add (CField (fldItalic));
		vFields.Add (CField (fldData, m_lpszDSN));
		vFields.Add (CField (fldData, m_lpszTable));
		vFields.Add (CField (fldData, m_lpszField));
		vFields.Add (CField (fldData, m_lpszKeyField));
		vFields.Add (CField (fldData, m_lpszKeyValue));
		vFields.Add (CField (fldData, m_lpszRow));
		vFields.Add (CField (fldData, m_lpszSQLb));
		vFields.Add (CField (fldData, m_lpszSQLstr));
		vFields.Add (CField (fldOrient));
		vFields.Add (CField (fldFontWidth));
		m_vMap.Add (new CConversion (vFields, vVer));

		vFields.RemoveAll ();
		vFields.Add (CField (lpszTypes [INDEX_SERIAL][::nWinIndex]));
		vFields.Add (CField (fldID));
		vFields.Add (CField (fldX));
		vFields.Add (CField (fldY));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (fldFontName));
		vFields.Add (CField (fldFontSize));
		vFields.Add (CField (fldBold));
		vFields.Add (CField (fldItalic));
		vFields.Add (CField (fldData, m_lpszIndex));
		vFields.Add (CField (fldData, m_lpszLength));
		vFields.Add (CField (fldOrient));
		vFields.Add (CField (fldFontWidth));
		m_vMap.Add (new CConversion (vFields, vVer));
	}


	{ // IP
		CVersionArray vVer;
		CFieldArray vFields;
		const CField fldID			(m_lpszID,				CField::FT_STRING_DELIMITED,	_T ("\"0\""));
		const CField fldFilename	(m_lpszFileName,		CField::FT_STRING_DELIMITED,	_T ("\"Def.bmp\""));
		const CField fldData		(m_lpszData,			CField::FT_STRING_DELIMITED);			
		const CField fldPosition	(m_lpszPosition,		CField::FT_STRING,	_T ("D"));
		const CField fldBold		(m_lpszBold,			CField::FT_LONG,	_T ("0"));
		const CField fldWidth		(m_lpszIPWidth,			CField::FT_LONG,	_T ("1"));
		const CField fldGap			(m_lpszIPGap,			CField::FT_LONG,	_T ("0"));
		const CField fldFlipH		(m_lpszFlipH,			CField::FT_BOOL_IP,	_T ("F"));
		const CField fldFlipV		(m_lpszFlipV,			CField::FT_BOOL_IP,	_T ("F"));
		const CField fldInverse		(m_lpszInverse,			CField::FT_BOOL_IP,	_T ("F"));
		const CField fldFont		(m_lpszFontName,		CField::FT_FONT_IP,	_T ("us32x32"));
		const CField fldLength		(m_lpszLength,			CField::FT_LONG,	_T ("0"));
		const CField fldAlign		(m_lpszAlignChar,		CField::FT_LONG,	_T ("R"));
		const CField fldFillChar	(m_lpszFillChar,		CField::FT_LONG);

		
		vVer.Add (CVersion (::ver03001));

		// NOTE:	the only case in which a field name in any of these blocks should 
		//			not be one of the 'const CField' objects above is ONLY if it can't 
		//			be mapped to another implementation.  for instance, a windows bitmap's
		//			width and height members cannot be mapped to an IP bitmap's members.

		vFields.RemoveAll ();
		vFields.Add (CField (lpszTypes [INDEX_TEXT][::nIpIndex]));
		vFields.Add (CField (fldID));
		vFields.Add (CField (fldX));
		vFields.Add (CField (fldY));
		vFields.Add (CField (fldPosition)); 
		vFields.Add (CField (fldBold)); 
		vFields.Add (CField (fldWidth)); 
		vFields.Add (CField (fldGap));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (fldFont));
		vFields.Add (CField (fldData, _T ("Text element")));
		m_vMap.Add (new CConversion (vFields, vVer));

		vFields.RemoveAll ();
		vFields.Add (CField (lpszTypes [INDEX_BITMAP][::nIpIndex]));
		vFields.Add (CField (fldID));
		vFields.Add (CField (fldX));
		vFields.Add (CField (fldY));
		vFields.Add (CField (fldPosition));
		vFields.Add (CField (fldBold));
		vFields.Add (CField (fldWidth));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (fldFilename));
		m_vMap.Add (new CConversion (vFields, vVer));

		vFields.RemoveAll ();
		vFields.Add (CField (lpszTypes [INDEX_BARCODE][::nIpIndex]));
		vFields.Add (CField (fldID));
		vFields.Add (CField (fldX));
		vFields.Add (CField (fldY));
		vFields.Add (CField (fldPosition));
		vFields.Add (CField (fldBold));
		vFields.Add (CField (fldWidth));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (m_lpszSysBCIndex,		CField::FT_LONG,	_T ("0")));
		vFields.Add (CField (m_lpszHROutput,		CField::FT_BOOL_IP,	_T ("T")));
		vFields.Add (CField (m_lpszHRBold,			CField::FT_LONG,	_T ("0")));
		m_vMap.Add (new CConversion (vFields, vVer));

		vFields.RemoveAll ();
		vFields.Add (CField (lpszTypes [INDEX_COUNT][::nIpIndex]));
		vFields.Add (CField (fldID));
		vFields.Add (CField (fldX));
		vFields.Add (CField (fldY));
		vFields.Add (CField (fldPosition));
		vFields.Add (CField (fldBold));
		vFields.Add (CField (fldWidth));
		vFields.Add (CField (fldGap));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (fldCountInc));
		vFields.Add (CField (m_lpszRepeat,		CField::FT_LONG,	_T ("1")));
		vFields.Add (CField (fldCountMin));
		vFields.Add (CField (fldCountMax));
		vFields.Add (CField (fldLength));
		vFields.Add (CField (fldAlign));
		vFields.Add (CField (fldFillChar));
		vFields.Add (CField (fldFont));
		m_vMap.Add (new CConversion (vFields, vVer));

		vFields.RemoveAll ();
		vFields.Add (CField (lpszTypes [INDEX_DATETIME][::nIpIndex]));
		vFields.Add (CField (fldID));
		vFields.Add (CField (fldX));
		vFields.Add (CField (fldY));
		vFields.Add (CField (fldPosition));
		vFields.Add (CField (fldBold));
		vFields.Add (CField (fldWidth));
		vFields.Add (CField (fldGap));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (m_lpszFormat,			CField::FT_LONG,	_T ("1")));
		vFields.Add (CField (m_lpszTimeType,		CField::FT_LONG,	_T ("0")));
		vFields.Add (CField (fldLength));			
		vFields.Add (CField (fldAlign));			
		vFields.Add (CField (fldFillChar));			
		vFields.Add (CField (m_lpszHourACount,		CField::FT_LONG,	_T ("24")));
		vFields.Add (CField (fldFont));
		m_vMap.Add (new CConversion (vFields, vVer));

		vFields.RemoveAll ();
		vFields.Add (CField (lpszTypes [INDEX_USER][::nIpIndex]));
		vFields.Add (CField (fldID));
		vFields.Add (CField (fldX));
		vFields.Add (CField (fldY));
		vFields.Add (CField (fldFlipH));
		vFields.Add (CField (fldFlipV));
		vFields.Add (CField (fldInverse));
		vFields.Add (CField (fldFont));
		vFields.Add (CField (fldBold));
		vFields.Add (CField (fldWidth));
		vFields.Add (CField (fldGap));
		vFields.Add (CField (CField (m_lpszData, CField::FT_STRING, _T ("User element"))));
		vFields.Add (CField (fldMaxChars));
		vFields.Add (CField (fldPromptString));
		vFields.Add (CField (fldPromptFlag));
		m_vMap.Add (new CConversion (vFields, vVer));

	}

}

CConversionMap::~CConversionMap ()
{
	for (int i = 0; i < m_vMap.GetSize (); i++) {
		CConversion * p = m_vMap [i];
		delete p;
	}

	m_vMap.RemoveAll ();
}

int CConversionMap::GetTypeIndex (const CVersion & ver)
{
	struct
	{
		const CVersion	*	m_pVer;
		int					m_nIndex;
	} map [] = 
	{
		{ &(::ver03000),	nWinIndex	},
		{ &(::ver03001),	nIpIndex	},
	};
	int nCount = sizeof (map) / sizeof (map [0]);

	for (int i = 0; i < nCount; i++) {
		ASSERT (map [i].m_pVer);

		if (* (map [i].m_pVer) == ver)
			return map [i].m_nIndex;
	}

	return 0;
}

bool CConversionMap::HasSynonym (const CVersion & verFrom, const CVersion & verTo)
{
	int nFrom = map.GetTypeIndex (verFrom);
	int nTo = map.GetTypeIndex (verTo);

	return nFrom != nTo;
}

CString CConversionMap::GetSynonym (const CString & strFrom, const CVersion & verFrom, const CVersion & verTo)
{
	int nFrom = map.GetTypeIndex (verFrom);
	int nTo = map.GetTypeIndex (verTo);

	ASSERT (nFrom != nTo);

	for (int i = 0; i < ::nTypeCount; i++) {
		CString strTmp (::lpszTypes [i][nFrom]);

		if (!strTmp.CompareNoCase (strFrom))
			return ::lpszTypes [i][nTo];
	}

	return _T ("");
}

bool CConversionMap::IsExcluded (const CString & strType, const CVersion & verFrom, const CVersion & verTo)
{
	int nSize = sizeof (exclude) / sizeof (exclude [0]);

	for (int i = 0; i < nSize; i++) {
		const CVersion * pFrom = ::exclude [i].m_lpverFrom;
		const CVersion * pTo = ::exclude [i].m_lpverTo;

		ASSERT (pFrom);
		ASSERT (pTo);

		const CVersion & to = * pTo;
		const CVersion & from = * pFrom;

		if (::exclude [i].m_lpszType == strType)
			if ((to == verTo && from == verFrom) || (from == verTo && to == verFrom))
				return true;
	}

	return false;
}

const CConversion * CConversionMap::GetConversion (const CString & str, const CVersion & ver) const
{
	int nIndex = map.GetTypeIndex (ver);

	for (int i = 0; i < m_vMap.GetSize (); i++) {
		const CConversion * p = m_vMap [i];

		if (!str.CompareNoCase (p->GetElementType ()) && p->Contains (ver))
			return p;
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

UTIL_API CString FoxjetCommon::Convert (const CString & str, const FoxjetCommon::CVersion & verFrom)
{
	const CVersion & verTo = ::verDLL;

	return Convert (str, verFrom, verTo, false);
}


UTIL_API CString FoxjetCommon::Convert (const CString & str, const FoxjetCommon::CVersion & verFrom,
										const FoxjetCommon::CVersion & verTo, bool bMakePacket)
{
	if (!verFrom.IsSynonym (verTo)) {
		CStringArray v;

		Tokenize (str, v);

		if (v.GetSize ()) {
			CString strType = v [0];
			CString strFrom = strType, strTo = strType;

			if (map.IsExcluded (strType, verFrom, verTo)) 
				return _T ("");

			if (map.HasSynonym (verFrom, verTo))
				strTo = map.GetSynonym (strFrom, verFrom, verTo);

			const CConversion * pFrom = map.GetConversion (strFrom, verFrom);
			const CConversion * pTo = map.GetConversion (strTo, verTo);

			if (pFrom && pTo) 
				return pFrom->Convert (v, * pTo, bMakePacket);
		}
	}

	return str;
}

UTIL_API void FoxjetCommon::EnumVersions (CArray <FoxjetCommon::CVersion, const FoxjetCommon::CVersion &> & v)
{
	int nCount = sizeof (::versions) / sizeof (::versions [0]);

	for (int i = 0; i < nCount; i++)
		v.Add (::versions [i]);
}


////////////////////////////////////////////////////////////////////////////////

CConversion::CConversion (const Conversion::CFieldArray & vFields, const CVersionArray & vVer)
{
	Copy (m_vFields, * (Conversion::CFieldArray *)(LPVOID)&vFields);
	Copy (m_vVer, * (CVersionArray *)(LPVOID)&vVer);
}

CConversion::CConversion (const CConversion & rhs)
{
	Copy (m_vFields, * (Conversion::CFieldArray *)(LPVOID)&rhs.m_vFields);
	Copy (m_vVer, * (CVersionArray *)(LPVOID)&rhs.m_vVer);
}

CConversion & CConversion::operator = (const CConversion & rhs)
{
	if (this != &rhs) {
		m_vFields.RemoveAll ();
		m_vVer.RemoveAll ();

		Copy (m_vFields, * (Conversion::CFieldArray *)(LPVOID)&rhs.m_vFields);
		Copy (m_vVer, * (CVersionArray *)(LPVOID)&rhs.m_vVer);
	}

	return * this;
}

CConversion::~CConversion ()
{
}

bool CConversion::Contains (const FoxjetCommon::CVersion & ver) const
{
	for (int i = 0; i < m_vVer.GetSize (); i++) 
		if (ver.IsSynonym (m_vVer [i]))
			return true;

	return false;
}


CString CConversion::GetElementType () const
{
	if (m_vFields.GetSize ()) 
		return m_vFields [0].GetName ();

	ASSERT (0);
	return _T ("");
}

CString CConversion::Convert (const CStringArray & v, const CConversion & to, bool bMakePacket) const
{
	CString str;
	CStringArray vResult;

	vResult.Add (to.GetElementType ());

	for (int i = 1; i < to.GetFields ().GetSize (); i++) 
		vResult.Add (_T (""));

	/* TODO: rem
	CString strTrace;
	strTrace.Format ("[%-15s][%-10s] --> [%-15s][%-10s]", GetElementType (), "", to.GetElementType (), "");
	TRACEF (strTrace);
	*/

	for (int i = 1; i < to.GetFields ().GetSize (); i++) {
		const CField & fldTo = to.GetFields ().GetAt (i);
		CString strField = fldTo.GetName ();
		int nIndex = Find (strField);
		const CField fldFrom = GetField (nIndex);

		if (nIndex > 0 && nIndex < v.GetSize ()) 
			vResult.SetAt (i, fldFrom.Convert (v [nIndex], fldTo));
		else
			vResult.SetAt (i, fldTo.GetDefault ());

		/* TODO: rem
		CString strFrom = (nIndex > 0 && nIndex < v.GetSize ()) ? v [nIndex]: "";
		CString strTo = vResult [i];
		strTrace.Format ("[%-15s][%-10s] --> [%-15s][%-10s]", 
			fldFrom.GetName (), strFrom,
			fldTo.GetName (), strTo);
		TRACEF (strTrace);
		*/

	}

	for (int i = 0; i < vResult.GetSize (); i++) {
		str += vResult [i];

		if (i < (vResult.GetSize () - 1))
			str += ',';
	}

	if (bMakePacket)
		str = '{' + str + '}';

	return str;
}

const Conversion::CFieldArray & CConversion::GetFields () const
{
	return m_vFields;
}

int CConversion::Find (const CString & strField) const
{
	for (int i = 0; i < m_vFields.GetSize (); i++) 
		if (!strField.CompareNoCase (m_vFields [i].GetName ()))
			return i;

	return -1;
}

Conversion::CField CConversion::GetField (int nIndex) const
{
	if (nIndex >= 0 && nIndex < m_vFields.GetSize ()) 
		return m_vFields [nIndex];
			
	return CField ();
}
