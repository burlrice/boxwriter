#if !defined(AFX_TABLETYPEDLG_H__2BD53570_7030_48B4_AAC2_13B9D24083C8__INCLUDED_)
#define AFX_TABLETYPEDLG_H__2BD53570_7030_48B4_AAC2_13B9D24083C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TableTypeDlg.h : header file
//

#include "UtilApi.h"
#include "RoundButtons.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CTableTypeDlg dialog

namespace FoxjetUtils
{
	class UTIL_API CTableTypeDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CTableTypeDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CTableTypeDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		CString m_strParams;
		CString m_strKey;

		static CString GetParams (const CString & strKey = _T (""));

		virtual int DoModal ();

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CTableTypeDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		CRoundButtons m_buttons;
		virtual void OnOK();

		afx_msg void OnClick ();

		CString m_strUser;

		// Generated message map functions
		//{{AFX_MSG(CTableTypeDlg)
		virtual BOOL OnInitDialog();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace FoxjetUtils

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABLETYPEDLG_H__2BD53570_7030_48B4_AAC2_13B9D24083C8__INCLUDED_)
