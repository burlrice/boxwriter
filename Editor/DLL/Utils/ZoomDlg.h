#if !defined(AFX_ZOOMDLG_H__7692D541_4386_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_ZOOMDLG_H__7692D541_4386_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ZoomDlg.h : header file
//

#include "UtilApi.h"
#include "Utils.h"


namespace FoxjetCommon {
	typedef struct {
		int m_nMin;
		int m_nMax;
		int m_nZoom;
		LPVOID m_lpData;
	} ZOOMSTRUCT;

	typedef bool (CALLBACK * PZOOMFCT)(ZOOMSTRUCT & zoom);
	typedef PZOOMFCT LPZOOMFCT;  

	/////////////////////////////////////////////////////////////////////////////
	// CZoomDlg dialog

	class UTIL_API CZoomDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CZoomDlg(LPZOOMFCT lpZoomFunction, const ZOOMSTRUCT * pZoom = NULL,
			CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CZoomDlg)
		int		m_nZoom;
		//}}AFX_DATA


	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CZoomDlg)
		public:
		virtual BOOL DestroyWindow();
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual afx_msg void OnCancel ();

		// Generated message map functions
		//{{AFX_MSG(CZoomDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
		//}}AFX_MSG

		int m_nOriginalZoom;
		LPZOOMFCT m_lpZoomFct;
		LPVOID m_lpZoomParam;
		ZOOMSTRUCT m_zoom;

		DECLARE_MESSAGE_MAP()
	};
}; // namespace FoxjetCommon

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ZOOMDLG_H__7692D541_4386_11D4_8FC6_006067662794__INCLUDED_)
