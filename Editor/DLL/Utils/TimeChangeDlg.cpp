// TimeChangePage.cpp : implementation file
//

#include "stdafx.h"
#include "utils.h"
#include "resource.h"
#include "TimeChangeDlg.h"
#include "Database.h"
#include "Parse.h"
#include "TemplExt.h"
#include "Debug.h"
#include "SystemDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define IDT_TIME 100

using namespace FoxjetCommon;
using namespace FoxjetUtils;
using namespace FoxjetDatabase;

static const LPCTSTR lpszTimeChangeKey = _T ("TimeChange");

/////////////////////////////////////////////////////////////////////////////
// CTimeChangeDlg property page

IMPLEMENT_DYNCREATE(CTimeChangeDlg, CPropertyPage)

CTimeChangeDlg::CTimeChangeDlg() 
:	m_hbrDlg (NULL),
	m_lpFormatFct (NULL),
	m_bSetTime (false),
	CPropertyPage(IDD_TIMECHANGE_MATRIX)
{
	//{{AFX_DATA_INIT(CTimeChangeDlg)
	m_bSuppress = FALSE;
	//}}AFX_DATA_INIT
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CTimeChangeDlg::~CTimeChangeDlg()
{
	if (m_hbrDlg) { 
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}
}

void CTimeChangeDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTimeChangeDlg)
	DDX_Check(pDX, CHK_SUPPRESS, m_bSuppress);
	//}}AFX_DATA_MAP
	::DDX_Text (pDX, TXT_FORMAT1, m_strTimeFormat [0]);
	::DDX_Text (pDX, TXT_FORMAT2, m_strTimeFormat [1]);
	::DDX_Text (pDX, TXT_FORMAT3, m_strTimeFormat [2]);

	if (pDX->m_bSaveAndValidate) {
		if (m_bSetTime) {
			CString str;
	
			KillTimer (IDT_TIME);
			::DDX_Text (pDX, TXT_LOCAL_TIME, str);
			TRACEF (str);

			try {
				COleDateTime t;

				if (t.ParseDateTime (str)) {
					TRACEF (t.Format (_T ("%c")));
					SYSTEMTIME sysTime;

					t.GetAsSystemTime (sysTime);

					if (!::SetLocalTime (&sysTime)) 
						MsgBox (* this, FormatMessage (::GetLastError ()));
				}
			}
			catch (COleException * e)		{ HANDLEEXCEPTION (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		}
	}
}


BEGIN_MESSAGE_MAP(CTimeChangeDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CTimeChangeDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(TXT_FORMAT1, OnUpdateUI)
	ON_EN_CHANGE(TXT_FORMAT2, OnUpdateUI)
	ON_EN_CHANGE(TXT_FORMAT3, OnUpdateUI)
	ON_EN_CHANGE(TXT_LOCAL_TIME, OnTimeEdited)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTimeChangeDlg message handlers

BOOL CTimeChangeDlg::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	OnUpdateUI ();
	SetTimer (IDT_TIME, 500, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

bool CTimeChangeDlg::IsTimeChangeMessageEnabled (FoxjetDatabase::COdbcDatabase & db)
{
	SETTINGSSTRUCT s;
	bool bResult = true;

	if (GetSettingsRecord (db, 0, lpszTimeChangeKey, s)) {
		CStringArray v;

		FoxjetDatabase::FromString (s.m_strData, v);

		if (v.GetSize () >= 1) 
			bResult = _ttoi (v [0]) ? true : false;
	}

	return bResult;
}

void CTimeChangeDlg::EnableTimeChangeMessage (FoxjetDatabase::COdbcDatabase & db, bool bEnable)
{
	CStringArray v;
	CString str;
	SETTINGSSTRUCT s;

	str.Format (_T ("%d"), bEnable);
	
	v.Add (str);

	s.m_lLineID = 0;
	s.m_strKey = lpszTimeChangeKey;
	s.m_strData = FoxjetDatabase::ToString (v);
	TRACEF (s.m_strData);

	if (!UpdateSettingsRecord (db, s))
		VERIFY (AddSettingsRecord (db, s));
}

void CTimeChangeDlg::OnUpdateUI ()
{
	struct
	{
		UINT m_nEditID;
		UINT m_nLabelID;
	}
	const map [] = 
	{
		{ TXT_FORMAT1, LBL_FORMAT1, },
		{ TXT_FORMAT2, LBL_FORMAT2, },
		{ TXT_FORMAT3, LBL_FORMAT3, },
	};

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		CString str;

		GetDlgItemText (map [i].m_nEditID, str);

		if (m_lpFormatFct) 
			(* m_lpFormatFct) (str);
		
		SetDlgItemText (map [i].m_nLabelID, str);
	}
}

HBRUSH CTimeChangeDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = ::GetSysColor (COLOR_BTNFACE);//FoxjetUtils::GetButtonColor (_T ("background"));
	
	if (IsControl (false)) 
		rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	if (!m_hbrDlg)	
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}

void CTimeChangeDlg::OnTimeEdited ()
{
	m_bSetTime = true;
}

void CTimeChangeDlg::OnTimer( UINT nIDEvent )
{
	if (nIDEvent == IDT_TIME) {
		if (!m_bSetTime) {
			SYSTEMTIME local, utc;

			::GetSystemTime (&utc);
			::GetLocalTime (&local);

			if (CEdit * p = (CEdit *)GetDlgItem (TXT_LOCAL_TIME)) {
				int nStart = 0, nEnd = 0;
				p->GetSel (nStart, nEnd);
				p->SetWindowText (CTime (local).Format (_T ("%c")));
				p->SetSel (nStart, nEnd);
				m_bSetTime = false;
			}
		}

		return;
	}

	CDialog::OnTimer (nIDEvent);
}