#if !defined(AFX_LOCALDBDLG_H__205C3324_DC70_49A5_B429_7B6464D14F96__INCLUDED_)
#define AFX_LOCALDBDLG_H__205C3324_DC70_49A5_B429_7B6464D14F96__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LocalDbDlg.h : header file
//

#include "RoundButtons.h"
#include "OdbcDatabase.h"
#include "UtilApi.h"

namespace FoxjetUtils
{
	/////////////////////////////////////////////////////////////////////////////
	// CLocalDbDlg dialog

	class UTIL_API CLocalDbDlg : public CPropertyPage
	{
		DECLARE_DYNCREATE(CLocalDbDlg)

	// Construction
	public:
		CLocalDbDlg();
		~CLocalDbDlg();

	// Dialog Data
		//{{AFX_DATA(CLocalDbDlg)
		BOOL	m_bEnabled;
		//}}AFX_DATA
		CArray <CTime, CTime &> m_vTrigger;

		bool Load (FoxjetDatabase::COdbcDatabase & db);
		bool Save (FoxjetDatabase::COdbcDatabase & db);

		static void SetActive (bool bActive, ULONG lThreadID);
		static bool GetActive ();


	// Overrides
		// ClassWizard generate virtual function overrides
		//{{AFX_VIRTUAL(CLocalDbDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		CRoundButtons m_buttons;
		UINT m_nTimer;

		// Generated message map functions
		//{{AFX_MSG(CLocalDbDlg)
		afx_msg void OnBackup();
	virtual BOOL OnInitDialog();
	afx_msg void OnLocaldbEnabled();
	afx_msg void OnStop();
	afx_msg void OnBackupAll();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnClose();
	afx_msg void OnView ();
	//}}AFX_MSG
		DECLARE_MESSAGE_MAP()

	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOCALDBDLG_H__205C3324_DC70_49A5_B429_7B6464D14F96__INCLUDED_)
