#include "stdafx.h"
#include "Utils.h"
#include "Registry.h"
#include "Resource.h"
#include "Debug.h"
#include "AppVer.h"
#include "Export.h"
#include "OdbcRecordset.h"
#include "Serialize.h"
#include "FieldDefs.h"
#include "Parse.h"
#include "Utils.h"
#include "Parse.h"
#include "TemplExt.h"

#define DEBUG_STRINGFCTS

using namespace FoxjetCommon;
using namespace FoxjetDatabase;

static const LPCTSTR lpszBITMAP = _T ("BITMAP");
static CString g_strLogosDir;
static CString g_strBmpExt;

////////////////////////////////////////////////////////////////////////////////
#ifdef __IPPRINTER__
void UTIL_API FoxjetCommon::SetMkNextParams (const CString & strLogosDir, const CString & strBmpExt)
{
	::g_strLogosDir = strLogosDir;
	::g_strBmpExt = strBmpExt;
}
#endif //__IPPRINTER__

////////////////////////////////////////////////////////////////////////////////
CString GetMkNextBmpFilename (CString strFile)
{
	CString strSuffix = ::g_strBmpExt;
	CString strPath = ::g_strLogosDir;
						
	ASSERT (::g_strLogosDir.GetLength ());
	ASSERT (::g_strBmpExt.GetLength ());

	strFile.Replace (_T ("\""), _T (""));
	strFile = strPath + strFile;
	strFile.MakeLower ();
	strSuffix.MakeLower ();

	if (strFile.Find (strSuffix) == -1)
		strFile += strSuffix;

	return strFile;
}


template <class TYPE> 
static bool WriteRecords (CArray <TYPE, TYPE &> & v, FILE * fp);

#define WRITERECORDS(db, t, lpszTable, fp) \
	{ \
		TRACEF (_T ("WRITERECORDS: ")  _T (#t) _T (", ") _T (#lpszTable)); \
		CArray < ##t , ##t &> v##t ; \
		GetRecords < ##t > ((db), v##t , ##lpszTable ); \
		WriteRecords < ##t > (v##t, (fp)); \
	}


static void WriteBitmaps (CArray <TASKSTRUCT, TASKSTRUCT &> & vTasks, FILE * fp)
{
	CStringArray vFiles;

	//GetTaskRecords (db, ALL, vTasks);
	
	for (int nTask = 0; nTask < vTasks.GetSize (); nTask++) {
		TASKSTRUCT & task = vTasks [nTask];
		int nChanged = 0;

		//TRACEF (task.m_strName);

		for (int nMsg = 0; nMsg < task.m_vMsgs.GetSize (); nMsg++) {
			MESSAGESTRUCT & msg = task.m_vMsgs [nMsg];
			CStringArray v;

			ParsePackets (msg.m_strData, v);

			for (int nPacket = 0; nPacket < v.GetSize (); nPacket++) {
				CStringArray vPacket;
				CString & strSeg = v [nPacket];

				Tokenize (strSeg, vPacket);

				if (vPacket.GetSize ()) { 
					CString & strToken = vPacket [0];

					if (!strToken.CompareNoCase (_T ("Bitmap"))) {
						#ifdef __WINPRINTER__
							const int nField = 9;
						#else
							const int nField = 10;
						#endif

						CString strFile = UnformatString (vPacket [nField]);

						#ifdef __IPPRINTER__
						strFile = GetMkNextBmpFilename (strFile);
						#endif

						if (Find (vFiles, strFile, false) == -1)
							vFiles.Add (strFile);
					}
				}
			}
		}
	}

	for (int i = 0; i < vFiles.GetSize (); i++) {
		CString strFile = vFiles [i];
		DWORD dwFileSize = 0;

		HANDLE hFile = ::CreateFile (strFile, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);

		TRACEF (strFile);

		if (hFile && hFile != INVALID_HANDLE_VALUE) {
			DWORD dwHighSize = 0;
			
			dwFileSize = GetFileSize (hFile, &dwHighSize);
			::CloseHandle (hFile);
		}

		if (FILE * fpData = _tfopen (strFile, _T ("rb"))) {
			int nMIME = (int)(dwFileSize * 2);
			BYTE * pMIME = new BYTE [nMIME];
			BYTE * p = new BYTE [dwFileSize];

			memset (p, 0, dwFileSize);
			fread (p, dwFileSize, 1, fpData);
			fclose (fpData);

			memset (pMIME, nMIME, 0);
			int nEncoded = EncodeBase64 (p, dwFileSize, pMIME, nMIME);
			pMIME [nEncoded] = 0;
			CString str = _T ("{") + CString (::lpszBITMAP) + _T (",") + FormatString (strFile) + _T (",\n") + CString (pMIME) + _T ("}\n");

			delete [] p;
			delete [] pMIME;

			//TRACEF (str);
			fwrite (w2a (str), str.GetLength (), 1, fp);
		}
	}
}

template <class TYPE> 
static bool WriteRecords (CArray <TYPE, TYPE &> & v, FILE * fp)
{
	if (fp) {
		CString str;

		for (int i = 0; i < v.GetSize (); i++) {
			str = ToString (v [i]) + _T ("\n");

			#if defined( _DEBUG ) && defined( DEBUG_STRINGFCTS )
			TYPE obj;
			const CString strTo		= ToString (v [i]);

			FromString (strTo, obj);
			const CString strFrom	= ToString (obj);

			if (strTo != strFrom) {
				TRACEF (_T ("DEBUG_STRINGFCTS"));
				TRACEF (strTo);
				TRACEF (strFrom);
			}
			#endif 

			fwrite (w2a (str), str.GetLength (), 1, fp);
		}

		str = _T ("\n");
		fwrite (w2a (str), str.GetLength (), 1, fp);

		return true;
	}

	return false;
}


UTIL_API void FoxjetCommon::GetBitmapSummary (CArray <FoxjetDatabase::TASKSTRUCT, FoxjetDatabase::TASKSTRUCT &> & vTasks, CMapStringToString & vFile, CMapStringToString & vDir)
{
	for (int nTask = 0; nTask < vTasks.GetSize (); nTask++) {
		TASKSTRUCT & t = vTasks [nTask];

		for (int nMsg = 0; nMsg < t.m_vMsgs.GetSize (); nMsg++) {
			MESSAGESTRUCT & msg = t.m_vMsgs [nMsg];
			CStringArray vElements;

			ParsePackets (msg.m_strData, vElements);
			TRACEF (msg.m_strData);

			for (int nElement = 0; nElement < vElements.GetSize (); nElement++) {
				CStringArray v;

				Tokenize (vElements [nElement], v);

				TRACEF (vElements [nElement]);

				if (v.GetSize () >= 9) {
					if (!v [0].CompareNoCase (_T ("Bitmap"))) {
						#ifdef __WINPRINTER__
							CString strFile = UnformatString (GetParam (v, 9));
						#else //__IPPRINTER__
							CString strFile = GetMkNextBmpFilename (UnformatString (GetParam (v, 10)));
						#endif

						int nIndex = strFile.ReverseFind ('\\');
						int nFile = 0;

						{
							CString str;
							vFile.Lookup (strFile, str);
							nFile = _ttoi (str) + 1;
						}

						vFile.SetAt (strFile, FoxjetDatabase::ToString (nFile));

						if (nIndex != -1) {
							int nDir = 0;
							CString str, strDir = strFile.Left (nIndex);

							{
								CString str;
								vDir.Lookup (strDir, str);
								nDir = _ttoi (str) + 1;
							}

							vDir.SetAt (strDir, FoxjetDatabase::ToString (nDir));
						}
					}
				}
			}
		}
	}
}

bool UTIL_API FoxjetCommon::Export (FoxjetDatabase::COdbcDatabase & database, const CString & strFile, 
									const FoxjetCommon::CLongArray & vTaskIDs, DWORD dwFlags)
{
#ifdef _DEBUG
	{
		struct
		{
			DWORD m_dw;
			LPCTSTR m_lpsz;
		}
		static const map [] =
		{
			{ IMPORT_LINESTRUCT,			_T ("IMPORT_LINESTRUCT"),			},
			{ IMPORT_BOXSTRUCT,				_T ("IMPORT_BOXSTRUCT"),			},
			{ IMPORT_BOXLINKSTRUCT,			_T ("IMPORT_BOXLINKSTRUCT"),		},
			{ IMPORT_TASKSTRUCT,			_T ("IMPORT_TASKSTRUCT"),			},
			{ IMPORT_HEADSTRUCT,			_T ("IMPORT_HEADSTRUCT"),			},
			{ IMPORT_PANELSTRUCT,			_T ("IMPORT_PANELSTRUCT"),			},
			{ IMPORT_OPTIONSSTRUCT,			_T ("IMPORT_OPTIONSSTRUCT"),		},
			{ IMPORT_OPTIONSLINKSTRUCT,		_T ("IMPORT_OPTIONSLINKSTRUCT"),	},
			{ IMPORT_USERSTRUCT,			_T ("IMPORT_USERSTRUCT"),			},
			{ IMPORT_SECURITYGROUPSTRUCT,	_T ("IMPORT_SECURITYGROUPSTRUCT"),	},
			{ IMPORT_SHIFTSTRUCT,			_T ("IMPORT_SHIFTSTRUCT"),			},
			{ IMPORT_REPORTSTRUCT,			_T ("IMPORT_REPORTSTRUCT"),			},
			{ IMPORT_BCSCANSTRUCT,			_T ("IMPORT_BCSCANSTRUCT"),			},
			{ IMPORT_DELETEDSTRUCT,			_T ("IMPORT_DELETEDSTRUCT"),		},
			{ IMPORT_SETTINGSSTRUCT,		_T ("IMPORT_SETTINGSSTRUCT"),		},
			{ IMPORT_IMAGESTRUCT,			_T ("IMPORT_IMAGESTRUCT"),			},
			{ IMPORT_PRINTERSTRUCT,			_T ("IMPORT_PRINTERSTRUCT"),		},
			{ IMPORT_BITMAPS,				_T ("IMPORT_BITMAPS"),				},
			{ IMPORT_ALL,					_T ("IMPORT_ALL"),					},
		};

		for (int i = 0; i < ARRAYSIZE (map); i++)
			if ((dwFlags & map [i].m_dw) == map [i].m_dw)
				TRACEF (map [i].m_lpsz);
	}
#endif //_DEBUG

	TRACEF (strFile);
	CArray <TASKSTRUCT, TASKSTRUCT &> vTasks;

	for (int i = 0; i < vTaskIDs.GetSize (); i++) {
		TASKSTRUCT t;

		VERIFY (GetTaskRecord (database, vTaskIDs [i], t));
		TRACEF (FoxjetDatabase::ToString (t.m_lID) + _T (": ") + t.m_strName);
		vTasks.Add (t);
	}

	ASSERT (database.IsOpen ());

	if (FILE * fp = _tfopen (strFile, _T ("w"))) {
		FILE * fpStats = NULL;

		{
			CString strKey = _T ("Software\\Foxjet\\MarksmanELITE\\ODBC");
			HKEY hKeyRoot = NULL;
			int nDSN = 0;
			
			::RegOpenKeyEx (HKEY_CURRENT_USER, strKey, 0, KEY_READ, &hKeyRoot);
			
			for (DWORD i = 0; ; i++) {
				TCHAR sz [255] = { 0 };
				DWORD dw = ARRAYSIZE (sz);
				CString str;

				if (::RegEnumKeyEx (hKeyRoot, i, sz, &dw, NULL, NULL, NULL, NULL) != ERROR_SUCCESS)
					break;
				
				str.Format (_T ("%s\\%s"), strKey, sz);
				CString strConnect = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, str, _T ("connect"), _T ("")); 

				if (strConnect.GetLength ()) {
					str.Format (_T ("{DSN,%s,%s}\n"), sz, strConnect);
					TRACEF (str);
					fwrite (w2a (str), str.GetLength (), 1, fp);
					nDSN++;
				}
			}

			::RegCloseKey (hKeyRoot);

			if (nDSN) {
				CString str = _T ("\n");
			
				fwrite (w2a (str), str.GetLength (), 1, fp);
			}

			{
				CMapStringToString vFile;
				CMapStringToString vDir;

				GetBitmapSummary (vTasks, vFile, vDir);

				{
					CString str;

					str.Format (_T ("{BitmapSummary:\n"));
					fwrite (w2a (str), str.GetLength (), 1, fp);

					for (POSITION pos = vFile.GetStartPosition (); pos; ) {
						CString str, strFile, strCount;

						vFile.GetNextAssoc (pos, strFile, strCount);

						str.Format (_T ("\t%s, %s\n"), strCount, strFile);
						fwrite (w2a (str), str.GetLength (), 1, fp);
					}

					str.Format (_T ("\n\n"));
					fwrite (w2a (str), str.GetLength (), 1, fp);

					for (POSITION pos = vDir.GetStartPosition (); pos; ) {
						CString str, strFile, strCount;

						vDir.GetNextAssoc (pos, strFile, strCount);

						str.Format (_T ("\tdir: %s, %s\n"), strCount, strFile);
						fwrite (w2a (str), str.GetLength (), 1, fp);
					}

					str.Format (_T ("}\n\n"));
					fwrite (w2a (str), str.GetLength (), 1, fp);
				}
			}
		}


		if (dwFlags & IMPORT_LINESTRUCT				)	WRITERECORDS (database, LINESTRUCT,				Lines::m_lpszTable,				fp);
		if (dwFlags & IMPORT_BOXSTRUCT				)	WRITERECORDS (database, BOXSTRUCT,				Boxes::m_lpszTable,				fp);
		if (dwFlags & IMPORT_BOXLINKSTRUCT			)	WRITERECORDS (database, BOXLINKSTRUCT,			BoxToLine::m_lpszTable,			fp);
		if (dwFlags & IMPORT_TASKSTRUCT				)	WriteRecords <TASKSTRUCT> (vTasks, fp); //WRITERECORDS (database, TASKSTRUCT,				Tasks::m_lpszTable,				fp);			
		if (dwFlags & IMPORT_HEADSTRUCT				)	WRITERECORDS (database, HEADSTRUCT,				Heads::m_lpszTable,				fp);	
		if (dwFlags & IMPORT_PANELSTRUCT			)	WRITERECORDS (database, PANELSTRUCT,			Panels::m_lpszTable,			fp);
		if (dwFlags & IMPORT_OPTIONSSTRUCT			)	WRITERECORDS (database, OPTIONSSTRUCT,			Options::m_lpszTable,			fp);
		if (dwFlags & IMPORT_OPTIONSLINKSTRUCT		)	WRITERECORDS (database, OPTIONSLINKSTRUCT,		OptionsToGroup::m_lpszTable,	fp);
		if (dwFlags & IMPORT_USERSTRUCT				)	WRITERECORDS (database, USERSTRUCT,				Users::m_lpszTable,				fp);	
		if (dwFlags & IMPORT_SECURITYGROUPSTRUCT	)	WRITERECORDS (database, SECURITYGROUPSTRUCT,	SecurityGroups::m_lpszTable,	fp);
		if (dwFlags & IMPORT_SHIFTSTRUCT			)	WRITERECORDS (database, SHIFTSTRUCT,			Shifts::m_lpszTable,			fp);
		if (dwFlags & IMPORT_REPORTSTRUCT			)	WRITERECORDS (database, REPORTSTRUCT,			Reports::m_lpszTable,			fp);
		if (dwFlags & IMPORT_BCSCANSTRUCT			)	WRITERECORDS (database, BCSCANSTRUCT,			BarcodeScans::m_lpszTable,		fp);
		if (dwFlags & IMPORT_DELETEDSTRUCT			)	WRITERECORDS (database, DELETEDSTRUCT,			Deleted::m_lpszTable,			fp);
		if (dwFlags & IMPORT_SETTINGSSTRUCT			)	WRITERECORDS (database, SETTINGSSTRUCT,			Settings::m_lpszTable,			fp);
		if (dwFlags & IMPORT_IMAGESTRUCT			)	WRITERECORDS (database, IMAGESTRUCT,			Images::m_lpszTable,			fp);

		if (database.HasTable (Printers::m_lpszTable))
			if (dwFlags & IMPORT_PRINTERSTRUCT		)	WRITERECORDS (database, PRINTERSTRUCT,			Printers::m_lpszTable,			fp); 

		if (dwFlags & IMPORT_BITMAPS				)	WriteBitmaps (vTasks, fp);

		fclose (fp);
		return true;
	}

	return false;
}
