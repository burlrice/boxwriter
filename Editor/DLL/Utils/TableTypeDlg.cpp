// TableTypeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
//#include "list.h"
#include "TableTypeDlg.h"
#include "Registry.h"
#include "TemplExt.h"
#include "Database.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;
using namespace FoxjetUtils;

extern HINSTANCE hInstance;

/////////////////////////////////////////////////////////////////////////////
// CTableTypeDlg dialog


CTableTypeDlg::CTableTypeDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg(IDD_DATABASEPARAMS_MATRIX, pParent)
{
	m_strKey = _T ("Software\\Foxjet\\Database");

	//{{AFX_DATA_INIT(CTableTypeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CTableTypeDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);

	DDX_Text (pDX, TXT_PARAMS, m_strUser);
	//{{AFX_DATA_MAP(CTableTypeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
}


BEGIN_MESSAGE_MAP(CTableTypeDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CTableTypeDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (CHK_TABLE,			 OnClick)
	ON_BN_CLICKED (CHK_VIEW,			 OnClick)
	ON_BN_CLICKED (CHK_SYSTEMTABLE,		 OnClick)
	ON_BN_CLICKED (CHK_GLOBALTEMPORARY,	 OnClick)
	ON_BN_CLICKED (CHK_LOCALTEMPORARY,	 OnClick)
	ON_BN_CLICKED (CHK_ALIAS,			 OnClick)
	ON_BN_CLICKED (CHK_SYNONYM,			 OnClick)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTableTypeDlg message handlers

struct
{
	UINT m_nID;
	LPCTSTR m_lpszType;
}
static const map [] = 
{
    { CHK_TABLE,			_T ("'TABLE'"),				},
    { CHK_VIEW,				_T ("'VIEW'"),				},
    { CHK_SYSTEMTABLE,		_T ("'SYSTEM TABLE'"),		},
    { CHK_GLOBALTEMPORARY,	_T ("'GLOBAL TEMPORARY'"),	},
    { CHK_LOCALTEMPORARY,	_T ("'LOCAL TEMPORARY'"),	},
    { CHK_ALIAS,			_T ("'ALIAS'"),				},
    { CHK_SYNONYM,			_T ("'SYNONYM'"),			},
};

CString CTableTypeDlg::GetParams (const CString & strKey) 
{
	CString strDef;
	CString str = strKey;

	if (!str.GetLength ())
		str = _T ("Software\\Foxjet\\Database");

	for (int i = 0; i < ARRAYSIZE (::map); i++) {
		strDef += ::map [i].m_lpszType;

		if ((i + 1) < ARRAYSIZE (::map))
			strDef += _T (", ");
	}

	return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, str, _T ("Table params"), strDef);
}

void CTableTypeDlg::OnOK()
{
	if (!UpdateData ())
		return;

	m_strParams.Empty ();

	for (int i = 0; i < ARRAYSIZE (::map); i++) {
		if (CButton * p = (CButton *)GetDlgItem (::map [i].m_nID)) {
			if (p->GetCheck () == 1) {
				m_strParams += ::map [i].m_lpszType;
				m_strParams += _T (", ");
			}
		}
	}

	m_strParams.TrimLeft (); 
	m_strParams.TrimRight ();

	if (m_strUser.GetLength ()) {
		if (m_strParams.GetLength ())
			if (m_strParams [m_strParams.GetLength () - 1] != ',')
				m_strParams += _T (",");

		m_strParams += _T (" ") + m_strUser;
	}

	FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, m_strKey, _T ("Table params"), m_strParams);
	FoxjetCommon::CEliteDlg::OnOK ();
}

BOOL CTableTypeDlg::OnInitDialog() 
{
	m_strParams = GetParams (m_strKey);

	for (int i = 0; i < ARRAYSIZE (::map); i++) {
		if (CButton * p = (CButton *)GetDlgItem (::map [i].m_nID)) {
			CString str;

			p->GetWindowText (str);
			p->SetWindowText (str + _T (" [") + CString (::map [i].m_lpszType) + _T ("]"));

			if (m_strParams.Find (::map [i].m_lpszType) != -1) {
				p->SetCheck (1);
				m_strParams.Replace (CString (_T (", ")) + ::map [i].m_lpszType, _T (""));
				m_strParams.Replace (::map [i].m_lpszType + CString (_T (", ")), _T (""));
				m_strParams.Replace (::map [i].m_lpszType, _T (""));
			}
		}
	}

	m_strUser = m_strParams;
	m_strUser.TrimLeft ();
	m_strUser.TrimRight ();

	if (m_strUser.GetLength ())
		if (m_strUser [m_strUser.GetLength () - 1] == ',')
			m_strUser.Delete (m_strUser.GetLength () - 1);
	
	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTableTypeDlg::OnClick ()
{
/* TODO: rem
	CString str;

	GetDlgItemText (TXT_PARAMS, str);

	const CString strOriginal = str;

	for (int i = 0; i < ARRAYSIZE (::map); i++) {
		if (CButton * p = (CButton *)GetDlgItem (::map [i].m_nID)) {
			if (p->GetCheck () == 1) {
				if (str.Find (::map [i].m_lpszType) == -1) {
					int nIndex = 0;

					for (int j = i; j < ARRAYSIZE (::map); j++) {
						nIndex = str.Find (::map [j].m_lpszType);

						if (nIndex != -1)
							break;
					}

					str.Insert (nIndex, CString (::map [i].m_lpszType) + _T (", "));
				}
			}
			else {
				if (str.Find (::map [i].m_lpszType) != -1) {
					str.Replace (CString (_T (", ")) + ::map [i].m_lpszType, _T (""));
					str.Replace (::map [i].m_lpszType + CString (_T (", ")), _T (""));
					str.Replace (::map [i].m_lpszType, _T (""));
				}
			}
		}
	}

	str.TrimLeft ();
	str.TrimRight ();

	if (str.GetLength ())
		if (str [str.GetLength () - 1] == ',')
			str.Delete (str.GetLength () - 1);

	if (strOriginal != str)
		SetDlgItemText (TXT_PARAMS, str);
*/
}

int CTableTypeDlg::DoModal ()
{
	HINSTANCE h = ::AfxGetResourceHandle ();
	::AfxSetResourceHandle (::hInstance);

	int nResult = FoxjetCommon::CEliteDlg::DoModal ();

	::AfxSetResourceHandle (h);

	return nResult;
}
