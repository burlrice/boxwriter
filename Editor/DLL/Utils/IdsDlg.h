#if !defined(AFX_IDSDLG_H__A136365C_DBA6_4CA5_A6E3_2850F4C00881__INCLUDED_)
#define AFX_IDSDLG_H__A136365C_DBA6_4CA5_A6E3_2850F4C00881__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IdsDlg.h : header file
//

#include "Database.h"

/////////////////////////////////////////////////////////////////////////////
// CIdsDlg dialog

namespace FoxjetUtils
{
	class UTIL_API CIdsDlg : public CPropertyPage
	{
	//	DECLARE_DYNCREATE(CIdsDlg)

	// Construction
	public:
		CIdsDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection);
		~CIdsDlg();

		const CString m_strRegSection;
		FoxjetDatabase::COdbcDatabase & m_db;

	// Dialog Data
		//{{AFX_DATA(CIdsDlg)
		CIPAddressCtrl	m_ctrlAddr;
		//}}AFX_DATA

		DWORD m_dwAddress;
		CString m_strMacAddr;

		static DWORD GetDefAddr ();

	// Overrides
		// ClassWizard generate virtual function overrides
		//{{AFX_VIRTUAL(CIdsDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual void OnOK();

		afx_msg void OnSetClick ();

		// Generated message map functions
		//{{AFX_MSG(CIdsDlg)
		virtual BOOL OnInitDialog();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()

	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IDSDLG_H__A136365C_DBA6_4CA5_A6E3_2850F4C00881__INCLUDED_)
