// StartupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "utils.h"
#include "StartupDlg.h"
#include "Database.h"
#include "ScannerPropDlg.h"
#include "Parse.h"
#include "SystemDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetUtils;
using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////
// CStartupDlg property page

//IMPLEMENT_DYNCREATE(CStartupDlg, CPropertyPage)

CStartupDlg::CStartupDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection) 
:	m_strRegSection (strRegSection),
	m_db (db),
	m_hbrDlg (NULL),
	m_bUserData (FALSE),
	m_bSerialData (FALSE),
	CPropertyPage(IDD_STARTUP_MATRIX)
{
	//{{AFX_DATA_INIT(CStartupDlg)
	m_bAutoStart = FALSE;
	//}}AFX_DATA_INIT
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CStartupDlg::~CStartupDlg()
{
	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}
}

void CStartupDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStartupDlg)
	DDX_Check(pDX, CHK_AUTOSTART, m_bAutoStart);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, CHK_USERDATA, m_bUserData);
	DDX_Check(pDX, CHK_SERIALDATA, m_bSerialData);
}


BEGIN_MESSAGE_MAP(CStartupDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CStartupDlg)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStartupDlg message handlers

bool CStartupDlg::IsAutoStartEnabled (FoxjetDatabase::COdbcDatabase & db)
{
	bool bResult = true;
	SETTINGSSTRUCT s;

	if (GetSettingsRecord (db, 0, lpszStartupKey, s)) {
		CStringArray v;

		FoxjetDatabase::FromString (s.m_strData, v);

		if (v.GetSize () >= 1) bResult = _ttoi (v [0]) ? true : false;
	}

	return bResult;
}

bool CStartupDlg::IsUserDataEnabled (FoxjetDatabase::COdbcDatabase & db)
{
	bool bResult = false;
	SETTINGSSTRUCT s;

	if (GetSettingsRecord (db, 0, lpszStartupKey, s)) {
		CStringArray v;

		FoxjetDatabase::FromString (s.m_strData, v);

		if (v.GetSize () >= 2) bResult = _ttoi (v [1]) ? true : false;
	}

	return bResult;
}

bool CStartupDlg::IsSerialDataEnabled (FoxjetDatabase::COdbcDatabase & db)
{
	bool bResult = false;
	SETTINGSSTRUCT s;

	if (GetSettingsRecord (db, 0, lpszStartupKey, s)) {
		CStringArray v;

		FoxjetDatabase::FromString (s.m_strData, v);

		if (v.GetSize () >= 3) bResult = _ttoi (v [2]) ? true : false;
	}

	return bResult;
}

HBRUSH CStartupDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = ::GetSysColor (COLOR_BTNFACE);//FoxjetUtils::GetButtonColor (_T ("background"));
	
	if (IsControl (false)) 
		rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	if (!m_hbrDlg)	
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}
