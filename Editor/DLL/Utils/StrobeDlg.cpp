// StrobeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "control.h"
#include "StrobeDlg.h"
#include "TemplExt.h"
//#include "Extern.h"
#include "Registry.h"
#include "Debug.h"
#include "Parse.h"
#include "StrobeEditDlg.h"
#include "Resource.h"
#include "WinMsg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ItiLibrary;
using namespace ItiLibrary::ListCtrlImp;
using namespace FoxjetCommon;
//using namespace ListGlobals;
using namespace FoxjetDatabase;
using namespace FoxjetUtils;

using FoxjetDatabase::STROBESTATE;
using FoxjetDatabase::HEADERRORTYPE;


CString CStrobeItem::GetStrobeState (STROBESTATE s)
{
	switch (s) {
	case STROBESTATE_OFF:	return LoadString (IDS_OFF);
	case STROBESTATE_ON:	return LoadString (IDS_ON);
	case STROBESTATE_FLASH:	return LoadString (IDS_FLASHING);
	}

	return _T ("");
}

STROBESTATE CStrobeItem::GetStrobeType (const CString & str)
{
	const CString strOn		= LoadString (IDS_ON);
	const CString strFlash	= LoadString (IDS_FLASHING);

	if (!str.CompareNoCase (strOn)) 		return STROBESTATE_ON;
	else if (!str.CompareNoCase (strFlash)) return STROBESTATE_FLASH;

	return STROBESTATE_OFF;
}

CString CStrobeItem::GetErrorString (HEADERRORTYPE s)
{
	switch (s) {
	case HEADERROR_LOWTEMP:			return LoadString (IDS_HEADERROR_LOWTEMP);
	case HEADERROR_HIGHVOLTAGE:		return LoadString (IDS_HEADERROR_HIGHVOLTAGE);
	case HEADERROR_LOWINK:			return LoadString (IDS_HEADERROR_LOWINK);
	case HEADERROR_OUTOFINK:		return LoadString (IDS_HEADERROR_OUTOFINK);
	case IVERROR_BROKENLINE:		return LoadString (IDS_IVERROR_BROKENLINE);
	case IVERROR_DISCONNECTED:		return LoadString (IDS_IVERROR_DISCONNECTED);
	case HPERROR_COMMPORTFAILURE:	return LoadString (IDS_HPERROR_COMMPORTFAILURE);
	case HEADERROR_DISCONNECTED:	return LoadString (IDS_HEADERROR_DISCONNECTED);
	case HUBERROR_IO:				return LoadString (IDS_HUBERROR_IO);
	case HEADERROR_READY:			return LoadString (IDS_HEADERROR_READY);
	case HEADERROR_APSCYCLE:		return LoadString (IDS_HEADERROR_APSCYCLE);
	case ERROR_NETWORK:				return LoadString (IDS_ERROR_NETWORK);
	case HEADERROR_EP8:				return LoadString (IDS_ERROR_EP8);
	}

	return _T ("");
}

HEADERRORTYPE CStrobeItem::GetErrorType (const CString & str)
{
	static const CString strLowTemp				= LoadString (IDS_HEADERROR_LOWTEMP);
	static const CString strHighVoltage			= LoadString (IDS_HEADERROR_HIGHVOLTAGE);
	static const CString strLowInk				= LoadString (IDS_HEADERROR_LOWINK);
	static const CString strOutOfInk			= LoadString (IDS_HEADERROR_OUTOFINK);
	static const CString strBrokenLine			= LoadString (IDS_IVERROR_BROKENLINE);
	static const CString strIVDisconnected		= LoadString (IDS_IVERROR_DISCONNECTED);
	static const CString strCommFailure			= LoadString (IDS_HPERROR_COMMPORTFAILURE);
	static const CString strHeadDisconnected	= LoadString (IDS_HEADERROR_DISCONNECTED);
	static const CString strHubError			= LoadString (IDS_HUBERROR_IO);
	static const CString strReady				= LoadString (IDS_HEADERROR_READY);
	static const CString strAps					= LoadString (IDS_HEADERROR_APSCYCLE);
	static const CString strNetwork				= LoadString (IDS_ERROR_NETWORK);
	static const CString strEp8					= LoadString (IDS_ERROR_EP8);

	if (!str.CompareNoCase (strLowTemp))				return HEADERROR_LOWTEMP;
	else if (!str.CompareNoCase (strHighVoltage))		return HEADERROR_HIGHVOLTAGE;
	else if (!str.CompareNoCase (strLowInk))			return HEADERROR_LOWINK;
	else if (!str.CompareNoCase (strOutOfInk))			return HEADERROR_OUTOFINK;
	else if (!str.CompareNoCase (strBrokenLine))		return IVERROR_BROKENLINE;
	else if (!str.CompareNoCase (strIVDisconnected))	return IVERROR_DISCONNECTED;
	else if (!str.CompareNoCase (strCommFailure))		return HPERROR_COMMPORTFAILURE;
	else if (!str.CompareNoCase (strHeadDisconnected))	return HEADERROR_DISCONNECTED;
	else if (!str.CompareNoCase (strHubError))			return HUBERROR_IO;
	else if (!str.CompareNoCase (strReady))				return HEADERROR_READY;
	else if (!str.CompareNoCase (strAps))				return HEADERROR_APSCYCLE;
	else if (!str.CompareNoCase (strNetwork))			return ERROR_NETWORK;
	else if (!str.CompareNoCase (strEp8))				return HEADERROR_EP8;
	else												return HEADERROR_READY;
}

CString CStrobeItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
		case 0:		return m_strName;
		case 1:		return m_green	== STROBESTATE_OFF ? LoadString (IDS_OFF) : GetStrobeState (m_green);

	//#ifdef __WINPRINTER__
	//	case 2:		return m_red	== STROBESTATE_OFF ? LoadString (IDS_OFF) : GetStrobeState (m_red);
	//#else
		case 2:		return m_yellow	== STROBESTATE_OFF ? LoadString (IDS_OFF) : GetStrobeState (m_yellow);
		case 3:		return m_red	== STROBESTATE_OFF ? LoadString (IDS_OFF) : GetStrobeState (m_red);
	//#endif
	}

	return _T ("");
}

CString CStrobeItem::ToString () const
{
	CStringArray v;

	v.Add (m_strName);
	v.Add (GetStrobeState (m_green));
	v.Add (GetStrobeState (m_red));
	v.Add (FoxjetDatabase::ToString (m_nDelay));
	v.Add (GetStrobeState (m_yellow));

	return FoxjetDatabase::ToString (v);
}

bool CStrobeItem::FromString (const CString & str)
{
	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	m_strName	= GetParam (v, nIndex++);
	m_green		= GetStrobeType (GetParam (v, nIndex++));
	m_red		= GetStrobeType (GetParam (v, nIndex++));
	m_nDelay	= _ttoi (GetParam (v, nIndex++));
	m_yellow	= GetStrobeType (GetParam (v, nIndex++));

	return true;
}


/////////////////////////////////////////////////////////////////////////////
// CStrobeDlg dialog


CStrobeDlg::CStrobeDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection, CWnd* pParent /*=NULL*/)
:	m_db (db),
	m_strRegSection (strRegSection),
	FoxjetCommon::CEliteDlg(IDD_STROBE_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CStrobeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CStrobeDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStrobeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_EDIT);
}


BEGIN_MESSAGE_MAP(CStrobeDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CStrobeDlg)
	ON_NOTIFY(NM_DBLCLK, LV_ERROR, OnDblclkError)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStrobeDlg message handlers


BOOL CStrobeDlg::OnInitDialog() 
{
	CArray <CColumn, CColumn> vCols;

	FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_ERRORCONDITION), 200));
	vCols.Add (CColumn (LoadString (IDS_GREENALARM), 200));
	
	//#ifdef __IPPRINTER__
	vCols.Add (CColumn (LoadString (IDS_YELLOWALARM), 200));
	//#endif

	vCols.Add (CColumn (LoadString (IDS_REDALARM), 200));

	m_lv.Create (LV_ERROR, vCols, this, m_strRegSection);

	Load (m_db, m_vStates);

	for (int i = 0; i < m_vStates.GetSize (); i++) {
		if (m_vStates [i].m_strName.CompareNoCase (CStrobeItem::GetErrorString (HEADERROR_EP8)) != 0) {
			CStrobeItem * pItem = new CStrobeItem ();

			* pItem = m_vStates [i];

			m_lv.InsertCtrlData (pItem);
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CStrobeDlg::OnDblclkError(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();	
	*pResult = 0;
}

void CStrobeDlg::OnEdit() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (!sel.GetSize ()) {
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
		return;
	}
	
	if (CStrobeItem * pItem = (CStrobeItem *)m_lv.GetCtrlData (sel [0])) {
		CStrobeEditDlg dlg (m_db, m_strRegSection, this);

		dlg.m_bAPS		= !pItem->m_strName.CompareNoCase (LoadString (IDS_HEADERROR_APSCYCLE)) ? true : false;
		dlg.m_strName	= pItem->m_strName;

		#ifdef __WINPRINTER__
		dlg.m_nGreen	= pItem->m_green	== STROBESTATE_OFF ? 0 : 1;
		dlg.m_nRed		= pItem->m_red		== STROBESTATE_OFF ? 0 : (pItem->m_red		== STROBESTATE_FLASH ? 1 : 2);
		dlg.m_nYellow	= pItem->m_yellow	== STROBESTATE_OFF ? 0 : 1;
		dlg.m_nDelay	= pItem->m_nDelay;
		#else
		dlg.m_nGreen	= pItem->m_green	== STROBESTATE_OFF ? 0 : 1;
		dlg.m_nRed		= pItem->m_red		== STROBESTATE_OFF ? 0 : 1;
		dlg.m_nYellow	= pItem->m_yellow	== STROBESTATE_OFF ? 0 : 1;
		dlg.m_nDelay	= pItem->m_nDelay;
		#endif


		if (dlg.DoModal () == IDOK) {
			pItem->m_strName	= dlg.m_strName;
			#ifdef __WINPRINTER__
			pItem->m_green		= dlg.m_nGreen	== 0 ? STROBESTATE_OFF : STROBESTATE_ON;
			pItem->m_red		= dlg.m_nRed	== 0 ? STROBESTATE_OFF : (dlg.m_nRed	== 1 ? STROBESTATE_FLASH : STROBESTATE_ON);
			pItem->m_yellow		= dlg.m_nYellow	== 0 ? STROBESTATE_OFF : STROBESTATE_ON;
			pItem->m_nDelay		= dlg.m_nDelay;
			#else
			pItem->m_green		= dlg.m_nGreen	== 0 ? STROBESTATE_OFF : STROBESTATE_ON;
			pItem->m_yellow		= dlg.m_nYellow	== 0 ? STROBESTATE_OFF : STROBESTATE_ON;
			pItem->m_red		= dlg.m_nRed	== 0 ? STROBESTATE_OFF : STROBESTATE_ON;
			#endif
			

			m_lv.UpdateCtrlData (pItem);
		}
	}
}

void CStrobeDlg::Load (FoxjetDatabase::COdbcDatabase & db, CArray <CStrobeItem, CStrobeItem &> & v)
{
	struct
	{
		HEADERRORTYPE	m_type;
		STROBESTATE		m_green;
		STROBESTATE		m_yellow;
		STROBESTATE		m_red;
		int				m_nDelay;
	} static const map [] = 
	{
	#ifdef __WINPRINTER__
		// type							green				yellow				red						delay
		{ HEADERROR_READY,				STROBESTATE_ON,		STROBESTATE_OFF,	STROBESTATE_OFF,		30, },
		{ HEADERROR_APSCYCLE,			STROBESTATE_OFF,	STROBESTATE_OFF,	STROBESTATE_OFF,		4, },
		{ HEADERROR_LOWTEMP,			STROBESTATE_OFF,	STROBESTATE_OFF,	STROBESTATE_FLASH,		4, },
		{ HEADERROR_HIGHVOLTAGE,		STROBESTATE_OFF,	STROBESTATE_OFF,	STROBESTATE_FLASH,		4, },
		{ HEADERROR_LOWINK,				STROBESTATE_OFF,	STROBESTATE_ON,		STROBESTATE_OFF,		4, },
		{ HEADERROR_OUTOFINK,			STROBESTATE_OFF,	STROBESTATE_OFF,	STROBESTATE_FLASH,		4, },
		{ ERROR_NETWORK,				STROBESTATE_OFF,	STROBESTATE_ON,		STROBESTATE_OFF,		60, },
		{ HEADERROR_EP8,				STROBESTATE_OFF,	STROBESTATE_OFF,	STROBESTATE_ON,			0, },
	#else
		{ HEADERROR_READY,				STROBESTATE_ON,		STROBESTATE_OFF,	STROBESTATE_OFF,		4, },
		{ HEADERROR_APSCYCLE,			STROBESTATE_OFF,	STROBESTATE_OFF,	STROBESTATE_OFF,		4, },
		{ HEADERROR_LOWTEMP,			STROBESTATE_OFF,	STROBESTATE_OFF,	STROBESTATE_ON,			4, },
		{ HEADERROR_HIGHVOLTAGE,		STROBESTATE_OFF,	STROBESTATE_OFF,	STROBESTATE_ON,			4, },
		{ HEADERROR_LOWINK,				STROBESTATE_OFF,	STROBESTATE_ON,		STROBESTATE_OFF,		4, },
		{ HEADERROR_OUTOFINK,			STROBESTATE_OFF,	STROBESTATE_OFF,	STROBESTATE_ON,			4, },
	#endif
	};

	v.RemoveAll ();

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		SETTINGSSTRUCT s;
		CStrobeItem item;
		CString strKey = _T ("Control::Error::") + CStrobeItem::GetErrorString (map [i].m_type);

		if (!IsNetworked () && map [i].m_type == ERROR_NETWORK)
			continue;

		if (!GetSettingsRecord (db, 0, strKey, s)) {
			item.m_strName	= CStrobeItem::GetErrorString (map [i].m_type);
			item.m_green	= map [i].m_green;
			item.m_yellow	= map [i].m_yellow;
			item.m_red		= map [i].m_red;
			item.m_nDelay	= map [i].m_nDelay;
		}
		else
			item.FromString (s.m_strData);

		v.Add (item);
	}
}

void CStrobeDlg::OnOK ()
{
	m_vStates.RemoveAll ();

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		CStrobeItem item, * pItem = (CStrobeItem *)m_lv.GetCtrlData (i);

		item = * pItem;
		m_vStates.Add (item);
	}

	Save ();
	FoxjetCommon::CEliteDlg::OnOK ();
}

void CStrobeDlg::Save (FoxjetDatabase::COdbcDatabase & db, CArray <CStrobeItem, CStrobeItem &> & v)
{
	for (int i = 0; i < v.GetSize (); i++) {
		SETTINGSSTRUCT s;
		CStrobeItem & item = v [i];
		
		s.m_lLineID = 0;
		s.m_strKey = _T ("Control::Error::") + item.m_strName;
		s.m_strData = item.ToString ();

		if (!UpdateSettingsRecord (db, s))
			VERIFY (AddSettingsRecord (db, s));
	}
}

void CStrobeDlg::Save ()
{
	Save (m_db, m_vStates);
}
