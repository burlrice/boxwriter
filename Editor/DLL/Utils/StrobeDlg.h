#if !defined(AFX_STROBEDLG_H__81977C0C_3FCF_4040_BC5B_12EE3C5FCEE5__INCLUDED_)
#define AFX_STROBEDLG_H__81977C0C_3FCF_4040_BC5B_12EE3C5FCEE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StrobeDlg.h : header file
//

#include "ListCtrlImp.h"
#include "DbTypeDefs.h"
#include "Database.h"
#include "RoundButtons.h"
#include "utils.h"

/////////////////////////////////////////////////////////////////////////////
// CStrobeDlg dialog

namespace FoxjetCommon
{
	class UTIL_API CStrobeItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		virtual CString GetDispText (int nColumn) const;

		static CString GetStrobeState (FoxjetDatabase::STROBESTATE s);
		static FoxjetDatabase::STROBESTATE GetStrobeType (const CString & str);
		static CString GetErrorString (FoxjetDatabase::HEADERRORTYPE s);
		static FoxjetDatabase::HEADERRORTYPE GetErrorType (const CString & str);

		CString						m_strName;
		FoxjetDatabase::STROBESTATE m_green;
		FoxjetDatabase::STROBESTATE m_red;
		FoxjetDatabase::STROBESTATE m_yellow;
		int							m_nDelay;

		CString ToString () const;
		bool FromString (const CString & str);
	};

	class UTIL_API CStrobeDlg : public FoxjetCommon::CEliteDlg
	{
	public:

	// Construction
	public:
		CStrobeDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection, CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CStrobeDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA


		static void Load (FoxjetDatabase::COdbcDatabase & db, CArray <CStrobeItem, CStrobeItem &> & v);
		static void Save (FoxjetDatabase::COdbcDatabase & db, CArray <CStrobeItem, CStrobeItem &> & v);

		void Save ();

		virtual void OnOK ();

		CArray <CStrobeItem, CStrobeItem &> m_vStates;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CStrobeDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		FoxjetUtils::CRoundButtons m_buttons;
		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
		FoxjetDatabase::COdbcDatabase & m_db;
		const CString m_strRegSection;

		// Generated message map functions
		//{{AFX_MSG(CStrobeDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnDblclkError(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnEdit();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; // namespace FoxjetCommon

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STROBEDLG_H__81977C0C_3FCF_4040_BC5B_12EE3C5FCEE5__INCLUDED_)
