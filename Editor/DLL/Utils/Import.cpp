#include "stdafx.h"
#include "Utils.h"
#include "Registry.h"
#include "Resource.h"
#include "Debug.h"
#include "AppVer.h"
#include "Export.h"
#include "OdbcRecordset.h"
#include "Serialize.h"
#include "FieldDefs.h"
#include "Parse.h"
#include "Utils.h"

#define DEBUG_STRINGFCTS

using namespace FoxjetCommon;
using namespace FoxjetDatabase;

////////////////////////////////////////////////////////////////////////////////

void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::LINESTRUCT & s)				{ rst << s; }
void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::BOXSTRUCT & s)				{ rst << s; }
void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::BOXLINKSTRUCT & s)			{ rst << s; }
void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::HEADSTRUCT & s)				{ rst << s; }
void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::PANELSTRUCT & s)			{ rst << s; }
void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::OPTIONSLINKSTRUCT & s)		{ rst << s; }
void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::USERSTRUCT & s)				{ rst << s; }
void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::SECURITYGROUPSTRUCT & s)	{ rst << s; }
void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::SHIFTSTRUCT & s)			{ rst << s; }
void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::REPORTSTRUCT & s)			{ rst << s; }
void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::BCSCANSTRUCT & s)			{ rst << s; }
void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::DELETEDSTRUCT & s)			{ rst << s; }
void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::SETTINGSSTRUCT & s)			{ rst << s; }
void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::IMAGESTRUCT & s)			{ rst << s; }
void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::PRINTERSTRUCT & s)			{ rst << s; }

////////////////////////////////////////////////////////////////////////////////////////////////////

void UTIL_API FoxjetCommon::UpdateRecord (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::OPTIONSSTRUCT & s)
{
//	rst.SetEditMode (COdbcRecordset::addnew);
	rst << s;
}

void UTIL_API FoxjetCommon::UpdateRecord (COdbcRecordset & rst, TASKSTRUCT & s)
{
	using namespace Tasks;

	try {
		rst << s;
		/* TODO: rem
		rst.SetFieldValue (m_lpszLineID,			(long)s.m_lLineID);
		rst.SetFieldValue (m_lpszBoxID,				(long)s.m_lBoxID);
		rst.SetFieldValue (m_lpszName,				s.m_strName);
		rst.SetFieldValue (m_lpszDesc,				s.m_strDesc);
		rst.SetFieldValue (m_lpszDownload,			s.m_strDownload);
		rst.SetFieldValue (m_lpszTransformation,	(long)s.m_lTransformation);
		*/

		//UpdateMessages (rst.GetOdbcDatabase (), * (TASKSTRUCT *)(LPVOID)&s);
		{
			CString strSQL;

			strSQL.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%d;"), Messages::m_lpszTable, Messages::m_lpszTaskID, s.m_lID);
			TRACEF (strSQL);
			rst.GetOdbcDatabase ().ExecuteSQL (strSQL);

			for (int i = 0; i < s.m_vMsgs.GetSize (); i++) {
				MESSAGESTRUCT msg = s.m_vMsgs [i];

				VERIFY (AddMessageRecord (rst.GetOdbcDatabase (), msg));

				#ifdef _DEBUG
				if (s.m_vMsgs [i].m_lID != msg.m_lID) {
					CString str;

					str.Format (_T ("%d --> %d"), 
						s.m_vMsgs [i].m_lID, 
						msg.m_lID);
					TRACEF (str);
				}
				#endif //_DEBUG

				TASKSTRUCT * pTask = (TASKSTRUCT *)(LPVOID)&s;
				pTask->m_vMsgs.SetAt (i, msg);
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }
}

int UTIL_API FoxjetCommon::Find (const CString & strStruct, const CImportArray & v) 
{
	for (int i = 0; i < v.GetSize (); i++)
		if (!v [i].m_strStruct.CompareNoCase (strStruct))
			return i;

	return -1;
}



////////////////////////////////////////////////////////////////////////////////////////////////////

int UTIL_API FoxjetCommon::OverwriteRecord (FoxjetDatabase::COdbcDatabase & db, CArray <BOXSTRUCT, BOXSTRUCT &> & v,
							CImportArray & vImport)
{
	int nResult = 0;

	try {
		CArray <LINESTRUCT, LINESTRUCT &> vLines;
		COdbcRecordset rstLinks (&db);
		/* TODO: rem
		CString str;
		
		str.Format (_T ("SELECT * FROM [%s];"), BoxToLine::m_lpszTable);
		rstLinks.Open (str, CRecordset::dynaset);
		*/

		GetPrinterLines (db, GetPrinterID (db), vLines); //GetLineRecords (db, vLines);

		for (int i = 0; i < v.GetSize (); i++) {
			BOXSTRUCT box = v [i];
			ULONG lID = GetBoxID (db, box.m_strName);

			if (lID != NOTFOUND) 
				DeleteBoxRecord (db, lID);

			// used by all lines
			for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
				BOXLINKSTRUCT link;

				//rstLinks.AddNew ();
				link.m_lBoxID = box.m_lID;
				link.m_lLineID = vLines [nLine].m_lID;
				rstLinks << link;
				//rstLinks.Update ();

				nResult++;
			}
		}

		//rstLinks.Close ();

		return UpdateRecords <BOXSTRUCT> (db, v, Boxes::m_lpszTable, false); 
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }

	return nResult;
}

int UTIL_API FoxjetCommon::OverwriteRecord (FoxjetDatabase::COdbcDatabase & db, CArray <TASKSTRUCT, TASKSTRUCT &> & v, CImportArray & vImport)
{
	for (int i = 0; i < v.GetSize (); i++) {
		TASKSTRUCT task = v [i];
		LINESTRUCT line;

		if (GetLineRecord (db, task.m_lLineID, line)) {
			ULONG lID = GetTaskID (db, task.m_strName, line.m_strName, GetPrinterID (db));
			
			if (lID != NOTFOUND) 
				DeleteTaskRecord (db, lID);
		}
	}

	return UpdateRecords <TASKSTRUCT> (db, v, Tasks::m_lpszTable, false); 
}


int UTIL_API FoxjetCommon::Import (FoxjetDatabase::COdbcDatabase & db, const CStringArray & v,
								   DWORD dwFlags)
{
	// NOTE: used by Control.  Editor uses CImportFileDlg::Import
	using namespace FoxjetCommon;

	DBMANAGETHREADSTATE (db);

#ifdef _DEBUG
	{
		struct
		{
			DWORD m_dw;
			LPCTSTR m_lpsz;
		}
		static const map [] =
		{
			{ IMPORT_LINESTRUCT,			_T ("IMPORT_LINESTRUCT"),			},
			{ IMPORT_BOXSTRUCT,				_T ("IMPORT_BOXSTRUCT"),			},
			{ IMPORT_BOXLINKSTRUCT,			_T ("IMPORT_BOXLINKSTRUCT"),		},
			{ IMPORT_TASKSTRUCT,			_T ("IMPORT_TASKSTRUCT"),			},
			{ IMPORT_HEADSTRUCT,			_T ("IMPORT_HEADSTRUCT"),			},
			{ IMPORT_PANELSTRUCT,			_T ("IMPORT_PANELSTRUCT"),			},
			{ IMPORT_OPTIONSSTRUCT,			_T ("IMPORT_OPTIONSSTRUCT"),		},
			{ IMPORT_OPTIONSLINKSTRUCT,		_T ("IMPORT_OPTIONSLINKSTRUCT"),	},
			{ IMPORT_USERSTRUCT,			_T ("IMPORT_USERSTRUCT"),			},
			{ IMPORT_SECURITYGROUPSTRUCT,	_T ("IMPORT_SECURITYGROUPSTRUCT"),	},
			{ IMPORT_SHIFTSTRUCT,			_T ("IMPORT_SHIFTSTRUCT"),			},
			{ IMPORT_REPORTSTRUCT,			_T ("IMPORT_REPORTSTRUCT"),			},
			{ IMPORT_BCSCANSTRUCT,			_T ("IMPORT_BCSCANSTRUCT"),			},
			{ IMPORT_DELETEDSTRUCT,			_T ("IMPORT_DELETEDSTRUCT"),		},
			{ IMPORT_SETTINGSSTRUCT,		_T ("IMPORT_SETTINGSSTRUCT"),		},
			{ IMPORT_IMAGESTRUCT,			_T ("IMPORT_IMAGESTRUCT"),			},
			{ IMPORT_PRINTERSTRUCT,			_T ("IMPORT_PRINTERSTRUCT"),		},
			{ IMPORT_ALL,					_T ("IMPORT_ALL"),					},
		};

		for (int i = 0; i < ARRAYSIZE (map); i++)
			if ((dwFlags & map [i].m_dw) == map [i].m_dw)
				TRACEF (map [i].m_lpsz);
	}
#endif //_DEBUG

	int nResult = 0;

	try {
		CImportArray vResult;

		DECLAREIMPORT (LINESTRUCT);
		DECLAREIMPORT (BOXSTRUCT);
		DECLAREIMPORT (BOXLINKSTRUCT);
		DECLAREIMPORT (TASKSTRUCT);	
		DECLAREIMPORT (HEADSTRUCT);	
		DECLAREIMPORT (PANELSTRUCT);
		DECLAREIMPORT (OPTIONSSTRUCT);
		DECLAREIMPORT (OPTIONSLINKSTRUCT);
		DECLAREIMPORT (USERSTRUCT);	
		DECLAREIMPORT (SECURITYGROUPSTRUCT);
		DECLAREIMPORT (SHIFTSTRUCT);
		DECLAREIMPORT (REPORTSTRUCT);
		DECLAREIMPORT (BCSCANSTRUCT);
		DECLAREIMPORT (DELETEDSTRUCT);
		DECLAREIMPORT (SETTINGSSTRUCT);
		DECLAREIMPORT (IMAGESTRUCT);
		DECLAREIMPORT (PRINTERSTRUCT); 

		for (int i = 0; i < v.GetSize (); i++) {
			CString str = v [i];
			bool bResult = false;
			
			if (!bResult && (dwFlags & IMPORT_LINESTRUCT			)) bResult = PARSERECORD (LINESTRUCT,			str);
			if (!bResult && (dwFlags & IMPORT_BOXSTRUCT				)) bResult = PARSERECORD (BOXSTRUCT,			str);
			if (!bResult && (dwFlags & IMPORT_BOXLINKSTRUCT			)) bResult = PARSERECORD (BOXLINKSTRUCT,		str);
			if (!bResult && (dwFlags & IMPORT_TASKSTRUCT			)) bResult = PARSERECORD (TASKSTRUCT,			str);	
			if (!bResult && (dwFlags & IMPORT_HEADSTRUCT			)) bResult = PARSERECORD (HEADSTRUCT,			str);	
			if (!bResult && (dwFlags & IMPORT_PANELSTRUCT			)) bResult = PARSERECORD (PANELSTRUCT,			str);
			if (!bResult && (dwFlags & IMPORT_OPTIONSSTRUCT			)) bResult = PARSERECORD (OPTIONSSTRUCT,		str);
			if (!bResult && (dwFlags & IMPORT_OPTIONSLINKSTRUCT		)) bResult = PARSERECORD (OPTIONSLINKSTRUCT,	str);
			if (!bResult && (dwFlags & IMPORT_USERSTRUCT			)) bResult = PARSERECORD (USERSTRUCT,			str);	
			if (!bResult && (dwFlags & IMPORT_SECURITYGROUPSTRUCT	)) bResult = PARSERECORD (SECURITYGROUPSTRUCT,	str);
			if (!bResult && (dwFlags & IMPORT_SHIFTSTRUCT			)) bResult = PARSERECORD (SHIFTSTRUCT,			str);
			if (!bResult && (dwFlags & IMPORT_REPORTSTRUCT			)) bResult = PARSERECORD (REPORTSTRUCT,			str);
			if (!bResult && (dwFlags & IMPORT_BCSCANSTRUCT			)) bResult = PARSERECORD (BCSCANSTRUCT,			str);
			if (!bResult && (dwFlags & IMPORT_DELETEDSTRUCT			)) bResult = PARSERECORD (DELETEDSTRUCT,		str);
			if (!bResult && (dwFlags & IMPORT_SETTINGSSTRUCT		)) bResult = PARSERECORD (SETTINGSSTRUCT,		str);
			if (!bResult && (dwFlags & IMPORT_IMAGESTRUCT			)) bResult = PARSERECORD (IMAGESTRUCT,			str);
			
			if (db.HasTable (Printers::m_lpszTable))
				if (!bResult && (dwFlags & IMPORT_PRINTERSTRUCT		)) bResult = PARSERECORD (PRINTERSTRUCT,		str); 

			if (!bResult) {
				TRACEF ("Failed to parse\n\t" + str);
				ASSERT (0);
			}
			else
				nResult++;
		}

		if (dwFlags & IMPORT_LINESTRUCT				) ADDRECORD (db, LINESTRUCT,			Lines::m_lpszTable,				vResult);
		if (dwFlags & IMPORT_BOXSTRUCT				) ADDRECORD (db, BOXSTRUCT,				Boxes::m_lpszTable,				vResult);
		if (dwFlags & IMPORT_BOXLINKSTRUCT			) ADDRECORD (db, BOXLINKSTRUCT,			BoxToLine::m_lpszTable,			vResult);
		if (dwFlags & IMPORT_TASKSTRUCT				) ADDRECORD (db, TASKSTRUCT,			Tasks::m_lpszTable,				vResult);
		if (dwFlags & IMPORT_HEADSTRUCT				) ADDRECORD (db, HEADSTRUCT,			Heads::m_lpszTable,				vResult);
		if (dwFlags & IMPORT_PANELSTRUCT			) ADDRECORD (db, PANELSTRUCT,			Panels::m_lpszTable,			vResult);
		if (dwFlags & IMPORT_OPTIONSSTRUCT			) ADDRECORD (db, OPTIONSSTRUCT,			Options::m_lpszTable,			vResult);
		if (dwFlags & IMPORT_OPTIONSLINKSTRUCT		) ADDRECORD (db, OPTIONSLINKSTRUCT,		OptionsToGroup::m_lpszTable,	vResult);
		if (dwFlags & IMPORT_USERSTRUCT				) ADDRECORD (db, USERSTRUCT,			Users::m_lpszTable,				vResult);	
		if (dwFlags & IMPORT_SECURITYGROUPSTRUCT	) ADDRECORD (db, SECURITYGROUPSTRUCT,	SecurityGroups::m_lpszTable,	vResult);
		if (dwFlags & IMPORT_SHIFTSTRUCT			) ADDRECORD (db, SHIFTSTRUCT,			Shifts::m_lpszTable,			vResult);
		if (dwFlags & IMPORT_REPORTSTRUCT			) ADDRECORD (db, REPORTSTRUCT,			Reports::m_lpszTable,			vResult);
		if (dwFlags & IMPORT_BCSCANSTRUCT			) ADDRECORD (db, BCSCANSTRUCT,			BarcodeScans::m_lpszTable,		vResult);	
		if (dwFlags & IMPORT_DELETEDSTRUCT			) ADDRECORD (db, DELETEDSTRUCT,			Deleted::m_lpszTable,			vResult);
		if (dwFlags & IMPORT_SETTINGSSTRUCT			) ADDRECORD (db, SETTINGSSTRUCT,		Settings::m_lpszTable,			vResult);
		if (dwFlags & IMPORT_IMAGESTRUCT			) ADDRECORD (db, IMAGESTRUCT,			Images::m_lpszTable,			vResult);
		
		if (db.HasTable (Printers::m_lpszTable))
			if (dwFlags & IMPORT_PRINTERSTRUCT		) ADDRECORD (db, PRINTERSTRUCT,			Printers::m_lpszTable,			vResult); 
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return nResult;	
}


bool UTIL_API FoxjetCommon::SqlInsert (FoxjetDatabase::COdbcDatabase & db, LPCTSTR lpszTable, const CString & strID, ULONG lID)
{
	using namespace FoxjetCommon;
	using namespace FoxjetDatabase;

	CString str;

	if (!_tcscmp (lpszTable, Heads::m_lpszTable))
		str.Format (_T ("INSERT INTO %s (\"%s\",\"%s\") VALUES ('%d','x%d')"), lpszTable, strID, Heads::m_lpszUID, lID, lID);
	else if (!_tcscmp (lpszTable, SecurityGroups::m_lpszTable))
		str.Format (_T ("INSERT INTO %s (\"%s\",\"%s\") VALUES ('%d','x%d')"), lpszTable, strID, SecurityGroups::m_lpszName, lID, lID);
	else if (!_tcscmp (lpszTable, Users::m_lpszTable))
		str.Format (_T ("INSERT INTO %s (\"%s\",\"%s\",\"%s\",\"%s\",\"%s\") VALUES ('%d','x%d','x%d','x%d','x%d')"), 
			lpszTable, 
			strID, Users::m_lpszFirstName, Users::m_lpszLastName, Users::m_lpszUserName, Users::m_lpszPassW, 
			lID, lID, lID, lID, lID);
	else if (!_tcscmp (lpszTable, Options::m_lpszTable))
		str.Format (_T ("INSERT INTO %s (\"%s\",\"%s\") VALUES ('%d','x%d')"), lpszTable, strID, Options::m_lpszOptionDesc, lID, lID);
	else
		str.Format (_T ("INSERT INTO %s (\"%s\") VALUES ('%d')"), lpszTable, strID, lID);

	TRACEF (str);
	return db.ExecuteSQL (str) > 0 ? true : false;
}
