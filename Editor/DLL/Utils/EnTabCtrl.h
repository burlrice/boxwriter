#if !defined(AFX_ENTABCTRL_H__38F5C380_E2DA_11D1_AB24_0000E8425C3E__INCLUDED_)
#define AFX_ENTABCTRL_H__38F5C380_E2DA_11D1_AB24_0000E8425C3E__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// EnTabCtrl.h : header file
//

#include "UtilApi.h"
#include "basetabctrl.h"
#include "RoundButtons.h"

namespace FoxjetCommon
{

	/////////////////////////////////////////////////////////////////////////////
	// CEnTabCtrl window

	// custom look
	enum
	{
		ETC_FLAT = 1, 
		ETC_COLOR = 2, // draw tabs with color
		ETC_SELECTION = 4, // highlight the selected tab
		ETC_GRADIENT = 8, // draw colors with a gradient
		ETC_BACKTABS = 16,
	};

	class UTIL_API CEnTabCtrl : public CBaseTabCtrl
	{
	// Construction
	public:
		CEnTabCtrl();

		static void EnableCustomLook(BOOL bEnable = TRUE, DWORD dwStyle = ETC_FLAT | ETC_COLOR);

	// Attributes
	public:

	protected:
		static DWORD s_dwCustomLook;

	// Operations
	public:

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CEnTabCtrl)
		protected:
		virtual void PreSubclassWindow();
		//}}AFX_VIRTUAL

	// Implementation
	public:
		virtual ~CEnTabCtrl();

		// Generated message map functions
	protected:
		virtual void DrawMainBorder(LPDRAWITEMSTRUCT lpDrawItemStruct);
		virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
		virtual void DrawItemBorder(LPDRAWITEMSTRUCT lpDrawItemStruct);
		virtual COLORREF GetTabColor(BOOL bSelected = FALSE);
		virtual COLORREF GetTabTextColor(BOOL bSelected = FALSE);

	};

	class UTIL_API CEnPropertySheet : public CPropertySheet
	{
	public:
		DECLARE_DYNAMIC (CEnPropertySheet)

		CEnPropertySheet ();
		CEnPropertySheet (UINT nIDCaption, CWnd *pParentWnd = NULL, UINT iSelectPage = 0);
		CEnPropertySheet (LPCTSTR pszCaption, CWnd *pParentWnd = NULL, UINT iSelectPage = 0);
		~CEnPropertySheet ();

		BOOL OnInitDialog ();
		HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
		afx_msg void OnHelp ();


		virtual void DoDataExchange( CDataExchange* pDX );

		static UINT CALLBACK PropSheetPageProc (HWND hwnd, UINT uMsg, LPPROPSHEETPAGE ppsp);
		static int CALLBACK PropSheetProc (HWND hwndDlg, UINT uMsg, LPARAM lParam);

	protected:

		FoxjetUtils::CRoundButtons m_buttons;

		DECLARE_MESSAGE_MAP()

	private:
		HBRUSH m_hbrDlg;
		CEnTabCtrl m_tab;
	};
/////////////////////////////////////////////////////////////////////////////
}; //namespace FoxjetCommon


//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ENTABCTRL_H__38F5C380_E2DA_11D1_AB24_0000E8425C3E__INCLUDED_)
