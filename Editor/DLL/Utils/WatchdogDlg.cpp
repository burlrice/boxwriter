// WatchdogDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "utils.h"
#include "StartupDlg.h"
#include "Database.h"
#include "ScannerPropDlg.h"
#include "Parse.h"
#include "WatchdogDlg.h"
#include "WdtTimeoutDlg.h"
#include "CardTimeoutDlg.h"
#include "TemplExt.h"
#include "SystemDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetUtils;
using namespace FoxjetDatabase;


/////////////////////////////////////////////////////////////////////////////
// CWatchdogDlg property page

//IMPLEMENT_DYNCREATE(CWatchdogDlg, CPropertyPage)

CWatchdogDlg::CWatchdogDlg (FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection) 
:	m_db (db),
	m_strRegSection (strRegSection),
	CPropertyPage (IDD_WATCHDOG),
	m_motherboard (MOTHERBOARD_REV3),
	m_lCardTimeout (30),
	m_lWDT (6),
	m_bWDT (false)
{
	//{{AFX_DATA_INIT(CWatchdogDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CWatchdogDlg::~CWatchdogDlg()
{
}

void CWatchdogDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWatchdogDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWatchdogDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CWatchdogDlg)
	ON_BN_CLICKED(BTN_WATCHDOGTIMEOUT, OnWatchdogtimeout)
	ON_BN_CLICKED(BTN_HEADTIMEOUT, OnHeadtimeout)
	ON_BN_CLICKED(RDO_MOTHERBOARD1, OnMotherboard)
	ON_BN_CLICKED(CHK_ENABLED, OnEnabled)
	ON_BN_CLICKED(RDO_MOTHERBOARD2, OnMotherboard)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWatchdogDlg message handlers

BOOL CWatchdogDlg::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	if (CButton * p = (CButton *)GetDlgItem (RDO_MOTHERBOARD1))
		p->SetCheck (m_motherboard == MOTHERBOARD_REV2 ? 1 : 0);
	if (CButton * p = (CButton *)GetDlgItem (RDO_MOTHERBOARD2))
		p->SetCheck (m_motherboard == MOTHERBOARD_REV3 ? 1 : 0);
	if (CButton * p = (CButton *)GetDlgItem (CHK_ENABLED))
		p->SetCheck (m_bWDT ? 1 : 0);

	InitWDT ();
	InitCard ();
	OnEnabled ();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CWatchdogDlg::InitWDT ()
{
	CString str = CWdtTimeoutDlg::FormatWDT (m_motherboard, m_lWDT);

	SetDlgItemText (TXT_WATCHDOGTIMEOUT, str);
}

void CWatchdogDlg::InitCard ()
{
	CString str;

	str.Format (_T ("%d %s"), m_lCardTimeout, LoadString (IDS_SEC));

	SetDlgItemText (TXT_HEADTIMEOUT, str);
}

void CWatchdogDlg::OnWatchdogtimeout() 
{
	CWdtTimeoutDlg dlg (this);

	if (CButton * p = (CButton *)GetDlgItem (RDO_MOTHERBOARD1))
		dlg.m_motherboard = p->GetCheck () ? MOTHERBOARD_REV2 : MOTHERBOARD_REV3;

	dlg.m_lWDT			= m_lWDT;

	if (dlg.DoModal () == IDOK) {
		m_lWDT = dlg.m_lWDT;
		InitWDT ();
	}
}

void CWatchdogDlg::OnHeadtimeout() 
{
	CCardTimeoutDlg dlg (this);

	dlg.m_nTimeout = m_lCardTimeout;

	if (dlg.DoModal () == IDOK) {
		m_lCardTimeout = dlg.m_nTimeout;
		InitCard ();
	}
}

void CWatchdogDlg::OnOK()
{
	if (CButton * p = (CButton *)GetDlgItem (RDO_MOTHERBOARD1))
		m_motherboard = p->GetCheck () ? MOTHERBOARD_REV2 : MOTHERBOARD_REV3;

	if (CButton * p = (CButton *)GetDlgItem (CHK_ENABLED))
		m_bWDT = p->GetCheck () ? true : false;

	CDialog::OnOK ();
}

void CWatchdogDlg::OnMotherboard() 
{
	if (CButton * p = (CButton *)GetDlgItem (RDO_MOTHERBOARD1))
		m_motherboard = p->GetCheck () ? MOTHERBOARD_REV2 : MOTHERBOARD_REV3;

	InitWDT ();
}

void CWatchdogDlg::OnEnabled() 
{
	UINT n [] = 
	{
		RDO_MOTHERBOARD1,
		RDO_MOTHERBOARD2,
		BTN_WATCHDOGTIMEOUT,
		BTN_HEADTIMEOUT,
	};

	if (CButton * pEnabled = (CButton *)GetDlgItem (CHK_ENABLED)) {
		bool bEnabled = pEnabled->GetCheck () ? true : false;

		for (int i = 0; i < ARRAYSIZE (n); i++) 
			if (CWnd * p = GetDlgItem (n [i]))
				p->EnableWindow (bEnabled);
	}
}

