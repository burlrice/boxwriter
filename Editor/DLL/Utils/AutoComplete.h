#pragma once

#include "UtilApi.h"
#include "afxwin.h"
#include <string>
#include <vector>

namespace FoxjetUtils
{
	class UTIL_API CAutoEdit : public CEdit
	{
	public:
		CAutoEdit (CWnd * pParent);

		virtual std::vector <std::wstring> OnTyping (const CString & str) = 0;
		virtual void OnAutoComplete (const CString & str);
		virtual double GetHeightFactor () const { return 2.5; }

		void Enable (bool bEnable = true);

		void DoDataExchange (CDataExchange * pDX, UINT nID);

		static const CString m_strMore;

	protected:
		class CListBoxWnd : public CFrameWnd
		{
		public:
			CListBoxWnd (CAutoEdit * pParent, const std::vector <std::wstring> & v, const CRect & rc);
			virtual ~CListBoxWnd ();

		protected:
			class CListBoxEx : public CListBox
			{
			public:
				CListBoxEx (const std::vector <std::wstring> & v, CAutoEdit * pParent);

				const std::vector <std::wstring> m_v;

				int GetText (int nIndex, LPTSTR lpszBuffer) const;
				void GetText (int nIndex, CString& rString) const; 
				CString GetText (int nIndex) const; 

			protected:
				virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
				virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
				virtual void MeasureItem (LPMEASUREITEMSTRUCT lpMeasureItemStruct);

				CAutoEdit * m_pParent;
			};

			DECLARE_MESSAGE_MAP()

			afx_msg void OnSize(UINT nType, int cx, int cy);
			afx_msg void OnSelChange ();
			afx_msg void OnNcPaint ();
			afx_msg LRESULT OnArrowKey (WPARAM wParam, LPARAM lParam);

			CListBoxEx * m_pLB;
			CAutoEdit * m_pParent;
		};

		void OnValueChanged ();
		void Dismiss ();

		virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

		CWnd * m_pParent;
		CListBoxWnd * m_pAutoList;
		bool m_bEnabled;
		WORD m_wLastKey;
		UINT m_nCtrlID;
	};
}; // namespace FoxjetUtils