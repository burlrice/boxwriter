// SysIdDlg.cpp : implementation file
//

#include "stdafx.h"
#include "utils.h"
#include "SysIdDlg.h"
#include "SystemDlg.h"
#include "Database.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSysIdDlg property page

IMPLEMENT_DYNCREATE(CSysIdDlg, CPropertyPage)

CSysIdDlg::CSysIdDlg() 
:	m_hbrDlg (NULL),
	CPropertyPage(IDD_SYSID_MATRIX)
{
	//{{AFX_DATA_INIT(CSysIdDlg)
	m_strSysID = _T("");
	//}}AFX_DATA_INIT
	m_psp.dwFlags |= PSH_USECALLBACK;
	m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CSysIdDlg::~CSysIdDlg()
{
	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}
}

void CSysIdDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSysIdDlg)
	DDX_Text(pDX, TXT_SYSID, m_strSysID);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSysIdDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CSysIdDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSysIdDlg message handlers

HBRUSH CSysIdDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = ::GetSysColor (COLOR_BTNFACE);//FoxjetUtils::GetButtonColor (_T ("background"));
	
	if (FoxjetDatabase::IsControl (false)) 
		rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	if (!m_hbrDlg)	
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}
