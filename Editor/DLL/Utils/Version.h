// Version.h: interface for the CVersion class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VERSION_H__D1646AC5_32DC_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_VERSION_H__D1646AC5_32DC_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afx.h>
#include "UtilApi.h"
#include "DllVer.h"

namespace FoxjetCommon {
	class UTIL_API CVersion : public CObject  
	{
		DECLARE_SERIAL (CVersion)

	public:
		CVersion ();
		CVersion(UINT nMajor, UINT nMinor, UINT nCustomMajor = 0, UINT nCustomMinor = 0, UINT nInternal = 0);
		CVersion (const FoxjetCommon::VERSIONSTRUCT & ver);
		CVersion (const CVersion & rhs);
		CVersion (const CString & strIpVer);
		CVersion & operator = (const CVersion & rhs);

		operator FoxjetCommon::LPVERSIONSTRUCT () { return &m_ver; }
		operator FoxjetCommon::VERSIONSTRUCT () const { return m_ver; }

		virtual ~CVersion();

		bool FromString (const CString & str);
		CString ToString () const;

		UINT GetMajor () const { return m_ver.m_nMajor; }
		UINT GetMinor () const { return m_ver.m_nMinor; }
		UINT GetCustomMajor () const { return m_ver.m_nCustomMajor; }
		UINT GetCustomMinor () const { return m_ver.m_nCustomMinor; }
		UINT GetInternal () const { return m_ver.m_nInternal; }
		CString Format (bool bShowInternal = false) const;
		void Serialize (CArchive & ar);
		void Serialize (CArchive & ar) const;

		bool IsSynonym (const FoxjetCommon::CVersion & ver) const;
		bool IsCustom (const FoxjetCommon::CVersion & ver);

	protected:
		FoxjetCommon::VERSIONSTRUCT m_ver;
	};
}; // FoxjetCommon 

UTIL_API bool operator == (const FoxjetCommon::VERSIONSTRUCT & lhs, const FoxjetCommon::VERSIONSTRUCT & rhs);
UTIL_API bool operator != (const FoxjetCommon::VERSIONSTRUCT & lhs, const FoxjetCommon::VERSIONSTRUCT & rhs);
UTIL_API bool operator >  (const FoxjetCommon::VERSIONSTRUCT & lhs, const FoxjetCommon::VERSIONSTRUCT & rhs);
UTIL_API bool operator >= (const FoxjetCommon::VERSIONSTRUCT & lhs, const FoxjetCommon::VERSIONSTRUCT & rhs);
UTIL_API bool operator <  (const FoxjetCommon::VERSIONSTRUCT & lhs, const FoxjetCommon::VERSIONSTRUCT & rhs);
UTIL_API bool operator <= (const FoxjetCommon::VERSIONSTRUCT & lhs, const FoxjetCommon::VERSIONSTRUCT & rhs);

#endif // !defined(AFX_VERSION_H__D1646AC5_32DC_11D4_8FC6_006067662794__INCLUDED_)
