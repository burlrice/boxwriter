// Quadruple.cpp: implementation of the CQuadruple class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Quadruple.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace Foxjet3d;

_3D_API int Foxjet3d::Min (const CPoint & pt)
{
	return min (pt.x, pt.y);
}

_3D_API int Foxjet3d::Max (const CPoint & pt)
{
	return max (pt.x, pt.y);
}

_3D_API int Foxjet3d::ThousandthsToLogical (int n)
{
	return ThousandthsToLogical (CPoint (n, 0)).x;
}

_3D_API int Foxjet3d::ThousandthsToLogical (int n, int nRes)
{
	return ThousandthsToLogical (CPoint (n, 0), CSize (nRes, nRes)).x;
}

/* TODO: rem
_3D_API CPoint Foxjet3d::ThousandthsToLogical (const CPoint & pt)
{
	HDC hDC = ::GetDC (NULL);
	int n [2] = { ::GetDeviceCaps (hDC, LOGPIXELSX), ::GetDeviceCaps (hDC, LOGPIXELSY) };
	double d [2] = { (double)pt.x / 1000.0, (double)pt.y / 1000.0 };

	::ReleaseDC (NULL, hDC);

	return CPoint ((int)(n [0] * d [0]), (int)(n [1] * d [1]));
}
*/

_3D_API CPoint Foxjet3d::ThousandthsToLogical (const CPoint & pt)
{
	HDC hDC = ::GetDC (NULL);
	int n [2] = { ::GetDeviceCaps (hDC, LOGPIXELSX), ::GetDeviceCaps (hDC, LOGPIXELSY) };

	CPoint ptResult = ThousandthsToLogical (pt, CSize (n [0], n [1]));

	::ReleaseDC (NULL, hDC);

	return ptResult;
}

_3D_API CPoint Foxjet3d::ThousandthsToLogical (const CPoint & pt, const CSize & res)
{
	int n [2] = { res.cx, res.cy };
	double d [2] = { (double)pt.x / 1000.0, (double)pt.y / 1000.0 };

	return CPoint ((int)(n [0] * d [0]), (int)(n [1] * d [1]));
}

_3D_API int Foxjet3d::LogicalToThousandths (int n)
{
	return LogicalToThousandths (CPoint (n, 0)).x;
}

_3D_API int Foxjet3d::LogicalToThousandths (int n, int nRes)
{
	return LogicalToThousandths (CPoint (n, 0), CSize (nRes, nRes)).x;
}

/* TODO: rem
_3D_API CPoint Foxjet3d::LogicalToThousandths (const CPoint & pt)
{
	HDC hDC = ::GetDC (NULL);
	int n [2] = { ::GetDeviceCaps (hDC, LOGPIXELSX), ::GetDeviceCaps (hDC, LOGPIXELSY) };
	ASSERT (n [0] != 0);	
	ASSERT (n [1] != 0);
	double d [2] = { (double)pt.x, (double)pt.y };

	::ReleaseDC (NULL, hDC);
	
	return CPoint ((int)((d [0] / n [0]) * 1000), (int)((d [1] / n [1]) * 1000));
}
*/

_3D_API CPoint Foxjet3d::LogicalToThousandths (const CPoint & pt)
{
	HDC hDC = ::GetDC (NULL);
	int n [2] = { ::GetDeviceCaps (hDC, LOGPIXELSX), ::GetDeviceCaps (hDC, LOGPIXELSY) };

	CPoint ptResult = LogicalToThousandths (pt, CSize (n [0], n [1]));

	::ReleaseDC (NULL, hDC);
	
	return ptResult;
}

_3D_API CPoint Foxjet3d::LogicalToThousandths (const CPoint & pt, const CSize & res)
{
	HDC hDC = ::GetDC (NULL);
	int n [2] = { res.cx, res.cy };
	ASSERT (n [0] != 0);	
	ASSERT (n [1] != 0);
	double d [2] = { (double)pt.x, (double)pt.y };

	::ReleaseDC (NULL, hDC);
	
	return CPoint ((int)((d [0] / n [0]) * 1000), (int)((d [1] / n [1]) * 1000));
}
