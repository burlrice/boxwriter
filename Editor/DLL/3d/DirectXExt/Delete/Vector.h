// Vector.h: interface for the CVector class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VECTOR_H__8DEAB767_4D8E_4C60_8C97_B21C0E3F01F1__INCLUDED_)
#define AFX_VECTOR_H__8DEAB767_4D8E_4C60_8C97_B21C0E3F01F1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "..\3dApi.h"

namespace Foxjet3d 
{
	class _3D_API CVector : public CQuadruple <REAL> 
	{
	public:
		CVector (REAL x = 0, REAL y = 0, REAL z = 0);
		CVector (const CVector & rhs);
		CVector (const CQuadruple <REAL> & rhs);
		CVector & operator = (const CVector & rhs);
		virtual ~CVector();

		CVector & Normalize ();

		friend CVector CrossProduct (const CVector & rhs, const CVector & lhs);
		friend REAL DotProduct (const CVector & rhs, const CVector & lhs);
	};
}; //namespace Foxjet3d

#endif // !defined(AFX_VECTOR_H__8DEAB767_4D8E_4C60_8C97_B21C0E3F01F1__INCLUDED_)
