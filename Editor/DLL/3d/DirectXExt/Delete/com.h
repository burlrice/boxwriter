#ifndef __COM_H__
#define __COM_H__

#include <atlbase.h>
#include "debug.h"

template <class T> class CDxComPtr;

LPUNKNOWN ComCast (REFIID iid, LPUNKNOWN lpFrom, LPCTSTR lpszIIDFrom, 
				   LPCTSTR lpszFrom, bool bDebugWarnNull, 
				   LPCTSTR lpszFile, UINT nLine);

template <class T> 
T * ComTemplateCast (REFIID iid, LPUNKNOWN lpFrom, LPCTSTR lpszIIDFrom, 
					 LPCTSTR lpszObjFrom, bool bDebugWarnNull,
					 LPCTSTR lpszFile, UINT nLine)
{
	return (T *)ComCast (iid, lpFrom, lpszIIDFrom, lpszObjFrom, 
		bDebugWarnNull, lpszFile, nLine);
}

template <class T> 
CComPtr <T> ComPtrCast (REFIID iid, LPUNKNOWN lpFrom, LPCTSTR lpszIIDFrom, 
						LPCTSTR lpszFrom, bool bDebugWarnNull,
						LPCTSTR lpszFile, UINT nLine)
{
	LPUNKNOWN lpResult = ComCast (iid, lpFrom, lpszIIDFrom, lpszFrom, 
		bDebugWarnNull, lpszFile, nLine);
	CComPtr <T> result ((T *)lpResult);
	
	if (lpResult)
		lpResult->Release ();

	return result;
}

#define RELEASE(p) \
	if (p) { \
		p->Release (); \
		p = NULL; \
	}\

#ifdef _DEBUG
	#define COMCAST(a,b)			ComTemplateCast <a> (IID_##a, b, "IID_" #a, #b, true, __FILE__, __LINE__)
	#define COMPTRCAST(a,b)			ComPtrCast <a> (IID_##a, b, "IID_" #a, #b, true, __FILE__, __LINE__)

	#define UNSAFECOMCAST(a,b)		ComTemplateCast <a> (IID_##a, b, "IID_" #a, #b, false, __FILE__, __LINE__)
	#define UNSAFECOMPTRCAST(a,b)	ComPtrCast <a> (IID_##a, b, "IID_" #a, #b, false, __FILE__, __LINE__)
#else //!_DEBUG
	#define COMCAST(a,b)			ComTemplateCast <a> (IID_##a, b, "IID_" #a, #b, false, __FILE__, __LINE__)
	#define COMPTRCAST(a,b)			ComPtrCast <a> (IID_##a, b, "IID_" #a, #b, false, __FILE__, __LINE__)

	#define UNSAFECOMCAST(a,b)		COMCAST(a,b)
	#define UNSAFECOMPTRCAST(a,b)	COMPTRCAST(a,b)
#endif

typedef struct tagGUIDSTRUCT
{
	tagGUIDSTRUCT (GUID guid) : m_guid (guid) { }
	GUID m_guid;
} GUIDSTRUCT;

#endif //__COM_H__
