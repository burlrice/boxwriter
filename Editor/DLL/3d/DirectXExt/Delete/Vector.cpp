// Vector.cpp: implementation of the CVector class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <math.h>

#include "Vector.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace Foxjet3d;

namespace Foxjet3d {
	CVector CrossProduct (const CVector & rhs, const CVector & lhs)
	{
		REAL x = rhs.GetY () * rhs.GetZ () - rhs.GetZ () * lhs.GetY ();
		REAL y = rhs.GetZ () * lhs.GetX () - rhs.GetX () * lhs.GetZ ();
		REAL z = rhs.GetX () * lhs.GetY () - rhs.GetY () * lhs.GetX ();

		return CVector (x, y, z);
	}

	
	REAL DotProduct (const CVector & rhs, const CVector & lhs)
	{
		return 
			rhs.GetX () * rhs.GetX () +
			rhs.GetY () * rhs.GetY () +
			rhs.GetZ () * rhs.GetZ ();
	}
};


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CVector::CVector (REAL x, REAL y, REAL z)
:	CQuadruple <REAL> (x, y, z, 0)
{
}

CVector::CVector (const CVector & rhs)
:	CQuadruple <REAL> (rhs)
{
}

CVector::CVector (const CQuadruple <REAL> & rhs)
:	CQuadruple <REAL> (rhs)
{
}

CVector & CVector::operator = (const CVector & rhs)
{
	CQuadruple <REAL>::operator = (rhs);
	return * this;
}

CVector::~CVector()
{
}

CVector & CVector::Normalize ()
{
	REAL rLength = sqrt (
		(GetX () * GetX ()) + 
		(GetY () * GetY ()) + 
		(GetZ () * GetZ ()));

	SetX (GetX () / rLength);
	SetY (GetY () / rLength);
	SetZ (GetZ () / rLength);

	return *this;
}

