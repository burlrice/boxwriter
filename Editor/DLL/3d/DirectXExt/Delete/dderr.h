#ifndef __DDERR_H__
#define __DDERR_H__

#include "..\3dApi.h"

HRESULT  _3D_API HandleDDErr (HRESULT hErr, LPCTSTR lpszFile, UINT nLine);
HRESULT  _3D_API TraceDDErr (HRESULT hErr, LPCTSTR lpszContext, LPCTSTR lpszFile, UINT nLine);

#ifdef _DEBUG
	#define HANDLEDDERR(h)	HandleDDErr (h, __FILE__, __LINE__)
	#define TRACEDDERR(h)	TraceDDErr	(h, NULL, __FILE__, __LINE__)
	#define DDCALL(s)		TraceDDErr (s, #s, __FILE__, __LINE__)
#else //!_DEBUG
	#define HANDLEDDERR(h) 
	#define TRACEDDERR(h)
	#define DDCALL(s) s
#endif //_DEBUG

#endif //__DDERR_H__
