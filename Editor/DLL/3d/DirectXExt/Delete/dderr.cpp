#include "stdafx.h"
#include "dderr.h"
#include "debug.h"

#if DIRECTDRAW_VERSION >= 0x0600
	#include <ddraw.h>
	#include <d3d.h>
	#include <d3drm.h>
#endif //DIRECTDRAW_VERSION >= 0x0600

typedef struct tagDDERRSTRUCT
{
	tagDDERRSTRUCT (const CString & str = "", const CString & strConstant = "", HRESULT hID = 0)
		: m_str (str), m_strConstant (strConstant), m_hID (hID) { }
	tagDDERRSTRUCT (const tagDDERRSTRUCT & rhs)
		: m_str (rhs.m_str), m_strConstant (rhs.m_strConstant), m_hID (rhs.m_hID) { }
	tagDDERRSTRUCT & operator = (const tagDDERRSTRUCT & rhs);

	CString m_str;
	CString m_strConstant;
	HRESULT m_hID;
} DDERRSTRUCT;

bool operator == (const DDERRSTRUCT & lhs, const DDERRSTRUCT & rhs)
{
	return 
		lhs.m_str == rhs.m_str && 
		lhs.m_strConstant == rhs.m_strConstant && 
		lhs.m_hID == rhs.m_hID;
}

bool operator != (const DDERRSTRUCT & lhs, const DDERRSTRUCT & rhs)
{
	return !(lhs == rhs);
}

tagDDERRSTRUCT & tagDDERRSTRUCT::operator = (const tagDDERRSTRUCT & rhs)
{
	if (this != &rhs) {
		m_str = rhs.m_str;
		m_strConstant = rhs.m_strConstant;
		m_hID = rhs.m_hID;
	}

	return * this;
}

#define MAKEDDERRSTRUCT(h, s) DDERRSTRUCT (s, #h, h)

static const DDERRSTRUCT end;
static const DDERRSTRUCT err [] = {
#if DIRECTDRAW_VERSION >= 0x0600

	MAKEDDERRSTRUCT (DDERR_ALREADYINITIALIZED, _T ("This object is already initialized")),
	MAKEDDERRSTRUCT (DDERR_CANNOTATTACHSURFACE, _T ("This surface can not be attached to the requested surface.")),
	MAKEDDERRSTRUCT (DDERR_CANNOTDETACHSURFACE, _T ("This surface can not be detached from the requested surface.")),
	MAKEDDERRSTRUCT (DDERR_CURRENTLYNOTAVAIL, _T ("Support is currently not available.")),
	MAKEDDERRSTRUCT (DDERR_EXCEPTION, _T ("An exception was encountered while performing the requested operation")),
	MAKEDDERRSTRUCT (DDERR_GENERIC, _T ("Generic failure.")),
	MAKEDDERRSTRUCT (DDERR_HEIGHTALIGN, _T ("Height of rectangle provided is not a multiple of reqd alignment")),
	MAKEDDERRSTRUCT (DDERR_INCOMPATIBLEPRIMARY, _T ("Unable to match primary surface creation request with existing primary surface.")),
	MAKEDDERRSTRUCT (DDERR_INVALIDCAPS, _T ("One or more of the caps bits passed to the callback are incorrect.")),
	MAKEDDERRSTRUCT (DDERR_INVALIDCLIPLIST, _T ("DirectDraw does not support provided Cliplist.")),
	MAKEDDERRSTRUCT (DDERR_INVALIDMODE, _T ("DirectDraw does not support the requested mode")),
	MAKEDDERRSTRUCT (DDERR_INVALIDOBJECT, _T ("DirectDraw received a pointer that was an invalid DIRECTDRAW object.")),
	MAKEDDERRSTRUCT (DDERR_INVALIDPARAMS, _T ("One or more of the parameters passed to the callback function are incorrect.")),
	MAKEDDERRSTRUCT (DDERR_INVALIDPIXELFORMAT, _T ("pixel format was invalid as specified")),
	MAKEDDERRSTRUCT (DDERR_INVALIDRECT, _T ("Rectangle provided was invalid.")),
	MAKEDDERRSTRUCT (DDERR_LOCKEDSURFACES, _T ("Operation could not be carried out because one or more surfaces are locked")),
	MAKEDDERRSTRUCT (DDERR_NO3D, _T ("There is no 3D present.")),
	MAKEDDERRSTRUCT (DDERR_NOALPHAHW, _T ("Operation could not be carried out because there is no alpha accleration hardware present or available.")),
	MAKEDDERRSTRUCT (DDERR_NOCLIPLIST, _T ("no clip list available")),
	MAKEDDERRSTRUCT (DDERR_NOCOLORCONVHW, _T ("Operation could not be carried out because there is no color conversion hardware present or available.")),
	MAKEDDERRSTRUCT (DDERR_NOCOOPERATIVELEVELSET, _T ("Create function called without DirectDraw object method SetCooperativeLevel being called.")),
	MAKEDDERRSTRUCT (DDERR_NOCOLORKEY, _T ("Surface doesn't currently have a color key")),
	MAKEDDERRSTRUCT (DDERR_NOCOLORKEYHW, _T ("Operation could not be carried out because there is no hardware support of the dest color key.")),
	MAKEDDERRSTRUCT (DDERR_NODIRECTDRAWSUPPORT, _T ("No DirectDraw support possible with current display driver")),
	MAKEDDERRSTRUCT (DDERR_NOEXCLUSIVEMODE, _T ("Operation requires the application to have exclusive mode but the application does not have exclusive mode.")),
	MAKEDDERRSTRUCT (DDERR_NOFLIPHW, _T ("Flipping visible surfaces is not supported.")),
	MAKEDDERRSTRUCT (DDERR_NOGDI, _T ("There is no GDI present.")),
	MAKEDDERRSTRUCT (DDERR_NOMIRRORHW, _T ("Operation could not be carried out because there is no hardware present or available.")),
	MAKEDDERRSTRUCT (DDERR_NOTFOUND, _T ("Requested item was not found")),
	MAKEDDERRSTRUCT (DDERR_NOOVERLAYHW, _T ("Operation could not be carried out because there is no overlay hardware present or available.")),
	MAKEDDERRSTRUCT (DDERR_NORASTEROPHW, _T ("Operation could not be carried out because there is no appropriate raster op hardware present or available.")),
	MAKEDDERRSTRUCT (DDERR_NOROTATIONHW, _T ("Operation could not be carried out because there is no rotation hardware present or available.")),
	MAKEDDERRSTRUCT (DDERR_NOSTRETCHHW, _T ("Operation could not be carried out because there is no hardware support for stretching")),
	MAKEDDERRSTRUCT (DDERR_NOT4BITCOLOR, _T ("DirectDrawSurface is not in 4 bit color palette and the requested operation requires 4 bit color palette.")),
	MAKEDDERRSTRUCT (DDERR_NOT4BITCOLORINDEX, _T ("DirectDrawSurface is not in 4 bit color index palette and the requested operation requires 4 bit color index palette.")),
	MAKEDDERRSTRUCT (DDERR_NOT8BITCOLOR, _T ("DirectDraw Surface is not in 8 bit color mode and the requested operation requires 8 bit color.")),
	MAKEDDERRSTRUCT (DDERR_NOTEXTUREHW, _T ("Operation could not be carried out because there is no texture mapping hardware present or available.")),
	MAKEDDERRSTRUCT (DDERR_NOVSYNCHW, _T ("Operation could not be carried out because there is no hardware support for vertical blank synchronized operations.")),
	MAKEDDERRSTRUCT (DDERR_NOZBUFFERHW, _T ("Operation could not be carried out because there is no hardware support for zbuffer blting.")),
	MAKEDDERRSTRUCT (DDERR_NOZOVERLAYHW, _T ("Overlay surfaces could not be z layered based on their BltOrder because the hardware does not support z layering of overlays.")),
	MAKEDDERRSTRUCT (DDERR_OUTOFCAPS, _T ("The hardware needed for the requested operation has already been allocated.")),
	MAKEDDERRSTRUCT (DDERR_OUTOFMEMORY, _T ("DirectDraw does not have enough memory to perform the operation.")),
	MAKEDDERRSTRUCT (DDERR_OUTOFVIDEOMEMORY, _T ("DirectDraw does not have enough memory to perform the operation.")),
	MAKEDDERRSTRUCT (DDERR_OVERLAYCANTCLIP, _T ("hardware does not support clipped overlays")),
	MAKEDDERRSTRUCT (DDERR_OVERLAYCOLORKEYONLYONEACTIVE, _T ("Can only have ony color key active at one time for overlays")),
	MAKEDDERRSTRUCT (DDERR_PALETTEBUSY, _T ("Access to this palette is being refused because the palette is already locked by another thread.")),
	MAKEDDERRSTRUCT (DDERR_COLORKEYNOTSET, _T ("No src color key specified for this operation.")),
	MAKEDDERRSTRUCT (DDERR_SURFACEALREADYATTACHED, _T ("This surface is already attached to the surface it is being attached to.")),
	MAKEDDERRSTRUCT (DDERR_SURFACEALREADYDEPENDENT, _T ("This surface is already a dependency of the surface it is being made a dependency of.")),
	MAKEDDERRSTRUCT (DDERR_SURFACEBUSY, _T ("Access to this surface is being refused because the surface is already locked by another thread.")),
	MAKEDDERRSTRUCT (DDERR_CANTLOCKSURFACE, _T ("Access to this surface is being refused because no driver exists which can supply a pointer to the surface. This is most likely to happen when attempting to lock the primary surface when no DCI provider is present. Will also happen on attempts to lock an optimized surface.")),
	MAKEDDERRSTRUCT (DDERR_SURFACEISOBSCURED, _T ("Access to Surface refused because Surface is obscured.")),
	MAKEDDERRSTRUCT (DDERR_SURFACELOST, _T ("Access to this surface is being refused because the surface is gone. The DIRECTDRAWSURFACE object representing this surface should have Restore called on it.")),
	MAKEDDERRSTRUCT (DDERR_SURFACENOTATTACHED, _T ("The requested surface is not attached.")),
	MAKEDDERRSTRUCT (DDERR_TOOBIGHEIGHT, _T ("Height requested by DirectDraw is too large.")),
	MAKEDDERRSTRUCT (DDERR_TOOBIGSIZE, _T ("Size requested by DirectDraw is too large --	 The individual height and width are OK.")),
	MAKEDDERRSTRUCT (DDERR_TOOBIGWIDTH, _T ("Width requested by DirectDraw is too large.")),
	MAKEDDERRSTRUCT (DDERR_UNSUPPORTED, _T ("Action not supported.")),
	MAKEDDERRSTRUCT (DDERR_UNSUPPORTEDFORMAT, _T ("FOURCC format requested is unsupported by DirectDraw")),
	MAKEDDERRSTRUCT (DDERR_UNSUPPORTEDMASK, _T ("Bitmask in the pixel format requested is unsupported by DirectDraw")),
	MAKEDDERRSTRUCT (DDERR_VERTICALBLANKINPROGRESS, _T ("vertical blank is in progress")),
	MAKEDDERRSTRUCT (DDERR_WASSTILLDRAWING, _T ("Informs DirectDraw that the previous Blt which is transfering information to or from this Surface is incomplete.")),
	MAKEDDERRSTRUCT (DDERR_XALIGN, _T ("Rectangle provided was not horizontally aligned on reqd. boundary")),
	MAKEDDERRSTRUCT (DDERR_INVALIDDIRECTDRAWGUID, _T ("The GUID passed to DirectDrawCreate is not a valid DirectDraw driver identifier.")),
	MAKEDDERRSTRUCT (DDERR_DIRECTDRAWALREADYCREATED, _T ("A DirectDraw object representing this driver has already been created for this process.")),
	MAKEDDERRSTRUCT (DDERR_NODIRECTDRAWHW, _T ("A hardware only DirectDraw object creation was attempted but the driver did not support any hardware.")),
	MAKEDDERRSTRUCT (DDERR_PRIMARYSURFACEALREADYEXISTS, _T ("this process already has created a primary surface")),
	MAKEDDERRSTRUCT (DDERR_NOEMULATION, _T ("software emulation not available.")),
	MAKEDDERRSTRUCT (DDERR_REGIONTOOSMALL, _T ("region passed to Clipper::GetClipList is too small.")),
	MAKEDDERRSTRUCT (DDERR_CLIPPERISUSINGHWND, _T ("an attempt was made to set a clip list for a clipper objec that is already monitoring an hwnd.")),
	MAKEDDERRSTRUCT (DDERR_NOCLIPPERATTACHED, _T ("No clipper object attached to surface object")),
	MAKEDDERRSTRUCT (DDERR_NOHWND, _T ("Clipper notification requires an HWND or no HWND has previously been set as the CooperativeLevel HWND.")),
	MAKEDDERRSTRUCT (DDERR_HWNDSUBCLASSED, _T ("HWND used by DirectDraw CooperativeLevel has been subclassed, this prevents DirectDraw from restoring state.")),
	MAKEDDERRSTRUCT (DDERR_HWNDALREADYSET, _T ("The CooperativeLevel HWND has already been set. It can not be reset while the process has surfaces or palettes created.")),
	MAKEDDERRSTRUCT (DDERR_NOPALETTEATTACHED, _T ("No palette object attached to this surface.")),
	MAKEDDERRSTRUCT (DDERR_NOPALETTEHW, _T ("No hardware support for 16 or 256 color palettes.")),
	MAKEDDERRSTRUCT (DDERR_BLTFASTCANTCLIP, _T ("If a clipper object is attached to the source surface passed into a BltFast call.")),
	MAKEDDERRSTRUCT (DDERR_NOBLTHW, _T ("No blter.")),
	MAKEDDERRSTRUCT (DDERR_NODDROPSHW, _T ("No DirectDraw ROP hardware.")),
	MAKEDDERRSTRUCT (DDERR_OVERLAYNOTVISIBLE, _T ("returned when GetOverlayPosition is called on a hidden overlay")),
	MAKEDDERRSTRUCT (DDERR_NOOVERLAYDEST, _T ("returned when GetOverlayPosition is called on a overlay that UpdateOverlay has never been called on to establish a destionation.")),
	MAKEDDERRSTRUCT (DDERR_INVALIDPOSITION, _T ("returned when the position of the overlay on the destionation is no longer legal for that destionation.")),
	MAKEDDERRSTRUCT (DDERR_NOTAOVERLAYSURFACE, _T ("returned when an overlay member is called for a non-overlay surface")),
	MAKEDDERRSTRUCT (DDERR_EXCLUSIVEMODEALREADYSET, _T ("An attempt was made to set the cooperative level when it was already set to exclusive.")),
	MAKEDDERRSTRUCT (DDERR_NOTFLIPPABLE, _T ("An attempt has been made to flip a surface that is not flippable.")),
	MAKEDDERRSTRUCT (DDERR_CANTDUPLICATE, _T ("Can't duplicate primary & 3D surfaces, or surfaces that are implicitly created.")),
	MAKEDDERRSTRUCT (DDERR_NOTLOCKED, _T ("Surface was not locked.  An attempt to unlock a surface that was not locked at all, or by this process, has been attempted.")),
	MAKEDDERRSTRUCT (DDERR_CANTCREATEDC, _T ("Windows can not create any more DCs")),
	MAKEDDERRSTRUCT (DDERR_NODC, _T ("No DC was ever created for this surface.")),
	MAKEDDERRSTRUCT (DDERR_WRONGMODE, _T ("This surface can not be restored because it was created in a different mode.")),
	MAKEDDERRSTRUCT (DDERR_IMPLICITLYCREATED, _T ("This surface can not be restored because it is an implicitly created surface.")),
	MAKEDDERRSTRUCT (DDERR_NOTPALETTIZED, _T ("The surface being used is not a palette-based surface")),
	MAKEDDERRSTRUCT (DDERR_UNSUPPORTEDMODE, _T ("The display is currently in an unsupported mode")),
	MAKEDDERRSTRUCT (DDERR_NOMIPMAPHW, _T ("Operation could not be carried out because there is no mip-map texture mapping hardware present or available.")),
	MAKEDDERRSTRUCT (DDERR_INVALIDSURFACETYPE, _T ("The requested action could not be performed because the surface was of the wrong type.")),
	MAKEDDERRSTRUCT (DDERR_NOOPTIMIZEHW, _T ("Device does not support optimized surfaces, therefore no video memory optimized surfaces")),
	MAKEDDERRSTRUCT (DDERR_NOTLOADED, _T ("Surface is an optimized surface, but has not yet been allocated any memory")),
	MAKEDDERRSTRUCT (DDERR_NOFOCUSWINDOW, _T ("Attempt was made to create or set a device window without first setting the focus window")),
	MAKEDDERRSTRUCT (DDERR_DCALREADYCREATED, _T ("A DC has already been returned for this surface. Only one DC can be retrieved per surface.")),
	MAKEDDERRSTRUCT (DDERR_NONONLOCALVIDMEM, _T ("An attempt was made to allocate non-local video memory from a device that does not support non-local video memory.")),
	MAKEDDERRSTRUCT (DDERR_CANTPAGELOCK, _T ("The attempt to page lock a surface failed.")),
	MAKEDDERRSTRUCT (DDERR_CANTPAGEUNLOCK, _T ("The attempt to page unlock a surface failed.")),
	MAKEDDERRSTRUCT (DDERR_NOTPAGELOCKED, _T ("An attempt was made to page unlock a surface with no outstanding page locks.")),
	MAKEDDERRSTRUCT (DDERR_MOREDATA, _T ("There is more data available than the specified buffer size could hold")),
	MAKEDDERRSTRUCT (DDERR_VIDEONOTACTIVE, _T ("The video port is not active")),
	MAKEDDERRSTRUCT (DDERR_DEVICEDOESNTOWNSURFACE, _T ("Surfaces created by one direct draw device cannot be used directly by another direct draw device.")),
	MAKEDDERRSTRUCT (DDERR_NOTINITIALIZED, _T ("An attempt was made to invoke an interface member of a DirectDraw object created by CoCreateInstance() before it was initialized.")),
	MAKEDDERRSTRUCT (DD_OK, _T ("DD_OK is not an error")),

	// D3D errors
	MAKEDDERRSTRUCT (D3D_OK, _T ("D3D_OK is not an error")), 
	MAKEDDERRSTRUCT (D3DERR_BADMAJORVERSION, _T ("D3DERR_BADMAJORVERSION")), 
	MAKEDDERRSTRUCT (D3DERR_BADMINORVERSION, _T ("D3DERR_BADMINORVERSION")), 
	MAKEDDERRSTRUCT (D3DERR_DEVICEAGGREGATED, _T ("D3DERR_DEVICEAGGREGATED (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_EXECUTE_CLIPPED_FAILED, _T ("D3DERR_EXECUTE_CLIPPED_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_EXECUTE_CREATE_FAILED, _T ("D3DERR_EXECUTE_CREATE_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_EXECUTE_DESTROY_FAILED, _T ("D3DERR_EXECUTE_DESTROY_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_EXECUTE_FAILED, _T ("D3DERR_EXECUTE_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_EXECUTE_LOCK_FAILED, _T ("D3DERR_EXECUTE_LOCK_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_EXECUTE_LOCKED, _T ("D3DERR_EXECUTE_LOCKED")), 
	MAKEDDERRSTRUCT (D3DERR_EXECUTE_NOT_LOCKED, _T ("D3DERR_EXECUTE_NOT_LOCKED")), 
	MAKEDDERRSTRUCT (D3DERR_EXECUTE_UNLOCK_FAILED, _T ("D3DERR_EXECUTE_UNLOCK_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_INITFAILED, _T ("D3DERR_INITFAILED (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_INBEGIN, _T ("D3DERR_INBEGIN (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_INVALID_DEVICE, _T ("D3DERR_INVALID_DEVICE (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_INVALIDCURRENTVIEWPORT, _T ("D3DERR_INVALIDCURRENTVIEWPORT (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_INVALIDPALETTE, _T ("D3DERR_INVALIDPALETTE(new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_INVALIDPRIMITIVETYPE, _T ("D3DERR_INVALIDPRIMITIVETYPE (new for DirectX 5)")),  
	MAKEDDERRSTRUCT (D3DERR_INVALIDRAMPTEXTURE, _T ("D3DERR_INVALIDRAMPTEXTURE (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_INVALIDVERTEXTYPE, _T ("D3DERR_INVALIDVERTEXTYPE (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_LIGHT_SET_FAILED, _T ("D3DERR_LIGHT_SET_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_LIGHTHASVIEWPORT, _T ("D3DERR_LIGHTHASVIEWPORT (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_LIGHTNOTINTHISVIEWPORT, _T ("D3DERR_LIGHTNOTINTHISVIEWPORT (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_MATERIAL_CREATE_FAILED, _T ("D3DERR_MATERIAL_CREATE_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_MATERIAL_DESTROY_FAILED, _T ("D3DERR_MATERIAL_DESTROY_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_MATERIAL_GETDATA_FAILED, _T ("D3DERR_MATERIAL_GETDATA_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_MATERIAL_SETDATA_FAILED, _T ("D3DERR_MATERIAL_SETDATA_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_MATRIX_CREATE_FAILED, _T ("D3DERR_MATRIX_CREATE_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_MATRIX_DESTROY_FAILED, _T ("D3DERR_MATRIX_DESTROY_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_MATRIX_GETDATA_FAILED, _T ("D3DERR_MATRIX_GETDATA_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_MATRIX_SETDATA_FAILED, _T ("D3DERR_MATRIX_SETDATA_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_NOCURRENTVIEWPORT, _T ("D3DERR_NOCURRENTVIEWPORT (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_NOTINBEGIN, _T ("D3DERR_NOTINBEGIN (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_NOVIEWPORTS, _T ("D3DERR_NOVIEWPORTS (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_SCENE_BEGIN_FAILED, _T ("D3DERR_SCENE_BEGIN_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_SCENE_END_FAILED, _T ("D3DERR_SCENE_END_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_SCENE_IN_SCENE, _T ("D3DERR_SCENE_IN_SCENE")), 
	MAKEDDERRSTRUCT (D3DERR_SCENE_NOT_IN_SCENE, _T ("D3DERR_SCENE_NOT_IN_SCENE")), 
	MAKEDDERRSTRUCT (D3DERR_SETVIEWPORTDATA_FAILED, _T ("D3DERR_SETVIEWPORTDATA_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_SURFACENOTINVIDMEM, _T ("D3DERR_SURFACENOTINVIDMEM (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_TEXTURE_BADSIZE, _T ("D3DERR_TEXTURE_BADSIZE (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_TEXTURE_CREATE_FAILED, _T ("D3DERR_TEXTURE_CREATE_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_TEXTURE_DESTROY_FAILED, _T ("D3DERR_TEXTURE_DESTROY_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_TEXTURE_GETSURF_FAILED, _T ("D3DERR_TEXTURE_GETSURF_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_TEXTURE_LOAD_FAILED, _T ("D3DERR_TEXTURE_LOAD_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_TEXTURE_LOCK_FAILED, _T ("D3DERR_TEXTURE_LOCK_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_TEXTURE_LOCKED, _T ("D3DERR_TEXTURE_LOCKED")), 
	MAKEDDERRSTRUCT (D3DERR_TEXTURE_NO_SUPPORT, _T ("D3DERR_TEXTURE_NO_SUPPORT")), 
	MAKEDDERRSTRUCT (D3DERR_TEXTURE_NOT_LOCKED, _T ("D3DERR_TEXTURE_NOT_LOCKED")), 
	MAKEDDERRSTRUCT (D3DERR_TEXTURE_SWAP_FAILED, _T ("D3DERR_TEXTURE_SWAP_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_TEXTURE_UNLOCK_FAILED, _T ("D3DERR_TEXTURE_UNLOCK_FAILED")), 
	MAKEDDERRSTRUCT (D3DERR_VIEWPORTDATANOTSET, _T ("D3DERR_VIEWPORTDATANOTSET (new for DirectX 5)")),
 	MAKEDDERRSTRUCT (D3DERR_VIEWPORTHASNODEVICE, _T ("D3DERR_VIEWPORTHASNODEVICE (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_ZBUFF_NEEDS_SYSTEMMEMORY, _T ("D3DERR_ZBUFF_NEEDS_SYSTEMMEMORY (new for DirectX 5)")), 
	MAKEDDERRSTRUCT (D3DERR_ZBUFF_NEEDS_VIDEOMEMORY, _T ("D3DERR_ZBUFF_NEEDS_VIDEOMEMORY (new for DirectX 5)")),

	
	// D3D errors (v8.0)
	MAKEDDERRSTRUCT (D3DRMERR_BADALLOC, _T ("Out of memory. ")),
	MAKEDDERRSTRUCT (D3DRMERR_BADCACHEFILE, _T ("An X file could not be opened because of a bad cache file. A cache file contains data retrieved from the network, cached on the hard disk, and retrieved in subsequent requests. ")),
	MAKEDDERRSTRUCT (D3DRMERR_BADDEVICE, _T ("Device is not compatible with renderer. ")),
	MAKEDDERRSTRUCT (D3DRMERR_BADFILE, _T ("Data file is corrupt. ")),
	MAKEDDERRSTRUCT (D3DRMERR_BADMAJORVERSION, _T ("Bad DLL major version. ")),
	MAKEDDERRSTRUCT (D3DRMERR_BADMINORVERSION, _T ("Bad DLL minor version. ")),
	MAKEDDERRSTRUCT (D3DRMERR_BADOBJECT, _T ("Object expected in argument. ")),
	MAKEDDERRSTRUCT (D3DRMERR_BADPMDATA, _T ("The data in the .x file is corrupted. The conversion to a progressive mesh succeeded but produced an invalid progressive mesh in the .x file. ")),
	MAKEDDERRSTRUCT (D3DRMERR_BADTYPE, _T ("Bad argument type passed. ")),
	MAKEDDERRSTRUCT (D3DRMERR_BADVALUE, _T ("Bad argument value passed. ")),
	MAKEDDERRSTRUCT (D3DRMERR_BOXNOTSET, _T ("An attempt was made to access a bounding box (for example, with IDirect3DRMFrame3::GetBox) when no bounding box was set on the frame. ")),
	MAKEDDERRSTRUCT (D3DRMERR_CLIENTNOTREGISTERED, _T ("Client has not been registered. Call IDirect3DRM3::RegisterClient. ")),
	MAKEDDERRSTRUCT (D3DRMERR_CONNECTIONLOST, _T ("Data connection was lost during a load, clone, or duplicate. ")),
	MAKEDDERRSTRUCT (D3DRMERR_ELEMENTINUSE, _T ("Element can't be modified or deleted while in use. To empty a submesh, call Empty() against its parent. ")),
	MAKEDDERRSTRUCT (D3DRMERR_FACEUSED, _T ("Face already used in a mesh. ")),
	MAKEDDERRSTRUCT (D3DRMERR_FILENOTFOUND, _T ("File cannot be opened. ")),
	MAKEDDERRSTRUCT (D3DRMERR_INCOMPATABLEKEY, _T ("Specified animation key is incompatible. The key cannot be modified. ")),
	MAKEDDERRSTRUCT (D3DRMERR_INVALIDLIBRARY, _T ("Specified libary is invalid. ")),
	MAKEDDERRSTRUCT (D3DRMERR_LIBRARYNOTFOUND, _T ("Specified libary not found. ")),
	MAKEDDERRSTRUCT (D3DRMERR_LOADABORTED, _T ("Load aborted by user. ")),
	MAKEDDERRSTRUCT (D3DRMERR_NOINTERNET, _T ("Downloading from a URL failed, or Urlmon.dll could not be found or was corrupt. ")),
	MAKEDDERRSTRUCT (D3DRMERR_NOSUCHKEY, _T ("Specified animation key does not exist. ")),
	MAKEDDERRSTRUCT (D3DRMERR_NOTAGGREGATED, _T ("An attempt was made to access IDirect3DRMObject methods through an external visual object, but the external visual was not created with IDirect3DRM::Load or IDirect3DRM::CreateObject, and is not aggregated with IDirect3DRMObject. ")),
	MAKEDDERRSTRUCT (D3DRMERR_NOTCREATEDFROMDDS, _T ("Specified texture was not created from a DirectDraw Surface. ")),
	MAKEDDERRSTRUCT (D3DRMERR_NOTDONEYET, _T ("Unimplemented. ")),
	MAKEDDERRSTRUCT (D3DRMERR_NOTENOUGHDATA, _T ("Not enough data has been loaded to perform the requested operation. ")),
	MAKEDDERRSTRUCT (D3DRMERR_NOTFOUND, _T ("Object not found in specified place. ")),
	MAKEDDERRSTRUCT (D3DRMERR_PENDING, _T ("Data required to supply the requested information has not finished loading. ")),
	MAKEDDERRSTRUCT (D3DRMERR_REQUESTTOOLARGE, _T ("Attempt was made to set a level of detail in a progressive mesh greater than the maximum available. ")),
	MAKEDDERRSTRUCT (D3DRMERR_REQUESTTOOSMALL, _T ("Attempt was made to set the minimum rendering detail of a progressive mesh smaller than the detail in the base mesh (the minimum for rendering). ")),
	MAKEDDERRSTRUCT (D3DRMERR_TEXTUREFORMATNOTFOUND, _T ("Texture format could not be found that meets the specified criteria and that the underlying Immediate Mode device supports. ")),
	MAKEDDERRSTRUCT (D3DRMERR_UNABLETOEXECUTE, _T ("Unable to carry out procedure. ")),
	MAKEDDERRSTRUCT (DDERR_INVALIDOBJECT, _T ("Received pointer that was an invalid object. ")),
	MAKEDDERRSTRUCT (DDERR_INVALIDPARAMS, _T ("One or more of the parameters passed to the method are incorrect. ")),
	MAKEDDERRSTRUCT (DDERR_NOTFOUND, _T ("The requested item was not found. ")),
	MAKEDDERRSTRUCT (DDERR_NOTINITIALIZED, _T ("An attempt was made to call an interface method of an object created by CoCreateInstance before the object was initialized. ")),
	MAKEDDERRSTRUCT (DDERR_OUTOFMEMORY, _T ("DirectDraw does not have enough memory to perform the operation. ")),
	
#endif //DIRECTDRAW_VERSION >= 0x0600

	::end
};

HRESULT _3D_API HandleDDErr (HRESULT hErr, LPCTSTR lpszFile, UINT nLine)
{
	CString str;
	str.Format (_T (
		"Unknown DirectDraw error number: 0x%08X\n"
		"%s, line: %u"),
		hErr,
		lpszFile,
		nLine);

	for (int i = 0; ::err [i] != ::end; i++) {
		if (err [i].m_hID == hErr) {
			str.Format (_T (
				"DirectDraw error number: 0x%08X [%s]\n"
				"%s\n"
				"%s, line: %u"),
				err [i].m_hID,
				err [i].m_strConstant,
				err [i].m_str,
				lpszFile,
				nLine);
			break;
		}
	}

	#ifdef _DEBUG
	/*
	::AfxDumpStack (AFX_STACK_DUMP_TARGET_CLIPBOARD);

	if (::OpenClipboard (NULL)) {
		CString strDump = (LPCTSTR)::GetClipboardData (CF_TEXT);
		::CloseClipboard ();
		str += "\n" + strDump;
	}
	else
		::AfxDumpStack (AFX_STACK_DUMP_TARGET_TRACE);
	*/
	CDebug::Trace (str, true, lpszFile, nLine);
	#endif //_DEBUG

	return hErr;
}
	
HRESULT  _3D_API TraceDDErr (HRESULT hErr, LPCTSTR lpszContext, LPCTSTR lpszFile, UINT nLine)
{
//	if (hErr != DD_OK && hErr != D3D_OK) {
	if (FAILED (hErr)) {
		CString str;
		
		str.Format (_T ("Unknown DirectDraw error number: 0x%08X\n"), hErr);

		for (int i = 0; ::err [i] != ::end; i++) {
			if (err [i].m_hID == hErr) {
				str.Format (_T (
					"DirectDraw error number: 0x%08X [%s]\n"
					"\t%s\n"),
					err [i].m_hID,
					err [i].m_strConstant,
					err [i].m_str);
				break;
			}
		}

		if (lpszContext)
			str = CString (lpszContext) + _T ("\n\t") + str;

		#ifdef _DEBUG
		/*
		::AfxDumpStack (AFX_STACK_DUMP_TARGET_CLIPBOARD);

		if (::OpenClipboard (NULL)) {
			CString strDump = (LPCTSTR)::GetClipboardData (CF_TEXT);
			::CloseClipboard ();
			str += "\n" + strDump;
		}
		else
			::AfxDumpStack (AFX_STACK_DUMP_TARGET_TRACE);
		*/
		CDebug::Trace (str, true, lpszFile, nLine);
		#endif //_DEBUG
	}

	return hErr;
}

