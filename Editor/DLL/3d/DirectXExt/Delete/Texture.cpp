// Texture.cpp: implementation of the CTexture class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Color.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace Foxjet3d;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#if DIRECTDRAW_VERSION >= 0x0600

CTexture::CTexture()
:	m_hHandle (NULL)
{
#if DIRECTDRAW_VERSION < 0x0600
	CString str;
	str.Format (_T ("Cannot create a CTexture object with DIRECTDRAW_VERSION 0x%p"), 
		DIRECTDRAW_VERSION);
	MsgBox (str);
#endif //DIRECTDRAW_VERSION < 0x0600
}

CTexture::CTexture (const CTexture & rhs)
:	m_lpMemorySurface (rhs.m_lpMemorySurface), 
	m_lpDeviceSurface (rhs.m_lpDeviceSurface),
	m_lpPalette (rhs.m_lpPalette),
	m_hHandle (rhs.m_hHandle)
{
#if DIRECTDRAW_VERSION < 0x0600
	CString str;
	str.Format (_T ("Cannot create a CTexture object with DIRECTDRAW_VERSION 0x%p"), 
		DIRECTDRAW_VERSION);
	MsgBox  (str);
#endif //DIRECTDRAW_VERSION < 0x0600
}

CTexture & CTexture::operator = (const CTexture & rhs)
{
	if (this != &rhs) {
		Release ();
		m_lpMemorySurface = rhs.m_lpMemorySurface;  
		m_lpDeviceSurface = rhs.m_lpDeviceSurface;  
		m_lpPalette = rhs.m_lpPalette;
		m_hHandle = rhs.m_hHandle;
	}

	return * this;
}

CTexture::~CTexture()
{
	Release ();
}

bool CTexture::Copy (HBITMAP hBmp)
{
	BITMAP bm;
	HDC dcBitmap;
	HDC dcSurface;
	bool bResult = false;

	::ZeroMemory (&bm, sizeof (bm));
	VERIFY (::GetObject(hBmp, sizeof(bm), &bm));    // get size of bitmap

	dcBitmap = ::CreateCompatibleDC(NULL);
	::SelectObject (dcBitmap, hBmp);

    if (SUCCEEDED (m_lpMemorySurface->GetDC (&dcSurface))) {
		CSize size = GetSize ();
        BOOL bStretchBlt = ::StretchBlt (dcSurface, 0, 0, size.cx, size.cy,  
			dcBitmap,  0, 0, bm.bmWidth, bm.bmHeight, SRCCOPY);
		m_lpMemorySurface->ReleaseDC (dcSurface);
		bResult = Restore();
    }
    
	::DeleteDC(dcBitmap);
    
	return bResult;
}

void CTexture::Release ()
{
/*
	CString str;
	str.Format ("CTexture::Release: 0x%p (mem: 0x%p[0x%p], dev: 0x%p[0x%p])",
		this,
		m_lpMemorySurface, m_lpMemorySurface.p,
		m_lpDeviceSurface, m_lpDeviceSurface.p,
		m_lpPalette, m_lpPalette.p);
	TRACEF (str);
*/

	m_lpPalette.Release ();
	m_lpDeviceSurface.Release();
	m_lpMemorySurface.Release();
	m_hHandle = NULL;
}

bool CTexture::Restore ()
{
    HRESULT hResult;

    if (m_lpDeviceSurface == NULL || m_lpMemorySurface == NULL)
        return false;

    // we dont need to do this step for system memory surfaces.
    if (m_lpDeviceSurface == m_lpMemorySurface)
        return true;

    // restore the video memory texture.
    if (FAILED (m_lpDeviceSurface->Restore ()))
        return false;

    CComPtr <IDirect3DTexture2> screen = COMPTRCAST (IDirect3DTexture2, m_lpDeviceSurface);
    CComPtr <IDirect3DTexture2> memory = COMPTRCAST (IDirect3DTexture2, m_lpMemorySurface);

    // call IDirect3DTexture::Load() to copy the texture to the device.
    hResult = screen->Load (memory);
	TRACEDDERR (hResult);

    return SUCCEEDED (hResult);
}


bool CTexture::Create(LPDIRECT3DDEVICE3 lpDev, int nSize, DWORD dwFlags)
{
    DDSURFACEDESC2 ddsd;
    LPDIRECTDRAW4 lpdd;
	DWORD dwBPP = 9; //16; // TODO: hack

    // we need a IDirectDraw so we can create a surface.
    if ((lpdd = GetDD (lpDev)) == NULL)
        return FALSE;

    // free any existing texture.
    Release();

    // find the best texture format to use.
    ZeroMemory(&ddsd, sizeof(ddsd));
    ddsd.dwSize = sizeof(ddsd);
	ChooseTextureFormat (lpDev, dwBPP, &ddsd.ddpfPixelFormat);
    ddsd.dwFlags |= DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT;
    ddsd.dwWidth = nSize;
    ddsd.dwHeight = nSize;

    // create a video memory texture
    //
    // if we are dealing with a HAL create in video memory, else
    // create in system memory.
    D3DDEVICEDESC hal, hel;
    ::ZeroMemory(&hal, sizeof(hal));
    hal.dwSize = sizeof(hal);
    ::ZeroMemory(&hel, sizeof(hel));
    hel.dwSize = sizeof(hel);
    DDCALL (lpDev->GetCaps(&hal, &hel));

    // BUGBUG should we check for texture caps?
    if (hal.dcmColorModel)
        ddsd.ddsCaps.dwCaps = DDSCAPS_VIDEOMEMORY | DDSCAPS_TEXTURE | DDSCAPS_ALLOCONLOAD | dwFlags;
    else
        ddsd.ddsCaps.dwCaps = DDSCAPS_SYSTEMMEMORY | DDSCAPS_TEXTURE | dwFlags;

    if (FAILED (DDCALL (lpdd->CreateSurface(&ddsd, &m_lpDeviceSurface, NULL))))
		return false;

    // create a system memory surface for the texture.
    //
    // we use this system memory surface for the ::Load call
    // and this surface does not get lost.
    if (hal.dcmColorModel) {
        ddsd.ddsCaps.dwCaps = DDSCAPS_SYSTEMMEMORY | DDSCAPS_TEXTURE | dwFlags;
        if (FAILED (DDCALL (lpdd->CreateSurface(&ddsd, &m_lpMemorySurface, NULL)))) {
			Release ();
			return false;
		}
    }
    else {
        // when dealing with a SW rasterizer we dont need to make 2 surfaces.
        m_lpMemorySurface = m_lpDeviceSurface;
    }

/* TODO
    // create a palette for the texture
    if (ddsd.ddpfPixelFormat.dwRGBBitCount <= 8) {
        m_lpPalette = PaletteFromBitmap (lpdd, hBmp);

        // now we have a palette, attach it to the surface and the texture
        m_lpMemorySurface->SetPalette(m_lpPalette);
        m_lpDeviceSurface->SetPalette(m_lpPalette);
    }
*/

    // get the texture handle
	CComPtr <IDirect3DDevice2> lpDev2 = COMCAST (IDirect3DDevice2, lpDev);
    CComPtr <IDirect3DTexture2> texture = COMPTRCAST (IDirect3DTexture2, 
		m_lpDeviceSurface);
    if (FAILED (DDCALL (texture->GetHandle (lpDev2, &m_hHandle))))
		return false;

	Fill ();

	return true;
}

CSize CTexture::GetSize() const
{
	return GetSize (GetSurface ());
}

CSize CTexture::GetSize(LPDIRECTDRAWSURFACE4 lpSurface)
{
	CSize result (0, 0);

	if (lpSurface) {
		DDSURFACEDESC2 ddsd;
	
		::ZeroMemory (&ddsd, sizeof (ddsd));
		ddsd.dwSize = sizeof (ddsd);
		ddsd.dwFlags = DDSD_HEIGHT | DDSD_WIDTH;

		if (SUCCEEDED (DDCALL (lpSurface->GetSurfaceDesc (&ddsd))))
			result = CSize (ddsd.dwWidth, ddsd.dwHeight);
	}

	return result;
}

D3DTEXTUREHANDLE CTexture::GetHandle (const CComPtr <IDirectDrawSurface4> & surface,
									  CComPtr <IDirect3DDevice3> & lpd3dDevice)
{
	D3DTEXTUREHANDLE hHandle = NULL;
	CComPtr <IDirect3DTexture2> handle = COMPTRCAST (IDirect3DTexture2, surface);
	CComPtr <IDirect3DDevice2> lpDev = COMCAST (IDirect3DDevice2, lpd3dDevice);

	DDCALL (handle->GetHandle (lpDev, &hHandle));
	
	return hHandle;
}

void CTexture::Fill(COLORREF rgb)
{
	Fill (GetSurface (), rgb);
}

void CTexture::Fill (LPDIRECTDRAWSURFACE4 lpSurface, COLORREF rgb)
{
	CSize size = GetSize (lpSurface);
	CDxDC dc (lpSurface);
	dc->FillSolidRect (CRect (0, 0, size.cx, size.cy), rgb);
/*
	DDBLTFX ddbltfx;

	ddbltfx.dwSize = sizeof(ddbltfx);
	ddbltfx.dwFillColor = rgb;//RGBA_MAKE (GetRValue (rgb), GetGValue (rgb), GetBValue (rgb));
	DDCALL (lpSurface->Blt (NULL, NULL, NULL, DDBLT_COLORFILL | DDBLT_WAIT, &ddbltfx));
*/
}
#endif //DIRECTDRAW_VERSION >= 0x0600
