#include "stdafx.h"

#include <objbase.h>

#if DIRECTDRAW_VERSION >= 0x0600
	#include <d3drm.h>
#endif //DIRECTDRAW_VERSION >= 0x0600

#include "dderr.h"
#include "Com.h"
#include "debug.h"

#ifdef _DEBUG
	#define LOCALASSERT(b,f,n) \
		do \
		{ \
		if (!(b) && AfxAssertFailedLine(f, n)) \
			AfxDebugBreak(); \
		} while (0) \

#else //!_DEBUG
	#define LOCALASSERT(b,f,n) 
#endif

static CString StringFromRefiid (REFIID iid);

template <class T>
HRESULT InstanceOf (REFIID iid, LPVOID lpObject)
{
	ATLASSERT(lpObject != NULL);
	LPUNKNOWN lpTmp = NULL; 
	T * lp = (T *)lpObject;
	HRESULT hResult = lp->QueryInterface(iid, (LPVOID *)&lpTmp);

	if (SUCCEEDED (hResult)) 
		lpTmp->Release ();

	return SUCCEEDED (hResult);
}

LPUNKNOWN ComCast (REFIID iid, LPUNKNOWN lpFrom, LPCTSTR lpszIIDFrom, 
				   LPCTSTR lpszObjFrom, bool bDebugWarnNull, 
				   LPCTSTR lpszFile, UINT nLine)
{
	#if defined( _DEBUG ) && defined( __TRACE_COMCAST__ )
	CString str;
	str.Format ("ComCast (%s, 0x%p, %s, %s, %s, %u)",
		StringFromRefiid (iid), lpFrom, lpszIIDFrom, lpszObjFrom, 
		lpszFile, nLine); 
	CDebug::Trace (str, true, lpszFile, nLine);
	#endif //_DEBUG

	LPUNKNOWN lpTo = NULL;

	if (lpFrom == NULL) {
		#ifdef _DEBUG
		CString str;
		str.Format ("%s is NULL", lpszObjFrom);
		CDebug::Trace (str, true, lpszFile, nLine);
		#endif //_DEBUG
	
		if (bDebugWarnNull)
			LOCALASSERT (0, lpszFile, nLine);
		
		return NULL;
	}

	HRESULT hResult = lpFrom->QueryInterface (iid, (LPVOID *)&lpTo);

	#ifdef _DEBUG
	if (FAILED (hResult) || lpTo == NULL) {
		CString str;
		str.Format ("ComCast failed to cast from %s to %s (%s)",
			lpszObjFrom, lpszIIDFrom, StringFromRefiid (iid)); 
		CDebug::Trace (str, true, lpszFile, nLine);
		TraceDDErr (hResult, NULL, lpszFile, nLine);

		if (FAILED (hResult)) {
			str.Format ("%s->QueryInterface failed",
				lpszObjFrom);
			CDebug::Trace (str, true, lpszFile, nLine);
		}
		if (lpTo == NULL) {
			str.Format ("%s->QueryInterface set 2nd param to NULL",
				lpszObjFrom);
			CDebug::Trace (str, true, lpszFile, nLine);
		}
	}

	if (bDebugWarnNull) {
		LOCALASSERT (SUCCEEDED (hResult), lpszFile, nLine);
		LOCALASSERT (lpTo != NULL, lpszFile, nLine);
	}
	#endif //_DEBUG

	return lpTo;
}

static CString StringFromRefiid (REFIID iid) 
{
	LPWSTR pwsz;
	CString str;

	::StringFromIID (iid, &pwsz);

	if (pwsz) {
		LPMALLOC pMalloc;

		str = pwsz;
		::CoGetMalloc(1, &pMalloc);
		pMalloc->Free(pwsz);
		pMalloc->Release();
	}

	return str;
}
