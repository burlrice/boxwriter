// Point.h: interface for the CPoint3 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_POINT_H__108BF715_AF26_4D1A_85D6_89B7BF575BBB__INCLUDED_)
#define AFX_POINT_H__108BF715_AF26_4D1A_85D6_89B7BF575BBB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Quadruple.h"
#include "..\3dApi.h"

namespace Foxjet3d 
{
	class _3D_API CPoint3 : public CQuadruple <REAL> 
	{
	public:
		CPoint3(REAL x = 0, REAL y = 0, REAL z = 0);
		CPoint3 (const CPoint3 & rhs);
		CPoint3 (const CQuadruple <REAL> & rhs);
		CPoint3 & operator = (const CPoint3 & rhs);
		virtual ~CPoint3();

		void Homogenize ();
	};
}; //namespace Foxjet3d

#endif // !defined(AFX_POINT_H__108BF715_AF26_4D1A_85D6_89B7BF575BBB__INCLUDED_)
