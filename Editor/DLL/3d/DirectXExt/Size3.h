// Size3.h: interface for the CSize class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SIZE_H__B48E1BE1_2072_4025_9321_57106F3CFABE__INCLUDED_)
#define AFX_SIZE_H__B48E1BE1_2072_4025_9321_57106F3CFABE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "..\3dApi.h"
#include "Point3.h"

namespace Foxjet3d 
{
	class _3D_API CSize3 : public Foxjet3d::CPoint3
	{
	public:
		CSize3 (REAL cx = 0, REAL cy = 0, REAL cz = 0);
		CSize3 (const CSize3 & rhs);
		CSize3 (const CPoint3 & rhs);
		CSize3 & operator = (const CSize3 & rhs);
		virtual ~CSize3();

		REAL Length () const;
		REAL Width () const;
		REAL Height () const;
	};
}; //namespace Foxjet3d


#endif // !defined(AFX_SIZE_H__B48E1BE1_2072_4025_9321_57106F3CFABE__INCLUDED_)
