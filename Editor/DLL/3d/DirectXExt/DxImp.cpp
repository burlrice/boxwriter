#include "stdafx.h"

//#include <typeinfo.h>
#include <math.h>

#include "..\3d.h"
#include "..\Resource.h"
#include "DxView.h"
#include "Color.h"
#include "Debug.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace Foxjet3d;
using namespace Foxjet3d::Box;
using namespace DxParams;

IMPLEMENT_DYNAMIC (CDxImp, CObject)

CDxImp::CDxImp (bool bAutoDeleteParam)
:	m_pParent (NULL),
	m_guidDevice (GUID_NULL),
	m_nResolution (7), // (10),
	m_pParams (NULL),
	m_bAutoDeleteParam (bAutoDeleteParam)
{
	m_fnt.CreateFont (12, 0, 0, 0, 
		FW_BOLD, FALSE, FALSE, FALSE,
		DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY, FF_DONTCARE, _T ("Verdana"));
}

CDxImp::~CDxImp ()
{
	Destroy ();
}

bool CDxImp::Create (CWnd * pParent, CDxParams * pParams)
{
	const CSize size = GetPrimarySurfaceSize ();
	int nSize = (int)pow ((double)2, (double)GetResolution ());

	Destroy ();

	if (!pParent || !pParams)
		return false;

	Restore ();

	if (!InitDirectX (pParent, size) ||
		!CreateSurfaces (size) ||
		!CreateDevice () ||
		!CreateViewport (size) ||
		!SetState ()) 
	{
		Destroy ();
		return false;
	}

	m_pParent = pParent;
	m_pParams = pParams;

	return true;
}

bool CDxImp::Destroy ()
{
	if (m_pParams && m_bAutoDeleteParam) {
		delete m_pParams;
		m_pParams = NULL;
	}

	return true;
}

int CDxImp::GetResolution () const
{
	return m_nResolution;
}

bool CDxImp::SetResolution (int nRes) 
{
	if (nRes >= 5 && nRes <= 10) {
		int nSize = (int)pow ((double)2, (double)nRes);
		CSize size (nSize, nSize);
		m_nResolution = nRes;
		
		return CreateSurfaces (size);
	}

	return false;
}

void CDxImp::Restore ()
{
}

bool CDxImp::SetState ()
{
    return true;
}

bool CDxImp::CreateViewport (const CSize & size)
{
	return true;
}

bool CDxImp::CreateDevice ()
{
	return true;
}

bool CDxImp::CreateSurfaces (const CSize & size)
{
	return true;
}

bool CDxImp::InitDirectX (CWnd * pParent, const CSize & size)
{
	return true;
}

CSize CDxImp::GetPrimarySurfaceSize() const
{
	HDC hDC = ::GetDC (NULL);
	int cx = ::GetDeviceCaps (hDC, HORZRES);
	int cy = ::GetDeviceCaps (hDC, VERTRES);

	::ReleaseDC (NULL, hDC);
	return CSize (cx, cy);
}

