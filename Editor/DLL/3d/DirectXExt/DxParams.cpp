#include "stdafx.h"
#include "DxObject.h"
#include "Debug.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace Foxjet3d;
using namespace Foxjet3d::DxParams;

IMPLEMENT_DYNAMIC (CDxParams, CObject)

_3D_API ULONG Foxjet3d::DxParams::Compare (const Foxjet3d::DxParams::CDxParams & lhs, const Foxjet3d::DxParams::CDxParams & rhs)
{
	ULONG lResult = 0;

	lResult |= lhs.m_rZoom != rhs.m_rZoom		? DxParams::ZOOM		: 0;

	return lResult;
}

CDxParams::CDxParams (CDxView & view)
:	m_view (view),
	m_rZoom (1.0)
{
}

CDxParams::CDxParams (const CDxParams & rhs)
:	m_view (rhs.m_view),
	m_rZoom (rhs.m_rZoom)
{
}

CDxParams & CDxParams::operator = (const CDxParams & rhs)
{
	if (this != &rhs) {
		//m_view	= rhs.m_view;
		m_rZoom		= rhs.m_rZoom;
	}

	return * this;
}

CDxParams::~CDxParams ()
{
}

void CDxParams::LoadSettings (CWinApp & app, const CString & strSection)
{
	m_rZoom = (double)app.GetProfileInt (strSection, _T ("m_rZoom"), 100) / 100.0;
}

void CDxParams::SaveSettings (CWinApp & app, const CString & strSection) const
{
	app.WriteProfileInt (strSection, _T ("m_rZoom"), (int)(m_rZoom * 100));
}