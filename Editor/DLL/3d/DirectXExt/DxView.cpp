// BoxView.cpp: implementation of the CDxView class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

//#include <typeinfo.h>

#include "..\3d.h"
#include "..\Resource.h"
#include "DxView.h"
#include "Color.h"
#include "Debug.h"
#include "..\Box.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace Foxjet3d;
using namespace Foxjet3d::Box;
using namespace DxParams;

IMPLEMENT_DYNAMIC (CDxView, CDxImp)

CDxView::CDxView(bool bAutoDeleteParam)
:	CDxImp (bAutoDeleteParam),
	m_bRunning (false)
{
}

CDxView::~CDxView()
{
}

bool CDxView::Create (CWnd * pParent, CDxParams * pParams)
{
	m_bRunning = CDxImp::Create (pParent, pParams);

	return m_bRunning;
}

bool CDxView::Destroy ()
{
	//LOCK (m_cs);
	m_bRunning = false;
	return CDxImp::Destroy ();
}

void CDxView::Draw (CDC * pDC, const CRect & rc, double dZoom)
{
	if (IsRunning ()) {
		//LOCK (m_cs);

		{
			for (int i = 0; i < m_vObjects.GetSize (); i++) {
				if (CBox * pBox = DYNAMIC_DOWNCAST (CBox, m_vObjects [i])) {
					CBoxParams * pParams = DYNAMIC_DOWNCAST (CBoxParams, m_pParams);
					ASSERT (pParams);

					pBox->Blt (* pDC, rc, dZoom, * pParams);
				}
			}
		}
		//else
		//	pDC->FillSolidRect (rc, Color::rgbRed);
	}
	else
		pDC->FillSolidRect (rc, Color::rgbRed);
}

void CDxView::Restore ()
{
	for (int i = 0; i < m_vObjects.GetSize (); i++) {
		CDxObject * p = m_vObjects [i];

		if (p)
			p->Restore (m_pParams, false);
	}
}

void CDxView::DrawParams (CDC & dc)
{
	int nBkMode = dc.SetBkMode (TRANSPARENT);
	COLORREF rgbText = dc.SetTextColor (Color::rgbYellow);
	CGdiObject * pOldFont = dc.SelectObject (&m_fnt);
	CRect rc;
	CString str;

	m_pParent->GetClientRect (&rc);

	str = CTime::GetCurrentTime ().Format ("%c");
	rc.top += dc.DrawText (str, rc, 0);

	for (int i = 0; i < m_vObjects.GetSize (); i++) {
		CDxObject * p = m_vObjects [i];
		str.Format (_T ("m_vObjects [%d] = %s [%s]"),
			i, a2w (p->GetRuntimeClass ()->m_lpszClassName),
			typeid (p).name ());
		rc.top += dc.DrawText (str, rc, 0);

		CPoint pos = p->GetPos ();
		str.Format (_T ("    pos = (%d, %d)"),
			pos.x, pos.y);
		rc.top += dc.DrawText (str, rc, 0);
	}

	dc.SelectObject (pOldFont);
	dc.SetBkMode (nBkMode);
	dc.SetTextColor (rgbText);
}

void CDxView::Update ()
{
	if (IsRunning ()) {
		//LOCK (m_cs);

		for (int i = 0; i < m_vObjects.GetSize (); i++) {
			CDxObject * p = m_vObjects [i];

			if (p)
				p->Render (m_pParams);
		}
	}
}

bool CDxView::AddObject(CDxObject *pObject)
{
	if (pObject && !ContainsObject (pObject)) {
		m_vObjects.Add (pObject);
		//Update ();
		return true;
	}

	return false;
}

bool CDxView::DeleteObject(const CDxObject *pObject)
{
	for (int i = 0; i < m_vObjects.GetSize (); i++) {
		if (m_vObjects [i] == pObject) {
			m_vObjects.RemoveAt (i);
			return true;
		}
	}

	return false;
}

bool CDxView::ContainsObject (CDxObject * pObject) const
{
	for (int i = 0; i < m_vObjects.GetSize (); i++) 
		if (m_vObjects [i] == pObject) 
			return true;

	return false;
}

bool CDxView::SetResolution (int nRes) 
{
	if (!IsRunning () && nRes > 2 && nRes < 15) {
		m_nResolution = nRes;
		return true;
	}

	return false;
}