// CQuadruple.h: interface for the CQuadruple class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_QUADRUPLE_H__6F895BB5_BF08_4469_B80C_5323DBC831A8__INCLUDED_)
#define AFX_QUADRUPLE_H__6F895BB5_BF08_4469_B80C_5323DBC831A8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include  <math.h>
#include "..\3dApi.h"

namespace Foxjet3d
{
	typedef long double REAL;

	const REAL rPI = (REAL)3.14159265358979323846;

	class CMatrix;

	_3D_API int Min (const CPoint & pt);
	_3D_API int Max (const CPoint & pt);

	_3D_API int ThousandthsToLogical (int n);
	_3D_API int ThousandthsToLogical (int n, int nRes);
	_3D_API CPoint ThousandthsToLogical (const CPoint & pt);
	_3D_API CPoint ThousandthsToLogical (const CPoint & pt, const CSize & res);

	_3D_API int LogicalToThousandths (int n);
	_3D_API int LogicalToThousandths (int n, int nRes);
	_3D_API CPoint LogicalToThousandths (const CPoint & pt);
	_3D_API CPoint LogicalToThousandths (const CPoint & pt, const CSize & res);

	template <class T = Foxjet3d::REAL>
	class CQuadruple
	{
	public:
		CQuadruple (T x = 0, T y = 0, T z = 0, T w = 0);
		CQuadruple (const CQuadruple & rhs);
		CQuadruple & operator = (CQuadruple const &);
		virtual ~CQuadruple ();

		typedef T TYPE;

		T & GetElement (int nIndex);
		const T & GetElement (int nIndex) const;

		T & operator [] (int nIndex);
		const T & operator [] (int nIndex) const;

		T GetX () const;
		T GetY () const;
		T GetZ () const;
		T GetW () const;

		bool SetElement (int nIndex, T rValue);

		void SetX (T rValue);
		void SetY (T rValue);
		void SetZ (T rValue);
		void SetW (T rValue);

	public:
		
		CQuadruple & operator += (const CQuadruple & rhs);
		CQuadruple & operator -= (const CQuadruple & rhs);
		CQuadruple & operator *= (T rFactor);
		CQuadruple & operator /= (T rFactor);

		static T GetDistance (const CQuadruple & from, const CQuadruple & to);

	protected:
		T m_rElements [4];
	};
}; // namespace Foxjet3d

template <class T> bool _3D_API operator == (const Foxjet3d::CQuadruple<T> & lhs, const Foxjet3d::CQuadruple<T> & rhs);
template <class T> bool _3D_API operator != (const Foxjet3d::CQuadruple<T> & lhs, const Foxjet3d::CQuadruple<T> & rhs);
template <class T> Foxjet3d::CQuadruple<T> _3D_API operator - (const Foxjet3d::CQuadruple<T> & lhs, const Foxjet3d::CQuadruple<T> & rhs);
template <class T> Foxjet3d::CQuadruple<T> _3D_API operator - (const Foxjet3d::CQuadruple<T> & rhs);
template <class T> Foxjet3d::CQuadruple<T> _3D_API operator + (const Foxjet3d::CQuadruple<T> & lhs, const Foxjet3d::CQuadruple<T> & rhs);
template <class T> Foxjet3d::CQuadruple<T> _3D_API operator * (const Foxjet3d::CQuadruple<T> & lhs, T rFactor);
template <class T> Foxjet3d::CQuadruple<T> _3D_API operator * (T rFactor, const Foxjet3d::CQuadruple<T> & rhs);
template <class T> Foxjet3d::CQuadruple<T> _3D_API operator / (const Foxjet3d::CQuadruple<T> & lhs, T rFactor);
template <class T> Foxjet3d::CQuadruple<T> _3D_API operator / (T rFactor, const Foxjet3d::CQuadruple<T> & rhs);
template <class T, class CAST> _3D_API Foxjet3d::CQuadruple<T> Cast (const Foxjet3d::CQuadruple<T> & rhs);

#ifndef __QUADRUPLE_INLINE__
	#define __QUADRUPLE_INLINE__
	#include <Quadruple.inl>
#endif //__QUADRUPLE_INLINE__

#endif // !defined(AFX_QUADRUPLE_H__6F895BB5_BF08_4469_B80C_5323DBC831A8__INCLUDED_)
