// DxObject.cpp: implementation of the CDxObject class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DxObject.h"
#include "Debug.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace Foxjet3d;
using namespace Foxjet3d::Box;
using namespace DxParams;

IMPLEMENT_DYNAMIC (CDxObject, CObject)

CDxObject::CDxObject()
:	m_pos (0, 0)
{
}

CDxObject::CDxObject (const CDxObject & rhs)
:	m_pos (rhs.m_pos)
{
}

CDxObject & CDxObject::operator = (const CDxObject & rhs)
{
	if (this != &rhs) {
		m_pos = rhs.m_pos;
	}

	return * this;
}

CDxObject::~CDxObject()
{
}

CPoint CDxObject::GetPos () const
{
	return m_pos;
}

bool CDxObject::SetPos (int x, int y)
{
	return SetPos (CPoint (x, y));
}

bool CDxObject::SetPos (const CPoint & pos)
{
	m_pos = pos;
	return true;
}

void CDxObject::Restore (CDxParams * pParams, bool bSurfaceOnly)
{
}

bool CDxObject::Create (CDxView & view)
{
	return false;
}

void CDxObject::Free (bool bKeepSurfaces)
{
}
