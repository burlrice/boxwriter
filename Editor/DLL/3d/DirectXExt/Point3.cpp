// Point3.cpp: implementation of the CPoint3 class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Point3.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace Foxjet3d;

Foxjet3d::CPoint3::CPoint3(REAL x, REAL y, REAL z)
:	CQuadruple <REAL> (x, y, z, 1)
{
}

Foxjet3d::CPoint3::CPoint3 (const Foxjet3d::CPoint3 & rhs)
:	CQuadruple <REAL> (rhs)
{
}

Foxjet3d::CPoint3::CPoint3 (const Foxjet3d::CQuadruple <REAL> & rhs)
:	CQuadruple <REAL> (rhs)
{
}

Foxjet3d::CPoint3 & Foxjet3d::CPoint3::operator = (const Foxjet3d::CPoint3 & rhs)
{
	CQuadruple <REAL>::operator = (rhs);
	return * this;
}

Foxjet3d::CPoint3::~CPoint3()
{
}

void Foxjet3d::CPoint3::Homogenize ()
{
	m_rElements [0] = m_rElements [0] / m_rElements [3];
	m_rElements [1] = m_rElements [1] / m_rElements [3];
	m_rElements [2] = m_rElements [2] / m_rElements [3];
}
