#ifdef __QUADRUPLE_INLINE__

template <class T> inline 
Foxjet3d::CQuadruple<T>::CQuadruple (T x, T y, T z, T w)
{
	m_rElements [0] = x;
	m_rElements [1] = y;
	m_rElements [2] = z;
	m_rElements [3] = w;
}

template <class T> inline 
Foxjet3d::CQuadruple<T>::CQuadruple (const CQuadruple & rhs)
{
	for (int i = 0; i < 4; i++)
		m_rElements [i] = rhs.m_rElements [i];
}

template <class T> inline 
Foxjet3d::CQuadruple<T> & Foxjet3d::CQuadruple<T>::operator = (const Foxjet3d::CQuadruple<T> & rhs)
{
	if (this != &rhs) {
		for (int i = 0; i < 4; i++)
			m_rElements [i] = rhs.m_rElements [i];
	}

	return * this;
}

template <class T> inline 
Foxjet3d::CQuadruple<T>::~CQuadruple ()
{
}

template <class T> inline 
T & Foxjet3d::CQuadruple<T>::GetElement (int nIndex)
{
	if (nIndex >= 0 && nIndex < 4)
		return m_rElements [nIndex];
	else {
		ASSERT (0);
		return m_rElements [0];
	}
}

template <class T> inline 
const T & Foxjet3d::CQuadruple<T>::GetElement (int nIndex) const
{
	if (nIndex >= 0 && nIndex < 4)
		return m_rElements [nIndex];
	else {
		ASSERT (0);
		return m_rElements [0];
	}
}

template <class T> inline 
T & Foxjet3d::CQuadruple<T>::operator [] (int nIndex)
{
	return GetElement (nIndex);
}

template <class T> inline 
const T & Foxjet3d::CQuadruple<T>::operator [] (int nIndex) const
{
	return GetElement (nIndex);
}

template <class T> inline 
bool Foxjet3d::CQuadruple<T>::SetElement (int nIndex, T rValue)
{
	if (nIndex >= 0 && nIndex < 4) {
		m_rElements [nIndex] = rValue;
		return true;
	}

	return false;
}

template <class T> inline 
T Foxjet3d::CQuadruple<T>::GetX () const
{
	return m_rElements [0];
}

template <class T> inline 
T Foxjet3d::CQuadruple<T>::GetY () const
{
	return m_rElements [1];
}

template <class T> inline 
T Foxjet3d::CQuadruple<T>::GetZ () const
{
	return m_rElements [2];
}

template <class T> inline 
T Foxjet3d::CQuadruple<T>::GetW () const
{
	return m_rElements [3];
}

template <class T> inline 
void Foxjet3d::CQuadruple<T>::SetX (T value)
{
	m_rElements [0] = value;
}

template <class T> inline 
void Foxjet3d::CQuadruple<T>::SetY (T value)
{
	m_rElements [1] = value;
}

template <class T> inline 
void Foxjet3d::CQuadruple<T>::SetZ (T value)
{
	m_rElements [2] = value;
}

template <class T> inline 
void Foxjet3d::CQuadruple<T>::SetW (T value)
{
	m_rElements [3] = value;
}

template <class T> inline 
T Foxjet3d::CQuadruple<T>::GetDistance (const CQuadruple & from, 
							  const CQuadruple & to)
{
	T x = (T)fabs (from.GetX () - to.GetX ());
	T y = (T)fabs (from.GetY () - to.GetY ());
	T z = (T)fabs (from.GetZ () - to.GetZ ());
	T w = (T)fabs (from.GetW () - to.GetW ());
	return (T)sqrt ((x * x) + (y * y) + (z * z) + (w * w));
}

template <class T> inline 
Foxjet3d::CQuadruple<T> & Foxjet3d::CQuadruple<T>::operator += (const Foxjet3d::CQuadruple<T> & rhs)
{
	* this = (* this) + rhs;
	return * this;
}

template <class T> inline 
Foxjet3d::CQuadruple<T> & Foxjet3d::CQuadruple<T>::operator -= (const Foxjet3d::CQuadruple<T> & rhs)
{
	* this = (* this) - rhs;
	return * this;
}

template <class T> inline 
Foxjet3d::CQuadruple<T> & Foxjet3d::CQuadruple<T>::operator *= (T rFactor)
{
	* this = (* this) * rFactor;
	return * this;
}

template <class T> inline 
Foxjet3d::CQuadruple<T> & Foxjet3d::CQuadruple<T>::operator /= (T rFactor)
{
	* this = (* this) / rFactor;
	return * this;
}

////////////////////////////////////////////////////////////////////////
// 
template <class T> inline 
bool operator == (const Foxjet3d::CQuadruple<T> & lhs, const Foxjet3d::CQuadruple<T> & rhs)
{
	return 
		lhs.GetX () == rhs.GetX () && 
		lhs.GetY () == rhs.GetY () && 
		lhs.GetZ () == rhs.GetZ () && 
		lhs.GetW () == rhs.GetW (); 
}

template <class T> inline 
bool operator != (const Foxjet3d::CQuadruple<T> & lhs, const Foxjet3d::CQuadruple<T> & rhs)
{
	return !(lhs == rhs);
}

template <class T> inline 
Foxjet3d::CQuadruple<T> operator - (const Foxjet3d::CQuadruple<T> & lhs, const Foxjet3d::CQuadruple<T> & rhs)
{
	return CQuadruple <T> (
		lhs.GetX () - rhs.GetX (),
		lhs.GetY () - rhs.GetY (),
		lhs.GetZ () - rhs.GetZ ());
}

template <class T> inline 
Foxjet3d::CQuadruple<T> operator - (const Foxjet3d::CQuadruple<T>  & rhs)
{
	return CQuadruple <T> (
		-rhs.GetX (),
		-rhs.GetY (),
		-rhs.GetZ ());
}

template <class T> inline 
Foxjet3d::CQuadruple<T> operator + (const Foxjet3d::CQuadruple<T>  & lhs, const Foxjet3d::CQuadruple<T> & rhs)
{
	return CQuadruple <T> (
		lhs.GetX () - rhs.GetX (),
		lhs.GetY () - rhs.GetY (),
		lhs.GetZ () - rhs.GetZ ());
}

template <class T> inline 
Foxjet3d::CQuadruple<T> operator * (const Foxjet3d::CQuadruple<T> & rhs, T rFactor)
{
	return CQuadruple <T> (
		rhs.GetX() * rFactor,
		rhs.GetY() * rFactor,
		rhs.GetZ() * rFactor);
}

template <class T> inline 
Foxjet3d::CQuadruple<T> operator / (const Foxjet3d::CQuadruple<T> & rhs, T rFactor)
{
	return CQuadruple <T> (
		rhs.GetX() / rFactor,
		rhs.GetY() / rFactor,
		rhs.GetZ() / rFactor);
}

template <class T> inline 
Foxjet3d::CQuadruple<T> operator / (T rFactor, const Foxjet3d::CQuadruple<T> & rhs) 
{ 
	return rhs / rFactor; 
}

template <class T> inline 
Foxjet3d::CQuadruple<T> operator * (T rFactor, const Foxjet3d::CQuadruple<T> & rhs) 
{ 
	return rhs * rFactor; 
}

template <class T, class CAST> inline
Foxjet3d::CQuadruple<CAST> Cast (const Foxjet3d::CQuadruple<T> & rhs) 
{ 
	CAST x = (CAST)rhs.GetX ();
	CAST y = (CAST)rhs.GetY ();
	CAST z = (CAST)rhs.GetZ ();
	CAST w = (CAST)rhs.GetW ();

	return CQuadruple <CAST> (x, y, z, w); 
}

#endif //__QUADRUPLE_INLINE__
