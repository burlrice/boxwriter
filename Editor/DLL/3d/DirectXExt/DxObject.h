// DxObject.h: interface for the CDxObject class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DXOBJECT_H__2732E66D_2856_4B4B_AE5B_EAB46F8F6D10__INCLUDED_)
#define AFX_DXOBJECT_H__2732E66D_2856_4B4B_AE5B_EAB46F8F6D10__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "..\3dApi.h"
#include "..\BoxForwardDeclare.h"

namespace Foxjet3d {
	typedef long double REAL;

	namespace DxParams
	{

		typedef enum
		{ 
			ZOOM			= (1 << 1), 
			ENUM_END		= ZOOM,
		};

		class _3D_API CDxParams : public CObject
		{
			DECLARE_DYNAMIC (CDxParams)
		public:
			CDxParams (CDxView & view);
			CDxParams (const CDxParams & rhs);
			CDxParams & operator = (const CDxParams & rhs);
			virtual ~CDxParams ();

			void LoadSettings (CWinApp & app, const CString & strSection);
			void SaveSettings (CWinApp & app, const CString & strSection) const;
		
			CDxView &	m_view;
			REAL		m_rZoom;

			friend _3D_API ULONG Compare (const CDxParams & lhs, const CDxParams & rhs);
		};

		class _3D_API CDxObject : public CObject
		{
			DECLARE_DYNAMIC (CDxObject)
		public:

			CDxObject ();
			CDxObject (const CDxObject & rhs);
			CDxObject & operator = (const CDxObject & rhs);
			virtual ~CDxObject();

			virtual void Render (CDxParams * pParams) = 0;
			virtual void Restore (CDxParams * pParams, bool bSurfaceOnly = true);
			virtual bool Create (CDxView & view);
			virtual void Free (bool bKeepSurfaces = false);

			CPoint GetPos () const;
			virtual bool SetPos (int x, int y);
			virtual bool SetPos (const CPoint & pos);

		protected:
			CPoint m_pos;
		};
	}; //namespace DxParams
}; //namespace Foxjet3d


#endif // !defined(AFX_DXOBJECT_H__2732E66D_2856_4B4B_AE5B_EAB46F8F6D10__INCLUDED_)
