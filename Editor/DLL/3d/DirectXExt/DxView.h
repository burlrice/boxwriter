// BoxView.h: interface for the CBoxView class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BOXVIEW_H__74869F0B_283B_46FF_AA0C_103704A12A30__INCLUDED_)
#define AFX_BOXVIEW_H__74869F0B_283B_46FF_AA0C_103704A12A30__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <atlbase.h>
#include <afxtempl.h>
#include <afxmt.h>
#include "..\3dApi.h"
#include "DxObject.h"

namespace Foxjet3d
{
	namespace DxParams
	{
		class _3D_API CDxImp : public CObject
		{
			DECLARE_DYNAMIC (CDxImp)
		public:
			CDxImp (bool bAutoDeleteParam = false);
			virtual ~CDxImp ();

			virtual bool Create (CWnd * pParent, DxParams::CDxParams * pParams);
			virtual bool Destroy ();
		
			void Restore ();
			CSize GetPrimarySurfaceSize () const;
			bool CreateDevice ();
			bool SetState ();
			bool CreateSurfaces (const CSize & size);
			bool InitDirectX (CWnd * pParent, const CSize & size);
			bool CreateViewport (const CSize & size);
			int GetResolution () const;
			virtual bool SetResolution (int nRes);

			CWnd * GetParent () const { return m_pParent; }
			GUID GetGuid () const { return m_guidDevice; }

		protected:
			CWnd *								m_pParent;
			GUID								m_guidDevice;
			int									m_nResolution;
			bool								m_bAutoDeleteParam;

		public:
			CFont								m_fnt;

		public:
			DxParams::CDxParams *				m_pParams;
			//CCriticalSection					m_cs;
		};

		class _3D_API CDxView : public CDxImp
		{
			DECLARE_DYNAMIC (CDxView)
		public:
			CDxView(bool bAutoDeleteParam = true);
			virtual ~CDxView();

			typedef enum { BEGINDRAWLOOP = 0, ENDDRAWLOOP } EVENTTYPE;
			virtual bool Create (CWnd * pParent, DxParams::CDxParams * pParams);
			virtual bool Destroy ();
			virtual void Restore ();
			virtual bool SetResolution (int nRes);

			void Update ();
			void Draw (CDC * pDC, const CRect & rc, double dZoom = 1.0);
			bool DeleteObject (const DxParams::CDxObject * pObject);
			bool AddObject (DxParams::CDxObject * pObject);
			bool ContainsObject (DxParams::CDxObject * pObject) const;

		protected:
			void DrawParams (CDC & dc);
			bool IsRunning () const { return m_bRunning; }
			//static DWORD CALLBACK DrawFunc (LPVOID lpArg);

			CArray <DxParams::CDxObject *, DxParams::CDxObject *>	m_vObjects;
			bool													m_bRunning;
		};
	}; //namespace DxParams
}; //namespace Foxjet3d

#endif // !defined(AFX_BOXVIEW_H__74869F0B_283B_46FF_AA0C_103704A12A30__INCLUDED_)
