// Size3.cpp: implementation of the CSize class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Size3.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace Foxjet3d;

Foxjet3d::CSize3::CSize3 (REAL cx, REAL cy, REAL cz)
:	CPoint3 (cx, cy, cz)
{
}

Foxjet3d::CSize3::CSize3 (const Foxjet3d::CSize3 & rhs)
:	CPoint3 (rhs.GetX (), rhs.GetY (), rhs.GetZ ())
{
}

Foxjet3d::CSize3::CSize3 (const Foxjet3d::CPoint3 & rhs)
:	CPoint3 (rhs)
{
}

Foxjet3d::CSize3 & Foxjet3d::CSize3::operator = (const Foxjet3d::CSize3 & rhs)
{
	CPoint3::operator = (rhs);
	return * this;
}

Foxjet3d::CSize3::~CSize3()
{
}

REAL Foxjet3d::CSize3::Length () const
{
	return GetY ();
}

REAL Foxjet3d::CSize3::Width () const
{
	return GetX ();
}

REAL Foxjet3d::CSize3::Height () const
{
	return GetZ ();
}

