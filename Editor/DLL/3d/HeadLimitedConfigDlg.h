#if !defined(AFX_HEADLIMITEDCONFIG_H__217EF007_B965_441E_9755_B445D2B7A35B__INCLUDED_)
#define AFX_HEADLIMITEDCONFIG_H__217EF007_B965_441E_9755_B445D2B7A35B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HeadLimitedConfig.h : header file
//

#include "OdbcDatabase.h"
#include "List.h"
#include "3dApi.h"
#include "RoundButtons.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CHeadLimitedConfigDlg dialog

namespace Foxjet3d
{
	class _3D_API CHeadLimitedConfigDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CHeadLimitedConfigDlg (FoxjetDatabase::COdbcDatabase & db, UNITS units, ULONG lLineID, CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CHeadLimitedConfigDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		typedef void (CALLBACK * PCHANGEFCT)(ULONG lHeadID, ULONG lOffset);
		typedef void (CALLBACK * PINKCODEFCT)(ULONG lHeadID);

		PCHANGEFCT m_lpfct;
		PINKCODEFCT m_lpfctInkCode;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CHeadLimitedConfigDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		FoxjetDatabase::COdbcDatabase & m_db;
		UNITS m_units;
		UINT m_nCurSelID;
		long m_lIncrement;
		FoxjetDatabase::HEADSTRUCT m_head;
		ULONG m_lLineID;
		FoxjetUtils::CRoundButtons m_buttons;
		int m_nInit;

		void DoCallback();

		afx_msg void OnLButtonDown( UINT nFlags, CPoint point );
		afx_msg void OnSelChangeLine();
		afx_msg void OnApply();

		virtual void OnOK ();
		virtual BOOL OnNotify( WPARAM wParam, LPARAM lParam, LRESULT* pResult );

		typedef struct 
		{
			UINT m_nBtnID;
			UINT m_nLblID;
			UINT m_nTxtID;
			long m_lPhotocellDelay;
			ULONG m_lHeadID;
		} MAPSTRUCT;

		static MAPSTRUCT m_map [8];

		// Generated message map functions
		//{{AFX_MSG(CHeadLimitedConfigDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void CHeadLimitedConfigDlg::OnClickedInkCode (UINT nID);

	//}}AFX_MSG

		DECLARE_MESSAGE_MAP()
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEADLIMITEDCONFIG_H__217EF007_B965_441E_9755_B445D2B7A35B__INCLUDED_)
