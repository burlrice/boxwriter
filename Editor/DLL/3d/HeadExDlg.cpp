// HeadExDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "3d.h"
#include "HeadExDlg.h"
#include "HeadDlg.h"
#include "Debug.h"
#include "ComPropertiesDlg.h"
#include "fj_defines.h"
#include "TemplExt.h"
#include "AppVer.h"
#include "DevGuids.h"
#include "HeadConfigDlg.h"
#include "ElementDefaults.h"
#include "StdCommDlg.h"
#include "List.h"

#ifdef __IPPRINTER__
	#include "NetCommDlg.h"
#endif

#ifdef __WINPRINTER__
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;

/////////////////////////////////////////////////////////////////////////////
// CHeadExDlg dialog

CHeadExDlg::CHeadExDlg(FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::HEADSTRUCT & head, 
					   UNITS units, 
#ifdef __WINPRINTER__
					   const CMapStringToString & vAvailAddressMap, 
#endif //__WINPRINTER__
					   ULONG lLineID, CWnd* pParent)
:	m_db (db),
	m_head (head),
	m_bReadOnly (false),
	m_units (units),
	m_lLineID (lLineID),
	FoxjetCommon::CEliteDlg(IDD_HEAD_EX_MATRIX, pParent)
{
#ifdef __WINPRINTER__
	for (POSITION pos = vAvailAddressMap.GetStartPosition(); pos != NULL; ) {
		CString strKey, strAddr;

		vAvailAddressMap.GetNextAssoc (pos, strKey, strAddr);
		m_vAvailAddressMap [strKey] = strAddr;
	}	
#endif //__WINPRINTER__
}


void CHeadExDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHeadExDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	int nNET = m_head.m_bDigiNET ? 1 : 0;

	DDX_Radio(pDX, RDO_NET, nNET);

	if (pDX->m_bSaveAndValidate)
		m_head.m_bDigiNET = nNET == 1;


	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_SERIALPARAMS);
}


BEGIN_MESSAGE_MAP(CHeadExDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CHeadExDlg)
	ON_BN_CLICKED(BTN_SERIALPARAMS, OnSerialparams)
	ON_BN_CLICKED(RDO_NET, OnUpdateUI)
	ON_BN_CLICKED(RDO_NEXT, OnUpdateUI)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHeadExDlg message handlers

void CHeadExDlg::OnSerialparams() 
{
#ifdef __WINPRINTER__
	if (EditSerialParams (m_head.m_strSerialParams)) {
		StdCommDlg::CCommItem item (false);

		item.FromString (_T ("0,") + m_head.m_strSerialParams);
		SetDlgItemText (TXT_SERIALPARAMS, item.GetDispText (1));
	}
#else //__IPPRINTER__
	if (EditNetSerialParams (m_head.m_strSerialParams)) {
		NetCommDlg::CCommItem item;

		item.FromString (m_head.m_strSerialParams);
		SetDlgItemText (TXT_SERIALPARAMS, item.GetDispText (1));
	}
#endif
}

BOOL CHeadExDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	if (CWnd * p = GetDlgItem (IDOK))
		p->EnableWindow (!m_bReadOnly);
	
	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_LINK)) {
		CArray <HEADSTRUCT, HEADSTRUCT &> v;

		GetLineHeads (m_db, m_lLineID, v);

		p->SetItemData (p->AddString (LoadString (IDS_NOTHING)), 0);

		for (int i = 0; i < v.GetSize (); i++) {
			HEADSTRUCT & h = v [i];

			if (h.m_lLinkedTo == 0) {
				if (h.m_lID != m_head.m_lID) {
					int nIndex = p->AddString (h.m_strName);
					p->SetItemData (nIndex, h.m_lID);

					if (h.m_lID == m_head.m_lLinkedTo)
						p->SetCurSel (nIndex);
				}
			}
		}

		if (p->GetCurSel () == CB_ERR)
			p->SetCurSel (0);
	}

#ifdef __WINPRINTER__
	{
		StdCommDlg::CCommItem item (false);

		item.FromString (_T ("0,") + m_head.m_strSerialParams);
		SetDlgItemText (TXT_SERIALPARAMS, item.GetDispText (1));

		if (IsValveHead (m_head.m_nHeadType)) {
			ULONG lAddress = _tcstoul (m_head.m_strUID, NULL, 16);

			if (CWnd * p = GetDlgItem (BTN_SERIALPARAMS))
				p->EnableWindow (lAddress == 0x300);
		}
	}

	if (CWnd * p = GetDlgItem (RDO_NET))
		p->ShowWindow (SW_HIDE);
	if (CWnd * p = GetDlgItem (RDO_NEXT))
		p->ShowWindow (SW_HIDE);
#else
	{
		NetCommDlg::CCommItem item;

		item.FromString (m_head.m_strSerialParams);
		SetDlgItemText (TXT_SERIALPARAMS, item.GetDispText (1));
	}
#endif

#ifndef __WINPRINTER__
	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_LINK)) 
		p->ShowWindow (SW_HIDE);
	if (CComboBox * p = (CComboBox *)GetDlgItem (LBL_LINK)) 
		p->ShowWindow (SW_HIDE);
	if (CButton * p = (CButton *)GetDlgItem (CHK_SAVEDYNDATA))
		p->SetCheck (FoxjetCommon::GetSaveDynData (m_db, m_head.m_lID));
#endif

#ifdef __WINPRINTER__
	if (CComboBox * p = (CComboBox *)GetDlgItem (GRP_CONTROLLER)) 
		p->ShowWindow (SW_HIDE);
	if (CWnd * p = GetDlgItem (CHK_SAVEDYNDATA))
		p->ShowWindow (SW_HIDE);
#endif

	OnUpdateUI ();

	if (m_head.IsUSB ()) {
		UINT n [] = 
		{
			LBL_SERIALPARAMS,
			TXT_SERIALPARAMS,
			BTN_SERIALPARAMS,
		};

		for (int i = 0; i < ARRAYSIZE (n); i++)
			if (CWnd * p = GetDlgItem (n [i]))
				p->ShowWindow (SW_HIDE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CHeadExDlg::OnOK()
{
	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_LINK)) {
		m_head.m_lLinkedTo = p->GetItemData (p->GetCurSel ());
	}

	#ifdef __IPPRINTER__
	if (CButton * p = (CButton *)GetDlgItem (CHK_SAVEDYNDATA))
		FoxjetCommon::SetSaveDynData (m_db, m_head.m_lID, p->GetCheck () == 1 ? true : false);
	#endif //__IPPRINTER__

	FoxjetCommon::CEliteDlg::OnOK ();
}

void CHeadExDlg::OnUpdateUI() 
{
	bool bNEXT = false;

	if (CButton * p = (CButton *)GetDlgItem (RDO_NEXT))
		bNEXT = p->GetCheck () == 1 ? true : false;

	if (CWnd * p = GetDlgItem (CHK_SAVEDYNDATA)) {
		#ifdef __IPPRINTER__
			p->EnableWindow (bNEXT);
		#else
			p->ShowWindow (SW_HIDE);
		#endif
	}

}
