#include "stdafx.h"
#include "3d.h"
#include "Box.h"
#include "Color.h"
#include "List.h"
#include "Database.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace Panel;

Panel::CBoundary::CBoundary ()
:	m_lID (-1),
	m_lPanelID (-1),
	m_y (0),
	m_cy (0),
	m_lLinkedTo (0)
{
}

Panel::CBoundary::CBoundary (const CBoundary & rhs)
:	m_lID (rhs.m_lID),
	m_lPanelID (rhs.m_lPanelID),
	m_card (rhs.m_card),
	m_y (rhs.m_y),
	m_cy (rhs.m_cy),
	m_lLinkedTo (rhs.m_lLinkedTo)
{
}

Panel::CBoundary::CBoundary (DB::HEADSTRUCT & card, CArray <DB::VALVESTRUCT, DB::VALVESTRUCT &> & vValve, ULONG lID, ULONG lPanelID)
:	m_lID (lID),
	m_lPanelID (lPanelID),
	m_card (card),
	m_y (0),
	m_cy (0),
	m_lLinkedTo (0)
{
//	TRACEF ("CBoundary::CBoundary");

	m_card.m_vValve.RemoveAll ();
	m_card.m_vValve.Copy (vValve);

	m_card.m_vValve.Copy (vValve);

	ASSERT (m_card.m_vValve.GetSize ());
	m_y = m_card.m_vValve [0].m_lHeight + GetValveHeight (m_card.m_vValve [0].m_type);

	m_card.m_nChannels		= 0;
	m_card.m_dNozzleSpan	= 0.0;

	for (int i = 0; i < m_card.m_vValve.GetSize (); i++) {
		VALVESTRUCT & valve = m_card.m_vValve [i];

		m_card.m_nChannels		+= GetValveChannels (valve.m_type);
		m_card.m_dNozzleSpan	+= (double)GetValveHeight (valve.m_type) / 1000.0;
		m_cy					+= GetValveHeight (valve.m_type);
	}
}

Panel::CBoundary::CBoundary (DB::HEADSTRUCT & card, ULONG lID, ULONG lPanelID)
:	m_lID (lID),
	m_lPanelID (lPanelID),
	m_card (card),
	m_y (0),
	m_cy (0),
	m_lLinkedTo (0)
{
	m_card.m_vValve.RemoveAll ();

	m_y		= card.m_lRelativeHeight;
	m_cy	= CBaseElement::LogicalToThousandths (CSize (0, card.Height ()), card).y;
}

CBoundary & Panel::CBoundary::operator = (const CBoundary & rhs)
{
//	TRACEF ("CBoundary::operator =");

	if (this != &rhs) {
		m_lID		= rhs.m_lID;
		m_lPanelID	= rhs.m_lPanelID;
		m_card		= rhs.m_card;
		m_y			= rhs.m_y;
		m_cy		= rhs.m_cy;
		m_lLinkedTo	= rhs.m_lLinkedTo;
	}

	return * this;
}

Panel::CBoundary::~CBoundary ()
{
}

int Panel::CBoundary::Height () const
{
	int cy = 0;

	if (!m_card.m_vValve.GetSize ()) 
		return m_card.Height ();

	for (int i = 0; i < m_card.m_vValve.GetSize (); i++)
		cy += m_card.m_vValve [i].Height ();

	return cy;
}

CString Panel::CBoundary::GetName () const
{
	if (m_card.m_vValve.GetSize ())
		return m_card.m_vValve [0].m_strName;

	return m_card.m_strName;
}

ULONG Panel::CBoundary::GetY () const 
{ 
	ULONG y = m_y;

	for (int i = 0; i < m_card.m_vValve.GetSize (); i++) {
		y -= GetValveHeight (m_card.m_vValve [i].m_type);
		//break;
	}

	return y; 
}
