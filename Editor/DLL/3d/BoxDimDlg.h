#if !defined(AFX_BOXDIMDLG_H__4E1CEE63_3AB7_423D_A245_12527F469C00__INCLUDED_)
#define AFX_BOXDIMDLG_H__4E1CEE63_3AB7_423D_A245_12527F469C00__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BoxDimDlg.h : header file
//

#include "Database.h"
#include "List.h"
#include "3dApi.h"
#include "DxView.h"
#include "Box.h"
#include "ListCtrlImp.h"
#include "Utils.h"

namespace Foxjet3d
{
	/////////////////////////////////////////////////////////////////////////////
	// CBoxDimDlg dialog

	class _3D_API CBoxDimDlg : public FoxjetCommon::CEliteDlg
	{
		DECLARE_DYNAMIC (CBoxDimDlg)
			
	// Construction
	public:
		CBoxDimDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent = NULL);   // standard constructor
		virtual ~CBoxDimDlg ();

	// Dialog Data
		//{{AFX_DATA(CBoxDimDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		FoxjetDatabase::COdbcDatabase & m_db;
		FoxjetDatabase::BOXSTRUCT		m_box;
		UNITS							m_units;
		bool							m_bReadOnly;
		BOOL							m_bPreview;
		bool							m_bEdit; // true if editing existing box, false if adding new
		ULONG							m_lLineID;

	public:
		bool IsPreviewEnabled () const;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CBoxDimDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		class CItem : 
			public ItiLibrary::ListCtrlImp::CItem,
			public FoxjetDatabase::IMAGESTRUCT
		{
		public:
			CItem (const FoxjetDatabase::IMAGESTRUCT & rhs);
			virtual ~CItem ();

			DECLARE_CONSTBASEACCESS (FoxjetDatabase::IMAGESTRUCT)

			virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
			virtual CString GetDispText (int nColumn) const;
		};

	protected:
		virtual void OnCancel();
		void InvalidateBox ();
		virtual void OnOK();
		afx_msg void OnDelete();
		afx_msg void OnEdit();
		afx_msg void OnAdd();
		afx_msg void OnPreviewClick();
		afx_msg void OnItemchangedImages(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnDblclickBoxes(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnHelp ();

		CStatic m_wndPreview;
		DxParams::CDxView m_3d;
		Box::CBox m_3dBox;
		CTask m_task;
		bool m_bCreated3d;
		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
		CArray <FoxjetDatabase::IMAGESTRUCT, FoxjetDatabase::IMAGESTRUCT &> m_vImages;

		// Generated message map functions
		//{{AFX_MSG(CBoxDimDlg)
		virtual BOOL OnInitDialog();
		afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
		afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
		afx_msg void OnDestroy();
	//}}AFX_MSG

		DECLARE_MESSAGE_MAP()
	};
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BOXDIMDLG_H__4E1CEE63_3AB7_423D_A245_12527F469C00__INCLUDED_)
