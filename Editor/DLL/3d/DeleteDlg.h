#if !defined(AFX_DELETEDLG_H__B7E3B625_9FA0_4D0C_B04C_BF5B8DBAC5C4__INCLUDED_)
#define AFX_DELETEDLG_H__B7E3B625_9FA0_4D0C_B04C_BF5B8DBAC5C4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DeleteDlg.h : header file
//

#include "3dApi.h"
#include "OpenDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CDeleteDlg dialog

namespace Foxjet3d
{
	typedef bool (CALLBACK * LPFCT_IS_TASK_OPEN) (ULONG lTaskID);								//bool IsTaskOpen (ULONG lTaskID) const;
	typedef void (CALLBACK * LPFCT_BEGIN_PROGRESS_BAR) (const CString & str, int nMax);			//void BeginProgressBar (const CString & str, int nMax);
	typedef void (CALLBACK * LPFCT_STEP_PROGRESS_BAR) (const CString & str);					//void StepProgressBar (const CString & str);
	typedef void (CALLBACK * LPFCT_END_PROGRESS_BAR) ();										//void EndProgressBar ();

	class _3D_API CDeleteDlg : public Foxjet3d::OpenDlg::COpenDlg
	{
	// Construction
	public:
		CDeleteDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CDeleteDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		LPFCT_IS_TASK_OPEN			m_lpfctIsTaskOpen;
		LPFCT_BEGIN_PROGRESS_BAR	m_lpfctBeginProgressBar;
		LPFCT_STEP_PROGRESS_BAR		m_lpfctStepProgressBar;
		LPFCT_END_PROGRESS_BAR		m_lpfctEndProgressBar;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CDeleteDlg)
		public:
		virtual BOOL PreTranslateMessage(MSG* pMsg);
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual void OnOK ();
		virtual void OnCancel ();
		afx_msg void OnDelete ();

		bool DocsOpen (const FoxjetCommon::CLongArray & sel);

		HACCEL m_hAccel;

		afx_msg void OnDblclkMessages(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnDblclkTasks(NMHDR* pNMHDR, LRESULT* pResult);

		// Generated message map functions
		//{{AFX_MSG(CDeleteDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnEditSelectall();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DELETEDLG_H__B7E3B625_9FA0_4D0C_B04C_BF5B8DBAC5C4__INCLUDED_)
