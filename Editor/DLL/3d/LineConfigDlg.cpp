// LineConfigDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "Resource.h"
#include "LineConfigDlg.h"
#include "LineDlg.h"
#include "List.h"
#include "Debug.h"
#include "FieldDefs.h"

using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace FoxjetCommon;
using namespace LineConfigDlg;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////

CLineItem::CLineItem (FoxjetDatabase::LINESTRUCT & line)
:	LINESTRUCT (line)
{

}

CLineItem::~CLineItem ()
{
}

CString CLineItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:		return m_strName;
	case 1:		return m_strDesc;
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CLineConfigDlg dialog


CLineConfigDlg::CLineConfigDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent /*=NULL*/)
:	m_db (db),
	m_bCoupled (FALSE),
	m_bLean (FALSE),
	m_lPrinterID (0), 
	FoxjetCommon::CEliteDlg(IDD_LINECONFIG_MATRIX, pParent)
{
	if (m_lPrinterID == 0)
		m_lPrinterID = GetPrinterID (m_db); 

	//{{AFX_DATA_INIT(CLineConfigDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


CLineConfigDlg::~CLineConfigDlg ()
{
}

void CLineConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLineConfigDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	DDX_Check (pDX, CHK_COUPLED, m_bCoupled);
	DDX_Check (pDX, CHK_IPLEAN, m_bLean);

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_ADD);
	m_buttons.DoDataExchange (pDX, BTN_EDIT);
	m_buttons.DoDataExchange (pDX, BTN_DELETE);
	m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);
}


BEGIN_MESSAGE_MAP(CLineConfigDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CLineConfigDlg)
	ON_BN_CLICKED(BTN_ADD, OnAdd)
	ON_BN_CLICKED(BTN_DELETE, OnDelete)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_NOTIFY(NM_DBLCLK, LV_LINE, OnDblclkLine)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLineConfigDlg message handlers

void CLineConfigDlg::UpdateSettingsTable (const LINESTRUCT & line, const CLineDlg & dlg)
{
	UpdateSettingsTable (m_db, line, dlg);
}

void CLineConfigDlg::UpdateSettingsTable (FoxjetDatabase::COdbcDatabase & db, const LINESTRUCT & line, const CLineDlg & dlg)
{
	SETTINGSSTRUCT s;

	s.m_strKey = FoxjetDatabase::Globals::m_lpszSerialDownload;
	s.m_lLineID = line.m_lID;
	s.m_strData = dlg.m_strSerialDownload;

	if (!UpdateSettingsRecord (db, s))
		VERIFY (AddSettingsRecord (db, s)); 

	s.m_strKey = FoxjetDatabase::Globals::m_lpszResetCounts;
	s.m_lLineID = line.m_lID;
	s.m_strData.Format (_T ("%d"), dlg.m_bResetCountsOnRestart);

	if (!UpdateSettingsRecord (db, s))
		VERIFY (AddSettingsRecord (db, s)); 

	s.m_strKey = FoxjetDatabase::Globals::m_lpszRequireLogin;
	s.m_lLineID = line.m_lID;
	s.m_strData.Format (_T ("%d"), dlg.m_bRequireLogin);

	if (!UpdateSettingsRecord (db, s))
		VERIFY (AddSettingsRecord (db, s)); 
}

void CLineConfigDlg::OnAdd() 
{
	LINESTRUCT line;
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	bool bUnnamed = true;
	const int nMax = GetMaxLines (m_db, _T ("TODO: rem"));

	GetPrinterLines (m_db, m_lPrinterID, vLines); //GetLineRecords (m_db, vLines);

	if (vLines.GetSize () >= nMax) {
		CString str;

		str.Format (LoadString (IDS_MAXPRODLINE), nMax);
		MsgBox (* this, str);
		return;
	}

	for (int i = 1; bUnnamed; i++) {
		CString str;
		ULONG lID = -1;

		str.Format (_T ("LINE%04d"), i);
		
		for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
			LINESTRUCT l = vLines [nLine];

			if (!l.m_strName.CompareNoCase (str)) {
				lID = l.m_lID;
				break;
			}
		}

		if (lID == -1) {
			bUnnamed = false;
			line.m_strName = str;
		}
	}

	line.m_lID						= -1;
	line.m_bResetScanInfo			= TRUE;
	line.m_strNoRead				= _T ("NOREAD");
	line.m_nMaxConsecutiveNoReads	= 3;
	line.m_nBufferOffset			= 0;
	line.m_nSignificantChars		= 5;
	line.m_tmMaintenance			= COleDateTime (2003, 1, 1, 12,	0, 0);

	line.m_strAuxBoxIp				= _T ("10.1.2.50");
	line.m_bAuxBoxEnabled			= false;

	line.m_strSerialParams1			= _T ("9600,8,N,1,0");
	line.m_strSerialParams2			= _T ("9600,8,N,1,0");
	line.m_strSerialParams3			= _T ("9600,8,N,1,0");

	line.m_nAMS32_A1				= 3;
	line.m_nAMS32_A2				= 1000;
	line.m_nAMS32_A3				= 10;
	line.m_nAMS32_A4				= 1000;
	line.m_nAMS32_A5				= 2500;
	line.m_nAMS32_A6				= 3;
	line.m_nAMS32_A7				= 20;

	line.m_nAMS256_A1				= 3;
	line.m_nAMS256_A2				= 1000;
	line.m_nAMS256_A3				= 28;
	line.m_nAMS256_A4				= 1000;
	line.m_nAMS256_A5				= 5500;
	line.m_nAMS256_A6				= 2;
	line.m_nAMS256_A7				= 100;
	line.m_dAMS_Interval			= 4;

	line.m_lPrinterID				= m_lPrinterID;


	CLineDlg dlg (m_db, line, this);
	bool bMore = true;

	dlg.m_strSerialDownload = _T ("COM1");

	while (bMore) {
		if (dlg.DoModal () == IDOK) {
			bool bUnique = true;

			line = dlg.m_line;

			for (int i = 0; i < vLines.GetSize (); i++) 
				if (!vLines [i].m_strName.CompareNoCase (line.m_strName))
					bUnique = false;

			if (bUnique) {
				VERIFY (AddLineRecord (m_db, line)); 
				UpdateSettingsTable (line, dlg);

				CLineItem * p = new CLineItem (line);

				m_lv.InsertCtrlData (p);
				int nIndex = m_lv.GetDataIndex (p);
				m_lv->SetItemState (nIndex, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);	

				UpdateUI ();
				
				bMore = false;
			}
			else {
				CString str;

				str.Format (LoadString (IDS_LINENAMENOTEUNIQUE), line.m_strName);
				MsgBox (* this, str, NULL, MB_ICONINFORMATION);
			}
		}
		else
			bMore = false;
	}
}

void CLineConfigDlg::OnDelete() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		int nResult = MsgBox (* this, LoadString (IDS_CONFIRMDELETE), LoadString (IDS_CONFIRM), MB_YESNO | MB_ICONQUESTION);

		if (nResult == IDYES) {
			for (int i = 0; i < sel.GetSize (); i++) {
				CLineItem * p = (CLineItem *)m_lv.GetCtrlData (sel [i]);
				CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

				ASSERT (p);
				GetLineHeads (m_db, p->m_lID, vHeads);
				
				if (vHeads.GetSize ()) {
					CString str;

					str.Format (LoadString (IDS_LINEHASHEADS), p->m_strName);
					MsgBox (* this, str, NULL, MB_ICONINFORMATION);
					break;
				}

				VERIFY (DeleteLineRecord (m_db, p->m_lID));
			}

			FillLineCtrl ();
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

bool CLineConfigDlg::LineNamesMatch (FoxjetDatabase::COdbcDatabase & db) // sw0867
{
	CArray <PRINTERSTRUCT, PRINTERSTRUCT &> vPrinters;

	GetPrinterRecords (db, vPrinters);

	for (int nPrinter = 1; nPrinter < vPrinters.GetSize (); nPrinter++) {
		CArray <LINESTRUCT, LINESTRUCT &> vFirst;
		CArray <LINESTRUCT, LINESTRUCT &> vCurrent;

		GetPrinterLines (db, vPrinters [0].m_lID, vFirst);
		GetPrinterLines (db, vPrinters [nPrinter].m_lID, vCurrent);

		if (vFirst.GetSize () != vCurrent.GetSize ())
			return false;

		for (int nLine = 0; nLine < vFirst.GetSize (); nLine++) 
			if (vFirst [nLine].m_strName.CompareNoCase (vCurrent [nLine].m_strName) != 0)
				return false;
	}

	return true;
}

void CLineConfigDlg::OnEdit() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		CLineItem * p = (CLineItem *)m_lv.GetCtrlData (sel [0]);
		CArray <LINESTRUCT, LINESTRUCT &> vLines;

		GetPrinterLines (m_db, m_lPrinterID, vLines); //GetLineRecords (m_db, vLines);
		ASSERT (p);

		LINESTRUCT line = * p;
		bool bMore = true;

		while (bMore) {
			SETTINGSSTRUCT s;
			CLineDlg dlg (m_db, line, this);

			{
				s.m_strData = _T ("COM1");
				GetSettingsRecord (m_db, line.m_lID, FoxjetDatabase::Globals::m_lpszSerialDownload, s);
				dlg.m_strSerialDownload = s.m_strData;
			}

			{
				s.m_strData = _T ("0");
				GetSettingsRecord (m_db, line.m_lID, FoxjetDatabase::Globals::m_lpszResetCounts, s);
				dlg.m_bResetCountsOnRestart = _ttoi (s.m_strData);
			}

			{
				s.m_strData = _T ("0");
				GetSettingsRecord (m_db, line.m_lID, FoxjetDatabase::Globals::m_lpszRequireLogin, s);
				dlg.m_bRequireLogin = _ttoi (s.m_strData);
			}

			if (dlg.DoModal () == IDOK) {
				line = dlg.m_line;

				bool bUnique = true;

				for (int i = 0; i < vLines.GetSize (); i++) {
					LINESTRUCT l = vLines [i];

					if (l.m_lID != line.m_lID && !l.m_strName.CompareNoCase (line.m_strName))
						bUnique = false;
				}

				if (bUnique) {
					VERIFY (UpdateLineRecord (m_db, line)); 
					UpdateSettingsTable (line, dlg);

					* p = line;
					m_lv.UpdateCtrlData (p);
					bMore = false;
				}
				else {
					CString str;

					str.Format (LoadString (IDS_LINENAMENOTEUNIQUE), line.m_strName);
					MsgBox (* this, str, NULL, MB_ICONINFORMATION);
				}
			}
			else
				bMore = false;
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

BOOL CLineConfigDlg::OnInitDialog() 
{
	using ListCtrlImp::CColumn;

	SETTINGSSTRUCT s;
	CArray <CColumn, CColumn> vCols;
	const CString strApp = GetAppTitle ();
	const CString strRegSection = _T ("Software\\FoxJet\\") + strApp + _T ("\\Defaults");

	vCols.Add (CColumn (LoadString (IDS_NAME), 200));
	vCols.Add (CColumn (LoadString (IDS_DESC), 200));

	m_lv.Create (LV_LINE, vCols, this, strRegSection);
	FillLineCtrl ();	

#ifdef __WINPRINTER__
	GetSettingsRecord (m_db, FoxjetDatabase::Globals::m_lGlobalID, FoxjetDatabase::Globals::m_lpszCoupled, s);
	m_bCoupled = _ttoi (s.m_strData) ? true : false;
#else
	GetSettingsRecord (m_db, FoxjetDatabase::Globals::m_lGlobalID, FoxjetDatabase::Globals::m_lpszLean, s);
	m_bLean = _ttoi (s.m_strData) ? true : false;
#endif 

	
	FoxjetCommon::CEliteDlg::OnInitDialog();

	if (IsControl (true) && IsNetworked ()) {
		UINT n [] = 
		{
			BTN_ADD,
			BTN_DELETE,
		};

		for (int i = 0; i < ARRAYSIZE (n); i++) 
			if (CWnd * p = GetDlgItem (n [i]))
				p->EnableWindow (FALSE);
	}

	return false;
}

void CLineConfigDlg::FillLineCtrl ()
{
	CArray <LINESTRUCT, LINESTRUCT &> v;

	m_lv.DeleteCtrlData ();
	GetPrinterLines (m_db, m_lPrinterID, v); //GetLineRecords (m_db, v);

	for (int i = 0; i < v.GetSize (); i++) 
		m_lv.InsertCtrlData (new CLineItem (v [i]));

	UpdateUI ();
}

void CLineConfigDlg::UpdateUI ()
{
	CButton * pCoupled = (CButton *)GetDlgItem (CHK_COUPLED);
	CButton * pLean = (CButton *)GetDlgItem (CHK_IPLEAN);

	ASSERT (pCoupled);
	ASSERT (pLean);

#ifdef __WINPRINTER__
	pCoupled->EnableWindow (m_lv->GetItemCount () == 2);
	pLean->ShowWindow (SW_HIDE);
#else
	pCoupled->ShowWindow (SW_HIDE);
	pLean->ShowWindow (SW_SHOW);
#endif
}

void CLineConfigDlg::OnDblclkLine(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();	
	*pResult = 0;
}

void CLineConfigDlg::OnOK ()
{
	SETTINGSSTRUCT s;

	s.m_lLineID = FoxjetDatabase::Globals::m_lGlobalID;
	FoxjetCommon::CEliteDlg::OnOK ();

#ifdef __WINPRINTER__
	s.m_strKey	= FoxjetDatabase::Globals::m_lpszCoupled;
	s.m_strData.Format (_T ("%d"), m_bCoupled);
#else
	s.m_strKey	= FoxjetDatabase::Globals::m_lpszLean;
	s.m_strData.Format (_T ("%d"), m_bLean);
#endif 

	if (!UpdateSettingsRecord (m_db, s)) 
		VERIFY (AddSettingsRecord (m_db, s));
}

