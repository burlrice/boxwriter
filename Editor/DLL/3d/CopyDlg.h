#if !defined(AFX_COPYDLG_H__333DC90E_FCD2_42F1_85B4_B5302C82CE22__INCLUDED_)
#define AFX_COPYDLG_H__333DC90E_FCD2_42F1_85B4_B5302C82CE22__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CopyDlg.h : header file
//

#include "3dApi.h"
#include "Utils.h"

namespace Foxjet3d
{
	/////////////////////////////////////////////////////////////////////////////
	// CCopyDlg dialog

	class _3D_API CCopyDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CCopyDlg(CWnd* pParent = NULL);   // standard constructor
		CCopyDlg(UINT nIDTemplate, CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CCopyDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA


	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CCopyDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:

		// Generated message map functions
		//{{AFX_MSG(CCopyDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COPYDLG_H__333DC90E_FCD2_42F1_85B4_B5302C82CE22__INCLUDED_)
