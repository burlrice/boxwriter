#if !defined(AFX_PRINTREPORTDLG_H__64D33E0B_11C1_4E43_BB31_D060C63A89D3__INCLUDED_)
#define AFX_PRINTREPORTDLG_H__64D33E0B_11C1_4E43_BB31_D060C63A89D3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrintReportDlg.h : header file
//

#include "ListCtrlImp.h"
#include "RoundButtons.h"
#include "3dApi.h"
#include "Database.h"
#include "Utils.h"

namespace Foxjet3d
{
	bool _3D_API IsPrintReportLoggingEnabled (FoxjetDatabase::COdbcDatabase & db, ULONG lPrinterID);

	/////////////////////////////////////////////////////////////////////////////
	// CPrintReportDlg dialog

	namespace PrintReportDlg
	{
		class _3D_API CReportItem : public ItiLibrary::ListCtrlImp::CItem
		{
		public:
			CReportItem (const FoxjetDatabase::REPORTSTRUCT & s, const CStringArray & vUserCols);
			virtual ~CReportItem ();

			virtual CString GetDispText (int nColumn) const;
			virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;

		protected:
			FoxjetDatabase::REPORTSTRUCT	m_rpt;
			std::vector<std::wstring>		m_vUserCols;
		};

		class _3D_API CPrintReportDlg : public FoxjetCommon::CEliteDlg
		{
		public:

			static bool IsAutoExport (FoxjetDatabase::COdbcDatabase & db, ULONG lPrinterID);
			static void Export (FoxjetDatabase::COdbcDatabase & db, const CString & strLine, ULONG lPrinterID);

			static const LPCTSTR m_lpszSettingsKey;
			static const LPCTSTR m_lpszSettingsKeyPath;

		// Construction
		public:
			CPrintReportDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent = NULL);   // standard constructor

			enum { RESTART = -2 };

		// Dialog Data
			//{{AFX_DATA(CPrintReportDlg)
			CString	m_sLine;
			//}}AFX_DATA
			ULONG m_lPrinterID;
			bool m_bEnable;
			
		// Overrides
			// ClassWizard generated virtual function overrides
			//{{AFX_VIRTUAL(CPrintReportDlg)
			public:
			protected:
			virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
			//}}AFX_VIRTUAL

		// Implementation
		protected:
			FoxjetUtils::CRoundButtons m_buttons;
			ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
			FoxjetDatabase::COdbcDatabase & m_db;

			virtual void OnOK ();
			void InitListView();


			// Generated message map functions
			//{{AFX_MSG(CPrintReportDlg)
			virtual BOOL OnInitDialog();
			afx_msg void OnClear();
			afx_msg void OnSelchangeLine();
			afx_msg void OnExport();
			afx_msg void OnFileSettings();
			//}}AFX_MSG
			DECLARE_MESSAGE_MAP()
			void ReSizeColumns();
		};
	}; //namespace PrintReportDlg
}; 

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTREPORTDLG_H__64D33E0B_11C1_4E43_BB31_D060C63A89D3__INCLUDED_)
