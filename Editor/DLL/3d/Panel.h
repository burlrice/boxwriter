// Panel.h: interface for the CPanel class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PANEL_H__C6AA50B8_CD65_494B_AAFA_A1F7AD3719D7__INCLUDED_)
#define AFX_PANEL_H__C6AA50B8_CD65_494B_AAFA_A1F7AD3719D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Database.h"
#include "TemplExt.h"
#include "3dApi.h"
#include "DxView.h"
#include "Image.h"
#include "DxView.h"
#include "CopyArray.h"
#include "BoxForwardDeclare.h"

namespace Foxjet3d
{
	typedef enum { FRONT = 0, RIGHT, BACK, LEFT, TOP, BOTTOM } FACE;

	namespace DB = FoxjetDatabase;

	namespace Panel
	{
		typedef enum 
		{
			CREATEIMAGE_RESIZE			= 0x01,
			CREATEIMAGE_DRAWELEMENTS	= 0x02,
		} CREATEIMAGETYPE;

		class _3D_API CBoundary 
		{
		public:
			CBoundary (DB::HEADSTRUCT & card, CArray <DB::VALVESTRUCT, DB::VALVESTRUCT &> & vValve, ULONG lID, ULONG lPanelID);
			CBoundary (DB::HEADSTRUCT & card, ULONG lID, ULONG lPanelID);
			CBoundary ();
			CBoundary (const CBoundary & rhs);
			CBoundary & operator = (const CBoundary & rhs);
			virtual ~CBoundary ();

			ULONG GetID () const { return m_lID; }
			ULONG GetPanelID () const { return m_lPanelID; }
			ULONG GetY () const;
			ULONG GetCY () const { return m_cy; }
			int Height () const;
			CString GetName () const;

			operator DB::HEADSTRUCT & () { return m_card; }
			operator const DB::HEADSTRUCT & () const { return m_card; }

			DB::HEADSTRUCT m_card;
			ULONG m_lLinkedTo;

		protected:
			ULONG m_y;
			ULONG m_cy;
			ULONG m_lID;
			ULONG m_lPanelID;
		};

		typedef CArray <CBoundary, CBoundary &> CBoundaryArray;

		class _3D_API CPanel : protected FoxjetDatabase::PANELSTRUCT  
		{
		public:
			CPanel (Box::CBox & box, FACE face, const FoxjetDatabase::PANELSTRUCT & rhs);
			CPanel (const CPanel & rhs);
			CPanel & operator = (const CPanel & rhs);
			virtual ~CPanel();

			CString GetTrace () const;

		public:
			DECLARE_CONSTBASEACCESS (FoxjetDatabase::PANELSTRUCT)
			bool Create (DxParams::CDxView & view);
	//		CSize Render (CTexture & mem, CBoxParams & params, FoxjetDatabase::ORIENTATION orient, ULONG lPanelID, bool bMsgs);
			CSize GetSize (const CBoundary & head, int nOrient = -1) const;
			void Free ();
			bool IsValid () const;
			void Invalidate ();
			bool ContainsHead (ULONG lHeadID) const;
			bool GetHead (ULONG lHeadID, CBoundary & head) const;
			FoxjetCommon::CLongArray GetHeads () const;
			void GetHeads (const Box::CBox & box, CBoundaryArray & vHeads) const;
			bool LoadImage (DxParams::CDxView & view, FoxjetDatabase::COdbcDatabase & db);
			bool LoadImage (CDC * pDC, FoxjetDatabase::COdbcDatabase & db);

			bool HasElements (Box::CBoxParams & p) const;
			bool HasImage (Box::CBoxParams & p) const;

			static FoxjetCommon::CLongArray GetMessageIndicies (const Box::CBoxParams & params, ULONG lPanelID);
			static void GetHeads (const Box::CBox & params, ULONG lPanelID, CBoundaryArray & vHeads);
			static CSize GetSize(const CBoundary & head, FoxjetDatabase::ORIENTATION orient, 
				const FoxjetDatabase::BOXSTRUCT & box, const FoxjetDatabase::PANELSTRUCT & panel);

		protected:

			CBitmap * m_pBmp;
			bool m_bValid;

		public:
			CBitmap * CreateImage (CDC & dc, Box::CBoxParams & p, const Box::CBox & box, FoxjetDatabase::ORIENTATION orient, 
				DWORD dwOptions = CREATEIMAGE_RESIZE | CREATEIMAGE_DRAWELEMENTS,
				CPanel * pAbsolute = NULL, FoxjetDatabase::ORIENTATION rotAbs = FoxjetDatabase::ROT_0) const;

			FACE			m_face;
			Box::CBox &		m_box;
			CImage			m_img;
			CBitmap *		m_pbmpImage;
			CBoundaryArray	m_vBounds;

		protected:

		};
	}; //namespace Panel

	inline _3D_API bool operator == (const Panel::CBoundary & lhs, const Panel::CBoundary & rhs) { return lhs.GetID () == rhs.GetID (); }
	inline _3D_API bool operator != (const Panel::CBoundary & lhs, const Panel::CBoundary & rhs) { return !(lhs == rhs); }

}; //namespace Foxjet3d

#endif // !defined(AFX_PANEL_H__C6AA50B8_CD65_494B_AAFA_A1F7AD3719D7__INCLUDED_)
