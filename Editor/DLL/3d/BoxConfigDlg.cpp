// BoxConfigDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "BoxConfigDlg.h"
#include "resource.h"
#include "Coord.h"
#include "BoxDimDlg.h"
#include "ItiLibrary.h"
#include "FieldDefs.h"
#include "Debug.h"
#include "OdbcRecordset.h"
#include "FileExt.h"
#include "Extern.h"

using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace Foxjet3d;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CBoxConfigDlg::CItem::CItem ()
{
}

CBoxConfigDlg::CItem::CItem (const FoxjetDatabase::BOXSTRUCT & rhs, UNITS units) 
:	m_box (rhs),
	m_units (units)
{
}

CBoxConfigDlg::CItem::~CItem ()
{
}

int CBoxConfigDlg::CItem::Compare (const ItiLibrary::ListCtrlImp::CItem & cmp, int nColumn) const
{
	const CItem & rhs = (CItem &)cmp;

	switch (nColumn) {
	case 0:		return m_box.m_strName.Compare (rhs.m_box.m_strName);
	case 1:		return m_box.m_lLength - rhs.m_box.m_lLength;
	case 2:		return m_box.m_lWidth - rhs.m_box.m_lWidth;
	case 3:		return m_box.m_lHeight - rhs.m_box.m_lHeight;
	}

	return 0;
}

CString CBoxConfigDlg::CItem::GetDispText (int nColumn) const
{
	const FoxjetDatabase::HEADSTRUCT & head = GetDefaultHead ();
	CString str;

	switch (nColumn) {
	case 0:		
		str = m_box.m_strName;
		break;
	case 1:		
		{
			CCoord c (CBaseElement::ThousandthsToLogical (
				CPoint (m_box.m_lLength, 0), head), m_units, head);
			str = c.FormatX ();
			break;
		}
	case 2:
		{
			CCoord c (CBaseElement::ThousandthsToLogical (
				CPoint (m_box.m_lWidth, 0), head), m_units, head);
			str = c.FormatX ();
			break;
		}
	case 3:
		{
			CCoord c (CBaseElement::ThousandthsToLogical (
				CPoint (m_box.m_lHeight, 0), head), m_units, head);
			str = c.FormatX ();
			break;
		}
	}

	return str;
}
////////////////////////////////////////////////////////////////////////////
// CBoxConfigDlg dialog


CBoxConfigDlg::CBoxConfigDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent /*=NULL*/)
:	m_units (INCHES),
	m_db (db),
	m_bReadOnly (false),
	FoxjetCommon::CEliteDlg(IDD_BOX, pParent)
{
	//{{AFX_DATA_INIT(CBoxConfigDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CBoxConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBoxConfigDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBoxConfigDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CBoxConfigDlg)
	ON_BN_CLICKED(BTN_ADD, OnAdd)
	ON_BN_CLICKED(BTN_DELETE, OnDelete)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_NOTIFY(NM_DBLCLK, LV_BOXES, OnDblclkBoxes)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (IDHELP, OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBoxConfigDlg message handlers

void CBoxConfigDlg::OnHelp ()
{
	CEliteDlg::OnHelp (this);
}

void CBoxConfigDlg::OnAdd() 
{
	CBoxDimDlg dlg (m_db, this);
	CWinApp * pApp = ::AfxGetApp ();

	dlg.m_units = m_units;
	dlg.m_bEdit = false;

	if (AddBoxRecord (m_db, dlg.m_box)) {
		if (pApp) 
			dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CBoxDimDlg"), _T ("Preview"), 0) ? true : false;
		
		if (dlg.DoModal () == IDOK) { // dlg updates box & images recs automatically
			m_lviBox.InsertCtrlData (new CItem (dlg.m_box, m_units));
			m_vIDUpdate.Add (dlg.m_box.m_lID);
		}
		else
			DeleteBoxRecord (m_db, dlg.m_box.m_lID);

		if (pApp) 
			pApp->WriteProfileInt (_T ("Settings\\CBoxDimDlg"), _T ("Preview"), dlg.m_bPreview);
	}
	else { ASSERT (0); }
}

void CBoxConfigDlg::OnDelete() 
{
	using namespace FoxjetDatabase;

	CLongArray sel = ItiLibrary::GetSelectedItems (m_lviBox.GetListCtrl ());

	if (sel.GetSize ()) {
		int nResult = MsgBox (* this, LoadString (IDS_CONFIRMELEMENTDELETE), 
			LoadString (IDS_CONFIRM), MB_YESNO | MB_ICONQUESTION);

		if (nResult == IDYES) {
			for (int i = sel.GetSize () - 1; i >= 0; i--) {
				CItem * p = (CItem *)m_lviBox.GetCtrlData (sel [i]);
				ULONG lBoxID = p->m_box.m_lID;
				CArray <BOXUSAGESTRUCT, BOXUSAGESTRUCT &> vInUse;

				// make sure this box is not in use!
				GetBoxUsage (m_db, lBoxID, -1, vInUse);

				if (vInUse.GetSize ()) {
					CString str = LoadString (IDS_BOXUSEDBYTASK);
					int nMax = 10;

					for (int i = 0; i < min (vInUse.GetSize (), nMax); i++) 
						str += 
							vInUse [i].m_line.m_strName + 
							_T ("\\") + 
							vInUse [i].m_task.m_strName + 
							_T (".") + 
							FoxjetFile::GetFileExt (FoxjetFile::TASK) +
							_T ("\n");

					if (vInUse.GetSize () > nMax)
						str += _T (".\n.\n.");

					MsgBox (* this, str);
					return;
				}

				if (DeleteBoxRecord (m_db, lBoxID)) 
					m_lviBox.DeleteCtrlData (sel [i]);
				else { ASSERT (0); }
			}
		}
	}
	else 
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CBoxConfigDlg::OnEdit() 
{
	CLongArray sel = ItiLibrary::GetSelectedItems (m_lviBox.GetListCtrl ());

	if (sel.GetSize ()) {
		CBoxDimDlg dlg (m_db, this);
		CItem * p = (CItem *)m_lviBox.GetCtrlData (sel [0]);
		CWinApp * pApp = ::AfxGetApp ();

		ASSERT (p);

		dlg.m_units = m_units;
		dlg.m_box = p->m_box;
		dlg.m_bEdit = true;
	
		if (pApp) 
			dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CBoxDimDlg"), _T ("Preview"), 0) ? true : false;

		// dlg updates box & images recs automatically
		if (dlg.DoModal () == IDOK) {
			p->m_box = dlg.m_box;
			m_lviBox.UpdateCtrlData (p);
			m_vIDUpdate.Add (dlg.m_box.m_lID);
		}

		if (pApp) 
			pApp->WriteProfileInt (_T ("Settings\\CBoxDimDlg"), _T ("Preview"), dlg.m_bPreview);
	}
	else 
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

BOOL CBoxConfigDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();

	BEGIN_TRANS (m_db);
	InitBoxCtrl ();
	ASSERT (GetDlgItem (IDOK));
	GetDlgItem (IDOK)->EnableWindow (!m_bReadOnly);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CBoxConfigDlg::InitBoxCtrl()
{
	using namespace ItiLibrary;

	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;
	CArray <BOXSTRUCT, BOXSTRUCT &> vBoxes;

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_NAME)));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_LENGTH), 50));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_WIDTH), 50));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_HEIGHT), 50));

	m_lviBox.Create (LV_BOXES, vCols, this, defElements.m_strRegSection);	
	GetBoxRecords (m_db, -1, vBoxes);

	for (int i = 0; i < vBoxes.GetSize (); i++) {
		CItem * p = new CItem (vBoxes [i], m_units);
		m_lviBox.InsertCtrlData (p, i);
	}
}

void CBoxConfigDlg::OnDblclkBoxes(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();

	if (pResult)
		* pResult = 0;
}

void CBoxConfigDlg::OnOK()
{
	COMMIT_TRANS (m_db);
	FoxjetCommon::CEliteDlg::OnOK ();
}

void CBoxConfigDlg::OnCancel()
{
	ROLLBACK_TRANS (m_db);
	FoxjetCommon::CEliteDlg::OnCancel ();
}

