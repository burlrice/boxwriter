// ProdArea.cpp: implementation of the CProductArea class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ProdArea.h"

#include "List.h"
#include "coord.h"
#include "Debug.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProductArea::CProductArea()
:	CRect (30, 30, 60, 60)
{
}


CProductArea::CProductArea (const CSize & rhs)
:	CRect (CPoint (0, 0), rhs)
{
}

CProductArea::CProductArea (const CRect & rhs)
:	CRect (rhs)
{
}

CProductArea::CProductArea (const CProductArea & rhs)
:	CRect (rhs)
{
}

CProductArea & CProductArea::operator = (const CProductArea & rhs)
{
	CRect::operator = (rhs);
	return * this;
}

CProductArea::~CProductArea()
{

}

CRect CProductArea::ClientToProd(const CRect &rc, double dZoom) const
{
	return (rc * dZoom) + TopLeft ();
}

CPoint CProductArea::ClientToProd(const CPoint &pt, double dZoom) const
{
	return (pt * dZoom) + TopLeft ();
}

CRect CProductArea::ProdToClient(const CRect &rc, double dZoom) const
{
	return (rc / dZoom) - TopLeft ();
}

CPoint CProductArea::ProdToClient(const CPoint &pt, double dZoom) const
{
	return (pt / dZoom) - TopLeft ();
}

