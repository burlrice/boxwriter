// Image.h: interface for the CImage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IMAGE_H__5094F423_923E_4C36_87CA_8A3C7115F5B2__INCLUDED_)
#define AFX_IMAGE_H__5094F423_923E_4C36_87CA_8A3C7115F5B2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Database.h"
#include "3dApi.h"
#include "TemplExt.h"

namespace Foxjet3d
{
	class _3D_API CImage : protected FoxjetDatabase::IMAGESTRUCT  
	{
	public:
		CImage();
		CImage(const FoxjetDatabase::IMAGESTRUCT & rhs);
		CImage (const CImage & rhs);
		CImage & operator = (const CImage & rhs);
		virtual ~CImage();

		DECLARE_CONSTBASEACCESS (FoxjetDatabase::IMAGESTRUCT)

		bool Load (const CString & strFile, UINT nPanel, ULONG lBoxID, CDC * pDC = NULL, FoxjetDatabase::ORIENTATION orient = FoxjetDatabase::ROT_0);
		void Free ();
		CSize GetSize () const;

		CBitmap * m_pBmp;
	};
}; //namespace Foxjet3d

#endif // !defined(AFX_IMAGE_H__5094F423_923E_4C36_87CA_8A3C7115F5B2__INCLUDED_)
