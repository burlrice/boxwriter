// Message.cpp: implementation of the CMessage class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Message.h"
#include "Color.h"
#include "Debug.h"

using namespace Foxjet3d;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

_3D_API bool Foxjet3d::operator == (const Foxjet3d::CMessage & lhs, const Foxjet3d::CMessage & rhs)
{
	return lhs.m_list == rhs.m_list;
}

_3D_API bool Foxjet3d::operator != (const Foxjet3d::CMessage & lhs, const Foxjet3d::CMessage & rhs)
{
	return !(lhs == rhs);
}

CMessage::CMessage()
{
	m_lID			= -1;
	m_lHeadID		= -1;
	m_lTaskID		= -1;
	m_lHeight		= 0;
	m_bIsUpdated	= false;

	m_list.FromString (_T (""), CProductArea (), INCHES, GetDefaultHead (), CVersion ());
}

CMessage::CMessage(const FoxjetDatabase::MESSAGESTRUCT & rhs)
{
	m_lID			= rhs.m_lID;
	m_lHeadID		= rhs.m_lHeadID;
	m_lTaskID		= rhs.m_lTaskID;
	m_strName		= rhs.m_strName;
	m_strDesc		= rhs.m_strDesc;
	m_strData		= rhs.m_strData;
	m_lHeight		= rhs.m_lHeight;
	m_bIsUpdated	= rhs.m_bIsUpdated;

	m_list.FromString (m_strData, CProductArea (), INCHES, m_list.m_bounds, CVersion ());
}

void CMessage::Assign (const FoxjetDatabase::MESSAGESTRUCT & rhs, const Panel::CBoundary & b)
{
	m_lID			= rhs.m_lID;
	m_lHeadID		= rhs.m_lHeadID;
	m_lTaskID		= rhs.m_lTaskID;
	m_strName		= rhs.m_strName;
	m_strDesc		= rhs.m_strDesc;
	m_strData		= rhs.m_strData;
	m_lHeight		= rhs.m_lHeight;
	m_bIsUpdated	= rhs.m_bIsUpdated;

	m_list.FromString (m_strData, CProductArea (), INCHES, m_list.m_bounds, CVersion ());

	VERIFY (m_list.SetHead (b));
	VERIFY (m_list.CElementList::SetHead (b));
	m_list.m_bounds = b;
}

CMessage::CMessage (const CMessage & rhs)
:	m_list (rhs.m_list)
{
	m_lID			= rhs.m_lID;
	m_lHeadID		= rhs.m_lHeadID;
	m_lTaskID		= rhs.m_lTaskID;
	m_strName		= rhs.m_strName;
	m_strDesc		= rhs.m_strDesc;
	m_strData		= rhs.m_strData;
	m_lHeight		= rhs.m_lHeight;
	m_bIsUpdated	= rhs.m_bIsUpdated;
}

CMessage & CMessage::operator = (const CMessage & rhs)
{
	if (this != &rhs) {
		m_list			= rhs.m_list;
		m_lID			= rhs.m_lID;
		m_lHeadID		= rhs.m_lHeadID;
		m_lTaskID		= rhs.m_lTaskID;
		m_strName		= rhs.m_strName;
		m_strDesc		= rhs.m_strDesc;
		m_strData		= rhs.m_strData;
		m_lHeight		= rhs.m_lHeight;
		m_bIsUpdated	= rhs.m_bIsUpdated;
	}

	return * this;
}

CMessage::~CMessage()
{
}

