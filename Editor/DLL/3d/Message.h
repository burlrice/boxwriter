// Message.h: interface for the CMessage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MESSAGE_H__143BE107_C60B_49C3_AD19_17DDC694B3DB__INCLUDED_)
#define AFX_MESSAGE_H__143BE107_C60B_49C3_AD19_17DDC694B3DB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Database.h"
#include "ElmntLst.h"
#include "3dApi.h"
#include "TemplExt.h"

namespace Foxjet3d
{
	class _3D_API CMessage : public FoxjetDatabase::MESSAGESTRUCT  
	{
	public:
		CMessage ();
		CMessage (const FoxjetDatabase::MESSAGESTRUCT & rhs);
		CMessage (const CMessage & rhs);
		CMessage & operator = (const CMessage & rhs);
		virtual ~CMessage();

		void Assign (const FoxjetDatabase::MESSAGESTRUCT & rhs, const Panel::CBoundary & b);

		DECLARE_CONSTBASEACCESS (FoxjetDatabase::MESSAGESTRUCT);

		CEditorElementList m_list;
	};

	_3D_API bool operator == (const CMessage & lhs, const CMessage & rhs);
	_3D_API bool operator != (const CMessage & lhs, const CMessage & rhs);
}; //namespace Foxjet3d

#endif // !defined(AFX_MESSAGE_H__143BE107_C60B_49C3_AD19_17DDC694B3DB__INCLUDED_)
