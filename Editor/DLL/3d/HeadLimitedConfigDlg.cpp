// HeadLimitedConfig.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "3d.h"
#include "HeadLimitedConfigDlg.h"
#include "TemplExt.h"
#include "Parse.h"
#include "Color.h"
#include "Coord.h"
#include "Extern.h"
#include "Registry.h"
#include "Debug.h"
#include "HeadDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace ListGlobals;
using namespace FoxjetUtils;

CHeadLimitedConfigDlg::MAPSTRUCT CHeadLimitedConfigDlg::m_map [8] =
{
	{ BTN_INKCODE1, LBL_HEAD1, TXT_HEAD1, 0, -1, },
	{ BTN_INKCODE2, LBL_HEAD2, TXT_HEAD2, 0, -1, },
	{ BTN_INKCODE3, LBL_HEAD3, TXT_HEAD3, 0, -1, },
	{ BTN_INKCODE4, LBL_HEAD4, TXT_HEAD4, 0, -1, },
	{ BTN_INKCODE5, LBL_HEAD5, TXT_HEAD5, 0, -1, },
	{ BTN_INKCODE6, LBL_HEAD6, TXT_HEAD6, 0, -1, },
	{ BTN_INKCODE7, LBL_HEAD7, TXT_HEAD7, 0, -1, },
	{ BTN_INKCODE8, LBL_HEAD8, TXT_HEAD8, 0, -1, },
};

/////////////////////////////////////////////////////////////////////////////
// CHeadLimitedConfigDlg dialog


CHeadLimitedConfigDlg::CHeadLimitedConfigDlg(FoxjetDatabase::COdbcDatabase & db, UNITS units, ULONG lLineID, CWnd* pParent)
:	m_db (db),
	m_nCurSelID (0),
	m_units (units),
	m_lIncrement (250),
	m_lpfct (NULL),
	m_lpfctInkCode (NULL),
	m_lLineID (lLineID),
	m_nInit (0),
	FoxjetCommon::CEliteDlg(IDD_HEAD_LIMITED_MATRIX, pParent)
{
	GetFirstHeadRecord (m_db, m_head);

	//{{AFX_DATA_INIT(CHeadLimitedConfigDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	
	m_lIncrement = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, defElements.m_strRegSection + _T ("\\CHeadLimitedConfigDlg"), _T ("m_lIncrement"), m_lIncrement);
}


void CHeadLimitedConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CCoord coordMin (0, 0, m_units, m_head);
	CCoord coordMax (0, 0, m_units, m_head);

	coordMin.m_x = coordMin.m_y = CBaseElement::LogicalToThousandths (CPoint (1, 0), m_head).x;
	coordMax.m_x = coordMax.m_y = 1000;

	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHeadLimitedConfigDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	if (m_nInit++ < 2) {
		m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
		m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
		m_buttons.DoDataExchange (pDX, BTN_APPLY);
		m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);
	}

	DDX_Coord (pDX, TXT_INCREMENT, m_lIncrement, m_units, m_head, true);
	DDV_Coord (pDX, TXT_INCREMENT, m_lIncrement, 0, m_units, m_head, DDVCOORDXBOTH, coordMin, coordMax);

	for (int i = 0; i < ARRAYSIZE (m_map); i++) 
		DDX_Coord (pDX, m_map [i].m_nTxtID, m_map [i].m_lPhotocellDelay, m_units, m_head, true);
}


BEGIN_MESSAGE_MAP(CHeadLimitedConfigDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CHeadLimitedConfigDlg)
	ON_WM_DRAWITEM()
	//}}AFX_MSG_MAP
	ON_WM_LBUTTONDOWN()
	ON_CBN_SELCHANGE (CB_LINE, OnSelChangeLine)
	ON_BN_CLICKED(BTN_APPLY, OnApply)
	ON_CONTROL_RANGE (BN_CLICKED, BTN_INKCODE1, BTN_INKCODE8, OnClickedInkCode)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHeadLimitedConfigDlg message handlers

void CHeadLimitedConfigDlg::OnLButtonDown (UINT nFlags, CPoint point)
{
	int nInvalidate [] = { m_nCurSelID, -1 };
	bool bHit = false;


	for (int i = 0; !bHit && i < ARRAYSIZE (m_map); i++) {
		UINT n [] = { m_map [i].m_nLblID, m_map [i].m_nTxtID, };

		for (int j = 0; !bHit && j < ARRAYSIZE (n); j++) {
			if (CWnd * p = GetDlgItem (n [j])) {
				if (p->IsWindowVisible ()) {
					CRect rc (0, 0, 0, 0);

					p->GetWindowRect (rc);
					ScreenToClient (rc);
					
					if (rc.PtInRect (point)) {
						nInvalidate [1] = m_nCurSelID = m_map [i].m_nTxtID;
						bHit = true;
					}
				}
			}
		}
	}

	/*
	for (i = 0; i < ARRAYSIZE (m_map); i++) {
		if (CStatic * p = (CStatic *)GetDlgItem (m_map [i].m_nLblID)) {
			if (m_map [i].m_nTxtID == m_nCurSelID) 
				p->ModifyStyleEx (0, SS_SUNKEN | WS_EX_STATICEDGE); 
			else 
				p->ModifyStyleEx (SS_SUNKEN | WS_EX_STATICEDGE, 0); 

			p->Invalidate ();
			p->RedrawWindow ();
		}
	}
	*/

	for (int i = 0; i < ARRAYSIZE (nInvalidate); i++) {
		if (CWnd * p = GetDlgItem (nInvalidate [i])) {
			p->Invalidate ();
			p->RedrawWindow ();
		}
	}

	if (CWnd * p = GetDlgItem (nInvalidate [1])) {
		if (!p->IsWindowVisible ()) {
			m_nCurSelID = m_map [0].m_nTxtID;

			if (CWnd * p = GetDlgItem (m_nCurSelID)) {
				p->Invalidate ();
				p->RedrawWindow ();
			}
		}
	}
}


BOOL CHeadLimitedConfigDlg::OnInitDialog() 
{
	m_nCurSelID = m_map [0].m_nTxtID;
	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	for (int i = 0; i < ARRAYSIZE (m_map); i++) {
		if (CWnd * p = GetDlgItem (m_map [i].m_nTxtID))
			p->ModifyStyle (0, SS_OWNERDRAW, 0);
	}
	
	if (CSpinButtonCtrl  * p = (CSpinButtonCtrl *)GetDlgItem (SPN_INCREMENT)) {
		CRect rc (0, 0, 0, 0);

		p->GetWindowRect (rc);
		//p->SetWindowPos (NULL, 0, 0, rc.Height () / 2, rc.Height (), SWP_NOZORDER | SWP_NOMOVE);
	}

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_LINE)) {
		CArray <LINESTRUCT, LINESTRUCT &> v;

		GetPrinterLines (m_db, GetPrinterID (m_db), v);

		for (int i = 0; i < v.GetSize (); i++) {
			p->SetItemData (p->AddString (v [i].m_strName), v [i].m_lID);
		}

		for (int i = 0; i < p->GetCount (); i++) {
			if (p->GetItemData (i) == m_lLineID) {
				p->SetCurSel (i);
				break;
			}
		}

		if (p->GetCurSel () == CB_ERR)
			p->SetCurSel (0);

		OnSelChangeLine ();
	}

	/* TODO: rem
	if (CWnd * p = GetDlgItem (m_nCurSelID)) {
		CRect rc (0, 0, 0, 0);

		OnLButtonDown (0, rc.CenterPoint ());
	}
	*/

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CHeadLimitedConfigDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	for (int i = 0; i < ARRAYSIZE (m_map); i++) {
		if (m_map [i].m_nLblID == (UINT)nIDCtl || m_map [i].m_nTxtID == (UINT)nIDCtl) {
			DRAWITEMSTRUCT & dis = * lpDrawItemStruct;
			CDC dc;
			CRect rc = dis.rcItem;
			CString str;

			GetDlgItemText (nIDCtl, str);

			dc.Attach (dis.hDC);
			dc.FillSolidRect (rc, m_nCurSelID == (UINT)nIDCtl ? Color::rgbWhite : Color::rgbLightGray);
			dc.DrawText (str, rc, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
			dc.Detach ();

			return;
		}
	}

	FoxjetCommon::CEliteDlg::OnDrawItem (nIDCtl, lpDrawItemStruct);
}

BOOL CHeadLimitedConfigDlg::OnNotify( WPARAM wParam, LPARAM lParam, LRESULT* pResult )
{
	if (wParam == SPN_INCREMENT) {
		NMUPDOWN * p = (NMUPDOWN *)lParam;

		if (UpdateData (TRUE)) {
			for (int i = 0; i < ARRAYSIZE (m_map); i++) {
				if (m_map [i].m_nTxtID == m_nCurSelID) {
					ULONG lDelay = m_map [i].m_lPhotocellDelay;

					if (p->iDelta > 0)
						lDelay -= m_lIncrement;
					else
						lDelay += m_lIncrement;

					if (lDelay >= CHeadDlg::m_lmtMphcPhotocellDelay.m_dwMin && lDelay <= CHeadDlg::m_lmtMphcPhotocellDelay.m_dwMax)
						m_map [i].m_lPhotocellDelay = lDelay;

					break;
				}
			}

			UpdateData (FALSE);

			if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (IDOK))
				p->EnableWindow (TRUE);
			if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_APPLY))
				p->EnableWindow (TRUE);
		}
	}

	return FoxjetCommon::CEliteDlg::OnNotify (wParam, lParam, pResult);
}

void CHeadLimitedConfigDlg::OnSelChangeLine()
{
	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_LINE)) {
		ULONG lLineID = p->GetItemData (p->GetCurSel ());
		CArray <HEADSTRUCT, HEADSTRUCT &> v;

		GetLineHeads (m_db, lLineID, v);

		for (int i = 0; i < v.GetSize (); i++) {
			if (CWnd * p = GetDlgItem (m_map [i].m_nLblID)) {
				p->ShowWindow (SW_SHOW);
				p->SetWindowText (v [i].m_strName);
			}
			if (CWnd * p = GetDlgItem (m_map [i].m_nTxtID)) {
				CString strKey = _T ("m_lPhotocellDelay [") + ToString (i) + _T ("]");

				p->ShowWindow (SW_SHOW);
				m_map [i].m_lHeadID = v [i].m_lID;
				m_map [i].m_lPhotocellDelay = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, 
					defElements.m_strRegSection + _T ("\\CHeadLimitedConfigDlg\\") + ToString (m_lLineID), 
					strKey, v [i].m_lPhotocellDelay);
			}
		}

		for (int i = v.GetSize (); i < ARRAYSIZE (m_map); i++) {
			if (CWnd * p = GetDlgItem (m_map [i].m_nBtnID))
				p->ShowWindow (SW_HIDE);
			if (CWnd * p = GetDlgItem (m_map [i].m_nLblID))
				p->ShowWindow (SW_HIDE);
			if (CWnd * p = GetDlgItem (m_map [i].m_nTxtID))
				p->ShowWindow (SW_HIDE);
		}

		if (!FoxjetDatabase::Encrypted::InkCodes::IsEnabled ()) {
			for (int i = 0; i < ARRAYSIZE (m_map); i++)
				if (CWnd * p = GetDlgItem (m_map [i].m_nBtnID))
					p->ShowWindow (SW_HIDE);
		}

		UpdateData (FALSE);

		if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (IDOK))
			p->EnableWindow (FALSE);
		if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_APPLY))
			p->EnableWindow (FALSE);
		if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (IDCANCEL))
			p->EnableWindow (TRUE);
	}
}

void CHeadLimitedConfigDlg::DoCallback()
{
	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, defElements.m_strRegSection + _T ("\\CHeadLimitedConfigDlg"), _T ("m_lIncrement"), m_lIncrement);

	for (int i = 0; i < ARRAYSIZE (m_map); i++) {
		CString strKey = _T ("m_lPhotocellDelay [") + ToString (i) + _T ("]");

		if (CWnd * p = GetDlgItem (m_map [i].m_nTxtID)) {
			if (p->IsWindowVisible ()) {
				FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, 
					defElements.m_strRegSection + _T ("\\CHeadLimitedConfigDlg\\") + ToString (m_lLineID), 
					strKey, m_map [i].m_lPhotocellDelay);

				if (m_lpfct) 
					(* m_lpfct) (m_map [i].m_lHeadID, m_map [i].m_lPhotocellDelay);
			}
		}
	}
}

void CHeadLimitedConfigDlg::OnOK()
{
	DoCallback ();
	FoxjetCommon::CEliteDlg::OnOK ();
}

void CHeadLimitedConfigDlg::OnApply()
{
	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_APPLY))
		p->EnableWindow (FALSE);
	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (IDCANCEL))
		p->EnableWindow (FALSE);

	DoCallback ();
}

void CHeadLimitedConfigDlg::OnClickedInkCode (UINT nID)
{
	if (m_lpfctInkCode) {
		for (int i = 0; i < ARRAYSIZE (m_map); i++) {
			if (m_map [i].m_nBtnID == nID) {
				(* m_lpfctInkCode) (m_map [i].m_lHeadID);
				break;
			}
		}
	}
}