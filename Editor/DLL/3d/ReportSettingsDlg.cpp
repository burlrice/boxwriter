// ReportSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "ReportSettingsDlg.h"
#include "Debug.h"
#include "Database.h"
#include "Parse.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;
using namespace Foxjet3d;

/////////////////////////////////////////////////////////////////////////////
// CReportSettingsDlg dialog


CReportSettingsDlg::CReportSettingsDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strKey, CWnd* pParent /*=NULL*/)
:	m_db (db),
	m_strKey (strKey),
	FoxjetCommon::CEliteDlg(IDD_REPORTSETTINGS_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CReportSettingsDlg)
	m_nDays = 0;
	m_bEnable = FALSE;
	//}}AFX_DATA_INIT
}


void CReportSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportSettingsDlg)
	DDX_Text(pDX, TXT_DAYS, m_nDays);
	DDV_MinMaxInt(pDX, m_nDays, 1, 365);
	DDX_Check(pDX, CHK_ENABLE, m_bEnable);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportSettingsDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CReportSettingsDlg)
	ON_BN_CLICKED(CHK_ENABLE, OnEnable)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CReportSettingsDlg message handlers

void CReportSettingsDlg::OnEnable() 
{
	bool bEnable = false;

	if (CButton * p = (CButton *)GetDlgItem (CHK_ENABLE))
		bEnable = p->GetCheck () == 1 ? true : false;

	if (CWnd * p = GetDlgItem (TXT_DAYS))
		p->EnableWindow (bEnable);
}

BOOL CReportSettingsDlg::OnInitDialog() 
{
	Load (m_db, m_strKey, m_bEnable, m_nDays);
	FoxjetCommon::CEliteDlg::OnInitDialog();
	OnEnable ();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CReportSettingsDlg::Load (FoxjetDatabase::COdbcDatabase & db, const CString & strKey, BOOL & bEnable, int & nDays)
{
	SETTINGSSTRUCT s;
	CStringArray v;
	int i = 0;

	GetSettingsRecord (db, 0, strKey + _T ("::Settings"), s);
	FoxjetDatabase::FromString (s.m_strData, v);

	bEnable	= _ttoi (GetParam (v, i++, _T ("0")));
	nDays	= _ttoi (GetParam (v, i++, _T ("30")));
}

void CReportSettingsDlg::OnOK()
{
	if (!UpdateData (TRUE))
		return;

	SETTINGSSTRUCT s;
	CStringArray v;

	v.Add (FoxjetDatabase::ToString (m_bEnable));
	v.Add (FoxjetDatabase::ToString (m_nDays));

	s.m_lLineID = 0;
	s.m_strKey = m_strKey + _T ("::Settings");
	s.m_strData = FoxjetDatabase::ToString (v);

	if (!UpdateSettingsRecord (m_db, s))
		VERIFY (AddSettingsRecord (m_db, s));

	FoxjetCommon::CEliteDlg::OnOK ();
}
