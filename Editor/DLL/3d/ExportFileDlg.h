#if !defined(AFX_EXPORTFILEDLG_H__440025A3_940C_4CA8_911C_FA0AA6C07B03__INCLUDED_)
#define AFX_EXPORTFILEDLG_H__440025A3_940C_4CA8_911C_FA0AA6C07B03__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExportFileDlg.h : header file
//

#include "ListCtrlImp.h"
#include "OpenDlg.h"
#include "3dApi.h"
#include "RoundButtons.h"

/////////////////////////////////////////////////////////////////////////////
// CExportFileDlg dialog

namespace Foxjet3d
{
	int _3D_API Export (CWnd * pParent);

	class _3D_API CExportFileDlg : public OpenDlg::COpenDlg // : public CBaseExportDlg
	{
	// Construction
	public:
		CExportFileDlg(CWnd* pParent = NULL);   // standard constructor

		CString m_strFilename;

		static const LPCTSTR m_lpszTxtExt;
		static const LPCTSTR m_lpszRegExt;

	// Dialog Data
		//{{AFX_DATA(CExportFileDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CExportFileDlg)
		public:
		virtual BOOL PreTranslateMessage(MSG* pMsg);
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

		virtual void OnOK ();
		virtual void OnCancel ();

	// Implementation
	protected:

		FoxjetUtils::CRoundButtons m_buttons;

		// Generated message map functions
		//{{AFX_MSG(CExportFileDlg)
		afx_msg void OnEditSelectall();
		virtual BOOL OnInitDialog();
		afx_msg void OnExport();
		afx_msg void OnBrowse();
		//}}AFX_MSG

		afx_msg void OnDblclkTasks(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnSelchangeLine ();

		bool IsCurSelRegistry ();

		HACCEL m_hAccel;

		DECLARE_MESSAGE_MAP()
	};
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXPORTFILEDLG_H__440025A3_940C_4CA8_911C_FA0AA6C07B03__INCLUDED_)
