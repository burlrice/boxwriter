// HpHeadDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "3d.h"
#include "Debug.h"
#include "HpHeadDlg.h"
#include "ComPropertiesDlg.h"
#include "TemplExt.h"
#include "Coord.h"
#include "HeadDlg.h"
#include "StdCommDlg.h"

using namespace FoxjetCommon;
using namespace Foxjet3d;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHpHeadDlg dialog

const FoxjetCommon::LIMITSTRUCT CHpHeadDlg::m_lmtPhotocellRate		= {	1,		144					}; // in inches
const FoxjetCommon::LIMITSTRUCT CHpHeadDlg::m_lmtPhotocellDelay		= {	500,	30000				}; // in 1000ths
const FoxjetCommon::LIMITSTRUCT CHpHeadDlg::m_lmtIntTachSpeed		= {	10,		300					}; // in ft/min

CHpHeadDlg::CHpHeadDlg(FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::HEADSTRUCT & head, 
			UNITS units, const CMapStringToString & vAvailAddressMap, CWnd* pParent)
:	m_head (head),
	m_bReadOnly (false),
	m_units (units),
	FoxjetCommon::CEliteDlg(IDD_HPHEAD, pParent)
{
	//{{AFX_DATA_INIT(CHpHeadDlg)
	//}}AFX_DATA_INIT
}


void CHpHeadDlg::DoDataExchange(CDataExchange* pDX)
{
	CCoord coordMin (0, 0, m_units, m_head);
	CCoord coordMax (0, 0, m_units, m_head);

	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	
	DDX_Text(pDX, TXT_NAME, m_head.m_strName);
	DDX_Text(pDX, TXT_PORT, m_head.m_strSerialParams);

	//{{AFX_DATA_MAP(CHpHeadDlg)
	//}}AFX_DATA_MAP

	DDX_Coord (pDX, TXT_RELATIVEHEIGHT, (long &)m_head.m_lRelativeHeight, m_units, m_head, false);
	coordMin.m_y = CHeadDlg::m_lmtRelativeHeight.m_dwMin;
	coordMax.m_y = CHeadDlg::m_lmtRelativeHeight.m_dwMax;
	DDV_Coord (pDX, TXT_RELATIVEHEIGHT, 0, m_head.m_lRelativeHeight, m_units, m_head, DDVCOORDYBOTH, coordMin, coordMax);

	{
		CCoord c (0, 0, HEAD_PIXELS, m_head);

		DDX_Coord (pDX, TXT_PHOTOCELLDELAY, (long &)m_head.m_lPhotocellDelay, m_units, m_head, true);

		coordMin.m_x = m_lmtPhotocellDelay.m_dwMin;
		coordMax.m_x = m_lmtPhotocellDelay.m_dwMax;
		DDV_Coord (pDX, TXT_PHOTOCELLDELAY, m_head.m_lPhotocellDelay, 0, m_units, m_head, DDVCOORDXBOTH, coordMin, coordMax);
	}

	{
		/*
		DDX_Text (pDX, TXT_INTTACHSPEED, m_head.m_nIntTachSpeed);
		DDV_MinMaxUInt (pDX, m_head.m_nIntTachSpeed, m_lmtIntTachSpeed.m_dwMin, m_lmtIntTachSpeed.m_dwMax);
		*/

		UNITS units = m_units;

		switch (units) {
		case INCHES:		units = FEET;		break;
		case CENTIMETERS:	units = METERS;		break;
		}

		// feet --> 1000ths
		long lSpeed = (long)(m_head.m_nIntTachSpeed * 12000.0);

		DDX_Coord (pDX, TXT_INTTACHSPEED, lSpeed, units, m_head, true);

		if (pDX->m_bSaveAndValidate) {
			CCoord c (0, 0, units, m_head);			// note: units may not be the same as m_units
			CCoord coordMin (0, 0, units, m_head);	// note: units may not be the same as m_units
			CCoord coordMax (0, 0, units, m_head);	// note: units may not be the same as m_units
			LIMITSTRUCT lmt = m_lmtIntTachSpeed;

			//if (IsValveHead (m_head)) 
			//	lmt.m_dwMax = CalcMaxLineSpeed (m_head);

			c.m_x = lSpeed;
			coordMin.m_x = lmt.m_dwMin * 12000.0;
			coordMax.m_x = lmt.m_dwMax * 12000.0;

			DDV_Coord (pDX, TXT_INTTACHSPEED, lSpeed, 0, units, m_head, DDVCOORDXBOTH, coordMin, coordMax);
			c.m_x /= 12000.0;
			m_head.m_nIntTachSpeed = (int)c.m_x;
		}
	}

	DDX_Radio (pDX, RDO_RTOL, m_head.m_nDirection);
	DDX_Radio (pDX, RDO_ENCODEREXT, m_head.m_nEncoder);
	DDX_Check (pDX, CHK_INVERT, m_head.m_bInverted);
}

int CHpHeadDlg::CalcMaxLineSpeed (const FoxjetDatabase::HEADSTRUCT & head)
{
	TRACEF ("***** TODO *****");

	int nDPI = (UINT)head.GetRes ();
	double dInchesPerSecond = 1000.0 / (double)nDPI; // 1 kHz max
	int nResult = (int)((dInchesPerSecond * 60.0) / 12.0);

	return BindTo (nResult, (int)CHeadDlg::m_lmtIntTachSpeed.m_dwMin, 620);
}

BEGIN_MESSAGE_MAP(CHpHeadDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CHpHeadDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (BTN_PORT, OnPortProperties)
	ON_BN_CLICKED (RDO_LTOR, OnDirection)
	ON_BN_CLICKED (RDO_RTOL, OnDirection)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHpHeadDlg message handlers

BOOL CHpHeadDlg::OnInitDialog() 
{	
	CUIntArray v;
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_PORT);
	
	m_head.m_strSerialParams.Replace (_T ("{"), _T (""));
	m_head.m_strSerialParams.Replace (_T ("}"), _T (""));

	m_bmpRTOL.LoadBitmap (IDB_RTOL);
	m_bmpLTOR.LoadBitmap (IDB_LTOR);

	ASSERT (pCB);
	FoxjetCommon::CEliteDlg::OnInitDialog();
	EnumerateSerialPorts (v);

	for (int i = 0; i < v.GetSize (); i++) {
		CString str;
		int nPort = v [i];

		str.Format (_T ("COM%d"), nPort);
		int nIndex = pCB->AddString (str);
		pCB->SetItemData (nIndex, nPort);

		if (!m_head.m_strUID.CompareNoCase (str))
			pCB->SetCurSel (nIndex);
	}

	if (pCB->GetCurSel () == CB_ERR)
		pCB->SetCurSel (0);
	
	UpdateUI ();

	m_head.m_nChannels = 600;
	m_head.m_dNozzleSpan = 0.500;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CHpHeadDlg::OnOK()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_PORT);
	
	ASSERT (pCB);
	FoxjetCommon::CEliteDlg::OnOK ();

	if (pCB->GetLBTextLen (pCB->GetCurSel ()) > 0)
		pCB->GetLBText (pCB->GetCurSel (), m_head.m_strUID);
}

void CHpHeadDlg::UpdateUI ()
{
	const UINT nMap [] = 
	{
		TXT_NAME,
		CB_PORT,
		IDOK,
	};

	for (int i = 0; i < ARRAYSIZE (nMap); i++)
		if (CWnd * p = GetDlgItem (nMap [i])) 
			p->EnableWindow (m_bReadOnly ? FALSE : TRUE);
}

void CHpHeadDlg::OnPortProperties ()
{
	CString strKey;
	CPropertySheet sheet;
	CComPropertiesDlg dlg;
	StdCommDlg::CCommItem tmp;

	tmp.FromString (m_head.m_strSerialParams);
	_stscanf (m_head.m_strUID, _T ("COM%d"), &tmp.m_dwPort);
	tmp.m_lLineID = 0;
	tmp.m_type = (eSerialDevice)0;

	dlg.m_bWin32			= true;
	dlg.m_bShowAssignment	= false;
	dlg.m_lBaud				= tmp.m_dwBaudRate;
	dlg.m_nDatabits			= tmp.m_nByteSize;
	dlg.m_cParity			= (char)StdCommDlg::CCommItem::ToString (tmp.m_parity).GetAt (0);
	dlg.m_nStopBits			= tmp.m_nStopBits;

	sheet.AddPage (&dlg);
	sheet.SetTitle (LoadString (IDS_SERIALSETTINGS));

	if (sheet.DoModal () == IDOK) {
		tmp.m_dwBaudRate	= dlg.m_lBaud;
		tmp.m_nByteSize		= dlg.m_nDatabits;
		tmp.m_parity		= StdCommDlg::CCommItem::GetParity (dlg.m_cParity);
		tmp.m_nStopBits		= dlg.m_nStopBits;

		m_head.m_strSerialParams = tmp.ToString ();
		m_head.m_strSerialParams.Replace (_T ("{"), _T (""));
		m_head.m_strSerialParams.Replace (_T ("}"), _T (""));
		TRACEF (m_head.m_strSerialParams);
		SetDlgItemText (TXT_PORT, m_head.m_strSerialParams);
	}
}

void CHpHeadDlg::OnDirection ()
{
	CButton * pRTOL = (CButton *)GetDlgItem (RDO_RTOL);
	CStatic * pBmp = (CStatic *)GetDlgItem (IDB_DIRECTION);

	ASSERT (pRTOL);
	ASSERT (pBmp);

	pBmp->SetBitmap (pRTOL->GetCheck () == 1 ? m_bmpRTOL : m_bmpLTOR);
}
