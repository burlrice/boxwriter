// Box.h: interface for the CBox class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BOX_H__298DD7F5_44A8_4E0F_81E9_9BBD0F2FB55C__INCLUDED_)
#define AFX_BOX_H__298DD7F5_44A8_4E0F_81E9_9BBD0F2FB55C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxmt.h>
#include "DxObject.h"
#include "Database.h"
#include "Task.h"
#include "CopyArray.h"
#include "3dApi.h"
#include "Panel.h"
#include "Size3.h"
#include "Point3.h"
#include "HeadConfigDlg.h"
#include "BoxForwardDeclare.h"

namespace Foxjet3d
{
	namespace DB = FoxjetDatabase;
	namespace CMN = FoxjetCommon;

	class _3D_API CTask;

	namespace Box
	{
		class _3D_API CBox;

		namespace BoxParams
		{
			typedef enum
			{ 
				SELECTEDPANEL	= (1 << (DxParams::ENUM_END + 1)), 
				SELECTEDHEAD	= (1 << (DxParams::ENUM_END + 2)), 
				OFFSET			= (1 << (DxParams::ENUM_END + 3)), 
				ROTATION		= (1 << (DxParams::ENUM_END + 4)), 
				BLEND			= (1 << (DxParams::ENUM_END + 5)), 
				SHOWHEADS		= (1 << (DxParams::ENUM_END + 6)), 
				SHOWDIRECTION	= (1 << (DxParams::ENUM_END + 7)), 
				TASK			= (1 << (DxParams::ENUM_END + 8)), 
				SHOWCROSSHAIRS	= (1 << (DxParams::ENUM_END + 9)), 
			};
		}; //namespace BoxParams

		typedef enum 
		{
			HT_PANEL			= 0x0001,
			HT_HEAD				= 0x0002,
			HT_ELEMENT			= 0x0004,
			HT_SETCROSSHAIRS	= 0x0008,
			HT_DEFAULT			= (HT_PANEL | HT_HEAD | HT_ELEMENT),
			HT_ALL				= 0xFFFF,
		} HITTYPE;


		class _3D_API CBoxParams : public DxParams::CDxParams
		{
			friend class _3D_API Foxjet3d::DxParams::CDxView;

			DECLARE_DYNAMIC (CBoxParams)
		public:
			CBoxParams (DxParams::CDxView & view, CTask & task);
			CBoxParams (const CBoxParams & rhs);
			CBoxParams & operator = (const CBoxParams & rhs);
			virtual ~CBoxParams ();

			void LoadSettings (CWinApp & app, const CString & strSection);
			void SaveSettings (CWinApp & app, const CString & strSection) const;

			inline bool GetHead (const CBox & box, const DB::HEADSTRUCT & head, Panel::CBoundary & b) const;
			inline bool GetHead (const CBox & box, ULONG lHeadID, Panel::CBoundary & b, bool bDefaultIfNotFound = false) const;
			inline bool GetActiveHead (const CBox & box, Panel::CBoundary & b) const;

			UINT					m_nPanel;
			CPoint					m_ptOffset;
			double					m_dRotation [2];
			UCHAR					m_uBlend;
			bool					m_bMessages;
			bool					m_bHeads;
			bool					m_bDirection;
			CTask &					m_task;
			ULONG					m_lChange;
			bool					m_bOutlineOnly;
			ULONG					m_lHeadID;
			ULONG					m_lValveID;
		
			struct 
			{
				POINT	m_pt;
				bool	m_bShow;
				SIZE	m_size;
			} m_crosshairs;
	
			friend _3D_API ULONG Compare (const CBoxParams & lhs, const CBoxParams & rhs);
		};

		class _3D_API CHit
		{ 
		public:
			CHit ();
			CHit (const CHit & rhs);
			CHit & operator = (const CHit & rhs);
			virtual ~CHit ();

			UINT						m_nPanel;
			CArray <ULONG, ULONG>		m_vHeadID;

			struct 
			{
				ULONG					m_lID;
				int						m_nType;
				Element::CElement *		m_pElement;
				CEditorElementList *	m_pList;
				POINT					m_ptTracker;
			} m_element;
		};

		typedef enum { LEFT = 0, RIGHT, UP, DOWN, CL, CCL } ROTATION;
		typedef enum { _FRONT, _BACK, _LEFT, _RIGHT, _TOP, _BOTTOM } PANEL;
		typedef enum { SOLID = 1, DASH = 2 } WIREFRAME;

		class _3D_API CBox : public DxParams::CDxObject
		{
			DECLARE_DYNAMIC (CBox);

			friend class _3D_API Foxjet3d::DxParams::CDxView;

		public:
			CBox (FoxjetDatabase::COdbcDatabase & db);
			CBox (const CBox & rhs);
			CBox & operator = (const CBox & rhs);	
			virtual ~CBox();

		public:
			void Reload ();
			CHit HitTest (const CPoint & pt, CBoxParams & params, DWORD dwType = HT_DEFAULT) const;

			CString ToString (const FoxjetCommon::CVersion & ver) const;
			bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);

			bool IsCreated () const;
			const DB::COrientation & GetTransform () const;
			UINT GetTransformFace () const;
			DB::ORIENTATION GetTransformOrientation () const;
			bool SetTransform(UINT nFace, DB::ORIENTATION orient);
			bool SetTransform (const DB::COrientation & transform);
			bool RotateTransform (UINT nFace, ROTATION r);
			void GetPoints (CBoxParams & params, CPoint pt [8]) const;
			DB::COrientation MapPanel (UINT nPanel) const;

			const DB::BOXSTRUCT & GetBox () const;
			bool SetBox (const DB::BOXSTRUCT & box);

			static bool GetImage (CArray <DB::IMAGESTRUCT, DB::IMAGESTRUCT &> & v, UINT nPanel, DB::IMAGESTRUCT & img);

			const DB::LINESTRUCT & GetLine () const;
			bool SetLine (const DB::LINESTRUCT & line);

			CSize CalcViewSize (CBoxParams & p) const;

			virtual void Render (DxParams::CDxParams * pParams);
			virtual bool Create (DxParams::CDxView & view);
			virtual void Free (bool bKeepSurfaces = false);
			void ClearHT () const;

			void Invalidate (UINT nPanel);			

			CRect Invalidate (CBoxParams & p, Element::CPairArray & vInvalidate, Element::CPairArray & vRedraw);
			CRect Erase (CBoxParams & p, Element::CPairArray & vInvalidate);
			Element::CPairArray GetRedraw (CBoxParams & p, Element::CPairArray & v);
			Foxjet3d::CSize3 GetSize (UINT nPanel, const Panel::CBoundary & head, int nOrient = 0) const;

			static void CalcPoints (CPoint pt [8], const CSize3 & ext, double dHorzRotation, double dVertRotation, const CPoint & ptOffset = CPoint (0, 0), double dZoom = 1.0);
			static REAL CalcAdjust (const CSize & viewSize, const CSize & surfaceSize);
			static CPoint GetLeftmost (const CPoint * pptSides, int nCount = 8);
			static CPoint GetRightmost (const CPoint * pptSides, int nCount = 8);
			static CPoint GetTopmost (const CPoint * pptSides, int nCount = 8);
			static CPoint GetBottommost (const CPoint * pptSides, int nCount = 8);

			bool HasPanels () const;
			const Foxjet3d::CImage * GetPanelImage (UINT nPanel) const;
			bool SetPanelImage (UINT nPanel, const DB::IMAGESTRUCT & img);
			bool DeletePanelImage (UINT nPanel);
			bool GetPanelImages (const DB::IMAGESTRUCT * pImages [6]) const;

			bool GetPanel (UINT nNumber, const Panel::CPanel ** pPanel) const;
			bool GetPanelByID (ULONG lID, const Panel::CPanel ** pPanel) const;
			bool GetPanels (const Panel::CPanel * pPanels [6]) const;

			bool GetPanel (UINT nNumber, const DB::PANELSTRUCT ** pPanel) const;
			bool GetPanelByID (ULONG lID, const DB::PANELSTRUCT ** pPanel) const;
			bool GetPanels (const DB::PANELSTRUCT * pPanels [6]) const;

			static int FindPanel (CArray <FoxjetDatabase::PANELSTRUCT, FoxjetDatabase::PANELSTRUCT &> & vPanels, ULONG lPanelID);
			Element::CPairArray GetElements (UINT nPanel, CBoxParams & p);

			DB::COdbcDatabase & GetDB () const { return m_db; }
			UINT GetPanelNumber (const Panel::CBoundary & head) const;
			inline CRect GetHeadRect (const Panel::CBoundary & head, CBoxParams & p, bool bClip = false) const;
			inline CRect GetPanelRect (CBoxParams & p) const;

			int LoadPanels (FoxjetDatabase::COdbcDatabase & db, ULONG lLineID);

			static CRect GetValveRect (Panel::CBoundary & head, const CRect & rcHead, ULONG lValveID);

			bool GetWorkingHead (CBoxParams & p, Panel::CBoundary & head) const;
			bool GetWorkingHead (Panel::CBoundaryArray & vHeads, Panel::CBoundary & head) const;
			bool GetWorkingHead (Panel::CBoundary & head) const;

			bool IsLinked (const Panel::CBoundary & b) const;
			bool GetLinkedSlave (const Panel::CBoundary & master, Panel::CBoundary & link) const;

			static const DB::COrientation m_orientCalc;
			static const int m_nHeadBoundary;

		protected:
			class _3D_API COrder
			{
			public:
				COrder ();
				COrder (const DB::COrientation & face, const CPoint pt [8]);
				COrder (int nAbsolute, const DB::COrientation & face, const CPoint pt [8]);
				COrder (const COrder & rhs);
				COrder & operator = (const COrder & rhs);
				virtual ~COrder ();

				int GetIndex () const { return m_nNumber - 1; }
				operator int () const { return GetIndex (); }

				bool GetHeadBounds (const CBox & box, CBoxParams & params, ULONG lHeadID, CPoint pt [4]) const; 

				int					m_nNumber;
				DB::ORIENTATION		m_rot;
				CPoint				m_pt [4];

			protected:
				void SetPoints (int nAbsolute, const CPoint pt [8]);
			};
			friend class COrder;

			class CCache
			{
			public:
				CCache ();
				virtual ~CCache ();

				inline CRect GetHeadRect (const Panel::CBoundary & head, const CBox & box, 
					CBoxParams & p, bool bClip = true);
				inline CRect GetPanelRect (const CBox & box, CBoxParams & p);

				inline void Build (const CBox & box, CBoxParams & p);
				inline bool IsValid (const CBoxParams & p) const;
				inline void Clear ();
				inline int Find (ULONG lHeadID) const;

			protected:
				typedef struct 
				{
					ULONG m_lID;
					CRect m_rc;
					CRect m_rcClipped;
				} HEADRECTSTRUCT;

				CBoxParams *								m_pLastParam;
				CRect										m_rcPanel;
				CArray <HEADRECTSTRUCT, HEADRECTSTRUCT &>	m_vrcHeads;
			};
			friend class CCache;

			bool RotateTransformByFace (ROTATION r);
			void DrawHeadBounds (CDC & dc, CBoxParams & p, COrder order [6], UINT nMap [6], const CSize3 & ext);
			void DrawBoxOutline (CDC & dc, CBoxParams & params);
			inline void DrawCrosshairs (CDC & dc, CBoxParams & p, const CRect & rcHead); // TODO: protected
			void GetOrder (CBoxParams & params, CPoint pt [8], COrder order [6], UINT nPanels [6]);
			void GetOrder (CBoxParams & params, CPoint pt [8], COrder order [6], UINT nPanels [6], const FoxjetDatabase::COrientation & face);
			void DrawBounds (CDC & dc, const CRect & rc, double dZoom, CBoxParams & params, Panel::CPanel & face);

			void Blt (CDC & dc, const CRect & rc, double dZoom, CBoxParams & params);
			void Erase (CDC & dc, const CRect & rc, const Panel::CBoundary & head, CBoxParams & params);
			void Redraw (CDC & dc, Element::CElement & e, CEditorElementList & list, CBoxParams & params);

			static ULONG GetWorkingHead (Panel::CBoundaryArray & vHeads);

		public:
			static DB::COrientation MapPanel (UINT nPanel, const DB::COrientation & o);
	
			CString GetTrace () const;

			int GetMinFontSize (int nHead) const;
			int GetMaxFontSize (int nHead) const;

			bool IsMonochrome () const { return m_bMonochrome; }

		protected:
			static DB::ORIENTATION TranslateOrientation (CBox & box, const DB::COrientation & panel, const DB::COrientation & relative);
			static DB::COrientation MapAbsolutePanel (const DB::COrientation & relative, ROTATION r);
			static void CalcPanelIndicies (UINT nFace, UINT nPanels [6], DB::ORIENTATION orient = DB::ROT_0);
			static UINT MapPanelNumber (PANEL p);
			static PANEL MapPanelNumber (UINT nPanel);

			CRect CalcPanelRect (CBoxParams & p) const;
			CRect CalcHeadRect (const Panel::CBoundary & b, CBoxParams & p, bool bClip) const;
			void BltSides (CDC & dc, CBoxParams & params, COrder order [6], CPoint pt [8]);
			void DeleteBoxCache ();
			void DeletePlgCache ();
			void DeleteFaceCache ();
			void DrawBackground (CBoxParams & p, CDC & dc, const CRect & rc);
			bool Add (Panel::CBoundary & b);
			void DrawFaceImage (CBoxParams & p, CDC & dc);

			DB::COdbcDatabase &											m_db;
			DB::BOXSTRUCT												m_box;
			DB::LINESTRUCT												m_line;
			Panel::CPanel *												m_pPanels [6];
			DB::COrientation											m_transform;
			CCache														m_cache;
			CArray <CRgn *, CRgn *>										m_vrgnHT;
			CBoxParams *												m_pParams;

			CBitmap *													m_pbmpBox;
			CBitmap *													m_pBmpPanel [6];
			CBitmap *													m_pbmpFace;

			static const UINT											m_nPoints [6][4];

			//CCriticalSection											m_cs;

			bool														m_bMonochrome;
		};
	}; //namespace Box

	_3D_API bool operator == (const Foxjet3d::Box::CBox & lhs, const Foxjet3d::Box::CBox & rhs);
	_3D_API bool operator != (const Foxjet3d::Box::CBox & lhs, const Foxjet3d::Box::CBox & rhs);

}; //namespace Foxjet3d

#ifndef __BOX_INLINE__
	#define __BOX_INLINE__
	#include "Box.inl"
#endif //__ELEMENT_INLINE__

#ifndef __BOXCACHE_INLINE__
	#define __BOXCACHE_INLINE__
	#include "BoxCache.inl"
#endif //__ELEMENT_INLINE__

#endif // !defined(AFX_BOX_H__298DD7F5_44A8_4E0F_81E9_9BBD0F2FB55C__INCLUDED_)
