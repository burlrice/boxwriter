// SelectBoxDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "SelectBoxDlg.h"
#include "BoxUsageDlg.h"
#include "Resource.h"
#include "ItiLibrary.h"
#include "Extern.h"

using namespace ListGlobals;
using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace BoxUsageDlg;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectBoxDlg dialog

CSelectBoxDlg::CSelectBoxDlg(FoxjetDatabase::COdbcDatabase & db, UNITS units, ULONG lLineID, CWnd* pParent /*=NULL*/)
:	m_db (db),
	m_lLineID (lLineID),
	m_units (units),
	FoxjetCommon::CEliteDlg(IDD_SELECTBOX, pParent)
{
	//{{AFX_DATA_INIT(CSelectBoxDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSelectBoxDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectBoxDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectBoxDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CSelectBoxDlg)
	ON_NOTIFY(NM_DBLCLK, LV_BOX, OnDblclkBox)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectBoxDlg message handlers

void CSelectBoxDlg::OnDblclkBox(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnOK ();	
	*pResult = 0;
}

BOOL CSelectBoxDlg::OnInitDialog() 
{
	using namespace ItiLibrary;

	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;
	CArray <BOXSTRUCT, BOXSTRUCT &> vBoxes;

	FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_NAME), 100));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_LENGTH), 100));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_WIDTH), 100));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_HEIGHT), 100));

	m_lvi.Create (LV_BOX, vCols, this, defElements.m_strRegSection);

	GetBoxRecords (m_db, m_lLineID, vBoxes);

	for (int i = 0; i < vBoxes.GetSize (); i++)
		m_lvi.InsertCtrlData (new CBoxItem (vBoxes [i], m_units));

	for (int i = 0; i < m_lvi.GetListCtrl ().GetItemCount (); i++) {
		CBoxItem * p = (CBoxItem *)m_lvi.GetCtrlData (i);

		for (int j = 0; j < m_vSelection.GetSize (); j++) 
			if (p->m_box.m_lID == m_vSelection [j]) 
				m_lvi.GetListCtrl ().SetItemState (i, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelectBoxDlg::OnOK()
{
	using namespace FoxjetCommon;
	using namespace ItiLibrary;

	CLongArray sel = GetSelectedItems (m_lvi.GetListCtrl ());

	if (sel.GetSize	()) {
		m_vSelection.RemoveAll ();

		for (int i = 0; i < sel.GetSize (); i++) {
			CBoxItem * p = (CBoxItem *)m_lvi.GetCtrlData (sel [i]);
			m_vSelection.Add (p->m_box.m_lID);
		}

		FoxjetCommon::CEliteDlg::OnOK ();
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}
