// Element.cpp: implementation of the CElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <math.h>

#include "Element.h"
#include "ElmntLst.h"
#include "Color.h"
#include "Coord.h"
#include "List.h"
#include "BaseDoc.h"
#include "BaseView.h"
#include "Debug.h"
#include "Compare.h"
#include "Quadruple.h"
#include "AppVer.h"
#include "ximage.h"
#include "Parse.h"
#include "BarcodeElement.h"
#include "Extern.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace FoxjetDocument;
using namespace Foxjet3d;
using namespace Color;
using namespace Element;
using namespace Panel;
using namespace Element;

CPair::CPair ()
:	m_pElement (NULL),
	m_pList (NULL),
	m_rc (::rcNull),
	m_nElementID (-1)
{
}

CPair::CPair (CElement * pElement, CEditorElementList * pList, const FoxjetDatabase::HEADSTRUCT * pWorkingHead)
:	m_pElement (pElement),
	m_pList (pList),
	m_rc (::rcNull),
	m_nElementID (-1)
{
	if (m_pElement && m_pList) {
		ASSERT (pWorkingHead);

		m_rc			= m_pElement->GetWindowRect (m_pList->GetHead (), * pWorkingHead);
		m_str			= m_pElement->GetElement ()->ToString (CVersion ());
		m_nElementID	= m_pElement->GetElement ()->GetID ();

		if (FoxjetDatabase::IsValveHead (m_pList->GetHead ()))
			m_rc.InflateRect (CBaseElement::GetValveDotSize ());
	}
}

CPair::CPair (const CPair & rhs)
:	m_pElement (rhs.m_pElement),
	m_pList (rhs.m_pList),
	m_rc (rhs.m_rc),
	m_str (rhs.m_str),
	m_nElementID (rhs.m_nElementID)
{
}

CPair & CPair::operator = (const CPair & rhs)
{
	if (this != &rhs) {
		m_pElement		= rhs.m_pElement;
		m_pList			= rhs.m_pList;
		m_rc			= rhs.m_rc;
		m_str			= rhs.m_str;
		m_nElementID	= rhs.m_nElementID;
	}

	return * this;
}

CPair::~CPair ()
{
}

CRect CPair::GetWindowRect (const FoxjetDatabase::HEADSTRUCT & workingHead) const
{
	return m_rc;
}

CString CPair::GetString () const
{
	return m_str;
}

UINT CPair::GetElementID () const
{
	return m_nElementID;
}

//////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC(CElement, CObject)

const CPoint CElement::m_ptError = CPoint (30, 30); // 0.030" error

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CElement::CElement (FoxjetCommon::CBaseElement * pElement)
:	m_pElement (pElement), 
	m_pTracker (NULL),
//	m_lPixels (0),
	m_pBmp (NULL)
{
	ASSERT (m_pElement);
	SetMovable (m_pElement->IsMovable ());
	SetResizable (m_pElement->IsResizable ());
}

CElement::CElement (const CElement & rhs)
:	m_pElement (CopyElement (rhs.m_pElement, NULL, false)),
	m_pTracker (NULL),
//	m_lPixels (0),
	m_pBmp (NULL)
{
	ASSERT (m_pElement);
	SetMovable (m_pElement->IsMovable ());
	SetResizable (m_pElement->IsResizable ());

	if (rhs.m_pTracker)
		m_pTracker = new CElementTracker (* rhs.m_pTracker);
}

CElement & CElement::operator = (const CElement & rhs)
{
	if (this != &rhs) {
		if (m_pTracker) {
			delete m_pTracker;
			m_pTracker = NULL;
		}

		m_pElement = CopyElement (rhs.GetElement (), NULL, false);

		if (rhs.m_pTracker) 
			m_pTracker = new CElementTracker (* rhs.m_pTracker);

		if (m_pBmp) {
			delete m_pBmp;
			m_pBmp = NULL;
		}
	}

	return * this;
}

CElement::~CElement ()
{
	if (m_pTracker) {
		delete m_pTracker;
		m_pTracker = NULL;
	}

	if (m_pElement) {
		delete m_pElement;
		m_pElement = NULL;
	}

	if (m_pBmp) {
		delete m_pBmp;
		m_pBmp = NULL;
	}
}

void CElement::SetResizable (bool bResizable)
{
	if (GetElement ()->SetResizable (bResizable))
		if (m_pTracker)
			m_pTracker->SetResizable (bResizable);
}

void CElement::SetMovable (bool bMovable)
{
	if (GetElement ()->SetMovable (bMovable))
		if (m_pTracker)
   			m_pTracker->SetMovable (bMovable);
}

bool CElement::SpansMultipleHeads (const CPairArray & v, ULONG lActiveHeadID)
{
	ULONG lID = -1;

	for (int i = 0; i < v.GetSize (); i++) {
		ASSERT (v [i].m_pList);
		const CBoundary & b = v [i].m_pList->m_bounds;

		if (lID != -1) {
			if (b.GetID () != lID)
				return true;
		}
		else if (lActiveHeadID != -1 && b.GetID () != lActiveHeadID)
			return true;

		lID = b.GetID ();
	}

	return false;
}

bool CElement::Track (FoxjetDocument::CBaseView * pParent, const CPoint & ptFrom, const CRect rcHead [2],
					  const CPairArray & v, LPSETLOCACTIONFCT lpLocationFct, FoxjetDatabase::HEADSTRUCT & workingHead)
{
	ASSERT (pParent);

	const CRect rcOld = m_pTracker->m_rect + pParent->GetScrollPosition ();
	double dZoom = pParent->GetZoom ();
	bool bResult = false;
	int nHit = HitTest (ptFrom);
	bool bResizing = ((nHit != CElementTracker::hitMiddle) && 
		(nHit != CElementTracker::hitNothing));

	ASSERT (v.GetSize ());

	if (IsMovable () && !SpansMultipleHeads (v)) {
		for (int i = 0; i < v.GetSize (); i++) {
			ASSERT (v [i].m_pElement);
			CElement & e = * v [i].m_pElement;

			m_pTracker->m_vSelected.Add (CPair (v [i]));

			if (this == &e)
				m_pTracker->m_nLeadIndex = i;
		}

		bResult = (m_pTracker->Track (pParent, ptFrom, rcHead, lpLocationFct) ? true : false);

		if (bResult) {
			const CRect rcNew = m_pTracker->m_rect;
			const CPoint ptScroll = pParent->GetScrollPosition ();
			const CPoint ptDiff = (rcNew.TopLeft () - rcOld.TopLeft () + ptScroll) / dZoom;
			const CSize sizeDiff = (rcNew.Size () - rcOld.Size ()) / dZoom;

			for (int i = 0; i < v.GetSize (); i++) { 
				ASSERT (v [i].m_pElement);
				ASSERT (v [i].m_pList);
				const FoxjetDatabase::HEADSTRUCT & head = v [i].m_pList->GetHead ();
				CElement & e = * v [i].m_pElement;
				CRect rc = e.GetWindowRect (head, workingHead);

				rc += ptDiff;
				rc.InflateRect (0, 0, sizeDiff.cx, sizeDiff.cy);

				e.SetWindowRect (rc, head, workingHead); 
			}
		}
	}

	return bResult;
}


bool CElement::SetWindowRect (const CRect & rc, const FoxjetDatabase::HEADSTRUCT & head,
							  const FoxjetDatabase::HEADSTRUCT & workingHead)
{
	CRect rcElement = CRect (CPoint (0, 0), CBaseElement::LogicalToThousandths (rc.Size (), head)) + 
		CBaseElement::LogicalToThousandths (rc.TopLeft (), head);
	const double dXStretch = 1.0 / CalcXStretch (head, workingHead);

	rcElement += CPoint ((int)((double)rcElement.left * dXStretch) - rcElement.left, 0);
	rcElement.right = rcElement.left + (int)((double)rcElement.Width () * dXStretch);

	bool bResult = GetElement ()->SetWindowRect (rcElement, head);

	if (bResult) {
		if (m_pTracker)
			m_pTracker->SetRect (rc);
	}

	return bResult;
}

double CElement::CalcXStretch (const FoxjetDatabase::HEADSTRUCT & head, const FoxjetDatabase::HEADSTRUCT & workingHead) 
{
#ifdef __WYSIWYGFIX__
	return 1;
#endif

	double dNumerator = workingHead.GetRes (); 
	double dDenominator = head.GetRes (); 

	double dResult = dNumerator / dDenominator;

	return dResult;
}

CRect CElement::GetWindowRect (const FoxjetDatabase::HEADSTRUCT & head, 
							   const FoxjetDatabase::HEADSTRUCT & workingHead) const
{
/*
	CRect rc = GetElement ()->GetWindowRect (head);
	const double dXStretch = CalcXStretch (head, workingHead);
	
	CRect rcResult = CRect (CPoint (0, 0), CBaseElement::ThousandthsToLogical (rc.Size (), head)) + 
		CBaseElement::ThousandthsToLogical (rc.TopLeft (), head);

	rcResult += CPoint ((int)((double)rcResult.left * dXStretch) - rcResult.left, 0);
	rcResult.right = rcResult.left + (int)((double)rcResult.Width () * dXStretch);

	return rcResult;
*/
	return GetWindowRect (* GetElement (), head, workingHead);
}

CRect CElement::GetWindowRect (const FoxjetCommon::CBaseElement & e,
							   const FoxjetDatabase::HEADSTRUCT & head, 
							   const FoxjetDatabase::HEADSTRUCT & workingHead) 
{
	CRect rc = e.GetWindowRect (head);
	const double dXStretch = CalcXStretch (head, workingHead);
	
	CRect rcResult = CRect (CPoint (0, 0), CBaseElement::ThousandthsToLogical (rc.Size (), head)) + 
		CBaseElement::ThousandthsToLogical (rc.TopLeft (), head);

	if (FoxjetDatabase::IsValveHead (head) && !e.IsResizable ()) {
		int nChannels = head.GetChannels ();
		double dVertRes = (double)head.Height () / (double)nChannels;
		double dStretch [2] = { 0 };

		e.CalcStretch (head, dStretch);

		//rcResult.bottom = (int)ceil ((double)(rcResult.bottom / dStretch [1]) * dVertRes);
		rcResult.bottom = rcResult.top + (int)ceil ((double)(rcResult.Height () / dStretch [1]) * dVertRes);
	}

	rcResult += CPoint ((int)((double)rcResult.left * dXStretch) - rcResult.left, 0);
	rcResult.right = rcResult.left + (int)((double)rcResult.Width () * dXStretch);

	return rcResult;
}

void CElement::RecalcSize(CDC & dc, const FoxjetDatabase::HEADSTRUCT & head)
{
	GetElement ()->Draw (dc, head, true); 
	// 'true' causes the element to only recalculate its size
}

void CElement::Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & workingHead,
					 const CEditorElementList * pList, 
					 FoxjetCommon::DRAWCONTEXT context,
					 const CPoint & pt)
{
	const FoxjetDatabase::HEADSTRUCT & head = pList ? pList->GetHead () : FoxjetDatabase::GetDefaultHead ();
	CBaseElement & e = * GetElement ();
	double dStretch [2] = { 1.0, 1.0 };

	e.Draw (dc, head, true, context); // calc size
	CRect rcWin = GetWindowRect (head, workingHead); 

	e.CalcStretch (head, dStretch);
	dStretch [0] *= CalcXStretch (head, workingHead);

//	if (!bTrackerOnly) {
		CSize sizeBmp = rcWin.Size ();

		sizeBmp.cy = (int)ceil ((double)sizeBmp.cy / dStretch [1]);//(int)((double)sizeBmp.cy / dStretch [1]);
		int nDestWidth = (int)((double)sizeBmp.cx * dStretch [0]);
		int nDestHeight = (int)((double)head.Height () * ((double)sizeBmp.cy / (double)head.m_nChannels));

//		CopyBackground (dc, dcMem, rcUnion, dZoom);

		/*
		if (!e.GetRedraw () && context == EDITOR) {
			bool bForceRedraw = false;
			ULONG lPixels = m_pBmp->CountPixels ();
			
			if (GetElement ()->GetClassID () != BMP) {
				BITMAP bm;

				::ZeroMemory (&bm, sizeof (bm)); 

				if (m_pBmp->GetBitmap (bm))
					bForceRedraw = lPixels >= (ULONG)(bm.bmWidth * bm.bmHeight); // if all white
			}

			if (m_lPixels != lPixels || bForceRedraw)
				e.SetRedraw (true);
		}
		*/

		if (e.GetRedraw ()) {
			CBitmap bmp, bmpTmp;
			CDC dcMem, dcElement;

			e.SetRedraw (false);

			VERIFY (dcElement.CreateCompatibleDC (&dc));
			VERIFY (dcMem.CreateCompatibleDC (&dc));

			VERIFY (bmpTmp.CreateCompatibleBitmap (&dc, sizeBmp.cx, nDestHeight));
			VERIFY (bmp.CreateCompatibleBitmap (&dc, sizeBmp.cx, sizeBmp.cy));

			CGdiObject * pOldMemObj = dcMem.SelectObject (&bmpTmp);
			CGdiObject * pOldElementObj = dcElement.SelectObject (&bmp);

			::BitBlt (dcElement, 0, 0, sizeBmp.cx, sizeBmp.cy, NULL, 0, 0, WHITENESS);
			e.Draw (dcElement, head, false, context);

			#ifdef __WYSIWYGFIX__
			nDestWidth = sizeBmp.cx;
			#endif

			if (GetElement ()->IsPrintable ()) {
				dcMem.StretchBlt (
					0, 0, nDestWidth, nDestHeight,
					&dcElement, 
					0, 0, sizeBmp.cx, sizeBmp.cy,
					SRCCOPY); 
			}
			else {
				CDC dcColor, dcTmp;
				CBitmap bmpColor, bmpTmp;
				const CSize size = sizeBmp;

				dcColor.CreateCompatibleDC (&dc);
				dcTmp.CreateCompatibleDC (&dc);

				bmpColor.CreateCompatibleBitmap (&dc, sizeBmp.cx, sizeBmp.cy);
				bmpTmp.CreateCompatibleBitmap (&dc, sizeBmp.cx, sizeBmp.cy);

				CBitmap * pColor = dcColor.SelectObject (&bmpColor);
				CBitmap * pMem = dcTmp.SelectObject (&bmpTmp);

				//SAVEBITMAP (dcElement, _T ("C:\\Foxjet\\Debug\\dcElement.bmp"));
				//SAVEBITMAP (dcMem, _T ("C:\\Foxjet\\Debug\\dcMem0.bmp"));

				dcColor.FillSolidRect (0, 0, sizeBmp.cx, sizeBmp.cy, GetElement ()->GetColor ());
				//SAVEBITMAP (dcColor, _T ("C:\\Foxjet\\Debug\\dcColor.bmp"));

				::BitBlt (dcTmp, 0, 0, sizeBmp.cx, sizeBmp.cy, NULL, 0, 0, WHITENESS);
				dcTmp.BitBlt (0, 0, sizeBmp.cx, sizeBmp.cy, &dcElement, 0, 0, SRCINVERT);
				//SAVEBITMAP (dcTmp, _T ("C:\\Foxjet\\Debug\\dcTmp0.bmp"));

				/*
				const COLORREF rgbRed						= RGB (255,	0,	 0);
				const COLORREF rgbBlack						= RGB (0,	0,	 0);
				const COLORREF rgbWhite						= RGB (255,	255, 255);

				*/

				dcTmp.BitBlt (0, 0, sizeBmp.cx, sizeBmp.cy, &dcColor, 0, 0, SRCAND);
				//SAVEBITMAP (dcTmp, _T ("C:\\Foxjet\\Debug\\dcTmp1.bmp"));

				dcTmp.BitBlt (0, 0, sizeBmp.cx, sizeBmp.cy, &dcElement, 0, 0, SRCINVERT);
				//SAVEBITMAP (dcTmp, _T ("C:\\Foxjet\\Debug\\dcTmp2.bmp"));

				dcMem.StretchBlt (0, 0, nDestWidth, nDestHeight, &dcTmp, 0, 0, sizeBmp.cx, sizeBmp.cy, SRCPAINT);
				//SAVEBITMAP (dcMem, _T ("C:\\Foxjet\\Debug\\dcMem1.bmp"));
				//dcMem.StretchBlt (
				//	0, 0, nDestWidth, nDestHeight,
				//	&dcElement, 
				//	0, 0, sizeBmp.cx, sizeBmp.cy,
				//	SRCCOPY); 
	
				dcColor.SelectObject (pColor);
				dcTmp.SelectObject (pMem);
			}

			/* TODO: rem
			if (context == FoxjetCommon::CBaseElement::EDITOR) 
				m_lPixels = CountPixels (bmpTmp);
				// remember that CountPixels actually counts white pixels (true dot in PRINTER context)
			*/

			//TRACEF (FoxjetDatabase::ToString ((long)GetElement ()->GetID ()) + _T (": ") + FoxjetDatabase::ToString (m_lPixels));

			if (m_pBmp) 
				delete m_pBmp;

			m_pBmp = new CRawBitmap (bmpTmp);

			dcMem.SelectObject (pOldMemObj);
			dcElement.SelectObject (pOldElementObj);

		}

		if (m_pBmp) {
			CBitmap bmp;
			CDC dcMem;

			VERIFY (dcMem.CreateCompatibleDC (&dc));
			VERIFY (m_pBmp->GetBitmap (bmp));

			CGdiObject * pOldMemObj = dcMem.SelectObject (&bmp);
			dc.BitBlt (pt.x, pt.y, sizeBmp.cx, nDestHeight, &dcMem, 0, 0, SRCAND); 
			dcMem.SelectObject (pOldMemObj);
		}
		else 
			dc.FillSolidRect (pt.x, pt.y, sizeBmp.cx, sizeBmp.cy, rgbDarkGray);

/*
	}

	if (m_pTracker && !bPreview) {
		// tracker rect must be drawn to reflect the zoom in non-active windows
		// (zoom will be 1.0 in active window)
		m_pTracker->Draw (&dc, pa.ClientToProd (GetWindowRect (head), dZoom));
	}

*/
}


void CElement::DrawTracker (CDC & dc, const CRect & rc, const CPoint & ptOrg, const FoxjetDatabase::HEADSTRUCT & head)
{
	if (m_pTracker) {
		m_pTracker->Draw (&dc, rc - ptOrg);
		SetTrackerRect (rc);
	}
}

void CElement::CopyBackground (CDC & dcScreen, CDC & dcMem, const CRect & rc, double dZoom, DWORD dwRop)
{ 
	int x = 0;
	int y = 0;
	int cx = (int)((double)rc.Width () / dZoom);
	int cy = (int)((double)rc.Height () / dZoom);

	dcMem.StretchBlt (x, y, cx, cy, &dcScreen, 
		rc.left, rc.top, 
		rc.Width (), rc.Height (), dwRop);
}


void CElement::SetSelected(bool bSelect, const CRect & rc, const FoxjetDatabase::HEADSTRUCT & head,
						   const FoxjetDatabase::HEADSTRUCT & workingHead)
{
	if (m_pTracker) {
		delete m_pTracker;
		m_pTracker = NULL;
	}

	if (bSelect) {
		m_pTracker = new CElementTracker (head, workingHead, &rc, 0, this);
		m_pTracker->SetResizable (IsResizable ());
		m_pTracker->SetMovable (IsMovable ());
		m_pTracker->SetLockAspectRatio (IsLockAspectRatio ());
	}
}

void CElement::SetSelected(bool bSelect, const FoxjetDocument::CBaseView * pView, 
						   const CEditorElementList & list, 
						   const FoxjetDatabase::HEADSTRUCT & workingHead)
{
	const FoxjetDatabase::HEADSTRUCT & head = list.GetHead ();
	CRect rc = GetWindowRect (head, workingHead);

	ASSERT (pView);

	SetSelected (bSelect, list.GetProductArea (0).ClientToProd (rc, pView->GetZoom ()), 
		head, workingHead);
}

bool CElement::SetFontName (const CString & str)
{
	if (GetElement ()->HasFont ()) {
		CBaseTextElement * p = (CBaseTextElement *)GetElement ();
		
		if (p->SetFontName (str)) {
			p->Build ();
			return true;
		}
	}

	return false;
}

bool CElement::SetFontSize (int nSize)
{
	if (GetElement ()->HasFont ()) {
		CBaseTextElement * p = (CBaseTextElement *)GetElement ();
		
		if (p->SetFontSize (nSize)) {
			p->Build ();
			return true;
		}
	}

	return false;
}

bool CElement::SetWidth (int nWidth)
{
	if (GetElement ()->SetWidth (nWidth)) {
		GetElement ()->Build ();
		return true;
	}

	return false;
}

bool CElement::SetFontBold (bool bBold)
{
	if (GetElement ()->HasFont ()) {
		CBaseTextElement * p = (CBaseTextElement *)GetElement ();
		
		if (p->SetFontBold (bBold)) {
			p->Build ();
			return true;
		}
	}

	return false;
}

bool CElement::SetFontItalic (bool bItalic)
{
	if (GetElement ()->HasFont ()) {
		CBaseTextElement * p = (CBaseTextElement *)GetElement ();
		
		if (p->SetFontItalic (bItalic)) {
			p->Build ();
			return true;
		}
	}

	return false;
}

// confines an element to the product area
// returns true if the element's pos was changed, false if not
bool CElement::Confine(CDC & dc, const CRect rcHead [2], double dZoom, 
					   const FoxjetDatabase::HEADSTRUCT & head,
					   const FoxjetDatabase::HEADSTRUCT & workingHead)
{
	bool bResult = false;

	// TODO: make sure this is called after every operation that can
	// change the element's size 

	if (IsConfiningOn ()) {
		CPoint ptError = m_ptError;
		
		//if ((int)(dZoom * 100) > 16)
		//	if (head.GetChannels () > 32) 
		//		ptError.y = (int)((double)ptError.y * 0.25);

		ptError.x = ceil (2.0 / (double)head.GetRes () * 1000.0);
		ptError.y = ceil (CBaseElement::LogicalToThousandths (CPoint (0, head.Height ()), head).y / (double)head.GetChannels ());
		ptError.y /= 2;

		RecalcSize (dc, head);
		const double dXStretch = CalcXStretch (head, workingHead);
		CRect rc = GetElement ()->GetWindowRect (head);
		int nMaxY = rcHead [0].top - rcHead [1].top;
		CSize pa = rcHead [1].Size ();

		pa.cx = (int)ceil ((double)pa.cx / dZoom);
		pa.cx = (int)((double)pa.cx / dXStretch);
		pa.cy = (int)ceil ((double)pa.cy / dZoom);

		nMaxY = (int)ceil /*floor*/ ((double)nMaxY / dZoom);
		nMaxY = CBaseElement::LogicalToThousandths (CPoint (0, nMaxY), head).y; 
		CRect rcProd (CPoint (0, nMaxY), CBaseElement::LogicalToThousandths (pa, head));

		rcProd.bottom = rcProd.top + CBaseElement::LogicalToThousandths (CPoint (0, head.Height ()), head).y;

		if (head.GetChannels () > 32) 
			rcProd.InflateRect (ptError.x, ptError.y);

		if (head.GetChannels () > 256) 
			rcProd.InflateRect (0, ptError.y, 0, ptError.y);

		if (!rcProd.PtInRect (CPoint (rc.BottomRight () - ptError))) {
			{ CString str; str.Format (_T ("ptError: (%d, %d)"), ptError.x, ptError.y); TRACEF (str); }
			{ CString str; str.Format (_T ("pa.cx: %d"), CBaseElement::LogicalToThousandths (CPoint (pa.cx, 0), head).x); TRACEF (str); }
			{ CString str; str.Format (_T ("rcProd: %d, %d (%d, %d)"), rcProd.left, rcProd.top, rcProd.Width (), rcProd.Height ()); TRACEF (str); }
			{ CString str; str.Format (_T ("rc: %d, %d (%d, %d)"), rc.left, rc.top, rc.Width (), rc.Height ()); TRACEF (str); }
			TRACEF (GetElement ()->ToString (::verApp));

			if (FoxjetElements::CBarcodeElement * p = DYNAMIC_DOWNCAST (FoxjetElements::CBarcodeElement, GetElement ())) {
				using namespace FoxjetElements;

				CBarcodeParams local = p->GetLocalParam (p->GetLocalParamUID (), p->GetSymbology ());
				CBarcodeParams global = GetBarcodeParam (p->GetLineID (), p->GetLocalParamUID (), p->GetSymbology (), head.m_nHeadType, head.GetRes (), p->GetMagnification (), ListGlobals::GetDB ());

				TRACEF (FoxjetDatabase::ToString (local.GetBarHeight ()));
				TRACEF (FoxjetDatabase::ToString (global.GetBarHeight ()));
				//FoxjetElements::CBarcodeElement::Get
			}

			int x = min (rcProd.BottomRight ().x - rc.Width (), rc.TopLeft ().x);
			int y = min (rcProd.BottomRight ().y - rc.Height (), rc.TopLeft ().y);

			x = max (x, 0);
			y = max (y, 0);

			TRACEF (_T ("\n\t*** out of bounds [BottomRight] ***"));
			GetElement ()->SetPos (CPoint (x, y), &head);
			bResult = true;
		}

		rc = GetElement ()->GetWindowRect (head); 
		// could have changed if the bottom right corner was out of bounds

		if (!rcProd.PtInRect (rc.TopLeft ())) { 
			int x = max (0, rc.TopLeft ().x);
			int y = max (nMaxY, rc.TopLeft ().y);

			TRACEF (_T ("\n\t*** out of bounds [TopLeft] ***"));
			GetElement ()->SetPos (CPoint (x, y), &head);
			bResult = true;
		}

		rc = GetElement ()->GetWindowRect (head); 
		// could have changed if the top left corner was out of bounds

		const CPoint ptBR = CPoint (rc.BottomRight () - ptError);
		bool bTL = rcProd.PtInRect (rc.TopLeft ()) ? true : false;
		bool bBR = rcProd.PtInRect (ptBR) ? true : false;

		if (!bTL || !bBR) { 
			TRACEF (_T ("\n\t*** out of bounds [could not resolve] ***"));
			GetElement ()->SetPos (CPoint (0, nMaxY), &head);
			bResult = true;
		}
	}

	return bResult;
}

bool CElement::GetHandles (CRect * pHandles, const FoxjetDatabase::HEADSTRUCT & head,
						   const FoxjetDatabase::HEADSTRUCT & workingHead) const
{
	if (pHandles && IsSelected ()) {
		CRect rcHandles [8];

		m_pTracker->CalcHandles (rcHandles, GetWindowRect (head, workingHead));

		for (int i = 0; i < 8; i++) 
			pHandles [i] = rcHandles [i];

		return true;
	}

	return false;
}


CRect CElement::GetAbsoluteRect (const FoxjetDatabase::HEADSTRUCT & head, const FoxjetDatabase::HEADSTRUCT & workingHead,
								 const CSize & panel) const
{
	CPoint pt = GetPos (head);
	CSize size = GetWindowRect (head, workingHead).Size ();
	int nHeadTop = ThousandthsToLogical (head.m_lRelativeHeight) + head.Height ();
	return CRect (CPoint (0, panel.cy - nHeadTop), size) + pt;
}

int CElement::Find (const CPairArray & v, const Element::CPair & p)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i] == p)
			return i;

	return -1;
}

CRect CElement::GetValveRect (const CRect & rc, const FoxjetDatabase::HEADSTRUCT & card, double dZoom)
{
	using namespace FoxjetDatabase;

	CRect rcResult (rc);

#ifdef __WINPRINTER__
	if (IsValveHead (card)) {
		CSize size = CBaseElement::GetValveDotSize (card);

		size.cx = max (2, (int)((double)(size.cx / 2.0) * dZoom));
		size.cy = max (2, (int)((double)(size.cy / 2.0) * dZoom));

		rcResult.InflateRect (size.cx, size.cy);
	}
#endif //__WINPRINTER__

	return rcResult;
}

void CElement::Draw (CDC & dcSrc, const FoxjetDatabase::HEADSTRUCT & workingHead, CEditorElementList * pList,
					 CDC & dcDest, int nXDest, int nYDest, int nDestWidth, int nDestHeight, 
					 int nSrcWidth, int nSrcHeight, double dZoom)
{
	using namespace FoxjetDatabase;
	using FoxjetCommon::EDITOR;
	using FoxjetCommon::PRINTER;

	ASSERT (dcSrc.GetCurrentBitmap () != NULL);		// bitmaps must have been created/selected prior to calling this fct
	ASSERT (dcDest.GetCurrentBitmap () != NULL);	// bitmaps must have been created/selected prior to calling this fct

	if (!IsValveHead (workingHead)) {
		::BitBlt (dcSrc, 0, 0, nSrcWidth, nSrcHeight, NULL, 0, 0, WHITENESS);
		Draw (dcSrc, workingHead, pList, EDITOR);
		dcDest.StretchBlt (nXDest, nYDest, nDestWidth, nDestHeight, &dcSrc, 0, 0, nSrcWidth, nSrcHeight, SRCAND);
	}
#ifdef __WINPRINTER__
	else {
		CBitmap bmpPrinter;
		CDC dcPrinter;
		CBaseElement & e = * GetElement ();
		CSize sizePrinter = e.Draw (dcSrc, workingHead, true, PRINTER); 

		dcPrinter.CreateCompatibleDC (&dcSrc);
		bmpPrinter.CreateCompatibleBitmap (&dcSrc, sizePrinter.cx, sizePrinter.cy);

		CBitmap * pPrinter = dcPrinter.SelectObject (&bmpPrinter);
		::BitBlt (dcPrinter, 0, 0, sizePrinter.cx, sizePrinter.cy, NULL, 0, 0, WHITENESS);
		e.Draw (dcPrinter, workingHead, false, PRINTER); 
		dcPrinter.SelectObject (pPrinter);

		e.Draw (dcSrc, workingHead, true, EDITOR); // restore internal size

		const CSize sizeEditor = GetWindowRect (* GetElement (), workingHead, workingHead).Size ();

		e.Draw (dcSrc, workingHead, true, EDITOR); // restore internal size

		{
			#ifndef __CXIMAGE__
			#error can't build without __CXIMAGE__
			#endif

			CxImage img;
			CBrush brush (rgbBlack);
			CPen pen (PS_SOLID, 1, rgbBlack);

			VERIFY (img.CreateFromHBITMAP (bmpPrinter));
			VERIFY (img.RotateLeft ());

			CBrush * pBrush = dcDest.SelectObject (&brush);
			CPen * pPen = dcDest.SelectObject (&pen);
			
			CCoord res = CBaseElement::GetValveRes (workingHead);
			CSize dot = CBaseElement::GetValveDotSize (workingHead);
			//double dStretch [2] = { 1, 1 };

			//CBaseElement::CalcStretch (workingHead, dStretch);

			{
				const HEADSTRUCT & head = pList->GetHead ();
				const int nChannels = head.GetChannels ();
				const int nHeight = (int)(head.GetSpan () * 1000.0);

				res.m_y = ((double)(nChannels - 1) / (double)nHeight * 1000.0); 
			}

			dot.cx = max (2, (int)((double)dot.cx * dZoom));
			//dot.cy = max (2, (int)((double)dot.cy * dZoom * 2.0));
			
			double i	= /* nXDest; */ (double)nXDest - ((double)dot.cx / 3.0);
			double j	= /* nYDest; */ (double)nYDest - ((double)dot.cy / 3.0);
			double cx	= ((double)dcDest.GetDeviceCaps (LOGPIXELSX) / res.m_x) * dZoom;
			double cy	= ((double)dcDest.GetDeviceCaps (LOGPIXELSY) / res.m_y) * dZoom;

			for (DWORD y = 0; y < img.GetHeight (); y++) {
				for (DWORD x = 0; x < img.GetWidth (); x++) {
					bool bBit = img.GetPixelGray (x, y) ? true : false;

					if (e.IsInverse ())
						bBit = !bBit;

					if (bBit) {
						CRect rc = CRect (0, 0, dot.cx, dot.cy);

						rc += CPoint ((int)i, (int)j);
						dcDest.Ellipse (rc);
						dcDest.FillRect (rc, &brush);
					}

					i += cx;
				}

				j += cy;
				i = /* nXDest; */ (double)nXDest - ((double)dot.cx / 3.0);
			}

			dcDest.SelectObject (pBrush);
			dcDest.SelectObject (pPen);
		}
	}
#endif //__WINPRINTER__
}