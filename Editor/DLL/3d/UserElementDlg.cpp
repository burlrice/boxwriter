// UserElementDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "3d.h"
#include "UserElementDlg.h"
#include "Debug.h"
#include "AppVer.h"
#include "DbFieldDlg.h"
#include "Extern.h"
#include "DbFieldDlg.h"
#include "MsgBox.h"
#include "Parse.h"
#include "IpElements.h"
#include "BarcodeElement.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetUtils;
using namespace Foxjet3d;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef __WINPRINTER__
	#include "DatabaseElement.h"
	#include "BitmapElement.h"
	#include "UserElement.h"
	#include "ExpDateElement.h"

	using namespace FoxjetElements;
#endif //__WINPRINTER__

#ifdef __IPPRINTER__
	#include "DynamicTableDlg.h"
	#include "DynamicTextElement.h"
	#include "DynamicBarcodeElement.h"

	using namespace FoxjetIpElements;
#endif //__IPPRINTER__

using namespace ItiLibrary;
using namespace FoxjetCommon;
using namespace UserElementDlg;

#ifdef __WINPRINTER__

std::vector <std::wstring> CUserElementDlg::CEditEx::OnTyping (const CString & str)
{
	std::vector <std::wstring> v;
	CLongArray sel = GetSelectedItems (m_pParent->m_lv);

	if (sel.GetSize ()) {
		CUserItem * pUser = (CUserItem *)m_pParent->m_lv.GetCtrlData (sel [0]);

		if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, pUser->m_pElement)) {
			const int nRecords = 10;
			CString strSQL;

			try {
				COdbcCache db = COdbcDatabase::Find (p->GetDSN ());

				strSQL.Format (_T ("SELECT TOP %d [%s] FROM [%s] WHERE [%s] LIKE '%s%%' ORDER BY [%s] ASC"), 
					nRecords + 1, 
					p->GetKeyField (), 
					p->GetTable (),
					p->GetKeyField (), 
					str, 
					p->GetKeyField ());
				//TRACEF (strSQL);

				COdbcRecordset rst (db.GetDB ());

				if (rst.Open (strSQL)) {
					while (!rst.IsEOF ()) {
						CString str = (CString)rst.GetFieldValue ((short)0);
						v.push_back (std::wstring (str));
						//TRACEF (FoxjetDatabase::ToString ((int)v.size ()) + _T (": ") + str);
						rst.MoveNext ();
					}
				}
			}
			catch (CDBException * e)		{ Enable (false); HANDLEEXCEPTION_TRACEONLY (e); }
			catch (CMemoryException * e)	{ Enable (false); HANDLEEXCEPTION_TRACEONLY (e); }

			if (v.size () >= nRecords) {
				v.pop_back ();
				v.push_back (std::wstring ((LPCTSTR)CEditEx::m_strMore));
			}
		}
	}

	return v;
}


bool CUserElementDlg::Get (const FoxjetCommon::CBaseElement * pElement, const FoxjetDatabase::HEADSTRUCT & h, Foxjet3d::UserElementDlg::CUserElementArray & vPrompt, bool bTaskStart) 
{
	bool bResult = false;
	FoxjetDatabase::HEADSTRUCT head = h;

	if (CUserElement * p = DYNAMIC_DOWNCAST (CUserElement, pElement)) {
		if ((bTaskStart && p->m_bPromptAtTaskStart) || !bTaskStart) {
			if (!Foxjet3d::UserElementDlg::CUserElementDlg::Find (p->GetPrompt (), vPrompt)) {
				vPrompt.Add (Foxjet3d::UserElementDlg::CUserItem (p, &head));
				bResult = true;
			}
		}
	}
	else if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, pElement)) {
		if ((bTaskStart && p->GetPromptAtTaskStart ()) || !bTaskStart) {
			if (!Foxjet3d::UserElementDlg::CUserElementDlg::Find (p->GetPrompt (), vPrompt)) {
				vPrompt.Add (Foxjet3d::UserElementDlg::CUserItem (p, &head));
				bResult = true;
			}
		}
	}
	else if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, pElement)) {
		if (p->IsDatabaseLookup ()) {
			if ((bTaskStart && p->GetDatabasePromptAtTaskStart ()) || !bTaskStart) {
				if (!Foxjet3d::UserElementDlg::CUserElementDlg::Find (p->GetDatabasePrompt (), vPrompt)) {
					vPrompt.Add (Foxjet3d::UserElementDlg::CUserItem (p, &head));
					bResult = true;
				}
			}
		}
		else if (p->IsUserPrompted ()) {
			if ((bTaskStart && p->IsPromptAtTaskStart ()) || !bTaskStart) {
				if (!Foxjet3d::UserElementDlg::CUserElementDlg::Find (p->GetUserPrompt (), vPrompt)) {
					vPrompt.Add (Foxjet3d::UserElementDlg::CUserItem (p, &head));
					bResult = true;
				}
			}
		}
	}
	else if (CExpDateElement * p = DYNAMIC_DOWNCAST (CExpDateElement, pElement)) { // sw0924
		switch (p->GetType ()) {
		case CExpDateElement::TYPE_DATABASE:
			if (p->GetKeyField ().GetLength () > 0) {
				if ((bTaskStart && p->GetPromptAtTaskStart ()) || !bTaskStart) {
					if (!Foxjet3d::UserElementDlg::CUserElementDlg::Find (p->GetPrompt (), vPrompt)) {
						vPrompt.Add (Foxjet3d::UserElementDlg::CUserItem (p, &head));
						bResult = true;
					}
				}
			}
			break;
		case CExpDateElement::TYPE_USER:
			if ((bTaskStart && p->GetPromptAtTaskStart ()) || !bTaskStart) {
				if (!Foxjet3d::UserElementDlg::CUserElementDlg::Find (p->GetPrompt (), vPrompt)) {
					vPrompt.Add (Foxjet3d::UserElementDlg::CUserItem (p, &head));
					bResult = true;
				}
			}
			break;
		}
	}
	else if (CBarcodeElement * pFind = DYNAMIC_DOWNCAST (CBarcodeElement, pElement)) { 
		for (int i = 0; i < pFind->GetSubElementCount (); i++)
			Get (pFind->GetSubElement (i).GetElement (), h, vPrompt, bTaskStart);
	}

	return bResult;
}

bool CUserElementDlg::Set (const FoxjetCommon::CBaseElement * pElement, const FoxjetDatabase::HEADSTRUCT & head, const CString & strPrompt, const CString & strData, const CString & strKeyValue, const FoxjetDatabase::USERSTRUCT & user, const FoxjetUtils::CDatabaseStartDlg & dlgDB) 
{
	bool bResult = false;

	if (CUserElement * pFind = DYNAMIC_DOWNCAST (CUserElement, pElement)) {
		if (!pFind->GetPrompt ().CompareNoCase (strPrompt)) {
			pFind->SetDefaultData (strData);
			TRACEF (pFind->GetPrompt () + " --> " + strData);
			#ifdef __WINPRINTER__
			{
				CString strSpecialData;

				if (!pFind->GetPrompt ().CompareNoCase (CUserElement::m_lpszUserFirst))
					strSpecialData = user.m_strFirstName;
				else if (!pFind->GetPrompt ().CompareNoCase (CUserElement::m_lpszUserLast))
					strSpecialData = user.m_strLastName;
				else if (!pFind->GetPrompt ().CompareNoCase (CUserElement::m_lpszUserName))
					strSpecialData = user.m_strUserName;

				if (strSpecialData.GetLength ()) {
					pFind->SetDefaultData (strSpecialData);
					bResult = true;
					TRACEF (pFind->GetPrompt () + " --> " + pFind->GetDefaultData ());

					//if (!pFind->GetPrompt ().CompareNoCase (_T ("DATE")))
					//	ASSERT (!pFind->GetDefaultData ().CompareNoCase (_T ("01/2003")));
				}
			}
			#endif //__WINPRINTER__
		}
	}
	else if (CDatabaseElement * pFind = DYNAMIC_DOWNCAST (CDatabaseElement, pElement)) {
		if (!pFind->GetPrompt ().CompareNoCase (strPrompt)) {
			pFind->SetKeyValue (strData);
			bResult = true;
			TRACEF (pFind->GetPrompt () + " --> " + strData);
		}

		if (!pFind->GetDSN ().CompareNoCase (dlgDB.m_strDSN) &&
			!pFind->GetTable ().CompareNoCase (dlgDB.m_strTable) &&
			!pFind->GetKeyField ().CompareNoCase (dlgDB.m_strKeyField))
		{
			pFind->SetKeyValue (strKeyValue);
			bResult = true;
			TRACEF (pFind->GetPrompt () + " --> " + strKeyValue);
		}
	}
	else if (CExpDateElement * pFind = DYNAMIC_DOWNCAST (CExpDateElement, pElement)) { // sw0924
		switch (pFind->GetType ()) {
		case CExpDateElement::TYPE_DATABASE:
			{
				if (!pFind->GetPrompt ().CompareNoCase (strPrompt)) {
					CDatabaseElement e (head);

					pFind->SetKeyValue (strData);
					e.Set (* pFind);
					e.Build ();
					TRACEF (e.GetImageData ());
					pFind->SetSpan (_ttoi (e.GetImageData ()));
					pFind->Build ();

					TRACEF (pFind->GetImageData ());
					bResult = true;
					TRACEF (pFind->GetPrompt () + " --> " + strData);
				}

				if (!pFind->GetDSN ().CompareNoCase (dlgDB.m_strDSN) &&
					!pFind->GetTable ().CompareNoCase (dlgDB.m_strTable) &&
					!pFind->GetKeyField ().CompareNoCase (dlgDB.m_strKeyField))
				{
					pFind->SetKeyValue (strKeyValue);
					bResult = true;
					TRACEF (pFind->GetPrompt () + " --> " + strKeyValue);
				}
			}
			break;
		case CExpDateElement::TYPE_USER:
			if (!pFind->GetPrompt ().CompareNoCase (strPrompt)) {
				//pFind->SetDefaultData (strData);
				pFind->SetSpan (_ttoi (strData));
				bResult = true;
			}
			break;
		}
	}
	else if (CBitmapElement * pFind = DYNAMIC_DOWNCAST (CBitmapElement, pElement)) {
		if (pFind->IsDatabaseLookup ()) {
			if (!pFind->GetDatabasePrompt ().CompareNoCase (strPrompt)) {
				pFind->SetDatabaseKeyValue (strData);
				pFind->Build ();
				TRACEF (pFind->GetFilePath ());
				bResult = true;
				TRACEF (pFind->GetDatabasePrompt () + " --> " + strData);
			}

			if (!pFind->GetDatabaseDSN ().CompareNoCase (dlgDB.m_strDSN) &&
				!pFind->GetDatabaseTable ().CompareNoCase (dlgDB.m_strTable) &&
				!pFind->GetDatabaseKeyField ().CompareNoCase (dlgDB.m_strKeyField))
			{
				pFind->SetDatabaseKeyValue (strKeyValue);
				bResult = true;
				TRACEF (pFind->GetDatabasePrompt () + " --> " + strKeyValue);
			}
		}
		else {
			if (FoxjetCommon::IsProductionConfig () || pFind->IsUserPrompted ()) {//#if __CUSTOM__ == __SW0833__
				if (!pFind->GetFilePath ().CompareNoCase (strPrompt)) {
					pFind->SetFilePath (strData, head);
					bResult = true;
					ResetSize (pFind, head);
					TRACEF (pFind->GetFilePath () + " --> " + strData);
				}
			}

			if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, pElement)) {
				if (p->IsUserPrompted ()) {
					if (!p->GetUserPrompt ().CompareNoCase (strPrompt)) {
						p->SetFilePath (strData, head);
						bResult = true;
						ResetSize (p, head);
					}
				}
			}

			/*
			if (theApp.IsExpo ()) {
				using namespace FoxjetElements;
				using namespace FoxjetCommon;

				CString strPath = pFind->GetFilePath ();

				strPath.MakeUpper ();
				TRACEF (strPath);

				if (strPath.Find (_T ("PLACEHOLDER.BMP")) != -1) {
					CString strPath = pFind->GetDefPath () + strData + _T (".bmp");

					if (!pFind->SetFilePath (strPath, info.m_Config.m_HeadSettings))
						pFind->SetFilePath (pFind->GetDefPath () + _T ("placeholder.bmp"), info.m_Config.m_HeadSettings);

					CSize size;
					
					SEND_THREAD_MESSAGE (info, TM_GET_PRODUCTAREA, &size);

					pFind->SetPos (CPoint (0, 0), &info.m_Config.m_HeadSettings);
					pFind->SetSize (size, info.m_Config.m_HeadSettings);
				}
			}
			*/
		}
	}
	else if (CBarcodeElement * pFind = DYNAMIC_DOWNCAST (CBarcodeElement, pElement)) { 
		for (int i = 0; i < pFind->GetSubElementCount (); i++)
			Set (pFind->GetSubElement (i).GetElement (), head, strPrompt, strData, strKeyValue, user, dlgDB);
	}

	return bResult;
}

void CUserElementDlg::ResetSize (FoxjetElements::CBitmapElement * p, const FoxjetDatabase::HEADSTRUCT & h)
{
	using namespace FoxjetElements;
	using namespace FoxjetCommon;

	double dStretch [2] = { 1, 1 };
	CBaseElement::CalcStretch (h, dStretch);
	CSize size = CBitmapElement::GetSizePixels (p->GetFilePath (), h);

	size.cy = (int)ceil ((double)size.cy * dStretch [1]);
	size = CBaseElement::LogicalToThousandths (size, h);
	p->SetSize (size, h);
}
#endif //__WINPRINTER__

CUserItem::CUserItem (FoxjetCommon::CBaseElement * pElement, FoxjetDatabase::HEADSTRUCT * pHead,
					  const CString & strPrompt, const CString & strData)
:	m_pElement (pElement),
	m_strPrompt (strPrompt),
	m_strData (strData)
{
	if (pHead)
		m_head = * pHead;

	if (m_pElement) {
		if (!m_strPrompt.GetLength ()) {
			#ifdef __WINPRINTER__
				if (CUserElement * p = DYNAMIC_DOWNCAST (CUserElement, m_pElement))
					m_strPrompt = p->GetPrompt ();
				else if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, m_pElement))
					m_strPrompt = p->GetPrompt ();
				else if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, m_pElement)) {
					if (p->IsDatabaseLookup ())
						m_strPrompt = p->GetDatabasePrompt ();
					else if (p->IsUserPrompted ())
						m_strPrompt = p->GetUserPrompt ();
					else
						m_strPrompt = p->GetFilePath (); //#if __CUSTOM__ == __SW0833__
				}
				else if (CExpDateElement * p = DYNAMIC_DOWNCAST (CExpDateElement, m_pElement)) { // sw0924
					switch (p->GetType ()) {
					case CExpDateElement::TYPE_DATABASE:
					case CExpDateElement::TYPE_USER:
						m_strPrompt = p->GetPrompt ();
						break;
					}
				}

			#endif //__WINPRINTER__
		}
		
		#ifdef __WINPRINTER__
			if (CUserElement * p = DYNAMIC_DOWNCAST (CUserElement, pElement))
				m_strData = p->GetDefaultData ();
			else if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, pElement))
				m_strData = p->GetKeyValue ();
			else if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, m_pElement)) {
				if (p->IsDatabaseLookup ())
					m_strData = p->GetDatabaseKeyValue ();
				else
					m_strData = p->GetFilePath (); //#if __CUSTOM__ == __SW0833__
			}
			else if (CExpDateElement * p = DYNAMIC_DOWNCAST (CExpDateElement, m_pElement)) { // sw0924
				switch (p->GetType ()) {
				case CExpDateElement::TYPE_DATABASE:
					m_strData = p->GetKeyValue ();
					break;
				case CExpDateElement::TYPE_USER:
					m_strData = FoxjetDatabase::ToString ((int)p->GetSpan ());
					break;
				}
			}
		#endif

		if (!m_strData.GetLength ())
			m_pElement->GetDefaultData ();
	}
}


CUserItem::CUserItem (const CUserItem & rhs)
:	m_pElement (rhs.m_pElement),
	m_head (rhs.m_head),
	m_strPrompt (rhs.m_strPrompt),
	m_strData (rhs.m_strData)
{
}

CUserItem & CUserItem::operator = (const CUserItem & rhs)
{
	if (this != &rhs) {
		m_pElement		= rhs.m_pElement;
		m_head			= rhs.m_head;
		m_strPrompt		= rhs.m_strPrompt;
		m_strData		= rhs.m_strData;
	}

	return * this;
}

CUserItem::~CUserItem ()
{
}

CString CUserItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:		return m_head.m_strName;
	case 1:		return m_strPrompt;
	case 2:		return m_strData;
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CUserElementDlg dialog


CUserElementDlg::CUserElementDlg(ULONG lLineID, CWnd* pParent /*=NULL*/)
:	m_lLineID (lLineID),
	m_txtData (this),
	FoxjetCommon::CEliteDlg(IDD_USER_ELEMENTS_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CUserElementDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CUserElementDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserElementDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_SELECT);
	m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);

	m_txtData.DoDataExchange (pDX, TXT_DATA);
	//DDX_Control (pDX, TXT_DATA, m_txtData);
}


BEGIN_MESSAGE_MAP(CUserElementDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CUserElementDlg)
	ON_NOTIFY(LVN_ITEMCHANGED, LV_ELEMENTS, OnItemchangedElements)
	ON_BN_CLICKED(BTN_SELECT, OnSelect)
	//}}AFX_MSG_MAP
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserElementDlg message handlers

BOOL CUserElementDlg::OnInitDialog() 
{	
	using ListCtrlImp::CColumn;

	CArray <CColumn, CColumn> vCols;

	FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_HEAD), 250));
	vCols.Add (CColumn (LoadString (IDS_PROMPT), 250));
	vCols.Add (CColumn (LoadString (IDS_DATA), 250));

	m_lv.Create (LV_ELEMENTS, vCols, this, _T ("Software\\FoxJet\\Control"));

	for (int i = 0; i < m_vElements.GetSize (); i++)
		m_lv.InsertCtrlData (new CUserItem (m_vElements [i]));

	if (m_lv->GetItemCount ())
		m_lv->SetItemState (0, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);

	if (CEdit * p = (CEdit *)GetDlgItem (TXT_DATA)) {
		p->SetFocus ();
		p->SetSel (0, -1);
	}

	return FALSE;
}

void CUserElementDlg::OnItemchangedElements(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		CUserItem * p = (CUserItem *)m_lv.GetCtrlData (sel [0]);
		bool bDB = false;

		SetDlgItemText (LBL_PROMPT, p->m_strPrompt);
		SetDlgItemText (TXT_DATA, p->m_strData);

		#ifdef __WINPRINTER__
			if (CDatabaseElement * pDatabase = DYNAMIC_DOWNCAST (CDatabaseElement, p->m_pElement)) 
				bDB = true;
			if (CBitmapElement * pBitmap = DYNAMIC_DOWNCAST (CBitmapElement, p->m_pElement)) //#if __CUSTOM__ == __SW0833__
				bDB = true;
			if (CExpDateElement * pDatabase = DYNAMIC_DOWNCAST (CExpDateElement, p->m_pElement)) { // sw0924
				if (pDatabase->GetType () == CExpDateElement::TYPE_DATABASE)
					bDB = true;
			}
		#else
			LONG lDynID = 0;

			if (CDynamicTextElement * pText = DYNAMIC_DOWNCAST (CDynamicTextElement, p->m_pElement)) 
				lDynID = pText->GetDynamicID ();
			if (CDynamicBarcodeElement * pBarcode = DYNAMIC_DOWNCAST (CDynamicBarcodeElement, p->m_pElement))
				lDynID = pBarcode->GetDynamicID ();

			if (lDynID) {
				using FoxjetIpElements::CItemArray;
				using FoxjetIpElements::CTableItem;

				CItemArray vItems;

				CDynamicTableDlg::GetDynamicData (m_lLineID, vItems);

				for (int i = 0; i < vItems.GetSize (); i++) {
					CTableItem item = vItems [i];
					
					if (item.m_lID == lDynID) {
						bDB = item.m_bUseDatabase;
						break;
					}
				}
			}
		#endif

		if (CWnd * p = GetDlgItem (BTN_SELECT)) 
			p->EnableWindow (bDB);
	}

	if (pResult)
		* pResult = 0;
}

void CUserElementDlg::OnOK()
{
	CLongArray sel = GetSelectedItems (m_lv);
	CEdit * pData = (CEdit *)GetDlgItem (TXT_DATA);

	ASSERT (pData);

	if (sel.GetSize ()) {
		int nIndex = sel [0];
		CUserItem * p = (CUserItem *)m_lv.GetCtrlData (nIndex);
		CString strData;

		ASSERT (p);
		ASSERT (p->m_pElement);

		#ifdef __WINPRINTER__
			bool bSetData = false;

			GetDlgItemText (TXT_DATA, strData);

			if (CUserElement * pUser = DYNAMIC_DOWNCAST (CUserElement, p->m_pElement)) 
				bSetData = pUser->SetDefaultData (strData);
			if (CDatabaseElement * pUser = DYNAMIC_DOWNCAST (CDatabaseElement, p->m_pElement)) 
				bSetData = pUser->SetKeyValue (strData);
			if (CBitmapElement * pUser = DYNAMIC_DOWNCAST (CBitmapElement, p->m_pElement)) { 
				if (pUser->IsDatabaseLookup ())
					bSetData = pUser->SetDatabaseKeyValue (strData);
				else {
					BeginWaitCursor ();
					bSetData = pUser->SetFilePath (strData, p->m_head); //#if __CUSTOM__ == __SW0833__
					EndWaitCursor ();

					//if (!bSetData) {
					//	CString str;

					//	str.Format (LoadString (IDS_FILENOTFOUND), strData);
					//	MsgBox (* this, str);
					//}
				}

				ResetSize (pUser, p->m_head);
			}
			if (CExpDateElement * pUser = DYNAMIC_DOWNCAST (CExpDateElement, p->m_pElement)) { // sw0924
				switch (pUser->GetType ()) {
				case CExpDateElement::TYPE_DATABASE:
					bSetData = pUser->SetKeyValue (strData);
					break;
				case CExpDateElement::TYPE_USER:
					bSetData = pUser->SetSpan (_ttoi (strData));
					break;
				}
			}

			switch (p->m_pElement->GetClassID ()) {
			case FoxjetIpElements::DYNAMIC_TEXT:
			case FoxjetIpElements::DYNAMIC_BARCODE:
				bSetData = true;
				break;
			}

			if (bSetData) {
				m_lv.UpdateCtrlData (p);

				for (int i = 0; i < m_vElements.GetSize (); i++) {
					CString strPrompt;

					#ifdef __WINPRINTER__
						if (CUserElement * pUser = DYNAMIC_DOWNCAST (CUserElement, p->m_pElement))
							strPrompt = pUser->GetPrompt ();
						else if (CDatabaseElement * pUser = DYNAMIC_DOWNCAST (CDatabaseElement, p->m_pElement))
							strPrompt = pUser->GetPrompt ();
						else if (CBitmapElement * pUser = DYNAMIC_DOWNCAST (CBitmapElement, p->m_pElement)) {
							if (pUser->IsDatabaseLookup ())
								strPrompt = pUser->GetDatabasePrompt ();
							else
								strPrompt = pUser->GetFilePath (); //#if __CUSTOM__ == __SW0833__
						}
						else if (CExpDateElement * pUser = DYNAMIC_DOWNCAST (CExpDateElement, p->m_pElement)) { // sw0924
							switch (pUser->GetType ()) {
							case CExpDateElement::TYPE_DATABASE:
							case CExpDateElement::TYPE_USER:
								strPrompt = pUser->GetPrompt ();
								break;
							}
						}
						else if (FoxjetIpElements::CDynamicTextElement * pUser = DYNAMIC_DOWNCAST (FoxjetIpElements::CDynamicTextElement, p->m_pElement)) 
							strPrompt = ToString (pUser->GetDynamicID ());
						else if (FoxjetIpElements::CDynamicBarcodeElement * pUser = DYNAMIC_DOWNCAST (FoxjetIpElements::CDynamicBarcodeElement, p->m_pElement)) 
							strPrompt = ToString (pUser->GetDynamicID ());
					#endif //__WINPRINTER__

					if (!m_vElements [i].m_strPrompt.CompareNoCase (strPrompt))
						m_vElements [i].m_strData = strData;
				}

				if ((nIndex + 1) < m_lv->GetItemCount ()) {
					m_lv->SetItemState (nIndex + 1, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
					pData->SetSel (0, -1);
					return;
				}
			}
			else {
				CString str;

				str.Format (LoadString (IDS_INVALIDUSERDATA), strData);
				MsgBox (* this, str);
				pData->SetSel (0, -1);
				return;
			}
		#else
			GetDlgItemText (TXT_DATA, strData);

			p->m_strData = strData;

			m_lv.UpdateCtrlData (p);

			if ((nIndex + 1) < m_lv->GetItemCount ()) {
				
				m_lv->SetItemState (nIndex + 1, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
				pData->SetSel (0, -1);
				return;
			}
		#endif 
	}

	FoxjetCommon::CEliteDlg::OnOK ();

	#ifdef __IPPRINTER__
	m_vElements.RemoveAll ();
	#endif

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		CUserItem * p = (CUserItem *)m_lv.GetCtrlData (i);

		ASSERT (p);
		ASSERT (p->m_pElement);

		#ifdef __WINPRINTER__
			if (CUserElement * pUser = DYNAMIC_DOWNCAST (CUserElement, Find (p->m_pElement, m_vElements))) {
				CString str = p->m_pElement->GetDefaultData ();
				
				TRACEF (str);

				pUser->SetDefaultData (str);
			}
		#else
			m_vElements.Add (* p);
		#endif 
	}
}

FoxjetCommon::CBaseElement * CUserElementDlg::Find (FoxjetCommon::CBaseElement * pElement, 
													const CUserElementArray & vElements)
{
	for (int i = 0; i < vElements.GetSize (); i++) 
		if (vElements [i].m_pElement == pElement)
			return pElement;

	return NULL;
}

FoxjetCommon::CBaseElement * CUserElementDlg::Find (const CString & str,
													const CUserElementArray & vElements)
{
	for (int i = 0; i < vElements.GetSize (); i++) {
		CUserItem item = vElements [i];

		if (!item.m_strPrompt.CompareNoCase (str))
			return item.m_pElement;
	}

	return NULL;
}

void CUserElementDlg::OnSelect() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		int nIndex = sel [0];
		CUserItem * p = (CUserItem *)m_lv.GetCtrlData (nIndex);
		
#ifdef __WINPRINTER__
		if (CBitmapElement * pElement = DYNAMIC_DOWNCAST (CBitmapElement, p->m_pElement)) {
			if (pElement->IsDatabaseLookup ()) {
				CInternalFormatting opt (false);
				CDbFieldDlg dlg (ListGlobals::defElements.m_strRegSection, this);

				dlg.m_dwFlags	= ListCtrlImp::CELLCTRL_NOCHANGECOLSEL; 
				dlg.m_strDSN	= pElement->GetDatabaseDSN ();
				dlg.m_strTable	= pElement->GetDatabaseTable ();
				dlg.m_strField	= pElement->GetDatabaseKeyField ();
				dlg.m_nRow		= 0;
			
				if (dlg.DoModal () == IDOK) 
					SetDlgItemText (TXT_DATA, dlg.m_strSelected);

				return;
			}
		}

		if (CDatabaseElement * pElement = DYNAMIC_DOWNCAST (CDatabaseElement, p->m_pElement)) {
			CInternalFormatting opt (false);
			CDbFieldDlg dlg (ListGlobals::defElements.m_strRegSection, this);

			dlg.m_dwFlags	= ListCtrlImp::CELLCTRL_NOCHANGECOLSEL; 
			dlg.m_strDSN	= pElement->GetDSN ();
			dlg.m_strTable	= pElement->GetTable ();
			dlg.m_strField	= pElement->GetKeyField ();
			dlg.m_nRow		= 0;
			
			if (dlg.DoModal () == IDOK) 
				SetDlgItemText (TXT_DATA, dlg.m_strSelected);
		}
		else if (CExpDateElement * pElement = DYNAMIC_DOWNCAST (CExpDateElement, p->m_pElement)) { // sw0924
			if (pElement->GetType () == CExpDateElement::TYPE_DATABASE) {
				CDbFieldDlg dlg (ListGlobals::defElements.m_strRegSection, this);

				dlg.m_dwFlags	= ListCtrlImp::CELLCTRL_NOCHANGECOLSEL; 
				dlg.m_strDSN	= pElement->GetDSN ();
				dlg.m_strTable	= pElement->GetTable ();
				dlg.m_strField	= pElement->GetKeyField ();
				dlg.m_nRow		= 0;
				
				if (dlg.DoModal () == IDOK) 
					SetDlgItemText (TXT_DATA, dlg.m_strSelected);
			}
		}
#endif //__WINPRINTER__

#ifdef __IPPRINTER__
		LONG lDynID = 0;

		if (CDynamicTextElement * pText = DYNAMIC_DOWNCAST (CDynamicTextElement, p->m_pElement)) 
			lDynID = pText->GetDynamicID ();
		if (CDynamicBarcodeElement * pBarcode = DYNAMIC_DOWNCAST (CDynamicBarcodeElement, p->m_pElement)) 
			lDynID = pBarcode->GetDynamicID ();

		if (lDynID) {
			using FoxjetIpElements::CItemArray;
			using FoxjetIpElements::CTableItem;

			CItemArray vItems;

			CDynamicTableDlg::GetDynamicData (m_lLineID, vItems);

			for (int i = 0; i < vItems.GetSize (); i++) {
				CTableItem item = vItems [i];
				
				if (item.m_lID == lDynID) {
					CInternalFormatting opt (false);
					CDbFieldDlg dlg (ListGlobals::defElements.m_strRegSection, this);

					dlg.m_dwFlags	= ListCtrlImp::CELLCTRL_NOCHANGECOLSEL; 
					dlg.m_strDSN	= item.m_params.m_strDSN;
					dlg.m_strTable	= item.m_params.m_strTable;
					dlg.m_strField	= item.m_params.m_strKeyField;
					dlg.m_nRow		= item.m_params.m_nRow;

					if (dlg.DoModal () == IDOK) 
						SetDlgItemText (TXT_DATA, dlg.m_strSelected);

					break;
				}
			}
		}
#endif //__IPPRINTER__

#ifdef __WINPRINTER__
		if (CBitmapElement * pElement = DYNAMIC_DOWNCAST (CBitmapElement, p->m_pElement)) { //#if __CUSTOM__ == __SW0833__
			CFileDialog dlg (TRUE, _T ("*.bmp"), pElement->GetFilePath (), 
				OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
				GetFileDlgList (), this);

			if (dlg.DoModal () == IDOK) 
				SetDlgItemText (TXT_DATA, dlg.GetPathName ());
		}
#endif //__WINPRINTER__
	}
}


void CUserElementDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	FoxjetCommon::CEliteDlg::OnShowWindow(bShow, nStatus);

	if (bShow) 
		AlignToKeyboard (this);
}
