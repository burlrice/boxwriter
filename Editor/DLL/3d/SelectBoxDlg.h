#if !defined(AFX_SELECTBOXDLG_H__5F421FE4_F6BA_4AEF_8C29_0CA8957F228D__INCLUDED_)
#define AFX_SELECTBOXDLG_H__5F421FE4_F6BA_4AEF_8C29_0CA8957F228D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectBoxDlg.h : header file
//

#include <afxtempl.h>
#include "Database.h"
#include "ListCtrlImp.h"
#include "CopyArray.h"
#include "3dApi.h"
#include "List.h"
#include "Utils.h"

namespace Foxjet3d 
{
	/////////////////////////////////////////////////////////////////////////////
	// CSelectBoxDlg dialog

	class _3D_API CSelectBoxDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		virtual void OnOK();
		CSelectBoxDlg(FoxjetDatabase::COdbcDatabase & db, UNITS units, ULONG lLineID = -1, CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CSelectBoxDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		FoxjetCommon::CLongArray m_vSelection;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CSelectBoxDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		FoxjetDatabase::COdbcDatabase & m_db;
		ItiLibrary::ListCtrlImp::CListCtrlImp m_lvi;
		ULONG m_lLineID;
		UNITS m_units;

		// Generated message map functions
		//{{AFX_MSG(CSelectBoxDlg)
		afx_msg void OnDblclkBox(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace Foxjet3d 

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTBOXDLG_H__5F421FE4_F6BA_4AEF_8C29_0CA8957F228D__INCLUDED_)
