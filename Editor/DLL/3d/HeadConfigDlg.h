#if !defined(AFX_HEADCONFIGDLG_H__CBC7B49D_E43B_4B10_93AF_42BC77E0B45F__INCLUDED_)
#define AFX_HEADCONFIGDLG_H__CBC7B49D_E43B_4B10_93AF_42BC77E0B45F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HeadConfigDlg.h : header file
//

#include "Database.h"
#include "OdbcDatabase.h"
#include "3dApi.h"
#include "Utils.h"
#include "ListCtrlImp.h"
#include "RoundButtons.h"
#include "xImage.h"
#include "Utils.h"

#ifdef __IPPRINTER__
	#include "UDPasocket.h"
#endif //__IPPRINTER__

/////////////////////////////////////////////////////////////////////////////
// CHeadConfigDlg dialog

namespace Foxjet3d
{
	void _3D_API SortByHeight (CArray <FoxjetDatabase::VALVESTRUCT, FoxjetDatabase::VALVESTRUCT &> & v);
	void _3D_API InsertByHeight (CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & v,
		FoxjetDatabase::HEADSTRUCT & h);

	namespace HeadConfigDlg
	{
		class _3D_API CFoundItem : public ItiLibrary::ListCtrlImp::CItem 
		{
		public:
			CFoundItem (const CString & strName = _T (""), const CString & strAddr = _T (""));
			virtual ~CFoundItem ();

			virtual CString GetDispText (int nColumn) const;

			CString m_strName;
			CString m_strAddr;
		};

		class _3D_API CPanel
		{
		public:
			CPanel ();
			CPanel (const FoxjetDatabase::PANELSTRUCT & panel);
			CPanel (const CPanel & rhs);
			CPanel & operator = (const CPanel & rhs);
			virtual ~CPanel ();

			static int Find (CArray <CPanel, CPanel &> & v, ULONG lFindID);
			static int FindByCardID (CArray <CPanel, CPanel &> & v, ULONG lFindID);			
			static void GetPanels (FoxjetDatabase::COdbcDatabase & db, ULONG lLineID, CArray <CPanel, CPanel &> & v);
			static void HeadConfigDlg::CPanel::GetPanels (ULONG lLineID, 
				CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> & v,
				const CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vHeads,
				const CArray <FoxjetDatabase::VALVESTRUCT, FoxjetDatabase::VALVESTRUCT &> & vValves,
				const CArray <FoxjetDatabase::PANELSTRUCT, FoxjetDatabase::PANELSTRUCT &> & vPanels);
			static void InsertByHeight (CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & v,
				FoxjetDatabase::HEADSTRUCT & h);
			static void InsertByHeight (CArray <FoxjetDatabase::VALVESTRUCT, FoxjetDatabase::VALVESTRUCT &> & v,
				FoxjetDatabase::VALVESTRUCT & h);
			static void UpdateValves (FoxjetDatabase::COdbcDatabase & db, ULONG lLineID,
				FoxjetDatabase::HEADSTRUCT & head);
			static FoxjetDatabase::HEADSTRUCT & GetCard (CArray <CPanel, CPanel &> & vPanels, ULONG lCardID);

			CString GetTrace ();

			FoxjetDatabase::PANELSTRUCT m_panel;
			CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> m_vCards; 
			CArray <FoxjetDatabase::VALVESTRUCT, FoxjetDatabase::VALVESTRUCT &> m_vValve; 
		};

		class _3D_API CHeadConfigDlg : public FoxjetCommon::CEliteDlg
		{
		public:
		// Construction
		public:
			CHeadConfigDlg(FoxjetDatabase::COdbcDatabase & db, UNITS units, ULONG lLineID, bool bRestricted, CWnd* pParent = NULL);   // standard constructor
			virtual ~CHeadConfigDlg ();

			CMapStringToString m_vAvailAddressMap;
			ULONG m_lPrinterID;

		// Dialog Data
			//{{AFX_DATA(CHeadConfigDlg)
				// NOTE: the ClassWizard will add data members here
			//}}AFX_DATA


		// Overrides
			// ClassWizard generated virtual function overrides
			//{{AFX_VIRTUAL(CHeadConfigDlg)
			protected:
			virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
			//}}AFX_VIRTUAL

		// Implementation
		protected:
			FoxjetUtils::CRoundButtons m_buttons;

			virtual void OnOK();
			virtual void OnCancel();

			class _3D_API CItem
			{ 
			public:
				typedef enum { PANEL = 0, CARD, VALVE_HEAD } TYPE;

				CItem (const FoxjetDatabase::PANELSTRUCT & s);
				CItem (const FoxjetDatabase::HEADSTRUCT & s);
				CItem (const FoxjetDatabase::VALVESTRUCT & s);
				virtual ~CItem ();

				TYPE m_type;

				union 
				{
					FoxjetDatabase::PANELSTRUCT *	m_pPanel;
					FoxjetDatabase::HEADSTRUCT *	m_pHead;
					FoxjetDatabase::VALVESTRUCT *	m_pValve;
				};

			protected:
				CItem ();
				CItem (const CItem & rhs);
				CItem & operator = (const CItem & rhs);
			};


			virtual void OnSetFont( CFont* pFont );

			void FillHeadCtrl ();
			void InsertItem (CArray <FoxjetDatabase::VALVESTRUCT, FoxjetDatabase::VALVESTRUCT &> & vValve, HTREEITEM hParent);
			void InsertItem (FoxjetDatabase::HEADSTRUCT & head, HTREEITEM hParent);
			CListBox & GetHeadCtrl ();
			CItem * GetCurSel ();
			bool IsAddressConflict (CStringArray & vError);
			bool IsOverlapConflict (CStringArray & vError);
			bool IsIndexConflict (CStringArray & vError);
			void SetLinked ();
			bool IsPHC1Master ();
			bool Edit (FoxjetDatabase::HEADSTRUCT & head, bool bNew = false);
			bool Edit (FoxjetDatabase::PANELSTRUCT & panel);
			bool Edit (FoxjetDatabase::HEADSTRUCT & head, FoxjetDatabase::VALVESTRUCT & valve);
			bool EditHpHead (FoxjetDatabase::HEADSTRUCT & head, bool bNew);
			bool SetNewMasterHead (const FoxjetDatabase::HEADSTRUCT * pHeadOld);
			ULONG GetMasterHeadID ();
			void ResetContent ();

			static BOOL CALLBACK NewHeadFct (const CString & str, CWnd * pWnd);
			void OnNewHead (const CString & strName, const CString & strAddr);

			FoxjetDatabase::HEADSTRUCT GetHeadByID (ULONG lID);

		public:

			FoxjetCommon::CLongArray		m_vAdded;
			FoxjetCommon::CLongArray		m_vEdited;
			FoxjetCommon::CLongArray		m_vDeleted;
			bool							m_bRestartTask;

		protected:

			HICON							m_hPanel;
			HICON							m_hHead;
			HICON							m_hCard;
			HICON							m_hMasterHead;
			HICON							m_hDisabledHead;
			HICON							m_hHeadLinked;

			CxImage							m_imgPanel;
			CxImage							m_imgHead;
			CxImage							m_imgCard;
			CxImage							m_imgMasterHead;
			CxImage							m_imgDisabledHead;
			CxImage							m_imgHeadLinked;
			const int						m_nItemHeight;

			FoxjetDatabase::COdbcDatabase & m_db;
			ULONG							m_lLineID;
			ULONG							m_lLastHeadID;
			ULONG							m_lLastValveID;
			const UNITS						m_units;
			bool							m_bAddressConflict;
			CImageList *					m_pDragImage;
			HTREEITEM						m_hitemDrag;
			HTREEITEM						m_hitemDrop;
			bool							m_bDragging;
			HCURSOR							m_hcurOriginal;
			HCURSOR							m_hcurNo;
			bool							m_bSync;
			CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> m_vSync;
			bool							m_bRestricted;
			CMap <ULONG, ULONG, ULONG, ULONG> m_vPhotoDelay;

	#ifdef __IPPRINTER__
			FoxjetIpElements::CUDPasocket *	m_pSocket;
			ItiLibrary::ListCtrlImp::CListCtrlImp m_lvFound;
	#endif //__IPPRINTER__

			// Generated message map functions
			//{{AFX_MSG(CHeadConfigDlg)
			afx_msg void OnAdd();
			afx_msg void OnDelete();
			afx_msg void OnEdit();
			virtual BOOL OnInitDialog();
			afx_msg void OnDestroy();
			//}}AFX_MSG

			afx_msg void OnAddValve ();
			afx_msg void OnDblclkHeads();
			afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);		
			afx_msg void OnClickHeads(NMHDR* pNMHDR, LRESULT* pResult);
			afx_msg void OnSelchangeLine ();
			afx_msg void OnBeginLabelEditHeads (NMHDR* pNMHDR, LRESULT* pResult); 
			afx_msg void OnEndLabelEditHeads (NMHDR* pNMHDR, LRESULT* pResult); 
			afx_msg void OnBegindrag (NMHDR* pNMHDR, LRESULT* pResult);
			afx_msg void OnDblclkNewHeads(NMHDR* pNMHDR, LRESULT* pResult);
			afx_msg void OnSelChanged (NMHDR* pNMHDR, LRESULT* pResult);
			afx_msg void OnSelChanging (NMHDR* pNMHDR, LRESULT* pResult);
			afx_msg void OnSync ();

			LRESULT OnSyncRemoteHead (WPARAM wParam, LPARAM lParam);
			LRESULT OnSyncComplete (WPARAM wParam, LPARAM lParam);

			DECLARE_MESSAGE_MAP()
		};
	}; //namespace HeadConfigDlg
}; // namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEADCONFIGDLG_H__CBC7B49D_E43B_4B10_93AF_42BC77E0B45F__INCLUDED_)
