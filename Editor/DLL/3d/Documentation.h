#if !defined(AFX_DOCUMENTATION_H__CB305E03_E3C6_47BA_B703_526A339257C4__INCLUDED_)
#define AFX_DOCUMENTATION_H__CB305E03_E3C6_47BA_B703_526A339257C4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Documentation.h : header file
//

#include "3dApi.h"

namespace Foxjet3d
{
	/////////////////////////////////////////////////////////////////////////////
	// CDocumentationPage dialog

	class _3D_API CDocumentationPage : public CPropertyPage
	{
		DECLARE_DYNCREATE(CDocumentationPage)

	// Construction
	public:
		CDocumentationPage();
		~CDocumentationPage();

	// Dialog Data
		//{{AFX_DATA(CDocumentationPage)
			// NOTE - ClassWizard will add data members here.
			//    DO NOT EDIT what you see in these blocks of generated code !
		//}}AFX_DATA


	// Overrides
		// ClassWizard generate virtual function overrides
		//{{AFX_VIRTUAL(CDocumentationPage)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		// Generated message map functions
		//{{AFX_MSG(CDocumentationPage)
		virtual BOOL OnInitDialog();
		//}}AFX_MSG

		afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
		HBRUSH m_hbrDlg;

		DECLARE_MESSAGE_MAP()

	};
}; //namespace Foxjet3d


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DOCUMENTATION_H__CB305E03_E3C6_47BA_B703_526A339257C4__INCLUDED_)
