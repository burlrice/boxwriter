// Box.cpp: implementation of the CBox class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "3d.h"
#include "Box.h"
#include "DxView.h"
#include "Color.h"
#include "List.h"
#include "Compare.h"
#include "TemplExt.h"
#include "Debug.h"
#include "Coord.h"
#include "BaseDoc.h"
#include "BaseDoc.h"
#include "Color.h"
#include "Utils.h"
#include "Element.h"
#include "Resource.h"
#include "AppVer.h"
#include "ximage.h"
#include "Parse.h"
#include "Extern.h"
#include "ValvePropDlg.h"
#include "WinMsg.h"

#ifdef __WINPRINTER__
	#include "LabelElement.h"
#endif //__WINPRINTER__

#include <math.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;
using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace Color;
using namespace FoxjetDocument;
using namespace ListGlobals;
using namespace Element;
using namespace Panel;
using namespace Box;
using namespace DxParams;

////////////////////////////////////////////////////////////////////////////////
class CPanelIndex
{
public:
	CPanelIndex (int nRel = 0, int nAbs = 0) 
	:	m_nRelative (nRel), 
		m_nAbsolute (nAbs) 
	{
	}

	int m_nRelative;
	int m_nAbsolute;
};

////////////////////////////////////////////////////////////////////////////////

const DB::COrientation CBox::m_orientCalc (0, ROT_0);
const int CBox::m_nHeadBoundary = 5;
const UINT CBox::m_nPoints [6][4] = 
{
	{ 0, 1, 2, 3 },	// face 1 // _FRONT
	{ 1, 5, 6, 2 },	// face 2 // _RIGHT
	{ 5, 4, 7, 6 },	// face 3 // _BACK
	{ 4, 0, 3, 7 },	// face 4 // _LEFT
	{ 4, 5, 1, 0 },	// face 5 // _TOP
	{ 6, 7, 3, 2 },	// face 6 // _BOTTOM
};

IMPLEMENT_DYNAMIC (CBox, CDxObject)

CBox::CBox (FoxjetDatabase::COdbcDatabase & db)
:	m_db (db),
	m_transform (1, ROT_0),
	m_pParams (NULL),
	m_pbmpFace (NULL),
	m_pbmpBox (NULL),
	m_bMonochrome (false)
{
	m_box.m_lID		= -1;
	m_box.m_lWidth	= 0;
	m_box.m_lLength	= 0;
	m_box.m_lHeight	= 0;
	m_line.m_lID	= -1;

	ASSERT (ARRAYSIZE (m_pPanels) == ARRAYSIZE (m_pBmpPanel));

	for (int i = 0; i < ARRAYSIZE (m_pPanels); i++) {
		m_pPanels [i]			= NULL;
		m_pBmpPanel [i]			= NULL;
	}
}

CBox::CBox (const CBox & rhs)
:	m_db (rhs.m_db),
	m_box (rhs.m_box),
	m_line (rhs.m_line),
	m_transform (rhs.m_transform),
	m_pParams (NULL),
	m_pbmpFace (NULL),
	m_pbmpBox (NULL),
	m_bMonochrome (false)
{
	ASSERT (ARRAYSIZE (m_pPanels) == ARRAYSIZE (m_pBmpPanel));

	for (int i = 0; i < ARRAYSIZE (m_pPanels); i++) {
		m_pPanels [i]			= NULL;
		m_pBmpPanel [i]			= NULL;
	}
}

CBox & CBox::operator = (const CBox & rhs)
{
	if (this != &rhs) {
		Free (true);
		m_transform	= rhs.m_transform;

		if (m_line != rhs.m_line)
			SetLine (rhs.m_line);
	
		if (m_box != rhs.m_box) 
			SetBox (rhs.m_box);
	}

	return * this;
}

CBox::~CBox()
{
	Free ();
}

bool CBox::IsCreated () const 
{
	return HasPanels ();
}

bool CBox::Create (CDxView & view)
{
	CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;
	bool bGetPanels = GetPanelRecords (m_db, m_line.m_lID, vPanels);

	Free ();

	if (bGetPanels && vPanels.GetSize () == 6) {
		for (int i = 0; i < vPanels.GetSize (); i++) {
			int nIndex = vPanels [i].m_nNumber - 1;
			FACE face = (FACE)i;

			ASSERT (nIndex >= 0 && nIndex < 6);
			
			CPanel * p = m_pPanels [nIndex] = new CPanel (* this, face, vPanels [i]);
			ASSERT (p);

			p->LoadImage (view, GetDB ());
			VERIFY (p->Create (view));
		}

		return true;
	}

	return false;
}

void CBox::ClearHT () const
{
	CBox * pNonconstThis = (CBox *)(DWORD)this;

	CBoxParams ** pParams = (CBoxParams **)(DWORD)&m_pParams;

	if (* pParams) {
		delete (* pParams);
		(* pParams) = NULL;
	}

	for (int i = 0; i < pNonconstThis->m_vrgnHT.GetSize (); i++) 
		if (CRgn * prgn = pNonconstThis->m_vrgnHT [i])
			delete prgn;

	pNonconstThis->m_vrgnHT.RemoveAll ();
}

void CBox::DeleteBoxCache ()
{
	//LOCK (m_cs);

	if (m_pbmpBox) {
		delete m_pbmpBox;
		m_pbmpBox = NULL;
	}
}

void CBox::Free (bool bKeepSurfaces)
{
	for (int i = 0; i < 6; i++) {
		if (m_pPanels [i]) {
			delete m_pPanels [i];
			m_pPanels [i] = NULL;
		}
	}

	DeleteBoxCache ();
	DeleteFaceCache ();
	DeletePlgCache ();
	ClearHT ();
}

bool Foxjet3d::operator == (const Foxjet3d::Box::CBox & lhs, const Foxjet3d::Box::CBox & rhs)
{
	return 
		lhs.GetBox ()		== rhs.GetBox () &&
		lhs.GetLine ()		== rhs.GetLine () &&
		lhs.GetTransform ()	== rhs.GetTransform ();
}

bool Foxjet3d::operator != (const Foxjet3d::Box::CBox & lhs, const Foxjet3d::Box::CBox & rhs)
{
	return !(lhs == rhs);
}

const DB::COrientation & CBox::GetTransform () const
{
	return m_transform;
}

UINT CBox::GetTransformFace () const
{
	return m_transform.m_nPanel;
}

DB::ORIENTATION CBox::GetTransformOrientation () const
{
	return m_transform.m_orient;
}

const DB::BOXSTRUCT & CBox::GetBox () const
{
	return m_box;
}

bool CBox::GetImage (CArray <FoxjetDatabase::IMAGESTRUCT, FoxjetDatabase::IMAGESTRUCT &> & v, UINT nPanel, FoxjetDatabase::IMAGESTRUCT & img)
{
	for (int i = 0; i < v.GetSize (); i++) {
		IMAGESTRUCT cur = v [i];

		if (cur.m_nPanel == nPanel) {
			img = v [i];
			return true;
		}
	}

	return false;
}

bool CBox::SetBox (const DB::BOXSTRUCT & box)
{
	bool bReload = m_box != box;

	if (!bReload) {
		const IMAGESTRUCT * pimgOld [6] = { NULL };
		CArray <IMAGESTRUCT, IMAGESTRUCT &> vimgNew, vimgOld;

		GetPanelImages (pimgOld);
		GetImageRecords (GetDB (), box.m_lID, vimgNew);

		if (vimgNew.GetSize ()) {
			for (int i = 0; i < ARRAYSIZE (pimgOld); i++)
				if (const IMAGESTRUCT * p = pimgOld [i])
					vimgOld.Add (IMAGESTRUCT (* p));

			for (UINT nPanel = 1; nPanel <= 6; nPanel++) {
				IMAGESTRUCT imgNew, imgOld;

				bool bOld = GetImage (vimgOld, nPanel, imgOld);
				bool bNew = GetImage (vimgNew, nPanel, imgNew);

				if (bOld ^ bNew) {
					bReload = true;
					break;
				}
				else if (bOld && bNew) { 
					if (imgOld.m_strImage.CompareNoCase (imgNew.m_strImage) != 0) {
						TRACEF (imgOld.m_strImage);
						TRACEF (imgNew.m_strImage);

						bReload = true;
						break;
					}
				}
			}
		}
	}

	m_box = box;

	if (bReload) 
		Reload ();

	return true;
}

const DB::LINESTRUCT & CBox::GetLine () const
{
	return m_line;
}

bool CBox::SetLine (const DB::LINESTRUCT & line)
{
	m_line = line;
	return true;
}

bool CBox::SetTransform(UINT nFace, ORIENTATION orient)
{
	return SetTransform (COrientation (nFace, orient));
}

bool CBox::SetTransform(const COrientation & transform)
{
	if (transform.m_nPanel >= 1 && transform.m_nPanel <= 6) {
		m_transform = transform;
		DeletePlgCache ();
		DeleteFaceCache ();

		return true;
	}

	return true;
}

DB::COrientation CBox::MapAbsolutePanel (const COrientation & relative, Box::ROTATION r)
{
	typedef COrientation _R;
	static const _R faceTable [6][4][4] =  
	{	{	//1	LEFT			RIGHT			UP				DOWN				
			{	_R (2, ROT_0),	_R (4, ROT_0),	_R (6, ROT_180),_R (5, ROT_0)	}, // ROT_0
			{	_R (5, ROT_90),	_R (6, ROT_270),_R (2, ROT_90),	_R (4, ROT_90)	}, // ROT_90
			{	_R (4, ROT_180),_R (2, ROT_180),_R (5, ROT_180),_R (6, ROT_0)	}, // ROT_180
			{	_R (6, ROT_90),	_R (5, ROT_270),_R (4, ROT_270),_R (2, ROT_270)	}, // ROT_270
		},{	//2	LEFT			RIGHT			UP				DOWN				
			{	_R (3, ROT_0),	_R (1, ROT_0),	_R (6, ROT_90),	_R (5, ROT_90)	}, // ROT_0
			{	_R (5, ROT_180),_R (6, ROT_180),_R (3, ROT_90),	_R (1, ROT_90)	}, // ROT_90
			{	_R (1, ROT_180),_R (3, ROT_180),_R (5, ROT_270),_R (6, ROT_270)	}, // ROT_180
			{	_R (6, ROT_0),	_R (5, ROT_0),	_R (1, ROT_270),_R (3, ROT_270)	}, // ROT_270
		},{	//3	LEFT			RIGHT			UP				DOWN				
			{	_R (4, ROT_0),	_R (2, ROT_0),	_R (6, ROT_0),	_R (5, ROT_180)	}, // ROT_0
			{	_R (5, ROT_270),_R (6, ROT_90),	_R (4, ROT_90),	_R (2, ROT_90)	},  // ROT_90
			{	_R (2, ROT_180),_R (4, ROT_180),_R (5, ROT_0),	_R (6, ROT_180)	}, // ROT_180
			{	_R (6, ROT_270),_R (5, ROT_90),	_R (2, ROT_270),_R (4, ROT_270)	}, // ROT_270
		},{	//4	LEFT			RIGHT			UP				DOWN				
			{	_R (1, ROT_0),	_R (3, ROT_0),	_R (6, ROT_270),_R (5, ROT_270)	}, // ROT_0
			{	_R (5, ROT_0),	_R (6, ROT_0),	_R (1, ROT_90),	_R (3, ROT_90)	},  // ROT_90
			{	_R (3, ROT_180),_R (1, ROT_180),_R (5, ROT_90),	_R (6, ROT_90)	}, // ROT_180
			{	_R (6, ROT_180),_R (5, ROT_180),_R (3, ROT_270),_R (1, ROT_270)	}, // ROT_270
		},{	//5	LEFT			RIGHT			UP				DOWN				
			{	_R (2, ROT_270),_R (4, ROT_90),	_R (1, ROT_0),	_R (3, ROT_180)	}, // ROT_0
			{	_R (3, ROT_270),_R (1, ROT_90),	_R (2, ROT_0),	_R (4, ROT_180)	}, // ROT_90
			{	_R (4, ROT_270),_R (2, ROT_90),	_R (3, ROT_0),	_R (1, ROT_180)	}, // ROT_180
			{	_R (1, ROT_270),_R (3, ROT_90),	_R (4, ROT_0),	_R (2, ROT_180)	}, // ROT_270
		},{	//6	LEFT			RIGHT			UP				DOWN				
			{	_R (4, ROT_90),	_R (2, ROT_270),_R (1, ROT_180),_R (3, ROT_0)	}, // ROT_0
			{	_R (3, ROT_90),	_R (1, ROT_270),_R (4, ROT_180),_R (2, ROT_0)	}, // ROT_90
			{	_R (2, ROT_90),	_R (4, ROT_270),_R (3, ROT_180),_R (1, ROT_0)	}, // ROT_180
			{	_R (1, ROT_90),	_R (3, ROT_270),_R (2, ROT_180),_R (4, ROT_0)	}, // ROT_270
		}
	};
	int nPanelIndex = relative.m_nPanel - 1;
	int nOrientIndex = relative.m_orient;

	ASSERT (nPanelIndex >= 0 && nPanelIndex < 6);
	ASSERT (nOrientIndex >= 0 && nOrientIndex < 4);
	ASSERT (r >= 0 && r < 4);

	return faceTable [nPanelIndex][nOrientIndex][r];
}

ORIENTATION CBox::TranslateOrientation (CBox & box,
										const COrientation & panel,
										const COrientation & relative)
{
	ASSERT (box.HasPanels ());

	const CPanel & absolute = * box.m_pPanels [panel.m_nPanel - 1];
	ORIENTATION orient = panel.m_orient;

	if (absolute->m_orientation != ROT_0) {
		const COrientation oldTransform = box.GetTransform ();
		const ORIENTATION table [] = { ROT_0, ROT_90, ROT_180, ROT_270 };
//		COrientation face (absolute.GetMembers ());

		for (int i = 0; i < 4; i++) {
			box.SetTransform (oldTransform.m_nPanel, table [i]);
			COrientation face (absolute.GetMembers ());
			
			face.m_orient = (ORIENTATION)((face.m_orient + i) % 4);

			if (face == panel) {
				orient = table [i];
				break;
			}
		}

		box.SetTransform (oldTransform);
	}

	return orient;
}

bool CBox::RotateTransform (UINT nFace, Box::ROTATION r)
{
	if ((nFace >= 1 && nFace <= 6) && (r >= LEFT && r <= CCL)) {
		static const ROTATION rotations [6][6] = 
		{
			//LEFT	RIGHT	UP		DOWN	CL		CCL	
			{ LEFT,	RIGHT,	UP,		DOWN,	CL,		CCL		}, // 1
			{ LEFT,	RIGHT,	CCL,	CL,		UP,		DOWN	}, // 2
			{ LEFT,	RIGHT,	DOWN,	UP,		CCL,	CL		}, // 3
			{ LEFT,	RIGHT,	CL,		CCL,	DOWN,	UP		}, // 4
			{ CCL,	CL,		UP,		DOWN,	LEFT,	RIGHT	}, // 5
			{ CCL,	CL,		DOWN,	UP,		RIGHT,	LEFT	}, // 6
		};
		// row 6 assumes panel 6's default state is upside down

		if (RotateTransformByFace (rotations [nFace - 1][r])) {
			Invalidate (-1);
			return true;
		}
	}

	return false;
}

bool CBox::RotateTransformByFace (Box::ROTATION r)
{
//	const COrientation oldFace = MapPanel (1, m_transform);
	const COrientation oldFace = MapPanel (1);

	switch (r) {
	case DOWN: 
	case UP: 
	case RIGHT: 
	case LEFT: 
		{
			COrientation newFace = MapAbsolutePanel (oldFace, r);
			ORIENTATION orient = TranslateOrientation (* this, newFace, oldFace);
			return SetTransform (newFace.m_nPanel, orient);
//			COrientation orient = TranslateOrientation (* this, newFace, oldFace);
//			return SetTransform (orient);
		}
	case CL:
		{
			COrientation newFace = oldFace;
			newFace.m_orient = (ORIENTATION)((oldFace.m_orient + 1) % 4);
			ORIENTATION orient = TranslateOrientation (* this, newFace, oldFace);
			return SetTransform (newFace.m_nPanel, orient);
//			COrientation orient = TranslateOrientation (* this, newFace, oldFace);
//			return SetTransform (orient);
		}
	case CCL:
		{
			COrientation newFace = oldFace;
			newFace.m_orient = (ORIENTATION)((oldFace.m_orient + 3) % 4);
			ORIENTATION orient = TranslateOrientation (* this, newFace, oldFace);
			return SetTransform (newFace.m_nPanel, orient);
//			COrientation orient = TranslateOrientation (* this, newFace, oldFace);
//			return SetTransform (orient);
		}
	}

	return false;
}

DB::COrientation CBox::MapPanel (UINT nPanel) const
{
	COrientation map = MapPanel (nPanel, m_transform);

	if (HasPanels ()) {
		const PANELSTRUCT & panel = m_pPanels [map.m_nPanel - 1]->GetMembers ();
		int nOrient = (panel.m_orientation + map.m_orient) % 4;

		map.m_orient = (ORIENTATION)nOrient;
	}

	return map;
}

void CBox::CalcPanelIndicies (UINT nFace, UINT nPanels [6], ORIENTATION orient)
{
	static const UINT nSides [6] = { 6, 4, 3, 2, 5, 1 };

	for (int i = 0; i < 6; i++) 
		nPanels [i] = MapPanel (nSides [i], COrientation (nFace, orient)).m_nPanel;
}

DB::COrientation CBox::MapPanel (UINT nPanel, const COrientation & o) 
{
	return FoxjetDatabase::MapPanel (nPanel, o);
}

UINT CBox::MapPanelNumber (Box::PANEL p)
{
	static const UINT nTable [6] = { 1, 3, 4, 2, 5, 6 };
	return nTable [p];
}

Box::PANEL CBox::MapPanelNumber (UINT nPanel)
{
	ASSERT (nPanel >= 1 && nPanel <= 6);
	static const PANEL table [6] = { _FRONT, _RIGHT, _BACK, _LEFT, _TOP, _BOTTOM };
	return table [nPanel - 1];
}

Box::CHit CBox::HitTest (const CPoint & ptTest, CBoxParams & p, DWORD dwType) const
{
	CHit hit;
	CBoundary active;
	CBox * pNonconstThis = (CBox *)(DWORD)this;
	const CPoint ptCrosshairs = p.m_crosshairs.m_pt;

	if (p.m_nPanel < 1 || p.m_nPanel > 6 || !HasPanels ()) 
		return hit;

	if (!m_pParams || Compare (* m_pParams, p) || m_pParams->m_lHeadID != p.m_lHeadID) {
		CBoxParams ** pParams = (CBoxParams **)(DWORD)&m_pParams;

		if (* pParams) {
			delete (* pParams);
			(* pParams) = NULL;
		}

		(* pParams) = new CBoxParams (p);

		for (int i = 0; i < pNonconstThis->m_vrgnHT.GetSize (); i++) 
			if (CRgn * prgn = pNonconstThis->m_vrgnHT [i])
				delete prgn;

		pNonconstThis->m_vrgnHT.RemoveAll ();
	}

	p.GetActiveHead (* this, active);

	if ((HT_PANEL | HT_HEAD) & dwType) {
		CPoint pt [8];
		const COrientation face = MapPanel (p.m_nPanel);
		const CSize3 size = GetSize (face.m_nPanel, active, face.m_orient);
		UINT nPanels [6] = { 0 };
		const UINT nPanelIndexMap [3] = 
		{ 
			5,								// _FRONT
			p.m_dRotation [0] > 0 ? 3 : 1,	// _RIGHT 
			p.m_dRotation [1] > 0 ? 4 : 0,	// _TOP
		};
		static const UINT nTable [3] = 
		{ 
			MapPanelNumber (_FRONT),
			MapPanelNumber (p.m_dRotation [0] > 0 ? _RIGHT : _LEFT),
			MapPanelNumber (p.m_dRotation [1] > 0 ? _TOP : _BOTTOM),
		};
		CBoundary workingHead;

		GetWorkingHead (p, workingHead);
		double dXStretch = CElement::CalcXStretch (active.m_card, workingHead.m_card);

		CalcPoints (pt, size, p.m_dRotation [0], p.m_dRotation [1], GetPos (), p.m_rZoom);
		CalcPanelIndicies (p.m_nPanel, nPanels);

		// stretch to account for multiple res
		for (int nPoint = 0; nPoint < sizeof (pt) / sizeof (pt [0]); nPoint++)
			pt [nPoint].x = (int)((double)pt [nPoint].x * dXStretch);

		COrder order [3] = 
		{ 
			COrder (face,				pt), // _FRONT
			COrder (nTable [1], face,	pt), // _RIGHT
			COrder (nTable [2], face,	pt), // _TOP
		};

		if (!pNonconstThis->m_vrgnHT.GetSize ()) {
			for (int i = 0; i < 3; i++) {
				CRgn * p = new CRgn ();

				VERIFY (p->CreatePolygonRgn (order [i].m_pt, 4, WINDING));
				pNonconstThis->m_vrgnHT.Add (p);
			}
		}

		ASSERT (m_vrgnHT.GetSize () == 3);

		for (int i = 0; i < 3; i++) {
			CRgn & rgn = * m_vrgnHT [i];

			if (rgn.PtInRegion (ptTest)) {
				UINT nPanel = nPanels [nPanelIndexMap [i]];
				
				hit.m_nPanel = nPanel;

				if (HT_HEAD & dwType) {
					if (nPanel == p.m_nPanel) {
						const CPanel * pPanel = NULL;

						VERIFY (GetPanel (nPanel, &pPanel));
						ASSERT (pPanel);

						for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) {
							const CBoundary & head = pPanel->m_vBounds [i];

							if (pPanel->GetMembers ().m_lID == head.GetPanelID ()) { 
								CRect rc = GetHeadRect (head, p);

								if (rc.PtInRect (ptTest)) {
									hit.m_vHeadID.Add (head.GetID ());

									if (HT_SETCROSSHAIRS & dwType) {
										if (hit.m_vHeadID.GetSize ()) {
											ULONG lHeadID = hit.m_vHeadID [hit.m_vHeadID.GetSize () - 1];

											if (lHeadID == active.GetID ())
												p.m_crosshairs.m_pt = CPoint (ptTest - rc.TopLeft ());
										}
									}
								}
							}
						}
					}
				}

				break;
			}
		}
	}

	if (HT_ELEMENT & dwType) {
		CBoundary workingHead;

		GetWorkingHead (p, workingHead);

		for (int i = 0; i < p.m_task.GetMessageCount (); i++) {
			if (CMessage * pMsg = p.m_task.GetMessage (i)) {
				CBoundary & head = pMsg->m_list.m_bounds;
				const CPanel * pFace = m_pPanels [p.m_nPanel - 1];

				// only consider the face panel & active head
				if (head.GetPanelID () == pFace->GetMembers ().m_lID && head.GetID () == active.GetID ()) {
					if (!IsLinked (head)) {
						CRect rcHead = GetHeadRect (head, p);
						int nIndex = pMsg->m_list.HitTest (ptTest - rcHead.TopLeft (), p.m_rZoom, workingHead);

						if (nIndex != -1) {
							CElement & e = pMsg->m_list [nIndex];
							CRect rcTracker = (e.GetWindowRect (head.m_card, workingHead.m_card) * p.m_rZoom) + rcHead.TopLeft ();
							CRect rcSave;

							e.GetTrackerRect (rcSave);
							e.SetTrackerRect (rcTracker);

							hit.m_element.m_lID			= e.GetElement ()->GetID ();
							hit.m_element.m_pElement	= &e;
							hit.m_element.m_pList		= &pMsg->m_list;

							p.m_crosshairs.m_pt = ptCrosshairs;
							
							if ((hit.m_element.m_nType = e.HitTest (ptTest)) != CRectTracker::hitNothing)
								hit.m_element.m_ptTracker = (CPoint)(ptTest - rcTracker.TopLeft ());
							
							e.SetTrackerRect (rcSave);

							return hit;
						}
					}
				}
			}
		}
	}

	return hit;
}

void CBox::GetPoints (CBoxParams & p, CPoint pt [8]) const 
{
	CBoundary head;
	
	if (p.m_nPanel >= 1 && p.m_nPanel <= 6) {
		if (!GetWorkingHead (p, head))
			p.GetActiveHead (* this, head);
		
		const COrientation face = MapPanel (p.m_nPanel);
		const CSize3 size = GetSize (face.m_nPanel, head, face.m_orient);

		CalcPoints (pt, size, p.m_dRotation [0], p.m_dRotation [1], GetPos (), p.m_rZoom);
	}
}

void CBox::DeleteFaceCache ()
{
	//LOCK (m_cs);

	if (m_pbmpFace) {
		delete m_pbmpFace;
		m_pbmpFace = NULL;
	}
}

void CBox::DeletePlgCache ()
{
	for (int i = 0; i < ARRAYSIZE (m_pBmpPanel); i++) {
		if (m_pBmpPanel [i]) {
			delete m_pBmpPanel [i];
			m_pBmpPanel [i] = NULL;
		}
	}
}

void CBox::DrawBounds (CDC & dc, const CRect & rc, double dZoom, CBoxParams & params, CPanel & face)
{
	CPoint ptOrg			= dc.GetWindowOrg ();
	COLORREF rgbActive		= ::GetSysColor (COLOR_HIGHLIGHT);
	COLORREF rgbInactive	= ::GetSysColor (COLOR_3DLIGHT);

	for (int i = 0; i < face.m_vBounds.GetSize (); i++) {
		CBoundary & b = face.m_vBounds [i];
		const CRect rcHead = GetHeadRect (b, params, true);

		if (params.m_lHeadID == b.GetID ()) {
			dc.FrameRect (rcHead - ptOrg, &CBrush (rgbActive));
			DrawCrosshairs (dc, params, rcHead);
		}
		else 
			dc.FrameRect (rcHead - ptOrg, &CBrush (rgbInactive));

		if (IsValveHead (b)) {
			CBrush active (rgbActive);
			CBrush inactive (rgbInactive);

			for (int nHead = 0; nHead < b.m_card.m_vValve.GetSize (); nHead++) {
				for (int i = 0; i < b.m_card.m_vValve.GetSize (); i++) {
					VALVESTRUCT & valve = b.m_card.m_vValve [i];
					CRect rc = GetValveRect (b, rcHead, valve.m_lID);

					rc -= ptOrg;
					dc.FrameRect (rc, params.m_lValveID == valve.m_lID ? &active : &inactive); 
				}
			}
		}
	}
}

void CBox::Blt (CDC & dc, const CRect & rc, double dZoom, CBoxParams & params)
{
	using namespace FoxjetDocument;
	using namespace Color;

	if (!IsCreated ())
		return;

	//LOCK (m_cs);

	ASSERT (params.m_nPanel >= 1 && params.m_nPanel <= 6);

	CBoundary workingHead;
	CPanel * pFace = m_pPanels [params.m_nPanel - 1];
	CPoint ptOrg = dc.GetWindowOrg ();
	COLORREF rgbActive = ::GetSysColor (COLOR_HIGHLIGHT);
	COLORREF rgbInactive = ::GetSysColor (COLOR_3DLIGHT);

	GetWorkingHead (params, workingHead);

	if (params.m_bOutlineOnly) {
		DeleteBoxCache ();
		DrawBoxOutline (dc, params);
		
		if (pFace)
			DrawBounds (dc, rc, dZoom, params, * pFace);

		return;
	}

	if (!pFace)
		return;

	CDC dcMem;
	VERIFY (dcMem.CreateCompatibleDC (&dc));
	CBitmap * pBmp = dcMem.SelectObject (m_pbmpBox);
	
	::BitBlt (dc, 0, 0, rc.Width (), rc.Height (), dcMem, rc.left, rc.top, SRCCOPY);
	dcMem.SelectObject (pBmp);

	// head bounds & element selection rects must be drawn here because they might not be 
	// completely bounded by the face's bitmap (thus, we draw into dc)

	// draw the elements first
	for (int nHead = 0; nHead < pFace->m_vBounds.GetSize (); nHead++) {
		CBoundary & head = pFace->m_vBounds [nHead];
		CMessage * pMsg = params.m_task.GetMessageByHead (head.GetID ());
		const CRect rcHead = GetHeadRect (head, params); 

		if (!pMsg)
			break; 

		for (int j = 0; j < pMsg->m_list.GetSize (); j++) {
			CEditorElementList & list = pMsg->m_list;
			CElement & e = list [j];
			CRect rc = (e.GetWindowRect (pMsg->m_list.GetHead (), workingHead) * params.m_rZoom) 
				+ rcHead.TopLeft ();

			if (defElements.GetOverlappedWarning ()) {
				Element::CPairArray vOverlap = list.GetOverlapping (&e, workingHead);

				if (vOverlap.GetSize ()) {
					CDC dcWarning;
					CBitmap bmpWarning;
					CRect rcWarning = rc - ptOrg;

					dcWarning.CreateCompatibleDC (&dc);
					bmpWarning.CreateCompatibleBitmap (&dc, rc.Width (), rc.Height ());

					CBitmap * pWarning = dcWarning.SelectObject (&bmpWarning);

					dcWarning.FillRect (CRect (0, 0, rc.Width (), rc.Height ()), &CBrush (rgbRed));
					dc.BitBlt (rcWarning.left, rcWarning.top, rcWarning.Width (), rcWarning.Height (), &dcWarning, 0, 0, SRCAND);

					dcWarning.SelectObject (pWarning);
				}
			}

			e.DrawTracker (dc, rc, ptOrg, head);
		}
	}

	DrawBoxOutline (dc, params);


	// draw the inactive head boundaries (if requested)
	for (int nHead = 0; nHead < pFace->m_vBounds.GetSize (); nHead++) {
		CBoundary & head = pFace->m_vBounds [nHead];
		const CRect rcHead = GetHeadRect (head, params, true);

		if (params.m_bHeads && params.m_lHeadID != head.GetID ())
			DrawHatchedRect (dc, rcHead - ptOrg, ENABLED, rgbInactive, Color::rgbWhite, m_nHeadBoundary, m_nHeadBoundary);
	}


	// draw the active head boundaries (if requested).  this forces the active head
	// to be drawn on top
	for (int nHead = 0; nHead < pFace->m_vBounds.GetSize (); nHead++) {
		CBoundary & head = pFace->m_vBounds [nHead];
		const CRect rcHead = GetHeadRect (head, params, true);

		if (params.m_lHeadID == head.GetID ()) {
			if (IsValveHead (head)) {
				CBrush active (rgbActive);
				CBrush inactive (rgbInactive);

				for (int i = 0; i < head.m_card.m_vValve.GetSize (); i++) {
					VALVESTRUCT & valve = head.m_card.m_vValve [i];
					CRect rc = GetValveRect (head, rcHead, valve.m_lID);

					rc -= ptOrg;
					dc.FrameRect (rc, (params.m_lHeadID == head.GetID ()) ? &active : &inactive); 
				}

				/*
				#ifdef _DEBUG
				{ // TODO: rem
					COLORREF rgb [] = 
					{
						rgbDarkGray,
						rgbBlue,
						rgbGreen,
						rgbRed,
					};
					CRect rc = GetHeadRect (head, params, true) - ptOrg;
					CPen pen (PS_SOLID, 1, rgb [head.GetID () % ARRAYSIZE (rgb)]);
					CPen * pOld = dc.SelectObject (&pen);

					{ // horizontal
						double x [] = { rc.left, rc.right };
						double y = rc.top;
						double d = (double)head.Height () / (double)(head.m_card.GetChannels () - 1);

						for (int i = 0; i < head.m_card.GetChannels (); i++) {
							dc.MoveTo (CPoint ((int)x [0], (int)y));
							dc.LineTo (CPoint ((int)x [1], (int)y));

							y += (d * dZoom);
						}
					}

					{ // vertical
						double y [] = { rc.top, rc.bottom };
						double x = rc.left;
						double d = (double)dc.GetDeviceCaps (LOGPIXELSY) / (double)head.m_card.GetRes ();

						while (x < (double)rc.right) {
							dc.MoveTo (CPoint ((int)x, (int)y [0]));
							dc.LineTo (CPoint ((int)x, (int)y [1]));

							x += (d * dZoom);
						}
					}

					dc.SelectObject (pOld);
				}
				#endif //_DEBUG
				*/
			}

			DrawHatchedRect (dc, rcHead - ptOrg, ENABLED, rgbActive, Color::rgbWhite, m_nHeadBoundary, m_nHeadBoundary);
			DrawCrosshairs (dc, params, rcHead);
		}
	}
}

CRect CBox::GetValveRect (Panel::CBoundary & head, const CRect & rcHead, ULONG lValveID) 
{
	if (IsValveHead (head.m_card)) {
		double dDPI = (double)rcHead.Height () / (double)head.m_card.GetChannels ();
		int yOffset = 0;

		for (int i = 0; i < head.m_card.m_vValve.GetSize (); i++) {
			VALVESTRUCT & valve = head.m_card.m_vValve [i];
			int nChannels = GetValveChannels (valve.m_type);
			int y = (int)((double)yOffset * dDPI);
			int cy = (int)((double)nChannels * dDPI);
			CRect rc (CPoint (0, y), CSize (rcHead.Width (), cy + 1));
			
			rc += rcHead.TopLeft ();

			if (valve.m_lID == lValveID)
				return rc;

			yOffset += nChannels;
		}
	}

	return CRect (0, 0, 0, 0);
}

void CBox::BltSides (CDC & dc, CBoxParams & params, COrder order [6], CPoint ptBox [8])
{	
	DECLARETRACECOUNT (100);

	CArray <CPanelIndex, CPanelIndex &> vIndex;
	COrder orderAbs [6];
	CBoundary workingHead;
	CDC dcMem;

	dcMem.CreateCompatibleDC (&dc);

	MARKTRACECOUNT ();
	GetWorkingHead (params, workingHead);

	if (params.m_uBlend != 255) {
		if (params.m_dRotation [1] != 0.0)	
			vIndex.Add (CPanelIndex (0, 0)); // bottom

		if (params.m_dRotation [0] != 0.0)	
			vIndex.Add (CPanelIndex (1, 1)); // left

		vIndex.Add (CPanelIndex (2, 2)); // back
	}

	if (params.m_dRotation [0] != 0.0)	
		vIndex.Add (CPanelIndex (3, 3)); // right 
	
	if (params.m_dRotation [1] != 0.0)	
		vIndex.Add (CPanelIndex (4, 4)); // top

	MARKTRACECOUNT ();

	{
		CPanel * pFace = m_pPanels [params.m_nPanel - 1];
		CPoint pt [8];
		UINT nPanels [6];
		ORIENTATION rot = pFace->GetMembers ().m_orientation;

		if (params.m_nPanel == 6)
			rot = (ORIENTATION)((rot + 2) % 4);

		GetOrder (params, pt, orderAbs, nPanels, COrientation (params.m_nPanel, rot));
	}

	for (int nSide = 0; nSide < vIndex.GetSize (); nSide++) {
		const int nRelIndex = vIndex [nSide].m_nRelative;
		const int nAbsIndex = vIndex [nSide].m_nAbsolute;
		const COrder rotRel = order [nRelIndex];
		const int nPanel = rotRel;
		CPoint pt [4];
		CBitmap * pBmpPanel = NULL;
		CBitmap * pBmpPanelMask = NULL;

		ASSERT (nRelIndex >= 0 && nRelIndex < 6);
		ASSERT (nAbsIndex >= 0 && nAbsIndex < 6);
		ASSERT (nPanel >= 0 && nPanel < 6);

		const CPanel * pPanel = m_pPanels [nPanel];
		ASSERT (pPanel);

		CSize sizeMask (0, 0);

		for (int i = 0; i < 4; i++)
			pt [i] = order [nRelIndex].m_pt [i];

		CPoint ptTL (pt [0].x, pt [0].y);

		// calc top-left most point
		for (int i = 0; i < 4; i++) {
			ptTL.x = min (ptTL.x, pt [i].x);
			ptTL.y = min (ptTL.y, pt [i].y);
		}

		// offset to (0, 0)
		for (int i = 0; i < 4; i++) 
			pt [i] -= ptTL;

		// calc size
		for (int i = 0; i < 4; i++) {
			sizeMask.cx = max (sizeMask.cx, pt [i].x);
			sizeMask.cy = max (sizeMask.cy, pt [i].y);
		}

		CPoint ptPlg [] = { pt [0], pt [1], pt [3] };

		MARKTRACECOUNT ();
	
		if (!(pBmpPanel = m_pBmpPanel [nPanel])) {
			DWORD dw = Panel::CREATEIMAGE_RESIZE | Panel::CREATEIMAGE_DRAWELEMENTS;
			const int nAbsPanelIndex = orderAbs [nAbsIndex].m_nNumber - 1;
			CPanel * pAbsolute = NULL; 


			if (nAbsPanelIndex >= 0 && nAbsPanelIndex < ARRAYSIZE (m_pPanels))
				pAbsolute = m_pPanels [nAbsPanelIndex]; 
			else { ASSERT (0); }

			::AfxGetMainWnd ()->BeginWaitCursor ();

			const ORIENTATION rot = rotRel.m_rot;
			//const ORIENTATION rotAbs = orderAbs [nRelIndex].m_rot;
			ORIENTATION rotAbs = (ORIENTATION)((4 + (int)orderAbs [nRelIndex].m_rot - (int)pAbsolute->GetMembers ().m_orientation) % 4);

			MARKTRACECOUNT ();

			if (CBitmap * pImage = m_pPanels [nPanel]->CreateImage (dc, params, * this, rot, dw, pAbsolute, rotAbs)) {
				CDC dcPlg;
				DIBSECTION ds;

				ASSERT (pImage);

				MARKTRACECOUNT ();
 
				dcPlg.CreateCompatibleDC (&dc);

				MARKTRACECOUNT ();
 
				pBmpPanel = m_pBmpPanel [nPanel] = new CBitmap ();
	
				MARKTRACECOUNT ();

				pBmpPanel->CreateCompatibleBitmap (&dc, sizeMask.cx, sizeMask.cy);

				MARKTRACECOUNT ();

				CBitmap * pMem = dcMem.SelectObject (pImage);
				CBitmap * pPlg = dcPlg.SelectObject (pBmpPanel);

				::ZeroMemory (&ds, sizeof (ds));
				VERIFY (pImage->GetObject (sizeof (ds), &ds));
	
				MARKTRACECOUNT ();
	 
				::BitBlt (dcPlg, 0, 0, sizeMask.cx, sizeMask.cy, NULL, 0, 0, WHITENESS);

				// PlgBlt appears to be faster for small images than CxImage::Skew
				dcPlg.PlgBlt (ptPlg, &dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, CBitmap (), 0, 0);

				/*
				{ // TODO: rem
					DECLARETRACECOUNT (20);
					CxImage img;
					CBitmap bmpTmp;
					CString str; 

					img.CreateFromHBITMAP (* pImage);			MARKTRACECOUNT ();
					img.Skew (0, DEG2RAD (45));					MARKTRACECOUNT ();
// 					img.Negative ();
					bmpTmp.Attach (img.MakeBitmap (dcMem));		MARKTRACECOUNT ();
					str.Format ("C:\\Temp\\Debug\\%s [Skew].bmp", pPanel->GetMembers ().m_strName); 
					SAVEBITMAP (bmpTmp, str); 

					TRACECOUNTARRAY ();
				}
				*/

	//			{ CString str; str.Format ("C:\\Temp\\Debug\\%s [CreateImage].bmp", pPanel->GetMembers ().m_strName); SAVEBITMAP (dcMem, str); } // TODO: rem
	//			{ CString str; str.Format ("C:\\Temp\\Debug\\%s [PlgBlt].bmp", pPanel->GetMembers ().m_strName); SAVEBITMAP (dcPlg, str); } // TODO: rem

				dcMem.SelectObject (pMem);
				dcPlg.SelectObject (pPlg);
				delete pImage;
			}

			MARKTRACECOUNT ();

			::AfxGetMainWnd ()->EndWaitCursor ();
		}

		MARKTRACECOUNT ();
		
		if (pBmpPanel) {
			CBitmap * pMem = dcMem.SelectObject (pBmpPanel);
			CPoint ptMaskBlt = ptTL; // TODO: rem // - ptScroll;

			MARKTRACECOUNT ();
	
			if (params.m_uBlend == 255) {
				::BitBlt (dc, ptMaskBlt.x, ptMaskBlt.y, sizeMask.cx, sizeMask.cy, dcMem, 0, 0, SRCAND);
				//::TransparentBlt (dc, ptMaskBlt.x, ptMaskBlt.y, sizeMask.cx, sizeMask.cy, dcMem, 0, 0, sizeMask.cx, sizeMask.cy, rgbWhite);
			}
			else {
				BLENDFUNCTION blend;

				blend.BlendOp				= AC_SRC_OVER;
				blend.AlphaFormat			= AC_SRC_OVER;
				blend.BlendFlags			= 0;
				blend.SourceConstantAlpha	= params.m_uBlend;

				BOOL bAlphaBlend = ::AlphaBlend (dc, ptMaskBlt.x, ptMaskBlt.y, sizeMask.cx, sizeMask.cy, 
					dcMem, 0, 0, sizeMask.cx, sizeMask.cy, blend);

				if (!bAlphaBlend)
					TRACEF (FORMATMESSAGE (::GetLastError ()));
			}

			dcMem.SelectObject (pMem);
		}
	}

	//TRACECOUNTARRAYMAX ();
}

void CBox::GetOrder (CBoxParams & params, CPoint pt [8], COrder order [6], UINT nPanels [6])

{
	const COrientation face = MapPanel (params.m_nPanel);

	GetOrder (params, pt, order, nPanels, face);
}

void CBox::GetOrder (CBoxParams & params, CPoint pt [8], COrder order [6], UINT nPanels [6], const COrientation & face)
{
	CBoundary head;
	
	params.GetActiveHead (* this, head);

	const CSize3 size = GetSize (face.m_nPanel, head, face.m_orient);

	GetPoints (params, pt);

	order [0] = COrder (6, face,	pt), // 0: _BOTTOM
	order [1] = COrder (4, face,	pt), // 1: _LEFT
	order [2] = COrder (3, face,	pt), // 2: _BACK
	order [3] = COrder (2, face,	pt), // 3: _RIGHT
	order [4] = COrder (5, face,	pt), // 4: _TOP
	order [5] = COrder (face,		pt), // 5: _FRONT

	CalcPanelIndicies (params.m_nPanel, nPanels);

	if (params.m_dRotation [0] < 0) {
		swap (order [1], order [3]);
		swap (nPanels [1], nPanels [3]);
	}
	if (params.m_dRotation [1] < 0) {
		swap (order [0], order [4]);
		swap (nPanels [0], nPanels [4]);
	}

}

void CBox::DrawBoxOutline (CDC & dc, Box::CBoxParams & params)
{
	CPoint pt [8];
	COrder order [6];
	UINT nPanels [6] = { 0 };
	int nLowerIndex = params.m_uBlend == 255 ? 3 : 0;
	CPoint ptScroll = CPoint (
		params.m_view.GetParent ()->GetScrollPos (SB_HORZ),
		params.m_view.GetParent ()->GetScrollPos (SB_VERT));

	GetOrder (params, pt, order, nPanels);

	CPen pen;
	
	pen.CreatePen (PS_SOLID, 1, rgbBlack);
	CPen * pPen = dc.SelectObject (&pen);

	for (int i = nLowerIndex; i < 6; i++) {
		dc.MoveTo ((CPoint)(order [i].m_pt [0] - ptScroll));

		for (int j = 1; j <= 4; j++) 
			dc.LineTo ((CPoint)(order [i].m_pt [j % 4] - ptScroll));
	}
}

CSize CBox::CalcViewSize (CBoxParams & p) const
{
	CPoint ptSides [8] = { CPoint (0, 0) };

	GetPoints (p, ptSides);
	const CPoint ptLeft = GetLeftmost (ptSides);
	const CPoint ptTop = GetTopmost (ptSides);

	int cx = abs (GetRightmost (ptSides).x - ptLeft.x);
	int cy = abs (ptTop.y - GetBottommost (ptSides).y);

	return CSize (ptLeft.x + cx, ptTop.y + cy);
}

REAL CBox::CalcAdjust (const CSize & viewSize, const CSize & surfaceSize)
{
	REAL rResult = 1.0;

	if (viewSize.cx > surfaceSize.cx ||
		viewSize.cy > surfaceSize.cy) 
	{
		REAL r [2] = 
		{
			(REAL)viewSize.cx / (REAL)surfaceSize.cx,
			(REAL)viewSize.cy / (REAL)surfaceSize.cy,
		};

		rResult = max (r [0], r [1]);
	}

	return rResult;
}

void CBox::CalcPoints (CPoint pt [8], const CSize3 & ext,  
					   double dHorzRotation, double dVertRotation,
					   const CPoint & ptOffset, double dZoom)
{
	double dWidth = ext.GetX ();
	double dTheta = dHorzRotation * (rPI / 180.0); 
	double dPhi	= dVertRotation * (rPI / 180.0);
	int x = (int)(dWidth * sin (dTheta));
	int y = (int)(dWidth * sin (dPhi));
	const CPoint ptAdjust = CPoint (x, -y) * dZoom;
	const CRect rc (0, 0, (int)ext.GetY (), (int)ext.GetZ ());

	pt [0] = (CPoint (rc.left,	rc.top)		+ ptOffset) * dZoom;	
	pt [1] = (CPoint (rc.right,	rc.top)		+ ptOffset) * dZoom;	
	pt [2] = (CPoint (rc.right,	rc.bottom)	+ ptOffset) * dZoom;	
	pt [3] = (CPoint (rc.left,	rc.bottom)	+ ptOffset) * dZoom;	

	for (int i = 0; i < 4; i++)
		pt [i + 4] = pt [i] + ptAdjust;

	int cx = GetLeftmost (pt).x - pt [0].x;
	int cy = GetTopmost (pt).y - pt [0].y;

	for (int i = 0; i < 8; i++)
		pt [i] += CPoint (cx, -cy);

	if (dHorzRotation < 0) {
		int nOffset = abs (pt [4].x - ptOffset.x);

		for (int i = 0; i < 8; i++)
			pt [i] += CPoint (nOffset, 0);
	}
}

Foxjet3d::CSize3 CBox::GetSize (UINT nPanel, const CBoundary & head, int nOrient) const
{
	ULONG x = 0, y = 0, z = 0;

	if (nPanel >= 1 && nPanel <= 6) {
		ORIENTATION orient = nOrient ? (ORIENTATION)nOrient : ROT_0;

		if (m_pPanels [nPanel - 1] && !nOrient)
			orient = m_pPanels [nPanel - 1]->GetMembers ().m_orientation;

		const ULONG w = m_box.m_lWidth;
		const ULONG l = m_box.m_lLength;
		const ULONG h = m_box.m_lHeight;
		static const int nFaceIndex [6] = 
		{
			0, // face 1
			2, // face 2
			0, // face 3
			2, // face 4
			4, // face 5
			4, // face 6
		};
		int nIndex = nFaceIndex [nPanel - 1];
		const ULONG lTable [6][3] = 
		{
			{ w, l, h }, // 0
			{ w, h, l }, // 1
			{ l, w, h }, // 2
			{ l, h, w }, // 3
			{ h, l, w }, // 4
			{ h, w, l }, // 5
		};

		if (orient == ROT_90 || orient == ROT_270)
			nIndex++;

		ASSERT (nIndex >= 0 && nIndex < 6);

		x = ThousandthsToLogical (lTable [nIndex][0]);
		y = CBaseElement::ThousandthsToLogical (CPoint (lTable [nIndex][1], 0), head.m_card).x;
		z = ThousandthsToLogical (lTable [nIndex][2]);
	}

	return Foxjet3d::CSize3 (x, y, z);
}

CPoint CBox::GetLeftmost (const CPoint * pptSides, int nCount)
{
	CPoint pt = pptSides [0];

	for (int i = 0; i < nCount; i++) 
		if (pptSides [i].x < pt.x)
			pt = pptSides [i];

	return pt;
}

CPoint CBox::GetRightmost (const CPoint * pptSides, int nCount)
{
	CPoint pt = pptSides [0];

	for (int i = 0; i < nCount; i++) 
		if (pptSides [i].x > pt.x)
			pt = pptSides [i];

	return pt;
}

CPoint CBox::GetTopmost (const CPoint * pptSides, int nCount)
{
	CPoint pt = pptSides [0];

	for (int i = 0; i < nCount; i++) 
		if (pptSides [i].y < pt.y)
			pt = pptSides [i];

	return pt;
}

CPoint CBox::GetBottommost (const CPoint * pptSides, int nCount)
{
	CPoint pt = pptSides [0];

	for (int i = 0; i < nCount; i++) 
		if (pptSides [i].y > pt.y)
			pt = pptSides [i];

	return pt;
}

bool CBox::HasPanels () const 
{
	for (int i = 0; i < 6; i++) 
		if (!m_pPanels [i])
			return false;

	return true;
}

const Foxjet3d::CImage * CBox::GetPanelImage (UINT nPanel) const
{
	if (HasPanels () && nPanel >= 1 && nPanel <= 6) 
		return &(m_pPanels [nPanel - 1]->m_img);

	return NULL;
}

bool CBox::SetPanelImage (UINT nPanel, const FoxjetDatabase::IMAGESTRUCT & img)
{
	if (nPanel >= 1 && nPanel <= 6) {
		int nIndex = nPanel - 1;
		
		if (CPanel * pPanel = m_pPanels [nIndex]) {
			IMAGESTRUCT imgNew (img);

			imgNew.m_nPanel = nPanel;
			imgNew.m_lBoxID = m_box.m_lID;

			pPanel->m_img = imgNew;

			pPanel->Invalidate ();
			
			return true;
		}
	}

	return false;
}

bool CBox::DeletePanelImage (UINT nPanel)
{
	if (nPanel >= 1 && nPanel <= 6) {
		int nIndex = nPanel - 1;

		if (CPanel * pPanel = m_pPanels [nIndex]) {
			pPanel->m_img.Free ();
			pPanel->Invalidate ();
			return true;
		}
	}

	return false;
}

bool CBox::GetPanelImages (const DB::IMAGESTRUCT * pImages [6]) const
{
	if (HasPanels ()) {
		for (int i = 0; i < 6; i++)
			pImages [i] = &(m_pPanels [i]->m_img.GetMembers ());

		return true;
	}

	return false;
}

bool CBox::GetPanel (UINT nNumber, const CPanel ** pPanel) const
{
	if (HasPanels ()) {
		for (int i = 0; i < 6; i++) {
			if (m_pPanels [i]->GetMembers ().m_nNumber == nNumber) {
				* pPanel = m_pPanels [i];
				return true;
			}
		}
	}

	return false;
}

bool CBox::GetPanelByID (ULONG lID, const CPanel ** pPanel) const
{
	if (HasPanels ()) {
		for (int i = 0; i < 6; i++) {
			if (m_pPanels [i]->GetMembers ().m_lID == lID) {
				* pPanel = m_pPanels [i];
				return true;
			}
		}
	}

	return false;
}

bool CBox::GetPanels (const CPanel * pPanels [6]) const
{
	if (HasPanels ()) {
		for (int i = 0; i < 6; i++)
			pPanels [i] = m_pPanels [i];

		return true;
	}

	return false;
}

bool CBox::GetPanel (UINT nNumber, const DB::PANELSTRUCT ** pPanel) const
{
	if (HasPanels ()) {
		for (int i = 0; i < 6; i++) {
			if (m_pPanels [i]->GetMembers ().m_nNumber == nNumber) {
				* pPanel = &(m_pPanels [i]->GetMembers ());
				return true;
			}
		}
	}

	return false;
}

bool CBox::GetPanelByID (ULONG lID, const DB::PANELSTRUCT ** pPanel) const
{
	if (HasPanels ()) {
		for (int i = 0; i < 6; i++) {
			if (m_pPanels [i]->GetMembers ().m_lID == lID) {
				* pPanel = &(m_pPanels [i]->GetMembers ());
				return true;
			}
		}
	}

	return false;
}

bool CBox::GetPanels (const DB::PANELSTRUCT * pPanels [6]) const
{
	if (HasPanels ()) {
		for (int i = 0; i < 6; i++)
			pPanels [i] = &(m_pPanels [i]->GetMembers ());

		return true;
	}

	return false;
}

void CBox::Invalidate (UINT nPanel)
{
	m_cache.Clear ();

	if (nPanel >= 1 && nPanel <= 6) {
		if (CPanel * p = m_pPanels [nPanel - 1])
			p->Invalidate ();
	}
	else {
		if (HasPanels ())
			for (int i = 0; i < 6; i++) 
				m_pPanels [i]->Invalidate ();
	}
}

UINT CBox::GetPanelNumber (const CBoundary & head) const
{
	for (int i = 0; i < 6; i++) {
		const CPanel * p = m_pPanels [i];

		if (p) {
			const PANELSTRUCT & panel = p->GetMembers ();
			
			if (panel.m_lID == head.GetPanelID ()) 
				return panel.m_nNumber;
		}
	}

	return -1;
}

void CBox::Reload()
{
	CDC * pDC = ::AfxGetMainWnd ()->GetDC ();

	Invalidate (-1);

	DeleteBoxCache ();
	DeleteFaceCache ();
	DeletePlgCache ();

	for (int i = 0; i < ARRAYSIZE (m_pPanels); i++)
		if (CPanel * p = m_pPanels [i])
			p->LoadImage (pDC, GetDB ());
	
	::AfxGetMainWnd ()->ReleaseDC (pDC);
}

CString CBox::ToString (const FoxjetCommon::CVersion & ver) const
{
	CString str;

	str.Format (_T ("{Box,%d,%s,%s,%d,%d,%d}"), 
		m_box.m_lID,
		m_box.m_strName,
		m_box.m_strDesc,
		m_box.m_lWidth,
		m_box.m_lLength,
		m_box.m_lHeight);

	return str;
}

bool CBox::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	const int nFields = 7;
	CStringArray vActual, vDef;
	CString strToken [nFields] = {
		_T (""),			// Prefix (required)
		_T (""),			// ID
		_T (""),			// Name
		_T (""),			// Desc
		_T (""),			// Width
		_T (""),			// Length
		_T (""),			// Height
	};
	int i;

	Tokenize (str, vActual);

	if (vActual.GetSize () < 1) {
		TRACEF (_T ("Too few tokens"));
		return false;
	}

	Tokenize (ToString (ver), vDef);

	for (i = 1; i < min (vDef.GetSize (), nFields); i++)
		if (vDef [i].GetLength ())
			strToken [i] = vDef [i];

	for (i = 0; i < min (vActual.GetSize (), nFields); i++)
		if (vActual [i].GetLength ())
			strToken [i] = vActual [i];

	if (strToken [0].CompareNoCase (_T ("Box")) != 0){
		TRACEF (_T ("Wrong prefix \"") + strToken [0] + _T ("\""));
		return false;
	}

	m_box.m_lID		= _tcstol (strToken [1], NULL, 10);
	m_box.m_strName = strToken [2];
	m_box.m_strDesc = strToken [3];
	m_box.m_lWidth	= _tcstol (strToken [4], NULL, 10);
	m_box.m_lLength = _tcstol (strToken [5], NULL, 10);
	m_box.m_lHeight = _tcstol (strToken [6], NULL, 10);

	return true;
}

int CBox::FindPanel (CArray <FoxjetDatabase::PANELSTRUCT, FoxjetDatabase::PANELSTRUCT &> & vPanels, ULONG lPanelID)
{
	for (int i = 0; i < vPanels.GetSize (); i++)
		if (vPanels [i].m_lID == lPanelID)
			return i;

	return -1;
}

Element::CPairArray CBox::GetElements (UINT nPanel, CBoxParams & p)
{
	ASSERT (nPanel >= 1 && nPanel <= 6);

	Element::CPairArray v;
	CPanel * pFace = m_pPanels [nPanel - 1];
	ASSERT (pFace);

	ULONG lPanelID = pFace->GetMembers ().m_lID;
	CLongArray vMsgs = pFace->GetMessageIndicies (p, lPanelID);
	CBoundary activeHead, workingHead;

	p.GetActiveHead (* this, activeHead);
	GetWorkingHead (p, workingHead);

	for (int nHead = 0; nHead < pFace->m_vBounds.GetSize (); nHead++) {
		if (CMessage * pMsg = p.m_task.GetMessageByHead (pFace->m_vBounds [nHead].GetID ())) {
			for (int i = 0; i < pMsg->m_list.GetSize (); i++) {
				CElement & e = pMsg->m_list [i];

				v.Add (Element::CPair (&e, &pMsg->m_list, &workingHead.m_card));
			}
		}
	}

	return v;
}

bool CBox::GetWorkingHead (Panel::CBoundary & head) const
{
	int nPanelIndex = -1;
	int nHeadIndex = -1;
	UINT nMaxRes = 0;

	for (int nPanel = 1; nPanel <= 6; nPanel++) {
		const CPanel * pPanel = NULL;

		if (GetPanel (nPanel, &pPanel)) {
			for (int nHead = 0; nHead < pPanel->m_vBounds.GetSize (); nHead++) {
				const CBoundary & h = pPanel->m_vBounds [nHead];
				UINT nRes = (UINT)h.m_card.GetRes ();

				if (nRes > nMaxRes) {
					nMaxRes		= nRes;
					nPanelIndex = nPanel;
					nHeadIndex	= nHead;
				}
			}
		}
	}

	if (nPanelIndex != -1 && nHeadIndex != -1) {
		const CPanel * pPanel = NULL;

		VERIFY (GetPanel (nPanelIndex, &pPanel));
	
		if (pPanel) {
			if (nHeadIndex >= 0 && nHeadIndex < pPanel->m_vBounds.GetSize ()) {
				head = pPanel->m_vBounds [nHeadIndex];
				return true;
			}
		}
	}

	return false;
}


ULONG CBox::GetWorkingHead (CBoundaryArray & vHeads) 
{
	ULONG lHeadID = -1;
	UINT nMaxRes = 0;

	for (int i = 0; i < vHeads.GetSize (); i++) {
		HEADSTRUCT & h = vHeads [i].m_card;
		UINT nRes = (UINT)h.GetRes ();

		if (IsValveHead (h)) { ASSERT (h.m_nEncoderDivisor == 2); }

		if (nRes > nMaxRes) {
			nMaxRes = nRes;
			lHeadID = h.m_lID;
		}
	}

	return lHeadID;
}

bool CBox::GetWorkingHead (CBoundaryArray & vHeads, CBoundary & head) const
{
	ULONG lWorkingID = GetWorkingHead (vHeads);

	for (int i = 0; i < vHeads.GetSize (); i++) {
		CBoundary & h = vHeads [i];

		if (h.GetID () == lWorkingID) {
			head = h;
			return true;
		}
	}

	return false;
}

bool CBox::GetWorkingHead (CBoxParams & p, CBoundary & head) const
{
	UINT nMaxRes = 0;
	int nIndex = -1;
	const CPanel * pPanel = NULL;

	if (GetPanel (p.m_nPanel, &pPanel)) {
		for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) {
			const CBoundary & h = pPanel->m_vBounds [i];
			UINT nRes = (UINT)h.m_card.GetRes ();

			if (nRes > nMaxRes) {
				nMaxRes		= nRes;
				nIndex		= i;
			}
		}

		if (nIndex != -1) {
			head = pPanel->m_vBounds [nIndex];
			return true;
		}
	}
	
	return false;
}

void CBox::Render (CDxParams * pParams) 
{
	//LOCK (m_cs);

	//DECLARETRACECOUNT (200);

	ASSERT (pParams);
	ASSERT (pParams->IsKindOf (RUNTIME_CLASS (CBoxParams)));

	CBoxParams & p = * (CBoxParams *)pParams;

	ASSERT (p.m_nPanel >= 1 && p.m_nPanel <= 6);

	//MARKTRACECOUNT (); 

	if (p.m_bOutlineOnly) {
		DeleteBoxCache ();
		return;
	}

	if (!HasPanels ())
		return;

	CWnd * pWnd = p.m_view.GetParent ();
	ASSERT (pWnd);
	CDC * pDC = pWnd->GetDC ();
	CDC dcMem;
	int nPanelIndex = p.m_nPanel - 1;

	//MARKTRACECOUNT ();

	ASSERT (pDC);
	CSize sizeBmp = CalcViewSize (p) + CSize (1, 1);
		
	if (ISVALVE ())
		sizeBmp += CBaseElement::GetValveDotSize ();

	if (m_pbmpBox) {
		DIBSECTION ds;

		::ZeroMemory (&ds, sizeof (ds));
		VERIFY (m_pbmpBox->GetObject (sizeof (ds), &ds));

		if (ds.dsBm.bmWidth != sizeBmp.cx || ds.dsBm.bmHeight != sizeBmp.cy) 
			DeleteBoxCache ();
	}

	//MARKTRACECOUNT ();

	if (!m_pbmpBox) {
		const bool bMonochrome = m_bMonochrome;
		m_pbmpBox = new CBitmap ();
	
		bool bCreateCompatibleBitmap = m_pbmpBox->CreateCompatibleBitmap (pDC, sizeBmp.cx, sizeBmp.cy) ? true : false;

		m_bMonochrome = !bCreateCompatibleBitmap;

		if (bMonochrome != m_bMonochrome) {
			::SendMessageTimeout (pParams->m_view.GetParent ()->m_hWnd, WM_EDITOR_REDRAW, 0, 0,  
				SMTO_NORMAL | SMTO_ABORTIFHUNG, 0, NULL);

			return;
		}

		if (!bCreateCompatibleBitmap) {
			CDC dcTmp;

			//	TRACEF (FoxjetDatabase::ToString (sizeBmp.cx) + _T (", ") + FoxjetDatabase::ToString (sizeBmp.cy));
			//	TRACEF (FoxjetDatabase::ToString ((double)pParams->m_rZoom) + _T (" : ") + FoxjetDatabase::ToString (sizeBmp.cx * sizeBmp.cy));
			//	sizeBmp.cx = (int)((double)sizeBmp.cx / pParams->m_rZoom);
			//	sizeBmp.cy = (int)((double)sizeBmp.cy / pParams->m_rZoom);
			//	TRACEF (FoxjetDatabase::ToString ((double)pParams->m_rZoom) + _T (" : ") + FoxjetDatabase::ToString (sizeBmp.cx * sizeBmp.cy));
			//	TRACEF (FoxjetDatabase::ToString (sizeBmp.cx) + _T (", ") + FoxjetDatabase::ToString (sizeBmp.cy));
			
			DeleteBoxCache ();
			m_pbmpBox = new CBitmap ();

			dcTmp.CreateCompatibleDC (NULL);
			VERIFY (m_pbmpBox->CreateCompatibleBitmap (&dcTmp, sizeBmp.cx, sizeBmp.cy));
			DeleteFaceCache ();
		}
	}

	if (!::AfxIsValidAddress (m_pbmpBox, sizeof (* m_pbmpBox), TRUE)) {
		{ CString str; str.Format (_T ("%s(%d): !AfxIsValidAddress: m_pbmpBox, 0x%p"), __FILE__, __LINE__, m_pbmpBox); MsgBox (str); }
	}

	//MARKTRACECOUNT ();

	ASSERT (m_pbmpBox);
	dcMem.CreateCompatibleDC (pDC);
	CBitmap * pBmp = dcMem.SelectObject (m_pbmpBox);

	::BitBlt (dcMem, 0, 0, sizeBmp.cx, sizeBmp.cy, NULL, 0, 0, WHITENESS);

	//MARKTRACECOUNT ();

	static int nCall = 0; // TODO: rem

	if (nCall == 0)	
		nCall = (FoxjetCommon::verApp >= CVersion (1, 20)) ? 1 : -1;

	if (nCall == 1) { // TODO: rem
		//MARKTRACECOUNT ();

		CPoint pt [8];
		COrder order [6];
		UINT nPanels [6] = { 0 };
		ULONG lPlgMask = 
			BoxParams::SELECTEDPANEL |
			BoxParams::OFFSET |
			BoxParams::ROTATION |
			BoxParams::BLEND |
			BoxParams::TASK |
			DxParams::ZOOM;
		ULONG lFaceMask = 
			BoxParams::SELECTEDPANEL | 
			BoxParams::ROTATION;
		bool bRedraw = (p.m_lChange & lPlgMask) || (p.m_lChange & lFaceMask);
		bool bDrawFace = 
			(p.m_lChange & DxParams::ZOOM) ? true : false ||
			m_pbmpFace == NULL;

		if (bRedraw)
			::AfxGetMainWnd ()->BeginWaitCursor ();

		//MARKTRACECOUNT ();

		GetOrder (p, pt, order, nPanels);

		if (p.m_lChange & lPlgMask)
			DeletePlgCache ();

		//MARKTRACECOUNT ();

		if (p.m_lChange & lFaceMask) {
			const COrientation face = MapPanel (p.m_nPanel);
			const COrder o (face, pt); 

			DeleteFaceCache ();
			bDrawFace = true;
		}

		//MARKTRACECOUNT ();

		BltSides (dcMem, p, order, pt);

		//MARKTRACECOUNT ();

		if (bDrawFace) {
			if (!bRedraw)
				::AfxGetMainWnd ()->BeginWaitCursor ();

			DrawFaceImage (p, dcMem);

			if (!bRedraw)
				::AfxGetMainWnd ()->EndWaitCursor ();
		}

		if (bRedraw)
			::AfxGetMainWnd ()->EndWaitCursor ();
	}

	dcMem.SelectObject (pBmp);

	//TRACECOUNTARRAYMAX ();
}

CRect CBox::Erase (CBoxParams & p, Element::CPairArray & vInvalidate)
{
	//LOCK (m_cs);

	CRect rcResult (0, 0, 0, 0);
	const double dZoom = p.m_rZoom;
	CPairArray vRedraw = GetRedraw (p, vInvalidate);
	CBoundary workingHead;

	GetWorkingHead (p, workingHead);

	for (int i = 0; i < vInvalidate.GetSize (); i++) {
		int nIndex = CElement::Find (vRedraw, vInvalidate [i]);

		if (nIndex != -1)
			vRedraw.RemoveAt (nIndex);
	}

	if (CBitmap * pCache = m_pbmpBox) {
		CDC dcCache, * pDC = p.m_view.GetParent ()->GetDC ();

		if (!pDC) {
			TRACEF (_T ("pDC == NULL"));
			return rcResult;
		}

		dcCache.CreateCompatibleDC (pDC);
		CBitmap * pOldCache = dcCache.SelectObject (pCache);

		for (int i = 0; i < vInvalidate.GetSize (); i++) {
			CElement & e = vInvalidate [i];
			CEditorElementList & list = vInvalidate [i];
			const CBoundary & head = list.m_bounds;
			const CRect rcHead = GetHeadRect (head, p);
			CRect rc = (vInvalidate [i].GetWindowRect (workingHead.m_card) * dZoom) + rcHead.TopLeft ();

			rc.IntersectRect (rc, rcHead); // clip to head

			if (IsValveHead (head.m_card)) 
				rc.InflateRect (CBaseElement::GetValveDotSize ());

			rcResult.UnionRect (rcResult, rc);

			// TODO: if a background bitmap is used, this will have to be changed
			//::BitBlt (dcCache, rc.left, rc.top, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
			DrawBackground (p, dcCache, CElement::GetValveRect (rc, workingHead.m_card, p.m_rZoom)); 
		}

		for (int i = 0; i < vRedraw.GetSize (); i++) {
			CElement & e = vRedraw [i];
			CEditorElementList & list = vRedraw [i];
			const CBoundary & head = list.m_bounds;
			const CRect rcHead = GetHeadRect (head, p);
			CRect rc = (e.GetWindowRect (head.m_card, workingHead.m_card) * dZoom) + rcHead.TopLeft ();
			const CSize size = e.GetWindowRect (head.m_card, workingHead.m_card).Size ();
			CDC dcElement;
			CBitmap bmpElement;

			VERIFY (dcElement.CreateCompatibleDC (pDC));

			VERIFY (bmpElement.CreateCompatibleBitmap (pDC, size.cx, size.cy));
			CBitmap * pBmp = dcElement.SelectObject (&bmpElement);

			rc.IntersectRect (rc, rcHead); // clip to head
			rcResult.UnionRect (rcResult, rc);

			// TODO: if a background bitmap is used, this will have to be changed
			//::BitBlt (dcCache, rc.left, rc.top, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
			DrawBackground (p, dcCache, CElement::GetValveRect (rc, workingHead.m_card, p.m_rZoom)); 

			/*
			::BitBlt (dcElement, 0, 0, size.cx, size.cy, NULL, 0, 0, WHITENESS);
			e.Draw (dcElement, workingHead.m_card, &list, FoxjetCommon::CBaseElement::EDITOR);
			dcCache.StretchBlt (rc.left, rc.top, rc.Width (), rc.Height (), &dcElement, 
				0, 0, size.cx, size.cy, SRCAND);
			*/
			e.Draw (dcElement, workingHead, &list, dcCache, rc.left, rc.top, rc.Width (), rc.Height (), size.cx, size.cy, p.m_rZoom);

			dcElement.SelectObject (pBmp);
		}

		dcCache.SelectObject (pOldCache);
	}

	return rcResult;
}

CRect CBox::Invalidate (CBoxParams & p, Element::CPairArray & vInvalidate, Element::CPairArray & vRedraw)
{
	//LOCK (m_cs);

	CRect rcResult (0, 0, 0, 0);
	const double dZoom = p.m_rZoom;
	CBoundary workingHead;

	GetWorkingHead (p, workingHead);
	vRedraw = GetRedraw (p, vInvalidate);
	
	if (CBitmap * pCache = m_pbmpBox) {
		CDC dcCache, * pDC = p.m_view.GetParent ()->GetDC ();

		if (!pDC) {
			TRACEF (_T ("pDC == NULL"));
			return rcResult;
		}

		dcCache.CreateCompatibleDC (pDC);
		CBitmap * pOldCache = dcCache.SelectObject (pCache);

		for (int i = 0; i < vInvalidate.GetSize (); i++) {
			CElement & e = vInvalidate [i];
			CEditorElementList & list = vInvalidate [i];
			const CBoundary & head = list.m_bounds;
			const CRect rcHead = GetHeadRect (head, p);
			CRect rcOld = (vInvalidate [i].GetWindowRect (workingHead.m_card) * dZoom) + rcHead.TopLeft ();
			CRect rcNew = (e.GetWindowRect (head.m_card, workingHead.m_card) * dZoom) + rcHead.TopLeft ();

			rcOld.IntersectRect (rcOld, rcHead); // clip to head
			rcNew.IntersectRect (rcNew, rcHead); // clip to head

			rcResult.UnionRect (rcResult, rcOld);
			rcResult.UnionRect (rcResult, rcNew);

			DrawBackground (p, dcCache, CElement::GetValveRect (rcOld, workingHead.m_card, p.m_rZoom)); 
		}

		for (int i = 0; i < vRedraw.GetSize (); i++) {
			CElement & e = vRedraw [i];
			CEditorElementList & list = vRedraw [i];
			const CBoundary & head = list.m_bounds;
			const CRect rcHead = GetHeadRect (head, p);
			CRect rc = (e.GetWindowRect (head.m_card, workingHead.m_card) * dZoom) + rcHead.TopLeft ();
			const CSize size = e.GetWindowRect (head.m_card, workingHead.m_card).Size ();
			CDC dcElement;
			CBitmap bmpElement;

			VERIFY (dcElement.CreateCompatibleDC (pDC));

			VERIFY (bmpElement.CreateCompatibleBitmap (pDC, size.cx, size.cy));
			CBitmap * pBmp = dcElement.SelectObject (&bmpElement);

			rc.IntersectRect (rc, rcHead); // clip to head
			rcResult.UnionRect (rcResult, rc);

			//if (e.GetElement ()->IsPrintable ()) 
			{
				/*
				::BitBlt (dcElement, 0, 0, size.cx, size.cy, NULL, 0, 0, WHITENESS);
				e.Draw (dcElement, workingHead.m_card, &list, FoxjetCommon::CBaseElement::EDITOR);
				dcCache.StretchBlt (rc.left, rc.top, rc.Width (), rc.Height (), &dcElement, 
					0, 0, size.cx, size.cy, SRCAND);
					*/
				e.Draw (dcElement, workingHead, &list, dcCache, rc.left, rc.top, rc.Width (), rc.Height (), size.cx, size.cy, p.m_rZoom);
			}
			//else {
			//	#ifdef __WINPRINTER__
			//	if (e.GetElement ()->GetClassID () == LABEL) 
			//		FoxjetElements::CLabelElement::DrawIcon (dcCache, rc);
			//	else {
			//		dcCache.FillSolidRect (rc, rgbRed); // TODO
			//	}
			//	#endif //__WINPRINTER__
			//}

			dcElement.SelectObject (pBmp);
		}

		dcCache.SelectObject (pOldCache);
	}

	return rcResult;
}

Element::CPairArray CBox::GetRedraw (CBoxParams & p, Element::CPairArray & v)
{
	const double dZoom = p.m_rZoom;
	CPairArray vRedraw;
	CBoundary workingHead;

	GetWorkingHead (p, workingHead);

	for (int i = 0; i < v.GetSize (); i++) {
		ASSERT (v [i].m_pElement);
		ASSERT (v [i].m_pList);

		CElement & e = v [i];
		CEditorElementList & list = v [i];
		const CBoundary & head = list.m_bounds;
		const CRect rcHead = GetHeadRect (head, p);
		const CRect rcOld = (v [i].GetWindowRect (workingHead.m_card) * dZoom) + rcHead.TopLeft ();
		const CRect rcNew = (e.GetWindowRect (head.m_card, workingHead.m_card) * dZoom) + rcHead.TopLeft ();
		CMessage * pMsg =  p.m_task.GetMessageByHead (head.GetID ()); 
		CPair pair (&e, &list, &workingHead.m_card);

		ASSERT (pMsg);

		if (CElement::Find (vRedraw, pair) == -1)
			vRedraw.Add (pair);
		
		for (int nElement = 0; nElement < pMsg->m_list.GetSize (); nElement++) {
			CElement & eCmp = pMsg->m_list [nElement];
			CEditorElementList & listCmp = pMsg->m_list;
			CRect rcElement = (eCmp.GetWindowRect (head.m_card, workingHead.m_card) * dZoom) + rcHead.TopLeft ();
			CRect rcTmp (0, 0, 0, 0);
			
			rcElement.IntersectRect (rcElement, rcHead); // clip to head rect

			if (rcTmp.IntersectRect (rcElement, rcOld) || rcTmp.IntersectRect (rcElement, rcNew)) {
				CPair pair (&eCmp, &listCmp, &workingHead.m_card);

				if (CElement::Find (vRedraw, pair) == -1)
					vRedraw.Add (pair);
			}
		}
	}

	return vRedraw;
}

void CBox::DrawFaceImage (CBoxParams & p, CDC & dcCache)
{
	//LOCK (m_cs);

	// note: m_pbmpBox MUST be selected into dcCache before this fct is called

	DECLARETRACECOUNT (20);

	const COrientation face = MapPanel (p.m_nPanel);
	int nPanelIndex = face.m_nPanel - 1;

	MARKTRACECOUNT ();

	::AfxGetMainWnd ()->BeginWaitCursor ();
	
	if (!m_pbmpFace) {
		ORIENTATION rot = face.m_orient;
		
		if (face.m_nPanel == 6)
			rot = (ORIENTATION)((rot + 2) % 4);

		m_pbmpFace = m_pPanels [nPanelIndex]->CreateImage (dcCache, p, * this, rot, 0);
	}

	MARKTRACECOUNT ();

	if (m_pbmpFace) {
		CPoint pt [8];
		COrder order [6];
		UINT nPanels [6] = { 0 };

		MARKTRACECOUNT ();

		GetOrder (p, pt, order, nPanels);
		
		const COrientation face = MapPanel (p.m_nPanel);
		const COrder o (face, pt); 
		CDC dcFace;
		DIBSECTION ds;
		int x = o.m_pt [0].x;
		int y = o.m_pt [0].y;
		int cx = o.m_pt [2].x - x;
		int cy = o.m_pt [2].y - y;

		MARKTRACECOUNT ();

		dcFace.CreateCompatibleDC (&dcCache);
		::ZeroMemory (&ds, sizeof (ds));
		VERIFY (m_pbmpFace->GetObject (sizeof (ds), &ds));

		MARKTRACECOUNT ();

		CBitmap * pFace = dcFace.SelectObject (m_pbmpFace);

		MARKTRACECOUNT ();

		dcCache.StretchBlt (x, y, cx, cy, &dcFace, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, SRCCOPY);
		
		MARKTRACECOUNT ();

		dcFace.SelectObject (pFace);

		MARKTRACECOUNT ();
	}

	::AfxGetMainWnd ()->EndWaitCursor ();

	//TRACECOUNTARRAYMAX ();
}

void CBox::DrawBackground (CBoxParams & p, CDC & dc, const CRect & rc)
{
	//LOCK (m_cs);

	const COrientation face = MapPanel (p.m_nPanel);
	int nPanelIndex = face.m_nPanel - 1;

	if (!m_pbmpFace) 
		DrawFaceImage (p, dc);

	if (m_pbmpFace) {
		CDC dcMem;
		CPoint pt [8];

		GetPoints (p, pt);

		COrder o (face,	pt); 

		VERIFY (dcMem.CreateCompatibleDC (&dc));

		const CPoint ptOffset (
			(int)(((double)rc.left - o.m_pt [0].x) / (double)p.m_rZoom),
			(int)(((double)rc.top - o.m_pt [0].y) / (double)p.m_rZoom));
		const CSize size (
			(int)((double)rc.Width () / (double)p.m_rZoom),
			(int)((double)rc.Height () / (double)p.m_rZoom));
		const CRect rcStretch = CRect (0, 0, size.cx, size.cy) + ptOffset;

		CBitmap * pMem = dcMem.SelectObject (m_pbmpFace);

		dc.BitBlt (rc.left, rc.top, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
		dc.StretchBlt (rc.left, rc.top, rc.Width (), rc.Height (), &dcMem,
			rcStretch.left, rcStretch.top, rcStretch.Width (), rcStretch.Height (), SRCCOPY);
//SAVEBITMAP (* m_pbmpFace, "C:\\Temp\\Debug\\dc.bmp");

		dcMem.SelectObject (pMem);
	}
	else
		TRACEF (_T ("!m_pbmpFace"));
}

int CBox::LoadPanels (FoxjetDatabase::COdbcDatabase & db, ULONG lLineID)
{
	const bool bValve = ISVALVE ();
	int nHeads = 0;
	CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> vPanels;
	ULONG lBoundaryID = 1;
	CMap <ULONG, ULONG, ULONG, ULONG> map;

	HeadConfigDlg::CPanel::GetPanels (db, lLineID, vPanels);

	for (int nPanel = 0; nPanel < vPanels.GetSize (); nPanel++) {
		HeadConfigDlg::CPanel & p = vPanels [nPanel];

		if (!bValve) {
			for (int nCard = 0; nCard < p.m_vCards.GetSize (); nCard++) {
				HEADSTRUCT & card = p.m_vCards [nCard];

				map.SetAt (card.m_lID, lBoundaryID);
				VERIFY (Add (CBoundary (card, lBoundaryID++, p.m_panel.m_lID)));
				nHeads++;
			}
		}
		else {
			for (int nCard = 0; nCard < p.m_vCards.GetSize (); nCard++) {
				HEADSTRUCT card = p.m_vCards [nCard];

				CValvePropDlg::GetPanelValves (db, card, card.m_lPanelID);

				const int nCount = card.m_vValve.GetSize ();
				int n = 0;

				while (card.m_vValve.GetSize ()) {
					VALVESTRUCT & valve = card.m_vValve [0];
					CArray <VALVESTRUCT, VALVESTRUCT &> v;

					CValvePropDlg::GetList (card, valve, v);
					VERIFY (Add (CBoundary (card, v, lBoundaryID++, p.m_panel.m_lID)));
					nHeads++;

					for (int i = 0; i < v.GetSize (); i++) {
						int nIndex = CTask::Find (card.m_vValve, v [i].m_lID);

						ASSERT (nIndex != -1);
						card.m_vValve.RemoveAt (nIndex);

						if (n++ > nCount) {
							ASSERT (0);
							break;
						}
					}
				}
			}

			{
				const int nCount = p.m_vValve.GetSize ();
				int n = 0;

				while (p.m_vValve.GetSize ()) {
					VALVESTRUCT & valve = p.m_vValve [0];
					CArray <VALVESTRUCT, VALVESTRUCT &> v;
					HEADSTRUCT card = HeadConfigDlg::CPanel::GetCard (vPanels, valve.m_lCardID);

					CValvePropDlg::GetPanelValves (db, card, valve.m_lPanelID);

					CValvePropDlg::GetList (card, valve, v);
					VERIFY (Add (CBoundary (card, v, lBoundaryID++, p.m_panel.m_lID)));
					nHeads++;

					for (int i = 0; i < v.GetSize (); i++) {
						int nIndex = CTask::Find (p.m_vValve, v [i].m_lID);

						ASSERT (nIndex != -1);
						p.m_vValve.RemoveAt (nIndex);

						if (n++ > nCount) {
							ASSERT (0);
							break;
						}
					}
				}
			}
		}
	}

	{	// remap based on boundary IDs

		// NOTE: was not implemented for VX
		
		for (int nPanel = 0; nPanel < 6; nPanel++) {
			if (CPanel * pPanel = m_pPanels [nPanel]) {
				for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) {
					CBoundary & b = pPanel->m_vBounds [i];
					ULONG lBoundaryID = 0;

					if (map.Lookup (b.m_card.m_lLinkedTo, lBoundaryID)) 
						b.m_lLinkedTo = lBoundaryID;
				}
			}
		}
	}

	return nHeads;
}

bool CBox::Add (Panel::CBoundary & b)
{
	ASSERT (HasPanels ());

	for (int nPanel = 0; nPanel < 6; nPanel++) {
		if (CPanel * pPanel = m_pPanels [nPanel]) {
			if (pPanel->GetMembers ().m_lID == b.GetPanelID ()) {
				for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) {
					if (pPanel->m_vBounds [i].GetY () < b.GetY ()) {
						pPanel->m_vBounds.InsertAt (i, b);

						return true;
					}
				}

				pPanel->m_vBounds.Add (b);			
				return true;
			}
		}
	}

	return false;
}

CString CBox::GetTrace () const
{
	CString str, strTrace;

	for (int nPanel = 1; nPanel <= 6; nPanel++) {
		const CPanel * pPanel = NULL;

		VERIFY (GetPanel (nPanel, &pPanel));
		ASSERT (pPanel);

		strTrace += pPanel->GetTrace ();
	}

	return strTrace;
}

int CBox::GetMinFontSize (int nHead) const
{
	DBMANAGETHREADSTATE (GetDB ());

	int nMin = 5;

	if (nHead == ALL) {
		for (int nPanel = 0; nPanel < 6; nPanel++) {
			CPanel * pPanel = m_pPanels [nPanel];

			for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) 
				nMin = min (nMin, (int)pPanel->m_vBounds [i].m_card.GetChannels ());
		}
	}

	return nMin;
}

int CBox::GetMaxFontSize (int nHead) const
{
	DBMANAGETHREADSTATE (GetDB ());

	int nMax = 32;

	if (nHead == ALL) {
		for (int nPanel = 0; nPanel < 6; nPanel++) {
			CPanel * pPanel = m_pPanels [nPanel];

			for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) 
				nMax = max (nMax, (int)pPanel->m_vBounds [i].m_card.GetChannels ());
		}
	}
	else {
		for (int nPanel = 0; nPanel < 6; nPanel++) {
			if (CPanel * pPanel = m_pPanels [nPanel]) {
				for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) {
					if (pPanel->m_vBounds [i].GetID () == (ULONG)nHead) {
						nMax = pPanel->m_vBounds [i].m_card.GetChannels ();
						return nMax; //break;
					}
				}
			}
		}
	}

	return nMax;
}

bool CBox::IsLinked (const Panel::CBoundary & b) const
{
	for (int nPanel = 1; nPanel <= 6; nPanel++) {
		const CPanel * pPanel = NULL;

		if (GetPanel (nPanel, &pPanel)) {
			ASSERT (pPanel);

			for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) {
				const Panel::CBoundary & h = pPanel->m_vBounds [i];

				if (b.m_lLinkedTo == h.GetID ())
					return true;
			}
		}
	}

	return false;
}

bool CBox::GetLinkedSlave (const Panel::CBoundary & master, Panel::CBoundary & link) const
{
	for (int nPanel = 1; nPanel <= 6; nPanel++) {
		const CPanel * pPanel = NULL;

		if (GetPanel (nPanel, &pPanel)) {
			ASSERT (pPanel);

			for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) {
				const Panel::CBoundary & h = pPanel->m_vBounds [i];

				if (master.GetID () == h.m_lLinkedTo) {
					link = h;
					return true;
				}
			}
		}
	}

	return false;
}
