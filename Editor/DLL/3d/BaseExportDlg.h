#if !defined(AFX_BASEEXPORTDLG_H__92CA5D35_76EB_4373_AAC6_339B68313AAA__INCLUDED_)
#define AFX_BASEEXPORTDLG_H__92CA5D35_76EB_4373_AAC6_339B68313AAA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BaseExportDlg.h : header file
//

#include "3dApi.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CBaseExportDlg dialog

namespace Foxjet3d
{
	class _3D_API CBaseExportDlg : public FoxjetCommon::CEliteDlg
	{
		DECLARE_DYNAMIC (CBaseExportDlg);

	// Construction
	public:
		CBaseExportDlg (UINT nIDTemplate, CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CBaseExportDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA


	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CBaseExportDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:

		ULONG GetSelectedFromLineID () const;
		
		afx_msg void OnSelchangeLinefrom();
		afx_msg void OnAll();
		afx_msg void OnSelect();


		// Generated message map functions
		//{{AFX_MSG(CBaseExportDlg)
			// NOTE: the ClassWizard will add member functions here
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BASEEXPORTDLG_H__92CA5D35_76EB_4373_AAC6_339B68313AAA__INCLUDED_)
