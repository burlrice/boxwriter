#ifdef __BOX_INLINE__

#include "Debug.h"

inline bool Foxjet3d::Box::CBoxParams::GetHead (const Foxjet3d::Box::CBox & box, ULONG lHeadID, Panel::CBoundary & b, bool bDefaultIfNotFound) const
{
	using namespace Foxjet3d::Box;

	for (int nPanel = 1; nPanel <= 6; nPanel++) {
		const Panel::CPanel * pPanel = NULL;

		if (box.GetPanel (nPanel, &pPanel)) {
			ASSERT (pPanel);

			for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) {
				const Panel::CBoundary & h = pPanel->m_vBounds [i];

				if (h.GetID () == lHeadID) {
					b = h;
					return true;
				}
			}
		}
	}

	if (bDefaultIfNotFound) {
		for (int nPanel = 1; nPanel <= 6; nPanel++) {
			const Panel::CPanel * pPanel = NULL;

			if (box.GetPanel (nPanel, &pPanel)) {
				ASSERT (pPanel);

				if (pPanel->m_vBounds.GetSize ()) {
					b = pPanel->m_vBounds [0];
					return false;
				}
			}
		}
	}

	return false;
}

inline bool Foxjet3d::Box::CBoxParams::GetActiveHead (const Foxjet3d::Box::CBox & box, Panel::CBoundary & b) const
{
	using namespace Foxjet3d::Box;

	return GetHead (box, m_lHeadID, b, true);
}

inline bool Foxjet3d::Box::CBoxParams::GetHead (const Foxjet3d::Box::CBox & box, const DB::HEADSTRUCT & head, Panel::CBoundary & b) const
{
	using namespace Box;

	for (int nPanel = 1; nPanel <= 6; nPanel++) {
		const Panel::CPanel * pPanel = NULL;

		VERIFY (box.GetPanel (nPanel, &pPanel));
		ASSERT (pPanel);

		for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) {
			const Panel::CBoundary & h = pPanel->m_vBounds [i];

			if (h.m_card.m_lID == head.m_lID) {
				b = h;
				return true;
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline CRect Foxjet3d::Box::CBox::GetHeadRect (const Panel::CBoundary & head, Foxjet3d::Box::CBoxParams & p, bool bClip) const
{
	using namespace Foxjet3d::Box;

	CCache * pCache = (CCache *)(DWORD)&m_cache; // const --> non-const cast

	if (!pCache->IsValid (p)) 
		pCache->Build (* this, p);

	return pCache->GetHeadRect (head, * this, p, bClip);
}

inline CRect Foxjet3d::Box::CBox::GetPanelRect (Foxjet3d::Box::CBoxParams & p) const
{
	using namespace Foxjet3d::Box;

	CCache * pCache = (CCache *)(DWORD)&m_cache;

	if (!pCache->IsValid (p))
		pCache->Build (* this, p);

	return pCache->GetPanelRect (* this, p);
}

inline CRect Foxjet3d::Box::CBox::CalcPanelRect (Foxjet3d::Box::CBoxParams & p) const
{
	using namespace Foxjet3d::Box;
	CPoint pt [8] = { CPoint (0, 0) };
	GetPoints (p, pt);
	return CRect (pt [0], pt [2]);
}

inline CRect Foxjet3d::Box::CBox::CalcHeadRect (const Panel::CBoundary & bounds, Foxjet3d::Box::CBoxParams & p, bool bClip) const
{
	using namespace Foxjet3d::Box;
	// TODO: this only works if the head is on the face panel

	CPoint pt [8] = { CPoint (0, 0) };

	GetPoints (p, pt);

	int l = pt [0].x;
	int r = pt [1].x;
	int b = pt [3].y - (int)((double)ThousandthsToLogical (bounds.GetY ()) * p.m_rZoom);
	int t = b - (int)((double)bounds.Height () * p.m_rZoom);
	CRect rcHead (l, t, r, b);
	CRect rcFace (pt [0], pt [2]);

	if (bClip)
		rcHead.IntersectRect (rcHead, rcFace);

	return rcHead;
}

inline void Foxjet3d::Box::CBox::DrawCrosshairs (CDC & dc, Foxjet3d::Box::CBoxParams & p, const CRect & rcHead)
{
	using namespace Foxjet3d::Box;
	using namespace Color;

	const CSize size = p.m_crosshairs.m_size;
	CPoint pt = (CPoint)p.m_crosshairs.m_pt + rcHead.TopLeft ();
	
	if (!rcHead.PtInRect (pt)) {
		p.m_crosshairs.m_pt = CPoint (0, 0);
		pt = rcHead.TopLeft ();
	}

	CPen pen;
	const CPoint ptCenter = pt - dc.GetWindowOrg ();
	const CPoint ptCrosshair [2][2] = 
	{
		{ CPoint (0, (size.cx * 2)) + ptCenter,		CPoint (0, -(size.cy * 2)) + ptCenter,	},
		{ CPoint ((size.cx * 2), 0) + ptCenter,		CPoint (-(size.cy * 2), 0) + ptCenter,	},
	};

	pen.CreatePen (PS_SOLID, 1, rgbDarkGray);
	CPen * pOldPen = dc.SelectObject (&pen);

	for (int i = 0; i < 2; i++) {
		dc.MoveTo (ptCrosshair [i][0]);
		dc.LineTo (ptCrosshair [i][1]);
	}

	dc.Ellipse (CRect (0, 0, size.cx, size.cy) + ptCenter - CPoint (size.cx / 2, size.cy / 2));

	dc.SelectObject (pOldPen);
}

#endif //__BOX_INLINE__
