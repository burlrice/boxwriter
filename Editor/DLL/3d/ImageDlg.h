#if !defined(AFX_IMAGEDLG_H__DCE3D10D_DCF9_43D5_BFDB_3BAE3B9879FC__INCLUDED_)
#define AFX_IMAGEDLG_H__DCE3D10D_DCF9_43D5_BFDB_3BAE3B9879FC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImageDlg.h : header file
//

#include "3dApi.h"
#include "Utils.h"

namespace Foxjet3d
{
	/////////////////////////////////////////////////////////////////////////////
	// CImageDlg dialog

	class _3D_API CImageDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CImageDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CImageDlg)
		CString	m_strPath;
		int		m_nPanel;
		//}}AFX_DATA

		ULONG m_lAttributes;
		CArray <UINT, UINT &> m_vInUse;

	protected:
		CBitmap m_bmpImage [5];
		HANDLE m_hThread;


	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CImageDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		void UpdateUI();
		virtual void OnOK();
		static ULONG CALLBACK WaitForEdit (LPVOID lpData);

		// Generated message map functions
		//{{AFX_MSG(CImageDlg)
		afx_msg void OnEdit();
		afx_msg void OnBrowse();
		afx_msg void OnAttributeClick();
	afx_msg void OnClose();
	//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};

}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMAGEDLG_H__DCE3D10D_DCF9_43D5_BFDB_3BAE3B9879FC__INCLUDED_)
