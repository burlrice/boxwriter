#if !defined(AFX_LINECONFIGDLG_H__B2A565B1_B36C_48D5_9AEB_710A9CA1AFCB__INCLUDED_)
#define AFX_LINECONFIGDLG_H__B2A565B1_B36C_48D5_9AEB_710A9CA1AFCB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LineConfigDlg.h : header file
//

#include "3dApi.h"
#include "OdbcDatabase.h"
#include "Database.h"
#include "ListCtrlImp.h"
#include "RoundButtons.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CLineConfigDlg dialog

namespace Foxjet3d
{
	class CLineDlg;

	namespace LineConfigDlg
	{
		class _3D_API CLineItem : 
			public ItiLibrary::ListCtrlImp::CItem,
			public FoxjetDatabase::LINESTRUCT
		{
		public:
			CLineItem (FoxjetDatabase::LINESTRUCT & line);
			virtual ~CLineItem ();

			virtual CString GetDispText (int nColumn) const;
		};

		class _3D_API CLineConfigDlg : public FoxjetCommon::CEliteDlg
		{
		// Construction
		public:
		
			CLineConfigDlg (FoxjetDatabase::COdbcDatabase & db, CWnd* pParent = NULL);   // standard constructor
			virtual ~CLineConfigDlg ();

			ULONG m_lPrinterID;


		// Dialog Data
			//{{AFX_DATA(CLineConfigDlg)
				// NOTE: the ClassWizard will add data members here
			//}}AFX_DATA

			FoxjetDatabase::COdbcDatabase & m_db;

		// Overrides
			// ClassWizard generated virtual function overrides
			//{{AFX_VIRTUAL(CLineConfigDlg)
			protected:
			virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
			//}}AFX_VIRTUAL

		public:
			static void UpdateSettingsTable (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::LINESTRUCT & line, const CLineDlg & dlg);
			static bool LineNamesMatch (FoxjetDatabase::COdbcDatabase & db); // sw0867

		// Implementation
		protected:
			ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
			FoxjetUtils::CRoundButtons m_buttons;
			BOOL m_bCoupled;
			BOOL m_bLean;

			void UpdateSettingsTable (const FoxjetDatabase::LINESTRUCT & line, const CLineDlg & dlg);

			virtual void OnOK ();
			void FillLineCtrl ();
			void UpdateUI ();

			// Generated message map functions
			//{{AFX_MSG(CLineConfigDlg)
			afx_msg void OnAdd();
			afx_msg void OnDelete();
			afx_msg void OnEdit();
			virtual BOOL OnInitDialog();
			afx_msg void OnDblclkLine(NMHDR* pNMHDR, LRESULT* pResult);
		//}}AFX_MSG

			DECLARE_MESSAGE_MAP()
		};
	};
}; //namespace Foxjet3d


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LINECONFIGDLG_H__B2A565B1_B36C_48D5_9AEB_710A9CA1AFCB__INCLUDED_)
