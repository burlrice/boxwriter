#if !defined(AFX_HEADTYPEDLG_H__ADA36E43_63C2_4482_A0E7_72394883F075__INCLUDED_)
#define AFX_HEADTYPEDLG_H__ADA36E43_63C2_4482_A0E7_72394883F075__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HeadTypeDlg.h : header file
//

#include "resource.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CHeadTypeDlg dialog

class CHeadTypeDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CHeadTypeDlg(CWnd* pParent = NULL);   // standard constructor

public:
	virtual void OnOK();

	int m_nHeadType;

// Dialog Data
	//{{AFX_DATA(CHeadTypeDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHeadTypeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CHeadTypeDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkType();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEADTYPEDLG_H__ADA36E43_63C2_4482_A0E7_72394883F075__INCLUDED_)
