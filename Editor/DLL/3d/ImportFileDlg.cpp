// ImportFileDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "ImportFileDlg.h"
#include "Extern.h"
#include "Utils.h"
#include "ListCtrlImp.h"
#include "ExportFileDlg.h"
#include "Task.h"
#include "BoxUsageDlg.h"
#include "Export.h"
#include "WinMsg.h"
#include "ImportResultsDlg.h"
#include "ProgressDlg.h"
#include "FieldDefs.h"
#include "resource.h"
#include "Registry.h"
#include "Parse.h"

using namespace Foxjet3d;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ListGlobals;
using namespace ItiLibrary;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

int _3D_API Foxjet3d::Import (CWnd * pParent)
{
	return CImportFileDlg (pParent).DoModal ();
}

bool CALLBACK ImportBarcodeParams (const CString & strFile, CWnd * pParent)
{
	CImportFileDlg dlg (pParent);
	
	dlg.m_strFile = strFile;

	return dlg.DoModal () == IDOK ? true : false;
}

/////////////////////////////////////////////////////////////////////////////
// CImportFileDlg dialog


CImportFileDlg::CImportFileDlg(CWnd* pParent /*=NULL*/)
:	m_bUIEnabled (true),
	m_hThread (NULL),
	m_nOrphaned (0),
	FoxjetCommon::CEliteDlg(IDD_IMPORT_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CImportFileDlg)
	m_bConfig = FALSE;
	m_bTasks = TRUE;
	m_bDeleteExistingTasks = FALSE;
	//}}AFX_DATA_INIT

	m_params.m_pDlg = NULL;
	m_params.m_pDB = NULL;
}


void CImportFileDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CImportFileDlg)
	DDX_Check(pDX, CHK_CONFIG, m_bConfig);
	DDX_Check(pDX, CHK_TASKS, m_bTasks);
	DDX_Check(pDX, CHK_TASKSDELETEEXISTING, m_bDeleteExistingTasks);
	//}}AFX_DATA_MAP

	//if (IsMatrix ()) 
	{
		m_buttons.DoDataExchange (pDX, BTN_BROWSE,	IDB_BROWSE);
		m_buttons.DoDataExchange (pDX, BTN_IMPORT,	IDB_OK);
		m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
		m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);
	}
}


BEGIN_MESSAGE_MAP(CImportFileDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CImportFileDlg)
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_BN_CLICKED(BTN_IMPORT, OnImport)
	ON_BN_CLICKED(CHK_TASKS, OnTasks)
	ON_BN_CLICKED(CHK_CONFIG, OnConfig)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(TXT_FILENAME, UpdateUI)
	ON_REGISTERED_MESSAGE (CProgressDlg::m_wmEndProgress, OnEndProgress)
	ON_REGISTERED_MESSAGE (CProgressDlg::m_wmMsgBox, OnMsgBox)
	ON_REGISTERED_MESSAGE (CProgressDlg::m_wmAddResult,	OnAddResult)
	ON_REGISTERED_MESSAGE (CProgressDlg::m_wmAddBitmapFile,	OnAddBitmapFile)
	ON_REGISTERED_MESSAGE (CProgressDlg::m_wmAddBitmapDir, OnAddBitmapDir)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImportFileDlg message handlers

BOOL CImportFileDlg::OnInitDialog() 
{
	CString strDefFile = FoxjetDatabase::GetProfileString (defElements.m_strRegSection + _T ("\\Settings\\CExportDlg"), _T ("Filename"), 
		GetHomeDir () + _T ("\\Export.txt"));
	CString strFile = FoxjetDatabase::GetProfileString (defElements.m_strRegSection + _T ("\\Settings\\CImportDlg"), _T ("Filename"), 
		strDefFile);

	FoxjetCommon::CEliteDlg::OnInitDialog();

//#ifdef _DEBUG
//	strFile = _T ("C:\\Program Files (x86)\\InkJet\\prds");
//	strFile = _T ("C:\\Dev\\Diagraph\\Unified")	 _T ("\\CHINA5-4900.yml");
//#endif

	SetDlgItemText (TXT_FILENAME, strFile);
	UpdateUI ();
	OnTasks ();
	OnConfig ();

	if (m_strFile.GetLength ()) {
		SetDlgItemText (TXT_FILENAME, m_strFile);
		OnImport ();
	}

	if (CButton * p = (CButton *)GetDlgItem (CHK_REMAP_LOGOS)) {
		CString strKey = defElements.m_strRegSection + _T ("CImportFileDlg");
		CString strLocal = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER,
			ListGlobals::defElements.m_strRegSection + _T ("\\Settings"), _T ("LocalLogosFolder"), GetHomeDir () + _T ("\\Logos"));

		p->SetCheck (FoxjetDatabase::GetProfileInt (strKey, _T ("remap"), 0));
		SetDlgItemText (TXT_REMAP_LOGOS, strLocal);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CImportFileDlg::OnBrowse() 
{
	CString strFile;
	CString strFilter = 
		CString (_T ("Text files (*.txt)|*.txt|")) +
		_T ("Registry files (*") + CString (CExportFileDlg::m_lpszRegExt) + _T (")|*") + CString (CExportFileDlg::m_lpszRegExt) + _T ("|") +
		CString (_T ("All files (*.*)|*.*||")); 
	
	TRACEF (strFilter);
	GetDlgItemText (TXT_FILENAME, strFile);

	CFileDialog dlg (TRUE, _T ("*.*"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		strFilter, this);
	
	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();
		SetDlgItemText (TXT_FILENAME, strFile);
		UpdateUI ();
	}
}

void CImportFileDlg::OnTasks() 
{
	CButton * pTasks = (CButton *)GetDlgItem (CHK_TASKS);
	CButton * pDelete = (CButton *)GetDlgItem (CHK_TASKSDELETEEXISTING);

	ASSERT (pTasks);
	ASSERT (pDelete);

	BOOL bEnable = pTasks->GetCheck () == 1;

	pDelete->EnableWindow (bEnable);

	if (!bEnable)
		pDelete->SetCheck (0);
}

void CImportFileDlg::OnConfig() 
{
	CButton * pConfig = (CButton *)GetDlgItem (CHK_CONFIG);
	CWnd * pNote = GetDlgItem (LBL_CONFIG);
	CString str;

	ASSERT (pConfig);
	ASSERT (pNote);

	str.Format (LoadString (IDS_RESTARTREQUIRED), GetAppTitle ());

	pNote->SetWindowText (str);
	pNote->ShowWindow (pConfig->GetCheck () == 1 ? SW_SHOW : SW_HIDE);
}

void CImportFileDlg::OnImport() 
{
	CString strFile;

	GetDlgItemText (TXT_FILENAME, strFile);
	UpdateData (TRUE);

	if (::GetFileAttributes (strFile) == -1) {
		CString str;

		str.Format (LoadString (IDS_FILEDOESNOTEXIST), strFile);
		MsgBox (* this, str, NULL, MB_ICONINFORMATION);

		if (m_strFile.GetLength ())
			EndDialog (IDCANCEL);

		return;
	}

	if (strFile.Find (CExportFileDlg::m_lpszRegExt) != -1) {
		HKEY hKey = NULL;
		CString strKey = _T ("Software\\FoxJet");

		VERIFY (EnablePrivilege (SE_RESTORE_NAME));
		VERIFY (EnablePrivilege (SE_BACKUP_NAME));

		LONG lResult = ::SHDeleteKey (HKEY_CURRENT_USER, strKey);

		if (lResult != ERROR_SUCCESS) {
			MsgBox (* this, _T ("SHDeleteKey:\n") + FormatMessage (lResult));
			return;
		}

		lResult = ::RegCreateKey (HKEY_CURRENT_USER, strKey, &hKey);

		if (lResult != ERROR_SUCCESS) {
			MsgBox (* this, _T ("RegCreateKey:\n") + FormatMessage (lResult));
			return;
		}

		lResult = ::RegRestoreKey (hKey, strFile, 0);
		::RegCloseKey (hKey);

		if (lResult == ERROR_SUCCESS) 
			EndDialog (IDOK);
		else
			MsgBox (* this, _T ("RegRestoreKey:n") + FormatMessage (lResult));

		return;
	}

	ASSERT (m_params.m_pDlg == NULL);
	ASSERT (m_hThread == NULL);

	if (m_strFile.GetLength () == 0) {
		if (!m_bConfig && !m_bTasks) {
			MsgBox (* this, LoadString (IDS_NOIMPORTOPTIONSSELCTED), NULL, MB_ICONINFORMATION);
			return;
		}

		if (!m_bConfig && m_bTasks) {
			if (MsgBox (* this, LoadString (IDS_IMPORTTASKSWITHOUTCONFIG), 
				LoadString (IDS_WARNING), MB_ICONQUESTION | MB_ICONWARNING | MB_YESNO) == IDNO) 
			{
				return;
			}
		}
		else if (m_bConfig && !m_bTasks) {
			if (MsgBox (* this, LoadString (IDS_IMPORTCONFIGWITHOUTTASKS), 
				LoadString (IDS_WARNING), MB_ICONQUESTION | MB_ICONWARNING | MB_YESNO) == IDNO) 
			{
				return;
			}
		}

		if (MsgBox (* this, LoadString (IDS_EXPORTBEFOREIMPORT), 
			NULL, MB_ICONQUESTION | MB_YESNO) == IDYES) 
		{
			CExportFileDlg dlg (this);

			dlg.m_strFilename = CreateTempFilename (_T ("."), _T ("Export"), _T ("txt"));

			if (dlg.DoModal () == IDCANCEL)	
				return;
		}

		if (!IsControl ()) {
			if (FoxjetDatabase::IsRunning (ISPRINTERRUNNING)) {
				if (MsgBox (* this, LoadString (IDS_SHUTDOWNCONTROL), NULL, MB_ICONQUESTION | MB_YESNO) == IDYES) {
					DWORD dwResult = 0;
					LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_SHUTDOWNAPP, SHUTDOWN_CONTROL, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 10000, &dwResult);
				}
				else 
					return;
			}
		}
	}

	if (m_strFile.GetLength () == 0) BEGIN_TRANS (ListGlobals::GetDB ());

	BeginWaitCursor ();
	DWORD dwImport = 
		(m_bConfig				? IMPORT_CONFIG	:				0) |
		(m_bTasks				? IMPORT_TASKS :				0) |
		(m_bDeleteExistingTasks	? IMPORT_TASKSDELETEEXISTING :	0);
	DWORD dwThreadID;

	m_params.m_pDlg		= new CProgressDlg (this);
	m_params.m_pDB		= &ListGlobals::GetDB ();
	m_params.m_strFile	= strFile;
	m_params.m_dwFlags	= dwImport;

	if (CButton * p = (CButton *)GetDlgItem (CHK_REMAP_LOGOS)) {
		if (p->GetCheck ())
			GetDlgItemText (TXT_REMAP_LOGOS, m_params.m_strLogosDir);
	}

	//{
	//	HINSTANCE hRestore = ::AfxGetResourceHandle ();
	//	::AfxSetResourceHandle (GetInstanceHandle ());
	//	VERIFY (m_params.m_pDlg->Create (IDD_PROGRESS_MATRIX, this));
	//	::AfxSetResourceHandle (hRestore);
	//}
	VERIFY (m_params.m_pDlg->Create (this));

	m_params.m_pDlg->ShowWindow (SW_SHOW);
	m_params.m_pDlg->BringWindowToTop ();

	m_nOrphaned = CountOrphanedRecords ();

	VERIFY (m_hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)Import, (LPVOID)&m_params, 0, &dwThreadID));
	EnableUI (false);
}

static int Find (const FoxjetCommon::CImportArray & v, CString strType)
{
	TRACEF (strType);

	for (int i = 0; i < v.GetSize (); i++) {
		IMPORTSTRUCT s = v [i];

		TRACEF (s.m_strStruct);

		if (!s.m_strStruct.CompareNoCase (strType))
			if (s.m_nImported > 0)
				return i;
	}

	return -1;
}

int CImportFileDlg::CountOrphanedRecords ()
{
	int nResult = 0;

	try
	{
		COdbcRecordset rst (ListGlobals::GetDB ());
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s].[%s] NOT IN (SELECT [%s].[%s] FROM [%s]);"), 
			Messages::m_lpszTable,
			Messages::m_lpszTable, Messages::m_lpszHeadID,
			Heads::m_lpszTable, Heads::m_lpszID,
			Heads::m_lpszTable);

		rst.Open (str);

		nResult = rst.CountRecords ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return nResult;
}

LRESULT CImportFileDlg::OnEndProgress (WPARAM wParam, LPARAM lParam)
{
	DWORD dwExitCode = STILL_ACTIVE;

	do {
		if (GetExitCodeThread (m_hThread, &dwExitCode)) {
			if (dwExitCode == STILL_ACTIVE) {
				::Sleep (1000);
			}
		}
	}
	while (dwExitCode == STILL_ACTIVE);

	{ CString str; str.Format (_T ("dwExitCode: %d"), dwExitCode); TRACER (str); }

	/*
	{
		HANDLE hThread = m_hThread;

		if (::WaitForSingleObject (m_hThread, 5000) == WAIT_OBJECT_0)
			m_hThread = NULL;
		else {
			TRACEF (_T ("m_hThread still active"));
			ASSERT (0);
		}

		for (int i = 0; i < 5; i++) {
			dwExitCode = -1;

			if (GetExitCodeThread (hThread, &dwExitCode))
				break;
			else { 
				TRACEF (FormatMessage (::GetLastError ())); 
				::Sleep (1000);
			}
		}

		ASSERT (dwExitCode != -1);
	}
	*/

	delete m_params.m_pDlg;
	m_params.m_pDlg = NULL;

	{ // TODO: rem
		CString str;

		str.Format (_T ("OnEndProgress: %d, %d: %s%s"),
			wParam, 
			lParam,
			wParam == IDCANCEL	? _T ("IDCANCEL")	: _T (""),
			wParam == IDOK		? _T ("IDOK")		: _T (""));
		TRACEF (str);
	}

	EnableUI (true);
	EndWaitCursor ();
	
	if (dwExitCode == CImportFileDlg::IMPORTTHREAD_SUCCESS) {
		int nOrphaned = CountOrphanedRecords ();

		if (nOrphaned) {
			CString str;

			str.Format (LoadString (IDS_MESSAGESORPHANED), nOrphaned, m_nOrphaned);

			if (MsgBox (* this, str, NULL, MB_ICONERROR | MB_YESNO) == IDNO) {
				if (m_strFile.GetLength () == 0) ROLLBACK_TRANS (ListGlobals::GetDB ());
				return 0;
			}
		}

		if (Find (/* m_params. */ m_vResult, _T ("SETTINGSSTRUCT")))
			::PostMessage (HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, 0);

		if (m_params.m_dwFlags & IMPORT_CONFIG) {
			int nLINESTRUCT				= Find (/* m_params. */ m_vResult, _T ("LINESTRUCT"));
			int nPANELSTRUCT			= Find (/* m_params. */ m_vResult, _T ("PANELSTRUCT"));
			int nHEADSTRUCT				= Find (/* m_params. */ m_vResult, _T ("HEADSTRUCT"));
			int nIMAGESTRUCT			= Find (/* m_params. */ m_vResult, _T ("IMAGESTRUCT"));
			int nSETTINGSSTRUCT			= Find (/* m_params. */ m_vResult, _T ("SETTINGSSTRUCT"));
			int nSECURITYGROUPSTRUCT	= Find (/* m_params. */ m_vResult, _T ("SECURITYGROUPSTRUCT"));	
			int nUSERSTRUCT				= Find (/* m_params. */ m_vResult, _T ("USERSTRUCT"));			
			int nOPTIONSSTRUCT			= Find (/* m_params. */ m_vResult, _T ("OPTIONSSTRUCT"));			
			int nOPTIONSLINKSTRUCT		= Find (/* m_params. */ m_vResult, _T ("OPTIONSLINKSTRUCT"));		
			//int nSHIFTSTRUCT			= Find (/* m_params. */ m_vResult, _T ("SHIFTSTRUCT"));			
			int nREPORTSTRUCT			= Find (/* m_params. */ m_vResult, _T ("REPORTSTRUCT"));			
			int nBCSCANSTRUCT			= Find (/* m_params. */ m_vResult, _T ("BCSCANSTRUCT"));			
			bool bCritical = 
				nLINESTRUCT				== -1 ||
				nPANELSTRUCT			== -1 ||
				nHEADSTRUCT				== -1;
				//nIMAGESTRUCT			== -1;
			bool bQuestionable = 
				nSETTINGSSTRUCT			== -1 ||
				nSECURITYGROUPSTRUCT	== -1 ||
				nUSERSTRUCT				== -1 ||
				nOPTIONSSTRUCT			== -1 ||
				nOPTIONSLINKSTRUCT		== -1;
				//nSHIFTSTRUCT			== -1;
				//nREPORTSTRUCT			== -1 ||
				//nBCSCANSTRUCT			== -1;

			if (bCritical) {
				MsgBox (* this, LoadString (IDS_CRITICALCONFIGMISSING), NULL, MB_ICONERROR);
				if (m_strFile.GetLength () == 0) ROLLBACK_TRANS (ListGlobals::GetDB ());
				return 0;
			}
			else if (bQuestionable) {
				CString str = LoadString (IDS_QUESTIONABLECONFIGMISSING);

				if (nSETTINGSSTRUCT			== -1) str += Settings::m_lpszTable			+ CString (_T ("\n"));
				if (nSECURITYGROUPSTRUCT	== -1) str += SecurityGroups::m_lpszTable	+ CString (_T ("\n"));
				if (nUSERSTRUCT				== -1) str += Users::m_lpszTable			+ CString (_T ("\n"));
				if (nOPTIONSSTRUCT			== -1) str += Options::m_lpszTable			+ CString (_T ("\n"));
				if (nOPTIONSLINKSTRUCT		== -1) str += OptionsToGroup::m_lpszTable	+ CString (_T ("\n"));
				//if (nSHIFTSTRUCT			== -1) str += Shifts::m_lpszTable			+ CString (_T ("\n"));

				str += _T ("\n") + LoadString (IDS_CONTINUE);

				if (MsgBox (* this, str, NULL, MB_ICONERROR | MB_YESNO) == IDNO) {
					if (m_strFile.GetLength () == 0) ROLLBACK_TRANS (ListGlobals::GetDB ());
					return 0;
				}
			}
		}

		CImportResultsDlg dlg;

		dlg.m_vImport = /* m_params. */ m_vResult;
		dlg.m_strImportFile = m_params.m_strFile;
		Copy (dlg.m_vBitmapFiles, /* m_params. */ m_vBitmapFiles);
		Copy (dlg.m_vBitmapDirs, /* m_params. */ m_vBitmapDirs);

		dlg.m_bRestart = m_bConfig ? true : false;

		BeginWaitCursor ();
		if (m_strFile.GetLength () == 0) COMMIT_TRANS (ListGlobals::GetDB ());

		dlg.DoModal ();
		OnOK ();

		if (CButton * p = (CButton *)GetDlgItem (CHK_REMAP_LOGOS)) {
			CString strKey = defElements.m_strRegSection + _T ("CImportFileDlg");
			FoxjetDatabase::WriteProfileInt (strKey, _T ("remap"), p->GetCheck ());
		}

		if (m_bConfig) {
			DWORD dwResult = 0;

			LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_SHUTDOWNAPP, SHUTDOWN_ALL, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 10000, &dwResult);
		}

		EndWaitCursor ();
		return 0;
	}
	else {
		CString str;

		str.Format (LoadString (IDS_IMPORTEDFAILED) + _T (" (") + ToString ((int)dwExitCode) + _T (")"), m_params.m_strFile);

		MsgBox (* this, str, NULL, MB_ICONERROR);
	}

	if (m_strFile.GetLength () == 0) ROLLBACK_TRANS (ListGlobals::GetDB ());

	return 0;
}

void CImportFileDlg::OnOK ()
{
	ASSERT (m_bUIEnabled);
	FoxjetCommon::CEliteDlg::OnOK ();
}

void CImportFileDlg::OnCancel ()
{
	ASSERT (m_bUIEnabled);
	FoxjetCommon::CEliteDlg::OnCancel ();
}

void CImportFileDlg::EnableUI (bool bEnable)
{
	UINT nID [] = 
	{
		TXT_FILENAME,
		BTN_BROWSE,
		CHK_CONFIG,
		CHK_TASKS,
		CHK_TASKSDELETEEXISTING,
		BTN_IMPORT,
	};

	m_bUIEnabled = bEnable;

	for (int i = 0; i < ARRAYSIZE (nID); i++) 
		if (CWnd * p = GetDlgItem (nID [i]))
			p->EnableWindow (m_bUIEnabled ? TRUE : FALSE);
}

LRESULT CImportFileDlg::OnMsgBox (WPARAM wParam, LPARAM lParam)
{
	int n = 0;

	if (CString * pstr = (CString *)wParam) {
		n = MsgBox (m_hWnd, * pstr, _T (""), lParam);
		delete pstr;
	}

	return n;
}

LRESULT CImportFileDlg::OnAddResult (WPARAM wParam, LPARAM lParam)
{
	if (IMPORTSTRUCT * p = (IMPORTSTRUCT *)wParam) {
		TRACEF (p->m_strStruct + _T (": ") + ToString (p->m_nParsed) + _T (": ") + ToString (p->m_nImported));
		m_vResult.Add (* p);
		delete p;
	}

	return 0;
}

LRESULT CImportFileDlg::OnAddBitmapFile (WPARAM wParam, LPARAM lParam)
{
	if (CString * pDir = (CString *)wParam) {
		if (CString * pCount = (CString *)lParam) {
			m_vBitmapFiles.SetAt (* pDir, * pCount);
			delete pCount;
		}

		delete pDir;
	}

	return 0;
}

LRESULT CImportFileDlg::OnAddBitmapDir (WPARAM wParam, LPARAM lParam)
{
	if (CString * pDir = (CString *)wParam) {
		if (CString * pCount = (CString *)lParam) {
			m_vBitmapDirs.SetAt (* pDir, * pCount);
			delete pCount;
		}

		delete pDir;
	}

	return 0;
}

void CImportFileDlg::UpdateUI ()
{
	CString str;

	GetDlgItemText (TXT_FILENAME, str);

	bool bRegistry = str.Find (CExportFileDlg::m_lpszRegExt) != -1 ? true : false;

	UINT n [] = 
	{
		CHK_CONFIG,
		CHK_TASKS,
		CHK_TASKSDELETEEXISTING,
	};

	for (int i = 0; i < ARRAYSIZE (n); i++) {
		if (CWnd * p = GetDlgItem (n [i]))
			p->EnableWindow (!bRegistry);
	}
}