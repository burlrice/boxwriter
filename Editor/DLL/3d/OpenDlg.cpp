// OpenDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "OpenDlg.h"
#include "Debug.h"
#include "Database.h"
#include "Extern.h"
#include "Registry.h"
#include "ItiLibrary.h"
#include "Color.h"
#include "Coord.h"
#include "Element.h"
#include "Compare.h"
#include "HeadConfigDlg.h"
#include "resource.h"
#include "Parse.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace ItiLibrary;
using namespace FoxjetDatabase;
using namespace Color;
using namespace Foxjet3d;
using namespace OpenDlg;

#define SS_PREVIEW	100

static class CPreviewCache : public CArray <CTaskPreview *, CTaskPreview *> 
{
public:
	CPreviewCache ();
	virtual ~CPreviewCache ();

} vCache;

CPreviewCache::CPreviewCache ()
{
}

CPreviewCache::~CPreviewCache ()
{
	for (int i = 0; i < GetSize (); i++) {
		CTaskPreview * p = GetAt (i);
		delete p;
	}

	RemoveAll ();
}

/////////////////////////////////////////////////////////////////////////////
// COpenDlg dialog

COpenDlg::COpenDlg(CWnd* pParent /*=NULL*/)
:	m_nPanel (0),
	CSaveDlg(NULL, IDD_OPEN_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(COpenDlg)
	m_bReadOnly = FALSE;
	m_bPreview = FALSE;
	//}}AFX_DATA_INIT
}

COpenDlg::COpenDlg (UINT nIdTemplate, CWnd* pParent)
:	m_nPanel (0),
	m_lLastLineID (-1),
	CSaveDlg (NULL, nIdTemplate, pParent)
{
	m_bReadOnly = FALSE;
	m_bPreview = FALSE;
}

COpenDlg::~COpenDlg ()
{
}

void COpenDlg::DoDataExchange(CDataExchange* pDX)
{
	CSaveDlg::DoDataExchange(pDX);
	
	if (GetDlgItem (CHK_READONLY))
		DDX_Check(pDX, CHK_READONLY, m_bReadOnly);
	
	//{{AFX_DATA_MAP(COpenDlg)
	DDX_Check(pDX, CHK_PREVIEW, m_bPreview);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COpenDlg, CSaveDlg)
	//{{AFX_MSG_MAP(COpenDlg)
	ON_BN_CLICKED(CHK_PREVIEW, OnPreview)
	ON_WM_CREATE()
	ON_WM_DRAWITEM()
	//}}AFX_MSG_MAP
	ON_NOTIFY(LVN_ITEMCHANGED, LV_TASKS, OnItemchangedTasks)
	ON_CBN_SELCHANGE(CB_LINE, OnSelchangeLine)
	ON_CBN_SELCHANGE(CB_PANEL, OnSelchangePanel)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COpenDlg message handlers

BOOL COpenDlg::OnInitDialog() 
{
	CRect rc;
	CStatic * p = (CStatic *)GetDlgItem (IDC_PREVIEW);
	CButton * pPreview = (CButton *)GetDlgItem (CHK_PREVIEW);
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB);
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);

	ASSERT (p);
	ASSERT (pPreview);
	ASSERT (pLine);

	CSaveDlg::OnInitDialog();
	if (pTab)
		pTab->EnableWindow (TRUE);
	pLine->EnableWindow (TRUE);

	p->ShowWindow (SW_HIDE);
	p->GetWindowRect (&rc);
	ScreenToClient (&rc);
	pPreview->SetCheck (IsPreviewEnabled () ? 1 : 0);
	BOOL bSetWindowPos = m_wndPreview.SetWindowPos (NULL, rc.left, rc.top,
		rc.Width (), rc.Height (), SWP_NOZORDER | SWP_SHOWWINDOW);
	ASSERT (bSetWindowPos);

	OnSelchangeLine ();
	OnPreview ();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void COpenDlg::OnOK ()
{
	CComboBox * pLine = (CComboBox * )GetDlgItem (CB_LINE);

	ASSERT (pLine);

	if (GetSelectedFilenames (m_vFiles, m_type)) {
		if (m_vFiles.GetSize ()) {
			m_lLineID = pLine->GetItemData (pLine->GetCurSel ());
			FoxjetCommon::CEliteDlg::OnOK ();
		}
		else
			MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
	}
	else {
		CString str, strFile;
		int nFilenameCtrl;
		int nTab = CSaveDlg::m_nTaskTab;
		
		if (CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB))
			nTab = pTab->GetCurSel ();

		if (nTab == CSaveDlg::m_nMsgTab) {
			nFilenameCtrl = TXT_MESSAGE;
			m_type = FoxjetFile::MESSAGE;
		}
		else if (nTab == CSaveDlg::m_nTaskTab) {
			nFilenameCtrl = TXT_TASK;
			m_type = FoxjetFile::TASK;
		} 
		else { 
			ASSERT (0); 
		}
		
		GetDlgItemText (nFilenameCtrl, strFile);
		str.Format (LoadString (IDS_FILEDOESNOTEXIST), strFile);
		MsgBox (* this, str, LoadString (IDS_ERROR), MB_ICONINFORMATION);
	}
}

bool COpenDlg::GetSelectedFilenames (CStringArray & vFiles, FoxjetFile::DOCTYPE & type) 
{
	//CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB);
	//ASSERT (pTab);
	int nTab = CSaveDlg::m_nTaskTab;//pTab->GetCurSel ();
	ListCtrlImp::CListCtrlImp * pLCI;
	UINT nFilenameCtrl;
	CComboBox * pCB;
	CString strPrefix;

	vFiles.RemoveAll ();

	pLCI = &m_lviTask;
	nFilenameCtrl = TXT_TASK;
	pCB = (CComboBox * )GetDlgItem (CB_LINE);
	type = FoxjetFile::TASK;

	CLongArray sel = GetSelectedItems (pLCI->GetListCtrl ());
	ASSERT (pCB);

	if (pCB->GetLBTextLen (pCB->GetCurSel ()) > 0)
		pCB->GetLBText (pCB->GetCurSel (), strPrefix);

	// no sel in the list ctrl, so try selecting what the user has typed
	if (sel.GetSize () == 0) {
		CString str;

		GetDlgItemText (nFilenameCtrl, str);
		Select (strPrefix + _T ("\\") + str, type, true);
		sel = GetSelectedItems (pLCI->GetListCtrl ());
	}

	for (int i = 0; i < sel.GetSize (); i++) {
		const CString strSuffix = FoxjetFile::GetFileExt (type);
		ULONG lID = _tcstoul (pLCI->GetListCtrl ().GetItemText (sel [i], CSaveDlg::m_nIDIndex), NULL, 10);
		const TASKSTRUCT * pData = GetTask (GetSelectedLineID (), lID);

		ASSERT (pData);

		vFiles.Add (strPrefix 
			+ _T ("\\") + pData->m_strName 
			+ _T (".") + strSuffix);
	}

	return (vFiles.GetSize () > 0);
}

void COpenDlg::OnPreview() 
{
	CComboBox * pPanel = (CComboBox *)GetDlgItem (CB_PANEL);
	CLongArray sel = GetSelectedItems (m_lviTask.GetListCtrl ());

	ASSERT (pPanel);

	int nIndex = pPanel->GetCurSel ();

	pPanel->EnableWindow (IsPreviewEnabled () ? TRUE : FALSE);

	if (sel.GetSize ()) {
		m_selLast.RemoveAll ();
		m_selLast.Add (-1);
		OnSelchangeLine ();
		m_lviTask->SetItemState (sel [0], (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);	
		pPanel->SetCurSel (nIndex != CB_ERR ? nIndex : 0);
		OnItemchangedTasks (NULL, NULL);
		//OnSelchangePanel ();
	}
}

int COpenDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CSaveDlg::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	BOOL bCreate = m_wndPreview.Create (_T (""), 
		SS_SUNKEN | SS_GRAYRECT | SS_OWNERDRAW, 
		CRect (), this, SS_PREVIEW);

	ASSERT (bCreate);
	
	return 0;
}

OpenDlg::CTaskPreview * COpenDlg::Find (const FoxjetDatabase::TASKSTRUCT & task, CDC & dc, CWnd * pWnd)
{
	for (int i = 0; i < ::vCache.GetSize (); i++) {
		CTaskPreview * pPreview = ::vCache [i];

		if (pPreview->m_task.m_lID == task.m_lID) {
			if ((pPreview->m_task) == task)
				return pPreview;
			else {
				::vCache.RemoveAt (i);
				delete pPreview;
				break;
			}
		}
	}

	if (pWnd)
		pWnd->BeginWaitCursor ();

	CTaskPreview * p = new CTaskPreview (task, dc, CRect (0, 0, 1, 1), CMapStringToString ()); TRACEF ("TODO");
	::vCache.Add (p);
	
	for (int i = 0; i < ARRAYSIZE (p->m_bmp); i++) 
		p->m_bmp [i].Save (GetHomeDir () + _T ("\\Debug\\preview") + FoxjetDatabase::ToString (i) + _T (".bmp"));

	if (pWnd)
		pWnd->EndWaitCursor ();

	return p;
}

void COpenDlg::OnSelchangePanel()
{
	CComboBox * pPanel = (CComboBox *)GetDlgItem (CB_PANEL);

	ASSERT (pPanel);
	int nIndex = pPanel->GetCurSel ();
	
	if (nIndex != CB_ERR)
		m_nPanel = pPanel->GetItemData (nIndex) - 1;
	else
		m_nPanel = -1;

	InvalidatePreview ();
}

void COpenDlg::InvalidatePreview ()
{
	TRACEF (_T ("COpenDlg::InvalidatePreview"));
	REPAINT (&m_wndPreview);
}

void COpenDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if (nIDCtl == SS_PREVIEW) {
		DRAWITEMSTRUCT & dis = * lpDrawItemStruct;
		CDC dc;
		CRect rc = dis.rcItem;

		dc.Attach (dis.hDC);
		dc.FillSolidRect (rc, rgbDarkGray);

		if (IsPreviewEnabled ()) {
			CDC dcMem;
			CLongArray sel = GetSelectedItems (m_lviTask.GetListCtrl ());

			if (sel.GetSize () == 1 && m_nPanel >= 0 && m_nPanel < 6) {
				CBitmap bmp;

				BeginWaitCursor ();

				ULONG lID = _tcstoul (m_lviTask->GetItemText (sel [0], CSaveDlg::m_nIDIndex), NULL, 10);
				const TASKSTRUCT * pData = GetTask (GetSelectedLineID (), lID);

				ASSERT (pData);

				CTaskPreview * pPreview = Find (* pData, dc, this);

				ASSERT (pPreview);

				pPreview->m_bmp [m_nPanel].GetBitmap (bmp);

				if (bmp.m_hObject) {
					DIBSECTION ds;

					::ZeroMemory (&ds, sizeof (ds));

					if (bmp.GetObject (sizeof (ds), &ds)) {
						CSize size (ds.dsBm.bmWidth, ds.dsBm.bmHeight);

						if (size.cx > 0 && size.cy > 0) {
							VERIFY (dcMem.CreateCompatibleDC (&dc));
							CGdiObject * pOldObject = dcMem.SelectObject (&bmp);

							CSize stretch = CalcFitSize (dc, CRect (0, 0, size.cx, size.cy), rc);
							stretch = rc.Size ();
							dc.StretchBlt (0, 0, stretch.cx, stretch.cy, &dcMem, 0, 0, size.cx, size.cy, SRCCOPY);

							dcMem.SelectObject (pOldObject);
						}
					}
				}

				EndWaitCursor ();
			}
		}

		dc.Detach ();
	}
	else
		CSaveDlg::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

bool COpenDlg::IsPreviewEnabled() const
{
	CButton * p = (CButton *)GetDlgItem (CHK_PREVIEW);
	ASSERT (p);
	return p->GetCheck () == 1;
}

CSize COpenDlg::CalcFitSize(const CDC &dc, const CRect &rcObj, const CRect &rcWin)
{
	const CSize inch = CCoord::CoordToPoint (1, 1, INCHES, GetDefaultHead ()); // TODO
	double dScreenAdjust = (double)inch.cx / (double)inch.cy;
	int nRes [] = {
		dc.GetDeviceCaps (LOGPIXELSX),
		dc.GetDeviceCaps (LOGPIXELSY)
	};
	double dDpiAdjust = (double)nRes [0] / (double)nRes [1];
	double dStretch [] = { 1.0, 1.0 };

	// try to stretch to fit widthwise first
	dStretch [0] = (double)rcWin.Width () / (double)rcObj.Width ();
	dStretch [1] = dStretch [0] * dScreenAdjust * dDpiAdjust;

	int nHeight = (int)(dStretch [1] * (double)rcObj.Height());

	if (nHeight > rcWin.Height ()) {
		// image is clipped because it is too tall,
		// stretch to fit heightwise instead
		dStretch [1] = (double)rcWin.Height () / (double)rcObj.Height ();
		dStretch [0] = dStretch [1] / (dScreenAdjust * dDpiAdjust);
	}

	int cx = (int)((double)rcObj.Width () * dStretch [0]);
	int cy = (int)((double)rcObj.Height () * dStretch [1]);

	return CSize (cx, cy);
}

void COpenDlg::OnItemchangedTasks(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CComboBox * pPanel = (CComboBox *)GetDlgItem (CB_PANEL);
	COdbcDatabase & db = ListGlobals::GetDB ();
	CLongArray sel = GetSelectedItems (m_lviTask.GetListCtrl ());

	if (sel != m_selLast) {
		ASSERT (pLine);
		ASSERT (pPanel);

		pPanel->ResetContent ();

		ULONG lLineID = pLine->GetItemData (pLine->GetCurSel ());

		if (CButton * pPreview = (CButton *)GetDlgItem (CHK_PREVIEW)) {
			if (pPreview->GetCheck () == 1) {
				if (sel.GetSize ()) {
					int nIndex = sel [0];

					if (CTaskItem * pData = (CTaskItem *)m_lviTask.GetCtrlData (nIndex)) {
						lLineID = pData->m_pTask->m_lLineID;
					}
					else {
						ULONG lTaskID = _ttoi (m_lviTask->GetItemText (nIndex, 2));
						TASKSTRUCT task;

						GetTaskRecord (db, lTaskID, task);
						lLineID = task.m_lLineID;
					}
				}

				if (lLineID != CB_ERR && sel.GetSize () == 1) {
					if (m_lLastLineID != lLineID) {
						HeadConfigDlg::CPanel::GetPanels (db, lLineID, m_vBounds);
						m_lLastLineID = lLineID;
					}

					for (int i = 0; i < m_vBounds.GetSize (); i++) {
						HeadConfigDlg::CPanel & panel = m_vBounds [i];
						bool bPreview = panel.m_vCards.GetSize () ? true : false;

						if (ISVALVE ())
							bPreview |= panel.m_vValve.GetSize () ? true : false;

						if (bPreview) {
							int nIndex = pPanel->AddString (panel.m_panel.m_strName);
							pPanel->SetItemData (nIndex, panel.m_panel.m_nNumber);

							if (pPanel->GetCurSel () == CB_ERR) 
								pPanel->SetCurSel (nIndex);
						}
					}
				}
			}
		}

		CSaveDlg::OnItemchangedTasks (pNMHDR, pResult);
		OnSelchangePanel ();

		m_selLast = sel;
	}
}

void COpenDlg::OnPanelClicked() 
{
	UpdateData (TRUE);
}

void COpenDlg::OnSelchangeLine() 
{
	CSaveDlg::OnSelchangeLine();
	OnSelchangePanel ();
}