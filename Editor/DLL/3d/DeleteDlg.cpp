// DeleteDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "resource.h"
#include "DeleteDlg.h"
#include "ItiLibrary.h"
#include "Extern.h"
#include "Database.h"

using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace FoxjetCommon;
using namespace ListGlobals;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDeleteDlg dialog


CDeleteDlg::CDeleteDlg(CWnd* pParent /*=NULL*/)
:	m_lpfctIsTaskOpen (NULL),
	m_lpfctBeginProgressBar (NULL),
	m_lpfctStepProgressBar (NULL),
	m_lpfctEndProgressBar (NULL),
	m_hAccel (NULL),
	COpenDlg (IDD_DELETE_MATRIX /* IsMatrix () ? IDD_DELETE_MATRIX : IDD_DELETE */ , pParent)
{
	//{{AFX_DATA_INIT(CDeleteDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDeleteDlg::DoDataExchange(CDataExchange* pDX)
{
	COpenDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDeleteDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDeleteDlg, COpenDlg)
	//{{AFX_MSG_MAP(CDeleteDlg)
	ON_COMMAND(ID_EDIT_SELECTALL, OnEditSelectall)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (BTN_DELETE, OnDelete)
	ON_NOTIFY(NM_DBLCLK, LV_MESSAGES, OnDblclkMessages)
	ON_NOTIFY(NM_DBLCLK, LV_TASKS, OnDblclkTasks)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDeleteDlg message handlers

bool CDeleteDlg::DocsOpen (const FoxjetCommon::CLongArray & sel)
{
	CString str = LoadString (IDS_CANTDELETEOPENTASK);
	int nOpen = 0;

	for (int i = 0; i < sel.GetSize (); i++) {
		ULONG lID = _tcstoul (m_lviTask->GetItemText (sel [i], 2), NULL, 10);
		const TASKSTRUCT * pTask = GetTask (GetSelectedLineID (), lID);

		ASSERT (pTask);

		ASSERT (m_lpfctIsTaskOpen);

		if ((* m_lpfctIsTaskOpen) (pTask->m_lID)) {
			str += _T ("\n") + pTask->m_strName;
			nOpen++;
		}
	}

	if (nOpen) 
		MsgBox (* this, str, LoadString (IDS_CONFIRM), MB_OK | MB_ICONWARNING);

	return nOpen != 0;
}

void CDeleteDlg::OnDelete ()
{
	CLongArray sel = GetSelectedItems (m_lviTask);

	if (sel.GetSize ()) { 
		const CString strDeleting = LoadString (IDS_DELETING);

		if (MsgBox (* this, LoadString (IDS_CONFIRMDELETE), LoadString (IDS_CONFIRM), MB_YESNO) == IDNO)
			return;

		if (DocsOpen (sel))
			return;

		BeginWaitCursor ();
		ASSERT (m_lpfctBeginProgressBar);
		(* m_lpfctBeginProgressBar) (strDeleting + _T ("..."), sel.GetSize ());

		for (int i = sel.GetSize () - 1; i >= 0; i--) {
			ULONG lID = _tcstoul (m_lviTask->GetItemText (sel [i], 2), NULL, 10);
			const TASKSTRUCT * pTask = GetTask (GetSelectedLineID (), lID);

			ASSERT (pTask);

			REPORTSTRUCT rpt;
			DELETEDSTRUCT del;

			ASSERT (pTask);

			rpt.m_dtTime		= COleDateTime::GetCurrentTime ();
			rpt.m_strAction		= _T ("");
			rpt.m_strLine		= GetSelectedLine ();
			rpt.m_strTaskName	= pTask->m_strName;
			rpt.m_strUsername	= GetUsername ();

			ASSERT (m_lpfctStepProgressBar);
			(* m_lpfctStepProgressBar) (strDeleting + _T (": \"") + pTask->m_strName + _T ("\"..."));

			VERIFY (DeleteTaskRecord (GetDB (), pTask->m_lID)); 
			VERIFY (DeleteCachedTask (pTask->m_lID));

			VERIFY (AddReportRecord (GetDB (), REPORT_TASK_DESTROY, rpt, GetPrinterID (GetDB ())));

			del.m_lLineID		= pTask->m_lLineID;
			del.m_strTaskName	= pTask->m_strName;
			VERIFY (AddDeletedRecord (GetDB (), del));
			
			m_lviTask->DeleteItem (sel [i]);
		}

		ASSERT (m_lpfctEndProgressBar);
		(* m_lpfctEndProgressBar) ();
		EndWaitCursor ();
		GetDlgItem (IDOK)->EnableWindow (TRUE);
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

BOOL CDeleteDlg::OnInitDialog() 
{
	BEGIN_TRANS (GetDB ());
	COpenDlg::OnInitDialog(); 

	GetDlgItem (IDOK)->EnableWindow (FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDeleteDlg::OnOK ()
{
	COMMIT_TRANS (GetDB ());
	FoxjetCommon::CEliteDlg::OnOK ();
}

void CDeleteDlg::OnCancel ()
{
	ROLLBACK_TRANS (GetDB ());
	FoxjetCommon::CEliteDlg::OnCancel ();
}

BOOL CDeleteDlg::PreTranslateMessage(MSG* pMsg) 
{
	if (!m_hAccel)
		VERIFY (m_hAccel = ::LoadAccelerators (::AfxGetInstanceHandle (), MAKEINTRESOURCE (IDA_LISTCTRL)));

	if (::TranslateAccelerator (m_hWnd, m_hAccel, pMsg))
		return TRUE;
	
	return COpenDlg::PreTranslateMessage(pMsg);
}

void CDeleteDlg::OnEditSelectall() 
{
	m_lviTask.SelectAll ();
}

void CDeleteDlg::OnDblclkTasks(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if (pResult)
		* pResult = 0;
}

void CDeleteDlg::OnDblclkMessages(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if (pResult)
		* pResult = 0;
}
