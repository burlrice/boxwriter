// AmsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "AmsDlg.h"
#include "Resource.h"
#include "TemplExt.h"
#include "Database.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Foxjet3d;
using namespace FoxjetCommon;

/////////////////////////////////////////////////////////////////////////////
// CAmsDlg dialog

const double CAmsDlg::m_dAmsIntervalMin = 0.3;
const double CAmsDlg::m_dAmsIntervalMax = 99.0;

CAmsDlg::CAmsDlg (const FoxjetDatabase::LINESTRUCT & line, CWnd* pParent /*=NULL*/)
:	m_line (line),
	FoxjetCommon::CEliteDlg (IDD_AMS_DLG_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CAmsDlg)
	//}}AFX_DATA_INIT
}


void CAmsDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAmsDlg)
	DDX_Text(pDX, IDC_32_A1, m_line.m_nAMS32_A1);
	DDX_Text(pDX, IDC_32_A2, m_line.m_nAMS32_A2);
	DDX_Text(pDX, IDC_32_A3, m_line.m_nAMS32_A3);
	DDX_Text(pDX, IDC_32_A4, m_line.m_nAMS32_A4);
	DDX_Text(pDX, IDC_32_A5, m_line.m_nAMS32_A5);
	DDX_Text(pDX, IDC_32_A6, m_line.m_nAMS32_A6);
	DDX_Text(pDX, IDC_32_A7, m_line.m_nAMS32_A7);

	DDX_Text(pDX, IDC_256_A1, m_line.m_nAMS256_A1);
	DDX_Text(pDX, IDC_256_A2, m_line.m_nAMS256_A2);
	DDX_Text(pDX, IDC_256_A3, m_line.m_nAMS256_A3);
	DDX_Text(pDX, IDC_256_A4, m_line.m_nAMS256_A4);
	DDX_Text(pDX, IDC_256_A5, m_line.m_nAMS256_A5);
	DDX_Text(pDX, IDC_256_A6, m_line.m_nAMS256_A6);
	DDX_Text(pDX, IDC_256_A7, m_line.m_nAMS256_A7);

	DDX_Text(pDX, IDC_INTERVAL, m_line.m_dAMS_Interval);
	//}}AFX_DATA_MAP

	DDV_MinMaxDouble (pDX, m_line.m_dAMS_Interval, m_dAmsIntervalMin, m_dAmsIntervalMax);

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
}


BEGIN_MESSAGE_MAP(CAmsDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CAmsDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (CHK_DISABLE, OnDisable)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAmsDlg message handlers

BOOL CAmsDlg::OnInitDialog() 
{
	CButton * pDisable = (CButton *)GetDlgItem (CHK_DISABLE);

	ASSERT (pDisable);

	if (m_line.m_dAMS_Interval == 0) {
		m_line.m_dAMS_Interval = 2;//m_dAmsIntervalMin;
		pDisable->SetCheck (1);
	}

	FoxjetCommon::CEliteDlg::OnInitDialog();


	OnDisable ();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAmsDlg::OnOK() 
{
	CButton * pDisable = (CButton *)GetDlgItem (CHK_DISABLE);

	ASSERT (pDisable);

	FoxjetCommon::CEliteDlg::OnOK();

	if (pDisable->GetCheck () == 1)
		m_line.m_dAMS_Interval = 0; 
}

void CAmsDlg::OnDisable ()
{
	CButton * pDisable = (CButton *)GetDlgItem (CHK_DISABLE);
	UINT nID [] =
	{
		IDC_INTERVAL,
		IDC_32_A1,
		IDC_32_A2,
		IDC_32_A3,
		IDC_32_A4,
		IDC_32_A5,
		IDC_32_A6,
		IDC_32_A7,
		IDC_256_A1,
		IDC_256_A2,
		IDC_256_A3,
		IDC_256_A4,
		IDC_256_A5,
		IDC_256_A6,
		IDC_256_A7,
	};

	ASSERT (pDisable);

	for (int i = 0; i < ARRAYSIZE (nID); i++)
		if (CWnd * p = GetDlgItem (nID [i]))
			p->EnableWindow (pDisable->GetCheck () == 0);
}