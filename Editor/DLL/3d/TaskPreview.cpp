
#include "stdafx.h"
#include "3d.h"
#include "OpenDlg.h"
#include "Database.h"
#include "Extern.h"
#include "Registry.h"
#include "Color.h"
#include "Coord.h"
#include "Element.h"
#include "Extern.h"
#include "Task.h"
#include "Box.h"
#include "Utils.h"
#include "ximage.h"
#include "UserElement.h"
#include "BitmapElement.h"
#include "UserElementDlg.h"
#include "Parse.h"
#include "ItiLibrary.h"
#include "BarcodeParams.h"
#include "BarcodeElement.h"
#include "DC.h"
#include "ImportFileDlg.h"

#ifdef __WINPRINTER__
	#include "DatabaseElement.h"
	#include "BitmappedFont.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace ItiLibrary;
using namespace FoxjetDatabase;
using namespace Color;
using namespace Foxjet3d;
using namespace ListGlobals;
using namespace OpenDlg;
using namespace Panel;
using namespace Element;

////////////////////////////////////////////////////////////////////////////////
static CString GetMessageData (CArray <MESSAGESTRUCT, MESSAGESTRUCT &> & v, ULONG lHeadID)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i].m_lHeadID == lHeadID)
			return v [i].m_strData;

	return _T ("");
}
////////////////////////////////////////////////////////////////////////////////

static CEditorElementList * GetList	(ULONG lHeadID, CArray <CEditorElementList *, CEditorElementList *> & v)
{
	for (int i = 0; i < v.GetSize (); i++) 
		if (CEditorElementList * p = v [i]) 
			if (p->GetHead ().m_lID == lHeadID)
				return p;

	return NULL;
}

int CALLBACK InitializeLynxPreview ()
{
	using namespace FoxjetElements;

	FoxjetDatabase::COdbcDatabase dbTmp (_T (__FILE__), __LINE__);
	const FoxjetCommon::CVersion ver = ::verApp;

	for (int nSymbology = kSymbologyFirst; nSymbology <= kSymbologyLast; nSymbology++) {
		if (CBarcodeElement::IsValidSymbology (nSymbology)) {
			CBarcodeParams p = GetDefParam (nSymbology, 0, UJI_96_32, 150, dbTmp);
			int nID = nSymbology;
			CBarcodeElement::SetLocalParam (nID, nSymbology, p);
		}
	}

	FoxjetCommon::InitInstance (ver);

	return 1;
}

int CALLBACK ReleaseLynxPreview ()
{
	VERIFY (FoxjetCommon::ExitInstance (::verApp));
	return 1;
}

int CALLBACK DrawLynxPreview (const CString & strFile, const CMapStringToString & map, DIBSECTION * pds, BYTE ** pData)
{
	CTmpDC hdc (NULL);
	CDC dcMem, & dc = * CDC::FromHandle (hdc);
	ULONG lResult = 0;
	int nChannels = 32;

	TASKSTRUCT task;
	BOXSTRUCT box;
	LINESTRUCT line;
	HEADSTRUCT head;
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
	CArray <VALVESTRUCT, VALVESTRUCT &> vValves;
	CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;
	CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> vBounds;

	VERIFY (dcMem.CreateCompatibleDC (&dc));

	//CString strTask		=	_T ("{TASKSTRUCT,2,1,44,text,,,0,{MESSAGESTRUCT,3,38,2,text,,\\{Ver\\,4\\,13\\,925\\,0\\,58\\}\\{Timestamp\\,03/16/18 19:07:54\\}\\{ProdArea\\,12000\\,1750\\,0\\,38\\}\\{Text\\,1\\,0\\,0\\,0\\,0\\,0\\,Vx 18b\\,18\\,0\\,0\\,DID IT WORK?\\,0\\,0\\,0\\,0\\,0\\,0\\,0\\,0\\},0,1}}");
	CString strTask		=	_T ("{TASKSTRUCT,2,1,44,text,,,0,{MESSAGESTRUCT,3,38,2,text,,\\{Ver\\,4\\,13\\,925\\,0\\,58\\}\\{Timestamp\\,03/16/18 19:07:54\\}\\{ProdArea\\,12000\\,1750\\,0\\,38\\}}");
	
	CString strBox		=	_T ("{BOXSTRUCT,44,lynx,,12000,12000,4000}");
	CString strLine		=	_T ("{LINESTRUCT,1,LINE0001,Test line 1,12:00,NO READ,1,3,0,5,10.1.2.50,0,9600\\,8\\,N\\,1\\,0,9600\\,8\\,N\\,1\\,0,9600\\,8\\,N\\,1\\,0,3,1000,10,1000,2500,3,20,3,1000,28,1000,5500,2,100,24.0,1}");
	CString strHead		=	_T ("{HEADSTRUCT,38,1,CARD 1,300,0,18,300,1000,0,1,1,0,90.0,0,1,1,7,0,1,60,1.813,36.0,9600\\,8\\,N\\,1\\,0,4,0}");
	CString strPanel [] = 
	{
							_T ("{PANELSTRUCT,1,1,Front,1,0,0}"),
							_T ("{PANELSTRUCT,2,1,Right,2,1,0}"),
							_T ("{PANELSTRUCT,3,1,Back,3,1,0}"),
							_T ("{PANELSTRUCT,4,1,Left,4,0,0}"),
							_T ("{PANELSTRUCT,5,1,Top,5,1,0}"),
							_T ("{PANELSTRUCT,6,1,Bottom,6,0,0}"),
	};

	FromString (strTask, task);
	FromString (strBox, box);
	FromString (strLine, line);
	FromString (strHead, head);

	box.m_lLength = box.m_lWidth = 48000;

	{
		CString str [] = 
		{
			_T ("{VALVESTRUCT,1,38,2000,1,0,1,4000,1,FJ0001,1,4294967295}"),
			//_T ("{VALVESTRUCT,2,38,2000,1,0,1,3125,1,FJ0002,2,1}"),
			//_T ("{VALVESTRUCT,3,38,2000,1,0,1,3125,1,FJ0003,2,2}"),
			//_T ("{VALVESTRUCT,4,38,2000,1,0,1,3125,1,FJ0004,2,3}"),
		};
		CStringArray v;
		
		head.m_nEncoderDivisor = 3;

		for (int i = 0; i < ARRAYSIZE (str); i++) {
			VALVESTRUCT valve;

			//TRACEF (str [i]);

			VERIFY (FromString (str [i], valve));
			valve.m_type = IV_LYNX_32;
			valve.m_lCardID = head.m_lID;
			head.m_vValve.Add (valve);
		}
	}

	vHeads.Add (head);

	//TRACEF (strTask);
	//TRACEF (strBox);
	//TRACEF (strLine);
	//TRACEF (strHead);

	for (int i = 0; i < ARRAYSIZE (strPanel); i++) {
		PANELSTRUCT p;

		TRACEF (strPanel [i]);
		FromString (strPanel [i], p);
		vPanels.Add (p);
	}

	for (int i = 0; i < vHeads.GetSize (); i++) 
		vValves.Append (vHeads [i].m_vValve);

	HeadConfigDlg::CPanel::GetPanels (task.m_lLineID, vBounds, vHeads, vValves, vPanels);

	/*
		{TASKSTRUCT,2,1,44,text,,,0,{MESSAGESTRUCT,3,38,2,text,,\{Ver\,4\,13\,925\,0\,58\}\{Timestamp\,03/16/18 19:07:54\}\{ProdArea\,12000\,1750\,0\,38\}\{Text\,1\,0\,0\,0\,0\,0\,Vx 18b\,18\,0\,0\,9X130001\,0\,0\,0\,0\,0\,0\,0\,0\},0,1}}
		{BOXSTRUCT,44,lynx,,12000,12000,5000}
		{LINESTRUCT,1,LINE0001,Test line 1,12:00,NO READ,1,3,0,5,10.1.2.50,0,9600\,8\,N\,1\,0,9600\,8\,N\,1\,0,9600\,8\,N\,1\,0,3,1000,10,1000,2500,3,20,3,1000,28,1000,5500,2,100,24.0,1}
		{HEADSTRUCT,38,1,CARD 1,300,0,18,300,1000,0,1,1,0,90.0,0,1,1,7,0,1,60,1.813,36.0,9600\,8\,N\,1\,0,4,0}
		{PANELSTRUCT,1,1,Front,1,0,0}
		{PANELSTRUCT,2,1,Right,2,1,0}
		{PANELSTRUCT,3,1,Back,3,1,0}
		{PANELSTRUCT,4,1,Left,4,0,0}
		{PANELSTRUCT,5,1,Top,5,1,0}
		{PANELSTRUCT,6,1,Bottom,6,0,0}
		{SETTINGSSTRUCT,0,Valve head:38,\{VALVESTRUCT\,1\,38\,2000\,1\,0\,1\,4000\,1\,FJ0001\,1\,4294967295\}\{VALVESTRUCT\,2\,38\,2000\,1\,0\,1\,3125\,1\,FJ0002\,2\,1\}}
	*/

	/*
	TRACEF (ToString (task));
	TRACEF (ToString (box));
	TRACEF (ToString (line));

	for (int i = 0; i < vHeads.GetSize (); i++)
		TRACEF (ToString (vHeads [i]));

	for (int i = 0; i < vPanels.GetSize (); i++)
		TRACEF (ToString (vPanels [i]));

	TRACEF (ToString ((int)head.m_lPanelID));

	for (int nPanel = 0; nPanel < vBounds.GetSize (); nPanel++) {
		const HeadConfigDlg::CPanel & panel = vBounds [nPanel];
		int nCards = panel.m_vCards.GetSize ();
		int nValves = panel.m_vValve.GetSize ();

		TRACEF (panel.m_panel.m_strName);
		TRACEF (ToString ((int)nCards));
		TRACEF (ToString ((int)nValves));
	}
	*/

	TRACEF (_T ("channels: ") + ToString (head.GetChannels ()));
	TRACEF (_T ("span:     ") + ToString (head.GetSpan ()) + _T ("\""));
	TRACEF (_T ("v res:    ") + ToString ((double)head.GetChannels () / head.GetSpan ()));
	TRACEF (_T ("h res:    ") + ToString (head.GetRes ()));

	if (task.m_vMsgs.GetSize ()) {
		task.m_vMsgs [0].m_strData += Foxjet3d::CImportFileDlg::ImportYml (strFile, head, map, nChannels);

		#if 0
		{
			LPCSTR lpszFont [] = 
			{
				"Vx 5s",
				"Vx 7b",
				"Vx 9b",
				"Vx 18b",
			};
			int nFont = 0;

			for (int i = 0; i < 5; i++) {
				FoxjetElements::CTextElement t (head);

				t.SetPos (CPoint (i * 1000, 0), &head);
				t.SetFont (FoxjetCommon::CPrinterFont (lpszFont [nFont++ % ARRAYSIZE (lpszFont)]));
				t.SetDefaultData (ToString (i));

				task.m_vMsgs [0].m_strData += t.ToString (::verApp);
			}
		}
		#endif
	}

	Foxjet3d::OpenDlg::CTaskPreview preview (task, dcMem, CRect (0, 0, 1, 1), CMapStringToString (), box, line, vHeads, vPanels, vBounds);

	for (int i = 0; i < ARRAYSIZE (preview.m_bmp); i++) {
		if (HBITMAP hbmp = preview.m_bmp [i].GetHBITMAP ()) {
			CRawBitmap & raw = preview.m_bmp [i];
			CxImage crop;
			CSize dot = CBaseElement::GetValveDotSize (head);

			//dot.cy *= 2; // TODO

			::GetObject (hbmp, sizeof (DIBSECTION), pds);
			crop.CreateFromArray (raw.GetBuffer (), pds->dsBm.bmWidth, pds->dsBm.bmHeight, pds->dsBm.bmBitsPixel, pds->dsBm.bmWidthBytes, true);
			CRect rcCrop (0, 0, pds->dsBm.bmWidth, pds->dsBm.bmHeight);

			rcCrop.bottom = rcCrop.top + (int)((double)dot.cy * (double)nChannels / 1.5);
			crop.Crop (rcCrop);

			#if 0 // #ifdef _DEBUG
			raw.Save (GetHomeDir () + _T ("\\Debug\\raw") + ToString (i) + _T (".bmp"));
			crop.Save (GetHomeDir () + _T ("\\Debug\\crop") + ToString (i) + _T (".bmp"), CXIMAGE_FORMAT_BMP);
			#endif

			if (HBITMAP h = crop.MakeBitmap ()) {
				CRawBitmap result (h);

				::GetObject (h, sizeof (DIBSECTION), pds);
				lResult = result.GetLength ();
				*pData = new BYTE [lResult];
				memcpy (*pData, result.GetBuffer (), lResult);
				::DeleteObject (h);
			}

			break;
		}
	}

	return lResult;
}


CTaskPreview::CTaskPreview (const FoxjetDatabase::TASKSTRUCT & task, CDC & dc, const CRect & rcPanel, const CMapStringToString & mapUser)
:	m_task (task)
{
	COdbcDatabase & db = ListGlobals::GetDB ();

	FoxjetDatabase::BOXSTRUCT box; 
	FoxjetDatabase::LINESTRUCT line; 
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
	CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;
	CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> vBounds;

	#ifdef __WINPRINTER__
	using namespace FoxjetElements;
	#endif

	using namespace Foxjet3d::Box;

	const CVersion ver = GetElementListVersion ();

	BOXSTRUCT b;

	VERIFY (GetBoxRecord (db, task.m_lBoxID, b));
	VERIFY (GetLineRecord (db, task.m_lLineID, line));
	VERIFY (GetLineHeads (db, task.m_lLineID, vHeads));
	VERIFY (GetPanelRecords (db, task.m_lLineID, vPanels));

	HeadConfigDlg::CPanel::GetPanels (db, task.m_lLineID, vBounds);

	Draw (task, dc, rcPanel, mapUser, b, line, vHeads, vPanels, vBounds, &db);
}

void CTaskPreview::Draw (const FoxjetDatabase::TASKSTRUCT & task, CDC & dc, const CRect & rcPanel, const CMapStringToString & mapUser,
	FoxjetDatabase::BOXSTRUCT & b, 
	const FoxjetDatabase::LINESTRUCT & line, 
	const CArray <HEADSTRUCT, HEADSTRUCT &> & vHeads,
	const CArray <PANELSTRUCT, PANELSTRUCT &> & vPanels,
	const CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> & vBounds,
	FoxjetDatabase::COdbcDatabase * pdb)
{
	#ifdef __WINPRINTER__
	using namespace FoxjetElements;
	#endif

	CDC dcPanel;
	CTask t (task);
	const FoxjetDatabase::COrientation orient (HIWORD (task.m_lTransformation) + 1, (ORIENTATION)LOWORD (task.m_lTransformation));
	CArray <CEditorElementList *, CEditorElementList *> vLists;
	const CVersion ver = GetElementListVersion ();
	FoxjetUtils::CDatabaseStartDlg dlgDB (NULL);
	int nResult = 0;

	//VERIFY (dcPanel.CreateCompatibleDC (&dc));
	VERIFY (dcPanel.CreateCompatibleDC (CTmpDC (NULL)));

	if (pdb) 
		dlgDB.Load (* pdb);
	else
		b.m_lLength = 6000;

	CArray <FoxjetCommon::CBaseElement *, FoxjetCommon::CBaseElement *> list;
	FoxjetDatabase::USERSTRUCT user;

	for (int nPanel = 0; nPanel < vPanels.GetSize (); nPanel++) {
		const PANELSTRUCT & panel = vPanels [nPanel];
		FoxjetDatabase::COrientation face = Foxjet3d::Box::CBox::MapPanel (panel.m_nNumber, orient);
		const CSize size = GetSize (vHeads, vPanels, b, face);
		const CRect rcPanel (CPoint (0, 0), size);

		for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
			const HEADSTRUCT & head = vHeads [nHead];

			if (head.m_lPanelID == panel.m_lID) {
				CEditorElementList * pList = new CEditorElementList ();
				CString str = GetMessageData (t.m_vMsgs, head.m_lID);
				CString strPrompt, strKeyValue;

				pList->FromString (str, CProductArea (), INCHES, head, CVersion ());
				pList->SetHead (head.m_lID);

				for (int nElement = 0; nElement < pList->GetSize (); nElement++) {
					#ifdef __WINPRINTER__
					CElement & e = pList->GetElement (nElement);

					//TRACEF (FoxjetDatabase::ToString (nElement) + _T (": ") + pList->GetElement (nElement).GetElement ()->ToString (::verApp));

					if (CUserElement * p = DYNAMIC_DOWNCAST (CUserElement, e.GetElement ())) 
						strPrompt = p->GetPrompt ();

					if (pdb) {
						for (POSITION pos = mapUser.GetStartPosition (); pos; ) {
							CString strPrompt, strData;

							mapUser.GetNextAssoc (pos, strPrompt, strData);
							Foxjet3d::UserElementDlg::CUserElementDlg::Set (e.GetElement (), head, strPrompt, strData, strKeyValue, user, dlgDB);
						}
					}

					#endif
				}

				vLists.Add (pList);
			}
		}
	}

	for (int nPanel = 0; nPanel < vPanels.GetSize (); nPanel++) {
		const PANELSTRUCT & panel = vPanels [nPanel];
		FoxjetDatabase::COrientation face = Box::CBox::MapPanel (panel.m_nNumber, orient);
		const CSize sizePanel = GetSize (vHeads, vPanels, b, face);
		const CRect rcPanel (CPoint (0, 0), sizePanel);

		for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
			const HEADSTRUCT & head = vHeads [nHead];
			const CSize dot = CBaseElement::GetValveDotSize (head);
			double dStretch [2] = { 0, 0 };

			CBaseElement::CalcStretch (head, dStretch);

			if (head.m_lPanelID == panel.m_lID) {
				//FoxjetDatabase::COrientation face = CBox::MapPanel (panel.m_nNumber, orient);
				CSize sizeHead = GetSize (vHeads, vPanels, b, face);
				sizeHead.cy = head.Height ();
				const CRect rcPanel (CPoint (0, 0), sizeHead);
				
				if (CEditorElementList * pList = GetList (head.m_lID, vLists)) {
					CBitmap bmp;
					CString str = GetMessageData (t.m_vMsgs, head.m_lID);

					if (!pdb) {
						for (int nElement = 0; nElement < pList->GetSize (); nElement++) {
							CElement & src = pList->GetElement (nElement);
							CElement e (src);
							CDC dcElement;

							e.GetElement ()->Build ();
							e.GetElement ()->FromString (src.GetElement ()->ToString (ver), ver); // to force x pos back to original state if centered, or right justified
							CSize sizeElement = e.GetElement ()->Draw (dcPanel, head, true, FoxjetCommon::EDITOR);
							CPoint pt = e.GetPos (head);

							sizeElement = CBaseElement::ThousandthsToLogical (sizeElement, head);
							//sizeElement.cx = (int)((double)sizeElement.cx * dStretch [0]);
							sizeElement.cy = (int)((double)sizeElement.cy * dStretch [1]);

							if (IsValveHead (head))
								pt.y += (dot.cy / 2);

							pt = CBaseElement::LogicalToThousandths (pt, head);
							sizeElement = CBaseElement::LogicalToThousandths (sizeElement, head);
							TRACEF (e.GetElement ()->GetImageData ());
							ULONG cx = (ULONG)ceil ((pt.x + (sizeElement.cx / dStretch [0]) + 500) / 1000.0) * 1000;
							b.m_lLength = max (b.m_lLength, cx);
						}

						sizeHead = GetSize (vHeads, vPanels, b, face);
					}

					VERIFY (bmp.CreateCompatibleBitmap (&dc, sizeHead.cx, sizeHead.cy));
					
					CBitmap * pPanelBmp = dcPanel.SelectObject (&bmp);
					::BitBlt (dcPanel, 0, 0, sizeHead.cx, sizeHead.cy, NULL, 0, 0, WHITENESS);

					for (int nElement = 0; nElement < pList->GetSize (); nElement++) {
						CElement & src = pList->GetElement (nElement);
						CElement e (src);
						CDC dcElement;
						CBitmap bmpElement;

						e.GetElement ()->Build ();
						e.GetElement ()->FromString (src.GetElement ()->ToString (ver), ver); // to force x pos back to original state if centered, or right justified
						CSize sizeElement = e.GetElement ()->Draw (dcPanel, head, true, FoxjetCommon::EDITOR);

						sizeElement = CBaseElement::ThousandthsToLogical (sizeElement, head);
						//sizeElement.cx = (int)((double)sizeElement.cx * dStretch [0]);
						sizeElement.cy = (int)((double)sizeElement.cy * dStretch [1]);

						dcElement.CreateCompatibleDC (CTmpDC (NULL)); // dcElement.CreateCompatibleDC (&dc);
						bmpElement.CreateCompatibleBitmap (&dc, sizeElement.cx, sizeElement.cy);
						CBitmap * pElement = dcElement.SelectObject (&bmpElement);

						CRect rc = e.GetWindowRect (head, head);
						CPoint pt = e.GetPos (head);

						//TRACEF (src.GetElement ()->ToString (ver));
						//{ CString str; str.Format (_T ("%d, %d (%d, %d): %s"), pt.x, pt.y, sizeElement.cx, sizeElement.cy, e.GetElement ()->ToString (ver)); TRACEF (str); }

						if (IsValveHead (head))
							pt.y += (dot.cy / 2);

						e.Draw (dcElement, head, pList, dcPanel, pt.x, pt.y, rc.Width (), rc.Height (), rc.Width (), rc.Height (), 1.0);

						pt = CBaseElement::LogicalToThousandths (pt, head);
						sizeElement = CBaseElement::LogicalToThousandths (sizeElement, head);
						ULONG cx = (ULONG)ceil ((pt.x + sizeElement.cx + 500) / 1000.0) * 1000;
						b.m_lLength = max (b.m_lLength, cx);

						dcElement.SelectObject (pElement);
					}

					//{ CString str; str.Format ("c:\\Temp\\Debug\\%s.bmp", head.m_strName); SAVEBITMAP (dcPanel, str); }

					dcPanel.SelectObject (pPanelBmp);

					CRawBitmap * pbmp = new CRawBitmap (bmp);
					ASSERT (!m_map.Lookup (head.m_lID, pbmp));
					m_map.SetAt (head.m_lID, pbmp);
				}
			}
		}
	}

	for (int nPanel = 0; nPanel < vBounds.GetSize (); nPanel++) {
		const HeadConfigDlg::CPanel & panel = vBounds [nPanel];
		FoxjetDatabase::COrientation face = Box::CBox::MapPanel (panel.m_panel.m_nNumber, orient);
		const CSize size = GetSize (vHeads, vPanels, b, face);
		const CRect rcPanel (CPoint (0, 0), size);


		{
			int nHeads = 0;

			for (int nCard = 0; nCard < panel.m_vCards.GetSize (); nCard++) {
				const HEADSTRUCT & card = panel.m_vCards [nCard];

				if (!IsValveHead (card)) {
					nHeads++;
					break;
				}

				nHeads += card.m_vValve.GetSize ();
			}

			nHeads += panel.m_vValve.GetSize ();

			if (!nHeads) {
				TRACEF ("no heads, skipping: " + panel.m_panel.m_strName);
				continue;
			}
		}

		CBitmap bmp;

		VERIFY (bmp.CreateCompatibleBitmap (&dc, size.cx, size.cy));
		CBitmap * pPanelBmp = dcPanel.SelectObject (&bmp);
		::BitBlt (dcPanel, 0, 0, size.cx, size.cy, NULL, 0, 0, WHITENESS);

		for (int nCard = 0; nCard < panel.m_vCards.GetSize (); nCard++) {
			const HEADSTRUCT & card = panel.m_vCards [nCard];

			if (!IsValveHead (card)) 
				Blt (dcPanel, card);

			for (int nHead = 0; nHead < card.m_vValve.GetSize (); nHead++) 
				Blt (dcPanel, vHeads, card.m_vValve [nHead]);
		}

		for (int nHead = 0; nHead < panel.m_vValve.GetSize (); nHead++) 
			Blt (dcPanel, vHeads, panel.m_vValve [nHead]);

		//SAVEBITMAP (dcPanel, "C:\\Temp\\Debug\\" + panel.m_panel.m_strName + ".bmp");
		dcPanel.SelectObject (pPanelBmp);

		m_bmp [nPanel].Load (bmp);
	}

	{
		for (int i = 0; i < vLists.GetSize (); i++) 
			if (CEditorElementList * p = vLists [i])
				delete p;

		vLists.RemoveAll ();
	}
}

CTaskPreview::CTaskPreview (const FoxjetDatabase::TASKSTRUCT & task, CDC & dc, const CRect & rcPanel, const CMapStringToString & mapUser,
	FoxjetDatabase::BOXSTRUCT & b, 
	const FoxjetDatabase::LINESTRUCT & line, 
	const CArray <HEADSTRUCT, HEADSTRUCT &> & vHeads,
	const CArray <PANELSTRUCT, PANELSTRUCT &> & vPanels,
	const CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> & vBounds,
	FoxjetDatabase::COdbcDatabase * pdb)
:	m_task (task)
{
	Draw (task, dc, rcPanel, mapUser, b, line, vHeads, vPanels, vBounds, pdb);
}

/* TODO: rem
CTaskPreview::CTaskPreview (const FoxjetDatabase::TASKSTRUCT & task, CDC & dc, const CRect & rcPanel, const CMapStringToString & mapUser)
:	m_task (task)
{
	#ifdef __WINPRINTER__
	using namespace FoxjetElements;
	#endif

	using namespace Foxjet3d::Box;

	const CVersion ver = GetElementListVersion ();
	CDC dcPanel;
	CTask t (task);
	BOXSTRUCT b;
	Foxjet3d::Box::CBox box (ListGlobals::GetDB ());
	const FoxjetDatabase::COrientation orient (HIWORD (task.m_lTransformation) + 1, (ORIENTATION)LOWORD (task.m_lTransformation));
	CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> vBounds;
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
	CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;
	CBoundary workingHead;
	CArray <CEditorElementList *, CEditorElementList *> vLists;

	VERIFY (dcPanel.CreateCompatibleDC (&dc));
	VERIFY (GetBoxRecord (ListGlobals::GetDB (), task.m_lBoxID, b));
	VERIFY (GetLineHeads (ListGlobals::GetDB (), task.m_lLineID, vHeads));
	VERIFY (GetPanelRecords (ListGlobals::GetDB (), task.m_lLineID, vPanels));

	HeadConfigDlg::CPanel::GetPanels (ListGlobals::GetDB (), task.m_lLineID, vBounds);

	box.SetBox (b);
	box.SetTransform (orient);
	box.GetWorkingHead (workingHead);

	{
		CArray <FoxjetCommon::CBaseElement *, FoxjetCommon::CBaseElement *> list;
		FoxjetDatabase::USERSTRUCT user;
		FoxjetUtils::CDatabaseStartDlg dlgDB (NULL);

		dlgDB.Load (ListGlobals::GetDB ());

		for (int nPanel = 0; nPanel < vPanels.GetSize (); nPanel++) {
			PANELSTRUCT & panel = vPanels [nPanel];
			FoxjetDatabase::COrientation face = Foxjet3d::Box::CBox::MapPanel (panel.m_nNumber, orient);
			const CSize size = GetSize (vHeads, box, face);
			const CRect rcPanel (CPoint (0, 0), size);

			for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
				HEADSTRUCT & head = vHeads [nHead];

				if (head.m_lPanelID == panel.m_lID) {
					CEditorElementList * pList = new CEditorElementList ();
					CString str = GetMessageData (t.m_vMsgs, head.m_lID);
					CString strPrompt, strKeyValue;

					pList->FromString (str, CProductArea (), INCHES, head, CVersion ());
					pList->SetHead (head.m_lID);

					for (int nElement = 0; nElement < pList->GetSize (); nElement++) {
						#ifdef __WINPRINTER__
						CElement & e = pList->GetElement (nElement);

						//TRACEF (FoxjetDatabase::ToString (nElement) + _T (": ") + pList->GetElement (nElement).GetElement ()->ToString (::verApp));

						if (CUserElement * p = DYNAMIC_DOWNCAST (CUserElement, e.GetElement ())) 
							strPrompt = p->GetPrompt ();

						for (POSITION pos = mapUser.GetStartPosition (); pos; ) {
							CString strPrompt, strData;

							mapUser.GetNextAssoc (pos, strPrompt, strData);
							Foxjet3d::UserElementDlg::CUserElementDlg::Set (e.GetElement (), head, strPrompt, strData, strKeyValue, user, dlgDB);
						}
						#endif

						/* TODO: rem
						CElement & e = pList->GetElement (nElement);
						CString strData;

						#ifdef __WINPRINTER__
						if (CUserElement * p = DYNAMIC_DOWNCAST (CUserElement, e.GetElement ())) {
							strPrompt = p->GetPrompt ();

							if (p->m_bPromptAtTaskStart) 
								if (mapUser.Lookup (p->GetPrompt (), strData))
									p->SetDefaultData (strData);
						}
						else if (CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, e.GetElement ())) {
							strPrompt = p->GetPrompt ();
							strKeyValue = p->GetKeyValue ();

							if (p->GetPromptAtTaskStart ()) {
								if (mapUser.Lookup (p->GetPrompt (), strData))
									p->SetKeyValue (strData);
							}
						}
						else if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, e.GetElement ())) {
							strPrompt = p->GetFilePath ();

							if (FoxjetCommon::IsProductionConfig ()) { //#if __CUSTOM__ == __SW0833__ 
								if (mapUser.Lookup (p->GetFilePath (), strData)) {
									p->SetFilePath (strData, head);
									UserElementDlg::CUserElementDlg::ResetSize (p, head);
								}
							}
						}
						#endif //__WINPRINTER__
						* /
					}

					vLists.Add (pList);
				}
			}
		}
	}

	for (int nPanel = 0; nPanel < vPanels.GetSize (); nPanel++) {
		PANELSTRUCT & panel = vPanels [nPanel];
		FoxjetDatabase::COrientation face = Box::CBox::MapPanel (panel.m_nNumber, orient);
		const CSize sizePanel = GetSize (vHeads, box, face);
		const CRect rcPanel (CPoint (0, 0), sizePanel);

		for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
			HEADSTRUCT & head = vHeads [nHead];
			const CSize dot = CBaseElement::GetValveDotSize (head);
			double dStretch [2] = { 0, 0 };

			CBaseElement::CalcStretch (head, dStretch);

			if (head.m_lPanelID == panel.m_lID) {
				//FoxjetDatabase::COrientation face = CBox::MapPanel (panel.m_nNumber, orient);
				CSize sizeHead = GetSize (vHeads, box, face);
				sizeHead.cy = head.Height ();
				const CRect rcPanel (CPoint (0, 0), sizeHead);
				
				if (CEditorElementList * pList = GetList (head.m_lID, vLists)) {
					CBitmap bmp;
					CString str = GetMessageData (t.m_vMsgs, head.m_lID);

					VERIFY (bmp.CreateCompatibleBitmap (&dc, sizeHead.cx, sizeHead.cy));
					
					CBitmap * pPanelBmp = dcPanel.SelectObject (&bmp);
					::BitBlt (dcPanel, 0, 0, sizeHead.cx, sizeHead.cy, NULL, 0, 0, WHITENESS);

					for (int nElement = 0; nElement < pList->GetSize (); nElement++) {
						CElement & src = pList->GetElement (nElement);
						CElement e (src);
						CDC dcElement;
						CBitmap bmpElement;

						e.GetElement ()->Build ();
						e.GetElement ()->FromString (src.GetElement ()->ToString (ver), ver); // to force x pos back to original state if centered, or right justified
						CSize sizeElement = e.GetElement ()->Draw (dcPanel, head, true, FoxjetCommon::EDITOR);

						sizeElement = CBaseElement::ThousandthsToLogical (sizeElement, head);
						//sizeElement.cx = (int)((double)sizeElement.cx * dStretch [0]);
						sizeElement.cy = (int)((double)sizeElement.cy * dStretch [1]);

						dcElement.CreateCompatibleDC (&dc);
						bmpElement.CreateCompatibleBitmap (&dc, sizeElement.cx, sizeElement.cy);
						CBitmap * pElement = dcElement.SelectObject (&bmpElement);

						CRect rc = e.GetWindowRect (head, head);
						CPoint pt = e.GetPos (head);

						//TRACEF (src.GetElement ()->ToString (ver));
						//{ CString str; str.Format (_T ("%d, %d (%d, %d): %s"), pt.x, pt.y, sizeElement.cx, sizeElement.cy, e.GetElement ()->ToString (ver)); TRACEF (str); }

						if (IsValveHead (head))
							pt.y += (dot.cy / 2);

						e.Draw (dcElement, head, pList, dcPanel, pt.x, pt.y, rc.Width (), rc.Height (), rc.Width (), rc.Height (), 1.0);

						dcElement.SelectObject (pElement);
					}

					//{ CString str; str.Format ("c:\\Temp\\Debug\\%s.bmp", head.m_strName); SAVEBITMAP (dcPanel, str); }

					dcPanel.SelectObject (pPanelBmp);

					CRawBitmap * pbmp = new CRawBitmap (bmp);
					ASSERT (!m_map.Lookup (head.m_lID, pbmp));
					m_map.SetAt (head.m_lID, pbmp);
				}
			}
		}
	}

	for (int nPanel = 0; nPanel < vBounds.GetSize (); nPanel++) {
		HeadConfigDlg::CPanel & panel = vBounds [nPanel];
		FoxjetDatabase::COrientation face = Box::CBox::MapPanel (panel.m_panel.m_nNumber, orient);
		const CSize size = GetSize (vHeads, box, face);
		const CRect rcPanel (CPoint (0, 0), size);


		{
			int nHeads = 0;

			for (int nCard = 0; nCard < panel.m_vCards.GetSize (); nCard++) {
				HEADSTRUCT & card = panel.m_vCards [nCard];

				if (!IsValveHead (card)) {
					nHeads++;
					break;
				}

				nHeads += card.m_vValve.GetSize ();
			}

			nHeads += panel.m_vValve.GetSize ();

			if (!nHeads) {
				TRACEF ("no heads, skipping: " + panel.m_panel.m_strName);
				continue;
			}
		}

		CBitmap bmp;

		VERIFY (bmp.CreateCompatibleBitmap (&dc, size.cx, size.cy));
		CBitmap * pPanelBmp = dcPanel.SelectObject (&bmp);
		::BitBlt (dcPanel, 0, 0, size.cx, size.cy, NULL, 0, 0, WHITENESS);

		for (int nCard = 0; nCard < panel.m_vCards.GetSize (); nCard++) {
			HEADSTRUCT & card = panel.m_vCards [nCard];

			if (!IsValveHead (card)) 
				Blt (dcPanel, card);

			for (int nHead = 0; nHead < card.m_vValve.GetSize (); nHead++) 
				Blt (dcPanel, vHeads, card.m_vValve [nHead]);
		}

		for (int nHead = 0; nHead < panel.m_vValve.GetSize (); nHead++) 
			Blt (dcPanel, vHeads, panel.m_vValve [nHead]);

		//SAVEBITMAP (dcPanel, "C:\\Temp\\Debug\\" + panel.m_panel.m_strName + ".bmp");
		dcPanel.SelectObject (pPanelBmp);

		m_bmp [nPanel].Load (bmp);
	}

	{
		for (int i = 0; i < vLists.GetSize (); i++) 
			if (CEditorElementList * p = vLists [i])
				delete p;

		vLists.RemoveAll ();
	}
}
*/

CTaskPreview::~CTaskPreview ()
{
	for (POSITION pos = m_map.GetStartPosition (); pos != NULL; ) {
		CRawBitmap * pBmp = NULL;
		ULONG lHeadID = 0;

		m_map.GetNextAssoc (pos, lHeadID, pBmp);

		if (pBmp)
			delete pBmp;
	}

	m_map.RemoveAll ();
}

CSize CTaskPreview::GetSize (const CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vHeads, 
	const CArray <PANELSTRUCT, PANELSTRUCT &> & vPanels,
	const FoxjetDatabase::BOXSTRUCT & box, const FoxjetDatabase::COrientation & orient)
{
	for (int i = 0; i < vHeads.GetSize (); i++) {
		const HEADSTRUCT & head = vHeads [i];

		CSize size1000s = m_task.GetPanelSize (vPanels, head.m_lPanelID, box);
		CSize sizeLogical = CBaseElement::ThousandthsToLogical (size1000s, head);
	
		return sizeLogical;
	}

	return CSize (0, 0);
}

void CTaskPreview::Blt (CDC & dcPanel, 
						const CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vHeads, 
						const FoxjetDatabase::VALVESTRUCT & valve)
{
	int nHeadIndex = CTask::Find (vHeads, valve.m_lCardID);
	CRawBitmap * pbmp = NULL;
	CDC dcPrinter;
	int nXSrc = 0, nYSrc = 0;
	int nXDest = 0, nYDest = 0;
	int nWidth = 0, nHeight = 0;

	ASSERT (nHeadIndex != -1);

	const HEADSTRUCT & head = vHeads [nHeadIndex];

	if (CBitmap * p = dcPanel.GetCurrentBitmap ()) {
		BITMAP bm;

		ZeroMemory (&bm, sizeof (bm));
		p->GetObject (sizeof (bm), &bm);

		nWidth	= bm.bmWidth;
		nHeight = bm.bmHeight;
	}

	nYDest = nHeight - CBaseElement::ThousandthsToLogical (CPoint (0, valve.m_lHeight), head).y;

	dcPrinter.CreateCompatibleDC (&dcPanel);
	VERIFY (m_map.Lookup (head.m_lID, pbmp));
	ASSERT (pbmp);

	if (pbmp) {
		CBitmap bmp;

		if (pbmp->GetBitmap (bmp)) {
			for (int i = 0; i < head.m_vValve.GetSize (); i++) {
				const VALVESTRUCT & tmp = head.m_vValve [i];

				nHeight = CBaseElement::ThousandthsToLogical (CPoint (0, GetValveHeight (tmp.m_type)), head).y;

				if (tmp.m_lID == valve.m_lID)
					break;

				nYSrc += nHeight;
			}

			CBitmap * pPrinter = dcPrinter.SelectObject (&bmp);

			dcPanel.BitBlt (nXDest, nYDest, nWidth, nHeight, &dcPrinter, nXSrc, nYSrc, SRCAND);
			//SAVEBITMAP (dcPanel, "C:\\Temp\\Debug\\" + valve.m_strName + ".bmp");

			dcPrinter.SelectObject (pPrinter);
		}
	}
}

CSize CTaskPreview::Blt (CDC & dcPanel, const FoxjetDatabase::HEADSTRUCT & head)
{
	CRawBitmap * pbmp = NULL;
	CDC dcPrinter;
	int nXSrc = 0, nYSrc = 0;
	int nXDest = 0, nYDest = 0;
	int nWidth = 0, nHeight = 0;

	if (CBitmap * p = dcPanel.GetCurrentBitmap ()) {
		BITMAP bm;

		ZeroMemory (&bm, sizeof (bm));
		p->GetObject (sizeof (bm), &bm);

		nWidth	= bm.bmWidth;
		nHeight = bm.bmHeight;
	}

	nYDest = nHeight - CBaseElement::ThousandthsToLogical (CPoint (0, head.m_lRelativeHeight), head).y;
	nYDest -= head.Height ();

	dcPrinter.CreateCompatibleDC (&dcPanel);
	VERIFY (m_map.Lookup (head.m_lID, pbmp));
	ASSERT (pbmp);

	if (pbmp) {
		CBitmap bmp;

		if (pbmp->GetBitmap (bmp)) {
			CBitmap * pPrinter = dcPrinter.SelectObject (&bmp);

			dcPanel.BitBlt (nXDest, nYDest, nWidth, nHeight, &dcPrinter, nXSrc, nYSrc, SRCAND);
			//SAVEBITMAP (dcPanel, "C:\\Temp\\Debug\\" + valve.m_strName + ".bmp");

			dcPrinter.SelectObject (pPrinter);

			return CSize (nWidth, nHeight);
		}
	}

	return CSize (0, 0);
}

