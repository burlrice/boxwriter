#if !defined(AFX_HEADDLG_H__0872C69B_1D21_43D7_B663_1B55BCE911CC__INCLUDED_)
#define AFX_HEADDLG_H__0872C69B_1D21_43D7_B663_1B55BCE911CC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HeadDlg.h : header file
//

#include "Database.h"
#include "List.h"
#include "3dApi.h"
#include "RoundButtons.h"
#include "Utils.h"

namespace Foxjet3d
{
	/////////////////////////////////////////////////////////////////////////////
	// CHeadDlg dialog

	class _3D_API CHeadDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CHeadDlg (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::HEADSTRUCT & head, 
			UNITS units, const CMapStringToString & vAvailAddressMap, bool bRestricted, CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CHeadDlg)
	//}}AFX_DATA
	
		bool m_bReadOnly;
		bool m_bNew;
		ULONG m_lLineID;
		bool m_bSync;

		static CString ConfigToString (const FoxjetDatabase::HEADSTRUCT & h);

#ifdef __WINPRINTER__
		typedef struct 
		{
			ULONG m_lAddr;
			LPCTSTR m_lpsz;
		} PHCSTRUCT;

		static const PHCSTRUCT m_mapPHC [6];
		static const PHCSTRUCT m_mapUSB [8];
		static const PHCSTRUCT m_mapIVPHC [2];

		CMapStringToString m_vAvailAddressMap;

		static CString GetPHC (ULONG lAddr);
		static ULONG GetPHC (const CString & str);

#endif //__WINPRINTER__

#ifdef __IPPRINTER__
	public:
	
		static DWORD GetSyncSettings (FoxjetDatabase::COdbcDatabase & db);

		static const CString m_strSyncKey;

#endif //__IPPRINTER__

		FoxjetDatabase::HEADSTRUCT GetHead () { return m_head; }

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CHeadDlg)
	protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	public:
		static double GetNozzleSpan (FoxjetDatabase::HEADTYPE type);
		static int GetChannels (FoxjetDatabase::HEADTYPE type);
		static int CalcNFactor (double dAngle, double dRes);
		static int CalcMaxLineSpeed (const FoxjetDatabase::HEADSTRUCT & head);

		static const FoxjetCommon::LIMITSTRUCT m_lmtPhotocellRate;
		static const FoxjetCommon::LIMITSTRUCT m_lmtMphcPhotocellDelay;
		static const FoxjetCommon::LIMITSTRUCT m_lmtVxPhotocellDelay;
		static const FoxjetCommon::LIMITSTRUCT m_lmtRelativeHeight;
		static const FoxjetCommon::LIMITSTRUCT m_lmtIntTachSpeed;
		static const FoxjetCommon::LIMITSTRUCT m_lmtHeadName;

	// Implementation
	protected:
		FoxjetUtils::CRoundButtons m_buttons;
		FoxjetDatabase::COdbcDatabase & m_db;
		FoxjetDatabase::HEADSTRUCT m_head;
		FoxjetDatabase::PANELSTRUCT m_panel;
		const UNITS m_units;
		CBitmap m_bmpRTOL;
		CBitmap m_bmpLTOR;
		bool m_bRestricted;

		void FillType ();
		void FillAddress ();
		void FillPanels ();
		void FillHorzRes (int nHorzRes, int nDivisor);
		void FillEncRes ();
		void SetDlgItemCheck (UINT nID, bool bCheck);
		bool GetDlgItemCheck (UINT nID);
		int SelectClosestAngle (double dAngle);
		bool IsUsbSelected ();
		void SelectUsbShare (UINT nID, int nSelected);
		void FillUsbShare (UINT nID, int nSelected);

		afx_msg void OnTypeChanged ();
		afx_msg void OnAngleChanged ();
		afx_msg void OnPhotocellSource ();
		afx_msg void OnEncoderSource ();
		afx_msg void OnMore ();
		afx_msg void OnMaster ();
		afx_msg void OnSelChangeAddress ();
		afx_msg void OnSelChangeEncRes ();
		afx_msg void OnHorzResChanged ();

		virtual void OnOK ();

		void UpdateUI ();

		// Generated message map functions
		//{{AFX_MSG(CHeadDlg)
		virtual BOOL OnInitDialog();
		//}}AFX_MSG

		afx_msg void OnDirection ();
		afx_msg void OnEnabled ();
		afx_msg void OnSync ();
		afx_msg void OnUsePcRepeater ();

		LRESULT OnSyncComplete (WPARAM wParam, LPARAM lParam);

		DECLARE_MESSAGE_MAP()
	};


	class _3D_API CHeadPhcDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CHeadPhcDlg (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::HEADSTRUCT & head, 
			UNITS units, const CMapStringToString & vAvailAddressMap, bool bRestricted, CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CHeadPhcDlg)
	//}}AFX_DATA
	
		bool m_bReadOnly;
		bool m_bNew;
		ULONG m_lLineID;

#ifdef __WINPRINTER__
		typedef struct 
		{
			ULONG m_lAddr;
			LPCTSTR m_lpsz;
		} PHCSTRUCT;

		static const PHCSTRUCT m_mapPHC [6];
		static const PHCSTRUCT m_mapIVPHC [2];

		CMapStringToString m_vAvailAddressMap;

		static CString GetPHC (ULONG lAddr);
		static ULONG GetPHC (const CString & str);

#endif //__WINPRINTER__

#ifdef __IPPRINTER__
	public:
	
		static DWORD GetSyncSettings (FoxjetDatabase::COdbcDatabase & db);
		static void SetPcRepeater (FoxjetDatabase::COdbcDatabase & db, ULONG lHeadID, bool bUse);
		static bool GetPcRepeater (FoxjetDatabase::COdbcDatabase & db, ULONG lHeadID);

		static const CString m_strSyncKey;

#endif //__IPPRINTER__

		FoxjetDatabase::HEADSTRUCT GetHead () { return m_head; }

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CHeadPhcDlg)
	protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	public:
		static double GetNozzleSpan (FoxjetDatabase::HEADTYPE type);
		static int CalcNFactor (double dAngle, double dRes);
		static int CalcMaxLineSpeed (const FoxjetDatabase::HEADSTRUCT & head);

		static const FoxjetCommon::LIMITSTRUCT m_lmtPhotocellRate;
		static const FoxjetCommon::LIMITSTRUCT m_lmtMphcPhotocellDelay;
		static const FoxjetCommon::LIMITSTRUCT m_lmtVxPhotocellDelay;
		static const FoxjetCommon::LIMITSTRUCT m_lmtRelativeHeight;
		static const FoxjetCommon::LIMITSTRUCT m_lmtIntTachSpeed;
		static const FoxjetCommon::LIMITSTRUCT m_lmtHeadName;

	// Implementation
	protected:
		FoxjetDatabase::COdbcDatabase & m_db;
		FoxjetDatabase::HEADSTRUCT m_head;
		FoxjetDatabase::PANELSTRUCT m_panel;
		const UNITS m_units;
		CBitmap m_bmpRTOL;
		CBitmap m_bmpLTOR;

		void FillType ();
		void FillAddress ();
		void FillPanels ();
		void FillHorzRes (int nHorzRes, int nDivisor);
		void FillEncRes ();
		void SetDlgItemCheck (UINT nID, bool bCheck);
		bool GetDlgItemCheck (UINT nID);
		int SelectClosestAngle (double dAngle);

		afx_msg void OnTypeChanged ();
		afx_msg void OnAngleChanged ();
		afx_msg void OnPhotocellSource ();
		afx_msg void OnEncoderSource ();
		afx_msg void OnSerial ();
		afx_msg void OnMaster ();
		afx_msg void OnSelChangeAddress ();
		afx_msg void OnSelChangeEncRes ();
		afx_msg void OnHorzResChanged ();

		virtual void OnOK ();

		void UpdateUI ();

		// Generated message map functions
		//{{AFX_MSG(CHeadPhcDlg)
		virtual BOOL OnInitDialog();
		//}}AFX_MSG

		afx_msg void OnDirection ();
		afx_msg void OnEnabled ();
		afx_msg void OnSync ();
		afx_msg void OnUsePcRepeater ();

		bool m_bRestricted;

		DECLARE_MESSAGE_MAP()
	};

}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEADDLG_H__0872C69B_1D21_43D7_B663_1B55BCE911CC__INCLUDED_)
