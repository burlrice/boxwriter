#ifdef __BOXCACHE_INLINE__

void Foxjet3d::Box::CBox::CCache::Clear ()
{
	m_vrcHeads.RemoveAll ();
	m_rcPanel = rcNull;
	
	if (m_pLastParam) {
		delete m_pLastParam;
		m_pLastParam = NULL;
	}
}

void Foxjet3d::Box::CBox::CCache::Build (const Foxjet3d::Box::CBox & box, Foxjet3d::Box::CBoxParams & p)
{
	using namespace FoxjetDatabase;
	using namespace Foxjet3d::Box;

	const Panel::CPanel * pPanel = NULL;

	Clear ();
	m_rcPanel = box.CalcPanelRect (p);

	if (box.GetPanel (p.m_nPanel, &pPanel)) {
		m_pLastParam = new CBoxParams (p);

		for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) {
			const Panel::CBoundary & b = pPanel->m_vBounds [i];
			HEADRECTSTRUCT hr;

			hr.m_rc					= box.CalcHeadRect (b, p, false);
			hr.m_rcClipped			= box.CalcHeadRect (b, p, true);
			hr.m_lID				= b.GetID ();

			m_vrcHeads.Add (hr);
		}

	}
}

int Foxjet3d::Box::CBox::CCache::Find (ULONG lHeadID) const
{
	for (int i = 0; i < m_vrcHeads.GetSize (); i++) {
		const HEADRECTSTRUCT & hr = m_vrcHeads [i];

		if (hr.m_lID == lHeadID)
			return i;
	}

	return -1;
}

inline CRect Foxjet3d::Box::CBox::CCache::GetHeadRect (const Panel::CBoundary & head, const Foxjet3d::Box::CBox & box, 
	Foxjet3d::Box::CBoxParams & p, bool bClip) 
{
	CRect rc (0, 0, 0, 0);
	bool bValid = IsValid (p);

	if (!bValid)
		Build (box, p);

	int nIndex = Find (head.GetID ());

	if (nIndex == -1) {
		Build (box, p);
		nIndex = Find (head.GetID ());
	}

	if (nIndex != -1)
		rc = bClip ? m_vrcHeads [nIndex].m_rcClipped : m_vrcHeads [nIndex].m_rc;

	return rc;
}

inline CRect Foxjet3d::Box::CBox::CCache::GetPanelRect (const Foxjet3d::Box::CBox & box, Foxjet3d::Box::CBoxParams & p) 
{
	if (m_rcPanel == ::rcNull || !IsValid (p))
		Build (box, p);

	return m_rcPanel;
}

inline bool Foxjet3d::Box::CBox::CCache::IsValid (const Foxjet3d::Box::CBoxParams & p) const
{
	if (m_pLastParam) {
		const CBoxParams & rhs = * m_pLastParam;
		const CBoxParams & lhs = p;

		return 
			lhs.m_rZoom			== rhs.m_rZoom &&
			lhs.m_nPanel		== rhs.m_nPanel &&
			lhs.m_lHeadID		== rhs.m_lHeadID &&
			lhs.m_ptOffset		== rhs.m_ptOffset &&
			lhs.m_dRotation [0] == rhs.m_dRotation [0] &&
			lhs.m_dRotation [1] == rhs.m_dRotation [1];
/*
			lhs.m_uBlend		== rhs.m_uBlend &&
			lhs.m_bMessages		== rhs.m_bMessages &&
			lhs.m_bHeads		== rhs.m_bHeads &&
			lhs.m_bDirection	== rhs.m_bDirection &&
			lhs.m_task			== rhs.m_task &&
			lhs.m_vHeads		== rhs.m_vHeads;
*/
	}

	return false;
}

#endif //__BOXCACHE_INLINE__

