#if !defined(AFX_USERELEMENTDLG_H__8C5EA503_766E_4FC4_A409_A9D71F4CC6AF__INCLUDED_)
#define AFX_USERELEMENTDLG_H__8C5EA503_766E_4FC4_A409_A9D71F4CC6AF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserElementDlg.h : header file
//

#include "ListCtrlImp.h"
#include "List.h"
#include "Database.h"
#include "3dApi.h"
#include "RoundButtons.h"
#include "Utils.h"
#include "DatabaseStartDlg.h"
#include "AutoComplete.h"
#include "Element.h"

#ifdef __WINPRINTER__
	#include "BitmapElement.h"
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserElementDlg dialog

namespace Foxjet3d
{
	namespace UserElementDlg
	{
		typedef FoxjetCommon::CCopyArray <FoxjetCommon::CBaseElement *, FoxjetCommon::CBaseElement *> CElementPtrArray;

		class _3D_API CUserItem : public ItiLibrary::ListCtrlImp::CItem
		{
		public:
			CUserItem (FoxjetCommon::CBaseElement * pElement = NULL, FoxjetDatabase::HEADSTRUCT * pHead = NULL,
				const CString & strPrompt = _T (""), const CString & strData = _T (""));
			CUserItem (const CUserItem & rhs);
			CUserItem & operator = (const CUserItem & rhs);
			virtual ~CUserItem ();

			virtual CString GetDispText (int nColumn) const;

			FoxjetCommon::CBaseElement * m_pElement;
			FoxjetDatabase::HEADSTRUCT m_head;

			CString m_strPrompt;
			CString m_strData;
		};

		typedef CArray <CUserItem, CUserItem &> CUserElementArray;

		class _3D_API CUserElementDlg : public FoxjetCommon::CEliteDlg
		{
		public:
			static FoxjetCommon::CBaseElement * Find (FoxjetCommon::CBaseElement * pElement, 
				const CUserElementArray & vElements);
			static FoxjetCommon::CBaseElement * Find (const CString & str,
				const CUserElementArray & vElements);

			CUserElementArray m_vElements;

		// Construction
		public:
			virtual void OnOK();
			CUserElementDlg(ULONG lLineID, CWnd* pParent = NULL);   // standard constructor

			//#ifdef __WINPRINTER__
			static bool Get (const FoxjetCommon::CBaseElement * pElement, const FoxjetDatabase::HEADSTRUCT & head, Foxjet3d::UserElementDlg::CUserElementArray & vPrompt, bool bTaskStart);
			static bool Set (const FoxjetCommon::CBaseElement * pElement, const FoxjetDatabase::HEADSTRUCT & head, const CString & strPrompt, const CString & strData, const CString & strKeyValue, const FoxjetDatabase::USERSTRUCT & user, const FoxjetUtils::CDatabaseStartDlg & dlgDB);
			static void ResetSize (FoxjetElements::CBitmapElement * p, const FoxjetDatabase::HEADSTRUCT & h);
			//#endif

		// Dialog Data
			//{{AFX_DATA(CUserElementDlg)
				// NOTE: the ClassWizard will add data members here
			//}}AFX_DATA


		// Overrides
			// ClassWizard generated virtual function overrides
			//{{AFX_VIRTUAL(CUserElementDlg)
			protected:
			virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
			//}}AFX_VIRTUAL

		// Implementation
		protected:
			class CEditEx : public FoxjetUtils::CAutoEdit
			{
			public:
				CEditEx (CUserElementDlg * pParent) : m_pParent (pParent), FoxjetUtils::CAutoEdit (pParent) { }

				virtual std::vector <std::wstring> OnTyping (const CString & str);

				CUserElementDlg * m_pParent;
			};

			CEditEx m_txtData;

			FoxjetUtils::CRoundButtons m_buttons;
			ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

			// Generated message map functions
			//{{AFX_MSG(CUserElementDlg)
			virtual BOOL OnInitDialog();
			afx_msg void OnItemchangedElements(NMHDR* pNMHDR, LRESULT* pResult);
			afx_msg void OnSelect();
			//}}AFX_MSG
			DECLARE_MESSAGE_MAP()

			afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

			ULONG m_lLineID;
		};
	}; //namespace UserElementDlg
}; 

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERELEMENTDLG_H__8C5EA503_766E_4FC4_A409_A9D71F4CC6AF__INCLUDED_)
