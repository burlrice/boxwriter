// LineDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "Resource.h"
#include "LineDlg.h"
#include "AmsDlg.h"
#include "ComPropertiesDlg.h"
#include "Debug.h"
#include "AppVer.h"
#include "ScannerPropDlg.h"
#include "StdCommDlg.h"
#include "Parse.h"

#include <AfxSock.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;

/////////////////////////////////////////////////////////////////////////////
// CLineDlg dialog

const FoxjetCommon::LIMITSTRUCT CLineDlg::m_lmtName			= {	1,	80 }; 


CLineDlg::CLineDlg (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::LINESTRUCT & line, CWnd* pParent)
:	m_db (db),
	m_line (line),
	m_bResetCountsOnRestart (FALSE),
	m_bRequireLogin (FALSE),
	FoxjetCommon::CEliteDlg(IDD_LINE_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CLineDlg)
	//}}AFX_DATA_INIT
}


void CLineDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLineDlg)
	//}}AFX_DATA_MAP

	DDX_Text(pDX, TXT_NAME, m_line.m_strName);
	DDV_MinMaxChars (pDX, m_line.m_strName, TXT_NAME, m_lmtName);

	DDX_Text(pDX, TXT_DESCRIPTION, m_line.m_strDesc);

	DDX_Text(pDX, TXT_CONSECUTIVENOREADS, m_line.m_nMaxConsecutiveNoReads);

	DDX_Text(pDX, TXT_BUFFEROFFSET, m_line.m_nBufferOffset);
	
	DDX_Text(pDX, TXT_DATALENGTH, m_line.m_nSignificantChars);
	
	//DDX_Control(pDX, IDC_AUXIP, m_ctrlAuxBoxIp);
	DDX_Text(pDX, TXT_NOREAD, m_line.m_strNoRead);	
	//DDX_Text(pDX, TXT_SERIAL1, m_line.m_strSerialParams1);
	//DDX_Text(pDX, TXT_SERIAL2, m_line.m_strSerialParams2);
	//DDX_Text(pDX, TXT_SERIAL3, m_line.m_strSerialParams3);
	//DDX_Check(pDX, CHK_AUXENABLED, m_line.m_bAuxBoxEnabled);
	DDX_Check(pDX, CHK_RESETONTASKSTART, m_line.m_bResetScanInfo);

	DDX_Check(pDX, CHK_RESETCOUNTS, m_bResetCountsOnRestart);
	DDX_Check(pDX, CHK_REQUIRELOGIN, m_bRequireLogin);

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_AMS);
	m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);
}


BEGIN_MESSAGE_MAP(CLineDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CLineDlg)
	ON_BN_CLICKED(BTN_AMS, OnAms)
	//ON_BN_CLICKED(BTN_SERIAL1, OnSerial1)
	//ON_BN_CLICKED(BTN_SERIAL2, OnSerial2)
	//ON_BN_CLICKED(BTN_SERIAL3, OnSerial3)
	//}}AFX_MSG_MAP
	//ON_BN_CLICKED(CHK_AUXENABLED, OnClickAuxEnabled)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLineDlg message handlers

void CLineDlg::OnAms() 
{
	CAmsDlg dlg (m_line, this);

	if (dlg.DoModal () == IDOK)
		m_line = dlg.m_line;
}

/*
void CLineDlg::OnSerial1() 
{
	CString str;

	GetDlgItemText (TXT_SERIAL1, str);
	
	if (EditSerialParams (str))
		SetDlgItemText (TXT_SERIAL1, str);
}

void CLineDlg::OnSerial2() 
{
	CString str;

	GetDlgItemText (TXT_SERIAL2, str);
	
	if (EditSerialParams (str))
		SetDlgItemText (TXT_SERIAL2, str);
}

void CLineDlg::OnSerial3() 
{
	CString str;

	GetDlgItemText (TXT_SERIAL3, str);
	
	if (EditSerialParams (str))
		SetDlgItemText (TXT_SERIAL3, str);
}
*/

BOOL CLineDlg::OnInitDialog() 
{
	CComboBox * pSerial = (CComboBox *)GetDlgItem (CB_SERIALDOWNLOAD);
	CUIntArray v;
	CStringArray vPorts;
	CString strParams;

	ASSERT (pSerial);

	{ // for backward compatability
		CStringArray v;

		Tokenize (m_strSerialDownload, v);

		if (v.GetSize ())
			m_strSerialDownload = v [0];
	}

//#if __CUSTOM__ == __SW0820__
	{
		using namespace FoxjetDatabase;
		using namespace FoxjetUtils;

		SETTINGSSTRUCT s;
		FIXEDSCANNERSTRUCT scanner;

		CScannerPropDlg::Init (scanner);

		if (GetSettingsRecord (m_db, m_line.m_lID /* 0 */, lpszScannerKey, s)) {
			TRACEF (s.m_strData);
			CScannerPropDlg::FromString (s.m_strData, scanner);
			
			//m_line.m_bResetScanInfo			= scanner.m_bReset;
			//m_line.m_strNoRead				= scanner.m_strNoRead;
			//m_line.m_nMaxConsecutiveNoReads	= scanner.m_nNoReads;
		}

		if (CButton * p = (CButton *)GetDlgItem (CHK_IDLE)) 
			p->SetCheck (scanner.m_bIdle ? 1 : 0);
	}
//#endif


	FoxjetCommon::CEliteDlg::OnInitDialog();

	//m_ctrlAuxBoxIp.SetAddress (htonl (inet_addr (w2a (m_line.m_strAuxBoxIp))));
	EnumerateSerialPorts (v);

	for (int i = 0; i < v.GetSize (); i++) {
		CString str;
		int nPort = v [i];
		
		str.Format (_T ("COM%d"), nPort);
		vPorts.Add (str);
	}

	/*
	vPorts.Add (_T ("HUB B"));
	vPorts.Add (_T ("HUB C"));
	vPorts.Add (_T ("HUB D"));
	*/

	for (int i = 0; i < vPorts.GetSize (); i++) {
		CString str = vPorts [i];
		int nIndex = pSerial->AddString (str);

		pSerial->SetItemData (nIndex, (str.Find (_T ("HUB")) == -1) ? 1 : 0);

		if (!m_strSerialDownload.CompareNoCase (str))
			pSerial->SetCurSel (nIndex);
	}

	if (pSerial->GetCurSel () == CB_ERR)
		pSerial->SetCurSel (0);

/* #if __CUSTOM__ == __SW0820__
	{
		UINT nID [] = 
		{
			TXT_NOREAD,
			TXT_CONSECUTIVENOREADS,
			CHK_RESETONTASKSTART,
		};

		for (int i = 0; i < ARRAYSIZE (nID); i++)
			if (CWnd * p = GetDlgItem (nID [i]))
				p->EnableWindow (false);
	}
#endif */

#ifdef __IPPRINTER__									// TODO: rem
	if (FoxjetCommon::verApp < CVersion (1, 30))		// TODO: rem
		if (CWnd * p = GetDlgItem (CHK_REQUIRELOGIN))	// TODO: rem
			p->ShowWindow (SW_HIDE);					// TODO: rem
#endif													// TODO: rem

	//if (ISVALVE () || FoxjetDatabase::IsHighResDisplay ()) 
	{
		if (CWnd * p = GetDlgItem (BTN_AMS))
			p->ShowWindow (SW_HIDE);
	}

	//OnClickAuxEnabled ();

	if (IsControl (true) && IsNetworked ()) {
		UINT n [] = 
		{
			TXT_NAME,
		};

		for (int i = 0; i < ARRAYSIZE (n); i++) 
			if (CWnd * p = GetDlgItem (n [i]))
				p->EnableWindow (FALSE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLineDlg::OnOK()
{
	ULONG lAddress = 0;
	in_addr addr;
	CComboBox * pSerial = (CComboBox *)GetDlgItem (CB_SERIALDOWNLOAD);
	CString strPort, strParams;

	ASSERT (pSerial);
	//m_ctrlAuxBoxIp.GetAddress (lAddress);

	addr.S_un.S_addr = ntohl (lAddress);
	m_line.m_strAuxBoxIp = inet_ntoa (addr);

	int nIndex = pSerial->GetCurSel ();

	ASSERT (nIndex != CB_ERR);

	if (pSerial->GetLBTextLen (nIndex) > 0)
		pSerial->GetLBText (nIndex, strPort);

	m_strSerialDownload = strPort;

	if (m_line.m_lID != -1) { // __SW0820__
		using namespace FoxjetDatabase;
		using namespace FoxjetUtils;

		SETTINGSSTRUCT s;
		FIXEDSCANNERSTRUCT scanner;

		CScannerPropDlg::Init (scanner);

		if (CButton * p = (CButton *)GetDlgItem (CHK_IDLE)) 
			scanner.m_bIdle = p->GetCheck () == 1 ? true : false;

		s.m_strKey	= lpszScannerKey;
		s.m_lLineID = m_line.m_lID;
		s.m_strData = CScannerPropDlg::ToString (scanner);

		TRACEF (s.m_strData);

		if (!UpdateSettingsRecord (m_db, s)) 
			VERIFY (AddSettingsRecord (m_db, s));
	}

	FoxjetCommon::CEliteDlg::OnOK ();
}

/*
void CLineDlg::OnClickAuxEnabled ()
{
	UINT nID [] = 
	{
		IDC_AUXIP,
		BTN_SERIAL1,
		BTN_SERIAL2,
		BTN_SERIAL3,
	};
	bool bEnabled = false;

	if (CButton * p = (CButton *)GetDlgItem (CHK_AUXENABLED)) 
		bEnabled = p->GetCheck () == 1;

	for (int i = 0; i < ARRAYSIZE (nID); i++)
		if (CWnd * p = GetDlgItem (nID [i]))
			p->EnableWindow (bEnabled);

}
*/