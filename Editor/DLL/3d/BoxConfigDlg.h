#if !defined(AFX_BOXCONFIGDLG_H__823C1616_C58E_4BE0_A8A8_4B0B5C13C19C__INCLUDED_)
#define AFX_BOXCONFIGDLG_H__823C1616_C58E_4BE0_A8A8_4B0B5C13C19C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BoxConfigDlg.h : header file
//

#include "ListCtrlImp.h"
#include "Database.h"
#include "List.h"
#include "3dApi.h"
#include "Utils.h"

namespace Foxjet3d
{
	/////////////////////////////////////////////////////////////////////////////
	// CBoxConfigDlg dialog

	class _3D_API CBoxConfigDlg : public FoxjetCommon::CEliteDlg
	{
	protected:
		class _3D_API CItem : public ItiLibrary::ListCtrlImp::CItem 
		{
		public:
			CItem ();
			CItem (const FoxjetDatabase::BOXSTRUCT & box, UNITS units);
			virtual ~CItem ();

			virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
			virtual CString GetDispText (int nColumn) const;

			FoxjetDatabase::BOXSTRUCT m_box;
			UNITS m_units;
		};

	public:
		CBoxConfigDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CBoxConfigDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		UNITS m_units;
		bool m_bReadOnly;
		CArray <ULONG, ULONG> m_vIDUpdate;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CBoxConfigDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual void OnCancel();
		virtual void OnOK();
		void InitBoxCtrl();

		ItiLibrary::ListCtrlImp::CListCtrlImp m_lviBox;
		FoxjetDatabase::COdbcDatabase & m_db;
		CArray <FoxjetDatabase::IMAGESTRUCT, FoxjetDatabase::IMAGESTRUCT &> m_vImages;

		// Generated message map functions
		//{{AFX_MSG(CBoxConfigDlg)
		afx_msg void OnAdd();
		afx_msg void OnDelete();
		afx_msg void OnEdit();
		virtual BOOL OnInitDialog();
		afx_msg void OnDblclkBoxes(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnHelp ();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BOXCONFIGDLG_H__823C1616_C58E_4BE0_A8A8_4B0B5C13C19C__INCLUDED_)
