// CardTypeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "3d.h"
#include "CardTypeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Foxjet3d;

/////////////////////////////////////////////////////////////////////////////
// CCardTypeDlg dialog


CCardTypeDlg::CCardTypeDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(IDD_CARDTYPE, pParent)
{
	//{{AFX_DATA_INIT(CCardTypeDlg)
	m_nType = CARD_USB;
	//}}AFX_DATA_INIT
}


void CCardTypeDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCardTypeDlg)
	DDX_Radio(pDX, RDO_PHC, m_nType);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCardTypeDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CCardTypeDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCardTypeDlg message handlers
