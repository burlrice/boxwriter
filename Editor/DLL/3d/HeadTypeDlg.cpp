// HeadTypeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "AppVer.h"
#include "HeadTypeDlg.h"
#include "DbTypeDefs.h"
#include "Extern.h"
#include "Registry.h"

using namespace FoxjetDatabase;
using namespace ListGlobals;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHeadTypeDlg dialog


CHeadTypeDlg::CHeadTypeDlg(CWnd* pParent /*=NULL*/)
:	m_nHeadType (HEADTYPE_FIRST),
	FoxjetCommon::CEliteDlg(IDD_HEADTYPE, pParent)
{
	//{{AFX_DATA_INIT(CHeadTypeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CHeadTypeDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHeadTypeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHeadTypeDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CHeadTypeDlg)
	ON_LBN_DBLCLK(LB_TYPE, OnDblclkType)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHeadTypeDlg message handlers

BOOL CHeadTypeDlg::OnInitDialog() 
{
	const bool bValve = ISVALVE ();
	CListBox * pLB = (CListBox *)GetDlgItem (LB_TYPE);
	const CString strSection = defElements.m_strRegSection + _T ("\\CHeadTypeDlg");
	struct 
	{ 
		UINT m_nType;
		UINT m_nID;
		bool m_bIsIV;
	} const map [] = 
	{
		{ UJ2_352_32,			IDS_UJ2_352_32,			false,	},
		{ UJ2_192_32,			IDS_UJ2_192_32,			false,	},
		{ UJ2_96_32,			IDS_UJ2_96_32,			false,	},

#if __CUSTOM__ != __SW0810__
		{ GRAPHICS_768_256,		IDS_GRAPHICS_768_256,	false,	},
		{ GRAPHICS_384_128,		IDS_GRAPHICS_384_128,	false,	},
		{ UR2,					IDS_UR2,				false	},
		{ UR4,					IDS_UR4,				false	},
#endif 

#ifdef __WINPRINTER__ 
		{ IV_72,				IDS_IV_72,				true,	},
#endif 

		{ UJ2_192_32NP,			IDS_UJ2_192_32NP,		false,	},
		{ ALPHA_CODER,			IDS_ALPHA_CODER,		false,	},

#ifdef __HPHEAD__
		{ HPHEAD,				IDS_HPHEAD,				false,	},
#endif //__HPHEAD__
	};

	ASSERT (pLB);

	FoxjetCommon::CEliteDlg::OnInitDialog();
	m_nHeadType = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("m_nHeadType"), m_nHeadType);
	
	for (int i = 0; i < (sizeof (map) / sizeof (map [0])); i++) {
		#ifndef _DEBUG
		if (map [i].m_bIsIV != bValve) 
			continue;
		#endif

		int nIndex = pLB->AddString (LoadString (map [i].m_nID));
		int nType = map [i].m_nType;

		pLB->SetItemData (nIndex, nType);

		if (m_nHeadType == nType)
			pLB->SetCurSel (nIndex);
	}

	if (pLB->GetCurSel () == CB_ERR)
		pLB->SetCurSel (0);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CHeadTypeDlg::OnOK()
{
	const CString strSection = defElements.m_strRegSection + _T ("\\CHeadTypeDlg");
	CListBox * pLB = (CListBox *)GetDlgItem (LB_TYPE);
	ASSERT (pLB);

	m_nHeadType = pLB->GetItemData (pLB->GetCurSel ());
	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("m_nHeadType"),	m_nHeadType);

	FoxjetCommon::CEliteDlg::OnOK ();
}

void CHeadTypeDlg::OnDblclkType() 
{
	OnOK ();	
}
