// 3d.h : main header file for the 3D DLL
//

#if !defined(AFX_3D_H__CBD829C4_AACC_462D_990B_DF1D6A7A30AF__INCLUDED_)
#define AFX_3D_H__CBD829C4_AACC_462D_990B_DF1D6A7A30AF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

CString LoadString (UINT nID);
HINSTANCE GetInstanceHandle ();

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_3D_H__CBD829C4_AACC_462D_990B_DF1D6A7A30AF__INCLUDED_)
