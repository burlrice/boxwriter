#if !defined(AFX_CARDTYPEDLG_H__220313BB_EE89_4755_89BF_F7436EE54728__INCLUDED_)
#define AFX_CARDTYPEDLG_H__220313BB_EE89_4755_89BF_F7436EE54728__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CardTypeDlg.h : header file
//

#include "3dApi.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CCardTypeDlg dialog

namespace Foxjet3d
{
	class _3D_API CCardTypeDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CCardTypeDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CCardTypeDlg)
		int		m_nType;
		//}}AFX_DATA

		typedef enum
		{
			CARD_PHC = 0,
			CARD_USB,
		} CARDTYPE;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CCardTypeDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:

		// Generated message map functions
		//{{AFX_MSG(CCardTypeDlg)
			// NOTE: the ClassWizard will add member functions here
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CARDTYPEDLG_H__220313BB_EE89_4755_89BF_F7436EE54728__INCLUDED_)
