// InkUsageDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "3d.h"
#include "InkUsageDlg.h"
#include "Extern.h"
#include "Database.h"
#include "Registry.h"
#include "Debug.h"
#include "Utils.h"
#include "DevGuids.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ListGlobals;
using namespace ItiLibrary;
using namespace Foxjet3d;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace InkUsageDlg;
using namespace Element;

CElementItem::CElementItem (CElement & e, CEditorElementList & list)
:	m_element (e),
	m_list (list),
	m_dInkCost (0.0),
	m_lPixels (0),
	m_dInkUsed (0.0)
{
	CDC dcMem;
	CBitmap bmp;
	HEADSTRUCT head = list.GetHead ();
	CBaseElement * p = e.GetElement ();

	if (head.m_bEnabled) {
		VERIFY (dcMem.CreateCompatibleDC (NULL));

		p->SetRedraw (true);
		CSize sizePrinter = p->Draw (dcMem, head, true, FoxjetCommon::PRINTER);
		VERIFY (bmp.CreateCompatibleBitmap (&dcMem, sizePrinter.cx, sizePrinter.cy));

		CBitmap * pOld = dcMem.SelectObject (&bmp);

		::BitBlt (dcMem, 0, 0, sizePrinter.cx, sizePrinter.cy, NULL, 0, 0, WHITENESS);
		p->Draw (dcMem, head, false, FoxjetCommon::PRINTER);
		m_lPixels = CountPixels (bmp);

		if (head.m_bDoublePulse)
			m_lPixels *= 2;

		// restore internal size
		p->SetRedraw ();
		p->Draw (dcMem, head, true, FoxjetCommon::EDITOR);

		dcMem.SelectObject (pOld);
	}
}

CElementItem::~CElementItem ()
{
}

int CElementItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	CElementItem & cmp = (CElementItem &)rhs;
	CString strLHS = GetDispText (nColumn);
	CString strRHS = cmp.GetDispText (nColumn);

	switch (nColumn) {
	case 0: // %d
	case 3: // %d
		{
			int nLHS = _ttoi (strLHS);
			int nRHS = _ttoi (strRHS);

			return nLHS - nRHS;
		}
		break;
	case 4: // %f
		{
			double dLHS = FoxjetDatabase::_ttof (strLHS);
			double dRHS = FoxjetDatabase::_ttof (strRHS);

			return (int)(dLHS * 100.0) - (int)(dRHS * 100.0);
		}
		break;
	case 5: // COleCurrency
		{
			COleCurrency curRHS, curLHS;
			
			curLHS.ParseCurrency (strLHS);
			curLHS.ParseCurrency (strRHS);

			if (curLHS > curRHS) 
				return 1;
			else if (curLHS < curRHS) 
				return -1;
			else 
				return 0;
		}
		break;
	}

	return CItem::Compare (rhs, nColumn);
}

CString CElementItem::GetDispText (int nColumn) const
{
	CString str;
	LPCTSTR lpszDouble = _T ("%.08f");

	switch (nColumn) {
	case 0:
		str.Format (_T ("%d"), m_element.GetElement ()->GetID ());
		break;
	case 1:
		str.Format (_T ("%s"), GetElementResStr (m_element.GetElement ()->GetClassID ()));
		break;
	case 2:
		str.Format (_T ("%s"), m_list.GetHead ().m_strName);
		break;
	case 3:
		str.Format (_T ("%d"), m_lPixels);
		break;
	case 4:
		{
			double d = CalcInkUsage (m_list.GetHead ().m_nHeadType, 
				INKTYPE_SCANTRUE2, // TODO
				m_lPixels);

			str.Format (lpszDouble, d);
		}
		break;
	case 5:
		{
			double dCost = m_dInkUsed * m_dInkCost; //CalcCost (); 
			str.Format (lpszDouble, dCost);
		}
		break;
	}

	return str;
}

bool CElementItem::IsNumeric (int nColumn) const
{
	switch (nColumn) {
	case 0: 
	case 3: 
	case 4: 
	case 5:
		return true;
	}

	return false;
}

double CElementItem::CalcCost () 
{
	m_dInkUsed = CalcInkUsage (m_list.GetHead ().m_nHeadType, 
		INKTYPE_SCANTRUE2, // TODO
		m_lPixels);

	// pico: 10^12, milli: 10^3
	// 0.000000001

	return m_dInkUsed * m_dInkCost;
}

/////////////////////////////////////////////////////////////////////////////
// CInkUsageDlg dialog

const CString CInkUsageDlg::m_strRegSection = defElements.m_strRegSection + _T ("\\CInkUsageDlg");

const FoxjetCommon::LIMITSTRUCT CInkUsageDlg::m_lmtSizeVX	= { 1,		35		};
const FoxjetCommon::LIMITSTRUCT CInkUsageDlg::m_lmtSizePro	= { 1,		9999	};

CInkUsageDlg::CInkUsageDlg(CTask & task, CWnd* pParent /*=NULL*/)
:	m_task (task),
	FoxjetCommon::CEliteDlg(IDD_INKUSAGE, pParent)
{
	//{{AFX_DATA_INIT(CInkUsageDlg)
	//}}AFX_DATA_INIT
	CString strDef = _T ("500.0");
	bool bValve = ISVALVE ();

	if (bValve)
		strDef = _T ("5");

	m_dSize = FoxjetDatabase::_ttof (FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("Size"), strDef));

	if (bValve)
		m_dSize = BindTo (m_dSize, (double)m_lmtSizeVX.m_dwMin, (double)m_lmtSizeVX.m_dwMax);
	else
		m_dSize = BindTo (m_dSize, (double)m_lmtSizePro.m_dwMin, (double)m_lmtSizePro.m_dwMax);

	m_curCost = COleCurrency (230, 0);
	m_curCost.ParseCurrency (FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("Cost"), _T ("245.00")));
}


void CInkUsageDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInkUsageDlg)
	DDX_Text(pDX, TXT_COST, m_curCost);
	DDX_Text(pDX, TXT_SIZE, m_dSize);
	//}}AFX_DATA_MAP

	if (ISVALVE ())
		DDV_MinMaxDouble (pDX, m_dSize, (double)m_lmtSizeVX.m_dwMin, (double)m_lmtSizeVX.m_dwMax);
	else
		DDV_MinMaxDouble (pDX, m_dSize, (double)m_lmtSizePro.m_dwMin, (double)m_lmtSizePro.m_dwMax);
}


BEGIN_MESSAGE_MAP(CInkUsageDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CInkUsageDlg)
	ON_BN_CLICKED(BTN_CALC, OnCalc)
	//}}AFX_MSG_MAP
	ON_NOTIFY(LVN_ITEMCHANGED, LV_ELEMENTS, OnItemchangedElements)
	ON_BN_CLICKED(BTN_EXPORT, OnExport)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInkUsageDlg message handlers

void CInkUsageDlg::OnCalc() 
{
	UpdateData (TRUE);

	double dTotal = 0.0, dInk = 0.0;
	double dCostPerML = FoxjetDatabase::_ttof (m_curCost.Format ()) / m_dSize;
	ULONG lPixels = 0;
	CString strSize;
	bool bValve = ISVALVE ();

	if (bValve) {
		// 220,000,000 pixels in 5 gallons (5 gallons = 18927.06 ml)
		double dSize = (m_dSize * (18927.06 / 5.0));

		dCostPerML = FoxjetDatabase::_ttof (m_curCost.Format ()) / dSize;
	}

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		CElementItem * pItem = (CElementItem *)m_lv.GetCtrlData (i);

		ASSERT (pItem);

		pItem->m_dInkCost = dCostPerML;
		dTotal	+= pItem->CalcCost ();
		dInk	+= pItem->m_dInkUsed;
		lPixels	+= pItem->m_lPixels;
		m_lv.UpdateCtrlData (pItem);
	}

	strSize.Format (_T ("%f"), m_dSize);

	FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("Size"), strSize);
	FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, m_strRegSection, _T ("Cost"), m_curCost.Format ());

	int nPrints = (int)floor (m_dSize / dInk);

	#ifdef _DEBUG
	if (CElementItem * pItem = (CElementItem *)m_lv.GetCtrlData (0)) {
		long double dPixel = FoxjetDatabase::CalcInkUsage (pItem->m_list.GetHead ().m_nHeadType, FoxjetDatabase::INKTYPE_FIRST, 1);
		unsigned __int64 lMax = (unsigned __int64)((long double)500.0 / dPixel);
		TRACEF (std::to_string (dPixel).c_str ());
		TRACEF (std::to_string (lMax).c_str ());
		unsigned __int64 lPrints = lMax / (unsigned __int64)lPixels;
		TRACEF (std::to_string ((unsigned __int64)nPrints).c_str ());
		TRACEF (std::to_string (lPrints).c_str ());
	}
	#endif
	
	if (bValve)  // 220,000,000 pixels in 5 gallons
		nPrints = (int)floor (220000000.0 * (m_dSize / 5.0) / (double)lPixels);

	SetDlgItemInt (TXT_PRINTS, nPrints);

	OnItemchangedElements (NULL, NULL);
}

BOOL CInkUsageDlg::OnInitDialog() 
{
	using ListCtrlImp::CColumn;

	CArray <CColumn, CColumn> vCols;
	bool bValve = ISVALVE ();

	FoxjetCommon::CEliteDlg::OnInitDialog();

	// id type head pixels cost
	vCols.Add (CColumn (LoadString (IDS_ID), 20));
	vCols.Add (CColumn (LoadString (IDS_TYPE)));
	vCols.Add (CColumn (LoadString (IDS_HEAD)));
	vCols.Add (CColumn (LoadString (IDS_PIXELS)));
	vCols.Add (CColumn (LoadString (IDS_INKUSED)));
	vCols.Add (CColumn (LoadString (IDS_COST)));

	m_lv.Create (LV_ELEMENTS, vCols, this, defElements.m_strRegSection);

	for (int nMessage = 0; nMessage < m_task.GetMessageCount (); nMessage++) {
		CMessage * pMsg = m_task.GetMessage (nMessage);

		ASSERT (pMsg);

		for (int nElement = 0; nElement < pMsg->m_list.GetSize (); nElement++) {
			CElement & e = pMsg->m_list.GetElement (nElement);

			m_lv.InsertCtrlData (new CElementItem (e, pMsg->m_list));
		}
	}

	if (bValve) 
		SetDlgItemText (LBL_SIZE, LoadString (IDS_SIZEGALLONS));

	OnCalc ();

	return TRUE;
}

void CInkUsageDlg::OnItemchangedElements(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CLongArray sel = GetSelectedItems (m_lv);
	double dTotal = 0.0, dInk = 0.0;
	double dCostPerML = FoxjetDatabase::_ttof (m_curCost.Format ()) / m_dSize;
	CString strCost = LoadString (IDS_SELECTEDCOST);
	CString strInk = LoadString (IDS_SELECTEDINK);

	if (sel.GetSize () == 0) {
		strCost = LoadString (IDS_TOTALCOST);
		strInk = LoadString (IDS_TOTALINK);
		
		for (int i = 0; i < m_lv->GetItemCount (); i++)
			sel.Add (i);
	}

	SetDlgItemText (LBL_TOTAL, strCost);
	SetDlgItemText (LBL_TOTALINK, strInk);

	for (int i = 0; i < sel.GetSize (); i++) {
		CElementItem * pItem = (CElementItem *)m_lv.GetCtrlData (sel [i]);

		ASSERT (pItem);

		dTotal	+= (pItem->m_dInkUsed * pItem->m_dInkCost);
		dInk	+= pItem->m_dInkUsed;
	}

/*
	int nDollars = (int)floor (dTotal);
	int nCents = (int)((dTotal - (double)nDollars) * 10000);
	COleCurrency cur (nDollars, nCents);
*/
	CString str;

	str.Format (_T ("%f"), dTotal);
	SetDlgItemText (TXT_TOTAL, str); //cur.Format ());
	
	str.Format (_T ("%f"), dInk);
	SetDlgItemText (TXT_TOTALINK, str);

	if (pResult)
		* pResult = 0;
}

void CInkUsageDlg::OnExport() 
{
	const CString strCrLf = _T ("\n"); //_T ("\r\n");
	CString strFile = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER,
		ListGlobals::defElements.m_strRegSection + _T ("\\CInkUsageDlg"), 
		_T ("Export"), GetHomeDir () + _T ("\\InkUsage.txt"));
	CFileDialog dlg (FALSE, _T ("*.txt"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
		_T ("Text files (*.txt)|*.txt|All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();
		FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER,
			ListGlobals::defElements.m_strRegSection + _T ("\\CInkUsageDlg"), 
			_T ("Export"), strFile);
		VERIFY (ItiLibrary::Export (m_lv, strFile));
	}
}
