#include "stdafx.h"
#include "3d.h"
#include "Box.h"
#include "Compare.h"
#include "Database.h"

using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace Box;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CBox::CCache::CCache ()
:	m_pLastParam (NULL),
	m_rcPanel (rcNull)
{
}

CBox::CCache::~CCache ()
{
	Clear ();
}

