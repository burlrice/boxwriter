#if !defined(AFX_HEADMAPDLG_H__8D0BADBE_0A8C_41B8_B260_DEE3F4E72B90__INCLUDED_)
#define AFX_HEADMAPDLG_H__8D0BADBE_0A8C_41B8_B260_DEE3F4E72B90__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HeadMapDlg.h : header file
//

#include "3dApi.h"
#include "Database.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CHeadMapDlg dialog

namespace Foxjet3d
{
	class _3D_API CHeadMapDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		virtual void OnOK();
		CHeadMapDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CHeadMapDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA


		FoxjetDatabase::HEADSTRUCT m_headFrom;
		FoxjetDatabase::HEADSTRUCT m_headTo;
		CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> m_vHeads;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CHeadMapDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:

		// Generated message map functions
		//{{AFX_MSG(CHeadMapDlg)
		afx_msg void OnFromproperties();
		afx_msg void OnToproperties();
		virtual BOOL OnInitDialog();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEADMAPDLG_H__8D0BADBE_0A8C_41B8_B260_DEE3F4E72B90__INCLUDED_)
