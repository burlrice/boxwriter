#if !defined(AFX_VALVEPROPDLG_H__386719DD_8D25_4C88_A690_0C1A4D7A5E7D__INCLUDED_)
#define AFX_VALVEPROPDLG_H__386719DD_8D25_4C88_A690_0C1A4D7A5E7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ValvePropDlg.h : header file
//

#include "Database.h"
#include "OdbcDatabase.h"
#include "resource.h"
#include "List.h"
#include "CopyArray.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CValvePropDlg dialog

class CValvePropDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CValvePropDlg (FoxjetDatabase::COdbcDatabase & db,
		ULONG lLineID,
		UNITS units,
		const FoxjetDatabase::HEADSTRUCT & head, 
		const FoxjetDatabase::VALVESTRUCT & valve, CWnd* pParent = NULL);   // standard constructor

	static void GetPanelValves (FoxjetDatabase::COdbcDatabase & db, FoxjetDatabase::HEADSTRUCT & card, ULONG lPanelID);
	static void GetValves(FoxjetDatabase::HEADSTRUCT & head, CArray <FoxjetDatabase::VALVESTRUCT, FoxjetDatabase::VALVESTRUCT &> & v, bool bLinked);
	static FoxjetDatabase::VALVESTRUCT GetRoot (const FoxjetDatabase::HEADSTRUCT & head, const FoxjetDatabase::VALVESTRUCT & valve);
	static void GetList (const FoxjetDatabase::HEADSTRUCT & head, const FoxjetDatabase::VALVESTRUCT & valve, CArray <FoxjetDatabase::VALVESTRUCT, FoxjetDatabase::VALVESTRUCT &> & v);
	static bool IsEndOfList (const FoxjetDatabase::HEADSTRUCT & head, const FoxjetDatabase::VALVESTRUCT & valve);
	static int GetLinkedTo (const FoxjetDatabase::HEADSTRUCT & head, const FoxjetDatabase::VALVESTRUCT & valve);
	static int FindIndex (FoxjetDatabase::HEADSTRUCT & head, int nIndex);

// Dialog Data
	//{{AFX_DATA(CValvePropDlg)
	enum { IDD = IDD_VALVE_PROPERTIES };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	FoxjetDatabase::VALVESTRUCT m_valve;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CValvePropDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	ULONG GetCurHeadID ();

	void FillCards();
	void FillTypes();
	void FillPanels();
	void FillIndex();
	void FillLinks();

	virtual void OnOK();

	FoxjetDatabase::HEADSTRUCT m_head;
	FoxjetDatabase::COdbcDatabase & m_db;
	const ULONG m_lLineID;
	UNITS m_units;

	CBitmap m_bmpRTOL;
	CBitmap m_bmpLTOR;

	// Generated message map functions
	//{{AFX_MSG(CValvePropDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDirection();
	afx_msg void OnSelchangeLinked();
	afx_msg void OnSelchangeCard();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VALVEPROPDLG_H__386719DD_8D25_4C88_A690_0C1A4D7A5E7D__INCLUDED_)
