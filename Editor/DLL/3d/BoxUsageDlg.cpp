// BoxUsageDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "Resource.h"
#include "BoxUsageDlg.h"
#include "ItiLibrary.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Serialize.h"
#include "Debug.h"
#include "FileExt.h"
#include "Extern.h"
#include "Coord.h"

using namespace ListGlobals;
using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace BoxUsageDlg;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BoxUsageDlg::CBoxItem::CBoxItem (UNITS units)
:	m_units (units)
{
}

BoxUsageDlg::CBoxItem::CBoxItem (const FoxjetDatabase::BOXSTRUCT & rhs, UNITS units)
:	m_box (rhs),
	m_units (units)
{
}

BoxUsageDlg::CBoxItem::~CBoxItem ()
{
}

int BoxUsageDlg::CBoxItem::Compare (const ItiLibrary::ListCtrlImp::CItem & cmp, int nColumn) const
{
	const CBoxItem & rhs = (CBoxItem &)cmp;

	switch (nColumn) {
	case 0:		return m_box.m_strName.Compare (rhs.m_box.m_strName);
	case 1:		return m_box.m_lLength - rhs.m_box.m_lLength;
	case 2:		return m_box.m_lWidth - rhs.m_box.m_lWidth;
	case 3:		return m_box.m_lHeight - rhs.m_box.m_lHeight;
	}

	return 0;
}

CString BoxUsageDlg::CBoxItem::GetDispText (int nColumn) const
{
	const FoxjetDatabase::HEADSTRUCT & head = GetDefaultHead ();
	CString str;

	switch (nColumn) {
	case 0:		
		str = m_box.m_strName;
		break;
	case 1:		
		{
			CCoord c (CBaseElement::ThousandthsToLogical (
				CPoint (m_box.m_lLength, 0), head), m_units, head);
			str = c.FormatX ();
			break;
		}
	case 2:
		{
			CCoord c (CBaseElement::ThousandthsToLogical (
				CPoint (m_box.m_lWidth, 0), head), m_units, head);
			str = c.FormatX ();
			break;
		}
	case 3:
		{
			CCoord c (CBaseElement::ThousandthsToLogical (
				CPoint (m_box.m_lHeight, 0), head), m_units, head);
			str = c.FormatX ();
			break;
		}
	}

	return str;
}

/////////////////////////////////////////////////////////////////////////////
// CBoxUsageDlg dialog


CBoxUsageDlg::CBoxUsageDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent)
:	m_db (db),
	m_units (INCHES),
	m_lLineID (-1),
	FoxjetCommon::CEliteDlg(IDD_BOXUSAGE, pParent)
{
	//{{AFX_DATA_INIT(CBoxUsageDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CBoxUsageDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBoxUsageDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

}


BEGIN_MESSAGE_MAP(CBoxUsageDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CBoxUsageDlg)
	ON_BN_CLICKED(BTN_UNUSE, OnUnuse)
	ON_BN_CLICKED(BTN_UNUSEALL, OnUnuseall)
	ON_BN_CLICKED(BTN_USE, OnUse)
	ON_BN_CLICKED(BTN_USEALL, OnUseall)
	ON_CBN_SELCHANGE(CB_LINE, OnSelchangeLine)
	ON_NOTIFY(NM_DBLCLK, LV_INUSE, OnDblclkInuse)
	ON_NOTIFY(NM_DBLCLK, LV_AVAILIBLE, OnDblclkAvailible)
	ON_BN_CLICKED (IDHELP, OnHelp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CBoxUsageDlg::OnHelp ()
{
	CEliteDlg::OnHelp (this);
}

BOOL CBoxUsageDlg::OnInitDialog() 
{
	using namespace ItiLibrary;

	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_NAME), 100));

	FoxjetCommon::CEliteDlg::OnInitDialog();
	m_lviAvail.Create (LV_AVAILIBLE, vCols, this, defElements.m_strRegSection);	
	m_lviInuse.Create (LV_INUSE, vCols, this, defElements.m_strRegSection);	

	GetBoxRecords (m_db, -1, m_vBoxes);
	InitLineCtrl ();	
	InitBoxCtrls (GetSelectedLine ());

	BEGIN_TRANS (m_db);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CBoxUsageDlg::InitLineCtrl()
{
	ASSERT (GetDlgItem (CB_LINE));
	CComboBox & cb = * (CComboBox *)GetDlgItem (CB_LINE);
	CArray <LINESTRUCT, LINESTRUCT &> vLines;

	GetLineRecords (vLines);

	int nIndex = cb.AddString (LoadString (IDS_ALLLINES));
	cb.SetItemData (nIndex, -1);

	for (int i = 0; i < vLines.GetSize (); i++) {
		const LINESTRUCT & line = vLines [i];

		int nIndex = cb.AddString (line.m_strName);
		cb.SetItemData (nIndex, line.m_lID);

		if (m_lLineID == line.m_lID)
			cb.SetCurSel (nIndex);
	}

	if (cb.GetCurSel () == CB_ERR)
		cb.SetCurSel (0);
}

ULONG CBoxUsageDlg::GetSelectedLine() const
{
	ASSERT (GetDlgItem (CB_LINE));
	CComboBox & cb = * (CComboBox *)GetDlgItem (CB_LINE);
	int nIndex = cb.GetCurSel ();

	if (nIndex != CB_ERR)
		return cb.GetItemData (nIndex);

	return -1;
}

void CBoxUsageDlg::InitBoxCtrls(ULONG lLineID)
{
	CArray <BOXLINKSTRUCT, BOXLINKSTRUCT &> vLinks;

	COdbcRecordset rst (m_db);
	CString str;

	try {
		if (lLineID != -1) {
			str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"),
				BoxToLine::m_lpszTable, 
				BoxToLine::m_lpszLineID, 
				lLineID);
		}
		else {
			CArray <LINESTRUCT, LINESTRUCT &> vLines;

			GetPrinterLines (m_db, GetPrinterID (m_db), vLines); //GetLineRecords (m_db, vLines);

			str.Format (_T ("SELECT * FROM [%s] WHERE"),
				BoxToLine::m_lpszTable);

			for (int i = 0; i < vLines.GetSize (); i++) {
				const LINESTRUCT & line = vLines [i];
				CString strTmp;

				strTmp.Format (_T (" [%s]=%u%s"),
					BoxToLine::m_lpszLineID, 
					line.m_lID,
					(i == (vLines.GetSize () - 1)) ? _T (";") : _T (" OR"));
				str += strTmp;
			}
		}

		rst.Open (str);

		while (!rst.IsEOF ()) {
			BOXLINKSTRUCT l;
			rst >> l;
			vLinks.Add (l);
			rst.MoveNext ();
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	m_lviAvail.DeleteCtrlData ();
	m_lviInuse.DeleteCtrlData ();

	for (int i = 0; i < m_vBoxes.GetSize (); i++) {
		BOXSTRUCT box = m_vBoxes [i];
		CBoxItem * pBox = new CBoxItem (box, m_units);
		bool bInuse = false;
		
		for (int j = 0; !bInuse && j < vLinks.GetSize (); j++) {
			const BOXLINKSTRUCT & link = vLinks [j];
		
			if (lLineID != -1)
				bInuse = 
					link.m_lLineID == lLineID && 
					link.m_lBoxID == box.m_lID;
			else
				bInuse = 
					link.m_lBoxID == box.m_lID;
		}

		if (bInuse) {
			int nIndex = m_lviInuse.GetListCtrl ().GetItemCount ();
			m_lviInuse.InsertCtrlData (pBox, nIndex);
		}
		else {
			int nIndex = m_lviAvail.GetListCtrl ().GetItemCount ();
			m_lviAvail.InsertCtrlData (pBox, nIndex);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CBoxUsageDlg message handlers

void CBoxUsageDlg::OnUnuse() 
{
	CLongArray sel = ItiLibrary::GetSelectedItems (m_lviInuse.GetListCtrl ());

	if (sel.GetSize ()) 
		SetAvailible (sel);
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}


void CBoxUsageDlg::OnUnuseall() 
{
	CLongArray sel;

	for (int i = 0; i < m_lviInuse.GetListCtrl ().GetItemCount (); i++) 
		sel.Add (i);

	SetAvailible (sel);
}

void CBoxUsageDlg::OnUse() 
{
	CLongArray sel = ItiLibrary::GetSelectedItems (m_lviAvail.GetListCtrl ());

	if (sel.GetSize ()) 
		SetUsed (sel);
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CBoxUsageDlg::OnUseall() 
{
	CLongArray sel;

	for (int i = 0; i < m_lviAvail.GetListCtrl ().GetItemCount (); i++) 
		sel.Add (i);

	SetUsed (sel);
}

void CBoxUsageDlg::OnSelchangeLine() 
{
	InitBoxCtrls (GetSelectedLine ());
}

void CBoxUsageDlg::OnDblclkInuse(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if (GetSelectedLine () != -1)
		OnUnuse ();

	if (pResult)
		* pResult = 0;
}

void CBoxUsageDlg::OnDblclkAvailible(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if (GetSelectedLine () != -1)
		OnUse ();

	if (pResult)
		* pResult = 0;
}

void CBoxUsageDlg::SetAvailible (const CLongArray &sel)
{
	if (sel.GetSize ()) {
		ULONG lLineID = GetSelectedLine ();

		try {
			for (int i = 0; i < sel.GetSize (); i++) {
				CBoxItem * p = (CBoxItem *)m_lviInuse.GetCtrlData (sel [i]);
				CString str;
				ULONG lBoxID = p->m_box.m_lID;
				CArray <BOXUSAGESTRUCT, BOXUSAGESTRUCT &> vInUse;

				// make sure this box is not in use!
				GetBoxUsage (m_db, lBoxID, lLineID, vInUse);

				if (vInUse.GetSize ()) {
					int nMax = 10;

					CString str = LoadString (IDS_BOXUSEDBYLINE);

					for (int i = 0; i < min (vInUse.GetSize (), nMax); i++) 
						str += 
							vInUse [i].m_line.m_strName + 
							_T ("\\") + 
							vInUse [i].m_task.m_strName + 
							_T (".") + 
							FoxjetFile::GetFileExt (FoxjetFile::TASK) +
							_T ("\n");

					if (vInUse.GetSize () > nMax)
						str += _T (".\n.\n.");

					MsgBox (* this, str);
					return;
				}

				VERIFY (SetUsed (m_db, p->m_box.m_lID, lLineID, false));
			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		InitBoxCtrls (lLineID);
	}
}

void CBoxUsageDlg::SetUsed (const CLongArray &sel)
{
	if (sel.GetSize ()) {
		ULONG lLineID = GetSelectedLine ();

		for (int i = 0; i < sel.GetSize (); i++) {
			CBoxItem * p = (CBoxItem *)m_lviAvail.GetCtrlData (sel [i]);

			VERIFY (SetUsed (m_db, p->m_box.m_lID, lLineID, true));
		}

		InitBoxCtrls (lLineID);
	}
}


LRESULT CBoxUsageDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if (::IsWindow (m_hWnd) && ::IsWindowVisible (m_hWnd))
		UpdateUI ();

	return FoxjetCommon::CEliteDlg::WindowProc(message, wParam, lParam);
}

void CBoxUsageDlg::UpdateUI()
{
	using namespace ItiLibrary;

	CWnd * pUse			= GetDlgItem (BTN_USE);
	CWnd * pUseAll		= GetDlgItem (BTN_USEALL);
	CWnd * pUnuse		= GetDlgItem (BTN_UNUSE);
	CWnd * pUnuseAll	= GetDlgItem (BTN_UNUSEALL);
	ULONG lLineID		= GetSelectedLine ();
	CLongArray inuse	= GetSelectedItems (m_lviInuse.GetListCtrl ());
	CLongArray avail	= GetSelectedItems (m_lviAvail.GetListCtrl ());
	int nInuse			= m_lviInuse.GetListCtrl ().GetItemCount ();
	int nAvail			= m_lviAvail.GetListCtrl ().GetItemCount ();

	ASSERT (pUse);
	ASSERT (pUseAll);
	ASSERT (pUnuse);
	ASSERT (pUnuseAll);

	pUse->EnableWindow		(lLineID != -1 && avail.GetSize ());
	pUseAll->EnableWindow	(lLineID != -1 && nAvail);
	pUnuse->EnableWindow	(lLineID != -1 && inuse.GetSize ());
	pUnuseAll->EnableWindow (lLineID != -1 && nInuse);
}


void CBoxUsageDlg::OnOK()
{
	m_lLineID = GetSelectedLine ();
	COMMIT_TRANS (m_db);
	FoxjetCommon::CEliteDlg::OnOK ();
}

void CBoxUsageDlg::OnCancel()
{
	ROLLBACK_TRANS (m_db);
	FoxjetCommon::CEliteDlg::OnCancel ();
}

bool CBoxUsageDlg::IsUsed (FoxjetDatabase::COdbcDatabase & db, ULONG lBoxID, ULONG lLineID)
{
	bool bResult = false;

	try { 
		COdbcRecordset rst (db);
		CString str;
		BOXLINKSTRUCT link;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%d AND [%s]=%d;"), 
			BoxToLine::m_lpszTable,
			BoxToLine::m_lpszLineID, lLineID,
			BoxToLine::m_lpszBoxID, lBoxID);

		rst.Open (str, rst.dynaset);
		bResult = !rst.IsEOF ();
		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

bool CBoxUsageDlg::SetUsed (FoxjetDatabase::COdbcDatabase & db, ULONG lBoxID, ULONG lLineID, bool bUsed)
{
	bool bResult = false;

	try { 
		if (bUsed) {
			COdbcRecordset rst (db);
			CString str;
			BOXLINKSTRUCT link;

			link.m_lBoxID = lBoxID;
			link.m_lLineID = lLineID;

			rst << link;

			bResult = true;
		}
		else { 
			CString str;

			str.Format (
				_T ("DELETE * FROM [%s] WHERE [%s]=%u AND [%s]=%u;"),
				BoxToLine::m_lpszTable,
				BoxToLine::m_lpszBoxID, lBoxID,
				BoxToLine::m_lpszLineID, lLineID);
			db.ExecuteSQL (str);
			bResult = true;
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}
