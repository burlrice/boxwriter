// StartTaskDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "Control.h"
#include "StartTaskDlg.h"
#include "Database.h"
#include "ListCtrlImp.h"
#include "TemplExt.h"
#include "AppVer.h"
#include "Registry.h"
#include "FieldDefs.h"
#include "Parse.h"
#include "Color.h"
#include "OdbcRecordset.h"
#include "Debug.h"
#include "Extern.h"

using namespace Foxjet3d;
using namespace ItiLibrary;
using namespace FoxjetCommon;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStartTaskDlg dialog

static bool bUsePHC [6] = { 0 };

bool Foxjet3d::GetTestPHC (ULONG lAddress)
{
	switch (lAddress) {
	case 0x300:		return ::bUsePHC [0];
	case 0x310:		return ::bUsePHC [1];
	case 0x320:		return ::bUsePHC [2];
	case 0x330:		return ::bUsePHC [3];
	case 0x340:		return ::bUsePHC [4];
	case 0x350:		return ::bUsePHC [5];
	}

	return false;
}

void Foxjet3d::SetTestPHC (ULONG lAddress, bool bUse)
{
	switch (lAddress) {
	case 0x300:		::bUsePHC [0] = bUse;		break;
	case 0x310:		::bUsePHC [1] = bUse;		break;
	case 0x320:		::bUsePHC [2] = bUse;		break;
	case 0x330:		::bUsePHC [3] = bUse;		break;
	case 0x340:		::bUsePHC [4] = bUse;		break;
	case 0x350:		::bUsePHC [5] = bUse;		break;
	}
}


CStartTaskDlg::CStartTaskDlg(CWnd* pParent /*=NULL*/)
:	m_lOldTaskID (-1),
	m_bTyping (false),
	m_bContinuousCount (false),
	FoxjetCommon::CEliteDlg (IDD_START_TASK_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CStartTaskDlg)
	m_bResetCounts = FALSE;
	//}}AFX_DATA_INIT
}


void CStartTaskDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStartTaskDlg)
	DDX_Control(pDX, IDC_TASKLIST, m_TaskList);
	DDX_Check(pDX, CHK_RESETCOUNTS, m_bResetCounts);
	//}}AFX_DATA_MAP

	//if (IsMatrix ()) 
	{
		m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
		m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
		m_buttons.DoDataExchange (pDX, BTN_LINE);
		m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);
	}
}

BEGIN_MESSAGE_MAP(CStartTaskDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CStartTaskDlg)
	ON_NOTIFY(NM_DBLCLK, IDC_TASKLIST, OnDblclkTasklist)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_TASKLIST, OnItemchangedTasklist)
	ON_NOTIFY(NM_CLICK, IDC_TASKLIST, OnClickTasklist)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(TXT_TASK, OnChangeTask)
	ON_BN_CLICKED(BTN_LINE, OnSelectLine)
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
	ON_COMMAND_RANGE(IDM_SELECT_LINE, IDM_SELECT_LINE+10, OnSelectLineRange) 
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStartTaskDlg message handlers

void CStartTaskDlg::FillCtrl ()
{
	using namespace FoxjetDatabase;

	DECLARETRACECOUNT (200);
	
	static bool bPrinterField = false;
	CString strKey = m_strRegKey + _T ("\\") + _T ("Settings\\CStartTaskDlg");
	ULONG lLastTaskID = FoxjetDatabase::GetProfileInt (strKey, _T ("m_lTaskID"), -1);

	{
		static bool bChecked = false;

		if (!bChecked) {
			try {
				using namespace FoxjetDatabase;
				using namespace FoxjetDatabase::Tasks;

				COdbcRecordset rst (ListGlobals::GetDB ());
				CString str;

				str.Format (_T ("SELECT * FROM [%s];"), Lines::m_lpszTable);
				rst.Open (str, CRecordset::snapshot, CRecordset::readOnly | CRecordset::executeDirect | CRecordset::noDirtyFieldCheck);

				bPrinterField = rst.GetFieldIndex (Lines::m_lpszPrinterID) != -1;
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
		}
	}

	if ( !m_sLineName.IsEmpty() )
	{
		ULONG lPrinterID = GetMasterPrinterID (ListGlobals::GetDB ()); //GetPrinterID (ListGlobals::GetDB ());
		ULONG lLineID = GetLineID (ListGlobals::GetDB (), m_sLineName, lPrinterID);
		
		m_lOldTaskID = GetTaskID (ListGlobals::GetDB (), m_strTaskName, m_sLineName, lPrinterID);

		MARKTRACECOUNT ();

		{
			m_TaskList.SetRedraw (FALSE);
			m_TaskList.DeleteAllItems ();

			try {
				using namespace FoxjetDatabase;
				using namespace FoxjetDatabase::Tasks;

				COdbcRecordset rst (ListGlobals::GetDB ());
				CString str;
				int i = 0;
				
				/*
				str.Format (
					_T ("SELECT [%s], [%s], [%s] FROM [%s] WHERE [%s]=%d ORDER BY [%s].[%s];"),
					Tasks::m_lpszName,
					Tasks::m_lpszDesc,
					Tasks::m_lpszID,
					Tasks::m_lpszTable,
					Tasks::m_lpszLineID,
					lLineID,
					Tasks::m_lpszTable, 
					Tasks::m_lpszName);
				*/
				if (bPrinterField) 
					str.Format (
						_T ("SELECT [%s].[%s], [%s].[%s], [%s].[%s] FROM [%s] INNER JOIN [%s] ON [%s].[%s] = [%s].[%s]")
						_T (" WHERE [%s].[%s]=%d AND [%s].[%s]=%d ORDER BY [%s].[%s];"),
						Tasks::m_lpszTable, Tasks::m_lpszName,
						Tasks::m_lpszTable, Tasks::m_lpszDesc,
						Tasks::m_lpszTable, Tasks::m_lpszID,
						Lines::m_lpszTable,
						Tasks::m_lpszTable,
						Lines::m_lpszTable, Lines::m_lpszID,
						Tasks::m_lpszTable, Tasks::m_lpszLineID,
						Lines::m_lpszTable, Lines::m_lpszPrinterID,
						GetMasterPrinterID (ListGlobals::GetDB ()), // sw0867
						Lines::m_lpszTable, Lines::m_lpszID, lLineID,
						Tasks::m_lpszTable, Tasks::m_lpszName);
				else
					str.Format (
						_T ("SELECT [%s].[%s], [%s].[%s], [%s].[%s] FROM [%s] INNER JOIN [%s] ON [%s].[%s] = [%s].[%s]")
						_T (" WHERE [%s].[%s]=%d")
						_T (" ORDER BY [%s].[%s];"),
						Tasks::m_lpszTable, Tasks::m_lpszName,
						Tasks::m_lpszTable, Tasks::m_lpszDesc,
						Tasks::m_lpszTable, Tasks::m_lpszID,
						Lines::m_lpszTable,
						Tasks::m_lpszTable,
						Lines::m_lpszTable, Lines::m_lpszID,
						Tasks::m_lpszTable, Tasks::m_lpszLineID,
						//Lines::m_lpszTable, Lines::m_lpszPrinterID,
						//GetMasterPrinterID (ListGlobals::GetDB ()), // sw0867
						Lines::m_lpszTable, Lines::m_lpszID, lLineID,
						Tasks::m_lpszTable, Tasks::m_lpszName);
				TRACEF (str);

				rst.Open (str, CRecordset::snapshot, CRecordset::readOnly | CRecordset::executeDirect | CRecordset::noDirtyFieldCheck);

				while (!rst.IsEOF ()) {
					int nIndex = m_TaskList.InsertItem (i, (CString)rst.GetFieldValue ((short)0), 0);

					m_TaskList.SetItemText (nIndex, 1, (CString)rst.GetFieldValue ((short)1));
					m_TaskList.SetItemText (nIndex, 2, FoxjetDatabase::ToString ((long)rst.GetFieldValue ((short)2)));

					rst.MoveNext ();
				}
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

			MARKTRACECOUNT ();

			{ 
				bool bSelected = false;

				for (int i = 0; i < m_TaskList.GetItemCount (); i++) {
					if (!m_TaskList.GetItemText (i, 0).CompareNoCase (m_strTaskName)) {
						ItiLibrary::Select (m_TaskList, i);
						bSelected = true;
						break;
					}
				}

				if (!bSelected) {
					for (int i = 0; i < m_TaskList.GetItemCount (); i++) {
						ULONG lID = _tcstoul (m_TaskList.GetItemText (i, 2), NULL, 10);
						
						if (lID == lLastTaskID) {
							ItiLibrary::Select (m_TaskList, i);
							bSelected = true;
							break;
						}
					}
				}

				if (!bSelected) 
					ItiLibrary::Select (m_TaskList, 0);
			}

			m_TaskList.SetRedraw (TRUE);
		}
	}

	MARKTRACECOUNT ();
	//TRACECOUNTARRAYMAX ();
}

BOOL CStartTaskDlg::OnInitDialog() 
{
	using namespace FoxjetDatabase;
	DECLARETRACECOUNT (200);

	m_lTaskID = -1;
	FoxjetCommon::CEliteDlg::OnInitDialog();

	m_TaskList.SetExtendedStyle (m_TaskList.GetExtendedStyle () | LVS_EX_FULLROWSELECT);
	m_TaskList.InsertColumn (0, LoadString (IDS_NAME), LVCFMT_LEFT, 400);
	m_TaskList.InsertColumn (1, LoadString (IDS_DESCRIPTION), LVCFMT_LEFT, 400);
	m_TaskList.InsertColumn (2, LoadString (IDS_ID), LVCFMT_LEFT, 0);

	if (CFont * p = GetFont ()) {
		LOGFONT lf;

		p->GetLogFont (&lf);
		lf.lfHeight = (int)((double)lf.lfHeight * 1.5);
		m_fnt.CreateFontIndirect (&lf);
		m_TaskList.SetFont (&m_fnt);
	}


	if (CWnd * p = GetDlgItem (BTN_LINE)) {
		GetPrinterLines (ListGlobals::GetDB (), GetPrinterID (ListGlobals::GetDB ()), m_vLines); 
		p->ShowWindow (m_vLines.GetSize () > 1 ? SW_SHOW : SW_HIDE);
	}

	GetWindowText (m_strTitle);
	m_strOriginalLine = m_sLineName;
	FillCtrl ();

	MARKTRACECOUNT ();
	//TRACECOUNTARRAYMAX ();

	LoadSettings (m_TaskList, _T ("Software\\FoxJet\\Control"), _T ("CStartTaskDlg"));
	m_TaskList.SetColumnWidth (2, 0);

	{
		UINT nID [] =
		{ 
			CHK_HEAD1,
			CHK_HEAD2,
			CHK_HEAD3,
			CHK_HEAD4,
			CHK_HEAD5,
			CHK_HEAD6,
		};

		for (int i = 0; i < ARRAYSIZE (nID); i++) {
			if (CButton * p = (CButton *)GetDlgItem (nID [i])) {
				bool bShow = false;

				#ifdef __WINPRINTER__
				bShow = FoxjetCommon::IsProductionConfig ();
				#endif

				if (bShow) { 
					CString str;

					str.Format (_T ("%d"), i);
					p->SetCheck (FoxjetDatabase::GetProfileInt (m_strRegKey + "\\Settings\\Test", str, 0));
				}
				else
					p->ShowWindow (SW_HIDE);
			}
		}
	}

	if (m_bContinuousCount) {
		m_bResetCounts = false; 

		if (CButton* p = (CButton*)GetDlgItem(CHK_RESETCOUNTS)) {
			p->EnableWindow(FALSE);
			p->SetCheck(0);
		}
	}

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CStartTaskDlg::OnCancel() 
{
	DeleteObjects();
	FoxjetCommon::CEliteDlg::OnCancel();
}

void CStartTaskDlg::OnOK() 
{
	using namespace FoxjetDatabase;

	CString strTask;

	UpdateData (TRUE);
	GetDlgItemText (TXT_TASK, strTask);

	{
		int nIndex = strTask.Find (_T ("\\"));

		if (nIndex != -1)
			strTask.Delete (0, nIndex + 1);
	}

	for (int i = 0; i < m_TaskList.GetItemCount (); i++) {
		CString strName = m_TaskList.GetItemText (i, 0);
		CString strDesc = m_TaskList.GetItemText (i, 1);
		ULONG lID = _tcstoul (m_TaskList.GetItemText (i, 2), NULL, 10);

		if (!strTask.CompareNoCase (strName)) {
			CString strKey = m_strRegKey + _T ("\\Settings\\CStartTaskDlg");
			m_lTaskID		= lID;
			m_strTaskName	= strName;

			FoxjetDatabase::WriteProfileInt (strKey, _T ("m_lTaskID"), m_lTaskID);
			FoxjetDatabase::WriteProfileInt (strKey, _T ("m_bResetCounts"), m_bResetCounts);
			
			DeleteObjects();
			FoxjetCommon::CEliteDlg::OnOK();

			#ifdef __WINPRINTER__
			if (FoxjetCommon::IsProductionConfig ()) {
				struct
				{
					UINT m_nID;
					ULONG m_lAddr;
				} static const map [] =
				{ 
					{ CHK_HEAD1, 0x300 },
					{ CHK_HEAD2, 0x310 },
					{ CHK_HEAD3, 0x320 },
					{ CHK_HEAD4, 0x330 },
					{ CHK_HEAD5, 0x340 },
					{ CHK_HEAD6, 0x350 },
				};

				for (int i = 0; i < ARRAYSIZE (map); i++) {
					if (CButton * p = (CButton *)GetDlgItem (map [i].m_nID)) {
						bool bUse = p->GetCheck () == 1 ? true : false;
						CString str;

						str.Format (_T ("%d"), i);
						SetTestPHC (map [i].m_lAddr, bUse);
						FoxjetDatabase::WriteProfileInt (m_strRegKey + _T ("\\Settings\\Test"), str, bUse);
					}
				}
			}
			#endif //__WINPRINTER__

			return;
		}
	}

	MsgBox (* this, LoadString (IDS_NO_TASK_SELECTED));
}

void CStartTaskDlg::DeleteObjects()
{
}


void CStartTaskDlg::OnClickTasklist(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CLongArray sel = GetSelectedItems (m_TaskList);

	if (sel.GetSize () == 1) 
		SetDlgItemText (TXT_TASK, m_TaskList.GetItemText (sel [0], 0));

	if (pResult)
		* pResult = 0;
}

void CStartTaskDlg::OnDblclkTasklist(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NMLISTVIEW * pInfo = (NMLISTVIEW *)pNMHDR;

	if ( pInfo->iItem != -1 ) {
		CString strName = m_TaskList.GetItemText (pInfo->iItem, 0);
		ULONG lID = _tcstoul (m_TaskList.GetItemText (pInfo->iItem, 2), NULL, 10);

		m_lTaskID = lID;
		m_strTaskName = strName;

		OnOK();
	}

	* pResult = 0;
}

BOOL CStartTaskDlg::DestroyWindow() 
{
	SaveSettings (m_TaskList, 2, _T ("Software\\FoxJet\\Control"), _T ("CStartTaskDlg"));
	
	return FoxjetCommon::CEliteDlg::DestroyWindow();
}

void CStartTaskDlg::OnItemchangedTasklist(NMHDR* pNMHDR, LRESULT* pResult) 
{
	using namespace FoxjetDatabase;

	m_bTyping = false;
	CLongArray sel = GetSelectedItems (m_TaskList);
	bool bReset = true;
	CButton * pReset = (CButton *)GetDlgItem (CHK_RESETCOUNTS);

	ASSERT (pReset);

	if (sel.GetSize () == 1) {
		CString strName = m_TaskList.GetItemText (sel [0], 0);
		ULONG lID = _tcstoul (m_TaskList.GetItemText (sel [0], 2), NULL, 10);

		if (m_sLineName.CompareNoCase (m_strOriginalLine) != 0)
			strName = m_sLineName + _T ("\\") + strName;

		SetDlgItemText (TXT_TASK, strName);
		bReset = m_lOldTaskID != lID;
	}

	if (m_bContinuousCount)
		bReset = false;

	pReset->SetCheck (bReset ? 1 : 0);
		
	if (pResult)
		* pResult = 0;

	m_bTyping = true;
}

void CStartTaskDlg::OnChangeTask ()
{
	if (m_bTyping) {
		CString str;
		bool bFound = false;

		GetDlgItemText (TXT_TASK, str);

		for (int i = 0; !bFound && i < m_TaskList.GetItemCount (); i++) {
			CString strTmp = m_TaskList.GetItemText (i, 0);

			if (!_tcsncicmp (str, strTmp, str.GetLength ())) {
				ItiLibrary::Select (m_TaskList, i);
				SetDlgItemText (TXT_TASK, str);

				if (CEdit * p = (CEdit *)GetDlgItem (TXT_TASK)) 
					p->SetSel (str.GetLength (), str.GetLength ());

				bFound = true;
			}
		}

		if (!bFound)
			ItiLibrary::SelectAll (m_TaskList, false);
	}
}

void CStartTaskDlg::OnSelectLine ()
{
	if (CWnd * p = GetDlgItem (BTN_LINE)) {
		CMenu menu;
		CRect rc;

		p->GetWindowRect (rc);
		menu.CreatePopupMenu ();

		for (int i = 0; i < m_vLines.GetSize (); i++) {
			MENUITEMINFO info;

			memset (&info, 0, sizeof (info));

			info.cbSize		= sizeof (info);
			info.fMask		= MIIM_TYPE | MIIM_ID | MIIM_DATA;
			info.fType		= MFT_OWNERDRAW;
			info.wID		= IDM_SELECT_LINE + i;//m_vLines [i].m_lID;

			::InsertMenuItem (menu, i, TRUE, &info);
		}

		menu.TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON, rc.left, rc.top, this);
	}
}

void CStartTaskDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	using namespace Color;
	using namespace FoxjetCommon;

	DRAWITEMSTRUCT dis;
	
	memcpy (&dis, lpDrawItemStruct, sizeof (dis));

	if (dis.CtlType == ODT_MENU) {
		bool bSelected = (dis.itemState & (ODS_SELECTED | ODS_FOCUS)) ? false : true;
		CString str;
		CRect rc = dis.rcItem;
		CDC dc;

		int nIndex = dis.itemID - IDM_SELECT_LINE;

		if (nIndex >= 0 && nIndex < m_vLines.GetSize ())
			str = m_vLines [nIndex].m_strName;

		dc.Attach (dis.hDC);
		COLORREF rgbSetTextColor = dc.SetTextColor (bSelected ? Color::rgbCOLOR_WINDOWTEXT : Color::rgbCOLOR_HIGHLIGHTTEXT);
		int nSetBkMode = dc.SetBkMode (TRANSPARENT);
		dc.FillRect (rc, &CBrush (bSelected ? Color::rgbCOLOR_MENU : Color::rgbCOLOR_HIGHLIGHT));
		dc.DrawEdge (rc, EDGE_SUNKEN, BF_LEFT | BF_RIGHT | BF_TOP | BF_BOTTOM | BF_ADJUST);

		dc.DrawText (str, rc, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_EXPANDTABS);
		dc.SetTextColor (rgbSetTextColor);
		dc.SetBkMode (nSetBkMode);
		dc.Detach ();
	}
	else 
		FoxjetCommon::CEliteDlg::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CStartTaskDlg::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpmis) 
{	
	//TRACEF (_T ("CControlView::OnMeasureItem: ") + FoxjetDatabase::ToString ((int)lpmis->CtlID));

	if (lpmis->CtlType == ODT_MENU) {
		lpmis->itemWidth = 200;
		lpmis->itemHeight = 50;
	}

	FoxjetCommon::CEliteDlg::OnMeasureItem (nIDCtl, lpmis);
}

void CStartTaskDlg::OnSelectLineRange (UINT nID)
{
	int nIndex = nID - IDM_SELECT_LINE;

	if (nIndex >= 0 && nIndex < m_vLines.GetSize ()) {
		CString strTitle = m_strTitle;

		m_sLineName = m_vLines [nIndex].m_strName;
		FillCtrl ();

		if (m_sLineName.CompareNoCase (m_strOriginalLine) != 0)
			strTitle += _T (": ") + m_sLineName;

		SetWindowText (strTitle);
	}
}
