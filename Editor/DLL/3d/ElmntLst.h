// EditorElementList.h: interface for the CEditorElementList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EDITORELEMENTLIST_H__692CEFC5_E689_42EC_AEE6_93571FDD07A9__INCLUDED_)
#define AFX_EDITORELEMENTLIST_H__692CEFC5_E689_42EC_AEE6_93571FDD07A9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "3dApi.h"
#include "element.h"
#include "ProdArea.h"
#include "CopyArray.h"
#include "BaseElement.h"
#include "List.h"
#include "Panel.h"
#include "HeadConfigDlg.h"


class _3D_API CEditorElementList : public FoxjetCommon::CElementList
{
public:
	CEditorElementList (const CProductArea & pa = CProductArea (), bool bAutoDelete = true);
	CEditorElementList (const CEditorElementList & rhs);
	CEditorElementList & operator = (const CEditorElementList & rhs);

	virtual ~CEditorElementList ();

	virtual FoxjetCommon::CBaseElement & GetObject (int nIndex);
	virtual const FoxjetCommon::CBaseElement & GetObject (int nIndex) const;

	virtual void DeleteAllElements ();

	int FromString (const CString & str, CProductArea & pa, 
		UNITS units, const FoxjetDatabase::HEADSTRUCT & head, FoxjetCommon::CVersion & ver, 
		bool bDeleteExisiting = true, ULONG lLocalParamUID = 0);
	CString ToString (const FoxjetCommon::CVersion & ver, 
		const CProductArea & pa, bool bBreakTokens = false) const;

	Element::CElement & operator [] (int nIndex);
	const Element::CElement & operator [] (int nIndex) const;
	
	Element::CElement & GetElement (int nIndex);
	const Element::CElement & GetElement (int nIndex) const;
	
	Element::CElement * GetElementPtr (int nIndex);
	const Element::CElement * GetElementPtr (int nIndex) const;

	Element::CPairArray GetOverlapping (const Element::CElement * pElement, const Foxjet3d::Panel::CBoundary & workingHead) const;

	Element::CElement * Find (const Element::CElement * p) const;

	Foxjet3d::Panel::CBoundary m_bounds;

public:
	int GetIndex (const Element::CElement * pElement) const;

	CPoint GetNextOpenPos (const CPoint & pt) const;

	int HitTest (const CPoint & pt, double dZoom, const Foxjet3d::Panel::CBoundary & workingHead) const;

	HBITMAP CreateBitmap (CDC & dc, const FoxjetCommon::CLongArray & vDraw,
		const CProductArea & pa, const Foxjet3d::Panel::CBoundary & workingHead) const;
	bool SetHead (ULONG lHeadID);
	bool SetHead (const Foxjet3d::Panel::CBoundary & head);

	const Element::CElement * GetObject (const FoxjetCommon::CBaseElement * p) const;

	static bool Contains (FoxjetCommon::CLongArray & v, int nIndex);

	bool SetProductArea (int cx, int cy, const CSize & sizePanel) { return SetProductArea (cx, cy, true, sizePanel); }
	bool SetProductArea (const CSize & pa, const CSize & sizePanel) { return SetProductArea (pa.cx, pa.cy, true, sizePanel); }

	CSize GetProductArea () const { return CElementList::GetProductArea (); }
	CProductArea GetProductArea (int) { return m_rcPA; }
	const CProductArea & GetProductArea (int) const { return m_rcPA; }
	bool SetProductArea (const CProductArea & pa, const CSize & sizePanel = CSize (0, 0));

	CRect GetBoundingRect (const Element::CElement & e, const Foxjet3d::Panel::CBoundary & workingHead) const;

	void SetAutoDelete (bool bDelete) { m_bAutoDelete = bDelete; }

protected:
	bool SetProductArea (int cx, int cy, bool bUpdateList, const CSize & sizePanel);

	CProductArea m_rcPA;
};

bool operator == (const CEditorElementList & lhs, const CEditorElementList & rhs);
bool operator != (const CEditorElementList & lhs, const CEditorElementList & rhs);

#endif // !defined(AFX_EDITORELEMENTLIST_H__692CEFC5_E689_42EC_AEE6_93571FDD07A9__INCLUDED_)
