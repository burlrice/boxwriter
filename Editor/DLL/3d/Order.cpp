
#include "stdafx.h"
#include "3d.h"
#include "Box.h"
#include "Debug.h"
#include "TemplExt.h"
#include "Coord.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace Foxjet3d::Box;

Box::CBox::COrder::COrder ()
:	m_nNumber (0),
	m_rot (ROT_0)
{
	for (int i = 0; i < ARRAYSIZE (m_pt); i++)
		m_pt [i] = CPoint (0);
}
 
Box::CBox::COrder::COrder (const DB::COrientation & face, const CPoint pt [8])
{
	m_nNumber = face.m_nPanel;
	m_rot = face.m_orient;
	SetPoints (1, pt);
}

Box::CBox::COrder::COrder (int nAbsolute, const DB::COrientation & face, const CPoint pt [8])
{
	DB::COrientation o = DB::MapPanel (nAbsolute, face);

	m_nNumber = o.m_nPanel;
	m_rot = o.m_orient;
	SetPoints (nAbsolute, pt);
}

Box::CBox::COrder::COrder (const Box::CBox::COrder & rhs)
:	m_nNumber (rhs.m_nNumber),
	m_rot (rhs.m_rot)
{
	for (int i = 0; i < 4; i++)
		m_pt [i] = rhs.m_pt [i];
}

Box::CBox::COrder & Box::CBox::COrder::operator = (const Box::CBox::COrder & rhs)
{
	if (this != &rhs) {
		m_nNumber	= rhs.m_nNumber;
		m_rot		= rhs.m_rot;
		
		for (int i = 0; i < 4; i++)
			m_pt [i] = rhs.m_pt [i];
	}

	return * this;
}

Box::CBox::COrder::~COrder ()
{
}

void Box::CBox::COrder::SetPoints (int nAbsolute, const CPoint pt [8])
{
	switch (nAbsolute) {
	case 1: 
		m_pt [0] = pt [0]; 
		m_pt [1] = pt [1]; 
		m_pt [2] = pt [2]; 
		m_pt [3] = pt [3]; 
		break;
	case 2: 
		m_pt [0] = pt [1]; 
		m_pt [1] = pt [5]; 
		m_pt [2] = pt [6]; 
		m_pt [3] = pt [2]; 
		break;
	case 3: 
		m_pt [0] = pt [5]; 
		m_pt [1] = pt [4]; 
		m_pt [2] = pt [7]; 
		m_pt [3] = pt [6]; 
		break;
	case 4: 
		m_pt [0] = pt [4]; 
		m_pt [1] = pt [0]; 
		m_pt [2] = pt [3]; 
		m_pt [3] = pt [7]; 
		break;
	case 5: 
		m_pt [0] = pt [4]; 
		m_pt [1] = pt [5]; 
		m_pt [2] = pt [1]; 
		m_pt [3] = pt [0]; 
		break;
	case 6: 
		m_pt [0] = pt [3]; 
		m_pt [1] = pt [2]; 
		m_pt [2] = pt [6]; 
		m_pt [3] = pt [7]; 
/*
		m_pt [0] = pt [6]; 
		m_pt [1] = pt [7]; 
		m_pt [2] = pt [3]; 
		m_pt [3] = pt [2]; 
*/
		break;
	}
}

