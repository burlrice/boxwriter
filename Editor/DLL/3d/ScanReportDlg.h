#if !defined(AFX_SCANREPORTDLG_H__4CFF1528_8F3B_4C0C_BF53_85DD3F2FACE1__INCLUDED_)
#define AFX_SCANREPORTDLG_H__4CFF1528_8F3B_4C0C_BF53_85DD3F2FACE1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScanReportDlg.h : header file
//

#include "ListCtrlImp.h"
#include "RoundButtons.h"
#include "3dApi.h"
#include "Database.h"
#include "Utils.h"

namespace Foxjet3d
{
	bool _3D_API IsScanReportLoggingEnabled (FoxjetDatabase::COdbcDatabase & db, ULONG lPrinterID);

	/////////////////////////////////////////////////////////////////////////////
	// CScanReportDlg dialog

	namespace ScanReportDlg
	{

		class _3D_API CReportItem : public ItiLibrary::ListCtrlImp::CItem
		{
		public:
			CReportItem (const CString & strLine, const FoxjetDatabase::BCSCANSTRUCT & s);
			virtual ~CReportItem ();

			virtual CString GetDispText (int nColumn) const;
			virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;

		protected:
			FoxjetDatabase::BCSCANSTRUCT m_rpt;
			CString m_strLine;
		};

		class _3D_API CScanReportDlg : public FoxjetCommon::CEliteDlg
		{
		public:
			CScanReportDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent = NULL);   // standard constructor

			static bool IsAutoExport (FoxjetDatabase::COdbcDatabase & db, ULONG lPrinterID);

			static const LPCTSTR m_lpszSettingsKey;
			static const LPCTSTR m_lpszSettingsKeyPath;

			static void Export (FoxjetDatabase::COdbcDatabase & db, const CString & strLine, ULONG lPrinterID);

		// Dialog Data
			//{{AFX_DATA(CScanReportDlg)
			CString	m_sLine;
			//}}AFX_DATA
			ULONG m_lPrinterID;
			bool m_bEnable;


		// Overrides
			// ClassWizard generated virtual function overrides
			//{{AFX_VIRTUAL(CScanReportDlg)
			public:
			protected:
			virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
			//}}AFX_VIRTUAL

		// Implementation
		protected:
			ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
			FoxjetUtils::CRoundButtons m_buttons;
			FoxjetDatabase::COdbcDatabase & m_db;

			virtual void OnOK ();

			// Generated message map functions
			//{{AFX_MSG(CScanReportDlg)
			virtual BOOL OnInitDialog();
			afx_msg void OnClear();
			afx_msg void OnExport();
			afx_msg void OnFileSettings();
			//}}AFX_MSG
			DECLARE_MESSAGE_MAP()
		};
	}; //namespace ScanReportDlg
}; 

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCANREPORTDLG_H__4CFF1528_8F3B_4C0C_BF53_85DD3F2FACE1__INCLUDED_)
