// Image.cpp: implementation of the CImage class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "3d.h"
#include "Image.h"
#include "Debug.h"
#include "Utils.h"
//#include "ximacfg.h"
#include "ximage.h"
#include "FileExt.h"
#include "AppVer.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace Foxjet3d;
using namespace FoxjetCommon;

CImage::CImage()
:	m_pBmp (NULL)
{
	m_lBoxID		= -1;
	m_nPanel		= -1;
	m_lAttributes	= FoxjetDatabase::STRETCH;
}

CImage::CImage(const FoxjetDatabase::IMAGESTRUCT & rhs)
:	m_pBmp (NULL)
{
	m_lBoxID		= rhs.m_lBoxID;
	m_nPanel		= rhs.m_nPanel;
	m_strImage		= rhs.m_strImage;
	m_lAttributes	= rhs.m_lAttributes;

	if (m_strImage.GetLength ())
		Load (m_strImage, m_nPanel, m_lBoxID);
}

CImage::CImage (const CImage & rhs)
:	m_pBmp (NULL)
{
	m_lBoxID		= rhs.m_lBoxID;
	m_nPanel		= rhs.m_nPanel;
	m_strImage		= rhs.m_strImage;
	m_lAttributes	= rhs.m_lAttributes;

	if (m_strImage.GetLength ())
		Load (m_strImage, m_nPanel, m_lBoxID);
}

CImage & CImage::operator = (const CImage & rhs)
{
	if (this != &rhs) {
		m_lBoxID		= rhs.m_lBoxID;
		m_nPanel		= rhs.m_nPanel;
		m_strImage		= rhs.m_strImage;
		m_lAttributes	= rhs.m_lAttributes;

		if (m_strImage.GetLength ())
			Load (m_strImage, m_nPanel, m_lBoxID);
	}

	return * this;
}

CImage::~CImage()
{
	Free ();
}

bool CImage::Load (const CString & strFilePath, UINT nPanel, ULONG lBoxID, CDC * pDC, FoxjetDatabase::ORIENTATION orient)
{
	CString strFile = FoxjetDatabase::MakeAbsolutePath (strFilePath);
	using namespace FoxjetDatabase;
	DWORD dwType = GetCxImageType (strFile);

	if (strFile.GetLength ()) {
		HBITMAP hBmp = NULL;

		if (dwType != CXIMAGE_FORMAT_BMP) {
			if (FoxjetCommon::verApp >= CVersion (1, 20)) { // TODO: rem
				CxImage img;
				bool bReleaseDC = false;

				if (!pDC) {
					bReleaseDC = true;
					pDC = ::AfxGetMainWnd ()->GetDC ();
					ASSERT (pDC);
				}

				if (img.Load (strFile, GetCxImageType (strFile))) 
					if (img.IsValid ()) 
						hBmp = img.MakeBitmap (pDC->m_hDC);

				if (bReleaseDC) {
					::AfxGetMainWnd ()->ReleaseDC (pDC);
					pDC = NULL;
				}
			}
		}
		
		if (!hBmp)
			hBmp = (HBITMAP)::LoadImage (NULL, strFile, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE); 

		if (hBmp && pDC) {
			CDC dcMem;

			dcMem.CreateCompatibleDC (pDC);

			switch (orient) {
			case ROT_90:
				{
					CBitmap & bmpSrc = * CBitmap::FromHandle (hBmp);
					CBitmap bmpDest;

					RotateLeft90 (* pDC, bmpSrc, bmpDest);

					::DeleteObject (hBmp);
					hBmp = (HBITMAP)bmpDest;
					bmpDest.Detach ();
				}
				break;
			case ROT_180:
				{
					DIBSECTION ds;
					HGDIOBJ hOld = ::SelectObject (dcMem, hBmp);

					::ZeroMemory (&ds, sizeof (ds));
					VERIFY (::GetObject (hBmp, sizeof (ds), &ds));
					Rotate180 (dcMem, CSize (ds.dsBm.bmWidth, ds.dsBm.bmHeight));
					
					::SelectObject (dcMem, hOld);
				}
				break;
			case ROT_270:
				{
					CBitmap & bmpSrc = * CBitmap::FromHandle (hBmp);
					CBitmap bmpDest;

					RotateRight90 (* pDC, bmpSrc, bmpDest);

					::DeleteObject (hBmp);
					hBmp = (HBITMAP)bmpDest;
					bmpDest.Detach ();
				}
				break;
			}
		}

		if (hBmp != NULL) {
			Free ();
			m_pBmp = new CBitmap ();
			ASSERT (m_pBmp);
			BOOL bAttach = m_pBmp->Attach (hBmp);

			if (bAttach) {
				m_strImage	= strFile;
				m_nPanel	= nPanel;
				m_lBoxID	= lBoxID;

				return true;
			}
			else 
				Free ();
		}
	}

	return false;
}

void CImage::Free()
{
	if (m_pBmp) {
		if (::DeleteObject (m_pBmp->m_hObject))
			m_pBmp->m_hObject = NULL;

//		m_pBmp->Detach ();
		delete m_pBmp;
		m_pBmp = NULL;
	}
}

CSize CImage::GetSize () const
{
	CSize size (0, 0);

	if (m_pBmp) {
		DIBSECTION ds;

		::ZeroMemory (&ds, sizeof (ds));
		VERIFY (m_pBmp->GetObject (sizeof (DIBSECTION), &ds));
		size.cx = ds.dsBm.bmWidth;
		size.cy = ds.dsBm.bmHeight;
	}

	return size;
}

