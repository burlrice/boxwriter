// HeadMapDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "HeadMapDlg.h"
#include "Extern.h"
#include "HeadDlg.h"
#include "HpHeadDlg.h"
#include "Resource.h"

using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace ListGlobals;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHeadMapDlg dialog


CHeadMapDlg::CHeadMapDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg(IDD_HEAPMAP, pParent)
{
	//{{AFX_DATA_INIT(CHeadMapDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CHeadMapDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHeadMapDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	DDX_Text (pDX, TXT_FROM, m_headFrom.m_strName);
}


BEGIN_MESSAGE_MAP(CHeadMapDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CHeadMapDlg)
	ON_BN_CLICKED(BTN_FROMPROPERTIES, OnFromproperties)
	ON_BN_CLICKED(BTN_TOPROPERTIES, OnToproperties)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHeadMapDlg message handlers

void CHeadMapDlg::OnFromproperties() 
{
	if (IsHpHead (m_headFrom.m_nHeadType)) {
		Foxjet3d::CHpHeadDlg dlg (GetDB (), m_headFrom, defElements.GetUnits (), CMapStringToString (), this);
		dlg.m_bReadOnly = true;
		dlg.DoModal ();
	}
	else {
		if (m_headFrom.IsUSB ()) {
			Foxjet3d::CHeadDlg dlg (GetDB (), m_headFrom, defElements.GetUnits (), CMapStringToString (), false, this);
			dlg.m_bReadOnly = true;
			dlg.DoModal ();
		}
		else {
			Foxjet3d::CHeadPhcDlg dlg (GetDB (), m_headFrom, defElements.GetUnits (), CMapStringToString (), false, this);
			dlg.m_bReadOnly = true;
			dlg.DoModal ();
		}
	}
}

void CHeadMapDlg::OnToproperties() 
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_TO);

	ASSERT (pCB);
	int nIndex = pCB->GetItemData (pCB->GetCurSel ());

	if (nIndex != CB_ERR) {
		if (IsHpHead (m_vHeads [nIndex].m_nHeadType)) {
			Foxjet3d::CHpHeadDlg dlg (GetDB (), m_vHeads [nIndex], defElements.GetUnits (), CMapStringToString (), this);
			dlg.m_bReadOnly = true;
			dlg.DoModal ();
		}
		else {
			if (m_vHeads [nIndex].IsUSB ()) {
				Foxjet3d::CHeadDlg dlg (GetDB (), m_vHeads [nIndex], defElements.GetUnits (), CMapStringToString (), false, this);
				dlg.m_bReadOnly = true;
				dlg.DoModal ();
			}
			else {
				Foxjet3d::CHeadPhcDlg dlg (GetDB (), m_vHeads [nIndex], defElements.GetUnits (), CMapStringToString (), false, this);
				dlg.m_bReadOnly = true;
				dlg.DoModal ();
			}
		}
	}
}

BOOL CHeadMapDlg::OnInitDialog() 
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_TO);

	ASSERT (pCB);
	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	for (int i = 0; i < m_vHeads.GetSize (); i++) {
		int nIndex = pCB->AddString (m_vHeads [i].m_strName);

		pCB->SetItemData (nIndex, i);

		if (m_vHeads [i].m_lID == m_headTo.m_lID)
			pCB->SetCurSel (nIndex);
	}

	if (pCB->GetCurSel () == CB_ERR && pCB->GetCount ())
		pCB->SetCurSel (0);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CHeadMapDlg::OnOK()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_TO);

	ASSERT (pCB);
	
	int nIndex = pCB->GetCurSel ();
	
	if (nIndex != CB_ERR) {
		m_headTo = m_vHeads [pCB->GetItemData (nIndex)];
		FoxjetCommon::CEliteDlg::OnOK ();
	}
}
