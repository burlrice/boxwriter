#include "stdafx.h"
#include "3d.h"

#include <afxext.h>

#include "Box.h"
#include "Debug.h"

using namespace FoxjetDatabase;
using namespace Foxjet3d;
using namespace Foxjet3d::Box;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CHit::CHit ()
:	m_nPanel (-1)
{
	m_element.m_lID			= -1;
	m_element.m_nType		= CRectTracker::hitNothing;
	m_element.m_pElement	= NULL;
	m_element.m_pList		= NULL;
	m_element.m_ptTracker	= CPoint (-1, -1);
}

CHit::CHit (const CHit & rhs)
:	m_nPanel (rhs.m_nPanel)
{
	CHit * pRhs = (CHit *)(LPVOID)&rhs;

	Copy (m_vHeadID,		pRhs->m_vHeadID);
	m_element.m_lID			= rhs.m_element.m_lID;
	m_element.m_nType		= rhs.m_element.m_nType;
	m_element.m_pElement	= rhs.m_element.m_pElement;
	m_element.m_pList		= rhs.m_element.m_pList;
	m_element.m_ptTracker	= rhs.m_element.m_ptTracker;
}

CHit & CHit::operator = (const CHit & rhs)
{
	if (this != &rhs) {
		CHit * pRhs = (CHit *)(LPVOID)&rhs;
	
		Copy (m_vHeadID,		pRhs->m_vHeadID);
		m_nPanel				= rhs.m_nPanel;
		m_element.m_lID			= rhs.m_element.m_lID;
		m_element.m_nType		= rhs.m_element.m_nType;
		m_element.m_pElement	= rhs.m_element.m_pElement;
		m_element.m_pList		= rhs.m_element.m_pList;
		m_element.m_ptTracker	= rhs.m_element.m_ptTracker;
	}

	return * this;
}

CHit::~CHit ()
{
}

