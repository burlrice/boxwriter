#if !defined(AFX_SAVEDLG_H__D2B68D73_92AA_4826_9849_B3DB5CDB11FC__INCLUDED_)
#define AFX_SAVEDLG_H__D2B68D73_92AA_4826_9849_B3DB5CDB11FC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SaveDlg.h : header file
//

#include "ListCtrlImp.h"
#include "Database.h"
#include "3dApi.h"
#include "BaseDocTemplate.h"
#include "version.h"
#include "Utils.h"

namespace Foxjet3d
{
	class _3D_API CMessageItem : public ItiLibrary::ListCtrlImp::CItem 
	{
	public:
		CMessageItem (const CString & strName = _T (""), 
			const CString & strDesc = _T (""), ULONG lID = -1);
		virtual ~CMessageItem ();

		virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
		virtual CString GetDispText (int nColumn) const;

		CString m_strName;
		CString m_strDesc;
		ULONG	m_lID;
	};

	class _3D_API CTaskItem : public CMessageItem 
	{
	public:
		CTaskItem (const FoxjetDatabase::TASKSTRUCT * pTask);
		virtual ~CTaskItem ();

		const FoxjetDatabase::TASKSTRUCT * m_pTask;
	};

	/////////////////////////////////////////////////////////////////////////////
	// CSaveDlg dialog

	class _3D_API CSaveDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CSaveDlg(FoxjetDocument::CBaseDocTemplate * pTemplate, CWnd* pParent = NULL);   // standard constructor
		CSaveDlg(FoxjetDocument::CBaseDocTemplate * pTemplate, UINT nIDTemplate, CWnd* pParent = NULL);

		virtual ~CSaveDlg ();

	// Dialog Data
		//{{AFX_DATA(CSaveDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA
		CStringArray						m_vFiles;
		FoxjetFile::DOCTYPE					m_type;
		FoxjetDocument::CBaseDocTemplate *	m_pTemplate;
		FoxjetCommon::CVersion				m_ver;
		CString								m_strDesc;
		ULONG								m_lLineID;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CSaveDlg)
		public:
		virtual BOOL PreTranslateMessage(MSG* pMsg);
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		class CTaskCache
		{
		public:
			CTaskCache ();
			virtual ~CTaskCache ();

			CArray <FoxjetDatabase::TASKSTRUCT *, FoxjetDatabase::TASKSTRUCT *> m_vTasks;
			ULONG m_lLineID;
		};

		virtual bool GetSelectedFilenames (CStringArray & vFiles, FoxjetFile::DOCTYPE & type);
		virtual void OnOK();
		bool Select (const CString & strFile, FoxjetFile::DOCTYPE type, bool bDeselectPrev = false);
		CString GetSelectedLine () const;
		ULONG GetSelectedLineID () const;
		const CTaskCache * GetTasks (ULONG lLineID);
		const FoxjetDatabase::TASKSTRUCT * GetTask (ULONG lLineID, ULONG lID);
		bool DeleteCachedTask (ULONG lTaskID);

		ItiLibrary::ListCtrlImp::CListCtrlImp m_lviTask;
		CArray <FoxjetCommon::CVersion, const FoxjetCommon::CVersion &> m_vVer;
		static const int m_nMsgTab;
		static const int m_nTaskTab;
		static const int m_nDescIndex;
		static const int m_nIDIndex;



		CArray <CTaskCache *, CTaskCache *> m_vCache;

		// Generated message map functions
		//{{AFX_MSG(CSaveDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnDblclkMessages(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnDblclkTasks(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnItemchangedTasks(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnItemchangedMessages(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnSelchangeTab(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnSelchangeLine();
		afx_msg void OnVersion();
		afx_msg void OnEditSelectall();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; // namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAVEDLG_H__D2B68D73_92AA_4826_9849_B3DB5CDB11FC__INCLUDED_)
