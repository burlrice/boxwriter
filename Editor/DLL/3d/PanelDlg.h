#if !defined(AFX_PANELDLG_H__167858F2_097D_47C9_8941_3E325BCF5E11__INCLUDED_)
#define AFX_PANELDLG_H__167858F2_097D_47C9_8941_3E325BCF5E11__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PanelDlg.h : header file
//

#include "Database.h"
#include "3dApi.h"
#include "RoundButtons.h"
#include "Utils.h"

namespace Foxjet3d
{
	/////////////////////////////////////////////////////////////////////////////
	// CPanelDlg dialog

	class _3D_API CPanelDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CPanelDlg (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::PANELSTRUCT & panel, CWnd* pParent = NULL);

	// Dialog Data
		//{{AFX_DATA(CPanelDlg)
		CString	m_strLine;
		CString	m_strName;
		UINT	m_nNumber;
		//}}AFX_DATA

		UINT	m_nDirection;
		UINT	m_nOrientation;
		bool	m_bReadOnly;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CPanelDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		FoxjetUtils::CRoundButtons m_buttons;

		afx_msg void OnDirectionClick();
		afx_msg void OnOrientationClick();
		FoxjetDatabase::COdbcDatabase & m_db;
		FoxjetDatabase::PANELSTRUCT m_panel;
		CBitmap m_bmpOrientation [4];
		CBitmap m_bmpDirection [4];

		// Generated message map functions
		//{{AFX_MSG(CPanelDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PANELDLG_H__167858F2_097D_47C9_8941_3E325BCF5E11__INCLUDED_)
