// BoxDimDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "BoxDimDlg.h"
#include "List.h"
#include "Coord.h"
#include "resource.h"
#include "Color.h"
#include "Debug.h"
#include "ItiLibrary.h"
#include "ImageDlg.h"
#include "Utils.h"
#include "Extern.h"
#include "AppVer.h"

using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace Foxjet3d;
using namespace Foxjet3d::Box;
using namespace FoxjetDatabase;
using namespace ItiLibrary;

#define SS_PREVIEW 100


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CBoxDimDlg::CItem::CItem (const FoxjetDatabase::IMAGESTRUCT & rhs)
{
	m_lBoxID		= rhs.m_lBoxID;
	m_nPanel		= rhs.m_nPanel;
	m_strImage		= rhs.m_strImage;
	m_lAttributes	= rhs.m_lAttributes;
}

CBoxDimDlg::CItem::~CItem ()
{
}

int CBoxDimDlg::CItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	const CItem * pRhs = (const CItem *)&rhs;

	switch (nColumn) {
	case 0:		return m_nPanel - pRhs->m_nPanel;
	case 1:		return m_lAttributes - pRhs->m_lAttributes;
	case 2:		return m_strImage.Compare (pRhs->m_strImage);
	}

	return 0;
}

CString CBoxDimDlg::CItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0:		
		str.Format (_T ("%d"), m_nPanel);
		break;
	case 1:	
		{
			static const CString strAttr [] = 
			{
				LoadString (IDS_TL), 
				LoadString (IDS_TR), 
				LoadString (IDS_BR), 
				LoadString (IDS_BL), 
				LoadString (IDS_STRETCH),
			};
			
			str = strAttr [m_lAttributes];
			break;
		}
	case 2:	
		str = m_strImage;
		break;
	}

	return str;
}

/////////////////////////////////////////////////////////////////////////////
// CBoxDimDlg dialog

IMPLEMENT_DYNAMIC (CBoxDimDlg, FoxjetCommon::CEliteDlg)

CBoxDimDlg::CBoxDimDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent /*=NULL*/)
:	m_units (INCHES),
	m_bReadOnly (false),
	m_bPreview (false),
	m_db (db),
	m_3dBox (db),
	m_bCreated3d (false),
	m_bEdit (true),
	m_lLineID (-1),
	FoxjetCommon::CEliteDlg(IDD_BOXDIM, pParent)
{
	m_box.m_lHeight = 1000;
	m_box.m_lLength = 1000;
	m_box.m_lWidth	= 1000;
	m_box.m_strName = _T ("");
}

CBoxDimDlg::~CBoxDimDlg ()
{
	m_vImages.RemoveAll ();
}

void CBoxDimDlg::DoDataExchange(CDataExchange* pDX)
{
	const FoxjetDatabase::HEADSTRUCT & head = GetDefaultHead ();
	CCoord coordMin (0, 0, m_units, head);
	CCoord coordMax (0, 0, m_units, head);
	coordMin.m_x = MIN_PRODLEN;
	coordMax.m_x = MAX_PRODLEN;

	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);

	DDX_Coord (pDX, TXT_LENGTH, (long &)m_box.m_lLength, m_units, head, true);
	DDV_Coord (pDX, TXT_LENGTH, m_box.m_lLength, 0, m_units, head, DDVCOORDXBOTH, coordMin, coordMax);

	DDX_Coord (pDX, TXT_WIDTH, (long &)m_box.m_lWidth, m_units, head, true);
	DDV_Coord (pDX, TXT_WIDTH, m_box.m_lWidth, 0, m_units, head, DDVCOORDXBOTH, coordMin, coordMax);

	DDX_Coord (pDX, TXT_HEIGHT, (long &)m_box.m_lHeight, m_units, head, true);
	DDV_Coord (pDX, TXT_HEIGHT, m_box.m_lHeight, 0, m_units, head, DDVCOORDXBOTH, coordMin, coordMax);

	DDX_Text (pDX, TXT_NAME, m_box.m_strName);
	DDV_MinMaxChars (pDX, m_box.m_strName, TXT_NAME, 1, 50);

	DDX_Text (pDX, TXT_DESC, m_box.m_strDesc);
	DDX_Check (pDX, CHK_PREVIEW, m_bPreview);
}


BEGIN_MESSAGE_MAP(CBoxDimDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CBoxDimDlg)
	ON_WM_CREATE()
	ON_WM_DRAWITEM()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (CHK_PREVIEW, OnPreviewClick)
	ON_NOTIFY(LVN_ITEMCHANGED, LV_IMAGES, OnItemchangedImages)
	ON_NOTIFY(NM_DBLCLK, LV_IMAGES, OnDblclickBoxes)
	ON_BN_CLICKED (BTN_ADD, OnAdd)
	ON_BN_CLICKED (BTN_EDIT, OnEdit)
	ON_BN_CLICKED (BTN_DELETE, OnDelete)
	ON_BN_CLICKED (IDHELP, OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBoxDimDlg message handlers

void CBoxDimDlg::OnHelp ()
{
	CEliteDlg::OnHelp (this);
}

BOOL CBoxDimDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();
	CWnd * p = GetDlgItem (IDC_BOX);
	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;

	GetImageRecords (m_db, m_box.m_lID, m_vImages);
	
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_PANEL), 40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_ATTRIBUTES), 60));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_PATH), 180));

	m_lv.Create (LV_IMAGES, vCols, this, defElements.m_strRegSection);

	for (int i = 0; i < m_vImages.GetSize (); i++) {
		const IMAGESTRUCT & img = m_vImages [i];
		m_lv.InsertCtrlData (new CItem (img));
	}

	ASSERT (GetDlgItem (IDOK));
	GetDlgItem (IDOK)->EnableWindow (!m_bReadOnly);

	ASSERT (p);
	CRect rc;
	p->GetWindowRect (&rc);
	ScreenToClient (&rc);
	m_wndPreview.ShowWindow (SW_SHOW);
	BOOL bSetWindowPos = m_wndPreview.SetWindowPos (NULL, rc.left, rc.top,
		rc.Width (), rc.Height (), SWP_NOZORDER | SWP_SHOWWINDOW);
	ASSERT (bSetWindowPos);

	OnPreviewClick ();
	InvalidateBox ();

	if (FoxjetCommon::verApp < CVersion (1, 20)) { // TODO: rem
		UINT nID [] = 
		{
			LV_IMAGES,
			BTN_ADD,
			BTN_EDIT,
			BTN_DELETE
		};

		for (int i = 0; i < ARRAYSIZE (nID); i++)
			if (CWnd * p = GetDlgItem (nID [i]))
				p->ShowWindow (SW_HIDE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CBoxDimDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (FoxjetCommon::CEliteDlg::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	BOOL bCreate = m_wndPreview.Create (NULL, 
		SS_SUNKEN | SS_GRAYRECT | SS_OWNERDRAW, 
		CRect (), this, SS_PREVIEW);
	ASSERT (bCreate);

	return 0;
}

void CBoxDimDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if (IsPreviewEnabled () && nIDCtl == SS_PREVIEW) {
		DRAWITEMSTRUCT & dis = * lpDrawItemStruct;
		CDC dc;
		CRect rc = dis.rcItem;

		if (!m_3dBox.IsCreated ()) {
			BeginWaitCursor ();

			m_3d.AddObject (&m_3dBox);
			m_3dBox.SetBox (m_box);

			if (m_3dBox.Create (m_3d)) {
				//m_3dBox.LoadPanels (theApp.GetDB (), m_task.m_lLineID);

				for (int i = 0; i < 6; i++) {
					IMAGESTRUCT img;
					int nPanel = i + 1;

					if (GetImageRecord (m_db, m_box.m_lID, nPanel, img)) {
						m_3dBox.SetPanelImage (nPanel, img);
					}
				}

				m_3d.Update ();
			}

			EndWaitCursor ();
		}

		dc.Attach (dis.hDC);
		dc.FillSolidRect (rc, Color::rgbDarkGray);

		if (m_3dBox.IsCreated ()) 
			m_3d.Draw (&dc, rc);

		dc.Detach ();
	}
	else
		FoxjetCommon::CEliteDlg::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

bool CBoxDimDlg::IsPreviewEnabled() const
{
	CButton * p = (CButton *)GetDlgItem (CHK_PREVIEW);
	ASSERT (p);

	return p->GetCheck () == 1;
}

void CBoxDimDlg::OnPreviewClick()
{
	CWnd * p = GetDlgItem (IDC_BOX);
	ASSERT (p);
	bool bPreview = IsPreviewEnabled ();

	if (bPreview) {
		if (!m_bCreated3d) {
			CBoxParams * pParams = new CBoxParams (m_3d, m_task);
			CString strSection = _T ("Settings\\CBoxDimDlg\\DirectX");
			int nRes = 7;
			
			VERIFY (m_3d.SetResolution (nRes));
	
			if (CWinApp * pApp = ::AfxGetApp ()) {
				nRes = pApp->GetProfileInt (_T ("Settings\\DirectX"), _T ("res"), 7);
				pParams->LoadSettings (* pApp, strSection);
			}

			VERIFY (m_bCreated3d = m_3d.Create (this, pParams));
			InvalidateBox ();
		}
	}

	m_wndPreview.ShowWindow (bPreview ? SW_SHOW : SW_HIDE);
	p->ShowWindow (bPreview ? SW_HIDE : SW_SHOW);
}

void CBoxDimDlg::OnDestroy() 
{
	CButton * pPreview = (CButton *)GetDlgItem (CHK_PREVIEW);
	ASSERT (pPreview);

	m_bPreview = pPreview->GetCheck () == 1;
	m_3d.Destroy ();	
	FoxjetCommon::CEliteDlg::OnDestroy();
}

void CBoxDimDlg::OnItemchangedImages(NMHDR *pNMHDR, LRESULT *pResult)
{
	if (IsPreviewEnabled ()) {
		CBoxParams & p = * (CBoxParams *)m_3d.m_pParams;
		CLongArray sel = GetSelectedItems (m_lv.GetListCtrl ());

		if (sel.GetSize ()) {
			if (CItem * pData = (CItem *)m_lv.GetCtrlData (sel [0])) {
				p.m_nPanel = pData->m_nPanel;
				BeginWaitCursor ();
				InvalidateBox ();
				EndWaitCursor ();
			}
		}
	}
}

void CBoxDimDlg::OnAdd()
{
	int nSize = m_lv->GetItemCount ();

	if (nSize == 6) {
		MsgBox (* this, LoadString (IDS_CANTADDIMAGE));
	}
	else {
		CImageDlg dlg (this);

		dlg.m_lAttributes = STRETCH;
		dlg.m_nPanel = nSize + 1;
		
		for (int i = 0; i < nSize; i++) {
			CItem * pItem = (CItem *)m_lv.GetCtrlData (i);
			dlg.m_vInUse.Add (pItem->m_nPanel);
		}

		for (int i = 0; i < nSize; i++) {
			CItem * pItem = (CItem *)m_lv.GetCtrlData (i);
			UINT nPanel = i + 1;

			if (pItem->m_nPanel != nPanel) {
				dlg.m_nPanel = nPanel;
				break;
			}
		}

		if (dlg.DoModal () == IDOK) {
			IMAGESTRUCT img;

			img.m_lBoxID = m_box.m_lID;
			img.m_strImage = dlg.m_strPath;
			img.m_lAttributes = (IMAGEATTRIBUTES)dlg.m_lAttributes;
			img.m_nPanel = dlg.m_nPanel;

			VERIFY (AddImageRecord (m_db, img));
			m_lv.InsertCtrlData (new CItem (img));
			
			m_3dBox.Free (); // causes a reload in OnDraw
			InvalidateBox ();
		}
	}
}

void CBoxDimDlg::OnDblclickBoxes(NMHDR* pNMHDR, LRESULT* pResult)
{
	OnEdit ();

	if (pResult)
		* pResult = 0;
}

void CBoxDimDlg::OnEdit()
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		CItem * pItem = (CItem * )m_lv.GetCtrlData (sel [0]);
		CImageDlg dlg (this);
		ASSERT (pItem);

		dlg.m_lAttributes = pItem->m_lAttributes;
		dlg.m_strPath = pItem->m_strImage;
		dlg.m_nPanel = pItem->m_nPanel;

		for (int i = 0; i < m_lv->GetItemCount (); i++) {
			CItem * pItem = (CItem *)m_lv.GetCtrlData (i);

			if (pItem->m_nPanel != (UINT)dlg.m_nPanel)
				dlg.m_vInUse.Add (pItem->m_nPanel);
		}

		if (dlg.DoModal () == IDOK) {
			int nIndex = m_lv.GetDataIndex (pItem);

			pItem->m_lAttributes = (IMAGEATTRIBUTES)dlg.m_lAttributes;
			pItem->m_strImage = dlg.m_strPath;

			if (pItem->m_nPanel != (UINT)dlg.m_nPanel) {
				IMAGESTRUCT img	= pItem->GetMembers ();

				VERIFY (DeleteImageRecord (m_db, m_box.m_lID, pItem->m_nPanel));
				DeleteImageRecord (m_db, m_box.m_lID, dlg.m_nPanel);
				img.m_nPanel = dlg.m_nPanel;
				VERIFY (AddImageRecord (m_db, img));
			}
			else {
				IMAGESTRUCT s = pItem->GetMembers ();
				VERIFY (UpdateImageRecord (m_db, s));
			}

			m_lv.UpdateCtrlData (pItem);
			m_3dBox.Free (); // causes a reload in OnDraw
			InvalidateBox ();
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CBoxDimDlg::OnDelete()
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		int nResult = MsgBox (* this, LoadString (IDS_CONFIRMELEMENTDELETE), 
			LoadString (IDS_CONFIRM), MB_YESNO);

		if (nResult == IDYES) {
			for (int i = 0; i < sel.GetSize (); i++) {
				CItem * pItem = (CItem *)m_lv.GetCtrlData (sel [i]);
				int nIndex = m_lv.GetDataIndex (pItem);

				VERIFY (DeleteImageRecord (m_db, m_box.m_lID, pItem->m_nPanel));
				m_lv.DeleteCtrlData (sel [i]);
			}

			m_3dBox.Free (); // causes a reload in OnDraw
			InvalidateBox ();
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CBoxDimDlg::OnOK()
{
	bool bDuplicateName = false;

	if (!UpdateData ())
		return;

	if (!m_bEdit) 
		bDuplicateName = GetBoxID (m_db, m_box.m_strName) != NOTFOUND;
	else {
		ULONG lID = GetBoxID (m_db, m_box.m_strName);

		if (lID != NOTFOUND)
			bDuplicateName = lID != m_box.m_lID; 
	}

	if (bDuplicateName) {
		CString str;

		str.Format (LoadString (IDS_BOXEXISTS), m_box.m_strName);
		MsgBox (* this, str, NULL, MB_ICONINFORMATION);
		return;
	}		

	FoxjetCommon::CEliteDlg::OnOK ();
	VERIFY (UpdateBoxRecord (m_db, m_box));
}

void CBoxDimDlg::InvalidateBox()
{
	if (IsPreviewEnabled ()) {
		const FoxjetDatabase::HEADSTRUCT & head = GetDefaultHead ();
		CString str [3]; 
		
		GetDlgItemText (TXT_WIDTH, str [0]);
		GetDlgItemText (TXT_LENGTH, str [1]);
		GetDlgItemText (TXT_HEIGHT, str [2]);
		
		double dDim [3] = 
		{
			FoxjetDatabase::_ttof (str [0]),
			FoxjetDatabase::_ttof (str [1]),
			FoxjetDatabase::_ttof (str [2]),
		};

		if (dDim [0] > 0.0 &&
			dDim [1] > 0.0 &&
			dDim [2] > 0.0)
		{
			int x = CBaseElement::LogicalToThousandths (
				CCoord::CoordToPoint (dDim [0], 0.0, m_units, head), head).x;
			int y = CBaseElement::LogicalToThousandths (
				CCoord::CoordToPoint (dDim [1], 0.0, m_units, head), head).x;
			int z = CBaseElement::LogicalToThousandths (
				CCoord::CoordToPoint (dDim [2], 0.0, m_units, head), head).x;

			m_box.m_lWidth = x;
			m_box.m_lLength = y;
			m_box.m_lHeight = z;

			m_3dBox.SetBox (m_box);
		}

		CRect rc;
		const CPoint ptOffset = CPoint (30, 30);
		CBoxParams & p = * (CBoxParams *)m_3d.m_pParams;

		m_wndPreview.GetClientRect (rc);
		m_3dBox.SetPos (ptOffset);
		p.m_rZoom = 1.0;

		CSize sizeBox = m_3dBox.CalcViewSize (p) + ptOffset;
		CSize sizeView = rc.Size ();

		double dZoom = min (
			(double)sizeView.cx / (double)sizeBox.cx,
			(double)sizeView.cy / (double)sizeBox.cy);

		p.m_rZoom = dZoom;
		m_3d.Update ();
		m_wndPreview.Invalidate ();
	}
}


void CBoxDimDlg::OnCancel()
{
	FoxjetCommon::CEliteDlg::OnCancel ();

	// restore the old images
	VERIFY (DeleteImageRecord (m_db, m_box.m_lID));

	for (int i = 0; i < m_vImages.GetSize (); i++) {
		IMAGESTRUCT img = m_vImages [i];
		VERIFY (AddImageRecord (m_db, img));
	}
}
