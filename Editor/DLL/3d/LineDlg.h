#if !defined(AFX_LINEDLG_H__25E2FC35_97A8_4DA8_87FC_FB1CA4FBA17A__INCLUDED_)
#define AFX_LINEDLG_H__25E2FC35_97A8_4DA8_87FC_FB1CA4FBA17A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LineDlg.h : header file
//

#include "3dApi.h"
#include "Database.h"
#include "OdbcDatabase.h"
#include "Utils.h"
#include "RoundButtons.h"

namespace Foxjet3d
{
	/////////////////////////////////////////////////////////////////////////////
	// CLineDlg dialog

	class _3D_API CLineDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CLineDlg (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::LINESTRUCT & line, CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CLineDlg)
		//}}AFX_DATA

		//CIPAddressCtrl	m_ctrlAuxBoxIp;
		FoxjetDatabase::LINESTRUCT m_line;
		CString m_strSerialDownload;
		BOOL m_bResetCountsOnRestart;
		BOOL m_bRequireLogin;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CLineDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

		static const FoxjetCommon::LIMITSTRUCT m_lmtName;

	// Implementation
	protected:
		virtual void OnOK();
		FoxjetDatabase::COdbcDatabase & m_db;
		FoxjetUtils::CRoundButtons m_buttons;

		// Generated message map functions
		//{{AFX_MSG(CLineDlg)
		afx_msg void OnAms();
		//afx_msg void OnSerial1();
		//afx_msg void OnSerial2();
		//afx_msg void OnSerial3();
		virtual BOOL OnInitDialog();
	//}}AFX_MSG
		
		//afx_msg void OnClickAuxEnabled ();

		DECLARE_MESSAGE_MAP()
	};
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LINEDLG_H__25E2FC35_97A8_4DA8_87FC_FB1CA4FBA17A__INCLUDED_)
