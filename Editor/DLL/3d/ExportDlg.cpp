// ExportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "ExportDlg.h"
#include "Extern.h"
#include "OpenDlg.h"
#include "FileExt.h"
#include "Registry.h"
#include "Utils.h"
#include "HeadMapDlg.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Serialize.h"
#include "Parse.h"
#include "Resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Foxjet3d;
using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace FoxjetFile;
using namespace FoxjetDatabase;
using namespace ItiLibrary;

//////////////////////////////////////////////////////////////////////

int _3D_API Foxjet3d::Copy (CWnd * pParent)
{
	return CExportDlg (pParent).DoModal ();
}

//////////////////////////////////////////////////////////////////////

CExportDlg::CHeadItem::CHeadItem ()
{
	m_headFrom.m_lID = -1;
	m_headTo.m_lID = -1;
}

CExportDlg::CHeadItem::~CHeadItem ()
{
}

int CExportDlg::CHeadItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	const CHeadItem & cmp = (CHeadItem &)rhs;

	switch (nColumn) {
	case 1:		return m_headFrom.m_strName.CompareNoCase (cmp.m_headFrom.m_strName);
	case 2:		return m_headTo.m_strName.CompareNoCase (cmp.m_headTo.m_strName);
	}

	return 0;
}

CString CExportDlg::CHeadItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 1:		return m_headFrom.m_strName;
	case 2:		return m_headTo.m_strName;
	}

	return _T ("");
}

////////////////////////////////////////////////////////////////////////////
// CExportDlg dialog

IMPLEMENT_DYNAMIC (CExportDlg, CBaseExportDlg);

CExportDlg::CExportDlg(CWnd* pParent /*=NULL*/)
:	CBaseExportDlg(IDD_COPY, pParent)
{
	//{{AFX_DATA_INIT(CExportDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CExportDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseExportDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExportDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CExportDlg, CBaseExportDlg)
	//{{AFX_MSG_MAP(CExportDlg)
	ON_CBN_SELCHANGE(CB_LINEFROM, OnSelchangeLinefrom)
	ON_NOTIFY(NM_DBLCLK, LV_HEADS, OnDblclkHeads)
	ON_NOTIFY(LVN_ITEMCHANGED, LV_HEADS, OnItemchangedHeads)
	ON_BN_CLICKED(BTN_EDITMAPPING, OnEditmapping)
	ON_BN_CLICKED(BTN_EXPORT, OnExport)
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE(CB_LINETO, OnSelchangeLineto)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExportDlg message handlers

BOOL CExportDlg::OnInitDialog() 
{
	using namespace ListCtrlImp;

	CArray <CColumn, CColumn> vCols;
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CComboBox * pFrom = (CComboBox *)GetDlgItem (CB_LINEFROM);

	ASSERT (pFrom);
	
	vCols.Add (CColumn (LoadString (IDS_EXCLUDE))); 
	vCols.Add (CColumn (LoadString (IDS_FROM)));
	vCols.Add (CColumn (LoadString (IDS_TO)));

	m_lvHeads.Create (LV_HEADS, vCols, this, a2w (GetRuntimeClass ()->m_lpszClassName));
	m_lvHeads->SetExtendedStyle (m_lvHeads->GetExtendedStyle () | LVS_EX_CHECKBOXES);
	
	GetLineRecords (vLines);

	for (int i = 0; i < vLines.GetSize (); i++) {
		int nIndex = pFrom->AddString (vLines [i].m_strName);

		pFrom->SetItemData (nIndex, vLines [i].m_lID);
	}

	if (pFrom->GetCount ()) {
		pFrom->SetCurSel (0);
		OnSelchangeLinefrom ();
	}

	CBaseExportDlg::OnInitDialog();
	BEGIN_TRANS (GetDB ());
	GetDlgItem (IDOK)->EnableWindow (FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

ULONG CExportDlg::GetSelectedToLineID () const
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_LINETO);

	ASSERT (pCB);

	return pCB->GetItemData (pCB->GetCurSel ());
}

void CExportDlg::OnSelchangeLineto() 
{
	LoadHeadMapping ();
}

void CExportDlg::OnSelchangeLinefrom() 
{
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CArray <TASKSTRUCT *, TASKSTRUCT *> vTasks;
	CComboBox * pTo = (CComboBox *)GetDlgItem (CB_LINETO);
	CListBox * pTasks = (CListBox *)GetDlgItem (LB_TASKS);
	ULONG lFromID = GetSelectedFromLineID ();

	ASSERT (pTo);
	ASSERT (pTasks);
	
	pTo->ResetContent ();
	GetLineRecords (vLines);

	for (int i = 0; i < vLines.GetSize (); i++) {
		if (vLines [i].m_lID != lFromID) {
			int nIndex = pTo->AddString (vLines [i].m_strName);

			pTo->SetItemData (nIndex, vLines [i].m_lID);
		}
	}

	if (CWnd * p = GetDlgItem (BTN_EXPORT))
		p->EnableWindow (pTo->GetCount ());

	if (pTo->GetCount ())
		pTo->SetCurSel (0);

	pTasks->ResetContent ();
	GetTaskRecords (GetDB (), lFromID, vTasks);

	for (int i = 0; i < vTasks.GetSize (); i++) {
		int nIndex = pTasks->AddString (vTasks [i]->m_strName);

		pTasks->SetItemData (nIndex, vTasks [i]->m_lID);
	}

	LoadHeadMapping ();
	Free (vTasks);
}

void CExportDlg::LoadHeadMapping ()
{
	ULONG lFromID = GetSelectedFromLineID ();
	ULONG lToID = GetSelectedToLineID ();
	CArray <HEADSTRUCT, HEADSTRUCT &> vFrom;
	CArray <HEADSTRUCT, HEADSTRUCT &> vTo;
	CString strSection;

	strSection.Format (_T ("%s\\%s\\%d --> %d"), 
		defElements.m_strRegSection,
		a2w (GetRuntimeClass ()->m_lpszClassName), lFromID, lToID);
	m_lvHeads.DeleteCtrlData ();

	GetLineHeads (lFromID, vFrom);
	GetLineHeads (lToID, vTo);

	while (vFrom.GetSize ()) {
		CHeadItem * p = new CHeadItem ();
		CString strKey;
		CStringArray v;
		bool bFound = false;

		p->m_headFrom = vFrom [0];
		m_lvHeads.InsertCtrlData (p);
		int nIndex = m_lvHeads.GetDataIndex (p);

		strKey.Format (_T ("%d"), p->m_headFrom.m_lID);
		CString strData = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strSection, strKey, _T (""));
		Tokenize (strData, v);

		if (v.GetSize () >= 2) {
			ULONG lHeadID = _ttol (v [0]);
			bool bExclude = _ttol (v [1]) ? true : false;

			m_lvHeads->SetCheck (nIndex, bExclude);

			for (int i = 0; i < vTo.GetSize (); i++) {
				if (vTo [i].m_lID == lHeadID) {
					p->m_headTo = vTo [i];
					bFound = true;
					break;
				}
			}
		}

		if (!bFound) {
			if (vTo.GetSize ()) {
				p->m_headTo = vTo [0];
				vTo.RemoveAt (0);
			}
			else
				m_lvHeads->SetCheck (nIndex, TRUE);
		}

		m_lvHeads.UpdateCtrlData (p);
		vFrom.RemoveAt (0);
	}
}

void CExportDlg::SaveHeadMapping ()
{
	ULONG lFromID = GetSelectedFromLineID ();
	ULONG lToID = GetSelectedToLineID ();
	CString strKey, strData, strSection = a2w (GetRuntimeClass ()->m_lpszClassName);
	int nCount = m_lvHeads->GetItemCount ();

	strSection.Format (_T ("%s\\%s\\%d --> %d"), 
		defElements.m_strRegSection,
		a2w (GetRuntimeClass ()->m_lpszClassName), 
		lFromID, lToID);

	for (int i = 0; i < nCount; i++) {
		if (CHeadItem * p = (CHeadItem *)m_lvHeads.GetCtrlData (i)) {
			bool bExclude = m_lvHeads->GetCheck (i) ? true : false;

			strKey.Format (_T ("%d"), p->m_headFrom.m_lID);
			strData.Format (_T ("%d,%d"), p->m_headTo.m_lID, bExclude);
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strSection, strKey, strData);
		}
	}
}

void CExportDlg::OnDblclkHeads(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEditmapping ();
	
	if (pResult)
		*pResult = 0;
}

void CExportDlg::OnItemchangedHeads(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pnm = (NM_LISTVIEW*)pNMHDR;

	// this seems screwy, but it's copied from the MFC src
	bool bChecked = ((pnm->uNewState >> 12) -1) ? true : false;
	bool bCheckStateChange = bChecked ^ (((pnm->uOldState >> 12) -1) ? true : false);

/* TODO: rem
	if (bCheckStateChange && IsWindowVisible ())
		SaveHeadMapping ();
*/

	if (pResult)
		*pResult = 0;
}

void CExportDlg::OnEditmapping() 
{
	CLongArray sel = GetSelectedItems (m_lvHeads);

	if (sel.GetSize ()) {
		CHeadMapDlg dlg (this);
		CHeadItem * p = (CHeadItem *)m_lvHeads.GetCtrlData (sel [0]);
		ULONG lToID = GetSelectedToLineID ();

		ASSERT (p);

		dlg.m_headFrom = p->m_headFrom;
		dlg.m_headTo = p->m_headTo;
		GetLineHeads (lToID, dlg.m_vHeads);
		
		if (dlg.DoModal () == IDOK) {
			p->m_headTo = dlg.m_headTo;
			m_lvHeads.UpdateCtrlData (p);
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOHEADSELECTED));
}

int CExportDlg::FindMessageByHeadID (const FoxjetDatabase::TASKSTRUCT & task, ULONG lHeadID)
{
	for (int i = 0; i < task.m_vMsgs.GetSize (); i++)
		if (task.m_vMsgs [i].m_lHeadID == lHeadID)
			return i;

	return -1;
}

void CExportDlg::PrepareForExport (FoxjetDatabase::TASKSTRUCT & task, ULONG lLineID) 
{
	task.m_lID = -1;
	task.m_lLineID = lLineID;

	for (int i = 0; i < task.m_vMsgs.GetSize (); i++) 
		task.m_vMsgs [i].m_lTaskID = -1;

	for (int i = 0; i < m_lvHeads->GetItemCount (); i++) {
		CHeadItem * p = (CHeadItem *)m_lvHeads.GetCtrlData (i);
		int nItemIndex = m_lvHeads.GetDataIndex (p);

		ASSERT (p);

		int nHeadIndex = FindMessageByHeadID (task, p->m_headFrom.m_lID);

		if (nHeadIndex != -1) {
			bool bExclude = m_lvHeads->GetCheck (nItemIndex) ? true : false;

			if (bExclude)
				task.m_vMsgs.RemoveAt (nHeadIndex);
			else
				task.m_vMsgs [nHeadIndex].m_lHeadID = p->m_headTo.m_lID;
		}
	}
}

void CExportDlg::SetBoxUsage (ULONG lBoxID, ULONG lLineID)
{
	try {
		COdbcRecordset rst (GetDB ());
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u AND [%s]=%u;"),
			BoxToLine::m_lpszTable, 
			BoxToLine::m_lpszLineID, lLineID,
			BoxToLine::m_lpszBoxID, lBoxID);
		rst.Open (str, rst.dynaset);

		if (rst.IsEOF ()) {
			BOXLINKSTRUCT link;

			link.m_lBoxID = lBoxID;
			link.m_lLineID = lLineID;
			rst << link;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
}

bool CExportDlg::PromptOverwriteExisting (FoxjetCommon::CLongArray & sel, ULONG lLineID)
{
	CListBox * pLB = (CListBox *)GetDlgItem (LB_TASKS);
	CStringArray v;
	
	ASSERT (pLB);

	for (int i = 0; i < sel.GetSize (); i++) {
		CString strTask;
		TASKSTRUCT task;

		pLB->GetText (sel [i], strTask);

		if (GetTaskRecord (GetDB (), strTask, lLineID, task))
			v.Add (task.m_strName);
	}

	if (v.GetSize ()) {
		CString str = LoadString (IDS_OVERWRITETASKS) + BuildNameList (v);

		if (MsgBox (* this, str, LoadString (IDS_CONFIRM), MB_YESNO | MB_ICONWARNING) == IDNO)
			return false;
	}

	return true;
}

CString CExportDlg::BuildNameList (const CStringArray & v)
{
	const int nMax = 10;
	CString str;

	str += _T ("\n");

	for (int i = 0; i < min (v.GetSize (), nMax); i++) 
		str += _T ("\n") + v [i];

	if (v.GetSize () > nMax)
		str += _T ("\n.\n.\n.");

	return str;
}

void CExportDlg::OnExport() 
{
	CListBox * pLB = (CListBox *)GetDlgItem (LB_TASKS);
	CComboBox * pLineTo = (CComboBox *)GetDlgItem (CB_LINETO);
	CString strLineTo;
	COdbcDatabase & db = GetDB ();

	ASSERT (pLineTo);
	ASSERT (pLB);

	BeginWaitCursor ();

	if (pLineTo->GetLBTextLen (pLineTo->GetCurSel ()) > 0)
		pLineTo->GetLBText (pLineTo->GetCurSel (), strLineTo);
	
	CLongArray sel = GetSelectedItems (* pLB);

	if (sel.GetSize ()) {
		ULONG lFromID = GetSelectedFromLineID ();
		ULONG lToID = GetSelectedToLineID ();
		CString strExport = LoadString (IDS_EXPORTEDTASKS);
		CStringArray vExport;

		if (!PromptOverwriteExisting (sel, lToID))
			return;

		try {
			for (int i = 0; i < sel.GetSize (); i++) {
				ULONG lTaskID = pLB->GetItemData (sel [i]);
				TASKSTRUCT task;

				GetTaskRecord (GetDB (), lTaskID, task);
				SetBoxUsage (task.m_lBoxID, lToID);
				PrepareForExport (task, lToID);

				ULONG lDelete = GetTaskID (db, task.m_strName, strLineTo, GetPrinterID (db));
				DeleteTaskRecord (db, lDelete);

				if (!AddTaskRecord (db, task)) {
					CString str;

					str.Format (LoadString (IDS_FAILEDTOEXPORTTASK), task.m_strName);
					MsgBox (* this, str, LoadString (IDS_WARNING), MB_OK | MB_ICONSTOP);

					return;
				}
				else 
					vExport.Add (task.m_strName);
			}

			SaveHeadMapping ();	
			MsgBox (* this, strExport + BuildNameList (vExport));
			CExportDlg::OnOK (); //GetDlgItem (IDOK)->EnableWindow (TRUE);
		}
		catch (COdbcException * e) { 
			MsgBox (* this, e->m_strError, MB_ICONWARNING);
			HANDLEEXCEPTION_TRACEONLY (e); 
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOTASKSELECTED));

	EndWaitCursor ();
}

void CExportDlg::OnOK()
{
	SaveHeadMapping ();	
	COMMIT_TRANS (GetDB ());
	CBaseExportDlg::OnOK ();
}

void CExportDlg::OnCancel()
{
	ROLLBACK_TRANS (GetDB ());
	CBaseExportDlg::OnCancel();
}

