#if !defined(AFX_AMSDLG_H__61D22769_1C1D_4A9B_9776_54A94F711CE4__INCLUDED_)
#define AFX_AMSDLG_H__61D22769_1C1D_4A9B_9776_54A94F711CE4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AmsDlg.h : header file
//

#include "Database.h"
#include "List.h"
#include "3dApi.h"
#include "RoundButtons.h"
#include "Utils.h"

namespace Foxjet3d
{
	/////////////////////////////////////////////////////////////////////////////
	// CAmsDlg dialog

	class _3D_API CAmsDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CAmsDlg (const FoxjetDatabase::LINESTRUCT & line, CWnd* pParent = NULL);   // standard constructor

		FoxjetDatabase::LINESTRUCT m_line;

	// Dialog Data
		//{{AFX_DATA(CAmsDlg)
		//}}AFX_DATA


	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CAmsDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

		static const double m_dAmsIntervalMin;
		static const double m_dAmsIntervalMax;

	// Implementation
	protected:
		FoxjetUtils::CRoundButtons m_buttons;
		afx_msg void OnDisable ();

		// Generated message map functions
		//{{AFX_MSG(CAmsDlg)
		virtual BOOL OnInitDialog();
		virtual void OnOK();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AMSDLG_H__61D22769_1C1D_4A9B_9776_54A94F711CE4__INCLUDED_)
