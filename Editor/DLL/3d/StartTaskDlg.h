#if !defined(AFX_STARTTASKDLG_H__781DC8EF_302E_45E8_A43A_4096F5E29A6E__INCLUDED_)
#define AFX_STARTTASKDLG_H__781DC8EF_302E_45E8_A43A_4096F5E29A6E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StartTaskDlg.h : header file
//

#include "3dApi.h"
#include "RoundButtons.h"
#include "Utils.h"
#include "Database.h"

namespace Foxjet3d
{
	bool _3D_API GetTestPHC (ULONG lAddress);
	void _3D_API SetTestPHC (ULONG lAddress, bool bUse);

	/////////////////////////////////////////////////////////////////////////////
	// CStartTaskDlg dialog

	class _3D_API CStartTaskDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CString m_strTaskName;
		ULONG m_lTaskID;
		CString m_sLineName;
		CStartTaskDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CStartTaskDlg)
		CListCtrl	m_TaskList;
		BOOL	m_bResetCounts;
		//}}AFX_DATA

		CString m_strRegKey;
		bool m_bContinuousCount;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CStartTaskDlg)
		public:
		virtual BOOL DestroyWindow();
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		FoxjetUtils::CRoundButtons m_buttons;

		void DeleteObjects (void);
		void FillCtrl ();

		ULONG m_lOldTaskID;
		CFont m_fnt;
		bool m_bTyping;
		CArray <FoxjetDatabase::LINESTRUCT, FoxjetDatabase::LINESTRUCT &> m_vLines;
		CString m_strTitle;
		CString m_strOriginalLine;

		afx_msg void OnSelectLine ();
		afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
		afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpmis);
		afx_msg void OnSelectLineRange (UINT nID);


		// Generated message map functions
		//{{AFX_MSG(CStartTaskDlg)
		virtual BOOL OnInitDialog();
		virtual void OnCancel();
		virtual void OnOK();
		afx_msg void OnClickTasklist(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnDblclkTasklist(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnItemchangedTasklist(NMHDR* pNMHDR, LRESULT* pResult);
		//}}AFX_MSG
		afx_msg void OnChangeTask ();

		DECLARE_MESSAGE_MAP()
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STARTTASKDLG_H__781DC8EF_302E_45E8_A43A_4096F5E29A6E__INCLUDED_)
