#include "stdafx.h"
#include "3d.h"
#include "Registry.h"
#include "Resource.h"
#include "Debug.h"
#include "AppVer.h"
#include "Export.h"
#include "OdbcRecordset.h"
#include "Serialize.h"
#include "FieldDefs.h"
#include "Extern.h"
#include "ImportFileDlg.h"
#include "Parse.h"
#include "BitmapElement.h"
#include "FileExt.h"
#include "Task.h"
#include "zlib.h"
#include "pugixml.hpp"
#include <map>

#ifdef __WINPRINTER__
	#include "BarcodeElement.h"
	#include "BarcodeParams.h"
#endif //__WINPRINTER__

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace Foxjet3d;

////////////////////////////////////////////////////////////////////////////////////////////////////

static const LPCTSTR lpszBITMAP = _T ("BITMAP");

static CString GetName (const LINESTRUCT & s)					{ return s.m_strName;				}
static CString GetName (const BOXSTRUCT & s)					{ return s.m_strName;				}
static CString GetName (const TASKSTRUCT & s)					{ return s.m_strName;				}
static CString GetName (const HEADSTRUCT & s)					{ return s.m_strName;				}
static CString GetName (const PANELSTRUCT & s)					{ return s.m_strName;				}
static CString GetName (const OPTIONSSTRUCT & s)				{ return s.m_strOptionDesc;			}
static CString GetName (const USERSTRUCT & s)					{ return s.m_strUserName;			}
static CString GetName (const SECURITYGROUPSTRUCT & s)			{ return s.m_strName;				}
static CString GetName (const SHIFTSTRUCT & s)					{ return s.m_strName;				}
static CString GetName (const DELETEDSTRUCT & s)				{ return _T ("");					}
static CString GetName (const BCSCANSTRUCT & s)					{ return _T ("");					}
static CString GetName (const BOXLINKSTRUCT & s)				{ return _T ("");					}
static CString GetName (const OPTIONSLINKSTRUCT & s)			{ return _T ("");					}
static CString GetName (const REPORTSTRUCT & s)					{ return _T ("");					}
static CString GetName (const SETTINGSSTRUCT & s)				{ return _T ("");					}
static CString GetName (const IMAGESTRUCT & s)					{ return _T ("");					}

////////////////////////////////////////////////////////////////////////////////////////////////////

static CString GetNameField (const LINESTRUCT & s)				{ return Lines::m_lpszName;			 }
static CString GetNameField (const BOXSTRUCT & s)				{ return Boxes::m_lpszName;			 }
static CString GetNameField (const TASKSTRUCT & s)				{ return Tasks::m_lpszName;			 }
static CString GetNameField (const HEADSTRUCT & s)				{ return Heads::m_lpszName;			 }
static CString GetNameField (const PANELSTRUCT & s)				{ return Panels::m_lpszName;		 }
static CString GetNameField (const OPTIONSSTRUCT & s)			{ return Options::m_lpszOptionDesc;	 }
static CString GetNameField (const USERSTRUCT & s)				{ return Users::m_lpszUserName;		 }
static CString GetNameField (const SECURITYGROUPSTRUCT & s)		{ return SecurityGroups::m_lpszName; }
static CString GetNameField (const SHIFTSTRUCT & s)				{ return Shifts::m_lpszName;		 }
static CString GetNameField (const DELETEDSTRUCT & s)			{ return _T ("");					 }
static CString GetNameField (const BCSCANSTRUCT & s)			{ return _T ("");					 }
static CString GetNameField (const BOXLINKSTRUCT & s)			{ return _T ("");					 }
static CString GetNameField (const OPTIONSLINKSTRUCT & s)		{ return _T ("");					 }
static CString GetNameField (const REPORTSTRUCT & s)			{ return _T ("");					 }
static CString GetNameField (const SETTINGSSTRUCT & s)			{ return _T ("");					 }
static CString GetNameField (const IMAGESTRUCT & s)				{ return _T ("");					 }

////////////////////////////////////////////////////////////////////////////////////////////////////

static void SetID (LINESTRUCT & s,			ULONG lID) { s.m_lID = lID; }
static void SetID (BOXSTRUCT & s,			ULONG lID) { s.m_lID = lID; }
static void SetID (HEADSTRUCT & s,			ULONG lID) { s.m_lID = lID; }
static void SetID (PANELSTRUCT & s,			ULONG lID) { s.m_lID = lID; }
static void SetID (OPTIONSSTRUCT & s,		ULONG lID) { s.m_lID = lID; }
static void SetID (USERSTRUCT & s,			ULONG lID) { s.m_lID = lID; }
static void SetID (SECURITYGROUPSTRUCT & s,	ULONG lID) { s.m_lID = lID; }
static void SetID (SHIFTSTRUCT & s,			ULONG lID) { s.m_lID = lID; }
static void SetID (DELETEDSTRUCT & s,		ULONG lID) { s.m_lID = lID; }
static void SetID (BCSCANSTRUCT & s,		ULONG lID) { s.m_lID = lID; }
static void SetID (TASKSTRUCT & s,			ULONG lID) { s.SetID (lID); }
static void SetID (BOXLINKSTRUCT & s,		ULONG lID) { }
static void SetID (OPTIONSLINKSTRUCT & s,	ULONG lID) { }
static void SetID (REPORTSTRUCT & s,		ULONG lID) { }
static void SetID (SETTINGSSTRUCT & s,		ULONG lID) { }
static void SetID (IMAGESTRUCT & s,			ULONG lID) { }

////////////////////////////////////////////////////////////////////////////////////////////////////

static void UpdateLinks (LINESTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew) { }
static void UpdateLinks (BOXSTRUCT & s,				LPCTSTR lpszField, ULONG lOld, ULONG lNew) { }
static void UpdateLinks (OPTIONSSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew) { }
static void UpdateLinks (SECURITYGROUPSTRUCT & s,	LPCTSTR lpszField, ULONG lOld, ULONG lNew) { }
static void UpdateLinks (SHIFTSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew) { }
static void UpdateLinks (REPORTSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew) { }
static void UpdateLinks (HEADSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew);
static void UpdateLinks (PANELSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew);
static void UpdateLinks (USERSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew);
static void UpdateLinks (DELETEDSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew);
static void UpdateLinks (BCSCANSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew);
static void UpdateLinks (BOXLINKSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew);
static void UpdateLinks (OPTIONSLINKSTRUCT & s,		LPCTSTR lpszField, ULONG lOld, ULONG lNew);
static void UpdateLinks (SETTINGSSTRUCT & s,		LPCTSTR lpszField, ULONG lOld, ULONG lNew);
static void UpdateLinks (IMAGESTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew);
static void UpdateLinks (TASKSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew);

////////////////////////////////////////////////////////////////////////////////////////////////////

CString RemapLogo (const CString & strPath, const CString & strDir);
CString RemapLogos (const CString & strMsg, const CString & strDir);

////////////////////////////////////////////////////////////////////////////////////////////////////

#define UPDATELINK(var, oldValue, newValue) \
	if ((var) == (oldValue)) \
		(var) = (newValue);

static void UpdateLinks (HEADSTRUCT & s, LPCTSTR lpszField, ULONG lOld, ULONG lNew)
{
	UPDATELINK (s.m_lPanelID, lOld, lNew)
}

static void UpdateLinks (PANELSTRUCT & s, LPCTSTR lpszField, ULONG lOld, ULONG lNew)
{
	UPDATELINK (s.m_lLineID, lOld, lNew)
}

static void UpdateLinks (USERSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew)
{
	UPDATELINK (s.m_lSecurityGroupID, lOld, lNew)
}

static void UpdateLinks (DELETEDSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew)
{
	UPDATELINK (s.m_lLineID, lOld, lNew)
}

static void UpdateLinks (BCSCANSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew)
{
	UPDATELINK (s.m_LineID, lOld, lNew)
}

static void UpdateLinks (BOXLINKSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew)
{
	if (!_tcscmp (lpszField, BoxToLine::m_lpszLineID))		UPDATELINK (s.m_lLineID, lOld, lNew)
	else if (!_tcscmp (lpszField, BoxToLine::m_lpszBoxID))	UPDATELINK (s.m_lBoxID, lOld, lNew)
}

static void UpdateLinks (OPTIONSLINKSTRUCT & s,		LPCTSTR lpszField, ULONG lOld, ULONG lNew)
{
	if (s.m_lGroupID == (long)lOld)
		s.m_lGroupID = (long)lNew;
}

static void UpdateLinks (SETTINGSSTRUCT & s,		LPCTSTR lpszField, ULONG lOld, ULONG lNew)
{
	UPDATELINK (s.m_lLineID, lOld, lNew)
}

static void UpdateLinks (IMAGESTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew)
{
	UPDATELINK (s.m_lBoxID, lOld, lNew)
}

static void UpdateLinks (TASKSTRUCT & s,			LPCTSTR lpszField, ULONG lOld, ULONG lNew)
{
	if (!_tcscmp (lpszField, Tasks::m_lpszLineID))			UPDATELINK (s.m_lLineID, lOld, lNew)
	else if (!_tcscmp (lpszField, Tasks::m_lpszBoxID))		UPDATELINK (s.m_lBoxID, lOld, lNew)
	else if (!_tcscmp (lpszField, Messages::m_lpszHeadID)) {
		//RESETIDS (Messages,		Messages::m_lpszHeadID,				HEADSTRUCT);
	}
	else if (!_tcscmp (lpszField, Messages::m_lpszTaskID)) {
		//RESETIDS (Messages,		Messages::m_lpszTaskID,				TASKSTRUCT);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template <class TYPE>
static int FindByName (CArray <TYPE, TYPE &> & v, const CString & strName)
{
	for (int i = 0; i < vTasks.GetSize (); i++) {
		CString str = GetName (v [i]);

		if (!str.CompareNoCase (strName))
			return i;
	}

	return -1;
}

template <class TYPE>
static int FindByID (CArray <TYPE, TYPE &> & v, ULONG lFindID)
{
	for (int i = 0; i < v.GetSize (); i++) {
		ULONG lID = GetID (v [i]);

		if (lFindID == lID)
			return i;
	}

	return -1;
}

template <class TYPE>
static ULONG GetMaxID (CArray <TYPE, TYPE &> & v)
{
	ULONG lMax = 1;

	for (int i = 0; i < v.GetSize (); i++)
		lMax = max ((ULONG)GetID (v [i]), lMax);

	return lMax;
}

template <class TYPE>
static ULONG GetIDFromName (const CString & strName, CArray <TYPE, TYPE &> & v)
{
	for (int i = 0; i < v.GetSize (); i++) {
		CString str = GetName (v [i]);

		if (!str.CompareNoCase (strName))
			return GetID (v [i]);
	}

	return FoxjetDatabase::NOTFOUND;
}

template <class TYPE>
static void UpdateIDs (FoxjetDatabase::COdbcDatabase & db, LPCTSTR lpszTable, 
					   CArray <TYPE, TYPE &> & v, CMap <ULONG, ULONG, ULONG, ULONG> & vID)
{
	if (!GetIDField (TYPE ()).GetLength () || !GetNameField (TYPE ()).GetLength ())
		return;

	CArray <TYPE, TYPE &> vOld;

	TRACEF ("UpdateIDs: " + CString (lpszTable) + " [" + GetIDField (TYPE ()) + ", " + GetNameField (TYPE ()) + "]");

	try
	{
		COdbcRecordset rst (db);
		CString strSQL;

		strSQL.Format (_T ("SELECT * FROM [%s]"), lpszTable);
		rst.Open (strSQL);

		while (!rst.IsEOF ()) {
			TYPE t;

			rst >> t;
			vOld.Add (t);
			rst.MoveNext ();
		}
	}
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }

	#ifdef _DEBUG
	CString str;

	str.Format ("%d records before import", vOld.GetSize ()); TRACEF (str);
	str.Format ("%d records parsed", v.GetSize ()); TRACEF (str);
	#endif

	const ULONG lMaxID = GetMaxID <TYPE> (vOld);
	ULONG lNextID = lMaxID + 1;

	for (int i = 0; i < v.GetSize (); i++) {
		TYPE & t = v [i];
		ULONG lID = GetIDFromName <TYPE> (GetName (t), vOld);
		bool bInUse = FindByID <TYPE> (vOld, GetID (t)) != -1;
		
		if (lID == FoxjetDatabase::NOTFOUND || lID < lMaxID || bInUse) {
			
			vID.SetAt (GetID (t), lNextID);
			SetID (t, lNextID); 
			lNextID++;

			#ifdef _DEBUG
			CString str = ToString (v [i]);
			
			TRACEF ("duplicate: " + str.Mid (0, min (80, str.GetLength ())));
			#endif
		}
		else {
			ULONG lExistingID = GetIDFromName <TYPE> (GetName (t), vOld);

			if (lID != FoxjetDatabase::NOTFOUND && lExistingID != FoxjetDatabase::NOTFOUND) {
				{ ULONG lTmp = 0; ASSERT (!vID.Lookup (GetID (t), lTmp)); }
				vID.SetAt (lExistingID, GetID (t));
			}

			#ifdef _DEBUG
			CString str = ToString (v [i]);
			
			TRACEF ("           " + str.Mid (0, min (80, str.GetLength ())));
			#endif
		}
	}

	#ifdef _DEBUG
	TRACEF ("");

	/*
	for (i = 0; i < v.GetSize (); i++) {
		CString str = ToString (v [i]);
		
		str = str.Mid (0, min (80, str.GetLength ()));
		TRACEF (str);
	}
	*/

	str.Format ("%d duplicated IDs", lNextID - (lMaxID + 1)); TRACEF (str);
	#endif

}

#define UPDATEIDS(t, lpszTable, db) UpdateIDs <t> (db, lpszTable, v##t, vID##t)

/*
template <class TYPE>
void ResetIDs (COdbcDatabase & db, CArray <TYPE, TYPE &> & vUpdate, 
			   LPCTSTR lpszTable, LPCTSTR lpszField, 
			   CMap <ULONG, ULONG, ULONG, ULONG> & vID, 
			   LPCTSTR lpszType, LPCTSTR lpszDependency)
{
	TRACEF (CString (lpszType) + ": " + CString (lpszField) + " --> " + CString (lpszDependency));

	for (POSITION pos = vID.GetStartPosition (); pos != NULL; ) {
		ULONG lOld = 0, lNew = 0;
		CString strSQL;
		
		vID.GetNextAssoc (pos, lOld, lNew);
		{ CString str; str.Format ("\t%d --> %d", lOld, lNew); TRACEF (str); }

		strSQL.Format (_T ("UPDATE [%s] SET [%s].[%s] = %u WHERE [%s].[%s]=%u;"),
			lpszTable,
			lpszTable, lpszField, 
			lNew,
			lpszTable, lpszField, 
			lOld);
		TRACEF (strSQL);
		db.ExecuteSQL (strSQL);

		int nIndex = FindByID <TYPE> (vUpdate, lOld);

		if (nIndex != -1) {
			TYPE & t = vUpdate [nIndex];

			UpdateLinks (t, lpszField, lOld, lNew);
		}
	}
}
#define RESETIDS(db, t, lpszTable, lpszField, id) ResetIDs <t> (db, v##t, lpszTable, lpszField, vID##id, #t, #id)
*/
template <class TYPE>
void ResetIDs (CArray <TYPE, TYPE &> & v, ULONG lOldID, ULONG lNewID)
{
	for (int i = 0; i < v.GetSize (); i++) {
		TYPE & t = v [i];

		if (v [i].m_lBoxID == lOldID) {
			//{ CString str; str.Format ("%d --> %d", v [i].m_lBoxID, lNewID); TRACEF (str); }
			t.m_lBoxID = lNewID;
			v.SetAt (i, t);
		}
	}
}
#define RESETIDS(t, lOldID, lNewID) ResetIDs <t> (v##t, lOldID, lNewID)

//////////////////////////////////////////////////////////////////////////////////////////

#define DEBUG_IMPORT

#define CHECKUISTATE() \
{ \
	::Sleep (1);  \
	\
	if (params.m_pDlg->IsCancelled ()) { \
		TRACEF (_T ("Cancelled")); \
		fclose (fp);  \
		return CImportFileDlg::IMPORTTHREAD_STOP;  \
	} \
}
			
static void SaveSetting (COdbcDatabase & database, const CString & strLike, CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> & v)
{
	CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> vTmp;

	GetSettingsRecords (database, 0, v);

	for (int i = 0; i < v.GetSize (); i++) {
		SETTINGSSTRUCT & s = v [i];

		if (s.m_strKey.Find (strLike) != -1) {
			bool bFound = false;

			TRACEF (s.m_strKey);

			for (int j = 0; !bFound && (j < v.GetSize ()); j++) {
				SETTINGSSTRUCT & s2 = v [j];

				if (!s.m_strKey.CompareNoCase (s2.m_strKey)) 
					bFound = true;
			}

			if (!bFound)
				vTmp.Add (s);
		}
	}

	v.Append (vTmp);
}


bool ParseBitmap (const CString & str, CMapStringToString & vBitmaps, const CString & strRemapDir)
{
#ifdef __WINPRINTER__
	using namespace FoxjetElements;
#else
	using namespace FoxjetIpElements;
#endif

	typedef struct tagZ64_STRUCT
	{
		tagZ64_STRUCT () : cx (0), cy (0), nBytesPerRow (0) { }

		int cx;
		int cy;
		int nBytesPerRow;
	} Z64_STRUCT;
	std::unique_ptr <Z64_STRUCT> pZ64;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszBITMAP)) {
		const CString strOriginalFile = GetParam (v, nIndex++);
		CString strMIME = GetParam (v, nIndex++);
		CString strFind;

		if (!vBitmaps.Lookup (strOriginalFile, strFind)) {
			const CString strDefFolder = CBitmapElement::GetDefPath ();
			CString strActualFile = strOriginalFile;
			bool bSaved = false;
			BYTE * p = NULL;
			int n = 0;

			strMIME.Replace (_T ("\n"), _T (""));
			strMIME.Replace (CString ((TCHAR)0x0a), _T (""));
			strMIME.Replace (CString ((TCHAR)0x0d), _T (""));
		
			CString strZ64 = Extract (str, _T (":Z64:"), _T (":"));

			if (strZ64.GetLength ()) {
				CStringArray v;

				Tokenize (UnformatString (strMIME), v);

				ASSERT (v.GetSize () >= 4);

				if (v.GetSize () >= 4) {
					pZ64.reset (new Z64_STRUCT);
					pZ64->cx = _ttoi (GetParam (v, 0));
					pZ64->cy = _ttoi (GetParam (v, 1));
					pZ64->nBytesPerRow = _ttoi (GetParam (v, 2));
					const int nBuffer = _ttoi (GetParam (v, 3));
					DWORD lUncompressed = nBuffer;
					UCHAR * pUncompressed = new UCHAR [nBuffer];

					n = strZ64.GetLength ();
					p = new BYTE [n];
					n = DecodeBase64 ((unsigned char *)(LPSTR)w2a (strZ64), p, n);

					memset (pUncompressed, 0, nBuffer);

					int nUncompress = uncompress (pUncompressed, &lUncompressed, p, n);
					ASSERT (nUncompress == Z_OK);

					delete [] p;
					p = pUncompressed;
				}
			}
			else {
				n = strMIME.GetLength ();
				p = new BYTE [n];
				n = DecodeBase64 ((unsigned char *)(LPSTR)w2a (strMIME), p, n);
			}

			TRACEF ("");
			ASSERT (p);
			ASSERT (n);

			for (int i = 0; i < 3 && !bSaved; i++) {
				CString strFolder = strActualFile;
				CString strTitle, strSuffix;

				//if (IsGojo ())
					if (strRemapDir.GetLength ())
						strActualFile = RemapLogo (strActualFile, strRemapDir);

				FoxjetFile::GetFileTitle (strActualFile, strFolder, strTitle, strSuffix);

				TRACEF (_T ("Attempting to write: [") + strFolder + _T ("] [") + strTitle + _T ("].[") + strSuffix + _T ("] [") + strActualFile + _T ("]"));

				if (pZ64) {
					CxImage img;

					if (img.CreateFromArray (p, pZ64->cx, pZ64->cy, 1, pZ64->nBytesPerRow, false)) {
						img.Negative ();
						VERIFY (bSaved = img.Save (strActualFile, CXIMAGE_FORMAT_BMP));
					}
				}
				else if (FILE * fp = _tfopen (strActualFile, _T ("wb"))) {
					fwrite (p, n, 1, fp);
					fclose (fp);

					#ifdef __IPPRINTER__
						CString strOld;
						CString strNew;
						
						strOld.Format (_T ("\"%s\""), FoxjetFile::GetFileTitle (strOriginalFile, strFolder, strTitle, strSuffix));
						strNew.Format (_T ("\"%s\""), strTitle);
						TRACEF (strOld + _T (" --> ") + strNew);
						vBitmaps.SetAt (strOld, strNew);
					#else
						vBitmaps.SetAt (strOriginalFile, strActualFile);
					#endif

					bSaved = true;
					break;
				}

				switch (i) {
				case 0:
					strActualFile.Format (_T ("%s%s.%s"), strDefFolder, strTitle, strSuffix);
					break;
				default:
					strActualFile = CreateTempFilename (strDefFolder, strTitle, strSuffix);
					break;
				}
			}

			ASSERT (bSaved);
			delete [] p;
		}

		return true;
	}

	return false; 
}

static int Total (const FoxjetCommon::CImportArray & v)
{
	int nResult = 0;

	for (int i = 0; i < v.GetSize (); i++) 
		nResult += v [i].m_nImported;

	return nResult;
}

int ImportPrd (const CImportFileDlg::IMPORTTHREADSTRUCT & params, LPCTSTR lpszFile)
{
	using namespace pugi;

	int nResult = 0;

	xml_document doc;
	xml_parse_result result = doc.load_file(lpszFile);

	if (result.status == status_ok) {
		xml_node root = doc.document_element();

		nResult = 1;
		TRACEF (lpszFile);
		TRACEF (std::string (std::string ("\t") + root.name ()).c_str ());

		for (xml_attribute attr = root.first_attribute (); !attr.empty (); attr = attr.next_attribute ()) {
			std::string s = std::string ("\t") + attr.name () + std::string ("=") + attr.value ();
			TRACEF (s.c_str ());
		}

		for (xml_node node = root.first_child(); !node.empty (); node = node.next_sibling ()) {
			TRACEF (std::string (std::string ("\t") + node.name ()).c_str ());

			for (xml_node x = node.first_child(); !x.empty (); x = x.next_sibling ()) {
				CString str = x.value ();
				str.Trim ();
				TRACEF (_T ("\t\t\"") + str + _T ("\""));
			}

			for (xml_attribute attr = node.first_attribute (); !attr.empty (); attr = attr.next_attribute ()) {
				std::string s = std::string ("\t\t") + attr.name () + std::string ("=\"") + attr.value () + std::string ("\"");
				TRACEF (s.c_str ());
			}
		}
	}

	return nResult;
}

int ImportPrds (const CImportFileDlg::IMPORTTHREADSTRUCT & params)
{
	CStringArray v;
	int nResult = 0;
	CString strConfig = params.m_strFile;

	strConfig.Replace (_T ("\\"), _T ("/"));

	std::vector <std::wstring> vConfig = explode ((std::wstring)(LPCTSTR)strConfig, '/');
	DWORD dw = ::GetFileAttributes (params.m_strFile);

	if (dw & FILE_ATTRIBUTE_DIRECTORY && dw != -1) {
		strConfig = std::wstring (implode (std::vector <std::wstring> (vConfig.begin (), vConfig.end () - 1), std::wstring (_T ("\\"))) + _T ("\\cfg\\setup.cfg")).c_str ();
		GetFiles (params.m_strFile, _T ("*.prd"), v);
	}
	else {
		strConfig = std::wstring (implode (std::vector <std::wstring> (vConfig.begin (), vConfig.end () - 2), std::wstring (_T ("\\"))) + _T ("\\cfg\\setup.cfg")).c_str ();
		v.Add (params.m_strFile);
	}

	TRACEF (strConfig);
	ImportPrd (params, strConfig);

	for (int i = 0; i < v.GetSize (); i++) {
		if (ImportPrd (params, v [i]))
			nResult++;
	}

	return nResult;
}

CString RemapLogo (const CString & strPath, const CString & strDir)
{
	CString str (strPath);
	CString strLocal = strDir;
	int nIndex = strPath.ReverseFind ('\\');

	if (nIndex != -1) {
		CString s = strPath.Left (nIndex);
		str.Replace (s, strLocal);
	}

	return str;
}

CString RemapLogos (const CString & strMsg, const CString & strDir)
{
	CString str (strMsg);
	CString strLocal = strDir;
	CStringArray vPackets;

	strLocal = FormatString (strLocal);
	FoxjetDatabase::ParsePackets (str, vPackets);

	for (int nElement = 0; nElement < vPackets.GetSize (); nElement++) {
		CString strElement = vPackets [nElement];
		const CString strOriginalElement = strElement;
		const CString strKey = _T ("Bitmap");

		if (strElement.GetLength () > strKey.GetLength () && !strElement.Left (strKey.GetLength ()).Compare (strKey)) {
			CStringArray v;

			Tokenize (strElement, v);

			if (v.GetSize () >= 9) {
				CString strPath = UnformatString (v [9]);
				int nIndex = strPath.ReverseFind ('\\');

				if (nIndex != -1) {
					strPath = FormatString (strPath.Left (nIndex));
					TRACEF (strElement);
					strElement.Replace (strPath, strLocal);
					TRACEF (strElement);
					str.Replace (strOriginalElement, strElement);
				}
			}
		}
	}

	return str;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

ULONG CALLBACK CImportFileDlg::Import (LPVOID lpData)
{
	ASSERT (lpData);

	IMPORTTHREADSTRUCT & params = * (IMPORTTHREADSTRUCT *)lpData;
	CMapStringToString vBitmaps;
	FoxjetCommon::CImportArray		& vResult = params.m_vResult;
	CMapStringToString				vBitmapFiles;
	CMapStringToString				vBitmapDirs;
	CMap <ULONG, ULONG, ULONG, ULONG> mapHeadID;

	if (params.m_strFile.GetLength ()) {
		int n = params.m_strFile.GetLength () - 1;

		if (params.m_strFile [n] == '\\' || params.m_strFile [n] == '/')
			params.m_strFile.Delete (n);
	}

	{
		DWORD dw = ::GetFileAttributes (params.m_strFile);
		CString str = params.m_strFile;
		CStringArray v;

		str.MakeLower ();

		if (dw & FILE_ATTRIBUTE_DIRECTORY && dw != -1) 
			GetFiles (str, _T ("*.yml"), v);
		else if (str.Find (_T (".yml")) != -1 || str.Find (_T (".yaml")) != -1) 
			v.Add (str);

		if (v.GetSize ()) {
			for (int i = 0; i < v.GetSize (); i++) {
				params.m_strFile = v [i];

				if (ImportYml (params)) {
					if (params.m_pDlg) {
						IMPORTSTRUCT r;
						CString strFile = v [i];
						strFile.Replace ('\\', '/');
						std::vector <std::wstring> vFile = explode (std::wstring (strFile), '/');

						if (vFile.size ())
							strFile = vFile [vFile.size () - 1].c_str ();

						r.m_nImported = r.m_nParsed = 1;
						r.m_strStruct = strFile;
						params.m_pDlg->SendMessage (CProgressDlg::m_wmAddResult, (WPARAM)new IMPORTSTRUCT (r), 0);
					}
				}
			}

			params.m_pDlg->PostMessage (CProgressDlg::m_wmEndProgress, IDOK, 0);

			return CImportFileDlg::IMPORTTHREAD_SUCCESS;
		}

		// TODO
		//if ((dw & FILE_ATTRIBUTE_DIRECTORY && dw != -1) || str.Find (_T (".prd")) != -1) {
		//	if (ImportPrds (params))
		//		return 0;
		//}
	}

	#ifdef __WINPRINTER__
	CString strBarcodeParam;

	{
		using namespace FoxjetElements;

		CBarcodeParams b;
		CStringArray v;

		FromString (b.ToString (::verApp), v);

		if (v.GetSize ())
			strBarcodeParam = v [0];
	}
	#endif //__WINPRINTER__

	#ifdef __IPPRINTER__
	FoxjetCommon::SetMkNextParams (FoxjetIpElements::CBitmapElement::GetDefPath (), FoxjetIpElements::CBitmapElement::m_strSuffix);
	#endif //__IPPRINTER__

	ASSERT (params.m_pDB);
	//ASSERT (params.m_pDlg);

	COdbcDatabase & database = * params.m_pDB;

	ASSERT (database.IsOpen ());

	const TCHAR cDelete [] = 
	{
		'\a', // Bell (alert) 
		'\b', // Backspace 
		'\f', // Formfeed 
		'\n', // New line 
		'\r', // Carriage return 
		'\t', // Horizontal tab 
		'\v', // Vertical tab 
	};

	if (params.m_pDlg) {
		while (!::IsWindow (params.m_pDlg->m_hWnd))
			::Sleep (100);

		while (!::IsWindowVisible (params.m_pDlg->m_hWnd))
			::Sleep (100);

		#if defined( _DEBUG ) && defined( DEBUG_IMPORT )
		params.m_pDlg->ShowWindow (SW_HIDE);
		#endif

		params.m_pDlg->PostMessage (CProgressDlg::m_wmStepProgress, IDS_PROCESSING, 0);
	}

	if (FILE * fp = _tfopen (params.m_strFile, _T ("r"))) {
		TCHAR sz [512];
		CString str;
		int nLine = 0;

		str.Format (LoadString (IDS_READINGFILE), params.m_strFile);

		DECLAREIMPORT (LINESTRUCT);
		DECLAREIMPORT (BOXSTRUCT);
		DECLAREIMPORT (BOXLINKSTRUCT);
		DECLAREIMPORT (TASKSTRUCT);	
		DECLAREIMPORT (HEADSTRUCT);	
		DECLAREIMPORT (PANELSTRUCT);
		DECLAREIMPORT (OPTIONSSTRUCT);
		DECLAREIMPORT (OPTIONSLINKSTRUCT);
		DECLAREIMPORT (USERSTRUCT);	
		DECLAREIMPORT (SECURITYGROUPSTRUCT);
		DECLAREIMPORT (SHIFTSTRUCT);
		DECLAREIMPORT (REPORTSTRUCT);
		DECLAREIMPORT (BCSCANSTRUCT);
		DECLAREIMPORT (DELETEDSTRUCT);
		DECLAREIMPORT (SETTINGSSTRUCT);
		DECLAREIMPORT (IMAGESTRUCT);
		DECLAREIMPORT (PRINTERSTRUCT);

		#ifdef __WINPRINTER__
		CArray <FoxjetElements::CBarcodeParams, FoxjetElements::CBarcodeParams &> vBcParams;
		#endif //__WINPRINTER__

		CString strBuffer;
		bool bGetMore = true, bProcessPacket = false;
		int nPrevLength = 0;
		int nSegment = 0;

		while (1) {
			int nStack = 0;

			if (bGetMore) {
				TCHAR sz [512] = { 0 };
			
				_fgetts (sz, ARRAYSIZE (sz) - 1, fp);
				int nLen = _tcslen (sz);

				nLine += CountChars (sz, '\n').GetSize ();

				if (sz [nLen - 1] == '\n' || sz [nLen - 1] == '\r')
					sz [nLen - 1] = 0;

				//TRACEF (sz);
				strBuffer += sz;
				bGetMore = false;
			}

			while (!bProcessPacket && !bGetMore) {
				TCHAR cPrev = 0;

				nPrevLength = strBuffer.GetLength ();

				for (int i = 0; i < strBuffer.GetLength (); i++) {
					TCHAR cCur = strBuffer [i];
					TCHAR cPrev = i > 0 ? strBuffer [i - 1] : 0;

					if (cPrev != '\\') {
						if (cCur == '{') {
							nStack++;
						}
						else if (cCur == '}') {
							if (--nStack <= 0) {
								int nIndex = i + 1;

								str = strBuffer.Left (nIndex);
								strBuffer.Delete (0, nIndex);
								bProcessPacket = true;
								bGetMore = strBuffer.GetLength () > 0 ? false : true;
								break;
							}
						}
					}
				}

				if (!bProcessPacket)
					if (strBuffer.GetLength () == nPrevLength)
						bGetMore = true;
			}

			if (bProcessPacket) {
				bProcessPacket = false;

				str.Trim ();

				if (str.GetLength ()) {
					TRACEF (_T ("(") + ToString (nLine) + _T ("): ") + ToString (str.GetLength ()) + _T (": ") + str + _T ("\n") + CString ('-', 80));

					if (params.m_pDlg) {
						params.m_pDlg->PostMessage (CProgressDlg::m_wmStepProgress, IDS_PROCESSINGLINE, nLine);
						CHECKUISTATE ();
					}

					for (int i = 0; i < ARRAYSIZE (cDelete); i++)
						str.Remove (cDelete [i]);

					if (params.m_pDlg) {
						#if defined( _DEBUG ) && defined( DEBUG_IMPORT )
						params.m_pDlg->ShowWindow (SW_HIDE);
						#endif
					}

					if (str.GetLength ()) {
						static const CString strBitmapToken = _T ("{") + CString (::lpszBITMAP) + _T (",");
						bool bResult = false;
	
						if (str.Find (strBitmapToken) != -1)
							if (!bResult) bResult = ParseBitmap (str, vBitmaps, params.m_strLogosDir);
				

						if (!bResult) bResult = PARSERECORD (LINESTRUCT,			str);
						if (!bResult) bResult = PARSERECORD (BOXSTRUCT,				str);
						if (!bResult) bResult = PARSERECORD (BOXLINKSTRUCT,			str);
						if (!bResult) bResult = PARSERECORD (TASKSTRUCT,			str);	
						if (!bResult) bResult = PARSERECORD (HEADSTRUCT,			str);	
						if (!bResult) bResult = PARSERECORD (PANELSTRUCT,			str);
						if (!bResult) bResult = PARSERECORD (OPTIONSSTRUCT,			str);
						if (!bResult) bResult = PARSERECORD (OPTIONSLINKSTRUCT,		str);
						if (!bResult) bResult = PARSERECORD (USERSTRUCT,			str);	
						if (!bResult) bResult = PARSERECORD (SECURITYGROUPSTRUCT,	str);
						if (!bResult) bResult = PARSERECORD (SHIFTSTRUCT,			str);
						if (!bResult) bResult = PARSERECORD (REPORTSTRUCT,			str);
						if (!bResult) bResult = PARSERECORD (BCSCANSTRUCT,			str);
						if (!bResult) bResult = PARSERECORD (DELETEDSTRUCT,			str);
						if (!bResult) bResult = PARSERECORD (SETTINGSSTRUCT,		str);
						if (!bResult) bResult = PARSERECORD (IMAGESTRUCT,			str);

						if (!bResult)	
							if (params.m_dwFlags & IMPORT_PRINTERSTRUCT)
								if (database.HasTable (Printers::m_lpszTable))
									bResult = PARSERECORD (PRINTERSTRUCT, str); 

						#ifdef __WINPRINTER__
						if (!bResult) {
							if (!_tcsnicmp (strBarcodeParam, str, strBarcodeParam.GetLength ())) {
								using namespace FoxjetElements;

								CArray <CBarcodeParams, CBarcodeParams &> v;

								CBarcodeParams::FromString (v, str, CVersion ());

								if (v.GetSize ()) {
									vBcParams.Append (v);
									bResult = true;
								}
							}
						}
						#endif //__WINPRINTER__

						if (!bResult) {
							CString strIgnore [] = 
							{
								_T ("{DSN,"),
								_T ("{BitmapSummary:"),
							};
							bool bIgnore = false;

							for (int i = 0; !bIgnore && i < ARRAYSIZE (strIgnore); i++) 
								bIgnore = (!_tcsnicmp (strIgnore [i], str, strIgnore [i].GetLength ())) ? true : false;

							if (!bIgnore) {
								CString strMsg;

								strMsg.Format (_T ("Failed to parse\n\t%s (%d)\n"), params.m_strFile, nLine);
								TRACEF (strMsg + str + _T ("\n") + CString ('-', 80));
							}
						}
					}
				}
			}

			if (feof (fp))
				break;
		}

		fclose (fp);

		
		COUNTPARSED (LINESTRUCT,			/* params.m_ */ vResult);
		COUNTPARSED (BOXSTRUCT,				/* params.m_ */ vResult);
		COUNTPARSED (BOXLINKSTRUCT,			/* params.m_ */ vResult);
		COUNTPARSED (TASKSTRUCT,			/* params.m_ */ vResult);
		COUNTPARSED (HEADSTRUCT,			/* params.m_ */ vResult);
		COUNTPARSED (PANELSTRUCT,			/* params.m_ */ vResult);
		COUNTPARSED (OPTIONSSTRUCT,			/* params.m_ */ vResult);
		COUNTPARSED (OPTIONSLINKSTRUCT,		/* params.m_ */ vResult);
		COUNTPARSED (USERSTRUCT,			/* params.m_ */ vResult);
		COUNTPARSED (SECURITYGROUPSTRUCT,	/* params.m_ */ vResult);
		COUNTPARSED (SHIFTSTRUCT,			/* params.m_ */ vResult);
		COUNTPARSED (REPORTSTRUCT,			/* params.m_ */ vResult);
		COUNTPARSED (BCSCANSTRUCT,			/* params.m_ */ vResult);
		COUNTPARSED (DELETEDSTRUCT,			/* params.m_ */ vResult);
		COUNTPARSED (SETTINGSSTRUCT,		/* params.m_ */ vResult);
		COUNTPARSED (IMAGESTRUCT,			/* params.m_ */ vResult);
		COUNTPARSED (PRINTERSTRUCT,			/* params.m_ */ vResult);

		#ifdef __WINPRINTER__
		if (vBcParams.GetSize ()) {
			using namespace FoxjetElements;
			int nResolution = 150;

			IMPORTSTRUCT r;

			r.m_nImported = r.m_nParsed = 0;
			r.m_strStruct = LoadString (IDS_BARCODEPARAMS);
			r.m_strStruct.Replace (_T ("&"), _T (""));

			for (int i = 0; i < vBcParams.GetSize (); i++) {
				CBarcodeParams & b = vBcParams [i];
				ULONG lLineID = b.GetLineID ();
				const HEADTYPE type = GRAPHICS_768_256;

				VERIFY (SetBarcodeParam (lLineID, b.GetSymbology (), type, nResolution, b.GetID (), b, database));
				SaveBarcodeParams (database, lLineID, b.GetSymbology (), type, nResolution);
				r.m_nImported++;
			}

			if (!Total (vResult)) {
				r.m_nParsed = r.m_nImported;				

				if (params.m_pDlg) {
					params.m_pDlg->SendMessage (CProgressDlg::m_wmAddResult, (WPARAM)new IMPORTSTRUCT (r), 0);
					params.m_pDlg->PostMessage (CProgressDlg::m_wmEndProgress, IDOK, 0);
				}

				::Sleep (1000);
				return CImportFileDlg::IMPORTTHREAD_SUCCESS;
			}
		}
		#endif //__WINPRINTER__

		if ((params.m_dwFlags & IMPORT_TASKS) && !(params.m_dwFlags & IMPORT_CONFIG)) { // the "fred" check
			CArray <HEADSTRUCT, HEADSTRUCT &> v;
			/*
			bool bConfigMatched = true;

			GetHeadRecords (database, v);

			for (int i = 0; i < vHEADSTRUCT.GetSize (); i++) {
				HEADSTRUCT & h = vHEADSTRUCT [i];
				bool bFound = false;

				for (int j = 0; !bFound && (j < v.GetSize ()); j++) {
					HEADSTRUCT & h2 = v [j];

					if (h.m_lID == h2.m_lID) {
						if (h.GetChannels () != h2.GetChannels ()) 
							bConfigMatched = false;

						bFound = true;
					}
				}

				if (!bFound) 
					bConfigMatched = false;
			}

			if (!bConfigMatched) {
				if (params.m_pDlg) {
					int n = params.m_pDlg->SendMessage (CProgressDlg::m_wmMsgBox, (WPARAM)new CString (LoadString (IDS_DISSIMILARCONFIG)), MB_ICONQUESTION | MB_YESNO);

					if (n == IDNO) {
						params.m_pDlg->PostMessage (CProgressDlg::m_wmEndProgress, IDCANCEL, 0);
						return CImportFileDlg::IMPORTTHREAD_FAILED;
					}
				}
			}
			*/
		}

		if (params.m_dwFlags & IMPORT_TASKS) {
			/*
			std::vector <ULONG> vLineIDs;
			CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> vBcParams;

			for (int i = 0; i < vTASKSTRUCT.GetSize (); i++) {
				if (Find (vLineIDs, vTASKSTRUCT [i].m_lLineID) == -1)
					vLineIDs.push_back (vTASKSTRUCT [i].m_lLineID);
			}

			for (int i = vSETTINGSSTRUCT.GetSize () - 1; i >= 0; i--) {
				if (Find (vLineIDs, vSETTINGSSTRUCT [i].m_lLineID) != -1) {
					CString s = vSETTINGSSTRUCT [i].m_strKey;

					s.MakeLower ();

					if (s.Find (_T ("barcode")) != -1) {
						TRACEF (vSETTINGSSTRUCT [i].m_strKey);
						vBcParams.Add (vSETTINGSSTRUCT [i]);
						vSETTINGSSTRUCT.RemoveAt (i);
					}
				}
			}

			if (vBcParams.GetSize ()) {
				//IMPORTRECORDS (database, SETTINGSSTRUCT,		Settings::m_lpszTable,	/* params.m_ * / vResult);
				int nUpdate = UpdateRecords <SETTINGSSTRUCT> (database, vBcParams, Settings::m_lpszTable, false, &vResult);
				Update <SETTINGSSTRUCT> (vBcParams, nUpdate, Settings::m_lpszTable, vResult); 
			}
			*/

			for (int i = 0; i < vBOXSTRUCT.GetSize (); i++) {
				BOXSTRUCT & box = vBOXSTRUCT [i];
				ULONG lID = GetBoxID (database, box.m_strName);

				if (lID != NOTFOUND && box.m_lID != lID) {
					RESETIDS (BOXLINKSTRUCT,	box.m_lID, lID);
					RESETIDS (IMAGESTRUCT,		box.m_lID, lID);
					RESETIDS (TASKSTRUCT,		box.m_lID, lID);

					box.m_lID = lID;
					vBOXSTRUCT.SetAt (i, box);
				}
			}

			if (!(params.m_dwFlags & IMPORT_TASKSDELETEEXISTING)) {
				// corresponds to calls to OVERWRITERECORDS [UpdateRecords (bDeleteAllExisting = true)]
				//UPDATEIDS (BOXSTRUCT,			Boxes::m_lpszTable,				database);
				//UPDATEIDS (TASKSTRUCT,			Tasks::m_lpszTable,			database);
			}
		}

		if (params.m_pDlg) {
			CHECKUISTATE ();
			params.m_pDlg->PostMessage (CProgressDlg::m_wmStepProgress, IDS_READINGCONFIG, 0);
		}

		// order matters here

		if (params.m_dwFlags & IMPORT_CONFIG) {
			IMPORTRECORDS (database, LINESTRUCT,			Lines::m_lpszTable,		/* params.m_ */ vResult);
			IMPORTRECORDS (database, PANELSTRUCT,			Panels::m_lpszTable,	/* params.m_ */ vResult);
			IMPORTRECORDS (database, HEADSTRUCT,			Heads::m_lpszTable,		/* params.m_ */ vResult);	
			IMPORTRECORDS (database, IMAGESTRUCT,			Images::m_lpszTable,	/* params.m_ */ vResult);

			if (database.HasTable (Printers::m_lpszTable))
				if (params.m_dwFlags & IMPORT_PRINTERSTRUCT) IMPORTRECORDS (database, PRINTERSTRUCT, Printers::m_lpszTable, /* params.m_ */ vResult); 

			SaveSetting (database, _T ("Control::Error::"), vSETTINGSSTRUCT);

			{
				for (int nSetting = 0; nSetting < vSETTINGSSTRUCT.GetSize (); nSetting++) {
					SETTINGSSTRUCT & s = vSETTINGSSTRUCT [nSetting];

					if (s.m_strData.Find (_T ("{BarcodeParam,")) != -1) {
						CStringArray vParams;

						ParsePackets (s.m_strData, vParams);

						for (int nParam = 0; nParam < vParams.GetSize (); nParam++) {
							CString strParam = _T ("{") +  vParams [nParam] + _T ("}");
							const int nSymbologyIndex = 1;
							CLongArray v = CountChars (strParam, ',');

							TRACEF (s.m_strKey + _T (": ") + s.m_strData);
							TRACEF (strParam);

							if (v.GetSize () > (nSymbologyIndex + 1)) {
								int nID = _ttoi (strParam.Mid (v [0] + 1, (v [1] - v [0]) - 1));
								const CString strOld = Extract (strParam, _T ("{BarcodeParam,"), _T ("}"), true);
								const int nBegin = v [nSymbologyIndex];
								const int nEnd = v [nSymbologyIndex + 1];
								const CString strSymbology = strOld.Mid (nBegin + 1, (nEnd - nBegin) - 1);
								const int nSymbology = _ttoi (strSymbology);

								if (nSymbology == 17) {
									CString strLeft = strOld.Left (nBegin);
									CString strRight = strOld.Mid (nEnd);
									const CString strNew = strLeft + _T (",") + ToString (nSymbology + 1) + strRight;

									//TRACEF (strLeft);
									//TRACEF (strRight);
									TRACEF (strOld);
									TRACEF (strNew);

									VERIFY (s.m_strData.Replace (strOld, strNew));
								}
							}
						}
					}
				}
			}

			IMPORTRECORDS (database, SETTINGSSTRUCT,		Settings::m_lpszTable,	/* params.m_ */ vResult);
		}

		{ // remap parsed heads to existing heads, then set [Messages].[HeadID]
			CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
			int nMatched = 0;
			ULONG lMasterID = GetMasterPrinterID (database);

			GetPrinterHeads (database, lMasterID, vHeads);

			if (!vHEADSTRUCT.GetSize ())
				vHEADSTRUCT.Append (vHeads);

			if (!vHEADSTRUCT.GetSize ()) 
				GetPrinterHeads (database, ALL, vHEADSTRUCT);

			ASSERT (vHEADSTRUCT.GetSize ());

			for (int i = 0; i < vHEADSTRUCT.GetSize (); i++) {
				HEADSTRUCT & headImported = vHEADSTRUCT [i];
				ULONG lAddrImported = _tcstoul (headImported.m_strUID, NULL, 16);

				TRACEF (ToString (lAddrImported) + _T (": ") + headImported.m_strUID); 

				for (int j = 0; j < vHeads.GetSize (); j++) {
					HEADSTRUCT & headExisting = vHeads [j];
					ULONG lAddrExisting = _tcstoul (headExisting.m_strUID, NULL, 16);

					if (GetSynonymPhcAddr (lAddrImported) == GetSynonymPhcAddr (lAddrExisting)) {
						mapHeadID.SetAt (headImported.m_lID, headExisting.m_lID);
						nMatched++;
						break;
					}
				}
			}

			for (POSITION pos = mapHeadID.GetStartPosition (); pos; ) {
				ULONG lImportedID = -1, lExistingID = -1;

				mapHeadID.GetNextAssoc (pos, lImportedID, lExistingID);

				TRACEF (ToString (lImportedID) + _T (" --> ") + ToString (lExistingID));
			}

			if (nMatched > vHeads.GetSize ()) {
				if (params.m_pDlg) {
					int n = params.m_pDlg->SendMessage (CProgressDlg::m_wmMsgBox, (WPARAM)new CString (LoadString (IDS_DISSIMILARCONFIG)), MB_ICONQUESTION | MB_YESNO);

					if (n == IDNO) {
						params.m_pDlg->PostMessage (CProgressDlg::m_wmEndProgress, IDCANCEL, 0);
						return CImportFileDlg::IMPORTTHREAD_FAILED;
					}

				}

				//return CImportFileDlg::IMPORTTHREAD_FAILED;
			}
		}

		if (params.m_pDlg) {
			CHECKUISTATE ();
			params.m_pDlg->PostMessage (CProgressDlg::m_wmStepProgress, IDS_READINGTASKS, 0);
		}

		if (params.m_dwFlags & IMPORT_TASKS) {
			if (params.m_dwFlags & IMPORT_TASKSDELETEEXISTING) {
				IMPORTRECORDS (database, BOXSTRUCT,			Boxes::m_lpszTable,		/* params.m_ */ vResult);
				IMPORTRECORDS (database, BOXLINKSTRUCT,		BoxToLine::m_lpszTable,	/* params.m_ */ vResult);
			}
			else {
				OVERWRITERECORDS (database, BOXSTRUCT, /* params.m_ */ vResult);
			}
		}

		if (params.m_pDlg) {
			CHECKUISTATE ();
			params.m_pDlg->PostMessage (CProgressDlg::m_wmStepProgress, IDS_READINGCONFIG, 0);
		}

		if (params.m_dwFlags & IMPORT_CONFIG) {
			IMPORTRECORDS (database, SECURITYGROUPSTRUCT,	SecurityGroups::m_lpszTable,	/* params.m_ */ vResult);
			IMPORTRECORDS (database, USERSTRUCT,			Users::m_lpszTable,				/* params.m_ */ vResult);	
			IMPORTRECORDS (database, OPTIONSSTRUCT,			Options::m_lpszTable,			/* params.m_ */ vResult);
			IMPORTRECORDS (database, OPTIONSLINKSTRUCT,		OptionsToGroup::m_lpszTable,	/* params.m_ */ vResult);

			IMPORTRECORDS (database, SHIFTSTRUCT,			Shifts::m_lpszTable,			/* params.m_ */ vResult);
			IMPORTRECORDS (database, REPORTSTRUCT,			Reports::m_lpszTable,			/* params.m_ */ vResult);
 			IMPORTRECORDS (database, BCSCANSTRUCT,			BarcodeScans::m_lpszTable,		/* params.m_ */ vResult);
			IMPORTRECORDS (database, DELETEDSTRUCT,			Deleted::m_lpszTable,			/* params.m_ */ vResult);
		}

		if (params.m_pDlg) {
			CHECKUISTATE ();
			params.m_pDlg->PostMessage (CProgressDlg::m_wmStepProgress, IDS_READINGCONFIG, 0);
		}

		if (params.m_dwFlags & IMPORT_TASKS) {
			#ifdef __WINPRINTER__

			using namespace FoxjetElements;

			std::map <ULONG, ULONG> mapLines;
			std::vector <ULONG> vLineIDs;
			CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> vBcParams;

			{
				std::vector <ULONG> vImported, vExisting;
				CArray <LINESTRUCT, LINESTRUCT &> v;

				GetPrinterLines (database, GetPrinterID (database), v);

				for (int i = 0; i < v.GetSize (); i++)
					if (Find (vExisting, v [i].m_lID) == -1)
						InsertAscending (vExisting, v [i].m_lID);

				for (int nTask = 0; nTask < vTASKSTRUCT.GetSize (); nTask++) {
					TASKSTRUCT & task = vTASKSTRUCT [nTask];

					if (Find (vImported, task.m_lLineID) == -1)
						InsertAscending (vImported, task.m_lLineID);
				}

				for (int i = 0; i < min (vExisting.size (), vImported.size ()); i++)
					mapLines [vImported [i]] = vExisting [i];
			}

			for (int nTask = 0; nTask < vTASKSTRUCT.GetSize (); nTask++) {
				std::map <ULONG, ULONG> mapHeads;
				TASKSTRUCT & task = vTASKSTRUCT [nTask];

				task.m_lID = GetNextID (database, Tasks::m_lpszTable, Tasks::m_lpszID) + (nTask + 1);
				TRACEF (task.m_strName + _T (": ") + ToString (task.m_lID));

				{
					LINESTRUCT line;

					if (!GetLineRecord (database, task.m_lLineID, line)) {
						const ULONG lOld = task.m_lLineID;

						TRACEF (ToString (task.m_lLineID));
						task.m_lLineID = mapLines [task.m_lLineID];
						TRACEF (ToString (task.m_lLineID));

						const ULONG lNew = task.m_lLineID;

						// remap barcode settings
						for (int i = vSETTINGSSTRUCT.GetSize () - 1; i >= 0; i--) {
						
							if (vSETTINGSSTRUCT [i].m_lLineID == lOld) {
								CString strKey = vSETTINGSSTRUCT [i].m_strKey;

								strKey.MakeLower ();

								if (strKey.Find (_T ("barcode")) != -1) {
									SETTINGSSTRUCT s = vSETTINGSSTRUCT [i];
									s.m_lLineID = lNew;
									vBcParams.Add (s);
									vSETTINGSSTRUCT.RemoveAt (i);
								}
							}
						}
					}
					else {
						ULONG lID = GetTaskID (database, task.m_strName, line.m_strName, GetMasterPrinterID (database));

						if (lID != NOTFOUND) {
							if (lID != task.m_lID) {
								TRACEF (task.m_strName + _T (" --> ") + ToString (lID));
								task.m_lID = lID;
							}
						}
					}

					if (Find (vLineIDs, task.m_lLineID) == -1) 
						vLineIDs.push_back (task.m_lLineID);
				}

				{
					for (int i = vSETTINGSSTRUCT.GetSize () - 1; i >= 0; i--) {
						if (Find (vLineIDs, vSETTINGSSTRUCT [i].m_lLineID) != -1) {
							CString strKey = vSETTINGSSTRUCT [i].m_strKey;

							strKey.MakeLower ();

							if (strKey.Find (_T ("barcode")) != -1) {
								TRACEF (vSETTINGSSTRUCT [i].m_strKey);
								vBcParams.Add (vSETTINGSSTRUCT [i]);
								vSETTINGSSTRUCT.RemoveAt (i);
							}
						}
					}
				}

				{
					std::vector <ULONG> vImported, vExisting;
					CArray <HEADSTRUCT, HEADSTRUCT &> v;

					GetLineHeads (database, task.m_lLineID, v);

					for (int i = 0; i < v.GetSize (); i++)
						if (Find (vExisting, v [i].m_lID) == -1)
							InsertAscending (vExisting, v [i].m_lID);

					for (int i = 0; i < task.m_vMsgs.GetSize (); i++) {
						ULONG lHeadID = task.m_vMsgs [i].m_lHeadID;

						if (Find (vImported, lHeadID) == -1)
							InsertAscending (vImported, lHeadID);
					}

					for (int i = 0; i < min (vExisting.size (), vImported.size ()); i++)
						mapHeads [vImported [i]] = vExisting [i];
				}

				for (int nMsg = 0; nMsg < task.m_vMsgs.GetSize (); nMsg++) {
					MESSAGESTRUCT & msg = task.m_vMsgs [nMsg];
					CStringArray vPackets;
					ULONG lExistingID = -1;

					msg.m_lTaskID = task.m_lID;
					TRACEF (ToString (msg.m_lHeadID));

					if (mapHeadID.Lookup (msg.m_lHeadID, lExistingID)) 
						msg.m_lHeadID = lExistingID;
					else {
						msg.m_lHeadID = mapHeads [msg.m_lHeadID];

						if (!msg.m_lHeadID) {
							TRACEF (_T ("NOT MAPPED: ") + ToString (msg.m_lHeadID));
							ASSERT (0);
						}
					}

					msg.m_strName = task.m_strName;
					TRACEF (msg.m_strData);
					ParsePackets (msg.m_strData, vPackets);

					/*
					if ((ver >= CVersion (3,41,894,0,16) && ver <= CVersion (3,41,894,0,24)) ||
						(ver >= CVersion (3,72,895,0,13) && ver <= CVersion (3,72,895,0,19)) ||
						(ver >= CVersion (3,72,897,0,1)  && ver <= CVersion (3,72,895,0,5))) 
					// ver >= 3.77.0911.006
					// ver >= 3.75.0908.016
					*/

					for (int nElement = 0; nElement < vPackets.GetSize (); nElement++) {
						CString strElement = vPackets [nElement];
						const CString strKey = _T ("Barcode");

						if (strElement.GetLength () > strKey.GetLength () && !strElement.Left (strKey.GetLength ()).Compare (strKey)) {
							CStringArray v;

							Tokenize (strElement, v);

							if (v.GetSize () > 2) {
								UINT nID = _ttoi (v [1]); 
								const CString strOld = Extract (msg.m_strData, _T ("{Barcode,") + ToString ((int)nID), _T ("}}"), true);
								CLongArray v = CountChars (strOld, ',');

								if (v.GetSize () > 7) {
									const int nSymbologyIndex = 6;
									const int nBegin = v [nSymbologyIndex];
									const int nEnd = v [nSymbologyIndex + 1];
									const CString strSymbology = strOld.Mid (nBegin + 1, (nEnd - nBegin) - 1);
									const int nSymbology = _ttoi (strSymbology);

									if (nSymbology == 17) {
										CString strLeft = strOld.Left (nBegin);
										CString strRight = strOld.Mid (nEnd);
										const CString strNew = strLeft + _T (",") + ToString (nSymbology + 1) + strRight;

										//TRACEF (strLeft);
										//TRACEF (strRight);
										TRACEF (strOld);
										TRACEF (strNew);

										VERIFY (msg.m_strData.Replace (strOld, strNew));
									}
								}
							}
						}
					}
				}

				if (vBcParams.GetSize ()) {
					//IMPORTRECORDS (database, SETTINGSSTRUCT,		Settings::m_lpszTable,	/* params.m_ * / vResult);
					int nUpdate = UpdateRecords <SETTINGSSTRUCT> (database, vBcParams, Settings::m_lpszTable, false, &vResult);
					Update <SETTINGSSTRUCT> (vBcParams, nUpdate, Settings::m_lpszTable, vResult); 
				}
			}
			#endif //__WINPRINTER__

			//if (IsGojo ()) 
			{
				if (params.m_strLogosDir.GetLength ()) {
					for (int nTask = 0; nTask < vTASKSTRUCT.GetSize (); nTask++) {
						for (int nMsg = 0; nMsg < vTASKSTRUCT [nTask].m_vMsgs.GetSize (); nMsg++) {
							vTASKSTRUCT [nTask].m_vMsgs [nMsg].m_strData = RemapLogos (vTASKSTRUCT [nTask].m_vMsgs [nMsg].m_strData, params.m_strLogosDir);
						}
					}
				}
			}

			if (params.m_dwFlags & IMPORT_TASKSDELETEEXISTING) {
				IMPORTRECORDS (database, TASKSTRUCT,		Tasks::m_lpszTable,		/* params.m_ */ vResult);
			}
			else {
				OVERWRITERECORDS (database, TASKSTRUCT, /* params.m_ */ vResult);

				//#ifdef _DEBUG
				{
					ULONG lMasterID = GetMasterPrinterID (database);

					for (int nTask = 0; nTask < vTASKSTRUCT.GetSize (); nTask++) {
						TASKSTRUCT taskDB;
						TASKSTRUCT & task = vTASKSTRUCT [nTask];
						LINESTRUCT line;

						GetLineRecord (database, task.m_lLineID, line);
						GetTaskRecord (database, task.m_lID, taskDB);

						ULONG lTaskID = GetTaskID (database, task.m_strName, line.m_strName, lMasterID);
						
						if (lTaskID == NOTFOUND) {
							for (int i = 0; i < vResult.GetSize (); i++) {
								if (!vResult [i].m_strStruct.CompareNoCase (_T ("TASKSTRUCT"))) {
									IMPORTSTRUCT s = vResult [i];

									s.m_nImported--;
									vResult.SetAt (i, s);
									break;
								}
							}
						}
						else {
							TRACEF (ToString (task.m_lID) + _T (": ") + task.m_strName);
							VERIFY (lTaskID == task.m_lID);
							VERIFY (taskDB.m_vMsgs.GetSize () == task.m_vMsgs.GetSize ());

							if (taskDB.m_vMsgs.GetSize () == task.m_vMsgs.GetSize ()) {
								for (int nMsg = 0; nMsg < task.m_vMsgs.GetSize (); nMsg++) {
									MESSAGESTRUCT & msgDB = taskDB.m_vMsgs [nMsg];
									MESSAGESTRUCT & msg = task.m_vMsgs [nMsg];

									VERIFY (msgDB.m_lID == msg.m_lID);
								}
							}
						}
					}
				}
				//#endif
			}

			GetBitmapSummary (vTASKSTRUCT, /* params.m_ */ vBitmapFiles, /* params.m_ */ vBitmapDirs);
		}

		{
			CArray <PRINTERSTRUCT, PRINTERSTRUCT &> vPrinters;
			CArray <LINESTRUCT, LINESTRUCT &> vLines;

			GetPrinterRecords (database, vPrinters);
			GetLineRecords (database, vLines);

			if (vPrinters.GetSize () >= 1) {
				PRINTERSTRUCT & printer = vPrinters [0];

				for (int i = 0; i < vLines.GetSize (); i++) {
					LINESTRUCT & line = vLines [i];
					PRINTERSTRUCT tmp;

					if (!GetPrinterRecord (database, line.m_lPrinterID, tmp)) {
						line.m_lPrinterID = printer.m_lID;
						VERIFY (UpdateLineRecord (database, line));
					}
				}
			}
		}


		try 
		{
			/* 
			//these are also dependencies, but no need to update because of 
			//IMPORTRECORDS [UpdateRecords (bDeleteAllExisting = false)]

			RESETIDS (BCSCANSTRUCT,			BarcodeScans::m_lpszTable,		BarcodeScans::m_lpszLineID,			LINESTRUCT);
			RESETIDS (BOXLINKSTRUCT,		BoxToLine::m_lpszTable,			BoxToLine::m_lpszLineID,			LINESTRUCT);
				RESETIDS (BOXLINKSTRUCT,	BoxToLine::m_lpszTable,			BoxToLine::m_lpszBoxID,				BOXSTRUCT);
			RESETIDS (DELETEDSTRUCT,		Deleted::m_lpszTable,			Deleted::m_lpszLineID,				LINESTRUCT);
			RESETIDS (HEADSTRUCT,			Heads::m_lpszTable,				Heads::m_lpszPanelID,				PANELSTRUCT);
			RESETIDS (IMAGESTRUCT,			Images::m_lpszTable,			Images::m_lpszBoxID,				BOXSTRUCT);
			RESETIDS (OPTIONSLINKSTRUCT,	OptionsToGroup::m_lpszTable,	OptionsToGroup::m_lpszGroupID,		SECURITYGROUPSTRUCT);
			RESETIDS (PANELSTRUCT,			Panels::m_lpszTable,			Panels::m_lpszLineID,				LINESTRUCT);
			RESETIDS (SETTINGSSTRUCT,		Settings::m_lpszTable,			Settings::m_lpszLineID,				LINESTRUCT);
			RESETIDS (TASKSTRUCT,			Tasks::m_lpszTable,				Tasks::m_lpszLineID,				LINESTRUCT);
				RESETIDS (TASKSTRUCT,		Tasks::m_lpszTable,				Tasks::m_lpszBoxID,					BOXSTRUCT);
			RESETIDS (USERSTRUCT,			Users::m_lpszTable,				Users::m_lpszSecurityGroupID,		SECURITYGROUPSTRUCT);
			*/
		
			//RESETIDS (database, BOXLINKSTRUCT,	BoxToLine::m_lpszTable, BoxToLine::m_lpszBoxID,		BOXSTRUCT);
			//RESETIDS (database, IMAGESTRUCT,	Images::m_lpszTable,	Images::m_lpszBoxID,		BOXSTRUCT);
			//RESETIDS (database, TASKSTRUCT,		Tasks::m_lpszTable,		Tasks::m_lpszBoxID,			BOXSTRUCT);
		}
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }

		{
			int nPending = 0, nTotal = 0;

			for (POSITION pos = vBitmaps.GetStartPosition (); pos; ) {
				CString strOld, strNew;

				vBitmaps.GetNextAssoc (pos, strOld, strNew);

				if (strOld.CompareNoCase (strNew) != 0) {
					TRACEF (strOld + _T (" --> ") + strNew);
					nPending++;
				}
			}

			if (nPending) {
				CArray <TASKSTRUCT, TASKSTRUCT &> vTasks;
				
				GetTaskRecords (database, ALL, vTasks);

				for (POSITION pos = vBitmaps.GetStartPosition (); pos; ) {
					CString strOld, strNew;

					vBitmaps.GetNextAssoc (pos, strOld, strNew);

					if (strOld.CompareNoCase (strNew) != 0) {
						for (int nTask = 0; nTask < vTasks.GetSize (); nTask++) {
							TASKSTRUCT & task = vTasks [nTask];
							int nChanged = 0;

							for (int nMsg = 0; nMsg < task.m_vMsgs.GetSize (); nMsg++) {
								MESSAGESTRUCT & msg = task.m_vMsgs [nMsg];
								CStringArray v;

								ParsePackets (msg.m_strData, v);

								for (int nPacket = 0; nPacket < v.GetSize (); nPacket++) {
									CStringArray vPacket;
									CString & strSeg = v [nPacket];

									Tokenize (strSeg, vPacket);

									if (vPacket.GetSize ()) { 
										CString & strToken = vPacket [0];

										if (!strToken.CompareNoCase (_T ("Bitmap"))) {
											#ifdef __WINPRINTER__
												const int nField = 9;
											#else
												const int nField = 10;
											#endif

											CString strFile = UnformatString (vPacket [nField]);

											if (!strFile.CompareNoCase (strOld)) {
												CString strElement;
				
												vPacket.SetAt (nField, FormatString (strNew));

												for (int nField = 0; nField < vPacket.GetSize (); nField++)
													strElement += vPacket [nField] + _T (",");

												if (strElement.GetLength () && strElement [strElement.GetLength () - 1] == ',')
													strElement.Delete (strElement.GetLength () - 1);
													
												//TRACEF (strElement);
												v.SetAt (nPacket, strElement);
												nChanged++;
											}
										}
									}
								}

								{
									msg.m_strData.Empty ();

									for (int nPacket = 0; nPacket < v.GetSize (); nPacket++) 
										msg.m_strData += _T ("{") + v [nPacket] + _T ("}");
								}
							}

							if (nChanged) {
								VERIFY (UpdateTaskRecord (database, task));
								nTotal++;
							}
						}
					}
				}
			}

			#ifdef _DEBUG
			CString str;

			str.Format (_T ("%d bitmaps redirected"), nTotal);
			TRACEF (str);
			#endif

			IMPORTSTRUCT import;

			import.m_strStruct = ::lpszBITMAP;
			import.m_nImported = import.m_nParsed = vBitmaps.GetCount ();
			/* params.m_ */ vResult.Add (import);

			if (nTotal) {
				import.m_strStruct.Format (_T ("%s (%s)"), ::lpszBITMAP, LoadString (IDS_RELOCATED));
				import.m_nImported = nTotal;
				/* params.m_ */ vResult.Add (import);
			}
		}

		{
			for (int i = 0; i < vResult.GetSize (); i++) {
				if (params.m_pDlg)
					params.m_pDlg->SendMessage (CProgressDlg::m_wmAddResult, (WPARAM)new IMPORTSTRUCT (vResult [i]), 0);

				TRACER (vResult [i].m_strStruct + _T (": ") + ToString (vResult [i].m_nParsed) + _T (": ") + ToString (vResult [i].m_nImported));
				::Sleep (1);
			}

			for (POSITION pos = vBitmapFiles.GetStartPosition (); pos; ) {
				CString str, strCount;

				vBitmapFiles.GetNextAssoc (pos, str, strCount);
				
				if (params.m_pDlg)
					params.m_pDlg->SendMessage (CProgressDlg::m_wmAddBitmapFile, (WPARAM)new CString (str), (LPARAM)new CString (strCount));

				TRACER (_T ("bitmap file: ") + str + _T (": ") + strCount);
				::Sleep (1);
			}

			for (POSITION pos = vBitmapDirs.GetStartPosition (); pos; ) {
				CString str, strCount;

				vBitmapDirs.GetNextAssoc (pos, str, strCount);
				
				if (params.m_pDlg)
					params.m_pDlg->SendMessage (CProgressDlg::m_wmAddBitmapDir, (WPARAM)new CString (str), (LPARAM)new CString (strCount));

				TRACER (_T ("bitmap dir:  ") + str + _T (": ") + strCount);
				::Sleep (1);
			}
		}

		if (params.m_pDlg) {
			params.m_pDlg->PostMessage (CProgressDlg::m_wmEndProgress, IDOK, 0);
			::Sleep (1000);
		}

		return CImportFileDlg::IMPORTTHREAD_SUCCESS;
	}
	else {
		if (params.m_pDlg)
			params.m_pDlg->PostMessage (CProgressDlg::m_wmEndProgress, IDCANCEL, 0);

		return CImportFileDlg::IMPORTTHREAD_FAILED;
	}

	return CImportFileDlg::IMPORTTHREAD_FAILED;
}


