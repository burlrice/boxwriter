#if !defined(AFX_PROGRESSDLG_H__8F6B806A_7816_4021_9FD0_C774D2D93CAA__INCLUDED_)
#define AFX_PROGRESSDLG_H__8F6B806A_7816_4021_9FD0_C774D2D93CAA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProgressDlg.h : header file
//

#include "3dApi.h"
#include "RoundButtons.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CProgressDlg dialog

namespace Foxjet3d
{
	class _3D_API CProgressDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CProgressDlg(CWnd* pParent = NULL);   // standard constructor
		virtual ~CProgressDlg ();

		static const int m_wmStepProgress;
		static const int m_wmEndProgress;
		static const int m_wmMsgBox;
		static const int m_wmAddResult;
		static const int m_wmAddBitmapFile;
		static const int m_wmAddBitmapDir;

	// Dialog Data
		//{{AFX_DATA(CProgressDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		bool IsCancelled () const { return m_bCancel; }
		virtual BOOL Create (CWnd * pParent);

		typedef void (CALLBACK * LPSTATUSFCT) (const CString & str);

		static void SetStatusFct (LPSTATUSFCT lpfct);

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CProgressDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:

		bool m_bCancel;

		// Generated message map functions
		//{{AFX_MSG(CProgressDlg)
		virtual void OnCancel();
		virtual void OnOK();
		//}}AFX_MSG

		afx_msg LRESULT OnStepProgress (WPARAM wParam, LPARAM lParam);
		afx_msg LRESULT OnEndProgress (WPARAM wParam, LPARAM lParam);
		afx_msg LRESULT OnMsgBox (WPARAM wParam, LPARAM lParam);
		afx_msg LRESULT OnAddResult (WPARAM wParam, LPARAM lParam);
		afx_msg LRESULT OnAddBitmapFile (WPARAM wParam, LPARAM lParam);
		afx_msg LRESULT OnAddBitmapDir (WPARAM wParam, LPARAM lParam);
		
		FoxjetUtils::CRoundButtons m_buttons;

		DECLARE_MESSAGE_MAP()
	};
}; // namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROGRESSDLG_H__8F6B806A_7816_4021_9FD0_C774D2D93CAA__INCLUDED_)
