// EditorElementList.cpp: implementation of the CEditorElementList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ElmntLst.h"
#include "Coord.h"
#include "Database.h"
#include "Color.h"
#include "Debug.h"
#include "Extern.h"
#include "TemplExt.h"
#include "DC.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace Color;
using namespace ListGlobals;
using namespace Foxjet3d;
using namespace Foxjet3d::Panel;
using namespace Element;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define HIT_LEFT	0x01
#define HIT_RIGHT	0x02
#define HIT_TOP		0x04
#define HIT_BOTTOM	0x08

static bool WithinMargin (int x, int y, int cx)
{
	return (x >= (y - cx) && x <= (y + cx));
}

bool operator == (const CEditorElementList & lhs, const CEditorElementList & rhs)
{
	// TODO: this might not be the most efficient way to compare
	return lhs.CElementList::ToString (lhs.GetVersion ()) == rhs.CElementList::ToString (rhs.GetVersion ());
}

bool operator != (const CEditorElementList & lhs, const CEditorElementList & rhs)
{
	return !(lhs == rhs);
}

CEditorElementList::CEditorElementList (const CProductArea & pa, bool bAutoDelete)
:	m_rcPA (pa),
	CElementList (bAutoDelete)
{
}

CEditorElementList::CEditorElementList(const CEditorElementList & rhs)
:	m_rcPA (rhs.m_rcPA)
{
	m_pa = rhs.m_pa;
	m_bAutoDelete = rhs.m_bAutoDelete;
	m_units = rhs.m_units;
	m_head = rhs.m_head;
	SetUserDefined (rhs.GetUserDefined ());

	for (int i = 0; i < rhs.GetSize (); i++) 
		Add (new CElement (rhs.GetElement (i)));
}

CEditorElementList & CEditorElementList::operator =(const CEditorElementList &rhs)
{
//	CElementList::operator = (rhs);
	// calling the base class assignment operator will result in 
	// elements derived from CBaseElement and not CElement

	if (this != &rhs) {
		DeleteAllElements ();

		m_pa = rhs.m_pa;
		m_bAutoDelete = rhs.m_bAutoDelete;
		m_units = rhs.m_units;
		m_head = rhs.m_head;
		m_rcPA  = rhs.m_rcPA;
		SetUserDefined (rhs.GetUserDefined ());

		for (int i = 0; i < rhs.GetSize (); i++) 
			Add (new CElement (rhs.GetElement (i)));
	}

	return * this;
}

CEditorElementList::~CEditorElementList ()
{
	if (m_bAutoDelete)  
		DeleteAllElements ();
}

CElement & CEditorElementList::GetElement (int nIndex)
{
	return * GetElementPtr (nIndex);
}

const CElement & CEditorElementList::GetElement (int nIndex) const
{
	return * GetElementPtr (nIndex);
}

CElement * CEditorElementList::GetElementPtr (int nIndex)
{
	#ifdef _DEBUG
	ASSERT (nIndex >= 0 && nIndex < GetSize ());
	const CObject * pObject = (CObject *)GetAt (nIndex);
	ASSERT (pObject);
	ASSERT (pObject->IsKindOf (RUNTIME_CLASS (CElement)));
	#endif //_DEBUG

	return (CElement *)GetAt (nIndex);
}

const CElement * CEditorElementList::GetElementPtr (int nIndex) const
{
	#ifdef _DEBUG
	ASSERT (nIndex >= 0 && nIndex < GetSize ());
	const CObject * pObject = (CObject *)GetAt (nIndex);
	ASSERT (pObject);
	ASSERT (pObject->IsKindOf (RUNTIME_CLASS (CElement)));
	#endif //_DEBUG

	return (CElement *)GetAt (nIndex);
}

int CEditorElementList::HitTest (const CPoint & ptTest, double dZoom, const CBoundary & workingHead) const
{
	// check selected elements first
	for (int i = 0; i < GetSize (); i++) {
		const CElement & e = GetElement (i);

		if (e.IsSelected ()) {
			CRect rc = e.GetWindowRect (GetHead (), workingHead.m_card);
			CPoint pt = rc.TopLeft () * dZoom;
			CSize size = rc.Size () * dZoom;
			CRect rcZoom (pt.x, pt.y, pt.x + size.cx, pt.y + size.cy);

			if (rcZoom.PtInRect (ptTest)) 
				return i;
		}
	}

	for (int i = 0; i < GetSize (); i++) {
		const CElement & e = GetElement (i);
		CRect rc = e.GetWindowRect (GetHead (), workingHead.m_card);
		CPoint pt = rc.TopLeft () * dZoom;
		CSize size = rc.Size () * dZoom;
		CRect rcZoom (pt.x, pt.y, pt.x + size.cx, pt.y + size.cy);

		if (rcZoom.PtInRect (ptTest)) 
			return i;
	}

	return -1;
}

CPoint CEditorElementList::GetNextOpenPos(const CPoint &pt) const
{
	CPoint ptResult (pt);
	int nMargin = 3;
	bool bFound = false;
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();

	while (!bFound) {
		bool bInvalid = false;

		for (int i = 0; i < GetSize (); i++) {
			const CElement & e = GetElement (i);

			if (WithinMargin (e.GetPos (head).x, ptResult.x, nMargin) &&
				WithinMargin (e.GetPos (head).y, ptResult.y, nMargin))
			{
				bInvalid = true;
				ptResult += CPoint (20, 0);
				break;
			}
		}

		bFound = !bInvalid;
	}

	return ptResult;
}

CElement & CEditorElementList::operator [] (int nIndex)
{
	return GetElement (nIndex);
}

const CElement & CEditorElementList::operator [] (int nIndex) const
{
	return GetElement (nIndex);
}

CBaseElement & CEditorElementList::GetObject (int nIndex)
{
	ASSERT (nIndex >= 0 && nIndex < GetSize ());
	const CObject * pObject = (CObject *)GetAt (nIndex);
	ASSERT (pObject);
	
	if (pObject->IsKindOf (RUNTIME_CLASS (CElement))) 
		return * GetElement (nIndex).GetElement ();
	else
		ASSERT (pObject->IsKindOf (RUNTIME_CLASS (CBaseElement)));
	
	return * (CBaseElement *) pObject;
}

const CBaseElement & CEditorElementList::GetObject (int nIndex) const
{
	ASSERT (nIndex >= 0 && nIndex < GetSize ());
	const CObject * pObject = (CObject *)GetAt (nIndex);
	ASSERT (pObject);
	
	if (pObject->IsKindOf (RUNTIME_CLASS (CElement))) 
		return * GetElement (nIndex).GetElement ();
	else
		ASSERT (pObject->IsKindOf (RUNTIME_CLASS (CBaseElement)));
	
	return * (CBaseElement *) pObject;
}

void CEditorElementList::DeleteAllElements ()
{
//	TRACEF (_T ("CEditorElementList::DeleteAllElements") +
//		CString (m_bAutoDelete ? _T (" [auto delete]") : _T ("")));

	for (int i = 0; i < GetSize (); i++) {
		CElement * p = &GetElement (i);
		delete p;
	}

	RemoveAll ();
}


int CEditorElementList::FromString (const CString & str, CProductArea & pa,
									UNITS units, const FoxjetDatabase::HEADSTRUCT & head,
									FoxjetCommon::CVersion & ver, 
									bool bDeleteExisiting, ULONG lLocalParamUID)
{
	CElementList v ((CElementList &)* this);
	CDC dcMem;
	
	dcMem.CreateCompatibleDC (FoxjetCommon::CTmpDC (::AfxGetMainWnd ())); //(::AfxGetMainWnd ()->GetDC ());

	CCoord coord ((double)pa.Width (), (double)pa.Height (), INCHES, head, 1.0);
	CSize paOriginal ((int)(coord.m_x * 1000.0), (int)(coord.m_y * 1000));

	// default to current settings
	v.SetProductArea (paOriginal);
	v.SetUnits (units);
	v.SetHead (head);

	int nResult = v.FromString (str, ver, false, lLocalParamUID);
	
	if (bDeleteExisiting)
		DeleteAllElements ();

	int nMax = min (v.GetSize (), GetMaxElements ());

	for (int i = GetSize (); i < nMax; i++) {
		// must make a new temp copy to image (ip ver must be imaged to set pos).
		// ip bitmap is a special case, needs memstore created - can't just 
		// image the old one because it could contain the wrong bitmap.
		CBaseElement * pTmp = CopyElement (&v [i], &head, false);
		
		pTmp->Build ();
		pTmp->Draw (dcMem, head, true);

		CBaseElement * pBase = CopyElement (pTmp, &head, false);
		CElement * p = new CElement (pBase);

		pBase->Build ();
		pBase->Draw (dcMem, head, true);
		pBase->SetLocalParamUID (lLocalParamUID);

		CPoint pt = CCoord::CoordToPoint (
			(double)pTmp->GetPos (&head).x / 1000.0, 
			(double)pTmp->GetPos (&head).y / 1000.0, 
			INCHES, head); 
		p->SetPos (pt, head);

		Add (p);
		delete pTmp;
	}
 
	if (v.GetProductArea () != paOriginal) {
		// rounding error could cause a slight change in the product area,
		// so don't reset it unless the original (in 1000-ths) has changed
		// as a result of the v.FromString call
		const CPoint pt = CCoord::CoordToPoint (
			(double)v.GetProductArea ().cx / 1000.0,
			(double)v.GetProductArea ().cy / 1000.0, 
			INCHES, head);
		
		pa.right = pa.left + pt.x;
		pa.bottom = pa.top + pt.y;
	}

	SetUnits (v.GetUnits ());
	CElementList::SetHead (v.GetHead ());
	SetVersion (v.GetVersion ());
	SetUserDefined (v.GetUserDefined ());

	return nResult;
}

CString CEditorElementList::ToString (const FoxjetCommon::CVersion & ver, 
									  const CProductArea & pa,
									  bool bBreakTokens) const
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();
	CCoord c (pa.Width (), pa.Height (), INCHES, head, 1.0);
	CSize prod ((LONG)(c.m_x * 1000.0), (LONG)(c.m_y * 1000.0));
	CEditorElementList v = * this;

	v.SetProductArea (prod);
	v.SetUnits (GetUnits ());
	v.CElementList::SetHead (GetHead ());

	CString str = v.CElementList::ToString (ver, bBreakTokens);
	v.DeleteAllElements ();

	return str;
}

HBITMAP CEditorElementList::CreateBitmap (CDC & dc, 
										  const FoxjetCommon::CLongArray & vDraw,
										  const CProductArea & pa,
										  const CBoundary & workingHead) const
{
	HBITMAP hBmp = NULL;
	const CBoundary & head = m_bounds;

	if (GetSize ()) {
		CEditorElementList sel; // temp list

		// copy the selection
		for (int i = 0; i < vDraw.GetSize (); i++) {
			CElement * p = new CElement (GetElement (vDraw [i]));
			p->GetElement ()->Build ();
			sel.Add (p);
		}

		if (sel.GetSize ()) {
			CPoint ptFirst = sel.GetElement (0).GetPos (head.m_card);
			int x = ptFirst.x;
			int y = ptFirst.y;
			int cx = 0;
			int cy = 0;

			// determine the bounding rect
			for (int i = 0; i < vDraw.GetSize (); i++) {
				const CElement & e = GetElement (vDraw [i]);
				//e.RecalcSize (dc);
				CPoint pt = e.GetPos (head.m_card);
				CSize size = CBaseElement::ThousandthsToLogical (e.GetElement ()->GetSize (head.m_card), head.m_card);

				x = min (x, pt.x);
				y = min (y, pt.y);

				cx = max (cx, pt.x + size.cx);
				cy = max (cy, pt.y + size.cy);
			}

			CRect rcSrc (x, y, cx, cy);
			CPoint ptOffset (x, y);
			CSize extent (rcSrc.Width (), rcSrc.Height ());

			CDC dcMem;

			dcMem.CreateCompatibleDC (&dc);
			hBmp = CreateCompatibleBitmap ((HDC)dc, extent.cx, extent.cy);

			if (hBmp) {
				// draw the little bastards into the bitmap
				CGdiObject * pOld = dcMem.SelectObject (CBitmap::FromHandle (hBmp));
				dcMem.FillSolidRect (0, 0, extent.cx, extent.cy, rgbWhite);

				for (int i = 0; i < sel.GetSize (); i++) {
					CElement & e = sel.GetElement (i);

					// offset so the top-leftmost element is at (0, 0)
					// (NOT including the product area offsets)
					e.SetPos (e.GetPos (head.m_card) - ptOffset - pa.TopLeft (), head.m_card);
					e.RecalcSize (dcMem, head.m_card);
					e.Draw (dcMem, workingHead.m_card);
				}

				dcMem.SelectObject (pOld);
			}
		}
	}

	return hBmp;
}

bool CEditorElementList::SetHead (ULONG lHeadID)
{
	HEADSTRUCT h;

	if (GetHeadRecord (lHeadID, h)) {
		CValveArray v;

		v.Copy (m_bounds.m_card.m_vValve);
		m_bounds.m_card = h;
		m_bounds.m_card.m_vValve.RemoveAll ();
		m_bounds.m_card.m_vValve.Copy (v);

		//CString str; str.Format ("%s (%d)", m_head.m_dbHead.m_strName, m_head.m_dbHead.m_vValve.GetSize ()); TRACEF (str);

		return CElementList::SetHead (h);
	}

	return false;
}


bool CEditorElementList::SetHead (const Foxjet3d::Panel::CBoundary & head) 
{ 
	if (SetHead (head.m_card.m_lID)) {
		m_head.m_dbHead.m_vValve.Copy (head.m_card.m_vValve);
		//CString str; str.Format ("%s (%d)", m_head.m_dbHead.m_strName, m_head.m_dbHead.m_vValve.GetSize ()); TRACEF (str);
		return true;
	}

	return false;
}

const CElement * CEditorElementList::GetObject (const FoxjetCommon::CBaseElement * p) const
{
	for (int i = 0; i < GetSize (); i++) {
		const CElement & e = GetElement (i);
		
		if (e.GetElement () == p)
			return &e;
	}

	return NULL;
}

int CEditorElementList::GetIndex(const CElement *pElement) const
{
	for (int i = 0; i < GetSize (); i++) {
		if (&GetElement (i) == pElement)
			return i;
	}

	return -1;
}


bool CEditorElementList::Contains (FoxjetCommon::CLongArray & v, int nIndex)
{
	for (int i = 0; i < v.GetSize (); i++) 
		if ((int)v [i] == nIndex)
			return true;

	return false;
}
	
bool CEditorElementList::SetProductArea (const CProductArea & pa, const CSize & sizePanel)
{
	bool bResult = SetProductArea (pa.Size (), sizePanel);

	if (bResult) {
		m_rcPA = pa;
	}

	return bResult;
}

bool CEditorElementList::SetProductArea(int cx, int cy, bool bUpdateList, const CSize & sizePanel)
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();

	cx = BindTo (cx, (int)MIN_PRODLEN, (int)MAX_PRODLEN);
	cy = BindTo (cy, (int)MIN_PRODHEIGHT, (int)MAX_PRODHEIGHT);

	if (bUpdateList) {
		CCoord c (cx, cy, INCHES, head);
		CSize pa ((UINT)(c.m_x * 1000.0),
			(UINT)(c.m_y * 1000.0));

		CElementList::SetProductArea (pa, sizePanel);
	}

	m_rcPA.right = m_rcPA.left + cx;
	m_rcPA.bottom = m_rcPA.top + cy;

	return true;
}

CRect CEditorElementList::GetBoundingRect (const CElement & element, const CBoundary & workingHead) const 
{
	const CBoundary & head = m_bounds;
	CRect rcResult = element.GetWindowRect (head.m_card, workingHead.m_card);

	for (int i = 0; i < GetSize (); i++) {
		const CElement & e = GetElement (i);
		CRect rc = e.GetWindowRect (head.m_card, workingHead.m_card);

		if (CRect ().IntersectRect (rc, rcResult))
			rcResult.UnionRect (rc, rcResult);
	}

	return rcResult;
}

Element::CPairArray CEditorElementList::GetOverlapping (const CElement * pElement, const CBoundary & workingHead) const
{
	const CBoundary & head = m_bounds;
	Element::CPairArray v;
	const CRect rc = pElement->GetWindowRect (head.m_card, workingHead.m_card);

	for (int i = 0; i < GetSize (); i++) {
		CElement * p = (CElement *)(DWORD)GetElementPtr (i); // const --> non-const cast
		CRect rcElement = CElement::GetValveRect (p->GetWindowRect (head.m_card, workingHead.m_card), workingHead, 1.0);

		if (!IsValveHead (head.m_card))
			rcElement.DeflateRect (1, 1);
		else {
			rcElement.DeflateRect (CBaseElement::GetValveDotSize ());
		}

		if (p != pElement && CRect ().IntersectRect (rc, rcElement)) {
			CEditorElementList * pList = (CEditorElementList *)(DWORD)this; // const --> non-const cast
			v.Add (Element::CPair (p, pList, &workingHead.m_card));
		}
	}

	return v;
}

CElement * CEditorElementList::Find (const CElement * pFind) const
{
	for (int i = 0; i < GetSize (); i++) {
		CElement * pElement = (CElement *)(DWORD)GetElementPtr (i); // const --> non-const cast

		if (pFind == pElement)
			return pElement;
	}

	return NULL;
}

