// HeadDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "Utils.h"
#include "HeadDlg.h"
#include "Resource.h"
#include "Debug.h"
#include "ComPropertiesDlg.h"
#include "fj_defines.h"
#include "TemplExt.h"
#include "AppVer.h"
#include "DevGuids.h"
#include "HeadConfigDlg.h"
#include "ElementDefaults.h"
#include "StdCommDlg.h"
#include "HeadExDlg.h"
#include "Parse.h"
#include "WinMsg.h"

#ifdef __IPPRINTER__
	#include "SyncDlg.h"
	#include "NetCommDlg.h"
#endif

static const LPCTSTR lpszPcRepeater = _T ("PC repeater, %d");
static const LPCTSTR lpszSaveDynData = _T ("SaveDynData, %d");

extern FoxjetCommon::CElementDefaults & defElements;

/*
slot 1 
	should be master [only head with true isa interrupt]
	can use encoder a & photocell a only, cannot share
slot 2
	can use pc b, enc b (standalone)
	can use a, but must share from head 1
master
	determines who's counts are used in print report
*/

#define SOFTWARELIMITEDCONFIG (true) // if this is ever set to false, update rdo check logic

#include "Coord.h" // this is the reason CHeadDlg must reside in 3d.dll

static const double dChannelSeparation = 0.05848; // Trident Heads

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace Foxjet3d;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef __WINPRINTER__
	const FoxjetCommon::LIMITSTRUCT CHeadDlg::m_lmtPhotocellRate	= {	6,		144					}; // in inches
#else
	const FoxjetCommon::LIMITSTRUCT CHeadDlg::m_lmtPhotocellRate	= {	1,		144					}; // in inches
#endif

const FoxjetCommon::LIMITSTRUCT CHeadDlg::m_lmtHeadName				= {	1,		FJPH_TYPE_NAME_SIZE	};
const FoxjetCommon::LIMITSTRUCT CHeadDlg::m_lmtMphcPhotocellDelay	= {	500,	30000				}; // in 1000ths
const FoxjetCommon::LIMITSTRUCT CHeadDlg::m_lmtVxPhotocellDelay		= {	2000,	30000				}; // in 1000ths
const FoxjetCommon::LIMITSTRUCT CHeadDlg::m_lmtRelativeHeight		= {	0,		40000				}; // in 1000ths
const FoxjetCommon::LIMITSTRUCT CHeadDlg::m_lmtIntTachSpeed			= {	10,		300					}; // in ft/min

int CHeadDlg::CalcMaxLineSpeed (const FoxjetDatabase::HEADSTRUCT & head)
{
	int nDPI = (UINT)head.GetRes ();
	double dInchesPerSecond = 1000.0 / (double)nDPI; // 1 kHz max
	int nResult = (int)((dInchesPerSecond * 60.0) / 12.0);

	return BindTo (nResult, (int)CHeadDlg::m_lmtIntTachSpeed.m_dwMin, 620);
}

#ifdef __WINPRINTER__
const CHeadDlg::PHCSTRUCT CHeadDlg::m_mapPHC [6] =
{
	{ 0x0300,		_T ("PHC_1"),	},
	{ 0x0310,		_T ("PHC_2"),	},
	{ 0x0320,		_T ("PHC_3"),	},
	{ 0x0330,		_T ("PHC_4"),	},
	{ 0x0340,		_T ("PHC_5"),	},
	{ 0x0350,		_T ("PHC_6"),	},
};

const CHeadDlg::PHCSTRUCT CHeadDlg::m_mapIVPHC [2] =
{
	{ 0x0300,		_T ("PHC_1"),	},
	{ 0x0320,		_T ("PHC_2"),	},
};

const CHeadDlg::PHCSTRUCT CHeadDlg::m_mapUSB [8] =
{
	{ 0x0001,		_T ("USB_1a"),	},
	{ 0x0002,		_T ("USB_1b"),	},
	{ 0x0003,		_T ("USB_2a"),	},
	{ 0x0004,		_T ("USB_2b"),	},
	{ 0x0005,		_T ("USB_3a"),	},
	{ 0x0006,		_T ("USB_3b"),	},
	{ 0x0007,		_T ("USB_4a"),	},
	{ 0x0008,		_T ("USB_4b"),	},
};

typedef enum
{
	PHC_1 = 0,
	PHC_2,
	PHC_3,
	PHC_4,
	PHC_5,
	PHC_6,
	USB_1a,
	USB_1b,
	USB_2a,
	USB_2b,
	USB_3a,
	USB_3b,
	USB_4a,
	USB_4b,
} CARDINDEX;

static ULONG GetAddress (CARDINDEX index)
{
	const CHeadDlg::PHCSTRUCT * p = CHeadDlg::m_mapPHC;
	int nSize = ARRAYSIZE (CHeadDlg::m_mapPHC);
	ULONG lResult = 0x300;
	bool bValve = ISVALVE ();

	if (bValve) {
		p = CHeadDlg::m_mapIVPHC;
		nSize = ARRAYSIZE (CHeadDlg::m_mapIVPHC);
	}

	if (index < nSize)
		lResult = p [index].m_lAddr;
	else { ASSERT (0); }

	return lResult;
}

static bool IsIsaCard (ULONG lAddress)
{
	return lAddress >= 0x300 && lAddress <= 0x350;
}

#endif //__WINPRINTER__

/////////////////////////////////////////////////////////////////////////////
// CHeadDlg dialog

#ifdef __IPPRINTER__
const CString CHeadDlg::m_strSyncKey = _T ("Head sync settings");
#endif //__IPPRINTER__

CHeadDlg::CHeadDlg(FoxjetDatabase::COdbcDatabase & db, 
				   const FoxjetDatabase::HEADSTRUCT & head, 
				   UNITS units, const CMapStringToString & vAvailAddressMap, bool bRestricted, CWnd* pParent)
:	m_db (db),
	m_head (head),
	m_bReadOnly (false),
	m_units (units),
	m_bNew (false),
	m_lLineID (-1),
	m_bSync (true),
	m_bRestricted (bRestricted),
//	FoxjetCommon::CEliteDlg (ISVALVE () ? IDD_IVCARD : IDD_HEAD_MATRIX, pParent)
	FoxjetCommon::CEliteDlg (IDD_HEAD_MATRIX, pParent)
{
#ifdef __WINPRINTER__
	for (POSITION pos = vAvailAddressMap.GetStartPosition(); pos != NULL; ) {
		CString strKey, strAddr;

		vAvailAddressMap.GetNextAssoc (pos, strKey, strAddr);
		m_vAvailAddressMap [strKey] = strAddr;
	}	
#endif //__WINPRINTER__

	VERIFY (GetPanelRecord (m_db, m_head.m_lPanelID, m_panel));
}

void CHeadDlg::DoDataExchange(CDataExchange* pDX)
{
	HEADSTRUCT & head = m_head;
	CCoord coordMin (0, 0, m_units, head);
	CCoord coordMax (0, 0, m_units, head);

	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHeadDlg)
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_MORE);
	m_buttons.DoDataExchange (pDX, BTN_SYNC);
	m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);

	DDX_Text (pDX, TXT_NAME, head.m_strName);
	DDV_MinMaxChars (pDX, head.m_strName, TXT_NAME, m_lmtHeadName);

#ifndef __WINPRINTER__
	DDX_Text (pDX, TXT_ADDRESS, head.m_strUID);
	DDV_MinMaxChars (pDX, head.m_strUID, TXT_ADDRESS, m_lmtHeadName);
#endif

	{
		CCoord c (0, 0, HEAD_PIXELS, head);

		DDX_Coord (pDX, TXT_PHOTOCELLDELAY, (long &)head.m_lPhotocellDelay, m_units, head, true);

		if (!IsValveHead (head)) {
			coordMin.m_x = m_lmtMphcPhotocellDelay.m_dwMin;
			coordMax.m_x = m_lmtMphcPhotocellDelay.m_dwMax;
			DDV_Coord (pDX, TXT_PHOTOCELLDELAY, head.m_lPhotocellDelay, 0, m_units, head, DDVCOORDXBOTH, coordMin, coordMax);
		}
	}

	{
		/*
		DDX_Text (pDX, TXT_PHOTOCELLRATE, head.m_dPhotocellRate);
		DDV_MinMaxDouble (pDX, head.m_dPhotocellRate, m_lmtPhotocellRate.m_dwMin, m_lmtPhotocellRate.m_dwMax);
		*/
		long lRate = (ULONG)(head.m_dPhotocellRate * 1000.0);

		DDX_Coord (pDX, TXT_PHOTOCELLRATE, lRate, m_units, head, true);

		if (pDX->m_bSaveAndValidate) {
			CCoord c (0, 0, m_units, head);

			c.m_x = lRate;
			coordMin.m_x = m_lmtPhotocellRate.m_dwMin * 1000.0;
			coordMax.m_x = m_lmtPhotocellRate.m_dwMax * 1000.0;

			// should be [6, 144"]

			DDV_Coord (pDX, TXT_PHOTOCELLRATE, lRate, 0, m_units, head, DDVCOORDXBOTH, coordMin, coordMax);
			c.m_x /= 1000.0;
			head.m_dPhotocellRate = c.m_x;

			if (IsValveHead (head)) {
				int nPixels = Round (head.m_dPhotocellRate * head.GetRes ());

				head.m_dPhotocellRate = (double)nPixels / head.GetRes ();
			}
		}
	}

	DDX_Coord (pDX, TXT_RELATIVEHEIGHT, (long &)head.m_lRelativeHeight, m_units, head, false);
	coordMin.m_y = m_lmtRelativeHeight.m_dwMin;
	coordMax.m_y = m_lmtRelativeHeight.m_dwMax;
	DDV_Coord (pDX, TXT_RELATIVEHEIGHT, 0, head.m_lRelativeHeight, m_units, head, DDVCOORDYBOTH, coordMin, coordMax);

	{
		/*
		DDX_Text (pDX, TXT_INTTACHSPEED, head.m_nIntTachSpeed);
		DDV_MinMaxUInt (pDX, head.m_nIntTachSpeed, m_lmtIntTachSpeed.m_dwMin, m_lmtIntTachSpeed.m_dwMax);
		*/

		UNITS units = m_units;

		switch (units) {
		case INCHES:		units = FEET;		break;
		case CENTIMETERS:	units = METERS;		break;
		}

		// feet --> 1000ths
		long lSpeed = (long)(head.m_nIntTachSpeed * 12000.0);

		DDX_Coord (pDX, TXT_INTTACHSPEED, lSpeed, units, head, true);

		if (pDX->m_bSaveAndValidate) {
			CCoord c (0, 0, units, head);			// note: units may not be the same as m_units
			CCoord coordMin (0, 0, units, head);	// note: units may not be the same as m_units
			CCoord coordMax (0, 0, units, head);	// note: units may not be the same as m_units
			LIMITSTRUCT lmt = m_lmtIntTachSpeed;

			if (IsValveHead (head)) 
				lmt.m_dwMax = CalcMaxLineSpeed (m_head);

			c.m_x = lSpeed;
			coordMin.m_x = lmt.m_dwMin * 12000.0;
			coordMax.m_x = lmt.m_dwMax * 12000.0;

			DDV_Coord (pDX, TXT_INTTACHSPEED, lSpeed, 0, units, head, DDVCOORDXBOTH, coordMin, coordMax);
			c.m_x /= 12000.0;
			head.m_nIntTachSpeed = (int)c.m_x;
		}
	}

	DDX_Radio (pDX, RDO_RTOL, head.m_nDirection);
	
	DDX_Radio (pDX, RDO_PHOTOCELLEXT, head.m_nPhotocell);
	DDX_Radio (pDX, RDO_ENCODEREXT, head.m_nEncoder);
	
//	if (!head.IsUSB ()) {
		DDX_Radio (pDX, RDO_PHOTOCELLSHAREDNONE, head.m_nSharePhoto);
		DDX_Radio (pDX, RDO_ENCODERSHAREDNONE, head.m_nShareEnc);
//	}
//	else {
//		if (pDX->m_bSaveAndValidate) {
//		}
//		else {
//		}
//	}

	DDX_Check (pDX, CHK_MASTER, head.m_bMasterHead);
	DDX_Check (pDX, CHK_DOUBLEPULSE, head.m_bDoublePulse);
	DDX_Check (pDX, CHK_ENABLED, head.m_bEnabled);
	DDX_Check (pDX, CHK_INVERT, head.m_bInverted);
	DDX_Check (pDX, CHK_UPSIDEDOWN, head.m_bUpsidedown);
}


BEGIN_MESSAGE_MAP(CHeadDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CHeadDlg)
	//}}AFX_MSG_MAP	

	ON_CBN_SELCHANGE (CB_ANGLE, OnAngleChanged)
	ON_BN_CLICKED (RDO_LTOR, OnDirection)
	ON_BN_CLICKED (RDO_RTOL, OnDirection)

	ON_BN_CLICKED (RDO_PHOTOCELLEXT, OnPhotocellSource)
	ON_BN_CLICKED (RDO_PHOTOCELLINT, OnPhotocellSource)
	ON_BN_CLICKED (RDO_PHOTOCELLSHARED, OnPhotocellSource)
	
	ON_BN_CLICKED (RDO_ENCODEREXT, OnEncoderSource)
	ON_BN_CLICKED (RDO_ENCODERINT, OnEncoderSource)
	ON_BN_CLICKED (RDO_ENCODERSHARED, OnEncoderSource)

	ON_BN_CLICKED (BTN_MORE, OnMore)
	ON_BN_CLICKED (CHK_MASTER, OnMaster)	
	ON_BN_CLICKED (CHK_ENABLED, OnEnabled)	
	ON_BN_CLICKED (BTN_SYNC, OnSync)	
	ON_BN_CLICKED (CHK_PCREPEATER, OnUsePcRepeater)	

	ON_CBN_SELCHANGE(CB_ADDRESS, OnSelChangeAddress)
	ON_CBN_SELCHANGE(CB_ENCRES, OnSelChangeEncRes) 

	ON_CBN_SELCHANGE (CB_TYPE, OnTypeChanged)
	ON_CBN_SELCHANGE (CB_HORZRES, OnHorzResChanged)

	ON_REGISTERED_MESSAGE (WM_SYNCREMOTEHEADCOMPLETE, OnSyncComplete)

	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHeadDlg message handlers

BOOL CHeadDlg::OnInitDialog() 
{
	CComboBox * pStandby = (CComboBox *)GetDlgItem (CB_STANDBY);
	CString str;
	SETTINGSSTRUCT s;
	int nStandby [] = 
	{
		1,
		2,
		5,
		10,
		20
	};
	bool bValve = ISVALVE ();

	ASSERT (pStandby);

#if defined( _DEBUG ) && ( __WINPRINTER__ )
	/*
	if (bValve) {
		m_vAvailAddressMap [_T ("300")] = _T ("PHC_1");
		m_vAvailAddressMap [_T ("320")] = _T ("PHC_2");
	}
	else {
		m_vAvailAddressMap [_T ("300")] = _T ("PHC_1");
		m_vAvailAddressMap [_T ("310")] = _T ("PHC_2");
		m_vAvailAddressMap [_T ("320")] = _T ("PHC_3");
		m_vAvailAddressMap [_T ("330")] = _T ("PHC_4");
		m_vAvailAddressMap [_T ("340")] = _T ("PHC_5");
		m_vAvailAddressMap [_T ("350")] = _T ("PHC_6");

		m_vAvailAddressMap [_T ("001")] = _T ("USB_1a");
		m_vAvailAddressMap [_T ("002")] = _T ("USB_1b");
		m_vAvailAddressMap [_T ("003")] = _T ("USB_2a");
		m_vAvailAddressMap [_T ("004")] = _T ("USB_2b");
		m_vAvailAddressMap [_T ("005")] = _T ("USB_3a");
		m_vAvailAddressMap [_T ("006")] = _T ("USB_3b");
		m_vAvailAddressMap [_T ("007")] = _T ("USB_4a");
		m_vAvailAddressMap [_T ("008")] = _T ("USB_4b");
	}
	*/
#endif //_DEBUG && __WINPRINTER__

	s.m_lLineID = 0;
	s.m_strKey.Format (_T ("%d, Standby mode"), m_head.m_lID);
	s.m_strData = _T ("0");

	GetSettingsRecord (m_db, s.m_lLineID, s.m_strKey, s);
	int nStandbyTimeout = _tcstoul (s.m_strData, NULL, 10) / 60;

	str.Format (LoadString (bValve ? IDS_CARDPROPERTIES : IDS_HEADPROPERTIES), m_head.m_strName);
	SetWindowText (str);

	FoxjetCommon::CEliteDlg::OnInitDialog();

	pStandby->SetItemData (pStandby->AddString (LoadString (IDS_NEVER)), 0);
	pStandby->SetCurSel (0);
	
	for (int i = 0; i < ARRAYSIZE (nStandby); i++) {
		int n = nStandby [i];

		str.Format (LoadString (IDS_STANDBYAFTER), n);
		int nIndex = pStandby->AddString (str);
		pStandby->SetItemData (nIndex, n);

		if (nStandbyTimeout == n)
			pStandby->SetCurSel (nIndex);
	}

	m_bmpRTOL.LoadBitmap (IDB_RTOL_MATRIX);
	m_bmpLTOR.LoadBitmap (IDB_LTOR_MATRIX);

	FillPanels ();
	FillEncRes ();
	FillType ();

#ifdef __WINPRINTER__
	FillAddress ();
	OnSelChangeAddress ();
	UpdateData (FALSE);
	OnEncoderSource ();
	OnPhotocellSource ();
	OnTypeChanged ();

	{
		StdCommDlg::CCommItem item (false);

		item.FromString (_T ("0,") + m_head.m_strSerialParams);
		SetDlgItemText (TXT_SERIALPARAMS, item.GetDispText (1));
	}

	ULONG lAddr = _tcstoul (m_head.m_strUID, NULL, 16);

	if (lAddr > GetAddress (PHC_2))
		OnSelChangeAddress ();

	if (SOFTWARELIMITEDCONFIG && IsIsaCard (lAddr)) {
		if (lAddr == GetAddress (PHC_1)) {
			SetDlgItemCheck (RDO_PHOTOCELLSHARED,		0);
			SetDlgItemCheck (RDO_PHOTOCELLSHAREDNONE,	0);
			SetDlgItemCheck (RDO_PHOTOCELLSHAREDA,		1);
			SetDlgItemCheck (RDO_PHOTOCELLSHAREDB,		0);

			SetDlgItemCheck (RDO_ENCODERSHARED,			0);
			SetDlgItemCheck (RDO_ENCODERSHAREDNONE,		0);
			SetDlgItemCheck (RDO_ENCODERSHAREDA,		1);
			SetDlgItemCheck (RDO_ENCODERSHAREDB,		0);
		}
		else if (lAddr == GetAddress (PHC_2)) {
			switch (m_head.m_nPhotocell) {
			case SHARED_PC:
				SetDlgItemCheck (RDO_PHOTOCELLSHARED,		1);
				SetDlgItemCheck (RDO_PHOTOCELLSHAREDNONE,	0);
				SetDlgItemCheck (RDO_PHOTOCELLSHAREDA,		1);
				SetDlgItemCheck (RDO_PHOTOCELLSHAREDB,		0);
				break;
			default:
				SetDlgItemCheck (RDO_PHOTOCELLSHARED,		0);
				SetDlgItemCheck (RDO_PHOTOCELLSHAREDNONE,	0);
				SetDlgItemCheck (RDO_PHOTOCELLSHAREDA,		0);
				SetDlgItemCheck (RDO_PHOTOCELLSHAREDB,		1);
				break;
			}

			switch (m_head.m_nEncoder) {
			case SHARED_ENC:
				SetDlgItemCheck (RDO_ENCODERSHARED,			1);
				SetDlgItemCheck (RDO_ENCODERSHAREDNONE,		0);
				SetDlgItemCheck (RDO_ENCODERSHAREDA,		1);
				SetDlgItemCheck (RDO_ENCODERSHAREDB,		0);
				break;
			default:
				SetDlgItemCheck (RDO_ENCODERSHARED,			0);
				SetDlgItemCheck (RDO_ENCODERSHAREDNONE,		0);
				SetDlgItemCheck (RDO_ENCODERSHAREDA,		0);
				SetDlgItemCheck (RDO_ENCODERSHAREDB,		1);
				break;
			}
		}
	}

	FillHorzRes (m_head.m_nHorzRes, m_head.m_nEncoderDivisor);

#else //!__WINPRINTER__
	FillHorzRes (m_head.m_nHorzRes, m_head.m_nEncoderDivisor);
	OnTypeChanged ();
	OnPhotocellSource ();
	OnEncoderSource ();

	SetDlgItemText (RDO_PHOTOCELLEXT, LoadString (IDS_HEAD));
	SetDlgItemText (RDO_PHOTOCELLINT, LoadString (IDS_SHAREDCABLE));

	if (CButton * p = (CButton *)GetDlgItem (CHK_PCREPEATER))
		p->SetCheck (GetPcRepeater (m_db, m_head.m_lID) ? 1 : 0);

	{
		NetCommDlg::CCommItem item;

		item.FromString (m_head.m_strSerialParams);
		SetDlgItemText (TXT_SERIALPARAMS, item.GetDispText (1));
	}

#endif

	UpdateUI ();
	OnDirection ();

	{
		if (bValve) {
			HEADSTRUCT & head = m_head;
			/*
			HEADSTRUCT head (m_head);

			head.m_vValve.RemoveAll (); // so DDX_Coord doesn't use m_vValve in call to LogicalToThousandths, or ThousandthsToLogical
			head.m_nHeadType = HEADTYPE_FIRST;
			*/

			CCoord c (0, 0, m_units, head);
			HEADSTRUCT h = head;
			UINT nHideID [] = 
			{
				CB_PANEL,
				LBL_PANEL,
				LBL_RELATIVEHEIGHT,
				TXT_RELATIVEHEIGHT,
				LBL_PRINTHEIGHT,
				TXT_PRINTHEIGHT,
				LBL_PHOTOCELLDELAY,
				TXT_PHOTOCELLDELAY,
				LBL_ANGLE,
				CB_ANGLE,
				CHK_DOUBLEPULSE,
				CHK_ENABLED,
				CHK_INVERT,
				GRP_DIRECTION,
				RDO_RTOL,
				RDO_LTOR,
				LBL_STANDBY,
				CB_STANDBY,
				IDB_DIRECTION,
				CHK_MASTER,
			};

			HeadConfigDlg::CPanel::UpdateValves (m_db, m_lLineID, h);

			for (int i = 0; i < h.m_vValve.GetSize (); i++)
				c.m_y += ((double)GetValveHeight (h.m_vValve [i].m_type) / 1000.0);

			SetDlgItemText (TXT_PRINTHEIGHT, c.FormatY ());

			for (int i = 0; i < ARRAYSIZE (nHideID); i++) 
				if (CWnd * p = GetDlgItem (nHideID [i]))
					p->ShowWindow (SW_HIDE);

			if (CWnd * p = GetDlgItem (CB_ADDRESS))
				p->EnableWindow (FALSE);
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CHeadDlg::FillEncRes ()
{
	CComboBox * pEncRes = (CComboBox *)GetDlgItem (CB_ENCRES);
	CArray <UINT, UINT> vRes;
	bool bValve = ISVALVE ();

	vRes.Add (300);

	#ifndef __WINPRINTER__
	if (!bValve) 
		vRes.Add (426);
	#endif

	ASSERT (pEncRes);

	for (int i = 0; i < vRes.GetSize (); i++) {
		CString str;

		str.Format (_T ("%d"), vRes [i]);
		int nIndex = pEncRes->AddString (str);
		pEncRes->SetItemData (nIndex, vRes [i]);

		if (m_head.m_nHorzRes == vRes [i])
			pEncRes->SetCurSel (nIndex);
	}

	if (pEncRes->GetCurSel () == CB_ERR)
		pEncRes->SetCurSel (0);
}

void CHeadDlg::FillPanels ()
{
	CComboBox * pPanel = (CComboBox *)GetDlgItem (CB_PANEL);
	CArray <PANELSTRUCT, PANELSTRUCT &> v;
	
	ASSERT (pPanel);

	GetPanelRecords (m_db, m_panel.m_lLineID, v);

	pPanel->ResetContent ();

	for (int i = 0; i < v.GetSize (); i++) {
		PANELSTRUCT p = v [i];

		int nIndex = pPanel->AddString (p.m_strName);
		pPanel->SetItemData (nIndex, p.m_lID);

		if (p.m_lID == m_head.m_lPanelID)
			pPanel->SetCurSel (nIndex);
	}
}

void CHeadDlg::OnSelChangeEncRes ()
{
	CComboBox * pEncRes = (CComboBox *)GetDlgItem (CB_ENCRES);
	CComboBox * pPrintRes = (CComboBox *)GetDlgItem (CB_HORZRES);

	ASSERT (pEncRes);
	ASSERT (pPrintRes);

	m_head.m_nHorzRes			= pEncRes->GetItemData (pEncRes->GetCurSel ());
	m_head.m_nEncoderDivisor	= pPrintRes->GetItemData (pPrintRes->GetCurSel ());
	
	OnTypeChanged ();
}

void CHeadDlg::OnHorzResChanged ()
{
	CComboBox * pEncRes = (CComboBox *)GetDlgItem (CB_ENCRES);
	CComboBox * pPrintRes = (CComboBox *)GetDlgItem (CB_HORZRES);

	ASSERT (pEncRes);
	ASSERT (pPrintRes);

	m_head.m_nHorzRes			= pEncRes->GetItemData (pEncRes->GetCurSel ());
	m_head.m_nEncoderDivisor	= pPrintRes->GetItemData (pPrintRes->GetCurSel ());
	
	OnTypeChanged ();
}

void CHeadDlg::FillHorzRes (int nHorzRes, int nDivisor)
{
	CComboBox * pPrintRes = (CComboBox *)GetDlgItem (CB_HORZRES);
	CComboBox * pType= (CComboBox *)GetDlgItem (CB_TYPE);
	CArray <UINT, UINT> vDivide;
	bool bValve = ISVALVE ();

	ASSERT (pPrintRes);
	ASSERT (pType);

	pPrintRes->ResetContent ();

	HEADTYPE type = (HEADTYPE)pType->GetItemData (pType->GetCurSel ());

	#ifdef __IPPRINTER__
		if (m_head.m_bDigiNET) {
			vDivide.Add (1);
			vDivide.Add (2);
			vDivide.Add (3);
		}
		else {
			vDivide.Add (1);
			vDivide.Add (2);
			vDivide.Add (3);
			vDivide.Add (4);
			vDivide.Add (6);
		}
	#else
		vDivide.Add (1);
		vDivide.Add (2);

		if (bValve) {
			vDivide.RemoveAll ();

			for (int i = 4; i <= 25; i++)
				vDivide.Add (i);
		}

		if (IsProductionConfig () || IsUsbSelected ())
			vDivide.Add (3);

	#endif

	for (int i = 0; i < vDivide.GetSize (); i++) {
		int nActualDivsor = vDivide [i];
		int nRes = (nHorzRes * 2) / nActualDivsor;
		CString str;
		int nIndex;

		#ifdef __WINPRINTER__
		nRes = nHorzRes / nActualDivsor;

		if (IsProductionConfig () || IsUsbSelected ()) 
			if (nRes == 100)
				nRes = 200;
		#endif //__WINPRINTER__

		#ifdef __IPPRINTER__
		if (m_head.m_bDigiNET) {
			switch (nActualDivsor) {
			case 1:	nRes = nHorzRes;				break; // 300
			default:
			case 2:	nRes = (int)(nHorzRes / 2.0);	break; // 150
			case 3:	nRes = (int)(nHorzRes / 1.5);	break; // 200
			}
		}
		#endif

		str.Format (_T ("%d"), nRes);

		if (bValve) {
			CString strDebug;
			int nDivideBy = (nHorzRes * 2) / 6;
		
			double dRes = (double)nDivideBy / (double)nActualDivsor;
			str.Format (_T ("%.02f"), dRes);

			strDebug.Format (_T ("[%d] %.02f"), nActualDivsor, dRes);
			TRACEF (strDebug);
		}
		

		//int nIndex = pPrintRes->AddString (str);

		for (nIndex = 0; nIndex < pPrintRes->GetCount (); nIndex++) {
			CString strTmp;

			pPrintRes->GetLBText (nIndex, strTmp);

			if (_ttoi (strTmp) < nRes)
				break;
		}

		pPrintRes->InsertString (nIndex, str);
		pPrintRes->SetItemData (nIndex, nActualDivsor);

		if (nDivisor == nActualDivsor)
			pPrintRes->SetCurSel (nIndex);
	}

	if (pPrintRes->GetCurSel () == CB_ERR)
		pPrintRes->SetCurSel (0);
}

void CHeadDlg::FillAddress ()
{
#ifdef __WINPRINTER__
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_ADDRESS);
	CString strAddress, strCard;
	ULONG lHeadAddr = _tcstoul (m_head.m_strUID, NULL, 16);
	bool bValve = ISVALVE ();

	ASSERT (pCB);

	pCB->ResetContent ();

	{
		CString str;

		str.Format (_T ("%03X"), lHeadAddr);

		if (!m_vAvailAddressMap.Lookup (str, strCard)) 
			m_vAvailAddressMap [str] = GetPHC (lHeadAddr);
	}

	for (POSITION pos = m_vAvailAddressMap.GetStartPosition (); pos != NULL; ) {
		m_vAvailAddressMap.GetNextAssoc (pos, strAddress, strCard);

		int nIndex = pCB->AddString (strCard);
		ULONG lAddress = _tcstoul (strAddress, NULL, 16);
		pCB->SetItemData (nIndex, lAddress);

		if (lHeadAddr == lAddress)
			pCB->SetCurSel (nIndex);
	}

	/* TODO: rem
	if (m_vAvailAddressMap.GetCount () == 0) {
		const PHCSTRUCT * p = m_mapPHC;
		int nSize = ARRAYSIZE (m_mapPHC);

		if (bValve) {
			p = m_mapIVPHC;
			nSize = ARRAYSIZE (m_mapIVPHC);
		}

		for (int i = 0; i < nSize; i++) {
			if (p [i].m_lAddr == lHeadAddr) {
				int nIndex = pCB->AddString (p [i].m_lpsz);

				pCB->SetItemData (nIndex, p [i].m_lAddr);
				pCB->SetCurSel (nIndex);
			}
		}
	}
	*/

	if (pCB->GetCurSel () == CB_ERR) 
		pCB->SetCurSel (0);

#endif //__WINPRINTER__
}

typedef struct tagHEADTYPESTRUCT
{ 
	tagHEADTYPESTRUCT (UINT nType = GRAPHICS_768_256, UINT nID = IDS_GRAPHICS_768_256, bool bIsIV = false)
	:	m_nType (nType),
		m_nID (nID),
		m_bIsIV (bIsIV)
	{
	}


	UINT m_nType;
	UINT m_nID;
	bool m_bIsIV;
} HEADTYPESTRUCT;

void CHeadDlg::FillType ()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_TYPE);
	bool bValve = ISVALVE ();

	ASSERT (pCB);

	pCB->ResetContent ();
	
	/*
	struct 
	{ 
		UINT m_nType;
		UINT m_nID;
		bool m_bIsIV;
	} const map [] = 
	{
		{ UJ2_352_32,				IDS_UJ2_352_32,				false,	},
		{ UJ2_192_32,				IDS_UJ2_192_32,				false,	},
		{ UJ2_96_32,				IDS_UJ2_96_32,				false,	},
																
#if __CUSTOM__ != __SW0810__									
		{ GRAPHICS_768_256,			IDS_GRAPHICS_768_256,		false,	},
		{ GRAPHICS_384_128,			IDS_GRAPHICS_384_128,		false,	},
		#ifdef __WINPRINTER__ 
		{ GRAPHICS_768_256_L310,	IDS_GRAPHICS_768_256_L310,	false,	},
		#endif
#endif 

#if __CUSTOM__ == __SW0888__									
		{ GRAPHICS_768_256_L310,	IDS_GRAPHICS_768_256_L310,	false,	},
		{ GRAPHICS_768_256_0,		IDS_GRAPHICS_768_256_0,		false,	},
#endif

#ifdef __WINPRINTER__ 
		{ IV_72,					IDS_IV_72,					true,	},
#endif 

		{ UJ2_192_32NP,				IDS_UJ2_192_32NP,			false,	},
		{ ALPHA_CODER,				IDS_ALPHA_CODER,			false,	},

#ifdef __WINPRINTER__ 
		{ UJI_96_32,				IDS_UJI_96_32,				false,	},
		{ UJI_192_32,				IDS_UJI_192_32,				false,	},
		{ UJI_256_32,				IDS_UJI_256_32,				false,	},
#endif
	};
	*/
	CArray <HEADTYPESTRUCT, HEADTYPESTRUCT &> v, vAll;
	bool bNonGraphic = true;

	vAll.Add (HEADTYPESTRUCT (UJ2_352_32,				IDS_UJ2_352_32,				false));
	vAll.Add (HEADTYPESTRUCT (UJ2_192_32,				IDS_UJ2_192_32,				false));
	vAll.Add (HEADTYPESTRUCT (UJ2_96_32,				IDS_UJ2_96_32,				false));
	vAll.Add (HEADTYPESTRUCT (GRAPHICS_768_256,			IDS_GRAPHICS_768_256,		false));
	vAll.Add (HEADTYPESTRUCT (GRAPHICS_384_128,			IDS_GRAPHICS_384_128,		false));
	vAll.Add (HEADTYPESTRUCT (GRAPHICS_768_256_L310,	IDS_GRAPHICS_768_256_L310,	false));
	vAll.Add (HEADTYPESTRUCT (GRAPHICS_768_256_0,		IDS_GRAPHICS_768_256_0,		false));
	vAll.Add (HEADTYPESTRUCT (UJ2_192_32NP,				IDS_UJ2_192_32NP,			false));
	vAll.Add (HEADTYPESTRUCT (ALPHA_CODER,				IDS_ALPHA_CODER,			false));
	vAll.Add (HEADTYPESTRUCT (UJI_96_32,				IDS_UJI_96_32,				false));
	vAll.Add (HEADTYPESTRUCT (UJI_192_32,				IDS_UJI_192_32,				false));
	vAll.Add (HEADTYPESTRUCT (UJI_256_32,				IDS_UJI_256_32,				false));

	#ifdef __WINPRINTER__ 
	bNonGraphic = IsProductionConfig ();
	#endif

	if (bNonGraphic) {
		v.Add (HEADTYPESTRUCT (UJ2_352_32,				IDS_UJ2_352_32,				false));
		v.Add (HEADTYPESTRUCT (UJ2_192_32,				IDS_UJ2_192_32,				false));
		v.Add (HEADTYPESTRUCT (UJ2_96_32,				IDS_UJ2_96_32,				false));
	}

	#if __CUSTOM__ != __SW0810__									
		v.Add (HEADTYPESTRUCT (GRAPHICS_768_256,			IDS_GRAPHICS_768_256,		false));
		v.Add (HEADTYPESTRUCT (GRAPHICS_384_128,			IDS_GRAPHICS_384_128,		false));

		if (FoxjetDatabase::IsUltraResEnabled ()) {
			v.Add (HEADTYPESTRUCT (UR2,							IDS_UR2,					false));
			v.Add (HEADTYPESTRUCT (UR4,							IDS_UR4,					false));
		}

		#ifdef __WINPRINTER__ 
		v.Add (HEADTYPESTRUCT (GRAPHICS_768_256_L310,	IDS_GRAPHICS_768_256_L310,	false));
		#endif
	#endif 

	#if __CUSTOM__ == __SW0888__									
	v.Add (HEADTYPESTRUCT (GRAPHICS_768_256_L310,		IDS_GRAPHICS_768_256_L310,	false));
	v.Add (HEADTYPESTRUCT (GRAPHICS_768_256_0,			IDS_GRAPHICS_768_256_0,		false));
	#endif

	if (bValve)
		v.Add (HEADTYPESTRUCT (IV_72,					IDS_IV_72,					true));

	if (bNonGraphic) {
		v.Add (HEADTYPESTRUCT (UJ2_192_32NP,			IDS_UJ2_192_32NP,			false));
		v.Add (HEADTYPESTRUCT (ALPHA_CODER,				IDS_ALPHA_CODER,			false));

		#ifdef __WINPRINTER__ 
		v.Add (HEADTYPESTRUCT (UJI_96_32,				IDS_UJI_96_32,				false));
		v.Add (HEADTYPESTRUCT (UJI_192_32,				IDS_UJI_192_32,				false));
		v.Add (HEADTYPESTRUCT (UJI_256_32,				IDS_UJI_256_32,				false));
		#endif
	}

	for (int i = 0; i < v.GetSize (); i++) {
		HEADTYPESTRUCT h = v [i];

		#ifndef _DEBUG
		if (h.m_bIsIV != bValve) 
			continue;
		#endif

		int nIndex = pCB->AddString (LoadString (h.m_nID));
		int nType = h.m_nType;

		pCB->SetItemData (nIndex, nType);

		if (m_head.m_nHeadType == nType)
			pCB->SetCurSel (nIndex);
	}

	if (pCB->GetCurSel () == CB_ERR) {
		for (int i = 0; i < vAll.GetSize (); i++) {
			HEADTYPESTRUCT h = vAll [i];

			if (m_head.m_nHeadType == h.m_nType) {
				int nIndex = pCB->AddString (LoadString (h.m_nID));

				pCB->SetItemData (nIndex, h.m_nType);
				pCB->SetCurSel (nIndex);
				break;
			}
		}
	}

	if (pCB->GetCurSel () == CB_ERR)
		pCB->SetCurSel (0);

	OnTypeChanged ();
}

int CHeadDlg::CalcNFactor (double dAngle, double dRes)
{
	double x = 0.0;
//	double dRes = nHorzRes / nEncoderDivisor;
	double dNFactor = 0;
	bool bMore = true;
	const int nAngle = (int)(dAngle * 10.0);

	while (bMore) {
		x = dNFactor / (dRes * ::dChannelSeparation);
		
		if (bMore = (x >= 0.0) && (x < 0.97)) {
			double dCalcAngle = RAD2DEG (acos (x));
			int nCalcAngle = (int)(dCalcAngle * 10.0);

			if (nAngle == nCalcAngle)
				return (int)dNFactor;
		}
		
		dNFactor++;
	}

	return 0;
}

void CHeadDlg::OnTypeChanged ()
{
	CComboBox * pAngle = (CComboBox *)GetDlgItem (CB_ANGLE);
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	CComboBox * pPrintRes = (CComboBox *)GetDlgItem (CB_HORZRES);
	CComboBox * pEncRes = (CComboBox *)GetDlgItem (CB_ENCRES);
	CComboBox * pStandby = (CComboBox *)GetDlgItem (CB_STANDBY);
	CString str, str90, str15;
	LPCTSTR lpszFormat = _T ("%04.1f �");
	const double dOldAngle = m_head.m_dHeadAngle;
	bool bValve = ISVALVE ();

	str90.Format (lpszFormat, 90.0);
	str15.Format (lpszFormat, 14.3);

	ASSERT (pAngle);
	ASSERT (pType);
	ASSERT (pPrintRes);
	ASSERT (pEncRes);
	ASSERT (pStandby);
	
	const HEADTYPE type = (HEADTYPE)pType->GetItemData (pType->GetCurSel ());

	pStandby->EnableWindow (type == UJ2_192_32NP);
	pAngle->ResetContent ();

	const int nHorzRes				= pEncRes->GetItemData (pEncRes->GetCurSel ());
	TRACEF (ToString (pPrintRes->GetItemData (pPrintRes->GetCurSel ())));
	
	m_head.m_nChannels				= 32;
	m_head.m_dNozzleSpan			= GetNozzleSpan (type);

	#ifdef __WINPRINTER__
	if (!IsValveHead (m_head)) {
		bool bDoublePulseEnable = true;

		switch (type) {
		default:
			if (!IsProductionConfig () && !IsUsbSelected ())
				m_head.m_nEncoderDivisor = 2;
			break;
		case UJ2_96_32:
		case UJ2_192_32NP:
			m_head.m_nEncoderDivisor = 1;
			bDoublePulseEnable = false;
			break;
		}

		if (nHorzRes == 426)
			m_head.m_nEncoderDivisor = 2;

		if (CWnd * p = GetDlgItem (CHK_DOUBLEPULSE))
			p->EnableWindow (bDoublePulseEnable);
	}
	#endif //__WINPRINTER__

	FillHorzRes (m_head.m_nHorzRes, m_head.m_nEncoderDivisor);

	// NOTE: this can't be before FillHorzRes, or print res could be wrong
	const int nEncoderDivisor = pPrintRes->GetItemData (pPrintRes->GetCurSel ());

	m_head.m_nChannels = GetChannels (type);

	switch (type) {
	case IV_72:
		{
			double dAngle = 90.0;
			str.Format (lpszFormat, dAngle);
			int nIndex = pAngle->AddString (str);

			pAngle->SetItemData (nIndex, (int)(dAngle * 10.0));
			pAngle->SetCurSel (nIndex);
		}
		break;
	case GRAPHICS_384_128:
		{
			double dAngle = 90.0;
			str.Format (lpszFormat, dAngle);
			int nIndex = pAngle->AddString (str);

			pAngle->SetItemData (nIndex, (int)(dAngle * 10.0));
			pAngle->SetCurSel (nIndex);
		}
		break;
	case UR2:
	case UR4:
	case GRAPHICS_768_256:
	case GRAPHICS_768_256_L310:
	case UJ2_352_32:
	case UJI_256_32:
//	case ALPHA_CODER:
		{
			double dAngle = 90.0;
			str.Format (lpszFormat, dAngle);
			int nIndex = pAngle->AddString (str);
			pAngle->SetItemData (nIndex, (int)(dAngle * 10.0));
			pAngle->SetCurSel (nIndex);
		}
		break;
	default:
		{
			double x = 0.0;
			double dRes = nHorzRes / nEncoderDivisor;
			double dNFactor = 0;
			bool bMore = true;

			#ifdef __IPPRINTER__
			{
				CString str;
				int nSel = pPrintRes->GetCurSel ();

				if (nSel != CB_ERR) {
					pPrintRes->GetLBText (nSel, str);
					dRes = FoxjetDatabase::_ttof (str);
				}
			}
			#endif //__IPPRINTER__

			while (bMore) {
				x = dNFactor / (dRes * ::dChannelSeparation);
				
				double dAngle = RAD2DEG (acos (x));
				
				if (bMore = (x >= 0.0) && (x < 0.97)) {
					if (dAngle < 90.0) {
						int nAngle = (int)(dAngle * 10.0);

						str.Format (lpszFormat, (double)nAngle / 10.0);

						int nIndex = pAngle->AddString (str);
						pAngle->SetItemData (nIndex, (int)(dAngle * 10.0));
						
						#ifdef _DEBUG
						{
							CString strDebug, strHead;
							CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);

							if (pType->GetCurSel () != CB_ERR)	
								pType->GetLBText (pType->GetCurSel (), strHead);

							strDebug.Format (_T ("[%d, %d]: %s: %.01f [%d]"), 
								m_head.m_nHorzRes, m_head.m_nEncoderDivisor, 
								strHead, 
								dAngle, (int)dNFactor);
							//TRACEF (strDebug);
						}
						#endif
					}
				}
				
				dNFactor++;
			}

			if (pAngle->FindString (-1, str90) == CB_ERR) 
				pAngle->SetItemData (pAngle->InsertString (0, str90), 900);

			SelectClosestAngle (dOldAngle);

			if (m_bNew) {
				switch (type) {
				case UJ2_192_32:	SelectClosestAngle (37.0);		break;
				case UJ2_96_32:		SelectClosestAngle (24.0);		break;
				case UJ2_192_32NP:	SelectClosestAngle (15.0);		break;
				}
			}
		}

		break;
	}

	bool bValveHead = IsValveHead (type);

	{
		bool bEnable = true;
		
		#ifdef __WINPRINTER__
		bEnable = bValveHead;

		if (IsProductionConfig () || IsUsbSelected ())
			bEnable = true;
		#endif

		pPrintRes->EnableWindow (bEnable);
	}

	for (int i = 0; i < pPrintRes->GetCount (); i++) {
		int nDivisor = (int)pPrintRes->GetItemData (i);

		if (nDivisor == m_head.m_nEncoderDivisor) {
			pPrintRes->SetCurSel (i);
			break;
		}
	}

	if (pPrintRes->GetCurSel () == CB_ERR)
		pPrintRes->SetCurSel (0);

/*
	{
		const UINT nID [] = 
		{
			TXT_PHOTOCELLDELAY,
			TXT_RELATIVEHEIGHT,
			RDO_RTOL,
			RDO_LTOR,
			CHK_INVERT,
			CHK_DOUBLEPULSE,
		};

		for (i = 0; i < ARRAYSIZE (nID); i++)
			if (CWnd * p = GetDlgItem (nID [i]))
				p->EnableWindow (!bValveHead);
	}
*/

	switch (type) {
	case UJ2_192_32NP:		
	case UJ2_96_32:		
		SetDlgItemCheck (CHK_DOUBLEPULSE, false);				
		break;
	default:	
		SetDlgItemCheck (CHK_DOUBLEPULSE, m_head.m_bDoublePulse ? true : false);	
		break;
	}

	OnAngleChanged ();
	UpdateUI ();
}

int CHeadDlg::SelectClosestAngle (double dAngle) 
{
	CComboBox * pAngle = (CComboBox *)GetDlgItem (CB_ANGLE);
	const int nAngle = (int)(dAngle * 10.0);
	int nMinDiff = 0;
	int nIndex = 0;

	ASSERT (pAngle);

	const int nCount = pAngle->GetCount ();

	for (int i = 0; i < nCount; i++) {
		int n = pAngle->GetItemData (i);
		int nDiff = abs (nAngle - n);

		if (i == 0) 
			nMinDiff = nDiff;

		if (nDiff < nMinDiff) {
			nMinDiff = nDiff;
			nIndex = i;
		}
	}

	pAngle->SetCurSel (nIndex);

	return nIndex;
}

void CHeadDlg::OnAngleChanged ()
{
	UpdateUI ();
}

void CHeadDlg::UpdateUI ()
{
	CComboBox * pAngle				= (CComboBox *)	GetDlgItem (CB_ANGLE);
	CComboBox * pType				= (CComboBox *)	GetDlgItem (CB_TYPE);
	CButton *	pPhotocellShared	= (CButton *)	GetDlgItem (RDO_PHOTOCELLSHARED);
	CButton *	pEncoderShared		= (CButton *)	GetDlgItem (RDO_ENCODERSHARED);
	CButton *	pEncoderInternal	= (CButton *)	GetDlgItem (RDO_ENCODERINT);
	CButton *	pMaster				= (CButton *)	GetDlgItem (CHK_MASTER);

	ASSERT (pAngle);
	ASSERT (pType);
	ASSERT (pPhotocellShared);
	ASSERT (pEncoderShared);
	ASSERT (pMaster);

	bool bPHC = false;
	bool bUSB = IsUsbSelected ();
	bool bIntEnc = pEncoderInternal->GetCheck () == 1 ? true : false;
	const bool bMatrix = true; //IsMatrix ();

#ifdef __WINPRINTER__
	bPHC = true;
#endif

	struct
	{
		UINT m_nID;
		bool m_bVisible;
	} map [] = 
	{
		{ TXT_NAME,				true,						},
		{ CB_PANEL,				true,						},
		{ CB_HEAD,				true,						},
		{ CB_ANGLE,				true,						},
		{ TXT_PRINTHEIGHT,		true,						},
		{ TXT_PHOTOCELLDELAY,	true,						},	
		{ TXT_RELATIVEHEIGHT,	true,						},
		{ TXT_INTTACHSPEED,		true,						},
		{ CHK_MASTER,			true,						},	
		{ CHK_ENABLED,			true,						},
		{ IDB_DIRECTION,		true,						},	
		{ RDO_RTOL,				true,						},	
		{ RDO_LTOR,				true,						},	
		{ IDOK,					true,						},	
													
		{ TXT_PHOTOCELLRATE,	true,						},
		{ LBL_PHOTOCELLRATE,	true,						},
													
		{ CHK_DOUBLEPULSE,		bPHC || m_head.m_bDigiNET,	},	
		{ CB_ADDRESS,			bPHC,						},
		{ GRP_PHOTOCELLSHARED,	bPHC && !bUSB,				},
		{ GRP_ENCODERSHARED,	bPHC && !bUSB,				},
		{ RDO_PHOTOCELLEXT,		true,						},
		{ RDO_PHOTOCELLINT,		true,						},
		{ RDO_ENCODEREXT,		true,						},	
		{ RDO_ENCODERINT,		true,						},	
		{ CHK_INVERT,			true,						},
		{ CHK_UPSIDEDOWN,		bPHC,						},
		
		{ RDO_PHOTOCELLSHARED,	bPHC,						},
		{ RDO_PHOTOCELLSHAREDA,	bPHC && !bUSB,				},
		{ RDO_PHOTOCELLSHAREDB,	bPHC && !bUSB,				},
		{ RDO_ENCODERSHARED,	bPHC,						},	
		{ RDO_ENCODERSHAREDA,	bPHC && !bUSB,				},	
		{ RDO_ENCODERSHAREDB,	bPHC && !bUSB,				},	
															
		{ CB_PHOTOCELL,			bPHC && bUSB,				},	
		{ CB_ENCODER,			bPHC && bUSB,				},	

//		{ LBL_SERIALPARAMS,		true,						},		
//		{ TXT_SERIALPARAMS,		true,						},
//		{ BTN_SERIALPARAMS,		true,						},
															
		{ TXT_ADDRESS,			!bPHC,						},
		
		{ CB_HORZRES,			true,						},
		{ LBL_HORZRES,			true,						},
		
		{ BTN_SYNC,				!bPHC && !m_bNew && m_bSync },
		{ CHK_PCREPEATER,		!bPHC,						},

		{ LBL_ENCRES,			!bMatrix,					},
		{ CB_ENCRES,			!bMatrix,					},
		{ CB_STANDBY,			!bMatrix,					},
		{ LBL_STANDBY,			!bMatrix,					},
		{ LBL_ANGLE,			!bMatrix,					},
		{ CB_ANGLE,				!bMatrix,					},
		{ CHK_INVERT,			!bMatrix,					},
	};
	int nType = pType->GetItemData (pType->GetCurSel ());

	for (int i = 0; i < (sizeof (map) / sizeof (map [0])); i++) {
		if (CWnd * p = GetDlgItem (map [i].m_nID)) {
			if (!map [i].m_bVisible) 
				p->ShowWindow (SW_HIDE);
			else if (!p->IsWindowVisible ())
				p->ShowWindow (SW_SHOW);
			
			//if (map [i].m_nID == CHK_DOUBLEPULSE)
			//	TRACEF (_T ("CHK_DOUBLEPULSE: ") + CString (map [i].m_bVisible ? _T ("visible"): _T ("hide")));

			//p->EnableWindow (map [i].m_bEnabled && !m_bReadOnly);
			
			if (m_bReadOnly)
				p->EnableWindow (FALSE);
		}
	}

	HEADSTRUCT h;

	h.m_dHeadAngle	= (double)pAngle->GetItemData (pAngle->GetCurSel ()) / 10.0;
	h.m_dNozzleSpan = GetNozzleSpan ((HEADTYPE)nType); 

	if (!IsValveHead (m_head.m_nHeadType)) {
		HEADSTRUCT & head = m_head;
		/*
		HEADSTRUCT head (m_head);

		head.m_vValve.RemoveAll (); // so DDX_Coord doesn't use m_vValve in call to LogicalToThousandths, or ThousandthsToLogical
		head.m_nHeadType = HEADTYPE_FIRST;
		*/

		CCoord c (0, h.Height (), m_units, head);
	
		SetDlgItemText (TXT_PRINTHEIGHT, c.FormatY ());
	}
	else {
		CComboBox * pAddress = (CComboBox *)GetDlgItem (CB_ADDRESS);

		ASSERT (pAddress);

		ULONG lAddress = pAddress->GetItemData (pAddress->GetCurSel ());

		if (CWnd * p = GetDlgItem (BTN_SERIALPARAMS))
			p->EnableWindow (lAddress == 0x300);
	}

#ifdef __IPPRINTER__
	{
		CButton * pUse = (CButton *)GetDlgItem (CHK_PCREPEATER);
		CEdit * pLen = (CEdit *)GetDlgItem (TXT_PHOTOCELLRATE);

		ASSERT (pUse);
		ASSERT (pLen);

		pLen->EnableWindow (pUse->GetCheck () == 1);
	}

#endif

	if (CButton * pInternalPC = (CButton *)GetDlgItem (RDO_PHOTOCELLINT)) {
		if (CWnd * p = GetDlgItem (TXT_PHOTOCELLRATE)) 
			p->EnableWindow (pInternalPC->GetCheck () == 1);
	}

	if (m_bRestricted) {
		UINT n [] =
		{
			TXT_NAME,
			CB_PANEL,
			CB_ENCRES,
			CB_HORZRES,
			CB_TYPE,
			CB_ANGLE,
			TXT_PRINTHEIGHT,
			CB_ADDRESS,
			//TXT_PHOTOCELLDELAY,
			TXT_PHOTOCELLRATE,
			CHK_MASTER,
			CHK_DOUBLEPULSE,
			CHK_ENABLED,
			CHK_INVERT,
			CHK_UPSIDEDOWN,
			CHK_PCREPEATER,
			TXT_ADDRESS,
			TXT_RELATIVEHEIGHT,
			TXT_INTTACHSPEED,
			TXT_SERIALPARAMS,
			BTN_SERIALPARAMS,
			RDO_PHOTOCELLEXT,
			RDO_PHOTOCELLINT,
			RDO_PHOTOCELLSHARED,
			RDO_PHOTOCELLSHAREDNONE,
			RDO_PHOTOCELLSHAREDA,
			RDO_PHOTOCELLSHAREDB,
			RDO_ENCODEREXT,
			RDO_ENCODERINT,
			RDO_ENCODERSHARED,
			RDO_ENCODERSHAREDNONE,
			RDO_ENCODERSHAREDA,
			RDO_ENCODERSHAREDB,
			RDO_RTOL,
			RDO_LTOR,
			IDB_DIRECTION, 
			CB_STANDBY
		};

		for (int i = 0; i < ARRAYSIZE (n); i++)
			if (CWnd * p = GetDlgItem (n [i]))
				p->EnableWindow (FALSE);
	}

	if (IsNetworked ()) {
		CArray <ULONG, ULONG> v;

		v.Add (TXT_NAME);
		v.Add (CB_PANEL);
		v.Add (CB_TYPE);
		v.Add (CB_ADDRESS);
		v.Add (TXT_RELATIVEHEIGHT);
		v.Add (CB_HORZRES);
		//v.Add (CHK_MASTER);
		v.Add (BTN_MORE);

		if (IsControl (true)) {
			for (int i = 0; i < v.GetSize (); i++)
				if (CWnd * p = GetDlgItem (v [i]))
					p->EnableWindow (FALSE);
		}
		else {
			if (IsConfig (true)) { 
				if (CWnd * p = GetDlgItem (CHK_MASTER))
					p->EnableWindow (FALSE);
			}

			if (HWND hwndParent = m_hWnd) { //::GetParent (m_hWnd)) {
				#ifdef _DEBUG
				//{ CString str; str.Format (_T ("GetParent: 0x%p: %s"), hwndParent, GetClassName (hwndParent)); TRACEF (str); }
				#endif

				for (HWND hwndChild = ::GetWindow (hwndParent, GW_CHILD); hwndChild != NULL; hwndChild = ::GetWindow (hwndChild, GW_HWNDNEXT)) {
					ULONG lID = ::GetWindowLong (hwndChild, GWL_ID);

					switch (lID) {
					case IDOK:
					case IDCANCEL:
						break;
					default:
						if (Find (v, lID) == -1)
							if (CWnd * p = GetDlgItem (lID))
								p->EnableWindow (FALSE);
						break;
					}
				}
			}
		}
	}
}

double CHeadDlg::GetNozzleSpan (HEADTYPE type)
{
	switch (type) {
	case UR2:
	case GRAPHICS_384_128:			return 2.00;
	case UR4:
	case GRAPHICS_768_256:	
	case GRAPHICS_768_256_L310:		return 4.00;
	}

	return 1.813;
}

int CHeadDlg::GetChannels (HEADTYPE type)
{
	switch (type) {
	case UR2:						return 256;
	case UR4:						return 384;
	case IV_72:						return 72;
	case GRAPHICS_384_128:			return 128;
	case GRAPHICS_768_256:
	case GRAPHICS_768_256_L310:		return 256;
	default:						return 32;
	}

	return 256;
}


void CHeadDlg::OnDirection ()
{
	CButton * pRTOL = (CButton *)GetDlgItem (RDO_RTOL);
	CStatic * pBmp = (CStatic *)GetDlgItem (IDB_DIRECTION);

	ASSERT (pRTOL);
	ASSERT (pBmp);

	pBmp->SetBitmap (pRTOL->GetCheck () == 1 ? m_bmpRTOL : m_bmpLTOR);
}

void CHeadDlg::OnPhotocellSource ()
{
	CButton * pShared = (CButton *)GetDlgItem (RDO_PHOTOCELLSHARED);
	CButton * pInternal = (CButton *)GetDlgItem (RDO_PHOTOCELLINT);
	CButton * pExternal = (CButton *)GetDlgItem (RDO_PHOTOCELLEXT);
	CButton * pA = (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDA);
	CButton * pB = (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDB);
	CButton * pNone = (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDNONE);
	CComboBox * pAddress = (CComboBox *)GetDlgItem (CB_ADDRESS);
	CEdit * pAutoPrint = (CEdit *)GetDlgItem (TXT_PHOTOCELLRATE);

	ASSERT (pShared);
	ASSERT (pInternal);
	ASSERT (pExternal);
	ASSERT (pA);
	ASSERT (pB);
	ASSERT (pNone);
	ASSERT (pAddress);
	ASSERT (pAutoPrint);

	if (IsUsbSelected ()) {
		CComboBox * pCB = (CComboBox *)GetDlgItem (CB_PHOTOCELL);
		CButton * pShare = (CButton *)GetDlgItem (RDO_PHOTOCELLSHARED);

		ASSERT (pCB);
		ASSERT (pShare);
		
		pShared->SetWindowText (LoadString (IDS_SHAREDBY));
		pCB->EnableWindow (pShare->GetCheck () == 1);
		SelectUsbShare (CB_PHOTOCELL,	m_head.m_nSharePhoto > 2	? m_head.m_nSharePhoto	: (pShare->GetCheck () == 1 ? 0 : -1));
		UpdateUI ();
		return;
	}

#ifdef __WINPRINTER__
	ULONG lAddress = pAddress->GetItemData (pAddress->GetCurSel ());

	if (SOFTWARELIMITEDCONFIG && IsIsaCard (lAddress)) {
		if (lAddress == GetAddress (PHC_2)) {
			if (pShared->GetCheck () == 1) {
				pExternal->SetCheck (0);
				pInternal->SetCheck (0);
				pNone->SetCheck (0);
				pA->SetCheck (1);
				pB->SetCheck (0);
			}
			else {
				pNone->SetCheck (0);
				pA->SetCheck (0);
				pB->SetCheck (1);
			}
		}
	}

	pShared->SetWindowText (LoadString (pShared->GetCheck () == 0 ? IDS_SHAREDAS : IDS_SHAREDBY));
	pAutoPrint->EnableWindow (pInternal->GetCheck () == 1);
#endif

	UpdateUI ();
}

void CHeadDlg::OnEncoderSource ()
{
#ifdef __WINPRINTER__
	CButton * pShared = (CButton *)GetDlgItem (RDO_ENCODERSHARED);
	CButton * pInternal = (CButton *)GetDlgItem (RDO_ENCODERINT);
	CButton * pExternal = (CButton *)GetDlgItem (RDO_ENCODEREXT);
	CButton * pA = (CButton *)GetDlgItem (RDO_ENCODERSHAREDA);
	CButton * pB = (CButton *)GetDlgItem (RDO_ENCODERSHAREDB);
	CButton * pNone = (CButton *)GetDlgItem (RDO_ENCODERSHAREDNONE);
	CComboBox * pAddress = (CComboBox *)GetDlgItem (CB_ADDRESS);
	CEdit * pSpeed = (CEdit *)GetDlgItem (TXT_INTTACHSPEED);
	
	ASSERT (pShared);
	ASSERT (pInternal);
	ASSERT (pExternal);
	ASSERT (pA);
	ASSERT (pB);
	ASSERT (pNone);
	ASSERT (pAddress);

	pSpeed->EnableWindow (pInternal->GetCheck () == 1);

	if (IsUsbSelected ()) {
		CComboBox * pCB = (CComboBox *)GetDlgItem (CB_ENCODER);
		CButton * pShare = (CButton *)GetDlgItem (RDO_ENCODERSHARED);

		ASSERT (pCB);
		ASSERT (pShare);
		
		pCB->EnableWindow ((m_bRestricted || m_bReadOnly) ? FALSE : pShare->GetCheck () == 1);
		SelectUsbShare (CB_ENCODER,	m_head.m_nShareEnc > 2	? m_head.m_nShareEnc	: (pShare->GetCheck () == 1 ? 0 : -1));

		pShared->SetWindowText (LoadString (IDS_SHAREDBY));
		UpdateUI ();
		return;
	}

	ULONG lAddress = pAddress->GetItemData (pAddress->GetCurSel ());

	if (SOFTWARELIMITEDCONFIG && IsIsaCard (lAddress)) {
		if (lAddress == GetAddress (PHC_2)) {
			if (pShared->GetCheck () == 1) {
				pExternal->SetCheck (0);
				pInternal->SetCheck (0);
				pNone->SetCheck (0);
				pA->SetCheck (1);
				pB->SetCheck (0);
			}
			else {
				pNone->SetCheck (0);
				pA->SetCheck (0);
				pB->SetCheck (1);
			}
		}
	}

	pShared->SetWindowText (LoadString (pShared->GetCheck () == 0 ? IDS_SHAREDAS : IDS_SHAREDBY));

	UpdateUI ();
#endif //__WINPRINTER__
}


void CHeadDlg::OnMore ()
{
	{
		CComboBox * pAddress = (CComboBox *)GetDlgItem (CB_ADDRESS);

		ASSERT (pAddress);
		ULONG lAddress = pAddress->GetItemData (pAddress->GetCurSel ());
		m_head.m_strUID.Format (_T ("%X"), lAddress);
	}
#ifdef __WINPRINTER__
	CHeadExDlg dlg (m_db, m_head, m_units, m_vAvailAddressMap, m_lLineID, this);
#else
	CHeadExDlg dlg (m_db, m_head, m_units, m_lLineID, this);
#endif

	dlg.m_bReadOnly = m_bReadOnly;

	if (dlg.DoModal () == IDOK) {
		m_head = dlg.m_head;
		FillHorzRes (m_head.m_nHorzRes, m_head.m_nEncoderDivisor);
		UpdateUI ();
	}
}

void CHeadDlg::OnOK ()
{
	if (!m_bReadOnly) {
		if (!UpdateData ())
			return;

		CComboBox * pAngle = (CComboBox *)GetDlgItem (CB_ANGLE);
		CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
		CComboBox * pPanel = (CComboBox *)GetDlgItem (CB_PANEL);
		CComboBox * pStandby = (CComboBox *)GetDlgItem (CB_STANDBY);
		SETTINGSSTRUCT s;

		ASSERT (pPanel);
		ASSERT (pAngle);
		ASSERT (pType);
		ASSERT (pStandby);

		int nStandby = pStandby->GetItemData (pStandby->GetCurSel ());

		s.m_lLineID = 0;
		s.m_strKey.Format (_T ("%d, Standby mode"), m_head.m_lID);
		s.m_strData.Format (_T ("%d"), nStandby * 60);

//		if (!m_bRestricted)
			if (!UpdateSettingsRecord (m_db, s))
				VERIFY (AddSettingsRecord (m_db, s));

		m_head.m_dHeadAngle	= (double)pAngle->GetItemData (pAngle->GetCurSel ()) / 10.0;
		m_head.m_nHeadType = (HEADTYPE)pType->GetItemData (pType->GetCurSel ());
		m_head.m_lPanelID = (ULONG)pPanel->GetItemData (pPanel->GetCurSel ());

#ifdef __WINPRINTER__
		CComboBox * pAddress = (CComboBox *)GetDlgItem (CB_ADDRESS);

		ASSERT (pAddress);

		ULONG lAddress = pAddress->GetItemData (pAddress->GetCurSel ());

		m_head.m_strUID.Format (_T ("%X"), lAddress);

		CButton * pShareEncA = (CButton *)GetDlgItem (RDO_ENCODERSHAREDA);
		CButton * pShareEncB = (CButton *)GetDlgItem (RDO_ENCODERSHAREDB);
		CButton * pSharePhotoA = (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDA);
		CButton * pSharePhotoB = (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDB);

		ASSERT (pShareEncA);
		ASSERT (pShareEncB);
		ASSERT (pSharePhotoA);
		ASSERT (pSharePhotoB);

		m_head.m_nSharePhoto		= SHARED_PCNONE;
		m_head.m_nShareEnc			= SHARED_ENCNONE;
		m_head.m_bRemoteHead		= false;
		m_head.m_nEncoderDivisor	= 2;

		if (m_head.m_nPhotocell == -1)
			m_head.m_nPhotocell = EXTERNAL_PC;

		if (m_head.m_nEncoder == -1)
			m_head.m_nEncoder = EXTERNAL_ENC;

		if (!IsUsbSelected ()) {
			if (pSharePhotoB->GetCheck () == 1)
				m_head.m_nSharePhoto = SHARED_PCB;
			else if (pSharePhotoA->GetCheck () == 1)
				m_head.m_nSharePhoto = SHARED_PCA;

			if (pShareEncB->GetCheck () == 1)
				m_head.m_nShareEnc = SHARED_ENCB;
			else if (pShareEncA->GetCheck () == 1)
				m_head.m_nShareEnc = SHARED_ENCA;
		}
		else {
			CComboBox * pPhotoCB = (CComboBox *)GetDlgItem (CB_PHOTOCELL);
			CComboBox * pEncCB = (CComboBox *)GetDlgItem (CB_ENCODER);
			CArray <HEADSTRUCT, HEADSTRUCT &> v;

			ASSERT (pPhotoCB);
			ASSERT (pEncCB);

			GetLineHeads (m_db, m_lLineID, v);

			for (int i = v.GetSize () - 1; i >= 0; i--) {
				HEADSTRUCT & h = v [i];

				if (_tcstoul (h.m_strUID, NULL, 16) == _tcstoul (m_head.m_strUID, NULL, 16))
					v.RemoveAt (i);
			}

			if (v.GetSize () == 0 && !m_head.m_bMasterHead) {
				MsgBox (* this, LoadString (IDS_MUSTBEMASTER));
				return;
			}

			m_head.m_nSharePhoto	= pPhotoCB->IsWindowEnabled ()	&& pPhotoCB->GetCurSel () != CB_ERR		? pPhotoCB->GetItemData (pPhotoCB->GetCurSel ())	: m_head.m_nPhotocell;
			m_head.m_nShareEnc		= pEncCB->IsWindowEnabled ()	&& pEncCB->GetCurSel () != CB_ERR		? pEncCB->GetItemData (pEncCB->GetCurSel ())		: m_head.m_nEncoder;

			if (m_head.m_nSharePhoto >= 4)
				m_head.m_nPhotocell = SHARED_PC;

			if (m_head.m_nShareEnc >= 4)
				m_head.m_nEncoder = SHARED_ENC;

			TRACEF (m_head.m_strUID + _T (": pc:  ") + ToString (m_head.m_nSharePhoto));
			TRACEF (m_head.m_strUID + _T (": enc: ") + ToString (m_head.m_nShareEnc));
		}

		CComboBox * pEncRes			= (CComboBox *)GetDlgItem (CB_ENCRES);

		ASSERT (pEncRes);

		m_head.m_nHorzRes			= pEncRes->GetItemData (pEncRes->GetCurSel ());

		CComboBox * pPrintRes		= (CComboBox *)GetDlgItem (CB_HORZRES);
		ASSERT (pPrintRes);
		m_head.m_nEncoderDivisor	= pPrintRes->GetItemData (pPrintRes->GetCurSel ());

#else // if __IPPRINTER__
		CComboBox * pPrintRes		= (CComboBox *)GetDlgItem (CB_HORZRES);
		CComboBox * pEncRes			= (CComboBox *)GetDlgItem (CB_ENCRES);

		ASSERT (pPrintRes);
		ASSERT (pEncRes);

		m_head.m_nEncoderDivisor	= pPrintRes->GetItemData (pPrintRes->GetCurSel ());
		m_head.m_nHorzRes			= pEncRes->GetItemData (pEncRes->GetCurSel ());

		/* TODO: rem
		CString str; 
		CString strRes;
		pPrintRes->GetLBText (pPrintRes->GetCurSel (), strRes);
		str.Format ("%d [%d] = %s",
			m_head.m_nHorzRes,
			m_head.m_nEncoderDivisor,
			strRes);
		TRACEF (str);
		*/

		if (CButton * p = (CButton *)GetDlgItem (CHK_PCREPEATER)) 
			SetPcRepeater (m_db, m_head.m_lID, p->GetCheck () ? true : false);
#endif 

		if (!m_bNew) {
//			if (!m_bRestricted) 
				VERIFY (UpdateHeadRecord (m_db, m_head));
		}

		m_head.m_nChannels = m_head.GetChannels ();

		EndDialog (IDOK);
		//FoxjetCommon::CEliteDlg::OnOK ();
	}
}

void CHeadDlg::OnMaster ()
{
	CButton * pMaster = (CButton *)GetDlgItem (CHK_MASTER);
	CButton * pEnabled = (CButton *)GetDlgItem (CHK_ENABLED);

	ASSERT (pMaster);
	ASSERT (pEnabled);

	if (pMaster->GetCheck () == 1)
		pEnabled->SetCheck (1);

	OnSelChangeAddress ();
}

void CHeadDlg::OnUsePcRepeater ()
{
	UpdateUI ();
}

void CHeadDlg::OnEnabled ()
{
	CButton * pEnabled = (CButton *)GetDlgItem (CHK_ENABLED);
	CButton * pMaster = (CButton *)GetDlgItem (CHK_MASTER);

	ASSERT (pMaster);
	ASSERT (pEnabled);

	if (pEnabled->GetCheck () == 0) 
		pMaster->SetCheck (0);

	UpdateUI ();
}

#ifdef __WINPRINTER__
CString CHeadDlg::GetPHC (ULONG lAddr)
{
	const PHCSTRUCT * p = m_mapPHC;
	int nSize = ARRAYSIZE (m_mapPHC);
	bool bValve = ISVALVE ();

	if (bValve) {
		p = m_mapIVPHC;
		nSize = ARRAYSIZE (m_mapIVPHC);
	}

	for (int i = 0; i < nSize; i++) 
		if (p [i].m_lAddr == lAddr)
			return p [i].m_lpsz;

	{
		const PHCSTRUCT * p = m_mapUSB;
		int nSize = ARRAYSIZE (m_mapUSB);

		for (int i = 0; i < nSize; i++) 
			if (p [i].m_lAddr == lAddr)
				return p [i].m_lpsz;
	}

	return _T ("");
}

ULONG CHeadDlg::GetPHC (const CString & str)
{
	const PHCSTRUCT * p = m_mapPHC;
	int nSize = ARRAYSIZE (m_mapPHC);
	bool bValve = ISVALVE ();

	if (bValve) {
		p = m_mapIVPHC;
		nSize = ARRAYSIZE (m_mapIVPHC);
	}

	for (int i = 0; i < nSize; i++) 
		if (p [i].m_lpsz == str)
			return p [i].m_lAddr;


	{
		const PHCSTRUCT * p = m_mapUSB;
		int nSize = ARRAYSIZE (m_mapUSB);

		for (int i = 0; i < nSize; i++) 
			if (p [i].m_lpsz == str)
				return p [i].m_lAddr;
	}

	return 0;
}
#endif //__WINPRINTER__

void CHeadDlg::SetDlgItemCheck (UINT nID, bool bCheck)
{
	if (CButton * p = (CButton *)GetDlgItem (nID)) 
		p->SetCheck (bCheck ? 1 : 0);
}

bool CHeadDlg::GetDlgItemCheck (UINT nID) 
{
	if (CButton * p = (CButton *)GetDlgItem (nID)) 
		return p->GetCheck () == 1 ? true : false;

	return false;
}

void CHeadDlg::OnSelChangeAddress() 
{
#ifdef __WINPRINTER__
	CComboBox * pAddress = (CComboBox *)GetDlgItem (CB_ADDRESS);

	ASSERT (pAddress);

	UpdateUI ();

	ULONG lAddress = pAddress->GetItemData (pAddress->GetCurSel ());
	bool bShare = lAddress != GetAddress (PHC_1) && lAddress != GetAddress (PHC_2);

	CButton * pPhotoExt			= (CButton *)GetDlgItem (RDO_PHOTOCELLEXT);
	CButton * pPhotoInt			= (CButton *)GetDlgItem (RDO_PHOTOCELLINT);
	CButton * pPhotoShare		= (CButton *)GetDlgItem (RDO_PHOTOCELLSHARED);
	CButton * pPhotoShareNone	= (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDNONE);
	CButton * pPhotoShareA		= (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDA);
	CButton * pPhotoShareB		= (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDB);

	CButton * pEncExt			= (CButton *)GetDlgItem (RDO_ENCODEREXT);
	CButton * pEncInt			= (CButton *)GetDlgItem (RDO_ENCODERINT);
	CButton * pEncShare			= (CButton *)GetDlgItem (RDO_ENCODERSHARED);
	CButton * pEncShareNone		= (CButton *)GetDlgItem (RDO_ENCODERSHAREDNONE);
	CButton * pEncShareA		= (CButton *)GetDlgItem (RDO_ENCODERSHAREDA);
	CButton * pEncShareB		= (CButton *)GetDlgItem (RDO_ENCODERSHAREDB);

	ASSERT (pEncExt);
	ASSERT (pEncInt);
	ASSERT (pEncShare);
	ASSERT (pEncShareNone);
	ASSERT (pEncShareA);
	ASSERT (pEncShareB);

	ASSERT (pPhotoExt); 
	ASSERT (pPhotoInt); 
	ASSERT (pPhotoShare);
	ASSERT (pPhotoShareNone);
	ASSERT (pPhotoShareA);
	ASSERT (pPhotoShareB);

	if (SOFTWARELIMITEDCONFIG && IsIsaCard (lAddress)) {
		if (lAddress == GetAddress (PHC_1)) {
			pPhotoExt->EnableWindow (TRUE); 
			pPhotoInt->EnableWindow (TRUE); 
			pPhotoShare->EnableWindow (FALSE);
			pPhotoShareA->EnableWindow (FALSE);
			pPhotoShareB->EnableWindow (FALSE);

			pEncExt->EnableWindow (TRUE);
			pEncInt->EnableWindow (TRUE);
			pEncShare->EnableWindow (FALSE);
			pEncShareA->EnableWindow (FALSE);
			pEncShareB->EnableWindow (FALSE);
		}
		else if (lAddress == GetAddress (PHC_2)) {
			pPhotoExt->EnableWindow (TRUE); 
			pPhotoInt->EnableWindow (TRUE); 
			pPhotoShare->EnableWindow (TRUE);
			pPhotoShareA->EnableWindow (FALSE);
			pPhotoShareB->EnableWindow (FALSE);

			pEncExt->EnableWindow (TRUE);
			pEncInt->EnableWindow (TRUE);
			pEncShare->EnableWindow (TRUE);
			pEncShareA->EnableWindow (FALSE);
			pEncShareB->EnableWindow (FALSE);
		}
		else {
			pPhotoExt->EnableWindow (FALSE); 
			pPhotoInt->EnableWindow (FALSE); 
			pPhotoShare->EnableWindow (TRUE);
			pPhotoShareA->EnableWindow (TRUE);
			pPhotoShareB->EnableWindow (TRUE);

			pEncExt->EnableWindow (FALSE);
			pEncInt->EnableWindow (FALSE);
			pEncShare->EnableWindow (TRUE);
			pEncShareA->EnableWindow (TRUE);
			pEncShareB->EnableWindow (TRUE);
		}

		if (bShare) {
			if (pPhotoExt->GetCheck () || pPhotoInt->GetCheck ()) {
				pPhotoExt->SetCheck (0);
				pPhotoInt->SetCheck (0);
				pPhotoShare->SetCheck (1);
				pPhotoShareNone->SetCheck (0);
				pPhotoShareA->SetCheck (1);
				pPhotoShareB->SetCheck (0);
			}

			if (pEncExt->GetCheck () || pEncInt->GetCheck ()) {
				pEncExt->SetCheck (0);
				pEncInt->SetCheck (0);
				pEncShare->SetCheck (1);
				pEncShareNone->SetCheck (0);
				pEncShareA->SetCheck (1);
				pEncShareB->SetCheck (0);
			}
		}
		else {
			if (lAddress == GetAddress (PHC_1)) {
				if (pPhotoShare->GetCheck () || pPhotoShareA->GetCheck () || pPhotoShareB->GetCheck ()) {
					pPhotoExt->SetCheck (1);
					pPhotoInt->SetCheck (0);
				}

				if (pEncShare->GetCheck () || pEncShareA->GetCheck () || pEncShareB->GetCheck ()) {
					pEncExt->SetCheck (1);
					pEncInt->SetCheck (0);
				}

				pPhotoShare->SetCheck (0);
				pPhotoShareNone->SetCheck (0);
				pPhotoShareA->SetCheck (1);
				pPhotoShareB->SetCheck (0);

				pEncShare->SetCheck (0);
				pEncShareNone->SetCheck (0);
				pEncShareA->SetCheck (1);
				pEncShareB->SetCheck (0);
			}

			if (lAddress == GetAddress (PHC_2)) {
				if (pPhotoShare->GetCheck ()) {
					pPhotoExt->SetCheck (0);
					pPhotoInt->SetCheck (0);
					pPhotoShare->SetCheck (0);
					pPhotoShareNone->SetCheck (0);
					pPhotoShareA->SetCheck (1);
					pPhotoShareB->SetCheck (0);
				}
				else {
					if (!pPhotoExt->GetCheck () && !pPhotoInt->GetCheck ()) {
						pPhotoExt->SetCheck (0);
						pPhotoInt->SetCheck (0);
					}

					pPhotoShare->SetCheck (0);
					pPhotoShareNone->SetCheck (0);
					pPhotoShareA->SetCheck (1);
					pPhotoShareB->SetCheck (0);
				}

				if (pEncShare->GetCheck ()) {
					pEncExt->SetCheck (0);
					pEncInt->SetCheck (0);
					pEncShare->SetCheck (0);
					pEncShareNone->SetCheck (0);
					pEncShareA->SetCheck (1);
					pEncShareB->SetCheck (0);
				}
				else {
					if (!pEncExt->GetCheck () && !pEncInt->GetCheck ()) {
						pEncExt->SetCheck (0);
						pEncInt->SetCheck (0);
					}

					pEncShare->SetCheck (0);
					pEncShareNone->SetCheck (0);
					pEncShareA->SetCheck (1);
					pEncShareB->SetCheck (0);
				}
			}
		}
	}
	else {
		if (IsNetworked () && !IsControl ())
			return;

		CButton * pMaster = (CButton *)GetDlgItem (CHK_MASTER);
		CComboBox * pPhotoCB = (CComboBox *)GetDlgItem (CB_PHOTOCELL);
		CComboBox * pEncCB = (CComboBox *)GetDlgItem (CB_ENCODER);

		if (pMaster && pPhotoCB && pEncCB) {
			pPhotoShare->EnableWindow (TRUE);
			pEncShare->EnableWindow (TRUE);
			
			FillUsbShare (CB_PHOTOCELL, m_head.m_nSharePhoto);
			FillUsbShare (CB_ENCODER, m_head.m_nShareEnc);
			OnPhotocellSource ();
			OnEncoderSource ();
		}
	}

	
	UpdateUI ();
	OnEncoderSource ();
	OnPhotocellSource ();
	OnTypeChanged ();
#endif //__WINPRINTER__
}


#ifdef __IPPRINTER__
DWORD CHeadDlg::GetSyncSettings (FoxjetDatabase::COdbcDatabase & db)
{
	DBMANAGETHREADSTATE (db);
	using namespace FoxjetIpElements;

	SETTINGSSTRUCT s;
	DWORD dw = CSyncDlg::SYNC_FONTS | CSyncDlg::SYNC_MESSAGES;

	if (GetSettingsRecord (db, 0, m_strSyncKey, s))
		dw = _tcstoul (s.m_strData, NULL, 16);

	return dw;
}
#endif //__IPPRINTER__

void CHeadDlg::OnSync ()
{
#ifdef __IPPRINTER__
	using namespace FoxjetIpElements;

	SETTINGSSTRUCT s;
	DWORD dw = CSyncDlg::SYNC_SHOW_DELETE | CSyncDlg::SYNC_SHOW_ALLHEADS;

	if (GetSettingsRecord (m_db, 0, m_strSyncKey, s))
		dw |= _tcstoul (s.m_strData, NULL, 16);

	CSyncDlg dlg (dw, 0, this);

	dlg.m_strPrompt = LoadString (IDS_SYNCNOW);

	if (dlg.DoModal () == IDOK) {
		HWND hWnd = NULL;
		ULONG lHeadID = m_head.m_lID;

		dw = 0;
		dw |= dlg.m_bFonts		? CSyncDlg::SYNC_FONTS				: 0;
		dw |= dlg.m_bMessages	? CSyncDlg::SYNC_MESSAGES			: 0;
		dw |= dlg.m_bDelete		? CSyncDlg::SYNC_DELETE_MEMSTORE	: 0;
		dw |= CSyncDlg::SYNC_NO_PROMPT;
		dw |= CSyncDlg::SYNC_NOTIFY_COMPLETE;

		if (dlg.m_bAllHeads)
			lHeadID = -1;

		if (CWnd * pConfigDlg = GetParent ())
			if (CWnd * pParent = pConfigDlg->GetParent ())
				hWnd = pParent->m_hWnd;

//		::PostMessage (hWnd, WM_SYNCREMOTEHEAD, lHeadID, dw);
		{
			DWORD dwResult = 0;
			LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_SYNCREMOTEHEAD, lHeadID, dw,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dwResult);
		}

		if (m_head.m_bDigiNET) {
			UINT n [] = { IDOK, /* IDCANCEL, */ BTN_SYNC };

			for (int i = 0; i < ARRAYSIZE (n); i++)
				if (CWnd * p = GetDlgItem (n [i]))
					p->EnableWindow (FALSE);
		}
	}

#endif //__IPPRINTER__
}


LRESULT CHeadDlg::OnSyncComplete (WPARAM wParam, LPARAM lParam)
{
	if (wParam == m_head.m_lID) {
		UINT n [] = { IDOK, /* IDCANCEL, */ BTN_SYNC };

		for (int i = 0; i < ARRAYSIZE (n); i++)
			if (CWnd * p = GetDlgItem (n [i]))
				p->EnableWindow (TRUE);
	}

	return WM_SYNCREMOTEHEADCOMPLETE;
}

CString CHeadDlg::ConfigToString (const FoxjetDatabase::HEADSTRUCT & h)
{
	CStringArray v;

//	v.Add (ToString (h.m_bDoublePulse));
//	v.Add (ToString ((int)h.m_nIntTachSpeed));
	v.Add (ToString (h.m_nEncoderDivisor));
	v.Add (ToString (h.m_bEnabled));
	v.Add (ToString (h.m_nHeadType));
	v.Add (ToString (h.m_bInverted));
	v.Add (ToString (h.m_nEncoder));
	v.Add (ToString (h.m_nShareEnc));
	v.Add (ToString (h.m_nPhotocell));
	v.Add (ToString (h.m_nSharePhoto));
	v.Add (ToString ((int)h.m_nHorzRes));
	v.Add (ToString (h.m_dPhotocellRate));
	v.Add (ToString (h.m_nDirection));

	return ToString (v);
}

bool CHeadDlg::IsUsbSelected ()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_ADDRESS);
	ASSERT (pCB);
	ULONG lAddr = pCB->GetItemData (pCB->GetCurSel ());
	
	if (lAddr == CB_ERR)
		lAddr = _tcstoul (m_head.m_strUID, NULL, 16);

	return lAddr < 0x300;
}

void CHeadDlg::SelectUsbShare (UINT nID, int nSelected)
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (nID);

	ASSERT (pCB);
	pCB->SetCurSel (nSelected < 0 ? -1 : 0);

	for (int i = 0; i < pCB->GetCount (); i++) {
		DWORD dw = pCB->GetItemData (i);

		if (dw == (DWORD)nSelected) {
			pCB->SetCurSel (i);
			break;
		}
	}
}

void CHeadDlg::FillUsbShare (UINT nID, int nSelected)
{
	CArray <HEADSTRUCT, HEADSTRUCT &> v;
	CComboBox * pAddress = (CComboBox *)GetDlgItem (CB_ADDRESS);
	CComboBox * pCB = (CComboBox *)GetDlgItem (nID);

	ASSERT (pCB);
	ASSERT (pAddress);
	pCB->ResetContent ();
	ULONG lSelf = pAddress->GetItemData (pAddress->GetCurSel ());

	GetPrinterHeads (m_db, GetPrinterID (m_db), v); //GetLineHeads (m_db, m_lLineID, v);	

	if (lSelf >= 5) {
		for (int i = v.GetSize () - 1; i >= 0; i--) {
			HEADSTRUCT & h = v [i];
			ULONG lAddr = _tcstoul (h.m_strUID, NULL, 16);

			if (lAddr == lSelf || lAddr < 5) 
				v.RemoveAt (i);
		}
	}

	for (int i = 0; i < v.GetSize (); i++) {
		HEADSTRUCT & h = v [i];
		ULONG lAddr = _tcstoul (h.m_strUID, NULL, 16);

		if (lAddr != lSelf) {
			int nIndex = pCB->AddString (h.m_strName);
			pCB->SetItemData (nIndex, lAddr + 3);
			//TRACEF (ToString (lAddr) + _T (": ") + ToString ((int)pCB->GetItemData (nIndex)));
			{ CString str; str.Format (_T ("[%d]: %s: %s"), h.m_lID, h.m_strName, h.m_strUID); TRACEF (str); }
		}
	}

	SelectUsbShare (nID, nSelected);
}