// ScanReportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "Control.h"
#include "ScanReportDlg.h"
#include "ListCtrlImp.h"
#include "Database.h"
#include "ReportSettingsDlg.h"
#include "Debug.h"
#include "FileExt.h"

using namespace ItiLibrary;
using namespace FoxjetDatabase;
using namespace Foxjet3d;
using namespace ScanReportDlg;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const LPCTSTR CScanReportDlg::m_lpszSettingsKey = _T ("CScanReportDlg::AutoExport");
const LPCTSTR CScanReportDlg::m_lpszSettingsKeyPath = _T("CScanReportDlg::ExportPath");

bool UTIL_API Foxjet3d::IsScanReportLoggingEnabled (FoxjetDatabase::COdbcDatabase & db, ULONG lPrinterID)
{
	SETTINGSSTRUCT s;

	if (GetSettingsRecord (db, lPrinterID, _T ("Control::scan report"), s))
		return _ttoi (s.m_strData) ? true : false;

	return false;
}

/////////////////////////////////////////////////////////////////////////////

CReportItem::CReportItem (const CString & strLine, const FoxjetDatabase::BCSCANSTRUCT & s)
:	m_rpt (s),
	m_strLine (strLine)
{
}

CReportItem::~CReportItem ()
{
}

CString CReportItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0:		
		str = m_rpt.m_dtTime.Format (_T ("%c"));		
		break;
	case 1:		return m_strLine;
	case 2:		return m_rpt.m_strTaskname;
	case 3:		return m_rpt.m_strBarcode;
	case 4:
		str.Format (_T ("%d"), m_rpt.m_lTotalScans);
		break;
	case 5:	
		str.Format (_T ("%d"), m_rpt.m_lGoodScans);
		break;
	}

	return str;
}

int CReportItem::Compare (const ItiLibrary::ListCtrlImp::CItem & cmp, int nColumn) const
{
	CReportItem & rhs = (CReportItem &)cmp;

	switch (nColumn) {
	case 0:		
		if (m_rpt.m_dtTime == rhs.m_rpt.m_dtTime)
			return 0;
		if (m_rpt.m_dtTime > rhs.m_rpt.m_dtTime)
			return 1;
		else 
			return -1;
	case 4:		return m_rpt.m_lTotalScans	- rhs.m_rpt.m_lTotalScans;
	case 5:		return m_rpt.m_lGoodScans	- rhs.m_rpt.m_lGoodScans;
	}

	return CItem::Compare (rhs, nColumn);
}

/////////////////////////////////////////////////////////////////////////////
// CScanReportDlg dialog


CScanReportDlg::CScanReportDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent /*=NULL*/)
:	m_db (db),
	FoxjetCommon::CEliteDlg (IDD_SCAN_REPORT_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CScanReportDlg)
	//}}AFX_DATA_INIT
	m_sLine = _T("");
	m_lPrinterID = GetPrinterID (db);
	m_bEnable = false;
}


void CScanReportDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CScanReportDlg)
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, IDC_CLEAR);
	m_buttons.DoDataExchange (pDX, BTN_EXPORT);
	m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);
}


BEGIN_MESSAGE_MAP(CScanReportDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CScanReportDlg)
	ON_BN_CLICKED(IDC_CLEAR, OnClear)
	ON_BN_CLICKED(BTN_EXPORT, OnExport)
	ON_COMMAND(ID_FILE_SETTINGS, OnFileSettings)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CScanReportDlg message handlers

BOOL CScanReportDlg::OnInitDialog() 
{
	using ListCtrlImp::CColumn;

	CArray <CColumn, CColumn> vCols;
	const ULONG lLineID = FoxjetDatabase::GetLineID (m_db, m_sLine, m_lPrinterID);

	FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_DATE)));
	vCols.Add (CColumn (LoadString (IDS_LINE)));
	vCols.Add (CColumn (LoadString (IDS_TASKNAME)));
	vCols.Add (CColumn (LoadString (IDS_BARCODE)));
	vCols.Add (CColumn (LoadString (IDS_TOTAL)));
	vCols.Add (CColumn (LoadString (IDS_GOOD)));

	m_lv.Create (LV_REPORT, vCols, this, _T ("Software\\FoxJet\\Control"));

	DWORD dwStyle = m_lv->GetExtendedStyle (); 
	m_lv->SetExtendedStyle (dwStyle| LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	{
		CArray <UINT, UINT> vID;

		if (m_sLine.IsEmpty ()) {
			CArray <LINESTRUCT, LINESTRUCT &> vLines;

			GetPrinterLines (m_db, m_lPrinterID, vLines);

			for (int i = 0; i < vLines.GetSize (); i++)
				vID.Add (vLines [i].m_lID);
		}
		else
			vID.Add (FoxjetDatabase::GetLineID (m_db, m_sLine, m_lPrinterID));

		for (int i = 0; i < vID.GetSize (); i++) {
			CArray <BCSCANSTRUCT, BCSCANSTRUCT &> v;
			LINESTRUCT line;

			GetLineRecord (m_db, vID [i], line);

			FoxjetDatabase::GetScanRecords (m_db, vID [i], v);

			for (int i = 0; i < v.GetSize(); i++) 
				m_lv.InsertCtrlData (new CReportItem (line.m_strName, v [i]));
		}
	}

	
	if (CButton * p = (CButton *)GetDlgItem (CHK_AUTOEXPORT)) 
		p->SetCheck (IsAutoExport (m_db, m_lPrinterID) ? 1 : 0);

	if (CButton * p = (CButton *)GetDlgItem (CHK_ENABLED)) 
		p->SetCheck (IsScanReportLoggingEnabled (m_db, m_lPrinterID) ? 1 : 0);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CScanReportDlg::OnClear() 
{
	if (MsgBox (* this, LoadString (IDS_CLEARREPORTS), MB_YESNO ) == IDYES )
	{
		CArray <UINT, UINT> vID;

		if (m_sLine.IsEmpty ()) {
			CArray <LINESTRUCT, LINESTRUCT &> vLines;

			GetPrinterLines (m_db, m_lPrinterID, vLines);

			for (int i = 0; i < vLines.GetSize (); i++)
				vID.Add (vLines [i].m_lID);
		}
		else
			vID.Add (FoxjetDatabase::GetLineID (m_db, m_sLine, m_lPrinterID));

		for (int i = 0; i < vID.GetSize (); i++)
			FoxjetDatabase::ClearScanRecords (m_db, vID [i]);

		m_lv.DeleteCtrlData ();
	}
}

void CScanReportDlg::OnExport() 
{
	CString strTitle, strFile;
	FoxjetDatabase::SETTINGSSTRUCT s;

	GetSettingsRecord(m_db, 0, m_lpszSettingsKeyPath, s);

	if (!s.m_strData.GetLength())
		s.m_strData = GetHomeDir();

	GetWindowText(strTitle);
	strFile.Format(_T("%s\\%s [%s].txt"), s.m_strData, strTitle, m_sLine);

	CFileDialog dlg (FALSE, _T ("*.*"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
		_T ("All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();

		if (ItiLibrary::Export(m_lv, strFile)) {
			FoxjetDatabase::SETTINGSSTRUCT s;

			s.m_lLineID = 0;
			s.m_strKey = m_lpszSettingsKeyPath;
			s.m_strData = FoxjetFile::GetPath(strFile);

			if (!UpdateSettingsRecord(m_db, s))
				AddSettingsRecord(m_db, s);
		}
		else {
			CString str;

			str.Format (LoadString (IDS_EXPORTFAILED), strFile);
			MsgBox (* this, str);
		}
	}
}

bool CScanReportDlg::IsAutoExport (FoxjetDatabase::COdbcDatabase & db, ULONG lPrinterID)
{
	SETTINGSSTRUCT s;

	GetSettingsRecord (db, lPrinterID, m_lpszSettingsKey, s);

	return _ttoi (s.m_strData) == 1 ? true : false;
}

void CScanReportDlg::Export (FoxjetDatabase::COdbcDatabase & db, const CString & strLine, ULONG lPrinterID)
{
	CScanReportDlg dlg (db);
	CString strFile;

	dlg.m_sLine = strLine;
	dlg.m_lPrinterID = lPrinterID;
	dlg.Create (IDD_SCAN_REPORT_MATRIX);
	dlg.GetWindowText (strFile);

	{
		FoxjetDatabase::SETTINGSSTRUCT s;

		GetSettingsRecord(db, 0, m_lpszSettingsKeyPath, s);

		if (!s.m_strData.GetLength())
			s.m_strData = GetHomeDir();

		strFile = s.m_strData + _T("\\") + strFile + _T(" [") + dlg.m_sLine + _T("].txt");
		TRACEF(strFile);
	}


	if (!ItiLibrary::Export (dlg.m_lv, strFile)) {
		CString str;

		str.Format (LoadString (IDS_EXPORTFAILED), strFile);
		MsgBox (str);
	}

	dlg.DestroyWindow ();
}

void CScanReportDlg::OnOK ()
{
	if (CButton * p = (CButton *)GetDlgItem (CHK_AUTOEXPORT)) {
		SETTINGSSTRUCT s;

		s.m_strKey	= m_lpszSettingsKey;
		s.m_lLineID = m_lPrinterID;
		s.m_strData = p->GetCheck () == 0 ? _T ("0") : _T ("1");

		if (!UpdateSettingsRecord (m_db, s))
			VERIFY (AddSettingsRecord (m_db, s));
	}

	if (CButton * p = (CButton *)GetDlgItem (CHK_ENABLED)) 
		m_bEnable = p->GetCheck () == 1 ? true : false;

	FoxjetCommon::CEliteDlg::OnOK ();
}

void CScanReportDlg::OnFileSettings() 
{
	CReportSettingsDlg (m_db, _T ("CScanReportDlg"), this).DoModal ();	
}
