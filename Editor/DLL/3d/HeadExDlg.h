#if !defined(AFX_HEADEXDLG_H__753C8DAF_52DE_4BB3_A433_7E6ADAA23A84__INCLUDED_)
#define AFX_HEADEXDLG_H__753C8DAF_52DE_4BB3_A433_7E6ADAA23A84__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HeadExDlg.h : header file
//

#include "Database.h"
#include "List.h"
#include "3dApi.h"
#include "RoundButtons.h"
#include "Utils.h"


namespace Foxjet3d
{
	/////////////////////////////////////////////////////////////////////////////
	// CHeadExDlg dialog

	class CHeadExDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CHeadExDlg(FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::HEADSTRUCT & head, 
			UNITS units, 
#ifdef __WINPRINTER__
			const CMapStringToString & vAvailAddressMap, 
#endif //__WINPRINTER__
			ULONG lLineID, CWnd* pParent = NULL);   // standard constructor

		bool m_bReadOnly;
		FoxjetDatabase::HEADSTRUCT m_head;

	// Dialog Data
		//{{AFX_DATA(CHeadExDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA


	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CHeadExDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		FoxjetUtils::CRoundButtons m_buttons;

		virtual void OnOK();
		FoxjetDatabase::COdbcDatabase & m_db;
		FoxjetDatabase::PANELSTRUCT m_panel;
		const UNITS m_units;
		ULONG m_lLineID;
		CMapStringToString m_vAvailAddressMap;

		// Generated message map functions
		//{{AFX_MSG(CHeadExDlg)
		afx_msg void OnSerialparams();
		virtual BOOL OnInitDialog();
	afx_msg void OnUpdateUI();
	//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEADEXDLG_H__753C8DAF_52DE_4BB3_A433_7E6ADAA23A84__INCLUDED_)
