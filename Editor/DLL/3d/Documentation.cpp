// Documentation.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "Documentation.h"
#include "Extern.h"
#include "Resource.h"
#include "Debug.h"
#include "Parse.h"
#include "SystemDlg.h"

using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace Foxjet3d;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDocumentationPage property page

IMPLEMENT_DYNCREATE(CDocumentationPage, CPropertyPage)

CDocumentationPage::CDocumentationPage() 
:	m_hbrDlg (NULL),
	CPropertyPage(IDD_DOCUMENTATION_MATRIX)
{
	//{{AFX_DATA_INIT(CDocumentationPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CDocumentationPage::~CDocumentationPage()
{
	if (m_hbrDlg) {
		::DeleteObject (m_hbrDlg);
		m_hbrDlg = NULL;
	}
}

void CDocumentationPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDocumentationPage)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDocumentationPage, CPropertyPage)
	//{{AFX_MSG_MAP(CDocumentationPage)
	//}}AFX_MSG_MAP
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDocumentationPage message handlers

BOOL CDocumentationPage::OnInitDialog() 
{

	CEdit * pEdit = (CEdit *)GetDlgItem (TXT_TEXT);
	TCHAR szCrLf [] = { '\r', '\n', 0 };
	CString str;
	
	ASSERT (pEdit);
	pEdit->SetTabStops (12);

	CPropertyPage::OnInitDialog();
	
	for (int nElement = 0; nElement < GetElementTypeCount (); nElement++) {
		if (IsValidElementType (nElement, FoxjetDatabase::GetDefaultHead ())) {
			const CBaseElement & e = defElements.GetElement (nElement);
			const ElementFields::FIELDSTRUCT * pFields = e.GetFieldBuffer ();

			str += a2w (e.GetRuntimeClass ()->m_lpszClassName);
			str += szCrLf;

			for (int nField = 0; nField < e.GetFieldCount (); nField++) {
				CString strField = pFields [nField].m_strField;
				CString strDef = pFields [nField].m_strDef;

				str += _T ("\t") + FoxjetDatabase::ToString (nField) + _T (":");
				str += _T ("\t") + strField;

				if (strDef.GetLength ()) 
					str += _T ("\t[") + strDef + _T ("]");

				str += szCrLf;
			}

			str += szCrLf;
		}
	}

	SetDlgItemText (TXT_TEXT, str);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

HBRUSH CDocumentationPage::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	COLORREF rgbBack = ::GetSysColor (COLOR_BTNFACE);//FoxjetUtils::GetButtonColor (_T ("background"));
	
	if (FoxjetDatabase::IsControl (false)) 
		rgbBack = FoxjetUtils::GetButtonColor (_T ("background"));

	if (!m_hbrDlg)	
		m_hbrDlg = ::CreateSolidBrush (rgbBack);

	switch(nCtlColor) {
	case CTLCOLOR_DLG:     
	case CTLCOLOR_STATIC:  
		pDC->SetBkColor (rgbBack);
		hbr = m_hbrDlg;
		break;
	}
	
	return hbr;
}
