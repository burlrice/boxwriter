// ProdArea.h: interface for the CProductArea class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PRODAREA_H__8E465047_3D14_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_PRODAREA_H__8E465047_3D14_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "3dApi.h"

class _3D_API CProductArea : public CRect  
{
public:
	CProductArea ();
	CProductArea (const CSize & rhs);
	CProductArea (const CRect & rhs);
	CProductArea (const CProductArea & rhs);
	CProductArea & operator = (const CProductArea & rhs);
	virtual ~CProductArea();

	CPoint ClientToProd (const CPoint & pt, double dZoom) const;
	CRect ClientToProd (const CRect &rc, double dZoom) const;

	CPoint ProdToClient (const CPoint & pt, double dZoom) const;
	CRect ProdToClient (const CRect &rc, double dZoom) const;
};

#endif // !defined(AFX_PRODAREA_H__8E465047_3D14_11D4_8FC6_006067662794__INCLUDED_)
