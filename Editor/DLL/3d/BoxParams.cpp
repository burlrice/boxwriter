#include "stdafx.h"
#include "3d.h"
#include "Box.h"
#include "Debug.h"
#include "Registry.h"
#include "Compare.h"
#include "Element.h"

#include <math.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace Foxjet3d;
using namespace Foxjet3d::Box;
using namespace DxParams;
using namespace Box;

IMPLEMENT_DYNAMIC (CBoxParams, CDxParams)

_3D_API ULONG Foxjet3d::Box::Compare (const Foxjet3d::Box::CBoxParams & lhs, const Foxjet3d::Box::CBoxParams & rhs)
{
	CBoxParams * pLhs = (CBoxParams *)(LPVOID)&lhs;
	CBoxParams * pRhs = (CBoxParams *)(LPVOID)&rhs;

	ULONG lResult = Compare ((CDxParams &)lhs, (CDxParams &)rhs);

	lResult |= lhs.m_nPanel					!= rhs.m_nPanel				? BoxParams::SELECTEDPANEL		: 0;
	lResult |= lhs.m_lHeadID				!= rhs.m_lHeadID			? BoxParams::SELECTEDHEAD		: 0;
	lResult |= lhs.m_ptOffset				!= rhs.m_ptOffset			? BoxParams::OFFSET			: 0;
	lResult |= lhs.m_dRotation [0]			!= rhs.m_dRotation [0]		? BoxParams::ROTATION			: 0;
	lResult |= lhs.m_dRotation [1]			!= rhs.m_dRotation [1]		? BoxParams::ROTATION			: 0;
	lResult |= lhs.m_uBlend					!= rhs.m_uBlend				? BoxParams::BLEND				: 0;
//	lResult |= lhs.m_bHeads					!= rhs.m_bHeads				? BoxParams::SHOWHEADS			: 0;
//	lResult |= lhs.m_bDirection				!= rhs.m_bDirection			? BoxParams::SHOWDIRECTION		: 0;
//	lResult |= lhs.m_task					!= rhs.m_task				? BoxParams::TASK				: 0;
//	lResult |= lhs.m_crosshairs.m_bShow		!= rhs.m_crosshairs.m_bShow	? BoxParams::SHOWCROSSHAIRS	: 0;

	return lResult;
}

CBoxParams::CBoxParams (CDxView & view, CTask & task)
:	m_nPanel (1),
	m_lHeadID (-1),
	m_lValveID (-1),
	m_ptOffset (0, 0),
	m_uBlend (255),
	m_bMessages (true),
	m_bHeads (false),
	m_bDirection (true),
	m_task (task),
	m_lChange (BoxParams::SELECTEDPANEL),
	m_bOutlineOnly (false),
	CDxParams (view)
{
	m_dRotation [0]			= 20;
	m_dRotation [1]			= 10;
	m_crosshairs.m_pt		= CPoint (0, 0);
	m_crosshairs.m_bShow	= true;
	m_crosshairs.m_size		= CSize (7, 7);
}

CBoxParams::CBoxParams (const CBoxParams & rhs)
:	m_nPanel (rhs.m_nPanel),
	m_lHeadID (rhs.m_lHeadID),
	m_lValveID (rhs.m_lValveID),
	m_ptOffset (rhs.m_ptOffset),
	m_uBlend (rhs.m_uBlend),
	m_bMessages (rhs.m_bMessages),
	m_bHeads (rhs.m_bHeads),
	m_bDirection (rhs.m_bDirection),
	m_task (rhs.m_task),
	m_lChange (0),						// note: different in default constructor
	m_bOutlineOnly (rhs.m_bOutlineOnly),
	CDxParams (rhs)
{
	CBoxParams * pRhs = (CBoxParams *)(LPVOID)&rhs;

	m_dRotation [0]			= rhs.m_dRotation [0];
	m_dRotation [1]			= rhs.m_dRotation [1];
	m_crosshairs.m_pt		= rhs.m_crosshairs.m_pt;
	m_crosshairs.m_bShow	= rhs.m_crosshairs.m_bShow;
	m_crosshairs.m_size		= rhs.m_crosshairs.m_size;
}

CBoxParams & CBoxParams::operator = (const CBoxParams & rhs)
{
	CDxParams::operator = (rhs);

	if (this != &rhs) {
		CBoxParams * pRhs = (CBoxParams *)(LPVOID)&rhs;
	
		m_nPanel					= rhs.m_nPanel;
		m_lHeadID					= rhs.m_lHeadID;
		m_lValveID					= rhs.m_lValveID;
		m_ptOffset					= rhs.m_ptOffset;
		m_uBlend					= rhs.m_uBlend;
		m_bMessages					= rhs.m_bMessages;
		m_bHeads					= rhs.m_bHeads;
		m_bDirection				= rhs.m_bDirection;
		m_dRotation [0]				= rhs.m_dRotation [0];
		m_dRotation [1]				= rhs.m_dRotation [1];
		m_lChange					= rhs.m_lChange;
		m_bOutlineOnly				= rhs.m_bOutlineOnly;
		//m_task					= rhs.m_task;
//		TRACEF ("Warning: 'operator =' can't assign CBoxParams::m_task");
		m_crosshairs.m_pt			= rhs.m_crosshairs.m_pt;
		m_crosshairs.m_bShow		= rhs.m_crosshairs.m_bShow;
		m_crosshairs.m_size			= rhs.m_crosshairs.m_size;
	}

	return * this;
}

CBoxParams::~CBoxParams()
{
}

void CBoxParams::LoadSettings (CWinApp & app, const CString & strSection)
{
	int x = 20, y = 10;

	CDxParams::LoadSettings (app, strSection);

	m_nPanel				= app.GetProfileInt (strSection, _T ("m_nPanel"),		1);
	m_lHeadID				= app.GetProfileInt (strSection, _T ("m_lHeadID"),		-1);
	m_lValveID				= app.GetProfileInt (strSection, _T ("m_lValveID"),		-1);
	m_ptOffset.x			= app.GetProfileInt (strSection, _T ("m_ptOffset.x"),	0);
	m_ptOffset.y			= app.GetProfileInt (strSection, _T ("m_ptOffset.y"),	0);
	m_uBlend				= app.GetProfileInt (strSection, _T ("m_uBlend"),		255);
	m_bHeads				= app.GetProfileInt (strSection, _T ("m_bHeads"),		false) ? true : false;
	m_bDirection			= app.GetProfileInt (strSection, _T ("m_bDirection"),	false) ? true : false;
	m_crosshairs.m_bShow	= app.GetProfileInt (strSection, _T ("m_bCrosshairs"),	true)  ? true : false;
	CString str				= app.GetProfileString (strSection, _T ("m_dRotation"),	_T ("20, 10"));

	_stscanf (str, _T ("%d, %d"), &x, &y);
	m_dRotation [0]			= max (-90, min (90, x));
	m_dRotation [1]			= max (-90, min (90, y));
}

void CBoxParams::SaveSettings (CWinApp & app, const CString & strSection) const
{
	CString str;

	CDxParams::SaveSettings (app, strSection);

	app.WriteProfileInt (strSection, _T ("m_nPanel"),		m_nPanel);
	app.WriteProfileInt (strSection, _T ("m_lHeadID"),		m_lHeadID);
	app.WriteProfileInt (strSection, _T ("m_lValveID"),		m_lValveID);
	app.WriteProfileInt (strSection, _T ("m_ptOffset.x"),	m_ptOffset.x);
	app.WriteProfileInt (strSection, _T ("m_ptOffset.y"),	m_ptOffset.y);
	app.WriteProfileInt (strSection, _T ("m_uBlend"),		m_uBlend);
	app.WriteProfileInt (strSection, _T ("m_bHeads"),		m_bHeads);
	app.WriteProfileInt (strSection, _T ("m_bDirection"),	m_bDirection);
	app.WriteProfileInt (strSection, _T ("m_bCrosshairs"),	m_crosshairs.m_bShow);
	
	str.Format (_T ("%d, %d"), (int)m_dRotation [0], (int)m_dRotation [1]);
	app.WriteProfileString (strSection, _T ("m_dRotation"),	str);
}

