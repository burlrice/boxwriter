// ImportResultsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "ImportResultsDlg.h"
#include "Extern.h"
#include "List.h"
#include "resource.h"
#include "Parse.h"
#include "Debug.h"

using namespace ListGlobals;
using namespace ItiLibrary;
using namespace FoxjetCommon;
using namespace Foxjet3d;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImportResultsDlg dialog

using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////
CImportResultsDlg::CSummaryItem::CSummaryItem (CString & strFile, int nCount)
:	m_strFile (strFile),
	m_nCount (nCount)
{
}

int CImportResultsDlg::CSummaryItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	const CSummaryItem & cmp = (const CSummaryItem &)rhs;

	switch (nColumn) {
	case 0:		return m_strFile.CompareNoCase (cmp.m_strFile);
	case 1:		return m_nCount - cmp.m_nCount;
	}

	return 0;
}

CString CImportResultsDlg::CSummaryItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:		return m_strFile;
	case 1:		return ToString (m_nCount);
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
CImportResultsDlg::CImportItem::CImportItem (const IMPORTSTRUCT & rhs)
{
	m_nImported		= rhs.m_nImported;
	m_nParsed		= rhs.m_nParsed;
	m_strStruct		= rhs.m_strStruct;
}

CImportResultsDlg::CImportItem::~CImportItem ()
{
}

int CImportResultsDlg::CImportItem::GetDiff () const
{
	return m_nParsed - m_nImported;
}

int CImportResultsDlg::CImportItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	const CImportItem & cmp = (const CImportItem &)rhs;

	switch (nColumn) {
	case 0:		return m_strStruct.CompareNoCase (cmp.m_strStruct);
	case 1:		return m_nParsed - cmp.m_nParsed;
	case 2:		return m_nImported - cmp.m_nImported;
	case 3:		return GetDiff () - cmp.GetDiff ();
	}

	return 0;
}

CString CImportResultsDlg::CImportItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0:		str = m_strStruct;						break;
	case 1:		str.Format (_T ("%d"), m_nParsed);		break;
	case 2:		str.Format (_T ("%d"), m_nImported);	break;
	case 3:		str.Format (_T ("%d"), GetDiff ());		break;
	}

	return str;
}

//////////////////////////////////////////////////

CImportResultsDlg::CImportResultsDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg(IDD_IMPORTRESULTS_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CImportResultsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CImportResultsDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CImportResultsDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	//if (IsMatrix ()) 
	{
		m_buttons.DoDataExchange (pDX, IDOK,			IDB_OK);
		m_buttons.DoDataExchange (pDX, BTN_CLIPBOARD,	IDB_CLIPBOARD);
		m_buttons.DoDataExchange (pDX, IDHELP,			IDB_INFO);
	}
}


BEGIN_MESSAGE_MAP(CImportResultsDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CImportResultsDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(BTN_CLIPBOARD, OnClipboard)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImportResultsDlg message handlers

BOOL CImportResultsDlg::OnInitDialog() 
{
	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;

	FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_STRUCT), 120));
//	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_PARSEDQTY), 60));
//	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_IMPORTEDQTY), 60));
//	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_SKIPPED), 60));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_TOTAL), 60));
	m_lv.Create (LV_RESULT, vCols, this, defElements.m_strRegSection);

	vCols.RemoveAll ();
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_FILE), 300));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_TOTAL), 60));
	m_lvSummary.Create (LV_SUMMARY, vCols, this, defElements.m_strRegSection);

	for (int i = 0; i < m_vImport.GetSize (); i++) {
		IMPORTSTRUCT import = m_vImport [i];

//		if (import.m_nParsed || import.m_nImported)
			m_lv.InsertCtrlData (new CImportItem (import));
	}

	for (POSITION pos = m_vBitmapDirs.GetStartPosition (); pos; ) {
		CString strFile, strCount;

		m_vBitmapDirs.GetNextAssoc (pos, strFile, strCount);
		m_lvSummary.InsertCtrlData (new CSummaryItem (strFile, _ttoi (strCount)));
	}

	//m_lvSummary.InsertCtrlData (new CSummaryItem (CString (_T ("")), 0));

	for (POSITION pos = m_vBitmapFiles.GetStartPosition (); pos; ) {
		CString strFile, strCount;

		m_vBitmapFiles.GetNextAssoc (pos, strFile, strCount);
		m_lvSummary.InsertCtrlData (new CSummaryItem (strFile, _ttoi (strCount)));
	}

	if (m_bRestart) { 
		CString str;

		str.Format (LoadString (IDS_MUSTRESTART), GetAppTitle ());
		SetDlgItemText (LBL_MSG, str);
	}

	if (CWnd * p = GetDlgItem (IDC_ICONINFORMATION))
		p->ShowWindow (m_bRestart ? SW_SHOW : SW_HIDE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CImportResultsDlg::OnClipboard ()
{
	CString strStatsFile (m_strImportFile);
	int nIndex = strStatsFile.ReverseFind ('.');

	if (nIndex != -1)
		strStatsFile.Insert (nIndex, _T ("_stats"));
	else
		strStatsFile += _T ("_stats.txt");

	if (FILE * fp = _tfopen (strStatsFile, _T ("w"))) {
		CString str;

		for (int i = 0; i < m_lv->GetItemCount (); i++) {
			if (CImportItem * p = (CImportItem *)m_lv.GetCtrlData (i)) {
				str = p->GetDispText (0) + _T (", ") + p->GetDispText (1) + _T ("\n");
				fwrite ((LPVOID)w2a (str), str.GetLength (), 1, fp);
			}
		}

		str = _T ("\n\nBitmap summary\n\n");
		fwrite ((LPVOID)w2a (str), str.GetLength (), 1, fp);

		for (int i = 0; i < m_lvSummary->GetItemCount (); i++) {
			if (CSummaryItem * p = (CSummaryItem *)m_lvSummary.GetCtrlData (i)) {
				str = p->GetDispText (0) + _T (", ") + p->GetDispText (1) + _T ("\n");
				fwrite ((LPVOID)w2a (str), str.GetLength (), 1, fp);
			}
		}

		fclose (fp);

		{
			CString strCmdLine = _T ("notepad.exe  \"") + strStatsFile + _T ("\"");
			STARTUPINFO si;     
			PROCESS_INFORMATION pi; 
			TCHAR szCmdLine [0xFF];

			_tcsncpy (szCmdLine, strCmdLine, 0xFF);
			memset (&pi, 0, sizeof(pi));
			memset (&si, 0, sizeof(si));
			si.cb = sizeof(si);     
			si.dwFlags = STARTF_USESHOWWINDOW;     
			si.wShowWindow = SW_SHOW;     

			TRACEF (szCmdLine);

			if (!::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
				MessageBox (LoadString (IDS_WRITTENTO) + strStatsFile);
		}
	}
}