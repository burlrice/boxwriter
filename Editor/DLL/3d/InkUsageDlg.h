#if !defined(AFX_INKUSAGEDLG_H__4E51B53C_5717_4CF8_9368_3A97E9590750__INCLUDED_)
#define AFX_INKUSAGEDLG_H__4E51B53C_5717_4CF8_9368_3A97E9590750__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InkUsageDlg.h : header file
//

#include "ListCtrlImp.h"
#include "Task.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CInkUsageDlg dialog

namespace Foxjet3d
{
	namespace InkUsageDlg
	{
		class _3D_API CElementItem : public ItiLibrary::ListCtrlImp::CItem
		{
		public:
			CElementItem (Element::CElement & e, CEditorElementList & list);
			virtual ~CElementItem ();

			virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
			virtual CString GetDispText (int nColumn) const;
			virtual bool IsNumeric (int nColumn) const;

			double CalcCost ();

			Element::CElement &		m_element;
			CEditorElementList &	m_list;
			double					m_dInkCost;
			ULONG					m_lPixels;
			double					m_dInkUsed;
		};

		class _3D_API CInkUsageDlg : public FoxjetCommon::CEliteDlg
		{
		// Construction
		public:
			CInkUsageDlg(CTask & task, CWnd* pParent = NULL);   // standard constructor

		// Dialog Data
			//{{AFX_DATA(CInkUsageDlg)
			COleCurrency m_curCost;
			double m_dSize;
			//}}AFX_DATA

		public:

		// Overrides
			// ClassWizard generated virtual function overrides
			//{{AFX_VIRTUAL(CInkUsageDlg)
			protected:
			virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
			//}}AFX_VIRTUAL

			static const FoxjetCommon::LIMITSTRUCT m_lmtSizeVX;
			static const FoxjetCommon::LIMITSTRUCT m_lmtSizePro;

		// Implementation
		protected:
			CTask & m_task;
			ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
			static const CString m_strRegSection;

			// Generated message map functions
			//{{AFX_MSG(CInkUsageDlg)
			afx_msg void OnCalc();
			virtual BOOL OnInitDialog();
			//}}AFX_MSG
			afx_msg void OnItemchangedElements(NMHDR* pNMHDR, LRESULT* pResult);
			afx_msg void OnExport ();

			DECLARE_MESSAGE_MAP()
		};
	};// namespace InkUsageDlg
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INKUSAGEDLG_H__4E51B53C_5717_4CF8_9368_3A97E9590750__INCLUDED_)
