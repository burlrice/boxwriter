#include "stdafx.h"
#include <afxsock.h>
#include <memory>
#include "3d.h"
#include "Registry.h"
#include "Resource.h"
#include "Debug.h"
#include "AppVer.h"
#include "Export.h"
#include "OdbcRecordset.h"
#include "Serialize.h"
#include "FieldDefs.h"
#include "Extern.h"
#include "ImportFileDlg.h"
#include "Parse.h"
#include "BitmapElement.h"
#include "FileExt.h"
#include "Task.h"
#include "zlib.h"
#include "pugixml.hpp"
#include "DC.h"
#include "OpenDlg.h"

#include "yaml-cpp\node\iterator.h"
#include "yaml-cpp\node\node.h"
#include "yaml-cpp\node\parse.h"
#include "yaml-cpp\node\impl.h"
#include "yaml-cpp\emitter.h"

#ifdef __WINPRINTER__
	#include "BarcodeElement.h"
	#include "BarcodeParams.h"
	#include "DatabaseElement.h"
	#include "BitmappedFont.h"
	#include "CountElement.h"
	#include "BitmapElement.h"
#endif //__WINPRINTER__

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace Foxjet3d;

#define TRACE(n) Trace ((n), _T (__FILE__), __LINE__, 0, 10)

#define LINX_OK(s) CheckLinxYmlStatus ((s), _T ("OK"), _T (__FILE__), __LINE__)
#define DECLARE_LINX_STATUS() bool $_bLinxStatus;

#ifndef _DEBUG
	int _CrtDbgReportW(
			_In_ int _ReportType,
			_In_opt_z_ const wchar_t * _Filename,
			_In_ int _LineNumber,
			_In_opt_z_ const wchar_t * _ModuleName,
			_In_opt_z_ const wchar_t * _Format,
			...)
	{
		return 1;
	}
#endif

#define VERIFY_LINX_OK(expr) \
	(bool)((!!($_bLinxStatus = CheckLinxYmlStatus ((expr), _T ("OK"), _T (__FILE__), __LINE__))) || \
		(1 != _CrtDbgReportW(_CRT_ASSERT, _T (__FILE__), __LINE__, NULL, _T ("Reply OK: not found in: ") _T (#expr))) || \
			(_CrtDbgBreak(), 0), $_bLinxStatus)

static bool IsSocketDataPending (SOCKET s);
static CString Send (CSocket & s, const CString & strSend, bool bMakeYML = true);
static bool CheckLinxYmlStatus (const CString & str, const CString & strExpected, bool bBreakOnError, LPCTSTR lpszFile, ULONG lLine);

void Trace (const YAML::Node & node, LPCTSTR lpszFile, ULONG lLine, int nStart = 0, int nDepth = 4)
{
	if (nStart > nDepth)
		return;

	for(YAML::const_iterator it = node.begin(); it != node.end (); ++it) {
		CString str = CString (' ', nStart * 4);
		YAML::Node key = it->first;
		YAML::Node value = it->second;
		
		if (it->IsDefined ())
			str += it->Scalar ().c_str ();

		if (key.IsDefined () || value.IsDefined ()) {
			str += key.Scalar ().c_str () + CString (_T (": ")) + value.Scalar ().c_str () + CString (_T (" "));

			#ifdef _DEBUG
			CDebug::Trace (str, true, lpszFile, lLine);
			#endif

			Trace (key, lpszFile, lLine, nStart + 1, nDepth);
			Trace (value, lpszFile, lLine, nStart + 1, nDepth);
		}

		if (it->IsDefined ()) {
			switch (it->Type ()) {
			case YAML::NodeType::Null:		str += ("[Null     ]: ");	break;
			case YAML::NodeType::Scalar:	str += ("[Scalar   ]: ");	break;
			case YAML::NodeType::Sequence:	str += ("[Sequence ]: ");	break;
			case YAML::NodeType::Undefined:	str += ("[Undefined]: ");	break;
			case YAML::NodeType::Map:		str += " - "; //str += ("[Map      ]: ");	
				for(YAML::const_iterator i = it->begin(); i != it->end (); ++i) {
					YAML::Node key = i->first;
					YAML::Node value = i->second;

					//if (key.Type () == YAML::NodeType::Scalar) TRACEF (key.as <std::string> ().c_str ());
					//if (value.Type () == YAML::NodeType::Scalar) TRACEF (value.as <std::string> ().c_str ());

					str += key.Scalar ().c_str () + CString (_T (": ")) + value.Scalar ().c_str () + CString (_T (" "));
				}
				break;
			}

			#ifdef _DEBUG
			CDebug::Trace (str, true, lpszFile, lLine);
			#endif

			switch (it->Type ()) {
			case YAML::NodeType::Map:
				for(YAML::const_iterator i = it->begin(); i != it->end (); ++i) {
					YAML::Node key = i->first;
					YAML::Node value = i->second;

					Trace (key, lpszFile, lLine, nStart + 1, nDepth);
					Trace (value, lpszFile, lLine, nStart + 1, nDepth);
				}
				break;
			}
		}
	}
}

CString CImportFileDlg::ImportYml (const CString & strFile, const FoxjetDatabase::HEADSTRUCT & head, const CMapStringToString & mapChar, int & nChannels)
{
	CString str = ::GetFileAttributes (strFile) != -1 ? FoxjetFile::ReadDirect (strFile) : strFile;
	CString strResult;
	
	nChannels = 32;

	if (str.Find (_T ("- Message:")) == -1) 
		str.Replace (_T ("Message:"), _T ("- Message:"));

	try 
	{
		YAML::Node doc = YAML::Load (std::string (w2a (str)));
		double dRes [] = { (double)head.GetChannels () / head.GetSpan (), head.GetRes () };
		std::vector <YAML::Node> v = doc ["Message"]["Line"].vector ("Field");

		TRACEF (_T ("channels: ") + ToString (head.GetChannels ()));
		TRACEF (_T ("span:     ") + ToString (head.GetSpan ()) + _T ("\""));
		TRACEF (_T ("v res:    ") + ToString ((double)head.GetChannels () / head.GetSpan ()));
		TRACEF (_T ("h res:    ") + ToString (head.GetRes ()));

		{
			int n = 0;
			CString str = a2w (doc["Message"].value ("Style").c_str ());
			str = Extract (str, _T ("x"), _T (" "));

			if (_stscanf (str, _T ("%d"), &n) == 1)
				nChannels = n;
		}

		for (int i = 0; i < v.size (); i++) {
			YAML::Node x = v [i];
			const CString strType = a2w (x.value ("Type").c_str ());
			CPoint pt (std::atoi (x ["Pos"].value ("X").c_str ()), std::atoi (x ["Pos"].value ("Y").c_str ()));
			int nFontHeight = std::atoi (x ["Font"].value ("Size").c_str ());
			int nFontWidth = (int)((double)nFontHeight * 0.85);

			TRACEF (ToString (pt.x) + _T (", ") + ToString (pt.y));

			pt.x = Round (pt.x / dRes [0] * 1000.0);
			pt.y = Round (pt.y / dRes [1] * 1000.0);

			TRACEF (ToString (pt.x) + _T (", ") + ToString (pt.y));

			if (!strType.CompareNoCase (_T ("Text"))) {
				FoxjetElements::CTextElement e (head);
				CString strValue, strKey = x.value ("Name").c_str ();

				e.SetPos (pt, &head);
				//e.SetFont (FoxjetCommon::CPrinterFont ("Vx 18b"));
				e.SetFont (FoxjetCommon::CPrinterFont ("MK Courier", nFontHeight, false));
				e.SetWidth (nFontWidth);

				e.SetDefaultData (x ["Parameters"].value ("Text").c_str ());

				TRACER (e.GetDefaultData ());

				if (mapChar.Lookup (strKey, strValue))
					e.SetDefaultData (strValue);

				strResult += e.ToString (::verApp) + _T ("\n");
			}
			else if (!strType.CompareNoCase (_T ("Date")) || !strType.CompareNoCase (_T ("Time"))) {
				FoxjetElements::CDateTimeElement e (head);
				int nFontSize = std::atoi (x ["Font"].value ("Size").c_str ());
				CString str = x ["Parameters"].value ("Format").c_str ();

				e.SetPos (pt, &head);
				//e.SetFont (FoxjetCommon::CPrinterFont ("Vx 7sf"));
				e.SetFont (FoxjetCommon::CPrinterFont ("MK Courier", nFontHeight, false));
				e.SetWidth (nFontWidth);

				e.SetDefaultData (str);

				strResult += e.ToString (::verApp) + _T ("\n");
			}
			else if (!strType.CompareNoCase (_T ("Sequential"))) {
				FoxjetElements::CCountElement e (head);
				int nFontSize = std::atoi (x ["Font"].value ("Size").c_str ());
				
				CString strText		= x ["Parameters"].value ("Text").c_str ();
				CString strStart	= x ["Parameters"].value ("Start").c_str ();
				CString strStop		= x ["Parameters"].value ("Stop").c_str ();
				CString strStep		= x ["Parameters"].value ("Step").c_str ();
				CString strRepeat	= x ["Parameters"].value ("Repeat").c_str ();
				CString strCurrentRepeat = x ["Parameters"].value ("CurrentRepeat").c_str ();

				e.SetPos (pt, &head);
				//e.SetFont (FoxjetCommon::CPrinterFont ("Vx 7sf"));
				e.SetFont (FoxjetCommon::CPrinterFont ("MK Courier", nFontHeight, false));
				e.SetWidth (nFontWidth);

				e.SetDigits (strText.GetLength ());
				e.SetLeadingZeros (true);
				e.SetCount (_ttoi (strText));
				e.SetMin (_ttoi (strStart));
				e.SetMax (_ttoi (strStop));
				e.SetIncrement (_ttoi (strRepeat));

				strResult += e.ToString (::verApp) + _T ("\n");
			}
			else if (!strType.CompareNoCase (_T ("Logo"))) {
				const CString strImage = x ["Parameters"].value ("Data").c_str ();
				int nBuffer = strImage.GetLength ();
				block <UCHAR> pDecoded (nBuffer);
				block <BYTE> pCompressed (nBuffer);
				int nFileSize = DecodeBase64 ((UCHAR *)(LPCSTR)w2a (strImage), pDecoded, strImage.GetLength ());
				DWORD dwCompressed;
				BITMAPFILEHEADER file;
				BITMAPINFOHEADER info;

				//TRACEF (DB::ToString (strImage.GetLength ()) + _T (" bytes: ") + strImage);
				//TRACEF ((std::wstring)block <char> (w2a (strImage), strImage.GetLength ()));
				//TRACEF (_T ("DecodeBase64: ") + (std::wstring)block <BYTE> (pDecoded.get (), nFileSize));

				memcpy (&file, pDecoded, sizeof (file));
				memcpy (&info, pDecoded + sizeof (file), sizeof (info));

				dwCompressed = nFileSize;
				int nCompress = compress (pCompressed, &dwCompressed, pDecoded, nFileSize);
				//TRACEF (_T ("compress: ") + (std::wstring)block <BYTE> (pCompressed.get (), dwCompressed));

				if (nCompress == Z_OK) {
					block <UCHAR> pEncoded (nBuffer);
					FoxjetElements::CBitmapElement e (head);

					int nEncoded = EncodeBase64 (pCompressed, dwCompressed, pEncoded, pEncoded.m_lBytes);
					CString strEncoded = a2w (pEncoded.get ());
					strEncoded.Trim ();
					//TRACEF (_T ("EncodeBase64: ") + (std::wstring)block <char> (w2a (strEncoded), strEncoded.GetLength ()));
					CString strCompressed = _T (":bmp64:") + ToString (strImage.GetLength ()) + _T (",") + strEncoded + _T (":");

					strCompressed.Replace (_T ("\n"), _T (""));
					strCompressed.Replace (_T ("\r"), _T (""));

					e.SetPos (pt, &head);
					e.SetFilePath (strCompressed, head);

					TRACEF (e.ToString (::verApp));

					strResult += e.ToString (::verApp) + _T ("\n");
				}
			}
			else {
				TRACEF (strType);
				int n = 0;
			}

		}
	}
	catch (YAML::Exception & e) {
		TRACEF (e.msg.c_str ());
		int nBreak = 0;
	}

	return strResult;
}

int CImportFileDlg::ImportYml (const CImportFileDlg::IMPORTTHREADSTRUCT & params)
{
	CString str = FoxjetFile::ReadDirect (params.m_strFile);

	if (str.Find (_T ("- Message:")) == -1) 
		str.Replace (_T ("Message:"), _T ("- Message:"));

	try 
	{
		YAML::Node doc = YAML::Load (std::string (w2a (str)));
		TASKSTRUCT task;
		MESSAGESTRUCT msg;
		CTmpDC hdc (NULL);
		CDC dcMem, & dc = * CDC::FromHandle (hdc);
		COdbcDatabase & db = ListGlobals::GetDB ();

		BOXSTRUCT box;
		LINESTRUCT line;
		HEADSTRUCT head;
		CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

		VERIFY (dcMem.CreateCompatibleDC (&dc));
		GetFirstLineRecord (db, line, GetPrinterID (db));
		GetLineHeads (db, line.m_lID, vHeads);

		task.m_strName = doc ["Message"].value ("Name").c_str ();

		if (!vHeads.GetSize ())
			return 0;
		else
			head = vHeads [0];

		box.m_strName = task.m_strName;
		box.m_lID = GetBoxID (db, box.m_strName);

		if (box.m_lID == NOTFOUND) {
			box.m_lLength = box.m_lWidth = box.m_lHeight = 6000;
			VERIFY (AddBoxRecord (db, box));
		}

		task.m_lLineID = line.m_lID;
		task.m_lBoxID = box.m_lID;
		task.m_strDesc = CTime::GetCurrentTime ().Format (_T ("Imported %c"));

		int nChannels = head.m_nChannels;
		msg.m_lHeadID = head.m_lID;
		msg.m_lID = -1;
		msg.m_strName = task.m_strName;
		msg.m_strData += Foxjet3d::CImportFileDlg::ImportYml (params.m_strFile, head, CMapStringToString (), nChannels);
		task.m_vMsgs.Add (msg);

		{

			CArray <VALVESTRUCT, VALVESTRUCT &> vValves;
			CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;
			CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> vBounds;
			BOXSTRUCT boxExisting;

			GetPanelRecords (db, line.m_lID, vPanels);

			#if 0
			{
				CString str [] = 
				{
					_T ("{VALVESTRUCT,1,38,2000,1,0,1,4000,1,FJ0001,1,4294967295}"),
					//_T ("{VALVESTRUCT,2,38,2000,1,0,1,3125,1,FJ0002,2,1}"),
					//_T ("{VALVESTRUCT,3,38,2000,1,0,1,3125,1,FJ0003,2,2}"),
					//_T ("{VALVESTRUCT,4,38,2000,1,0,1,3125,1,FJ0004,2,3}"),
				};
				CStringArray v;
		
				head.m_nEncoderDivisor = 3;

				for (int i = 0; i < ARRAYSIZE (str); i++) {
					VALVESTRUCT valve;

					//TRACEF (str [i]);

					VERIFY (FromString (str [i], valve));
					valve.m_type = IV_LYNX_32;
					valve.m_lCardID = head.m_lID;
					head.m_vValve.Add (valve);
				}

				for (int i = 0; i < vHeads.GetSize (); i++) 
					vValves.Append (vHeads [i].m_vValve);
			}
			#endif

			for (int i = 0; i < vHeads.GetSize (); i++) 
				vHeads [i].m_vValve.RemoveAll ();

			HeadConfigDlg::CPanel::GetPanels (task.m_lLineID, vBounds, vHeads, vValves, vPanels);

			bool bExistingBox = GetBoxRecord (db, box.m_lID, boxExisting);
			
			Foxjet3d::OpenDlg::CTaskPreview preview (task, dcMem, CRect (0, 0, 1, 1), CMapStringToString (), box, line, vHeads, vPanels, vBounds);

			if (bExistingBox) {
				box.m_lLength	= boxExisting.m_lLength;
				box.m_lWidth	= boxExisting.m_lWidth;
				box.m_lHeight	= boxExisting.m_lHeight;
			}
		}

		VERIFY (UpdateBoxRecord (db, box));

		ULONG lExistingID = GetTaskID (db, task.m_strName, line.m_strName, GetPrinterID  (db));

		if (lExistingID != NOTFOUND)
			VERIFY (DeleteTaskRecord (db, lExistingID));

		if (AddTaskRecord (db, task))
			return 1;
	}
	catch (YAML::Exception & e) {
		TRACEF (e.msg.c_str ());
		int nBreak = 0;
	}

	return 0;
}

typedef enum { PENDING_READ, PENDING_WRITE, } PENDING;

static bool Pending (SOCKET s, PENDING pending)
{
	struct timeval timeout;
	fd_set read, write, except;

	FD_ZERO (&read);
	FD_ZERO (&write);
	FD_ZERO (&except);

	read.fd_count		= write.fd_count		= except.fd_count		= 1;
	read.fd_array [0]	= write.fd_array [0]	= except.fd_array [0]	= s;

	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

	int nSelect = ::select (0, 
		pending == PENDING_READ ? &read : NULL,
		pending == PENDING_WRITE ? &write : NULL,
		&except, 
		&timeout);

	bool bRead = FD_ISSET (s, &read);
	bool bWrite = FD_ISSET (s, &write);
	bool bExcept = FD_ISSET (s, &except);

	ASSERT (!bExcept);

	if (nSelect != SOCKET_ERROR && nSelect > 0) 
		return pending == PENDING_READ ? bRead : bWrite;

	return false;
}

static CString Send (CSocket & s, const CString & strSend, bool bMakeYML)
{
	char sz [256];
	std::string str = !bMakeYML ? (std::string)w2a (strSend) : YAML::Load (w2a (strSend)).toLinxString ();

	s.Send (str.c_str (), str.length ());
	TRACEF (_T ("write: [") + ToString ((int)str.length ()) + _T ("] bytes: ") + Format ((LPBYTE)str.c_str (), str.length ()));
	str = "";
	::Sleep (100);

	while (Pending (s.m_hSocket, PENDING_READ)) {
		memset (sz, 0, ARRAYSIZE (sz));
		int n = s.Receive (sz, ARRAYSIZE (sz) - 1);

		if (n <= 0)
			break;

		TRACEF (_T ("read:  [") + ToString (n) + _T ("] bytes: ") + Format ((LPBYTE)sz, n));
		str += sz;
	}

	CString strResult (a2w (str.c_str ()));
	
	if (strResult.Find (_T ("---\n- ")) == -1)
		strResult.Replace (_T ("---\n"), _T ("---\n- "));

	return strResult;
}

static bool CheckLinxYmlStatus (const CString & str, const CString & strExpected, LPCTSTR lpszFile, ULONG lLine)
{
	bool bResult = false;

	try 
	{
		const YAML::Node doc = YAML::Load (std::string (w2a (str)));
		const CString strResult = doc.begin()->begin()->second.Scalar().c_str ();

		bResult = strResult == strExpected;
	}
	catch (YAML::Exception e) 
	{
		#ifdef _DEBUG
		CDebug::Trace (e.what (), true, lpszFile, lLine);
		CDebug::Trace (e.msg.c_str (), true, lpszFile, lLine);
		#endif
	}

	if (!bResult) {
		#ifdef _DEBUG
		CDebug::Trace (str, true, lpszFile, lLine);
		CDebug::Trace (_T ("NOT FOUND: ") + strExpected, true, lpszFile, lLine);
		#endif 
	}

	return bResult;
}

static unsigned __int8 RciCalcChecksum (const CString & str)
{
	unsigned __int8 * psz = new unsigned __int8 [str.GetLength ()];
	unsigned __int64 nSum = 0;

	memset (psz, 0, str.GetLength ());
	int n = FoxjetDatabase::Unformat (str, psz);

	//TRACEF (DB::Format (psz, n));
	//ASSERT (str == DB::Format (psz, n));

	for (int i = 1; i < n; i++) {
		if (psz [i] != 27) {
			nSum += psz [i];
			//{ CString str; str.Format (_T ("0x%02X [0x%02X]"), psz [i], nSum); TRACEF (str); } 
		}
	}

	unsigned __int8 nCheckSum = nSum &= 0xFF;
	nCheckSum = 0x100 - nCheckSum;
	delete [] psz;

	return nCheckSum;
}

static CString RciSend (CSocket & sock, const CString & str)
{
	std::unique_ptr <BYTE>  psz (new BYTE [str.GetLength () + 1]);
	unsigned __int8 nCheckSum = RciCalcChecksum (str);
	CString strResult;

	memset (psz.get (), 0, str.GetLength () + 1);
	int n = FoxjetDatabase::Unformat (str, psz.get ());
	psz.get () [n++] = nCheckSum;

	//ASSERT (str == DB::Format (psz.get (), n));

	int nSent = sock.Send (psz.get (), n);
	TRACEF (_T ("[") + DB::ToString (nSent) + _T (" bytes]: ") + DB::Format (psz.get (), n));
	::Sleep (100);

	// <ESC><STX> 0x26 <ESC><ETX>
	//					 <ESC><STX><SIB><ESC><ETX>
	// read:  [8] bytes: <ESC><NAK><NULL><ACK><NULL><ESC><ETX>�

	while (Pending (sock.m_hSocket, PENDING_READ)) {
		BYTE sz [256] = { 0 };

		memset (sz, 0, ARRAYSIZE (sz));
		int n = sock.Receive (sz, ARRAYSIZE (sz) - 1);

		if (n <= 0)
			break;

		TRACEF (_T ("read:  [") + ToString (n) + _T ("] bytes: ") + Format ((LPBYTE)sz, n));
		//strResult += sz;
		::Sleep (1);
	}

	return strResult;
}

void CImportFileDlg::RciGetMessages (const CString & strSocket, int nPort, const CString & strUser, const CString & strPass)
{
	CSocket sock;

	sock.Create ();

	if (!sock.Connect (strSocket, nPort)) 
		TRACEF (FormatMessage (::WSAGetLastError ()));

	int nCmdID = 0x1a;
	//CString strCmd = _T ("<ESC><STX>") + FoxjetDatabase::Format ((LPBYTE)&nCmdID, 1) + _T ("<ESC><ETX>");
	CString strCmd;
	
	strCmd += _T ("<ESC><STX>");
	strCmd += FoxjetDatabase::Format ((LPBYTE)&nCmdID, 1);
	//nCmdID = 0;
	//strCmd += FoxjetDatabase::Format ((LPBYTE)&nCmdID, 1);
	strCmd += _T ("0");
	strCmd += _T ("<ESC><ETX>");
	TRACEF (RciSend (sock, strCmd));
	int n = 0;
}

void CImportFileDlg::GetMessages (const CString & strSocket, int nPort, const CString & strUser, const CString & strPass)
{
	{
		{
			DECLARETRACECOUNT (200);
			DECLARE_LINX_STATUS();
			CSocket socketTmp, * pSocket = NULL;
			int nSent = 0;
			std::vector <std::string> v;

			socketTmp.Create ();

			if (!socketTmp.Connect (strSocket, nPort)) 
				TRACEF (FormatMessage (::WSAGetLastError ()));

			CString str = Send (socketTmp, _T ("{UserLevel: [User: ") + strUser + _T (", PIN: ") + strPass + _T ("]}"));

			if (!VERIFY_LINX_OK (str)) 
				return;

			MARKTRACECOUNT();
			CString strMessage = Send (socketTmp, _T ("Resources: [Message: QY]"));
			YAML::Node messages = YAML::Load (std::string (w2a (strMessage)));

			TRACEF (strMessage);

			YAML::Node n = messages ["Resources"]["Message"];

			for(YAML::const_iterator it = n.begin(); it != n.end (); ++it) {
				if (it->IsDefined ()) 
					v.push_back (it->Scalar ());
			}

			MARKTRACECOUNT();

			for (int i = 0; i < v.size (); i++) {
				YAML::Node msg = YAML::Load (std::string (w2a (Send (socketTmp, _T ("Message: ") + a2w (v [i].c_str ())))));

				MARKTRACECOUNT();
				//TRACE (msg);
			}

			MARKTRACECOUNT();
			TRACECOUNTARRAY ();
		}

		{
			DECLARETRACECOUNT (200);
			DECLARE_LINX_STATUS();
			CSocket socketTmp, * pSocket = NULL;
			int nSent = 0;
			std::vector <std::string> v;

			socketTmp.Create ();

			if (!socketTmp.Connect (strSocket, nPort)) 
				TRACEF (FormatMessage (::WSAGetLastError ()));

			CString str = Send (socketTmp, _T ("{UserLevel: [User: ") + strUser + _T (", PIN: ") + strPass + _T ("]}"));

			if (!VERIFY_LINX_OK (str)) 
				return;

			MARKTRACECOUNT();
			CString strMessage = Send (socketTmp, _T ("Resources: [Message: QY]"));
			YAML::Node messages = YAML::Load (std::string (w2a (strMessage)));

			YAML::Node n = messages ["Resources"]["Message"];

			for(YAML::const_iterator it = n.begin(); it != n.end (); ++it) {
				if (it->IsDefined ()) 
					v.push_back (it->Scalar ());
			}

			MARKTRACECOUNT();

			for (int i = 0; i < v.size (); i++) {
				MARKTRACECOUNT();
				VERIFY_LINX_OK (Send (socketTmp, _T ("PrintMessage: ") + a2w (v [i].c_str ())));

				MARKTRACECOUNT();
				YAML::Node image = YAML::Load (std::string (w2a (Send (socketTmp, _T ("PrintImage: QY")))));
				//TRACE (image);
				MARKTRACECOUNT();
			}

			MARKTRACECOUNT();
			TRACECOUNTARRAY ();
		}
	}

	DECLARE_LINX_STATUS();
	CSocket socketTmp, * pSocket = NULL;
	int nSent = 0;

	socketTmp.Create ();

	if (!socketTmp.Connect (strSocket, nPort)) 
		TRACEF (FormatMessage (::WSAGetLastError ()));

	CString str = Send (socketTmp, _T ("{UserLevel: [User: ") + strUser + _T (", PIN: ") + strPass + _T ("]}"));

	if (!VERIFY_LINX_OK (str)) 
		return;

	CString strMessage = Send (socketTmp, _T ("Message: linx"));
	TRACEF (strMessage);

	YAML::Node message = YAML::Load (std::string (w2a (strMessage)));
	CString strImage = message ["Message"]["Line"]["Field"]["Parameters"].value ("Data").c_str ();

	TRACE (message);
	TRACEF (strImage);
	CxImage img;
	int nBuffer = strImage.GetLength ();
	UCHAR * pDecoded = new UCHAR [nBuffer];
	int n = DecodeBase64 ((UCHAR *)(LPCSTR)w2a (strImage), pDecoded, strImage.GetLength ());

	if (n <= 0 || !strImage.GetLength ())
		TRACEF (str);
	else {
		BITMAPFILEHEADER file;
		BITMAPINFOHEADER info;

		memcpy (&file, pDecoded, sizeof (file));
		memcpy (&info, pDecoded + sizeof (file), sizeof (info));

		int nBytesPerLine = ((info.biBitCount * info.biWidth + 31) / 32) * 4;
		img.CreateFromArray (pDecoded + file.bfOffBits, info.biWidth, info.biHeight, info.biBitCount, nBytesPerLine, false);

		if (HBITMAP h = img.MakeBitmap ()) {
			SAVEBITMAP (h, GetHomeDir () + _T ("\\Debug\\NLRCI.bmp"));
			::DeleteObject (h);
		}
	}

	delete [] pDecoded;
}

void CImportFileDlg::GetImage (const CString & strSocket, int nPort, const CString & strUser, const CString & strPass)
{
	DECLARE_LINX_STATUS();
	CSocket socketTmp, * pSocket = NULL;
	int nSent = 0;

	socketTmp.Create ();

	if (!socketTmp.Connect (strSocket, nPort)) 
		TRACEF (FormatMessage (::WSAGetLastError ()));

	CString str = Send (socketTmp, _T ("{UserLevel: [User: ") + strUser + _T (", PIN: ") + strPass + _T ("]}"));

	if (!VERIFY_LINX_OK (str)) 
		return;

	str = Send (socketTmp, _T ("Resources: [Font: EQY, MessageStyle: EQY]"));
	YAML::Node resources = YAML::Load (std::string (w2a (str)));

	socketTmp.Close ();

	for (YAML::iterator i = resources ["Resources"]["Font"].begin (); i != resources ["Resources"]["Font"].end (); ++i) {
		CString strFont = i->Scalar ().c_str ();

		CString strDir = GetHomeDir () + _T ("\\Fonts\\Linx\\") + strFont;
		std::vector <std::wstring> v = explode (std::wstring (strFont), ' ');

		CreateDir (strDir);
		TRACEF (strFont);

		if (v.size ()) {
			CString strFamily = CString (implode (std::vector <std::wstring> (&v [0], &v [v.size () - 1]), std::wstring (_T (" "))).c_str ());
			const int nSize = _ttoi (v [v.size () - 1].c_str ());
			CMap <int, int, CString, CString> mapChar;
			std::vector <std::wstring> vError;
			CString str;
			int nParsed = 0;

			if (nSize > 25)
				continue; // TODO

			for (char c = '!'; c <= '~'; c++) 
				mapChar.SetAt (c, CString (c));

			{
				const char c [] = { '`', '>', '-', ']', '*', '|', '\'', '!', '@', ',', ':', '{', '&', '#', '?', '}', '[', '%', };

				mapChar.SetAt ((int)'"', _T ("\"\\\"\""));

				for (int i = 0; i < ARRAYSIZE (c); i++) {
					CString str = _T ("\"") + CString (c [i]) + _T ("\"");

					//TRACEF (str);
					mapChar.SetAt ((int)c [i], str);
				}
			}

			for (POSITION pos = mapChar.GetStartPosition (); pos; ) {
				CString strChar;
				CString strFile;
				int i;

				mapChar.GetNextAssoc (pos, i, strChar);
				strFile.Format (_T ("%s\\%03d.bmp"), strDir, i);

				if (::GetFileAttributes (strFile) == -1) {
					if (!((nSent++ + 1) % 20)) {
						if (pSocket) {
							pSocket->Close ();
							delete pSocket;
							pSocket = NULL;
							::Sleep (2000);
						}
					}

					if (!pSocket) {
						pSocket = new CSocket ();
						pSocket->Create ();

						if (!pSocket->Connect (_T ("192.168.1.100"), 29044)) {
							TRACEF (FormatMessage (::WSAGetLastError ()));
							return;
						}

						str = Send (* pSocket, _T ("{UserLevel: [User: ") + strUser + _T (", PIN: ") + strPass + _T ("]}"));
						VERIFY_LINX_OK (str);
					}
			
					str = Send (* pSocket, "Delete: [Message: BOXWRITER]");
					LINX_OK (str);

					CString strMsg = 
						"---\n"
						"Message:\n"
						"  - Name: BOXWRITER\n"
						"  - Style: #STYLE#\n"
						"  - Line: \n"
						"    - Field: \n"
						"      - Font: \n"
						"        - Name: #FONT#\n"
						"        - Size: #SIZE#\n"
						"      - Parameters:\n"
						"        - Text: #TEXT#\n"
						"...";
				
					//strMsg.Replace (_T ("#STYLE#"), _T ("1x") + ToString (nSize) + _T (" Western Flexible"));
					strMsg.Replace (_T ("#STYLE#"), _T ("1x25 Western Flexible"));				
					strMsg.Replace (_T ("#FONT#"), strFamily);
					strMsg.Replace (_T ("#SIZE#"), ToString (nSize));
					strMsg.Replace (_T ("#TEXT#"), strChar);
					str = Send (* pSocket, strMsg, false);

					if (!LINX_OK (str)) {
						TRACEF (str);
						TRACEF (strMsg);
						vError.push_back (std::wstring (strChar));

						if (pSocket) {
							pSocket->Close ();
							delete pSocket;
							pSocket = NULL;
						}

						continue;
					}

					str = Send (* pSocket, _T ("PrintMessage: BOXWRITER"));
					VERIFY_LINX_OK (str);

					str = Send (* pSocket, _T ("PrintImage: QY"));
					YAML::Node image = YAML::Load (std::string (w2a (str)));
					CxImage img;
					CString strImage = image ["PrintImage"].value ("Data").c_str ();
					int nBuffer = strImage.GetLength ();
					UCHAR * pDecoded = new UCHAR [nBuffer];
					int n = DecodeBase64 ((UCHAR *)(LPCSTR)w2a (strImage), pDecoded, strImage.GetLength ());

					if (n <= 0 || !strImage.GetLength ())
						TRACEF (str);
					else {
						BITMAPFILEHEADER file;
						BITMAPINFOHEADER info;

						memcpy (&file, pDecoded, sizeof (file));
						memcpy (&info, pDecoded + sizeof (file), sizeof (info));

						int nBytesPerLine = ((info.biBitCount * info.biWidth + 31) / 32) * 4;
						img.CreateFromArray (pDecoded + file.bfOffBits, info.biWidth, info.biHeight, info.biBitCount, nBytesPerLine, false);

						if (info.biHeight > nSize) 
							img.Crop (0, 0, info.biWidth, nSize);

						if (HBITMAP h = img.MakeBitmap ()) {
							SAVEBITMAP (h, strFile);
							::DeleteObject (h);
							nParsed++;
						}
					}

					delete [] pDecoded;
				}
			}

			if (nParsed) {
				std::vector <int> vWidth;
				CStringArray vFiles;
				
				GetFiles (strDir, _T ("*.bmp"), vFiles);
				
				for (int i = 0; i < vFiles.GetSize (); i++) {
					CxImage img;

					if (img.Load (vFiles [i])) 
						if (Find (vWidth, (int)img.GetWidth ()) == -1)
							vWidth.push_back ((int)img.GetWidth ());
				}

				for (int i = 0; i < vWidth.size (); i++)
					TRACEF (ToString (vWidth [i]));

				if (vWidth.size () >= 1) {
					CString strFile;

					strFile.Format (_T ("%s\\%03d.bmp"), strDir, (int)' ');

					if (::GetFileAttributes (strFile) == -1) {
						CTmpDC dc (NULL);
						CDC dcMem;
						CBitmap bmp;
						int cx = vWidth [0];

						dcMem.CreateCompatibleDC (dc);
						bmp.CreateCompatibleBitmap (&dcMem, cx, nSize);
						CBitmap * pOld = dcMem.SelectObject (&bmp);
						::BitBlt (dcMem, 0, 0, cx, nSize, NULL, 0, 0, WHITENESS);
						SAVEBITMAP (bmp, strFile);
						dcMem.SelectObject (pOld);
					}
				}
			}

			if (vError.size ())
				TRACEF (implode (vError, std::wstring (_T (" "))).c_str ());
		}
	}
}
