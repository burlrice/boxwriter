// HeadDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "Utils.h"
#include "HeadDlg.h"
#include "Resource.h"
#include "Debug.h"
#include "ComPropertiesDlg.h"
#include "fj_defines.h"
#include "TemplExt.h"
#include "AppVer.h"
#include "DevGuids.h"
#include "HeadConfigDlg.h"
#include "ElementDefaults.h"

#ifdef __IPPRINTER__
	#include "SyncDlg.h"
	#include "WinMsg.h"
#endif

#ifdef __WINPRINTER__
	#include "StdCommDlg.h"
#endif

static const LPCTSTR lpszPcRepeater = _T ("PC repeater, %d");

extern FoxjetCommon::CElementDefaults & defElements;

/*
slot 1 
	should be master [only head with true isa interrupt]
	can use encoder a & photocell a only, cannot share
slot 2
	can use pc b, enc b (standalone)
	can use a, but must share from head 1
master
	determines who's counts are used in print report
*/

#define SOFTWARELIMITEDCONFIG (true) // if this is ever set to false, update rdo check logic

#include "Coord.h" // this is the reason CHeadPhcDlg must reside in 3d.dll

static const double dChannelSeparation = 0.05848; // Trident Heads

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace Foxjet3d;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef __WINPRINTER__
	const FoxjetCommon::LIMITSTRUCT CHeadPhcDlg::m_lmtPhotocellRate	= {	6,		144					}; // in inches
#else
	const FoxjetCommon::LIMITSTRUCT CHeadPhcDlg::m_lmtPhotocellRate	= {	1,		144					}; // in inches
#endif

const FoxjetCommon::LIMITSTRUCT CHeadPhcDlg::m_lmtHeadName				= {	1,		FJPH_TYPE_NAME_SIZE	};
const FoxjetCommon::LIMITSTRUCT CHeadPhcDlg::m_lmtMphcPhotocellDelay	= {	500,	30000				}; // in 1000ths
const FoxjetCommon::LIMITSTRUCT CHeadPhcDlg::m_lmtVxPhotocellDelay		= {	2000,	30000				}; // in 1000ths
const FoxjetCommon::LIMITSTRUCT CHeadPhcDlg::m_lmtRelativeHeight		= {	0,		40000				}; // in 1000ths
const FoxjetCommon::LIMITSTRUCT CHeadPhcDlg::m_lmtIntTachSpeed			= {	10,		300					}; // in ft/min

int CHeadPhcDlg::CalcMaxLineSpeed (const FoxjetDatabase::HEADSTRUCT & head)
{
	int nDPI = (UINT)head.GetRes ();
	double dInchesPerSecond = 1000.0 / (double)nDPI; // 1 kHz max
	int nResult = (int)((dInchesPerSecond * 60.0) / 12.0);

	return BindTo (nResult, (int)CHeadPhcDlg::m_lmtIntTachSpeed.m_dwMin, 620);
}

#ifdef __WINPRINTER__
const CHeadPhcDlg::PHCSTRUCT CHeadPhcDlg::m_mapPHC [6] =
{
	{ 0x0300,		_T ("PHC_1"),	},
	{ 0x0310,		_T ("PHC_2"),	},
	{ 0x0320,		_T ("PHC_3"),	},
	{ 0x0330,		_T ("PHC_4"),	},
	{ 0x0340,		_T ("PHC_5"),	},
	{ 0x0350,		_T ("PHC_6"),	},
};

const CHeadPhcDlg::PHCSTRUCT CHeadPhcDlg::m_mapIVPHC [2] =
{
	{ 0x0300,		_T ("PHC_1"),	},
	{ 0x0320,		_T ("PHC_2"),	},
};

typedef enum
{
	PHC_1 = 0,
	PHC_2,
	PHC_3,
	PHC_4,
	PHC_5,
	PHC_6,
} CARDINDEX;

static ULONG GetAddress (CARDINDEX index)
{
	const CHeadPhcDlg::PHCSTRUCT * p = CHeadPhcDlg::m_mapPHC;
	int nSize = ARRAYSIZE (CHeadPhcDlg::m_mapPHC);
	ULONG lResult = 0x300;
	bool bValve = ISVALVE ();

	if (bValve) {
		p = CHeadPhcDlg::m_mapIVPHC;
		nSize = ARRAYSIZE (CHeadPhcDlg::m_mapIVPHC);
	}

	if (index < nSize)
		lResult = p [index].m_lAddr;
	else { ASSERT (0); }

	return lResult;
}

#endif //__WINPRINTER__

/////////////////////////////////////////////////////////////////////////////
// CHeadPhcDlg dialog

#ifdef __IPPRINTER__
const CString CHeadPhcDlg::m_strSyncKey = "Head sync settings";
#endif //__IPPRINTER__

CHeadPhcDlg::CHeadPhcDlg(FoxjetDatabase::COdbcDatabase & db, 
				   const FoxjetDatabase::HEADSTRUCT & head, 
				   UNITS units, const CMapStringToString & vAvailAddressMap, bool bRestricted,
				   CWnd* pParent)
:	m_db (db),
	m_head (head),
	m_bReadOnly (false),
	m_units (units),
	m_bNew (false),
	m_lLineID (-1),
	m_bRestricted (bRestricted),
	FoxjetCommon::CEliteDlg (ISVALVE () ? IDD_IVCARD : IDD_HEAD_PHC, pParent)
{
#ifdef __WINPRINTER__
	for (POSITION pos = vAvailAddressMap.GetStartPosition(); pos != NULL; ) {
		CString strKey, strAddr;

		vAvailAddressMap.GetNextAssoc (pos, strKey, strAddr);
		m_vAvailAddressMap [strKey] = strAddr;
	}	
#endif //__WINPRINTER__

	VERIFY (GetPanelRecord (m_db, m_head.m_lPanelID, m_panel));
}


void CHeadPhcDlg::DoDataExchange(CDataExchange* pDX)
{
	HEADSTRUCT & head = m_head;
	CCoord coordMin (0, 0, m_units, head);
	CCoord coordMax (0, 0, m_units, head);

	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHeadPhcDlg)
	//}}AFX_DATA_MAP

	DDX_Text (pDX, TXT_NAME, head.m_strName);
	DDV_MinMaxChars (pDX, head.m_strName, TXT_NAME, m_lmtHeadName);

#ifndef __WINPRINTER__
	DDX_Text (pDX, TXT_ADDRESS, head.m_strUID);
	DDV_MinMaxChars (pDX, head.m_strUID, TXT_ADDRESS, m_lmtHeadName);
#endif

	{
		CCoord c (0, 0, HEAD_PIXELS, head);

		DDX_Coord (pDX, TXT_PHOTOCELLDELAY, (long &)head.m_lPhotocellDelay, m_units, head, true);

		if (!IsValveHead (head)) {
			coordMin.m_x = m_lmtMphcPhotocellDelay.m_dwMin;
			coordMax.m_x = m_lmtMphcPhotocellDelay.m_dwMax;
			DDV_Coord (pDX, TXT_PHOTOCELLDELAY, head.m_lPhotocellDelay, 0, m_units, head, DDVCOORDXBOTH, coordMin, coordMax);
		}
	}

	{
		/*
		DDX_Text (pDX, TXT_PHOTOCELLRATE, head.m_dPhotocellRate);
		DDV_MinMaxDouble (pDX, head.m_dPhotocellRate, m_lmtPhotocellRate.m_dwMin, m_lmtPhotocellRate.m_dwMax);
		*/
		long lRate = (ULONG)(head.m_dPhotocellRate * 1000.0);

		DDX_Coord (pDX, TXT_PHOTOCELLRATE, lRate, m_units, head, true);

		if (pDX->m_bSaveAndValidate) {
			CCoord c (0, 0, m_units, head);

			c.m_x = lRate;
			coordMin.m_x = m_lmtPhotocellRate.m_dwMin * 1000.0;
			coordMax.m_x = m_lmtPhotocellRate.m_dwMax * 1000.0;

			// should be [6, 144"]

			DDV_Coord (pDX, TXT_PHOTOCELLRATE, lRate, 0, m_units, head, DDVCOORDXBOTH, coordMin, coordMax);
			c.m_x /= 1000.0;
			head.m_dPhotocellRate = c.m_x;

			if (IsValveHead (head)) {
				int nPixels = Round (head.m_dPhotocellRate * head.GetRes ());

				head.m_dPhotocellRate = (double)nPixels / head.GetRes ();
			}
		}
	}

	DDX_Coord (pDX, TXT_RELATIVEHEIGHT, (long &)head.m_lRelativeHeight, m_units, head, false);
	coordMin.m_y = m_lmtRelativeHeight.m_dwMin;
	coordMax.m_y = m_lmtRelativeHeight.m_dwMax;
	DDV_Coord (pDX, TXT_RELATIVEHEIGHT, 0, head.m_lRelativeHeight, m_units, head, DDVCOORDYBOTH, coordMin, coordMax);

	{
		/*
		DDX_Text (pDX, TXT_INTTACHSPEED, head.m_nIntTachSpeed);
		DDV_MinMaxUInt (pDX, head.m_nIntTachSpeed, m_lmtIntTachSpeed.m_dwMin, m_lmtIntTachSpeed.m_dwMax);
		*/

		UNITS units = m_units;

		switch (units) {
		case INCHES:		units = FEET;		break;
		case CENTIMETERS:	units = METERS;		break;
		}

		// feet --> 1000ths
		long lSpeed = (long)(head.m_nIntTachSpeed * 12000.0);

		DDX_Coord (pDX, TXT_INTTACHSPEED, lSpeed, units, head, true);

		if (pDX->m_bSaveAndValidate) {
			CCoord c (0, 0, units, head);			// note: units may not be the same as m_units
			CCoord coordMin (0, 0, units, head);	// note: units may not be the same as m_units
			CCoord coordMax (0, 0, units, head);	// note: units may not be the same as m_units
			LIMITSTRUCT lmt = m_lmtIntTachSpeed;

			if (IsValveHead (head)) 
				lmt.m_dwMax = CalcMaxLineSpeed (m_head);

			c.m_x = lSpeed;
			coordMin.m_x = lmt.m_dwMin * 12000.0;
			coordMax.m_x = lmt.m_dwMax * 12000.0;

			DDV_Coord (pDX, TXT_INTTACHSPEED, lSpeed, 0, units, head, DDVCOORDXBOTH, coordMin, coordMax);
			c.m_x /= 12000.0;
			head.m_nIntTachSpeed = (int)c.m_x;
		}
	}

	DDX_Radio (pDX, RDO_RTOL, head.m_nDirection);
	DDX_Radio (pDX, RDO_PHOTOCELLEXT, head.m_nPhotocell);
	DDX_Radio (pDX, RDO_PHOTOCELLSHAREDNONE, head.m_nSharePhoto);
	DDX_Radio (pDX, RDO_ENCODEREXT, head.m_nEncoder);
	DDX_Radio (pDX, RDO_ENCODERSHAREDNONE, head.m_nShareEnc);

	DDX_Check (pDX, CHK_MASTER, head.m_bMasterHead);
	DDX_Check (pDX, CHK_DOUBLEPULSE, head.m_bDoublePulse);
	DDX_Check (pDX, CHK_ENABLED, head.m_bEnabled);
	DDX_Check (pDX, CHK_INVERT, head.m_bInverted);
	DDX_Check (pDX, CHK_UPSIDEDOWN, head.m_bUpsidedown);
}


BEGIN_MESSAGE_MAP(CHeadPhcDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CHeadPhcDlg)
	//}}AFX_MSG_MAP	

	ON_CBN_SELCHANGE (CB_ANGLE, OnAngleChanged)
	ON_BN_CLICKED (RDO_LTOR, OnDirection)
	ON_BN_CLICKED (RDO_RTOL, OnDirection)

	ON_BN_CLICKED (RDO_PHOTOCELLEXT, OnPhotocellSource)
	ON_BN_CLICKED (RDO_PHOTOCELLINT, OnPhotocellSource)
	ON_BN_CLICKED (RDO_PHOTOCELLSHARED, OnPhotocellSource)
	
	ON_BN_CLICKED (RDO_ENCODEREXT, OnEncoderSource)
	ON_BN_CLICKED (RDO_ENCODERINT, OnEncoderSource)
	ON_BN_CLICKED (RDO_ENCODERSHARED, OnEncoderSource)

	ON_BN_CLICKED (BTN_SERIALPARAMS, OnSerial)
	ON_BN_CLICKED (CHK_MASTER, OnMaster)	
	ON_BN_CLICKED (CHK_ENABLED, OnEnabled)	
	ON_BN_CLICKED (BTN_SYNC, OnSync)	
	ON_BN_CLICKED (CHK_PCREPEATER, OnUsePcRepeater)	

	ON_CBN_SELCHANGE(CB_ADDRESS, OnSelChangeAddress)
	ON_CBN_SELCHANGE(CB_ENCRES, OnSelChangeEncRes) 

	ON_CBN_SELCHANGE (CB_TYPE, OnTypeChanged)
	ON_CBN_SELCHANGE (CB_HORZRES, OnHorzResChanged)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHeadPhcDlg message handlers

BOOL CHeadPhcDlg::OnInitDialog() 
{
	CComboBox * pStandby = (CComboBox *)GetDlgItem (CB_STANDBY);
	CString str;
	SETTINGSSTRUCT s;
	int nStandby [] = 
	{
		1,
		2,
		5,
		10,
		20
	};
	bool bValve = ISVALVE ();

	ASSERT (pStandby);

#if defined( _DEBUG ) && ( __WINPRINTER__ )
	if (bValve) {
		m_vAvailAddressMap [_T ("300")] = _T ("PHC_1");
		m_vAvailAddressMap [_T ("320")] = _T ("PHC_2");
	}
	else {
		m_vAvailAddressMap [_T ("300")] = _T ("PHC_1");
		m_vAvailAddressMap [_T ("310")] = _T ("PHC_2");
		m_vAvailAddressMap [_T ("320")] = _T ("PHC_3");
		m_vAvailAddressMap [_T ("330")] = _T ("PHC_4");
		m_vAvailAddressMap [_T ("340")] = _T ("PHC_5");
		m_vAvailAddressMap [_T ("350")] = _T ("PHC_6");
	}
#endif //_DEBUG && __WINPRINTER__

	s.m_lLineID = 0;
	s.m_strKey.Format (_T ("%d, Standby mode"), m_head.m_lID);
	s.m_strData = _T ("0");

	GetSettingsRecord (m_db, s.m_lLineID, s.m_strKey, s);
	int nStandbyTimeout = _tcstoul (s.m_strData, NULL, 10) / 60;

	str.Format (LoadString (bValve ? IDS_CARDPROPERTIES : IDS_HEADPROPERTIES), m_head.m_strName);
	SetWindowText (str);

	FoxjetCommon::CEliteDlg::OnInitDialog();

	pStandby->SetItemData (pStandby->AddString (LoadString (IDS_NEVER)), 0);
	pStandby->SetCurSel (0);
	
	for (int i = 0; i < ARRAYSIZE (nStandby); i++) {
		int n = nStandby [i];

		str.Format (LoadString (IDS_STANDBYAFTER), n);
		int nIndex = pStandby->AddString (str);
		pStandby->SetItemData (nIndex, n);

		if (nStandbyTimeout == n)
			pStandby->SetCurSel (nIndex);
	}

	m_bmpRTOL.LoadBitmap (IDB_RTOL_MATRIX); 
	m_bmpLTOR.LoadBitmap (IDB_LTOR_MATRIX); 

	FillPanels ();
	FillEncRes ();
	FillType ();

#ifdef __WINPRINTER__
	FillAddress ();
	OnSelChangeAddress ();
	UpdateData (FALSE);
	OnEncoderSource ();
	OnPhotocellSource ();
	OnTypeChanged ();

	{
		StdCommDlg::CCommItem item (false);

		item.FromString (_T ("0,") + m_head.m_strSerialParams);
		SetDlgItemText (TXT_SERIALPARAMS, item.GetDispText (1));
	}

	ULONG lAddr = _tcstoul (m_head.m_strUID, NULL, 16);

	if (lAddr > GetAddress (PHC_2))
		OnSelChangeAddress ();

	if (SOFTWARELIMITEDCONFIG) {
		if (lAddr == GetAddress (PHC_1)) {
			SetDlgItemCheck (RDO_PHOTOCELLSHARED,		0);
			SetDlgItemCheck (RDO_PHOTOCELLSHAREDNONE,	0);
			SetDlgItemCheck (RDO_PHOTOCELLSHAREDA,		1);
			SetDlgItemCheck (RDO_PHOTOCELLSHAREDB,		0);

			SetDlgItemCheck (RDO_ENCODERSHARED,			0);
			SetDlgItemCheck (RDO_ENCODERSHAREDNONE,		0);
			SetDlgItemCheck (RDO_ENCODERSHAREDA,		1);
			SetDlgItemCheck (RDO_ENCODERSHAREDB,		0);
		}
		else if (lAddr == GetAddress (PHC_2)) {
			switch (m_head.m_nPhotocell) {
			case SHARED_PC:
				SetDlgItemCheck (RDO_PHOTOCELLSHARED,		1);
				SetDlgItemCheck (RDO_PHOTOCELLSHAREDNONE,	0);
				SetDlgItemCheck (RDO_PHOTOCELLSHAREDA,		1);
				SetDlgItemCheck (RDO_PHOTOCELLSHAREDB,		0);
				break;
			default:
				SetDlgItemCheck (RDO_PHOTOCELLSHARED,		0);
				SetDlgItemCheck (RDO_PHOTOCELLSHAREDNONE,	0);
				SetDlgItemCheck (RDO_PHOTOCELLSHAREDA,		0);
				SetDlgItemCheck (RDO_PHOTOCELLSHAREDB,		1);
				break;
			}

			switch (m_head.m_nEncoder) {
			case SHARED_ENC:
				SetDlgItemCheck (RDO_ENCODERSHARED,			1);
				SetDlgItemCheck (RDO_ENCODERSHAREDNONE,		0);
				SetDlgItemCheck (RDO_ENCODERSHAREDA,		1);
				SetDlgItemCheck (RDO_ENCODERSHAREDB,		0);
				break;
			default:
				SetDlgItemCheck (RDO_ENCODERSHARED,			0);
				SetDlgItemCheck (RDO_ENCODERSHAREDNONE,		0);
				SetDlgItemCheck (RDO_ENCODERSHAREDA,		0);
				SetDlgItemCheck (RDO_ENCODERSHAREDB,		1);
				break;
			}
		}
	}

	FillHorzRes (m_head.m_nHorzRes, m_head.m_nEncoderDivisor);

#else //!__WINPRINTER__
	FillHorzRes (m_head.m_nHorzRes, m_head.m_nEncoderDivisor);
	OnTypeChanged ();
	OnPhotocellSource ();
	OnEncoderSource ();

	SetDlgItemText (RDO_PHOTOCELLEXT, LoadString (IDS_HEAD));
	SetDlgItemText (RDO_PHOTOCELLINT, LoadString (IDS_SHAREDCABLE));

	if (CButton * p = (CButton *)GetDlgItem (CHK_PCREPEATER))
		p->SetCheck (GetPcRepeater (m_db, m_head.m_lID) ? 1 : 0);

#endif

	UpdateUI ();
	OnDirection ();

	{
		if (bValve) {
			HEADSTRUCT & head = m_head;
			/*
			HEADSTRUCT head (m_head);

			head.m_vValve.RemoveAll (); // so DDX_Coord doesn't use m_vValve in call to LogicalToThousandths, or ThousandthsToLogical
			head.m_nHeadType = HEADTYPE_FIRST;
			*/

			CCoord c (0, 0, m_units, head);
			HEADSTRUCT h = head;
			UINT nHideID [] = 
			{
				CB_PANEL,
				LBL_PANEL,
				LBL_RELATIVEHEIGHT,
				TXT_RELATIVEHEIGHT,
				LBL_PRINTHEIGHT,
				TXT_PRINTHEIGHT,
				LBL_PHOTOCELLDELAY,
				TXT_PHOTOCELLDELAY,
				LBL_ANGLE,
				CB_ANGLE,
				CHK_DOUBLEPULSE,
				CHK_ENABLED,
				CHK_INVERT,
				GRP_DIRECTION,
				RDO_RTOL,
				RDO_LTOR,
				LBL_STANDBY,
				CB_STANDBY,
				IDB_DIRECTION,
				CHK_MASTER,
			};

			HeadConfigDlg::CPanel::UpdateValves (m_db, m_lLineID, h);

			for (int i = 0; i < h.m_vValve.GetSize (); i++)
				c.m_y += ((double)GetValveHeight (h.m_vValve [i].m_type) / 1000.0);

			SetDlgItemText (TXT_PRINTHEIGHT, c.FormatY ());

			for (int i = 0; i < ARRAYSIZE (nHideID); i++) 
				if (CWnd * p = GetDlgItem (nHideID [i]))
					p->ShowWindow (SW_HIDE);

			if (CWnd * p = GetDlgItem (CB_ADDRESS))
				p->EnableWindow (FALSE);
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CHeadPhcDlg::FillEncRes ()
{
	CComboBox * pEncRes = (CComboBox *)GetDlgItem (CB_ENCRES);
	CArray <UINT, UINT> vRes;
	bool bValve = ISVALVE ();

	vRes.Add (300);

	if (!bValve) 
		vRes.Add (426);

	ASSERT (pEncRes);

	for (int i = 0; i < vRes.GetSize (); i++) {
		CString str;

		str.Format (_T ("%d"), vRes [i]);
		int nIndex = pEncRes->AddString (str);
		pEncRes->SetItemData (nIndex, vRes [i]);

		if (m_head.m_nHorzRes == vRes [i])
			pEncRes->SetCurSel (nIndex);
	}

	if (pEncRes->GetCurSel () == CB_ERR)
		pEncRes->SetCurSel (0);
}

void CHeadPhcDlg::FillPanels ()
{
	CComboBox * pPanel = (CComboBox *)GetDlgItem (CB_PANEL);
	CArray <PANELSTRUCT, PANELSTRUCT &> v;
	
	ASSERT (pPanel);

	GetPanelRecords (m_db, m_panel.m_lLineID, v);

	pPanel->ResetContent ();

	for (int i = 0; i < v.GetSize (); i++) {
		PANELSTRUCT p = v [i];

		int nIndex = pPanel->AddString (p.m_strName);
		pPanel->SetItemData (nIndex, p.m_lID);

		if (p.m_lID == m_head.m_lPanelID)
			pPanel->SetCurSel (nIndex);
	}
}

void CHeadPhcDlg::OnSelChangeEncRes ()
{
	CComboBox * pEncRes = (CComboBox *)GetDlgItem (CB_ENCRES);
	CComboBox * pPrintRes = (CComboBox *)GetDlgItem (CB_HORZRES);

	ASSERT (pEncRes);
	ASSERT (pPrintRes);

	m_head.m_nHorzRes			= pEncRes->GetItemData (pEncRes->GetCurSel ());
	m_head.m_nEncoderDivisor	= pPrintRes->GetItemData (pPrintRes->GetCurSel ());
	
	OnTypeChanged ();
}

void CHeadPhcDlg::OnHorzResChanged ()
{
	CComboBox * pEncRes = (CComboBox *)GetDlgItem (CB_ENCRES);
	CComboBox * pPrintRes = (CComboBox *)GetDlgItem (CB_HORZRES);

	ASSERT (pEncRes);
	ASSERT (pPrintRes);

	m_head.m_nHorzRes			= pEncRes->GetItemData (pEncRes->GetCurSel ());
	m_head.m_nEncoderDivisor	= pPrintRes->GetItemData (pPrintRes->GetCurSel ());
	
	OnTypeChanged ();
}

void CHeadPhcDlg::FillHorzRes (int nHorzRes, int nDivisor)
{
	CComboBox * pPrintRes = (CComboBox *)GetDlgItem (CB_HORZRES);
	CComboBox * pType= (CComboBox *)GetDlgItem (CB_TYPE);
	CArray <UINT, UINT> vDivide;
	bool bValve = ISVALVE ();

	ASSERT (pPrintRes);
	ASSERT (pType);

	pPrintRes->ResetContent ();

	HEADTYPE type = (HEADTYPE)pType->GetItemData (pType->GetCurSel ());

	#ifdef __IPPRINTER__
		vDivide.Add (1);
		vDivide.Add (2);
		vDivide.Add (3);
		vDivide.Add (4);
		vDivide.Add (6);
	#else
		vDivide.Add (1);
		vDivide.Add (2);

		if (bValve) {
			vDivide.RemoveAll ();

			for (int i = 4; i <= 25; i++)
				vDivide.Add (i);
		}
	#endif

	for (int i = 0; i < vDivide.GetSize (); i++) {
		int nActualDivsor = vDivide [i];
		int nRes = (nHorzRes * 2) / nActualDivsor;
		CString str;

		#ifdef __WINPRINTER__
		nRes = nHorzRes / nActualDivsor;
		#endif //__WINPRINTER__

		str.Format (_T ("%d"), nRes);

		if (bValve) {
			CString strDebug;
			int nDivideBy = (nHorzRes * 2) / 6;
		
			double dRes = (double)nDivideBy / (double)nActualDivsor;
			str.Format (_T ("%.02f"), dRes);

			strDebug.Format (_T ("[%d] %.02f"), nActualDivsor, dRes);
			TRACEF (strDebug);
		}
		

		int nIndex = pPrintRes->AddString (str);
		pPrintRes->SetItemData (nIndex, nActualDivsor);

		if (nDivisor == nActualDivsor)
			pPrintRes->SetCurSel (nIndex);
	}

	if (pPrintRes->GetCurSel () == CB_ERR)
		pPrintRes->SetCurSel (0);
}

void CHeadPhcDlg::FillAddress ()
{
#ifdef __WINPRINTER__
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_ADDRESS);
	CString strAddress, strCard;
	ULONG lHeadAddr = _tcstoul (m_head.m_strUID, NULL, 16);
	bool bValve = ISVALVE ();

	ASSERT (pCB);

	pCB->ResetContent ();

	for (POSITION pos = m_vAvailAddressMap.GetStartPosition (); pos != NULL; ) {
		m_vAvailAddressMap.GetNextAssoc (pos, strAddress, strCard);

		int nIndex = pCB->AddString (strCard);
		ULONG lAddress = _tcstoul (strAddress, NULL, 16);
		pCB->SetItemData (nIndex, lAddress);

		if (lHeadAddr == lAddress)
			pCB->SetCurSel (nIndex);
	}

	if (m_vAvailAddressMap.GetCount () == 0) {
		const PHCSTRUCT * p = m_mapPHC;
		int nSize = ARRAYSIZE (m_mapPHC);

		if (bValve) {
			p = m_mapIVPHC;
			nSize = ARRAYSIZE (m_mapIVPHC);
		}

		for (int i = 0; i < nSize; i++) {
			if (p [i].m_lAddr == lHeadAddr) {
				int nIndex = pCB->AddString (p [i].m_lpsz);

				pCB->SetItemData (nIndex, p [i].m_lAddr);
				pCB->SetCurSel (nIndex);
			}
		}
	}

	if (pCB->GetCurSel () == CB_ERR)
		pCB->SetCurSel (0);

#endif //__WINPRINTER__
}

void CHeadPhcDlg::FillType ()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_TYPE);
	bool bValve = ISVALVE ();

	ASSERT (pCB);

	pCB->ResetContent ();
	
	struct 
	{ 
		UINT m_nType;
		UINT m_nID;
		bool m_bIsIV;
	} const map [] = 
	{
		{ UJ2_352_32,			IDS_UJ2_352_32,			false,	},
		{ UJ2_192_32,			IDS_UJ2_192_32,			false,	},
		{ UJ2_96_32,			IDS_UJ2_96_32,			false,	},

#if __CUSTOM__ != __SW0810__
		{ GRAPHICS_768_256,		IDS_GRAPHICS_768_256,	false,	},
		{ GRAPHICS_384_128,		IDS_GRAPHICS_384_128,	false,	},
#endif 

#ifdef __WINPRINTER__ 
		{ IV_72,				IDS_IV_72,				true,	},
#endif 

		{ UJ2_192_32NP,			IDS_UJ2_192_32NP,		false,	},
		{ ALPHA_CODER,			IDS_ALPHA_CODER,		false,	},
	};

	for (int i = 0; i < (sizeof (map) / sizeof (map [0])); i++) {
		#ifndef _DEBUG
		if (map [i].m_bIsIV != bValve) 
			continue;
		#endif

		int nIndex = pCB->AddString (LoadString (map [i].m_nID));
		int nType = map [i].m_nType;

		pCB->SetItemData (nIndex, nType);

		if (m_head.m_nHeadType == nType)
			pCB->SetCurSel (nIndex);
	}

	if (pCB->GetCurSel () == CB_ERR)
		pCB->SetCurSel (0);

	OnTypeChanged ();
}

int CHeadPhcDlg::CalcNFactor (double dAngle, double dRes)
{
	double x = 0.0;
//	double dRes = nHorzRes / nEncoderDivisor;
	double dNFactor = 0;
	bool bMore = true;
	const int nAngle = (int)(dAngle * 10.0);

	while (bMore) {
		x = dNFactor / (dRes * ::dChannelSeparation);
		
		if (bMore = (x >= 0.0) && (x < 0.97)) {
			double dCalcAngle = RAD2DEG (acos (x));
			int nCalcAngle = (int)(dCalcAngle * 10.0);

			if (nAngle == nCalcAngle)
				return (int)dNFactor;
		}
		
		dNFactor++;
	}

	return 0;
}

void CHeadPhcDlg::OnTypeChanged ()
{
	CComboBox * pAngle = (CComboBox *)GetDlgItem (CB_ANGLE);
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	CComboBox * pPrintRes = (CComboBox *)GetDlgItem (CB_HORZRES);
	CComboBox * pEncRes = (CComboBox *)GetDlgItem (CB_ENCRES);
	CComboBox * pStandby = (CComboBox *)GetDlgItem (CB_STANDBY);
	CString str, str90, str15;
	LPCTSTR lpszFormat = _T ("%04.1f �");
	const double dOldAngle = m_head.m_dHeadAngle;
	bool bValve = ISVALVE ();

	str90.Format (lpszFormat, 90.0);
	str15.Format (lpszFormat, 14.3);

	ASSERT (pAngle);
	ASSERT (pType);
	ASSERT (pPrintRes);
	ASSERT (pEncRes);
	ASSERT (pStandby);
	
	const HEADTYPE type = (HEADTYPE)pType->GetItemData (pType->GetCurSel ());

	pStandby->EnableWindow (type == UJ2_192_32NP);
	pAngle->ResetContent ();

	const int nHorzRes				= pEncRes->GetItemData (pEncRes->GetCurSel ());
	
	m_head.m_nChannels				= 32;
	m_head.m_dNozzleSpan			= GetNozzleSpan (type);

	#ifdef __WINPRINTER__
	if (!IsValveHead (m_head)) {
		bool bDoublePulseEnable = true;

		switch (type) {
		default:
			m_head.m_nEncoderDivisor = 2;
			break;
		case UJ2_96_32:
		case UJ2_192_32NP:
			m_head.m_nEncoderDivisor = 1;
			bDoublePulseEnable = false;
			break;
		}

		if (nHorzRes == 426)
			m_head.m_nEncoderDivisor = 2;

		if (CWnd * p = GetDlgItem (CHK_DOUBLEPULSE))
			p->EnableWindow (bDoublePulseEnable);
	}
	#endif //__WINPRINTER__

	FillHorzRes (m_head.m_nHorzRes, m_head.m_nEncoderDivisor);

	// NOTE: this can't be before FillHorzRes, or print res could be wrong
	const int nEncoderDivisor = pPrintRes->GetItemData (pPrintRes->GetCurSel ());

	switch (type) {
	case IV_72:
		{
			double dAngle = 90.0;
			str.Format (lpszFormat, dAngle);
			int nIndex = pAngle->AddString (str);

			m_head.m_nChannels	= 72;
			pAngle->SetItemData (nIndex, (int)(dAngle * 10.0));
			pAngle->SetCurSel (nIndex);
		}
		break;
	case GRAPHICS_384_128:
		{
			double dAngle = 90.0;
			str.Format (lpszFormat, dAngle);
			int nIndex = pAngle->AddString (str);

			m_head.m_nChannels	= 128;
			pAngle->SetItemData (nIndex, (int)(dAngle * 10.0));
			pAngle->SetCurSel (nIndex);
		}
		break;
	case UR4:
		m_head.m_nChannels = 384;
		break;
	case UR2:
	case GRAPHICS_768_256:
		m_head.m_nChannels = 256;
	case UJ2_352_32:
//	case ALPHA_CODER:
		{
			double dAngle = 90.0;
			str.Format (lpszFormat, dAngle);
			int nIndex = pAngle->AddString (str);
			pAngle->SetItemData (nIndex, (int)(dAngle * 10.0));
			pAngle->SetCurSel (nIndex);
		}
		break;
	default:
		{
			double x = 0.0;
			double dRes = nHorzRes / nEncoderDivisor;
			double dNFactor = 0;
			bool bMore = true;

			#ifdef __IPPRINTER__
			{
				CString str;
				int nSel = pPrintRes->GetCurSel ();

				if (nSel != CB_ERR) {
					pPrintRes->GetLBText (nSel, str);
					dRes = FoxjetDatabase::_ttof (str);
				}
			}
			#endif //__IPPRINTER__

			while (bMore) {
				x = dNFactor / (dRes * ::dChannelSeparation);
				
				double dAngle = RAD2DEG (acos (x));
				
				if (bMore = (x >= 0.0) && (x < 0.97)) {
					if (dAngle < 90.0) {
						int nAngle = (int)(dAngle * 10.0);

						str.Format (lpszFormat, (double)nAngle / 10.0);

						int nIndex = pAngle->AddString (str);
						pAngle->SetItemData (nIndex, (int)(dAngle * 10.0));
						
						#ifdef _DEBUG
						{
							CString strDebug, strHead;
							CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);

							if (pType->GetCurSel () != CB_ERR)	
								pType->GetLBText (pType->GetCurSel (), strHead);

							strDebug.Format (_T ("[%d, %d]: %s: %.01f [%d]"), 
								m_head.m_nHorzRes, m_head.m_nEncoderDivisor, 
								strHead, 
								dAngle, (int)dNFactor);
							//TRACEF (strDebug);
						}
						#endif
					}
				}
				
				dNFactor++;
			}

			if (pAngle->FindString (-1, str90) == CB_ERR) 
				pAngle->SetItemData (pAngle->InsertString (0, str90), 900);

			SelectClosestAngle (dOldAngle);

			if (m_bNew) {
				switch (type) {
				case UJ2_192_32:	SelectClosestAngle (37.0);		break;
				case UJ2_96_32:		SelectClosestAngle (24.0);		break;
				case UJ2_192_32NP:	SelectClosestAngle (15.0);		break;
				}
			}
		}

		break;
	}

	bool bValveHead = IsValveHead (type);

	{
		bool bEnable = true;
		
		#ifdef __WINPRINTER__
		bEnable = bValveHead;
		#endif

		pPrintRes->EnableWindow (bEnable);
	}

	for (int i = 0; i < pPrintRes->GetCount (); i++) {
		int nDivisor = (int)pPrintRes->GetItemData (i);

		if (nDivisor == m_head.m_nEncoderDivisor) {
			pPrintRes->SetCurSel (i);
			break;
		}
	}

	if (pPrintRes->GetCurSel () == CB_ERR)
		pPrintRes->SetCurSel (0);

/*
	{
		const UINT nID [] = 
		{
			TXT_PHOTOCELLDELAY,
			TXT_RELATIVEHEIGHT,
			RDO_RTOL,
			RDO_LTOR,
			CHK_INVERT,
			CHK_DOUBLEPULSE,
		};

		for (i = 0; i < ARRAYSIZE (nID); i++)
			if (CWnd * p = GetDlgItem (nID [i]))
				p->EnableWindow (!bValveHead);
	}
*/

	OnAngleChanged ();
	UpdateUI ();
}

int CHeadPhcDlg::SelectClosestAngle (double dAngle) 
{
	CComboBox * pAngle = (CComboBox *)GetDlgItem (CB_ANGLE);
	const int nAngle = (int)(dAngle * 10.0);
	int nMinDiff = 0;
	int nIndex = 0;

	ASSERT (pAngle);

	const int nCount = pAngle->GetCount ();

	for (int i = 0; i < nCount; i++) {
		int n = pAngle->GetItemData (i);
		int nDiff = abs (nAngle - n);

		if (i == 0) 
			nMinDiff = nDiff;

		if (nDiff < nMinDiff) {
			nMinDiff = nDiff;
			nIndex = i;
		}
	}

	pAngle->SetCurSel (nIndex);

	return nIndex;
}

void CHeadPhcDlg::OnAngleChanged ()
{
	UpdateUI ();
}

void CHeadPhcDlg::UpdateUI ()
{
	CComboBox * pAngle				= (CComboBox *)	GetDlgItem (CB_ANGLE);
	CComboBox * pType				= (CComboBox *)	GetDlgItem (CB_TYPE);
	CButton *	pPhotocellShared	= (CButton *)	GetDlgItem (RDO_PHOTOCELLSHARED);
	CButton *	pEncoderShared		= (CButton *)	GetDlgItem (RDO_ENCODERSHARED);
	CButton *	pEncoderInternal	= (CButton *)	GetDlgItem (RDO_ENCODERINT);
	CButton *	pMaster				= (CButton *)	GetDlgItem (CHK_MASTER);

	ASSERT (pAngle);
	ASSERT (pType);
	ASSERT (pPhotocellShared);
	ASSERT (pEncoderShared);
	ASSERT (pMaster);

	bool bPHC = false;
	bool bIntEnc = pEncoderInternal->GetCheck () == 1 ? true : false;

#ifdef __WINPRINTER__
	bPHC = true;
#endif

	struct
	{
		UINT m_nID;
		bool m_bVisible;
	} map [] = 
	{
		{ TXT_NAME,				true,				},
		{ CB_PANEL,				true,				},
		{ CB_HEAD,				true,				},
		{ CB_ANGLE,				true,				},
		{ TXT_PRINTHEIGHT,		true,				},
		{ TXT_PHOTOCELLDELAY,	true,				},	
		{ TXT_RELATIVEHEIGHT,	true,				},
		{ TXT_INTTACHSPEED,		true,				},
		{ CHK_MASTER,			true,				},	
		{ CHK_ENABLED,			true,				},
		{ IDB_DIRECTION,		true,				},	
		{ RDO_RTOL,				true,				},	
		{ RDO_LTOR,				true,				},	
		{ IDOK,					true,				},	
													
		{ TXT_PHOTOCELLRATE,	true,				},
		{ LBL_PHOTOCELLRATE,	true,				},
													
		{ CHK_DOUBLEPULSE,		bPHC,				},	
		{ CB_ADDRESS,			bPHC,				},
		{ GRP_PHOTOCELLSHARED,	bPHC,				},
		{ GRP_ENCODERSHARED,	bPHC,				},
		{ RDO_PHOTOCELLEXT,		true,				},
		{ RDO_PHOTOCELLINT,		true,				},
		{ RDO_ENCODEREXT,		true,				},	
		{ RDO_ENCODERINT,		true,				},	
		{ CHK_INVERT,			true,				},
		{ CHK_UPSIDEDOWN,		bPHC,				},

		{ RDO_PHOTOCELLSHARED,	bPHC,				},
		{ RDO_PHOTOCELLSHAREDA,	bPHC,				},
		{ RDO_PHOTOCELLSHAREDB,	bPHC,				},
		{ RDO_ENCODERSHARED,	bPHC,				},	
		{ RDO_ENCODERSHAREDA,	bPHC,				},	
		{ RDO_ENCODERSHAREDB,	bPHC,				},	
													
		{ LBL_SERIALPARAMS,		bPHC,				},		
		{ TXT_SERIALPARAMS,		bPHC,				},
		{ BTN_SERIALPARAMS,		bPHC,				},
													
		{ TXT_ADDRESS,			!bPHC,				},

		{ CB_HORZRES,			true,				},
		{ LBL_HORZRES,			true,				},

		{ BTN_SYNC,				!bPHC && !m_bNew,	},
		{ CHK_PCREPEATER,		!bPHC,				},

		{ LBL_ENCRES,			true,				},
		{ CB_STANDBY,			true,				},
		{ LBL_STANDBY,			true,				},
		{ CB_ENCRES,			true,				},
	};
	int nType = pType->GetItemData (pType->GetCurSel ());

	for (int i = 0; i < (sizeof (map) / sizeof (map [0])); i++) {
		if (CWnd * p = GetDlgItem (map [i].m_nID)) {

			if (!map [i].m_bVisible) 
				p->ShowWindow (SW_HIDE);
			
			//p->EnableWindow (map [i].m_bEnabled && !m_bReadOnly);
			
			if (m_bReadOnly)
				p->EnableWindow (FALSE);
		}
	}

	HEADSTRUCT h;

	h.m_dHeadAngle	= (double)pAngle->GetItemData (pAngle->GetCurSel ()) / 10.0;
	h.m_dNozzleSpan = GetNozzleSpan ((HEADTYPE)nType); 

	if (!IsValveHead (m_head.m_nHeadType)) {
		HEADSTRUCT & head = m_head;
		/*
		HEADSTRUCT head (m_head);

		head.m_vValve.RemoveAll (); // so DDX_Coord doesn't use m_vValve in call to LogicalToThousandths, or ThousandthsToLogical
		head.m_nHeadType = HEADTYPE_FIRST;
		*/

		CCoord c (0, h.Height (), m_units, head);
	
		SetDlgItemText (TXT_PRINTHEIGHT, c.FormatY ());
	}
	else {
		CComboBox * pAddress = (CComboBox *)GetDlgItem (CB_ADDRESS);

		ASSERT (pAddress);

		ULONG lAddress = pAddress->GetItemData (pAddress->GetCurSel ());

		if (CWnd * p = GetDlgItem (BTN_SERIALPARAMS))
			p->EnableWindow (lAddress == 0x300);
	}

#ifdef __IPPRINTER__
	{
		CButton * pUse = (CButton *)GetDlgItem (CHK_PCREPEATER);
		CEdit * pLen = (CEdit *)GetDlgItem (TXT_PHOTOCELLRATE);

		ASSERT (pUse);
		ASSERT (pLen);

		pLen->EnableWindow (pUse->GetCheck () == 1);
	}

#endif

	if (m_bRestricted) {
		UINT n [] =
		{
			TXT_NAME,
			CB_PANEL,
			CB_ENCRES,
			CB_HORZRES,
			CB_TYPE,
			CB_ANGLE,
			TXT_PRINTHEIGHT,
			CB_ADDRESS,
			//TXT_PHOTOCELLDELAY,
			TXT_PHOTOCELLRATE,
			CHK_MASTER,
			CHK_DOUBLEPULSE,
			CHK_ENABLED,
			CHK_INVERT,
			CHK_UPSIDEDOWN,
			CHK_PCREPEATER,
			TXT_ADDRESS,
			TXT_RELATIVEHEIGHT,
			TXT_INTTACHSPEED,
			TXT_SERIALPARAMS,
			BTN_SERIALPARAMS,
			RDO_PHOTOCELLEXT,
			RDO_PHOTOCELLINT,
			RDO_PHOTOCELLSHARED,
			RDO_PHOTOCELLSHAREDNONE,
			RDO_PHOTOCELLSHAREDA,
			RDO_PHOTOCELLSHAREDB,
			RDO_ENCODEREXT,
			RDO_ENCODERINT,
			RDO_ENCODERSHARED,
			RDO_ENCODERSHAREDNONE,
			RDO_ENCODERSHAREDA,
			RDO_ENCODERSHAREDB,
			RDO_RTOL,
			RDO_LTOR,
			IDB_DIRECTION, 
			CB_STANDBY
		};

		for (int i = 0; i < ARRAYSIZE (n); i++)
			if (CWnd * p = GetDlgItem (n [i]))
				p->EnableWindow (FALSE);
	}
}

double CHeadPhcDlg::GetNozzleSpan (HEADTYPE type)
{
	switch (type) {
	case GRAPHICS_384_128:	return 2.00;
	case GRAPHICS_768_256:	return 4.00;
	}

	return 1.813;
}

void CHeadPhcDlg::OnDirection ()
{
	CButton * pRTOL = (CButton *)GetDlgItem (RDO_RTOL);
	CStatic * pBmp = (CStatic *)GetDlgItem (IDB_DIRECTION);

	ASSERT (pRTOL);
	ASSERT (pBmp);

	pBmp->SetBitmap (pRTOL->GetCheck () == 1 ? m_bmpRTOL : m_bmpLTOR);
}

void CHeadPhcDlg::OnPhotocellSource ()
{
	CButton * pShared = (CButton *)GetDlgItem (RDO_PHOTOCELLSHARED);
	CButton * pInternal = (CButton *)GetDlgItem (RDO_PHOTOCELLINT);
	CButton * pExternal = (CButton *)GetDlgItem (RDO_PHOTOCELLEXT);
	CButton * pA = (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDA);
	CButton * pB = (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDB);
	CButton * pNone = (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDNONE);
	CComboBox * pAddress = (CComboBox *)GetDlgItem (CB_ADDRESS);
	CEdit * pAutoPrint = (CEdit *)GetDlgItem (TXT_PHOTOCELLRATE);

	ASSERT (pShared);
	ASSERT (pInternal);
	ASSERT (pExternal);
	ASSERT (pA);
	ASSERT (pB);
	ASSERT (pNone);
	ASSERT (pAddress);
	ASSERT (pAutoPrint);

#ifdef __WINPRINTER__
	ULONG lAddress = pAddress->GetItemData (pAddress->GetCurSel ());

	if (SOFTWARELIMITEDCONFIG) {
		if (lAddress == GetAddress (PHC_2)) {
			if (pShared->GetCheck () == 1) {
				pExternal->SetCheck (0);
				pInternal->SetCheck (0);
				pNone->SetCheck (0);
				pA->SetCheck (1);
				pB->SetCheck (0);
			}
			else {
				pNone->SetCheck (0);
				pA->SetCheck (0);
				pB->SetCheck (1);
			}
		}
	}

	pShared->SetWindowText (LoadString (pShared->GetCheck () == 0 ? IDS_SHAREDAS : IDS_SHAREDBY));
	pAutoPrint->EnableWindow (pInternal->GetCheck () == 1);
#endif

	UpdateUI ();
}

void CHeadPhcDlg::OnEncoderSource ()
{
#ifdef __WINPRINTER__
	CButton * pShared = (CButton *)GetDlgItem (RDO_ENCODERSHARED);
	CButton * pInternal = (CButton *)GetDlgItem (RDO_ENCODERINT);
	CButton * pExternal = (CButton *)GetDlgItem (RDO_ENCODEREXT);
	CButton * pA = (CButton *)GetDlgItem (RDO_ENCODERSHAREDA);
	CButton * pB = (CButton *)GetDlgItem (RDO_ENCODERSHAREDB);
	CButton * pNone = (CButton *)GetDlgItem (RDO_ENCODERSHAREDNONE);
	CComboBox * pAddress = (CComboBox *)GetDlgItem (CB_ADDRESS);
	CEdit * pSpeed = (CEdit *)GetDlgItem (TXT_INTTACHSPEED);
	
	ASSERT (pShared);
	ASSERT (pInternal);
	ASSERT (pExternal);
	ASSERT (pA);
	ASSERT (pB);
	ASSERT (pNone);
	ASSERT (pAddress);

	pSpeed->EnableWindow (pInternal->GetCheck () == 1);

	ULONG lAddress = pAddress->GetItemData (pAddress->GetCurSel ());

	if (SOFTWARELIMITEDCONFIG) {
		if (lAddress == GetAddress (PHC_2)) {
			if (pShared->GetCheck () == 1) {
				pExternal->SetCheck (0);
				pInternal->SetCheck (0);
				pNone->SetCheck (0);
				pA->SetCheck (1);
				pB->SetCheck (0);
			}
			else {
				pNone->SetCheck (0);
				pA->SetCheck (0);
				pB->SetCheck (1);
			}
		}
	}

	pShared->SetWindowText (LoadString (pShared->GetCheck () == 0 ? IDS_SHAREDAS : IDS_SHAREDBY));

	UpdateUI ();
#endif //__WINPRINTER__
}


void CHeadPhcDlg::OnSerial ()
{
#ifdef __WINPRINTER__
	if (EditSerialParams (m_head.m_strSerialParams)) {
		StdCommDlg::CCommItem item (false);

		item.FromString (_T ("0,") + m_head.m_strSerialParams);
		SetDlgItemText (TXT_SERIALPARAMS, item.GetDispText (1));
	}
#endif
}

void CHeadPhcDlg::OnOK ()
{
	if (!m_bReadOnly) {
		if (!UpdateData ())
			return;

		CComboBox * pAngle = (CComboBox *)GetDlgItem (CB_ANGLE);
		CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
		CComboBox * pPanel = (CComboBox *)GetDlgItem (CB_PANEL);
		CComboBox * pStandby = (CComboBox *)GetDlgItem (CB_STANDBY);
		SETTINGSSTRUCT s;

		ASSERT (pPanel);
		ASSERT (pAngle);
		ASSERT (pType);
		ASSERT (pStandby);

		int nStandby = pStandby->GetItemData (pStandby->GetCurSel ());

		s.m_lLineID = 0;
		s.m_strKey.Format (_T ("%d, Standby mode"), m_head.m_lID);
		s.m_strData.Format (_T ("%d"), nStandby * 60);

//		if (!m_bRestricted)
			if (!UpdateSettingsRecord (m_db, s))
				VERIFY (AddSettingsRecord (m_db, s));

		m_head.m_dHeadAngle	= (double)pAngle->GetItemData (pAngle->GetCurSel ()) / 10.0;
		m_head.m_nHeadType = (HEADTYPE)pType->GetItemData (pType->GetCurSel ());
		m_head.m_lPanelID = (ULONG)pPanel->GetItemData (pPanel->GetCurSel ());

#ifdef __WINPRINTER__
		CComboBox * pAddress = (CComboBox *)GetDlgItem (CB_ADDRESS);

		ASSERT (pAddress);

		ULONG lAddress = pAddress->GetItemData (pAddress->GetCurSel ());

		m_head.m_strUID.Format (_T ("%X"), lAddress);

		CButton * pShareEncA = (CButton *)GetDlgItem (RDO_ENCODERSHAREDA);
		CButton * pShareEncB = (CButton *)GetDlgItem (RDO_ENCODERSHAREDB);
		CButton * pSharePhotoA = (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDA);
		CButton * pSharePhotoB = (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDB);

		ASSERT (pShareEncA);
		ASSERT (pShareEncB);
		ASSERT (pSharePhotoA);
		ASSERT (pSharePhotoB);

		m_head.m_nSharePhoto		= SHARED_PCNONE;
		m_head.m_nShareEnc			= SHARED_ENCNONE;
		m_head.m_bRemoteHead		= false;
		m_head.m_nEncoderDivisor	= 2;

		if (m_head.m_nPhotocell == -1)
			m_head.m_nPhotocell = EXTERNAL_PC;

		if (m_head.m_nEncoder == -1)
			m_head.m_nEncoder = EXTERNAL_ENC;

		if (pSharePhotoB->GetCheck () == 1)
			m_head.m_nSharePhoto = SHARED_PCB;
		else if (pSharePhotoA->GetCheck () == 1)
			m_head.m_nSharePhoto = SHARED_PCA;

		if (pShareEncB->GetCheck () == 1)
			m_head.m_nShareEnc = SHARED_ENCB;
		else if (pShareEncA->GetCheck () == 1)
			m_head.m_nShareEnc = SHARED_ENCA;

		CComboBox * pEncRes			= (CComboBox *)GetDlgItem (CB_ENCRES);

		ASSERT (pEncRes);

		m_head.m_nHorzRes			= pEncRes->GetItemData (pEncRes->GetCurSel ());

		CComboBox * pPrintRes		= (CComboBox *)GetDlgItem (CB_HORZRES);
		ASSERT (pPrintRes);
		m_head.m_nEncoderDivisor	= pPrintRes->GetItemData (pPrintRes->GetCurSel ());

#else // if __IPPRINTER__
		CComboBox * pPrintRes		= (CComboBox *)GetDlgItem (CB_HORZRES);
		CComboBox * pEncRes			= (CComboBox *)GetDlgItem (CB_ENCRES);

		ASSERT (pPrintRes);
		ASSERT (pEncRes);

		m_head.m_nEncoderDivisor	= pPrintRes->GetItemData (pPrintRes->GetCurSel ());
		m_head.m_nHorzRes			= pEncRes->GetItemData (pEncRes->GetCurSel ());

		/* TODO: rem
		CString str; 
		CString strRes;
		pPrintRes->GetLBText (pPrintRes->GetCurSel (), strRes);
		str.Format ("%d [%d] = %s",
			m_head.m_nHorzRes,
			m_head.m_nEncoderDivisor,
			strRes);
		TRACEF (str);
		*/

		if (CButton * p = (CButton *)GetDlgItem (CHK_PCREPEATER)) 
			SetPcRepeater (m_db, m_head.m_lID, p->GetCheck () ? true : false);
#endif 

		if (!m_bNew) {
//			if (!m_bRestricted) 
				VERIFY (UpdateHeadRecord (m_db, m_head));
		}

		m_head.m_nChannels = m_head.GetChannels ();

		EndDialog (IDOK);
		//FoxjetCommon::CEliteDlg::OnOK ();
	}
}

void CHeadPhcDlg::OnMaster ()
{
	CButton * pMaster = (CButton *)GetDlgItem (CHK_MASTER);
	CButton * pEnabled = (CButton *)GetDlgItem (CHK_ENABLED);

	ASSERT (pMaster);
	ASSERT (pEnabled);

	if (pMaster->GetCheck () == 1)
		pEnabled->SetCheck (1);
}

void CHeadPhcDlg::OnUsePcRepeater ()
{
	UpdateUI ();
}

void CHeadPhcDlg::OnEnabled ()
{
	CButton * pEnabled = (CButton *)GetDlgItem (CHK_ENABLED);
	CButton * pMaster = (CButton *)GetDlgItem (CHK_MASTER);

	ASSERT (pMaster);
	ASSERT (pEnabled);

	if (pEnabled->GetCheck () == 0) 
		pMaster->SetCheck (0);

	UpdateUI ();
}

#ifdef __WINPRINTER__
CString CHeadPhcDlg::GetPHC (ULONG lAddr)
{
	const PHCSTRUCT * p = m_mapPHC;
	int nSize = ARRAYSIZE (m_mapPHC);
	bool bValve = ISVALVE ();

	if (bValve) {
		p = m_mapIVPHC;
		nSize = ARRAYSIZE (m_mapIVPHC);
	}

	for (int i = 0; i < nSize; i++) 
		if (p [i].m_lAddr == lAddr)
			return p [i].m_lpsz;

	return _T ("");
}

ULONG CHeadPhcDlg::GetPHC (const CString & str)
{
	const PHCSTRUCT * p = m_mapPHC;
	int nSize = ARRAYSIZE (m_mapPHC);
	bool bValve = ISVALVE ();

	if (bValve) {
		p = m_mapIVPHC;
		nSize = ARRAYSIZE (m_mapIVPHC);
	}

	for (int i = 0; i < nSize; i++) 
		if (p [i].m_lpsz == str)
			return p [i].m_lAddr;

	return 0;
}
#endif //__WINPRINTER__

void CHeadPhcDlg::SetDlgItemCheck (UINT nID, bool bCheck)
{
	if (CButton * p = (CButton *)GetDlgItem (nID)) 
		p->SetCheck (bCheck ? 1 : 0);
}

bool CHeadPhcDlg::GetDlgItemCheck (UINT nID) 
{
	if (CButton * p = (CButton *)GetDlgItem (nID)) 
		return p->GetCheck () == 1 ? true : false;

	return false;
}

void CHeadPhcDlg::OnSelChangeAddress() 
{
#ifdef __WINPRINTER__
	CComboBox * pAddress = (CComboBox *)GetDlgItem (CB_ADDRESS);

	ASSERT (pAddress);
	UpdateUI ();

	ULONG lAddress = pAddress->GetItemData (pAddress->GetCurSel ());
	bool bShare = lAddress != GetAddress (PHC_1) && lAddress != GetAddress (PHC_2);

	CButton * pPhotoExt			= (CButton *)GetDlgItem (RDO_PHOTOCELLEXT);
	CButton * pPhotoInt			= (CButton *)GetDlgItem (RDO_PHOTOCELLINT);
	CButton * pPhotoShare		= (CButton *)GetDlgItem (RDO_PHOTOCELLSHARED);
	CButton * pPhotoShareNone	= (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDNONE);
	CButton * pPhotoShareA		= (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDA);
	CButton * pPhotoShareB		= (CButton *)GetDlgItem (RDO_PHOTOCELLSHAREDB);

	CButton * pEncExt			= (CButton *)GetDlgItem (RDO_ENCODEREXT);
	CButton * pEncInt			= (CButton *)GetDlgItem (RDO_ENCODERINT);
	CButton * pEncShare			= (CButton *)GetDlgItem (RDO_ENCODERSHARED);
	CButton * pEncShareNone		= (CButton *)GetDlgItem (RDO_ENCODERSHAREDNONE);
	CButton * pEncShareA		= (CButton *)GetDlgItem (RDO_ENCODERSHAREDA);
	CButton * pEncShareB		= (CButton *)GetDlgItem (RDO_ENCODERSHAREDB);

	ASSERT (pEncExt);
	ASSERT (pEncInt);
	ASSERT (pEncShare);
	ASSERT (pEncShareNone);
	ASSERT (pEncShareA);
	ASSERT (pEncShareB);

	ASSERT (pPhotoExt); 
	ASSERT (pPhotoInt); 
	ASSERT (pPhotoShare);
	ASSERT (pPhotoShareNone);
	ASSERT (pPhotoShareA);
	ASSERT (pPhotoShareB);

	if (SOFTWARELIMITEDCONFIG) {
		if (lAddress == GetAddress (PHC_1)) {
			pPhotoExt->EnableWindow (TRUE); 
			pPhotoInt->EnableWindow (TRUE); 
			pPhotoShare->EnableWindow (FALSE);
			pPhotoShareA->EnableWindow (FALSE);
			pPhotoShareB->EnableWindow (FALSE);

			pEncExt->EnableWindow (TRUE);
			pEncInt->EnableWindow (TRUE);
			pEncShare->EnableWindow (FALSE);
			pEncShareA->EnableWindow (FALSE);
			pEncShareB->EnableWindow (FALSE);
		}
		else if (lAddress == GetAddress (PHC_2)) {
			pPhotoExt->EnableWindow (TRUE); 
			pPhotoInt->EnableWindow (TRUE); 
			pPhotoShare->EnableWindow (TRUE);
			pPhotoShareA->EnableWindow (FALSE);
			pPhotoShareB->EnableWindow (FALSE);

			pEncExt->EnableWindow (TRUE);
			pEncInt->EnableWindow (TRUE);
			pEncShare->EnableWindow (TRUE);
			pEncShareA->EnableWindow (FALSE);
			pEncShareB->EnableWindow (FALSE);
		}
		else {
			pPhotoExt->EnableWindow (FALSE); 
			pPhotoInt->EnableWindow (FALSE); 
			pPhotoShare->EnableWindow (TRUE);
			pPhotoShareA->EnableWindow (TRUE);
			pPhotoShareB->EnableWindow (TRUE);

			pEncExt->EnableWindow (FALSE);
			pEncInt->EnableWindow (FALSE);
			pEncShare->EnableWindow (TRUE);
			pEncShareA->EnableWindow (TRUE);
			pEncShareB->EnableWindow (TRUE);
		}

		if (bShare) {
			if (pPhotoExt->GetCheck () || pPhotoInt->GetCheck ()) {
				pPhotoExt->SetCheck (0);
				pPhotoInt->SetCheck (0);
				pPhotoShare->SetCheck (1);
				pPhotoShareNone->SetCheck (0);
				pPhotoShareA->SetCheck (1);
				pPhotoShareB->SetCheck (0);
			}

			if (pEncExt->GetCheck () || pEncInt->GetCheck ()) {
				pEncExt->SetCheck (0);
				pEncInt->SetCheck (0);
				pEncShare->SetCheck (1);
				pEncShareNone->SetCheck (0);
				pEncShareA->SetCheck (1);
				pEncShareB->SetCheck (0);
			}
		}
		else {
			if (lAddress == GetAddress (PHC_1)) {
				if (pPhotoShare->GetCheck () || pPhotoShareA->GetCheck () || pPhotoShareB->GetCheck ()) {
					pPhotoExt->SetCheck (1);
					pPhotoInt->SetCheck (0);
				}

				if (pEncShare->GetCheck () || pEncShareA->GetCheck () || pEncShareB->GetCheck ()) {
					pEncExt->SetCheck (1);
					pEncInt->SetCheck (0);
				}

				pPhotoShare->SetCheck (0);
				pPhotoShareNone->SetCheck (0);
				pPhotoShareA->SetCheck (1);
				pPhotoShareB->SetCheck (0);

				pEncShare->SetCheck (0);
				pEncShareNone->SetCheck (0);
				pEncShareA->SetCheck (1);
				pEncShareB->SetCheck (0);
			}

			if (lAddress == GetAddress (PHC_2)) {
				if (pPhotoShare->GetCheck ()) {
					pPhotoExt->SetCheck (0);
					pPhotoInt->SetCheck (0);
					pPhotoShare->SetCheck (0);
					pPhotoShareNone->SetCheck (0);
					pPhotoShareA->SetCheck (1);
					pPhotoShareB->SetCheck (0);
				}
				else {
					if (!pPhotoExt->GetCheck () && !pPhotoInt->GetCheck ()) {
						pPhotoExt->SetCheck (0);
						pPhotoInt->SetCheck (0);
					}

					pPhotoShare->SetCheck (0);
					pPhotoShareNone->SetCheck (0);
					pPhotoShareA->SetCheck (1);
					pPhotoShareB->SetCheck (0);
				}

				if (pEncShare->GetCheck ()) {
					pEncExt->SetCheck (0);
					pEncInt->SetCheck (0);
					pEncShare->SetCheck (0);
					pEncShareNone->SetCheck (0);
					pEncShareA->SetCheck (1);
					pEncShareB->SetCheck (0);
				}
				else {
					if (!pEncExt->GetCheck () && !pEncInt->GetCheck ()) {
						pEncExt->SetCheck (0);
						pEncInt->SetCheck (0);
					}

					pEncShare->SetCheck (0);
					pEncShareNone->SetCheck (0);
					pEncShareA->SetCheck (1);
					pEncShareB->SetCheck (0);
				}
			}
		}
	}
	
	UpdateUI ();
	OnEncoderSource ();
	OnPhotocellSource ();
#endif //__WINPRINTER__
}

#ifdef __IPPRINTER__
bool CHeadPhcDlg::GetPcRepeater (FoxjetDatabase::COdbcDatabase & db, ULONG lHeadID)
{
	SETTINGSSTRUCT pc;

	pc.m_lLineID = 0;
	pc.m_strKey.Format (lpszPcRepeater, lHeadID);
	
	GetSettingsRecord (db, pc.m_lLineID, pc.m_strKey, pc);

	return _ttoi (pc.m_strData) ? true : false;
}

void CHeadPhcDlg::SetPcRepeater (FoxjetDatabase::COdbcDatabase & db, ULONG lHeadID, bool bUse)
{
	DBMANAGETHREADSTATE (db);
	SETTINGSSTRUCT pc;

	pc.m_lLineID = 0;
	pc.m_strKey.Format (lpszPcRepeater, lHeadID);
	pc.m_strData.Format (_T ("%d"), bUse);
	
	if (!UpdateSettingsRecord (db, pc))
		VERIFY (AddSettingsRecord (db, pc));
}

DWORD CHeadPhcDlg::GetSyncSettings (FoxjetDatabase::COdbcDatabase & db)
{
	DBMANAGETHREADSTATE (db);
	using namespace FoxjetIpElements;

	SETTINGSSTRUCT s;
	DWORD dw = CSyncDlg::SYNC_FONTS | CSyncDlg::SYNC_MESSAGES;

	if (GetSettingsRecord (db, 0, m_strSyncKey, s))
		dw = _tcstoul (s.m_strData, NULL, 16);

	return dw;
}
#endif //__IPPRINTER__

void CHeadPhcDlg::OnSync ()
{
#ifdef __IPPRINTER__
	using namespace FoxjetIpElements;

	SETTINGSSTRUCT s;
	DWORD dw = CSyncDlg::SYNC_SHOW_DELETE | CSyncDlg::SYNC_SHOW_ALLHEADS;

	if (GetSettingsRecord (m_db, 0, m_strSyncKey, s))
		dw |= _tcstoul (s.m_strData, NULL, 16);

	CSyncDlg dlg (dw, 0, this);

	dlg.m_strPrompt = LoadString (IDS_SYNCNOW);

	if (dlg.DoModal () == IDOK) {
		HWND hWnd = NULL;
		ULONG lHeadID = m_head.m_lID;

		dw = 0;
		dw |= dlg.m_bFonts		? CSyncDlg::SYNC_FONTS				: 0;
		dw |= dlg.m_bMessages	? CSyncDlg::SYNC_MESSAGES			: 0;
		dw |= dlg.m_bDelete		? CSyncDlg::SYNC_DELETE_MEMSTORE	: 0;
		dw |= CSyncDlg::SYNC_NO_PROMPT;

		if (dlg.m_bAllHeads)
			lHeadID = -1;

		if (CWnd * pConfigDlg = GetParent ())
			if (CWnd * pParent = pConfigDlg->GetParent ())
				hWnd = pParent->m_hWnd;

		::PostMessage (hWnd, WM_SYNCREMOTEHEAD, lHeadID, dw);
	}

#endif //__IPPRINTER__
}
