#ifdef __ELEMENT_INLINE__

static const CRect rcNull (0, 0, 0, 0);

inline _3D_API bool operator == (const Element::CPair & lhs, const Element::CPair & rhs)
{
	return 
		lhs.m_pElement == rhs.m_pElement &&
		lhs.m_pList == rhs.m_pList;
}

inline _3D_API bool operator != (const Element::CPair & lhs, const Element::CPair & rhs)
{
	return !(lhs == rhs);
}

inline bool Element::CElement::GetTrackerRect (CRect & rc) const
{
	if (m_pTracker) {
		rc = m_pTracker->m_rect;
	}

	return m_pTracker ? true : false;
}

inline bool Element::CElement::SetTrackerRect (const CRect & rcTracker)
{
	CRect rc (rcTracker);

	if (m_pTracker) {
		m_pTracker->m_rect = rc;
	}

	return m_pTracker ? true : false;
}

inline int Element::CElement::HitTest (const CPoint & pt) const
{
	int nResult = CRectTracker::hitNothing;

	if (m_pTracker) 
		nResult = m_pTracker->HitTest (pt);

	return nResult;
}

inline HCURSOR Element::CElement::GetCursor (const CPoint & pt) const
{
	HCURSOR hCursor = ::AfxGetApp ()->LoadStandardCursor (IDC_ARROW);

	if (m_pTracker) {
      switch (m_pTracker->HitTest (pt)) {
        	case CRectTracker::hitTopLeft:
        	case CRectTracker::hitBottomRight:
			  	hCursor = ::AfxGetApp ()->LoadStandardCursor (IDC_SIZENWSE);
            break;
        	case CRectTracker::hitTopRight:
        	case CRectTracker::hitBottomLeft:
			  	hCursor = ::AfxGetApp ()->LoadStandardCursor (IDC_SIZENESW);
            break;
        	case CRectTracker::hitRight:
        	case CRectTracker::hitLeft:
			  	hCursor = ::AfxGetApp ()->LoadStandardCursor (IDC_SIZEWE);
            break;
        	case CRectTracker::hitTop:
        	case CRectTracker::hitBottom:
			  	hCursor = ::AfxGetApp ()->LoadStandardCursor (IDC_SIZENS);
            break;
        	case CRectTracker::hitMiddle:
			  	hCursor = ::AfxGetApp ()->LoadStandardCursor (IDC_SIZEALL);
          	break;
  	   }
  	}

   return hCursor;
}

inline HCURSOR Element::CElement::GetCursor (int nHit)
{
	HCURSOR hCursor = ::AfxGetApp ()->LoadStandardCursor (IDC_ARROW);

	switch (nHit) {
		case CRectTracker::hitTopLeft:
		case CRectTracker::hitBottomRight:
			hCursor = ::AfxGetApp ()->LoadStandardCursor (IDC_SIZENWSE);
			break;
		case CRectTracker::hitTopRight:
		case CRectTracker::hitBottomLeft:
			hCursor = ::AfxGetApp ()->LoadStandardCursor (IDC_SIZENESW);
			break;
		case CRectTracker::hitRight:
		case CRectTracker::hitLeft:
			hCursor = ::AfxGetApp ()->LoadStandardCursor (IDC_SIZEWE);
			break;
		case CRectTracker::hitTop:
		case CRectTracker::hitBottom:
			hCursor = ::AfxGetApp ()->LoadStandardCursor (IDC_SIZENS);
			break;
		case CRectTracker::hitMiddle:
			hCursor = ::AfxGetApp ()->LoadStandardCursor (IDC_SIZEALL);
			break;
	}

   return hCursor;
}

inline const FoxjetCommon::CBaseElement * Element::CElement::GetElement () const
{
	ASSERT (m_pElement);
	return m_pElement;
}

inline FoxjetCommon::CBaseElement * Element::CElement::GetElement ()
{
	ASSERT (m_pElement);
	return m_pElement;
}

inline bool Element::CElement::IsResizable () const
{
	return GetElement ()->IsResizable ();
}

inline bool Element::CElement::IsMovable () const
{
//	if (GetOwner () != NULL)
//		return false;
//	else
		return GetElement ()->IsMovable ();
}

inline bool Element::CElement::IsLockAspectRatio () const
{
	return GetElement ()->IsLockAspectRatio ();
}

inline bool Element::CElement::IsSelected () const
{
	return m_pTracker != NULL;
}

inline CString Element::CElement::GetFontName () const
{
	if (GetElement ()->HasFont ()) {
		FoxjetCommon::CBaseTextElement * p = (FoxjetCommon::CBaseTextElement *)GetElement ();
		return p->GetFontName ();
	}

	return _T ("");
}

inline UINT Element::CElement::GetFontSize () const
{
	if (GetElement ()->HasFont ()) {
		FoxjetCommon::CBaseTextElement * p = (FoxjetCommon::CBaseTextElement *)GetElement ();
		return p->GetFontSize ();
	}

	return 0;
}

inline bool Element::CElement::IsFontBold () const
{
	if (GetElement ()->HasFont ()) {
		FoxjetCommon::CBaseTextElement * p = (FoxjetCommon::CBaseTextElement *)GetElement ();
		return p->GetFontBold ();
	}

	return false;
}

inline int Element::CElement::GetWidth () const
{
	return GetElement ()->GetWidth ();
}

inline bool Element::CElement::IsFontItalic () const
{
	if (GetElement ()->HasFont ()) {
		FoxjetCommon::CBaseTextElement * p = (FoxjetCommon::CBaseTextElement *)GetElement ();
		return p->GetFontItalic ();
	}

	return false;
}

inline bool Element::CElement::SetPos(const CPoint &pt, const FoxjetDatabase::HEADSTRUCT & head)
{
	return GetElement ()->SetPos (FoxjetCommon::CBaseElement::LogicalToThousandths (pt, head), &head);
}

inline bool Element::CElement::SetXPos(const CPoint &pt, const FoxjetDatabase::HEADSTRUCT & head)
{
	return GetElement ()->SetXPos (FoxjetCommon::CBaseElement::LogicalToThousandths (pt, head), &head);
}

inline bool Element::CElement::SetYPos(const CPoint &pt, const FoxjetDatabase::HEADSTRUCT & head)
{
	return GetElement ()->SetYPos (FoxjetCommon::CBaseElement::LogicalToThousandths (pt, head), &head);
}

inline CPoint Element::CElement::GetPos(const FoxjetDatabase::HEADSTRUCT & head) const
{
	return FoxjetCommon::CBaseElement::ThousandthsToLogical (GetElement ()->GetPos (&head), head);
}

////////////////////////////////////////////////////////////////////////////////////////

inline int Element::CElementTracker::HitTest (CPoint pt)
{
	int nResult = CRectTracker::hitNothing;

	if (IsMovable ()) {
		CRect rc = m_rect;

		if (rc.PtInRect (pt))
			nResult = CRectTracker::hitMiddle;

		if (IsResizable ()) {
			CalcHandles ();

			for (int i = 0; i < 8; i++) {
				int nIndex = m_nHandleIndex [i];

				if (m_rcHandles [nIndex].PtInRect (pt)) {
					switch (nIndex) {
						case TL: nResult = hitTopLeft;		break;
						case T:  nResult = hitTop;			break;
						case TR: nResult = hitTopRight;		break;
						case R:  nResult = hitRight;		break;
						case BR: nResult = hitBottomRight;	break;
						case B:  nResult = hitBottom;		break;
						case BL: nResult = hitBottomLeft;	break;
						case L:  nResult = hitLeft;			break;
					}
				}
			}
		}
	}

	return nResult;
}

inline void Element::CElementTracker::CalcHandles ()
{
	CalcHandles (m_rcHandles, m_rect);
}

inline void Element::CElementTracker::CalcHandles (CRect * pHandles, const CRect & rcFrom) const
{
	for (int i = 0; i < 8; i++)
		pHandles [i] = ::rcNull;

	CRect rc = rcFrom;
	const int cx = rc.Width ();
	const int cy = rc.Height ();
	const CSize handle = IsResizable () ? GetHandleSize () : CSize (m_nDragHandleSize, m_nDragHandleSize);
	const CSize offset (0, 0); //(handle.cx / 2, handle.cy / 2);

	pHandles [TL] 	= CRect (rc.left, rc.top, rc.left + handle.cx, rc.top + handle.cy);
	pHandles [TR] 	= CRect (rc.right - handle.cx, rc.top, rc.right, rc.top + handle.cy);
	pHandles [BR] 	= CRect (rc.right - handle.cx, rc.bottom - handle.cy, rc.right, rc.bottom);
	pHandles [BL] 	= CRect (rc.left, rc.bottom - handle.cy, rc.left + handle.cx, rc.bottom);

	pHandles [TL]	+= CPoint (-offset.cx, -offset.cy);
	pHandles [TR] 	+= CPoint (offset.cx, -offset.cy);
	pHandles [BR] 	+= CPoint (offset.cx, offset.cy);
	pHandles [BL] 	+= CPoint (-offset.cx, offset.cy);

	if (IsResizable ()) {
		int nCorner [4] = 
		{
			TL,
			TR,
			BR,
			BL,
		};
		int nSide [4] = 
		{
			T,
			R,
			B,
			L,
		};

		pHandles [T] 	= CRect (rc.left + (rc.Width () - handle.cx) / 2, rc.top, rc.left + (rc.Width () + handle.cx) / 2, rc.top + handle.cy);
		pHandles [R] 	= CRect (rc.right - handle.cx, rc.top + (rc.Height () - handle.cy) / 2, rc.right, rc.top + (rc.Height () + handle.cy) / 2);
		pHandles [B] 	= CRect (rc.left + (rc.Width () - handle.cx) / 2, rc.bottom - handle.cy, rc.left + (rc.Width () + handle.cx) / 2, rc.bottom);
		pHandles [L] 	= CRect (rc.left, rc.top + (rc.Height () - handle.cy) / 2, rc.left + handle.cx, rc.top + (rc.Height () + handle.cy) / 2);

		pHandles [T] 	+= CPoint (0, -offset.cy);
		pHandles [R] 	+= CPoint (offset.cx, 0);
		pHandles [B] 	+= CPoint (0, offset.cy);
		pHandles [L] 	+= CPoint (-offset.cx, 0);

		// remove any side handles that overlap the corners
		for (int i = 0; i < 4; i++) {
			int nSideIndex = nSide [i];
			CRect rcSide = * pHandles [nSideIndex];

			for (int j = 0; j < 4; j++) {
				CRect rcCorner = * pHandles [nCorner [j]];

				if (rcCorner.IntersectRect (rcCorner, rcSide)) {
					pHandles [nSideIndex] = ::rcNull;
					break;
				}
			}
		}
	}

	for (int i = 0; i < 8; i++)
		pHandles [i].IntersectRect (pHandles [i], rcFrom);
}

inline CRect Element::CElementTracker::GetHandle (int nIndex)
{
	if (nIndex >= 0 && nIndex < 8)
		return m_rcHandles [nIndex];
	else
		return CRect (0, 0, 0, 0);
}

#endif __ELEMENT_INLINE__
