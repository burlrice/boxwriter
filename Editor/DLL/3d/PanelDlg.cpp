// PanelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "PanelDlg.h"
#include "Resource.h"
#include "Debug.h"
#include "List.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Foxjet3d;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;

/////////////////////////////////////////////////////////////////////////////
// CPanelDlg dialog

CPanelDlg::CPanelDlg (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::PANELSTRUCT & panel, CWnd* pParent)
:	m_db (db),
	m_panel (panel),
	m_bReadOnly (false),
	FoxjetCommon::CEliteDlg(IDD_PANEL_MATRIX, pParent)
{
	LINESTRUCT line;

	if (GetLineRecord (panel.m_lLineID, line))
		m_strLine = line.m_strName;
	else { ASSERT (0); }

	m_strName		= m_panel.m_strName;
	m_nNumber		= m_panel.m_nNumber;
	m_nDirection	= m_panel.m_direction;
	m_nOrientation	= m_panel.m_orientation;

	m_bmpDirection [0].LoadBitmap (IDB_LTOR_MATRIX);		
	m_bmpDirection [1].LoadBitmap (IDB_RTOL_MATRIX);		

	m_bmpOrientation [0].LoadBitmap (IDB_ROT0_MATRIX);		
	m_bmpOrientation [1].LoadBitmap (IDB_ROT90_MATRIX);		
	m_bmpOrientation [2].LoadBitmap (IDB_ROT180_MATRIX);	
	m_bmpOrientation [3].LoadBitmap (IDB_ROT270_MATRIX);	
}

void CPanelDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPanelDlg)
	DDX_Text(pDX, TXT_LINE, m_strLine);
	DDX_Text(pDX, TXT_NAME, m_strName);
	DDX_Text(pDX, TXT_NUMBER, m_nNumber);
	//}}AFX_DATA_MAP

	DDX_Radio (pDX, RDO_LTOR, (int &)m_nDirection);
	DDX_Radio (pDX, RDO_ROT0, (int &)m_nOrientation);

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
}


BEGIN_MESSAGE_MAP(CPanelDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CPanelDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (RDO_LTOR, OnDirectionClick)
	ON_BN_CLICKED (RDO_RTOL, OnDirectionClick)
	ON_BN_CLICKED (RDO_ROT0, OnOrientationClick)
	ON_BN_CLICKED (RDO_ROT90, OnOrientationClick)
	ON_BN_CLICKED (RDO_ROT180, OnOrientationClick)
	ON_BN_CLICKED (RDO_ROT270, OnOrientationClick)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPanelDlg message handlers

BOOL CPanelDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	ASSERT (GetDlgItem (IDOK));
	GetDlgItem (IDOK)->EnableWindow (!m_bReadOnly);
	OnDirectionClick ();
	OnOrientationClick ();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPanelDlg::OnOrientationClick()
{
	CButton * pRadio [] = 
	{
		(CButton *)GetDlgItem (RDO_ROT0),
		(CButton *)GetDlgItem (RDO_ROT90),
		(CButton *)GetDlgItem (RDO_ROT180),
		(CButton *)GetDlgItem (RDO_ROT270),
	};

	for (int i = 0; i < 4; i++) {
		ASSERT (pRadio [i]);
		
		if (pRadio [i]->GetCheck () == 1) {
			CStatic * pBmp = (CStatic *)GetDlgItem (BMP_ROT);
			ASSERT (pBmp);
			
			pBmp->SetBitmap (m_bmpOrientation [i]);
		}
	}
}

void CPanelDlg::OnDirectionClick()
{
	CButton * pRadio [] = 
	{
		(CButton *)GetDlgItem (RDO_LTOR),
		(CButton *)GetDlgItem (RDO_RTOL),
	};

	for (int i = 0; i < 2; i++) {
		ASSERT (pRadio [i]);
		
		if (pRadio [i]->GetCheck () == 1) {
			CStatic * pBmp = (CStatic *)GetDlgItem (BMP_DIRECTION);
			ASSERT (pBmp);
			
			pBmp->SetBitmap (m_bmpDirection [i]);
		}
	}
}
