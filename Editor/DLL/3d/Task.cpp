// Task.cpp: implementation of the CTask class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "3d.h"
#include "Task.h"
#include "TemplExt.h" 
#include "Debug.h"
#include "FileExt.h"
#include "Compare.h"
#include "Utils.h"
#include "Coord.h"
#include "InvalidElementsDlg.h"
#include "Resource.h"
#include "Box.h"
#include "Extern.h"
#include "Parse.h"
#include "ValvePropDlg.h"
#include "TextElement.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;
using namespace Foxjet3d;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ListGlobals;
using namespace Panel;
using namespace Element;

#ifdef __WINPRINTER__
	#include "BarcodeParams.h"
	#include "BarcodeElement.h"

	using namespace FoxjetElements;
#endif //__WINPRINTER__


_3D_API bool Foxjet3d::operator == (const Foxjet3d::CTask & lhs, const Foxjet3d::CTask & rhs)
{
	const TASKSTRUCT & lhsTask = lhs.GetMembers ();
	const TASKSTRUCT & rhsTask = rhs.GetMembers ();

	if (lhsTask == rhsTask) {
		if (lhs.m_vLists.GetSize () == rhs.m_vLists.GetSize ()) {
			for (int i = 0; i < lhs.m_vLists.GetSize (); i++) {
				const CMessage * pLhs = lhs.m_vLists [i];
				const CMessage * pRhs = rhs.m_vLists [i];

				if (pLhs && pRhs && (* pLhs) != (* pRhs))
					return false;
			}

			return true;
		}
	}

	return false;
}

_3D_API bool Foxjet3d::operator != (const Foxjet3d::CTask & lhs, const Foxjet3d::CTask & rhs)
{
	return !(lhs == rhs);
}

const FoxjetCommon::CVersion CTask::m_verUninit = CVersion (-1, -1);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTask::CTask()
:	TASKSTRUCT ()
{
}

CTask::CTask(const FoxjetDatabase::TASKSTRUCT & rhs)
:	TASKSTRUCT (rhs)
{
	TASKSTRUCT * pRhs = (TASKSTRUCT *)(LPVOID)&rhs;

	m_lID				= rhs.m_lID;
	m_lLineID			= rhs.m_lLineID;
	m_lBoxID			= rhs.m_lBoxID;
	m_strName			= rhs.m_strName;
	m_strDesc			= rhs.m_strDesc;
	m_strDownload		= rhs.m_strDownload;
	m_lTransformation	= rhs.m_lTransformation;
	
	Copy (m_vMsgs, pRhs->m_vMsgs);
}

CTask::CTask(const CTask & rhs)
:	TASKSTRUCT (rhs)
{
	TASKSTRUCT * pRhs = (TASKSTRUCT *)(LPVOID)&rhs;

	m_lID				= rhs.m_lID;
	m_lLineID			= rhs.m_lLineID;
	m_lBoxID			= rhs.m_lBoxID;
	m_strName			= rhs.m_strName;
	m_strDesc			= rhs.m_strDesc;
	m_strDownload		= rhs.m_strDownload;
	m_lTransformation	= rhs.m_lTransformation;
	
	Copy (m_vMsgs, pRhs->m_vMsgs);
}

CTask & CTask::operator = (const CTask & rhs)
{
	if (this != &rhs) {
		CTask * pRhs = (CTask *)(LPVOID)&rhs;

		m_lID				= rhs.m_lID;
		m_lLineID			= rhs.m_lLineID;
		m_lBoxID			= rhs.m_lBoxID;
		m_strName			= rhs.m_strName;
		m_strDesc			= rhs.m_strDesc;
		m_strDownload		= rhs.m_strDownload;
		m_lTransformation	= rhs.m_lTransformation;
		
		Copy (m_vMsgs,		pRhs->m_vMsgs);
	}

	return * this;
}

CTask::~CTask()
{
	Free ();
}

void CTask::Free ()
{
	for (int i = 0; i < m_vLists.GetSize (); i++) {
		if (CMessage * p = m_vLists [i])
			delete p;
	}

	m_vLists.RemoveAll ();
}

FoxjetDatabase::TASKSTRUCT CTask::GetTaskStruct () const
{
	TASKSTRUCT t;
	CTask * pThis = (CTask *)(LPVOID)this;

	t.m_lID				= m_lID;
	t.m_lLineID			= m_lLineID;
	t.m_lBoxID			= m_lBoxID;
	t.m_strName			= m_strName;
	t.m_strDesc			= m_strDesc;
	t.m_strDownload		= m_strDownload;
	t.m_lTransformation	= m_lTransformation;
	t.m_strNotes		= m_strNotes;

	Copy (t.m_vMsgs, pThis->m_vMsgs);

	return t;
}

bool CTask::Create (const Foxjet3d::Box::CBox & box, const FoxjetDatabase::LINESTRUCT & line, FoxjetDatabase::COdbcDatabase & db)
{
	Free ();
	
	for (int nPanel = 1; nPanel <= 6; nPanel++) {
		const CPanel * pPanel = NULL;

		VERIFY (box.GetPanel (nPanel, &pPanel));
		ASSERT (pPanel);

		if (pPanel) {
			for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) {
				const CBoundary & b = const_cast <CBoundaryArray &> (pPanel->m_vBounds) [i];
				MESSAGESTRUCT msg;

				msg.m_lHeadID = b.m_card.m_lID;
				msg.m_lTaskID = m_lID;
				msg.m_strName = m_strName;

				CMessage * pMsg = new CMessage (); 

				pMsg->Assign (msg, b);

				//CString str; str.Format ("%s (%d, %d)", pMsg->m_list.GetHead ().m_strName, pMsg->m_list.GetHead ().m_vValve.GetSize (), pMsg->m_list.m_bounds.m_card.m_vValve.GetSize ()); TRACEF (str);

				AddMessage (pMsg);
			}
		}
	}

	return true;
}

int CTask::Find (const CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vHeads, ULONG lHeadID)
{
	for (int i = 0; i < vHeads.GetSize (); i++) {
		const HEADSTRUCT & head = vHeads [i];

		if (head.m_lID == lHeadID) 
			return i;
	}

	return -1;
}

int CTask::Find (const CArray <FoxjetDatabase::MESSAGESTRUCT, FoxjetDatabase::MESSAGESTRUCT &> & vMsgs, ULONG lHeadID)
{
	for (int i = 0; i < vMsgs.GetSize (); i++) 
		if (vMsgs [i].m_lHeadID == lHeadID) 
			return i;

	return -1;
}

int CTask::Find (const CArray <FoxjetDatabase::TASKSTRUCT, FoxjetDatabase::TASKSTRUCT &> & v, ULONG lID)
{
	for (int i = 0; i < v.GetSize (); i++) 
		if (v [i].m_lID == lID) 
			return i;

	return -1;
}

int CTask::Find (const CArray <FoxjetDatabase::BOXSTRUCT, FoxjetDatabase::BOXSTRUCT &> & v, ULONG lID)
{
	for (int i = 0; i < v.GetSize (); i++) 
		if (v [i].m_lID == lID) 
			return i;

	return -1;
}

int CTask::Find (const CArray <FoxjetDatabase::VALVESTRUCT, FoxjetDatabase::VALVESTRUCT &> & vHeads, ULONG lFindID)
{
	for (int i = 0; i < vHeads.GetSize (); i++)
		if (vHeads [i].m_lID == lFindID)
			return i;

	return -1;
}

int CTask::Find (const CArray <FoxjetDatabase::PANELSTRUCT, FoxjetDatabase::PANELSTRUCT &> & v, ULONG lFindID)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i].m_lID == lFindID)
			return i;

	return -1;
}

int CTask::Find (const Panel::CBoundaryArray & vHeads, ULONG lHeadID)
{
	for (int i = 0; i < vHeads.GetSize (); i++)
		if (vHeads [i].GetID () == lHeadID)
			return i;

	return -1;
}

int CTask::FindByIndex (const CArray <FoxjetDatabase::VALVESTRUCT, FoxjetDatabase::VALVESTRUCT &> & vHeads, int nFind)
{
	if (nFind > 0) {
		CArray <VALVESTRUCT, VALVESTRUCT &> & v = const_cast <CArray <VALVESTRUCT, VALVESTRUCT &> &> (vHeads);

		for (int i = 0; i < v.GetSize (); i++)
			if (v [i].m_nIndex == nFind)
				return i;
	}

	return -1;
}

void CTask::Add (CElement * pElement, CMessage & msg, const FoxjetDatabase::HEADSTRUCT & head, CDC & dcMem)
{
	CBaseElement * p = pElement->GetElement ();
	CPoint pt = CCoord::CoordToPoint (
		(double)p->GetPos (&head).x / 1000.0, 
		(double)p->GetPos (&head).y / 1000.0, 
		INCHES, head); 

	#ifdef __IPPRINTER__
	// must make a new temp copy to image (ip ver must be imaged to set pos).
	// ip bitmap is a special case, needs memstore created - can't just 
	// image the old one because it could contain the wrong bitmap.
	if (p->GetClassID () == BMP) {
		CBaseElement * pTmp = CopyElement (p, &head, false);

		pTmp->Build ();
		pTmp->Draw (dcMem, head, true);
		pt = CCoord::CoordToPoint (
			(double)pTmp->GetPos (&head).x / 1000.0, 
			(double)pTmp->GetPos (&head).y / 1000.0, 
			INCHES, head); 

		delete pTmp;
	}
	#endif //__IPPRINTER__

	if (msg.m_list.FindID (pElement->GetElement ()->GetID ()) != -1)
		pElement->GetElement ()->SetID (msg.m_list.GetNextID ());

	#if defined( _DEBUG ) && defined( __IPPRINTER__ )
	CPoint ptOld = p->GetPos (&head); 
	#endif 

	if (!IsValveHead (head))
		pElement->SetPos (pt, head);
	
	/*
	#if defined( _DEBUG ) && defined( __IPPRINTER__ )
	CPoint ptNew = p->GetPos (&head); 

	//if (ptOld != ptNew) {
	if (ptOld.y != ptNew.y && GetHeadChannels (head.m_nHeadType) == 32) {
		CPoint ptDiff (abs (ptOld.x - ptNew.x), abs (ptOld.y - ptNew.y));
		CString strDebug;

		strDebug.Format ("(%d, %d) --> (%d, %d), diff: (%d, %d): %s",
			ptOld.x, ptOld.y,
			ptNew.x, ptNew.y,
			ptDiff.x, ptDiff.y,
			str);
		TRACEF (strDebug);
		ASSERT (0);
	}
	#endif
	*/

	pElement->GetElement ()->Build ();

	#ifdef __WINPRINTER__
	if (p->GetClassID () == BARCODE) 
		p->AdjustPos (head, ALIGNMENT_ADJUST_SUBTRACT);
	#endif

	msg.m_list.Add (pElement);
}

void CTask::Validate (CElement * pElement, const FoxjetDatabase::HEADSTRUCT & head, const CString & str,
					  const CSize & size, const FoxjetCommon::CVersion & ver, const FoxjetCommon::CVersion & verActual, 
					  CArray <InvalidElementsDlg::CItem, InvalidElementsDlg::CItem &> * pvErrors)
{
	using namespace InvalidElementsDlg;

	CBaseElement * p = pElement->GetElement ();
	CString strBuilt = p->ToString (ver);
	CString strParsed = _T ("{") + str + _T ("}");
	CLongArray vnParsed = CountChars (strParsed, ',');
	CLongArray vnBuilt = CountChars (strBuilt, ',');
	int nNameIndex = p->GetFieldIndex (ElementFields::m_lpszFontName);
	int nSizeIndex = p->GetFieldIndex (ElementFields::m_lpszFontSize);
	CStringArray vstrParsed, vstrBuilt;

	ASSERT (pvErrors);

	Tokenize (strParsed, vstrParsed);
	Tokenize (strBuilt, vstrBuilt);
							
	// ignore font name
	if (nNameIndex != -1 && 
		nNameIndex < vstrBuilt.GetSize () && 
		nNameIndex < vstrParsed.GetSize ()) 
	{
		vstrBuilt [nNameIndex] = vstrParsed [nNameIndex];
	}

	// ignore font size
	if (nSizeIndex != -1 && 
		nSizeIndex < vstrBuilt.GetSize () && 
		nSizeIndex < vstrParsed.GetSize ()) 
	{
		vstrBuilt [nSizeIndex] = vstrParsed [nSizeIndex];
	}

	// ignore the first 4 params (potential rounding errors possible in position).
	// since they are not optional, the call to FromString (above) 
	// will have already validated them.

	if (vnParsed.GetSize () > 4)  
		strParsed = strParsed.Mid (vnBuilt [3] + 1);

	strBuilt.Empty ();

	for (int i = 4; i < vstrBuilt.GetSize (); i++) {
		strBuilt += vstrBuilt [i];
		
		if ((i + 1) == vstrBuilt.GetSize ())
			strBuilt += _T ("}"); 
		else
			strBuilt += _T (","); 
	}

	if (strBuilt != strParsed && verActual == ver) {
		#ifdef _DEBUG
		CString strDebug;

		strDebug.Format (_T ("strBuilt != strParsed\n\tParsed: \"%s\"\n\tBuilt:  \"%s\""), strParsed, strBuilt);
		TRACEF (strDebug);
		#endif //_DEBUG

		pvErrors->Add (CItem (head.m_strName, p->GetID (), p->GetClassID (), str, LoadString (IDS_INVALIDPARAMS)));
	}
	else {
		if (!size.cx || !size.cy) {
			pvErrors->Add (CItem (head.m_strName, p->GetID (), p->GetClassID (), str, LoadString (IDS_ELEMENTHASINVALIDDATA)));
		}
	}
}

int CTask::GetValveIndex (const CElement & e, const FoxjetDatabase::HEADSTRUCT & h)
{
#ifdef __IPPRINTER__
	return -1;
#else

	HEADSTRUCT & head	= const_cast <HEADSTRUCT &> (h);
	long lTotalInches	= 0;
	const CPoint pt		= e.GetElement ()->GetPos (NULL);
	CPoint ptLogical	= CBaseElement::ThousandthsToLogical (pt, h, const_cast <FoxjetDatabase::CValveArray &> (h.m_vValve));

	ASSERT (head.m_vValve.GetSize ());

	{
		VALVESTRUCT & v = head.m_vValve [0];
		
		if (v.m_lID == -1 && v.m_lCardID == -1 && v.m_lPanelID == -1) 
			return -1; // the head this element resides on must have been deleted from config
	}

	for (int i = 0; i < head.m_vValve.GetSize (); i++) {
		VALVESTRUCT & valve = head.m_vValve [i];
		long lInches = GetValveHeight (valve.m_type);

		if (pt.y >= lTotalInches && pt.y < (lTotalInches + lInches)) 
			return valve.m_nIndex;

		lTotalInches += lInches;
	}

	// if not found, default to last index
	return head.m_vValve [head.m_vValve.GetSize () - 1].m_nIndex;
#endif
}

CMessage * CTask::FindMessage (ULONG lHeadID, int nValveIndex)
{
	for (int i = 0; i < m_vLists.GetSize (); i++) {
		CMessage * pMsg = m_vLists [i];

		ASSERT (pMsg);

		if (pMsg->m_list.GetHead ().m_lID == lHeadID && nValveIndex == -1)
			return pMsg;

		HEADSTRUCT & h = const_cast <HEADSTRUCT &> (pMsg->m_list.GetHead ());

		if (h.m_lID == lHeadID) {
			for (int nValve = 0; nValve < h.m_vValve.GetSize (); nValve++) {
				if (h.m_vValve [nValve].m_nIndex == nValveIndex)
					return pMsg;
			}
		}
	}

	ASSERT (m_vLists.GetSize ());

	return m_vLists [0];
}

ULONG CTask::GetIndexHeight (int nIndex, const FoxjetDatabase::HEADSTRUCT & head)
{
	HEADSTRUCT & h = const_cast <HEADSTRUCT &> (head);
	ULONG lResult = 0;

	for (int i = 0; i < h.m_vValve.GetSize (); i++) {
		const VALVESTRUCT & valve = h.m_vValve [i];

		if (valve.m_nIndex == nIndex)
			break;

		lResult += GetValveHeight (valve.m_type);
	}

	return lResult;
}

void CTask::AssignElements (CMessage & msg, CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vHeads,
							FoxjetDatabase::COdbcDatabase & db)
{
	ULONG lCardID = msg.m_list.GetHead ().m_lID;
	int nHeadIndex = Find (vHeads, lCardID);
	bool bValve = ISVALVE ();

	ASSERT (nHeadIndex != -1);

	HEADSTRUCT & headPrinter = vHeads [nHeadIndex];

	ASSERT (headPrinter.m_lID == lCardID);

	if (CMessage * pMsg = FindMessage (headPrinter.m_lID, -1))
		pMsg->m_list.m_strLocalParams = msg.m_list.m_strLocalParams;

	while (msg.m_list.GetSize ()) {
		CElement & e = msg.m_list.GetElement (0);
		const CPoint ptOld = e.GetElement ()->GetPos (NULL);
		const CCoord coord (CBaseElement::ThousandthsToLogical (ptOld, headPrinter), INCHES, headPrinter); 
		CPoint ptNew ((int)(coord.m_x * 1000.0), (int)(coord.m_y * 1000.0));
		CMessage * pMsg = NULL;

		msg.m_list.RemoveAt (0);

		if (!bValve) {
			VERIFY (pMsg = FindMessage (headPrinter.m_lID, -1));
		}
		else {
			int nIndex = GetValveIndex (e, headPrinter);

			if (nIndex == -1) { 
				TRACEF ("Head not found: " + e.GetElement ()->ToString (CVersion ()));
				delete &e;
				ASSERT (0);
				continue;
			}

			VERIFY (pMsg = FindMessage (headPrinter.m_lID, nIndex));

			const HEADSTRUCT & headEditor = pMsg->m_list.GetHead ();

{ CString str; str.Format (_T ("%s (%d, %d)"), e.GetElement ()->GetImageData (), e.GetElement ()->GetPos (NULL).x, e.GetElement ()->GetPos (NULL).y); TRACEF (str); }

			if (headPrinter.m_vValve.GetSize () && headEditor.m_vValve.GetSize ()) {
				const VALVESTRUCT & valve = headEditor.m_vValve [0];
				ULONG lOffset = 0;

				{ // determine if this head has more than one CBoundary object, and if so, what the offset is
					int n;

					for (n = nIndex; FindByIndex (headEditor.m_vValve, n) != -1; n--);
					
					if (n > 0) {
						//lOffset = GetIndexHeight (nIndex, headPrinter);

						CArray <VALVESTRUCT, VALVESTRUCT &> v;
						int nFind = FindByIndex (headPrinter.m_vValve, nIndex);
						ASSERT (nFind != -1);

						if (nFind >= 0 && nFind < headPrinter.m_vValve.GetSize ()) {
							VALVESTRUCT & valve = headPrinter.m_vValve [nFind];

							CValvePropDlg::GetList (headPrinter, valve, v);
							ASSERT (v.GetSize ());

							if (v.GetSize ())
								lOffset = GetIndexHeight (v [0].m_nIndex, headPrinter);
						}
					}
				}

				if (lOffset) {
					CPoint pt = e.GetElement ()->GetPos (NULL);
			
					if (lOffset <= (coord.m_y * 1000.0)) {
						pt.y -= lOffset;
						ASSERT (pt.y >= 0);
						{ CString str; str.Format (_T ("%s (%d, %d)"), e.GetElement ()->GetImageData (), e.GetElement ()->GetPos (NULL).x, e.GetElement ()->GetPos (NULL).y); TRACEF (str); }
						e.GetElement ()->SetPos (pt, NULL);
						{ CString str; str.Format (_T ("%s (%d, %d)"), e.GetElement ()->GetImageData (), e.GetElement ()->GetPos (NULL).x, e.GetElement ()->GetPos (NULL).y); TRACEF (str); }
					}
				}
			}
		}

		ASSERT (pMsg);
		pMsg->m_list.Add (&e);

		#ifdef __WINPRINTER__
		{ // build linked dependencies
			using namespace TextElement;

			for (int i = 0; i < pMsg->m_list.GetSize (); i++) {
				if (CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, &pMsg->m_list.GetObject (i))) {
					if (p->GetLink ()) 
						p->Build (pMsg->m_list);
				}
			}
		}
		#endif //__WINPRINTER__
	}
}

bool CTask::Load (Foxjet3d::Box::CBox & box, const CString & strFile, 
				  FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver,
				  CArray <InvalidElementsDlg::CItem, InvalidElementsDlg::CItem &> * pvErrors,
				  LPPROGRESSFCT lpProgress, bool * pbModifiedAtLoad)
{
	CString strLine = FoxjetFile::GetFileHead (strFile);
	CString strTask = FoxjetFile::GetFileTitle (strFile);
	TASKSTRUCT task;

	if (GetTaskRecord (db, strTask, strLine, GetPrinterID (db), task)) 
		return Load (box, task, db, ver, pvErrors, lpProgress, pbModifiedAtLoad) > 0 ? true : false;

	return false;
}

int CTask::RemoveDuplicates (CStringArray & vSrc, const FoxjetDatabase::HEADSTRUCT & head)
{
	int nResult = 0;
	CStringArray v;
	
	v.Copy (vSrc);

	for (int i = 0; i < v.GetSize (); i++) {
		CString str;
		CStringArray vTmp;

		Tokenize (v [i], vTmp);

		if (GetElementType (vTmp [0]) != -1 && vTmp.GetSize () > 4) {
			str = vTmp [0] + _T (",");

			CPoint pt (_ttoi (vTmp [2]), _ttoi (vTmp [3]));
			pt = CBaseElement::ThousandthsToLogical (pt, head);
			pt = CBaseElement::LogicalToThousandths (pt, head);

			str += FoxjetDatabase::ToString (pt.x) + _T (",");
			str += FoxjetDatabase::ToString (pt.y) + _T (",");

			for (int j = 4; j < vTmp.GetSize (); j++) {
				str += vTmp [j];

				if ((j + 1) < vTmp.GetSize ())
					str +=  _T (",");
			}

			v [i] = str;
		}
	}


	for (int k  = 0; k < v.GetSize (); k++) {
		for (int i = 0; i < v.GetSize (); i++) {
			CString str = v [i];

			for (int j = 0; j < v.GetSize (); j++) {
				if (i != j && str == v [j]) {
					TRACEF (_T ("Duplicate: ") + v [j]);
					v.RemoveAt (j);
					vSrc.RemoveAt (j);
					nResult++;
				}
			}
		}
	}

	return nResult;
}

int CTask::Load (Foxjet3d::Box::CBox & box, 
				  FoxjetDatabase::TASKSTRUCT & task, FoxjetDatabase::COdbcDatabase & db, 
				  const FoxjetCommon::CVersion & ver, 
				  CArray <InvalidElementsDlg::CItem, InvalidElementsDlg::CItem &> * pvErrors,
				  LPPROGRESSFCT lpProgress,
				  bool * pbModifiedAtLoad)
{
	int nResult = 1;
	using namespace InvalidElementsDlg;

	int nProgressMax = 0;
	const CString strLoading = LoadString (IDS_LOADING);
	CString strProgress;
	CDC dcMem;
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
	LINESTRUCT line;

	dcMem.CreateCompatibleDC (::AfxGetMainWnd ()->GetDC ());	

	GetLineRecord (task.m_lLineID, line);
	GetLineHeads (task.m_lLineID, vHeads);
//	box.LoadPanels (db, task.m_lLineID);

	CStringArray * pvstr = new CStringArray [max (task.m_vMsgs.GetSize (), 1)];

	for (int nCard = 0 ; nCard < task.m_vMsgs.GetSize (); nCard++) { 
		ParsePackets (task.m_vMsgs [nCard].m_strData, pvstr [nCard]);
		int nCount = pvstr [nCard].GetSize ();
		nProgressMax += nCount;
		int nHeadIndex = Find (vHeads, task.m_vMsgs [nCard].m_lHeadID);

		if (nHeadIndex != -1) {
			HEADSTRUCT head = vHeads [nHeadIndex];
			int nDuplicates = RemoveDuplicates (pvstr [nCard], head);
			
			if (nDuplicates) {
				nResult = 2;

				TRACEF (_T ("removed ") + FoxjetDatabase::ToString (nDuplicates) + _T (" duplicates [") + FoxjetDatabase::ToString (nCount) + _T ("]"));
				
				for (int i = 0; i < pvstr [nCard].GetSize (); i++)
					TRACEF (FoxjetDatabase::ToString (head.m_lID) + _T (": ") + pvstr [nCard].GetAt (i));

				ASSERT ((nDuplicates = RemoveDuplicates (pvstr [nCard], head)) == 0);
			}
		}
	}

	if (lpProgress) {
		strProgress.Format (_T ("%s '%s' ..."),
			strLoading,
			task.m_strName);

		TRACEF (strProgress);
		(* lpProgress) (PROGRESS_SET, strLoading + _T (" ") + task.m_strName + _T (" ..."), max (nProgressMax, 1));
	}

	VERIFY (Create (box, line, db));

	for (int nCard = 0; nCard < task.m_vMsgs.GetSize (); nCard++) { 
		CVersion verActual (ver);

		if (CMessage * p = GetMessage (nCard)) {
			bool bNEXT = task.m_vMsgs [nCard].m_strData.Find (_T ("{Label,")) != -1 ? true : false;

			p->m_list.SetNEXT (bNEXT); 
		}

		task.m_vMsgs [nCard].m_strData.Empty (); // don't want to build with FromString yet 
		CMessage msg (task.m_vMsgs [nCard]);

		if (msg.m_list.SetHead (task.m_vMsgs [nCard].m_lHeadID)) {
			msg.m_list.SetAutoDelete (false);

			for (int nString = 0; nString < pvstr [nCard].GetSize (); nString++) {
				CStringArray vToken;
				CString str = pvstr [nCard][nString];
				HEADSTRUCT head;

				int nHeadIndex = Find (vHeads, task.m_vMsgs [nCard].m_lHeadID);

				if (nHeadIndex != -1)
					head = vHeads [nHeadIndex];

				Tokenize (str, vToken);

				if (lpProgress) {
					strProgress.Format (_T ("%s '%s', Card '%s' [parsing packet %d of %d] ..."),
						strLoading,
						task.m_strName,
						head.m_strName,
						nString, pvstr [nCard].GetSize ());

					(* lpProgress) (PROGRESS_STEP, strProgress, 0);
				}

				if (vToken.GetSize ()) {
					CString strPrefix = vToken [0];
					int nType = GetElementType (strPrefix);

					if (nType != -1) {
						const HEADSTRUCT & h = msg.m_list.GetHead ();
						const CBaseElement & def = defElements.GetElement (nType);
						CBaseElement * p = CopyElement (&def, &h, true);

						bool bSnap = p->GetSnap ();
						p->SetSnap (false);
						bool bFromString = p->FromString (str, verActual);
						p->SetSnap (bSnap);

						p->Build ();
						p->ClipTo (h);
						CSize size = p->Draw (dcMem, h);

						CElement * pElement = new CElement (p);

						if (!bFromString) {
							if (pvErrors)
								pvErrors->Add (CItem (h.m_strName, -1, -1, str, LoadString (IDS_FAILEDTOPARSE)));
						}
						else {
							if (pvErrors)
								Validate (pElement, h, str, size, ver, verActual, pvErrors);
						}

						Add (pElement, msg, head, dcMem);
					}
					else {
						if (strPrefix == _T ("BarcodeParam")) {
							CString strParam = _T ("{") + str + _T ("}");

							if (msg.m_list.m_strLocalParams.Find (strParam) == -1) {
								TRACEF (strParam);
								msg.m_list.m_strLocalParams += strParam;
							}
						}
						else {
							CVersion verTmp;

							if (verTmp.FromString (str))
								verActual = verTmp;
							else {
								CString strUserDefined = _T ("{") + str + _T ("}"); 

								msg.m_list.SetUserDefined (strUserDefined);
							}
						}
					}
				}
			}

			AssignElements (msg, vHeads, db);
		}
	}

	for (int nPanel = 1; nPanel <= 6; nPanel++) {
		const CPanel * pPanel = NULL;

		VERIFY (box.GetPanel (nPanel, &pPanel));
		ASSERT (pPanel);

		if (pPanel) {
			for (int i = 0; i < pPanel->m_vBounds.GetSize (); i++) {
				const CBoundary & b = const_cast <CBoundaryArray &> (pPanel->m_vBounds) [i];

				if (!box.IsLinked (b)) {
					CBoundary link;

					if (box.GetLinkedSlave (b, link)) {
						CMessage * pMaster = GetMessageByHead (b.GetID ());
						CMessage * pSlave = GetMessageByHead (link.GetID ());

						if (pMaster && pSlave) {
							CString str = pMaster->m_list.ToString (CVersion (verApp), pMaster->m_list.GetProductArea (0));

							if (pbModifiedAtLoad) {
								CString strMaster;
								CString strSlave;

								for (int i = 0; i < pMaster->m_list.GetSize (); i++)
									strMaster += pMaster->m_list.GetElement (i).GetElement ()->ToString (CVersion (verApp));
							
								for (int i = 0; i < pSlave->m_list.GetSize (); i++)
									strSlave += pSlave->m_list.GetElement (i).GetElement ()->ToString (CVersion (verApp));

								if (strMaster != strSlave) {
									* pbModifiedAtLoad = true;
									TRACEF (strMaster);
									TRACEF (strSlave);
								}
							}

							pSlave->m_list.DeleteAllElements ();
							pSlave->m_list.FromString (str, pMaster->m_list.GetProductArea (0), pMaster->m_list.GetUnits (), 
								pMaster->m_list.m_bounds, CVersion (verApp), true);
						}
					}
				}
			}
		}
	}

	delete [] pvstr;

	if (lpProgress)
		(* lpProgress) (PROGRESS_END, _T (""), 0);

	return nResult;
}

bool CTask::Save (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	TRACEF ("CTask::Save: " + ver.ToString ());

	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

	Update ();

	TASKSTRUCT task = * this;
	GetLineHeads (task.m_lLineID, vHeads);

	//#ifdef _DEBUG
	//{
	//	CString str;

	//	str.Format (_T ("task.m_lID: %d"), task.m_lID);
	//	TRACEF (str);

	//	for (int i = 0; i < task.m_vMsgs.GetSize (); i++) {
	//		str.Format (_T ("task.m_vMsgs [%d].m_lID: %d, m_lTaskID: %d"), i, task.m_vMsgs [i].m_lID, task.m_vMsgs [i].m_lTaskID);
	//		TRACEF (str);
	//	}
	//}
	//#endif

	for (int i = 0; i < m_vLists.GetSize (); i++) {
		CMessage * pMsg = m_vLists [i];
		
		ASSERT (pMsg);

		HEADSTRUCT & h = const_cast <HEADSTRUCT &> (pMsg->m_list.m_bounds.m_card);
		MESSAGESTRUCT msg;

		CElementList tmpList (pMsg->m_list);
	
		tmpList.DeleteAllElements ();

		msg.m_lHeadID = h.m_lID;
		msg.m_lTaskID = task.m_lID;
		msg.m_strName = task.m_strName;
		msg.m_lHeight = 0;
		msg.m_strData = tmpList.ToString (verApp);

		int nMsgIndex = Find (task.m_vMsgs, h.m_lID);

		if (nMsgIndex != -1) {
			msg.m_lID = task.m_vMsgs [nMsgIndex].m_lID;
			task.m_vMsgs [nMsgIndex] = msg;
		}
		else
			task.m_vMsgs.Add (msg);
			
	}

	for (int i = 0; i < m_vLists.GetSize (); i++) {
		CMessage * pMsg = m_vLists [i];
		HEADSTRUCT & h = const_cast <HEADSTRUCT &> (pMsg->m_list.m_bounds.m_card);
		int nMsgIndex = Find (task.m_vMsgs, h.m_lID);

		ASSERT (nMsgIndex != -1);

		MESSAGESTRUCT & msg = task.m_vMsgs [nMsgIndex];

		for (int nElement = 0; nElement < pMsg->m_list.GetSize (); nElement++) {
			CBaseElement * pElement = pMsg->m_list.GetElement (nElement).GetElement ();
			const bool bSnap = pElement->GetSnap ();

			#ifdef __WINPRINTER__
			const CPoint ptOld = pElement->GetPos (NULL);

			pElement->SetSnap (false);
			pElement->AdjustPos (h, FoxjetCommon::ALIGNMENT_ADJUST_ADD);

			if (h.m_vValve.GetSize ()) {
				int nHeadIndex = Find (vHeads, h.m_lID);
				ASSERT (nHeadIndex != -1);
				const HEADSTRUCT & headPrinter = vHeads [nHeadIndex];


				if (ULONG lOffset = GetIndexHeight (h.m_vValve [0].m_nIndex, headPrinter)) {
					CPoint pt = pElement->GetPos (NULL);

					pt.y += lOffset;

					//{ CString str; str.Format ("%s (%d, %d)", pElement->GetImageData (), pElement->GetPos (NULL).x, pElement->GetPos (NULL).y); TRACEF (str); }
					pElement->SetPos (pt, NULL);
					//{ CString str; str.Format ("%s (%d, %d)", pElement->GetImageData (), pElement->GetPos (NULL).x, pElement->GetPos (NULL).y); TRACEF (str); }
				}
			}
			#endif //__WINPRINTER__

			msg.m_strData += pElement->ToString (ver);

			#ifdef __WINPRINTER__
			pElement->SetPos (ptOld, NULL, FoxjetCommon::ALIGNMENT_ADJUST_ADD);
			#endif //__WINPRINTER__

			pElement->SetSnap (bSnap);
		}
	}

	ULONG lMasterID = GetMasterPrinterID (db);
	ULONG lPrinterID = GetPrinterID (db);
	CString strFile;

	//#ifdef _DEBUG
	//{
	//	CString str;

	//	str.Format (_T ("task.m_lID: %d"), task.m_lID);
	//	TRACEF (str);

	//	for (int i = 0; i < task.m_vMsgs.GetSize (); i++) {
	//		str.Format (_T ("task.m_vMsgs [%d].m_lID: %d, m_lTaskID: %d"), i, task.m_vMsgs [i].m_lID, task.m_vMsgs [i].m_lTaskID);
	//		TRACEF (str);
	//	}
	//}
	//#endif

	Unmap (db, task, lPrinterID, lMasterID); // sw0867

	//#ifdef _DEBUG
	//{
	//	CString str;

	//	str.Format (_T ("task.m_lID: %d"), task.m_lID);
	//	TRACEF (str);

	//	for (int i = 0; i < task.m_vMsgs.GetSize (); i++) {
	//		str.Format (_T ("task.m_vMsgs [%d].m_lID: %d, m_lTaskID: %d"), i, task.m_vMsgs [i].m_lID, task.m_vMsgs [i].m_lTaskID);
	//		TRACEF (str);
	//	}
	//}
	//#endif

	bool bResult = FoxjetDatabase::UpdateTaskRecord (db, task);

	try {
		if (!bResult) 
			bResult = FoxjetDatabase::AddTaskRecord (db, task);
	}
	catch (COdbcException * e) { 
		MsgBox (e->m_strError, MB_ICONWARNING);
		HANDLEEXCEPTION_TRACEONLY (e); 
	}

	{
		for (int i = 0; i < task.m_vMsgs.GetSize (); i++) {
			MESSAGESTRUCT & m = task.m_vMsgs [i];
			MESSAGESTRUCT tmp;

			if (GetMessageRecord (db, m.m_strName, m.m_lHeadID, tmp)) {
				task.m_vMsgs [i].m_lID = tmp.m_lID;
			}

			CString str;

			str.Format (_T ("task.m_vMsgs [%d].m_lID: %d"), i, task.m_vMsgs [i].m_lID);
			TRACEF (str);
		}

		* this = task;
	}

	return bResult;
}

////////////////////////////////////////////////////////////////////////
//
const CMessage * CTask::GetMessage (int nIndex) const
{
	if (nIndex >= 0 && nIndex < m_vLists.GetSize ()) {
		ASSERT (m_vLists [nIndex]);
		return m_vLists [nIndex];
	}

	return NULL;
}

CMessage * CTask::GetMessage (int nIndex)
{
	if (nIndex >= 0 && nIndex < m_vLists.GetSize ()) {
		ASSERT (m_vLists [nIndex]);
		return m_vLists [nIndex];
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////
//
const FoxjetDatabase::MESSAGESTRUCT * CTask::GetMessageStruct (int nIndex) const
{
	if (nIndex >= 0 && nIndex < m_vLists.GetSize ()) 
		return m_vLists [nIndex];

	return NULL;
}

FoxjetDatabase::MESSAGESTRUCT * CTask::GetMessageStruct (int nIndex)
{
	if (nIndex >= 0 && nIndex < m_vLists.GetSize ()) 
		return m_vLists [nIndex];

	return NULL;
}

////////////////////////////////////////////////////////////////////////
//

const CMessage * CTask::GetMessageByHead (ULONG lHeadID) const
{
	ULONG lMax = 0;

	for (int i = 0; i < m_vLists.GetSize (); i++) {
		ULONG lID = m_vLists [i]->m_list.m_bounds.GetID ();

		lID = max (lID, lMax);

		if (lID == lHeadID) 
			return m_vLists [i];
	}

	ASSERT (lHeadID < lMax); // probably passed in HEADSTRUCT::m_lID instead of CBoundary::GetID

	return NULL;
}

CMessage * CTask::GetMessageByHead (ULONG lHeadID) 
{
	ULONG lMax = 0;

	for (int i = 0; i < m_vLists.GetSize (); i++) {
		ULONG lID = m_vLists [i]->m_list.m_bounds.GetID ();

		lID = max (lID, lMax);

		if (lID == lHeadID) 
			return m_vLists [i];
	}

	ASSERT (lHeadID < lMax); // probably passed in HEADSTRUCT::m_lID instead of CBoundary::GetID

	return NULL;
}

////////////////////////////////////////////////////////////////////////
//

void CTask::Update()
{
	for (int i = 0; i < m_vMsgs.GetSize (); i++) {
		MESSAGESTRUCT & msg = m_vMsgs [i];

		for (int j = 0; j < m_vLists.GetSize (); j++) {
			CMessage & list = * m_vLists [j];
			const HEADSTRUCT & head = list.m_list.GetHead ();
			const CVersion & v = list.m_list.GetVersion ();

			ASSERT (v.IsSynonym (GetElementListVersion ()));

			if (head.m_lID == msg.m_lHeadID) {
				msg.m_strName = m_strName;
				msg.m_strData = list.m_list.CElementList::ToString (v, false, m_strName);
				list.m_strData = msg.m_strData;
				msg.m_lTaskID = m_lID;
				msg.m_strData = Convert (msg.m_strData, v);
			}
		}
	}
}

bool CTask::SetVersion (const FoxjetCommon::CVersion & ver)
{
	for (int j = 0; j < m_vLists.GetSize (); j++) {
		CMessage & list = * m_vLists [j];

		list.m_list.SetVersion (ver);
	}

	return true;
}

CString CTask::ToString (const CVersion & ver) const
{
	CString str;

	str.Format (_T ("{Task,%d,%d,%d,%s,%s,%s,%d,%s}"),
		m_lID,
		m_lLineID, 
		m_lBoxID, 
		m_strName,
		m_strDesc,
		m_strDownload,
		m_lTransformation,
		m_strNotes); 

	return str;
}

bool CTask::FromString (const CString & str, const CVersion & ver)
{
	const int nFields = 9;
	CStringArray vActual, vDef;
	CString strToken [nFields] = {
		_T (""),			// Prefix (required)
		_T (""),			// ID
		_T (""),			// LineID
		_T (""),			// BoxID
		_T (""),			// Name
		_T (""),			// Desc
		_T (""),			// Download
		_T (""),			// Transform
		_T (""),			// Notes
	};
	int i;

	Tokenize (str, vActual);

	if (vActual.GetSize () < 1) {
		TRACEF (_T ("Too few tokens"));
		return false;
	}

	Tokenize (ToString (ver), vDef);

	for (i = 1; i < min (vDef.GetSize (), nFields); i++)
		if (vDef [i].GetLength ())
			strToken [i] = vDef [i];

	for (i = 0; i < min (vActual.GetSize (), nFields); i++)
		if (vActual [i].GetLength ())
			strToken [i] = vActual [i];

	if (strToken [0].CompareNoCase (_T ("Task")) != 0){
		TRACEF (_T ("Wrong prefix \"") + strToken [0] + _T ("\""));
		return false;
	}

	m_lID				= _tcstol (strToken [1], NULL, 10);
//	m_lLineID			= _tcstol (strToken [2], NULL, 10);		// never changes
	m_lBoxID			= _tcstol (strToken [3], NULL, 10);
//	m_strName			= strToken [4];							// never changes
	m_strDesc			= strToken [5];
	m_strDownload		= strToken [6];
	m_lTransformation	= _tcstol (strToken [7], NULL, 10);
	m_strNotes			= strToken [8];

	return true;
}

bool CTask::UpdateUserDefined  (const FoxjetDatabase::MESSAGESTRUCT & msg) 
{
	for (int i = 0; i < m_vLists.GetSize (); i++) {
		CMessage * pMsg = m_vLists [i];

		if (pMsg->m_list.m_bounds.m_card.m_lID == msg.m_lHeadID) {
			return pMsg->m_list.SetUserDefined (msg.m_strData);
		}
	}

	return false;
}

bool CTask::AddMessage (CMessage * pMsg)
{
	m_vLists.Add (pMsg);
	return true;
}
