#ifndef __BOXFORWARDDECLARE__
#define __BOXFORWARDDECLARE__

#include "3dApi.h"

namespace Foxjet3d
{
	class _3D_API CTask;

	namespace Box
	{
		class _3D_API CBoxParams;
		class _3D_API CHit;
		class _3D_API CBox;
	};

	namespace DxParams
	{
		class _3D_API CDxImp;
		class _3D_API CDxParams;
		class _3D_API CDxView;
	};
};

#endif //__BOXFORWARDDECLARE__