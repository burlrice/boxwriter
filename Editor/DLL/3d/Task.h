// Task.h: interface for the CTask class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TASK_H__C632481C_E901_428E_85AC_BB301A87231A__INCLUDED_)
#define AFX_TASK_H__C632481C_E901_428E_85AC_BB301A87231A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Database.h"
#include "List.h"
#include "Message.h"
#include "3dApi.h"
#include "InvalidElementsDlg.h"
#include "Box.h"
#include "BoxForwardDeclare.h"

namespace Foxjet3d
{
	enum
	{
		PROGRESS_SET = 0,
		PROGRESS_STEP,
		PROGRESS_END,
	};

	typedef void (CALLBACK * LPPROGRESSFCT) (int nType, const CString & str, int nMax);

	using namespace FoxjetDatabase;

	//class _3D_API CTask : protected FoxjetDatabase::TASKSTRUCT  
	class _3D_API CTask : public FoxjetDatabase::TASKSTRUCT  
	{
	public:
		CTask ();
		CTask(const FoxjetDatabase::TASKSTRUCT & rhs);
		CTask(const CTask & rhs);
		CTask & operator = (const CTask & rhs);
		virtual ~CTask();

		static const FoxjetCommon::CVersion CTask::m_verUninit;

		friend _3D_API bool operator == (const CTask & lhs, const CTask & rhs);
		friend _3D_API bool operator != (const CTask & lhs, const CTask & rhs);

	public:
		DECLARE_CONSTBASEACCESS (FoxjetDatabase::TASKSTRUCT);

		bool Create (const Box::CBox & box, const FoxjetDatabase::LINESTRUCT & line, FoxjetDatabase::COdbcDatabase & db);
		bool Load (Foxjet3d::Box::CBox & box, const CString & strFile, FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver,
			CArray <InvalidElementsDlg::CItem, InvalidElementsDlg::CItem &> * pvErrors = NULL,
			LPPROGRESSFCT lpProgress = NULL, bool * pbModifiedAtLoad = NULL);
		int Load (Foxjet3d::Box::CBox & box, FoxjetDatabase::TASKSTRUCT & task, FoxjetDatabase::COdbcDatabase & db, 
			const FoxjetCommon::CVersion & ver, 
			CArray <InvalidElementsDlg::CItem, InvalidElementsDlg::CItem &> * pvErrors = NULL,
			LPPROGRESSFCT lpProgress = NULL, bool * pbModifiedAtLoad = NULL);
		bool Save (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);
		void Update ();
		bool SetVersion (const FoxjetCommon::CVersion & ver);
		int RemoveDuplicates (CStringArray & v, const FoxjetDatabase::HEADSTRUCT & head);

		CString ToString (const FoxjetCommon::CVersion & ver) const;
		bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);

		operator FoxjetDatabase::TASKSTRUCT () const { return GetTaskStruct (); }
		FoxjetDatabase::TASKSTRUCT GetTaskStruct () const;

		int GetMessageCount () const { return m_vLists.GetSize (); }
		const CMessage * GetMessage (int nIndex) const;
		CMessage * GetMessage (int nIndex);

		const FoxjetDatabase::MESSAGESTRUCT * GetMessageStruct (int nIndex) const;
		FoxjetDatabase::MESSAGESTRUCT * GetMessageStruct (int nIndex);

		const CMessage * GetMessageByHead (ULONG lHeadID) const;
		CMessage * GetMessageByHead (ULONG lHeadID);

		bool UpdateUserDefined  (const FoxjetDatabase::MESSAGESTRUCT & msg);
		bool AddMessage (CMessage * pMsg);
		void Free ();

		static int Find (const CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vHeads, ULONG lHeadID);
		static int Find (const CArray <FoxjetDatabase::MESSAGESTRUCT, FoxjetDatabase::MESSAGESTRUCT &> & vMsgs, ULONG lHeadID);
		static int Find (const CArray <FoxjetDatabase::TASKSTRUCT, FoxjetDatabase::TASKSTRUCT &> & v, ULONG lID);
		static int Find (const CArray <FoxjetDatabase::BOXSTRUCT, FoxjetDatabase::BOXSTRUCT &> & v, ULONG lID);
		static int Find (const CArray <FoxjetDatabase::VALVESTRUCT, FoxjetDatabase::VALVESTRUCT &> & vHeads, ULONG lFindID);
		static int Find (const CArray <FoxjetDatabase::PANELSTRUCT, FoxjetDatabase::PANELSTRUCT &> & v, ULONG lFindID);
		static int Find (const Panel::CBoundaryArray & vHeads, ULONG lHeadID);
		static int FindByIndex (const CArray <FoxjetDatabase::VALVESTRUCT, FoxjetDatabase::VALVESTRUCT &> & vHeads, int nFind);


	protected:
		void Add (Element::CElement * p, CMessage & msg, const FoxjetDatabase::HEADSTRUCT & head, CDC & dcMem);
		void Validate (Element::CElement * p, const FoxjetDatabase::HEADSTRUCT & head, 
			const CString & str, const CSize & size,
			const FoxjetCommon::CVersion & ver, const FoxjetCommon::CVersion & verActual, 
			CArray <InvalidElementsDlg::CItem, InvalidElementsDlg::CItem &> * pvErrors);
		void AssignElements (CMessage & msg, CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vHeads,
			FoxjetDatabase::COdbcDatabase & db);
		int GetValveIndex (const Element::CElement & e, const FoxjetDatabase::HEADSTRUCT & head);
		CMessage * FindMessage (ULONG lHeadID, int nValveIndex);
		ULONG GetIndexHeight (int nIndex, const FoxjetDatabase::HEADSTRUCT & head);

		CArray <CMessage *, CMessage *> m_vLists;
	};
}; //namespace Foxjet3d

#endif // !defined(AFX_TASK_H__C632481C_E901_428E_85AC_BB301A87231A__INCLUDED_)
