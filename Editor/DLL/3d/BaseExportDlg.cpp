// BaseExportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "BaseExportDlg.h"
#include "Database.h"
#include "List.h"
#include "Extern.h"
#include "OpenDlg.h"
#include "FileExt.h"
#include "resource.h"

using namespace Foxjet3d;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ListGlobals;
using namespace FoxjetFile;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBaseExportDlg dialog

IMPLEMENT_DYNAMIC (CBaseExportDlg, FoxjetCommon::CEliteDlg);

CBaseExportDlg::CBaseExportDlg (UINT nIDTemplate, CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(nIDTemplate, pParent)
{
	//{{AFX_DATA_INIT(CBaseExportDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CBaseExportDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBaseExportDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBaseExportDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CBaseExportDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE(CB_LINEFROM, OnSelchangeLinefrom)
	ON_BN_CLICKED(CHK_ALL, OnAll)
	ON_BN_CLICKED(BTN_SELECT, OnSelect)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBaseExportDlg message handlers

ULONG CBaseExportDlg::GetSelectedFromLineID () const
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_LINEFROM);

	ASSERT (pCB);

	return pCB->GetItemData (pCB->GetCurSel ());
}

void CBaseExportDlg::OnSelchangeLinefrom() 
{
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CArray <TASKSTRUCT *, TASKSTRUCT *> vTasks;
	CListBox * pTasks = (CListBox *)GetDlgItem (LB_TASKS);
	ULONG lFromID = GetSelectedFromLineID ();
	BeginWaitCursor ();

	ASSERT (pTasks);
	
	GetLineRecords (vLines);

	pTasks->ResetContent ();
	GetTaskRecords (ListGlobals::GetDB (), lFromID, vTasks);

	for (int i = 0; i < vTasks.GetSize (); i++) {
		int nIndex = pTasks->AddString (vTasks [i]->m_strName);

		pTasks->SetItemData (nIndex, vTasks [i]->m_lID);
	}

	Free (vTasks);
	EndWaitCursor ();
}

void CBaseExportDlg::OnAll() 
{
	CButton * pAll = (CButton *)GetDlgItem (CHK_ALL);
	CButton * pSelect = (CButton *)GetDlgItem (BTN_SELECT);
	CListBox * pTasks = (CListBox *)GetDlgItem (LB_TASKS);

	ASSERT (pAll);
	ASSERT (pSelect);
	ASSERT (pTasks);
	
	bool bAll = pAll->GetCheck () == 1;

	pSelect->EnableWindow (!bAll);
	pTasks->EnableWindow (!bAll);

	for (int i = 0; i < pTasks->GetCount (); i++) 
		pTasks->SetSel (i, bAll);
}

void CBaseExportDlg::OnSelect() 
{
	OpenDlg::COpenDlg dlg (this);

	dlg.m_lLineID = GetSelectedFromLineID ();

	if (dlg.DoModal () == IDOK) {
		CString strLine;
		CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINEFROM);
		CListBox * pTasks = (CListBox *)GetDlgItem (LB_TASKS);

		ASSERT (pLine);
		ASSERT (pTasks);

		if (dlg.m_vFiles.GetSize ()) {
			int nLineIndex = pLine->GetCurSel ();
			CString strLine = GetFileHead (dlg.m_vFiles [0]);
		
			pLine->SelectString (-1, strLine);

			if (nLineIndex != pLine->GetCurSel ())
				OnSelchangeLinefrom ();
		}

		pTasks->SetSel (-1, false);

		for (int i = 0; i < dlg.m_vFiles.GetSize (); i++) {
			CString strTask = GetFileTitle (dlg.m_vFiles [i]);

			pTasks->SetSel (pTasks->FindString (-1, strTask), true);
		}
	}
}

