#if !defined(AFX_INVALIDELEMENTSDLG_H__AE9F73A5_2A20_4425_8077_AC5B14D60734__INCLUDED_)
#define AFX_INVALIDELEMENTSDLG_H__AE9F73A5_2A20_4425_8077_AC5B14D60734__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InvalidElementsDlg.h : header file
//

#include "ListCtrlImp.h"
#include "3dApi.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CInvalidElementsDlg dialog

namespace InvalidElementsDlg
{
	class _3D_API CItem : public ItiLibrary::ListCtrlImp::CItem 
	{
	public:
		CItem (const CString & strHead = _T (""), ULONG lID = -1, int nType = -1,
			const CString & strData = _T (""), const CString & strError = _T (""));
		CItem (const CItem & rhs);
		CItem & operator = (const CItem & rhs);
		virtual ~CItem ();

		virtual CString GetDispText (int nColumn) const;

		CString m_strHead;
		CString m_strID;
		CString m_strType;
		CString m_strError;
		CString m_strData;
	};

	class _3D_API CInvalidElementsDlg : public FoxjetCommon::CEliteDlg
	{
	public:

	// Construction
	public:
		CInvalidElementsDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CInvalidElementsDlg)
		CString	m_strLine;
		CString	m_strTask;
		//}}AFX_DATA


		CArray <CItem, CItem &> m_vErrors;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CInvalidElementsDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL


	// Implementation
	protected:

		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

		// Generated message map functions
		//{{AFX_MSG(CInvalidElementsDlg)
		virtual BOOL OnInitDialog();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace InvalidElementsDlg

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INVALIDELEMENTSDLG_H__AE9F73A5_2A20_4425_8077_AC5B14D60734__INCLUDED_)
