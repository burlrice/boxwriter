// CopyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "CopyDlg.h"
#include "Resource.h"
#include "Database.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Foxjet3d;

/////////////////////////////////////////////////////////////////////////////
// CCopyDlg dialog


CCopyDlg::CCopyDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg(IDD_COPY, pParent)
{
	//{{AFX_DATA_INIT(CCopyDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CCopyDlg::CCopyDlg (UINT nIDTemplate, CWnd* pParent)
:	FoxjetCommon::CEliteDlg (nIDTemplate, pParent)
{
}

void CCopyDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCopyDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCopyDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CCopyDlg)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCopyDlg message handlers



BOOL CCopyDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();

	CAnimateCtrl * pAvi = (CAnimateCtrl *)GetDlgItem (IDC_AVI);
	CProgressCtrl * pProgress = (CProgressCtrl *)GetDlgItem (IDC_STATUS);

	ASSERT (pAvi);
	ASSERT (pProgress);

	BOOL bOpen = pAvi->Open (IDR_AVIFILECOPY);
	ASSERT (bOpen);

	pProgress->SetRange (0, 100);
	pProgress->SetStep (1);
	pProgress->SetPos (100);
	SetTimer (0, 100, NULL);
	OnTimer (0);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCopyDlg::OnTimer(UINT nIDEvent) 
{
	CProgressCtrl * pProgress = (CProgressCtrl *)GetDlgItem (IDC_STATUS);
	static int nCalls = 0;
	static int nFile = 1;

	ASSERT (pProgress);

	if (pProgress->GetPos () == 100) {
		pProgress->SetPos (0);
		nFile = 1;
	}
	else
		pProgress->StepIt ();

	if (!(nCalls++ % 15)) {
		CWnd * pText = GetDlgItem (LBL_COPY);
		CString str;

		str.Format (_T ("Copying file%03d..."), nFile++);
		pText->SetWindowText (str);
	}
}
