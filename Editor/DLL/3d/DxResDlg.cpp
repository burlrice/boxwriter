// DxResDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "Resource.h"
#include "DxResDlg.h"
#include "MsgBox.h"

#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Foxjet3d;
using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////
// CDxResDlg dialog


CDxResDlg::CDxResDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(IDD_RESOLUTION, pParent)
{
	//{{AFX_DATA_INIT(CDxResDlg)
	m_nRes = 0;
	//}}AFX_DATA_INIT
}


void CDxResDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDxResDlg)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDxResDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDxResDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDxResDlg message handlers

BOOL CDxResDlg::OnInitDialog() 
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_SIZE);

	ASSERT (pCB);
	FoxjetCommon::CEliteDlg::OnInitDialog();

	for (int i = 2; i < 15; i++) {
		CString str;
		int n = (int)pow ((double)2, (double)i);

		str.Format (_T ("%d x %d"), n, n);
		int nIndex = pCB->AddString (str);
		pCB->SetItemData (nIndex, i);

		if (m_nRes == i)
			pCB->SetCurSel (nIndex);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDxResDlg::OnOK()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_SIZE);
	ASSERT (pCB);
	int nIndex = pCB->GetCurSel ();
	
	if (nIndex != CB_ERR) {
		m_nRes = (int)pCB->GetItemData (nIndex);
		FoxjetCommon::CEliteDlg::OnOK ();
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}
