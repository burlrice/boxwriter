#if !defined(AFX_IMPORTFILEDLG_H__063A68DC_5174_455D_8140_44F11D17F208__INCLUDED_)
#define AFX_IMPORTFILEDLG_H__063A68DC_5174_455D_8140_44F11D17F208__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImportFileDlg.h : header file
//

#include "ListCtrlImp.h"
#include "Export.h"
#include "ProgressDlg.h"
#include "3dApi.h"
#include "RoundButtons.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CImportFileDlg dialog

namespace Foxjet3d
{
	int _3D_API Import (CWnd * pParent);

	class _3D_API CImportFileDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CImportFileDlg(CWnd* pParent = NULL);   // standard constructor

	public:
		typedef enum 
		{
			IMPORTTHREAD_FAILED,
			IMPORTTHREAD_RUNNING,
			IMPORTTHREAD_SUCCESS,
			IMPORTTHREAD_STOP,
		} IMPORTTHREADSTATUS;

		typedef struct
		{
			CProgressDlg *					m_pDlg;
			FoxjetDatabase::COdbcDatabase * m_pDB;
			CString							m_strFile;
			DWORD							m_dwFlags;
			CString							m_strLogosDir;
			FoxjetCommon::CImportArray		m_vResult;
			/*
			CMapStringToString				m_vBitmapFiles;
			CMapStringToString				m_vBitmapDirs;
			*/
		} IMPORTTHREADSTRUCT;

		CString m_strFile;
		
		static CString ImportYml (const CString & strFile, const FoxjetDatabase::HEADSTRUCT & head, const CMapStringToString & map, int & nChannels);
		static int ImportYml (const IMPORTTHREADSTRUCT & params);
		static void GetImage (const CString & strSocket, int nPort, const CString & strUser, const CString & strPass);
		static void GetMessages (const CString & strSocket, int nPort, const CString & strUser, const CString & strPass);
		static void RciGetMessages (const CString & strSocket, int nPort, const CString & strUser, const CString & strPass);

	// Dialog Data
		//{{AFX_DATA(CImportFileDlg)
		BOOL	m_bConfig;
		BOOL	m_bTasks;
		BOOL	m_bDeleteExistingTasks;
		//}}AFX_DATA

		static ULONG CALLBACK Import (LPVOID lpData);

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CImportFileDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:

		FoxjetUtils::CRoundButtons m_buttons;
		IMPORTTHREADSTRUCT m_params;
		HANDLE m_hThread;
		int m_nOrphaned;

		void EnableUI (bool bEnable);

		bool m_bUIEnabled;
		FoxjetCommon::CImportArray		m_vResult;
		CMapStringToString				m_vBitmapFiles;
		CMapStringToString				m_vBitmapDirs;

		virtual void OnOK ();
		virtual void OnCancel ();
		
		// Generated message map functions
		//{{AFX_MSG(CImportFileDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnBrowse();
		afx_msg void OnImport();
		afx_msg void OnTasks();
		afx_msg void OnConfig();
		//}}AFX_MSG
		
		afx_msg LRESULT OnEndProgress (WPARAM wParam, LPARAM lParam);
		afx_msg LRESULT OnMsgBox (WPARAM wParam, LPARAM lParam);
		afx_msg LRESULT OnAddResult (WPARAM wParam, LPARAM lParam);
		afx_msg LRESULT OnAddBitmapFile (WPARAM wParam, LPARAM lParam);
		afx_msg LRESULT OnAddBitmapDir (WPARAM wParam, LPARAM lParam);
		afx_msg void UpdateUI ();
		int CountOrphanedRecords ();

		DECLARE_MESSAGE_MAP()
	};
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMPORTFILEDLG_H__063A68DC_5174_455D_8140_44F11D17F208__INCLUDED_)
