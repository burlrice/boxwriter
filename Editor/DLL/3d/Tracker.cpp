// Tracker.cpp: implementation of the CElementTracker class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BaseView.h"
#include "BaseDoc.h"
#include "Coord.h"
#include "Color.h"
#include "Debug.h"
#include "ElmntLst.h"
#include "Element.h"
#include "Registry.h"
#include "Parse.h"

using namespace FoxjetDatabase;
using namespace Element;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//#define DEBUG_DRAGDROP

const FoxjetCommon::LIMITSTRUCT Element::CElementTracker::m_lmtHandle			= {	5,	15 }; // in pixels 

const int Element::CElementTracker::m_nDragHandleSize = 6;
const int Element::CElementTracker::m_nHandleIndex [8] = 
{
	TR,
	R,
	BR,
	B,
	BL,
	L,
	TL,
	T,
};


using namespace FoxjetCommon;
using namespace FoxjetDocument;
using namespace Color;

CElementTracker::CElementTracker (const FoxjetDatabase::HEADSTRUCT & head, 
								  const FoxjetDatabase::HEADSTRUCT & workingHead,
								  CElement * pElement)
: 	m_nLeadIndex (-1), 
	m_bResizing (false), m_bResizable (false), 
	m_bMovable (false),
	m_bRubberBand (false),
	m_bLockAspectRatio (false),
	m_pElement (pElement),
	m_pActiveView (NULL),
	m_lpLocationFct (NULL),
	m_head (head),
	m_workingHead (workingHead),
	m_rcLastFocus (0, 0, 0, 0),
	m_rcOriginal (0, 0, 0, 0),
	CRectTracker ()
{
	m_nHandleSize = GetHandleSize ().cx;

	if (m_pElement)
		m_rcOriginal = m_pElement->GetWindowRect (m_head, m_workingHead);
}

CElementTracker::CElementTracker (const FoxjetDatabase::HEADSTRUCT & head, 
								  const FoxjetDatabase::HEADSTRUCT & workingHead,
								  LPCRECT lpSrcRect, UINT nStyle, CElement * pElement)
: 	m_nLeadIndex (-1), 
	m_bResizing (false), m_bResizable (false), 
	m_bMovable (false), 
	m_bRubberBand (false),
	m_bLockAspectRatio (false),
	m_pElement (pElement),
	m_pActiveView (NULL),
	m_lpLocationFct (NULL),
	m_head (head),
	m_workingHead (workingHead),
	m_rcLastFocus (0, 0, 0, 0),
	m_rcOriginal (0, 0, 0, 0),
	CRectTracker (lpSrcRect, nStyle)
{
	m_nHandleSize = GetHandleSize ().cx;

	if (m_pElement)
		m_rcOriginal = m_pElement->GetWindowRect (m_head, m_workingHead);
}

CElementTracker::CElementTracker (const CElementTracker & rhs)
:	m_pActiveView (rhs.m_pActiveView),
	m_rcLastFocus (rhs.m_rcLastFocus),
	m_ptClick (rhs.m_ptClick),
	m_nHit (rhs.m_nHit),
	m_bResizing (rhs.m_bResizing),
	m_bResizable (rhs.m_bResizable),
	m_bMovable (rhs.m_bMovable),
	m_bLockAspectRatio (rhs.m_bLockAspectRatio),
	m_bRubberBand (rhs.m_bRubberBand),
	m_lpLocationFct (rhs.m_lpLocationFct),
	m_pElement (rhs.m_pElement),
	m_head (rhs.m_head),
	m_workingHead (rhs.m_workingHead)
{
	m_rcHead [0]		= rhs.m_rcHead [0];
	m_rcHead [1]		= rhs.m_rcHead [1];
	m_sizeMin			= rhs.m_sizeMin;
	m_nStyle			= rhs.m_nStyle;
	m_rect				= rhs.m_rect;

	for (int i = 0; i < 8; i++)
		m_rcHandles [i] = rhs.m_rcHandles [i];
}

CElementTracker & CElementTracker::operator = (const CElementTracker & rhs)
{
	CRectTracker::operator = (rhs);

	if (this != &rhs) {
		m_sizeMin			= rhs.m_sizeMin;
		m_nStyle			= rhs.m_nStyle;
		m_rect				= rhs.m_rect;
		m_pActiveView		= rhs.m_pActiveView;
		m_rcLastFocus		= rhs.m_rcLastFocus;
		m_rcHead [0]		= rhs.m_rcHead [0];
		m_rcHead [1]		= rhs.m_rcHead [1];
		m_ptClick			= rhs.m_ptClick;
		m_nHit				= rhs.m_nHit;
		m_bResizing			= rhs.m_bResizing;
		m_bResizable		= rhs.m_bResizable;
		m_bMovable			= rhs.m_bMovable;
		m_bRubberBand		= rhs.m_bRubberBand;
		m_bLockAspectRatio	= rhs.m_bLockAspectRatio;
		m_pElement			= rhs.m_pElement;
		m_lpLocationFct		= rhs.m_lpLocationFct;
		m_head				= rhs.m_head;
		m_workingHead		= rhs.m_workingHead;

		for (int i = 0; i < 8; i++)
			m_rcHandles [i] = rhs.m_rcHandles [i];
	}

	return * this;
}

void CElementTracker::SetResizable (bool bResizable) 
{ 
	m_bResizable = bResizable;
	m_nHandleSize = bResizable ? GetHandleSize ().cx : 0;
	// use a handle size of 0 for non-resizables so the
	// base class won't allow resizing
}

void CElementTracker::Draw (CDC * pDC, const CRect & rc) const
{
	bool bResizable = IsResizable ();
	COLORREF rgbBorder, rgbFill, rgbRect = ::GetSysColor (COLOR_HIGHLIGHT);
	CRect rcHandles [8];

	if (bResizable) {
		rgbBorder = ::GetSysColor (COLOR_HIGHLIGHTTEXT);
		rgbFill = ::GetSysColor (COLOR_HIGHLIGHT);
	}
	else {
		rgbBorder = ::GetSysColor (COLOR_HIGHLIGHT);
		rgbFill = ::GetSysColor (COLOR_HIGHLIGHTTEXT);
	}

	CBrush brBorder (rgbBorder);

	//pDC->DrawFocusRect (rc);
	//DrawSelectionRect (* pDC, rc);
	pDC->FrameRect (rc, &CBrush (rgbRect));//::GetSysColor (COLOR_HIGHLIGHT)));
	CalcHandles (rcHandles, rc);

	for (int i = 0; i < 8; i++) {
		int nIndex = m_nHandleIndex [i];

		if (rcHandles [nIndex] != ::rcNull) {
			pDC->FillSolidRect (rcHandles [nIndex], rgbFill);
			pDC->FrameRect (rcHandles [nIndex], &brBorder);
		}
	}

	if (m_pActiveView)
		m_pActiveView->OnDrawTrackerRect (pDC, rc, (DWORD)this);
}

void CElementTracker::DrawSelectionRect(CDC &dc, const CRect &rcSelection) const
{
	CRect rc (rcSelection);
	CPen pen (PS_DASH, 1, ::GetSysColor (COLOR_HIGHLIGHT));
	CGdiObject * pOld = dc.SelectObject (&pen);
	CBrush brWhite (rgbWhite);

	dc.FrameRect (rc, &brWhite);

	rc.DeflateRect (0, 0, 1, 1);

	dc.MoveTo (rc.left, rc.top);
	dc.LineTo (rc.right, rc.top);
	dc.LineTo (rc.right, rc.bottom);
	dc.LineTo (rc.left, rc.bottom);
	dc.LineTo (rc.left, rc.top);

	dc.SelectObject (pOld);
}

void CElementTracker::Draw (CDC * pDC) const
{
	Draw (pDC, m_rect);
}

BOOL CElementTracker::Track (FoxjetDocument::CBaseView * pActiveView, 
							 const CPoint & ptWhere, 
							 const CRect rcHead [2], 
							 LPSETLOCACTIONFCT lpLocationFct,
							 BOOL bAllowInvert, 
							 CWnd * pWndClipTo)
{

	CRect rcOld = m_rect, rcCalc;
	BOOL bResult = FALSE;
	int i, nHit = HitTest (ptWhere);

	ASSERT (pActiveView);

	const FoxjetDatabase::HEADSTRUCT * pHead  = pActiveView->GetDocument ()->GetSelectedHead ();
	ASSERT (pHead);
	const FoxjetDatabase::HEADSTRUCT & head  = * pHead;
	m_bResizing = ((nHit != hitMiddle) && (nHit != hitNothing));
	m_rcHead [0] = rcHead [0];
	m_rcHead [1] = rcHead [1];
	m_lpLocationFct = lpLocationFct;
	m_ptScroll = pActiveView->GetScrollPosition ();

	if (CWnd * pWnd = CWnd::GetCapture ()) {
		// CRectTracker::Track will fail if capture is set
		::SetCapture (NULL);

		#ifdef _DEBUG
		CString str;
		str.Format (_T ("Warning: GetCapture = 0x%p"), pWnd);
		TRACEF (str);
		#endif //_DEBUG
	}

	// if multiple-selection, other elements must be setup for resize/move
	for (i = 0; i < m_vSelected.GetSize (); i++) {
		CElement & e = * m_vSelected [i].m_pElement;
		ASSERT (e.m_pTracker);
		e.m_pTracker->m_bResizing = m_bResizing;
		e.m_pTracker->m_rcHead [0] = rcHead [0];
		e.m_pTracker->m_rcHead [1] = rcHead [1];
		e.m_pTracker->m_pActiveView = pActiveView;

		if (i == m_nLeadIndex) {
			e.m_pTracker->m_ptClick	= ptWhere;
			e.m_pTracker->m_nHit	= nHit;
		}
	}

	// bound the cursor
	if (IsConfiningOn ()) {
		m_rcClip = CalcClipRect (m_vSelected, ptWhere, nHit);

		// can't track on an empty rect, so make sure we clip in the right spot
		if (m_rcClip.Width () <= 0 || m_rcClip.Height () <= 0) {
			CPoint pt (ptWhere);
			
			pActiveView->ClientToScreen (&pt);
			m_rcClip = CRect (0, 0, 1, 1) + pt;
		}

		rcCalc = ClipToDesktop (m_rcClip);

//		::ClipCursor (rcCalc);
		if (nHit == CRectTracker::hitMiddle)
			::ClipCursor (rcCalc);
		else
			::ClipCursor (NULL);

		::Sleep (0);
	}

	const bool bResizing = m_bResizing;

	bResult = CRectTracker::Track (pActiveView, ptWhere, bAllowInvert, pWndClipTo);

	::ClipCursor (NULL);
	::Sleep (0);

	// if multiple-selection, other elements' resize/move complete now
	for (i = 0; i < m_vSelected.GetSize (); i++) {
		CElement & e = * m_vSelected [i].m_pElement;
		
		ASSERT (e.m_pTracker);
		e.m_pTracker->m_bResizing = false;
		e.m_pTracker->m_pActiveView = NULL;

		if (i == m_nLeadIndex) {
			e.m_pTracker->m_ptClick	= CPoint (0, 0);
			e.m_pTracker->m_nHit	= hitNothing;
		}
	}

	m_nLeadIndex = -1;
	m_vSelected.RemoveAll ();

	if (IsLockAspectRatio ()) {
		if (bResizing) 
			m_rect = m_rcLockAspectRatio;
	}

	return bResult;
}

BOOL CElementTracker::TrackRubberBand (FoxjetDocument::CBaseView * pActiveView, CPoint point, 
									   BOOL bAllowInvert)
{
	m_bRubberBand = true;
	m_pActiveView = pActiveView;
	BOOL bResult = CRectTracker::TrackRubberBand (pActiveView, point, bAllowInvert);
	m_pActiveView = NULL;
	m_bRubberBand = false;

	return bResult;
}

void CElementTracker::AdjustRect (int nHandle, LPRECT lpRect)
{
	CRectTracker::AdjustRect (nHandle, lpRect);
	CalcHandles ();
}

void CElementTracker::DrawTrackerRect (LPCRECT lpRect, CWnd * pWndClipTo, CDC * pDC, CWnd * pWnd)
{
	CRect rc = CRect (lpRect); 

	rc.NormalizeRect ();
	m_rcLastFocus.NormalizeRect ();

	if (m_bResizing) {
		if (m_bLockAspectRatio) {
			CSize size (rc.Size ());
			CSize sizeOrignial (m_rcOriginal.Size ());

			if (m_nHit == hitLeft || m_nHit == hitRight) 
				rc.bottom = rc.top + (int)(rc.Width () * ((double)m_rcOriginal.Height () / (double)m_rcOriginal.Width ()));
			else
				rc.right = rc.left + (int)(rc.Height () * ((double)m_rcOriginal.Width () / (double)m_rcOriginal.Height ()));

			if (m_nHit == hitTopLeft || m_nHit == hitBottomLeft) 
				rc += CRect (lpRect).BottomRight () - rc.BottomRight ();

			if ((rc.Width () > m_rcHead [0].Width ()) || (rc.Height () > m_rcHead [0].Height ())) {
				double d [] = 
				{
					(double)rc.Width ()		/ (double)m_rcHead [0].Width (),
					(double)rc.Height ()	/ (double)m_rcHead [0].Height (),
				};
				double dRatio = max (d [0], d [1]);

				rc.right = rc.left + (int)floor (rc.Width () / dRatio);
				rc.bottom = rc.top + (int)floor (rc.Height () / dRatio);
				ASSERT (rc.Width () <= m_rcHead [0].Width ());
				ASSERT (rc.Height () <= m_rcHead [0].Height ());
			}

			m_rcLockAspectRatio = rc;
		}
	}

	pDC->DrawFocusRect (m_rcLastFocus); // this erases the last focus rect

//	CRectTracker::DrawTrackerRect (lpRect, pWndClipTo, pDC, pWnd);
	CRectTracker::DrawTrackerRect (&rc, pWndClipTo, pDC, pWnd);

	FoxjetDocument::CBaseView * pView = DYNAMIC_DOWNCAST (CBaseView, m_pActiveView ? m_pActiveView : pWnd);
	CString strDisplay;
	ASSERT (pView);
	CBaseDoc * pDoc = pView->GetDocument ();
	ASSERT (pDoc);

	if (!m_bResizing) { // display the coordinates
		CPoint pt = CRect (rc).TopLeft () - (m_rcHead [0].TopLeft () + pView->GetOffset ());
		strDisplay = CCoord (pt, pDoc->GetUnits (), m_head, pView->GetZoom ()).Format ();
	}
	else { // display the size
		CPoint pt = CRect (rc).Size ();
		strDisplay = CCoord (pt, pDoc->GetUnits (), m_head, pView->GetZoom ()).Format ();
	}

	if (m_lpLocationFct)
		(* m_lpLocationFct) (strDisplay);

	// save last focus rect
	// if this is a multi-select and the resize/move is successful, 
	// the last focus rect is needed to calculate the elements' new rects
	m_rcLastFocus = rc; 

	if (m_nLeadIndex >= 0 && m_nLeadIndex < m_vSelected.GetSize ()) {
		CElement & eLead = * m_vSelected [m_nLeadIndex].m_pElement;
		const FoxjetDatabase::HEADSTRUCT  * pHead  = pView->GetDocument ()->GetSelectedHead ();
		ASSERT (pHead);
		const FoxjetDatabase::HEADSTRUCT & head = * pHead;
	
		ASSERT (eLead.m_pTracker);

		for (int i = 0; i < m_vSelected.GetSize (); i++) {
			if (i != m_nLeadIndex) { 
				//CRectTracker::Track will draw the lead object
				
				CElement & e = * m_vSelected [i].m_pElement;
				CRect rcCur (rc), rcFrom (e.GetWindowRect (head, m_workingHead));
				CRect rcLead (eLead.GetWindowRect (head, m_workingHead));
				
				ASSERT (e.m_pTracker);
				rcCur.NormalizeRect ();
				rcFrom.NormalizeRect ();
				rcLead.NormalizeRect ();

				// save last focus rect & update the tracker rect
				e.m_pTracker->m_rcLastFocus = CalcNewRect (
					rcCur, rcFrom, rcLead, m_bResizing, 
					m_pActiveView->GetZoom ()); 
				e.SetTrackerRect (e.m_pTracker->m_rcLastFocus);
				pDC->DrawFocusRect (e.m_pTracker->m_rcLastFocus);
			}
		}
	}

	if (m_pActiveView)
		m_pActiveView->OnDrawTrackerRect (pDC, * rc, (DWORD)this);

	#ifdef DEBUG_DRAGDROP
	{
		CRect rcClip;

		if (m_pActiveView && ::GetClipCursor (&rcClip)) {
			m_pActiveView->ScreenToClient (rcClip);
			pDC->FrameRect (rcClip, &CBrush (rgbRed));
		}
	}
	#endif //DEBUG_DRAGDROP
}

CRect CElementTracker::CalcNewRect (const CRect & rcCurrent, 
									const CRect & rcFrom,
									const CRect & rcLead, 
									bool bResize,
									double dZoom) const
{
	CRect rcNew = rcFrom;

	if (bResize) { // resizing
		CRect rcCurZoom = rcCurrent / dZoom;
		int left 	= rcCurZoom.left 	- rcLead.left;
		int top 	= rcCurZoom.top 	- rcLead.top;
		int bottom 	= rcCurZoom.bottom	- rcLead.bottom;
		int right 	= rcCurZoom.right 	- rcLead.right;

		if 		(top && left)		// NW
			rcNew.InflateRect (-left, -top, right, bottom);
		else if (top && right) 		// NE
			rcNew.InflateRect (left, -top, right, bottom);
		else if (bottom && left)	// SW
			rcNew.InflateRect (-left, top, right, bottom);
		else if (bottom && right)	// SE
			rcNew.InflateRect (left, top, right, bottom);
		else if (top || left)		// N || W
			rcNew.InflateRect (-left, -top, right, bottom);
		else if (bottom || right)	// S || E
			rcNew.InflateRect (left, top, right, bottom);

		rcNew = rcNew * dZoom;
	}
	else { // moving
		CPoint ptDiff = rcLead.TopLeft () - rcCurrent.TopLeft ();
		
		rcNew -= ptDiff;

		int right = (int)((double)rcNew.Width () * dZoom);
		int bottom = (int)((double)rcNew.Height () * dZoom);
		CPoint pt = (rcCurrent.TopLeft () - rcNew.TopLeft ()) * dZoom;

		rcNew.top = rcCurrent.top - pt.y;
		rcNew.left = rcCurrent.left - pt.x;
		rcNew.right = rcNew.left + right;
		rcNew.bottom = rcNew.top + bottom;
	}
	
	rcNew.NormalizeRect ();
	
	return rcNew;
}

void CElementTracker::OnChangedRect(const CRect &rectOld)
{
	int nMargin = 5;
	static CPoint ptPrevScroll (0, 0);

	// only want to do this for a resize operation
	if (m_pActiveView && !m_bResizing) {
		CRect rcClient;
		CPoint ptMouse;

		m_pActiveView->GetClientRect (&rcClient);
		m_pActiveView->ClientToScreen (&rcClient);
		rcClient.DeflateRect (-nMargin, -nMargin, nMargin, nMargin);
		::GetCursorPos (&ptMouse);

		ptPrevScroll = m_pActiveView->GetScrollPosition ();

		// if the mouse is with nMargin of the client rect,
		// scroll the view's window
		if (rcClient.left >= ptMouse.x) 
			m_pActiveView->OnHScroll (SB_LINELEFT, 0, NULL);
		if (rcClient.right <= ptMouse.x) 
			m_pActiveView->OnHScroll (SB_LINERIGHT, 0, NULL);
		if (rcClient.top >= ptMouse.y) 
			m_pActiveView->OnVScroll (SB_LINEUP, 0, NULL);
		if (rcClient.bottom <= ptMouse.y) 
			m_pActiveView->OnVScroll (SB_LINEDOWN, 0, NULL);

		const CPoint ptScroll = m_pActiveView->GetScrollPosition ();
		const CPoint ptChange = ptScroll - ptPrevScroll;

		// a scroll means we may have to recalc the clipping rect
		if (ptChange != CPoint (0, 0)) {
			if (m_nLeadIndex >= 0 && m_nLeadIndex < m_vSelected.GetSize ()) {
				const CElement & e = * m_vSelected [m_nLeadIndex].m_pElement;

				// only want to recalc if this tracker belongs to the 
				// lead element
				if (e.m_pTracker == this && IsConfiningOn ()) {
					CRect rcClip = ClipToDesktop (m_rcClip - ptScroll + m_ptScroll);

					::ClipCursor (rcClip);
					::Sleep (0);
				}
			}
			else if (m_bRubberBand) 
				m_rect.InflateRect (ptChange.x, ptChange.y, 0, 0);
		}
	}

	ptPrevScroll = m_pActiveView->GetScrollPosition ();
}

CRect CElementTracker::ClipToDesktop (const CRect & rc) 
{
	CRect rcClip (rc);

	// get the destop's rect
	if (CWnd * pDesktop = CWnd::GetDesktopWindow ()) {
		CRect rcDesktop (rc);

		pDesktop->GetWindowRect (&rcDesktop);
		rcClip.IntersectRect (&rcClip, &rcDesktop);
	}

	return rcClip;
}

CRect CElementTracker::CalcClipRect(const Element::CPairArray &vSelected, 
									const CPoint &ptClick, 
									int nHit) const
{
	ASSERT (m_pActiveView);

	double dZoom = m_pActiveView->GetZoom ();
	const CPoint ptScroll (0, 0); 
	CRect rcClip;
	CRect rcGroup (0, 0, 0, 0); 
	bool bResize = ((nHit != hitMiddle) && (nHit != hitNothing));
	const FoxjetDatabase::HEADSTRUCT * pHead = m_pActiveView->GetDocument ()->GetSelectedHead ();
	ASSERT (pHead);
	const FoxjetDatabase::HEADSTRUCT & head = * pHead;
	int i;
	const CPoint ptError = CBaseElement::ThousandthsToLogical (CElement::m_ptError, head);

	for (i = 0; i < vSelected.GetSize (); i++) {
		const CElement & e = vSelected [i];

		if ((!e.IsResizable () && bResize) || !e.IsMovable ()) {
			// if trying to resize a non-sizable element,
			// or moving (also includes resizing) a non-movable element,
			// the bounding rect should be as small as possible (allow
			// no movement)
			return CRect (0, 0, 1, 1) + ptClick;
		}
	}

	// caculate bounding rect of the group
	for (i = 0; i < vSelected.GetSize (); i++) {
		const CElement & e = vSelected [i];
		const CRect & rc = e.GetWindowRect (head, m_workingHead) * dZoom;
		rcGroup.UnionRect (&rcGroup, &rc);
	}

	CRect rcPA = (LPCRECT)m_rcHead [0];
	CRect rcProd = rcPA - ptScroll;

	#ifdef DEBUG_DRAGDROP
	m_pActiveView->GetDC ()->FrameRect (rcProd, &CBrush (rgbGreen));
	#endif //DEBUG_DRAGDROP

	m_pActiveView->ClientToScreen (&rcProd);
	rcClip = rcProd; // default rect is now the product area

	// normalized cursor coords w/respect to the group's rect
	CPoint ptNormHit = rcGroup.TopLeft () - ptClick;

	int l = ptNormHit.x;
	int t = ptNormHit.y;
	int r = - (ptNormHit.x + rcGroup.Width ());
	int b = - (ptNormHit.y + rcGroup.Height ());
	
	rcClip.InflateRect (l, t, r + max (ptError.x, 1), b + max (ptError.y, 1));
	rcClip -= m_rcHead [0].TopLeft ();

	if (int nDiff = max (0, m_rcHead [1].Size ().cy - m_rcHead [0].Size ().cy))
		rcClip += CPoint (0, nDiff);

	if (!bResize) {
		rcClip += ptScroll;
	}
	else {
		CPoint ptScreenClick (ptClick);

		m_pActiveView->ClientToScreen (&ptScreenClick);
		rcClip = CalcResizeRect (vSelected, ptScreenClick, 
			ptScroll, rcClip, rcProd, nHit, dZoom);
	}

	return rcClip;
}

// returns smallest width & height of the elements in vSelected
// note that both dimensions may not belong to the same element
CSize CElementTracker::CalcMinSize(const Element::CPairArray & vSelected) const
{
	int x = 0, y = 0;
	ASSERT (m_pActiveView);
	const FoxjetDatabase::HEADSTRUCT * pHead = m_pActiveView->GetDocument ()->GetSelectedHead ();
	ASSERT (pHead);
	const FoxjetDatabase::HEADSTRUCT & head = * pHead;

	for (int i = 0; i < vSelected.GetSize (); i++) {
		const CElement & e = vSelected [i];
		const CRect & rc = e.GetWindowRect (head, m_workingHead);

		if (i == 0) { 
			// x & y will always be 0 if not initialized 
			// on the first element
			x = rc.Width ();
			y = rc.Height ();
		}

		x = min (x, rc.Width ());
		y = min (y, rc.Height ());
	}

	return CSize (x, y);
}

CSize CElementTracker::GetSize() const
{
	return CSize (m_rect.Width (), m_rect.Height ());
}

CRect CElementTracker::CalcResizeRect(const Element::CPairArray & vSelected, 
									  const CPoint &ptScreenClick,
									  const CPoint &ptOffset,
									  const CRect &rcClip, 
									  const CRect &rcProd, 
									  int nHit,
									  double dZoom) const
{
	CRect rcResult;
	CSize min = (CalcMinSize (vSelected) * dZoom) - GetHandleSize ();
	CWnd * pDesktop = CWnd::GetDesktopWindow ();

	// default clip rect is the destop's rect
	if (pDesktop) 
		pDesktop->GetWindowRect (&rcResult);

	switch (nHit) {
		case hitRight:
			rcResult = CRect (ptScreenClick.x - min.cx, rcProd.top, rcClip.right, rcProd.bottom);
			rcResult.InflateRect (0, 0, ptOffset.x, 0);
			break;
		case hitLeft:
			rcResult = CRect (rcClip.left, rcProd.top, ptScreenClick.x + min.cx, rcProd.bottom);
			rcResult += CPoint (ptOffset.x, 0);
			rcResult.DeflateRect (0, 0, ptOffset.x, 0);
			break;
		case hitTop:
			rcResult = CRect (rcProd.left, rcClip.top, rcProd.right, ptScreenClick.y + min.cy);
			rcResult.DeflateRect (0, ptOffset.y, 0, 0);
			break;
		case hitBottom: 
			rcResult = CRect (rcProd.left, ptScreenClick.y - min.cy, rcProd.right, rcClip.bottom);
			rcResult.InflateRect (0, 0, 0, ptOffset.y);
			break;
		case hitTopLeft:
			rcResult = CRect (rcClip.left, rcClip.top, ptScreenClick.x + min.cx, ptScreenClick.y + min.cy);
			rcResult.DeflateRect (ptOffset.x, ptOffset.y, 0, 0);
			break;
		case hitBottomRight:
			rcResult = CRect (ptScreenClick.x - min.cx, ptScreenClick.y - min.cy, rcClip.right, rcClip.bottom);
			rcResult.InflateRect (0, 0, ptOffset.x, ptOffset.y);
			break;
		case hitTopRight:
			rcResult = CRect (ptScreenClick.x - min.cx, rcClip.top, rcClip.right, ptScreenClick.y + min.cy);
			rcResult.InflateRect (0, -ptOffset.y, ptOffset.x, 0);
			break;
		case hitBottomLeft:
			rcResult = CRect (rcClip.left, ptScreenClick.y - min.cy, ptScreenClick.x + min.cx, rcClip.bottom);
			rcResult.InflateRect (-ptOffset.x, 0, 0, ptOffset.y);
			break;
	}

	return rcResult;
}

CSize CElementTracker::GetHandleSize () 
{
	const CString strApp = GetAppTitle ();
	const CString strRegSection = _T ("Software\\FoxJet\\") + strApp + _T ("\\Defaults\\Bitmap");
	int n = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strRegSection, _T ("HandleSize"), 15);

	return CSize (n, n);
}

bool CElementTracker::SetHandleSize (const CSize & size)
{
	const CString strApp = GetAppTitle ();
	const CString strRegSection = _T ("Software\\FoxJet\\") + strApp + _T ("\\Defaults\\Bitmap");
	int n = max (size.cx, size.cy);

	if (n >= (int)m_lmtHandle.m_dwMin && n <=  (int)m_lmtHandle.m_dwMax) 
		return FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strRegSection, _T ("HandleSize"), n);

	return false;
}
