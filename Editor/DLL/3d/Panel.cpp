// Panel.cpp: implementation of the CPanel class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <math.h>

#include "3d.h"
#include "Panel.h"
#include "List.h"
#include "Color.h"
#include "DxView.h"
#include "Box.h"
#include "Debug.h"
#include "Coord.h"

#ifdef __WINPRINTER__
	#include "LabelElement.h"
#endif //__WINPRINTER__

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace Foxjet3d;
using namespace Foxjet3d::Box;
using namespace Color;
using namespace Panel;
using namespace Element;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


CPanel::CPanel (CBox & box, FACE face, const FoxjetDatabase::PANELSTRUCT & rhs)
:	m_face (face),
	m_box (box),
	m_pBmp (NULL),
	m_bValid (false),
	m_pbmpImage (NULL)
{
	m_lID			= rhs.m_lID;
	m_lLineID		= rhs.m_lLineID;
	m_strName		= rhs.m_strName;
	m_nNumber		= rhs.m_nNumber;
	m_direction		= rhs.m_direction;
	m_orientation	= rhs.m_orientation;
}

CPanel::CPanel (const CPanel & rhs)
:	m_face (rhs.m_face),
	m_box (rhs.m_box),
	m_pBmp (NULL),
	m_bValid (false),
	m_pbmpImage (NULL)
{
	m_lID			= rhs.m_lID;
	m_lLineID		= rhs.m_lLineID;
	m_strName		= rhs.m_strName;
	m_nNumber		= rhs.m_nNumber;
	m_direction		= rhs.m_direction;
	m_orientation	= rhs.m_orientation;
}

CPanel & CPanel::operator = (const CPanel & rhs)
{
	if (this != &rhs) {
		m_lID			= rhs.m_lID;
		m_lLineID		= rhs.m_lLineID;
		m_strName		= rhs.m_strName;
		m_nNumber		= rhs.m_nNumber;
		m_direction		= rhs.m_direction;
		m_orientation	= rhs.m_orientation;
		m_face			= rhs.m_face;

		Invalidate ();
	}

	return * this;
}

CPanel::~CPanel()
{
	Free ();
}

bool CPanel::IsValid () const
{
	return m_bValid;
}

void CPanel::Invalidate ()
{
	m_bValid = false;
}

bool CPanel::LoadImage (DxParams::CDxView & view, FoxjetDatabase::COdbcDatabase & db)
{
	CDC * pDC = NULL;

	if (CWnd * pParent = view.GetParent ()) 
		pDC = pParent->GetDC (); 

	return LoadImage (pDC, db);
}

bool CPanel::LoadImage (CDC * pDC, FoxjetDatabase::COdbcDatabase & db)
{
	IMAGESTRUCT img;

	m_img.Free ();

	if (GetImageRecord (db, m_box.GetBox ().m_lID, m_nNumber, img)) {
		if (!m_img.Load (img.m_strImage, m_nNumber, m_box.GetBox ().m_lID, pDC, m_orientation)) 
			return false;
	}

	return true;
}

bool CPanel::Create (DxParams::CDxView & view)
{
	if (CWnd * pParent = view.GetParent ()) {
		if (CDC * pDC = pParent->GetDC ()) {
			CDC dcMem;
			CString str = m_strName;
			CRect rc (0, 0, 200, 200);
			CRect rcText (0, 0, 0, 0); 
			CFont fnt;

			if (str.IsEmpty ())
				str.Format (_T ("%d"), m_nNumber);

			fnt.CreateFont (100, 0, 0, 0, FW_NORMAL, FALSE, TRUE, 0,
				DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
				DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("Verdana"));

			dcMem.CreateCompatibleDC (pDC);
			CFont * pFont = dcMem.SelectObject (&fnt);
			int nBkMode = dcMem.SetBkMode (TRANSPARENT);
			COLORREF rgbText = dcMem.SetTextColor (rgbLightGray);
			dcMem.DrawText (str, &rcText, DT_CALCRECT);

			m_pbmpImage = new CBitmap ();
			m_pbmpImage->CreateCompatibleBitmap (pDC, rc.Width (), rc.Height ());
			CBitmap * pBmp = dcMem.SelectObject (m_pbmpImage);

			::BitBlt (dcMem, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
			dcMem.DrawText (str, rc, DT_CENTER | DT_VCENTER);

			dcMem.SelectObject (pBmp);
			dcMem.SelectObject (pFont);
			dcMem.SetBkMode (nBkMode);
			dcMem.SetTextColor (rgbText);
		}
	}

	return true;
}

CSize CPanel::GetSize(const CBoundary & head, ORIENTATION orient, const FoxjetDatabase::BOXSTRUCT & box, const FoxjetDatabase::PANELSTRUCT & panel) 
{
	CSize size (0, 0);
	const ULONG w = box.m_lWidth;
	const ULONG l = box.m_lLength;
	const ULONG h = box.m_lHeight;
	const ULONG lTable [6][2] = 
	{
		{ l, h }, // 1
		{ w, h }, // 2
		{ l, h }, // 3
		{ w, h }, // 4
		{ l, w }, // 5
		{ l, w }, // 6
	};
	int nIndex = panel.m_nNumber - 1;

	if (orient == ROT_90 || orient == ROT_270) {
		size.cx = CBaseElement::ThousandthsToLogical (CPoint (lTable [nIndex][1], 0), head.m_card).x;
		size.cy = ThousandthsToLogical (lTable [nIndex][0]);
	}
	else {
		size.cx = CBaseElement::ThousandthsToLogical (CPoint (lTable [nIndex][0], 0), head.m_card).x;
		size.cy = ThousandthsToLogical (lTable [nIndex][1]);
	}
		
	return size;
}

CSize CPanel::GetSize(const CBoundary & head, int nOrient) const
{
	ORIENTATION orient = nOrient == -1 ? m_orientation : (ORIENTATION)nOrient;

	return GetSize (head, orient, m_box.GetBox (), GetMembers ());
}

FoxjetCommon::CLongArray CPanel::GetMessageIndicies (const CBoxParams & params, ULONG lPanelID)
{
	CLongArray result;

	for (int i = 0; i < params.m_task.GetMessageCount (); i++) {
		const CMessage * pMsg = params.m_task.GetMessage (i);

		if (pMsg->m_list.GetHead ().m_lPanelID == lPanelID) 
			result.Add (i);
	}

	return result;
}


void CPanel::Free ()
{
	m_img.Free ();
	Invalidate ();

	if (m_pbmpImage) {
		delete m_pbmpImage;
		m_pbmpImage = NULL;
	}

	if (m_pBmp) {
		delete m_pBmp;
		m_pBmp = NULL;
	}
}

void CPanel::GetHeads (const CBox & box, CBoundaryArray & vHeads) const
{
	GetHeads (box, m_lID, vHeads);
}

void CPanel::GetHeads (const CBox & box, ULONG lPanelID, CBoundaryArray & vHeads)
{
	vHeads.RemoveAll ();

	for (int nPanel = 1; nPanel <= 6; nPanel++) {
		const CPanel * p = NULL;

		if (box.GetPanel (nPanel, &p)) {
			ASSERT (p);

			if (p->GetMembers ().m_lID == lPanelID) {
				for (int i = 0; i < p->m_vBounds.GetSize (); i++) {
					CBoundary h = p->m_vBounds [i];

					vHeads.Add (h);
				}
			}
		}
	}
}

FoxjetCommon::CLongArray CPanel::GetHeads () const
{
	CLongArray sel;

	for (int i = 0; i < m_vBounds.GetSize (); i++) {
		const CBoundary & head = m_vBounds [i];

		if (head.GetPanelID () == m_lID) 
			sel.Add (head.GetID ());
	}

	return sel;
}

bool CPanel::GetHead (ULONG lHeadID, CBoundary & head) const
{
	for (int i = 0; i < m_vBounds.GetSize (); i++) {
		const CBoundary & h = m_vBounds [i];

		if (h.GetID () == lHeadID) {
			head = h;
			return true;
		}
	}

	return false;
}

bool CPanel::ContainsHead (ULONG lHeadID) const
{
	for (int i = 0; i < m_vBounds.GetSize (); i++) {
		const CBoundary & head = m_vBounds [i];

		if (head.GetID () == lHeadID)
			return true;
	}

	return false;
}

bool CPanel::HasElements (CBoxParams & p) const
{
	for (int nHead = 0; nHead < m_vBounds.GetSize (); nHead++) 
		if (CMessage * pMsg = p.m_task.GetMessageByHead (m_vBounds [nHead].GetID ())) 
			if (pMsg->m_list.GetSize ())
				return true;

	return false;
}

bool CPanel::HasImage (CBoxParams & p) const
{
	return m_img.m_pBmp ? true : false;
}

CBitmap * CPanel::CreateImage (CDC & dc, CBoxParams & p, const CBox & box, ORIENTATION orientation, 
							   DWORD dwOptions, CPanel * pAbsolute, ORIENTATION rotAbs) const
{
	DECLARETRACECOUNT (20);

	const bool bResize		= (dwOptions & CREATEIMAGE_RESIZE)			? true : false;
	const bool bElements	= (dwOptions & CREATEIMAGE_DRAWELEMENTS)	? true : false;
	bool bHasElements		= HasElements (p);
	bool bHasImage			= HasImage (p);

	if (pAbsolute) 
		bHasElements = pAbsolute->HasElements (p);

	if (!bHasElements && !bHasImage && bResize) 
		return NULL;

	CDC dcMem, dcFull;
	CBitmap * pbmpResult = new CBitmap ();
	CBitmap bmpMem, bmpFull;
	CBoundary workingHead;
	ORIENTATION orientActual = (ORIENTATION)(((int)orientation + (int)m_orientation) % 4);
	const int nMax = 256;
	CBoundaryArray vHeads;

	MARKTRACECOUNT ();

	if (pAbsolute) 
		GetHeads (box, pAbsolute->GetMembers ().m_lID, vHeads);

	box.GetWorkingHead (workingHead);

	MARKTRACECOUNT ();

	CSize sizeFull = GetSize (workingHead);
	double dFactor = min (1.0, (double)nMax / (double)max (sizeFull.cx, sizeFull.cy));
	CSize size (
		(int)((double)sizeFull.cx * dFactor),
		(int)((double)sizeFull.cy * dFactor));
	CSize sizeZoom (sizeFull);

	if (!bResize)
		size = sizeFull;
	else 
		sizeZoom = size;

	if (!vHeads.GetSize ())
		sizeFull = size;

	dcMem.CreateCompatibleDC (&dc);
	dcFull.CreateCompatibleDC (&dc);

	MARKTRACECOUNT ();
 
	bmpMem.CreateCompatibleBitmap (&dc, size.cx, size.cy);

	MARKTRACECOUNT ();
 
	if (bElements)
		bmpFull.CreateCompatibleBitmap (&dc, sizeZoom.cx, sizeZoom.cy);

	MARKTRACECOUNT ();

	CBitmap * pBmp = dcFull.SelectObject (&bmpFull);

	MARKTRACECOUNT ();

	if (bElements)
		::BitBlt (dcFull, 0, 0, sizeZoom.cx, sizeZoom.cy, NULL, 0, 0, WHITENESS);

	MARKTRACECOUNT ();
 
	if (orientActual == ROT_0 || orientActual == ROT_180)
		pbmpResult->CreateCompatibleBitmap (&dc, size.cx, size.cy);

	MARKTRACECOUNT ();
  
	CBitmap * pSelect = &bmpMem;

	if (orientActual == ROT_0 || orientActual == ROT_180)
		pSelect = pbmpResult;

	CBitmap * pMem = dcMem.SelectObject (pSelect);
	CBitmap * pFull = dcFull.SelectObject (&bmpFull);

	MARKTRACECOUNT ();

	if (m_img.m_pBmp) {
		CDC dcImage;
		DIBSECTION ds;

		dcImage.CreateCompatibleDC (&dc);
		::ZeroMemory (&ds, sizeof (ds));
		VERIFY (m_img.m_pBmp->GetObject (sizeof (ds), &ds));

		CBitmap * pImage = dcImage.SelectObject (m_img.m_pBmp);

		MARKTRACECOUNT ();
 
		if (box.IsMonochrome ())
			::BitBlt (dcMem, 0, 0, size.cx, size.cy, NULL, 0, 0, WHITENESS);
		else
			dcMem.StretchBlt (0, 0, size.cx, size.cy, &dcImage, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, SRCCOPY);

		MARKTRACECOUNT ();

		if (bElements)
			::TransparentBlt (dcMem, 0, 0, size.cx, size.cy, dcFull, 0, 0, sizeZoom.cx, sizeZoom.cy, Color::rgbWhite);

		dcImage.SelectObject (pImage);

		MARKTRACECOUNT ();
	}
	else {
		MARKTRACECOUNT ();

		::BitBlt (dcMem, 0, 0, size.cx, size.cy, NULL, 0, 0, WHITENESS);
		
		MARKTRACECOUNT ();

		dcMem.StretchBlt (0, 0, size.cx, size.cy, &dcFull, 0, 0, sizeZoom.cx, sizeZoom.cy, SRCCOPY);
	}

	dcMem.SelectObject (pMem);
	dcFull.SelectObject (pFull);

	switch (orientActual) {
	case ROT_90:
		RotateRight90 (dc, bmpMem, * pbmpResult);
		break;
	case ROT_180:
		{
			CBitmap * pBmp = dcMem.SelectObject (pbmpResult);
			Rotate180 (dcMem, size);
			dcMem.SelectObject (pBmp);
		}
		break;
	case ROT_270:
		RotateLeft90 (dc, bmpMem, * pbmpResult);
		break;
	}

	if (bElements) {
		CDC dcResult;
		int nOffsetY = 0;
		CDC dcElements;
		CBitmap bmpElements;
		double dZoom = min ((double)sizeZoom.cx / (double)sizeFull.cx, (double)sizeZoom.cy / (double)sizeFull.cy);
		CSize sizeElements (sizeZoom);

		int nRotDiff = abs ((int)rotAbs - (int)orientActual);

		if ((nRotDiff % 2) != 0) {
			nOffsetY = (sizeZoom.cy - sizeZoom.cx);
			swap (sizeElements.cx, sizeElements.cy);
		}

		dcElements.CreateCompatibleDC (&dc);
		bmpElements.CreateCompatibleBitmap (&dc, sizeElements.cx, sizeElements.cy);
		CBitmap * pElements = dcElements.SelectObject (&bmpElements);
		::BitBlt (dcElements, 0, 0, sizeElements.cx, sizeElements.cy, NULL, 0, 0, WHITENESS);

		for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
			CBoundary & head = vHeads [nHead];

			if (CMessage * pMsg = p.m_task.GetMessageByHead (head.GetID ())) {
				CPoint ptOffset = CPoint (0, sizeFull.cy - head.Height ()) - ThousandthsToLogical (CPoint (0, head.GetY ()));
				const CRect rcHead = CRect (0, 0, sizeFull.cx, head.Height ()) + ptOffset;

				for (int j = 0; j < pMsg->m_list.GetSize (); j++) {
					CElement & e = pMsg->m_list [j];
					CEditorElementList & list = pMsg->m_list;

					CRect rc = e.GetWindowRect (head.m_card, workingHead.m_card) + rcHead.TopLeft ();
					const CSize sizeElement = e.GetWindowRect (head.m_card, workingHead.m_card).Size ();
					CDC dcTmp;
					CBitmap bmpTmp;

					VERIFY (dcTmp.CreateCompatibleDC (&dc));

					VERIFY (bmpTmp.CreateCompatibleBitmap (&dc, sizeElement.cx, sizeElement.cy));
					CBitmap * pBmp = dcTmp.SelectObject (&bmpTmp);

					//rc.IntersectRect (rc, rcHead); // clip to head

					//if (e.GetElement ()->IsPrintable ())
					{
						int x = (int)((double)rc.left * dZoom); 
						int y = (int)((double)rc.top * dZoom) - nOffsetY;
						int cx = (int)((double)rc.Width () * dZoom);
						int cy = (int)((double)rc.Height () * dZoom);

						::BitBlt (dcTmp, 0, 0, sizeElement.cx, sizeElement.cy, NULL, 0, 0, WHITENESS);

						e.Draw (dcTmp, workingHead.m_card, &list, FoxjetCommon::EDITOR);
						dcElements.StretchBlt (x, y, cx, cy, &dcTmp, 0, 0, sizeElement.cx, sizeElement.cy, SRCAND);
					}
					//else {
					//	#ifdef __WINPRINTER__
					//		if (e.GetElement ()->GetClassID () == LABEL) 
					//			FoxjetElements::CLabelElement::DrawIcon (dcElements, rc);
					//	#endif //__WINPRINTER__
					//}

					dcTmp.SelectObject (pBmp);
				}
			}
		}

		dcElements.SelectObject (pElements);

		{ // bmpElements --> pbmpResult
			CDC dcMem;

			dcMem.CreateCompatibleDC (&dc);

			switch (rotAbs) {
			default:
				{
					CBitmap * pMem = dcMem.SelectObject (pbmpResult);
					CBitmap * pElements = dcElements.SelectObject (&bmpElements);
					dcMem.BitBlt (0, 0, sizeElements.cx, sizeElements.cy, &dcElements, 0, 0, SRCAND);
					dcElements.SelectObject (pElements);
					dcMem.SelectObject (pMem);
				}
				break;
			case ROT_270:
				{
					CBitmap bmp;

					RotateLeft90 (dc, bmpElements, bmp);

					CBitmap * pMem = dcMem.SelectObject (pbmpResult);
					CBitmap * pElements = dcElements.SelectObject (&bmp);
					dcMem.BitBlt (0, 0, sizeElements.cy, sizeElements.cx, &dcElements, 0, 0, SRCAND);
					dcElements.SelectObject (pElements);
					dcMem.SelectObject (pMem);
				}
				break;
			case ROT_180:
				{
					CBitmap * pBmp = dcMem.SelectObject (&bmpElements);
					Rotate180 (dcMem, sizeElements);
					dcMem.SelectObject (pBmp);

					CBitmap * pMem = dcMem.SelectObject (pbmpResult);
					CBitmap * pElements = dcElements.SelectObject (&bmpElements);
					dcMem.BitBlt (0, 0, sizeElements.cx, sizeElements.cy, &dcElements, 0, 0, SRCAND);
					dcElements.SelectObject (pElements);
					dcMem.SelectObject (pMem);
				}
				break;
			case ROT_90:
				{
					CBitmap bmp;

					RotateRight90 (dc, bmpElements, bmp);
					
					CBitmap * pMem = dcMem.SelectObject (pbmpResult);
					CBitmap * pElements = dcElements.SelectObject (&bmp);
					dcMem.BitBlt (0, 0, sizeElements.cy, sizeElements.cx, &dcElements, 0, 0, SRCAND);
					dcElements.SelectObject (pElements);
					dcMem.SelectObject (pMem);
				}
				break;
			}
		}
	}

	dcFull.SelectObject (pBmp);

	return pbmpResult;
}

CString CPanel::GetTrace () const
{
	CString str, strTrace;

	str.Format (_T ("%s [0x%p]\n"), m_strName, this);
	strTrace += str;

	for (int i = 0; i < m_vBounds.GetSize (); i++) {
		const CBoundary & b = m_vBounds [i];

		str.Format (_T ("\t[%s] %s\n"), b.m_card.m_strName, b.GetName ());
		strTrace += str;
	}

	return strTrace;
}
