#if !defined(AFX_IMPORTRESULTSDLG_H__83187EF2_4016_4A00_8F6A_395FB934AB80__INCLUDED_)
#define AFX_IMPORTRESULTSDLG_H__83187EF2_4016_4A00_8F6A_395FB934AB80__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImportResultsDlg.h : header file
//

#include "Export.h"
#include "ListCtrlImp.h"
#include "RoundButtons.h"
#include "3dApi.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CImportResultsDlg dialog

namespace Foxjet3d
{
	class _3D_API CImportResultsDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CImportResultsDlg(CWnd* pParent = NULL);   // standard constructor

		FoxjetCommon::CImportArray		m_vImport;
		CMapStringToString				m_vBitmapFiles;
		CMapStringToString				m_vBitmapDirs;
		CString							m_strImportFile;
		bool							m_bRestart;

	// Dialog Data
		//{{AFX_DATA(CImportResultsDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA


	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CImportResultsDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		class CImportItem : 
			public ItiLibrary::ListCtrlImp::CItem,
			public FoxjetCommon::IMPORTSTRUCT
		{
		public:
			CImportItem (const FoxjetCommon::IMPORTSTRUCT & rhs);
			virtual ~CImportItem ();

			virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
			virtual CString GetDispText (int nColumn) const;

			int GetDiff () const;
		};

		class CSummaryItem: public ItiLibrary::ListCtrlImp::CItem
		{
		public:
			CSummaryItem (CString & strFile, int nCount);

			virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
			virtual CString GetDispText (int nColumn) const;

			const CString m_strFile;
			const int m_nCount;
		};

		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
		ItiLibrary::ListCtrlImp::CListCtrlImp m_lvSummary;
		FoxjetUtils::CRoundButtons m_buttons;

		afx_msg void OnClipboard ();

		// Generated message map functions
		//{{AFX_MSG(CImportResultsDlg)
		virtual BOOL OnInitDialog();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMPORTRESULTSDLG_H__83187EF2_4016_4A00_8F6A_395FB934AB80__INCLUDED_)
