#if !defined(AFX_EXPORTDLG_H__CB233C4D_72F7_419A_A2A7_F1488F544C9A__INCLUDED_)
#define AFX_EXPORTDLG_H__CB233C4D_72F7_419A_A2A7_F1488F544C9A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExportDlg.h : header file
//

#include "3dApi.h"
#include "ListCtrlImp.h"
#include "BaseExportDlg.h"
#include "Database.h"

namespace Foxjet3d
{
	/////////////////////////////////////////////////////////////////////////////
	// CExportDlg dialog

	int _3D_API Copy (CWnd * pParent);

	class _3D_API CExportDlg : public Foxjet3d::CBaseExportDlg
	{
		DECLARE_DYNAMIC (CExportDlg);

	// Construction
	public:
		CExportDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CExportDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA


	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CExportDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual void OnCancel();
		virtual void OnOK();
		ULONG GetSelectedToLineID () const;
		void LoadHeadMapping ();
		void SaveHeadMapping ();
		void PrepareForExport (FoxjetDatabase::TASKSTRUCT & task, ULONG lLineID);
		int FindMessageByHeadID (const FoxjetDatabase::TASKSTRUCT & task, ULONG lHeadID);
		void SetBoxUsage (ULONG lBoxID, ULONG lLineID);
		bool PromptOverwriteExisting (FoxjetCommon::CLongArray & sel, ULONG lLineID);
		CString BuildNameList (const CStringArray & v);

		class CHeadItem : public ItiLibrary::ListCtrlImp::CItem
		{ 
		public:
			CHeadItem ();
			virtual ~CHeadItem ();

			virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
			virtual CString GetDispText (int nColumn) const;

			FoxjetDatabase::HEADSTRUCT m_headFrom;
			FoxjetDatabase::HEADSTRUCT m_headTo;
		};

		ItiLibrary::ListCtrlImp::CListCtrlImp m_lvHeads;

		// Generated message map functions
		//{{AFX_MSG(CExportDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnSelchangeLinefrom();
		afx_msg void OnDblclkHeads(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnItemchangedHeads(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnEditmapping();
		afx_msg void OnExport();
		//}}AFX_MSG

		afx_msg void OnSelchangeLineto();

		DECLARE_MESSAGE_MAP()
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXPORTDLG_H__CB233C4D_72F7_419A_A2A7_F1488F544C9A__INCLUDED_)
