// HeadConfigDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "HeadDlg.h"
#include "HeadConfigDlg.h"
#include "Resource.h"
#include "Database.h"
#include "TemplExt.h"
#include "Debug.h"
#include "List.h"
#include "PanelDlg.h"
#include "Utils.h"
#include "Extern.h"
#include "Parse.h"
#include "DevGuids.h"
#include "ValvePropDlg.h"
#include "Task.h"
#include "HeadTypeDlg.h"
#include "HpHeadDlg.h"
#include "StdCommDlg.h"
#include "WinMsg.h"
#include "CardTypeDlg.h"

#ifdef __IPPRINTER__
	#include "SyncDlg.h"
#endif //__IPPRINTER__

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Foxjet3d;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace ListGlobals;
using namespace HeadConfigDlg;

////////////////////////////////////////////////////////////////////////////////

typedef struct
{
	CString m_strName;
	ULONG m_y;
	ULONG m_cy;
} BOUNDARYSTRUCT;


////////////////////////////////////////////////////////////////////////////////

static void GetLineValves (COdbcDatabase & db, ULONG lLineID, CArray <VALVESTRUCT, VALVESTRUCT &> & v)
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

	GetLineHeads (db, lLineID, vHeads);

	for (int i = 0; i < vHeads.GetSize (); i++) 
		v.Append (vHeads [i].m_vValve);
}

static ULONG GetNextValveID (const FoxjetDatabase::HEADSTRUCT & head)
{
	ULONG lNext = 0;

	for (int i = 0; i < head.m_vValve.GetSize (); i++)
		lNext = max (lNext, head.m_vValve [i].m_lID);

	return lNext + 1;
}

static int GetNextValveIndex (const FoxjetDatabase::HEADSTRUCT & head)
{
	int nNext = 0;

	for (int i = 0; i < head.m_vValve.GetSize (); i++)
		nNext = max (nNext, head.m_vValve [i].m_nIndex);

	return nNext + 1;
}

////////////////////////////////////////////////////////////////////////////////
HeadConfigDlg::CPanel::CPanel ()
{
}

HeadConfigDlg::CPanel::CPanel (const FoxjetDatabase::PANELSTRUCT & panel)
:	m_panel (panel)
{
}

HeadConfigDlg::CPanel::CPanel (const CPanel & rhs)
:	m_panel (rhs.m_panel)
{
	m_vCards.Append (rhs.m_vCards);
	m_vValve.Append (rhs.m_vValve);
}

HeadConfigDlg::CPanel & HeadConfigDlg::CPanel::operator = (const CPanel & rhs)
{
	if (this != &rhs) {
		m_panel = rhs.m_panel;

		m_vCards.RemoveAll ();
		m_vCards.Append (rhs.m_vCards);
		
		m_vValve.RemoveAll ();
		m_vValve.Append (rhs.m_vValve);
	}

	return * this;
}

HeadConfigDlg::CPanel::~CPanel ()
{
}

int HeadConfigDlg::CPanel::Find (CArray <CPanel, CPanel &> & v, ULONG lFindID)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i].m_panel.m_lID == lFindID)
			return i;

	return -1;
}

int HeadConfigDlg::CPanel::FindByCardID (CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> & v, ULONG lFindID)
{
	for (int nPanel = 0; nPanel < v.GetSize (); nPanel++) {
		HeadConfigDlg::CPanel & panel = v [nPanel];

		for (int nCard = 0; nCard < panel.m_vCards.GetSize (); nCard++) 
			if (panel.m_vCards [nCard].m_lID == lFindID)
				return nPanel;
	}

	return -1;
}

void HeadConfigDlg::CPanel::InsertByHeight (CArray <FoxjetDatabase::VALVESTRUCT, FoxjetDatabase::VALVESTRUCT &> & v,
											 FoxjetDatabase::VALVESTRUCT & h)
{
	for (int i = 0; i < v.GetSize (); i++) {
		if (v [i].m_lHeight < h.m_lHeight) {
			v.InsertAt (i, h);
			return;
		}
	}

	v.Add (h);
}

void HeadConfigDlg::CPanel::InsertByHeight (CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & v,
											 FoxjetDatabase::HEADSTRUCT & h)
{
	for (int i = 0; i < v.GetSize (); i++) {
		if (v [i].m_lRelativeHeight < h.m_lRelativeHeight) {
			v.InsertAt (i, h);
			return;
		}
	}

	v.Add (h);
}

void HeadConfigDlg::CPanel::GetPanels (ULONG lLineID, 
	CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> & v,
	const CArray <HEADSTRUCT, HEADSTRUCT &> & vHeads,
	const CArray <VALVESTRUCT, VALVESTRUCT &> & vValves,
	const CArray <PANELSTRUCT, PANELSTRUCT &> & vPanels)
{
	{ // insert panels
		for (int nPanel = 0; nPanel < vPanels.GetSize (); nPanel++) 
			v.Add (HeadConfigDlg::CPanel (vPanels [nPanel]));
	}

	{ // insert cards
		CArray <HEADSTRUCT, HEADSTRUCT &> vTmp;
		
		vTmp.Copy (vHeads);

		while (vTmp.GetSize () > 0) {
			HEADSTRUCT h = vTmp [0];

			vTmp.RemoveAt (0);
			h.m_vValve.RemoveAll ();

			int nIndex = CPanel::Find (v, h.m_lPanelID);

			ASSERT (nIndex != -1);

			CPanel::InsertByHeight (v [nIndex].m_vCards, h); 
		}
	}

	{ // assign valves
		for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
			const HEADSTRUCT & h = vHeads [nHead];

			for (int nValve = 0; nValve < h.m_vValve.GetSize (); nValve++) {
				VALVESTRUCT valve = h.m_vValve [nValve];

				if (ToString (valve) == ToString (VALVESTRUCT ()))
					continue;

				if ((long)valve.m_lPanelID <= 0 || valve.m_lPanelID == h.m_lPanelID) {
					int nPanel = FindByCardID (v, valve.m_lCardID);
					ASSERT (nPanel != -1);
					CPanel & panel = v [nPanel];

					int nCard = CTask::Find (panel.m_vCards, valve.m_lCardID);
					ASSERT (nCard != -1);

					CPanel::InsertByHeight (panel.m_vCards [nCard].m_vValve, valve);
				}
				else {
					int nPanel = Find (v, valve.m_lPanelID);
					bool bDef = valve.m_lCardID == -1 && valve.m_lPanelID == -1 && valve.m_lID == -1;
					
					//if (!bDef && !IsHpHead (h)) { ASSERT (nPanel != -1); }

					if (nPanel != -1) {
						CPanel & p = v [nPanel];

						CPanel::InsertByHeight (p.m_vValve, valve);
					}
					else {
						#ifdef __WINPRINTER__
						TRACEF ("NOT FOUND: " + valve.m_strName);
						ASSERT (0);
						#endif
					}
				}
			}
		}
	}
}

void HeadConfigDlg::CPanel::GetPanels (FoxjetDatabase::COdbcDatabase & db, ULONG lLineID, 
										CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> & v)
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
	CArray <VALVESTRUCT, VALVESTRUCT &> vValves;
	CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;

	GetLineHeads (db, lLineID, vHeads);
	GetLineValves (db, lLineID, vValves);
	GetPanelRecords (db, lLineID, vPanels);
	GetPanels (lLineID, v, vHeads, vValves, vPanels);
}

CString HeadConfigDlg::CPanel::GetTrace ()
{
	CString str;
	
	str.Format (_T ("\t%s {m_lID: %d}\n"), m_panel.m_strName, m_panel.m_lID);

	for (int nCard = 0; nCard < m_vCards.GetSize (); nCard++) {
		CString strTmp;
		HEADSTRUCT h = m_vCards [nCard];

		strTmp.Format (_T ("\t\t%s {m_lID: %d} [m_lPanelID: %d]\n"), h.m_strName, h.m_lID, h.m_lPanelID);
		str += strTmp;

		for (int nValve = 0; nValve < h.m_vValve.GetSize (); nValve++) {
			VALVESTRUCT valve = h.m_vValve [nValve];

			strTmp.Format (_T ("\t\t\t%s, m_lPanelID: %d, m_lCardID: %d {m_lID: %d} {index: %d, linked: %d}\n"), valve.m_strName, valve.m_lPanelID, valve.m_lCardID, valve.m_lID, valve.m_nIndex, valve.m_nLinked);
			str += strTmp;
		}
	}
	
	for (int nValve = 0; nValve < m_vValve.GetSize (); nValve++) {
		CString strTmp;
		VALVESTRUCT valve = m_vValve [nValve];

		strTmp.Format (_T ("\t\t%s, m_lPanelID: %d, m_lCardID: %d {m_lID: %d} {index: %d, linked: %d}\n"), valve.m_strName, valve.m_lPanelID, valve.m_lCardID, valve.m_lID, valve.m_nIndex, valve.m_nLinked);
		str += strTmp;
	}
	

	return str;
}

void HeadConfigDlg::CPanel::UpdateValves (FoxjetDatabase::COdbcDatabase & db, ULONG lLineID, FoxjetDatabase::HEADSTRUCT & head)
{
	CArray <CPanel, CPanel &> v;

	head.m_vValve.RemoveAll ();
	CPanel::GetPanels (db, lLineID, v);

	for (int nPanel = 0; nPanel < v.GetSize (); nPanel++) {
		CPanel & p = v [nPanel];

		for (int nCard = 0; nCard < p.m_vCards.GetSize (); nCard++) {
			HEADSTRUCT & h = p.m_vCards [nCard];
	
			for (int i = 0; i < h.m_vValve.GetSize (); i++) {
				VALVESTRUCT & valve = h.m_vValve [i];

				if (valve.m_lCardID == head.m_lID)
					head.m_vValve.Add (valve);
			}
		}

		for (int i = 0; i < p.m_vValve.GetSize (); i++) {
			VALVESTRUCT & valve = p.m_vValve [i];

			if (valve.m_lCardID == head.m_lID)
				head.m_vValve.Add (valve);
		}
	}
}

FoxjetDatabase::HEADSTRUCT & HeadConfigDlg::CPanel::GetCard (CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> & vPanels, ULONG lCardID)
{
	static HEADSTRUCT err;

	for (int nPanel = 0; nPanel < vPanels.GetSize (); nPanel++) {
		HeadConfigDlg::CPanel & p = vPanels [nPanel];
		int nIndex = CTask::Find (p.m_vCards, lCardID);

		if (nIndex != -1)
			return p.m_vCards [nIndex];
	}

	ASSERT (0); 
	return err;
}
////////////////////////////////////////////////////////////////////////////////

void _3D_API Foxjet3d::InsertByHeight (CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & v,
								FoxjetDatabase::HEADSTRUCT & h)
{
	for (int i = 0; i < v.GetSize (); i++) {
		if (v [i].m_lRelativeHeight < h.m_lRelativeHeight) {
			v.InsertAt (i, h);
			return;
		}
	}

	v.Add (h);
}

void _3D_API Foxjet3d::SortByHeight (CArray <FoxjetDatabase::VALVESTRUCT, FoxjetDatabase::VALVESTRUCT &> & v)
{
	CArray <VALVESTRUCT, VALVESTRUCT &> vTmp;

	for (int i = 0; i < v.GetSize (); i++) 
		HeadConfigDlg::CPanel::InsertByHeight (vTmp, v [i]);

	Copy (v, vTmp);
}

/////////////////////////////////////////////////////////////////////////////

CFoundItem::CFoundItem (const CString & strName, const CString & strAddr)
:	m_strName (strName),
	m_strAddr (strAddr)
{
}

CFoundItem::~CFoundItem ()
{
}

CString CFoundItem::GetDispText (int nColumn) const
{
	return m_strName;
}

/////////////////////////////////////////////////////////////////////////////

CHeadConfigDlg::CItem::CItem (const FoxjetDatabase::PANELSTRUCT & s)
:	m_type (PANEL)
{
	m_pPanel = new PANELSTRUCT (s);
}

CHeadConfigDlg::CItem::CItem (const FoxjetDatabase::HEADSTRUCT & s)
:	m_type (CARD)
{
	m_pHead = new HEADSTRUCT (s);
}

CHeadConfigDlg::CItem::CItem (const FoxjetDatabase::VALVESTRUCT & s)
:	m_type (VALVE_HEAD)
{
	m_pValve = new VALVESTRUCT (s);
}

CHeadConfigDlg::CItem::~CItem ()
{
	switch (m_type) {
	case PANEL:			delete m_pPanel;	m_pPanel = NULL;	break;
	case CARD:			delete m_pHead;		m_pHead = NULL;		break;
	case VALVE_HEAD:	delete m_pValve;	m_pValve = NULL;	break;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CHeadConfigDlg dialog



CHeadConfigDlg::CHeadConfigDlg (FoxjetDatabase::COdbcDatabase & db, UNITS units, ULONG lLineID, bool bRestricted, CWnd * pParent)
:	m_db (db),
	m_units (units),
	m_lLineID (lLineID),
	m_imgPanel (NULL),
	m_imgHead (NULL),
	m_imgCard (NULL),
	m_imgMasterHead (NULL),
	m_imgHeadLinked (NULL),
	m_imgDisabledHead (NULL),
	m_lLastHeadID (-1),
	m_lLastValveID (-1),
	m_bAddressConflict (false),
	m_pDragImage (NULL),
	m_hitemDrag (NULL),
	m_hitemDrop (NULL),
	m_bDragging (false),
	m_hcurOriginal (NULL),
	m_hcurNo (NULL),
	m_bRestartTask (false),
	m_lPrinterID (0), 
	m_bSync (true),
	m_bRestricted (bRestricted),
	m_nItemHeight (48 /* IsMatrix () ? 48 : 24 */ ),
	FoxjetCommon::CEliteDlg(IDD_HEADCONFIG_MATRIX, pParent)
{
	HINSTANCE hInst = GetInstanceHandle ();

	if (m_lPrinterID == 0) 
		m_lPrinterID = GetPrinterID (m_db); 

	m_hPanel		= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_PANEL),			IMAGE_ICON, m_nItemHeight, m_nItemHeight, LR_SHARED);
	m_hHead			= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_HEAD),			IMAGE_ICON, m_nItemHeight, m_nItemHeight, LR_SHARED);
	m_hMasterHead	= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_HEADMASTER),		IMAGE_ICON, m_nItemHeight, m_nItemHeight, LR_SHARED);
	m_hDisabledHead	= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_HEADDISABLED),	IMAGE_ICON, m_nItemHeight, m_nItemHeight, LR_SHARED);
	m_hHeadLinked	= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_HEADLINKED),		IMAGE_ICON, m_nItemHeight, m_nItemHeight, LR_SHARED);
	m_hCard			= (HICON)::LoadImage (hInst, MAKEINTRESOURCE (IDI_CARD),			IMAGE_ICON, m_nItemHeight, m_nItemHeight, LR_SHARED);

	m_imgPanel.CreateFromHICON			(m_hPanel);
	m_imgHead.CreateFromHICON			(m_hHead);
	m_imgCard.CreateFromHICON			(m_hCard);
	m_imgMasterHead.CreateFromHICON		(m_hMasterHead);
	m_imgDisabledHead.CreateFromHICON	(m_hDisabledHead);
	m_imgHeadLinked.CreateFromHICON		(m_hHeadLinked);

	m_hcurOriginal			= ::GetCursor ();
	m_hcurNo				= ::LoadCursor (NULL, IDC_NO);
}

CHeadConfigDlg::~CHeadConfigDlg ()
{
	::DestroyIcon (m_hPanel);
	::DestroyIcon (m_hHead);
	::DestroyIcon (m_hMasterHead);
	::DestroyIcon (m_hDisabledHead);
	::DestroyIcon (m_hHeadLinked);
	::DestroyIcon (m_hCard);

	::SetCursor (m_hcurOriginal);

	if (m_pDragImage) 
		delete m_pDragImage;

#ifdef __IPPRINTER__
	if (m_pSocket) {
		m_pSocket->Close ();
		delete m_pSocket;
		m_pSocket = NULL;
	}
#endif //__IPPRINTER__
}

void CHeadConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHeadConfigDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_ADD);
	m_buttons.DoDataExchange (pDX, BTN_EDIT);
	m_buttons.DoDataExchange (pDX, BTN_DELETE);
	m_buttons.DoDataExchange (pDX, BTN_ADDVALVE);
	m_buttons.DoDataExchange (pDX, BTN_SYNC);
	m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);
}


BEGIN_MESSAGE_MAP(CHeadConfigDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CHeadConfigDlg)
	ON_BN_CLICKED(BTN_ADD, OnAdd)
	ON_BN_CLICKED(BTN_DELETE, OnDelete)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(BTN_ADDVALVE, OnAddValve)
	ON_CBN_SELCHANGE (CB_LINE, OnSelchangeLine)
	ON_NOTIFY(NM_DBLCLK, LV_FOUND, OnDblclkNewHeads)
	ON_BN_CLICKED(BTN_SYNC, OnSync)
	ON_REGISTERED_MESSAGE (WM_SYNCREMOTEHEAD, OnSyncRemoteHead)
	ON_REGISTERED_MESSAGE (WM_SYNCREMOTEHEADCOMPLETE, OnSyncComplete)
	ON_CBN_DBLCLK (LB_HEADS, OnDblclkHeads)
	ON_WM_DRAWITEM()
/*
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_NOTIFY(NM_DBLCLK, TV_HEADS, OnDblclkHeads)
	ON_NOTIFY(TVN_BEGINLABELEDIT, TV_HEADS, OnBeginLabelEditHeads)
	ON_NOTIFY(TVN_ENDLABELEDIT, TV_HEADS, OnEndLabelEditHeads)
	ON_NOTIFY(TVN_BEGINDRAG, TV_HEADS, OnBegindrag)
	ON_NOTIFY(TVN_SELCHANGED, TV_HEADS, OnSelChanged)
	ON_NOTIFY(TVN_SELCHANGING, TV_HEADS, OnSelChanging)
	ON_NOTIFY(NM_CLICK, TV_HEADS, OnClickHeads)
*/
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHeadConfigDlg message handlers

void CHeadConfigDlg::OnAdd() 
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads, vTotalHeads;
	HEADSTRUCT head;
	bool bUnnamed = true;
	int nMax = GetMaxHeads (m_db, _T ("TODO: rem"));
	bool bValve = ISVALVE ();
	CCardTypeDlg dlgCardType (this);

	if (bValve)
		nMax = min (2, nMax);

	GetLineHeads (m_db, m_lLineID, vHeads);
	GetPrinterHeads (m_db, m_lPrinterID, vTotalHeads); //GetHeadRecords (m_db, vTotalHeads);

	if (vTotalHeads.GetSize () >= nMax) {
		CString str;

		str.Format (LoadString (bValve ? IDS_MAXCARDS : IDS_MAXHEADS), nMax);
		MsgBox (* this, str);
		return;
	}

	dlgCardType.m_nType = CCardTypeDlg::CARD_USB;

	if (IsNetworked ()) {
		if (vTotalHeads.GetSize ())
			dlgCardType.m_nType = vTotalHeads [0].IsUSB () ? CCardTypeDlg::CARD_USB : CCardTypeDlg::CARD_PHC;

		if (!vTotalHeads.GetSize ())
			if (dlgCardType.DoModal () == IDCANCEL)
				return;
	}

	for (int i = 1; bUnnamed; i++) {
		CString str;
		ULONG lID = -1;

		str.Format (_T ("FJ%04d"), i);

		if (bValve)
			str.Format (_T ("CARD %d"), i);
		
		for (int nHead = 0; nHead < vTotalHeads.GetSize (); nHead++) {
			HEADSTRUCT h = vTotalHeads [nHead];

			if (!h.m_strName.CompareNoCase (str)) {
				lID = h.m_lID;
				break;
			}
		}

		if (lID == -1) {
			bUnnamed = false;
			head.m_strName = str;
		}
	}

	head.m_lPanelID			= -1;
	head.m_nHorzRes			= 300;
	head.m_nChannels		= 32;
	head.m_lPhotocellDelay	= 1000;
	head.m_lRelativeHeight	= 1000;
	head.m_nDirection		= LTOR;
	head.m_dHeadAngle		= 90.0;
	head.m_bInverted		= FALSE;
	head.m_nPhotocell		= EXTERNAL_PC;
	head.m_nSharePhoto		= SHARED_PCNONE;
	head.m_nEncoder			= EXTERNAL_ENC;
	head.m_nShareEnc		= SHARED_ENCNONE;
	head.m_nHeadType		= HEADTYPE_FIRST;
	head.m_bRemoteHead		= FALSE;
	head.m_bMasterHead		= vHeads.GetSize () == 0;
	head.m_bEnabled			= true; //vHeads.GetSize () == 0;
	head.m_nIntTachSpeed	= 60;
	head.m_dNozzleSpan		= 1.88;
	head.m_nEncoderDivisor	= 2;
	head.m_dPhotocellRate	= 36.0;
	head.m_strSerialParams.Format (_T ("%i,%i,%c,%i,%i"), 9600, 8, 'N', 1, 0);

	#ifdef __IPPRINTER__
	head.m_strSerialParams	= _T ("38400,0");
	head.m_bDigiNET			= true;
	#endif

	if (bValve) 
		head.m_nHeadType	= IV_72;

	#ifdef __HPHEAD__
	{
		CHeadTypeDlg dlg (this);

		if (dlg.DoModal () == IDOK) {
			head.m_nHeadType = (HEADTYPE)dlg.m_nHeadType;

			if (IsHpHead (head.m_nHeadType)) {
				using CStdCommDlg::CCommItem;

				CCommItem tmp;

				tmp.m_dwPort		= 1;
				tmp.m_lLineID		= 0;
				tmp.m_type			= (eSerialDevice)0;
				tmp.m_dwBaudRate	= 9600;
				tmp.m_nByteSize		= 8;
				tmp.m_parity		= CCommItem::GetParity ('N');
				tmp.m_nStopBits		= 0;

				head.m_strSerialParams = tmp.ToString ();
				TRACEF (head.m_strSerialParams);
				head.m_strSerialParams.Replace (_T ("{"), _T (""));
				head.m_strSerialParams.Replace (_T ("}"), _T (""));
			}
		}
		else
			return;
	}
	#endif

#ifdef __IPPRINTER__
	CLongArray sel = GetSelectedItems (m_lvFound);
	bool bAutoFound = false;

	if (sel.GetSize ()) {
		CFoundItem * p = (CFoundItem *)m_lvFound.GetCtrlData (sel [0]);

		ASSERT (p);

		head.m_strName = p->m_strName;
		head.m_strUID = p->m_strAddr;

		bAutoFound = true;
	}

	head.m_bRemoteHead		= TRUE;
#endif //__IPPRINTER__

#ifdef __WINPRINTER__
	bool bValidAddress = false;
	const CHeadDlg::PHCSTRUCT * pAddr	= bValve ? CHeadDlg::m_mapIVPHC				: (dlgCardType.m_nType == CCardTypeDlg::CARD_USB ? CHeadDlg::m_mapUSB : CHeadDlg::m_mapPHC);
	int nAddresses						= bValve ? ARRAYSIZE (CHeadDlg::m_mapIVPHC) : (ARRAYSIZE (CHeadDlg::m_mapPHC) + ARRAYSIZE (CHeadDlg::m_mapUSB));

	for (int i = 0; i < nAddresses; i++) {
		head.m_strUID.Format (_T ("%X"), pAddr [i].m_lAddr);

		if (GetHeadID (m_db, head.m_strUID, m_lPrinterID) == NOTFOUND) {
			bValidAddress = true;
			break;
		}
	}

	if (!bValidAddress) {
		MsgBox (* this, LoadString (IDS_NOMOREHEADADDRESSES), NULL, MB_ICONINFORMATION);
		return;
	}
#endif //__WINPRINTER__

	if (CItem * p = GetCurSel ()) {
		ASSERT (p->m_pHead);

		if (p->m_type == CItem::PANEL)
			head.m_lPanelID = p->m_pPanel->m_lID;
		else if (p->m_type == CItem::CARD) 
			head.m_lPanelID = p->m_pHead->m_lPanelID;
	}

	if (head.m_lPanelID == -1) {
		CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;

		GetPanelRecords (m_db, m_lLineID, vPanels);
		ASSERT (vPanels.GetSize ());
		head.m_lPanelID = vPanels [0].m_lID;
	}

	if (Edit (head, true)) {
		CListBox & lb = GetHeadCtrl ();

		if (Find (m_vAdded, head.m_lID) == -1)
			m_vAdded.Add (head.m_lID);

#ifdef __IPPRINTER__
		if (bAutoFound) {
			CLongArray sel = GetSelectedItems (m_lvFound);

			if (sel.GetSize ())
				m_lvFound.DeleteCtrlData (sel [0]);
		}
#endif //__IPPRINTER__


		lb.SetCurSel (LB_ERR);

		for (int i = 0; (i < lb.GetCount ()) && (lb.GetCurSel () == LB_ERR); i++) {
			if (CItem * p = (CItem *)lb.GetItemData (i)) {
				switch (p->m_type) {
				case CItem::CARD:
					if (p->m_pHead->m_lID == head.m_lID)
						lb.SetCurSel (i);
					break;
				case CItem::VALVE_HEAD:
					if (p->m_pValve->m_lID == head.m_lID)
						lb.SetCurSel (i);
					break;
				}
			}
		}
	}
}

void CHeadConfigDlg::OnDblclkNewHeads(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnAdd ();
	
	if (pResult)
		* pResult = 1;
}

FoxjetDatabase::HEADSTRUCT CHeadConfigDlg::GetHeadByID (ULONG lID)
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

	GetLineHeads (m_db, m_lLineID, vHeads);
	int nIndex = CTask::Find (vHeads, lID);

	ASSERT (nIndex != -1);
	return vHeads [nIndex];
}

void CHeadConfigDlg::OnDelete() 
{
	bool bHeadSelected = false;

	if (CItem * p = GetCurSel ()) {
		if (p->m_type == CItem::CARD) {
			ASSERT (p->m_pHead);

			bHeadSelected = true;
			HEADSTRUCT head = * p->m_pHead;
			
			int nResponse = MsgBox (* this, LoadString (IDS_CONFIRMDELETE), NULL, MB_YESNO | MB_ICONQUESTION);

			if (nResponse == IDYES) {
				m_lLastHeadID = -1;
				m_lLastValveID = -1;

				VERIFY (DeleteHeadRecord (m_db, head.m_lID));
				
				if (Find (m_vDeleted, head.m_lID) == -1)
					m_vDeleted.Add (head.m_lID);

				if (head.m_bMasterHead) 
					SetNewMasterHead (&head);

				m_bAddressConflict = IsAddressConflict (CStringArray ());
				FillHeadCtrl ();
			}
		}
		else if (p->m_type == CItem::VALVE_HEAD) {
			ASSERT (p->m_pValve);

			int nResponse = MsgBox (* this, LoadString (IDS_CONFIRMDELETE), NULL, MB_YESNO | MB_ICONQUESTION);

			if (nResponse == IDYES) {
				HEADSTRUCT head = GetHeadByID (p->m_pValve->m_lCardID);

				m_lLastHeadID	= -1;
				m_lLastValveID	= -1;
				bHeadSelected	= true;

				int nIndex = CTask::Find (head.m_vValve, p->m_pValve->m_lID);

				ASSERT (nIndex != -1);

				head.m_vValve.RemoveAt (nIndex);
				VERIFY (UpdateHeadRecord (m_db, head));
				m_bAddressConflict = IsAddressConflict (CStringArray ());
				FillHeadCtrl ();
			}
		}
	}

	if (!bHeadSelected)
		MsgBox (* this, LoadString (IDS_NOHEADSELECTED));
}

bool CHeadConfigDlg::Edit (FoxjetDatabase::HEADSTRUCT & head, FoxjetDatabase::VALVESTRUCT & valve) 
{
	CValvePropDlg dlg (m_db, m_lLineID, m_units, head, valve, this);

	if (dlg.DoModal () == IDOK) {
		int nIndex = CTask::Find (head.m_vValve, valve.m_lID);

		m_lLastHeadID = head.m_lID;
		m_lLastValveID = valve.m_lID;

		if (valve.m_lPanelID != dlg.m_valve.m_lPanelID) { // changed panel [check linked heads]
			CArray <VALVESTRUCT, VALVESTRUCT &> v;

			CValvePropDlg::GetList (head, dlg.m_valve, v);

			for (int i = 0; i < v.GetSize (); i++) {
				int nFind = CTask::Find (head.m_vValve, v [i].m_lID);
				
				ASSERT (nFind != -1);
				head.m_vValve [nFind].m_lPanelID = dlg.m_valve.m_lPanelID;
			}
		}

		ASSERT (nIndex != -1);

		if (dlg.m_valve.m_lCardID != valve.m_lCardID) {
			HEADSTRUCT headOld = GetHeadByID (valve.m_lCardID);
			HEADSTRUCT headNew = GetHeadByID (dlg.m_valve.m_lCardID);
			int i;

			if ((i = CTask::Find (headOld.m_vValve, valve.m_lID)) != -1) {
				headOld.m_vValve.RemoveAt (i);
				VERIFY (UpdateHeadRecord (m_db, headOld));
			}

			dlg.m_valve.m_lID = GetNextValveID (headNew);
			headNew.m_vValve.Add (dlg.m_valve);
			VERIFY (UpdateHeadRecord (m_db, headNew));

			valve = dlg.m_valve;
		}
		else {
			head.m_vValve [nIndex] = valve = dlg.m_valve;

			VERIFY (UpdateHeadRecord (m_db, head));
		}

		/*
		{ // TODO: rem
			CString str;

			str.Format (
				"\n\tm_lID: %d"
				"\n\tm_lCardID: %d"
				"\n\tm_lPanelID: %d"
				"\n\tm_lPhotocellDelay: %d"
				"\n\tm_lHeight: %d"
				"\n\tm_bEnabled: %d"
				"\n\tm_bReversed: %d"
				"\n\tm_type: %d",		
				valve.m_lID,
				valve.m_lCardID,
				valve.m_lPanelID,
				valve.m_lPhotocellDelay,
				valve.m_lHeight,
				valve.m_bEnabled,
				valve.m_bReversed,
				valve.m_type);
			TRACEF (str);
		}
		*/

		SetLinked ();

		return true;
	}

	return false;
}

bool CHeadConfigDlg::Edit (FoxjetDatabase::PANELSTRUCT & panel) 
{
	CPanelDlg dlg (m_db, panel, this);

	if (dlg.DoModal () == IDOK) {
		panel.m_strName		= dlg.m_strName;
		panel.m_direction	= (DIRECTION)dlg.m_nDirection;
		panel.m_orientation = (ORIENTATION)dlg.m_nOrientation;

		if (UpdatePanelRecord (m_db, panel)) {
			FillHeadCtrl ();
			return true;
		}
	}

	return false;
}

bool CHeadConfigDlg::EditHpHead (FoxjetDatabase::HEADSTRUCT & head, bool bNew) 
{
	CHpHeadDlg dlg (m_db, head, m_units, m_vAvailAddressMap, this);

	if (dlg.DoModal () == IDOK) {
		head = dlg.m_head;

		if (!UpdateHeadRecord (m_db, head)) 
			VERIFY (AddHeadRecord (m_db, head));

		m_bAddressConflict = IsAddressConflict (CStringArray ());
		m_lLastHeadID = head.m_lID;
		FillHeadCtrl ();

		return true;
	}

	return false;
}

bool CHeadConfigDlg::Edit (FoxjetDatabase::HEADSTRUCT & head, bool bNew) 
{
//	DECLARE_MEMVERIFY ();

	bool bMaster = head.m_bMasterHead ? true : false;

	if (IsHpHead (head.m_nHeadType)) 
		return EditHpHead (head, bNew);

	if (m_bRestricted) {
		m_vPhotoDelay.Lookup (head.m_lID, head.m_lPhotocellDelay);
	}

	CHeadPhcDlg dlgPHC (m_db, head, m_units, m_vAvailAddressMap, m_bRestricted, this);
	CHeadDlg dlgUSB (m_db, head, m_units, m_vAvailAddressMap, m_bRestricted, this);
	HEADSTRUCT headSaved = head;
	const CString strPrev = CHeadDlg::ConfigToString (head);

	dlgUSB.m_bNew		= bNew;
	dlgUSB.m_lLineID	= m_lLineID;
	dlgUSB.m_bSync		= m_bSync;

	dlgPHC.m_bNew		= bNew;
	dlgPHC.m_lLineID	= m_lLineID;
	//dlgPHC.m_bSync		= m_bSync;

	m_lLastHeadID = head.m_lID;
	m_lLastValveID = -1;

	CPanel::UpdateValves (m_db, m_lLineID, headSaved);

	int nResult = !head.IsUSB () ? dlgPHC.DoModal () : dlgUSB.DoModal ();

	if (nResult == IDOK) {
		head = !head.IsUSB () ? dlgPHC.GetHead () : dlgUSB.GetHead ();

		if (m_bRestricted) {
			m_vPhotoDelay.SetAt (head.m_lID, head.m_lPhotocellDelay);
			return false;
		}

		const CString strCur = CHeadDlg::ConfigToString (head);
		
		if (IsConfig ()) {
			UpdateNetworkedHeads (m_db, head);
		}

		/*
		if (strPrev != strCur) {
			UINT nID [] = 
			{
				IDC_INFO,
				TXT_INFO,
			};

			m_bRestartTask = true;

			for (int i = 0; i < ARRAYSIZE (nID); i++)
				if (CWnd * p = GetDlgItem (nID [i]))
					p->ShowWindow (SW_SHOW);
		}
		*/

#ifdef __IPPRINTER__
		head.m_bRemoteHead = TRUE;
#else
		head.m_bRemoteHead = FALSE;
#endif

		head.m_vValve.Copy (headSaved.m_vValve);

		if (!UpdateHeadRecord (m_db, head)) {
			VERIFY (AddHeadRecord (m_db, head));

			if (ISVALVE ()) {
				VALVESTRUCT valve;

				if (head.m_vValve.GetSize ())
					valve = head.m_vValve [0];

				valve.m_lID					= 1;
				valve.m_lCardID				= head.m_lID;		
				valve.m_lPanelID			= head.m_lPanelID;
				valve.m_strName				= _T ("FJ0001");

				if (_tcstoul (head.m_strUID, NULL, 16) == 0x320)
					valve.m_strName			= _T ("FJ0009");

				head.m_vValve.RemoveAll ();
				head.m_vValve.Add (valve);

				VERIFY (UpdateHeadRecord (m_db, head));
			}
		}

		if (!bMaster && head.m_bMasterHead) {
			// it wasn't the master before, but now is
			// find the old master & change it
			CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

			GetLineHeads (m_db, m_lLineID, vHeads);

			for (int i = 0; i < vHeads.GetSize (); i++) {
				HEADSTRUCT h = vHeads [i];

				if (h.m_lID != head.m_lID && h.m_bMasterHead) {
					h.m_bMasterHead = false;
					VERIFY (UpdateHeadRecord (m_db, h));
				}
			}
		}
		else if (bMaster && !head.m_bMasterHead) {
			// it was the master head before, but now is not
			// pick a new master head

			SetNewMasterHead (&head);
		}
		else {
			// verify that one head in the system is the master
			if (GetMasterHeadID () == -1)
				SetNewMasterHead (NULL);
		}

		m_bAddressConflict = IsAddressConflict (CStringArray ());
		m_lLastHeadID = head.m_lID;
		FillHeadCtrl ();
		
		return true;
	}

	return false;
}

void CHeadConfigDlg::OnEdit() 
{
//	DECLARE_MEMVERIFY ();

	bool bHeadSelected = false;

	if (CItem * p = GetCurSel ()) {
		const CItem::TYPE type = p->m_type;
		CListBox & lb = GetHeadCtrl ();
		ULONG lID;

		switch (p->m_type) {
		case CItem::PANEL:		lID = p->m_pPanel->m_nNumber;	break;
		case CItem::CARD:		lID = p->m_pHead->m_lID;		break;
		case CItem::VALVE_HEAD:	lID = p->m_pValve->m_lID;		break;
		}

		if (p->m_type == CItem::CARD) {
			ASSERT (p->m_pHead);
			HEADSTRUCT head = * p->m_pHead;

			bHeadSelected = true;

			if (Edit (head))
				if (Find (m_vEdited, head.m_lID) == -1)
					m_vEdited.Add (head.m_lID);
		}
		else if (p->m_type == CItem::PANEL) {
			ASSERT (p->m_pPanel);
			PANELSTRUCT panel = * p->m_pPanel;

			bHeadSelected = true;
			Edit (panel);
		}
		else if (p->m_type == CItem::VALVE_HEAD) {
			ASSERT (p->m_pValve);

			VALVESTRUCT valve = * p->m_pValve;
			HEADSTRUCT head = GetHeadByID (p->m_pValve->m_lCardID);

			bHeadSelected = true;

			if (Edit (head, valve))	
				FillHeadCtrl ();
		}

		BeginWaitCursor ();
		int nSel = LB_ERR;

		for (int i = 0; (i < lb.GetCount ()) && (nSel == LB_ERR); i++) {
			if (CItem * p = (CItem *)lb.GetItemData (i)) {
				if (p->m_type == type) {
					switch (p->m_type) {
					case CItem::PANEL:
						if (p->m_pPanel->m_nNumber == lID)
							nSel = i;
						break;
					case CItem::CARD:
						if (p->m_pHead->m_lID == lID)
							nSel = i;
						break;
					case CItem::VALVE_HEAD:
						if (p->m_pValve->m_lID == lID)
							nSel = i;
						break;
					}
				}
			}
		}

		lb.SetCurSel (nSel);

		EndWaitCursor ();
	}

	if (!bHeadSelected)
		MsgBox (* this, LoadString (IDS_NOHEADSELECTED));
}

bool CHeadConfigDlg::SetNewMasterHead (const FoxjetDatabase::HEADSTRUCT * pHeadOld)
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

	GetLineHeads (m_db, m_lLineID, vHeads);

	if (pHeadOld) {
		for (int i = 0; i < vHeads.GetSize (); i++) {
			HEADSTRUCT h = vHeads [i];

			if (h.m_lID != pHeadOld->m_lID && h.m_bEnabled) {
				h.m_bMasterHead = true;
				h.m_bEnabled = true;

				VERIFY (UpdateHeadRecord (m_db, h));
				return true;
			}
		}
	}

	if (GetMasterHeadID () == -1 && vHeads.GetSize ()) {
		HEADSTRUCT h = vHeads [0];

		h.m_bMasterHead = true;
		h.m_bEnabled = true;

		VERIFY (UpdateHeadRecord (m_db, h));

		return true;
	}

	return false;
}

ULONG CHeadConfigDlg::GetMasterHeadID ()
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

	GetLineHeads (m_db, m_lLineID, vHeads);

	for (int i = 0; i < vHeads.GetSize (); i++) 
		if (vHeads [i].m_bMasterHead) 
			return vHeads [i].m_lID;

	return -1;
}

void CHeadConfigDlg::OnDblclkHeads() 
{
	OnEdit ();
}

void CHeadConfigDlg::OnSetFont (CFont* p)
{
	LOGFONT lf;

	p->GetLogFont (&lf);

	CString str;
	str.Format (_T ("%s(%d)"), lf.lfFaceName, lf.lfHeight);
	//TRACEF (str);

	FoxjetCommon::CEliteDlg::OnSetFont (p);
}

BOOL CHeadConfigDlg::OnInitDialog() 
{
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);

	ASSERT (pLine);
	FoxjetCommon::CEliteDlg::OnInitDialog();

	GetPrinterLines (m_db, m_lPrinterID, vLines); //GetLineRecords (m_db, vLines);

	for (int i = 0; i < vLines.GetSize (); i++) {
		LINESTRUCT line = vLines [i];

		int nIndex = pLine->AddString (line.m_strName);
		pLine->SetItemData (nIndex, line.m_lID);

		if (m_lLineID == line.m_lID)
			pLine->SetCurSel (nIndex);
	}

	if (pLine->GetCurSel () == CB_ERR)
		pLine->SetCurSel (0);

	FillHeadCtrl ();

#ifdef __IPPRINTER__
	using namespace FoxjetIpElements;

	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;
	int nFlag = 1;

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_NEWHEADS), 100));
	m_lvFound.Create (LV_FOUND, vCols, this, defElements.m_strRegSection);	
	m_pSocket = new CUDPasocket (NewHeadFct, this);

	if (!m_pSocket->Create (FJ_UDP_PORT, SOCK_DGRAM)) {
		CString str;

		str.Format (_T ("Failed to create UDP socket: [%d] %s"), m_pSocket->GetLastError (), FormatMessage (m_pSocket->GetLastError ()));
		TRACEF (str);
	}
	else if (!m_pSocket->SetSockOpt(SO_BROADCAST, &nFlag, sizeof (int))) {
		CString str;

		str.Format (_T ("UDP SetSockOpt failed to set SO_BROADCAST: [%d] %s"), m_pSocket->GetLastError (), FormatMessage (m_pSocket->GetLastError ()));
		TRACEF (str);
	}


#endif //__IPPRINTER__

#ifdef __WINPRINTER__
	if (CWnd * p = GetDlgItem (BTN_SYNC))
		p->ShowWindow (SW_HIDE);

	if (CWnd * p = GetDlgItem (LV_FOUND))
		p->ShowWindow (SW_HIDE);
#else
	if (!m_bSync)
		if (CWnd * p = GetDlgItem (BTN_SYNC))
			p->ShowWindow (SW_HIDE);
#endif 

	bool bValve = ISVALVE ();

	if (bValve) {
		if (CWnd * p = GetDlgItem (BTN_ADDVALVE))
			p->SetWindowText (LoadString (IDS_ADDVALVE));
		
		if (CWnd * p = GetDlgItem (BTN_ADD))
			p->SetWindowText (LoadString (IDS_ADDHEAD));
	}
	else {
		if (CWnd * p = GetDlgItem (BTN_ADDVALVE))
			p->ShowWindow (SW_HIDE);
	}

	if (m_bRestricted) {
		UINT nID [] = 
		{	
			BTN_ADD,
			BTN_DELETE,
			BTN_ADDVALVE,
		};

		for (int i = 0; i < ARRAYSIZE (nID); i++)
			if (CWnd * p = GetDlgItem (nID [i]))
				p->EnableWindow (FALSE);
	}

	if (IsControl (true) && IsNetworked ()) {
		UINT n [] = 
		{
			BTN_ADD,
			BTN_DELETE,
		};

		for (int i = 0; i < ARRAYSIZE (n); i++) 
			if (CWnd * p = GetDlgItem (n [i]))
				p->EnableWindow (FALSE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CALLBACK CHeadConfigDlg::NewHeadFct (const CString & str, CWnd * pWnd)
{
#ifdef __IPPRINTER__

	ASSERT (pWnd);

	CHeadConfigDlg & dlg = * (CHeadConfigDlg *)pWnd;
	CStringArray v;
	CString strName, strAddr;

	Tokenize (str, v, ';');

	for (int i = 0; i < v.GetSize (); i++) {

		if (strName.GetLength () == 0)
			strName = Extract (v [i], _T ("GROUP_IDS=\""), _T ("\""));

		if (strAddr.GetLength () == 0)
			strAddr = Extract (v [i], _T ("GROUP_IPADDRS="));
	}

	if (strName.GetLength () && strAddr.GetLength ()) 
		dlg.OnNewHead (strName, strAddr);

	return TRUE;
#endif //__IPPRINTER__

	return FALSE;
}

void CHeadConfigDlg::OnNewHead (const CString & strName, const CString & strAddr)
{
#ifdef __IPPRINTER__
	CArray <HEADSTRUCT, HEADSTRUCT &> v;

	GetPrinterHeads (m_db, m_lPrinterID, v); //GetHeadRecords (m_db, v);

	TRACEF (strAddr);

	for (int i = 0; i < v.GetSize (); i++) {
		HEADSTRUCT h = v [i];

		if (!h.m_strUID.CompareNoCase (strAddr))
			return;
	}

	CFoundItem * p = new CFoundItem (strName, strAddr);
	m_lvFound.InsertCtrlData (p);
	m_lvFound->ShowWindow (SW_SHOW);

#endif //__IPPRINTER__
}

CListBox & CHeadConfigDlg::GetHeadCtrl ()
{
	CListBox * p = (CListBox *)GetDlgItem (LB_HEADS);
	
	ASSERT (p);

	return * p;
}

CHeadConfigDlg::CItem * CHeadConfigDlg::GetCurSel ()
{
	CListBox & lb = GetHeadCtrl ();

	int nIndex = lb.GetCurSel ();

	if (nIndex != LB_ERR)
		return (CItem *)lb.GetItemData (nIndex);

	return NULL;
}

/* TODO: rem
void CHeadConfigDlg::InsertItem (CArray <VALVESTRUCT, VALVESTRUCT &> & vValve, HTREEITEM hParent)
{
	CTreeCtrl & tree = GetTreeCtrl ();

	for (int nValve = 0; nValve < vValve.GetSize (); nValve++) {
		VALVESTRUCT & valve = vValve [nValve];
		CString str;
		int nIndex = valve.IsLinked () ? m_nHeadLinkedIndex : m_nHeadIndex;

		str.Format (_T ("%s [%d]"), valve.GetName (), valve.m_nIndex);

		HTREEITEM hValve = tree.InsertItem (str, nIndex, nIndex, hParent, TVI_LAST);

		tree.SetItemData (hValve, (DWORD)new CItem (valve));

		if (m_lLastValveID == valve.m_lID)
			tree.Select (hValve, TVGN_CARET);
	}
}

void CHeadConfigDlg::InsertItem (HEADSTRUCT & head, HTREEITEM hParent)
{
	CTreeCtrl & tree = GetTreeCtrl ();
	int nImage = head.m_bMasterHead ? m_nMasterHeadIndex : m_nHeadIndex;
	CString str = head.m_strName;

#ifdef __WINPRINTER__
	if (m_bAddressConflict) {
		if (IsHpHead (head.m_nHeadType))
			str += _T (" (") + head.m_strUID + _T (")");
		else
			str += _T (" (") + CHeadDlg::GetPHC (_tcstoul (head.m_strUID, NULL, 16)) + _T (")");
	}
#endif //__WINPRINTER__

	if (IsValveHead (head)) {
		CString strTmp;
		HEADSTRUCT tmp;

		GetHeadRecord (m_db, head.m_lID, tmp);
		
		strTmp.Format (_T (" (%d)"), tmp.GetChannels ());
		str += strTmp;

		nImage = m_nCardIndex;
	}

	if (!head.m_bEnabled)
		nImage = m_nDisabledHeadIndex;

	HTREEITEM hHead = tree.InsertItem (str, nImage, nImage, hParent, TVI_LAST);

	tree.SetItemData (hHead, (DWORD)new CItem (head));

	if (m_lLastHeadID == head.m_lID)
		tree.Select (hHead, TVGN_CARET);
}
*/

void CHeadConfigDlg::FillHeadCtrl()
{
	CListBox & lb = GetHeadCtrl ();
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CArray <CPanel, CPanel &> vPanels;
	bool bValve = ISVALVE ();

	ASSERT (pLine);
	m_lLineID = pLine->GetItemData (pLine->GetCurSel ());

	CPanel::GetPanels (m_db, m_lLineID, vPanels);

	ResetContent ();

	/* TODO: rem
	if (bValve) {
		for (int nPanel = 0; nPanel < vPanels.GetSize (); nPanel++) {
			CPanel & panel = vPanels [nPanel];

			for (int nHead = 0; nHead < panel.m_vCards.GetSize (); nHead++) {
				HEADSTRUCT & head = panel.m_vCards [nHead];

				InsertItem (head, NULL); 
			}
		}
	}

	for (int nPanel = 0; nPanel < vPanels.GetSize (); nPanel++) {
		CPanel & panel = vPanels [nPanel];

		if (!FoxjetDatabase::IsHighResDisplay () && !panel.m_vCards.GetSize ())
			continue;

		HTREEITEM hPanel = tree.InsertItem (panel.m_panel.m_strName, 
			m_nPanelIndex, m_nPanelIndex, NULL, TVI_LAST);
		CArray <VALVESTRUCT, VALVESTRUCT &> vValve;

		tree.SetItemData (hPanel, (DWORD)new CItem (panel.m_panel));

		for (int nHead = 0; nHead < panel.m_vCards.GetSize (); nHead++) {
			HEADSTRUCT & head = panel.m_vCards [nHead];

			if (!bValve)
				InsertItem (head, hPanel); 
			else	
				vValve.Append (head.m_vValve);
		}

		if (bValve) {
			vValve.Append (panel.m_vValve);
			SortByHeight (vValve);
			InsertItem (vValve, hPanel); 
		}

		ExpandTreeItems (tree, tree.GetRootItem ());
	}
	*/

	for (int nPanel = 0; nPanel < vPanels.GetSize (); nPanel++) {
		CPanel & panel = vPanels [nPanel];

		if (panel.m_vCards.GetSize ()) {
			int nIndex = lb.AddString (panel.m_panel.m_strName);
			lb.SetItemData (nIndex, (DWORD)new CItem (panel.m_panel));
			lb.SetItemHeight (nIndex, m_nItemHeight);

			for (int nHead = 0; nHead < panel.m_vCards.GetSize (); nHead++) {
				HEADSTRUCT & head = panel.m_vCards [nHead];

				int nIndex = lb.AddString (head.m_strName);
				lb.SetItemData (nIndex, (DWORD)new CItem (head));
				lb.SetItemHeight (nIndex, m_nItemHeight);
			}
		}
	}
}

/* TODO: rem
void CHeadConfigDlg::DeleteTreeItem (CTreeCtrl & tree, HTREEITEM hItem)
{
	while (hItem) {
		if (CItem * p = (CItem *)tree.GetItemData (hItem)) 
			delete p;

		if (tree.ItemHasChildren (hItem))
			DeleteTreeItem (tree, tree.GetChildItem (hItem));

		hItem = tree.GetNextItem (hItem, TVGN_NEXT);
	}
}
 
void CHeadConfigDlg::ExpandTreeItems (CTreeCtrl & tree, HTREEITEM hItem, UINT nCode)
{
	while (hItem) {
		tree.Expand (hItem, nCode);

		if (tree.ItemHasChildren (hItem))
			ExpandTreeItems (tree, tree.GetChildItem (hItem), nCode);

		hItem = tree.GetNextItem (hItem, TVGN_NEXT);
	}
}
*/

void CHeadConfigDlg::ResetContent ()
{
	CListBox & lb = GetHeadCtrl ();
	CArray <CItem *, CItem *> v;

	for (int i = 0; i < lb.GetCount (); i++) 
		if (CItem * p = (CItem *)lb.GetItemData (i))
			v.Add (p);

	lb.ResetContent ();

	for (int i = 0; i < v.GetSize (); i++)
		if (CItem * p = v [i])	
			delete p;

}

void CHeadConfigDlg::OnDestroy() 
{
	ResetContent ();
/*
	CTreeCtrl & tree = GetTreeCtrl ();

	DeleteTreeItem (tree, tree.GetRootItem ());

	if (CImageList * p = tree.GetImageList (TVSIL_NORMAL)) 
		p->DeleteImageList ();
*/

	FoxjetCommon::CEliteDlg::OnDestroy();
}

void CHeadConfigDlg::OnSelchangeLine ()
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);

	ASSERT (pLine);
	
	m_lLineID = pLine->GetItemData (pLine->GetCurSel ());
	FillHeadCtrl ();
}

bool CHeadConfigDlg::IsAddressConflict (CStringArray & vError)
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

	GetPrinterHeads (m_db, m_lPrinterID, vHeads); //GetHeadRecords (m_db, vHeads);

	for (int i = 0; i < vHeads.GetSize (); i++) {
		for (int j = 0; j < vHeads.GetSize (); j++) {
			if (i != j) {
				HEADSTRUCT lhs = vHeads [i];
				HEADSTRUCT rhs = vHeads [j];

				if (!lhs.m_strUID.CompareNoCase (rhs.m_strUID)) {
					if (Find (vError, rhs.m_strName, false) == -1)
						vError.Add (rhs.m_strName);
				}
			}
		}
	}

	return vError.GetSize () != 0;
}


bool CHeadConfigDlg::IsOverlapConflict (CStringArray & vError)
{
	CArray <LINESTRUCT, LINESTRUCT &> vLines;

	vError.RemoveAll ();
	GetPrinterLines (m_db, m_lPrinterID, vLines); //GetLineRecords (m_db, vLines);

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		LINESTRUCT line = vLines [nLine];
		CArray <CPanel, CPanel &> v;

		CPanel::GetPanels (m_db, line.m_lID, v);

		for (int nPanel = 0; nPanel < v.GetSize (); nPanel++) {
			CPanel & p = v [nPanel];
			CArray <BOUNDARYSTRUCT, BOUNDARYSTRUCT &> vBounds;

			for (int nCard = 0; nCard < p.m_vCards.GetSize (); nCard++) {
				HEADSTRUCT & h = p.m_vCards [nCard];
				BOUNDARYSTRUCT b;

				if (!IsValveHead (h)) {
					b.m_strName = h.m_strName;
					b.m_y		= h.m_lRelativeHeight;
					b.m_cy		= CBaseElement::LogicalToThousandths (CPoint (0, h.Height ()), h).y;

					//{ CString str; str.Format ("%s: %d (%d)", b.m_strName, b.m_y, b.m_cy); TRACEF (str); }
					vBounds.Add (b);
				}
				else {
					for (int nValve = 0; nValve < h.m_vValve.GetSize (); nValve++) {
						VALVESTRUCT & valve = h.m_vValve [nValve];
						BOUNDARYSTRUCT b;

						b.m_strName		= valve.GetName ();
						b.m_y			= valve.m_lHeight;
						b.m_cy			= GetValveHeight (valve.m_type);//CBaseElement::LogicalToThousandths (CPoint (0, valve.Height ()), h).y;

						//{ CString str; str.Format ("%s: %d (%d)", b.m_strName, b.m_y, b.m_cy); TRACEF (str); }
						vBounds.Add (b);
					}
				}
			}

			for (int nValve = 0; nValve < p.m_vValve.GetSize (); nValve++) {
				VALVESTRUCT & valve = p.m_vValve [nValve];
				BOUNDARYSTRUCT b;

				b.m_strName		= valve.GetName ();
				b.m_y			= valve.m_lHeight;
				b.m_cy			= GetValveHeight (valve.m_type);//CBaseElement::LogicalToThousandths (CPoint (0, valve.Height ()), h).y;

				//{ CString str; str.Format ("%s: %d (%d)", b.m_strName, b.m_y, b.m_cy); TRACEF (str); }
				vBounds.Add (b);
			}

			for (int i = 0; i < vBounds.GetSize (); i++) {
				for (int j = 0; j < vBounds.GetSize (); j++) {
					if (i != j) {
						BOUNDARYSTRUCT a = vBounds [i];
						BOUNDARYSTRUCT b = vBounds [j];
						CRect rcA = CRect (CPoint (0, 0), CSize (100, a.m_cy)) + CPoint (0, a.m_y);
						CRect rcB = CRect (CPoint (0, 0), CSize (100, b.m_cy)) + CPoint (0, b.m_y);

						if (CRect ().IntersectRect (rcA, rcB)) {
							TRACEF (a.m_strName + ", " + b.m_strName);

							if (Find (vError, a.m_strName, false) == -1)
								vError.Add (a.m_strName);
						}
					}
				}
			}
		}
	}
	
	return vError.GetSize () != 0;
}

bool CHeadConfigDlg::IsIndexConflict (CStringArray & vError)
{
	CArray <LINESTRUCT, LINESTRUCT &> vLines;

	vError.RemoveAll ();
	GetPrinterLines (m_db, m_lPrinterID, vLines); //GetLineRecords (m_db, vLines);

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		LINESTRUCT line = vLines [nLine];
		CArray <HEADSTRUCT, HEADSTRUCT &> v;

		GetLineHeads (m_db, line.m_lID, v);

		for (int nHead = 0; nHead < v.GetSize (); nHead++) {
			HEADSTRUCT & h = v [nHead];

			for (int i = 0; i < h.m_vValve.GetSize (); i++) {
				for (int j = 0; j < h.m_vValve.GetSize (); j++) {
					if (i != j) {
						VALVESTRUCT & a = h.m_vValve [i];
						VALVESTRUCT & b = h.m_vValve [j];

						if (a.m_nIndex == b.m_nIndex) {
							TRACEF (a.m_strName + ", " + b.m_strName);

							if (Find (vError, a.m_strName, false) == -1)
								vError.Add (a.m_strName);
						}
					}
				}
			}
		}
	}
	
	return vError.GetSize () != 0;
}

void CHeadConfigDlg::SetLinked ()
{
	CArray <LINESTRUCT, LINESTRUCT &> vLines;

	GetPrinterLines (m_db, m_lPrinterID, vLines); //GetLineRecords (m_db, vLines);

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		LINESTRUCT line = vLines [nLine];
		CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

		GetLineHeads (m_db, line.m_lID, vHeads);

		for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
			HEADSTRUCT & h = vHeads [nHead];

			for (int nValve = 0; nValve < h.m_vValve.GetSize (); nValve++) {
				VALVESTRUCT s = h.m_vValve [nValve];

				if (s.IsLinked ()) {
					CArray <VALVESTRUCT, VALVESTRUCT &> v;

					CValvePropDlg::GetList (h, s, v);
					ASSERT (v.GetSize ());
					const VALVESTRUCT root = v [0];

					ULONG y		= root.m_lHeight + GetValveHeight (root.m_type);
					int nIndex	= root.m_nIndex;
					int nRootRes = (int)(((double)GetValveChannels (root.m_type) / (double)GetValveHeight (root.m_type)) * 10000.0);

					for (int i = 0; i < v.GetSize (); i++) {
						VALVESTRUCT valve = v [i];
						ULONG lHeight = GetValveHeight (valve.m_type);
						int nRes = (int)(((double)GetValveChannels (valve.m_type) / (double)lHeight) * 10000.0);

						if (nRootRes != nRes)
							valve.m_type = root.m_type;

						if ((int)(y - lHeight) >= 0)
							y -= lHeight;
						else
							TRACEF (valve.m_strName + ": height below lower limit");
							

						valve.m_lHeight		= y;
						valve.m_nIndex		= nIndex++;
						valve.m_lCardID		= root.m_lCardID;
						valve.m_lPanelID	= root.m_lPanelID;
						valve.m_bReversed	= root.m_bReversed;
						valve.m_bUpsidedown	= root.m_bUpsidedown;

						int nUpdate = CTask::Find (h.m_vValve, valve.m_lID);
						ASSERT (nUpdate != -1);
						h.m_vValve [nUpdate] = valve;

						//{ CString str; str.Format ("%s [%d]: %d [%d]", valve.m_strName, valve.m_nIndex, valve.m_lHeight, GetValveHeight (valve.m_type)); TRACEF (str); }
					}

					UpdateHeadRecord (m_db, h);
				}
			}
		}
	}
}

void CHeadConfigDlg::OnCancel ()
{
	m_vAdded.RemoveAll ();
	m_vEdited.RemoveAll ();
	m_vDeleted.RemoveAll ();

	FoxjetCommon::CEliteDlg::OnCancel ();
}

bool CHeadConfigDlg::IsPHC1Master ()
{
	CArray <HEADSTRUCT, HEADSTRUCT &> v;

	GetPrinterHeads (m_db, m_lPrinterID, v); //GetHeadRecords (m_db, v);

	{
		bool bHP = true;
		bool bUSB = true;

		for (int i = 0; i < v.GetSize (); i++) {
			ULONG lAddr = _tcstoul (v [i].m_strUID, NULL, 16);

			bHP &= IsHpHead (v [i]);
			bUSB &= lAddr >= 0x001 && lAddr < 0x300;
		}

		if (bHP)
			return true;
		if (bUSB)
			return true;
	}

	#ifdef _DEBUG
	if (v.GetSize () == 1 && _tcstoul (v [0].m_strUID, NULL, 16) < 0x300) {
		TRACEF ("*** assuming master head for testing");
		return true;
	}
	#endif //_DEBUG

	for (int i = 0; i < v.GetSize (); i++) {
		HEADSTRUCT h = v [i];
		ULONG lAddr = _tcstoul (h.m_strUID, NULL, 16);

		if (lAddr == 0x300 || lAddr == 0x001)
			return h.m_bMasterHead ? true : false;
	}

	return false;
}

void CHeadConfigDlg::OnOK()
{
	CStringArray vAddress, vOverlap;
	bool bValid = true;
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	bool bValve = ISVALVE ();

	GetPrinterLines (m_db, m_lPrinterID, vLines); //GetLineRecords (m_db, vLines);
	m_bAddressConflict = IsAddressConflict (vAddress);

	if (vAddress.GetSize ()) {
		CString str = LoadString (IDS_ADDRESSCONFLICT);

		for (int i = 0; i < vAddress.GetSize (); i++) 
			str += vAddress [i] + _T ("\n");

		MsgBox (* this, str, NULL, MB_OK | MB_ICONINFORMATION);
		return;
	}

	if (bValve) {
		CStringArray v;

		SetLinked ();
		IsIndexConflict (v);

		if (v.GetSize ()) {
			CString str = LoadString (IDS_INDEXCONFLICT);

			for (int i = 0; i < v.GetSize (); i++) 
				str += v [i] + _T ("\n");

			MsgBox (* this, str, NULL, MB_OK | MB_ICONINFORMATION);
			return;
		}
	}

	IsOverlapConflict (vOverlap);

	if (vOverlap.GetSize ()) {
		CString str = LoadString (IDS_OVERLAPCONFLICT);

		for (int i = 0; i < vOverlap.GetSize (); i++) 
			str += vOverlap [i] + _T ("\n");

		MsgBox (* this, str, NULL, MB_OK | MB_ICONINFORMATION);
		return;
	}

#ifdef __WINPRINTER__

	// the "michael" check
	if (!IsPHC1Master ()) {
		MsgBox (* this, LoadString (IDS_PHC1MUSTBEMASTER), NULL, MB_OK | MB_ICONINFORMATION);
		return;
	}

#endif //__WINPRINTER__

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
		ULONG lLineID = vLines [nLine].m_lID;
		int nMaster = 0;

		GetLineHeads (m_db, lLineID, vHeads);

		for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
			HEADSTRUCT head = vHeads [nHead];

			if (head.m_bMasterHead) 
				nMaster++;
		}

		if (nMaster != 1 && vHeads.GetSize ()) {
			CString str;

			str.Format (LoadString (IDS_NOMASTERHEAD), vLines [nLine].m_strName);
			MsgBox (* this, str, NULL, MB_OK | MB_ICONINFORMATION);
			return;
		}
	}

	for (int i = 0; i < m_vDeleted.GetSize (); i++) {
		ULONG lID = m_vDeleted [i];
		int nFound;
		
		while ((nFound = Find (m_vAdded, lID)) != -1)
			m_vAdded.RemoveAt (nFound);

		while ((nFound = Find (m_vEdited, lID)) != -1)
			m_vEdited.RemoveAt (nFound);
	}
	
#ifdef __WINPRINTER__
	if (m_vAdded.GetSize ()) {
		CString str = LoadString (IDS_MUSTRESTARTNEWHEADS);

		if (MsgBox (* this, str, NULL, MB_OKCANCEL | MB_ICONINFORMATION) == IDCANCEL)
			return;

		m_bRestartTask = false;
	}

#endif

	FoxjetCommon::CEliteDlg::OnOK ();
}

/* TODO: rem
void CHeadConfigDlg::OnBeginLabelEditHeads (NMHDR* pNMHDR, LRESULT* pResult) 
{
	TV_DISPINFO * ptv = (TV_DISPINFO*)pNMHDR;
	
	LRESULT lResult = TRUE;

	if (CItem * p = (CItem *)ptv->item.lParam) 
		if (p->m_type == CItem::CARD) 
			lResult = 0;
		else if (p->m_type == CItem::PANEL) 
			lResult = 0;

	if (pResult)
		* pResult = lResult;
}

void CHeadConfigDlg::OnEndLabelEditHeads (NMHDR* pNMHDR, LRESULT* pResult) 
{
	CTreeCtrl & tree = GetTreeCtrl ();
	TV_DISPINFO * ptv = (TV_DISPINFO*)pNMHDR;
	
	LRESULT lResult = FALSE;

	if (CItem * p = (CItem *)ptv->item.lParam) {
		if ((ptv->item.mask & TVIF_TEXT)) {
			if (p->m_type == CItem::CARD) {
				HEADSTRUCT & head = * p->m_pHead;
				
				head.m_strName = ptv->item.pszText;

				if (UpdateHeadRecord (m_db, head)) 
					lResult = TRUE;
			}
			else if (p->m_type == CItem::PANEL) {
				PANELSTRUCT & panel = * p->m_pPanel;
				
				panel.m_strName = ptv->item.pszText;

				if (UpdatePanelRecord (m_db, panel)) 
					lResult = TRUE;
			}
		}
	}

	if (pResult)
		* pResult = lResult;
}

void CHeadConfigDlg::OnBegindrag(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CTreeCtrl & tree = GetTreeCtrl ();
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	
	if (!FoxjetDatabase::IsHighResDisplay () || ISVALVE ())
		return;

	if (CItem * p = (CItem *)tree.GetItemData (pNMTreeView->itemNew.hItem)) {
		if (p->m_type == CItem::CARD) {

			m_hitemDrop = NULL;

			if (m_pDragImage) {
				delete m_pDragImage;
				m_pDragImage = NULL;
			}

			if (m_pDragImage = tree.CreateDragImage (pNMTreeView->itemNew.hItem)) {
				CPoint pt = pNMTreeView->ptDrag;

				ClientToScreen (&pt);

				m_hitemDrag = pNMTreeView->itemNew.hItem;
				m_bDragging = TRUE;
				m_pDragImage->BeginDrag (0, CPoint (0, 0));
				m_pDragImage->DragEnter (&tree, pt);

				SetCapture();
			}
		}
	}

	if (pResult)
		* pResult = 0;
}

void CHeadConfigDlg::OnMouseMove(UINT nFlags, CPoint point) 
{
	CTreeCtrl & tree = GetTreeCtrl ();

	if (m_bDragging) {
		bool bTarget = false;
		POINT pt = point;
		UINT flags;
		
		::GetCursorPos (&pt);
		CImageList::DragMove (pt);

		tree.ScreenToClient (&pt);

		if (HTREEITEM hItem = tree.HitTest (pt, &flags)) {
			//CImageList::DragShowNolock (FALSE);

			tree.SelectDropTarget (hItem);

			if (CItem * p = (CItem *)tree.GetItemData (hItem)) {
				if (p->m_type == CItem::PANEL) {
					m_hitemDrop = hItem;
					//CImageList::DragShowNolock (TRUE);
					::SetCursor (m_hcurOriginal);
					bTarget = true;
				}
			}
		}

		if (!bTarget) {
			m_hitemDrop = NULL;
			::SetCursor (m_hcurNo);
		}
	}

	FoxjetCommon::CEliteDlg::OnMouseMove (nFlags, point);
}

void CHeadConfigDlg::OnLButtonUp (UINT nFlags, CPoint point) 
{
	CTreeCtrl & tree = GetTreeCtrl ();

	if (m_bDragging && m_pDragImage) {
		ULONG lHeadID = -1;

		m_pDragImage->DragLeave (&tree);
		m_pDragImage->EndDrag ();

		delete m_pDragImage;
		m_pDragImage = NULL;

		if (m_hitemDrop != NULL) {
			CItem * pFrom = (CItem *)tree.GetItemData (m_hitemDrag);
			CItem * pTo = (CItem *)tree.GetItemData (m_hitemDrop);
			
			if (pFrom && pTo) {
				if (pFrom->m_type == CItem::CARD && pTo->m_type == CItem::PANEL) {
					HEADSTRUCT head = * pFrom->m_pHead;
					ULONG lPanelID = pTo->m_pPanel->m_lID;

					if (head.m_lPanelID != lPanelID) {
						head.m_lPanelID = lPanelID;
						
						for (int i = 0; i < head.m_vValve.GetSize (); i++)
							head.m_vValve [i].m_lPanelID = lPanelID;

						if (UpdateHeadRecord (m_db, head)) 
							lHeadID = head.m_lID;
					}
				}
			}
		}

		ReleaseCapture();
		m_bDragging = false;
		tree.SelectDropTarget (NULL);

		if (lHeadID != -1) {
			m_lLastHeadID = lHeadID;
			FillHeadCtrl ();
		}
		else {
			tree.Invalidate ();
			tree.RedrawWindow ();
		}
	}

	::SetCursor (m_hcurOriginal);
}
*/

void CHeadConfigDlg::OnSync ()
{
#ifdef __IPPRINTER__
	using namespace FoxjetIpElements;

	SETTINGSSTRUCT s;
	DWORD dw = CSyncDlg::SYNC_FONTS | CSyncDlg::SYNC_MESSAGES;

	if (GetSettingsRecord (m_db, 0, CHeadDlg::m_strSyncKey, s))
		dw = _tcstoul (s.m_strData, NULL, 16);

	CSyncDlg dlg (dw, 0, this);

	if (dlg.DoModal () == IDOK) {
		dw = 0;
		dw |= dlg.m_bFonts		? CSyncDlg::SYNC_FONTS		: 0;
		dw |= dlg.m_bMessages	? CSyncDlg::SYNC_MESSAGES	: 0;

		s.m_lLineID = 0;
		s.m_strKey = CHeadDlg::m_strSyncKey;
		s.m_strData.Format (_T ("%p"), dw);

		if (!UpdateSettingsRecord (m_db, s))
			VERIFY (AddSettingsRecord (m_db, s));
	}

#endif //__IPPRINTER__
}

void CHeadConfigDlg::OnAddValve ()
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
	HEADSTRUCT head;
	VALVESTRUCT valve;
	bool bUnnamed = true;
	
	GetLineHeads (m_db, m_lLineID, vHeads);

	if (!vHeads.GetSize ()) {
		MsgBox (* this, LoadString (IDS_NOCARDSEXIST));
		return;
	}

	head = vHeads [0];

	if (CItem * pItem = GetCurSel ()) {
		if (pItem->m_type == CItem::CARD)
			head = * pItem->m_pHead;
		else if (pItem->m_type == CItem::VALVE_HEAD) {
			ULONG lHeadID = pItem->m_pValve->m_lCardID;
			int nIndex = CTask::Find (vHeads, lHeadID);
			
			if (nIndex != -1)
				head = vHeads [nIndex];
		}
	}

	if (head.GetChannels () >= 72) {
		MsgBox (* this, LoadString (IDS_VALVEHEADMAX));
		return;
	}


	{ // set default name
		CArray <VALVESTRUCT, VALVESTRUCT &> vAll;
		CArray <CPanel, CPanel &> vPanels;

		CPanel::GetPanels (m_db, m_lLineID, vPanels);

		for (int nPanel = 0; nPanel < vPanels.GetSize (); nPanel++) {
			CPanel & p = vPanels [nPanel];

			for (int nCard = 0; nCard < p.m_vCards.GetSize (); nCard++) 
				vAll.Append (p.m_vCards [nCard].m_vValve);
			
			vAll.Append (p.m_vValve);
		}

		for (int i = 1; bUnnamed; i++) {
			CString str;
			ULONG lID = -1;
			ULONG lAddr = _tcstoul (head.m_strUID, NULL, 16);
			int nIndex = i + (lAddr == 0x300 ? 0 : 8);

			str.Format (_T ("FJ%04d"), nIndex);

			for (int nHead = 0; nHead < vAll.GetSize (); nHead++) {
				VALVESTRUCT h = vAll [nHead];

				if (!h.m_strName.CompareNoCase (str)) {
					lID = h.m_lID;
					break;
				}
			}

			if (lID == -1) {
				bUnnamed = false;
				valve.m_strName = str;
			}
		}
	}

	valve.m_lCardID			= head.m_lID;
	valve.m_lID				= GetNextValveID (head); 
	valve.m_nIndex			= GetNextValveIndex (head);

	CValvePropDlg dlg (m_db, m_lLineID, m_units, head, valve, this);

	if (dlg.DoModal () == IDOK) {
		valve = dlg.m_valve;
		m_lLastValveID	= -1;
		m_lLastHeadID	= head.m_lID;

		GetHeadRecord (m_db, valve.m_lCardID, head);
		head.m_vValve.Add (valve);

		if (head.GetChannels () > 72) {
			MsgBox (* this, LoadString (IDS_VALVEHEADMAX));
			return;
		}

		VERIFY (UpdateHeadRecord (m_db, head));
		FillHeadCtrl ();
	}
}

LRESULT CHeadConfigDlg::OnSyncRemoteHead (WPARAM wParam, LPARAM lParam)
{
	if (CWnd * p = GetDlgItem (IDOK))
		p->EnableWindow (FALSE);

	if (wParam == -1) 
		GetPrinterHeads (m_db, m_lPrinterID, m_vSync); 

	TRACEF (_T ("OnSyncRemoteHead: ") + FoxjetDatabase::ToString ((int)wParam));

	return WM_SYNCREMOTEHEAD;
}


LRESULT CHeadConfigDlg::OnSyncComplete (WPARAM wParam, LPARAM lParam)
{
	for (int i = 0; i < m_vSync.GetSize (); i++) {
		if (m_vSync [i].m_lID == wParam) {
			m_vSync.RemoveAt (i);
			TRACEF (_T ("m_vSync.Remove: ") + FoxjetDatabase::ToString ((int)wParam));
			break;
		}
	}

	if (!m_vSync.GetSize ())
		if (CWnd * p = GetDlgItem (IDOK))
			p->EnableWindow (TRUE);

	return WM_SYNCREMOTEHEADCOMPLETE;
}

void CHeadConfigDlg::OnSelChanged (NMHDR* pNMHDR, LRESULT* pResult)
{
	NMTREEVIEW * p = (NMTREEVIEW *)pNMHDR;
	
	/*
	if (p) {
		#ifdef _DEBUG
		CString str;

		str.Format (_T ("\n")
			_T ("\thdr.hwndFrom: 0x%p\n")
			_T ("\thdr.idFrom:   %d\n")
			_T ("\thdr.code:     %d\n")
			_T ("\taction:       %d"), 
			_T ("\titemOld:      0x%p [%s]"), 
			_T ("\titemNew:      0x%p [%s]"), 
			_T ("\tptDrag:       %d, %d"),
			p->hdr.hwndFrom,
			p->hdr.idFrom,
			p->hdr.code,
			p->action,
			p->itemOld.hItem, p->itemOld.pszText, 
			p->itemNew.hItem, p->itemNew.pszText, 
			p->ptDrag.x, p->ptDrag.y);
		TRACEF (str);
		#endif //_DEBUG
	}
	*/

	if (pResult)
		* pResult = 0;
}

void CHeadConfigDlg::OnSelChanging (NMHDR* pNMHDR, LRESULT* pResult)
{
	NMTREEVIEW * p = (NMTREEVIEW *)pNMHDR;

	if (p) {
		/*
		#ifdef _DEBUG
		CString str;

		str.Format (_T ("\n")
			_T ("\thdr.hwndFrom: 0x%p\n")
			_T ("\thdr.idFrom:   %d\n")
			_T ("\thdr.code:     %d\n")
			_T ("\taction:       %d"), 
			_T ("\titemOld:      0x%p [%s]"), 
			_T ("\titemNew:      0x%p [%s]"), 
			_T ("\tptDrag:       %d, %d"),
			p->hdr.hwndFrom,
			p->hdr.idFrom,
			p->hdr.code,
			p->action,
			p->itemOld.hItem, p->itemOld.pszText, 
			p->itemNew.hItem, p->itemNew.pszText, 
			p->ptDrag.x, p->ptDrag.y);
		TRACEF (str);
		#endif //_DEBUG
		*/
	}

	if (pResult)
		* pResult = 0;
}

void CHeadConfigDlg::OnClickHeads(NMHDR* pNMHDR, LRESULT* pResult)
{
	#ifdef _DEBUG
	if (CItem * p = GetCurSel ()) {
		if (p->m_type == CItem::CARD) {
			ASSERT (p->m_pHead);
			HEADSTRUCT head = * p->m_pHead;
			TRACEF (head.m_strName);
		}
		else if (p->m_type == CItem::PANEL) {
			ASSERT (p->m_pPanel);
			PANELSTRUCT panel = * p->m_pPanel;
			TRACEF (panel.m_strName);
		}
		else if (p->m_type == CItem::VALVE_HEAD) {
			ASSERT (p->m_pValve);
			VALVESTRUCT valve = * p->m_pValve;
			HEADSTRUCT head = GetHeadByID (p->m_pValve->m_lCardID);
			TRACEF (head.m_strName);
		}
	}
	#endif //_DEBUG

	if (pResult)
		* pResult = 0;
}


void CHeadConfigDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if (nIDCtl == LB_HEADS) {
		CListBox & lb = GetHeadCtrl ();
		const bool bValve = ISVALVE ();
		DRAWITEMSTRUCT & dis = * lpDrawItemStruct;
		CDC dc;
		CRect rc = dis.rcItem;
		const UINT nCurSel = lb.GetCurSel ();
		CString str;

		dc.Attach (dis.hDC);

		if (dis.itemID != CB_ERR) {
			CDC dcMem;

			VERIFY (dcMem.CreateCompatibleDC (&dc));

			COLORREF crOldTextColor = dc.GetTextColor();
			COLORREF crOldBkColor = dc.GetBkColor();

			CxImage * pImg = NULL;
			int nTab = 0;

			if (CItem * p = (CItem *)dis.itemData) {
				switch (p->m_type) {
				case CItem::PANEL:	
					nTab = 1;
					str = p->m_pPanel->m_strName; 
					pImg = &m_imgPanel;
					break;
				case CItem::CARD:
					nTab = 2;
					str = p->m_pHead->m_strName; 

					if (!p->m_pHead->m_bEnabled)
						pImg = &m_imgDisabledHead;
					else if (p->m_pHead->m_bMasterHead)
						pImg = &m_imgMasterHead;
					else
						pImg = &m_imgHead;
					break;
				//case CItem::VALVE_HEAD:
				//	break;
				}
			}

			if (pImg) {
				HBITMAP hbmp = pImg->MakeBitmap (dc);
				HBITMAP hOld = (HBITMAP)::SelectObject (dcMem, hbmp);
				int cx = pImg->GetWidth ();
				int cy = pImg->GetHeight ();
				double dZoom = rc.Height () / (double)cy;
				dc.StretchBlt (rc.left + (cx * (nTab - 1)), rc.top, (int)(cx * dZoom), (int)(cy * dZoom), &dcMem, 0, 0, cx, cy, SRCCOPY);
				rc.left += (cx * nTab) + 10;
				::SelectObject (dcMem, hOld);
				::DeleteObject (hbmp);
			}

			if ((dis.itemAction | ODA_SELECT) && (dis.itemState & ODS_SELECTED)) {
				dc.SetTextColor (::GetSysColor (COLOR_HIGHLIGHTTEXT));
				dc.SetBkColor (::GetSysColor (COLOR_HIGHLIGHT));
				dc.FillSolidRect (rc, ::GetSysColor (COLOR_HIGHLIGHT));
			}
			else
				dc.FillSolidRect (rc, crOldBkColor);

			dc.DrawText (str, rc, DT_VCENTER | DT_SINGLELINE);

			if (dis.itemState & ODS_FOCUS) 
				dc.DrawFocusRect (rc);

			dc.SetTextColor(crOldTextColor);
			dc.SetBkColor(crOldBkColor);
		}

		dc.Detach ();
	}
	else
		FoxjetCommon::CEliteDlg::OnDrawItem(nIDCtl, lpDrawItemStruct);
}
