// 3d.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include <shlwapi.h>
#include <afxmt.h>
#include "3d.h"
#include "DllVer.h"
#include "Debug.h"
#include "Database.h"
#include "AppVer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//
static AFX_EXTENSION_MODULE _3dDLL = { NULL, NULL };
static HINSTANCE hInstance = NULL;

HINSTANCE GetInstanceHandle ()
{
	return ::hInstance;
}

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("_3d.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(_3dDLL, hInstance)) {
			MESSAGEBOX (_T ("AfxInitExtensionModule failed"));
			return 0;
		}

		// Insert this DLL into the resource chain
		// NOTE: If this Extension DLL is being implicitly linked to by
		//  an MFC Regular DLL (such as an ActiveX Control)
		//  instead of an MFC application, then you will want to
		//  remove this line from DllMain and put it in a separate
		//  function exported from this Extension DLL.  The Regular DLL
		//  that uses this Extension DLL should then explicitly call that
		//  function to initialize this Extension DLL.  Otherwise,
		//  the CDynLinkLibrary object will not be attached to the
		//  Regular DLL's resource chain, and serious problems will
		//  result.

		::hInstance = hInstance;
		new CDynLinkLibrary(_3dDLL);
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("3d.DLL Terminating!\n");
		// Terminate the library before destructors are called
		AfxTermExtensionModule(_3dDLL);
	}
	return 1;   // ok
}

void CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer)
{
	if (lpVer) {
		* lpVer = FoxjetCommon::verApp; // memcpy (lpVer, &FoxjetCommon::verApp, sizeof (FoxjetCommon::VERSIONSTRUCT));
	}
}

HRESULT CALLBACK DllGetVersion (DLLVERSIONINFO * pdvi)
{
	FoxjetCommon::VERSIONSTRUCT ver;

	fj_GetDllVersion (&ver);

	if (pdvi) {
		pdvi->dwMajorVersion	= ver.m_nMajor;
		pdvi->dwMinorVersion	= ver.m_nMinor;
		pdvi->dwBuildNumber		= ver.m_nInternal;
		pdvi->dwPlatformID		= DLLVER_PLATFORM_WINDOWS;
	}

	return NOERROR;
}

CString LoadString (UINT nID)
{
	return FoxjetDatabase::LoadString (nID, _T ("3d.dll"));
}

