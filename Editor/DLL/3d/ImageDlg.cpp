// ImageDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "3d.h"
#include "Image.h"
#include "ImageDlg.h"
#include "Debug.h"
#include "List.h"
#include "Utils.h"
#include "Database.h"
#include "MsgBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Foxjet3d;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////
// CImageDlg dialog


CImageDlg::CImageDlg(CWnd* pParent /*=NULL*/)
:	m_hThread (NULL),
	FoxjetCommon::CEliteDlg(IDD_IMAGE, pParent)
{
	//{{AFX_DATA_INIT(CImageDlg)
	m_strPath = _T("");
	m_nPanel = 0;
	//}}AFX_DATA_INIT

	m_bmpImage [0].LoadBitmap (IDB_STRETCH);
	m_bmpImage [1].LoadBitmap (IDB_TL);
	m_bmpImage [2].LoadBitmap (IDB_TR);
	m_bmpImage [3].LoadBitmap (IDB_BR);
	m_bmpImage [4].LoadBitmap (IDB_BL);
}


void CImageDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	
	DDX_Text(pDX, TXT_PANEL, m_nPanel);
	DDV_MinMaxInt (pDX, m_nPanel, 1, 6);
	DDX_Text(pDX, TXT_PATH, m_strPath);
	DDX_Radio (pDX, RDO_TL, (int &)m_lAttributes);

	if (pDX->m_bSaveAndValidate) {
		CImage img;

		for (int i = 0; i < m_vInUse.GetSize (); i++) {
			int nUsed = m_vInUse [i];

			if (m_nPanel == nUsed) {
				CString str;

				str.Format (LoadString (IDS_PANELEXISTS), m_nPanel);
				MsgBox (* this, str);
				pDX->PrepareEditCtrl (TXT_PANEL);
				pDX->Fail ();
			}
		}

		if (!img.Load (m_strPath, -1, -1)) {
			CString str;

			str.Format (LoadString (IDS_INVALIDPATH), m_strPath);
			MsgBox (* this, str);
			pDX->PrepareCtrl (TXT_PATH);
			pDX->Fail ();
		}
	}
	//{{AFX_DATA_MAP(CImageDlg)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CImageDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CImageDlg)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_BN_CLICKED(RDO_BL, OnAttributeClick)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(RDO_TL, OnAttributeClick)
	ON_BN_CLICKED(RDO_TR, OnAttributeClick)
	ON_BN_CLICKED(RDO_BR, OnAttributeClick)
	ON_BN_CLICKED(RDO_STRETCH, OnAttributeClick)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImageDlg message handlers

void CImageDlg::OnEdit() 
{
	DWORD dw;

	m_hThread = ::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0,
		(LPTHREAD_START_ROUTINE)WaitForEdit, (LPVOID)this, 0, &dw);
	ASSERT (m_hThread != NULL);
}

ULONG CALLBACK CImageDlg::WaitForEdit (LPVOID lpData)
{
	ASSERT (lpData);

	CImageDlg & dlg = * (CImageDlg *)lpData;
	CString strApp = FoxjetCommon::GetBitmapEditorPath ();
	CString strCmdLine = strApp + _T (" \"") + FoxjetDatabase::MakeAbsolutePath (dlg.m_strPath) + _T ("\"");
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [0xFF];

	_tcsncpy (szCmdLine, strCmdLine, 0xFF);
    memset (&pi, 0, sizeof(pi));
    memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	BOOL bResult = ::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi); 

	if (!bResult) {
		MsgBox (dlg, LoadString (IDS_FAILEDTOLANUCHEDITOR));
	}
	else {
		bool bMore;

		TRACEF (_T ("begin edit"));

		do {
			DWORD dwExitCode = 0;
		
			dlg.UpdateUI ();

			if (!GetExitCodeProcess (pi.hProcess, &dwExitCode))
				bMore = false;
			else {
				bMore = (dwExitCode == STILL_ACTIVE);
				WaitForSingleObjectEx(pi.hProcess, 3000, TRUE);
				TRACEF (_T ("still waiting"));
			}
		}
		while (bMore);

		TRACEF (_T ("edit complete"));
	}

	dlg.m_hThread = NULL;
	dlg.UpdateUI ();

	return 0;
}

void CImageDlg::OnBrowse() 
{
	CFileDialog dlg (TRUE, _T ("*.bmp"), FoxjetDatabase::MakeAbsolutePath (m_strPath), 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		GetFileDlgList (), //_T ("Bitmaps (*.bmp)|*.bmp||"), 
		this);

	if (dlg.DoModal () == IDOK) {
		CImage img;
		CString strPath = FoxjetDatabase::MakeRelativePath (dlg.GetPathName ());

		if (img.Load (strPath, -1, -1)) {
			m_strPath = strPath;
			SetDlgItemText (TXT_PATH, m_strPath);
		}
		else {
			CString str;
			str.Format (LoadString (IDS_INVALIDIMAGE), dlg.GetPathName ());
			MsgBox (* this, str);
		}
	}
}

void CImageDlg::OnAttributeClick() 
{
	CButton * pRadio [5] = 
	{
		(CButton *)GetDlgItem (RDO_STRETCH),
		(CButton *)GetDlgItem (RDO_TL),
		(CButton *)GetDlgItem (RDO_TR),
		(CButton *)GetDlgItem (RDO_BR),
		(CButton *)GetDlgItem (RDO_BL),
	};
	CStatic * pBmp = (CStatic *)GetDlgItem (IDC_IMAGE);

	ASSERT (pBmp);

	for (int i = 0; i < 5; i++) {
		ASSERT (pRadio [i]);
		
		if (pRadio [i]->GetCheck () == 1) 
			pBmp->SetBitmap (m_bmpImage [i]);
	}
}

void CImageDlg::OnOK()
{
	FoxjetCommon::CEliteDlg::OnOK ();
}

void CImageDlg::UpdateUI()
{
	UINT nID [] = 
	{
		BTN_BROWSE,
		BTN_EDIT,
		IDOK,
		IDCANCEL,
		-1
	};

	for (int i = 0; nID [i] != -1; i++)
		if (CWnd * p = GetDlgItem (nID [i]))
			p->EnableWindow (m_hThread ? FALSE : TRUE);
}

void CImageDlg::OnClose() 
{
	if (!m_hThread)
		FoxjetCommon::CEliteDlg::OnClose();
}
