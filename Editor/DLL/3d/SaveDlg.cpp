// SaveDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "SaveDlg.h"
#include "Debug.h"
#include "Database.h"
#include "Extern.h"
#include "Registry.h"
#include "ItiLibrary.h"
#include "Compare.h"
#include "Utils.h"
#include "Extern.h"
#include "Parse.h"
#include "Resource.h"

using namespace Foxjet3d;
using namespace ListGlobals;
using namespace ItiLibrary;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace FoxjetDocument;
using namespace ListGlobals;

static const CVersion verDLL = GetElementListVersion ();

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CMessageItem::CMessageItem (const CString & strName, 
							const CString & strDesc,
							ULONG lID)
:	m_strName (strName), 
	m_strDesc (strDesc),
	m_lID (lID)
{
}

CMessageItem::~CMessageItem ()
{
}

int CMessageItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	switch (nColumn) {
	case 0:		return m_strName.CompareNoCase (((CMessageItem &)rhs).m_strName);
	case 1:		return m_strDesc.CompareNoCase (((CMessageItem &)rhs).m_strDesc);
	}

	return 0;
}

CString CMessageItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:		return m_strName;
	case 1:		return m_strDesc;
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
//
CTaskItem::CTaskItem (const FoxjetDatabase::TASKSTRUCT * pTask)
:	m_pTask (pTask),
	CMessageItem (pTask->m_strName, pTask->m_strDesc, pTask->m_lID)
{
	ASSERT (pTask);
}

CTaskItem::~CTaskItem ()
{
/*
	if (m_pTask) {
		delete m_pTask;
		m_pTask = NULL;
	}
*/
}

/////////////////////////////////////////////////////////////////////////////
//
CSaveDlg::CTaskCache::CTaskCache ()
{
}

CSaveDlg::CTaskCache::~CTaskCache ()
{
	Free (m_vTasks);
}

/////////////////////////////////////////////////////////////////////////////
// CSaveDlg dialog
const int CSaveDlg::m_nTaskTab		= 0;
const int CSaveDlg::m_nMsgTab		= 1;

const int CSaveDlg::m_nDescIndex	= 1;
const int CSaveDlg::m_nIDIndex		= 2;


CSaveDlg::CSaveDlg(FoxjetDocument::CBaseDocTemplate * pTemplate, UINT nIDTemplate, CWnd* pParent)
:	m_pTemplate (pTemplate), 
	m_ver (::verDLL),
	m_lLineID (-1),
	FoxjetCommon::CEliteDlg(nIDTemplate, pParent)
{
}

CSaveDlg::CSaveDlg(FoxjetDocument::CBaseDocTemplate * pTemplate, CWnd* pParent /*=NULL*/)
:	m_pTemplate (pTemplate), 
	m_ver (::verDLL),
	m_lLineID (-1),
	FoxjetCommon::CEliteDlg (IDD_SAVE_MATRIX /* IsMatrix () ? IDD_SAVE_MATRIX : IDD_SAVE */ , pParent)
{
}
 
CSaveDlg::~CSaveDlg ()
{
	for (int i = 0; i < m_vCache.GetSize (); i++) {
		if (CTaskCache * p = m_vCache [i])
			delete p;
	}

	m_vCache.RemoveAll ();
}

void CSaveDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSaveDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSaveDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CSaveDlg)
	ON_NOTIFY(NM_DBLCLK, LV_MESSAGES, OnDblclkMessages)
	ON_NOTIFY(NM_DBLCLK, LV_TASKS, OnDblclkTasks)
	ON_NOTIFY(LVN_ITEMCHANGED, LV_TASKS, OnItemchangedTasks)
	ON_NOTIFY(LVN_ITEMCHANGED, LV_MESSAGES, OnItemchangedMessages)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB, OnSelchangeTab)
	ON_CBN_SELCHANGE(CB_LINE, OnSelchangeLine)
	ON_BN_CLICKED(CHK_VERSION, OnVersion)
	ON_COMMAND(ID_EDIT_SELECTALL, OnEditSelectall)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSaveDlg message handlers
BOOL CSaveDlg::OnInitDialog() 
{
	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;
	CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CComboBox * pVersion = (CComboBox *)GetDlgItem (CB_VERSION);
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB);
	int nSelTab = m_nTaskTab;
	int nFilenameCtrl = TXT_TASK;

	ASSERT (pLine);

	FoxjetCommon::CEliteDlg::OnInitDialog();

	if (pTab)
		pTab->InsertItem (m_nTaskTab, LoadString (IDS_MESSAGE));

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_NAME), 200)); // IsMatrix () ? 200 : 80));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_DESC), 300)); // IsMatrix () ? 300 : 120));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_ID), 0));

	m_lviTask.Create (LV_TASKS, vCols, this, defElements.m_strRegSection);

	ULONG lPrinterID = GetMasterPrinterID (ListGlobals::GetDB ()); //GetPrinterID (ListGlobals::GetDB ());

	GetPrinterHeads (ListGlobals::GetDB (), lPrinterID, vHeads); // sw0867 //GetHeadRecords (vHeads);
	GetPrinterLines (ListGlobals::GetDB (), lPrinterID, vLines); // sw0867 //GetLineRecords (vLines);

	for (int i = 0; i < vLines.GetSize (); i++) {
		const LINESTRUCT & line = vLines [i];
		int nIndex = pLine->AddString (line.m_strName);
		pLine->SetItemData (nIndex, (DWORD)line.m_lID);

		if (line.m_lID == m_lLineID)
			pLine->SetCurSel (nIndex);
	}

	if (pLine->GetCurSel () == CB_ERR && pLine->GetCount ())
		pLine->SetCurSel (0);
	
	OnSelchangeLine ();
	pLine->EnableWindow (FALSE);

	if (pTab) {
		pTab->SetCurSel (nSelTab); 
		OnSelchangeTab (NULL, NULL); 
		pTab->EnableWindow (FALSE);
	}

	if (m_vFiles.GetSize ()) {
		const CString strFile = m_vFiles [0];
		FoxjetFile::DOCTYPE type = FoxjetFile::GetFileType (strFile);

		if (type == FoxjetFile::UNKNOWNDOCTYPE)
			type = m_type;
	
		ASSERT (type != FoxjetFile::UNKNOWNDOCTYPE);
		
		if (!Select (strFile, type)) {
			const CString strTitle = FoxjetFile::GetFileTitle (strFile);
			TRACEF ("Failed to select: " + strFile);
			SetDlgItemText (nFilenameCtrl, strTitle);
		}
	}

	if (pVersion) {
		EnumVersions (m_vVer);

		for (int i = 0; i < m_vVer.GetSize (); i++) {
			const CVersion & ver = m_vVer [i];
			int nIndex = pVersion->AddString (ver.Format (false));
			
			pVersion->SetItemData (nIndex, i);

			if (ver.IsSynonym (::verDLL))
				pVersion->SetCurSel (nIndex);
		}

		OnVersion ();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSaveDlg::OnDblclkMessages(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if (pResult)
		* pResult = 0;
}

void CSaveDlg::OnDblclkTasks(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CLongArray sel = GetSelectedItems (m_lviTask.GetListCtrl ());

	if (sel.GetSize ()) 
		OnOK ();

	if (pResult)
		* pResult = 0;
}

void CSaveDlg::OnItemchangedTasks(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CLongArray sel = GetSelectedItems (m_lviTask.GetListCtrl ());

	if (sel.GetSize ()) {
		ULONG lID = _tcstoul (m_lviTask->GetItemText (sel [0], m_nIDIndex), NULL, 10);
		
		if (const TASKSTRUCT * pData = GetTask (GetSelectedLineID (), lID)) {
			SetDlgItemText (TXT_TASK, pData->m_strName);
			SetDlgItemText (TXT_TASKDESC, pData->m_strDesc);
		}
	}

	if (pResult)
		* pResult = 0;
}

void CSaveDlg::OnItemchangedMessages(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if (pResult)
		* pResult = 0;
}

void CSaveDlg::OnSelchangeTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	typedef struct 
	{
		CWnd * m_pCtrl;
		int m_nTab;
	} TABINDEXSTRUCT;
	TABINDEXSTRUCT ctrls [] =
	{
		{ GetDlgItem (CB_HEAD),				m_nMsgTab	},
		{ GetDlgItem (LBL_HEAD),			m_nMsgTab	},
		{ GetDlgItem (LV_MESSAGES),			m_nMsgTab	},
		{ GetDlgItem (LBL_MESSAGES),		m_nMsgTab	},
		{ GetDlgItem (LBL_MESSAGE),			m_nMsgTab	},
		{ GetDlgItem (TXT_MESSAGE),			m_nMsgTab	},
		{ GetDlgItem (LBL_MESSAGEDESC),		m_nMsgTab	},
		{ GetDlgItem (TXT_MESSAGEDESC),		m_nMsgTab	},
		
		{ GetDlgItem (LBL_LINE),			m_nTaskTab	},
		{ GetDlgItem (CB_LINE),				m_nTaskTab	},
		{ GetDlgItem (LV_TASKS),			m_nTaskTab	},
		{ GetDlgItem (LBL_TASKS),			m_nTaskTab	},
		{ GetDlgItem (LBL_TASK),			m_nTaskTab	},
		{ GetDlgItem (TXT_TASK),			m_nTaskTab	},
		{ GetDlgItem (LBL_TASKDESC),		m_nTaskTab	},
		{ GetDlgItem (TXT_TASKDESC),		m_nTaskTab	},
		{ GetDlgItem (TXT_TASKCOUNT),		m_nTaskTab	},
		
		{ NULL,								-1			},
	};

	if (CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB)) {
		int nTab = pTab->GetCurSel ();

		for (int i = 0; ctrls [i].m_nTab != -1 && ctrls [i].m_pCtrl; i++) {
			ASSERT (ctrls [i].m_pCtrl);
			ctrls [i].m_pCtrl->ShowWindow (nTab == ctrls [i].m_nTab ? 
				SW_SHOW : SW_HIDE);
		}
	}

	if (pResult)
		* pResult = 0;
}

const FoxjetDatabase::TASKSTRUCT * CSaveDlg::GetTask (ULONG lLineID, ULONG lID)
{
	if (const CTaskCache * p = GetTasks (lLineID)) {
		for (int i = 0; i < p->m_vTasks.GetSize (); i++)
			if (p->m_vTasks [i]->m_lID == lID)
				return p->m_vTasks [i];
	}

	return NULL;
}

const CSaveDlg::CTaskCache * CSaveDlg::GetTasks (ULONG lLineID)
{
	for (int i = 0; i < m_vCache.GetSize (); i++) 
		if (CTaskCache * p = m_vCache [i]) 
			if (p->m_lLineID == lLineID)
				return p;

	BeginWaitCursor ();
	
	CTaskCache * p = new CTaskCache ();

	p->m_lLineID = lLineID;
	COdbcDatabase & db = ListGlobals::GetDB ();
	GetTaskRecords (db, lLineID, p->m_vTasks);
	m_vCache.Add (p);

	EndWaitCursor ();

	return p;
}

bool CSaveDlg::DeleteCachedTask (ULONG lTaskID)
{
	for (int i = 0; i < m_vCache.GetSize (); i++) {
		if (CTaskCache * p = m_vCache [i]) {
			for (int nTask = 0; nTask < p->m_vTasks.GetSize (); nTask++) {
				if (p->m_vTasks [nTask]->m_lID == lTaskID) {
					p->m_vTasks.RemoveAt (nTask);
					return true;
				}
			}
		}
	}

	return false;
}

ULONG CSaveDlg::GetSelectedLineID () const
{ 
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	ASSERT (pLine);
	
	return (ULONG)pLine->GetItemData (pLine->GetCurSel ());
}

void CSaveDlg::OnSelchangeLine() 
{
	BeginWaitCursor ();

	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB);
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	ASSERT (pLine);
	ULONG lLineID = (ULONG)pLine->GetItemData (pLine->GetCurSel ());
	CString strTasks;
	SETTINGSSTRUCT s;
	CStringArray vTemplates;

	//ASSERT (lLineID != CB_ERR);
	const CTaskCache * pTasks = GetTasks (lLineID);
	m_lviTask.DeleteCtrlData ();

	ASSERT (pTasks);

	strTasks.Format (LoadString (IDS_TASKCOUNT), pTasks->m_vTasks.GetSize (), GetMaxTasks ());
	SetDlgItemText (TXT_TASKCOUNT, strTasks);

	GetSettingsRecord (ListGlobals::GetDB (), lLineID, _T ("Templates"), s);
	FromString (s.m_strData, vTemplates);

	for (int i = 0; i < pTasks->m_vTasks.GetSize (); i++) {
		TASKSTRUCT * p = pTasks->m_vTasks [i];

		m_lviTask->InsertItem (i, p->m_strName);
		m_lviTask->SetItemText (i, m_nDescIndex, p->m_strDesc);
		m_lviTask->SetItemText (i, m_nIDIndex, FoxjetDatabase::ToString (p->m_lID));

		if (Find (vTemplates, p->m_strName, false) != -1)
			m_lviTask->SetItemText (i, m_nDescIndex, _T ("[") + LoadString (IDS_TEMPLATE) + _T ("] ") + p->m_strDesc);

	}

	m_lviTask->SortItems (ItiLibrary::ListCtrlImp::CListCtrlImp::CompareFunc, 0);

	EndWaitCursor ();
}

bool CSaveDlg::Select(const CString &strFile, FoxjetFile::DOCTYPE type, bool bDeselectPrev)
{
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB);
	int nTab = pTab->GetCurSel ();
	ListCtrlImp::CListCtrlImp * pLCI = NULL;
	const CString strPrefix = FoxjetFile::GetFileHead (strFile);
	const CString strTitle = FoxjetFile::GetFileTitle (strFile);
	int nCbCtrl = 0;
	int nSelectedItem = -1;

	nTab = m_nTaskTab;
	pLCI = &m_lviTask;
	nCbCtrl = CB_LINE;

	if (pTab) {
		if (nTab != pTab->GetCurSel ()) {
			pTab->SetCurSel (nTab);
			OnSelchangeTab (NULL, NULL);
		}
	}

	CListCtrl & lv = pLCI->GetListCtrl ();
	CComboBox * pCB = (CComboBox *)GetDlgItem (nCbCtrl);
	ASSERT (pCB);
	ULONG lCurID = pCB->GetItemData (pCB->GetCurSel ());
	ULONG lNewID = pCB->GetItemData (pCB->FindStringExact (-1, strPrefix));

	if (lCurID == CB_ERR || lNewID == CB_ERR) 
		return false;

	if (lCurID != lNewID) {
		int nIndex = pCB->SelectString (-1, strPrefix);

		if (nIndex == CB_ERR)
			return false;

		if (type == FoxjetFile::MESSAGE) { 
		} 
		else if (type == FoxjetFile::TASK) 
			OnSelchangeLine ();
	}

	for (int i = 0; i < lv.GetItemCount (); i++) {
		ULONG lID = _tcstoul (lv.GetItemText (i, m_nIDIndex), NULL, 10);
		
		if (const TASKSTRUCT * p = GetTask (GetSelectedLineID (), lID)) {
			if (!p->m_strName.CompareNoCase (strTitle)) {
				nSelectedItem = i;
				break;
			}
		}
	}

	if (bDeselectPrev)
		SelectAll (lv, false); // clear selection
	
	if (nSelectedItem != -1)
		lv.SetItemState (nSelectedItem, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);

	if (type == FoxjetFile::MESSAGE) 
		OnItemchangedMessages (NULL, NULL);
	else if (type == FoxjetFile::TASK) 
		OnItemchangedTasks (NULL, NULL);

	return nSelectedItem != -1;
}

void CSaveDlg::OnOK()
{
	CComboBox * pVersion = (CComboBox *)GetDlgItem (CB_VERSION);
	CComboBox * pLine = (CComboBox * )GetDlgItem (CB_LINE);

	ASSERT (pLine);
	ASSERT (pVersion);

	{ // TODO: this is a hack
		if (CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB)) {
			if (pTab->GetCurSel () == m_nMsgTab) {
				MsgBox (* this, _T ("Cannot save messages stand-alone"));
				return;
			}
		}
	}

	if (GetSelectedFilenames (m_vFiles, m_type)) {
		if (m_vFiles.GetSize ()) {
			CString strFile = m_vFiles [0];
			CString strTitle = FoxjetFile::GetFileTitle (strFile);

			if (!FoxjetCommon::IsValidFileTitle (strTitle)) {
				MsgBox (* this, LoadString (IDS_INVALIDFILETITLE));
				return;
			}

			bool bOverwrite = 
				m_pTemplate->IsExistingDoc (strFile) && 
				!m_pTemplate->GetOpenDoc (strFile);

			if (bOverwrite) {
				CString str;
				CString strTitle = FoxjetFile::GetFileTitle (strFile);
				CString strPrefix = FoxjetFile::GetFileHead (strFile);
				
				str.Format (LoadString (IDS_FILECLOSEDALREADYEXISTS),
					strTitle, strPrefix);
				int nResult = MsgBox (* this, str, LoadString (IDS_CONFIRM), MB_YESNO);
				
				if (nResult == IDNO)
					return;
			}

			int nIndex = pVersion->GetItemData (pVersion->GetCurSel ());

			if (nIndex >= 0 && nIndex < m_vVer.GetSize ()) {
				const CVersion & ver = m_vVer [nIndex];

				if (!m_ver.IsSynonym (ver)) {
					if (MsgBox (* this, LoadString (IDS_SAVEDIFFERENTVERSION), MB_OKCANCEL) == IDCANCEL)
						return;

					m_ver = ver;
				}
			}

			GetDlgItemText (TXT_TASKDESC, m_strDesc);
			m_lLineID = pLine->GetItemData (pLine->GetCurSel ());

			FoxjetCommon::CEliteDlg::OnOK ();
		}
		else
			MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
	}
}

bool CSaveDlg::GetSelectedFilenames (CStringArray & vFiles, FoxjetFile::DOCTYPE & type) 
{
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB);
	int nTab = m_nTaskTab;
	UINT nFilenameCtrl;
	CComboBox * pCB;
	CString strPrefix, strTitle;

	vFiles.RemoveAll ();

	if (pTab)
		pTab->GetCurSel ();

	switch (nTab) {
	case m_nMsgTab:
		nFilenameCtrl = TXT_MESSAGE;
		pCB = (CComboBox * )GetDlgItem (CB_HEAD);
		type = FoxjetFile::MESSAGE;
		break;
	case m_nTaskTab:
		nFilenameCtrl = TXT_TASK;
		pCB = (CComboBox * )GetDlgItem (CB_LINE);
		type = FoxjetFile::TASK;
		break;
	default:
		ASSERT (0);
	}

	ASSERT (pCB);

	if (pCB->GetLBTextLen (pCB->GetCurSel ()) > 0)
		pCB->GetLBText (pCB->GetCurSel (), strPrefix);

	GetDlgItemText (nFilenameCtrl, strTitle);
	
	if (strTitle.GetLength ()) {
		const CString strSuffix = FoxjetFile::GetFileExt (type);
		const CString strFile = strPrefix 
			+ _T ("\\") + strTitle 
			+ _T (".") + strSuffix;

		vFiles.Add (strFile);
		return true;
	}

	return false;
}

void CSaveDlg::OnVersion() 
{
	CButton * pChk = (CButton *)GetDlgItem (CHK_VERSION);
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_VERSION);

	ASSERT (pChk);
	ASSERT (pCB);

	bool bEnable = pChk->GetCheck () == 1;

	pCB->EnableWindow (bEnable);

	if (!bEnable) 
		int nSel = pCB->SelectString (-1, verDLL.Format (false));
}

CString CSaveDlg::GetSelectedLine () const
{
	CComboBox * pCB = (CComboBox * )GetDlgItem (CB_LINE);
	CString str;

	ASSERT (pCB);

	if (pCB->GetLBTextLen (pCB->GetCurSel ()) > 0)
		pCB->GetLBText (pCB->GetCurSel (), str);

	return str;
}

BOOL CSaveDlg::PreTranslateMessage(MSG* pMsg) 
{
	static HACCEL hAccel = NULL;
	
	if (!hAccel)
		VERIFY (hAccel = ::LoadAccelerators (GetInstanceHandle (), MAKEINTRESOURCE (IDA_SAVEDLG)));

	if (::TranslateAccelerator (m_hWnd, hAccel, pMsg))
		return TRUE;
	
	return FoxjetCommon::CEliteDlg::PreTranslateMessage(pMsg);
}

void CSaveDlg::OnEditSelectall() 
{
	m_lviTask.SelectAll ();
}
