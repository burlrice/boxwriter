#if !defined(AFX_OPENDLG_H__049B081E_CAC9_4610_926B_B9073E1ECD21__INCLUDED_)
#define AFX_OPENDLG_H__049B081E_CAC9_4610_926B_B9073E1ECD21__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OpenDlg.h : header file
//

#include "RawBitmap.h"
#include "SaveDlg.h"
#include "Box.h"
#include "3dApi.h"

/////////////////////////////////////////////////////////////////////////////
// COpenDlg dialog

namespace Foxjet3d
{
	namespace OpenDlg
	{
		class _3D_API CTaskPreview
		{
		public:
			CTaskPreview (const FoxjetDatabase::TASKSTRUCT & task, CDC & dc, const CRect & rcPanel, const CMapStringToString & mapUser,
				FoxjetDatabase::BOXSTRUCT & box, 
				const FoxjetDatabase::LINESTRUCT & line, 
				const CArray <HEADSTRUCT, HEADSTRUCT &> & vHeads,
				const CArray <PANELSTRUCT, PANELSTRUCT &> & vPanels,
				const CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> & vBounds,
				FoxjetDatabase::COdbcDatabase * pdb = NULL);
			CTaskPreview (const FoxjetDatabase::TASKSTRUCT & task, CDC & dc, const CRect & rcPanel, const CMapStringToString & mapUser);
			virtual ~CTaskPreview ();

			void Draw (const FoxjetDatabase::TASKSTRUCT & task, CDC & dc, const CRect & rcPanel, const CMapStringToString & mapUser,
				FoxjetDatabase::BOXSTRUCT & box, 
				const FoxjetDatabase::LINESTRUCT & line, 
				const CArray <HEADSTRUCT, HEADSTRUCT &> & vHeads,
				const CArray <PANELSTRUCT, PANELSTRUCT &> & vPanels,
				const CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> & vBounds,
				FoxjetDatabase::COdbcDatabase * pdb = NULL);

			CSize GetSize (const CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vHeads, 
				const CArray <FoxjetDatabase::PANELSTRUCT, FoxjetDatabase::PANELSTRUCT &> & vPanels,
				const FoxjetDatabase::BOXSTRUCT & box, const FoxjetDatabase::COrientation & orient);

			FoxjetDatabase::TASKSTRUCT			m_task;
			FoxjetCommon::CRawBitmap			m_bmp [6];
			CMap <ULONG, ULONG, 
				FoxjetCommon::CRawBitmap *, 
				FoxjetCommon::CRawBitmap *>		m_map;

			void Blt (CDC & dcPanel, const CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & vHeads, 
				const FoxjetDatabase::VALVESTRUCT & valve);
			CSize Blt (CDC & dcPanel, const FoxjetDatabase::HEADSTRUCT & head);
		};

		class _3D_API COpenDlg : public CSaveDlg
		{
		// Construction
		public:
			COpenDlg(CWnd* pParent = NULL);   // standard constructor
			COpenDlg(UINT nIdTemplate, CWnd* pParent = NULL);   // standard constructor
			virtual ~COpenDlg ();

		// Dialog Data
			//{{AFX_DATA(COpenDlg)
			BOOL	m_bReadOnly;
			BOOL	m_bPreview;
			int		m_nPanel;
			//}}AFX_DATA

		// Overrides
			// ClassWizard generated virtual function overrides
			//{{AFX_VIRTUAL(COpenDlg)
			protected:
			virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
			//}}AFX_VIRTUAL

		// Implementation
		protected:

			static CTaskPreview * Find (const FoxjetDatabase::TASKSTRUCT & task, CDC & dc, CWnd * pWnd);

			CSize CalcFitSize (const CDC & dc, const CRect & rcObj, const CRect & rcWin);
			bool IsPreviewEnabled () const;
			void InvalidatePreview ();
			CStatic m_wndPreview;
			virtual bool GetSelectedFilenames (CStringArray & vFiles, FoxjetFile::DOCTYPE & type);
			virtual void OnOK();
			afx_msg void OnItemchangedTasks(NMHDR* pNMHDR, LRESULT* pResult);
			afx_msg void OnSelchangeLine();
			afx_msg void OnSelchangePanel ();

			FoxjetCommon::CLongArray m_selLast;
			CArray <HeadConfigDlg::CPanel, HeadConfigDlg::CPanel &> m_vBounds;
			ULONG m_lLastLineID;

			// Generated message map functions
			//{{AFX_MSG(COpenDlg)
			virtual BOOL OnInitDialog();
			afx_msg void OnPreview();
			afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
			afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
			afx_msg void OnPanelClicked();
			//}}AFX_MSG
			DECLARE_MESSAGE_MAP()
		};
	}; //namespace OpenDlg
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPENDLG_H__049B081E_CAC9_4610_926B_B9073E1ECD21__INCLUDED_)
