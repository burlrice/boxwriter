// InvalidElementsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "InvalidElementsDlg.h"
#include "List.h"
#include "Resource.h"
#include "Extern.h"

using namespace FoxjetCommon;
using namespace ListGlobals;
using namespace InvalidElementsDlg;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CItem::CItem (const CString & strHead, ULONG lID, int nType, const CString & strData, const CString & strError)
:	m_strHead (strHead),
	m_strError (strError),
	m_strData (strData)
{
	if (lID != -1)
		m_strID.Format (_T ("%d"), lID);

	if (IsValidElementType (nType, FoxjetDatabase::GetDefaultHead ()))
		m_strType = GetElementResStr (nType);
}

CItem::CItem (const CItem & rhs)
:	m_strHead (rhs.m_strHead),
	m_strID (rhs.m_strID),
	m_strType (rhs.m_strType),
	m_strError (rhs.m_strError),
	m_strData (rhs.m_strData)
{
}

CItem & CItem::operator = (const CItem & rhs)
{
	if (this != &rhs) {
		m_strHead	= rhs.m_strHead;
		m_strID		= rhs.m_strID;
		m_strType	= rhs.m_strType;
		m_strError	= rhs.m_strError;
		m_strData	= rhs.m_strData;
	}

	return * this;
}

CItem::~CItem ()
{
}

CString CItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:	return m_strHead;
	case 1:	return m_strID;
	case 2:	return m_strType;
	case 3:	return m_strError;
	case 4:	return m_strData;
	}

	return _T ("");
}


/////////////////////////////////////////////////////////////////////////////
// CInvalidElementsDlg dialog


CInvalidElementsDlg::CInvalidElementsDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg(IDD_INVALIDELEMENTS, pParent)
{
	//{{AFX_DATA_INIT(CInvalidElementsDlg)
	m_strLine = _T("");
	m_strTask = _T("");
	//}}AFX_DATA_INIT
}


void CInvalidElementsDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInvalidElementsDlg)
	DDX_Text(pDX, TXT_LINE, m_strLine);
	DDX_Text(pDX, TXT_TASK, m_strTask);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInvalidElementsDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CInvalidElementsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInvalidElementsDlg message handlers

BOOL CInvalidElementsDlg::OnInitDialog() 
{
	using namespace ItiLibrary;

	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;

	FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_HEAD)));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_ID)));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_TYPE)));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_ERROR)));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_DATA), 400));

	m_lv.Create (LV_ELEMENTS, vCols, this, defElements.m_strRegSection);	

	for (int i = 0; i < m_vErrors.GetSize (); i++) 
		m_lv.InsertCtrlData (new CItem (m_vErrors [i]), i);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
