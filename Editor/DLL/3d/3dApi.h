#ifndef __3DAPI_H__
#define __3DAPI_H__

#ifdef __BUILD_3D__
	#define _3D_API __declspec(dllexport)
#else
	#define _3D_API __declspec(dllimport)
#endif //__BUILD_3D__

#endif //#ifndef __3DAPI_H__
