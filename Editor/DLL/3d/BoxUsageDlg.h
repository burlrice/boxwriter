#if !defined(AFX_BOXUSAGEDLG_H__3ABDBF8D_4A72_4FF8_96C6_AF5C390B0A7F__INCLUDED_)
#define AFX_BOXUSAGEDLG_H__3ABDBF8D_4A72_4FF8_96C6_AF5C390B0A7F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BoxUsageDlg.h : header file
//

#include "3dApi.h"
#include "ListCtrlImp.h"
#include "Database.h"
#include "CopyArray.h"
#include "List.h"
#include "Database.h"
#include "Utils.h"

namespace Foxjet3d
{
	/////////////////////////////////////////////////////////////////////////////
	// CBoxUsageDlg dialog
	namespace BoxUsageDlg
	{
		class _3D_API CBoxItem : public ItiLibrary::ListCtrlImp::CItem
		{ 
		public:
			CBoxItem (UNITS units);
			CBoxItem (const FoxjetDatabase::BOXSTRUCT & rhs, UNITS units);
			virtual ~CBoxItem ();

			virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
			virtual CString GetDispText (int nColumn) const;

			FoxjetDatabase::BOXSTRUCT m_box;
			UNITS m_units;
		};

		class _3D_API CBoxUsageDlg : public FoxjetCommon::CEliteDlg
		{
		public:
			CBoxUsageDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent = NULL);

			UNITS m_units;
			ULONG m_lLineID;

			static bool IsUsed (FoxjetDatabase::COdbcDatabase & db, ULONG lBoxID, ULONG lLineID);
			static bool SetUsed (FoxjetDatabase::COdbcDatabase & db, ULONG lBoxID, ULONG lLineID, bool bUsed);

		// Dialog Data
			//{{AFX_DATA(CBoxUsageDlg)
				// NOTE: the ClassWizard will add data members here
			//}}AFX_DATA

		// Overrides
			// ClassWizard generated virtual function overrides
			//{{AFX_VIRTUAL(CBoxUsageDlg)
		protected:
			virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
			virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
		//}}AFX_VIRTUAL

		// Implementation
		protected:
			virtual void OnCancel ();
			virtual void OnOK();
			void UpdateUI ();
			void SetUsed (const FoxjetCommon::CLongArray & sel);
			void SetAvailible (const FoxjetCommon::CLongArray & sel);

			ULONG GetSelectedLine () const;
			void InitBoxCtrls (ULONG lLineID);
			void InitLineCtrl();

			FoxjetDatabase::COdbcDatabase & m_db;
			ItiLibrary::ListCtrlImp::CListCtrlImp m_lviAvail;
			ItiLibrary::ListCtrlImp::CListCtrlImp m_lviInuse;
			CArray <FoxjetDatabase::BOXSTRUCT, FoxjetDatabase::BOXSTRUCT &> m_vBoxes;

			// Generated message map functions
			//{{AFX_MSG(CBoxUsageDlg)
			afx_msg void OnUnuse();
			afx_msg void OnUnuseall();
			afx_msg void OnUse();
			afx_msg void OnUseall();
			afx_msg void OnSelchangeLine();
			afx_msg void OnDblclkInuse(NMHDR* pNMHDR, LRESULT* pResult);
			afx_msg void OnDblclkAvailible(NMHDR* pNMHDR, LRESULT* pResult);
			virtual BOOL OnInitDialog();
			afx_msg void OnHelp ();
			//}}AFX_MSG
			DECLARE_MESSAGE_MAP()
		};
	}; //namespace BoxUsageDlg
}; //namespace Foxjet3d


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BOXUSAGEDLG_H__3ABDBF8D_4A72_4FF8_96C6_AF5C390B0A7F__INCLUDED_)
