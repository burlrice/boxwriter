// Element.h: interface for the CElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ELEMENT_H__40AC74DD_3604_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_ELEMENT_H__40AC74DD_3604_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afx.h>
#include "ProdArea.h"
#include "BaseElement.h"
#include "BaseView.h"
#include "3dApi.h"
#include "CopyArray.h"
#include "Callbacks.h"
#include "Database.h"
#include "Color.h"
#include "Utils.h"
#include "RawBitmap.h"

// causes the WM_NCCALCSIZE message to be sent
#define NCINVALIDATE(p)	if (p) (p)->SetWindowPos (NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER); 

bool operator == (const CFont & lhs, const CFont & rhs);
bool operator != (const CFont & lhs, const CFont & rhs);

class CEditorElementList;

namespace Element
{
	class _3D_API CElement;

	class _3D_API CPair
	{
	public:
		CPair ();
		CPair (CElement * pElement, CEditorElementList * pList, const FoxjetDatabase::HEADSTRUCT * pWorkingHead);
		CPair (const CPair & rhs);
		CPair & operator = (const CPair & rhs);
		virtual ~CPair ();

		operator const CElement & () const { ASSERT (m_pElement); return * m_pElement; }
		operator CElement & () { ASSERT (m_pElement); return * m_pElement; }

		operator const CEditorElementList & () const { ASSERT (m_pList); return * m_pList; }
		operator CEditorElementList & () { ASSERT (m_pList); return * m_pList; }

		CRect GetWindowRect (const FoxjetDatabase::HEADSTRUCT & workingHead) const;
		CString GetString () const;
		UINT GetElementID () const;

		CElement *				m_pElement;
		CEditorElementList *	m_pList;

	private:
		CString	m_str;
		CRect	m_rc;
		UINT	m_nElementID;
	};
	
	typedef FoxjetCommon::CCopyArray <CPair, CPair &> CPairArray;
	typedef FoxjetCommon::CCopyArray <CElement *, CElement *> CElementPtrArray;

	class _3D_API CElement : public CObject
	{
		DECLARE_DYNAMIC (CElement)

		friend class CElementTracker;

	#ifdef _DEBUG
		friend class FoxjetDocument::CBaseView;
	#endif //_DEBUG

	public:

		static int Find (const CPairArray & v, const CPair & p);
		static bool SpansMultipleHeads (const CPairArray & v, ULONG lActiveHeadID = -1);

		static CRect GetValveRect (const CRect & rc, const FoxjetDatabase::HEADSTRUCT & card, double dZoom);
		void Draw (CDC & dcElement, const FoxjetDatabase::HEADSTRUCT & workingHead, CEditorElementList * pList,
			CDC & dc, int nXDest, int nYDest, int nDestWidth, int nDestHeight, 
			int nSrcWidth, int nSrcHeight, double dZoom);

	public:
		CElement (FoxjetCommon::CBaseElement * pElement);
		CElement (const CElement & rhs);
		CElement & operator = (const CElement & rhs);
		virtual ~CElement ();

		inline const FoxjetCommon::CBaseElement * GetElement () const;
		inline FoxjetCommon::CBaseElement * GetElement ();

	protected:
		CElement (); // not implemented

		bool SetWindowRect (const CRect & rc, const FoxjetDatabase::HEADSTRUCT & head,
			const FoxjetDatabase::HEADSTRUCT & workingHead);
		void SetSelected (bool bSelect, const CRect & rc, const FoxjetDatabase::HEADSTRUCT & head,
			const FoxjetDatabase::HEADSTRUCT & workingHead);

		FoxjetCommon::CBaseElement * m_pElement;

	public:
		bool GetHandles (CRect * pHandles, const FoxjetDatabase::HEADSTRUCT & head, const FoxjetDatabase::HEADSTRUCT & workingHead) const;
		bool Confine (CDC & dc, const CRect rcHead [2], double dZoom, 
			const FoxjetDatabase::HEADSTRUCT & head, const FoxjetDatabase::HEADSTRUCT & workingHead);

		void SetSelected (bool bSelect, const FoxjetDocument::CBaseView * pView, 
			const CEditorElementList & list, const FoxjetDatabase::HEADSTRUCT & workingHead);
		CRect GetAbsoluteRect (const FoxjetDatabase::HEADSTRUCT & head, 
			const FoxjetDatabase::HEADSTRUCT & workingHead, const CSize & panel) const;
		void RecalcSize (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head);
		CRect GetWindowRect (const FoxjetDatabase::HEADSTRUCT & head, const FoxjetDatabase::HEADSTRUCT & workingHead) const;
		static CRect GetWindowRect (const FoxjetCommon::CBaseElement & e, const FoxjetDatabase::HEADSTRUCT & head, const FoxjetDatabase::HEADSTRUCT & workingHead);

		inline bool SetPos (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT & head);
		inline CPoint GetPos (const FoxjetDatabase::HEADSTRUCT & head) const;
		inline bool SetXPos(const CPoint &pt, const FoxjetDatabase::HEADSTRUCT & head);
		inline bool SetYPos(const CPoint &pt, const FoxjetDatabase::HEADSTRUCT & head);

		virtual void Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & workingHead,
			const CEditorElementList * pList = NULL, 
			FoxjetCommon::DRAWCONTEXT context = FoxjetCommon::EDITOR,
			const CPoint & pt = CPoint (0, 0));
		void DrawTracker (CDC & dc, const CRect & rc, const CPoint & ptOrg, const FoxjetDatabase::HEADSTRUCT & head);
		bool SetTrackerRect (const CRect & rc);
		bool GetTrackerRect (CRect & rc) const;

		inline bool IsResizable () const;
		inline bool IsMovable () const;
		inline bool IsSelected () const;
		inline bool IsLockAspectRatio () const;
		void SetResizable (bool bResizable);
		void SetMovable (bool bMovable);

		bool Track (FoxjetDocument::CBaseView * pParent, const CPoint & ptFrom, const CRect rcHead [2],
			const CPairArray & v, LPSETLOCACTIONFCT lpLocationFct, FoxjetDatabase::HEADSTRUCT & workingHead);
		int HitTest (const CPoint & pt) const;

		HCURSOR GetCursor (const CPoint & pt) const;
		static HCURSOR GetCursor (int nHit);

		inline CString GetFontName () const;
		bool SetFontName (const CString & str);

		inline UINT GetFontSize () const;
		bool SetFontSize (int nSize);

		inline int GetWidth () const;
		bool SetWidth (int nSize);

		inline bool IsFontBold () const;
		bool SetFontBold (bool bBold);
	
		inline bool IsFontItalic () const;
		bool SetFontItalic (bool bItalic);

		FoxjetCommon::CRawBitmap * GetCache () const { return m_pBmp; }

		static double CalcXStretch (const FoxjetDatabase::HEADSTRUCT & head, const FoxjetDatabase::HEADSTRUCT & workingHead);

		static const CPoint m_ptError;
	
	protected:
		static void CopyBackground (CDC & dcScreen, CDC & dcMem, const CRect & rc, double dZoom, DWORD dwRop = SRCCOPY);

		CElementTracker * m_pTracker;
		FoxjetCommon::CRawBitmap * m_pBmp;
	};

	enum Handles { 
		TL = 0,	// top-left
		T,		// top
		TR,		// top-right
		R,		// right
		BR,		// bottom-right
		B,		// bottom
		BL,		// bottom-left
		L		// left
	};

	class _3D_API CElementTracker : public CRectTracker  
	{
	friend class CElement;

	public:
		CElementTracker (const FoxjetDatabase::HEADSTRUCT & head, 
			const FoxjetDatabase::HEADSTRUCT & workingHead, CElement * pElement = NULL);
		CElementTracker (const FoxjetDatabase::HEADSTRUCT & head, 
			const FoxjetDatabase::HEADSTRUCT & workingHead, LPCRECT lpSrcRect, 
			UINT nStyle = 0, CElement * pElement = NULL);
		CElementTracker (const CElementTracker & rhs);
		CElementTracker & operator = (const CElementTracker & rhs);

		BOOL Track (FoxjetDocument::CBaseView * pActiveView, const CPoint & ptWhere, 
			const CRect rcHead [2], LPSETLOCACTIONFCT lpLocationFct,
			BOOL bAllowInvert = FALSE, CWnd * pWndClipTo = NULL);
		BOOL TrackRubberBand (FoxjetDocument::CBaseView * pActiveView, CPoint point, BOOL bAllowInvert = TRUE);
		virtual void DrawTrackerRect (LPCRECT lpRect, CWnd * pWndClipTo, CDC * pDC, CWnd * pWnd);
		int HitTest (CPoint point);
		virtual void AdjustRect (int nHandle, LPRECT lpRect);

		void Draw (CDC * pDC) const;
		void CalcHandles (CRect * pHandles, const CRect & rcFrom) const;
		void CalcHandles ();
		CRect GetHandle (int nIndex);
		bool IsResizable () const { return m_bResizable; }
		bool IsMovable () const { return m_bMovable; }
		bool IsLockAspectRatio () const { return m_bLockAspectRatio; }
		void SetResizable (bool bResizable);
		void SetMovable (bool bMovable) { m_bMovable = bMovable; }
		void SetLockAspectRatio (bool b) { m_bLockAspectRatio = b; } 
		void SetRect (const CRect & rc) { m_rect = rc; }
		CSize GetSize () const;

		static CSize GetHandleSize ();
		static bool SetHandleSize (const CSize & size);

		CPairArray m_vSelected;
		int m_nLeadIndex;

		static const int m_nDragHandleSize;
		static const int CElementTracker::m_nHandleIndex [8];

		static const FoxjetCommon::LIMITSTRUCT m_lmtHandle;

	protected:
		void DrawSelectionRect (CDC & dc, const CRect & rc) const;
		void Draw (CDC * pDC, const CRect & rc) const;

		CRect CalcResizeRect (const CPairArray & vSelected, 
			const CPoint & ptScreenClick, const CPoint & ptOffset,
			const CRect & rcClip, const CRect & rcProductArea, 
			int nHit, double dZoom) const;
		CSize CalcMinSize (const CPairArray & vSelected) const;
		CRect CalcClipRect (const CPairArray & vSelected, 
			const CPoint & ptClick, int nHit) const;
		virtual void OnChangedRect (const CRect& rectOld);
		CRect CalcNewRect (const CRect & rcCurrent, const CRect & rcFrom,
			const CRect & rcLead, bool bResize, double dZoom) const;
		static CRect ClipToDesktop (const CRect & rc);


		FoxjetDocument::CBaseView * m_pActiveView;
		CRect m_rcHandles [8];
		CRect m_rcLastFocus;
		CRect m_rcClip;
		CPoint m_ptScroll;
		CRect m_rcHead [2];
		CPoint m_ptClick;
		CRect m_rcOriginal;
		CRect m_rcLockAspectRatio;
		int m_nHit;
		bool m_bResizing;
		bool m_bResizable;
		bool m_bMovable;
		bool m_bRubberBand;
		bool m_bLockAspectRatio;
		CElement * m_pElement;
		LPSETLOCACTIONFCT m_lpLocationFct;
		FoxjetDatabase::HEADSTRUCT m_head;
		FoxjetDatabase::HEADSTRUCT m_workingHead;
	};
};

#ifndef __ELEMENT_INLINE__
	#define __ELEMENT_INLINE__
	#include "Element.inl"
#endif //__ELEMENT_INLINE__


#endif // !defined(AFX_ELEMENT_H__40AC74DD_3604_11D4_8FC6_006067662794__INCLUDED_)
