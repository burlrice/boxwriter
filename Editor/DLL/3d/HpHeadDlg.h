#if !defined(AFX_HPHEADDLG_H__7BF5EFFE_6B9C_416D_91CA_5BBBC8DE6436__INCLUDED_)
#define AFX_HPHEADDLG_H__7BF5EFFE_6B9C_416D_91CA_5BBBC8DE6436__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HpHeadDlg.h : header file
//

#include "Database.h"
#include "List.h"
#include "3dApi.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CHpHeadDlg dialog

namespace Foxjet3d
{
	class _3D_API CHpHeadDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CHpHeadDlg(FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::HEADSTRUCT & head, 
			UNITS units, const CMapStringToString & vAvailAddressMap, CWnd* pParent = NULL);

	// Dialog Data
		//{{AFX_DATA(CHpHeadDlg)
		//}}AFX_DATA

		FoxjetDatabase::HEADSTRUCT m_head;
		bool m_bReadOnly;
		UNITS m_units;

		static const FoxjetCommon::LIMITSTRUCT m_lmtPhotocellRate;
		static const FoxjetCommon::LIMITSTRUCT m_lmtPhotocellDelay;
		static const FoxjetCommon::LIMITSTRUCT m_lmtIntTachSpeed;

		static int CalcMaxLineSpeed (const FoxjetDatabase::HEADSTRUCT & head);

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CHpHeadDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual void OnOK();
		void UpdateUI ();

		afx_msg void OnPortProperties ();
		afx_msg void OnDirection ();

		CBitmap m_bmpRTOL;
		CBitmap m_bmpLTOR;

		// Generated message map functions
		//{{AFX_MSG(CHpHeadDlg)
		virtual BOOL OnInitDialog();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HPHEADDLG_H__7BF5EFFE_6B9C_416D_91CA_5BBBC8DE6436__INCLUDED_)
