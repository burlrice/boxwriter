// ExportFileDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "ExportFileDlg.h"
#include "Utils.h"
#include "Extern.h"
#include "ListCtrlImp.h"
#include "Task.h"
#include "Export.h"
#include "resource.h"
#include "registry.h"

#ifdef __IPPRINTER__
	#include "BitmapElement.h"
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ListGlobals;
using namespace ItiLibrary;
using namespace Foxjet3d;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const LPCTSTR CExportFileDlg::m_lpszTxtExt =  _T (".txt");
const LPCTSTR CExportFileDlg::m_lpszRegExt = _T (".registry");

int _3D_API Foxjet3d::Export (CWnd * pParent)
{
	return CExportFileDlg (pParent).DoModal ();
}

/////////////////////////////////////////////////////////////////////////////
// CExportFileDlg dialog


CExportFileDlg::CExportFileDlg(CWnd* pParent /*=NULL*/)
:	m_hAccel (NULL),
	COpenDlg(IDD_EXPORT_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CExportFileDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CExportFileDlg::DoDataExchange(CDataExchange* pDX)
{
	COpenDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExportFileDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	//if (IsMatrix ()) 
	{
		m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
		m_buttons.DoDataExchange (pDX, BTN_BROWSE,	IDB_BROWSE);
		m_buttons.DoDataExchange (pDX, BTN_EXPORT,	IDB_OK);
		m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);
	}
}


BEGIN_MESSAGE_MAP(CExportFileDlg, COpenDlg)
	//{{AFX_MSG_MAP(CExportFileDlg)
	ON_BN_CLICKED(BTN_EXPORT, OnExport)
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_EDIT_SELECTALL, OnEditSelectall)
	ON_NOTIFY(NM_DBLCLK, LV_TASKS, OnDblclkTasks)
	ON_CBN_SELCHANGE(CB_LINE, OnSelchangeLine)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExportFileDlg message handlers

BOOL CExportFileDlg::OnInitDialog() 
{
	CString strFile = FoxjetDatabase::GetProfileString (defElements.m_strRegSection + _T ("\\Settings\\CExportDlg"), _T ("Filename"), 
		GetHomeDir () + _T ("\\Export.txt"));

	m_lLineID = FoxjetDatabase::GetProfileInt (defElements.m_strRegSection + _T ("\\Settings\\CExportDlg"), _T ("m_lLineID"), -1);

	if (m_strFilename.GetLength ())
		strFile = m_strFilename;

	SetDlgItemText (TXT_FILENAME, strFile);

	if (CButton * p = (CButton *)GetDlgItem (CHK_CONFIG)) 
		p->SetCheck (FoxjetDatabase::GetProfileInt (defElements.m_strRegSection + _T ("\\Settings\\CExportDlg"), _T ("Config"), 1));

	if (CButton * p = (CButton *)GetDlgItem (CHK_BITMAPS)) 
		p->SetCheck (FoxjetDatabase::GetProfileInt (defElements.m_strRegSection + _T ("\\Settings\\CExportDlg"), _T ("Bitmaps"), 1));

	COpenDlg::OnInitDialog();

	if (CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE)) {
		int nIndex = pLine->AddString (LoadString (IDS_ALLLINES));
		pLine->SetItemData (nIndex, (DWORD)FoxjetDatabase::ALL);

		for (int i = 0; i < pLine->GetCount (); i++) {
			if (m_lLineID == pLine->GetItemData (i)) {
				pLine->SetCurSel (i);
				break;
			}
		}

		pLine->SetItemData (pLine->AddString (LoadString (IDS_REGISTRY)), -2);

		if (pLine->GetCurSel () == CB_ERR)
			pLine->SetCurSel (0);

		OnSelchangeLine ();
	}

	m_lviTask.SetTouchScreenMultiSelect (true);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CExportFileDlg::OnOK()
{
	if (!UpdateData(TRUE))
		return;

	OnExport ();
	//FoxjetCommon::CEliteDlg::OnOK (); //COpenDlg::OnOK ();
}

void CExportFileDlg::OnCancel()
{
	COpenDlg::OnCancel();
}


void CExportFileDlg::OnBrowse() 
{
	CString strFile;
	bool bRegistry = IsCurSelRegistry ();
	CString strFilter =
		CString (_T ("Text files (*.txt)|*.txt|")) +
		_T ("Registry files (*") + CString (CExportFileDlg::m_lpszRegExt) + _T (")|*") + CString (CExportFileDlg::m_lpszRegExt) + _T ("|") +
		CString (_T ("All files (*.*)|*.*||")); 

	if (bRegistry)
		strFilter = 
			_T ("Registry files (*") + CString (CExportFileDlg::m_lpszRegExt) + _T (")|*") + CString (CExportFileDlg::m_lpszRegExt) + _T ("|") +
			CString (_T ("All files (*.*)|*.*||")); 

	TRACEF (strFilter);
	GetDlgItemText (TXT_FILENAME, strFile);

	CFileDialog dlg (FALSE, _T ("*.*"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY,
		strFilter, this);
	
	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();
		SetDlgItemText (TXT_FILENAME, strFile);
	}
}

void CExportFileDlg::OnExport() 
{
	using namespace Foxjet3d;

	CString strFile;
	DWORD dwFlags = IMPORT_ALL;
	CLongArray sel = GetSelectedItems (m_lviTask.GetListCtrl ());
	CLongArray id;

	if (CButton * p = (CButton *)GetDlgItem (CHK_CONFIG)) {
		FoxjetDatabase::WriteProfileInt (defElements.m_strRegSection + _T ("\\Settings\\CExportDlg"), _T ("Config"), p->GetCheck ());

		if (p->GetCheck () == 0)
			dwFlags &= ~(IMPORT_CONFIG | IMPORT_USERS | IMPORT_REPORTS);
	}

	if (CButton * p = (CButton *)GetDlgItem (CHK_BITMAPS)) {
		FoxjetDatabase::WriteProfileInt (defElements.m_strRegSection + _T ("\\Settings\\CExportDlg"), _T ("Bitmaps"), p->GetCheck ());

		if (p->GetCheck () == 0)
			dwFlags &= ~IMPORT_BITMAPS;
	}

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_LINE)) {
		m_lLineID = p->GetItemData (p->GetCurSel ());
		FoxjetDatabase::WriteProfileInt (defElements.m_strRegSection + _T ("\\Settings\\CExportDlg"), _T ("m_lLineID"), m_lLineID);
	}

	GetDlgItemText (TXT_FILENAME, strFile);

	if (strFile.IsEmpty ()) {
		CString str;

		str.Format (LoadString (IDS_INVALIDFILENAME), _T (""));
		MsgBox (* this, str);
		return;
	}
	
	if (FoxjetFile::FileExists (strFile)) {
		CString str;

		str.Format (LoadString (IDS_OVERWRITEFILE), strFile);

		if (MsgBox (* this, str, LoadString (IDS_WARNING), MB_ICONQUESTION | MB_YESNO) == IDNO) 
			return;
	}

	if (m_lLineID == -2) {
		HKEY hKey = NULL;

		if (::RegCreateKey (HKEY_CURRENT_USER, _T ("Software\\FoxJet"), &hKey) == ERROR_SUCCESS) {
			CString strMsg;

			::DeleteFile (strFile);
			VERIFY (FoxjetDatabase::EnablePrivilege (SE_BACKUP_NAME));
			LRESULT lResult = ::RegSaveKey (hKey, strFile, NULL);
			TRACEF (FormatMessage (lResult));
			::RegCloseKey (hKey);

			strMsg.Format (LoadString (IDS_REGEXPORTEDTO), strFile);
			MsgBox (* this, strMsg, NULL, MB_ICONINFORMATION);
			FoxjetCommon::CEliteDlg::OnOK ();
			return;
		}
		else {
			CString str;

			str.Format (LoadString (IDS_FAILEDTOWRITEFILE), strFile);
			MsgBox (* this, str);
		}
	}

	if (sel.GetSize () == 0) 
		for (int i = 0; i < m_lviTask->GetItemCount (); i++)
			sel.Add (i);

	for (int i = 0; i < sel.GetSize (); i++) {
		if (CTaskItem * pData = (CTaskItem *)m_lviTask.GetCtrlData (sel [i])) 
			id.Add (pData->m_pTask->m_lID);
		else
			id.Add (_tcstoul (m_lviTask->GetItemText (sel [i], CSaveDlg::m_nIDIndex), NULL, 10));
	}

	BeginWaitCursor ();
	#ifdef __IPPRINTER__
	FoxjetCommon::SetMkNextParams (FoxjetIpElements::CBitmapElement::GetDefPath (), FoxjetIpElements::CBitmapElement::m_strSuffix);
	#endif
	bool bExport = FoxjetCommon::Export (ListGlobals::GetDB (), strFile, id, dwFlags);
	EndWaitCursor ();

	if (!bExport) {
		CString str;

		str.Format (LoadString (IDS_FAILEDTOWRITEFILE), strFile);
		MsgBox (* this, str);
	}
	else {
		CString strMsg;

		strMsg.Format (LoadString (IDS_EXPORTEDTO), strFile);
		MsgBox (* this, strMsg, NULL, MB_ICONINFORMATION);
		FoxjetCommon::CEliteDlg::OnOK ();
		FoxjetDatabase::WriteProfileString (defElements.m_strRegSection + _T ("\\Settings\\CExportDlg"), _T ("Filename"), strFile);
	}
}


BOOL CExportFileDlg::PreTranslateMessage(MSG* pMsg) 
{
	if (!m_hAccel)
		VERIFY (m_hAccel = ::LoadAccelerators (GetInstanceHandle (), MAKEINTRESOURCE (IDA_LISTCTRL)));

	if (::TranslateAccelerator (m_hWnd, m_hAccel, pMsg))
		return TRUE;
	
	return COpenDlg::PreTranslateMessage(pMsg);
}

void CExportFileDlg::OnEditSelectall() 
{
	m_lviTask.SelectAll ();
}

void CExportFileDlg::OnDblclkTasks(NMHDR* pNMHDR, LRESULT* pResult) 
{
}

bool CExportFileDlg::IsCurSelRegistry ()
{
	if (CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE)) 
		return pLine->GetItemData (pLine->GetCurSel ()) == -2 ? true : false;

	return false;
}

void CExportFileDlg::OnSelchangeLine ()
{
	if (CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE)) {
		UINT n [] =  
		{
			LV_TASKS,
			CB_PANEL,
			CHK_PREVIEW,
			CHK_CONFIG,
			CHK_BITMAPS,
		};
		CString strFile;
		bool bRegistry = IsCurSelRegistry ();

		for (int i = 0; i < ARRAYSIZE (n); i++) 
			if (CWnd * p = GetDlgItem (n [i]))
				p->EnableWindow (bRegistry ? FALSE : TRUE);

		GetDlgItemText (TXT_FILENAME, strFile);

		if (strFile.Replace (bRegistry ? m_lpszTxtExt : m_lpszRegExt, bRegistry ? m_lpszRegExt : m_lpszTxtExt))
			SetDlgItemText (TXT_FILENAME, strFile);
	}

	COpenDlg::OnSelchangeLine ();
}