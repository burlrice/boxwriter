// ProgressDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "ProgressDlg.h"
#include "Debug.h"
#include "Extern.h"
#include "resource.h"
#include "Database.h"
#include "ImportFileDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Foxjet3d;
using namespace ListGlobals;

static CProgressDlg::LPSTATUSFCT lpStatusFct = NULL;

void CProgressDlg::SetStatusFct (LPSTATUSFCT lpfct)
{
	::lpStatusFct = lpfct;
}

/////////////////////////////////////////////////////////////////////////////
// CProgressDlg dialog

const int CProgressDlg::m_wmStepProgress	= ::RegisterWindowMessage (_T ("CProgressDlg::m_wmStepProgress"));
const int CProgressDlg::m_wmEndProgress		= ::RegisterWindowMessage (_T ("CProgressDlg::m_wmEndProgress"));
const int CProgressDlg::m_wmMsgBox			= ::RegisterWindowMessage (_T ("CProgressDlg::m_wmMsgBox"));
const int CProgressDlg::m_wmAddResult		= ::RegisterWindowMessage (_T ("CProgressDlg::m_wmAddResult"));
const int CProgressDlg::m_wmAddBitmapFile	= ::RegisterWindowMessage (_T ("CProgressDlg::m_wmAddBitmapFile"));
const int CProgressDlg::m_wmAddBitmapDir	= ::RegisterWindowMessage (_T ("CProgressDlg::m_wmAddBitmapDir"));

CProgressDlg::CProgressDlg(CWnd* pParent /*=NULL*/)
:	m_bCancel (false),
	FoxjetCommon::CEliteDlg(IDD_PROGRESS_MATRIX, pParent)
{
	//{{AFX_DATA_INIT(CProgressDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CProgressDlg::~CProgressDlg ()
{
}

void CProgressDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProgressDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	//if (FoxjetDatabase::IsMatrix ()) 
	{
		if (GetDlgItem (IDCANCEL))
			m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	}
}


BEGIN_MESSAGE_MAP(CProgressDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CProgressDlg)
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE (CProgressDlg::m_wmStepProgress, OnStepProgress)
	ON_REGISTERED_MESSAGE (CProgressDlg::m_wmEndProgress, OnEndProgress)
	ON_REGISTERED_MESSAGE (CProgressDlg::m_wmMsgBox, OnMsgBox)
	ON_REGISTERED_MESSAGE (CProgressDlg::m_wmAddResult,	OnAddResult)
	ON_REGISTERED_MESSAGE (CProgressDlg::m_wmAddBitmapFile,	OnAddBitmapFile)
	ON_REGISTERED_MESSAGE (CProgressDlg::m_wmAddBitmapDir, OnAddBitmapDir)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProgressDlg message handlers

void CProgressDlg::OnCancel() 
{
	m_bCancel = true;

	if (CWnd * p = GetParent ())
		p->PostMessage (CProgressDlg::m_wmEndProgress, IDCANCEL, 0);
}

void CProgressDlg::OnOK ()
{
}

LRESULT CProgressDlg::OnStepProgress (WPARAM wParam, LPARAM lParam)
{
	if (wParam) {
		CString str;
		
		str.Format (LoadString (wParam), lParam);

		//TRACEF (str);
		SetDlgItemText (LBL_MESSAGE, str);
		Invalidate ();
		RedrawWindow ();
		BringWindowToTop ();

		if (::lpStatusFct)
			(* ::lpStatusFct) (str);
		//theApp.SetStatusText (str);
	}

	return 0;
}

LRESULT CProgressDlg::OnEndProgress (WPARAM wParam, LPARAM lParam)
{
	if (CWnd * p = GetParent ()) {
		p->PostMessage (CProgressDlg::m_wmEndProgress, wParam, lParam);

		if (::lpStatusFct)
			(* ::lpStatusFct) (_T (""));
	}

	return 0;
}

LRESULT CProgressDlg::OnMsgBox (WPARAM wParam, LPARAM lParam)
{
	int n = 0;

	if (CString * pstr = (CString *)wParam) {
		n = MsgBox (m_hWnd, * pstr, _T (""), lParam);
		delete pstr;
	}

	return n;
}


LRESULT CProgressDlg::OnAddResult (WPARAM wParam, LPARAM lParam)
{
	if (CWnd * p = GetParent ()) 
		return p->SendMessage (CProgressDlg::m_wmAddResult, wParam, lParam);

	return 0;
}

LRESULT CProgressDlg::OnAddBitmapFile (WPARAM wParam, LPARAM lParam)
{
	if (CWnd * p = GetParent ()) 
		return p->SendMessage (CProgressDlg::m_wmAddBitmapFile, wParam, lParam);

	return 0;
}

LRESULT CProgressDlg::OnAddBitmapDir (WPARAM wParam, LPARAM lParam)
{
	if (CWnd * p = GetParent ()) 
		return p->SendMessage (CProgressDlg::m_wmAddBitmapDir, wParam, lParam);

	return 0;
}

BOOL CProgressDlg::Create (CWnd * pParent)
{
	HINSTANCE hRestore = ::AfxGetResourceHandle ();
	::AfxSetResourceHandle (GetInstanceHandle ());
	BOOL bResult = FoxjetCommon::CEliteDlg::Create (IDD_PROGRESS_MATRIX, pParent);
	::AfxSetResourceHandle (hRestore);
	return bResult;
}
