// ValvePropDlg.cpp : implementation file
//

#include "stdafx.h"
#include "3d.h"
#include "ValvePropDlg.h"
#include "Coord.h"
#include "HeadDlg.h"
#include "Database.h"
#include "TemplExt.h"
#include "IvRegs.h"
#include "Debug.h"
#include "task.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace Foxjet3d;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CValvePropDlg dialog


CValvePropDlg::CValvePropDlg(FoxjetDatabase::COdbcDatabase & db,
							 ULONG lLineID,
							 UNITS units,
							 const FoxjetDatabase::HEADSTRUCT & head, 
							 const FoxjetDatabase::VALVESTRUCT & valve,
							 CWnd* pParent /*=NULL*/)
:	m_valve (valve),
	m_head (head),
	m_db (db),
	m_lLineID (lLineID),
	m_units (units),
	FoxjetCommon::CEliteDlg(CValvePropDlg::IDD, pParent)
{
	m_valve.m_strName = m_valve.GetName ();

	//{{AFX_DATA_INIT(CValvePropDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CValvePropDlg::DoDataExchange(CDataExchange* pDX)
{
	HEADSTRUCT & head = m_head;

	CCoord coordMin (0, 0, m_units, head);
	CCoord coordMax (0, 0, m_units, head);
	
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);

	DDX_Text (pDX, TXT_NAME, m_valve.m_strName);
	DDV_MinMaxChars (pDX, m_valve.m_strName, TXT_NAME, 1, 30);

	//{{AFX_DATA_MAP(CValvePropDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	DDX_Coord (pDX, TXT_PHOTOCELLDELAY, (long &)m_valve.m_lPhotocellDelay, m_units, head, true);
	coordMin.m_x = CHeadDlg::m_lmtVxPhotocellDelay.m_dwMin;
	coordMax.m_x = CHeadDlg::m_lmtVxPhotocellDelay.m_dwMax;
	DDV_Coord (pDX, TXT_PHOTOCELLDELAY, m_valve.m_lPhotocellDelay, 0, m_units, head, DDVCOORDXBOTH, coordMin, coordMax);

	DDX_Coord (pDX, TXT_HEIGHT, (long &)m_valve.m_lHeight, m_units, head, false);
	coordMin.m_y = CHeadDlg::m_lmtRelativeHeight.m_dwMin; 
	coordMax.m_y = CHeadDlg::m_lmtRelativeHeight.m_dwMax; 
	DDV_Coord (pDX, TXT_HEIGHT, 0, m_valve.m_lHeight, m_units, head, DDVCOORDYBOTH, coordMin, coordMax);

	m_valve.m_bReversed = !m_valve.m_bReversed;
	DDX_Radio (pDX, RDO_RTOL, m_valve.m_bReversed);
	m_valve.m_bReversed = !m_valve.m_bReversed;

	DDX_Check (pDX, CHK_UPSIDEDOWN, (int &)m_valve.m_bUpsidedown);
}


BEGIN_MESSAGE_MAP(CValvePropDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CValvePropDlg)
	ON_BN_CLICKED(RDO_LTOR, OnDirection)
	ON_CBN_SELCHANGE(CB_LINKED, OnSelchangeLinked)
	ON_BN_CLICKED(RDO_RTOL, OnDirection)
	ON_CBN_SELCHANGE(CB_CARD, OnSelchangeCard)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CValvePropDlg message handlers

BOOL CValvePropDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();

	m_bmpRTOL.LoadBitmap (IDB_RTOL);
	m_bmpLTOR.LoadBitmap (IDB_LTOR);

	FillPanels ();
	FillCards ();
	FillLinks ();
	OnSelchangeLinked ();
	OnDirection ();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CValvePropDlg::OnOK()
{
	CComboBox * pPanel = (CComboBox *)GetDlgItem (CB_PANEL);
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	CComboBox * pCard = (CComboBox *)GetDlgItem (CB_CARD);
	CComboBox * pIndex = (CComboBox *)GetDlgItem (CB_INDEX);
	CComboBox * pLinked = (CComboBox *)GetDlgItem (CB_LINKED);
	
	ASSERT (pPanel);
	ASSERT (pType);
	ASSERT (pCard);
	ASSERT (pIndex);
	ASSERT (pLinked);

	if (!UpdateData ())
		return;

	m_valve.m_lPanelID	= pPanel->GetItemData (pPanel->GetCurSel ());
	m_valve.m_type		= (VALVETYPE)pType->GetItemData (pType->GetCurSel ());
	m_valve.m_lCardID	= pCard->GetItemData (pCard->GetCurSel ());
	m_valve.m_nIndex	= pIndex->GetItemData (pIndex->GetCurSel ());
	m_valve.m_nLinked	= pLinked->GetItemData (pLinked->GetCurSel ());

	FoxjetCommon::CEliteDlg::OnOK ();
}

void CValvePropDlg::FillPanels()
{
	CComboBox * pPanel = (CComboBox *)GetDlgItem (CB_PANEL);
	CArray <PANELSTRUCT, PANELSTRUCT &> v;
	
	GetPanelRecords (m_db, m_lLineID, v);

	pPanel->ResetContent ();

	for (int i = 0; i < v.GetSize (); i++) {
		PANELSTRUCT p = v [i];

		int nIndex = pPanel->AddString (p.m_strName);
		pPanel->SetItemData (nIndex, p.m_lID);

		if (p.m_lID == m_valve.m_lPanelID)
			pPanel->SetCurSel (nIndex);
	}

	if (pPanel->GetCurSel () == CB_ERR)
		pPanel->SetCurSel (0);
}

VALVESTRUCT CValvePropDlg::GetRoot (const HEADSTRUCT & head, const VALVESTRUCT & valve)
{
	CArray <VALVESTRUCT, VALVESTRUCT &> v;

	GetList (head, valve, v);

	ASSERT (v.GetSize ());
	
	return v [0];
}

void CValvePropDlg::FillTypes()
{
	struct
	{
		VALVETYPE	m_type;
		UINT		m_nID;
	} static const map [] = 
	{
		{ IV_12_9,	IDS_IV_12_9,	},
		{ IV_78_9,	IDS_IV_78_9,	},
		{ IV_10_18,	IDS_IV_10_18,	},
		{ IV_20_18,	IDS_IV_20_18,	},
	};
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	CComboBox * pLinked = (CComboBox *)GetDlgItem (CB_LINKED);

	ASSERT (pLinked);
	ASSERT (pType);

	pType->ResetContent ();

	m_valve.m_nLinked = pLinked->GetItemData (pLinked->GetCurSel ());
	VALVESTRUCT root = GetRoot (m_head, m_valve);

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		if (m_valve.IsLinked ()) {
			double dRes [2] = 
			{
				(double)GetValveChannels (map [i].m_type)	/ (double)GetValveHeight (map [i].m_type),
				(double)GetValveChannels (root.m_type)		/ (double)GetValveHeight (root.m_type),
			};
			int nRes [2] = { (int)(dRes [0] * 10000.0), (int)(dRes [1] * 10000.0) };

			if (nRes [0] != nRes [1])
				continue;
		}

		int nIndex = pType->AddString (LoadString (map [i].m_nID));
		pType->SetItemData (nIndex, map [i].m_type);

		if (m_valve.m_type == map [i].m_type)
			pType->SetCurSel (nIndex);
	}

	if (pType->GetCurSel () == CB_ERR)
		pType->SetCurSel (0);
}

int CValvePropDlg::FindIndex (FoxjetDatabase::HEADSTRUCT & head, int nIndex)
{
	for (int i = 0; i < head.m_vValve.GetSize (); i++)
		if (head.m_vValve [i].m_nIndex == nIndex)
			return i;

	return -1;
}

void CValvePropDlg::FillIndex ()
{
	CComboBox * pIndex = (CComboBox *)GetDlgItem (CB_INDEX);
	CComboBox * pLinked = (CComboBox *)GetDlgItem (CB_LINKED);
	CLongArray v;

	ASSERT (pLinked);
	ASSERT (pIndex);

	m_valve.m_nLinked = pLinked->GetItemData (pLinked->GetCurSel ());

	pIndex->ResetContent ();

	{
		HEADSTRUCT head;

		GetHeadRecord (m_db, GetCurHeadID (), head);
		
		for (int i = 1; i <= IV_MAX_HEADS; i++) {
			if (m_valve.m_nIndex == i)
				v.Add (i);
			else if (FindIndex (head, i) == -1)
				v.Add (i);
		}
	}

	if (m_valve.IsLinked ()) {
		CArray <VALVESTRUCT, VALVESTRUCT &> vTmp;
		HEADSTRUCT head;

		GetHeadRecord (m_db, GetCurHeadID (), head);
		GetValves (head, vTmp, false);

		for (int i = 0; i < vTmp.GetSize (); i++) {
			int nRemove = vTmp [i].m_nIndex;

			if (nRemove != m_valve.m_nIndex) {
				for (int j = 0; j < v.GetSize (); j++) {
					if (nRemove == (int)v [j]) {
						v.RemoveAt (j);
						break;
					}
				}
			}
		}
	}

	for (int i = 0; i < v.GetSize (); i++) {
		CString str;
		int n = v [i];

		str.Format (_T ("%d"), n);
		int nIndex = pIndex->AddString (str);
		pIndex->SetItemData (nIndex, n);
	}

	if (pIndex->GetCurSel () == CB_ERR)
		pIndex->SetCurSel (0);
}

bool CValvePropDlg::IsEndOfList (const FoxjetDatabase::HEADSTRUCT & head, const FoxjetDatabase::VALVESTRUCT & valve)
{
	return GetLinkedTo (head, valve) == -1;
}

void CValvePropDlg::FillLinks ()
{
	CComboBox * pLinked = (CComboBox *)GetDlgItem (CB_LINKED);
	CComboBox * pCard = (CComboBox *)GetDlgItem (CB_CARD);
	CArray <VALVESTRUCT, VALVESTRUCT &> vUnlinked;
	HEADSTRUCT head;

	ASSERT (pLinked);
	ASSERT (pCard);

	GetHeadRecord (m_db, GetCurHeadID (), head);
	GetValves (head, vUnlinked, false);

	m_valve.m_lCardID = pCard->GetItemData (pCard->GetCurSel ());

	pLinked->ResetContent ();
	pLinked->EnableWindow (vUnlinked.GetSize ());
	pLinked->SetItemData (pLinked->AddString (LoadString (IDS_NOTHING)), -1);

	for (int i = 0; i < head.m_vValve.GetSize (); i++) {
		CArray <VALVESTRUCT, VALVESTRUCT &> v;
		CString str;

		GetList (head, head.m_vValve [i], v);
		VALVESTRUCT h = v [v.GetSize () - 1];

		if (IsEndOfList (head, h)) {
			str.Format (_T ("%s [%d]"), h.m_strName, h.m_nIndex);

			if (h.m_lID != m_valve.m_lID) { // can't link to self
				int nIndex = pLinked->FindStringExact (-1, str);

				if (nIndex == CB_ERR) { // not alreay in list
					int nIndex = pLinked->AddString (str);
					pLinked->SetItemData (nIndex, h.m_nIndex);

					if (m_valve.m_nLinked == h.m_nIndex)
						pLinked->SetCurSel (nIndex);
				}
			}
		}
	}

	if (pLinked->GetCurSel () == CB_ERR) {
		int nFind = CTask::FindByIndex (m_head.m_vValve, m_valve.m_nLinked);

		if (nFind != -1) {
			VALVESTRUCT h = m_head.m_vValve [nFind];
			CString str;

			str.Format (_T ("%s [%d]"), h.m_strName, h.m_nIndex);
			int nIndex = pLinked->AddString (str);
			pLinked->SetItemData (nIndex, h.m_nIndex);
			pLinked->SetCurSel (nIndex);
		}
	}

	if (pLinked->GetCurSel () == CB_ERR)
		pLinked->SetCurSel (0);
}


void CValvePropDlg::FillCards()
{
#ifdef __WINPRINTER__
	CComboBox * pCard = (CComboBox *)GetDlgItem (CB_CARD);
	CArray <HEADSTRUCT, HEADSTRUCT &> v;

	ASSERT (pCard);

	GetLineHeads (m_db, m_lLineID, v);

	for (int i = 0; i < v.GetSize (); i++) {
		HEADSTRUCT h = v [i];
		ULONG lAddr = _tcstoul (h.m_strUID, NULL, 16);
		CString str;

		str.Format (_T ("%s [%s]"), h.m_strName, CHeadDlg::GetPHC (lAddr));

		int nIndex = pCard->AddString (str);

		pCard->SetItemData (nIndex, h.m_lID);

		if (m_valve.m_lCardID == h.m_lID)
			pCard->SetCurSel (nIndex);
	}

	if (pCard->GetCurSel () == CB_ERR)
		pCard->SetCurSel (0);
#endif // __WINPRINTER__
}

void CValvePropDlg::OnDirection() 
{
	CButton * pRTOL = (CButton *)GetDlgItem (RDO_RTOL);
	CStatic * pBmp = (CStatic *)GetDlgItem (IDB_DIRECTION);

	ASSERT (pRTOL);
	ASSERT (pBmp);

	pBmp->SetBitmap (pRTOL->GetCheck () == 1 ? m_bmpRTOL : m_bmpLTOR);
}

void CValvePropDlg::GetList (const HEADSTRUCT & head, const VALVESTRUCT & valve, CArray <VALVESTRUCT, VALVESTRUCT &> & v)
{
	VALVESTRUCT tail = valve;

	int nTail = GetLinkedTo (head, tail);

	while (nTail != -1) {
		tail = head.m_vValve [nTail];
		nTail = GetLinkedTo (head, tail);
	}


	// starting at the tail, build the list backwards
	int nIndex = CTask::FindByIndex (head.m_vValve, tail.m_nLinked);

	v.InsertAt (0, tail);

	while (nIndex != -1) {
		VALVESTRUCT h = head.m_vValve [nIndex];
		v.InsertAt (0, h);
		nIndex = CTask::FindByIndex (head.m_vValve, h.m_nLinked);
	}
}

int CValvePropDlg::GetLinkedTo (const HEADSTRUCT & head, const VALVESTRUCT & valve) 
{
	if (valve.m_nIndex > 0) {
		for (int i = 0; i < head.m_vValve.GetSize (); i++) {
			const VALVESTRUCT & s = head.m_vValve [i];

			if (s.m_nLinked == valve.m_nIndex)
				return i;
		}
	}

	return -1;
}

void CValvePropDlg::OnSelchangeLinked() 
{
	UINT nID [] = 
	{
		CB_CARD,
		CB_PANEL,
		RDO_RTOL,
		RDO_LTOR,
		CHK_UPSIDEDOWN,
	};

	CComboBox * pLinked = (CComboBox *)GetDlgItem (CB_LINKED);
	CComboBox * pIndex = (CComboBox *)GetDlgItem (CB_INDEX);
	CEdit * pHeight = (CEdit *)GetDlgItem (TXT_HEIGHT);
	ULONG y = m_valve.m_lHeight;
	int nIndex = m_valve.m_nIndex;

	ASSERT (pLinked);
	ASSERT (pIndex);
	ASSERT (pHeight);

	m_valve.m_nLinked = pLinked->GetItemData (pLinked->GetCurSel ());

	FillIndex ();
	FillTypes ();

	pHeight->EnableWindow (!m_valve.IsLinked ());
	pIndex->EnableWindow (!m_valve.IsLinked ());

	for (int i = 0; i < ARRAYSIZE (nID); i++)
		if (CWnd * p = GetDlgItem (nID [i]))
			p->EnableWindow (!m_valve.IsLinked ());

	if (m_valve.IsLinked ()) {
		CArray <VALVESTRUCT, VALVESTRUCT &> v;

		GetList (m_head, m_valve, v);

		ASSERT (v.GetSize ());
		const VALVESTRUCT root = v [0];

		y						= root.m_lHeight + GetValveHeight (root.m_type);
		nIndex					= root.m_nIndex;
		m_valve.m_lCardID		= root.m_lCardID;
		m_valve.m_lPanelID		= root.m_lPanelID;
		m_valve.m_bReversed		= root.m_bReversed;
		m_valve.m_bUpsidedown	= root.m_bUpsidedown;

		for (int i = 0; i < v.GetSize (); i++) {
			VALVESTRUCT h = v [i];

			y -= GetValveHeight (h.m_type);

			if (h.m_lID == m_valve.m_lID)
				break;

			nIndex++;
		}
	}

	UpdateData (FALSE);

	CCoord height (CBaseElement::ThousandthsToLogical (CPoint (0, y), m_head), m_units, m_head); 
	SetDlgItemText (TXT_HEIGHT, height.FormatY ());

	if (y != m_valve.m_lHeight)
		pHeight->SetModify (TRUE); // so DDX_Coord will work

	for (int i = 0; i < pIndex->GetCount (); i++) {
		if ((int)pIndex->GetItemData (i) == nIndex) {
			pIndex->SetCurSel (i);
			break;
		}
	}

	if (pIndex->GetCurSel () == CB_ERR) {
		CString str;

		str.Format (_T ("%d"), nIndex);
		int nSel = pIndex->AddString (str);
		pIndex->SetItemData (nSel, nIndex);
		pIndex->SetCurSel (nSel);
	}
}

void CValvePropDlg::GetPanelValves (FoxjetDatabase::COdbcDatabase & db, FoxjetDatabase::HEADSTRUCT & card, ULONG lPanelID)
{
	HEADSTRUCT h;

	VERIFY (GetHeadRecord (db, card.m_lID, h));
	card.m_vValve.RemoveAll ();

	for (int i = 0; i < h.m_vValve.GetSize (); i++) {
		VALVESTRUCT v = h.m_vValve [i];

		if (v.m_lPanelID == lPanelID)
			card.m_vValve.Add (v);
	}

	SortByHeight (card.m_vValve);
}

void CValvePropDlg::GetValves(HEADSTRUCT & head, CArray <VALVESTRUCT, VALVESTRUCT &> & v, bool bLinked)
{
	for (int i = 0; i < head.m_vValve.GetSize (); i++) {
		VALVESTRUCT & h = head.m_vValve [i];

		if (h.IsLinked () == bLinked)
			v.Add (h);
	}
}

void CValvePropDlg::OnSelchangeCard() 
{
	GetHeadRecord (m_db, GetCurHeadID (), m_head);
	FillLinks ();	
	FillIndex ();
}

ULONG CValvePropDlg::GetCurHeadID ()
{
	CComboBox * pCard = (CComboBox *)GetDlgItem (CB_CARD);

	ASSERT (pCard);

	return pCard->GetItemData (pCard->GetCurSel ());
}