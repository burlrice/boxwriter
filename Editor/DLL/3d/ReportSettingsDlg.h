#if !defined(AFX_REPORTSETTINGSDLG_H__5A4732FC_9852_4C47_BFE4_47E46932D856__INCLUDED_)
#define AFX_REPORTSETTINGSDLG_H__5A4732FC_9852_4C47_BFE4_47E46932D856__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ReportSettingsDlg.h : header file
//

#include "3dApi.h"
#include "Database.h"
#include "Utils.h"

namespace Foxjet3d
{

	/////////////////////////////////////////////////////////////////////////////
	// CReportSettingsDlg dialog

	class _3D_API CReportSettingsDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CReportSettingsDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strKey, CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CReportSettingsDlg)
		int		m_nDays;
		BOOL	m_bEnable;
		//}}AFX_DATA

		static void Load (FoxjetDatabase::COdbcDatabase & db, const CString & strKey, BOOL & bEnable, int & nDays);

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CReportSettingsDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual void OnOK();
		CString m_strKey;
		FoxjetDatabase::COdbcDatabase & m_db;

		// Generated message map functions
		//{{AFX_MSG(CReportSettingsDlg)
		afx_msg void OnEnable();
		virtual BOOL OnInitDialog();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REPORTSETTINGSDLG_H__5A4732FC_9852_4C47_BFE4_47E46932D856__INCLUDED_)
