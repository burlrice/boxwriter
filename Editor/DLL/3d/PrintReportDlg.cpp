// PrintReportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "3d.h"
#include "PrintReportDlg.h"
#include "ListCtrlImp.h"
#include "Debug.h"
#include "Database.h"
#include "ReportSettingsDlg.h"
#include "FileExt.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Parse.h"

using namespace ItiLibrary;
using namespace FoxjetDatabase;
using namespace Foxjet3d;
using namespace PrintReportDlg;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const LPCTSTR CPrintReportDlg::m_lpszSettingsKey = _T ("CPrintReportDlg::AutoExport");
const LPCTSTR CPrintReportDlg::m_lpszSettingsKeyPath = _T("CPrintReportDlg::ExportPath");

bool UTIL_API Foxjet3d::IsPrintReportLoggingEnabled (FoxjetDatabase::COdbcDatabase & db, ULONG lPrinterID)
{
	SETTINGSSTRUCT s;

	if (GetSettingsRecord (db, lPrinterID, _T ("Control::print report"), s))
		return _ttoi (s.m_strData) ? true : false;

	return false;
}

/////////////////////////////////////////////////////////////////////////////

CReportItem::CReportItem (const FoxjetDatabase::REPORTSTRUCT & s, const CStringArray& vUserCols)
:	m_rpt (s)
{
	for (int i = 0; i < vUserCols.GetSize(); i++)
		m_vUserCols.push_back((LPCTSTR)vUserCols[i]);
}

CReportItem::~CReportItem ()
{
}

CString CReportItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:		return m_rpt.m_dtTime.Format (_T ("%c"));		
	case 1:		return m_rpt.m_strAction;
	case 2:		return m_rpt.m_strUsername;
	case 3:		return m_rpt.m_strLine;
	case 4:		return m_rpt.m_strTaskName;
	case 5:		return m_rpt.m_strCounts;
	default:
		{
			int nUserIndex = nColumn - 6;

			if (nUserIndex >= 0 && nUserIndex < m_vUserCols.size()) {
				auto col = m_vUserCols[nUserIndex];
				auto* pmap = m_rpt.m_pmapUser.get();

				if (pmap) {
					auto find = pmap->find(col);

					if (find != pmap->end())
						return find->second.c_str();
				}
			}
		}
		break;
	}

	return _T ("");
}

int CReportItem::Compare (const ItiLibrary::ListCtrlImp::CItem & cmp, int nColumn) const
{
	CReportItem & rhs = (CReportItem &)cmp;

	switch (nColumn) {
	case 0:		
		if (m_rpt.m_dtTime == rhs.m_rpt.m_dtTime)
			return 0;
		if (m_rpt.m_dtTime > rhs.m_rpt.m_dtTime)
			return 1;
		else 
			return -1;
	}

	return CItem::Compare (rhs, nColumn);
}

/////////////////////////////////////////////////////////////////////////////
// CPrintReportDlg dialog


CPrintReportDlg::CPrintReportDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent /*=NULL*/)
:	m_db (db),
	FoxjetCommon::CEliteDlg (IDD_PRINT_REPORT, pParent)
{
	//{{AFX_DATA_INIT(CPrintReportDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_sLine = _T("");
	m_lPrinterID = GetPrinterID (db);
	m_bEnable = false;
}


void CPrintReportDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrintReportDlg)
	//}}AFX_DATA_MAP

	m_buttons.DoDataExchange (pDX, IDOK,		IDB_OK);
	m_buttons.DoDataExchange (pDX, IDCANCEL,	IDB_CANCEL);
	m_buttons.DoDataExchange (pDX, BTN_EXPORT);
	m_buttons.DoDataExchange (pDX, IDC_CLEAR);
	m_buttons.DoDataExchange (pDX, IDHELP,		IDB_INFO);
}


BEGIN_MESSAGE_MAP(CPrintReportDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CPrintReportDlg)
	ON_BN_CLICKED(IDC_CLEAR, OnClear)
	ON_BN_CLICKED(BTN_EXPORT, OnExport)
	ON_COMMAND(ID_FILE_SETTINGS, OnFileSettings)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintReportDlg message handlers

BOOL CPrintReportDlg::OnInitDialog() 
{
	using ListCtrlImp::CColumn;

	CArray <REPORTSTRUCT, REPORTSTRUCT&> v;
	CArray <CColumn, CColumn> vCols;
	CStringArray vUserCols;

	FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_TIME), 120));
	vCols.Add (CColumn (LoadString (IDS_ACTION), 120));
	vCols.Add (CColumn (LoadString (IDS_USER), 100));
	vCols.Add (CColumn (LoadString (IDS_LINE), 100));
	vCols.Add (CColumn (LoadString (IDS_TASKNAME), 100));
	vCols.Add (CColumn (LoadString (IDS_COUNTS), 60));

	FoxjetDatabase::GetReportUserColumns(m_db, vUserCols);

	for (int i = 0; i < vUserCols.GetSize (); i++)
		vCols.Add(CColumn(vUserCols[i], 100));

	m_lv.Create (LV_REPORT, vCols, this, _T ("Software\\FoxJet\\Control"));

	DWORD dwStyle = m_lv->GetExtendedStyle (); 
	m_lv->SetExtendedStyle (dwStyle| LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	FoxjetDatabase::GetPrinterReportRecords (m_db, m_sLine, v, m_lPrinterID);

	for (int i = 0; i < v.GetSize(); i++) 
		m_lv.InsertCtrlData (new CReportItem (v [i], vUserCols));

	if (CButton * p = (CButton *)GetDlgItem (CHK_AUTOEXPORT)) 
		p->SetCheck (IsAutoExport (m_db, m_lPrinterID) ? 1 : 0);

	if (CButton * p = (CButton *)GetDlgItem (CHK_ENABLED)) 
		p->SetCheck (IsPrintReportLoggingEnabled (m_db, m_lPrinterID) ? 1 : 0);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPrintReportDlg::OnClear() 
{
	if (MsgBox (* this, LoadString (IDS_CLEARREPORTS), MB_YESNO ) == IDYES )
	{
		FoxjetDatabase::ClearReportRecords (m_db, m_lPrinterID);
		m_lv.DeleteCtrlData ();
		EndDialog(RESTART);
	}
}

void CPrintReportDlg::OnExport() 
{
	CString strTitle, strFile;
	FoxjetDatabase::SETTINGSSTRUCT s;

	GetSettingsRecord(m_db, 0, m_lpszSettingsKeyPath, s);

	if (!s.m_strData.GetLength())
		s.m_strData = GetHomeDir();

	GetWindowText (strTitle);
	strFile.Format(_T("%s\\%s [%s].txt"), s.m_strData, strTitle, m_sLine);

	CFileDialog dlg (FALSE, _T ("*.*"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
		_T ("All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();

		if (ItiLibrary::Export (m_lv, strFile)) {
			FoxjetDatabase::SETTINGSSTRUCT s;

			s.m_lLineID = 0;
			s.m_strKey = m_lpszSettingsKeyPath;
			s.m_strData = FoxjetFile::GetPath(strFile);

			if (!UpdateSettingsRecord(m_db, s))
				AddSettingsRecord(m_db, s);
		}
		else {
			CString str;

			str.Format(LoadString(IDS_EXPORTFAILED), strFile);
			MsgBox(*this, str);
		}
	}
}

bool CPrintReportDlg::IsAutoExport (FoxjetDatabase::COdbcDatabase & db, ULONG lPrinterID)
{
	SETTINGSSTRUCT s;

	GetSettingsRecord (db, lPrinterID, m_lpszSettingsKey, s);

	return _ttoi (s.m_strData) == 1 ? true : false;
}

void CPrintReportDlg::OnOK ()
{
	if (CButton * p = (CButton *)GetDlgItem (CHK_AUTOEXPORT)) {
		SETTINGSSTRUCT s;

		s.m_strKey	= m_lpszSettingsKey;
		s.m_lLineID = m_lPrinterID;
		s.m_strData = p->GetCheck () == 0 ? _T ("0") : _T ("1");

		if (!UpdateSettingsRecord (m_db, s))
			VERIFY (AddSettingsRecord (m_db, s));
	}

	if (CButton * p = (CButton *)GetDlgItem (CHK_ENABLED)) 
		m_bEnable = p->GetCheck () == 1 ? true : false;

	FoxjetCommon::CEliteDlg::OnOK ();
}

void CPrintReportDlg::Export (FoxjetDatabase::COdbcDatabase & db, const CString & strLine, ULONG lPrinterID)
{
	CPrintReportDlg dlg (db);
	CString strFile;

	dlg.m_sLine = strLine;
	dlg.m_lPrinterID = lPrinterID;
	dlg.Create (IDD_PRINT_REPORT);
	dlg.GetWindowText (strFile);

	{ 
		FoxjetDatabase::SETTINGSSTRUCT s;

		GetSettingsRecord(db, 0, m_lpszSettingsKeyPath, s);

		if (!s.m_strData.GetLength())
			s.m_strData = GetHomeDir();

		strFile = s.m_strData + _T("\\") + strFile + _T(" [") + dlg.m_sLine + _T("].txt");
		TRACEF(strFile);
	}

	if (!ItiLibrary::Export (dlg.m_lv, strFile)) {
		CString str;

		str.Format (LoadString (IDS_EXPORTFAILED), strFile);
		MsgBox (str);
	}

	dlg.DestroyWindow ();
}

void CPrintReportDlg::OnFileSettings() 
{
	CReportSettingsDlg dlg (m_db, _T ("CPrintReportDlg"), this);
	
	if (dlg.DoModal () == IDOK) {
		FoxjetDatabase::EnablePrintReportPurge (dlg.m_bEnable ? true : false, dlg.m_nDays);
	}
}

