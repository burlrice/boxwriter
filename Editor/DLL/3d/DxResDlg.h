#if !defined(AFX_DXRESDLG_H__B07766DA_8ADC_43BB_8296_8D282AC4EB2D__INCLUDED_)
#define AFX_DXRESDLG_H__B07766DA_8ADC_43BB_8296_8D282AC4EB2D__INCLUDED_

#include "3dApi.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DxResDlg.h : header file
//

#include "Utils.h"

namespace Foxjet3d
{
	/////////////////////////////////////////////////////////////////////////////
	// CDxResDlg dialog

	class _3D_API CDxResDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CDxResDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CDxResDlg)
		int		m_nRes;
		//}}AFX_DATA


	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CDxResDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual void OnOK();

		// Generated message map functions
		//{{AFX_MSG(CDxResDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace Foxjet3d

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DXRESDLG_H__B07766DA_8ADC_43BB_8296_8D282AC4EB2D__INCLUDED_)
