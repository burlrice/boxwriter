#ifndef __RUNTIMEEXT_H__
#define __RUNTIMEEXT_H__

#define NESTED_RUNTIME_CLASS(container, class_name) \
	((CRuntimeClass*)(&container##::##class_name::class##class_name))
#define NESTED_STATIC_DOWNCAST(container, class_name, object) \
	((container##::class_name*)AfxStaticDownCast(NESTED_RUNTIME_CLASS(container, class_name), object))
#define NESTED_DYNAMIC_DOWNCAST(container, class_name, object) \
	((container##::class_name*)AfxDynamicDownCast(NESTED_RUNTIME_CLASS(container, class_name), object))

#endif //__RUNTIMEEXT_H__
