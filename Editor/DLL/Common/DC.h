#pragma once

namespace FoxjetCommon
{
	class CTmpDC
	{
	public:
		CTmpDC (CWnd * pWnd)
		: m_hwnd (NULL)
		{
			if (pWnd)
				m_hwnd = pWnd->m_hWnd;

			m_hdc = ::GetDC (m_hwnd);
		}

		virtual ~CTmpDC ()
		{
			::ReleaseDC (m_hwnd, m_hdc);
		}

		operator HDC () { return m_hdc; }
		operator CDC * () { return CDC::FromHandle (m_hdc); }

		HWND m_hwnd;
		HDC m_hdc;
	};
};