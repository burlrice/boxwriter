#ifndef __DLLVER_H__
#define __DLLVER_H__

#define FJGETDLLVERSIONPROCADDRESS	_T("fj_GetDllVersion")

#ifdef __cplusplus
namespace FoxjetCommon 
{
#endif //__cplusplus

	typedef struct 
	{
		UINT m_nMajor;
		UINT m_nMinor;
		UINT m_nCustomMajor;
		UINT m_nCustomMinor;
		UINT m_nInternal;
	} VERSIONSTRUCT, * LPVERSIONSTRUCT;

	typedef const VERSIONSTRUCT * LPCVERSIONSTRUCT;

	typedef void (CALLBACK * LPGETDLLVERSION) (LPVERSIONSTRUCT lpVer);

#ifdef __cplusplus
};
#endif //__cplusplus

#endif //__DLLVER_H__
