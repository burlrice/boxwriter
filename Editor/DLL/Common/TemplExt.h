#ifndef __TEMPLEXT_H__
#define __TEMPLEXT_H__

#include <string>
#include <vector>
#include <afxtempl.h>
#include "CopyArray.h"
#include <memory>

#ifdef ARRAYSIZE
	#undef ARRAYSIZE
#endif

#define ARRAYSIZE(s) (sizeof ((s)) / sizeof ((s) [0]))

#define DECLARE_CONSTBASEACCESS(base_class) \
	public: \
	const base_class & GetMembers () const { return * this; } \
	operator const base_class * () const { return this; }; \
	const base_class * operator -> () const { return this; }

template <class T>
class block : public std::unique_ptr <T>
{
public:
	block (long lBytes = 0) 
	:	m_lBytes (lBytes),
		std::unique_ptr <T> (new T [lBytes])
	{	
		memset (get (), 0, lBytes);
	}
	
	block (T * lpData, long lBytes) 
	:	m_lBytes (lBytes),
		std::unique_ptr <T> (new T [lBytes])
	{	
		memcpy (get (), lpData, lBytes);
	}

	operator std::wstring () const
	{
		CString strBin, strFormat, strAscii;
		std::vector <CString> vBin, vAscii;

		strFormat.Format (_T ("%%0%dX "), sizeof (T) * 2);
		vBin.push_back (_T (""));
		vAscii.push_back (_T (""));

		for (int i = 0; i < m_lBytes; i++) {
			ULONG lOffset = &get () [i] - &get () [0];

			if (!(lOffset % 8)) 
				vBin [vBin.size () - 1] += _T (" ");

			if (!(lOffset % 16)) {
				CString strAddr;
				strAddr.Format (_T ("%p "), i);
				vBin.push_back (strAddr);
			}

			strBin.Format (strFormat, get () [i]);
			ASSERT (vBin.size ());
			vBin [vBin.size () - 1] += strBin;
		}

		if (vBin.size () > 1) {
			while (vBin [1].GetLength () > vBin [vBin.size () - 1].GetLength ())
				vBin [vBin.size () - 1] += _T (" ");
		}

		for (int i = 0; i < m_lBytes; i++) {
			ULONG lOffset = &get () [i] - &get () [0];
			int n = (int)get () [i];

			if (!(lOffset % 8)) 
				vAscii [vAscii.size () - 1] += _T (" ");

			if (!(lOffset % 16)) 
				vAscii.push_back (_T (""));

			ASSERT (vAscii.size ());
			vAscii [vAscii.size () - 1] += (n >= ' ' && n <= '~') ? CString ((TCHAR)n) : '.';
		}
					
		CString str = CString (std::to_wstring ((__int64)m_lBytes).c_str ()) + _T (" bytes:");

		ASSERT (vBin.size () == vAscii.size ());
					
		for (int i = 0; i < vBin.size (); i++)  
			str += vBin [i] + _T (" ") + vAscii [i] + _T ("\n");

		str.Trim ();

		return std::wstring (str);
	}

	operator const void * () const { return get (); }
	operator void * () { return get (); }

	operator const T * () const { return get (); }
	operator T * () { return get (); }

	const long m_lBytes;
};

template <class TYPE, class ARG_TYPE>
void Free (CArray <TYPE, ARG_TYPE> & v)
{
	for (int i = 0; i < v.GetSize (); i++)
		delete v [i];

	v.RemoveAll ();
}

template <class T>
void swap (T & lhs, T & rhs)
{
	T temp = lhs;
	lhs = rhs;
	rhs = temp;
}

template <class T>
T BindTo (T value, T lBound, T uBound)
{
	return max (min (value, uBound), lBound);
}

template <class TYPE, class ARG_TYPE>
void Copy (CArray <TYPE, ARG_TYPE> & dest, CArray <TYPE, ARG_TYPE> & src)
{
	dest.RemoveAll ();
	dest.Copy (src);

//	for (int i = 0; i < src.GetSize (); i++) 
//		dest.Add (src [i]);
}

// this assumes TYPE and ARG_TYPE have inequality operators
template <class TYPE, class ARG_TYPE>
bool operator == (CArray <TYPE, ARG_TYPE> & lhs, CArray  <TYPE, ARG_TYPE> & rhs)
{
	if (lhs.GetSize () == rhs.GetSize ()) {
		for (int i = 0; i < lhs.GetSize (); i++) 
			if (lhs [i] != rhs [i])
				return false;

		return true;
	}

	return false;
}

template <class TYPE, class ARG_TYPE>
bool operator != (CArray <TYPE, ARG_TYPE> & lhs, CArray  <TYPE, ARG_TYPE> & rhs)
{
	return !(rhs == lhs);
}

// this assumes ARG_TYPE has an equality operator
template <class TYPE, class ARG_TYPE>
int Find (CArray <TYPE, ARG_TYPE> & v, ARG_TYPE e, bool (* pCompare)(const LPVOID pLhs, const LPVOID pRhs))
{
	ASSERT (pCompare);

	for (int i = 0; i < v.GetSize (); i++) {
		ARG_TYPE t = v [i];

		if ((* pCompare) (t, e))
			return i;
	}

	return -1;
}

template <class TYPE, class ARG_TYPE>
int Find (CArray <TYPE, ARG_TYPE> & v, ARG_TYPE e)
{
	for (int i = 0; i < v.GetSize (); i++) {
		ARG_TYPE t = v [i];

		if (t == e)
			return i;
	}

	return -1;
}

template <class TYPE, class ARG_TYPE>
void InsertAscending (CArray <TYPE, ARG_TYPE> & v, TYPE value) 
{
	for (int i = 0; i < v.GetSize (); i++) {
		if (value <= v [i]) {
			v.InsertAt (i, value);
			return;
		}
	}

	v.Add (value);
}

template <class TYPE>
void InsertAscending (std::vector <TYPE> & v, TYPE value) 
{
	for (int i = 0; i < v.size (); i++) {
		if (value <= v [i]) {
			v.insert (v.begin () + i, value);
			return;
		}
	}

	v.push_back (value);
}

inline void InsertAscending (CStringArray & v, const CString & str) 
{
	for (int i = 0; i < v.GetSize (); i++) {
		if (str <= v [i]) {
			v.InsertAt (i, str);
			return;
		}
	}

	v.Add (str);
}

template <class TYPE, class ARG_TYPE>
FoxjetCommon::CCopyArray <TYPE, ARG_TYPE> MergeAcending (CArray <TYPE, ARG_TYPE> & v1, CArray <TYPE, ARG_TYPE> & v2)
{
	FoxjetCommon::CCopyArray <TYPE, ARG_TYPE> vResult;

	for (int i = 0; i < v1.GetSize (); i++) 
		InsertAscending (vResult, v1 [i]);

	for (int i = 0; i < v2.GetSize (); i++) 
		InsertAscending (vResult, v2 [i]);

	return vResult;
};

inline int Find (const CStringArray & v, const CString & str, bool bCaseSensitive = true)
{
	if (bCaseSensitive) {
		for (int i = 0; i < v.GetSize (); i++) 
			if (!str.Compare (v [i]))
				return i;
	}
	else {
		for (int i = 0; i < v.GetSize (); i++) 
			if (!str.CompareNoCase (v [i]))
				return i;
	}

	return -1;
}

#ifdef UNICODE
inline int Find (const std::vector <std::wstring> & v, const std::wstring & str, bool bCaseSensitive = true)
{
	if (bCaseSensitive) {
		for (int i = 0; i < v.size (); i++) 
			if (!_tcscmp (str.c_str (), v [i].c_str ()))
				return i;
	}
	else {
		for (int i = 0; i < v.size (); i++) 
			if (!_tcsicmp (str.c_str (), v [i].c_str ()))
				return i;
	}

	return -1;
}
#endif

inline BYTE * Find (const LPBYTE pBuffer, ULONG lSize, BYTE nFind)
{
	for (ULONG i = 0; i < lSize; i++)
		if (pBuffer [i] == nFind)
			return &pBuffer [i];

	return NULL;
}

inline LPCTSTR Find (LPCTSTR pBuffer, ULONG lSize, TCHAR nFind)
{
	for (ULONG i = 0; i < lSize; i++)
		if (pBuffer [i] == nFind)
			return &pBuffer [i];

	return NULL;
}

inline int Find (const std::string & s, char c)
{
	for (int i = 0; i < s.length (); i++)
		if (s [i] == c)
			return i;

	return -1;
}

inline int Find (const std::wstring & s, TCHAR c)
{
	for (int i = 0; i < s.length (); i++)
		if (s [i] == c)
			return i;

	return -1;
}

template <class T>
inline int Find (const std::vector <T> & v, const T & p)
{
	for (int i = 0; i < v.size (); i++)
		if (v [i] == p)
			return i;

	return -1;
}

inline void Append (CMapStringToString & dest, const CMapStringToString & src)
{
	for (POSITION pos = src.GetStartPosition (); pos; ) {
		CString strFile, strCount;

		src.GetNextAssoc (pos, strFile, strCount);
		dest.SetAt (strFile, strCount);
	}
}

inline void Copy (CMapStringToString & dest, const CMapStringToString & src)
{
	dest.RemoveAll ();
	Append (dest, src);
}

inline int FindNoCase (const CString & str, const CString & strFind)
{
	CString strTmp = str, strFindTmp = strFind;

	strTmp.MakeLower ();
	strFindTmp.MakeLower ();

	return strTmp.Find (strFindTmp);
}


template <class T>
inline T implode (const std::vector<T> & v, const T & delim)
{
	T result;

	for (UINT i = 0; i < v.size (); i++) {
		result += v [i];

		if (i < (v.size () - 1))
			result += delim;
	}

	return result;
}

inline CString implode (const CStringArray & v, const CString & strDelim = _T (","))
{
	std::vector <std::wstring> a;

	for (int i = 0; i < v.GetSize (); i++)
		a.push_back ((std::wstring)(wchar_t *)(LPCTSTR)v [i]);

	return implode <std::wstring> (a, (std::wstring)(wchar_t *)(LPCTSTR)strDelim).c_str ();
}

template <class T>
CString ToBinString (T n)
{
	if (sizeof (n) == sizeof (__int64)) {
		unsigned __int32 lLO = (unsigned __int32)((n)		& 0xFFFFFFFF);
		unsigned __int32 lHI = (unsigned __int32)((n >> 32)	& 0xFFFFFFFF);
		CString strUpper = ToBinString (lHI);
		CString strLower = ToBinString (lLO);

		return  strUpper + _T (" ") + strLower;
	}
	CString str;


	for (unsigned __int64 i = 0; i < (sizeof (n) * 8); i++) {
		if ((i % 8) == 0)
			str += _T (" ");

		str += (n & (1 << i)) ? _T ("1") : _T ("0");
	}

	str.MakeReverse ();
	str.TrimLeft ();
	str.TrimRight ();

	return str;
}

template <class T>
CString ToHexString (T n)
{
	if (sizeof (n) == sizeof (__int64)) {
		unsigned __int32 lLO = (unsigned __int32)((n)		& 0xFFFFFFFF);
		unsigned __int32 lHI = (unsigned __int32)((n >> 32)	& 0xFFFFFFFF);
		CString strUpper = ToHexString (lHI);
		CString strLower = ToHexString (lLO);

		strLower.Replace (_T ("0x"), _T (""));
		return  strUpper + strLower;
	}

	CString str, strFormat;

	strFormat.Format (_T ("0x%%0%dX"), sizeof (n) * 2);
	str.Format (strFormat, n);

	return str;
}

inline void Scramble (LPBYTE p, DWORD dw)
{
#ifdef UNICODE
	std::vector <std::wstring> v;

	for (int i = 0; i < dw; i++) 
		v.push_back (std::wstring (ToBinString (p [i])));
		
	std::reverse (std::begin (v), std::end (v));

	for (int i = 0; i < v.size (); i++) 
		if (((i + 1) % 191) != 0)
			std::reverse (v [i].begin (), v [i].end ());

	ASSERT (v.size () == dw);

	for (int i = 0; i < v.size (); i++)
		p [i] = (BYTE)_tcstoul (v [i].c_str (), NULL, 2);
#else
	std::vector <std::wstring> v;

	for (int i = 0; i < dw; i++) 
		v.push_back (std::wstring ((CStringW)ToBinString (p [i])));
		
	std::reverse (std::begin (v), std::end (v));

	for (int i = 0; i < v.size (); i++) 
		if (((i + 1) % 191) != 0)
			std::reverse (v [i].begin (), v [i].end ());

	ASSERT (v.size () == dw);

	for (int i = 0; i < v.size (); i++)
		p [i] = (BYTE)wcstoul (v [i].c_str (), NULL, 2);
#endif
}

inline void Unscramble (LPBYTE p, DWORD dw)
{
#ifdef UNICODE
	std::vector <std::wstring> v;

	for (int i = 0; i < dw; i++) 
		v.push_back (std::wstring (ToBinString (p [i])));
		
	for (int i = 0; i < v.size (); i++) 
		if (((i + 1) % 191) != 0)
			std::reverse (v [i].begin (), v [i].end ());

	std::reverse (std::begin (v), std::end (v));
	ASSERT (v.size () == dw);

	for (int i = 0; i < v.size (); i++)
		p [i] = (BYTE)_tcstoul (v [i].c_str (), NULL, 2);
#else
	std::vector <std::wstring> v;

	for (int i = 0; i < dw; i++) 
		v.push_back (std::wstring ((CStringW)ToBinString (p [i])));
		
	for (int i = 0; i < v.size (); i++) 
		if (((i + 1) % 191) != 0)
			std::reverse (v [i].begin (), v [i].end ());

	std::reverse (std::begin (v), std::end (v));
	ASSERT (v.size () == dw);

	for (int i = 0; i < v.size (); i++)
		p [i] = (BYTE)wcstoul (v [i].c_str (), NULL, 2);
#endif
}
#endif //__TEMPLEXT_H__
