#ifndef __COPYARRAY_H__
#define __COPYARRAY_H__

#include <afxcoll.h>
#include <afxtempl.h>

namespace FoxjetCommon
{
	template<class TYPE, class ARG_TYPE = TYPE>
	class CCopyArray : public CArray <TYPE, ARG_TYPE>
	{
	public:
		CCopyArray ();
		CCopyArray (const CCopyArray & rhs);
		CCopyArray & operator = (const CCopyArray & rhs);
		virtual ~CCopyArray ();
	};

	typedef CCopyArray <ULONG, ULONG> CLongArray;

}; // namespace FoxjetCommon

template<class TYPE, class ARG_TYPE>
inline FoxjetCommon::CCopyArray<TYPE, ARG_TYPE>::CCopyArray ()
{
}

template<class TYPE, class ARG_TYPE>
inline FoxjetCommon::CCopyArray<TYPE, ARG_TYPE>::CCopyArray (const FoxjetCommon::CCopyArray<TYPE, ARG_TYPE> & rhs) 
{
	RemoveAll ();

	for (int i = 0; i < rhs.GetSize (); i++) {
		TYPE t = rhs [i];
		Add (t);
	}
}

template<class TYPE, class ARG_TYPE>
inline FoxjetCommon::CCopyArray<TYPE, ARG_TYPE> & FoxjetCommon::CCopyArray<TYPE, ARG_TYPE>::operator = (const FoxjetCommon::CCopyArray<TYPE, ARG_TYPE> & rhs)
{
	if (this != &rhs) {
		RemoveAll ();
		Copy (rhs);
	}

	return * this;
}

template<class TYPE, class ARG_TYPE>
inline FoxjetCommon::CCopyArray<TYPE, ARG_TYPE>::~CCopyArray ()
{
}


#endif //__COPYARRAY_H__
