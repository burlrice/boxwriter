#ifndef __WINMSG_H__
#define __WINMSG_H__

#define ISEDITORRUNNING						(_T ("Foxjet::Editor"))
#define ISEDITORINITIALIZED					(_T ("Foxjet::Editor::Initialized"))
#define ISPRINTERRUNNING					(_T ("Foxjet::Printer"))
#define ISCONFIGRUNNING						(_T ("Foxjet::Config"))
#define ISHOSTRUNNING						(_T ("Foxjet::Host"))
#define ISKEYBOARD							(_T ("Foxjet::Keyboard"))
#define ISKEYBOARDRUNNING					(_T ("Foxjet::Keyboard::Running"))
#define ISKEYBOARDMONITORRUNNING			(_T ("Foxjet::Keyboard monitor"))
#define ISKEYBOARDVISIBLE					(_T ("Foxjet::Keyboard::Visible"))
#define SHUTDOWNAPP							(_T ("Foxjet::Shut down app"))
#define RIGHT_CLICK_DETECTED				(_T ("Foxjet::Right click detected"))
#define CLICK_DETECTED						(_T ("Foxjet::click detected"))
#define APP_INITIALIZED						(_T ("Foxjet::App inititalized"))
#define ISSETUPRUNNING						(_T ("Foxjet::Setup"))
#define ISSKUMGRRUNNING						(_T ("Foxjet::SKU Manager"))

typedef enum 
{
	SHUTDOWN_EDITOR		= 0x0001,
	SHUTDOWN_CONTROL	= 0x0002,
	SHUTDOWN_CONFIG		= 0x0004,
	SHUTDOWN_HOST		= 0x0008,
	SHUTDOWN_ALL		= 0xFFFF,
} SHUTDOWN_TYPE;

#define SYSTEMPARAMCHANGE					(_T ("Foxjet::System params changed"))
											
#define GETPRINTERWND						(_T ("Foxjet::ControlView::m_hWnd"))
#define COMMAPPEDFILE						(_T ("ComMappedFile"))
#define COMMAPPEDFILESIZE					(0x0000FFFF)
#define OPENMAPFILE							(_T ("Foxjet::Open map file"))
#define CLOSEMAPFILE						(_T ("Foxjet::Close map file"))
#define SENDMAPPEDCMD						(_T ("Foxjet::Send mapped command"))
#define PRINTERERROR						(_T ("Foxjet::Printer error"))
#define TASKUPDATED							(_T ("Foxjet::Task updated"))
#define PRINTER_SOP							(_T ("Foxjet::SOP"))
#define PRINTER_EOP							(_T ("Foxjet::EOP"))
#define PRINTER_HEAD_STATE_CHANGE			(_T ("Foxjet::Head state change"))
#define EDITOR_REDRAW						(_T ("Foxjet::Editor::Redraw"))
#define CREATE_CAPTION						(_T ("Foxjet::Editor::Create caption"))
#define DBSTARTCONFIGCHANGED				(_T ("Foxjet::Database Start"))
#define SYNCREMOTEHEAD						(_T ("Foxjet::Synchronize remote head"))
#define SYNCREMOTEHEADCOMPLETE				(_T ("Foxjet::Synchronize remote head complete"))
#define SHUTDOWNKEYBOARD					(_T ("Foxjet::Keyboard::Shutdown"))
#define KEYBOARDCHANGE						(_T ("Foxjet::Keyboard::Change"))
#define DISPLAY_MSG							(_T ("Foxjet::Display message"))
#define FOCUS_CHANGED						(_T ("Foxjet::Keyboard::WM_FOCUS_CHANGED"))
#define EDITOR_NEW_TASK						(_T ("Foxjet::Editor::New task")) // sw0867
#define EDITOR_TASK_CHANGE					(_T ("Foxjet::Editor::Tasks changed")) // sw0867
#define EDITOR_OPEN_TASK					(_T ("Foxjet::Editor::Open task")) // sw0867
#define DATABASE_CLOSED						(_T ("Foxjet::Database::Closed")) // sw0867
#define ISKBWINRUNNING						(_T ("Foxjet::KbWin::Running"))
#define SHOWKBWIN							(_T ("Foxjet::KbWin::Visible"))
#define KBWINSETTEXT						(_T ("Foxjet::KbWin::GetText"))
#define KBWINSETCARET						(_T ("Foxjet::KbWin::SetCaret"))
#define KBWINCREATED						(_T ("Foxjet::KbWin::Created"))
#define KB_ON_EN_CHANGE						(_T ("Foxjet::KbWin::ON_EN_CHANGE"))
#define LOCAL_DB_BACKUP						(_T ("Foxjet::LOCAL_DB_BACKUP"))
#define DATABASECONNECTIONLOST				(_T ("Foxjet::DATABASECONNECTIONLOST"))
#define SUSPEND_COMM_THREADS				(_T ("Foxjet::SUSPEND_COMM_THREADS"))


const UINT WM_ISEDITORRUNNING				= ::RegisterWindowMessage (ISEDITORRUNNING);
const UINT WM_ISEDITORINITIALIZED			= ::RegisterWindowMessage (ISEDITORINITIALIZED);
const UINT WM_ISPRINTERRUNNING				= ::RegisterWindowMessage (ISPRINTERRUNNING);
const UINT WM_ISCONFIGRUNNING				= ::RegisterWindowMessage (ISCONFIGRUNNING);
const UINT WM_ISHOSTRUNNING					= ::RegisterWindowMessage (ISHOSTRUNNING);
const UINT WM_ISKEYBOARD					= ::RegisterWindowMessage (ISKEYBOARD);
const UINT WM_ISKEYBOARDRUNNING				= ::RegisterWindowMessage (ISKEYBOARDRUNNING);
const UINT WM_ISKEYBOARDMONITORRUNNING		= ::RegisterWindowMessage (ISKEYBOARDMONITORRUNNING);
const UINT WM_ISKEYBOARDVISIBLE				= ::RegisterWindowMessage (ISKEYBOARDVISIBLE);
const UINT WM_RIGHT_CLICK_DETECTED			= ::RegisterWindowMessage (RIGHT_CLICK_DETECTED);
const UINT WM_FOCUS_CHANGED					= ::RegisterWindowMessage (FOCUS_CHANGED);
const UINT WM_CLICK_DETECTED				= ::RegisterWindowMessage (CLICK_DETECTED);
const UINT WM_ISKBWINRUNNING				= ::RegisterWindowMessage (ISKBWINRUNNING);
const UINT WM_SHOWKBWIN						= ::RegisterWindowMessage (SHOWKBWIN);
const UINT WM_KBWINSETTEXT					= ::RegisterWindowMessage (KBWINSETTEXT);
const UINT WM_KBWINSETCARET					= ::RegisterWindowMessage (KBWINSETCARET);
const UINT WM_KBWINCREATED					= ::RegisterWindowMessage (KBWINCREATED);
const UINT WM_KB_ON_EN_CHANGE 				= ::RegisterWindowMessage (KB_ON_EN_CHANGE);
const UINT WM_ISSETUPRUNNING				= ::RegisterWindowMessage (ISSETUPRUNNING);
const UINT WM_ISSKUMGRRUNNING				= ::RegisterWindowMessage (ISSKUMGRRUNNING);

											
const UINT WM_SHUTDOWNAPP					= ::RegisterWindowMessage (SHUTDOWNAPP);
const UINT WM_SYSTEMPARAMCHANGE				= ::RegisterWindowMessage (SYSTEMPARAMCHANGE);
const UINT WM_SHUTDOWNKEYBOARD				= ::RegisterWindowMessage (SHUTDOWNKEYBOARD);
const UINT WM_DISPLAY_MSG					= ::RegisterWindowMessage (DISPLAY_MSG);
											
const UINT WM_GETPRINTERWND					= ::RegisterWindowMessage (GETPRINTERWND);
const UINT WM_OPENMAPFILE					= ::RegisterWindowMessage (OPENMAPFILE);
const UINT WM_CLOSEMAPFILE					= ::RegisterWindowMessage (CLOSEMAPFILE);
const UINT WM_SENDMAPPEDCMD					= ::RegisterWindowMessage (SENDMAPPEDCMD);
const UINT WM_TASKUPDATED					= ::RegisterWindowMessage (TASKUPDATED);
const UINT WM_PRINTER_SOP					= ::RegisterWindowMessage (PRINTER_SOP);
const UINT WM_PRINTER_EOP					= ::RegisterWindowMessage (PRINTER_EOP);
const UINT WM_EDITOR_REDRAW					= ::RegisterWindowMessage (EDITOR_REDRAW);
const UINT WM_EDITOR_NEW_TASK				= ::RegisterWindowMessage (EDITOR_NEW_TASK); // sw0867
const UINT WM_EDITOR_TASK_CHANGE			= ::RegisterWindowMessage (EDITOR_TASK_CHANGE); // sw0867
const UINT WM_EDITOR_OPEN_TASK				= ::RegisterWindowMessage (EDITOR_OPEN_TASK); // sw0867
const UINT WM_DATABASE_CLOSED				= ::RegisterWindowMessage (DATABASE_CLOSED); // sw0867

const UINT WM_PRINTER_HEAD_STATE_CHANGE		= ::RegisterWindowMessage (PRINTER_HEAD_STATE_CHANGE);
const UINT WM_CREATE_CAPTION				= ::RegisterWindowMessage (CREATE_CAPTION);
const UINT WM_DBSTARTCONFIGCHANGED			= ::RegisterWindowMessage (DBSTARTCONFIGCHANGED);
const UINT WM_SYNCREMOTEHEAD				= ::RegisterWindowMessage (SYNCREMOTEHEAD);
const UINT WM_SYNCREMOTEHEADCOMPLETE		= ::RegisterWindowMessage (SYNCREMOTEHEADCOMPLETE);
const UINT WM_APP_INITIALIZED				= ::RegisterWindowMessage (APP_INITIALIZED);
const UINT WM_LOCAL_DB_BACKUP				= ::RegisterWindowMessage (LOCAL_DB_BACKUP);
const UINT WM_DATABASECONNECTIONLOST		= ::RegisterWindowMessage (DATABASECONNECTIONLOST);
const UINT WM_DEBUG							= ::RegisterWindowMessage (_T ("Foxjet::Debug"));
const UINT WM_INK_CODE_LOCKOUT				= ::RegisterWindowMessage (_T ("Foxjet::Ink code::lockout"));

const UINT WM_KEYBOARDCHANGE				= ::RegisterWindowMessage (KEYBOARDCHANGE);
const UINT WM_SYSTEM_COLOR_CHANGE			= ::RegisterWindowMessage (_T ("Foxjet::colors changed"));
const UINT WM_SUSPEND_COMM_THREADS			= ::RegisterWindowMessage (SUSPEND_COMM_THREADS);

const UINT WM_DEBUG_REGISTER_THREAD			= ::RegisterWindowMessage (_T ("Foxjet::Debug::RegisterThread"));
const UINT WM_DEBUG_ENUM_REGISTERED_THREADS = ::RegisterWindowMessage (_T ("Foxjet::Debug::EnumRegisteredThreads"));

typedef enum 
{
	KBCHANGETYPE_DOCKSTATE = 0,
	KBCHANGETYPE_SHOWSTATE,
	KBCHANGETYPE_DOCKSTATE_DOCK,
	KBCHANGETYPE_DOCKSTATE_UNDOCK,
	KBCHANGETYPE_SHOWSTATE_SHOW,
	KBCHANGETYPE_SHOWSTATE_HIDE,
} KBCHANGETYPE;

#define TM_ERROR_KILLTHREAD		(WM_APP+1)
#define TM_ERROR_RAISED			(WM_APP+2)
#define TM_ERROR_CLEARED		(WM_APP+3)

inline void CopyToMapFile (LPVOID lpMapAddress, const char * psz)
{
	int nLen = strlen (psz);

	ASSERT (nLen < COMMAPPEDFILESIZE);

	memcpy (lpMapAddress, (LPVOID)psz, nLen);
	memset ((LPVOID)((DWORD)lpMapAddress + nLen), 0, 1);
}

#endif //__WINMSG_H__
