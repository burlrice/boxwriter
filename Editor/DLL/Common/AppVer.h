#ifndef __APPVER_H__
#define __APPVER_H__

#pragma once

#include "DllVer.h"

#define __SW0806__		(806)		// RJR 
#define __SW0808__		(808)		// one-to-one scan & shoot / print
#define __SW0810__		(810)		// 40 inch message (PRO/23 channel heads)
#define __SW0811__		(811)		// RJR (SOCK_STREAM)
#define __SW0812__		(812)		// coke (brad)
#define __SW0813__		(813)		// db prompt at task start
#define __SW0814__		(814)		// c128/ean128/c39 / invert head
#define __SW0816__		(816)		// harland - serial shop order# --> task start
#define __SW0818__		(818)		// 50 line / 50 head NET (for phil)
#define __SW0819__		(819)		// country pure test
#define __SW0820__		(820)		// harland - COM1 for scanner/verifier
#define __SW0821__		(821)		// uno's - fix for WYSIWIG barcode issue / 1:1 preview
#define __SW0823__		(823)		// NET database element
#define __SW0826__		(826)		// Sara Lee (speed issue)
#define __SW0828__		(828)		// Chicago Coding/Tech Specialty - global box/pallet count
#define __SW0831__		(831)		// "hold" for exp date
#define __SW0832__		(832)		// bayer - hub digital inputs/relays
#define __SW0833__		(833)		// test app for terry smith
#define __SW0837__		(837)		// beta for internal testing
#define __SW0840__		(840)		// quarter hour codes for Kimberly Clark
#define __SW0842__		(842)		// ean128/c128 fix for Sunnyvale
#define __SW0835__		(835)		// baxter
#define __SW0846__		(846)		// running beta for /IMAGE_ON_EOP
#define __SW0854__		(854)		// running beta for 2.00
#define __SW0861__		(861)		// running beta for HP head
#define __SW0865__		(865)		// custom version for DataMatrix
#define __SW0875__		(875)		// beta for 3.xx fixes
#define __SW0877__		(877)		// external beta for Advanced Foods, etc
#define __SW0879__		(879)		// national starch / watchdog timer
#define __SW0881__		(881)		// internal beta for net NET board
#define __SW0882__		(882)		// /NO_EXPIRE
#define __SW0885__		(885)		// fix for "black box" print in Control
#define __SW0886__		(886)		// Elite beta
#define __SW0887__		(887)		// NEXT print driver
#define __SW0888__		(888)		// trident
#define __SW0890__		(890)		// running beta for last PRO 
#define __SW0893__		(893)		// running beta for ELITE
#define __SW0895__		(895)		
#define __SW0899__		(899)		
#define __SW0900__		(900)		
#define __SW0905__		(905)		
#define __SW0907__		(907)		
#define __SW0908__		(908)		
#define __SW0914__		(914)		
#define __SW0919__		(919)		
#define __SW0921__		(921)		
#define __SW0922__		(922)		
#define __SW0923__		(923)		
#define __SW0925__		(925)
#define __SW0931__		(931)


#define __MAJOR__			4 
#define __MINOR__			20

#define __CUSTOM__			__SW0931__
#define __CUSTOM_MINOR__	0

#define __BUILD__			7
 
////////////////////////////////////////////////////////////////////////////////

//#define __NICELABEL__
#define __CXIMAGE__

#if ((__CUSTOM__ == __SW0806__) || (__CUSTOM__ == __SW0811__))
	#define __RJR__	
#endif

//#if ((__CUSTOM__ == __SW0821__) || (__CUSTOM__ == __SW0814__))
//	#define __WYSIWYGFIX__	
//#endif
#define __WYSIWYGFIX__

#if (__CUSTOM__ == __SW0861__)
	#define __HPHEAD__
#endif

////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
namespace FoxjetCommon 
{
#endif //__cplusplus

	const VERSIONSTRUCT verApp = 
	{ 
		__MAJOR__,					// m_nMajor
		__MINOR__,					// m_nMinor

		__CUSTOM__,					// m_nCustomMajor
		__CUSTOM_MINOR__,			// m_nCustomMinor
		
		__BUILD__,					// m_nInternal
	};

#ifdef __cplusplus
}; //namespace FoxjetCommon
#endif //__cplusplus


#endif //__APPVER_H__
