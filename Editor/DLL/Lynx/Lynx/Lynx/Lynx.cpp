// This is the main DLL file.


#include "stdafx.h"
#include "Lynx.h"
#include <string>
#include <map>

using namespace Diagraph::Lynx;


/*

Debug pre-link step:
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\Debug\Debug\Debug.dll" "$(ProjectDir)..\..\Test\Test\bin\Debug"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\Debug\Debug\Debug.pdb" "$(ProjectDir)..\..\Test\Test\bin\Debug"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\3d\Debug\3d.dll" "$(ProjectDir)..\..\Test\Test\bin\Debug"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\3d\Debug\3d.pdb" "$(ProjectDir)..\..\Test\Test\bin\Debug"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\Database\Debug\Database.dll" "$(ProjectDir)..\..\Test\Test\bin\Debug"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\Database\Debug\Database.pdb" "$(ProjectDir)..\..\Test\Test\bin\Debug"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\Document\Debug\Document.dll" "$(ProjectDir)..\..\Test\Test\bin\Debug"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\Document\Debug\Document.pdb" "$(ProjectDir)..\..\Test\Test\bin\Debug"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\Utils\Debug\Utils.dll" "$(ProjectDir)..\..\Test\Test\bin\Debug"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\Utils\Debug\Utils.pdb" "$(ProjectDir)..\..\Test\Test\bin\Debug"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\ElementList\Win\Debug\ElementList.dll" "$(ProjectDir)..\..\Test\Test\bin\Debug"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\ElementList\Win\Debug\ElementList.pdb" "$(ProjectDir)..\..\Test\Test\bin\Debug"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\..\..\IP\Raster\IP_Debug\Raster.dll" "$(ProjectDir)..\..\Test\Test\bin\Debug"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\..\..\IP\Raster\IP_Debug\Raster.pdb" "$(ProjectDir)..\..\Test\Test\bin\Debug"


Release pre-link step:
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\3d\Release\3d.dll" "$(ProjectDir)..\..\Test\Test\bin\Release"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\3d\Release\3d.pdb" "$(ProjectDir)..\..\Test\Test\bin\Release"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\Database\Release\Database.dll" "$(ProjectDir)..\..\Test\Test\bin\Release"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\Database\Release\Database.pdb" "$(ProjectDir)..\..\Test\Test\bin\Release"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\Document\Release\Document.dll" "$(ProjectDir)..\..\Test\Test\bin\Release"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\Document\Release\Document.pdb" "$(ProjectDir)..\..\Test\Test\bin\Release"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\Utils\Release\Utils.dll" "$(ProjectDir)..\..\Test\Test\bin\Release"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\Utils\Release\Utils.pdb" "$(ProjectDir)..\..\Test\Test\bin\Release"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\ElementList\Win\Release\ElementList.dll" "$(ProjectDir)..\..\Test\Test\bin\Release"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\ElementList\Win\Release\ElementList.pdb" "$(ProjectDir)..\..\Test\Test\bin\Release"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\..\..\IP\Raster\IP_Release\Raster.dll" "$(ProjectDir)..\..\Test\Test\bin\Release"
call "$(ProjectDir)copy.bat" "$(ProjectDir)..\..\..\..\..\IP\Raster\IP_Release\Raster.pdb" "$(ProjectDir)..\..\Test\Test\bin\Release"

*/


CString GetHomeDir()
{
	TCHAR sz[MAX_PATH] = { 0 };

	HMODULE hModule = ::GetModuleHandle(NULL);
	::GetModuleFileName(hModule, sz, ARRAYSIZE(sz) - 1);

	const CString strPath(sz);
	CString strPathLower(sz);
	strPathLower.MakeLower();

	VERIFY(::GetFileTitle(strPath, sz, ARRAYSIZE(sz) - 1) == 0);

	CString strTitle(sz);
	strTitle.MakeLower();

	int nIndex = strPathLower.Find(strTitle);

	ASSERT(nIndex != -1);
	CString str = strPath.Left(nIndex - 1);

	return str;
}

Diagraph::Lynx::RawBitmap::RawBitmap()
:	m_cx (0), 
	m_cy (0)
{
}

//////////////////////////////////////////////////////////////////////////////////////////
Diagraph::Lynx::Preview::Preview()
{
	Initialize();
}

Diagraph::Lynx::Preview::!Preview()
{
	Release();
}

Diagraph::Lynx::Preview::~Preview()
{
}

int Diagraph::Lynx::Preview::Initialize()
{
	int nResult = 0;

	if (HMODULE hDLL = ::LoadLibrary(_T("3d.dll"))) {
		typedef int (CALLBACK * LPFCT) ();

		if (LPFCT lp = (LPFCT)GetProcAddress(hDLL, "InitializeLynxPreview")) 
			nResult = (*lp) ();
	}

	return nResult;
}

int Diagraph::Lynx::Preview::Release()
{
	int nResult = 0;

	if (HMODULE hDLL = ::LoadLibrary(_T("3d.dll"))) {
		typedef int (CALLBACK * LPFCT) ();

		if (LPFCT lp = (LPFCT)GetProcAddress(hDLL, "ReleaseLynxPreview"))
			nResult = (*lp) ();
	}

	return nResult;
}

RawBitmap ^ Diagraph::Lynx::Preview::Draw(System::String ^ str, System::Collections::Generic::Dictionary <System::String ^, System::String ^> ^ remoteFields)
{
	using namespace System::Runtime::InteropServices;

	RawBitmap ^ img = gcnew RawBitmap();

	TRACEF(GetHomeDir());
	//TRACEF (str);

	if (HMODULE hDLL = ::LoadLibrary(_T("3d.dll"))) {
		typedef int (CALLBACK * LPFCT) (const CString & strFile, const CMapStringToString & map, DIBSECTION * pds, BYTE ** pData);

		if (LPFCT lp = (LPFCT)GetProcAddress(hDLL, "DrawLynxPreview")) {
			DIBSECTION ds = { 0 };
			BYTE * pData = NULL;
			CMapStringToString map;

			for each (auto pair in remoteFields)
			{
				map.SetAt (CString (pair.Key), CString(pair.Value));
			}

			int n = (*lp) ((CString)str, map, &ds, &pData);
			TRACEF (std::to_string (n));

			img->m_cx = ds.dsBm.bmWidth;
			img->m_cy = ds.dsBm.bmHeight;
			img->m_pData = gcnew array<Byte> (n);
			Marshal::Copy ((IntPtr)pData, img->m_pData, 0, n);
		}
		else
			TRACEF ("could not locate DrawLynxPreview");

		::FreeLibrary(hDLL);
	}
	else
		TRACEF("could not load 3d.dll");

	return img;
}
