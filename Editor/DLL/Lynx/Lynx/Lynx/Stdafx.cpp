// stdafx.cpp : source file that includes just the standard includes
// Lynx.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

using namespace System;

void MarshalString(String ^ s, std::string& os) {
	using namespace Runtime::InteropServices;
	const char* chars =
		(const char*)(Marshal::StringToHGlobalAnsi(s)).ToPointer();
	os = chars;
	Marshal::FreeHGlobal(IntPtr((void*)chars));
}

void MarshalString(String ^ s, std::wstring& os) {
	using namespace Runtime::InteropServices;
	const wchar_t* chars =
		(const wchar_t*)(Marshal::StringToHGlobalUni(s)).ToPointer();
	os = chars;
	Marshal::FreeHGlobal(IntPtr((void*)chars));
}

void Trace(LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	CString str;

	str.Format(_T("%s(%d): %s\n"), lpszFile, lLine, lpsz);
	std::wstring s = str;

	while (s.length()) {
		std::wstring strSeg = s.substr(0, 512);
		s.erase(0, 512);
		std::wcout << strSeg.c_str();
		::OutputDebugString(strSeg.c_str());
		Console::Write (gcnew String(strSeg.c_str()));
		::Sleep(1);
	}
}

void Trace(System::String ^ str, LPCTSTR lpszFile, ULONG lLine)
{
	std::wstring s;

	MarshalString (str, s);
	Trace (s, lpszFile, lLine);
}

void Trace(const std::string & str, LPCTSTR lpszFile, ULONG lLine)
{
	Trace(CString(a2w(str).c_str()), lpszFile, lLine);
}

void Trace(const std::wstring & str, LPCTSTR lpszFile, ULONG lLine)
{
	Trace(str.c_str(), lpszFile, lLine);
}

std::wstring a2w(const std::string & str)
{
	std::wstring str2(str.length(), L' ');

	std::copy(str.begin(), str.end(), str2.begin());

	return str2;
}

std::string w2a(const std::wstring & str)
{
	std::string str2(str.length(), L' ');

	std::copy(str.begin(), str.end(), str2.begin());

	return str2;
}

CString a2w(const CString & str)
{
	std::string a = std::string((LPCSTR)(LPCTSTR)str);
	std::wstring w = a2w(a);
	return (CString)w.c_str();
}

CString w2a(const CString & str)
{
	std::wstring w = std::wstring((LPCTSTR)str);
	std::string a = w2a(w);
	return (CString)a.c_str();
}

