// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#include <afx.h>
#include <afxwin.h>
#include <iostream>

std::wstring a2w(const std::string & str);
std::string w2a(const std::wstring & str);
CString a2w(const CString & str);
CString w2a(const CString & str);

void Trace(LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine);
void Trace(System::String ^ str, LPCTSTR lpszFile, ULONG lLine);
void Trace(const std::string & str, LPCTSTR lpszFile, ULONG lLine);
void Trace(const std::wstring & str, LPCTSTR lpszFile, ULONG lLine);

#define TRACEF(s) Trace ((s), _T (__FILE__), __LINE__)