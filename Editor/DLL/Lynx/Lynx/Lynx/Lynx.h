// Lynx.h

#pragma once

using namespace System;

namespace Diagraph
{
	namespace Lynx
	{
		public ref class RawBitmap
		{
		public:
			RawBitmap ();

			array<Byte>^ m_pData;
			int m_cx;
			int m_cy;
		};

		public ref class Preview
		{
		public:
			Preview ();
			!Preview();
			~Preview();

			RawBitmap ^ Draw(System::String ^ strFilename, System::Collections::Generic::Dictionary <System::String ^, System::String ^> ^ remoteFields);

		private:
			int Initialize();
			int Release();
		};
	};
}
