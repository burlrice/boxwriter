﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Test
{
    public partial class Form1 : Form
    {
        public Bitmap m_bmp;
        Diagraph.Lynx.Preview m_preview = new Diagraph.Lynx.Preview();

        public Form1()
        {
            InitializeComponent();

            Dictionary<string, string> remoteFields = new Dictionary<string, string>();

            remoteFields.Add("Field 1", "Diagraph");

            string strYML = System.IO.File.ReadAllText(@"C:\Dev\Diagraph\Unified\CHINA5-4900.yml");
            Diagraph.Lynx.RawBitmap img = m_preview.Draw(strYML, remoteFields);

            m_bmp = new Bitmap(img.m_cx, img.m_cy, PixelFormat.Format1bppIndexed);
            var bmpData = m_bmp.LockBits(new Rectangle(0, 0, img.m_cx, img.m_cy), ImageLockMode.ReadWrite, m_bmp.PixelFormat);

            Marshal.Copy(img.m_pData, 0, bmpData.Scan0, img.m_cx * img.m_cy / 8);
            m_bmp.UnlockBits(bmpData);
        }

        ~Form1()
        {
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.DrawImage(m_bmp, 0, 0);
        }
    }
}
