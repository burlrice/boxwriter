#if !defined(AFX_DATAMATRIXPARAMSDLG_H__B04B4D6A_5DD9_4D76_AD80_26D3D269DB18__INCLUDED_)
#define AFX_DATAMATRIXPARAMSDLG_H__B04B4D6A_5DD9_4D76_AD80_26D3D269DB18__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataMatrixParamsDlg.h : header file
//

#include "UpcBarcodeParamsDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CDataMatrixParamsDlg dialog

class CDataMatrixParamsDlg : public CUpcBarcodeParamsDlg
{
// Construction
public:
	CDataMatrixParamsDlg(FoxjetDatabase::HEADTYPE type, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDataMatrixParamsDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	int m_nWidth;
	int m_nHeight;
	int	m_nAlignment;
	int m_nEncoding;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataMatrixParamsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void InvalidatePreview ();
	virtual int GetDefSymbology () const;

	CBitmap m_bmpCaption [7];

	// Generated message map functions
	//{{AFX_MSG(CDataMatrixParamsDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAMATRIXPARAMSDLG_H__B04B4D6A_5DD9_4D76_AD80_26D3D269DB18__INCLUDED_)
