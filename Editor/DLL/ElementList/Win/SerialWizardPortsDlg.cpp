// SerialWizardPortsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SerialWizardPortsDlg.h"
#include "resource.h"
#include "Utils.h"
#include "WinMsg.h"
#include "Extern.h"
#include "Database.h"
#include "ComPropertiesDlg.h"
#include "Comm32.h"
#include "Debug.h"
#include "Parse.h"
#include "EnTabCtrl.h"
#include "Registry.h"

using namespace FoxjetElements;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#define ID_STROKE_COMM_THREAD	1000
#define WM_SERIAL_DATA			WM_USER+1

////////////////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC(CSerialWizardSheet, CBaseWizardSheet)

CSerialWizardSheet::CSerialWizardSheet(CWnd * pParent)
:	m_lLineID (-1),
	m_pdlgPort (NULL),
	m_pdlgEncoding (NULL),
	m_pdlgPrintOnce (NULL),
	m_pdlgIndex (NULL),
	m_pdlgFinish (NULL),
	CBaseWizardSheet (pParent, IDS_SERIALWIZARD)
{
	AddPageToTree (NULL, m_pdlgPort			= new CSerialWizardPortsPage (this));
	//AddPageToTree (NULL, m_pdlgEncoding		= new CSerialWizardEncodingPage (this));
	AddPageToTree (NULL, m_pdlgPrintOnce	= new CSerialWizardPrintOncePage (this));
	AddPageToTree (NULL, m_pdlgIndex		= new CSerialWizardIndexPage (this));
	AddPageToTree (NULL, m_pdlgFinish		= new CSerialWizardFinishPage (this));
}

CSerialWizardSheet::~CSerialWizardSheet()
{
	if (m_pdlgPort)			{ delete m_pdlgPort;		m_pdlgPort		= NULL; }
	if (m_pdlgEncoding)		{ delete m_pdlgEncoding;	m_pdlgEncoding	= NULL; }
	if (m_pdlgPrintOnce)	{ delete m_pdlgPrintOnce;	m_pdlgPrintOnce = NULL; }
	if (m_pdlgIndex)		{ delete m_pdlgIndex;		m_pdlgIndex		= NULL; }
	if (m_pdlgFinish)		{ delete m_pdlgFinish;		m_pdlgFinish	= NULL; }
}


BEGIN_MESSAGE_MAP(CSerialWizardSheet, CBaseWizardSheet)
	ON_WM_DESTROY ()
	ON_WM_TIMER ()
END_MESSAGE_MAP()


BOOL CSerialWizardSheet::OnInitDialog()
{
	SetTimer (ID_STROKE_COMM_THREAD, 30 * 1000, NULL);
	OnTimer (ID_STROKE_COMM_THREAD);
	return CBaseWizardSheet::OnInitDialog ();
}

void CSerialWizardSheet::OnTimer(UINT nIDEvent)
{
	if (nIDEvent == ID_STROKE_COMM_THREAD) {
		if (HWND hwndControl = LocateControlWnd ())
			::SendMessage (hwndControl, WM_SUSPEND_COMM_THREADS, 1, (1 * 60) * 1000);

		return;
	}

	return CBaseWizardSheet::OnTimer (nIDEvent);
}

void CSerialWizardSheet::OnDestroy()
{
	if (HWND hwndControl = LocateControlWnd ())
		::PostMessage (HWND_BROADCAST, WM_SUSPEND_COMM_THREADS, 0, 0);

	return CBaseWizardSheet::OnDestroy ();
}

////////////////////////////////////////////////////////////////////////////////////////////////////

// CSerialWizardPortsPage

CString CPortItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0:
		str.Format (_T ("COM%d"), m_dwPort);
		break;
	case 1:
		str = m_strLine;
		break;
	case 2:
		str = FoxjetDatabase::Format (m_strSampleData, m_strSampleData.GetLength ());
		break;
	}

	return str;
}

IMPLEMENT_DYNAMIC(CSerialWizardPortsPage, CDbWizardBasePage)

CSerialWizardPortsPage::CSerialWizardPortsPage(CSerialWizardSheet * pParent)
:	m_hExit (NULL),
	CWizardBasePage (pParent, IDD_SERIAL_WIZARD_PORTS)
{
}

CSerialWizardPortsPage::~CSerialWizardPortsPage()
{
	KillThreads ();
	::CloseHandle (m_hExit);
}

BOOL CSerialWizardPortsPage::OnSetActive ()
{
	return CWizardBasePage::OnSetActive ();
}

BOOL CSerialWizardPortsPage::OnKillActive ()
{
	CLongArray sel = ItiLibrary::GetCheckedItems (m_lv.GetListCtrl ());

	if (sel.GetSize ()) {
		if (CPortItem * p = (CPortItem *)m_lv.GetCtrlData (sel [0])) {
			SETTINGSSTRUCT s;

			s.m_strKey.Format (_T ("COM%d"), p->m_dwPort);
			m_port.m_dwPort = p->m_dwPort;

			if (GetSettingsRecord (ListGlobals::GetDB (), 0, s.m_strKey, s)) 
				m_port.FromString (s.m_strData);
		}
	}

	return CWizardBasePage::OnKillActive ();
}


BEGIN_MESSAGE_MAP(CSerialWizardPortsPage, CWizardBasePage)
	ON_BN_CLICKED (BTN_BAUD, OnBaud)
	ON_MESSAGE (WM_SERIAL_DATA,	OnSerialData)
	ON_NOTIFY(LVN_ITEMCHANGED, LV_PORTS, OnItemchangedPorts)
END_MESSAGE_MAP()


BOOL CSerialWizardPortsPage::OnInitDialog()
{
	using namespace ItiLibrary;

	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;
	CUIntArray vPorts;

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_PORT), 100));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_USAGE), 200));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_SAMPLEDATA), 400));

	m_lv.Create (LV_PORTS, vCols, this, ListGlobals::defElements.m_strRegSection);
	m_lv->SetExtendedStyle (LVS_EX_CHECKBOXES | m_lv->GetExtendedStyle ());

	CWizardBasePage::OnInitDialog ();

	FoxjetCommon::EnumerateSerialPorts (vPorts);

	for (int i = 0; i < vPorts.GetSize (); i++) {
		CPortItem * p = new CPortItem ();
		SETTINGSSTRUCT s;
		CString strKey;

		p->m_dwPort = vPorts [i];
		strKey.Format (_T ("COM%d"), p->m_dwPort);
		p->m_strSampleData = FoxjetDatabase::GetProfileString (ListGlobals::defElements.m_strRegSection + _T ("\\") + a2w (GetRuntimeClass ()->m_lpszClassName), strKey, _T (""));

		if (GetSettingsRecord (ListGlobals::GetDB (), 0, strKey, s)) {
			LINESTRUCT line;

			p->FromString (s.m_strData);
			GetLineRecord (ListGlobals::GetDB (), p->m_lLineID, line);
			p->m_strLine = line.m_strName;

			CString strType = p->CCommItem::GetDispText (2);
			TRACEF (strType);

			if (strType.GetLength ())
				p->m_strLine += _T ("\\") + strType;
		}
		
		if (p->m_strLine.IsEmpty () || p->m_type == NOTUSED)
			p->m_strLine = LoadString (IDS_AVAILABLE);

		m_lv.InsertCtrlData (p);
		m_lv->SetCheck (m_lv.GetDataIndex (p), FALSE);
	}

	if (CSerialWizardSheet * pParent = DYNAMIC_DOWNCAST (CSerialWizardSheet, m_pdlgParent)) {
		for (int i = 0; i < vPorts.GetSize (); i++) {
			if (CPortItem * p = (CPortItem *)m_lv.GetCtrlData (i)) {
				if (p->m_type == REMOTEDEVICE) {
					if (p->m_lLineID == pParent->m_lLineID) {
						m_lv->SetCheck (m_lv.GetDataIndex (p), TRUE);
						break;
					}
				}
			}
		}

		if (!ItiLibrary::GetCheckedItems (m_lv).GetSize ()) 
			m_lv->SetCheck (0, TRUE);
	}

	SetDlgItemText (TXT_BAUD, _T ("9600,8,N,1"));

	StartThreads ();

	return TRUE;
}


// CSerialWizardPortsPage message handlers

void CSerialWizardPortsPage::OnDestroy()
{
	CWizardBasePage::OnDestroy ();
}


static int GetStopBits (const CString & str)
{
	int n = (int)(FoxjetDatabase::_ttof (str) * 10.0);

	switch (n) {
	case 10: return ONESTOPBIT;
	case 15: return ONE5STOPBITS;
	case 12: return TWOSTOPBITS;
	}

	return ONESTOPBIT;
}

static CString GetStopBits (int n)
{
	switch (n) {
	case ONESTOPBIT:		return _T ("1");	
	case ONE5STOPBITS:		return _T ("1.5");	
	case TWOSTOPBITS:		return _T ("2");	
	}

	return _T ("");
}

void CSerialWizardPortsPage::OnBaud ()
{
	CPropertySheet sheet;
	CComPropertiesDlg dlg;
	CStringArray v;
	CString str;

	// device type, line usage, print once, encoding 
	dlg.m_bBaudRateOnly = true;
	sheet.m_psh.dwFlags |= PSH_USECALLBACK;
    sheet.m_psh.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetProc;

	GetDlgItemText (TXT_BAUD, str);

	Tokenize (str, v, ',');

	/*
	9600,8,N,1
	*/

	dlg.m_bWin32		= true;
	dlg.m_lBaud			= _ttoi (GetParam (v, 0, _T ("9600")));
	dlg.m_nDatabits		= _ttoi (GetParam (v, 1, _T ("8")));
	dlg.m_cParity		= GetParam (v, 2, _T ("N")).GetAt (0);
	dlg.m_nStopBits		= GetStopBits (GetParam (v, 3, _T ("1")));
	
	sheet.AddPage (&dlg);
	sheet.SetTitle (LoadString (IDS_SERIALSETTINGS));

	if (sheet.DoModal() == IDOK) {
		str.Format (_T ("%d,%d,%c,%s"), 
			dlg.m_lBaud, dlg.m_nDatabits, dlg.m_cParity, GetStopBits (dlg.m_nStopBits));
		SetDlgItemText (TXT_BAUD, str);
		KillThreads ();
		StartThreads ();
	}
}

typedef struct 
{
	CPortItem * m_pItem;
	HWND m_hWnd;
	HANDLE m_hExit;
	DWORD m_dwWait;
} PORT_FUNC_STRUCT;

static ULONG CALLBACK PortFunc (LPVOID lpData)
{
	PORT_FUNC_STRUCT * pParam = (PORT_FUNC_STRUCT *)lpData;

	ASSERT (pParam);

	::Sleep (pParam->m_dwWait);
	HANDLE hComm = Open_Comport (pParam->m_pItem->m_dwPort, pParam->m_pItem->m_dwBaudRate, pParam->m_pItem->m_nByteSize, pParam->m_pItem->m_parity, pParam->m_pItem->m_nStopBits);

	if (hComm == INVALID_HANDLE_VALUE || hComm == NULL) {
		CString str = _T (""); //LoadString (IDS_FAILEDTOOPEN) + _T (" ") + FormatMessage (::GetLastError ());

		TRACEF (_T ("Failed to open: ") + pParam->m_pItem->ToString ());

		while (!::PostMessage (pParam->m_hWnd, WM_SERIAL_DATA, (WPARAM)pParam->m_pItem->m_dwPort, (LPARAM)new CString (str)))
			::Sleep (0);
	}

	if (hComm != INVALID_HANDLE_VALUE && hComm != NULL) {
		CString strData;

		while (1) {
			BYTE buffer [1024];
			DWORD dwBytesRead = 0;

			if (::WaitForSingleObject (pParam->m_hExit, 0) == WAIT_OBJECT_0) {
				TRACEF (_T ("Close_Comport: ") + pParam->m_pItem->ToString ());
				Close_Comport (hComm);
				break;
			}

			ZeroMemory (buffer, sizeof (buffer));
			Read_Comport (hComm, &dwBytesRead, sizeof (buffer), buffer);

			if (dwBytesRead) {
				for (int i = 0; i < (int)dwBytesRead; i++) {
					if (buffer [i] == 0x0c)
						buffer [i] = '\r';

					strData += (TCHAR)buffer [i];
				}

				if (strData.Find ('\r') != -1) {
					while (!::PostMessage (pParam->m_hWnd, WM_SERIAL_DATA, (WPARAM)pParam->m_pItem->m_dwPort, (LPARAM)new CString (strData)))
						::Sleep (0);

					TRACEF (strData);
					strData.Empty ();
				}
			}

			::Sleep (250);
		}
	}

	delete pParam;

	return 0;
}

void CSerialWizardPortsPage::StartThreads (DWORD dwWait)
{
	FoxjetCommon::StdCommDlg::CCommItem baud;
	CString str;
	CStringArray v;

	GetDlgItemText (TXT_BAUD, str);

	if (!m_hExit)
		m_hExit = ::CreateEvent (NULL, TRUE, FALSE, _T ("CSerialWizardPortsPage::m_hExit"));
	else
		::ResetEvent (m_hExit);

	baud.FromString (str);

	Tokenize (str, v, ',');

	TCHAR c = GetParam (v, 2, _T ("N")).GetAt (0);

	switch (c) {
	case 'N': baud.m_parity = NO_PARITY;	break;
	case 'E': baud.m_parity = EVEN_PARITY;	break;
	case 'O': baud.m_parity = ODD_PARITY;	break;
	}

	baud.m_dwBaudRate	= _ttoi (GetParam (v, 0, _T ("9600")));
	baud.m_nByteSize	= _ttoi (GetParam (v, 1, _T ("8")));
	baud.m_nStopBits	= GetStopBits (GetParam (v, 3, _T ("1")));

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (CPortItem * p = (CPortItem *)m_lv.GetCtrlData (i)) {
			PORT_FUNC_STRUCT * pParam = new PORT_FUNC_STRUCT;
			DWORD dw = 0;

			p->m_dwBaudRate		= baud.m_dwBaudRate;
			p->m_nByteSize		= baud.m_nByteSize;
			p->m_parity			= baud.m_parity;
			p->m_nStopBits		= baud.m_nStopBits;
			p->m_type			= baud.m_type;
			p->m_bPrintOnce		= baud.m_bPrintOnce;

			pParam->m_pItem		= p;
			pParam->m_hWnd		= m_hWnd;
			pParam->m_hExit		= m_hExit;
			pParam->m_dwWait	= dwWait;
			m_vThreads.Add (::CreateThread (NULL, 0, PortFunc, (LPVOID)pParam, 0, &dw));
		}
	}
}

void CSerialWizardPortsPage::KillThreads ()
{ 
	::SetEvent (m_hExit);

	for (int i = 0; i < m_vThreads.GetSize (); i++) {
		DWORD dwWait = ::WaitForSingleObject (m_vThreads [i], 2000);

		if (dwWait == WAIT_OBJECT_0)	TRACEF ("WAIT_OBJECT_0");
		if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
		if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
		if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");

		ASSERT (dwWait == WAIT_OBJECT_0);
	}

	m_vThreads.RemoveAll ();
}

LRESULT CSerialWizardPortsPage::OnSerialData (WPARAM wParam, LPARAM lParam)
{
	DWORD dwPort = wParam;
	
	if (CString * pstr = (CString *)lParam) {
		for (int i = 0; i < m_lv->GetItemCount (); i++) {
			if (CPortItem * p = (CPortItem *)m_lv.GetCtrlData (i)) {
				if (p->m_dwPort == dwPort) {
					CString str;

					p->m_strSampleData = * pstr;
					str.Format (_T ("COM%d"), p->m_dwPort);
					FoxjetDatabase::WriteProfileString (ListGlobals::defElements.m_strRegSection + _T ("\\") + a2w (GetRuntimeClass ()->m_lpszClassName), str, p->m_strSampleData);
					m_lv.UpdateCtrlData (p);
				}

				m_lv->SetCheck (m_lv.GetDataIndex (p), p->m_dwPort == dwPort);
			}
		}

		delete pstr;
	}

	return 0;
}


void CSerialWizardPortsPage::OnItemchangedPorts(NMHDR *pNMHDR, LRESULT *pResult)
{
	if (pResult)
		* pResult = 0;
}

bool CSerialWizardPortsPage::CanAdvance ()
{
	CLongArray sel = ItiLibrary::GetCheckedItems (m_lv);

	if (sel.GetSize ()) {
		if (CPortItem * p = (CPortItem *)m_lv.GetCtrlData (sel [0])) {
			SETTINGSSTRUCT s;

			s.m_strKey.Format (_T ("COM%d"), p->m_dwPort);

			if (GetSettingsRecord (ListGlobals::GetDB (), 0, s.m_strKey, s)) 
				p->FromString (s.m_strData);


			TRACEF (p->CCommItem::GetDispText (2));

			if (p->m_strLine.GetLength ()) {
				switch (p->m_type) {
				case REMOTEDEVICE:
				case NOTUSED:
					break;
				default:
					if (MsgBox (LoadString (IDS_PORTINUSE), LoadString (IDS_WARNING), MB_ICONERROR | MB_YESNO) == IDNO) {
						m_lv->SetCheck (m_lv.GetDataIndex (p), FALSE);
						return false;
					}
					break;
				}
			}
		}
	}

	return sel.GetSize () == 1;
}

CString CSerialWizardPortsPage::GetSampleData () const
{
	CLongArray sel = ItiLibrary::GetCheckedItems (m_lv.GetListCtrl ());

	if (sel.GetSize ()) {
		if (CPortItem * p = (CPortItem *)m_lv.GetCtrlData (sel [0])) 
			return p->m_strSampleData;
	}

	return _T ("");
}

void CSerialWizardPortsPage::SetSampleData (const CString & str)
{
	CLongArray sel = ItiLibrary::GetCheckedItems (m_lv.GetListCtrl ());

	if (sel.GetSize ()) {
		if (CPortItem * p = (CPortItem *)m_lv.GetCtrlData (sel [0])) {
			while (!::PostMessage (m_hWnd, WM_SERIAL_DATA, (WPARAM)p->m_dwPort, (LPARAM)new CString (str)))
				::Sleep (0);
		}
	}
}
