// BuildMaskDlg.h: interface for the CBuildMaskDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BUILDMASKDLG_H__97A2D104_8C96_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_BUILDMASKDLG_H__97A2D104_8C96_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ListCtrlImp.h"
#include "Resource.h"
#include "Utils.h"

class CMask : public ItiLibrary::ListCtrlImp::CItem
{
public:

	CMask ();
	CMask (const CMask & rhs);
	CMask & operator = (const CMask & rhs);
	virtual ~CMask ();

	virtual CString GetDispText (int nColumn) const;

	TCHAR m_cChar;
	int m_nLength;
};

class CBuildMaskDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CBuildMaskDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CBuildMaskDlg ();

// Dialog Data
	//{{AFX_DATA(CBuildMaskDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	CString m_strMask;
	bool m_bEditable;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBuildMaskDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	bool OnEdit (int nIndex);
	virtual void OnOK ();

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

	// Generated message map functions
	//{{AFX_MSG(CBuildMaskDlg)
	afx_msg void OnAdd();
	afx_msg void OnDelete();
	afx_msg void OnDown();
	afx_msg void OnEdit();
	afx_msg void OnUp();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	afx_msg void OnDblclkListCtrl(NMHDR* pNMHDR, LRESULT* pResult);
	
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_BUILDMASKDLG_H__97A2D104_8C96_11D4_915E_00104BEF6341__INCLUDED_)
