// DataMatrixDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DataMatrixDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDataMatrixDlg dialog


CDataMatrixDlg::CDataMatrixDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(CDataMatrixDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDataMatrixDlg)
	m_nLength = 0;
	//}}AFX_DATA_INIT
}


void CDataMatrixDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDataMatrixDlg)
	DDX_Text(pDX, TXT_LENGTH, m_nLength);
	//}}AFX_DATA_MAP
	DDV_MinMaxInt(pDX, m_nLength, 0, INT_MAX);
}


BEGIN_MESSAGE_MAP(CDataMatrixDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDataMatrixDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataMatrixDlg message handlers

