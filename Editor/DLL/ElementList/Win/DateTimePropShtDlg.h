#if !defined(AFX_DATETIMEPROPSHTDLG_H__BFDECDEF_5919_487B_8B2E_B25962C99007__INCLUDED_)
#define AFX_DATETIMEPROPSHTDLG_H__BFDECDEF_5919_487B_8B2E_B25962C99007__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DateTimePropShtDlg.h : header file
//

#include "OdbcDatabase.h"
#include "AppVer.h"
#include "EnTabCtrl.h"

#include "CodesDlg.h"			

/////////////////////////////////////////////////////////////////////////////
// CDateTimePropShtDlg

class CDateTimePropShtDlg : public FoxjetCommon::CEnPropertySheet
{
	DECLARE_DYNAMIC(CDateTimePropShtDlg)

// Construction
public:
	CDateTimePropShtDlg(FoxjetDatabase::COdbcDatabase & db, UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CDateTimePropShtDlg(FoxjetDatabase::COdbcDatabase & db, LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

// Attributes
public:

	static const UINT WM_LINECHANGED;
	static const UINT WM_GETLINEID;

// Operations
public:
	static bool SetCodes (FoxjetDatabase::COdbcDatabase & db, ULONG lLineID, const CodesDlg::CCodes & c, CodesDlg::CCodesArray & v, const CString & strName);
	static CodesDlg::CCodes GetCodes (ULONG lLineID, const CodesDlg::CCodesArray & v);

	ULONG					m_lLineID;
	CodesDlg::CCodesArray	m_vMonthcodes;
	CodesDlg::CCodesArray	m_vHourcodes;
	CodesDlg::CCodesArray	m_vDayOfWeekCodes;
	CodesDlg::CCodesArray	m_vRollover;

	// std // #if __CUSTOM__ == __SW0840__
	CodesDlg::CCodesArray	m_vQuarterHourcodes;
	// std // #endif

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDateTimePropShtDlg)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CDateTimePropShtDlg();

protected:
	virtual void OnOK();
	virtual void OnCancel();

	LRESULT OnLineChanged (WPARAM wParam, LPARAM lParam);
	LRESULT OnGetLineID (WPARAM wParam, LPARAM lParam);

	// Generated message map functions
protected:
	void Init ();

	FoxjetDatabase::COdbcDatabase & m_db;


	//{{AFX_MSG(CDateTimePropShtDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATETIMEPROPSHTDLG_H__BFDECDEF_5919_487B_8B2E_B25962C99007__INCLUDED_)
