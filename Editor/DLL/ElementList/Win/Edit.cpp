#include "stdafx.h"

#include <afxdb.h>

#include "Edit.h"
#include "List.h"
#include "ElementDefaults.h"
#include "Debug.h"
#include "ItiLibrary.h"
#include "Database.h"
#include "Registry.h"
#include "Extern.h"
#include "TemplExt.h"
#include "WinMsg.h"

#include "TextDlg.h"
#include "BitmapDlg.h"
#include "CountDlg.h"
#include "DateTimeDlg.h"
#include "ExpDateDlg.h"
#include "UserDlg.h"
#include "ShiftDlg.h"
#include "BarcodeDlg.h"
#include "AiListDlg.h"
#include "CustomFmtDlg.h"
#include "DatabaseDlg.h"
#include "SerialDlg.h"
#include "Utils.h"
#include "Extern.h"
#include "GlobalBarcodeDlg.h"
#include "DefineShiftDlg.h"
#include "SubElement.h"
#include "CodesDlg.h"
#include "LabelElement.h"
#include "LabelElementDlg.h"
#include "ShapeDlg.h"
#include "DateTimePropShtDlg.h"
#include "Parse.h"
#include "BitmapSettingsDlg.h"

using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace FoxjetElements;

namespace SubElements
{
	CArray <CSubElement *, CSubElement *> vAI;
	CArray <CSubElement *, CSubElement *> vSub;
}; //namespace SubElements

static bool SaveAi (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);
static void LoadAi (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);
static bool SaveSubElements (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);
static void LoadSubElements (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);
static FoxjetElements::CSubElement * FindAi (const CString & strID);
static FoxjetElements::CSubElement * FindSubElement (const CString & strID);

extern CDiagnosticCriticalSection csDays;
extern CodesDlg::CCodesArray vMonthcodes;
extern CodesDlg::CCodesArray vHourcodes;
extern CodesDlg::CCodesArray vDayOfWeekCodes;

typedef FoxjetCommon::CCopyArray <CCountElement *, CCountElement *> CCountArray;

static CCountArray GetCountByName (const CString & strName, FoxjetCommon::CElementListArray * pAllLists)
{
	CCountArray v;

	if (pAllLists)
		for (int nList = 0; nList < pAllLists->GetSize (); nList++) 
			if (FoxjetCommon::CElementList * pList = pAllLists->GetAt (nList)) 
				for (int nElement = 0; nElement < pList->GetSize (); nElement++) 
					if (CCountElement * pCount = DYNAMIC_DOWNCAST (CCountElement, &pList->GetObject (nElement))) 
						if (!strName.CompareNoCase (pCount->GetName ()))
							v.Add (pCount);

	return v;
}

static CCountArray GetPalletCounts (FoxjetCommon::CElementListArray * pAllLists)
{
	CCountArray v;

	if (pAllLists)
		for (int nList = 0; nList < pAllLists->GetSize (); nList++) 
			if (FoxjetCommon::CElementList * pList = pAllLists->GetAt (nList)) 
				for (int nElement = 0; nElement < pList->GetSize (); nElement++) 
					if (CCountElement * pCount = DYNAMIC_DOWNCAST (CCountElement, &pList->GetObject (nElement))) 
						if (pCount->IsPalletCount ())
							v.Add (pCount);

	return v;
}

CArray <FoxjetElements::CSubElement *, FoxjetElements::CSubElement *> & GetAI () 
{
	using namespace SubElements;

	static bool bLoaded = false;

	if (!bLoaded) {
		if (CWnd * p = ::AfxGetMainWnd ())
			p->BeginWaitCursor ();

		LoadAi (GetDB (), CVersion ());
		bLoaded = true;

		if (CWnd * p = ::AfxGetMainWnd ())
			p->EndWaitCursor ();
	}

	return vAI;
}

CArray <FoxjetElements::CSubElement *, FoxjetElements::CSubElement *> & GetSubElements () 
{
	using namespace SubElements;

	static bool bLoaded = false;

	if (!bLoaded) {
		if (CWnd * p = ::AfxGetMainWnd ())
			p->BeginWaitCursor ();

		LoadSubElements (GetDB (), CVersion ());
		bLoaded = true;

		if (CWnd * p = ::AfxGetMainWnd ())
			p->EndWaitCursor ();
	}

	return vSub;
}

static FoxjetElements::CSubElement * FindAi (const CString & strID)
{
	using namespace SubElements;

	for (int i = 0; i < vAI.GetSize (); i++) 
		if (CSubElement * p = vAI [i])
			if (!p->m_strID.CompareNoCase (strID))
				return p;

	return NULL;
}

static FoxjetElements::CSubElement * FindSubElement (const CString & strID)
{
	using namespace SubElements;

	for (int i = 0; i < vSub.GetSize (); i++) 
		if (CSubElement * p = vSub [i])
			if (!p->m_strID.CompareNoCase (strID))
				return p;

	return NULL;
}

static bool DeleteSubElement (const CString & strID, CArray <CSubElement *, CSubElement *> & v)
{
	for (int i = 0; i < v.GetSize (); i++) {
		if (CSubElement * p = v [i]) {
			if (!p->m_strID.CompareNoCase (strID)) {
				v.RemoveAt (i);
				delete p;

				return true;
			}
		}
	}

	return false;
}

ELEMENT_API CString FoxjetCommon::GetBitmapEditorPath ()
{
	LPCTSTR lpszRoot = _tgetenv (_T ("SystemRoot"));
	CString strPath = CString (lpszRoot ? lpszRoot : _T ("%SystemRoot%")) + _T ("\\System32\\mspaint.exe");

	return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER,
		ListGlobals::defElements.m_strRegSection + _T ("\\Bitmap editor"), 
		_T ("Path"), strPath);
}


void ELEMENT_API CALLBACK FoxjetElements::OnDefineBitmapEditor(CWnd * pParent, const CVersion & version) 
{
	if (CBitmapSettingsDlg ().DoModal () == IDOK) 
		FoxjetDatabase::NotifySystemParamChange (ListGlobals::GetDB ());
}

void ELEMENT_API CALLBACK FoxjetElements::OnDefineLabelEditor(CWnd * pParent, const CVersion & version) 
{
	CString strApp = CLabelElement::GetEditorPath ();
	CFileDialog dlg (TRUE, _T ("*.exe"), strApp, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("Applications (*.exe)|*.exe|All files (*.*)|*.*||"), pParent);

	if (dlg.DoModal () == IDOK) {
		strApp = dlg.GetPathName ();
		VERIFY (CLabelElement::SetEditorPath (strApp));
	}
}

void ELEMENT_API CALLBACK FoxjetElements::OnDefineCustomdatetimeformats(CWnd * pParent, const CVersion & version) 
{
	CCustomFmtDlg dlg (-1, pParent);
	CStringArray v;
	
	LoadCustomDateTimeFmts (defElements.m_strRegSection, _T ("Custom date/time formats"), v);
	dlg.SetData (v);

	if (dlg.DoModal () == IDOK) {
		v.RemoveAll ();
		dlg.GetData (v);
		SaveCustomDateTimeFmts (defElements.m_strRegSection, _T ("Custom date/time formats"), v);
	}
}

void ELEMENT_API CALLBACK FoxjetElements::OnDefineMonthHourCodes (CWnd * pParent, const FoxjetCommon::CVersion & version)
{
	const CString strSection = defElements.m_strRegSection + _T ("\\CDefineShiftDlg");
	COdbcDatabase & db = ListGlobals::GetDB ();

	CDateTimePropShtDlg dlg (GetDB (), LoadString (IDS_DATETIMECODES), pParent);

	if (dlg.DoModal () == IDOK)
		FoxjetDatabase::NotifySystemParamChange (db);
	
	return;
}

bool ELEMENT_API CALLBACK FoxjetElements::OnDefineShiftCodes (CWnd * pParent, const CVersion & version) 
{
	using namespace DefineShiftDlg;

	CDefineShiftDlg dlg (pParent);
	COdbcDatabase & db = ListGlobals::GetDB ();
	const CString strSection = defElements.m_strRegSection + _T ("\\CDefineShiftDlg");
	bool bResult = false;

	dlg.m_lLineID = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("LineID"), -1);

	if (dlg.DoModal () == IDOK) {
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("LineID"), dlg.m_lLineID);
		FoxjetDatabase::NotifySystemParamChange (db);
		bResult = true;
	}

	return bResult;
}

bool ELEMENT_API CALLBACK FoxjetElements::OnEditTextElement (CBaseElement & element, 
															 const FoxjetCommon::CElementList * pList, 
															 CWnd * pParent, 
															 const CSize & pa, UNITS units,
															 bool bReadOnlyElement, bool bReadOnlyLocation,
															 FoxjetCommon::CElementListArray * pAllLists,
															 FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == TEXT);
	CTextElement & e = * (CTextElement *)&element;
	CTextDlg dlg (e, pa, pList, pParent);
	CWinApp * pApp = ::AfxGetApp ();
	bool bStatus = false;
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;

	dlg.m_bReadOnly			= bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units				= units;
	dlg.m_pAllLists			= pAllLists;
	dlg.Get (e, pHead);

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), dlg.m_bPreview);

		dlg.Set (e, pHead);
		e.Build (pAllLists);
		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);

		bStatus = true;
	}

	if (dlg.IsDefault ()) {
		if (CTextElement * pDefault = DYNAMIC_DOWNCAST (CTextElement, CopyElement (&e, pHead, true))) {
			dlg.Set (* pDefault, pHead);
			pDefault->Build (pAllLists);
			pDefault->Build ();
			defElements.SetElement (pDefault->GetClassID (), pDefault->ToString (verApp), verApp);
			delete pDefault;
		}
	}

	return bStatus;
}

static CString GetFilePath (const CString & str) 
{
	int nIndex = str.ReverseFind ('\\');

	if (nIndex != -1)
		return str.Left (nIndex);

	return _T ("");
}

bool ELEMENT_API CALLBACK FoxjetElements::OnEditBitmapElement (CBaseElement & element, 
															   const FoxjetCommon::CElementList * pList, 
															   CWnd * pParent, 
															   const CSize & pa, UNITS units,
															   bool bReadOnlyElement, bool bReadOnlyLocation,
															   FoxjetCommon::CElementListArray * pAllLists,
															   FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (pList);
	ASSERT (element.GetClassID () == BMP);
	CBitmapElement & e = * (CBitmapElement *)&element;
	CBitmapDlg dlg (e, pa, pList, pParent);
	const FoxjetDatabase::HEADSTRUCT & head = pList->GetHead ();
	bool bStatus = false;
	CWinApp * pApp = ::AfxGetApp ();
	const CString strFilePath = e.GetFilePath ();

	dlg.m_bReadOnly			= bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units				= units;
	dlg.Get (e, &head);

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CBitmapDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CBitmapDlg"), _T ("Preview"), dlg.m_bPreview);

		if (IsNetworked ()) {
			if (head.m_strName == GetDefaultHead ().m_strName) {
				if (strFilePath.CompareNoCase (dlg.m_strFilePath) != 0) {
					MsgBox (* pParent, LoadString (IDS_CHANGEBITMAPPATH));
					return false;
				}
			}
		}


		TRACEF (ToString ((int)e.GetSize (head).cx) + _T (", ") + ToString ((int)e.GetSize (head).cy));
		TRACEF (ToString (dlg.m_size.cx) + _T (", ") + ToString (dlg.m_size.cy));

		dlg.Set (e, &head);

		e.Build ();

		// Build can change size if db prompted
		e.SetSize (dlg.m_size, head);

		if (e.IsDatabaseLookup ())
			e.SetSize (e.CalcDatabasePromptedSize (e.GetFilePath ()), head);

		if (pUpdate)
			pUpdate->Add (&element);
		
		bStatus = true;
	}

	if (dlg.IsDefault ()) {
		if (CBitmapElement * pDefault = DYNAMIC_DOWNCAST (CBitmapElement, CopyElement (&e, &head, true))) {
			dlg.Set (* pDefault, &head);
			pDefault->Build ();
			defElements.SetElement (pDefault->GetClassID (), pDefault->ToString (verApp), verApp);
			delete pDefault;
		}
	}

	return bStatus;
}

bool ELEMENT_API CALLBACK FoxjetElements::OnEditCountElement (CBaseElement & element, 						 
															  const FoxjetCommon::CElementList * pList, 
															  CWnd * pParent, 
															  const CSize & pa, UNITS units,
															  bool bReadOnlyElement, bool bReadOnlyLocation,
															  FoxjetCommon::CElementListArray * pAllLists,
															  FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == COUNT);
	CCountElement & e = * (CCountElement *)&element;
	CCountDlg dlg (e, pa, pList, pParent);
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	bool bStatus = false;
	CWinApp * pApp = ::AfxGetApp ();

	dlg.m_bReadOnly			= bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units				= units;
	dlg.m_pAllLists			= pAllLists;
	dlg.Get (e, pHead);

	if (!GetPalletCounts (pAllLists).GetCount ())
		dlg.m_bIrregularPalletSize = false;
	
	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CCountDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CCountDlg"), _T ("Preview"), dlg.m_bPreview);

		CCountArray v = GetCountByName (dlg.m_strName, pAllLists); 

		e.SetID (dlg.m_nID);
		e.SetPos (dlg.m_pt, pHead);
		e.SetFontName (dlg.m_strFont);

		if (Find (v, &e) == -1)
			v.Add (&e);

		if (dlg.m_bMaster) {
			if (pAllLists) {
				for (int nList = 0; nList < pAllLists->GetSize (); nList++) {
					if (FoxjetCommon::CElementList * pList = pAllLists->GetAt (nList)) {
						for (int nElement = 0; nElement < pList->GetSize (); nElement++) {
							if (CCountElement * pCount = DYNAMIC_DOWNCAST (CCountElement, &pList->GetObject (nElement))) {
								pCount->SetMaster (false); // real master will get set below
							}
						}
					}
				}
			}
		}

		for (int i = 0; i < v.GetSize (); i++) {
			CCountElement * p = v [i];

			if (p == &element)
				dlg.Set (* p, pHead);

			p->Build ();

			if (pUpdate)
				pUpdate->Add (p);
		}
		
		bStatus = true;

		if (pAllLists) {
			CCountArray vPallet = GetPalletCounts (pAllLists);

			if (e.IsPalletCount ())
				if (Find (vPallet, &e) == -1)
					vPallet.Add (&e);

			if (vPallet.GetSize () == 0)
				dlg.m_bIrregularPalletSize = false;

			for (int nList = 0; nList < pAllLists->GetSize (); nList++) {
				if (FoxjetCommon::CElementList * pList = pAllLists->GetAt (nList)) {
					for (int nElement = 0; nElement < pList->GetSize (); nElement++) { 
						if (CCountElement * pCount = DYNAMIC_DOWNCAST (CCountElement, &pList->GetObject (nElement))) {
							pCount->SetIrregularPalletSize (dlg.m_bIrregularPalletSize);

							if (Find (v, pCount) == -1)
								v.Add (pCount);
						}
					}
				}
			}
		}
	}

	if (dlg.IsDefault ()) {
		if (CCountElement * pDefault = DYNAMIC_DOWNCAST (CCountElement, CopyElement (&e, pHead, true))) {
			dlg.Set (* pDefault, pHead);
			pDefault->Build ();
			defElements.SetElement (pDefault->GetClassID (), pDefault->ToString (verApp), verApp);
			delete pDefault;
		}
	}

	return bStatus;
}

bool ELEMENT_API CALLBACK FoxjetElements::OnEditDateTimeElement (CBaseElement & element, 						
																 const FoxjetCommon::CElementList * pList, 
																 CWnd * pParent, 
																 const CSize & pa, UNITS units,
																 bool bReadOnlyElement, bool bReadOnlyLocation,
																 FoxjetCommon::CElementListArray * pAllLists,
																 FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == DATETIME);
	CDateTimeElement & e = * (CDateTimeElement *)&element;
	CDateTimeDlg dlg (e, pa, pList, pParent);
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	bool bResult = false;
	CWinApp * pApp = ::AfxGetApp ();

	dlg.m_bReadOnly			= bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units				= units;
	dlg.m_pAllLists			= pAllLists;
	dlg.Get (e, pHead);

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CDateTimeDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CDateTimeDlg"), _T ("Preview"), dlg.m_bPreview);

		dlg.Set (e, pHead);
		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bResult = true;
	}

	if (dlg.IsDefault ()) {
		if (CDateTimeElement * pDefault = DYNAMIC_DOWNCAST (CDateTimeElement, CopyElement (&e, pHead, true))) {
			dlg.Set (* pDefault, pHead);
			pDefault->Build ();
			defElements.SetElement (pDefault->GetClassID (), pDefault->ToString (verApp), verApp);
			delete pDefault;
		}
	}

	return bResult;
}

bool ELEMENT_API CALLBACK FoxjetElements::OnEditExpDateElement (CBaseElement & element, 
																const FoxjetCommon::CElementList * pList, 
																CWnd * pParent,
																const CSize & pa, UNITS units,
																bool bReadOnlyElement, bool bReadOnlyLocation,
																FoxjetCommon::CElementListArray * pAllLists,
																FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == EXPDATE);
	CExpDateElement & e = * (CExpDateElement *)&element;
	CExpDateDlg dlg (e, pa, pList, pParent);
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	bool bResult = false;
	CWinApp * pApp = ::AfxGetApp ();

	dlg.m_bReadOnly			= bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units				= units;
	dlg.m_pAllLists			= pAllLists;
	dlg.Get (e, pHead);

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CExpDateDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CExpDateDlg"), _T ("Preview"), dlg.m_bPreview);

		dlg.Set (e, pHead);
		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bResult = true;
	}

	if (dlg.IsDefault ()) {
		if (CExpDateElement * pDefault = DYNAMIC_DOWNCAST (CExpDateElement, CopyElement (&e, pHead, true))) {
			dlg.Set (* pDefault, pHead);
			pDefault->Build ();
			defElements.SetElement (pDefault->GetClassID (), pDefault->ToString (verApp), verApp);
			delete pDefault;
		}
	}

	return bResult;
}

bool ELEMENT_API CALLBACK FoxjetElements::OnEditUserElement (CBaseElement & element, 
															 const FoxjetCommon::CElementList * pList, 
															 CWnd * pParent, const CSize & pa, UNITS units,
															 bool bReadOnlyElement, bool bReadOnlyLocation,
															 FoxjetCommon::CElementListArray * pAllLists,
															 FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == USER);
	CUserElement & e = * (CUserElement *)&element;
	CUserDlg dlg (e, pa, pList, pParent);
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	bool bStatus = false;
	CWinApp * pApp = ::AfxGetApp ();

	dlg.m_bReadOnly			= bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units				= units;
	dlg.m_pAllLists			= pAllLists;
	dlg.Get (e, pHead);

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CUserDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CUserDlg"), _T ("Preview"), dlg.m_bPreview);

		dlg.Set (e, pHead);
		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bStatus = true;
	}

	if (dlg.IsDefault ()) {
		if (CUserElement * pDefault = DYNAMIC_DOWNCAST (CUserElement, CopyElement (&e, pHead, true))) {
			dlg.Set (* pDefault, pHead);
			pDefault->Build ();
			defElements.SetElement (pDefault->GetClassID (), pDefault->ToString (verApp), verApp);
			delete pDefault;
		}
	}

	return bStatus;
}

bool ELEMENT_API CALLBACK FoxjetElements::OnEditShiftElement (CBaseElement & element, 
															  const FoxjetCommon::CElementList * pList, 
															  CWnd * pParent, 
															  const CSize & pa, UNITS units,
															  bool bReadOnlyElement, bool bReadOnlyLocation,
															  FoxjetCommon::CElementListArray * pAllLists,
															  FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == SHIFTCODE);
	CShiftElement & e = * (CShiftElement *)&element;
	CShiftDlg dlg (e, pa, pList, pParent);
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	bool bStatus = false;
	CWinApp * pApp = ::AfxGetApp ();

	dlg.m_bReadOnly			= bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units				= units;
	dlg.m_pAllLists			= pAllLists;
	dlg.Get (e, pHead);

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CShiftDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CShiftDlg"), _T ("Preview"), dlg.m_bPreview);

		dlg.Set (e, pHead);
		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bStatus = true;
	}

	if (dlg.IsDefault ()) {
		if (CShiftElement * pDefault = DYNAMIC_DOWNCAST (CShiftElement, CopyElement (&e, pHead, true))) {
			dlg.Set (* pDefault, pHead);
			pDefault->Build ();
			defElements.SetElement (pDefault->GetClassID (), pDefault->ToString (verApp), verApp);
			delete pDefault;
		}
	}

	return bStatus;
}

bool ELEMENT_API CALLBACK FoxjetElements::OnEditBarcodeElement (CBaseElement & element, 
																const FoxjetCommon::CElementList * pList, 
																CWnd * pParent, const CSize & pa, UNITS units,
																bool bReadOnlyElement, bool bReadOnlyLocation,
																FoxjetCommon::CElementListArray * pAllLists,
																FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == BARCODE);
	CBarcodeElement & e = * (CBarcodeElement *)&element;
	CBarcodeDlg dlg (e, pa, pList, pParent);
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	bool bResult = false;
	CWinApp * pApp = ::AfxGetApp ();

	dlg.m_bReadOnly			= bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units				= units;
	dlg.m_pAllLists			= pAllLists;
	dlg.Get (e, pHead);

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CBarcodeDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CBarcodeDlg"), _T ("Preview"), dlg.m_bPreview);

		dlg.Set (e, pHead);
		e.Build ();

		{ // to update caption for linked text elements
			CDC dcMem;
			dcMem.CreateCompatibleDC (NULL);
			element.Draw (dcMem, * pHead, true, FoxjetCommon::EDITOR);
		}

		if (pUpdate) {
			pUpdate->Add (&element);

			if (pHead) {
				for (int j = 0; j < pAllLists->GetSize (); j++) {
					FoxjetCommon::CElementList & list = * (pAllLists->GetAt (j));

					if (list.GetHead ().m_lID == pHead->m_lID) {
						for (int i = 0; i < list.GetSize (); i++) {
							if (CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, &list.GetObject (i))) {
								if (p->GetLink ()) {
									p->Build (pAllLists);
									pUpdate->Add (p);
								}
							}
						}
					}
				}
			}
		}

		bResult = true;
	}

	if (dlg.IsDefault ()) {
		if (CBarcodeElement * pDefault = DYNAMIC_DOWNCAST (CBarcodeElement, CopyElement (&e, pHead, true))) {
			dlg.Set (* pDefault, pHead);
			pDefault->Build ();
			defElements.SetElement (pDefault->GetClassID (), pDefault->ToString (verApp), verApp);
			delete pDefault;
		}
	}

	return bResult;
}

bool ELEMENT_API CALLBACK FoxjetElements::OnEditDatabaseElement (CBaseElement & element, 
																 const FoxjetCommon::CElementList * pList, 
																 CWnd * pParent, const CSize & pa, UNITS units,
																 bool bReadOnlyElement, bool bReadOnlyLocation,
																 FoxjetCommon::CElementListArray * pAllLists,
																 FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == DATABASE);
	CDatabaseElement & e = * (CDatabaseElement *)&element;
	CDatabaseDlg dlg (e, pa, pList, pParent);
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	bool bResult = false;
	CWinApp * pApp = ::AfxGetApp ();

	dlg.m_bReadOnly			= bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units				= units;
	dlg.m_pAllLists			= pAllLists;
	dlg.Get (e, pHead);

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CDatabaseDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CDatabaseDlg"), _T ("Preview"), dlg.m_bPreview);

		dlg.Set (e, pHead);
		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bResult = true;
	}

	if (dlg.IsDefault ()) {
		if (CDatabaseElement * pDefault = DYNAMIC_DOWNCAST (CDatabaseElement, CopyElement (&e, pHead, true))) {
			dlg.Set (* pDefault, pHead);
			pDefault->Build ();
			defElements.SetElement (pDefault->GetClassID (), pDefault->ToString (verApp), verApp);
			delete pDefault;
		}
	}

	return bResult;
}

bool ELEMENT_API CALLBACK FoxjetElements::OnEditSerialElement (CBaseElement & element, const FoxjetCommon::CElementList * pList, 
															   CWnd * pParent, const CSize & pa, UNITS units,
															   bool bReadOnlyElement, bool bReadOnlyLocation,
															   FoxjetCommon::CElementListArray * pAllLists,
															   FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == SERIAL);
	CSerialElement & e = * (CSerialElement *)&element;
	CSerialDlg dlg (e, pa, pList, pParent);
	CWinApp * pApp = ::AfxGetApp ();
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	bool bStatus = false;

	dlg.m_bReadOnly			= bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units				= units;
	dlg.m_pAllLists			= pAllLists;
	dlg.Get (e, pHead);

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CSerialDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CSerialDlg"), _T ("Preview"), dlg.m_bPreview);

		dlg.Set (e, pHead);
		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bStatus = true;
	}

	if (dlg.IsDefault ()) {
		if (CSerialElement * pDefault = DYNAMIC_DOWNCAST (CSerialElement, CopyElement (&e, pHead, true))) {
			dlg.Set (* pDefault, pHead);
			pDefault->Build ();
			defElements.SetElement (pDefault->GetClassID (), pDefault->ToString (verApp), verApp);
			delete pDefault;
		}
	}

	return bStatus;
}


bool ELEMENT_API CALLBACK FoxjetElements::OnEditLabelElement (CBaseElement & element, 
															  const FoxjetCommon::CElementList * pList, 
															  CWnd * pParent, const CSize & pa, UNITS units,
															  bool bReadOnlyElement, bool bReadOnlyLocation,
															  FoxjetCommon::CElementListArray * pAllLists,
															  FoxjetCommon::CElementArray * pUpdate)
{
	bool bStatus = false;

	ASSERT (element.GetClassID () == LABEL);
	CLabelElement & e = * (CLabelElement *)&element;
	CLabelElementDlg dlg (e, pa, pList, pParent);
	CWinApp * pApp = ::AfxGetApp ();
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;

	dlg.m_bReadOnly			= bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units				= units;
	dlg.m_pAllLists			= pAllLists;
	dlg.Get (e, pHead);

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CLabelDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CLabelDlg"), _T ("Preview"), dlg.m_bPreview);

		dlg.Set (e, pHead);
		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bStatus = true;
	}

	if (dlg.IsDefault ()) {
		if (CLabelElement * pDefault = DYNAMIC_DOWNCAST (CLabelElement, CopyElement (&e, pHead, true))) {
			dlg.Set (* pDefault, pHead);
			pDefault->Build ();
			defElements.SetElement (pDefault->GetClassID (), pDefault->ToString (verApp), verApp);
			delete pDefault;
		}
	}

	return bStatus;
}

bool ELEMENT_API CALLBACK FoxjetElements::OnDefineBarcodeParams (CWnd * pParent, const FoxjetCommon::CVersion & version)
{
	bool bResult = false;
	const CString strSection = defElements.m_strRegSection + _T ("\\CGlobalBarcodeDlg");
	COdbcDatabase & db = GetDB ();
	CGlobalBarcodeDlg dlg (db, pParent);

	BEGIN_TRANS (db);

	dlg.m_lLineID			= FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("LineID"), -1);
	dlg.m_nHeadType			= (HEADTYPE)FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("HeadType"), HEADTYPE_FIRST);
	dlg.m_nSymbology		= FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("Symbology"), -1);
	dlg.m_nMagnification	= FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("Magnification"), -1);
	dlg.m_nResolution		= FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("Resolution"), 150);

	int nResult = dlg.DoModal ();

	pParent->BeginWaitCursor ();
	
	if (nResult == IDOK) {
		COMMIT_TRANS (db);

		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("LineID"),		dlg.m_lLineID);
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("HeadType"),	dlg.m_nHeadType);
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("Symbology"),	dlg.m_nSymbology);
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("Resolution"),	dlg.m_nResolution);

		FoxjetDatabase::NotifySystemParamChange (db);
		
		bResult = true;
	}
	else {
		ROLLBACK_TRANS (db); // wipe away any changes
	}

	pParent->EndWaitCursor ();

	return bResult;
}

void DeleteAi ()
{
	using namespace SubElements;

	for (int i = 0; i < vAI.GetSize (); i++) 
		if (CSubElement * p = vAI [i])
			delete p;

		vAI.RemoveAll ();
}

void DeleteSubElements ()
{
	using namespace SubElements;

	for (int i = 0; i < vSub.GetSize (); i++) 
		if (CSubElement * p = vSub [i])
			delete p;

		vSub.RemoveAll ();
}

void ELEMENT_API CALLBACK FoxjetElements::OnDefineApplicationIdentifiers (CWnd * pParent, const FoxjetCommon::CVersion & version)
{
	using namespace SubElements;

	if (pParent)
		pParent->BeginWaitCursor ();

	CAiListDlg dlg (pParent);
	dlg.SetData (GetAI ());

	if (dlg.DoModal () == IDOK) {
		for (int i = 0; i < dlg.m_vDeleted.GetSize (); i++) {
			CString strID = dlg.m_vDeleted [i];
			
			VERIFY (DeleteSubElement (strID, vAI));
		}

		for (int i = 0; i < dlg.m_vUpdated.GetSize (); i++) {
			CSubElement * pNew = dlg.m_vUpdated [i];
			
			ASSERT (pNew);

			if (CSubElement * pOld = FindAi (pNew->m_strID)) 
				* pOld = * pNew;
			else
				vAI.Add (new CSubElement (* pNew));
		}

		VERIFY (SaveAi (GetDB (), version));
	}

	if (pParent)
		pParent->EndWaitCursor ();
}

void LoadAi (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	using namespace SubElements;

	const TCHAR N = CSubElement::m_cNumericReq;
	const TCHAR n = CSubElement::m_cNumericOpt;
	const TCHAR A = CSubElement::m_cAlphaReq;
	const TCHAR a = CSubElement::m_cAlphaOpt;
	const TCHAR X = CSubElement::m_cAlphaNumericReq;
	const TCHAR x = CSubElement::m_cAlphaNumericOpt;
	static const CSubElement ai [] = 
	{ //				type	id				mask																			def data				desc
		CSubElement (	TEXT,	_T ("00"),		/* CString (N, 2) + _T (",") + */ CString (N, 18),								CString ('0', 20 - 2),	_T ("Serial Shipping Container Code")),									// n2+nl8
		CSubElement (	TEXT,	_T ("01"),		/* CString (N, 2) + _T (",") + */ CString (N, 14),								CString ('0', 16 - 2),	_T ("EAN Article Number / Shipping Container Code")),					// n2+nl4
		CSubElement (	TEXT,	_T ("02"),		/* CString (N, 2) + _T (",") + */ CString (N, 14),								CString ('0', 16 - 2),	_T ("EAN Article Number of goods contained within another unit")),		// n2+nl4
		CSubElement (	TEXT,	_T ("10"),		/* CString (N, 2) + _T (",") + */ CString (x, 20),								CString ('0', 22 - 2),	_T ("Batch or Lot number")),											// n2+an..20
		CSubElement (	TEXT,	_T ("11"),		/* CString (N, 2) + _T (",") + */ CString (N, 6),								CString ('0', 8  - 2),	_T ("Production date (YYMMDD)")),										// n2+n6
		CSubElement (	TEXT,	_T ("13"),		/* CString (N, 2) + _T (",") + */ CString (N, 6),								CString ('0', 8  - 2),	_T ("Packaging date (YYMMDD)")),										// n2+n6
		CSubElement (	TEXT,	_T ("15"),		/* CString (N, 2) + _T (",") + */ CString (N, 6),								CString ('0', 8  - 2),	_T ("Minimum durability date (YYMMDD)")),								// n2+n6
		CSubElement (	TEXT,	_T ("17"),		/* CString (N, 2) + _T (",") + */ CString (N, 6),								CString ('0', 8  - 2),	_T ("Maximum durability date (YYMMDD)")),								// n2+n6
		CSubElement (	TEXT,	_T ("20"),		/* CString (N, 2) + _T (",") + */ CString (N, 2),								CString ('0', 4  - 2),	_T ("Maximum durability date (YYMMDD)")),								// n2+n2
		CSubElement (	TEXT,	_T ("21"),		/* CString (N, 2) + _T (",") + */ CString (x, 20),								CString ('0', 2  - 2),	_T ("Serial number")),													// n2+an..20
		CSubElement (	TEXT,	_T ("22"),		/* CString (N, 2) + _T (",") + */ CString (x, 29),								CString ('0', 2  - 2),	_T ("HIBCC - quantity, date, batch and link")),							// n2+an..29
		CSubElement (	TEXT,	_T ("23"),		/* CString (N, 3) + _T (",") + */ CString (n, 19),								CString ('0', 3  - 3),	_T ("Lot number (transitional use)")),									// n3+n..l9
		CSubElement (	TEXT,	_T ("240"),		/* CString (N, 3) + _T (",") + */ CString (x, 30),								CString ('0', 3  - 3),	_T ("Additional Product Identification assigned by the manufacturer")),	// n3+an..30
		CSubElement (	TEXT,	_T ("250"),		/* n3						   */ CString (x,30),								CString ('0', 30),		_T ("Secondary Serial Number")),										// n3+an..30		
		CSubElement (	TEXT,	_T ("30"),		/* CString (N, 2) + _T (",") + */ CString (n, 8),								CString ('0', 2  - 2),	_T ("Variable quantity")),												// n2+n..8
		CSubElement (	TEXT,	_T ("301"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Net weight, kilograms")),											// n4+n6
		CSubElement (	TEXT,	_T ("310"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Net Weight, Kilograms")),											// n4+n6
		CSubElement (	TEXT,	_T ("311"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Length or 1st dimension, metres, trade")),							// n4+n6
		CSubElement (	TEXT,	_T ("312"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Width, Diameter or 2nd dimension, metres, trade")),				// n4+n6
		CSubElement (	TEXT,	_T ("312"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Width, Diameter or 2nd dimension, metres, trade")),				// n4+n6
		CSubElement (	TEXT,	_T ("313"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Depth, Thickness, height or 3rd dimension, metres, trade")),		// n4+n6
		CSubElement (	TEXT,	_T ("314"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Area, square metres, trade")),										// n4+n6
		CSubElement (	TEXT,	_T ("315"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Net volume, litres")),												// n4+n6
		CSubElement (	TEXT,	_T ("316"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Net volume, cubic metres")),										// n4+n6
		CSubElement (	TEXT,	_T ("320"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Net weight, pounds")),												// n4+n6
		CSubElement (	TEXT,	_T ("321"),		/* n4						   */ CString (N, 6), 								CString ('0', 6),		_T ("Length or 1st Dimension, Inches")),								// n4+n6
		CSubElement (	TEXT,	_T ("322"),		/* n4						   */ CString (N, 6), 								CString ('0', 6),		_T ("Length or 1st Dimension, Feet")),									// n4+n6
		CSubElement (	TEXT,	_T ("223"),		/* n4						   */ CString (N, 6), 								CString ('0', 6),		_T ("Length or 1st Dimension, Yards")),									// n4+n6
		CSubElement (	TEXT,	_T ("324"),		/* n4						   */ CString (N, 6), 								CString ('0', 6),		_T ("Width, Diameter, or 2nd Dimension, Inches")),						// n4+n6
		CSubElement (	TEXT,	_T ("325"),		/* n4						   */ CString (N, 6), 								CString ('0', 6),		_T ("Width, Diameter, or 2nd Dimension, Feet")),						// n4+n6	
		CSubElement (	TEXT,	_T ("326"),		/* n4						   */ CString (N, 6), 								CString ('0', 6),		_T ("Width, Diameter, or 2nd Dimension, Yards")),						// n4+n6
		CSubElement (	TEXT,	_T ("327"),		/* n4						   */ CString (N, 6), 								CString ('0', 6),		_T ("Depth, Thickness, Height or 3rd Dimension, Inches")),				// n4+n6
		CSubElement (	TEXT,	_T ("328"),		/* n4						   */ CString (N, 6), 								CString ('0', 6),		_T ("Depth, Thickness, Height or 3rd Dimension, Feet")),				// n4+n6
		CSubElement (	TEXT,	_T ("329"),		/* n4						   */ CString (N, 6), 								CString ('0', 6),		_T ("Depth, Thickness, Height or 3rd Dimension, Yards")),				// n4+n6	
		CSubElement (	TEXT,	_T ("330"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Gross weight, kilograms")),										// n4+n6
		CSubElement (	TEXT,	_T ("331"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Length or 1st dimension, metres, logistics")),						// n4+n6
		CSubElement (	TEXT,	_T ("332"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Width, Diameter or 2nd dimension, metres, logistics")),			// n4+n6
		CSubElement (	TEXT,	_T ("333"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Depth, Thickness, height or 3rd dimension, metres, logistics")),	// n4+n6
		CSubElement (	TEXT,	_T ("334"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Area, square metres, logistics")),									// n4+n6
		CSubElement (	TEXT,	_T ("335"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Gross volume, litres")),											// n4+n6
		CSubElement (	TEXT,	_T ("336"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Gross volume, cubic metres")),										// n4+n6
		CSubElement (	TEXT,	_T ("340"),		/* CString (N, 4) + _T (",") + */ CString (N, 6),								CString ('0', 10 - 4),	_T ("Gross weight, pounds")),											// n4+n6
		CSubElement (	TEXT,	_T ("341"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Length or 1st Dimension, Inches, Logistics")),						// n4+n6	
		CSubElement (	TEXT,	_T ("342"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Length or 1st Dimension, Feet, Logistics")),						// n4+n6	
		CSubElement (	TEXT,	_T ("343"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Length or 1st Dimension, Yards, Logistics")),						// n4+n6		
		CSubElement (	TEXT,	_T ("344"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Width, Diameter or 2nd Dimension, Inches, Logistics")),			// n4+n6	
		CSubElement (	TEXT,	_T ("345"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Width, Diameter or 2nd Dimension, Feet, Logistics")),				// n4+n6			
		CSubElement (	TEXT,	_T ("346"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Width, Diameter or 2nd Dimension, Yards, Logistics")),				// n4+n6			
		CSubElement (	TEXT,	_T ("347"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Depth, Thickness, Height or 3rd Dimension, Inches, Logistics")),	// n4+n6		
		CSubElement (	TEXT,	_T ("348"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Depth, Thickness, Height or 3rd Dimension, Feet, Logistics")),		// n4+n6	
		CSubElement (	TEXT,	_T ("349"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Depth, Thickness, Height or 3rd Dimension, Yards, Logistics")),	// n4+n6		
		CSubElement (	TEXT,	_T ("350"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Area, Square Inches")),											// n4+n6				
		CSubElement (	TEXT,	_T ("351"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Area, Square Feet")),												// n4+n6				
		CSubElement (	TEXT,	_T ("352"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Area, Square Yards")),												// n4+n6				
		CSubElement (	TEXT,	_T ("353"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Area, Square Inches, Logistics")),									// n4+n6							
		CSubElement (	TEXT,	_T ("354"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Area, Square Feet, Logistics")),									// n4+n6							
		CSubElement (	TEXT,	_T ("355"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Area, Square Yards, Logistics")),									// n4+n6							
		CSubElement (	TEXT,	_T ("356"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Net Weight, Troy Ounce")),											// n4+n6					
		CSubElement (	TEXT,	_T ("360"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Volume, Quarts")),													// n4+n6			
		CSubElement (	TEXT,	_T ("361"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Volume, Gallons")),												// n4+n6			
		CSubElement (	TEXT,	_T ("362"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Gross Volume, Quarts")),											// n4+n6					
		CSubElement (	TEXT,	_T ("363"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Gross Volume, Gallons")),											// n4+n6								
		CSubElement (	TEXT,	_T ("364"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Volume, Cubic Inches")),											// n4+n6								
		CSubElement (	TEXT,	_T ("365"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Volume, Cubic Feet")),												// n4+n6								
		CSubElement (	TEXT,	_T ("366"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Volume, Cubic Yards")),											// n4+n6								
		CSubElement (	TEXT,	_T ("367"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Gross Volume, Cubic Inches")),										// n4+n6								
		CSubElement (	TEXT,	_T ("368"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Gross Volume, Cubic Feet")),										// n4+n6								
		CSubElement (	TEXT,	_T ("369"),		/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Gross Volume, Cubic Yards")),										// n4+n6								
		CSubElement (	TEXT,	_T ("37"),		/* CString (N, 2) + _T (",") + */ CString (n, 8),								CString ('0', 2  - 2),	_T ("Quantity")),														// n2+n..8
		CSubElement (	TEXT,	_T ("400"),		/* CString (N, 3) + _T (",") + */ CString (x, 30),								CString ('0', 3  - 3),	_T ("Customer's purchase order number")),								// n3+an..30 
		CSubElement (	TEXT,	_T ("410"),		/* CString (N, 3) + _T (",") + */ CString (N, 13),								CString ('0', 16 - 3),	_T ("Ship to (deliver to) location code using EAN-13 or DUNS (Dun & Bradstreet) number with leading zeroes")),	// n3+nl3
		CSubElement (	TEXT,	_T ("411"),		/* CString (N, 3) + _T (",") + */ CString (N, 13),								CString ('0', 16 - 3),	_T ("Bill to (invoice Co) location code using EAN-13 or DUNS (Dun & Bradstreet) number with leading zeroes")),	// n3+nl3
		CSubElement (	TEXT,	_T ("412"),		/* CString (N, 3) + _T (",") + */ CString (N, 13),								CString ('0', 16 - 3),	_T ("Purchase from (location code of the party from whom the goods are purchased) using EAN-13 or DUNS")),		// n3+nl3 
		CSubElement (	TEXT,	_T ("414"),		/* n3						   */ CString (N, 13),								CString ('0', 13),		_T ("BAN Location Code for Physical Identification")),															// n3+nl3 
		CSubElement (	TEXT,	_T ("420"),		/* CString (N, 3) + _T (",") + */ CString (x, 9),								CString ('0', 3  - 3),	_T ("Ship to (deliver to) postal code within a single postal authority")),										// n3+an..9 
		CSubElement (	TEXT,	_T ("421"),		/* CString (N, 3) + _T (",") + */ CString (N, 3) + _T (",") + CString (x, 9),	CString ('0', 6  - 3),	_T ("Ship to (deliver to) postal code with 3-digit ISO country code prefix")),									// n3+n3+an..9
		CSubElement (	TEXT,	_T ("8001"),	/* CString (N, 4) + _T (",") + */ CString (N, 14),								CString ('0', 18 - 4),	_T ("Roll products - width, length, core diameter, direction and splices")),									// n4+nl4
		CSubElement (	TEXT,	_T ("8002"),	/* CString (N, 4) + _T (",") + */ CString (x, 20),								CString ('0', 4  - 4),	_T ("Electronic Serial Number for Cellular Mobile Telephones")),												// n4+an..20 
		CSubElement (	TEXT,	_T ("8003"),	/* n4						   */ CString (N, 14) + CString (x, 16),			CString ('0', 14),		_T ("UPC/EAN Number and Serial Number of Returnable Asset")),													// n4+nl4+an..16				
		CSubElement (	TEXT,	_T ("8005"),	/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Price Per Unit of Measure")),																				// n4+n6						
		CSubElement (	TEXT,	_T ("8100"),	/* n4						   */ CString (N, 6),								CString ('0', 6),		_T ("Coupon Extended Code - Number System Character and Offer")),												// n4+nl+n5					
		CSubElement (	TEXT,	_T ("8101"),	/* n4						   */ CString (N, 10),								CString ('0', 10),		_T ("Coupon Extended Code - Number System Character, Offer, and End of Offer")),								// n4+nl+n5+n4					
		CSubElement (	TEXT,	_T ("8102"),	/* n4						   */ CString (N, 2),								CString ('0', 2),		_T ("Coupon Extended Code - Number System Character preceded by zero")),										// n4+nl+nl					
		CSubElement (	TEXT,	_T ("90"),		/* CString (N, 2) + _T (",") + */ CString (x, 30),								CString ('0', 2  - 2),	_T ("Internal applications")),																					// n2+an..30
		CSubElement (	TEXT,	_T ("91"),		/* CString (N, 2) + _T (",") + */ CString (x, 30),								CString ('0', 2  - 2),	_T ("Internal - Raw material,Packaging,Components")),															// n2+an..30
		CSubElement (	TEXT,	_T ("92"),		/* CString (N, 2) + _T (",") + */ CString (x, 30),								CString ('0', 2  - 2),	_T ("Internal - Raw material,Packaging,Components")),															// n2+an..30 
		CSubElement (	TEXT,	_T ("93"),		/* CString (N, 2) + _T (",") + */ CString (x, 30),								CString ('0', 2  - 2),	_T ("Internal - Product manufacturers")),																		// n2+an..30 
		CSubElement (	TEXT,	_T ("94"),		/* CString (N, 2) + _T (",") + */ CString (x, 30),								CString ('0', 2  - 2),	_T ("Internal - Product manufacturers")),																		// n2+an..30 
		CSubElement (	TEXT,	_T ("95"),		/* CString (N, 2) + _T (",") + */ CString (x, 30),								CString ('0', 2  - 2),	_T ("Internal - Carriers")),																					// n2+an..30 
		CSubElement (	TEXT,	_T ("96"),		/* CString (N, 2) + _T (",") + */ CString (x, 30),								CString ('0', 2  - 2),	_T ("Internal - Carriers")),																					// n2+an..30 
		CSubElement (	TEXT,	_T ("97"),		/* CString (N, 2) + _T (",") + */ CString (x, 30),								CString ('0', 2  - 2),	_T ("Internal - Wholesalers & Retailers")),																		// n2+an..30 
		CSubElement (	TEXT,	_T ("98"),		/* CString (N, 2) + _T (",") + */ CString (x, 30),								CString ('0', 2  - 2),	_T ("Internal - Wholesalers & Retailers")),																		// n2+an..30 
		CSubElement (	TEXT,	_T ("99"),		/* CString (N, 2) + _T (",") + */ CString (x, 30),								CString ('0', 2  - 2),	_T ("Free text")),																								// n2+an..30 
	};
	SETTINGSSTRUCT s;
	CStringArray v;

	DeleteAi (); 
	GetSettingsRecord (db, 0, ListGlobals::Keys::m_strAi, s);
	ParsePackets (s.m_strData, v);

	for (int i = 0; i < v.GetSize (); i++) {
		CString str = v [i];
		CSubElement * p = new CSubElement ();

		if (p->FromString (str, ver)) 
			vAI.Add (p);
		else 
			delete p;
	}

	// default anything that was missing
	for (int i = 0; i < sizeof (ai) / sizeof (ai [0]); i++) 
		if (!FindAi (ai [i].m_strID))
			vAI.Add (new CSubElement (ai [i]));
}

bool SaveAi (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	CArray <CSubElement *, CSubElement *> & vAI = GetAI ();
	SETTINGSSTRUCT s;

	s.m_lLineID = 0;
	s.m_strKey = ListGlobals::Keys::m_strAi;

	for (int i = 0; i < vAI.GetSize (); i++) 
		if (const CSubElement * p = vAI [i])
			s.m_strData += p->ToString (ver);

	if (UpdateSettingsRecord (db, s))
		return true;

	return AddSettingsRecord (db, s);
}

void LoadSubElements (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	using namespace SubElements;

	SETTINGSSTRUCT s;
	CStringArray v;
	CSubElement def [] = 
	{ 
		CSubElement (TEXT,	_T ("DEF"), CString (CSubElement::m_cAnyOpt, 20), CString ('0', 10), LoadString (IDS_DEFBARCODESUBELEMENT)),

		#if __CUSTOM__ == __SW0840__
		CSubElement (TEXT,		_T ("SUB1"),	CString (CSubElement::m_cNumericReq, 3), _T ("000"),	_T ("Product code")),
		CSubElement (TEXT,		_T ("SUB2"),	CString (CSubElement::m_cNumericReq, 1), _T ("0"),		_T ("Line number")),
		CSubElement (DATETIME,	_T ("SUB3"),	CString (CSubElement::m_cNumericReq, 2), _T ("%%0Q"),	_T ("Quarter hour code")),
		#endif 
	};

	DeleteSubElements (); 
	GetSettingsRecord (db, 0, ListGlobals::Keys::m_strSub, s);
	ParsePackets (s.m_strData, v);

	for (int i = 0; i < v.GetSize (); i++) {
		CString str = v [i];
		CSubElement * p = new CSubElement ();

		if (p->FromString (str, ver)) 
			vSub.Add (p);
		else 
			delete p;
	}

	for (int i = 0; i < ARRAYSIZE (def); i++)
		if (!FindSubElement (def [i].m_strID))
			vSub.Add (new CSubElement (def [i]));
}

bool SaveSubElements (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	SETTINGSSTRUCT s;
	CArray <CSubElement *, CSubElement *> & vSub = GetSubElements ();

	s.m_lLineID = 0;
	s.m_strKey = ListGlobals::Keys::m_strSub;

	for (int i = 0; i < vSub.GetSize (); i++) 
		if (const CSubElement * p = vSub [i])
			s.m_strData += p->ToString (ver);

	if (UpdateSettingsRecord (db, s))
		return true;

	return AddSettingsRecord (db, s);
}

void ELEMENT_API CALLBACK FoxjetElements::OnDefineSubelements(CWnd * pParent, const FoxjetCommon::CVersion & version) 
{
	if (pParent)
		pParent->BeginWaitCursor ();

	CSubElementListDlg dlg (pParent);
	CArray <CSubElement *, CSubElement *> & vSub = GetSubElements ();

	dlg.SetData (vSub);

	if (dlg.DoModal () == IDOK) {
		for (int i = 0; i < dlg.m_vDeleted.GetSize (); i++) {
			CString strID = dlg.m_vDeleted [i];
			
			VERIFY (DeleteSubElement (strID, vSub));
		}

		for (int i = 0; i < dlg.m_vUpdated.GetSize (); i++) {
			CSubElement * pNew = dlg.m_vUpdated [i];
			
			ASSERT (pNew);

			if (CSubElement * pOld = FindSubElement (pNew->m_strID)) 
				* pOld = * pNew;
			else
				vSub.Add (new CSubElement (* pNew));
		}

		VERIFY (SaveSubElements (GetDB (), version));
	}

	if (pParent)
		pParent->EndWaitCursor ();
}

bool ELEMENT_API CALLBACK FoxjetElements::OnEditShapeElement (CBaseElement & element, 
															  const FoxjetCommon::CElementList * pList, 
															  CWnd * pParent, const CSize & pa, UNITS units,
															  bool bReadOnlyElement, bool bReadOnlyLocation,
															  FoxjetCommon::CElementListArray * pAllLists,
															  FoxjetCommon::CElementArray * pUpdate)
{
	using namespace ShapeElement;

	bool bStatus = false;

	ASSERT (element.GetClassID () == SHAPE);
	CShapeElement & e = * (CShapeElement *)&element;
	CShapeDlg dlg (e, pa, pList, pParent);
	CWinApp * pApp = ::AfxGetApp ();
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;

	dlg.m_bReadOnly			= bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units				= units;
	dlg.m_pAllLists			= pAllLists;
	dlg.Get (e, pHead);

	if (dlg.DoModal () == IDOK) {
		dlg.Set (e, pHead);
		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bStatus = true;
	}

	if (dlg.IsDefault ()) {
		if (CShapeElement * pDefault = DYNAMIC_DOWNCAST (CShapeElement, CopyElement (&e, pHead, true))) {
			dlg.Set (* pDefault, pHead);
			pDefault->Build ();
			defElements.SetElement (pDefault->GetClassID (), pDefault->ToString (verApp), verApp);
			delete pDefault;
		}
	}

	return bStatus;
}
