// MaskDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SubElement.h"
#include "MaskDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetElements;

/////////////////////////////////////////////////////////////////////////////
// CMaskDlg dialog


CMaskDlg::CMaskDlg(CWnd* pParent /*=NULL*/)
: m_nType (0), m_bVariable (true), FoxjetCommon::CEliteDlg(IDD_EDITMASK, pParent)
{
	//{{AFX_DATA_INIT(CMaskDlg)
	m_nLength = 0;
	m_bVariable = FALSE;
	//}}AFX_DATA_INIT
}


void CMaskDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMaskDlg)
	DDX_Text(pDX, TXT_LENGTH, m_nLength);
	DDV_MinMaxInt(pDX, m_nLength, 0, 65535);
	DDX_Check(pDX, CHK_VARIABLE, m_bVariable);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMaskDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CMaskDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMaskDlg message handlers

BOOL CMaskDlg::OnInitDialog() 
{
	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();
	
	CButton * pAlpha = (CButton *)GetDlgItem (RDO_ALPHA);
	CButton * pNumeric = (CButton *)GetDlgItem (RDO_NUMERIC);
	CButton * pAlphaNumeric = (CButton *)GetDlgItem (RDO_ALPHANUMERIC);
	CButton * pPunct = (CButton *)GetDlgItem (RDO_PUNCTUATION);
	CButton * pAny = (CButton *)GetDlgItem (RDO_ANY);
	CButton * pVar = (CButton *)GetDlgItem (CHK_VARIABLE);

	switch (m_nType) {
	case CSubElement::ALPHA:
		pAlpha->SetCheck (1);
		break;
	case CSubElement::NUMERIC:
		pNumeric->SetCheck (1);
		break;
	//default:
	case CSubElement::ALPHANUMERIC:
		pAlphaNumeric->SetCheck (1);
		break;
	case CSubElement::PUNCTUATION:
		pPunct->SetCheck (1);
		break;
	case CSubElement::ANY:
		pAny->SetCheck (1);
		break;
	}

	return bResult;
}

void CMaskDlg::OnOK()
{
	CButton * pAlpha = (CButton *)GetDlgItem (RDO_ALPHA);
	CButton * pNumeric = (CButton *)GetDlgItem (RDO_NUMERIC);
	CButton * pAlphaNumeric = (CButton *)GetDlgItem (RDO_ALPHANUMERIC);
	CButton * pPunct = (CButton *)GetDlgItem (RDO_PUNCTUATION);
	CButton * pAny = (CButton *)GetDlgItem (RDO_ANY);
	CButton * pVar = (CButton *)GetDlgItem (CHK_VARIABLE);

	if (pAlpha->GetCheck () == 1)
		m_nType = CSubElement::ALPHA;
	else if (pNumeric->GetCheck () == 1)
		m_nType = CSubElement::NUMERIC;
	else if (pAlphaNumeric->GetCheck () == 1)
		m_nType = CSubElement::ALPHANUMERIC;
	else if (pPunct->GetCheck () == 1)
		m_nType = CSubElement::PUNCTUATION;
	else if (pAny->GetCheck () == 1)
		m_nType = CSubElement::ANY;
	else {
		ASSERT (0);
	}

	FoxjetCommon::CEliteDlg::OnOK ();
}

