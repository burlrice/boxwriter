// CodesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "CodesDlg.h"
#include "Extern.h"
#include "TemplExt.h"
#include "DateTimeElement.h"
#include "Debug.h"
#include "EditValueDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace ListGlobals;
using namespace CodesDlg;

CItem::CItem (const CString & str, int nIndex)
:	m_str (str), 
	m_nIndex (nIndex)
{
}

CItem::~CItem ()
{
}

CString CItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0: 
		str.Format (_T ("%d"), m_nIndex);
		break;
	case 1:
		str = m_str;
	}

	return str;
}

int CItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	CItem & cmp = (CItem &)rhs;

	switch (nColumn) {
	case 0:		return m_nIndex - cmp.m_nIndex;
	case 1:		return m_str.Compare (cmp.m_str);
	}

	return 0;
}

CCodes::CCodes ()
{
}

CCodes::CCodes (const CCodes & rhs)
:	m_lLineID (rhs.m_lLineID)
{
	m_vCodes.Append (rhs.m_vCodes);
}

CCodes & CCodes::operator = (const CCodes & rhs)
{
	if (this != &rhs) {
		m_lLineID = rhs.m_lLineID;
		m_vCodes.RemoveAll ();
		m_vCodes.Append (rhs.m_vCodes);
	}

	return * this;
}

CCodes::~CCodes ()
{
}


