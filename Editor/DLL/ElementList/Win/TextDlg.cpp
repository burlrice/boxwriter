// TextDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TextElement.h"
#include "TextDlg.h"
#include "Debug.h"
#include "BarcodeElement.h"
#include "Extern.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace FoxjetElements::TextElement;
using namespace ListGlobals;
using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////
// CTextDlg dialog


CTextDlg::CTextDlg(const CBaseElement & e, const CSize & pa, 
				   const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_dwProcessId (0),
	m_nParagraphGap (0),
	m_lLink (0),
	CBaseTextDlg(e, pa, IDD_TEXT, pList, pParent)
{
	//{{AFX_DATA_INIT(CTextDlg)
	//}}AFX_DATA_INIT
}

CTextDlg::CTextDlg(const CBaseElement & e, const CSize & pa, UINT nID, 
				   const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_dwProcessId (0),
	m_nParagraphGap (0),
	m_lLink (0),
	CBaseTextDlg(e, pa, nID, pList, pParent)
{
}


void CTextDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseTextDlg::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CTextDlg)
	//}}AFX_DATA_MAP

	DDX_Text(pDX, TXT_TEXT, m_strText);
	DDV_MinMaxChars (pDX, m_strText, TXT_TEXT, CTextElement::m_lmtString);
	DDX_Radio (pDX, RDO_ALIGNLEFT, (int &)m_alignPara);

	DDX_Text(pDX, TXT_GAP, m_nParagraphGap);
	DDV_MinMaxInt (pDX, m_nParagraphGap, CTextElement::m_lmtParagraphGap.m_dwMin,  CTextElement::m_lmtParagraphGap.m_dwMax);
}


BEGIN_MESSAGE_MAP(CTextDlg, CBaseTextDlg)
	//{{AFX_MSG_MAP(CTextDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (BTN_CHARMAP, OnCharMap)
	ON_EN_CHANGE (TXT_TEXT, UpdateUI)
	ON_CBN_SELCHANGE(CB_LINK, OnChangeLink)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTextDlg message handlers

BOOL CTextDlg::OnInitDialog() 
{
	if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_LINK)) {
		pCB->SetItemData (pCB->AddString (LoadString (IDS_NOTHING)), 0);

		if (m_pAllLists) {
			for (int j = 0; j < m_pAllLists->GetSize (); j++) {
				FoxjetCommon::CElementList & list = * (m_pAllLists->GetAt (j));

				if (list.GetHead ().m_lID == GetHead ().m_lID) {
					for (int i = 0; i < list.GetSize (); i++) {
						if (CBarcodeElement * p = DYNAMIC_DOWNCAST (CBarcodeElement, &list.GetObject (i))) {
							CString str;

							str.Format (_T ("[%d] %s"), 
								p->GetID (), 
								FoxjetElements::GetSymbology (p->GetSymbology (), GetDB ()));
							int nIndex = pCB->AddString (str);
							pCB->SetItemData (nIndex, p->GetID ());

							if (m_lLink == p->GetID ())
								pCB->SetCurSel (nIndex);
						}
					}
				}
			}
		}

		if (pCB->GetCurSel () == CB_ERR)
			pCB->SetCurSel (0);

		OnChangeLink ();
	}

	CBaseTextDlg::OnInitDialog();
	UpdateUI ();

	return FALSE;
}

int CTextDlg::BuildElement ()
{
	CTextElement & e = GetElement ();
	CString strText, strFont;
	CComboBox * pLink = (CComboBox *)GetDlgItem (CB_LINK);
	
	ASSERT (pLink);

	GetDlgItemText (TXT_TEXT, strText);
//	GetDlgItemText (CB_FONT, strFont);

//	e.SetFontName (strFont);
//	e.SetBold (GetDlgItemInt (TXT_BOLD));
//	e.SetFlippedH (GetCheck (CHK_FLIPH));
//	e.SetFlippedV (GetCheck (CHK_FLIPV));
//	e.SetInverse (GetCheck (CHK_INVERSE));
	e.SetDefaultData (strText);
	e.SetOrientation (GetOrientation ());
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));
	e.SetLink (pLink->GetItemData (pLink->GetCurSel ()));
	e.Build (m_pAllLists);

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

CTextElement & CTextDlg::GetElement ()
{
	CBaseElement & e = CBaseTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CTextElement)));
	return (CTextElement &)e;
}

const CTextElement & CTextDlg::GetElement () const
{
	const CBaseElement & e = CBaseTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CTextElement)));
	return (CTextElement &)e;
}


void CTextDlg::OnCharMap ()
{
	if (m_dwProcessId > 0) 
		FoxjetCommon::ActivateProcess (m_dwProcessId);
	else {
		DWORD dw;

		HANDLE hThread = ::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0,
			(LPTHREAD_START_ROUTINE)CharMapFunc, (LPVOID)this, 0, &dw);

		ASSERT (hThread != NULL);
	}
}

void CTextDlg::UpdateUI()
{
	if (::IsWindow(m_hWnd)) {
		CString str;
		static const UINT nAlign [] = 
		{
			RDO_ALIGNLEFT,	
			RDO_ALIGNCENTER,
			RDO_ALIGNRIGHT,	
			TXT_GAP,
		};

		GetDlgItemText (TXT_TEXT, str);
		bool bParagraph = CTextElement::IsParagraphMode (str);

		for (int i = 0; i < ARRAYSIZE (nAlign); i++) {
			if (CButton * p = (CButton *)GetDlgItem (nAlign [i])) {
				//p->ShowWindow (bParagraph ? SW_SHOW : SW_HIDE);
				p->EnableWindow (bParagraph);
			}
		}

		//GetDlgItem (BTN_CHARMAP)->EnableWindow (m_dwProcessId ? FALSE : TRUE);
		GetDlgItem (IDOK)->EnableWindow (m_dwProcessId ? FALSE : TRUE);
		GetDlgItem (IDCANCEL)->EnableWindow (m_dwProcessId ? FALSE : TRUE);
	}
}

ULONG CALLBACK CTextDlg::CharMapFunc (LPVOID lpData)
{
	ASSERT (lpData);

	CTextDlg & dlg = * (CTextDlg *)lpData;
	CString strApp = _tgetenv (_T ("SystemRoot")) + CString (_T ("\\System32\\charmap.exe"));
	CString strINI = _tgetenv (_T ("SystemRoot")) + CString (_T ("\\win.ini"));
	CString strFont = dlg.GetElement ().GetFontName ();
	CString strCmdLine = strApp;
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [0xFF];

	_tcsncpy (szCmdLine, strCmdLine, 0xFF);
    memset (&pi, 0, sizeof(pi));
    memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	VERIFY (::WritePrivateProfileString (_T ("MSUCE"), _T ("Advanced"), _T ("1"),		strINI));
	VERIFY (::WritePrivateProfileString (_T ("MSUCE"), _T ("CodePage"), _T ("Unicode"), strINI));
	VERIFY (::WritePrivateProfileString (_T ("MSUCE"), _T ("Font"),		strFont,		strINI));

	BOOL bResult = ::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi); 

	dlg.UpdateUI ();

	if (!bResult) {
		MsgBox (dlg, LoadString (IDS_FAILEDTOLAUNCHCHARMAP));
	}
	else {
		bool bMore;
		
		dlg.m_dwProcessId = pi.dwProcessId;

		TRACEF (_T ("begin edit"));

		do {
			DWORD dwExitCode = 0;
			
			dlg.UpdateUI ();

			if (!GetExitCodeProcess (pi.hProcess, &dwExitCode))
				bMore = false;
			else if (!::IsWindow (dlg.m_hWnd))
				bMore = false;
			else {
				bMore = (dwExitCode == STILL_ACTIVE);
				WaitForSingleObjectEx(pi.hProcess, 3000, TRUE);
				TRACEF (_T ("still waiting"));
			}
		}
		while (bMore);

		TRACEF (_T ("edit complete"));
	}

	dlg.m_dwProcessId = 0;
	dlg.UpdateUI ();

	return 0;
}

bool CTextDlg::GetValues ()
{
	CComboBox * pLink = (CComboBox *)GetDlgItem (CB_LINK);
	
	ASSERT (pLink);

	m_lLink = pLink->GetItemData (pLink->GetCurSel ());

	return CBaseTextDlg::GetValues ();
}

void CTextDlg::OnChangeLink ()
{
	CComboBox * pLink = (CComboBox *)GetDlgItem (CB_LINK);
	UINT nID [] = 
	{
		TXT_TEXT,
		BTN_CHARMAP,
	};

	ASSERT (pLink);

	ULONG lLink = pLink->GetItemData (pLink->GetCurSel ());

	for (int i = 0; i < ARRAYSIZE (nID); i++)
		if (CWnd * p = GetDlgItem (nID [i]))
			p->EnableWindow (lLink == 0);

	if (lLink) {
		CComboBox * pLink = (CComboBox *)GetDlgItem (CB_LINK);
		CTextElement & e = GetElement ();
		
		ASSERT (pLink);

		e.SetLink (pLink->GetItemData (pLink->GetCurSel ()));
		e.Build (m_pAllLists);
		SetDlgItemText (TXT_TEXT, e.GetImageData ());
	}
	else
		SetDlgItemText (TXT_TEXT, m_strText);
}

void CTextDlg::Get (const FoxjetElements::TextElement::CTextElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Get (e, pHead);

	m_pt			= e.GetPos (pHead);
	m_nID			= e.GetID ();
	m_bFlipH		= e.IsFlippedH ();
	m_bFlipV		= e.IsFlippedV ();
	m_bInverse		= e.IsInverse ();
	m_strText		= e.GetDefaultData ();
	m_nOrientation	= e.GetOrientation ();
	m_nWidth		= e.GetWidth ();
	m_alignPara		= e.GetParaAlign ();
	m_nParagraphGap	= e.GetParaGap ();
	m_lLink			= e.GetLink ();
}

void CTextDlg::Set (FoxjetElements::TextElement::CTextElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Set (e, pHead);

	e.SetAlignment (m_align);
	e.SetPos (m_pt = e.GetPosInternal (m_pt), pHead, ALIGNMENT_ADJUST_SUBTRACT);
	e.SetID (m_nID);
//	e.SetFlippedH (m_bFlipH ? true : false);
//	e.SetFlippedV (m_bFlipV ? true : false);
//	e.SetInverse (m_bInverse ? true : false);
	e.SetDefaultData (m_strText);
//	e.SetFontName (m_strFont);
//	e.SetBold (m_lBold);
	e.SetOrientation (m_nOrientation);
	e.SetWidth (m_nWidth);
	e.SetParaAlign (m_alignPara);
	e.SetParaGap (m_nParagraphGap);
	e.SetLink (m_lLink);
	e.SetFontName (m_strFont);
}