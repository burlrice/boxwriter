#include "stdafx.h"
#include "SerialWizardPortsDlg.h"
#include "resource.h"
#include "Utils.h"
#include "WinMsg.h"
#include "Extern.h"
#include "Database.h"
#include "ComPropertiesDlg.h"
#include "Comm32.h"
#include "Debug.h"
#include "Parse.h"
#include "SerialSampleDlg.h"
#include "color.h"
#include <string>

using namespace FoxjetElements;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

IMPLEMENT_DYNAMIC(CSerialWizardFinishPage, CWizardBasePage)


BEGIN_MESSAGE_MAP(CSerialWizardFinishPage, CWizardBasePage)
	ON_BN_CLICKED (CHK_YES, OnUpdateUI)
	ON_BN_CLICKED (CHK_NO, OnUpdateUI)
END_MESSAGE_MAP()

CSerialWizardFinishPage::CSerialWizardFinishPage (CSerialWizardSheet * pParent)
:	m_bTemplate (false),
	CWizardBasePage (pParent, IDD_DATABASE_WIZARD_FINISH)
{
}

CSerialWizardFinishPage::~CSerialWizardFinishPage ()
{
}

BOOL CSerialWizardFinishPage::OnInitDialog ()
{
	CWizardBasePage::OnInitDialog ();

	if (CButton * p = (CButton *)GetDlgItem (CHK_YES)) 
		p->SetCheck (1);

	OnUpdateUI ();

	return TRUE;
}

void CSerialWizardFinishPage::OnUpdateUI ()
{
	if (CButton * pYes = (CButton *)GetDlgItem (CHK_YES)) 
		m_bTemplate = (pYes->GetCheck () == 1) ? true : false;
}
