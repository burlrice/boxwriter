// DbFormatDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "list.h"
#include "DbFormatDlg.h"
#include "TemplExt.h"
#include "ItiLibrary.h"
#include "Extern.h"
#include "DateTimeFmtDlg.h"

using namespace ListGlobals;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDbFormatDlg dialog


CDbFormatDlg::CDbFormatDlg(ULONG lLineID, CWnd* pParent /*=NULL*/)
:	m_lLineID (lLineID),
	FoxjetCommon::CEliteDlg(IDD_DBFORMAT, pParent)
{
	//{{AFX_DATA_INIT(CDbFormatDlg)
	m_nLeading = 0;
	m_nTailing = 0;
	m_nRdoType = DEFAULT;
	m_bZeroes = FALSE;
	//}}AFX_DATA_INIT
}


void CDbFormatDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDbFormatDlg)
	DDX_Text(pDX, TXT_FLOAT_LEADING, m_nLeading);
	DDV_MinMaxInt(pDX, m_nLeading, 0, 12);
	DDX_Text(pDX, TXT_FLOAT_TAILING, m_nTailing);
	DDV_MinMaxInt(pDX, m_nTailing, 0, 12);
	DDX_Radio(pDX, RDO_DEFAULT, m_nRdoType);
	DDX_Check(pDX, CHK_ZEROES, m_bZeroes);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDbFormatDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDbFormatDlg)
	ON_BN_CLICKED(RDO_DEFAULT, OnDefault)
	ON_BN_CLICKED(BTN_BUILD, OnBuild)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(RDO_FLOAT, OnDefault)
	ON_BN_CLICKED(RDO_INT, OnDefault)
	ON_BN_CLICKED(RDO_DATETIME, OnDefault)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDbFormatDlg message handlers

BOOL CDbFormatDlg::OnInitDialog() 
{
	const int nType [] = { DEFAULT, INT, FLOAT, DATE_TIME };

	{
		m_bZeroes = m_strFormat.Find (_T ("%0")) != -1;

		switch (m_nType) {
		case INT:
			VERIFY (_stscanf (m_strFormat, _T ("%%%dlu"), &m_nLeading) == 1);
			break;
		case FLOAT:
			VERIFY (_stscanf (m_strFormat, _T ("%%%d.%df"), &m_nLeading, &m_nTailing) == 2);
			break;
		}
	}

	for (int i = 0; i < ARRAYSIZE (nType); i++) {
		if (m_nType == nType [i]) {
			m_nRdoType = i;
			break;
		}
	}

	FoxjetCommon::CEliteDlg::OnInitDialog();

	{
		CComboBox * pCB = (CComboBox *)GetDlgItem (CB_DATETIME);
		CStringArray v;
	
		ASSERT (pCB);
		ItiLibrary::LoadCustomDateTimeFmts (defElements.m_strRegSection, _T ("Custom date/time formats"), v);

		for (int i = 0; i < v.GetSize (); i++) {
			pCB->AddString (v [i]);

			if (m_strFormat == v [i])
				pCB->SetCurSel (i);
		}

		if (pCB->GetCurSel () == CB_ERR) {
			if (m_nType == DATE_TIME) 
				pCB->InsertString (0, m_strFormat);

			pCB->SetCurSel (0);
		}
	}
	
	UpdateUI ();	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDbFormatDlg::UpdateUI()
{
	struct
	{
		UINT m_nID;
		DWORD m_dw;
	} const map [] = 
	{
		{ CHK_ZEROES,			FLOAT | INT },
		{ TXT_FLOAT_LEADING,	FLOAT | INT },
		{ TXT_FLOAT_TAILING,	FLOAT		},
		{ CB_DATETIME,			DATE_TIME	},
		{ BTN_BUILD,			DATE_TIME	},
	};
	const int n [] = { DEFAULT, INT, FLOAT, DATE_TIME };

	if (!UpdateData (TRUE))
		return;

	m_nType = n [m_nRdoType];

	for (int i = 0; i < ARRAYSIZE (map); i++) 
		if (CWnd * p = GetDlgItem (map [i].m_nID)) 
			p->EnableWindow (map [i].m_dw & m_nType);
}

void CDbFormatDlg::OnDefault() 
{
	UpdateUI ();	
}

void CDbFormatDlg::OnBuild() 
{
	CDateTimeFmtDlg dlg (m_lLineID, this);

	GetDlgItemText (CB_DATETIME, dlg.m_strFmt);

	if (dlg.DoModal () == IDOK) 
		SetDlgItemText (CB_DATETIME, dlg.m_strFmt);
}

void CDbFormatDlg::OnOK()
{
	if (!UpdateData (TRUE))
		return;
	
	switch (m_nType) {
	case DEFAULT:
		m_strFormat = _T ("");
		break;
	case INT:
		m_strFormat.Format (_T ("%%%s%dlu"), 
			m_bZeroes ? _T ("0") : _T (""), 
			m_nLeading);
		break;
	case FLOAT:
		m_strFormat.Format (_T ("%%%s%d.%df"), 
			m_bZeroes ? _T ("0") : _T (""), 
			m_nLeading,
			m_nTailing);
		break;
	case DATE_TIME:
		GetDlgItemText (CB_DATETIME, m_strFormat);
		break;
	}

	FoxjetCommon::CEliteDlg::OnOK ();
}
