// MonthsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "MonthsDlg.h"
#include "Extern.h"
#include "SystemDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace CodesDlg;

/////////////////////////////////////////////////////////////////////////////
// CMonthsDlg property page

IMPLEMENT_DYNCREATE(CMonthsDlg, CDaysDlg)

CMonthsDlg::CMonthsDlg(FoxjetDatabase::COdbcDatabase & db, const CCodesArray & vCodes) 
:	m_vCodes (vCodes),
	CDaysDlg (db, IDD_PROP_MONTHS)
{
	//{{AFX_DATA_INIT(CMonthsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CMonthsDlg::~CMonthsDlg()
{
}

void CMonthsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDaysDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMonthsDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMonthsDlg, CDaysDlg)
	//{{AFX_MSG_MAP(CMonthsDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMonthsDlg message handlers

CCodes CMonthsDlg::GetCodes (ULONG lLineID) const
{
	CCodes c = CDateTimePropShtDlg::GetCodes (lLineID, m_vCodes);
	
	ASSERT (c.m_vCodes.GetSize () == 12);

	return c;
}

void CMonthsDlg::Update (ULONG lLineID, UINT nIndex, const CString & strValue)
{
	#if __CUSTOM__ == __SW0877__
	lLineID = 0;
	#endif

	lLineID = FoxjetDatabase::GetMasterLineID (lLineID);
	CCodes c = CDateTimePropShtDlg::GetCodes (lLineID, m_vCodes);

	ASSERT (c.m_vCodes.GetSize () == 12);
	c.m_vCodes [nIndex] = strValue;
	VERIFY (CDateTimePropShtDlg::SetCodes (m_db, lLineID, c, m_vCodes, ListGlobals::Keys::m_strMonths));
}

