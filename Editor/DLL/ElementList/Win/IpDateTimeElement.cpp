// DateTimeElement.cpp: implementation of the CDateTimeElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "IpElements.h"
#include "List.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"

#include "fj_printhead.h"
#include "fj_element.h"
#include "fj_text.h"
#include "fj_datetime.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_font.h"
#include "fj_label.h"
#include "fj_system.h"

using namespace FoxjetCommon;
using namespace FoxjetIpElements;
using namespace Color;
using namespace FoxjetCommon::ElementFields;

extern CArray <LPFJSYSTEM, LPFJSYSTEM> vIpSystems;

IMPLEMENT_DYNAMIC (CDateTimeElement, CIpBaseElement);

const FoxjetCommon::LIMITSTRUCT CDateTimeElement::m_lmtHourACount	= { 1,	24	};
const FoxjetCommon::LIMITSTRUCT CDateTimeElement::m_lmtLength		= { 0,	20	};

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CDateTimeElement::CDateTimeElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head)
:	CIpBaseElement (fj_ElementDatetimeNew(), pHead, head)
{
	LPFJDATETIME p = GetMember ();

	p->lFormat = DT_DATE_FIRST;
	VERIFY (p->pfeText = fj_ElementTextNew ());
}

CDateTimeElement::CDateTimeElement (const CDateTimeElement & rhs)
: 	CIpBaseElement (rhs)
{
}

CDateTimeElement & CDateTimeElement::operator = (const CDateTimeElement & rhs)
{
	CIpBaseElement::operator = (rhs);

	if (this != &rhs) {
	}

	return * this;
}

CDateTimeElement::~CDateTimeElement ()
{
}

LPFJTEXT CDateTimeElement::GetText ()
{
	LPFJDATETIME pCounter = GetMember ();
	ASSERT (pCounter->pfeText->pDesc);
	return (LPFJTEXT)pCounter->pfeText->pDesc;
}

CLPFJTEXT CDateTimeElement::GetText () const
{
	CLPFJDATETIME pCounter = GetMember ();
	ASSERT (pCounter->pfeText->pDesc);
	return (CLPFJTEXT)pCounter->pfeText->pDesc;
}

int CDateTimeElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	HDC hdc = ::GetDC (NULL);
	CDC & dc = * CDC::FromHandle (hdc);

	Invalidate ();
	Draw (dc, CHead (GetHead ()), true);
	::ReleaseDC (NULL, hdc);
	return GetClassID ();
}

CString CDateTimeElement::GetImageData () const
{
	CLPFJTEXT pText = GetText ();
	return pText->strText;
}

CString CDateTimeElement::GetDefaultData () const
{
	return GetImageData ();
}

CString CDateTimeElement::GetElementFontName () const
{
	LPFJDATETIME p = GetMember ();
	return p->strFontName;
}

void CDateTimeElement::CreateImage () 
{
	LPFJDATETIME p = GetMember ();
	LPFJTEXT pText = GetText ();
	const time_t t = time (NULL);

	// update the stored ip time
	if (struct tm * pTime = ::localtime (&t)) {
		if (LPFJSYSTEM pSystem = GetSystem ()) {
			memcpy (&(pSystem->tmPhotoCell), pTime, sizeof (struct tm));
		}
	}

	strcpy (p->strFontName, m_pFont->strName);    
	pText->pff = m_pFont;
	p->pfeText->pfm = GetElement ()->pfm;

	/*
	if (m_pLabel) {
		CString str;
		LPCTSTR lpsz [] =
		{ 
			_T (""),
			_T ("FJ_LABEL_EXP_DAYS"),
			_T ("FJ_LABEL_EXP_WEEKS"),
			_T ("FJ_LABEL_EXP_MONTHS"),
			_T ("FJ_LABEL_EXP_YEARS"),
		};

		str.Format (_T ("%d %s"), m_pLabel->lExpValue, lpsz [m_pLabel->lExpUnits]);
		TRACEF (str);
	}
	else { TRACEF ("m_pLabel == NULL"); }
	*/

	CIpBaseElement::CreateImage ();
}

LONG CDateTimeElement::GetBold () const
{
	return GetMember ()->lBoldValue;
}

bool CDateTimeElement::SetBold (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, CIpBaseElement::m_lmtBold)) {
		GetMember ()->lBoldValue = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

int CDateTimeElement::GetWidth () const
{
	return GetMember ()->lWidthValue;
}

bool CDateTimeElement::SetWidth (int nValue)
{
	if (FoxjetCommon::IsValid (nValue, CIpBaseElement::m_lmtWidth)) {
		GetMember ()->lWidthValue = nValue;
		Invalidate ();
		return true;
	}

	return false;
}

FJDTFORMAT CDateTimeElement::GetFormat () const
{
	return GetMember ()->lFormat;
}

bool CDateTimeElement::SetFormat (FJDTFORMAT fmt)
{
	GetMember ()->lFormat = fmt;
	Invalidate ();
	return true;
}

LONG CDateTimeElement::GetGap () const
{
	return GetMember ()->lGapValue;
}

bool CDateTimeElement::SetGap (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, CIpBaseElement::m_lmtGap)) {
		GetMember ()->lGapValue = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

FJDTTYPE CDateTimeElement::GetTimeType () const
{
	return GetMember ()->lTimeType;
}

bool CDateTimeElement::SetTimeType (FJDTTYPE type)
{
	GetMember ()->lTimeType = type;
	Invalidate ();
	return true;
}

CHAR CDateTimeElement::GetDelim () const
{
	return GetMember ()->cDelim;
}

bool CDateTimeElement::SetDelim (CHAR c)
{
	GetMember ()->cDelim = c; // TODO
	Invalidate ();
	return true;
}

BOOL CDateTimeElement::GetStringNormal () const
{
	return GetMember ()->bStringNormal;
}

bool CDateTimeElement::SetStringNormal (BOOL b)
{
	GetMember ()->bStringNormal = b; 
	Invalidate ();
	return true;
}

LONG CDateTimeElement::GetHourACount () const
{
	return GetMember ()->lHourACount;
}

bool CDateTimeElement::SetHourACount (LONG l)
{
	if (FoxjetCommon::IsValid (l, CDateTimeElement::m_lmtHourACount)) {
		GetMember ()->lHourACount = l; 
		Invalidate ();
		return true;
	}

	return false;
}

LONG CDateTimeElement::GetLength () const
{
	return GetMember ()->lLength;
}

bool CDateTimeElement::SetLength (LONG l)
{
	if (FoxjetCommon::IsValid (l, CDateTimeElement::m_lmtLength)) {
		GetMember ()->lLength = l; 
		Invalidate ();
		return true;
	}

	return false;
}

CHAR CDateTimeElement::GetAlign () const
{
	return GetMember ()->cAlign;
}

bool CDateTimeElement::SetAlign (CHAR c)
{
	if (c == 'R' || c == 'L') {
		GetMember ()->cAlign = c;
		Invalidate ();
		return true;
	}

	return false;
}

CHAR CDateTimeElement::GetFill () const
{
	return GetMember ()->cFill;
}

bool CDateTimeElement::SetFill (CHAR c)
{
	GetMember ()->cFill = c; // TODO
	Invalidate ();
	return true;
}

bool CDateTimeElement::SetFont (const FoxjetCommon::CPrinterFont & f)
{
	strncpy (GetMember ()->strFontName, w2a (f.m_strName), FJFONT_NAME_SIZE);
	return CIpBaseElement::SetFont (f);
}

static const ElementFields::FIELDSTRUCT lpszFields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType, 	_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszID,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszX,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszY,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszPosition,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszBold,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszWidth,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszIPGap,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszFlipV,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFlipH,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszInverse,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFormat,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszTimeType,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszLength,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszAlignChar,	_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFillChar,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszDelimChar,	_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszStringNormal,	_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszHourACount,	_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFontName,		_T ("0")),	
	ElementFields::FIELDSTRUCT (),
};

const FoxjetCommon::ElementFields::FIELDSTRUCT * CDateTimeElement::GetFieldBuffer () const
{
	return ::lpszFields;
}
