// HoursDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "HoursDlg.h"
#include "Extern.h"
#include "SystemDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace CodesDlg;

/////////////////////////////////////////////////////////////////////////////
// CHoursDlg property page

IMPLEMENT_DYNCREATE(CHoursDlg, CDaysDlg)

CHoursDlg::CHoursDlg(FoxjetDatabase::COdbcDatabase & db, const CCodesArray & vCodes) 
:	m_vCodes (vCodes),
	CDaysDlg(db, IDD_PROP_HOURS)
{
	//{{AFX_DATA_INIT(CHoursDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CHoursDlg::~CHoursDlg()
{
}

void CHoursDlg::DoDataExchange(CDataExchange* pDX)
{
	CDaysDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHoursDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHoursDlg, CDaysDlg)
	//{{AFX_MSG_MAP(CHoursDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHoursDlg message handlers


CCodes CHoursDlg::GetCodes (ULONG lLineID) const
{
	CCodes c = CDateTimePropShtDlg::GetCodes (lLineID, m_vCodes);
	
	ASSERT (c.m_vCodes.GetSize () == 24);

	return c;
}

void CHoursDlg::Update (ULONG lLineID, UINT nIndex, const CString & strValue)
{
	#if __CUSTOM__ == __SW0877__
	lLineID = 0;
	#endif

	lLineID = FoxjetDatabase::GetMasterLineID (lLineID);
	CCodes c = CDateTimePropShtDlg::GetCodes (lLineID, m_vCodes);
	
	ASSERT (c.m_vCodes.GetSize () == 24);
	c.m_vCodes [nIndex] = strValue;
	VERIFY (CDateTimePropShtDlg::SetCodes (m_db, lLineID, c, m_vCodes, ListGlobals::Keys::m_strHours));
}

