// SerialElement.cpp: implementation of the CSerialElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "list.h"
#include "SerialElement.h"
#include "PrinterFont.h"
#include "Debug.h"
#include "Utils.h"
#include "Extern.h"
#include "Resource.h"
#include "Parse.h"

using namespace FoxjetElements;
using namespace FoxjetElements::TextElement;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace FoxjetCommon::ElementFields;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNAMIC (CSerialElement, CTextElement);

CSerialElement::CSerialElement (const FoxjetDatabase::HEADSTRUCT & head)
:	m_pBuffer (NULL),
	m_nIndex (0),
	m_nLen (1),
	m_nTableIndex (-1),
	CTextElement (head)
{
}

CSerialElement::CSerialElement (const CSerialElement & rhs)
:	m_pBuffer (rhs.m_pBuffer),
	m_nIndex (rhs.m_nIndex),
	m_nLen (rhs.m_nLen),
	m_strDefault (rhs.m_strDefault),
	m_nTableIndex (rhs.m_nTableIndex),
	CTextElement (rhs)
{
}

CSerialElement & CSerialElement::operator = (const CSerialElement & rhs)
{
	CTextElement::operator = (rhs);

	if (this != &rhs) {
		m_pBuffer		= rhs.m_pBuffer;
		m_nIndex		= rhs.m_nIndex;
		m_nLen			= rhs.m_nLen;
		m_nTableIndex	= rhs.m_nTableIndex;
		m_strDefault	= rhs.m_strDefault;
	}

	return * this;
}

CSerialElement::~CSerialElement()
{
}

// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
static const ElementFields::FIELDSTRUCT fields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType,		_T ("")				),	// 0:	Prefix (required)
	ElementFields::FIELDSTRUCT (m_lpszID,				_T ("")				),	// 1:	ID (required)
	ElementFields::FIELDSTRUCT (m_lpszX,				_T ("")				),	// 2:	x (required)
	ElementFields::FIELDSTRUCT (m_lpszY,				_T ("")				),	// 3:	y (required)
	ElementFields::FIELDSTRUCT (m_lpszFlipH,			_T ("0")			),	// 4:	flip h
	ElementFields::FIELDSTRUCT (m_lpszFlipV,			_T ("0")			),	// 5:	flip v
	ElementFields::FIELDSTRUCT (m_lpszInverse,			_T ("0")			),	// 6:	inverse
	ElementFields::FIELDSTRUCT (m_lpszFontName,			_T ("Courier New")	),	// 7:	font name
	ElementFields::FIELDSTRUCT (m_lpszFontSize,			_T ("32")			),	// 8:	font size
	ElementFields::FIELDSTRUCT (m_lpszBold,				_T ("0")			),	// 9:	bold
	ElementFields::FIELDSTRUCT (m_lpszItalic,			_T ("0")			),	// 10:	italic
	ElementFields::FIELDSTRUCT (m_lpszIndex,			_T ("0")			),	// 11:	index
	ElementFields::FIELDSTRUCT (m_lpszLength,			_T ("1")			),	// 12:	length
	ElementFields::FIELDSTRUCT (m_lpszOrient,			_T ("0")			),	// 13:	orientation
	ElementFields::FIELDSTRUCT (m_lpszWidth,			_T ("0")			),	// 14:	font width
	ElementFields::FIELDSTRUCT (m_lpszDefault,			_T ("")				),	// 15:	default data
	ElementFields::FIELDSTRUCT (m_lpszParaAlign,		_T ("0")			),	// 16:	paragraph alignment
	ElementFields::FIELDSTRUCT (m_lpszParaGap,			_T ("0")			),	// 17:  paragraph gap
	ElementFields::FIELDSTRUCT (m_lpszResizable,		_T ("0")			),	// 18:	resizable
	ElementFields::FIELDSTRUCT (m_lpszParaWidth,		_T ("0")			),	// 19:	paragraph width
	ElementFields::FIELDSTRUCT (m_lpszAlign,			_T ("0")			),	// 20:  alignment
	ElementFields::FIELDSTRUCT (m_lpszTableIndex,		_T ("0")			),	// 21:  table index
	ElementFields::FIELDSTRUCT (m_lpszDither,			_T ("0")			),	// 22:
	ElementFields::FIELDSTRUCT (m_lpszColor,			_T ("0")			),	// 23:
	ElementFields::FIELDSTRUCT (),
};

CString CSerialElement::ToString (const FoxjetCommon::CVersion & ver) const
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated

	CString str;
	CPrinterFont f = GetFont ();
	CPoint pt = GetPosInternal (ALIGNMENT_ADJUST_ADD);

	str.Format (_T ("{Serial,%u,%u,%u,%d,%d,%d,%s,%u,%d,%d,%d,%d,%u,%d,%s,%d,%d,%d,%d,%d,%d,%d,%06X}"),
		GetID (),
		pt.x, pt.y,
		IsFlippedH (),
		IsFlippedV (),
		IsInverse (),
		f.m_strName, f.m_nSize, f.m_bBold, f.m_bItalic,
		GetIndex (),
		GetLength (),
		(int)GetOrientation (),
		GetWidth (),
		FormatString (m_strDefault),
		(int)GetParaAlign (),
		GetParaGap (),
		IsResizable (),
		GetParaWidth (),
		GetAlignment (),
		GetTableIndex (),
		GetDither (),
		GetColor ());

	return str;
}

bool CSerialElement::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
	CStringArray vstrToken;
	CPrinterFont f;

	Parse (str, ver, vstrToken);

	if (vstrToken [0].CompareNoCase (_T ("Serial")) != 0){
		TRACEF ("Wrong prefix \"" + vstrToken [0] + "\"");
		return false;
	}

	f.m_strName = vstrToken [7];
	f.m_nSize = (UINT)_tcstol (vstrToken [8], NULL, 10);
	f.m_bBold = _ttoi (vstrToken [9]) ? true : false;
	f.m_bItalic = _ttoi (vstrToken [10]) ? true : false;

	bool bResult = true;

	bResult &= SetID ((UINT)_tcstol (vstrToken [1], NULL, 10));
	bResult &= SetPos (CPoint (
		(UINT)_tcstol (vstrToken [2], NULL, 10), 
		(UINT)_tcstol (vstrToken [3], NULL, 10)), NULL);
	bResult &= SetFlippedH (_ttoi (vstrToken [4]) ? true : false);
	bResult &= SetFlippedV (_ttoi (vstrToken [5]) ? true : false);
	bResult &= SetInverse (_ttoi (vstrToken [6]) ? true : false);
	bResult &= SetFont (f);
	bResult &= SetIndex ((UINT)_tcstol (vstrToken [11], NULL, 10));
	bResult &= SetLength ((UINT)_tcstol (vstrToken [12], NULL, 10));
	bResult &= SetOrientation ((FONTORIENTATION)_tcstol (vstrToken [13], NULL, 10));
	bResult &= SetWidth (_ttoi (vstrToken [14]));
	bResult &= SetDefault (UnformatString (vstrToken [15]));

	bResult &= SetParaAlign ((ALIGNMENT)_ttoi (GetParam (vstrToken, 16)));
	bResult &= SetParaGap (_ttoi (GetParam (vstrToken, 17)));
	bResult &= SetResizable (_ttoi (GetParam (vstrToken, 18)) ? true : false);
	bResult &= SetParaWidth (_ttoi (GetParam (vstrToken, 19)));
	bResult &= SetAlignment ((ALIGNMENT)_ttoi (GetParam (vstrToken, 20)));
	
	bResult &= SetTableIndex ((ALIGNMENT)_ttoi (GetParam (vstrToken, 21)));
	bResult &= SetDither (_ttoi (GetParam (vstrToken, 22)));
	bResult &= SetColor (_tcstoul (vstrToken [23], NULL, 16));

	return bResult;
}

int CSerialElement::GetTableIndex () const
{
	return m_nTableIndex;
}

bool CSerialElement::SetTableIndex (int nIndex)
{
	m_nTableIndex = nIndex;
	return true;
}

int CSerialElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	CString str ('W', GetLength ());

	if (m_strDefault.GetLength ()) {
		int nLen = min (m_strDefault.GetLength (), GetLength ());

		str = m_strDefault.Left (nLen);

		int nDiff = GetLength () - str.GetLength ();

		if (nDiff > 0)
			str += CString ('W', nDiff);
	}

	bCanThrowException &= (type == IMAGING);
	//TRACEF (str);

	if (!m_pBuffer) {
		if (bCanThrowException)
			throw new CElementException (EXCEPTION_SERIAL, LoadString (IDS_SERIAL_NOBUFFER));
	}
	else {
  		int nFirst = GetIndex (), nCount = GetLength ();
		const TCHAR * p = m_pBuffer;

		for (int i = 0; i < nFirst; i++, p++) { }

		for (int i = 0; i < nCount; i++, p++) {
			TCHAR c = (TCHAR)* p;

			if (c == NULL) { // sw0835
				str.SetAt (i, NULL);
				continue; 
			} 

			//if (!_istascii (c))
			//	c = ' ';

			if (c)
				str.SetAt (i, (TCHAR)c);
		}

		{
			for (int i = str.GetLength () - 1; i >= 0; i--) // sw0835
				if (str [i] == NULL) 
					str.Delete (i);
		}
	}

	CTextElement::SetDefaultData (str);
	//CTextElement::Build (type, bCanThrowException);
	m_strImageData = str;


	return GetClassID ();
}

const TCHAR * CSerialElement::GetBuffer () const
{
	return m_pBuffer;
}

bool CSerialElement::SetBuffer (const TCHAR * pBuffer)
{
	SetRedraw ();
	m_pBuffer = pBuffer;
	return true;
}

int CSerialElement::GetIndex () const
{
	return m_nIndex;
}

bool CSerialElement::SetIndex (int nIndex)
{
	if (nIndex >= 0 && nIndex <= (MAXBUFFLEN - 1)) {
		m_nIndex = nIndex;
		return true;
	}

	return false;
}

int CSerialElement::GetLength () const
{
	return m_nLen;
}

bool CSerialElement::SetLength (int nLength)
{
	int nMaxLen = (MAXBUFFLEN - GetIndex ());

	if (nLength > 0 && nLength <= nMaxLen) {
		m_nLen = nLength;
		return true;
	}

	return false;
}

const FoxjetCommon::ElementFields::FIELDSTRUCT * CSerialElement::GetFieldBuffer () const
{
	return ::fields;
}

bool CSerialElement::SetDefaultData(const CString &strData)
{
	SetLength (strData.GetLength ());
	return CTextElement::SetDefaultData (strData);
}

CString CSerialElement::GetDefault () const
{
	return m_strDefault;
}

bool CSerialElement::SetDefault (const CString & str)
{
	m_strDefault = str;
	SetRedraw (true);

	return true;
}