#ifndef __COUNTRYCODES_H__
#define __COUNTRYCODES_H__

namespace Ritz 
{
	namespace CountryCodes
	{
		typedef struct 
		{
			CString m_strCountry;
			CString m_strA2;
			CString m_strA3;
			CString m_strNumber;
		} COUNTRYCODERECSTRUCT;

		extern LPCTSTR m_lpszTable;
		extern LPCTSTR m_lpszCountry;
		extern LPCTSTR m_lpszA2;
		extern LPCTSTR m_lpszA3;
		extern LPCTSTR m_lpszNumber;
	}; //namespace ClassOfService

}; //namespace Ritz 

#endif //__COUNTRYCODES_H__
