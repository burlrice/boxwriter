#if !defined(__SHAPEDLG_H__)
#define __SHAPEDLG_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BitmapDlg.h : header file
//

#include "ElementDlg.h"
#include "resource.h"
#include "ShapeElement.h"

/////////////////////////////////////////////////////////////////////////////
// CShapeDlg dialog

class CShapeDlg : public CElementDlg
{
// Construction
public:
	CShapeDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CShapeDlg)
	//}}AFX_DATA

	CSize m_size;
	CSize m_thickness;
	int m_nType;

	void Get (const FoxjetElements::ShapeElement::CShapeElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);
	void Set (FoxjetElements::ShapeElement::CShapeElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShapeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnCancel();
	virtual bool GetValues ();
	
	// Generated message map functions
	//{{AFX_MSG(CShapeDlg)
	afx_msg void OnBrowse();
	virtual BOOL OnInitDialog();
	afx_msg void OnEdit();
	//}}AFX_MSG

	afx_msg void OnClickType ();

	CBitmap m_bmpHorz;
	CBitmap m_bmpVert;
	CBitmap m_bmpRect;

	virtual int BuildElement ();
	const FoxjetElements::ShapeElement::CShapeElement & GetElement () const;
	FoxjetElements::ShapeElement::CShapeElement & GetElement ();
	
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(__SHAPEDLG_H__)
