// CountElement.cpp: implementation of the CCountElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "IpElements.h"
#include "List.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"

#include "fj_printhead.h"
#include "fj_element.h"
#include "fj_text.h"
#include "fj_counter.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_font.h"
#include "fj_message.h"
#include "fj_system.h"

using namespace FoxjetCommon;
using namespace FoxjetIpElements;
using namespace Color;
using namespace FoxjetCommon::ElementFields;


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNAMIC (CCountElement, CIpBaseElement);

const FoxjetCommon::LIMITSTRUCT CCountElement::m_lmtLimit		= { 0,	FJCOUNTER_MAX_VALUE						}; 
const FoxjetCommon::LIMITSTRUCT CCountElement::m_lmtRepeat		= { 0,	FJCOUNTER_MAX_VALUE						}; 
const FoxjetCommon::LIMITSTRUCT CCountElement::m_lmtStart		= { 0,	FJCOUNTER_MAX_VALUE						};
const FoxjetCommon::LIMITSTRUCT CCountElement::m_lmtLength		= { 0,	GetNumberOfDigits (FJCOUNTER_MAX_VALUE)	}; 
const FoxjetCommon::LIMITSTRUCT CCountElement::m_lmtChange		= { 1,	999										}; 
const FoxjetCommon::LIMITSTRUCT CCountElement::m_lmtCurValue	= { 0,	FJCOUNTER_MAX_VALUE						};
const FoxjetCommon::LIMITSTRUCT CCountElement::m_lmtCurRepeat	= { 0,	FJCOUNTER_MAX_VALUE						};
																											
CCountElement::CCountElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head)
:	CIpBaseElement (fj_ElementCounterNew (), pHead, head)
{
	LPFJCOUNTER pCount = GetMember ();
	VERIFY (pCount->pfeText = fj_ElementTextNew ());
}

CCountElement::CCountElement (const CCountElement & rhs)
:	CIpBaseElement (rhs)
{
}

CCountElement & CCountElement::operator = (const CCountElement & rhs)
{
	CIpBaseElement::operator = (rhs);

	if (this != &rhs) {
	}

	return * this;
}

CCountElement::~CCountElement ()
{
}

LPFJTEXT CCountElement::GetText ()
{
	LPFJCOUNTER p = GetMember ();
	ASSERT (p->pfeText->pDesc);
	return (LPFJTEXT)p->pfeText->pDesc;
}

CLPFJTEXT CCountElement::GetText () const
{
	CLPFJCOUNTER p = GetMember ();
	ASSERT (p->pfeText->pDesc);
	return (CLPFJTEXT)p->pfeText->pDesc;
}

int CCountElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	HDC hdc = ::GetDC (NULL);
	CDC & dc = * CDC::FromHandle (hdc);

	Invalidate ();
	Draw (dc, CHead (GetHead ()), true);
	::ReleaseDC (NULL, hdc);
	return GetClassID ();
}

CString CCountElement::GetImageData () const
{
	CLPFJTEXT pText = GetText ();
	return pText->strText;
}

CString CCountElement::GetDefaultData () const
{
	return GetImageData ();
}

CString CCountElement::GetElementFontName () const
{
	LPFJCOUNTER p = GetMember ();
	return p->strFontName;
}

void CCountElement::CreateImage () 
{
	LPFJCOUNTER pCount = GetMember ();
	LPFJTEXT pText = GetText ();

	strcpy (pCount->strFontName, m_pFont->strName);    
	pText->pff = m_pFont;
	pCount->pfeText->pfm = GetElement ()->pfm;

	CIpBaseElement::CreateImage ();
}

LONG CCountElement::GetBold () const
{
	return GetMember ()->lBoldValue;
}

bool CCountElement::SetBold (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, CCountElement::m_lmtBold)) {
		GetMember ()->lBoldValue = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

int CCountElement::GetWidth () const
{
	return GetMember ()->lWidthValue;
}

bool CCountElement::SetWidth (int nValue)
{
	if (FoxjetCommon::IsValid (nValue, CCountElement::m_lmtWidth)) {
		GetMember ()->lWidthValue = nValue;
		Invalidate ();
		return true;
	}

	return false;
}

LONG CCountElement::GetGap () const
{
	return GetMember ()->lGapValue;
}

bool CCountElement::SetGap (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, CCountElement::m_lmtGap)) {
		GetMember ()->lGapValue = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

LONG CCountElement::GetChange () const
{
	return GetMember ()->lChange;
}

bool CCountElement::SetChange (LONG l)
{
	if (FoxjetCommon::IsValid (l, CCountElement::m_lmtChange)) {
		GetMember ()->lChange = l; 
		Invalidate ();
		return true;
	}

	return false;
}

LONG CCountElement::GetRepeat () const
{
	return GetMember ()->lRepeat;
}

bool CCountElement::SetRepeat (LONG l)
{
	if (FoxjetCommon::IsValid (l, CCountElement::m_lmtRepeat)) {
		GetMember ()->lRepeat = l; 
		Invalidate ();
		return true;
	}

	return false;
}

LONG CCountElement::GetStart () const
{
	return GetMember ()->lStart;
}

bool CCountElement::SetStart (LONG l)
{
	if (FoxjetCommon::IsValid (l, CCountElement::m_lmtStart)) {
		GetMember ()->lStart = l; 
		Invalidate ();
		return true;
	}

	return false;
}

LONG CCountElement::GetLimit () const
{
	return GetMember ()->lLimit;
}

bool CCountElement::SetLimit (LONG l)
{
	if (FoxjetCommon::IsValid (l, CCountElement::m_lmtLimit)) {
		GetMember ()->lLimit = l; 
		Invalidate ();
		return true;
	}

	return false;
}

LONG CCountElement::GetCurValue () const
{
	return GetMember ()->lCurValue;
}

bool CCountElement::SetCurValue (LONG l)
{
	if (FoxjetCommon::IsValid (l, CCountElement::m_lmtCurValue)) {
		GetMember ()->lCurValue = l; 
		Invalidate ();
		return true;
	}

	return false;
}

LONG CCountElement::GetCurRepeat () const
{
	return GetMember ()->lCurRepeat;
}

bool CCountElement::SetCurRepeat (LONG l)
{
	if (FoxjetCommon::IsValid (l, CCountElement::m_lmtCurRepeat)) {
		GetMember ()->lCurRepeat = l; 
		Invalidate ();
		return true;
	}

	return false;
}

LONG CCountElement::GetLength () const
{
	return GetMember ()->lLength;
}

bool CCountElement::SetLength (LONG l)
{
	if (FoxjetCommon::IsValid (l, CCountElement::m_lmtLength)) {
		GetMember ()->lLength = l; 
		Invalidate ();
		return true;
	}

	return false;
}

CHAR CCountElement::GetAlign () const
{
	return GetMember ()->cAlign;
}

bool CCountElement::SetAlign (CHAR c)
{
	if (c == 'R' || c == 'L') {
		GetMember ()->cAlign = c; 
		Invalidate ();
		return true;
	}

	return false;
}

CHAR CCountElement::GetFill () const
{
	return GetMember ()->cFill;
}

bool CCountElement::SetFill (CHAR c)
{
	GetMember ()->cFill = c; // TODO
	Invalidate ();
	return true;
}

bool CCountElement::SetFont (const FoxjetCommon::CPrinterFont & f)
{
	strncpy (GetMember ()->strFontName, w2a (f.m_strName), FJFONT_NAME_SIZE);
	return CIpBaseElement::SetFont (f);
}

extern "C" FJDLLExport VOID fj_CounterUpdate( CLPFJCOUNTER pfc );

void CCountElement::Increment ()
{
	CIpBaseElement::Increment ();

	if (LPFJELEMENT pElement = GetElement ()) {
		LPFJCOUNTER pCount = GetMember ();

		fj_CounterUpdate (pCount);
	}
}

static const ElementFields::FIELDSTRUCT lpszFields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType, 	_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszID,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszX,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszY,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszPosition,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszBold,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszWidth,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszIPGap,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszFlipV,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFlipH,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszInverse,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszChange,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszRepeat,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszStart,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszLimit,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszLength,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszAlignChar,	_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFillChar,	_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFontName,		_T ("0")),	
	ElementFields::FIELDSTRUCT (),
};

const FoxjetCommon::ElementFields::FIELDSTRUCT * CCountElement::GetFieldBuffer () const
{
	return ::lpszFields;
}
