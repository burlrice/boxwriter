// QuarterHourCodeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "QuarterHourCodeDlg.h"
#include "Extern.h"
#include "Database.h"
#include "List.h"
#include "Debug.h"
#include "TextElement.h"
#include "SystemDlg.h"

using namespace ItiLibrary;
using namespace ListGlobals;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace FoxjetElements::TextElement;

// std // #if __CUSTOM__ == __SW0840__

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////

using namespace CodesDlg;

CQuarterHourCodeDlg::CItem::CItem (const CString & str, int nIndex)
:	m_str (str), 
	m_nIndex (nIndex)
{
}

CQuarterHourCodeDlg::CItem::~CItem ()
{
}

CString CQuarterHourCodeDlg::CItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0: 
		{
			CTime tmBase	= CTime (2006, 1, 1, 0, 0, 0) ;
			CTime tm1		= tmBase + CTimeSpan (0, 0, ((m_nIndex - 1)	* 15),		0);
			CTime tm2		= tmBase + CTimeSpan (0, 0, ((m_nIndex)		* 15) - 1,	0);
			LPCTSTR lpsz	= _T ("%I:%M %p");

			str = tm1.Format (lpsz) + CString (_T (" - ")) + tm2.Format (lpsz);

			#ifdef _DEBUG
			CString strTmp;
			strTmp.Format (_T (" [%d]"), m_nIndex);
			str += strTmp;
			#endif

		}
		break;
	case 1:
		str = m_str;
	}

	return str;
}

int CQuarterHourCodeDlg::CItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	CItem & cmp = (CItem &)rhs;

	switch (nColumn) {
	case 0:		return m_nIndex - cmp.m_nIndex;
	case 1:		return m_str.Compare (cmp.m_str);
	}

	return 0;
}

/////////////////////////////////////////////////////////////////////////////


CQuarterHourCodeDlg::CEditDlg::CEditDlg (CWnd * pParent)
:	CEditValueDlg (pParent)
{
}

void CQuarterHourCodeDlg::CEditDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);

	CString str = CItem ("", m_nIndex).GetDispText (0);
	DDX_Text(pDX, TXT_INDEX, str);

	DDX_Text(pDX, TXT_VALUE, m_strValue);
	DDV_MinMaxChars (pDX, m_strValue, TXT_VALUE, CTextElement::m_lmtString);
}


/////////////////////////////////////////////////////////////////////////////
// CQuarterHourCodeDlg property page

IMPLEMENT_DYNCREATE(CQuarterHourCodeDlg, CPropertyPage)

CQuarterHourCodeDlg::CQuarterHourCodeDlg () 
:	m_db (ListGlobals::GetDB ()),
	CPropertyPage (IDD_PROP_QUARTERHOURS)
{ 
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CQuarterHourCodeDlg::CQuarterHourCodeDlg (FoxjetDatabase::COdbcDatabase & db, const CCodesArray & vCodes) 
:	m_vCodes (vCodes),
	m_db (db),
	CPropertyPage(IDD_PROP_QUARTERHOURS)
{
	//{{AFX_DATA_INIT(CQuarterHourCodeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CQuarterHourCodeDlg::~CQuarterHourCodeDlg()
{
}

void CQuarterHourCodeDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CQuarterHourCodeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CQuarterHourCodeDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CQuarterHourCodeDlg)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_CBN_SELCHANGE(CB_LINE, OnSelchangeLine)
	ON_NOTIFY(NM_DBLCLK, LV_TYPE, OnDblclkType)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CQuarterHourCodeDlg message handlers

void CQuarterHourCodeDlg::OnEdit() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		CEditDlg dlg (this);
		CItem * p = (CItem *)m_lv.GetCtrlData (sel [0]);
		ULONG lLineID = GetCurLine ();

		dlg.m_nIndex = p->m_nIndex;
		dlg.m_strValue = p->m_str;

		if (dlg.DoModal () == IDOK) {
			int nIndex = p->m_nIndex - 1;

			p->m_str = dlg.m_strValue;
			m_lv.UpdateCtrlData (p);

			Update (lLineID, nIndex, dlg.m_strValue);
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CQuarterHourCodeDlg::OnSelchangeLine() 
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	ULONG lLineID = GetCurLine ();

	ASSERT (pLine);
	
	CCodes c = GetCodes (lLineID);

	m_lv.DeleteCtrlData ();

	for (int i = 0; i < c.m_vCodes.GetSize (); i++) {
		int nIndex = i + 1;
		CString str = c.m_vCodes [i];

		m_lv.InsertCtrlData (new CItem (str, nIndex));
	}
}

void CQuarterHourCodeDlg::OnDblclkType(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();
	*pResult = 0;
}


BOOL CQuarterHourCodeDlg::OnInitDialog() 
{
	using namespace ItiLibrary::ListCtrlImp;

	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	ULONG lLineID = GetCurLine ();

	ASSERT (pLine);

	CPropertyPage::OnInitDialog();


	#if __CUSTOM__ == __SW0877__
	pLine->EnableWindow (FALSE);
	lLineID = 0;

		#ifndef _DEBUG
		pLine->ShowWindow (SW_HIDE);
		#endif
	#endif

	CArray <CColumn, CColumn> vCols;

	vCols.Add (CColumn (LoadString (IDS_INDEX), 200));
	vCols.Add (CColumn (LoadString (IDS_VALUE), 80));

	m_lv.Create (LV_TYPE, vCols, this, defElements.m_strRegSection);	

	GetLineRecords (vLines);

	for (int i = 0; i < vLines.GetSize (); i++) {
		LINESTRUCT line = vLines [i];
		int nIndex = pLine->AddString (line.m_strName);

		pLine->SetItemData (nIndex, line.m_lID);

		if (lLineID == line.m_lID) 
			pLine->SetCurSel (nIndex);
	}

	if (pLine->GetCurSel () == CB_ERR) 
		pLine->SetCurSel (0);

	OnSelchangeLine ();

	return TRUE;	
}

ULONG CQuarterHourCodeDlg::GetCurLine() const
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	ULONG lLineID = -1;

	ASSERT (pLine);

	int nSel = pLine->GetCurSel ();

	if (nSel != CB_ERR)
		lLineID = pLine->GetItemData (nSel);

	return lLineID;
}

CCodes CQuarterHourCodeDlg::GetCodes (ULONG lLineID) const
{
	CCodes c = CDateTimePropShtDlg::GetCodes (lLineID, m_vCodes);
	
	ASSERT (c.m_vCodes.GetSize () == (24 * 4));

	return c;
}

void CQuarterHourCodeDlg::Update (ULONG lLineID, UINT nIndex, const CString & strValue)
{
	lLineID = FoxjetDatabase::GetMasterLineID (lLineID);

	#if __CUSTOM__ == __SW0877__
	lLineID = 0;
	#endif

	CCodes c = CDateTimePropShtDlg::GetCodes (lLineID, m_vCodes);
	
	ASSERT (c.m_vCodes.GetSize () == (24 * 4));
	c.m_vCodes [nIndex] = strValue;
	VERIFY (CDateTimePropShtDlg::SetCodes (m_db, lLineID, c, m_vCodes, ListGlobals::Keys::m_strQuarterHours));
}

