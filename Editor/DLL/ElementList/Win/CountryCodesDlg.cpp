// CountryCodesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CountryCodesDlg.h"
#include "CountryCodes.h"
#include "OdbcRecordset.h"
#include "ItiLibrary.h"
#include "Debug.h"
#include "Database.h"
#include "Extern.h"

using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace Ritz::CountryCodes;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CCountryCodesDlg::CCode::CCode ()
{
}

CCountryCodesDlg::CCode::CCode (const Ritz::CountryCodes::COUNTRYCODERECSTRUCT & rhs)
{
	m_strCountry	= rhs.m_strCountry;
	m_strA2			= rhs.m_strA2;
	m_strA3			= rhs.m_strA3;
	m_strNumber		= rhs.m_strNumber;
}

CCountryCodesDlg::CCode::CCode (const CCode & rhs)
{
	m_strCountry	= rhs.m_strCountry;
	m_strA2			= rhs.m_strA2;
	m_strA3			= rhs.m_strA3;
	m_strNumber		= rhs.m_strNumber;
}

CCountryCodesDlg::CCode & CCountryCodesDlg::CCode::operator = (const CCountryCodesDlg::CCode & rhs)
{
	if (this != &rhs) {
		m_strCountry	= rhs.m_strCountry;
		m_strA2			= rhs.m_strA2;
		m_strA3			= rhs.m_strA3;
		m_strNumber		= rhs.m_strNumber;
	}

	return * this;
}

CCountryCodesDlg::CCode::~CCode ()
{
}

CString CCountryCodesDlg::CCode::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0: 
		str = m_strCountry;
		break;
	case 1: 
		str = m_strA2;
		break;
	case 2: 
		str = m_strA3;
		break;
	case 3: 
		str = m_strNumber;
		break;
	}

	return str;
}


/////////////////////////////////////////////////////////////////////////////
// CCountryCodesDlg dialog


CCountryCodesDlg::CCountryCodesDlg(FoxjetDatabase::COdbcDatabase * pDatabase, CWnd* pParent /*=NULL*/)
:	m_pDatabase (pDatabase),
	FoxjetCommon::CEliteDlg(CCountryCodesDlg::IDD, pParent)
{
	ASSERT (pDatabase);
	//{{AFX_DATA_INIT(CCountryCodesDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CCountryCodesDlg::~CCountryCodesDlg()
{
}

void CCountryCodesDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCountryCodesDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCountryCodesDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CCountryCodesDlg)
	//}}AFX_MSG_MAP
	ON_NOTIFY(NM_DBLCLK, LV_LIB, OnDblclkListCtrl)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCountryCodesDlg message handlers

void CCountryCodesDlg::OnDblclkListCtrl(NMHDR* pNMHDR, LRESULT* pResult)
{
	OnOK ();
	* pResult = 0;
}


void CCountryCodesDlg::OnOK()
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		m_vSelected.RemoveAll ();

		for (int i = 0; i < sel.GetSize (); i++) {
			CCode * p = (CCode *)m_lv.GetCtrlData (sel [i]);
			m_vSelected.Add (p->m_strNumber);
		}

		FoxjetCommon::CEliteDlg::OnOK ();
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

BOOL CCountryCodesDlg::OnInitDialog() 
{
	using namespace ListCtrlImp;

	CLongArray vSel;
	CArray <CColumn, CColumn> vCols;

	vCols.Add (CColumn (LoadString (IDS_COUNTRY), 140));
	vCols.Add (CColumn (LoadString (IDS_A2), 70));
	vCols.Add (CColumn (LoadString (IDS_A3), 70));
	vCols.Add (CColumn (LoadString (IDS_NUMBER), 70));

	m_lv.Create (LV_COUNTRYCODES, vCols, this, defElements.m_strRegSection);

	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();

	try {
		COdbcRecordset rst (m_pDatabase);
		CString strSQL;
		int nIndex = 0;
		
		strSQL.Format (_T ("SELECT * FROM [%s] ORDER BY %s;"),
			m_lpszTable, m_lpszCountry);
		
		rst.Open (strSQL);

		while (!rst.IsEOF ()) {
			CCode * p = new CCode;

			p->m_strCountry	= rst.GetFieldValue (m_lpszCountry);
			p->m_strA2		= rst.GetFieldValue (m_lpszA2);
			p->m_strA3		= rst.GetFieldValue (m_lpszA3);
			p->m_strNumber	= rst.GetFieldValue (m_lpszNumber);
			m_lv.InsertCtrlData (p);

			for (int i = 0; i < m_vSelected.GetSize (); i++) 
				if (p->m_strNumber == m_vSelected [i])	
					vSel.Add (nIndex);

			nIndex++;
			rst.MoveNext ();
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	for (int i = 0; i < vSel.GetSize (); i++) 
		m_lv->SetItemState (vSel [i], (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);	

	return bResult;
}

bool CCountryCodesDlg::GetData (Ritz::CountryCodes::COUNTRYCODERECSTRUCT & ccs, 
								const CString & strNumber, 
								FoxjetDatabase::COdbcDatabase * pDatabase)
{
	try {
		COdbcRecordset rst (pDatabase);
		CString strSQL;
		
		strSQL.Format (_T ("SELECT * FROM [%s] WHERE %s='%s';"), 
			m_lpszTable, m_lpszNumber, strNumber);
		TRACEF (strSQL);

		rst.Open (CRecordset::snapshot, strSQL);

		if (!rst.IsEOF ()) {
			ccs.m_strCountry	= rst.GetFieldValue (m_lpszCountry);
			ccs.m_strA2			= rst.GetFieldValue (m_lpszA2);
			ccs.m_strA3			= rst.GetFieldValue (m_lpszA3);
			ccs.m_strNumber		= rst.GetFieldValue (m_lpszNumber);

			return true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return false;
}

bool CCountryCodesDlg::UpdateData (Ritz::CountryCodes::COUNTRYCODERECSTRUCT & ccs, 
								   FoxjetDatabase::COdbcDatabase * pDatabase)
{
	try {
		COdbcRecordset rst (pDatabase);
		CString strWhere;
		CStringArray name, value;

		name.Add (m_lpszCountry);	value.Add (ccs.m_strCountry);
		name.Add (m_lpszA2);		value.Add (ccs.m_strA2);
		name.Add (m_lpszA3);		value.Add (ccs.m_strA3);
		name.Add (m_lpszNumber);	value.Add (ccs.m_strNumber);

		CString strSQL = FoxjetDatabase::FormatSQL (rst, m_lpszTable, name, value, _T (""), 0);

		strWhere.Format (_T (" WHERE %s='%s';"), m_lpszTable, m_lpszNumber, ccs.m_strNumber);
		strSQL += strWhere;
		TRACEF (strSQL);

		rst.GetOdbcDatabase ().ExecuteSQL (strSQL);
		/* TODO: rem
		CString strSQL;
		
		strSQL.Format (_T ("SELECT .* FROM %s WHERE %s='%s';"), 
			m_lpszTable, m_lpszNumber, ccs.m_strNumber);

		rst.Open (CRecordset::dynaset, strSQL);

		if (!rst.IsEOF ()) {
			rst.Edit ();

			rst.SetFieldValue (m_lpszCountry,	(CString)ccs.m_strCountry);
			rst.SetFieldValue (m_lpszA2,		(CString)ccs.m_strA2);
			rst.SetFieldValue (m_lpszA3,		(CString)ccs.m_strA3);
			rst.SetFieldValue (m_lpszNumber,	(CString)ccs.m_strNumber);
			
			rst.Update ();
			rst.Close ();

			return true;
		}
		*/
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return false;
}
