// BarcodeCaptionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BarcodeCaptionDlg.h"
#include "Alias.h"
#include "Color.h"
#include "List.h"
#include "ItiLibrary.h"
#include "Debug.h"
#include "Registry.h"
#include "ElementDefaults.h"
#include "Extern.h"
#include "ListCtrlImp.h"

using namespace Color;
using namespace FoxjetCommon;
using namespace ItiLibrary;
using namespace FoxjetElements;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define SS_PREVIEW	0

/////////////////////////////////////////////////////////////////////////////
// CBarcodeCaptionDlg dialog


CBarcodeCaptionDlg::CBarcodeCaptionDlg(const FoxjetDatabase::HEADSTRUCT & head, CWnd * pParent /*=NULL*/)
:	m_head (head),
	m_barcode (head),
	m_bPreview (TRUE),
	FoxjetCommon::CEliteDlg(CBarcodeCaptionDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBarcodeCaptionDlg)
	//}}AFX_DATA_INIT
}


void CBarcodeCaptionDlg::DoDataExchange(CDataExchange* pDX)
{
	CTextInfo caption = m_barcode.GetCaption ();
	CButton * pAuto = (CButton *)GetDlgItem (CHK_AUTOUPDATE);

	ASSERT (pAuto);

	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);

	DDX_Text(pDX, TXT_TEXT, caption.m_strText);
	DDX_Check (pDX, CHK_PREVIEW, m_bPreview);

	if (!pDX->m_bSaveAndValidate) {
		pAuto->SetCheck (caption.m_bAuto ? 1 : 0);
	}
	else {
		caption.m_bAuto = pAuto->GetCheck () == 1;
		m_barcode.SetCaption (caption);
	}
}


BEGIN_MESSAGE_MAP(CBarcodeCaptionDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CBarcodeCaptionDlg)
	ON_BN_CLICKED(CHK_PREVIEW, OnPreview)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(TXT_TEXT, OnParamsChanged)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBarcodeCaptionDlg message handlers

BOOL CBarcodeCaptionDlg::OnInitDialog() 
{
	CRect rc;
	CButton * pPreview = (CButton *)GetDlgItem (CHK_PREVIEW);

	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();

	ASSERT (pPreview);

	pPreview->SetCheck (IsPreviewEnabled () ? 1 : 0);
	
	if (CWnd * pPreview = GetDlgItem (IDC_PREVIEW)) {
		CRect rc;
		pPreview->GetWindowRect (&rc);

		ScreenToClient (&rc);

		VERIFY (m_wndPreview.Create (this, rc, &m_barcode, &m_head));
	}

	OnPreview ();

	return bResult;
}

void CBarcodeCaptionDlg::OnParamsChanged ()
{
	CTextInfo c;
	CButton * pAuto = (CButton *)GetDlgItem (CHK_AUTOUPDATE);

	ASSERT (pAuto);

	GetDlgItemText (TXT_TEXT, c.m_strText);
	c.m_bAuto = false;

	m_barcode.SetCaption (c);

	if (IsPreviewEnabled ()) {
		m_barcode.Build ();
		m_wndPreview.Invalidate ();
		m_wndPreview.RedrawWindow ();
	}
}

bool CBarcodeCaptionDlg::IsPreviewEnabled() const
{
	if (CButton * p = (CButton *)GetDlgItem (CHK_PREVIEW))
		return p->GetCheck () == 1;

	return false;
}

void CBarcodeCaptionDlg::OnPreview() 
{
	bool bEnable = IsPreviewEnabled ();

	m_wndPreview.EnableWindow (bEnable);
	
	if (!bEnable) {
		m_wndPreview.Invalidate ();
		m_wndPreview.RedrawWindow ();
	}
	else
		OnParamsChanged ();
}

