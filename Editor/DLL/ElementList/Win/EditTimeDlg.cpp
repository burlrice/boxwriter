// EditTimeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "EditTimeDlg.h"
#include "Debug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditTimeDlg dialog


CEditTimeDlg::CEditTimeDlg(CWnd* pParent /*=NULL*/)
:	m_tm (2004, 1, 1, 0, 0, 0),
	m_nDirection (0),
	FoxjetCommon::CEliteDlg(IDD_EDITTIME, pParent)
{
	//{{AFX_DATA_INIT(CEditTimeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CEditTimeDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);

	DDX_DateTimeCtrl (pDX, DT_TIME, m_tm);
	DDX_Radio (pDX, RDO_FORWARD, m_nDirection);

	//{{AFX_DATA_MAP(CEditTimeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEditTimeDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CEditTimeDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditTimeDlg message handlers

BOOL CEditTimeDlg::OnInitDialog()
{
	FoxjetCommon::CEliteDlg::OnInitDialog ();

	if (CDateTimeCtrl * p = (CDateTimeCtrl *)GetDlgItem (DT_TIME)) 
		p->SetFormat (_T ("h:mm tt"));

	return TRUE;
}
