#if !defined(AFX_EXPORTBARCODEPARAMSDLG_H__14BB1864_FCE1_4923_9A11_C2C57DBAE539__INCLUDED_)
#define AFX_EXPORTBARCODEPARAMSDLG_H__14BB1864_FCE1_4923_9A11_C2C57DBAE539__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExportBarcodeParamsDlg.h : header file
//

#include "Resource.h"
#include "ListCtrlImp.h"
#include "Database.h"
#include "Utils.h"


/////////////////////////////////////////////////////////////////////////////
// CExportBarcodeParamsDlg dialog

class CExportBarcodeParamsDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CExportBarcodeParamsDlg(CWnd* pParent = NULL);   // standard constructor

	class CParamItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CParamItem ();

		virtual CString GetDispText (int nColumn) const;

		FoxjetDatabase::LINESTRUCT m_line;
		CString m_strName;
		bool m_bSet;
		int m_nSymbology;
	};

// Dialog Data
	//{{AFX_DATA(CExportBarcodeParamsDlg)
	enum { IDD = IDD_GLOBALBARCODE_EXPORT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExportBarcodeParamsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();
	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

	// Generated message map functions
	//{{AFX_MSG(CExportBarcodeParamsDlg)
	afx_msg void OnBrowse();
	virtual BOOL OnInitDialog();
	afx_msg void OnItemchangedParams(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXPORTBARCODEPARAMSDLG_H__14BB1864_FCE1_4923_9A11_C2C57DBAE539__INCLUDED_)
