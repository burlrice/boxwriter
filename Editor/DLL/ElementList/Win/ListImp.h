#ifndef __LISTIMP_H__
#define __LISTIMP_H__

#include "List.h"

bool ParseProductArea (FoxjetCommon::CElementList & vElements, const CStringArray & v, const FoxjetCommon::CVersion & ver);
bool ParseFontDef (FoxjetCommon::CElementList & vElements, const CStringArray & v, const FoxjetCommon::CVersion & ver);
bool ParsePacket (FoxjetCommon::CElementList & vElements, const CString & str, const FoxjetCommon::CVersion & ver, ULONG lLocalParamUID);
bool ParseElementDef (FoxjetCommon::CElementList & vElements, const CString & str, const FoxjetCommon::CVersion & ver);
bool ParseFontDef (FoxjetCommon::CElementList & vElements, const CStringArray & v, const FoxjetCommon::CVersion & ver);
bool ParseVersion (FoxjetCommon::CElementList & vElements, const CString & str);
bool ParseTimestamp (FoxjetCommon::CElementList & vElements, const CString & str);
//int GetElementType (const CString & strPrefix);
int CALLBACK EnumFontFamExProc(ENUMLOGFONTEX *lpelfe, NEWTEXTMETRICEX *lpntme, int FontType, LPARAM lParam);
bool InitFontNames (const FoxjetCommon::CVersion & ver);
UINT ForceUniqueID (const FoxjetCommon::CElementList & v, FoxjetCommon::CBaseElement & e);
void ChangeConcatID (CMap <int, int, UINT, UINT> & concat, const CMap <UINT, UINT, UINT, UINT> & id);
bool ChangeConcatID (CMap <int, int, UINT, UINT> & map, UINT nOldID, UINT nNewID);
bool InitDatabase (FoxjetDatabase::COdbcDatabase & database);

#endif //__LISTIMP_H__
