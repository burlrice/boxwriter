#include "stdafx.h"

#include <math.h>

#include "BarcodeElement.h"
#include "Resource.h"
#include "CopyArray.h"
#include "List.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"
#include "Utils.h"
#include "TemplExt.h"
#include "BarcodeParams.h"
#include "BarcodeDlg.h"
#include "Extern.h"
#include "Coord.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

typedef enum { CODESET_A, CODESET_B, CODESET_C } CODESET;

typedef struct
{
	CODESET m_codeset;
	CString m_strData;
} DATA;

#define FUNCTION1	102
#define FUNCTION2	97
#define FUNCTION3	96

using namespace FoxjetCommon;
using namespace Color;
using namespace FoxjetElements;


class CPattern
{
public:
	CPattern (int nIndex = 0,
		int b1 = 0, int s1 = 0, 
		int b2 = 0, int s2 = 0, 
		int b3 = 0, int s3 = 0, 
		int b4 = 0);
	CPattern (const CPattern & rhs);
	CPattern & operator = (const CPattern & rhs);
	virtual ~CPattern ();

	CString GetChar (bool bNumeric = false) const;

	int		m_nBar1;
	int		m_nSpace1;
	int		m_nBar2;
	int		m_nSpace2;
	int		m_nBar3;
	int		m_nSpace3;
	int		m_nBar4;

	int		m_nIndex;
};

static void Optimize (const CString & str, CArray <CPattern, CPattern &> & vPattern, const CBarcodeParams & params);
static void Update (CArray <DATA, DATA &> & v, DATA & encode, CODESET codeset, CODESET & lastCodeset);

//////////////////////////////////////////////////////////////////////////////////////////

CPattern::CPattern (int nIndex, int b1, int s1, int b2, int s2, int b3, int s3, int b4)
:	m_nIndex	(nIndex),
	m_nBar1		(b1),
	m_nSpace1	(s1),
	m_nBar2		(b2),
	m_nSpace2	(s2),
	m_nBar3		(b3),
	m_nSpace3	(s3),
	m_nBar4		(b4)
{
}

CPattern::CPattern (const CPattern & rhs)
:	m_nIndex	(rhs.m_nIndex),
	m_nBar1		(rhs.m_nBar1),
	m_nSpace1	(rhs.m_nSpace1),
	m_nBar2		(rhs.m_nBar2),
	m_nSpace2	(rhs.m_nSpace2),
	m_nBar3		(rhs.m_nBar3),
	m_nSpace3	(rhs.m_nSpace3),
	m_nBar4		(rhs.m_nBar4)
{
}

CPattern & CPattern::operator = (const CPattern & rhs)
{
	if (this != &rhs) {
		m_nIndex	= rhs.m_nIndex;
		m_nBar1		= rhs.m_nBar1;
		m_nSpace1	= rhs.m_nSpace1;
		m_nBar2		= rhs.m_nBar2;
		m_nSpace2	= rhs.m_nSpace2;
		m_nBar3		= rhs.m_nBar3;
		m_nSpace3	= rhs.m_nSpace3;
		m_nBar4		= rhs.m_nBar4;
	}

	return * this;
}

CPattern::~CPattern () 
{
}

CString CPattern::GetChar (bool bNumeric) const 
{ 
	if (bNumeric) {
		if (m_nIndex >= 0 && m_nIndex <= 99) {
			CString str;

			str.Format (_T ("%02d"), m_nIndex);
			return str;
		}
	}

	switch (m_nIndex) {
	case 103:	return _T ("START_A");
	case 104:	return _T ("START_B");
	case 105:	return _T ("START_C");
	case 106:	return _T ("STOP");
	}

	return m_nIndex + _T (" "); 
}

//////////////////////////////////////////////////////////////////////////////////////////

int nPatternIndex = 0;
static const CPattern map [] =
{
	CPattern (nPatternIndex++, 2, 1, 2, 2, 2, 2),
	CPattern (nPatternIndex++, 2, 2, 2, 1, 2, 2),
	CPattern (nPatternIndex++, 2, 2, 2, 2, 2, 1),
	CPattern (nPatternIndex++, 1, 2, 1, 2, 2, 3),
	CPattern (nPatternIndex++, 1, 2, 1, 3, 2, 2),
	CPattern (nPatternIndex++, 1, 3, 1, 2, 2, 2),
	CPattern (nPatternIndex++, 1, 2, 2, 2, 1, 3),
	CPattern (nPatternIndex++, 1, 2, 2, 3, 1, 2),
	CPattern (nPatternIndex++, 1, 3, 2, 2, 1, 2),
	CPattern (nPatternIndex++, 2, 2, 1, 2, 1, 3),
	CPattern (nPatternIndex++, 2, 2, 1, 3, 1, 2),
	CPattern (nPatternIndex++, 2, 3, 1, 2, 1, 2),
	CPattern (nPatternIndex++, 1, 1, 2, 2, 3, 2),
	CPattern (nPatternIndex++, 1, 2, 2, 1, 3, 2),
	CPattern (nPatternIndex++, 1, 2, 2, 2, 3, 1),
	CPattern (nPatternIndex++, 1, 1, 3, 2, 2, 2),
	CPattern (nPatternIndex++, 1, 2, 3, 1, 2, 2),
	CPattern (nPatternIndex++, 1, 2, 3, 2, 2, 1),
	CPattern (nPatternIndex++, 2, 2, 3, 2, 1, 1),
	CPattern (nPatternIndex++, 2, 2, 1, 1, 3, 2),
	CPattern (nPatternIndex++, 2, 2, 1, 2, 3, 1),
	CPattern (nPatternIndex++, 2, 1, 3, 2, 1, 2),
	CPattern (nPatternIndex++, 2, 2, 3, 1, 1, 2),
	CPattern (nPatternIndex++, 3, 1, 2, 1, 3, 1),
	CPattern (nPatternIndex++, 3, 1, 1, 2, 2, 2),
	CPattern (nPatternIndex++, 3, 2, 1, 1, 2, 2),
	CPattern (nPatternIndex++, 3, 2, 1, 2, 2, 1),
	CPattern (nPatternIndex++, 3, 1, 2, 2, 1, 2),
	CPattern (nPatternIndex++, 3, 2, 2, 1, 1, 2),
	CPattern (nPatternIndex++, 3, 2, 2, 2, 1, 1),
	CPattern (nPatternIndex++, 2, 1, 2, 1, 2, 3),
	CPattern (nPatternIndex++, 2, 1, 2, 3, 2, 1),
	CPattern (nPatternIndex++, 2, 3, 2, 1, 2, 1),
	CPattern (nPatternIndex++, 1, 1, 1, 3, 2, 3),
	CPattern (nPatternIndex++, 1, 3, 1, 1, 2, 3),
	CPattern (nPatternIndex++, 1, 3, 1, 3, 2, 1),
	CPattern (nPatternIndex++, 1, 1, 2, 3, 1, 3),
	CPattern (nPatternIndex++, 1, 3, 2, 1, 1, 3),
	CPattern (nPatternIndex++, 1, 3, 2, 3, 1, 1),
	CPattern (nPatternIndex++, 2, 1, 1, 3, 1, 3),
	CPattern (nPatternIndex++, 2, 3, 1, 1, 1, 3),
	CPattern (nPatternIndex++, 2, 3, 1, 3, 1, 1),
	CPattern (nPatternIndex++, 1, 1, 2, 1, 3, 3),
	CPattern (nPatternIndex++, 1, 1, 2, 3, 3, 1),
	CPattern (nPatternIndex++, 1, 3, 2, 1, 3, 1),
	CPattern (nPatternIndex++, 1, 1, 3, 1, 2, 3),
	CPattern (nPatternIndex++, 1, 1, 3, 3, 2, 1),
	CPattern (nPatternIndex++, 1, 3, 3, 1, 2, 1),
	CPattern (nPatternIndex++, 3, 1, 3, 1, 2, 1),
	CPattern (nPatternIndex++, 2, 1, 1, 3, 3, 1),
	CPattern (nPatternIndex++, 2, 3, 1, 1, 3, 1),
	CPattern (nPatternIndex++, 2, 1, 3, 1, 1, 3),
	CPattern (nPatternIndex++, 2, 1, 3, 3, 1, 1),
	CPattern (nPatternIndex++, 2, 1, 3, 1, 3, 1),
	CPattern (nPatternIndex++, 3, 1, 1, 1, 2, 3),
	CPattern (nPatternIndex++, 3, 1, 1, 3, 2, 1),
	CPattern (nPatternIndex++, 3, 3, 1, 1, 2, 1),
	CPattern (nPatternIndex++, 3, 1, 2, 1, 1, 3),
	CPattern (nPatternIndex++, 3, 1, 2, 3, 1, 1),
	CPattern (nPatternIndex++, 3, 3, 2, 1, 1, 1),
	CPattern (nPatternIndex++, 3, 1, 4, 1, 1, 1),
	CPattern (nPatternIndex++, 2, 2, 1, 4, 1, 1),
	CPattern (nPatternIndex++, 4, 3, 1, 1, 1, 1),
	CPattern (nPatternIndex++, 1, 1, 1, 2, 2, 4),
	CPattern (nPatternIndex++, 1, 1, 1, 4, 2, 2),
	CPattern (nPatternIndex++, 1, 2, 1, 1, 2, 4),
	CPattern (nPatternIndex++, 1, 2, 1, 4, 2, 1),
	CPattern (nPatternIndex++, 1, 4, 1, 1, 2, 2),
	CPattern (nPatternIndex++, 1, 4, 1, 2, 2, 1),
	CPattern (nPatternIndex++, 1, 1, 2, 2, 1, 4),
	CPattern (nPatternIndex++, 1, 1, 2, 4, 1, 2),
	CPattern (nPatternIndex++, 1, 2, 2, 1, 1, 4),
	CPattern (nPatternIndex++, 1, 2, 2, 4, 1, 1),
	CPattern (nPatternIndex++, 1, 4, 2, 1, 1, 2),
	CPattern (nPatternIndex++, 1, 4, 2, 2, 1, 1),
	CPattern (nPatternIndex++, 2, 4, 1, 2, 1, 1),
	CPattern (nPatternIndex++, 2, 2, 1, 1, 1, 4),
	CPattern (nPatternIndex++, 4, 1, 3, 1, 1, 1),
	CPattern (nPatternIndex++, 2, 4, 1, 1, 1, 2),
	CPattern (nPatternIndex++, 1, 3, 4, 1, 1, 1),
	CPattern (nPatternIndex++, 1, 1, 1, 2, 4, 2),
	CPattern (nPatternIndex++, 1, 2, 1, 1, 4, 2),
	CPattern (nPatternIndex++, 1, 2, 1, 2, 4, 1),
	CPattern (nPatternIndex++, 1, 1, 4, 2, 1, 2),
	CPattern (nPatternIndex++, 1, 2, 4, 1, 1, 2),
	CPattern (nPatternIndex++, 1, 2, 4, 2, 1, 1),
	CPattern (nPatternIndex++, 4, 1, 1, 2, 1, 2),
	CPattern (nPatternIndex++, 4, 2, 1, 1, 1, 2),
	CPattern (nPatternIndex++, 4, 2, 1, 2, 1, 1),
	CPattern (nPatternIndex++, 2, 1, 2, 1, 4, 1),
	CPattern (nPatternIndex++, 2, 1, 4, 1, 2, 1),
	CPattern (nPatternIndex++, 4, 1, 2, 1, 2, 1),
	CPattern (nPatternIndex++, 1, 1, 1, 1, 4, 3),
	CPattern (nPatternIndex++, 1, 1, 1, 3, 4, 1),
	CPattern (nPatternIndex++, 1, 3, 1, 1, 4, 1),
	CPattern (nPatternIndex++, 1, 1, 4, 1, 1, 3),
	CPattern (nPatternIndex++, 1, 1, 4, 3, 1, 1),
	CPattern (nPatternIndex++, 4, 1, 1, 1, 1, 3),
	CPattern (nPatternIndex++, 4, 1, 1, 3, 1, 1),
	CPattern (nPatternIndex++, 1, 1, 3, 1, 4, 1),
	CPattern (nPatternIndex++, 1, 1, 4, 1, 3, 1),
	CPattern (nPatternIndex++, 3, 1, 1, 1, 4, 1),
	CPattern (nPatternIndex++, 4, 1, 1, 1, 3, 1),
	CPattern (nPatternIndex++, 2, 1, 1, 4, 1, 2),		//START (Code A)
	CPattern (nPatternIndex++, 2, 1, 1, 2, 1, 4),		//START (Code B)
	CPattern (nPatternIndex++, 2, 1, 1, 2, 3, 2),		//START (Code C)
	CPattern (nPatternIndex++, 2, 3, 3, 1, 1, 1, 2),	//STOP
};
static const CPattern startA	= ::map [ARRAYSIZE (::map) - 4];
static const CPattern startB	= ::map [ARRAYSIZE (::map) - 3];
static const CPattern startC	= ::map [ARRAYSIZE (::map) - 2];
static const CPattern stop		= ::map [ARRAYSIZE (::map) - 1];

static const CPattern BtoC		= ::map [99];
static const CPattern CtoB		= ::map [100];


//////////////////////////////////////////////////////////////////////////////////////////

static bool IsCodeC (const CString & str);
static bool IsValidData (const CString & str);
static CPattern CalcCheckDigit (CArray <CPattern, CPattern &> & v);

//////////////////////////////////////////////////////////////////////////////////////////

static bool IsCodeC (const CString & str)
{
	bool bResult = false;

	if ((str.GetLength () % 2) == 0) {
		bResult = true;

		for (int i = 0; i < str.GetLength (); i++)
			if (!isdigit (str [i])) {
				bResult = false;
				break;
			}
	}

	return bResult;
}

static bool IsValidData (const CString & str)
{
	const int nMin = 0;
	const int nMax = (ARRAYSIZE (::map) - 1) - 4;
	bool bResult = false;

	if (IsCodeC (str))
		bResult = true;
	else if (str.GetLength ()) {
		bResult = true;

		for (int i = 0; i < str.GetLength (); i++) {
			TCHAR c = str [i];

			switch (c) {
			case DC1:
			case DC2:
			case DC3:
				break; // skip now, will be replaced later with correct value
			default:
				{
					int nDigit = c - ' ';

					if (nDigit < nMin || nDigit > nMax) 
						return false;
				}
				break;
			}
		}
	}

	return bResult;
}

static CPattern CalcCheckDigit (CArray <CPattern, CPattern &> & v)
{
	int nTotal = 0;

	for (int i = 0; i < v.GetSize (); i++) {
		int nPos = BindTo (i, 1, (int)ARRAYSIZE (::map));
		int nValue = v [i].m_nIndex;

		nTotal += (nValue * nPos);
	}

	int nDigit = nTotal % 103;

	return ::map [nDigit];
}

//////////////////////////////////////////////////////////////////////////////////////////

bool CBarcodeElement::IsAiCaption () const
{
	for (int i = 0; i < GetSubElementCount (); i++) {
		const CSubElement & e = GetSubElement (i);

		if (!e.IsAi ())
			return false;
	}

	return true;
}

CString CBarcodeElement::FormatCaption () const
{
	CString str;
	//bool bAI = IsAiCaption ();

	for (int i = 0; i < GetSubElementCount (); i++) {
		const CSubElement & e = GetSubElement (i);

		//if (bAI)
		if (e.IsAi ())
			str += _T ("(") + e.m_strID + _T (")");

		str += e.GetElement ()->GetImageData ();
	}

	return str;
}

int CBarcodeElement::DrawC128Symbol (const CString & str, const CString & strCaption, const CBarcodeParams & params, 
									 const FoxjetDatabase::HEADSTRUCT & head, LPBYTE pBuffer, 
									 UINT nBufferWidth, ULONG lBufferSize, CDC * pDC, 
									 CBitmap * pbmpCaption,
									 FoxjetCommon::DRAWCONTEXT context,
									 CString * pstrFormattedCaption)
{
	int nWidth = 0;
	CString strData (str);
	CArray <CPattern, CPattern &> v;
	int nQuietzone = CBaseElement::ThousandthsToLogical (CPoint (params.GetQuietZone (), 0), head).x;
	CSize bearer = CBaseElement::ThousandthsToLogical (CPoint (params.GetVertBearer (), params.GetHorzBearer ()), head);
	double dStretch [2] = { 0, 0 };


	CBaseElement::CalcStretch (head, dStretch);
	bearer.cy = (int)((double)bearer.cy / dStretch [1]);

	nQuietzone = StretchToPrinterRes (nQuietzone, head);
	bearer.cx = StretchToPrinterRes (bearer.cx, head);

	if (IsValidData (strData)) {
		const int nBarHeight		= min ((int)head.GetChannels (), params.GetBarHeight ()); //params.GetBarHeight ();
		const BYTE nBarMask			= GetBarMask (nBarHeight);
		const BYTE nNotBarMask		= ~nBarMask;

		Optimize (str, v, params);

		nWidth += nQuietzone + bearer.cx; 

		for (int i = 0; i < v.GetSize (); i++) {
			CPattern p = v [i];
			struct
			{
				int		m_nWidth;
				bool	m_bBar;
			}
			const bars [7] =
			{
				{ params.GetBarPixels	(p.m_nBar1),	true,	},
				{ params.GetSpacePixels	(p.m_nSpace1),	false,	},
				{ params.GetBarPixels	(p.m_nBar2),	true,	},
				{ params.GetSpacePixels	(p.m_nSpace2),	false,	},
				{ params.GetBarPixels	(p.m_nBar3),	true,	},
				{ params.GetSpacePixels	(p.m_nSpace3),	false,	},
				{ params.GetBarPixels	(p.m_nBar4),	true,	},
			};

/*
{// TODO: rem
	CString str;
	str.Format ("%4d: %3d [%3d] %3d [%3d] %3d [%3d] %3d", 
		p.m_nIndex, 
		bars [0].m_nWidth,
		bars [1].m_nWidth,
		bars [2].m_nWidth,
		bars [3].m_nWidth,
		bars [4].m_nWidth,
		bars [5].m_nWidth,
		bars [6].m_nWidth);
	TRACEF (str);

	for (int i = 0; i < ARRAYSIZE (bars); i++) { 
		if (bars [i].m_bBar)
			ASSERT ((bars [i].m_nWidth % 2) == 0);
		else
			ASSERT ((bars [i].m_nWidth % 2) == 1);
	}
}
*/
			for (int nBar = 0; nBar < ARRAYSIZE (bars); nBar++) {
				int cx = bars [nBar].m_nWidth;

				if (bars [nBar].m_bBar) {
					for (int nCol = 0; nCol < cx; nCol++) { 
						int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
						int nBytes = nBarHeight / 8;

						if (pBuffer) {
							memset (&pBuffer [nIndex], 0, nBytes); 

							if ((ULONG)(nIndex + nBytes) < lBufferSize)
								memcpy (&pBuffer [nIndex + nBytes], &nBarMask, 1); 
						}
					}
				}

				nWidth += cx;
			}
		}

		nWidth += nQuietzone + bearer.cx; 
	
		if (pbmpCaption) 
			DrawCaption (* pbmpCaption, strCaption, params, head, pDC, context, nWidth);
	}

	if (pstrFormattedCaption)
		* pstrFormattedCaption = strCaption;

	return nWidth;
}

static void Update (CArray <DATA, DATA &> & v, DATA & encode, CODESET codeset, CODESET & lastCodeset)
{
	if (lastCodeset != codeset) {
		v.Add (encode);
		encode.m_codeset = codeset;
		encode.m_strData.Empty ();
	}

	lastCodeset = codeset;
}

static void Optimize (const CString & str, CArray <CPattern, CPattern &> & vPattern, const CBarcodeParams & params)
{
	CStringArray vData;
	int nFNC1 = 0;
	DATA encode;
	CODESET lastCodeset = encode.m_codeset = CODESET_C;
	CArray <DATA, DATA &> v;

	//TRACEF (str);

	for (int i = 0; i < str.GetLength (); i++) {
		const TCHAR c1 = str [i];

		if (c1 == DC1) {
			nFNC1++;
			//TRACEF ("FNC1");
			encode.m_strData += c1;
			continue;
		}

		if ((i + 1) < str.GetLength ()) {
			const TCHAR c2 = str [i + 1];

			if (c2 == DC1) {
				nFNC1++;
				//TRACEF ("B: " + CString (c1));
				//TRACEF ("FNC1");

				Update (v, encode, CODESET_B, lastCodeset);
				encode.m_strData += c1;
				encode.m_strData += c2;
				i++;
				continue;
			}

			bool bCodeC = _istdigit (c1) && _istdigit (c2) && (((i - nFNC1) % 2) == 0);
			
			if (bCodeC) {
				//TRACEF ("C: " + CString (c1) + CString (c2));

				Update (v, encode, CODESET_C, lastCodeset);
				encode.m_strData += c1;
				encode.m_strData += c2;

				i++;
			}
			else {
				//TRACEF ("B: " + CString (c1));

				Update (v, encode, CODESET_B, lastCodeset);
				encode.m_strData += c1;
			}
		}
		else {
			//TRACEF ("B: " + CString (c1));

			Update (v, encode, CODESET_B, lastCodeset);
			encode.m_strData += c1;
		}
	}

	if (encode.m_strData)
		v.Add (encode);

	for (int i = v.GetSize () - 1; i >= 0; i--) 
		if (v [i].m_strData.IsEmpty ())
			v.RemoveAt (i);

#ifdef _DEBUG
	{ 
		CString strVerify;

		for (int i = 0; i < v.GetSize (); i++) {
			DATA encode = v [i];

			strVerify += encode.m_strData;

			/*
			{
				CString str;
				static const LPCTSTR lpsz [] = { "CODESET_A", "CODESET_B", "CODESET_C" };
				str.Format ("%s: %s [%d chars]",
					lpsz [encode.m_codeset],
					encode.m_strData,
					encode.m_strData.GetLength ());
				TRACEF (str);
			}
			*/
		}

		//TRACEF (strVerify);
		//TRACEF (str);
		ASSERT (strVerify == str);
	}
#endif

	{
		CODESET lastCodset = CODESET_C;

		if (v.GetSize ()) {
			lastCodeset = v [0].m_codeset;
			vPattern.Add (CPattern (lastCodeset == CODESET_C ? ::startC : ::startB));
		}

		for (int i = 0; i < v.GetSize (); i++) {
			DATA encode = v [i];

			if (lastCodeset != encode.m_codeset) {
				if (encode.m_codeset == CODESET_C)
					vPattern.Add (CPattern (::BtoC));
				else
					vPattern.Add (CPattern (::CtoB));
				
				lastCodeset = encode.m_codeset;
			}

			if (encode.m_codeset == CODESET_C) {
				for (int i = 0; i < encode.m_strData.GetLength (); i++) {
					int nIndex;

					switch (encode.m_strData [i]) {
					case DC1:		nIndex = FUNCTION1;			break;
					case DC2:		nIndex = FUNCTION2;			break;
					case DC3:		nIndex = FUNCTION3;			break;
					default:
						{
							CString strSeg = encode.m_strData.Mid (i, 2);
							nIndex = _ttoi (strSeg);
							i++;
						}
						break;
					}

					vPattern.Add (CPattern (::map [nIndex]));
				}
			}
			else { // if (encode.m_codeset == CODESET_B) 
				for (int i = 0; i < encode.m_strData.GetLength (); i++) {
					TCHAR c = encode.m_strData [i];
					int nIndex;

					switch (c) {
					case DC1:		nIndex = FUNCTION1;			break;
					case DC2:		nIndex = FUNCTION2;			break;
					case DC3:		nIndex = FUNCTION3;			break;
					default:		nIndex = c - ' ';			break;
					}

					vPattern.Add (CPattern (::map [nIndex]));
				}
			}
		}

		if (params.GetCheckSum ())
			vPattern.Add (CalcCheckDigit (vPattern));

		vPattern.Add (CPattern (::stop));


/*
		#ifdef _DEBUG
		{ 
			CString str;

			for (int i = 0; i < vPattern.GetSize (); i++) {
				str.Format ("vPattern [%d]: %d",
					i, vPattern [i].m_nIndex);
				TRACEF (str);
			}
		}
		#endif //_DEBUG
*/
	}
}

CSize CBarcodeElement::DrawC128 (CDC &dc, const FoxjetDatabase::HEADSTRUCT & head, 
								 CSize & sizeLogical, FoxjetCommon::DRAWCONTEXT context, 
								 bool bThrowException, const CBarcodeParams & params, int nSymbology)
{
	ASSERT (kSymbologyEAN128 != -1);
	CString str = GetImageData ();
	const CString strCaption = FormatCaption ();
	CSize bearer = CBaseElement::ThousandthsToLogical (CPoint (params.GetVertBearer (), params.GetHorzBearer ()), head);
	int nHeight = min ((int)head.GetChannels (), params.GetBarHeight ());
	int nWidth = 0;
	double dStretch [2] = { 0, 0 };
	bool bEAN = nSymbology == kSymbologyEAN128;

	CBaseElement::CalcStretch (head, dStretch);
	bearer.cy = (int)((double)bearer.cy / dStretch [1]);
	
	if (!IsValidData (str)) {
		if (bThrowException)
			throw new CBarcodeException (LoadString (bEAN ? IDS_INVALIDEAN128DATA : IDS_INVALIDC128DATA));
		else
			str = FoxjetElements::GetDefaultData (kSymbologyCode128, params);
	}

	nWidth = DrawC128Symbol (str, strCaption, params, head, NULL, 0, 0, NULL, NULL, context, &m_caption.m_strText);

	const LONG lRowLen = (int)ceil ((long double)nHeight / 16.0) * 16; // win bitmap is word aligned
	const int nBufferSize = lRowLen * nWidth / 8;
	LPBYTE pBuffer = new BYTE [nBufferSize];
	CBitmap bmp, bmpCaption;
	CDC dcMem;

	dcMem.CreateCompatibleDC (&dc);
	memset (pBuffer, ~0, nBufferSize);

	DrawC128Symbol (str, strCaption, params, head, pBuffer, lRowLen, nBufferSize, &dc, &bmpCaption, context, &m_caption.m_strText);
	Blt (dc, head, context, params, nWidth, nHeight, pBuffer, bmp, bmpCaption);

	delete [] pBuffer;

	if (context == PRINTER) 
		swap (nWidth, nHeight);


/*
	int x = 0, y = 0, cx = nWidth, cy = nHeight;

	if (context == PRINTER) {
		// flip h
		x += (cx - 1);
		cx *= -1;
	}

//	dc.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, nWidth, nHeight, !IsInverse () ? SRCINVERT : SRCCOPY);
*/

	CBitmap * pMem = dcMem.SelectObject (&bmp);
	dc.BitBlt (0, 0, nWidth, nHeight, &dcMem, 0, 0, !IsInverse () ? SRCINVERT : SRCCOPY);
	dcMem.SelectObject (pMem);

	DrawBearer (dc, bearer, context, params, nWidth, nHeight, head);

	CCoord c ((double)nWidth, (double)nHeight, INCHES, head);
	CSize size = m_size = CSize ((int)ceil (c.m_x * 1000.0), (int)ceil (c.m_y * 1000.0));

	sizeLogical = CSize (nWidth, nHeight);

	return size;
}


