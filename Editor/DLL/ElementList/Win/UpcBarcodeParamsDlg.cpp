// UpcBarcodeParams.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "UpcBarcodeParamsDlg.h"
#include "Resource.h"
#include "BarcodeParams.h"
#include "BarcodeElement.h"
#include "Head.h"
#include "Extern.h"
#include "Database.h"
#include "Debug.h"
#include "Parse.h"

using namespace FoxjetElements;
using namespace FoxjetElements::TextElement;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ListGlobals;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUpcBarcodeParamsDlg dialog


CUpcBarcodeParamsDlg::CUpcBarcodeParamsDlg(UINT nID, HEADTYPE type, CWnd* pParent)
:	m_type (type),
	m_orient (NORMAL),
	FoxjetCommon::CEliteDlg (nID, pParent)
{
	m_bBold = FALSE;
	m_bItalic = FALSE;
	m_bChecksum = FALSE;
	m_nBarHeight = 0;
	m_nMag = 0;
	m_strSymbology = _T("");
	m_nSize = 0;
	m_nFontWidth = 0;

	for (int i = 0; i < 4; i++)
		m_nSpace [i] = m_nBar [i] = 0;

	VERIFY (m_bmpOrientation [0].LoadBitmap (IDB_NORMAL));
	VERIFY (m_bmpOrientation [1].LoadBitmap (IDB_VERTICAL));
}

CUpcBarcodeParamsDlg::CUpcBarcodeParamsDlg(HEADTYPE type, CWnd* pParent /*=NULL*/)
:	m_type (type),
	FoxjetCommon::CEliteDlg (IDD_UPCBARCODEPARAMS, pParent)
{
	CUpcBarcodeParamsDlg::CUpcBarcodeParamsDlg (IDD_UPCBARCODEPARAMS, type, pParent);

	VERIFY (m_bmpOrientation [0].LoadBitmap (IDB_NORMAL));
	VERIFY (m_bmpOrientation [1].LoadBitmap (IDB_VERTICAL));
}


void CUpcBarcodeParamsDlg::DoDataExchange(CDataExchange* pDX)
{
	struct 
	{
		UINT m_nBarID;
		UINT m_nSpaceID;
	} static const map [] = 
	{
		{ TXT_BAR1,		TXT_SPACE1 },
		{ TXT_BAR2,		TXT_SPACE2 },
		{ TXT_BAR3,		TXT_SPACE3 },
		{ TXT_BAR4,		TXT_SPACE4 },
	};

	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUpcBarcodeParamsDlg)
	//}}AFX_DATA_MAP

	DDX_Text(pDX, TXT_SYMBOLOGY, m_strSymbology);
	DDX_Check(pDX, CHK_BOLD, m_bBold);
	DDX_Check(pDX, CHK_ITALIC, m_bItalic);

	if (GetDlgItem (CHK_CHECKSUM))
		DDX_Check(pDX, CHK_CHECKSUM, m_bChecksum);

	if (GetDlgItem (RDO_NORMAL))
		DDX_Radio (pDX, RDO_NORMAL, (int &)m_orient);

	if (GetDlgItem (TXT_BARHEIGHT)) {
		DDX_Text(pDX, TXT_BARHEIGHT, m_nBarHeight);

		if (m_orient == NORMAL) 
			DDV_MinMaxInt (pDX, m_nBarHeight, CBarcodeElement::m_lmtBarHeight.m_dwMin, GetHeadChannels (m_type)); //CBarcodeElement::m_lmtBarHeight.m_dwMax);
		else 
			DDV_MinMaxInt (pDX, m_nBarHeight, CBarcodeElement::m_lmtBarHeightVertical.m_dwMin, CBarcodeElement::m_lmtBarHeightVertical.m_dwMax);
	}

	DDX_Text(pDX, TXT_MAG, m_nMag);
	DDV_MinMaxInt (pDX, m_nMag, CBarcodeElement::m_lmtMag.m_dwMin, CBarcodeElement::m_lmtMag.m_dwMax);
	
	for (int i = 0; i < 4; i++) {
		if (GetDlgItem (map [i].m_nBarID)) {
			DDX_Text(pDX, map [i].m_nBarID, m_nBar [i]);
			DDV_MinMaxInt (pDX, m_nBar [i], CBarcodeParams::m_lmtBar.m_dwMin, CBarcodeParams::m_lmtBar.m_dwMax);
		}

		if (GetDlgItem (map [i].m_nSpaceID)) {
			DDX_Text(pDX, map [i].m_nSpaceID, m_nSpace [i]);
			DDV_MinMaxInt (pDX, m_nSpace  [i], CBarcodeParams::m_lmtBar.m_dwMin, CBarcodeParams::m_lmtBar.m_dwMax);
		}
	}

	DDX_Text(pDX, TXT_SIZE, m_nSize);
	DDX_Text(pDX, TXT_FONTWIDTH, m_nFontWidth);
}


BEGIN_MESSAGE_MAP(CUpcBarcodeParamsDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CUpcBarcodeParamsDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (RDO_NORMAL, OnOrientationClick)
	ON_BN_CLICKED (RDO_VERTICAL, OnOrientationClick)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUpcBarcodeParamsDlg message handlers

BOOL CUpcBarcodeParamsDlg::OnInitDialog() 
{
	CStringArray vFonts;
	CComboBox * pFont = (CComboBox *)GetDlgItem (CB_FONT);
	
	CHead::GetWinFontNames (vFonts);

	ASSERT (pFont);

	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	for (int i = 0; i < vFonts.GetSize (); i++) {
		CString str = vFonts [i];
		int nIndex = pFont->AddString (str);

		if (!str.CompareNoCase (m_strFont))
			pFont->SetCurSel (nIndex);
	}

	if (!m_bStandardParam) {
		UINT nID [] = 
		{
			TXT_MAG,
			TXT_BAR1,
			TXT_BAR2,
			TXT_BAR3,
			TXT_BAR4,
			TXT_SPACE1,
			TXT_SPACE2,
			TXT_SPACE3,
			TXT_SPACE4,
			RDO_NORMAL,
			RDO_VERTICAL,
		};

		for (int i = 0; i < (sizeof (nID) / sizeof (nID [0])); i++) {
			if (CWnd * p = GetDlgItem (nID [i]))
				p->EnableWindow (FALSE);
		}
	}

	OnOrientationClick ();

	return TRUE;
}

int CUpcBarcodeParamsDlg::GetDefSymbology () const 
{
	int nSymbology = FoxjetElements::GetSymbology (m_strSymbology, GetDB ());

	if (nSymbology == kSymbologyDataMatrixEcc200 || nSymbology == kSymbologyDataMatrixGs1)
		return kSymbologyDataMatrixEcc200;

	return nSymbology == -1 ? kSymbologyUPCA : nSymbology;
}

void CUpcBarcodeParamsDlg::OnOK()
{
	CComboBox * pFont = (CComboBox *)GetDlgItem (CB_FONT);

	ASSERT (pFont);

	int nIndex = pFont->GetCurSel ();
	ASSERT (nIndex != CB_ERR);

	if (pFont->GetLBTextLen (nIndex) > 0)
		pFont->GetLBText (nIndex, m_strFont);

	if (UpdateData (TRUE)) {
		try {
			int nSymbology = GetDefSymbology ();
			TRACEF (m_strSymbology + _T (": ") + FoxjetElements::GetSymbology (nSymbology, GetDB ()));
			CBarcodeParams params (nSymbology, 0, m_nBarHeight, 0, 0, m_nMag);
			CBarcodeElement barcode ((const CBarcodeElement &)defElements.GetElement (BARCODE));
			HEADSTRUCT head;
			CSubElement & sub = * new CSubElement (TEXT);
			CDC dcMem;
			CString strData = FoxjetElements::GetDefaultData (nSymbology, params);

			GetFirstHeadRecord (head);
			head.m_nChannels = GetHeadChannels (m_type);
			TRACEF (FoxjetElements::GetSymbology (nSymbology, ListGlobals::GetDB ()) + _T (": ") + strData);

			dcMem.CreateCompatibleDC (GetDC ());

			barcode.SetSymbology (nSymbology);
			barcode.DeleteAllSubElements ();
			sub.m_strDataMask = CString (CSubElement::m_cAnyOpt, strData.GetLength ());
			TRACEF (FoxjetDatabase::ToString (strData.GetLength ()) + _T (": ") + strData);
			TRACEF (FoxjetDatabase::ToString (sub.m_strDataMask.GetLength ()) + _T (": ") + sub.m_strDataMask);
			sub.SetData (strData);
			barcode.InsertSubElement (0, sub);

			params.SetOrientation (m_orient);
			params.SetCheckSum (m_bChecksum);

			for (int i = 0; i < 4; i++) {
				params.SetBarPixels (i + 1, m_nBar [i]);
				params.SetSpacePixels (i + 1, m_nSpace [i]);
			}

			barcode.ClipTo (head);
			barcode.Build ();
			CSize size1000ths = barcode.DrawBarcode (dcMem, head, CSize (), FoxjetCommon::EDITOR, true, &params);
			CSize size = CBaseElement::ThousandthsToLogical (size1000ths, head);

			if (m_orient == NORMAL) {
				if (size.cy > (int)head.GetChannels () || m_nBarHeight > (int)head.GetChannels ()) {
					MsgBox (* this, LoadString (IDS_SYMBOLTOOTALL));
					return;
				}
			}

			FoxjetCommon::CEliteDlg::OnOK ();
		}
		catch (CBarcodeException * e) { 
			MsgBox (* this, e->GetErrorMessage ());
			HANDLEEXCEPTION_TRACEONLY (e);
			return;
		}
	}
}

void CUpcBarcodeParamsDlg::OnOrientationClick()
{
	CButton * pRadio [] = 
	{
		(CButton *)GetDlgItem (RDO_NORMAL),
		(CButton *)GetDlgItem (RDO_VERTICAL),
	};

	for (int i = 0; i < 2; i++) {
		if (pRadio [i]) {
			if (pRadio [i]->GetCheck () == 1) {
				CStatic * pBmp = (CStatic *)GetDlgItem (BMP_TEXT);
				ASSERT (pBmp);
				
				pBmp->SetBitmap (m_bmpOrientation [i]);
			}
		}
	}
}
