// DateTimePropShtDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "resource.h"
#include "DateTimePropShtDlg.h"
#include "Debug.h"
#include "Database.h"
#include "SystemDlg.h"

#include "MonthsDlg.h"
#include "HoursDlg.h"
#include "DaysDlg.h"
#include "RolloverDlg.h"
#include "Extern.h"
#include "Registry.h"
#include "Parse.h"
#include "AppVer.h"
#include "QuarterHourCodeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;

extern CDiagnosticCriticalSection csDays;

extern CodesDlg::CCodesArray vMonthcodes;
extern CodesDlg::CCodesArray vHourcodes;
extern CodesDlg::CCodesArray vDayOfWeekCodes;
extern CodesDlg::CCodesArray vRollover;
extern CodesDlg::CCodesArray vQuarterHourcodes;

/////////////////////////////////////////////////////////////////////////////

CodesDlg::CCodes CDateTimePropShtDlg::GetCodes (ULONG lLineID, const CodesDlg::CCodesArray & v)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i].m_lLineID == lLineID)
			return v [i];

	return v [0];
}

/////////////////////////////////////////////////////////////////////////////

bool CDateTimePropShtDlg::SetCodes (FoxjetDatabase::COdbcDatabase & db, ULONG lLineID, const CodesDlg::CCodes & c, CodesDlg::CCodesArray & v, const CString & strName)
{
	for (int i = 0; i < v.GetSize (); i++) {
		if (v [i].m_lLineID == lLineID) {
			SETTINGSSTRUCT s;

			v [i] = c;
	
			s.m_lLineID = c.m_lLineID;
			s.m_strKey	= strName;

			for (int nItem = 0; nItem < c.m_vCodes.GetSize (); nItem++) {
				s.m_strData += FormatString (c.m_vCodes [nItem]);

				if ((nItem + 1) != c.m_vCodes.GetSize ())
					s.m_strData += ',';
			}

			if (!UpdateSettingsRecord (db, s))
				return AddSettingsRecord (db, s);

			return true;
		}
	}

	return false;
}


/////////////////////////////////////////////////////////////////////////////
// CDateTimePropShtDlg

const UINT CDateTimePropShtDlg::WM_LINECHANGED = ::RegisterWindowMessage (_T ("CDateTimePropShtDlg::OnSelchangeLine"));
const UINT CDateTimePropShtDlg::WM_GETLINEID = ::RegisterWindowMessage (_T ("CDateTimePropShtDlg::OnGetLineID"));


IMPLEMENT_DYNAMIC(CDateTimePropShtDlg, CEnPropertySheet)

CDateTimePropShtDlg::CDateTimePropShtDlg(FoxjetDatabase::COdbcDatabase & db, UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
:	m_lLineID (-1),
	m_db (db),
	CEnPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
	Init ();
}

CDateTimePropShtDlg::CDateTimePropShtDlg(FoxjetDatabase::COdbcDatabase & db, LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
:	m_lLineID (-1),
	m_db (db),
	CEnPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	Init ();
}

CDateTimePropShtDlg::~CDateTimePropShtDlg()
{
	for (int i = 0; i < GetPageCount (); i++)
		if (CPropertyPage * p = GetPage (i))
			delete p;
}


BEGIN_MESSAGE_MAP(CDateTimePropShtDlg, CEnPropertySheet)
	//{{AFX_MSG_MAP(CDateTimePropShtDlg)
	//}}AFX_MSG_MAP
	ON_COMMAND(IDOK, OnOK)
	ON_COMMAND(IDCANCEL, OnCancel)
	ON_REGISTERED_MESSAGE (CDateTimePropShtDlg::WM_LINECHANGED, OnLineChanged)
	ON_REGISTERED_MESSAGE (CDateTimePropShtDlg::WM_GETLINEID, OnGetLineID)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDateTimePropShtDlg message handlers

void CDateTimePropShtDlg::Init () 
{
	LOCK (::csDays);

	const CString strRegSection = ListGlobals::defElements.m_strRegSection + _T ("\\CDateTimePropShtDlg");


	m_vMonthcodes		= ::vMonthcodes;
	m_vHourcodes		= ::vHourcodes;
	m_vDayOfWeekCodes	= ::vDayOfWeekCodes;
	m_vRollover			= ::vRollover;

	AddPage (new CMonthsDlg				(m_db, m_vMonthcodes));
	AddPage (new CHoursDlg				(m_db, m_vHourcodes));
	
	// std // #if __CUSTOM__ == __SW0840__
	m_vQuarterHourcodes = ::vQuarterHourcodes;
	AddPage (new CQuarterHourCodeDlg	(m_db, m_vQuarterHourcodes));
	// std // #endif 

	AddPage (new CDaysDlg				(m_db, m_vDayOfWeekCodes));
	AddPage (new CRolloverDlg			(m_db, m_vRollover));

	m_lLineID = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strRegSection, _T ("LineID"), -1);
	SetActivePage (FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strRegSection, _T ("ActiveIndex"), 0));

	BEGIN_TRANS (m_db);
}

void CDateTimePropShtDlg::OnOK()
{
	const CString strRegSection = ListGlobals::defElements.m_strRegSection + _T ("\\CDateTimePropShtDlg");

	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strRegSection, _T ("LineID"), m_lLineID);
	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strRegSection, _T ("ActiveIndex"), GetActiveIndex ());

	COMMIT_TRANS (m_db);
	EndDialog (IDOK);
}

void CDateTimePropShtDlg::OnCancel()
{
	ROLLBACK_TRANS (m_db);
	EndDialog (IDCANCEL);
}

LRESULT CDateTimePropShtDlg::OnLineChanged (WPARAM wParam, LPARAM lParam)
{
	m_lLineID = (ULONG)lParam;
	return 0;
}

LRESULT CDateTimePropShtDlg::OnGetLineID (WPARAM wParam, LPARAM lParam)
{
	return m_lLineID;
}


BOOL CDateTimePropShtDlg::OnInitDialog() 
{
	CEnPropertySheet::OnInitDialog();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


