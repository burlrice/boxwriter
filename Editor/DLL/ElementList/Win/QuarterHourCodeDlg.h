#if !defined(AFX_QUARTERHOURCODEDLG_H__AA1EC135_D892_4C6F_B1B3_D6B149C8208F__INCLUDED_)
#define AFX_QUARTERHOURCODEDLG_H__AA1EC135_D892_4C6F_B1B3_D6B149C8208F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// QuarterHourCodeDlg.h : header file
//

#include "AppVer.h"
#include "ListCtrlImp.h"
#include "resource.h"
#include "DateTimePropShtDlg.h"
#include "EditValueDlg.h"

// std // #if __CUSTOM__ == __SW0840__

/////////////////////////////////////////////////////////////////////////////
// CQuarterHourCodeDlg dialog

class CQuarterHourCodeDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CQuarterHourCodeDlg)

	CQuarterHourCodeDlg (); // only implemented for DYNCREATE

	class CEditDlg : public CEditValueDlg
	{
	public:
		CEditDlg (CWnd * pParent);

		virtual void DoDataExchange(CDataExchange* pDX);
	};

	class CItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CItem (const CString & str, int nIndex);
		virtual ~CItem ();

		virtual CString GetDispText (int nColumn) const;
		virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;

		CString m_str;
		int m_nIndex;
	};

// Construction
public:
	CQuarterHourCodeDlg (FoxjetDatabase::COdbcDatabase & db, const CodesDlg::CCodesArray & vCodes);
	~CQuarterHourCodeDlg();

// Dialog Data
	//{{AFX_DATA(CQuarterHourCodeDlg)
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CQuarterHourCodeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CodesDlg::CCodes GetCodes (ULONG lLineID) const;
	void Update (ULONG lLineID, UINT nIndex, const CString & strValue);

	ULONG GetCurLine() const;

	CodesDlg::CCodesArray m_vCodes;
	FoxjetDatabase::COdbcDatabase & m_db;
	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

	// Generated message map functions
	//{{AFX_MSG(CQuarterHourCodeDlg)
	afx_msg void OnEdit();
	afx_msg void OnSelchangeLine();
	afx_msg void OnDblclkType(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_QUARTERHOURCODEDLG_H__AA1EC135_D892_4C6F_B1B3_D6B149C8208F__INCLUDED_)
