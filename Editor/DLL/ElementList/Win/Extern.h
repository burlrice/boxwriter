#ifndef __ELEMENTLIST_EXTERN_H__
#define __ELEMENTLIST_EXTERN_H__

#include "OdbcDatabase.h"
#include "ElementDefaults.h"
#include "Head.h"

namespace ListGlobals
{
	namespace Keys
	{
		static const CString m_strBarcode			= _T ("WASP Barcode");
		static const CString m_strShiftcode			= _T ("Shift codes");
		static const CString m_strAi				= _T ("AI");
		static const CString m_strSub				= _T ("SubElement");
		static const CString m_strHours				= _T ("Hour codes");
		static const CString m_strMonths			= _T ("Month codes");
		static const CString m_strDayOfWeek			= _T ("Day of week codes");
		static const CString m_strRollover			= _T ("Rollover");
		static const CString m_strQuarterHours		= _T ("Quarter hour codes");

		namespace NEXT
		{
			static const CString m_strDataMatrix	= _T ("IP DataMatrix");
			static const CString m_strBarcode		= _T ("IP Barcode");
			static const CString m_strTime			= _T ("IP Time");
			static const CString m_strDays			= _T ("IP Days");
		}; 
	}; //namespace Keys

	extern ELEMENT_API FoxjetCommon::CElementDefaults defElements;

	ELEMENT_API FoxjetDatabase::COdbcDatabase & GetDB ();
	ELEMENT_API bool SetDB (FoxjetDatabase::COdbcDatabase & db);
	ELEMENT_API bool IsDBSet ();
	
}; //namespace ListGlobals

#endif //__ELEMENTLIST_EXTERN_H__
