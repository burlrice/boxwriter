// DbWizardTypesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DbWizardTypesDlg.h"
#include "DbWizardFieldDlg.h"
#include "Debug.h"
#include "Extern.h"
#include "DatabaseElement.h"
#include "Parse.h"

using namespace FoxjetDatabase;
using namespace FoxjetElements;
using namespace ItiLibrary;
using namespace FoxjetCommon;


// CDbWizardTypesDlg

IMPLEMENT_DYNAMIC(CDbWizardTypesDlg, CDbWizardBasePage)

CDbWizardTypesDlg::CDbWizardTypesDlg(CDbWizardDlg * pParent)
: CDbWizardBasePage (pParent, IDD_DATABASE_WIZARD_TYPES)
{

}

CDbWizardTypesDlg::~CDbWizardTypesDlg()
{
}


BEGIN_MESSAGE_MAP(CDbWizardTypesDlg, CDbWizardBasePage)
END_MESSAGE_MAP()


BOOL CDbWizardTypesDlg::OnInitDialog()
{
	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;

	CDbWizardBasePage::OnInitDialog();

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_NAME), 140));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_TYPE), 190));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_EXAMPLE_DATA), 180));

	m_lv.Create (LV_FIELDS, vCols, this, ListGlobals::defElements.m_strRegSection);
	m_lv->SetExtendedStyle (LVS_EX_CHECKBOXES | m_lv->GetExtendedStyle ());

	return TRUE;
}

// CDbWizardTypesDlg message handlers

BOOL CDbWizardTypesDlg::OnSetActive()
{
	CCopyArray <CFieldItem, CFieldItem &> sel = m_pdlgMain->m_pdlgField->GetCurSelFields ();

	// TODO: allow choice of database, barcode, or bitmap

	for (int i = 0; i < sel.GetSize (); i++) {
//		TRACEF (sel [i].m_strName + _T (", ") + sel [i].m_strType + _T (", ") + sel [i].m_strExample);
	}

	return TRUE;
}
