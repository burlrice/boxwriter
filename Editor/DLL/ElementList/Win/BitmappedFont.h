// BitmappedFont.h: interface for the CBitmappedFont class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BITMAPPEDFONT_H__4A185BAA_5D64_4174_B4E8_E20096617DCF__INCLUDED_)
#define AFX_BITMAPPEDFONT_H__4A185BAA_5D64_4174_B4E8_E20096617DCF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CBitmappedFont  
{
public:
	CBitmappedFont (const CString & strName, const CString & strFilePath);
	virtual ~CBitmappedFont();

	CString GetName () const { return m_strName; }
	CString GetFilePath () const { return m_strFilePath; }
	int GetHeight () const { return m_wDots; }
	int DrawChar (TCHAR c, PUCHAR lpBuffer = NULL) const;
	int GetWidthWords () const { return m_nWidthWords; }

protected:
	int GetOffset (TCHAR c) const;

	CString m_strName;
	CString m_strFilePath;

	PUCHAR m_pBuffer;
	ULONG m_lLen;
	ULONG * m_pIndex;
	USHORT * m_pData;

	USHORT m_wVersion;
	USHORT m_wDots;
	USHORT m_wStart;
	USHORT m_wEnd;

	int m_nWidthWords;
};

#endif // !defined(AFX_BITMAPPEDFONT_H__4A185BAA_5D64_4174_B4E8_E20096617DCF__INCLUDED_)
