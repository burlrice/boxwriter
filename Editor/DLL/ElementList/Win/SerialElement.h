// SerialElement.h: interface for the CSerialElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERIALELEMENT_H__FCBFA143_B7A4_4EB6_8E0B_05E5E760D6E7__INCLUDED_)
#define AFX_SERIALELEMENT_H__FCBFA143_B7A4_4EB6_8E0B_05E5E760D6E7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TextElement.h"

namespace FoxjetElements
{
	class ELEMENT_API CSerialElement : public TextElement::CTextElement  
	{
		DECLARE_DYNAMIC (CSerialElement);

	public:
		static enum { MAXBUFFLEN = 1024 };

		CSerialElement (const FoxjetDatabase::HEADSTRUCT & head);
		CSerialElement (const CSerialElement & rhs);
		CSerialElement & operator = (const CSerialElement & rhs);
		virtual ~CSerialElement();

		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);
		virtual int GetClassID () const { return SERIAL; }
		virtual bool SetDefaultData (const CString &strData);

		virtual int Build (BUILDTYPE type = INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		const TCHAR * GetBuffer () const;
		bool SetBuffer (const TCHAR * pBuffer);

		int GetIndex () const;
		bool SetIndex (int nIndex);

		int GetLength () const;
		bool SetLength (int nLength);

		CString GetDefault () const;
		bool SetDefault (const CString & str);

		int GetTableIndex () const;
		bool SetTableIndex (int nIndex);

	protected:
		const TCHAR	*	m_pBuffer;
		int				m_nIndex;
		int				m_nLen;
		int				m_nTableIndex;

		CString			m_strDefault;
	};
}; //namespace FoxjetElements

#endif // !defined(AFX_SERIALELEMENT_H__FCBFA143_B7A4_4EB6_8E0B_05E5E760D6E7__INCLUDED_)
