// ElementDlg.cpp : implementation file
//

#include "stdafx.h"

#include <math.h>

#include "ElementDlg.h"
#include "Resource.h"
#include "Coord.h"
#include "Color.h"
#include "Debug.h"
#include "TextElement.h"
#include "TemplExt.h"
#include "Utils.h"
#include "Extern.h"
#include "FontPickerDlg.h"
#include "Parse.h"

using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetElements;
using namespace FoxjetElements::TextElement;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define SS_PREVIEW 100
#define ID_COLOR	1000

void DDV_ID (CDataExchange * pDX, UINT nElementID, const CBaseElement & e, const CElementList & v, UINT nCtrlID)
{
	if (pDX->m_bSaveAndValidate) {
		int nFind = v.FindID (nElementID);

		if (e.GetID () != nElementID && nFind != -1) {
			CString str;
			str.Format (LoadString (IDS_IDEXISTS), nElementID);
			MsgBox (* pDX->m_pDlgWnd, str);
			pDX->PrepareEditCtrl (nCtrlID);
			pDX->Fail ();
		}
	}
}

static const COLORREF rgbMap [] = 
{
	RGB (0,		0,		0),
	RGB (128,	128,	128),
	RGB (255,	0,		0),
	RGB (128,	0,		0),
	RGB (255,	255,	0),
	RGB (128,	128,	0),
	RGB (0,		255,	0),
	RGB (0,		128,	0),
	RGB (0,		255,	255),
	RGB (0,		128,	128),
	RGB (0,		0,		255),
	RGB (0,		0,		128),
	RGB (255,	0,		255),
	RGB (128,	0,		128),		
};

/////////////////////////////////////////////////////////////////////////////
// CElementDlg dialog

CElementDlg::CElementDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
						 UINT nDlgID, const FoxjetCommon::CElementList * pList,
						 CWnd * pParent)
:	m_pt (0, 0), 
	m_bReadOnly (false), 
	m_bReadOnlyID (false), 
	m_bReadOnlyLocation (false), 
	m_pa (pa), 
	m_pElement (CopyElement (&e, NULL, false)), 
	m_nID (0),
	m_pList (pList),
	m_bFlipH (e.IsFlippedH ()),
	m_bFlipV (e.IsFlippedV ()),
	m_bInverse (e.IsInverse ()),
	m_bPreview (true),
	m_lLineID (-1),
	m_pAllLists (NULL),
	m_hAccel (NULL),
	m_bSetDefault (false),
	m_align (e.GetAlignment ()),
	m_bInkSaver (FALSE),
	m_rgb (Color::rgbBlack),
	FoxjetCommon::CEliteDlg (nDlgID, pParent)
{
	ASSERT (m_pElement);

	const HEADSTRUCT & h = GetHead ();
	PANELSTRUCT p;

	m_lLineID = e.GetLineID ();

	if (GetPanelRecord (GetDB (), h.m_lPanelID, p))
		m_lLineID = p.m_lLineID;

	if (CBaseTextElement * p = DYNAMIC_DOWNCAST (CBaseTextElement, m_pElement)) 
		m_strFont = p->GetFontName ();
}

CElementDlg::~CElementDlg ()
{
	delete m_pElement;
}

void CElementDlg::DoDataExchange(CDataExchange* pDX)
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();

	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);

	DDX_Coord (pDX, TXT_LOCATIONX, m_pt.x, m_units, head, true);
	DDX_Coord (pDX, TXT_LOCATIONY, m_pt.y, m_units, head, false);
	DDX_Text (pDX, TXT_ID, m_nID);

	if (GetDlgItem (CHK_FLIPH))		DDX_Check (pDX, CHK_FLIPH, m_bFlipH);
	if (GetDlgItem (CHK_FLIPV))		DDX_Check (pDX, CHK_FLIPV, m_bFlipV);
	if (GetDlgItem (CHK_INVERSE))	DDX_Check (pDX, CHK_INVERSE, m_bInverse);
	if (GetDlgItem (CHK_PREVIEW))	DDX_Check (pDX, CHK_PREVIEW, m_bPreview);
	if (GetDlgItem (CHK_INKSAVER))	DDX_Check (pDX, CHK_INKSAVER, m_bInkSaver);

	if (m_pList)
		DDV_ID (pDX, m_nID, GetElement (), * m_pList, TXT_ID);
}


BEGIN_MESSAGE_MAP(CElementDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CElementDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(CHK_PREVIEW, InvalidatePreview)
	ON_BN_CLICKED(CHK_FLIPH, InvalidatePreview)
	ON_BN_CLICKED(CHK_FLIPV, InvalidatePreview)
	ON_BN_CLICKED(CHK_INVERSE, InvalidatePreview)
	ON_CBN_SELCHANGE(CB_FONT, InvalidatePreview)
	ON_EN_CHANGE(TXT_TEXT, InvalidatePreview)
	ON_EN_CHANGE(TXT_BOLD, InvalidatePreview)
	ON_EN_CHANGE(TXT_WIDTH, InvalidatePreview)
	ON_EN_CHANGE(TXT_GAP, InvalidatePreview)
	ON_BN_CLICKED (BTN_FONT, OnFont)
	ON_BN_CLICKED (BTN_DEFAULT, OnDefault)
	ON_BN_CLICKED (BTN_ALIGNMENT, OnAlignment)
	ON_BN_CLICKED (BTN_PRINTABLE, OnPrintable)
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM ()
	ON_COMMAND_RANGE (ID_COLOR, ID_COLOR + ARRAYSIZE (::rgbMap), OnColor)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CElementDlg message handlers

BOOL CElementDlg::OnInitDialog() 
{
	{
		CRect rc = m_pElement->GetWindowRect (GetHead ());

		switch (m_pElement->GetAlignment ()) {
		case FoxjetCommon::ALIGNMENT_CENTER:
			m_pt.x = rc.left + (rc.Width () / 2);
			break;
		case FoxjetCommon::ALIGNMENT_RIGHT:
			m_pt.x = rc.left + rc.Width ();
			break;
		}

		VERIFY (m_bmpAlightLeft.LoadBitmap (IDB_ALIGNLEFT));
		VERIFY (m_bmpAlightRight.LoadBitmap (IDB_ALIGNRIGHT));
		VERIFY (m_bmpAlightCenter.LoadBitmap (IDB_ALIGNCENTER));
		VERIFY (m_bmpChecked.LoadBitmap (IDB_CHECKED));
	}

	FoxjetCommon::CEliteDlg::OnInitDialog();

	//FoxjetUtils::RegisterAutoButton (m_hWnd);

	if (m_pList == NULL)
		m_bReadOnlyLocation = true;

	ASSERT (GetDlgItem (IDOK));
	ASSERT (GetDlgItem (TXT_LOCATIONX));
	ASSERT (GetDlgItem (TXT_LOCATIONY));
	ASSERT (GetDlgItem (TXT_ID));

	if (m_bReadOnly)
		GetDlgItem (IDOK)->EnableWindow (false);

	if (m_bReadOnlyLocation) {
		GetDlgItem (TXT_LOCATIONX)->EnableWindow (false);
		GetDlgItem (TXT_LOCATIONY)->EnableWindow (false);
	}

	if (m_bReadOnlyID) 
		GetDlgItem (TXT_ID)->EnableWindow (false);

	if (CWnd * pPreview = GetDlgItem (IDC_PREVIEW)) {
		CRect rc;
		pPreview->GetWindowRect (&rc);

		ScreenToClient (&rc);

		ASSERT (m_pElement);
		ASSERT (m_pList);
		VERIFY (m_wndPreview.Create (this, rc, m_pElement, &m_pList->GetHead ()));
		InvalidatePreview ();
	}

	if (CWnd * p = GetDlgItem (TXT_ID)) 
		p->EnableWindow (FALSE);

	if (CTextElement * pText = DYNAMIC_DOWNCAST (CTextElement, &GetElement ())) {
		if (CWnd * p = GetDlgItem (LBL_WIDTH)) {
			UINT nID = IsBitmappedFont (pText->GetFont ().m_strName) ? IDS_GAP : IDS_AVGWIDTH;

			p->SetWindowText (LoadString (nID));
		}
	}

	{
		int nSize = GetCtrlFontSize (GetDefCtrlID ());
		CString strFace = _T ("Microsoft Sans Serif");

		if (CBaseTextElement * p = DYNAMIC_DOWNCAST (CBaseTextElement, m_pElement)) 
			strFace = p->GetFontName ();

		m_fntCtrl.CreateFont (nSize, 0, 0, 0, FW_NORMAL, FALSE, 0, 0,
			DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
			DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, strFace); 

		if (CWnd * p = GetDlgItem (TXT_TEXT))
			p->SetFont (&m_fntCtrl);
	}

	SetAlignmentBmp ();

	if (CWnd * p = GetDlgItem (BTN_PRINTABLE))
		p->ShowWindow (SW_HIDE);

	if (CWnd * p = GetDlgItem (GetDefCtrlID ())) {
		GotoDlgCtrl (p);
		return FALSE;
	}

	return TRUE;
}

CSize CElementDlg::GetElementSize ()
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();

	ASSERT (m_pElement); 
	BuildElement ();
	m_pElement->Draw (* GetDC (), head, true);
	CSize size = m_pElement->GetWindowRect (head).Size ();

	if (CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, m_pElement)) {
		if (p->GetOrientation () == TextElement::VERTICAL) {
			long lMaxHeight = CBaseElement::LogicalToThousandths (CPoint (0, head.Height ()), head).y;
			size.cy = BindTo (size.cy, 0L, lMaxHeight);
		}
	}

	return size;
}

void CElementDlg::OnOK()
{
	if (!UpdateData ())
		return;

	if (!(m_bReadOnly || m_bReadOnlyLocation) && IsConfiningOn ()) {
		if (BuildElement () == UNKNOWN)
			return;
	}

	const HEADSTRUCT & head = GetHead ();
	CSize size = CBaseElement::ThousandthsToLogical (GetElement ().GetWindowRect (head).Size (), head);
	int nHeight = head.Height ();

	if (size.cx > m_pa.cx) {
		MsgBox (* this, LoadString (IDS_ELEMENTTOOLONG));
		return;
	}
	else if (size.cy > nHeight) {
		MsgBox (* this, LoadString (IDS_ELEMENTTOOTALL));
		return;
	}

	ASSERT (!m_bReadOnly);
	
	if (!GetValues ())
		return;

	FoxjetCommon::CEliteDlg::OnOK ();
}

bool CElementDlg::IsPreviewEnabled() const
{
	if (CButton * p = (CButton *)GetDlgItem (CHK_PREVIEW))
		return p->GetCheck () == 1;

	return false;
}

void CElementDlg::InvalidatePreview()
{
	TRACEF (_T ("InvalidatePreview ()"));

	if (m_wndPreview.m_hWnd) {// && m_wndPreview.IsWindowVisible ()) {
		BuildElement ();
		m_wndPreview.Invalidate ();
		m_wndPreview.RedrawWindow ();
	}
}

FoxjetCommon::CBaseElement & CElementDlg::GetElement ()
{
	ASSERT (m_pElement);
	return * m_pElement;
}

const FoxjetCommon::CBaseElement & CElementDlg::GetElement () const
{
	ASSERT (m_pElement);
	return * m_pElement;
}

bool CElementDlg::GetCheck(UINT nID) const
{
	return GetCheck (this, nID);
}

bool CElementDlg::GetCheck(const CWnd * pWnd, UINT nID)
{
	bool bResult = false;

	if (CButton * p = (CButton *)pWnd->GetDlgItem (nID))
		bResult = p->GetCheck () == 1;

	return bResult;
}

const FoxjetDatabase::HEADSTRUCT & CElementDlg::GetHead() const
{
	if (m_pList)
		return m_pList->GetHead ();
	else {
		TRACEF (_T ("Warning: m_pList == NULL"));
		return FoxjetDatabase::GetDefaultHead ();
	}
}

ULONG CElementDlg::GetLineID () const
{
	return m_lLineID;
}

void CElementDlg::SetDlgItemText (int nID, LPCTSTR lpszString)
{
	CEdit * p = (CEdit *)GetDlgItem (nID);

	ASSERT (p);
	CWnd::SetDlgItemText (nID, lpszString);
	p->SetModify (TRUE);
}

void CElementDlg::SetDlgItemInt (int nID, UINT nValue, BOOL bSigned)
{
	CEdit * p = (CEdit *)GetDlgItem (nID);

	ASSERT (p);
	CWnd::SetDlgItemInt (nID, nValue, bSigned);
	p->SetModify (TRUE);
}

CString CElementDlg::GetCtrlText (UINT nID) const
{
	CString str;
	GetDlgItemText (nID, str);
	return str;
}

int CElementDlg::GetCtrlFontSize (UINT nID)
{
	int cy = 11;

	if (CEdit * p = (CEdit *)GetDlgItem (nID)) {
		LOGFONT lf;

		if (CFont * pFont = p->GetFont ()) {
			pFont->GetObject (sizeof (lf), &lf);
			cy = lf.lfHeight;
		}
		else if (CFont * pFont = GetFont ()) {
			pFont->GetObject (sizeof (lf), &lf);
			cy = lf.lfHeight;
		}
	}

	return cy;
}

BOOL CElementDlg::PreTranslateMessage(MSG* pMsg) 
{
	if (!m_hAccel)
		VERIFY (m_hAccel = ::LoadAccelerators (GetInstanceHandle (), MAKEINTRESOURCE (IDA_ELEMENT_DIALOG)));

	if (::TranslateAccelerator (m_hWnd, m_hAccel, pMsg))
		return TRUE;
	
	return FoxjetCommon::CEliteDlg::PreTranslateMessage(pMsg);
}

void CElementDlg::OnFont ()
{
	if (m_pElement) {
		switch (m_pElement->GetClassID ()) {
		case TEXT:
		case DATETIME:
		case EXPDATE:
		case USER:
		//case BARCODE:
		case SHIFTCODE:
		case DATABASE:
		case SERIAL:
			{
				CFontPickerDlg dlg (m_pList, this);

				if (CWnd * p = GetDlgItem (GetDefCtrlID ()))
					GetDlgItemText (GetDefCtrlID (), dlg.m_strSample);

				if (CBaseTextElement * p = DYNAMIC_DOWNCAST (CBaseTextElement, m_pElement)) 
					dlg.m_strFont = p->GetFontName ();

				switch (m_pElement->GetClassID ()) {
				case DATETIME:
				case EXPDATE:
				//case BARCODE:
				case SHIFTCODE:
				case SERIAL:
					BuildElement ();
					dlg.m_strSample = m_pElement->GetImageData ();
					break;
				case DATABASE:
					dlg.m_strSample = m_pElement->GetDefaultData ();
					break;
				}

				if (dlg.DoModal () == IDOK) {
					m_strFont = dlg.m_strFont;

					{
						CFont fnt;
						int nSize = GetCtrlFontSize (GetDefCtrlID ());

						fnt.CreateFont (nSize, 0, 0, 0, FW_NORMAL, FALSE, 0, 0,
							DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
							DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, m_strFont); 
						::DeleteObject (m_fntCtrl.Detach ());
						m_fntCtrl.Attach (fnt.Detach ());

						if (CWnd * p = GetDlgItem (TXT_TEXT))
							p->SetFont (&m_fntCtrl);
					}

					switch (m_pElement->GetClassID ()) {
					case TEXT:
					case USER:
						if (CWnd * p = GetDlgItem (TXT_TEXT))
							SetDlgItemText (TXT_TEXT, dlg.m_strSample);

						break;
					}
				}
			}
			break;
		}
	}
}

void CElementDlg::OnDefault ()
{
	if (!UpdateData (TRUE))
		return;

	if (GetValues ()) {
		if (MsgBox (* this, LoadString (IDS_SETDEFAULT), LoadString (IDS_CONFIRM), MB_YESNO | MB_ICONINFORMATION) == IDYES) 
			m_bSetDefault = true;
	}
}

void CElementDlg::SetAlignmentBmp ()
{
	if (CButton * p = (CButton *)GetDlgItem (BTN_ALIGNMENT)) {
		CBitmap * pbmp [] =
		{
			&m_bmpAlightLeft,
			&m_bmpAlightCenter,
			&m_bmpAlightRight,
		};

		p->SetBitmap (* pbmp [m_align]);
	}
}


void CElementDlg::OnAlignment ()
{
	int n = (FoxjetCommon::ALIGNMENT_RIGHT - FoxjetCommon::ALIGNMENT_LEFT) + 1;
	m_align = (FoxjetCommon::ALIGNMENT)(((int)m_align + 1) % n);
	SetAlignmentBmp ();
}

void CElementDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	using namespace Color;
	using namespace FoxjetCommon;

	DRAWITEMSTRUCT dis;
	
	memcpy (&dis, lpDrawItemStruct, sizeof (dis));

	if (dis.CtlType == ODT_MENU) {
		bool bSelected = (dis.itemState & (ODS_SELECTED | ODS_FOCUS)) ? false : true;
		CRect rc = dis.rcItem;
		CDC dc;
		MEASUREITEMSTRUCT m;
		COLORREF rgb = rgbBlack;
		int nIndex = dis.itemID - ID_COLOR;

		dc.Attach (dis.hDC);
		memset (&m, 0, sizeof (m));

		if (nIndex >= 0 && nIndex < ARRAYSIZE (::rgbMap))
			rgb = ::rgbMap [nIndex];

		int nBkMode = dc.SetBkMode (TRANSPARENT);
		CFont * pFont = dc.SelectObject (GetFont ());

		dc.FillRect (rc, &CBrush (::GetSysColor (COLOR_BTNFACE)));

		if (rgb == rgbBlack) 
			dc.DrawText (LoadString (IDS_PRINTABLE), rc, DT_CENTER | DT_SINGLELINE | DT_VCENTER);
		else 
			dc.FillRect (rc, &CBrush (rgb));

		if (rgb == m_rgb) {
			CDC dcMem;

			dcMem.CreateCompatibleDC (&dc);
			CBitmap * pBitmap = dcMem.SelectObject (&m_bmpChecked);
			DIBSECTION ds = { 0 };

			m_bmpChecked.GetObject (sizeof (ds), &ds);
			int x = rc.left + 2;
			int y = rc.top + (rc.Height () - ds.dsBm.bmHeight) / 2;
			dc.BitBlt (x, y, ds.dsBm.bmWidth, ds.dsBm.bmHeight, &dcMem, 0, 0, SRCCOPY);
				
			dcMem.SelectObject (pBitmap);
		}

		dc.DrawEdge (rc, EDGE_SUNKEN, BF_LEFT | BF_RIGHT | BF_TOP | BF_BOTTOM | BF_ADJUST);

		dc.SetBkMode (nBkMode);
		dc.SelectObject (pFont);
		dc.Detach ();
	}
	else if (dis.CtlType == ODT_BUTTON) {
		if (dis.CtlID == BTN_PRINTABLE) {
			CDC dc;
			CRect rc = dis.rcItem;

			dc.Attach (dis.hDC);
			CFont * pFont = dc.SelectObject (GetFont ());
			
			if (dis.itemState & ODS_SELECTED) 
				dc.DrawEdge (rc, EDGE_SUNKEN, BF_RECT);
			else
				dc.DrawEdge (rc, EDGE_RAISED, BF_RECT);

			rc.DeflateRect (2, 2);
			dc.FillRect (rc, &CBrush (::GetSysColor (COLOR_BTNFACE)));

			CString str = m_rgb == rgbBlack ? LoadString (IDS_PRINTABLE) : LoadString (IDS_NOTATION);
			dc.DrawText (str, rc, DT_CENTER | DT_SINGLELINE | DT_VCENTER);

			rc.left = rc.right - rc.Height ();
			rc.DeflateRect (1, 1);
			dc.FillRect (rc, &CBrush (m_rgb));

			if (dis.itemState & ODS_FOCUS) {
				CRect rc = dis.rcItem;

				rc.DeflateRect (3, 3);
				dc.DrawFocusRect (rc);
			}

			dc.SelectObject (pFont);

			dc.Detach ();
		}
	}
	//else 
	//	FoxjetCommon::CEliteDlg::OnDrawItem (nIDCtl, lpDrawItemStruct);
}


void CElementDlg::OnMeasureItem (int nCtrl, LPMEASUREITEMSTRUCT lpmis)
{
	if (lpmis->CtlType == ODT_MENU) {
		lpmis->itemWidth = 200;
		lpmis->itemHeight = 50;
	}
}

void CElementDlg::OnPrintable ()
{
	if (CButton * p = (CButton *)GetDlgItem (BTN_PRINTABLE)) {
		CMenu menu;
		CRect rc;

		menu.CreatePopupMenu ();

		for (int i = 0; i < ARRAYSIZE (::rgbMap); i++) {
			MENUITEMINFO info;

			memset (&info, 0, sizeof (info));

			info.cbSize		= sizeof (info);
			info.fMask		= MIIM_TYPE | MIIM_ID | MIIM_DATA | MIIM_SUBMENU;
			info.fType		= MFT_OWNERDRAW;
			info.wID		= ID_COLOR + i;
	
			menu.InsertMenuItem (i, &info);
		}

		p->GetWindowRect (rc);

		menu.TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON, rc.left, rc.bottom, this);
	}
}

void CElementDlg::OnColor (UINT nCmdID)
{
	int nIndex = nCmdID - ID_COLOR;

	if (nIndex >= 0 && nIndex < ARRAYSIZE (::rgbMap)) {
		m_rgb = ::rgbMap [nIndex];
		REPAINT (GetDlgItem (BTN_PRINTABLE));
	}
}

void CElementDlg::Get (const FoxjetCommon::CBaseElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	m_rgb = e.GetColor ();
	
	if (e.HasDither ())
		m_bInkSaver = e.GetDither () > 0;
}

void CElementDlg::Set (FoxjetCommon::CBaseElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	e.SetColor (m_rgb);

	if (e.HasDither ())
		e.SetDither (m_bInkSaver ? 50 : 0);
}
