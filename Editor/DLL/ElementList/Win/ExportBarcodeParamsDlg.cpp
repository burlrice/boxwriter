// ExportBarcodeParamsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "ExportBarcodeParamsDlg.h"
#include "Registry.h"
#include "Debug.h"
#include "Extern.h"
#include "Database.h"
#include "Wasp\bcxl.h"
#include "BarcodeElement.h"
#include "FileExt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace ItiLibrary::ListCtrlImp;
using namespace FoxjetCommon;
using namespace ListGlobals;
using namespace FoxjetElements;

const CString strAll = LoadString (IDS_ALL);
const HEADTYPE type = GRAPHICS_768_256;

CExportBarcodeParamsDlg::CParamItem::CParamItem ()
:	m_bSet (false),
	m_nSymbology (-1)
{
}

CString CExportBarcodeParamsDlg::CParamItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0: return m_strName == ::strAll ? m_strName : m_strName + (m_bSet ? _T ("") : _T (" (") + LoadString (IDS_NOTSET) + _T (")"));
	case 1: return m_line.m_strName;
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CExportBarcodeParamsDlg dialog


CExportBarcodeParamsDlg::CExportBarcodeParamsDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(CExportBarcodeParamsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CExportBarcodeParamsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CExportBarcodeParamsDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExportBarcodeParamsDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CExportBarcodeParamsDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CExportBarcodeParamsDlg)
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_NOTIFY(LVN_ITEMCHANGED, LV_PARAMS, OnItemchangedParams)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExportBarcodeParamsDlg message handlers

void CExportBarcodeParamsDlg::OnBrowse() 
{
	CString str;

	GetDlgItemText (TXT_FILENAME, str);

	CFileDialog dlg (TRUE, _T ("*.txt"), str, OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("Text files (*.txt)|*.txt|All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) 
		SetDlgItemText (TXT_FILENAME, dlg.GetPathName ());
}

BOOL CExportBarcodeParamsDlg::OnInitDialog() 
{
	CString strFile = FoxjetDatabase::GetProfileString (defElements.m_strRegSection + _T ("\\Settings\\CExportBarcodeParamsDlg"), _T ("Filename"), 
		GetHomeDir () + _T ("\\BarcodeParams.txt"));
	CArray <CColumn, CColumn> vCols;
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	COdbcDatabase & db = GetDB ();

	vCols.Add (CColumn (LoadString (IDS_NAME), 380));
	//vCols.Add (CColumn (LoadString (IDS_LINE), 80));

	m_lv.Create (LV_PARAMS, vCols, this, defElements.m_strRegSection);
	GetPrinterLines (db, GetPrinterID (db), vLines);

	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	SetDlgItemText (TXT_FILENAME, strFile);

	m_lv->SetExtendedStyle (LVS_EX_CHECKBOXES | m_lv->GetExtendedStyle ());

	{
		bool bCheckAll = true;
		const int nResolution = 150;

		for (int nSymbology = kSymbologyFirst; nSymbology <= kSymbologyLast; nSymbology++) {
			if (CBarcodeElement::IsValidSymbology (nSymbology)) {
				CString strKey = GetSettingsKey (::type, nResolution, nSymbology, db);
				SETTINGSSTRUCT s;

				for (int nLine = 0; nLine < 1 /* vLines.GetSize () */; nLine++) {
					LINESTRUCT & line = vLines [nLine];
					CParamItem * p = new CParamItem;
					SETTINGSSTRUCT s;

					p->m_strName = GetSymbology (nSymbology, db);
					p->m_line = line;
					p->m_bSet = GetSettingsRecord (db, line.m_lID, GetSettingsKey (::type, nResolution, nSymbology, db), s);

					p->m_nSymbology = nSymbology;
					TRACEF (p->m_strName + _T (": ") + p->m_line.m_strName);

					m_lv.InsertCtrlData (p);
					int nIndex = m_lv.GetDataIndex (p);

					m_lv->SetCheck (nIndex, p->m_bSet);

					if (!p->m_bSet)
						bCheckAll = false;
				}
			}
		}
		
		m_lv->SortItems (CListCtrlImp::CompareFunc, 0);
		//m_lv->SortItems (CListCtrlImp::CompareFunc, 1);

		{
			CParamItem * p = new CParamItem;

			p->m_strName = ::strAll;

			m_lv.InsertCtrlData (p, 0);

			if (bCheckAll)
				m_lv->SetCheck (0, true);
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CExportBarcodeParamsDlg::OnOK()
{
	CString str, strFile;
	COdbcDatabase & db = GetDB ();

	GetDlgItemText (TXT_FILENAME, strFile);
	FoxjetDatabase::WriteProfileString (defElements.m_strRegSection + _T ("\\Settings\\CExportBarcodeParamsDlg"), _T ("Filename"), strFile);

	if (FoxjetFile::FileExists (strFile)) {
		CString str;

		str.Format (LoadString (IDS_OVERWRITEFILE), strFile);

		if (MsgBox (* this, str, LoadString (IDS_WARNING), MB_ICONQUESTION | MB_YESNO) == IDNO) 
			return;
	}

	if (FILE * fp = _tfopen (strFile, _T ("w"))) {
		for (int i = 0; i < m_lv->GetItemCount (); i++) {
			if (CParamItem * pItem = (CParamItem *)m_lv.GetCtrlData (i)) {
				if (pItem->m_strName != ::strAll) {
					if (m_lv->GetCheck (i)) {
						CArray <CBarcodeParams, CBarcodeParams &> v;
						std::vector <int> vRes = GetResolutions ();

						for (int i = 0; i < vRes.size (); i++)
							FoxjetElements::GetBarcodeParams (pItem->m_line.m_lID, pItem->m_nSymbology, ::type, vRes [i], v, db);
			
						for (int nParam = 0; nParam < v.GetSize (); nParam++)
							str += v [nParam].ToString (CVersion ());

						str += _T ("\n");
					}
				}
			}
		}

		fwrite (w2a (str), str.GetLength (), 1, fp);
		fclose (fp);
		MsgBox (* this, LoadString (IDS_BARCODEPARAMS_EXPORTED) + _T ("\n") + strFile);
		FoxjetCommon::CEliteDlg::OnOK ();
	}
}

void CExportBarcodeParamsDlg::OnItemchangedParams(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if (NM_LISTVIEW * p = (NM_LISTVIEW*)pNMHDR) {
		if (p->uChanged & LVIF_STATE) {
			if (CParamItem * pItem = (CParamItem *)m_lv.GetCtrlData (p->iItem)) {
				if (pItem->m_strName == ::strAll) {
					bool bCheckAll = false;

					switch(p->uNewState & LVIS_STATEIMAGEMASK) {
					case INDEXTOSTATEIMAGEMASK(BST_CHECKED + 1):
						bCheckAll = true;
						break;
					case INDEXTOSTATEIMAGEMASK(BST_UNCHECKED + 1):
						bCheckAll = false;
						break; 
					}

					for (int i = 0; i < m_lv->GetItemCount (); i++) {
						if (CParamItem * pItem = (CParamItem *)m_lv.GetCtrlData (i)) {
							if (pItem->m_strName != ::strAll) {
								m_lv->SetCheck (i, bCheckAll);
							}
						}
					}
				}
			}
		}
	}
	
	if (pResult)
		* pResult = 0;
}
