// PrinterFont.cpp: implementation of the CPrinterFont class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PrinterFont.h"
#include "Debug.h"

using namespace FoxjetCommon;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPrinterFont::CPrinterFont(const CString & strName, UINT nSize,
						   bool bBold, bool bItalic)
:	m_strName (strName), m_nSize (nSize), 
	m_bBold (bBold), m_bItalic (bItalic)
{
}

CPrinterFont::CPrinterFont(const LOGFONT & lf, int nSize)
:	m_strName (lf.lfFaceName), m_nSize (nSize), 
	m_bBold (lf.lfWeight == FW_BOLD), m_bItalic (lf.lfItalic ? true : false)
{
}

CPrinterFont::CPrinterFont (const CPrinterFont & rhs)
:	m_strName (rhs.m_strName), m_nSize (rhs.m_nSize), 
	m_bBold (rhs.m_bBold), m_bItalic (rhs.m_bItalic)
{
}

CPrinterFont & CPrinterFont::operator = (const CPrinterFont & rhs)
{
	if (this != &rhs) {
		m_strName = rhs.m_strName;
		m_nSize = rhs.m_nSize;
		m_bBold = rhs.m_bBold;
		m_bItalic = rhs.m_bItalic;
	}

	return * this;
}

CPrinterFont::~CPrinterFont()
{
}

/* TODO: rem: these were used to remove whitespace (row-wise) from std windows fonts
int CPrinterFont::GetLeading (HFONT hFont)
{
	int nLeading = 0;

	if (hFont) {
		TEXTMETRIC tm;
		HDC hDC = ::GetDC (NULL);
		HGDIOBJ hTmp = ::SelectObject (hDC, hFont);

		::ZeroMemory (&tm, sizeof (tm));
		::GetTextMetrics (hDC, &tm);
		::SelectObject (hDC, hTmp);
		::ReleaseDC (NULL, hDC);

		nLeading = max (tm.tmExternalLeading, 0) + max (tm.tmInternalLeading, 0);
	}

	return nLeading;
}

int CPrinterFont::GetHeight (HFONT hFont)
{
	int nHeight = 0;

	if (hFont) {
		TEXTMETRIC tm;
		HDC hDC = ::GetDC (NULL);
		HGDIOBJ hTmp = ::SelectObject (hDC, hFont);

		::ZeroMemory (&tm, sizeof (tm));
		::GetTextMetrics (hDC, &tm);
		::SelectObject (hDC, hTmp);
		::ReleaseDC (NULL, hDC);

		nHeight = tm.tmHeight - GetLeading (hFont);
	}

	return nHeight;
}
*/