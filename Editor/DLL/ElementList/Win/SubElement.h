// SubElement.h: interface for the CSubElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SUBELEMENT_H__DFB7DE8D_A4B8_429C_9567_9CAD862F0C29__INCLUDED_)
#define AFX_SUBELEMENT_H__DFB7DE8D_A4B8_429C_9567_9CAD862F0C29__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Types.h"
#include "TextElement.h"
#include "ElementApi.h"

namespace FoxjetElements
{
	class ELEMENT_API CSubElement : public CObject  
	{
		DECLARE_DYNAMIC(CSubElement)

	public:
		CSubElement (int nType = TEXT, const CString & strID = _T ("DEF"), 
			const CString & strMask = _T (""), 
			const CString & strData = _T ("0123456789"), 
			const CString & strDesc = _T (""));
		CSubElement (const CSubElement & rhs);
		CSubElement & operator = (const CSubElement & rhs);
		virtual ~CSubElement();

		typedef enum { 
			ALPHA = 0, 
			NUMERIC, 
			ALPHANUMERIC, 
			PUNCTUATION,	// TODO: haven't fully tested this type yet
			ANY,			// TODO: haven't fully tested this type yet
			DELIMETER 
		} CHARTYPE;


		static const TCHAR m_cAlphaReq;
		static const TCHAR m_cAlphaOpt;
		static const TCHAR m_cNumericReq;
		static const TCHAR m_cNumericOpt;
		static const TCHAR m_cAlphaNumericReq;
		static const TCHAR m_cAlphaNumericOpt;
		static const TCHAR m_cPunctReq;
		static const TCHAR m_cPunctOpt;
		static const TCHAR m_cAnyReq;
		static const TCHAR m_cAnyOpt;
		static const TCHAR m_cDelimeter;
		static const TCHAR m_cDef;

		static int GetSubtypeCount ();
		static UINT GetSubtypeResID (int nIndex);
		static int GetSubtypeClassID (int nIndex);
		static CString GetSubtype (int nIndex);

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		int GetDataMaskLength () const;
		int GetType () const { return m_pObj ? m_pObj->GetClassID () : -1; }
		bool SetType (int type);
		bool SetData (const CString & str);
		CString GetData () const;
		bool IsAi () const;
		bool IsValidData () const;
		bool IsValidData (const CString & strData) const;

		const TextElement::CTextElement * GetElement () const;
		TextElement::CTextElement * GetElement ();
		bool SetElement (const TextElement::CTextElement & e);

		CString GetDataMaskDisp () const;
		static CString GetDataMaskDisp (const CString & str);

		void Reset ();

		bool IsVariableLen () const;
		static bool IsVariableLen (const CString & strAI);

		static bool IsAlphaNumeric (TCHAR c);
		static bool IsNumeric (TCHAR c);
		static bool IsAlpha (TCHAR c);
		static bool IsPunct (TCHAR c);
		static bool IsAny (TCHAR c);
		static bool IsAi (const CString & str);
		static void ParseMask (const CString & str, CStringArray & v);
		static bool IsValidData (const CString & strData, const CString & strMask, ULONG & lCalls);
		static int GetMaskCharType (TCHAR c);
		static int GetMaskChar (int nType, bool bRequired);
		static bool IsOptChar (TCHAR c);
		static bool IsReqChar (TCHAR c);
		static int GetOptCharCount (const CString & str);
		static int GetReqCharCount (const CString & str);

		CString m_strDataMask;
		CString m_strDesc;
		CString m_strID;

		static CString GetPrefix (int nType);
		static int GetType (const CString & strPrefix);
		CString ToString(const FoxjetCommon::CVersion & ver) const;
		bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);


		#ifdef _DEBUG
		static void RunIsValidDataTest (LPCTSTR pszFile, int nLen, 
			const TCHAR cMask [], const TCHAR cData []);
		static void GenerateMask (CString strMask, const TCHAR cMask [], 
			const TCHAR cData [], int nMaxLen, LPCTSTR pszFile);
		static void GenerateData (const CString & strData, const CString & strMask, 
			const TCHAR cData [], LPCTSTR pszFile);
		#endif //_DEBUG
			
		// std // #if __CUSTOM__ == __SW0840__
		ULONG GetLineID () const { return m_lLineID; }
		void SetLineID (ULONG lLineID);
		// std // #endif

	protected:
		static int CountDataChars (const CString & str, int nType);
		static int CountMaskChars (const CString & str, int nType, bool bRequired);
		void InitObject (int type);
		static bool IsValidData (const CString & strData, const CString & strMask, int & nCalls, int nTab = 0);
		static bool IsValidData (const CString & strData, int);
		static int GetMaskEntryVector (TCHAR cLookAhead, const CString & strMask, CDWordArray & v);
		static int GetDataCharType (TCHAR c);
		static bool MaskContainsReqChars (const CString & strMask);
		static bool MaskContainsOptChars (const CString & strMask);
		static bool IsValidMask (const CString & str);

		TextElement::CTextElement * m_pObj;

		// std // #if __CUSTOM__ == __SW0840__
		ULONG m_lLineID;
		// std // #endif
	};
}; //namespace FoxjetElements

#endif // !defined(AFX_SUBELEMENT_H__DFB7DE8D_A4B8_429C_9567_9CAD862F0C29__INCLUDED_)
