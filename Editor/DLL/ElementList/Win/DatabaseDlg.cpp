// DatabaseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "list.h"
#include "DatabaseDlg.h"
#include "OdbcRecordset.h"
#include "OdbcTable.h"
#include "Utils.h"
#include "DbFieldDlg.h"
#include "Debug.h"
#include "Extern.h"
#include "Parse.h"
#include "Coord.h"
#include "DbFormatDlg.h"
#include "Registry.h"
#include "TableTypeDlg.h"
#include "DbSetupDlg.h"
#include "DatabaseStartDlg.h"
#include "DatabaseElement.h"
#include "ResID.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ListGlobals;
using namespace FoxjetElements;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetUtils;

/////////////////////////////////////////////////////////////////////////////
// CDatabaseDlg dialog


CDatabaseDlg::CDatabaseDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_nSQLType (0),
	m_nFormat (0),
	m_bMaxWidth (false),
	m_lMaxWidth (5000),
	CBaseTextDlg (e, pa, IDD_DATABASE, pList, pParent)
{
	//{{AFX_DATA_INIT(CDatabaseDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CDatabaseDlg::~CDatabaseDlg ()
{
	COdbcDatabase::FreeTables (m_vTables);
}

void CDatabaseDlg::DoDataExchange(CDataExchange* pDX)
{
	const HEADSTRUCT & head = GetHead ();
	CCoord coordMin (0, 0, m_units, head);
	CCoord coordMax (0, 0, m_units, head);

	const CSize pa1000s = CBaseElement::LogicalToThousandths (CSize (GetPA ().cx, head.Height ()), head); 

	coordMin.m_x = TextElement::CTextElement::m_lmtParagraphWidth.m_dwMin;
	coordMin.m_y = TextElement::CTextElement::m_lmtParagraphWidth.m_dwMin;

	coordMax.m_x = pa1000s.cx;
	coordMax.m_y = pa1000s.cy;

	CBaseTextDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDatabaseDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate)
		UpdateKeyField ();

	DDX_Text (pDX, TXT_ROW, m_nRow);

	DDX_Text (pDX, TXT_KEYVALUE, m_strKeyValue);
	DDV_MinMaxChars (pDX, m_strKeyValue, TXT_KEYVALUE, CDatabaseElement::m_lmtKey);

	DDX_Text (pDX, TXT_SQL, m_strSQL);

	DDX_Check (pDX, CHK_PROMPT, m_bPrompt);
	DDX_Check (pDX, CHK_SERIAL, m_bSerial);
	DDX_Check (pDX, CHK_SERIALDOWNLOAD, m_bSerialDownload);

	DDX_Check (pDX, CHK_PARA, m_bParaEnable);
	DDX_Coord (pDX, TXT_PARAWIDTH, m_lParaWidth, m_units, head, true);
	if (m_bParaEnable)
		DDV_Coord (pDX, TXT_PARAWIDTH, m_lParaWidth, 0, m_units, head, DDVCOORDXBOTH, coordMin, coordMax);
	DDX_Radio (pDX, RDO_ALIGNLEFT, (int &)m_alignPara);
	DDX_Text(pDX, TXT_PARAGAP, m_nParaGap);
	if (m_bParaEnable)
		DDV_MinMaxInt (pDX, m_nParaGap, TextElement::CTextElement::m_lmtParagraphGap.m_dwMin, TextElement::CTextElement::m_lmtParagraphGap.m_dwMax);

	DDX_Text(pDX, TXT_DEFAULTDATA, m_strEmpty);
	DDV_MinMaxChars (pDX, m_strEmpty, TXT_DEFAULTDATA, 0, TextElement::CTextElement::m_lmtString.m_dwMax);

	DDX_Check (pDX, CHK_MAXWIDTH, m_bMaxWidth);
	DDX_Coord (pDX, TXT_MAXWIDTH, m_lMaxWidth, m_units, head, true);
	if (m_bMaxWidth)
		DDV_Coord (pDX, TXT_MAXWIDTH, m_lMaxWidth, 0, m_units, head, DDVCOORDXBOTH, coordMin, coordMax);
}


BEGIN_MESSAGE_MAP(CDatabaseDlg, CBaseTextDlg)
	//{{AFX_MSG_MAP(CDatabaseDlg)
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_CBN_SELCHANGE(CB_TABLE, OnSelchangeTable)
	ON_CBN_SELCHANGE(CB_FIELD, OnSelchangeField)
	ON_BN_CLICKED(BTN_SELECT, OnSelect)
	ON_BN_CLICKED(BTN_SELECTKEY, OnSelectKey)
	ON_BN_CLICKED(CHK_KEY, OnKey)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB, OnSelchangeTab)
	ON_BN_CLICKED(CHK_PARA, OnParaEnable)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(BTN_FORMAT, OnFormat)
	ON_CBN_SELCHANGE(CB_KEYFIELD, UpdateKeyField)
	ON_CBN_SELCHANGE(CB_DSN, OnSelchangeDSN)
	ON_BN_CLICKED(BTN_OBJECTS, OnObjects)
	ON_BN_CLICKED(BTN_SETUP, OnSetup)
	ON_BN_CLICKED(BTN_DBSTART, OnDbStartSet)
	ON_BN_CLICKED(BTN_DBCONFIG, OnDbStartConfig)
	ON_BN_CLICKED(CHK_MAXWIDTH, OnMaxWidth)
END_MESSAGE_MAP()

int CDatabaseDlg::BuildElement ()
{
	CDatabaseElement & e = GetElement ();
	CString strDSN, strTable, strField, strKeyField, strKeyValue, strSQL, strEmpty;
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	CComboBox * pKeyField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB);

	ASSERT (pTable);
	ASSERT (pField);
	ASSERT (pKeyField);
	ASSERT (pTab);

	UpdateData ();

	strDSN = GetCurSelDsn (); 
	
	if (pTable->GetLBTextLen (pTable->GetCurSel ()) > 0)
		pTable->GetLBText (pTable->GetCurSel (), strTable);
	
	if (pField->GetLBTextLen (pField->GetCurSel ()) > 0)
		pField->GetLBText (pField->GetCurSel (), strField);

	GetDlgItemText (TXT_SQL, strSQL);
	GetDlgItemText (TXT_DEFAULTDATA, strEmpty);

	if (GetCheck (CHK_KEY)) {
		GetDlgItemText (TXT_KEYVALUE, strKeyValue);

		if (pKeyField->GetLBTextLen (pKeyField->GetCurSel ()) > 0)
			pKeyField->GetLBText (pKeyField->GetCurSel (), strKeyField);
	}

	e.SetDSN (strDSN);
	e.SetTable (strTable);
	e.SetField (strField);
	e.SetKeyField (strKeyField);
	e.SetKeyValue (strKeyValue);
	e.SetRow (GetDlgItemInt (TXT_ROW));
	e.SetSQL (strSQL.GetLength () ? true : false, strSQL); 
	e.SetOrientation (GetOrientation ());
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));
	e.SetEmptyString (strEmpty);

	UpdateKeyField ();
	e.SetSQLType (m_nSQLType);

	e.SetMaxWidthEnabled (GetCheck (CHK_MAXWIDTH));
	e.SetMaxWidth (m_lMaxWidth);

	CString strError;

	try {
		e.Validate ();
	}
	catch (CDBException * e)		{ strError = HANDLEEXCEPTION_TRACEONLY (e); }
	catch (CMemoryException * e)	{ strError = HANDLEEXCEPTION_TRACEONLY (e); }

	if (strError.GetLength ()) {
		strError = e.CreateSQL () + _T ("\n\n") + strError;
		MsgBox (* this, strError, LoadString (IDS_ERROR), MB_ICONERROR);
		return UNKNOWN;
	}

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

const FoxjetElements::CDatabaseElement & CDatabaseDlg::GetElement () const
{
	const CBaseElement & e = CBaseTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CDatabaseElement)));
	return (CDatabaseElement &)e;
}

FoxjetElements::CDatabaseElement & CDatabaseDlg::GetElement ()
{
	CBaseElement & e = CBaseTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CDatabaseElement)));
	return (CDatabaseElement &)e;
}

BOOL CDatabaseDlg::OnInitDialog() 
{
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB);

	ASSERT (pTab);
	BOOL bResult = CBaseTextDlg::OnInitDialog();

	pTab->InsertItem (0, LoadString (IDS_BUILD));
	pTab->InsertItem (1, LoadString (IDS_GENERAL));
	pTab->InsertItem (2, LoadString (IDS_PARAGRAPHMODE));
	pTab->InsertItem (3, LoadString (IDS_DEFAULT));
	pTab->InsertItem (4, LoadString (IDS_ORIENATION));

	{
		COdbcCache db = COdbcDatabase::Find (m_strDSN);

		db.GetDB ().CopyTables (m_vTables);
	}

	InitTables (m_strDSN, m_strTable, m_strField, m_strKeyField, m_strBmpWidthField, m_strBmpHeightField);

	if (CComboBox * pDSN = (CComboBox *)GetDlgItem (CB_DSN)) {
		CStringArray v;

		COdbcDatabase::GetDsnEntries (v);

		for (int i = 0; i < v.GetSize (); i++) 
			if (pDSN->FindString (-1, v [i]) == CB_ERR)
				pDSN->AddString (v [i]);

		int nIndex = pDSN->FindString (-1, m_strDSN);

		if (nIndex == CB_ERR)
			nIndex = pDSN->AddString (m_strDSN);

		pDSN->SetCurSel (nIndex);
	}

	if (m_strKeyField.GetLength () || m_strKeyValue.GetLength ()) {
		CButton * p = (CButton *)GetDlgItem (CHK_KEY);

		ASSERT (p);
		p->SetCheck (1);
	}

	if (m_bSQL)
		pTab->SetCurSel (1);

	OnKey ();
	OnSelchangeTab (NULL, NULL);

	OnParaEnable ();
	OnSelchangeField ();
	OnMaxWidth ();

	{
		CDatabaseStartDlg dlg (this);

		dlg.Load (GetDB ());

		bool bConfigured = dlg.m_strDSN.GetLength () > 0 ? true : false;

		if (CWnd * p = GetDlgItem (BTN_DBSTART))
			p->EnableWindow (bConfigured);
	}

	return bResult;
}

void CDatabaseDlg::OnBrowse() 
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	COdbcDatabase db (_T (__FILE__), __LINE__);

	ASSERT (pTable);

	try {
		if (db.Open (NULL)) {
			CString strDSN = Extract (db.GetConnect (), _T ("DSN="), _T (";"));

			int nTables = db.GetTableCount ();

			COdbcDatabase::FreeTables (m_vTables);
			db.CopyTables (m_vTables);
			db.Close ();

			if (!nTables) {
				CString str;

				str.Format (LoadString (IDS_DSNHASNOTABLES), strDSN);
				MsgBox (* this, str);
			}
			else {
				if (CComboBox * pDSN = (CComboBox *)GetDlgItem (CB_DSN)) {
					int nIndex = pDSN->FindString (-1, strDSN);

					if (nIndex == CB_ERR)
						nIndex = pDSN->AddString (strDSN);

					pDSN->SetCurSel (nIndex);
				}

				CString strTable = GetCurSelTable ();

				if (m_vTables.GetSize ()) {
					if (COdbcTable * p = (COdbcTable *)m_vTables [0]) 
						strTable = p->GetName ();
				}

				InitTables (GetCurSelDsn (), strTable, GetCurSelField (), GetCurSelKeyField (), GetCurSelBmpWidth (), GetCurSelBmpHeight ());
				GotoDlgCtrl (GetDlgItem (CB_TABLE));
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
}

void CDatabaseDlg::InitTables (const CString & strDSN, const CString & strTable, const CString & strField, 
							   const CString & strKeyField, const CString & strBmpWidth, const CString & strBmpHeight) 
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);

	BeginWaitCursor ();

	ASSERT (pTable);
	pTable->ResetContent ();

	if (strDSN.GetLength ()) {

		try {
			CString strTableTmp (strTable);
			COdbcCache db = COdbcDatabase::Find (strDSN);
			CString strType = CTableTypeDlg::GetParams (); 

			for (int i = 0; i < m_vTables.GetSize (); i++) {
				if (COdbcTable * p = (COdbcTable *)m_vTables [i]) {
					CString strFind = _T ("'") + p->GetType () + _T ("'");

					if (strType.Find (strFind) != -1) {
						CString strName = p->GetName ();
						int nIndex = pTable->AddString (strName);

						if (!strName.CompareNoCase (strTable)) 
							pTable->SetCurSel (nIndex);
					}
				}
			}

			if (pTable->GetCurSel () == CB_ERR) {
				//pTable->SetCurSel (0);
				//pTable->GetLBText (0, strTableTmp);
				pTable->SetCurSel (pTable->AddString (strTableTmp));
			}

			InitFields (strDSN, strTableTmp, strField, strKeyField, strBmpWidth, strBmpHeight);
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	EndWaitCursor ();
}

void CDatabaseDlg::InitFields (const CString & strDSN, const CString & strTable, const CString & strField,
							   const CString & strKeyField, const CString & strBmpWidth, const CString & strBmpHeight) 
{
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	CComboBox * pKeyField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CComboBox * pBmpWidth = (CComboBox *)GetDlgItem (CB_BMPWIDTH);
	CComboBox * pBmpHeight = (CComboBox *)GetDlgItem (CB_BMPHEIGHT);
	CStringArray vBmp;

	ASSERT (pField);
	ASSERT (pKeyField);
	ASSERT (pBmpWidth);
	ASSERT (pBmpHeight);

	BeginWaitCursor ();

	pField->ResetContent ();
	pKeyField->ResetContent ();
	pBmpWidth->ResetContent ();
	pBmpHeight->ResetContent ();

	if (strDSN.GetLength ()) {

		try {
			COdbcCache db = COdbcDatabase::Find (strDSN);
			COdbcRecordset rst (db.GetDB ());
			CString strSQL;

			strSQL.Format (_T ("SELECT * FROM [%s]"), strTable);

			/*
			if (db.GetDB ().IsProgress ()) {
				try
				{
					COdbcRecordset rst (db.GetDB ());
					//ODBC;DSN=Progress;HOST=localhost;PORT=7777;DB=sports;UID=PJH
					strSQL.Format (_T ("SELECT * FROM PUB.Customer"), strTable);
					TRACEF (db.GetDB ().GetConnect ());
					TRACEF (strSQL);
					rst.Open (strSQL); //, CRecordset::forwardOnly);
					TRACEF ("success");
				}
				catch (CDBException * e)		
				{ 
					HANDLEEXCEPTION_TRACEONLY (e); 
				}

				int nBreak = 0;
			}
			*/

			if (rst.Open (strSQL, CRecordset::forwardOnly)) {
				for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
					CODBCFieldInfo info;

					rst.GetODBCFieldInfo (i, info);
					CString strName = info.m_strName;
					int nIndex = pField->AddString (strName);

					InsertAscending (vBmp, strName);

					pField->SetItemData (nIndex, info.m_nSQLType);

					//if (IsSqlString (info.m_nSQLType))
					//	TRACEF ("IsSqlString");

					if (!strName.CompareNoCase (strField))
						pField->SetCurSel (nIndex);

					nIndex = pKeyField->AddString (strName);
					pKeyField->SetItemData (nIndex, info.m_nSQLType);

					if (!strName.CompareNoCase (strKeyField))
						pKeyField->SetCurSel (nIndex);
				}

				rst.Close ();

				if (pField->GetCurSel () == CB_ERR) {
					pField->SetCurSel (0);
					SetDlgItemInt (TXT_ROW, 1);
				}

				if (pKeyField->GetCurSel () == CB_ERR) 
					pKeyField->SetCurSel (0);

				OnSelchangeField ();
				UpdateKeyField ();
			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	vBmp.InsertAt (0, CDatabaseElement::GetStatic ());
	vBmp.InsertAt (0, CDatabaseElement::GetNative ());
	vBmp.InsertAt (0, CDatabaseElement::GetAuto ());

	for (int i = 0; i < vBmp.GetSize (); i++) {
		int nIndex = pBmpWidth->AddString (vBmp [i]);

		if (!m_strBmpWidthField.CompareNoCase (vBmp [i]))
			pBmpWidth->SetCurSel (nIndex);
	}

	for (int i = 0; i < vBmp.GetSize (); i++) {
		int nIndex = pBmpHeight->AddString (vBmp [i]);

		if (!m_strBmpHeightField.CompareNoCase (vBmp [i]))
			pBmpHeight->SetCurSel (nIndex);
	}

	if (pBmpHeight->GetCurSel () == CB_ERR)
		pBmpHeight->SetCurSel (0);

	if (pBmpWidth->GetCurSel () == CB_ERR)
		pBmpWidth->SetCurSel (0);

	EndWaitCursor ();
}

void CDatabaseDlg::OnSelchangeTable() 
{
	CString strDSN, strTable;	
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);

	ASSERT (pTable);
	strDSN = GetCurSelDsn ();
	
	if (pTable->GetLBTextLen (pTable->GetCurSel ()) > 0)
		pTable->GetLBText (pTable->GetCurSel (), strTable);

	InitFields (strDSN, strTable);
	SetDlgItemInt (TXT_ROW, 1);
}

void CDatabaseDlg::OnSelchangeField() 
{
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	bool bEnable = false;

	ASSERT (pField);

	switch (pField->GetItemData (pField->GetCurSel ())) {
		case SQL_SMALLINT:
		case SQL_INTEGER:
		case SQL_REAL:	
		case SQL_FLOAT:	
		case SQL_DOUBLE:	
		case SQL_DATE:	
		case SQL_TIME:	
		case SQL_TIMESTAMP:
		bEnable = true;
	}

	if (CWnd * p = GetDlgItem (BTN_FORMAT))
		p->EnableWindow (bEnable);
}

bool CDatabaseDlg::GetValues ()
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	CComboBox * pKeyField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB);
	CComboBox * pDSN = (CComboBox *)GetDlgItem (CB_DSN);

	ASSERT (pTable);
	ASSERT (pField);
	ASSERT (pKeyField);
	ASSERT (pTab);
	ASSERT (pDSN);

	int nIndex = pDSN->GetCurSel ();

	if (nIndex == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NODSNSELECTED));
		return false;
	}

	pDSN->GetLBText (nIndex, m_strDSN);

	nIndex = pTable->GetCurSel ();

	if (nIndex == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOTABLESELECTED));
		return false;
	}

	pTable->GetLBText (nIndex, m_strTable);

	nIndex = pField->GetCurSel ();

	if (nIndex == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOFIELDSELECTED));
		return false;
	}

	pField->GetLBText (nIndex, m_strField);
	m_strBmpWidthField = GetCurSelBmpWidth ();
	m_strBmpHeightField = GetCurSelBmpHeight ();

	if (GetCheck (CHK_KEY)) {
		nIndex = pKeyField->GetCurSel ();

		if (nIndex != CB_ERR) 
			pKeyField->GetLBText (nIndex, m_strKeyField);
	}
	else {
		m_strKeyField = _T ("");		
		SetDlgItemText (TXT_KEYVALUE, m_strKeyValue = _T (""));
	}

	{
		CString strSQL;

		GetDlgItemText (TXT_SQL, strSQL);
		m_bSQL = strSQL.GetLength () ? true : false;
	}

	UpdateKeyField ();

	if (CWnd * p = GetDlgItem (BTN_FORMAT))
		if (!p->IsWindowEnabled ())
			m_nFormat = CDbFormatDlg::DEFAULT;

	return CElementDlg::GetValues ();
}

void CDatabaseDlg::OnSelect() 
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	CInternalFormatting opt (false);
	CDbFieldDlg dlg (defElements.m_strRegSection, this);

	ASSERT (pTable);
	ASSERT (pField);

	dlg.m_strDSN = GetCurSelDsn ();

	if (pTable->GetLBTextLen (pTable->GetCurSel ()) > 0)
		pTable->GetLBText (pTable->GetCurSel (), dlg.m_strTable);
	
	if (pField->GetLBTextLen (pField->GetCurSel ()) > 0)
		pField->GetLBText (pField->GetCurSel (), dlg.m_strField);

	dlg.m_nRow = 0; //GetDlgItemInt (TXT_ROW);
	dlg.m_nMaxRows = 100;
	
	if (dlg.DoModal () == IDOK) {
		pField->SetCurSel (pField->FindStringExact (-1, dlg.m_strField));
		OnSelchangeField ();
		SetDlgItemInt (TXT_ROW, 0); //dlg.m_nRow);
	}
}

void CDatabaseDlg::OnSelectKey() 
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CInternalFormatting opt (false);
	CDbFieldDlg dlg (defElements.m_strRegSection, this);

	ASSERT (pTable);
	ASSERT (pField);

	dlg.m_strDSN = GetCurSelDsn ();

	if (pTable->GetLBTextLen (pTable->GetCurSel ()) > 0)
		pTable->GetLBText (pTable->GetCurSel (), dlg.m_strTable);
	
	if (pField->GetLBTextLen (pField->GetCurSel ()) > 0)
		pField->GetLBText (pField->GetCurSel (), dlg.m_strField);

	dlg.m_nRow = 0; //GetDlgItemInt (TXT_ROW);
	dlg.m_nMaxRows = 100;
	
	if (dlg.DoModal () == IDOK) {
		pField->SetCurSel (pField->FindStringExact (-1, dlg.m_strField));
		OnSelchangeField ();
		SetDlgItemInt (TXT_ROW, 0); //dlg.m_nRow);
		SetDlgItemText (TXT_KEYVALUE, dlg.m_strSelected);
		UpdateKeyField ();
	}
}

void CDatabaseDlg::OnKey() 
{
	bool bUse = GetCheck (CHK_KEY);

	GetDlgItem (CB_KEYFIELD)->EnableWindow (bUse);	
	GetDlgItem (TXT_KEYVALUE)->EnableWindow (bUse);	
	GetDlgItem (BTN_SELECTKEY)->EnableWindow (bUse);	
	GetDlgItem (CHK_PROMPT)->EnableWindow (bUse);	
	GetDlgItem (CHK_SERIAL)->EnableWindow (bUse);	

	if (CButton * p = (CButton *)GetDlgItem (CHK_PROMPT)) {
		p->EnableWindow (bUse);	
		p->SetCheck (bUse ? (m_bPrompt ? 1 : 0): 0);
	}

	if (CButton * p = (CButton *)GetDlgItem (CHK_SERIAL)) {
		p->EnableWindow (bUse);	
		p->SetCheck (bUse ? (m_bSerial ? 1 : 0): 0);
	}

	if (CButton * p = (CButton *)GetDlgItem (CHK_SERIALDOWNLOAD)) {
		p->EnableWindow (bUse);	
		p->SetCheck (bUse ? (m_bSerialDownload ? 1 : 0): 0);
	}
}

void CDatabaseDlg::OnSelchangeTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB);
	struct
	{
		CWnd *	m_pCtrl;
		int		m_nTab;
	} map [] = 
	{
		{ GetDlgItem (CB_TABLE),			0 },
//		{ GetDlgItem (LBL_TABLE),			0 },
		{ GetDlgItem (CB_FIELD),			0 },
		{ GetDlgItem (LBL_FIELD),			0 },
//		{ GetDlgItem (TXT_ROW),				0 },
//		{ GetDlgItem (LBL_ROW),				0 },
		{ GetDlgItem (BTN_OBJECTS),			0 },
		{ GetDlgItem (BTN_SELECT),			0 },
		{ GetDlgItem (BTN_FORMAT),			0 },
		{ GetDlgItem (GRP_KEY),				0 },
		{ GetDlgItem (CB_KEYFIELD),			0 },
		{ GetDlgItem (LBL_KEYFIELD),		0 },
		{ GetDlgItem (TXT_KEYVALUE),		0 },
		{ GetDlgItem (LBL_KEYVALUE),		0 },
		{ GetDlgItem (BTN_SELECTKEY),		0 },
		{ GetDlgItem (LBL_BMPWIDTH),		0 },
		{ GetDlgItem (LBL_BMPHEIGHT),		0 },
		{ GetDlgItem (CB_BMPWIDTH),			0 },
		{ GetDlgItem (CB_BMPHEIGHT),		0 },
		{ GetDlgItem (CHK_KEY),				0 },
		{ GetDlgItem (CHK_PROMPT),			0 },
		{ GetDlgItem (CHK_SERIAL),			0 },
		{ GetDlgItem (CHK_SERIALDOWNLOAD),	0 },
		{ GetDlgItem (GRP_DBSTART),			0 },
		{ GetDlgItem (BTN_DBSTART),			0 },
		{ GetDlgItem (BTN_DBCONFIG),		0 },

		{ GetDlgItem (TXT_SQL),				1 },
		{ GetDlgItem (LBL_SQL),				1 },

		{ GetDlgItem (GRP_ALIGN),			2 },
		{ GetDlgItem (TXT_PARAWIDTH),		2 },
		{ GetDlgItem (LBL_PARAWIDTH),		2 },
		{ GetDlgItem (TXT_PARAGAP),			2 },
		{ GetDlgItem (LBL_PARAGAP),			2 },
		{ GetDlgItem (RDO_ALIGNLEFT),		2 },
		{ GetDlgItem (RDO_ALIGNCENTER),		2 },
		{ GetDlgItem (RDO_ALIGNRIGHT),		2 },
		{ GetDlgItem (CHK_PARA),			2 },

		{ GetDlgItem (TXT_DEFAULTDATA),		3 },
		{ GetDlgItem (LBL_DEFAULTDATA),		3 },

		{ GetDlgItem (GRP_ORIENTATION),		4 },
		{ GetDlgItem (RDO_NORMAL),			4 },
		{ GetDlgItem (RDO_VERTICAL),		4 },
		{ GetDlgItem (BMP_TEXT),			4 },
		{ GetDlgItem (TXT_MAXWIDTH),		4 },
		{ GetDlgItem (GRP_MAXWIDTH),		4 },
		{ GetDlgItem (CHK_MAXWIDTH),		4 },
	};
	ASSERT (pTab);

	int nTab = pTab->GetCurSel ();

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		if (map [i].m_pCtrl)
			map [i].m_pCtrl->ShowWindow (map [i].m_nTab == nTab ? SW_SHOW : SW_HIDE);
	}

	{
		UINT n [] = { CHK_MAXWIDTH, GRP_MAXWIDTH, TXT_MAXWIDTH };

		for (int i = 0; i < ARRAYSIZE (n); i++)
			if (CWnd * p = (CWnd *)GetDlgItem (n [i])) 
				p->ShowWindow (SW_HIDE);
	}

	{
		bool bShow = !m_pAllLists && !GetList ();
		UINT n [] = { LBL_BMPWIDTH, LBL_BMPHEIGHT, CB_BMPWIDTH, CB_BMPHEIGHT };		

		for (int i = 0; i < ARRAYSIZE (n); i++) {
			if (CWnd * p = (CWnd *)GetDlgItem (n [i])) {

				for (int n = 0; n < ARRAYSIZE (map); n++) {
					if (p == map [n].m_pCtrl) {
						if (map [n].m_nTab == nTab) 
							p->ShowWindow (bShow ? SW_SHOW : SW_HIDE);
						else
							p->ShowWindow (SW_HIDE);
						
						break;
					}
				}
			}
		}
	}

	if (pResult)
		* pResult = 0;
}

void CDatabaseDlg::UpdateKeyField() 
{
	CComboBox * pKeyField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CString strKeyField, strKeyValue;

	ASSERT (pKeyField);

	GetDlgItemText (TXT_KEYVALUE, strKeyValue);

//	if (!strKeyValue.GetLength ()) 
//		return;

	int nIndex = pKeyField->GetCurSel ();
	
	if (pKeyField->GetLBTextLen (nIndex) > 0)
		pKeyField->GetLBText (nIndex, strKeyField);

	m_nSQLType = pKeyField->GetItemData (nIndex);
}

void CDatabaseDlg::OnParaEnable() 
{
	UINT n [] = 
	{
		{ TXT_PARAWIDTH		},
		{ TXT_PARAGAP		},
		{ RDO_ALIGNLEFT		},
		{ RDO_ALIGNCENTER	},
		{ RDO_ALIGNRIGHT	},
	};
	CButton * p = (CButton *)GetDlgItem (CHK_PARA);

	ASSERT (p);

	BOOL bEnable = p->GetCheck () == 1;

	for (int i = 0; i < ARRAYSIZE (n); i++)
		if (CWnd * p = GetDlgItem (n [i]))
			p->EnableWindow (bEnable);
}


void CDatabaseDlg::OnFormat()
{
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	bool bEnable = false;
	CDbFormatDlg dlg (GetLineID (), this);

	ASSERT (pField);

/*
	switch (pField->GetItemData (pField->GetCurSel ())) {
		case SQL_SMALLINT:
		case SQL_INTEGER:
			dlg.m_nType = CDbFormatDlg::INT;
			break;
		case SQL_REAL:	
		case SQL_FLOAT:	
		case SQL_DOUBLE:	
			dlg.m_nType = CDbFormatDlg::FLOAT;
			break;
		case SQL_DATE:	
		case SQL_TIME:	
		case SQL_TIMESTAMP:
			dlg.m_nType = CDbFormatDlg::DATE_TIME;
			break;
		default:
			dlg.m_nType = CDbFormatDlg::DEFAULT;
			break;
	}
*/
	dlg.m_nType		= m_nFormat;
	dlg.m_strFormat = m_strFormat;

	if (dlg.DoModal () == IDOK) {
		m_nFormat	= dlg.m_nType;
		m_strFormat = dlg.m_strFormat;
	}
}

CString CDatabaseDlg::GetCurSelDsn () const
{
	CString strResult;

	if (CComboBox * pDSN = (CComboBox *)GetDlgItem (CB_DSN)) 
		if (pDSN->GetLBTextLen (pDSN->GetCurSel ()) > 0)
			pDSN->GetLBText (pDSN->GetCurSel (), strResult);

	return strResult;
}

CString CDatabaseDlg::GetCurSelTable ()
{
	CString strResult;

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_TABLE)) 
		if (p->GetLBTextLen (p->GetCurSel ()) > 0)
			p->GetLBText (p->GetCurSel (), strResult);

	return strResult;
}

CString CDatabaseDlg::GetCurSelField ()
{
	CString strResult;

	if (CComboBox * p= (CComboBox *)GetDlgItem (CB_FIELD)) 
		if (p->GetLBTextLen (p->GetCurSel ()) > 0)
			p->GetLBText (p->GetCurSel (), strResult);

	return strResult;
}

CString CDatabaseDlg::GetCurSelKeyField ()
{
	CString strResult;

	if (CComboBox * p= (CComboBox *)GetDlgItem (CB_KEYFIELD)) 
		if (p->GetLBTextLen (p->GetCurSel ()) > 0)
			p->GetLBText (p->GetCurSel (), strResult);

	return strResult;
}

CString CDatabaseDlg::GetCurSelBmpWidth ()
{
	CString strResult;

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_BMPWIDTH)) 
		if (p->GetLBTextLen (p->GetCurSel ()) > 0)
			p->GetLBText (p->GetCurSel (), strResult);

	return strResult;
}

CString CDatabaseDlg::GetCurSelBmpHeight ()
{
	CString strResult;

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_BMPHEIGHT)) 
		if (p->GetLBTextLen (p->GetCurSel ()) > 0)
			p->GetLBText (p->GetCurSel (), strResult);

	return strResult;
}


void CDatabaseDlg::OnSelchangeDSN ()
{
	COdbcCache db = COdbcDatabase::Find (GetCurSelDsn ());
	CString strTable = m_strTable;

	BeginWaitCursor ();
	COdbcDatabase::FreeTables (m_vTables);
	db.GetDB ().CopyTables (m_vTables);
	EndWaitCursor ();

	if (m_vTables.GetSize ())
		if (COdbcTable * p = (COdbcTable *)m_vTables [0]) 
			strTable = p->GetName ();

	for (int i = 0; i < m_vTables.GetSize (); i++) {
		if (COdbcTable * p = (COdbcTable *)m_vTables [i]) {
			if (!m_strTable.CompareNoCase (p->GetName ())) {
				strTable = p->GetName ();
				break;
			}
		}
	}

	InitTables (GetCurSelDsn (), strTable, GetCurSelField (), GetCurSelKeyField (), GetCurSelBmpWidth (), GetCurSelBmpHeight ());
}

void CDatabaseDlg::OnObjects ()
{
	CTableTypeDlg dlg (this);

	if (dlg.DoModal () == IDOK) 
		InitTables (GetCurSelDsn (), GetCurSelTable (), GetCurSelField (), GetCurSelKeyField (), GetCurSelBmpWidth (), GetCurSelBmpHeight ());
}

void CDatabaseDlg::OnSetup ()
{
	CDbSetupDlg dlg (this);

	dlg.m_strDSN = GetCurSelDsn ();
	dlg.m_strFile = COdbcDatabase::GetFilePath (dlg.m_strDSN);

	if (dlg.DoModal () == IDOK) {
		m_strDSN = dlg.m_strDSN;

		if (CComboBox * pDSN = (CComboBox *)GetDlgItem (CB_DSN)) {
			int nIndex = pDSN->FindString (-1, m_strDSN);

			if (nIndex == CB_ERR)
				nIndex = pDSN->AddString (m_strDSN);

			pDSN->SetCurSel (nIndex);
		}

		OnSelchangeDSN ();
		GotoDlgCtrl (GetDlgItem (CB_TABLE));
	}
}

void CDatabaseDlg::OnDbStartSet ()
{
	CDatabaseStartDlg dlg (this);

	dlg.Load (GetDB ());
	
	m_strDSN		= dlg.m_strDSN;
	m_strTable		= dlg.m_strTable;
	m_strKeyField	= dlg.m_strKeyField;

	if (CComboBox * pDSN = (CComboBox *)GetDlgItem (CB_DSN)) 
		pDSN->SelectString (-1, m_strDSN);

	InitTables (m_strDSN, m_strTable, GetCurSelField (), m_strKeyField, m_strBmpWidthField, m_strBmpHeightField);

	if (CButton * p = (CButton *)GetDlgItem (CHK_KEY)) {
		if (p->GetCheck () != 1) {
			p->SetCheck (1);
			OnKey ();
		}
	}
}

void CDatabaseDlg::OnDbStartConfig ()
{
	CDatabaseStartDlg dlg (this);
	CString strUser = GetUsername ();

	dlg.Load (GetDB ());
	dlg.m_bReadOnly = !LoginPermits (GetDB (), strUser, Control::nConfigureDatabaseID);
	TRACEF (strUser + _T (": ") + CString (dlg.m_bReadOnly ? _T (" [READ ONLY]") : _T ("")));

	if (dlg.DoModal () == IDOK) {
		bool bConfigured = dlg.m_strDSN.GetLength () > 0 ? true : false;

		dlg.Save (GetDB ());

		if (CWnd * p = GetDlgItem (BTN_DBSTART))
			p->EnableWindow (bConfigured);
	}
}

void CDatabaseDlg::Get (const FoxjetElements::CDatabaseElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Get (e, pHead);

	m_pt				= e.GetPos (pHead);
	m_nID				= e.GetID ();
	m_strDSN			= e.GetDSN ();
	m_strTable			= e.GetTable ();
	m_strField			= e.GetField ();
	m_strKeyField		= e.GetKeyField ();
	m_strKeyValue		= e.GetKeyValue ();
	m_nRow				= e.GetRow ();
	m_bFlipH			= e.IsFlippedH ();
	m_bFlipV			= e.IsFlippedV ();
	m_bInverse			= e.IsInverse ();
	m_strSQL			= e.GetSQL ();
	m_bSQL				= m_strSQL.GetLength () > 0;
	m_nOrientation		= e.GetOrientation ();
	m_nWidth			= e.GetWidth ();
	m_bPrompt			= e.GetPromptAtTaskStart ();
	m_nSQLType			= e.GetSQLType (); 
	m_bSerial			= e.GetSerial ();
	m_bSerialDownload	= e.GetSerialDownload ();
	m_bParaEnable		= e.IsResizable ();
	m_alignPara			= e.GetParaAlign ();
	m_lParaWidth		= e.GetParaWidth ();
	m_nParaGap			= e.GetParaGap ();
	m_nFormat			= e.GetFormatType ();
	m_strFormat			= e.GetFormat ();
	m_strEmpty			= e.GetEmptyString ();
	m_bMaxWidth			= e.IsMaxWidthEnabled ();
	m_lMaxWidth			= e.GetMaxWidth ();
	m_strBmpWidthField 	= e.GetBmpWidthField ();
	m_strBmpHeightField	= e.GetBmpHeightField ();
}

void CDatabaseDlg::Set (FoxjetElements::CDatabaseElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Set (e, pHead);

	e.SetDSN (m_strDSN);
	e.SetTable (m_strTable);
	e.SetField (m_strField);
	e.SetKeyField (m_strKeyField);
	e.SetKeyValue (m_strKeyValue);
	e.SetRow (m_nRow);
	e.SetFlippedH (m_bFlipH ? true : false);
	e.SetFlippedV (m_bFlipV ? true : false);
	e.SetInverse (m_bInverse ? true : false);
	e.SetSQL (m_bSQL, m_strSQL);
	e.SetOrientation (m_nOrientation);
	e.SetWidth (m_nWidth);
	e.SetPromptAtTaskStart (m_bPrompt ? true : false);
	e.SetSQLType (m_nSQLType); 
	e.SetSerial (m_bSerial ? true : false);
	e.SetSerialDownload (m_bSerialDownload ? true : false);
	e.SetResizable (m_bParaEnable ? true : false);
	e.SetParaAlign (m_alignPara);
	e.SetParaWidth (m_lParaWidth);
	e.SetParaGap (m_nParaGap);
	e.SetFormatType (m_nFormat);
	e.SetFormat (m_strFormat);
	e.SetEmptyString (m_strEmpty);
	e.SetMaxWidthEnabled (m_bMaxWidth ? true : false);
	e.SetMaxWidth (m_lMaxWidth);
	e.SetBmpWidthField  (m_strBmpWidthField);
	e.SetBmpHeightField (m_strBmpHeightField);

	e.SetID (m_nID);
	e.SetAlignment (m_align);
	e.SetPos (m_pt = e.GetPosInternal (m_pt), pHead, ALIGNMENT_ADJUST_SUBTRACT);
	e.SetFontName (m_strFont);
}

void CDatabaseDlg::OnMaxWidth ()
{
	if (CButton * pChk = (CButton *)GetDlgItem (CHK_MAXWIDTH)) 
		if (CWnd * pTxt = GetDlgItem (TXT_MAXWIDTH))
			pTxt->EnableWindow (pChk->GetCheck () == 1);
}

