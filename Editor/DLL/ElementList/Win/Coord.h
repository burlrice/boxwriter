// Coord.h: interface for the CCoord class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COORD_H__48883827_3AB7_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_COORD_H__48883827_3AB7_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ElementApi.h"
#include "List.h"
#include "Database.h"


namespace FoxjetCommon {
	namespace DB = FoxjetDatabase;

	class ELEMENT_API CCoord  
	{
	public:
		CCoord ();
		CCoord (const CCoord & rhs);
		CCoord (double x, double y, UNITS units, const DB::HEADSTRUCT & head, double dZoom = 1.0);
		CCoord (const CPoint & pt, UNITS units, const DB::HEADSTRUCT & head, double dZoom = 1.0);
		CCoord & operator = (const CCoord & rhs);
		virtual ~CCoord();

		CString Format () const;
		CString FormatX () const;
		CString FormatY () const;

		UNITS GetUnits () const { return m_units; }

		static CPoint CoordToPoint (const CCoord & pt, UNITS units, const DB::HEADSTRUCT & head);
		static CPoint CoordToPoint (const CPoint & pt, UNITS units, const DB::HEADSTRUCT & head);
		static CPoint CoordToPoint (double x, double y, UNITS units, const DB::HEADSTRUCT & head);

		double m_x;
		double m_y;

		static double GetPixelsPerXUnit (UNITS units, const DB::HEADSTRUCT & head);
		static double GetPixelsPerYUnit (UNITS unit, const DB::HEADSTRUCT & heads);

		static const CCoord m_coordMin; //CCoord (0, 0)
		static const CCoord m_coordMax; //CCoord (INT_MAX, INT_MAX)

	protected:
		void Translate (double dZoom, const DB::HEADSTRUCT & head);
		
		UNITS		m_units;

	private:
		CCoord (double x, double y);
	};

	ELEMENT_API CRect operator * (const CRect & rc, double dFactor);
	ELEMENT_API CRect operator * (double dFactor, const CRect & rc);
	ELEMENT_API CRect operator / (const CRect & rc, double dFactor);

	ELEMENT_API CPoint operator * (const CPoint & pt, double dFactor);
	ELEMENT_API CPoint operator * (double dFactor, const CPoint & pt);
	ELEMENT_API CPoint operator / (const CPoint & pt, double dFactor);

	typedef enum 
	{ 
		DDVCOORDXMIN		= 0x01, 
		DDVCOORDXMAX		= 0x02, 
		DDVCOORDXBOTH		= DDVCOORDXMAX | DDVCOORDXMIN, 
		DDVCOORDYMIN		= 0x04, 
		DDVCOORDYMAX		= 0x08, 
		DDVCOORDYBOTH		= DDVCOORDYMIN | DDVCOORDYMAX, 
		DDVCOORDBOTH		= DDVCOORDXBOTH | DDVCOORDYBOTH,
	} DDVCOORDTYPE;


	ELEMENT_API void DDX_Coord (CDataExchange * pDX, int nIDC, long & value, 
		UNITS units, const FoxjetDatabase::HEADSTRUCT & head, bool bXComponent);
	ELEMENT_API void DDV_Coord (CDataExchange * pDX, int nIDC, ULONG x, ULONG y, UNITS units,
		const FoxjetDatabase::HEADSTRUCT & head,
		DWORD dwType = DDVCOORDBOTH, const CCoord & coordMin = CCoord::m_coordMin, 
		const CCoord & coordMax = CCoord::m_coordMax);
}; // namespace FoxjetCommon

#endif // !defined(AFX_COORD_H__48883827_3AB7_11D4_8FC6_006067662794__INCLUDED_)
