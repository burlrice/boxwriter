#if !defined(AFX_FONTPICKERDLG_H__93C7B704_5A73_4A89_A913_4AED8FAB0C5F__INCLUDED_)
#define AFX_FONTPICKERDLG_H__93C7B704_5A73_4A89_A913_4AED8FAB0C5F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FontPickerDlg.h : header file
//

#include "ListCtrlImp.h"
#include "List.h"
#include "Utils.h"

using namespace FoxjetCommon;

/////////////////////////////////////////////////////////////////////////////
// CFontPickerDlg dialog

class CFontPickerDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CFontPickerDlg(const CElementList * pList, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CFontPickerDlg)
	CString	m_strSample;
	//}}AFX_DATA

	CString m_strFont;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFontPickerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	class CFontItem : public ItiLibrary::ListCtrlImp::CItem 
	{
	public:
		CFontItem (const CString & strFont = _T (""), int nTotal = 0, int nMatched = 0);
		virtual ~CFontItem ();

		virtual CString GetDispText (int nColumn) const;
		virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & cmp, int nColumn) const;

		double CalcMatch () const;

		CString m_strFont;
		int m_nTotal;
		int m_nMatched;
	};

	static ULONG CALLBACK Match (LPVOID lpData);

	CFont m_fnt;
	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
	const CElementList * m_pList;
	HANDLE m_hThread;

	// Generated message map functions
	//{{AFX_MSG(CFontPickerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnMatch();
	afx_msg void OnDestroy();
	afx_msg void OnItemchangedFont(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkFont(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FONTPICKERDLG_H__93C7B704_5A73_4A89_A913_4AED8FAB0C5F__INCLUDED_)
