#include "stdafx.h"

#include <math.h>

#include "BarcodeElement.h"
#include "Resource.h"
#include "CopyArray.h"
#include "List.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"
#include "Utils.h"
#include "TemplExt.h"
#include "BarcodeParams.h"
#include "BarcodeDlg.h"
#include "Extern.h"
#include "Coord.h"
#include "ximage.h"
#include "Parse.h"
#include "Registry.h"
#include "qrencode-3.4.4\qrencode.h"
#include "Parse.h"

#define QR_DATAMATRIX 7000

CRITICAL_SECTION csQR;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace Color;
using namespace FoxjetElements;

CString DebugDump (const CBarcodeParams & p)
{
	CString str = _T ("version: ") + FoxjetDatabase::ToString (p.GetVersion ()) + _T (", ");

	{
		struct 
		{
			int m_n;
			LPCTSTR m_lpsz;
		} static const mapLevel [] = 
		{
			{ QR_ECLEVEL_L, _T ("QR_ECLEVEL_L"), },
			{ QR_ECLEVEL_M, _T ("QR_ECLEVEL_M"), },
			{ QR_ECLEVEL_Q, _T ("QR_ECLEVEL_Q"), },
			{ QR_ECLEVEL_H, _T ("QR_ECLEVEL_H"), },
		};

		for (int i = 0; i < ARRAYSIZE (mapLevel); i++)
			if (mapLevel [i].m_n == p.GetLevel ())
				str += CString (_T ("level: ")) + mapLevel [i].m_lpsz + CString (_T (", "));
	}

	{
		struct 
		{
			int m_n;
			LPCTSTR m_lpsz;
		} static const mapHint [] = 
		{
			{ QR_MODE_NUL,			_T ("QR_MODE_NUL"), },
			{ QR_MODE_NUM,			_T ("QR_MODE_NUM"), },
			{ QR_MODE_AN,			_T ("QR_MODE_AN"), },
			{ QR_MODE_8,			_T ("QR_MODE_8"), },
			{ QR_MODE_KANJI,		_T ("QR_MODE_KANJI"), },
			{ QR_MODE_STRUCTURE,	_T ("QR_MODE_STRUCTURE"), },
			{ QR_MODE_ECI,			_T ("QR_MODE_ECI"), },
			{ QR_MODE_FNC1FIRST,	_T ("QR_MODE_FNC1FIRST"), },
			{ QR_MODE_FNC1SECOND,	_T ("QR_MODE_FNC1SECOND"), },
		};

		for (int i = 0; i < ARRAYSIZE (mapHint); i++) 
			if (mapHint [i].m_n == p.GetHint ())
				str += CString (_T ("hint: ")) + mapHint [i].m_lpsz + CString (_T (", "));
	}

	str += CString (_T ("case sensitive: ")) + FoxjetDatabase::ToString (p.IsCaseSensitive ());

	return str;
}



CSize CBarcodeElement::DrawQR (CDC &dc, const FoxjetDatabase::HEADSTRUCT & head, 
	CSize & sizeLogical, FoxjetCommon::DRAWCONTEXT context, 
	bool bThrowException, const CBarcodeParams & params)
{
	::EnterCriticalSection (&(::csQR));

	const CString str = GetImageData ();
	CSize size (0, 0);

	TRACEF (params.ToString (verApp));
	TRACEF (DebugDump (params));

	std::string strEncode = FoxjetDatabase::UnicodeToUTF8 (std::wstring (str));
	TRACEF (FoxjetDatabase::ToString ((int)str.GetLength ()) + _T (": ") + str);
	TRACEF (FoxjetDatabase::ToString ((int)strEncode.length ()) + _T (": ") + (CString)strEncode.c_str ());

//	if (QRcode * pGrid = QRcode_encodeString (w2a (str), params.GetVersion (), params.GetLevel (), params.GetHint (), params.IsCaseSensitive ())) {
	if (QRcode * pGrid = QRcode_encodeString (strEncode.c_str (), params.GetVersion (), params.GetLevel (), params.GetHint (), params.IsCaseSensitive ())) {
		int cx = pGrid->width;
		int cy = pGrid->width;
		CDC dcMem;
		CBitmap bmp;
		CxImage img;

		TRACEF (_T ("version: ") + FoxjetDatabase::ToString (pGrid->version) + _T (", width: ") + FoxjetDatabase::ToString (pGrid->width));

		size = CSize (cx * params.GetBarPixels (1), cy * params.GetBarPixels (2));

		VERIFY (dcMem.CreateCompatibleDC (&dc));
		VERIFY (bmp.CreateCompatibleBitmap (&dcMem, cy, cx));

		CBitmap * pBmp = dcMem.SelectObject (&bmp);

		::BitBlt (dcMem, 0, 0, cy, cx, NULL, 0, 0, BLACKNESS);
		img.CreateFromHBITMAP (bmp);

		/*
		{
			CString strEncoded;
			int nIndex = 0;

			for (int y = 0; y < cy; y++) {
				for (int x = 0; x < cx; x++) {
					CString strByte;

					strByte.Format (_T ("0x%X, "), pGrid->data [nIndex++]);
					strEncoded += strByte;
				}
			}

			TRACEF (str);
			TRACEF (strEncoded);
		}
		*/

		{
			for (int y = 0; y < cy; y++) {
				for (int x = 0; x < cx; x++) {
					bool bBit = (pGrid->data [(cx * y) + x] & 0x0001) ? true : false;
					
					//::OutputDebugString (bBit ? _T ("X") : _T (" "));
					img.SetPixelColor (cy - (y + 1), cx - (x + 1), bBit ? rgbWhite : rgbBlack);
					//img.SetPixelColor (x, y, bBit ? rgbWhite : rgbBlack);
				}
				//::OutputDebugString (_T ("\n"));
			}
			//::OutputDebugString (_T ("\n\n"));
		}

		if (context == EDITOR) {
			VERIFY (img.RotateLeft ());

			CxBitmap hBmp = img.MakeBitmap (dc);
			HGDIOBJ hMem = ::SelectObject (dcMem, (HGDIOBJ)hBmp);
			::StretchBlt (dc, 0, 0, size.cx, size.cy, dcMem, 0, 0, cy, cx, !IsInverse () ? SRCINVERT : SRCCOPY);
			::SelectObject (dcMem, hMem);
		}
		else {
			CxBitmap hBmp = img.MakeBitmap (dc);
			HGDIOBJ hMem = ::SelectObject (dcMem, (HGDIOBJ)hBmp);
			::StretchBlt (dc, 0, 0, size.cy, size.cx, dcMem, 0, 0, cx, cy, !IsInverse () ? SRCINVERT : SRCCOPY);
			::SelectObject (dcMem, hMem);
		}

		dcMem.SelectObject (pBmp);

		QRcode_free (pGrid);
		QRcode_clearCache ();
	}
	else {
		CString str = _T ("(") + FoxjetDatabase::ToString (errno) + _T (") ");

		size = CSize (21 * params.GetBarPixels (1), 21 * params.GetBarPixels (2));

		switch (errno) {
		case EINVAL:	str += _T ("(EINVAL) ") + LoadString (IDS_EINVAL);		break;
		case ENOMEM:	str += _T ("(ENOMEM) ") + LoadString (IDS_ENOMEM);		break;
		case ERANGE:	str += _T ("(ERANGE) ") + LoadString (IDS_ERANGE);		break;
		}

		::LeaveCriticalSection (&(::csQR));
		
		if (bThrowException)
			throw new CBarcodeException (LoadString (IDS_INVALIDQRDATA) + _T ("\n") + str);	
	}

	::LeaveCriticalSection (&(::csQR));

	sizeLogical = context == EDITOR ? size : CSize (size.cy, size.cx);

	::LeaveCriticalSection (&(::csQR));

	return m_size = LogicalToThousandths (size, head);
}

