#if !defined(AFX_GLOBALBARCODEDLG_H__1AF07EFD_D34A_437F_951D_48E775992A1D__INCLUDED_)
#define AFX_GLOBALBARCODEDLG_H__1AF07EFD_D34A_437F_951D_48E775992A1D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GlobalBarcodeDlg.h : header file
//

#include "Resource.h"
#include "ListCtrlImp.h"
#include "BarcodeParams.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CGlobalBarcodeDlg dialog

class CGlobalBarcodeDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CGlobalBarcodeDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent = NULL);   // standard constructor

	FoxjetDatabase::HEADTYPE	m_nHeadType;
	ULONG						m_lLineID;
	int							m_nSymbology;
	int							m_nMagnification;
	int							m_nResolution;

	static FoxjetElements::CBarcodeParams * OnEdit (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db);

// Dialog Data
	//{{AFX_DATA(CGlobalBarcodeDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGlobalBarcodeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	class CBarcodeItem : 
		public ItiLibrary::ListCtrlImp::CItem,
		public FoxjetElements::CBarcodeParams
	{
	public:
		CBarcodeItem ();
		CBarcodeItem (const FoxjetElements::CBarcodeParams & rhs);
		virtual ~CBarcodeItem ();

		virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
		virtual CString GetDispText (int nColumn) const;
	};

	ULONG GetCurSelLine () const;
	int GetCurSelSymbology () const;
	int GetCurSelResolution() const;
	FoxjetDatabase::HEADTYPE GetCurSelHeadType () const;
	virtual void OnCancel();
	void Load ();

	static bool EditUPCEAN (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db);
	static bool Edit (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db);
	static bool EditI2o5 (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db);
	static bool EditC128 (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db);
	static bool EditC39 (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db);
	static bool EditC93 (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db);
	static bool EditDataMatrix (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db);
	static bool EditQR (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db);

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
	FoxjetDatabase::COdbcDatabase & m_db;
	bool m_bUpdated;

	// Generated message map functions
	//{{AFX_MSG(CGlobalBarcodeDlg)
	afx_msg void OnDblclkBarcode(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEdit();
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeSelection ();
	afx_msg void OnRclickBarcode(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReset();
	//}}AFX_MSG

	afx_msg void OnImport ();
	afx_msg void OnExport ();
	afx_msg void OnCopyFrom ();

	bool m_bImported;

	DECLARE_MESSAGE_MAP()
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GLOBALBARCODEDLG_H__1AF07EFD_D34A_437F_951D_48E775992A1D__INCLUDED_)
