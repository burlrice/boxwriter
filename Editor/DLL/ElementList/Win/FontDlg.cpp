// FontDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "FontDlg.h"
#include "Extern.h"
#include "DevGuids.h"
#include "TemplExt.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetElements;
using namespace ListGlobals;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

ELEMENT_API bool FoxjetCommon::DoFontDialog (CPrinterFont & f, int & nWidth,
											 const CString & strSampleText, 
											 CWnd * pParent)
{
	CFontDlg dlg (pParent);
	FoxjetDatabase::HEADSTRUCT head = GetDefaultHead ();

	dlg.m_strSampleText = strSampleText;
	dlg.m_head			= head;
	dlg.m_strFont		= f.m_strName;
	dlg.m_nSize			= f.m_nSize;
	dlg.m_bItalic		= f.m_bItalic;
	dlg.m_bBold			= f.m_bBold;
	dlg.m_nWidth		= nWidth;

	if (dlg.DoModal () == IDOK) {
		f.m_strName		= dlg.m_strFont;
		f.m_nSize		= dlg.m_nSize;
		f.m_bItalic		= dlg.m_bItalic ? true : false;
		f.m_bBold		= dlg.m_bBold	? true : false;
		nWidth			= dlg.m_nWidth;

		return true;
	}

	return false;
}

/////////////////////////////////////////////////////////////////////////////
// CFontDlg dialog


CFontDlg::CFontDlg(CWnd* pParent /*=NULL*/)
:	m_pElement (NULL),
	m_bValve (ISVALVE ()),
	FoxjetCommon::CEliteDlg (ISVALVE () ? IDD_VXFONT : IDD_TTFONT, pParent)
{

	//{{AFX_DATA_INIT(CFontDlg)
	//}}AFX_DATA_INIT
}

CFontDlg::~CFontDlg ()
{
	if (m_pElement) {
		delete m_pElement;
		m_pElement = NULL;
	}
}

void CFontDlg::DoDataExchange(CDataExchange* pDX)
{
	CComboBox * pName = (CComboBox *)GetDlgItem (CB_NAME);

	ASSERT (pName);

	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFontDlg)
	DDX_Text(pDX, TXT_SIZE, m_nSize);
	DDX_Check(pDX, CHK_BOLD, m_bBold);
	DDX_Check(pDX, CHK_ITALIC, m_bItalic);
	DDX_Text(pDX, TXT_WIDTH, m_nWidth);

	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate) {
		int nIndex = pName->GetCurSel ();

		if (pName->GetLBTextLen (nIndex) > 0)
			pName->GetLBText (nIndex, m_strFont);
	}
	else {
		int nSel = pName->SelectString (-1, m_strFont);
	}
}


BEGIN_MESSAGE_MAP(CFontDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CFontDlg)
	ON_CBN_SELCHANGE(CB_NAME, OnUpdate)
	ON_BN_CLICKED(CHK_BOLD, OnUpdate)
	ON_BN_CLICKED(CHK_ITALIC, OnUpdate)
	ON_EN_CHANGE(TXT_WIDTH, OnUpdate)
	ON_EN_CHANGE(TXT_SIZE, OnUpdate)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFontDlg message handlers

void CFontDlg::OnUpdate() 
{
	if (!m_bValve) {
		if (m_pElement) {
			CPrinterFont f;
			CDC dcMem;
			CString str, strFont;
			CComboBox * pName = (CComboBox *)GetDlgItem (CB_NAME);
			CButton * pBold = (CButton *)GetDlgItem (CHK_BOLD);
			CButton * pItalic = (CButton *)GetDlgItem (CHK_ITALIC);

			ASSERT (pName);
			ASSERT (pBold);
			ASSERT (pItalic);

			int nIndex = pName->GetCurSel ();

			if (nIndex != CB_ERR)
				pName->GetLBText (nIndex, strFont);

			f.m_strName = strFont;
			f.m_nSize	= GetDlgItemInt (TXT_SIZE);
			f.m_bBold	= pBold->GetCheck () == 1;
			f.m_bItalic	= pItalic->GetCheck () == 1;

			m_pElement->SetDefaultData (m_strSampleText);
			m_pElement->SetFont (f);
			m_pElement->SetWidth (GetDlgItemInt (TXT_WIDTH));

			m_pElement->Build ();
			m_pElement->SetRedraw (true);

			dcMem.CreateCompatibleDC (GetDC ());

			m_pElement->Draw (dcMem, m_head, true);
		}

		m_wndPreview.Invalidate ();
		m_wndPreview.RedrawWindow ();
	}
}

BOOL CFontDlg::OnInitDialog() 
{
	CComboBox * pName = (CComboBox *)GetDlgItem (CB_NAME);
	CStringArray v;
	
	ASSERT (pName);

	CHead::GetWinFontNames (v);

	if (m_bValve)
		GetBitmappedFonts (v);

	for (int i = 0; i < v.GetSize (); i++) {
		int nIndex = pName->AddString (v [i]);

		if (!m_strFont.CompareNoCase (v [i]))
			pName->SetCurSel (nIndex);
	}

	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();

	if (!m_bValve) {
		if (CWnd * pPreview = GetDlgItem (IDC_PREVIEW)) {
			CRect rc;
			pPreview->GetWindowRect (&rc);

			ScreenToClient (&rc);

			m_pElement = new TextElement::CTextElement (m_head);
			m_pElement->Build ();

			VERIFY (m_wndPreview.Create (this, rc, m_pElement, &m_head));
		}
	}

	if (pName->GetCurSel () == CB_ERR)
		pName->SetCurSel (0);

	if (m_bValve) {
		UINT n [] = 
		{
			IDC_PREVIEW,
			IDC_SAMPLE,
		};

		for (int i = 0; i < ARRAYSIZE (n); i++)
			if (CWnd * p = GetDlgItem (n [i]))
				p->ShowWindow (SW_HIDE);

		if (CWnd * p = GetDlgItem (LBL_WIDTH))
			p->SetWindowText (LoadString (IDS_GAP));
	}

	OnUpdate ();

	return bResult;
}

void CFontDlg::OnPaint() 
{
	FoxjetCommon::CEliteDlg::OnPaint();
	
	if (!m_bValve) 
		OnUpdate ();
}
