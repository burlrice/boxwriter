#if !defined(AFX_COPYBARCODEPARAMSDLG_H__46A61D34_C21F_4D5F_B1F8_7E433BD5C54D__INCLUDED_)
#define AFX_COPYBARCODEPARAMSDLG_H__46A61D34_C21F_4D5F_B1F8_7E433BD5C54D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CopyBarcodeParamsDlg.h : header file
//

#include "Database.h"
#include "ListCtrlImp.h"
#include "Resource.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CCopyBarcodeParamsDlg dialog

class CCopyBarcodeParamsDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CCopyBarcodeParamsDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCopyBarcodeParamsDlg)
	enum { IDD = IDD_GLOBALBARCODE_COPYFROM };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	ULONG m_lLineID;

	class CCopyItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CCopyItem ();
		virtual ~CCopyItem ();

		//virtual int Compare (const ItiLibrary::CListCtrlImp::CItem & rhs, int nColumn) const;
		virtual CString GetDispText (int nColumn) const;
		
		FoxjetDatabase::PRINTERSTRUCT m_printer;
		FoxjetDatabase::LINESTRUCT m_line;
	};

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCopyBarcodeParamsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
	FoxjetDatabase::COdbcDatabase & m_db;
	CString m_strRegSection;

	afx_msg void OnDblclkParams(NMHDR* pNMHDR, LRESULT* pResult);

	// Generated message map functions
	//{{AFX_MSG(CCopyBarcodeParamsDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COPYBARCODEPARAMSDLG_H__46A61D34_C21F_4D5F_B1F8_7E433BD5C54D__INCLUDED_)
