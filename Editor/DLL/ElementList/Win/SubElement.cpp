// SubElement.cpp: implementation of the CSubElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "list.h"
#include "SubElement.h"
#include "TextElement.h"
#include "Resource.h"
#include "Edit.h"
#include "ElementDefaults.h"
#include "List.h"
#include "Debug.h"
#include "Extern.h"
#include "Utils.h"
#include "SerialElement.h"
#include "TemplExt.h"
#include "Parse.h"
#include "Registry.h"

using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace FoxjetElements::TextElement;
using namespace FoxjetDatabase;

//30Q324343430794<OQQ
//abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890`~!@#$%^&*()-=_+[]{}\|;':",.<>/?
//#define __SHOWISVALIDDATA__
//#define __SHOWMASKENTRYVECTOR__
//#define __TRACE_TOFROMSTRING__
//#define __NOVALIDATEDATA__


IMPLEMENT_DYNAMIC (CSubElement, CObject)

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

struct 
{
	LPCTSTR m_lpsz;
	int		m_nType;
	UINT	m_nResID;
} static const map [] = 
{
	{ _T ("TextObj"),		TEXT,		IDS_TEXT,		},
	{ _T ("CountObj"),		COUNT,		IDS_COUNT,		},
	{ _T ("DateTimeObj"),	DATETIME,	IDS_DATETIME,	},
	{ _T ("ExpDateObj"),	EXPDATE,	IDS_EXPDATE,	},
	{ _T ("ShiftObj"),		SHIFTCODE,	IDS_SHIFT,		},
	{ _T ("UserObj"),		USER,		IDS_USER,		},
	{ _T ("SerialObj"),		SERIAL,		IDS_SERIAL,		},
	{ _T ("DatabaseObj"),	DATABASE,	IDS_DATABASE,	},
};


const TCHAR CSubElement::m_cAlphaReq		= 'A';
const TCHAR CSubElement::m_cAlphaOpt		= 'a';
const TCHAR CSubElement::m_cNumericReq		= 'N';
const TCHAR CSubElement::m_cNumericOpt		= 'n';	
const TCHAR CSubElement::m_cAlphaNumericReq	= 'X';
const TCHAR CSubElement::m_cAlphaNumericOpt	= 'x';
const TCHAR CSubElement::m_cPunctReq		= 'P';
const TCHAR CSubElement::m_cPunctOpt		= 'p';
const TCHAR CSubElement::m_cAnyReq			= 'Z';
const TCHAR CSubElement::m_cAnyOpt			= 'z';
const TCHAR CSubElement::m_cDelimeter		= ',';
const TCHAR CSubElement::m_cDef				= CSubElement::m_cAnyOpt;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSubElement::CSubElement (int nType, const CString & strID, const CString & strConstMask, 
						  const CString & strData, const CString & strConstDesc)
:	m_pObj (NULL)
{
	CString strDesc (strConstDesc);
	CString strMask (strConstMask);

	InitObject (nType);
	ASSERT (m_pObj);

	if (!strDesc.GetLength ())
		strDesc = LoadString (IDS_DEFBARCODESUBELEMENT);

	if (!strMask.GetLength ()) {
		//strMask = CString (m_cDef);
		strMask = CString (m_cDef, strData.GetLength ());
	}

	m_strID			= strID;
	m_strDataMask	= strMask;
	m_strDesc		= strDesc;

	m_pObj->SetDefaultData (strData);
}

CSubElement::CSubElement (const CSubElement & rhs)
:	m_pObj (NULL),
	m_strID (rhs.m_strID),
	m_strDataMask (rhs.m_strDataMask),
	m_strDesc (rhs.m_strDesc)
{
	m_pObj = DYNAMIC_DOWNCAST (CTextElement, CopyElement (rhs.m_pObj, NULL, false));
	ASSERT (m_pObj);
}

CSubElement & CSubElement::operator = (const CSubElement & rhs)
{
	ASSERT (rhs.m_pObj);

	if (this != &rhs) {
		if (m_pObj) {
			delete m_pObj;
			m_pObj = NULL;
		}

		m_strID = rhs.m_strID;
		m_strDataMask = rhs.m_strDataMask;
		m_strDesc = rhs.m_strDesc;
		m_pObj = DYNAMIC_DOWNCAST (CTextElement, CopyElement (rhs.m_pObj, NULL, false));
		ASSERT (m_pObj);
	}

	return * this;
}

CSubElement::~CSubElement()
{
	if (m_pObj)
		delete m_pObj;
}

const CTextElement * CSubElement::GetElement () const
{
	ASSERT (m_pObj);
	return m_pObj;
}

CTextElement * CSubElement::GetElement ()
{
	ASSERT (m_pObj);
	return m_pObj;
}

bool CSubElement::SetElement (const CTextElement & e)
{
	// e.Build must have been called prior to this call
	if (IsValidData (e.GetImageData ())) {
		if (m_pObj) {
			delete m_pObj;
			m_pObj = NULL;
		}

		m_pObj = DYNAMIC_DOWNCAST (CTextElement, CopyElement (&e, NULL, false));
		ASSERT (m_pObj);

		if (CSerialElement * p = DYNAMIC_DOWNCAST (CSerialElement, m_pObj)) 
			p->SetLength (e.GetDefaultData ().GetLength ());

		return (m_pObj != NULL);
	}

	ASSERT (0);
	return false;
}

int CSubElement::Build (BUILDTYPE type, bool bCanThrowException) 
{
	return m_pObj->Build (type, bCanThrowException);
}

bool CSubElement::SetType (int type)
{
	for (int i = 0; i < ARRAYSIZE (::map); i++) 
		if (::map [i].m_nType == type)
			return true;

	return false;
}

void CSubElement::InitObject(int type)
{
	if (m_pObj) {
		delete m_pObj;
		m_pObj = NULL;
	}

	for (int i = 0; i < ARRAYSIZE (::map); i++) {
		if (::map [i].m_nType == type) {
			const CBaseElement & def = ListGlobals::defElements.GetElement (type);
			m_pObj = DYNAMIC_DOWNCAST (CTextElement, CopyElement (&def, NULL, false));
			ASSERT (m_pObj);
			break;
		}
	}

	ASSERT (m_pObj);
	m_strID = _T ("DEF");
	m_strDesc = LoadString (IDS_DEFBARCODESUBELEMENT);

	#ifdef __WASPDEMO__
		m_pObj->SetDefaultData (_T ("0123401234"));
	#else //!__WASPDEMO__
		m_pObj->SetDefaultData (_T ("0123456789"));
	#endif

	m_strDataMask = CString ('x', m_pObj->GetDefaultData ().GetLength ());
}

bool CSubElement::IsAi(const CString &str)
{
	// a valid AI string is 2, 3, or 4 numeric chars
	bool bResult = (str.GetLength () >= 2 && str.GetLength () <= 4);

	for (int i = 0; i < str.GetLength (); i++)
		if (str [i] < '0' || str [i] > '9')
			return false;

	return bResult;
}

bool CSubElement::IsAi() const
{
	ASSERT (m_pObj);
	return IsAi (m_strID);
}

CString CSubElement::GetData() const
{
	ASSERT (m_pObj);
	return ((CTextElement *)m_pObj)->GetDefaultData ();
}

bool CSubElement::SetData(const CString &str)
{
	CString strData (str);
	bool bResult = false;

	if (IsValidData (strData)) {
		ASSERT (m_pObj);
		CTextElement * p = (CTextElement *)m_pObj;
		
		bResult = p->SetDefaultData (strData);
		p->Build ();
	}

	return bResult;
}

bool CSubElement::IsValidData() const
{
	#ifdef __NOVALIDATEDATA__
	return true;
	#endif //__NOVALIDATEDATA__

	ASSERT (m_pObj);
	m_pObj->Build ();
	CString strData = m_pObj->GetImageData ();
	return IsValidData (strData);
}

bool CSubElement::IsValidData(const CString &strData) const
{
	#ifdef __NOVALIDATEDATA__
	return true;
	#endif //__NOVALIDATEDATA__

	ULONG lCalls;
	return IsValidData (strData, m_strDataMask, lCalls);
}

bool CSubElement::IsValidData(const CString &strData, const CString &strOrigMask, ULONG & lCalls)
{
	#ifdef __NOVALIDATEDATA__
	return true;
	#endif //__NOVALIDATEDATA__

	CString strMask (strOrigMask);

	strMask.Remove (m_cDelimeter);

	if (IsValidData (strData, 0) && IsValidMask (strMask)) {
		// no illegal chars in data or mask

		/*
		need to count alpha & numeric chars seperately
		*/
		int nAlphaMaskChars			= CountMaskChars (strMask, ALPHA, true);
		int nNumericMaskChars		= CountMaskChars (strMask, NUMERIC, true);
		int nAlphaNumericMaskChars	= CountMaskChars (strMask, ALPHANUMERIC, true);
		int nPunctMaskChars			= CountMaskChars (strMask, PUNCTUATION, true);
		int nAnyMaskChars			= CountMaskChars (strMask, ANY, true);
		
		int nAlphaDataChars			= CountDataChars (strData, ALPHA);
		int nNumericDataChars		= CountDataChars (strData, NUMERIC);
		int nPunctDataChars			= CountDataChars (strData, PUNCTUATION);
		int nAnyDataChars			= CountDataChars (strData, ANY);

		int nAlphaDiff = nAlphaDataChars - nAlphaMaskChars;
		int nNumericDiff = nNumericDataChars - nNumericMaskChars;
		int nAlphaNumericDiff = (nAlphaDiff + nNumericDiff) -
			nAlphaNumericMaskChars;
		int nPunctDiff = nPunctDataChars - nPunctMaskChars;
		int nAnyDiff = nAnyDataChars - nAnyMaskChars;
			
		#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
		CString strDebug;
		strDebug.Format (
			"\n\tstrData                = \"%s\" [%d]"
			"\n\tstrMask                = \"%s\" [%d]\n"
			"\n\tnAlphaMaskChars        = %d"
			"\n\tnNumericMaskChars      = %d"
			"\n\tnAlphaNumericMaskChars = %d"
			"\n\tnPunctMaskChars        = %d"
			"\n\tnAnyMaskChars          = %d"
			"\n"
			"\n\tnAlphaDataChars        = %d"
			"\n\tnNumericDataChars      = %d"
			"\n\tnPunctDataChars        = %d"
			"\n\tnAnyDataChars          = %d"
			"\n"
			"\n\tnAlphaDiff             = %d"
			"\n\tnNumericDiff           = %d"
			"\n\tnAlphaNumericDiff      = %d"
			"\n\tnPunctDiff             = %d"
			"\n\tnAnyDiff               = %d",
			strData, strData.GetLength (),
			strMask, strMask.GetLength (),
			nAlphaMaskChars,
			nNumericMaskChars,
			nAlphaNumericMaskChars,
			nPunctMaskChars,
			nAnyMaskChars,
			nAlphaDataChars,
			nNumericDataChars,
			nPunctDataChars,
			nAnyDataChars,
			nAlphaDiff,
			nNumericDiff,
			nAlphaNumericDiff,
			nPunctDiff,
			nAnyDiff);
		//MsgBox (strDebug, "Debug", MB_OK); 
		TRACEF (strDebug);
		#endif //_DEBUG	&& __TRACE_TOFROMSTRING__

		if ((nAlphaDiff < 0) || 
			(nNumericDiff < 0) || 
			(nAlphaNumericDiff < 0) ||
			(nPunctDiff < 0) ||
			(nAnyDiff < 0))
		{
			return false;
		}
		else if (strData.GetLength () > strMask.GetLength ())
			return false;
		else {
			//#ifdef _DEBUG
			CTime tm = CTime::GetCurrentTime ();
			//#endif //_DEBUG

			int nCalls = 0;
			bool bResult = IsValidData (strData, strMask, nCalls);
			lCalls = nCalls;
			//#ifdef _DEBUG
			CString str, strTime;
			CTimeSpan tmSpan = CTime::GetCurrentTime () - tm;

			strTime.Format (_T ("nCalls = %d, tmSpan = %u seconds"),
				nCalls, tmSpan.GetTotalSeconds ());
			//MsgBox (strTime, "Debug", MB_OK);
			str.Format (
				_T ("\n\tIsValidData (\"%s\", \"%s\", %d) = %s\n")
				_T ("\t%s"),
				strData, strMask, nCalls,
				bResult ? _T ("true") : _T ("false"),
				strTime);
			//TRACEF (str);
			//#endif //_DEBUG

			return bResult;
		}
	}

	return false;
}

int CSubElement::GetDataCharType (TCHAR c)
{
	if (IsAlpha (c))
		return ALPHA;
	else if (IsNumeric (c))
		return NUMERIC;
	/*
	else if (IsAlphaNumeric (c))
		return ALPHANUMERIC;
	*/
	else if (IsPunct (c))
		return PUNCTUATION;
	else if (IsAny (c))
		return ANY;
	else {
		#ifdef _DEBUG
		TRACEF ((CString)"c = '" + c + (CString)"' invalid");
		#endif //_DEBUG
		
		ASSERT (0);
		return -1;
	}
}

/*
returns all the possible points (as 0-based indicies) in strMask that
cLookAhead (a character from the data) could possibly occur.
these indices are inserted into v (a CDWordArray)
*/
int CSubElement::GetMaskEntryVector (TCHAR cLookAhead, const CString & strMask, CDWordArray & v)
{
	#ifdef _DEBUG
	/*
	CString str;
	str.Format ("GetMaskEntryVector ('%c', \"%s\", v)",
		cLookAhead, strMask);
	TRACEF (str);
	*/
	#endif //_DEBUG

	int nType = GetDataCharType (cLookAhead);

	v.RemoveAll ();

	if (nType != DELIMETER) {
		for (int i = 0; i < strMask.GetLength (); i++) {
			TCHAR c = strMask [i];
			bool bMatch = false;
			int nType = GetMaskCharType (c);

			switch (nType) {
			case ALPHA:
				bMatch = 
					nType == ALPHA ||
					nType == ALPHANUMERIC;
				break;
			case NUMERIC:
				bMatch = 
					nType == NUMERIC ||
					nType == ALPHANUMERIC;
				break;
			/*	GetMaskCharType (TCHAR) only returns ALPHA, NUMERIC, 
				PUCTUATION, or ANY
			*/
			case ALPHANUMERIC: 
				bMatch = 
					nType == ALPHA || 
					nType == NUMERIC ||
					nType == ALPHANUMERIC;
				break;
			case PUNCTUATION:
				bMatch = 
					nType == PUNCTUATION;
				break;
			case ANY:
				bMatch = 
					nType == ALPHA ||
					nType == NUMERIC ||
					nType == PUNCTUATION ||
					nType == ANY ||
					nType == ALPHANUMERIC;
				break;
			}

			if (bMatch) 
				v.Add (i);

			// can't go any farther if strMask [i] is a required char
			if (IsReqChar (c)) 
				break;
		}
	}

	return v.GetSize ();
}

int CSubElement::GetMaskCharType(TCHAR c)
{
	switch (c) {
	case CSubElement::m_cAlphaOpt:
	case CSubElement::m_cAlphaReq:
		return ALPHA;
	case CSubElement::m_cNumericOpt:
	case CSubElement::m_cNumericReq:
		return NUMERIC;
	case m_cAlphaNumericOpt:
	case m_cAlphaNumericReq:
		return ALPHANUMERIC;
	case m_cPunctOpt:
	case m_cPunctReq:
		return PUNCTUATION;
	case m_cAnyOpt:
	case m_cAnyReq:
		return ANY;
	case m_cDelimeter:
		return DELIMETER;
	}

	return -1;
}

bool CSubElement::MaskContainsReqChars (const CString & strMask)
{
	for (int i = 0; i < strMask.GetLength (); i++) 
		if (IsReqChar (strMask [i]))
			return true;

	return false;
}

bool CSubElement::MaskContainsOptChars (const CString & strMask)
{
	for (int i = 0; i < strMask.GetLength (); i++)
		if (IsOptChar (strMask [i]))
			return true;

	return false;
}

bool CSubElement::IsValidData (const CString & strData, const CString & strMask, 
							   int & nCalls, int nTab)
{
	#ifdef __NOVALIDATEDATA__
	return true;
	#endif //__NOVALIDATEDATA__

	//#ifdef _DEBUG
	// N,xxxxxxxxxx,xxxxxxxxxx,xxxxxxxxxx,N,xxxxxxxxxx,xxxxxxxxxx,xxxxxxxxxx,N
	//
	// 1ABBBBBBBBB2ABBBBBBBBBABBBBBBBBB3	12s
	// 1ABBBBBB2ABBBBBBBBBABBBBBBBBB3		91s
	// 1ABBBBB2ABBBBBBBBBABBBBBBBBB3		177s
	// 1ABBBB2ABBBBBBBBBABBBBBBBBB3			324s
	// 1ABBB2ABBBBBBBBBABBBBBBBBB3			530s
	// 1ABB2ABBBBBBBBBABBBBBBBBB3			1034s
	// 1AB2ABBBBBBBBBABBBBBBBBB3			2199s
	// 1A2ABBBBBBBBBABBBBBBBBB3				6921s (1.9225 hrs!)
	// 12ABBBBBBBBBABBBBBBBBB3				s
	const int nMaxCalls = 2000000000; // 2,000,000,000

	if (nCalls >= nMaxCalls) { 
		CString str;
		str.Format (_T ("nCalls >= %u"), nMaxCalls);
		MsgBox (str, _T ("Debug"), MB_OK);
		return false; 
	}
	//#endif //_DEBUG

	nCalls++;
	ASSERT (CString (strMask).Remove (m_cDelimeter) == 0);

	#if defined( _DEBUG ) && defined( __SHOWISVALIDDATA__ )
	CString strDebug, strTab = "\n" + CString ('\t', nTab);
	strDebug.Format ("CSubElement::IsValidData(\"%s\", \"%s\", %d, %d)",
		(LPCTSTR)strData, (LPCTSTR)strMask, nCalls, nTab);
	TRACEF (strTab + strDebug);
	#endif //_DEBUG && __SHOWISVALIDDATA__

	if (strData.GetLength () == 0) {
		bool bResult = !MaskContainsReqChars (strMask);

		#if defined( _DEBUG ) && defined( __SHOWISVALIDDATA__ )
		TRACEF (strTab + (bResult ? 
			"### true ### (no required chars remain)" :
			"--- false --- (required chars remain)"));
		#endif //_DEBUG && __SHOWISVALIDDATA__

		return bResult;
	}
	else {
		CDWordArray vEntry;

		GetMaskEntryVector (strData [0], strMask, vEntry);
		// gets all the possible indices strData [0] could occur in strMask

		#if defined( _DEBUG ) && defined( __SHOWISVALIDDATA__ )
			#ifdef __SHOWMASKENTRYVECTOR__
				CString strTmp;

				strDebug.Format ("%sGetMaskEntryVector ('%c', \"%s\", ...)",
					strTab, strData [0], strMask);

				for (int j = 0; j < vEntry.GetSize (); j++) {
					strTmp.Format ("%s\t'%c' --> \"%s\"",
						strTab, 
						strMask [(int)vEntry [j]],
						strMask.Mid ((int)vEntry [j]));
					strDebug += strTmp;
				}

				// this may fail an assertion on dumpout.cpp, line 52 
				// because the string can get too long
				TRACEF (strDebug + "\n");
			#endif //__SHOWMASKENTRYVECTOR__
		#endif //_DEBUG && __SHOWISVALIDDATA__

		// check each possible occurance index
		for (int i = 0; i < vEntry.GetSize (); i++) {
		//for (int i = vEntry.GetSize () - 1; i >= 0; i--) {
			CString strSubMask = strMask.Mid (vEntry [i]);
			bool bMatch = false;

			#if defined( _DEBUG ) && defined( __SHOWISVALIDDATA__ )
				strDebug.Format ("%strying vEntry [%d]:%s\tmask: \"%s\"%s\tdata: \"%s\"", 
					strTab, i, strTab, strSubMask, strTab, strData);
				TRACEF (strDebug + "\n");
			#endif //_DEBUG && __SHOWISVALIDDATA__

			if (strSubMask.GetLength ()) {
				#if defined( _DEBUG ) && defined( __SHOWISVALIDDATA__ )
				TRACEF (strTab + "mask = '" + CString (strSubMask [0]) +
					"', data = '" + CString (strData [0]) + "'");
				#endif //_DEBUG && __SHOWISVALIDDATA__

				switch (strSubMask [0]) {
				case m_cAlphaReq:
				case m_cAlphaOpt:
					bMatch = IsAlpha (strData [0]);
					break;
				case m_cNumericReq:
				case m_cNumericOpt:
					bMatch = IsNumeric (strData [0]);
					break;
				case m_cAlphaNumericReq:
				case m_cAlphaNumericOpt:
					bMatch = IsAlphaNumeric (strData [0]);
					break;
				case m_cPunctReq:
				case m_cPunctOpt:
					bMatch = IsPunct (strData [0]);
					break;
				case m_cAnyReq:
				case m_cAnyOpt:
					bMatch = IsAny (strData [0]);
					break;
				}

				if (bMatch) {
					//#ifdef _DEBUG
					if ((nCalls + 1) >= nMaxCalls) { 
						CString str;
						str.Format (_T ("(nCalls + 1) >= %u"), nMaxCalls);
						MsgBox (str, _T ("Debug"), MB_OK);
						return false; 
					}
					//#endif //_DEBUG

					// strip of the first chars of strData & strMask
					// and repeat
					if (IsValidData (strData.Mid (1), strSubMask.Mid (1), 
						nCalls, nTab + 1)) 
					{
						#if defined( _DEBUG ) && defined( __SHOWISVALIDDATA__ )
						TRACEF (strTab + "### true ###");
						#endif //_DEBUG && __SHOWISVALIDDATA__ 

						return true;
					}
				}
			}
		}
	}

	#if defined( _DEBUG ) && defined( __SHOWISVALIDDATA__ )
	TRACEF (strTab + "--- false ---");
	#endif //_DEBUG && __SHOWISVALIDDATA__

	return false;
}

bool CSubElement::IsAlpha(TCHAR c)
{
	return c >= 'A' && c <= 'Z';
}

bool CSubElement::IsNumeric(TCHAR c)
{
	return c >= '0' && c <= '9';
}

bool CSubElement::IsAlphaNumeric(TCHAR c)
{
	return IsAlpha (c) || IsNumeric (c);
}

bool CSubElement::IsPunct(TCHAR c)
{
	bool bResult =
		(c <= 33 && c >= 47) ||
		(c <= 58 && c >= 64) ||
		(c <= 91 && c >= 96) ||
		(c <= 123 && c >= 126);

	return bResult;
}

bool CSubElement::IsAny(TCHAR c)
{
	return true;
}

void CSubElement::ParseMask(const CString &str, CStringArray & v)
{
	if (str.GetLength ()) {
		CString strTmp;

		for (int i = 0; i < str.GetLength (); i++) {
			TCHAR cCur = str [i];
			
			switch (str [i]) {
				case m_cAlphaReq:
				case m_cAlphaOpt:
				case m_cNumericReq:
				case m_cNumericOpt:
				case m_cAlphaNumericReq:
				case m_cAlphaNumericOpt:
				case m_cPunctReq:
				case m_cPunctOpt:
				case m_cAnyReq:
				case m_cAnyOpt:
				case m_cDelimeter:
					break;
				default:
					ASSERT (false);
					TRACEF (_T ("void CSubElement::ParseMask(const CString &, CStringArray &)\n\tInvalid char"));
					return;
			}

			if (GetMaskCharType (cCur) == DELIMETER) {
				v.Add (strTmp);
				strTmp = _T ("");
			}
			else
				strTmp += cCur;
		}

		if (strTmp.GetLength ()) 
			v.Add (strTmp);
	}
}

int CSubElement::GetMaskChar (int nType, bool bRequired)
{
	switch (nType) {
	case CSubElement::ALPHA:
		return bRequired ? m_cAlphaReq : m_cAlphaOpt;
	case CSubElement::NUMERIC:
		return bRequired ? m_cNumericReq : m_cNumericOpt;
	case CSubElement::ALPHANUMERIC:
		return bRequired ? m_cAlphaNumericReq : m_cAlphaNumericOpt;
	case CSubElement::PUNCTUATION:
		return bRequired ? m_cPunctReq : m_cPunctOpt;
	case CSubElement::ANY:
		return bRequired ? m_cAnyReq : m_cAnyOpt;
	}

	return m_cAlphaNumericOpt;
}

bool CSubElement::IsOptChar (TCHAR c)
{
	switch (c) {
	case m_cAlphaOpt:
	case m_cNumericOpt:
	case m_cAlphaNumericOpt:
	case m_cPunctOpt:
	case m_cAnyOpt:
		return true;
	}

	return false;
}

bool CSubElement::IsReqChar (TCHAR c)
{
	switch (c) {
	case m_cAlphaReq:
	case m_cNumericReq:
	case m_cAlphaNumericReq:
	case m_cPunctReq:
	case m_cAnyReq:
		return true;
	}

	return false;
}

int CSubElement::GetOptCharCount (const CString & str)
{
	int nCount = 0;

	for (int i = 0; i < str.GetLength (); i++) 
		if (IsOptChar (str [i]))
			nCount++;

	return nCount;
}

int CSubElement::GetReqCharCount (const CString & str)
{
	int nCount = 0;

	for (int i = 0; i < str.GetLength (); i++) 
		if (IsReqChar (str [i]))
			nCount++;

	return nCount;
}

int CSubElement::GetType(const CString &strPrefix)
{
	for (int i = 0; i < ARRAYSIZE (::map); i++) 
		if (!strPrefix.CompareNoCase (map [i].m_lpsz))
			return map [i].m_nType;

	return -1;
}

CString CSubElement::GetPrefix(int nType) 
{
	for (int i = 0; i < ARRAYSIZE (::map); i++) 
		if (map [i].m_nType == nType)
			return map [i].m_lpsz;

	ASSERT (0);
	return _T ("");
}


CString CSubElement::ToString(const FoxjetCommon::CVersion & ver) const
{
	ASSERT (m_pObj);
	CString strData, str = m_pObj->ToString (ver);
	CLongArray commas = CountChars (str, (TCHAR)',');
	CLongArray braces = CountChars (str, (TCHAR)'}');

	if (commas.GetSize () > 10 && braces.GetSize ()) {
		int nLen = braces [0] - commas [10] - 1;

		strData = str.Mid (commas [10] + 1, nLen);

		#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
		TRACEF (strData);
		#endif //_DEBUG && __TRACE_TOFROMSTRING__ 
	}

	return 
		_T ("{") +
		GetPrefix (GetType ()) + _T (",") +
		FormatString (m_strID) + _T (",") +
		FormatString (m_strDesc) + _T (",") +
		FormatString (m_strDataMask) + _T (",") +
		strData +	
		_T ("}");
}

bool CSubElement::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
	TRACEF ("CSubElement::FromString (\"" + str + "\")");
	#endif //_DEBUG && __TRACE_TOFROMSTRING__

	CStringArray v;
	bool bResult = false;

	Tokenize (str, v);

	if (v.GetSize () >= 4) {
		CString strPrefix = v [0];

		int nIndex = strPrefix.Find (_T ("Obj"));

		if (nIndex != -1) {
			int nType = GetType (strPrefix);

			if (nType != -1) { 
				if (m_pObj) {
					delete m_pObj;
					m_pObj = NULL;
				}

				const CBaseElement & def = ListGlobals::defElements.GetElement (nType);
				m_pObj = DYNAMIC_DOWNCAST (CTextElement, CopyElement (&def, NULL, false));
				ASSERT (m_pObj);

				// std // #if __CUSTOM__ == __SW0840__
				SetLineID (GetLineID ());
				// std // #endif

				CString strElementData = strPrefix.Left (strPrefix.GetLength () - 3) + 
					_T (",0,0,0") + CString ((TCHAR)',', 8); 

				for (int i = 4; i < v.GetSize (); i++) 
					strElementData += v [i] + ',';

				#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
				TRACEF ("element data = \"" + strElementData + "\"");
				#endif //_DEBUG && __TRACE_TOFROMSTRING__ 

				if (m_pObj->FromString (strElementData, ver)) {
					m_strID = UnformatString (v [1]);
					m_strDesc = UnformatString (v [2]);
					m_strDataMask = UnformatString (v [3]);

					bResult = IsValidData ();
				}
			}
		}
	}

	return bResult;
}

bool CSubElement::IsValidMask(const CString &str)
{
	for (int i = 0; i < str.GetLength (); i++) {
		switch (str [i]) {
			case m_cAlphaReq:
			case m_cAlphaOpt:
			case m_cNumericReq:
			case m_cNumericOpt:
			case m_cAlphaNumericReq:
			case m_cAlphaNumericOpt:
			case m_cPunctReq:
			case m_cPunctOpt:
			case m_cAnyReq:
			case m_cAnyOpt:
			case m_cDelimeter:
				break;
			default:
				return false;
		}
	}

	return true;
}

bool CSubElement::IsValidData (const CString & str, int)
{
	// this can't work with the ANY char type
	/*
	for (int i = 0; i < str.GetLength (); i++) {
		if (!IsAlphaNumeric (str [i]))
			return false;
	}
	*/

	return true;
}

int CSubElement::CountMaskChars(const CString &str, int nType, bool bRequired)
{
	int nCount = 0;
	TCHAR cCmp [2] = { -1, -1 };

	switch (nType) {
	case ALPHA:
		cCmp [0] = m_cAlphaReq;
		cCmp [1] = m_cAlphaOpt;
		break;
	case NUMERIC:
		cCmp [0] = m_cNumericReq;
		cCmp [1] = m_cNumericOpt;
		break;
	case ALPHANUMERIC:
		cCmp [0] = m_cAlphaNumericReq;
		cCmp [1] = m_cAlphaNumericOpt;
		break;
	case PUNCTUATION:
		cCmp [0] = m_cPunctReq;
		cCmp [1] = m_cPunctOpt;
		break;
	case ANY:
		cCmp [0] = m_cAnyReq;
		cCmp [1] = m_cAnyOpt;
		break;
	}

	if (bRequired) 
		cCmp [1] = -1;

	for (int i = 0; i < str.GetLength (); i++)
		if (str [i] == cCmp [0] || str [i] == cCmp [1])
			nCount++;

	return nCount;
}

static bool DefCountDataCharsFct (TCHAR)
{
	return false;
}

int CSubElement::CountDataChars(const CString &str, int nType)
{
	int nCount = 0;
	bool (* pFct)(TCHAR) = DefCountDataCharsFct;

	switch (nType) {
	case ALPHA:			pFct = IsAlpha;			break;
	case NUMERIC:		pFct = IsNumeric;		break;
	case ALPHANUMERIC:	pFct = IsAlphaNumeric;	break;
	case PUNCTUATION:	pFct = IsPunct;			break;
	case ANY:			pFct = IsAny;			break;
	}

	for (int i = 0; i < str.GetLength (); i++)
		if (pFct (str [i]))
			nCount++;

	return nCount;
}

#ifdef _DEBUG
void CSubElement::RunIsValidDataTest (LPCTSTR pszFile, int nLen, const TCHAR cMask [], const TCHAR cData [])
{
	FILE * fp = _tfopen (pszFile, _T ("w"));
	CTime tmStart = CTime::GetCurrentTime ();
	CString str;

	if (fp == NULL) {
		MsgBox (_T ("Couldn't open ") + CString (pszFile), _T ("Debug"), MB_OK);
		return;
	}
	else {
		str = _T ("Mask,Data,Result,Total seconds,Calls,\n");
		fwrite (w2a (str), str.GetLength (), 1, fp);
	}

	fclose (fp);

	CSubElement::GenerateMask ("", cMask, cData, nLen, pszFile);

	if ((fp = _tfopen (pszFile, _T ("a"))) != NULL) {
		CTimeSpan tmSpan = CTime::GetCurrentTime () - tmStart;
		str.Format (_T (",,,%u,,\n"), tmSpan.GetTotalSeconds ());
		fwrite (w2a (str), str.GetLength (), 1, fp);
		fclose (fp);
	}
}

void CSubElement::GenerateData (const CString & strData, const CString & strMask, 
							const TCHAR cData [], LPCTSTR pszFile)
{
	FILE * fp = _tfopen (pszFile, _T ("a"));

	#ifdef _DEBUG
	CString str;
	str.Format (_T ("CSubElement::GenerateData (%s, %s, ... %s)"),
		strData, strMask, pszFile);
	TRACEF (str);
	#endif //_DEBUG

	if (fp != NULL) {
		CString str;

		CTime tmStart = CTime::GetCurrentTime ();
		ULONG lCalls = 0;
		bool bResult = CSubElement::IsValidData (strData, strMask, lCalls);
		CTime tmStop = CTime::GetCurrentTime ();
		
		CTimeSpan tmSpan = tmStop - tmStart;

		if (lCalls > (ULONG)(strMask.GetLength () + 1) || 
			tmSpan.GetTotalSeconds () > 0) 
		{
			str.Format (_T ("%s,%s,%d,%u,%u\n"),
				strMask,
				strData,
				bResult ? 1 : 0,
				tmSpan.GetTotalSeconds (),
				lCalls);
			fwrite (w2a (str), str.GetLength (), 1, fp);
			fclose (fp);
		}

		if (strData.GetLength () < strMask.GetLength ()) {
			for (int i = 0; cData [i] != -1; i++) 
				GenerateData (strData + cData [i], strMask, cData, pszFile);
		}
	}
}

void CSubElement::GenerateMask (CString strMask, const TCHAR cMask [], 
							const TCHAR cData [], int nMaxLen, 
							LPCTSTR pszFile) 
{
	if (strMask.GetLength () >= nMaxLen)
		//GenerateData (CString ('A', strMask.GetLength ()), strMask, cData, pszFile);
		GenerateData ("", strMask, cData, pszFile);
	else
		for (int i = 0; cMask [i] != -1; i++) 
			GenerateMask (strMask + cMask [i], cMask, cData, nMaxLen, pszFile);
}
#endif //_DEBUG

CString CSubElement::GetDataMaskDisp() const
{
	return GetDataMaskDisp (m_strDataMask);
}

CString CSubElement::GetDataMaskDisp(const CString & str) 
{
	CStringArray v;
	CString strResult;

	ParseMask (str, v);

	for (int i = 0; i < v.GetSize (); i++) {
		if (v [i].GetLength ()) {
			CString strSeg;
			TCHAR c = v [i][0];
			bool bVariable = IsOptChar (c);

			switch (CSubElement::GetMaskCharType (c)) {
			case ALPHA:
				strSeg.Format (_T ("a%s%d"), bVariable ? _T ("..") : _T (""), 
					v [i].GetLength ());
				break;
			case NUMERIC:
				strSeg.Format (_T ("n%s%d"), bVariable ? _T ("..") : _T (""),
					v [i].GetLength ());
				break;
			case ALPHANUMERIC:
				strSeg.Format (_T ("an%s%d"), bVariable ? _T ("..") : _T (""), 
					v [i].GetLength ());
				break;
			case PUNCTUATION:
				strSeg.Format (_T ("p%s%d"), bVariable ? _T ("..") : _T (""), 
					v [i].GetLength ());
				break;
			case ANY:
				strSeg.Format (_T ("z%s%d"), bVariable ? _T ("..") : _T (""), 
					v [i].GetLength ());
				break;
			}

			if (strSeg.GetLength ())
				strResult += strSeg + _T ("+");
		}
	}

	// remove the last "+" char
	if (strResult.GetLength () && strResult [strResult.GetLength () - 1] == (TCHAR)'+')
		strResult.Delete (strResult.GetLength () - 1);

	return strResult;
}

int CSubElement::GetDataMaskLength() const
{
	CString strMask = m_strDataMask;
	strMask.Remove (CSubElement::m_cDelimeter);
	return strMask.GetLength ();
}

void CSubElement::Reset ()
{
	ASSERT (m_pObj);
	m_pObj->Reset ();
}


int CSubElement::GetSubtypeCount ()
{
	return ARRAYSIZE (::map);
}

UINT CSubElement::GetSubtypeResID (int nIndex)
{
	if (nIndex >= 0 && nIndex < GetSubtypeCount ())
		return ::map [nIndex].m_nResID;

	return -1;
}

int CSubElement::GetSubtypeClassID (int nIndex)
{
	if (nIndex >= 0 && nIndex < GetSubtypeCount ())
		return ::map [nIndex].m_nType;

	return -1;
}

CString CSubElement::GetSubtype (int nIndex)
{
	if (nIndex >= 0 && nIndex < GetSubtypeCount ())
		return ::map [nIndex].m_lpsz;

	return _T ("");
}

bool CSubElement::IsVariableLen (const CString & strAI)
{
	int n = _ttoi (strAI);

	if (n >= 310 && n <= 369)
		return true;

	if (n >= 92 && n <= 98)
		return true;

	switch (n) {
	case 10:
	case 21:
	case 30:
	case 37:
	case 420:
	case 3922:
	case 3932:
	case 8200:
		return true;
	}

	return false;
}

bool CSubElement::IsVariableLen () const
{
	const TCHAR map [] = 
	{
		m_cAlphaOpt,
		m_cNumericOpt,
		m_cAlphaNumericOpt,
		m_cPunctOpt,
		m_cAnyOpt,
	};

	for (int i = 0; i < ARRAYSIZE (map); i++)
		if (m_strDataMask.Find (map [i]) != -1)
			return true;

	return false;
}

// std // #if __CUSTOM__ == __SW0840__
void CSubElement::SetLineID (ULONG lLineID)
{
	m_lLineID = lLineID;

	if (m_pObj)
		m_pObj->SetLineID (lLineID);
}
// std // #endif //__CUSTOM__ == __SW0840__
