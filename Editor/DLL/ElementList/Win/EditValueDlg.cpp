// EditValueDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "EditValueDlg.h"
#include "TextElement.h"

using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace FoxjetElements::TextElement;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditValueDlg dialog

CEditValueDlg::CEditValueDlg(CWnd* pParent /*=NULL*/)
:	m_nIndex (-1),
	FoxjetCommon::CEliteDlg(IDD_EDITVALUE, pParent)
{
	//{{AFX_DATA_INIT(CEditValueDlg)
	m_strValue = _T("");
	//}}AFX_DATA_INIT
}

void CEditValueDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditValueDlg)
	DDX_Text(pDX, TXT_INDEX, m_nIndex);
	//}}AFX_DATA_MAP

	DDX_Text(pDX, TXT_VALUE, m_strValue);
	DDV_MinMaxChars (pDX, m_strValue, TXT_VALUE, CTextElement::m_lmtString);
}


BEGIN_MESSAGE_MAP(CEditValueDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CEditValueDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditValueDlg message handlers

BOOL CEditValueDlg::OnInitDialog ()
{
	FoxjetCommon::CEliteDlg::OnInitDialog ();

	if (m_nIndex == -1) {
		if (CWnd * p = GetDlgItem (LBL_INDEX))
			p->ShowWindow (SW_HIDE);
		if (CWnd * p = GetDlgItem (TXT_INDEX))
			p->ShowWindow (SW_HIDE);
	}

	return TRUE;
}