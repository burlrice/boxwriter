// DatabaseElement.h: interface for the CDatabaseElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATABASEELEMENT_H__255395A2_94F0_45C8_91FF_2B2209386D17__INCLUDED_)
#define AFX_DATABASEELEMENT_H__255395A2_94F0_45C8_91FF_2B2209386D17__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TextElement.h"
#include "ExpDateElement.h"

namespace FoxjetElements
{
	class ELEMENT_API CDatabaseElement : public FoxjetElements::TextElement::CTextElement  
	{
		DECLARE_DYNAMIC (CDatabaseElement);
	public:
		CDatabaseElement (const FoxjetDatabase::HEADSTRUCT & head);
		CDatabaseElement (const CDatabaseElement & rhs);
		CDatabaseElement & operator = (const CDatabaseElement & rhs);
		virtual ~CDatabaseElement();

		virtual int GetClassID () const { return DATABASE; }
		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);

		virtual CSize Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly = false, FoxjetCommon::DRAWCONTEXT context = EDITOR);
		virtual int Build (BUILDTYPE type = INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;
	
		virtual bool SetFontSize (UINT nSize);

		CString GetDSN () const;
		bool SetDSN (const CString & str);

		CString GetTable () const;
		bool SetTable (const CString & str);

		CString GetField () const;
		bool SetField (const CString & str);

		int GetRow () const;
		bool SetRow (int nRow);

		CString GetKeyField () const;
		bool SetKeyField (const CString & str);

		CString GetKeyValue () const;
		bool SetKeyValue (const CString & str);

		bool SetSQL (bool bUseSQL = false, const CString & strSQL = _T (""));
		CString GetSQL () const;
		
		bool SetPromptAtTaskStart (bool bPrompt);
		bool GetPromptAtTaskStart () const;
		CString GetPrompt () const;

		static CString GetSQLType (DWORD dwType);
		int GetSQLType () const;
		bool SetSQLType (int nType);

		CString CreateSQL (FoxjetDatabase::COdbcDatabase * pdb = NULL) const;

		CString GetEmptyString () const;
		bool SetEmptyString (const CString & str);

		void Validate () const;
		// throws CDBException, CMemoryException


		bool GetSerial () const;
		bool SetSerial (bool bSerial);

		bool GetSerialDownload () const;
		bool SetSerialDownload (bool bValue);

		CString GetFormat () const;
		bool SetFormat (const CString & str);
	
		int GetFormatType () const;
		bool SetFormatType (int nType);

		bool SetMaxWidthEnabled (bool bEnabled);
		bool IsMaxWidthEnabled () const;

		bool SetMaxWidth (ULONG lValue);
		ULONG GetMaxWidth () const;

		void Set (const CExpDateElement & e);

		bool SetBmpWidthField (const CString & str) { m_strBmpWidthField = str; return true; }
		CString GetBmpWidthField () const { return m_strBmpWidthField; }

		bool SetBmpHeightField (const CString & str) { m_strBmpHeightField = str; return true; }
		CString GetBmpHeightField () const { return m_strBmpHeightField; }

		CSize GetBitmapSize () const { return m_sizeBmp; }

		void Invalidate ();

		static std::map <std::wstring, std::wstring> GetGeneralArgs (const CString & strSQL);

		static const FoxjetCommon::LIMITSTRUCT m_lmtKey;

		static const CString m_strNA;
		static const DWORD m_dwOpenEx;
		static CString GetAuto ();
		static CString GetStatic ();
		static CString GetNative ();

		static CExpDateElement::SPAN CDatabaseElement::GetTtlSpan (const CString & str);
		bool TTL () const;
		CString GetRstCacheKey () const { return m_strRstCacheKey; }

	protected:

		int LookupSQLType ();
		bool IsBypassCache (const CString & strSQL) const;
		int BypassCache ();

		CString		m_strDSN;
		CString		m_strTable;
		CString		m_strField;
		CString		m_strKeyField;
		CString		m_strKeyValue;
		int			m_nRow;

		bool		m_bSQL;
		CString		m_strSQL;

		bool		m_bPrompt;
		int			m_nSQLType;
		bool		m_bInvalid;
		bool		m_bSerial;
		bool		m_bSerialDownload;
		int			m_nFormat;
		CString		m_strFormat;
		CString		m_strEmptyString;

		bool		m_bMaxWidth;
		ULONG		m_lMaxWidth;
		int			m_nFontMax;
		bool		m_bDrawing;
		CString		m_strBmpWidthField;
		CString		m_strBmpHeightField;
		CSize		m_sizeBmp;

		CTime		m_tmRstCache;
		CString		m_strRstCacheKey;
		std::map <std::wstring, std::wstring> m_mapGeneral;
	};
}; //namespace FoxjetElements

#endif // !defined(AFX_DATABASEELEMENT_H__255395A2_94F0_45C8_91FF_2B2209386D17__INCLUDED_)
