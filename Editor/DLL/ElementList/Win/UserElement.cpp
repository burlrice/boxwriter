// UserElement.cpp: implementation of the CUserElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UserElement.h"
#include "List.h"
#include "ElementDefaults.h"
#include "Edit.h"
#include "Debug.h"
#include "Extern.h"
#include "Utils.h"
#include "Parse.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetElements;
using namespace FoxjetCommon::ElementFields;


IMPLEMENT_DYNAMIC (CUserElement, CTextElement);

const FoxjetCommon::LIMITSTRUCT CUserElement::m_lmtString = { 0, lmtString.m_dwMax };
const LPCTSTR CUserElement::m_lpszDate		= _T ("DATE");
const LPCTSTR CUserElement::m_lpszSysID		= _T ("SYSID");
const LPCTSTR CUserElement::m_lpszUserFirst	= _T ("USERFIRST");
const LPCTSTR CUserElement::m_lpszUserLast	= _T ("USERLAST");
const LPCTSTR CUserElement::m_lpszUserName	= _T ("USERUSER");

CUserElement::CUserElement(const FoxjetDatabase::HEADSTRUCT & head)
:	m_strPrompt (_T ("Enter user data: ")), 
	m_bPromptAtTaskStart (true),
	m_nMaxChars (15),
	CTextElement (head)
{
	m_strDefaultData = _T ("User element");
}

CUserElement::CUserElement (const CUserElement & rhs)
:	m_strPrompt (rhs.m_strPrompt),
	m_bPromptAtTaskStart (rhs.m_bPromptAtTaskStart),
	m_nMaxChars (rhs.m_nMaxChars),
	CTextElement (rhs)
{
}

CUserElement & CUserElement::operator = (const CUserElement & rhs)
{
	CTextElement::operator = (rhs);

	if (this != & rhs) {
		m_strPrompt				= rhs.m_strPrompt;
		m_bPromptAtTaskStart	= rhs.m_bPromptAtTaskStart;
		m_nMaxChars				= rhs.m_nMaxChars;
	}

	return * this;
}

CUserElement::~CUserElement()
{

}

// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
static const ElementFields::FIELDSTRUCT fields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType,		_T ("")					),	// [0]	Prefix (required)
	ElementFields::FIELDSTRUCT (m_lpszID,				_T ("")					),	// [1]	ID (required)
	ElementFields::FIELDSTRUCT (m_lpszX,				_T ("")					),	// [2]	x (required)
	ElementFields::FIELDSTRUCT (m_lpszY,				_T ("")					),	// [3]	y (required)
	ElementFields::FIELDSTRUCT (m_lpszFlipH,			_T ("0")				),	// [4]	flip h
	ElementFields::FIELDSTRUCT (m_lpszFlipV,			_T ("0")				),	// [5]	flip v
	ElementFields::FIELDSTRUCT (m_lpszInverse,			_T ("0")				),	// [6]	inverse
	ElementFields::FIELDSTRUCT (m_lpszFontName,			_T ("Courier New")		),	// [7]	font name
	ElementFields::FIELDSTRUCT (m_lpszFontSize,			_T ("32")				),	// [8]	font size
	ElementFields::FIELDSTRUCT (m_lpszBold,				_T ("0")				),	// [9]	bold
	ElementFields::FIELDSTRUCT (m_lpszItalic,			_T ("0")				),	// [10]	italic
	ElementFields::FIELDSTRUCT (m_lpszData,				_T ("User element")		),	// [11]	data
	ElementFields::FIELDSTRUCT (m_lpszMaxChars,			_T ("15")				),	// [12]	max chars
	ElementFields::FIELDSTRUCT (m_lpszPrompt,			_T ("Enter user data")	),	// [13]	prompt
	ElementFields::FIELDSTRUCT (m_lpszPromptAtStart,	_T ("1")				),	// [14]	prompt at task start
	ElementFields::FIELDSTRUCT (m_lpszOrient,			_T ("0")				),	// [15]	orientation
	ElementFields::FIELDSTRUCT (m_lpszWidth,			_T ("0")				),	// [16]	font width
	ElementFields::FIELDSTRUCT (m_lpszParaAlign,		_T ("0")				),	// [17]	paragraph alignment
	ElementFields::FIELDSTRUCT (m_lpszParaGap,			_T ("0")				),	// [18]	paragraph gap
	ElementFields::FIELDSTRUCT (m_lpszResizable,		_T ("0")				),	// [19] resizable
	ElementFields::FIELDSTRUCT (m_lpszParaWidth,		_T ("0")				),	// [20] paragraph width
	ElementFields::FIELDSTRUCT (m_lpszAlign,			_T ("0")				),	// [21] alignment
	ElementFields::FIELDSTRUCT (m_lpszDither,			_T ("0")				),	// [22]
	ElementFields::FIELDSTRUCT (m_lpszColor,			_T ("0")				),	// [23]
	ElementFields::FIELDSTRUCT (),
};

CString CUserElement::ToString (const FoxjetCommon::CVersion & ver) const
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated

	CString str;
	CPrinterFont f = GetFont ();
	CPoint pt = GetPosInternal (ALIGNMENT_ADJUST_ADD);

	str.Format (_T ("{User,%u,%u,%u,%d,%d,%d,%s,%u,%d,%d,%s,%d,%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%06X}"),
		GetID (),
		pt.x, pt.y,
		IsFlippedH (),
		IsFlippedV (),
		IsInverse (),
		f.m_strName, f.m_nSize, f.m_bBold, f.m_bItalic,
		FormatString (GetDefaultData ()),
		m_nMaxChars,
		FormatString (m_strPrompt),
		m_bPromptAtTaskStart,
		(int)GetOrientation (),
		GetWidth (),
		(int)GetParaAlign (),
		GetParaGap (),
		IsResizable (),
		GetParaWidth (),
		GetAlignment (),
		GetDither (),
		GetColor ());

	return str;
}


bool CUserElement::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	CStringArray vstrToken;
	CPrinterFont f;

	Parse (str, ver, vstrToken);

	if (vstrToken [0].CompareNoCase (_T ("User")) != 0){
		TRACEF ("Wrong prefix \"" + vstrToken [0] + "\"");
		return false;
	}

	f.m_strName = vstrToken [7];
	f.m_nSize = (UINT)_tcstol (vstrToken [8], NULL, 10);
	f.m_bBold = _ttoi (vstrToken [9]) ? true : false;
	f.m_bItalic = _ttoi (vstrToken [10]) ? true : false;

	m_strPrompt = UnformatString (vstrToken [13]);
	m_bPromptAtTaskStart = _ttoi (vstrToken [14]) ? true : false;

	bool bResult = true;
	
	// must be called before SetDefaultData
	bResult &= SetMaxChars ((UINT)_tcstol (vstrToken [12], NULL, 10));

	bResult &= SetID ((UINT)_tcstol (vstrToken [1], NULL, 10));
	bResult &= SetPos (CPoint (
		(UINT)_tcstol (vstrToken [2], NULL, 10), 
		(UINT)_tcstol (vstrToken [3], NULL, 10)), NULL);
	bResult &= SetFlippedH (_ttoi (vstrToken [4]) ? true : false);
	bResult &= SetFlippedV (_ttoi (vstrToken [5]) ? true : false);
	bResult &= SetInverse (_ttoi (vstrToken [6]) ? true : false);
	bResult &= SetFont (f);
	bResult &= SetDefaultData (UnformatString (vstrToken [11]));
	bResult &= SetOrientation ((TextElement::FONTORIENTATION)_ttoi (vstrToken [15]));
	bResult &= SetWidth (_ttoi (vstrToken [16]));
	bResult &= SetParaAlign ((ALIGNMENT)_ttoi (vstrToken [17]));
	bResult &= SetParaGap (_ttoi (vstrToken [18]));
	bResult &= SetResizable (_ttoi (vstrToken [19]) ? true : false);
	bResult &= SetParaWidth (_ttoi (vstrToken [20]));
	bResult &= SetAlignment ((ALIGNMENT)_ttoi (vstrToken [21]));
	bResult &= SetDither (_ttoi (vstrToken [22]));
	bResult &= SetColor (_tcstoul (vstrToken [23], NULL, 16));

	return bResult;
}

int CUserElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	m_strImageData = m_strDefaultData;
	return GetClassID ();
}

int CUserElement::GetMaxChars () const 
{
	return m_nMaxChars;
}

bool CUserElement::SetMaxChars (int nMax)
{
	if (FoxjetCommon::IsValid (nMax, CUserElement::m_lmtString)) {
		SetRedraw ();
		m_nMaxChars = nMax;
		return true;
	}

	return false;
}

CString CUserElement::GetPrompt () const
{
	return m_strPrompt;
}

bool CUserElement::SetPrompt (const CString & strSet)
{
	CString str (strSet);

	str.TrimLeft ();
	str.TrimRight ();

	if (str.GetLength () <= (int)m_lmtString.m_dwMax) {
		m_strPrompt = str;
		return true;
	}

	return false;
}

const FoxjetCommon::ElementFields::FIELDSTRUCT * CUserElement::GetFieldBuffer () const
{
	return ::fields;
}

bool CUserElement::SetDefaultData(const CString &strData)
{
	CString str = (strData.GetLength () <= GetMaxChars ()) ? strData : strData.Mid (0, GetMaxChars ());

	if (IsValid (str.GetLength (), m_lmtString)) {
		SetRedraw ();
		m_strDefaultData = str;
		return true;
	}

	return false;

	if (strData.GetLength () <= GetMaxChars ()) 
		return CTextElement::SetDefaultData (strData);
	else
		return CTextElement::SetDefaultData (strData.Mid (0, GetMaxChars ()));
}

bool CUserElement::IsNoPrompt () const
{
	LPCTSTR map [] = 
	{
		m_lpszDate,
		m_lpszSysID,
		m_lpszUserFirst,
		m_lpszUserLast,
		m_lpszUserName,
	};

	for (int i = 0; i < ARRAYSIZE (map); i++)
		if (m_strPrompt.CompareNoCase (map [i]))
			return true;

	return false;
}
