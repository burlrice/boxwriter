#include "stdafx.h"

#include <math.h>

#include "BarcodeElement.h"
#include "Resource.h"
#include "CopyArray.h"
#include "List.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"
#include "Utils.h"
#include "TemplExt.h"
#include "BarcodeParams.h"
#include "BarcodeDlg.h"
#include "Extern.h"
#include "Coord.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace Color;
using namespace FoxjetElements;

namespace Code39 
{
	class CPattern
	{
	public:
		CPattern (
			int b1 = 0, int s1 = 0, 
			int b2 = 0, int s2 = 0, 
			int b3 = 0, int s3 = 0, 
			int b4 = 0, int s4 = 0, 
			int b5 = 0, 
			int nCheck = 0, TCHAR c = ' ');
		CPattern (const CPattern & rhs);
		CPattern & operator = (const CPattern & rhs);
		virtual ~CPattern ();

		int		m_nBar [5];
		int		m_nSpace [4];
		int		m_nCheck;
		TCHAR	m_c;

		CString ToString () const;
	};

	const int n = 1;
	const int w = 2;

	bool IsValidData (const CString & str);
	TCHAR CalcChecksum (const CString & str);
}; //namespace Code39

using namespace Code39;

////////////////////////////////////////////////////////////////////////////////

CPattern::CPattern (int b1, int s1, 
					int b2, int s2, 
					int b3, int s3, 
					int b4, int s4,
					int b5,  
					int nCheck, TCHAR c)
{
	m_nBar [0]		= b1;
	m_nBar [1]		= b2;
	m_nBar [2]		= b3;
	m_nBar [3]		= b4;
	m_nBar [4]		= b5;

	m_nSpace [0]	= s1;
	m_nSpace [1]	= s2;
	m_nSpace [2]	= s3;
	m_nSpace [3]	= s4;

	m_nCheck		= nCheck;
	m_c				= c;
}

CPattern::CPattern (const CPattern & rhs)
{
	for (int i = 0; i < ARRAYSIZE (m_nBar); i++)
		m_nBar [i] = rhs.m_nBar [i];

	for (int i = 0; i < ARRAYSIZE (m_nSpace); i++)
		m_nSpace [i] = rhs.m_nSpace [i];

	m_nCheck	= rhs.m_nCheck;
	m_c			= rhs.m_c;
}

CPattern & CPattern::operator = (const CPattern & rhs)
{
	if (this != &rhs) {
		for (int i = 0; i < ARRAYSIZE (m_nBar); i++)
			m_nBar [i] = rhs.m_nBar [i];

		for (int i = 0; i < ARRAYSIZE (m_nSpace); i++)
			m_nSpace [i] = rhs.m_nSpace [i];

		m_nCheck	= rhs.m_nCheck;
		m_c			= rhs.m_c;
	}

	return * this;
}

CPattern::~CPattern ()
{
}

CString CPattern::ToString () const
{
	CString str;

	str.Format (_T ("%c: %c %c %c %c %c %c %c %c %c"),
		m_c,

		m_nBar		[0]		== 1 ? 'n' : 'w',
		m_nSpace	[0]		== 1 ? 'n' : 'w',

		m_nBar		[1]		== 1 ? 'n' : 'w',
		m_nSpace	[1]		== 1 ? 'n' : 'w',

		m_nBar		[2]		== 1 ? 'n' : 'w',
		m_nSpace	[2]		== 1 ? 'n' : 'w',

		m_nBar		[3]		== 1 ? 'n' : 'w',
		m_nSpace	[3]		== 1 ? 'n' : 'w',

		m_nBar		[4]		== 1 ? 'n' : 'w');

	return str;
}

////////////////////////////////////////////////////////////////////////////////

static const CPattern map [44] = 
{
    CPattern (n,  n,  n,  w,  w,  n,  w,  n,  n,		0 ,	'0'),
    CPattern (w,  n,  n,  w,  n,  n,  n,  n,  w,		1 ,	'1'),
    CPattern (n,  n,  w,  w,  n,  n,  n,  n,  w,		2 ,	'2'),
    CPattern (w,  n,  w,  w,  n,  n,  n,  n,  n,		3 ,	'3'),
    CPattern (n,  n,  n,  w,  w,  n,  n,  n,  w,		4 ,	'4'),
    CPattern (w,  n,  n,  w,  w,  n,  n,  n,  n,		5 ,	'5'),
    CPattern (n,  n,  w,  w,  w,  n,  n,  n,  n,		6 ,	'6'),
    CPattern (n,  n,  n,  w,  n,  n,  w,  n,  w,		7 ,	'7'),
    CPattern (w,  n,  n,  w,  n,  n,  w,  n,  n,		8 ,	'8'),
    CPattern (n,  n,  w,  w,  n,  n,  w,  n,  n,		9 ,	'9'),
    CPattern (w,  n,  n,  n,  n,  w,  n,  n,  w,		10,	'A'),
    CPattern (n,  n,  w,  n,  n,  w,  n,  n,  w,		11,	'B'),
    CPattern (w,  n,  w,  n,  n,  w,  n,  n,  n,		12,	'C'),
    CPattern (n,  n,  n,  n,  w,  w,  n,  n,  w,		13,	'D'),
    CPattern (w,  n,  n,  n,  w,  w,  n,  n,  n,		14,	'E'),
    CPattern (n,  n,  w,  n,  w,  w,  n,  n,  n,		15,	'F'),
    CPattern (n,  n,  n,  n,  n,  w,  w,  n,  w,		16,	'G'),
    CPattern (w,  n,  n,  n,  n,  w,  w,  n,  n,		17,	'H'),
    CPattern (n,  n,  w,  n,  n,  w,  w,  n,  n,		18,	'I'),
    CPattern (n,  n,  n,  n,  w,  w,  w,  n,  n,		19,	'J'),
    CPattern (w,  n,  n,  n,  n,  n,  n,  w,  w,		20,	'K'),
    CPattern (n,  n,  w,  n,  n,  n,  n,  w,  w,		21,	'L'),
    CPattern (w,  n,  w,  n,  n,  n,  n,  w,  n,		22,	'M'),
    CPattern (n,  n,  n,  n,  w,  n,  n,  w,  w,		23,	'N'),
    CPattern (w,  n,  n,  n,  w,  n,  n,  w,  n,		24,	'O'),
    CPattern (n,  n,  w,  n,  w,  n,  n,  w,  n,		25,	'P'),
    CPattern (n,  n,  n,  n,  n,  n,  w,  w,  w,		26,	'Q'),
    CPattern (w,  n,  n,  n,  n,  n,  w,  w,  n,		27,	'R'),
    CPattern (n,  n,  w,  n,  n,  n,  w,  w,  n,		28,	'S'),
    CPattern (n,  n,  n,  n,  w,  n,  w,  w,  n,		29,	'T'),
    CPattern (w,  w,  n,  n,  n,  n,  n,  n,  w,		30,	'U'),
    CPattern (n,  w,  w,  n,  n,  n,  n,  n,  w,		31,	'V'),
    CPattern (w,  w,  w,  n,  n,  n,  n,  n,  n,		32,	'W'),
    CPattern (n,  w,  n,  n,  w,  n,  n,  n,  w,		33,	'X'),
    CPattern (w,  w,  n,  n,  w,  n,  n,  n,  n,		34,	'Y'),
    CPattern (n,  w,  w,  n,  w,  n,  n,  n,  n,		35,	'Z'),
    CPattern (n,  w,  n,  n,  n,  n,  w,  n,  w,		36,	'-'),
    CPattern (w,  w,  n,  n,  n,  n,  w,  n,  n,		37,	'.'),
    CPattern (n,  w,  w,  n,  n,  n,  w,  n,  n,		38,	' '),
    CPattern (n,  w,  n,  n,  w,  n,  w,  n,  n,		-1,	'*'),
	CPattern (n,  w,  n,  w,  n,  w,  n,  n,  n,		39,	'$'),
    CPattern (n,  w,  n,  w,  n,  n,  n,  w,  n,		40,	'/'),
    CPattern (n,  w,  n,  n,  n,  w,  n,  w,  n,		41,	'+'),
    CPattern (n,  n,  n,  w,  n,  w,  n,  w,  n,		42,	'%'),
};

static const int nLookupTable [256] =
{
	-1, -1, -1, -1, -1, -1, -1, -1,  	// [0	- 7]
	-1, -1, -1, -1, -1, -1, -1, -1,  	// [8	- 15] 
	-1, -1, -1, -1, -1, -1, -1, -1,  	// [16	- 23] 
	-1, -1, -1, -1, -1, -1, -1, -1,  	// [24	- 31] 
	38, -1, -1, -1, 40, 43, -1, -1,  	// [32	- 39] 
	-1, -1, 39, 42, -1, 36, 37, 41,  	// [40	- 47] 
	0 , 1 , 2 , 3 , 4 , 5 , 6 , 7 ,  	// [48	- 55] 
	8 , 9 , -1, -1, -1, -1, -1, -1,  	// [56	- 63] 
	-1, 10, 11, 12, 13, 14, 15, 16,  	// [64	- 71] 
	17, 18, 19, 20, 21, 22, 23, 24,  	// [72	- 79] 
	25, 26, 27, 28, 29, 30, 31, 32,  	// [80	- 87] 
	33, 34, 35, -1, -1, -1, -1, -1,  	// [88	- 95] 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
};

static void GenerateLookupTable ()
{ 
	TRACEF (_T (""));

	for (int c = 0; c < 256; c++) {
		CString strTmp;
		int nIndex = -1;

		for (int i = 0; i < ARRAYSIZE (::map); i++) {
			if (::map [i].m_c == c) {
				nIndex = i;
				break;
			}
		}

		strTmp.Format (_T ("%-2d, "), nIndex); 
		::OutputDebugString (strTmp);

		if (((c + 1) % 8) == 0)
			::OutputDebugString (_T ("\n"));
	}

	::OutputDebugString (_T ("\n"));
}

////////////////////////////////////////////////////////////////////////////////

bool Code39::IsValidData (const CString & str)
{
	for (int i = 0; i < str.GetLength (); i++) {
		int nIndex = str [i];

		if (::nLookupTable [nIndex] == -1)
			return false; 
	}

	return true;
}

TCHAR Code39::CalcChecksum (const CString & str)
{
	int nSum = 0;

	for (int i = 0; i < str.GetLength (); i++) {
		int nIndex = str [i];
		int nLookup = ::nLookupTable [nIndex];

		ASSERT (nLookup != -1);
		int nCheck = ::map [nLookup].m_nCheck;

		nSum += nCheck;
	}

	int nDigit = nSum % 29;

	for (int i = 0; i < ARRAYSIZE (::nLookupTable); i++) {
		if (::nLookupTable [i] == nDigit)
			return i + 1;
	}

	ASSERT (0);
	return '*';
}

////////////////////////////////////////////////////////////////////////////////

int CBarcodeElement::DrawC39Symbol (const CString & str, const CString & strCaption, const CBarcodeParams & params, 
									const FoxjetDatabase::HEADSTRUCT & head, LPBYTE pBuffer, 
									UINT nBufferWidth, ULONG lBufferSize, CDC * pDC, 
									CBitmap * pbmpCaption,
									FoxjetCommon::DRAWCONTEXT context,
									CString * pstrFormattedCaption)
{
	int nWidth = 0;
	CString strData (str);
	CArray <CPattern, CPattern &> v;
	int nQuietzone = CBaseElement::ThousandthsToLogical (CPoint (params.GetQuietZone (), 0), head).x;
	CSize bearer = CBaseElement::ThousandthsToLogical (CPoint (params.GetVertBearer (), params.GetHorzBearer ()), head);
	double dStretch [2] = { 0, 0 };

	CBaseElement::CalcStretch (head, dStretch);
	bearer.cy = (int)((double)bearer.cy / dStretch [1]);

	nQuietzone = StretchToPrinterRes (nQuietzone, head);
	bearer.cx = StretchToPrinterRes (bearer.cx, head);

	if (Code39::IsValidData (strData)) {
		const int nBarHeight		= min ((int)head.GetChannels (), params.GetBarHeight ()); //params.GetBarHeight ();
		const BYTE nBarMask			= GetBarMask (nBarHeight);
		const BYTE nNotBarMask		= ~nBarMask;

		if (params.GetCheckSum ())
			strData += Code39::CalcChecksum (strData);

		v.Add (CPattern (::map [::nLookupTable ['*']]));

		for (int i = 0; i < strData.GetLength (); i++) 
			v.Add (CPattern (::map [::nLookupTable [strData [i]]]));

		v.Add (CPattern (::map [::nLookupTable ['*']]));

		nWidth += nQuietzone + bearer.cx; 

		for (int i = 0; i < v.GetSize (); i++) {
			CPattern p = v [i];
			struct
			{
				int		m_nWidth;
				bool	m_bBar;
			}
			const bars [] =
			{
				{ params.GetBarPixels	(p.m_nBar	[0]),		true,	},
				{ params.GetSpacePixels	(p.m_nSpace [0]),		false,	},
				{ params.GetBarPixels	(p.m_nBar	[1]),		true,	},
				{ params.GetSpacePixels	(p.m_nSpace [1]),		false,	},
				{ params.GetBarPixels	(p.m_nBar	[2]),		true,	},
				{ params.GetSpacePixels	(p.m_nSpace [2]),		false,	},
				{ params.GetBarPixels	(p.m_nBar	[3]),		true,	},
				{ params.GetSpacePixels	(p.m_nSpace [3]),		false,	},
				{ params.GetBarPixels	(p.m_nBar	[4]),		true,	},
				{ params.GetSpacePixels	(1),					false,	},
			};

			for (int nBar = 0; nBar < ARRAYSIZE (bars); nBar++) {
				int cx = bars [nBar].m_nWidth;

				if (bars [nBar].m_bBar) {
					for (int nCol = 0; nCol < cx; nCol++) { 
						int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
						int nBytes = nBarHeight / 8;

						if (pBuffer) {
							memset (&pBuffer [nIndex], 0, nBytes); 

							if ((ULONG)(nIndex + nBytes) < lBufferSize)
								memcpy (&pBuffer [nIndex + nBytes], &nBarMask, 1); 
						}
					}
				}

				nWidth += cx;
			}
		}

		nWidth += nQuietzone + bearer.cx; 
		
		if (pbmpCaption) 
			DrawCaption (* pbmpCaption, strCaption, params, head, pDC, context, nWidth);
	}

	if (pstrFormattedCaption)
		* pstrFormattedCaption = strCaption;

	return nWidth;
}

CSize CBarcodeElement::DrawC39 (CDC &dc, const FoxjetDatabase::HEADSTRUCT & head, 
								CSize & sizeLogical, FoxjetCommon::DRAWCONTEXT context, 
								bool bThrowException, const CBarcodeParams & params)
{
	CString str = GetImageData ();
	const CString strCaption = FormatCaption ();
	CSize bearer = CBaseElement::ThousandthsToLogical (CPoint (params.GetVertBearer (), params.GetHorzBearer ()), head);
	int nHeight = min ((int)head.GetChannels (), params.GetBarHeight ());
	int nWidth = 0;
	double dStretch [2] = { 0, 0 };

	CBaseElement::CalcStretch (head, dStretch);
	bearer.cy = (int)((double)bearer.cy / dStretch [1]);
	
	if (!Code39::IsValidData (str)) {
		if (bThrowException)
			throw new CBarcodeException (LoadString (IDS_INVALIDC39DATA));
		else
			str = FoxjetElements::GetDefaultData (kSymbologyCode39Standard, params);
	}

	nWidth = DrawC39Symbol (str, strCaption, params, head, NULL, 0, 0, NULL, NULL, context, &m_caption.m_strText);

	const LONG lRowLen = (int)ceil ((long double)nHeight / 16.0) * 16; // win bitmap is word aligned
	const int nBufferSize = lRowLen * nWidth / 8;
	LPBYTE pBuffer = new BYTE [nBufferSize];
	int x = 0;
	CBitmap bmp, bmpCaption;
	CDC dcMem;

	dcMem.CreateCompatibleDC (&dc);
	memset (pBuffer, ~0, nBufferSize);

	DrawC39Symbol (str, strCaption, params, head, pBuffer, lRowLen, nBufferSize, &dc, &bmpCaption, context, &m_caption.m_strText);
	Blt (dc, head, context, params, nWidth, nHeight, pBuffer, bmp, bmpCaption);

	delete [] pBuffer;

	if (context == PRINTER) 
		swap (nWidth, nHeight);

	CBitmap * pMem = dcMem.SelectObject (&bmp);
	dc.BitBlt (0, 0, nWidth, nHeight, &dcMem, 0, 0, !IsInverse () ? SRCINVERT : SRCCOPY);
	dcMem.SelectObject (pMem);

	DrawBearer (dc, bearer, context, params, nWidth, nHeight, head);

	CCoord c ((double)nWidth, (double)nHeight, INCHES, head);
	CSize size = m_size = CSize ((int)ceil (c.m_x * 1000.0), (int)ceil (c.m_y * 1000.0));

	sizeLogical = CSize (nWidth, nHeight);

	return size;
}
