// BitmappedFont.cpp: implementation of the CBitmappedFont class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "list.h"
#include "BitmappedFont.h"
#include "Debug.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBitmappedFont::CBitmappedFont (const CString & strName, const CString & strFilePath)
:	m_strName (strName),
	m_strFilePath (strFilePath),
	m_wVersion (0),
	m_wDots (0),
	m_wStart (0),
	m_wEnd (0),
	m_lLen (0),
	m_pIndex(NULL),
	m_pBuffer (NULL),
	m_pData (NULL),
	m_nWidthWords (0)
{
	CFile f;
	CFileException e;

	TRACEF (m_strFilePath);

	if (f.Open (m_strFilePath, CFile::modeRead, &e)) {
		m_lLen = f.GetLength();
		m_pBuffer = new UCHAR [m_lLen];

		UINT nBytes = f.Read (m_pBuffer, m_lLen);
		ASSERT (nBytes == m_lLen);
		f.Close();

		USHORT * pHeader = (USHORT *)m_pBuffer;

		m_wVersion	= * pHeader++;
		m_wDots		=  * pHeader++;
		m_wStart	=  * pHeader++;
		m_wEnd		=  * pHeader++;

		swab (m_wVersion);
		swab (m_wDots);
		swab (m_wStart);
		swab (m_wEnd);

		m_pIndex = (ULONG *)pHeader;
		
		if (ULONG * pIndex = (ULONG *)m_pIndex)  {
			for (USHORT i = m_wStart; i <= m_wEnd; i++) {
				swab (* pIndex);
				pIndex++;
			}

			m_pData = (USHORT *)pIndex;
		}

		int nWidthBits = (int)ceil ((long double)GetHeight () / 16.0) * 16; 
		m_nWidthWords = nWidthBits / 16;

		/* don't byte swap char data, windows already expects it swapped
		{ 
			USHORT * p = m_pData;
			int nEnd = GetOffset ((TCHAR)m_wEnd - 1);

			for (int n = 0; n < nEnd; n++) 
				for (int nWord = 0; nWord < m_nWidthWords; nWord++) 
					swab (* p++);
		}
		*/
	}
	else 
		e.ReportError (); 
}

CBitmappedFont::~CBitmappedFont()
{
	if (m_pBuffer) {
		delete [] m_pBuffer;
		m_pBuffer = NULL;
	}
}

int CBitmappedFont::GetOffset (TCHAR c) const
{
	int n = (int)c;

	if (n >= m_wStart && n <= m_wEnd) {
		int nIndex = n - m_wStart;
		
		return m_pIndex [nIndex];
	}

	return 0;
}


int CBitmappedFont::DrawChar (TCHAR c, PUCHAR lpBuffer) const
{
	int nLen = 0;

	if (c >= m_wStart && c <= m_wEnd) {
		int nIndex	= (TCHAR)(c - 1);
		int nOffset = GetOffset (nIndex);
		int nStart	= nOffset;
		int nEnd	= GetOffset (nIndex + 1) - 1;
		
		nLen = nEnd - nStart + 1;

		if (lpBuffer) {
			USHORT * pSrc = &m_pData [nStart * m_nWidthWords];
			USHORT * pDest = (USHORT *)lpBuffer;

			/*
			CString str;

			str.Format ("%c [%d] --> [%d, %d] [%d]", c, c, nStart, nEnd, nLen);
			TRACEF (str);
			*/

			for (int nCol = 0; nCol < nLen; nCol++) {
				/*
				CString str;

				str.Format ("[%-4d] ", nCol);
				*/

				for (int nWord = 0; nWord < m_nWidthWords; nWord++) {
					/*
					{ // TODO: rem
						USHORT n = * pSrc;
						swab (n);
						str += Format (n) + " "; 
					}
					*/

					* pDest = * pSrc;
					pDest++;
					pSrc++;
				}

				//::OutputDebugString (str + "\n");
			}
		}
	}

	return nLen;
}
