// IpBaseElement.cpp: implementation of the CIpBaseElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "IpElements.h"
//#include "List.h"
//#include "ListImp.h"
//#include "IpBaseElement.h"
//#include "Debug.h"
#include "fj_element.h"
#include "fj_defines.h"
#include "fj_printhead.h"
//#include "fj_export.h"
#include "fj_image.h"
#include "fj_font.h"
#include "fj_message.h"
#include "fj_system.h"
#include "fj_mem.h"
#include "fj_label.h"
//#include "Color.h"
//#include "Utils.h"
//#include "TemplExt.h"
//#include "AppVer.h"
#include "Debug.h"
#include "Database.h"
//#include "Parse.h"
#include "Extern.h"
#include "ximage.h"
#include "fj_dyntext.h"
#include "Parse.h"

#include <math.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetIpElements;
using namespace FoxjetCommon;
using namespace ListGlobals;

CArray <LPFJSYSTEM, LPFJSYSTEM> vIpSystems;

const LIMITSTRUCT CIpBaseElement::m_lmtBold		= { 0,	9			};
const LIMITSTRUCT CIpBaseElement::m_lmtWidth	= { 1,	9			};
const LIMITSTRUCT CIpBaseElement::m_lmtGap		= { 0,	255			};
const LIMITSTRUCT CIpBaseElement::m_lmtString	= { 0,	100			};

const CHAR fj_FontID[]				= "Font";
const CHAR fj_LabelID[]				= "Label";
const CHAR fj_BarCodeID[]			= "Barcode";
//const CHAR fj_DynBarCodeDefData[]	= "000000";
const CHAR fj_BarCodeFont32[]		= "barcode";
const CHAR fj_BarCodeFont256[]		= "barcode256";

extern "C" 
{
	FJDLLExport VOID fj_TextToStr( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_TextFromStr( LPFJELEMENT pfe, LPCSTR pStr );

	FJDLLExport VOID fj_BarCodeToStr( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_BarCodeFromStr( LPFJELEMENT pfe, LPSTR pStr );

	FJDLLExport VOID fj_BitmapToString( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_BitmapFromString( LPFJELEMENT pfe, LPBYTE pBuff );

	FJDLLExport VOID fj_DynimageToString( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_DynimageFromString( LPFJELEMENT pfe, LPBYTE pBuff );

	FJDLLExport VOID fj_DatetimeToStr( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_DatetimeFromStr( LPFJELEMENT pfe, LPCSTR pStr );

	FJDLLExport VOID fj_CounterToStr( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_CounterFromStr( LPFJELEMENT pfe, LPCSTR pStr );

	FJDLLExport VOID fj_DynTextToStr( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_DynTextFromStr( LPFJELEMENT pfe, LPCSTR pStr );

	FJDLLExport VOID fj_DynBarCodeToStr( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_DynBarCodeFromStr( LPFJELEMENT pfe, LPCSTR pStr );

	FJDLLExport VOID fj_DataMatrixToStr( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_DataMatrixFromStr( LPFJELEMENT pfe, LPCSTR pStr );
}

void BarCode_FromString (LPFJELEMENT pfe, LPCSTR pStr )
{
	CString str (pStr);
	fj_BarCodeFromStr (pfe, w2a (str));
}

void Bitmap_FromString (LPFJELEMENT pfe, LPCSTR pStr )
{
	CString str (pStr);
	//fj_BitmapFromString (pfe, (LPBYTE)str.GetBuffer (str.GetLength ()));
	fj_BitmapFromString (pfe, (LPBYTE)(char *)w2a (str));
}

LPPFROMSTRINGFCT ELEMENT_API FoxjetIpElements::GetFromStringFct (FoxjetIpElements::IPELEMENTTYPE type)
{
	switch (type) {
	case FoxjetIpElements::TEXT:			return fj_TextFromStr;
	case FoxjetIpElements::BARCODE:			return BarCode_FromString;
	case FoxjetIpElements::BMP:				return Bitmap_FromString;
	case FoxjetIpElements::DATETIME:		return fj_DatetimeFromStr;
	case FoxjetIpElements::COUNT:			return fj_CounterFromStr;
	case FoxjetIpElements::DYNAMIC_TEXT:	return fj_DynTextFromStr;
	case FoxjetIpElements::DYNAMIC_BARCODE:	return fj_DynBarCodeFromStr;
	case FoxjetIpElements::DATAMATRIX:		return fj_DataMatrixFromStr;
	}

	ASSERT (0);
	return NULL;
}

LPPTOSTRINGFCT ELEMENT_API FoxjetIpElements::GetToStringFct (FoxjetIpElements::IPELEMENTTYPE type)
{
	switch (type) {
	case FoxjetIpElements::TEXT:			return fj_TextToStr;
	case FoxjetIpElements::BARCODE:			return fj_BarCodeToStr;
	case FoxjetIpElements::BMP:				return fj_BitmapToString;
	case FoxjetIpElements::DATETIME:		return fj_DatetimeToStr;
	case FoxjetIpElements::COUNT:			return fj_CounterToStr;
	case FoxjetIpElements::DYNAMIC_TEXT:	return fj_DynTextToStr;
	case FoxjetIpElements::DYNAMIC_BARCODE:	return fj_DynBarCodeToStr;
	case FoxjetIpElements::DATAMATRIX:		return fj_DataMatrixToStr;
	}

	ASSERT (0);
	return NULL;
}

bool CIpBaseElement::GetFonts (LPCFJPRINTHEAD pHead, CArray <CPrinterFont, CPrinterFont &> &v)
{
	using namespace FoxjetDatabase;

	HEADSTRUCT dbHead;

	VERIFY (FoxjetDatabase::GetHeadRecord (GetDB (), atol (pHead->strID), dbHead));
	const int nMax = dbHead.GetChannels ();

	v.RemoveAll ();

	if (pHead->papffSelected) {
		for (int i = 0; pHead->papffSelected [i]; i++) {
			LPFJFONT pFont = pHead->papffSelected [i];

			// only submit fonts that can fit on this head
			if (pFont->lHeight <= nMax)
				v.Add (CPrinterFont (pFont->strName));
		}
	}

	return v.GetSize () > 0;
}

LPFJSYSTEM FoxjetIpElements::GetSystem (ULONG lLineID) 
{
	for (int i = 0; i < ::vIpSystems.GetSize (); i++) 
		if ((ULONG)atol (::vIpSystems [i]->strID) == lLineID)
			return ::vIpSystems [i];

	return NULL;
}

void ELEMENT_API FoxjetIpElements::InitIpElements () 
{
	using namespace FoxjetDatabase;

	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	int nHeads = 0;
	COdbcDatabase & db = ListGlobals::GetDB ();

	FoxjetIpElements::CHead::InitFonts ();

	if (GetLineRecords (db, vLines)) {
		for (int i = 0; i < vLines.GetSize (); i++) {
			LINESTRUCT & line = vLines [i];
			LPFJSYSTEM pSystem = fj_SystemNew ();
			CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

			ASSERT (pSystem);
			::vIpSystems.Add (pSystem);
			sprintf (pSystem->strID, "%d", line.m_lID);

			FoxjetDatabase::GetLineHeads (db, vLines [i].m_lID, vHeads);

			for (int j = 0; j < min (vHeads.GetSize (), FJSYS_NUMBER_PRINTERS); j++, nHeads++) {
				LPFJPRINTHEAD pHead = NULL;
				CString strID;

				VERIFY (pHead = pSystem->gmMembers [j].pfph = fj_PrintHeadNew ());
				FoxjetIpElements::CHead::SetFonts (pHead);
				strID.Format (_T ("%d"), vHeads [j].m_lID);
				strncpy (pSystem->gmMembers [i].strID, w2a (strID), FJPH_ID_SIZE);
				strncpy (pHead->strID, w2a (strID), FJPH_ID_SIZE);
				pHead->pfsys = pSystem;
			}
			
			VERIFY (pSystem->pfl = fj_LabelNew ());
		}
	}

	fj_initWinDynData ();
}

void ELEMENT_API FoxjetIpElements::FreeIpElements ()
{
	for (int i = 0; i < ::vIpSystems.GetSize (); i++) {
		if (LPFJSYSTEM pSystem = ::vIpSystems [i]) {
			for (int j = 0; j < FJSYS_NUMBER_PRINTERS; j++) {
				if (LPFJPRINTHEAD pHead = pSystem->gmMembers [j].pfph) {
					FoxjetIpElements::CHead::FreeFonts (pHead);
					fj_PrintHeadDestroy (pHead);
					pSystem->gmMembers [j].pfph = NULL;
				}
			}

			fj_SystemDestroy (pSystem);
		}
	}
}

IMPLEMENT_DYNAMIC (CIpBaseElement, CBaseTextElement);

CIpBaseElement::CIpBaseElement(LPFJELEMENT pElement, LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head)
:	m_pElement (pElement),
	m_pFont (NULL),
	m_pHead (pHead),
	m_pBmp (NULL),
	m_lLastBold (0),
	m_lastContext (EDITOR),
	m_pLabel (NULL),
	CBaseTextElement (head)
{
	if (HasFont ()) {
		if ((m_pFont = fj_PrintHeadBuildFont (pHead, "fx32x22")) == NULL) {
			CArray <CPrinterFont, CPrinterFont &> v;

			/*CHead::*/GetFonts (pHead, v);
			
			// if anything was loaded, take the first font
			if (v.GetSize ()) {
				CString strDef = v [0].m_strName;

				VERIFY (m_pFont = fj_PrintHeadBuildFont (pHead, w2a (strDef)));
			}
		}
	}

	ASSERT (m_pElement);
	ASSERT (m_pHead);

	if (::vIpSystems.GetSize ()) {
		m_lLineID = atoi (::vIpSystems [0]->strID);
	}
}

CIpBaseElement::CIpBaseElement (const CIpBaseElement & rhs)
:	CBaseTextElement (rhs),
	m_pHead (rhs.m_pHead),
	m_pFont (rhs.m_pFont),
	m_pElement (fj_ElementDup (rhs.m_pElement)),
	m_pBmp (NULL),
	m_lastContext (EDITOR),
	m_pLabel (rhs.m_pLabel),
	m_lLastBold (rhs.m_lLastBold)
{
	ASSERT (m_pElement);
}

CIpBaseElement & CIpBaseElement::operator = (const CIpBaseElement & rhs)
{
	CBaseTextElement::operator = (rhs);

	if (this != &rhs) {
		Invalidate ();
		m_pHead			= rhs.m_pHead;
		m_pFont			= rhs.m_pFont;
		m_lLastBold		= rhs.m_lLastBold;
		m_lastContext	= EDITOR;
		VERIFY (SetElement (fj_ElementDup (rhs.m_pElement)));
		VERIFY (SetLabel (rhs.m_pLabel));
	}

	return * this;
}

CIpBaseElement::~CIpBaseElement()
{
	SetElement (NULL);
}

LPFJELEMENT CIpBaseElement::GetElement ()
{
	ASSERT (m_pElement);
	return m_pElement;
}

const LPFJELEMENT CIpBaseElement::GetElement () const
{
	ASSERT (m_pElement);
	return m_pElement;
}

LPCFJLABEL CIpBaseElement::GetLabel () const
{
	return m_pLabel;
}

bool CIpBaseElement::SetLabel (LPFJLABEL pNew)
{
	Invalidate ();

	m_pLabel = pNew;
	return true;
}

bool CIpBaseElement::SetElement (LPFJELEMENT pNew)
{
	if (m_pElement && m_pElement != pNew) {
		fj_ElementDestroy (m_pElement);
		m_pElement = NULL;
	}

	Invalidate ();

	if (pNew) {
		m_pElement = pNew;
		return true;
	}

	return false;
}

CString CIpBaseElement::ToString (const FoxjetCommon::CVersion & ver) const
{
	LPPTOSTRINGFCT lpToString = GetToStringFct ((IPELEMENTTYPE)GetClassID ());
	char sz [FJTEXT_MEMSTOR_SIZE] = { 0 };

	ASSERT (lpToString);
	(* lpToString) (GetElement (), sz);
	//TRACEF (Format ((const LPBYTE)sz, strlen (sz)));

	return a2w (sz);
}

bool CIpBaseElement::FromString (const CString & strSrc, const FoxjetCommon::CVersion & ver)
{
	LPPFROMSTRINGFCT lpFromString = GetFromStringFct ((IPELEMENTTYPE)GetClassID ());
	LPFJELEMENT pElement = GetElement ();
	CString str (strSrc);

	ASSERT (lpFromString);
	(* lpFromString) (pElement, w2a (str));
	//TRACEF (Format (str, str.GetLength ()));

	// since this uses m_pFont, get the ip element structure's font to get the right name
	SetFontName (GetElementFontName ());
	SetElement (pElement);

	return true;
}

int CIpBaseElement::GetClassID () const
{
	switch (GetType ()) {
	case FJ_TYPE_TEXT:			return TEXT;
	case FJ_TYPE_BARCODE:		return BARCODE;
	case FJ_TYPE_BITMAP:		return BMP;
	case FJ_TYPE_DATETIME:		return DATETIME;
	case FJ_TYPE_COUNTER:		return COUNT;
	case FJ_TYPE_DYNTEXT:		return DYNAMIC_TEXT;
	case FJ_TYPE_DYNBARCODE:	return DYNAMIC_BARCODE;
	case FJ_TYPE_DATAMATRIX:	return DATAMATRIX;
	}

	ASSERT (0);

	return UNKNOWN;
}

LPVOID CIpBaseElement::New ()
{
	if (LPFJELEMENT pElement = GetElement ())
		if (pElement->pActions)
			return pElement->pActions->fj_DescNew ();

	return NULL;
}

LPVOID CIpBaseElement::Dup (LPVOID pfd)
{
	if (LPFJELEMENT pElement = GetElement ())
		if (pElement->pActions)
			return pElement->pActions->fj_DescDup (pElement);

	return NULL;
}

void CIpBaseElement::Destroy ()
{
	if (LPFJELEMENT pElement = GetElement ()) {
		if (pElement->pActions) {
			pElement->pActions->fj_DescDestroy (pElement);
			m_pElement = NULL;
		}
	}
}

void CIpBaseElement::CreateImage () 
{
	if (LPFJELEMENT pElement = GetElement ()) {
		if (pElement->pActions) {
			LONG lRow = pElement->lRow;
			FLOAT fLeft = pElement->fLeft;
			LPCFJPRINTHEAD pHead = GetHead ();
			ULONG lTransforms = 0;
			FLOAT fEncoder = 300.0;
			LONG lEncoderDivisor = 1;

			pElement->lRow = 0;
			pElement->fLeft = 0.0;

//			if (m_pElement && m_pElement->pfm && m_pElement->pfm) {
			if (m_pElement && m_pElement->pfm && m_pElement->pfm->pfph) {
				if (LPFJSYSTEM pSystem = m_pElement->pfm->pfph->pfsys) {
					const FoxjetDatabase::HEADSTRUCT & head = CBaseElement::GetHead ();
					const float fPrintRes = CalcPrintRes (head);

					fEncoder					= pSystem->fEncoder;
					lEncoderDivisor				= pSystem->lEncoderDivisor;

					pSystem->fEncoder			= (float)head.m_nHorzRes;
					pSystem->lEncoderDivisor	= head.m_nEncoderDivisor;

#ifdef __WYSIWYGFIX__
					pSystem->fPrintResolution = pSystem->fEncoder = fPrintRes; 
#endif
				}

				if (m_pElement->pfm->pfph->pfphy)
					m_pElement->pfm->pfph->pfphy->lChannels = (m_head).GetChannels ();
			}

			pElement->pActions->fj_DescCreateImage (pElement);

			if (pElement->pfi->lHeight < 8)
				pElement->pfi->lHeight = 8;

			ASSERT (pHead);
			ASSERT (pElement->pfi);
			ASSERT (AfxIsValidAddress (pElement->pfi, sizeof (FJIMAGE)));

			pElement->pfi->lTransforms = FJ_TRANS_NORMAL;
			
			if (IsFlippedH ())	lTransforms |= FJ_TRANS_REVERSE;
			if (IsFlippedV ())	lTransforms |= FJ_TRANS_INVERSE;
			if (IsInverse ())	lTransforms |= FJ_TRANS_NEGATIVE;

			if (pElement->pfi->lHeight && pElement->pfi->lLength)
				pElement->pfi = fj_ImageRasterTransform (pElement->pfi, lTransforms);


			// restore old params
			pElement->lRow = lRow;
			pElement->fLeft = fLeft;
			
			if (m_pElement && m_pElement->pfm && m_pElement->pfm) {
				if (LPFJSYSTEM pSystem = m_pElement->pfm->pfph->pfsys) {
					const FoxjetDatabase::HEADSTRUCT & head = CBaseElement::GetHead ();

					pSystem->fEncoder			= fEncoder;
					pSystem->lEncoderDivisor	= lEncoderDivisor;
				}
			}
		}
	}
}

void CIpBaseElement::PhotocellBuild () const
{
	if (LPFJELEMENT pElement = GetElement ())
		if (pElement->pActions)
			pElement->pActions->fj_DescPhotocellBuild (pElement);
}

long CIpBaseElement::GetType () const
{
	if (LPFJELEMENT pElement = GetElement ())
		if (pElement->pActions)
			return pElement->pActions->fj_DescGetType ();

	return UNKNOWN;
}

CString CIpBaseElement::GetTypeString () const
{
	if (LPFJELEMENT pElement = GetElement ())
		if (pElement->pActions)
			return pElement->pActions->fj_DescGetTypeString ();

	return _T ("");
}

bool CIpBaseElement::SetPos (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT * pHead, ALIGNMENT_ADJUST adj)
{
	using namespace FoxjetDatabase;

	if (IsMovable ()) {
		if (LPFJELEMENT p = GetElement ()) {

			p->fLeft = (float)pt.x;
			p->lRow = pt.y;

			// ip coordinates are as follows:
			//	x: feet, distance from left of product area to the left side of the element
			//		ie, 1 inch = 0.0833...
			//	y: pixels (nozzles), distance from bottom nozzle to bottom of element
			//		ie, 0 aligns the bottom of the element with the bottom of the head

 
			if (pHead) {
				double dStretch [2] = { 1.0, 1.0 };
				const CPoint ptPrinter = CBaseElement::ThousandthsToLogical (pt, * pHead); 

				CBaseElement::CalcStretch (* pHead, dStretch);
				p->fLeft = (float)pt.x / 1000.0F;

				//p->lRow = (int)((double)ptPrinter.y / dStretch [1]);
				p->lRow = (int)DivideAndRound ((double)ptPrinter.y, dStretch [1]);

				if (p->pfi) { 
					const CSize size = CBaseElement::ThousandthsToLogical (GetSize (* pHead), * pHead);
					
					p->lRow = pHead->GetChannels () - (p->lRow + size.cy);
					p->lRow = BindTo (p->lRow, 0L, (LONG)(pHead->GetChannels () - 1));

					if (p->lRow <= 1)
						p->lRow = 0; 

					AdjustYPos (p, * pHead);

					if (p->fLeft < 0)
						p->fLeft = 0; 
				}
				else {
					TRACEF ("SetPos: Warning: element hasn't been imaged yet, pos may be incorrect");
				}
			}

			return CBaseElement::SetPos (pt, pHead, adj); 
		}
	}

	return false;
}

bool CIpBaseElement::SetXPos (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT * pHead)
{
//	return CIpBaseElement::SetXPos (pt, pHead);
	if (IsMovable ()) {
		if (LPFJELEMENT p = GetElement ()) {

			p->fLeft = (float)pt.x;

			// ip coordinates are as follows:
			//	x: feet, distance from left of product area to the left side of the element
			//		ie, 1 inch = 0.0833...
			//	y: pixels (nozzles), distance from bottom nozzle to bottom of element
			//		ie, 0 aligns the bottom of the element with the bottom of the head

 
			if (pHead) {
				double dStretch [2] = { 1.0, 1.0 };
				const CPoint ptPrinter = CBaseElement::ThousandthsToLogical (pt, * pHead); 

				CBaseElement::CalcStretch (* pHead, dStretch);
				p->fLeft = (float)pt.x / 1000.0F;

				if (p->fLeft < 0)
					p->fLeft = 0; 
			}

			return CBaseElement::SetXPos (pt, pHead); 
		}
	}

	return false;
}

bool CIpBaseElement::SetYPos (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	using namespace FoxjetDatabase;

//	return CIpBaseElement::SetYPos (pt, pHead);
	if (IsMovable ()) {
		if (LPFJELEMENT p = GetElement ()) {

			p->lRow = pt.y;

			// ip coordinates are as follows:
			//	x: feet, distance from left of product area to the left side of the element
			//		ie, 1 inch = 0.0833...
			//	y: pixels (nozzles), distance from bottom nozzle to bottom of element
			//		ie, 0 aligns the bottom of the element with the bottom of the head

 
			if (pHead) {
				double dStretch [2] = { 1.0, 1.0 };
				const CPoint ptPrinter = CBaseElement::ThousandthsToLogical (pt, * pHead); 

				CBaseElement::CalcStretch (* pHead, dStretch);

				//p->lRow = (int)((double)ptPrinter.y / dStretch [1]);
				p->lRow = (int)DivideAndRound ((double)ptPrinter.y, dStretch [1]);

				if (p->pfi) { 
					const CSize size = CBaseElement::ThousandthsToLogical (GetSize (* pHead), * pHead);
					
					p->lRow = pHead->GetChannels () - (p->lRow + size.cy);
					p->lRow = BindTo (p->lRow, 0L, (LONG)(pHead->GetChannels () - 1));

					if (p->lRow <= 1)
						p->lRow = 0; 

					AdjustYPos (p, * pHead);
				}
				else {
					TRACEF ("SetPos: Warning: element hasn't been imaged yet, pos may be incorrect");
				}
			}

			return CBaseElement::SetYPos (pt, pHead); 
		}
	}

	return false;
}

CPoint CIpBaseElement::GetPos (const FoxjetDatabase::HEADSTRUCT * pHead) const
{
	CPoint pt (0, 0);

	if (LPFJELEMENT p = GetElement ()) {

		pt.x = (int)p->fLeft;
		pt.y = p->lRow;

		if (pHead) {
			double dStretch [2] = { 1.0, 1.0 };

			CBaseElement::CalcStretch (* pHead, dStretch);
			const CPoint ptPrinter = CBaseElement::LogicalToThousandths (pt, * pHead); 

			pt.x = (int)(p->fLeft * 1000.0F);
			pt.y = (int)((double)ptPrinter.y * dStretch [1]);

			if (p->pfi) { 
				const CSize size = CBaseElement::ThousandthsToLogical (GetSize (* pHead), * pHead);
				int y = pHead->GetChannels () - (p->lRow + size.cy);

				pt.y = CBaseElement::LogicalToThousandths (CPoint (0, y), * pHead).y;

				pt.y = (int)((double)pt.y * dStretch [1]);
			}
			else {
				TRACEF ("GetPos: Warning: element hasn't been imaged yet, pos may be incorrect");
			}
		}
	}

	return pt; 
}

bool CIpBaseElement::Move (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT & head)
{
	if (LPFJELEMENT p = GetElement ()) {
		double dStretch [2] = { 1.0, 1.0 };
		const CPoint ptPrinter = CBaseElement::ThousandthsToLogical (pt, head); 
		CPoint ptElement = GetPos (NULL);

		CBaseElement::CalcStretch (head, dStretch);
		p->fLeft += (float)pt.x / 1000.0F;
		p->lRow -= (int)((double)ptPrinter.y / dStretch [1]);

		AdjustYPos (p, head);

		return CBaseElement::SetPos (GetPos (&head), &head); 
	}

	return false;
}

bool CIpBaseElement::IsFlippedH () const
{
	return GetElement ()->lTransform & FJ_TRANS_REVERSE ? true : false; 
}

bool CIpBaseElement::SetFlippedH (bool bFlip)
{
	if (bFlip)
		GetElement ()->lTransform |= FJ_TRANS_REVERSE;
	else
		GetElement ()->lTransform &= ~FJ_TRANS_REVERSE;

	Invalidate ();

	return true;
}

bool CIpBaseElement::IsFlippedV () const
{
	return GetElement ()->lTransform & FJ_TRANS_INVERSE ? true : false; 
}

bool CIpBaseElement::SetFlippedV (bool bFlip)
{
	if (bFlip)
		GetElement ()->lTransform |= FJ_TRANS_INVERSE;
	else
		GetElement ()->lTransform &= ~FJ_TRANS_INVERSE;

	Invalidate ();

	return true;
}

bool CIpBaseElement::IsInverse () const
{
	return GetElement ()->lTransform & FJ_TRANS_NEGATIVE ? true : false; 
}

bool CIpBaseElement::SetInverse (bool bInverse)
{
	if (bInverse)
		GetElement ()->lTransform |= FJ_TRANS_NEGATIVE;
	else
		GetElement ()->lTransform &= ~FJ_TRANS_NEGATIVE;

	Invalidate ();

	return true;
}

UINT CIpBaseElement::GetID () const
{
	return atol (GetElement ()->strName);
}

bool CIpBaseElement::SetID (UINT nID) 
{
	CString str;
	str.Format (_T ("%d"), nID);
	strncpy (GetElement ()->strName, w2a (str), FJELEMENT_NAME_SIZE);
	return true;
}

bool CIpBaseElement::HasFont () const
{
	switch (GetType ()) {
	case FJ_TYPE_TEXT:
	case FJ_TYPE_DATETIME:
	case FJ_TYPE_COUNTER:
	case FJ_TYPE_DYNTEXT:
		return true;
	}

	return false;
}

bool CIpBaseElement::SetFont (const FoxjetCommon::CPrinterFont & f)
{
	if (HasFont () && m_pHead) {
		for (int i = 0; m_pHead->papffSelected [i]; i++) {
			LPFJFONT pFont = m_pHead->papffSelected [i];

			if (pFont->strName == f.m_strName) {
				if (LONG lBold = GetBold ()) 
					m_lLastBold = lBold;

				m_pFont = pFont;
				SetBold (f.m_bBold ? max (m_lLastBold, 1) : 0);
				Invalidate ();

				return true;
			}
		}
	}

	return false;
}

bool CIpBaseElement::GetFont (FoxjetCommon::CPrinterFont & f) const
{
	if (HasFont ()) {
		if (m_pFont) {
			f.m_strName		= m_pFont->strName;
			f.m_nSize		= m_pFont->lHeight;
			f.m_bBold		= GetBold () ? true : false;
			f.m_bItalic		= false;

			return true;
		}
	}

	return false;
}

LONG CIpBaseElement::GetBold () const
{
	return 0;
}

bool CIpBaseElement::SetBold (LONG l)
{
	return false;
}

CSize CIpBaseElement::Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly, FoxjetCommon::DRAWCONTEXT context)
{
	DIBSECTION ds;

	if (m_lastContext != context) {
		Invalidate ();
		m_lastContext = context;
	}

	if (!m_pBmp) 
		m_pBmp = CreateBitmap (dc, context);

	if (!m_pBmp) {
		dc.TextOut (0, 0, "CreateBitmap failed");
		ASSERT (0);
		return CSize (0, 0);
	}

	::ZeroMemory (&ds, sizeof (ds));
	int nGetObject = m_pBmp->GetObject (sizeof (DIBSECTION), &ds);
	ASSERT (nGetObject);
	
	if (context == EDITOR) {
		if (!bCalcSizeOnly) {
			CDC dcMem;
			CSize logical = CBaseElement::ThousandthsToLogical (GetSize (head), head);
			int x = 0, y = 0, cx = logical.cx, cy = logical.cy;

			VERIFY (dcMem.CreateCompatibleDC (&dc));
			CGdiObject * pOld = dcMem.SelectObject (m_pBmp);

			/*
			#ifdef __WYSIWYGFIX__
			if (CBitmap * p = dc.GetCurrentBitmap ()) { 
				DIBSECTION ds;

				::ZeroMemory (&ds, sizeof (ds));
				VERIFY (p->GetObject (sizeof (DIBSECTION), &ds));
				cx = ds.dsBm.bmWidth;
				cy = ds.dsBm.bmHeight;
			}
			#endif
			*/ 
			
			cx = (int)((double)cx * ((double)dc.GetDeviceCaps (LOGPIXELSX) / head.GetRes ()));

			dc.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, SRCCOPY);

			dcMem.SelectObject (pOld);
		}

		return GetSize (head);
	}
	else {
		if (!bCalcSizeOnly) {
			CDC dcMem;
			int x = 0, y = 0, cx = ds.dsBm.bmWidth, cy = ds.dsBm.bmHeight;
			CxImage img;

			dcMem.CreateCompatibleDC (&dc);
			CGdiObject * pOld = dcMem.SelectObject (m_pBmp);

			::BitBlt (dc, x, y, cx, cy, NULL, 0, 0, WHITENESS);
			dc.BitBlt (x, y, cx, cy, &dcMem, 0, 0, SRCINVERT);

			VERIFY (img.CreateFromHBITMAP ((HBITMAP)m_pBmp->m_hObject));
			VERIFY (img.Mirror ());
			VERIFY (img.Negative ());
			HBITMAP hbmp = img.MakeBitmap (dc);
			::SelectObject (dcMem, hbmp);
			dc.BitBlt (x, y, cx, cy, &dcMem, 0, 0, SRCCOPY);
			::DeleteObject (hbmp);

			dcMem.SelectObject (pOld);
		}

		return CSize (ds.dsBm.bmWidth, ds.dsBm.bmHeight);
	}
}

CSize CIpBaseElement::GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI) const
{
	CSize size (0, 0);

	if (LPFJELEMENT pElement = GetElement ()) {
		if (pElement->pfi) {
			/*
			HDC hdc = ::GetDC (NULL);
			const int nDPI = ::GetDeviceCaps (hdc, LOGPIXELSX);

			//const double dFactor = ((double)nDPI / (double)head.m_nHorzRes) / (double)head.m_nEncoderDivisor;
			//const double dFactor = (double)nDPI / (double)head.m_nHorzRes;
			const double dFactor = (double)nDPI / head.GetRes ();
			// scaling factor to compensate for head & screen resolution

			::ReleaseDC (NULL, hdc);
			*/

			size.cx = (int)((double)pElement->pfi->lLength / ceil ((double)pElement->pfi->lHeight / 8.0));
			size.cy = pElement->pfi->lHeight;
		}
		else {
			TRACEF ("Warning: element hasn't been imaged yet, size will be incorrect");
		}
	}

	CSize size1000s = LogicalToThousandths (size, m_head);

	return size1000s;
}

CSize CIpBaseElement::GetImageSize (LPCFJIMAGE pfi)
{
	ASSERT (pfi);

	int cx = (int)((double)pfi->lLength / ceil ((double)pfi->lHeight / 8.0));
	int cy = pfi->lHeight;

	return CSize (cx, cy);
}

LPBYTE CIpBaseElement::ImageIpToWinRotate (LPCFJIMAGE pfi, LPBYTE pSrcBuffer, CSize & size)
{
	ASSERT (pfi);
	ASSERT (pSrcBuffer);

	LONG lHeight = pfi->lHeight;
	LONG lLength = pfi->lLength; 
	LONG lLen = lLength * 8;
	int cx = GetImageSize (pfi).cx;
	int cy = GetImageSize (pfi).cy;
	LONG lRowLen = (int)ceil ((long double)cx / 16.0) * 16; // win bitmap is word aligned
	LONG lBufferLen = lRowLen * cy;
	LPBYTE pBuffer = new BYTE [lBufferLen];
	int x = 0, y = 0;

	size.cx = cx;
	size.cy = cy;

	memset (pBuffer, ~0, lBufferLen);

	for (LONG lSrc = 0; lSrc < lLen; lSrc++) {
		bool bBit = GetPixel (lSrc, pSrcBuffer);
		int nOffset = (lRowLen * y) / 8;

		SetPixel (x, &pBuffer [nOffset], bBit);

		y++;

		if (y > (cy - 1)) {
			// IP scan lines are on a one byte boundary
			lSrc = (LONG)((ceil ((long double)(lSrc + 1.0) / 8.0)) * 8) - 1;

			y = 0;
			x++;
		}
	}

	return pBuffer;
}

LPBYTE CIpBaseElement::ImageIpToWin (LPCFJIMAGE pfi, LPBYTE pSrcBuffer, CSize & size)
{
	ASSERT (pfi);
	ASSERT (pSrcBuffer);

	LONG lHeight = pfi->lHeight;
	LONG lLength = pfi->lLength; 
	LONG lLen = lLength * 8;
	int cx = GetImageSize (pfi).cx;
	int cy = GetImageSize (pfi).cy;
	LONG lRowLen = (int)ceil ((long double)cx / 16.0) * 16; // win bitmap is word aligned
	LONG lBufferLen = lRowLen * cy;
	LPBYTE pBuffer = new BYTE [lBufferLen];
	int x = 0, y = 0;

	size.cx = cy;
	size.cy = cx;

	memset (pBuffer, ~0, lBufferLen);

	for (LONG lSrc = 0, lDest = 0; lSrc < lLen; lSrc++, lDest++) {
		bool bBit = GetPixel (lSrc, pSrcBuffer);

		SetPixel (lDest, pBuffer, bBit);

		y++;

		if (y > (cy - 1)) {
			// IP scan lines are on a one byte boundary
			lSrc = (LONG)((ceil ((long double)(lSrc + 1.0) / 8.0)) * 8) - 1;

			// win bitmap scan lines are on a word boundary
			lDest = (LONG)((ceil ((long double)(lDest + 1.0) / 16.0)) * 16) - 1;

			y = 0;
			x++;
		}
	}

	return pBuffer;
}

CBitmap * CIpBaseElement::CreateBitmap (CDC & dc, FoxjetCommon::DRAWCONTEXT context)
{
	if ((HasFont () && !m_pFont)|| !m_pHead) {
		#ifdef _DEBUG
		CString str;
		str.Format (_T ("m_pFont = 0x%p, m_pHead= 0x%p"), m_pFont, m_pHead);
		TRACEF (str);
		#endif _DEBUG

		return NULL;
	}

	LPFJELEMENT pElement = GetElement ();

	pElement->pfm				= fj_MessageNew ();
	pElement->pfm->pfph			= m_pHead;
	pElement->pfm->pfph->pfsys	= GetSystem ();
	pElement->pfm->pfl			= m_pLabel;

	Validate ();
	CreateImage ();

	//#ifdef _DEBUG
	//if (!pElement->pfi)
	//	CreateImage ();
	//#endif

	//ASSERT (pElement->pfi);
	
	/*
	#ifdef _DEBUG
	{
		CSize sizeRot (0, 0);
		CBitmap bmp;
		const PUCHAR pSrcBuffer = (LPBYTE)pElement->pfi + sizeof(FJIMAGE);

		LPBYTE pDestBuffer = ImageIpToWin (pElement->pfi, pSrcBuffer, sizeRot);

		if (bmp.CreateBitmap (sizeRot.cx, sizeRot.cy, 1, 1, pDestBuffer)) {
			SAVEBITMAP (bmp, _T ("C:\\Foxjet\\Debug\\ImageIpToWin.bmp"));
		}
	}
	{
		CSize sizeRot (0, 0);
		CBitmap bmp;
		const PUCHAR pSrcBuffer = (LPBYTE)pElement->pfi + sizeof(FJIMAGE);

		LPBYTE pDestBuffer = ImageIpToWinRotate (pElement->pfi, pSrcBuffer, sizeRot);

		if (bmp.CreateBitmap (sizeRot.cx, sizeRot.cy, 1, 1, pDestBuffer)) {
			SAVEBITMAP (bmp, _T ("C:\\Foxjet\\Debug\\ImageIpToWinRotate.bmp"));
		}
	}
	#endif // _DEBUG
	*/

	CBitmap * pBmp = new CBitmap ();
		
	if (!pElement || !pElement->pfi || !pElement->pfi->lHeight || !pElement->pfi->lLength) {
		CDC dcMem;
		const int nSize = 32;

		dcMem.CreateCompatibleDC (&dc);
		pBmp->CreateCompatibleBitmap (&dc, nSize * 2, nSize);

		CBitmap * pMem = dcMem.SelectObject (pBmp);

		::BitBlt (dcMem, 0, 0, nSize, nSize, NULL, 0, 0, WHITENESS);
		dcMem.TextOut (0, 0, "Invalid");

		dcMem.SelectObject (pMem);
		return pBmp;
	}

	const PUCHAR pSrcBuffer = (LPBYTE)pElement->pfi + sizeof(FJIMAGE);

	if (context == EDITOR) {
		CSize sizeRot (0, 0);
		CBitmap bmp;

		LPBYTE pDestBuffer = ImageIpToWinRotate (pElement->pfi, pSrcBuffer, sizeRot);

		if (!bmp.CreateBitmap (sizeRot.cx, sizeRot.cy, 1, 1, pDestBuffer)) {

			CDC dcMem;
			CRect rc (0, 0, 0, 0);
			CString strError = LoadString (IDS_INVALIDDATA);

			dcMem.CreateCompatibleDC (&dc);
			dcMem.DrawText (strError, &rc, DT_CALCRECT);

			pBmp->CreateCompatibleBitmap (&dcMem, rc.Width (), rc.Height ());
			CBitmap * pMem = dcMem.SelectObject (pBmp);

			::BitBlt (dcMem, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
			dcMem.DrawText (strError, &rc, 0);

			dcMem.SelectObject (pMem);
		}
		else {
			CSize size = sizeRot;
			CDC dcMem, dcElement;

			dcMem.CreateCompatibleDC (&dc);
			dcElement.CreateCompatibleDC (&dc);

			pBmp->CreateCompatibleBitmap (&dc, size.cx, size.cy);

			CBitmap * pOldMem = dcMem.SelectObject (pBmp);
			CBitmap * pOldElement = dcElement.SelectObject (&bmp);

			::BitBlt (dcMem, 0, 0, size.cx, size.cy, NULL, 0, 0, WHITENESS);

			int x = 0, y = 0, cx = size.cx, cy = size.cy;

			// ImageIpToWinRotate returns bitmap upside down
			y += (cy - 1);
			cy *= -1;

			#ifdef _DEBUG
			if (CBitmap * p = dcElement.GetCurrentBitmap ()) {
				using namespace FoxjetDatabase;

				DIBSECTION ds;

				ZeroMemory (&ds, sizeof (ds));
				p->GetObject (sizeof (ds), &ds);

				TRACEF (FoxjetDatabase::ToString ((int)GetClassID ()) + _T (": ") + FoxjetDatabase::ToString ((int)ds.dsBm.bmWidth) + _T (", ") + FoxjetDatabase::ToString ((int)ds.dsBm.bmHeight));
			}
			#endif

			dcMem.StretchBlt (x, y, cx, cy, &dcElement, 0, 0, size.cx, size.cy, SRCCOPY);
			//SAVEBITMAP (bmp, _T ("C:\\Foxjet\\Debug\\CreateImage.bmp"));
			//SAVEBITMAP (dcMem, _T ("C:\\Foxjet\\Debug\\EDITOR.bmp"));

			dcMem.SelectObject (pOldMem);
			dcElement.SelectObject (pOldElement);
		}

		delete [] pDestBuffer;
	}
	else {
		CSize size;

		LPBYTE pDestBuffer = ImageIpToWin (pElement->pfi, pSrcBuffer, size);

		if (!pBmp->CreateBitmap (size.cx, size.cy, 1, 1, pDestBuffer)) {
			CDC dcMem;
			CRect rc (0, 0, 0, 0);
			CString strError = LoadString (IDS_INVALIDDATA);

			dcMem.CreateCompatibleDC (&dc);
			dcMem.DrawText (strError, &rc, DT_CALCRECT);

			pBmp->CreateCompatibleBitmap (&dcMem, rc.Width (), rc.Height ());
			CBitmap * pMem = dcMem.SelectObject (pBmp);

			::BitBlt (dcMem, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
			dcMem.DrawText (strError, &rc, 0);

			dcMem.SelectObject (pMem);
		}

		delete [] pDestBuffer;
	}

	pElement->pfm->pfl = NULL;
	fj_MessageDestroy (pElement->pfm);
	pElement->pfm = NULL;

	return pBmp;
}

bool CIpBaseElement::SetRedraw (bool bRedraw)
{
	if (bRedraw && m_pBmp) {
		delete m_pBmp;
		m_pBmp = NULL;
	}

	return CBaseElement::SetRedraw (bRedraw);
}

void CIpBaseElement::Invalidate()
{
	SetRedraw (true);

	if (m_pBmp) {
		delete m_pBmp;
		m_pBmp = NULL;
	}
}

bool CIpBaseElement::Validate () 
{ 
	return true; 
}

LPCFJSYSTEM CIpBaseElement::GetSystem () const
{
	return FoxjetIpElements::GetSystem (GetLineID ());
}

LPFJSYSTEM CIpBaseElement::GetSystem ()
{
	return FoxjetIpElements::GetSystem (GetLineID ());
}

void CIpBaseElement::ClipTo (const FoxjetDatabase::HEADSTRUCT & head)
{
	LPFJELEMENT p = GetElement ();

	AdjustYPos (p, head);
	CBaseElement::ClipTo (head);

	if (HasFont ()) {
		UINT nSize = GetFontSize ();

		if (nSize > (UINT)head.GetChannels ())
			SetFontSize (head.GetChannels ());
	}
}

void CIpBaseElement::AdjustYPos (LPFJELEMENT p, const FoxjetDatabase::HEADSTRUCT & head)
{
	if (FoxjetDatabase::GetHeadChannels (head.m_nHeadType) > 32)
		p->lRow = (int)ceil (p->lRow / 8.0) * 8;
}
