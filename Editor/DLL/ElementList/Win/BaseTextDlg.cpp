// BaseTextDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "BaseTextDlg.h"
#include "Debug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetElements;
using namespace FoxjetElements::TextElement;

/////////////////////////////////////////////////////////////////////////////
// CBaseTextDlg dialog


CBaseTextDlg::CBaseTextDlg (const FoxjetCommon::CBaseElement & e, const CSize & pa, 
							UINT nID, const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_nOrientation (NORMAL),
	m_nWidth (0),
	CElementDlg (e, pa, nID, pList, pParent)
{
	VERIFY (m_bmpOrientation [0].LoadBitmap (IDB_NORMAL));
	VERIFY (m_bmpOrientation [1].LoadBitmap (IDB_VERTICAL));
}


void CBaseTextDlg::DoDataExchange(CDataExchange* pDX)
{
	CElementDlg::DoDataExchange(pDX);

	if (GetDlgItem (RDO_NORMAL))
		DDX_Radio (pDX, RDO_NORMAL, (int &)m_nOrientation);

	if (GetDlgItem (TXT_WIDTH)) {
		bool bBitmapped = false;

		DDX_Text (pDX, TXT_WIDTH, m_nWidth);

		if (CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, &GetElement ()))
			bBitmapped = FoxjetCommon::IsBitmappedFont (p->GetFontName ());

		if (bBitmapped)
			DDV_MinMaxInt (pDX, m_nWidth, CTextElement::m_lmtGap.m_dwMin, CTextElement::m_lmtGap.m_dwMax);
		else
			DDV_MinMaxInt (pDX, m_nWidth, CTextElement::m_lmtWidth.m_dwMin, CTextElement::m_lmtWidth.m_dwMax);
	}
}


BEGIN_MESSAGE_MAP(CBaseTextDlg, CElementDlg)
	ON_BN_CLICKED (RDO_NORMAL, OnOrientationClick)
	ON_BN_CLICKED (RDO_VERTICAL, OnOrientationClick)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBaseTextDlg message handlers

BOOL CBaseTextDlg::OnInitDialog() 
{
	BOOL bResult = CElementDlg::OnInitDialog();
	
	OnOrientationClick ();

	return bResult;
}

FoxjetElements::TextElement::FONTORIENTATION CBaseTextDlg::GetOrientation () const
{
	if (CButton * pNormal = (CButton *)GetDlgItem (RDO_NORMAL))
		return pNormal->GetCheck () == 1 ? TextElement::NORMAL : TextElement::VERTICAL;

	return TextElement::NORMAL;
}

void CBaseTextDlg::OnOrientationClick()
{
	CButton * pRadio [] = 
	{
		(CButton *)GetDlgItem (RDO_NORMAL),
		(CButton *)GetDlgItem (RDO_VERTICAL),
	};

	for (int i = 0; i < 2; i++) {
		if (pRadio [i]) {	
			if (pRadio [i]->GetCheck () == 1) {
				CStatic * pBmp = (CStatic *)GetDlgItem (BMP_TEXT);
				ASSERT (pBmp);
				
				pBmp->SetBitmap (m_bmpOrientation [i]);
			}
		}
	}
}

