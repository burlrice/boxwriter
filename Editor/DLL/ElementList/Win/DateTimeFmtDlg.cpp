// DateTimeFmtDlg.cpp: implementation of the CDateTimeFmtDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DateTimeFmtDlg.h"
#include "CopyArray.h"
#include "ItiLibrary.h"
#include "Debug.h"
#include "Extern.h"
#include "DateTimeElement.h"
#include "TemplExt.h"

using namespace FoxjetElements;
using namespace ListGlobals;
using namespace ItiLibrary;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CDateTimeFmtDlg::CItem::CItem (ULONG lLineID, const CString & strCode, const CString & strDesc)
:	m_strCode (strCode),
	m_strDesc (strDesc),
	m_lLineID (lLineID)
{
	m_strExample = CDateTimeElement::Format (m_lLineID, m_strCode, 0, NULL);
}

CDateTimeFmtDlg::CItem::~CItem ()
{
}

/* TODO: rem
int CDateTimeFmtDlg::CItem::Compare (const ItiLibrary::CListCtrlImp::CItem & cmp, int nColumn) const
{
	const CItem & rhs = (CItem &)cmp;

	switch (nColumn) {
	case 0:		return m_strCode.Compare (rhs.m_strCode);
	case 1:		return m_strDesc.Compare (rhs.m_strDesc);
	}

	return 0;
}
*/

CString CDateTimeFmtDlg::CItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0:		return m_strCode;
	case 1:		return m_strExample;
	case 2:		return m_strDesc;
	}

	return str;
}

typedef struct tagFORMATRESSTRUCT 
{
	tagFORMATRESSTRUCT (UINT nIDDesc, const CString & strFmt) 
		: m_nIDDesc (nIDDesc), m_strFmt (strFmt) 
	{
	}

	UINT	m_nIDDesc;
	CString m_strFmt;
} FORMATRESSTRUCT;

bool operator == (const FORMATRESSTRUCT & lhs, const FORMATRESSTRUCT & rhs)
{
	return lhs.m_strFmt == rhs.m_strFmt && lhs.m_nIDDesc == rhs.m_nIDDesc;
}

bool operator != (const FORMATRESSTRUCT & lhs, const FORMATRESSTRUCT & rhs)
{
	return !(lhs == rhs);
}

static const FORMATRESSTRUCT fmtFormats [] = {
	FORMATRESSTRUCT (IDS_DATETIMEFMT_a_,				_T ("%a")), 
	FORMATRESSTRUCT (IDS_DATETIMEFMT_A, 				_T ("%A")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_b_,				_T ("%b")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_B, 				_T ("%B")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_c, 				_T ("%c")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_d,					_T ("%d")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_DD,				_T ("%%D")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_DD_L,				_T ("%%-D")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_H, 				_T ("%H")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_I, 				_T ("%I")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_j,					_T ("%j")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_j_euro,			_T ("%J")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_m_, 				_T ("%m")), 
	FORMATRESSTRUCT (IDS_DATETIMEFMT_MM, 				_T ("%%M")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_MM_L, 				_T ("%%-M")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_M, 				_T ("%M")), 
	FORMATRESSTRUCT (IDS_DATETIMEFMT_p,					_T ("%p")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_S, 				_T ("%S")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_U, 				_T ("%U")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_w_,				_T ("%w")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_W, 				_T ("%W")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_1W, 				_T ("%1W")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_x_, 				_T ("%x")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_X,					_T ("%X")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_y_, 				_T ("%y")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_Y, 				_T ("%Y")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_YYYY, 				_T ("%%Y")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_YYYY_L, 			_T ("%%-Y")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_z,					_T ("%z")),
														
	FORMATRESSTRUCT (IDS_DATETIMEFMT_HOURCODE,			_T ("%%0H")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_MONTHCODE,			_T ("%%0M")),
	FORMATRESSTRUCT (IDS_DATETIMEFMT_DAYCODE,			_T ("%%0A")),

	// std // #if __CUSTOM__ == __SW0840__
	FORMATRESSTRUCT (IDS_DATETIMEFMT_QUARTERHOURCODE,	_T ("%%0Q")),
	// std // #endif
};

/////////////////////////////////////////////////////////////////////////////
// CDateTimeFmtDlg dialog

CDateTimeFmtDlg::CDateTimeFmtDlg(ULONG lLineID, CWnd* pParent /*=NULL*/)
:	m_lLineID (lLineID),
	FoxjetCommon::CEliteDlg (IDD_BUILDDATETIMEFORMAT, pParent)
{
	//{{AFX_DATA_INIT(CDateTimeFmtDlg)
	m_strFmt = _T("");
	//}}AFX_DATA_INIT
}

CDateTimeFmtDlg::~CDateTimeFmtDlg ()
{
}

void CDateTimeFmtDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDateTimeFmtDlg)
	DDX_Text(pDX, TXT_DATETIMEFMTSTR, m_strFmt);
	//}}AFX_DATA_MAP

	if (!pDX->m_bSaveAndValidate)
		OnUpdateDatetimefmtstr ();
}


BEGIN_MESSAGE_MAP(CDateTimeFmtDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDateTimeFmtDlg)
	ON_BN_CLICKED(BN_INSERT, OnInsert)
	ON_EN_UPDATE(TXT_DATETIMEFMTSTR, OnUpdateDatetimefmtstr)
	ON_NOTIFY(NM_DBLCLK, LV_DATETIMEFMTCODES, OnDblclkDatetimefmtcodes)
	//}}AFX_MSG_MAP
	ON_NOTIFY(NM_DBLCLK, LV_DATETIMEFMTCODES, OnDblclkListCtrl)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDateTimeFmtDlg message handlers

BOOL CDateTimeFmtDlg::OnInitDialog() 
{
	using namespace ItiLibrary;

	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;

	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog ();

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_FORMATCODE), 40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_FORMATEXAMPLE), 100));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_DESC), 270));
	
	m_lvi.Create (LV_DATETIMEFMTCODES, vCols, this, defElements.m_strRegSection);	

	for (int i = 0; i < ARRAYSIZE (::fmtFormats); i++) {
		CItem * p = new CItem (m_lLineID, ::fmtFormats [i].m_strFmt, LoadString (::fmtFormats [i].m_nIDDesc));

		TRACEF (::fmtFormats [i].m_strFmt + _T (" ") + LoadString (::fmtFormats [i].m_nIDDesc));

		m_lvi.InsertCtrlData (p);
	}

	if (IsSunnyvale ()) {
		static const FORMATRESSTRUCT fmtSunnyvale [] = 
		{
			FORMATRESSTRUCT (IDS_SUNNYVALE_DAYCODE,				_T ("%%D")), 
			FORMATRESSTRUCT (IDS_SUNNYVALE_SHIFTCODE,			_T ("%%S")), 
		};

		for (int i = 0; i < ARRAYSIZE (fmtSunnyvale); i++) {
			CItem * p = new CItem (m_lLineID, fmtSunnyvale [i].m_strFmt, LoadString (fmtSunnyvale [i].m_nIDDesc));
			m_lvi.InsertCtrlData (p);
		}
	}

	return bResult;
}

void CDateTimeFmtDlg::OnInsert() 
{
	CLongArray sel = GetSelectedItems (m_lvi);

	if (sel.GetSize ()) {
		CString str;
		CEdit & txt = * (CEdit *)GetDlgItem (TXT_DATETIMEFMTSTR);

		txt.GetWindowText (str);

		for (int i = 0; i < sel.GetSize (); i++) {
			CItem * p = (CItem *)m_lvi.GetCtrlData (sel [i]);
			str += CString (p->m_strCode);

			if (i <= sel.GetSize () - 1)
				str += _T (" ");
		}

		txt.SetWindowText (str);
	}
	else
		MsgBox (* this, LoadString (IDS_NOFORMATSELECTED));
}

void CDateTimeFmtDlg::OnUpdateDatetimefmtstr() 
{
	CString strFmt, strSample;

	GetDlgItemText (TXT_DATETIMEFMTSTR, strFmt);
	strSample = CDateTimeElement::Format (FoxjetDatabase::GetMasterLineID (m_lLineID), strFmt, 0, NULL);
	SetDlgItemText (TXT_DATETIMESAMPLE, strSample);
}

void CDateTimeFmtDlg::OnDblclkDatetimefmtcodes(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnInsert ();	
	*pResult = 0;
}


void CDateTimeFmtDlg::OnDblclkListCtrl(NMHDR* pNMHDR, LRESULT* pResult)
{
	OnInsert ();
	* pResult = 0;
}
