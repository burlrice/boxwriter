#include "stdafx.h"
#include "SerialWizardPortsDlg.h"
#include "resource.h"
#include "Utils.h"
#include "WinMsg.h"
#include "Extern.h"
#include "Database.h"
#include "ComPropertiesDlg.h"
#include "Comm32.h"
#include "Debug.h"
#include "Parse.h"
#include "SerialSampleDlg.h"
#include "color.h"
#include <string>

using namespace FoxjetElements;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

IMPLEMENT_DYNAMIC(CSerialWizardPrintOncePage, CWizardBasePage)


BEGIN_MESSAGE_MAP(CSerialWizardPrintOncePage, CWizardBasePage)
END_MESSAGE_MAP()

CSerialWizardPrintOncePage::CSerialWizardPrintOncePage (CSerialWizardSheet * pParent)
:	m_bPrintOnce (false),
	CWizardBasePage (pParent, IDD_SERIAL_WIZARD_PRINT_ONCE)
{
}

CSerialWizardPrintOncePage::~CSerialWizardPrintOncePage ()
{
}

BOOL CSerialWizardPrintOncePage::OnInitDialog()
{
	if (CSerialWizardSheet * pParent = DYNAMIC_DOWNCAST (CSerialWizardSheet, m_pdlgParent)) {
		SETTINGSSTRUCT s;
		FoxjetCommon::StdCommDlg::CCommItem comm;
		CString str;

		str.Format (_T ("COM%d"), pParent->m_pdlgPort->m_port.m_dwPort);

		GetSettingsRecord (ListGlobals::GetDB (), 0, str, s);
		comm.FromString (s.m_strData);

		if (CButton * p = (CButton *)GetDlgItem (CHK_USEONCE)) 
			p->SetCheck (comm.m_bPrintOnce);
	}

	return CWizardBasePage::OnInitDialog ();
}

BOOL CSerialWizardPrintOncePage::OnKillActive()
{
	if (CButton * p = (CButton *)GetDlgItem (CHK_USEONCE)) 
		m_bPrintOnce = p->GetCheck () ? true : false;

	return CWizardBasePage::OnKillActive ();
}
