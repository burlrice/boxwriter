#include "stdafx.h"
#include "List.h"
#include "ximage.h"

using namespace FoxjetCommon;

namespace FoxjetCommon {
	ELEMENT_API bool SaveBitmap(HBITMAP hBmp, const CString & strFile)
	{
		CxImage img;

		img.CreateFromHBITMAP (hBmp);
		return img.Save (strFile, CXIMAGE_FORMAT_BMP);
	}
}

