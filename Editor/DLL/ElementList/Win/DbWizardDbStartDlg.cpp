// DbWizardDbStartDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DbWizardDbStartDlg.h"
#include "DbWizardDsnDlg.h"
#include "DbWizardFieldDlg.h"
#include "DbWizardKeyFieldDlg.h"
#include "DatabaseStartDlg.h"
#include "Extern.h"
#include "OdbcRecordset.h"
#include "Parse.h"

using namespace FoxjetElements;
using namespace FoxjetDatabase;

// CDbWizardDbStartDlg

IMPLEMENT_DYNAMIC(CDbWizardDbStartDlg, CDbWizardBasePage)

CDbWizardDbStartDlg::CDbWizardDbStartDlg(CDbWizardDlg * pParent)
:	CDbWizardBasePage (pParent, IDD_DATABASE_WIZARD_DB_START)
{
}

CDbWizardDbStartDlg::~CDbWizardDbStartDlg()
{
}


BEGIN_MESSAGE_MAP(CDbWizardDbStartDlg, CDbWizardBasePage)
	ON_BN_CLICKED (CHK_YES, OnUpdateUI)
	ON_BN_CLICKED (CHK_NO, OnUpdateUI)
	ON_BN_CLICKED (BTN_DBSTART, OnDbStart)
END_MESSAGE_MAP()



// CDbWizardDbStartDlg message handlers


BOOL CDbWizardDbStartDlg::OnSetActive()
{
	return TRUE;
}

BOOL CDbWizardDbStartDlg::OnInitDialog ()
{
	FoxjetUtils::CDatabaseStartDlg dlgDbStart (this);

	CDbWizardBasePage::OnInitDialog ();

	dlgDbStart.Load (ListGlobals::GetDB ());

	m_params.m_strDSN		= dlgDbStart.m_strDSN;
	m_params.m_strTable		= dlgDbStart.m_strTable;
	m_params.m_strKeyField	= dlgDbStart.m_strKeyField;
	m_params.m_strTaskField	= dlgDbStart.m_strTaskField;

	SetDlgItemText (TXT_DSN,		m_params.m_strDSN);
	SetDlgItemText (TXT_TABLE,		m_params.m_strTable);
	SetDlgItemText (TXT_KEYFIELD,	m_params.m_strKeyField);
	SetDlgItemText (TXT_TASKFIELD,	m_params.m_strTaskField);

	if (CButton * p = (CButton *)GetDlgItem (CHK_YES)) 
		p->SetCheck (1);

	/*
	{
		CString strKeyField = m_pdlgMain->m_pdlgKeyField->GetCurSelKeyField ();
		UINT n [] = { CHK_YES, BTN_DBSTART };

		for (int i = 0; i < ARRAYSIZE (n); i++) 
			if (CWnd * p = GetDlgItem (n [i])) 
				p->EnableWindow (!strKeyField.IsEmpty ());

		if (CButton * p = (CButton *)GetDlgItem (CHK_YES)) 
			p->SetCheck (strKeyField.IsEmpty () ? 0 : 1);

		if (CButton * p = (CButton *)GetDlgItem (CHK_NO)) 
			p->SetCheck (strKeyField.IsEmpty () ? 1 : 0);
	}
	*/

	OnUpdateUI ();

	return TRUE;
}

void CDbWizardDbStartDlg::OnUpdateUI ()
{
	if (CButton * pYes = (CButton *)GetDlgItem (CHK_YES)) {
		bool bEnabled = (pYes->GetCheck () == 1) ? true : false;

		if (CWnd * p = GetDlgItem (BTN_DBSTART)) 
			p->EnableWindow (bEnabled);
	}
}

bool CDbWizardDbStartDlg::CanAdvance ()
{
	if (CButton * pYes = (CButton *)GetDlgItem (CHK_YES)) {
		if (pYes->GetCheck () == 0)
			return true;
		else
			return 
				m_params.m_strDSN.GetLength () && 
				m_params.m_strTable.GetLength () && 
				m_params.m_strKeyField.GetLength () && 
				m_params.m_strTaskField.GetLength ();
	}

	return false;
}

void CDbWizardDbStartDlg::OnDbStart ()
{
	FoxjetUtils::CDatabaseStartDlg dlg (this);

	dlg.m_bFreeCache = false;
	dlg.Load (ListGlobals::GetDB ());

	dlg.m_strDSN		= m_pdlgMain->m_pdlgDSN->GetCurSelDsn ();
	dlg.m_strTable		= m_pdlgMain->m_pdlgField->GetCurSelTable ();
	dlg.m_strKeyField	= m_pdlgMain->m_pdlgKeyField->GetCurSelKeyField ();

	try
	{
		//COdbcRecordset rst (&m_pdlgMain->m_db);
		CString strSQL = _T ("SELECT * FROM [") + dlg.m_strTable + _T ("];");
		COdbcCache rst = COdbcDatabase::Find (m_pdlgMain->m_pdb->GetDB (), m_pdlgMain->m_pdb->GetDSN (), strSQL);

		if (!rst.GetRst ().IsEOF ()) {
			for (short i = 0; i < rst.GetRst ().GetODBCFieldCount (); i++) {
				CODBCFieldInfo info;
				const CString strName = Extract (rst.GetRst ().GetFieldName (i), _T ("].["), _T ("]"));
				CString strNameLower = strName;

				strNameLower.MakeLower ();

				if (strNameLower.Find (_T ("task")) != -1) {
					dlg.m_strTaskField = strName;
					break;
				}
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }

	if (dlg.DoModal () == IDOK) {
		m_params.m_strDSN		= dlg.m_strDSN;
		m_params.m_strTable		= dlg.m_strTable;
		m_params.m_strKeyField	= dlg.m_strKeyField;
		m_params.m_strTaskField	= dlg.m_strTaskField;
		m_params.m_nSQLType		= dlg.m_nSQLType;

		SetDlgItemText (TXT_DSN,		m_params.m_strDSN);
		SetDlgItemText (TXT_TABLE,		m_params.m_strTable);
		SetDlgItemText (TXT_KEYFIELD,	m_params.m_strKeyField);
		SetDlgItemText (TXT_TASKFIELD,	m_params.m_strTaskField);
	}
}