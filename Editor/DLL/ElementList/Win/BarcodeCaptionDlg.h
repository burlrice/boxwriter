#if !defined(AFX_BARCODEFONTDLG_H__D7E96821_ED33_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_BARCODEFONTDLG_H__D7E96821_ED33_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BarcodeCaptionDlg.h : header file
//

#include "resource.h"
#include "BarcodeElement.h"
#include "PreviewCtrl.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CBarcodeCaptionDlg dialog

class CBarcodeCaptionDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CBarcodeCaptionDlg(const FoxjetDatabase::HEADSTRUCT & head, CWnd * pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBarcodeCaptionDlg)
	enum { IDD = IDD_BARCODECAPTION };
	//}}AFX_DATA

public:
	bool IsPreviewEnabled () const;

	FoxjetElements::CBarcodeElement		m_barcode;
	const FoxjetDatabase::HEADSTRUCT	m_head;
	BOOL								m_bPreview;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBarcodeCaptionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CPreviewCtrl m_wndPreview;

	// Generated message map functions
	//{{AFX_MSG(CBarcodeCaptionDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPreview();
	//}}AFX_MSG

	afx_msg void OnParamsChanged ();

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BARCODEFONTDLG_H__D7E96821_ED33_11D4_8FC6_006067662794__INCLUDED_)
