#if !defined(AFX_BITMAPSETTINGSDLG_H__69093A1D_ECB4_4C1B_93FA_439DBCD2A5E5__INCLUDED_)
#define AFX_BITMAPSETTINGSDLG_H__69093A1D_ECB4_4C1B_93FA_439DBCD2A5E5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BitmapSettingsDlg.h : header file
//

#include "resource.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CBitmapSettingsDlg dialog

class CBitmapSettingsDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CBitmapSettingsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBitmapSettingsDlg)
	enum { IDD = IDD_BITMAPSETTINGS };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBitmapSettingsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	afx_msg void UpdateUI ();

	// Generated message map functions
	//{{AFX_MSG(CBitmapSettingsDlg)
	afx_msg void OnBrowseEditor();
	afx_msg void OnBrowseLogos();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnLocate();
	//}}AFX_MSG
	afx_msg void OnBrowseLogosLocal();

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BITMAPSETTINGSDLG_H__69093A1D_ECB4_4C1B_93FA_439DBCD2A5E5__INCLUDED_)
