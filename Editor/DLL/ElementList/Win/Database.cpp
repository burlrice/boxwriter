#include "stdafx.h"
#include "List.h"
#include "Database.h"
#include "Extern.h"
#include "TemplExt.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ListGlobals;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

static CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
static CArray <LINESTRUCT, LINESTRUCT &> vLines;
static CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;

bool InitDatabase (FoxjetDatabase::COdbcDatabase & database)
{
	DBMANAGETHREADSTATE (database);

	::vLines.RemoveAll ();
	::vHeads.RemoveAll ();
	::vPanels.RemoveAll ();

	GetPrinterLines (database, GetPrinterID (database), ::vLines); //FoxjetDatabase::GetLineRecords (database, ::vLines);
	GetPrinterHeads (database, GetPrinterID (database), ::vHeads); //FoxjetDatabase::GetHeadRecords (database, ::vHeads);

	for (int i = 0; i < ::vLines.GetSize (); i++) {
		CArray <PANELSTRUCT, PANELSTRUCT &> v;
		ULONG lLineID = ::vLines [i].m_lID;

		FoxjetDatabase::GetPanelRecords (database, lLineID, v);
		::vPanels.Append (v);
	}

	return true;
}

// these functions have identical signatures to their counterparts in
// the "Database" project, except the "COdbcDatabase *" param.
// InitDatabase caches the data here to avoid hitting the database
// while printing

ELEMENT_API UINT FoxjetCommon::GetLineRecordCount ()
{
	DBMANAGETHREADSTATE (GetDB ());

	return ::vLines.GetSize ();
}

ELEMENT_API ULONG FoxjetCommon::GetLineID (const CString & str)
{
	DBMANAGETHREADSTATE (GetDB ());

	for (int i = 0; i < ::vLines.GetSize (); i++) {
		const LINESTRUCT & line = ::vLines [i];

		if (!line.m_strName.CompareNoCase (str))
			return line.m_lID;
	}

	return NOTFOUND;
}

ELEMENT_API bool FoxjetCommon::GetLineRecord (ULONG lID, FoxjetDatabase::LINESTRUCT & cs)
{
	DBMANAGETHREADSTATE (GetDB ());

	for (int i = 0; i < ::vLines.GetSize (); i++) {
		const LINESTRUCT & line = ::vLines [i];

		if (line.m_lID == lID) {
			cs = line;
			return true;
		}
	}

	return false;
}

ELEMENT_API bool FoxjetCommon::GetLineRecords (CArray <FoxjetDatabase::LINESTRUCT, FoxjetDatabase::LINESTRUCT &> & v)
{
	DBMANAGETHREADSTATE (GetDB ());

	v.Copy (::vLines);

	return v.GetSize () > 0;
}

ELEMENT_API bool FoxjetCommon::GetFirstLineRecord (FoxjetDatabase::LINESTRUCT & line)
{
	DBMANAGETHREADSTATE (GetDB ());

	if (::vLines.GetSize ()) {
		line = ::vLines [0];
		return true;
	}

	return false;
}

ELEMENT_API bool FoxjetCommon::GetLineHeads (ULONG lLineID, CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (GetDB ());

	v.RemoveAll ();

	for (int nPanel = 0; nPanel < ::vPanels.GetSize (); nPanel++) {
		PANELSTRUCT & panel = ::vPanels [nPanel];

		if (panel.m_lLineID == lLineID) {
			for (int nHead = 0; nHead < ::vHeads.GetSize (); nHead++) {
				HEADSTRUCT & head = ::vHeads [nHead];

				if (head.m_lPanelID == panel.m_lID)
					v.Add (head);
			}
		}
	}

	return  v.GetSize () > 0 ? true : false;
}

ELEMENT_API UINT FoxjetCommon::GetHeadRecordCount ()
{
	DBMANAGETHREADSTATE (GetDB ());

	return ::vHeads.GetSize ();
}

ELEMENT_API bool FoxjetCommon::GetHeadRecord (ULONG lID, FoxjetDatabase::HEADSTRUCT & hs)
{
//	DBMANAGETHREADSTATE (GetDB ());

	for (int i = 0; i < ::vHeads.GetSize (); i++) {
		HEADSTRUCT & head = ::vHeads [i];

		if (head.m_lID == lID) {
			hs = head;
			return true;
		}
	}

	return false;
}

ELEMENT_API bool FoxjetCommon::GetHeadRecords (CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (GetDB ());

	Copy (v, ::vHeads);

	return v.GetSize () > 0;
}

ELEMENT_API ULONG FoxjetCommon::GetHeadID (const CString & strUID)
{
	DBMANAGETHREADSTATE (GetDB ());

	for (int i = 0; i < ::vHeads.GetSize (); i++) {
		HEADSTRUCT & head = ::vHeads [i];

		if (!head.m_strUID.CompareNoCase (strUID))
			return head.m_lID;
	}

	return NOTFOUND;
}

ELEMENT_API ULONG FoxjetCommon::GetHeadID (ULONG lLineID, const CString & strHead)
{
	DBMANAGETHREADSTATE (GetDB ());

	for (int i = 0; i < ::vHeads.GetSize (); i++) {
		HEADSTRUCT & head = ::vHeads [i];

		if (!head.m_strName.CompareNoCase (strHead))
			return head.m_lID;
	}

	return NOTFOUND;
}

ELEMENT_API bool FoxjetCommon::GetFirstHeadRecord (FoxjetDatabase::HEADSTRUCT & head)
{
	DBMANAGETHREADSTATE (GetDB ());

	if (::vHeads.GetSize ()) {
		head = ::vHeads [0];
		return true;
	}

	return false;
}

