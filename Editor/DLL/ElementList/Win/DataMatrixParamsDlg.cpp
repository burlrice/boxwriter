// DataMatrixParamsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "DataMatrixParamsDlg.h"
#include "resource.h"
#include "BarcodeElement.h"
#include "list.h"
#include "TemplExt.h"
#include "BarcodeParams.h"
#include "BarcodeParamDlg.h"

using namespace FoxjetElements;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

struct
{
	UINT	m_nCtrlID;
	int		m_nAlign;
}
static const map [] =
{
	{ RDO_OFF,				kCapAlignOff,			},
	{ RDO_BELOWLEFT,		kCapAlignBelowLeft,		},
	{ RDO_BELOWCENTER,		kCapAlignBelowCenter,	},
	{ RDO_BELOWRIGHT,		kCapAlignBelowRight,	},
	{ RDO_ABOVELEFT,		kCapAlignAboveLeft,		},
	{ RDO_ABOVECENTER,		kCapAlignAboveCenter,	},
	{ RDO_ABOVERIGHT,		kCapAlignAboveRight,	}, 
};

/////////////////////////////////////////////////////////////////////////////
// CDataMatrixParamsDlg dialog

CDataMatrixParamsDlg::CDataMatrixParamsDlg(FoxjetDatabase::HEADTYPE type, CWnd* pParent /*=NULL*/)
:	CUpcBarcodeParamsDlg(IDD_DATAMATRIXPARAMS, type, pParent),
	m_nWidth (0),
	m_nHeight (0),
	m_nAlignment (0),
	m_nEncoding (0)
{
	//{{AFX_DATA_INIT(CDataMatrixParamsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	VERIFY (m_bmpCaption [0].LoadBitmap (IDB_CAPTIONOFF));
	VERIFY (m_bmpCaption [1].LoadBitmap (IDB_CAPTIONBELOWLEFT));
	VERIFY (m_bmpCaption [2].LoadBitmap (IDB_CAPTIONBELOWCENTER));
	VERIFY (m_bmpCaption [3].LoadBitmap (IDB_CAPTIONBELOWRIGHT));
	VERIFY (m_bmpCaption [4].LoadBitmap (IDB_CAPTIONABOVELEFT));
	VERIFY (m_bmpCaption [5].LoadBitmap (IDB_CAPTIONABOVECENTER));
	VERIFY (m_bmpCaption [6].LoadBitmap (IDB_CAPTIONABOVERIGHT));
}


void CDataMatrixParamsDlg::DoDataExchange(CDataExchange* pDX)
{
	CUpcBarcodeParamsDlg::DoDataExchange(pDX);

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		CButton * p = (CButton *)GetDlgItem (map [i].m_nCtrlID);
		
		ASSERT (p);
		
		if (!pDX->m_bSaveAndValidate)
			p->SetCheck (map [i].m_nAlign == m_nAlignment);
		else {
			if (p->GetCheck () == 1) {
				m_nAlignment = map [i].m_nAlign;
				break;
			}
		}	
	}

	DDX_Text (pDX, TXT_WIDTH, m_nWidth);
	DDV_MinMaxInt (pDX, m_nWidth, CBarcodeElement::m_lmtWidthFactor.m_dwMin, CBarcodeElement::m_lmtWidthFactor.m_dwMax); 

	DDX_Text (pDX, TXT_HEIGHT, m_nHeight);
	DDV_MinMaxInt (pDX, m_nHeight, CBarcodeElement::m_lmtHeightFactor.m_dwMin, CBarcodeElement::m_lmtHeightFactor.m_dwMax); 
}


BEGIN_MESSAGE_MAP(CDataMatrixParamsDlg, CUpcBarcodeParamsDlg)
	//{{AFX_MSG_MAP(CDataMatrixParamsDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (RDO_ABOVELEFT, InvalidatePreview)
	ON_BN_CLICKED (RDO_ABOVECENTER, InvalidatePreview)
	ON_BN_CLICKED (RDO_ABOVERIGHT, InvalidatePreview)
	ON_BN_CLICKED (RDO_BELOWLEFT, InvalidatePreview)
	ON_BN_CLICKED (RDO_BELOWCENTER, InvalidatePreview)
	ON_BN_CLICKED (RDO_BELOWRIGHT, InvalidatePreview)
	ON_BN_CLICKED (RDO_OFF, InvalidatePreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataMatrixParamsDlg message handlers

void CDataMatrixParamsDlg::InvalidatePreview ()
{
	CStatic * pPreview = (CStatic *)GetDlgItem (IDC_PREVIEW);

	ASSERT (pPreview);

	for (int i = 0; i < (sizeof (map) / sizeof (map [0])); i++) {
		CButton * p = (CButton *)GetDlgItem (map [i].m_nCtrlID);

		ASSERT (p);

		if (p->GetCheck () == 1) {
			pPreview->SetBitmap (m_bmpCaption [i]);
			CBarcodeParamDlg::ResizePreview (this, IDC_PREVIEW, RDO_ABOVERIGHT, RDO_BELOWLEFT);
			break;
		}
	}
}


int CDataMatrixParamsDlg::GetDefSymbology () const
{
	return kSymbologyDataMatrixEcc200;
}

BOOL CDataMatrixParamsDlg::OnInitDialog() 
{
	CUpcBarcodeParamsDlg::OnInitDialog();
	
	InvalidatePreview ();
	
	if (!m_bStandardParam) {
		UINT nID [] = 
		{
			TXT_MAG,
			TXT_WIDTH,
			TXT_HEIGHT,
		};

		for (int i = 0; i < (sizeof (nID) / sizeof (nID [0])); i++) {
			if (CWnd * p = GetDlgItem (nID [i]))
				p->EnableWindow (FALSE);
		}
	}

	{ // TODO: rem ?
		UINT nID [] = 
		{
			RDO_ABOVELEFT,
			RDO_ABOVECENTER,
			RDO_ABOVERIGHT,
			RDO_BELOWLEFT,
			RDO_BELOWCENTER,
			RDO_BELOWRIGHT,
			RDO_OFF,
		};

		for (int i = 0; i < (sizeof (nID) / sizeof (nID [0])); i++) {
			if (CWnd * p = GetDlgItem (nID [i]))
				p->EnableWindow (FALSE);
		}
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
