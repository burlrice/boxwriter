#if !defined(AFX_DEFINESHIFTDLG_H__061A2167_E0EE_4E7B_8840_F061288EED41__INCLUDED_)
#define AFX_DEFINESHIFTDLG_H__061A2167_E0EE_4E7B_8840_F061288EED41__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DefineShiftDlg.h : header file
//

#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CDefineShiftDlg dialog

namespace DefineShiftDlg
{
	class CShiftCode
	{
	public:
		CShiftCode (ULONG lLineID = -1);
		CShiftCode (const CShiftCode & rhs);
		CShiftCode & operator = (const CShiftCode & rhs);
		virtual ~CShiftCode ();

		CString ToString (const FoxjetCommon::CVersion & ver) const;
		bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);

		ULONG			m_lLineID;
		COleDateTime	m_dtShift1;
		COleDateTime	m_dtShift2;
		COleDateTime	m_dtShift3;
		CString			m_strShiftCode1;
		CString			m_strShiftCode2;
		CString			m_strShiftCode3;
	};

	class CDefineShiftDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CDefineShiftDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CDefineShiftDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		static const FoxjetCommon::LIMITSTRUCT m_lmtShift;

		ULONG m_lLineID;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CDefineShiftDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:

		CShiftCode GetCurSelShiftCode () const;
		bool SetCurSelShiftCode (const CShiftCode & s);
		bool Update (const CShiftCode & code);
		void InitCtrls ();

		CArray <CShiftCode, CShiftCode &> m_vCodes;

		// Generated message map functions
		//{{AFX_MSG(CDefineShiftDlg)
		virtual BOOL OnInitDialog();
		virtual void OnOK();
		afx_msg void OnSelchangeLine();
		afx_msg void OnApply();
		afx_msg void OnDatetimechangeShift(NMHDR* pNMHDR, LRESULT* pResult);
		//}}AFX_MSG

		afx_msg void EnableApply ();

		DECLARE_MESSAGE_MAP()
	};
}; //namespace DefineShiftDlg
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEFINESHIFTDLG_H__061A2167_E0EE_4E7B_8840_F061288EED41__INCLUDED_)
