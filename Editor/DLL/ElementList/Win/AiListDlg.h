// AiListDlg.h: interface for the CAiListDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AILISTDLG_H__97A2D103_8C96_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_AILISTDLG_H__97A2D103_8C96_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SubElementListDlg.h"

class CAiListDlg : public CSubElementListDlg  
{
// Construction
public:
	CAiListDlg(CWnd* pParent = NULL);   // standard constructor
	CAiListDlg(UINT nIDTemplate, CWnd* pParent = NULL);

// Dialog Data
	//{{AFX_DATA(CAiListDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAiListDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual bool IsAI () const { return true; }

	// Generated message map functions
	//{{AFX_MSG(CAiListDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_AILISTDLG_H__97A2D103_8C96_11D4_915E_00104BEF6341__INCLUDED_)
