// BarcodeDlg.cpp : implementation file
//

#include "stdafx.h"

#include "ItiLibrary.h"
#include "SubElement.h"
#include "Debug.h"
#include "Edit.h"
#include "Coord.h"
#include "List.h"
#include "BarcodeDlg.h"
#include "BarcodeElement.h"
#include "TextElement.h"
#include "DateTimeElement.h"
#include "ExpDateElement.h"
#include "CountElement.h"
#include "UserElement.h"
#include "SelectSubElementDlg.h"
#include "BarcodeCaptionDlg.h"
#include "MaxiCodeInfoDlg.h"
#include "DataMatrixDlg.h"
#include "Extern.h"
#include "Registry.h"
#include "BarcodeParams.h"
#include "WinMsg.h"
#include "GlobalBarcodeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ItiLibrary;
using namespace FoxjetCommon;
using namespace FoxjetElements::TextElement;
using namespace FoxjetElements;
using namespace ListGlobals;
using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////
// CBarcodeDlg dialog

const int CBarcodeDlg::m_nIDCol			= 0;
const int CBarcodeDlg::m_nElementCol	= 1;
const int CBarcodeDlg::m_nLengthCol		= 2;
const int CBarcodeDlg::m_nDescCol		= 3;


CBarcodeDlg::CBarcodeDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
						 const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_bSimple (true),
	m_nOrientation (FoxjetElements::TextElement::NORMAL),
	CElementDlg(e, pa, IDD_BARCODE, pList, pParent)
{
	//{{AFX_DATA_INIT(CBarcodeDlg)
	m_bQuietZone = FALSE;
	m_bIncludeText = FALSE;
	m_nSymbology = -1;
	m_nMagnification = 1;
	m_bCheckSum = FALSE;
	m_nRatio = 0;
	m_nHeight = 0;
	m_nWidth = 0;
	//}}AFX_DATA_INIT

	VERIFY (m_bmpOrientation [0].LoadBitmap (IDB_NORMAL));
	VERIFY (m_bmpOrientation [1].LoadBitmap (IDB_VERTICAL));
}

CBarcodeDlg::~CBarcodeDlg ()
{
	m_vSubElements.RemoveAll ();
}

void CBarcodeDlg::DoDataExchange(CDataExchange* pDX)
{
	// note: members not set here are manipulated in OnInitDialog 
	using namespace FoxjetDatabase;

	CElementDlg::DoDataExchange(pDX);

	const HEADSTRUCT & head = GetHead ();
	int nSymb = GetCurSelSymbology ();

	//{{AFX_DATA_MAP(CBarcodeDlg)
	//}}AFX_DATA_MAP

	DDX_Check(pDX, CHK_SIMPLE, m_bSimple);
	DDX_Text(pDX, TXT_SIMPLEDATA, m_strData);

	if (m_bSimple)
		DDV_MinMaxChars (pDX, m_strData, TXT_SIMPLEDATA, 
			CBarcodeElement::m_lmtString.m_dwMin,
			CBarcodeElement::m_lmtString.m_dwMax);

	DDX_Text (pDX, TXT_WIDTH, m_nWidth);
	if (nSymb == kSymbologyDataMatrixEcc200 || nSymb == kSymbologyDataMatrixGs1)
		DDV_MinMaxInt (pDX, m_nWidth, -2, CBarcodeElement::m_lmtCX.m_dwMax); 

	DDX_Text (pDX, TXT_HEIGHT, m_nHeight);
	if (nSymb == kSymbologyDataMatrixEcc200 || nSymb == kSymbologyDataMatrixGs1)
		DDV_MinMaxInt (pDX, m_nHeight, -2, CBarcodeElement::m_lmtCY.m_dwMax); 

	if (pDX->m_bSaveAndValidate) {
		CString strFail;
		bool bFail = false;
		int nMag = GetCurSelMagnification ();

		if (IsSimple ()) {
			CSubElement sub (TEXT);

			m_vSubElements.RemoveAll ();		
			sub.m_strDataMask = CString (CSubElement::m_cDef, m_strData.GetLength ());
			//TRACEF (sub.m_strDataMask);
			bool bSetData = sub.SetData (m_strData);

			if (!bSetData) {
				MsgBox (* this, LoadString (IDS_INVALIDBARCODEDATA));
				pDX->Fail ();
			}

			// std // #if __CUSTOM__ == __SW0840__
			if (m_pAllLists) {
				ASSERT (GetLineID () != -1);
				sub.GetElement ()->SetLineID (GetLineID ());
			}
			// std // #endif

			m_vSubElements.Add (sub);
		}

		try {
			bool bValidSymb = CBarcodeElement::IsValidSymbology (nSymb);
			bool bValidData = CBarcodeElement::IsValid (nSymb, m_vSubElements, true);

			if (!bFail && !bValidSymb) {
				strFail = LoadString (IDS_NOSYMBSELECTED);
				pDX->PrepareCtrl (CB_SYMBOLOGY);
				bFail = true;
			}

			if (!bFail && !bValidData) {
				strFail = LoadString (IDS_INVALIDBARCODEDATA);
				bFail = true;
			}

			CDC dc;
			dc.CreateCompatibleDC (GetDC ());
			DrawBarcode (dc, true); // let WASP generate exceptions
		}
		catch (CBarcodeException * e) {
			strFail = e->GetErrorMessage ();
			bFail = true;
			HANDLEEXCEPTION_TRACEONLY (e);
		}

		if (bFail) {
			MsgBox (* this, strFail);
			pDX->Fail ();
		}
		else {
			m_nSymbology = nSymb;
			m_nMagnification = nMag;
		}
	}
}


BEGIN_MESSAGE_MAP(CBarcodeDlg, CElementDlg)
	//{{AFX_MSG_MAP(CBarcodeDlg)
	ON_BN_CLICKED(BTN_ADDAI, OnAddAi)
	ON_BN_CLICKED(BTN_DELETE, OnDelete)
	ON_BN_CLICKED(BTN_DOWN, OnDown)
	ON_BN_CLICKED(BTN_UP, OnUp)
	ON_BN_CLICKED(BTN_ADDELEMENT, OnAddelement)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_NOTIFY(NM_DBLCLK, LV_AGGREGATE, OnDblclkAggregate)
	ON_BN_CLICKED(BTN_CAPTION, OnCaption)
	ON_BN_CLICKED(BTN_MORE, OnMore)
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(CHK_SIMPLE, OnSimple)
	ON_CBN_SELCHANGE (CB_SYMBOLOGY, OnSelchangeSymbology)
	ON_CBN_SELCHANGE (CB_MAGNIFICATION, OnSelchangeMag)
	ON_BN_CLICKED(BTN_PARAMS, OnEditParams)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBarcodeDlg message handlers


BOOL CBarcodeDlg::OnInitDialog() 
{
	CComboBox * pSymbology = (CComboBox *)GetDlgItem (CB_SYMBOLOGY);
	int i;

	InitData ();
	BOOL bResult = CElementDlg::OnInitDialog();

	for (i = kSymbologyFirst; i <= kSymbologyLast; i++) {
		if (CBarcodeElement::IsValidSymbology (i)) {
			int nIndex = pSymbology->AddString (GetSymbology (i, GetDB ()));
		
			pSymbology->SetItemData (nIndex, i);
		}
	}
	
	if (!SelectItemData (pSymbology, m_nSymbology))
		pSymbology->SetCurSel (0);

	InitAggregatesHeader ();
	InitAggregateData ();

	HINSTANCE hInst = GetInstanceHandle ();
	ASSERT (hInst);

	CButton * pButtons [6] = {
		(CButton *)GetDlgItem (BTN_UP),
		(CButton *)GetDlgItem (BTN_DOWN),
		(CButton *)GetDlgItem (BTN_ADDAI),
		(CButton *)GetDlgItem (BTN_ADDELEMENT),
		(CButton *)GetDlgItem (BTN_DELETE),
		(CButton *)GetDlgItem (BTN_EDIT),
	};
	UINT nIDs [] = { 
		IDB_UP,				IDB_DOWN,		IDB_ADDAI,
		IDB_ADDELEMENT,		IDB_DELETE,		IDB_PROPERTIES,
	};
	#ifdef _DEBUG
	CString strIDs [6] = { 
		_T ("IDB_UP"),			_T ("IDB_DOWN"),		_T ("IDB_ADDAI"),
		_T ("IDB_ADDELEMENT"),	_T ("IDB_DELETE"),		_T ("IDB_PROPERTIES"),
	};
	#endif //_DEBUG

	for (i = 0; i < 6; i++) {
//		HBITMAP hBmp = ::LoadBitmap (hInst, MAKEINTRESOURCE (nIDs [i]));
		HBITMAP hBmp = (HBITMAP)::LoadImage (hInst, MAKEINTRESOURCE (nIDs [i]), IMAGE_BITMAP, 0, 0, 
			LR_DEFAULTSIZE | LR_LOADTRANSPARENT);

		if (hBmp == NULL) {
			#ifdef _DEBUG
			CString str;
			str.Format (_T ("LoadBitmap (0x%08X, %s) failed"),
				hInst, strIDs [i]);
			TRACEF (str);
			ASSERT (0);
			#endif //_DEBUG
		}

		pButtons [i]->SetBitmap (hBmp);
	}
	
	OnSelchangeSymbology ();
	OnSimple ();
	OnOrientationClick ();

	if (CButton * p = (CButton *)GetDlgItem (BTN_CAPTION)) {
		p->EnableWindow (FALSE);

		if (m_pAllLists) {
			for (int j = 0; j < m_pAllLists->GetSize (); j++) {
				FoxjetCommon::CElementList & list = * (m_pAllLists->GetAt (j));

				if (list.GetHead ().m_lID == GetHead ().m_lID) {
					for (int i = 0; i < list.GetSize (); i++) {
						UINT nID = list.GetObject (i).GetID ();

						if (nID == m_nID)
							p->EnableWindow (TRUE);
					}
				}
			}
		}
	}

	return bResult;
}

void CBarcodeDlg::InitAggregatesHeader()
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_AGGREGATE);

	pLV->InsertColumn (m_nIDCol, LoadString (IDS_TEMPLATEID), LVCFMT_LEFT, 40);
	pLV->InsertColumn (m_nElementCol, LoadString (IDS_ELEMENTTYPE), LVCFMT_LEFT, 80);
	pLV->InsertColumn (m_nLengthCol, LoadString (IDS_LENGTH), LVCFMT_LEFT, 40);
	pLV->InsertColumn (m_nDescCol, LoadString (IDS_DESC), LVCFMT_LEFT, 160);

	pLV->SetExtendedStyle (LVS_EX_FULLROWSELECT);
	pLV->ModifyStyle (0, LVS_SHOWSELALWAYS);
	pLV->GetHeaderCtrl ()->ModifyStyle (HDS_BUTTONS, 0);

	LoadSettings (* pLV, defElements.m_strRegSection, _T ("Aggregate columns"));
}


void CBarcodeDlg::OnAddAi() 
{
	AddSubElement (true);
}

void CBarcodeDlg::OnDelete() 
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_AGGREGATE);
	CLongArray sel = GetSelectedItems (* pLV);

	if (sel.GetSize ()) {
		int nResult = MsgBox (* this, LoadString (IDS_CONFIRMELEMENTDELETE), 
			LoadString (IDS_CONFIRM), MB_YESNO);

		if (nResult == IDYES) {
			for (int i = sel.GetSize () - 1; i >= 0; i--) 
				m_vSubElements.RemoveAt (sel [i]);

			RefreshAggregateData ();
		}
	}
}

void CBarcodeDlg::OnDown() 
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_AGGREGATE);
	CLongArray sel = GetSelectedItems (* pLV);
	
	if (sel.GetSize ()) {
		// since the ctrl doesn't do column-click sorting,
		// the last selected item has the largest index in
		// the ctrl

		// can't shift down if the last item is selected
		if ((int)sel [sel.GetSize () - 1] < (pLV->GetItemCount () - 1)) { 
			int i;

			for (i = 0; i < sel.GetSize (); i++) {
				CSubElement sub = m_vSubElements [sel [i]];
				m_vSubElements.RemoveAt (sel [i]);
				m_vSubElements.InsertAt (sel [i] + 1, sub);
			}

			RefreshAggregateData ();
			SelectAll (* pLV, false);

			for (i = 0; i < sel.GetSize (); i++) 
				pLV->SetItemState (sel [i] + 1, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
		}
	}
}

void CBarcodeDlg::OnUp() 
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_AGGREGATE);
	CLongArray sel = GetSelectedItems (* pLV);
	
	if (sel.GetSize ()) {
		// since the ctrl doesn't do column-click sorting,
		// the first selected item has the smallest index in
		// the ctrl

		if (sel [0] > 0) { // can't shift up if the first item is selected
			int i;

			for (i = 0; i < sel.GetSize (); i++) {
				CSubElement sub = m_vSubElements [sel [i]];
				m_vSubElements.RemoveAt (sel [i]);
				m_vSubElements.InsertAt (sel [i] - 1, sub);
			}

			RefreshAggregateData ();
			SelectAll (* pLV, false);

			for (i = 0; i < sel.GetSize (); i++) 
				pLV->SetItemState (sel [i] - 1, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
		}
	}
}

BOOL CBarcodeDlg::DestroyWindow() 
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_AGGREGATE);

	SaveSettings (* pLV, 4, defElements.m_strRegSection, _T ("Aggregate columns"));
	DeleteAggregateData ();
	FreeMagCtrl ();

	return CElementDlg::DestroyWindow();
}

void CBarcodeDlg::OnAddelement() 
{
	AddSubElement (false);
}

void CBarcodeDlg::RefreshAggregateData ()
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_AGGREGATE);
	CLongArray sel = GetSelectedItems (* pLV);

	DeleteAggregateData ();
	InitAggregateData ();

	// reselect
	for (int i = 0; i < sel.GetSize (); i++) 
		pLV->SetItemState (sel [i], (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
}

void CBarcodeDlg::InitAggregateData()
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_AGGREGATE);

	for (int i = 0; i < m_vSubElements.GetSize (); i++) {
		AGGREGATESTRUCT * p = new AGGREGATESTRUCT;
		const CSubElement & sub = m_vSubElements [i];
		CString strMask = sub.m_strDataMask;

		strMask.Remove (CSubElement::m_cDelimeter);

		p->m_strID		= sub.m_strID;
		p->m_nField		= sub.GetType ();
		p->m_nLength	= strMask.GetLength ();
		p->m_strDesc	= sub.m_strDesc;
		
		InsertAggregateItem (* p, pLV->GetItemCount ());
	}
}

void CBarcodeDlg::DeleteAggregateData()
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_AGGREGATE);
	
	for (int i = 0; i < pLV->GetItemCount (); i++) {
		AGGREGATESTRUCT * p = (AGGREGATESTRUCT *)pLV->GetItemData (i);
		ASSERT (p);
		delete p;
	}

	pLV->DeleteAllItems ();
}

bool CBarcodeDlg::UpdateAggregateItem(AGGREGATESTRUCT &a, int nIndex)
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_AGGREGATE);

	if (nIndex >= 0 && nIndex < pLV->GetItemCount ()) {
		CString strLen;
			
		strLen.Format (_T ("%d"), a.m_nLength);

		pLV->SetItemText (nIndex, m_nIDCol, a.m_strID);
		pLV->SetItemText (nIndex, m_nElementCol, 
			GetElementResStr (a.m_nField));
		pLV->SetItemText (nIndex, m_nDescCol, a.m_strDesc);
		pLV->SetItemText (nIndex, m_nLengthCol, strLen);
		pLV->SetItemData (nIndex, (DWORD)&a);

		return true;
	}

	return false;
}

bool CBarcodeDlg::InsertAggregateItem(AGGREGATESTRUCT &a, int nIndex)
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_AGGREGATE);
	LV_ITEM lvi;

	lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM; 
	lvi.iItem = nIndex; 
	lvi.iSubItem = 0; 
	lvi.iImage = 0;
	lvi.pszText = _T (""); 
	lvi.lParam = (LPARAM) &a;

	if (pLV->InsertItem (&lvi) != -1) 
		return UpdateAggregateItem (a, nIndex);
	else
		return false;
}

void CBarcodeDlg::OnEdit() 
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_AGGREGATE);
	CLongArray sel = GetSelectedItems (* pLV);

	if (sel.GetSize ()) {
		if (OnEdit (sel [0]))
			RefreshAggregateData ();
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

bool CBarcodeDlg::OnEdit(int nIndex)
{
	CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_AGGREGATE);

	ASSERT (m_vSubElements.GetSize () == pLV->GetItemCount ());

	if (nIndex >= 0 && nIndex < pLV->GetItemCount ()) {
		// have to instantiate a new one here because the m_vSubElements ref is non-const
		CSubElement sub = m_vSubElements [nIndex];	
		CSubElementDlg dlg (sub, this);

		dlg.m_nType		= sub.GetType ();
		dlg.m_bAI		= sub.IsAi ();
		dlg.m_bReadOnly = true; 
		dlg.m_strDesc	= sub.m_strDesc;
		dlg.m_strID		= sub.m_strID;
		dlg.m_strMask	= sub.m_strDataMask;
		dlg.m_pAllLists = m_pAllLists;

		sub.Build ();
		dlg.m_strData	= sub.GetData ();

		if (dlg.DoModal () == IDOK) {
			const CSubElementDlg & constDlg = dlg;

			#ifdef _DEBUG
			CString str;
			str.Format (_T ("constDlg.GetElement ()->GetDefaultData () = %s"),
				constDlg.GetElement ()->GetDefaultData ());
			TRACEF (str);
			#endif //_DEBUG

			CTextElement * pTmp = (CTextElement *)CopyElement (constDlg.GetElement (), &GetHead (), false);

			pTmp->Build ();
			pTmp->SetDefaultData (dlg.m_strData);

			sub.m_strDesc		= dlg.m_strDesc;
			sub.m_strID			= dlg.m_strID;
			sub.m_strDataMask	= dlg.m_strMask;
			sub.SetElement (* pTmp);
			m_vSubElements [nIndex] = sub;

			delete pTmp;

			return true;
		}
	}

	return false;
}

void CBarcodeDlg::AddSubElement(bool bAI)
{
	CSelectSubElementDlg dlg (this);
	CWaitCursor wait;
	CVersion ver; 

	dlg.SetData (bAI ? GetAI () : GetSubElements ());
	dlg.m_bAI = bAI;

	if (dlg.DoModal () == IDOK) {
		CListCtrl * pLV = (CListCtrl *)GetDlgItem (LV_AGGREGATE);
		int i;

		for (i = 0; i < dlg.m_vSelected.GetSize (); i++) {
			AGGREGATESTRUCT * p = new AGGREGATESTRUCT;
			CSubElement sub (* dlg.m_vSelected [i]);

			sub.Build ();

			p->m_strID		= sub.m_strID;
			p->m_nField		= sub.GetElement ()->GetClassID (); 
			p->m_nLength	= sub.GetDataMaskLength ();
			p->m_strDesc	= sub.m_strDesc;

			/*
			pSubObj->SetData (a.m_strDefaultData);
			pSubObj->m_strID = a.m_strID;
			pSubObj->m_strDataMask = a.m_strDataMask;
			pSubObj->m_strDesc = a.m_strDesc;
			pSubObj->Build ();
			*/

			// std // #if __CUSTOM__ == __SW0840__
			if (m_pAllLists) {
				ASSERT (GetLineID () != -1);
				sub.GetElement ()->SetLineID (GetLineID ());
			}
			// std // #endif

			m_vSubElements.Add (sub);
			InsertAggregateItem (* p, pLV->GetItemCount ());

			int nIndex = m_vSubElements.GetSize () - 1;

			if (!OnEdit (nIndex)) {
				m_vSubElements.RemoveAt (nIndex);
				// refresh fct handles deleting p
			}

			RefreshAggregateData ();
		}
	}
}

BOOL CBarcodeDlg::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	BOOL bResult = CElementDlg::OnCommand(wParam, lParam);
	UpdateUI ();
	return bResult;
}

void CBarcodeDlg::OnSimple ()
{
	typedef enum MODE 
	{ 
		NONE		= 0,
		SIMPLE		= 0x01, 
		AGGREGATE	= 0x02,
		BOTH		= SIMPLE | AGGREGATE,
	};
	struct 
	{
		UINT m_nCtrlID;
		MODE m_mode;
		MODE m_enabled;
	} static const map [] = 
	{
		{ LV_AGGREGATE,		AGGREGATE,		NONE,			},
		{ BTN_UP,			AGGREGATE,		NONE,			},
		{ BTN_DOWN,			AGGREGATE,		NONE,			},
		{ BTN_ADDAI,		AGGREGATE,		NONE,			},
		{ BTN_ADDELEMENT,	AGGREGATE,		NONE,			},
		{ BTN_DELETE,		AGGREGATE,		NONE,			},
		{ BTN_EDIT,			AGGREGATE,		NONE,			},
		{ TXT_SIMPLEDATA,	SIMPLE,			NONE,			},
		{ -1,				NONE,			NONE,			},
	};
	MODE mode = IsSimple () ? SIMPLE : AGGREGATE;

	for (int i = 0; map [i].m_nCtrlID != -1; i++) {
		CWnd * p = GetDlgItem (map [i].m_nCtrlID);

		ASSERT (p);

		p->ShowWindow ((map [i].m_mode & mode) ? SW_SHOW : SW_HIDE);

		if (map [i].m_enabled & BOTH)
			p->EnableWindow (map [i].m_enabled & mode);
	}
}

void CBarcodeDlg::UpdateUI() 
{
	int nSymb = GetCurSelSymbology ();
	bool bMore =
		nSymb == kSymbologyMaxiCode ||
		nSymb == kSymbologyDataMatrixEcc200 || 
		nSymb == kSymbologyDataMatrixGs1;

	GetDlgItem (BTN_MORE)->EnableWindow (bMore);

	if (!IsSimple ()) {
		CListCtrl & lv = * (CListCtrl *)GetDlgItem (LV_AGGREGATE);
		CLongArray sel = GetSelectedItems (lv);
		int nSelCount = sel.GetSize ();
		int nSubElements = m_vSubElements.GetSize ();
		bool bAggregate = CBarcodeElement::SymbologyAllowsSubelements (nSymb);
		bool bAI = CBarcodeElement::SymbologyAllowsAI (nSymb);

		GetDlgItem (BTN_UP)->EnableWindow (nSelCount != 0 && nSubElements > 1);
		GetDlgItem (BTN_DOWN)->EnableWindow (nSelCount != 0 && nSubElements > 1);
		GetDlgItem (BTN_ADDAI)->EnableWindow (bAI);// && nSubElements == 0);
		GetDlgItem (BTN_ADDELEMENT)->EnableWindow (bAggregate || nSubElements == 0);
		GetDlgItem (BTN_DELETE)->EnableWindow (nSelCount != 0);
		GetDlgItem (BTN_EDIT)->EnableWindow (nSelCount != 0);
		//GetDlgItem (BTN_CAPTION)->EnableWindow (CBarcodeElement::SymbologyHasCaption (nSymb));
	}
}


BOOL CBarcodeDlg::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	BOOL bResult = CElementDlg::OnNotify(wParam, lParam, pResult);
	UpdateUI ();
	return bResult;
}


void CBarcodeDlg::OnDblclkAggregate(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();
	*pResult = 0;
}


int CBarcodeDlg::GetCurSelSymbology () const
{
	CComboBox * pSymbology = (CComboBox *)GetDlgItem (CB_SYMBOLOGY);
	ASSERT (pSymbology);
	int nIndex = pSymbology->GetCurSel ();

	if (nIndex != CB_ERR)
		return pSymbology->GetItemData (nIndex);

	return m_nSymbology;
}

int CBarcodeDlg::GetCurSelMagnification () const
{
	int nResult = -1;
	CComboBox * pMag = (CComboBox *)GetDlgItem (CB_MAGNIFICATION);
	ASSERT (pMag);
	int nIndex = pMag->GetCurSel ();

	ASSERT (nIndex != CB_ERR);

	DWORD dw = pMag->GetItemData (nIndex);

	if (dw != CB_ERR)
		if (CBarcodeParams * p = (CBarcodeParams *)dw)
			nResult = p->GetID ();

	return nResult;
}

void CBarcodeDlg::OnCaption() 
{
	DWORD dw = 0;

	::SendMessageTimeout (GetParent ()->m_hWnd, WM_CREATE_CAPTION, GetHead ().m_lID, m_nID, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dw);

	if (!dw) 
		MsgBox (* this, LoadString (IDS_CAPTIONFAILED));
/*
	CBarcodeCaptionDlg dlg (GetHead (), this);
	CString strRegSection = ListGlobals::defElements.m_strRegSection + CString ("\\CBarcodeCaptionDlg\\Settings");

	BuildElement ();
	dlg.m_barcode = GetElement ();
	dlg.m_bPreview = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strRegSection, 
		_T ("Preview"), 0) ? true : false;

	if (dlg.DoModal () == IDOK) {
		m_caption = dlg.m_barcode.GetCaption ();
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strRegSection, _T ("Preview"), dlg.m_bPreview);
	}
*/
}

void CBarcodeDlg::OnMore() 
{
	int nSymb = GetCurSelSymbology ();

	if (nSymb == kSymbologyMaxiCode) {
		CMaxiCodeInfoDlg dlg (this);
		CBarcodeElement & e = GetElement ();

		dlg.m_strClassOfSerice	= m_maxicode.m_strClassOfService;
		dlg.m_strCountryCode	= m_maxicode.m_strCountryCode;
		dlg.m_strPostalCode		= m_maxicode.m_strPostalCode;
		dlg.m_nLength			= m_maxicode.m_nDataLen;
		dlg.m_nMode				= m_maxicode.m_nMode;
		
		if (dlg.DoModal () == IDOK) {
			m_maxicode.m_strClassOfService	= dlg.m_strClassOfSerice;
			m_maxicode.m_strCountryCode		= dlg.m_strCountryCode;
			m_maxicode.m_strPostalCode		= dlg.m_strPostalCode;
			m_maxicode.m_nDataLen			= dlg.m_nLength;
			m_maxicode.m_nMode				= dlg.m_nMode;
		}
	}
	else if (nSymb == kSymbologyDataMatrixEcc200 || nSymb == kSymbologyDataMatrixGs1) {
		CDataMatrixDlg dlg (this);
		CBarcodeElement & e = GetElement ();

		dlg.m_nLength = m_datamatrix.m_nLength;
		
		if (dlg.DoModal () == IDOK) {
			m_datamatrix.m_nLength = dlg.m_nLength;
		}
	}
}

CSize CBarcodeDlg::DrawBarcode(CDC &dc, bool bThrowException)
{
	CSize size (0, 0);

	try {
		CBarcodeElement & barcode = GetElement ();

		BuildElement ();

		// IsValid and DrawBarcode can both throw CBarcodeException
		if (barcode.IsValid (true)) {
			barcode.Build ();
			size = barcode.DrawBarcode (dc, GetHead (), CSize (), FoxjetCommon::EDITOR, true);
		}
	}
	catch (CBarcodeException * e) { 
		if (bThrowException)
			throw (e); 
		else 
			HANDLEEXCEPTION_TRACEONLY (e);
	}

	return size;
}

int CBarcodeDlg::BuildElement ()
{
	CBarcodeElement & e = GetElement ();
	int nSymbology = GetCurSelSymbology ();

	e.SetID (GetDlgItemInt (TXT_ID));
	e.SetSymbology (nSymbology);
	e.SetMagnification (GetCurSelMagnification ());
	e.SetCaption (m_caption);
	
	if (nSymbology == kSymbologyDataMatrixEcc200 || nSymbology == kSymbologyDataMatrixGs1 || nSymbology == kSymbologyQR) {
		e.SetWidth (GetDlgItemInt (TXT_WIDTH));
		e.SetHeight (GetDlgItemInt (TXT_HEIGHT));
	}

	e.DeleteAllSubElements ();

	if (IsSimple ()) {
		CString strData;
		CSubElement * p = new CSubElement (TEXT);

		GetDlgItemText (TXT_SIMPLEDATA, strData);
		int nMaskLen = strData.GetLength ();

		p->m_strDataMask = CString (CSubElement::m_cDef, nMaskLen);
		//TRACEF (p->m_strDataMask);

		p->SetData (strData);
		e.InsertSubElement (0, * p);
	}
	else {
		for (int i = 0; i < m_vSubElements.GetSize (); i++) {
			CSubElement * pSub = new CSubElement (m_vSubElements [i]);
			e.InsertSubElement (i, * pSub);
		}
	}

	e.ClipTo (GetHead ());

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);

	TRACEF (e.GetImageData ());
	TRACEF (FoxjetDatabase::Format (e.GetImageData (), e.GetImageData ().GetLength ()));

	return nResult;
}

const CBarcodeElement & CBarcodeDlg::GetElement () const
{
	const CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CBarcodeElement)));
	return (CBarcodeElement &)e;
}

CBarcodeElement & CBarcodeDlg::GetElement ()
{
	CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CBarcodeElement)));
	return (CBarcodeElement &)e;
}

bool CBarcodeDlg::GetValues ()
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();

	try {
		CDC dc;
		dc.CreateCompatibleDC (GetDC ());
		DrawBarcode (dc, true); // let WASP generate exceptions
	}
	catch (CBarcodeException * e) {
		MsgBox (* this, e->GetErrorMessage ());
		HANDLEEXCEPTION_TRACEONLY (e);
		return false;
	}

	return CElementDlg::GetValues ();
}

void CBarcodeDlg::InitData ()
{
	// NOTE: this must be called before DoDataExchange

	m_bSimple = false;
	
	// a "simple" barcode must have one sub element, which is a text element with an input mask
	// of optional alpha-numerical chars exactly the same length as the element's data

	if (m_vSubElements.GetSize () == 1) {
		const CSubElement & sub = m_vSubElements [0];

		if (sub.GetType () == TEXT) {
			m_bSimple = true;

			for (int i = 0; i < sub.m_strDataMask.GetLength (); i++) {
				TCHAR c = sub.m_strDataMask [i];

				if (c != CSubElement::m_cAlphaNumericOpt &&
					c != CSubElement::m_cDef) 
				{
					m_bSimple = false;
					break;
				}
			}
		}
	}

	if (m_vSubElements.GetSize ()) {
		const CSubElement & sub = m_vSubElements [0];

		m_strData = sub.GetData ();
	}
}


bool CBarcodeDlg::IsSimple () const 
{
	CButton * pSimple = (CButton *)GetDlgItem (CHK_SIMPLE);
	
	ASSERT (pSimple);
	
	return pSimple->GetCheck () == 1;
}

void CBarcodeDlg::FreeMagCtrl ()
{
	CComboBox * pMag = (CComboBox *)GetDlgItem (CB_MAGNIFICATION);

	ASSERT (pMag);

	for (int i = 0; i < pMag->GetCount (); i++) 
		if (CBarcodeParams * p = (CBarcodeParams *)pMag->GetItemData (i))
			delete p;

	pMag->ResetContent ();
}

void CBarcodeDlg::OnSelchangeSymbology ()
{
	CComboBox * pMag = (CComboBox *)GetDlgItem (CB_MAGNIFICATION);
	int nSymb = GetCurSelSymbology ();
	CArray <CBarcodeParams, CBarcodeParams &> v;
	ULONG lLineID = GetLineID ();

	ASSERT (pMag);
	FreeMagCtrl ();

	GetBarcodeParams (lLineID, nSymb, GetHead ().m_nHeadType, GetHead ().GetRes (), v, GetDB ());

	if (!v.GetSize ()) {
		// GetLineID probably returned -1 (this dlg was created by element defs dlg)
		int nIndex = pMag->AddString (_T ("100 %"));
		pMag->SetItemData (nIndex, m_nMagnification);
		pMag->SetCurSel (nIndex);
	}
	else {
		ASSERT (GetElement ().GetLocalParamUID ());
		v.Add (CBarcodeElement::GetLocalParam (GetElement ().GetLocalParamUID (), nSymb));

		for (int i = 0; i < v.GetSize (); i++) {
			CBarcodeParams * p = new CBarcodeParams (v [i]);
			CString str;

			str.Format (_T ("%d %%"), 
				p->GetMagPct ());

			if (i >= GetParamsPerSymbology ()) 
				str = LoadString (IDS_LOCAL_BC_PARAM);
			else if (p->IsCustom ()) {
				CString strTmp, strVertical;

				if (p->GetOrientation () == TextElement::VERTICAL)
					strVertical = _T (", ") + LoadString (IDS_VERTICAL);

				strTmp.Format (_T ("(%s%s)"), LoadString (IDS_CUSTOM), strVertical);
				str += strTmp;
			}
			
			int nIndex = pMag->AddString (str);

			ASSERT (nIndex != CB_ERR);
			VERIFY (pMag->SetItemData (nIndex, (DWORD)p) != CB_ERR);

			if ((int)p->GetID () == m_nMagnification)
				pMag->SetCurSel (nIndex);
		}
	}

	if (pMag->GetCurSel () == CB_ERR)
		pMag->SetCurSel (0);

	{
		UINT nID [] = 
		{
			TXT_WIDTH,
			TXT_HEIGHT,
		};

		for (int i = 0; i < ARRAYSIZE (nID); i++)
			if (CWnd * p = GetDlgItem (nID [i]))
				p->EnableWindow (nSymb == kSymbologyDataMatrixEcc200 || nSymb == kSymbologyDataMatrixGs1);
	}

	OnSelchangeMag ();
}

void CBarcodeDlg::OnSelchangeMag ()
{
	CButton * pNormal = (CButton *)GetDlgItem (RDO_NORMAL);
	CButton * pVertical = (CButton *)GetDlgItem (RDO_VERTICAL);
	CComboBox * pMag = (CComboBox *)GetDlgItem (CB_MAGNIFICATION);
	bool bNormal = true;

	ASSERT (pNormal);
	ASSERT (pVertical);
	ASSERT (pMag);

	DWORD dw = pMag->GetItemData (pMag->GetCurSel ());

	if (dw != CB_ERR) 
		if (CBarcodeParams * p = (CBarcodeParams *)dw)
			if (p->GetOrientation () == VERTICAL) 
				bNormal = false;

	pVertical->SetCheck (bNormal ? 0 : 1);
	pNormal->SetCheck	(bNormal ? 1 : 0);
	OnOrientationClick ();
}

void CBarcodeDlg::OnOrientationClick()
{
	CButton * pRadio [] = 
	{
		(CButton *)GetDlgItem (RDO_NORMAL),
		(CButton *)GetDlgItem (RDO_VERTICAL),
	};

	for (int i = 0; i < 2; i++) {
		if (pRadio [i]) {
			if (pRadio [i]->GetCheck () == 1) {
				CStatic * pBmp = (CStatic *)GetDlgItem (BMP_TEXT);
				ASSERT (pBmp);
				
				pBmp->SetBitmap (m_bmpOrientation [i]);
			}
		}
	}

	//OnSelchangeSymbology ();
}


void CBarcodeDlg::OnEditParams ()
{
	int nSymbology	= GetCurSelSymbology ();
	int nMag		= GetCurSelMagnification ();
	ULONG lLineID	= GetLineID ();

	if (nMag == GetParamsPerSymbology ()) {
		CBarcodeParams b = CBarcodeElement::GetLocalParam (GetElement ().GetLocalParamUID (), nSymbology);

		b.SetLineID (lLineID);

		if (CBarcodeParams * pUpdate = CGlobalBarcodeDlg::OnEdit (lLineID, GetHead ().m_nHeadType, &b, this, ListGlobals::GetDB ())) {
			FoxjetDatabase::NotifySystemParamChange (ListGlobals::GetDB ());
			CBarcodeElement::SetLocalParam (GetElement ().GetLocalParamUID (), nSymbology, * pUpdate);
		}
	}
	else {
		const CString strSection = defElements.m_strRegSection + _T ("\\CGlobalBarcodeDlg");

		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("LineID"),			lLineID);
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("HeadType"),		GetHead ().m_nHeadType);
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("Symbology"),		nSymbology);
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("Magnification"),	nMag);
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("Resolution"),		GetHead ().GetRes ());

		FoxjetElements::OnDefineBarcodeParams (this, verApp);

		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("Magnification"),	-1);
	}
}

void CBarcodeDlg::Get (const FoxjetElements::CBarcodeElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Get (e, pHead);
	m_nID				= e.GetID ();
	m_nSymbology		= e.GetSymbology ();
	m_nMagnification	= e.GetMagnification ();
	m_caption			= e.GetCaption ();
	m_maxicode			= e.GetMaxiCodeInfo ();
	m_datamatrix		= e.GetDataMatrixInfo ();
	m_pt				= e.GetPos (pHead);
	m_nWidth			= e.GetWidth ();
	m_nHeight			= e.GetHeight ();

	for (int i = 0; i < e.GetSubElementCount (); i++) {
		CBarcodeElement & element = * (CBarcodeElement *)(LPVOID)&e;

		// std // #if __CUSTOM__ == __SW0840__
		if (m_pAllLists) {
			ASSERT (element.GetLineID () != -1);
			element.GetSubElement (i).GetElement ()->SetLineID (element.GetLineID ());
		}
		// std // #endif

		m_vSubElements.Add (element.GetSubElement (i));
	}
}

void CBarcodeDlg::Set (FoxjetElements::CBarcodeElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Set (e, pHead);
	e.SetID (m_nID);
	e.SetSymbology (m_nSymbology);
	e.SetMagnification (m_nMagnification);
	e.SetCaption (m_caption);
	//e.SetMaxiCodeInfo (m_maxicode);
	e.SetDataMatrixInfo (m_datamatrix);
	e.SetWidth (m_nWidth);
	e.SetHeight (m_nHeight);
	
	e.DeleteAllSubElements ();
	
	for (int i = 0; i < m_vSubElements.GetSize (); i++) {
		CSubElement * p = new CSubElement (m_vSubElements [i]);

		e.InsertSubElement (i, * p);
	}
	
	e.SetAlignment (m_align);
	e.SetPos (m_pt = e.GetPosInternal (m_pt), pHead, ALIGNMENT_ADJUST_SUBTRACT);
}
