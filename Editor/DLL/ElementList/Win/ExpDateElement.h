// ExpDateElement.h: interface for the CExpDateElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EXPDATEELEMENT_H__6CB62660_8AD9_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_EXPDATEELEMENT_H__6CB62660_8AD9_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DateTimeElement.h"
#include "Types.h"

namespace FoxjetElements
{
	class ELEMENT_API CExpDateElement : public CDateTimeElement  
	{
		DECLARE_DYNAMIC (CExpDateElement);

	public:
		typedef enum { SPAN_DAYS = 0, SPAN_HOURS, SPAN_MINUTES, SPAN_SECONDS } SPAN;
		typedef enum { PERIOD_DAYS = 0, PERIOD_WEEKS, PERIOD_MONTHS, PERIOD_YEARS } PERIOD;
		typedef enum { TYPE_NORMAL = 0, TYPE_DATABASE, TYPE_USER } TYPE;

		CExpDateElement(const FoxjetDatabase::HEADSTRUCT & head);
		CExpDateElement (const CExpDateElement & rhs);
		CExpDateElement & operator = (const CExpDateElement & rhs);
		virtual ~CExpDateElement();

		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);
		virtual int GetClassID () const { return EXPDATE; }

		virtual int Build (BUILDTYPE type = INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		ULONG GetSpan (SPAN span = SPAN_DAYS) const;
		bool SetSpan (ULONG lSpan, SPAN span = SPAN_DAYS);

		PERIOD GetPeriod () const;
		bool SetPeriod (PERIOD n);
	
		typedef enum { EXP_NOROUND = 0, EXP_ROUNDUP, EXP_ROUNDDOWN } EXP_ROUND;

		EXP_ROUND GetRound () const;
		bool SetRound (EXP_ROUND r);
	
		int GetFirstDayOfWeek () const;
	
		//static time_t CExpDateElement::fj_mktime (struct tm *pTM);
		static int GetDstOffset (const CTime & tmNow, const CTime & tmExp);

		int Build (BUILDTYPE type, bool bCanThrowException, CTime tmNow);

		TYPE GetType () const;
		bool SetType (TYPE type);

		bool GetPromptAtTaskStart () const;
		bool SetPromptAtTaskStart (bool bPrompt);

		CString GetPrompt () const;
		bool SetPrompt (const CString & str);

		CString GetDSN () const;
		bool SetDSN (const CString & str);

		CString GetTable () const;
		bool SetTable (const CString & str);

		CString GetField () const;
		bool SetField (const CString & str);

		CString GetKeyField () const;
		bool SetKeyField	(const CString & str);

		CString GetKeyValue () const;
		bool SetKeyValue (const CString & str);

		int GetSQLType () const;
		bool SetSQLType (int nType);

		int GetFormatType () const;
		bool SetFormatType (int nFormat);

		CString GetFormat () const;
		bool SetFormat (const CString & str);

	protected:
		CTime CalcExpTime (const CTime & tm, const CTime & tmNow) const;

		PERIOD		m_period;
		EXP_ROUND	m_round;
		ULONG		m_lDays;
		int			m_nHours;
		int			m_nMins;
		int			m_nSecs; 

		TYPE	m_type;
		bool	m_bPromptAtTaskStart;
		CString m_strPrompt;
		CString m_strDSN;
		CString m_strTable;
		CString m_strField;
		CString m_strKeyField;
		CString m_strKeyValue;
		int		m_nSQLType;
		int		m_nFormat;
		CString	m_strFormat;
	};
}; //namespace FoxjetElements

#endif // !defined(AFX_EXPDATEELEMENT_H__6CB62660_8AD9_11D4_8FC6_006067662794__INCLUDED_)
