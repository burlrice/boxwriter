#if !defined(AFX_USERDLG_H__471C7428_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_USERDLG_H__471C7428_8B3D_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserDlg.h : header file
//

#include "BaseTextDlg.h"
#include "UserElement.h"

/////////////////////////////////////////////////////////////////////////////
// CUserDlg dialog

class CUserDlg : public CBaseTextDlg
{
// Construction
public:
	CUserDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CUserDlg)
	BOOL	m_bPrompt;
	CString	m_strData;
	CString	m_strPrompt;
	//}}AFX_DATA

	UINT	m_nMaxChars;

	void Get (const FoxjetElements::CUserElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);
	void Set (FoxjetElements::CUserElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);

	virtual UINT GetDefCtrlID () const { return TXT_TEXT; }

protected:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserDlg)
	protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	afx_msg void OnSpecial();
	afx_msg void OnSpecialCmd (UINT nID);
	afx_msg void OnDrawItem( int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct );
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpmis);

	virtual int BuildElement();
	FoxjetElements::CUserElement & GetElement ();
	const FoxjetElements::CUserElement & GetElement () const;
	
	afx_msg void SetDefaultData ();
	afx_msg void OnDefaultClick ();

	// Generated message map functions
	//{{AFX_MSG(CUserDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERDLG_H__471C7428_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
