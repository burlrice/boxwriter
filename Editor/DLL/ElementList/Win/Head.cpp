// Head.cpp: implementation of the CHead class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Head.h"
#include "Extern.h"
#include "Debug.h"
#include "ListImp.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;

static CStringArray vFonts;

CHead::CHead(const FoxjetDatabase::HEADSTRUCT & head)
:	m_dbHead (head)
{
}

CHead::CHead (const CHead & rhs)
:	m_dbHead (rhs.m_dbHead)
{
}

CHead & CHead::operator = (const CHead & rhs)
{
	if (this != &rhs) {
		m_dbHead = rhs.m_dbHead;
	}

	return * this;
}

CHead::~CHead()
{
}

void CHead::GetBitmappedFonts (CArray <CPrinterFont, CPrinterFont &> &v)
{
	CStringArray vFonts;

	FoxjetCommon::GetBitmappedFonts (vFonts);

	for (int i = 0; i < vFonts.GetSize (); i++) {
		CPrinterFont f;

		f.m_strName = vFonts [i];
		
		v.Add (f);
	}
}

bool CHead::GetFonts(CArray <CPrinterFont, CPrinterFont &> &v) const
{
	if (!::vFonts.GetSize ()) 
		GetWinFontNames (::vFonts);

	v.RemoveAll ();

/*
	if (IsHpHead (m_dbHead.m_nHeadType)) {
		CPrinterFont f;
		f.m_strName = "TODO: hp font";
		v.Add (f);
	}
	else {
*/
		for (int i = 0; i < ::vFonts.GetSize (); i++) 
			v.Add (CPrinterFont (::vFonts [i]));

		if (IsValveHead (m_dbHead)) {
			CHead * pNonConst = (CHead *)(LPVOID)this;

			pNonConst->GetBitmappedFonts (v);
		}
//	}

	return v.GetSize () > 0;
}

bool CHead::GetWinFontNames(CStringArray &v)
{
	v.RemoveAll ();

	LOGFONT lf;
	HDC hDC = ::GetDC (NULL);

	memset (&lf, 0, sizeof (LOGFONT));
	lf.lfCharSet = DEFAULT_CHARSET;
	lf.lfFaceName [0] =  '\0';
	lf.lfPitchAndFamily = 0;

	::EnumFontFamiliesEx (hDC, &lf, (FONTENUMPROC)EnumFontFamExProc, (LPARAM)&v, 0);
	::ReleaseDC (NULL, hDC);

	return v.GetSize () > 0;
}

bool CHead::GetFont(const CString &str, CPrinterFont &fnt) const
{
	if (!::vFonts.GetSize ()) 
		GetWinFontNames (::vFonts);

	for (int i = 0; i < ::vFonts.GetSize (); i++) {
		if (::vFonts [i] == str) {
			fnt.m_strName = ::vFonts [i];
			fnt.m_nSize = 32;
			fnt.m_bBold = false;
			fnt.m_bItalic = false;
			return true;
		}
	}

	return false;
}

bool CHead::IsValidFontName (const CString & str) const
{
	if (!::vFonts.GetSize ()) 
		GetWinFontNames (::vFonts);

	for (int i = 0; i < ::vFonts.GetSize (); i++) {
		if (!::vFonts [i].CompareNoCase (str))
			return true;
	}

	return false;
}

int CHead::GetFontIndex (const CString & str)
{
	if (!::vFonts.GetSize ()) 
		GetWinFontNames (::vFonts);

	for (int i = 0; i < ::vFonts.GetSize (); i++) 
		if (!str.CompareNoCase (::vFonts [i]))
			return i;

	return -1;
}
