// DateTimeFmtDlg.h: interface for the CDateTimeFmtDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATETIMEFMTDLG_H__97A2D107_8C96_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_DATETIMEFMTDLG_H__97A2D107_8C96_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ListCtrlImp.h"
#include "Resource.h"
#include "Utils.h"

class CDateTimeFmtDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CDateTimeFmtDlg(ULONG lLineID = -1, CWnd* pParent = NULL);   // standard constructor
	virtual ~CDateTimeFmtDlg ();

// Dialog Data
	//{{AFX_DATA(CDateTimeFmtDlg)
 	CString	m_strFmt;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDateTimeFmtDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	class CItem : public ItiLibrary::ListCtrlImp::CItem 
	{
	public:
		CItem (ULONG lLineID = -1, const CString & strCode = _T (""), const CString & strDesc =  _T(""));
		virtual ~CItem ();

		virtual CString GetDispText (int nColumn) const;

		CString m_strCode;
		CString m_strDesc;
		CString m_strExample;
		ULONG m_lLineID;
	};

	// Generated message map functions
	//{{AFX_MSG(CDateTimeFmtDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnInsert();
	afx_msg void OnUpdateDatetimefmtstr();
	afx_msg void OnDblclkDatetimefmtcodes(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lvi;
	ULONG m_lLineID;

	afx_msg void OnDblclkListCtrl(NMHDR* pNMHDR, LRESULT* pResult);

	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_DATETIMEFMTDLG_H__97A2D107_8C96_11D4_915E_00104BEF6341__INCLUDED_)
