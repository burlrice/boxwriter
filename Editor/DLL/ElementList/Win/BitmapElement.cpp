// BitmapElement.cpp: implementation of the CBitmapElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <math.h>
#include <string>
#include <vector>

#include "BitmapElement.h"
#include "List.h"
#include "Resource.h"
#include "ElementDefaults.h"
#include "Coord.h"
#include "Edit.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"
#include "Utils.h"
#include "TemplExt.h"
#include "Utils.h"
#include "ximage.h"
#include "Parse.h"
#include "Registry.h"
#include "DatabaseElement.h"
#include "OdbcRecordset.h"
#include "FileExt.h"
#include "zlib.h"
#include "AclInfo.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//extern CCriticalSection csDatabase;

using namespace FoxjetCommon;
using namespace Color;
using namespace FoxjetElements;
using namespace FoxjetDatabase;
using namespace ListGlobals;
using namespace FoxjetCommon::ElementFields;

#define BLANK_BMP _T ("blank.bmp")

IMPLEMENT_DYNAMIC (CBitmapElement, CBaseElement);

CBitmapElement::CBitmapElement(const FoxjetDatabase::HEADSTRUCT & head)
:	m_pbmpPrinter (NULL),
	m_bMonochrome (false),
	m_bResizeOnRedraw (false),
	m_bUserPrompted (false),
	m_bPromptAtTaskStart (false),
	m_bLockAspectRatio (false),
	CBaseElement (head)
{
	m_db.m_bInvalid = true;
	m_db.m_bLookup = false;
	m_db.m_bPrompt = false;
	m_db.m_nSQLType = 0;

	LoadDefResBitmap (head);
}

CBitmapElement::CBitmapElement (const CBitmapElement & rhs)
:	m_pbmpPrinter (NULL),
	m_bMonochrome (false),
	m_bResizeOnRedraw (false),
	m_bUserPrompted (rhs.m_bUserPrompted),
	m_strUserPrompt (rhs.m_strUserPrompt),
	m_bPromptAtTaskStart (rhs.m_bPromptAtTaskStart),
	m_bLockAspectRatio (rhs.m_bLockAspectRatio),
	CBaseElement (rhs)
{
	Copy (rhs);
}

CBitmapElement & CBitmapElement::operator = (const CBitmapElement & rhs)
{
	CBaseElement::operator = (rhs);

	if (this != &rhs) {
		Copy (rhs);
	}

	return * this;
}

CBitmapElement::~CBitmapElement()
{
	Free ();
}

bool CBitmapElement::Copy (const CBitmapElement & rhs)
{
	DECLARETRACECOUNT (20);
	bool bResult = false;

	MARKTRACECOUNT ();

	m_bmpEditor					= rhs.m_bmpEditor;
	m_strFilePath				= rhs.m_strFilePath;
	m_size						= rhs.m_size;
	m_sizeStatic				= rhs.m_sizeStatic;

	m_db.m_bInvalid				= true;
	m_db.m_bLookup				= rhs.m_db.m_bLookup;
	m_db.m_bPrompt				= rhs.m_db.m_bPrompt;
	m_db.m_strDSN				= rhs.m_db.m_strDSN,
	m_db.m_strTable				= rhs.m_db.m_strTable;
	m_db.m_strField				= rhs.m_db.m_strField;
	m_db.m_strKeyField			= rhs.m_db.m_strKeyField;
	m_db.m_strKeyValue			= rhs.m_db.m_strKeyValue;
	m_db.m_nSQLType				= rhs.m_db.m_nSQLType;
	m_db.m_strBmpWidthField		= rhs.m_db.m_strBmpWidthField;
	m_db.m_strBmpHeightField	= rhs.m_db.m_strBmpHeightField;

	m_bUserPrompted				= rhs.m_bUserPrompted;
	m_strUserPrompt				= rhs.m_strUserPrompt;
	m_bPromptAtTaskStart		= rhs.m_bPromptAtTaskStart;
	m_bLockAspectRatio			= rhs.m_bLockAspectRatio;

	bResult = true;

	MARKTRACECOUNT ();

	if (!bResult)
		bResult = SetFilePath (rhs.GetFilePath (), rhs.m_head);
	
//	TRACECOUNTARRAYMAX ();

	return bResult;
}

// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
static const ElementFields::FIELDSTRUCT fields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType,		_T ("")								),	// Prefix (required)
	ElementFields::FIELDSTRUCT (m_lpszID,				_T ("0")							),	// ID (required)
	ElementFields::FIELDSTRUCT (m_lpszX,				_T ("0")							),	// x (required)
	ElementFields::FIELDSTRUCT (m_lpszY,				_T ("0")							),	// y (required)
	ElementFields::FIELDSTRUCT (m_lpszWidth,			_T ("0")							),	// width
	ElementFields::FIELDSTRUCT (m_lpszHeight,			_T ("0")							),	// height
	ElementFields::FIELDSTRUCT (m_lpszFlipH,			_T ("0")							),	// flip h
	ElementFields::FIELDSTRUCT (m_lpszFlipV,			_T ("0")							),	// flip v
	ElementFields::FIELDSTRUCT (m_lpszInverse,			_T ("0")							),	// inverse
	ElementFields::FIELDSTRUCT (m_lpszFileName,			CBitmapElement::GetDefFilename ()	),	// file path
	ElementFields::FIELDSTRUCT (m_lpszAlign,			_T ("0")							),	// alignment

	ElementFields::FIELDSTRUCT (m_lpszDatabaseLookup,	_T ("0")							),	// 
	ElementFields::FIELDSTRUCT (m_lpszPromptAtStart,	_T ("0")							),	// 
	ElementFields::FIELDSTRUCT (m_lpszDSN,				_T ("")								),	// 
	ElementFields::FIELDSTRUCT (m_lpszTable,			_T ("")								),	// 
	ElementFields::FIELDSTRUCT (m_lpszField,			_T ("")								),	// 
	ElementFields::FIELDSTRUCT (m_lpszKeyField,			_T ("")								),	// 
	ElementFields::FIELDSTRUCT (m_lpszKeyValue,			_T ("")								),	// 
	ElementFields::FIELDSTRUCT (m_lpszSQLType,			_T ("")								),	// 

	ElementFields::FIELDSTRUCT (m_lpszPrompt,			_T ("0")							),	// 
	ElementFields::FIELDSTRUCT (m_lpszUserPrompt,		_T ("")								),	// 
	ElementFields::FIELDSTRUCT (m_lpszPromptAtTaskStart,_T ("0")							),	// 

	ElementFields::FIELDSTRUCT (m_lpszBmpWidth,			_T ("")								),	// 
	ElementFields::FIELDSTRUCT (m_lpszBmpHeight,		_T ("")								),	// 
	ElementFields::FIELDSTRUCT (m_lpszDither,			_T ("0")							),	// 
	ElementFields::FIELDSTRUCT (m_lpszLockAspectRatio,	_T ("0")							),	// 
	ElementFields::FIELDSTRUCT (m_lpszColor,			_T ("0")							),	// 

	ElementFields::FIELDSTRUCT (),
};

CString CBitmapElement::ToString (const FoxjetCommon::CVersion & ver) const
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated

	CString str;
	CPoint pt = GetPosInternal (ALIGNMENT_ADJUST_ADD);

	str.Format (_T ("{Bitmap,%u,%u,%u,%u,%u,%d,%d,%d,%s,%d,%d,%d,%s,%s,%s,%s,%s,%d,%d,%s,%d,%s,%s,%d,%d,%06X}"),
		GetID (),
		pt.x, pt.y,
		m_size.cx, m_size.cy,
		IsFlippedH (),
		IsFlippedV (),
		IsInverse (),
		FormatString (GetFilePath (), true),
		GetAlignment (),
		m_db.m_bLookup,
		m_db.m_bPrompt,
		m_db.m_strDSN,
		m_db.m_strTable,
		m_db.m_strField,
		m_db.m_strKeyField,
		m_db.m_strKeyValue,
		m_db.m_nSQLType,
		m_bUserPrompted,
		FormatString (m_strUserPrompt),
		m_bPromptAtTaskStart,
		FormatString (m_db.m_strBmpWidthField),
		FormatString (m_db.m_strBmpHeightField),
		GetDither (),
		m_bLockAspectRatio,
		GetColor ());

	return str;
}

bool CBitmapElement::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	CStringArray vstrToken;
	CPrinterFont f;

	Parse (str, ver, vstrToken);

	if (vstrToken [0].CompareNoCase (_T ("Bitmap")) != 0) {
		TRACEF ("Wrong prefix \"" + vstrToken [0] + "\"");
		return false;
	}

	CSize size ((UINT)_tcstol (vstrToken [4], NULL, 10), 
		(UINT)_tcstol (vstrToken [5], NULL, 10));
	CString strFilename = UnformatString (vstrToken [9]);

	if (size.cx == 0 || size.cy == 0)
		size = GetSizePixels (strFilename, GetDefaultHead ());

	bool bResult = true;

	CPoint pt (
		(UINT)_tcstol (vstrToken [2], NULL, 10), 
		(UINT)_tcstol (vstrToken [3], NULL, 10));

	bResult &= SetID ((UINT)_tcstol (vstrToken [1], NULL, 10));
	bResult &= SetPos (pt, NULL);
	bResult &= SetFlippedH (_ttoi (vstrToken [6]) ? true : false);
	bResult &= SetFlippedV (_ttoi (vstrToken [7]) ? true : false);
	bResult &= SetInverse (_ttoi (vstrToken [8]) ? true : false);
	bResult &= SetFilePath (strFilename, m_head, size);
	bResult &= SetAlignment ((ALIGNMENT)_ttoi (vstrToken [10]));

	bResult &= SetDatabaseLookup (_ttoi (vstrToken [11]) ? true : false);

	if (IsDatabaseLookup ()) {
		bResult &= SetDatabasePromptAtTaskStart (_ttoi (vstrToken [12]) ? true : false);
		bResult &= SetDatabaseDSN (vstrToken [13]);
		bResult &= SetDatabaseTable (vstrToken [14]);
		bResult &= SetDatabaseField (vstrToken [15]);
		bResult &= SetDatabaseKeyField (vstrToken [16]);
		bResult &= SetDatabaseKeyValue (vstrToken [17]);
		bResult &= SetDatabaseSQLType (_ttoi (vstrToken [18]));
	}

	bResult &= SetUserPrompted (_ttoi (vstrToken [19]) ? true : false);
	bResult &= SetUserPrompt (UnformatString (vstrToken [20]));
	bResult &= SetPromptAtTaskStart (_ttoi (vstrToken [21]) ? true : false);

	if (IsDatabaseLookup ()) {
		bResult &= SetDbWidthField (vstrToken [22]);
		bResult &= SetDbHeightField (vstrToken [23]);
	}

	bResult &= SetDither (_ttoi (vstrToken [24]));
	bResult &= SetLockAspectRatio (_ttoi (vstrToken [25]) ? true : false);
	bResult &= SetColor (_tcstoul (vstrToken [26], NULL, 16));

	if (bResult) 
		m_size = m_sizeStatic = size;

	return bResult;
}

CSize CBitmapElement::Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly, 
							FoxjetCommon::DRAWCONTEXT context)
{
	double dStretch [2] = { 1.0, 1.0 };
	const int nDither = GetDither ();

	if (GetRedraw ()) {
//		if (m_db.m_bInvalid)
//			Build (IMAGING, false);

		if (m_pbmpPrinter) {
			delete m_pbmpPrinter;
			m_pbmpPrinter = NULL;
		}

		if (m_bResizeOnRedraw) {
			HBITMAP hBmp = LoadBitmap (GetFilePath (), head, m_size, &m_bMonochrome);

			Free ();
			VERIFY (m_bmpEditor.Load (hBmp)); 
			m_bResizeOnRedraw = false;
		}
	}

	CalcStretch (head, dStretch);
	CSize size = CBaseElement::ThousandthsToLogical (GetWindowRect (head).Size (), head);
	CSize sizeNormal = m_bmpEditor.GetSize ();
	CSize sizeStretch = m_size;

	CSize sizePrinter = CBaseElement::ThousandthsToLogical (m_size, head);

#ifdef __WYSIWYGFIX__

	if (context == PRINTER) {
		double dScreenRes = CCoord::GetPixelsPerXUnit (INCHES, head);
		double dPrintRes = CBaseElement::CalcPrintRes (head);
		double dFactor = dPrintRes / dScreenRes;

		size.cx			= (int)((double)size.cx			* dFactor);
		sizePrinter.cx	= (int)((double)sizePrinter.cx	* dFactor);
		sizeStretch.cx	= (int)((double)sizeStretch.cx	* dFactor);
	}

#endif

	size.cy			= (int)((double)size.cy			/ dStretch [1]);
	sizePrinter.cy	= (int)((double)sizePrinter.cy	/ dStretch [1]);

	swap (sizeNormal.cx, sizeNormal.cy);
	swap (sizePrinter.cx, sizePrinter.cy);

	sizeStretch = CBaseElement::ThousandthsToLogical (sizeStretch, head);
	swap (sizeStretch.cx, sizeStretch.cy);
	sizeStretch.cx = (int)((double)sizeStretch.cx / dStretch [1]);

	if (!bCalcSizeOnly) {
		int x = 0, y = 0, cx = size.cx, cy = size.cy;
		CDC dcMem;

		if (context == EDITOR)
			::BitBlt (dc, x, y, cx, cy, NULL, 0, 0, WHITENESS);
		else
			::BitBlt (dc, x, y, cy, cx, NULL, 0, 0, WHITENESS);

		if (IsFlippedV ()) {
			y += (cy - 1);
			cy *= -1;
		}
		if (IsFlippedH ()) {
			x += (cx - 1);
			cx *= -1;
		}

		VERIFY (dcMem.CreateCompatibleDC (&dc));

		if (context == PRINTER) {
			if (!m_pbmpPrinter) {
				CxImage img;

				{
					CBitmap bmp;

					m_bmpEditor.GetBitmap (bmp);
					img.CreateFromHBITMAP (bmp);
				}

				if (!IsInverse ())
					img.Negative ();

				if (IsFlippedH ()) 
					img.Mirror ();
				if (!IsFlippedV ()) 
					img.Flip ();

				img.RotateRight ();
				//img.Resample (Round (sizePrinter.cx, 8), Round (sizePrinter.cy, 8));
				img.Resample (sizePrinter.cx, sizePrinter.cy);

				if (HBITMAP h = img.MakeBitmap (dc)) {
					m_pbmpPrinter = new CRawBitmap (h);
					::DeleteObject (h);
				}
			}

			if (m_pbmpPrinter) {
				if (HBITMAP hbmp = m_pbmpPrinter->GetHBITMAP ()) {
					CxImage img;
		
					img.CreateFromHBITMAP (hbmp);

					TRACEF (DB::ToString (sizePrinter.cx) + _T (", ") + DB::ToString (sizePrinter.cy));
					TRACEF (DB::ToString (img.GetWidth ()) + _T (", ") + DB::ToString (img.GetHeight ()));

					if (nDither == 0 || nDither == 100)
						img.Draw (dc, 0, 0, img.GetWidth (), sizePrinter.cy);
					else {
						CDC dcMem, dcPat, dcDither;
						CBitmap bmpPat, bmpDither;

						CSize size (img.GetWidth (), sizePrinter.cy);

						VERIFY (dcMem.CreateCompatibleDC (&dc));
						VERIFY (dcPat.CreateCompatibleDC (&dc));
						VERIFY (dcDither.CreateCompatibleDC (&dc));

						VERIFY (bmpPat.CreateCompatibleBitmap (&dc, size.cx, size.cy));
						VERIFY (bmpDither.CreateCompatibleBitmap (&dc, size.cx, size.cy));
						CBitmap * pDither = dcDither.SelectObject (&bmpDither);
						HBITMAP hbmpPrinter = img.MakeBitmap ();
						HBITMAP hbmpMem = (HBITMAP)::SelectObject (dcMem, hbmpPrinter);

						CxImage img = CTextElement::MakePatternDither (100 - nDither, context);
						HBITMAP hbmp = img.MakeBitmap ();
						//SAVEBITMAP (hbmp, _T ("C:\\Foxjet\\hbmp9.bmp"));

						HBRUSH hbrush = ::CreatePatternBrush (hbmp);
						HGDIOBJ hOld = ::SelectObject (dcPat, hbrush);

						CBitmap * pOld = dcPat.SelectObject (&bmpPat);

						dcPat.PatBlt (0, 0, size.cx, size.cy, PATCOPY);
						//SAVEBITMAP (bmpPat, _T ("C:\\Foxjet\\bmpPat.bmp"));

						if (!IsInverse ()) {
							//SAVEBITMAP (dcMem, _T ("C:\\Foxjet\\bmpMem.bmp"));
							::BitBlt (dcDither, 0, 0, size.cx, size.cy, NULL, 0, 0, WHITENESS);
							//SAVEBITMAP (dcDither, _T ("C:\\Foxjet\\dc0.bmp"));
							dcDither.BitBlt (0, 0, size.cx, size.cy, &dcMem, 0, 0, SRCCOPY);
							//SAVEBITMAP (dcDither, _T ("C:\\Foxjet\\dc1.bmp"));
							dcDither.BitBlt (0, 0, size.cx, size.cy, &dcPat, 0, 0, SRCAND);
							//SAVEBITMAP (dcDither, _T ("C:\\Foxjet\\dc2.bmp"));
							dc.BitBlt (0, 0, size.cx, size.cy, &dcDither, 0, 0, SRCCOPY);
							//SAVEBITMAP (dc, _T ("C:\\Foxjet\\dc3.bmp"));
							// img.Draw (dc, 0, 0, img.GetWidth (), sizePrinter.cy);
						}
						else {
							//SAVEBITMAP (dcMem, _T ("C:\\Foxjet\\bmpMem.bmp"));
							::BitBlt (dcDither, 0, 0, size.cx, size.cy, NULL, 0, 0, WHITENESS);
							//SAVEBITMAP (dcDither, _T ("C:\\Foxjet\\dc0.bmp"));
							dcDither.BitBlt (0, 0, size.cx, size.cy, &dcMem, 0, 0, SRCINVERT);
							//SAVEBITMAP (dcDither, _T ("C:\\Foxjet\\dc1.bmp"));
							dcDither.BitBlt (0, 0, size.cx, size.cy, &dcPat, 0, 0, SRCPAINT);
							//SAVEBITMAP (dcDither, _T ("C:\\Foxjet\\dc2.bmp"));
							dc.BitBlt (0, 0, size.cx, size.cy, &dcDither, 0, 0, SRCINVERT);
							//SAVEBITMAP (dc, _T ("C:\\Foxjet\\dc3.bmp"));
							// img.Draw (dc, 0, 0, img.GetWidth (), sizePrinter.cy);
						}

						dcDither.SelectObject (pDither);
						dcPat.SelectObject (pOld);
						::SelectObject (dcMem, hbmpMem);
						::SelectObject (dcPat, hOld);
						::DeleteObject (hbrush);
						::DeleteObject (hbmp);
						::DeleteObject (hbmpPrinter);
					}

					::DeleteObject (hbmp);
				}
			}
		}
		else {
			CBitmap bmp;
			CSize size = m_bmpEditor.GetSize ();

			m_bmpEditor.GetBitmap (bmp);
			CBitmap * pOld = dcMem.SelectObject (&bmp);

			if (nDither == 0 || nDither == 100) 
				dc.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, size.cx, size.cy, IsInverse () ? SRCINVERT : SRCCOPY);
			else {
				CDC dcPat;
				CBitmap bmpPat;
				const CSize sizeBmp = size;
				CSize size = ThousandthsToLogical (m_size, head); 

				size.cx = ceil ((double)size.cx * dStretch [0]); 
				size.cy = ceil ((double)size.cy / dStretch [1]);
				
				VERIFY (dcPat.CreateCompatibleDC (&dc));
				VERIFY (bmpPat.CreateCompatibleBitmap (&dc, size.cx, size.cy));

				CxImage img = CTextElement::MakePatternDither (nDither, context);
				HBITMAP hbmp = img.MakeBitmap ();
				HBRUSH hbrush = ::CreatePatternBrush (hbmp);
				HGDIOBJ hOld = ::SelectObject (dcPat, hbrush);

				CBitmap * pOld = dcPat.SelectObject (&bmpPat);

				dcPat.PatBlt (0, 0, size.cx, size.cy, PATCOPY);

				if (!IsInverse ()) {
					dc.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, sizeBmp.cx, sizeBmp.cy, SRCINVERT);
					dc.BitBlt (0, 0, size.cx, size.cy, &dcPat, 0, 0, SRCAND);
					dc.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, sizeBmp.cx, sizeBmp.cy, SRCINVERT);
				}
				else {
					dc.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, sizeBmp.cx, sizeBmp.cy, SRCINVERT);
					dc.BitBlt (0, 0, size.cx, size.cy, &dcPat, 0, 0, SRCPAINT);
					dc.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, sizeBmp.cx, sizeBmp.cy, SRCINVERT);
				}

				dcPat.SelectObject (pOld);
				::SelectObject (dcPat, hOld);
				::DeleteObject (hbrush);
				::DeleteObject (hbmp);
			}

			dcMem.SelectObject (pOld);
		}
	}

	//{ CString str; str.Format (_T ("CBitmapElement::Draw: %d, %d (%d, %d)"), m_pt.x, m_pt.y, m_size.cx, m_size.cy); TRACEF (str); }

	if (context == PRINTER)
		return sizePrinter;
	else {
		CSize sizeLogical = m_size;
		
		sizeLogical.cx = (int)((double)sizeLogical.cx * dStretch [0]);
		sizeLogical.cy = (int)((double)sizeLogical.cy / dStretch [1]);

		return sizeLogical;
	}
}

bool CBitmapElement::SetSize (const CSize & size, const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI)
{
	const CSize min = GetMinSize ();
	CSize adjust (size);

	if (!bHeadDPI) {
		double dStretch [2] = { 1, 1 };
	
		CBaseElement::CalcStretch (head, dStretch);
		adjust.cy = (int)((double)adjust.cy / dStretch [1]);
	}

	if ((adjust.cx >= min.cx) && (adjust.cy >= min.cy)) {
		if (!m_bMonochrome) {
			CSize sizeOld = ThousandthsToLogical (m_size, head);
			CSize sizeNew = ThousandthsToLogical (adjust, head);
			CSize diff (abs (sizeOld.cx - sizeNew.cx), abs (sizeOld.cy - sizeNew.cy));
	
			if (diff.cx > 1 || diff.cy > 0)
				m_bResizeOnRedraw = true;
		}

		m_size = adjust;
		SetRedraw ();

		return true;
	}

	return false;
}

CSize CBitmapElement::GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI) const
{
	CSize size (m_size);

	if (!bHeadDPI) {
		double dStretch [2] = { 1, 1 };
	
		CBaseElement::CalcStretch (head, dStretch);
		size.cy = (int)((double)size.cy * dStretch [1]);
	}

#ifdef __WYSIWYGFIX__

	double dScreenRes = CCoord::GetPixelsPerXUnit (INCHES, head);
	double dPrintRes = CBaseElement::CalcPrintRes (head);
	double dFactor = dPrintRes / dScreenRes;

	size.cx = (int)((double)size.cx * dFactor);
#endif

	return size;
}

CSize CBitmapElement::GetDbSize (const CSize & size, const CString & strWidthField, const CString & strHeightField)
{
	CSize result (size);

	if (!strWidthField.CompareNoCase (CDatabaseElement::GetAuto ()))
		result.cx = DBSIZE_AUTO;
	else if (!strWidthField.CompareNoCase (CDatabaseElement::GetStatic ()))
		result.cx = DBSIZE_STATIC;
	else if (!strWidthField.CompareNoCase (CDatabaseElement::GetNative ()))
		result.cx = DBSIZE_NATIVE;
		
	if (!strHeightField.CompareNoCase (CDatabaseElement::GetAuto ()))
		result.cy = DBSIZE_AUTO;
	else if (!strHeightField.CompareNoCase (CDatabaseElement::GetStatic ()))
		result.cy = DBSIZE_STATIC;
	else if (!strHeightField.CompareNoCase (CDatabaseElement::GetNative ()))
		result.cy = DBSIZE_NATIVE;
		
	return result;
}

CSize CBitmapElement::GetDbSize () const
{
	return GetDbSize (m_size, GetDbWidthField (), GetDbHeightField ());
}

CPoint CBitmapElement::NextWholePixel (const CPoint & size, const FoxjetDatabase::HEADSTRUCT & h)
{
	CSize pixel ((int)Round (1.0 / h.GetRes () * 1000.0), (int)Round (h.GetSpan () / h.GetChannels () * 1000.0));
	CSize result (size);

	result.cx = (int)(ceil (result.cx / (double)pixel.cx) * (double)pixel.cx);
	result.cy = (int)(ceil (result.cy / (double)pixel.cy) * (double)pixel.cy);

	return result;
}

CSize CBitmapElement::CalcDatabasePromptedSize (const CString & strPath)
{
	const HEADSTRUCT & h = GetHead ();
	CSize sizeAuto = GetSize1000ths (strPath);
	CSize sizeNative = GetSizePixels (strPath, GetHead ());
	CSize sizeStatic = m_sizeStatic; //m_size;
	CSize sizeDB = m_db.m_sizeDB; // p->GetBitmapSize ();
	CSize size (sizeAuto);
	CSize type = GetDbSize ();

	sizeNative.cx = (int)((double)sizeNative.cx / h.GetRes () * 1000.0);
	sizeNative.cy = (int)((double)sizeNative.cy / (h.GetChannels () / h.GetSpan ()) * 1000.0);

	TRACER (strPath);

	TRACEF (_T ("auto:   ") + DB::ToString (sizeAuto.cx) + _T (", ") + DB::ToString (sizeAuto.cy));
	sizeAuto	= NextWholePixel (sizeAuto, h);
	TRACEF (_T ("auto:   ") + DB::ToString (sizeAuto.cx) + _T (", ") + DB::ToString (sizeAuto.cy));

	TRACEF (_T ("native: ") + DB::ToString (sizeNative.cx) + _T (", ") + DB::ToString (sizeNative.cy));
	sizeNative	= NextWholePixel (sizeNative, h);
	TRACEF (_T ("native: ") + DB::ToString (sizeNative.cx) + _T (", ") + DB::ToString (sizeNative.cy));
						
	//sizeStatic	= NextWholePixel (sizeStatic, h);
	//sizeDB		= NextWholePixel (sizeDB, h);

	TRACEF (_T ("static: ") + DB::ToString (sizeStatic.cx) + _T (", ") + DB::ToString (sizeStatic.cy));
	TRACEF (_T ("db:     ") + DB::ToString (sizeDB.cx) + _T (", ") + DB::ToString (sizeDB.cy));

	#ifndef _DEBUG
	TRACER (_T ("auto:   ") + DB::ToString (sizeAuto.cx) + _T (", ") + DB::ToString (sizeAuto.cy));
	TRACER (_T ("native: ") + DB::ToString (sizeNative.cx) + _T (", ") + DB::ToString (sizeNative.cy));
	TRACER (_T ("static: ") + DB::ToString (sizeStatic.cx) + _T (", ") + DB::ToString (sizeStatic.cy));
	TRACER (_T ("db:     ") + DB::ToString (sizeDB.cx) + _T (", ") + DB::ToString (sizeDB.cy));
	#endif 

	if (type.cx == DBSIZE_AUTO)
		size.cx = sizeAuto.cx;
	else if (type.cx == DBSIZE_STATIC)
		size.cx = sizeStatic.cx;
	else if (type.cx == DBSIZE_NATIVE)
		size.cx = sizeNative.cx;
	else
		size.cx = sizeDB.cx;
							
	if (type.cy == DBSIZE_AUTO)
		size.cy = sizeAuto.cy;
	else if (type.cy == DBSIZE_STATIC)
		size.cy = sizeStatic.cy;
	else if (type.cy == DBSIZE_NATIVE)
		size.cy = sizeNative.cy;
	else
		size.cy = sizeDB.cy;

	TRACEF (_T ("size:   ") 
		+ DB::ToString (size.cx) + _T (" [") + GetDbWidthField () + _T ("], ") 
		+ DB::ToString (size.cy) + _T (" [") + GetDbHeightField () + _T ("]"));
	TRACER (_T ("size:   ") + DB::ToString (size.cx) + _T (", ") + DB::ToString (size.cy));

	return size;
}

int CBitmapElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	if (m_db.m_bLookup) {
		//LOCK (::csDatabase);

		if (type == INITIAL || m_db.m_bInvalid) {
			CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, CopyElement (&ListGlobals::defElements.GetElement (DATABASE), &GetHead (), false));

			if (p) {
				p->SetPromptAtTaskStart (m_db.m_bPrompt);

				if (m_db.m_strDSN.GetLength () && m_db.m_strTable.GetLength () && m_db.m_strField.GetLength ()) {
					p->SetDSN (m_db.m_strDSN);
					p->SetTable (m_db.m_strTable);
					p->SetField (m_db.m_strField);
					p->SetKeyField (m_db.m_strKeyField);
					p->SetKeyValue (m_db.m_strKeyValue);
					p->SetBmpWidthField (m_db.m_strBmpWidthField);
					p->SetBmpHeightField (m_db.m_strBmpHeightField);
					p->Build (type, bCanThrowException);

					m_db.m_nSQLType = p->GetSQLType ();
					m_db.m_sizeDB = p->GetBitmapSize ();
					
					CString strPath = p->GetDefaultData ();

					strPath.Trim ();
					TRACEF (strPath);

					if (strPath.GetLength () && strPath != p->GetEmptyString ()) {
						if (strPath.Find (_T ("\\")) == -1)
							strPath = CBitmapElement::GetNetworkFilePath () + _T ("\\") + strPath;

						TRACER (strPath);
						TRACEF (strPath);
						Trace (strPath);

						if (SetFilePath (strPath, m_head)) {
							CSize size = CalcDatabasePromptedSize (strPath);

							SetSize (size, GetHead ());
						}
						else 
							SetFilePath (m_strFilePath, m_head);
					}
					else {
						SetFilePath (BLANK_BMP, m_head);
					}
				}

				delete p;
			}
			/* TODO: rem
			CString strSQL = COdbcDatabase::CreateSQL (NULL, GetDatabaseDSN (), GetDatabaseTable (), GetDatabaseField (), GetDatabaseKeyField (), GetDatabaseKeyValue (), SQL_VARCHAR);
			
			try {
				COdbcCache db = COdbcDatabase::Find (GetDatabaseDSN ());
				COdbcCache rstCache = COdbcDatabase::Find (db.GetDB (), GetDatabaseDSN (), strSQL);
				COdbcRecordset & rst = rstCache.GetRst ();

				CODBCFieldInfo info;
				int nIndex = 0;

				CString strField = GetDatabaseField ();

				if (!rst.IsEOF ()) {
					nIndex = rst.GetFieldIndex (strField);
					CString str = (CString)rst.GetFieldValue (strField);
					SetFilePath (str, m_head);
				}
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }
			*/
		}

		m_db.m_bInvalid = false;
	}

	return GetClassID ();
}

CString CBitmapElement::GetFilePath() const
{
	return m_strFilePath;
}

CString CBitmapElement::GetDefaultData() const
{
	return GetFilePath ();
}

CString CBitmapElement::GetImageData() const
{
	return GetDefaultData ();
}

CSize CBitmapElement::GetMinSize()
{
	int nMin = TextElement::CTextElement::m_lmtFontSize.m_dwMin;

	return CSize (nMin, nMin);
}

CString CBitmapElement::FindFile(const CString &strPath, const CString &strTitle, const FoxjetDatabase::HEADSTRUCT & head, const CSize & sizeDef, bool bMonochrome)
{
	CStringArray v;

	GetFiles (strPath, _T ("*.*"), v);

	for (int i = 0; i < v.GetSize (); i++) {
		CString strDir = v [i];
		CString strTmp (strDir);

		strTmp.Replace (strPath + _T ("\\"), _T (""));

		if (strTmp.CompareNoCase (_T (".")) != 0 && strTmp.CompareNoCase (_T ("..")) != 0) {
			if (::GetFileAttributes (strDir) & FILE_ATTRIBUTE_DIRECTORY) {
				CString str = strDir + _T ("\\") + strTitle;

				if (HBITMAP hBmp = LoadBitmap (str, head, sizeDef, &bMonochrome)) {
					::DeleteObject (hBmp);
					return str;
				}
				else {
					CString str = FindFile (strDir, strTitle, head, sizeDef, bMonochrome);

					if (str.GetLength ())
						return str;
				}
			}
		}
	}

	return _T ("");
}

bool CBitmapElement::LoadBmp64(const CString &strPath, const FoxjetDatabase::HEADSTRUCT & head, const CSize & sizeDef)
{
	CString str = Extract (strPath, _T (":bmp64:"), _T (":"));

	if (str.GetLength ()) {
		HBITMAP hBmp = NULL;

		{
			CString strCompressed = strPath;
		
			strCompressed.Replace (CString ((TCHAR)0x0a), _T (""));
			strCompressed.Replace (CString ((TCHAR)0x0d), _T (""));
			strCompressed = Extract (strCompressed, _T (":bmp64:"), _T (":"));

			if (strCompressed.GetLength ()) {
				CStringArray v;

				Tokenize (strCompressed, v);
	
				ULONG lUncompressed = _ttoi (GetParam (v, 0));
				CString str64 = GetParam (v, 1);

				block <UCHAR> decoded (lUncompressed);
				block <UCHAR> uncompressed (lUncompressed);

				//TRACEF ((std::wstring)block <char> (w2a (str64), str64.GetLength ()));

				int n = DecodeBase64 ((UCHAR *)(LPCSTR)w2a (str64), decoded, str64.GetLength ());
				//TRACEF (_T ("DecodeBase64: ") + (std::wstring)block <UCHAR> (decoded.get (), n));

				int nUncompress = uncompress (uncompressed, &lUncompressed, decoded, n);
				//TRACEF (_T ("uncompress: ") + (std::wstring)block <UCHAR> (uncompressed.get (), lUncompressed));

				if (nUncompress == Z_OK) { 
					CxImage img;
					BITMAPFILEHEADER file;
					BITMAPINFOHEADER info;

					memcpy (&file, uncompressed, sizeof (file));
					memcpy (&info, uncompressed + sizeof (file), sizeof (info));

					/*
					#ifdef _DEBUG
					{
						block <UCHAR> encoded (_ttoi (GetParam (v, 0)) * 1.1);

						int nEncoded = EncodeBase64 (uncompressed, lUncompressed, encoded, encoded.m_lBytes);
						CString str = a2w (encoded.get ());
						str.Replace (_T ("\r"), _T (""));
						str.Replace (_T ("\n"), _T (""));
						TRACEF (_T ("EncodeBase64: ") + (std::wstring)block <BYTE> (encoded, nEncoded));
						TRACEF (str);
					}
					#endif
					*/

					int nBytesPerLine = ((info.biBitCount * info.biWidth + 31) / 32) * 4;

					if (img.CreateFromArray (uncompressed + file.bfOffBits, info.biWidth, info.biHeight, info.biBitCount, nBytesPerLine, false)) {
						if (hBmp = img.MakeBitmap ()) {
							//SAVEBITMAP (hbmp, GetHomeDir () + _T ("\\Debug\\uncompressed.bmp"));
						}
					}
				}
			}
		}

		if (hBmp) {
			BOOL bAttach = m_bmpEditor.Load (hBmp); 
			ASSERT (bAttach);

			SetRedraw ();

			{
				DIBSECTION ds = { 0 };

				::GetObject (hBmp, sizeof (ds), &ds);

				CSize sizeNative (ds.dsBm.bmWidth, ds.dsBm.bmHeight);

				sizeNative.cx = (int)((double)sizeNative.cx / head.GetRes () * 1000.0);
				sizeNative.cy = (int)((double)sizeNative.cy / (head.GetChannels () / head.GetSpan ()) * 1000.0);
				sizeNative	= FoxjetElements::CBitmapElement::NextWholePixel (sizeNative, head);

				SetSize (sizeNative, head);
			}

			m_strFilePath = strPath;
			m_strFilePath.Replace (CString ((TCHAR)0x0a), _T (""));
			m_strFilePath.Replace (CString ((TCHAR)0x0d), _T (""));
			return true;
		}
	}

	return false;
}

bool CBitmapElement::LoadZ64(const CString &strPath, const FoxjetDatabase::HEADSTRUCT & head, const CSize & sizeDef)
{
	CString str = Extract (strPath, _T (":Z64:"), _T (":"));

	if (str.GetLength ()) {
		HBITMAP hBmp = NULL;
		CStringArray v;

		Tokenize (strPath, v);

		{
			CString str = strPath;
		
			str.Replace (CString ((TCHAR)0x0a), _T (""));
			str.Replace (CString ((TCHAR)0x0d), _T (""));
			str = Extract (str, _T (":Z64:"), _T (":"));

			if (str.GetLength ()) {

				//#ifdef _DEBUG
				//for (int i = 0; i < v.GetSize (); i++)
				//	TRACEF (FoxjetCommon::Format (v [i], v [i].GetLength ()));
				//#endif

				if (v.GetSize () >= 4) {
					const int nBytes = _ttoi (GetParam (v, 1));
					const int nBytesPerRow = _ttoi (GetParam (v, 3));
					const int cx = nBytesPerRow * 8;
					const int cy = nBytes / nBytesPerRow;
					const int nBuffer = nBytes * 2;
					ULONG lUncompressed = nBuffer;
					UCHAR * pDecoded = new UCHAR [nBuffer];
					UCHAR * pUncompressed = new UCHAR [nBuffer];

					//TRACEF (_T ("[") + FoxjetDatabase::ToString (str.GetLength ()) + _T ("] ") + FoxjetCommon::Format (str, str.GetLength ()));
					memset (pDecoded, 0, nBuffer);
					memset (pUncompressed, 0, nBuffer);
					int n = DecodeBase64 ((UCHAR *)(LPCSTR)w2a (str), pDecoded, str.GetLength ());
					ASSERT (n);
					ASSERT (n <= nBuffer);
			
					int nUncompress = uncompress (pUncompressed, &lUncompressed, pDecoded, n);

					if (nUncompress != Z_OK) { 
						switch (nUncompress) {
						case Z_MEM_ERROR:	TRACEF ("Z_MEM_ERROR");		break;
						case Z_BUF_ERROR:	TRACEF ("Z_BUF_ERROR");		break;
						case Z_DATA_ERROR:	TRACEF ("Z_DATA_ERROR");	break;
						}
						ASSERT (0); 
					}
					else {
						CxImage img;

						if (img.CreateFromArray (pUncompressed, cx, cy, 1, nBytesPerRow, true)) {
							img.Negative ();
							hBmp = img.MakeBitmap (); 
						}
					}

					delete [] pDecoded;
					delete [] pUncompressed;
				}
			}
		}

		if (hBmp) {
			int nBytes = _ttoi (GetParam (v, 1));
			int nBytesPerRow = _ttoi (GetParam (v, 3));
			int cx = nBytesPerRow * 8;
			int cy = nBytes / nBytesPerRow;

			BOOL bAttach = m_bmpEditor.Load (hBmp); 
			ASSERT (bAttach);

			CSize size = CBaseElement::LogicalToThousandths (CPoint (cx, cy), head); //CSize size = CBaseElement::LogicalToThousandths (m_bmpEditor.GetSize (), head);
			size.cy = CBaseElement::StretchByAngle (size.cy, head.m_dHeadAngle);

			SetSize (size, head);
			SetRedraw ();
			SetInverse (true);
			m_strFilePath = strPath;
			m_strFilePath.Replace (CString ((TCHAR)0x0a), _T (""));
			m_strFilePath.Replace (CString ((TCHAR)0x0d), _T (""));
			return true;
		}
	}

	return false;
}

bool CBitmapElement::SetFilePath(const CString &strPath, const FoxjetDatabase::HEADSTRUCT & head, const CSize & sizeDef)
{
	bool bResult = false;
	CString strFilepath (strPath);

	if (strPath.Find (_T (":bmp64:")) != -1) 
		return LoadBmp64 (strPath, head, sizeDef);
	else if (strPath.Find (_T (":Z64:")) != -1) 
		return LoadZ64 (strPath, head, sizeDef);

	strFilepath.Trim ();

	bool bBlank = strFilepath.IsEmpty ();

	if (!bBlank) { 
		CString str = strPath;
		str.Replace ('\\', '/');
		std::vector <std::wstring> v = explode (std::wstring (str), '/');

		if (v.size () <= 1) 
			strFilepath = GetDefPath () + strFilepath;
	}

	bBlank |= !strFilepath.CompareNoCase (GetDefPath ()) || strPath.Find (BLANK_BMP) != -1;

	if (bBlank) {
		const CString strEmpty = GetDefPath () + BLANK_BMP;

		if (::GetFileAttributes (strEmpty) == -1) {
			HINSTANCE hInst = GetInstanceHandle (); 
			HBITMAP hBmp = (HBITMAP)::LoadImage (hInst, MAKEINTRESOURCE (IDB_BLANK), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_MONOCHROME);
			bool bSave = SaveBitmap (hBmp, strEmpty);
		}

		strFilepath = strEmpty;
	}

	#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
	CString str;
	str.Format ("CBitmapElement::SetFilePath(%s)", strFilepath);
	TRACEF (str);
	#endif //_DEBUG && __TRACE_TOFROMSTRING__


	Free ();

	HBITMAP hBmp = LoadBitmap (strFilepath, head, sizeDef, &m_bMonochrome);

	if (!hBmp && IsNetworked ()) {
		CString strNetwork = GetNetworkFilePath ();
		CString strLocal = GetLocalFilePath ();

		strFilepath.Replace (strNetwork, strLocal);
		hBmp = LoadBitmap (strFilepath, head, sizeDef, &m_bMonochrome);

		if (hBmp) {
			TRACER (_T ("substituting: ") + strFilepath);
		}
	}

	if (!hBmp && IsNetworked ()) {
		CString strNetwork = GetNetworkFilePath ();
		CString strLocal = GetLocalFilePath ();

		strFilepath.Replace (strLocal, strNetwork);
		hBmp = LoadBitmap (strFilepath, head, sizeDef, &m_bMonochrome);

		if (hBmp) {
			TRACER (_T ("substituting: ") + strFilepath);
		}
	}

	if (!hBmp) {
		int nIndex = strFilepath.ReverseFind ('\\');

		TRACER (_T ("NOT FOUND: ") + strPath);

		if (nIndex != -1) {
			CString strTitle = strFilepath.Mid (nIndex + 1);
			CString strFolder = strFilepath.Left (nIndex + 1);

			if (!strTitle.CompareNoCase (_T ("Def.bmp"))) {
				{
					CString str (strFolder);
		
					str.Replace (_T ("\\"), _T ("/"));

					std::vector <std::wstring> v = explode ((std::wstring)(LPCTSTR)str, '/');

					if (v.size ()) {
						TRACEF (v [0].c_str ());

						DWORD dw = FoxjetFile::GetFileAttributesTimeout (v [0].c_str ());

						if (dw != -1)
							CreateDir (strFolder);
					}
				}

				LoadDefResBitmap (head, strFolder);
				Trace ();
				return true;
			}

			CString strFind = FindFile (GetHomeDir (), strTitle, head, sizeDef, m_bMonochrome);

			if (strFind.GetLength ()) {
				if (hBmp = LoadBitmap (strFind, head, sizeDef, &m_bMonochrome)) {
					TRACEF (strPath + _T (" --> ") + strFind);
					strFilepath = strFind;
				}
			}
		}
	}

	#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
	str.Format ("hBmp = 0x%08X", hBmp);
	TRACEF (str);
	#endif //_DEBUG && __TRACE_TOFROMSTRING__ 

	if (hBmp != NULL) {
		bResult = true;
		m_strFilePath = strFilepath;
		
		BOOL bAttach = m_bmpEditor.Load (hBmp); 
		ASSERT (bAttach);

		CSize size = CBaseElement::LogicalToThousandths (m_bmpEditor.GetSize (), head);
		size.cy = CBaseElement::StretchByAngle (size.cy, head.m_dHeadAngle);

		SetSize (size, head);
		SetRedraw ();

		{
			CxImage img;

			TRACEF (strPath);
			CSize size = m_bmpEditor.GetSize ();
			TRACEF (FoxjetDatabase::ToString (size.cx) + _T (", ") + FoxjetDatabase::ToString (size.cy));

			size = CBaseElement::LogicalToThousandths (m_bmpEditor.GetSize (), head);
			size.cy = CBaseElement::StretchByAngle (size.cy, head.m_dHeadAngle);
			TRACEF (FoxjetDatabase::ToString (size.cx) + _T (", ") + FoxjetDatabase::ToString (size.cy));

			TRACEF (FoxjetDatabase::ToString (GetSize (head).cx) + _T (", ") + FoxjetDatabase::ToString (GetSize (head).cy));

			img.CreateFromHBITMAP (hBmp);
			size = CSize (img.GetWidth (), img.GetHeight ());
			TRACEF (FoxjetDatabase::ToString (size.cx) + _T (", ") + FoxjetDatabase::ToString (size.cy));
		}

		#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
		BITMAP b;
		CBitmap * p = CBitmap::FromHandle (hBmp);
		p->GetBitmap (&b);

		str.Format (
			"\n\tbmType        = %d"
			"\n\tbmWidth       = %d"
			"\n\tbmHeight      = %d"
			"\n\tbmWidthBytes  = %d"
			"\n\tbmPlanes      = %d"
			"\n\tbmBitsPixel   = %d"
			"\n\tbmBits        = 0x%08X",
			b.bmType,
			b.bmWidth,
			b.bmHeight,
			b.bmWidthBytes,
			b.bmPlanes,
			b.bmBitsPixel,
			b.bmBits);
		TRACEF (str);
		#endif //_DEBUG	&& __TRACE_TOFROMSTRING__
	}

	#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
	TRACEF ("SetFilePath = " + CString (bResult ? "true" : "false"));
	#endif //_DEBUG && __TRACE_TOFROMSTRING__

	if (!bResult)
		LoadDefResBitmap (head);

	Trace ();

	return bResult;
}

void CBitmapElement::Trace (const CString & strPathIn)
{
	CString str;
	CString strPath = strPathIn.IsEmpty () ? m_strFilePath : strPathIn;
	DWORD dwAttr = ::GetFileAttributes (strPath);

	{
		CString str;

		str.Format (_T ("0x%p: %s"), dwAttr, strPath);
		TRACEF (str);
		TRACER (str);
	}

#if 0
	if (dwAttr != -1) {
		DWORD dwSize = GetFileSize (strPath);

		str.Format (_T ("[%s] attr: 0x%p [%d bytes]"), CVersion (::verApp).Format (), dwAttr, dwSize);
		TRACEF (str);
		TRACER (str);

		FoxjetDatabase::ACL::CACLInfo info((LPCTSTR)strPath);

		info.Query();

		std::vector <FoxjetDatabase::ACL::PERMISSION> v = info.GetPermissions();

		for (int n = 0; n < v.size(); n++) {
			CString s;

			s.Format(_T("%d%d%d [%s]: %s\\%s"), v[n].m_mod.owner, v[n].m_mod.group, v[n].m_mod.other, CString(to_string(v[n].m_mod).c_str()),
				v[n].m_strDomain.c_str(),
				v[n].m_strName.c_str());
			TRACER(s);
		}
	}
#endif

}

bool CBitmapElement::Free()
{
	bool bResult = false;

	if (m_pbmpPrinter) {
		delete m_pbmpPrinter;
		m_pbmpPrinter = NULL;
	}

	return bResult;
}

// this returns the size in pixels
CSize CBitmapElement::GetSizePixels(const CString &strFilename, const FoxjetDatabase::HEADSTRUCT & head)
{
	CSize size (0, 0);
	HBITMAP hBmp = LoadBitmap (strFilename, head);
	BITMAP bm;

	memset (&bm, 0, sizeof (BITMAP));
	::GetObject (hBmp, sizeof (bm), &bm);
	size.cx = bm.bmWidth;
	size.cy = bm.bmHeight;

	if (hBmp) 
		::DeleteObject (hBmp);

	return size;
}


CSize CBitmapElement::GetSize1000ths (const CString & strFilename)
{
	CSize size (0, 0);
	CString strExt = FoxjetFile::GetFileExt (strFilename);

	if (!strExt.CompareNoCase (_T ("bmp"))) {
		if (FILE * fp = _tfopen (strFilename, _T ("rb"))) {
			BITMAPFILEHEADER file = { 0 };
			BITMAPINFOHEADER info = { 0 };
			//BITMAPV5HEADER info = { 0 };

			fread (&file, sizeof (file), 1, fp);
			fread (&info, sizeof (info), 1, fp);
			fclose (fp);

			CSize res (
				info.biXPelsPerMeter > 0 ? Round (info.biXPelsPerMeter / 39.3701) : 96, 
				info.biYPelsPerMeter > 0 ? Round (info.biYPelsPerMeter / 39.3701) : 96); 

			size = CSize ((int)((double)info.biWidth / (double)res.cx * 1000.0), (int)((double)info.biHeight / (double)res.cy * 1000.0));
		}
	}
	else {
		CxImage img;

		if (img.Load (strFilename)) {
			CSize res (96, 96);

			size = CSize ((int)((double)img.GetWidth () / (double)res.cx * 1000.0), (int)((double)img.GetHeight () / (double)res.cy * 1000.0));
		}
	}

	return size;
}


HBITMAP CBitmapElement::LoadBitmap(const CString &strFile, const FoxjetDatabase::HEADSTRUCT & head, const CSize & size, bool * pbMonochrome)
{
//	DECLARETRACECOUNT (20);

	if (FoxjetFile::GetFileAttributesTimeout (strFile) == -1) 
		return NULL;

	HBITMAP hBmp = NULL;
	DWORD dwType = GetCxImageType (strFile);

	if (FoxjetCommon::verApp >= CVersion (1, 20)) { // TODO: rem
//		if (dwType != CXIMAGE_FORMAT_BMP) {
			CxImage img;

//			MARKTRACECOUNT ();

			if (img.Load (strFile, dwType)) {
//				MARKTRACECOUNT ();

				if (img.IsValid ()) {
					bool bMonochrome = IsMonochrome (strFile);

					if (pbMonochrome)
						* pbMonochrome = bMonochrome;

					if (!bMonochrome) {
						if (size.cx <= 0 || size.cy <= 0) {
							img.Dither ();
							hBmp = img.MakeBitmap (NULL);
						}
						else {
							CSize sizeLogical = CBaseElement::ThousandthsToLogical (size, head);
							double dStretch [2] = { 1.0, 1.0 };
							double d = CCoord::GetPixelsPerYUnit (INCHES, head) / ((double)head.GetChannels () / (double)head.m_dNozzleSpan);
							CBitmap bmpTmp;
							CxImage tmp;
							CDC dcTmp;

							CalcStretch (head, dStretch);

							sizeLogical.cx = (int)((double)sizeLogical.cx * dStretch [0]);
							sizeLogical.cy = (int)((double)sizeLogical.cy / d);

							{
								HDC hDC = ::GetDC (NULL);
			
								dcTmp.CreateCompatibleDC (CDC::FromHandle (hDC));
								bmpTmp.CreateCompatibleBitmap (CDC::FromHandle (hDC), sizeLogical.cx, sizeLogical.cy);

								::ReleaseDC (NULL, hDC);
							}

							CBitmap * pBmp = dcTmp.SelectObject (&bmpTmp);
							dcTmp.BitBlt (0, 0, sizeLogical.cx, sizeLogical.cy, NULL, 0, 0, WHITENESS);
							img.Draw2 (dcTmp, 0, 0, sizeLogical.cx, sizeLogical.cy);
							dcTmp.SelectObject (pBmp);

							tmp.CreateFromHBITMAP ((HBITMAP)bmpTmp);
							tmp.Dither ();
							hBmp = tmp.MakeBitmap (NULL);
						}
					}

					if (!hBmp) {
						CDC dc;
						
						dc.CreateCompatibleDC (NULL); // force monochrome

						hBmp = img.MakeBitmap (dc);
					}
				}
			}
//		}
	}

//	MARKTRACECOUNT ();

	if (!hBmp)
		hBmp = (HBITMAP)::LoadImage (NULL, strFile, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_MONOCHROME); 

//	TRACEF (strFile);
//	TRACECOUNTARRAY ();

	return hBmp;
}

HBITMAP CBitmapElement::LoadBitmap(UINT nResource, HINSTANCE hInst)
{
	if (hInst == NULL)
		hInst = GetInstanceHandle ();

	ASSERT (hInst);
	
	return (HBITMAP)::LoadImage (hInst, MAKEINTRESOURCE (nResource), 
		IMAGE_BITMAP, 0, 0, LR_MONOCHROME); 
}

bool CBitmapElement::IsMonochrome (const CString &strFile)
{
	CxImage img;
	
	if (img.Load (strFile))
		return img.GetBpp () == 1;

	return false;
/* TODO: rem
	bool bResult = false;

	if (afxCurrentInstanceHandle != NULL) {
		HBITMAP hBmp = (HBITMAP)::LoadImage (GetInstanceHandle (),
			strFile, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);

		if (hBmp) {
			CBitmap * p = CBitmap::FromHandle (hBmp);
			BITMAP b;

			p->GetBitmap (&b);
			bResult = (b.bmPlanes == 1) && (b.bmBitsPixel == 1);
			::DeleteObject (hBmp);
		}
	}

	return bResult;
*/
}

void CBitmapElement::CreateDefBitmap (const CString & strFile)
{
	const CString strLang = FoxjetDatabase::GetLangExt ();
	CDC dcMem;
	HDC hDC = ::GetDC (NULL);
	CFont fnt;
	CString str = LoadString (IDS_NOLOGOFOUND);
	HINSTANCE hInst = GetInstanceHandle ();
	HBITMAP hBmp = (HBITMAP)::LoadImage (hInst, MAKEINTRESOURCE (IDB_DEFBLANK), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_MONOCHROME);
	DIBSECTION ds;
	
	ASSERT (hBmp);

	CBitmap * pDef = CBitmap::FromHandle (hBmp);
	::ZeroMemory (&ds, sizeof (ds));

	VERIFY (pDef->GetObject (sizeof (ds), &ds));
	
	const CSize sizeDef (ds.dsBm.bmWidth, ds.dsBm.bmHeight);
	int nFontHeight = (int)floor (sizeDef.cy / 5.0);
	int nFontWidth = nFontHeight;//(int)(nFontHeight * 1.5);
	CRect rc (CPoint (0, 0), sizeDef);

	if (!strLang.CompareNoCase (_T ("en"))) { }
	else if (!strLang.CompareNoCase (_T ("sp"))) { nFontHeight = nFontWidth = (int)(nFontHeight * 0.8); }
	else if (!strLang.CompareNoCase (_T ("po"))) { }
	else if (!strLang.CompareNoCase (_T ("ru"))) { }
	else if (!strLang.CompareNoCase (_T ("nl"))) { }
	else if (!strLang.CompareNoCase (_T ("hu"))) { }

	rc.DeflateRect (3, 1, 5, 2);
	rc.DeflateRect (2, 2);

	fnt.CreateFont (nFontHeight, nFontWidth, 0, 0, FW_BOLD, FALSE, 0, 0,
		DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("Arial"));

	dcMem.CreateCompatibleDC (CDC::FromHandle (hDC));
	CBitmap * pMem = dcMem.SelectObject (pDef);
	CFont * pFont = dcMem.SelectObject (&fnt);
	int nBkMode = dcMem.SetBkMode (TRANSPARENT);

	int nDrawText = dcMem.DrawText (str, &rc, DT_WORDBREAK | DT_VCENTER | DT_CENTER | DT_CALCRECT);
	dcMem.DrawText (str, rc + CPoint (((sizeDef.cx - rc.Width ()) / 2) - 4, ((sizeDef.cy - nDrawText) / 2) - 4), DT_WORDBREAK | DT_VCENTER | DT_CENTER);

	{
		int nIndex = strFile.ReverseFind ('\\');
		
		if (nIndex != -1) {
			CString strPath = strFile.Left (nIndex);

			FoxjetDatabase::CreateDir (strPath);
		}
	}

	TRACEF ("create: " + strFile);
	bool bSave = SaveBitmap (hBmp, strFile);
	ASSERT (bSave);	

	dcMem.SelectObject (pMem);
	dcMem.SelectObject (pFont);
	dcMem.SetBkMode (nBkMode);
	::ReleaseDC (NULL, hDC);

	::DeleteObject (hBmp);
}

void CBitmapElement::LoadDefResBitmap(const FoxjetDatabase::HEADSTRUCT & head, const CString & strFolder)
{ 
	struct
	{
		UINT	m_nID;
		LPCTSTR m_lpsz;
	}
	static const map [] =
	{
		//{ IDB_DEFBITMAP,	_T ("Def.bmp"),			}, // def should always be first
		{ IDB_DEFBLANK,		_T ("Def.bmp"),			}, // def should always be first
		{ IDB_192_32,		_T ("192Sample.bmp"),	},
		{ IDB_96_32,		_T ("96Sample.bmp"),	},
		{ IDB_786_256,		_T ("768Sample.bmp"),	},
		{ IDB_768JET,		_T ("768jet.BMP"),		},
		{ IDB_384_128,		_T ("384Sample.bmp"),	},
		{ IDB_384JET,		_T ("384perf300.bmp"),	},
		{ IDB_384JET2,		_T ("384perf.BMP"),		},
		{ IDB_352_32,		_T ("352Sample.bmp"),	},
		{ IDB_AC_32,		_T ("AC.bmp"),			},
	};
	const CString strPath = (strFolder.GetLength () && (::GetFileAttributes (strFolder) != -1)) ? strFolder : GetDefPath ();

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		CString strFilepath = strPath + map [i].m_lpsz;
		DIBSECTION ds;
		bool bInvalid = false;

		HINSTANCE hInst = GetInstanceHandle ();
		HBITMAP hBmp = (HBITMAP)::LoadImage (hInst, 
			MAKEINTRESOURCE (map [i].m_nID), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_MONOCHROME);
		ASSERT (hBmp);

		CBitmap * pDef = CBitmap::FromHandle (hBmp);
		::ZeroMemory (&ds, sizeof (ds));
		VERIFY (pDef->GetObject (sizeof (ds), &ds));
		const CSize sizeDef (ds.dsBm.bmWidth, ds.dsBm.bmHeight);

		// verify the default has not had it's size changed
		if (HBITMAP hBmpFile = LoadBitmap (strFilepath, head)) {
			CBitmap * pFile = CBitmap::FromHandle (hBmp);

			::ZeroMemory (&ds, sizeof (ds));
			VERIFY (pFile->GetObject (sizeof (ds), &ds));
			const CSize sizeFile (ds.dsBm.bmWidth, ds.dsBm.bmHeight);

			bInvalid = (sizeFile != sizeDef) ? true : false;
			::DeleteObject (hBmpFile);
		}
		else
			bInvalid = true;

		if (i == 0) { // first bitmap is the default for the element
			if (!bInvalid) 
				bInvalid = !SetFilePath (strFilepath, head);
		}

		// create it if its not there
		if (bInvalid) {
			CreateDefBitmap (strFilepath);

			if (i == 0) { // first bitmap is the default for the element
				bool bSetFilePath = SetFilePath (strFilepath, head);
				ASSERT (bSetFilePath);
			}
		}

		{
			double dStretch [2] = { 1, 1 };
			CSize size (sizeDef);

			CBaseElement::CalcStretch (head, dStretch);
			size.cy = (int)ceil ((double)size.cy * dStretch [1]);
			size = CBaseElement::LogicalToThousandths (size, head);

			SetSize (size, head);
		}
		
		::DeleteObject (hBmp);

		if (!IsProductionConfig ())
			return;
	}
}

void CBitmapElement::ClipTo (const FoxjetDatabase::HEADSTRUCT & head)
{
	CBaseElement::ClipTo (head);

	CSize size = GetSize (head);
	int cy = CBaseElement::LogicalToThousandths (CSize (0, head.Height ()), head).y;

	if (size.cy > cy) {
		size.cy = cy;

		//SetSize (size, head);
		m_size.cy = cy;
		SetRedraw ();
	}
}

CString CBitmapElement::GetDefPath ()
{
	CString strDef = GetHomeDir () + _T ("\\Logos\\");

	return strDef;
}

CString CBitmapElement::GetDefFilename ()
{
	return CBitmapElement::GetDefPath () + _T ("Def.bmp");
}

const FoxjetCommon::ElementFields::FIELDSTRUCT * CBitmapElement::GetFieldBuffer () const
{
	return ::fields;
}

CString CBitmapElement::GetNetworkFilePath ()
{
	CString str = FoxjetDatabase::GetProfileString (
		HKEY_CURRENT_USER, 
		defElements.m_strRegSection + _T ("\\Elements"), 
		_T ("Bitmap"), 
		defElements.GetElement (BMP).ToString (verApp));
	CStringArray v;

	Tokenize (str, v);

	if (v.GetSize () > 9) {
		CString strPath = UnformatString (v [9]);
		int nIndex = strPath.ReverseFind ('\\');

		if (nIndex != -1) {
			strPath = strPath.Left (nIndex);
			return strPath;
		}
	}

	return GetDefPath ();
}

CString CBitmapElement::GetLocalFilePath ()
{
	return FoxjetDatabase::GetProfileString (
		HKEY_CURRENT_USER, 
		ListGlobals::defElements.m_strRegSection + _T ("\\Settings"), 
		_T ("LocalLogosFolder"), 
		GetHomeDir () + _T ("\\Logos"));
}

CString CBitmapElement::GetDatabasePrompt () const
{
	CString str;
	
	str.Format (_T ("[%s].[%s].[%s]"),
		m_db.m_strDSN,
		m_db.m_strTable,
		m_db.m_strKeyField);

	return str;
}

CString CBitmapElement::GetDatabaseDSN () const
{
	return m_db.m_strDSN;
}

bool CBitmapElement::SetDatabaseDSN (const CString & str) 
{
	bool bResult = false;

	if (m_db.m_bLookup) {
		//LOCK (::csDatabase);

		try {
			//COdbcCache db = COdbcDatabase::Find (str);
			m_db.m_strDSN = str;
			SetRedraw ();
			m_db.m_bInvalid = true;
			bResult = true;
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }
	}

	return bResult;
}

CString CBitmapElement::GetDatabaseTable () const
{
	return m_db.m_strTable;
}

bool CBitmapElement::SetDatabaseTable (const CString & str) 
{
	m_db.m_strTable = str;

	if (m_db.m_bLookup) {
		if (COdbcDatabase::FindTable (GetDatabaseDSN (), str) != -1) {
			SetRedraw ();
			m_db.m_bInvalid = true;
		}
	}

	return true;
}

CString CBitmapElement::GetDatabaseField () const
{
	return m_db.m_strField;
}

bool CBitmapElement::SetDatabaseField (const CString & str) 
{
	m_db.m_strField = str;

	if (m_db.m_bLookup) {
		if (COdbcDatabase::FindField (GetDatabaseDSN (), GetDatabaseTable (), str) != -1) {
			SetRedraw ();
			m_db.m_bInvalid = true;
		}
	}

	return true;
}

CString CBitmapElement::GetDatabaseKeyField () const
{
	return m_db.m_strKeyField;
}

bool CBitmapElement::SetDatabaseKeyField (const CString & str) 
{
	m_db.m_strKeyField = str;

	if (m_db.m_bLookup) {

		// NULL value allowed
		if (!str.GetLength () || COdbcDatabase::FindField (GetDatabaseDSN (), GetDatabaseTable (), str) != -1) {
			SetRedraw ();
			m_db.m_bInvalid = true;
		}
	}

	return true;
}

CString CBitmapElement::GetDatabaseKeyValue () const
{
	return m_db.m_strKeyValue;
}

bool CBitmapElement::SetDatabaseKeyValue (const CString & str) 
{
	m_db.m_strKeyValue = str;

	if (m_db.m_bLookup) {
		if (IsValid (str.GetLength (), CDatabaseElement::m_lmtKey)) {
			SetRedraw ();
			m_db.m_bInvalid = true;
		}
	}

	return true;
}

bool CBitmapElement::IsUserPrompted () const
{
	return m_bUserPrompted;
}

bool CBitmapElement::SetUserPrompted (bool b) 
{
	m_bUserPrompted = b;
	return true;
}

CString CBitmapElement::GetUserPrompt () const
{
	return m_strUserPrompt;
}

bool CBitmapElement::SetUserPrompt (const CString & str)
{
	m_strUserPrompt = str;
	return true;
}

bool CBitmapElement::IsPromptAtTaskStart () const
{
	return m_bPromptAtTaskStart;
}

bool CBitmapElement::SetPromptAtTaskStart (bool b)
{
	m_bPromptAtTaskStart = b;
	return true;
}

bool CBitmapElement::IsLockAspectRatio () const 
{ 
	return m_bLockAspectRatio; 
}


bool CBitmapElement::SetLockAspectRatio (bool bLock)
{
	m_bLockAspectRatio = bLock;
	return true;
}
