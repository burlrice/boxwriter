// DbWizardKeyFieldDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DbWizardKeyFieldDlg.h"
#include "DbWizardDsnDlg.h"
#include "Parse.h"
#include "OdbcRecordset.h"
#include "DbWizardFieldDlg.h"
#include "resource.h"
#include "DbFieldDlg.h"
#include "Extern.h"

using namespace FoxjetElements;
using namespace FoxjetDatabase;

// CDbWizardKeyFieldDlg

IMPLEMENT_DYNAMIC(CDbWizardKeyFieldDlg, CDbWizardBasePage)

CDbWizardKeyFieldDlg::CDbWizardKeyFieldDlg(CDbWizardDlg * pParent)
: CDbWizardBasePage (pParent, IDD_DATABASE_WIZARD_KEY_FIELD)
{
}

CDbWizardKeyFieldDlg::~CDbWizardKeyFieldDlg()
{
}


BEGIN_MESSAGE_MAP(CDbWizardKeyFieldDlg, CDbWizardBasePage)
	ON_BN_CLICKED (CHK_KEYFIELD, OnCheckKeyField)
	ON_BN_CLICKED (BTN_VIEW, OnView)
	ON_CBN_SELCHANGE (CB_FIELDS, OnFieldsChange)
END_MESSAGE_MAP()



// CDbWizardKeyFieldDlg message handlers


BOOL CDbWizardKeyFieldDlg::OnSetActive()
{
	OnCheckKeyField ();

	if (m_strDSN != m_pdlgMain->m_pdlgDSN->GetCurSelDsn ()) {
		m_strDSN = m_pdlgMain->m_pdlgDSN->GetCurSelDsn ();

		if (CComboBox * p = (CComboBox *)GetDlgItem (CB_FIELDS)) {
			p->ResetContent ();

			try {
				if (m_pdlgMain->m_pdb->GetDB ().IsOpen ()) {
					//COdbcRecordset rst (&m_pdlgMain->m_db);
					CString strSQL = _T ("SELECT * FROM [") + m_pdlgMain->m_pdlgField->GetCurSelTable () + _T ("];");
					COdbcCache rst = COdbcDatabase::Find (m_pdlgMain->m_pdb->GetDB (), m_pdlgMain->m_pdb->GetDSN (), strSQL);


					for (short i = 0; i < rst.GetRst ().GetODBCFieldCount (); i++) {
						CString strName = Extract (rst.GetRst ().GetFieldName (i), _T ("].["), _T ("]"));
						int nIndex = p->AddString (strName);
					}
				}
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }

			if (p->GetCurSel () == CB_ERR)
				p->SetCurSel (0);
		}
	}

	return TRUE;
}

BOOL CDbWizardKeyFieldDlg::OnKillActive()
{
	m_strKeyField = GetCurSelKeyField ();
	GetDlgItemText (TXT_KEYVALUE, m_strKeyValue);

	return CDbWizardBasePage::OnKillActive ();
}

bool CDbWizardKeyFieldDlg::CanAdvance () 
{ 
	return true; 
}


void CDbWizardKeyFieldDlg::OnFieldsChange ()
{
	m_strKeyField = GetCurSelKeyField ();

	if (m_strKeyField.GetLength ()) {
		try {
			if (m_pdlgMain->m_pdb->GetDB ().IsOpen ()) {
				//COdbcRecordset rst (&m_pdlgMain->m_db);
				CString strSQL = _T ("SELECT [") + m_strKeyField + _T ("] FROM [") + m_pdlgMain->m_pdlgField->GetCurSelTable () + _T ("];");
				COdbcCache rst = COdbcDatabase::Find (m_pdlgMain->m_pdb->GetDB (), m_pdlgMain->m_pdb->GetDSN (), strSQL);

				if (!rst.GetRst ().IsEOF ())
					SetDlgItemText (TXT_KEYVALUE, (CString)rst.GetRst ().GetFieldValue ((short)0));
			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }
	}
}

void CDbWizardKeyFieldDlg::SetCurSelKeyField (const CString & str) 
{
	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_FIELDS)) {
		int nIndex = p->FindString (-1, str);
		p->SetCurSel (nIndex);
		OnFieldsChange ();
	}
}

CString CDbWizardKeyFieldDlg::GetCurSelKeyField () const
{
	CString str;

	if (CButton * p = (CButton *)GetDlgItem (CHK_KEYFIELD)) 
		if (p->GetCheck () == 1)
			return str;

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_FIELDS)) 
		if (p->GetCount ())
			p->GetLBText (p->GetCurSel (), str);

	return str;
}

void CDbWizardKeyFieldDlg::OnCheckKeyField ()
{
	if (CButton * p = (CButton *)GetDlgItem (CHK_KEYFIELD)) {
		UINT n [] = 
		{
			CB_FIELDS,
			BTN_VIEW,
			TXT_KEYVALUE,
		};

		for (int i = 0; i < ARRAYSIZE (n); i++)
			if (CWnd * pCB = GetDlgItem (n [i]))
				pCB->EnableWindow (p->GetCheck () == 0);

		if (p->GetCheck () == 0)
			m_strKeyField.Empty ();
	}
}

void CDbWizardKeyFieldDlg::OnView ()
{
	CInternalFormatting opt (false);
	FoxjetUtils::CDbFieldDlg dlg (ListGlobals::defElements.m_strRegSection, this);

	dlg.m_dwFlags	= 0;//ListCtrlImp::CELLCTRL_NOCHANGECOLSEL; 
	dlg.m_strDSN	= m_pdlgMain->m_pdlgDSN->GetCurSelDsn ();
	dlg.m_strTable	= m_pdlgMain->m_pdlgField->GetCurSelTable ();
	dlg.m_strField	= GetCurSelKeyField ();
	//dlg.m_nRow		= 0;

	if (dlg.DoModal () == IDOK) {
		if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_FIELDS)) 
			pCB->SetCurSel (pCB->FindString (-1, dlg.m_strField));

		SetDlgItemText (TXT_KEYVALUE, dlg.m_strSelected);
	}
}