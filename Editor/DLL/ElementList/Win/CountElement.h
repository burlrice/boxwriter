// CountElement.h: interface for the CCountElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COUNTELEMENT_H__6CB6265E_8AD9_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_COUNTELEMENT_H__6CB6265E_8AD9_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TextElement.h"
#include "Types.h"

namespace FoxjetElements
{
	class ELEMENT_API CCount
	{
	public:
		CCount (__int64 lCount = 0, __int64 lMin = 0, __int64 lMax = 0, __int64 lInc = 0);
		CCount (const CCount & rhs);
		CCount & operator = (const CCount & rhs);

		bool Increment (); 

		bool SetCount (__int64 lCount);
		__int64 GetCount () const { return m_lCount; }
		operator __int64 () const { return GetCount (); }
		bool SetMin (__int64 lMin);
		__int64 GetMin () const { return m_lMin; }
		bool SetMax (__int64 lMax);
		__int64 GetMax () const { return m_lMax; }
		bool SetIncrement (__int64 lIncrement);
		__int64 GetIncrement () const { return m_lIncrement; }

		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);

	protected:
		__int64 m_lCount;
		__int64 m_lMin;
		__int64 m_lMax;
		__int64 m_lIncrement;
	};

	class ELEMENT_API CCountElement : public TextElement::CTextElement  
	{
		DECLARE_DYNAMIC (CCountElement);

	public:
		static enum { MINDIGITS = 1, MAXDIGITS = 19 };

		CCountElement(const FoxjetDatabase::HEADSTRUCT & head);
		CCountElement (const CCountElement & rhs);
		CCountElement & operator = (const CCountElement & rhs);
		virtual ~CCountElement();

		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);
		virtual int GetClassID () const { return COUNT; }

		virtual int Build (BUILDTYPE type = INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		virtual void Reset ();
		virtual void Increment ();

		void IncrementTo (__int64 lCount);
		bool SetCount (__int64 lCount);

	public:
		__int64 GetCount () const;
		operator __int64 () const { return GetCount (); }
		bool SetMin (__int64 lMin);
		__int64 GetMin () const;
		bool SetMax (__int64 lMax);
		__int64 GetMax () const;
		bool SetIncrement (__int64 lIncrement);
		__int64 GetIncrement () const;

		bool SetPalCount (__int64 lCount);
		__int64 GetPalCount () const;
		bool SetPalMin (__int64 lMin);
		__int64 GetPalMin () const;
		bool SetPalMax (__int64 lMax);
		__int64 GetPalMax () const;
		bool SetPalIncrement (__int64 lIncrement);
		__int64 GetPalIncrement () const;

		bool SetDigits (int nDigits);
		int GetDigits () const;
		bool SetLeadingZeros (bool bValue);
		bool GetLeadingZeros () const;
		bool IsPalletCount () const;
		bool EnablePalletCount (bool bPalletCount);

		CString GetName () const;
		bool SetName (const CString & str);

		bool GetRollover () const;
		bool SetRollover (bool bRollover);
		bool IsMaster () const;
		bool SetMaster (bool bMaster);

		bool IsIrregularPalletSize () const;
		bool SetIrregularPalletSize (bool bPrompt);

		static const __int64 m_iLmtStart;
		static const LPCTSTR m_lpszDefName;

	protected:
		CCount m_cnt;
		CCount m_cntPal;
		bool m_bIsPalCount;
		bool m_bLeadingZeros;
		int m_nDigits;
		CString m_strName;
		bool m_bRollover;
		bool m_bMaster;
		bool m_bIrregularPalletSize;
	};
}; //namespace FoxjetElements

#endif // !defined(AFX_COUNTELEMENT_H__6CB6265E_8AD9_11D4_8FC6_006067662794__INCLUDED_)

