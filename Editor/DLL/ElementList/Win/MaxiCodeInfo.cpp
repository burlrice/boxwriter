// MaxiCodeInfo.cpp: implementation of the CMaxiCodeInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MaxiCodeInfo.h"
#include "OdbcRecordset.h"
#include "Debug.h"
#include "Extern.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;
using namespace FoxjetElements;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMaxiCodeInfo::CMaxiCodeInfo()
:	m_strPostalCode (_T ("000000000")), 
	m_strCountryCode (_T ("840")), 
	m_strClassOfService (_T ("003")), 
	m_nDataLen (0), 
	m_nMode (2)
{

}

CMaxiCodeInfo::CMaxiCodeInfo (const CMaxiCodeInfo & rhs)
:	m_strPostalCode		(rhs.m_strPostalCode),
	m_strCountryCode	(rhs.m_strCountryCode),
	m_strClassOfService (rhs.m_strClassOfService),
	m_nDataLen			(rhs.m_nDataLen),
	m_nMode				(rhs.m_nMode)
{
}

CMaxiCodeInfo & CMaxiCodeInfo::operator = (const CMaxiCodeInfo & rhs)
{
	if (this != &rhs) {
		m_strPostalCode		= rhs.m_strPostalCode;
		m_strCountryCode	= rhs.m_strCountryCode;
		m_strClassOfService = rhs.m_strClassOfService;
		m_nDataLen			= rhs.m_nDataLen;
		m_nMode				= rhs.m_nMode;
	}

	return * this;
}

CMaxiCodeInfo::~CMaxiCodeInfo()
{

}

bool CMaxiCodeInfo::IsValid() const
{
	return 
		IsValidPostalCode (m_strPostalCode, m_nMode) &&
		IsValidCountryCode (m_strCountryCode) &&
		IsValidClassOfService (m_strClassOfService) &&
		IsValidMode (m_nMode);
}

bool CMaxiCodeInfo::IsValidPostalCode (const CString & str, int nMode)
{
	bool bResult = true;//false;
	return bResult;
}

bool CMaxiCodeInfo::IsValidCountryCode (const CString & strCode)
{
	bool bResult = false;

	try {
		COdbcRecordset rst (&(ListGlobals::GetDB ()));
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE %s='%s';"),
			_T ("CountryCodes"), _T ("Number"), // TODO
			strCode);
		//TRACEF (str);
		rst.Open (str);
		bResult = !rst.IsEOF ();
		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

bool CMaxiCodeInfo::IsValidClassOfService (const CString & str)
{
	bool bResult = false;

	try {
		COdbcRecordset rst (&(ListGlobals::GetDB ()));
		CString strSQL;

		strSQL.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s';"), 
			_T ("ClassOfService"), _T ("Service Indicator"), // TODO
			str);

		rst.Open (strSQL);
		bResult = !rst.IsEOF ();
		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

bool CMaxiCodeInfo::IsValidMode (int nMode)
{
	return nMode >= 2 && nMode <= 6;
}

bool CMaxiCodeInfo::IsValidLength (int nLen)
{
	return nLen >= 0;
}

