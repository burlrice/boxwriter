// BitmapElement.h: interface for the CBitmapElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BITMAPELEMENT_H__6CB6265B_8AD9_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_BITMAPELEMENT_H__6CB6265B_8AD9_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxwin.h>

#include "BaseElement.h"
#include "Types.h"
#include "ElementApi.h"
#include "ximage.h"
#include "RawBitmap.h"
#include "DatabaseElement.h"

namespace FoxjetElements
{
	class ELEMENT_API CBitmapElement : public FoxjetCommon::CBaseElement  
	{
		DECLARE_DYNAMIC (CBitmapElement);

	public:
		CBitmapElement (const FoxjetDatabase::HEADSTRUCT & head);
		CBitmapElement (const CBitmapElement & rhs);
		CBitmapElement & operator = (const CBitmapElement & rhs);
		virtual ~CBitmapElement();

		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);
		virtual CSize Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly = false, FoxjetCommon::DRAWCONTEXT context = EDITOR);
		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		virtual int GetClassID () const { return BMP; }
		virtual bool IsResizable () const { return true; }

		virtual int Build (BUILDTYPE type = INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		virtual bool SetSize (const CSize & size, const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI = true);
		virtual CSize GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI = true) const;
		virtual void ClipTo (const FoxjetDatabase::HEADSTRUCT & head);

		virtual bool IsLockAspectRatio () const;
		bool SetLockAspectRatio (bool bLock);

		CString GetFilePath () const;
		bool SetFilePath (const CString & strPath, const FoxjetDatabase::HEADSTRUCT & head, const CSize & size = CSize (0, 0));

		bool IsDatabaseLookup () const { return m_db.m_bLookup; }
		bool SetDatabaseLookup (bool b) { m_db.m_bLookup = b; return true; }
		bool GetDatabasePromptAtTaskStart () const  { return m_db.m_bPrompt; }
		bool SetDatabasePromptAtTaskStart (bool b)  { m_db.m_bPrompt = b; return true; }
		CString GetDatabasePrompt () const;
		CString GetDatabaseDSN () const;
		bool SetDatabaseDSN (const CString & str);
		CString GetDatabaseTable () const;
		bool SetDatabaseTable (const CString & str);
		CString GetDatabaseField () const;
		bool SetDatabaseField (const CString & str);
		CString GetDatabaseKeyField () const;
		bool SetDatabaseKeyField	(const CString & str);
		CString GetDatabaseKeyValue () const;
		bool SetDatabaseKeyValue (const CString & str);
		int GetDatabaseSQLType () const { return m_db.m_nSQLType; }
		bool SetDatabaseSQLType (int n) { m_db.m_nSQLType = n; return true; }

		bool IsUserPrompted () const;
		bool SetUserPrompted (bool b);

		CString GetUserPrompt () const;
		bool SetUserPrompt (const CString & str);

		bool IsPromptAtTaskStart () const;
		bool SetPromptAtTaskStart (bool b);

		bool SetDbWidthField (const CString & str) { m_db.m_strBmpWidthField = str; return true; }
		CString GetDbWidthField () const { return m_db.m_strBmpWidthField; }

		bool SetDbHeightField (const CString & str) { m_db.m_strBmpHeightField = str; return true; }
		CString GetDbHeightField () const { return m_db.m_strBmpHeightField; }

		typedef enum { DBSIZE_NATIVE = -3, DBSIZE_AUTO = -2, DBSIZE_STATIC = -1 } DBSIZE;

		CSize GetDbSize () const;
		static CSize GetDbSize (const CSize & size, const CString & strWidthField, const CString & strHeightField);

		void Trace (const CString & strPath = _T (""));

		bool LoadBmp64(const CString &strPath, const FoxjetDatabase::HEADSTRUCT & head, const CSize & sizeDef);
		bool LoadZ64(const CString &strPath, const FoxjetDatabase::HEADSTRUCT & head, const CSize & sizeDef);
		CSize CalcDatabasePromptedSize (const CString & strPath);

		static CString GetNetworkFilePath ();
		static CString GetLocalFilePath ();

		static CString FindFile(const CString &strPath, const CString &strTitle, const FoxjetDatabase::HEADSTRUCT & head, const CSize & sizeDef, bool bMonochrome);

		static CSize GetMinSize ();
		static CSize GetSize1000ths (const CString & strFilename);
		static CSize GetSizePixels (const CString & strFilename, const FoxjetDatabase::HEADSTRUCT & head);
		static HBITMAP LoadBitmap (const CString & strFile, const FoxjetDatabase::HEADSTRUCT & head, const CSize & size = CSize (0, 0), bool * pbMonochrome = NULL);
		static HBITMAP LoadBitmap (UINT nResource, HINSTANCE hInst = NULL);
		static bool IsMonochrome (const CString & strFile);

		static CString GetDefPath ();
		static CString GetDefFilename ();
		static void CreateDefBitmap (const CString & strPath);
		static CPoint NextWholePixel (const CPoint & size, const FoxjetDatabase::HEADSTRUCT & h);

		typedef struct 
		{
			bool m_bLookup;
			bool m_bPrompt;
			CString m_strDSN;
			CString m_strTable;
			CString m_strField;
			CString m_strKeyField;
			CString m_strKeyValue;
			int m_nSQLType;
			bool m_bInvalid;
			CString m_strBmpWidthField;
			CString m_strBmpHeightField;
			CSize m_sizeDB;
		} DATABASESTRUCT;

	protected:
		void LoadDefResBitmap (const FoxjetDatabase::HEADSTRUCT & head, const CString & strFolder = _T (""));
		bool Free ();
		bool Copy (const CBitmapElement & rhs);

		CSize m_size, m_sizeStatic;
		CString m_strFilePath;
		FoxjetCommon::CRawBitmap m_bmpEditor;
		FoxjetCommon::CRawBitmap * m_pbmpPrinter;
		bool m_bMonochrome;
		bool m_bResizeOnRedraw;
		bool m_bUserPrompted;
		CString m_strUserPrompt;
		bool m_bPromptAtTaskStart;
		bool m_bLockAspectRatio;

		DATABASESTRUCT m_db;
	};
}; // namespace FoxjetElements


#endif // !defined(AFX_BITMAPELEMENT_H__6CB6265B_8AD9_11D4_8FC6_006067662794__INCLUDED_)
