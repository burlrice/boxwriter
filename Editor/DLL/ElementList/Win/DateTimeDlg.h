#if !defined(AFX_DATETIMEDLG_H__471C7425_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_DATETIMEDLG_H__471C7425_8B3D_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DateTimeDlg.h : header file
//

#include "BaseTextDlg.h"
#include "DateTimeElement.h"
#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// CDateTimeDlg dialog

class CDateTimeDlg : public CBaseTextDlg
{
// Construction
public:
	CDateTimeDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   // standard constructor
	CDateTimeDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		UINT nID, const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);

// Dialog Data
	//{{AFX_DATA(CDateTimeDlg)
	//}}AFX_DATA

	CString	m_strFmt;
	BOOL m_bRollover;

	void Get (const FoxjetElements::CDateTimeElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);
	void Set (FoxjetElements::CDateTimeElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);

	virtual UINT GetDefCtrlID () const { return CB_DATETIMEFORMAT; }

protected:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDateTimeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual int BuildElement();
	FoxjetElements::CDateTimeElement & GetElement ();
	const FoxjetElements::CDateTimeElement & GetElement () const;

	// Generated message map functions
	//{{AFX_MSG(CDateTimeDlg)
	afx_msg void OnBuild();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATETIMEDLG_H__471C7425_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
