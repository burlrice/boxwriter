// BitmapDlg.cpp : implementation file
//

#include "stdafx.h"

#include <math.h>

#include "BitmapDlg.h"
#include "BitmapElement.h"
#include "List.h"
#include "Coord.h"
#include "Debug.h"
#include "Edit.h"
#include "Utils.h"
#include "ximage.h"
#include "AppVer.h"
#include "DatabaseElement.h"
#include "Extern.h"
#include "Parse.h"

using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBitmapDlg dialog


CBitmapDlg::CBitmapDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
					   const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_size (1000, 500), 
	m_dwProcessId (0),
	m_hbmpOriginal (NULL),
	m_bEdit (false),
	CElementDlg(e, pa, IDD_BITMAP, pList, pParent)
{
	//{{AFX_DATA_INIT(CBitmapDlg)
	m_strFilePath = _T("");
	//}}AFX_DATA_INIT
}


void CBitmapDlg::DoDataExchange(CDataExchange* pDX)
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();
	
	CCoord coordMin (0, 0, m_units, head);
	CCoord coordMax (0, 0, m_units, head);
	CCoord coordValidate (0, 0, m_units, head);
	
	const CSize pa1000s = CBaseElement::LogicalToThousandths (CSize (GetPA ().cx, head.Height ()), head); 
	const CSize sizeMin1000s = CBaseElement::LogicalToThousandths (CBitmapElement::GetMinSize (), head);

	coordMin.m_x = sizeMin1000s.cx;
	coordMin.m_y = sizeMin1000s.cy;

	coordMax.m_x = pa1000s.cx;
	coordMax.m_y = pa1000s.cy;

	//{{AFX_DATA_MAP(CBitmapDlg)
	DDX_Text(pDX, TXT_FILEPATH, m_strFilePath);
	//}}AFX_DATA_MAP

	DDX_Coord (pDX, TXT_WIDTH, (long &)m_size.cx, m_units, head, true);
	DDV_Coord (pDX, TXT_WIDTH, m_size.cx, 0, m_units, head, DDVCOORDXBOTH, coordMin, coordMax);

	DDX_Coord (pDX, TXT_HEIGHT, (long &)m_size.cy, m_units, head, false);
	DDV_Coord (pDX, TXT_HEIGHT, 0, m_size.cy, m_units, head, DDVCOORDYBOTH, coordMin, coordMax);

	DDX_Check (pDX, CHK_USER_PROMPT, m_bUserPrompted);
	DDX_Text (pDX, TXT_USER_PROMPT, m_strUserPrompt);
	DDX_Check (pDX, CHK_PROMPT_AT_TASK_START, m_bPromptAtTaskStart);
	
	if (GetDlgItem (CHK_LOCKASPECTRATIO)) DDX_Check (pDX, CHK_LOCKASPECTRATIO, m_bLockAspectRatio);

	{
		int n = m_db.m_bLookup;
		DDX_Check (pDX, CHK_DATABASE, n);
		m_db.m_bLookup = n;
	}

	CElementDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CBitmapDlg, CElementDlg)
	//{{AFX_MSG_MAP(CBitmapDlg)
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (CHK_DATABASE, OnEnableDatabaseClick)
	ON_BN_CLICKED (BTN_DATABASE, OnDatabaseClick)
	ON_BN_CLICKED (CHK_USER_PROMPT, OnUserPrompt)
	ON_EN_CHANGE (TXT_WIDTH, OnWidthChange)
	ON_EN_CHANGE (TXT_HEIGHT, OnHeightChange)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBitmapDlg message handlers

void CBitmapDlg::SetSize (const CString & strFilepath)
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();
	CSize sizeAuto = CBitmapElement::GetSize1000ths (strFilepath);

	CSize sizeNative = CBitmapElement::GetSizePixels (strFilepath, GetHead ());
	sizeNative.cx = (int)((double)sizeNative.cx / head.GetRes () * 1000.0);
	sizeNative.cy = (int)((double)sizeNative.cy / (head.GetChannels () / head.GetSpan ()) * 1000.0);

	CCoord c (CBaseElement::ThousandthsToLogical (sizeNative /* sizeAuto */, head), m_units, head, 1.0);

	{
		double dStretch [2] = { 1, 1 };
		CxImage img;

		CBaseElement::CalcStretch (head, dStretch);

		img.Load (strFilepath);
		CSize size (img.GetWidth (), img.GetHeight ());
		size.cy = (int)ceil ((double)size.cy * dStretch [1]);
		size = CBaseElement::LogicalToThousandths (size, head);
		c.m_y = CCoord (CBaseElement::ThousandthsToLogical (size, head), m_units, head, 1.0).m_y;
	}

	TRACEF (c.FormatX () + _T (", ") + c.FormatY ());
	SetDlgItemText (TXT_FILEPATH, strFilepath);
	SetDlgItemText (TXT_WIDTH, c.FormatX ());
	SetDlgItemText (TXT_HEIGHT, c.FormatY ());
}

void CBitmapDlg::OnBrowse() 
{
	CFileDialog dlg (TRUE, _T ("*.bmp"), m_strFilePath, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_SHAREAWARE, 
		GetFileDlgList (), 
		this);

	if (dlg.DoModal () == IDOK) {
		if (!CBitmapElement::IsMonochrome (dlg.GetPathName ())) 
			MsgBox (* this, LoadString (IDS_BITMAPNOTMONOCHROME), 
				LoadString (IDS_WARNING), MB_OK | MB_ICONINFORMATION);

		m_strFilePath = dlg.GetPathName ();
		SetSize (m_strFilePath);
	}
}

int CBitmapDlg::BuildElement ()
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();
	CBitmapElement & e = GetElement ();
	CString strPath, strWidth, strHeight;

	GetDlgItemText (TXT_FILEPATH, strPath);
	GetDlgItemText (TXT_WIDTH, strWidth);
	GetDlgItemText (TXT_HEIGHT, strHeight);

	e.SetFilePath (strPath, head);

	CSize size = CCoord::CoordToPoint (FoxjetDatabase::_ttof (strWidth), FoxjetDatabase::_ttof (strHeight), m_units, head);
	TRACEF (DB::ToString ((int)size.cx) + _T (", ") + DB::ToString ((int)size.cy));
	size = CBaseElement::LogicalToThousandths (size, head);
	TRACEF (DB::ToString ((int)size.cx) + _T (", ") + DB::ToString ((int)size.cy));

	e.SetSize (size, head);
	e.SetDbWidthField (e.GetDbWidthField ());
	e.SetDbHeightField (e.GetDbHeightField ());

	TRACEF (m_db.m_strBmpWidthField + _T (", ") + m_db.m_strBmpHeightField);
	TRACEF (e.GetDbWidthField () + _T (", ") + e.GetDbHeightField ());
	TRACEF (DB::ToString ((int)e.GetSize (head).cx) + _T (", ") + DB::ToString ((int)e.GetSize (head).cy));

	int nResult = e.Build ();

	CSize type = e.GetDbSize ();
	
	TRACEF (DB::ToString ((int)e.GetSize (head).cx) + _T (", ") + DB::ToString ((int)e.GetSize (head).cy));

	if (type.cx == CBitmapElement::DBSIZE_STATIC) 
		e.SetSize (CSize (size.cx, e.GetSize (head).cy), head);

	if (type.cy == CBitmapElement::DBSIZE_STATIC) 
		e.SetSize (CSize (e.GetSize (head).cx, size.cy), head);

	TRACEF (DB::ToString ((int)e.GetSize (head).cx) + _T (", ") + DB::ToString ((int)e.GetSize (head).cy));

	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

CBitmapElement & CBitmapDlg::GetElement ()
{
	CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CBitmapElement)));
	return (CBitmapElement &)e;
}

const CBitmapElement & CBitmapDlg::GetElement () const
{
	const CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CBitmapElement)));
	return (CBitmapElement &)e;
}

BOOL CBitmapDlg::OnInitDialog() 
{
#ifdef __WYSIWYGFIX__

	double dScreenRes = CCoord::GetPixelsPerXUnit (INCHES, GetHead ());
	double dPrintRes = CBaseElement::CalcPrintRes (GetHead ());
	double dFactor = dScreenRes / dPrintRes;

	m_size.cx = (int)((double)m_size.cx * dFactor);
#endif

	CElementDlg::OnInitDialog();
	
	if (m_bReadOnlyLocation) {
		GetDlgItem (TXT_WIDTH)->EnableWindow (false);
		GetDlgItem (TXT_HEIGHT)->EnableWindow (false);
	}

	OnEnableDatabaseClick ();
	OnUserPrompt ();

	return FALSE;
}

void CBitmapDlg::OnEdit() 
{
	if (m_dwProcessId > 0) 
		FoxjetCommon::ActivateProcess (m_dwProcessId);
	else {
		DWORD dw;

		CString strFile = GetElement ().GetFilePath ();

		ASSERT (m_hbmpOriginal == NULL);

		if (FoxjetCommon::verApp >= CVersion (1, 20)) { // TODO: rem
			CxImage img;
			DWORD dwType = GetCxImageType (strFile);

			if (img.Load (strFile, dwType)) 
				if (img.IsValid ()) 
					m_hbmpOriginal = img.MakeBitmap (GetDC ()->m_hDC);
		}
		else {
			VERIFY (m_hbmpOriginal = (HBITMAP)::LoadImage (GetInstanceHandle (),
					strFile, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION));
		}

		ASSERT (m_hbmpOriginal);

		HANDLE hThread = ::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0,
			(LPTHREAD_START_ROUTINE)WaitForEdit, (LPVOID)this, 0, &dw);
		ASSERT (hThread != NULL);
	}
}

ULONG CALLBACK CBitmapDlg::WaitForEdit (LPVOID lpData)
{
	ASSERT (lpData);

	CBitmapDlg & dlg = * (CBitmapDlg *)lpData;
	CString strApp = GetBitmapEditorPath ();
	CString strCmdLine = strApp + _T (" \"") + dlg.m_strFilePath + _T ("\"");
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [MAX_PATH];

	_tcsncpy (szCmdLine, strCmdLine, MAX_PATH);
    memset (&pi, 0, sizeof(pi));
    memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	TRACEF (szCmdLine);
	BOOL bResult = ::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi); 

	dlg.UpdateUI ();

	if (!bResult) {
		MsgBox (dlg, LoadString (IDS_FAILEDTOLANUCHEDITOR));
	}
	else {
		bool bMore = true;
		int nTries = 0;
		
		dlg.m_dwProcessId = pi.dwProcessId;

		TRACEF (_T ("begin edit"));

		do {
			DWORD dwExitCode = 0;
			
			dlg.UpdateUI ();

			if (!GetExitCodeProcess (pi.hProcess, &dwExitCode))
				bMore = false;
			else if (!::IsWindow (dlg.m_hWnd))
				bMore = false;
			else {
				bMore = (dwExitCode == STILL_ACTIVE);
				WaitForSingleObjectEx(pi.hProcess, 3000, TRUE);
				TRACEF (_T ("still waiting"));
			}
		}
		while (bMore);

		TRACEF (_T ("edit complete"));
	}

	dlg.m_dwProcessId = 0;
	dlg.ReloadBitmap ();
	dlg.SetSize (dlg.m_strFilePath);
	dlg.UpdateUI ();

	return 0;
}

bool CBitmapDlg::GetValues ()
{
	CString strWidth, strHeight;
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();

	if (m_dwProcessId > 0)
		return false;

	GetDlgItemText (TXT_WIDTH, strWidth);
	GetDlgItemText (TXT_HEIGHT, strHeight);
	CSize size = CBaseElement::LogicalToThousandths (
		CCoord::CoordToPoint (FoxjetDatabase::_ttof (strWidth), FoxjetDatabase::_ttof (strHeight), m_units, head), head);
	const CSize paLogical = GetPA ();
	const CSize pa1000s = CBaseElement::LogicalToThousandths (CSize (paLogical.cx, head.Height ()), head); 

	if (size.cy > pa1000s.cy) {
		CCoord c (paLogical, m_units, head, 1.0);
		CString str;

		str.Format (LoadString (IDS_INVAILDHEIGHTCEILCORRECT), c.FormatY ());

		int nResult = MsgBox (* this, str, LoadString (IDS_WARNING), MB_YESNO | MB_ICONEXCLAMATION);

		if (nResult == IDYES)
			SetDlgItemText (TXT_HEIGHT, c.FormatY ());
		else
			return false;
	}
	
	if (size.cx > pa1000s.cx) {
		CCoord c (paLogical, m_units, head, 1.0);
		CString str;

		str.Format (LoadString (IDS_INVAILDWIDTHCEILCORRECT), c.FormatX ());

		int nResult = MsgBox (* this, str, LoadString (IDS_WARNING), MB_YESNO | MB_ICONEXCLAMATION);

		if (nResult == IDYES)
			SetDlgItemText (TXT_WIDTH, c.FormatX ());
		else
			return false;
	}

	return CElementDlg::GetValues ();
}

void CBitmapDlg::OnCancel()
{
	if (!m_dwProcessId)
		CElementDlg::OnCancel ();
}

void CBitmapDlg::UpdateUI(UINT nID)
{
	if (::IsWindow(m_hWnd)) {
		UINT nIDs [] = {
			BTN_BROWSE,
			//BTN_EDIT,
			TXT_LOCATIONX,
			TXT_LOCATIONY,
			TXT_WIDTH,
			TXT_HEIGHT,
			IDOK,
			IDCANCEL,
			-1
		};

		for (int i = 0; nIDs [i] != -1; i++)
			GetDlgItem (nIDs [i])->EnableWindow (!m_dwProcessId);

		if (CButton * pDatabase = (CButton *)GetDlgItem (CHK_DATABASE)) {
			if (CButton * pUser = (CButton *)GetDlgItem (CHK_USER_PROMPT)) {
				UINT n [] = { TXT_USER_PROMPT, CHK_PROMPT_AT_TASK_START };

				switch (nID) {
					case CHK_DATABASE:
						if (pDatabase->GetCheck ()) {
							if (pUser->GetCheck ()) 
								pUser->SetCheck (0);
						}
						break;
					case CHK_USER_PROMPT:
						if (pUser->GetCheck ()) {
							if (pDatabase->GetCheck ()) 
								pDatabase->SetCheck (0);
						}
						break;
				}

				for (int i = 0; i < ARRAYSIZE (n); i++) {
					if (CWnd * p = GetDlgItem (n [i]))
						p->EnableWindow (pUser->GetCheck () == 1);
				}

				if (CButton * pProperties = (CButton *)GetDlgItem (BTN_DATABASE)) 
					pProperties->EnableWindow (pDatabase->GetCheck () ? TRUE : FALSE);
			}
		}
	}
}

void CBitmapDlg::ReloadBitmap ()
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();
	CBitmapElement * pElement = (CBitmapElement *)CopyElement (&GetElement (), &head, false);
	bool bInvalid = false;
	CString strMsg;

	ASSERT (pElement);
	CBitmapElement & e = * pElement;
	CString strFile = e.GetFilePath ();

	if (!e.SetFilePath (strFile, head)) {
		strMsg.Format (LoadString (IDS_LOADBITMAPFAILED), strFile);
		bInvalid = true;
	}

	if (bInvalid) {
		if (m_hbmpOriginal) {
			if (FoxjetCommon::verApp >= CVersion (1, 20)) { // TODO: rem
					CxImage img;
					CString str = LoadString (IDS_RESTORINGOLDBITMAP) + strMsg;

					MsgBox (* this, str);

					img.CreateFromHBITMAP (m_hbmpOriginal);
					VERIFY (img.Save (strFile, GetCxImageType (strFile)));
			}
			else {
				VERIFY (SaveBitmap (m_hbmpOriginal, strFile));
			}
		}
	}

	if (m_hbmpOriginal) {
		::DeleteObject (m_hbmpOriginal);
		m_hbmpOriginal = NULL;
	}

	delete pElement;

	InvalidatePreview ();
}

void CBitmapDlg::Get (const FoxjetElements::CBitmapElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	ASSERT (pHead);
	
	CElementDlg::Get (e, pHead);

	m_nID						= e.GetID ();
	m_pt						= e.GetPos (pHead);
	m_strFilePath				= e.GetFilePath ();
	m_size						= e.GetSize (* pHead, true);
	m_db.m_bLookup				= e.IsDatabaseLookup ();
	m_db.m_bPrompt				= e.GetDatabasePromptAtTaskStart ();
	m_db.m_strDSN				= e.GetDatabaseDSN ();
	m_db.m_strTable				= e.GetDatabaseTable ();
	m_db.m_strField				= e.GetDatabaseField ();
	m_db.m_strKeyField			= e.GetDatabaseKeyField ();
	m_db.m_strKeyValue			= e.GetDatabaseKeyValue ();

	m_bUserPrompted				= e.IsUserPrompted ();
	m_strUserPrompt				= e.GetUserPrompt ();
	m_bPromptAtTaskStart		= e.IsPromptAtTaskStart ();

	m_db.m_strBmpWidthField		= e.GetDbWidthField ();
	m_db.m_strBmpHeightField	= e.GetDbHeightField ();

//	m_bFlipH					= e.IsFlippedH ();
//	m_bFlipV					= e.IsFlippedV ();
//	m_bInverse					= e.IsInverse ();

	m_bLockAspectRatio			= e.IsLockAspectRatio ();
}

void CBitmapDlg::Set (FoxjetElements::CBitmapElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	ASSERT (pHead);

	CElementDlg::Set (e, pHead);

	e.SetID (m_nID);
	e.SetFilePath (m_strFilePath, * pHead);
	e.SetAlignment (m_align);
	e.SetPos (m_pt = e.GetPosInternal (m_pt), pHead, ALIGNMENT_ADJUST_SUBTRACT);
	e.SetSize (m_size, * pHead, true);

	e.SetDatabaseLookup (m_db.m_bLookup);
	e.SetDatabasePromptAtTaskStart (m_db.m_bPrompt);
	e.SetDatabaseDSN (m_db.m_strDSN);
	e.SetDatabaseTable (m_db.m_strTable);
	e.SetDatabaseField (m_db.m_strField);
	e.SetDatabaseKeyField (m_db.m_strKeyField);
	e.SetDatabaseKeyValue (m_db.m_strKeyValue);

	e.SetUserPrompted (m_bUserPrompted);
	e.SetUserPrompt (m_strUserPrompt);
	e.SetPromptAtTaskStart (m_bPromptAtTaskStart);

	e.SetDbWidthField (m_db.m_strBmpWidthField);
	e.SetDbHeightField (m_db.m_strBmpHeightField);

//	e.SetFlippedH (m_bFlipH ? true : false);
//	e.SetFlippedV (m_bFlipV ? true : false);
//	e.SetInverse (m_bInverse ? true : false);

	e.SetLockAspectRatio (m_bLockAspectRatio);
}


void CBitmapDlg::OnEnableDatabaseClick ()
{
	UpdateUI (CHK_DATABASE);
}


void CBitmapDlg::OnUserPrompt ()
{
	UpdateUI (CHK_USER_PROMPT);
}

void CBitmapDlg::OnDatabaseClick ()
{
	CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, CopyElement (&ListGlobals::defElements.GetElement (DATABASE), &GetHead (), false));

	if (p) {
		p->SetPromptAtTaskStart (m_db.m_bPrompt);

		if (m_db.m_strDSN.GetLength () && m_db.m_strTable.GetLength () && m_db.m_strField.GetLength ()) {
			p->SetDSN (m_db.m_strDSN);
			p->SetTable (m_db.m_strTable);
			p->SetField (m_db.m_strField);
			p->SetKeyField (m_db.m_strKeyField);
			p->SetKeyValue (m_db.m_strKeyValue);
			p->SetBmpWidthField  (m_db.m_strBmpWidthField);
			p->SetBmpHeightField (m_db.m_strBmpHeightField);
		}

		if (OnEditElement (* p, this, CSize (MAX_PRODLEN, MAX_PRODHEIGHT), INCHES, NULL, false, true)) {
			CBitmapElement & e = GetElement ();

			m_db.m_bLookup				= true;
			m_db.m_bPrompt				= p->GetPromptAtTaskStart ();
			m_db.m_strDSN				= p->GetDSN ();
			m_db.m_strTable				= p->GetTable ();
			m_db.m_strField				= p->GetField ();
			m_db.m_strKeyField			= p->GetKeyField ();
			m_db.m_strKeyValue			= p->GetKeyValue ();
			m_db.m_strBmpWidthField		= p->GetBmpWidthField ();
			m_db.m_strBmpHeightField	= p->GetBmpHeightField ();
			m_db.m_sizeDB				= p->GetBitmapSize ();
		}

		delete p;
	}
}


void CBitmapDlg::OnWidthChange ()
{
	if (!m_bEdit) {
		if (CButton * p = (CButton *)GetDlgItem (CHK_LOCKASPECTRATIO)) {
			if (p->GetCheck () == 1) {
				CSize size (0, 0);
				CDataExchange dxGet (this, TRUE);
				CDataExchange dxSet (this, FALSE);
				const double dRatio = m_size.cx / (double)m_size.cy;

				m_bEdit = true;
				DDX_Coord (&dxGet, TXT_WIDTH, (long &)size.cx, m_units, GetHead (), true);
				size.cy = size.cx / dRatio;
				DDX_Coord (&dxSet, TXT_HEIGHT, (long &)size.cy, m_units, GetHead (), true);

				if (CEdit * pEdit = (CEdit *)GetDlgItem (TXT_HEIGHT))
					pEdit->SetModify (TRUE);

				m_bEdit = false;
			}
		}
	}
}

void CBitmapDlg::OnHeightChange ()
{
	if (!m_bEdit) {
		if (CButton * p = (CButton *)GetDlgItem (CHK_LOCKASPECTRATIO)) {
			if (p->GetCheck () == 1) {
				CSize size (0, 0);
				CDataExchange dxGet (this, TRUE);
				CDataExchange dxSet (this, FALSE);
				const double dRatio = m_size.cx / (double)m_size.cy;

				m_bEdit = true;
				DDX_Coord (&dxGet, TXT_HEIGHT, (long &)size.cy, m_units, GetHead (), true);
				size.cx = size.cy * dRatio;
				DDX_Coord (&dxSet, TXT_WIDTH, (long &)size.cx, m_units, GetHead (), true);
				
				if (CEdit * pEdit = (CEdit *)GetDlgItem (TXT_WIDTH))
					pEdit->SetModify (TRUE);

				m_bEdit = false;
			}
		}
	}
}
