// DbWizardDsnDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DbWizardDsnDlg.h"
#include "EnTabCtrl.h"
#include "DbSetupDlg.h"
#include "OdbcDatabase.h"
#include "DatabaseElement.h"
#include "Extern.h"

using namespace FoxjetElements;
using namespace FoxjetDatabase;

// CDbWizardDsnDlg

IMPLEMENT_DYNAMIC(CDbWizardDsnDlg, CDbWizardBasePage)

CDbWizardDsnDlg::CDbWizardDsnDlg (CDbWizardDlg * pParent)
: CDbWizardBasePage (pParent, IDD_DATABASE_WIZARD_DSN)
{
}

CDbWizardDsnDlg::~CDbWizardDsnDlg()
{
}


BEGIN_MESSAGE_MAP(CDbWizardDsnDlg, CDbWizardBasePage)
	ON_BN_CLICKED(BTN_OTHER, OnSetup)
	ON_CBN_SELCHANGE (CB_DSN, OnDsnChange)
END_MESSAGE_MAP()


// CDbWizardDsnDlg message handlers

BOOL CDbWizardDsnDlg::OnInitDialog()
{
	CDbWizardBasePage ::OnInitDialog();

	if (CComboBox * pDSN = (CComboBox *)GetDlgItem (CB_DSN)) {
		CStringArray v;
		CString strDSN;

		if (FoxjetElements::CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, &ListGlobals::defElements.GetElement (DATABASE)))
			strDSN = p->GetDSN ();

		COdbcDatabase::GetDsnEntries (v);

		for (int i = 0; i < v.GetSize (); i++) {
			if (pDSN->FindString (-1, v [i]) == CB_ERR) {
				int nIndex = pDSN->AddString (v [i]);

				if (!strDSN.CompareNoCase (v [i]))
					pDSN->SetCurSel (nIndex);
			}
		}

		if (pDSN->GetCurSel () == CB_ERR)
			pDSN->SetCurSel (0);
	}

	OnDsnChange ();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDbWizardDsnDlg::OnDsnChange ()
{
	m_strDSN = GetCurSelDsn ();

	try
	{
		if (m_pdlgMain->m_pdb) {
			delete m_pdlgMain->m_pdb;
			m_pdlgMain->m_pdb = NULL;
		}

		//m_pdlgMain->m_db.Open (_T (";DSN=") + m_strDSN, FALSE, TRUE);
		//m_pdlgMain->m_db.OpenEx (_T (";DSN=") + m_strDSN, CDatabase::noOdbcDialog);
		m_pdlgMain->m_pdb = new COdbcCache (COdbcDatabase::Find (m_strDSN));
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }
}

CString CDbWizardDsnDlg::GetCurSelDsn () const
{
	CString str;

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_DSN)) 
		if (p->GetCount ())
			p->GetLBText (p->GetCurSel (), str);

	return str;
}

void CDbWizardDsnDlg::OnSetup ()
{
	CDbSetupDlg dlg (this);

	dlg.m_strDSN = GetCurSelDsn ();
	dlg.m_strFile = COdbcDatabase::GetFilePath (dlg.m_strDSN);

	if (dlg.DoModal () == IDOK) {
		if (CComboBox * pDSN = (CComboBox *)GetDlgItem (CB_DSN)) {
			int nIndex = pDSN->FindString (-1, dlg.m_strDSN);

			if (nIndex == CB_ERR)
				nIndex = pDSN->AddString (dlg.m_strDSN);

			pDSN->SetCurSel (nIndex);
			OnDsnChange ();
		}
	}
}

BOOL CDbWizardDsnDlg::OnSetActive()
{
	return TRUE;
}
