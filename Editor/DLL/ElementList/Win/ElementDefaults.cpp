// ElementDefaults.cpp: implementation of the CElementDefaults class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ElementDefaults.h"
#include "Resource.h"
#include "Edit.h"
#include "Debug.h"
#include "Registry.h"
#include "Extern.h"
#include "shlwapi.h"
#include "DevGuids.h"
#include "TextElement.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetElements;
using namespace FoxjetElements::TextElement;

CElementDefaults::CDefault::CDefault (int nClassID)
:	m_pElement (NULL),
	m_nClassID (nClassID)
{
	VERIFY (m_pElement = CreateElement (m_nClassID, GetDefaultHead ()));
	bool bValve = ISVALVE ();

	if (bValve) {
		if (CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, m_pElement)) {
			CStringArray v;

			GetBitmappedFonts (v);

			for (int i = 0; i < v.GetSize (); i++) {
				p->SetFontName (v [i]);
				if (p->GetFontSize () <= 9)
					break;
			}
		}
	}
}

CElementDefaults::CDefault::CDefault (const CDefault & rhs)
:	m_pElement (NULL),
	m_nClassID (rhs.m_nClassID)
{
	VERIFY (m_pElement = CreateElement (m_nClassID, GetDefaultHead ()));
	GetElement () = rhs.GetElement ();
}

CElementDefaults::CDefault & CElementDefaults::CDefault::operator = (const CDefault & rhs)
{
	if (this != &rhs) {
		if (m_pElement) {
			delete m_pElement;
			m_pElement = NULL;
		}

		m_nClassID = rhs.m_nClassID;
		VERIFY (m_pElement = CreateElement (m_nClassID, GetDefaultHead ()));
		GetElement () = rhs.GetElement ();
	}

	return * this;
}

CElementDefaults::CDefault::~CDefault ()
{
	if (m_pElement) {
		delete m_pElement;
		m_pElement = NULL;
	}
}

const FoxjetCommon::CBaseElement & CElementDefaults::CDefault::GetElement () const
{
	ASSERT (m_pElement);
	return * m_pElement;
}

FoxjetCommon::CBaseElement & CElementDefaults::CDefault::GetElement ()
{
	ASSERT (m_pElement);
	return * m_pElement;
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CElementDefaults::CElementDefaults()
:	m_paDef (10000, 4500),
	m_bOverlapped (false)
{
	m_strRegSection = _T ("Software\\FoxJet\\") + GetAppTitle () + _T ("\\Defaults");
}


CElementDefaults::~CElementDefaults()
{
	Free ();
}

void CElementDefaults::Free ()
{
	for (int i = 0; i < m_vDefElements.GetSize (); i++) {
		CDefault * p = m_vDefElements [i];
		delete p;
	}

	m_vDefElements.RemoveAll ();
}

bool CElementDefaults::Reset (const CVersion & ver)
{
	bool bResult = false;
	HKEY hKey = NULL;

	if (::RegOpenKey (HKEY_CURRENT_USER, m_strRegSection, &hKey) == ERROR_SUCCESS) {
		bResult = ::SHDeleteKey (hKey, _T ("Elements")) == ERROR_SUCCESS;
		LoadRegDefs (ver);
	}

	return bResult;
}

void CElementDefaults::LoadRegDefs(const CVersion & ver)
{
	Init (); 

	for (int i = 0; i < GetElementTypeCount (); i++) 
		LoadRegElementDefs (i, ver);
}

void CElementDefaults::SaveRegDefs(const CVersion & ver)
{
	for (int i = 0; GetElementTypeCount (); i++)
		SaveRegElementDefs (i, ver);
}

bool CElementDefaults::SaveRegElementDefs(int nType, const CVersion & ver)
{
	if (IsValidElementType (nType, GetDefaultHead ())) {
		const CBaseElement & element = GetElement (nType);
		CRuntimeClass * pClass = element.GetRuntimeClass ();
		CString strKey = GetElementResStr (nType);
		CString strData = element.ToString (ver);

		TRACEF ("WriteProfileString\n\t" +
			m_strRegSection + "\\" + strKey + "\\" + strData);
		FoxjetDatabase::WriteProfileString (
			m_strRegSection + _T ("\\Elements"), strKey, strData);
		return true;
	}

	return false;
}

bool CElementDefaults::LoadRegElementDefs(int nType, const CVersion & ver)
{
	bool bStatus = false;

	#ifdef _DEBUG
	CString strDebug;
	#endif //_DEBUG

	if (IsValidElementType (nType, GetDefaultHead ())) {
		CString str = GetRegElementDefs (nType, ver);

		if (GetElement (nType, 0).FromString (str, ver)) {
			bStatus = true;

			#ifdef _DEBUG
			strDebug.Format (
				_T ("\n\tIsValidElementType (%d)")
				_T ("\n\tGetRegElementDefs (...) = \"%s\""),
				nType, str);
			#endif //_DEBUG
		}
	}

	#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
	strDebug += _T ("\n\tbResult = ") + CString (bStatus ? _T ("true") : _T ("false"));
	TRACEF (strDebug);
	#endif //_DEBUG	&& __TRACE_TOFROMSTRING__

	return bStatus;
}

CString CElementDefaults::GetRegElementDefs(int nType, const CVersion &ver) const
{
	CString strDef, strKey;

	if (IsValidElementType (nType, GetDefaultHead ())) {
		const CBaseElement & def = GetElement (nType);
		
		strKey = GetElementResStr (nType);
		strDef = def.ToString (ver);
	}

	return FoxjetDatabase::GetProfileString (
		m_strRegSection + _T ("\\Elements"), strKey, strDef);
}

void CElementDefaults::SetElementFonts (const CString & strFont, int nSize, 
										bool bBold, bool bItalic, const CVersion & ver)
{
	CPrinterFont fnt (strFont, nSize, bBold, bItalic);

	for (int i = 0; i < GetElementTypeCount (); i++) {
		if (GetElement (i, 0).HasFont ()) {
			CBaseTextElement & e = * (CBaseTextElement *)&GetElement (i, 0);
			e.SetFont (fnt);
		}

		SaveRegElementDefs (i, ver);
	}
}

// 2nd param is only used to resolve this fct from
// GetElement (int) because it is public and this one
// is not.
CBaseElement & CElementDefaults::GetElement(int nType, int)
{
	ASSERT (m_vDefElements.GetSize ());

	for (int i = 0; i < m_vDefElements.GetSize (); i++) {
		CDefault * pDef = m_vDefElements [i];

		if (pDef->GetClassID () == nType)
			return pDef->GetElement ();
	}

	ASSERT (0);
	CDefault * pDef = m_vDefElements [0];
	return pDef->GetElement ();
}

const CBaseElement & CElementDefaults::GetElement(int nType) const
{
	ASSERT (m_vDefElements.GetSize ());

	for (int i = 0; i < m_vDefElements.GetSize (); i++) {
		const CDefault * pDef = m_vDefElements [i];

		if (pDef->GetClassID () == nType)
			return pDef->GetElement ();
	}

	ASSERT (0);
	const CDefault * pDef = m_vDefElements [0];
	return pDef->GetElement ();
}

bool CElementDefaults::SetElement(int nType, const CString &str, const CVersion & ver)
{
	if (IsValidElementType (nType, GetDefaultHead ())) {
		if (GetElement (nType, 0).FromString (str, ver)) {
			return SaveRegElementDefs (nType, ver);
			//return true;
		}
		else {
			TRACEF (_T ("FromString failed"));
		}
	}

	return false;
}

bool CElementDefaults::SetElement(int nType, const CBaseElement &obj, const CVersion & ver)
{
	for (int i = 0; i < m_vDefElements.GetSize (); i++) {
		CDefault * pDef = m_vDefElements [i];

		if (pDef->GetClassID () == nType) {
			if (pDef->GetElement ().FromString (obj.ToString (::verApp), ::verApp)) {
				TRACEF (pDef->GetElement ().ToString (::verApp));
				TRACEF (obj.ToString (::verApp));
				return SaveRegElementDefs (nType, ver);
			}
		}
	}

	return false;
}

UNITS CElementDefaults::GetUnits () const
{
	UNITS def = INCHES;

	int n = FoxjetDatabase::GetProfileInt (m_strRegSection + _T ("\\ProductArea"), 
		_T ("Units"), (UINT)def); 

	return (UNITS)n;
}

CSize CElementDefaults::GetProductArea () const
{
	return CSize (
		FoxjetDatabase::GetProfileInt (m_strRegSection + _T ("\\ProductArea"), 
			_T ("cx"), 10000),
		FoxjetDatabase::GetProfileInt (m_strRegSection + _T ("\\ProductArea"), 
			_T ("cy"), 1000));
}

bool CElementDefaults::SetUnits (UNITS units)
{
	return FoxjetDatabase::WriteProfileInt (
		m_strRegSection + _T ("\\ProductArea"), _T ("Units"), units); 
}

bool CElementDefaults::SetProductArea (const CSize & pa) 
{
	return
		WriteProfileInt (m_strRegSection + _T ("\\ProductArea"), _T ("cx"), pa.cx) &&
		WriteProfileInt (m_strRegSection + _T ("\\ProductArea"), _T ("cy"), pa.cy);
}

void CElementDefaults::Init()
{
	Free ();

	for (int i = 0; i < GetElementTypeCount (); i++) {
		CDefault * p = new CDefault (i);

		p->GetElement ().SetID (0);
		m_vDefElements.Add (p);
	}
}


ULONG CElementDefaults::GetHeadID () const
{
	return FoxjetDatabase::GetProfileInt (m_strRegSection + _T ("\\ProductArea"), _T ("Head"), 1); 
}

bool CElementDefaults::SetHeadID (ULONG lHeadID)
{
	return WriteProfileInt (m_strRegSection + _T ("\\ProductArea"), _T ("Head"), 
		lHeadID); 
}

bool CElementDefaults::GetOverlappedWarning () const
{
	return m_bOverlapped;
}

void CElementDefaults::SetOverlappedWarning (bool bWarn)
{
	m_bOverlapped = bWarn;
}
