#if !defined(AFX_DATAMATRIXDLG_H__1FD88B61_F5C3_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_DATAMATRIXDLG_H__1FD88B61_F5C3_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataMatrixDlg.h : header file
//

#include "resource.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CDataMatrixDlg dialog

class CDataMatrixDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CDataMatrixDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDataMatrixDlg)
	enum { IDD = IDD_DATAMATRIX };
	int		m_nLength;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataMatrixDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDataMatrixDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAMATRIXDLG_H__1FD88B61_F5C3_11D4_8FC6_006067662794__INCLUDED_)
