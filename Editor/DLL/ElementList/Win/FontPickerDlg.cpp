// FontPickerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "list.h"
#include "FontPickerDlg.h"
#include "Extern.h"
#include "TemplExt.h"
#include "Debug.h"
#include "Parse.h"
#include "WinGdi.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ListGlobals;
using namespace ItiLibrary;

CFontPickerDlg::CFontItem::CFontItem (const CString & strFont, int nTotal, int nMatched)
:	m_strFont (strFont),
	m_nTotal (nTotal),
	m_nMatched (nMatched)
{
}

CFontPickerDlg::CFontItem::~CFontItem ()
{
}

CString CFontPickerDlg::CFontItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0:
		return m_strFont;
	case 1:
		if (CalcMatch () > 0.0)
			str.Format (_T ("%.01f %%"), CalcMatch ());
		break;
	}

	return str;

}

int CFontPickerDlg::CFontItem::Compare (const ItiLibrary::ListCtrlImp::CItem & cmp, int nColumn) const
{
	CFontItem * rhs = (CFontItem *)&cmp;

	switch (nColumn) {
	case 1:		
		int nRHS = (int)(rhs->CalcMatch () * 1000.0);
		int nLHS = (int)(CalcMatch () * 1000.0);

		return nRHS - nLHS;
	}

	return CItem::Compare (cmp, nColumn);
}

double CFontPickerDlg::CFontItem::CalcMatch () const
{
	if (m_nMatched > 0 && m_nTotal > 0)
		return (double)m_nMatched / (double)m_nTotal * 100.0;

	return 0.0;
}

/////////////////////////////////////////////////////////////////////////////
// CFontPickerDlg dialog


CFontPickerDlg::CFontPickerDlg(const CElementList * pList, CWnd* pParent /*=NULL*/)
:	m_pList (pList),
	m_hThread (NULL),
	FoxjetCommon::CEliteDlg(IDD_FONT_PICKER, pParent)
{
	//{{AFX_DATA_INIT(CFontPickerDlg)
	m_strSample = _T("");
	//}}AFX_DATA_INIT
}


void CFontPickerDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFontPickerDlg)
	DDX_Text(pDX, TXT_SAMPLE, m_strSample);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFontPickerDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CFontPickerDlg)
	ON_BN_CLICKED(BTN_MATCH, OnMatch)
	ON_WM_DESTROY()
	ON_NOTIFY(LVN_ITEMCHANGED, LV_FONT, OnItemchangedFont)
	ON_NOTIFY(NM_DBLCLK, LV_FONT, OnDblclkFont)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFontPickerDlg message handlers

BOOL CFontPickerDlg::OnInitDialog() 
{
	using namespace ItiLibrary::ListCtrlImp;

	CArray <CColumn, CColumn> vCols;
	CArray <CPrinterFont, CPrinterFont &> vFonts;

	FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_FONT), 160));
	vCols.Add (CColumn (LoadString (IDS_MATCH), 80));
	
	m_lv.Create (LV_FONT, vCols, this, defElements.m_strRegSection);

	if (m_pList) {
		m_pList->GetFonts (vFonts);

		for (int i = 0; i < vFonts.GetSize (); i++) 
			m_lv.InsertCtrlData (new CFontItem (vFonts [i].m_strName));
	}

	m_lv->SortItems (ItiLibrary::ListCtrlImp::CListCtrlImp::CompareFunc, 0);

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (!m_lv->GetItemText (i, 0).CompareNoCase (m_strFont)) {
			m_lv->SetItemState (i, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
			m_lv->EnsureVisible (i, FALSE);
			OnItemchangedFont (NULL, NULL);
			break;
		}
	}

	if (GetSelectedItems (m_lv).GetSize () == 0) {
		m_lv->SetItemState (0, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
		m_lv->EnsureVisible (0, FALSE);
		OnItemchangedFont (NULL, NULL);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFontPickerDlg::OnMatch() 
{
	DWORD dwThreadID;
	VERIFY (m_hThread = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)Match, (LPVOID)this, 0, &dwThreadID));
}

ULONG CALLBACK CFontPickerDlg::Match (LPVOID lpData)
{
	CFontPickerDlg & dlg = * (CFontPickerDlg *)lpData;
	UINT nID [] = 
	{
		IDOK,
		IDCANCEL,
		BTN_MATCH,
		LV_FONT,
		TXT_SAMPLE,
	};

	dlg.BeginWaitCursor ();

	for (int i = 0; i < ARRAYSIZE (nID); i++)
		if (CWnd * p = dlg.GetDlgItem (nID [i]))
			p->EnableWindow (FALSE);


	{
		CString strSample;

		dlg.GetDlgItemText (TXT_SAMPLE, strSample);

		for (int j = 0; j < strSample.GetLength (); j++)
			TRACEF (FoxjetDatabase::ToString (j) + _T (": ") + FoxjetDatabase::ToString (strSample [j]));

		const int nLen = strSample.GetLength ();

		for (int i = 0; i < dlg.m_lv->GetItemCount (); i++) {
			if (CFontPickerDlg::CFontItem * pItem = (CFontPickerDlg::CFontItem *)dlg.m_lv.GetCtrlData (i)) {
				CFont fnt;
				GCP_RESULTS gcp;
				CDC dc;

				VERIFY (dc.CreateCompatibleDC (dlg.GetDC ()));
				VERIFY (fnt.CreateFont (72, 0, 0, 0, FW_NORMAL,  FALSE, 0, 0,
					DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
					DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, pItem->m_strFont)); 

				CFont * pFont = dc.SelectObject (&fnt);

				pItem->m_nTotal = strSample.GetLength ();
				pItem->m_nMatched = 0;

				//TRACEF (pItem->m_strFont);

				for (int j = 0; j < strSample.GetLength (); j++) {
					CString str = strSample [j];
					WORD wGlyphs [256] = { 0 };  // no documentation found to explain this, 
												// but GetCharacterPlacement will overwrite a small [2] buffer

					::ZeroMemory (&gcp, sizeof (gcp));
					gcp.lStructSize = sizeof (gcp);
					gcp.lpGlyphs = (LPWSTR)&wGlyphs;
					gcp.nGlyphs = nLen;

					DWORD dwGetCharacterPlacement = ::GetCharacterPlacement (dc, str, nLen, 0, &gcp, 0);
					
					/*
					{
						CString strDebug, strTrace = pItem->m_strFont;

						for (int n = 0; n < ARRAYSIZE (wGlyphs); n++) {
							if (gcp.lpGlyphs [n]) {
								strDebug.Format (_T (" [%d: 0x%04X],"), n, gcp.lpGlyphs [n]);
								strTrace += strDebug;
							}
						}
						
						TRACEF (strTrace);
					}
					*/

					if (dwGetCharacterPlacement)
						if (gcp.lpGlyphs [0])
							pItem->m_nMatched++;
				}

				//TRACEF ("");

				dlg.m_lv.UpdateCtrlData (pItem);
				dlg.m_lv->EnsureVisible (i, FALSE);
			}
		}
	}

	dlg.m_lv->SortItems (ItiLibrary::ListCtrlImp::CListCtrlImp::CompareFunc, 1);
	dlg.m_lv->EnsureVisible (0, FALSE);
	dlg.m_hThread = NULL;

	for (int i = 0; i < ARRAYSIZE (nID); i++)
		if (CWnd * p = dlg.GetDlgItem (nID [i]))
			p->EnableWindow (TRUE);

	dlg.EndWaitCursor ();
	
	return 0;
}

void CFontPickerDlg::OnDestroy() 
{
	if (m_hThread)
		return;

	FoxjetCommon::CEliteDlg::OnDestroy();	
}

void CFontPickerDlg::OnItemchangedFont(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		if (CFontItem * pItem = (CFontPickerDlg::CFontItem *)m_lv.GetCtrlData (sel [0])) {
			CFont fnt;

			VERIFY (fnt.CreateFont (72, 0, 0, 0, FW_NORMAL,  FALSE, 0, 0,
				DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
				DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, pItem->m_strFont)); 

			m_strFont = pItem->m_strFont;
			::DeleteObject (m_fnt.Detach ());
			m_fnt.Attach (fnt.Detach ());

			if (CWnd * p = GetDlgItem (TXT_SAMPLE))
				p->SetFont (&m_fnt);
		}
	}
	
	if (pResult)
		* pResult = 0;
}

void CFontPickerDlg::OnDblclkFont(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnOK ();	
	*pResult = 0;
}
