#pragma once

#include "DbWizard.h"
#include "ListCtrlImp.h"
#include "StdCommDlg.h"

namespace FoxjetElements
{
	// CSerialWizardPortsPage

	class ELEMENT_API CSerialWizardSheet;

	class ELEMENT_API CPortItem : public FoxjetCommon::StdCommDlg::CCommItem
	{
	public:
		virtual CString GetDispText (int nColumn) const;

		CString m_strSampleData;
	};

	class ELEMENT_API CSerialWizardPortsPage : public CWizardBasePage
	{
		DECLARE_DYNAMIC(CSerialWizardPortsPage)

	public:
		CSerialWizardPortsPage(CSerialWizardSheet * pParent);
		virtual ~CSerialWizardPortsPage();

		virtual BOOL OnSetActive();
		virtual BOOL OnKillActive();
		virtual BOOL OnInitDialog();
		virtual bool CanAdvance ();

		CPortItem m_port;

		CString GetSampleData () const;
		void SetSampleData (const CString & str);

	protected:
		void StartThreads (DWORD dwWait = 1000);
		void KillThreads ();

		afx_msg void OnBaud ();
		afx_msg void OnDestroy();
		afx_msg LRESULT OnSerialData (WPARAM wParam, LPARAM lParam);
		afx_msg void OnItemchangedPorts(NMHDR *pNMHDR, LRESULT *pResult);

		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
		CArray <HANDLE, HANDLE> m_vThreads;
		HANDLE m_hExit;

		DECLARE_MESSAGE_MAP()
	};

	class ELEMENT_API CSerialWizardIndexPage : public CWizardBasePage
	{
		DECLARE_DYNAMIC(CSerialWizardIndexPage)

	public:
		CSerialWizardIndexPage (CSerialWizardSheet * pParent);
		virtual ~CSerialWizardIndexPage ();

		int m_nIndex;
		int m_nLength;
		CString m_strSampleData;
		bool m_bSetDefaultData;

	protected:
		virtual BOOL OnSetActive();
		virtual BOOL OnKillActive();
		virtual BOOL OnInitDialog();
		virtual bool CanAdvance ();

		afx_msg void OnChangeSampleData ();
		afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
		afx_msg void OnDeltaposIndex(NMHDR* pNMHDR, LRESULT* pResult); 
		afx_msg void OnDeltaposLength(NMHDR* pNMHDR, LRESULT* pResult); 

		bool m_bChangedSampleData;

		DECLARE_MESSAGE_MAP()
	};

	class ELEMENT_API CSerialWizardPrintOncePage : public CWizardBasePage
	{
		DECLARE_DYNAMIC(CSerialWizardPrintOncePage)

	public:
		CSerialWizardPrintOncePage (CSerialWizardSheet * pParent);
		virtual ~CSerialWizardPrintOncePage ();

		virtual BOOL OnKillActive();

		bool m_bPrintOnce;

	protected:
		virtual BOOL OnInitDialog();

		DECLARE_MESSAGE_MAP()
	};

	class ELEMENT_API CSerialWizardEncodingPage : public CWizardBasePage
	{
		DECLARE_DYNAMIC(CSerialWizardEncodingPage)

	public:
		CSerialWizardEncodingPage (CSerialWizardSheet * pParent);
		virtual ~CSerialWizardEncodingPage ();

		int m_nEncoding;

	protected:
		virtual BOOL OnInitDialog();
		virtual BOOL OnKillActive ();

		DECLARE_MESSAGE_MAP()
	};

	class ELEMENT_API CSerialWizardFinishPage : public CWizardBasePage
	{
		DECLARE_DYNAMIC(CSerialWizardFinishPage)

	public:
		CSerialWizardFinishPage (CSerialWizardSheet * pParent);
		virtual ~CSerialWizardFinishPage ();

		bool m_bTemplate;

	protected:
		void OnUpdateUI ();
		virtual BOOL OnInitDialog();

		DECLARE_MESSAGE_MAP()
	};

	class ELEMENT_API CSerialWizardSheet : public CBaseWizardSheet
	{
		DECLARE_DYNAMIC(CSerialWizardSheet)

	public:
		CSerialWizardSheet(CWnd * pParent);
		virtual ~CSerialWizardSheet();

		virtual BOOL OnInitDialog();

		afx_msg void OnDestroy();
		afx_msg void OnTimer(UINT nIDEvent);

		ULONG m_lLineID;

		CSerialWizardPortsPage *		m_pdlgPort;
		CSerialWizardIndexPage *		m_pdlgIndex;
		CSerialWizardEncodingPage *		m_pdlgEncoding;
		CSerialWizardPrintOncePage *	m_pdlgPrintOnce;
		CSerialWizardFinishPage *		m_pdlgFinish;

	protected:
		DECLARE_MESSAGE_MAP()
	};
};

