#if !defined(AFX_SHIFTDLG_H__471C7427_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_SHIFTDLG_H__471C7427_8B3D_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ShiftDlg.h : header file
//

#include "BaseTextDlg.h"
#include "ShiftElement.h"
#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// CShiftDlg dialog

class CShiftDlg : public CBaseTextDlg
{
// Construction
public:
	CShiftDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CShiftDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	void Get (const FoxjetElements::CShiftElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);
	void Set (FoxjetElements::CShiftElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);

protected:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShiftDlg)
	protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual int BuildElement ();
	const FoxjetElements::CShiftElement & GetElement () const;
	FoxjetElements::CShiftElement & GetElement ();

	// Generated message map functions
	//{{AFX_MSG(CShiftDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHIFTDLG_H__471C7427_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
