// CopyBarcodeParamsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "CopyBarcodeParamsDlg.h"
#include "OdbcRecordset.h"
#include "Debug.h"
#include "FieldDefs.h"
#include "serialize.h"
#include "Extern.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ItiLibrary;
using namespace FoxjetDatabase;

CCopyBarcodeParamsDlg::CCopyItem::CCopyItem ()
{
}

CCopyBarcodeParamsDlg::CCopyItem::~CCopyItem ()
{
}

CString CCopyBarcodeParamsDlg::CCopyItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:	return m_printer.m_strName;
	case 1:	return m_printer.m_strAddress;
	case 2:	return m_line.m_strName;
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CCopyBarcodeParamsDlg dialog


CCopyBarcodeParamsDlg::CCopyBarcodeParamsDlg(FoxjetDatabase::COdbcDatabase & db, const CString & strRegSection, CWnd* pParent /*=NULL*/)
:	m_db (db),
	m_strRegSection (strRegSection),
	FoxjetCommon::CEliteDlg(CCopyBarcodeParamsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCopyBarcodeParamsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCopyBarcodeParamsDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCopyBarcodeParamsDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCopyBarcodeParamsDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CCopyBarcodeParamsDlg)
	//}}AFX_MSG_MAP
	ON_NOTIFY(NM_DBLCLK, LV_PARAMS, OnDblclkParams)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCopyBarcodeParamsDlg message handlers

BOOL CCopyBarcodeParamsDlg::OnInitDialog() 
{
	using namespace FoxjetDatabase;
	using ListCtrlImp::CColumn;

	CArray <CColumn, CColumn> vCols;
	CArray <PRINTERSTRUCT, PRINTERSTRUCT &> vPrinters;
	LINESTRUCT line;

	GetLineRecord (m_db, m_lLineID, line);

	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	vCols.Add (CColumn (LoadString (IDS_PRINTER), 200));
	vCols.Add (CColumn (LoadString (IDS_ADDRESS), 150));
	vCols.Add (CColumn (LoadString (IDS_LINE), 200));

	m_lv.Create (LV_PARAMS, vCols, this, m_strRegSection);
	GetPrinterRecords (m_db, vPrinters);

	for (int nPrinter = 0; nPrinter < vPrinters.GetSize (); nPrinter++) {
		CArray <LINESTRUCT, LINESTRUCT &> vLines;
		PRINTERSTRUCT printer = vPrinters [nPrinter];
		bool bFound = false;

		GetPrinterLines (m_db, printer.m_lID, vLines);
		
		for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
			if (vLines [nLine].m_lID == m_lLineID) {
				bFound = true;
				break;
			}
		}

		if (bFound)
			continue;

		for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
			if (!vLines [nLine].m_strName.CompareNoCase (line.m_strName)) {
				CCopyItem * p = new CCopyItem ();

				try {
					CString str;
					COdbcRecordset rst (m_db);

					str.Format (_T ("SELECT * FROM [%s] WHERE [%s] LIKE '%%%s%%' AND [%s]=%d;"),
						Settings::m_lpszTable, 
						Settings::m_lpszKey, ListGlobals::Keys::m_strBarcode,
						Settings::m_lpszLineID, vLines [nLine].m_lID);
					TRACEF (str);

					if (rst.Open (str)) {
						if (!rst.IsEOF ()) {
							p->m_printer = printer;
							p->m_line = vLines [nLine];
							m_lv.InsertCtrlData (p);
						}
					}
				}
				catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
				catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
			}
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CCopyBarcodeParamsDlg::OnDblclkParams(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnOK ();
	
	if (pResult)
		* pResult = 0;
}

void CCopyBarcodeParamsDlg::OnOK()
{
	FoxjetCommon::CLongArray sel = GetSelectedItems (m_lv);

	if (!sel.GetSize ()) {
		MsgBox (LoadString (IDS_NOTHINGSELECTED));
		return;
	}

	if (CCopyItem * p = (CCopyItem * )m_lv.GetCtrlData (sel [0])) {
		m_lLineID = p->m_line.m_lID;
		FoxjetCommon::CEliteDlg::OnOK ();
	}
}

