// DefineShiftDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "Resource.h"
#include "DefineShiftDlg.h"
#include "Extern.h"
#include "Edit.h"
#include "Database.h"
#include "Extern.h"
#include "Debug.h"
#include "Utils.h"
#include "ShiftElement.h"
#include "Debug.h"
#include "Parse.h"
#include "MsgBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace FoxjetDatabase;
using namespace ListGlobals;
using namespace DefineShiftDlg;

extern CDiagnosticCriticalSection csShift;
extern CArray <CShiftCode, CShiftCode &> vShiftcodes;

/////////////////////////////////////////////////////////////////////////////
CShiftCode::CShiftCode (ULONG lLineID)
:	m_lLineID			(lLineID),
	m_dtShift1			(COleDateTime (2003, 1, 1, 7,	0, 0)),
	m_dtShift2			(COleDateTime (2003, 1, 1, 15,	0, 0)),
	m_dtShift3			(COleDateTime (2003, 1, 1, 23,	0, 0))
{
	m_strShiftCode1			= LoadString (IDS_FIRST);
	m_strShiftCode2			= LoadString (IDS_SECOND);
	m_strShiftCode3			= LoadString (IDS_THIRD);
}

CShiftCode::CShiftCode (const CShiftCode & rhs)
:	m_lLineID			(rhs.m_lLineID),
	m_dtShift1         	(rhs.m_dtShift1),
	m_dtShift2         	(rhs.m_dtShift2),
	m_dtShift3         	(rhs.m_dtShift3),
	m_strShiftCode1		(rhs.m_strShiftCode1),
	m_strShiftCode2		(rhs.m_strShiftCode2),
	m_strShiftCode3		(rhs.m_strShiftCode3)
{
}

CShiftCode & CShiftCode::operator = (const CShiftCode & rhs)
{
	if (this != &rhs) {
		m_lLineID				= rhs.m_lLineID;
		m_dtShift1         		= rhs.m_dtShift1;
		m_dtShift2         		= rhs.m_dtShift2;
		m_dtShift3         		= rhs.m_dtShift3;
		m_strShiftCode1			= rhs.m_strShiftCode1;	
		m_strShiftCode2			= rhs.m_strShiftCode2;	
		m_strShiftCode3			= rhs.m_strShiftCode3;	
	}

	return * this;
}

CShiftCode::~CShiftCode ()
{
}

CString CShiftCode::ToString (const FoxjetCommon::CVersion & ver) const
{
	CString str;

	str.Format (_T ("{Shiftcode,%d,%s,%s,%s,%s,%s,%s}"),
		m_lLineID,
		m_dtShift1.Format (_T ("%H:%M:%S")),
		m_strShiftCode1,
		m_dtShift2.Format (_T ("%H:%M:%S")),
		m_strShiftCode2,
		m_dtShift3.Format (_T ("%H:%M:%S")),
		m_strShiftCode3);

	return str;
}

bool CShiftCode::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	CStringArray vActual;
	CPrinterFont f;
	CString strToken [] = {
		_T (""),			// Prefix (required)
		_T (""),			// LineID (required)
		_T ("7:00:00"),		// 
		_T ("First"),		// 
		_T ("15:00:00"),	// 
		_T ("Second"),		// 
		_T ("23:00:00"),	// 
		_T ("Third"),		// 
	};
	const int nFields = sizeof (strToken) / sizeof (strToken [0]);

	Tokenize (str, vActual);

	if (vActual.GetSize () < 2) {
		TRACEF (_T ("Too few tokens"));
		return false;
	}

	for (int i = 0; i < min (vActual.GetSize (), nFields); i++)
		if (vActual [i].GetLength ())
			strToken [i] = vActual [i];

	if (strToken [0].CompareNoCase (_T ("Shiftcode")) != 0){
		TRACEF (_T ("Wrong prefix \"") + strToken [0] + _T ("\""));
		return false;
	}

	try {
		int nIndex = 1;

		m_lLineID		= _tcstoul (strToken [nIndex++], NULL, 10);
		m_dtShift1.ParseDateTime (	strToken [nIndex++]);
		m_strShiftCode1 =			strToken [nIndex++];
		m_dtShift2.ParseDateTime (	strToken [nIndex++]);
		m_strShiftCode2 =			strToken [nIndex++];
		m_dtShift3.ParseDateTime (	strToken [nIndex++]);
		m_strShiftCode3 =			strToken [nIndex++];
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); return false; }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); return false; }

	return true;
}

void ELEMENT_API FoxjetElements::LoadShiftcodes (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	LOCK (::csShift);
	CArray <LINESTRUCT, LINESTRUCT &> vLines;

	GetLineRecords (vLines);
	::vShiftcodes.RemoveAll ();

	for (int i = 0; i < vLines.GetSize (); i++) {
		SETTINGSSTRUCT s;
		LINESTRUCT & line = vLines [i];
		CShiftCode code (line.m_lID);

		line.m_lID = GetMasterLineID (line.m_lID);

		#if __CUSTOM__ == __SW0877__
		line.m_lID = 0;
		#endif

		if (GetSettingsRecord (db, line.m_lID, Keys::m_strShiftcode, s)) 
			code.FromString (s.m_strData, ver);

		#if __CUSTOM__ == __SW0877__
		code.m_lLineID = 0;
		#endif

		ASSERT (code.m_dtShift1.GetStatus () == COleDateTime::valid);
		ASSERT (code.m_dtShift2.GetStatus () == COleDateTime::valid);
		ASSERT (code.m_dtShift3.GetStatus () == COleDateTime::valid);

		::vShiftcodes.Add (code);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDefineShiftDlg dialog

const FoxjetCommon::LIMITSTRUCT CDefineShiftDlg::m_lmtShift = { 1, 15 };

CDefineShiftDlg::CDefineShiftDlg(CWnd* pParent /*=NULL*/)
:	m_lLineID (-1),
	FoxjetCommon::CEliteDlg(IDD_DEFINESHIFTCODES, pParent)
{
	//{{AFX_DATA_INIT(CDefineShiftDlg)
	//}}AFX_DATA_INIT
}

CShiftCode CDefineShiftDlg::GetCurSelShiftCode () const
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CShiftCode s;

	ASSERT (pLine);

	int nIndex = pLine->GetCurSel ();

	if (nIndex != CB_ERR) {
		int nShiftCode = pLine->GetItemData (nIndex);

		s = m_vCodes [nShiftCode];
	}

	return s;
}

bool CDefineShiftDlg::SetCurSelShiftCode (const CShiftCode & s)
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);

	ASSERT (pLine);

	int nIndex = pLine->GetCurSel ();

	if (nIndex != CB_ERR) {
		int nShiftCode = pLine->GetItemData (nIndex);

		m_vCodes.SetAt (nShiftCode, CShiftCode (s));
		return true;
	}

	return false;
}

void CDefineShiftDlg::DoDataExchange(CDataExchange* pDX)
{
	CShiftCode s = GetCurSelShiftCode ();

	struct
	{
		CDateTimeCtrl * m_pCtrl;
		COleDateTime * m_pMember;
	} map [3] = 
	{
		{ (CDateTimeCtrl *)GetDlgItem (DT_SHIFT1), &s.m_dtShift1, },
		{ (CDateTimeCtrl *)GetDlgItem (DT_SHIFT2), &s.m_dtShift2, },
		{ (CDateTimeCtrl *)GetDlgItem (DT_SHIFT3), &s.m_dtShift3, },
	};

	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDefineShiftDlg)
	//}}AFX_DATA_MAP

	DDX_Text(pDX, TXT_SHIFTCODE1,	s.m_strShiftCode1);
	DDV_MinMaxChars (pDX, s.m_strShiftCode1, TXT_SHIFTCODE1, m_lmtShift);

	DDX_Text(pDX, TXT_SHIFTCODE2,	s.m_strShiftCode2);
	DDV_MinMaxChars (pDX, s.m_strShiftCode2, TXT_SHIFTCODE2, m_lmtShift);
	
	DDX_Text(pDX, TXT_SHIFTCODE3,	s.m_strShiftCode3);
	DDV_MinMaxChars (pDX, s.m_strShiftCode3, TXT_SHIFTCODE3, m_lmtShift);
	
	if (pDX->m_bSaveAndValidate) {
		COleDateTime * pdt [3] = 
		{
			map [0].m_pMember,
			map [1].m_pMember,
			map [2].m_pMember,
		};

		for (int i = 0; i < 3; i++) {
			ASSERT (map [i].m_pCtrl);
			ASSERT (map [i].m_pMember);

			VERIFY (map [i].m_pCtrl->GetTime (* pdt [i]));
		}

		ULONG lShift1 = CShiftElement::TimeToLong (* pdt [0]);
		ULONG lShift2 = CShiftElement::TimeToLong (* pdt [1]);
		ULONG lShift3 = CShiftElement::TimeToLong (* pdt [2]);

		if (lShift1 >= lShift2) {
			MsgBox (* this, LoadString (IDS_SHIFTCODESOUTOFORDER1), (LPCTSTR)NULL, MB_ICONWARNING);
			pDX->PrepareCtrl (DT_SHIFT1);
			pDX->Fail ();
		}
		else if (lShift2 >= lShift3) {
			MsgBox (* this, LoadString (IDS_SHIFTCODESOUTOFORDER2), (LPCTSTR)NULL, MB_ICONWARNING);
			pDX->PrepareCtrl (DT_SHIFT2);
			pDX->Fail ();
		}

		VERIFY (SetCurSelShiftCode (s));
	}
	else {
		for (int i = 0; i < 3; i++) {
			ASSERT (map [i].m_pCtrl);
			ASSERT (map [i].m_pMember);
			
			VERIFY (map [i].m_pCtrl->SetTime (* map [i].m_pMember));
		}
	}
}

BEGIN_MESSAGE_MAP(CDefineShiftDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDefineShiftDlg)
	ON_CBN_SELCHANGE(CB_LINE, OnSelchangeLine)
	ON_BN_CLICKED(BTN_APPLY, OnApply)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE (TXT_SHIFTCODE1,	EnableApply)
	ON_EN_CHANGE (TXT_SHIFTCODE2,	EnableApply)
	ON_EN_CHANGE (TXT_SHIFTCODE3,	EnableApply)
	ON_NOTIFY(DTN_DATETIMECHANGE, DT_SHIFT1, OnDatetimechangeShift)
	ON_NOTIFY(DTN_DATETIMECHANGE, DT_SHIFT2, OnDatetimechangeShift)
	ON_NOTIFY(DTN_DATETIMECHANGE, DT_SHIFT3, OnDatetimechangeShift)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDefineShiftDlg message handlers

BOOL CDefineShiftDlg::OnInitDialog() 
{
	using namespace FoxjetDatabase;

	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	
	ASSERT (pLine);
	
	#if __CUSTOM__ == __SW0877__
	pLine->EnableWindow (FALSE);
	m_lLineID = 0;

		#ifndef _DEBUG
		pLine->ShowWindow (SW_HIDE);
		#endif
	#endif

	GetLineRecords (vLines);

	for (int i = 0; i < vLines.GetSize (); i++) {
		LINESTRUCT line = vLines [i];

		int nIndex = pLine->AddString (line.m_strName);
		pLine->SetItemData (nIndex, i);

		#if __CUSTOM__ == __SW0877__
		pLine->SetItemData (nIndex, 0);
		#endif

		if (m_lLineID == line.m_lID)
			pLine->SetCurSel (nIndex);
	}

	if (pLine->GetCurSel () == CB_ERR && pLine->GetCount ())
		pLine->SetCurSel (0);

	{
		LOCK (::csShift);

		for (int i = 0; i < ::vShiftcodes.GetSize (); i++)
			m_vCodes.Add (::vShiftcodes [i]);
	}

	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();
	InitCtrls ();

	ASSERT (GetDlgItem (BTN_APPLY));
	GetDlgItem (BTN_APPLY)->EnableWindow (FALSE);

	return bResult;
}

void CDefineShiftDlg::InitCtrls ()
{
	const CShiftCode s = GetCurSelShiftCode ();
	struct 
	{
		CDateTimeCtrl *			m_pCtrl;
		const COleDateTime *	m_pDT;
	} shiftmap [] =
	{
		{ (CDateTimeCtrl *)GetDlgItem (DT_SHIFT1),	&s.m_dtShift1, },
		{ (CDateTimeCtrl *)GetDlgItem (DT_SHIFT2),	&s.m_dtShift2, },
		{ (CDateTimeCtrl *)GetDlgItem (DT_SHIFT3),	&s.m_dtShift3, },
	};

	for (int i = 0; i < (sizeof (shiftmap) / sizeof (shiftmap [0])); i++) {
		ASSERT (shiftmap [i].m_pCtrl);
		ASSERT (shiftmap [i].m_pDT);

		shiftmap [i].m_pCtrl->SetFormat (_T ("HH:mm"));
	}
}

void CDefineShiftDlg::OnSelchangeLine() 
{
	UpdateData (FALSE);
	ASSERT (GetDlgItem (BTN_APPLY));
	GetDlgItem (BTN_APPLY)->EnableWindow (FALSE);
}

void CDefineShiftDlg::OnApply() 
{
	if (UpdateData (TRUE)) {
		CShiftCode s = GetCurSelShiftCode ();

		VERIFY (Update (s));
		ASSERT (GetDlgItem (BTN_APPLY));
		GetDlgItem (BTN_APPLY)->EnableWindow (FALSE);
	}
}

bool CDefineShiftDlg::Update (const CShiftCode & code)
{
	LOCK (::csShift);

	SETTINGSSTRUCT s;
	COdbcDatabase & db = GetDB ();
	bool bResult = false;

	s.m_lLineID = GetMasterLineID (code.m_lLineID);
	s.m_strKey	= ListGlobals::Keys::m_strShiftcode;
	s.m_strData = code.ToString (CVersion ());

	if (!(bResult = UpdateSettingsRecord (db, s)))
		bResult = AddSettingsRecord (db, s);

	if (bResult) {
		LoadShiftcodes (GetDB (), CVersion ());
		m_vCodes.RemoveAll ();
		
		for (int i = 0; i < ::vShiftcodes.GetSize (); i++) 
			m_vCodes.Add (::vShiftcodes [i]);
	}

	return bResult;
}

void CDefineShiftDlg::OnOK()
{
	if (UpdateData (TRUE)) {
		CShiftCode code = GetCurSelShiftCode ();

		VERIFY (Update (code));
		m_lLineID = code.m_lLineID;

		FoxjetCommon::CEliteDlg::OnOK ();
	}
}

void CDefineShiftDlg::EnableApply ()
{
	ASSERT (GetDlgItem (BTN_APPLY));
	GetDlgItem (BTN_APPLY)->EnableWindow (TRUE);
}

void CDefineShiftDlg::OnDatetimechangeShift(NMHDR* pNMHDR, LRESULT* pResult) 
{
	EnableApply ();

	if (pResult) 
		* pResult = 0;
}
