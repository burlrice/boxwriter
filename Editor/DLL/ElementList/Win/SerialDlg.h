#if !defined(AFX_SERIALDLG_H__112B9968_6D03_4FB1_A6A8_A47CD87D560B__INCLUDED_)
#define AFX_SERIALDLG_H__112B9968_6D03_4FB1_A6A8_A47CD87D560B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SerialDlg.h : header file
//

#include "resource.h"
#include "BaseTextDlg.h"
#include "SerialElement.h"

/////////////////////////////////////////////////////////////////////////////
// CSerialDlg dialog

class CSerialDlg : public CBaseTextDlg
{
// Construction
public:
	CSerialDlg (const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   

// Dialog Data
	//{{AFX_DATA(CSerialDlg)
	int m_nIndex;
	int	m_nLength;
	int m_nTableIndex;
	//}}AFX_DATA

	CString m_strDefault;

	virtual UINT GetDefCtrlID () const { return TXT_INDEX; }

	void Get (const FoxjetElements::CSerialElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);
	void Set (FoxjetElements::CSerialElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerialDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual BOOL OnInitDialog();
	virtual int BuildElement ();
	virtual void OnOK ();
	const FoxjetElements::CSerialElement & GetElement () const;
	FoxjetElements::CSerialElement & GetElement ();

	// Generated message map functions
	//{{AFX_MSG(CSerialDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERIALDLG_H__112B9968_6D03_4FB1_A6A8_A47CD87D560B__INCLUDED_)
