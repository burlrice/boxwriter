
//#ifdef DATAMATRIXENCODEDLL_EXPORTS
//#define DATAMATRIXENCODEDLL_API __declspec(dllexport)
//#else
//#define DATAMATRIXENCODEDLL_API __declspec(dllimport)
//#endif

#include <windows.h>

//constant for sizePattern of datamatrix image.
#define SQUAREAUTOSIZE		-1
#define RECTANGLEAUTOSIZE   -2

#define MAPSIZE10X10    0
#define MAPSIZE12X12    1
#define MAPSIZE14X14    2
#define MAPSIZE16X16    3
#define MAPSIZE18X18    4
#define MAPSIZE20X20    5
#define MAPSIZE22X22    6
#define MAPSIZE24X24    7
#define MAPSIZE26X26    8
#define MAPSIZE32X32    9
#define MAPSIZE36X36    10
#define MAPSIZE40X40    11
#define MAPSIZE44X44    12
#define MAPSIZE48X48    13
#define MAPSIZE52X52    14
#define MAPSIZE64X64    15
#define MAPSIZE72X72    16
#define MAPSIZE80X80    17
#define MAPSIZE88X88    18
#define MAPSIZE96X96    19
#define MAPSIZE104X104  20
#define MAPSIZE120X120  21
#define MAPSIZE132X132  22
#define MAPSIZE144X144  23
#define MAPSIZE8X18     24
#define MAPSIZE8X32     25
#define MAPSIZE12X26    26
#define MAPSIZE12X36    27
#define MAPSIZE16X36    28
#define MAPSIZE16X48    29

typedef struct _tagDATAMATRIXCONTEXT
{
	char code[3300];
	int size;
	int encoding;
	int dotPixel;
	int sizePattern;
	int nMargin;
	COLORREF clBackGround;
	COLORREF clForeGround;
} _DATAMATRIXCONTEXT,*_LPDATAMATRIXCONTEXT;

void __stdcall _InitDataMatrixContext(_LPDATAMATRIXCONTEXT pDmCtx);
BOOL  __stdcall _DataMatrixEncode2File(_LPDATAMATRIXCONTEXT pDmCtx, LPCTSTR lpImageFile);
HBITMAP __stdcall _DataMatrixEncode2Bitmap(_LPDATAMATRIXCONTEXT pDmCtx);
BOOL __stdcall _FreeDataMatrixContext();
BOOL __stdcall _DataMatrixEncode2SMSImage(char *pText, LPCTSTR pFile,int nWidth,int nHeight);

