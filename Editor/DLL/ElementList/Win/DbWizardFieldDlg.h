#pragma once

#include "DbWizard.h"
#include "ListCtrlImp.h"
#include "CopyArray.h"
#include "Types.h"

// CDbWizardFieldDlg

namespace FoxjetElements
{
	class ELEMENT_API CFieldItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CFieldItem (const CString & strName = _T (""), int nSQLType = 0, int nElementType = MAKEWORD (TEXT, 0));
		virtual CString GetDispText (int nColumn) const;

		CString m_strName;
		int m_nSQLType;
		CString m_strSQLType;
		int m_nClassID;
		int m_nSymbology;
	};

	class ELEMENT_API CDbWizardFieldDlg : public CDbWizardBasePage
	{
		DECLARE_DYNAMIC(CDbWizardFieldDlg)

		class CTypeListCtrl : public ItiLibrary::ListCtrlImp::CListCtrlImp
		{
			void OnClick (LPNMITEMACTIVATE lpnma);

			static LRESULT CALLBACK WindowProc (HWND hWnd, UINT nMsg, WPARAM wParam, LPARAM lParam);

		public:
			void Create (UINT nListCtrlID, CArray <ItiLibrary::ListCtrlImp::CColumn, ItiLibrary::ListCtrlImp::CColumn> & vCols, CWnd * pParent, const CString & strRegSection)
			{
				ItiLibrary::ListCtrlImp::CListCtrlImp::Create (nListCtrlID, vCols, pParent, strRegSection, CTypeListCtrl::WindowProc);
			}
		};

	public:
		CDbWizardFieldDlg(CDbWizardDlg * pParent);
		virtual ~CDbWizardFieldDlg();

		CString GetCurSelTable () const;
		FoxjetCommon::CCopyArray <CFieldItem, CFieldItem &> GetCurSelFields () const;

		CString m_strTable;
		FoxjetCommon::CCopyArray <CFieldItem, CFieldItem &> m_vFields;

	protected:

		virtual BOOL OnInitDialog ();
		virtual BOOL OnSetActive ();
		virtual BOOL OnKillActive ();
		virtual bool CanAdvance ();

		afx_msg void OnView ();
		afx_msg void OnSelchangeTables ();
		afx_msg void OnDrawItem (int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
		afx_msg void OnMeasureItem (int nCtrl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
		afx_msg void OnElementCmd (UINT nID);

		CString m_strDSN;
		CTypeListCtrl m_lv;

		DECLARE_MESSAGE_MAP()
	};
};

