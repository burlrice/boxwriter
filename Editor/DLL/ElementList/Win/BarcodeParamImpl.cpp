#include "stdafx.h"
#include "list.h"
#include "BarcodeParams.h"
#include "BarcodeElement.h"
#include "Utils.h"
#include "Debug.h"
#include "Database.h"
#include "Extern.h"
#include "BarcodeDlg.h"
#include "TemplExt.h"
#include "Edit.h"
#include "AppVer.h"
#include "Parse.h"

using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace FoxjetDatabase;
using namespace ListGlobals;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

////////////////////////////////////////////////////////////////////////////////

static ULONG GetLineMapping (FoxjetDatabase::COdbcDatabase & db, ULONG lLineID) // sw0867
{ 
	return GetMasterLineID (lLineID);
	/* TODO: rem
	ULONG lResult = -1;

	if (!::vLineMapping.Lookup (lLineID, lResult)) {
		ASSERT (db.IsOpen ());

		if (db.IsOpen ()) {
			CArray <LINESTRUCT, LINESTRUCT &> v;
			ULONG lPrinterID = GetPrinterID (db); //GetMasterPrinterID (db);
			
			GetPrinterLines (db, lPrinterID, v); 

			if (v.GetSize ()) {
				lResult = v [0].m_lID;
				::vLineMapping.SetAt (lLineID, lResult);
				TRACEF ("map line: " + FoxjetDatabase::ToString (lLineID) + " --> " + FoxjetDatabase::ToString (lResult));
			}
		}
	}		

	ASSERT (lResult != -1);

	return lResult;
	*/
}
////////////////////////////////////////////////////////////////////////////////

class CSymbology 
{
public:
	CSymbology (int nSymbology = kSymbologyFirst, UINT nResID = -1, LPCTSTR lpszName = _T (""));
	CSymbology (const CSymbology & rhs);
	CSymbology & operator = (const CSymbology & rhs);
	virtual ~CSymbology ();

	int m_nSymbology;
	int m_nResID;
	CString m_str;
	CString m_strEnglishName;
};

////////////////////////////////////////////////////////////////////////////////

CSymbology::CSymbology (int nSymbology, UINT nResID, LPCTSTR lpszName)
:	m_nSymbology (nSymbology),
	m_nResID (nResID),
	m_strEnglishName (lpszName)
{
}

CSymbology::CSymbology (const CSymbology & rhs)
:	m_nSymbology (rhs.m_nSymbology),
	m_nResID (rhs.m_nResID),
	m_str (rhs.m_str),
	m_strEnglishName (rhs.m_strEnglishName)
{
}

CSymbology & CSymbology::operator = (const CSymbology & rhs)
{
	if (this != &rhs) {
		m_nSymbology		= rhs.m_nSymbology;
		m_nResID			= rhs.m_nResID;
		m_str				= rhs.m_str;
		m_strEnglishName	= rhs.m_strEnglishName;
	}

	return * this;
}

CSymbology::~CSymbology ()
{
}

////////////////////////////////////////////////////////////////////////////////

typedef enum 
{
	C39R				= kSymbologyCode39Regular,
	C39F				= kSymbologyCode39Full,
	C39S				= kSymbologyCode39Standard,
	I2o5				= kSymbologyInter2of5,
	Codabar				= kSymbologyCodabar,
	MSI					= kSymbologyMSI,
	C93    				= kSymbologyCode93,
	UPCA				= kSymbologyUPCA,
	UPCE				= kSymbologyUPCE,
	PostNet				= kSymbologyPostNet,
	EANJAN8				= kSymbologyEANJAN_8,
	EANJAN13			= kSymbologyEANJAN_13,
	C128				= kSymbologyCode128,
	PDF417				= kSymbologyPDF417,
	MaxiCode			= kSymbologyMaxiCode,
	DataMatrixEcc200	= kSymbologyDataMatrixEcc200,
	DataMatrixGs1		= kSymbologyDataMatrixGs1,
	LOGMARS				= kSymbologyLOGMARS,
	EAN128				= kSymbologyEAN128,
	GTIN14				= kSymbologyGTIN14,
	QR					= kSymbologyQR,

} BCTYPE;

static CSymbology symbology [] = 
{
	CSymbology (C39R,				IDS_C39REGULAR,			_T ("Code 39 standard set")),
	CSymbology (C39F,				IDS_C39FULL,			_T ("Code 39 Full ASCII set")),
	CSymbology (I2o5,				IDS_I2O5,				_T ("I2of5")),
	CSymbology (Codabar,			IDS_CODABAR,			_T ("Codabar")),
	CSymbology (MSI,				IDS_MSI,				_T ("MSI")),		
	CSymbology (C93,   				IDS_C93REGULAR,			_T ("Code 93")),	
	CSymbology (UPCA,				IDS_UPCA,				_T ("GTIN-12 (UPC-A)")),		
	CSymbology (UPCE,				IDS_UPCE,				_T ("GTIN-12 (UPC-E)")),		
	CSymbology (PostNet,			IDS_POSTNET,			_T ("PostNet")),	
	CSymbology (EANJAN8,			IDS_EANJAN8,			_T ("GTIN-8 (EAN-8)")),	
	CSymbology (EANJAN13,			IDS_EANJAN13,			_T ("GTIN-13 (EAN-13)")),	
	CSymbology (C128,				IDS_C128,				_T ("C128")),		
	CSymbology (C39S,				IDS_C39,				_T ("Code 39 Standard")),		
	CSymbology (PDF417,				IDS_PDF417,				_T ("PDF417")),	
	CSymbology (MaxiCode,			IDS_MAXICODE,			_T ("MaxiCode")),	
	CSymbology (DataMatrixEcc200,	IDS_DATAMATRIXECC200,	_T ("Datamatrix ECC200")),
	CSymbology (DataMatrixGs1,		IDS_DATAMATRIXGS1,		_T ("GS1 DataMatrix")),
	CSymbology (LOGMARS,			IDS_LOGMARS,			_T ("LOGMARS")),	
	CSymbology (EAN128,				IDS_EAN128,				_T ("GS1-128")),
	CSymbology (GTIN14,				IDS_GTIN14,				_T ("GTIN-14 (ITF-14)")),
	CSymbology (QR,					IDS_QR,					_T ("QR code")),
};

const LPCTSTR CBarcodeParams::m_lpszDefFont = _T ("MK BARCODE"); 
const LPCTSTR CBarcodeParams::m_lpszUPCFont = _T ("MK Courier");;
const int CBarcodeParams::m_nUPCFontWidth = 25;

LPCTSTR lpszDefFont = CBarcodeParams::m_lpszDefFont;
LPCTSTR lpszUPCFont = CBarcodeParams::m_lpszUPCFont;
int nUPCFontWidth	= CBarcodeParams::m_nUPCFontWidth;

static const int nResolutions = 3;//GetResolutions ().size ();
static const int nSymbologies = kSymbologyLast - kSymbologyFirst + 1;
static const int nParamTypes = 4;
static const int nParamsPerSymbology = 8;//3;

static const CBarcodeParams defs [][::nParamsPerSymbology] = 
{
	{
		CBarcodeParams (C39F,		40,	128,	20,	0,	100, false),
		CBarcodeParams (C39F,		40,	128,	20,	0,	70, false),
		CBarcodeParams (C39F,		40,	128,	20,	0,	70, false),
		CBarcodeParams (C39F), // custom 1
		CBarcodeParams (C39F), // custom 2
		CBarcodeParams (C39F), // custom 3
		CBarcodeParams (C39F), // custom 4
		CBarcodeParams (C39F), // custom 5
	}, {
		// (150dpi, doubled pulsed, Scran true ink)
		// standard					ns ws	nb wb	vbb	 hbb  qz	chksum
		CI2o5Params (100, 118,		6,	16,	5,	14,	190, 190, 450,	true,	0, lpszDefFont, 13, 0, false),
		CI2o5Params (80,  111,		5,	13,	4,	11,	130, 130, 360,	true,	0, lpszDefFont, 13, 0, false),
		CI2o5Params (67,  108,		4,	11,	3,	10,	123, 123, 288,	true,	0, lpszDefFont, 13, 0, false),
		CI2o5Params (), // custom 1
		CI2o5Params (), // custom 2
		CI2o5Params (), // custom 3
		CI2o5Params (), // custom 4
		CI2o5Params (), // custom 5

		// NOTE: see Get32ChannelI2o5 for others

/*
		// allen canning 426 dpi	ns ws	nb wb		
		CI2o5Params (100, 118,		12, 27, 5, 17,  40,  40,  500,	0, lpszDefFont, 21, 0, false),
		CI2o5Params (80,  118,		9, 21,  4, 15,  40,  40,  400,	0, lpszDefFont, 19, 0, false),
		CI2o5Params (60,  118,		4,  9,  3,	8,  84,  84,  257,	0, lpszDefFont, 10, 0, false),
*/



/*
		CBarcodeParams (I2o5,		40,	137,23,	1,	100,190,190, 50, lpszDefFont, 21, false),
		CBarcodeParams (I2o5,		39,	107,24,	1,	80,	152,152, 90, lpszDefFont, 19, false),
		CBarcodeParams (I2o5,		40,	256,20,	1,	50,	0,	0,	 0,	 lpszDefFont, 19, false),
*/
/*
		CI2o5Params (100, 118,		12, 27, 5, 17,  40,  40,  500,	0, lpszDefFont, 21, 0, false),
		CI2o5Params (80,   97,		9, 21,  4, 15,  40,  40,  400,	0, lpszDefFont, 19, 0, false),
		CI2o5Params (60,   83,		4,  9,  3,	8,  84,  84,  257,	0, lpszDefFont, 10, 0, false),
*/
/* TODO: rem: WASPI2O5 // 
#else
		CBarcodeParams (I2o5,		40,	118,25,	1,	100,190,190, 55, lpszDefFont, 21, false),
		CBarcodeParams (I2o5,		39,	97,25,	1,	80,	135,135, 70, lpszDefFont, 19, false),
		CBarcodeParams (I2o5,		40,	83,25,	1,	67,	123,123, 95, lpszDefFont, 10, false),
#endif
*/
	}, {														
		CBarcodeParams (Codabar,	40,	128,20,	0,	100,0,  0,   0,	 lpszDefFont, 21, false),
		CBarcodeParams (Codabar,	40,	128,20,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (Codabar,	40,	128,20,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (Codabar), // custom 1
		CBarcodeParams (Codabar), // custom 2
		CBarcodeParams (Codabar), // custom 3
		CBarcodeParams (Codabar), // custom 4
		CBarcodeParams (Codabar), // custom 5
	}, {												
		CBarcodeParams (MSI,		40,	128,20,	0,	100,0,  0,   0,	 lpszDefFont, 21, false),
		CBarcodeParams (MSI,		10,	128,20,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (MSI,		10,	128,20,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (MSI), // custom 1
		CBarcodeParams (MSI), // custom 2
		CBarcodeParams (MSI), // custom 3
		CBarcodeParams (MSI), // custom 4
		CBarcodeParams (MSI), // custom 5
	}, {													
		CBarcodeParams (C93,    	40,	128,1,	0,	100,0,  0,   0,	 lpszDefFont, 21, false),
		CBarcodeParams (C93,    	40,	128,1,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (C93),//    	40,	128,1,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (C93), // custom 1
		CBarcodeParams (C93), // custom 2
		CBarcodeParams (C93), // custom 3
		CBarcodeParams (C93), // custom 4
		CBarcodeParams (C93), // custom 5
	}, {
/*
		CBarcodeParams (UPCA,		80,	256,1,	0,	100,0,  0,   0,	 lpszDefFont, 21, false),
		CBarcodeParams (UPCA,		80,	256,1,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (UPCA,		80,	256,1,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
*/
		CUPCAParams (200, 128, 6, 10, 13, 17, 3, 7, 10, 14, nUPCFontWidth, lpszUPCFont, 8, 0, false),
		CUPCAParams (160, 128, 5, 8 , 10, 14, 2, 5, 7, 11,	nUPCFontWidth, lpszUPCFont, 8, 0, false),
		CUPCAParams (100, 128, 5, 8 , 10, 14, 2, 5, 7, 11,	nUPCFontWidth, lpszUPCFont, 8, 0, false),
		CUPCAParams (), // custom 1
		CUPCAParams (), // custom 2
		CUPCAParams (), // custom 3
		CUPCAParams (), // custom 4
		CUPCAParams (), // custom 5
	}, {											
		CBarcodeParams (UPCE,		80,	128,1,	0,	100,0,  0,   0,	 lpszDefFont, 21, false),
		CBarcodeParams (UPCE,		80,	128,1,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (UPCE),//,	80,	128,1,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (UPCE), // custom 1
		CBarcodeParams (UPCE), // custom 2
		CBarcodeParams (UPCE), // custom 3
		CBarcodeParams (UPCE), // custom 4
		CBarcodeParams (UPCE), // custom 5
	}, {											
		CBarcodeParams (EANJAN8,	80,	128,1,	0,	100,0,  0,   0,	 lpszUPCFont, 21, false),
		CBarcodeParams (EANJAN8,	80,	128,1,	0,	70,	0,  0,   0,	 lpszUPCFont, 19, false),
		CBarcodeParams (EANJAN8),//,80,	128,1,	0,	70,	0,  0,   0,	 lpszUPCFont, 19, false),
		CBarcodeParams (EANJAN8), // custom 1
		CBarcodeParams (EANJAN8), // custom 2
		CBarcodeParams (EANJAN8), // custom 3
		CBarcodeParams (EANJAN8), // custom 4
		CBarcodeParams (EANJAN8), // custom 5
	}, {											
		CBarcodeParams (EANJAN13,	80,	128,1,	0,	100,0,  0,   0,	 lpszUPCFont, 21, false),
		CBarcodeParams (EANJAN13,	80,	128,1,	0,	70,	0,  0,   0,	 lpszUPCFont, 19, false),
		CBarcodeParams (EANJAN13),//,	80,	128,1,	0,	70,	0,  0,   0,	 lpszUPCFont, 19, false),
		CBarcodeParams (EANJAN13), // custom 1
		CBarcodeParams (EANJAN13), // custom 2
		CBarcodeParams (EANJAN13), // custom 3
		CBarcodeParams (EANJAN13), // custom 4
		CBarcodeParams (EANJAN13), // custom 5
	}, {											
		CBarcodeParams (PostNet,	40,	128,1,	0,	100,0,  0,   0,	 lpszDefFont, 21, false),
		CBarcodeParams (PostNet,	40,	128,1,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (PostNet),//,40,	128,1,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (PostNet), // custom 1
		CBarcodeParams (PostNet), // custom 2
		CBarcodeParams (PostNet), // custom 3
		CBarcodeParams (PostNet), // custom 4
		CBarcodeParams (PostNet), // custom 5
	}, {												
		CC128Params (C128, 100, 128, 5,  9, 13, 17, 4, 8, 12, 16, 0, lpszDefFont, 21, 0, false),
		CC128Params (C128, 70,  128, 4,  7, 10, 13, 3, 6,  9, 12, 0, lpszDefFont, 19, 0, false),
		CC128Params (C128, 100, 128, 6, 10, 14, 18, 3, 7, 11, 15, 0, lpszDefFont, 21, 0, true), //(70,  128, 4,  7, 10, 13, 3, 6,  9, 12,	0, lpszDefFont, 19, 0, false),
		CC128Params (C128, 100, 128, 6, 10, 14, 18, 3, 7, 11, 15, 0, lpszDefFont, 21, 0, true), // custom 1
		CC128Params (C128, 100, 128, 6, 10, 14, 18, 3, 7, 11, 15, 0, lpszDefFont, 21, 0, true), // custom 2
		CC128Params (C128, 100, 128, 6, 10, 14, 18, 3, 7, 11, 15, 0, lpszDefFont, 21, 0, true), // custom 3
		CC128Params (C128, 100, 128, 6, 10, 14, 18, 3, 7, 11, 15, 0, lpszDefFont, 21, 0, true), // custom 4
		CC128Params (C128, 100, 128, 6, 10, 14, 18, 3, 7, 11, 15, 0, lpszDefFont, 21, 0, true), // custom 5
	}, {										
		CBarcodeParams (C39S,		40,	128,1,	0,	100,0,  0,   0,	 lpszDefFont, 21, false),
		CBarcodeParams (C39S,		40,	128,1,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (C39S),//,	40,	128,1,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (C39S), // custom 1
		CBarcodeParams (C39S), // custom 2
		CBarcodeParams (C39S), // custom 3
		CBarcodeParams (C39S), // custom 4
		CBarcodeParams (C39S), // custom 5
	}, {											
		CBarcodeParams (PDF417,		40,	128,1,	0,	100,0,  0,   0,	 lpszDefFont, 21, false),
		CBarcodeParams (PDF417,		40,	128,1,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (PDF417),//,	40,	128,1,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (PDF417), // custom 1
		CBarcodeParams (PDF417), // custom 2
		CBarcodeParams (PDF417), // custom 3
		CBarcodeParams (PDF417), // custom 4
		CBarcodeParams (PDF417), // custom 5
	}, {												
		CDataMatrixParams (DataMatrixEcc200, 100, 14, 6, lpszDefFont, 21, 0, false),
		CDataMatrixParams (DataMatrixEcc200, 80,  7, 3,  lpszDefFont, 19, 0, false),
		CDataMatrixParams (DataMatrixEcc200, 70,  4, 2,  lpszDefFont, 19, 0, false),
		CDataMatrixParams (DataMatrixEcc200), // custom 1
		CDataMatrixParams (DataMatrixEcc200), // custom 2
		CDataMatrixParams (DataMatrixEcc200), // custom 3
		CDataMatrixParams (DataMatrixEcc200), // custom 4
		CDataMatrixParams (DataMatrixEcc200), // custom 5
	}, {									
		CDataMatrixParams (DataMatrixGs1, 100, 14, 6,  lpszDefFont, 21, 0, false),
		CDataMatrixParams (DataMatrixGs1, 80,  7, 3,   lpszDefFont, 19, 0, false),
		CDataMatrixParams (DataMatrixGs1, 70,  4, 2,   lpszDefFont, 19, 0, false),
		CDataMatrixParams (DataMatrixGs1), // custom 1
		CDataMatrixParams (DataMatrixGs1), // custom 2
		CDataMatrixParams (DataMatrixGs1), // custom 3
		CDataMatrixParams (DataMatrixGs1), // custom 4
		CDataMatrixParams (DataMatrixGs1), // custom 5
	}, {									
		CBarcodeParams (LOGMARS,	40,	128,1,	0,	100,0,  0,   0,	 lpszDefFont, 21, false),
		CBarcodeParams (LOGMARS,	40,	128,1,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (LOGMARS),//,40,	128,1,	0,	70,	0,  0,   0,	 lpszDefFont, 19, false),
		CBarcodeParams (LOGMARS), // custom 1
		CBarcodeParams (LOGMARS), // custom 2
		CBarcodeParams (LOGMARS), // custom 3
		CBarcodeParams (LOGMARS), // custom 4
		CBarcodeParams (LOGMARS), // custom 5
	},
	{ 
		CC128Params (EAN128, 100, 128, 5,  9, 13, 17, 4, 8, 12, 16, 0, lpszDefFont, 21, 0, false),
		CC128Params (EAN128, 70,  128, 4,  7, 10, 13, 3, 6,  9, 12,	0, lpszDefFont, 19, 0, false),
		CC128Params (EAN128, 100, 128, 6, 10, 14, 18, 3, 7, 11, 15, 0, lpszDefFont, 21, 0, true), //(70,  128, 4,  7, 10, 13, 3, 6,  9, 12,	0, lpszDefFont, 19, 0, false),
		CC128Params (EAN128, 100, 128, 6, 10, 14, 18, 3, 7, 11, 15, 0, lpszDefFont, 21, 0, true), // custom 1
		CC128Params (EAN128, 100, 128, 6, 10, 14, 18, 3, 7, 11, 15, 0, lpszDefFont, 21, 0, true), // custom 2
		CC128Params (EAN128, 100, 128, 6, 10, 14, 18, 3, 7, 11, 15, 0, lpszDefFont, 21, 0, true), // custom 3
		CC128Params (EAN128, 100, 128, 6, 10, 14, 18, 3, 7, 11, 15, 0, lpszDefFont, 21, 0, true), // custom 4
		CC128Params (EAN128, 100, 128, 6, 10, 14, 18, 3, 7, 11, 15, 0, lpszDefFont, 21, 0, true), // custom 5
	},
	{
		CGTIN14Params (100, 118,		6,	16,	5,	14,	190, 190, 450,	true,	0, lpszDefFont, 13, 0, false),
		CGTIN14Params (80,  111,		5,	13,	4,	11,	130, 130, 360,	true,	0, lpszDefFont, 13, 0, false),
		CGTIN14Params (67,  108,		4,	11,	3,	10,	123, 123, 288,	true,	0, lpszDefFont, 13, 0, false),
		CGTIN14Params (), // custom 1
		CGTIN14Params (), // custom 2
		CGTIN14Params (), // custom 3
		CGTIN14Params (), // custom 4
		CGTIN14Params (), // custom 5
	},
	{									
		CQrParams (100, 14, 6,  false),
		CQrParams (80,  7, 3,   false),
		CQrParams (70,  4, 2,   false),
		CQrParams (), // custom 1
		CQrParams (), // custom 2
		CQrParams (), // custom 3
		CQrParams (), // custom 4
		CQrParams (), // custom 5
	},									
};

// note that the order of symbologies must match 'defs' in order for GetIndex to work
struct 
{
	int		m_nSymbology;
	LPCTSTR m_lpszData;
} 
const static defaultData [] = 
{
	{	C39R,				_T ("0123401234")		},
	{	C39F,				_T ("0123401234")		},
	{	I2o5,				_T ("0123401234")		},
	{ 	Codabar,			_T ("0123401234")		},
	{ 	MSI,				_T ("0123401234")		},
	{ 	C93,				_T ("0123401234")		},
	{ 	UPCA,				_T ("01234567890")		},
	{ 	UPCE,				_T ("01234012340")		},
	{ 	PostNet,			_T ("012340123401")		 },
	{ 	EANJAN13,			_T ("012345678901")		},
	{ 	EANJAN8,			_T ("0123456")			},
	{ 	C128,				_T ("0123401234")		},
	{ 	C39S,				_T ("0123401234")		},
	{ 	PDF417,				_T ("0123401234")		},
	{ 	MaxiCode,			_T ("0123401234")		},
	{ 	DataMatrixEcc200,	_T ("0123401234")		},
	{ 	DataMatrixGs1,		_T ("0123401234")		},
	{ 	LOGMARS,			_T ("0123401234")		},
	{ 	EAN128,				_T ("0123401234")		},
	{	GTIN14,				_T ("01234012340123")	},
	{	QR,					_T ("0123401234")		},
};


////////////////////////////////////////////////////////////////////////////////

static int GetHeadTypeIndex (HEADTYPE type);
static CI2o5Params Get32ChannelI2o5 (int nIndex);

class CBcParamArray
{
public:

	CBcParamArray (ULONG lLineID);
	virtual ~CBcParamArray ();

	ULONG GetLineID () { return m_lLineID; }

	CBarcodeParams * GetParam (int nSymbology, HEADTYPE type, int nResolution, int nIndex);

private:
	ULONG			m_lLineID;
	CBarcodeParams	m_array [::nSymbologies][::nParamTypes][::nResolutions][::nParamsPerSymbology];

	CBcParamArray (const CBcParamArray & rhs) { ASSERT (0); }
	CBcParamArray & operator = (const CBcParamArray & rhs) { ASSERT (0); return * this; }

};

#pragma data_seg( "BarcodeParams" )
	static CArray <CBcParamArray *, CBcParamArray *> vBarcodeParams;
	static DIAGNOSTIC_CRITICAL_SECTION (csBarcodeParams);
#pragma

CBcParamArray::CBcParamArray(ULONG lLineID)
:	m_lLineID (lLineID)
{
	std::vector <int> vRes = GetResolutions ();

	for (int nSymb = kSymbologyFirst; nSymb <= kSymbologyLast; nSymb++) {
		for (int nHeadIndex = 0; nHeadIndex < ::nParamTypes; nHeadIndex++) {
			for (int nIndex = 0; nIndex < ::nParamsPerSymbology; nIndex++) {
				for (int i = 0; i < vRes.size (); i++) {
					HEADTYPE type = nHeadIndex == 0 ? HEADTYPE_FIRST : GRAPHICS_768_256;
					CBarcodeParams * p = GetParam (nSymb, type, vRes [i], nIndex);

					ASSERT (p);

					if (CBarcodeElement::IsValidSymbology (nSymb)) 
						* p = GetDefParam (nSymb, nIndex, type, vRes [i], GetDB ());

					p->SetID (nIndex);
					p->ClipTo (type);
					p->SetLineID (lLineID);
				}
			}
		}
	}
}

CBcParamArray::~CBcParamArray ()
{
//	TRACEF ("~CBcParamArray ()");
}

CBarcodeParams * CBcParamArray::GetParam (int nSymbology, HEADTYPE type, int nResolution, int nIndex)
{
	std::vector <int> vRes = GetResolutions ();
	int nRes = Find (vRes, nResolution);

	if (nRes == -1)
		nRes = 0;

	ASSERT (nSymbology >= kSymbologyFirst && nSymbology <= kSymbologyLast);
	ASSERT (nIndex >= 0 && nIndex < GetParamsPerSymbology ());

	int nHeadIndex = GetHeadTypeIndex (type);

	ASSERT (nSymbology	>= 0 && nSymbology	< ::nSymbologies);
	ASSERT (nHeadIndex	>= 0 && nHeadIndex	< ::nParamTypes);
	ASSERT (nRes		>= 0 && nRes		< ::nResolutions);
	ASSERT (nIndex		>= 0 && nIndex		< ::nParamsPerSymbology);

	return &m_array [nSymbology][nHeadIndex][nRes][nIndex];
}

static CBcParamArray * GetLineParams (ULONG lLineID, FoxjetDatabase::COdbcDatabase & db)
{
	//DBMANAGETHREADSTATE (db);
	LOCK (::csBarcodeParams);

/*
	{
		static ULONG lLastAddr = -1;
		CString str; str.Format ("DBMANAGETHREADSTATE: 0x%p [0x%p]", &db.m_mutex, &db); TRACEF (str); 
		if (lLastAddr != (ULONG)&db.m_mutex) TRACEF ("DBMANAGETHREADSTATE: ***** lLastAddr != &db.m_mutex *****");
		lLastAddr = (ULONG)&db.m_mutex;
	}
*/

	for (int i = 0; i < ::vBarcodeParams.GetSize (); i++)  
		if (CBcParamArray * p = ::vBarcodeParams [i]) 
			if (p->GetLineID () == lLineID || lLineID == -1) 
				return p;

	// not found, create it now
	CBcParamArray * p = new CBcParamArray (lLineID);
	::vBarcodeParams.Add (p);

	return p;
}

static int GetHeadTypeIndex (HEADTYPE type)
{
	switch (type) {
	case GRAPHICS_384_128:
	case GRAPHICS_768_256:	
	case GRAPHICS_768_256_L310:		return 1;
	case UR2:						return 2;
	case UR4:						return 3;
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////


CString ELEMENT_API FoxjetElements::GetDefaultData (int nSymbology, const CBarcodeParams & params)
{
	CString str;

	for (int i = 0; i < (sizeof (::defaultData) / sizeof (::defaultData [0])); i++) {
		if (::defaultData [i].m_nSymbology == nSymbology) {
			str = ::defaultData [i].m_lpszData;
			break;
		}
	}

	if (nSymbology == kSymbologyUPCA)
		str = CString ('0', !params.GetCheckSum () ? 12 : 11);

	return str;
}

void ELEMENT_API FoxjetElements::LoadBarcodeParams (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	LOCK (::csBarcodeParams);
	//DBMANAGETHREADSTATE (db);

	CArray <LINESTRUCT, LINESTRUCT &> vLines; 
	CStringArray vstrTypes;

	DeleteBarcodeParams (db);
	GetLineRecords (vLines);

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		ULONG lLineID = vLines [nLine].m_lID;
		
		lLineID = GetLineMapping (db, lLineID);

		if (CBcParamArray * pParams = GetLineParams (lLineID, db)) {
			vstrTypes.RemoveAll ();

			for (int nType = HEADTYPE_FIRST; nType <= HEADTYPE_LAST; nType++) {
				HEADTYPE type = (HEADTYPE)nType;
				CString strType = GetHeadTypeString (type, db);

				if (Find (vstrTypes, strType) == -1) {
					vstrTypes.Add (strType);

					for (int nSymbology = kSymbologyFirst; nSymbology <= kSymbologyLast; nSymbology++) {
						if (CBarcodeElement::IsValidSymbology (nSymbology)) {
							std::vector <int> vRes = GetResolutions ();

							for (int i = 0; i < vRes.size (); i++) {
								SETTINGSSTRUCT s;
								CStringArray vParams;

								s.m_lLineID = lLineID;
								s.m_strKey = GetSettingsKey ((HEADTYPE)nType, vRes [i], nSymbology, db);
								TRACEF (s.m_strKey);

								bool bGetSettingsRecord = GetSettingsRecord (db, lLineID, s.m_strKey, s); 

								if (!bGetSettingsRecord && s.m_strKey.Replace (_T ("GS1-128"), _T ("EAN128")) == 1)
									bGetSettingsRecord = GetSettingsRecord (db, lLineID, s.m_strKey, s); 
							
								ParsePackets (s.m_strData, vParams);

								for (int nIndex = 0; nIndex < GetParamsPerSymbology (); nIndex++) {
									CBarcodeParams param = GetDefParam (nSymbology, nIndex, type, vRes [i], db);

									param.SetID (nIndex);

									if (nIndex < vParams.GetSize ()) {
										CString str = vParams [nIndex];
										CBarcodeParams b;

										if (b.FromString (str, CVersion ())) {
											param = b;
										}
									}

									VERIFY (SetBarcodeParam (lLineID, nSymbology, type, vRes [i], nIndex, param, db));
								}
							}
						}
					}
				}
			}
		}
	}
}


CString ELEMENT_API FoxjetElements::GetHeadTypeString (HEADTYPE type, FoxjetDatabase::COdbcDatabase & db)
{
	//DBMANAGETHREADSTATE (db);

	int nID = IDS_DEFAULTHEADTYPE;

	switch (type) {
	case GRAPHICS_768_256:
	case GRAPHICS_384_128:
	case GRAPHICS_768_256_L310:		nID = IDS_GRAPHICS_768_256;		break;
	case UR2:						nID = IDS_UR2;					break;
	case UR4:						nID = IDS_UR4;					break;
	}

	return LoadString (nID);
}

CString ELEMENT_API FoxjetElements::GetSymbology (int nSymbology, FoxjetDatabase::COdbcDatabase & db)
{
	LOCK (::csBarcodeParams);//DBMANAGETHREADSTATE (db);

	for (int i = 0; i < (sizeof (::symbology) / sizeof (::symbology [0])); i++) {
		CSymbology & s = ::symbology [i];

		if (s.m_str.GetLength () == 0)
			s.m_str = LoadString (s.m_nResID);

		if (s.m_nSymbology == nSymbology)
			return s.m_str;
	}

	return _T ("");
}

int ELEMENT_API FoxjetElements::GetSymbology (const CString & strSymbology, FoxjetDatabase::COdbcDatabase & db)
{
	LOCK (::csBarcodeParams);//DBMANAGETHREADSTATE (db);

	for (int i = 0; i < (sizeof (::symbology) / sizeof (::symbology [0])); i++) {
		CSymbology & s = ::symbology [i];

		if (s.m_str.GetLength () == 0)
			s.m_str = LoadString (s.m_nResID);

		if (!::symbology [i].m_str.CompareNoCase (strSymbology))
			return ::symbology [i].m_nSymbology;
	}

	return -1;
}

////////////////////////////////////////////////////////////////////////////////

static CString GetSymbologyEnglish (int nSymbology)
{
	for (int i = 0; i < (sizeof (::symbology) / sizeof (::symbology [0])); i++) {
		CSymbology & s = ::symbology [i];

		if (s.m_nSymbology == nSymbology)
			return s.m_strEnglishName;
	}

	return _T ("");
}

static CString GetHeadTypeStringEnglish (HEADTYPE type) // for settings table only
{
	switch (type) {
	case GRAPHICS_768_256:
	case GRAPHICS_384_128:
	case GRAPHICS_768_256_L310:		return _T ("256 channel");
	case UR2:						return _T ("UR2");
	case UR4:						return _T ("UR4");
	}

	return _T ("32 channel");
}

////////////////////////////////////////////////////////////////////////////////

std::vector <int> ELEMENT_API FoxjetElements::GetResolutions ()
{
	std::vector <int> v;

	v.push_back (150);
	v.push_back (200);
	v.push_back (300);

	return v;
}

CString ELEMENT_API FoxjetElements::GetSettingsKey (HEADTYPE headType, int nResolution, int nSymbology, FoxjetDatabase::COdbcDatabase & db)
{
	//DBMANAGETHREADSTATE (db);
	
	CString strRes;
	
	if (FoxjetDatabase::IsUltraResEnabled ())
		strRes = nResolution == 150 ? _T ("") : (ToString (nResolution) + _T (" dpi, "));

	return 
		ListGlobals::Keys::m_strBarcode		+ _T (", ") + 
		GetHeadTypeStringEnglish (headType)	+ _T (", ") + 
		strRes +
		GetSymbologyEnglish (nSymbology);
}

int ELEMENT_API FoxjetElements::GetParamsPerSymbology ()
{
	return ::nParamsPerSymbology;
}

CI2o5Params Get32ChannelI2o5 (int nIndex)
{
	//								mag		height 	ns  ws 	nb 	wb	 vbb  hbb  qz 	chksum		font		 size
	static const CI2o5Params custom (100,	32,		8,	16,	4,	13,	190, 190, 450,	true,	0,	lpszDefFont, 6);
	static const CI2o5Params defs [] = 
	{
		//			mag		height 	ns  ws 	nb 	wb  vbb  hbb  qz 	chksum	font		 size
		CI2o5Params (100,	32,		8,	16,	4,	13,	190, 190, 450,	true,	0,	lpszDefFont, 6,		0, false),
		CI2o5Params (80,	32,		6,	12,	4,	10,	130, 130, 360,	true,	0,	lpszDefFont, 6,		0, false),
		CI2o5Params (67,	32,		6,	9,	3,	8,	123, 123, 288,	true,	0,	lpszDefFont, 6,		0, false),
		CI2o5Params (custom), // custom 1
		CI2o5Params (custom), // custom 2
		CI2o5Params (custom), // custom 3
		CI2o5Params (custom), // custom 4
		CI2o5Params (custom), // custom 5
	};
	CI2o5Params p = custom;

	if (nIndex >= 0 && nIndex < GetParamsPerSymbology ()) {
		p = defs [nIndex];
		p.SetStyle (p.GetStyle () | kCapStyleBold);
	}

	return p;
}

typedef struct tagBCPARAM_HEIGHT_STRUCT
{
	tagBCPARAM_HEIGHT_STRUCT (int nMag, int nBar, int nText) : m_nMag (nMag), m_nBar (nBar), m_nText (nText) { }

	int m_nMag;
	int m_nBar;
	int m_nText;
} BCPARAM_HEIGHT_STRUCT;

typedef struct tagBCPARAM_STRUCT
{
	tagBCPARAM_STRUCT (
		int nMag, int nDPI, 
		int nBar1, int nSpace1, 
		int nBar2 = 0, int nSpace2 = 0, 
		int nBar3 = 0, int nSpace3 = 0, 
		int nBar4 = 0, int nSpace4 = 0)
	{
		m_nMag = nMag;
		m_nDPI = nDPI;

		m_nBar [0] = nBar1;
		m_nBar [1] = nBar2;
		m_nBar [2] = nBar3;
		m_nBar [3] = nBar4;

		m_nSpace [0] = nSpace1;
		m_nSpace [1] = nSpace2;
		m_nSpace [2] = nSpace3;
		m_nSpace [3] = nSpace4;
	}

	int m_nMag;
	int m_nDPI;
	int m_nBar [4];
	int m_nSpace [4];
} BCPARAM_STRUCT;


static const BCPARAM_STRUCT			endBCPARAM_STRUCT (0, 0, 0, 0);
static const BCPARAM_HEIGHT_STRUCT	endBCPARAM_HEIGHT_STRUCT (0, 0, 0);

static const BCPARAM_STRUCT defI2o5 [] = 
{
	BCPARAM_STRUCT (100, 150, 5,  6,  14, 16),
	BCPARAM_STRUCT (100, 200, 7,  9,  19, 20),
	BCPARAM_STRUCT (100, 300, 10, 12, 28, 32),

	BCPARAM_STRUCT (80,  150, 4,  5,  11, 13),
	BCPARAM_STRUCT (80,  200, 6,  7,  15, 16),
	BCPARAM_STRUCT (80,  300, 8,  10, 22, 26),

	BCPARAM_STRUCT (67,  150, 3,  4,  10, 11),
	BCPARAM_STRUCT (67,  200, 5,  6,  13, 14),
	BCPARAM_STRUCT (67,  300, 6,  8,  20, 22),

	::endBCPARAM_STRUCT,
};
static const tagBCPARAM_HEIGHT_STRUCT urI2o5 [] = 
{
	BCPARAM_HEIGHT_STRUCT (100, 165, 18),	
	BCPARAM_HEIGHT_STRUCT (80,  140, 15),	
	BCPARAM_HEIGHT_STRUCT (67,  110, 13),	
	::endBCPARAM_HEIGHT_STRUCT,
};

static const BCPARAM_STRUCT defC128 [] = 
{
	BCPARAM_STRUCT (100, 150, 4,  5,  8,  9,  12, 13, 16, 17),
	BCPARAM_STRUCT (100, 200, 5,  6,  10, 11, 15, 16, 20, 21),
	BCPARAM_STRUCT (100, 300, 8,  12, 16, 22, 24, 32, 32, 42),

	BCPARAM_STRUCT (70,  150, 3,  4,  6,  7,  9,  10, 12, 13),
	BCPARAM_STRUCT (70,  200, 4,  4,  8,  8,  12, 12, 16, 16),
	BCPARAM_STRUCT (70,  300, 6,  7, 12, 14,  18, 20, 24, 26),

	::endBCPARAM_STRUCT,
};
static const tagBCPARAM_HEIGHT_STRUCT urC128 [] = 
{
	BCPARAM_HEIGHT_STRUCT (100, 130, 18),	
	BCPARAM_HEIGHT_STRUCT (70,  92,  15),	
	::endBCPARAM_HEIGHT_STRUCT,
};

static const BCPARAM_STRUCT defC39 [] = 
{
	BCPARAM_STRUCT (100, 150, 4,  4,  8,  8),
	BCPARAM_STRUCT (100, 200, 5,  5,  10, 10),
	BCPARAM_STRUCT (100, 300, 8,  8,  16, 16),

	BCPARAM_STRUCT (70,  150, 4,  4,  8,  8),
	BCPARAM_STRUCT (70,  200, 5,  5,  10, 10),
	BCPARAM_STRUCT (70,  300, 8,  8,  16, 16),

	::endBCPARAM_STRUCT,
};
static const tagBCPARAM_HEIGHT_STRUCT urC39 [] = 
{
	BCPARAM_HEIGHT_STRUCT (100, 138, 15),	
	BCPARAM_HEIGHT_STRUCT (70,  138, 15),	
	::endBCPARAM_HEIGHT_STRUCT,
};

static int GetMag (int nSymbology, int nIndex) 
{
	for (int i = 0; i < (sizeof (::defs) / sizeof (::defs [0])); i++) {
		if (::defs [i][0].GetSymbology () == nSymbology) {
			CBarcodeParams p = ::defs [i][nIndex];

			return p.GetMagPct ();
		}
	}

	ASSERT (0);
	return -1;
}

CBarcodeParams ELEMENT_API FoxjetElements::GetDefParam (int nSymbology, int nIndex, HEADTYPE type, int nResolution, FoxjetDatabase::COdbcDatabase & db)
{
	LOCK (::csBarcodeParams);//DBMANAGETHREADSTATE (db);

	std::vector <int> vRes = FoxjetElements::GetResolutions ();
	int nMag = GetMag (nSymbology, nIndex);

	ASSERT (nIndex < GetParamsPerSymbology ());

	for (int i = 0; i < (sizeof (::defs) / sizeof (::defs [0])); i++) {
		if (::defs [i][0].GetSymbology () == nSymbology) {
			CBarcodeParams p = ::defs [i][nIndex];

			if (GetHeadChannels (type) == 32) 
				if (nSymbology == I2o5)
					return Get32ChannelI2o5 (nIndex);

			if (type != GRAPHICS_768_256) {
				double dSize = ((double)p.GetSize () / 2.5);
				p.SetSize ((int)max (dSize, 6.0));
			}

			const BCPARAM_STRUCT * pWidth = NULL;
			const BCPARAM_HEIGHT_STRUCT * pUltraRes = NULL;

			switch (nSymbology) {
			case kSymbologyInter2of5:
			case kSymbologyGTIN14:		
				pWidth		= ::defI2o5;
				pUltraRes	= ::urI2o5;
				break;
			case kSymbologyCode128:
			case kSymbologyEAN128:		
				pWidth		= ::defC128;
				pUltraRes	= ::urC128;
				break;
			case kSymbologyCode39Full:
			case kSymbologyCode39Regular:
			case kSymbologyCode39Standard:
				pWidth		= ::defC39;
				pUltraRes	= ::urC39;
				break;
			}

			if (pWidth && nMag != -1) {
				for (int i = 0; i < pWidth [i].m_nDPI != 0; i++) {
					if (pWidth [i].m_nDPI == nResolution && pWidth [i].m_nMag == nMag) {
						for (int n = 0; n < 4; n++) {
							if (pWidth [i].m_nBar [n]) 
								VERIFY (p.SetBarPixels (n + 1, pWidth [i].m_nBar [n]));
						}

						for (int n = 0; n < 4; n++) {
							if (pWidth [i].m_nSpace [n])
								VERIFY (p.SetSpacePixels (n + 1, pWidth [i].m_nSpace [n]));
						}

						break;
					}
				}
			}

			if (pUltraRes && nMag != -1) {
				if (type == UR2 || type == UR4) {
					for (int i = 0; pUltraRes [i].m_nMag != 0; i++) {
						if (nMag == pUltraRes [i].m_nMag) {
							VERIFY (p.SetBarHeight (pUltraRes [i].m_nBar));
							VERIFY (p.SetSize (pUltraRes [i].m_nText));
							break;
						}
					}
				}
			}

			return p;
		}
	}

	ASSERT (0);

	return CBarcodeParams ();
}

bool ELEMENT_API FoxjetElements::SetBarcodeParam (ULONG lLineID, UINT nSymbology, 
												  HEADTYPE type, int nResolution, 
												  int nIndex, const CBarcodeParams & param, 
												  FoxjetDatabase::COdbcDatabase & db)
{
	//DBMANAGETHREADSTATE (db);
	LOCK (::csBarcodeParams);

	lLineID = GetLineMapping (db, lLineID);

/*
	{
		static ULONG lLastAddr = -1;
		CString str; str.Format ("DBMANAGETHREADSTATE: 0x%p [0x%p]", &db.m_mutex, &db); TRACEF (str); 
		if (lLastAddr != (ULONG)&db.m_mutex) TRACEF ("DBMANAGETHREADSTATE: ***** lLastAddr != &db.m_mutex *****");
		lLastAddr = (ULONG)&db.m_mutex;
	}
*/

	if (CBcParamArray * pParams = GetLineParams (lLineID, db)) {
		if (CBarcodeParams * p = pParams->GetParam (nSymbology, type, nResolution, nIndex)) {
			* p = param;

			p->ClipTo (type);
			p->SetLineID (lLineID);

			return true;
		}
	}

	return false;
}

void ELEMENT_API FoxjetElements::SaveBarcodeParams (FoxjetDatabase::COdbcDatabase & db,
													ULONG lLineID, UINT nSymbology, 
													HEADTYPE type, int nResolution)
{
	LOCK (::csBarcodeParams);//DBMANAGETHREADSTATE (db);

	SETTINGSSTRUCT s;
	
	lLineID = GetLineMapping (db, lLineID);

	if (CBcParamArray * pParams = GetLineParams (lLineID, db)) {
		CString strKey;

		s.m_strKey = GetSettingsKey (type, nResolution, nSymbology, db);
		s.m_lLineID = lLineID;
		TRACEF (s.m_strKey);

		for (int i = 0; i < GetParamsPerSymbology (); i++) {
			if (const CBarcodeParams * p = pParams->GetParam (nSymbology, type, nResolution, i)) {
				CString strParam = p->ToString (CVersion ());

				strKey += strParam;
			}
		}

		s.m_strData = strKey;

		if (!UpdateSettingsRecord (db, s))
			VERIFY (AddSettingsRecord (db, s));
	}
}

void ELEMENT_API FoxjetElements::DeleteBarcodeParams (FoxjetDatabase::COdbcDatabase & db)
{
	//DBMANAGETHREADSTATE (db);
	LOCK (::csBarcodeParams);

/*
	{
		static ULONG lLastAddr = -1;
		CString str; str.Format ("DBMANAGETHREADSTATE: 0x%p [0x%p]", &db.m_mutex, &db); TRACEF (str); 
		if (lLastAddr != (ULONG)&db.m_mutex) TRACEF ("DBMANAGETHREADSTATE: ***** lLastAddr != &db.m_mutex *****");
		lLastAddr = (ULONG)&db.m_mutex;
	}
*/

	while (::vBarcodeParams.GetSize () > 0) {
		if (CBcParamArray * p = ::vBarcodeParams [0]) 
			delete p;

		::vBarcodeParams.RemoveAt (0);
	}

	// TODO: rem //::vLineMapping.RemoveAll ();
}

void ELEMENT_API FoxjetElements::GetBarcodeParams (ULONG lLineID, int nSymbology, 
												   HEADTYPE type, int nResolution, 
												   CArray <CBarcodeParams, CBarcodeParams &> & v,
												   FoxjetDatabase::COdbcDatabase & db)
{
	LOCK (::csBarcodeParams);
	//DBMANAGETHREADSTATE (db);

	lLineID = GetLineMapping (db, lLineID);

	if (CBcParamArray * pParams = GetLineParams (lLineID, db)) {
		ASSERT (CBarcodeElement::IsValidSymbology (nSymbology));

		for (int i = 0; i < GetParamsPerSymbology (); i++) {
			const CBarcodeParams * p = pParams->GetParam (nSymbology, type, nResolution, i);

			CBarcodeParams b (* p);

			v.Add (b);
		}
	}
}

CBarcodeParams ELEMENT_API FoxjetElements::GetBarcodeParam (ULONG lLineID, ULONG lLocalParamUID, int nSymbology, 
															HEADTYPE type, int nResolution, int nIndex,
															FoxjetDatabase::COdbcDatabase & db)
{
	LOCK (::csBarcodeParams);
	//DBMANAGETHREADSTATE (db);

	CArray <CBarcodeParams, CBarcodeParams &> v;

	if (type == UR4)
		type = UR2;

	GetBarcodeParams (lLineID, nSymbology, type, nResolution, v, db);

	if (nIndex >= v.GetSize ()) {
		//ASSERT (pTaskDoc);

		return CBarcodeElement::GetLocalParam (lLocalParamUID, nSymbology);
	}

	if (v.GetSize () > nIndex) {
		#ifdef _DEBUG	
		CBarcodeParams p = v [nIndex];
		#endif

		return v [nIndex];
	}

	ASSERT (0);
	return GetDefParam (nSymbology, nIndex, type, nResolution, db);
}
