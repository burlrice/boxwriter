#if !defined(AFX_CODESDLG_H__992886D0_699B_4014_8050_F76021DF0F5D__INCLUDED_)
#define AFX_CODESDLG_H__992886D0_699B_4014_8050_F76021DF0F5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CodesDlg.h : header file
//

#include "Resource.h"
#include "Database.h"
#include "ListCtrlImp.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CCodesDlg dialog

namespace CodesDlg
{
	class CItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CItem (const CString & str, int nIndex);
		virtual ~CItem ();

		virtual CString GetDispText (int nColumn) const;
		virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;

		CString m_str;
		int m_nIndex;
	};

	class CCodes
	{
	public:
		CCodes ();
		CCodes (const CCodes & rhs);
		CCodes & operator = (const CCodes & rhs);
		virtual ~CCodes ();

		ULONG m_lLineID;
		CStringArray m_vCodes;
	};

	typedef FoxjetCommon::CCopyArray <CCodes, CCodes &> CCodesArray;

	class CCodesDlg : public FoxjetCommon::CEliteDlg
	{
	public:


	/*
	public:
	// Construction
		CCodesDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent = NULL);   // standard constructor

		ULONG m_lLineID;
	public:

	// Dialog Data
		//{{AFX_DATA(CCodesDlg)
		enum { IDD = IDD_MONTHHOURCODES };
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		CCodesArray m_vMonthcodes;
		CCodesArray m_vHourcodes;
		CCodesArray m_vDayOfWeekCodes;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CCodesDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		FoxjetDatabase::COdbcDatabase & m_db;
		ItiLibrary::CListCtrlImp m_lv [3];
	
		CCodes GetMonthCodes (ULONG lLineID);
		bool SetMonthCodes (ULONG lLineID, const CCodes & c);

		CCodes GetHourCodes (ULONG lLineID);
		bool SetHourCodes (ULONG lLineID, const CCodes & c);

		CCodes GetDayOfWeekCodes (ULONG lLineID);
		bool SetDayOfWeekCodes (ULONG lLineID, const CCodes & c);

		void OnSelchangeType(NMHDR* pNMHDR, LRESULT* pResult);
		int GetTab() const;
		void OnDblclkType(NMHDR* pNMHDR, LRESULT* pResult); 
		void OnEdit(); 
		ULONG GetCurLine ();
		void OnSelchangeLine();
		virtual void OnOK ();
		virtual void OnCancel ();

		// Generated message map functions
		//{{AFX_MSG(CCodesDlg)
		virtual BOOL OnInitDialog();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	*/
	};
}; //namespace CodesDlg

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CODESDLG_H__992886D0_699B_4014_8050_F76021DF0F5D__INCLUDED_)
