// BitmapElement.cpp: implementation of the CShapeElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <math.h>

#include "ShapeElement.h"
#include "List.h"
#include "Resource.h"
#include "ElementDefaults.h"
#include "Coord.h"
#include "Edit.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"
#include "Utils.h"
#include "TemplExt.h"
#include "Utils.h"
#include "ximage.h"
#include "Parse.h"
#include "BitmapElement.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace Color;
using namespace FoxjetElements;
using namespace FoxjetElements::ShapeElement;
using namespace FoxjetDatabase;
using namespace ListGlobals;
using namespace FoxjetCommon::ElementFields;

IMPLEMENT_DYNAMIC (CShapeElement, CBaseElement);

CShapeElement::CShapeElement(const FoxjetDatabase::HEADSTRUCT & head)
:	m_size (1000, 0),
	m_thickness (0, 5),
	m_type (SHAPETYPE_HORZ),
	m_pbmpPrinter (NULL),
	m_pbmpEditor (NULL),
	CBaseElement (head)
{
}

CShapeElement::CShapeElement (const CShapeElement & rhs)
:	m_pbmpPrinter (NULL),
	m_pbmpEditor (NULL),
	CBaseElement (rhs)
{
	Copy (rhs);
}

CShapeElement & CShapeElement::operator = (const CShapeElement & rhs)
{
	CBaseElement::operator = (rhs);

	if (this != &rhs) {
		Copy (rhs);
	}

	return * this;
}

CShapeElement::~CShapeElement()
{
	Free ();
}

bool CShapeElement::Copy (const CShapeElement & rhs)
{
	bool bResult = true;

	m_size		= rhs.m_size;
	m_thickness = rhs.m_thickness;
	m_type		= rhs.m_type;

	return bResult;
}

void CShapeElement::Free ()
{
	if (m_pbmpPrinter) {
		delete m_pbmpPrinter;
		m_pbmpPrinter = NULL;
	}

	if (m_pbmpEditor) {
		delete m_pbmpEditor;
		m_pbmpEditor = NULL;
	}
}

// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
static const ElementFields::FIELDSTRUCT fields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType,	_T ("")								),	// Prefix (required)
	ElementFields::FIELDSTRUCT (m_lpszID,			_T ("0")							),	// ID (required)
	ElementFields::FIELDSTRUCT (m_lpszX,			_T ("1000")							),	// x (required)
	ElementFields::FIELDSTRUCT (m_lpszY,			_T ("0")							),	// y (required)
	ElementFields::FIELDSTRUCT (m_lpszWidth,		_T ("0")							),	// size.cx
	ElementFields::FIELDSTRUCT (m_lpszHeight,		_T ("0")							),	// size.cy
	ElementFields::FIELDSTRUCT (m_lpszThicknessCX,	_T ("0")							),	// thickness.cx
	ElementFields::FIELDSTRUCT (m_lpszThicknessCY,	_T ("5")							),	// thickness.cy
	ElementFields::FIELDSTRUCT (m_lpszType,			_T ("0")							),	// type
	ElementFields::FIELDSTRUCT (m_lpszColor,		_T ("0")							),
	ElementFields::FIELDSTRUCT (),
};

CString CShapeElement::ToString (const FoxjetCommon::CVersion & ver) const
{
	CStringArray v;

	v.Add (_T ("Shape"));
	v.Add (FoxjetDatabase::ToString ((long)GetID ()));
	v.Add (FoxjetDatabase::ToString (m_pt.x));
	v.Add (FoxjetDatabase::ToString (m_pt.y));
	v.Add (FoxjetDatabase::ToString (m_size.cx));
	v.Add (FoxjetDatabase::ToString (m_size.cy));
	v.Add (FoxjetDatabase::ToString (m_thickness.cx));
	v.Add (FoxjetDatabase::ToString (m_thickness.cy));
	v.Add (FoxjetDatabase::ToString ((int)m_type));

	{
		CString str;
		str.Format (_T ("%06X"), GetColor ());
		v.Add (str);
	}

	return FoxjetDatabase::ToString (v);
}

bool CShapeElement::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	CStringArray vstrToken;
	CPrinterFont f;
	bool bResult = true;

	Parse (str, ver, vstrToken);

	if (vstrToken [0].CompareNoCase (_T ("Shape")) != 0) {
		TRACEF ("Wrong prefix \"" + vstrToken [0] + "\"");
		return false;
	}

	int nIndex = 1;
	CPoint pt (0, 0);
	CSize size (0, 0), thickness (0, 0);

	bResult &= SetID (_tcstoul (GetParam (vstrToken, nIndex++), NULL, 10));
		pt.x = _ttoi (GetParam (vstrToken, nIndex++));
		pt.y = _ttoi (GetParam (vstrToken, nIndex++));
		size.cx = _ttoi (GetParam (vstrToken, nIndex++));
		size.cy = _ttoi (GetParam (vstrToken, nIndex++));
		thickness.cx = _ttoi (GetParam (vstrToken, nIndex++));
		thickness.cy = _ttoi (GetParam (vstrToken, nIndex++));
	bResult &= SetPos (pt, NULL);
	bResult &= SetSize (size);
	bResult &= SetThickness (thickness);
	bResult &= SetType ((SHAPETYPE)BindTo (_ttoi (GetParam (vstrToken, nIndex++)), (int)SHAPETYPE_FIRST, (int)SHAPETYPE_LAST));
	bResult &= SetColor (_tcstoul (vstrToken [nIndex++], NULL, 16));

	return bResult;
}

CSize CShapeElement::Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly, 
							FoxjetCommon::DRAWCONTEXT context)
{
	CSize sizeEditor = GetSize (head);
	CSize sizePrinter = CBaseElement::ThousandthsToLogical (sizeEditor, head);
	double dScreenRes = CCoord::GetPixelsPerXUnit (INCHES, head);
	double dPrintRes = CBaseElement::CalcPrintRes (head);
	double dFactor = dPrintRes / dScreenRes;
	double dStretch [2] = { 1, 1 };

	CBaseElement::CalcStretch (head, dStretch);

	switch (m_type) {
	case SHAPETYPE_HORZ:	
		sizePrinter.cy = m_thickness.cy;	
		break;
	case SHAPETYPE_VERT:	
		sizePrinter.cx = m_thickness.cx;	
		sizePrinter.cy = (int)((double)sizePrinter.cy / dStretch [1]);
		break;
	case SHAPETYPE_RECT:	
		sizePrinter.cy = (int)((double)sizePrinter.cy / dStretch [1]);
		break;
	}

	int cx = (int)((double)sizePrinter.cx / dFactor);
	int cy = (int)((double)sizePrinter.cy * dStretch [1]);

	if (GetRedraw ())
		Free ();

	if (m_type == SHAPETYPE_RECT) 
		cy = sizePrinter.cy;

	if (!bCalcSizeOnly) {
		CDC dcMem;
		CBitmap bmpTmp;
		CRect rc (CPoint (0, 0), CSize (sizePrinter.cy, sizePrinter.cx));
		CxImage img;

		dcMem.CreateCompatibleDC (&dc);

		if (!m_pbmpPrinter || !m_pbmpEditor) {
			CBitmap bmpEditor, bmpPrinter, bmpTmp;

			Free ();

			bmpPrinter.CreateCompatibleBitmap (&dc, sizePrinter.cy, sizePrinter.cx);
			CBitmap * pBmp = dcMem.SelectObject (&bmpPrinter);
			::BitBlt (dcMem, 0, 0, sizePrinter.cy, sizePrinter.cx, NULL, 0, 0, BLACKNESS);

			dcMem.FillRect (rc, &CBrush (Color::rgbWhite));
		
			if (m_type == SHAPETYPE_RECT) {
				CRect rcInner (rc);

				rcInner.DeflateRect (m_thickness.cy, m_thickness.cx);
				rcInner.NormalizeRect ();
				dcMem.FillRect (rcInner, &CBrush (Color::rgbBlack));
				//SAVEBITMAP (dcMem, _T ("C:\\Temp\\Debug\\dcMem.bmp"));
			}

			dcMem.SelectObject (pBmp);

			m_pbmpPrinter = new CRawBitmap (bmpPrinter);

			RotateLeft90AndFlip (dcMem, bmpPrinter, bmpTmp);

			VERIFY (img.CreateFromHBITMAP (bmpTmp));

			bmpEditor.CreateCompatibleBitmap (&dc, cx, cy);

			CxBitmap hbmpTmp (img.MakeBitmap (dcMem));
			HDC hdc = ::CreateCompatibleDC (dc);
			HGDIOBJ hOld = ::SelectObject (hdc, hbmpTmp);
			pBmp = dcMem.SelectObject (&bmpEditor);
			::BitBlt (dcMem, 0, 0, cx, cy, NULL, 0, 0, WHITENESS);
			::StretchBlt (dcMem, 0, 0, cx, cy, hdc, 0, 0, sizePrinter.cx, sizePrinter.cy, SRCINVERT);
			dcMem.SelectObject (pBmp);
			::SelectObject (hdc, hOld);
			::DeleteDC (hdc);

			m_pbmpEditor = new CRawBitmap (bmpEditor);
		}
	}

	if (!bCalcSizeOnly) {
		CDC dcMem;
		CBitmap bmp;
		CBitmap * pBmp = NULL; 

		dcMem.CreateCompatibleDC (&dc);

		if (context == PRINTER) {
			m_pbmpPrinter->GetBitmap (bmp);
			pBmp = dcMem.SelectObject (&bmp);
			VERIFY (dc.BitBlt (0, 0, sizePrinter.cy, sizePrinter.cx, &dcMem, 0, 0, SRCCOPY));
		}
		else {
			m_pbmpEditor->GetBitmap (bmp);
			pBmp = dcMem.SelectObject (&bmp);
			dc.StretchBlt (0, 0, (int)((double)sizePrinter.cx / dStretch [0]), sizePrinter.cy, &dcMem, 0, 0, cx, cy, SRCCOPY);
		}

		dcMem.SelectObject (pBmp);
	}

	//sizeEditor.cx = (int)((double)sizeEditor.cx / dStretch [0]);
	sizeEditor.cy = (int)((double)sizeEditor.cy / dStretch [1]);

	return (context == PRINTER) ? CSize (sizePrinter.cy, sizePrinter.cx) : sizeEditor;
}

bool CShapeElement::SetSize (const CSize & size, const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI)
{
	const CSize min = CBitmapElement::GetMinSize ();
	CSize adjust (size);

	if (!bHeadDPI) {
		double dStretch [2] = { 1, 1 };
	
		CBaseElement::CalcStretch (head, dStretch);
		adjust.cy = (int)((double)adjust.cy / dStretch [1]);
	}

	if ((adjust.cx >= min.cx) && (adjust.cy >= min.cy)) {
		m_size = adjust;
		SetRedraw ();

		return true;
	}

	return false;
}

int CShapeElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	return GetClassID ();
}

CString CShapeElement::GetDefaultData() const
{
	switch (m_type) {
	case SHAPETYPE_HORZ:	return LoadString (IDS_HORZIONTALLINE);
	case SHAPETYPE_VERT:	return LoadString (IDS_VERTICALLINE);
	case SHAPETYPE_RECT:	return LoadString (IDS_RECTANGLE);
	}

	return _T ("CShapeElement");
}

CString CShapeElement::GetImageData() const
{
	return GetDefaultData ();
}

// this returns the size in pixels
CSize CShapeElement::GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI) const
{
	CRect rc;
	CSize sizeLogical = CBaseElement::ThousandthsToLogical (m_size, head);
	CSize thickness (m_thickness);
	double dStretch [2] = { 1, 1 };

	CalcStretch (head, dStretch);

	switch (m_type) {
	case SHAPETYPE_HORZ:	
		sizeLogical.cx = (int)((double)sizeLogical.cx * dStretch [0]);
		thickness.cy = (int)((double)thickness.cy * dStretch [1]);
		rc = CRect (CPoint (0, 0), CSize (sizeLogical.cx, thickness.cy));	
		break;
	case SHAPETYPE_VERT:	
		sizeLogical.cx = (int)((double)sizeLogical.cx * dStretch [0]);
		rc = CRect (CPoint (0, 0), CSize (thickness.cx, sizeLogical.cy));	
		break;
	case SHAPETYPE_RECT:	
		sizeLogical.cx = (int)((double)sizeLogical.cx * dStretch [0]);
		rc = CRect (CPoint (0, 0), CSize (sizeLogical.cx, sizeLogical.cy));	
		break;
	}

	CSize sizePrinter = CSize (rc.Width (), rc.Height ());
	CSize sizeEditor = CBaseElement::LogicalToThousandths (sizePrinter, head);

	return sizeEditor;
}

void CShapeElement::ClipTo (const FoxjetDatabase::HEADSTRUCT & head)
{
	CBaseElement::ClipTo (head);

	CSize size = GetSize (head);
	int cy = CBaseElement::LogicalToThousandths (CSize (0, head.Height ()), head).y;

	if (size.cy > cy) {
		size.cy = cy;

		//SetSize (size, head);
		m_size.cy = cy;
		SetRedraw ();
	}
}

const FoxjetCommon::ElementFields::FIELDSTRUCT * CShapeElement::GetFieldBuffer () const
{
	return ::fields;
}

bool CShapeElement::SetSize (const CSize & size)
{
	m_size = size;
	SetRedraw ();
	return true;
}

bool CShapeElement::SetThickness (const CSize & thickness)
{
	m_thickness = thickness;
	SetRedraw ();
	return true;
}

bool CShapeElement::SetType (SHAPETYPE type)
{
	m_type = type;
	SetRedraw ();
	return true;
}