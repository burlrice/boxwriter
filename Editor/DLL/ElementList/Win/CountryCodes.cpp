#include "stdafx.h"
#include "CountryCodes.h"

LPCTSTR Ritz::CountryCodes::m_lpszTable		= _T ("CountryCodes");
LPCTSTR Ritz::CountryCodes::m_lpszCountry	= _T ("Country");
LPCTSTR Ritz::CountryCodes::m_lpszA2		= _T ("A2");
LPCTSTR Ritz::CountryCodes::m_lpszA3		= _T ("A3");
LPCTSTR Ritz::CountryCodes::m_lpszNumber	= _T ("Number");
