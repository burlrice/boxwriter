#if !defined(__QRPARAMSDLG_H__)
#define __QRPARAMSDLG_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UpcBarcodeParams.h : header file
//

#include "Utils.h"
#include "qrencode-3.4.4\qrencode.h"

// CQrParamsDlg dialog

class CQrParamsDlg : public FoxjetCommon::CEliteDlg
{
	DECLARE_DYNAMIC(CQrParamsDlg)

public:
	CQrParamsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CQrParamsDlg();

// Dialog Data
	enum { IDD = IDD_QRPARAMS };

	CString			m_strSymbology;
	int				m_nMag;
	int				m_nWidth;
	int				m_nHeight;
	int				m_nVersion;
	QRecLevel		m_level;
	QRencodeMode	m_hint;
	BOOL			m_bCaseSensitive;
	bool			m_bStandardParam;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog(void);
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()
};

#endif //__QRPARAMSDLG_H__