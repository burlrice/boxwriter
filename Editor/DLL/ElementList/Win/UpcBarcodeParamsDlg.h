#if !defined(AFX_UPCBARCODEPARAMS_H__56789B0A_CB30_47EB_A351_04DDD8D09D6B__INCLUDED_)
#define AFX_UPCBARCODEPARAMS_H__56789B0A_CB30_47EB_A351_04DDD8D09D6B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UpcBarcodeParams.h : header file
//

#include "TextElement.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CUpcBarcodeParamsDlg dialog

class CUpcBarcodeParamsDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CUpcBarcodeParamsDlg(UINT nID, FoxjetDatabase::HEADTYPE type, CWnd* pParent = NULL);
	CUpcBarcodeParamsDlg(FoxjetDatabase::HEADTYPE type, CWnd* pParent = NULL);   // standard constructor

	virtual int GetDefSymbology () const;

// Dialog Data
	//{{AFX_DATA(CUpcBarcodeParamsDlg)
	BOOL	m_bBold;
	BOOL	m_bItalic;
	BOOL	m_bChecksum;
	int		m_nBar [4];
	int		m_nSpace [4];
	int		m_nBarHeight;
	int		m_nMag;
	CString	m_strSymbology;
	int		m_nSize;
	//}}AFX_DATA

	BOOL m_bStandardParam;
	CString m_strFont;
	int m_nFontWidth;

	const FoxjetDatabase::HEADTYPE m_type;
	FoxjetElements::TextElement::FONTORIENTATION m_orient;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUpcBarcodeParamsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();

	// Generated message map functions
	//{{AFX_MSG(CUpcBarcodeParamsDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	afx_msg void OnOrientationClick();

	CBitmap m_bmpOrientation [2];

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UPCBARCODEPARAMS_H__56789B0A_CB30_47EB_A351_04DDD8D09D6B__INCLUDED_)
