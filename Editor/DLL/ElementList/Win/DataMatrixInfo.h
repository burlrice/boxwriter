// DataMatrixInfo.h: interface for the CDataMatrixInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATAMATRIXINFO_H__1FD88B62_F5C3_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_DATAMATRIXINFO_H__1FD88B62_F5C3_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ElementApi.h"

namespace FoxjetElements
{
	class CDataMatrixInfo  
	{
	public:
		bool IsValid () const;
		CDataMatrixInfo();
		CDataMatrixInfo (const CDataMatrixInfo & rhs);
		CDataMatrixInfo & operator = (const CDataMatrixInfo & rhs);
		virtual ~CDataMatrixInfo();

		static bool IsValidLength (int nLen);

		int m_nLength;
	};
}; //namespace FoxjetElements

#endif // !defined(AFX_DATAMATRIXINFO_H__1FD88B62_F5C3_11D4_8FC6_006067662794__INCLUDED_)

