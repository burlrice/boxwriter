#if !defined(AFX_C93BCPARAMSDLG_H__DA15BA10_E2B0_4EE7_B84D_8C923065C093__INCLUDED_)
#define AFX_C93BCPARAMSDLG_H__DA15BA10_E2B0_4EE7_B84D_8C923065C093__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// C93BcParamsDlg.h : header file
//

#include "Resource.h"
#include "Code128ParamsDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CC93BcParamsDlg dialog

class CC93BcParamsDlg : public CCode128ParamsDlg
{
// Construction
public:
	CC93BcParamsDlg(FoxjetDatabase::HEADTYPE type, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CC93BcParamsDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CC93BcParamsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CC93BcParamsDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_C93BCPARAMSDLG_H__DA15BA10_E2B0_4EE7_B84D_8C923065C093__INCLUDED_)
