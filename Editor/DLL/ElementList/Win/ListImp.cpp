#include "stdafx.h"

#include <math.h>

#include "AppVer.h"
#include "ListImp.h"

#include "List.h"
#include "CopyArray.h"
#include "resource.h"
#include "Types.h"
#include "ElementDefaults.h"
#include "Debug.h"
#include "Edit.h"
#include "PropertiesDlg.h"
#include "Coord.h"
#include "Database.h"
#include "OdbcDatabase.h"
#include "Registry.h"
#include "Alias.h"
#include "FileExt.h"
#include "Extern.h"
#include "ItiLibrary.h"
#include "TemplExt.h"
#include "PropertiesDlg.h"

#include "TextElement.h"
#include "CountElement.h"
#include "DateTimeElement.h"
#include "BitmapElement.h"
#include "ExpDateElement.h"
#include "UserElement.h"
#include "ShiftElement.h"
#include "BarcodeElement.h"
#include "DatabaseElement.h"
#include "SerialElement.h"
#include "Utils.h"
#include "BarcodeParams.h"
#include "DefineShiftDlg.h"
#include "CodesDlg.h"
#include "LabelElement.h"
#include "LabelElementDlg.h"
#include "Parse.h"
#include "DevGuids.h"
#include "BitmappedFont.h"
#include "HpTextElement.h"
#include "ShapeElement.h"
#include "IpElements.h"

extern HINSTANCE hInstance;
static bool bValve = ISVALVE ();

CArray <const CBitmappedFont  *, const CBitmappedFont *> vBitmappedFonts; 

extern int *g_log, *g_alog, *g_rspoly;
extern CRITICAL_SECTION csDatamatrix;
extern CRITICAL_SECTION csQR;

#pragma data_seg( "csTime" )
	extern CRITICAL_SECTION csTime;
#pragma

namespace DefineMenu
{
	using namespace FoxjetElements;

	typedef void (CALLBACK * LPDEFINECMD) (CWnd * pParent, const FoxjetCommon::CVersion & ver);

	struct
	{
		UINT		m_nMenuStrID;
		UINT		m_nMenuStatusStrID;
		LPDEFINECMD	m_lpfct;
	} static const map [] = 
	{
		{ IDS_CUSTOMDATETIMEFORMATS,	IDS_CUSTOMDATETIMEFORMATS_STATUSTEXT,	OnDefineCustomdatetimeformats,				},
		{ IDS_HOURMONTHCODES,			IDS_HOURMONTHCODES_STATUSTEXT,			OnDefineMonthHourCodes,						},
		{ IDS_BITMAPEDITOR,				IDS_BITMAPEDITOR_STATUSTEXT,			OnDefineBitmapEditor,						},
		{ IDS_LABELEDITOR,				IDS_LABELEDITOR_STATUSTEXT,				OnDefineLabelEditor,						},
		{ IDS_SHIFTCODES,				IDS_SHIFTCODES_STATUSTEXT,				(LPDEFINECMD)OnDefineShiftCodes,			},
		{ IDS_SUBELEMENTS,				IDS_SUBELEMENTS_STATUSTEXT,				OnDefineSubelements,						},
		{ IDS_APPLICATIONIDENTIFIERS,	IDS_APPLICATIONIDENTIFIERS_STATUSTEXT,	OnDefineApplicationIdentifiers,				},
		{ IDS_BARCODEPARAMS,			IDS_BARCODEPARAMS_STATUSTEXT,			(LPDEFINECMD)OnDefineBarcodeParams,			},
	};
	static const int nFirst = 1301;
	static const int nSize = (sizeof (map) / sizeof (map [0]));
}; //namespace DefineMenu

namespace ElementMenu
{
	using namespace FoxjetElements;
	using namespace FoxjetElements::ShapeElement;

#define DECLARE_CREATE(t) \
	FoxjetCommon::CBaseElement * CALLBACK Create##t (const FoxjetDatabase::HEADSTRUCT & head) \
	{ \
		return new ( ##t ) (head); \
	} \
	FoxjetCommon::CBaseElement * CALLBACK Copy##t (const FoxjetCommon::CBaseElement * pSrc) \
	{ \
		return new ( ##t ) (* ( ##t *)pSrc); \
	}

#define CREATEMAPENTRY(nType, lpszType, nMenuStrID, nMenuStatusStrID, classname, pfctEdit) \
	{ nType, lpszType, nMenuStrID, nMenuStatusStrID, Create##classname, Copy##classname, pfctEdit, }

	DECLARE_CREATE (CTextElement)
	DECLARE_CREATE (CBitmapElement)
	DECLARE_CREATE (CCountElement)
	DECLARE_CREATE (CDateTimeElement)
	DECLARE_CREATE (CExpDateElement)
	DECLARE_CREATE (CUserElement)
	DECLARE_CREATE (CShiftElement)
	DECLARE_CREATE (CBarcodeElement)
	DECLARE_CREATE (CDatabaseElement)
	DECLARE_CREATE (CSerialElement)
	DECLARE_CREATE (CLabelElement)
	DECLARE_CREATE (CShapeElement)

	typedef FoxjetCommon::CBaseElement * (CALLBACK * LPCREATEELEMENT) (const FoxjetDatabase::HEADSTRUCT & head);
	typedef FoxjetCommon::CBaseElement * (CALLBACK * LPCOPYELEMENT) (const FoxjetCommon::CBaseElement * pSrc);
	typedef bool (CALLBACK * LPEDITELEMENT) (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, CWnd * pParent, const CSize & pa, UNITS units, 
		bool bReadOnlyElement, bool bReadOnlyLocation, 
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);

	struct
	{
		ELEMENTTYPE			m_type;
		LPCTSTR				m_lpszType;
		UINT				m_nMenuStrID;
		UINT				m_nMenuStatusStrID;
		LPCREATEELEMENT		m_lpCreate;
		LPCOPYELEMENT		m_lpCopy;
		LPEDITELEMENT		m_lpEdit;
	} static const map [] = 
	{
		CREATEMAPENTRY (TEXT,				_T ("Text"),		IDS_TEXT,				IDS_MENUTEXT_,				CTextElement,		OnEditTextElement),
		CREATEMAPENTRY (BMP,				_T ("Bitmap"),		IDS_BITMAP,				IDS_MENUBITMAP_,			CBitmapElement,		OnEditBitmapElement),
		CREATEMAPENTRY (COUNT,				_T ("Count"),		IDS_COUNT,				IDS_MENUCOUNT_,				CCountElement,		OnEditCountElement),
		CREATEMAPENTRY (DATETIME,			_T ("DateTime"),	IDS_DATETIME,			IDS_MENUDATETIME_,			CDateTimeElement,	OnEditDateTimeElement),
		CREATEMAPENTRY (EXPDATE,			_T ("ExpDate"),		IDS_EXPDATE,			IDS_MENUEXPDATE_,			CExpDateElement,	OnEditExpDateElement),
		CREATEMAPENTRY (USER,				_T ("User"),		IDS_USER,				IDS_MENUUSER_,				CUserElement,		OnEditUserElement),
		CREATEMAPENTRY (SHIFTCODE,			_T ("Shift"),		IDS_SHIFT,				IDS_MENUSHIFT_,				CShiftElement,		OnEditShiftElement),
		CREATEMAPENTRY (BARCODE,			_T ("Barcode"),		IDS_BARCODE,			IDS_MENUBARCODE_,			CBarcodeElement,	OnEditBarcodeElement),
		CREATEMAPENTRY (DATABASE,			_T ("Database"),	IDS_DATABASE,			IDS_MENUDATABASE,			CDatabaseElement,	OnEditDatabaseElement),
		CREATEMAPENTRY (SERIAL,				_T ("Serial"),		IDS_SERIAL,				IDS_MENUSERIAL_,			CSerialElement,		OnEditSerialElement),
		CREATEMAPENTRY (LABEL,				_T ("Label"),		IDS_LABEL,				IDS_MENULABEL_,				CLabelElement,		OnEditLabelElement),
		CREATEMAPENTRY (SHAPE,				_T ("Shape"),		IDS_SHAPE,				IDS_MENUSHAPE_,				CShapeElement,		OnEditShapeElement),
	};
	static const int nSize = (sizeof (map) / sizeof (map [0]));

	int GetIndex (int nType) {
		for (int i = 0; i < nSize; i++) 
			if ((int)map [i].m_type == nType)
				return i;

		return -1;
	}
}; //namespace ElementMenu



#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//#define __SHOW_TOKENS__

using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace ListGlobals;
using namespace FoxjetElements;

static bool	bConfining	= true;

DIAGNOSTIC_CRITICAL_SECTION (csShift);
DIAGNOSTIC_CRITICAL_SECTION (csDays);

CArray <DefineShiftDlg::CShiftCode, DefineShiftDlg::CShiftCode &> vShiftcodes;
CodesDlg::CCodesArray vMonthcodes;
CodesDlg::CCodesArray vHourcodes;
CodesDlg::CCodesArray vDayOfWeekCodes;
CodesDlg::CCodesArray vRollover;
CodesDlg::CCodesArray vQuarterHourcodes;

////////////////////////////////////////////////////////////////////////////////

static bool bProductionConfig = false;

ELEMENT_API bool FoxjetCommon::IsProductionConfig ()
{
	return ::bProductionConfig;
}

ELEMENT_API void FoxjetCommon::SetProductionConfig (bool bValue, FoxjetDatabase::COdbcDatabase & db)
{
	::bProductionConfig = bValue;

	if (::bProductionConfig) {
		CArray <LINESTRUCT, LINESTRUCT &> vLines;
		const CString strName = _T ("PRODUCTION");

		TRACEF (_T ("SetProductionConfig (true)"));

		GetPrinterLines (db, GetPrinterID (db), vLines); //FoxjetDatabase::GetLineRecords (db, vLines);

		for (int i = 0; i < vLines.GetSize (); i++) {
			if (GetTaskID (db, strName, vLines [i].m_strName, GetPrinterID (db)) == NOTFOUND) {
				CArray <BOXSTRUCT, BOXSTRUCT &> vBoxes;
				CArray <HEADSTRUCT, HEADSTRUCT&> vHeads;
				TASKSTRUCT task;
				ULONG lLineID = vLines [i].m_lID;

				task.m_lBoxID = -1;
				task.m_lLineID = lLineID;
				task.m_strDesc = _T ("For printhead testing");
				task.m_strName = strName;

				FoxjetDatabase::GetBoxRecords (db, lLineID, vBoxes);
				FoxjetDatabase::GetLineHeads (db, lLineID, vHeads);

				if (vBoxes.GetSize ())
					task.m_lBoxID = vBoxes [0].m_lID;

				VERIFY (AddTaskRecord (db, task));

				for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
					MESSAGESTRUCT msg;
					HEADSTRUCT head = vHeads [nHead];
					
					msg.m_lTaskID = task.m_lID;
					msg.m_strName = strName;
					msg.m_strDesc = task.m_strDesc;
					msg.m_lHeadID = head.m_lID;
					msg.m_strData.Format (_T ("{Bitmap,1,0,0,,,,,,%s}{Count,2,8000,0,,,,MK Arial}"), FormatString (CBitmapElement::GetDefFilename ()));
					TRACEF (msg.m_strData);

					task.m_vMsgs.Add (msg);
				}

				VERIFY (UpdateTaskRecord (db, task));
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

bool ParseProductArea (CElementList & vElements, const CStringArray & v, 
					   const CVersion & ver)
{
	CString strToken [] = { 
		_T (""),			// prefix
		_T ("10000"),		// width in 1000-ths of an inch
		_T ("1000"),		// height in 1000-ths of an inch
		_T ("0"),			// units
		_T ("0")			// head ID
	};

	if (v.GetSize () && v [0].CompareNoCase (_T ("ProdArea")) != 0) 
		return false;

	for (int i = 0; i < min (v.GetSize (), 5); i++)
		if (v [i].GetLength ())
			strToken [i] = v [i];

	CSize pa (
		(int)_tcstol (strToken [1], NULL, 10),
		(int)_tcstol (strToken [2], NULL, 10));
	vElements.SetProductArea (pa);
	vElements.SetUnits ((UNITS)(UINT)_tcstol (strToken [3], NULL, 10));
	ULONG lHeadID = (ULONG)_tcstol (strToken [4], NULL, 10);
	HEADSTRUCT h;

//	if (GetHeadRecord (lHeadID, h)) // can't do this here // see 1.20.0837.54, note on CTask::Load
//		vElements.SetHead (h);
	
	#ifdef _DEBUG
	CString str;
	str.Format (_T ("Product area = (%u, %u), units = %u"), 
		vElements.GetProductArea ().cx, 
		vElements.GetProductArea ().cy, 
		(UINT)vElements.GetUnits ());
	//TRACEF (str);
	#endif //_DEBUG

	return true;
}

bool ParsePacket (FoxjetCommon::CElementList & vElements, const CString & str, 
				  const FoxjetCommon::CVersion & ver, ULONG lLocalParamUID)
{
	bool bResult = false;

	#ifdef _DEBUG
	CString strDebug;
	//TRACEF ("ParsePacket (" + str + ")");
	#endif //_DEBUG

	CStringArray v;

	Tokenize (str, v);

	if (v.GetSize ()) {
		if (!v [0].CompareNoCase (_T ("ProdArea"))) 
			bResult = ParseProductArea (vElements, v, ver);
		else if (!v [0].CompareNoCase (_T ("FontDef")))
			bResult = ParseFontDef (vElements, v, ver);
		else if (!v [0].CompareNoCase (_T ("ElementDef"))) 
			bResult = ParseElementDef (vElements, str, ver);
		else if (!v [0].CompareNoCase (_T ("Ver"))) 
			bResult = ParseVersion (vElements, str);
		else if (!v [0].CompareNoCase (_T ("BarcodeParam"))) {
			CBarcodeParams b;

			b.FromString (str, ::verApp);
			b.SetID (GetParamsPerSymbology ());
			TRACEF (b.ToString (::verApp));
			CBarcodeElement::SetLocalParam (lLocalParamUID, b.GetSymbology (), b);
		}
	}

	return bResult;
}

bool ParseElementDef (FoxjetCommon::CElementList & vElements, const CString & str, 
					  const FoxjetCommon::CVersion & ver)
{
	//TRACEF ("CElementList::ParseElementDef (\"" + str + "\"");
	CStringArray v;

	Tokenize (str, v);

	if (v.GetSize () >= 2 && v [0].CompareNoCase (_T ("ElementDef")) != 0) 
		return false;

	int nType = FoxjetCommon::GetElementType (v [1]);

	if (nType != -1) {
		const CBaseElement & def = ListGlobals::defElements.GetElement (nType);
		CBaseElement * pTmp = CopyElement (&def, &vElements.GetHead (), false);
		CLongArray braces;
		CString strElement = str, strToken (_T ("ElementDef"));

		// remove the "ElementDef" token
		int nIndex = strElement.Find (strToken, 0);
		
		if (nIndex != -1) {
			strElement.Delete (nIndex, strToken.GetLength () + 1);
			//TRACEF (strElement);

			if (pTmp->FromString (strElement, ver)) {
				//TRACEF ("Setting default for " + pTmp->ToString (ver));
				return ListGlobals::defElements.SetElement (nType, * pTmp, ver);
			}
			else {
				//TRACEF ("Failed to set default for: " + GetElementResStr (nType));
			}
		}
		else {
			//TRACEF ("No \"ElementDef\" token");
		}

		delete pTmp;
	}
	else {
		//TRACEF ("Failed to set default for: " + v [2]);
	}

	return false;
}

bool ParseFontDef(FoxjetCommon::CElementList & vElements, const CStringArray & v, 
				  const FoxjetCommon::CVersion & ver)
{
	CString strToken [] = { 
		_T (""), 
		_T (""), 
		_T ("Courier New"), 
		_T ("30"), 
		_T ("1"), 
		_T ("0"),
	};

	if (v.GetSize () && v [0].CompareNoCase (_T ("FontDef")) != 0) 
		return false;

	for (int i = 0; i < min (v.GetSize (), 6); i++)
		if (v [i].GetLength ())
			strToken [i] = v [i];

	CString strPrefix = strToken [1];
	CString strName = strToken [2];
	int nSize = _ttoi (strToken [3]);
	bool bBold = _ttoi (strToken [4]) ? true : false;
	bool bItalic = _ttoi (strToken [5]) ? true : false;

	#ifdef _DEBUG
	CString strDebug;
	strDebug.Format (_T ("%s, %d, %d, %d"), strName, nSize, bBold, bItalic);
	//TRACEF (strDebug);
	#endif //_DEBUG

	if (strPrefix == _T ("")) { // set defs for all elements
		//TRACEF ("Setting font for all elements");
		ListGlobals::defElements.SetElementFonts (strName, nSize, bBold, bItalic, ver);
		return true;
	}
	else {
		int nType = FoxjetCommon::GetElementType (strPrefix);

		if (nType != -1) {
			const CBaseElement & obj = ListGlobals::defElements.GetElement (nType);

			if (obj.HasFont ()) {
				CPrinterFont fnt (strName, nSize, bBold, bItalic);
				CBaseTextElement * pTxt = (CBaseTextElement *)CopyElement (&obj, &vElements.GetHead (), false);
				bool bResult = false;

				if (pTxt->SetFont (fnt))
					bResult = ListGlobals::defElements.SetElement (nType, * pTxt, ver);

				delete pTxt;
				return bResult;
			}
		}
		else {
			//TRACEF ("Can't set font for " + strPrefix);
		}
	}

	return false;
}

bool ParseVersion (FoxjetCommon::CElementList & vElements, const CString & str)
{
	CVersion ver;

	if (ver.FromString (str)) 
		return vElements.SetVersion (ver);

	return false;
}

bool ParseTimestamp (FoxjetCommon::CElementList & vElements, const CString & str)
{
	//TRACEF (str);
	return true;
}

int CALLBACK EnumFontFamExProc(ENUMLOGFONTEX *lpelfe,
							   NEWTEXTMETRICEX *lpntme, 
							   int FontType, LPARAM lParam)
{
	CStringArray * p = (CStringArray *)lParam;
	ASSERT (p);

	if (FontType & TRUETYPE_FONTTYPE) {
		CStringArray & v = * p;
		CString strFont = lpelfe->elfLogFont.lfFaceName;

		for (int i = 0; i < v.GetSize (); i++) 
			if (v [i] == strFont)
				return 1;

		p->Add (strFont);
	}

	return 1;
}

ELEMENT_API CString FoxjetCommon::GetAppTitle ()
{
	if (afxCurrentResourceHandle != NULL)
		return LoadString (::bValve ? IDS_BWVALVE : IDS_BWPRO); 
	
	return ::bValve ? _T ("BoxWriter Vx") : _T ("BoxWriter ELITE"); 
}

ELEMENT_API int FoxjetCommon::GetElementType (const CString & strPrefix)
{
	for (int i = 0; i < ElementMenu::nSize; i++) 
		if (!strPrefix.CompareNoCase (ElementMenu::map [i].m_lpszType)) 
			return ElementMenu::map [i].m_type;

	return -1;
}

ELEMENT_API int FoxjetCommon::GetElementTypeCount ()
{
#ifdef __NICELABEL__
	return LAST - FIRST;
#else
	return (SHAPE + 1) - FIRST; // no RFID
#endif
}

ELEMENT_API bool FoxjetCommon::IsValidElementType (int nType, const FoxjetDatabase::HEADSTRUCT & head)
{
	if (FoxjetDatabase::IsHpHead (head.m_nHeadType)) 
		return HpElements::IsValidElementType (nType, head);

	if (nType == LABEL) 
		return CLabelElement::IsLabelEditorPresent ();

	if (::bValve) {
		//if (nType == BARCODE)
		//	return false;
	}

	return nType >= 0 && nType <= GetElementTypeCount ();
}

ELEMENT_API CString FoxjetCommon::GetElementResStr(int nType)
{
	CString str;
	int nIndex = ElementMenu::GetIndex (nType);

	if (nIndex != -1) 
		str = LoadString (ElementMenu::map [nIndex].m_nMenuStrID);
	else {
		ASSERT (0);
	}

	return str;
}

ELEMENT_API CString FoxjetCommon::GetElementMenuStr(int nType)
{
	CString str;
	int nIndex = ElementMenu::GetIndex (nType);

	if (nIndex != -1) 
		str = LoadString (ElementMenu::map [nIndex].m_nMenuStatusStrID);
	else {
		ASSERT (0);
	}

	return str;
}

ELEMENT_API FoxjetCommon::CBaseElement * FoxjetCommon::CreateElement (int nType, const FoxjetDatabase::HEADSTRUCT & head)
{
	if (FoxjetDatabase::IsHpHead (head.m_nHeadType)) 
		return HpElements::CreateElement (nType, head);

	CBaseElement * p = NULL;
	int nIndex = ElementMenu::GetIndex (nType);

	if (nIndex != -1) {
		ASSERT (ElementMenu::map [nIndex].m_lpCreate);

		p = (* ElementMenu::map [nIndex].m_lpCreate) (head);

		ASSERT (p);
	}

	if (p)
		p->ClipTo (head);

	return p;
}

ELEMENT_API FoxjetCommon::CBaseElement * FoxjetCommon::CreateElement (HBITMAP hBmp, const FoxjetDatabase::HEADSTRUCT & head,
																	  const CString & strName)
{
	if (FoxjetDatabase::IsHpHead (head.m_nHeadType)) 
		return HpElements::CreateElement (hBmp, head, strName);

	CBaseElement * pResult = NULL;

	if (hBmp) {
		const CBitmapElement & def = (CBitmapElement &)ListGlobals::defElements.GetElement (BMP);
		CString strDefFilePath = def.GetFilePath ();
		CString strDir = CBitmapElement::GetDefPath ();
		CString strPrefix = strName.GetLength () ? strName : _T ("Image");
		int nIndex = strDefFilePath.ReverseFind ((TCHAR)'\\');

		if (nIndex != -1)
			strDir = strDefFilePath.Left (nIndex + 1);

		CString strFilepath = CreateTempFilename (strDir, strPrefix, _T ("bmp"));
		
		if (SaveBitmap (hBmp, strFilepath)) {
			pResult = CreateElement (BMP, head);

			if (pResult) {
				CBitmapElement * pBmp = (CBitmapElement *)pResult;
				
				if (!pBmp->SetFilePath (strFilepath, head)) {
					delete pResult;
					pResult = NULL;
				}
			}
		}
	}

	if (pResult)
		pResult->ClipTo (head);

	return pResult;
}

ELEMENT_API FoxjetCommon::CBaseElement * FoxjetCommon::CreateElement (const CString & str, const FoxjetDatabase::HEADSTRUCT & head)
{
	if (FoxjetDatabase::IsHpHead (head.m_nHeadType)) 
		return HpElements::CreateElement (str, head);

	const CBaseElement & def = ListGlobals::defElements.GetElement (TEXT);
	CBaseElement * pResult = CopyElement (&def, &head, true);

	if (CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, pResult)) {
		p->SetDefaultData (str);
	}
	else {
		delete pResult;
		pResult = NULL;
	}

	if (pResult)
		pResult->ClipTo (head);

	return pResult;
}

ELEMENT_API FoxjetCommon::CBaseElement * FoxjetCommon::CopyElement (const FoxjetCommon::CBaseElement * pSrc, const FoxjetDatabase::HEADSTRUCT * pHead, bool bUseDefSize)
{
	if (pHead) {
		if (FoxjetDatabase::IsHpHead (pHead->m_nHeadType)) 
			return HpElements::CopyElement (pSrc, pHead, bUseDefSize);
	}
	else if (pSrc) {
		if (FoxjetDatabase::IsHpHead (pSrc->GetHead ().m_nHeadType)) 
			return HpElements::CopyElement (pSrc, &pSrc->GetHead (), bUseDefSize);
	}

	CBaseElement * p = NULL;

	ASSERT (pSrc);

	int nType = pSrc->GetClassID ();
	int nIndex = ElementMenu::GetIndex (nType);

	if (nIndex != -1) {
		ASSERT (ElementMenu::map [nIndex].m_lpCopy);

		p = (* ElementMenu::map [nIndex].m_lpCopy) (pSrc);

		ASSERT (p);

		if (pHead)
			p->SetHead (* pHead);

		if (nType == BMP) {
			CBitmapElement * pBmp = (CBitmapElement *)p;

			if (pHead && bUseDefSize) {
				double dStretch [2] = { 1, 1 };
				CSize size = CBitmapElement::GetSizePixels (pBmp->GetFilePath (), * pHead);

				size = CBaseElement::LogicalToThousandths (size, * pHead);

				CBaseElement::CalcStretch (* pHead, dStretch);

				size.cy = (int) ((double)size.cy * dStretch [1]);
				pBmp->SetSize (size, * pHead);
			}
		}
	}

	if (p && pHead)
		p->ClipTo (* pHead);

	return p;
}

ELEMENT_API bool FoxjetCommon::LoadToolBar (CToolBar & bar) 
{
	return bar.LoadToolBar (IDR_RITZELEMENTBAR_MATRIX) ? true : false;
}

ELEMENT_API bool FoxjetCommon::OpenDatabase (COdbcDatabase & database, const CString & strDSN, const CString & strFilename, bool bCanThrowException)
{
	CString strFilepath = GetHomeDir () + _T ("\\") + strFilename;
	CString strKey = _T ("Software\\Foxjet\\MarksmanELITE\\ODBC\\") + strDSN;
	CString str = strDSN;
	CString strUID = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("UID"), _T ("")); 
	CString strPWD = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("PWD"), _T ("")); 

	TRACEF (_T ("IsGojo: ") + ToString (IsGojo ()));
	TRACEF (_T ("IsNetworked: ") + ToString (IsNetworked ()));

	if (IsNetworked ()) {
		if (strUID.GetLength () && strPWD.GetLength ())
			str += _T (";UID=") + strUID + _T (";PWD=") + strPWD + _T (";");

		try {
			if (database.OpenEx (_T ("DSN=") + str, CDatabase::noOdbcDialog)) {
				UpgradeDatabase (database, strDSN);
				return true;
			}
		}
		catch (CDBException * e)		{ if (bCanThrowException) HANDLEEXCEPTION (e); return false; }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}
	
	if (::GetFileAttributes (strFilepath) == -1) {
		bool bACCDB = FoxjetDatabase::IsAccdb () || FoxjetDatabase::IsAccdb (strFilepath);
		HINSTANCE hModule = ::hInstance; 
		bool bResult = false;
		UINT nID = ISVALVE () ? IDR_MKVXDB : (bACCDB ? IDR_MKELITEACCDB : IDR_MKELITEDB);

		TRACEF (CString (_T ("Creating database: ")) + (ISVALVE () ? _T ("IDR_MKVXDB: ") : (bACCDB ? _T ("IDR_MKELITEACCDB: ") : _T ("IDR_MKELITEDB: "))) + strFilepath);

		if (HRSRC hRSRC = ::FindResource (hModule, MAKEINTRESOURCE (nID), _T ("MSAccess"))) {
			DWORD dwSize = ::SizeofResource (hModule, hRSRC);

			if (HGLOBAL hGlobal = ::LoadResource (hModule, hRSRC)) {
				if (LPVOID lpData = ::LockResource (hGlobal)) {
					if (FILE * fp = _tfopen (strFilepath, _T ("wb"))) {
						if (fwrite (lpData, dwSize, 1, fp) == 1)
							bResult = true;
		
						fclose (fp);
					}
				}
			}
		}

		if (!bResult) {
			CString str;

			str.Format (LoadString (IDS_CANTCREATEDB),
				strFilepath);

			MsgBox (str, MB_ICONERROR);
		}
	}

	return FoxjetDatabase::OpenDatabase (database, strDSN, strFilename);
}

ELEMENT_API bool FoxjetCommon::InitInstance (const FoxjetCommon::CVersion & ver)
{
	::InitializeCriticalSection (&(::csDatamatrix));
	::InitializeCriticalSection (&(::csQR));
	::InitializeCriticalSection (&(::csTime));

	CBitmapElement::CreateDefBitmap (CBitmapElement::GetDefPath () + _T ("Def.bmp"));
	LoadBitmappedFonts (ver);
	//InitDatabase (db);
	DefaultCustomDateTimeFmts (defElements.m_strRegSection);
	//LoadSystemParams (db, ver);
	ListGlobals::defElements.LoadRegDefs (ver);
	//FoxjetIpElements::InitIpElements ();

	return true;
}

ELEMENT_API bool FoxjetCommon::InitInstance (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	DBMANAGETHREADSTATE (db);

	if (!InitInstance (ver))
		return false;

	ASSERT (db.IsOpen ());

	if (!SetDB (db))
		return false;

	//CBitmapElement::CreateDefBitmap (CBitmapElement::GetDefPath () + _T ("Def.bmp"));
	//LoadBitmappedFonts (ver);
	InitDatabase (db);
	//DefaultCustomDateTimeFmts (defElements.m_strRegSection);
	LoadSystemParams (db, ver);
	//ListGlobals::defElements.LoadRegDefs (ver);
	FoxjetIpElements::InitIpElements ();

	::bValve = ISVALVE ();

	return true;
}


/* TODO: rem
ELEMENT_API bool FoxjetCommon::InitInstance (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{

	DBMANAGETHREADSTATE (db);

	ASSERT (db.IsOpen ());

	if (!SetDB (db))
		return false;

	CBitmapElement::CreateDefBitmap (CBitmapElement::GetDefPath () + _T ("Def.bmp"));
	LoadBitmappedFonts (ver);
	InitDatabase (db);
	DefaultCustomDateTimeFmts (defElements.m_strRegSection);
	LoadSystemParams (db, ver);
	ListGlobals::defElements.LoadRegDefs (ver);
	FoxjetIpElements::InitIpElements ();

	::bValve = ISVALVE ();

	return true;
}
*/

ELEMENT_API void FoxjetCommon::GetBitmappedFonts (CStringArray & v)
{
	for (int i = 0; i < ::vBitmappedFonts.GetSize (); i++)
		if (const CBitmappedFont * p = ::vBitmappedFonts [i])
			v.Add (p->GetName ());
}

ELEMENT_API void FoxjetCommon::LoadBitmappedFonts (const FoxjetCommon::CVersion & ver)
{
	CStringArray vFiles;
	const CString strDir = GetHomeDir () + _T ("\\Fonts");
	const CString strExt = _T (".fnt");
	const CString strFile = _T ("*") + strExt;

	ASSERT (::vBitmappedFonts.GetSize () == 0);

	FoxjetDatabase::GetFiles (strDir, strFile, vFiles);

	for (int i = 0; i < vFiles.GetSize (); i++) {
		CString strName = vFiles [i];
		
		strName.Replace (strDir + _T ("\\"), _T ("Vx "));
		strName.Replace (strExt, _T (""));

		::vBitmappedFonts.Add (new CBitmappedFont (strName, vFiles [i]));
	}
}

ELEMENT_API bool FoxjetCommon::IsBitmappedFont (const CString & str)
{
	for (int i = 0; i < ::vBitmappedFonts.GetSize (); i++) 
		if (const CBitmappedFont * p = ::vBitmappedFonts [i])
			if (!str.CompareNoCase (p->GetName ()))
				return true;

	return false;
}

ELEMENT_API bool FoxjetCommon::LoadSystemParams (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	DBMANAGETHREADSTATE (db);
	
	InitDatabase (db);
	LoadLineMapping (db);
	LoadBarcodeParams (db, ver);
	LoadShiftcodes (db, ver);
	LoadMonthCodes (db, ver);
	LoadHourCodes (db, ver);
	LoadDayCodes (db, ver);
	LoadRolloverSettings (db, ver);

	// std // #if __CUSTOM__ == __SW0840__
	LoadQuarterHourCodes (db, ver);
	// std // #endif

	return true;
}

static void DeleteBitmappedFonts ()
{
	for (int i = 0; i < ::vBitmappedFonts.GetSize (); i++)
		if (const CBitmappedFont * p = ::vBitmappedFonts [i])
			delete p;

	::vBitmappedFonts.RemoveAll ();
}

ELEMENT_API bool FoxjetCommon::ExitInstance (const FoxjetCommon::CVersion & ver)
{
	COdbcDatabase::FreeDsnCache ();
	CLabelElement::FreeStaticMembers ();

	DeleteAi ();
	DeleteSubElements ();

	if (ListGlobals::IsDBSet ())
		DeleteBarcodeParams (GetDB ());

	DeleteBitmappedFonts ();

	FoxjetElements::CDateTimeElement::SetPrintTime (NULL, -1);

	::EnterCriticalSection (&(::csDatamatrix));

	if (g_log) {
		delete [] g_log;
		g_log = NULL;
	}
	
	if (g_alog) {
		delete [] g_alog;
		g_alog = NULL;
	}

	if (g_rspoly) {
		delete [] g_rspoly;
		g_rspoly = NULL;
	}

	::LeaveCriticalSection (&(::csDatamatrix));

	::DeleteCriticalSection (&(::csDatamatrix));
	::DeleteCriticalSection (&(::csQR));
	::DeleteCriticalSection (&(::csTime));

	FoxjetIpElements::FreeIpElements ();

	return true;
}


ELEMENT_API CString FoxjetCommon::GetDSN ()
{
	if (ISVALVE ())
		return _T ("MarksmanVX");
	else
		return _T ("MarksmanELITE");
}

ELEMENT_API CString FoxjetCommon::GetDatabaseName ()
{
	if (ISVALVE ())
		return _T ("MarksmanVX.mdb");
	else
		return IsAccdb () ? _T ("MarksmanELITE.accdb") : _T ("MarksmanELITE.mdb");
}

ELEMENT_API bool FoxjetCommon::OnEditElement (FoxjetCommon::CBaseElement & e, 
											  CWnd * pParent, 
											  const CSize & pa, UNITS units,
											  const FoxjetCommon::CElementList * pList, 
											  bool bReadOnlyElement, 
											  bool bReadOnlyLocation,
											  CElementListArray * pAllLists,
											  CElementArray * pUpdate)
{
	if (IsHpHead (e.GetHead ().m_nHeadType)) 
		return HpElements::OnEditElement (e, pParent, pa, units, pList, bReadOnlyElement, bReadOnlyLocation, pAllLists, pUpdate);

	bool bResult = false;
	int nIndex = ElementMenu::GetIndex (e.GetClassID ());

	if (nIndex != -1) {
		ASSERT (ElementMenu::map [nIndex].m_lpEdit);

		bResult = (* ElementMenu::map [nIndex].m_lpEdit) (e, pList, pParent, pa, units, 
			bReadOnlyElement, bReadOnlyLocation, pAllLists, pUpdate);
	}

	return bResult;
}

ELEMENT_API bool FoxjetCommon::IsDefineCmdEnabled (UINT nID)
{
	const UINT nLower = DefineMenu::nFirst;
	UINT nUpper = nLower + DefineMenu::nSize;

	if (::bValve) {
		DefineMenu::LPDEFINECMD p = DefineMenu::map [nID - DefineMenu::nFirst].m_lpfct;

		if (p == OnDefineSubelements ||
			p == OnDefineApplicationIdentifiers ||
			p == (DefineMenu::LPDEFINECMD)OnDefineBarcodeParams ||
			p == OnDefineLabelEditor)
		{
			return false;
		}
	}

	return (nID >= nLower && nID < nUpper);
}

ELEMENT_API CString FoxjetCommon::GetDefineMenuStr (UINT nID)
{
	CString str;

	if (IsDefineCmdEnabled (nID)) 
		str = LoadString (DefineMenu::map [nID - DefineMenu::nFirst].m_nMenuStrID);

	return str;
}

ELEMENT_API CString FoxjetCommon::GetDefineCmdStatusText (UINT nID)
{
	CString str;

	if (IsDefineCmdEnabled (nID)) 
		str = LoadString (DefineMenu::map [nID - DefineMenu::nFirst].m_nMenuStatusStrID);

	return str;
}

ELEMENT_API void FoxjetCommon::OnDefineCmd (UINT nID, CWnd * pParent, 
											const FoxjetCommon::CVersion & ver)
{
	if (IsDefineCmdEnabled (nID)) {
		int nIndex = nID - DefineMenu::nFirst;
		
		ASSERT (nIndex >= 0);
		ASSERT (nIndex < DefineMenu::nSize);
		ASSERT (DefineMenu::map [nIndex].m_lpfct);

		(* DefineMenu::map [nIndex].m_lpfct) (pParent, ver);
	}
	else { ASSERT (0); }
}

ELEMENT_API CRect FoxjetCommon::GetSplashTextRect ()
{
	// coords are relative to the upper-left corner
	// of the bitmap loaded in GetSplashBitmap (CBitmap &)
	int x = 10;
	int y = 80;
	int cx = 260;
	int cy = 150;
/*
	int x = 250;
	int y = 10;
	int cx = 180;
	int cy = 200;
*/
	return CRect (x, y, x + cx, y + cy);
}

ELEMENT_API bool FoxjetCommon::GetSplashBitmap (CBitmap & bmp)
{
	UINT nID = IDB_FOXJET;

	bool bResult = bmp.LoadBitmap (nID) ? true : false;
	ASSERT (bResult);
	return bResult;
}

ELEMENT_API CString FoxjetCommon::GetSplashText (const FoxjetCommon::CVersion & ver)
{
	return FoxjetCommon::GetAppTitle ();
}

ELEMENT_API bool FoxjetCommon::IsConfiningOn ()
{
	return ::bConfining;
}

ELEMENT_API bool FoxjetCommon::SetConfiningOn (bool bOn)
{
	::bConfining = bOn;
	return true;
}

ELEMENT_API bool FoxjetCommon::DoPromptDocName (bool bOpen, CString & strFile, 
												CWnd * pParent, bool & bReadOnly)
{
	ASSERT (0);
	return false;
/*
	CMessageDlg dlg (bOpen, FoxjetFile::GetFileTitle (strFile), bReadOnly, 
		&(ListGlobals::database), pParent);
	
	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetFilename () + _T (".") + 
			FoxjetFile::GetFileExt (FoxjetFile::MESSAGE);
		bReadOnly = dlg.GetReadOnly ();

		if (!bOpen) {  // saving
			MESSAGESTRUCT msg;
			CString strHead = FoxjetFile::GetFileHead (strFile);
			CString strMsg = FoxjetFile::GetFileTitle (strFile);
			COdbcDatabase & db = ListGlobals::database;

			if (GetMessageRecord (db, strMsg, strHead, msg)) {
				msg.m_strDesc = dlg.m_strDesc;
				return UpdateMessageRecord (db, msg);
			}
			else {
				msg.m_lHeadID = GetHeadIDByName (db, strHead);
				msg.m_strName = strMsg;
				msg.m_strDesc = dlg.m_strDesc;

				bool bAdd = AddMessageRecord (ListGlobals::database, msg);
				ASSERT (bAdd);
				return false; // doc mgr must save the new doc's m_strData
			}
		}
		else 
			// opening, only need to change strFile & bReadOnly.
			// doc mgr will do the rest
			return true; 
	}
	return false;
*/
}

ELEMENT_API bool FoxjetCommon::OnProperties (FoxjetDatabase::TASKSTRUCT & task, FoxjetDatabase::BOXSTRUCT & box, CWnd *pParent, bool bReadOnly)
{
	CPropertiesDlg dlg (pParent);

	dlg.m_bReadOnly		= bReadOnly;
	dlg.m_strName		= task.m_strName;
	dlg.m_strDesc		= task.m_strDesc;
	dlg.m_strDownload	= task.m_strDownload;
	dlg.m_lLineID		= task.m_lLineID;
	dlg.m_lBoxID		= box.m_lID;
	dlg.m_lTaskID		= task.m_lID;
	dlg.m_strNotes		= task.m_strNotes;
	
	for (int i = 0; i < task.m_vMsgs.GetSize (); i++) 
		dlg.m_vMsgs.Add (task.m_vMsgs [i].m_strData);

	if (dlg.DoModal () == IDOK) {
		ASSERT (!bReadOnly);

		task.m_strDesc		= dlg.m_strDesc;
		task.m_strDownload	= dlg.m_strDownload;
		task.m_strNotes		= dlg.m_strNotes;
		box.m_lID			= dlg.m_lBoxID;

		return true;
	}

	return false;
}

ELEMENT_API FoxjetCommon::CVersion FoxjetCommon::GetDocVersion (const CString & strDoc)
{
ASSERT ("strDoc should be the head's uid" == 0);

	CVersion version;
	COdbcDatabase & db = GetDB ();
	CString strSQL, strDocName = FoxjetFile::GetRelativeName (strDoc);
	CString strMessage = FoxjetFile::GetFileTitle (strDocName);
	CString strHead = FoxjetFile::GetFileHead (strDocName);
	ULONG lHeadID = GetHeadID (strHead);
	MESSAGESTRUCT msg;

	if (GetMessageRecord (db, strMessage, lHeadID, msg)) {
		CString str = msg.m_strData;
		CLongArray braces = CountChars (str, (TCHAR)'{');

		for (int i = 0; i < braces.GetSize (); i++) {
			CVersion ver;
			CString strVer = str.Mid (braces [i]);

			if (ver.FromString (strVer)) {
				version = ver;
				break;
			}
		}
	}

	return version;
}

UINT ForceUniqueID (const FoxjetCommon::CElementList & v, FoxjetCommon::CBaseElement & e)
{
	if (v.FindID (e.GetID ()) != -1)
		e.SetID (v.GetNextID ());

	return e.GetID ();
}

void ChangeConcatID (CMap <int, int, UINT, UINT> & concat,
					 const CMap <UINT, UINT, UINT, UINT> & id)
{
	for (POSITION pos = id.GetStartPosition(); pos != NULL; ) {
		UINT nOldID = -1;
		UINT nNewID = -1;

		id.GetNextAssoc (pos, nOldID, nNewID);
		ChangeConcatID (concat, nOldID, nNewID);
	}
}

bool ChangeConcatID (CMap <int, int, UINT, UINT> & map, UINT nOldID, UINT nNewID)
{
	for (POSITION pos = map.GetStartPosition(); pos != NULL; ) {
		UINT nConcatID = -1;
		int nIndex = -1;
		
		map.GetNextAssoc (pos, nIndex, nConcatID);

		if (nConcatID == nOldID) {
			map.SetAt (nIndex, nNewID);
			return true;
		}
	}

	return false;
}

ELEMENT_API bool FoxjetCommon::IsValidFileTitle (const CString & str)
{
	return FoxjetFile::IsValidFileTitle (str);
}
