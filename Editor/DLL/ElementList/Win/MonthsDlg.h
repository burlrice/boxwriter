#if !defined(AFX_MONTHSDLG_H__347092CB_4ADC_4B31_A6DC_9A7E0370B38A__INCLUDED_)
#define AFX_MONTHSDLG_H__347092CB_4ADC_4B31_A6DC_9A7E0370B38A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MonthsDlg.h : header file
//

#include "DaysDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CMonthsDlg dialog

class CMonthsDlg : public CDaysDlg
{
	DECLARE_DYNCREATE(CMonthsDlg)

protected:
	CMonthsDlg () : CDaysDlg () { } // only implemented for DYNCREATE

// Construction
public:
	CMonthsDlg(FoxjetDatabase::COdbcDatabase & db, const CodesDlg::CCodesArray & vCodes);
	~CMonthsDlg();

// Dialog Data
	//{{AFX_DATA(CMonthsDlg)
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CMonthsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual CodesDlg::CCodes GetCodes (ULONG lLineID) const;
	virtual void Update (ULONG lLineID, UINT nIndex, const CString & strValue);

	CodesDlg::CCodesArray m_vCodes;

	// Generated message map functions
	//{{AFX_MSG(CMonthsDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MONTHSDLG_H__347092CB_4ADC_4B31_A6DC_9A7E0370B38A__INCLUDED_)
