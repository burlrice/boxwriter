#if !defined(AFX_C39BCPARAMSDLG_H__685B7AA9_490F_4189_9CFC_34A8CD776465__INCLUDED_)
#define AFX_C39BCPARAMSDLG_H__685B7AA9_490F_4189_9CFC_34A8CD776465__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// C39BcParamsDlg.h : header file
//

#include "Resource.h"
#include "Code128ParamsDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CC39BcParamsDlg dialog

class CC39BcParamsDlg : public CCode128ParamsDlg
{
// Construction
public:
	CC39BcParamsDlg(FoxjetDatabase::HEADTYPE type, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CC39BcParamsDlg)
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CC39BcParamsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CC39BcParamsDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_C39BCPARAMSDLG_H__685B7AA9_490F_4189_9CFC_34A8CD776465__INCLUDED_)
