#pragma once

#include "DbWizard.h"
#include "CellCtrlImp.h"

// CDbWizardKeyFieldDlg

namespace FoxjetElements
{
	class ELEMENT_API CDbWizardKeyFieldDlg : public CDbWizardBasePage
	{
		DECLARE_DYNAMIC(CDbWizardKeyFieldDlg)

	public:
		CDbWizardKeyFieldDlg(CDbWizardDlg * pParent);
		virtual ~CDbWizardKeyFieldDlg();

		CString GetCurSelKeyField () const;
		void SetCurSelKeyField (const CString & str);

		CString m_strKeyField;
		CString m_strKeyValue;

	protected:
		CString m_strDSN;

		virtual BOOL OnSetActive();
		virtual BOOL OnKillActive();
		virtual bool CanAdvance ();

		afx_msg void OnCheckKeyField ();
		afx_msg void OnView ();
		afx_msg void OnFieldsChange ();
		
		DECLARE_MESSAGE_MAP()
	};
};

