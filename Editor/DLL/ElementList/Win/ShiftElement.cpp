// ShiftElement.cpp: implementation of the CShiftElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ShiftElement.h"
#include "List.h"
#include "ElementDefaults.h"
#include "Edit.h"
#include "Debug.h"
#include "Extern.h"
#include "Utils.h"
#include "DefineShiftDlg.h"
#include "Parse.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace DefineShiftDlg;
using namespace FoxjetCommon::ElementFields;

extern CArray <CShiftCode, CShiftCode &> vShiftcodes;
extern CDiagnosticCriticalSection csShift;

IMPLEMENT_DYNAMIC (CShiftElement, CTextElement);

CShiftElement::CShiftElement(const FoxjetDatabase::HEADSTRUCT & head)
: CTextElement (head)
{
}

CShiftElement::CShiftElement (const CShiftElement & rhs)
: CTextElement (rhs)
{
}

CShiftElement & CShiftElement::operator = (const CShiftElement & rhs)
{
	CTextElement::operator = (rhs);

	if (this != &rhs) {
	}

	return * this;
}

CShiftElement::~CShiftElement()
{
}

// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
static const ElementFields::FIELDSTRUCT fields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType,		_T ("")				),	// Prefix (required)
	ElementFields::FIELDSTRUCT (m_lpszID,				_T ("")				),	// ID (required)
	ElementFields::FIELDSTRUCT (m_lpszX,				_T ("")				),	// x (required)
	ElementFields::FIELDSTRUCT (m_lpszY,				_T ("")				),	// y (required)
	ElementFields::FIELDSTRUCT (m_lpszFlipH,			_T ("0")			),	// flip h
	ElementFields::FIELDSTRUCT (m_lpszFlipV,			_T ("0")			),	// flip v
	ElementFields::FIELDSTRUCT (m_lpszInverse,			_T ("0")			),	// inverse
	ElementFields::FIELDSTRUCT (m_lpszFontName,			_T ("Courier New")	),	// font name
	ElementFields::FIELDSTRUCT (m_lpszFontSize,			_T ("32")			),	// font size
	ElementFields::FIELDSTRUCT (m_lpszBold,				_T ("0")			),	// bold
	ElementFields::FIELDSTRUCT (m_lpszItalic,			_T ("0")			),	// italic
	ElementFields::FIELDSTRUCT (m_lpszOrient,			_T ("0")			),	// orientation
	ElementFields::FIELDSTRUCT (m_lpszWidth,			_T ("0")			),	// font width
	ElementFields::FIELDSTRUCT (m_lpszParaAlign,		_T ("0")			),	// paragraph alignment
	ElementFields::FIELDSTRUCT (m_lpszParaGap,			_T ("0")			),	// paragraph gap
	ElementFields::FIELDSTRUCT (m_lpszResizable,		_T ("0")			),	// resizable
	ElementFields::FIELDSTRUCT (m_lpszParaWidth,		_T ("0")			),	// paragraph width
	ElementFields::FIELDSTRUCT (m_lpszAlign,			_T ("0")			),	// alignment
	ElementFields::FIELDSTRUCT (m_lpszDither,			_T ("0")			),	// 
	ElementFields::FIELDSTRUCT (m_lpszColor,			_T ("0")			),	// 
	ElementFields::FIELDSTRUCT (),
};

CString CShiftElement::ToString (const FoxjetCommon::CVersion & ver) const
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated

	CString str;
	CPrinterFont f = GetFont ();
	CPoint pt = GetPosInternal (ALIGNMENT_ADJUST_ADD);

	str.Format (_T ("{Shift,%u,%u,%u,%d,%d,%d,%s,%u,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%06X}"),
		GetID (),
		pt.x, pt.y,
		IsFlippedH (),
		IsFlippedV (),
		IsInverse (),
		f.m_strName, f.m_nSize, f.m_bBold, f.m_bItalic,
		(int)GetOrientation (),
		GetWidth (),
		(int)GetParaAlign (),
		GetParaWidth (),
		IsResizable (),
		GetParaWidth (),
		GetAlignment (),
		GetDither (),
		GetColor ());

	return str;
}

bool CShiftElement::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
	CStringArray vstrToken;
	CPrinterFont f;

	Parse (str, ver, vstrToken);

	if (vstrToken [0].CompareNoCase (_T ("Shift")) != 0){
		TRACEF ("Wrong prefix \"" + vstrToken [0] + "\"");
		return false;
	}

	bool bResult = true;

	f.m_strName = vstrToken [7];
	f.m_nSize = (UINT)_tcstol (vstrToken [8], NULL, 10);
	f.m_bBold = _ttoi (vstrToken [9]) ? true : false;
	f.m_bItalic = _ttoi (vstrToken [10]) ? true : false;

	bResult &= SetID ((UINT)_tcstol (vstrToken [1], NULL, 10));
	bResult &= SetPos (CPoint (
		(UINT)_tcstol (vstrToken [2], NULL, 10), 
		(UINT)_tcstol (vstrToken [3], NULL, 10)), NULL);
	bResult &= SetFlippedH (_ttoi (vstrToken [4]) ? true : false);
	bResult &= SetFlippedV (_ttoi (vstrToken [5]) ? true : false);
	bResult &= SetInverse (_ttoi (vstrToken [6]) ? true : false);
	bResult &= SetFont (f);
	bResult &= SetOrientation ((TextElement::FONTORIENTATION)_ttoi (vstrToken [11]));
	bResult &= SetWidth (_ttoi (vstrToken [12]));
	bResult &= SetParaAlign ((ALIGNMENT)_ttoi (vstrToken [13]));
	bResult &= SetParaGap (_ttoi (vstrToken [14]));
	bResult &= SetResizable (_ttoi (vstrToken [15]) ? true : false);
	bResult &= SetParaWidth (_ttoi (vstrToken [16]));
	bResult &= SetAlignment ((ALIGNMENT)_ttoi (vstrToken [17]));
	bResult &= SetDither (_ttoi (vstrToken [18]));
	bResult &= SetColor (_tcstoul (vstrToken [19], NULL, 16));

	return bResult;
}

ULONG CShiftElement::TimeToLong (const COleDateTime & tm)
{
	return (tm.GetHour () * 10000) + (tm.GetMinute () * 100) + tm.GetSecond ();
}

int CShiftElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	LOCK (::csShift);
	ULONG lLineID = GetLineID ();

	#if __CUSTOM__ == __SW0877__
	lLineID = 0;
	#endif

	CShiftCode tmp;

	if (::vShiftcodes.GetSize ()) {
		for (int i = 0; i < ::vShiftcodes.GetSize (); i++) {
			if (::vShiftcodes [i].m_lLineID == lLineID) {
				CShiftCode & c = ::vShiftcodes [i];

				ULONG lShift1 = TimeToLong (c.m_dtShift1);
				ULONG lShift2 = TimeToLong (c.m_dtShift2);
				ULONG lShift3 = TimeToLong (c.m_dtShift3);
				ULONG lNow = TimeToLong (COleDateTime::GetCurrentTime ());

				if (lNow >= lShift1 && lNow < lShift2)
					m_strImageData = c.m_strShiftCode1;
				else if (lNow >= lShift2 && lNow < lShift3)
					m_strImageData = c.m_strShiftCode2;
				else
					m_strImageData = c.m_strShiftCode3;
			}
		}
	}

	/*
	C:\Dev\BoxWriter\Editor\DLL\ElementList\Win\ShiftElement.cpp(167): 1
	C:\Dev\BoxWriter\Editor\DLL\ElementList\Win\ShiftElement.cpp(168): 12/30/0/ 03:05:00
	C:\Dev\BoxWriter\Editor\DLL\ElementList\Win\ShiftElement.cpp(169): 12/30/0/ 06:30:00
	C:\Dev\BoxWriter\Editor\DLL\ElementList\Win\ShiftElement.cpp(170): 12/30/0/ 16:50:00
	C:\Dev\BoxWriter\Editor\DLL\ElementList\Win\ShiftElement.cpp(171): C
	C:\Dev\BoxWriter\Editor\DLL\ElementList\Win\ShiftElement.cpp(172): A
	C:\Dev\BoxWriter\Editor\DLL\ElementList\Win\ShiftElement.cpp(173): B

	c.m_lLineID = 1;
	c.m_dtShift1 = COleDateTime (2011, 12, 30, 3, 5, 0);
	c.m_dtShift2 = COleDateTime (2011, 12, 30, 6, 30, 0);
	c.m_dtShift3 = COleDateTime (2011, 12, 30, 16, 50, 0);
	c.m_strShiftCode1 = _T ("C");
	c.m_strShiftCode2 = _T ("A");
	c.m_strShiftCode3 = _T ("B");
	*/

	/*
	ULONG lShift1 = TimeToLong (c.m_dtShift1);
	ULONG lShift2 = TimeToLong (c.m_dtShift2);
	ULONG lShift3 = TimeToLong (c.m_dtShift3);
	ULONG lNow = TimeToLong (COleDateTime::GetCurrentTime ());

	if (lNow >= lShift1 && lNow < lShift2)
		m_strImageData = c.m_strShiftCode1;
	else if (lNow >= lShift2 && lNow < lShift3)
		m_strImageData = c.m_strShiftCode2;
	else
		m_strImageData = c.m_strShiftCode3;
	*/

	return GetClassID ();
}

const FoxjetCommon::ElementFields::FIELDSTRUCT * CShiftElement::GetFieldBuffer () const
{
	return ::fields;
}
