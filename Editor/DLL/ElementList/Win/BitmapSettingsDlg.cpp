// BitmapSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "BitmapSettingsDlg.h"
#include "Extern.h"
#include "Debug.h"
#include "Registry.h"
#include "BitmapElement.h"
#include "Database.h"
#include "Parse.h"
#include "NetworkDlg.h"
#include "LocalDbDlg.h"
#include "FileExt.h"

using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetElements;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBitmapSettingsDlg dialog


CBitmapSettingsDlg::CBitmapSettingsDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(CBitmapSettingsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBitmapSettingsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CBitmapSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBitmapSettingsDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBitmapSettingsDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CBitmapSettingsDlg)
	ON_BN_CLICKED(BTN_BROWSE_EDITOR, OnBrowseEditor)
	ON_BN_CLICKED(BTN_BROWSE_LOGOS, OnBrowseLogos)
	ON_BN_CLICKED(BTN_LOCATE, OnLocate)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(BTN_BROWSE_LOGOS_LOCAL, OnBrowseLogosLocal)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBitmapSettingsDlg message handlers

void CBitmapSettingsDlg::OnBrowseEditor() 
{
	CString strApp = GetBitmapEditorPath ();
	CFileDialog dlg (TRUE, _T ("*.exe"), strApp, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("Applications (*.exe)|*.exe|All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		strApp = dlg.GetPathName ();
		SetDlgItemText (TXT_BITMAPEDITORPATH, strApp);
	}
}

static CString ChangePath (const CString & strOldPath, const CString & strNewPath)
{
	CString str = strOldPath;
	int nIndex = str.ReverseFind ('\\');

	if (nIndex != -1) 
		str = strNewPath + str.Mid (nIndex);

	return str;
}

static void ChangeBmpPath (const CString & strOldPath, const CString & strNewPath, bool bCopyFiles)
{
	CMapStringToString vCopy;

	try 
	{
		COdbcDatabase & db = GetDB ();
		CArray <TASKSTRUCT, TASKSTRUCT &> vTasks;

		GetTaskRecords (db, FoxjetDatabase::ALL, vTasks);

		for (int nTask = 0; nTask < vTasks.GetSize (); nTask++) {
			TASKSTRUCT & task = vTasks [nTask];
			bool bUpdated = false;

			for (int nMsg = 0; nMsg < task.m_vMsgs.GetSize (); nMsg++) {
				MESSAGESTRUCT & msg = task.m_vMsgs [nMsg];
				CStringArray vParams;

				ParsePackets (msg.m_strData, vParams);

				for (int nParam = 0; nParam < vParams.GetSize (); nParam++) {
					CString strParam = _T ("{") +  vParams [nParam] + _T ("}");
					const int nIndex = 9;
					const CString strBMP = _T ("{Bitmap,");
					CLongArray v = CountChars (strParam, ',');

					if (strParam.Find (strBMP) != -1) {
						CStringArray v;

						Tokenize (strParam, v);

						CString strOld = UnformatString (v [nIndex]);
						CString strNew = ChangePath (strOld, strNewPath);

						v.SetAt (nIndex, strNew);

						TRACEF (strOld);
						TRACEF (strNew);
						TRACEF (strParam);
						TRACEF (ToString (v));

						VERIFY (msg.m_strData.Replace (strParam, ToString (v)));
						bUpdated = true;

						if (bCopyFiles) {
							vCopy.SetAt (strOld, strNew);
						}
					}
				}
			}

			if (bUpdated) {
				VERIFY (UpdateTaskRecord (db, task));
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	for (POSITION pos = vCopy.GetStartPosition (); pos; ) {
		CString strFrom, strTo;

		vCopy.GetNextAssoc (pos, strFrom, strTo);
		TRACEF (strFrom + _T (" --> ") + strTo);
		::CopyFile (strFrom, strTo, FALSE);
	}
}

void CBitmapSettingsDlg::OnOK() 
{
	CString strApp, strLocal;

	GetDlgItemText (TXT_BITMAPEDITORPATH, strApp);
	GetDlgItemText (TXT_LOGOPATH_LOCAL, strLocal);

	FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER,
		ListGlobals::defElements.m_strRegSection + _T ("\\Bitmap editor"), 
		_T ("Path"), strApp);
	FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER,
		ListGlobals::defElements.m_strRegSection + _T ("\\Settings"), _T ("LocalLogosFolder"), strLocal);

	if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, &defElements.GetElement (BMP))) {
		CString str = p->GetFilePath ();
		int nIndex = str.ReverseFind ('\\');

		if (nIndex != -1) {
			const CString strOldPath = str.Left (nIndex);
			CString strNewPath;

			GetDlgItemText (TXT_LOGOPATH, strNewPath);

			{
				CString str1 = strNewPath;
				CString str2 = strLocal;

				str1.MakeLower ();
				str2.MakeLower ();

				if (!(str1.Find (_T ("c:\\")) != -1 || str2.Find (_T ("c:\\")) != -1)) {
					if (str1.Find (str2) != -1 || str2.Find (str1) != -1) {
						MsgBox (LoadString (IDS_BADLOCALPATH));
						return;
					}
				}
			}

//#ifndef _DEBUG
			if (strOldPath.CompareNoCase (strNewPath) != 0) 
//#endif
			{
				if (str.Replace (strOldPath, strNewPath)) {
					CVersion ver;

					p->SetFilePath (str, GetDefaultHead ());
					
					if (p->GetFilePath ().CompareNoCase (str) != 0) {
						CString strDef = strNewPath + _T ("\\Def.bmp");
						HINSTANCE hInst = GetInstanceHandle ();
						HBITMAP hBmp = (HBITMAP)::LoadImage (hInst, MAKEINTRESOURCE (IDB_DEFBITMAP), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_MONOCHROME);

						ASSERT (hBmp);
						VERIFY (SaveBitmap (hBmp, strDef));
						::DeleteObject (hBmp);

						p->SetFilePath (strDef, GetDefaultHead ());
					}

					TRACEF (str);
					TRACEF (p->GetFilePath ());
					ASSERT (!p->GetFilePath ().CompareNoCase (str));

					defElements.SetElement (BMP, p->ToString (ver), ver);
					bool bCopy = MsgBox (LoadString (IDS_COPYBMPS), _T (""), MB_YESNO) == IDYES ? true : false;

					BeginWaitCursor ();
					ChangeBmpPath (strOldPath, strNewPath, bCopy);
					EndWaitCursor ();
				}
			}
		}
	}

	FoxjetCommon::CEliteDlg::OnOK();
}

BOOL CBitmapSettingsDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();
	CString str;
	CString strLocal = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER,
		ListGlobals::defElements.m_strRegSection + _T ("\\Settings"), _T ("LocalLogosFolder"), GetHomeDir () + _T ("\\Logos"));

	if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, &defElements.GetElement (BMP))) {
		str = p->GetFilePath ();
		int nIndex = str.ReverseFind ('\\');

		if (nIndex != -1)
			str = str.Left (nIndex);
	}
	
	SetDlgItemText (TXT_BITMAPEDITORPATH, GetBitmapEditorPath ());
	SetDlgItemText (TXT_LOGOPATH, str);
	SetDlgItemText (TXT_LOGOPATH_LOCAL, strLocal);

	UpdateUI ();

	{
		UINT n [] = 
		{
			LBL_LOGOPATH_LOCAL,
			TXT_LOGOPATH_LOCAL,
			BTN_BROWSE_LOGOS_LOCAL,
		};
		bool bLocalDB = false;

		if (IsNetworked ()) {
			FoxjetUtils::CLocalDbDlg dlg;

			dlg.Load (GetDB ());

			bLocalDB = dlg.m_bEnabled ? true : false;
		}

		for (int i = 0; i < ARRAYSIZE (n); i++) 
			if (CWnd * p = GetDlgItem (n [i]))
				p->ShowWindow (bLocalDB ? SW_SHOW : SW_HIDE);

		if (bLocalDB)
			SetDlgItemText (LBL_LOGOPATH, LoadString (IDS_NETWORKBITMAPFOLDER));
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

static TCHAR szBrowse [MAX_PATH] = { 0 };

static int CALLBACK BrowseCallbackProc(HWND hwnd,UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	// If the BFFM_INITIALIZED message is received
	// set the path to the start path.
	switch (uMsg)
	{
		case BFFM_INITIALIZED:
		{
			if (NULL != lpData)
			{
				SendMessage(hwnd, BFFM_SETSELECTION, TRUE, (LPARAM)::szBrowse);
			}
		}
	}

	return 0; // The function should always return 0.
}

void CBitmapSettingsDlg::OnBrowseLogos() 
{
	#define BIF_NEWDIALOGSTYLE (0x00000040)

	BROWSEINFO info;
	TCHAR szTitle [MAX_PATH] = { 0 };

	ZeroMemory (&info, sizeof (info));
	_stprintf (szTitle, _T ("%s"), LoadString (IDS_LOGOFOLDER));
	ZeroMemory (::szBrowse, ARRAYSIZE (::szBrowse));
	GetDlgItemText (TXT_LOGOPATH, ::szBrowse, ARRAYSIZE (::szBrowse));

    info.hwndOwner		= m_hWnd; 
    info.pidlRoot		= NULL; 
    info.pszDisplayName	= ::szBrowse; 
    info.lpszTitle		= szTitle; 
	info.lParam			= (LPARAM)::szBrowse;
	info.lpfn			= BrowseCallbackProc;
	info.ulFlags        = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;

	if (LPITEMIDLIST lpItem = ::SHBrowseForFolder (&info)) {
		::SHGetPathFromIDList(lpItem, ::szBrowse);
		SetDlgItemText (TXT_LOGOPATH, ::szBrowse);
		UpdateUI ();
	}
}

void CBitmapSettingsDlg::OnBrowseLogosLocal() 
{
	#define BIF_NEWDIALOGSTYLE (0x00000040)

	BROWSEINFO info;
	TCHAR szTitle [MAX_PATH] = { 0 };

	ZeroMemory (&info, sizeof (info));
	_stprintf (szTitle, _T ("%s"), LoadString (IDS_LOGOFOLDER));
	ZeroMemory (::szBrowse, ARRAYSIZE (::szBrowse));
	GetDlgItemText (TXT_LOGOPATH_LOCAL, ::szBrowse, ARRAYSIZE (::szBrowse));

    info.hwndOwner		= m_hWnd; 
    info.pidlRoot		= NULL; 
    info.pszDisplayName	= ::szBrowse; 
    info.lpszTitle		= szTitle; 
	info.lParam			= (LPARAM)::szBrowse;
	info.lpfn			= BrowseCallbackProc;
	info.ulFlags        = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;

	if (LPITEMIDLIST lpItem = ::SHBrowseForFolder (&info)) {
		::SHGetPathFromIDList(lpItem, ::szBrowse);
		SetDlgItemText (TXT_LOGOPATH_LOCAL, ::szBrowse);
		UpdateUI ();
	}
}

void CBitmapSettingsDlg::UpdateUI () 
{
	CString strDefault, strCurrent;

	GetDlgItemText (TXT_LOGOPATH, strCurrent);

	if (CBitmapElement * p = DYNAMIC_DOWNCAST (CBitmapElement, &defElements.GetElement (BMP))) {
		strDefault = p->GetFilePath ();
		int nIndex = strDefault.ReverseFind ('\\');

		if (nIndex != -1)
			strDefault = strDefault.Left (nIndex);
	}

	TRACEF (strDefault);
	TRACEF (strCurrent);

	bool bShow = strDefault.CompareNoCase (strCurrent) != 0 ? true : false;
	int n [] = 
	{
		LBL_WARNING,
		BMP_WARNING
	};

	for (int i = 0; i < ARRAYSIZE (n); i++) 
		if (CWnd * p = GetDlgItem (n [i]))
			p->ShowWindow (bShow ? SW_SHOW : SW_HIDE);
}

void CBitmapSettingsDlg::OnLocate() 
{
	CMapStringToString vFile, vPath;

	BeginWaitCursor ();

	try 
	{
		COdbcDatabase & db = GetDB ();
		CArray <TASKSTRUCT, TASKSTRUCT &> vTasks;

		GetTaskRecords (db, FoxjetDatabase::ALL, vTasks);

		for (int nTask = 0; nTask < vTasks.GetSize (); nTask++) {
			TASKSTRUCT & task = vTasks [nTask];
			bool bUpdated = false;

			for (int nMsg = 0; nMsg < task.m_vMsgs.GetSize (); nMsg++) {
				MESSAGESTRUCT & msg = task.m_vMsgs [nMsg];
				CStringArray vParams;

				ParsePackets (msg.m_strData, vParams);

				for (int nParam = 0; nParam < vParams.GetSize (); nParam++) {
					CString strParam = _T ("{") +  vParams [nParam] + _T ("}");
					const int nIndex = 9;
					const CString strBMP = _T ("{Bitmap,");
					CLongArray v = CountChars (strParam, ',');

					if (strParam.Find (strBMP) != -1) {
						CStringArray v;

						Tokenize (strParam, v);

						CString strFile = UnformatString (v [nIndex]);
						CString strPath = FoxjetFile::GetPath (strFile);

						{
							CString str;
							vFile.Lookup (strFile, str);
							str = ToString (_ttoi (str) + 1);
							vFile.SetAt (strFile, str);
						}

						{
							CString str;
							vPath.Lookup (strPath, str);
							str = ToString (_ttoi (str) + 1);
							vPath.SetAt (strPath, str);
						}
					}
				}
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	const CString strFile = GetHomeDir () + _T ("\\BitmapStats.txt");

	if (FILE * fp = _tfopen (strFile, _T ("wb"))) {
		for (POSITION pos = vPath.GetStartPosition (); pos; ) {
			CString strFile, strCount;

			vPath.GetNextAssoc (pos, strFile, strCount);

			CString str = strFile + _T (", ") + strCount + _T ("\r\n");
			fwrite ((LPVOID)(LPCTSTR)str, str.GetLength () * sizeof (TCHAR), 1, fp);
		}

		CString str = _T ("\r\n\r\n");
		fwrite ((LPVOID)(LPCTSTR)str, str.GetLength () * sizeof (TCHAR), 1, fp);

		for (POSITION pos = vFile.GetStartPosition (); pos; ) {
			CString strFile, strCount;

			vFile.GetNextAssoc (pos, strFile, strCount);

			CString str = strFile + _T (", ") + strCount + _T ("\r\n");
			fwrite ((LPVOID)(LPCTSTR)str, str.GetLength () * sizeof (TCHAR), 1, fp);
		}

		fclose (fp);

		{
			CString strCmdLine = _T ("notepad.exe  \"") + strFile + _T ("\"");
			STARTUPINFO si;     
			PROCESS_INFORMATION pi; 
			TCHAR szCmdLine [0xFF];

			_tcsncpy (szCmdLine, strCmdLine, 0xFF);
			memset (&pi, 0, sizeof(pi));
			memset (&si, 0, sizeof(si));
			si.cb = sizeof(si);     
			si.dwFlags = STARTF_USESHOWWINDOW;     
			si.wShowWindow = SW_SHOW;     

			TRACEF (szCmdLine);

			if (!::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
				MessageBox (LoadString (IDS_WRITTENTO) + strFile);
		}
	}

	EndWaitCursor ();
}
