#if !defined(AFX_BITMAPDLG_H__471C7424_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_BITMAPDLG_H__471C7424_8B3D_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BitmapDlg.h : header file
//

#include "ElementDlg.h"
#include "resource.h"
#include "BitmapElement.h"

/////////////////////////////////////////////////////////////////////////////
// CBitmapDlg dialog

class CBitmapDlg : public CElementDlg
{
// Construction
public:
	CBitmapDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBitmapDlg)
	CString	m_strFilePath;
	FoxjetElements::CBitmapElement::DATABASESTRUCT m_db;
	BOOL m_bUserPrompted;
	CString m_strUserPrompt;
	BOOL m_bPromptAtTaskStart;
	BOOL m_bLockAspectRatio;
	//}}AFX_DATA

	CSize m_size;
	bool m_bResizable;

	void Get (const FoxjetElements::CBitmapElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);
	void Set (FoxjetElements::CBitmapElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBitmapDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void UpdateUI(UINT nID = 0);
	virtual void OnCancel();
	virtual bool GetValues ();
	static ULONG CALLBACK WaitForEdit (LPVOID lpData);
	void ReloadBitmap ();
	void SetSize (const CString & strFilepath);
	
	DWORD m_dwProcessId;
	HBITMAP m_hbmpOriginal;
	bool m_bEdit;

	// Generated message map functions
	//{{AFX_MSG(CBitmapDlg)
	afx_msg void OnBrowse();
	virtual BOOL OnInitDialog();
	afx_msg void OnEdit();
	//}}AFX_MSG
	afx_msg void OnDatabaseClick ();
	afx_msg void OnEnableDatabaseClick ();
	afx_msg void OnUserPrompt ();
	afx_msg void OnWidthChange();
	afx_msg void OnHeightChange();

	virtual int BuildElement ();
	const FoxjetElements::CBitmapElement & GetElement () const;
	FoxjetElements::CBitmapElement & GetElement ();
	
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BITMAPDLG_H__471C7424_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
