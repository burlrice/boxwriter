#include "stdafx.h"

#include <math.h>

#include "BarcodeElement.h"
#include "Resource.h"
#include "CopyArray.h"
#include "List.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"
#include "Utils.h"
#include "TemplExt.h"
#include "BarcodeParams.h"
#include "BarcodeDlg.h"
#include "Extern.h"
#include "Coord.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace Color;
using namespace FoxjetElements;

namespace Code93 
{
	class CPattern
	{
	public:
		CPattern (
			int nIndex = -1, TCHAR c = NULL,
			int nBar1 = 0, int nSpace1 = 0,
			int nBar2 = 0, int nSpace2 = 0,
			int nBar3 = 0, int nSpace3 = 0);
		CPattern (const CPattern & rhs);
		CPattern & operator = (const CPattern & rhs);
		virtual ~CPattern ();

		int m_nIndex;
		TCHAR m_c;
		int m_nBar1;
		int m_nBar2;
		int m_nBar3;
		int m_nSpace1;
		int m_nSpace2;
		int m_nSpace3;

		CString ToString () const;
	};

	bool IsValidData (const CString & str);
	CString CalcChecksum (const CString & str);
	CString Format (const CString & str);

	bool operator == (const CPattern & lhs, const CPattern & rhs);
	bool operator != (const CPattern & lhs, const CPattern & rhs);

}; //namespace Code93

using namespace Code93;

////////////////////////////////////////////////////////////////////////////////

CPattern::CPattern (int nIndex, TCHAR c,
					int nBar1, int nSpace1,
					int nBar2, int nSpace2,
					int nBar3, int nSpace3)
:	m_nIndex	(nIndex),
	m_c			(c),
	m_nBar1		(nBar1),
	m_nBar2		(nBar2),
	m_nBar3		(nBar3),
	m_nSpace1	(nSpace1),
	m_nSpace2	(nSpace2),
	m_nSpace3	(nSpace3)
{
}

CPattern::CPattern (const CPattern & rhs)
:	m_nIndex	(rhs.m_nIndex),
	m_c			(rhs.m_c),
	m_nBar1		(rhs.m_nBar1),
	m_nBar2		(rhs.m_nBar2),
	m_nBar3		(rhs.m_nBar3),
	m_nSpace1	(rhs.m_nSpace1),
	m_nSpace2	(rhs.m_nSpace2),
	m_nSpace3	(rhs.m_nSpace3)
{
}

CPattern & CPattern::operator = (const CPattern & rhs)
{
	if (this != &rhs) {
		m_nIndex	= rhs.m_nIndex;
		m_c			= rhs.m_c;
		m_nBar1		= rhs.m_nBar1;
		m_nBar2		= rhs.m_nBar2;
		m_nBar3		= rhs.m_nBar3;
		m_nSpace1	= rhs.m_nSpace1;
		m_nSpace2	= rhs.m_nSpace2;
		m_nSpace3	= rhs.m_nSpace3;
	}

	return * this;
}

CPattern::~CPattern ()
{
}

CString CPattern::ToString () const
{
	CString str;
	int n [] = 
	{
		m_nBar1,
		m_nSpace1,

		m_nBar2,
		m_nSpace2,

		m_nBar3,
		m_nSpace3,
	};

	str.Format (_T ("%-2d [%c (%-2d)]: "),
		m_nIndex, 
		_istprint (m_c) ? m_c : ' ',
		m_c);

	for (int i = 0; i < ARRAYSIZE (n); i++) {
		LPCTSTR lpsz = (i % 2) == 0 ? _T ("1") : _T ("0");
	
		switch (n [i]) {
		case 4:		str += lpsz;
		case 3:		str += lpsz;
		case 2:		str += lpsz;
		case 1:		str += lpsz;
			break;
		}
	}

	return str;
}

////////////////////////////////////////////////////////////////////////////////

static const CPattern map [48] = 
{
	//						b	s	b	s	b	s
	CPattern (0,  	'0',	1,  3,	1,  1,	1,  2),	// 100010100  	
	CPattern (1,	'1', 	1,  1,	1,  2,	1,  3),	// 101001000 	
	CPattern (2, 	'2', 	1,  1,	1,  3,	1,  2),	// 101000100 	
	CPattern (3, 	'3', 	1,  1,	1,  4,	1,  1),	// 101000010 	
	CPattern (4, 	'4', 	1,  2,	1,  1,	1,  3),	// 100101000 	
	CPattern (5, 	'5', 	1,  2,	1,  2,	1,  2),	// 100100100 	
	CPattern (6, 	'6', 	1,  2,	1,  3,	1,  1),	// 100100010 	
	CPattern (7, 	'7', 	1,  1,	1,  1,	1,  4),	// 101010000 	
	CPattern (8, 	'8', 	1,  3,	1,  2,	1,  1),	// 100010010 	
	CPattern (9, 	'9', 	1,  4,	1,  1,	1,  1),	// 100001010 	
	CPattern (10, 	'A', 	2,  1,	1,  1,	1,  3),	// 110101000 	
	CPattern (11, 	'B', 	2,  1,	1,  2,	1,  2),	// 110100100 	
	CPattern (12, 	'C', 	2,  1,	1,  3,	1,  1),	// 110100010 	
	CPattern (13, 	'D', 	2,  2,	1,  1,	1,  2),	// 110010100 	
	CPattern (14, 	'E', 	2,  2,	1,  2,	1,  1),	// 110010010 	
	CPattern (15, 	'F', 	2,  3,	1,  1,	1,  1),	// 110001010 	
	CPattern (16, 	'G', 	1,  1,	2,  1,	1,  3),	// 101101000 	
	CPattern (17, 	'H', 	1,  1,	2,  2,	1,  2),	// 101100100 	
	CPattern (18, 	'I', 	1,  1,	2,  3,	1,  1),	// 101100010 	
	CPattern (19, 	'J', 	1,  2,	2,  1,	1,  2),	// 100110100 	
	CPattern (20, 	'K', 	1,  3,	2,  1,	1,  1),	// 100011010 	
	CPattern (21, 	'L', 	1,  1,	1,  1,	2,  3),	// 101011000 	
	CPattern (22, 	'M', 	1,  1,	1,  2,	2,  2),	// 101001100 	
	CPattern (23, 	'N', 	1,  1,	1,  3,	2,  1),	// 101000110 	
	CPattern (24, 	'O',  	1,  2,	1,  1,	2,  2),	// 100101100
	CPattern (25, 	'P', 	1,  3,	1,  1,	2,  1),	// 100010110
	CPattern (26, 	'Q', 	2,  1,	2,  1,	1,  2),	// 110110100
	CPattern (27, 	'R', 	2,  1,	2,  2,	1,  1),	// 110110010
	CPattern (28, 	'S', 	2,  1,	1,  1,	2,  2),	// 110101100
	CPattern (29, 	'T', 	2,  1,	1,  2,	2,  1),	// 110100110
	CPattern (30, 	'U', 	2,  2,	1,  1,	2,  1),	// 110010110
	CPattern (31, 	'V', 	2,  2,	2,  1,	1,  1),	// 110011010
	CPattern (32, 	'W', 	1,  1,	2,  1,	2,  2),	// 101101100
	CPattern (33, 	'X', 	1,  1,	2,  2,	2,  1),	// 101100110
	CPattern (34, 	'Y', 	1,  2,	2,  1,	2,  1),	// 100110110
	CPattern (35, 	'Z', 	1,  2,	3,  1,	1,  1),	// 100111010
	CPattern (36, 	'-', 	1,  2,	1,  1,	3,  1),	// 100101110
	CPattern (37, 	'.', 	3,  1,	1,  1,	1,  2),	// 111010100
	CPattern (38, 	' ', 	3,  1,	1,  2,	1,  1),	// 111010010
	CPattern (39, 	'$', 	3,  2,	1,  1,	1,  1),	// 111001010
	CPattern (40, 	'/', 	1,  1,	2,  1,	3,  1),	// 101101110
	CPattern (41, 	'+',	1,  1,	3,  1,	2,  1),	// 101110110
	CPattern (42, 	'%', 	2,  1,	1,  1,	3,  1),	// 110101110
	CPattern (43, 	DC1, 	1,  2,	1,  2,	2,  1),	// 100100110	// ($) // 43
	CPattern (44, 	DC2, 	3,  1,	2,  1,	1,  1),	// 111011010	// (%) // 44
	CPattern (45, 	DC3, 	3,  1,	1,  1,	2,  1),	// 111010110	// (/) // 45
	CPattern (46, 	DC4, 	1,  2,	2,  2,	1,  1),	// 100110010	// (+) // 46
	CPattern (-1, 	'*', 	1,  1,	1,  1,	4,  1),	// 101011110	
};

static const int nLookupTable [256] =
{
	-1, -1, -1, -1, -1, -1, -1, -1,  	// [0	- 7]
	-1, -1, -1, -1, -1, -1, -1, -1,  	// [8	- 15] 
	-1, -1, -1, -1, -1, -1, -1, -1,  	// [16	- 23] 
	-1, -1, -1, -1, -1, -1, -1, -1,  	// [24	- 31] 
	38, -1, -1, -1, 39, 42, -1, -1,  	// [32	- 39] 
	-1, -1, 47, 41, -1, 36, 37, 40,  	// [40	- 47] 
	0 , 1 , 2 , 3 , 4 , 5 , 6 , 7 ,  	// [48	- 55] 
	8 , 9 , -1, -1, -1, -1, -1, -1,  	// [56	- 63] 
	-1, 10, 11, 12, 13, 14, 15, 16,  	// [64	- 71] 
	17, 18, 19, 20, 21, 22, 23, 24,  	// [72	- 79] 
	25, 26, 27, 28, 29, 30, 31, 32,  	// [80	- 87] 
	33, 34, 35, -1, -1, -1, -1, -1,  	// [88	- 95] 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
	-1, -1, -1, -1, -1, -1, -1, -1, 
};

static void GenerateLookupTable ()
{ 
	TRACEF (_T (""));

	for (int c = 0; c < 256; c++) {
		CString strTmp;
		int nIndex = -1;

		for (int i = 0; i < ARRAYSIZE (::map); i++) {
			if (::map [i].m_c == c) {
				nIndex = i;
				break;
			}
		}

		strTmp.Format (_T ("%-2d, "), nIndex); 
		::OutputDebugString (strTmp);

		if (((c + 1) % 8) == 0)
			::OutputDebugString (_T ("\n"));
	}

	::OutputDebugString (_T ("\n"));
}

////////////////////////////////////////////////////////////////////////////////

CString Code93::Format (const CString & strAscii)
{
	CString str;
	TCHAR cMap [][3] = 
	{
		{ DC2, 'U', '\0', }, 	// NL	
		{ DC1, 'A', '\0', }, 	// SH	
		{ DC1, 'B', '\0', }, 	// SX	
		{ DC1, 'C', '\0', }, 	// EX	
		{ DC1, 'D', '\0', }, 	// ET	
		{ DC1, 'E', '\0', }, 	// EQ	
		{ DC1, 'F', '\0', }, 	// AK	
		{ DC1, 'G', '\0', }, 	// BL	
		{ DC1, 'H', '\0', }, 	// BS	
		{ DC1, 'I', '\0', }, 	// HT	
		{ DC1, 'J', '\0', }, 	// LF	
		{ DC1, 'K', '\0', }, 	// VT	
		{ DC1, 'L', '\0', }, 	// FF	
		{ DC1, 'M', '\0', }, 	// CR	
		{ DC1, 'N', '\0', }, 	// SO	
		{ DC1, 'O', '\0', }, 	// SI	
		{ DC1, 'P', '\0', }, 	// DL	
		{ DC1, 'Q', '\0', }, 	// D1	
		{ DC1, 'R', '\0', }, 	// D2	
		{ DC1, 'S', '\0', }, 	// D3	
		{ DC1, 'T', '\0', }, 	// D4	
		{ DC1, 'U', '\0', }, 	// NK	
		{ DC1, 'V', '\0', }, 	// SY	
		{ DC1, 'W', '\0', }, 	// EB	
		{ DC1, 'X', '\0', }, 	// CN	
		{ DC1, 'Y', '\0', }, 	// EM	
		{ DC1, 'Z', '\0', }, 	// SB	
		{ DC2, 'A', '\0', }, 	// EC	
		{ DC2, 'B', '\0', }, 	// FS	
		{ DC2, 'C', '\0', },	// GS	
		{ DC2, 'D', '\0', },	// RS	
		{ DC2, 'E', '\0', },	// US	
		{ ' ', '\0', '\0', },	// Space  
		{ DC3, 'A', '\0', }, 	// !	    
		{ DC3, 'B', '\0', }, 	// "	    
		{ DC3, 'C', '\0', }, 	// #	    
		{ DC3, 'D', '\0', }, 	// $	
		{ DC3, 'E', '\0', }, 	// %	
		{ DC3, 'F', '\0', }, 	// &	    
		{ DC3, 'G', '\0', }, 	// '	    
		{ DC3, 'H', '\0', }, 	// (	    
		{ DC3, 'I', '\0', }, 	// )	     
		{ DC3, 'J', '\0', }, 	// *	     
		{ DC3, 'K', '\0', }, 	// +	 
		{ DC3, 'L', '\0', },	// ,	    
		{ DC3, 'M', '\0', }, 	// -	
		{ DC3, 'N', '\0', }, 	// .	
		{ DC3, 'O', '\0', }, 	// /	
		{ DC3, 'P', '\0', }, 	// 0	
		{ DC3, 'Q', '\0', }, 	// 1	
		{ DC3, 'R', '\0', }, 	// 2	
		{ DC3, 'S', '\0', }, 	// 3	
		{ DC3, 'T', '\0', }, 	// 4	
		{ DC3, 'U', '\0', }, 	// 5	
		{ DC3, 'V', '\0', }, 	// 6	
		{ DC3, 'W', '\0', }, 	// 7	
		{ DC3, 'X', '\0', }, 	// 8	
		{ DC3, 'Y', '\0', }, 	// 9	
		{ DC3, 'Z', '\0', }, 	// :	    
		{ DC2, 'F', '\0', }, 	// ;	    
		{ DC2, 'G', '\0', }, 	// <	    
		{ DC2, 'H', '\0', }, 	// =	   
		{ DC2, 'I', '\0', }, 	// >	   
		{ DC2, 'J', '\0', }, 	// ?	   
		{ DC2, 'V', '\0', }, 	// @	
		{ 'A', '\0', '\0', },	// A	 
		{ 'B', '\0', '\0', }, 	// B       
		{ 'C', '\0', '\0', }, 	// C       
		{ 'D', '\0', '\0', }, 	// D       
		{ 'E', '\0', '\0', }, 	// E       
		{ 'F', '\0', '\0', }, 	// F       
		{ 'G', '\0', '\0', }, 	// G       
		{ 'H', '\0', '\0', }, 	// H       
		{ 'I', '\0', '\0', }, 	// I       
		{ 'J', '\0', '\0', }, 	// J       
		{ 'K', '\0', '\0', }, 	// K       
		{ 'L', '\0', '\0', }, 	// L       
		{ 'M', '\0', '\0', }, 	// M       
		{ 'N', '\0', '\0', }, 	// N       
		{ 'O', '\0', '\0', }, 	// O       
		{ 'P', '\0', '\0', }, 	// P       
		{ 'Q', '\0', '\0', }, 	// Q       
		{ 'R', '\0', '\0', }, 	// R       
		{ 'S', '\0', '\0', }, 	// S       
		{ 'T', '\0', '\0', }, 	// T        
		{ 'U', '\0', '\0', }, 	// U        
		{ 'V', '\0', '\0', }, 	// V       
		{ 'W', '\0', '\0', }, 	// W       
		{ 'X', '\0', '\0', }, 	// X       
		{ 'Y', '\0', '\0', }, 	// Y       
		{ 'Z', '\0', '\0', },	// Z       
		{ DC2, 'K', '\0', }, 	// [      
		{ DC2, 'L', '\0', }, 	// \      
		{ DC2, 'M', '\0', }, 	// ]      
		{ DC2, 'N', '\0', }, 	// ^      
		{ DC2, 'O', '\0', }, 	// _      
		{ DC2, 'W', '\0', }, 	// `       
		{ DC4, 'A', '\0', }, 	// a       
		{ DC4, 'B', '\0', }, 	// b       
		{ DC4, 'C', '\0', }, 	// c       
		{ DC4, 'D', '\0', }, 	// d       
		{ DC4, 'E', '\0', }, 	// e       
		{ DC4, 'F', '\0', }, 	// f       
		{ DC4, 'G', '\0', }, 	// g       
		{ DC4, 'H', '\0', }, 	// h       
		{ DC4, 'I', '\0', }, 	// i      
		{ DC4, 'J', '\0', }, 	// j      
		{ DC4, 'K', '\0', }, 	// k      
		{ DC4, 'L', '\0', }, 	// l      
		{ DC4, 'M', '\0', }, 	// m       
		{ DC4, 'N', '\0', }, 	// n       
		{ DC4, 'O', '\0', }, 	// o       
		{ DC4, 'P', '\0', }, 	// p      
		{ DC4, 'Q', '\0', }, 	// q      
		{ DC4, 'R', '\0', }, 	// r      
		{ DC4, 'S', '\0', }, 	// s      
		{ DC4, 'T', '\0', }, 	// t      
		{ DC4, 'U', '\0', }, 	// u      
		{ DC4, 'V', '\0', }, 	// v      
		{ DC4, 'W', '\0', }, 	// w      
		{ DC4, 'X', '\0', }, 	// x      
		{ DC4, 'Y', '\0', }, 	// y      
		{ DC4, 'Z', '\0', }, 	// z      
		{ DC2, 'P', '\0', }, 	// {      
		{ DC2, 'Q', '\0', }, 	// |	   
		{ DC2, 'R', '\0', }, 	// }      
		{ DC2, 'S', '\0', }, 	// ~      
		{ DC2, 'T', '\0', },	// DEL    
	}; 
	// ($) // 43 // DC1
	// (%) // 44 // DC2
	// (/) // 45 // DC3
	// (+) // 46 // DC4
	// NOTE: The character pairs (%)X, (%)Y, (%)Z will decode as ASCII DEL. 


	for (int i = 0; i < strAscii.GetLength (); i++) {
		int nIndex = strAscii [i];

		if (::nLookupTable [nIndex] == -1) 
			str += cMap [nIndex];
		else
			str += strAscii [i];
	}

	return str;
}

bool Code93::IsValidData (const CString & str)
{
	for (int i = 0; i < str.GetLength (); i++) {
		int nIndex = str [i];

		switch (nIndex) {
		case DC1: // special chars, but legal
		case DC2:
		case DC3:
		case DC4:
			break;

		default:
			if (::nLookupTable [nIndex] == -1)
				return false; 

			break;
		}
	}

	return true;
}

CString Code93::CalcChecksum (const CString & strEncode)
{
	CString str, strData (strEncode);
	int nSum = 0;

	for (int i = 0; i < strData.GetLength (); i++) {
		int nIndex = strData [i];
		int nValue = ::map [::nLookupTable [nIndex]].m_nIndex;
		int nWeight = ((strData.GetLength () - (i + 1)) % 20) + 1;

		nSum += (nValue * nWeight);

		//{ CString strDebug; strDebug.Format ("%c [%d]: %d * %d", strData [i], strData [i], nValue, nWeight); TRACEF (strDebug); }
	}

	str += ::map [nSum % 47].m_c;
	strData += str;
	nSum = 0;

	for (int i = 0; i < strData.GetLength (); i++) {
		int nIndex = strData [i];
		int nValue = ::map [::nLookupTable [nIndex]].m_nIndex;
		int nWeight = ((strData.GetLength () - (i + 1)) % 15) + 1;

		nSum += (nValue * nWeight);
		
		//{ CString strDebug; strDebug.Format ("%c [%d]: %d * %d", strData [i], strData [i], nValue, nWeight); TRACEF (strDebug); }
	}

	str += ::map [nSum % 47].m_c;

	return str;
}

bool Code93::operator == (const CPattern & lhs, const CPattern & rhs)
{
	return lhs.m_c == rhs.m_c;
}

bool Code93::operator != (const CPattern & lhs, const CPattern & rhs)
{
	return !(lhs == rhs);
}

////////////////////////////////////////////////////////////////////////////////

int CBarcodeElement::DrawC93Symbol (const CString & str, const CString & strCaption, const CBarcodeParams & params, 
									const FoxjetDatabase::HEADSTRUCT & head, LPBYTE pBuffer, 
									UINT nBufferWidth, ULONG lBufferSize, CDC * pDC, 
									CBitmap * pbmpCaption,
									FoxjetCommon::DRAWCONTEXT context,
									CString * pstrFormattedCaption)
{
	int nWidth = 0;
	CString strData = Code93::Format (str);
	CArray <CPattern, CPattern &> v;
	int nQuietzone = CBaseElement::ThousandthsToLogical (CPoint (params.GetQuietZone (), 0), head).x;
	CSize bearer = CBaseElement::ThousandthsToLogical (CPoint (params.GetVertBearer (), params.GetHorzBearer ()), head);
	double dStretch [2] = { 0, 0 };
	const CPattern terminator (-1, NULL, 1);

	CBaseElement::CalcStretch (head, dStretch);
	bearer.cy = (int)((double)bearer.cy / dStretch [1]);

	nQuietzone = StretchToPrinterRes (nQuietzone, head);
	bearer.cx = StretchToPrinterRes (bearer.cx, head);

	if (Code93::IsValidData (strData)) {
		const int nBarHeight		= min ((int)head.GetChannels (), params.GetBarHeight ()); //params.GetBarHeight ();
		const BYTE nBarMask			= GetBarMask (nBarHeight);
		const BYTE nNotBarMask		= ~nBarMask;

		strData += Code93::CalcChecksum (strData);

		v.Add (CPattern (::map [::nLookupTable ['*']]));

		for (int i = 0; i < strData.GetLength (); i++) 
			v.Add (CPattern (::map [::nLookupTable [strData [i]]]));

		v.Add (CPattern (::map [::nLookupTable ['*']]));
		v.Add (CPattern (terminator));

		nWidth += nQuietzone + bearer.cx; 

		for (int i = 0; i < v.GetSize (); i++) {
			CPattern p = v [i];
			struct
			{
				int		m_nWidth;
				bool	m_bBar;
			}
			bars [] =
			{
				{ params.GetBarPixels	(p.m_nBar1),		true,	},
				{ params.GetSpacePixels	(p.m_nSpace1),		false,	},

				{ params.GetBarPixels	(p.m_nBar2),		true,	},
				{ params.GetSpacePixels	(p.m_nSpace2),		false,	},

				{ params.GetBarPixels	(p.m_nBar3),		true,	},
				{ params.GetSpacePixels	(p.m_nSpace3),		false,	},
			};

			if (p == terminator) 
				for (int j = 1; j < ARRAYSIZE (bars); j++)
					bars [j].m_nWidth = 0;

			//TRACEF (p.ToString ());

			for (int nBar = 0; nBar < ARRAYSIZE (bars); nBar++) {
				int cx = bars [nBar].m_nWidth;

				if (bars [nBar].m_bBar) {
					for (int nCol = 0; nCol < cx; nCol++) { 
						int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
						int nBytes = nBarHeight / 8;

						if (pBuffer) {
							memset (&pBuffer [nIndex], 0, nBytes); 

							if ((ULONG)(nIndex + nBytes) < lBufferSize)
								memcpy (&pBuffer [nIndex + nBytes], &nBarMask, 1); 
						}
					}
				}

				nWidth += cx;
			}
		}

		nWidth += nQuietzone + bearer.cx; 
		
		if (pbmpCaption) 
			DrawCaption (* pbmpCaption, strCaption, params, head, pDC, context, nWidth);
	}

	if (pstrFormattedCaption)
		* pstrFormattedCaption = strCaption;

	return nWidth;
}

CSize CBarcodeElement::DrawC93 (CDC &dc, const FoxjetDatabase::HEADSTRUCT & head, 
								CSize & sizeLogical, FoxjetCommon::DRAWCONTEXT context, 
								bool bThrowException, const CBarcodeParams & params)
{
	CString str = Code93::Format (GetImageData ());
	const CString strCaption = FormatCaption ();
	CSize bearer = CBaseElement::ThousandthsToLogical (CPoint (params.GetVertBearer (), params.GetHorzBearer ()), head);
	int nHeight = min ((int)head.GetChannels (), params.GetBarHeight ());
	int nWidth = 0;
	double dStretch [2] = { 0, 0 };

	//TRACEF (FoxjetCommon::Format (str, str.GetLength ()));

	CBaseElement::CalcStretch (head, dStretch);
	bearer.cy = (int)((double)bearer.cy / dStretch [1]);
	
	if (!Code93::IsValidData (str)) {
		if (bThrowException)
			throw new CBarcodeException (LoadString (IDS_INVALIDC93DATA));
		else
			str = FoxjetElements::GetDefaultData (kSymbologyCode93, params);
	}

	nWidth = DrawC93Symbol (str, strCaption, params, head, NULL, 0, 0, NULL, NULL, context, &m_caption.m_strText);

	const LONG lRowLen = (int)ceil ((long double)nHeight / 16.0) * 16; // win bitmap is word aligned
	const int nBufferSize = lRowLen * nWidth / 8;
	LPBYTE pBuffer = new BYTE [nBufferSize];
	int x = 0;
	CBitmap bmp, bmpCaption;
	CDC dcMem;

	dcMem.CreateCompatibleDC (&dc);
	memset (pBuffer, ~0, nBufferSize);

	DrawC93Symbol (str, strCaption, params, head, pBuffer, lRowLen, nBufferSize, &dc, &bmpCaption, context, &m_caption.m_strText);
	Blt (dc, head, context, params, nWidth, nHeight, pBuffer, bmp, bmpCaption);

	delete [] pBuffer;

	if (context == PRINTER) 
		swap (nWidth, nHeight);

	CBitmap * pMem = dcMem.SelectObject (&bmp);
	dc.BitBlt (0, 0, nWidth, nHeight, &dcMem, 0, 0, !IsInverse () ? SRCINVERT : SRCCOPY);
	dcMem.SelectObject (pMem);

	DrawBearer (dc, bearer, context, params, nWidth, nHeight, head);

	CCoord c ((double)nWidth, (double)nHeight, INCHES, head);
	CSize size = m_size = CSize ((int)ceil (c.m_x * 1000.0), (int)ceil (c.m_y * 1000.0));

	sizeLogical = CSize (nWidth, nHeight);

	return size;
}
