#if !defined(AFX_PROPERTIESDLG_H__DA0CB72E_7905_4264_A437_3813125D83A7__INCLUDED_)
#define AFX_PROPERTIESDLG_H__DA0CB72E_7905_4264_A437_3813125D83A7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropertiesDlg.h : header file
//

#include "ListCtrlImp.h"
#include "Database.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CPropertiesDlg dialog

class CPropertiesDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CPropertiesDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPropertiesDlg)
	CString	m_strDesc;
	CString	m_strDownload;
	CString	m_strName;
	//}}AFX_DATA

	ULONG m_lBoxID;
	ULONG m_lLineID;
	ULONG m_lTaskID;
	bool m_bReadOnly;
	CString m_strNotes;

	CStringArray m_vMsgs;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPropertiesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();
	// Generated message map functions
	//{{AFX_MSG(CPropertiesDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPERTIESDLG_H__DA0CB72E_7905_4264_A437_3813125D83A7__INCLUDED_)
