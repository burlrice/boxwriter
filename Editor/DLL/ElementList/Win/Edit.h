#ifndef __EDIT_H__
#define __EDIT_H__

#include "define.h"

#include <afxwin.h>
#include "List.h"
#include "BaseElement.h"
#include "CopyArray.h"
#include "SubElement.h"

namespace FoxjetElements
{
	bool ELEMENT_API CALLBACK OnEditTextElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditBitmapElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditCountElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditDateTimeElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditExpDateElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditUserElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditShiftElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditBarcodeElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditDatabaseElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditSerialElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditLabelElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditShapeElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);

	void ELEMENT_API CALLBACK OnDefineLabelEditor (CWnd * pParent, const FoxjetCommon::CVersion & version);

	void ELEMENT_API CALLBACK OnDefineBitmapEditor (CWnd * pParent, const FoxjetCommon::CVersion & version);
	void ELEMENT_API CALLBACK OnDefineCustomdatetimeformats (CWnd * pParent, const FoxjetCommon::CVersion & version); 
	void ELEMENT_API CALLBACK OnDefineMonthHourCodes (CWnd * pParent, const FoxjetCommon::CVersion & version); 
	bool ELEMENT_API CALLBACK OnDefineShiftCodes (CWnd * pParent, const FoxjetCommon::CVersion & version); 
	void ELEMENT_API CALLBACK OnDefineSubelements(CWnd * pParent, const FoxjetCommon::CVersion & version);
	void ELEMENT_API CALLBACK OnDefineApplicationIdentifiers (CWnd * pParent, const FoxjetCommon::CVersion & version);
	bool ELEMENT_API CALLBACK OnDefineBarcodeParams (CWnd * pParent, const FoxjetCommon::CVersion & version);
	
	void ELEMENT_API LoadBarcodeParams (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);
	void ELEMENT_API LoadShiftcodes (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);

	void ELEMENT_API LoadMonthCodes (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);
	void ELEMENT_API LoadHourCodes (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);
	void ELEMENT_API LoadDayCodes (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);
	void ELEMENT_API LoadRolloverSettings (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);

	// std // #if __CUSTOM__ == __SW0840__
	void ELEMENT_API LoadQuarterHourCodes (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);
	// std // #endif

}; //namespace FoxjetElements

CArray <FoxjetElements::CSubElement *, FoxjetElements::CSubElement *> & GetAI ();
CArray <FoxjetElements::CSubElement *, FoxjetElements::CSubElement *> & GetSubElements (); 

void DeleteAi ();
void DeleteSubElements ();

void CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer);

#endif //__EDIT_H__