#include "stdafx.h"

#include <math.h>

#include "BarcodeElement.h"
#include "Resource.h"
#include "CopyArray.h"
#include "List.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"
#include "Utils.h"
#include "TemplExt.h"
#include "BarcodeParams.h"
#include "BarcodeDlg.h"
#include "Extern.h"
#include "Coord.h"
#include "AppVer.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace Color;
using namespace FoxjetElements;


int CBarcodeElement::StretchToPrinterRes (int n, const FoxjetDatabase::HEADSTRUCT & head)
{
#ifdef __WYSIWYGFIX__
	
	double dScreenRes = CCoord::GetPixelsPerXUnit (INCHES, head);
	double dPrintRes = CBaseElement::CalcPrintRes (head);
	double dFactor = dPrintRes / dScreenRes;

	n = (int)((double)n * dFactor);

#endif

	return n;
}

void CBarcodeElement::DrawBearer (CDC & dc, CSize bearer, 
								  FoxjetCommon::DRAWCONTEXT context,
								  const CBarcodeParams & params, int nWidth, int nHeight,
								  const FoxjetDatabase::HEADSTRUCT & head)
{
	CRect rcOutter;
	int nCaptionSize = params.GetSize () + 1;

	if (context == EDITOR) 
		bearer.cx = StretchToPrinterRes (bearer.cx, head);
	else
		bearer.cy = StretchToPrinterRes (bearer.cy, head);

	if (params.GetAlignment () == kCapAlignOff)
		nCaptionSize = 0;

	if (context == PRINTER) {
		rcOutter = CRect (nCaptionSize, 0, nWidth, nHeight);

		switch (params.GetAlignment ()) {
			case kCapAlignBelowLeft:
			case kCapAlignBelowCenter:
			case kCapAlignBelowRight:
				rcOutter -= CPoint (nCaptionSize, 0);
				break;
		}
	}
	else {
		// TODO: rem // nCaptionSize -= 1;

		rcOutter = CRect (0, 0, nWidth, nHeight - nCaptionSize);
		
		switch (params.GetAlignment ()) {
			case kCapAlignAboveLeft:
			case kCapAlignAboveCenter:
			case kCapAlignAboveRight:
				rcOutter += CPoint (0, nCaptionSize);
				break;
		}
	}

	CRect rcInner (rcOutter);

//	if (context == EDITOR)
		rcInner.DeflateRect (bearer.cx, bearer.cy);
//	else
//		rcInner.DeflateRect (bearer.cy, bearer.cx);

	CRect rcBearer [] =
	{
		CRect (rcOutter.left,	rcOutter.top,		rcOutter.right,		rcInner.top),
		CRect (rcOutter.left,	rcInner.bottom,		rcOutter.right,		rcOutter.bottom),
		CRect (rcOutter.left,	rcInner.top,		rcInner.left,		rcInner.bottom),
		CRect (rcInner.right,	rcInner.top,		rcOutter.right,		rcInner.bottom),
	};

	for (int i = 0; i < ARRAYSIZE (rcBearer); i++)
		::BitBlt (dc, rcBearer [i].left, rcBearer [i].top, 
			rcBearer [i].Width (), rcBearer [i].Height (), NULL, 0, 0,
			IsInverse () ? WHITENESS : BLACKNESS);
}

void CBarcodeElement::Blt (CDC &dc, const FoxjetDatabase::HEADSTRUCT & head, 
						   FoxjetCommon::DRAWCONTEXT context, 
						   const CBarcodeParams & params, int nWidth, int nHeight, LPBYTE pBuffer,
						   CBitmap & bmp, CBitmap & bmpCaption, CSize & bearer)
{
	CDC dcMem;
	DIBSECTION ds;

	::ZeroMemory (&ds, sizeof (ds));
	VERIFY (bmpCaption.GetObject (sizeof (ds), &ds));
	dcMem.CreateCompatibleDC (&dc);

	if (context == EDITOR) {
		CBitmap bmpPrinter, bmpEditor;
 		CDC dcCaption, dcEditor;

		VERIFY (dcCaption.CreateCompatibleDC (&dc));
		VERIFY (dcEditor.CreateCompatibleDC (&dc));

		VERIFY (bmpPrinter.CreateBitmap (nHeight, nWidth, 1, 1, pBuffer));
		VERIFY (bmp.CreateCompatibleBitmap (&dc, nWidth, nHeight));
		
		RotateLeft90AndFlip (dcMem, bmpPrinter, bmpEditor);

		CBitmap * pCaption = dcCaption.SelectObject (&bmpCaption);
		CBitmap * pMem = dcMem.SelectObject (&bmp);
		CBitmap * pEditor = dcEditor.SelectObject (&bmpEditor);

		VERIFY (::BitBlt (dcMem, 0, 0, nWidth, nHeight, NULL, 0, 0, WHITENESS));
		VERIFY (dcMem.BitBlt (0, 0, nWidth, nHeight, &dcEditor, 0, 0, SRCINVERT));

		if (params.GetAlignment () != kCapAlignOff) {
			int x = 0;
			int y = nHeight - ds.dsBm.bmHeight;
			int cx = ds.dsBm.bmWidth;
			int cy = ds.dsBm.bmHeight;
			CDC dcInvert;
			CBitmap bmpInvert;

			VERIFY (dcInvert.CreateCompatibleDC (&dcMem));
			VERIFY (bmpInvert.CreateCompatibleBitmap (&dcMem, cx, cy));
			CBitmap * pInvert = dcInvert.SelectObject (&bmpInvert);

			switch (params.GetAlignment ()) {
				case kCapAlignAboveLeft:	
				case kCapAlignAboveCenter:
				case kCapAlignAboveRight:	
					y = 0; 
					::BitBlt (dcMem, x, cy, cx, 1, NULL, 0, 0, BLACKNESS);
					break;
				default:
					::BitBlt (dcMem, x, y - 1, cx, 1, NULL, 0, 0, BLACKNESS);
					break;
			}

			// flip h
			::BitBlt (dcMem, x, y, cx, cy, NULL, 0, 0, BLACKNESS);
			
			VERIFY (::BitBlt (dcInvert, 0, 0, cx, cy, NULL, 0, 0, WHITENESS));
			VERIFY (dcInvert.BitBlt (0, 0, cx, cy, &dcCaption, 0, 0, SRCINVERT));
			VERIFY (dcMem.BitBlt (x, y, cx, cy, &dcInvert, 0, 0, SRCCOPY));

			dcInvert.SelectObject (pInvert);
		}

		dcMem.SelectObject (pMem);
		dcCaption.SelectObject (pCaption);
		dcEditor.SelectObject (pEditor);
	}
	else {
		CDC dcMem;
		CDC dcCaption;
		CBitmap bmpPrinter;

		swap (bearer.cx, bearer.cy);

		VERIFY (dcMem.CreateCompatibleDC (&dc));
		VERIFY (dcCaption.CreateCompatibleDC (&dc));

		swap (nWidth, nHeight);
		VERIFY (bmpPrinter.CreateBitmap (nWidth, nHeight, 1, 1, pBuffer));
		VERIFY (bmp.CreateCompatibleBitmap (&dc, nWidth, nHeight)); 

		CBitmap * pMem = dcMem.SelectObject (&bmp);
		CBitmap * pCaption = dcCaption.SelectObject (&bmpCaption);

		{
			CBitmap * pOld = dcCaption.SelectObject (&bmpPrinter);
			::BitBlt (dcMem, 0, 0, nWidth, nHeight, NULL, 0, 0, WHITENESS); 
			dcMem.BitBlt (0, 0, nWidth, nHeight, &dcCaption, 0, 0, SRCINVERT);
			dcCaption.SelectObject (pOld);
		}

		if (params.GetAlignment () != kCapAlignOff) {
			int x = 0;
			int y = 0;
			int cx = ds.dsBm.bmWidth;
			int cy = ds.dsBm.bmHeight;

			switch (params.GetAlignment ()) {
				case kCapAlignBelowLeft:	
				case kCapAlignBelowCenter:		
				case kCapAlignBelowRight:	
					::BitBlt (dcMem, cx, y, 1, cy, NULL, 0, 0, BLACKNESS);
					break;
				default:
					x = nWidth - ds.dsBm.bmWidth;
					::BitBlt (dcMem, x - 1, y, 1, cy, NULL, 0, 0, BLACKNESS);
					break;
			}

			::BitBlt (dcMem, x, y, cx, cy, NULL, 0, 0, WHITENESS);

			dcMem.StretchBlt (x, y, cx, cy, &dcCaption, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, SRCINVERT);
		}

		dcMem.SelectObject (pMem);
		dcCaption.SelectObject (pCaption);
	}
}

void CBarcodeElement::DrawCaption (CBitmap & bmp,
								   const CString & strCaption, 
								   const CBarcodeParams & params, 				  
								   const FoxjetDatabase::HEADSTRUCT & head, 
								   CDC * pDC, 
								   FoxjetCommon::DRAWCONTEXT context,
								   int nWidth,
								   CSize & bearer)
{
	CFont fntFull;
	CDC dcMem;
	const int nEscapement = context == PRINTER ? -900 : 0; 
	const int nFontSize = max (params.GetSize (), 6);
	const int nFontWidth = params.GetFontWidth ();
	const bool bBold = (params.GetStyle () & kCapStyleBold) ? true : false;
	const bool bItalic = (params.GetStyle () & kCapStyleItalic) ? true : false;
	const int x = nFontSize + ((context == PRINTER) ? 1 : 0);
	int y = 0;
	int cx = nFontSize, cy = nWidth;
	int nQueitzone = CBaseElement::ThousandthsToLogical (CPoint (params.GetQuietZone (), 0), head).x;
	CRect rc (0, 0, 0, 0);

	nQueitzone = StretchToPrinterRes (nQueitzone, head);

	if (context == EDITOR)
		swap (cx, cy);
	else {
		swap (bearer.cx, bearer.cy);
	}

	ASSERT (pDC);

	fntFull.CreateFont (nFontSize, nFontWidth, nEscapement, nEscapement,
		bBold ? FW_BOLD : FW_NORMAL, 
		bItalic ? TRUE : FALSE, 0, 0,
		DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, params.GetFont ()); 
	
	VERIFY (dcMem.CreateCompatibleDC (pDC));
	VERIFY (bmp.CreateCompatibleBitmap (pDC, cx, cy));

	CBitmap * pBmp			= dcMem.SelectObject (&bmp);
	CFont * pFont			= dcMem.SelectObject (&fntFull);
	COLORREF rgbTextColor	= dcMem.SetTextColor (rgbBlack);
	COLORREF rgbBkColor		= dcMem.SetBkColor (rgbWhite);
	int nBkMode				= dcMem.SetBkMode (OPAQUE);

	::BitBlt (dcMem, 0, 0, cx, cy, NULL, 0, 0, WHITENESS);

	dcMem.DrawText (strCaption, &rc, DT_CALCRECT);

	switch (params.GetAlignment ()) {
		case kCapAlignBelowLeft:
		case kCapAlignAboveLeft:
			y = nQueitzone + bearer.cx;
			break;
		case kCapAlignBelowCenter:
		case kCapAlignAboveCenter:
			y = (nWidth - rc.Width ()) / 2;
			break;
		case kCapAlignBelowRight:
		case kCapAlignAboveRight:
			y = nWidth - rc.Width ();
			y -= ((nQueitzone + bearer.cx) * 1);
			break;
	}

	if (context == PRINTER) dcMem.TextOut (x, y, strCaption);
	else					dcMem.DrawText (strCaption, rc + CPoint (y, 0), 0);

	dcMem.SetTextColor (rgbTextColor);
	dcMem.SetBkColor (rgbBkColor);
	dcMem.SetBkMode (nBkMode);

	dcMem.SelectObject (pFont);
	dcMem.SelectObject (pBmp);
}
