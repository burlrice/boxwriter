// ExpDateElement.cpp: implementation of the CExpDateElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ExpDateElement.h"
#include "List.h"
#include "ElementDefaults.h"
#include "Edit.h"
#include "Debug.h"
#include "Extern.h"
#include "Utils.h"
#include "TemplExt.h"
#include "CodesDlg.h"
#include "Parse.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace FoxjetDatabase;
using namespace FoxjetCommon::ElementFields;

extern CDiagnosticCriticalSection csDays;
extern CodesDlg::CCodesArray vRollover;

IMPLEMENT_DYNAMIC (CExpDateElement, CDateTimeElement);

CExpDateElement::CExpDateElement(const FoxjetDatabase::HEADSTRUCT & head)
:	m_lDays (30),
	m_nHours (0),
	m_nMins (0),
	m_nSecs (0), 
	m_period (PERIOD_DAYS),
	m_round (EXP_NOROUND),
	m_type (TYPE_NORMAL),
	m_bPromptAtTaskStart (false),
	m_nSQLType (0),
	m_nFormat (0),
	CDateTimeElement (head)
{
	m_strDefaultData = _T ("%m/%d/%Y");
}

CExpDateElement::CExpDateElement (const CExpDateElement & rhs)
:	m_lDays (rhs.m_lDays),
	m_nHours (rhs.m_nHours),
	m_nMins (rhs.m_nMins),
	m_nSecs (rhs.m_nSecs), 
	m_period (rhs.m_period),
	m_round (rhs.m_round),
	m_type (rhs.m_type),
	m_bPromptAtTaskStart (rhs.m_bPromptAtTaskStart),
	m_strPrompt (rhs.m_strPrompt),
	m_strDSN (rhs.m_strDSN),
	m_strTable (rhs.m_strTable),
	m_strField (rhs.m_strField),
	m_strKeyField (rhs.m_strKeyField),
	m_strKeyValue (rhs.m_strKeyValue),
	m_nSQLType (rhs.m_nSQLType),
	m_nFormat (rhs.m_nFormat),
	m_strFormat (rhs.m_strFormat),
	CDateTimeElement (rhs)
{
}

CExpDateElement & CExpDateElement::operator = (const CExpDateElement & rhs)
{
	CDateTimeElement::operator = (rhs);

	if (this != &rhs) {
		m_lDays					= rhs.m_lDays;
		m_nHours				= rhs.m_nHours;
		m_nMins					= rhs.m_nMins;
		m_nSecs					= rhs.m_nSecs; 
		m_period				= rhs.m_period;
		m_round					= rhs.m_round;
		m_type					= rhs.m_type;
		m_bPromptAtTaskStart	= rhs.m_bPromptAtTaskStart;
		m_strPrompt				= rhs.m_strPrompt;
		m_strDSN				= rhs.m_strDSN;
		m_strTable				= rhs.m_strTable;
		m_strField				= rhs.m_strField;
		m_strKeyField			= rhs.m_strKeyField;
		m_strKeyValue			= rhs.m_strKeyValue;
		m_nSQLType				= rhs.m_nSQLType;
		m_nFormat				= rhs.m_nFormat;
		m_strFormat				= rhs.m_strFormat;
	}
	
	return * this;
}

CExpDateElement::~CExpDateElement()
{
}

// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
static const ElementFields::FIELDSTRUCT fields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType,				_T ("")									),	// [0]	Prefix (required)
	ElementFields::FIELDSTRUCT (m_lpszID,						_T ("")									),	// [1]	ID (required)
	ElementFields::FIELDSTRUCT (m_lpszX,						_T ("")									),	// [2]	x (required)
	ElementFields::FIELDSTRUCT (m_lpszY,						_T ("")									),	// [3]	y (required)
	ElementFields::FIELDSTRUCT (m_lpszFlipH,					_T ("0")								),	// [4]	flip h
	ElementFields::FIELDSTRUCT (m_lpszFlipV,					_T ("0")								),	// [5]	flip v	
	ElementFields::FIELDSTRUCT (m_lpszInverse,					_T ("0")								),	// [6]	inverse
	ElementFields::FIELDSTRUCT (m_lpszFontName,					_T ("Courier New")						),	// [7]	font name
	ElementFields::FIELDSTRUCT (m_lpszFontSize,					_T ("32")								),	// [8]	font size
	ElementFields::FIELDSTRUCT (m_lpszBold,						_T ("0")								),	// [9]	bold
	ElementFields::FIELDSTRUCT (m_lpszItalic,					_T ("0")								),	// [10] italic
	ElementFields::FIELDSTRUCT (m_lpszData,						_T ("%H:%M:%S")							),	// [11] format string
	ElementFields::FIELDSTRUCT (m_lpszDays,						_T ("30")								),	// [12] days
	ElementFields::FIELDSTRUCT (m_lpszHours,					_T ("0")								),	// [13] hours
	ElementFields::FIELDSTRUCT (m_lpszMinutes,					_T ("0")								),	// [14] minutes
	ElementFields::FIELDSTRUCT (m_lpszSeconds,					_T ("0")								),	// [15] seconds
	ElementFields::FIELDSTRUCT (m_lpszOrient,					_T ("0")								),	// [16] orientation
	ElementFields::FIELDSTRUCT (m_lpszWidth,					_T ("0")								),	// [17] font width
	ElementFields::FIELDSTRUCT (m_lpszPeriod,					_T ("0")								),	// [18] period
	ElementFields::FIELDSTRUCT (m_lpszRound,					_T ("0")								),	// [19] round
	ElementFields::FIELDSTRUCT (m_lpszRollover,					_T ("1")								),	// [20] rollover
	ElementFields::FIELDSTRUCT (m_lpszAlign,					_T ("0")								),	// [21] alignment

	ElementFields::FIELDSTRUCT (m_lpszType,						ToString (CExpDateElement::TYPE_NORMAL)	),	// [22]
	ElementFields::FIELDSTRUCT (m_lpszUserPrompt,				_T ("")									),	// [23]
	ElementFields::FIELDSTRUCT (m_lpszPromptAtTaskStart,		_T ("0")								),	// [24]

	ElementFields::FIELDSTRUCT (m_lpszDSN,						_T ("")									),	// [25]
	ElementFields::FIELDSTRUCT (m_lpszTable,					_T ("")									),	// [26]
	ElementFields::FIELDSTRUCT (m_lpszField,					_T ("")									),	// [27]
	ElementFields::FIELDSTRUCT (m_lpszKeyField,					_T ("")									),	// [28]
	ElementFields::FIELDSTRUCT (m_lpszKeyValue,					_T ("")									),	// [29]
	ElementFields::FIELDSTRUCT (m_lpszSQLType,					_T ("")									),	// [30]
	ElementFields::FIELDSTRUCT (m_lpszFormatType,				_T ("0")								),	// [31]
	ElementFields::FIELDSTRUCT (m_lpszFormat,					_T ("")									),	// [32]
	ElementFields::FIELDSTRUCT (m_lpszDither,					_T ("0")								),	// [33]
	ElementFields::FIELDSTRUCT (m_lpszColor,					_T ("0")								),	// [34]
	ElementFields::FIELDSTRUCT (m_lpszHoldStartDate,			_T ("0")								),	// [35]

	ElementFields::FIELDSTRUCT (),
};
	
CString CExpDateElement::ToString (const FoxjetCommon::CVersion & ver) const
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated

	CString str;
	CPrinterFont f = GetFont ();
	CPoint pt = GetPosInternal (ALIGNMENT_ADJUST_ADD);

	str.Format (
		_T ("{ExpDate,%u,%u,%u,%d,%d,%d,%s,%u,%d,%d,%s,%u,%d,%d,%d,%d,%d,%d,%d,%d,%d,")
		_T ("%d,%s,%d,%s,%s,%s,%s,%s,%d,%d,%s,%d,%06X,%d}"),

		GetID (),
		pt.x, pt.y,
		IsFlippedH (),
		IsFlippedV (),
		IsInverse (),
		f.m_strName, f.m_nSize, f.m_bBold, f.m_bItalic,
		FormatString (GetDefaultData ()),
		GetSpan (SPAN_DAYS),
		GetSpan (SPAN_HOURS),
		GetSpan (SPAN_MINUTES),
		GetSpan (SPAN_SECONDS),
		(int)GetOrientation (),
		GetWidth (),
		GetPeriod (),
		GetRound (),
		GetRollover (),
		GetAlignment (),

		GetType (),
		GetPrompt (),
		GetPromptAtTaskStart (),
		GetDSN (),
		GetTable (),
		GetField (),
		GetKeyField (),
		GetKeyValue (),
		GetSQLType (),
		GetFormatType (),
		GetFormat (),
		GetDither (),
		GetColor (),
		GetHoldStartDate ());

	return str;
}

bool CExpDateElement::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
	CStringArray vstrToken;
	CPrinterFont f;

	Parse (str, ver, vstrToken);

	if (vstrToken [0].CompareNoCase (_T ("ExpDate")) != 0){
		TRACEF ("Wrong prefix \"" + vstrToken [0] + "\"");
		return false;
	}

	f.m_strName = vstrToken [7];
	f.m_nSize = (UINT)_tcstol (vstrToken [8], NULL, 10);
	f.m_bBold = _ttoi (vstrToken [9]) ? true : false;
	f.m_bItalic = _ttoi (vstrToken [10]) ? true : false;

	bool bResult = true;

	bResult &= SetID ((UINT)_tcstol (vstrToken [1], NULL, 10));
	bResult &= SetPos (CPoint (
		(UINT)_tcstol (vstrToken [2], NULL, 10), 
		(UINT)_tcstol (vstrToken [3], NULL, 10)), NULL);
	bResult &= SetFlippedH (_ttoi (vstrToken [4]) ? true : false);
	bResult &= SetFlippedV (_ttoi (vstrToken [5]) ? true : false);
	bResult &= SetInverse (_ttoi (vstrToken [6]) ? true : false);
	bResult &= SetFont (f);
	bResult &= SetDefaultData (UnformatString (vstrToken [11]));
	bResult &= SetSpan ((UINT)_tcstol (vstrToken [12], NULL, 10), SPAN_DAYS);
	bResult &= SetSpan ((UINT)_tcstol (vstrToken [13], NULL, 10), SPAN_HOURS);
	bResult &= SetSpan ((UINT)_tcstol (vstrToken [14], NULL, 10), SPAN_MINUTES);
	bResult &= SetSpan ((UINT)_tcstol (vstrToken [15], NULL, 10), SPAN_SECONDS);
	bResult &= SetOrientation ((TextElement::FONTORIENTATION)_ttoi (vstrToken [16]));
	bResult &= SetWidth (_ttoi (vstrToken [17]));
	bResult &= SetPeriod ((PERIOD)_ttoi (vstrToken [18]));
	bResult &= SetRound ((EXP_ROUND)_ttoi (vstrToken [19]));
	bResult &= SetRollover (_ttoi (vstrToken [20]) ? true : false);
	bResult &= SetAlignment ((ALIGNMENT)_ttoi (vstrToken [21]));

	bResult &= SetType ((TYPE)_ttoi (vstrToken [22]));
	bResult &= SetPrompt	(vstrToken [23]);
	bResult &= SetPromptAtTaskStart (_ttoi (vstrToken [24]) ? true : false);
	bResult &= SetDSN		(vstrToken [25]);
	bResult &= SetTable		(vstrToken [26]);
	bResult &= SetField		(vstrToken [27]);
	bResult &= SetKeyField	(vstrToken [28]);
	bResult &= SetKeyValue	(vstrToken [29]);
	bResult &= SetSQLType	(_ttoi (vstrToken [30]));
	bResult &= SetFormatType (_ttoi (vstrToken [31]));
	bResult &= SetFormat (vstrToken [32]);
	bResult &= SetDither (_ttoi (vstrToken [33]));
	bResult &= SetColor (_tcstoul (vstrToken [34], NULL, 16));
	bResult &= SetHoldStartDate (_ttoi (vstrToken [35]) ? true : false);

	return bResult;
}

CExpDateElement::PERIOD CExpDateElement::GetPeriod () const
{
	return m_period;
}

bool CExpDateElement::SetPeriod (CExpDateElement::PERIOD n)
{
	m_period = n;
	return true;
}

CExpDateElement::EXP_ROUND CExpDateElement::GetRound () const
{
	return m_round;
}

bool CExpDateElement::SetRound (EXP_ROUND r)
{
	m_round = r;

	return true;
}

static long DateDiff (const CTime & tm1, const CTime & tm2) // TODO
{
	long n1 = (tm1.GetYear () * 12) + tm1.GetMonth ();
	long n2 = (tm2.GetYear () * 12) + tm2.GetMonth ();

	return n1 - n2;
}

int CExpDateElement::GetFirstDayOfWeek () const
{
	LOCK (::csDays);

	int nResult = 0;

	for (int i = 0; i < ::vRollover.GetSize (); i++) {
		CodesDlg::CCodes c = ::vRollover [i];

		if (c.m_lLineID == GetLineID ()) {
			ASSERT (c.m_vCodes.GetSize () >= 2);

			nResult = _ttoi (c.m_vCodes [1]);
			break;
		}
	}

	return nResult;
}

int CExpDateElement::GetDstOffset (const CTime & now, const CTime & exp)
{
	struct tm tmNow;
	struct tm tmExp;

	now.GetLocalTm (&tmNow); //GetGmtTm always returns DST = 0
	exp.GetLocalTm (&tmExp);

	mktime (&tmNow);
	mktime (&tmExp);

	/*
	TRACEF ("now: " 
		+ CDateTimeElement::Format (GetLineID (), m_strDefaultData, 0, &now) + " [" 
		+ CDateTimeElement::Format (GetLineID (), m_strDefaultData, 0, &CTime (mktime (&tmNow))) + "]");
	TRACEF ("exp: " 
		+ CDateTimeElement::Format (GetLineID (), m_strDefaultData, 0, &exp) + " [" 
		+ CDateTimeElement::Format (GetLineID (), m_strDefaultData, 0, &CTime (mktime (&tmExp))) + "]");
	*/

	return tmNow.tm_isdst - tmExp.tm_isdst;
}

int CExpDateElement::Build(BUILDTYPE type, bool bCanThrowException)
{
//	int n = GetRolloverMinutes ();
//	CTime tmNow = CalcCurrentTime (GetRolloverMinutes (), m_lLineID);
	CTime tmNow = CalcCurrentTime (0, m_lLineID);

	if (type == INITIAL)
		m_tmInitial = CalcCurrentTime (0, m_lLineID);

	if (GetHoldStartDate ()) 
		tmNow = m_tmInitial;

	TRACEF ((GetHoldStartDate () ? _T ("hold: ") : _T ("      ")) + tmNow.Format (_T ("%c")));

	return Build (type, bCanThrowException, tmNow);
}

int CExpDateElement::Build (BUILDTYPE type, bool bCanThrowException, CTime tmBuild)
{
	// special case where current time is not DST, but rollover is DST (or vice versa)
	if (int nRolloverOffset = GetDstOffset (tmBuild, tmBuild + CTimeSpan (0, 0, GetRolloverMinutes (), 0))) {
		const CTimeSpan span = CTimeSpan (0, 0, abs (GetRolloverMinutes ()), 0);
		const ULONG lMinutes = (span.GetHours () * 60) + span.GetMinutes ();
		const CTime tmStart (tmBuild.GetYear (), tmBuild.GetMonth (), tmBuild.GetDay (), 0, 0, 0);
		const CTimeSpan rollover (0, 0, lMinutes, 0);
		const CTime tmEnd = tmStart + rollover;
		
		if (nRolloverOffset < 0) {
			bool bRollback = (tmBuild >= tmStart) && (tmBuild < (tmEnd + CTimeSpan (0, 1, 0, 0)));

			if (bRollback) {
				tmBuild = tmStart;
				
				ASSERT (GetDstOffset (tmBuild, tmBuild + CTimeSpan (0, 0, GetRolloverMinutes (), 0)) == 0);
			}
		}
		else {
			bool bRollback = (tmBuild >= tmStart) && (tmBuild < (tmEnd - CTimeSpan (0, 1, 0, 0)));

			if (bRollback) {
				tmBuild = tmStart;

				ASSERT (GetDstOffset (tmBuild, tmBuild + CTimeSpan (0, 0, GetRolloverMinutes (), 0)) == 0);
			}
			else
				tmBuild = tmEnd;
		}
	}

	CString strBuild = tmBuild.Format ("%c");
	CTime tmNow = CalcCurrentTime (GetRolloverMinutes (), m_lLineID, &tmBuild);

	if (GetPeriod () == PERIOD_DAYS) {
		const CTimeSpan tmSpan = CTimeSpan (m_lDays, 0, 0, 0); //m_nHours, m_nMins, m_nSecs);
		const int nDstOffset = GetDstOffset (tmNow, tmNow + tmSpan);
		const CTime tm = CalcExpTime (tmNow + tmSpan + CTimeSpan (0, nDstOffset, 0, 0), tmNow);

		m_strImageData = CDateTimeElement::Format (GetLineID (), m_strDefaultData, 0, &tm);
		return GetClassID ();
	}
	else {
		struct tm mtm;
		EXP_ROUND round = GetRound ();

		tmNow.GetLocalTm (&mtm); 

		switch (GetPeriod ()) {	
		case PERIOD_WEEKS: 
			{
				const int nFDOW = GetFirstDayOfWeek ();
				const CTimeSpan tmSpan = CTimeSpan (m_lDays * 7, m_nHours, m_nMins, m_nSecs);
				const int nDstOffset = GetDstOffset (tmNow, tmNow + tmSpan);
				CTime tm = CalcExpTime (tmNow + tmSpan + CTimeSpan (0, nDstOffset, 0, 0), tmNow);

				if (round == EXP_ROUNDDOWN) {
					CTime tmRound = tmNow + CTimeSpan (GetSpan () * 7, 0, 0, 0);

					if (int nDST = GetDstOffset (tmNow, tmRound))
						tmRound += CTimeSpan (0, nDST, 0, 0);

					if (nFDOW == 0) 
						tmRound -= CTimeSpan (1, 0, 0, 0);

					int nCurrentDOW = tmRound.GetDayOfWeek ();

					while ((nCurrentDOW = tmRound.GetDayOfWeek ()) != (nFDOW + 1)) 
						tmRound -= CTimeSpan (1, 0, 0, 0);

					tm = tmRound;
				}
				else if (round == EXP_ROUNDUP) {
					int nTop = (nFDOW + 6) % 7;
					CTime tmRound = tmNow + CTimeSpan (GetSpan () * 7, 0, 0, 0);

					if (int nDST = GetDstOffset (tmNow, tmRound))
						tmRound += CTimeSpan (0, nDST, 0, 0);

					if (nFDOW == 0) 
						tmRound -= CTimeSpan (1, 0, 0, 0);

					int nCurrentDOW = tmRound.GetDayOfWeek ();

					while ((nCurrentDOW = tmRound.GetDayOfWeek ()) != (nTop + 1))
						tmRound += CTimeSpan (1, 0, 0, 0);

					tm = tmRound;
				}

				m_strImageData = CDateTimeElement::Format (GetLineID (), m_strDefaultData, 0, &tm);
				return GetClassID ();
			}
		case PERIOD_MONTHS:	
			{
				const ULONG lSpan = GetSpan ();
				CTimeSpan tmSpan = CTimeSpan (0, m_nHours, m_nMins, m_nSecs);
				CTime tm = CalcExpTime (tmNow + tmSpan, tmNow);
				CTime tmTmp (tm);

				tm.GetLocalTm (&mtm); 
				mtm.tm_mon += lSpan;
				tmTmp = CTime (mktime (&mtm));

				if (int nDstOffset = GetDstOffset (tmNow, tmTmp)) 
					tmTmp += CTimeSpan (0, nDstOffset, 0, 0);

				long nDiff = DateDiff (tmTmp, tmNow);

				// if we added one month to 1/31 and got 3/2, roll back until 
				// we hit the last day of the previous month (2/29)
				while (nDiff > (long)lSpan) {
					tmTmp -= CTimeSpan (1, 0, 0, 0);

					nDiff = DateDiff (tmTmp, tmNow);
				}
				
				if (round == EXP_ROUNDDOWN) {
					tmTmp = CTime (
						tmTmp.GetYear (), tmTmp.GetMonth (), 1, 
						tmTmp.GetHour (), tmTmp.GetMinute (), tmTmp.GetSecond ());
				}
				else if (round == EXP_ROUNDUP) {
					CTime tmTop (tmTmp);
					int nDay = tmTop.GetDay ();

					while (tmTop.GetMonth () == tmTmp.GetMonth ()) {
						nDay = tmTop.GetDay ();
						tmTop += CTimeSpan (1, 0, 0, 0);
					}

					tmTmp = CTime (
						tmTmp.GetYear (), tmTmp.GetMonth (), nDay, 
						tmTmp.GetHour (), tmTmp.GetMinute (), tmTmp.GetSecond ());
				}

				tm = tmTmp;
				CString strTM = tm.Format ("%c");
				m_strImageData = CDateTimeElement::Format (GetLineID (), m_strDefaultData, 0, &tm);
				return GetClassID ();
			}
		case PERIOD_YEARS: 
			{
				const ULONG lSpan = GetSpan ();
				const CTimeSpan tmSpan = CTimeSpan (0, m_nHours, m_nMins, m_nSecs);
				const int nDstOffset = GetDstOffset (tmNow, tmNow + tmSpan);
				CTime tm = CalcExpTime (tmNow + tmSpan + CTimeSpan (0, nDstOffset, 0, 0), tmNow);
				int nMonth = tm.GetMonth ();
				int nDay = tm.GetDay ();

				if (round == EXP_ROUNDDOWN) {
					nMonth	= 1;
					nDay	= 1;
				}
				else if (round == EXP_ROUNDUP) {
					nMonth	= 12;
					nDay	= 31;
				}

				tm = CTime (
					tm.GetYear () + lSpan, nMonth, nDay, 
					tm.GetHour (), tm.GetMinute (), tm.GetSecond ());

				/*
				tm = CTime (
					tm.GetYear (), nMonth, nDay, 
					tm.GetHour (), tm.GetMinute (), tm.GetSecond ());

				TRACEF (tm.Format (_T ("%c")));

				for (int n = 0; n < lSpan; n++) {
					tm += CTimeSpan (IsLeapYear (tm.GetYear () + n) ? 366 : 365, 0, 0, 0);
					TRACEF (tm.Format (_T ("%c")));
				}
				*/

				if (tm == -1)
					tm = CTime::GetCurrentTime (); //sw0924

				m_strImageData = CDateTimeElement::Format (GetLineID (), m_strDefaultData, 0, &tm);
				return GetClassID ();
			}
		}
	}

	// DO NOT CALL 
	/* CDateTimeElement::Build (type, bCanThrowException); */

	ASSERT (0); 
	return GetClassID ();
}

CTime CExpDateElement::CalcExpTime (const CTime & tm, const CTime & tmNow) const
{
	int nYear	= tm.GetYear ();
	int nMonth	= tm.GetMonth ();
	int nDay	= tm.GetDay ();
	int nHour	= tmNow.GetHour ();		//+ m_nHours;
	int nMin	= tmNow.GetMinute ();	//+ m_nMins;
	int nSec	= tmNow.GetSecond ();	//+ m_nSecs;
	
	CTime tmResult = CTime (nYear, nMonth, nDay, nHour, nMin, nSec) + CTimeSpan (0, m_nHours, m_nMins, m_nSecs);

	//TRACEF (tmResult.Format (_T ("%c")));

	return tmResult;
}

bool CExpDateElement::SetSpan (ULONG lSpan, SPAN span)
{
	switch (span) {
	case SPAN_DAYS:
		if (lSpan >= 0 && lSpan <= 25000) {
			// 25000 comes from the msdn documentation for
			// CTimeSpan( LONG lDays, int nHours, int nMins, int nSecs );
			SetRedraw ();
			m_lDays = lSpan;
			return true;
		}
	case SPAN_HOURS:
		if (lSpan >= 0 && lSpan <= 23) {
			SetRedraw ();
			m_nHours = lSpan;
			return true;
		}
	case SPAN_MINUTES:	
		if (lSpan >= 0 && lSpan <= 59) {
			SetRedraw ();
			m_nMins = lSpan;
			return true;
		}
	case SPAN_SECONDS:	
		if (lSpan >= 0 && lSpan <= 59) {
			SetRedraw ();
			m_nSecs = lSpan;
			return true;
		}
	}

	return false;
}

ULONG CExpDateElement::GetSpan (SPAN span) const
{
	switch (span) {
	case SPAN_DAYS:		return m_lDays;
	case SPAN_HOURS:	return m_nHours;
	case SPAN_MINUTES:	return m_nMins;
	case SPAN_SECONDS:	return m_nSecs; 
	}

	ASSERT (0);
	return 0;
}

const FoxjetCommon::ElementFields::FIELDSTRUCT * CExpDateElement::GetFieldBuffer () const
{
	return ::fields;
}

CExpDateElement::TYPE CExpDateElement::GetType () const
{
	return m_type;
}

bool CExpDateElement::SetType (TYPE type)
{
	m_type = type;
	return true;
}

bool CExpDateElement::GetPromptAtTaskStart () const
{
	return m_bPromptAtTaskStart;
}

bool CExpDateElement::SetPromptAtTaskStart (bool bPrompt)
{
	m_bPromptAtTaskStart = bPrompt;

	return true;
}

CString CExpDateElement::GetPrompt () const
{
	if (m_type == TYPE_DATABASE) {
		CString str;
		
		str.Format (_T ("[%s].[%s].[%s]"),
			m_strDSN,
			m_strTable,
			m_strKeyField);

		return str;
	}

	return m_strPrompt;
}

bool CExpDateElement::SetPrompt (const CString & str)
{
	m_strPrompt = str;
	return true;
}

CString CExpDateElement::GetDSN () const
{
	return m_strDSN;
}

bool CExpDateElement::SetDSN (const CString & str)
{
	m_strDSN = str;
	return true;
}

CString CExpDateElement::GetTable () const
{
	return m_strTable;
}

bool CExpDateElement::SetTable (const CString & str)
{
	m_strTable = str;
	return true;
}

CString CExpDateElement::GetField () const
{
	return m_strField;
}

bool CExpDateElement::SetField (const CString & str)
{
	m_strField = str;
	return true;
}

CString CExpDateElement::GetKeyField () const
{
	return m_strKeyField;
}

bool CExpDateElement::SetKeyField (const CString & str)
{
	m_strKeyField = str;
	return true;
}

CString CExpDateElement::GetKeyValue () const
{
	return m_strKeyValue;
}

bool CExpDateElement::SetKeyValue (const CString & str)
{
	m_strKeyValue = str;
	return true;
}

int CExpDateElement::GetSQLType () const
{
	return m_nSQLType;
}

bool CExpDateElement::SetSQLType (int nType)
{
	m_nSQLType = nType;
	return true;
}

int CExpDateElement::GetFormatType () const
{
	return m_nFormat;
}

bool CExpDateElement::SetFormatType (int nFormat)
{
	m_nFormat = nFormat;
	return true;
}

CString CExpDateElement::GetFormat () const
{
	return m_strFormat;
}

bool CExpDateElement::SetFormat (const CString & str)
{
	m_strFormat = str;
	return true;
}

