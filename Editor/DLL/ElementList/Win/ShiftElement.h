// ShiftElement.h: interface for the CShiftElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SHIFTELEMENT_H__6CB62661_8AD9_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_SHIFTELEMENT_H__6CB62661_8AD9_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TextElement.h"
#include "Types.h"

namespace FoxjetElements
{
	using namespace TextElement;

	class ELEMENT_API CShiftElement : public CTextElement  
	{
		DECLARE_DYNAMIC (CShiftElement);

	public:
		CShiftElement(const FoxjetDatabase::HEADSTRUCT & head);
		CShiftElement (const CShiftElement & rhs);
		CShiftElement & operator = (const CShiftElement & rhs);
		virtual ~CShiftElement();

		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);
		virtual int GetClassID () const { return SHIFTCODE; }

		virtual int Build (BUILDTYPE type = INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		static ULONG TimeToLong (const COleDateTime & tm);
	};
}; //namespace FoxjetElements

#endif // !defined(AFX_SHIFTELEMENT_H__6CB62661_8AD9_11D4_8FC6_006067662794__INCLUDED_)
