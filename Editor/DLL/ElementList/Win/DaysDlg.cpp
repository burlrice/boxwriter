// DaysDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "DaysDlg.h"
#include "Extern.h"
#include "Database.h"
#include "List.h"
#include "DateTimePropShtDlg.h"
#include "Debug.h"
#include "EditValueDlg.h"
#include "SystemDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ItiLibrary;
using namespace ListGlobals;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;


/////////////////////////////////////////////////////////////////////////////

CDaysDlg::CItem::CItem (const CString & str, int nIndex, CDaysDlg * pParent)
:	m_str (str), 
	m_nIndex (nIndex),
	m_pParent (pParent)
{
}

CDaysDlg::CItem::~CItem ()
{
}

CString CDaysDlg::CItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0: 
		if (CString (_T ("CHoursDlg")).Compare (a2w (m_pParent->GetRuntimeClass ()->m_lpszClassName)))
			str.Format (_T ("%d"), m_nIndex);
		else {
			CTime tmBase	= CTime (2006, 1, 1, 0, 0, 0) ;
			CTime tm1		= tmBase + CTimeSpan (0, m_nIndex - 1,	0,	0);
			CTime tm2		= tmBase + CTimeSpan (0, m_nIndex - 1,	59,	59);
			LPCTSTR lpsz	= _T ("%I:%M %p");

			str = tm1.Format (lpsz) + CString (_T (" - ")) + tm2.Format (lpsz);

			#ifdef _DEBUG
			CString strTmp;
			strTmp.Format (_T (" [%d]"), m_nIndex);
			str += strTmp;
			#endif
		}
		break;
	case 1:
		str = m_str;
	}

	return str;
}

int CDaysDlg::CItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	CItem & cmp = (CItem &)rhs;

	switch (nColumn) {
	case 0:		return m_nIndex - cmp.m_nIndex;
	case 1:		return m_str.Compare (cmp.m_str);
	}

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CDaysDlg property page

IMPLEMENT_DYNCREATE(CDaysDlg, CPropertyPage)

CDaysDlg::CDaysDlg () 
:	m_db (ListGlobals::GetDB ()),
	CPropertyPage(IDD_PROP_DAYS)
{
	//{{AFX_DATA_INIT(CDaysDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CDaysDlg::CDaysDlg (FoxjetDatabase::COdbcDatabase & db, const CodesDlg::CCodesArray & vCodes)
:	m_db (db), 
	m_vCodes (vCodes),
	CPropertyPage (IDD_PROP_DAYS)	
{
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CDaysDlg::CDaysDlg (FoxjetDatabase::COdbcDatabase & db, UINT nIDTemplate, UINT nIDCaption)
:	m_db (db),
	CPropertyPage (nIDTemplate, nIDCaption)	
{
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CDaysDlg::CDaysDlg (FoxjetDatabase::COdbcDatabase & db, LPCTSTR lpszTemplateName, UINT nIDCaption)
:	m_db (db),
	CPropertyPage (lpszTemplateName, nIDCaption)
{
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CDaysDlg::~CDaysDlg()
{
}

void CDaysDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);

	if (!pDX->m_bSaveAndValidate) {
		ULONG lLineID = GetParent ()->SendMessage (CDateTimePropShtDlg::WM_GETLINEID, 0, 0);
		CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);

		ASSERT (pLine);

		int nCurSel = pLine->GetCurSel ();

		for (int i = 0; i < pLine->GetCount (); i++) {
			if (lLineID == pLine->GetItemData (i)) {
				pLine->SetCurSel (i);
				break;
			}
		}

		if (pLine->GetCurSel () == CB_ERR)
			pLine->SetCurSel (0);

		if (nCurSel != pLine->GetCurSel ())
			OnSelchangeLine ();
	}

	//{{AFX_DATA_MAP(CDaysDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDaysDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CDaysDlg)
	ON_CBN_SELCHANGE(CB_LINE, OnSelchangeLine)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_NOTIFY(NM_DBLCLK, LV_TYPE, OnDblclkType)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDaysDlg message handlers

BOOL CDaysDlg::OnInitDialog() 
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	ULONG lLineID = GetCurLine ();

	ASSERT (pLine);

	CPropertyPage::OnInitDialog();

	#if __CUSTOM__ == __SW0877__
	pLine->EnableWindow (FALSE);
	lLineID = 0;

		#ifndef _DEBUG
		pLine->ShowWindow (SW_HIDE);
		#endif
	#endif

	if (GetListCtrlID () != -1) {
		CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;
	
		vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_INDEX), 140));
		vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_VALUE), 140));

		m_lv.Create (GetListCtrlID (), vCols, this, defElements.m_strRegSection);	
	}

	GetLineRecords (vLines);

	for (int i = 0; i < vLines.GetSize (); i++) {
		LINESTRUCT line = vLines [i];
		int nIndex = pLine->AddString (line.m_strName);

		pLine->SetItemData (nIndex, line.m_lID);

		if (lLineID == line.m_lID) 
			pLine->SetCurSel (nIndex);
	}

	if (pLine->GetCurSel () == CB_ERR) 
		pLine->SetCurSel (0);

	OnSelchangeLine ();

	return TRUE;	
}

ULONG CDaysDlg::GetCurLine() const
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	ULONG lLineID = -1;

	ASSERT (pLine);

	int nSel = pLine->GetCurSel ();

	if (nSel != CB_ERR)
		lLineID = pLine->GetItemData (nSel);
	else
		lLineID = GetParent ()->SendMessage (CDateTimePropShtDlg::WM_GETLINEID, 0, 0);

	return lLineID;
}

void CDaysDlg::OnSelchangeLine() 
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	ULONG lLineID = GetCurLine ();

	ASSERT (pLine);
	
	if (CWnd * p = GetParent ()) 
		p->SendMessage (CDateTimePropShtDlg::WM_LINECHANGED, (WPARAM)this, (LPARAM)lLineID);

	if (GetListCtrlID () != -1) {
		CodesDlg::CCodes c = GetCodes (lLineID);

		m_lv.DeleteCtrlData ();

		for (int i = 0; i < c.m_vCodes.GetSize (); i++) {
			int nIndex = i + 1;
			CString str = c.m_vCodes [i];

			m_lv.InsertCtrlData (new CItem (str, nIndex, this));
		}
	}
}

void CDaysDlg::OnDblclkType(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();
	*pResult = 0;
}

CodesDlg::CCodes CDaysDlg::GetCodes (ULONG lLineID) const
{
	CodesDlg::CCodes c = CDateTimePropShtDlg::GetCodes (lLineID, m_vCodes);
	
	ASSERT (c.m_vCodes.GetSize () == 7);

	return c;
}

void CDaysDlg::Update (ULONG lLineID, UINT nIndex, const CString & strValue)
{
	lLineID = FoxjetDatabase::GetMasterLineID (lLineID);

	#if __CUSTOM__ == __SW0877__
	lLineID = 0;
	#endif

	CodesDlg::CCodes c = CDateTimePropShtDlg::GetCodes (lLineID, m_vCodes);

	ASSERT (c.m_vCodes.GetSize () == 7);
	c.m_vCodes [nIndex] = strValue;
	VERIFY (CDateTimePropShtDlg::SetCodes (m_db, lLineID, c, m_vCodes, ListGlobals::Keys::m_strDayOfWeek));
}

void CDaysDlg::OnEdit() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		CEditValueDlg dlg (this);
		CItem * p = (CItem *)m_lv.GetCtrlData (sel [0]);
		ULONG lLineID = GetCurLine ();

		dlg.m_nIndex = p->m_nIndex;
		dlg.m_strValue = p->m_str;

		if (dlg.DoModal () == IDOK) {
			int nIndex = p->m_nIndex - 1;

			p->m_str = dlg.m_strValue;
			m_lv.UpdateCtrlData (p);

			Update (lLineID, nIndex, dlg.m_strValue);
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}
