// ElementList.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"

#include <afxdllx.h>
#include <shlwapi.h>

#include "Debug.h"
#include "DllVer.h"
#include "AppVer.h"
#include "List.h"
#include "Resource.h"
#include "FileExt.h"
#include "Parse.h"
#include "Registry.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

HINSTANCE hInstance = NULL;
static AFX_EXTENSION_MODULE ElementListDLL = { NULL, NULL };

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACEF (_T ("ELEMENTLIST.DLL Initializing!"));
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(ElementListDLL, hInstance)) {
			MESSAGEBOX (_T ("AfxInitExtensionModule failed"));
			return 0;
		}

		// Insert this DLL into the resource chain
		// NOTE: If this Extension DLL is being implicitly linked to by
		//  an MFC Regular DLL (such as an ActiveX Control)
		//  instead of an MFC application, then you will want to
		//  remove this line from DllMain and put it in a separate
		//  function exported from this Extension DLL.  The Regular DLL
		//  that uses this Extension DLL should then explicitly call that
		//  function to initialize this Extension DLL.  Otherwise,
		//  the CDynLinkLibrary object will not be attached to the
		//  Regular DLL's resource chain, and serious problems will
		//  result.

		::hInstance = hInstance;
		new CDynLinkLibrary(ElementListDLL);
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("ELEMENTLIST.DLL Terminating!\n");
		// Terminate the library before destructors are called
		AfxTermExtensionModule(ElementListDLL);
	}
	return 1;   // ok
}

void CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer)
{
	if (lpVer) 
		* lpVer = FoxjetCommon::verApp; // memcpy (lpVer, &FoxjetCommon::verApp, sizeof (FoxjetCommon::VERSIONSTRUCT));
}

HRESULT CALLBACK DllGetVersion (DLLVERSIONINFO * pdvi)
{
	FoxjetCommon::VERSIONSTRUCT ver;

	fj_GetDllVersion (&ver);

	if (pdvi) {
		pdvi->dwMajorVersion	= ver.m_nMajor;
		pdvi->dwMinorVersion	= ver.m_nMinor;
		pdvi->dwBuildNumber		= ver.m_nInternal;
		pdvi->dwPlatformID		= DLLVER_PLATFORM_WINDOWS;
	}

	return NOERROR;
}

HINSTANCE GetInstanceHandle ()
{
	return ::hInstance;
}

CString LoadString (UINT nID)
{
	return FoxjetDatabase::LoadString (nID, _T ("ElementList.dll"));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct _tagTT_OFFSET_TABLE{
	USHORT	uMajorVersion;
	USHORT	uMinorVersion;
	USHORT	uNumOfTables;
	USHORT	uSearchRange;
	USHORT	uEntrySelector;
	USHORT	uRangeShift;
}TT_OFFSET_TABLE;

typedef struct _tagTT_TABLE_DIRECTORY{
	char	szTag[4];			//table name
	ULONG	uCheckSum;			//Check sum
	ULONG	uOffset;			//Offset from beginning of file
	ULONG	uLength;			//length of the table in bytes
}TT_TABLE_DIRECTORY;

typedef struct _tagTT_NAME_TABLE_HEADER{
	USHORT	uFSelector;			//format selector. Always 0
	USHORT	uNRCount;			//Name Records count
	USHORT	uStorageOffset;		//Offset for strings storage, from start of the table
}TT_NAME_TABLE_HEADER;

typedef struct _tagTT_NAME_RECORD{
	USHORT	uPlatformID;
	USHORT	uEncodingID;
	USHORT	uLanguageID;
	USHORT	uNameID;
	USHORT	uStringLength;
	USHORT	uStringOffset;	//from start of storage area
}TT_NAME_RECORD;

#define SWAPWORD(x)		MAKEWORD(HIBYTE(x), LOBYTE(x))
#define SWAPLONG(x)		MAKELONG(SWAPWORD(HIWORD(x)), SWAPWORD(LOWORD(x)))


ELEMENT_API CString FoxjetCommon::GetFontNameFromFile(LPCTSTR lpszFilePath)
{
	CFile f;

	if(f.Open(lpszFilePath, CFile::modeRead|CFile::shareDenyWrite)){
		TT_OFFSET_TABLE ttOffsetTable;
		f.Read(&ttOffsetTable, sizeof(TT_OFFSET_TABLE));
		ttOffsetTable.uNumOfTables = SWAPWORD(ttOffsetTable.uNumOfTables);
		ttOffsetTable.uMajorVersion = SWAPWORD(ttOffsetTable.uMajorVersion);
		ttOffsetTable.uMinorVersion = SWAPWORD(ttOffsetTable.uMinorVersion);

		//check is this is a true type font and the version is 1.0
		if(ttOffsetTable.uMajorVersion != 1 || ttOffsetTable.uMinorVersion != 0)
			return _T ("");
		
		TT_TABLE_DIRECTORY tblDir;
		BOOL bFound = FALSE;
		//CString csTemp;
		
		for(int i=0; i< ttOffsetTable.uNumOfTables; i++){
			f.Read(&tblDir, sizeof(TT_TABLE_DIRECTORY));
			//strncpy(csTemp.GetBuffer(5), tblDir.szTag, 4);
			//csTemp.ReleaseBuffer(4);
			//if(csTemp.CompareNoCase(_T("name")) == 0){
			if (!_strnicmp (tblDir.szTag, "name", 4)) {
				bFound = TRUE;
				tblDir.uLength = SWAPLONG(tblDir.uLength);
				tblDir.uOffset = SWAPLONG(tblDir.uOffset);
				break;
			}
		}
		
		if(bFound){
			f.Seek(tblDir.uOffset, CFile::begin);
			TT_NAME_TABLE_HEADER ttNTHeader;
			f.Read(&ttNTHeader, sizeof(TT_NAME_TABLE_HEADER));
			ttNTHeader.uNRCount = SWAPWORD(ttNTHeader.uNRCount);
			ttNTHeader.uStorageOffset = SWAPWORD(ttNTHeader.uStorageOffset);
			TT_NAME_RECORD ttRecord;
			bFound = FALSE;
			
			for(int i=0; i<ttNTHeader.uNRCount; i++){
				f.Read(&ttRecord, sizeof(TT_NAME_RECORD));
				ttRecord.uNameID = SWAPWORD(ttRecord.uNameID);
				if(ttRecord.uNameID == 1){
					ttRecord.uStringLength = SWAPWORD(ttRecord.uStringLength);
					ttRecord.uStringOffset = SWAPWORD(ttRecord.uStringOffset);
					int nPos = f.GetPosition();
					f.Seek(tblDir.uOffset + ttRecord.uStringOffset + ttNTHeader.uStorageOffset, CFile::begin);
					char sz [_MAX_PATH] = { 0 };
					UINT nRead = f.Read (sz, ttRecord.uStringLength);

					if (nRead > 0)
						return CString (w2a (sz));

					f.Seek(nPos, CFile::begin);
				}
			}			
		}

		f.Close();
	}

	return _T ("");
}

ELEMENT_API bool FoxjetCommon::InstallFont (const CString & strPath)
{
	TCHAR sz [_MAX_PATH] = { 0 };

	::GetWindowsDirectory (sz, ARRAYSIZE (sz));
	CString strFontDir = sz + CString (_T ("\\Fonts"));

	int nIndex = strPath.ReverseFind ('\\');

	if (nIndex != -1) {
		CString strFile = strPath.Mid (nIndex + 1);
		CString strDest = strFontDir + _T ("\\") + strFile;

		TRACEF (strPath + _T (" --> ") + strDest);
		
		if (!::CopyFile (strPath, strDest, FALSE)) {
			TRACEF (FORMATMESSAGE (::GetLastError ()));
			return false;
		}

		int n = ::AddFontResource (strDest);
		TRACEF (_T ("::AddFontResource: ") + FoxjetDatabase::ToString (n));

		if (n) {
			CString strName = GetFontNameFromFile (strPath) + _T (" (True Type)");

			TRACEF (_T ("write reg key: ") + strName + _T (": ") + strFile);

			VERIFY (FoxjetDatabase::WriteProfileString (HKEY_LOCAL_MACHINE, 
				_T ("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Fonts"), 
				strName, strFile));
			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
ELEMENT_API void FoxjetCommon::CheckFonts ()
{
	using namespace FoxjetDatabase;

	CStringArray vFonts, vFiles;
	const CString strDir = GetHomeDir () + _T ("\\Fonts");
	CMapStringToString v;

	FoxjetDatabase::GetFiles (strDir, _T ("*.ttf"), vFiles);
	CHead::GetWinFontNames (vFonts);
	
	for (int i = 0; i < vFiles.GetSize (); i++) {
		CString strPath = vFiles [i];
		CString strFile (strPath);

		if (strFile.Replace (strDir + _T ("\\"), _T (""))) {
			CString strFont = GetFontNameFromFile (strPath);

			ASSERT (strFont.GetLength ());

			if (Find (vFonts, strFont, false) == -1) {
				TRACEF (_T ("missing: ") + strFont + _T (": ") + strFile + _T (": ") + strPath);
				v.SetAt (strFont, strPath);
			}
		}
	}

	if (v.GetCount ()) {
		CString str = LoadString (IDS_MISSINGFONTS);

		for (POSITION pos = v.GetStartPosition (); pos; ) {
			CString strFont, strFile;

			v.GetNextAssoc (pos, strFont, strFile);
			str += _T ("\r\n") + strFont;
		}

		str += _T ("\r\n\r\n") + LoadString (IDS_INSTALLNOW);

		if (MsgBox (str, MB_YESNO) == IDYES) {
			CStringArray vFailed;

			for (POSITION pos = v.GetStartPosition (); pos; ) {
				CString strFont, strFile;

				v.GetNextAssoc (pos, strFont, strFile);

				if (!InstallFont (strFile))
					vFailed.Add (strFont);
			}

			::SendMessage (HWND_BROADCAST, WM_FONTCHANGE, 0, 0);

			if (vFailed.GetSize ()) {
				CString str = LoadString (IDS_FONTINSTALLFAILED);

				for (int i = 0; i < vFailed.GetSize (); i++)
					str += _T ("\r\n") + vFailed [i];

				TRACEF (str);
				MsgBox (str);
			}
		}
	}
}

ELEMENT_API void FoxjetCommon::EnumDSNs (CStringArray & vDSN, FoxjetDatabase::COdbcDatabase & db) 
{
	using namespace FoxjetDatabase;

	CArray <TASKSTRUCT, TASKSTRUCT &> vTasks;

	GetTaskRecords (db, ALL, vTasks);

	for (int nTask = 0; nTask < vTasks.GetSize (); nTask++) {
		TASKSTRUCT & task = vTasks [nTask];

		for (int nMsg = 0; nMsg < task.m_vMsgs.GetSize (); nMsg++) {
			MESSAGESTRUCT & msg = task.m_vMsgs [nMsg];
			CStringArray v;

			ParsePackets (msg.m_strData, v);

			for (int i = 0; i < v.GetSize (); i++) {
				CStringArray vPacket;
				CString & strSeg = v [i];

				Tokenize (strSeg, vPacket);

				if (vPacket.GetSize ()) { 
					CString & strToken = vPacket [0];
					int nType = GetElementType (strToken);

					if (nType == DATABASE) {
						const int nIndex = 11;

						if (vPacket.GetSize () >= nIndex) {
							CString strDSN = vPacket [nIndex];

							if (Find (vDSN, strDSN) == -1)
								vDSN.Add (strDSN);
						}
					}
					else if (nType == BARCODE) {
						CStringArray vSub;

						ParsePackets (strSeg, vSub);

						for (int nSub = 0; nSub < vSub.GetSize (); nSub++) {
							CStringArray vElement;

							TRACEF (vSub [nSub]);
							Tokenize (vSub [nSub], vElement);

							if (vElement.GetSize ()) {
								const CString strPrefix = _T ("DatabaseObj");

								if (vElement [0].Find (strPrefix) != -1) {
									CString str = vElement [0];

									vElement [0] = strPrefix;
									str.Replace (strPrefix, _T (""));

									if (str.GetLength ())
										vElement.InsertAt (1, str);
								}

								if (!vElement [0].CompareNoCase (_T ("DatabaseObj"))) {
									const int nIndex = 4;

									if (vElement.GetSize () >= nIndex) {
										CString strDSN = vElement [nIndex];

										if (Find (vDSN, strDSN) == -1)
											vDSN.Add (strDSN);
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
