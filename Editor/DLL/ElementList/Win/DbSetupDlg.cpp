// DbSetupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ODBCInst.h"
#include "resource.h"
#include "list.h"
#include "DbSetupDlg.h"
#include "DatabaseDlg.h"
#include "Debug.h"
#include "TemplExt.h"
#include "Extern.h"
#include "Parse.h"
#include "FileExt.h"
#include "MsgBox.h"

using namespace ListGlobals;
using namespace FoxjetElements;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDbSetupDlg dialog


CDbSetupDlg::CDbSetupDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(IDD_DATABASE_SETUP, pParent)
{
	//{{AFX_DATA_INIT(CDbSetupDlg)
	m_strDSN = _T("");
	m_strFile = _T("");
	//}}AFX_DATA_INIT
}


void CDbSetupDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDbSetupDlg)
	DDX_Text(pDX, TXT_DSN, m_strDSN);
	DDX_Text(pDX, TXT_FILE, m_strFile);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDbSetupDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDbSetupDlg)
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_EN_CHANGE(TXT_DSN, OnChangeDsn)
	ON_BN_CLICKED(BTN_OTHER, OnOther)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDbSetupDlg message handlers

void CDbSetupDlg::OnBrowse() 
{
	if (::GetFileAttributes (m_strFile) == -1)
		m_strFile = FoxjetDatabase::GetHomeDir () + _T ("\\") + FoxjetCommon::GetDatabaseName ();

	CFileDialog dlg (TRUE, _T ("*.mdb"), m_strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("Microsoft Access/Excel files (*.mdb, *.mde, *.xls, *.xlsx)|*.mdb;*.mde;*.xls;*.xlsx||Microsoft Access databases (*.mdb, *.mde)|*.mdb;*.mde|Microsoft Excel files (*.xls, *.xlsx)|*.xls;*.xlsx||"), 
		this);
	
	if (m_strFile.Find (_T (".xls")) != -1)
		dlg.m_ofn.nFilterIndex = 2;

	if (dlg.DoModal () == IDOK) {
		SetDlgItemText (TXT_DSN, dlg.GetFileTitle ());
		SetDlgItemText (TXT_FILE, dlg.GetPathName ());
		UpdateUI ();
	}
}

bool CDbSetupDlg::CreateAccessDsn ()
{
	static const LPCTSTR lpszDBDriver = SQL_MDB_DRIVER;

	CStringArray v;

	COdbcDatabase::GetDsnEntries (v);

	if (Find (v, m_strDSN, false) != -1) {
		CString strFile = COdbcDatabase::GetFilePath (m_strDSN);
		CString str;

		str.Format (LoadString (IDS_DSNEXISTS), m_strDSN, strFile);

		if (MsgBox (* this, str, (LPCTSTR)NULL, MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
			return false;
	}

	if (::GetFileAttributes (m_strFile) == -1) {
		CString str;

		str.Format (LoadString (IDS_FILENOTFOUND), m_strFile);

		MsgBox (* this, str, (LPCTSTR)NULL, MB_ICONSTOP | MB_OK);
		return false; 
	}

	LPCTSTR lpszFormat = 
		_T ("DSN=%s~ ")					// dsn
		_T ("DBQ=%s\\%s~ ")				// file
		_T ("Description=%s~ ")			// dsn
		_T ("FileType=MicrosoftAccess~ ")
		_T ("DataDirectory=%s~ ")
		_T ("MaxScanRows=20~ ")
		_T ("DefaultDir=%s~ ")
		_T ("FIL=MS Access;~ ")
		_T ("MaxBufferSize=2048~ ")
		_T ("MaxScanRows=8~ ")
		_T ("PageTimeout=5~ ")
		_T ("Threads=3~ ");
	TCHAR sz [512];
	TCHAR /*char*/ szAttr [512];
	CString strTitle, strDir = m_strFile;

	{
		int nIndex = m_strFile.ReverseFind ('\\');

		if (nIndex != -1) { 
			strDir = m_strFile.Left (nIndex);
			strTitle = m_strFile.Mid (nIndex + 1);
		}
	}

	_stprintf (sz, lpszFormat, 
		m_strDSN, 
		strDir, strTitle,
		m_strDSN,
		strDir, 
		strDir);
	int nLen = _tcsclen (sz);

	_tcscpy (szAttr, /*w2a*/ (sz));
	TRACEF (szAttr);

	for (int i = 0; i < nLen; i++)
		if (szAttr [i] == '~')
			szAttr [i] = '\0';

	bool bResult = ::SQLConfigDataSource (NULL, ODBC_ADD_SYS_DSN, /*w2a*/ (lpszDBDriver), szAttr) ? true : false;

	if (!bResult)
		TRACE_SQLERROR ();

	return bResult;
}

bool CDbSetupDlg::CreateExcelDsn ()
{
	static const LPCTSTR lpszXlsDriver	= SQL_XLS_DRIVER;
	static const LPCTSTR lpszXlsxDriver = SQL_XLSX_DRIVER;
	bool bXLSX = false;
	CStringArray v;

	COdbcDatabase::GetDsnEntries (v);

	if (Find (v, m_strDSN, false) != -1) {
		CString strFile = COdbcDatabase::GetFilePath (m_strDSN);
		CString str;

		str.Format (LoadString (IDS_DSNEXISTS), m_strDSN, strFile);

		if (MsgBox (* this, str, (LPCTSTR)NULL, MB_ICONEXCLAMATION | MB_YESNO) == IDNO)
			return false;
	}

	if (::GetFileAttributes (m_strFile) == -1) {
		CString str;

		str.Format (LoadString (IDS_FILENOTFOUND), m_strFile);

		MsgBox (* this, str, (LPCTSTR)NULL, MB_ICONSTOP | MB_OK);
		return false; 
	}

	{
		CString s = m_strFile;

		s.MakeLower ();	
		bXLSX = s.Find (_T (".xlsx")) != -1;
	}

	LPCTSTR lpszFormat = 
		_T ("DSN=%s~ ")				// dsn
		_T ("DBQ=%s\\%s~ ")			// ex: C:\temp\debug\boxprinting.xls
		_T ("Description=%s~ ")		// dsn
		_T ("FileType=Excel~ ")		
		_T ("DataDirectory=%s~ ");	// dir

	TCHAR sz [512];
	TCHAR /*char*/ szAttr [512];
	CString strTitle, strDir = m_strFile;

	{
		int nIndex = m_strFile.ReverseFind ('\\');

		if (nIndex != -1) { 
			strDir = m_strFile.Left (nIndex);
			strTitle = m_strFile.Mid (nIndex + 1);
		}
	}

	_stprintf (sz, lpszFormat, 
		m_strDSN, 
		strDir, strTitle,
		m_strDSN,
		strDir);
	int nLen = _tcsclen (sz);

	_tcscpy (szAttr, /*w2a*/ (sz));
	TRACEF (szAttr);

	for (int i = 0; i < nLen; i++)
		if (szAttr [i] == '~')
			szAttr [i] = '\0';

	bool bResult = ::SQLConfigDataSource (NULL, ODBC_ADD_SYS_DSN, bXLSX ? lpszXlsxDriver : lpszXlsDriver, szAttr) ? true : false;

	if (!bResult)
		TRACE_SQLERROR ();

	return bResult;
}

void CDbSetupDlg::OnOK()
{
	if (!UpdateData ())
		return;

	if (m_strFile.GetLength () > 5) {
		int nIndex = m_strFile.ReverseFind ('.');

		if (nIndex != -1) {
			CString strExt = m_strFile.Mid (nIndex);
			bool bCreated = false;

			strExt.MakeLower ();

			if (!strExt.CompareNoCase (_T (".mdb")) || !strExt.CompareNoCase (_T (".mde"))) 
				bCreated = CreateAccessDsn ();
			else if (!strExt.CompareNoCase (_T (".xls")) || !strExt.CompareNoCase (_T (".xlsx"))) 
				bCreated = CreateExcelDsn ();

			if (bCreated)
				FoxjetCommon::CEliteDlg::OnOK ();
			else
				MsgBox (* this, LoadString (IDS_FAILEDTOCREATEDSN));
		}
	}
}

void CDbSetupDlg::OnChangeDsn() 
{
	CString str;

	GetDlgItemText (TXT_DSN, str);

	bool bWarn = !str.CompareNoCase (FoxjetCommon::GetDSN ());

	if (CWnd * p = GetDlgItem (LBL_WARNING)) 
		p->ShowWindow (bWarn ? SW_SHOW : SW_HIDE);
	if (CWnd * p = GetDlgItem (IDI_DATABASE_SETUP_ICON)) 
		p->ShowWindow (bWarn ? SW_SHOW : SW_HIDE);
}

BOOL CDbSetupDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	if (CWnd * p = GetDlgItem (LBL_WARNING)) {
		CString str;

		str.Format (LoadString (IDS_OVERWRITEWARNING), FoxjetCommon::GetDSN ());
		p->SetWindowText (str);
	}
	
	OnChangeDsn ();

	if (!GetParent()->IsKindOf (RUNTIME_CLASS (CDatabaseDlg)))
		if (CWnd * p = GetDlgItem (BTN_OTHER))
			p->EnableWindow (FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDbSetupDlg::OnOther() 
{
	if (CWnd * p = GetParent ())
		p->PostMessage (WM_COMMAND, MAKEWPARAM (BTN_BROWSE, BN_CLICKED), NULL);

	FoxjetCommon::CEliteDlg::OnCancel ();
}

void CDbSetupDlg::UpdateUI()
{
	CString strFile;
	bool bEnabled = false;

	GetDlgItemText (TXT_FILE, strFile);

	if (strFile.GetLength () > 5) {
		int nIndex = strFile.ReverseFind ('.');

		if (nIndex != -1) {
			CString strExt = strFile.Mid (nIndex);

			strExt.MakeLower ();

			bEnabled = 
				!strExt.CompareNoCase (_T (".mdb")) ||
				!strExt.CompareNoCase (_T (".mde")) ||
				!strExt.CompareNoCase (_T (".xls")) ||
				!strExt.CompareNoCase (_T (".xlsx"));
			}
	}

	if (CWnd * p = GetDlgItem (IDOK))
		p->EnableWindow (bEnabled);
}
