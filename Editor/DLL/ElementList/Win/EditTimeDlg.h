#if !defined(AFX_EDITTIMEDLG_H__1BE2E2CC_879E_4850_8A33_B72D6EC98328__INCLUDED_)
#define AFX_EDITTIMEDLG_H__1BE2E2CC_879E_4850_8A33_B72D6EC98328__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EditTimeDlg.h : header file
//

#include "resource.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CEditTimeDlg dialog

class CEditTimeDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CEditTimeDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEditTimeDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	CTime	m_tm;
	int		m_nDirection;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditTimeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual BOOL OnInitDialog();

	// Generated message map functions
	//{{AFX_MSG(CEditTimeDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITTIMEDLG_H__1BE2E2CC_879E_4850_8A33_B72D6EC98328__INCLUDED_)
