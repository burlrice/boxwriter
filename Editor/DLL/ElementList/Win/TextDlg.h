#if !defined(AFX_TEXTDLG_H__471C7421_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_TEXTDLG_H__471C7421_8B3D_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TextDlg.h : header file
//

#include "resource.h"
#include "BaseTextDlg.h"
#include "TextElement.h"

/////////////////////////////////////////////////////////////////////////////
// CTextDlg dialog

class CTextDlg : public CBaseTextDlg
{
// Construction
public:
	CTextDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   
	CTextDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		UINT nID, const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   


// Dialog Data
	//{{AFX_DATA(CTextDlg)
	CString	m_strText;
	//}}AFX_DATA
	int m_nParagraphGap;
	ULONG m_lLink;

	FoxjetCommon::ALIGNMENT m_alignPara;

	void Get (const FoxjetElements::TextElement::CTextElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);
	void Set (FoxjetElements::TextElement::CTextElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTextDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	virtual UINT GetDefCtrlID () const { return TXT_TEXT; }

// Implementation
protected:
	virtual bool GetValues ();

	static ULONG CALLBACK CTextDlg::CharMapFunc (LPVOID lpData);
	void UpdateUI();

	DWORD m_dwProcessId;

	// Generated message map functions
	//{{AFX_MSG(CTextDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	afx_msg void OnCharMap ();
	afx_msg void OnChangeLink ();

	virtual int BuildElement ();
	const FoxjetElements::TextElement::CTextElement & GetElement () const;
	FoxjetElements::TextElement::CTextElement & GetElement ();

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXTDLG_H__471C7421_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
