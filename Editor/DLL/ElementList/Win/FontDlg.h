#if !defined(AFX_FONTDLG_H__21E62ED5_5EA2_470F_B480_2B87B6382C4F__INCLUDED_)
#define AFX_FONTDLG_H__21E62ED5_5EA2_470F_B480_2B87B6382C4F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FontDlg.h : header file
//

#include "Resource.h"
#include "Database.h"
#include "PreviewCtrl.h"
#include "TextElement.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CFontDlg dialog

class CFontDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CFontDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFontDlg ();

// Dialog Data
	//{{AFX_DATA(CFontDlg)
	//}}AFX_DATA


	FoxjetDatabase::HEADSTRUCT	m_head;
	CString						m_strSampleText;
	CString						m_strFont;
	int							m_nSize;
	BOOL						m_bItalic;
	BOOL						m_bBold;
	int							m_nWidth;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFontDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	FoxjetElements::TextElement::CTextElement *	m_pElement;
	CPreviewCtrl								m_wndPreview;
	bool										m_bValve;

	// Generated message map functions
	//{{AFX_MSG(CFontDlg)
	afx_msg void OnUpdate();
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FONTDLG_H__21E62ED5_5EA2_470F_B480_2B87B6382C4F__INCLUDED_)
