#pragma once

#include "DbWizard.h"
#include "DatabaseStartDlg.h"

// CDbWizardDbStartDlg

namespace FoxjetElements
{
	class ELEMENT_API CDbWizardDbStartDlg : public CDbWizardBasePage
	{
		DECLARE_DYNAMIC(CDbWizardDbStartDlg)

	public:
		CDbWizardDbStartDlg(CDbWizardDlg * pParent);
		virtual ~CDbWizardDbStartDlg();

		FoxjetUtils::CDatabaseStartParams m_params;
		bool m_bEnabled;

	protected:
		virtual bool CanAdvance ();
		virtual BOOL OnSetActive();
		virtual BOOL OnInitDialog ();

		afx_msg void OnDbStart ();
		afx_msg void OnUpdateUI ();

		DECLARE_MESSAGE_MAP()
	};
};

