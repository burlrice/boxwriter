// C39BcParamsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "C39BcParamsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CC39BcParamsDlg dialog


CC39BcParamsDlg::CC39BcParamsDlg(FoxjetDatabase::HEADTYPE type, CWnd* pParent /*=NULL*/)
:	CCode128ParamsDlg (IDD_C39PARAMS, type, pParent)
{
	//{{AFX_DATA_INIT(CC39BcParamsDlg)
	//}}AFX_DATA_INIT
}


void CC39BcParamsDlg::DoDataExchange(CDataExchange* pDX)
{
	CCode128ParamsDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CC39BcParamsDlg)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CC39BcParamsDlg, CCode128ParamsDlg)
	//{{AFX_MSG_MAP(CC39BcParamsDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CC39BcParamsDlg message handlers
