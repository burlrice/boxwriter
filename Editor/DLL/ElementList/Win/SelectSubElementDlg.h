// SelectSubElementDlg.h: interface for the CSelectSubElementDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SELECTSUBELEMENTDLG_H__97A2D106_8C96_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_SELECTSUBELEMENTDLG_H__97A2D106_8C96_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AiListDlg.h"


class CSelectSubElementDlg : public CAiListDlg  
{
// Construction
public:
	CSelectSubElementDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectSubElementDlg ();

// Dialog Data
	//{{AFX_DATA(CSelectSubElementDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


	CArray <const FoxjetElements::CSubElement *, const FoxjetElements::CSubElement *> m_vSelected;
	bool m_bAI;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectSubElementDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual BOOL OnInitDialog();

	virtual void OnOK();

	// Generated message map functions
	//{{AFX_MSG(CSelectSubElementDlg)
	//}}AFX_MSG

	virtual afx_msg void OnDblclkListCtrl(NMHDR* pNMHDR, LRESULT* pResult);

	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_SELECTSUBELEMENTDLG_H__97A2D106_8C96_11D4_915E_00104BEF6341__INCLUDED_)
