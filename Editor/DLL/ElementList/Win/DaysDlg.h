#if !defined(AFX_DAYSDLG_H__B4E2844C_398E_4554_A0E5_6EDC268BEDEE__INCLUDED_)
#define AFX_DAYSDLG_H__B4E2844C_398E_4554_A0E5_6EDC268BEDEE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DaysDlg.h : header file
//

#include "ListCtrlImp.h"
#include "resource.h"
#include "DateTimePropShtDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CDaysDlg dialog

class CDaysDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CDaysDlg)

	class CItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CItem (const CString & str, int nIndex, CDaysDlg * pParent);
		virtual ~CItem ();

		virtual CString GetDispText (int nColumn) const;
		virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;

		CString m_str;
		int m_nIndex;
		CDaysDlg * m_pParent;
	};

protected:
	CDaysDlg (); // only implemented for DYNCREATE

// Construction
public:

	CDaysDlg (FoxjetDatabase::COdbcDatabase & db, const CodesDlg::CCodesArray & vCodes);
	CDaysDlg (FoxjetDatabase::COdbcDatabase & db, UINT nIDTemplate, UINT nIDCaption = 0);
	CDaysDlg (FoxjetDatabase::COdbcDatabase & db, LPCTSTR lpszTemplateName, UINT nIDCaption = 0);
	~CDaysDlg();

public:
	ULONG GetCurLine () const;

// Dialog Data
	//{{AFX_DATA(CDaysDlg)
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CDaysDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual int GetListCtrlID () const { return LV_TYPE; }

	afx_msg void OnDblclkType(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEdit() ;

	virtual CodesDlg::CCodes GetCodes (ULONG lLineID) const;
	virtual void Update (ULONG lLineID, UINT nIndex, const CString & strValue);

	FoxjetDatabase::COdbcDatabase & m_db;
	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
	CodesDlg::CCodesArray m_vCodes;

	// Generated message map functions
	//{{AFX_MSG(CDaysDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeLine();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DAYSDLG_H__B4E2844C_398E_4554_A0E5_6EDC268BEDEE__INCLUDED_)
