// DatabaseElement.cpp: implementation of the CDatabaseElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "list.h"
#include "DatabaseElement.h"
#include "BitmapElement.h"
#include "Debug.h"
#include "Extern.h"
#include "Utils.h"
#include "OdbcTable.h"
#include "OdbcRecordset.h"
#include "TemplExt.h"
#include "Parse.h"
#include "DbFormatDlg.h"
#include "Extern.h"
#include "Registry.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetElements;
using namespace FoxjetCommon::ElementFields;

//CCriticalSection csDatabase;
static CMapStringToPtr vSQLType;		

//#ifdef _DEBUG
static CString GetSQLType (DWORD dwType)
{
	typedef struct tagSQLTYPESTRUCT
	{
		tagSQLTYPESTRUCT (DWORD dw, LPCTSTR lpsz = NULL) 
			: m_dw (dw), m_str (lpsz) 
		{ 
		}

		DWORD m_dw;
		CString m_str;

	} SQLTYPESTRUCT;

	#define MAKESQLTYPESTRUCT(n) SQLTYPESTRUCT(n, _T ( #n ))

	SQLTYPESTRUCT types [] = 
	{
		MAKESQLTYPESTRUCT (SQL_CHAR),
		MAKESQLTYPESTRUCT (SQL_VARCHAR),
		MAKESQLTYPESTRUCT (SQL_LONGVARCHAR),
		MAKESQLTYPESTRUCT (SQL_WCHAR),
		MAKESQLTYPESTRUCT (SQL_WVARCHAR),
		MAKESQLTYPESTRUCT (SQL_WLONGVARCHAR),
		MAKESQLTYPESTRUCT (SQL_DECIMAL),
		MAKESQLTYPESTRUCT (SQL_NUMERIC),
		MAKESQLTYPESTRUCT (SQL_SMALLINT),
		MAKESQLTYPESTRUCT (SQL_INTEGER),
		MAKESQLTYPESTRUCT (SQL_REAL),
		MAKESQLTYPESTRUCT (SQL_FLOAT),
		MAKESQLTYPESTRUCT (SQL_DOUBLE),
		MAKESQLTYPESTRUCT (SQL_BIT),
		MAKESQLTYPESTRUCT (SQL_TINYINT),
		MAKESQLTYPESTRUCT (SQL_BIGINT),
		MAKESQLTYPESTRUCT (SQL_BINARY),
		MAKESQLTYPESTRUCT (SQL_VARBINARY),
		MAKESQLTYPESTRUCT (SQL_LONGVARBINARY),
		MAKESQLTYPESTRUCT (SQL_TYPE_DATE),
		MAKESQLTYPESTRUCT (SQL_TYPE_TIME),
		MAKESQLTYPESTRUCT (SQL_TYPE_TIMESTAMP),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_MONTH),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_YEAR),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_YEAR_TO_MONTH),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_DAY),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_HOUR),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_MINUTE),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_SECOND),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_DAY_TO_HOUR),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_DAY_TO_MINUTE),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_DAY_TO_SECOND),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_HOUR_TO_MINUTE),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_HOUR_TO_SECOND),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_MINUTE_TO_SECOND),
		MAKESQLTYPESTRUCT (SQL_GUID),
		SQLTYPESTRUCT (0)
	};

	for (int i = 0; types [i].m_str.GetLength (); i++)
		if (types [i].m_dw == dwType)
			return types [i].m_str;

	CString str;
	str.Format (_T ("GetSQLType: unknown type: %d"), dwType);
	TRACEF (str);

	return _T ("FOOBAR");
}
//#endif //_DEBUG

IMPLEMENT_DYNAMIC (CDatabaseElement, CTextElement);

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

const FoxjetCommon::LIMITSTRUCT CDatabaseElement::m_lmtKey = { 0,	lmtString.m_dwMax };
const DWORD CDatabaseElement::m_dwOpenEx = /* CDatabase::openReadOnly | */ CDatabase::noOdbcDialog;
const CString CDatabaseElement::m_strNA = _T ("[na]");

CString CDatabaseElement::GetAuto () { return _T ("[") + LoadString (IDS_AUTO) + _T("]"); }
CString CDatabaseElement::GetStatic () { return _T ("[") + LoadString (IDS_STATIC) + _T("]"); };
CString CDatabaseElement::GetNative () { return _T ("[") + LoadString (IDS_NATIVE) + _T("]"); };


CDatabaseElement::CDatabaseElement (const FoxjetDatabase::HEADSTRUCT & head)
:	m_strDSN (_T ("MarksmanELITE")), 
	m_strTable (_T ("Messages")), 
	m_strField (_T ("ID")),
	m_strKeyField (_T ("")),
	m_strKeyValue (_T ("")),
	m_nRow (0),
	m_bSQL (false),
	m_bPrompt (false),
	m_nSQLType (SQL_INTEGER),
	m_bInvalid (true),
	m_bSerial (false),
	m_bSerialDownload (false),
	m_nFormat (0),
	m_strEmptyString (_T ("[NO RECORD]")),
	m_bMaxWidth (false),
	m_lMaxWidth (5000),
	m_nFontMax (0),
	m_bDrawing (false),
	CTextElement (head)
{
	m_strDSN = FoxjetCommon::GetDSN ();

	{
		CString str = COdbcDatabase::GetAppDsn ();

		if (str.GetLength ())
			m_strDSN = str;
	}
}

CDatabaseElement::CDatabaseElement (const CDatabaseElement & rhs)
:	m_strDSN (rhs.m_strDSN), 
	m_strTable (rhs.m_strTable), 
	m_strField (rhs.m_strField),
	m_strKeyField (rhs.m_strKeyField),
	m_strKeyValue (rhs.m_strKeyValue),
	m_nRow (rhs.m_nRow),
	m_bSQL (rhs.m_bSQL),
	m_strSQL (rhs.m_strSQL),
	m_bPrompt (rhs.m_bPrompt),
	m_nSQLType (rhs.m_nSQLType),
	m_bInvalid (true),
	m_bSerial (rhs.m_bSerial),
	m_bSerialDownload (rhs.m_bSerialDownload),
	m_nFormat (rhs.m_nFormat),
	m_strFormat (rhs.m_strFormat),
	m_strEmptyString (rhs.m_strEmptyString),
	m_bMaxWidth (rhs.m_bMaxWidth),
	m_lMaxWidth (rhs.m_lMaxWidth),
	m_nFontMax (rhs.m_nFontMax),
	m_bDrawing (false),
	m_strBmpWidthField  (rhs.m_strBmpWidthField),
	m_strBmpHeightField (rhs.m_strBmpHeightField),
	CTextElement (rhs)
{
}

CDatabaseElement & CDatabaseElement::operator = (const CDatabaseElement & rhs)
{
	CTextElement::operator = (rhs);

	if (this != & rhs) {
		m_strDSN				= rhs.m_strDSN; 
		m_strTable				= rhs.m_strTable;
		m_strField				= rhs.m_strField;
		m_strKeyField			= rhs.m_strKeyField;
		m_strKeyValue			= rhs.m_strKeyValue;
		m_nRow					= rhs.m_nRow;
		m_bSQL					= rhs.m_bSQL;
		m_strSQL				= rhs.m_strSQL;
		m_bPrompt				= rhs.m_bPrompt;
		m_nSQLType				= rhs.m_nSQLType;
		m_bSerial				= rhs.m_bSerial;
		m_bSerialDownload		= rhs.m_bSerialDownload;
		m_nFormat				= rhs.m_nFormat;
		m_strFormat				= rhs.m_strFormat;
		m_strEmptyString		= rhs.m_strEmptyString;
		m_bMaxWidth				= rhs.m_bMaxWidth;
		m_lMaxWidth				= rhs.m_lMaxWidth;
		m_nFontMax				= rhs.m_nFontMax;
		m_strBmpWidthField		= rhs.m_strBmpWidthField;
		m_strBmpHeightField		= rhs.m_strBmpHeightField;
	}

	return * this;
}

CDatabaseElement::~CDatabaseElement()
{
}

// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
static const ElementFields::FIELDSTRUCT fields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType,		_T ("")				),	// 0:	Prefix (required)
	ElementFields::FIELDSTRUCT (m_lpszID,				_T ("")				),	// 1:	ID (required)	
	ElementFields::FIELDSTRUCT (m_lpszX,				_T ("")				),	// 2:	x (required)
	ElementFields::FIELDSTRUCT (m_lpszY,				_T ("")				),	// 3:	y (required)
	ElementFields::FIELDSTRUCT (m_lpszFlipH,			_T ("0")			),	// 4:	flip h
	ElementFields::FIELDSTRUCT (m_lpszFlipV,			_T ("0")			),	// 5:	flip v
	ElementFields::FIELDSTRUCT (m_lpszInverse,			_T ("0")			),	// 6:	inverse
	ElementFields::FIELDSTRUCT (m_lpszFontName,			_T ("Courier New")	),	// 7:	font name	
	ElementFields::FIELDSTRUCT (m_lpszFontSize,			_T ("32")			),	// 8:	font size	
	ElementFields::FIELDSTRUCT (m_lpszBold,				_T ("0")			),	// 9:	bold	
	ElementFields::FIELDSTRUCT (m_lpszItalic,			_T ("0")			),	// 10:	italic	
	ElementFields::FIELDSTRUCT (m_lpszDSN,				_T ("MarksmanELITE")),	// 11:	DSN
	ElementFields::FIELDSTRUCT (m_lpszTable,			_T ("Messages")		),	// 12:	table 
	ElementFields::FIELDSTRUCT (m_lpszField,			_T ("ID")			),	// 13:	field
	ElementFields::FIELDSTRUCT (m_lpszKeyField,			_T ("")				),	// 14:	key field	
	ElementFields::FIELDSTRUCT (m_lpszKeyValue,			_T ("")				),	// 15:	key value	
	ElementFields::FIELDSTRUCT (m_lpszRow,				_T ("0")			),	// 16:	row
	ElementFields::FIELDSTRUCT (m_lpszSQLb,				_T ("0")			),	// 17:	use SQL
	ElementFields::FIELDSTRUCT (m_lpszSQLstr,			_T ("")				),	// 18:	SQL statement
	ElementFields::FIELDSTRUCT (m_lpszOrient,			_T ("0")			),	// 19:	orientation
	ElementFields::FIELDSTRUCT (m_lpszWidth,			_T ("0")			),	// 20:	font width
	ElementFields::FIELDSTRUCT (m_lpszPromptAtStart,	_T ("0")			),	// 21:	prompt at task start
	ElementFields::FIELDSTRUCT (m_lpszSQLType,			_T ("4")			),	// 22:	SQL type [SQL_INTEGER]
	ElementFields::FIELDSTRUCT (m_lpszSerial,			_T ("40")			),	// 23:	get key field from serial data
	ElementFields::FIELDSTRUCT (m_lpszPort,				_T ("0")			),	// 24:	serial download
	ElementFields::FIELDSTRUCT (m_lpszParaAlign,		_T ("0")			),	// 25:	paragraph alignment
	ElementFields::FIELDSTRUCT (m_lpszParaGap,			_T ("0")			),	// 26:  paragraph gap
	ElementFields::FIELDSTRUCT (m_lpszResizable,		_T ("0")			),	// 27:	resizable
	ElementFields::FIELDSTRUCT (m_lpszParaWidth,		_T ("0")			),	// 28:	paragraph width
	ElementFields::FIELDSTRUCT (m_lpszFormatType,		_T ("0")			),	// 29:	format type
	ElementFields::FIELDSTRUCT (m_lpszFormat,			_T ("")				),	// 30:	format
	ElementFields::FIELDSTRUCT (m_lpszEmptyString,		_T ("[NO RECORD]")	),	// 31:	empty string
	ElementFields::FIELDSTRUCT (m_lpszMaxWidthEnable,	_T ("0")			),	// 32:	max width enable
	ElementFields::FIELDSTRUCT (m_lpszMaxWidth,			_T ("5000")			),	// 33:	max width
	ElementFields::FIELDSTRUCT (m_lpszAlign,			_T ("0")			),	// 34:  alignment
	ElementFields::FIELDSTRUCT (m_lpszBmpWidth,			_T ("")				),	// 35:  bitmap width
	ElementFields::FIELDSTRUCT (m_lpszBmpHeight,		_T ("")				),	// 36:  bitmap height
	ElementFields::FIELDSTRUCT (m_lpszDither,			_T ("0")			),	// 37:
	ElementFields::FIELDSTRUCT (m_lpszColor,			_T ("0")			),	// 37:
	ElementFields::FIELDSTRUCT (),
};

static CString FormatEmptyString (const CString & str)
{
	return FormatString (str.IsEmpty () ? _T ("\\0") : str);
}

static CString UnformatEmptyString (const CString & str)
{
	CString strResult = UnformatString (str);

	return UnformatString (strResult == _T ("\\0") ? _T ("") : strResult);
}

CString CDatabaseElement::ToString (const FoxjetCommon::CVersion & ver) const
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated

	CString str;
	CPrinterFont f = GetFont ();
	CPoint pt = GetPosInternal (ALIGNMENT_ADJUST_ADD);
	bool bKeyValue = !m_strKeyField.IsEmpty ();// && !m_strKeyValue.IsEmpty ();

	str.Format (_T ("{Database,%u,%u,%u,%d,%d,%d,%s,%u,%d,%d,%s,%s,%s,%s,%s,%d,%d,%s,%u,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%s,%s,%d,%d,%d,%s,%s,%d,%06X}"),
		GetID (),
		pt.x, pt.y,
		IsFlippedH (),
		IsFlippedV (),
		IsInverse (),
		f.m_strName, f.m_nSize, f.m_bBold, f.m_bItalic,
		m_strDSN,
		m_strTable,
		m_strField,
		bKeyValue ? m_strKeyField : m_strNA,
		bKeyValue ? m_strKeyValue : m_strNA,
		m_nRow,
		m_bSQL,
		FormatString (m_strSQL),
		(int)GetOrientation (),
		GetWidth (),
		GetPromptAtTaskStart (),
		GetSQLType (),
		GetSerial (),
		GetSerialDownload (),
		GetParaAlign (),
		GetParaGap (),
		IsResizable (),
		GetParaWidth (),
		GetFormatType (),
		FormatString (GetFormat ()),
		FormatEmptyString (GetEmptyString ()),
		IsMaxWidthEnabled (),
		GetMaxWidth (),
		GetAlignment (),
		FormatString (m_strBmpWidthField),
		FormatString (m_strBmpHeightField),
		GetDither (),
		GetColor ());

	return str;
}

bool CDatabaseElement::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	CStringArray vstrToken;
	CPrinterFont f;

	Parse (str, ver, vstrToken);

	if (vstrToken [0].CompareNoCase (_T ("Database")) != 0){
		TRACEF ("Wrong prefix \"" + vstrToken [0] + "\"");
		return false;
	}

	f.m_strName = vstrToken [7];
	m_nFontMax = f.m_nSize = (UINT)_tcstol (vstrToken [8], NULL, 10);
	f.m_bBold = _ttoi (vstrToken [9]) ? true : false;
	f.m_bItalic = _ttoi (vstrToken [10]) ? true : false;

	bool bResult = true;

	bResult &= SetID ((UINT)_tcstol (vstrToken [1], NULL, 10));
	bResult &= SetPos (CPoint (
		(UINT)_tcstol (vstrToken [2], NULL, 10), 
		(UINT)_tcstol (vstrToken [3], NULL, 10)), NULL);
	bResult &= SetFlippedH (_ttoi (vstrToken [4]) ? true : false);
	bResult &= SetFlippedV (_ttoi (vstrToken [5]) ? true : false);
	bResult &= SetInverse (_ttoi (vstrToken [6]) ? true : false);
	bResult &= SetFont (f);
	bResult &= SetDSN (vstrToken [11]);
	bResult &= SetTable (vstrToken [12]);
	bResult &= SetField (vstrToken [13]);
	bResult &= SetKeyField (vstrToken [14]);
	bResult &= SetKeyValue (vstrToken [15]);
	bResult &= SetRow ((UINT)_tcstol (vstrToken [16], NULL, 10));
	bResult &= SetSQL ((UINT)_tcstol (vstrToken [17], NULL, 10) ? true : false, UnformatString (vstrToken [18]));
	bResult &= SetOrientation ((TextElement::FONTORIENTATION)_tcstol (vstrToken [19], NULL, 10));
	bResult &= SetWidth (_ttoi (vstrToken [20]));
	bResult &= SetPromptAtTaskStart (_ttoi (vstrToken [21]) ? true : false);
	SetSQLType (LookupSQLType ()); // bResult &= SetSQLType (_ttoi (vstrToken [22]));
	bResult &= SetSerial (_ttoi (vstrToken [23]) ? true : false);
	bResult &= SetSerialDownload (_ttoi (vstrToken [24]) ? true : false);
	bResult &= SetParaAlign ((ALIGNMENT)_ttoi (vstrToken [25]));
	bResult &= SetParaGap (_ttoi (vstrToken [26]));
	bResult &= SetResizable (_ttoi (vstrToken [27]) ? true : false);
	bResult &= SetParaWidth ((UINT)_tcstol (vstrToken [28], NULL, 10));
	bResult &= SetFormatType (_ttoi (vstrToken [29]));
	bResult &= SetFormat (UnformatString (vstrToken [30]));
	bResult &= SetEmptyString (UnformatEmptyString (vstrToken [31]));
	bResult &= SetMaxWidthEnabled (_ttoi (vstrToken [32]) ? true : false);
	bResult &= SetMaxWidth (_ttoi (vstrToken [33]));
	bResult &= SetAlignment ((ALIGNMENT)_ttoi (vstrToken [34]));
	bResult &= SetBmpWidthField (UnformatEmptyString (vstrToken [35]));
	bResult &= SetBmpHeightField (UnformatEmptyString (vstrToken [36]));
	bResult &= SetDither (_ttoi (vstrToken [37]));
	bResult &= SetColor (_tcstoul (vstrToken [38], NULL, 16));

	//TRACEF (str);
	//TRACEF (implode (vstrToken));
	//TRACEF (ToString (ver));

	return bResult;
}

void CDatabaseElement::Validate () const
{
	//LOCK (::csDatabase);

	try {
		COdbcCache db = COdbcDatabase::Find (GetDSN ());
		COdbcRecordset rst (db.GetDB ());
		CString strSQL = CreateSQL (&db.GetDB ());
		DWORD dwOptions = CRecordset::readOnly | CRecordset::noDirtyFieldCheck;

		TRACEF (strSQL);

		if (rst.Open (strSQL, db.GetDB ().GetOpenType (), dwOptions)) 
			rst.Close ();
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
}

static CMap <ULONG, ULONG, CString, CString> mapLines;
static CMapStringToString vSubstitute;

CString CDatabaseElement::CreateSQL (FoxjetDatabase::COdbcDatabase * pdb) const
{
	//LOCK (::csDatabase);
	CString str, strSubstitute;
	const CString strPlaceHolder = _T ("bw_key_value");

	if (m_bSQL) {
		const CString strComment = Extract (m_strSQL, _T ("/*"), _T ("*/"));

		str = m_strSQL;

		if (strComment.GetLength ()) {
			str.Replace (_T ("/*") + strComment + _T ("*/"), _T (""));
			//TRACEF (strComment);
		}

		if (m_mapGeneral.size ()) {
			const CString strDelim = IsSqlString (m_nSQLType) ? _T ("'") : _T ("");
			std::vector <std::wstring> v;

			if (GetKeyField ().GetLength ())
				v.push_back (std::wstring (GetKeyField () + _T ("=") + strDelim + GetKeyValue () + strDelim));

			for (auto i = m_mapGeneral.begin (); i != m_mapGeneral.end (); i++) 
				v.push_back (i->first + _T ("=") + i->second);

			str += CString (_T (" WHERE "))  + implode (v, std::wstring (_T (" AND "))).c_str ();
		}

		str.Replace (strPlaceHolder, GetKeyValue ());
		//TRACEF (str);
	}
	else
		str = COdbcDatabase::CreateSQL (pdb, GetDSN (), GetTable (), GetField (), GetKeyField (), GetKeyValue (), GetSQLType ());

	if (str.Find (strPlaceHolder) != -1) {
		CString strLine;
		ULONG lLineID = GetLineID ();

		if (!::mapLines.Lookup (lLineID, strLine)) {
			LINESTRUCT line;

			GetLineRecord (ListGlobals::GetDB (), lLineID, line);
			::mapLines.SetAt (lLineID, line.m_strName);
			strLine = line.m_strName;
		}

		CString strRegKey = _T ("Software\\FoxJet\\Control\\") + strLine + _T ("\\DbTaskStart");
		CString strKeyValue = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strRegKey, _T ("KeyValue"), _T ("")); 

		if (!strKeyValue.GetLength ())
			str = m_strEmptyString;
		else if (str.Replace (strPlaceHolder, strKeyValue)) {
			TRACEF (str);
		}
	}

	if (::vSubstitute.Lookup (str, strSubstitute))
		return strSubstitute;

	return str;
}

bool CDatabaseElement::SetFontSize (UINT nSize)
{
	bool bResult = CTextElement::SetFontSize (nSize);
	CDC dc;

	dc.CreateCompatibleDC (NULL);

	CTextElement::Draw (dc, m_head, true, EDITOR);
	m_nFontMax = nSize;

	return bResult;
}

CSize CDatabaseElement::Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly, 
						  FoxjetCommon::DRAWCONTEXT context)
{
	CSize size (0, 0);
	bool bResize = true;
	int nSize = m_nFontMax;
	const int nOriginal = GetFontSize ();

	if (context == EDITOR) {
		if (GetSQL () && GetImageData ().IsEmpty ()) {
			m_strImageData = m_strDefaultData = m_strEmptyString;
		}
	}

	m_bDrawing = true;

	for (int nTries = 0; (nTries < 255) && bResize; nTries++) {
		SetFontSize (nSize);
		size = CTextElement::Draw (dc, head, bCalcSizeOnly, context);
		double dStretch [2] = { 1, 1 };

		CBaseElement::CalcStretch (head, dStretch);
		ULONG lWidth = (int)((double)size.cx / dStretch [0]);

		bResize = (lWidth > m_lMaxWidth) && IsMaxWidthEnabled ();

		#ifdef _DEBUG
		if (bResize) { CString str; str.Format (_T ("m_lMaxWidth: %d, width: %d"), m_lMaxWidth, lWidth); TRACEF (str); }
		#endif

		if (bResize)
			nSize--;

	}

	m_nFontMax = nSize;
	m_bDrawing = false;
	SetFontSize (nOriginal);

	return size;
}

bool CDatabaseElement::IsBypassCache (const CString & strSQL) const
{
	if (m_bSQL) {
		CString s = strSQL;

		s.MakeLower ();

		if (s.Find (_T ("date")) != -1 || s.Find (_T ("now")) != -1)
			return true;
	}

	return false;
}

bool CDatabaseElement::TTL () const
{
	CString strSQL = CreateSQL (); 

	if (IsBypassCache (strSQL)) {
		CExpDateElement::SPAN span = GetTtlSpan (strSQL);
		CTime tmNow = CTime::GetCurrentTime ();
		CString strFormat;

		switch (span) {
		case CExpDateElement::SPAN_DAYS:	strFormat = _T ("%j");		break;
		case CExpDateElement::SPAN_HOURS:	strFormat = _T ("%H");		break;
		case CExpDateElement::SPAN_MINUTES:	strFormat = _T ("%M");		break;
		case CExpDateElement::SPAN_SECONDS:	strFormat = _T ("%S");		break;
		}

		CString strNow = CTime::GetCurrentTime ().Format (strFormat);
		CString strCreated = m_tmRstCache.Format (strFormat);

		return _ttoi (strCreated) == _ttoi (strNow);
	}

	return true;
}

void CDatabaseElement::Invalidate ()
{
	m_bInvalid = true;
	//TRACEF (CTime::GetCurrentTime ().Format (_T ("%c: ")) + _T ("COdbcDatabase::FreeRstCache: ") + m_strRstCacheKey);
	//COdbcDatabase::FreeRstCache (m_strRstCacheKey);
}

CExpDateElement::SPAN CDatabaseElement::GetTtlSpan (const CString & strSQL)
{
	CExpDateElement::SPAN result = CExpDateElement::SPAN_DAYS;
	CString s = strSQL;

	s.MakeLower ();

	struct
	{
		LPCTSTR m_lpsz;
		CExpDateElement::SPAN m_span;
	}
	static const map [] = 
	{
		{ _T ("yyyy"),	CExpDateElement::SPAN_DAYS,		}, // yyyy = Year
		{ _T ("q"),		CExpDateElement::SPAN_DAYS,		}, // q = Quarter
		{ _T ("m"),		CExpDateElement::SPAN_DAYS,		}, // m = month
		{ _T ("y"),		CExpDateElement::SPAN_DAYS,		}, // y = Day of the year
		{ _T ("d"), 	CExpDateElement::SPAN_DAYS,		}, // d = Day
		{ _T ("w"), 	CExpDateElement::SPAN_DAYS,		}, // w = Weekday
		{ _T ("ww"), 	CExpDateElement::SPAN_DAYS,		}, // ww = Week
		{ _T ("h"), 	CExpDateElement::SPAN_HOURS,	}, // h = hour
		{ _T ("n"), 	CExpDateElement::SPAN_MINUTES,	}, // n = Minute
		{ _T ("s"), 	CExpDateElement::SPAN_SECONDS,	}, // s = Second
	};

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		if (s.Find (_T ("'") + CString (map [i].m_lpsz) + _T ("'")) != -1)
			result = max (map [i].m_span, result);
	}

	return result;
}

int CDatabaseElement::BypassCache ()
{
	try
	{
		COdbcCache db = COdbcDatabase::Find (GetDSN ());
		CString strSQL = CreateSQL (&db.GetDB ()); 
		COdbcCache rstCache = COdbcDatabase::Find (db.GetDB (), GetDSN (), strSQL);
		COdbcRecordset & rst = rstCache.GetRst ();
		
		m_strDefaultData = rst.GetFieldValue ((short)0);
		m_strImageData = m_strDefaultData;
		m_strImageData.Replace (_T ("\\n"), _T ("\n"));
		m_bInvalid = false;
		//COdbcDatabase::FreeRstCache (rstCache.GetKey ());
	//TRACEF (m_strImageData + _T (" [") +  m_strDefaultData + _T ("]"));
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }

	return GetClassID ();
}

int CDatabaseElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	//LOCK (::csDatabase);
	CInternalFormatting opt (false);

	if (type == INITIAL || m_bInvalid) {
		bool bSubstitute = false;
		CString strSQL;
		const CString strOriginal = CreateSQL ();

		//if (IsBypassCache (CreateSQL ()))
		//	return BypassCache ();

		for (int nTries = 0; nTries < 2; nTries++) {
			CString strSQL = CreateSQL (); 

			m_strDefaultData = m_strEmptyString;

			if (strSQL != m_strDefaultData) {
				try {
					COdbcCache db = COdbcDatabase::Find (GetDSN ());
					DBMANAGETHREADSTATE (db.GetDB ());
					strSQL = CreateSQL (&db.GetDB ()); 
					COdbcCache rstCache = COdbcDatabase::Find (db.GetDB (), GetDSN (), strSQL);
					COdbcRecordset & rst = rstCache.GetRst ();

					#if 0 // #ifdef _DEBUG
					TRACEF (strSQL);

					for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
						CODBCFieldInfo info;

						rst.GetODBCFieldInfo (i, info);
						TRACEF (FoxjetDatabase::ToString (i) + _T (": ") + info.m_strName);
					}
					#endif

					if (IsBypassCache (strSQL)) {
						m_strRstCacheKey = rstCache.GetKey ();
						m_tmRstCache = rstCache.GetRst ().GetCreationTime ();
					}

					try {
						int nRow = GetRow ();

						if (nRow > 1) {
							rst.Move (nRow, SQL_FETCH_ABSOLUTE);
							rst.SetAbsolutePosition (nRow);
						}
					}
					catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
					catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }

					CODBCFieldInfo info;
					int nIndex = 0;

					if (!m_bSQL) {
						CString strField = GetField ();

						//TRACEF (m_strDSN + ": " + strField);
						//TRACEF (GetDSN () + _T (":") + strSQL);
						//TRACECACHE ();

						if (!rst.IsEOF ()) {
							CSize type = CBitmapElement::GetDbSize (m_sizeBmp, m_strBmpWidthField, m_strBmpHeightField);

							if (type.cx != CBitmapElement::DBSIZE_AUTO && type.cx != CBitmapElement::DBSIZE_STATIC) {
								int nBitmapWidthIndex = rst.GetFieldIndex (m_strBmpWidthField);

								if (nBitmapWidthIndex != -1) {
									double f = FoxjetDatabase::_ttof ((CString)rst.GetFieldValue ((short)nBitmapWidthIndex));

									m_sizeBmp.cx = (int)(f * 1000.0);
								}
							}

							if (type.cy != CBitmapElement::DBSIZE_AUTO && type.cy != CBitmapElement::DBSIZE_STATIC) {
								int nBitmapHeightIndex = rst.GetFieldIndex (m_strBmpHeightField);

								if (nBitmapHeightIndex != -1) {
									double f = FoxjetDatabase::_ttof ((CString)rst.GetFieldValue ((short)nBitmapHeightIndex));
										
									m_sizeBmp.cy = (int)(f * 1000.0);
								}
							}

							nIndex = rst.GetFieldIndex (strField);
							m_strDefaultData = (CString)rst.GetFieldValue (strField);
						}
					}
					else {
						if (!rst.IsEOF ()) {
							std::map <std::wstring, std::wstring> args = GetGeneralArgs (m_strSQL);
							CString strField = args [(std::wstring)m_lpszField].c_str ();

							if (strField.GetLength ())
								nIndex = rst.GetFieldIndex (strField);

							m_strDefaultData = (CString)rst.GetFieldValue (nIndex);
						}
					}

	//{ m_strImageData = m_strDefaultData; /* "TODO"; */ m_bInvalid = false; return GetClassID (); }
					rst.GetODBCFieldInfo (nIndex, info);
					m_nSQLType = info.m_nSQLType;

					switch (m_nSQLType) {
					case SQL_BINARY:
					case SQL_VARBINARY:
					case SQL_LONGVARBINARY:
						m_strDefaultData.Empty ();
						break;
					}

					//TRACEF (m_strDefaultData + ": " + strSQL + " [" + CString (type == IMAGING ? "IMAGING" : "INITIAL") + "]");

					switch (GetFormatType ()) {
					case CDbFormatDlg::INT:
						{
							long l = _ttol (m_strDefaultData);
							int nLeading = 0;

							VERIFY (_stscanf (GetFormat (), _T ("%%%dlu"), &nLeading) == 1);
							m_strDefaultData.Format (GetFormat (), l);

							if (m_strDefaultData.GetLength () > nLeading)
								m_strDefaultData = m_strDefaultData.Right (nLeading);

							m_strDefaultData.TrimLeft ();
							m_strDefaultData.TrimRight ();
						}
						break;
					case CDbFormatDlg::FLOAT:
						{
							long double f = FoxjetDatabase::_ttof (m_strDefaultData);
							long l = (long)floor (f);
							int nLeading = 0, nTailing = 0;
							bool bZeroes = GetFormat ().Find (_T ("%0")) != -1 ? true : false;
							CString str, strFmt;
							
							VERIFY (_stscanf (GetFormat (), _T ("%%%d.%df"), &nLeading, &nTailing) == 2);
							str.Format (GetFormat (), f);
							str = Extract (str, _T ("."));
							
							strFmt.Format (_T ("%%%s%dlu"), 
								bZeroes ? _T ("0") : _T (""),
								nLeading);
							m_strDefaultData.Format (strFmt, l);

							if (m_strDefaultData.GetLength () > nLeading)
								m_strDefaultData = m_strDefaultData.Right (nLeading);

							m_strDefaultData += _T (".") + str;
							m_strDefaultData.TrimLeft ();
							m_strDefaultData.TrimRight ();
						}
						break;
					case CDbFormatDlg::DATE_TIME:
						{
							COleDateTime t (100, 0, 0, 0, 0, 0);

							try {
								t.ParseDateTime (m_strDefaultData);
							}
							catch (COleException * e)		{ e->Delete (); }
							catch (CMemoryException * e)	{ e->Delete (); }

							m_strDefaultData = t.Format (GetFormat ());
						}
						break;
					}

					if (bSubstitute && nTries > 0) {
						TRACEF (strOriginal);
						TRACEF (strSQL);
						::vSubstitute.SetAt (strOriginal, strSQL);
					}

					break;
				}
				catch (CDBException * e) {
					if (nTries == 0) { 
						TRACEF (::GetSQLType (m_nSQLType));
						m_nSQLType = LookupSQLType ();
						TRACEF (::GetSQLType (m_nSQLType));
						bSubstitute = true;
						e->Delete ();
					}
					else {
						CElementException * p = NULL;

						if (bCanThrowException)
							p = new CElementException (e); 

						TRACEF (strSQL);
						HANDLEEXCEPTION_TRACEONLY (e); 
					
						if (p)
							throw p;
					}
				}
				catch (CMemoryException * e) { 
					CElementException * p = NULL;

					if (bCanThrowException)
						p = new CElementException (e); 
					
					HANDLEEXCEPTION_TRACEONLY (e); 
				
					if (p)
						throw p;
				}
			}
		}
	}

	m_strImageData = m_strDefaultData;
	m_strImageData.Replace (_T ("\\n"), _T ("\n"));
	m_bInvalid = false;

	//TRACEF (m_strImageData + _T (" [") +  m_strDefaultData + _T ("]"));

	return GetClassID ();
}

CString CDatabaseElement::GetDSN () const
{
	return m_strDSN;
}

bool CDatabaseElement::SetDSN (const CString & str) 
{
	//LOCK (::csDatabase);

	bool bResult = false;

	try {
		COdbcCache db = COdbcDatabase::Find (str);

		m_strDSN = str;
		SetRedraw ();
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }

	return bResult;
}

CString CDatabaseElement::GetTable () const
{
	return m_strTable;
}

bool CDatabaseElement::SetTable (const CString & str) 
{
	if (COdbcDatabase::FindTable (GetDSN (), str) != -1) {
		m_strTable = str;
		SetRedraw ();
		return true;
	}

	return false;
}

CString CDatabaseElement::GetField () const
{
	return m_strField;
}

bool CDatabaseElement::SetField (const CString & str) 
{
	if (COdbcDatabase::FindField (GetDSN (), GetTable (), str) != -1) {
		m_strField = str;
		SetRedraw ();
		return true;
	}

	return false;
}

CString CDatabaseElement::GetKeyField () const
{
	return m_strKeyField != m_strNA ? m_strKeyField : _T ("");
}

bool CDatabaseElement::SetKeyField (const CString & str) 
{
	// NULL value allowed
	if (!str.GetLength () || COdbcDatabase::FindField (GetDSN (), GetTable (), str) != -1) {
		m_strKeyField = str;
		SetRedraw ();
		return true;
	}

	return false;
}

CString CDatabaseElement::GetKeyValue () const
{
	return m_strKeyValue != m_strNA ? m_strKeyValue : _T ("");
}

bool CDatabaseElement::SetKeyValue (const CString & str) 
{
	if (IsValid (str.GetLength (), m_lmtKey)) {
		m_strKeyValue = str;
		SetRedraw ();
		m_bInvalid = true;
		return true;
	}

	return false;
}

int CDatabaseElement::GetRow () const
{
	return m_nRow;
}

bool CDatabaseElement::SetRow (int nRow) 
{
	m_nRow = nRow;
	SetRedraw ();
	return true;
}

std::map <std::wstring, std::wstring> CDatabaseElement::GetGeneralArgs (const CString & strSQL) 
{
	const CString strComment = Extract (strSQL, _T ("/*"), _T ("*/"));
	std::map <std::wstring, std::wstring> result;

	if (strComment.GetLength ()) {
		std::vector <std::wstring> vArg = explode (std::wstring ((LPCTSTR)strComment), ';');

		for (int i = 0; i < vArg.size (); i++) {
			std::vector <std::wstring> v = explode (vArg [i], '=');

			if (v.size () >= 2) 
				result [v [0]] = v [1];
		}
	}

	return result;
}

bool CDatabaseElement::SetSQL (bool bUseSQL, const CString & strSQL) 
{
	m_strSQL = (m_bSQL = bUseSQL) ? strSQL : _T ("");

	if (m_bSQL) {
		std::map <std::wstring, std::wstring> args = GetGeneralArgs (strSQL);

		for (std::map <std::wstring, std::wstring>::iterator i = args.begin (); i != args.end (); i++) {
			if (!_tcsicmp (i->first.c_str (), m_lpszDSN)) 
				SetDSN (i->second.c_str ());
			else if (!_tcsicmp (i->first.c_str (), m_lpszTable)) 
				SetTable (i->second.c_str ());
			else if (!_tcsicmp (i->first.c_str (), m_lpszField)) 
				SetField (i->second.c_str ());
			else if (!_tcsicmp (i->first.c_str (), m_lpszKeyField)) 
				SetKeyField (i->second.c_str ());
			else if (!_tcsicmp (i->first.c_str (), m_lpszKeyValue)) 
				SetKeyValue (i->second.c_str ());
			else if (!_tcsicmp (i->first.c_str (), m_lpszPromptAtStart)) 
				SetPromptAtTaskStart (_ttoi (i->second.c_str ()) ? true : false);
			else if (!_tcsicmp (i->first.c_str (), m_lpszDefault)) 
				SetDefaultData (i->second.c_str ());
			else {
				m_mapGeneral [i->first] = i->second;
			}
		}
	}

	return true;
}

CString CDatabaseElement::GetSQL () const
{
	if (m_bSQL)
		return m_strSQL;
	else
		return _T ("");
}

const FoxjetCommon::ElementFields::FIELDSTRUCT * CDatabaseElement::GetFieldBuffer () const
{
	return ::fields;
}

bool CDatabaseElement::GetPromptAtTaskStart () const
{
	return m_bPrompt;
}

bool CDatabaseElement::SetPromptAtTaskStart (bool bPrompt)
{
	m_bPrompt = bPrompt;
	return true;
}

CString CDatabaseElement::GetPrompt () const
{
	CString str;
	
	str.Format (_T ("[%s].[%s].[%s]"),
		m_strDSN,
		m_strTable,
		m_strKeyField);

	return str;
}

CString CDatabaseElement::GetSQLType (DWORD dwType)
{
	switch (dwType) {
	case SQL_BIT:
		case SQL_TINYINT:
		case SQL_SMALLINT:
		case SQL_INTEGER:
		case SQL_REAL:
		case SQL_FLOAT:
		case SQL_DOUBLE:
			return LoadString (IDS_NUMERIC);
		case SQL_DATE:
		case SQL_TIME:
		case SQL_TIMESTAMP:
			return LoadString (IDS_DATETIME);
		default:
		//case SQL_CHAR:
		//case SQL_VARCHAR:
		//case SQL_LONGVARCHAR:
		//	return LoadString (IDS_STRING);
			if (IsSqlString (dwType))
				return LoadString (IDS_STRING);
			break;
	}

	return ::GetSQLType (dwType);
}

int CDatabaseElement::GetSQLType () const
{
	return m_nSQLType;
}

bool CDatabaseElement::SetSQLType (int nType) 
{
	m_nSQLType = nType;
	return true;
}

int CDatabaseElement::LookupSQLType ()
{
	//LOCK (::csDatabase);

	int nResult = SQL_INTEGER;
	CString strKeyField = GetKeyField ().GetLength () ? (_T ("[") + GetKeyField () + _T ("]")) : _T ("*");
	CString strSQL = _T ("SELECT " + strKeyField + " FROM [") + GetTable () + _T ("]");
	CString strKey = GetDSN () + _T (":") + strSQL;

	if (!::vSQLType.Lookup (strKey, (void *&)nResult)) {
		//TRACEF ("LookupSQLType: " + strKey);

		try
		{
			COdbcCache db = COdbcDatabase::Find (GetDSN ());
			COdbcCache rst = COdbcDatabase::Find (db.GetDB (), GetDSN (), strSQL);
			CODBCFieldInfo info;

			rst.GetRst ().GetODBCFieldInfo ((short)0, info);
			nResult = info.m_nSQLType;
			::vSQLType.SetAt (strKey, (void *&)nResult);
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }
	}

	return nResult;
}

bool CDatabaseElement::GetSerial () const
{
	return m_bSerial;
}

bool CDatabaseElement::SetSerial (bool bSerial)
{
	m_bSerial = bSerial;
	return true;
}

bool CDatabaseElement::GetSerialDownload () const
{
	return m_bSerialDownload;
}

bool CDatabaseElement::SetSerialDownload (bool bValue)
{
	m_bSerialDownload = bValue;
	return true;
}

int CDatabaseElement::GetFormatType () const
{
	return m_nFormat;
}

bool CDatabaseElement::SetFormatType (int nType)
{
	switch (nType) {
	case CDbFormatDlg::DEFAULT:
	case CDbFormatDlg::INT:
	case CDbFormatDlg::FLOAT:
	case CDbFormatDlg::DATE_TIME:
		m_nFormat = nType;
		return true;
	}

	return false;
}

CString CDatabaseElement::GetFormat () const
{
	return m_strFormat;
}

bool CDatabaseElement::SetFormat (const CString & str)
{
	m_strFormat = str;
	return true;
}

CString CDatabaseElement::GetEmptyString () const
{
	return m_strEmptyString;
}

bool CDatabaseElement::SetEmptyString (const CString & str)
{
	m_strEmptyString = str;
	return true;
}

bool CDatabaseElement::SetMaxWidthEnabled (bool bEnabled)
{
	m_bMaxWidth = bEnabled;
	SetRedraw ();
	return true;
}

bool CDatabaseElement::IsMaxWidthEnabled () const
{
	return m_bMaxWidth;
}

bool CDatabaseElement::SetMaxWidth (ULONG lValue)
{
	if (IsValid (lValue, m_lmtParagraphWidth)) {
		m_lMaxWidth = lValue;
		m_nFontMax = GetFontSize ();
		SetRedraw ();
		return true;
	}

	return false;
}

ULONG CDatabaseElement::GetMaxWidth () const
{
	return m_lMaxWidth;
}

void CDatabaseElement::Set (const CExpDateElement & e)
{
	SetDSN			(e.GetDSN ());
	SetTable		(e.GetTable ());
	SetField		(e.GetField ());
	SetKeyField		(e.GetKeyField ());
	SetKeyValue		(e.GetKeyValue ());
	SetSQLType		(e.GetSQLType ());
	SetFormatType	(e.GetFormatType ());
	SetFormatType	(e.GetFormatType ());
	SetFormat		(e.GetFormat ());
}