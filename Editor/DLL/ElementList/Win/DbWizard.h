#pragma once

#include "afxpropertysheet.h"
#include "afxpropertypage.h"
#include "ElementApi.h"
#include "OdbcDatabase.h"

namespace FoxjetElements
{
	class ELEMENT_API CDbWizardDsnDlg;
	class ELEMENT_API CDbWizardFieldDlg;
	class ELEMENT_API CDbWizardTypesDlg;
	class ELEMENT_API CDbWizardKeyFieldDlg;
	class ELEMENT_API CDbWizardDbStartDlg;
	class ELEMENT_API CDbWizardFinishDlg;

	class ELEMENT_API CBaseWizardSheet : public CMFCPropertySheet
	{
		DECLARE_DYNAMIC(CBaseWizardSheet)

	public:
		CBaseWizardSheet(CWnd * pParent, UINT nID);
		virtual ~CBaseWizardSheet();

	protected:
		virtual BOOL OnInitDialog();
		void OnNext ();
		void OnBack ();
		virtual void OnOK();
		virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

		CButton m_btnBack;
		HINSTANCE m_hRestore;

		DECLARE_MESSAGE_MAP()
	};

	class ELEMENT_API CDbWizardDlg : public CBaseWizardSheet
	{
		DECLARE_DYNAMIC(CDbWizardDlg)

	public:
		CDbWizardDlg(CWnd * pParent);
		virtual ~CDbWizardDlg();

		CDbWizardDsnDlg			* m_pdlgDSN;
		CDbWizardFieldDlg		* m_pdlgField;
		CDbWizardKeyFieldDlg	* m_pdlgKeyField;
		CDbWizardDbStartDlg		* m_pdlgDbStart;
		CDbWizardFinishDlg		* m_pdlgFinish;
		FoxjetDatabase::COdbcCache * m_pdb;
	};

	class ELEMENT_API CWizardBasePage : public CMFCPropertyPage
	{
		DECLARE_DYNAMIC(CWizardBasePage)

	public:
		CWizardBasePage (CBaseWizardSheet * pParent, UINT nID);
		virtual ~CWizardBasePage ();

		virtual BOOL OnInitDialog();
		//virtual BOOL OnSetActive() = 0;
		virtual bool CanAdvance () { return true; }

		CBaseWizardSheet * m_pdlgParent;
	};

	class ELEMENT_API CDbWizardBasePage : public CWizardBasePage
	{
		DECLARE_DYNAMIC(CDbWizardBasePage)

	public:
		CDbWizardBasePage (CDbWizardDlg * pParent, UINT nID);
		virtual ~CDbWizardBasePage ();

		virtual BOOL OnSetActive() = 0;

	protected:

		CDbWizardDlg * m_pdlgMain;
	};

};


