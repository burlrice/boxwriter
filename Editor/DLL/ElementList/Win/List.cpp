// List.cpp: implementation of the CElementList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "List.h"
#include "CopyArray.h"
#include "resource.h"
#include "Debug.h"
#include "Edit.h"
#include "Coord.h"
#include "Database.h"
#include "OdbcDatabase.h"
#include "Registry.h"
#include "Alias.h"
#include "FileExt.h"
#include "Extern.h"
#include "ListImp.h"
#include "ItiLibrary.h"
#include "TextElement.h"
#include "Compare.h"
#include "Utils.h"
#include "Parse.h"
#include "IpElements.h"
#include "fj_system.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;

extern CArray <LPFJSYSTEM, LPFJSYSTEM> vIpSystems;

namespace FoxjetIpElements
{
	namespace ElementMenu
	{
		//using namespace FoxjetIpElements;

	#define DECLARE_CREATE(t) \
		FoxjetCommon::CBaseElement * CALLBACK Create##t (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head) \
		{ \
			return new ( ##t ) (pHead, head); \
		} \
		FoxjetCommon::CBaseElement * CALLBACK Copy##t (const FoxjetCommon::CBaseElement * pSrc) \
		{ \
			return new ( ##t ) (* ( ##t *)pSrc); \
		}

	/* #define CREATEMAPENTRY(nType, lpszType, nMenuStrID, nMenuStatusStrID, classname, pfctEdit) \ */
	#define CREATEMAPENTRY(nType, lpszType, classname) \
		{ nType, lpszType, /* nMenuStrID, nMenuStatusStrID, */ Create##classname, /* Copy##classname, pfctEdit, */ }

		DECLARE_CREATE (CTextElement)
		DECLARE_CREATE (CBitmapElement)
		DECLARE_CREATE (CCountElement)
		DECLARE_CREATE (CDateTimeElement)
		DECLARE_CREATE (CBarcodeElement)
		DECLARE_CREATE (CDynamicTextElement)
		DECLARE_CREATE (CDynamicBarcodeElement)
		DECLARE_CREATE (CDataMatrixElement)

		typedef FoxjetCommon::CBaseElement * (CALLBACK * LPCREATEELEMENT) (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		typedef FoxjetCommon::CBaseElement * (CALLBACK * LPCOPYELEMENT) (const FoxjetCommon::CBaseElement * pSrc);
		typedef bool (CALLBACK * LPEDITELEMENT) (FoxjetCommon::CBaseElement & element, 
			const FoxjetCommon::CElementList * pList, CWnd * pParent, const CSize & pa, UNITS units, 
			bool bReadOnlyElement, bool bReadOnlyLocation,
			FoxjetCommon::CElementListArray * pAllLists,
			FoxjetCommon::CElementArray * pUpdate);

		struct
		{
			IPELEMENTTYPE		m_type;
			LPCTSTR				m_lpszType;
			//UINT				m_nMenuStrID;
			//UINT				m_nMenuStatusStrID;
			LPCREATEELEMENT		m_lpCreate;
			//LPCOPYELEMENT		m_lpCopy;
			//LPEDITELEMENT		m_lpEdit;
		} static const map [] = 
		{
			CREATEMAPENTRY (TEXT,				_T ("Text"),		/* IDS_TEXT,		IDS_MENUTEXT_,		 */ CTextElement			/* , OnEditTextElement				*/ ),
			CREATEMAPENTRY (BMP,				_T ("Bitmap"),		/* IDS_BITMAP,		IDS_MENUBITMAP_,	 */ CBitmapElement			/* , OnEditBitmapElement			*/ ),
			CREATEMAPENTRY (COUNT,				_T ("Counter"),		/* IDS_COUNT,		IDS_MENUCOUNT_,		 */ CCountElement			/* , OnEditCountElement				*/ ),
			CREATEMAPENTRY (DATETIME,			_T ("DateTime"),	/* IDS_DATETIME,	IDS_MENUDATETIME_,	 */ CDateTimeElement		/* , OnEditDateTimeElement			*/ ),
			CREATEMAPENTRY (BARCODE,			_T ("Barcode"),		/* IDS_BARCODE,		IDS_MENUBARCODE_,	 */ CBarcodeElement			/* , OnEditBarcodeElement			*/ ),
			CREATEMAPENTRY (DATAMATRIX,			_T ("DataMatrix"),	/* IDS_DATAMATRIX,	IDS_MENUDATAMATRIX_, */ CDataMatrixElement		/* , OnEditDataMatrixElement		*/ ),
			CREATEMAPENTRY (DYNAMIC_TEXT,		_T ("Dyntext"),		/* IDS_DYNTEXT,		IDS_MENUDYNTEXT_,	 */ CDynamicTextElement		/* , OnEditDynamicTextElement		*/ ),
			CREATEMAPENTRY (DYNAMIC_BARCODE,	_T ("Dynbarcode"),	/* IDS_DYNBARCODE,	IDS_MENUDYNBARCODE_, */ CDynamicBarcodeElement	/* , OnEditDynamicBarcodeElement	*/ ),
		};
		static const int nSize = (sizeof (map) / sizeof (map [0]));

		int GetIndex (int nType) {
			for (int i = 0; i < nSize; i++) 
				if ((int)map [i].m_type == nType)
					return i;

			ASSERT (0);
			return -1;
		}
	}; //namespace ElementMenu

	ELEMENT_API int GetElementType (const CString & strPrefix)
	{
		for (int i = 0; i < ElementMenu::nSize; i++) 
			if (!strPrefix.CompareNoCase (ElementMenu::map [i].m_lpszType)) 
				return ElementMenu::map [i].m_type;

		return -1;
	}
};

ELEMENT_API bool FoxjetCommon::operator == (const CElementList & lhs, const CElementList& rhs)
{
	const CString strLhs = lhs.ToString (lhs.m_version);
	const CString strRhs = rhs.ToString (rhs.m_version);

	TRACEF ("Comparing: \n\t[" + strLhs + "]\n\t[" + strRhs + "]");
	return strLhs == strRhs;
}

ELEMENT_API bool FoxjetCommon::operator != (const CElementList & lhs, const CElementList& rhs)
{
	return !(lhs == rhs);
}

CElementList::CElementList(bool bAutoDelete)
:	m_pa (10000, 4000), 
	m_bAutoDelete (bAutoDelete), 
	m_units (INCHES), 
	m_head (GetDefaultHead ()),
	m_bSnap (true),
	m_bNEXT (false),
	CPtrArray ()
{
	fj_GetDllVersion (m_version);
}

CElementList::CElementList (const CElementList & rhs)
:	m_pa (rhs.m_pa),
	m_bAutoDelete (rhs.m_bAutoDelete),
	m_units (rhs.m_units),
	m_head (rhs.m_head),
	m_bSnap (rhs.m_bSnap),
	m_bNEXT (rhs.m_bNEXT),
	m_strLocalParams (rhs.m_strLocalParams),
	m_version (rhs.m_version)
{
	for (int i = 0; i < rhs.GetSize (); i++) {
		const CBaseElement & e = rhs.GetObject (i);
		CBaseElement * p = CopyElement (&e, &GetHead (), false);
		ASSERT (p);
		Add (p);
	}
}

CElementList & CElementList::operator = (const CElementList & rhs)
{
	if (this != &rhs) {
		m_pa				= rhs.m_pa;
		m_bAutoDelete		= rhs.m_bAutoDelete;
		m_units				= rhs.m_units;
		m_head				= rhs.m_head;
		m_version			= rhs.m_version;
		m_bSnap				= rhs.m_bSnap;
		m_bNEXT				= rhs.m_bNEXT;
		m_strLocalParams	= rhs.m_strLocalParams;

		DeleteAllElements ();

		for (int i = 0; i < rhs.GetSize (); i++) {
			const CBaseElement & e = rhs.GetObject (i);
			CBaseElement * p = CopyElement (&e, &GetHead (), false);
			ASSERT (p);
			Add (p);
		}
	}

	return * this;
}

CElementList::~CElementList()
{
	if (m_bAutoDelete) 
		DeleteAllElements ();
}

CBaseElement & CElementList::operator [] (int nIndex)
{
	return GetObject (nIndex);
}

const CBaseElement & CElementList::operator [] (int nIndex) const
{
	return GetObject (nIndex);
}

CBaseElement & CElementList::GetObject (int nIndex)
{
	#ifdef _DEBUG
	ASSERT (nIndex >= 0 && nIndex < GetSize ());
	ASSERT (GetAt (nIndex) != NULL);
	#endif //_DEBUG

	return * (CBaseElement *)GetAt (nIndex);
}

const CBaseElement & CElementList::GetObject (int nIndex) const
{
	#ifdef _DEBUG
	ASSERT (nIndex >= 0 && nIndex < GetSize ());
	ASSERT (GetAt (nIndex) != NULL);
	#endif //_DEBUG

	return * (CBaseElement *)GetAt (nIndex);
}

void CElementList::DeleteAllElements()
{
	for (int i = 0; i < GetSize (); i++) {
		CBaseElement * p = &GetObject (i);
		delete p;
	}

	RemoveAll ();
}

CSize CElementList::GetProductArea () const
{
	return m_pa;
}

bool CElementList::SetProductArea (const CSize & pa, const CSize & sizePanel)
{
	if ((pa.cx >= MIN_PRODLEN && pa.cx <= MAX_PRODLEN) &&
		(pa.cy >= MIN_PRODHEIGHT && pa.cy <= MAX_PRODHEIGHT))
	{
		m_pa = pa;
		return true;
	}

	return false;
}

// returns the number of tokens successfully parsed
int CElementList::FromString(const CString & str, FoxjetCommon::CVersion & ver, bool bDeleteExisiting, ULONG lLocalParamUID)
{
	int nCount = 0;
	CMap <UINT, UINT, UINT, UINT> id;
	CStringArray v;
	m_bNEXT = str.Find (_T ("{Label,")) != -1 ? true : false;

	ParsePackets (str, v);

	if (bDeleteExisiting)
		DeleteAllElements ();

	fj_GetDllVersion ((LPVERSIONSTRUCT)m_version); 
	ver = m_version;

	TRACEF (str);
	// {Ver,4,10,923,0,11}{Timestamp,05/18/16 17:47:13}{ProdArea,15000,4000,0,21}{Datetime,28,0,0,0,0,0,Arial,128,1,0,%%Y%%0M%%D%%S ,0,0,0,0}
	// {Ver,4,10,923,0,8}{Timestamp,05/16/16 20:19:14}{Label,"Default",5.0,15.0,0,3,F,"Default"}{ProdArea,15000,4000,0,21}{Text,"1",0.000000,128,D,2,1,0,F,F,F,"ArialReg128","Marksman NET by FoxJet",L}

	for (int i = 0; i < v.GetSize (); i++) {
		CStringArray vPacket;
		//CString strSeg = Convert (v [i], m_version);
		CString & strSeg = v [i];

		#if defined( __SHOW_PARSE__ ) && defined( _DEBUG )
		TRACEF ("segment: \"" + strSeg + "\"");
		#endif

		Tokenize (strSeg, vPacket);

		if (vPacket.GetSize ()) { 
			CString & strToken = vPacket [0];
			int nType = -1;

			if (m_bNEXT) {
				using namespace FoxjetIpElements;
				using namespace ElementMenu;

				nType = FoxjetIpElements::GetElementType (strToken);

				if (nType == -1) {
					if (!strToken.CompareNoCase (_T ("Label"))) {
						if (vPacket.GetSize () > 3) {
							int cx = (int)(FoxjetDatabase::_ttof (vPacket [3]) * 1000);
							int cy = (int)(FoxjetDatabase::_ttof (vPacket [2]) * 1000);

							SetProductArea (CSize (cx, cy));
						}
					}
				}
				else //if (nType != -1) 
				{
					int nIndex = FoxjetIpElements::ElementMenu::GetIndex (nType);

					if (nIndex != -1) {
						if (LPCREATEELEMENT pfct = FoxjetIpElements::ElementMenu::map [nIndex].m_lpCreate) {
							LPFJPRINTHEAD pHead = (::vIpSystems.GetSize ()) ? ::vIpSystems [0]->gmMembers [0].pfph : NULL;

							CBaseElement * p = (* pfct) (pHead, GetHead ());

							if (!p->FromString (strSeg, m_version)) {
								TRACEF ("FromString failed: " + strSeg);
								delete p;
							}
							else {
								UINT nOldID = p->GetID ();
								UINT nNewID = ForceUniqueID (* this, * p);

								if (nOldID != nNewID)
									id.SetAt (nOldID, nNewID);

								ASSERT (p);
								Add (p);
								nCount++;
							}
						}
					}
				}

				continue;
			}
			else 
				nType = GetElementType (strToken);

			if (nType != -1) {
				// note that embedded elements must have
				// a different prefix than a normal
				// element for this to work
				const CBaseElement & def = ListGlobals::defElements.GetElement (nType);
				CBaseElement * p = CopyElement (&def, &GetHead (), false);
				
				p->SetSnap (GetSnap ());

				if (!p->FromString (strSeg, m_version)) {
					TRACEF ("FromString failed: " + strSeg);
					delete p;
				}
				else {
					UINT nOldID = p->GetID ();
					UINT nNewID = ForceUniqueID (* this, * p);

					if (nOldID != nNewID)
						id.SetAt (nOldID, nNewID);

					ASSERT (p);
					Add (p);
					nCount++;
				}
			}
			else if (ParsePacket (* this, strSeg, ver, lLocalParamUID))
				nCount++;
			else {
				TRACEF ("Could not parse: " + strSeg);
			}
		}
	}

/*
	if (!nCount) {
		CBitmappedTextElement * pText = (CBitmappedTextElement *)CreateElement (TEXT);

		if (pText) {
			pText->SetDefaultData (str);
			Add (pText);
			nCount++;
		}
		else
			delete pText;
	}
*/

	return nCount;
}

CString CElementList::ToString(const FoxjetCommon::CVersion &ver, bool bBreakTokens, const CString & strName) const
{
	CString str;
	CStringArray v;

	v.Add (_T ("Timestamp"));
	v.Add (CTime::GetCurrentTime ().Format (_T ("%c")));

	str.Format (_T ("%s{ProdArea,%u,%u,%u,%d}"), 
		(bBreakTokens ? _T ("\n") : _T ("")), 
		GetProductArea ().cx, GetProductArea ().cy,
		(UINT)GetUnits (), GetHead ().m_lID);

	str = ver.ToString () + FoxjetDatabase::ToString (v) + str + m_strLocalParams;

	if (bBreakTokens)
		str += (TCHAR)'\n';

	for (int i = 0; i < GetSize (); i++) {
		const CBaseElement & e = GetObject (i);

//		str += e.ToString (ver);
		str += Convert (e.ToString (m_version), m_version, ver);

		if (bBreakTokens)
			str += (TCHAR)'\n';
	}

	return str;
//	return _T ("{") + str + _T ("}");
}

const CString CElementList::GetUserDefined () const
{
	return _T ("");
}

bool CElementList::SetUserDefined (const CString & str)
{
	CStringArray v;
	bool bResult = false;

	ParsePackets (str, v);

	for (int i = 0; i < v.GetSize (); i++) {
		CString strKey = GetKey (v [i]);

		if (!strKey.CompareNoCase (_T ("Timestamp"))) 
			bResult |= ParseTimestamp (* this, str);
	}

	return bResult;
}

UNITS CElementList::GetUnits () const
{
	return m_units;
}

bool CElementList::SetUnits (UNITS units)
{
	m_units = units;
	return true;
}

bool CElementList::GetFonts (CArray <CPrinterFont, CPrinterFont &> & v) const
{
	return m_head.GetFonts (v);
}

bool CElementList::SetHead (const FoxjetDatabase::HEADSTRUCT & head)
{
	m_head = CHead (head);
	//CString str; str.Format ("%s (%d)", head.m_strName, head.m_vValve.GetSize ()); TRACEF (str);

	return true;
}

int CElementList::GetNextID() const
{
	UINT nID = 0;

	for (int i = 0; i < GetSize (); i++) 
		nID = max (GetObject (i).GetID (), nID);

	return nID + 1;
}


bool CElementList::Load (const CString & strFile, CVersion & ver, FoxjetDatabase::MESSAGESTRUCT *pMsg)
{
	if (pMsg != NULL) {
		FromString (pMsg->m_strData, ver);
		ver = m_version;
	}
	else 
		return Load (strFile, ver);

	return true;
}

bool CElementList::Load (const CString & strFile, CVersion & ver)
{
	using namespace FoxjetFile;

	DOCTYPE type = GetFileType (strFile);

	if (type == MESSAGE || type == UNKNOWNDOCTYPE) {
		CString strMessage = FoxjetFile::GetFileTitle (strFile);
		CString strHead = FoxjetFile::GetFileHead (strFile);
		FoxjetDatabase::COdbcDatabase & database = ListGlobals::GetDB ();
		ULONG lHeadID = GetHeadID (strHead);
		MESSAGESTRUCT msg;

		TRACEF (CString ("\nLoad : ") +
			"\n\tstrFile    = " + strFile +
			"\n\tstrMessage = " + strMessage +
			"\n\tstrHead    = " + strHead);

		ASSERT ("strMessage should be the head's uid" == 0);

		if (GetMessageRecord (database, strMessage, lHeadID, msg)) {
			FromString (msg.m_strData, ver);
			ver = m_version;
			return true;
		}
	}
	return false;
}

bool CElementList::Store (const CString & strFile, const CVersion & ver) const
{
	using namespace FoxjetFile;

	DOCTYPE type = GetFileType (strFile);

	if (type == MESSAGE || type == UNKNOWNDOCTYPE) {
		CString strMessage = GetFileTitle (strFile);
		CString strHead = GetFileHead (strFile);
		FoxjetDatabase::COdbcDatabase & database = ListGlobals::GetDB ();
		ULONG lHeadID = GetHeadID (strHead);
		MESSAGESTRUCT msg;

		TRACEF (CString ("\nStore : ") +
			"\n\tstrFile    = " + strFile +
			"\n\tstrHead    = " + strHead +
			"\n\tstrMessage = " + strMessage);
		ASSERT (lHeadID != -1);

		ASSERT ("strMessage should be the head's uid" == 0);

		if (GetMessageRecord (database, strMessage, lHeadID, msg)) {
			msg.m_strData = ToString (ver);

			return UpdateMessageRecord (database, msg);
		}
		else { 
			// it doesn't exist in the database, so add it
			msg.m_lHeadID = lHeadID;
			msg.m_strName = strMessage;
			msg.m_strData = ToString (ver);

			return AddMessageRecord (database, msg);
		}
	}

	return false;
}


int CElementList::FindID(UINT nID) const
{
	for (int i = 0; i < GetSize (); i++) 
		if (GetObject (i).GetID () == nID)
			return i;

	return -1;
}

const FoxjetCommon::CVersion & CElementList::GetVersion () const
{
	return m_version;
}

bool CElementList::SetVersion (const FoxjetCommon::CVersion & ver)
{
	m_version = ver;
	return true;
}

int CElementList::GetMaxElements () 
{
	return INT_MAX;
}

bool CElementList::IsMaxedOut () const 
{ 
	return !CanAdd (0);
}

bool CElementList::CanAdd (int nElements) const
{
	int nSize = GetSize () + nElements;
	int nMax = GetMaxElements ();
	bool bResult = (nSize <= nMax);

	return bResult;
}

void CElementList::Reset ()
{
	for (int i = 0; i < GetSize (); i++) {
		CBaseElement * p = &GetObject (i);

		p->Reset ();
	}
}
