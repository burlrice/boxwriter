// SubElementDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SubElementDlg.h"
#include "BuildMaskDlg.h"
#include "SubElement.h"
#include "TextElement.h"
#include "BarcodeElement.h"
#include "Types.h"
#include "ItiLibrary.h"
#include "List.h"
#include "Debug.h"
#include "Extern.h"
#include "Resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace ItiLibrary;
using namespace FoxjetElements;
using namespace FoxjetElements::TextElement;
using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////
// CSubElementDlg dialog

CSubElementDlg::CSubElementDlg(const CSubElement & e, 
							   CWnd* pParent /*=NULL*/)
:	m_bAI (false), 
	m_bReadOnly (false), 
	m_bReadOnlyID (false),
	m_pAllLists (NULL),
	m_lLineID (-1),
	FoxjetCommon::CEliteDlg(IDD_SUBELEMENT, pParent)
{
	//{{AFX_DATA_INIT(CSubElementDlg)
	m_bAI = FALSE;
	m_strData = _T("");
	m_strDesc = _T("");
	m_strID = _T("");
	m_strMask = _T("");
	m_nType = -1;
	//}}AFX_DATA_INIT

	for (int i = 0; i < CSubElement::GetSubtypeCount (); i++) {
		int nClassID = CSubElement::GetSubtypeClassID (i);

		if (e.GetElement ()->GetClassID () == nClassID) {
			// initialize the selected sub-element with 'e'
			CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, CopyElement (e.GetElement (), NULL, false));
			ASSERT (p);
			m_vSubElements.Add (p);
		}
		else {
			// initialize all others from the registry
			const CBaseElement & def = ListGlobals::defElements.GetElement (nClassID);
			CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, CopyElement (&def, NULL, false));
			ASSERT (p);
			m_vSubElements.Add (p);
		}
	}

	m_lLineID = e.GetElement ()->GetLineID ();
	ASSERT (m_lLineID != -1);
}

CSubElementDlg::~CSubElementDlg ()
{
	for (int i = 0; i < m_vSubElements.GetSize (); i++) 
		delete m_vSubElements [i];

	m_vSubElements.RemoveAll ();
}

const CTextElement * CSubElementDlg::GetElement (int nType) const
{
	#ifdef _DEBUG
	CString str;
	str.Format (_T ("GetElement (%d) const"), nType);
	TRACEF (str);
	#endif //_DEBUG

	if (nType == -1 && m_nType != -1)
		return GetElement (m_nType);

	for (int i = 0; i < m_vSubElements.GetSize (); i++) 
		if (CSubElement::GetSubtypeClassID (i) == nType)
			return m_vSubElements [i];

	ASSERT (0);
	return NULL;
}

CTextElement * CSubElementDlg::GetElement (int nType)
{
	#ifdef _DEBUG
	CString str;
	str.Format (_T ("GetElement (%d)"), nType);
	TRACEF (str);
	#endif //_DEBUG

	if (nType == -1 && m_nType != -1)
		return GetElement (m_nType);

	for (int i = 0; i < m_vSubElements.GetSize (); i++) 
		if (CSubElement::GetSubtypeClassID (i) == nType)
			return m_vSubElements [i];

	ASSERT (0);
	return NULL;
}

void CSubElementDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSubElementDlg)
	//}}AFX_DATA_MAP

	DDX_Text(pDX, TXT_DATA, m_strData);
	DDV_MinMaxChars (pDX, m_strData, TXT_DATA, 
		CBarcodeElement::m_lmtString.m_dwMin,
		CBarcodeElement::m_lmtString.m_dwMax);

	DDX_Text(pDX, TXT_DESC, m_strDesc);
	DDX_Text(pDX, TXT_ID, m_strID);

	DDV_SubElementID (pDX, m_strID, m_bAI, TXT_ID);

	if (!pDX->m_bSaveAndValidate) {
		CString str = CSubElement::GetDataMaskDisp (m_strMask);

		#ifdef _DEBUG
		str += " [" + m_strMask + "]";
		#endif //_DEBUG
		
		SetDlgItemText (TXT_MASK, str);
	}

	BeginWaitCursor ();
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_ELEMENTTYPE);
	int nType = TEXT;
	int nIndex = pCB->GetCurSel ();

	if (nIndex != CB_ERR) 
		nType = pCB->GetItemData (nIndex);

	DDV_SubElementData (pDX, m_strData, m_strMask, TXT_DATA, nType);//m_lExpDateSpan);
	EndWaitCursor ();
}


BEGIN_MESSAGE_MAP(CSubElementDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CSubElementDlg)
	ON_CBN_SELCHANGE(CB_ELEMENTTYPE, OnSelchangeElementtype)
	ON_BN_CLICKED(BTN_PROPERTIES, OnProperties)
	ON_BN_CLICKED(BTN_BUILDMASK, OnBuildmask)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSubElementDlg message handlers

void CSubElementDlg::OnSelchangeElementtype() 
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_ELEMENTTYPE);
	CString strLabel = LoadString (IDS_DATA);
	CString strData = m_strData;
	int nIndex = pCB->GetCurSel ();
	int nType = -1;
	bool bEnableBuildFmt = false;
	bool bEnableProps = false;
	bool bEnableData = true;

	if (nIndex != CB_ERR) 
		nType = pCB->GetItemData (nIndex);

	CTextElement * p = GetElement (nType);
	p->SetLineID (m_lLineID);
	p->Build ();
	strData = p->GetImageData ();

	switch (nType) {
	case TEXT:
	case USER: 
		bEnableProps = true;
		break;
	case DATETIME: 
	case EXPDATE:
		strLabel.LoadString (IDS_FORMATSTRING);
		strData = p->GetDefaultData ();
		bEnableBuildFmt = true;
		bEnableProps = true;
		break;
	case COUNT: 
		bEnableProps = true;
		bEnableData = false;
		break;
	case SHIFTCODE:
		bEnableData = false;
		break;
	case SERIAL: 
		bEnableProps = true;
		bEnableData = true;
		break;
	case DATABASE: 
		bEnableProps = true;
		break;
	default:
		bEnableData = false;
		break;
	}

	CEdit * pData = (CEdit *)GetDlgItem (TXT_DATA);
	pData->SetWindowText (strData);
	pData->EnableWindow (bEnableData);

	GetDlgItem (LBL_DATA)->SetWindowText (strLabel);
	GetDlgItem (BTN_PROPERTIES)->EnableWindow (bEnableProps);
}

BOOL CSubElementDlg::OnInitDialog() 
{
	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();

	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_ELEMENTTYPE);

	ASSERT (pCB);

	for (int i = 0; i < CSubElement::GetSubtypeCount (); i++) {
		int nResID = CSubElement::GetSubtypeResID (i);
		int nType = CSubElement::GetSubtypeClassID (i);

		int nIndex = pCB->AddString (LoadString (nResID));
		pCB->SetItemData (nIndex, nType);
	}

	SelectItemData (pCB, (DWORD)m_nType);
	OnSelchangeElementtype ();
	GetDlgItem (LBL_ID)->SetWindowText (LoadString (!m_bAI ? IDS_ID : IDS_AI));
	SetWindowText (LoadString (!m_bAI ? IDS_SUBELEMENT : IDS_APPLICATIONIDENTIFIER));

	GetDlgItem (TXT_ID)->EnableWindow (/* !m_bAI && */ !(m_bReadOnlyID || m_bReadOnly));
	GetDlgItem (TXT_DESC)->EnableWindow (/* !m_bAI && */ !m_bReadOnly);

	if (CWnd * p = GetDlgItem (TXT_DATA)) {
		GotoDlgCtrl (p);

		return FALSE;
	}

	return bResult;
}

void CSubElementDlg::OnOK()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_ELEMENTTYPE);
	int nIndex = pCB->GetCurSel ();

	if (nIndex != CB_ERR) {
		m_nType = pCB->GetItemData (nIndex);
		FoxjetCommon::CEliteDlg::OnOK ();
	}
	else
		MsgBox (* this, LoadString (IDS_NOELEMENTTYPESELECTED));
}

void CSubElementDlg::OnProperties() 
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_ELEMENTTYPE);
	int nIndex = pCB->GetCurSel ();

	if (nIndex != CB_ERR) {
		int nType = pCB->GetItemData (nIndex);
		CTextElement * p = GetElement (nType); 
		CString str;

		GetDlgItemText (TXT_DATA, str);
		p->SetDefaultData (str);
		p->Build ();

		// std // #if __CUSTOM__ == __SW0840__
		p->SetLineID (m_lLineID);
		ASSERT (p->GetLineID () != -1);
		// std // #endif

		if (OnEditElement (* p, this, CSize (0, 0), INCHES, NULL, false, true, m_pAllLists)) {
			p->Build ();

			if (p->GetClassID () == DATETIME || p->GetClassID () == EXPDATE)
				str = p->GetDefaultData ();
			else
				str = p->GetImageData ();

			SetDlgItemText (TXT_DATA, str);
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOELEMENTTYPESELECTED));
}

void AFXAPI CSubElementDlg::DDV_SubElementID(CDataExchange *pDX, 
											 const CString & str,
											 bool bAI, UINT nCtrlID)
{
	if (pDX->m_bSaveAndValidate) {
		bool bFail = false;
		CString strMsg;

		if (bAI) {
			if (!CSubElement::IsAi (str)) {
				strMsg = LoadString (IDS_INVALIDAI);
				bFail = true;
			}
		}
		else {
			if (CSubElement::IsAi (str) || str.GetLength () == 0) {
				strMsg = LoadString (IDS_INVALIDSUBELEMENTID);
				bFail = true;
			}
		}

		if (bFail) {
			MsgBox (* this, strMsg);
			pDX->PrepareEditCtrl (nCtrlID);
			pDX->Fail ();
		}
	}
}

void AFXAPI CSubElementDlg::DDV_SubElementData(CDataExchange *pDX, 
											   const CString &strData, 
											   const CString &strMask,
											   UINT nCtrlID,
											   int nType)
{
	#ifdef _DEBUG
	CString str;
	str.Format (
		_T ("\n\tstrData = \"%s\"")
		_T ("\n\tstrMask = \"%s\""),
		strData,
		strMask);
	TRACEF (str);
	#endif //_DEBUG

	if (pDX->m_bSaveAndValidate) {
		CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, CopyElement (GetElement (nType), NULL, false));
		
		ASSERT (p);
		p->SetDefaultData (strData);
		p->Build ();
		CString strFmtData = p->GetImageData ();
		delete p;

		#ifdef _DEBUG
		CString str;
		str.Format (
			_T ("\n\tstrData = \"%s\"")
			_T ("\n\tstrFmtData = \"%s\""),
			strData, strFmtData);
		TRACEF (str);
		#endif //_DEBUG

		CString strMsg;
		bool bFail = false;
		int nIndex = -1;

		// this can't work with the ANY char type
		/*
		// check for invalid chars BEFORE IsValidData is called
		for (int i = 0; i < strFmtData.GetLength (); i++) {
			if (!CSubElement::IsAlphaNumeric (strFmtData [i])) {
				strMsg.Format (LoadString (IDS_NONALPHANUMERICCHARS), 
					strFmtData [i]);
				bFail = true;
				nIndex = i;
				break;
			}
		}
		*/

		ULONG lCalls;

		if (!bFail && !CSubElement::IsValidData (strFmtData, strMask, lCalls)) {
			strMsg = LoadString (IDS_INVALIDSUBELEMENTDATA);
			bFail = true;
		}

		if (bFail) {
			MsgBox (* this, strMsg);
			pDX->PrepareEditCtrl (nCtrlID);
			pDX->Fail ();

			if (nIndex != -1) {
				CEdit * pCtrl = (CEdit *)GetDlgItem (nCtrlID);
				ASSERT (pCtrl);
				pCtrl->SetSel (nIndex, nIndex + 1);
			}
		}
		else { // !bFail
			GetElement (nType)->SetDefaultData (strData);
		}
	}
}

void CSubElementDlg::OnBuildmask() 
{
	CBuildMaskDlg dlg (this);
	
	dlg.m_bEditable = !m_bAI && !m_bReadOnly;
	dlg.m_strMask = m_strMask;
	
	if (dlg.DoModal () == IDOK) {
		m_strMask = dlg.m_strMask;
		SetDlgItemText (TXT_MASK, CSubElement::GetDataMaskDisp (m_strMask));
	}
}
