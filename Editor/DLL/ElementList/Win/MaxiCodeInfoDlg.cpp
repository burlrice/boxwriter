// MaxiCodeInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MaxiCodeInfoDlg.h"
#include "CountryCodesDlg.h"
#include "MaxiCodeInfo.h"
#include "ClassOfServiceDlg.h"
#include "ItiLibrary.h"
#include "Extern.h"
#include "MsgBox.h"

using namespace FoxjetCommon;
using namespace ItiLibrary;
using namespace FoxjetElements;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMaxiCodeInfoDlg dialog


CMaxiCodeInfoDlg::CMaxiCodeInfoDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(CMaxiCodeInfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMaxiCodeInfoDlg)
	m_strClassOfSerice = _T("");
	m_strCountryCode = _T("");
	m_strPostalCode = _T("");
	m_nLength = 0;
	//}}AFX_DATA_INIT
	m_nMode = 2;
}


void CMaxiCodeInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	
	//{{AFX_DATA_MAP(CMaxiCodeInfoDlg)
	DDX_Text(pDX, TXT_CLASSOFSERVICE, m_strClassOfSerice);
	DDX_Text(pDX, TXT_COUNTRYCODE, m_strCountryCode);
	DDX_Text(pDX, TXT_POSTALCODE, m_strPostalCode);
	DDX_Text(pDX, TXT_LENGTH, m_nLength);
	//}}AFX_DATA_MAP

	DDXV_Mode (pDX, m_nMode);
	DDV_ClassOfService (pDX, m_strClassOfSerice);
	DDV_CountryCode (pDX, m_strCountryCode);
	DDV_PostalCode (pDX, m_strPostalCode, m_nMode);
	DDV_Length (pDX, m_nLength);
}


BEGIN_MESSAGE_MAP(CMaxiCodeInfoDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CMaxiCodeInfoDlg)
	ON_BN_CLICKED(BTN_CLASSOFSERVICE, OnClassofservice)
	ON_BN_CLICKED(BTN_COUNTRYCODE, OnCountrycode)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMaxiCodeInfoDlg message handlers

void CMaxiCodeInfoDlg::OnClassofservice() 
{
	CClassOfServiceDlg dlg (&(ListGlobals::GetDB ()), this);
	CString str;

	GetDlgItemText (TXT_CLASSOFSERVICE, str);
	dlg.m_vSelected.Add (str);

	if (dlg.DoModal () == IDOK) {
		if (dlg.m_vSelected.GetSize () == 1)
			SetDlgItemText (TXT_CLASSOFSERVICE, dlg.m_vSelected [0]);
		else
			ASSERT (0);
	}
}

void CMaxiCodeInfoDlg::OnCountrycode() 
{
	CCountryCodesDlg dlg (&(ListGlobals::GetDB ()), this);
	CString str;

	GetDlgItemText (TXT_COUNTRYCODE, str);
	dlg.m_vSelected.Add (str);

	if (dlg.DoModal () == IDOK) {
		if (dlg.m_vSelected.GetSize () == 1)
			SetDlgItemText (TXT_COUNTRYCODE, dlg.m_vSelected [0]);
		else
			ASSERT (0);
	}
}

BOOL CMaxiCodeInfoDlg::OnInitDialog() 
{
	CComboBox * pMode = (CComboBox *)GetDlgItem (CB_MODE);
	ASSERT (pMode);

	for (int i = 2; i <= 6; i++) {
		CString str;
		str.Format (_T ("%d"), i);
		int nIndex = pMode->AddString (str);
		pMode->SetItemData (nIndex, i);
	}
	
	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();

	return bResult;
}

void CMaxiCodeInfoDlg::DDV_ClassOfService (CDataExchange* pDX, const CString & str)
{
	if (pDX->m_bSaveAndValidate) {
		if (!CMaxiCodeInfo::IsValidClassOfService (str)) {
			MsgBox (* this, LoadString (IDS_INVALIDCLASSOFSERVICE));
			pDX->PrepareEditCtrl (TXT_CLASSOFSERVICE);
			pDX->Fail ();
			return;
		}
	}
}

void CMaxiCodeInfoDlg::DDV_CountryCode (CDataExchange* pDX, const CString & str)
{
	if (pDX->m_bSaveAndValidate) {
		if (!CMaxiCodeInfo::IsValidCountryCode (str)) {
			MsgBox (* this, LoadString (IDS_INVALIDCOUNTRYCODE));
			pDX->PrepareEditCtrl (TXT_COUNTRYCODE);
			pDX->Fail ();
			return;
		}
	}
}

void CMaxiCodeInfoDlg::DDV_PostalCode (CDataExchange* pDX, const CString & str, int nMode)
{
	if (pDX->m_bSaveAndValidate) {
	}
		if (!CMaxiCodeInfo::IsValidPostalCode (str, nMode)) {
			MsgBox (* this, LoadString (IDS_INVALIDPOSTALCODE));
			pDX->PrepareEditCtrl (TXT_POSTALCODE);
			pDX->Fail ();
			return;
		}
}

void CMaxiCodeInfoDlg::DDV_Length (CDataExchange* pDX, int n)
{
	if (pDX->m_bSaveAndValidate) {
		if (!CMaxiCodeInfo::IsValidLength (n)) {
			MsgBox (* this, LoadString (IDS_INVALIDLENGTH));
			pDX->PrepareEditCtrl (TXT_LENGTH);
			pDX->Fail ();
			return;
		}
	}
}

void CMaxiCodeInfoDlg::DDXV_Mode (CDataExchange* pDX, int n)
{
	CComboBox * pMode = (CComboBox *)GetDlgItem (CB_MODE);
	ASSERT (pMode); 

	if (pDX->m_bSaveAndValidate) {
		int nIndex = pMode->GetCurSel (); 

		if (nIndex != CB_ERR) {
			int nMode = pMode->GetItemData (nIndex);

			if (!CMaxiCodeInfo::IsValidMode (nMode)) {
				MsgBox (* this, LoadString (IDS_INVALIDMODE));
				pDX->PrepareCtrl (CB_MODE);
				pDX->Fail ();
				return;
			}
			else
				m_nMode = nMode;
		}
		else {
			MsgBox (* this, LoadString (IDS_NOMODESELECTED));
			pDX->PrepareCtrl (CB_MODE);
			pDX->Fail ();
			return;
		}
	}
	else {
		bool bSelectItemData = SelectItemData (pMode, m_nMode);
		ASSERT (bSelectItemData);
	}
}

