// C93BcParamsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "C93BcParamsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CC93BcParamsDlg dialog


CC93BcParamsDlg::CC93BcParamsDlg(FoxjetDatabase::HEADTYPE type, CWnd* pParent /*=NULL*/)
	: CCode128ParamsDlg(IDD_C93PARAMS, type, pParent)
{
	//{{AFX_DATA_INIT(CC93BcParamsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CC93BcParamsDlg::DoDataExchange(CDataExchange* pDX)
{
	CCode128ParamsDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CC93BcParamsDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CC93BcParamsDlg, CCode128ParamsDlg)
	//{{AFX_MSG_MAP(CC93BcParamsDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CC93BcParamsDlg message handlers
