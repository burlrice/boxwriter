#if !defined(AFX_SELECTSHIFTCODEDLG_H__2BEAA88F_098C_44C2_ACB3_81FE9CE90D8E__INCLUDED_)
#define AFX_SELECTSHIFTCODEDLG_H__2BEAA88F_098C_44C2_ACB3_81FE9CE90D8E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectShiftCodeDlg.h : header file
//

#include "CopyArray.h"
#include "ListCtrlImp.h"
#include "resource.h"
#include "Database.h"
#include "OdbcDatabase.h"
#include "Head.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CSelectShiftCodeDlg dialog
/*
this dialog differs from the standard implementation of
a windows dialog in that the changes the user makes are saved
in the m_pDatabase member as they are made, you do NOT save them
when DoModal returns IDOK.  call BEGIN_TRANS before DoModal, 
then call COMMIT_TRANS if it returns IDOK.  Otherwise, call 
ROLLBACK_TRANS to discard the changes.
*/

class CSelectShiftCodeDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CSelectShiftCodeDlg(FoxjetDatabase::COdbcDatabase & database, const FoxjetCommon::CHead & head, CWnd* pParent = NULL);   // standard constructor
	CSelectShiftCodeDlg(FoxjetDatabase::COdbcDatabase & database, const FoxjetCommon::CHead & head, UINT nIDTemplate, CWnd* pParent = NULL);

// Dialog Data
	//{{AFX_DATA(CSelectShiftCodeDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	FoxjetCommon::CLongArray m_sel;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectShiftCodeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK ();

	class CShift :	public FoxjetDatabase::SHIFTSTRUCT,
					public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CShift ();
		CShift (const FoxjetDatabase::SHIFTSTRUCT & rhs);
		CShift (const CShift & rhs);
		CShift & operator = (const CShift & rhs);
		virtual ~CShift ();

		virtual CString GetDispText (int nColumn) const;
	};

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
	FoxjetDatabase::COdbcDatabase * m_pDatabase;
	const FoxjetCommon::CHead & m_head;

	// Generated message map functions
	//{{AFX_MSG(CSelectShiftCodeDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTSHIFTCODEDLG_H__2BEAA88F_098C_44C2_ACB3_81FE9CE90D8E__INCLUDED_)
