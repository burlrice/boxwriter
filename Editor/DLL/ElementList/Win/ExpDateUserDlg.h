#if !defined(AFX_EXPDATEUSERDLG_H__A2683EE6_C7B9_41F6_BB65_8E4ED951D8D2__INCLUDED_)
#define AFX_EXPDATEUSERDLG_H__A2683EE6_C7B9_41F6_BB65_8E4ED951D8D2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExpDateUserDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CExpDateUserDlg dialog

class CExpDateUserDlg : public CDialog
{
// Construction
public:
	CExpDateUserDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CExpDateUserDlg)
	enum { IDD = IDD_EXPDATE_USER };
	CString	m_strPrompt;
	BOOL	m_bPromptAtTaskStart;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExpDateUserDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CExpDateUserDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXPDATEUSERDLG_H__A2683EE6_C7B9_41F6_BB65_8E4ED951D8D2__INCLUDED_)
