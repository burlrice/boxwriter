// ExpDateUserDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "ExpDateUserDlg.h"
#include "Resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExpDateUserDlg dialog


CExpDateUserDlg::CExpDateUserDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CExpDateUserDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CExpDateUserDlg)
	m_strPrompt = _T("");
	m_bPromptAtTaskStart = FALSE;
	//}}AFX_DATA_INIT
}


void CExpDateUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExpDateUserDlg)
	DDX_Text(pDX, TXT_PROMPT, m_strPrompt);
	DDX_Check(pDX, CHK_PROMPT, m_bPromptAtTaskStart);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CExpDateUserDlg, CDialog)
	//{{AFX_MSG_MAP(CExpDateUserDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExpDateUserDlg message handlers
