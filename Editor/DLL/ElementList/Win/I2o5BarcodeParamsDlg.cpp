// I2o5BarcodeParamsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "BarcodeElement.h"
#include "list.h"
#include "I2o5BarcodeParamsDlg.h"
#include "TemplExt.h"
#include "BarcodeParams.h"
#include "BarcodeParamDlg.h"

using namespace FoxjetElements;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

struct
{
	UINT	m_nCtrlID;
	int		m_nAlign;
}
static const map [] =
{
	{ RDO_OFF,				kCapAlignOff,			},
	{ RDO_BELOWLEFT,		kCapAlignBelowLeft,		},
	{ RDO_BELOWCENTER,		kCapAlignBelowCenter,	},
	{ RDO_BELOWRIGHT,		kCapAlignBelowRight,	},
	{ RDO_ABOVELEFT,		kCapAlignAboveLeft,		},
	{ RDO_ABOVECENTER,		kCapAlignAboveCenter,	},
	{ RDO_ABOVERIGHT,		kCapAlignAboveRight,	}, 
};

/////////////////////////////////////////////////////////////////////////////
// CI2o5BarcodeParamsDlg dialog


CI2o5BarcodeParamsDlg::CI2o5BarcodeParamsDlg(FoxjetDatabase::HEADTYPE type, CWnd* pParent /*=NULL*/)
	: CUpcBarcodeParamsDlg (IDD_I2O5BARCODEPARAMS, type, pParent)
{
	//{{AFX_DATA_INIT(CI2o5BarcodeParamsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	VERIFY (m_bmpCaption [0].LoadBitmap (IDB_CAPTIONOFF));
	VERIFY (m_bmpCaption [1].LoadBitmap (IDB_CAPTIONBELOWLEFT));
	VERIFY (m_bmpCaption [2].LoadBitmap (IDB_CAPTIONBELOWCENTER));
	VERIFY (m_bmpCaption [3].LoadBitmap (IDB_CAPTIONBELOWRIGHT));
	VERIFY (m_bmpCaption [4].LoadBitmap (IDB_CAPTIONABOVELEFT));
	VERIFY (m_bmpCaption [5].LoadBitmap (IDB_CAPTIONABOVECENTER));
	VERIFY (m_bmpCaption [6].LoadBitmap (IDB_CAPTIONABOVERIGHT));
}


void CI2o5BarcodeParamsDlg::DoDataExchange(CDataExchange* pDX)
{
	CUpcBarcodeParamsDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CI2o5BarcodeParamsDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		CButton * p = (CButton *)GetDlgItem (map [i].m_nCtrlID);
		
		ASSERT (p);
		
		if (!pDX->m_bSaveAndValidate)
			p->SetCheck (map [i].m_nAlign == m_nAlignment);
		else {
			if (p->GetCheck () == 1) {
				m_nAlignment = map [i].m_nAlign;
				break;
			}
		}	
	}

	DDX_Check (pDX, CHK_CHECKSUM, m_nCheckSum);

	DDX_Text (pDX, TXT_HORZBEARER, m_nHorzBearer);
	DDV_MinMaxInt (pDX, m_nHorzBearer, CBarcodeParams::m_lmtBearer.m_dwMin, CBarcodeParams::m_lmtBearer.m_dwMax);

	DDX_Text (pDX, TXT_VERTBEARER, m_nVertBearer);
	DDV_MinMaxInt (pDX, m_nVertBearer, CBarcodeParams::m_lmtBearer.m_dwMin, CBarcodeParams::m_lmtBearer.m_dwMax);

	DDX_Text (pDX, TXT_QUIETZONE, m_nQuietZone);
	DDV_MinMaxInt (pDX, m_nQuietZone, CBarcodeParams::m_lmtQuietZone.m_dwMin, CBarcodeParams::m_lmtQuietZone.m_dwMax);

	// all other validation happens in OnOK
}


BEGIN_MESSAGE_MAP(CI2o5BarcodeParamsDlg, CUpcBarcodeParamsDlg)
	//{{AFX_MSG_MAP(CI2o5BarcodeParamsDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (RDO_ABOVELEFT, InvalidatePreview)
	ON_BN_CLICKED (RDO_ABOVECENTER, InvalidatePreview)
	ON_BN_CLICKED (RDO_ABOVERIGHT, InvalidatePreview)
	ON_BN_CLICKED (RDO_BELOWLEFT, InvalidatePreview)
	ON_BN_CLICKED (RDO_BELOWCENTER, InvalidatePreview)
	ON_BN_CLICKED (RDO_BELOWRIGHT, InvalidatePreview)
	ON_BN_CLICKED (RDO_OFF, InvalidatePreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CI2o5BarcodeParamsDlg message handlers

void CI2o5BarcodeParamsDlg::InvalidatePreview ()
{
	CStatic * pPreview = (CStatic *)GetDlgItem (IDC_PREVIEW);

	ASSERT (pPreview);

	for (int i = 0; i < (sizeof (map) / sizeof (map [0])); i++) {
		CButton * p = (CButton *)GetDlgItem (map [i].m_nCtrlID);

		ASSERT (p);

		if (p->GetCheck () == 1) {
			pPreview->SetBitmap (m_bmpCaption [i]);
			CBarcodeParamDlg::ResizePreview (this, IDC_PREVIEW, RDO_ABOVERIGHT, RDO_BELOWLEFT);
			break;
		}
	}
}

BOOL CI2o5BarcodeParamsDlg::OnInitDialog() 
{
	CUpcBarcodeParamsDlg::OnInitDialog();
	
	InvalidatePreview ();	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CI2o5BarcodeParamsDlg::GetDefSymbology () const
{
	return kSymbologyInter2of5;
}
