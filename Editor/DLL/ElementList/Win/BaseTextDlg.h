#if !defined(AFX_BASETEXTDLG_H__12999848_EC7E_4748_B9BF_7B4D9E97312B__INCLUDED_)
#define AFX_BASETEXTDLG_H__12999848_EC7E_4748_B9BF_7B4D9E97312B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BaseTextDlg.h : header file
//

#include "TextElement.h"
#include "ElementDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CBaseTextDlg dialog

class CBaseTextDlg : public CElementDlg
{
// Construction
public:
	CBaseTextDlg (const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		UINT nID, const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBaseTextDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBaseTextDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementation
protected:

public:
	FoxjetElements::TextElement::FONTORIENTATION m_nOrientation;
	int	m_nWidth;

protected:

	afx_msg void OnOrientationClick();
	FoxjetElements::TextElement::FONTORIENTATION GetOrientation () const;

	CBitmap m_bmpOrientation [2];

	// Generated message map functions
	//{{AFX_MSG(CBaseTextDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BASETEXTDLG_H__12999848_EC7E_4748_B9BF_7B4D9E97312B__INCLUDED_)
