#include "stdafx.h"
#include "SerialWizardPortsDlg.h"
#include "resource.h"
#include "Utils.h"
#include "WinMsg.h"
#include "Extern.h"
#include "Database.h"
#include "ComPropertiesDlg.h"
#include "Comm32.h"
#include "Debug.h"
#include "Parse.h"
#include "SerialSampleDlg.h"
#include "color.h"
#include <string>
#include <stdexcept>

using namespace FoxjetElements;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

IMPLEMENT_DYNAMIC(CSerialWizardIndexPage, CWizardBasePage)


BEGIN_MESSAGE_MAP(CSerialWizardIndexPage, CWizardBasePage)
	ON_WM_DRAWITEM ()
	ON_BN_CLICKED(BTN_CHANGE, OnChangeSampleData)
	ON_NOTIFY(UDN_DELTAPOS, SPN_INDEX, OnDeltaposIndex)
	ON_NOTIFY(UDN_DELTAPOS, SPN_LENGTH, OnDeltaposLength)
END_MESSAGE_MAP()

CSerialWizardIndexPage::CSerialWizardIndexPage (CSerialWizardSheet * pParent)
:	m_nIndex (0),
	m_nLength (0),
	m_bChangedSampleData (false),
	m_bSetDefaultData (false),
	CWizardBasePage (pParent, IDD_SERIAL_WIZARD_OFFSET)
{
}

CSerialWizardIndexPage::~CSerialWizardIndexPage ()
{
}

BOOL CSerialWizardIndexPage::OnSetActive()
{
	if (!m_bChangedSampleData) {
		if (CSerialWizardSheet * pParent = DYNAMIC_DOWNCAST (CSerialWizardSheet, m_pdlgParent)) {
			CString str = pParent->m_pdlgPort->GetSampleData ();

			if (str.GetLength ()) {
				m_strSampleData = str;

				//m_nIndex = 0;
				//m_nLength = m_strSampleData.GetLength ();

				SetDlgItemInt (TXT_INDEX, m_nIndex);
				SetDlgItemInt (TXT_LENGTH, m_nLength);

				if (CSpinButtonCtrl * p = (CSpinButtonCtrl *)GetDlgItem (SPN_INDEX))
					p->SetPos (m_nIndex);

				if (CSpinButtonCtrl * p = (CSpinButtonCtrl *)GetDlgItem (SPN_LENGTH))
					p->SetPos (m_nLength);
			}
		}
	}

	return CWizardBasePage::OnSetActive ();
}

BOOL CSerialWizardIndexPage::OnKillActive ()
{
	if (CButton * p = (CButton *)GetDlgItem (CHK_DEFAULT)) {
		m_bSetDefaultData = p->GetCheck () == 1 ? true : false;
	}

	return CWizardBasePage::OnKillActive ();
}

BOOL CSerialWizardIndexPage::OnInitDialog()
{
	LINESTRUCT line;

	if (CSerialWizardSheet * pParent = DYNAMIC_DOWNCAST (CSerialWizardSheet, m_pdlgParent)) {
		if (GetLineRecord (ListGlobals::GetDB (), pParent->m_lLineID, line)) {
			m_nIndex = line.m_nBufferOffset;
			m_nLength = line.m_nSignificantChars;
		}
	}

	SetDlgItemInt (TXT_INDEX, m_nIndex);
	SetDlgItemInt (TXT_LENGTH, m_nLength);

	if (CSpinButtonCtrl * p = (CSpinButtonCtrl *)GetDlgItem (SPN_INDEX)) {
		//CRect rcSpin (0, 0, 0, 0);

		//p->GetWindowRect (rcSpin);
		//rcSpin.right = rcSpin.left + rcSpin.Height () / 2;
		//p->SetWindowPos (NULL, 0, 0, rcSpin.Width (), rcSpin.Height (), SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
		p->SetPos (m_nIndex);
	}

	if (CSpinButtonCtrl * p = (CSpinButtonCtrl *)GetDlgItem (SPN_LENGTH)) {
		//CRect rcSpin (0, 0, 0, 0);

		//p->GetWindowRect (rcSpin);
		//rcSpin.right = rcSpin.left + rcSpin.Height () / 2;
		//p->SetWindowPos (NULL, 0, 0, rcSpin.Width (), rcSpin.Height (), SWP_NOMOVE | SWP_NOZORDER | SWP_SHOWWINDOW);
		p->SetPos (m_nLength);
	}

	if (CWnd * p = GetDlgItem (LBL_SAMPLE_DATA))
		p->ModifyStyle (0, SS_OWNERDRAW, 0);

	return CWizardBasePage::OnInitDialog ();
}

bool CSerialWizardIndexPage::CanAdvance () 
{ 
	return true; 
}

void CSerialWizardIndexPage::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	DRAWITEMSTRUCT dis;
	
	memcpy (&dis, lpDrawItemStruct, sizeof (dis));

	if (nIDCtl == LBL_SAMPLE_DATA) {
		typedef struct tagSEGMENT
		{
			tagSEGMENT (const CString & str = _T (""), bool bSelected = true) : m_str (str), m_bSelected (bSelected) { }

			CString m_str;
			bool m_bSelected;
		} SEGMENT;
		CArray <SEGMENT, SEGMENT &> v;
		CRect rc = dis.rcItem;
		CDC dc;
		std::wstring str (m_strSampleData);

		//if (str.length () < (m_nIndex + m_nLength) && (m_nIndex >= 0 && m_nLength >= 0))
		//	str += std::wstring (CString (' ', (m_nIndex + m_nLength) - str.length ()));

		try { v.Add (SEGMENT (str.substr (0, m_nIndex).c_str (), false)); } catch (const std::out_of_range & oor) { v.Add (SEGMENT (_T (""), false)); }
		try { v.Add (SEGMENT (str.substr (m_nIndex, m_nLength).c_str (), true)); } catch (const std::out_of_range & oor) { v.Add (SEGMENT (_T (""), true)); }
		try { v.Add (SEGMENT (str.substr (m_nIndex + m_nLength).c_str (), false)); } catch (const std::out_of_range & oor) { v.Add (SEGMENT (_T (""), false)); }

		dc.Attach (dis.hDC);
		int nTextColor = dc.GetTextColor ();
		int nGetBkColor = dc.GetBkColor ();

		dc.FillSolidRect (rc, ::GetSysColor (COLOR_BTNFACE));

		for (int i = 0; i < v.GetSize (); i++) {
			const int nFormat = DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_EXPANDTABS;
			CRect rcTmp (rc);
			SEGMENT s = v [i];

			s.m_str = FoxjetDatabase::Format (s.m_str, s.m_str.GetLength ());

			dc.SetTextColor (s.m_bSelected ? Color::rgbWhite : Color::rgbDarkGray);
			dc.SetBkColor (s.m_bSelected ? ::GetSysColor (COLOR_HIGHLIGHT) : ::GetSysColor (COLOR_BTNFACE));
			dc.DrawText (s.m_str, rcTmp, nFormat | DT_CALCRECT);
			dc.DrawText (s.m_str, rc, nFormat);
			rc.left += rcTmp.Width ();
		}

		dc.SetTextColor (nTextColor);
		dc.SetBkColor (nGetBkColor);
		dc.Detach ();
	}
}

void CSerialWizardIndexPage::OnChangeSampleData ()
{
	CSerialSampleDlg dlg (this);

	dlg.m_strData = FoxjetDatabase::Format (m_strSampleData, m_strSampleData.GetLength ());

	if (dlg.DoModal () == IDOK) {
		int n = dlg.m_strData.GetLength () + 1;
		TCHAR * psz = new TCHAR [n];

		memset (psz, 0, sizeof (TCHAR) * n);
		m_bChangedSampleData = true;
		FoxjetDatabase::Unformat (dlg.m_strData, psz);
		m_strSampleData = psz;
		delete [] psz;

		if (CSerialWizardSheet * pParent = DYNAMIC_DOWNCAST (CSerialWizardSheet, m_pdlgParent)) 
			pParent->m_pdlgPort->SetSampleData (m_strSampleData);

		//m_nIndex = 0;
		//m_nLength = m_strSampleData.GetLength ();

		SetDlgItemInt (TXT_INDEX, m_nIndex);
		SetDlgItemInt (TXT_LENGTH, m_nLength);

		if (CSpinButtonCtrl * p = (CSpinButtonCtrl *)GetDlgItem (SPN_INDEX))
			p->SetPos (m_nIndex);

		if (CSpinButtonCtrl * p = (CSpinButtonCtrl *)GetDlgItem (SPN_LENGTH))
			p->SetPos (m_nLength);

		REPAINT (GetDlgItem (LBL_SAMPLE_DATA));
	}
}

void CSerialWizardIndexPage::OnDeltaposIndex(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pnmud = (NM_UPDOWN*)pNMHDR;
	
	TRACEF (_T ("iPos: ") + ToString ((int)pnmud->iPos) + _T (", iDelta: ") + ToString ((int)pnmud->iDelta));

	m_nIndex -= pnmud->iDelta;

	if (m_nIndex < 0)
		m_nIndex = 0;

	SetDlgItemInt (TXT_INDEX, m_nIndex);
	REPAINT (GetDlgItem (LBL_SAMPLE_DATA));

	if (pResult)
		* pResult = 0;
}

void CSerialWizardIndexPage::OnDeltaposLength(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pnmud = (NM_UPDOWN*)pNMHDR;
	
	TRACEF (_T ("iPos: ") + ToString ((int)pnmud->iPos) + _T (", iDelta: ") + ToString ((int)pnmud->iDelta));

	m_nLength -= pnmud->iDelta;

	if (m_nLength < 0)
		m_nLength = 0;

	SetDlgItemInt (TXT_LENGTH, m_nLength);
	REPAINT (GetDlgItem (LBL_SAMPLE_DATA));

	if (pResult)
		* pResult = 0;
}

