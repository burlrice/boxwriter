#include "stdafx.h"

#include <math.h>

#include "ShapeDlg.h"
#include "ShapeElement.h"
#include "List.h"
#include "Coord.h"
#include "Debug.h"
#include "Edit.h"
#include "Utils.h"
#include "ximage.h"
#include "AppVer.h"
#include "BitmapElement.h"

using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ShapeElement;

/////////////////////////////////////////////////////////////////////////////
// CShapeDlg dialog

CShapeDlg::CShapeDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
					   const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	CElementDlg(e, pa, IDD_SHAPE, pList, pParent)
{
	//{{AFX_DATA_INIT(CShapeDlg)
	//}}AFX_DATA_INIT

	m_bmpHorz.LoadBitmap (IDB_SHAPE_HORZ);
	m_bmpVert.LoadBitmap (IDB_SHAPE_VERT);
	m_bmpRect.LoadBitmap (IDB_SHAPE_RECT);
}


void CShapeDlg::DoDataExchange(CDataExchange* pDX)
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();
	
	DDX_Radio (pDX, RDO_HORIZONTALLINE, m_nType);

	{	
		CCoord coordMin (0, 0, m_units, head);
		CCoord coordMax (0, 0, m_units, head);
		CCoord coordValidate (0, 0, m_units, head);

		const CSize pa1000s = CBaseElement::LogicalToThousandths (CSize (GetPA ().cx, head.Height ()), head); 
		const CSize sizeMin1000s = CBaseElement::LogicalToThousandths (CBitmapElement::GetMinSize (), head);

		coordMin.m_x = sizeMin1000s.cx;
		coordMin.m_y = sizeMin1000s.cy;

		coordMax.m_x = pa1000s.cx;
		coordMax.m_y = pa1000s.cy;

		DDX_Coord (pDX, TXT_WIDTH, (long &)m_size.cx, m_units, head, true);
		if (m_nType == SHAPETYPE_HORZ || m_nType == SHAPETYPE_RECT) 
			DDV_Coord (pDX, TXT_WIDTH, m_size.cx, 0, m_units, head, DDVCOORDXBOTH, coordMin, coordMax);
		else if (pDX->m_bSaveAndValidate)
			m_size.cx = 0;

		DDX_Coord (pDX, TXT_HEIGHT, (long &)m_size.cy, m_units, head, false);
		if (m_nType == SHAPETYPE_VERT || m_nType == SHAPETYPE_RECT) 
			DDV_Coord (pDX, TXT_HEIGHT, 0, m_size.cy, m_units, head, DDVCOORDYBOTH, coordMin, coordMax);
		else if (pDX->m_bSaveAndValidate)
			m_size.cy = 0;
	}

	{	
		int nWidth1000ths = CBaseElement::LogicalToThousandths (CPoint (GetPA ().cx, 0), head).x;
		int nWidthMax = (int)(((double)nWidth1000ths / 1000.0) * head.GetRes ());

		DDX_Text (pDX, TXT_CY, m_thickness.cy);
		if (m_nType == SHAPETYPE_HORZ || m_nType == SHAPETYPE_RECT) 
			DDV_MinMaxLong (pDX, m_thickness.cy, 1, head.GetChannels ()); 
		else if (pDX->m_bSaveAndValidate)
			m_thickness.cy = 0;

		DDX_Text (pDX, TXT_CX, m_thickness.cx);
		if (m_nType == SHAPETYPE_VERT || m_nType == SHAPETYPE_RECT) 
			DDV_MinMaxLong (pDX, m_thickness.cx, 1, nWidthMax); 
		else if (pDX->m_bSaveAndValidate)
			m_thickness.cx = 0;
	}

	CElementDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CShapeDlg, CElementDlg)
	//{{AFX_MSG_MAP(CShapeDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (RDO_HORIZONTALLINE, OnClickType)
	ON_BN_CLICKED (RDO_VERTICALLINE, OnClickType)
	ON_BN_CLICKED (RDO_RECTANGLE, OnClickType)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CShapeDlg message handlers

int CShapeDlg::BuildElement ()
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();
	CShapeElement & e = GetElement ();

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

CShapeElement & CShapeDlg::GetElement ()
{
	CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CShapeElement)));
	return (CShapeElement &)e;
}

const CShapeElement & CShapeDlg::GetElement () const
{
	const CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CShapeElement)));
	return (CShapeElement &)e;
}

BOOL CShapeDlg::OnInitDialog() 
{
	CElementDlg::OnInitDialog();
	
	if (m_bReadOnlyLocation) {
		GetDlgItem (TXT_WIDTH)->EnableWindow (false);
		GetDlgItem (TXT_HEIGHT)->EnableWindow (false);
	}

	OnClickType ();

	return FALSE;
}


bool CShapeDlg::GetValues ()
{
	CString strWidth, strHeight;
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();

	GetDlgItemText (TXT_WIDTH, strWidth);
	GetDlgItemText (TXT_HEIGHT, strHeight);
	CSize size = CBaseElement::LogicalToThousandths (
		CCoord::CoordToPoint (FoxjetDatabase::_ttof (strWidth), FoxjetDatabase::_ttof (strHeight), m_units, head), head);
	const CSize paLogical = GetPA ();
	const CSize pa1000s = CBaseElement::LogicalToThousandths (CSize (paLogical.cx, head.Height ()), head); 

	if (m_nType == SHAPETYPE_VERT || m_nType == SHAPETYPE_RECT) {
		if (size.cy > pa1000s.cy) {
			CCoord c (paLogical, m_units, head, 1.0);
			CString str;

			str.Format (LoadString (IDS_INVAILDHEIGHTCEILCORRECT), c.FormatY ());

			int nResult = MsgBox (* this, str, LoadString (IDS_WARNING), MB_YESNO | MB_ICONEXCLAMATION);

			if (nResult == IDYES)
				SetDlgItemText (TXT_HEIGHT, c.FormatY ());
			else
				return false;
		}
	}
	
	if (m_nType == SHAPETYPE_VERT || m_nType == SHAPETYPE_RECT) {
		if (size.cx > pa1000s.cx) {
			CCoord c (paLogical, m_units, head, 1.0);
			CString str;

			str.Format (LoadString (IDS_INVAILDWIDTHCEILCORRECT), c.FormatX ());

			int nResult = MsgBox (* this, str, LoadString (IDS_WARNING), MB_YESNO | MB_ICONEXCLAMATION);

			if (nResult == IDYES)
				SetDlgItemText (TXT_WIDTH, c.FormatX ());
			else
				return false;
		}
	}

	return CElementDlg::GetValues ();
}

void CShapeDlg::OnCancel()
{
	CElementDlg::OnCancel ();
}

void CShapeDlg::OnClickType ()
{
	CButton * pHorz = (CButton *)GetDlgItem (RDO_HORIZONTALLINE);
	CButton * pVert = (CButton *)GetDlgItem (RDO_VERTICALLINE);
	CButton * pRect = (CButton *)GetDlgItem (RDO_RECTANGLE);
	CStatic * pBmp = (CStatic *)GetDlgItem (BMP_SHAPE);

	ASSERT (pHorz);
	ASSERT (pVert);
	ASSERT (pRect);
	ASSERT (pBmp);

	GetDlgItem (TXT_WIDTH)->EnableWindow (TRUE);
	GetDlgItem (TXT_HEIGHT)->EnableWindow (TRUE);
	GetDlgItem (TXT_CX)->EnableWindow (TRUE);
	GetDlgItem (TXT_CY)->EnableWindow (TRUE);

	if (pHorz->GetCheck () == 1) {
		GetDlgItem (TXT_HEIGHT)->EnableWindow (FALSE);
		GetDlgItem (TXT_CX)->EnableWindow (FALSE);
		pBmp->SetBitmap (m_bmpHorz);
	}
	else if (pVert->GetCheck () == 1) {
		GetDlgItem (TXT_WIDTH)->EnableWindow (FALSE);
		GetDlgItem (TXT_CY)->EnableWindow (FALSE);
		pBmp->SetBitmap (m_bmpVert);
	}
	else {
		pBmp->SetBitmap (m_bmpRect);
	}
}

void CShapeDlg::Get (const FoxjetElements::ShapeElement::CShapeElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Get (e, pHead);

	m_pt			= e.GetPos (pHead);
	m_nID			= e.GetID ();
	m_size			= e.GetSize ();
	m_thickness		= e.GetThickness ();
	m_nType			= e.GetType ();
}

void CShapeDlg::Set (FoxjetElements::ShapeElement::CShapeElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Set (e, pHead);

	e.SetPos (m_pt, pHead);
	e.SetID (m_nID);
	e.SetSize (m_size);
	e.SetThickness (m_thickness);
	e.SetType ((SHAPETYPE)m_nType);
}
