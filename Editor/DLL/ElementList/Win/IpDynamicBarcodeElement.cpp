// DynamicBarcodeElement.cpp: implementation of the CDynamicBarcodeElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "IpElements.h"
#include "Types.h"
#include "List.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"
#include "Database.h"
#include "Resource.h"
#include "TemplExt.h"
//#include "DynamicTableDlg.h"
#include "Parse.h"

#include "fj_system.h"
#include "fj_printhead.h"
#include "fj_element.h"
#include "BarCode\barcode.h"
#include "fj_dynbarcode.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_font.h"
#include "fj_message.h"
#include "fj_text.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetIpElements;
using namespace Color;
using namespace FoxjetCommon::ElementFields;

static const LPCTSTR lpszKey = _T ("DynTbl");
static const LPCTSTR lpszDynData = _T ("Dynamic data");
const CHAR fj_DynBarCodeDefData[]	= "000000";

static CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> vDynData;

static bool GetDynData (ULONG lLineID, SETTINGSSTRUCT & s)
{
	for (int i = 0; i < ::vDynData.GetSize (); i++) {
		if (::vDynData [i].m_lLineID == lLineID) {
			s = ::vDynData [i];
			return true;
		}
	}

	return false;
}

static bool SetDynData (ULONG lLineID, SETTINGSSTRUCT & s)
{
	for (int i = 0; i < ::vDynData.GetSize (); i++) {
		if (::vDynData [i].m_lLineID == lLineID) {
			::vDynData.SetAt (i, s);
			return true;
		}
	}

	::vDynData.Add (s);

	return true;
}


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNAMIC (CDynamicBarcodeElement, CIpBaseElement)

CDynamicBarcodeElement::CDynamicBarcodeElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head)
:	m_nEncode (-1),
	CIpBaseElement (fj_ElementDynBarCodeNew (), pHead, head)
{
	LPFJDYNBARCODE p = GetMember ();

	p->lHRBold = 1;

	if (p->pBC) {
		p->pBC->flags   = 28;
		p->pBC->flags   = BARCODE_128 | BARCODE_NO_CHECKSUM;
	}
}

CDynamicBarcodeElement::CDynamicBarcodeElement (const CDynamicBarcodeElement & rhs)
:	m_nEncode (-1),
	CIpBaseElement (rhs)
{
}

CDynamicBarcodeElement & CDynamicBarcodeElement::operator = (const CDynamicBarcodeElement & rhs)
{
	CIpBaseElement::operator = (rhs);

	if (this != &rhs) {
		m_nEncode  = -1;
	}

	return * this;
}

CDynamicBarcodeElement::~CDynamicBarcodeElement ()
{
}

bool CDynamicBarcodeElement::SetRedraw (bool bRedraw)
{
	return CIpBaseElement::SetRedraw (bRedraw);
}

int CDynamicBarcodeElement::Build (BUILDTYPE type, bool bCanThrowException)
{
	CArray <CTableItem, CTableItem &> vItems;

	GetDynamicData (GetLineID (), vItems);

	for (int i = 0; i < vItems.GetSize (); i++) {
		CTableItem item = vItems [i];

		if (item.m_lID == GetDynamicID ()) {
			TRACEF (item.ToString ());
			TRACEF (item.m_strData);
			m_strData = item.m_strData;
			//TRACEF (item.GetSqlResult ());
			break;
		}
	}

	return GetClassID ();
}

CString CDynamicBarcodeElement::GetImageData () const
{
	return m_strData; //CString ('0', 10);
}

CString CDynamicBarcodeElement::GetDefaultData () const
{
	return GetImageData ();
}

bool CDynamicBarcodeElement::SetDefaultData (const CString &strData)
{
	return false;
}

void CDynamicBarcodeElement::LoadGlobalParams () 
{
	LPFJDYNBARCODE pBarcode = GetMember ();
	LPFJSYSTEM pSystem = GetSystem ();

	ASSERT (pSystem);

	CString strSettings = CBarcodeElement::GetBarcodeSettings (ListGlobals::GetDB (), GetLineID (), GetHeadType ()); 
	VERIFY (fj_SystemFromString (pSystem, w2a (strSettings)));
}
#include "fj_dyntext.h"

int ELEMENT_API FoxjetIpElements::GetDynamicData (ULONG lLineID, CItemArray & vItems)
{
	SETTINGSSTRUCT s;
	CStringArray v;

	vItems.RemoveAll ();

	if (!GetDynData (lLineID, s))
		GetSettingsRecord (ListGlobals::GetDB (), lLineID, lpszDynData, s);
	
	FoxjetDatabase::FromString (s.m_strData, v);

	for (int i = 0; i < v.GetSize (); i++) {
		CString str = v [i];
		FoxjetIpElements::CTableItem item;

		if (item.FromString (str)) 
			vItems.Add (item);
	}

	// default it if it's not there
	for (int i = v.GetSize (); i < (int)CDynamicTextElement::m_lmtID.m_dwMax; i++) {
		FoxjetIpElements::CTableItem item;

		item.m_lID = i + 1;
		vItems.Add (item);
	}

	{
		for (int i = 0; i < vItems.GetSize (); i++) {
			vItems [i].m_lID = i + 1; // force IDs to be sequential
			//TRACEF (vItems [i].ToString ());
		}
	}

	SetDynamicData (lLineID, vItems);

	return vItems.GetSize ();
}

bool ELEMENT_API FoxjetIpElements::SetDynamicData (ULONG lLineID, CItemArray & vItems)
{
	bool bResult = false;
	SETTINGSSTRUCT s;
	CStringArray v;

	for (int i = 0; i < vItems.GetSize (); i++) 
		v.Add (vItems [i].ToString ());

	s.m_lLineID = lLineID;
	s.m_strKey	= lpszDynData;
	s.m_strData = ToString (v);

	VERIFY (SetDynData (lLineID, s));

	return true;
}

void CDynamicBarcodeElement::CreateImage () 
{
	using FoxjetIpElements::CTableItem;

	if (IsValid ()) {
		/* TODO: rem
		LPFJDYNBARCODE pBarcode = GetMember ();
		LPFJSYSTEM pSystem = GetSystem ();

		ASSERT (pSystem);

		LoadGlobalParams ();

		CArray <CTableItem, CTableItem &> vItems;

		GetDynamicData (GetLineID (), vItems);

		for (int i = 0; i < vItems.GetSize (); i++) {
			CTableItem item = vItems [i];

			if (item.m_lID == GetDynamicID ()) {
				TRACEF (item.ToString ());
				TRACEF (item.m_strData);
				//TRACEF (item.GetSqlResult ());
				break;
			}
		}
		*/

		CIpBaseElement::CreateImage ();
	}
}

CString CDynamicBarcodeElement::GetElementFontName () const
{
	return BARCODE_FONT; 
}

bool CDynamicBarcodeElement::SetElement (LPFJELEMENT pNew)
{
	if (pNew) {
		ASSERT (pNew);
		ASSERT (pNew->pActions->fj_DescGetType () == FJ_TYPE_DYNBARCODE);
		ASSERT (pNew->pDesc);

		Invalidate ();
	}

	return CIpBaseElement::SetElement (pNew);
}

bool CDynamicBarcodeElement::GetOutputText () const
{
	LPFJDYNBARCODE p = GetMember ();
	
	return (p->bHROutput) ? true : false;
}

bool CDynamicBarcodeElement::SetOutputText (bool bValue)
{
	LPFJDYNBARCODE p = GetMember ();

	p->bHROutput = bValue;
	Invalidate ();

	return true;
}

int CDynamicBarcodeElement::GetDynamicID () const
{
	LPFJDYNBARCODE p = GetMember ();
	
	return p->lDynamicID + 1;
}

bool CDynamicBarcodeElement::SetDynamicID (LONG lID)
{
	if (FoxjetCommon::IsValid (lID, CDynamicTextElement::m_lmtID)) {
		LPFJDYNBARCODE p = GetMember ();
		
		p->lDynamicID = lID - 1;
		return true;
	}

	return false;
}

LONG CDynamicBarcodeElement::GetBold () const
{
	return GetMember ()->lBoldValue;
}

bool CDynamicBarcodeElement::SetBold (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, m_lmtBold)) {
		GetMember ()->lBoldValue = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

LONG CDynamicBarcodeElement::GetTextBold () const
{
	return GetMember ()->lHRBold;
}

bool CDynamicBarcodeElement::SetTextBold (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, m_lmtBold)) {
		GetMember ()->lHRBold = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

int CDynamicBarcodeElement::GetWidth () const
{
	return GetMember ()->lWidthValue;
}

bool CDynamicBarcodeElement::SetWidth (int nValue)
{
/* width is not used 
	if (FoxjetCommon::IsValid (nValue, m_lmtWidth)) {
		GetMember ()->lWidthValue = nValue;
		Invalidate ();
		return true;
	}
*/

	return false;
}


LONG CDynamicBarcodeElement::GetBcType () const
{
	return GetMember ()->lSysBCIndex;
}

bool CDynamicBarcodeElement::SetBcType (LONG lValue)
{
	LPFJDYNBARCODE p = GetMember ();

	if (lValue >= 0 && lValue < FJSYS_BARCODES) {
		p->lSysBCIndex = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

CHAR CDynamicBarcodeElement::GetTextAlign () const
{
	return GetMember ()->cHRAlign;
}

bool CDynamicBarcodeElement::SetTextAlign (CHAR cValue)
{
	if (cValue == 'L' || cValue == 'R' || cValue == 'C') {
		GetMember ()->cHRAlign = cValue;
		Invalidate ();
		return true;
	}

	return false;
}

void CDynamicBarcodeElement::Invalidate ()
{
	LoadGlobalParams ();
	m_nEncode = -1;
	CIpBaseElement::Invalidate ();
}

bool CDynamicBarcodeElement::Validate ()
{
	CString str = GetImageData ();

	LoadGlobalParams ();
	const FJSYSBARCODE & global = GetGlobal ();
	const ULONG lBCFlags = global.lBCFlags;
	
	m_nEncode = -1;

	if (Barcode_Item * p = Barcode_Create (w2a (str))) {
		m_nEncode = Barcode_Encode (p, lBCFlags);
		Barcode_Delete (p);
	}

	if (m_nEncode == -1) {
		LPFJSYSTEM pSystem = GetSystem ();

		// try to reimage again with default data
		str = fj_DynBarCodeDefData;
		TRACEF (str);

		if (Barcode_Item * p = Barcode_Create (w2a (str))) {
			m_nEncode = Barcode_Encode (p, lBCFlags);
			Barcode_Delete (p);
		}
	}

	return IsValid ();
}

bool CDynamicBarcodeElement::IsValid () const
{
	return m_nEncode == 0;
}


const FJSYSBARCODE & CDynamicBarcodeElement::GetGlobal () const
{
	LPCFJSYSTEM pSystem = GetSystem ();
	LPCFJDYNBARCODE p = GetMember ();
	
	ASSERT (pSystem);

	LONG lIndex = p->lSysBCIndex;
	ASSERT (lIndex >= 0 && lIndex < FJSYS_BARCODES);

	return pSystem->bcArray [lIndex];
}

static const ElementFields::FIELDSTRUCT lpszFields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType, 	_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszID,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszX,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszY,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszPosition,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszBold,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszWidth,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszFlipV,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFlipH,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszInverse,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszSysBCIndex,	_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszHROutput,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszHRAlign,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszHRBold,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszData,			_T ("0")),	
	ElementFields::FIELDSTRUCT (),
};

const FoxjetCommon::ElementFields::FIELDSTRUCT * CDynamicBarcodeElement::GetFieldBuffer () const
{
	return ::lpszFields;
}

LONG CDynamicBarcodeElement::GetFirstChar () const
{
	return GetMember ()->lFirstChar;
}

bool CDynamicBarcodeElement::SetFirstChar (LONG l)
{
	if (FoxjetCommon::IsValid (l, CDynamicTextElement::m_lmtLen)) {
		GetMember ()->lFirstChar = l;
		return true;
	}

	return false;
}

LONG CDynamicBarcodeElement::GetNumChar () const
{
	return GetMember ()->lNumChar;
}

bool CDynamicBarcodeElement::SetNumChar (LONG l)
{
	if (FoxjetCommon::IsValid (l, CDynamicTextElement::m_lmtLen)) {
		GetMember ()->lNumChar = l;
		return true;
	}

	return false;
}

