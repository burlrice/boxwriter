#if !defined(AFX_DATABASEDLG_H__0818BE92_D117_4CB1_85D8_D29F7175A1EF__INCLUDED_)
#define AFX_DATABASEDLG_H__0818BE92_D117_4CB1_85D8_D29F7175A1EF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DatabaseDlg.h : header file
//

#include "BaseTextDlg.h"
#include "DatabaseElement.h"

/////////////////////////////////////////////////////////////////////////////
// CDatabaseDlg dialog

class CDatabaseDlg : public CBaseTextDlg
{
// Construction
public:
	CDatabaseDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);

	virtual ~CDatabaseDlg ();

// Dialog Data
	//{{AFX_DATA(CDatabaseDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	CString m_strDSN;
	CString m_strTable;
	CString m_strField;
	CString m_strKeyField;
	CString m_strKeyValue;
	int m_nRow;
	bool m_bSQL;
	CString m_strSQL;
	BOOL m_bPrompt;
	int m_nSQLType;
	BOOL m_bSerial;
	BOOL m_bSerialDownload;
	FoxjetCommon::ALIGNMENT m_alignPara;
	BOOL m_bParaEnable;
	long m_lParaWidth;
	int m_nParaGap;
	CString m_strFormat;
	int m_nFormat;
	CString m_strEmpty;
	BOOL m_bMaxWidth;
	long m_lMaxWidth;
	CString m_strBmpWidthField;
	CString m_strBmpHeightField;

	virtual UINT GetDefCtrlID () const { return CB_TABLE; }

	void Get (const FoxjetElements::CDatabaseElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);
	void Set (FoxjetElements::CDatabaseElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDatabaseDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
protected:
	void InitTables (const CString & strDSN, const CString & strTable = _T (""), const CString & strField = _T (""),
		const CString & strKeyField = _T (""), const CString & strBmpWidth = _T (""), const CString & strBmpHeight = _T ("")); 
	void InitFields (const CString & strDSN, const CString & strTable, const CString & strField = _T (""),
		const CString & strKeyField = _T (""), const CString & strBmpWidth = _T (""), const CString & strBmpHeight = _T (""));

	virtual int BuildElement ();
	const FoxjetElements::CDatabaseElement & GetElement () const;
	FoxjetElements::CDatabaseElement & GetElement ();

	virtual bool GetValues ();

	afx_msg void UpdateKeyField();
	afx_msg void OnSelchangeDSN ();
	afx_msg void OnObjects ();
	afx_msg void OnMaxWidth ();

	CString GetCurSelDsn () const;
	CString GetCurSelTable ();
	CString GetCurSelField ();
	CString GetCurSelKeyField ();
	CString GetCurSelBmpWidth ();
	CString GetCurSelBmpHeight ();

	CPtrArray m_vTables;


	// Generated message map functions
	//{{AFX_MSG(CDatabaseDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBrowse();
	afx_msg void OnSelchangeTable();
	afx_msg void OnSelchangeField();
	afx_msg void OnSelect();
	afx_msg void OnSelectKey();
	afx_msg void OnKey();
	afx_msg void OnSelchangeTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnParaEnable();
	//}}AFX_MSG
	afx_msg void OnFormat ();
	afx_msg void OnSetup ();
	afx_msg void OnDbStartSet ();
	afx_msg void OnDbStartConfig ();

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATABASEDLG_H__0818BE92_D117_4CB1_85D8_D29F7175A1EF__INCLUDED_)
