#ifndef __TYPES_H__
#define __TYPES_H__

/*
from fj_element.h

#define FJ_TYPE_TEXT        1      // text
#define FJ_TYPE_BARCODE     2      // bar code
#define FJ_TYPE_BITMAP      3      // bitmap
#define FJ_TYPE_DYNIMAGE    4      // dynamic image
#define FJ_TYPE_DATETIME    5      // date time
#define FJ_TYPE_COUNTER     6      // counter
*/

typedef enum {
	UNKNOWN						= -1,
	FIRST						= 0,
	TEXT						= FIRST,
	BMP,						
	COUNT,
	DATETIME,
	EXPDATE,
	USER,
	SHIFTCODE,
	BARCODE,
	DATABASE,
	SERIAL,
	SHAPE,
	LABEL,
	LAST = LABEL,
} ELEMENTTYPE;

#define FORWARD_DECLARE(fwd_class_name, real_class_name) \
	struct fwd_class_name; \
	typedef struct fwd_class_name real_class_name, FAR * LP##real_class_name, FAR * const CLP##real_class_name; \
	typedef const struct fwd_class_name FAR * LPC##real_class_name, FAR * const CLPC##real_class_name; 

#endif //__TYPES_H__
