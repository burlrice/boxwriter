#if !defined(AFX_DAYOFWEEKDLG_H__AF334E01_3222_450A_9AEA_C04A32266201__INCLUDED_)
#define AFX_DAYOFWEEKDLG_H__AF334E01_3222_450A_9AEA_C04A32266201__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DayOfWeekDlg.h : header file
//

#include "resource.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CDayOfWeekDlg dialog

class CDayOfWeekDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CDayOfWeekDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDayOfWeekDlg)
	int		m_nDay;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDayOfWeekDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDayOfWeekDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DAYOFWEEKDLG_H__AF334E01_3222_450A_9AEA_C04A32266201__INCLUDED_)
