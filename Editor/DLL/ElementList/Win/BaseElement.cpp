// BaseElement.cpp: implementation of the CBaseElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <math.h>

#include "BaseElement.h"
#include "Coord.h"
#include "Debug.h"
#include "Types.h"
#include "ListImp.h"
#include "Compare.h"
#include "Database.h"
#include "Extern.h"
#include "Utils.h"
#include "AppVer.h"
#include "IvRegs.h"
#include "Parse.h"
#include "BarcodeElement.h"
#include "color.h"

using namespace FoxjetCommon::ElementFields;
using namespace ListGlobals;
using namespace FoxjetCommon;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

static const ElementFields::FIELDSTRUCT lpszFields [] =
{
	ElementFields::FIELDSTRUCT ( m_lpszElementType, 	_T ("")),
	ElementFields::FIELDSTRUCT ( m_lpszID,				_T ("")),
	ElementFields::FIELDSTRUCT ( m_lpszX,				_T ("")),
	ElementFields::FIELDSTRUCT ( m_lpszY,				_T ("")),
	ElementFields::FIELDSTRUCT (),
};

UINT CBaseElement::nNextID = 0;

IMPLEMENT_DYNAMIC (CBaseElement, CObject);

CBaseElement::CBaseElement(const FoxjetDatabase::HEADSTRUCT & head)
:	m_pt (0, 0), 
	m_bFlippedH (false), 
	m_bFlippedV (false), 
	m_bInverse (false), 
	m_alignment (ALIGNMENT_LEFT),
	m_nID (nNextID++), 
	m_bRedraw (true), 
	m_lLocalParamUID (0),
	m_lLineID (-1),
	m_bSnap (true),
	m_head (head),
	m_rcLast (0, 0, 0, 0),
	m_nDither (0),
	m_rgb (Color::rgbBlack)
{
}

CBaseElement::CBaseElement (const CBaseElement & rhs)
:	m_pt (rhs.m_pt), 
	m_bFlippedH (rhs.m_bFlippedH), 
	m_bFlippedV (rhs.m_bFlippedV), 
	m_bInverse (rhs.m_bInverse), 
	m_alignment (rhs.m_alignment),
	m_nID (rhs.m_nID), 
	m_bRedraw (true),
	m_lLineID (rhs.m_lLineID),
	m_lLocalParamUID (rhs.m_lLocalParamUID ),
	m_bSnap (rhs.m_bSnap),
	m_head (rhs.m_head),
	m_rcLast (rhs.m_rcLast),
	m_nDither (rhs.m_nDither),
	m_rgb (rhs.m_rgb)
{
}

CBaseElement & CBaseElement::operator = (const CBaseElement & rhs)
{
	if (this != &rhs) {
		m_pt			= rhs.m_pt;
		m_bFlippedH		= rhs.m_bFlippedH;
		m_bFlippedV		= rhs.m_bFlippedV;
		m_bInverse		= rhs.m_bInverse;
		m_alignment		= rhs.m_alignment;
		m_nID			= rhs.m_nID;
		m_lLineID		= rhs.m_lLineID;
		m_bRedraw		= true;
		m_head			= rhs.m_head;
		m_bSnap			= rhs.m_bSnap;
		m_rcLast		= rhs.m_rcLast;
		m_nDither		= rhs.m_nDither;
		m_rgb			= rhs.m_rgb;
	}

	return * this;
}

CBaseElement::~CBaseElement()
{
}

CSize CBaseElement::GetValveDotSize ()
{
	return CSize (5, 5); 
}

CSize CBaseElement::GetValveDotSize (const FoxjetDatabase::HEADSTRUCT & head)
{
	using namespace FoxjetDatabase;

	if (!IsValveHead (head))
		return CSize (0, 0);

	return CBaseElement::GetValveDotSize ();
}

CCoord CBaseElement::GetValveRes (const FoxjetDatabase::HEADSTRUCT & head)
{
	using namespace FoxjetDatabase;
	CCoord result;

	if (IsValveHead (head)) {
		const int nChannels = head.GetChannels ();
		const int nHeight = LogicalToThousandths (CPoint (0, head.Height ()), head).y;

		result.m_x = head.GetRes ();

		result.m_y = ((double)nChannels / (double)nHeight * 1000.0);
	}

	return result;
}

CPoint CBaseElement::Snap (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT & head)
{
	using namespace FoxjetDatabase;

	double d [] = 
	{

		1.0 / head.GetRes (),
		head.GetSpan () / (double)head.GetChannels (),
	};
	int n [] = 
	{
		DivideAndRound (d [0] * 1000.0, 1),
		DivideAndRound (d [1] * 1000.0, 1),
	};

	// round to nearest pixel
	int x = Round (pt.x, n [0]);
	int y = Round (pt.y, n [1]);

	//CString str; str.Format ("%d --> %d [%f]\t%d --> %d [%f]", pt.x, x, (double)x / (double)n [0], pt.y, y, (double)y / (double)n [1]); TRACEF (str);

	return CPoint (x, y);
}

bool CBaseElement::Move (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT & head)
{
	bool bSnap = GetSnap ();

	SetSnap (false);
	bool bResult = SetPos (GetPosInternal () + pt, NULL);
	SetSnap (bSnap);

	return bResult;
}


bool CBaseElement::SetFlippedH (bool bFlip)
{
	SetRedraw ();
	m_bFlippedH = bFlip;
	return true;
}

bool CBaseElement::SetFlippedV (bool bFlip)
{
	SetRedraw ();
	m_bFlippedV = bFlip;
	return true;
}

bool CBaseElement::SetInverse (bool bInverse)
{
	SetRedraw ();
	m_bInverse = bInverse;
	return true;
}

bool CBaseElement::SetID (UINT nID) 
{
	if (nID >= 0) {
		m_nID = nID;
		return true;
	}

	return false;
}

CPoint CBaseElement::GetPosInternal (ALIGNMENT_ADJUST adj) const
{
	return GetPosInternal (m_pt, adj);
}

CPoint CBaseElement::GetPosInternal (CPoint pt, ALIGNMENT_ADJUST adj) const
{
	return GetPosInternal (pt, m_rcLast.Size (), adj);
}

CPoint CBaseElement::GetPosInternal (CPoint pt, const CSize & size, ALIGNMENT_ADJUST adj) const
{
	switch (adj) {
	case ALIGNMENT_ADJUST_SUBTRACT:
		switch (GetAlignment ()) {
		case ALIGNMENT_CENTER:
			pt.x -= (size.cx / 2);
			break;
		case ALIGNMENT_RIGHT:
			pt.x -= size.cx;
			break;
		}
		break;
	case ALIGNMENT_ADJUST_ADD:
		switch (GetAlignment ()) {
		case ALIGNMENT_CENTER:
			pt.x += (size.cx / 2);
			break;
		case ALIGNMENT_RIGHT:
			pt.x += size.cx;
			break;
		}
		break;
	}

	return pt; 
}

CPoint CBaseElement::GetPos (const FoxjetDatabase::HEADSTRUCT * pHead) const 
{ 
	return GetPosInternal (ALIGNMENT_ADJUST_SUBTRACT);
}

bool CBaseElement::SetPos (const CPoint & ptSet, const FoxjetDatabase::HEADSTRUCT * pHead, ALIGNMENT_ADJUST adj)
{
	using namespace FoxjetDatabase;
	using namespace FoxjetElements;

	CPoint pt = GetPosInternal (ptSet, adj);

	if (IsMovable ()) {
		m_pt = pt;

		if (CBaseTextElement * p = DYNAMIC_DOWNCAST (CBaseTextElement, this)) 
			if (pHead && (p->GetFontSize () >= (UINT)pHead->GetChannels ()))
				m_pt.y = 0;

		if (GetSnap ()) {
			if (pHead) {
				if (IsValveHead (* pHead)) 
					m_pt = Snap (m_pt, * pHead);
			}
			else {
				if (IsValveHead (m_head)) 
					m_pt = Snap (m_pt, m_head);
			}
		}

		return true;
	}

	return false;
}

CPoint CBaseElement::AdjustPos (const FoxjetDatabase::HEADSTRUCT & head, ALIGNMENT_ADJUST adj)
{
	const CPoint ptOld = GetPos (NULL);
	CCoord coord (CBaseElement::ThousandthsToLogical (ptOld, head), INCHES, head); 
	CPoint ptNew ((int)(coord.m_x * 1000.0), (int)(coord.m_y * 1000.0));
	
	CRect rc = GetWindowRect (head);
	SetPos (ptNew, NULL, adj);

	return ptNew;
}

UINT CBaseElement::GetID () const 
{ 
	return m_nID; 
}

bool CBaseElement::IsFlippedH () const 
{ 
	return m_bFlippedH; 
}

bool CBaseElement::IsFlippedV () const 
{ 
	return m_bFlippedV; 
}

bool CBaseElement::IsInverse () const 
{ 
	return m_bInverse; 
}

bool CBaseElement::SetRedraw (bool bRedraw)
{
	m_bRedraw = bRedraw;
	return true;
}

bool CBaseElement::GetRedraw ()
{
	return m_bRedraw;
}

CString CBaseElement::GetFieldName (int nIndex) const
{
	if (const ElementFields::FIELDSTRUCT * lp = GetFieldBuffer ()) {
		if (nIndex >= 0 && nIndex < GetFieldCount ())
			return lp [nIndex].m_strField;
	}

	return _T ("");
}

CString CBaseElement::GetFieldDef (int nIndex) const
{
	if (const ElementFields::FIELDSTRUCT * lp = GetFieldBuffer ()) {
		if (nIndex >= 0 && nIndex < GetFieldCount ())
			return lp [nIndex].m_strDef;
	}

	return _T ("");
}

int CBaseElement::GetFieldIndex (const CString & str) const
{
	if (const ElementFields::FIELDSTRUCT * lp = GetFieldBuffer ()) {
		for (int i = 0; i < GetFieldCount (); i++)
			if (!str.CompareNoCase (lp [i].m_strField))
				return i;
	}

	return -1;
}

int CBaseElement::GetFieldCount () const
{
	if (const ElementFields::FIELDSTRUCT * lp = GetFieldBuffer ()) {
		int i;

		for (i = 0; lp [i].m_strField.GetLength (); i++)
			;

		return i;
	}	

	return 0;
}

const FoxjetCommon::ElementFields::FIELDSTRUCT * CBaseElement::GetFieldBuffer () const
{
	return ::lpszFields;
}

float CBaseElement::CalcPrintRes (const FoxjetDatabase::HEADSTRUCT & head)
{
	//float fEncoderWheel = (float)head.m_nHorzRes;
	//int nEncoderDivisor = head.m_nEncoderDivisor;
	float fPrintRes = (float)head.GetRes (); //(float)(fEncoderWheel / (float)nEncoderDivisor);

	return fPrintRes;
}

void CBaseElement::Parse (const CString & str, const CVersion & ver, CStringArray & v)
{
	using namespace FoxjetDatabase;

	const int nFields = GetFieldCount ();
	CStringArray vDef;

	v.RemoveAll ();
	Tokenize (str, v);
	Tokenize (ListGlobals::defElements.GetRegElementDefs (GetClassID (), ver), vDef);

	while (vDef.GetSize () < nFields)
		vDef.Add (GetFieldDef (vDef.GetSize ()));

	while (v.GetSize () > nFields) // delete extra tokens
		v.RemoveAt (v.GetSize () - 1);

	while (v.GetSize () < nFields) {
		int nIndex = v.GetSize ();
		CString strDef = vDef [nIndex];

		if (strDef.IsEmpty ())
			strDef = GetFieldDef (nIndex);
			
		v.Add (strDef);
	}

	for (int i = 0; i < v.GetSize (); i++) { 
		if (v [i].IsEmpty ()) {
			CString strDef = vDef [i];

			if (strDef.IsEmpty ())
				strDef = GetFieldDef (i);
				
			v.SetAt (i, strDef);
		}
	}
}

bool CBaseElement::SetDither (int n)
{
	if (HasDither ()) {
		 m_nDither = n;
		 SetRedraw (true);
		return true;
	}
	
	return false;
}


bool CBaseElement::IsPrintable () const 
{ 
	return m_rgb == Color::rgbBlack; 
}

bool CBaseElement::SetColor (COLORREF rgb)
{
	m_rgb = rgb;
	SetRedraw (true);
	return true;
}

////////////////////////////////////////////////////////////////////////
IMPLEMENT_DYNAMIC (CBaseTextElement, CBaseElement);

CBaseTextElement::CBaseTextElement (const FoxjetDatabase::HEADSTRUCT & head)
:	CBaseElement (head)
{
}

CBaseTextElement::CBaseTextElement (const CBaseTextElement & rhs)
:	CBaseElement (rhs)
{
}

CBaseTextElement & CBaseTextElement::operator = (const CBaseTextElement & rhs)
{
	CBaseElement::operator = (rhs);
	
	if (this != &rhs) {
		CPrinterFont fnt;
		rhs.GetFont (fnt);
		SetFont (fnt);
	}

	return * this;
}

CBaseTextElement::~CBaseTextElement ()
{
}

bool CBaseTextElement::SetFontName (const CString & strName)
{
	CPrinterFont fnt;
	GetFont (fnt);
	SetRedraw ();
	fnt.m_strName = strName;
	return SetFont (fnt);
}

CString CBaseTextElement::GetFontName () const
{
	CPrinterFont fnt;
	GetFont (fnt);
	return fnt.m_strName;
}

bool CBaseTextElement::SetFontSize (UINT nSize)
{
	CPrinterFont fnt;
	GetFont (fnt);
	SetRedraw ();
	fnt.m_nSize = nSize;

	return SetFont (fnt);
}

UINT CBaseTextElement::GetFontSize () const
{
	CPrinterFont fnt;
	GetFont (fnt);
	return fnt.m_nSize;
}

bool CBaseTextElement::SetFontBold (bool bBold)
{
	CPrinterFont fnt;
	GetFont (fnt);
	SetRedraw ();
	fnt.m_bBold = bBold;
	return SetFont (fnt);
}

bool CBaseTextElement::GetFontBold () const
{
	CPrinterFont fnt;
	GetFont (fnt);
	return fnt.m_bBold;
}

bool CBaseTextElement::SetFontItalic (bool bItalic)
{
	CPrinterFont fnt;
	GetFont (fnt);
	SetRedraw ();
	fnt.m_bItalic = bItalic;
	return SetFont (fnt);
}

bool CBaseTextElement::GetFontItalic () const
{
	CPrinterFont fnt;
	GetFont (fnt);
	return fnt.m_bItalic;
}

void CBaseElement::CalcStretch (const FoxjetDatabase::HEADSTRUCT & head, double dStretch [2])
{
	dStretch [0] = dStretch [1] = 1.0;

	int nHeight = head.Height ();
	double dFactor = 1.0;

	dStretch [1] = ((double)nHeight * dFactor) / (double)head.GetChannels ();

#ifdef __WYSIWYGFIX__
	double dPrintRes	= (double)CalcPrintRes (head);
	double dScreenRes	= CCoord::GetPixelsPerXUnit (INCHES, head);

	dStretch [0] = dPrintRes / dScreenRes;
#endif
}

CRect CBaseElement::GetWindowRect (const FoxjetDatabase::HEADSTRUCT & head) const
{
	CPoint pt = GetPosInternal ();
	CSize size = GetSize (head);
	double dStretch [2] = { 1, 1 };

	CalcStretch (head, dStretch);

	size.cx = (int)((double)size.cx * dStretch [0]);

#ifdef __WYSIWYGFIX__
	{
		size = GetSize (head);
		size.cx = (int)((double)size.cx / dStretch [0]);
	}
#endif

	if (!IsResizable ()) 
		size.cy = (int)((double)size.cy * dStretch [1]);

	* (CRect *)(LPVOID)&m_rcLast = CRect (pt.x, pt.y, pt.x + size.cx, pt.y + size.cy);

	return m_rcLast;
}

bool CBaseElement::SetWindowRect (const CSize & size, const FoxjetDatabase::HEADSTRUCT & head)
{
	CRect rc = CRect (CPoint (0, 0), size) + GetPosInternal ();

	return SetWindowRect (rc, head);
}

bool CBaseElement::SetWindowRect (const CRect & rc, const FoxjetDatabase::HEADSTRUCT & head)
{
	bool bResult = false;

	if (IsMovable ()) {
		CPoint pt = rc.TopLeft ();
		
		if (IsResizable ()) {
			CSize size = rc.Size ();

			double dStretch [2] = { 0 };

			CalcStretch (head, dStretch);

#ifndef __WYSIWYGFIX__
			size.cx = (int)((double)size.cx / dStretch [0]);
#endif

			bResult = SetSize (size, head) && SetPos (pt, &head);
		}
		else
			bResult = SetPos (pt, &head);
	}

	return bResult;
}

void CBaseElement::ClipTo (const FoxjetDatabase::HEADSTRUCT & head)
{
	FoxjetDatabase::PANELSTRUCT p;

	if (ListGlobals::IsDBSet ())
		if (FoxjetDatabase::GetPanelRecord (GetDB (), head.m_lPanelID, p))
			m_lLineID = p.m_lLineID;
}

int CBaseElement::StretchByAngle (int cy, double dAngle)
{
	static const double dPI = PI;
	double dRadians = (double)dAngle * dPI / 180.0;
	double d = (double)cy * sin (dRadians);

	return (int)floor (d);
}

bool CBaseElement::SetHead (const FoxjetDatabase::HEADSTRUCT & head)
{
	m_head = head;
	return true;
}

const FoxjetDatabase::HEADSTRUCT & CBaseElement::GetHead () const
{
	return m_head;
}

bool CBaseElement::SetXPos (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	int y = GetPosInternal ().y;

	return SetPos (CPoint (pt.x, y), pHead);
}

bool CBaseElement::SetYPos (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	int x = GetPosInternal ().x;

	return SetPos (CPoint (x, pt.y), pHead);
}

void CBaseElement::Reset ()
{
}

CPoint CBaseElement::LogicalToThousandths (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT & head)
{
//	if (FoxjetDatabase::IsValveHead (head)) 
//		return CBaseElement::LogicalToThousandths (pt, head, const_cast <FoxjetDatabase::CValveArray &> (head.m_vValve));

	CCoord c (pt, INCHES, head);
	return CPoint ((int)ceil ((c.m_x * 1000.0)), (int)ceil ((c.m_y * 1000.0)));
}

CPoint CBaseElement::ThousandthsToLogical (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT & head)
{
//	if (FoxjetDatabase::IsValveHead (head))
//		return CBaseElement::ThousandthsToLogical (pt, head, const_cast <FoxjetDatabase::CValveArray &> (head.m_vValve));

	return CCoord::CoordToPoint (
		(double)pt.x / 1000.0, 
		(double)pt.y / 1000.0, 
		INCHES, head);
}


CPoint CBaseElement::ThousandthsToLogical (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT & head, FoxjetDatabase::CValveArray & v)
{
	using namespace FoxjetDatabase;

	CPoint ptResult;// = CBaseElement::ThousandthsToLogical (pt, head);
	{ // other version can call this one recursively
		ptResult = CCoord::CoordToPoint ((double)pt.x / 1000.0, (double)pt.y / 1000.0, INCHES, head);
	}
	int nTotalPixels	= 0;
	int nPixels			= 0;
	ULONG lTotalInches	= 0;
	ULONG lInches		= 0;

	ASSERT (v.GetSize ());

	for (int i = 0; i < v.GetSize (); i++) {
		VALVETYPE & type = v [i].m_type;
		
		nPixels		= GetValveChannels (type);
		lInches		= GetValveHeight (type);

		if ((ULONG)pt.y >= lTotalInches && (ULONG)pt.y < (lTotalInches + lInches)) 
			break;

		nTotalPixels += nPixels;
		lTotalInches += lInches;
	}

	int y = pt.y - lTotalInches;

	ASSERT (y >= 0);

	ptResult.y = nTotalPixels;
	double dy = ((double)nPixels / (double)lInches) * (double)y;
	ptResult.y += (int)ceil (dy);

	return ptResult;
}

CPoint CBaseElement::LogicalToThousandths (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT & head, FoxjetDatabase::CValveArray & v)
{
	using namespace FoxjetDatabase;

	CPoint ptResult; // = CBaseElement::LogicalToThousandths (pt, head);
	{ // other version can call this one recursively
		CCoord c (pt, INCHES, head);
		ptResult = CPoint ((int)ceil ((c.m_x * 1000.0)), (int)ceil ((c.m_y * 1000.0)));
	}

	int nTotalPixels	= 0;
	int nPixels			= 0;
	ULONG lTotalInches	= 0;
	ULONG lInches		= 0;

	ASSERT (v.GetSize ());

	for (int i = 0; i < v.GetSize (); i++) {
		VALVETYPE & type = v [i].m_type;
		
		nPixels		= GetValveChannels (type);
		lInches		= GetValveHeight (type);

		if (pt.y >= nTotalPixels && pt.y < (nTotalPixels + nPixels)) 
			break;

		nTotalPixels += nPixels;
		lTotalInches += lInches;
	}

	int y = pt.y - nTotalPixels;

	ASSERT (y >= 0);

	ptResult.y = lTotalInches;
	double dy = ((double)lInches / (double)nPixels) * (double)y;
	ptResult.y += (int)dy;

	return ptResult;
}

void CBaseElement::Increment ()
{
}

