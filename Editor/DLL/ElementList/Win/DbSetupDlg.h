#if !defined(AFX_DBSETUPDLG_H__2A170D02_8FC1_4F71_B9E2_C5995AC971A6__INCLUDED_)
#define AFX_DBSETUPDLG_H__2A170D02_8FC1_4F71_B9E2_C5995AC971A6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DbSetupDlg.h : header file
//

#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CDbSetupDlg dialog

class CDbSetupDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CDbSetupDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDbSetupDlg)
	CString	m_strDSN;
	CString	m_strFile;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDbSetupDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void UpdateUI();
	virtual void OnOK();

	bool CreateAccessDsn ();
	bool CreateExcelDsn ();

	// Generated message map functions
	//{{AFX_MSG(CDbSetupDlg)
	afx_msg void OnBrowse();
	afx_msg void OnChangeDsn();
	virtual BOOL OnInitDialog();
	afx_msg void OnOther();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DBSETUPDLG_H__2A170D02_8FC1_4F71_B9E2_C5995AC971A6__INCLUDED_)
