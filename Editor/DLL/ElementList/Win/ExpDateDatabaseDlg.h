#if !defined(AFX_EXPDATEDATABASEDLG_H__B27BBAE6_1DB0_4655_B71D_A8457B7ECD7E__INCLUDED_)
#define AFX_EXPDATEDATABASEDLG_H__B27BBAE6_1DB0_4655_B71D_A8457B7ECD7E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExpDateDatabaseDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CExpDateDatabaseDlg dialog

class CExpDateDatabaseDlg : public CDialog
{
// Construction
public:
	CExpDateDatabaseDlg(ULONG lLineID, CWnd* pParent = NULL);   // standard constructor
	virtual ~CExpDateDatabaseDlg ();

// Dialog Data
	//{{AFX_DATA(CExpDateDatabaseDlg)
	enum { IDD = IDD_EXPDATE_DATABASE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	CString m_strPrompt;
	CString m_strDSN;
	CString m_strTable;
	CString m_strField;
	CString m_strKeyField;
	CString m_strKeyValue;
	int		m_bPromptAtTaskStart;
	int		m_nSQLType;
	int		m_nFormat;
	CString	m_strFormat;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExpDateDatabaseDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();

	void InitTables (const CString & strDSN, const CString & strTable = _T (""), const CString & strField = _T (""),
		const CString & strKeyField = _T ("")); 
	void InitFields (const CString & strDSN, const CString & strTable, const CString & strField = _T (""),
		const CString & strKeyField = _T (""));
	CString GetCurSelDsn () const;
	CString GetCurSelField ();
	CString GetCurSelKeyField ();
	void UpdateKeyField();

	CPtrArray m_vTables;
	const ULONG m_lLineID;

	// Generated message map functions
	//{{AFX_MSG(CExpDateDatabaseDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeDsn();
	afx_msg void OnSelchangeTable();
	afx_msg void OnSelchangeField();
	afx_msg void OnSelect();
	afx_msg void OnSelectkey();
	afx_msg void OnFormat();
	afx_msg void OnKey();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXPDATEDATABASEDLG_H__B27BBAE6_1DB0_4655_B71D_A8457B7ECD7E__INCLUDED_)
