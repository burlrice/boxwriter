// DbWizardFinishDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DbWizardFinishDlg.h"


using namespace FoxjetElements;

// CDbWizardFinishDlg

IMPLEMENT_DYNAMIC(CDbWizardFinishDlg, CDbWizardBasePage)

CDbWizardFinishDlg::CDbWizardFinishDlg(CDbWizardDlg * pParent)
:	m_bTemplate (false),
	CDbWizardBasePage (pParent, IDD_DATABASE_WIZARD_FINISH)
{
}

CDbWizardFinishDlg::~CDbWizardFinishDlg()
{
}


BEGIN_MESSAGE_MAP(CDbWizardFinishDlg, CDbWizardBasePage)
	ON_BN_CLICKED (CHK_YES, OnUpdateUI)
	ON_BN_CLICKED (CHK_NO, OnUpdateUI)
END_MESSAGE_MAP()



// CDbWizardFinishDlg message handlers

BOOL CDbWizardFinishDlg::OnInitDialog ()
{
	CDbWizardBasePage::OnInitDialog ();

	if (CButton * p = (CButton *)GetDlgItem (CHK_YES)) 
		p->SetCheck (1);

	OnUpdateUI ();

	return TRUE;
}

BOOL CDbWizardFinishDlg::OnSetActive()
{
	return TRUE;
}

void CDbWizardFinishDlg::OnUpdateUI ()
{
	if (CButton * pYes = (CButton *)GetDlgItem (CHK_YES)) 
		m_bTemplate = (pYes->GetCheck () == 1) ? true : false;
}
