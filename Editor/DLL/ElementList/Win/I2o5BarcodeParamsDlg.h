#if !defined(AFX_I2O5BARCODEPARAMSDLG_H__449E2EEE_D1AD_44A6_BF45_E85FB1F02F52__INCLUDED_)
#define AFX_I2O5BARCODEPARAMSDLG_H__449E2EEE_D1AD_44A6_BF45_E85FB1F02F52__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// I2o5BarcodeParamsDlg.h : header file
//

#include "UpcBarcodeParamsDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CI2o5BarcodeParamsDlg dialog

class CI2o5BarcodeParamsDlg : public CUpcBarcodeParamsDlg
{
// Construction
public:
	CI2o5BarcodeParamsDlg(FoxjetDatabase::HEADTYPE type, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CI2o5BarcodeParamsDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	int		m_nHorzBearer;
	int		m_nVertBearer;
	int		m_nQuietZone;
	int		m_nAlignment;
	int		m_nCheckSum;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CI2o5BarcodeParamsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual int GetDefSymbology () const;

	CBitmap m_bmpCaption [7];

	// Generated message map functions
	//{{AFX_MSG(CI2o5BarcodeParamsDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	afx_msg void InvalidatePreview ();

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_I2O5BARCODEPARAMSDLG_H__449E2EEE_D1AD_44A6_BF45_E85FB1F02F52__INCLUDED_)
