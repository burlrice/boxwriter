// RolloverDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "RolloverDlg.h"
#include "Extern.h"
#include "List.h"
#include "EditValueDlg.h"
#include "EditTimeDlg.h"
#include "Debug.h"
#include "DayOfWeekDlg.h"
#include "AppVer.h"
#include "DateTimeElement.h"
#include "SystemDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace CodesDlg;

static const LPCTSTR lpszFormat = _T ("%I:%M %p, ");
static const LPCTSTR lpszHold = _T ("Hold start date");

/////////////////////////////////////////////////////////////////////////////
// CRolloverDlg property page

IMPLEMENT_DYNCREATE(CRolloverDlg, CDaysDlg)

CRolloverDlg::CRolloverDlg() 
:	m_nHours (0),
	CDaysDlg(ListGlobals::GetDB (), IDD_PROP_ROLLOVER)
{
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CRolloverDlg::CRolloverDlg(FoxjetDatabase::COdbcDatabase & db, const CodesDlg::CCodesArray & vCodes) 
:	m_nHours (0),
	m_vCodes (vCodes),
	CDaysDlg (db, IDD_PROP_ROLLOVER)
{
	//{{AFX_DATA_INIT(CRolloverDlg)
	//}}AFX_DATA_INIT
	m_psp.dwFlags |= PSH_USECALLBACK;
    m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
}

CRolloverDlg::~CRolloverDlg()
{
}

void CRolloverDlg::DoDataExchange(CDataExchange* pDX)
{
	CDaysDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRolloverDlg)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRolloverDlg, CDaysDlg)
	//{{AFX_MSG_MAP(CRolloverDlg)
	ON_BN_CLICKED(CHK_HOLD, OnHold)
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE(CB_LINE, OnSelchangeLine)
	ON_BN_CLICKED (BTN_EDIT, OnEdit)
	ON_BN_CLICKED (BTN_EDITFIRSTDAYOFWEEK, OnEditFirstDayOfWeek)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRolloverDlg message handlers

CCodes CRolloverDlg::GetCodes (ULONG lLineID) const
{
	CCodes c = CDateTimePropShtDlg::GetCodes (lLineID, m_vCodes);
	
	ASSERT (c.m_vCodes.GetSize () == 2);

	return c;
}

void CRolloverDlg::Update (ULONG lLineID, UINT nIndex, const CString & strValue)
{
	lLineID = FoxjetDatabase::GetMasterLineID (lLineID);

	#if __CUSTOM__ == __SW0877__
	lLineID = 0;
	#endif

	CCodes c = CDateTimePropShtDlg::GetCodes (lLineID, m_vCodes);

	ASSERT (c.m_vCodes.GetSize () == 2);
	c.m_vCodes [nIndex] = strValue;
	VERIFY (CDateTimePropShtDlg::SetCodes (m_db, lLineID, c, m_vCodes, ListGlobals::Keys::m_strRollover));
}

CString CRolloverDlg::GetDay (int nDay)
{
	TCHAR sz [512] = { 0 };
	UINT n [] = 
	{
		LOCALE_SDAYNAME7, 
		LOCALE_SDAYNAME1, 
		LOCALE_SDAYNAME2, 
		LOCALE_SDAYNAME3, 
		LOCALE_SDAYNAME4, 
		LOCALE_SDAYNAME5, 
		LOCALE_SDAYNAME6, 
	};

	::GetLocaleInfo (LOCALE_SYSTEM_DEFAULT, n [nDay], sz, sizeof (sz));
	
	return CString (sz);
}

void CRolloverDlg::OnEditFirstDayOfWeek ()
{
	CDayOfWeekDlg dlg (this);
	ULONG lLineID = GetCurLine ();
	CCodes c = GetCodes (lLineID);

	dlg.m_nDay = _ttoi (c.m_vCodes [1]);

	if (dlg.DoModal () == IDOK) {
		CString strValue;

		strValue.Format (_T ("%d"), dlg.m_nDay);
		Update (lLineID, 1, strValue);
		SetDlgItemText (TXT_FIRSTDAYOFWEEK, GetDay (dlg.m_nDay));
	}
}

void CRolloverDlg::OnEdit ()
{
	CEditTimeDlg dlg (this);
	ULONG lLineID = GetCurLine ();
	CCodes c = GetCodes (lLineID);

	int nRollover = _ttoi (c.m_vCodes [0]);

	if (nRollover > 0) {
		dlg.m_nDirection = 0;
		nRollover += -(24 * 60);
	}
	else {
		dlg.m_nDirection = 1;
	}

	dlg.m_tm = CTime (2004, 1, 1, 0, 0, 0) + CTimeSpan (0, 0, abs (nRollover), 0);

	if (dlg.DoModal () == IDOK) {
		CString strValue, str = dlg.m_tm.Format (::lpszFormat);

		nRollover = (dlg.m_tm.GetHour () * 60) + dlg.m_tm.GetMinute ();
		
		if (dlg.m_nDirection == 0) {
			nRollover = ((24 * 60) - nRollover);
			str += LoadString (IDS_BEFOREMIDNIGHT);
		}
		else {
			nRollover = -nRollover;
			str += LoadString (IDS_AFTERMIDNIGHT);
		}

		SetDlgItemText (TXT_HOURS, str);
	
		strValue.Format (_T ("%d"), nRollover);
		Update (lLineID, 0, strValue);
	}
}

void CRolloverDlg::OnSelchangeLine()
{
	CString str = _T ("0");
	CDaysDlg::OnSelchangeLine ();

	CCodes c = CDateTimePropShtDlg::GetCodes (GetCurLine (), m_vCodes);

	if (c.m_vCodes.GetSize () > 0) {
		int nRollover = _ttoi (c.m_vCodes [0]);
		int nDirection = 1;
		
		if (nRollover > 0) {
			nRollover += -(24 * 60);
			nDirection = 0;
		}

		CTime tm = CTime (2004, 1, 1, 0, 0, 0) + CTimeSpan (0, 0, abs (nRollover), 0);

		str = tm.Format (lpszFormat);
		str += LoadString (nDirection > 0 ? IDS_AFTERMIDNIGHT : IDS_BEFOREMIDNIGHT);
	}

	if (c.m_vCodes.GetSize () > 1) {
		int nDay = _ttoi (c.m_vCodes [1]);
		
		SetDlgItemText (TXT_FIRSTDAYOFWEEK, GetDay (nDay));
	}

	SetDlgItemText (TXT_HOURS, str);

	if (CButton * p = (CButton *)GetDlgItem (CHK_HOLD)) 
		p->SetCheck (GetHold (m_db, GetCurLine ()));
}

BOOL CRolloverDlg::OnInitDialog() 
{
	CDaysDlg::OnInitDialog();
	
	OnSelchangeLine ();
	
/* 1.20 std 
#if __CUSTOM__ != __SW0831__
	if (CWnd * p = GetDlgItem (CHK_HOLD))
		p->ShowWindow (SW_HIDE);
#endif
*/

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

bool CRolloverDlg::GetHold (FoxjetDatabase::COdbcDatabase & db, ULONG lLineID)
{
	SETTINGSSTRUCT s;

	lLineID = FoxjetDatabase::GetMasterLineID (lLineID);

	if (GetSettingsRecord (db, lLineID, ::lpszHold, s)) 
		return _ttoi (s.m_strData) ? true : false;

	return false;
}

void CRolloverDlg::OnHold() 
{
// 1.20 std // #if __CUSTOM__ == __SW0831__
	if (CButton * p = (CButton *)GetDlgItem (CHK_HOLD)) {
		bool bHold = p->GetCheck () == 1;
		SETTINGSSTRUCT s;

		s.m_lLineID = FoxjetDatabase::GetMasterLineID (GetCurLine ());
		s.m_strKey = ::lpszHold;
		s.m_strData.Format (_T ("%d"), bHold);

		if (!UpdateSettingsRecord (m_db, s))
			VERIFY (AddSettingsRecord (m_db, s));

		FoxjetElements::CDateTimeElement::SetHold (s.m_lLineID, bHold);
	}
// 1.20 std // #endif 
}
