// SelectShiftCodeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SelectShiftCodeDlg.h"
#include "ItiLibrary.h"
#include "resource.h"
#include "Extern.h"

using namespace ListGlobals;

using namespace ItiLibrary;
using namespace ItiLibrary::ListCtrlImp;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CSelectShiftCodeDlg::CShift::CShift ()
{
}

CSelectShiftCodeDlg::CShift::CShift (const FoxjetDatabase::SHIFTSTRUCT & rhs)
{
	m_lID		= rhs.m_lID;
	m_lLineID	= rhs.m_lLineID;
	m_strName	= rhs.m_strName;
	m_dtTime	= rhs.m_dtTime;
	m_strCode	= rhs.m_strCode;
}

CSelectShiftCodeDlg::CShift::CShift (const CShift & rhs)
{
	m_lID		= rhs.m_lID;
	m_lLineID	= rhs.m_lLineID;
	m_strName	= rhs.m_strName;
	m_dtTime	= rhs.m_dtTime;
	m_strCode	= rhs.m_strCode;
}

CSelectShiftCodeDlg::CShift & CSelectShiftCodeDlg::CShift::operator = (const CSelectShiftCodeDlg::CShift & rhs)
{
	if (this != &rhs) {
		m_lID		= rhs.m_lID;
		m_lLineID	= rhs.m_lLineID;
		m_strName	= rhs.m_strName;
		m_dtTime	= rhs.m_dtTime;
		m_strCode	= rhs.m_strCode;
	}

	return * this;
}

CSelectShiftCodeDlg::CShift::~CShift ()
{
}

CString CSelectShiftCodeDlg::CShift::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0:
		str = m_strName;
		break;
	case 1:
		str = m_dtTime.Format (_T ("%H:%M:%S"));
		break;
	case 2:
		str = m_strCode;
		break;
	}

	return str;
}


/////////////////////////////////////////////////////////////////////////////
// CSelectShiftCodeDlg dialog

CSelectShiftCodeDlg::CSelectShiftCodeDlg(FoxjetDatabase::COdbcDatabase & database, 
										 const FoxjetCommon::CHead & head,
										 CWnd * pParent)
:	m_pDatabase (&database),
	m_head (head),
	FoxjetCommon::CEliteDlg(IDD_SELECTSHIFT, pParent)
{
}

CSelectShiftCodeDlg::CSelectShiftCodeDlg(FoxjetDatabase::COdbcDatabase & database, 
										 const FoxjetCommon::CHead & head,
										 UINT nIDTemplate, CWnd* pParent)
:	m_pDatabase (&database),
	m_head (head),
	FoxjetCommon::CEliteDlg(nIDTemplate, pParent)
{
}


void CSelectShiftCodeDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectShiftCodeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectShiftCodeDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CSelectShiftCodeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


void CSelectShiftCodeDlg::OnOK ()
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize () == 1) {
		m_sel = sel;
		FoxjetCommon::CEliteDlg::OnOK ();
	}
	else if (sel.GetSize () == 0)
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
	else if (sel.GetSize () > 1)
		MsgBox (* this, LoadString (IDS_NOMULTIPLESELECTION));
}


BOOL CSelectShiftCodeDlg::OnInitDialog() 
{
	CArray <SHIFTSTRUCT, SHIFTSTRUCT> v;
	CArray <CColumn, CColumn> vCols;
	PANELSTRUCT panel;

	vCols.Add (CColumn (LoadString (IDS_NAME), 80));
	vCols.Add (CColumn (LoadString (IDS_TIME), 80));
	vCols.Add (CColumn (LoadString (IDS_CODE), 80));

	m_lv.Create (LV_CODES, vCols, this, defElements.m_strRegSection);

	VERIFY (GetPanelRecord (* m_pDatabase, m_head.m_dbHead.m_lPanelID, panel));
	VERIFY (GetShiftRecords (* m_pDatabase, panel.m_lID, v));

	for (int i = 0; i < v.GetSize (); i++) {
		CShift * p = new CShift (v [i]);
		m_lv.InsertCtrlData (p);
	}
	
	return FoxjetCommon::CEliteDlg::OnInitDialog();
}
