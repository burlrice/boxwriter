// BarcodeParamDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "BarcodeParamDlg.h"
#include "BarcodeDlg.h"
#include "BarcodeParams.h"
#include "Extern.h"
#include "Database.h"
#include "SubElement.h"
#include "Debug.h"
#include "TemplExt.h"

using namespace FoxjetElements;
using namespace FoxjetElements::TextElement;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ListGlobals;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


struct
{
	UINT	m_nCtrlID;
	int		m_nAlign;
}
static const map [] =
{
	{ RDO_OFF,				kCapAlignOff,			},
	{ RDO_BELOWLEFT,		kCapAlignBelowLeft,		},
	{ RDO_BELOWCENTER,		kCapAlignBelowCenter,	},
	{ RDO_BELOWRIGHT,		kCapAlignBelowRight,	},
	{ RDO_ABOVELEFT,		kCapAlignAboveLeft,		},
	{ RDO_ABOVECENTER,		kCapAlignAboveCenter,	},
	{ RDO_ABOVERIGHT,		kCapAlignAboveRight,	}, 
};

/////////////////////////////////////////////////////////////////////////////
// CBarcodeParamDlg dialog


CBarcodeParamDlg::CBarcodeParamDlg(FoxjetDatabase::HEADTYPE type, CWnd* pParent /*=NULL*/)
:	m_lID (-1),
	m_bStandardParam (false),
	m_nAlignment (0),
	m_bBold (FALSE),
	m_bItalic (FALSE),
	m_nFontSize (10),
	m_strFont (_T ("MK BARCODE")),
	m_type (type),
	m_nFontWidth (0),
	FoxjetCommon::CEliteDlg(IDD_BARCODEPARAMS, pParent)
{

	//{{AFX_DATA_INIT(CBarcodeParamDlg)
	//}}AFX_DATA_INIT

	VERIFY (m_bmpCaption [0].LoadBitmap (IDB_CAPTIONOFF));
	VERIFY (m_bmpCaption [1].LoadBitmap (IDB_CAPTIONBELOWLEFT));
	VERIFY (m_bmpCaption [2].LoadBitmap (IDB_CAPTIONBELOWCENTER));
	VERIFY (m_bmpCaption [3].LoadBitmap (IDB_CAPTIONBELOWRIGHT));
	VERIFY (m_bmpCaption [4].LoadBitmap (IDB_CAPTIONABOVELEFT));
	VERIFY (m_bmpCaption [5].LoadBitmap (IDB_CAPTIONABOVECENTER));
	VERIFY (m_bmpCaption [6].LoadBitmap (IDB_CAPTIONABOVERIGHT));

	VERIFY (m_bmpOrientation [0].LoadBitmap (IDB_NORMAL));
	VERIFY (m_bmpOrientation [1].LoadBitmap (IDB_VERTICAL));
}

void CBarcodeParamDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBarcodeParamDlg)
	DDX_Check(pDX, CHK_BOLD, m_bBold);
	DDX_Check(pDX, CHK_ITALIC, m_bItalic);
	DDX_Text(pDX, TXT_SIZE, m_nFontSize);
	//}}AFX_DATA_MAP

	for (int i = 0; i < (sizeof (map) / sizeof (map [0])); i++) {
		CButton * p = (CButton *)GetDlgItem (map [i].m_nCtrlID);
		
		ASSERT (p);
		
		if (!pDX->m_bSaveAndValidate)
			p->SetCheck (map [i].m_nAlign == m_nAlignment);
		else {
			if (p->GetCheck () == 1) {
				m_nAlignment = map [i].m_nAlign;
				break;
			}
		}	
	}

	if (GetDlgItem (RDO_NORMAL))
		DDX_Radio (pDX, RDO_NORMAL, (int &)m_orient);

	DDX_Text (pDX, TXT_BARWIDTH, m_nBarWidth);	
	
	DDX_Text (pDX, TXT_BARHEIGHT, m_nBarHeight);
	if (m_orient == NORMAL)
		DDV_MinMaxInt (pDX, m_nBarHeight, CBarcodeElement::m_lmtBarHeight.m_dwMin, CBarcodeElement::m_lmtBarHeight.m_dwMax);
	else
		DDV_MinMaxInt (pDX, m_nBarHeight, CBarcodeElement::m_lmtBarHeightVertical.m_dwMin, CBarcodeElement::m_lmtBarHeightVertical.m_dwMax);
	
	DDX_Text (pDX, TXT_RATIO, m_nRatio);	
	DDX_Check (pDX, CHK_CHECKSUM, m_nCheckSum);

	DDX_Text (pDX, TXT_MAG, m_nMagPct);
	DDV_MinMaxInt (pDX, m_nMagPct, CBarcodeElement::m_lmtMag.m_dwMin, CBarcodeElement::m_lmtMag.m_dwMax);

	DDX_Text (pDX, TXT_HORZBEARER, m_nHorzBearer);
	DDV_MinMaxInt (pDX, m_nHorzBearer, CBarcodeParams::m_lmtBearer.m_dwMin, CBarcodeParams::m_lmtBearer.m_dwMax);

	DDX_Text (pDX, TXT_VERTBEARER, m_nVertBearer);
	DDV_MinMaxInt (pDX, m_nVertBearer, CBarcodeParams::m_lmtBearer.m_dwMin, CBarcodeParams::m_lmtBearer.m_dwMax);

	DDX_Text (pDX, TXT_QUIETZONE, m_nQuietZone);
	DDV_MinMaxInt (pDX, m_nQuietZone, CBarcodeParams::m_lmtQuietZone.m_dwMin, CBarcodeParams::m_lmtQuietZone.m_dwMax);

	DDX_Text (pDX, TXT_FONTWIDTH, m_nFontWidth);

	// all other validation happens in OnOK
}


BEGIN_MESSAGE_MAP(CBarcodeParamDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CBarcodeParamDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (RDO_ABOVELEFT, InvalidatePreview)
	ON_BN_CLICKED (RDO_ABOVECENTER, InvalidatePreview)
	ON_BN_CLICKED (RDO_ABOVERIGHT, InvalidatePreview)
	ON_BN_CLICKED (RDO_BELOWLEFT, InvalidatePreview)
	ON_BN_CLICKED (RDO_BELOWCENTER, InvalidatePreview)
	ON_BN_CLICKED (RDO_BELOWRIGHT, InvalidatePreview)
	ON_BN_CLICKED (RDO_OFF, InvalidatePreview)

	ON_EN_CHANGE (TXT_BARHEIGHT, UpdateBarHeight)
	ON_EN_CHANGE (TXT_SIZE, UpdateBarHeight)	
	ON_EN_CHANGE (TXT_HORZBEARER, UpdateBarHeight)	
	ON_BN_CLICKED (RDO_NORMAL, OnOrientationClick)
	ON_BN_CLICKED (RDO_VERTICAL, OnOrientationClick)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBarcodeParamDlg message handlers

BOOL CBarcodeParamDlg::OnInitDialog() 
{
	CStatic * pWidth = (CStatic *)GetDlgItem (LBL_WIDTH);
	CComboBox * pFont = (CComboBox *)GetDlgItem (CB_FONT);
	const bool bUPC = (m_nSymbology == kSymbologyUPCA) || (m_nSymbology == kSymbologyUPCE);
	const bool bI2o5 = m_nSymbology == kSymbologyInter2of5 || m_nSymbology == kSymbologyGTIN14;
	const UINT nCustomIDs [] = 
	{
		TXT_MAG,
		TXT_BARWIDTH,
		TXT_BARHEIGHT,
		TXT_RATIO,
		RDO_NORMAL,
		RDO_VERTICAL,
	};
	struct 
	{
		UINT m_nID;
		bool m_bEnabled;
	} const symbologyMap [] = 
	{
		{ TXT_HORZBEARER,	bI2o5		},
		{ TXT_VERTBEARER,	bI2o5		},
		{ TXT_QUIETZONE,	bI2o5		},
		{ TXT_FONTWIDTH,	bI2o5		},

		{ TXT_RATIO,		!bUPC		},
		{ TXT_BARHEIGHT,	!bUPC		},
		{ TXT_BARHEIGHT,	!bUPC		},
		{ CHK_CHECKSUM,		!bUPC		},
		{ TXT_SIZE,			!bUPC		},
		{ RDO_ABOVELEFT,	!bUPC		},
		{ RDO_ABOVECENTER,	!bUPC		},
		{ RDO_ABOVERIGHT,	!bUPC		},
		{ RDO_BELOWLEFT,	!bUPC		},
		{ RDO_BELOWCENTER,	!bUPC		},
		{ RDO_BELOWRIGHT,	!bUPC		},
		{ RDO_OFF,			!bUPC		},
	};
	CStringArray vFonts;
	
	CHead::GetWinFontNames (vFonts);

	ASSERT (pWidth);
	ASSERT (pFont);

	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();
	
	SetDlgItemText (TXT_SYMBOLOGY, GetSymbology (m_nSymbology, GetDB ()));
	pWidth->SetWindowText (LoadString (CBarcodeElement::WidthIsPercentage (m_nSymbology) ? IDS_WIDTHPCT : IDS_WIDTHMILS));
	
	for (int i = 0; i < (sizeof (nCustomIDs) / sizeof (nCustomIDs [0])); i++) 
		if (CWnd * p = GetDlgItem (nCustomIDs [i]))
			p->EnableWindow (m_bStandardParam ? TRUE : FALSE);

	for (int i = 0; i < vFonts.GetSize (); i++) {
		CString str = vFonts [i];
		int nIndex = pFont->AddString (str);

		if (!str.CompareNoCase (m_strFont))
			pFont->SetCurSel (nIndex);
	}

	for (int i = 0; i < (sizeof (symbologyMap) / sizeof (symbologyMap [0])); i++) {
		CWnd * p = GetDlgItem (symbologyMap [i].m_nID);

		ASSERT (p);

		//p->IsWindowEnabled ()
		if (!symbologyMap [i].m_bEnabled)
			p->EnableWindow (FALSE);
	}

	InvalidatePreview ();
	UpdateBarHeight ();
	OnOrientationClick ();

	return bResult;
}

void CBarcodeParamDlg::OnOK()
{
	CComboBox * pFont = (CComboBox *)GetDlgItem (CB_FONT);

	ASSERT (pFont);

	int nIndex = pFont->GetCurSel ();
	ASSERT (nIndex != CB_ERR);

	if (pFont->GetLBTextLen (nIndex) > 0)
		pFont->GetLBText (nIndex, m_strFont);

	if (UpdateData (TRUE)) {
		try {
			CBarcodeParams params (m_nSymbology, m_nBarWidth, m_nBarHeight, m_nRatio, m_nCheckSum, m_nMagPct);
			CBarcodeElement barcode ((const CBarcodeElement &)defElements.GetElement (BARCODE));
			HEADSTRUCT head;
			CSubElement & sub = * new CSubElement (TEXT);
			CDC dcMem;

			GetFirstHeadRecord (head);
			head.m_nChannels = GetHeadChannels (m_type);

			dcMem.CreateCompatibleDC (GetDC ());

			barcode.SetSymbology (m_nSymbology);
			barcode.DeleteAllSubElements ();

			CString strData = FoxjetElements::GetDefaultData (m_nSymbology, params);
			TRACEF (FoxjetElements::GetSymbology (m_nSymbology, ListGlobals::GetDB ()) + _T (": ") + strData);
			sub.SetData (strData);
			barcode.InsertSubElement (0, sub);

			sub.m_strDataMask = CString (CSubElement::m_cAnyOpt, strData.GetLength ());
			TRACEF (sub.m_strDataMask);

			params.SetOrientation (m_orient);
			params.SetHorzBearer (m_nHorzBearer);
			params.SetVertBearer (m_nVertBearer);
			params.SetQuietZone (m_nQuietZone);

			barcode.ClipTo (head);
			barcode.Build ();
			CSize size1000ths = barcode.DrawBarcode (dcMem, head, CSize (), FoxjetCommon::EDITOR, true, &params);
			CSize size = CBaseElement::ThousandthsToLogical (size1000ths, head);

			if (m_orient == NORMAL) {
				if (size.cy > (int)head.GetChannels () || m_nBarHeight > (int)head.GetChannels ()) {
					MsgBox (* this, LoadString (IDS_SYMBOLTOOTALL));
					return;
				}
			}
			
			FoxjetCommon::CEliteDlg::OnOK ();
		}
		catch (CBarcodeException * e) { 
			MsgBox (* this, e->GetErrorMessage ());
			HANDLEEXCEPTION_TRACEONLY (e);
			return;
		}
	}
}

void CBarcodeParamDlg::ResizePreview (CDialog * pDlg, int nPreview, int nLeft, int nBottom)
{
	if (CStatic * pPreview = (CStatic *)pDlg->GetDlgItem(nPreview)) {
		CRect rc (0, 0, 0, 0);

		pPreview->GetWindowRect (&rc);

		if (CWnd * p = pDlg->GetDlgItem (nLeft)) {
			CRect rcTmp (0, 0, 0, 0);

			p->GetWindowRect (&rcTmp);
			rc.right = rcTmp.right;
		}
		if (CWnd * p = pDlg->GetDlgItem (nBottom)) {
			CRect rcTmp (0, 0, 0, 0);

			p->GetWindowRect (&rcTmp);
			rc.bottom = rcTmp.top - 2;
		}

		pPreview->SetWindowPos (NULL, rc.left, rc.top, rc.Width (), rc.Height (), SWP_NOMOVE | SWP_NOZORDER);
		pPreview->Invalidate ();
		pPreview->RedrawWindow ();
	}
}

void CBarcodeParamDlg::InvalidatePreview ()
{
	CStatic * pPreview = (CStatic *)GetDlgItem (IDC_PREVIEW);

	ASSERT (pPreview);

	UpdateBarHeight ();

	for (int i = 0; i < (sizeof (map) / sizeof (map [0])); i++) {
		CButton * p = (CButton *)GetDlgItem (map [i].m_nCtrlID);

		ASSERT (p);

		if (p->GetCheck () == 1) {
			pPreview->SetBitmap (m_bmpCaption [i]);
			CBarcodeParamDlg::ResizePreview (this, IDC_PREVIEW, RDO_ABOVERIGHT, RDO_BELOWLEFT);
			break;
		}
	}
}

void CBarcodeParamDlg::UpdateBarHeight ()
{
	CButton * pOff = (CButton *)GetDlgItem (RDO_OFF);

	ASSERT (pOff);

	if (pOff->GetCheck () == 1) 
		SetDlgItemInt (TXT_INNERBARHEIGHT, GetDlgItemInt (TXT_BARHEIGHT));
	else {
		const HEADSTRUCT head = GetDefaultHead ();
	
		int nTotal	= GetDlgItemInt (TXT_BARHEIGHT);
		int nText	= GetDlgItemInt (TXT_SIZE);
		int nBearer = GetDlgItemInt (TXT_HORZBEARER);

		// TODO: no good way to do this without a specific head
		nBearer = CBaseElement::ThousandthsToLogical (CPoint (0, nBearer), head).y;

		SetDlgItemInt (TXT_INNERBARHEIGHT, nTotal - (nBearer + nText));
	}
}

void CBarcodeParamDlg::OnOrientationClick()
{
	CButton * pRadio [] = 
	{
		(CButton *)GetDlgItem (RDO_NORMAL),
		(CButton *)GetDlgItem (RDO_VERTICAL),
	};

	for (int i = 0; i < 2; i++) {
		if (pRadio [i]) {
			if (pRadio [i]->GetCheck () == 1) {
				CStatic * pBmp = (CStatic *)GetDlgItem (BMP_TEXT);
				ASSERT (pBmp);
				
				pBmp->SetBitmap (m_bmpOrientation [i]);
			}
		}
	}
}
