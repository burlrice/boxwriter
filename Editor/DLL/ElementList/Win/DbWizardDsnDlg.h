#pragma once

#include "DbWizard.h"

// CDbWizardDsnDlg

namespace FoxjetElements
{
	class ELEMENT_API CDbWizardDsnDlg : public CDbWizardBasePage
	{
		DECLARE_DYNAMIC(CDbWizardDsnDlg)

	public:
		CDbWizardDsnDlg(CDbWizardDlg * pParent);
		virtual ~CDbWizardDsnDlg();

		CString m_strDSN;

	protected:
		DECLARE_MESSAGE_MAP()
	public:
		CString GetCurSelDsn () const;

		virtual BOOL OnInitDialog();
		virtual BOOL OnSetActive();

		afx_msg void OnDsnChange ();
		afx_msg void OnSetup ();
	};
};

