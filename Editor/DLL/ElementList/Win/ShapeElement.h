// BitmapElement.h: interface for the CShapeElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(__SHAPEELEMENT_H__)
#define __SHAPEELEMENT_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxwin.h>

#include "BaseElement.h"
#include "Types.h"
#include "ElementApi.h"
#include "ximage.h"
#include "RawBitmap.h"

namespace FoxjetElements
{
	namespace ShapeElement
	{
		typedef enum 
		{ 
			SHAPETYPE_HORZ = 0, 
			SHAPETYPE_VERT, 
			SHAPETYPE_RECT, 
			SHAPETYPE_FIRST = SHAPETYPE_HORZ,
			SHAPETYPE_LAST	= SHAPETYPE_RECT, 
		} SHAPETYPE;

		class ELEMENT_API CShapeElement : public FoxjetCommon::CBaseElement  
		{
			DECLARE_DYNAMIC (CShapeElement);

		public:

			CShapeElement (const FoxjetDatabase::HEADSTRUCT & head);
			CShapeElement (const CShapeElement & rhs);
			CShapeElement & operator = (const CShapeElement & rhs);
			virtual ~CShapeElement();

			virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
			virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);
			virtual CSize Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly = false, FoxjetCommon::DRAWCONTEXT context = EDITOR);
			virtual CString GetImageData () const;
			virtual CString GetDefaultData () const;
			virtual int GetClassID () const { return SHAPE; }
			virtual bool IsResizable () const { return true; }

			virtual int Build (BUILDTYPE type = INITIAL, bool bCanThrowException = false);
			// throws CElementException

			virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

			virtual bool SetSize (const CSize & size, const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI = true);
			virtual CSize GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI = true) const;
			virtual void ClipTo (const FoxjetDatabase::HEADSTRUCT & head);

			virtual bool HasDither () const { return false; }

			CSize GetSize () const { return m_size; }
			bool SetSize (const CSize & size);

			CSize GetThickness () const { return m_thickness; }
			bool SetThickness (const CSize & thickness);

			SHAPETYPE GetType () const { return m_type; }
			bool SetType (SHAPETYPE type);

		protected:
			bool Copy (const CShapeElement & rhs);
			void Free ();

			CSize m_size;
			CSize m_thickness;
			SHAPETYPE m_type;
			FoxjetCommon::CRawBitmap * m_pbmpPrinter;
			FoxjetCommon::CRawBitmap * m_pbmpEditor;
		};
	}; //namespace ShapeElement
}; // namespace FoxjetElements


#endif // !defined(__SHAPEELEMENT_H__)
