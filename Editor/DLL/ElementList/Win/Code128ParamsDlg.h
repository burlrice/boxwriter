#if !defined(AFX_CODE128PARAMSDLG_H__4EEA5D8F_4292_4E97_A72C_F806F0D49157__INCLUDED_)
#define AFX_CODE128PARAMSDLG_H__4EEA5D8F_4292_4E97_A72C_F806F0D49157__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Code128ParamsDlg.h : header file
//

#include "UpcBarcodeParamsDlg.h"
#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////
// CCode128ParamsDlg dialog

class CCode128ParamsDlg : public CUpcBarcodeParamsDlg
{
// Construction
public:
	CCode128ParamsDlg(FoxjetDatabase::HEADTYPE type, CWnd* pParent = NULL);   // standard constructor
	CCode128ParamsDlg(UINT nResID, FoxjetDatabase::HEADTYPE type, CWnd* pParent = NULL); 

// Dialog Data
	//{{AFX_DATA(CCode128ParamsDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	int	m_nAlignment;
	BOOL m_bChecksum;
	BOOL m_bOptimizeFNC1;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCode128ParamsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	void Init ();

	CBitmap m_bmpCaption [7];

	afx_msg void InvalidatePreview ();

	// Generated message map functions
	//{{AFX_MSG(CCode128ParamsDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CODE128PARAMSDLG_H__4EEA5D8F_4292_4E97_A72C_F806F0D49157__INCLUDED_)
