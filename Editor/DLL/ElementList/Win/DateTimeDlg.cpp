// DateTimeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DateTimeElement.h"
#include "DateTimeDlg.h"
#include "DateTimeFmtDlg.h"
#include "ItiLibrary.h"
#include "Extern.h"
#include "Debug.h"
#include "Parse.h"

using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace ItiLibrary;
using namespace FoxjetElements;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDateTimeDlg dialog

CDateTimeDlg::CDateTimeDlg(const CBaseElement & e, const CSize & pa, 
						   const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_bRollover (FALSE),
	CBaseTextDlg(e, pa, IDD_DATETIME, pList, pParent)
{
}

CDateTimeDlg::CDateTimeDlg(const CBaseElement & e, const CSize & pa, 
						   UINT nID, const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_bRollover (FALSE),
	CBaseTextDlg(e, pa, nID, pList, pParent)
{
}


void CDateTimeDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseTextDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDateTimeDlg)
	//}}AFX_DATA_MAP

	DDX_Text(pDX, CB_DATETIMEFORMAT, m_strFmt);
	DDV_MinMaxChars (pDX, m_strFmt, CB_DATETIMEFORMAT, CDateTimeElement::m_lmtString);

	DDX_Check (pDX, CHK_ROLLOVER, m_bRollover);
}


BEGIN_MESSAGE_MAP(CDateTimeDlg, CBaseTextDlg)
	//{{AFX_MSG_MAP(CDateTimeDlg)
	ON_BN_CLICKED(BN_BUILD, OnBuild)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDateTimeDlg message handlers

void CDateTimeDlg::OnBuild() 
{
	ASSERT (GetLineID () != -1);

	CDateTimeFmtDlg dlg (GetLineID (), this);

	GetDlgItemText (CB_DATETIMEFORMAT, dlg.m_strFmt);

	if (dlg.DoModal () == IDOK) 
		SetDlgItemText (CB_DATETIMEFORMAT, dlg.m_strFmt);
}

BOOL CDateTimeDlg::OnInitDialog() 
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_DATETIMEFORMAT);
	CStringArray v;

	ASSERT (pCB);

	BOOL bResult = CBaseTextDlg::OnInitDialog();
	LoadCustomDateTimeFmts (defElements.m_strRegSection, _T ("Custom date/time formats"), v);
	
	for (int i = 0; i < v.GetSize (); i++)
		pCB->AddString (v [i]);

	OnOrientationClick ();

	pCB->SetFont (&m_fntCtrl);

	if (CBaseTextElement * p = DYNAMIC_DOWNCAST (CBaseTextElement, &GetElement ())) {
		if (CDC * pDC = GetDC ()) {
			CFont * pFont = pDC->SelectObject (&m_fntCtrl);
			TEXTMETRIC m;

			pDC->GetTextMetrics (&m);
			
			//TRACEF (p->GetFontName () + ": " + FoxjetDatabase::ToString (m.tmDescent) + ", " + FoxjetDatabase::ToString (m.tmAscent));
			if (m.tmDescent <= 0)
				pCB->SetItemHeight (0, pCB->GetItemHeight (0) + 4);

			pDC->SelectObject (pFont);
		}
	}

	return bResult;
}

int CDateTimeDlg::BuildElement ()
{
	CDateTimeElement & e = GetElement ();
	CString str;
	CSize size;

	GetDlgItemText (CB_DATETIMEFORMAT, str);
	e.SetDefaultData (str);
	e.SetOrientation (GetOrientation ());
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));

	if (CButton * p = (CButton *)GetDlgItem (CHK_ROLLOVER))
		e.SetRollover (p->GetCheck () == 1);

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

CDateTimeElement & CDateTimeDlg::GetElement ()
{
	CBaseElement & e = CBaseTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CDateTimeElement)));
	return (CDateTimeElement &)e;
}

const CDateTimeElement & CDateTimeDlg::GetElement () const
{
	const CBaseElement & e = CBaseTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CDateTimeElement)));
	return (CDateTimeElement &)e;
}

void CDateTimeDlg::Get (const FoxjetElements::CDateTimeElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Get (e, pHead);

	m_nID				= e.GetID ();
	m_pt				= e.GetPos (pHead);
	m_strFmt			= e.GetDefaultData ();
	m_nOrientation		= e.GetOrientation ();
	m_nWidth			= e.GetWidth ();
	m_bRollover			= e.GetRollover ();
}

void CDateTimeDlg::Set (FoxjetElements::CDateTimeElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Set (e, pHead);

	e.SetID (m_nID);
	e.SetDefaultData(m_strFmt);
	e.SetAlignment (m_align);
	e.SetPos (m_pt = e.GetPosInternal (m_pt), pHead, ALIGNMENT_ADJUST_SUBTRACT);
	e.SetOrientation (m_nOrientation);
	e.SetWidth (m_nWidth);
	e.SetRollover (m_bRollover ? true : false);
	e.SetFontName (m_strFont);
}
