// DateTimeElement.cpp: implementation of the CDateTimeElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <odbcinst.h>
#include <vector>
#include <string>
#include "DateTimeElement.h"
#include "List.h"
#include "ElementDefaults.h"
#include "Edit.h"
#include "Debug.h"
#include "Extern.h"
#include "Utils.h"
#include "TemplExt.h"
#include "CodesDlg.h"
#include "Extern.h"
#include "AppVer.h"
#include "RolloverDlg.h"
#include "Parse.h"
#include "OdbcDatabase.h"
#include "OdbcTable.h"
#include "OdbcRecordset.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetElements;
using namespace FoxjetCommon::ElementFields;

typedef struct _HOLDSTRUCT
{
	_HOLDSTRUCT () 
	:	m_tm (CTime::GetCurrentTime ()),
		m_lLineID (-1),
		m_bHold (false)
	{
	}

	ULONG m_lLineID;
	CTime m_tm;
	bool m_bHold;
} HOLDSTRUCT;

static CMap <ULONG, ULONG, HOLDSTRUCT, HOLDSTRUCT> mapHold;
static DIAGNOSTIC_CRITICAL_SECTION (csMap);

static const TCHAR cSpecifier = '%';
static const TCHAR cLeftJustify = '-';

static void InsertByLength (const CString & str, CStringArray & v); 
static int FindExact (const CString & strFind, const CStringArray & v); 

extern CDiagnosticCriticalSection csDays;
extern CodesDlg::CCodesArray vMonthcodes;
extern CodesDlg::CCodesArray vHourcodes;
extern CodesDlg::CCodesArray vDayOfWeekCodes;
extern CodesDlg::CCodesArray vRollover;
extern CodesDlg::CCodesArray vQuarterHourcodes;

////////////////////////////////////////////////////////////////////////////////	

static int _GetRolloverMinutes (ULONG lLineID)
{
	LOCK (::csDays);

	int nRollover = 0;

	for (int i = 0; i < ::vRollover.GetSize (); i++) {
		CodesDlg::CCodes c = ::vRollover [i];

		if (c.m_lLineID == lLineID || lLineID == -1) {
			ASSERT (c.m_vCodes.GetSize () >= 1);

			nRollover = _ttoi (c.m_vCodes [0]);
			break;
		}
	}

	return nRollover;
}

////////////////////////////////////////////////////////////////////////////////	

void ELEMENT_API FoxjetElements::LoadMonthCodes (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	LOCK (::csDays);

	static const LPCTSTR lpsz [] = 
	{
		_T ("Jan"),
		_T ("Feb"),
		_T ("Mar"),
		_T ("Apr"),
		_T ("May"),
		_T ("Jun"),
		_T ("Jul"),
		_T ("Aug"),
		_T ("Sep"),
		_T ("Oct"),
		_T ("Nov"),
		_T ("Dec"),
	};
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CString strDef;

	GetPrinterLines (db, GetPrinterID (db), vLines); //GetLineRecords (db, vLines);
	::vMonthcodes.RemoveAll ();

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		SETTINGSSTRUCT s;
		CodesDlg::CCodes c;
		ULONG lLineID = vLines [nLine].m_lID;

		lLineID = FoxjetDatabase::GetMasterLineID (lLineID);

		#if __CUSTOM__ == __SW0877__
		lLineID = 0;
		#endif

		GetSettingsRecord (db, lLineID, ListGlobals::Keys::m_strMonths, s);

		c.m_lLineID = lLineID;
		Tokenize (s.m_strData, c.m_vCodes);

		// remove anything extra
		while (c.m_vCodes.GetSize () > ARRAYSIZE (lpsz))
			c.m_vCodes.RemoveAt (c.m_vCodes.GetSize () - 1);

		// default anything missing
		for (int i = c.m_vCodes.GetSize (); i < ARRAYSIZE (lpsz); i++)
			c.m_vCodes.Add (lpsz [i]);

		for (int i = 0; i < c.m_vCodes.GetSize (); i++)
			c.m_vCodes [i] = UnformatString (c.m_vCodes [i]);

		::vMonthcodes.Add (c);
	}
}

void ELEMENT_API FoxjetElements::LoadDayCodes (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	LOCK (::csDays);

	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CString strDef;
	CStringArray vDefs;
	UINT nDefIDs [7] =
	{
		IDS_SUNDAY,
		IDS_MONDAY,
		IDS_TUESDAY,
		IDS_WEDNESDAY,
		IDS_THURSDAY,
		IDS_FRIDAY,
		IDS_SATURDAY,
	};

	GetPrinterLines (db, GetPrinterID (db), vLines); //GetLineRecords (db, vLines);
	::vDayOfWeekCodes.RemoveAll ();

	for (int i = 0; i < ARRAYSIZE (nDefIDs); i++) 
		vDefs.Add (LoadString (nDefIDs [i]));

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		SETTINGSSTRUCT s;
		CodesDlg::CCodes c;
		ULONG lLineID = vLines [nLine].m_lID;

		lLineID = FoxjetDatabase::GetMasterLineID (lLineID);

		#if __CUSTOM__ == __SW0877__
		lLineID = 0;
		#endif

		GetSettingsRecord (db, lLineID, ListGlobals::Keys::m_strDayOfWeek, s);

		c.m_lLineID = lLineID;
		Tokenize (s.m_strData, c.m_vCodes);

		// remove anything extra
		while (c.m_vCodes.GetSize () > vDefs.GetSize ())
			c.m_vCodes.RemoveAt (c.m_vCodes.GetSize () - 1);

		// default anything missing
		for (int i = c.m_vCodes.GetSize (); i < vDefs.GetSize (); i++)
			c.m_vCodes.Add (vDefs [i]);

		for (int i = 0; i < c.m_vCodes.GetSize (); i++)
			c.m_vCodes [i] = UnformatString (c.m_vCodes [i]);

		::vDayOfWeekCodes.Add (c);
	}
}

void ELEMENT_API FoxjetElements::LoadHourCodes (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	LOCK (::csDays);

	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CString strDef;
	CStringArray vDefs;

	GetPrinterLines (db, GetPrinterID (db), vLines); //GetLineRecords (db, vLines);
	::vHourcodes.RemoveAll ();

	for (int i = 0; i < 24; i++) {
		CString str;

		str.Format (_T ("%d"), i + 1);
		vDefs.Add (str);
	}

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		SETTINGSSTRUCT s;
		CodesDlg::CCodes c;
		ULONG lLineID = vLines [nLine].m_lID;

		lLineID = FoxjetDatabase::GetMasterLineID (lLineID);

		#if __CUSTOM__ == __SW0877__
		lLineID = 0;
		#endif

		GetSettingsRecord (db, lLineID, ListGlobals::Keys::m_strHours, s);

		c.m_lLineID = lLineID;
		Tokenize (s.m_strData, c.m_vCodes);

		// remove anything extra
		while (c.m_vCodes.GetSize () > vDefs.GetSize ())
			c.m_vCodes.RemoveAt (c.m_vCodes.GetSize () - 1);

		// default anything missing
		for (int i = c.m_vCodes.GetSize (); i < vDefs.GetSize (); i++)
			c.m_vCodes.Add (vDefs [i]);

		for (int i = 0; i < c.m_vCodes.GetSize (); i++)
			c.m_vCodes [i] = UnformatString (c.m_vCodes [i]);

		::vHourcodes.Add (c);
	}
}

// std // #if __CUSTOM__ == __SW0840__
void ELEMENT_API FoxjetElements::LoadQuarterHourCodes (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	LOCK (::csDays);

	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CString strDef;
	CStringArray vDefs;

	GetPrinterLines (db, GetPrinterID (db), vLines); //GetLineRecords (db, vLines);

	::vQuarterHourcodes.RemoveAll ();

	for (int i = 1; i <= (24 * 4); i++) {
		CString str;

		str.Format (_T ("%02d"), i); //01 - 96
		vDefs.Add (str);
	}

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		SETTINGSSTRUCT s;
		CodesDlg::CCodes c;
		ULONG lLineID = vLines [nLine].m_lID;

		lLineID = FoxjetDatabase::GetMasterLineID (lLineID);

		#if __CUSTOM__ == __SW0877__
		lLineID = 0;
		#endif

		GetSettingsRecord (db, lLineID, ListGlobals::Keys::m_strQuarterHours, s);

		c.m_lLineID = lLineID;
		Tokenize (s.m_strData, c.m_vCodes);

		// remove anything extra
		while (c.m_vCodes.GetSize () > vDefs.GetSize ())
			c.m_vCodes.RemoveAt (c.m_vCodes.GetSize () - 1);

		// default anything missing
		for (int i = c.m_vCodes.GetSize (); i < vDefs.GetSize (); i++)
			c.m_vCodes.Add (vDefs [i]);

		for (int i = 0; i < c.m_vCodes.GetSize (); i++) {
			c.m_vCodes [i] = UnformatString (c.m_vCodes [i]);
//			TRACEF (c.m_vCodes [i]);
		}

		::vQuarterHourcodes.Add (c);
	}
}
// std // #endif //__CUSTOM__ == __SW0840__

void ELEMENT_API FoxjetElements::LoadRolloverSettings (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	LOCK (::csDays);

	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CStringArray vDefs;

	for (int i = 0; i < 2; i++)
		vDefs.Add (_T ("0"));

//	vDefs.SetAt (0, _T ("-180"));
//	vDefs.SetAt (1, _T ("1"));

	GetPrinterLines (db, GetPrinterID (db), vLines); //GetLineRecords (db, vLines);
	::vRollover.RemoveAll ();

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		SETTINGSSTRUCT s;
		CodesDlg::CCodes c;
		ULONG lLineID = vLines [nLine].m_lID;

		lLineID = FoxjetDatabase::GetMasterLineID (lLineID);

		#if __CUSTOM__ == __SW0877__
		lLineID = 0;
		#endif

		GetSettingsRecord (db, lLineID, ListGlobals::Keys::m_strRollover, s);

		c.m_lLineID = lLineID;
		Tokenize (s.m_strData, c.m_vCodes);

		// remove anything extra
		while (c.m_vCodes.GetSize () > vDefs.GetSize ())
			c.m_vCodes.RemoveAt (c.m_vCodes.GetSize () - 1);

		// default anything missing
		for (int i = c.m_vCodes.GetSize (); i < vDefs.GetSize (); i++)
			c.m_vCodes.Add (vDefs [i]);

		for (int i = 0; i < c.m_vCodes.GetSize (); i++)
			c.m_vCodes [i] = UnformatString (c.m_vCodes [i]);

		::vRollover.Add (c);
	}
}

////////////////////////////////////////////////////////////////////////////////	

IMPLEMENT_DYNAMIC (CDateTimeElement, CTextElement);

const FoxjetCommon::LIMITSTRUCT CDateTimeElement::m_lmtString = { 1,	lmtString.m_dwMax };

CDateTimeElement::CDateTimeElement(const FoxjetDatabase::HEADSTRUCT & head)
:	m_bRollover (false),
	m_bHoldStartDate (false),
	CTextElement (head)
{
	m_strDefaultData = _T ("%H:%M:%S");
}

CDateTimeElement::CDateTimeElement (const CDateTimeElement & rhs)
:	m_bRollover (rhs.m_bRollover),
	m_bHoldStartDate (rhs.m_bHoldStartDate),
	CTextElement (rhs)
{
}

CDateTimeElement & CDateTimeElement::operator = (const CDateTimeElement & rhs)
{
	CTextElement::operator = (rhs);

	if (this != &rhs) {
		m_bRollover			= rhs.m_bRollover;
		m_bHoldStartDate	= rhs.m_bHoldStartDate;
	}

	return * this;
}

CDateTimeElement::~CDateTimeElement()
{
}

bool CDateTimeElement::SetDefaultData(const CString &strData)
{
	if (IsValid (strData.GetLength (), m_lmtString)) {
		SetRedraw ();
		m_strDefaultData = strData;
		return true;
	}

	return false;
}

static int FindExact (const CString & strFind, const CStringArray & v) 
{
	for (int i = 0; i < v.GetSize (); i++) {
		CString str = v [i];

		if (!str.Compare (strFind))
			return i;
	}

	return -1;
}

static void InsertByLength (const CString & str, CStringArray & v) 
{
	int nLen = str.GetLength ();

	for (int i = 0; i < v.GetSize (); i++) {
		if (v [i].GetLength () < nLen) {
			v.InsertAt (i, str);
			return;
		}
	}

	v.Add (str);
}

int CDateTimeElement::IsMonthCode (const CString & strInput)
{
	for (int i = 0; i <= 12; i++) {
		CString str;

		str.Format (_T ("%%%dM"), i);

		if (strInput.Find (str) != -1) 
			return i;
	}

	return -1;
}

CString CDateTimeElement::GetMonthCode (ULONG lLineID, int nIndex, const CTime & tm) 
{
	LOCK (::csDays);

	for (int i = 0; i < ::vMonthcodes.GetSize (); i++) {
		CodesDlg::CCodes c = ::vMonthcodes [i];

		if (c.m_lLineID == lLineID || lLineID == -1) {
			if (nIndex == 0) 
				nIndex = tm.GetMonth ();

			if (nIndex >= 1 && nIndex <= c.m_vCodes.GetSize ()) 
				return c.m_vCodes [nIndex - 1];
		}
	}

	return _T ("");
}

// std // #if __CUSTOM__ == __SW0840__

int CDateTimeElement::IsQuarterHourCode (const CString & strInput) 
{
	for (int i = 0; i <= (24 * 4); i++) {
		CString str;

		str.Format (_T ("%%%dQ"), i);

		if (strInput.Find (str) != -1) 
			return i;
	}

	return -1;
}

CString CDateTimeElement::GetQuarterHourCode (ULONG lLineID, int nIndex, const CTime & tm)
{
	LOCK (::csDays);

	for (int i = 0; i < ::vQuarterHourcodes.GetSize (); i++) {
		CodesDlg::CCodes c = ::vQuarterHourcodes [i];

		if (c.m_lLineID == lLineID || lLineID == -1) {
			if (nIndex == 0) {
				int nQuarterHour = (tm.GetHour () * 4) + (int)floor (tm.GetMinute () / 15.0) + 1;

				nIndex = BindTo (nQuarterHour, 1, c.m_vCodes.GetSize ());
			}

			if (nIndex >= 1 && nIndex <= c.m_vCodes.GetSize ()) 
				return c.m_vCodes [nIndex - 1];
		}
	}

	return _T ("");
}

// std // #endif //__CUSTOM__ == __SW0840__

int CDateTimeElement::IsHourCode (const CString & strInput) 
{
	for (int i = 0; i <= 24; i++) {
		CString str;

		str.Format (_T ("%%%dH"), i);

		if (strInput.Find (str) != -1) 
			return i;
	}

	return -1;
}

int CDateTimeElement::IsDayOfWeekCode (const CString & strInput) 
{
	for (int i = 0; i <= 7; i++) {
		CString str;

		str.Format (_T ("%%%dA"), i);

		if (strInput.Find (str) != -1) 
			return i;
	}

	return -1;
}

CString CDateTimeElement::GetDayOfWeekCode (ULONG lLineID, int nIndex, const CTime & tm)
{
	LOCK (::csDays);

	for (int i = 0; i < ::vDayOfWeekCodes.GetSize (); i++) {
		CodesDlg::CCodes c = ::vDayOfWeekCodes [i];

		if (c.m_lLineID == lLineID || lLineID == -1) {
			if (nIndex == 0) 
				nIndex = BindTo (tm.GetDayOfWeek (), 1, c.m_vCodes.GetSize ());

			if (nIndex >= 1 && nIndex <= c.m_vCodes.GetSize ()) 
				return c.m_vCodes [nIndex - 1];
		}
	}

	return _T ("");
}

CString CDateTimeElement::GetHourCode (ULONG lLineID, int nIndex, const CTime & tm) 
{
	LOCK (::csDays);

	for (int i = 0; i < ::vHourcodes.GetSize (); i++) {
		CodesDlg::CCodes c = ::vHourcodes [i];

		if (c.m_lLineID == lLineID || lLineID == -1) {
			if (nIndex == 0) {
				nIndex = BindTo (tm.GetHour (), 0, c.m_vCodes.GetSize ());

				if (nIndex >= 0 && nIndex < c.m_vCodes.GetSize ()) 
					return c.m_vCodes [nIndex];
			}
			else {
				if (nIndex > 0 && nIndex <= c.m_vCodes.GetSize ()) 
					return c.m_vCodes [nIndex - 1];
			}
		}
	}

	return _T ("");
}

void CDateTimeElement::TokenizeFormats (CString strInput, CStringArray & v)
{
	int nIndex = 0;

	for (int i = 0; i <= 12; i++) {
		CString str;

		str.Format (_T ("%%%dM"), i);

		if (strInput.Replace (str, _T (""))) 
			v.Add (str);
	}

	for (int i = 0; i <= 24; i++) {
		CString str;

		str.Format (_T ("%%%dH"), i);

		if (strInput.Replace (str, _T (""))) 
			v.Add (str);
	}

	// std // #if __CUSTOM__ == __SW0840__
	for (int i = 0; i <= (24 * 4); i++) {
		CString str;

		str.Format (_T ("%%%dQ"), i);

		if (strInput.Replace (str, _T (""))) 
			v.Add (str);
	}
	// std // #endif

	for (int i = 0; i <= 7; i++) {
		CString str;

		str.Format (_T ("%%%dA"), i);

		if (strInput.Replace (str, _T (""))) 
			v.Add (str);
	}

	// takes a string like "YY YYYY YYYY YYYYY Y Y" and breaks it down into:
	// "YYYYY", "YYYY", "YY", "Y"
	// the longest strings are always assigned a lower index in 'v'
	// duplicates are removed
	nIndex = 0;

	while ((nIndex = strInput.Find (::cSpecifier, nIndex)) != -1) {
		CString strToken, str = strInput.Mid (nIndex);
		
		nIndex++;

		for (int i = 1; i < str.GetLength (); i++) {
			TCHAR c = str [i];

			if (c == ::cSpecifier)
				continue;
			
			strToken += c;

			if (c == ::cLeftJustify) 
				continue;

			if ((i + 1) < str.GetLength ()) {
				TCHAR cNext = str [i + 1];

				if (c != cNext) {
					strToken.TrimLeft ();
					strToken.TrimRight ();

					if (strToken.GetLength ()) {
						strToken = ::cSpecifier + strToken;

						int nIndex = FindExact (strToken, v);

						if (nIndex == -1) 
							InsertByLength (strToken, v);
					}

					strToken.Empty ();
				}
			}
		}

		strToken.TrimLeft ();
		strToken.TrimRight ();

		if (strToken.GetLength ()) {
			strToken = ::cSpecifier + strToken;
			
			if (FindExact (strToken, v) == -1) 
				InsertByLength (strToken, v);
		}
	}
}

namespace Sunnyvale
{
	const CString strDSN = _T ("Sunnyvale_DateCode");

	#define DO_SQL_CALL_BOOL(f) DoSqlCall ((f) ? true : false, _T (__FILE__), __LINE__)

	bool DoSqlCall (bool bResult, LPCTSTR lpszFile, ULONG lLine)
	{
		if (!bResult) 
			TRACE_SQLERROR ();

		return bResult;
	}

	void CheckForDSN ()
	{
		const CString strDir = GetHomeDir ();
		const CString strFileTitle = _T("Sunnyvale.xls");
		const CString strFilepath = strDir + _T("\\") + strFileTitle;
		const LPCTSTR lpszDBDriver = SQL_XLS_DRIVER;

		if (::GetFileAttributes (strFilepath) == -1) {
			HINSTANCE hModule = GetInstanceHandle (); 
			UINT nID = IDR_SUNNYVALE_XLS;

			if (HRSRC hRSRC = ::FindResource (hModule, MAKEINTRESOURCE (nID), _T ("XLS"))) {
				DWORD dwSize = ::SizeofResource (hModule, hRSRC);

				if (HGLOBAL hGlobal = ::LoadResource (hModule, hRSRC)) {
					if (LPVOID lpData = ::LockResource (hGlobal)) {
						if (FILE * fp = _tfopen (strFilepath, _T ("wb"))) {
							TCHAR sz[512];
							TCHAR szAttr[512];

							VERIFY (fwrite (lpData, dwSize, 1, fp) == 1);
							fclose (fp);

							DO_SQL_CALL_BOOL(::SQLConfigDataSource(NULL, ODBC_REMOVE_SYS_DSN, lpszDBDriver, _T("DSN=") + strDSN));

							LPCTSTR lpszFormat =
								_T("DSN=%s~ ")					// dsn
								_T("DBQ=%s\\%s~ ");				// file

							_stprintf(sz, lpszFormat,
								strDSN,
								strDir, strFileTitle,
								strDir);

							int nLen = _tcsclen(sz);

							_tcscpy(szAttr, (sz));
							TRACEF(szAttr);

							for (int i = 0; i < nLen; i++)
								if (szAttr[i] == '~')
									szAttr[i] = '\0';

							DO_SQL_CALL_BOOL(SQLConfigDataSource(NULL, ODBC_ADD_SYS_DSN, lpszDBDriver, szAttr));
						}
					}
				}
			}
		}
	}

	int GetMinutes (const COleDateTime & tm)
	{
		return (tm.GetHour () * 60) + tm.GetMinute ();
	}
	
	CString GetIndicator (const CTime & tm, LPCTSTR lpszField [3])
	{
		if (!IsSunnyvale ()) 
			return _T ("");

		CheckForDSN ();

		try
		{
			COdbcCache db = COdbcDatabase::Find (strDSN);

			if (db.GetDB ().GetTableCount ()) {
				CString strSQL;
				int nDay = _ttoi (tm.Format (_T ("%d")));
				int nMinutes = GetMinutes (COleDateTime (tm.GetTime ()));

				strSQL.Format(_T("SELECT * FROM [%s] WHERE [Day of month]=%d"), db.GetDB ().GetTable(0)->GetName(), nDay);

				COdbcCache rst = COdbcDatabase::Find (db.GetDB (), strDSN, strSQL);

				rst.GetRst ().MoveFirst ();

				if (!rst.GetRst ().IsEOF ()) {
					COdbcRecordset & r = rst.GetRst ();
					CString strShift [3][2] = 
					{
						{ (CString)r.GetFieldValue (_T ("[Shift 1 begin]")), (CString)r.GetFieldValue (_T ("[Shift 1 end]")), },
						{ (CString)r.GetFieldValue (_T ("[Shift 2 begin]")), (CString)r.GetFieldValue (_T ("[Shift 2 end]")), },
						{ (CString)r.GetFieldValue (_T ("[Shift 3 begin]")), (CString)r.GetFieldValue (_T ("[Shift 3 end]")), },
					};
					int n [3][2] = { 0 };

					for (int i = 0; i < ARRAYSIZE (n); i++) {
						for (int j = 0; j < ARRAYSIZE (n [0]); j++) {
							try
							{
								COleDateTime tm;
								tm.ParseDateTime (strShift [i][j]);
								n [i][j] = GetMinutes (tm);
							}
							catch (COleException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
							catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }
						}
					}

					struct
					{
						bool m_bCondition;
						//LPCTSTR m_lpszField;
					}
					map [] = 
					{
						{ (nMinutes >= n [0][0] && nMinutes <= n [0][1]), /* _T ("[Shift 1 indicator]"), */ },
						{ (nMinutes >= n [1][0] && nMinutes <= n [1][1]), /* _T ("[Shift 2 indicator]"), */ },
//						{ (nMinutes <  n [2][0] || nMinutes >= n [2][1]), /* _T ("[Shift 3 indicator]"), */ },
						{ !map [0].m_bCondition && !map [1].m_bCondition, /* _T ("[Shift 3 indicator]"), */ },
					};

					for (int i = 0; i < ARRAYSIZE (map); i++) 
						if (map [i].m_bCondition)
							return ToString (_ttoi ((CString)r.GetFieldValue (lpszField [i])));

					ASSERT (0);
				}
			}
		}
		catch (COleException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }

		return _T ("");
	}

	int IsDayIndicator (const CString & str) 
	{
		return str.Find (_T ("%D"));
	}

	CString GetDayIndicator (const CTime & tm)
	{
		LPCTSTR lpsz [3] = 
		{
			_T ("[Day 1 indicator]"),
			_T ("[Day 2 indicator]"),
			_T ("[Day 3 indicator]"),
		};

		return GetIndicator (tm, lpsz);
	}

	int IsShiftIndicator (const CString & str)
	{
		return str.Find (_T ("%S"));
	}

	CString GetShiftIndicator (const CTime & tm)
	{
		LPCTSTR lpsz [3] = 
		{
			_T ("[Shift 1 indicator]"),
			_T ("[Shift 2 indicator]"),
			_T ("[Shift 3 indicator]"),
		};

		return GetIndicator (tm, lpsz);
	}

	#ifdef _DEBUG
	std::vector <std::wstring> Test ()
	{
		std::vector <std::wstring> v;
		CTime tm (2016, 5, 18, 0, 0, 0);
		int nStart;
		CString str;

		/*
		for (str = _T ("start"), tm = CTime (2016, 5, 18, 0, 0, 0), nStart = tm.GetDay (); tm.GetDay () <= (nStart + 1); tm += CTimeSpan (0, 0, 1, 0)) {
			CString strNext = Sunnyvale::GetDayIndicator (tm);

			if (str != strNext) {
				TRACEF (tm.Format (_T ("%c: ") + strNext));
				str = strNext;
			}
		}

		for (str = _T ("start"), tm = CTime (2016, 3, 1, 0, 0, 0), nStart = tm.GetMonth (); tm.GetMonth () <= (nStart + 1); tm += CTimeSpan (0, 1, 0, 0)) {
			CString strNext = Sunnyvale::GetShiftIndicator (tm);

			if (str != strNext) {
				TRACEF (tm.Format (_T ("%c: ") + strNext));
				str = strNext;
			}
		}
		*/

		for (str = _T ("start"), tm = CTime (2016, 3, 1, 0, 0, 0), nStart = tm.GetMonth (); tm.GetMonth () < (nStart + 1); tm += CTimeSpan (0, 0, 1, 0)) {
			CString strNext = Sunnyvale::GetDayIndicator (tm) + _T (", ") + Sunnyvale::GetShiftIndicator (tm);

			if (str != strNext) {
				v.push_back (std::wstring (tm.Format (_T ("%c: ") + strNext)));
				TRACEF (tm.Format (_T ("%c: ") + strNext));
				str = strNext;
			}
		}

		return v;
	}

	/*
	{
		std::vector <std::wstring> v = Sunnyvale::Test ();

		for (int i = 0; i < v.size (); i++) {
			CString str;

			str.Format (_T ("[%03d] %s"), i, v [i].c_str ());
			TRACEF (str);
		}

		ASSERT (0);
	}
	*/
	#endif

	void ELEMENT_API Init ()
	{
		try
		{
			CString str = GetShiftIndicator (CTime::GetCurrentTime ());
			TRACEF (str);
		}
		catch (COleException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }
	}
};

CString CDateTimeElement::Format (ULONG lLineID, const CString & strInput, const CTime & tm)
{
	struct
	{
		TCHAR m_c;
		LPCTSTR m_lpsz;
	} static const map [] = 
	{
		'Y',	_T ("%Y"),
		'D',	_T ("%d"),
		'M',	_T ("%m"),
	};
	CString strFormat (strInput);
	bool bLeftJustify = false;
	int nIndex;

	if ((nIndex = IsHourCode (strInput)) != -1)
		return GetHourCode (lLineID, nIndex, tm);
	else if ((nIndex = IsMonthCode (strInput)) != -1)
		return GetMonthCode (lLineID, nIndex, tm);
	else if ((nIndex = IsDayOfWeekCode (strInput)) != -1)
		return GetDayOfWeekCode (lLineID, nIndex, tm);
// std // #if __CUSTOM__ == __SW0840__
	else if ((nIndex = IsQuarterHourCode (strInput)) != -1)
		return GetQuarterHourCode (lLineID, nIndex, tm);
// std // #endif
	else if ((nIndex = Sunnyvale::IsDayIndicator (strInput)) != -1)
		return Sunnyvale::GetDayIndicator (tm);
	else if ((nIndex = Sunnyvale::IsShiftIndicator (strInput)) != -1)
		return Sunnyvale::GetShiftIndicator (tm);

	strFormat.Remove (::cSpecifier);

	if (strFormat.Remove (::cLeftJustify)) 
		bLeftJustify = true;

	if (strFormat.GetLength ()) {
		TCHAR c = strFormat [0];

		for (int i = 0; i < ARRAYSIZE (map); i++) {
			if (map [i].m_c == c) {
				CString str = tm.Format (map [i].m_lpsz);
				int nLen = strFormat.GetLength ();

				if (!bLeftJustify) {
					if (str.GetLength () >= nLen)
						return str.Right (nLen);
					else 
						return CString ('0', nLen - str.GetLength ()) + str;
				}
				else {
					if (str.GetLength () >= nLen)
						return str.Left (nLen);
					else 
						return str + CString ('0', nLen - str.GetLength ());
				}
			}
		}
	}

	return strFormat;
}

int CDateTimeElement::GetRolloverMinutes () const
{
	LOCK (::csDays);

	int nRollover = 0;

	if (GetRollover ()) {
		for (int i = 0; i < ::vRollover.GetSize (); i++) {
			CodesDlg::CCodes c = ::vRollover [i];

			if (c.m_lLineID == GetLineID ()) {
				ASSERT (c.m_vCodes.GetSize () >= 1);

				nRollover = _ttoi (c.m_vCodes [0]);
				break;
			}
		}
	}

	return nRollover;
}

bool CDateTimeElement::SetRollover (bool b)
{
	m_bRollover = b;
	SetRedraw ();

	return true;
}

bool CDateTimeElement::GetRollover () const
{
	return m_bRollover;
}

void CDateTimeElement::SetHold (ULONG lLineID, bool bHold)
{
	LOCK (::csMap);

	HOLDSTRUCT s;

	s.m_lLineID = lLineID;
	s.m_bHold = bHold;

	::mapHold.SetAt (lLineID, s);
}

void CDateTimeElement::ResetHold (ULONG lLineID)
{
	LOCK (::csMap);

	{
		HOLDSTRUCT s;
		int nRolloverMinutes = _GetRolloverMinutes (lLineID);

		if (::mapHold.Lookup (lLineID, s)) {
			//s.m_tm = CTime::GetCurrentTime ();
			s.m_tm = CTime::GetCurrentTime () + CTimeSpan (0, 0, nRolloverMinutes, 0);
			::mapHold.SetAt (lLineID, s);
		}
		else {
			s.m_lLineID = lLineID;
			s.m_bHold = CRolloverDlg::GetHold (GetDB (), lLineID);
			//s.m_tm = CTime::GetCurrentTime ();
			s.m_tm = CTime::GetCurrentTime () + CTimeSpan (0, 0, nRolloverMinutes, 0);
			::mapHold.SetAt (lLineID, s);
		}
	}
}

bool CDateTimeElement::GetHold (ULONG lLineID)
{
	if (!ListGlobals::IsDBSet ())
		return false;

	LOCK (::csMap);

	HOLDSTRUCT s;

	{
		if (!::mapHold.Lookup (lLineID, s)) {
			int nRolloverMinutes = _GetRolloverMinutes (lLineID);

			s.m_lLineID = lLineID;
			s.m_bHold = CRolloverDlg::GetHold (GetDB (), lLineID);
			//s.m_tm = CTime::GetCurrentTime ();
			s.m_tm = CTime::GetCurrentTime () + CTimeSpan (0, 0, nRolloverMinutes, 0);
			::mapHold.SetAt (lLineID, s);
		}
	}

	return s.m_bHold;
}

#pragma data_seg( "csTime" )
	CRITICAL_SECTION csTime;
	static CMap <ULONG, ULONG, CTime *, CTime *> vTime;
#pragma

void CDateTimeElement::SetPrintTime (CTime * pTime, ULONG lLineID)
{
	::EnterCriticalSection (&::csTime);

	CTime * p = NULL;

	if (!pTime && lLineID == -1) {
		for (POSITION pos = ::vTime.GetStartPosition (); pos; ) {
			ULONG l;

			::vTime.GetNextAssoc (pos, l, p);

			if (p)
				delete p;
		}

		::vTime.RemoveAll ();
		::LeaveCriticalSection (&::csTime);
		return;
	}

	if (::vTime.Lookup (lLineID, p))
		delete p;

	::vTime.SetAt (lLineID, pTime);
	::LeaveCriticalSection (&::csTime);
}

CTime CDateTimeElement::GetPrintTime (ULONG lLineID)
{
	::EnterCriticalSection (&::csTime);
	
	CTime tm = CTime::GetCurrentTime ();
	CTime * p = NULL;

	if (::vTime.Lookup (lLineID, p))
		tm = * p;

	::LeaveCriticalSection (&::csTime);

	return tm;
}

//#define __DEBUG_CALCCURRENTTIME__
CTime CDateTimeElement::CalcCurrentTime (int nRolloverMinutes, ULONG lLineID, const CTime * ptm)
{
	CTime tmCurrent = GetPrintTime (lLineID);
	CTime tmNow = ptm ? * ptm : tmCurrent;

	#if defined( __DEBUG_CALCCURRENTTIME__ ) && !defined( _DEBUG )
		#error can't use __DEBUG_CALCCURRENTTIME__ in a release build
	#endif

	#if defined( __DEBUG_CALCCURRENTTIME__ ) && defined( _DEBUG )
	{
		int nDay		= 29;
		int nHour		= 12 + 10;
		int nMinutes	= 59 + 0;

		//nMinutes += (60 * 3) - 0;

		tmNow = CTime (2007, 5, nDay, nHour, nMinutes, 0);
	}
	#endif

	CTime tmRollover = tmNow + CTimeSpan (0, 0, nRolloverMinutes, 0);

	int nYear	= tmRollover.GetYear ();
	int nMonth	= tmRollover.GetMonth ();
	int nDay	= tmRollover.GetDay ();

	int nHour	= tmNow.GetHour ();
	int nMin	= tmNow.GetMinute ();
	int nSec	= tmNow.GetSecond ();

	#if defined( __DEBUG_CALCCURRENTTIME__ ) && defined( _DEBUG )
	LPCTSTR lpsz = "%c [%a]"; //"%m/%d/%y %I:%M %p [%a]";
	TRACEF (tmNow.Format (lpsz));
	TRACEF (tmRollover.Format (lpsz));
	TRACEF (CTime (nYear, nMonth, nDay, nHour, nMin, nSec).Format (lpsz) + "\n");
	#endif

	if (lLineID > 0 && GetHold (lLineID)) {
		LOCK (::csMap);
		HOLDSTRUCT s;

		VERIFY (::mapHold.Lookup (lLineID, s));

		//TRACEF (s.m_tm.Format ("%c"));

		nYear	= s.m_tm.GetYear ();
		nMonth	= s.m_tm.GetMonth ();
		nDay	= s.m_tm.GetDay ();
	}

	return CTime (nYear, nMonth, nDay, nHour, nMin, nSec);
}

CString CDateTimeElement::PreFormat (const CString & strIn)
{
	CString str (strIn);
	static const LPCTSTR lpsz [] = 
	{
		_T ("a"),
		_T ("A"),
		_T ("b"),
		_T ("B"),
		_T ("c"),
		_T ("d"),
		_T ("H"),
		_T ("I"),
		_T ("j"),
		_T ("m"),
		_T ("M"),
		_T ("p"),
		_T ("S"),
		_T ("U"),
		_T ("w"),
		_T ("W"),
		_T ("x"),
		_T ("X"),
		_T ("y"),
		_T ("Y"),
		_T ("z"),
		_T ("Z"),
		_T ("%"),
		_T ("#a"),
		_T ("#A"),
		_T ("#b"),
		_T ("#B"),
		_T ("#p"),
		_T ("#X"),
		_T ("#z"),
		_T ("#Z"),
		_T ("#%"),
		_T ("#c"),
		_T ("#x"),
		_T ("#d"),
		_T ("#H"),
		_T ("#I"),
		_T ("#j"),
		_T ("#m"),
		_T ("#M"),
		_T ("#S"),
		_T ("#U"),
		_T ("#w"),
		_T ("#W"),
		_T ("#y"),
		_T ("#Y"),
	};
	CStringArray v;

	for (int i = 0; i < str.GetLength (); ) {
		if (str [i] == '%') {
			bool bFound = false;
			LPCTSTR p = str;

			for (int j = 0; !bFound && j < ARRAYSIZE (lpsz); j++) {
				int nLen = _tcslen (lpsz [j]);

				if (str.GetLength () > (i + nLen)) {
					if (!_tcsncmp (&p [i + 1], lpsz [j], nLen)) {
						bFound = true;
						i += (nLen + 1);
					}
				}
			}

			if (!bFound) 
				str.Delete (i);
		}
		else
			i++;
	}

	return str;
}

/*
Resources:
  - DateTimeElements:
      - D ,Description = (Day of month 1-31, without leading zero)
      - DD ,Description = (Day of month 00-31)
      - d ,Description = (Day of week 1-7 (1 = Mon))
      - M ,Description = (Month of year 1-12, without leading zero)
      - MM ,Description = (Month of year 01-12)
      - MMM ,Description = (Month Abbreviated JAN-DEC)
      - MMMM ,Description = (Month Complete JANUARY-DECEMBER)
      - J366 ,Description = (Day of year 1-366 (29th Feb = 366), without leading zero)
      - JJ366 ,Description = (Day of year 001-366 (29th Feb = 366))
      - j60 ,Description = (Day of year 1-366 (29th Feb = 60), without leading zero)
      - jj60 ,Description = (Day of year 001-366 (29th Feb = 060))
      - y ,Description = (Year of decade 0-9)
      - Y ,Description = (Year of century 0-99, without leading zero)
      - YY ,Description = (Year of century 00-99)
      - YYYY ,Description = (Year e.g. 2014)
      - W ,Description = (Week of year 1-53, without leading zero)
      - WW ,Description = (Week of year 01-53)
      - HH ,Description = (Hour 00-23)
      - HHH ,Description = (Hour of the week)
      - hh ,Description = (Hour 00-12)
      - mm ,Description = (Minute 00-59)
      - mmm ,Description = (Minute of the day)
      - ss ,Description = (Second 00-59)
      - tt ,Description = (AM/PM)

	%a Abbreviated weekday name
	%A Full weekday name
	%b Abbreviated month name
	%B Full month name
	%c Date and time representation appropriate for locale
	%d Day of month as decimal number (01 - 31)
	%%D Day of month as decimal number, arbitrary length
	%%-D Day of month as decimal number, arbitrary length (left justified)
	%H Hour in 24-hour format (00 - 23)
	%I Hour in 12-hour format (01 - 12)
	%j Day of year as decimal number (001 - 366)
	%J Day of year as decimal number (001 - 366) (European)
	%m Month as decimal number (01 - 12)
	%%M Month as decimal number, arbitrary length
	%%-M Month as decimal number, arbitrary length (left justified)
	%M Minute as decimal number (00 - 59)
	%p Current locale's A.M./P.M. indicator for 12-hour clock
	%S Second as decimal number (00 - 59)
	%U Week of year as decimal number, with Sunday as first day of week (00 - 53)
	%w Weekday as decimal number (0 - 6; Sunday is 0)
	%W Week of year as decimal number, with Monday as first day of week (00 - 53)
	%1W Week of year as decimal number, with Monday as first day of week (01 - 54)
	%x Date representation for current locale
	%X Time representation for current locale
	%y Year without century, as decimal number (00 - 99)
	%Y Year with century, as decimal number
	%%Y Year, as decimal number (arbitrary length)
	%%-Y Year, as decimal number, arbitrary length (left justified)
	%z Time-zone name or abbreviation; no characters if time zone is unknown
	%%0H Hour code
	%%0M Month code
	%%0A Day code
	%%0Q Quarter hour code
*/

struct
{
	LPCTSTR m_lpszLinx;
	LPCTSTR m_lpszBW;
}
static const mapLinx [] = 
{
	{ _T ("D"),			_T (""),		}, // (Day of month 1-31, without leading zero)
	{ _T ("DD"),		_T ("%d"),		}, // (Day of month 00-31)
	{ _T ("d"),			_T (""),		}, // (Day of week 1-7 (1 = Mon))
	{ _T ("M"),			_T ("%%M"),		}, // (Month of year 1-12, without leading zero)
	{ _T ("MM"),		_T ("%m"),		}, // (Month of year 01-12)
	{ _T ("MMM"),		_T (""),		}, // (Month Abbreviated JAN-DEC)
	{ _T ("MMMM"),		_T (""),		}, // (Month Complete JANUARY-DECEMBER)
	{ _T ("J366"),		_T ("%j"),		}, // (Day of year 1-366 (29th Feb = 366), without leading zero)	
	{ _T ("JJ366"),		_T ("%J"),		}, // (Day of year 001-366 (29th Feb = 366))
	{ _T ("j60"),		_T (""),		}, // (Day of year 1-366 (29th Feb = 60), without leading zero)
	{ _T ("jj60"),		_T (""),		}, // (Day of year 001-366 (29th Feb = 060))
	{ _T ("y"),			_T (""),		}, // (Year of decade 0-9)
	{ _T ("Y"),			_T (""),		}, // (Year of century 0-99, without leading zero)
	{ _T ("YY"),		_T ("%y"),		}, // (Year of century 00-99)
	{ _T ("YYYY"),		_T ("%Y"),		}, // (Year e.g. 2014)
	{ _T ("W"),			_T (""),		}, // (Week of year 1-53, without leading zero)
	{ _T ("WW"),		_T ("%1W"),		}, // (Week of year 01-53)
	{ _T ("HH"),		_T ("%H"),		}, // (Hour 00-23)
	{ _T ("HHH"),		_T (""),		}, // (Hour of the week)
	{ _T ("hh"),		_T ("%I"),		}, // (Hour 00-12)
	{ _T ("mm"),		_T ("%M"),		}, // (Minute 00-59)
	{ _T ("mmm"),		_T (""),		}, // (Minute of the day)
	{ _T ("ss"),		_T ("%S"),		}, // (Second 00-59)
	{ _T ("tt"),		_T ("%p"),		}, // (AM/PM)
};

	
#if 0
static CString TranslateLinxFormat (const CString & str)
{
	CString strResult (str);

	for (int i = 0; i < ARRAYSIZE (::mapLinx); i++) {
		CString strFind = _T ("%") + CString (::mapLinx[i].m_lpszLinx) + _T ("%");
		CString strReplace = _T ("#") + CString (::mapLinx[i].m_lpszLinx) + _T ("#");
	
		strResult.Replace (strFind, strReplace);
	}

	for (int i = 0; i < ARRAYSIZE (::mapLinx); i++) {
		if (_tcslen (::mapLinx [i].m_lpszBW)) {
			CString strFind = _T ("#") + CString (::mapLinx[i].m_lpszLinx) + _T ("#");

			strResult.Replace (strFind, ::mapLinx [i].m_lpszBW);
		}
	}

	return strResult;
}
#endif

CString CDateTimeElement::Format (ULONG lLineID, const CString & strInput, int nRollover, const CTime * ptm)
{
	CTime tm = ptm ? * ptm : CalcCurrentTime (nRollover, lLineID);
	CString strInputTmp (strInput);

	#if __CUSTOM__ == __SW0877__
	lLineID = 0;
	#endif

	#if 0
	{ // Linx preview
		strInputTmp = TranslateLinxFormat (strInputTmp);

		if (strInputTmp.Find (_T ("#D#")) != -1) // (Day of year 1-366 (29th Feb = 60), without leading zero)
			strInputTmp.Replace (_T ("#D#"), FoxjetDatabase::ToString (_ttoi (tm.Format (_T ("%d")))));

		if (strInputTmp.Find (_T ("#d#")) != -1) // (Day of week 1-7 (1 = Mon))
			strInputTmp.Replace (_T ("#d#"), FoxjetDatabase::ToString (_ttoi (tm.Format (_T ("%w"))) + 1));

		if (strInputTmp.Find (_T ("#MMM#")) != -1) { // (Month Abbreviated JAN-DEC)
			CString strMonth = tm.Format (_T ("%B"));

			strMonth.MakeUpper ();
			strInputTmp.Replace (_T ("#MMM#"), std::wstring (strMonth).substr (0, 3).c_str ());
		}

		if (strInputTmp.Find (_T ("#MMMM#")) != -1) { // (Month Complete JANUARY-DECEMBER)
			CString strMonth = tm.Format (_T ("%B"));

			strMonth.MakeUpper ();
			strInputTmp.Replace (_T ("#MMMM#"), strMonth);
		}

		if (strInputTmp.Find (_T ("#j60#")) != -1) // (Day of year 1-366 (29th Feb = 60), without leading zero)
			strInputTmp.Replace (_T ("#j60#"), FoxjetDatabase::ToString (_ttoi (tm.Format (_T ("%j")))));

		if (strInputTmp.Find (_T ("#jj60#")) != -1) // (Day of year 001-366 (29th Feb = 060))
			strInputTmp.Replace (_T ("#jj60#"), FoxjetDatabase::ToString (_ttoi (Format (lLineID, _T ("%J"), 0, &tm))));

		if (strInputTmp.Find (_T ("#y#")) != -1) { // (Year of decade 0-9)
			CString str = FoxjetDatabase::ToString (_ttoi (tm.Format (_T ("%y"))));

			str = str.Mid (str.GetLength () - 1);
			strInputTmp.Replace (_T ("#y#"), str);
		}

		if (strInputTmp.Find (_T ("#Y#")) != -1) { // (Year of century 0-99, without leading zero)
			CString str = FoxjetDatabase::ToString (_ttoi (tm.Format (_T ("%y"))));

			str = str.Mid (str.GetLength () - 2);
			strInputTmp.Replace (_T ("#Y#"), str);
		}

		if (strInputTmp.Find (_T ("#W#")) != -1)
			strInputTmp.Replace (_T ("#W#"), FoxjetDatabase::ToString (_ttoi (tm.Format (_T ("%W")))));

		if (strInputTmp.Find (_T ("#HHH#")) != -1) { // (Hour of the week)
			CTime tmStart (tm.GetYear (), tm.GetMonth (), tm.GetDay (), 0, 0, 0);
			
			for (int n = _ttoi (tmStart.Format (_T ("%W"))); n == _ttoi (tmStart.Format (_T ("%W"))); )
				tmStart -= CTimeSpan (1, 0, 0, 0);

			CTimeSpan tmDiff = tm - tmStart;
			int nDiff = tmDiff.GetTotalHours ();

			strInputTmp.Replace (_T ("#HHH#"), FoxjetDatabase::ToString (nDiff));
		}

		if (strInputTmp.Find (_T ("#mmm#")) != -1) { // (Minute of the day)
			CTime tmStart (tm.GetYear (), tm.GetMonth (), tm.GetDay (), 0, 0, 0);
			CTimeSpan tmDiff = tm - tmStart;
			int nDiff = tmDiff.GetTotalMinutes ();

			strInputTmp.Replace (_T ("#mmm#"), FoxjetDatabase::ToString (nDiff));
		}
	}
	#endif

	{ // case for european julian day
		if (tm.GetMonth () == 2 && tm.GetDay () == 29)
			strInputTmp.Replace (_T ("%J"), _T ("366"));
		else if (tm.GetMonth () > 2) {
			bool bLeapYear = (tm.GetYear () % 4) == 0 ? true : false;
			CTime tmTmp = tm;
			
			if (bLeapYear)
				tmTmp -= CTimeSpan (1, 0, 0, 0);

			CString str = tmTmp.Format (_T ("%j"));

			strInputTmp.Replace (_T ("%J"), str);
		}
		else 
			strInputTmp.Replace (_T ("%J"), _T ("%j"));
	}

	strInputTmp.Replace (_T ("%1W"), FoxjetDatabase::ToString (_ttoi (tm.Format (_T ("%W"))) + 1));
	strInputTmp = PreFormat (strInputTmp);
	CString str = UnformatString (tm.Format (FormatString (strInputTmp)));
	CStringArray v;

	TokenizeFormats (str, v);

	for (int nToken = 0; nToken < v.GetSize (); nToken++) {
		CString strToken = v [nToken];
		CString strReplace = Format (lLineID, strToken, tm);

		if (strToken != strReplace) 
			int nReplace = str.Replace (strToken, strReplace);
	}

	return str;
}

int CDateTimeElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	if (type == INITIAL)
		m_tmInitial = CalcCurrentTime (0, m_lLineID);

	m_strImageData = Format (GetLineID (), m_strDefaultData, GetRolloverMinutes (), !GetHoldStartDate () ? NULL : &m_tmInitial);
	return GetClassID ();
}

// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
static const ElementFields::FIELDSTRUCT fields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType,	_T ("")				),	// [0]	Prefix (required)
	ElementFields::FIELDSTRUCT (m_lpszID,			_T ("")				),	// [1]	ID (required)
	ElementFields::FIELDSTRUCT (m_lpszX,			_T ("")				),	// [2]	x (required)
	ElementFields::FIELDSTRUCT (m_lpszY,			_T ("")				),	// [3]	y (required)	
	ElementFields::FIELDSTRUCT (m_lpszFlipH,		_T ("0")			),	// [4]	flip h	
	ElementFields::FIELDSTRUCT (m_lpszFlipV,		_T ("0")			),	// [5]	flip v	
	ElementFields::FIELDSTRUCT (m_lpszInverse,		_T ("0")			),	// [6]	inverse	
	ElementFields::FIELDSTRUCT (m_lpszFontName,		_T ("Courier New")	),	// [7]	font name	
	ElementFields::FIELDSTRUCT (m_lpszFontSize,		_T ("32")			),	// [8]	font size	
	ElementFields::FIELDSTRUCT (m_lpszBold,			_T ("0")			),	// [9]	bold
	ElementFields::FIELDSTRUCT (m_lpszItalic,		_T ("0")			),	// [10] italic
	ElementFields::FIELDSTRUCT (m_lpszData,			_T ("%H:%M:%S")		),	// [11] format string
	ElementFields::FIELDSTRUCT (m_lpszOrient,		_T ("0")			),	// [12] orientation	
	ElementFields::FIELDSTRUCT (m_lpszWidth,		_T ("0")			),	// [13] font width
	ElementFields::FIELDSTRUCT (m_lpszRollover,		_T ("1")			),	// [14] rollover
	ElementFields::FIELDSTRUCT (m_lpszAlign,		_T ("0")			),	// [15] alignment
	ElementFields::FIELDSTRUCT (m_lpszDither,		_T ("0")			),	// [16]
	ElementFields::FIELDSTRUCT (m_lpszColor,		_T ("0")			),	// [17]
	ElementFields::FIELDSTRUCT (m_lpszHoldStartDate,_T ("0")			),	// [18]
	ElementFields::FIELDSTRUCT (),
};

CString CDateTimeElement::ToString (const FoxjetCommon::CVersion & ver) const
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated

	CString str;
	CPrinterFont f = GetFont ();
	CPoint pt = GetPosInternal (ALIGNMENT_ADJUST_ADD);

	str.Format (_T ("{Datetime,%u,%u,%u,%d,%d,%d,%s,%u,%d,%d,%s,%d,%d,%d,%d,%d,%06X,%d}"),
		GetID (),
		pt.x, pt.y,
		IsFlippedH (),
		IsFlippedV (),
		IsInverse (),
		f.m_strName, f.m_nSize, f.m_bBold, f.m_bItalic,
		FormatString (GetDefaultData ()),
		(int)GetOrientation (),
		GetWidth (),
		GetRollover (),
		GetAlignment (),
		GetDither (),
		GetColor (),
		GetHoldStartDate ());

	return str;
}

bool CDateTimeElement::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
/*
	const int nFields = GetFieldCount ();
	CStringArray vstrToken, vActual, vDef;
	CPrinterFont f;
	int i;
	
	for (i = 0; i < nFields; i++) 
		vstrToken.Add (GetFieldDef (i));

	vstrToken [14] = _T ("1"); // default rollover to "1"

	Tokenize (str, vActual);

	if (vActual.GetSize () < 4) {
		TRACEF ("Too few tokens");
		return false;
	}

	Tokenize (ListGlobals::defElements.GetRegElementDefs (GetClassID (), ver), vDef);

	for (i = 1; i < min (vDef.GetSize (), nFields); i++)
		if (vDef [i].GetLength ())
			vstrToken [i] = vDef [i];

	for (i = 0; i < min (vActual.GetSize (), nFields); i++)
		if (vActual [i].GetLength ())
			vstrToken [i] = vActual [i];
*/
	CStringArray vstrToken;
	CPrinterFont f;

	Parse (str, ver, vstrToken);

	if (vstrToken [0].CompareNoCase (_T ("Datetime")) != 0){
		TRACEF ("Wrong prefix \"" + vstrToken [0] + "\"");
		return false;
	}

	f.m_strName = vstrToken [7];
	f.m_nSize = (UINT)_tcstol (vstrToken [8], NULL, 10);
	f.m_bBold = _ttoi (vstrToken [9]) ? true : false;
	f.m_bItalic = _ttoi (vstrToken [10]) ? true : false;

	bool bResult = true;

	bResult &= SetID ((UINT)_tcstol (vstrToken [1], NULL, 10));
	bResult &= SetPos (CPoint (
		(UINT)_tcstol (vstrToken [2], NULL, 10), 
		(UINT)_tcstol (vstrToken [3], NULL, 10)), NULL);
	bResult &= SetFlippedH (_ttoi (vstrToken [4]) ? true : false);
	bResult &= SetFlippedV (_ttoi (vstrToken [5]) ? true : false);
	bResult &= SetInverse (_ttoi (vstrToken [6]) ? true : false);
	bResult &= SetFont (f);
	bResult &= SetDefaultData (UnformatString (vstrToken [11]));
	bResult &= SetOrientation ((TextElement::FONTORIENTATION)_ttoi (vstrToken [12]));
	bResult &= SetWidth (_ttoi (vstrToken [13]));
	bResult &= SetRollover (_ttoi (vstrToken [14]) ? true : false);
	bResult &= SetAlignment ((ALIGNMENT)_ttoi (vstrToken [15]));
	bResult &= SetDither (_ttoi (vstrToken [16]));
	bResult &= SetColor (_tcstoul (vstrToken [17], NULL, 16));
	bResult &= SetHoldStartDate (_ttoi (vstrToken [18]) ? true : false);

	return bResult;
}

const FoxjetCommon::ElementFields::FIELDSTRUCT * CDateTimeElement::GetFieldBuffer () const
{
	return ::fields;
}

bool CDateTimeElement::GetHoldStartDate () const
{
	return m_bHoldStartDate;
}

bool CDateTimeElement::SetHoldStartDate (bool b) 
{
	m_bHoldStartDate = b;

	return true;
}
