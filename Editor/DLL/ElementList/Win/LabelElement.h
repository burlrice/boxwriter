#ifndef __RFIDELEMENT_H__
#define __RFIDELEMENT_H__

#include "BaseElement.h"
#include "Types.h"
#include "ElementApi.h"

namespace FoxjetElements
{
	class ELEMENT_API CLabelElement : public FoxjetCommon::CBaseElement  
	{
		DECLARE_DYNAMIC (CLabelElement);

	public:
		CLabelElement (const FoxjetDatabase::HEADSTRUCT & head);
		CLabelElement (const CLabelElement & rhs);
		CLabelElement & operator = (const CLabelElement & rhs);
		virtual ~CLabelElement();

		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);
		virtual CSize Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly = false, FoxjetCommon::DRAWCONTEXT context = EDITOR);
		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		virtual int GetClassID () const { return LABEL; }
		virtual bool IsPrintable () const { return false; }

		virtual int Build (BUILDTYPE type = INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;
		virtual void ClipTo (const FoxjetDatabase::HEADSTRUCT & head);
		virtual CSize GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI = true) const;

		virtual void OnCreateNew (const FoxjetDatabase::HEADSTRUCT & head, const CString & strTask);
		virtual void OnDestroyNew ();

		bool SetFilePath (const CString &strPath);
		CString GetFilePath () const;
		
		ULONG GetBatchQuantity () const;
		bool SetBatchQuantity (ULONG lQty);

		static CString GetDefFilePath ();

		static CString GetEditorPath ();
		static bool SetEditorPath (const CString & str);
		static bool IsLabelEditorPresent ();

		static CString GetLabelDirectory ();

		static void FreeStaticMembers ();

		static CBitmap * m_pbmpIcon;
		static bool * m_pbLabelEditorPresent;

		static void DrawIcon (CDC & dc, const CRect & rc);

	protected:
		CString m_strFilePath;
		ULONG m_lBatchQuantity;
	};
}; // namespace FoxjetElements


#endif //__RFIDELEMENT_H__
