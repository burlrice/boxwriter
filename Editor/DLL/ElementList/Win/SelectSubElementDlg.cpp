// SelectSubElementDlg.cpp: implementation of the CSelectSubElementDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SelectSubElementDlg.h"
#include "SubElement.h"
#include "CopyArray.h"
#include "ItiLibrary.h"
#include "Edit.h"
#include "MsgBox.h"

using namespace ItiLibrary;
using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/*
note: since the list ctrl on this dialog is sortable,
m_vCtrlData [nIndex] == GetListCtrl ().GetItemData (nIndex)
may not always be true, use GetListCtrl ().GetItemData (nIndex) instead
*/

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSelectSubElementDlg::CSelectSubElementDlg(CWnd* pParent /*=NULL*/)
	: CAiListDlg(IDD_SELECTSUBELEMENTS, pParent)
{
	//{{AFX_DATA_INIT(CSelectSubElementDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CSelectSubElementDlg::~CSelectSubElementDlg ()
{
}

void CSelectSubElementDlg::DoDataExchange(CDataExchange* pDX)
{
	CAiListDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectSubElementDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectSubElementDlg, CAiListDlg)
	//{{AFX_MSG_MAP(CSelectSubElementDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectSubElementDlg message handlers


void CSelectSubElementDlg::OnOK()
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		for (int i = 0; i < sel.GetSize (); i++) {
			CSubItem * p = (CSubItem *)m_lv.GetCtrlData (sel [i]);

			ASSERT (p);
			m_vSelected.Add (p->m_pElement);
		}

		FoxjetCommon::CEliteDlg::OnOK ();
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CSelectSubElementDlg::OnDblclkListCtrl(NMHDR* pNMHDR, LRESULT* pResult)
{
	OnOK ();
	* pResult = 0;
}

BOOL CSelectSubElementDlg::OnInitDialog()
{
	if (!m_bAI) 
		SetWindowText (LoadString (IDS_SELECTSUBELEMENTS));

	return CAiListDlg::OnInitDialog ();
}
