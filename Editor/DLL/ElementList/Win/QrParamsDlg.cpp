// QrParamsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "QrParamsDlg.h"
#include "afxdialogex.h"
#include "Debug.h"
#include "Parse.h"
#include "Resource.h"


// CQrParamsDlg dialog

IMPLEMENT_DYNAMIC(CQrParamsDlg, FoxjetCommon::CEliteDlg)

CQrParamsDlg::CQrParamsDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(CQrParamsDlg::IDD, pParent)
{

}

CQrParamsDlg::~CQrParamsDlg()
{
}

void CQrParamsDlg::DoDataExchange(CDataExchange* pDX)
{
	DDX_Text (pDX, TXT_SYMBOLOGY, m_strSymbology);
	DDX_Text (pDX, TXT_MAG, m_nMag);
	DDX_Text (pDX, TXT_WIDTH, m_nWidth);
	DDX_Text (pDX, TXT_HEIGHT, m_nHeight);
	DDX_Check (pDX, CHK_CASESENSITIVE, m_bCaseSensitive);
}


BEGIN_MESSAGE_MAP(CQrParamsDlg, CDialog)
END_MESSAGE_MAP()


// CQrParamsDlg message handlers


BOOL CQrParamsDlg::OnInitDialog(void)
{
	FoxjetCommon::CEliteDlg::OnInitDialog ();

	if (m_bStandardParam) {
		if (CWnd * p = GetDlgItem (TXT_MAG))
			p->EnableWindow (FALSE);
	}

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_VERSION)) {
		for (int i = 0; i <= 6; i++) {
			CString str = FoxjetDatabase::ToString (i);

			if (i == 0)
				str = LoadString (IDS_AUTOSELECT);

			int nIndex = p->AddString (str);

			p->SetItemData (nIndex, i);

			if (m_nVersion == i)
				p->SetCurSel (nIndex);
		}

		if (p->GetCurSel () == CB_ERR)
			p->SetCurSel (0);
	}

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_LEVEL)) {
		struct 
		{
			int m_n;
			UINT m_nID;
		} static const mapLevel [] = 
		{
			{ QR_ECLEVEL_L, IDS_ECLEVEL_L, },
			{ QR_ECLEVEL_M, IDS_ECLEVEL_M, },
			{ QR_ECLEVEL_Q, IDS_ECLEVEL_Q, },
			{ QR_ECLEVEL_H, IDS_ECLEVEL_H, },
		};

		for (int i = 0; i < ARRAYSIZE (mapLevel); i++) {
			int nIndex = p->AddString (LoadString (mapLevel [i].m_nID));

			p->SetItemData (nIndex, mapLevel [i].m_n);

			if (m_level == mapLevel [i].m_n)
				p->SetCurSel (nIndex);
		}

		if (p->GetCurSel () == CB_ERR)
			p->SetCurSel (0);
	}

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_HINT)) {
		struct 
		{
			int m_n;
			UINT m_nID;
		} static const mapHint [] = 
		{
			{ QR_MODE_NUM,			IDS_QR_MODE_NUM,			},	///< Numeric mode		
			{ QR_MODE_AN,			IDS_QR_MODE_AN,				},	///< Alphabet-numeric mode		
			{ QR_MODE_8,			IDS_QR_MODE_8,				},	///< 8-bit data mode		
			{ QR_MODE_KANJI,		IDS_QR_MODE_KANJI,			},	///< Kanji (shift-jis) mode		
			{ QR_MODE_ECI,			IDS_QR_MODE_ECI,			},	///< ECI mode		
		//	{ QR_MODE_NUL,			_T ("QR_MODE_NUL"),			},	///< Terminator (NUL character). Internal use only		
		//	{ QR_MODE_STRUCTURE,	_T ("QR_MODE_STRUCTURE"),	},	///< Internal use only
		//	{ QR_MODE_FNC1FIRST,	_T ("QR_MODE_FNC1FIRST"),	},	 ///< FNC1, first position	
		//	{ QR_MODE_FNC1SECOND,	_T ("QR_MODE_FNC1SECOND"),	},	 ///< FNC1, second position
		};

		for (int i = 0; i < ARRAYSIZE (mapHint); i++) {
			int nIndex = p->AddString (LoadString (mapHint [i].m_nID));

			p->SetItemData (nIndex, mapHint [i].m_n);

			if (m_hint == mapHint [i].m_n)
				p->SetCurSel (nIndex);
		}

		if (p->GetCurSel () == CB_ERR)
			p->SetCurSel (3);
	}

	return TRUE;
}

void CQrParamsDlg::OnOK()
{
	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_VERSION)) 
		m_nVersion = (int)p->GetItemData (p->GetCurSel ());

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_LEVEL)) 
		m_level = (QRecLevel)p->GetItemData (p->GetCurSel ());

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_HINT)) 
		m_hint = (QRencodeMode)p->GetItemData (p->GetCurSel ());

	FoxjetCommon::CEliteDlg::OnOK ();
}
