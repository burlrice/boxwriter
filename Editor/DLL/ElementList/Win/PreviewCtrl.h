#if !defined(AFX_PREVIEWCTRL_H__4D7B78D3_3C0F_4478_AE37_88F755EE65BD__INCLUDED_)
#define AFX_PREVIEWCTRL_H__4D7B78D3_3C0F_4478_AE37_88F755EE65BD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PreviewCtrl.h : header file
//

#include <afxwin.h>
#include "BaseElement.h"

/////////////////////////////////////////////////////////////////////////////
// CPreviewCtrl window

class CPreviewCtrl : public CScrollView
{
// Construction
public:
	CPreviewCtrl();

// Attributes
public:

// Operations
public:

	bool Create (CWnd * pParent, const CRect & rc, const FoxjetCommon::CBaseElement * pElement,
		const FoxjetDatabase::HEADSTRUCT * pHead);

	bool SetZoom (double dZoom);
	double CalcFitZoom ();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPreviewCtrl)
	protected:
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CPreviewCtrl();
	void Invalidate( BOOL bErase = TRUE );

	// Generated message map functions
protected:
	virtual void OnDraw(CDC* pDC);
	void Free ();

	const FoxjetCommon::CBaseElement *	m_pElement;
	const FoxjetDatabase::HEADSTRUCT *	m_pHead;
	CSize								m_sizeElement;
	double								m_dZoom;
	CBitmap	*							m_pBmp;

	//{{AFX_MSG(CPreviewCtrl)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PREVIEWCTRL_H__4D7B78D3_3C0F_4478_AE37_88F755EE65BD__INCLUDED_)
