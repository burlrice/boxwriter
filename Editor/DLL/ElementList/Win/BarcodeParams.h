// BarcodeParams.h: interface for the CBarcodeParams class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BARCODEPARAMS_H__5C071DB7_6A83_4871_9FCE_DD86E81F122B__INCLUDED_)
#define AFX_BARCODEPARAMS_H__5C071DB7_6A83_4871_9FCE_DD86E81F122B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ElementApi.h"
#include "TextElement.h"
#include "Wasp\bcxl.h"
#include "qrencode-3.4.4\qrencode.h"

namespace FoxjetElements
{
	class ELEMENT_API CBarcodeParams  
	{
	public:
		CBarcodeParams (int nSymbology = kSymbologyCode39Full, int nBarWidth = 40, 
			int nBarHeight = 32, int nRatio = 20, int nCheckSum = 0, int nMagPct = 100, 
			int nHBearer = 0, int nVBearer = 0, int nQueitzone = 0,
			LPCTSTR lpszFont = m_lpszDefFont, int nFontSize = 21,
			bool bCustom = true);
		CBarcodeParams (const CBarcodeParams & rhs);
		CBarcodeParams & operator = (const CBarcodeParams & rhs);
		virtual ~CBarcodeParams();

		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);

		static void FromString (CArray <CBarcodeParams, CBarcodeParams &> & v, const CString & str, 
			const FoxjetCommon::CVersion & ver);

		ULONG GetID () const;
		bool SetID (ULONG lID);

		int GetSymbology () const;
		bool SetSymbology (int n);

		int GetBarWidth () const;
		bool SetBarWidth (int n, int nSymbology);
		
		int GetBarHeight () const;
		bool SetBarHeight (int n);
		
		int GetRatio () const;
		bool SetRatio (int n, int nSymbology);
		
		int GetCheckSum () const;
		bool SetCheckSum (int n);
		
		int GetMagPct () const;
		bool SetMagPct (int n, int nSymbology);

		int GetHorzBearer () const;
		bool SetHorzBearer (int n);

		int GetVertBearer () const;
		bool SetVertBearer (int n);
		
		int	GetQuietZone () const;
		bool SetQuietZone (int n);

		CString GetFont () const;
		bool SetFont (const CString & str);

		int GetSize () const;
		bool SetSize (int n);
		
		int	GetStyle () const;
		bool SetStyle (int n);
		
		int	GetAlignment () const;
		bool SetAlignment (int n);

		int	GetBarPixels (int nBar) const;
		bool SetBarPixels (int nBar, int n);

		int	GetSpacePixels (int nBar) const;
		bool SetSpacePixels (int nBar, int n);

		int GetFontWidth () const;
		bool SetFontWidth (int n);

		int GetEncoding () const;
		bool SetEncoding (int nEncoding);

		ULONG GetLineID () const { return m_lLineID; }
		bool SetLineID (ULONG lLineID) { m_lLineID = lLineID; return true; }

		void SetCustom (bool b);
		bool IsCustom () const;


		void ClipTo (FoxjetDatabase::HEADTYPE type);

		bool SetOrientation (TextElement::FONTORIENTATION orient);
		TextElement::FONTORIENTATION GetOrientation () const;

		int GetVersion () const				{ return m_qr.m_nVersion; }
		void SetVersion (int n)				{ m_qr.m_nVersion = n; }

		QRecLevel GetLevel () const			{ return m_qr.m_level; }
		void SetLevel (QRecLevel level)		{ m_qr.m_level = level; }

		QRencodeMode GetHint () const		{ return m_qr.m_hint; }
		void SetHint (QRencodeMode hint)	{ m_qr.m_hint = hint; }

		bool IsCaseSensitive () const		{ return m_qr.m_bCaseSensitive; }
		void SetCaseSensitive (bool b)		{ m_qr.m_bCaseSensitive = b; }

		bool IsOptimizedFNC1 () const		{ return m_bOptimizeFNC1; }
		bool SetOptimizedFNC1 (bool b)		{ m_bOptimizeFNC1 = b; return true; }

		static const FoxjetCommon::LIMITSTRUCT m_lmtBearer;		// in mils
		static const FoxjetCommon::LIMITSTRUCT m_lmtQuietZone;	// in mils
		static const FoxjetCommon::LIMITSTRUCT m_lmtBar;		// in pixels

		static const LPCTSTR m_lpszDefFont;
		static const LPCTSTR m_lpszUPCFont;
		static const int m_nUPCFontWidth;

	protected:
		ULONG	m_lID;

		int		m_nSymbology;
		int		m_nBarWidth;
		int		m_nBarHeight;
		int		m_nRatio;
		int		m_nCheckSum;
		int		m_nMagPct; 

		int		m_nHorzBearer;
		int		m_nVertBearer;
		int		m_nQuietZone;

		CString m_strFont;
		int		m_nSize;
		int		m_nStyle;
		int		m_nAlignment;

		bool	m_bCustom;

		int		m_nBar [4];
		int		m_nSpace [4];
		int		m_nFontWidth;

		int		m_nEncoding;
		ULONG	m_lLineID;

		bool	m_bOptimizeFNC1;

		struct
		{
			int m_nVersion;
			QRecLevel m_level;
			QRencodeMode m_hint;
			bool m_bCaseSensitive;
		} m_qr;

		FoxjetElements::TextElement::FONTORIENTATION m_orient;
	};

	inline bool operator == (const CBarcodeParams & lhs, const CBarcodeParams & rhs) { return lhs.ToString (FoxjetCommon::verApp) == rhs.ToString (FoxjetCommon::verApp); }
	inline bool operator != (const CBarcodeParams & lhs, const CBarcodeParams & rhs) { return !(lhs == rhs); }

	class ELEMENT_API CUPCAParams : public CBarcodeParams
	{
	public:
		CUPCAParams (int nMag = 200, int nBarHeight = 128, 
			int nSpace1 = 6,	int nSpace2 = 10,	int nSpace3 = 13,	int nSpace4 = 17,
			int nBar1 = 3,		int nBar2 = 7,		int nBar3 = 10,		int nBar4 = 14,
			int nFontWidth = m_nUPCFontWidth,
			LPCTSTR lpszFont = m_lpszUPCFont, int nSize = 16,
			int nAlignment = 0, bool bCustom = true);
	};

	class ELEMENT_API CC128Params : public CBarcodeParams
	{
	public:
		CC128Params (int nSymbology, // for EAN128
			int nMag, int nBarHeight, 
			int nSpace1,	int nSpace2,	int nSpace3,	int nSpace4,
			int nBar1,		int nBar2,		int nBar3,		int nBar4,
			int nFontWidth,
			LPCTSTR lpszFont, int nSize,
			int nAlignment, bool bCustom);
	};

	class ELEMENT_API CI2o5Params : public CBarcodeParams
	{
	public:
		CI2o5Params (int nMag = 100, int nBarHeight = 118, 
			int nSpace1 = 6,	int nSpace2 = 16,
			int nBar1 = 5,		int nBar2 = 14,	
			int nHBearer = 190,	int nVBearer = 190, int nQuietzone = 450,
			bool bChecksum = true, int nFontWidth = 0,
			LPCTSTR lpszFont = m_lpszDefFont, int nSize = 16,
			int nAlignment = 0, bool bCustom = true);
	};

	class ELEMENT_API CGTIN14Params : public CI2o5Params
	{
	public:
		CGTIN14Params (int nMag = 100, int nBarHeight = 118, 
			int nSpace1 = 6,	int nSpace2 = 16,
			int nBar1 = 5,		int nBar2 = 14,	
			int nHBearer = 190,	int nVBearer = 190, int nQuietzone = 450,
			bool bChecksum = true, int nFontWidth = 0,
			LPCTSTR lpszFont = m_lpszDefFont, int nSize = 16,
			int nAlignment = 0, bool bCustom = true);
	};


	class ELEMENT_API CDataMatrixParams : public CBarcodeParams
	{
	public:
		CDataMatrixParams (int nSymbology = kSymbologyDataMatrixEcc200, int nMag = 100, 
			int nWidthFactor = 30, int nHeightFactor = 12, 
			LPCTSTR lpszFont = m_lpszDefFont, int nSize = 16, int nFontWidth = 0, 
			bool bCustom = true);
	};

	class ELEMENT_API CQrParams : public CDataMatrixParams
	{
	public:
		CQrParams (int nMag = 100, int nWidthFactor = 30, int nHeightFactor = 12, bool bCustom = true);
	};

	std::vector <int> ELEMENT_API GetResolutions ();

	CString ELEMENT_API GetDefaultData (int nSymbology, const CBarcodeParams & params);
	CString ELEMENT_API GetHeadTypeString (FoxjetDatabase::HEADTYPE type, FoxjetDatabase::COdbcDatabase & db);
	void ELEMENT_API GetBarcodeParams (ULONG lLineID, int nSymbology, FoxjetDatabase::HEADTYPE type, int nResolution, 
		CArray <CBarcodeParams, CBarcodeParams &> & v,
		FoxjetDatabase::COdbcDatabase & db);

	CString ELEMENT_API GetSymbology (int nSymbology, FoxjetDatabase::COdbcDatabase & db);
	int ELEMENT_API GetSymbology (const CString & strSymbology, FoxjetDatabase::COdbcDatabase & db);
	CString ELEMENT_API GetSettingsKey (FoxjetDatabase::HEADTYPE headType, int nResolution, int nSymbology, FoxjetDatabase::COdbcDatabase & db);
	int ELEMENT_API GetParamsPerSymbology ();
	CBarcodeParams ELEMENT_API GetDefParam (int nSymbology, int nIndex, FoxjetDatabase::HEADTYPE type, int nResolution, FoxjetDatabase::COdbcDatabase & db);
	CBarcodeParams ELEMENT_API GetBarcodeParam (ULONG lLineID, ULONG lLocalParamUID, int nSymbology, FoxjetDatabase::HEADTYPE type, int nResolution, 
		int nIndex, FoxjetDatabase::COdbcDatabase & db);

	bool ELEMENT_API SetBarcodeParam (ULONG lLineID, UINT nSymbology, FoxjetDatabase::HEADTYPE type, int nResolution, int nIndex, const CBarcodeParams & param, 
		FoxjetDatabase::COdbcDatabase & db);
	void ELEMENT_API SaveBarcodeParams (FoxjetDatabase::COdbcDatabase & db, ULONG lLineID, UINT nSymbology, FoxjetDatabase::HEADTYPE type, int nResolution);
	void ELEMENT_API DeleteBarcodeParams (FoxjetDatabase::COdbcDatabase & db);

}; //namespace FoxjetElements

#endif // !defined(AFX_BARCODEPARAMS_H__5C071DB7_6A83_4871_9FCE_DD86E81F122B__INCLUDED_)
