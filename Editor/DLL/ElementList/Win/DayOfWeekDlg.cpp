// DayOfWeekDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "DayOfWeekDlg.h"
#include "TemplExt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDayOfWeekDlg dialog


CDayOfWeekDlg::CDayOfWeekDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(IDD_DAYOFWEEK, pParent)
{
	//{{AFX_DATA_INIT(CDayOfWeekDlg)
	m_nDay = -1;
	//}}AFX_DATA_INIT
}


void CDayOfWeekDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDayOfWeekDlg)
	DDX_Radio(pDX, IDC_DAY1, m_nDay);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDayOfWeekDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDayOfWeekDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDayOfWeekDlg message handlers

BOOL CDayOfWeekDlg::OnInitDialog() 
{
	struct
	{
		UINT m_nLCID;
		UINT m_nCtrl;
	} static const map [] =
	{
		{ LOCALE_SDAYNAME7,		IDC_DAY1	},
		{ LOCALE_SDAYNAME1,		IDC_DAY2	},
		{ LOCALE_SDAYNAME2,		IDC_DAY3	},
		{ LOCALE_SDAYNAME3,		IDC_DAY4	},
		{ LOCALE_SDAYNAME4,		IDC_DAY5	},
		{ LOCALE_SDAYNAME5,		IDC_DAY6	},
		{ LOCALE_SDAYNAME6,		IDC_DAY7	},
	};

	FoxjetCommon::CEliteDlg::OnInitDialog();

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		TCHAR sz [512] = { 0 };

		::GetLocaleInfo (LOCALE_SYSTEM_DEFAULT, map [i].m_nLCID, sz, sizeof (sz));
		SetDlgItemText (map [i].m_nCtrl, sz);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
