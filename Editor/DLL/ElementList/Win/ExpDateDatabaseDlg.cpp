// ExpDateDatabaseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "ExpDateDatabaseDlg.h"
#include "Resource.h"
#include "OdbcDatabase.h"
#include "OdbcTable.h"
#include "TableTypeDlg.h"
#include "OdbcRecordset.h"
#include "DbFormatDlg.h"
#include "DbFieldDlg.h"
#include "Extern.h"
#include "TemplExt.h"
#include "DatabaseElement.h"
#include "Utils.h"
#include "ElementDlg.h"

using namespace FoxjetDatabase;
using namespace FoxjetUtils;
using namespace ListGlobals;
using namespace FoxjetElements;
using namespace FoxjetCommon;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExpDateDatabaseDlg dialog


CExpDateDatabaseDlg::CExpDateDatabaseDlg(ULONG lLineID, CWnd* pParent /*=NULL*/)
:	m_lLineID (lLineID),
	CDialog(CExpDateDatabaseDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CExpDateDatabaseDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CExpDateDatabaseDlg::~CExpDateDatabaseDlg ()
{
	COdbcDatabase::FreeTables (m_vTables);
}

void CExpDateDatabaseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExpDateDatabaseDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate)
		UpdateKeyField ();

	DDX_Text (pDX, TXT_KEYVALUE, m_strKeyValue);
	DDV_MinMaxChars (pDX, m_strKeyValue, TXT_KEYVALUE, CDatabaseElement::m_lmtKey);

	DDX_Check (pDX, CHK_PROMPT, m_bPromptAtTaskStart);
}


BEGIN_MESSAGE_MAP(CExpDateDatabaseDlg, CDialog)
	//{{AFX_MSG_MAP(CExpDateDatabaseDlg)
	ON_CBN_SELCHANGE(CB_DSN, OnSelchangeDsn)
	ON_CBN_SELCHANGE(CB_TABLE, OnSelchangeTable)
	ON_CBN_SELCHANGE(CB_FIELD, OnSelchangeField)
	ON_BN_CLICKED(BTN_SELECT, OnSelect)
	ON_BN_CLICKED(BTN_SELECTKEY, OnSelectkey)
	ON_BN_CLICKED(BTN_FORMAT, OnFormat)
	ON_BN_CLICKED(CHK_KEY, OnKey)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExpDateDatabaseDlg message handlers

BOOL CExpDateDatabaseDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if (CComboBox * pDSN = (CComboBox *)GetDlgItem (CB_DSN)) {
		CStringArray v;

		COdbcDatabase::GetDsnEntries (v);

		for (int i = 0; i < v.GetSize (); i++) 
			if (pDSN->FindString (-1, v [i]) == CB_ERR)
				pDSN->AddString (v [i]);

		int nIndex = pDSN->FindString (-1, m_strDSN);

		if (nIndex == CB_ERR)
			nIndex = pDSN->AddString (m_strDSN);

		pDSN->SetCurSel (nIndex);
		InitTables (m_strDSN, m_strTable, m_strField, m_strKeyField);
		
		if (m_strKeyField.GetLength ())
			if (CButton * p = (CButton *)GetDlgItem (CHK_KEY))
				p->SetCheck (1);

		OnKey ();
	}

	OnKey ();

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

CString CExpDateDatabaseDlg::GetCurSelDsn () const
{
	CString strResult;

	if (CComboBox * pDSN = (CComboBox *)GetDlgItem (CB_DSN)) 
		if (pDSN->GetLBTextLen (pDSN->GetCurSel ()) > 0)
			pDSN->GetLBText (pDSN->GetCurSel (), strResult);

	return strResult;
}

void CExpDateDatabaseDlg::OnSelchangeDsn() 
{
	COdbcCache db = COdbcDatabase::Find (GetCurSelDsn ());
	CString strTable = m_strTable;

	BeginWaitCursor ();
	COdbcDatabase::FreeTables (m_vTables);
	db.GetDB ().CopyTables (m_vTables);
	EndWaitCursor ();

	if (m_vTables.GetSize ())
		if (COdbcTable * p = (COdbcTable *)m_vTables [0]) 
			strTable = p->GetName ();

	for (int i = 0; i < m_vTables.GetSize (); i++) {
		if (COdbcTable * p = (COdbcTable *)m_vTables [i]) {
			if (!m_strTable.CompareNoCase (p->GetName ())) {
				strTable = p->GetName ();
				break;
			}
		}
	}

	InitTables (GetCurSelDsn (), strTable, GetCurSelField (), GetCurSelKeyField ());
}

void CExpDateDatabaseDlg::OnSelchangeTable() 
{
	CString strDSN, strTable;	
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);

	ASSERT (pTable);
	strDSN = GetCurSelDsn ();
	
	if (pTable->GetLBTextLen (pTable->GetCurSel ()) > 0)
		pTable->GetLBText (pTable->GetCurSel (), strTable);

	InitFields (strDSN, strTable);
//	SetDlgItemInt (TXT_ROW, 1);
}

void CExpDateDatabaseDlg::InitTables (const CString & strDSN, const CString & strTable, const CString & strField, 
									  const CString & strKeyField) 
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);

	BeginWaitCursor ();

	ASSERT (pTable);
	pTable->ResetContent ();

	if (strDSN.GetLength ()) {

		try {
			CString strTableTmp (strTable);
			COdbcCache db = COdbcDatabase::Find (strDSN);
			CString strType = CTableTypeDlg::GetParams (); 

			for (int i = 0; i < m_vTables.GetSize (); i++) {
				if (COdbcTable * p = (COdbcTable *)m_vTables [i]) {
					CString strFind = _T ("'") + p->GetType () + _T ("'");

					if (strType.Find (strFind) != -1) {
						CString strName = p->GetName ();
						int nIndex = pTable->AddString (strName);

						if (!strName.CompareNoCase (strTable)) 
							pTable->SetCurSel (nIndex);
					}
				}
			}

			if (pTable->GetCurSel () == CB_ERR) {
				//pTable->SetCurSel (0);
				//pTable->GetLBText (0, strTableTmp);
				pTable->SetCurSel (pTable->AddString (strTableTmp));
			}

			InitFields (strDSN, strTableTmp, strField, strKeyField);
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	EndWaitCursor ();
}

void CExpDateDatabaseDlg::InitFields (const CString & strDSN, const CString & strTable, const CString & strField,
									  const CString & strKeyField) 
{
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	CComboBox * pKeyField = (CComboBox *)GetDlgItem (CB_KEYFIELD);

	ASSERT (pField);
	ASSERT (pKeyField);

	BeginWaitCursor ();

	pField->ResetContent ();
	pKeyField->ResetContent ();

	if (strDSN.GetLength ()) {

		try {
			COdbcCache db = COdbcDatabase::Find (strDSN);
			COdbcRecordset rst (db.GetDB ());
			CString strSQL;

			strSQL.Format (_T ("SELECT * FROM [%s]"), strTable);

			if (rst.Open (strSQL, CRecordset::forwardOnly)) {
				for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
					CODBCFieldInfo info;

					rst.GetODBCFieldInfo (i, info);
					CString strName = info.m_strName;
					int nIndex = pField->AddString (strName);

					pField->SetItemData (nIndex, info.m_nSQLType);

					if (!strName.CompareNoCase (strField))
						pField->SetCurSel (nIndex);

					nIndex = pKeyField->AddString (strName);
					pKeyField->SetItemData (nIndex, info.m_nSQLType);

					if (!strName.CompareNoCase (strKeyField))
						pKeyField->SetCurSel (nIndex);
				}

				rst.Close ();

				if (pField->GetCurSel () == CB_ERR) {
					pField->SetCurSel (0);
					SetDlgItemInt (TXT_ROW, 1);
				}

				if (pKeyField->GetCurSel () == CB_ERR) 
					pKeyField->SetCurSel (0);

				OnSelchangeField ();
				UpdateKeyField ();
			}
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	EndWaitCursor ();
}

CString CExpDateDatabaseDlg::GetCurSelField ()
{
	CString strResult;

	if (CComboBox * p= (CComboBox *)GetDlgItem (CB_FIELD)) 
		if (p->GetLBTextLen (p->GetCurSel ()) > 0)
			p->GetLBText (p->GetCurSel (), strResult);

	return strResult;
}

CString CExpDateDatabaseDlg::GetCurSelKeyField ()
{
	CString strResult;

	if (CComboBox * p= (CComboBox *)GetDlgItem (CB_KEYFIELD)) 
		if (p->GetLBTextLen (p->GetCurSel ()) > 0)
			p->GetLBText (p->GetCurSel (), strResult);

	return strResult;
}

void CExpDateDatabaseDlg::OnSelchangeField() 
{
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	bool bEnable = false;

	ASSERT (pField);

	switch (pField->GetItemData (pField->GetCurSel ())) {
		case SQL_SMALLINT:
		case SQL_INTEGER:
		case SQL_REAL:	
		case SQL_FLOAT:	
		case SQL_DOUBLE:	
		case SQL_DATE:	
		case SQL_TIME:	
		case SQL_TIMESTAMP:
		bEnable = true;
	}

	if (CWnd * p = GetDlgItem (BTN_FORMAT))
		p->EnableWindow (bEnable);
}

void CExpDateDatabaseDlg::UpdateKeyField() 
{
	CComboBox * pKeyField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CString strKeyField, strKeyValue;

	ASSERT (pKeyField);

	GetDlgItemText (TXT_KEYVALUE, strKeyValue);

//	if (!strKeyValue.GetLength ()) 
//		return;

	int nIndex = pKeyField->GetCurSel ();
	
	if (pKeyField->GetLBTextLen (nIndex) > 0)
		pKeyField->GetLBText (nIndex, strKeyField);

	m_nSQLType = pKeyField->GetItemData (nIndex);
}

void CExpDateDatabaseDlg::OnSelect() 
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	CInternalFormatting opt (false);
	CDbFieldDlg dlg (defElements.m_strRegSection, this);

	ASSERT (pTable);
	ASSERT (pField);

	dlg.m_strDSN = GetCurSelDsn ();

	if (pTable->GetLBTextLen (pTable->GetCurSel ()) > 0)
		pTable->GetLBText (pTable->GetCurSel (), dlg.m_strTable);
	
	if (pField->GetLBTextLen (pField->GetCurSel ()) > 0)
		pField->GetLBText (pField->GetCurSel (), dlg.m_strField);

	dlg.m_nRow = 0; //GetDlgItemInt (TXT_ROW);
	dlg.m_nMaxRows = 100;
	
	if (dlg.DoModal () == IDOK) {
		pField->SetCurSel (pField->FindStringExact (-1, dlg.m_strField));
		OnSelchangeField ();
//		SetDlgItemInt (TXT_ROW, 0); //dlg.m_nRow);
	}
}

void CExpDateDatabaseDlg::OnSelectkey() 
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CInternalFormatting opt (false);
	CDbFieldDlg dlg (defElements.m_strRegSection, this);

	ASSERT (pTable);
	ASSERT (pField);

	dlg.m_strDSN = GetCurSelDsn ();

	if (pTable->GetLBTextLen (pTable->GetCurSel ()) > 0)
		pTable->GetLBText (pTable->GetCurSel (), dlg.m_strTable);
	
	if (pField->GetLBTextLen (pField->GetCurSel ()) > 0)
		pField->GetLBText (pField->GetCurSel (), dlg.m_strField);

	dlg.m_nRow = 0; //GetDlgItemInt (TXT_ROW);
	dlg.m_nMaxRows = 100;
	
	if (dlg.DoModal () == IDOK) {
		pField->SetCurSel (pField->FindStringExact (-1, dlg.m_strField));
		OnSelchangeField ();
//		SetDlgItemInt (TXT_ROW, 0); //dlg.m_nRow);
		SetDlgItemText (TXT_KEYVALUE, dlg.m_strSelected);
		UpdateKeyField ();
	}
}

void CExpDateDatabaseDlg::OnFormat() 
{
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	bool bEnable = false;
	CDbFormatDlg dlg (m_lLineID, this);

	ASSERT (pField);

	dlg.m_nType		= m_nFormat;
	dlg.m_strFormat = m_strFormat;

	if (dlg.DoModal () == IDOK) {
		m_nFormat	= dlg.m_nType;
		m_strFormat = dlg.m_strFormat;
	}
}

void CExpDateDatabaseDlg::OnKey() 
{
	bool bUse = CElementDlg::GetCheck (this, CHK_KEY);
	UINT nID [] =
	{
		CB_KEYFIELD,
		TXT_KEYVALUE,
		BTN_SELECTKEY,
		CHK_PROMPT,
		//CHK_SERIAL,
	};

	for (int i = 0; i < ARRAYSIZE (nID); i++)
		if (CWnd * p = GetDlgItem (nID [i]))
			p->EnableWindow (bUse);	

	if (CButton * p = (CButton *)GetDlgItem (CHK_PROMPT)) {
		p->EnableWindow (bUse);	
		p->SetCheck (bUse ? (m_bPromptAtTaskStart ? 1 : 0): 0);
	}

	/*
	if (CButton * p = (CButton *)GetDlgItem (CHK_SERIAL)) {
		p->EnableWindow (bUse);	
		p->SetCheck (bUse ? (m_bSerial ? 1 : 0): 0);
	}

	if (CButton * p = (CButton *)GetDlgItem (CHK_SERIALDOWNLOAD)) {
		p->EnableWindow (bUse);	
		p->SetCheck (bUse ? (m_bSerialDownload ? 1 : 0): 0);
	}
	*/
}

void CExpDateDatabaseDlg::OnOK()
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	CComboBox * pKeyField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CComboBox * pDSN = (CComboBox *)GetDlgItem (CB_DSN);

	ASSERT (pTable);
	ASSERT (pField);
	ASSERT (pKeyField);
	ASSERT (pDSN);

	int nIndex = pDSN->GetCurSel ();

	if (nIndex == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NODSNSELECTED));
		return;
	}

	pDSN->GetLBText (nIndex, m_strDSN);

	nIndex = pTable->GetCurSel ();

	if (nIndex == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOTABLESELECTED));
		return;
	}

	pTable->GetLBText (nIndex, m_strTable);

	nIndex = pField->GetCurSel ();

	if (nIndex == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOFIELDSELECTED));
		return;
	}

	pField->GetLBText (nIndex, m_strField);

	if (CElementDlg::GetCheck (this, CHK_KEY)) {
		nIndex = pKeyField->GetCurSel ();

		if (nIndex != CB_ERR) 
			pKeyField->GetLBText (nIndex, m_strKeyField);
	}
	else {
		m_strKeyField = _T ("");
		SetDlgItemText (TXT_KEYVALUE, m_strKeyValue = _T (""));
	}

	UpdateKeyField ();

	if (CWnd * p = GetDlgItem (BTN_FORMAT))
		if (!p->IsWindowEnabled ())
			m_nFormat = CDbFormatDlg::DEFAULT;

	CDialog::OnOK ();
}

