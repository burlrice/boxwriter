// TextElement.cpp: implementation of the CTextElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <math.h>

#include "TextElement.h"
#include "List.h"
#include "ElementDefaults.h"
#include "Coord.h"
#include "Edit.h"
#include "Debug.h"
#include "Color.h"
#include "Database.h"
#include "Extern.h"
#include "TemplExt.h"
#include "Utils.h"
#include "Parse.h"
#include "BitmappedFont.h"
#include "ximage.h"
#include "BarcodeElement.h"
#include "RawBitmap.h"
#include "DatabaseElement.h"
#include "Parse.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace Color;
using namespace FoxjetElements;
using namespace FoxjetElements::TextElement;
using namespace FoxjetCommon::ElementFields;

extern CArray <const CBitmappedFont  *, const CBitmappedFont *> vBitmappedFonts; 

static CMap <DWORD, DWORD, int, int> vFontSizes;
static const LPCTSTR lpszParaDelim [] = 
{
	_T ("<CR>"),
	_T ("\r\n"),
};

////////////////////////////////////////////////////////////////////////////////

typedef struct
{
	CString m_str;
	int m_nLen;
} PARAGRAPHSTRUCT;

////////////////////////////////////////////////////////////////////////////////

static int FindBitmappedFont (const CString & strFont)
{
	for (int i = 0; i < ::vBitmappedFonts.GetSize (); i++)
		if (const CBitmappedFont * p = ::vBitmappedFonts [i])
			if (!p->GetName ().CompareNoCase (strFont))
				return i;

	return -1;
}

////////////////////////////////////////////////////////////////////////////////


IMPLEMENT_DYNAMIC (CTextElement, CBaseTextElement);

const FoxjetCommon::LIMITSTRUCT CTextElement::m_lmtFontSize			= { 5,		384					};
const FoxjetCommon::LIMITSTRUCT CTextElement::m_lmtString			= { 1,		lmtString.m_dwMax	};
const FoxjetCommon::LIMITSTRUCT CTextElement::m_lmtParagraphGap		= { -32,	32					};
const FoxjetCommon::LIMITSTRUCT CTextElement::m_lmtParagraphWidth	= { 1000,	ULONG_MAX			};
const FoxjetCommon::LIMITSTRUCT CTextElement::m_lmtWidth			= { 0,		512					};
const FoxjetCommon::LIMITSTRUCT CTextElement::m_lmtGap				= { 0,		99					};

CTextElement::CTextElement(const FoxjetDatabase::HEADSTRUCT & head)
: 	m_strDefaultData (_T ("Text element")),
	m_orient (NORMAL),
	m_hFont (NULL),
	m_hPrinterFont (NULL),
	m_nHeight (0),
	m_nWidth (0),
	m_size (0, 0),
	m_sizeParagraph (0, 0),
	m_bResizable (false),
	m_alignPara (ALIGNMENT_LEFT),
	m_nParagraphGap (0),
	m_lLink (0),
	CBaseTextElement (head)
{
	SetFont (CPrinterFont ());
}

CTextElement::CTextElement (const CTextElement & rhs)
: 	m_strDefaultData (rhs.m_strDefaultData),
	m_orient (rhs.m_orient),
	m_hFont (NULL),
	m_hPrinterFont (NULL),
	m_size (rhs.m_size),
	m_sizeParagraph (rhs.m_sizeParagraph),
	m_bResizable (rhs.m_bResizable),
	m_nHeight (0),
	m_nWidth (rhs.m_nWidth),
	m_alignPara (rhs.m_alignPara),
	m_nParagraphGap (rhs.m_nParagraphGap),
	m_lLink (rhs.m_lLink),
	CBaseTextElement (rhs)
{
	SetFont (rhs.GetFont ());
}

CTextElement & CTextElement::operator = (const CTextElement & rhs)
{
	CBaseTextElement::operator = (rhs);

	if (this != &rhs) {
		m_strDefaultData	= rhs.m_strDefaultData;
		m_orient			= rhs.m_orient;
		m_size				= rhs.m_size;
		m_nWidth			= rhs.m_nWidth;
		m_alignPara			= rhs.m_alignPara;
		m_nParagraphGap		= rhs.m_nParagraphGap;
		m_bResizable		= rhs.m_bResizable;
		m_sizeParagraph		= rhs.m_sizeParagraph;
		m_lLink				= rhs.m_lLink;
		SetFont (rhs.GetFont ());
	}

	return * this;
}


CTextElement::~CTextElement()
{
	SetHFont (NULL);
	SetPrinterHFont (NULL);
}

// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
static const ElementFields::FIELDSTRUCT fields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType,		_T ("")				),	// 1	// Prefix (required)
	ElementFields::FIELDSTRUCT (m_lpszID,				_T ("")				),	// 2	// ID (required)
	ElementFields::FIELDSTRUCT (m_lpszX,				_T ("")				),	// 3	// x (required)
	ElementFields::FIELDSTRUCT (m_lpszY,				_T ("")				),	// 4	// y (required)
	ElementFields::FIELDSTRUCT (m_lpszFlipH,			_T ("0")			),	// 5	// flip h
	ElementFields::FIELDSTRUCT (m_lpszFlipV,			_T ("0")			),	// 6	// flip v
	ElementFields::FIELDSTRUCT (m_lpszInverse,			_T ("0")			),	// 7	// inverse
	ElementFields::FIELDSTRUCT (m_lpszFontName,			_T ("Courier New")	),	// 8	// font name
	ElementFields::FIELDSTRUCT (m_lpszFontSize,			_T ("32")			),	// 9	// font size
	ElementFields::FIELDSTRUCT (m_lpszBold,				_T ("0")			),	// 10	// bold
	ElementFields::FIELDSTRUCT (m_lpszItalic,			_T ("0")			),	// 11	// italic
	ElementFields::FIELDSTRUCT (m_lpszData,				_T ("Text Element")	),	// 12	// data
	ElementFields::FIELDSTRUCT (m_lpszOrient,			_T ("0")			),	// 13	// orientation
	ElementFields::FIELDSTRUCT (m_lpszWidth,			_T ("1")			),	// 14	// font width
	ElementFields::FIELDSTRUCT (m_lpszParaAlign,		_T ("0")			),	// 15	// paragraph alignment
	ElementFields::FIELDSTRUCT (m_lpszParaGap,			_T ("0")			),	// 16	// paragraph gap
	ElementFields::FIELDSTRUCT (m_lpszResizable,		_T ("0")			),	// 17	// resizable
	ElementFields::FIELDSTRUCT (m_lpszParaWidth,		_T ("0")			),	// 18	// paragraph width
	ElementFields::FIELDSTRUCT (m_lpszLink,				_T ("0")			),	// 19	// linked to (barcode)
	ElementFields::FIELDSTRUCT (m_lpszAlign,			_T ("0")			),	// 20	//  alignment
	ElementFields::FIELDSTRUCT (m_lpszDither,			_T ("0")			),	// 21	//  dither
	ElementFields::FIELDSTRUCT (m_lpszColor,			_T ("")				),	// 22	//  color
	ElementFields::FIELDSTRUCT (),
};

CString CTextElement::ToString (const FoxjetCommon::CVersion & ver) const
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated

	CString str;
	CPrinterFont f = GetFont ();
	CString strData = GetDefaultData ();
	CPoint pt = GetPosInternal (ALIGNMENT_ADJUST_ADD);

	str.Format (_T ("{Text,%u,%u,%u,%d,%d,%d,%s,%u,%d,%d,%s,%u,%d,%d,%d,%d,%d,%d,%d,%d,%06X}"),
		GetID (),		
		pt.x, pt.y, 
		IsFlippedH (),
		IsFlippedV (),
		IsInverse (),
		f.m_strName, f.m_nSize, f.m_bBold, f.m_bItalic,
		FormatString (strData),
		(int)GetOrientation (),
		GetWidth (),
		GetParaAlign (),
		GetParaGap (),
		IsResizable (),
		GetParaWidth (),
		GetLink (),
		GetAlignment (),
		GetDither (),
		GetColor ());

	return str;
}

bool CTextElement::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	CStringArray vstrToken;
	CPrinterFont f;

	Parse (str, ver, vstrToken);

	if (vstrToken [0].CompareNoCase (_T ("Text")) != 0){
		TRACEF ("Wrong prefix \"" + vstrToken [0] + "\"");
		return false;
	}

	f.m_strName = vstrToken [7];
	f.m_nSize = (UINT)_tcstol (vstrToken [8], NULL, 10);
	f.m_bBold = _ttoi (vstrToken [9]) ? true : false;
	f.m_bItalic = _ttoi (vstrToken [10]) ? true : false;

	bool bResult = true;

	CPoint pt = CPoint (
		(UINT)_tcstol (vstrToken [2], NULL, 10), 
		(UINT)_tcstol (vstrToken [3], NULL, 10));

	bResult &= SetID ((UINT)_tcstol (vstrToken [1], NULL, 10));
	bResult &= SetPos (pt, NULL);
	bResult &= SetFlippedH (_ttoi (vstrToken [4]) ? true : false);
	bResult &= SetFlippedV (_ttoi (vstrToken [5]) ? true : false);
	bResult &= SetInverse (_ttoi (vstrToken [6]) ? true : false);
	bResult &= SetFont (f);
	bResult &= SetDefaultData (UnformatString (vstrToken [11]));
	bResult &= SetOrientation ((FONTORIENTATION)_tcstol (vstrToken [12], NULL, 10));
	bResult &= SetWidth (_ttoi (vstrToken [13]));
	bResult &= SetParaAlign ((ALIGNMENT)_ttoi (vstrToken [14]));
	bResult &= SetParaGap (_ttoi (vstrToken [15]));
	bResult &= SetResizable (_ttoi (vstrToken [16]) ? true : false);
	bResult &= SetParaWidth (_ttoi (vstrToken [17]));
	bResult &= SetLink (_ttoi (GetParam (vstrToken, 18)));
	bResult &= SetAlignment ((ALIGNMENT)_ttoi (vstrToken [19]));
	bResult &= SetDither (_ttoi (vstrToken [20]));
	bResult &= SetColor (_tcstoul (vstrToken [21], NULL, 16));

	return bResult;
}

bool CTextElement::SetParaWidth (LONG l)
{
	double dStretch [2] = { 1, 1 };
	
	CBaseElement::CalcStretch (GetHead (), dStretch);
	m_sizeParagraph.cx = (LONG)((double)l * dStretch [0]);
	SetRedraw ();

	return true;
}

LONG CTextElement::GetParaWidth () const
{
	double dStretch [2] = { 1, 1 };
	
	CBaseElement::CalcStretch (GetHead (), dStretch);

	return (LONG)((double)m_sizeParagraph.cx / dStretch [0]);
}

int CTextElement::GetWidth () const
{
	return m_nWidth;
}

bool CTextElement::SetWidth (int nWidth)
{
	if (!FoxjetCommon::IsValid (nWidth, FoxjetCommon::IsBitmappedFont (GetFontName ()) ? m_lmtGap : m_lmtWidth))
		return false;

	m_nWidth = (nWidth > 0) ? nWidth : 0;
	SetRedraw (true);
	SetFont (GetFont ());

	return true;
}

bool CTextElement::IsParagraphMode () const
{
	return IsParagraphMode (GetImageData ());
}

bool CTextElement::IsParagraphMode (const CString & str)
{
	for (int i = 0; i < ARRAYSIZE (::lpszParaDelim); i++)
		if (str.Find (::lpszParaDelim [i]) != -1)
			return true;

	return false;
}

ALIGNMENT CTextElement::GetParaAlign () const
{
	return m_alignPara;
}

ALIGNMENT CTextElement::GetParaAlign (int) const
{
//	if (m_orient == VERTICAL)
//		return ALIGNMENT_LEFT;

	bool bInvert = 
		(IsFlippedH () && GetOrientation (EDITOR) == NORMAL) ||
		(IsFlippedV () && GetOrientation (EDITOR) == VERTICAL);

	if (bInvert) {
		switch (m_alignPara) {
		case ALIGNMENT_LEFT:	return ALIGNMENT_RIGHT;
		case ALIGNMENT_RIGHT:	return ALIGNMENT_LEFT;
		}
	}

	return m_alignPara;
}

bool CTextElement::SetParaAlign (ALIGNMENT align)
{
	m_alignPara = align;
	SetRedraw (true);
	return true;
}

int CTextElement::GetParaGap () const
{
	return m_nParagraphGap;
}

bool CTextElement::SetParaGap (int nGap)
{
	if (IsValid (nGap, m_lmtParagraphGap)) {
		m_nParagraphGap = nGap;
		return true;
	}

	return false;
}

CString CTextElement::Convert (const CString & strInput)
{
	CString str;
	int nLen = max (1, strInput.GetLength ()) + 1;
	TCHAR * psz = new TCHAR [nLen];
	nLen = Unformat (strInput, psz);

	for (int i = 0; i < nLen; i++)
		str += psz [i];

	delete [] psz;

	for (int i = 0; i < ARRAYSIZE (::lpszParaDelim); i++) 
		str.Replace (::lpszParaDelim [i], _T ("\n"));

	//TRACEF (strInput);
	//TRACEF (str);

	return str;
}

int CTextElement::CalcLen (const CString & str, CDC & dc)
{
	int nBitmapIndex = FindBitmappedFont (GetFont ().m_strName);
	const CPrinterFont f = GetFont ();
	UINT nSize = f.m_nSize;
	CRect rc (0, 0, 0, 0);

	if (nBitmapIndex != -1) {
		if (const CBitmappedFont * p = ::vBitmappedFonts [nBitmapIndex]) {
			for (int i = 0; i < str.GetLength (); i++) {
				rc.right += (p->DrawChar (str [i]) * 2);

				if ((i + 1) < str.GetLength ())
					rc.right += (GetWidth () * 2);
			}
		}
	}
	else {
		CFont fnt;
		CDC dcMem;

		VERIFY (dcMem.CreateCompatibleDC (&dc));
		VERIFY (fnt.Attach (CreateFont (f.m_strName, nSize, GetWidth (), f.m_bBold, f.m_bItalic, NORMAL)));
		CFont * pFont = dcMem.SelectObject (&fnt);
	
		dcMem.DrawText (str, &rc, DT_CALCRECT | DT_NOPREFIX);

		dcMem.SelectObject (pFont);
	}

	return rc.Width ();
}

CString CTextElement::Tokenize (const CString & strInput, CStringArray & v, CDC & dc)
{
	CString strResult;
	CString str = Convert (strInput);
	const HEADSTRUCT & head = GetHead ();
	double dStretch [2] = { 1, 1 };

	CBaseElement::CalcStretch (head, dStretch);

	if (IsParaWidthEnabled ()) {
		if (m_sizeParagraph.cx > 0) {
			CStringArray vTmp;
			int nBitmapIndex = FindBitmappedFont (GetFont ().m_strName);
			bool bTrueType = (nBitmapIndex == -1);
			int nLimit = ThousandthsToLogical (m_sizeParagraph, head).x;
			const CPrinterFont f = GetFont ();
			UINT nSize = f.m_nSize;
			CFont fnt;
			CDC dcMem;

			VERIFY (dcMem.CreateCompatibleDC (&dc));
			VERIFY (fnt.Attach (CreateFont (f.m_strName, nSize, GetWidth (), f.m_bBold, f.m_bItalic, NORMAL)));

			str.Replace (_T ("\n"), _T (" "));
			str.TrimLeft (); str.TrimRight ();

			CFont * pFont = dcMem.SelectObject (&fnt);

			CString strTmp = str;

			while (strTmp.GetLength ()) {
				bool bTrim = true;
				CString strSegment = strTmp;
				int nCount = 0;

				do {
					CRect rc (0, 0, 0, 0);
	
					if (bTrueType)
						dcMem.DrawText (strSegment, &rc, DT_CALCRECT | DT_NOPREFIX);
					else {
						ASSERT (nBitmapIndex >= 0 && nBitmapIndex < ::vBitmappedFonts.GetSize ());

						if (const CBitmappedFont * p = ::vBitmappedFonts [nBitmapIndex]) {
							for (int i = 0; i < strSegment.GetLength (); i++) {
								rc.right += (p->DrawChar (strSegment [i]) * 2);

								if ((i + 1) < strSegment.GetLength ())
									rc.right += (GetWidth () * 2);
							}
						}
					}
					
					if (bTrim = (rc.Width () > nLimit)) {
						while (strSegment.GetLength () > 0 && !_istspace (strSegment [strSegment.GetLength () - 1])) {
							strSegment.Delete (strSegment.GetLength () - 1);
							nCount++;
						}
						while (strSegment.GetLength () > 0 && _istspace (strSegment [strSegment.GetLength () - 1])) {
							strSegment.Delete (strSegment.GetLength () - 1);
							nCount++;
						}
					}
				}
				while (bTrim);
				
				int nHeight = (nSize * (vTmp.GetSize () + 1)) + ((vTmp.GetSize ()) * m_nParagraphGap);
				int nMax = head.GetChannels ();

				if (nHeight > nMax) {
					strTmp.Empty ();
					break;
				}
				
				strTmp = strTmp.Mid (strSegment.GetLength ());
				strTmp.TrimLeft (); strTmp.TrimRight ();

				strSegment.TrimLeft (); strSegment.TrimRight ();
				vTmp.Add (strSegment);

			}

			dcMem.SelectObject (pFont);

			if (vTmp.GetSize ()) {
				CString strTmp = vTmp [0];
				int nMin = CBaseElement::ThousandthsToLogical (CSize (CTextElement::m_lmtParagraphWidth.m_dwMin, 0), head).x;

				strTmp.TrimLeft (); strTmp.TrimRight ();
				nMin = (int)((double)nMin * dStretch [0]);
				nMin = CBaseElement::LogicalToThousandths (CSize (nMin, 0), head).x;

				if (!strTmp.GetLength ()) {
					CString strWord;

					for (int i = 0; i < str.GetLength () && !_istspace (str [i]); i++) 
						strWord += str [i];

					int nCalc = CalcLen (strWord, dc);

					nCalc = CBaseElement::LogicalToThousandths (CSize (nCalc, 0), head).x;
					nMin = max (nMin, nCalc);
				}

				if (nMin > m_sizeParagraph.cx) 
					m_sizeParagraph.cx = nMin;						
			}

			{ 
				str.Empty ();
				
				for (int i = 0; i < vTmp.GetSize (); i++) {
					str += vTmp [i];

					if ((i + 1) < vTmp.GetSize ())
						str += '\n';
				}
			}
		}
	}

	{
		//TRACEF (strInput);
		//TRACEF (str);

		//const TCHAR cDelim = '\n';
		CLongArray vDelim = MergeAcending (CountChars (str, 13), CountChars (str, '\n'));
		int nIndex = 0;

		v.RemoveAll ();

		for (int i = 0; i < vDelim.GetSize (); i++) {
			int nLen = vDelim [i] - nIndex;
			v.Add (str.Mid (nIndex, nLen));
			nIndex = vDelim [i] + 1;
		}

		CString strRemainder = str.Mid (nIndex);

		if (strRemainder.GetLength ())
			v.Add (strRemainder);
		else {
			if (strInput.GetLength ()) {
				TCHAR c = strInput [strInput.GetLength () - 1];

				if (strInput.GetLength () && (c == '\n' || c == 13))
					v.Add (_T (""));
			}
		}
	}

	if (v.GetSize () > 1) {
		for (int i = v.GetSize () - 1; i >= 0; i--) {
			v [i].Trim ();

			if (v [i].GetLength () == 0)
				v.RemoveAt (i);
			else 
				break;
		}
	}

	//#ifdef _DEBUG
	//for (int i = 0; i < v.GetSize (); i++) 
	//	TRACEF (FoxjetDatabase::ToString (i) + _T (": \"") + v [i] + _T ("\""));
	//#endif

	for (int i = 0; i < v.GetSize (); i++) {
		strResult += v [i];

		//TRACEF (_T ("[") + FoxjetDatabase::ToString (i) + _T ("] ") + v [i]);

		if (i < (v.GetSize () - 1))
			strResult += '\n';
	}

	return strResult;
}

void CTextElement::DrawText (CDC & dc, const CStringArray & v, const CRect & rc, int nFormat, const CPrinterFont & f)
{
	int y = 0;
	CDC dcMem;
	CBitmap bmpMem;
	CFont fnt;
	UINT nSize = f.m_nSize;

	/* TODO: rem
	CStringArray v;
	CString str (strDraw);

	str.Replace (_T ("\r"), _T ("\n"));
	
//	FoxjetDatabase::Tokenize (str, v, '\n');
	CTextElement::Tokenize (str, v, dc);
	*/

	VERIFY (fnt.Attach (CreateFont (f.m_strName, nSize, GetWidth (), f.m_bBold, f.m_bItalic, NORMAL)));
	
	dcMem.CreateCompatibleDC (&dc);
	bmpMem.CreateCompatibleBitmap (&dc, rc.Width (), f.m_nSize);
	CBitmap * pBitmap = dcMem.SelectObject (&bmpMem);
	CFont * pFont = dcMem.SelectObject (&fnt);

	for (int i = 0; i < v.GetSize (); i++) {
		CRect rcLine (0, 0, rc.Width (), f.m_nSize);
		CDC dcTmp;
		CBitmap bmpTmp;

		VERIFY (dcTmp.CreateCompatibleDC (&dcMem));
		VERIFY (bmpTmp.CreateCompatibleBitmap (&dcMem, rcLine.Width (), rcLine.Height ()));
		CBitmap * pTmp = dcTmp.SelectObject (&bmpTmp);

		::BitBlt (dcMem, 0, 0, rc.Width (), f.m_nSize, NULL, 0, 0, IsInverse () ? BLACKNESS : WHITENESS);
		dcMem.DrawText (v [i], rcLine, nFormat);
		::BitBlt (dcTmp, 0, 0, rcLine.Width (), rcLine.Height (), NULL, 0, 0, WHITENESS);
		dcTmp.BitBlt (0, 0, rcLine.Width (), rcLine.Height (), &dcMem, 0, 0, IsInverse () ? SRCINVERT : SRCCOPY);
		dc.BitBlt (0, y, rc.Width (), rcLine.Height (), &dcTmp, 0, 0, SRCCOPY);

		dcTmp.SelectObject (pTmp);
		/*
		#ifdef _DEBUG
		{
			CString str;
			str.Format (_T ("%d, %d (%d, %d) [%d, %d (%d, %d)] [%d]: %s"), 
				rcLine.left, rcLine.top, rcLine.Width (), rcLine.Height (),
				0, y, rc.Width (), rcLine.Height (), 
				i, v [i]);
			TRACEF (str);
		}
		#endif
		*/

		y += (f.m_nSize + m_nParagraphGap);
	}

	dcMem.SelectObject (pBitmap);
	dcMem.SelectObject (pFont);
}

/* TODO: rem
CSize CTextElement::DrawParagraphMode (const CString & strInput,
									   CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, 
									   bool bCalcSizeOnly,  
									   FoxjetCommon::DRAWCONTEXT context)
{
	DECLARETRACECOUNT (200);
	CRect rc (0, 0, 0, 0), rcSingle (0, 0, 0, 0);
	CStringArray v;
	const CString str = CTextElement::Tokenize (strInput, v, dc);
	const CPrinterFont f = GetFont ();
	UINT nSize = f.m_nSize;
	CFont fnt;
	CDC dcMem;
	UINT nFormat = DT_NOPREFIX;
	FONTORIENTATION orient = GetOrientation ();
	const bool bFlippedV = m_bFlippedV;
	const bool bFlippedH = m_bFlippedH;
	double dStretch [2] = { 1, 1 };

	CBaseElement::CalcStretch (head, dStretch);

	switch (GetParaAlign (0)) {
	case ALIGNMENT_LEFT:	nFormat |= DT_LEFT;		break;
	case ALIGNMENT_CENTER:	nFormat |= DT_CENTER;	break;
	case ALIGNMENT_RIGHT:	nFormat |= DT_RIGHT;	break;
	}

	MARKTRACECOUNT ();

	VERIFY (fnt.Attach (CreateFont (f.m_strName, nSize, GetWidth (), f.m_bBold, f.m_bItalic, NORMAL)));
	VERIFY (dcMem.CreateCompatibleDC (&dc));

	MARKTRACECOUNT ();

	CFont * pFont = dcMem.SelectObject (&fnt);

	dcMem.DrawText (str, &rcSingle, DT_SINGLELINE | DT_CALCRECT | nFormat);
	dcMem.DrawText (str, &rc, DT_CALCRECT | nFormat);
	rc.bottom += ((v.GetSize () - 1) * m_nParagraphGap);

	if (IsParaWidthEnabled () && v.GetSize () == 1) 
		rc.right = rc.left + CBaseElement::ThousandthsToLogical (m_sizeParagraph, head).x;

	const int nLineHeight = rcSingle.Height ();
	const CSize sizeCalc (rc.Width (), (v.GetSize () * nLineHeight) + ((v.GetSize () - 1) * m_nParagraphGap));
	CSize size (sizeCalc);
	const int nParaWidth = size.cx;

	//TRACEF (f.m_strName + _T (": ") + FoxjetDatabase::ToString ((int)f.m_nSize) + _T (" [") + FoxjetDatabase::ToString ((int)rcSingle.Height ()) + _T ("]"));

	MARKTRACECOUNT ();

	if (IsParaWidthEnabled ()) 
		if (m_sizeParagraph.cx)
			size.cx = CBaseElement::ThousandthsToLogical (m_sizeParagraph, head).x;

	if (orient == VERTICAL) {
		swap (size.cx, size.cy);

		if (IsKindOf (RUNTIME_CLASS (CDatabaseElement))) {
			if (IsResizable ()) {
				size.cx = CBaseElement::ThousandthsToLogical  (CPoint (GetParaWidth (), 0), head).x;

				if (context == PRINTER) {
					size.cx = (int)(double)size.cx / dStretch [1];
					size.cy = rc.Height ();
				}
			}
		}

		size.cy = BindTo (size.cy, 0L, (long)head.GetChannels ());

		if (context == PRINTER) 
			swap (m_bFlippedV, m_bFlippedH);
	}

	m_size = CBaseElement::LogicalToThousandths (size, head);

	if (IsKindOf (RUNTIME_CLASS (CDatabaseElement))) {
		if (orient == VERTICAL) {
			swap (size.cx, size.cy);
			swap (m_size.cx, m_size.cy);
		}
	}

	if (!bCalcSizeOnly) {
		CBitmap bmp;
		int x = 0, y = 0, cx = size.cx, cy = size.cy;

		if (IsParaWidthEnabled ()) 
			cx = sizeCalc.cx;

		MARKTRACECOUNT ();

		bmp.CreateCompatibleBitmap (&dc, size.cx, size.cy);

		CBitmap * pBmp = dcMem.SelectObject (&bmp);
		int nBkMode = dcMem.SetBkMode (TRANSPARENT);
		COLORREF rgbTextColor = dcMem.SetTextColor (IsInverse () ? rgbWhite : rgbBlack);
		COLORREF rgbBkColor = dcMem.SetBkColor (!IsInverse () ? rgbWhite : rgbBlack);

		MARKTRACECOUNT ();

		if (orient == VERTICAL) {
			CBitmap bmpNormal;
			CBitmap bmpRotate;
			CDC dcRotate;
			int y = 0;

			if (IsKindOf (RUNTIME_CLASS (CDatabaseElement))) {
				if (IsResizable ()) {
					int cx = (int)((double)size.cy / dStretch [1]) - nParaWidth;

					if (context == PRINTER)
						cx = size.cy - nParaWidth;

					switch (m_alignPara) {
					case ALIGNMENT_CENTER:
						rc += CSize (cx / 2.0, 0);
						break;
					case ALIGNMENT_RIGHT:
						rc += CSize (cx, 0);
						break;
					}
				}
			}

			MARKTRACECOUNT ();

			dcRotate.CreateCompatibleDC (&dc);
			
			{
				if (IsKindOf (RUNTIME_CLASS (CDatabaseElement))) 
					swap (size.cx, size.cy);
			
				bmpNormal.CreateCompatibleBitmap (&dc, size.cx, size.cy);

				CBitmap * pMem = dcMem.SelectObject (&bmpNormal);
				::BitBlt (dcMem, 0, 0, size.cx, size.cy, NULL, 0, 0, IsInverse () ? BLACKNESS : WHITENESS);
				DrawText (dcMem, str, &rc, nFormat, f);
				dcMem.SelectObject (pMem);

				if (IsKindOf (RUNTIME_CLASS (CDatabaseElement))) 
					swap (size.cx, size.cy);
			}

			MARKTRACECOUNT ();

			RotateRight90 (dc, bmpNormal, bmpRotate);

			MARKTRACECOUNT ();

			{
				int nShift = (context == EDITOR ? IsFlippedV () : IsFlippedH ()) ? (sizeCalc.cx - size.cy) : 0;
				CBitmap * pRotate = dcRotate.SelectObject (&bmpRotate);
				dcMem.BitBlt (0, 0, size.cx, size.cy, &dcRotate, 0, nShift, SRCCOPY);
				dcRotate.SelectObject (pRotate);
			}

			{
				CSize result = (context == EDITOR) ? size : CSize (size.cy, size.cx);

				cx = result.cx;
				cy = result.cy;

				if (context == EDITOR) 
					cx = (int)((double)cx / dStretch [1]);
			}

			MARKTRACECOUNT ();
		}
		else {
			MARKTRACECOUNT ();

			::BitBlt (dcMem, 0, 0, size.cx, size.cy, NULL, 0, 0, IsInverse () ? BLACKNESS : WHITENESS);
			DrawText (dcMem, str, &rc, nFormat, f);
		}

		#ifdef __WYSIWYGFIX__
		if (context == EDITOR) {
			if (m_size.cx) {
				CRect rcAdj = GetWindowRect (head); 
				CSize sizeAdj = ThousandthsToLogical (rcAdj.Size (), head);

				if (orient == NORMAL) {
					double dAdj = (double)sizeAdj.cx / (double)rc.Width ();

					x = (int)((double)x * dAdj);
					cx = (int)((double)cx * dAdj);
				}
				else 
					cx = sizeAdj.cx;
			}
		}
		#endif

		if (IsFlippedV ()) {
			y += (cy - 1);
			cy *= -1;
		}
		if (IsFlippedH ()) {
			x += (cx - 1);
			cx *= -1;
		}

		if (context == EDITOR) {
			MARKTRACECOUNT ();

			dc.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, size.cx, size.cy, SRCCOPY); 

			MARKTRACECOUNT ();
		}
		else {
			CBitmap bmpPrinter;
			CBitmap bmpEditor;
			CDC dcEditor;

			dcEditor.CreateCompatibleDC (&dc);
			
			bmpEditor.CreateCompatibleBitmap (&dc, size.cx, size.cy);
			
			MARKTRACECOUNT ();

			{
				CBitmap * pBmp = dcEditor.SelectObject (&bmpEditor);

				//SAVEBITMAP (dcMem, _T ("C:\\Temp\\Debug\\dcMem [PRINTER].bmp"));

				if (orient == VERTICAL) {
					::BitBlt (dcEditor, 0, 0, size.cx, size.cy, NULL, 0, 0, WHITENESS); 
					dcEditor.StretchBlt (y, x, cy, cx, &dcMem, 0, 0, size.cx, size.cy, SRCCOPY); 
					dcEditor.BitBlt (0, 0, size.cx, size.cy, &dcEditor, 0, 0, DSTINVERT);
				}
				else {
					CSize blt = rc.Size ();

					if (IsParaWidthEnabled ()) {
						blt.cx = size.cx;

						if (IsFlippedH ()) {
							int nDiff = max (0, size.cx - sizeCalc.cx);

							x += nDiff;
						}
					}

					::BitBlt (dcEditor, 0, 0, blt.cx, blt.cy, NULL, 0, 0, IsInverse () ? BLACKNESS : WHITENESS); 
					dcEditor.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, rc.Width (), rc.Height (), SRCCOPY); 
					dcEditor.BitBlt (0, 0, size.cx, size.cy, &dcEditor, 0, 0, DSTINVERT);
				}

				dcEditor.SelectObject (pBmp);
			}

			MARKTRACECOUNT ();

			dcMem.SelectObject (pBmp);
			RotateLeft90AndFlip (dc, bmpEditor, bmpPrinter);

			MARKTRACECOUNT ();

			dcMem.SelectObject (&bmpPrinter);

			dc.StretchBlt (0, 0, size.cy, size.cx, &dcMem, 0, 0, size.cy, size.cx, SRCCOPY); 
		}

		dcMem.SetBkMode (nBkMode);
		dcMem.SelectObject (pBmp);
		dcMem.SetTextColor (rgbTextColor);
		dcMem.SetBkColor (rgbBkColor);
	}

	MARKTRACECOUNT ();

	dcMem.SelectObject (pFont);

	m_bFlippedV = bFlippedV;
	m_bFlippedH = bFlippedH;

	//TRACECOUNTARRAY ();

	CSize result = (context == EDITOR) ? m_size : CSize (size.cy, size.cx);

	return result;
}
*/
CSize CTextElement::DrawParagraphMode (const CString & strInput,
									   CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, 
									   bool bCalcSizeOnly,  
									   FoxjetCommon::DRAWCONTEXT context)
{
	DECLARETRACECOUNT (200);
	CRect rc (0, 0, 0, 0), rcSingle (0, 0, 0, 0);
	CStringArray v;
	const CString str = CTextElement::Tokenize (strInput, v, dc);
	const CPrinterFont f = GetFont ();
	UINT nSize = f.m_nSize;
	CFont fnt;
	CDC dcMem;
	UINT nFormat = DT_NOPREFIX;
	FONTORIENTATION orient = GetOrientation ();
	const bool bFlippedV = m_bFlippedV;
	const bool bFlippedH = m_bFlippedH;
	double dStretch [2] = { 1, 1 };

	CBaseElement::CalcStretch (head, dStretch);

	switch (GetParaAlign (0)) {
	case ALIGNMENT_LEFT:	nFormat |= DT_LEFT;		break;
	case ALIGNMENT_CENTER:	nFormat |= DT_CENTER;	break;
	case ALIGNMENT_RIGHT:	nFormat |= DT_RIGHT;	break;
	}

	MARKTRACECOUNT ();

	VERIFY (fnt.Attach (CreateFont (f.m_strName, nSize, GetWidth (), f.m_bBold, f.m_bItalic, NORMAL)));
	VERIFY (dcMem.CreateCompatibleDC (&dc));

	MARKTRACECOUNT ();

	CFont * pFont = dcMem.SelectObject (&fnt);

	dcMem.DrawText (str, &rcSingle, DT_SINGLELINE | DT_CALCRECT | nFormat);
	dcMem.DrawText (str, &rc, DT_CALCRECT | nFormat);
	rc.bottom += ((v.GetSize () - 1) * m_nParagraphGap);

	if (IsParaWidthEnabled ())// && v.GetSize () == 1) 
		rc.right = rc.left + CBaseElement::ThousandthsToLogical (m_sizeParagraph, head).x;

	const int nLineHeight = rcSingle.Height ();
	const CSize sizeCalc (rc.Width (), (v.GetSize () * nLineHeight) + ((v.GetSize () - 1) * m_nParagraphGap));
	CSize size (sizeCalc);
	const int nParaWidth = size.cx;

	//TRACEF (f.m_strName + _T (": ") + FoxjetDatabase::ToString ((int)f.m_nSize) + _T (" [") + FoxjetDatabase::ToString ((int)rcSingle.Height ()) + _T ("]"));

	MARKTRACECOUNT ();

	if (IsParaWidthEnabled ()) 
		if (m_sizeParagraph.cx)
			size.cx = CBaseElement::ThousandthsToLogical (m_sizeParagraph, head).x;

	if (orient == VERTICAL) {
		swap (size.cx, size.cy);

		/*
		if (IsKindOf (RUNTIME_CLASS (CDatabaseElement))) {
			if (IsResizable ()) {
				size.cx = CBaseElement::ThousandthsToLogical  (CPoint (GetParaWidth (), 0), head).x;

				if (context == PRINTER) {
					size.cx = (int)(double)size.cx / dStretch [1];
					size.cy = rc.Height ();
				}
			}
		}
		*/

		size.cy = BindTo (size.cy, 0L, (long)head.GetChannels ());

		if (context == PRINTER) 
			swap (m_bFlippedV, m_bFlippedH);
	}

	m_size = CBaseElement::LogicalToThousandths (size, head);

	/*
	if (IsKindOf (RUNTIME_CLASS (CDatabaseElement))) {
		if (orient == VERTICAL) {
			swap (size.cx, size.cy);
			swap (m_size.cx, m_size.cy);
		}
	}
	*/

	if (!bCalcSizeOnly) {
		CBitmap bmp;
		int x = 0, y = 0, cx = size.cx, cy = size.cy;
		const int nDither = GetDither ();

		if (IsParaWidthEnabled ()) 
			cx = sizeCalc.cx;

		MARKTRACECOUNT ();

		bmp.CreateCompatibleBitmap (&dc, size.cx, size.cy);

		CBitmap * pBmp = dcMem.SelectObject (&bmp);
		int nBkMode = dcMem.SetBkMode (TRANSPARENT);
		COLORREF rgbTextColor = dcMem.SetTextColor (IsInverse () ? rgbWhite : rgbBlack);
		COLORREF rgbBkColor = dcMem.SetBkColor (!IsInverse () ? rgbWhite : rgbBlack);

		MARKTRACECOUNT ();

		if (orient == VERTICAL) {
			CBitmap bmpNormal;
			CBitmap bmpRotate;
			CDC dcRotate;

			/*
			if (IsKindOf (RUNTIME_CLASS (CDatabaseElement))) {
				if (IsResizable ()) {
					int cx = (int)((double)size.cy / dStretch [1]) - nParaWidth;

					if (context == PRINTER)
						cx = size.cy - nParaWidth;

					switch (m_alignPara) {
					case ALIGNMENT_CENTER:
						rc += CSize (cx / 2.0, 0);
						break;
					case ALIGNMENT_RIGHT:
						rc += CSize (cx, 0);
						break;
					}
				}
			}
			*/

			MARKTRACECOUNT ();

			dcRotate.CreateCompatibleDC (&dc);
			
			{
				/*
				if (IsKindOf (RUNTIME_CLASS (CDatabaseElement))) 
					swap (size.cx, size.cy);
				*/

				bmpNormal.CreateCompatibleBitmap (&dc, size.cx, size.cy);

				CBitmap * pMem = dcMem.SelectObject (&bmpNormal);
				::BitBlt (dcMem, 0, 0, size.cx, size.cy, NULL, 0, 0, IsInverse () ? BLACKNESS : WHITENESS);
				DrawText (dcMem, v, &rc, nFormat, f);
				dcMem.SelectObject (pMem);

				/*
				if (IsKindOf (RUNTIME_CLASS (CDatabaseElement))) 
					swap (size.cx, size.cy);
				*/
			}

			bmpNormal.CreateCompatibleBitmap (&dc, sizeCalc.cx, sizeCalc.cy);
			
			CBitmap * pMem = dcMem.SelectObject (&bmpNormal);
			::BitBlt (dcMem, 0, 0, sizeCalc.cx, sizeCalc.cy, NULL, 0, 0, IsInverse () ? BLACKNESS : WHITENESS);

			DrawText (dcMem, v, rc, nFormat, f);

			dcMem.SelectObject (pMem);

			MARKTRACECOUNT ();

			RotateRight90 (dc, bmpNormal, bmpRotate);

			MARKTRACECOUNT ();

			int nShift = (context == EDITOR ? IsFlippedV () : IsFlippedH ()) ? (sizeCalc.cx - size.cy) : 0;
			CBitmap * pRotate = dcRotate.SelectObject (&bmpRotate);
			dcMem.BitBlt (0, 0, size.cx, size.cy, &dcRotate, 0, nShift, SRCCOPY);
			dcRotate.SelectObject (pRotate);

			{
				CSize result = (context == EDITOR) ? size : CSize (size.cy, size.cx);

				cx = result.cx;
				cy = result.cy;

				if (context == EDITOR) 
					cx = (int)((double)cx / dStretch [1]);
			}

			MARKTRACECOUNT ();
		}
		else {
			MARKTRACECOUNT ();

			::BitBlt (dcMem, 0, 0, size.cx, size.cy, NULL, 0, 0, IsInverse () ? BLACKNESS : WHITENESS);
			DrawText (dcMem, v, &rc, nFormat, f);

			MARKTRACECOUNT ();
		}

		#ifdef __WYSIWYGFIX__
		if (context == EDITOR) {
			if (m_size.cx) {
				CRect rcAdj = GetWindowRect (head); 
				CSize sizeAdj = ThousandthsToLogical (rcAdj.Size (), head);

				if (orient == NORMAL) {
					double dAdj = (double)sizeAdj.cx / (double)rc.Width ();

					x = (int)((double)x * dAdj);
					cx = (int)((double)cx * dAdj);
				}
				else 
					cx = sizeAdj.cx;
			}
		}
		#endif

		if (IsFlippedV ()) {
			y += (cy - 1);
			cy *= -1;
		}
		if (IsFlippedH ()) {
			x += (cx - 1);
			cx *= -1;
		}

		if (context == EDITOR) {
			if (nDither > 0 && nDither != 100) {
				CDC dcPat;
				CBitmap bmpPat;

				VERIFY (dcPat.CreateCompatibleDC (&dc));
				VERIFY (bmpPat.CreateCompatibleBitmap (&dc, size.cx, size.cy));

				CxImage img = MakePatternDither (nDither, context);
				HBITMAP hbmp = img.MakeBitmap ();
				//SAVEBITMAP (hbmp, _T ("C:\\Foxjet\\hbmp.bmp"));

				HBRUSH hbrush = ::CreatePatternBrush (hbmp);
				HGDIOBJ hOld = ::SelectObject (dcPat, hbrush);

				CBitmap * pOld = dcPat.SelectObject (&bmpPat);

				dcPat.PatBlt (0, 0, size.cx, size.cy, PATCOPY);
				//SAVEBITMAP (bmpPat, _T ("C:\\Foxjet\\bmpPat.bmp"));

				//SAVEBITMAP (dcMem, _T ("C:\\Foxjet\\bmpMem.bmp"));
				dc.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, size.cx, size.cy, SRCINVERT);
				//SAVEBITMAP (dc, _T ("C:\\Foxjet\\dc1.bmp"));
				dc.BitBlt (0, 0, size.cx, size.cy, &dcPat, 0, 0, SRCAND);
				//SAVEBITMAP (dc, _T ("C:\\Foxjet\\dc2.bmp"));
				dc.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, size.cx, size.cy, SRCINVERT);
				//SAVEBITMAP (dc, _T ("C:\\Foxjet\\dc3.bmp"));

				dcPat.SelectObject (pOld);
				::SelectObject (dcPat, hOld);
				::DeleteObject (hbrush);
				::DeleteObject (hbmp);
			}
			else {
				dc.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, size.cx, size.cy, SRCCOPY);
			}

			MARKTRACECOUNT ();
		}
		else {
			CBitmap bmpPrinter;
			CBitmap bmpEditor;
			CDC dcEditor;

			dcEditor.CreateCompatibleDC (&dc);
			
			bmpEditor.CreateCompatibleBitmap (&dc, size.cx, size.cy);
			
			MARKTRACECOUNT ();

			{
				CBitmap * pBmp = dcEditor.SelectObject (&bmpEditor);

				//SAVEBITMAP (dcMem, _T ("C:\\Temp\\Debug\\dcMem [PRINTER].bmp"));

				if (orient == VERTICAL) {
					::BitBlt (dcEditor, 0, 0, size.cx, size.cy, NULL, 0, 0, WHITENESS); 
					dcEditor.StretchBlt (y, x, cy, cx, &dcMem, 0, 0, size.cx, size.cy, SRCCOPY); 
					dcEditor.BitBlt (0, 0, size.cx, size.cy, &dcEditor, 0, 0, DSTINVERT);
				}
				else {
					CSize blt = rc.Size ();

					if (IsParaWidthEnabled ()) {
						blt.cx = size.cx;

						if (IsFlippedH ()) {
							int nDiff = max (0, size.cx - sizeCalc.cx);

							x += nDiff;
						}
					}

					::BitBlt (dcEditor, 0, 0, blt.cx, blt.cy, NULL, 0, 0, IsInverse () ? BLACKNESS : WHITENESS); 
					dcEditor.StretchBlt (x, y, cx, cy, &dcMem, 0, 0, rc.Width (), rc.Height (), SRCCOPY); 
					dcEditor.BitBlt (0, 0, size.cx, size.cy, &dcEditor, 0, 0, DSTINVERT);
				}

				dcEditor.SelectObject (pBmp);
			}

			MARKTRACECOUNT ();

			dcMem.SelectObject (pBmp);
			RotateLeft90AndFlip (dc, bmpEditor, bmpPrinter);

			MARKTRACECOUNT ();

			dcMem.SelectObject (&bmpPrinter);

			if (nDither > 0 && nDither != 100) {
				CDC dcPat, dcDither;
				CBitmap bmpPat, bmpDither;
				DIBSECTION ds;

				MARKTRACECOUNT ();

				bmpPrinter.GetObject (sizeof (ds), &ds);
				CSize size (ds.dsBm.bmWidth, ds.dsBm.bmHeight);

				VERIFY (dcPat.CreateCompatibleDC (&dc));
				VERIFY (dcDither.CreateCompatibleDC (&dc));

				MARKTRACECOUNT ();

				VERIFY (bmpPat.CreateCompatibleBitmap (&dc, size.cx, size.cy));
				VERIFY (bmpDither.CreateCompatibleBitmap (&dc, size.cx, size.cy));
				CBitmap * pDither = dcDither.SelectObject (&bmpDither);

				MARKTRACECOUNT ();

				CxImage img = MakePatternDither (100 - nDither, context);
				HBITMAP hbmp = img.MakeBitmap ();
				//SAVEBITMAP (hbmp, _T ("C:\\Foxjet\\hbmp9.bmp"));

				MARKTRACECOUNT ();

				HBRUSH hbrush = ::CreatePatternBrush (hbmp);
				HGDIOBJ hOld = ::SelectObject (dcPat, hbrush);

				CBitmap * pOld = dcPat.SelectObject (&bmpPat);

				MARKTRACECOUNT ();

				dcPat.PatBlt (0, 0, size.cx, size.cy, PATCOPY);
				//SAVEBITMAP (bmpPat, _T ("C:\\Foxjet\\bmpPat.bmp"));

				//SAVEBITMAP (dcMem, _T ("C:\\Foxjet\\bmpMem.bmp"));
				::BitBlt (dcDither, 0, 0, size.cx, size.cy, NULL, 0, 0, WHITENESS);
				//SAVEBITMAP (dcDither, _T ("C:\\Foxjet\\dc0.bmp"));
				dcDither.BitBlt (0, 0, size.cx, size.cy, &dcMem, 0, 0, SRCCOPY);
				//SAVEBITMAP (dcDither, _T ("C:\\Foxjet\\dc1.bmp"));
				dcDither.BitBlt (0, 0, size.cx, size.cy, &dcPat, 0, 0, SRCAND);
				//SAVEBITMAP (dcDither, _T ("C:\\Foxjet\\dc2.bmp"));
				dcMem.BitBlt (0, 0, size.cx, size.cy, &dcDither, 0, 0, SRCCOPY);

				MARKTRACECOUNT ();

				dcDither.SelectObject (pDither);
				dcPat.SelectObject (pOld);
				::SelectObject (dcPat, hOld);
				::DeleteObject (hbrush);
				::DeleteObject (hbmp);

				//TRACECOUNTARRAY ();
			}

			dc.StretchBlt (0, 0, size.cy, size.cx, &dcMem, 0, 0, size.cy, size.cx, SRCCOPY); 
		}

		dcMem.SetBkMode (nBkMode);
		dcMem.SelectObject (pBmp);
		dcMem.SetTextColor (rgbTextColor);
		dcMem.SetBkColor (rgbBkColor);
	}

	MARKTRACECOUNT ();

	dcMem.SelectObject (pFont);

	m_bFlippedV = bFlippedV;
	m_bFlippedH = bFlippedH;

	//TRACECOUNTARRAY ();

	CSize result = (context == EDITOR) ? m_size : CSize (size.cy, size.cx);
	return result;
}


static CMap <int, int, CxImage, CxImage &> mapDither;

CxImage CTextElement::MakePatternDither (int nPercent, FoxjetCommon::DRAWCONTEXT context)
{
	CxImage img;
	int nKey = context == EDITOR ? nPercent : -nPercent;

	if (!::mapDither.Lookup (nKey, img)) {
		const int cx = 32, cy = 32;
		static const BYTE n [cx / 8 * cy] = 
		{ 
			0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
			0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
			0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
			0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
			0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
			0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
			0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
			0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
		};

		if (nPercent == 50) {
			BYTE n [cx / 8 * cy];

			memset (n, 0xAA, ARRAYSIZE (n));
			img.CreateFromArray ((BYTE *)(LPVOID)n, 32, 32, 1, 4, 0);
		}
		else {
			int nContrast = BindTo (nPercent, 0, 100);
			img.CreateFromArray ((BYTE *)(LPVOID)n, 32, 32, 1, 4, 0);
			img.IncreaseBpp (24);
			img.Light (0, nContrast <= 50 ? (nContrast * -2) : ((100 - nContrast) * -2));
			img.Dither (7);
			img.DecreaseBpp (1, false);

			if (nContrast > 50) 
				img.Negative ();
		}

		if (context == PRINTER) {
			const CSize size0 (img.GetWidth (), img.GetHeight ());

			img.Rotate (90);
			const CSize size90 (img.GetWidth (), img.GetHeight ());
			img.Crop (size90.cx - size0.cx, size90.cy - size0.cy, size90.cx, size90.cy);
		}

		::mapDither.SetAt (nKey, img);
	}

	return img;
}

CSize CTextElement::DrawBitmappedFont (int nIndex, CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, 
									   bool bCalcSizeOnly, FoxjetCommon::DRAWCONTEXT context)
{
	int cx = 0, cy = 0;
	int nTotalLen = 0;
	CStringArray v;
	CString str = CTextElement::Tokenize (GetImageData (), v, dc);
	int nBold = GetFont ().m_bBold ? 2 : 1;
	CArray <PARAGRAPHSTRUCT, PARAGRAPHSTRUCT &> vParagraph;

	const int nLines = max (1, v.GetSize ());
	str.Replace (_T ("\n"), _T (""));

	ASSERT (nIndex >= 0);
	ASSERT (nIndex < ::vBitmappedFonts.GetSize ());

	if (const CBitmappedFont * p = ::vBitmappedFonts [nIndex]) {
		cx = p->GetHeight ();
		
		for (int nLine = 0; nLine < v.GetSize (); nLine++) {
			CString strLine = v [nLine];
			int nWidth = 0;

			for (int i = 0; i < strLine.GetLength (); i++) {
				int nLen = p->DrawChar (strLine [i]);

				nWidth		+= nLen;
				nTotalLen	+= nLen;

				//if ((i + 1) < strLine.GetLength ()) {
					nWidth		+= GetWidth (); 
					nTotalLen	+= GetWidth ();
				//}
			}

			PARAGRAPHSTRUCT p;

			p.m_str		= strLine;
			p.m_nLen	= nWidth;
			vParagraph.Add (p);

			cy = max (cy, nWidth);
		}

		if (!bCalcSizeOnly) {
			CBitmap bmpMem;
			int nWidth = (int)ceil ((long double)cx / 16.0) * 16; 

//			int nBufferSize = cx * cy;
			int nBufferSize = cx * nTotalLen;

			UCHAR * pBuffer = new UCHAR [nBufferSize];

			memset (pBuffer, 0x00, nBufferSize);

			{
				int nOffset = 0;

				for (int i = 0; i < str.GetLength (); i++) {
					int n = p->DrawChar (str [i], &pBuffer [nOffset]);
					int nWords = p->GetWidthWords ();

					n += m_nWidth; 
					nOffset += ((n * nWidth) / 8); 
				}
			}

			VERIFY (bmpMem.CreateBitmap (nWidth, nTotalLen, 1, 1, pBuffer));
			//SAVEBITMAP (bmpMem, "C:\\Temp\\Debug\\bmpMem.bmp");

			CxImage img;
			CDC dcMem;
			int x = nWidth - cx;

			VERIFY (img.CreateFromHBITMAP (bmpMem));

			if (vParagraph.GetSize () == 1) { 
				CRect rcCrop (x, 0, nWidth, cy);
			
				VERIFY (img.Crop (rcCrop));

				if (!IsFlippedV ())
					VERIFY (img.Mirror ());
				if (IsFlippedH ())
					VERIFY (img.Flip ());
			}

			VERIFY (dcMem.CreateCompatibleDC (&dc));

			CxBitmap hBmp = img.MakeBitmap (dc);

			if (vParagraph.GetSize () == 1) {
				if (GetOrientation () == VERTICAL) {
					CxImage img;

					img.CreateFromHBITMAP (hBmp);
					img.RotateLeft ();
					img.Stretch (dc, 0, 0, cy * nBold, cx);
				}
				else {
					HGDIOBJ hMem = ::SelectObject (dcMem, (HGDIOBJ)hBmp);
					::StretchBlt (dc, 0, 0, cx, cy * nBold, dcMem, 0, 0, cx, cy, SRCCOPY);
					::SelectObject (dcMem, hMem);
				}
			}
			else {
				CxImage img;
				//CBitmap * pbmpTmp = NULL;
				CBitmap bmpTmp;

				int nHeight = p->GetHeight ();
				int y = 0;
				int nXDest = (cx * nLines) - nHeight;
				
				{
					int nWidth = (cx * nLines) + ((nLines - 1) * m_nParagraphGap);
					int nHeight = cy * nBold;

					if (GetOrientation () == VERTICAL) {
						swap (nHeight, nWidth);
						nWidth = min (nWidth, head.GetChannels ());
					}

					if (GetOrientation () == VERTICAL)
						swap (nWidth, nHeight);

					bmpTmp.CreateCompatibleBitmap (&dc, nWidth, nHeight);
				}

				//ASSERT (pbmpTmp);
				//CBitmap * pOld = dc.SelectObject (pbmpTmp);
				CBitmap * pOld = dc.SelectObject (&bmpTmp);

				nXDest += ((nLines - 1) * m_nParagraphGap);
				VERIFY (img.CreateFromHBITMAP (hBmp));

				//SAVEBITMAP (CxBitmap (img.MakeBitmap (dc)), "C:\\Temp\\Debug\\img0.bmp");
				::BitBlt (dc, 0, 0, nXDest, cy * nBold, NULL, 0, 0, BLACKNESS);

				for (int i = 0; i < vParagraph.GetSize (); i++) {
					CxImage imgSlice (img);
					int nYDest = 0;
					int nLen = vParagraph [i].m_nLen;

					imgSlice.Crop (x, y, x + p->GetHeight (), y + nLen);
					y += nLen;

					CxBitmap hSlice = imgSlice.MakeBitmap (dc);

					if (i == (vParagraph.GetSize () - 1))
						nLen += GetWidth ();

					switch (GetParaAlign (0)) {
					case ALIGNMENT_CENTER:	
						nYDest = ((cy * nBold) - (nLen * nBold)) / 2;	
						break;
					case ALIGNMENT_RIGHT:	
						nYDest = (cy * nBold) - (nLen * nBold);	
						break;
					}

					ASSERT (nXDest >= 0);

					HGDIOBJ hMem = ::SelectObject (dcMem, (HGDIOBJ)hSlice);
					::StretchBlt (dc, nXDest, nYDest, cx, cy * nBold, dcMem, 0, 0, cx, cy, SRCCOPY);
					::SelectObject (dcMem, (HGDIOBJ)hMem);

					nXDest -= nHeight;
					nXDest -= m_nParagraphGap;
				}

				dc.SelectObject (pOld);

				{ // after all slices are combined, perform final image manipulation
					CxImage img;
					
					//img.CreateFromHBITMAP (* pbmpTmp);
					img.CreateFromHBITMAP (bmpTmp);

					if (GetOrientation () == VERTICAL) {
						img.RotateLeft ();

						if (IsFlippedV ())
							VERIFY (img.Mirror ());
						if (!IsFlippedH ())
							VERIFY (img.Flip ());
					}
					else {
						if (!IsFlippedV ())
							VERIFY (img.Mirror ());
						if (IsFlippedH ())
							VERIFY (img.Flip ());
					}

					img.Draw (dc);
				}
			}

			delete [] pBuffer;
		}

	}
	
	int nHeight = (cx * nLines) + ((nLines - 1) * m_nParagraphGap);
	int nWidth = cy * nBold;

	if (GetOrientation () == VERTICAL) {
		nWidth = min (nWidth, head.GetChannels ());
		swap (nHeight, nWidth);
	}

	if (context == PRINTER)
		return CSize (nHeight, nWidth);
	else 
		return m_size = LogicalToThousandths (CPoint (nWidth, nHeight), head);
}

CSize CTextElement::Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly, 
						  FoxjetCommon::DRAWCONTEXT context)
{
	{
		int nIndex = FindBitmappedFont (GetFont ().m_strName);

		if (nIndex != -1) 
			return DrawBitmappedFont (nIndex, dc, head, bCalcSizeOnly, context);
	}

	//TRACEF (GetImageData ());
	CSize size = DrawParagraphMode (GetImageData (), dc, head, bCalcSizeOnly, context);

	return size;
}

HFONT CTextElement::CreateFont (const CString &strFace, UINT & nSize, int nWidth, bool bBold, bool bItalic, TextElement::FONTORIENTATION orient)
{
	UINT nMin = CTextElement::m_lmtFontSize.m_dwMin;
	UINT nMax = CTextElement::m_lmtFontSize.m_dwMax;
	int nEscapement = orient == VERTICAL ? -900 : 0; 
	int nActualSize = nSize;

	if (nSize < nMin)
		nSize = nMin;
	else if (nSize > nMax)
		nSize = nMax;

	return ::CreateFont (nSize, nWidth, nEscapement, nEscapement,
		bBold ? FW_BOLD : FW_NORMAL, 
		bItalic ? TRUE : FALSE, 0, 0,
		DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, strFace); 
}

int CTextElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	m_strImageData = m_strDefaultData;
	return GetClassID ();
}

CString CTextElement::GetDefaultData() const
{
	return m_strDefaultData;
}

bool CTextElement::SetDefaultData(const CString &strData)
{
	if (IsValid (strData.GetLength (), m_lmtString)) {
		SetRedraw ();
		m_strDefaultData = strData;
		return true;
	}

	return false;
}


CString CTextElement::GetImageData() const
{
	return m_strImageData;
}

CPrinterFont CTextElement::GetFont() const
{
	LOGFONT lf;

	if (GetLogFont (lf)) 
		return CPrinterFont (lf, m_nHeight); 
	else
		return CPrinterFont ();
}

HFONT CTextElement::GetHFont() const
{
	return m_hFont;
}

bool CTextElement::SetHFont(HFONT hFont)
{
	if (m_hFont != NULL)
		::DeleteObject (m_hFont);

	if (hFont != NULL) {
		SetRedraw ();
		m_hFont = hFont;
		return true;
	}

	return false;
}

HFONT CTextElement::GetPrinterHFont () const
{
	return m_hPrinterFont;
}

bool CTextElement::SetPrinterHFont(HFONT hFont)
{
	if (m_hPrinterFont != NULL)
		::DeleteObject (m_hPrinterFont);

	if (hFont != NULL) {
		SetRedraw ();
		m_hPrinterFont = hFont;
		return true;
	}

	return false;
}

bool CTextElement::SetFont(const FoxjetCommon::CPrinterFont &fntSet) 
{
	CPrinterFont fnt = fntSet;
	bool bResult = true;
	int nIndex = FindBitmappedFont (fnt.m_strName);

	if (nIndex != -1) {
		if (const CBitmappedFont * p = ::vBitmappedFonts [nIndex]) {
			fnt.m_nSize		= p->GetHeight ();
			//fnt.m_bBold	= false;
			fnt.m_bItalic	= false;
		}
	}
	else {
		CString strOldFont = GetFont ().m_strName;

		if (FindBitmappedFont (strOldFont) != -1) 
			m_nWidth = 0;
	}

	UINT nSize = fnt.m_nSize;

/*
	bResult &= SetPrinterHFont (CreateFont (fnt.m_strName, nSize, GetWidth (),
		fnt.m_bBold, fnt.m_bItalic, 
		(FONTORIENTATION)  ((GetOrientation () + 1) % 2)));
*/

	nSize = fnt.m_nSize;
	
	bResult &= SetHFont (CreateFont (fnt.m_strName, nSize, GetWidth (),
		fnt.m_bBold, fnt.m_bItalic, GetOrientation ()));

	if (bResult) 
		m_nHeight = fnt.m_nSize;

	return bResult;
}

bool CTextElement::GetFont(CPrinterFont &fnt) const
{
	LOGFONT lf;

	if (GetLogFont (lf)) {
		fnt = CPrinterFont (lf, m_nHeight); 
		return true;
	}

	return false;
}

bool CTextElement::GetLogFont(LOGFONT &lf) const
{
	::ZeroMemory (&lf, sizeof (lf));
	return ::GetObject (GetHFont (), sizeof(LOGFONT), (LPVOID)&lf) != 0;
}

void CTextElement::ClipTo (const FoxjetDatabase::HEADSTRUCT & head)
{
	CBaseElement::ClipTo (head);

	UINT nSize = GetFontSize ();

	if (nSize > (UINT)head.GetChannels ())
		SetFontSize (head.GetChannels ());
}

bool CTextElement::SetOrientation (TextElement::FONTORIENTATION orient)
{
	if (orient != NORMAL && orient != VERTICAL)
		orient = NORMAL;

	SetRedraw ();
	m_orient = orient;
	VERIFY (SetFont (GetFont ()));

	return true;
}

TextElement::FONTORIENTATION CTextElement::GetOrientation (FoxjetCommon::DRAWCONTEXT context) const
{
	FONTORIENTATION o = m_orient;

	if (context == PRINTER) 
		o = (FONTORIENTATION)(((int)o + 1) % 2);

	return o;
}

const FoxjetCommon::ElementFields::FIELDSTRUCT * CTextElement::GetFieldBuffer () const
{
	return ::fields;
}

CSize CTextElement::GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI) const
{
	CSize size (m_size);

	if (IsParaWidthEnabled ()) {
		double dStretch [2] = { 1, 1 };
	
		CBaseElement::CalcStretch (head, dStretch);

		if (m_sizeParagraph.cx)
			size.cx = m_sizeParagraph.cx;

		size.cy = (int)((double)size.cy * dStretch [1]);
	}

	return size;
}

bool CTextElement::SetSize (const CSize & size, const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI) 
{ 
	if (IsParaWidthEnabled ())
		SetParaWidth (size.cx);

	return true; 
}

bool CTextElement::SetResizable (bool bResizable) 
{ 
	m_bResizable = bResizable;
	return true; 
}

bool CTextElement::IsResizable () const
{ 
	return m_bResizable;
}

bool CTextElement::IsParaWidthEnabled () const
{
	if (GetOrientation () == NORMAL)
		return IsResizable ();

	return false;
}

bool CTextElement::SetLink (ULONG l)
{
	m_lLink = l;
	SetRedraw ();

	return true;
}

ULONG CTextElement::GetLink () const
{
	return m_lLink;
}

int CTextElement::Build (CElementList & list)
{
	for (int i = 0; i < list.GetSize (); i++) {
		if (CBarcodeElement * p = DYNAMIC_DOWNCAST (CBarcodeElement, &list.GetObject (i))) {
			if (GetLink () == p->GetID ()) {
				//TRACEF (p->GetCaption ().m_strText);
				SetDefaultData (p->GetCaption ().m_strText);
				Build ();
				return TEXT;
			}
		}
	}

	return TEXT;
}

int CTextElement::Build (FoxjetCommon::CElementListArray * pAllLists)
{
	if (GetLink () == 0)
		return TEXT;

	ASSERT (pAllLists);

	for (int j = 0; j < pAllLists->GetSize (); j++) {
		FoxjetCommon::CElementList & list = * (pAllLists->GetAt (j));

		if (list.GetHead ().m_lID == GetHead ().m_lID) 
			return Build (list);
	}

	return TEXT;
}