// BarcodeElement.cpp: implementation of the CBarcodeElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "IpElements.h"
//#include "Types.h"
//#include "List.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"
#include "Database.h"
#include "Resource.h"
#include "TemplExt.h"

#include "fj_system.h"
#include "fj_printhead.h"
#include "fj_element.h"
#include "BarCode\barcode.h"
#include "fj_barcode.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_font.h"
//#include "fj_export.h"
#include "fj_message.h"
#include "fj_text.h"
#include "Parse.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetIpElements;
using namespace Color;
using namespace FoxjetCommon::ElementFields;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

const FoxjetCommon::LIMITSTRUCT CBarcodeElement::m_lmtString		= { 1,	FJBARCODE_SIZE }; 

IMPLEMENT_DYNAMIC (CBarcodeElement, CIpBaseElement)

CBarcodeElement::CBarcodeElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head)
:	m_nEncode (-1),
	CIpBaseElement (fj_ElementBarCodeNew (), pHead, head)
{
	LPFJBARCODE p = GetMember ();

	strncpy (p->strText, "123456", FJBARCODE_SIZE);
	p->lHRBold		= 1;

	if (p->pBC) {
		p->pBC->flags   = 28;
		p->pBC->flags   = BARCODE_128 | BARCODE_NO_CHECKSUM;
	}
}

CBarcodeElement::CBarcodeElement (const CBarcodeElement & rhs)
:	m_nEncode (-1),
	CIpBaseElement (rhs)
{
}

CBarcodeElement & CBarcodeElement::operator = (const CBarcodeElement & rhs)
{
	CIpBaseElement::operator = (rhs);

	if (this != &rhs) {
		m_nEncode  = -1;
	}

	return * this;
}

CBarcodeElement::~CBarcodeElement ()
{
}

int CBarcodeElement::Build (BUILDTYPE type, bool bCanThrowException)
{
	return GetClassID ();
}

CString CBarcodeElement::GetImageData () const
{
	return GetMember ()->strText;
}

CString CBarcodeElement::GetDefaultData () const
{
	return GetMember ()->strText;
}

bool CBarcodeElement::SetDefaultData (const CString &strData)
{
	if (LPFJBARCODE p = GetMember ()) {

		Invalidate ();
		strncpy (p->strText, w2a (strData), FJBARCODE_SIZE);

		return true;
	}

	return false;
}

void CBarcodeElement::LoadGlobalParams () 
{
	LPFJBARCODE pBarcode = GetMember ();
	LPFJSYSTEM pSystem = GetSystem ();

	ASSERT (pSystem);

	CString strSettings = GetBarcodeSettings (ListGlobals::GetDB (), GetLineID (), GetHeadType ()); 
	//TRACEF (strSettings);
	VERIFY (fj_SystemFromString (pSystem, w2a (strSettings)));
}

void CBarcodeElement::CreateImage () 
{
	if (IsValid ()) {
		LPFJBARCODE pBarcode = GetMember ();

		LoadGlobalParams ();

		strncpy (pBarcode->strText, w2a (GetImageData ()), FJBARCODE_SIZE);

		//TRACEF (FoxjetDatabase::ToString ((int)GetBcType ()) + _T (": ") + FoxjetDatabase::ToString ((int)GetHeight (GetSystem (), GetBcType ())));
		//TRACEF (FoxjetDatabase::ToString ((int)GetElement ()->pfm->pfph->pfphy->lChannels));
		//TRACEF (FoxjetDatabase::ToString ((int)m_head.m_nChannels));
		//TRACEF (FoxjetDatabase::ToString (m_head.Height ()));
		
		CIpBaseElement::CreateImage ();
	}
}

CString CBarcodeElement::GetElementFontName () const
{
	return BARCODE_FONT; 
}

bool CBarcodeElement::SetElement (LPFJELEMENT pNew)
{
	if (pNew) {
		ASSERT (pNew);
		ASSERT (pNew->pActions->fj_DescGetType () == FJ_TYPE_BARCODE);
		ASSERT (pNew->pDesc);

		Invalidate ();

		LPFJBARCODE p = (LPFJBARCODE)pNew->pDesc;
		SetDefaultData (p->strText);
	}

	return CIpBaseElement::SetElement (pNew);
}

bool CBarcodeElement::GetOutputText () const
{
	LPFJBARCODE p = GetMember ();
	
	return (p->bHROutput) ? true : false;
}

bool CBarcodeElement::SetOutputText (bool bValue)
{
	LPFJBARCODE p = GetMember ();

	p->bHROutput = bValue;
	Invalidate ();

	return true;
}

LONG CBarcodeElement::GetBold () const
{
	return GetMember ()->lBoldValue;
}

bool CBarcodeElement::SetBold (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, m_lmtBold)) {
		GetMember ()->lBoldValue = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

CString CBarcodeElement::GetText () const
{
	LPFJBARCODE p = GetMember ();
	return p->strText;
}

bool CBarcodeElement::SetText (const CString & strValue)
{
	LPFJBARCODE p = GetMember ();
	
	strncpy (p->strText, w2a (strValue), FJBARCODE_SIZE);
	Invalidate ();

	return true;
}

LONG CBarcodeElement::GetTextBold () const
{
	return GetMember ()->lHRBold;
}

bool CBarcodeElement::SetTextBold (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, m_lmtBold)) {
		GetMember ()->lHRBold = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

int CBarcodeElement::GetWidth () const
{
	return GetMember ()->lWidthValue;
}

bool CBarcodeElement::SetWidth (int nValue)
{
/* width is not used 
	if (FoxjetCommon::IsValid (nValue, m_lmtWidth)) {
		GetMember ()->lWidthValue = nValue;
		Invalidate ();
		return true;
	}
*/

	return false;
}


LONG CBarcodeElement::GetBcType () const
{
	return GetMember ()->lSysBCIndex;
}

bool CBarcodeElement::SetBcType (LONG lValue)
{
	LPFJBARCODE p = GetMember ();

	if (lValue >= 0 && lValue < FJSYS_BARCODES) {
		p->lSysBCIndex = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

CHAR CBarcodeElement::GetTextAlign () const
{
	return GetMember ()->cHRAlign;
}

bool CBarcodeElement::SetTextAlign (CHAR cValue)
{
	if (cValue == 'L' || cValue == 'R' || cValue == 'C') {
		GetMember ()->cHRAlign = cValue;
		Invalidate ();
		return true;
	}

	return false;
}

void CBarcodeElement::Invalidate ()
{
	LoadGlobalParams ();
	m_nEncode = -1;
	CIpBaseElement::Invalidate ();
}

bool CBarcodeElement::Validate ()
{
	CString str = GetImageData ();

	LoadGlobalParams ();
	const FJSYSBARCODE & global = GetGlobal ();
	const ULONG lBCFlags = global.lBCFlags;
	
	m_nEncode = -1;

	if (Barcode_Item * p = Barcode_Create (w2a (str))) {
		m_nEncode = Barcode_Encode (p, lBCFlags);
		Barcode_Delete (p);
	}

	if (m_nEncode == -1) {
		// try to reimage again with default data

		str = GetDefaultData (lBCFlags & BARCODE_ENCODING_MASK);
		SetText (str);
		TRACEF (str);

		if (Barcode_Item * p = Barcode_Create (w2a (str))) {
			m_nEncode = Barcode_Encode (p, lBCFlags);
			ASSERT (m_nEncode != -1);
			Barcode_Delete (p);
		}
	}

	return IsValid ();
}

CString CBarcodeElement::GetDefaultData (ULONG lType)
{
	struct 
	{
		ULONG	m_lType;
		LPCTSTR m_lpsz;
	} static const map [] = 
	{
		{ BARCODE_EAN,		_T ("012345678901"),	},

//		{ BARCODE_UPC,		_T ("01234012340"),		},
		{ BARCODE_UPC,		_T ("123456"),			},
		{ BARCODE_ISBN,		_T ("012345678901"),	},
		{ BARCODE_39,		_T ("0123401234"),		},
		{ BARCODE_128,		_T ("0123401234"),		},
		{ BARCODE_128C,		_T ("0123401234"),		},
		{ BARCODE_128B,		_T ("0123401234"),		},
		{ BARCODE_128RAW,	_T ("0123401234"),		},
		{ BARCODE_I25,		_T ("0123401234"),		},
		{ BARCODE_CBR,		_T ("0123401234"),		},
		{ BARCODE_EAN128,	_T ("0123401234"),		}, // __SW0842__
	};

	for (int i = 0; i < ARRAYSIZE (map); i++) 
		if (map [i].m_lType == lType)
			return map [i].m_lpsz;

	return _T ("0123");
}

bool CBarcodeElement::IsValid () const
{
	return m_nEncode == 0;
}


const FJSYSBARCODE & CBarcodeElement::GetGlobal () const
{
	LPCFJSYSTEM pSystem = GetSystem ();
	LPCFJBARCODE p = GetMember ();
	
	ASSERT (pSystem);


	LONG lIndex = p->lSysBCIndex;
	ASSERT (lIndex >= 0 && lIndex < FJSYS_BARCODES);

	return pSystem->bcArray [lIndex];
}

////////////////////////////////////////////
bool CBarcodeElement::IsValidGlobalIndex (int nIndex) 
{
	return nIndex >= 0 && nIndex < FJSYS_BARCODES;
}


bool CBarcodeElement::GetChecksum (LPFJSYSTEM pSystem, int nIndex) 
{
	ASSERT (pSystem);

	if (IsValidGlobalIndex (nIndex))
		return (pSystem->bcArray [nIndex]).bCheckDigit ? true : false;

	ASSERT (0);
	return false;
}

bool CBarcodeElement::SetChecksum (LPFJSYSTEM pSystem, int nIndex, bool bValue)
{
	ASSERT (pSystem);
	
	if (IsValidGlobalIndex (nIndex)) {
		(pSystem->bcArray [nIndex]).bCheckDigit = bValue;
		return true;
	}

	ASSERT (0);
	return false;
}

CString CBarcodeElement::GetBcName (LPFJSYSTEM pSystem, int nIndex)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nIndex));
	return (pSystem->bcArray [nIndex]).strName;
}

CString CBarcodeElement::GetBcType (LONG lBCFlags)
{
	CString str;

	LONG lType = lBCFlags & BARCODE_ENCODING_MASK;
	
	switch (lType) {
		case BARCODE_ANY:		str = LoadString (IDS_BARCODE_ANY);		break;
		case BARCODE_EAN:		str = LoadString (IDS_BARCODE_EAN);		break;
		case BARCODE_UPC:		str = LoadString (IDS_BARCODE_UPC);		break;
		case BARCODE_ISBN:		str = LoadString (IDS_BARCODE_ISBN);	break;
		case BARCODE_39:		str = LoadString (IDS_BARCODE_39);		break;
		case BARCODE_128:		str = LoadString (IDS_BARCODE_128);		break;
		case BARCODE_128C:		str = LoadString (IDS_BARCODE_128C);	break;
		case BARCODE_128B:		str = LoadString (IDS_BARCODE_128B);	break;
		case BARCODE_I25:		str = LoadString (IDS_BARCODE_I25);		break;
		case BARCODE_128RAW:	str = LoadString (IDS_BARCODE_128RAW);	break;
		case BARCODE_CBR:		str = LoadString (IDS_BARCODE_CBR);		break;
		case BARCODE_MSI:		str = LoadString (IDS_BARCODE_MSI);		break;
		case BARCODE_PLS:		str = LoadString (IDS_BARCODE_PLS);		break;
		case BARCODE_EAN128:	str = LoadString (IDS_BARCODE_EAN128);	break; // __SW0842__
	}

	return str;
}

CString CBarcodeElement::GetName (LPFJSYSTEM pSystem, int nIndex)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nIndex));
	return (pSystem->bcArray [nIndex]).strName;
}

bool CBarcodeElement::SetName (LPFJSYSTEM pSystem, int nIndex, const CString & str)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nIndex));
	strncpy ((pSystem->bcArray [nIndex]).strName, w2a (str), FJSYS_BARCODE_NAME_SIZE);
	return true;
	
}

LONG CBarcodeElement::GetVerticalBearer (LPFJSYSTEM pSystem, int nIndex)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nIndex));
	return (pSystem->bcArray [nIndex]).lVer_Bearer;
}

bool CBarcodeElement::SetVerticalBearer (LPFJSYSTEM pSystem, int nIndex, LONG lValue)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nIndex));
	(pSystem->bcArray [nIndex]).lVer_Bearer = lValue;
	return true;
}

LONG CBarcodeElement::GetHorizontalBearer (LPFJSYSTEM pSystem, int nIndex)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nIndex));
	return (pSystem->bcArray [nIndex]).lHor_Bearer;
}

bool CBarcodeElement::SetHorizontalBearer (LPFJSYSTEM pSystem, int nIndex, LONG lValue)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nIndex));
	(pSystem->bcArray [nIndex]).lHor_Bearer = lValue;
	return true;
}

LONG CBarcodeElement::GetBarValue (LPFJSYSTEM pSystem, int nGlobalIndex, int nBarIndex)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nGlobalIndex));

	if (nBarIndex >= 0 && nBarIndex <= 5) 
		return (pSystem->bcArray [nGlobalIndex]).lBarVal [nBarIndex];

	return 0;
}

bool CBarcodeElement::SetBarValue (LPFJSYSTEM pSystem, int nGlobalIndex, int nBarIndex, LONG lValue)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nGlobalIndex));

	if (nBarIndex >= 0 && nBarIndex <= 5) {
		(pSystem->bcArray [nGlobalIndex]).lBarVal [nBarIndex] = lValue;
		return true;
	}

	return false;
}

LONG CBarcodeElement::GetHeight (LPFJSYSTEM pSystem, int nIndex)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nIndex));
	
	return pSystem->bcArray [nIndex].lBCHeight;
}

bool CBarcodeElement::SetHeight (LPFJSYSTEM pSystem, int nIndex, LONG lValue)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nIndex));
	
	pSystem->bcArray [nIndex].lBCHeight = lValue;

	return true;
}

LONG CBarcodeElement::GetBCFlags (LPCFJSYSTEM pSystem, int nIndex)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nIndex));
	
	return pSystem->bcArray [nIndex].lBCFlags;
}

CString CBarcodeElement::GetBcType (LPCFJSYSTEM pSystem, int nIndex)
{
	CString str;

	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nIndex));
	
	return GetBcType (pSystem->bcArray [nIndex].lBCFlags);
}

bool CBarcodeElement::SetBCFlags (LPFJSYSTEM pSystem, int nIndex, LONG lValue)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nIndex));
	
	pSystem->bcArray [nIndex].lBCFlags = lValue;

	return true;
}

LONG CBarcodeElement::GetQuietZone (LPFJSYSTEM pSystem, int nIndex)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nIndex));
	
	return pSystem->bcArray [nIndex].lQuietZone;
}

bool CBarcodeElement::SetQuietZone (LPFJSYSTEM pSystem, int nIndex, LONG lValue)
{
	ASSERT (pSystem);
	ASSERT (IsValidGlobalIndex (nIndex));
	
	pSystem->bcArray [nIndex].lQuietZone = lValue;
	return true;
}

static const ElementFields::FIELDSTRUCT lpszFields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType, 	_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszID,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszX,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszY,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszPosition,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszBold,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszWidth,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszFlipV,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFlipH,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszInverse,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszSysBCIndex,	_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszHROutput,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszHRAlign,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszHRBold,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszData,			_T ("0")),	
	ElementFields::FIELDSTRUCT (),
};

const FoxjetCommon::ElementFields::FIELDSTRUCT * CBarcodeElement::GetFieldBuffer () const
{
	return ::lpszFields;
}
