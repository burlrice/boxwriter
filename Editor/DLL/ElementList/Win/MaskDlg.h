#if !defined(AFX_MASKDLG_H__97A2D105_8C96_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_MASKDLG_H__97A2D105_8C96_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MaskDlg.h : header file
//

#include "Resource.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CMaskDlg dialog

class CMaskDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CMaskDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMaskDlg)
	int		m_nLength;
	BOOL	m_bVariable;
	//}}AFX_DATA

	int m_nType;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMaskDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();

	// Generated message map functions
	//{{AFX_MSG(CMaskDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MASKDLG_H__97A2D105_8C96_11D4_915E_00104BEF6341__INCLUDED_)
