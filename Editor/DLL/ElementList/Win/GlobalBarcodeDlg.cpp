// GlobalBarcodeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "GlobalBarcodeDlg.h"
#include "BarcodeElement.h"
#include "BarcodeParams.h"
#include "BarcodeDlg.h"
#include "Extern.h"
#include "Database.h"
#include "BarcodeParamDlg.h"
#include "Debug.h"
#include "Edit.h"
#include "UpcBarcodeParamsDlg.h"
#include "I2o5BarcodeParamsDlg.h"
#include "Code128ParamsDlg.h"
#include "AppVer.h"
#include "C39BcParamsDlg.h"
#include "C93BcParamsDlg.h"
#include "DataMatrixParamsDlg.h"
#include "QrParamsDlg.h"
#include "Parse.h"
#include "ExportBarcodeParamsDlg.h"
#include "Registry.h"
#include "CopyBarcodeParamsDlg.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "serialize.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetElements;
using namespace ListGlobals;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CGlobalBarcodeDlg::CBarcodeItem::CBarcodeItem ()
:	CBarcodeParams ()
{
}

CGlobalBarcodeDlg::CBarcodeItem::CBarcodeItem (const FoxjetElements::CBarcodeParams & rhs)
:	CBarcodeParams (rhs)
{
}

CGlobalBarcodeDlg::CBarcodeItem::~CBarcodeItem ()
{
}

int CGlobalBarcodeDlg::CBarcodeItem::Compare (const ItiLibrary::ListCtrlImp::CItem & cmp, int nColumn) const
{
	CBarcodeItem & rhs = (CBarcodeItem &)cmp;

	switch (nColumn) {
	case 0:
		{
			return GetID () - rhs.GetID ();
/*
			int nCustom		= m_bCustom		? 0 : 1000;
			int nRhsCustom	= rhs.m_bCustom ? 0 : 1000;

			return (nCustom + m_nMagPct) - (nRhsCustom + rhs.m_nMagPct);
*/
//			return m_nMagPct - rhs.m_nMagPct;
		}
	case 1:		return m_nBarHeight		- rhs.m_nBarHeight;
	case 2:		return m_nCheckSum		- rhs.m_nCheckSum;
	}

	return CItem::Compare (rhs, nColumn);
}

CString CGlobalBarcodeDlg::CBarcodeItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0:		
		str.Format (_T ("%d"), m_nMagPct);

		if (m_bCustom) {
			CString strTmp, strVertical;

			if (GetOrientation () == TextElement::VERTICAL)
				strVertical = _T (", ") + LoadString (IDS_VERTICAL);

			strTmp.Format (_T ("(%s%s)"), LoadString (IDS_CUSTOM), strVertical);
			str += strTmp;
		}
		break;
	case 1:		str.Format (_T ("%d"), m_nBarHeight);									break;
	case 2:		str = LoadString (m_nCheckSum	? IDS_YES : IDS_NO);					break;
	case 3:
		str.Format (_T ("%s, %d"), GetFont (), GetSize ());
		break;
	}

	return str;
}


/////////////////////////////////////////////////////////////////////////////
// CGlobalBarcodeDlg dialog


CGlobalBarcodeDlg::CGlobalBarcodeDlg(FoxjetDatabase::COdbcDatabase & db, CWnd* pParent /*=NULL*/)
:	m_bUpdated (false),
	m_db (db),
	m_nMagnification (-1),
	m_bImported (false),
	m_nResolution (150),
	FoxjetCommon::CEliteDlg(IDD_GLOBALBARCODE, pParent)
{
	//{{AFX_DATA_INIT(CGlobalBarcodeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CGlobalBarcodeDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGlobalBarcodeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGlobalBarcodeDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CGlobalBarcodeDlg)
	ON_NOTIFY(NM_DBLCLK, LV_BARCODE, OnDblclkBarcode)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_CBN_SELCHANGE(CB_LINE, OnChangeSelection)
	ON_CBN_SELCHANGE(CB_RESOLUTION, OnChangeSelection)
	ON_NOTIFY(NM_RCLICK, LV_BARCODE, OnRclickBarcode)
	ON_COMMAND(ID_RESET, OnReset)
	ON_CBN_SELCHANGE(CB_SYMBOLOGY, OnChangeSelection)
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE(CB_TYPE, OnChangeSelection)
	ON_BN_CLICKED(BTN_IMPORT, OnImport)
	ON_BN_CLICKED(BTN_EXPORT, OnExport)
	ON_BN_CLICKED(BTN_COPYFROM, OnCopyFrom)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGlobalBarcodeDlg message handlers

void CGlobalBarcodeDlg::OnDblclkBarcode(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();	
	*pResult = 0;
}

bool CGlobalBarcodeDlg::EditC128 (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db)
{
	bool bResult = false;
	CCode128ParamsDlg dlg (type, pParent);

	dlg.m_strSymbology		= GetSymbology (pItem->GetSymbology (), db);
	dlg.m_nAlignment		= pItem->GetAlignment ();
	dlg.m_nBarHeight		= pItem->GetBarHeight ();
	dlg.m_nMag				= pItem->GetMagPct ();
	dlg.m_strFont			= pItem->GetFont ();
	dlg.m_nSize				= pItem->GetSize ();
	dlg.m_nFontWidth		= pItem->GetFontWidth ();
	dlg.m_bBold				= (pItem->GetStyle () & kCapStyleBold) ? true : false;
	dlg.m_bItalic			= (pItem->GetStyle () & kCapStyleItalic) ? true : false;
	dlg.m_bChecksum			= pItem->GetCheckSum ();
	dlg.m_bStandardParam	= pItem->IsCustom ();
	dlg.m_bOptimizeFNC1		= pItem->IsOptimizedFNC1 () ? TRUE : FALSE;

	for (int i = 0; i < 4; i++) {
		dlg.m_nBar [i] = pItem->GetBarPixels (i + 1);
		dlg.m_nSpace [i] = pItem->GetSpacePixels (i + 1);
	}

	if (dlg.DoModal () == IDOK) {
		pParent->BeginWaitCursor ();

		pItem->SetStyle (0);

		VERIFY (pItem->SetAlignment (dlg.m_nAlignment));
		VERIFY (pItem->SetBarHeight (dlg.m_nBarHeight));
		VERIFY (pItem->SetMagPct (dlg.m_nMag, pItem->GetSymbology ()));
		VERIFY (pItem->SetFont (dlg.m_strFont));
		VERIFY (pItem->SetSize (dlg.m_nSize));
		VERIFY (pItem->SetStyle ((dlg.m_bBold ? kCapStyleBold : 0)		| pItem->GetStyle ()));
		VERIFY (pItem->SetStyle ((dlg.m_bItalic ? kCapStyleItalic : 0)	| pItem->GetStyle ()));
		VERIFY (pItem->SetFontWidth (dlg.m_nFontWidth));
		VERIFY (pItem->SetCheckSum (dlg.m_bChecksum));
		VERIFY (pItem->SetOptimizedFNC1 (dlg.m_bOptimizeFNC1));

		for (int i = 0; i < 4; i++) {
			VERIFY (pItem->SetBarPixels (i + 1, dlg.m_nBar [i]));
			VERIFY (pItem->SetSpacePixels (i + 1, dlg.m_nSpace [i]));
		}

		pParent->EndWaitCursor ();
		bResult = true;
	}

	return bResult;
}

bool CGlobalBarcodeDlg::EditC39 (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db)
{
	bool bResult = false;
	CC39BcParamsDlg dlg (type, pParent);

	dlg.m_strSymbology		= GetSymbology (pItem->GetSymbology (), db);
	dlg.m_nAlignment		= pItem->GetAlignment ();
	dlg.m_nBarHeight		= pItem->GetBarHeight ();
	dlg.m_nMag				= pItem->GetMagPct ();
	dlg.m_strFont			= pItem->GetFont ();
	dlg.m_nSize				= pItem->GetSize ();
	dlg.m_nFontWidth		= pItem->GetFontWidth ();
	dlg.m_bBold				= (pItem->GetStyle () & kCapStyleBold) ? true : false;
	dlg.m_bItalic			= (pItem->GetStyle () & kCapStyleItalic) ? true : false;
	dlg.m_bChecksum			= pItem->GetCheckSum ();
	dlg.m_bStandardParam	= pItem->IsCustom ();

	for (int i = 0; i < 4; i++) {
		dlg.m_nBar [i] = pItem->GetBarPixels (i + 1);
		dlg.m_nSpace [i] = pItem->GetSpacePixels (i + 1);
	}

	if (dlg.DoModal () == IDOK) {
		pParent->BeginWaitCursor ();

		pItem->SetStyle (0);

		VERIFY (pItem->SetAlignment (dlg.m_nAlignment));
		VERIFY (pItem->SetBarHeight (dlg.m_nBarHeight));
		VERIFY (pItem->SetMagPct (dlg.m_nMag, pItem->GetSymbology ()));
		VERIFY (pItem->SetFont (dlg.m_strFont));
		VERIFY (pItem->SetSize (dlg.m_nSize));
		VERIFY (pItem->SetStyle ((dlg.m_bBold ? kCapStyleBold : 0)		| pItem->GetStyle ()));
		VERIFY (pItem->SetStyle ((dlg.m_bItalic ? kCapStyleItalic : 0)	| pItem->GetStyle ()));
		VERIFY (pItem->SetFontWidth (dlg.m_nFontWidth));
		VERIFY (pItem->SetCheckSum (dlg.m_bChecksum));

		for (int i = 0; i < 4; i++) {
			VERIFY (pItem->SetBarPixels (i + 1, dlg.m_nBar [i]));
			VERIFY (pItem->SetSpacePixels (i + 1, dlg.m_nSpace [i]));
		}

		pParent->EndWaitCursor ();
		bResult = true;
	}

	return bResult;
}

bool CGlobalBarcodeDlg::EditC93 (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db)
{
	bool bResult = false;
	CC93BcParamsDlg dlg (type, pParent);

	dlg.m_strSymbology		= GetSymbology (pItem->GetSymbology (), db);
	dlg.m_nAlignment		= pItem->GetAlignment ();
	dlg.m_nBarHeight		= pItem->GetBarHeight ();
	dlg.m_nMag				= pItem->GetMagPct ();
	dlg.m_strFont			= pItem->GetFont ();
	dlg.m_nSize				= pItem->GetSize ();
	dlg.m_nFontWidth		= pItem->GetFontWidth ();
	dlg.m_bBold				= (pItem->GetStyle () & kCapStyleBold) ? true : false;
	dlg.m_bItalic			= (pItem->GetStyle () & kCapStyleItalic) ? true : false;
	dlg.m_bStandardParam	= pItem->IsCustom ();

	for (int i = 0; i < 4; i++) {
		dlg.m_nBar [i] = pItem->GetBarPixels (i + 1);
		dlg.m_nSpace [i] = pItem->GetSpacePixels (i + 1);
	}

	if (dlg.DoModal () == IDOK) {
		pParent->BeginWaitCursor ();

		pItem->SetStyle (0);

		VERIFY (pItem->SetAlignment (dlg.m_nAlignment));
		VERIFY (pItem->SetBarHeight (dlg.m_nBarHeight));
		VERIFY (pItem->SetMagPct (dlg.m_nMag, pItem->GetSymbology ()));
		VERIFY (pItem->SetFont (dlg.m_strFont));
		VERIFY (pItem->SetSize (dlg.m_nSize));
		VERIFY (pItem->SetStyle ((dlg.m_bBold ? kCapStyleBold : 0)		| pItem->GetStyle ()));
		VERIFY (pItem->SetStyle ((dlg.m_bItalic ? kCapStyleItalic : 0)	| pItem->GetStyle ()));
		VERIFY (pItem->SetFontWidth (dlg.m_nFontWidth));

		for (int i = 0; i < 4; i++) {
			VERIFY (pItem->SetBarPixels (i + 1, dlg.m_nBar [i]));
			VERIFY (pItem->SetSpacePixels (i + 1, dlg.m_nSpace [i]));
		}

		pParent->EndWaitCursor ();
		bResult = true;
	}

	return bResult;
}

bool CGlobalBarcodeDlg::EditUPCEAN (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db)
{
	bool bResult = false;
	CUpcBarcodeParamsDlg dlg (type, pParent);

	dlg.m_strSymbology		= GetSymbology (pItem->GetSymbology (), db);
	dlg.m_nBarHeight		= pItem->GetBarHeight ();
	dlg.m_nMag				= pItem->GetMagPct ();
	dlg.m_strFont			= pItem->GetFont ();
	dlg.m_nSize				= pItem->GetSize ();
	dlg.m_nFontWidth		= pItem->GetFontWidth ();
	dlg.m_bBold				= (pItem->GetStyle () & kCapStyleBold) ? true : false;
	dlg.m_bItalic			= (pItem->GetStyle () & kCapStyleItalic) ? true : false;
	dlg.m_bStandardParam	= pItem->IsCustom ();
	dlg.m_orient			= pItem->GetOrientation ();
	dlg.m_bChecksum			= pItem->GetCheckSum ();

	for (int i = 0; i < 4; i++) {
		dlg.m_nBar [i] = pItem->GetBarPixels (i + 1);
		dlg.m_nSpace [i] = pItem->GetSpacePixels (i + 1);
	}

	if (dlg.DoModal () == IDOK) {
		pParent->BeginWaitCursor ();

		pItem->SetStyle (0);

		VERIFY (pItem->SetOrientation (dlg.m_orient));
		VERIFY (pItem->SetBarHeight (dlg.m_nBarHeight));
		VERIFY (pItem->SetMagPct (dlg.m_nMag, pItem->GetSymbology ()));
		VERIFY (pItem->SetFont (dlg.m_strFont));
		VERIFY (pItem->SetSize (dlg.m_nSize));
		VERIFY (pItem->SetStyle ((dlg.m_bBold ? kCapStyleBold : 0)		| pItem->GetStyle ()));
		VERIFY (pItem->SetStyle ((dlg.m_bItalic ? kCapStyleItalic : 0)	| pItem->GetStyle ()));
		VERIFY (pItem->SetFontWidth (dlg.m_nFontWidth));
		VERIFY (pItem->SetCheckSum (dlg.m_bChecksum));

		for (int i = 0; i < 4; i++) {
			VERIFY (pItem->SetBarPixels (i + 1, dlg.m_nBar [i]));
			VERIFY (pItem->SetSpacePixels (i + 1, dlg.m_nSpace [i]));
		}

		pParent->EndWaitCursor ();
		bResult = true;
	}

	return bResult;
}

bool CGlobalBarcodeDlg::Edit (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db)
{
	bool bResult = false;
	CBarcodeParamDlg dlg (type, pParent);
	int nResolution = 0;

	GetBarcodeParams (lLineID, pItem->GetSymbology (), type, nResolution, dlg.m_vParams, db);

	dlg.m_nSymbology		= pItem->GetSymbology ();
	dlg.m_nBarWidth			= pItem->GetBarWidth ();
	dlg.m_nBarHeight		= pItem->GetBarHeight ();
	dlg.m_nRatio			= pItem->GetRatio ();
	dlg.m_nCheckSum			= pItem->GetCheckSum ();
	dlg.m_nMagPct			= pItem->GetMagPct ();
	dlg.m_nHorzBearer		= pItem->GetHorzBearer ();
	dlg.m_nVertBearer		= pItem->GetVertBearer ();
	dlg.m_nQuietZone		= pItem->GetQuietZone ();

	dlg.m_lID				= pItem->GetID ();
	dlg.m_strFont			= pItem->GetFont ();
	dlg.m_nFontSize			= pItem->GetSize ();
	dlg.m_bBold				= (pItem->GetStyle () & kCapStyleBold) ? true : false;
	dlg.m_bItalic			= (pItem->GetStyle () & kCapStyleItalic) ? true : false;
	dlg.m_nAlignment		= pItem->GetAlignment ();
	dlg.m_nFontWidth		= pItem->GetFontWidth ();
	dlg.m_orient			= pItem->GetOrientation ();

	dlg.m_bStandardParam	= pItem->IsCustom ();

	if (dlg.DoModal () == IDOK) {
		pParent->BeginWaitCursor ();

		VERIFY (pItem->SetOrientation	(dlg.m_orient));
		VERIFY (pItem->SetBarWidth		(dlg.m_nBarWidth, pItem->GetSymbology ()));
		VERIFY (pItem->SetBarHeight		(dlg.m_nBarHeight));
		VERIFY (pItem->SetRatio			(dlg.m_nRatio, pItem->GetSymbology ()));
		VERIFY (pItem->SetCheckSum		(dlg.m_nCheckSum));
		VERIFY (pItem->SetMagPct		(dlg.m_nMagPct, pItem->GetSymbology ()));
		VERIFY (pItem->SetHorzBearer	(dlg.m_nHorzBearer));
		VERIFY (pItem->SetVertBearer	(dlg.m_nVertBearer));
		VERIFY (pItem->SetQuietZone		(dlg.m_nQuietZone));

		pItem->SetStyle (0);

		VERIFY (pItem->SetFont (dlg.m_strFont));
		VERIFY (pItem->SetSize (dlg.m_nFontSize));
		VERIFY (pItem->SetStyle ((dlg.m_bBold ? kCapStyleBold : 0)		| pItem->GetStyle ()));
		VERIFY (pItem->SetStyle ((dlg.m_bItalic ? kCapStyleItalic : 0)	| pItem->GetStyle ()));
		VERIFY (pItem->SetAlignment (dlg.m_nAlignment));
		VERIFY (pItem->SetFontWidth (dlg.m_nFontWidth));
		
		pParent->EndWaitCursor ();
		bResult = true;
	}

	return bResult;
}

bool CGlobalBarcodeDlg::EditQR (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db)
{
	bool bResult = false;
	CQrParamsDlg dlg (pParent);
	const int nSymbology = pItem->GetSymbology ();

	dlg.m_strSymbology		= GetSymbology (pItem->GetSymbology (), db);
	dlg.m_nMag				= pItem->GetMagPct ();
	dlg.m_nWidth			= pItem->GetBarPixels (1);
	dlg.m_nHeight			= pItem->GetBarPixels (2);
	dlg.m_nVersion			= pItem->GetVersion ();
	dlg.m_level				= pItem->GetLevel ();
	dlg.m_hint				= pItem->GetHint ();
	dlg.m_bCaseSensitive	= pItem->IsCaseSensitive ();
	dlg.m_bStandardParam	= pItem->IsCustom ();

	if (dlg.DoModal () == IDOK) {
		pParent->BeginWaitCursor ();

		VERIFY (pItem->SetMagPct (dlg.m_nMag, pItem->GetSymbology ()));

		pItem->SetStyle (0);

		/*
		for (int i = 0; i < 4; i++) {
			VERIFY (pItem->SetBarPixels (i + 1, 0));
			VERIFY (pItem->SetSpacePixels (i + 1, 0));
		}
		*/

		VERIFY (pItem->SetBarPixels (1, dlg.m_nWidth));
		VERIFY (pItem->SetBarPixels (2, dlg.m_nHeight));

		VERIFY (pItem->SetBarHeight (dlg.m_nWidth));
		VERIFY (pItem->SetBarWidth (dlg.m_nHeight, nSymbology));

		pItem->SetVersion		(dlg.m_nVersion);
		pItem->SetLevel			(dlg.m_level);
		pItem->SetHint			(dlg.m_hint);
		pItem->SetCaseSensitive	(dlg.m_bCaseSensitive);

		pParent->EndWaitCursor ();
		bResult = true;
	}

	return bResult;
}

bool CGlobalBarcodeDlg::EditDataMatrix (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db)
{
	bool bResult = false;
	CDataMatrixParamsDlg dlg (type, pParent);
	const int nSymbology = pItem->GetSymbology ();
	
	dlg.m_strSymbology		= GetSymbology (pItem->GetSymbology (), db);
	dlg.m_nBarHeight		= pItem->GetBarHeight ();
	dlg.m_nMag				= pItem->GetMagPct ();
	dlg.m_nWidth			= pItem->GetBarPixels (1);
	dlg.m_nHeight			= pItem->GetBarPixels (2);

	dlg.m_strFont			= pItem->GetFont ();
	dlg.m_nSize				= pItem->GetSize ();
	dlg.m_bBold				= (pItem->GetStyle () & kCapStyleBold) ? true : false;
	dlg.m_bItalic			= (pItem->GetStyle () & kCapStyleItalic) ? true : false;
	dlg.m_nAlignment		= pItem->GetAlignment ();
	dlg.m_nFontWidth		= pItem->GetFontWidth ();
	dlg.m_orient			= pItem->GetOrientation ();
	dlg.m_nEncoding			= pItem->GetEncoding ();

	dlg.m_bStandardParam	= pItem->IsCustom ();

	if (dlg.DoModal () == IDOK) {
		pParent->BeginWaitCursor ();

		VERIFY (pItem->SetMagPct		(dlg.m_nMag, pItem->GetSymbology ()));

		pItem->SetStyle (0);

		VERIFY (pItem->SetFont (dlg.m_strFont));
		VERIFY (pItem->SetSize (dlg.m_nSize));
		VERIFY (pItem->SetStyle ((dlg.m_bBold ? kCapStyleBold : 0)		| pItem->GetStyle ()));
		VERIFY (pItem->SetStyle ((dlg.m_bItalic ? kCapStyleItalic : 0)	| pItem->GetStyle ()));
		VERIFY (pItem->SetAlignment (dlg.m_nAlignment));
		VERIFY (pItem->SetFontWidth (dlg.m_nFontWidth));
			
		for (int i = 0; i < 4; i++) {
			VERIFY (pItem->SetBarPixels (i + 1, 0));
			VERIFY (pItem->SetSpacePixels (i + 1, 0));
		}

		VERIFY (pItem->SetBarPixels (1, dlg.m_nWidth));
		VERIFY (pItem->SetBarPixels (2, dlg.m_nHeight));

		VERIFY (pItem->SetBarHeight (dlg.m_nWidth));
		VERIFY (pItem->SetBarWidth (dlg.m_nHeight, nSymbology));
		VERIFY (pItem->SetEncoding (dlg.m_nEncoding));

		TRACEF (FoxjetDatabase::ToString (pItem->GetBarHeight ()));

		pParent->EndWaitCursor ();
		bResult = true;
	}

	return bResult;
}

bool CGlobalBarcodeDlg::EditI2o5 (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db)
{
	bool bResult = false;
	CI2o5BarcodeParamsDlg dlg (type, pParent);

	dlg.m_strSymbology		= GetSymbology (pItem->GetSymbology (), db);
	dlg.m_nBarHeight		= pItem->GetBarHeight ();
	dlg.m_nCheckSum			= pItem->GetCheckSum ();
	dlg.m_nMag				= pItem->GetMagPct ();
	dlg.m_nHorzBearer		= pItem->GetHorzBearer ();
	dlg.m_nVertBearer		= pItem->GetVertBearer ();
	dlg.m_nQuietZone		= pItem->GetQuietZone ();

	dlg.m_strFont			= pItem->GetFont ();
	dlg.m_nSize				= pItem->GetSize ();
	dlg.m_bBold				= (pItem->GetStyle () & kCapStyleBold) ? true : false;
	dlg.m_bItalic			= (pItem->GetStyle () & kCapStyleItalic) ? true : false;
	dlg.m_nAlignment		= pItem->GetAlignment ();
	dlg.m_nFontWidth		= pItem->GetFontWidth ();
	dlg.m_orient			= pItem->GetOrientation ();

	for (int i = 0; i < 4; i++) {
		dlg.m_nBar [i] = pItem->GetBarPixels (i + 1);
		dlg.m_nSpace [i] = pItem->GetSpacePixels (i + 1);
	}

	dlg.m_bStandardParam	= pItem->IsCustom ();

	if (dlg.DoModal () == IDOK) {
		pParent->BeginWaitCursor ();

		VERIFY (pItem->SetOrientation	(dlg.m_orient));
		VERIFY (pItem->SetBarHeight		(dlg.m_nBarHeight));
		VERIFY (pItem->SetCheckSum		(dlg.m_nCheckSum));
		VERIFY (pItem->SetMagPct		(dlg.m_nMag, pItem->GetSymbology ()));
		VERIFY (pItem->SetHorzBearer	(dlg.m_nHorzBearer));
		VERIFY (pItem->SetVertBearer	(dlg.m_nVertBearer));
		VERIFY (pItem->SetQuietZone		(dlg.m_nQuietZone));

		pItem->SetStyle (0);

		VERIFY (pItem->SetFont (dlg.m_strFont));
		VERIFY (pItem->SetSize (dlg.m_nSize));
		VERIFY (pItem->SetStyle ((dlg.m_bBold ? kCapStyleBold : 0)		| pItem->GetStyle ()));
		VERIFY (pItem->SetStyle ((dlg.m_bItalic ? kCapStyleItalic : 0)	| pItem->GetStyle ()));
		VERIFY (pItem->SetAlignment (dlg.m_nAlignment));
		VERIFY (pItem->SetFontWidth (dlg.m_nFontWidth));
		
		for (int i = 0; i < 4; i++) {
			VERIFY (pItem->SetBarPixels (i + 1, dlg.m_nBar [i]));
			VERIFY (pItem->SetSpacePixels (i + 1, dlg.m_nSpace [i]));
		}

		pParent->EndWaitCursor ();
		bResult = true;
	}

	return bResult;
}

FoxjetElements::CBarcodeParams * CGlobalBarcodeDlg::OnEdit (ULONG lLineID, FoxjetDatabase::HEADTYPE type, FoxjetElements::CBarcodeParams * pItem, CWnd * pParent, FoxjetDatabase::COdbcDatabase & db) 
{
	FoxjetElements::CBarcodeParams * pUpdate = NULL;

	ASSERT (pItem);

	switch (pItem->GetSymbology ()) {
	case kSymbologyEAN128:
	case kSymbologyCode128:
		if (EditC128 (lLineID, type, pItem, pParent, db))
			pUpdate = pItem;
		break;
	case kSymbologyCode39Standard:
		if (EditC39 (lLineID, type, pItem, pParent, db))
			pUpdate = pItem;
		break;
	case kSymbologyCode93:
		if (EditC93 (lLineID, type, pItem, pParent, db))
			pUpdate = pItem;
		break;

	case kSymbologyUPCA:
	case kSymbologyUPCE:
	case kSymbologyEANJAN_8:
	case kSymbologyEANJAN_13:
		if (EditUPCEAN (lLineID, type, pItem, pParent, db))
			pUpdate = pItem;
		break;
	case kSymbologyInter2of5:
	case kSymbologyGTIN14:
		if (EditI2o5 (lLineID, type, pItem, pParent, db))
			pUpdate = pItem;
		break;
	case kSymbologyDataMatrixEcc200:
	case kSymbologyDataMatrixGs1:
		if (EditDataMatrix (lLineID, type, pItem, pParent, db))
			pUpdate = pItem;
		break;
	case kSymbologyQR:
		if (EditQR (lLineID, type, pItem, pParent, db))
			pUpdate = pItem;
		break;
	default:
		if (Edit (lLineID, type, pItem, pParent, db))
			pUpdate = pItem;
	}

	return pUpdate;
}

void CGlobalBarcodeDlg::OnEdit() 
{
	CLongArray sel = ItiLibrary::GetSelectedItems (m_lv.GetListCtrl ());
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_LINE);
	HEADTYPE type = GetCurSelHeadType ();

	ASSERT (pCB);
	ULONG lLineID = pCB->GetItemData (pCB->GetCurSel ());

	if (sel.GetSize ()) {
		CBarcodeItem * pItem = (CBarcodeItem *)m_lv.GetCtrlData (sel [0]);

		if (CBarcodeItem * pUpdate = (CBarcodeItem *)OnEdit (lLineID, type, pItem, this, m_db)) {
			BeginWaitCursor ();
			m_lv.UpdateCtrlData (pUpdate);

			HEADTYPE type = GetCurSelHeadType ();

			TRACEF (pUpdate->ToString (verApp));
			VERIFY (SetBarcodeParam (lLineID, pUpdate->GetSymbology (), type, GetCurSelResolution (), pUpdate->GetID (), * pUpdate, m_db));
			SaveBarcodeParams (GetDB (), lLineID, pUpdate->GetSymbology (), type, GetCurSelResolution ());

			m_bUpdated = true;
			LoadBarcodeParams (GetDB (), CVersion ());
			EndWaitCursor ();
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CGlobalBarcodeDlg::Load () 
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);	
	CComboBox * pSymbology = (CComboBox *)GetDlgItem (CB_SYMBOLOGY);	
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);	
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	
	GetLineRecords (vLines);

	ASSERT (pLine);
	ASSERT (pSymbology);
	ASSERT (pType);

	pLine->ResetContent ();
	pSymbology->ResetContent ();
	pType->ResetContent ();

	for (int i = 0; i < vLines.GetSize (); i++) {
		int nIndex = pLine->AddString (vLines [i].m_strName);
		LINESTRUCT & line = vLines [i];

		pLine->SetItemData (nIndex, line.m_lID);

		if (m_lLineID == line.m_lID)
			pLine->SetCurSel (nIndex);
	}

	for (int i = kSymbologyFirst; i <= kSymbologyLast; i++) {
		if (CBarcodeElement::IsValidSymbology (i)) {
			CString str = FoxjetElements::GetSymbology (i, m_db);
			int nIndex = pSymbology->AddString (str);

			pSymbology->SetItemData (nIndex, i);

			if (m_nSymbology == i)
				pSymbology->SetCurSel (nIndex);
		}
	}

	bool bUltraRes = FoxjetDatabase::IsUltraResEnabled ();

	for (int nType = HEADTYPE_FIRST; nType <= HEADTYPE_LAST; nType++) {
		int nChannels = GetHeadChannels ((HEADTYPE)nType);

		if (!bUltraRes && (nType == UR2 || nType == UR4))
			continue;

		if (nChannels >= 128) {
			CString str = FoxjetElements::GetHeadTypeString ((HEADTYPE)nType, m_db);

			if (nType == UR2 || nType == UR4) 
				str = LoadString (IDS_ULTRARES);

			if (pType->FindString (-1, str) == CB_ERR) {
				int nIndex = pType->AddString (str);

				pType->SetItemData (nIndex, nType);

				if ((m_nHeadType == (HEADTYPE)nType) || (m_nHeadType == GRAPHICS_384_128 && nType == GRAPHICS_768_256))
					pType->SetCurSel (nIndex);

				if ((m_nHeadType == UR2 || m_nHeadType == UR4) && (nType == UR2 || nType == UR4))
					pType->SetCurSel (nIndex);
			}
		}
	}

	if (pLine->GetCurSel () == CB_ERR)
		pLine->SetCurSel (0);

	if (pSymbology->GetCurSel () == CB_ERR)
		pSymbology->SetCurSel (0);

	if (pType->GetCurSel () == CB_ERR)
		pType->SetCurSel (0);

	OnChangeSelection ();

	if (m_nMagnification != -1) {
		m_lv->SetItemState (m_nMagnification, LVIS_SELECTED | LVIS_FOCUSED, 0xFFFF);
		m_lv->EnsureVisible (m_nMagnification, FALSE);
	}
}

BOOL CGlobalBarcodeDlg::OnInitDialog() 
{
	using namespace ItiLibrary;
	using namespace ItiLibrary::ListCtrlImp;

	CArray <CColumn, CColumn> vCols;

	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_MAGNIFICATION)));
//	vCols.Add (CColumn (LoadString (IDS_BARWIDTH)));
	vCols.Add (CColumn (LoadString (IDS_BARHEIGHT)));
//	vCols.Add (CColumn (LoadString (IDS_RATIO)));
	vCols.Add (CColumn (LoadString (IDS_CHECKSUM)));
	vCols.Add (CColumn (LoadString (IDS_FONT)));

	m_lv.Create (LV_BARCODE, vCols, this, defElements.m_strRegSection);	

	m_lv->SortItems (m_lv.CompareFunc, (DWORD)0);
	
	if (IsNetworked ()) {
		UINT n [] = { BTN_IMPORT, BTN_EXPORT, BTN_COPYFROM };

		for (int i = 0; i < ARRAYSIZE (n); i++)
			if (CWnd * p = GetDlgItem (n [i]))
				p->ShowWindow (SW_SHOW);
	}

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_RESOLUTION)) {
		std::vector <int> vRes = GetResolutions ();

		for (int i = 0; i < vRes.size (); i++) {
			int nIndex = p->AddString (ToString (vRes [i]) + _T (" ") + LoadString (IDS_DPI));

			p->SetItemData (nIndex, vRes [i]);

			if (m_nResolution == vRes [i])
				p->SetCurSel (nIndex);
		}

		if (p->GetCurSel () == CB_ERR || !FoxjetDatabase::IsUltraResEnabled ())
			p->SetCurSel (0);
	}

	if (!FoxjetDatabase::IsUltraResEnabled ()) {
		if (CWnd * p = GetDlgItem (CB_RESOLUTION))
			p->ShowWindow (SW_HIDE);
		if (CWnd * p = GetDlgItem (LBL_RESOLUTION))
			p->ShowWindow (SW_HIDE);
	}

	Load ();

	return bResult;
}

void CGlobalBarcodeDlg::OnChangeSelection () 
{
	ULONG lLineID = GetCurSelLine ();
	int nSymbology = GetCurSelSymbology ();
	HEADTYPE type = GetCurSelHeadType ();
	int nResolution = GetCurSelResolution ();

	CArray <CBarcodeParams, CBarcodeParams &> v;

	GetBarcodeParams (lLineID, nSymbology, type, nResolution, v, m_db);
	m_lv.DeleteCtrlData ();

	m_lLineID = lLineID;
	m_nHeadType = type;
	m_nSymbology = nSymbology;

	for (int i = 0; i < v.GetSize (); i++) {
		TRACEF (ToString (v [i].GetSymbology ()) + _T (": ") + GetSymbology (v [i].GetSymbology (), m_db) + _T (": ") + v [i].ToString (verApp));
		
		m_lv.InsertCtrlData (new CBarcodeItem (v [i]));
	}
}

ULONG CGlobalBarcodeDlg::GetCurSelLine () const
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_LINE);	

	ASSERT (pCB);

	return pCB->GetItemData (pCB->GetCurSel ());
}

int CGlobalBarcodeDlg::GetCurSelSymbology () const
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_SYMBOLOGY);	

	ASSERT (pCB);

	return pCB->GetItemData (pCB->GetCurSel ());
}

int CGlobalBarcodeDlg::GetCurSelResolution() const
{
	if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_RESOLUTION)) 
		return pCB->GetItemData (pCB->GetCurSel ());

	return 0;
}


HEADTYPE CGlobalBarcodeDlg::GetCurSelHeadType() const
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_TYPE);	
	
	ASSERT (pCB);

	int nIndex = pCB->GetCurSel ();

	if (nIndex != CB_ERR) 
		return (HEADTYPE)pCB->GetItemData (nIndex);

	return HEADTYPE_FIRST;
}


void CGlobalBarcodeDlg::OnCancel()
{
	if (m_bImported) {
		ROLLBACK_TRANS (GetDB ());
		DeleteBarcodeParams (GetDB ());
		LoadBarcodeParams (GetDB (), CVersion ());
		BEGIN_TRANS (GetDB ());
	}

	FoxjetCommon::CEliteDlg::OnCancel ();
}


void CGlobalBarcodeDlg::OnRclickBarcode(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CMenu mnu;

	if (mnu.LoadMenu (IDM_BCPARAMS)) {
		if (CMenu * pMenu = mnu.GetSubMenu (0)) {
			CPoint ptScreen (0, 0);
	
			::GetCursorPos (&ptScreen);

			pMenu->TrackPopupMenu (
				TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON,
				ptScreen.x, ptScreen.y, this);
		}
	}

	*pResult = 0;
}

void CGlobalBarcodeDlg::OnReset() 
{
	if (MsgBox (* this, LoadString (IDS_RESETBCPARAMS), LoadString (IDS_CONFIRM), MB_YESNO) == IDYES) {
		CComboBox * pCB = (CComboBox *)GetDlgItem (CB_LINE);
		HEADTYPE type = GetCurSelHeadType ();
		CString strKey;

		ASSERT (pCB);
		ULONG lLineID = pCB->GetItemData (pCB->GetCurSel ());
		
		
		for (int i = kSymbologyFirst; i <= kSymbologyLast; i++) {
			CString strKey = GetSettingsKey (type, GetCurSelResolution (), i, m_db);
			bool bDelete = DeleteSettingsRecord (GetDB (), lLineID, strKey);
		}

		LoadBarcodeParams (GetDB (), CVersion ()); 
		Load ();
	}
}


void CGlobalBarcodeDlg::OnCopyFrom () 
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_LINE);
	CCopyBarcodeParamsDlg dlg (m_db, defElements.m_strRegSection + _T ("\\Settings\\CCopyBarcodeParamsDlg"), this);

	ASSERT (pCB);
	ULONG lLineTo = dlg.m_lLineID = pCB->GetItemData (pCB->GetCurSel ());

	if (dlg.DoModal () == IDOK) {
		ULONG lLineFrom = dlg.m_lLineID;

		TRACEF (ToString (lLineFrom) + _T (" --> ") + ToString (lLineTo));

		if (MsgBox (* this, LoadString (IDS_CONFIRMBCPARAMCOPY), NULL, MB_ICONQUESTION | MB_YESNO) == IDYES) {
			try {
				CString str;
				COdbcRecordset rst (m_db);
				CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> v;

				str.Format (_T ("SELECT * FROM [%s] WHERE [%s] LIKE '%%%s%%' AND [%s]=%d;"),
					Settings::m_lpszTable, 
					Settings::m_lpszKey, ListGlobals::Keys::m_strBarcode,
					Settings::m_lpszLineID, lLineFrom);
				TRACEF (str);

				if (rst.Open (str)) {
					while (!rst.IsEOF ()) {
						SETTINGSSTRUCT s;

						rst >> s;
						v.Add (s);
						rst.MoveNext ();
					}

					rst.Close ();
				}

				for (int i = 0; i < v.GetSize (); i++) {
					SETTINGSSTRUCT s = v [i];

					s.m_lLineID = lLineTo;

					if (!UpdateSettingsRecord (m_db, s))
						VERIFY (AddSettingsRecord (m_db, s));
				}

				LoadBarcodeParams (m_db, verApp);
				Load ();
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
		}
	}
}

void CGlobalBarcodeDlg::OnExport () 
{
	CExportBarcodeParamsDlg dlg (this);
	
	dlg.DoModal ();
}

void CGlobalBarcodeDlg::OnImport () 
{
	CString strFile = GetHomeDir () + _T ("\\BarcodeParams.txt");
	CString strFilter = 
		CString (_T ("Text files (*.txt)|*.txt|")) +
		CString (_T ("All files (*.*)|*.*||")); 
	
	strFile = FoxjetDatabase::GetProfileString (defElements.m_strRegSection + _T ("\\Settings\\CExportBarcodeParamsDlg"), _T ("Filename"), strFile);
	TRACEF (strFilter);
	GetDlgItemText (TXT_FILENAME, strFile);

	CFileDialog dlg (TRUE, _T ("*.*"), strFile, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		strFilter, this);
	
	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetPathName ();
		TRACEF (strFile);

		if (HMODULE hDLL = ::LoadLibrary (_T ("3d.dll"))) {
			typedef bool (CALLBACK * LPFCT) (const CString & strFile, CWnd * pParent);

			if (LPFCT lp = (LPFCT)GetProcAddress (hDLL, "ImportBarcodeParams")) {
				bool bResult = (* lp) (strFile, this);

				if (bResult) {
					OnChangeSelection ();
					m_bImported = true;
				}
			}

			::FreeLibrary (hDLL);
		}
	}
}

