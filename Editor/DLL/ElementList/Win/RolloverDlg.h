#if !defined(AFX_ROLLOVERDLG_H__A70542E4_0C90_4AF2_B704_1226C88F9B86__INCLUDED_)
#define AFX_ROLLOVERDLG_H__A70542E4_0C90_4AF2_B704_1226C88F9B86__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RolloverDlg.h : header file
//

#include "DaysDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CRolloverDlg dialog

class CRolloverDlg : public CDaysDlg
{
	DECLARE_DYNCREATE(CRolloverDlg)

protected:
	CRolloverDlg (); // only implemented for DYNCREATE

// Construction
public:
	CRolloverDlg(FoxjetDatabase::COdbcDatabase & db, const CodesDlg::CCodesArray & vCodes);
	~CRolloverDlg();

// Dialog Data
	//{{AFX_DATA(CRolloverDlg)
	int		m_nHours;
	//}}AFX_DATA

	static CString GetDay (int nDay);

	static bool GetHold (FoxjetDatabase::COdbcDatabase & db, ULONG lLineID);

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CRolloverDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CodesDlg::CCodesArray m_vCodes;

	virtual int GetListCtrlID () const { return -1; }
	virtual CodesDlg::CCodes GetCodes (ULONG lLineID) const;
	virtual void Update (ULONG lLineID, UINT nIndex, const CString & strValue);

	afx_msg void OnSelchangeLine();
	afx_msg void OnEdit ();
	afx_msg void OnEditFirstDayOfWeek ();

	// Generated message map functions
	//{{AFX_MSG(CRolloverDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnHold();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROLLOVERDLG_H__A70542E4_0C90_4AF2_B704_1226C88F9B86__INCLUDED_)
