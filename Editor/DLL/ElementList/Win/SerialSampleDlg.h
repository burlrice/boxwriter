#pragma once


// CSerialSampleDlg dialog

class CSerialSampleDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSerialSampleDlg)

public:
	CSerialSampleDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSerialSampleDlg();

// Dialog Data
	enum { IDD = IDD_SERIAL_SAMPLE };
	
	CString m_strData;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
