#pragma once

#include "DbWizard.h"


// CDbWizardFinishDlg

namespace FoxjetElements
{
	class CSerialWizardSheet;

	class ELEMENT_API CDbWizardFinishDlg : public CDbWizardBasePage
	{
		DECLARE_DYNAMIC(CDbWizardFinishDlg)

	public:
		CDbWizardFinishDlg(CDbWizardDlg * pParent);
		CDbWizardFinishDlg(CSerialWizardSheet * pParent);
		virtual ~CDbWizardFinishDlg();

		bool m_bTemplate;

	protected:
		virtual BOOL OnSetActive();
		virtual BOOL OnInitDialog ();
		//afx_msg void OnDbStart ();
		afx_msg void OnUpdateUI ();

		DECLARE_MESSAGE_MAP()
	};
};