// DataMatrixVc6TestDlg.h : header file
//

#if !defined(AFX_DATAMATRIXVC6TESTDLG_H__C8EEBB48_E3BB_4A2A_BCD3_9CE5530AF892__INCLUDED_)
#define AFX_DATAMATRIXVC6TESTDLG_H__C8EEBB48_E3BB_4A2A_BCD3_9CE5530AF892__INCLUDED_

#include "ColorBox.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CDataMatrixVc6TestDlg dialog

class CDataMatrixVc6TestDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CDataMatrixVc6TestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CDataMatrixVc6TestDlg)
	enum { IDD = IDD_DATAMATRIXVC6TEST_DIALOG };
	CComboBox	m_EncodeMode;
	CColorBox	m_ForeColor;
	CColorBox	m_BackColor;
	int		m_nColumns;
	int		m_nMargin;
	int		m_nModuleSize;
	int		m_nRows;
	CString	m_strText;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataMatrixVc6TestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDataMatrixVc6TestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnEncode();
	afx_msg void OnSave();
	afx_msg void OnBuy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:

	void SetDropDownSize(CComboBox &box, UINT LinesToDisplay);
	BOOL SaveBitmapToFile(HBITMAP hBitmap , LPCTSTR lpFileName);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAMATRIXVC6TESTDLG_H__C8EEBB48_E3BB_4A2A_BCD3_9CE5530AF892__INCLUDED_)
