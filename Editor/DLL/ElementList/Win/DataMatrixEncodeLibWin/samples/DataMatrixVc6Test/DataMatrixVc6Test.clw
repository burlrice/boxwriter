; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDataMatrixVc6TestDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "DataMatrixVc6Test.h"

ClassCount=3
Class1=CDataMatrixVc6TestApp
Class2=CDataMatrixVc6TestDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_DATAMATRIXVC6TEST_DIALOG

[CLS:CDataMatrixVc6TestApp]
Type=0
HeaderFile=DataMatrixVc6Test.h
ImplementationFile=DataMatrixVc6Test.cpp
Filter=N

[CLS:CDataMatrixVc6TestDlg]
Type=0
HeaderFile=DataMatrixVc6TestDlg.h
ImplementationFile=DataMatrixVc6TestDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=DataMatrixVc6TestDlg.h
ImplementationFile=DataMatrixVc6TestDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_DATAMATRIXVC6TEST_DIALOG]
Type=1
Class=CDataMatrixVc6TestDlg
ControlCount=17
Control1=IDENCODE,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDSAVE,button,1342242817
Control4=IDBUY,button,1342242817
Control5=IDC_STATIC,button,1342177287
Control6=IDC_STATIC,static,1342308352
Control7=IDC_ENCODEMODE,combobox,1344339970
Control8=IDC_STATIC,static,1342308352
Control9=IDC_MODULESIZE,edit,1350631552
Control10=IDC_STATIC,static,1342308352
Control11=IDC_MARGIN,edit,1350631552
Control12=IDC_BACKCOLOR,button,1342242816
Control13=IDC_FORECOLOR,button,1342242816
Control14=IDC_STATIC,static,1342308352
Control15=IDC_TEXT,edit,1350631552
Control16=IDC_STATIC,static,1342308352
Control17=IDC_COLUMNS,edit,1350631552

