// DataMatrixVc6Test.h : main header file for the DATAMATRIXVC6TEST application
//

#if !defined(AFX_DATAMATRIXVC6TEST_H__A46F8A7F_27D4_4D1C_8DEE_A9A209B4D483__INCLUDED_)
#define AFX_DATAMATRIXVC6TEST_H__A46F8A7F_27D4_4D1C_8DEE_A9A209B4D483__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CDataMatrixVc6TestApp:
// See DataMatrixVc6Test.cpp for the implementation of this class
//

class CDataMatrixVc6TestApp : public CWinApp
{
public:
	CDataMatrixVc6TestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataMatrixVc6TestApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDataMatrixVc6TestApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAMATRIXVC6TEST_H__A46F8A7F_27D4_4D1C_8DEE_A9A209B4D483__INCLUDED_)
