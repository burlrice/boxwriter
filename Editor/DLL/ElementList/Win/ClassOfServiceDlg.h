#if !defined(AFX_CLASSOFSERVICEDLG_H__EA87F464_F387_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_CLASSOFSERVICEDLG_H__EA87F464_F387_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ClassOfServiceDlg.h : header file
//

#include "resource.h"
#include "ListCtrlImp.h"
#include "OdbcDatabase.h"
#include "ClassOfService.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CClassOfServiceDlg dialog

class CClassOfServiceDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CClassOfServiceDlg(FoxjetDatabase::COdbcDatabase * pDatabase, CWnd* pParent = NULL);   // standard constructor
	virtual ~CClassOfServiceDlg ();

// Dialog Data
	//{{AFX_DATA(CClassOfServiceDlg)
	enum { IDD = IDD_CLASSOFSERVICE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

public:

	virtual void OnOK();

	CStringArray m_vSelected;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClassOfServiceDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	class CClass :		public Ritz::ClassOfService::CLASSOFSERVICERECSTRUCT,
						public ItiLibrary::ListCtrlImp::CItem
	{
	public:

		CClass ();
		CClass (const Ritz::ClassOfService::CLASSOFSERVICERECSTRUCT & rhs);
		CClass (const CClass & rhs);
		CClass & operator = (const CClass & rhs);
		virtual ~CClass ();

		virtual CString GetDispText (int nColumn) const;
	};

	FoxjetDatabase::COdbcDatabase * m_pDatabase;
	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

	// Generated message map functions
	//{{AFX_MSG(CClassOfServiceDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkClassofservice(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLASSOFSERVICEDLG_H__EA87F464_F387_11D4_8FC6_006067662794__INCLUDED_)
