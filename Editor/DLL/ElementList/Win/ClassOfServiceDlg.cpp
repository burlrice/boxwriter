// ClassOfServiceDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ClassOfServiceDlg.h"
#include "ClassOfService.h"
#include "OdbcRecordset.h"
#include "ItiLibrary.h"
#include "Debug.h"
#include "Database.h"
#include "Extern.h"

using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace Ritz::ClassOfService;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



CClassOfServiceDlg::CClass::CClass ()
{
}

CClassOfServiceDlg::CClass::CClass (const Ritz::ClassOfService::CLASSOFSERVICERECSTRUCT & rhs)
{
	m_strType		= rhs.m_strType;
	m_strIndicator	= rhs.m_strIndicator;
}

CClassOfServiceDlg::CClass::CClass (const CClass & rhs)
{
	m_strType		= rhs.m_strType;
	m_strIndicator	= rhs.m_strIndicator;
}

CClassOfServiceDlg::CClass & CClassOfServiceDlg::CClass::operator = (const CClassOfServiceDlg::CClass & rhs)
{
	if (this != &rhs) {
		m_strType		= rhs.m_strType;
		m_strIndicator	= rhs.m_strIndicator;
	}

	return * this;
}

CClassOfServiceDlg::CClass::~CClass ()
{
}

CString CClassOfServiceDlg::CClass::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0:
		str = m_strType;
		break;
	case 1:
		str = m_strIndicator;
		break;
	}

	return str;
}


/////////////////////////////////////////////////////////////////////////////
// CClassOfServiceDlg dialog


CClassOfServiceDlg::CClassOfServiceDlg(FoxjetDatabase::COdbcDatabase * pDatabase, CWnd* pParent /*=NULL*/)
:	m_pDatabase (pDatabase), 
	FoxjetCommon::CEliteDlg(CClassOfServiceDlg::IDD, pParent)
{
	ASSERT (pDatabase);
	//{{AFX_DATA_INIT(CClassOfServiceDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CClassOfServiceDlg::~CClassOfServiceDlg ()
{
}

void CClassOfServiceDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CClassOfServiceDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CClassOfServiceDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CClassOfServiceDlg)
	ON_NOTIFY(NM_DBLCLK, LV_CLASSOFSERVICE, OnDblclkClassofservice)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CClassOfServiceDlg message handlers

void CClassOfServiceDlg::OnOK()
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		m_vSelected.RemoveAll ();

		for (int i = 0; i < sel.GetSize (); i++) {
			CClass * p = (CClass *)m_lv.GetCtrlData (sel [i]);
			m_vSelected.Add (p->m_strIndicator);
		}

		FoxjetCommon::CEliteDlg::OnOK ();
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

BOOL CClassOfServiceDlg::OnInitDialog() 
{
	using ListCtrlImp::CColumn;

	CLongArray vSel;
	CArray <CColumn, CColumn> vCols;

	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();

	vCols.Add (CColumn (LoadString (IDS_TYPE), 140));
	vCols.Add (CColumn (LoadString (IDS_INDICATOR), 70));

	m_lv.Create (LV_CLASSOFSERVICE, vCols, this, defElements.m_strRegSection);

	try {
		COdbcRecordset rst (m_pDatabase);
		CString strSQL;
		int nIndex = 0;
		
		strSQL.Format (_T ("SELECT * FROM [%s] ORDER BY %s;"),
			m_lpszTable, m_lpszIndicator);
		
		rst.Open (CRecordset::snapshot, strSQL);

		while (!rst.IsEOF ()) {
			CClass * p = new CClass;

			p->m_strType		= (CString)rst.GetFieldValue (m_lpszType);
			p->m_strIndicator	= (CString)rst.GetFieldValue (m_lpszIndicator);

			m_lv.InsertCtrlData (p);

			for (int i = 0; i < m_vSelected.GetSize (); i++) 
				if (p->m_strIndicator == m_vSelected [i])	
					vSel.Add (nIndex);

			nIndex++;
			rst.MoveNext ();
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	for (int i = 0; i < vSel.GetSize (); i++) 
		m_lv->SetItemState (vSel [i], (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);	

	return bResult;
}

void CClassOfServiceDlg::OnDblclkClassofservice(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnOK ();	
	*pResult = 0;
}
