#pragma once

#include "DbWizard.h"
#include "ListCtrlImp.h"

// CDbWizardTypesDlg

namespace FoxjetElements
{
	class ELEMENT_API CTypeItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CTypeItem (const CString & strName = _T (""), const CString & strType = _T (""), const CString & strExample = _T ("")) 
			:	m_strName (strName), 
				m_strType (strType), 
				m_strExample (strExample) 
		{
		}

		virtual CString GetDispText (int nColumn) const { return _T (""); }

		CString m_strName;
		CString m_strType;
		CString m_strExample;
	};

	class ELEMENT_API CDbWizardTypesDlg : public CDbWizardBasePage
	{
		DECLARE_DYNAMIC(CDbWizardTypesDlg)

	public:
		CDbWizardTypesDlg(CDbWizardDlg * pParent);
		virtual ~CDbWizardTypesDlg();

	protected:
		virtual BOOL OnInitDialog();
		virtual BOOL OnSetActive();

		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

		DECLARE_MESSAGE_MAP()
	};
};

