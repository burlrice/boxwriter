// DbWizardFieldDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DbWizardFieldDlg.h"
#include "DbWizardDsnDlg.h"
#include "Parse.h"
#include "Debug.h"
#include "OdbcTable.h"
#include "OdbcTables.h"
#include "OdbcRecordset.h"
#include "Extern.h"
#include "DatabaseElement.h"
#include "DbFieldDlg.h"
#include "color.h"
#include "BarcodeElement.h"

using namespace FoxjetDatabase;
using namespace FoxjetElements;
using namespace ItiLibrary;
using namespace FoxjetCommon;

#define MENUITEM_STD		(0)
#define MENUITEM_NOSECURITY	(-1)
#define MENUITEM_SUBMENU	(-2)

struct
{
	UINT	m_nResID;
	UINT	m_nCmdID;
	UINT	m_nBitmapID;
	int		m_nType;
}
static const mapMenu [] = 
{
	{ ID_ELEMENT_TEXT,				ID_ELEMENT_TEXT,				-1,			MENUITEM_STD,			},
	{ ID_ELEMENT_BITMAP,			ID_ELEMENT_BITMAP,				-1,			MENUITEM_STD,			},
	{ ID_ELEMENT_BARCODE,			ID_ELEMENT_BARCODE,				-1,			MENUITEM_SUBMENU,		},
};

////////////////////////////////////////////////////////////////////////////////////////////////////

LRESULT CALLBACK CDbWizardFieldDlg::CTypeListCtrl::WindowProc (HWND hWnd, UINT nMsg, WPARAM wParam, LPARAM lParam)
{
	CArray <CListCtrlImp *, CListCtrlImp *> v;

	GetDispatch (hWnd, v);

	switch (nMsg) {
	case WM_NOTIFY:
		{
			UINT nCtrl = (UINT)wParam;

			for (int i = 0; i < v.GetSize (); i++) {
				if (nCtrl == v [i]->GetListCtrlID ()) {
					LPNMHDR pnmh = (LPNMHDR)lParam;

					switch (pnmh->code) {
					case NM_CLICK:
					case NM_RCLICK:
						((CTypeListCtrl *)v [i])->OnClick ((LPNMITEMACTIVATE)lParam);
						break;
					}
				}
			}
		}
		break;
	}

	LRESULT lResult = ListCtrlImp::CListCtrlImp::WindowProc (hWnd, nMsg, wParam, lParam);

	return lResult;
}

void CDbWizardFieldDlg::CTypeListCtrl::OnClick (LPNMITEMACTIVATE lpnma)
{
	if (lpnma->iSubItem == 2) {
		CMenu menu, menuBarcode;
		
		menu.CreatePopupMenu ();

		menuBarcode.CreatePopupMenu ();

		for (int i = kSymbologyFirst; i <= kSymbologyLast; i++) {
			if (CBarcodeElement::IsValidSymbology (i)) {
				MENUITEMINFO info;

				memset (&info, 0, sizeof (info));

				info.cbSize		= sizeof (info);
				info.fMask		= MIIM_TYPE | MIIM_ID | MIIM_DATA | MIIM_SUBMENU;
				info.fType		= MFT_OWNERDRAW;
				info.wID		= i;

				::InsertMenuItem (menuBarcode, i, TRUE, &info);
			}
		}


		for (int i = 0; i < ARRAYSIZE (mapMenu); i++) {
			MENUITEMINFO info;

			memset (&info, 0, sizeof (info));

			info.cbSize		= sizeof (info);
			info.fMask		= MIIM_TYPE | MIIM_ID | MIIM_DATA | MIIM_SUBMENU;
			info.fType		= MFT_OWNERDRAW;
			info.wID		= mapMenu [i].m_nCmdID;
	
			switch (info.wID) {
			case ID_ELEMENT_BARCODE:
				info.hSubMenu = menuBarcode;
				break;
			}

			::InsertMenuItem (menu, i, TRUE, &info);
		}

		DWORD dwpos = GetMessagePos();
		menu.TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON, GET_X_LPARAM (dwpos), GET_Y_LPARAM (dwpos), GetParent ());
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////////


CFieldItem::CFieldItem (const CString & strName, int nSQLType, int nElementType) 
:	m_strName (strName), 
	m_nSQLType (nSQLType),
	m_strSQLType (CDatabaseElement::GetSQLType (nSQLType)), 
	m_nClassID (LOWORD (nElementType)),
	m_nSymbology (HIWORD (nElementType))
{
}


CString CFieldItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0: return m_strName;
	case 1: return m_strSQLType;
	case 2: 
		switch (m_nClassID) {
		case TEXT:		return LoadString (IDS_TEXT);
		case BMP:		return LoadString (IDS_BITMAP);
		case BARCODE:	return FoxjetElements::GetSymbology (m_nSymbology, ListGlobals::GetDB ());
		}
	}

	return _T ("");
}

////////////////////////////////////////////////////////////////////////////////////////////////////


// CDbWizardFieldDlg

IMPLEMENT_DYNAMIC(CDbWizardFieldDlg, CDbWizardBasePage)

CDbWizardFieldDlg::CDbWizardFieldDlg(CDbWizardDlg * pParent)
: CDbWizardBasePage (pParent, IDD_DATABASE_WIZARD_FIELDS)
{
}

CDbWizardFieldDlg::~CDbWizardFieldDlg()
{
}


BEGIN_MESSAGE_MAP(CDbWizardFieldDlg, CDbWizardBasePage)
	ON_CBN_SELCHANGE(CB_TABLES, OnSelchangeTables)
	ON_BN_CLICKED(BTN_VIEW, OnView)
	ON_WM_DRAWITEM ()
	ON_WM_MEASUREITEM ()
	ON_COMMAND_RANGE (ID_ELEMENT_TEXT, ID_ELEMENT_SHAPE, OnElementCmd)
	ON_COMMAND_RANGE (kSymbologyFirst, kSymbologyLast, OnElementCmd)
END_MESSAGE_MAP()



// CDbWizardFieldDlg message handlers

BOOL CDbWizardFieldDlg::OnInitDialog()
{
	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;

	CDbWizardBasePage::OnInitDialog();

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_NAME), 200));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_SQL_TYPE), 200));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_ELEMENT_TYPE), 200));

	m_lv.Create (LV_FIELDS, vCols, this, ListGlobals::defElements.m_strRegSection);
	m_lv->SetExtendedStyle (LVS_EX_CHECKBOXES | m_lv->GetExtendedStyle ());

	return TRUE;
}

CString CDbWizardFieldDlg::GetCurSelTable () const
{
	CString str;

	if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_TABLES)) 
		if (pCB->GetCount ())
			pCB->GetLBText (pCB->GetCurSel (), str);

	return str;
}

BOOL CDbWizardFieldDlg::OnSetActive()
{
	if (m_strDSN != m_pdlgMain->m_pdlgDSN->GetCurSelDsn ()) {
		m_strDSN = m_pdlgMain->m_pdlgDSN->GetCurSelDsn ();

		if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_TABLES)) {
			CString strTable;

			if (FoxjetElements::CDatabaseElement * p = DYNAMIC_DOWNCAST (CDatabaseElement, &ListGlobals::defElements.GetElement (DATABASE)))
				strTable = p->GetTable ();
						
			pCB->ResetContent ();

			for (int i = 0; i < m_pdlgMain->m_pdb->GetDB ().GetTableCount (); i++) {
				CString str = m_pdlgMain->m_pdb->GetDB ().GetTable (i)->GetName ();

				int nIndex = pCB->AddString (str);

				if (!strTable.CompareNoCase (str))
					pCB->SetCurSel (nIndex);
			}

			if (pCB->GetCurSel () == CB_ERR)
				pCB->SetCurSel (0);

			OnSelchangeTables ();
		}
	}

	return TRUE;
}

BOOL CDbWizardFieldDlg::OnKillActive()
{
	m_vFields = GetCurSelFields ();
	return TRUE;
}

bool CDbWizardFieldDlg::CanAdvance () 
{ 
	return ItiLibrary::GetCheckedItems (m_lv).GetSize () ? true : false;
}

void CDbWizardFieldDlg::OnSelchangeTables ()
{
	CString strTable = GetCurSelTable ();

	m_lv.DeleteCtrlData ();

	try
	{
		//COdbcRecordset rst (&m_pdlgMain->m_db);
		CString strDSN = FoxjetDatabase::Extract (m_pdlgMain->m_pdb->GetDB ().GetConnect (), _T ("DSN="), _T (";"));
		CString strSQL = _T ("SELECT * FROM [") + strTable + _T ("];");

		if (m_pdlgMain->m_pdb->GetDB ().IsProgress ())
			strSQL.Format (_T ("SELECT * FROM [%s]"), strTable);

		COdbcCache rst = COdbcDatabase::Find (m_pdlgMain->m_pdb->GetDB (), strDSN, strSQL);

		if (!rst.GetRst ().IsEOF ()) {
			for (short i = 0; i < rst.GetRst ().GetODBCFieldCount (); i++) {
				CODBCFieldInfo info;
				CString strName = Extract (rst.GetRst ().GetFieldName (i), _T ("].["), _T ("]"));
				
				rst.GetRst ().GetFieldInfo (strName, info);
				CString strType = CDatabaseElement::GetSQLType (info.m_nSQLType);
				
				CFieldItem * p = new CFieldItem (strName, info.m_nSQLType);
				m_lv.InsertCtrlData (p);
				m_lv->SetCheck (m_lv.GetDataIndex (p), 0);
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }

	m_strTable = GetCurSelTable ();
}

FoxjetCommon::CCopyArray <CFieldItem, CFieldItem &> CDbWizardFieldDlg::GetCurSelFields () const
{
	CCopyArray <CFieldItem, CFieldItem &> result;
	CLongArray sel = ItiLibrary::GetCheckedItems (m_lv.GetListCtrl ()); 

	for (int i = 0; i < sel.GetSize (); i++) 
		result.Add (CFieldItem (* (CFieldItem *)m_lv.GetCtrlData (sel [i])));

	return result; 
}

void CDbWizardFieldDlg::OnView ()
{
	CInternalFormatting opt (false);
	FoxjetUtils::CDbFieldDlg dlg (ListGlobals::defElements.m_strRegSection, this);

	dlg.m_dwFlags	= 0;//ListCtrlImp::CELLCTRL_NOCHANGECOLSEL; 
	dlg.m_strDSN	= m_strDSN;
	dlg.m_strTable	= GetCurSelTable ();
	//dlg.m_strField	= pElement->GetDatabaseKeyField ();
	//dlg.m_nRow		= 0;

	dlg.DoModal ();
}

void CDbWizardFieldDlg::OnMeasureItem (int nCtrl, LPMEASUREITEMSTRUCT lpmis)
{
	if (lpmis->CtlType == ODT_MENU) {
		lpmis->itemWidth = 200;
		lpmis->itemHeight = 50;
	}
}

void CDbWizardFieldDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	using namespace Color;
	using namespace FoxjetCommon;

	DRAWITEMSTRUCT dis;
	
	memcpy (&dis, lpDrawItemStruct, sizeof (dis));

	if (dis.CtlType == ODT_MENU) {
		bool bSelected = (dis.itemState & (ODS_SELECTED | ODS_FOCUS)) ? false : true;
		CString str;
		UINT nBitmapID = -1;
		CRect rc = dis.rcItem;
		CDC dc;
		MEASUREITEMSTRUCT m;
		const CSize sizeBitmap (32, 32);

		memset (&m, 0, sizeof (m));

		for (int i = 0; i < ARRAYSIZE (::mapMenu); i++) {
			if (::mapMenu [i].m_nCmdID == dis.itemID) {
				str = LoadString (::mapMenu [i].m_nResID);
				int nIndex = str.Find (_T ("\n"));

				if (nIndex != -1)
					str = str.Left (nIndex);

				break;
			}
		}

		if (str.IsEmpty ()) {
			if (CBarcodeElement::IsValidSymbology (dis.itemID)) 
				str = GetSymbology (dis.itemID, ListGlobals::GetDB ());
		}

		dc.Attach (dis.hDC);
		COLORREF rgbSetTextColor = dc.SetTextColor (bSelected ? Color::rgbCOLOR_WINDOWTEXT : Color::rgbCOLOR_HIGHLIGHTTEXT);
		int nSetBkMode = dc.SetBkMode (TRANSPARENT);
		dc.FillRect (rc, &CBrush (bSelected ? Color::rgbCOLOR_MENU : Color::rgbCOLOR_HIGHLIGHT));
		dc.DrawEdge (rc, EDGE_SUNKEN, BF_LEFT | BF_RIGHT | BF_TOP | BF_BOTTOM | BF_ADJUST);
		rc.left += sizeBitmap.cy + 5; 

		if (nBitmapID != -1) {
			CBitmap bmp;

			if (bmp.LoadBitmap (nBitmapID)) {
				CDC dcMem;
				DIBSECTION ds;

				memset (&ds, 0, sizeof (ds));
				bmp.GetObject (sizeof (ds), &ds);
				int x = 0;
				int y = rc.top + (rc.Height () - sizeBitmap.cy) / 2;

				dcMem.CreateCompatibleDC (&dc);
				CBitmap * pBitmap = dcMem.SelectObject (&bmp);
				COLORREF rgb = dcMem.GetPixel (0, 0);
				::TransparentBlt (dc, x, y, sizeBitmap.cx, sizeBitmap.cy, dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, rgb);
				dcMem.SelectObject (pBitmap);
			}
		}

		//{ CString strDebug; strDebug.Format (_T ("[%d] %s"), dis.itemID, str); TRACEF (strDebug); }
		dc.DrawText (str, rc, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_EXPANDTABS);
		dc.SetTextColor (rgbSetTextColor);
		dc.SetBkMode (nSetBkMode);
		dc.Detach ();
	}
}

void CDbWizardFieldDlg::OnElementCmd (UINT nID)
{
	CLongArray sel = GetSelectedItems (m_lv);

	for (int i = 0; i < sel.GetSize (); i++) {
		if (CFieldItem * p = (CFieldItem *)m_lv.GetCtrlData (sel [i])) {
			if (CBarcodeElement::IsValidSymbology (nID)) {
				p->m_nClassID = BARCODE;
				p->m_nSymbology = nID;
				m_lv.UpdateCtrlData (p);
			}
			else {
				switch (nID) {
				case ID_ELEMENT_TEXT:		p->m_nClassID = TEXT;		break;
				case ID_ELEMENT_BITMAP:		p->m_nClassID = BMP;		break;
				case ID_ELEMENT_BARCODE:	p->m_nClassID = BARCODE;	break;
				}

				m_lv.UpdateCtrlData (p);
			}
		}
	}
}
