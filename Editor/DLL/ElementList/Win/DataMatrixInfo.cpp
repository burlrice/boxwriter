// DataMatrixInfo.cpp: implementation of the CDataMatrixInfo class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DataMatrixInfo.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetElements;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDataMatrixInfo::CDataMatrixInfo()
:	m_nLength (0)
{

}

CDataMatrixInfo::CDataMatrixInfo (const CDataMatrixInfo & rhs)
:	m_nLength (rhs.m_nLength)
{
}

CDataMatrixInfo & CDataMatrixInfo::operator = (const CDataMatrixInfo & rhs)
{
	if (this != &rhs) {
		m_nLength = rhs.m_nLength;
	}

	return * this;
}

CDataMatrixInfo::~CDataMatrixInfo()
{
}

bool CDataMatrixInfo::IsValidLength(int nLen)
{
	return nLen >= 0;
}

bool CDataMatrixInfo::IsValid() const
{
	return IsValidLength (m_nLength);
}
