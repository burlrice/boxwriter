#pragma once

#include "PrinterFont.h"
#include "BaseElement.h"
#include "fj_element.h"
#include "fj_datetime.h"
#include "PrinterFont.h"
#include "ListCtrlImp.h"
#include "fj_printhead.h"
#include "fj_message.h"
#include "fj_system.h"

#define FORWARD_DECLARE(fwd_class_name, real_class_name) \
	struct fwd_class_name; \
	typedef struct fwd_class_name real_class_name, FAR * LP##real_class_name, FAR * const CLP##real_class_name; \
	typedef const struct fwd_class_name FAR * LPC##real_class_name, FAR * const CLPC##real_class_name; 

#define DECLARE_MEMBERPOINTERACCESS(class_pointer_name, member_pointer_name) \
	public: \
		const class_pointer_name GetMember () const \
		{ \
			const class_pointer_name p = (const class_pointer_name)member_pointer_name; \
			ASSERT (p); \
			return p; \
		} \
		operator const class_pointer_name () const { return GetMember (); } 


FORWARD_DECLARE (fjprinthead,	FJPRINTHEAD)
FORWARD_DECLARE (fjelement,		FJELEMENT)
FORWARD_DECLARE (fjfont,		FJFONT)
FORWARD_DECLARE (fjsystem,		FJSYSTEM)
FORWARD_DECLARE (fjlabel,		FJLABEL)
FORWARD_DECLARE (fjimage,		FJIMAGE)
FORWARD_DECLARE (fjbarcode,		FJBARCODE)
FORWARD_DECLARE (fjsysbarcode,	FJSYSBARCODE)
FORWARD_DECLARE (fjmessage,		FJMESSAGE)
FORWARD_DECLARE (fjdatamatrix,	FJDATAMATRIX)
FORWARD_DECLARE (fjbitmap,		FJBITMAP)
FORWARD_DECLARE (fjmemstor,		FJMEMSTOR)
FORWARD_DECLARE (fjtext,		FJTEXT)
FORWARD_DECLARE (fjcounter,		FJCOUNTER)
FORWARD_DECLARE (fjdatetime,	FJDATETIME)
FORWARD_DECLARE (fjdyntext,		FJDYNTEXT)
FORWARD_DECLARE (fjdynbarcode,	FJDYNBARCODE)

namespace FoxjetIpElements
{
	/*
	from fj_element.h
		FJ_TYPE_TEXT		    // 1 text
		FJ_TYPE_BARCODE		    // 2 bar code
		FJ_TYPE_BITMAP		    // 3 bitmap
		FJ_TYPE_DYNIMAGE	    // 4 dynamic image	
		FJ_TYPE_DATETIME	    // 5 date time	
		FJ_TYPE_COUNTER		    // 6 counter
		FJ_TYPE_DYNTEXT			// 7 dynamic text
		FJ_TYPE_DYNBARCODE		// 8 dynamic barcode
		FJ_TYPE_DATAMATRIX		// 9 data matrix
	*/

	typedef enum {
		UNKNOWN						= -1,
		FIRST						= 0,
		TEXT						= FIRST,
		BMP,						
		COUNT,
		DATETIME,
		BARCODE,
		DATAMATRIX,
		DYNAMIC_TEXT,
		DYNAMIC_BARCODE,
		USER,
		LAST						= DYNAMIC_BARCODE + 1,
	} IPELEMENTTYPE;

	typedef VOID (* PFROMSTRINGFCT) (LPFJELEMENT pfe, LPCSTR pStr);
	typedef PFROMSTRINGFCT LPPFROMSTRINGFCT;  

	typedef VOID (* PTOSTRINGFCT) (CLPCFJELEMENT pfe, LPSTR pStr);
	typedef PTOSTRINGFCT LPPTOSTRINGFCT;  

	void ELEMENT_API InitIpElements ();
	void ELEMENT_API FreeIpElements ();

	ELEMENT_API int GetElementType (const CString & strPrefix);

	LPPFROMSTRINGFCT ELEMENT_API GetFromStringFct (IPELEMENTTYPE type);
	LPPTOSTRINGFCT ELEMENT_API GetToStringFct (IPELEMENTTYPE type);
	LPFJSYSTEM ELEMENT_API GetSystem (ULONG lLineID);

	void ELEMENT_API LoadBarcodeParams (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);

	class ELEMENT_API CHead  
	{
	public:
		CHead(LPFJPRINTHEAD pHead);
		CHead (const CHead & rhs);
		CHead & operator = (const CHead & rhs);
		virtual ~CHead();

	public:
		bool GetFont (const CString & str, FoxjetCommon::CPrinterFont & fnt) const;
		bool GetFont (const CString & str, LPFJFONT * pResult) const;
		bool GetFonts (CArray <FoxjetCommon::CPrinterFont, FoxjetCommon::CPrinterFont &> & v) const;

		operator LPFJPRINTHEAD () { return m_pFjHead; }
		operator const LPFJPRINTHEAD () const { return m_pFjHead; }
		LPFJPRINTHEAD operator -> () { return m_pFjHead; }
		const LPFJPRINTHEAD operator -> () const { return m_pFjHead; }

		operator FoxjetDatabase::HEADSTRUCT & () { return m_dbHead; }
		operator const FoxjetDatabase::HEADSTRUCT & () const { return m_dbHead; }

		LPFJPRINTHEAD m_pFjHead;
		FoxjetDatabase::HEADSTRUCT m_dbHead;

		typedef enum { FONTS_OK, FONTS_RELOAD, FONTS_CANCEL } FONTSTATE;

		static FONTSTATE InitFonts ();
		static void GetFontPaths (CStringArray & v);
		static void SetFonts (LPFJPRINTHEAD pHead);
		static void FreeFonts (LPFJPRINTHEAD pHead);
		static bool GetFonts (LPCFJPRINTHEAD pHead, CArray <FoxjetCommon::CPrinterFont, FoxjetCommon::CPrinterFont &> &v);

		static const LPFJPRINTHEAD GetHead (const FoxjetDatabase::HEADSTRUCT & head);

	protected:

	};

	class ELEMENT_API CDatabaseParams
	{
	public:
		CDatabaseParams ();
		CDatabaseParams (const CDatabaseParams & rhs);
		CDatabaseParams & operator = (const CDatabaseParams & rhs);
		virtual ~CDatabaseParams ();

		CString m_strDSN;
		CString m_strTable;
		CString m_strField;
		CString m_strKeyField;
		CString m_strKeyValue;
		int m_nRow;
		bool m_bSQL;
		CString m_strSQL;
		int m_nSQLType;
	};

	class ELEMENT_API CTableItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CTableItem (LONG lID = 0, bool bPrompt = false, const CString & strPrompt = _T (""), const CString & strDefData = _T (""));
		CTableItem (const CTableItem & rhs);
		CTableItem & operator = (const CTableItem & rhs);
		virtual ~CTableItem ();

		virtual CString GetDispText (int nColumn) const;

		CString ToString () const;
		bool FromString (const CString & str);

		CString GetPrompt () const;
		bool UsesKeyField () const;
		CString GetDefData () const;

		CString CreateSQL () const;

		CString GetSqlResult () const;
		// throw CDBException, CMemoryException

		LONG			m_lID;
		bool			m_bPrompt;
		CString			m_strPrompt;
		CString			m_strData;
		bool			m_bUseDatabase;
		CDatabaseParams	m_params;
	};

	typedef CArray <CTableItem, CTableItem &> CItemArray;

	int ELEMENT_API GetDynamicData (ULONG lLineID, CItemArray & vItems);
	bool ELEMENT_API SetDynamicData (ULONG lLineID, CItemArray & vItems);

	class ELEMENT_API CIpBaseElement : public FoxjetCommon::CBaseTextElement
	{
		DECLARE_DYNAMIC (CIpBaseElement)
	public:
		CIpBaseElement (LPFJELEMENT pElement, LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CIpBaseElement (const CIpBaseElement & rhs);
		CIpBaseElement & operator = (const CIpBaseElement & rhs);
		virtual ~CIpBaseElement();

	public:
		static bool GetFonts (LPCFJPRINTHEAD pHead, CArray <FoxjetCommon::CPrinterFont, FoxjetCommon::CPrinterFont &> &v);

		virtual CSize Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly = false, FoxjetCommon::DRAWCONTEXT context = FoxjetCommon::EDITOR);
		virtual int GetClassID () const;
		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);

		virtual bool SetPos (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT * pHead, FoxjetCommon::ALIGNMENT_ADJUST adj = FoxjetCommon::ALIGNMENT_ADJUST_NONE);
		virtual CPoint GetPos (const FoxjetDatabase::HEADSTRUCT * pHead) const;
		virtual bool Move (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT & head);
		virtual bool SetXPos (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT * pHead);
		virtual bool SetYPos (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT * pHead);

		virtual CSize GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI = true) const;
		virtual void ClipTo (const FoxjetDatabase::HEADSTRUCT & head);
		virtual bool SetRedraw (bool bRedraw = true);

		virtual bool IsFlippedH () const;
		virtual bool IsFlippedV () const;
		virtual bool IsInverse () const;

		virtual bool SetFlippedH (bool bFlip);
		virtual bool SetFlippedV (bool bFlip);
		virtual bool SetInverse (bool bInverse);

		virtual UINT GetID () const;
		virtual bool SetID (UINT nID); 

		virtual bool HasFont () const;
		virtual bool SetFont (const FoxjetCommon::CPrinterFont & f);
		virtual bool GetFont (FoxjetCommon::CPrinterFont & f) const;
		virtual LONG GetBold () const;
		virtual bool SetBold (LONG l);
		virtual CString GetElementFontName () const = 0;

		virtual bool Validate ();
		virtual bool IsValid () const { return true; }

		LPCFJPRINTHEAD GetHead () const { return m_pHead; }
		LPFJPRINTHEAD GetHead () { return m_pHead; }

		LPCFJSYSTEM GetSystem () const;
		LPFJSYSTEM GetSystem ();

		LPCFJLABEL GetLabel () const;
		bool SetLabel (LPFJLABEL pLabel);

		LPVOID New ();
		LPVOID Dup (LPVOID pfd);
		void Destroy ();
		virtual void CreateImage ();
		void PhotocellBuild () const;

		long GetType () const;
		CString GetTypeString () const;

		static CSize GetImageSize (LPCFJIMAGE pfi);
		static LPBYTE ImageIpToWinRotate (LPCFJIMAGE pfi, LPBYTE pSrcBuffer, CSize & size);
		static LPBYTE ImageIpToWin (LPCFJIMAGE pfi, LPBYTE pSrcBuffer, CSize & size);

		static void AdjustYPos (LPFJELEMENT p, const FoxjetDatabase::HEADSTRUCT & head);
		
		static const FoxjetCommon::LIMITSTRUCT m_lmtBold;
		static const FoxjetCommon::LIMITSTRUCT m_lmtWidth;
		static const FoxjetCommon::LIMITSTRUCT m_lmtGap;
		static const FoxjetCommon::LIMITSTRUCT m_lmtString;

	protected:
		const LPFJELEMENT GetElement () const;
		LPFJELEMENT GetElement ();

		virtual bool SetElement (LPFJELEMENT pNew);
		virtual CBitmap * CreateBitmap (CDC & dc, FoxjetCommon::DRAWCONTEXT context);
		virtual void Invalidate ();

		LPFJPRINTHEAD m_pHead;
		LPFJELEMENT m_pElement;
		LPFJFONT m_pFont;
		LPFJLABEL m_pLabel;
		CBitmap * m_pBmp;
		FoxjetCommon::DRAWCONTEXT m_lastContext;
		LONG m_lLastBold;
	};


	class ELEMENT_API CTextElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CTextElement);

	public:
		CTextElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CTextElement (const CTextElement & rhs);
		CTextElement & operator = (const CTextElement & rhs);
		virtual ~CTextElement ();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		virtual void CreateImage ();
		virtual CString GetElementFontName () const;
		virtual bool SetFont (const FoxjetCommon::CPrinterFont & f);

		virtual bool SetDefaultData (const CString &strData);

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		DECLARE_MEMBERPOINTERACCESS (LPFJTEXT, GetElement ()->pDesc)

		virtual LONG GetBold () const;
		virtual bool SetBold (LONG lValue);

		virtual int GetWidth () const;
		virtual bool SetWidth (int nValue);
		
		LONG GetGap () const;
		bool SetGap (LONG lValue);

		CHAR GetAlignment (int) const;
		bool SetAlignment (CHAR cAlign);

		static CString GetFontDir ();

		static const FoxjetCommon::LIMITSTRUCT m_lmtString; 
		static const FoxjetCommon::LIMITSTRUCT m_lmtFontSize;

	protected:
		virtual bool SetElement (LPFJELEMENT pNew);

		CString m_strDefaultData;
		CString m_strImageData;
	};


	class ELEMENT_API CUserElement : public CTextElement  
	{
		DECLARE_DYNAMIC (CUserElement);

	public:
		CUserElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CUserElement (const CUserElement & rhs);
		CUserElement & operator = (const CUserElement & rhs);
		virtual ~CUserElement();

		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);
		virtual int GetClassID () const { return USER; }

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual bool SetDefaultData (const CString &strData);

		CString ToString (const FoxjetCommon::CVersion & ver, int nType);

		int GetMaxChars () const;
		bool SetMaxChars (int nMax);

		CString GetPrompt () const;
		bool SetPrompt (const CString & str);

		bool	m_bPromptAtTaskStart;

		static const FoxjetCommon::LIMITSTRUCT m_lmtString;

	protected:
		CString m_strPrompt;
		int		m_nMaxChars;
	};


	class ELEMENT_API CCountElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CCountElement);
	public:
		CCountElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CCountElement (const CCountElement & rhs);
		CCountElement & operator = (const CCountElement & rhs);
		virtual ~CCountElement ();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		virtual void CreateImage ();
		virtual CString GetElementFontName () const;
		virtual bool SetFont (const FoxjetCommon::CPrinterFont & f);

		virtual void Increment ();

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		DECLARE_MEMBERPOINTERACCESS (LPFJCOUNTER, GetElement ()->pDesc)

		virtual LONG GetBold () const;
		virtual bool SetBold (LONG lValue);

		virtual int GetWidth () const;
		virtual bool SetWidth (int nValue);
		
		LONG GetGap () const;
		bool SetGap (LONG lValue);

		LONG GetChange () const;
		bool SetChange (LONG l);

		LONG GetRepeat () const;
		bool SetRepeat (LONG l);

		LONG GetStart () const;
		bool SetStart (LONG l);

		LONG GetLimit () const;
		bool SetLimit (LONG l);

		LONG GetCurValue () const;
		bool SetCurValue (LONG l);

		LONG GetCurRepeat () const;
		bool SetCurRepeat (LONG l);

		LONG GetLength () const;
		bool SetLength (LONG l);

		CHAR GetAlign () const;
		bool SetAlign (CHAR c);

		CHAR GetFill () const;
		bool SetFill (CHAR c);

		static const FoxjetCommon::LIMITSTRUCT m_lmtRepeat;
		static const FoxjetCommon::LIMITSTRUCT m_lmtLimit;
		static const FoxjetCommon::LIMITSTRUCT m_lmtStart;
		static const FoxjetCommon::LIMITSTRUCT m_lmtLength;
		static const FoxjetCommon::LIMITSTRUCT m_lmtChange;
		static const FoxjetCommon::LIMITSTRUCT m_lmtCurValue;
		static const FoxjetCommon::LIMITSTRUCT m_lmtCurRepeat;

	protected:
		LPFJTEXT GetText ();
		CLPFJTEXT GetText () const;
	};

	class ELEMENT_API CBitmapElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CBitmapElement);
	public:
		CBitmapElement(LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CBitmapElement (const CBitmapElement & rhs);
		CBitmapElement & operator = (const CBitmapElement & rhs);
		virtual ~CBitmapElement();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		virtual void CreateImage ();
		virtual CString GetElementFontName () const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		void Free ();
		bool SetFilePath (const CString &strData, const FoxjetDatabase::HEADSTRUCT & head);
		CString GetFilePath () const;

		DECLARE_MEMBERPOINTERACCESS (LPFJBITMAP, GetElement ()->pDesc)

		LONG GetBold () const;
		bool SetBold (LONG lValue);

		virtual int GetWidth () const;
		virtual bool SetWidth (int nValue);
		
		static PBYTE LoadBitmap (const CString & strFile);
		static bool FileExists (const CString & strFile);

		static CString GetDefPath ();
		static CString GetDefFilename ();

		static CString AddSuffix (const CString & strFile);
		static CString RemoveSuffix (const CString & strFile);

		static const CString m_strSuffix;

	protected:
		virtual void Invalidate ();
		void LoadDefResBitmap(const FoxjetDatabase::HEADSTRUCT & head);

	public: 
		LPFJMEMSTOR m_pMemStore;
	};


	class ELEMENT_API CBarcodeElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CBarcodeElement);

	public:
		CBarcodeElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CBarcodeElement (const CBarcodeElement & rhs);
		CBarcodeElement & operator = (const CBarcodeElement & rhs);
		virtual ~CBarcodeElement ();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		bool SetDefaultData (const CString &strData);

		virtual void CreateImage ();
		virtual CString GetElementFontName () const;
		virtual bool SetElement (LPFJELEMENT pNew);

		virtual bool Validate ();
		virtual bool IsValid () const;

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		DECLARE_MEMBERPOINTERACCESS (LPFJBARCODE, GetElement ()->pDesc)

	public:

		bool GetOutputText () const;
		bool SetOutputText (bool bValue);

		LONG GetBold () const;
		bool SetBold (LONG lValue);
		
		CString GetText () const;
		bool SetText (const CString & strValue);
		
		LONG GetTextBold () const;
		bool SetTextBold (LONG lValue);
		
		virtual int GetWidth () const;
		virtual bool SetWidth (int nValue);
		
		LONG GetBcType () const;
		bool SetBcType (LONG lValue);
		
		CHAR GetTextAlign () const;
		bool SetTextAlign (CHAR cValue);

		const FJSYSBARCODE & GetGlobal () const;

		static bool IsValidGlobalIndex (int nIndex);

		static CString GetBcType (LONG lBCFlags);
		static CString GetBcName (LPFJSYSTEM pSystem, int nIndex);
		static CString GetBcType (LPCFJSYSTEM pSystem, int nIndex);

		static CString GetName (LPFJSYSTEM pSystem, int nIndex);
		static bool SetName (LPFJSYSTEM pSystem, int nIndex, const CString & str);
		
		static LONG GetVerticalBearer (LPFJSYSTEM pSystem, int nIndex);
		static bool SetVerticalBearer (LPFJSYSTEM pSystem, int nIndex, LONG lValue);
		
		static LONG GetHorizontalBearer (LPFJSYSTEM pSystem, int nIndex);
		static bool SetHorizontalBearer (LPFJSYSTEM pSystem, int nIndex, LONG lValue);
		
		static LONG GetBarValue (LPFJSYSTEM pSystem, int nGlobalIndex, int nBarIndex);
		static bool SetBarValue (LPFJSYSTEM pSystem, int nGlobalIndex, int nBarIndex, LONG lValue);

		static bool GetChecksum (LPFJSYSTEM pSystem, int nIndex);
		static bool SetChecksum (LPFJSYSTEM pSystem, int nIndex, bool bValue);

		static LONG GetHeight (LPFJSYSTEM pSystem, int nIndex);
		static bool SetHeight (LPFJSYSTEM pSystem, int nIndex, LONG lValue);
		
		static LONG GetQuietZone (LPFJSYSTEM pSystem, int nIndex);
		static bool SetQuietZone (LPFJSYSTEM pSystem, int nIndex, LONG lValue);

		static LONG GetBCFlags (LPCFJSYSTEM pSystem, int nIndex);
		static bool SetBCFlags (LPFJSYSTEM pSystem, int nIndex, LONG lValue);

		static CString GetMagName (int nIndex);
		static CString GetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
			ULONG lLineID, FoxjetDatabase::HEADTYPE type);
		static bool SetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
			ULONG lLineID, FoxjetDatabase::HEADTYPE type, const CString & strSettings);
		static bool SetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
			ULONG lLineID, FoxjetDatabase::HEADTYPE type, const CString & strSettings, LPFJSYSTEM pSystem);
		static CString GetHeadTypeString (FoxjetDatabase::HEADTYPE type);

		static CString GetDefaultData (ULONG lType);
	
		static const FoxjetCommon::LIMITSTRUCT m_lmtString;

	protected:
		virtual void Invalidate ();
		void LoadGlobalParams ();

		int m_nEncode;
	};


	class ELEMENT_API CDynamicBarcodeElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CDynamicBarcodeElement);

	public:
		CDynamicBarcodeElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CDynamicBarcodeElement (const CDynamicBarcodeElement & rhs);
		CDynamicBarcodeElement & operator = (const CDynamicBarcodeElement & rhs);
		virtual ~CDynamicBarcodeElement ();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual bool SetElement (LPFJELEMENT pNew);
		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		bool SetDefaultData (const CString &strData);

		virtual void CreateImage ();
		virtual CString GetElementFontName () const;

		virtual bool Validate ();
		virtual bool IsValid () const;
		virtual bool SetRedraw (bool bRedraw = true);

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		DECLARE_MEMBERPOINTERACCESS (LPFJDYNBARCODE, GetElement ()->pDesc)

	public:

		int GetDynamicID () const;
		bool SetDynamicID (LONG lID);

		bool GetOutputText () const;
		bool SetOutputText (bool bValue);

		LONG GetBold () const;
		bool SetBold (LONG lValue);
				
		LONG GetTextBold () const;
		bool SetTextBold (LONG lValue);
		
		virtual int GetWidth () const;
		virtual bool SetWidth (int nValue);
		
		LONG GetBcType () const;
		bool SetBcType (LONG lValue);
		
		CHAR GetTextAlign () const;
		bool SetTextAlign (CHAR cValue);

		LONG GetFirstChar () const;
		bool SetFirstChar (LONG l);

		LONG GetNumChar () const;
		bool SetNumChar (LONG l);

		const FJSYSBARCODE & GetGlobal () const;

	protected:
		virtual void Invalidate ();
		void LoadGlobalParams ();

		int m_nEncode;
		CString m_strData;
	};


	class ELEMENT_API CDataMatrixElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CDataMatrixElement);

	public:
		CDataMatrixElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CDataMatrixElement (const CDataMatrixElement & rhs);
		CDataMatrixElement & operator = (const CDataMatrixElement & rhs);
		virtual ~CDataMatrixElement ();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual bool SetDefaultData(const CString &strData);
		virtual void CreateImage ();
		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		virtual CString GetElementFontName () const { return _T (""); }
		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;
		
		LONG GetCX () const;
		bool SetCX (LONG l);

		LONG GetCY () const;
		bool SetCY (LONG l);

		LONG GetBcType () const;
		bool SetBcType (LONG lValue);

		int GetDynamicID () const;
		bool SetDynamicID (LONG lID);

		LONG GetFirstChar () const;
		bool SetFirstChar (LONG l);

		LONG GetNumChar () const;
		bool SetNumChar (LONG l);

		static const FoxjetCommon::LIMITSTRUCT m_lmtCX;
		static const FoxjetCommon::LIMITSTRUCT m_lmtCY;
		static const FoxjetCommon::LIMITSTRUCT m_lmtHeightFactor;
		static const FoxjetCommon::LIMITSTRUCT m_lmtWidthFactor;

		static CString GetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
			ULONG lLineID, FoxjetDatabase::HEADTYPE type);
		static bool SetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
			ULONG lLineID, FoxjetDatabase::HEADTYPE type, const CString & strSettings);
		static bool SetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
			ULONG lLineID, FoxjetDatabase::HEADTYPE type, const CString & strSettings, LPFJSYSTEM pSystem);
		static bool IsDataMatrixEnabled ();
		static void EnableDataMatrix (bool bEnable);

		DECLARE_MEMBERPOINTERACCESS (LPFJDATAMATRIX, GetElement ()->pDesc)
	};

	class ELEMENT_API CDateTimeElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CDateTimeElement);
	public:
		CDateTimeElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CDateTimeElement (const CDateTimeElement & rhs);
		CDateTimeElement & operator = (const CDateTimeElement & rhs);
		virtual ~CDateTimeElement ();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		virtual void CreateImage ();
		virtual CString GetElementFontName () const;
		virtual bool SetFont (const FoxjetCommon::CPrinterFont & f);

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		DECLARE_MEMBERPOINTERACCESS (LPFJDATETIME, GetElement ()->pDesc)

		virtual LONG GetBold () const;
		virtual bool SetBold (LONG lValue);

		virtual int GetWidth () const;
		virtual bool SetWidth (int nValue);
		
		LONG GetGap () const;
		bool SetGap (LONG lValue);

		FJDTFORMAT GetFormat () const;
		bool SetFormat (FJDTFORMAT fmt);

		FJDTTYPE GetTimeType () const;
		bool SetTimeType (FJDTTYPE type);

		CHAR GetDelim () const;
		bool SetDelim (CHAR c);
		
		BOOL GetStringNormal () const;
		bool SetStringNormal (BOOL b);
		
		LONG GetHourACount () const;
		bool SetHourACount (LONG l);

		LONG GetLength () const;
		bool SetLength (LONG l);

		CHAR GetAlign () const;
		bool SetAlign (CHAR c);

		CHAR GetFill () const;
		bool SetFill (CHAR c);

		static const FoxjetCommon::LIMITSTRUCT m_lmtHourACount;
		static const FoxjetCommon::LIMITSTRUCT m_lmtLength;

	protected:
		LPFJTEXT GetText ();
		CLPFJTEXT GetText () const;
	};


	class ELEMENT_API CDynamicTextElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CDynamicTextElement);

	public:

		CDynamicTextElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CDynamicTextElement (const CDynamicTextElement & rhs);
		CDynamicTextElement & operator = (const CDynamicTextElement & rhs);
		virtual ~CDynamicTextElement ();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual bool SetDefaultData(const CString &strData);
		virtual bool SetElement (LPFJELEMENT pNew);
		virtual void CreateImage ();
		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		virtual CString GetElementFontName () const;
		virtual bool SetFont (const FoxjetCommon::CPrinterFont & f);


		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		virtual LONG GetBold () const;
		virtual bool SetBold (LONG lValue);

		virtual int GetWidth () const;
		virtual bool SetWidth (int nValue);
		
		LONG GetGap () const;
		bool SetGap (LONG lValue);

		int GetDynamicID () const;
		bool SetDynamicID (LONG lID);

		LONG GetLength () const;
		bool SetLength (LONG l);

		LONG GetFirstChar () const;
		bool SetFirstChar (LONG l);

		LONG GetNumChar () const;
		bool SetNumChar (LONG l);

		CHAR GetAlign () const;
		bool SetAlign (CHAR c);

		CHAR GetFill () const;
		bool SetFill (CHAR c);
		
		DECLARE_MEMBERPOINTERACCESS (LPFJDYNTEXT, GetElement ()->pDesc)

		static const FoxjetCommon::LIMITSTRUCT m_lmtID;
		static const FoxjetCommon::LIMITSTRUCT m_lmtLen;

	};
};
