// SerialDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "SerialDlg.h"
#include "SerialElement.h"
#include "Parse.h"

using namespace FoxjetElements;
using namespace FoxjetCommon;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSerialDlg dialog


CSerialDlg::CSerialDlg (const FoxjetCommon::CBaseElement & e, const CSize & pa, 
						const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	CBaseTextDlg (e, pa, IDD_SERIAL, pList, pParent)
{
	//{{AFX_DATA_INIT(CSerialDlg)
	m_nIndex = 0;
	m_nLength = 1;
	//}}AFX_DATA_INIT
}


void CSerialDlg::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CSerialDlg)
	//}}AFX_DATA_MAP

	DDX_Text(pDX, TXT_INDEX, m_nIndex);
	DDV_MinMaxInt (pDX, m_nIndex, 0, CSerialElement::MAXBUFFLEN - 1);

	DDX_Text(pDX, TXT_LENGTH, m_nLength);
	DDV_MinMaxInt (pDX, m_nLength, 1, CSerialElement::MAXBUFFLEN - m_nIndex);

	DDX_Text(pDX, TXT_DEFAULT, m_strDefault);

	CBaseTextDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSerialDlg, CBaseTextDlg)
	//{{AFX_MSG_MAP(CSerialDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerialDlg message handlers

int CSerialDlg::BuildElement ()
{
	CSerialElement & e = GetElement ();
	CString strText, strFont, strDefault;

	GetDlgItemText (TXT_DEFAULT, strDefault);
//	GetDlgItemText (CB_FONT, strFont);

//	e.SetFontName (strFont);
//	e.SetBold (GetDlgItemInt (TXT_BOLD));
//	e.SetFlippedH (GetCheck (CHK_FLIPH));
//	e.SetFlippedV (GetCheck (CHK_FLIPV));
//	e.SetInverse (GetCheck (CHK_INVERSE));

	e.SetIndex (GetDlgItemInt (TXT_INDEX));
	e.SetLength (GetDlgItemInt (TXT_LENGTH));
	e.SetOrientation (GetOrientation ());
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));
	e.SetDefault (strDefault);

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

const FoxjetElements::CSerialElement & CSerialDlg::GetElement () const
{
	const CBaseElement & e = CBaseTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CSerialElement)));
	return (CSerialElement &)e;
}

FoxjetElements::CSerialElement & CSerialDlg::GetElement ()
{
	CBaseElement & e = CBaseTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CSerialElement)));
	return (CSerialElement &)e;
}

BOOL CSerialDlg::OnInitDialog() 
{
	BOOL bResult = CBaseTextDlg::OnInitDialog ();

	if (CWnd * p = GetDlgItem (TXT_DEFAULT))
		p->SetFont (&m_fntCtrl);

	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_TABLEINDEX)) {
		p->SetItemData (p->AddString (LoadString (IDS_NOTUSED)), -1);

		for (int i = 1; i <= 30; i++) {
			int nIndex = p->AddString (FoxjetDatabase::ToString (i));

			p->SetItemData (nIndex, i);

			if (m_nTableIndex == i) 
				p->SetCurSel (i);
		}

		if (p->GetCurSel () == CB_ERR)
			p->SetCurSel (0);
	}

	return bResult;
}

void CSerialDlg::OnOK ()
{
	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_TABLEINDEX)) 
		m_nTableIndex = p->GetItemData (p->GetCurSel ());

	CBaseTextDlg::OnOK ();
}

void CSerialDlg::Get (const FoxjetElements::CSerialElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Get (e, pHead);

	m_pt			= e.GetPos (pHead);
	m_nID			= e.GetID ();
	m_bFlipH		= e.IsFlippedH ();
	m_bFlipV		= e.IsFlippedV ();
	m_bInverse		= e.IsInverse ();
	m_nIndex		= e.GetIndex ();
	m_nLength		= e.GetLength ();
	m_nOrientation	= e.GetOrientation ();
	m_nWidth		= e.GetWidth ();
	m_strDefault	= e.GetDefault ();
	m_nTableIndex	= e.GetTableIndex ();
}

void CSerialDlg::Set (FoxjetElements::CSerialElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Set (e, pHead);

	e.SetAlignment (m_align);
	e.SetPos (m_pt = e.GetPosInternal (m_pt), pHead, ALIGNMENT_ADJUST_SUBTRACT);
	e.SetID (m_nID);
//	e.SetFlippedH (m_bFlipH ? true : false);
//	e.SetFlippedV (m_bFlipV ? true : false);
//	e.SetInverse (m_bInverse ? true : false);
	e.SetIndex (m_nIndex);
	e.SetLength (m_nLength);
//	e.SetFontName (m_strFont);
//	e.SetBold (m_lBold);
	e.SetOrientation (m_nOrientation);
	e.SetWidth (m_nWidth);
	e.SetDefault (m_strDefault);
	e.SetFontName (m_strFont);
	e.SetTableIndex (m_nTableIndex);
}
