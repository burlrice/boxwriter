#ifndef __DEFINE_H__
#define __DEFINE_H__
	// modifications to this file (ie, adding a new build config)
	// should be reflected in the "3 TEXTINCLUDE DISCARDABLE"
	// section of ElementList.rc.
	//
	// NOTES:
	// there are 2 sections by this name in the resource script, 
	// one near the top & one at the very bottom).
	//
	// preprocessr directives for the project are NOT applied to the
	// resource script unless specifically set for the particular .rc
	// file in the project settings.
	//
	// when adding a new resource script, import it into the 
	// ElementList resources as a custom resource (this insures 
	// a dependency from ElementList.rc to the new script).

	#ifdef __BUILD_ELEMENTS__

	#endif //__BUILD_ELEMENTS__
#endif //__DEFINE_H__