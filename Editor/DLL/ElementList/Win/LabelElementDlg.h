#if !defined(AFX_RFIDELEMENTDLG_H__8EB6DF01_BFAD_486D_9C36_0C59D10C3CD7__INCLUDED_)
#define AFX_RFIDELEMENTDLG_H__8EB6DF01_BFAD_486D_9C36_0C59D10C3CD7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LabelElementDlg.h : header file
//

#include "ElementDlg.h"
#include "resource.h"
#include "LabelElement.h"

/////////////////////////////////////////////////////////////////////////////
// CLabelElementDlg dialog
namespace FoxjetElements
{
	class CLabelElementDlg : public CElementDlg
	{
	// Construction
	public:
		CLabelElementDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
			const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CLabelElementDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		CString m_strFilePath;
		DWORD m_dwQty;

		void Get (const FoxjetElements::CLabelElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);
		void Set (FoxjetElements::CLabelElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CLabelElementDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual int BuildElement ();
		void UpdateUI ();

		CLabelElement & GetElement ();
		const CLabelElement & GetElement () const;

		static ULONG CALLBACK CLabelElementDlg::WaitForEdit (LPVOID lpData);

		bool m_bEditorRunning;

		// Generated message map functions
		//{{AFX_MSG(CLabelElementDlg)
		afx_msg void OnBrowse();
		afx_msg void OnEdit();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; // namespace FoxjetElements

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RFIDELEMENTDLG_H__8EB6DF01_BFAD_486D_9C36_0C59D10C3CD7__INCLUDED_)
