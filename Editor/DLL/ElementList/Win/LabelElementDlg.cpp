// RFIDElementDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "LabelElement.h"
#include "LabelElementDlg.h"
#include "Resource.h"
#include "TemplExt.h"

#include "List.h"
#include "Coord.h"
#include "Debug.h"
#include "Edit.h"
#include "Utils.h"
#include "Database.h"

using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLabelElementDlg dialog


CLabelElementDlg::CLabelElementDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
								 const FoxjetCommon::CElementList * pList, CWnd * pParent)
:	m_bEditorRunning (false),
	CElementDlg (e, pa, IDD_LABEL, pList, pParent)
{
	//{{AFX_DATA_INIT(CLabelElementDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CLabelElementDlg::DoDataExchange(CDataExchange* pDX)
{
	DDX_Text(pDX, TXT_FILEPATH, m_strFilePath);

	DDX_Text(pDX, TXT_BATCHQTY, m_dwQty);
	DDV_MinMaxDWord (pDX, m_dwQty, 0, ULONG_MAX);

	CElementDlg::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CLabelElementDlg, CElementDlg)
	//{{AFX_MSG_MAP(CLabelElementDlg)
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLabelElementDlg message handlers

void CLabelElementDlg::OnBrowse() 
{
	CFileDialog dlg (TRUE, _T ("*.lbl"), m_strFilePath, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("NiceLabel files (*.lbl)|*.lbl|All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		m_strFilePath = dlg.GetPathName ();
		UpdateData (FALSE);
	}
}

void CLabelElementDlg::OnEdit() 
{
	DWORD dw;

	HANDLE hThread = ::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0,
		(LPTHREAD_START_ROUTINE)WaitForEdit, (LPVOID)this, 0, &dw);
	ASSERT (hThread != NULL);
}

ULONG CALLBACK CLabelElementDlg::WaitForEdit (LPVOID lpData)
{
	ASSERT (lpData);

	TCHAR szPath [MAX_PATH] = { 0 };
	CLabelElementDlg & dlg = * (CLabelElementDlg *)lpData;
	CString strApp = CLabelElement::GetEditorPath ();
	CString strCmdLine = strApp + _T (" \"") + dlg.m_strFilePath + _T ("\"");
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [0xFF];

	_tcsncpy (szCmdLine, strCmdLine, 0xFF);
    memset (&pi, 0, sizeof(pi));
    memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	BOOL bResult = ::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi); 

	dlg.UpdateUI ();

	if (!bResult) {
		MsgBox (dlg, LoadString (IDS_FAILEDTOLANUCHEDITOR));
	}
	else {
		bool bMore;
		
		dlg.m_bEditorRunning = true;

		TRACEF (_T ("begin edit"));

		do {
			DWORD dwExitCode = 0;
			
			dlg.UpdateUI ();

			if (!GetExitCodeProcess (pi.hProcess, &dwExitCode))
				bMore = false;
			else if (!::IsWindow (dlg.m_hWnd))
				bMore = false;
			else {
				bMore = (dwExitCode == STILL_ACTIVE);
				WaitForSingleObjectEx(pi.hProcess, 3000, TRUE);
				TRACEF (_T ("still waiting"));
			}
		}
		while (bMore);

		TRACEF (_T ("edit complete"));
	}

	dlg.m_bEditorRunning = false;
	dlg.UpdateUI ();

	return 0;
}

void CLabelElementDlg::UpdateUI ()
{
	if (::IsWindow(m_hWnd)) {
		UINT nIDs [] = {
			BTN_BROWSE,
			BTN_EDIT,
			TXT_LOCATIONX,
			TXT_LOCATIONY,
			IDOK,
			IDCANCEL,
		};

		for (int i = 0; i < ARRAYSIZE (nIDs); i++)
			if (CWnd * p = GetDlgItem (nIDs [i]))
				p->EnableWindow (!m_bEditorRunning);
	}
}

CLabelElement & CLabelElementDlg::GetElement ()
{
	CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CLabelElement)));
	return (CLabelElement &)e;
}

const CLabelElement & CLabelElementDlg::GetElement () const
{
	const CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CLabelElement)));
	return (CLabelElement &)e;
}

int CLabelElementDlg::BuildElement ()
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();
	CLabelElement & e = GetElement ();
	CString strPath;

	GetDlgItemText (TXT_FILEPATH, strPath);

	e.SetFilePath (strPath);

	int nResult = e.Build ();
	e.Draw (* GetDC (), head, true);
	return nResult;
}

void CLabelElementDlg::Get (const FoxjetElements::CLabelElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	m_pt			= e.GetPos (pHead);
	m_nID			= e.GetID ();
	m_strFilePath	= e.GetFilePath ();
	m_dwQty			= e.GetBatchQuantity ();
}

void CLabelElementDlg::Set (FoxjetElements::CLabelElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	e.SetAlignment (m_align);
	e.SetPos (m_pt = e.GetPosInternal (m_pt), pHead, ALIGNMENT_ADJUST_SUBTRACT);
	e.SetID (m_nID);
	e.SetFilePath (m_strFilePath);
	e.SetBatchQuantity (m_dwQty);
}
