// PropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "List.h"
#include "PropertiesDlg.h"
#include "Extern.h"
#include "Database.h"
#include "Resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ListGlobals;
using namespace ItiLibrary;


/////////////////////////////////////////////////////////////////////////////
// CPropertiesDlg dialog

CPropertiesDlg::CPropertiesDlg(CWnd* pParent /*=NULL*/)
:	m_lBoxID (-1),
	m_lLineID (-1),
	m_lTaskID (-1),
	m_bReadOnly (false),
	FoxjetCommon::CEliteDlg(IDD_PROPERTIES, pParent)
{
	//{{AFX_DATA_INIT(CPropertiesDlg)
	m_strDesc = _T("");
	m_strDownload = _T("");
	m_strName = _T("");
	//}}AFX_DATA_INIT
}


void CPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropertiesDlg)
	DDX_Text(pDX, TXT_DESC, m_strDesc);
	DDX_Text(pDX, TXT_DOWNLOADSTRING, m_strDownload);
	DDX_Text(pDX, TXT_NAME, m_strName);
	DDX_Text(pDX, TXT_NOTES, m_strNotes);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropertiesDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CPropertiesDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropertiesDlg message handlers

BOOL CPropertiesDlg::OnInitDialog() 
{
	CArray <BOXSTRUCT, BOXSTRUCT &> vBoxes;
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_BOX);
	GetDlgItem (IDOK)->EnableWindow (!m_bReadOnly);


	ASSERT (pCB);
	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();

	if (IsNetworked ())
		GetNetworkedBoxRecords (GetDB (), m_lLineID, vBoxes);
	else
		GetBoxRecords (GetDB (), m_lLineID, vBoxes);

	for (int i = 0; i < vBoxes.GetSize (); i++) {
		int nIndex = pCB->AddString (vBoxes [i].m_strName);
		pCB->SetItemData (nIndex, vBoxes [i].m_lID);

		if (vBoxes [i].m_lID == m_lBoxID)
			pCB->SetCurSel (nIndex);
	}

	return bResult;
}

void CPropertiesDlg::OnOK()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_BOX);
	ASSERT (pCB);
	
	int nIndex = pCB->GetCurSel ();
	ASSERT (nIndex != CB_ERR);

	m_lBoxID = pCB->GetItemData (nIndex);
	FoxjetCommon::CEliteDlg::OnOK ();

}
