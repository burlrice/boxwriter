#ifndef __ELEMENT_API_H__
#define __ELEMENT_API_H__

#ifdef __BUILD_ELEMENTS__
	#define ELEMENT_API __declspec(dllexport)
#else
	#define ELEMENT_API __declspec(dllimport)
#endif //__BUILD_ELEMENTS__

#endif //__ELEMENT_API_H__