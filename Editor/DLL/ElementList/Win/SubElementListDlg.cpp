// SubElementListDlg.cpp: implementation of the CSubElementListDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SubElementListDlg.h"

#include "SubElementDlg.h"
#include "SubElement.h"
#include "CopyArray.h"
#include "ItiLibrary.h"
#include "Types.h"
#include "List.h"
#include "Debug.h"
#include "Extern.h"
#include "TemplExt.h"
#include "SerialElement.h"

using namespace ListGlobals;
using namespace ItiLibrary;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetElements;

CSubItem::CSubItem (const CSubElement * pElement)
:	m_pElement (pElement)
{
	ASSERT (m_pElement);
}

CSubItem::CSubItem (const CSubItem & rhs)
:	m_pElement (rhs.m_pElement)
{
	ASSERT (m_pElement);
}

CSubItem & CSubItem::operator = (const CSubItem & rhs)
{
	if (this != &rhs) {
		m_pElement = rhs.m_pElement;
		ASSERT (m_pElement);
	}

	return * this;
}

CSubItem::~CSubItem ()
{
}

int CSubItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	const CSubItem & cmp = (const CSubItem &)rhs;

	switch (nColumn) {
	default:	
		return GetDispText (nColumn).Compare (cmp.GetDispText (nColumn));
	case 3:
		return m_pElement->GetElement ()->GetClassID () - cmp.m_pElement->GetElement ()->GetClassID ();
	}

	return 0;
}

CString CSubItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0:
		str = m_pElement->m_strID;
		break;
	case 1:
		str = m_pElement->m_strDesc;
		break;
	case 2:
		str = m_pElement->GetDataMaskDisp ();
		break;
	case 3:
		str = FoxjetCommon::GetElementResStr (m_pElement->GetElement ()->GetClassID ());
		break;
	}

	return str;
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CSubElementListDlg::CSubElementListDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(IDD_EDITSUBELEMENT, pParent)
{
}

CSubElementListDlg::CSubElementListDlg(UINT nIDTemplate, CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(nIDTemplate, pParent)
{
}

CSubElementListDlg::~CSubElementListDlg ()
{
	for (int i = 0; i < m_vUpdated.GetSize (); i++)
		if (CSubElement * p = m_vUpdated [i])
			delete p;

	m_vUpdated.RemoveAll ();
}

void CSubElementListDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSubElementListDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSubElementListDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CSubElementListDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(BTN_PROPERTIES, OnProperties)
	ON_BN_CLICKED(BTN_ADD, OnAdd)
	ON_BN_CLICKED(BTN_DELETE, OnDelete)
	ON_NOTIFY(NM_DBLCLK, LV_SUBELEMENTS, OnDblclkListCtrl)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSubElementListDlg message handlers

BOOL CSubElementListDlg::OnInitDialog() 
{
	using ListCtrlImp::CColumn;

	CArray <CColumn, CColumn> vCols;

	vCols.Add (CColumn (LoadString (IDS_ID), 40));
	vCols.Add (CColumn (LoadString (IDS_DESC), 265));
	vCols.Add (CColumn (LoadString (IDS_DATAFORMAT), 70));
	vCols.Add (CColumn (LoadString (IDS_FIELDTYPE), 50));

	m_lv.Create (LV_SUBELEMENTS, vCols, this, defElements.m_strRegSection);

	for (int i = 0; i < m_vSubElements.GetSize (); i++) 
		m_lv.InsertCtrlData (new CSubItem (m_vSubElements [i]));

	m_lv->SortItems (ListCtrlImp::CListCtrlImp::CompareFunc, 0);

	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog ();

	HINSTANCE hInst = GetInstanceHandle (); 
	CButton * pButtons [] = {
		(CButton *)GetDlgItem (BTN_ADD),
		(CButton *)GetDlgItem (BTN_DELETE),
		(CButton *)GetDlgItem (BTN_PROPERTIES), 
	};
	UINT nIDs [] = {
		IDB_ADD_,
		IDB_DELETE,
		IDB_PROPERTIES,
	};

	for (int i = 0; i < 3; i++) {
		HBITMAP hBmp = ::LoadBitmap (hInst, MAKEINTRESOURCE (nIDs [i]));

		if (hBmp == NULL) {
			#ifdef _DEBUG
			CString str;
			str.Format (_T ("LoadBitmap (0x%08X, %u) failed"),
				hInst, nIDs [i]);
			TRACEF (str);
			ASSERT (0);
			#endif //_DEBUG
		}

		if (pButtons [i])
			pButtons [i]->SetBitmap (hBmp);
	}

	return bResult;
}

void CSubElementListDlg::OnProperties ()
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		CSubItem * p = (CSubItem *)m_lv.GetCtrlData (sel [0]);

		ASSERT (p);
		ASSERT (p->m_pElement);

		CSubElement * pEdit = new CSubElement (* p->m_pElement);

		if (OnEdit (* pEdit, IsAI (), false)) {
			m_vUpdated.Add (pEdit);
			p->m_pElement = pEdit;
			m_lv.UpdateCtrlData (p);
		}
		else
			delete pEdit;
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CSubElementListDlg::OnAdd ()
{
	CSubElement * pElement = new CSubElement (TEXT);
	CSubItem * p = new CSubItem (pElement);

	if (CSubItem * pTmp = (CSubItem *)m_lv.GetCtrlData (0)) 
		if (pTmp->m_pElement)
			pElement->SetLineID (pTmp->m_pElement->GetLineID ());

	ASSERT (p);
	ASSERT (pElement);

	pElement->m_strDataMask = CString (CSubElement::m_cAlphaNumericOpt, 10);
	TRACEF (pElement->m_strDataMask);
	pElement->SetData (CString ((TCHAR)'A', 10));

	if (OnEdit (* pElement, IsAI (), true)) {
		m_vUpdated.Add (pElement);
		bool bInsert = m_lv.InsertCtrlData (p);
		ASSERT (bInsert);
	}
	else {
		delete p;
		delete pElement;
	}
}

void CSubElementListDlg::OnDelete ()
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		int nResult = MsgBox (* this, LoadString (IDS_CONFIRMITEMDELETE), 
			LoadString (IDS_CONFIRM), MB_YESNO);

		if (nResult == IDYES) {
			for (int i = sel.GetSize () - 1; i >= 0; i--) {
				int nIndex = sel [i];

				CSubItem * p = (CSubItem *)m_lv.GetCtrlData (nIndex);
				ASSERT (p);

				m_vDeleted.Add (p->m_pElement->m_strID);
				m_lv.DeleteCtrlData (nIndex);
			}
		}
	}
	else 
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

bool CSubElementListDlg::OnEdit (CSubElement & s, bool bAI, bool bAdd) 
{
	using namespace TextElement;

	// have to instantiate a new one here because the ref passed in is non-const
	CSubElement constSub = s; 
	CSubElementDlg dlg (constSub, this);

	int nOldIndex = GetIdIndex (s.m_strID);
	
	dlg.m_nType			= s.GetElement ()->GetClassID ();
	dlg.m_bAI			= bAI;
	dlg.m_bReadOnlyID	= !bAdd;
	dlg.m_strDesc		= s.m_strDesc;
	dlg.m_strID			= s.m_strID;
	dlg.m_strMask		= s.m_strDataMask;
	dlg.m_strData		= s.GetElement ()->GetDefaultData ();

	while (true) {
		if (dlg.DoModal () == IDOK) {
			int nNewIndex = GetIdIndex (dlg.m_strID);

			// force the ID to be unique
			if (nNewIndex == nOldIndex || nNewIndex == -1) {
				CBaseElement * p = CopyElement (((const CSubElementDlg &)dlg).GetElement (), NULL, true);
				CTextElement * pText = DYNAMIC_DOWNCAST (CTextElement, p);

				ASSERT (pText);
				pText->Build ();
				
				s.SetElement (* pText);

				s.m_strDesc = dlg.m_strDesc;
				s.m_strID = dlg.m_strID;
				s.m_strDataMask = dlg.m_strMask;
				TRACEF (s.m_strDataMask);
				s.SetData (dlg.m_strData);

				if (CSerialElement * p = DYNAMIC_DOWNCAST (CSerialElement, s.GetElement ())) 
					p->SetLength (dlg.m_strData.GetLength ());


				delete p;

				return true;
			}
			else {
				CString str;
				CString strFmt = LoadString (bAI ? IDS_AIEXISTS : IDS_SUBELEMENTIDEXISTS);
				str.Format (strFmt, dlg.m_strID);
				MsgBox (* this, str);
			}
		}
		else {
			return false;
		}
	}
}

void CSubElementListDlg::SetData(CArray <FoxjetElements::CSubElement *, FoxjetElements::CSubElement *> & v)
{
	Copy (m_vSubElements, v);
}

int CSubElementListDlg::GetIdIndex(const CString &strID) const
{
	CListCtrl & lv = m_lv.GetListCtrl ();
	int nItems = lv.GetItemCount ();

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		CSubItem * p = (CSubItem *)m_lv.GetCtrlData (i);
		ASSERT (p);
		
		if (p->m_pElement->m_strID == strID)
			return i;
	}

	return -1;
}

void CSubElementListDlg::OnDblclkListCtrl(NMHDR* pNMHDR, LRESULT* pResult)
{
	OnProperties ();
	* pResult = 0;
}


void CSubElementListDlg::OnOK()
{
	FoxjetCommon::CEliteDlg::OnOK ();
}
