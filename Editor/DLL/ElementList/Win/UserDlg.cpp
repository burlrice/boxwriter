// UserDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "UserElement.h"
#include "UserDlg.h"
#include "TemplExt.h"
#include "Database.h"
#include "Color.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetElements;

#define IDM_USER_SPECIAL_SYSID		100
#define IDM_USER_SPECIAL_DATE		101
#define IDM_USER_SPECIAL_FIRST		102
#define IDM_USER_SPECIAL_LAST		103
#define IDM_USER_SPECIAL_USERNAME	104

/////////////////////////////////////////////////////////////////////////////
// CUserDlg dialog


CUserDlg::CUserDlg(const CBaseElement & e, const CSize & pa, 
				   const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_nMaxChars (15),
	CBaseTextDlg(e, pa, IDD_USERPROMPTED, pList, pParent)
{
	//{{AFX_DATA_INIT(CUserDlg)
	m_bPrompt = FALSE;
	m_strData = _T("");
	m_strPrompt = _T("");
	//}}AFX_DATA_INIT
}


void CUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseTextDlg::DoDataExchange(pDX);

	// must be dont first
	DDX_Text(pDX, TXT_MAXCHARS, m_nMaxChars);
	DDV_MinMaxInt (pDX, m_nMaxChars, CUserElement::m_lmtString.m_dwMin, CUserElement::m_lmtString.m_dwMax);

	if (pDX->m_bSaveAndValidate) {
		CString str;

		GetDlgItemText (TXT_TEXT, str);

		if (str.GetLength () > m_nMaxChars)
			m_nMaxChars = str.GetLength ();
	}

	DDX_Check(pDX, CHK_PROMPT, m_bPrompt);

	DDX_Text (pDX, TXT_TEXT, m_strData);
	//DDV_MinMaxChars (pDX, m_strData, TXT_TEXT, CUserElement::m_lmtString.m_dwMin, m_nMaxChars);

	DDX_Text(pDX, TXT_PROMPT, m_strPrompt);
	DDV_MinMaxChars (pDX, m_strPrompt, TXT_PROMPT, CUserElement::m_lmtString); 
}

BOOL CUserDlg::OnInitDialog() 
{
	OnDefaultClick ();
	CBaseTextDlg::OnInitDialog();
	
	return FALSE;
}


BEGIN_MESSAGE_MAP(CUserDlg, CBaseTextDlg)
	//{{AFX_MSG_MAP(CUserDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (RDO_NORMAL, OnOrientationClick)
	ON_BN_CLICKED (RDO_VERTICAL, OnOrientationClick)
	ON_BN_CLICKED (CHK_DEFAULT, OnDefaultClick)
	ON_EN_CHANGE (TXT_MAXCHARS, SetDefaultData)
	ON_BN_CLICKED (BTN_SPECIAL, OnSpecial)
	ON_WM_MEASUREITEM ()
	ON_WM_DRAWITEM()
	ON_COMMAND_RANGE (IDM_USER_SPECIAL_SYSID, IDM_USER_SPECIAL_USERNAME, OnSpecialCmd)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserDlg message handlers

int CUserDlg::BuildElement()
{
	CUserElement & e = GetElement ();
	CString str;

	GetDlgItemText (TXT_TEXT, str);
	e.SetDefaultData (str);
	e.SetOrientation (GetOrientation ());
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

CUserElement & CUserDlg::GetElement ()
{
	const CBaseElement & e = CBaseTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CUserElement)));
	return (CUserElement &)e;
}

const CUserElement & CUserDlg::GetElement () const
{
	const CBaseElement & e = CBaseTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CUserElement)));
	return (CUserElement &)e;
}

void CUserDlg::SetDefaultData ()
{
	CButton * pDef = (CButton *)GetDlgItem (CHK_DEFAULT);
	CEdit * pText = (CEdit *)GetDlgItem (TXT_TEXT);
	UINT nMax = GetDlgItemInt (TXT_MAXCHARS);

	ASSERT (pDef);
	ASSERT (pText);

	if (pDef->GetCheck () == 1) {
		CString str;

		GetDlgItemText (TXT_MAXCHARS, str);
		SetDlgItemText (TXT_TEXT, CString ('W', _ttol (str)));
	}

	//if (nMax > pText->GetLimitText ())
	//	pText->SetLimitText (nMax);
}

void CUserDlg::OnDefaultClick ()
{
	CButton * pDef = (CButton *)GetDlgItem (CHK_DEFAULT);
	CWnd * pData = GetDlgItem (TXT_TEXT);

	ASSERT (pDef);
	ASSERT (pData);

	bool bDef = pDef->GetCheck () == 1;

	SetDefaultData ();
	pData->EnableWindow (!bDef);

	if (!bDef)
		SetDlgItemText (TXT_TEXT, m_strData);
}

void CUserDlg::Get (const FoxjetElements::CUserElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Get (e, pHead);

	m_nID				= e.GetID ();
	m_pt				= e.GetPos (pHead);
	m_strData			= e.GetDefaultData ();
	m_nMaxChars			= e.GetMaxChars ();
	m_bPrompt			= e.m_bPromptAtTaskStart;
	m_strPrompt			= e.GetPrompt ();
	m_nOrientation		= e.GetOrientation ();
	m_nWidth			= e.GetWidth ();
}

void CUserDlg::Set (FoxjetElements::CUserElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Set (e, pHead);

	e.SetID (m_nID);
	e.SetAlignment (m_align);
	e.SetPos (m_pt = e.GetPosInternal (m_pt), pHead, ALIGNMENT_ADJUST_SUBTRACT);
	e.SetMaxChars (m_nMaxChars); // must be before SetDefaultData
	e.SetDefaultData (m_strData);
	e.m_bPromptAtTaskStart = m_bPrompt ? true : false;
	e.SetPrompt (m_strPrompt);
	e.SetOrientation (m_nOrientation);
	e.SetWidth (m_nWidth);
	e.SetFontName (m_strFont);
}

struct
{
	LPCTSTR m_lpsz;
	UINT	m_nCmdID;
}
static const mapMenu [] = 
{
	{ _T ("DATE"),		IDM_USER_SPECIAL_DATE,		},
	{ _T ("SYSID"),		IDM_USER_SPECIAL_SYSID,		},
	{ _T ("USERFIRST"),	IDM_USER_SPECIAL_FIRST,		},
	{ _T ("USERLAST"),	IDM_USER_SPECIAL_LAST,		},
	{ _T ("USERUSER"),	IDM_USER_SPECIAL_USERNAME,	},
};

void CUserDlg::OnSpecial()
{
	CMenu menu;
	CRect rc (0, 0, 0, 0);

	if (CWnd * p = GetDlgItem (BTN_SPECIAL))
		p->GetWindowRect (rc);

	menu.CreatePopupMenu ();

	for (int i = 0; i < ARRAYSIZE (::mapMenu); i++) {
		MENUITEMINFO info;

		memset (&info, 0, sizeof (info));

		info.cbSize		= sizeof (info);
		info.fMask		= MIIM_TYPE | MIIM_ID | MIIM_DATA | MIIM_SUBMENU;
		info.fType		= MFT_OWNERDRAW;
		info.wID		= ::mapMenu [i].m_nCmdID;
	
		/*
		switch (info.wID) {
		case x:
			info.hSubMenu = menuX;
			break;
		}
		*/

		::InsertMenuItem (menu, i, TRUE, &info);
	}

	if (!FoxjetDatabase::IsGojo ()) {
		VERIFY (menu.DeleteMenu (IDM_USER_SPECIAL_DATE, MF_BYCOMMAND));
		VERIFY (menu.DeleteMenu (IDM_USER_SPECIAL_SYSID, MF_BYCOMMAND));
	}

	menu.TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON, rc.right, rc.top, this);
}

void CUserDlg::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpmis) 
{	
	if (lpmis->CtlType == ODT_MENU) {
		lpmis->itemWidth = 200;
		lpmis->itemHeight = 50;
	}

	FoxjetCommon::CEliteDlg::OnMeasureItem (nIDCtl, lpmis);
}

void CUserDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	using namespace Color;
	using namespace FoxjetCommon;

	DRAWITEMSTRUCT dis;
	
	memcpy (&dis, lpDrawItemStruct, sizeof (dis));

	if (dis.CtlType == ODT_MENU) {
		bool bSelected = (dis.itemState & (ODS_SELECTED | ODS_FOCUS)) ? false : true;
		CString str;
		UINT nBitmapID = -1;
		CRect rc = dis.rcItem;
		CDC dc;
		MEASUREITEMSTRUCT m;
		const CSize sizeBitmap (32, 32);

		memset (&m, 0, sizeof (m));

		for (int i = 0; i < ARRAYSIZE (::mapMenu); i++) {
			if (::mapMenu [i].m_nCmdID == dis.itemID) {
				str			= ::mapMenu [i].m_lpsz;
				//nBitmapID	= ::mapMenu [i].m_nBitmapID;
				//TRACEF (str + _T (": ") + FoxjetDatabase::ToString ((int)nBitmapID));
				break;
			}
		}

		dc.Attach (dis.hDC);
		COLORREF rgbSetTextColor = dc.SetTextColor (bSelected ? Color::rgbCOLOR_WINDOWTEXT : Color::rgbCOLOR_HIGHLIGHTTEXT);
		int nSetBkMode = dc.SetBkMode (TRANSPARENT);
		dc.FillRect (rc, &CBrush (bSelected ? Color::rgbCOLOR_MENU : Color::rgbCOLOR_HIGHLIGHT));
		dc.DrawEdge (rc, EDGE_SUNKEN, BF_LEFT | BF_RIGHT | BF_TOP | BF_BOTTOM | BF_ADJUST);
		rc.left += sizeBitmap.cy + 5; 

		if (nBitmapID != -1) {
			CBitmap bmp;

			if (bmp.LoadBitmap (nBitmapID)) {
				CDC dcMem;
				DIBSECTION ds;

				memset (&ds, 0, sizeof (ds));
				bmp.GetObject (sizeof (ds), &ds);
				int x = 0;
				int y = rc.top + (rc.Height () - sizeBitmap.cy) / 2;

				dcMem.CreateCompatibleDC (&dc);
				CBitmap * pBitmap = dcMem.SelectObject (&bmp);
				COLORREF rgb = dcMem.GetPixel (0, 0);
				::TransparentBlt (dc, x, y, sizeBitmap.cx, sizeBitmap.cy, dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, rgb);
				dcMem.SelectObject (pBitmap);
			}
		}

		//{ CString strDebug; strDebug.Format (_T ("[%d] %s"), dis.itemID, str); TRACEF (strDebug); }
		dc.DrawText (str, rc, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_EXPANDTABS);
		dc.SetTextColor (rgbSetTextColor);
		dc.SetBkMode (nSetBkMode);
		dc.Detach ();
	}
	else 
		FoxjetCommon::CEliteDlg::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CUserDlg::OnSpecialCmd (UINT nID)
{
	for (int i = 0; i < ARRAYSIZE (::mapMenu); i++) {
		if (::mapMenu [i].m_nCmdID == nID) {
			SetDlgItemText (TXT_PROMPT, ::mapMenu [i].m_lpsz);
			break;
		}
	}
}