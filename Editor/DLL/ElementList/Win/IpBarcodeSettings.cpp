
#include "stdafx.h"
#include "IpElements.h"
//#include "List.h"
//#include "ListImp.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"
#include "Database.h"
#include "Resource.h"
//#include "Edit.h"
#include "TemplExt.h"

#include "fj_system.h"
#include "fj_printhead.h"
#include "fj_element.h"
#include "BarCode\barcode.h"
#include "fj_barcode.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_font.h"
//#include "fj_export.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetIpElements;
using namespace Color;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


static const CString strBcMag [] = 
{
	_T ("Mag 100"),
	_T ("Mag  90"),
	_T ("Mag  80"),
	_T ("Mag  70"),
	_T ("Mag  62"),
	_T ("Custom 1"),
	_T ("Custom 2"),
	_T ("Custom 3"),
};

static CString GetHeadTypeStringEnglish (HEADTYPE type) // for settings table only
{
	switch (type) {
	case GRAPHICS_768_256:
	case GRAPHICS_384_128:
		return _T ("256 channel");
	}

	return _T ("32 channel");
}

class CSettings
{
public:
	CSettings (ULONG lLineID = -1, const CString & strKey = _T (""), const CString & strData = _T (""));
	CSettings (const CSettings & rhs);
	CSettings & operator = (const CSettings & rhs);
	virtual ~CSettings ();

	ULONG	m_lLineID;
	CString m_strKey;
	CString m_strData;
};

static CArray <CSettings, CSettings &> vCache;
static int FindSetting (ULONG lLineID, const CString & strKey);
static void CacheSetting (ULONG lLineID, const CString & strKey, const CString & strSettings);

CSettings::CSettings (ULONG lLineID, const CString & strKey, const CString & strData)
:	m_lLineID (lLineID),
	m_strKey (strKey),
	m_strData (strData)
{
}

CSettings::CSettings (const CSettings & rhs)
:	m_lLineID (rhs.m_lLineID),
	m_strKey (rhs.m_strKey),
	m_strData (rhs.m_strData)
{
}

CSettings & CSettings::operator = (const CSettings & rhs)
{
	if (this != &rhs) {
		m_lLineID	= rhs.m_lLineID;
		m_strKey	= rhs.m_strKey;
		m_strData	= rhs.m_strData;
	}

	return * this;
}

CSettings::~CSettings ()
{
}

static int FindSetting (ULONG lLineID, const CString & strKey)
{
	for (int i = 0; i < ::vCache.GetSize (); i++) {
		const CSettings & s = ::vCache [i];

		if (s.m_lLineID == lLineID && s.m_strKey == strKey)
			return i;
	}

	return -1;
}

static void CacheSetting (ULONG lLineID, const CString & strKey, const CString & strSettings)
{
	int nIndex = FindSetting (lLineID, strKey);

	if (nIndex == -1) {
		CSettings s;

		s.m_lLineID = lLineID;
		s.m_strKey	= strKey;
		s.m_strData = strSettings;

		::vCache.Add (s);
	}
	else 
		::vCache [nIndex].m_strData = strSettings;
}

void ELEMENT_API FoxjetIpElements::LoadBarcodeParams (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	DBMANAGETHREADSTATE (db);

	CArray <LINESTRUCT, LINESTRUCT &> vLines;

	GetLineRecords (vLines);
	::vCache.RemoveAll ();

	for (int i = 0; i < vLines.GetSize (); i++) {
		LINESTRUCT & line = vLines [i];
		LPFJSYSTEM pSystem = GetSystem (line.m_lID);
		SETTINGSSTRUCT barcode;

		ASSERT (pSystem);

		barcode.m_strData = CBarcodeElement::GetBarcodeSettings (db, line.m_lID, HEADTYPE_FIRST);
		VERIFY (fj_SystemFromString (pSystem, w2a (barcode.m_strData)));

		barcode.m_strData = CDataMatrixElement::GetBarcodeSettings (db, line.m_lID, HEADTYPE_FIRST);
		VERIFY (fj_SystemFromString (pSystem, w2a (barcode.m_strData)));
	}
}

CString CBarcodeElement::GetMagName (int nIndex)
{
	CString str;

	if (nIndex >= 0 && nIndex < ARRAYSIZE (::strBcMag))
		str = ::strBcMag [nIndex];

	return str;
}

CString CBarcodeElement::GetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
											 ULONG lLineID, HEADTYPE type)
{
	DBMANAGETHREADSTATE (database);

	CString strResult = _T (
		"BC_N=0;BC_NAME=\"" + strBcMag [0] + "\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;"
		"BC_N=1;BC_NAME=\"" + strBcMag [1] + "\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;"
		"BC_N=2;BC_NAME=\"" + strBcMag [2] + "\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;"
		"BC_N=3;BC_NAME=\"" + strBcMag [3] + "\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;"
		"BC_N=4;BC_NAME=\"" + strBcMag [4] + "\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;"
		"BC_N=5;BC_NAME=\"" + strBcMag [5] + "\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;"
		"BC_N=6;BC_NAME=\"" + strBcMag [6] + "\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;"
		"BC_N=7;BC_NAME=\"" + strBcMag [7] + "\";BC_TY=8;BC_H=32;BC_CD=T;BC_HB=0;BC_VB=0;BC_QZ=0;BC_BASE=10;BC_B1=0;BC_B2=0;BC_B3=0;BC_B4=0;");
	SETTINGSSTRUCT s;
	CString strKey;

	strKey.Format (_T ("%s (%s)"), ListGlobals::Keys::m_strBarcode, GetHeadTypeStringEnglish (type));
	//TRACEF (strKey);
	int nCache = FindSetting (lLineID, strKey);

	if (nCache == -1) {
		if (GetSettingsRecord (database, lLineID, strKey, s)) 
			strResult = s.m_strData;

		CacheSetting (lLineID, strKey, strResult);
	}
	else 
		strResult = ::vCache [nCache].m_strData;

	return strResult;
}

bool CBarcodeElement::SetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
										  ULONG lLineID, HEADTYPE type, 
										  const CString & strSettings)

{
	DBMANAGETHREADSTATE (database);

	if (LPFJSYSTEM pSystem = ::GetSystem (lLineID)) 
		return SetBarcodeSettings (database, lLineID, type, strSettings, pSystem);

	return false;
}

bool CBarcodeElement::SetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
										  ULONG lLineID, HEADTYPE type, const CString & strSettings, LPFJSYSTEM pSystem)
{
	DBMANAGETHREADSTATE (database);

	if (pSystem) {
		SETTINGSSTRUCT s;
		CString strKey;
		char sz [1024] = { 0 };

		strKey.Format (_T ("%s (%s)"), ListGlobals::Keys::m_strBarcode, GetHeadTypeStringEnglish (type));

		s.m_lLineID = lLineID;
		s.m_strKey	= strKey;
		s.m_strData = strSettings;

		strncpy (sz, w2a (strSettings), sizeof (sz));
		VERIFY (fj_SystemFromString (pSystem, sz));
		CacheSetting (lLineID, strKey, strSettings);

		if (UpdateSettingsRecord (database, s))
			return true;
		else
			return AddSettingsRecord (database, s);
	}

	return false;
}

CString CBarcodeElement::GetHeadTypeString (HEADTYPE type)
{
	int nID = IDS_DEFAULTHEADTYPE;

	switch (type) {
	case GRAPHICS_768_256:
	case GRAPHICS_384_128:
		nID = IDS_GRAPHICS_768_256;
		break;
	}

	return LoadString (nID);
}

CString CDataMatrixElement::GetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
												ULONG lLineID, FoxjetDatabase::HEADTYPE type)
{
	DBMANAGETHREADSTATE (database);

	CString strResult = _T (
		"DM_N=0;DM_NAME=\"" + strBcMag [0] + "\";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;"
		"DM_N=1;DM_NAME=\"" + strBcMag [1] + "\";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;"
		"DM_N=2;DM_NAME=\"" + strBcMag [2] + "\";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;"
		"DM_N=3;DM_NAME=\"" + strBcMag [3] + "\";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;"
		"DM_N=4;DM_NAME=\"" + strBcMag [4] + "\";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;"
		"DM_N=5;DM_NAME=\"" + strBcMag [5] + "\";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;"
		"DM_N=6;DM_NAME=\"" + strBcMag [6] + "\";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;"
		"DM_N=7;DM_NAME=\"" + strBcMag [7] + "\";DM_H=1;DM_HB=12;DM_W=1;DM_WB=40;");
	SETTINGSSTRUCT s;
	CString strKey;

	strKey.Format (_T ("%s (%s)"), ListGlobals::Keys::NEXT::m_strDataMatrix, GetHeadTypeStringEnglish (type));
	int nCache = FindSetting (lLineID, strKey);

	if (nCache == -1) {
		if (GetSettingsRecord (database, lLineID, strKey, s)) 
			strResult = s.m_strData;

		CacheSetting (lLineID, strKey, strResult);
	}
	else 
		strResult = ::vCache [nCache].m_strData;

	return strResult;
}

bool CDataMatrixElement::SetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
											 ULONG lLineID, FoxjetDatabase::HEADTYPE type, 
											 const CString & strSettings)
{
	DBMANAGETHREADSTATE (database);

	if (LPFJSYSTEM pSystem = ::GetSystem (lLineID)) 
		return SetBarcodeSettings (database, lLineID, type, strSettings, pSystem);

	return false;
}

bool CDataMatrixElement::SetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
											 ULONG lLineID, FoxjetDatabase::HEADTYPE type, 
											 const CString & strSettings, LPFJSYSTEM pSystem)
{
	DBMANAGETHREADSTATE (database);

	if (pSystem) {
		SETTINGSSTRUCT s;
		CString strKey;
		char sz [1024] = { 0 };

		strKey.Format (_T ("%s (%s)"), ListGlobals::Keys::NEXT::m_strDataMatrix, GetHeadTypeStringEnglish (type));

		s.m_lLineID = lLineID;
		s.m_strKey	= strKey;
		s.m_strData = strSettings;

		strncpy (sz, w2a (strSettings), sizeof (sz));
		VERIFY (fj_SystemFromString (pSystem, sz));
		CacheSetting (lLineID, strKey, strSettings);

		if (UpdateSettingsRecord (database, s))
			return true;
		else
			return AddSettingsRecord (database, s);
	}

	return false;
}
