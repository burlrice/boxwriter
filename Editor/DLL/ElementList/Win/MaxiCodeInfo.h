// MaxiCodeInfo.h: interface for the CMaxiCodeInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAXICODEINFO_H__EA87F462_F387_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_MAXICODEINFO_H__EA87F462_F387_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ElementApi.h"

namespace FoxjetElements
{
	class ELEMENT_API CMaxiCodeInfo  
	{
	public:
		CMaxiCodeInfo();
		CMaxiCodeInfo (const CMaxiCodeInfo & rhs);
		CMaxiCodeInfo & operator = (const CMaxiCodeInfo & rhs);
		virtual ~CMaxiCodeInfo();

	public:
		bool IsValid () const;

		static bool IsValidPostalCode (const CString & str, int nMode);
		static bool IsValidCountryCode (const CString & str);
		static bool IsValidClassOfService (const CString & str);
		static bool IsValidMode (int nMode);
		static bool IsValidLength (int nLen);

		CString m_strPostalCode;
		CString m_strCountryCode;
		CString m_strClassOfService;
		int m_nDataLen;
		int m_nMode;
	};
}; //namespace FoxjetElements

#endif // !defined(AFX_MAXICODEINFO_H__EA87F462_F387_11D4_8FC6_006067662794__INCLUDED_)
