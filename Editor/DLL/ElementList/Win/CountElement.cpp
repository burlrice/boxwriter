// CountElement.cpp: implementation of the CCountElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CountElement.h"
#include "List.h"
#include "ElementDefaults.h"
#include "Edit.h"
#include "Debug.h"
#include "Extern.h"
#include "Database.h"
#include "Utils.h"
#include "TemplExt.h"
#include "Parse.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetElements;
using namespace FoxjetCommon::ElementFields;

CCount::CCount(__int64 lCount, __int64 lMin, __int64 lMax, __int64 lInc)
: m_lCount (lCount), m_lMin (lMin), m_lMax (lMax), m_lIncrement (lInc)
{
}

CCount::CCount (const CCount & rhs)
:	m_lCount (rhs.m_lCount),
	m_lMin (rhs.m_lMin),
	m_lMax (rhs.m_lMax),
	m_lIncrement (rhs.m_lIncrement)
{
}

CCount & CCount::operator = (const CCount & rhs)
{
	if (this != &rhs) {
		m_lCount = rhs.m_lCount;
		m_lMin = rhs.m_lMin;
		m_lMax = rhs.m_lMax;
		m_lIncrement = rhs.m_lIncrement;
	}

	return * this;
}

CString CCount::ToString (const FoxjetCommon::CVersion & ver) const
{
	CString str;
	str.Format (_T ("%I64d,%I64d,%I64d,%I64d"), m_lCount, m_lMin, m_lMax, m_lIncrement);
	return str;
}

bool CCount::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	__int64 x [4];

	if (_stscanf (str, _T ("%I64d,%I64d,%I64d,%I64d"), &x [0], &x [1], &x [2], &x [3]) == 4) {
		m_lCount		= x [0];
		m_lMin			= x [1];
		m_lMax			= x [2];
		m_lIncrement	= x [3];
		return true;
	}

	return false;
}

bool CCount::Increment ()
{
	bool bResult = false;

	m_lCount += m_lIncrement;

	if (m_lIncrement > 0) {
		if (m_lCount < m_lMin || m_lCount > m_lMax) {
			m_lCount = m_lMin;
			// TODO: rem //m_lCount = m_lMin + (m_lCount - m_lMax) - 1;
			bResult = true;
		}
	}
	else { // decrement
		if (m_lCount < m_lMin || m_lCount > m_lMax) {
			m_lCount = m_lMax;
			//TODO: rem //m_lCount = m_lMax - (m_lMin - m_lCount);
			bResult = true;
		}
	}

	return bResult;
}

bool CCount::SetCount (__int64 lCount)
{
	if (lCount >= m_lMin && lCount <= m_lMax) {
		m_lCount = lCount;
		return true;
	}

	return false;
}

bool CCount::SetMin (__int64 lMin)
{
	if (lMin >= 0 && lMin < I64_MAX - 1) {
		m_lMin = lMin;
		return true;
	}

	return false;
}

bool CCount::SetMax (__int64 lMax)
{
	if (lMax >= m_lMin && lMax <= I64_MAX) {
		m_lMax = lMax;
		return true;
	}

	return false;
}

bool CCount::SetIncrement (__int64 lIncrement)
{
	//if (lIncrement >= 0 && lIncrement < I64_MAX) {
	__int64 lMax = CCountElement::m_iLmtStart;
	__int64 lMin = -lMax;

	if (lIncrement > lMin && lIncrement < lMax) {
		m_lIncrement = lIncrement;
		return true;
	}

	return false;
}
////////////////////////////////////////////////////////////////////////////////

const __int64 CCountElement::m_iLmtStart	= 999999999999999999;
const LPCTSTR CCountElement::m_lpszDefName	= _T ("Count");

IMPLEMENT_DYNAMIC (CCountElement, CTextElement);

CCountElement::CCountElement(const FoxjetDatabase::HEADSTRUCT & head)
:	m_bLeadingZeros (true), m_nDigits (6), 
	m_cnt (1, 1, 999999, 1), m_cntPal (1, 1, 999, 1), m_bIsPalCount (false),
	m_strName (m_lpszDefName),
	m_bRollover (true),
	m_bMaster (false),
	m_bIrregularPalletSize (false),
	CTextElement (head)
{

}

CCountElement::CCountElement (const CCountElement & rhs)
:	m_bLeadingZeros (rhs.m_bLeadingZeros), 
	m_nDigits (rhs.m_nDigits), 
	m_cnt (rhs.m_cnt), m_cntPal (rhs.m_cntPal), 
	m_bIsPalCount (rhs.m_bIsPalCount),
	m_strName (rhs.m_strName),
	m_bRollover (rhs.m_bRollover),
	m_bMaster (rhs.m_bMaster),
	m_bIrregularPalletSize (rhs.m_bIrregularPalletSize),
	CTextElement (rhs)
{
}

CCountElement & CCountElement::operator = (const CCountElement & rhs)
{
	CTextElement::operator = (rhs);

	if (this != &rhs) {
		m_cnt					= rhs.m_cnt;
		m_cntPal				= rhs.m_cntPal;
		m_bIsPalCount			= rhs.m_bIsPalCount;
		m_bLeadingZeros			= rhs.m_bLeadingZeros;
		m_nDigits				= rhs.m_nDigits;
		m_strName				= rhs.m_strName;
		m_bRollover				= rhs.m_bRollover;
		m_bMaster				= rhs.m_bMaster;
		m_bIrregularPalletSize	= rhs.m_bIrregularPalletSize;
	}

	return * this;
}

CCountElement::~CCountElement()
{

}

// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
static const ElementFields::FIELDSTRUCT fields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType,		_T ("")						),	//	[00]	Prefix (required)
	ElementFields::FIELDSTRUCT (m_lpszID,				_T ("")						),	//	[01]	ID (required)
	ElementFields::FIELDSTRUCT (m_lpszX,				_T ("")						),	//	[02]	x (required)
	ElementFields::FIELDSTRUCT (m_lpszY,				_T ("")						),	//	[03]	y (required)
	ElementFields::FIELDSTRUCT (m_lpszFlipH,			_T ("0")					),	//	[04]	flip h
	ElementFields::FIELDSTRUCT (m_lpszFlipV,			_T ("0")					),	//	[05]	flip v
	ElementFields::FIELDSTRUCT (m_lpszInverse,			_T ("0")					),	//	[06]	inverse
	ElementFields::FIELDSTRUCT (m_lpszFontName,			_T ("Courier New")			),	//	[07]	font name
	ElementFields::FIELDSTRUCT (m_lpszFontSize,			_T ("32")					),	//	[08]	font size
	ElementFields::FIELDSTRUCT (m_lpszBold,				_T ("0")					),	//	[09]	bold
	ElementFields::FIELDSTRUCT (m_lpszItalic,			_T ("0")					),	//	[10]	italic
	ElementFields::FIELDSTRUCT (m_lpszCount,			_T ("1")					),	//	[11]	count
	ElementFields::FIELDSTRUCT (m_lpszCountMin,			_T ("0")					),	//	[12]	min
	ElementFields::FIELDSTRUCT (m_lpszCountMax,			_T ("999999")				),	//	[13]	max
	ElementFields::FIELDSTRUCT (m_lpszCountInc,			_T ("1")					),	//	[14]	inc
	ElementFields::FIELDSTRUCT (m_lpszPalCount,			_T ("1")					),	//	[15]	pal count
	ElementFields::FIELDSTRUCT (m_lpszPalCountMin,		_T ("0")					),	//	[16]	pal min	
	ElementFields::FIELDSTRUCT (m_lpszPalCountMax,		_T ("999")					),	//	[17]	pal max
	ElementFields::FIELDSTRUCT (m_lpszPalCountInc,		_T ("1")					),	//	[18]	pal inc
	ElementFields::FIELDSTRUCT (m_lpszLeadingZeros,		_T ("1")					),	//	[19]	leading zeros
	ElementFields::FIELDSTRUCT (m_lpszDigits,			_T ("6")					),	//	[20]	digits
	ElementFields::FIELDSTRUCT (m_lpszPalCountEnabled,	_T ("0")					),	//	[21]	pal count enabled
	ElementFields::FIELDSTRUCT (m_lpszOrient,			_T ("0")					),	//	[22]	orientation
	ElementFields::FIELDSTRUCT (m_lpszWidth,			_T ("0")					),	//	[23]	font width
	ElementFields::FIELDSTRUCT (m_lpszName,				CCountElement::m_lpszDefName),	//	[24]	name
	ElementFields::FIELDSTRUCT (m_lpszRollover,			_T ("1")					),	//	[25]	rollover
	ElementFields::FIELDSTRUCT (m_lpszMaster,			_T ("0")					),	//	[26]	master
	ElementFields::FIELDSTRUCT (m_lpszAlign,			_T ("0")					),	//  [27]	alignment
	ElementFields::FIELDSTRUCT (m_lpszPromptAtTaskStart,_T ("0")					),	//  [28]	prompt at task start
	ElementFields::FIELDSTRUCT (m_lpszDither,			_T ("0")					),	//  [29]
	ElementFields::FIELDSTRUCT (m_lpszColor,			_T ("")						),	//	[30]
	ElementFields::FIELDSTRUCT (),
};

CString CCountElement::ToString (const FoxjetCommon::CVersion & ver) const
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated

	CString str;
	CPrinterFont f = GetFont ();
	CPoint pt = GetPosInternal (ALIGNMENT_ADJUST_ADD);

	str.Format (_T ("{Count,%u,%u,%u,%d,%d,%d,%s,%u,%d,%d,%s,%s,%d,%d,%d,%d,%d,%s,%d,%d,%d,%d,%d,%06X}"),
		GetID (),
		pt.x, pt.y,
		IsFlippedH (),
		IsFlippedV (),
		IsInverse (),
		f.m_strName, f.m_nSize, f.m_bBold, f.m_bItalic,
		m_cnt.ToString (ver), 
		m_cntPal.ToString (ver), 
		m_bLeadingZeros, 
		m_nDigits, 
		m_bIsPalCount,
		(int)GetOrientation (),
		GetWidth (),
		GetName (),
		GetRollover (),
		IsMaster (),
		GetAlignment (),
		IsIrregularPalletSize (),
		GetDither (),
		GetColor ());

	return str;
}

bool CCountElement::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	CStringArray vstrToken;
	CPrinterFont f;

	Parse (str, ver, vstrToken);

	if (vstrToken [0].CompareNoCase (_T ("Count")) != 0) {
		TRACEF ("Wrong prefix \"" + vstrToken [0] + "\"");
		return false;
	}

	f.m_strName = vstrToken [7];
	f.m_nSize = (UINT)_tcstol (vstrToken [8], NULL, 10);
	f.m_bBold = _ttoi (vstrToken [9]) ? true : false;
	f.m_bItalic = _ttoi (vstrToken [10]) ? true : false;

	bool bResult = true;

	bResult &= SetID ((UINT)_tcstol (vstrToken [1], NULL, 10));
	bResult &= SetPos (CPoint (
		(UINT)_tcstol (vstrToken [2], NULL, 10), 
		(UINT)_tcstol (vstrToken [3], NULL, 10)), NULL);
	bResult &= SetFlippedH (_ttoi (vstrToken [4]) ? true : false);
	bResult &= SetFlippedV (_ttoi (vstrToken [5]) ? true : false);
	bResult &= SetInverse (_ttoi (vstrToken [6]) ? true : false);
	bResult &= SetFont (f);

	bResult &= SetMax (strtoul64 (vstrToken [13]));
	bResult &= SetIncrement (strtoul64 (vstrToken [14]));
	bResult &= SetCount (strtoul64 (vstrToken [11]));
	bResult &= SetPalMin (strtoul64 (vstrToken [16]));
	bResult &= SetPalMax (strtoul64 (vstrToken [17]));
	bResult &= SetPalIncrement (strtoul64 (vstrToken [18]));
	bResult &= SetPalCount (strtoul64 (vstrToken [15]));

	bResult &= SetLeadingZeros (_ttoi (vstrToken [19]) ? true : false);
	bResult &= SetDigits (_tcstoul (vstrToken [20], NULL, 10));
	bResult &= EnablePalletCount (_ttoi (vstrToken [21]) ? true : false);
	bResult &= SetOrientation ((TextElement::FONTORIENTATION)_ttoi (vstrToken [22]));
	bResult &= SetWidth (_ttoi (vstrToken [23]));
	bResult &= SetName (vstrToken [24]);
	bResult &= SetRollover (_ttoi (vstrToken [25]) ? true : false);
	bResult &= SetMaster (_ttoi (vstrToken [26]) ? true : false);
	bResult &= SetAlignment ((ALIGNMENT)_ttoi (vstrToken [27]));
	bResult &= SetIrregularPalletSize (_ttoi (vstrToken [28]) ? true : false);
	bResult &= SetDither (_ttoi (vstrToken [29]));
	bResult &= SetColor (_tcstoul (vstrToken [30], NULL, 16));

	return bResult;
}

bool CCountElement::GetRollover () const
{
	return m_bRollover;
}

bool CCountElement::SetRollover (bool bRollover)
{
	m_bRollover = bRollover;
	return true;
}

bool CCountElement::IsMaster () const
{
	return m_bMaster;
}

bool CCountElement::SetMaster (bool bMaster)
{
	m_bMaster = bMaster;
	return true;
}

int CCountElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	CString str = _T ("%I64u");//_T ("%lu");

	if (type == INITIAL) {
		if (!IsPalletCount ())
			m_cnt.SetCount (m_cnt.GetIncrement () > 0 ? m_cnt.GetMin () : m_cnt.GetMax ());
		else
			m_cnt.SetCount (m_cnt.GetMin ());

		m_cntPal.SetCount (m_cntPal.GetIncrement () > 0 ? m_cntPal.GetMin () : m_cntPal.GetMax ());
	}

	if (m_bLeadingZeros)
		str.Format (_T ("%%0%dI64u"), m_nDigits);//_T ("%%0%dlu"), m_nDigits);

	__int64 lCount = IsPalletCount () ? m_cntPal.GetCount () : m_cnt.GetCount ();

	m_strImageData.Format (str, lCount);

//	if (type == IMAGING) 
//		Increment ();

	return GetClassID ();
}

bool CCountElement::GetLeadingZeros() const
{
	return m_bLeadingZeros;
}

bool CCountElement::SetLeadingZeros(bool bValue)
{
	SetRedraw ();
	m_bLeadingZeros = bValue;
	return true;
}

int CCountElement::GetDigits() const
{
	return m_nDigits;
}

bool CCountElement::SetDigits(int nDigits)
{
	if (nDigits >= MINDIGITS && nDigits <= MAXDIGITS) {
		SetRedraw ();
		m_nDigits = nDigits;
		return true;
	}
	else
		return false;
		
}

void CCountElement::Increment ()
{
	m_cnt.Increment ();

	__int64 lCount = m_cnt.GetCount ();

	SetRedraw ();

	if (IsPalletCount ()) {
		CCount cnt (m_cnt);
		const bool bRollover = cnt.Increment ();
		
		if (bRollover) { 
			const __int64 lLast = m_cntPal.GetCount ();
			CCount cntPal (m_cntPal);

			if (m_cntPal.Increment ()) {
				int nBreak = 0;
				cntPal.Increment ();
			}

			m_cnt.Increment ();

			if (!GetRollover ()) { // sw0844
				// should stick at m_cntPal.GetMax and never roll over to a value greater than this

				const __int64 lCurrent = m_cntPal.GetCount ();

				if (GetPalIncrement () > 0) {
					if ((m_cntPal.GetCount () > m_cntPal.GetMax ()) || lCurrent < lLast) 
						m_cntPal.SetCount (m_cntPal.GetMax ()); 
				}
				else {
					if ((m_cntPal.GetCount () < m_cntPal.GetMin ()) || lCurrent != lLast) 
						m_cntPal.SetCount (m_cntPal.GetMin ()); 
				}
			}
		}

		lCount = m_cntPal.GetCount ();
	}
}

void CCountElement::IncrementTo (__int64 lCount)
{
	Reset ();

	ASSERT (lCount >= 0);

	__int64 lRolledOver = (__int64)(lCount / m_cnt.GetMax ());

	if (IsPalletCount ()) {
		const __int64 lRem = lCount - (lRolledOver * m_cnt.GetMax ());
		const __int64 lInc = m_cntPal.GetIncrement () > 0 ? m_cntPal.GetIncrement () : -m_cntPal.GetIncrement ();
		const __int64 lRange = (__int64)ceil ((m_cntPal.GetMax () - m_cntPal.GetMin () + 1) / (double)lInc);
		const __int64 lIndex = lRolledOver % lRange;

		if (m_cntPal.GetIncrement () < 0) {
			__int64 lPalletCountRev = m_cntPal.GetMax () - (lIndex * lInc);

			VERIFY (m_cntPal.SetCount (lPalletCountRev));
		}
		else {
			__int64 lPalletCount = m_cntPal.GetMin () + (lIndex * lInc);

			VERIFY (m_cntPal.SetCount (lPalletCount));
		}

		VERIFY (m_cnt.SetCount (lRem));

		if (!GetRollover ()) { // sw0844
			// should stick at m_cntPal.GetMax and never roll over to a value greater than this
			
			__int64 lTmp = m_cntPal.GetMin () + (lRolledOver * m_cntPal.GetIncrement ());

			if (GetPalIncrement () > 0) {
				if (lTmp > m_cntPal.GetMax ())
					VERIFY (m_cntPal.SetCount (m_cntPal.GetMax ())); 
			}
			else {
				if (lTmp < m_cntPal.GetMin ())
					VERIFY (m_cntPal.SetCount (m_cntPal.GetMin ())); 
			}
		}
	}
	else {
		__int64 lRem = lCount;
		__int64 lSpan = m_cnt.GetMax () - m_cnt.GetMin () + 1;
		__int64 lInc = m_cnt.GetIncrement () > 0 ? m_cnt.GetIncrement () : -m_cnt.GetIncrement ();

		if (m_cnt.GetIncrement () > 0) 
			lRem = m_cnt.GetMin () + ((lCount * lInc) % lSpan);
		else 
			lRem = m_cnt.GetMax () - ((lCount * lInc) % lSpan);

		VERIFY (m_cnt.SetCount (lRem));
	}
}

bool CCountElement::SetCount (__int64 lCount)
{
	const __int64 lMin = m_cnt.GetMin (), lMax = m_cnt.GetMax ();

	lCount = BindTo (lCount, lMin, lMax);

	if (IsPalletCount ()) {
		SetRedraw ();

		VERIFY (m_cntPal.SetCount (GetIncrement () > 0 ? m_cntPal.GetMin () : m_cntPal.GetMax ()));
		return m_cnt.SetCount (lCount);
	}

	VERIFY (m_cnt.SetCount (GetIncrement () > 0 ? lMin : lMax));

	for (__int64 i = 0; i < lCount; i++) 
		Increment ();

	SetRedraw ();

	return true;
}

__int64 CCountElement::GetCount () const
{
	return m_cnt.GetCount ();
}

bool CCountElement::SetMin (__int64 lMin)
{	
	if (m_cnt.SetMin (lMin)) {
		SetRedraw ();
		SetCount (lMin);

		return true;
	}

	return false;
}

__int64 CCountElement::GetMin () const
{
	return m_cnt.GetMin ();
}

bool CCountElement::SetMax (__int64 lMax)
{
	SetRedraw ();
	return m_cnt.SetMax (lMax);
}

__int64 CCountElement::GetMax () const
{
	return m_cnt.GetMax ();
}

bool CCountElement::SetIncrement (__int64 lIncrement)
{
	SetRedraw ();
	return m_cnt.SetIncrement (lIncrement);
}

__int64 CCountElement::GetIncrement () const
{
	return m_cnt.GetIncrement ();
}

bool CCountElement::IsPalletCount () const
{
	return m_bIsPalCount;
}

bool CCountElement::EnablePalletCount (bool bPalletCount)
{
	SetRedraw ();
	m_bIsPalCount = bPalletCount;
	return true;
}

bool CCountElement::SetPalCount (__int64 lCount)
{
	SetRedraw ();
	return m_cntPal.SetCount (lCount);
}

__int64 CCountElement::GetPalCount () const
{
	return m_cntPal.GetCount ();
}

bool CCountElement::SetPalMin (__int64 lMin)
{
	SetRedraw ();
	return m_cntPal.SetMin (lMin);
}

__int64 CCountElement::GetPalMin () const
{
	return m_cntPal.GetMin ();
}

bool CCountElement::SetPalMax (__int64 lMax)
{
	SetRedraw ();
	return m_cntPal.SetMax (lMax);
}

__int64 CCountElement::GetPalMax () const
{
	return m_cntPal.GetMax ();
}

bool CCountElement::SetPalIncrement (__int64 lIncrement)
{
	SetRedraw ();
	return m_cntPal.SetIncrement (lIncrement);
}

__int64 CCountElement::GetPalIncrement () const
{
	return m_cntPal.GetIncrement ();
}

const FoxjetCommon::ElementFields::FIELDSTRUCT * CCountElement::GetFieldBuffer () const
{
	return ::fields;
}

void CCountElement::Reset ()
{
	if (IsPalletCount ()) {
		m_cnt.SetCount (GetMin ());
		m_cntPal.SetCount (GetPalIncrement () > 0 ? GetPalMin () : GetPalMax ());
		//Increment ();
		//SetCount (GetPalIncrement () > 0 ? GetPalMin () : GetPalMax ());
	}
	else
		SetCount (GetIncrement () > 0 ? GetMin () : GetMax ());
}

CString CCountElement::GetName () const
{
	return m_strName;
}

bool CCountElement::SetName (const CString & str)
{
	m_strName = str;
	return true;
}

bool CCountElement::IsIrregularPalletSize () const
{
	return m_bIrregularPalletSize;
}

bool CCountElement::SetIrregularPalletSize (bool bPrompt)
{
	m_bIrregularPalletSize = bPrompt;
	return true;
}
