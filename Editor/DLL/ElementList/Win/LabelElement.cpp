#include "stdafx.h"

#include <math.h>

#include "LabelElement.h"
#include "List.h"
#include "Resource.h"
#include "ElementDefaults.h"
#include "Coord.h"
#include "Edit.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"
#include "Utils.h"
#include "TemplExt.h"
#include "Utils.h"
#include "Parse.h"
#include "Registry.h"
#include "Parse.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace Color;
using namespace FoxjetElements;
using namespace FoxjetDatabase;
using namespace ListGlobals;
using namespace FoxjetCommon::ElementFields;

CBitmap * CLabelElement::m_pbmpIcon			= NULL;
bool * CLabelElement::m_pbLabelEditorPresent	= NULL;


IMPLEMENT_DYNAMIC (CLabelElement, CBaseElement);

CLabelElement::CLabelElement(const FoxjetDatabase::HEADSTRUCT & head)
:	m_lBatchQuantity (1),
	CBaseElement (head)
{
	m_strFilePath = GetDefFilePath ();
}

CLabelElement::CLabelElement (const CLabelElement & rhs)
:	m_strFilePath (rhs.m_strFilePath),
	m_lBatchQuantity (rhs.m_lBatchQuantity),
	CBaseElement (rhs)
{
}

CLabelElement & CLabelElement::operator = (const CLabelElement & rhs)
{
	CBaseElement::operator = (rhs);

	if (this != &rhs) {
		m_strFilePath		= rhs.m_strFilePath;
		m_lBatchQuantity	= rhs.m_lBatchQuantity;
	}

	return * this;
}

CLabelElement::~CLabelElement()
{
}

// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated
static const ElementFields::FIELDSTRUCT fields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType,		_T ("")								),	// Prefix (required)
	ElementFields::FIELDSTRUCT (m_lpszID,				_T ("0")							),	// ID (required)
	ElementFields::FIELDSTRUCT (m_lpszX,				_T ("0")							),	// x (required)
	ElementFields::FIELDSTRUCT (m_lpszY,				_T ("0")							),	// y (required)
	ElementFields::FIELDSTRUCT (m_lpszFileName,			CLabelElement::GetDefFilePath ()		),	// file path
	ElementFields::FIELDSTRUCT (m_lpszBatchQuantity,	_T ("1")							),	// batch quantity
	ElementFields::FIELDSTRUCT (),
};

CString CLabelElement::ToString (const FoxjetCommon::CVersion & ver) const
{
	CStringArray v;

	v.Add (_T ("Label"));									// 0
	v.Add (FoxjetDatabase::ToString ((ULONG)GetID ()));		// 1
	v.Add (FoxjetDatabase::ToString (GetPos (NULL).x));		// 2
	v.Add (FoxjetDatabase::ToString (GetPos (NULL).y));		// 3
	v.Add (GetFilePath ());									// 4
	v.Add (FoxjetDatabase::ToString (GetBatchQuantity ()));	// 5

	return FoxjetDatabase::ToString (v);
}

bool CLabelElement::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
/*
	CStringArray vstrToken, vActual, vDef;
	const int nFields = GetFieldCount ();
	int i;
	
	for (i = 0; i < nFields; i++) 
		vstrToken.Add (GetFieldDef (i));

	Tokenize (str, vActual);

	if (vActual.GetSize () < 4) {
		TRACEF ("Too few tokens");
		return false;
	}

	Tokenize (ListGlobals::defElements.GetRegElementDefs (GetClassID (), ver), vDef);

	for (i = 1; i < min (vDef.GetSize (), nFields); i++)
		if (vDef [i].GetLength ())
			vstrToken [i] = vDef [i];

	for (i = 0; i < min (vActual.GetSize (), nFields); i++)
		if (vActual [i].GetLength ())
			vstrToken [i] = vActual [i];
*/
	CStringArray vstrToken;
	CPrinterFont f;

	Parse (str, ver, vstrToken);

	if (vstrToken [0].CompareNoCase (_T ("Label")) != 0) {
		TRACEF ("Wrong prefix \"" + vstrToken [0] + "\"");
		return false;
	}

	CString strFilename = UnformatString (vstrToken [4]);

	bool bResult = true;

	bResult &= SetID ((UINT)_tcstol (vstrToken [1], NULL, 10));
	bResult &= SetPos (CPoint (
		(UINT)_tcstol (vstrToken [2], NULL, 10), 
		(UINT)_tcstol (vstrToken [3], NULL, 10)), NULL);
	bResult &= SetFilePath (strFilename);
	bResult &= SetBatchQuantity (_tcstoul (vstrToken [5], NULL, 10));

	return bResult;
}

CSize CLabelElement::Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly, 
						  FoxjetCommon::DRAWCONTEXT context)
{
	CSize size = CBaseElement::ThousandthsToLogical (GetWindowRect (head).Size (), head);


	if (context == EDITOR) {
		if (!bCalcSizeOnly) {
			int x = 0, y = 0, cx = size.cx, cy = size.cy;

			dc.TextOut (0, 0, _T ("RFID: " + m_strFilePath));
		}
	}
	else {
		TRACEF (_T ("Warning: CLabelElement::Draw called in PRINTER context"));
	}

	return size;
}

CSize CLabelElement::GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI) const
{
	CSize size = CBaseElement::LogicalToThousandths (CSize (500, head.GetChannels () / 2), head);

	HDC hDC = ::GetDC (NULL);
	
	double dRes = head.GetRes (); //(double)head.m_nHorzRes / (double)head.m_nEncoderDivisor;
	int cx = (int)(((double)dRes / (double)::GetDeviceCaps (hDC, LOGPIXELSX)) * GetSystemMetrics (SM_CXICON) * 2);
	int cy = BindTo (::GetSystemMetrics (SM_CYICON) * 2, 0, (int)head.GetChannels ());
	const CSize sizeIcon (cx, cy);

	::ReleaseDC (NULL, hDC);

	size = CBaseElement::LogicalToThousandths (sizeIcon, head);

	return size;
}

int CLabelElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	return GetClassID ();
}

CString CLabelElement::GetFilePath() const
{
	return m_strFilePath;
}

CString CLabelElement::GetDefFilePath () 
{
	CString str = GetEditorPath ();
	TCHAR szTitle [MAX_PATH] = { 0 };
	LPCTSTR lpszDef = _T ("Def.lbl");

	GetFileTitle (str, szTitle, ARRAYSIZE (szTitle) - 1);

	int nIndex = str.Find (szTitle);

	if (nIndex != -1)
		str = str.Mid (0, nIndex) + lpszDef;
	else
		str = lpszDef;

	return str;
}

CString CLabelElement::GetDefaultData() const
{
	return GetFilePath ();
}

CString CLabelElement::GetImageData() const
{
	return GetDefaultData ();
}

bool CLabelElement::SetFilePath (const CString &strPath)
{
	m_strFilePath = strPath;
	SetRedraw (true);

	return true;
}

ULONG CLabelElement::GetBatchQuantity () const
{
	return m_lBatchQuantity;
}

bool CLabelElement::SetBatchQuantity (ULONG lQty)
{
	m_lBatchQuantity = lQty;
	return true;
}

void CLabelElement::ClipTo (const FoxjetDatabase::HEADSTRUCT & head)
{
	CBaseElement::ClipTo (head);

/*
	CSize size = GetSize (head);
	int cy = CBaseElement::LogicalToThousandths (CSize (0, head.Height ()), head).y;

	if (size.cy > cy) {
		size.cy = cy;
		SetSize (size, head);
	}
*/
}

const FoxjetCommon::ElementFields::FIELDSTRUCT * CLabelElement::GetFieldBuffer () const
{
	return ::fields;
}

void CLabelElement::OnDestroyNew ()
{
	CString str = GetFilePath ();

	if (::GetFileAttributes (str) != -1)
		VERIFY (::DeleteFile (str));
}

void CLabelElement::OnCreateNew (const FoxjetDatabase::HEADSTRUCT & head, const CString & strTask)
{
	HMODULE hModule = GetInstanceHandle ();

	if (HRSRC hRSRC = ::FindResource (hModule, MAKEINTRESOURCE (IDR_DEFLABEL), _T ("NiceLabel"))) {
		DWORD dwSize = ::SizeofResource (hModule, hRSRC);

		if (HGLOBAL hGlobal = ::LoadResource (hModule, hRSRC)) {
			if (LPVOID lpData = ::LockResource (hGlobal)) {
				const CString strDir = GetLabelDirectory (); 
				CString strFile = strDir + _T ("\\") + strTask + _T (".lbl");

				if (::GetFileAttributes (strDir) != FILE_ATTRIBUTE_DIRECTORY)
					VERIFY (::CreateDirectory (strDir, NULL));

				for (int i = 0; ::GetFileAttributes (strFile) != -1; i++) {
					strFile.Format (_T ("%s\\%s%d.lbl"),
						strDir,
						strTask,
						i + 1);
				}

				TRACEF (strFile);

				if (FILE * fp = _tfopen (strFile, _T ("wb"))) {
					if (fwrite (lpData, dwSize, 1, fp) == 1)
						SetFilePath (strFile);
	
					fclose (fp);
				}
			}
		}
	}
}

CString CLabelElement::GetLabelDirectory () 
{
	return GetHomeDir () + _T ("\\Labels");
}

bool CLabelElement::IsLabelEditorPresent ()
{
	if (!m_pbLabelEditorPresent) {
		m_pbLabelEditorPresent = new bool;
	
		* m_pbLabelEditorPresent = ::GetFileAttributes (CLabelElement::GetEditorPath ()) != -1;
	}

	ASSERT (m_pbLabelEditorPresent);

	return * m_pbLabelEditorPresent;
}

CString CLabelElement::GetEditorPath () 
{
	CString str = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER,
		ListGlobals::defElements.m_strRegSection + _T ("\\Label editor"), 
		_T ("Path"), _T ("C:\\Program Files\\EuroPlus\\NiceLabel\\Bin\\nice3.exe"));

	return str;
}

void CLabelElement::FreeStaticMembers ()
{
	if (m_pbmpIcon) {
		delete m_pbmpIcon;
		m_pbmpIcon = NULL;
	}

	if (m_pbLabelEditorPresent) {
		delete m_pbLabelEditorPresent;
		m_pbLabelEditorPresent = NULL;
	}
}

bool CLabelElement::SetEditorPath (const CString & str)
{
	FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER,
		ListGlobals::defElements.m_strRegSection + _T ("\\Label editor"), 
		_T ("Path"), str);

	FreeStaticMembers ();

	return true;
}

void CLabelElement::DrawIcon (CDC & dc, const CRect & rc)
{
	if (!CLabelElement::m_pbmpIcon) {
		int cx = ::GetSystemMetrics (SM_CXICON);
		int cy = ::GetSystemMetrics (SM_CYICON);
		WORD wIcon = 0;
		CString str = CLabelElement::GetEditorPath ();
		CDC dcIcon;

		CLabelElement::m_pbmpIcon = new CBitmap ();

		VERIFY (dcIcon.CreateCompatibleDC (&dc));
		VERIFY (CLabelElement::m_pbmpIcon->CreateCompatibleBitmap (&dc, cx, cy));
		
		CBitmap * pBitmap = dcIcon.SelectObject (CLabelElement::m_pbmpIcon);

		::BitBlt (dcIcon, 0, 0, cx, cy, NULL, 0, 0, WHITENESS);

		if (HICON hIcon = ::ExtractAssociatedIcon (NULL, str.GetBuffer (str.GetLength ()), &wIcon)) {
			CBitmap bmpShortcut;
			CDC dcShortcut;
			DIBSECTION ds;

			VERIFY (dcShortcut.CreateCompatibleDC (&dc));
			VERIFY (bmpShortcut.LoadBitmap (IDB_SHORTCUT));
			::ZeroMemory (&ds, sizeof (ds));
			VERIFY (bmpShortcut.GetObject (sizeof (ds), &ds));

			CBitmap * pBitmap = dcShortcut.SelectObject (&bmpShortcut);

			BOOL bDrawIcon = ::DrawIcon (dcIcon, 0, 0, hIcon);
			dcIcon.BitBlt (0, cy - ds.dsBm.bmHeight, ds.dsBm.bmWidth, ds.dsBm.bmHeight, &dcShortcut, 0, 0, SRCCOPY);

			dcShortcut.SelectObject (pBitmap);
		}

		dcIcon.SelectObject (pBitmap);
	}

	if (CLabelElement::m_pbmpIcon) {
		int nYOffset = 0;//abs (rc.Height () - ::GetSystemMetrics (SM_CYICONSPACING)) / 2;
		CDC dcIcon;
		DIBSECTION ds;

		VERIFY (dcIcon.CreateCompatibleDC (&dc));
		CBitmap * pBitmap = dcIcon.SelectObject (CLabelElement::m_pbmpIcon);
		::ZeroMemory (&ds, sizeof (ds));
		VERIFY (CLabelElement::m_pbmpIcon->GetObject (sizeof (ds), &ds));

		dc.StretchBlt (rc.left, rc.top + nYOffset, rc.Width (), rc.Height (), &dcIcon, 
			0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, SRCCOPY);

		dcIcon.SelectObject (pBitmap);
	}
}