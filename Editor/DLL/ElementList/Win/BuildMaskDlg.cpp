// BuildMaskDlg.cpp: implementation of the CBuildMaskDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BuildMaskDlg.h"
#include "SubElement.h"
#include "MaskDlg.h"
#include "CopyArray.h"
#include "ItiLibrary.h"
#include "List.h"
#include "Extern.h"

using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace ItiLibrary;
using namespace FoxjetElements;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


CMask::CMask ()
:	m_cChar (CSubElement::m_cDef),
	m_nLength (10)
{
	
}

CMask::CMask (const CMask & rhs)
:	m_cChar (rhs.m_cChar),
	m_nLength (rhs.m_nLength)
{
}

CMask & CMask::operator = (const CMask & rhs)
{
	if (this != &rhs) {
		m_cChar = rhs.m_cChar;
		m_nLength = rhs.m_nLength;
	}

	return * this;
}

CMask::~CMask ()
{
}

CString CMask::GetDispText (int nColumn) const 
{
	CString str;

	switch (nColumn) {
	case 0: 
		switch (CSubElement::GetMaskCharType (m_cChar)) {
		case CSubElement::ALPHA:
			str.LoadString (IDS_ALPHAONLY);
			break;
		case CSubElement::NUMERIC:
			str.LoadString (IDS_NUMERICONLY);
			break;
		case CSubElement::ALPHANUMERIC:
			str.LoadString (IDS_ALPHANUMERIC);
			break;
		case CSubElement::PUNCTUATION:
			str.LoadString (IDS_PUNCTUATION);
			break;
		case CSubElement::ANY:
			str.LoadString (IDS_ANY);
			break;
		}
		break;
	case 1: 
		str.Format (_T ("%d"), m_nLength);
		break;
	case 2: 
		str = LoadString (CSubElement::IsOptChar (m_cChar) ? IDS_YES : IDS_NO);
		break;
	}

	return str;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBuildMaskDlg::CBuildMaskDlg(CWnd* pParent /*=NULL*/)
:	m_bEditable (false), 
	FoxjetCommon::CEliteDlg(IDD_BUILDMASK, pParent)
{
	//{{AFX_DATA_INIT(CBuildMaskDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CBuildMaskDlg::~CBuildMaskDlg ()
{
}

void CBuildMaskDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBuildMaskDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBuildMaskDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CBuildMaskDlg)
	ON_BN_CLICKED(BTN_ADD, OnAdd)
	ON_BN_CLICKED(BTN_DELETE, OnDelete)
	ON_BN_CLICKED(BTN_DOWN, OnDown)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_BN_CLICKED(BTN_UP, OnUp)
	//}}AFX_MSG_MAP
	ON_NOTIFY(NM_DBLCLK, LV_MASK, OnDblclkListCtrl)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBuildMaskDlg message handlers

void CBuildMaskDlg::OnAdd() 
{
	CMask * p = new CMask;
	ASSERT (p);

	m_lv.InsertCtrlData (p);
	int nIndex = m_lv.GetDataIndex (p);

	if (!OnEdit (nIndex)) {
		m_lv.DeleteCtrlData (nIndex);
	}

	m_lv.RefreshCtrlData ();
}

void CBuildMaskDlg::OnDelete() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		int nResult = MsgBox (* this, LoadString (IDS_CONFIRMITEMDELETE), 
			LoadString (IDS_CONFIRM), MB_YESNO);

		if (nResult == IDYES) {
			for (int i = sel.GetSize () - 1; i >= 0; i--) 
				m_lv.DeleteCtrlData (sel [i]);
		}
	}
}

void CBuildMaskDlg::OnDown() 
{
	CLongArray sel = GetSelectedItems (m_lv);
	
	if (sel.GetSize ()) {
		// since the ctrl doesn't do column-click sorting,
		// the last selected item has the largest index in
		// the ctrl

		// can't shift down if the last item is selected
		if ((int)sel [sel.GetSize () - 1] < (m_lv->GetItemCount () - 1)) { 
			for (int i = 0; i < sel.GetSize (); i++) {
				CMask * p = new CMask (* (CMask *)m_lv.GetCtrlData (sel [i]));
				m_lv.DeleteCtrlData (sel [i]);
				m_lv.InsertCtrlData (p, sel [i] + 1);
			}

			m_lv.SelectAll (false); // clear selection

			// reselect items (refresh fct doesn't account for index shift)
			for (int i = 0; i < sel.GetSize (); i++) 
				m_lv->SetItemState (sel [i] + 1, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
		}
	}
}

void CBuildMaskDlg::OnUp() 
{
	CLongArray sel = GetSelectedItems (m_lv);
	
	if (sel.GetSize ()) {
		// since the ctrl doesn't do column-click sorting,
		// the first selected item has the smallest index in
		// the ctrl

		if (sel [0] > 0) { // can't shift up if the first item is selected
			int i;

			for (i = 0; i < sel.GetSize (); i++) {
				CMask * p = new CMask (* (CMask *)m_lv.GetCtrlData (sel [i]));
				m_lv.DeleteCtrlData (sel [i]);
				m_lv.InsertCtrlData (p, sel [i] - 1);
			}

			m_lv.SelectAll (false); // clear selection

			// reselect items (refresh fct doesn't account for index shift)
			for (i = 0; i < sel.GetSize (); i++) 
				m_lv->SetItemState (sel [i] - 1, (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
		}
	}
}

void CBuildMaskDlg::OnEdit() 
{
	if (m_bEditable) {
		CLongArray sel = GetSelectedItems (m_lv);

		if (sel.GetSize ()) {
			for (int i = 0; i < sel.GetSize (); i++) 
				OnEdit (sel [i]);
		}
		else
			MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
	}
}

BOOL CBuildMaskDlg::OnInitDialog() 
{
	using ListCtrlImp::CColumn;
	
	CStringArray v;
	CArray <CColumn, CColumn>  vCols;

	HINSTANCE hInst = GetInstanceHandle ();
	CButton * pUp = (CButton *)GetDlgItem (BTN_UP);
	CButton * pDown = (CButton *)GetDlgItem (BTN_DOWN);
	CButton * pAdd = (CButton *)GetDlgItem (BTN_ADD);
	CButton * pDel = (CButton *)GetDlgItem (BTN_DELETE);
	CButton * pEdit = (CButton *)GetDlgItem (BTN_EDIT);

	pUp->SetBitmap (::LoadBitmap (hInst, MAKEINTRESOURCE (IDB_UP)));
	pDown->SetBitmap (::LoadBitmap (hInst, MAKEINTRESOURCE (IDB_DOWN)));
	pAdd->SetBitmap (::LoadBitmap (hInst, MAKEINTRESOURCE (IDB_ADD_)));
	pDel->SetBitmap (::LoadBitmap (hInst, MAKEINTRESOURCE (IDB_DELETE)));
	pEdit->SetBitmap (::LoadBitmap (hInst, MAKEINTRESOURCE (IDB_PROPERTIES)));

	pUp->EnableWindow (m_bEditable);
	pDown->EnableWindow (m_bEditable);
	pAdd->EnableWindow (m_bEditable);
	pDel->EnableWindow (m_bEditable);
	pEdit->EnableWindow (m_bEditable);

	vCols.Add (CColumn (LoadString (IDS_MASKTYPE), 110));
	vCols.Add (CColumn (LoadString (IDS_LENGTH), 75));
	vCols.Add (CColumn (LoadString (IDS_VARIABLE), 65));
	m_lv.Create (LV_MASK, vCols, this, defElements.m_strRegSection);

	CSubElement::ParseMask (m_strMask, v);

	for (int i = 0; i < v.GetSize (); i++) {
		if (v [i].GetLength ()) {
			CMask * p = new CMask;
			p->m_cChar = v [i].GetAt (0);
			p->m_nLength = v [i].GetLength ();
			m_lv.InsertCtrlData (p);
		}
	}

	return FoxjetCommon::CEliteDlg::OnInitDialog();
}

void CBuildMaskDlg::OnOK()
{
	m_strMask = _T ("");

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		CMask * p = (CMask *)m_lv.GetCtrlData (i);
		CString str (p->m_cChar, p->m_nLength);
		m_strMask += str;
		
		if (i < m_lv->GetItemCount () - 1)
			m_strMask += CSubElement::m_cDelimeter;
	}

	FoxjetCommon::CEliteDlg::OnOK ();
}

bool CBuildMaskDlg::OnEdit(int nIndex)
{
	if (m_bEditable) {
		if (nIndex >= 0 && nIndex < m_lv->GetItemCount ()) {
			CMaskDlg dlg (this);
			CMask * p = (CMask *)m_lv.GetCtrlData (nIndex);
			ASSERT (p);

			dlg.m_bVariable = CSubElement::IsOptChar (p->m_cChar);
			dlg.m_nLength = p->m_nLength;
			dlg.m_nType = CSubElement::GetMaskCharType (p->m_cChar);

			if (dlg.DoModal () == IDOK) {
				p->m_cChar = CSubElement::GetMaskChar (dlg.m_nType, !dlg.m_bVariable);
				p->m_nLength = dlg.m_nLength;
				m_lv.UpdateCtrlData (p);
				
				return true;
			}
		}
	}

	return false;
}

void CBuildMaskDlg::OnDblclkListCtrl(NMHDR* pNMHDR, LRESULT* pResult)
{
	OnEdit ();
	* pResult = 0;
}
