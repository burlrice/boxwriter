#if !defined(AFX_HOURSDLG_H__62B62C1D_B345_42B1_8D66_D3B9C7906AC4__INCLUDED_)
#define AFX_HOURSDLG_H__62B62C1D_B345_42B1_8D66_D3B9C7906AC4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HoursDlg.h : header file
//

#include "DaysDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CHoursDlg dialog

class CHoursDlg : public CDaysDlg
{
	DECLARE_DYNCREATE(CHoursDlg)

protected:
	CHoursDlg () : CDaysDlg () { } // only for DYNCREATE

// Construction
public:
	CHoursDlg(FoxjetDatabase::COdbcDatabase & db, const CodesDlg::CCodesArray & vCodes);
	~CHoursDlg();

// Dialog Data
	//{{AFX_DATA(CHoursDlg)
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CHoursDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual CodesDlg::CCodes GetCodes (ULONG lLineID) const;
	virtual void Update (ULONG lLineID, UINT nIndex, const CString & strValue);

	CodesDlg::CCodesArray m_vCodes;

	// Generated message map functions
	//{{AFX_MSG(CHoursDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HOURSDLG_H__62B62C1D_B345_42B1_8D66_D3B9C7906AC4__INCLUDED_)
