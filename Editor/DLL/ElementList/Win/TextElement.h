// TextElement.h: interface for the CTextElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXTELEMENT_H__6CB6265D_8AD9_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_TEXTELEMENT_H__6CB6265D_8AD9_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "PrinterFont.h"	// Added by ClassView
#include "BaseElement.h"
#include "PrinterFont.h"
#include "Types.h"
#include "ElementApi.h"
#include "Utils.h"
#include "list.h"
#include "ximage.h"

namespace FoxjetElements
{
	const FoxjetCommon::LIMITSTRUCT lmtString	= { 0,	6000 };

	namespace TextElement
	{
		typedef enum { NORMAL, VERTICAL } FONTORIENTATION;

		class ELEMENT_API CTextElement : public FoxjetCommon::CBaseTextElement  
		{
			DECLARE_DYNAMIC (CTextElement);

		public:
			CTextElement(const FoxjetDatabase::HEADSTRUCT & head);
			CTextElement (const CTextElement & rhs);
			CTextElement & operator = (const CTextElement & rhs);
			virtual ~CTextElement();

			virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
			virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);
			virtual CSize Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly = false, FoxjetCommon::DRAWCONTEXT context = FoxjetCommon::EDITOR);
			virtual int GetClassID () const { return TEXT; }
			virtual CSize GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI = true) const;
			virtual void ClipTo (const FoxjetDatabase::HEADSTRUCT & head);

			CSize DrawBitmappedFont (int nIndex, CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, 
				bool bCalcSizeOnly, FoxjetCommon::DRAWCONTEXT context);

			virtual int Build (BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
			// throws CElementException

 			virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

			virtual bool SetFont (const FoxjetCommon::CPrinterFont & f);
			virtual bool GetFont (FoxjetCommon::CPrinterFont & f) const;

			virtual bool SetOrientation (FONTORIENTATION orient);
			virtual FONTORIENTATION GetOrientation (FoxjetCommon::DRAWCONTEXT context = FoxjetCommon::EDITOR) const;

			ALIGNMENT GetParaAlign () const;
			bool SetParaAlign (ALIGNMENT align);

			virtual bool IsParagraphMode () const;
			bool SetParaWidth (LONG l);
			LONG GetParaWidth () const;

			int GetParaGap () const;
			bool SetParaGap (int nGap);

			ULONG GetLink () const;
			bool SetLink (ULONG l);

			int Build (FoxjetCommon::CElementList & list);
			int Build (FoxjetCommon::CElementListArray * pAllLists);

			static const FoxjetCommon::LIMITSTRUCT m_lmtFontSize;
			static const FoxjetCommon::LIMITSTRUCT m_lmtString;
			static const FoxjetCommon::LIMITSTRUCT m_lmtParagraphGap;
			static const FoxjetCommon::LIMITSTRUCT m_lmtParagraphWidth;
			static const FoxjetCommon::LIMITSTRUCT m_lmtWidth;
			static const FoxjetCommon::LIMITSTRUCT m_lmtGap;

			static bool IsParagraphMode (const CString & str);
			static CxImage MakePatternDither (int nPercent, FoxjetCommon::DRAWCONTEXT context);
		
			CString Tokenize (const CString & str, CStringArray & v, CDC & dc);
			int CalcLen (const CString & str, CDC & dc);


		public:
			bool GetLogFont (LOGFONT & lf) const;
			bool SetHFont (HFONT hFont);
			HFONT GetHFont () const;

			virtual int GetWidth () const;
			virtual bool SetWidth (int nWidth);
			virtual bool IsResizable () const;
			virtual bool SetResizable (bool bResizable);
			virtual bool SetSize (const CSize & size, const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI = true);

			bool SetPrinterHFont(HFONT hFont);
			HFONT GetPrinterHFont () const;

			FoxjetCommon::CPrinterFont GetFont () const;
			CString GetImageData () const;
			static HFONT CreateFont (const CString & strFace, UINT & nSize, int nWidth, bool bBold, bool bItalic, FONTORIENTATION orient);
			virtual bool SetDefaultData (const CString & strData);
			virtual CString GetDefaultData () const;

		protected:

			CSize DrawParagraphMode (const CString & str, CDC & dc, 
				const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly, 
				FoxjetCommon::DRAWCONTEXT context);
			void DrawText (CDC & dc, const CStringArray & v, const CRect & rc, int nFormat, const FoxjetCommon::CPrinterFont & f);
		
			static CString Convert (const CString & strInput);
			ALIGNMENT GetParaAlign (int) const;

			CSize							m_size;
			CSize							m_sizeParagraph;
			bool							m_bResizable;
			HFONT							m_hFont;
			HFONT							m_hPrinterFont;
			CString							m_strDefaultData;
			CString							m_strImageData;
			FONTORIENTATION					m_orient;
			int								m_nHeight;
			int								m_nWidth;
			ALIGNMENT						m_alignPara;
			int								m_nParagraphGap;
			ULONG							m_lLink;

			CRect							m_rcEditor;
			CRect							m_rcPrinter;

		private:
			bool IsParaWidthEnabled () const;
		};
	}; //namespace TextElement 
}; //namespace FoxjetElements

#endif // !defined(AFX_TEXTELEMENT_H__6CB6265D_8AD9_11D4_8FC6_006067662794__INCLUDED_)
