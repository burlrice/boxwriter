// BarcodeElement.h: interface for the CBarcodeElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BARCODEELEMENT_H__6CB6265C_8AD9_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_BARCODEELEMENT_H__6CB6265C_8AD9_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "BaseElement.h"
#include "Types.h"
#include "Wasp\BcDraw.h"
#include "Wasp\Bcxl.h"
#include "SubElement.h"
#include "MaxiCodeInfo.h"
#include "DataMatrixInfo.h"
#include "ElementApi.h"
#include "BarcodeParams.h"
#include "Utils.h"
#include "AppVer.h"
#include "RawBitmap.h"

namespace FoxjetElements
{
	inline BYTE GetBarMask (int nBarHeight);

	using FoxjetCommon::DRAWCONTEXT;
	using FoxjetCommon::EDITOR;
	using FoxjetCommon::PRINTER;
	using FoxjetCommon::BUILDTYPE;
	using FoxjetCommon::INITIAL;
	using FoxjetCommon::IMAGING;
	using FoxjetCommon::IMAGING_REFRESH;
	using FoxjetCommon::ALIGNMENT_LEFT;
	using FoxjetCommon::ALIGNMENT_CENTER;
	using FoxjetCommon::ALIGNMENT_RIGHT;
	using FoxjetCommon::ALIGNMENT;
	using FoxjetCommon::ALIGNMENT_ADJUST_NONE;
	using FoxjetCommon::ALIGNMENT_ADJUST_SUBTRACT;
	using FoxjetCommon::ALIGNMENT_ADJUST_ADD;
	using FoxjetCommon::ALIGNMENT_ADJUST;

	class ELEMENT_API CTextInfo
	{
	public:
		CTextInfo ();
		CTextInfo (const TEXTINFO & rhs);
		CTextInfo (const CTextInfo & rhs);
		CTextInfo & operator = (const CTextInfo & rhs);
		virtual ~CTextInfo ();

		CString m_strText;
		bool	m_bAuto;
	};

	class ELEMENT_API CBarcodeException : public CException
	{
		DECLARE_DYNAMIC (CBarcodeException);

	public:
		CBarcodeException (int nWaspError, const FoxjetDatabase::HEADSTRUCT & head,
			int nSymbology, bool bAutoDelete = true);
		CBarcodeException (const CString & strError, bool bAutoDelete = true);
		virtual ~CBarcodeException ();

		virtual BOOL GetErrorMessage (LPTSTR lpszError, UINT nMaxError, PUINT pnHelpContext = NULL);
		CString GetErrorMessage () const;
		static CString GetErrorMessage (int nError, int nSymbology, const FoxjetDatabase::HEADSTRUCT & head);

	protected:
		CBarcodeException (const CBarcodeException & rhs);
		CBarcodeException & operator = (const CBarcodeException & rhs);
		 
		CString m_str;
	};

	class ELEMENT_API CBarcodeElement : public FoxjetCommon::CBaseElement  
	{
		DECLARE_DYNAMIC (CBarcodeElement);

	public:
		CBarcodeElement (const FoxjetDatabase::HEADSTRUCT & head);
		CBarcodeElement (const CBarcodeElement & rhs);
		CBarcodeElement & operator = (const CBarcodeElement & rhs);
		virtual ~CBarcodeElement();

		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);
		virtual CSize Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly = false, FoxjetCommon::DRAWCONTEXT context = EDITOR);
		virtual int GetClassID () const { return BARCODE; }
		virtual CString GetDefaultData () const;
		virtual CString GetImageData () const;
		virtual CSize GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI = true) const;
		virtual void ClipTo (const FoxjetDatabase::HEADSTRUCT & head);

		virtual int Build (BUILDTYPE type = INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual bool SetRedraw (bool bRedraw = true);
		
		virtual bool HasDither () const { return false; }

		virtual CString GetFieldName (int nIndex) const;
		virtual int GetFieldIndex (const CString & str) const;

		virtual void Reset ();
		virtual void Increment ();

		static void EnableDataMatrix (bool bEnable);
		static bool IsDataMatrixEnabled ();

		static const FoxjetCommon::LIMITSTRUCT m_lmtString;

	public:
		CMaxiCodeInfo GetMaxiCodeInfo () const;
		bool SetMaxiCodeInfo (const CMaxiCodeInfo & m);

		CDataMatrixInfo GetDataMatrixInfo () const;
		bool SetDataMatrixInfo (const CDataMatrixInfo & m);

		bool SetCaption (const CTextInfo & caption);
		CTextInfo GetCaption () const;
		
		static bool SymbologyHasCaption (int nSymbology);
		static bool SymbologyAllowsSubelements (int nSymbology);
		static bool SymbologyAllowsAI (int nSymbology);

		bool SetSymbology (int nSymbology);
		int GetSymbology () const;

		bool SetMagnification (ULONG lMagID);
		ULONG GetMagnification () const;

		bool SetWidth (int n);
		int GetWidth () const;

		bool SetHeight (int n);
		int GetHeight () const;

		bool IsValidMagnification (int nMag) const;
		static bool IsValidMagnification (int nSymbology, int nMag);
		static bool IsValidSymbology (int nSymbology);


		static int GetBarWidthMin (int nSymbology);
		static int GetBarWidthMax (int nSymbology);

		static bool IsValidBarWidth (int nSymbology, int nValue);
		static bool IsValidRatio (int nSymbology, int nValue);

		static bool WidthIsPercentage (int nSymbology);

		static void BcThousandthsToLogical (BARCODEINFO & bc, CDC & dc,
			FoxjetCommon::DRAWCONTEXT context);

		static const FoxjetCommon::LIMITSTRUCT m_lmtRatio;
		static const FoxjetCommon::LIMITSTRUCT m_lmtBarHeight;
		static const FoxjetCommon::LIMITSTRUCT m_lmtBarHeightVertical;
		static const FoxjetCommon::LIMITSTRUCT m_lmtMag;
		static const FoxjetCommon::LIMITSTRUCT m_lmtCX;
		static const FoxjetCommon::LIMITSTRUCT m_lmtCY;
		static const FoxjetCommon::LIMITSTRUCT m_lmtHeightFactor;
		static const FoxjetCommon::LIMITSTRUCT m_lmtWidthFactor;

		CString GetTextData () const;
		int GetSubElementCount () const;
		const CSubElement & GetSubElement (int nIndex) const;
		CSubElement & GetSubElement (int nIndex);
		bool DeleteSubElement (int nIndex);
		bool InsertSubElement (int nIndex, CSubElement & obj);
		void DeleteAllSubElements ();

		static bool IsValid (int nSymbology, CArray <FoxjetElements::CSubElement, FoxjetElements::CSubElement& > & vSubElements, bool bThrowException = false);
		// throw (CBarcodeException);
		bool IsValid (bool bThrowException = false) const;
		// throw (CBarcodeException);

		CSize DrawBarcode (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, CSize & sizeLogical,
			FoxjetCommon::DRAWCONTEXT context = EDITOR, bool bThrowException = false, 
			CBarcodeParams * pParams = NULL);
		// throw (CBarcodeException);

		bool IsAiCaption () const;
		CString FormatCaption () const;

		static void ClearLocalParams (ULONG lLocalParamUID);
		static void SetLocalParam (ULONG lLocalParamUID, int nSymbology, const CBarcodeParams & param);
		static CBarcodeParams GetLocalParam (ULONG lLocalParamUID, int nSymbology);


	protected:
		CSize DrawC128 (CDC &dc, const FoxjetDatabase::HEADSTRUCT & head, CSize & sizeLogical,
			FoxjetCommon::DRAWCONTEXT context, bool bThrowException,
			const CBarcodeParams & params, int nSymbology);
		// throw (CBarcodeException);

		static int DrawC128Symbol (const CString & str, const CString & strCaption, const CBarcodeParams & params, 
			const FoxjetDatabase::HEADSTRUCT & head, LPBYTE pBuffer = NULL, 
			UINT nBufferWidth = 0, ULONG lBufferSize = 0, CDC * pDC = NULL, CBitmap * pbmpCaption = NULL,
			FoxjetCommon::DRAWCONTEXT context = PRINTER,
			CString * pstrFormattedCaption = NULL);

		CSize DrawI2o5 (CDC &dc, const FoxjetDatabase::HEADSTRUCT & head, CSize & sizeLogical,
			FoxjetCommon::DRAWCONTEXT context, bool bThrowException,
			const CBarcodeParams & params);
		// throw (CBarcodeException);

		static int DrawI2o5Symbol (const CString & str, const CBarcodeParams & params, 
			const FoxjetDatabase::HEADSTRUCT & head, LPBYTE pBuffer = NULL, 
			UINT nBufferWidth = 0, ULONG lBufferSize = 0, CDC * pDC = NULL, CBitmap * pbmpCaption = NULL,
			FoxjetCommon::DRAWCONTEXT context = PRINTER,
			CString * pstrFormattedCaption = NULL);

		CSize DrawUPCEAN (CDC &dc, int nType, const FoxjetDatabase::HEADSTRUCT & head, CSize & sizeLogical,
			FoxjetCommon::DRAWCONTEXT context, bool bThrowException,
			const CBarcodeParams & params);
		// throw (CBarcodeException);

		static int DrawEAN8 (const CString & str, const CBarcodeParams & params, LPBYTE pBuffer = NULL, 
			UINT nBufferWidth = 0, ULONG nBufferSize = 0, CDC * pDC = NULL, CBitmap * pbmpCaption = NULL,
			FoxjetCommon::DRAWCONTEXT context = PRINTER,
			CString * pstrFormattedCaption = NULL);
		// throw (CBarcodeException);

		static int DrawEAN13 (const CString & str, const CBarcodeParams & params, LPBYTE pBuffer = NULL, 
			UINT nBufferWidth = 0, ULONG nBufferSize = 0, CDC * pDC = NULL, CBitmap * pbmpCaption = NULL,
			FoxjetCommon::DRAWCONTEXT context = PRINTER,
			CString * pstrFormattedCaption = NULL);
		// throw (CBarcodeException);

		static int DrawUPCA (const CString & str, const CBarcodeParams & params, LPBYTE pBuffer = NULL, 
			UINT nBufferWidth = 0, ULONG nBufferSize = 0, CDC * pDC = NULL, CBitmap * pbmpCaption = NULL,
			FoxjetCommon::DRAWCONTEXT context = PRINTER,
			CString * pstrFormattedCaption = NULL);
		// throw (CBarcodeException);

		static int DrawUPCE (const CString & str, const CBarcodeParams & params, LPBYTE pBuffer = NULL, 
			UINT nBufferWidth = 0, ULONG nBufferSize = 0, CDC * pDC = NULL, CBitmap * pbmpCaption = NULL,
			FoxjetCommon::DRAWCONTEXT context = PRINTER,
			CString * pstrFormattedCaption = NULL);
		// throw (CBarcodeException);

		static int DrawC39Symbol (const CString & str, const CString & strCaption, 
			const CBarcodeParams & params, const FoxjetDatabase::HEADSTRUCT & head, LPBYTE pBuffer = NULL, 
			UINT nBufferWidth = 0, ULONG lBufferSize = 0, CDC * pDC = NULL, CBitmap * pbmpCaption = NULL,
			FoxjetCommon::DRAWCONTEXT context = PRINTER,
			CString * pstrFormattedCaption = NULL);
		// throw (CBarcodeException);

		CSize DrawC39 (CDC &dc, const FoxjetDatabase::HEADSTRUCT & head, 
			CSize & sizeLogical, FoxjetCommon::DRAWCONTEXT context, 
			bool bThrowException, const CBarcodeParams & params);
		// throw (CBarcodeException);

		static int DrawC93Symbol (const CString & str, const CString & strCaption, 
			const CBarcodeParams & params, const FoxjetDatabase::HEADSTRUCT & head, LPBYTE pBuffer = NULL, 
			UINT nBufferWidth = 0, ULONG lBufferSize = 0, CDC * pDC = NULL, CBitmap * pbmpCaption = NULL,
			FoxjetCommon::DRAWCONTEXT context = PRINTER,
			CString * pstrFormattedCaption = NULL);
		// throw (CBarcodeException);

		CSize DrawC93 (CDC &dc, const FoxjetDatabase::HEADSTRUCT & head, 
			CSize & sizeLogical, FoxjetCommon::DRAWCONTEXT context, 
			bool bThrowException, const CBarcodeParams & params);
		// throw (CBarcodeException);

		CSize DrawDataMatrix (CDC &dc, const FoxjetDatabase::HEADSTRUCT & head, 
			CSize & sizeLogical, FoxjetCommon::DRAWCONTEXT context, 
			bool bThrowException, const CBarcodeParams & params);
		// throw (CBarcodeException);

		CSize DrawQR (CDC &dc, const FoxjetDatabase::HEADSTRUCT & head, 
			CSize & sizeLogical, FoxjetCommon::DRAWCONTEXT context, 
			bool bThrowException, const CBarcodeParams & params);
		// throw (CBarcodeException);

		static void DrawCaption (CBitmap & bmp, const CString & strCaption, const CBarcodeParams & params, 				  
			const FoxjetDatabase::HEADSTRUCT & head, CDC * pDC, 
			FoxjetCommon::DRAWCONTEXT context, int nWidth,
			CSize & bearer = CSize (0, 0));
		
		static void Blt (CDC &dc, const FoxjetDatabase::HEADSTRUCT & head, 
			FoxjetCommon::DRAWCONTEXT context, 
			const CBarcodeParams & params, int nWidth, int nHeight, LPBYTE pBuffer,
			CBitmap & bmp, CBitmap & bmpCaption, CSize & bearer = CSize (0, 0));

		static bool IsValid(int nSymbology, int nSubElements, bool bThrowException);
		// throw (CBarcodeException);

		void DrawBearer (CDC & dc, CSize bearer, FoxjetCommon::DRAWCONTEXT context,
			const CBarcodeParams & params, int nWidth, int nHeight,
			const FoxjetDatabase::HEADSTRUCT & head);

		static int StretchToPrinterRes (int n, const FoxjetDatabase::HEADSTRUCT & head);
		static UINT GetDataMatrixSizeConstant (int cx, int cy);

		void Free (bool bBitmapOnly = false);

		int SubElementsFromString (const CString &strSubElements, 
			const FoxjetCommon::CVersion & ver);
		void ParseSubElements (const CString & str, CStringArray & v);
		void PreParse (const CString & strFrom, CString & str, 
			CString & strSubElements);

		bool SetOrientation (FoxjetElements::TextElement::FONTORIENTATION orient);
		FoxjetElements::TextElement::FONTORIENTATION GetOrientation () const;

		CPtrArray												m_vSubElements;
		int														m_nSymbology;
		int														m_lMagID;
		CSize													m_size;
		FoxjetCommon::CRawBitmap *								m_pBmp;
		CString													m_strImageData;
		CString													m_strEncoding;
		CTextInfo												m_caption;
		CMaxiCodeInfo											m_maxicode;
		CDataMatrixInfo											m_datamatrix;
		FoxjetElements::TextElement::FONTORIENTATION			m_orient;
		int														m_nWidth;
		int														m_nHeight;

		typedef CMap <int, int, CBarcodeParams, CBarcodeParams>			CBcParamArray;
		static CMap <ULONG, ULONG, CBcParamArray *, CBcParamArray *>	m_mapParams;
	};
}; //namespace FoxjetElements

inline BYTE FoxjetElements::GetBarMask (int nBarHeight) 
{
	using namespace FoxjetCommon;

	const int nHeight = (int)ceil ((long double)nBarHeight / 8.0) * 8;
	int nDiff = nHeight - nBarHeight;
	BYTE nMask = nDiff ? 0 : ~0;

	if (nDiff)
		for (int i = 0; i < nDiff; i++)
			SetPixel (7 - i, &nMask, false);

	return nMask;
}

#endif // !defined(AFX_BARCODEELEMENT_H__6CB6265C_8AD9_11D4_8FC6_006067662794__INCLUDED_)
