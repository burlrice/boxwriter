#if !defined(AFX_EDITSHIFTCODEDLG_H__01223F15_F51D_41DE_BE24_EE4BE867FCF6__INCLUDED_)
#define AFX_EDITSHIFTCODEDLG_H__01223F15_F51D_41DE_BE24_EE4BE867FCF6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EditShiftCodeDlg.h : header file
//

#include "SelectShiftCodeDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CEditShiftCodeDlg dialog

class CEditShiftCodeDlg : public CSelectShiftCodeDlg
{
// Construction
public:
	CEditShiftCodeDlg(FoxjetDatabase::COdbcDatabase & database, const FoxjetCommon::CHead & head,
		CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEditShiftCodeDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditShiftCodeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();

	// Generated message map functions
	//{{AFX_MSG(CEditShiftCodeDlg)
	afx_msg void OnAdd();
	afx_msg void OnDelete();
	afx_msg void OnEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITSHIFTCODEDLG_H__01223F15_F51D_41DE_BE24_EE4BE867FCF6__INCLUDED_)
