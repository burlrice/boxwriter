// EditShiftCodeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EditShiftCodeDlg.h"
#include "resource.h"
#include "MsgBox.h"

using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditShiftCodeDlg dialog


CEditShiftCodeDlg::CEditShiftCodeDlg(FoxjetDatabase::COdbcDatabase & database, 
									 const FoxjetCommon::CHead & head,
									 CWnd * pParent)
	: CSelectShiftCodeDlg (database, head, IDD_EDITSHIFT, pParent)
{
	//{{AFX_DATA_INIT(CEditShiftCodeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CEditShiftCodeDlg::DoDataExchange(CDataExchange* pDX)
{
	CSelectShiftCodeDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditShiftCodeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEditShiftCodeDlg, CSelectShiftCodeDlg)
	//{{AFX_MSG_MAP(CEditShiftCodeDlg)
	ON_BN_CLICKED(BN_ADD, OnAdd)
	ON_BN_CLICKED(BN_DELETE, OnDelete)
	ON_BN_CLICKED(BN_EDIT, OnEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditShiftCodeDlg message handlers

void CEditShiftCodeDlg::OnAdd() 
{
	MsgBox (* this, _T ("OnAdd"));
}

void CEditShiftCodeDlg::OnDelete() 
{
	MsgBox (* this, _T ("OnDelete"));
}

void CEditShiftCodeDlg::OnEdit() 
{
	MsgBox (* this, _T ("OnEdit"));
}

void CEditShiftCodeDlg::OnOK()
{
	FoxjetCommon::CEliteDlg::OnOK (); // don't want to call CSelectShiftCodeDlg::OnOK
}
