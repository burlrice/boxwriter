// BitmapElement.cpp: implementation of the CBitmapElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "list.h"
#include "IpElements.h"
#include "Database.h"
#include "Registry.h"
#include "Extern.h"
#include "Debug.h"
#include "FileExt.h"

#include "fj_bitmap.h"
#include "fj_element.h"
#include "fj_printhead.h"
//#include "fj_export.h"
#include "fj_image.h"
#include "fj_message.h"
#include "fj_mem.h"
#include "fj_memstorage.h"
#include "fj_misc.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace FoxjetIpElements;
using namespace FoxjetDatabase;
using namespace FoxjetCommon::ElementFields;

const CString CBitmapElement::m_strSuffix = _T (".bmp");

IMPLEMENT_DYNAMIC (CBitmapElement, CIpBaseElement);

CBitmapElement::CBitmapElement(LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head)
:	m_pMemStore (NULL),
	CIpBaseElement (fj_ElementBitmapNew (), pHead, head)
{
	LoadDefResBitmap (head);
}

CBitmapElement::CBitmapElement (const CBitmapElement & rhs)
:	m_pMemStore (NULL),
 	CIpBaseElement (rhs)
{
	if (!SetFilePath (rhs.GetFilePath (), rhs.m_head))
		LoadDefResBitmap (rhs.m_head);
}

CBitmapElement & CBitmapElement::operator = (const CBitmapElement & rhs)
{
	CIpBaseElement::operator = (rhs);

	if (this != &rhs) {
		Free ();

		if (!SetFilePath (rhs.GetFilePath (), rhs.m_head))
			LoadDefResBitmap (rhs.m_head);
		
		SetBold (rhs.GetBold ());
		SetWidth (rhs.GetWidth ());
	}

	return * this;
}

CBitmapElement::~CBitmapElement()
{
	Free ();
}

void CBitmapElement::Free ()
{
	if (m_pMemStore) {
		fj_MemStorDestroy (m_pMemStore);
		m_pMemStore = NULL;
	}
}

int CBitmapElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	return GetClassID ();
}

CString CBitmapElement::GetImageData () const
{
	return GetDefaultData ();
}

CString CBitmapElement::GetDefaultData () const
{
	CString str;

	if (LPFJBITMAP p = GetMember ()) 
		str = p->strName;

	return str;
}

CString CBitmapElement::GetFilePath () const
{
	return AddSuffix (GetDefPath () + GetDefaultData ());
}

CString CBitmapElement::GetElementFontName () const
{
	return _T ("");
}

void CBitmapElement::CreateImage () 
{
	LPFJELEMENT pElement = GetElement ();
	LPFJMEMSTOR pMem = NULL;
	const HEADSTRUCT & head = GetDefaultHead ();
	bool bRestore = false;

	if (!FileExists (GetFilePath ())) 
		LoadDefResBitmap (head);

	if (m_pMemStore && pElement->pfm && pElement->pfm->pfph) {
		pMem = pElement->pfm->pfph->pMemStor;
		pElement->pfm->pfph->pMemStor = m_pMemStore;
		bRestore = true;
	}

	CIpBaseElement::CreateImage ();

	ASSERT (pElement->pfi);
	ASSERT (AfxIsValidAddress (pElement->pfi, sizeof (FJIMAGE)));

// 	if (pElement->pfi->lHeight && pElement->pfi->lLength)
//		pElement->pfi = fj_ImageRasterTransform (pElement->pfi, FJ_TRANS_NEGATIVE);

	if (bRestore)
		pElement->pfm->pfph->pMemStor = pMem;
}

PBYTE CBitmapElement::LoadBitmap (const CString & strFile)
{
	PBYTE pFile = NULL;
	HANDLE hFile = ::CreateFile (strFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 
		FILE_FLAG_SEQUENTIAL_SCAN, NULL);

	if (hFile != INVALID_HANDLE_VALUE) {
		DWORD dwHighSize = 0;
		DWORD dwBytesRead = 0;
		DWORD dwFileSize = GetFileSize (hFile, &dwHighSize);
		
		pFile = new BYTE [dwFileSize];
		BOOL bRead = ::ReadFile (hFile, pFile, dwFileSize, &dwBytesRead, NULL);

		if (bRead) {
			if (!fj_bmpCheckFormat (pFile)) {
				delete pFile;
				pFile = NULL;
			}
		}

		::CloseHandle (hFile);
	}

	if (pFile) { // TODO: rem: this is a hack so fj_ImageNegative will be called in fj_BitmapCreateImage
		BITMAPFILEHEADER * pbmfh = (BITMAPFILEHEADER *)pFile;
		BITMAPINFOHEADER * pbmih = (BITMAPINFOHEADER *)(pFile + sizeof(BITMAPFILEHEADER));
			
		pbmih->biXPelsPerMeter = 0;
		pbmih->biYPelsPerMeter = 0;
	}

	return pFile;
}

bool CBitmapElement::SetFilePath (const CString &strData, const FoxjetDatabase::HEADSTRUCT & head)
{
	bool bResult = false;

	if (LPFJBITMAP p = GetMember ()) {
		TCHAR sz [FJBMP_NAME_SIZE + 1] = { 0 };
		SHELLFLAGSTATE sfs;

		::SHGetSettings (&sfs, SSF_SHOWEXTENSIONS);
		::GetFileTitle (strData, sz, FJBMP_NAME_SIZE);
		CString strPath = AddSuffix (GetDefPath () + sz);
		CString strName = RemoveSuffix (sz);

		if (!strName.CompareNoCase (a2w (FJSYS_TEST_PATTERN))) {
			strncpy (p->strName, w2a (strName), FJBMP_NAME_SIZE);
			return true;
		}

		if (PBYTE pFile = LoadBitmap (strPath)) {
			BITMAPFILEHEADER * pbmfh = (BITMAPFILEHEADER *)pFile;
			LONG lSize = pbmfh->bfSize + (FJBMP_MEMSTOR_SIZE * 2);
			LPBYTE pMemStoreBuffer = (LPBYTE)fj_calloc (1, lSize);

			Free ();
			Invalidate ();

			ASSERT (pMemStoreBuffer);
			ASSERT (m_pMemStore == NULL);
			VERIFY (m_pMemStore = fj_MemStorNew (pMemStoreBuffer, lSize));
			m_pMemStore->bDoNotFreeBuffer = FALSE;

			strncpy (p->strName, w2a (strName), FJBMP_NAME_SIZE);
			VERIFY (fj_bmpAddToMemstor (m_pMemStore, (LPBYTE)pFile, w2a (strName)));

			bResult = true;
			delete pFile;
		}
	}

	return bResult;
}

LONG CBitmapElement::GetBold () const
{
	return GetMember ()->lBoldValue;
}

bool CBitmapElement::SetBold (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, CBitmapElement::m_lmtBold)) {
		Invalidate ();
		GetMember ()->lBoldValue = lValue;
		return true;
	}

	return false;
}

int CBitmapElement::GetWidth () const
{
	return GetMember ()->lWidthValue;
}

bool CBitmapElement::SetWidth (int nValue)
{
	if (FoxjetCommon::IsValid (nValue, CBitmapElement::m_lmtWidth)) {
		Invalidate ();
		GetMember ()->lWidthValue = nValue;
		return true;
	}

	return false;
}

bool CBitmapElement::FileExists (const CString & strFile)
{
	bool bResult = false;
	TCHAR sz [_MAX_FNAME] = { 0 };

	::GetFileTitle (strFile, sz, ARRAYSIZE (sz) - 1);
	CString strTitle = FoxjetFile::GetFileTitle (CString (sz));

	if (!strTitle.CompareNoCase (a2w (FJSYS_TEST_PATTERN)))
		bResult = true;
	else {
		HANDLE hFile = ::CreateFile (AddSuffix (strFile), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 
			FILE_FLAG_SEQUENTIAL_SCAN, NULL);

		if (hFile != INVALID_HANDLE_VALUE) {
			bResult = true;
			::CloseHandle (hFile);
		}
	}

	return bResult;
}

void CBitmapElement::LoadDefResBitmap(const FoxjetDatabase::HEADSTRUCT & head)
{
	const CString strPath = GetDefPath ();
	const CString strFilepath = strPath + _T ("Def.bmp");
	DIBSECTION ds;
	bool bInvalid = false;

	HINSTANCE hInst = GetInstanceHandle ();
	HBITMAP hBmp = (HBITMAP)::LoadImage (hInst, 
		MAKEINTRESOURCE (IDB_DEFBITMAP), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE | LR_MONOCHROME);
	ASSERT (hBmp);

	CBitmap * pDef = CBitmap::FromHandle (hBmp);
	::ZeroMemory (&ds, sizeof (ds));
	VERIFY (pDef->GetObject (sizeof (ds), &ds));
	const CSize sizeDef (ds.dsBm.bmWidth, ds.dsBm.bmHeight);

	// verify the default has not had it's size changed
	if (PBYTE pFile = LoadBitmap (strFilepath)) {
		BITMAPFILEHEADER * pbmfh = (BITMAPFILEHEADER *)pFile;
		BITMAPINFOHEADER * pbmih = (BITMAPINFOHEADER *)(pFile + sizeof(BITMAPFILEHEADER));

		const CSize sizeFile (pbmih->biWidth, pbmih->biHeight);

		bInvalid = (sizeFile != sizeDef) ? true : false;
		delete pFile;
	}
	else
		bInvalid = true;

	if (!bInvalid)
		bInvalid = !SetFilePath (strFilepath, head);

	// create it if its not there
	if (bInvalid) {
		if (::GetFileAttributes (strPath) == -1) {
			VERIFY (::CreateDirectory (strPath, NULL));
		}

		bool bSave = SaveBitmap (hBmp, strFilepath);
		ASSERT (bSave);	

		bool bSetFilePath = SetFilePath (strFilepath, head);
		ASSERT (bSetFilePath);
	}
	
	::DeleteObject (hBmp);
}

void CBitmapElement::Invalidate()
{
	CIpBaseElement::Invalidate ();
	LPFJELEMENT pElement = GetElement ();

	if (LPFJBITMAP p = GetMember ()) {
		p->lbmpSize = 0;

		if (p->pbmp) {
			fj_free (p->pbmp);
			p->pbmp = NULL;
		}
	}
}

CString CBitmapElement::GetDefPath ()
{
	CString strDef = GetHomeDir () + _T ("\\IP\\Logos\\");

	CString str = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, 
		ListGlobals::defElements.m_strRegSection + _T ("\\IP\\Bitmap"), 
		_T ("Path"), strDef);

	if (str [str.GetLength () - 1] != '\\')
		str += '\\';

	return str;
}

CString CBitmapElement::GetDefFilename ()
{
	return CBitmapElement::GetDefPath () + _T ("Def.bmp");
}

bool CBitmapElement::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	if (CIpBaseElement::FromString (str, ver)) {
		CString strFilename = GetFilePath ();

		return SetFilePath (strFilename, m_head);
	}

	return false;
}

CString CBitmapElement::RemoveSuffix (const CString & strFile)
{
	CString str (strFile);
	CString strLower = str;

	strLower.MakeLower ();
	int nIndex = strLower.Find (m_strSuffix);

	if (nIndex != -1)
		str = str.Left (nIndex);

	return str;
}

CString CBitmapElement::AddSuffix (const CString & strFile)
{
	CString str (strFile);
	CString strLower = str;

	strLower.MakeLower ();
	int nIndex = strLower.Find (m_strSuffix);

	if (nIndex == -1)
		str += m_strSuffix;

	return str;
}

static const ElementFields::FIELDSTRUCT lpszFields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType, 	_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszID,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszX,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszY,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszPosition,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszBold,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszWidth,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszFlipV,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFlipH,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszInverse,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszData,			_T ("0")),	
	ElementFields::FIELDSTRUCT (),
};

const FoxjetCommon::ElementFields::FIELDSTRUCT * CBitmapElement::GetFieldBuffer () const
{
	return ::lpszFields;
}
