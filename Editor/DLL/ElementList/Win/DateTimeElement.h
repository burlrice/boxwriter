// DateTimeElement.h: interface for the CDateTimeElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATETIMEELEMENT_H__6CB6265F_8AD9_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_DATETIMEELEMENT_H__6CB6265F_8AD9_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TextElement.h"
#include "Types.h"

namespace Sunnyvale
{
	void ELEMENT_API Init ();
};

namespace FoxjetElements
{
	using namespace TextElement;

	class ELEMENT_API CDateTimeElement : public CTextElement  
	{
		DECLARE_DYNAMIC (CDateTimeElement);

	public:
		CDateTimeElement(const FoxjetDatabase::HEADSTRUCT & head);
		CDateTimeElement (const CDateTimeElement & rhs);
		CDateTimeElement & operator = (const CDateTimeElement & rhs);
		virtual ~CDateTimeElement();

		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);
		virtual int GetClassID () const { return DATETIME; }
		virtual bool SetDefaultData (const CString & strData);

		virtual int Build (BUILDTYPE type = INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		static void SetPrintTime (CTime * pTime, ULONG lLineID);
		static CTime GetPrintTime (ULONG lLineID);
		static CString PreFormat (const CString & strIn);


		int GetRolloverMinutes () const;
		bool SetRollover (bool b);
		bool GetRollover () const;

		bool GetHoldStartDate () const;
		bool SetHoldStartDate (bool b);

		static CString Format (ULONG lLineID, const CString & str, int nRolloverHours, const CTime * ptm);
		static CString Format (ULONG lLineID, const CString & str, const CTime & tm);
		static void TokenizeFormats (CString strInput, CStringArray & v);

		static int IsHourCode (const CString & strInput); 
		static CString GetHourCode (ULONG lLineID, int nIndex, const CTime & tm); 

		static int IsMonthCode (const CString & strInput);
		static CString GetMonthCode (ULONG lLineID, int nIndex, const CTime & tm); 

		static int IsDayOfWeekCode (const CString & strInput); 
		static CString GetDayOfWeekCode (ULONG lLineID, int nIndex, const CTime & tm); 

		// std // #if __CUSTOM__ == __SW0840__
		static int IsQuarterHourCode (const CString & strInput);
		static CString GetQuarterHourCode (ULONG lLineID, int nIndex, const CTime & tm); 
		// std // #endif

		static const FoxjetCommon::LIMITSTRUCT m_lmtString;

		static CTime CalcCurrentTime (int nRolloverMinutes, ULONG lLineID, const CTime * ptm = NULL);

		static void SetHold (ULONG lLineID, bool bHold);
		static void ResetHold (ULONG lLineID);
		static bool GetHold (ULONG lLineID);

	protected:
		bool m_bRollover;
		bool m_bHoldStartDate;
		CTime m_tmInitial;
	};
}; //namespace FoxjetElements

#endif // !defined(AFX_DATETIMEELEMENT_H__6CB6265F_8AD9_11D4_8FC6_006067662794__INCLUDED_)
