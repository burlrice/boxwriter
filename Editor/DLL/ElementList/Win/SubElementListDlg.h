// SubElementListDlg.h: interface for the CSubElementListDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SUBELEMENTLISTDLG_H__97A2D102_8C96_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_SUBELEMENTLISTDLG_H__97A2D102_8C96_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ListCtrlImp.h"
#include "Resource.h"
#include "SubElement.h"
#include "Utils.h"

class CSubItem : public ItiLibrary::ListCtrlImp::CItem
{
public:
	CSubItem (const FoxjetElements::CSubElement * pElement);
	CSubItem (const CSubItem & rhs);
	CSubItem & operator = (const CSubItem & rhs);
	virtual ~CSubItem ();

	virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
	virtual CString GetDispText (int nColumn) const;

	const FoxjetElements::CSubElement * m_pElement;
};


class CSubElementListDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CSubElementListDlg(CWnd* pParent = NULL);   // standard constructor
	CSubElementListDlg(UINT nIDTemplate, CWnd* pParent = NULL);
	
	virtual ~CSubElementListDlg ();

// Dialog Data
	//{{AFX_DATA(CSubElementListDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSubElementListDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
public:
	void SetData (CArray <FoxjetElements::CSubElement *, FoxjetElements::CSubElement *> & v);

	CArray <FoxjetElements::CSubElement *, FoxjetElements::CSubElement *> m_vUpdated;
	CStringArray m_vDeleted;

protected:
	virtual void OnOK();
	int GetIdIndex (const CString & strID) const;

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
	CArray <FoxjetElements::CSubElement *, FoxjetElements::CSubElement *> m_vSubElements;

	virtual bool IsAI () const { return false; }
	virtual bool OnEdit (FoxjetElements::CSubElement & s, bool bAI, bool bAdd);
	afx_msg void OnProperties ();
	afx_msg void OnAdd ();
	afx_msg void OnDelete ();

	// Generated message map functions
	//{{AFX_MSG(CSubElementListDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	virtual afx_msg void OnDblclkListCtrl(NMHDR* pNMHDR, LRESULT* pResult);

	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_SUBELEMENTLISTDLG_H__97A2D102_8C96_11D4_915E_00104BEF6341__INCLUDED_)
