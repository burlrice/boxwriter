// Head.h: interface for the CHead class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HEAD_H__218919D6_AB4C_41C3_8069_9231B288F66A__INCLUDED_)
#define AFX_HEAD_H__218919D6_AB4C_41C3_8069_9231B288F66A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "BaseElement.h"
#include "ElementApi.h"
#include "PrinterFont.h"
#include "Types.h"
#include "Database.h"

namespace FoxjetCommon
{
	class ELEMENT_API CHead  
	{
	public:
		CHead(const FoxjetDatabase::HEADSTRUCT & head);
		CHead (const CHead & rhs);
		CHead & operator = (const CHead & rhs);
		virtual ~CHead();

	public:
		bool GetFont (const CString & str, CPrinterFont & fnt) const;
		bool GetFonts (CArray <CPrinterFont, CPrinterFont &> & v) const;
		
		static bool GetWinFontNames (CStringArray & v);
		static int GetFontIndex (const CString & str);

		operator FoxjetDatabase::HEADSTRUCT & () { return m_dbHead; }
		operator const FoxjetDatabase::HEADSTRUCT & () const { return m_dbHead; }

		FoxjetDatabase::HEADSTRUCT m_dbHead;
		bool IsValidFontName (const CString & str) const;

		void GetBitmappedFonts (CArray <CPrinterFont, CPrinterFont &> &v);

	protected:
		CStringArray m_vBitmappedFonts;
	};
}; //namespace FoxjetCommon

#endif // !defined(AFX_HEAD_H__218919D6_AB4C_41C3_8069_9231B288F66A__INCLUDED_)
