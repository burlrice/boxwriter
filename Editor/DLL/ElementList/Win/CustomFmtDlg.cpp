// CustomFmtDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CustomFmtDlg.h"
#include "DateTimeFmtDlg.h"
#include "CopyArray.h"
#include "ItiLibrary.h"
#include "Extern.h"
#include "Debug.h"
#include "DateTimeElement.h"

using namespace FoxjetElements;
using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace ItiLibrary;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CCustomFmtDlg::CItem::CItem (ULONG lLineID, const CString & strFormat)
:	m_lLineID (lLineID),
	m_strFormat (strFormat)
{
	m_strExample = CDateTimeElement::Format (m_lLineID, m_strFormat, 0, NULL);
}

CCustomFmtDlg::CItem::~CItem ()
{
}

int CCustomFmtDlg::CItem::Compare (const ItiLibrary::ListCtrlImp::CItem & cmp, int nColumn) const
{
	const CItem & rhs = (CItem &)cmp;

	switch (nColumn) {
	case 0:		return m_strFormat.Compare (rhs.m_strFormat);
	case 1:		return m_strExample.Compare (rhs.m_strExample);
	}

	return 0;
}

CString CCustomFmtDlg::CItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0:		
		str = m_strFormat;
		break;
	case 1:		
		str = m_strExample;
		break;
	}

	return str;
}

/////////////////////////////////////////////////////////////////////////////
// CCustomFmtDlg dialog

CCustomFmtDlg::CCustomFmtDlg(ULONG lLineID, CWnd* pParent /*=NULL*/)
:	m_lLineID (lLineID),
	FoxjetCommon::CEliteDlg(IDD_CUSTOMDATEFORMAT, pParent)
{
	//{{AFX_DATA_INIT(CCustomFmtDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CCustomFmtDlg::~CCustomFmtDlg()
{
}

void CCustomFmtDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCustomFmtDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCustomFmtDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CCustomFmtDlg)
	ON_BN_CLICKED(BTN_ADD, OnAdd)
	ON_BN_CLICKED(BTN_DELETE, OnDelete)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	//}}AFX_MSG_MAP
	ON_NOTIFY(NM_DBLCLK, LV_FMTS, OnDblclkListCtrl)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCustomFmtDlg message handlers

void CCustomFmtDlg::OnAdd() 
{
	CDateTimeFmtDlg dlg (m_lLineID, this);

	if (dlg.DoModal () == IDOK) 
		m_lvi.InsertCtrlData (new CItem (m_lLineID, dlg.m_strFmt));
}

void CCustomFmtDlg::OnEdit()
{
	CLongArray sel = GetSelectedItems (m_lvi);
	
	if (sel.GetSize ()) {
		CDateTimeFmtDlg dlg (m_lLineID, this);
		CItem * pItem = (CItem *)m_lvi.GetCtrlData (sel [0]);
	
		dlg.m_strFmt = pItem->m_strFormat;

		if (dlg.DoModal () == IDOK) {
			pItem->m_strFormat = dlg.m_strFmt;
			m_lvi.UpdateCtrlData (pItem);
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CCustomFmtDlg::OnDelete() 
{
	CLongArray sel = GetSelectedItems (m_lvi);

	if (sel.GetSize ()) {
		int nResult = MsgBox (* this, LoadString (IDS_CONFIRMFORMATDELETE), 
			LoadString (IDS_CONFIRM), MB_YESNO);

		if (nResult == IDYES) 
			for (int i = sel.GetSize () - 1; i >= 0; i--) 
				m_lvi.DeleteCtrlData (sel [i]);
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CCustomFmtDlg::OnOK()
{
	m_vFormats.RemoveAll ();

	for (int i = 0; i < m_lvi.GetListCtrl ().GetItemCount (); i++) {
		CItem * pItem = (CItem *)m_lvi.GetCtrlData (i);
		m_vFormats.Add (pItem->m_strFormat);
	}

	FoxjetCommon::CEliteDlg::OnOK ();
}

BOOL CCustomFmtDlg::OnInitDialog ()
{
	using namespace ItiLibrary;

	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;

	FoxjetCommon::CEliteDlg::OnInitDialog ();

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_FORMATSTRING), 100));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_FORMATEXAMPLE), 100));

	m_lvi.Create (LV_FMTS, vCols, this, defElements.m_strRegSection);	

	for (int i = 0; i < m_vFormats.GetSize (); i++) {
		CItem * p = new CItem (m_lLineID, m_vFormats [i]);
		m_lvi.InsertCtrlData (p, i);
	}

	return TRUE;
}

void CCustomFmtDlg::SetData(const CStringArray &v)
{
	m_vFormats.RemoveAll ();

	for (int i = 0; i < v.GetSize (); i++)
		m_vFormats.Add (v [i]);
}

void CCustomFmtDlg::GetData(CStringArray &v)
{
	v.RemoveAll ();

	for (int i = 0; i < m_vFormats.GetSize (); i++) 
		v.Add (m_vFormats [i]);
}

void CCustomFmtDlg::OnDblclkListCtrl(NMHDR* pNMHDR, LRESULT* pResult)
{
	OnEdit ();
	* pResult = 0;
}

