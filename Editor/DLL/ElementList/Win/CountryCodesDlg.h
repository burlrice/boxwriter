#if !defined(AFX_COUNTRYCODESDLG_H__EA87F463_F387_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_COUNTRYCODESDLG_H__EA87F463_F387_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CountryCodesDlg.h : header file
//

#include "resource.h"
#include "ListCtrlImp.h"
#include "CountryCodes.h"
#include "OdbcRecordset.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CCountryCodesDlg dialog

class CCountryCodesDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CCountryCodesDlg(FoxjetDatabase::COdbcDatabase * pDatabase, CWnd* pParent = NULL);   // standard constructor
	virtual ~CCountryCodesDlg ();

// Dialog Data
	//{{AFX_DATA(CCountryCodesDlg)
	enum { IDD = IDD_COUNTRYCODES };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

public:
	static void DeleteData (CPtrArray & v);
	static bool GetData (Ritz::CountryCodes::COUNTRYCODERECSTRUCT & ccs, 
		const CString & strNumber, FoxjetDatabase::COdbcDatabase * pDatabase);
	static bool UpdateData (Ritz::CountryCodes::COUNTRYCODERECSTRUCT & ccs, 
		FoxjetDatabase::COdbcDatabase * pDatabase);

	CStringArray m_vSelected;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCountryCodesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	virtual void OnOK();

	afx_msg void OnDblclkListCtrl(NMHDR* pNMHDR, LRESULT* pResult);

// Implementation
protected:

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

	class CCode :	public Ritz::CountryCodes::COUNTRYCODERECSTRUCT,
					public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CCode ();
		CCode (const Ritz::CountryCodes::COUNTRYCODERECSTRUCT & rhs);
		CCode (const CCode & rhs);
		CCode & operator = (const CCode & rhs);
		virtual ~CCode ();

		virtual CString GetDispText (int nColumn) const;
	};

	// Generated message map functions
	//{{AFX_MSG(CCountryCodesDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	FoxjetDatabase::COdbcDatabase * m_pDatabase;

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COUNTRYCODESDLG_H__EA87F463_F387_11D4_8FC6_006067662794__INCLUDED_)
