#if !defined(AFX_BARCODEDLG_H__471C7422_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_BARCODEDLG_H__471C7422_8B3D_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BarcodeDlg.h : header file
//

#include "ElementDlg.h"
#include "resource.h"
#include "SubElement.h"
#include "SubElementDlg.h"
#include "BarcodeElement.h"

/////////////////////////////////////////////////////////////////////////////
// CBarcodeDlg dialog

class CBarcodeDlg : public CElementDlg
{
// Construction
public:
	CBarcodeDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   // standard constructor
	~CBarcodeDlg ();

// Dialog Data
	//{{AFX_DATA(CBarcodeDlg)
	BOOL	m_bQuietZone;
	BOOL	m_bIncludeText;
	int		m_nSymbology;
	int		m_nMagnification;
	BOOL	m_bCheckSum;
	int		m_nRatio;
	int		m_nHeight;
	int		m_nWidth;
	//}}AFX_DATA

	FoxjetElements::CTextInfo m_caption;
	FoxjetElements::CMaxiCodeInfo m_maxicode;
	FoxjetElements::CDataMatrixInfo m_datamatrix;
	FoxjetElements::TextElement::FONTORIENTATION m_nOrientation;

	CArray <FoxjetElements::CSubElement, FoxjetElements::CSubElement &> m_vSubElements;

	static const int m_nIDCol;
	static const int m_nElementCol;
	static const int m_nLengthCol;
	static const int m_nDescCol;

	void Get (const FoxjetElements::CBarcodeElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);
	void Set (FoxjetElements::CBarcodeElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);

	virtual UINT GetDefCtrlID () const { return TXT_SIMPLEDATA; }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBarcodeDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual bool GetValues ();
	CSize CBarcodeDlg::DrawBarcode(CDC &dc, bool bThrowException);
	// throw CBarcodeException

	BOOL	m_bSimple;
	CString m_strData;

	void InitData ();
	bool IsSimple () const;
	virtual int BuildElement ();
	const FoxjetElements::CBarcodeElement & GetElement () const;
	FoxjetElements::CBarcodeElement & GetElement ();

	typedef struct 
	{
		CString m_strID;
		CString m_strDesc;
		int m_nField;
		int m_nLength;
	} AGGREGATESTRUCT;

	int GetCurSelSymbology () const;
	int GetCurSelMagnification () const;
	void RefreshAggregateData ();
	void DeleteAggregateData ();
	bool UpdateAggregateItem (AGGREGATESTRUCT & a, int nIndex = -1);
	bool InsertAggregateItem (AGGREGATESTRUCT & a, int nIndex = -1);
	void InitAggregateData ();
	void InitAggregatesHeader ();
	void AddSubElement (bool bAI);
	void UpdateUI ();
	bool OnEdit (int nIndex);
	void FreeMagCtrl ();

	// Generated message map functions
	//{{AFX_MSG(CBarcodeDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAddAi();
	afx_msg void OnDelete();
	afx_msg void OnDown();
	afx_msg void OnUp();
	afx_msg void OnAddelement();
	afx_msg void OnEdit();
	afx_msg void OnDblclkAggregate(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCaption();
	afx_msg void OnMore();
	//}}AFX_MSG
	afx_msg void OnSimple();
	afx_msg void OnSelchangeSymbology ();
	afx_msg void OnSelchangeMag ();
	afx_msg void OnOrientationClick();
	afx_msg void OnEditParams ();

	CBitmap m_bmpOrientation [2];

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BARCODEDLG_H__471C7422_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
