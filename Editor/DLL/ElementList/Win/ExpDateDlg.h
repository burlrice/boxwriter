#if !defined(AFX_EXPDATEDLG_H__471C7426_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_EXPDATEDLG_H__471C7426_8B3D_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExpDateDlg.h : header file
//

#include "DateTimeDlg.h"
#include "ExpDateElement.h"

using namespace FoxjetElements;

/////////////////////////////////////////////////////////////////////////////
// CExpDateDlg dialog

class CExpDateDlg : public CDateTimeDlg
{
// Construction
public:
	CExpDateDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CExpDateDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	virtual UINT GetDefCtrlID () const { return CB_DATETIMEFORMAT; }

	CExpDateElement::TYPE GetCurSelType () const;

	long						m_lSpan;
	CTime						m_tmExp;
	CExpDateElement::PERIOD		m_period;
	BOOL						m_bRoundUp;
	BOOL						m_bRoundDown;
	BOOL						m_bHoldStartDate;

	CExpDateElement::TYPE		m_type;
	bool						m_bPromptAtTaskStart;
	CString 					m_strPrompt;
	CString 					m_strDSN;
	CString 					m_strTable;
	CString 					m_strField;
	CString 					m_strKeyField;
	CString 					m_strKeyValue;
	int							m_nSQLType;
	int							m_nFormat;
	CString						m_strFormat;

	void Get (const FoxjetElements::CExpDateElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);
	void Set (FoxjetElements::CExpDateElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExpDateDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();
	virtual bool GetValues ();
	virtual int BuildElement();
	FoxjetElements::CExpDateElement & GetElement ();
	const FoxjetElements::CExpDateElement & GetElement () const;

	afx_msg void OnRoundUpClick ();
	afx_msg void OnRoundDownClick ();
	afx_msg void OnPeriodChange ();
	afx_msg void OnDatabaseClick ();
	afx_msg void OnUserClick ();
	afx_msg void OnProperties ();

	// Generated message map functions
	//{{AFX_MSG(CExpDateDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXPDATEDLG_H__471C7426_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
