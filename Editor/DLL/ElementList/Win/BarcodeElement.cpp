// BarcodeElement.cpp: implementation of the CBarcodeElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <math.h>

#include "BarcodeElement.h"
#include "Resource.h"
#include "Wasp\BcErr.h"
#include "CopyArray.h"
#include "List.h"
#include "ElementDefaults.h"
#include "Edit.h"
#include "Debug.h"
#include "Color.h"
#include "Coord.h"
#include "Extern.h"
#include "Utils.h"
#include "TemplExt.h"
#include "BarcodeParams.h"
#include "BarcodeDlg.h"
#include "SerialElement.h"
#include "Extern.h"
#include "AppVer.h"
#include "Parse.h"
#include "CountElement.h"
#include "ximage.h"
#include "Registry.h"

//#define __TRACE_TOFROMSTRING__ 

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/*
c39: http://www.barcode-1.net/pub/russadam/39code.html
c128: http://www.barcode-1.net/pub/russadam/128code.html
c93: http://www.barcodeisland.com/code93.phtml
*/

using namespace FoxjetCommon;
using namespace Color;
using namespace FoxjetElements;
using namespace FoxjetElements::TextElement;
using namespace FoxjetDatabase;

static bool bDataMatrix = true;

static bool IsNumeric (const CString & str);
static bool IsAscii (const CString & str);
static bool UsesWasp (int nSymbology);
static bool IsUPC (int nSymbology);

static bool IsNumeric (const CString & str)
{
	for (int i = 0; i < str.GetLength (); i++)
		if (!_istdigit (str [i]))
			return false;

	return true;
}

static bool IsAscii (const CString & str)
{
	for (int i = 0; i < str.GetLength (); i++)
		if (!_istascii (str [i]))
			return false;

	return true;
}

static bool UsesWasp (int nSymbology)
{
	switch (nSymbology) {
	case kSymbologyUPCA:
	case kSymbologyUPCE:
	case kSymbologyEANJAN_8:
	case kSymbologyEANJAN_13:
//#ifndef WASPI2O5 // TODO: rem: WASPI2O5
	case kSymbologyInter2of5:
	case kSymbologyGTIN14:
//#endif //WASPI2O5 // TODO: rem: WASPI2O5
		return false;
	}

	return true;
}

static bool IsUPC (int nSymbology)
{
	switch (nSymbology) {
	case kSymbologyUPCA:
	case kSymbologyUPCE:
	case kSymbologyEANJAN_8:
	case kSymbologyEANJAN_13:
		return true;
	}

	return false;
}

/////////////////////////////////////////////////////////////////////////////
namespace I2o5
{
	static const BYTE nEncode [10][5] = 
	{	
		{ 0, 0, 1, 1, 0, }, // 0
		{ 1, 0, 0, 0, 1, }, // 1
		{ 0, 1, 0, 0, 1, }, // 2 
		{ 1, 1, 0, 0, 0, }, // 3
		{ 0, 0, 1, 0, 1, }, // 4
		{ 1, 0, 1, 0, 0, }, // 5
		{ 0, 1, 1, 0, 0, }, // 6
		{ 0, 0, 0, 1, 1, }, // 7
		{ 1, 0, 0, 1, 0, }, // 8
		{ 0, 1, 0, 1, 0, }, // 9
	};

	typedef enum BarColor 
	{ 
		WHITE, 
		BLACK 
	};

	typedef enum BarWidth 
	{ 
		NARROW, 
		WIDE
	};

	typedef struct tagBARSTRUCT
	{
		tagBARSTRUCT (BarColor color = BLACK, BarWidth width = NARROW);

		BarColor m_color;
		BarWidth m_width;
	} BARSTRUCT;

	bool IsValidData (const CString & str, const CBarcodeParams & params);
	CString CalcCheckDigit (const CString & str);

	tagBARSTRUCT::tagBARSTRUCT (BarColor color, BarWidth width) 
	:	m_width (width), 
		m_color (color)
	{ 
	}

}; //namespace I2o5

/////////////////////////////////////////////////////////////////////////////
namespace UPCA
{
	static const BYTE nEncLeft [10][4] = 
	{	
		{ 3, 2, 1, 1 },		// 0:	0001101		
		{ 2, 2, 2, 1 },		// 1:	0011001		
		{ 2, 1, 2, 2 },		// 2:	0010011		
		{ 1, 4, 1, 1 },		// 3:	0111101		
		{ 1, 1, 3, 2 },		// 4:	0100011		
		{ 1, 2, 3, 1 },		// 5:	0110001		
		{ 1, 1, 1, 4 },		// 6:	0101111		
		{ 1, 3, 1, 2 },		// 7:	0111011		
		{ 1, 2, 1, 3 },		// 8:	0110111		
		{ 3, 1, 1, 2 },		// 9:	0001011		
	};
	static const BYTE nEncRight [10][4] = 
	{	
		{ 3, 2, 1, 1, },	// 0:	1110010	
		{ 2, 2, 2, 1, },	// 1:	1100110	
		{ 2, 1, 2, 2, },	// 2:	1101100	
		{ 1, 4, 1, 1, },	// 3:	1000010	
		{ 1, 1, 3, 2, },	// 4:	1011100	
		{ 1, 2, 3, 1, },	// 5:	1001110	
		{ 1, 1, 1, 4, },	// 6:	1010000	
		{ 1, 3, 1, 2, },	// 7:	1000100	
		{ 1, 2, 1, 3, },	// 8:	1001000	
		{ 3, 1, 1, 2, },	// 9:	1110100	
	};
	static const BYTE nEncGuard []	= { 1, 0, 1, };
	static const BYTE nEncCenter [] = { 0, 1, 0, 1, 0 };

	CString CalcCheckDigit (const CString & strData);
	bool IsValidData (const CString & str, const CBarcodeParams & params);

	// NOTE: this symbology must be accounted for by UsesWasp

}; //namespace UPCA

namespace UPCE
{
	static const BYTE nEncOdd [10][4] = 
	{
		{ 3, 2, 1, 1,  },  	// 0:	0001101
 		{ 2, 2, 2, 1,  },  	// 1:	0011001
 		{ 2, 1, 2, 2,  },  	// 2:	0010011
 		{ 1, 4, 1, 1,  },  	// 3:	0111101
 		{ 1, 1, 3, 2,  },  	// 4:	0100011
 		{ 1, 2, 3, 1,  },  	// 5:	0110001
 		{ 1, 1, 1, 4,  },  	// 6:	0101111
 		{ 1, 3, 1, 2,  },  	// 7:	0111011
 		{ 1, 2, 1, 3,  },  	// 8:	0110111
 		{ 3, 1, 1, 2,  },  	// 9:	0001011
	};
	static const BYTE nEncEven [10][4] = 
	{
		{ 1, 1, 2, 3, },  	// 0:	0100111	
 		{ 1, 2, 2, 2, },  	// 1:	0110011	
 		{ 2, 2, 1, 2, },  	// 2:	0011011	
 		{ 1, 1, 4, 1, },  	// 3:	0100001	
 		{ 2, 3, 1, 1, },  	// 4:	0011101	
 		{ 1, 3, 2, 1, },  	// 5:	0111001	
 		{ 4, 1, 1, 1, },  	// 6:	0000101	
 		{ 2, 1, 3, 1, },  	// 7:	0010001	
 		{ 3, 1, 2, 1, },  	// 8:	0001001	
 		{ 2, 1, 1, 3, },  	// 9:	0010111	
	};
	typedef enum { E = 0, O = 1 } PARITY;
	static const BYTE n0 [10][6] = 
	{
		{ E, E,	E, O, O, O,  }, 	// 0
		{ E, E,	O, E, O, O,  }, 	// 1
		{ E, E,	O, O, E, O,  }, 	// 2
		{ E, E,	O, O, O, E,  }, 	// 3
		{ E, O,	E, E, O, O,  }, 	// 4
		{ E, O,	O, E, E, O,  }, 	// 5
		{ E, O,	O, O, E, E,  }, 	// 6
		{ E, O,	E, O, E, O,  }, 	// 7
		{ E, O,	E, O, O, E,  }, 	// 8
		{ E, O,	O, E, O, E,  }, 	// 9
	};
	static const BYTE n1 [10][6] = 
	{
		{ O, O, O, E, E, E,  },		// 0
		{ O, O, E, O, E, E,  },  	// 1
		{ O, O, E, E, O, E,  },		// 2
		{ O, O, E, E, E, O,  },		// 3
		{ O, E, O, O, E, E,  },		// 4
		{ O, E, E, O, O, E,  },		// 5
		{ O, E, E, E, O, O,  },		// 6
		{ O, E, O, E, O, E,  },		// 7
		{ O, E, O, E, E, O,  },		// 8
		{ O, E, E, O, E, O,  },		// 9
	};

	CString CalcCheckDigit (const CString & strData, const CBarcodeParams & params);
	bool IsValidData (const CString & str, const CBarcodeParams & params);

	static const BYTE nEncRight [] = { 0, 1, 0, 1, 0, 1 };
};

namespace EAN
{
	typedef enum
	{
		A = 0,
		B,
		C,
	} EANNUMBERSET;

	static const BYTE nNumberset [10][6] = 
	{
		{ A, A, A, A, A, A, },	// 0
		{ A, A, B, A, B, B, },	// 1
		{ A, A, B, B, A, B, },	// 2
		{ A, A, B, B, B, A, },	// 3
		{ A, B, A, A, B, B, },	// 4
		{ A, B, B, A, A, B, },	// 5
		{ A, B, B, B, A, A, },	// 6
		{ A, B, A, B, A, B, },	// 7
		{ A, B, A, B, B, A, },	// 8
		{ A, B, B, A, B, A, },	// 9
	};

	static const BYTE nEncA [10][4] = 
	{	
		{ 3, 2, 1, 1 },		// 0:	0001101		
		{ 2, 2, 2, 1 },		// 1:	0011001		
		{ 2, 1, 2, 2 },		// 2:	0010011		
		{ 1, 4, 1, 1 },		// 3:	0111101		
		{ 1, 1, 3, 2 },		// 4:	0100011		
		{ 1, 2, 3, 1 },		// 5:	0110001		
		{ 1, 1, 1, 4 },		// 6:	0101111		
		{ 1, 3, 1, 2 },		// 7:	0111011		
		{ 1, 2, 1, 3 },		// 8:	0110111		
		{ 3, 1, 1, 2 },		// 9:	0001011		
	};
	static const BYTE nEncB [10][4] = 
	{	
		{ 1, 1, 2, 3 },		// 0:	0100111
		{ 1, 2, 2, 2 },		// 1:	0110011
		{ 2, 2, 1, 2 },		// 2:	0011011
		{ 1, 1, 4, 1 },		// 3:	0100001
		{ 2, 3, 1, 1 },		// 4:	0011101
		{ 1, 3, 2, 1 },		// 5:	0111001
		{ 4, 1, 1, 1 },		// 6:	0000101
		{ 2, 1, 3, 1 },		// 7:	0010001
		{ 3, 1, 2, 1 },		// 8:	0001001
		{ 2, 1, 1, 3 },		// 9:	0010111
	};
	static const BYTE nEncC [10][4] = 
	{	
		{ 3, 2, 1, 1, },	// 0:	1110010	
		{ 2, 2, 2, 1, },	// 1:	1100110	
		{ 2, 1, 2, 2, },	// 2:	1101100	
		{ 1, 4, 1, 1, },	// 3:	1000010	
		{ 1, 1, 3, 2, },	// 4:	1011100	
		{ 1, 2, 3, 1, },	// 5:	1001110	
		{ 1, 1, 1, 4, },	// 6:	1010000	
		{ 1, 3, 1, 2, },	// 7:	1000100	
		{ 1, 2, 1, 3, },	// 8:	1001000	
		{ 3, 1, 1, 2, },	// 9:	1110100	
	};

	// NOTE: this symbology must be accounted for by UsesWasp

}; //namespace EAN

namespace EAN8
{
	bool IsValidData (const CString & str);

	// NOTE: this symbology must be accounted for by UsesWasp

}; //namespace EAN8

namespace EAN13
{
	bool IsValidData (const CString & str);

	// NOTE: this symbology must be accounted for by UsesWasp

}; //namespace EAN13

/////////////////////////////////////////////////////////////////////////////
CString UPCA::CalcCheckDigit (const CString & strData) 
{
	CString strResult = _T ("0");
	CString str (strData);
	int nEven = 0, nOdd = 0;

	str.MakeReverse ();
	
	for (int i = 0; i < str.GetLength (); i++) {
		int n = str [i] - '0';

		if (!(i % 2))
			nEven += n;
		else
			nOdd += n;
	}

	int nSum = nOdd + (nEven * 3);
	int nMultiple = (int)ceil ((double)nSum / 10.0);
	int nCheckDigit = (nMultiple * 10) - nSum;

	strResult.Format (_T ("%d"), nCheckDigit);

	return strResult;
}

bool UPCA::IsValidData (const CString & str, const CBarcodeParams & params)
{
	if (params.GetCheckSum ())
		return str.GetLength () == 11 && IsNumeric (str);
	else
		return str.GetLength () == 12 && IsNumeric (str);

	//switch (str.GetLength ()) {
	//case 11:
	//	return IsNumeric (str);
	//}

	//return false;
}

/////////////////////////////////////////////////////////////////////////////

CString UPCE::CalcCheckDigit (const CString & strData, const CBarcodeParams & params) 
{
	CString str;

	if (UPCE::IsValidData (strData, params)) {
		TCHAR cNumberSys = strData [0];
		TCHAR c = strData [strData.GetLength () - 1];

		switch (c) {
		case '0': case '1': case '2': 
			{ // rule 1
				str = cNumberSys + strData.Mid (1, 2) + c + CString (_T ("0000")) + strData.Mid (3, 3);
			}
			break;
		case '3': 
			{ // rule 2
				str = cNumberSys + strData.Mid (1, 3) + CString (_T ("00000")) + strData.Mid (4, 2);
			}
			break;
		case '4': 
			{ // rule 3
				str = cNumberSys + strData.Mid (1, 4) + CString (_T ("00000")) + strData.Mid (5, 1);
			}
			break;
		case '5': case '6': case '7': case '8': case '9': 
			{ // rule 4
				str = cNumberSys + strData.Mid (1, 5) + CString (_T ("0000")) + c;
			}
			break;
		}
	}

	ASSERT (UPCA::IsValidData (str, params));
	//TRACEF (strData + " --> " + str);

	return UPCA::CalcCheckDigit (str);
}

bool UPCE::IsValidData (const CString & str, const CBarcodeParams & params)
{
	switch (str.GetLength ()) {
	case 7:
		return IsNumeric (str);
	}

	return false;
}

/////////////////////////////////////////////////////////////////////////////

bool EAN8::IsValidData (const CString & str)
{
	if (str.GetLength () == 7)
		return IsNumeric (str);

	return false;
}

/////////////////////////////////////////////////////////////////////////////

bool EAN13::IsValidData (const CString & str)
{
	if (str.GetLength () == 12)
		return IsNumeric (str);

	return false;
}

/////////////////////////////////////////////////////////////////////////////

bool I2o5::IsValidData (const CString & str, const CBarcodeParams & params) 
{
	if (params.GetSymbology () == kSymbologyGTIN14) {
		if (str.GetLength () != 14)
			return false;
	}
	else {
		if ((str.GetLength () % 2) != 0)
			return false;
	}

	return IsNumeric (str);
}

CString I2o5::CalcCheckDigit (const CString & str) 
{
	return UPCA::CalcCheckDigit (str);
}

/////////////////////////////////////////////////////////////////////////////

CTextInfo::CTextInfo ()
:	m_strText ( _T("")), 
	m_bAuto (true)
{
}

CTextInfo::CTextInfo (const TEXTINFO & rhs)
{
	CTextInfo tmp;

	tmp.m_strText	= rhs.sText;
	m_bAuto			= true;

	* this = tmp;
}

CTextInfo::CTextInfo (const CTextInfo & rhs)
:	m_strText (rhs.m_strText),
	m_bAuto (rhs.m_bAuto)
{
}

CTextInfo & CTextInfo::operator = (const CTextInfo & rhs)
{
	if (this != &rhs) {
		m_strText		= rhs.m_strText;
		m_bAuto			= rhs.m_bAuto;
	}

	return * this;
}

CTextInfo::~CTextInfo ()
{
}

/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC (CBarcodeException, CException);

CBarcodeException::CBarcodeException (int nWaspError, const FoxjetDatabase::HEADSTRUCT & head,
									  int nSymbology, bool bAutoDelete)
:	CException (bAutoDelete)
{
	m_str = GetErrorMessage (nWaspError, nSymbology, head);
}

CBarcodeException::CBarcodeException (const CString & strError, bool bAutoDelete)
:	m_str (strError), CException (bAutoDelete)
{
	
}

CBarcodeException::~CBarcodeException ()
{
}

BOOL CBarcodeException::GetErrorMessage (LPTSTR lpszError, 
										 UINT nMaxError, 
										 PUINT pnHelpContext)
{
	_tcsncpy (lpszError, m_str, nMaxError);
	return TRUE;
}

CString CBarcodeException::GetErrorMessage () const
{
	return m_str;
}

CString CBarcodeException::GetErrorMessage (int nError, int nSymbology, const FoxjetDatabase::HEADSTRUCT & head) 
{
	typedef struct {
		int m_nError;
		UINT m_nResID;
	} ERRORMAPSTRUCT;

	static const ERRORMAPSTRUCT err [] = {
		{ kERR_INCOMPLETE_UPCA,			IDS_kERR_INCOMPLETE_UPCA						},
		{ kERR_INCOMPLETE_UPCE,			IDS_kERR_INCOMPLETE_UPCE						},
		{ kERR_INCOMPLETE_EANJAN_8,		IDS_kERR_INCOMPLETE_EANJAN_8					},
		{ kERR_INCOMPLETE_EANJAN_13,	IDS_kERR_INCOMPLETE_EANJAN_13					},
		{ kERR_INCOMPLETE_POSTNET,		IDS_kERR_INCOMPLETE_POSTNET						},
		{ kERR_BAD_CHAR,				IDS_kERR_BAD_CHAR								},
		{ kERR_BAD_CHAR_ASCII_0_127,	IDS_kERR_BAD_CHAR_ASCII_0_127					},
		{ kERR_BAD_CHAR_NUMERIC,		IDS_kERR_BAD_CHAR_NUMERIC						},
		{ kERR_FIRST_CHAR_ZERO,			IDS_kERR_FIRST_CHAR_ZERO						},
		{ kERR_UPCE_MISSING_ZERO,		IDS_kERR_UPCE_MISSING_ZERO						},
		{ kERR_TOO_MANY,				IDS_kERR_TOO_MANY								},
		{ kERR_UNKNOWN_TYPE,			IDS_kERR_UNKNOWN_TYPE							},
		{ kERR_MSG_LEN,					IDS_kERR_MSG_LEN								},
		{ kERR_SOFTWARE_LOGIC,			IDS_kERR_SOFTWARE_LOGIC							},
		{ kERR_INVALID_SYMBOL,			IDS_kERR_INVALID_SYMBOL							},
		{ kERR_DC_ATTACH,				IDS_kERR_DC_ATTACH								},
		{ kERR_INVALID_SYMBOLOGY,		IDS_kERR_INVALID_SYMBOLOGY						},
		{ kERR_INVALID_RATIO,			IDS_kERR_INVALID_RATIO							},
		{ kERR_INVALID_ROTATION,		IDS_kERR_INVALID_ROTATION						},
		{ kERR_INVALID_MAG,				IDS_kERR_INVALID_MAG							},
		{ kERR_INVALID_UNITS,			IDS_kERR_INVALID_UNITS							},
		{ kERR_INVALID_BAR_WIDTH,		IDS_kERR_INVALID_BAR_WIDTH						},
		{ kERR_INVALID_BAR_HEIGHT,		IDS_kERR_INVALID_BAR_HEIGHT						},
		{ kERR_FONT_CREATE,				IDS_kERR_FONT_CREATE							},
		{ kERR_FONT_SELECT,				IDS_kERR_FONT_SELECT							},
		{ kERR_TEXTOUT,					IDS_kERR_TEXTOUT								},
		{ kERR_XLATEMAP,				IDS_kERR_XLATEMAP								},
		{ kERR_DEMO_CHAR,				IDS_kERR_DEMO_CHAR								},
//		{ kERRDESC_DEMO_NONFEATURE,		IDS_kERR_DEMO_NONFEATURE						},
		{ kERR_MEMALLOC,				IDS_kERR_MEMALLOC								},
		{ kERR_DLL_LOAD,				IDS_kERR_DLL_LOAD								},
		{ kERR_PDF417_ROWCOL_FIT,		IDS_kERR_PDF417_ROWCOL_FIT						},
		{ kERR_PDF417_EDAC,				IDS_kERR_PDF417_EDAC							},
		{ kERR_PDF417_GENDATA,			IDS_kERR_PDF417_GENDATA							},
		{ kERR_MAXICODE_INVALID_CHAR,	IDS_kERR_MAXICODE_INVALID_CHAR					},
		{ kERR_MAXICODE_TOO_MANY_CHARS, IDS_kERR_MAXICODE_TOO_MANY_CHARS				},
		{ kERR_MAXICODE_INVALID_MODE,	IDS_kERR_MAXICODE_INVALID_MODE					},
		{ kERR_MAXICODE_INVALID_NUM_CODEWORDS, IDS_kERR_MAXICODE_INVALID_NUM_CODEWORDS	},
		{ kERR_MAXICODE_DRAWBULLSEYE,	IDS_kERR_MAXICODE_DRAWBULLSEYE					},
		{ kERR_MAXICODE_DRAWMODULE,		IDS_kERR_MAXICODE_DRAWMODULE					},
		{ kERR_DATAMATRIX_GEN_EDAC,		IDS_kERR_DATAMATRIX_GEN_EDAC					},
		{ kERR_DATAMATRIX_DRAWMODULE,	IDS_kERR_DATAMATRIX_DRAWMODULE					},
		{ kERR_DATAMATRIX_FINDER,		IDS_kERR_DATAMATRIX_FINDER						},
		{ kERR_DATAMATRIX_PLACEDATA,	IDS_kERR_DATAMATRIX_PLACEDATA					},
		{ kERR_DATAMATRIX_LOOKAHEAD,	IDS_kERR_DATAMATRIX_LOOKAHEAD					},
		{ kERR_DATAMATRIX_X12,			IDS_kERR_DATAMATRIX_X12							},
		{ kERR_DATAMATRIX_EDIFACT,		IDS_kERR_DATAMATRIX_EDIFACT						},
		{ kERR_DATAMATRIX_C40_TEXT,		IDS_kERR_DATAMATRIX_C40_TEXT					},
		{ kERR_DATAMATRIX_ENCODE,		IDS_kERR_DATAMATRIX_ENCODE						},
		{ kERR_DATAMATRIX_SYMBOL_SIZE,	IDS_kERR_DATAMATRIX_SYMBOL_SIZE					},
		{ kERR_INVALID_ALIGNMENT,		IDS_kERR_INVALID_ALIGNMENT						},
		{ -1,							-1												},
	};

	for (int i = 0; err [i].m_nError != -1 && err [i].m_nResID != -1; i++) {
		if (nError == err [i].m_nError) {
			CString str = LoadString (err [i].m_nResID);

			switch (nError) {
			case kERR_INVALID_RATIO:
				str.Format (LoadString (err [i].m_nResID), 
					CBarcodeElement::m_lmtRatio.m_dwMin, CBarcodeElement::m_lmtRatio.m_dwMax);
				break;
			case kERR_INVALID_MAG:			
				str.Format (LoadString (err [i].m_nResID), 
					10, 500);
				break;
			case kERR_INVALID_BAR_WIDTH:	
				str.Format (LoadString (err [i].m_nResID), 
					CBarcodeElement::GetBarWidthMin (nSymbology), CBarcodeElement::GetBarWidthMax (nSymbology));
				break;
			case kERR_INVALID_BAR_HEIGHT:	
				str.Format (LoadString (err [i].m_nResID), 
					CBarcodeElement::m_lmtBarHeight.m_dwMin, CBarcodeElement::m_lmtBarHeight.m_dwMax);
				break;
			}
			
			
			return str;
		}
	}

	ASSERT (0);
	return LoadString (IDS_UNKOWNERROR);
}
/////////////////////////////////////////////////////////////////////////////

const FoxjetCommon::LIMITSTRUCT CBarcodeElement::m_lmtRatio				= { 20, 30	};
const FoxjetCommon::LIMITSTRUCT CBarcodeElement::m_lmtBarHeight			= { 5,	384 };
const FoxjetCommon::LIMITSTRUCT CBarcodeElement::m_lmtBarHeightVertical	= { 10,	600 };
const FoxjetCommon::LIMITSTRUCT CBarcodeElement::m_lmtMag				= { 10,	500	};
const FoxjetCommon::LIMITSTRUCT CBarcodeElement::m_lmtString			= { 1,	100	};
const FoxjetCommon::LIMITSTRUCT CBarcodeElement::m_lmtCX				= { 8,	144	}; 
const FoxjetCommon::LIMITSTRUCT CBarcodeElement::m_lmtCY				= { 8,	144	}; 
const FoxjetCommon::LIMITSTRUCT CBarcodeElement::m_lmtHeightFactor		= { 0,	144	}; 
const FoxjetCommon::LIMITSTRUCT CBarcodeElement::m_lmtWidthFactor		= { 0,	144	}; 


IMPLEMENT_DYNAMIC (CBarcodeElement, CBaseElement);

CBarcodeElement::CBarcodeElement(const FoxjetDatabase::HEADSTRUCT & head)
:	m_lMagID (1), 
	m_pBmp (NULL),
	m_size (0, 0),
	m_nWidth (14),
	m_nHeight (14),
	m_orient (NORMAL),
	CBaseElement (head)
{
	if (!SetSymbology (kSymbologyInter2of5)) {
		for (int i = kSymbologyFirst; i <= kSymbologyLast; i++)
			if (IsValidSymbology (i))
				if (SetSymbology (i))
					break;
	}

	InsertSubElement (0, * new CSubElement ());
	Build ();
}

CBarcodeElement::CBarcodeElement (const CBarcodeElement & rhs)
:	m_nSymbology (rhs.m_nSymbology),
	m_lMagID (rhs.m_lMagID),
	m_caption (rhs.m_caption),
	m_maxicode (rhs.m_maxicode),
	m_datamatrix (rhs.m_datamatrix),
	m_pBmp (NULL),
	m_size (rhs.m_size),
	m_nWidth (rhs.m_nWidth),
	m_nHeight (rhs.m_nHeight),
	m_orient (rhs.m_orient),
	CBaseElement (rhs)
{
	for (int i = 0; i < rhs.m_vSubElements.GetSize (); i++) 
		m_vSubElements.Add (new CSubElement (* (CSubElement *)rhs.m_vSubElements [i]));
}

CBarcodeElement & CBarcodeElement::operator = (const CBarcodeElement & rhs)
{
	CBaseElement::operator = (rhs);
	
	if (this != &rhs) {
		Free ();

		m_nSymbology		= rhs.m_nSymbology;
		m_lMagID			= rhs.m_lMagID;
		m_caption			= rhs.m_caption;
		m_maxicode			= rhs.m_maxicode;
		m_datamatrix		= rhs.m_datamatrix;
		m_size				= rhs.m_size;
		m_orient			= rhs.m_orient;
		m_nWidth			= rhs.m_nWidth;
		m_nHeight			= rhs.m_nHeight;

		DeleteAllSubElements ();
	
		for (int i = 0; i < rhs.m_vSubElements.GetSize (); i++) 
			m_vSubElements.Add (new CSubElement (* (CSubElement *)rhs.m_vSubElements [i]));
	}

	return * this;
}

CBarcodeElement::~CBarcodeElement()
{
	Free ();
}

void CBarcodeElement::Free (bool bBitmapOnly)
{
	if (m_pBmp) {
		delete m_pBmp;
		m_pBmp = NULL;
	}

	if (!bBitmapOnly)
		DeleteAllSubElements ();
}

bool CBarcodeElement::SetRedraw (bool bRedraw)
{
	if (bRedraw)
		Free (true);

	return CBaseElement::SetRedraw (bRedraw);
}

CString CBarcodeElement::ToString (const FoxjetCommon::CVersion & ver) const
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated

	CString str;
	CPoint pt = GetPosInternal (ALIGNMENT_ADJUST_ADD);

	str.Format (_T ("{Barcode,%u,%u,%u,%d,%d,%d,%d,%d,%d,%s,%s,%s,%s,%d,%d,%d,%d,%d,%d,%06X"),
		GetID (),
		pt.x, pt.y,
		IsFlippedH (),
		IsFlippedV (),
		IsInverse (),

		m_nSymbology, 
		m_lMagID,
		
		m_caption.m_bAuto,
		FormatString (m_caption.m_strText),

		FormatString (m_maxicode.m_strPostalCode),
		FormatString (m_maxicode.m_strCountryCode),
		FormatString (m_maxicode.m_strClassOfService),
		m_maxicode.m_nDataLen,
		m_maxicode.m_nMode,
		m_datamatrix.m_nLength,
		
		m_nWidth,
		m_nHeight,
		GetAlignment (),
		GetColor ());

	for (int i = 0; i < m_vSubElements.GetSize (); i++) {
		CSubElement * p = (CSubElement *)m_vSubElements [i];
		str += p->ToString (ver);
	}

	return str + _T ("}");
}

bool CBarcodeElement::FromString (const CString & strFrom, const FoxjetCommon::CVersion & ver)
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated

	CString str, strSubElements;
	const CString strDefault = ListGlobals::defElements.GetRegElementDefs (
		GetClassID (), ver);
	CStringArray vActual, vDef;
	CString strToken [] = {
		_T (""),			// 0:	Prefix (required)
		_T (""),			// 1:	ID (required)
		_T (""),			// 2:	x (required)
		_T (""),			// 3:	y (required)
		_T ("0"),			// 4:	flip h
		_T ("0"),			// 5:	flip v
		_T ("0"),			// 6:	inverse
		_T ("0"),			// 7:	symbology
		_T ("1"),			// 8:	magnification ID
		_T ("1"),			// 9:	auto caption update
		_T (""),			// 10:	caption text	
		_T ("000000000"),	// 11:	maxicode postal code
		_T ("840"),			// 12:	maxicode country code
		_T ("003"),			// 13:	maxicode class of service	
		_T ("0"),			// 14:	maxicode data length	
		_T ("2"),			// 15:	maxicode mode	
		_T ("0"),			// 16:	datamatrix data length
		_T ("14"),			// 17:	DataMatrix width
		_T ("14"),			// 18:	DataMatrix height
		_T ("0"),			// 19:	alignment
		_T ("0"),			// 20:	color
	};
	const int nFields = sizeof (strToken) / sizeof (strToken [0]);

	int i;
	
	PreParse (strFrom, str, strSubElements);

	#ifdef __TRACE_TOFROMSTRING__
		TRACEF (strFrom);
		TRACEF (str);
		TRACEF (strSubElements);
	#endif

	Tokenize (str, vActual);
	{
		CString s, sSub;

		PreParse (strDefault, s, sSub);
		Tokenize (s /* strDefault */, vDef);
	}

	for (int i = 0; i < vDef.GetSize (); i++)
		TRACEF (vDef [i]);

	if (vActual.GetSize () < 4) {
		TRACEF (_T ("Too few tokens"));
		return false;
	}

	DeleteAllSubElements ();

	for (i = 1; i < min (vDef.GetSize (), nFields); i++) 
		if (vDef [i].GetLength ()) 
			strToken [i] = vDef [i];

	for (i = 0; i < min (vActual.GetSize (), nFields); i++) 
		if (vActual [i].GetLength ()) 
			strToken [i] = vActual [i];

	if (strToken [0].CompareNoCase (_T ("Barcode")) != 0) {
		TRACEF (_T ("Wrong prefix \"") + strToken [0] + _T ("\""));
		return false;
	}

	CTextInfo c;
	CMaxiCodeInfo m;
	CDataMatrixInfo d;

	c.m_bAuto				= (_ttoi (strToken [9]) ? true : false);
	c.m_strText				= UnformatString (strToken [10]);
	
    m.m_strPostalCode		= UnformatString (strToken [11]);
    m.m_strCountryCode		= UnformatString (strToken [12]);
    m.m_strClassOfService	= UnformatString (strToken [13]);
    m.m_nDataLen			= _ttoi (strToken [14]);
    m.m_nMode				= _ttoi (strToken [15]);
	
	d.m_nLength				= _ttoi (strToken [16]);

	m_nWidth				= _ttoi (strToken [17]);
	m_nHeight				= _ttoi (strToken [18]);


	bool bResult = true;

	CPoint pt (
		(UINT)_tcstol (strToken [2], NULL, 10), 
		(UINT)_tcstol (strToken [3], NULL, 10));

	bResult &= SetID ((UINT)_tcstol (strToken [1], NULL, 10));
	bResult &= SetPos (pt, NULL);
	bResult &= SetFlippedH (_ttoi (strToken [4]) ? true : false);
	bResult &= SetFlippedV (_ttoi (strToken [5]) ? true : false);
	bResult &= SetInverse (_ttoi (strToken [6]) ? true : false);
	bResult &= SetSymbology (_ttoi (strToken [7])); 
	bResult &= SetAlignment ((ALIGNMENT)_ttoi (strToken [19]));
	bResult &= SetColor (_tcstoul (strToken [20], NULL, 16));

	
	//SetMaxiCodeInfo (m);
	SetDataMatrixInfo (d);

	if (bResult) {
		int nMag = _ttoi (strToken [8]);

		if (nMag > GetParamsPerSymbology ()) 
			nMag = 0;

		/*
		if (ver < CVersion (0, 13, 0, 0, 0)) {
			// prior to version 0.13, strToken [8] was the actual mag pct
			// in later versions, it specifies the ID of a CBarcodeParam object from the Settings table

			ULONG lLineID = GetLineID ();
			FoxjetDatabase::HEADTYPE type = GetHeadType ();
			int nSymbology = GetSymbology ();

			CBarcodeParams p = GetBarcodeParam (lLineID, nSymbology, type, nMag, ListGlobals::GetDB ());

			nMag = p.GetID ();
		}
		*/
		
		SetMagnification (nMag);
		// this can return false, but will set a valid mag anyway

		SubElementsFromString (strSubElements, ver);

		if (GetSubElementCount () == 0) {
			// no sub elements in the string, try the registry
			TRACEF (_T ("*** no sub elements in string ***"));

			SubElementsFromString (strDefault, ver);
		}

		if (GetSubElementCount () == 0) {
			// no sub elements in the registry, insert the
			// hard-coded default
			TRACEF (_T ("*** no sub elements in registry ***"));

			CSubElement * p = new CSubElement ();

			if (!InsertSubElement (0, * p)) {
				delete p;
				bResult = false;
				TRACEF (_T ("*** no sub elements inserted ***"));
			}
			else {
				TRACEF (_T ("inserted hard-coded default: ") + p->ToString (ver));
			}
		}
	}

	return bResult;
}

// parses str (the whole string used with FromString ()) 
// into the barcode data and the sub element data
void CBarcodeElement::PreParse (const CString & strFrom, 
								CString & str, 
								CString & strSubElements)
{
	CLongArray lhs;
	bool bMore;

	str = strFrom;

	do {
		bMore = false;
		str.TrimLeft ();
		str.TrimRight ();
		lhs = CountChars (str, '{');

		if (lhs.GetSize ()) {
			if (lhs [0] == 0) {
				CLongArray rhs = CountChars (str, '}');

				if (rhs.GetSize ()) 
					str.Delete (rhs [rhs.GetSize () - 1]);

				str.Delete (0);
				bMore = true;
			}
		}
	}
	while (bMore);

	if (lhs.GetSize ()) {
		strSubElements = str.Mid (lhs [0]);
		str = str.Left (lhs [0]);
	}

	#ifdef _DEBUG
	/*
	CString strDebug;
	strDebug.Format (
		"\n\tstrFrom        = %s"
		"\n\tstr            = %s"
		"\n\tstrSubElements = %s",
		strFrom, str, strSubElements);
	TRACEF (strDebug);
	*/
	#endif //_DEBUG
}

int CBarcodeElement::SubElementsFromString (const CString &strSubElements, 
											const CVersion & ver)
{
	CStringArray vSubElements;

	#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
	TRACEF ("strSubElements = " + strSubElements);
	#endif //_DEBUG && __TRACE_TOFROMSTRING__

	ParseSubElements (strSubElements, vSubElements);
	
	for (int i = 0; i < vSubElements.GetSize (); i++) {
		CSubElement * p = new CSubElement ();
		CString strSubElement = vSubElements [i];
	
		// std // #if __CUSTOM__ == __SW0840__
		p->SetLineID (GetLineID ());
		// std // #endif

		if (p->FromString (strSubElement, ver)) {
			if (p->IsValidData ()) {
				int nIndex = GetSubElementCount ();

				#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
				CString str;
				str.Format ("nIndex = %d", nIndex);
				TRACEF (str);
				#endif //_DEBUG	&& __TRACE_TOFROMSTRING__

				if (!InsertSubElement (nIndex, * p)) {
					#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
					TRACEF ("InsertSubElement (\"" + strSubElement + "\") failed");
					#endif //_DEBUG && __TRACE_TOFROMSTRING__ 

					delete p;
				}
				else {
					#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
					TRACEF ("inserted " + p->ToString (ver));
					#endif //_DEBUG && __TRACE_TOFROMSTRING__ 
				}
			}
			else {
				#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
				TRACEF ("invalid data: " + p->ToString (ver));
				#endif //_DEBUG && __TRACE_TOFROMSTRING__ 

				delete p;
			}
		}
		else {
			TRACEF ("CSubElement::FromString (\"" + strSubElement + "\") failed");
			delete p;
		}
	}

	return GetSubElementCount ();
}

void CBarcodeElement::ParseSubElements(const CString &str, CStringArray &v)
{
	// this fct depends on 'str' consisting of only sub elements 
	// delimited by braces, with no leading or trailing garbage.  
	// ie, "{TextObj,...}{TextObj,...}{TextObj,...}"
	#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
	CString strDebug;
	TRACEF ("ParseSubElements (\"" + str + "\", ...)");
	#endif //_DEBUG	&& __TRACE_TOFROMSTRING__ 

	CLongArray braces [] = {
		CountChars (str, '{'),
		CountChars (str, '}')
	};

	#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
	strDebug.Format ("braces [0].GetSize () = %u", braces [0].GetSize ());
	TRACEF (strDebug);
	strDebug.Format ("braces [1].GetSize () = %u", braces [1].GetSize ());
	TRACEF (strDebug);
	#endif //_DEBUG && __TRACE_TOFROMSTRING__

	v.RemoveAll ();

	if (braces [0].GetSize () &&
		braces [0].GetSize () <= braces [1].GetSize ()) 
	{
		int nSubElements = braces [0].GetSize ();

		for (int i = 0; i < nSubElements; i++) {
			int nLen = braces [1][i] - braces [0][i] - 1;
			CString strSubElement = str.Mid (braces [0][i] + 1, nLen);
			v.Add (strSubElement);

			#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
			strDebug.Format ("v [%d] = \"%s\"", i, v [i]);
			TRACEF (strDebug);
			#endif //_DEBUG	&& __TRACE_TOFROMSTRING__
		}
	}
}

int CBarcodeElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	CString strCaption;
	bool bFNC1 = true;
	//bool bAI = false;
	const CString strSection = ListGlobals::defElements.m_strRegSection + _T ("\\GS1");
	const TCHAR cFNC1 = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("FNC1"), 0xe8);
	CBarcodeParams param = ListGlobals::IsDBSet () ? 
		GetBarcodeParam (GetLineID (), GetLocalParamUID (), GetSymbology (), GetHeadType (), GetHead ().GetRes (), GetMagnification (), ListGlobals::GetDB ()) : 
		CBarcodeElement::GetLocalParam (GetSymbology (), GetSymbology ()); // w/out db, id = symbology

	m_strImageData = m_strEncoding = _T ("");

	Free (true);

	//for (int i = 0; !bAI && i < m_vSubElements.GetSize (); i++) {
	//	CSubElement * p = (CSubElement *)m_vSubElements [i];

	//	if (p->IsAi ())
	//		bAI = true;
	//}

	for (int i = 0; i < m_vSubElements.GetSize (); i++) {
		CSubElement * p = (CSubElement *)m_vSubElements [i];

		p->Build (type, bCanThrowException);
		CString str = p->GetElement ()->GetImageData ();

		if (p->GetElement ()->GetClassID () == SERIAL) {
			int n = ((CSerialElement *)p->GetElement ())->GetLength ();
			int nLen = str.GetLength ();

			for (int i = 0; i < nLen; i++) {
				TCHAR c = str [i];

				switch (c) {
				case '\0':
				case '\r':
				case '\n':
					str = str.Mid (0, i);
					nLen = str.GetLength (); // don't pad end with 0's
					break;
				}
			}

			switch (GetSymbology ()) {
			case kSymbologyInter2of5:
			case kSymbologyGTIN14:
			case kSymbologyUPCA:
			case kSymbologyUPCE:
			case kSymbologyPostNet:
			case kSymbologyEANJAN_8:
			case kSymbologyEANJAN_13:
				for (int i = 0; i < nLen; i++) 
					str.SetAt (i, BindTo (str [i], (TCHAR)'0', (TCHAR)'9'));

				break;
			}
		}
		/*
		else if (p->GetElement ()->GetClassID () == USER) {
			bool bMore = true;

			str = p->GetData (); 

			while (bMore) {
				CString strAI = Extract (str, _T ("("), _T (")"));

				if (!strAI.GetLength ())
					break;
				else {
					switch (m_nSymbology) {
					case kSymbologyEAN128: 
						str.Replace (_T ("(") + strAI + _T (")"), (bFNC1 ? CString ((TCHAR)DC1) : _T ("")) + strAI);
						bFNC1 = !param.IsOptimizedFNC1 () ? true : CSubElement::IsVariableLen (strAI);
						break;
					case kSymbologyDataMatrixEcc200:
					case kSymbologyDataMatrixGs1:
						// a // ASCII
						// c // C40
						// t // Text
						// x // X12
						str.Replace (_T ("(") + strAI + _T (")"), (bFNC1 ? CString (cFNC1) : _T ("")) + strAI);
						bFNC1 = !param.IsOptimizedFNC1 () ? true : CSubElement::IsVariableLen (strAI);
						break;
					default:
						bMore = false;
						break;
					}
				}
			}

			TRACEF (FoxjetDatabase::Format (str, str.GetLength ()));
			m_strImageData += str;
			m_strEncoding += CString ('a', str.GetLength ());
			strCaption += p->GetElement ()->GetImageData ();
			continue;
		}
		*/

		if (p->IsAi ()) {
			switch (m_nSymbology) {
			case kSymbologyEAN128: 
				if (bFNC1)
					m_strImageData += CString ((TCHAR)DC1); // place holder for FUNCTION1 // sw0814

				m_strImageData += p->m_strID;
				bFNC1 = p->IsVariableLen ();
				break;
			case kSymbologyDataMatrixEcc200:
			case kSymbologyDataMatrixGs1:
				// a // ASCII
				// c // C40
				// t // Text
				// x // X12

				if (bFNC1) {
					m_strImageData += CString (cFNC1); 
					m_strImageData += p->m_strID;
					m_strEncoding += 'a' + CString ('a', p->m_strID.GetLength ());
				}
				else {
					m_strImageData += p->m_strID;
					m_strEncoding += CString ('a', p->m_strID.GetLength ());
				}

				bFNC1 = p->IsVariableLen ();
				break;
			default:
				m_strImageData += p->m_strID;
				break;
			}

			strCaption += '(' + p->m_strID + ')';
		}
		//else {
		//	if (bAI) {
		//		if (bFNC1) {
		//			str = CString ((TCHAR)DC1) + str; 
		//			bFNC1 = false;
		//		}
		//	}
		//}

		m_strImageData += str;
		m_strEncoding += CString ('a', str.GetLength ());
		strCaption += str;
	}

	switch (m_nSymbology) {
	case kSymbologyDataMatrixEcc200:
	case kSymbologyDataMatrixGs1:
		m_caption.m_strText = strCaption;
		break;
	}

	if (m_nSymbology == kSymbologyDataMatrixGs1) {
		if (m_strImageData.Find (cFNC1) != 0) {
			m_strImageData	= CString (cFNC1) + m_strImageData;
			m_strEncoding	= 'a' + m_strEncoding;
		}
	}

	if (!IsValid ())
		m_strImageData = GetDefaultData ();

	//TRACEF (FoxjetDatabase::Format (m_strImageData, m_strImageData.GetLength ()) + _T ("  encoding: ") + m_strEncoding);

	return GetClassID ();
}

CSize CBarcodeElement::GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI) const
{
	return m_size;
}

int CBarcodeElement::GetSymbology () const
{
	return m_nSymbology;
}

bool CBarcodeElement::SetSymbology (int nSymbology)
{
	if (nSymbology == 17)
		nSymbology++;

	if (IsValidSymbology (nSymbology)) {
		m_nSymbology = nSymbology;
		SetRedraw ();
		return true;
	}
	else {
		TRACEF ("SetSymbology failed: " + FoxjetElements::GetSymbology (nSymbology, ListGlobals::GetDB ()));

		return false;
	}
}

ULONG CBarcodeElement::GetMagnification () const
{
	return m_lMagID;
}

bool CBarcodeElement::SetMagnification (ULONG nMagnification)
{
	m_lMagID = nMagnification;
	SetRedraw ();

	return true;
}

bool CBarcodeElement::IsValid(int nSymbology, int nSubElements, bool bThrowException)
{
	bool bMultiple = SymbologyAllowsSubelements (nSymbology);
	
	if (!nSubElements) {
		if (bThrowException)
			throw new CBarcodeException (LoadString (IDS_BARCODECONTAINSNOSUBELEMENTS));

		return false;
	}
	if (!bMultiple && nSubElements > 1) {
		if (bThrowException)
			throw new CBarcodeException (LoadString (IDS_MORETHANONESUBELEMENTS));

		return false;
	}

	return true;
}

bool CBarcodeElement::IsValid(int nSymbology, CArray <FoxjetElements::CSubElement, FoxjetElements::CSubElement& > & vSubElements, bool bThrowException)
{
	return IsValid (nSymbology, vSubElements.GetSize (), bThrowException);
}

bool CBarcodeElement::IsValid(bool bThrowException) const
{
	return IsValid (m_nSymbology, m_vSubElements.GetSize (), bThrowException);
}

bool CBarcodeElement::IsValidSymbology(int nSymbology)
{
	static const int nValid [] =
	{
//		kSymbologyCode39Regular,    // Code 3 of 9 standard set, DO NOT USE THIS ONE!
//		kSymbologyCode39Full,		// Code 3 of 9 Full ASCII set....USE THIS ONE!
		kSymbologyCode39Standard,	// Standard Code39                          // 9710310
		kSymbologyInter2of5,		// Interleave 2of5
		kSymbologyGTIN14,
//		kSymbologyCodabar,			// Codabar
//		kSymbologyMSI,				// MSI
		kSymbologyCode93,    		// Code 93, full ASCII set

		kSymbologyUPCA,				// UPC A
		kSymbologyEANJAN_8,			// EAN/JAN 8
		kSymbologyEANJAN_13,		// EAN/JAN 13
		kSymbologyUPCE,				// UPC E

//		kSymbologyPostNet,			// PostNet
		kSymbologyCode128,			// Code128                                  // 9606240
//		kSymbologyPDF417,			// PDF417
//		kSymbologyMaxiCode,			// MaxiCode
		kSymbologyDataMatrixEcc200,		// DataMatrix
		kSymbologyDataMatrixGs1,		// DataMatrix
		kSymbologyQR,
//		kSymbologyLOGMARS,			// LOGMARS

		kSymbologyEAN128,
	};

	if (!::bDataMatrix && (nSymbology == kSymbologyDataMatrixEcc200 || nSymbology == kSymbologyDataMatrixGs1))
		return false;

	for (int i = 0; i < ARRAYSIZE (nValid); i++) 
		if (nValid [i] == nSymbology)
			return true;

	return false;
}

bool CBarcodeElement::IsDataMatrixEnabled () 
{
	return ::bDataMatrix;
}

void CBarcodeElement::EnableDataMatrix (bool bEnable)
{
	::bDataMatrix = bEnable;
}

bool CBarcodeElement::IsValidMagnification(int nMag) const
{
	return IsValidMagnification (GetSymbology (), nMag);
}

bool CBarcodeElement::IsValidMagnification(int nSymbology, int nMag)
{
	bool bResult = (nMag >= 10 && nMag <= 500);

/*	
	switch (nSymbology) {
	case kSymbologyCode39Regular:
	case kSymbologyCode39Full:
	case kSymbologyInter2of5:
	case kSymbologyGTIN14:
	case kSymbologyCodabar:
	case kSymbologyMSI:
	case kSymbologyCode93:
	case kSymbologyUPCA:
	case kSymbologyUPCE:
	case kSymbologyPostNet:
	case kSymbologyEANJAN_8:
	case kSymbologyEANJAN_13:
	case kSymbologyCode128:
	case kSymbologyCode39Standard:
	case kSymbologyPDF417:
	case kSymbologyMaxiCode:
	case kSymbologyDataMatrixEcc200:
	case kSymbologyDataMatrixGs1:
	case kSymbologyLOGMARS:
	}
*/

	return bResult;
}

int CBarcodeElement::GetSubElementCount () const
{
	return m_vSubElements.GetSize ();
}

const CSubElement & CBarcodeElement::GetSubElement (int nIndex) const
{
	ASSERT (nIndex >= 0 && nIndex < m_vSubElements.GetSize ());
	return * (CSubElement *)m_vSubElements [nIndex];
}

CSubElement & CBarcodeElement::GetSubElement (int nIndex)
{
	ASSERT (nIndex >= 0 && nIndex < m_vSubElements.GetSize ());
	return * (CSubElement *)m_vSubElements [nIndex];
}

bool CBarcodeElement::DeleteSubElement (int nIndex)
{
	TRACEF (_T ("CBarcodeElement::DeleteAllSubElements (int)"));

	if (nIndex >= 0 && nIndex < m_vSubElements.GetSize ()) {
		CSubElement * p = (CSubElement *)m_vSubElements [nIndex];
		m_vSubElements.RemoveAt (nIndex);
		delete p;
		return true;
	}

	return false;
}

void CBarcodeElement::DeleteAllSubElements ()
{
	#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
	TRACEF ("CBarcodeElement::DeleteAllSubElements ()");
	#endif //_DEBUG && __TRACE_TOFROMSTRING__

	for (int i = 0; i < m_vSubElements.GetSize (); i++) {
		CSubElement * p = (CSubElement *)m_vSubElements [i];
		delete p;
	}

	m_vSubElements.RemoveAll ();
}

bool CBarcodeElement::InsertSubElement (int nIndex, CSubElement & obj)
{
	bool bResult = false;

	if (nIndex >= 0 && nIndex <= m_vSubElements.GetSize ()) 
		bResult = 
			SymbologyAllowsSubelements (m_nSymbology) ||
			(m_vSubElements.GetSize () == 0);

	if (bResult)
		m_vSubElements.InsertAt (nIndex, &obj);

	#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
	CString str;
	str.Format ("InsertSubElement (%d, %s) = %s",
		nIndex, obj.ToString (CVersion ()), 
		bResult ? "true" : "false");
	TRACEF (str);
	#endif //_DEBUG && __TRACE_TOFROMSTRING__ 


	return bResult;
}


CString CBarcodeElement::GetTextData() const
{
	CString str;

	for (int i = 0; i < m_vSubElements.GetSize (); i++) {
		CSubElement & obj = * (CSubElement *)m_vSubElements [i];
		str += obj.GetData ();
	}

	return str;
}

CString CBarcodeElement::GetDefaultData () const
{
	return m_strImageData;
}

CString CBarcodeElement::GetImageData() const
{
	return GetDefaultData ();
}

bool CBarcodeElement::SymbologyAllowsSubelements(int nSymbology)
{
	switch (nSymbology) {
	case kSymbologyCode128: 
	case kSymbologyEAN128: 
	case kSymbologyEANJAN_8: 
	case kSymbologyEANJAN_13:
	case kSymbologyDataMatrixEcc200:
	case kSymbologyDataMatrixGs1:
	case kSymbologyQR:

	// std // #if __CUSTOM__ == __SW0840__
	case kSymbologyInter2of5:
	case kSymbologyGTIN14:
	// std // #endif

		return true;
	}

	return false;
}

bool CBarcodeElement::SymbologyAllowsAI (int nSymbology)
{
	return SymbologyAllowsSubelements (nSymbology);
}

int CBarcodeElement::GetBarWidthMin (int nSymbology)
{
/* TODO: rem
	bool bUpcEanJan = 
		nSymbology == kSymbologyUPCA ||
		nSymbology == kSymbologyUPCE ||
		nSymbology == kSymbologyEANJAN_8 ||
		nSymbology == kSymbologyEANJAN_13;
	return bUpcEanJan ? 80 : 1;
*/
	return 1;
}

int CBarcodeElement::GetBarWidthMax (int nSymbology)
{
	return 200;
/*
	bool bUpcEanJan = 
		nSymbology == kSymbologyUPCA ||
		nSymbology == kSymbologyUPCE ||
		nSymbology == kSymbologyEANJAN_8 ||
		nSymbology == kSymbologyEANJAN_13;
	return bUpcEanJan ? 200.0F : FLT_MAX;
*/
}

bool CBarcodeElement::IsValidBarWidth (int nSymbology, int nValue)
{
	switch (nSymbology) {
	case kSymbologyMaxiCode:			// not applicable
	case kSymbologyDataMatrixEcc200:	// not applicable
	case kSymbologyDataMatrixGs1:		// not applicable
	case kSymbologyQR:
		return true;
	default:
		return 
			nValue >= GetBarWidthMin (nSymbology) && 
			nValue <= GetBarWidthMax (nSymbology);
	}
}

bool CBarcodeElement::IsValidRatio (int nSymbology, int nValue)
{
	switch (nSymbology) {
	case kSymbologyCode39Regular:
	case kSymbologyCode39Full:
	case kSymbologyInter2of5:
	case kSymbologyGTIN14:
	case kSymbologyCodabar:
	case kSymbologyMSI:
		return nValue >= (int)m_lmtRatio.m_dwMin && nValue <= (int)m_lmtRatio.m_dwMax;
	default:
		return true;
	}
}

namespace Code39 { bool IsValidData (const CString & str); };
namespace Code93 { bool IsValidData (const CString & str); };

CSize CBarcodeElement::Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly, 
							 FoxjetCommon::DRAWCONTEXT context)
{
	CSize sizeActual (0, 0);
	const bool bUsesWasp = UsesWasp (GetSymbology ());

	if (context == PRINTER) {
		if (bUsesWasp)
			m_bFlippedV = !m_bFlippedV;
	}

	if (!m_pBmp) {
		CDC dcTmp;
		dcTmp.CreateCompatibleDC (&dc);
		m_size = DrawBarcode (dcTmp, head, sizeActual, context, false);

		if (GetOrientation () == VERTICAL) 
			swap (m_size.cx, m_size.cy);

		if (!bCalcSizeOnly) {
			CDC dcMem;
			CBitmap bmp;
			
			dcMem.CreateCompatibleDC (&dc);
			bmp.CreateCompatibleBitmap (&dc, sizeActual.cx, sizeActual.cy);

			CGdiObject * pOldBmp = dcMem.SelectObject (&bmp);

			if (bUsesWasp)
				::BitBlt (dcMem, 0, 0, sizeActual.cx, sizeActual.cy, NULL, 0, 0, IsInverse () ? BLACKNESS : WHITENESS);
			else
				::BitBlt (dcMem, 0, 0, sizeActual.cx, sizeActual.cy, NULL, 0, 0, WHITENESS);

			DrawBarcode (dcMem, head, sizeActual, context, false);

			if (GetOrientation () == VERTICAL) 
				swap (sizeActual.cx, sizeActual.cy);

			m_pBmp = new CRawBitmap (bmp);
			dcMem.SelectObject (pOldBmp);
		}
	}

	if (!bCalcSizeOnly) {
		if (m_pBmp) {
			if (GetOrientation () == VERTICAL) {
				CxImage img;
				CBitmap bmp;

				m_pBmp->GetBitmap (bmp);
				VERIFY (img.CreateFromHBITMAP (bmp));
				
				if (context == EDITOR)
					VERIFY (img.RotateRight ());
				else
					VERIFY (img.RotateLeft ());

				if (HBITMAP hBmp = img.MakeBitmap (dc)) {
					m_pBmp->Load (hBmp);
					::DeleteObject (hBmp);
				}
			}

			bool bInvert = IsInverse ();
			CDC dcMem;
			DIBSECTION ds;
			int x = 0, y = 0, cx = sizeActual.cx, cy = sizeActual.cy;

			dc.FillSolidRect (x, y, cx, cy, rgbWhite);

			if (IsFlippedV ()) {
				y += (cy - 1);
				cy *= -1;
			}
			if (IsFlippedH ()) {
				x += (cx - 1);
				cx *= -1;
			}

			CBitmap bmp;

			m_pBmp->GetBitmap (bmp);
			::ZeroMemory (&ds, sizeof (ds));
			VERIFY (bmp.GetObject (sizeof (DIBSECTION), &ds));
			dcMem.CreateCompatibleDC (&dc);
			CBitmap * pOld = dcMem.SelectObject (&bmp);

			if (context == PRINTER) {
				bInvert = !bInvert;

				if (IsFlippedV () ^ IsFlippedH ())
					Rotate180 (dcMem, sizeActual);
			}

			int nXOriginSrc = 0;
			int nYOriginSrc = 0;
			int nWidthSrc	= ds.dsBm.bmWidth;
			int nHeightSrc	= ds.dsBm.bmHeight;

			
#ifdef __WYSIWYGFIX__
			if (context == EDITOR) {
				double dScreenRes = CCoord::GetPixelsPerXUnit (INCHES, head);
				double dPrintRes = CalcPrintRes (head);
				double dFactor = dScreenRes / dPrintRes;

				x = (int)((double)x * dFactor);
				cx = (int)((double)cx * dFactor);
			}
#endif

			dc.StretchBlt (x, y, cx, cy,
				&dcMem, nXOriginSrc, nYOriginSrc, nWidthSrc, nHeightSrc, 
				context == PRINTER ? SRCINVERT : SRCCOPY);

			dcMem.SelectObject (pOld);

			if (GetOrientation () == VERTICAL) {
				swap (m_size.cx, m_size.cy);
				swap (sizeActual.cx, sizeActual.cy);
			}
		}
		else {
			dc.FillSolidRect (0, 0, sizeActual.cx, sizeActual.cy, rgbDarkGray);
		}
	}

	if (GetOrientation () == VERTICAL) {
		int nMax1000ths = CBaseElement::LogicalToThousandths (CPoint (0, head.GetChannels ()), head).y;

		m_size.cy = BindTo ((int)m_size.cy, 1, nMax1000ths);
		sizeActual.cy = BindTo ((int)sizeActual.cy, 1, head.GetChannels ());
	}

	if (context == PRINTER) {
		if (bUsesWasp)
			m_bFlippedV = !m_bFlippedV;

		if (GetOrientation () == VERTICAL) 
			swap (sizeActual.cx, sizeActual.cy);

		return sizeActual;
	}
	else 
		return m_size;
}

void CBarcodeElement::BcThousandthsToLogical (BARCODEINFO & bc, CDC & dc, 
											  FoxjetCommon::DRAWCONTEXT context)
{
	const double dBPP [] = 
	{
		(double)dc.GetDeviceCaps (LOGPIXELSX),
		(double)dc.GetDeviceCaps (LOGPIXELSY)
	};

	int x = context == EDITOR ? 0 : 1;
	int y = context == EDITOR ? 1 : 0;

	bc.fDrawnWidth	= (bc.fDrawnWidth / 1000.0F) * (float)dBPP [x];
	bc.fDrawnHeight	= (bc.fDrawnHeight / 1000.0F) * (float)dBPP [y];
	bc.fAutoHeight	= (bc.fAutoHeight / 1000.0F) * (float)dBPP [y];
}


/* TODO: rem
CString CBarcodeElement::CalcUPCEANCheckDigit (const CString & strData) 
{
	CString strResult = _T ("0");
	CString str (strData);
	int nEven = 0, nOdd = 0;

	str.MakeReverse ();
	
	for (int i = 0; i < str.GetLength (); i++) {
		int n = str [i] - '0';

		if (!(i % 2))
			nEven += n;
		else
			nOdd += n;
	}

	int nSum = nOdd + (nEven * 3);
	int nMultiple = (int)ceil ((double)nSum / 10.0);
	int nCheckDigit = (nMultiple * 10) - nSum;

	strResult.Format (_T ("%d"), nCheckDigit);

	return strResult;
}

bool CBarcodeElement::IsValidUPCData (const CString & str)
{
	switch (str.GetLength ()) {
	case 11:
		return IsNumeric (str);
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////
bool CBarcodeElement::IsValidEAN8Data (const CString & str)
{
	switch (str.GetLength ()) {
	case 7:
		return IsNumeric (str);
	}

	return false;
}

bool CBarcodeElement::IsValidEAN13Data (const CString & str)
{
	switch (str.GetLength ()) {
	case 12:
		return IsNumeric (str);
	}

	return false;
}
*/


/////////////////////////////////

int CBarcodeElement::DrawEAN8 (const CString & str, const CBarcodeParams & params, LPBYTE pBuffer, 
							   UINT nBufferWidth, ULONG nBufferSize, CDC * pDC, CBitmap * pbmpCaption,
							   FoxjetCommon::DRAWCONTEXT context, 
							   CString * pstrFormattedCaption)
{
	using namespace UPCA;
	using namespace EAN;

	int nWidth = 0;

	if (EAN8::IsValidData (str)) {
		const CString strCheckDigit		= UPCA::CalcCheckDigit (str);
		const CString strRight			= str.Right (3) + strCheckDigit;
		const CString strLeft			= str.Mid (0, 4);

		//TRACEF (strLeft + " " + strRight);

		const int nMargin				= params.GetSpacePixels (1) * 9;
		const int nGuard				= (params.GetBarPixels (1) * 2) + params.GetSpacePixels (1);
		const int nCenter				= (params.GetBarPixels (1) * 2) + (params.GetSpacePixels (1) * 3);
		const int nBarHeight			= params.GetBarHeight () - (params.GetSize () / 2);
		const int nDataBarHeight		= params.GetBarHeight () - params.GetSize () - 1;
		int nRightCaption [2]			= { 0 };
		int nLeftCaption [2]			= { 0 };
		const BYTE nFullBarMask = GetBarMask (nBarHeight);
		const BYTE nDataBarMask = GetBarMask (nDataBarHeight);

		nWidth += nMargin;

		for (int i = 0; i < ARRAYSIZE (nEncGuard); i++) {
			int n = nEncGuard [i];
			bool bBar = n == 1;
			int cx;

			if (bBar)
				cx = params.GetBarPixels (1);
			else
				cx = params.GetSpacePixels (1);
 
			if (pBuffer) {
				for (int nCol = 0; nCol < cx; nCol++) { 
					int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
					int nBytes = nBarHeight / 8;
	
					memset (&pBuffer [nIndex], bBar ? 0 : ~0, nBytes); 

					if (bBar) 
						memcpy (&pBuffer [nIndex + nBytes], &nFullBarMask, 1); 
				}
			}

			nWidth += cx;
		}

		for (int i = 0; i < strLeft.GetLength (); i++) {
			int nDigit = _ttoi (strLeft.Mid (i, 1));

			for (int nBar = 0; nBar < 4; nBar++) {
				int n = nEncA [nDigit][nBar];
				bool bEven = (nBar % 2) == 0 ? true : false; 
				int cx;

				if (bEven)
					cx = params.GetSpacePixels (n);
				else
					cx = params.GetBarPixels (n);

				if (pBuffer) {
					const int nRowHeight = nDataBarHeight;
					const BYTE nMask = nDataBarMask;

					if (i == 0)
						nLeftCaption [0] = nWidth;
					else if (i == strLeft.GetLength () - 1)
						nLeftCaption [1] = nWidth;

					for (int nCol = 0; nCol < cx; nCol++) { 
						int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
						int nBytes = nRowHeight / 8;

						memset (&pBuffer [nIndex], bEven ? ~0 : 0, nBytes); 

						if (!bEven) 
							memcpy (&pBuffer [nIndex + nBytes], &nMask, 1); 
					}
				}

				nWidth += cx;
			}
		}

		for (int i = 0; i < ARRAYSIZE (nEncCenter); i++) {
			int n = nEncCenter [i];
			bool bBar = n == 1;
			int cx;

			if (bBar)
				cx = params.GetBarPixels (1);
			else
				cx = params.GetSpacePixels (1);

			if (pBuffer) {
				for (int nCol = 0; nCol < cx; nCol++) { 
					int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
					int nBytes = nBarHeight / 8;

					memset (&pBuffer [nIndex], bBar ? 0 : ~0, nBytes); 

					if (bBar) 
						memcpy (&pBuffer [nIndex + nBytes], &nFullBarMask, 1); 
				}
			}

			nWidth += cx;
		}

		for (int i = 0; i < strRight.GetLength (); i++) {
			int nDigit = _ttoi (strRight.Mid (i, 1));

			for (int nBar = 0; nBar < 4; nBar++) {
				int n = nEncC [nDigit][nBar];
				bool bEven = (nBar % 2) == 0 ? true : false; 
				int cx;

				if (bEven)
					cx = params.GetBarPixels (n);
				else
					cx = params.GetSpacePixels (n);

				if (pBuffer) {
					const int nRowHeight = nDataBarHeight;
					const BYTE nMask = nDataBarMask;

					if (i == 0)
						nRightCaption [0] = nWidth;
					else if (i == strRight.GetLength () - 1)
						nRightCaption [1] = nWidth;

					for (int nCol = 0; nCol < cx; nCol++) { 
						int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
						int nBytes = nRowHeight / 8;

						memset (&pBuffer [nIndex], bEven ? 0 : ~0, nBytes); 

						if (bEven) 
							memcpy (&pBuffer [nIndex + nBytes], &nMask, 1); 
					}
				}

				nWidth += cx;
			}
		}

		for (int i = 0; i < ARRAYSIZE (nEncGuard); i++) {
			int n = nEncGuard [i];
			bool bBar = n == 1;
			int cx;

			if (bBar)
				cx = params.GetBarPixels (1);
			else
				cx = params.GetSpacePixels (1);

			if (pBuffer) {
				for (int nCol = 0; nCol < cx; nCol++) { 
					int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
					int nBytes = nBarHeight / 8;

					memset (&pBuffer [nIndex], bBar ? 0 : ~0, nBytes); 

					if (bBar) 
						memcpy (&pBuffer [nIndex + nBytes], &nFullBarMask, 1); 
				}
			}

			nWidth += cx;
		}

		nWidth += nMargin;

		if (pBuffer && pbmpCaption) {
			CFont fntFull;
			CDC dcMem;
			CBitmap & bmp = * pbmpCaption;
			const int nEscapement = context == PRINTER ? -900 : 0; 
			const int nFontSize = max (params.GetSize (), 6);
			const int nFontWidth = params.GetFontWidth ();
			const bool bBold = (params.GetStyle () & kCapStyleBold) ? true : false;
			const bool bItalic = (params.GetStyle () & kCapStyleItalic) ? true : false;
			const int x = nFontSize + ((context == PRINTER) ? 1 : 0);
			int y = 0;
			int cx = nFontSize, cy = nWidth;
			CRect rc (0, 0, 0, 0);

			if (context == EDITOR)
				swap (cx, cy);

			ASSERT (pDC);

			fntFull.CreateFont (nFontSize, nFontWidth, nEscapement, nEscapement,
				bBold ? FW_BOLD : FW_NORMAL, 
				bItalic ? TRUE : FALSE, 0, 0,
				DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
				DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, params.GetFont ()); 
			
			VERIFY (dcMem.CreateCompatibleDC (pDC));
			VERIFY (bmp.CreateCompatibleBitmap (pDC, cx, cy));

			CBitmap * pBmp			= dcMem.SelectObject (&bmp);
			CFont * pFont			= dcMem.SelectObject (&fntFull);
			COLORREF rgbTextColor	= dcMem.SetTextColor (rgbBlack);
			COLORREF rgbBkColor		= dcMem.SetBkColor (rgbWhite);
			int nBkMode				= dcMem.SetBkMode (OPAQUE);

			::BitBlt (dcMem, 0, 0, cx, cy, NULL, 0, 0, WHITENESS);

			dcMem.DrawText (strLeft, &rc, DT_CALCRECT);
			y = nLeftCaption [0] + (((nLeftCaption [1] - nLeftCaption [0]) - rc.Width ()) / 2);
			if (context == PRINTER) dcMem.TextOut (x, y, strLeft);
			else					dcMem.DrawText (strLeft, rc + CPoint (y, 0), 0);

			dcMem.DrawText (strRight, &rc, DT_CALCRECT);
			y = nRightCaption [0] + (((nRightCaption [1] - nRightCaption [0]) - rc.Width ()) / 2);
			if (context == PRINTER) dcMem.TextOut (x, y, strRight);
			else					dcMem.DrawText (strRight, rc + CPoint (y, 0), 0);

			dcMem.SetTextColor (rgbTextColor);
			dcMem.SetBkColor (rgbBkColor);
			dcMem.SetBkMode (nBkMode);

			dcMem.SelectObject (pFont);
			dcMem.SelectObject (pBmp);
		}

		if (pstrFormattedCaption)
			* pstrFormattedCaption = strLeft + _T (" ") + strRight;

	}

	return nWidth;
}

int CBarcodeElement::DrawEAN13 (const CString & str, const CBarcodeParams & params, LPBYTE pBuffer, 
							   UINT nBufferWidth, ULONG nBufferSize, CDC * pDC, CBitmap * pbmpCaption,
							   FoxjetCommon::DRAWCONTEXT context,
							   CString * pstrFormattedCaption)
{
	using namespace UPCA;
	using namespace EAN;

	int nWidth = 0;

	if (EAN13::IsValidData (str)) {
		const CString strNumberSys		= str.Mid (0, 1); 
		const CString strCheckDigit		= UPCA::CalcCheckDigit (str);
		const CString strRight			= str.Right (5) + strCheckDigit;
		const CString strLeft			= str.Mid (1, 6);
		const int nMargin				= params.GetSpacePixels (1) * 9;
		const int nGuard				= (params.GetBarPixels (1) * 2) + params.GetSpacePixels (1);
		const int nCenter				= (params.GetBarPixels (1) * 2) + (params.GetSpacePixels (1) * 3);
		const int nBarHeight			= params.GetBarHeight () - (params.GetSize () / 2);
		const int nDataBarHeight		= params.GetBarHeight () - params.GetSize () - 1;
		int nRightCaption [2]			= { 0 };
		int nLeftCaption [2]			= { 0 };
		const BYTE nFullBarMask = GetBarMask (nBarHeight);
		const BYTE nDataBarMask = GetBarMask (nDataBarHeight);
		const int n13thDigit = _ttoi (strNumberSys);

		nWidth += nMargin;

		for (int i = 0; i < ARRAYSIZE (nEncGuard); i++) {
			int n = nEncGuard [i];
			bool bBar = n == 1;
			int cx;

			if (bBar)
				cx = params.GetBarPixels (1);
			else
				cx = params.GetSpacePixels (1);
 
			if (pBuffer) {
				for (int nCol = 0; nCol < cx; nCol++) { 
					int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
					int nBytes = nBarHeight / 8;
	
					memset (&pBuffer [nIndex], bBar ? 0 : ~0, nBytes); 

					if (bBar) 
						memcpy (&pBuffer [nIndex + nBytes], &nFullBarMask, 1); 
				}
			}

			nWidth += cx;
		}

		for (int i = 0; i < strLeft.GetLength (); i++) {
			int nDigit = _ttoi (strLeft.Mid (i, 1));
			const EANNUMBERSET numberset = (EANNUMBERSET)nNumberset [n13thDigit][i];

			for (int nBar = 0; nBar < 4; nBar++) {
				int n = (numberset == A) ? nEncA [nDigit][nBar] : nEncB [nDigit][nBar];
				bool bEven = (nBar % 2) == 0 ? true : false; 
				int cx;

				if (bEven)
					cx = params.GetSpacePixels (n);
				else
					cx = params.GetBarPixels (n);

				if (pBuffer) {
					const int nRowHeight = nDataBarHeight;
					const BYTE nMask = nDataBarMask;

					if (i == 0)
						nLeftCaption [0] = nWidth;
					else if (i == strLeft.GetLength () - 1)
						nLeftCaption [1] = nWidth;

					for (int nCol = 0; nCol < cx; nCol++) { 
						int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
						int nBytes = nRowHeight / 8;

						memset (&pBuffer [nIndex], bEven ? ~0 : 0, nBytes); 

						if (!bEven) 
							memcpy (&pBuffer [nIndex + nBytes], &nMask, 1); 
					}
				}

				nWidth += cx;
			}
		}

		for (int i = 0; i < ARRAYSIZE (nEncCenter); i++) {
			int n = nEncCenter [i];
			bool bBar = n == 1;
			int cx;

			if (bBar)
				cx = params.GetBarPixels (1);
			else
				cx = params.GetSpacePixels (1);

			if (pBuffer) {
				for (int nCol = 0; nCol < cx; nCol++) { 
					int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
					int nBytes = nBarHeight / 8;

					memset (&pBuffer [nIndex], bBar ? 0 : ~0, nBytes); 

					if (bBar) 
						memcpy (&pBuffer [nIndex + nBytes], &nFullBarMask, 1); 
				}
			}

			nWidth += cx;
		}

		for (int i = 0; i < strRight.GetLength (); i++) {
			int nDigit = _ttoi (strRight.Mid (i, 1));

			for (int nBar = 0; nBar < 4; nBar++) {
				int n = nEncC [nDigit][nBar];
				bool bEven = (nBar % 2) == 0 ? true : false; 
				int cx;

				if (bEven)
					cx = params.GetBarPixels (n);
				else
					cx = params.GetSpacePixels (n);

				if (pBuffer) {
					const int nRowHeight = nDataBarHeight;
					const BYTE nMask = nDataBarMask;

					if (i == 0)
						nRightCaption [0] = nWidth;
					else if (i == strRight.GetLength () - 1)
						nRightCaption [1] = nWidth;

					for (int nCol = 0; nCol < cx; nCol++) { 
						int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
						int nBytes = nRowHeight / 8;

						memset (&pBuffer [nIndex], bEven ? 0 : ~0, nBytes); 

						if (bEven) 
							memcpy (&pBuffer [nIndex + nBytes], &nMask, 1); 
					}
				}

				nWidth += cx;
			}
		}

		for (int i = 0; i < ARRAYSIZE (nEncGuard); i++) {
			int n = nEncGuard [i];
			bool bBar = n == 1;
			int cx;

			if (bBar)
				cx = params.GetBarPixels (1);
			else
				cx = params.GetSpacePixels (1);

			if (pBuffer) {
				for (int nCol = 0; nCol < cx; nCol++) { 
					int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
					int nBytes = nBarHeight / 8;

					memset (&pBuffer [nIndex], bBar ? 0 : ~0, nBytes); 

					if (bBar) 
						memcpy (&pBuffer [nIndex + nBytes], &nFullBarMask, 1); 
				}
			}

			nWidth += cx;
		}

		nWidth += nMargin;

		if (pBuffer && pbmpCaption) {
			CFont fntFull, fntHalf;
			CDC dcMem;
			CBitmap & bmp = * pbmpCaption;
			const int nEscapement = context == PRINTER ? -900 : 0; 
			const int nFontSize = max (params.GetSize (), 6);
			const int nFontSizeSmall = max ((int)((double)nFontSize * 0.75), 6);
			const int nFontWidth = params.GetFontWidth ();
			const bool bBold = (params.GetStyle () & kCapStyleBold) ? true : false;
			const bool bItalic = (params.GetStyle () & kCapStyleItalic) ? true : false;
			const int x = nFontSize + ((context == PRINTER) ? 1 : 0);
			int y = 0;
			int cx = nFontSize, cy = nWidth;
			CRect rc (0, 0, 0, 0);

			if (context == EDITOR)
				swap (cx, cy);

			ASSERT (pDC);

			fntFull.CreateFont (nFontSize, nFontWidth, nEscapement, nEscapement,
				bBold ? FW_BOLD : FW_NORMAL, 
				bItalic ? TRUE : FALSE, 0, 0,
				DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
				DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, params.GetFont ()); 
			fntHalf.CreateFont (nFontSizeSmall, nFontWidth, nEscapement, nEscapement,
				bBold ? FW_BOLD : FW_NORMAL, 
				bItalic ? TRUE : FALSE, 0, 0,
				DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
				DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, params.GetFont ()); 
			
			VERIFY (dcMem.CreateCompatibleDC (pDC));
			VERIFY (bmp.CreateCompatibleBitmap (pDC, cx, cy));

			CBitmap * pBmp			= dcMem.SelectObject (&bmp);
			CFont * pFont			= dcMem.SelectObject (&fntFull);
			COLORREF rgbTextColor	= dcMem.SetTextColor (rgbBlack);
			COLORREF rgbBkColor		= dcMem.SetBkColor (rgbWhite);
			int nBkMode				= dcMem.SetBkMode (OPAQUE);

			::BitBlt (dcMem, 0, 0, cx, cy, NULL, 0, 0, WHITENESS);

			dcMem.DrawText (strLeft, &rc, DT_CALCRECT);
			y = nLeftCaption [0] + (((nLeftCaption [1] - nLeftCaption [0]) - rc.Width ()) / 2);
			if (context == PRINTER) dcMem.TextOut (x, y, strLeft);
			else					dcMem.DrawText (strLeft, rc + CPoint (y, 0), 0);

			dcMem.DrawText (strRight, &rc, DT_CALCRECT);
			y = nRightCaption [0] + (((nRightCaption [1] - nRightCaption [0]) - rc.Width ()) / 2);
			if (context == PRINTER) dcMem.TextOut (x, y, strRight);
			else					dcMem.DrawText (strRight, rc + CPoint (y, 0), 0);

			{
				CFont * pOld = dcMem.SelectObject (&fntHalf);
				dcMem.DrawText (strNumberSys, &rc, DT_CALCRECT);
				y = 2;
				if (context == PRINTER) dcMem.TextOut (x - (nFontSizeSmall / 2) + 2, y, strNumberSys);
				else					dcMem.TextOut (y, x - nFontSizeSmall, strNumberSys);
				dcMem.SelectObject (pOld);
			}

			dcMem.SetTextColor (rgbTextColor);
			dcMem.SetBkColor (rgbBkColor);
			dcMem.SetBkMode (nBkMode);

			dcMem.SelectObject (pFont);
			dcMem.SelectObject (pBmp);
		}

		if (pstrFormattedCaption)
			* pstrFormattedCaption = 
				strNumberSys	+ _T (" ") + 
				strLeft			+ _T (" ") +
				strRight;

	}

	return nWidth;
}

////////////////////////////////////////////////////////////////////////////////

int CBarcodeElement::DrawUPCA (const CString & strEncode, const CBarcodeParams & params, LPBYTE pBuffer, 
							   UINT nBufferWidth, ULONG nBufferSize, CDC * pDC, CBitmap * pbmpCaption,
							   FoxjetCommon::DRAWCONTEXT context,
							   CString * pstrFormattedCaption)
{
	using namespace UPCA;
	CString str (strEncode);

	int nWidth = 0;

	if (UPCA::IsValidData (str, params)) {
		if (!params.GetCheckSum ())
			str.Delete (str.GetLength () - 1);

		const CString strCaptionRight	= str.Right (5);
		const CString strCaptionLeft	= str.Mid (1, 5); //str.Left (5);
		const CString strNumberSys		= str.Mid (0, 1); //_T ("0");
		const CString strLeft			= strNumberSys + strCaptionLeft;
		const CString strCheckDigit		= !params.GetCheckSum () ? strEncode.Right (1) : UPCA::CalcCheckDigit (strNumberSys + strCaptionLeft + strCaptionRight);
		const CString strRight			= strCaptionRight + strCheckDigit;
		const int nMargin				= params.GetSpacePixels (1) * 9;
		const int nGuard				= (params.GetBarPixels (1) * 2) + params.GetSpacePixels (1);
		const int nCenter				= (params.GetBarPixels (1) * 2) + (params.GetSpacePixels (1) * 3);
		const int nBarHeight			= params.GetBarHeight () - (params.GetSize () / 2);
		const int nDataBarHeight		= params.GetBarHeight () - params.GetSize () - 1;
		int nRightCaption [2]			= { 0 };
		int nLeftCaption [2]			= { 0 };
		const BYTE nFullBarMask = GetBarMask (nBarHeight);
		const BYTE nDataBarMask = GetBarMask (nDataBarHeight);

		//TRACEF (_T ("params.GetSize (): ") + FoxjetDatabase::ToString ((int)params.GetSize ()));
		//TRACEF (_T ("nBarHeight: ") + FoxjetDatabase::ToString ((int)nBarHeight));
		//TRACEF (_T ("nDataBarHeight: ") + FoxjetDatabase::ToString ((int)nDataBarHeight));

		nWidth += nMargin;

		for (int i = 0; i < (sizeof (nEncGuard) / sizeof (nEncGuard [0])); i++) {
			int n = nEncGuard [i];
			bool bBar = n == 1;
			int cx;

			if (bBar)
				cx = params.GetBarPixels (1);
			else
				cx = params.GetSpacePixels (1);
 
			if (pBuffer) {
				for (int nCol = 0; nCol < cx; nCol++) { 
					int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
					int nBytes = nBarHeight / 8;
	
					memset (&pBuffer [nIndex], bBar ? 0 : ~0, nBytes); 

					if (bBar) 
						memcpy (&pBuffer [nIndex + nBytes], &nFullBarMask, 1); 
				}
			}

			nWidth += cx;
		}

		for (int i = 0; i < strLeft.GetLength (); i++) {
			int nDigit = _ttoi (strLeft.Mid (i, 1));

			for (int nBar = 0; nBar < 4; nBar++) {
				int n = nEncLeft [nDigit][nBar];
				bool bEven = (nBar % 2) == 0 ? true : false; 
				int cx;

				if (bEven)
					cx = params.GetSpacePixels (n);
				else
					cx = params.GetBarPixels (n);

				if (pBuffer) {
					const int nRowHeight = i == 0 ? nBarHeight : nDataBarHeight;
					const BYTE nMask = i == 0 ? nFullBarMask : nDataBarMask;

					if (i == 0)
						nLeftCaption [0] = nWidth;
					else if (i == 5)
						nLeftCaption [1] = nWidth;

					for (int nCol = 0; nCol < cx; nCol++) { 
						int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
						int nBytes = nRowHeight / 8;

						memset (&pBuffer [nIndex], bEven ? ~0 : 0, nBytes); 

						if (!bEven) 
							memcpy (&pBuffer [nIndex + nBytes], &nMask, 1); 
					}
				}

				nWidth += cx;
			}
		}

		for (int i = 0; i < (sizeof (nEncCenter) / sizeof (nEncCenter [0])); i++) {
			int n = nEncCenter [i];
			bool bBar = n == 1;
			int cx;

			if (bBar)
				cx = params.GetBarPixels (1);
			else
				cx = params.GetSpacePixels (1);

			if (pBuffer) {
				for (int nCol = 0; nCol < cx; nCol++) { 
					int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
					int nBytes = nBarHeight / 8;

					memset (&pBuffer [nIndex], bBar ? 0 : ~0, nBytes); 

					if (bBar) 
						memcpy (&pBuffer [nIndex + nBytes], &nFullBarMask, 1); 
				}
			}

			nWidth += cx;
		}

		for (int i = 0; i < strRight.GetLength (); i++) {
			int nDigit = _ttoi (strRight.Mid (i, 1));

			for (int nBar = 0; nBar < 4; nBar++) {
				int n = nEncRight [nDigit][nBar];
				bool bEven = (nBar % 2) == 0 ? true : false; 
				int cx;

				if (bEven)
					cx = params.GetBarPixels (n);
				else
					cx = params.GetSpacePixels (n);

				if (pBuffer) {
					const int nRowHeight = i == 5 ? nBarHeight : nDataBarHeight;
					const BYTE nMask = i == 5 ? nFullBarMask : nDataBarMask;

					if (i == 0)
						nRightCaption [0] = nWidth;
					else if (i == 4)
						nRightCaption [1] = nWidth;

					for (int nCol = 0; nCol < cx; nCol++) { 
						int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
						int nBytes = nRowHeight / 8;

						memset (&pBuffer [nIndex], bEven ? 0 : ~0, nBytes); 

						if (bEven) 
							memcpy (&pBuffer [nIndex + nBytes], &nMask, 1); 
					}
				}

				nWidth += cx;
			}
		}

		for (int i = 0; i < (sizeof (nEncGuard) / sizeof (nEncGuard [0])); i++) {
			int n = nEncGuard [i];
			bool bBar = n == 1;
			int cx;

			if (bBar)
				cx = params.GetBarPixels (1);
			else
				cx = params.GetSpacePixels (1);

			if (pBuffer) {
				for (int nCol = 0; nCol < cx; nCol++) { 
					int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
					int nBytes = nBarHeight / 8;

					memset (&pBuffer [nIndex], bBar ? 0 : ~0, nBytes); 

					if (bBar) 
						memcpy (&pBuffer [nIndex + nBytes], &nFullBarMask, 1); 
				}
			}

			nWidth += cx;
		}

		nWidth += nMargin;

		if (pBuffer && pbmpCaption) {
			CFont fntFull, fntHalf;
			CDC dcMem;
			CBitmap & bmp = * pbmpCaption;
			const int nEscapement = context == PRINTER ? -900 : 0; 
			const int nFontSize = max (params.GetSize (), 6);
			const int nFontSizeSmall = max ((int)((double)nFontSize * 0.75), 6);
			const int nFontWidth = params.GetFontWidth ();
			const bool bBold = (params.GetStyle () & kCapStyleBold) ? true : false;
			const bool bItalic = (params.GetStyle () & kCapStyleItalic) ? true : false;
			const int x = nFontSize + ((context == PRINTER) ? 1 : 0);
			int y = 0;
			int cx = nFontSize, cy = nWidth;
			CRect rc (0, 0, 0, 0);

			if (context == EDITOR)
				swap (cx, cy);

			ASSERT (pDC);

			fntFull.CreateFont (nFontSize, nFontWidth, nEscapement, nEscapement,
				bBold ? FW_BOLD : FW_NORMAL, 
				bItalic ? TRUE : FALSE, 0, 0,
				DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
				DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, params.GetFont ()); 
			fntHalf.CreateFont (nFontSizeSmall, nFontWidth, nEscapement, nEscapement,
				bBold ? FW_BOLD : FW_NORMAL, 
				bItalic ? TRUE : FALSE, 0, 0,
				DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
				DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, params.GetFont ()); 

			VERIFY (dcMem.CreateCompatibleDC (pDC));
			VERIFY (bmp.CreateCompatibleBitmap (pDC, cx, cy));

			CBitmap * pBmp			= dcMem.SelectObject (&bmp);
			CFont * pFont			= dcMem.SelectObject (&fntFull);
			COLORREF rgbTextColor	= dcMem.SetTextColor (rgbBlack);
			COLORREF rgbBkColor		= dcMem.SetBkColor (rgbWhite);
			int nBkMode				= dcMem.SetBkMode (OPAQUE);

			::BitBlt (dcMem, 0, 0, cx, cy, NULL, 0, 0, WHITENESS);

			dcMem.DrawText (strCaptionLeft, &rc, DT_CALCRECT);
			y = nLeftCaption [0] + (((nLeftCaption [1] - nLeftCaption [0]) - rc.Width ()) / 2);
			if (context == PRINTER) dcMem.TextOut (x, y, strCaptionLeft);
			else					dcMem.DrawText (strCaptionLeft, rc + CPoint (y, 0), 0);

			dcMem.DrawText (strCaptionRight, &rc, DT_CALCRECT);
			y = nRightCaption [0] + (((nRightCaption [1] - nRightCaption [0]) - rc.Width ()) / 2);
			if (context == PRINTER) dcMem.TextOut (x, y, strCaptionRight);
			else					dcMem.DrawText (strCaptionRight, rc + CPoint (y, 0), 0);

			dcMem.SelectObject (&fntHalf);

			{
				int nXPrinter = x - (nFontSizeSmall / 2) + 1;

				nXPrinter = max (nFontSizeSmall + 1, nXPrinter);

				dcMem.DrawText (strNumberSys, &rc, DT_CALCRECT);
				y = 2;
				if (context == PRINTER) dcMem.TextOut (nXPrinter, y, strNumberSys);
				else					dcMem.TextOut (y, x - nFontSizeSmall, strNumberSys);

				dcMem.DrawText (strCheckDigit, &rc, DT_CALCRECT);
				y = nWidth - rc.Width () - 2;
				if (context == PRINTER) dcMem.TextOut (nXPrinter, y, strCheckDigit);
				else					dcMem.TextOut (y, x - nFontSizeSmall, strCheckDigit);
			}

			dcMem.SetTextColor (rgbTextColor);
			dcMem.SetBkColor (rgbBkColor);
			dcMem.SetBkMode (nBkMode);

			dcMem.SelectObject (pFont);
			dcMem.SelectObject (pBmp);
		}

		if (pstrFormattedCaption)
			* pstrFormattedCaption = 
				strNumberSys	+ _T (" ") + 
				strCaptionLeft	+ _T (" ") +
				strCaptionRight	+ _T (" ") + 
				strCheckDigit;
	}

	return nWidth;
}

/////////////////////////////////

int CBarcodeElement::DrawUPCE (const CString & str, const CBarcodeParams & params, LPBYTE pBuffer, 
							   UINT nBufferWidth, ULONG nBufferSize, CDC * pDC, CBitmap * pbmpCaption,
							   FoxjetCommon::DRAWCONTEXT context,
							   CString * pstrFormattedCaption)
{

	using namespace UPCE;

	int nWidth = 0;

	if (UPCE::IsValidData (str, params)) {
		const CString strNumberSys		= str.Mid (0, 1); 
		const CString strCheckDigit		= UPCE::CalcCheckDigit (str, params);
		const int nCheckDigit			= _ttoi (strCheckDigit);
		const int nMargin				= params.GetSpacePixels (1) * 9;
		const int nGuard				= (params.GetBarPixels (1) * 2) + params.GetSpacePixels (1);
		const int nCenter				= (params.GetBarPixels (1) * 2) + (params.GetSpacePixels (1) * 3);
		const int nBarHeight			= params.GetBarHeight () - (params.GetSize () / 2);
		const int nDataBarHeight		= params.GetBarHeight () - params.GetSize () - 1;
		const BYTE nFullBarMask = GetBarMask (nBarHeight);
		const BYTE nDataBarMask = GetBarMask (nDataBarHeight);

		nWidth += nMargin;

		for (int i = 0; i < ARRAYSIZE (UPCA::nEncGuard); i++) {
			int n = UPCA::nEncGuard [i];
			bool bBar = n == 1;
			int cx;

			if (bBar)
				cx = params.GetBarPixels (1);
			else
				cx = params.GetSpacePixels (1);
 
			if (pBuffer) {
				for (int nCol = 0; nCol < cx; nCol++) { 
					int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
					int nBytes = nBarHeight / 8;
	
					memset (&pBuffer [nIndex], bBar ? 0 : ~0, nBytes); 

					if (bBar) 
						memcpy (&pBuffer [nIndex + nBytes], &nFullBarMask, 1); 
				}
			}

			nWidth += cx;
		}

		const BYTE * pParity = (str [0] == '0') ? &UPCE::n0 [nCheckDigit][0] : &UPCE::n1 [nCheckDigit][0];

		for (int i = 1; i < str.GetLength (); i++) {
			int nDigit = _ttoi (str.Mid (i, 1));

			for (int nBar = 0; nBar < 4; nBar++) {
				int n = pParity [i - 1] == E ? nEncEven [nDigit][nBar] : nEncOdd [nDigit][nBar];
				bool bEven = (nBar % 2) == 0 ? true : false; 
				int cx;

				if (bEven)
					cx = params.GetSpacePixels (n);
				else
					cx = params.GetBarPixels (n);

				if (pBuffer) {
					const int nRowHeight = nDataBarHeight;
					const BYTE nMask = nDataBarMask;

					for (int nCol = 0; nCol < cx; nCol++) { 
						int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
						int nBytes = nRowHeight / 8;

						memset (&pBuffer [nIndex], bEven ? ~0 : 0, nBytes); 

						if (!bEven) 
							memcpy (&pBuffer [nIndex + nBytes], &nMask, 1); 
					}
				}

				nWidth += cx;
			}
		}

		for (int i = 0; i < ARRAYSIZE (UPCE::nEncRight); i++) {
			int n = UPCE::nEncRight [i];
			bool bBar = n == 1;
			int cx;

			if (bBar)
				cx = params.GetBarPixels (1);
			else
				cx = params.GetSpacePixels (1);

			if (pBuffer) {
				for (int nCol = 0; nCol < cx; nCol++) { 
					int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
					int nBytes = nBarHeight / 8;

					memset (&pBuffer [nIndex], bBar ? 0 : ~0, nBytes); 

					if (bBar) 
						memcpy (&pBuffer [nIndex + nBytes], &nFullBarMask, 1); 
				}
			}

			nWidth += cx;
		}

		nWidth += nMargin;

		if (pBuffer && pbmpCaption) {
			CFont fntFull, fntHalf;
			CDC dcMem;
			CBitmap & bmp = * pbmpCaption;
			const CString strCaption = str.Mid (1);
			const int nCaption [2] = { 0, nWidth };
			const int nEscapement = context == PRINTER ? -900 : 0; 
			const int nFontSize = max (params.GetSize (), 6);
			const int nFontSizeSmall = max ((int)((double)nFontSize * 0.75), 6);
			const int nFontWidth = params.GetFontWidth ();
			const bool bBold = (params.GetStyle () & kCapStyleBold) ? true : false;
			const bool bItalic = (params.GetStyle () & kCapStyleItalic) ? true : false;
			const int x = nFontSize + ((context == PRINTER) ? 1 : 0);
			int y = 0;
			int cx = nFontSize, cy = nWidth;
			CRect rc (0, 0, 0, 0);

			if (context == EDITOR)
				swap (cx, cy);

			ASSERT (pDC);

			fntFull.CreateFont (nFontSize, nFontWidth, nEscapement, nEscapement,
				bBold ? FW_BOLD : FW_NORMAL, 
				bItalic ? TRUE : FALSE, 0, 0,
				DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
				DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, params.GetFont ()); 
			fntHalf.CreateFont (nFontSizeSmall, nFontWidth, nEscapement, nEscapement,
				bBold ? FW_BOLD : FW_NORMAL, 
				bItalic ? TRUE : FALSE, 0, 0,
				DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
				DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, params.GetFont ()); 
			
			VERIFY (dcMem.CreateCompatibleDC (pDC));
			VERIFY (bmp.CreateCompatibleBitmap (pDC, cx, cy));

			CBitmap * pBmp			= dcMem.SelectObject (&bmp);
			CFont * pFont			= dcMem.SelectObject (&fntFull);
			COLORREF rgbTextColor	= dcMem.SetTextColor (rgbBlack);
			COLORREF rgbBkColor		= dcMem.SetBkColor (rgbWhite);
			int nBkMode				= dcMem.SetBkMode (OPAQUE);

			::BitBlt (dcMem, 0, 0, cx, cy, NULL, 0, 0, WHITENESS);

			dcMem.DrawText (str, &rc, DT_CALCRECT);
			y = nCaption [0] + (((nCaption [1] - nCaption [0]) - rc.Width ()) / 2);
			if (context == PRINTER) dcMem.TextOut (x, y, strCaption);
			else					dcMem.DrawText (strCaption, rc + CPoint (y, 0), 0);

			{
				CFont * pOld = dcMem.SelectObject (&fntHalf);
			
				dcMem.DrawText (strNumberSys, &rc, DT_CALCRECT);
				y = 2;
				if (context == PRINTER) dcMem.TextOut (x - (nFontSizeSmall / 2) + 2, y, strNumberSys);
				else					dcMem.TextOut (y, x - nFontSizeSmall, strNumberSys);

				dcMem.DrawText (strCheckDigit, &rc, DT_CALCRECT);
				y = nWidth - rc.Width () - 2;
				if (context == PRINTER) dcMem.TextOut (x - (nFontSizeSmall / 2) + 2, y, strCheckDigit);
				else					dcMem.TextOut (y, x - nFontSizeSmall, strCheckDigit);
				
				dcMem.SelectObject (pOld);
			}

			dcMem.SetTextColor (rgbTextColor);
			dcMem.SetBkColor (rgbBkColor);
			dcMem.SetBkMode (nBkMode);

			dcMem.SelectObject (pFont);
			dcMem.SelectObject (pBmp);
		}

		if (pstrFormattedCaption && str.GetLength () > 1)
			* pstrFormattedCaption = strNumberSys + _T (" ") + str.Mid (1) + _T (" ") + strCheckDigit;
	}

	return nWidth;
}

/////////////////////////////////

int CBarcodeElement::DrawI2o5Symbol (const CString & str, const CBarcodeParams & params, 
									 const FoxjetDatabase::HEADSTRUCT & head, LPBYTE pBuffer, 
									 UINT nBufferWidth, ULONG lBufferSize, 
									 CDC * pDC, CBitmap * pbmpCaption,
									 FoxjetCommon::DRAWCONTEXT context, 
									 CString * pstrFormattedCaption)
{
	using namespace I2o5;

	int nWidth = 0;
	CString strData (str);
	CArray <BARSTRUCT, BARSTRUCT &> v;
	int nQuietzone = CBaseElement::ThousandthsToLogical (CPoint (params.GetQuietZone (), 0), head).x;
	CSize bearer = CBaseElement::ThousandthsToLogical (CPoint (params.GetVertBearer (), params.GetHorzBearer ()), head);
	double dStretch [2] = { 0, 0 };
	CString strCaption (strData);

	if (strData.GetLength () == 14) 
		strCaption = 
			strData.Mid (0, 1) + _T (" ") +
			strData.Mid (1, 2) + _T (" ") +
			strData.Mid (3, 5) + _T (" ") +
			strData.Mid (8, 5) + _T (" ") +
			strData.Mid (13, 1);  

	CBaseElement::CalcStretch (head, dStretch);
	bearer.cy = (int)((double)bearer.cy / dStretch [1]);

	nQuietzone = StretchToPrinterRes (nQuietzone, head);
	bearer.cx = StretchToPrinterRes (bearer.cx, head);

	if (I2o5::IsValidData (strData, params)) {
		const int nBarHeight		= params.GetOrientation () == NORMAL ? (min ((int)head.GetChannels (), params.GetBarHeight ())) : params.GetBarHeight ();
		const BYTE nBarMask			= GetBarMask (nBarHeight);
		const BYTE nNotBarMask		= ~nBarMask;
		const int nNarrowBar		= params.GetBarPixels (1);
		const int nWideBar			= params.GetBarPixels (2);
		const int nNarrowSpace		= params.GetSpacePixels (1);
		const int nWideSpace		= params.GetSpacePixels (2);

		// left guard pattern: 1010
		v.Add (BARSTRUCT (BLACK, NARROW));
		v.Add (BARSTRUCT (WHITE, NARROW));
		v.Add (BARSTRUCT (BLACK, NARROW));
		v.Add (BARSTRUCT (WHITE, NARROW));

		for (int i = 0; i < strData.GetLength (); i += 2) {
			const int nBarDigit		= _ttoi (strData.Mid (i, 1));
			const int nSpaceDigit	= _ttoi (strData.Mid (i + 1, 1));
			
			for (int nIndex = 0; nIndex < 5; nIndex++) {
				const bool bWideBar		= nEncode [nBarDigit][nIndex]	? true : false;
				const bool bWideSpace	= nEncode [nSpaceDigit][nIndex] ? true : false;

				v.Add (BARSTRUCT (BLACK, bWideBar ? WIDE : NARROW));
				v.Add (BARSTRUCT (WHITE, bWideSpace ? WIDE : NARROW));
			}
		}

		// right guard pattern: 1101
		v.Add (BARSTRUCT (BLACK, WIDE));
		v.Add (BARSTRUCT (WHITE, NARROW));
		v.Add (BARSTRUCT (BLACK, NARROW));
		//v.Add (BARSTRUCT (WHITE, NARROW));

		nWidth += nQuietzone + bearer.cx; 

		for (int i = 0; i < v.GetSize (); i++) {
			BARSTRUCT b = v [i];
			int cx = 0;

			if (b.m_color == BLACK)
				cx = b.m_width == WIDE ? nWideBar : nNarrowBar;
			else
				cx = b.m_width == WIDE ? nWideSpace : nNarrowSpace;

			for (int nCol = 0; nCol < cx; nCol++) { 
				int nIndex = (nBufferWidth / 8) * (nCol + nWidth);
				int nBytes = nBarHeight / 8;

				if (pBuffer) {
					if (b.m_color == BLACK) {
						memset (&pBuffer [nIndex], 0, nBytes); 

						if ((ULONG)(nIndex + nBytes) < lBufferSize)
							memcpy (&pBuffer [nIndex + nBytes], &nBarMask, 1); 
					}
				}
			}

			nWidth += cx;
		}

		nWidth += nQuietzone + bearer.cx; 

		if (pbmpCaption) {
			CFont fntFull;
			CDC dcMem;
			CBitmap & bmp = * pbmpCaption;
			const int nEscapement = 0;
			const int nFontSize = max (params.GetSize (), 6);
			const int nFontWidth = params.GetFontWidth ();
			const bool bBold = (params.GetStyle () & kCapStyleBold) ? true : false;
			const bool bItalic = (params.GetStyle () & kCapStyleItalic) ? true : false;
			int nFormat = 0;
			CRect rc (0, 0, 0, 0);

			ASSERT (pDC);

			fntFull.CreateFont (nFontSize, nFontWidth, nEscapement, nEscapement,
				bBold ? FW_BOLD : FW_NORMAL, 
				bItalic ? TRUE : FALSE, 0, 0,
				DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
				DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, params.GetFont ()); 
			
			VERIFY (dcMem.CreateCompatibleDC (pDC));
			VERIFY (bmp.CreateCompatibleBitmap (pDC, nWidth, nFontSize));
			
			CBitmap * pBmp			= dcMem.SelectObject (&bmp);
			CFont * pFont			= dcMem.SelectObject (&fntFull);
			COLORREF rgbTextColor	= dcMem.SetTextColor (rgbBlack);
			COLORREF rgbBkColor		= dcMem.SetBkColor (rgbWhite);
			int nBkMode				= dcMem.SetBkMode (OPAQUE);

			::BitBlt (dcMem, 0, 0, nWidth, nFontSize, NULL, 0, 0, WHITENESS);

			dcMem.DrawText (strCaption, &rc, DT_CALCRECT);

			switch (params.GetAlignment ()) {
				case kCapAlignBelowLeft:
				case kCapAlignAboveLeft:
					nFormat |= DT_LEFT;
					break;
				case kCapAlignBelowCenter:
				case kCapAlignAboveCenter:
					nFormat |= DT_CENTER;
					break;
				case kCapAlignBelowRight:
				case kCapAlignAboveRight:
					nFormat |= DT_RIGHT;
					break;
			}

			dcMem.DrawText (strCaption, CRect (0, 0, nWidth, nFontSize), nFormat);

			if (context == PRINTER) {
				CxImage img;

				img.CreateFromHBITMAP (bmp);
				img.RotateRight ();
				::DeleteObject (bmp.Detach ());
				bmp.Attach (img.MakeBitmap ());
			}

			dcMem.SetTextColor (rgbTextColor);
			dcMem.SetBkColor (rgbBkColor);
			dcMem.SetBkMode (nBkMode);

			dcMem.SelectObject (pFont);
			dcMem.SelectObject (pBmp);
		}
	}

	if (pstrFormattedCaption)
		* pstrFormattedCaption = strCaption;

	return nWidth;
}

CSize CBarcodeElement::DrawI2o5 (CDC &dc, const FoxjetDatabase::HEADSTRUCT & head, CSize & sizeLogical,
								 FoxjetCommon::DRAWCONTEXT context, bool bThrowException,
								 const CBarcodeParams & params)
{
	CString str = GetImageData ();
	CSize bearer = CBaseElement::ThousandthsToLogical (CPoint (params.GetVertBearer (), params.GetHorzBearer ()), head);
	int nHeight = params.GetBarHeight ();
	int nWidth = 0;
	double dStretch [2] = { 0, 0 };

	if (GetOrientation () == NORMAL)
		nHeight = min ((int)head.GetChannels (), params.GetBarHeight ());

	CBaseElement::CalcStretch (head, dStretch);
	bearer.cy = (int)((double)bearer.cy / dStretch [1]);

	if (params.GetCheckSum ())
		str += I2o5::CalcCheckDigit (str);

	int nLen = str.GetLength (); 

	if (params.GetSymbology () != kSymbologyGTIN14)
		if (nLen % 2)
			str = _T ("0") + str;
	
	if (!I2o5::IsValidData (str, params)) {
		if (bThrowException)
			throw new CBarcodeException (LoadString (params.GetSymbology () == kSymbologyInter2of5 ? IDS_INVALIDI2O5DATA : IDS_INVALIDGTIN14DATA));
		else {
			str = FoxjetElements::GetDefaultData (params.GetSymbology () /* kSymbologyInter2of5 */, params);
			ASSERT (I2o5::IsValidData (str, params));
		}
	}

	nWidth = DrawI2o5Symbol (str, params, head, NULL, 0, 0, NULL, NULL, context, &m_caption.m_strText);

//{ CString str; str.Format ("nWidth: %d [%s]", nWidth, context == PRINTER ? "PRINTER" : "EDITOR"); TRACEF (str); } // TODO: rem

	const LONG lRowLen = (int)ceil ((long double)nHeight / 16.0) * 16; // win bitmap is word aligned
	const int nBufferSize = lRowLen * nWidth / 8;
	LPBYTE pBuffer = new BYTE [nBufferSize];
	int x = 0;
	CBitmap bmp, bmpCaption;
	DIBSECTION ds;
	CDC dcMem;

	dcMem.CreateCompatibleDC (&dc);
	memset (pBuffer, ~0, nBufferSize);

	DrawI2o5Symbol (str, params, head, pBuffer, lRowLen, nBufferSize, &dc, &bmpCaption, context, &m_caption.m_strText);
	
	::ZeroMemory (&ds, sizeof (ds));
	VERIFY (bmpCaption.GetObject (sizeof (ds), &ds));

	if (context == EDITOR) {
		CBitmap bmpPrinter, bmpEditor;
 		CDC dcCaption, dcEditor;

//		bearer = CBaseElement::ThousandthsToLogical (CPoint (params.GetVertBearer (), params.GetHorzBearer ()), head);

		VERIFY (dcCaption.CreateCompatibleDC (&dc));
		VERIFY (dcEditor.CreateCompatibleDC (&dc));

		VERIFY (bmpPrinter.CreateBitmap (nHeight, nWidth, 1, 1, pBuffer));
		VERIFY (bmp.CreateCompatibleBitmap (&dc, nWidth, nHeight));
		
		RotateLeft90AndFlip (dcMem, bmpPrinter, bmpEditor);

		CBitmap * pCaption = dcCaption.SelectObject (&bmpCaption);
		CBitmap * pMem = dcMem.SelectObject (&bmp);
		CBitmap * pEditor = dcEditor.SelectObject (&bmpEditor);

		VERIFY (::BitBlt (dcMem, 0, 0, nWidth, nHeight, NULL, 0, 0, WHITENESS));
		VERIFY (dcMem.BitBlt (0, 0, nWidth, nHeight, &dcEditor, 0, 0, SRCINVERT));

		if (params.GetAlignment () != kCapAlignOff) {
			int x = 0;
			int y = nHeight - ds.dsBm.bmHeight;
			int cx = ds.dsBm.bmWidth;
			int cy = ds.dsBm.bmHeight;
			CDC dcInvert;
			CBitmap bmpInvert;

			VERIFY (dcInvert.CreateCompatibleDC (&dcMem));
			VERIFY (bmpInvert.CreateCompatibleBitmap (&dcMem, cx, cy));
			CBitmap * pInvert = dcInvert.SelectObject (&bmpInvert);

			switch (params.GetAlignment ()) {
				case kCapAlignAboveLeft:	
				case kCapAlignAboveCenter:
				case kCapAlignAboveRight:	
					y = 0; 
//					::BitBlt (dcMem, x, cy, cx, 1, NULL, 0, 0, WHITENESS);
					::BitBlt (dcMem, x, cy, cx, 1, NULL, 0, 0, BLACKNESS);
					break;
				default:
					// TODO: this is causing the bottom bearer to be too tall
					// invert caption first?

//					::BitBlt (dcMem, x, y - 1, cx, 1, NULL, 0, 0, WHITENESS);
					::BitBlt (dcMem, x, y - 1, cx, 1, NULL, 0, 0, BLACKNESS);
					break;
			}

			// flip h
//			::BitBlt (dcMem, x, y, cx, cy, NULL, 0, 0, WHITENESS);
			::BitBlt (dcMem, x, y, cx, cy, NULL, 0, 0, BLACKNESS);
			
			// TODO: rem: VERIFY (dcMem.BitBlt (x, y, cx, cy, &dcCaption, 0, 0, SRCINVERT));

			VERIFY (::BitBlt (dcInvert, 0, 0, cx, cy, NULL, 0, 0, WHITENESS));
			VERIFY (dcInvert.BitBlt (0, 0, cx, cy, &dcCaption, 0, 0, SRCINVERT));
			VERIFY (dcMem.BitBlt (x, y, cx, cy, &dcInvert, 0, 0, SRCCOPY));

			dcInvert.SelectObject (pInvert);
		}

		dcMem.SelectObject (pMem);
		dcCaption.SelectObject (pCaption);
		dcEditor.SelectObject (pEditor);
	}
	else {
		CDC dcMem;
		CDC dcCaption;
		CBitmap bmpPrinter;

//		bearer = CBaseElement::ThousandthsToLogical (CPoint (params.GetHorzBearer (), params.GetVertBearer ()), head);
		swap (bearer.cx, bearer.cy);

		VERIFY (dcMem.CreateCompatibleDC (&dc));
		VERIFY (dcCaption.CreateCompatibleDC (&dc));

		swap (nWidth, nHeight);
		VERIFY (bmpPrinter.CreateBitmap (nWidth, nHeight, 1, 1, pBuffer));
		VERIFY (bmp.CreateCompatibleBitmap (&dc, nWidth, nHeight)); 

		CBitmap * pMem = dcMem.SelectObject (&bmp);
		CBitmap * pCaption = dcCaption.SelectObject (&bmpCaption);

		{
			CBitmap * pOld = dcCaption.SelectObject (&bmpPrinter);
			::BitBlt (dcMem, 0, 0, nWidth, nHeight, NULL, 0, 0, WHITENESS); 
			dcMem.BitBlt (0, 0, nWidth, nHeight, &dcCaption, 0, 0, SRCINVERT);
			dcCaption.SelectObject (pOld);
		}

		if (params.GetAlignment () != kCapAlignOff) {
			int x = 0, y = 0;
			int cx = ds.dsBm.bmWidth;
			int cy = ds.dsBm.bmHeight;

			switch (params.GetAlignment ()) {
				case kCapAlignBelowLeft:	
				case kCapAlignBelowCenter:		
				case kCapAlignBelowRight:	
					x = nWidth - ds.dsBm.bmWidth;
					::BitBlt (dcMem, x - 1, y, 1, cy, NULL, 0, 0, BLACKNESS);
					break;
				default:
					::BitBlt (dcMem, cx, y, 1, cy, NULL, 0, 0, BLACKNESS);
					break;
			}

			::BitBlt (dcMem, x, y, cx, cy, NULL, 0, 0, WHITENESS);

			x += (cx - 1);
			cx *= -1;

			dcMem.StretchBlt (x, y, cx, cy, &dcCaption, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, SRCINVERT);
		}

		dcMem.SelectObject (pMem);
		dcCaption.SelectObject (pCaption);
	}

	delete [] pBuffer;

	CBitmap * pMem = dcMem.SelectObject (&bmp);
	dc.BitBlt (0, 0, nWidth, nHeight, &dcMem, 0, 0, !IsInverse () ? SRCINVERT : SRCCOPY);
	dcMem.SelectObject (pMem);

	DrawBearer (dc, bearer, context, params, nWidth, nHeight, head);

	CCoord c ((double)nWidth, (double)nHeight, INCHES, head);

	CSize size = m_size = CSize ((int)ceil (c.m_x * 1000.0), (int)ceil (c.m_y * 1000.0));

//	if (context == PRINTER)
//		swap (nWidth, nHeight);

	sizeLogical = CSize (nWidth, nHeight);

	return size;
}

CSize CBarcodeElement::DrawUPCEAN (CDC &dc, int nType, const FoxjetDatabase::HEADSTRUCT & head, CSize & sizeLogical,
								 FoxjetCommon::DRAWCONTEXT context, bool bThrowException,
								 const CBarcodeParams & params)
{
	CString str = GetImageData ();
	int nHeight = min ((int)head.GetChannels (), params.GetBarHeight ()); //params.GetBarHeight ();
	int nWidth = 0;

	switch (nType) {
	case kSymbologyUPCA: 
		{
			const int nLen = !params.GetCheckSum () ? 12 : 11;

			if (str.GetLength () >= nLen)
				str = str.Left (nLen);

			if (!UPCA::IsValidData (str, params)) {
				if (bThrowException)
					throw new CBarcodeException (LoadString (IDS_INVALIDUPCDATA));
				else
					str = FoxjetElements::GetDefaultData (nType, params);
			}

			nWidth = DrawUPCA (str, params, NULL, 0, 0, NULL, NULL, PRINTER, &m_caption.m_strText);
		}
		break;
	case kSymbologyUPCE:
		if (!UPCE::IsValidData (str, params)) {
			if (bThrowException)
				throw new CBarcodeException (LoadString (IDS_INVALIDUPCEDATA));
			else
				str = FoxjetElements::GetDefaultData (nType, params);
		}

		nWidth = DrawUPCE (str, params, NULL, 0, 0, NULL, NULL, PRINTER, &m_caption.m_strText);
		break;
	case kSymbologyEANJAN_8:
		if (!EAN8::IsValidData (str)) {
			if (bThrowException)
				throw new CBarcodeException (LoadString (IDS_INVALIDEAN8DATA));
			else
				str = FoxjetElements::GetDefaultData (nType, params);
		}

		nWidth = DrawEAN8 (str, params, NULL, 0, 0, NULL, NULL, PRINTER, &m_caption.m_strText);
		break;
	case kSymbologyEANJAN_13:
		if (!EAN13::IsValidData (str)) {
			if (bThrowException)
				throw new CBarcodeException (LoadString (IDS_INVALIDEAN13DATA));
			else
				str = FoxjetElements::GetDefaultData (nType, params);
		}

		nWidth = DrawEAN13 (str, params, NULL, 0, 0, NULL, NULL, PRINTER, &m_caption.m_strText);
		break;
	default:
		ASSERT (0);
	}

	const LONG lRowLen = (int)ceil ((long double)nHeight / 16.0) * 16; // win bitmap is word aligned
	const int nBufferSize = lRowLen * nWidth / 8;
	LPBYTE pBuffer = new BYTE [nBufferSize];
	int x = 0;
	CBitmap bmp, bmpCaption;
	DIBSECTION ds;
	CDC dcMem;

	dcMem.CreateCompatibleDC (&dc);
	memset (pBuffer, ~0, nBufferSize);

	switch (nType) {
	case kSymbologyUPCA:
		DrawUPCA (str, params, pBuffer, lRowLen, nBufferSize, &dc, &bmpCaption, context, &m_caption.m_strText);
		break;
	case kSymbologyUPCE:
		DrawUPCE (str, params, pBuffer, lRowLen, nBufferSize, &dc, &bmpCaption, context, &m_caption.m_strText);
		break;
	case kSymbologyEANJAN_8:
		DrawEAN8 (str, params, pBuffer, lRowLen, nBufferSize, &dc, &bmpCaption, context, &m_caption.m_strText);
		break;
	case kSymbologyEANJAN_13:
		DrawEAN13 (str, params, pBuffer, lRowLen, nBufferSize, &dc, &bmpCaption, context, &m_caption.m_strText);
		break;
	default:
		ASSERT (0);
	}

	::ZeroMemory (&ds, sizeof (ds));
	VERIFY (bmpCaption.GetObject (sizeof (ds), &ds));

	if (context == EDITOR) {
		CBitmap bmpPrinter, bmpEditor;
 		CDC dcCaption, dcEditor;

		VERIFY (dcCaption.CreateCompatibleDC (&dc));
		VERIFY (dcEditor.CreateCompatibleDC (&dc));

		VERIFY (bmpPrinter.CreateBitmap (nHeight, nWidth, 1, 1, pBuffer));
		VERIFY (bmp.CreateCompatibleBitmap (&dc, nWidth, nHeight));
		
		RotateLeft90AndFlip (dcMem, bmpPrinter, bmpEditor);

		CBitmap * pCaption = dcCaption.SelectObject (&bmpCaption);
		CBitmap * pMem = dcMem.SelectObject (&bmp);
		CBitmap * pEditor = dcEditor.SelectObject (&bmpEditor);

		VERIFY (::BitBlt (dcMem, 0, 0, nWidth, nHeight, NULL, 0, 0, WHITENESS));
		VERIFY (dcMem.BitBlt (0, 0, nWidth, nHeight, &dcEditor, 0, 0, SRCCOPY));

		// flip h
		VERIFY (dcMem.BitBlt (0, nHeight - ds.dsBm.bmHeight, ds.dsBm.bmWidth, ds.dsBm.bmHeight, &dcCaption, 0, 0, SRCAND));

		dcMem.SelectObject (pMem);
		dcCaption.SelectObject (pCaption);
		dcEditor.SelectObject (pEditor);
	}
	else {
		CDC dcMem, dcCaption;
		CBitmap bmpPrinter;

		VERIFY (dcMem.CreateCompatibleDC (&dc));
		VERIFY (dcCaption.CreateCompatibleDC (&dc));

		swap (nWidth, nHeight);
		VERIFY (bmpPrinter.CreateBitmap (nWidth, nHeight, 1, 1, pBuffer));
		VERIFY (bmp.CreateCompatibleBitmap (&dc, nWidth, nHeight)); 

		CBitmap * pMem = dcMem.SelectObject (&bmp);
		CBitmap * pCaption = dcCaption.SelectObject (&bmpCaption);

		::BitBlt (dcMem, 0, 0, nWidth, nHeight, NULL, 0, 0, WHITENESS); 

		int x = nWidth - ds.dsBm.bmWidth;
		int y = 0;
		int cx = ds.dsBm.bmWidth;
		int cy = ds.dsBm.bmHeight;

		// flip h
		x += (cx - 1);
		cx *= -1;

		dcMem.StretchBlt (x, y, cx, cy, &dcCaption, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, SRCAND);
		//SAVEBITMAP (dcMem, "C:\\Temp\\Debug\\dcMem1.bmp"); // TODO: rem

		dcCaption.SelectObject (&bmpPrinter);
		dcMem.BitBlt (0, 0, nWidth, nHeight, &dcCaption, 0, 0, SRCAND);

		dcMem.SelectObject (pMem);
		dcCaption.SelectObject (pCaption);
	}

	delete [] pBuffer;

	CBitmap * pMem = dcMem.SelectObject (&bmp);
	dc.BitBlt (0, 0, nWidth, nHeight, &dcMem, 0, 0, IsInverse () ? SRCINVERT : SRCCOPY);
	dcMem.SelectObject (pMem);

	CCoord c ((double)nWidth, (double)nHeight, INCHES, head);

	CSize size = m_size = CSize ((int)ceil (c.m_x * 1000.0), (int)ceil (c.m_y * 1000.0));

	sizeLogical = CSize (nWidth, nHeight);

	return size;
}

static int xtoy (int n, const FoxjetDatabase::HEADSTRUCT & head)
{
	double dHeadHeight = CBaseElement::LogicalToThousandths (CPoint (0, head.Height ()), head).y;
	double dRes [2] = 
	{
		(double)head.GetChannels () / (dHeadHeight / 1000.0),
		head.GetRes (),
	};
	int nResult = DivideAndRound ((double)n, (dRes [0] / dRes [1]));

	return nResult;
}

static int ytox (int n, const FoxjetDatabase::HEADSTRUCT & head)
{
	double dHeadHeight = CBaseElement::LogicalToThousandths (CPoint (0, head.Height ()), head).y;
	double dRes [2] = 
	{
		(double)head.GetChannels () / (dHeadHeight / 1000.0),
		head.GetRes (),
	};
	int nResult = DivideAndRound ((double)n, (dRes [1] / dRes [0]));

	return nResult;
}

CMap <ULONG, ULONG, CBarcodeElement::CBcParamArray *, CBarcodeElement::CBcParamArray *> CBarcodeElement::m_mapParams;

void CBarcodeElement::ClearLocalParams (ULONG lLocalParamUID)
{
	CBarcodeElement::CBcParamArray * p = NULL;

	if (m_mapParams.Lookup (lLocalParamUID, p)) {
		if (p) {
			m_mapParams.RemoveKey (lLocalParamUID);
			delete p;
		}
	}
}

void CBarcodeElement::SetLocalParam (ULONG lLocalParamUID, int nSymbology, const CBarcodeParams & param)
{
	CBarcodeElement::CBcParamArray * p = NULL;

	if (!m_mapParams.Lookup (lLocalParamUID, p)) {
		p = new CBarcodeElement::CBcParamArray;
		m_mapParams.SetAt (lLocalParamUID, p);
	}

	p->SetAt (nSymbology, param);
}

CBarcodeParams CBarcodeElement::GetLocalParam (ULONG lLocalParamUID, int nSymbology)
{
	CBarcodeParams param;
	CBarcodeElement::CBcParamArray * p = NULL;

	if (m_mapParams.Lookup (lLocalParamUID, p)) 
		VERIFY (p->Lookup (nSymbology, param));

	param.SetCustom (true);

	return param;
}



CSize CBarcodeElement::DrawBarcode (CDC &dc, const FoxjetDatabase::HEADSTRUCT & head, CSize & sizeLogical,
									FoxjetCommon::DRAWCONTEXT context, bool bThrowException,
									CBarcodeParams * pParams)
{
	int nSymbology			= pParams ? pParams->GetSymbology ()	: GetSymbology ();
	ULONG lMagID			= pParams ? pParams->GetID ()			: GetMagnification ();
	CBarcodeParams p		= pParams ? (* pParams)					: GetBarcodeParam (GetLineID (), GetLocalParamUID (), nSymbology, head.m_nHeadType, head.GetRes (), lMagID, ListGlobals::GetDB ());
	int nMag				= p.GetMagPct ();
	CString strBcHrText (GetImageData ());
	CSize size (0, 0);

	SetOrientation (p.GetOrientation ());

	if (GetOrientation () == VERTICAL) {
		VERIFY (p.SetVertBearer (ytox (p.GetVertBearer (), head)));
		VERIFY (p.SetQuietZone (ytox (p.GetQuietZone (), head)));
		VERIFY (p.SetHorzBearer (xtoy (p.GetHorzBearer (), head)));
	}
	else {
		if (p.GetBarHeight () > head.GetChannels ())
			p.SetBarHeight (head.GetChannels ());
	}

	// if bc param ID was invalid for some reason, set default
	if (p.GetSymbology () != GetSymbology ()) {
		p = GetBarcodeParam (GetLineID (), GetLocalParamUID (), GetSymbology (), head.m_nHeadType, head.GetRes (), 0, ListGlobals::GetDB ());
		p.ClipTo (head.m_nHeadType);

		if (p.GetID () != -1)
			if (GetLocalParamUID ())
				SetMagnification (p.GetID ());
	}

	switch (nSymbology) {
	case kSymbologyUPCA:
	case kSymbologyUPCE:
	case kSymbologyEANJAN_8:
	case kSymbologyEANJAN_13:
		return DrawUPCEAN (dc, nSymbology, head, sizeLogical, context, bThrowException, p);
	case kSymbologyInter2of5:
	case kSymbologyGTIN14:
		return DrawI2o5 (dc, head, sizeLogical, context, bThrowException, p);
	case kSymbologyCode128:
	case kSymbologyEAN128:
		return DrawC128 (dc, head, sizeLogical, context, bThrowException, p, nSymbology);
	case kSymbologyCode39Standard:
		return DrawC39 (dc, head, sizeLogical, context, bThrowException, p);
	case kSymbologyCode93:
		return DrawC93 (dc, head, sizeLogical, context, bThrowException, p);
	case kSymbologyDataMatrixEcc200:
	case kSymbologyDataMatrixGs1:
		return DrawDataMatrix (dc, head, sizeLogical, context, bThrowException, p);
	case kSymbologyQR:
		return DrawQR (dc, head, sizeLogical, context, bThrowException, p);
	}

	ASSERT (0);
	return CSize (0, 0);
}

bool CBarcodeElement::SymbologyHasCaption (int nSymbology)
{
	switch (nSymbology) {
	case kSymbologyCode39Full:
	case kSymbologyCode39Standard:
	case kSymbologyInter2of5:
	case kSymbologyGTIN14:
	case kSymbologyCodabar:
	case kSymbologyMSI:
	case kSymbologyCode93:
	case kSymbologyPostNet:
	case kSymbologyCode128:
	case kSymbologyEAN128:
	case kSymbologyUPCA:
	case kSymbologyUPCE:
	case kSymbologyEANJAN_8:
	case kSymbologyEANJAN_13:
//	case kSymbologyPDF417:
//	case kSymbologyMaxiCode:
//	case kSymbologyDataMatrixEcc200:
//	case kSymbologyDataMatrixGs1:
	case kSymbologyLOGMARS:
		return true;
	}

	return false;
}

CTextInfo CBarcodeElement::GetCaption() const
{
	return m_caption;
}

bool CBarcodeElement::SetCaption(const CTextInfo &caption)
{
	m_caption = caption;
	
	return true;

}


bool CBarcodeElement::SetMaxiCodeInfo(const CMaxiCodeInfo &m)
{
	if (m.IsValid ()) {
		m_maxicode = m;
		SetRedraw ();
		return true;
	}

	return false;
}

CMaxiCodeInfo CBarcodeElement::GetMaxiCodeInfo() const
{
	return m_maxicode;
}

CDataMatrixInfo CBarcodeElement::GetDataMatrixInfo () const
{
	return m_datamatrix;
}

bool CBarcodeElement::SetDataMatrixInfo (const CDataMatrixInfo & d)
{
	if (d.IsValid ()) {
		m_datamatrix = d;
		SetRedraw ();
		return true;
	}

	return false;
}


void CBarcodeElement::ClipTo (const FoxjetDatabase::HEADSTRUCT & head)
{
	CBaseElement::ClipTo (head);
}

bool CBarcodeElement::WidthIsPercentage (int nSymbology)
{
	switch (nSymbology) {
	case kSymbologyUPCA:
	case kSymbologyUPCE:
	case kSymbologyEANJAN_8:
	case kSymbologyEANJAN_13:
		return true;
	}

	return false;
}

CString CBarcodeElement::GetFieldName (int nIndex) const
{
	if (nIndex == 11)
		return ElementFields::m_lpszFontName;
	else
		return CBaseElement::GetFieldName (nIndex);
}

int CBarcodeElement::GetFieldIndex (const CString & str) const
{
	if (!str.CompareNoCase (ElementFields::m_lpszFontName))
		return 11;
	else
		return CBaseElement::GetFieldIndex (str);
}

void CBarcodeElement::Reset ()
{
	for (int i = 0; i < m_vSubElements.GetSize (); i++) {
		CSubElement * p = (CSubElement *)m_vSubElements [i];

		p->Reset ();
	}
}

bool CBarcodeElement::SetOrientation (FoxjetElements::TextElement::FONTORIENTATION orient)
{
	m_orient = orient;

	return true;
}

FoxjetElements::TextElement::FONTORIENTATION CBarcodeElement::GetOrientation () const
{
	return m_orient;
}

void CBarcodeElement::Increment ()
{
	for (int i = 0; i < m_vSubElements.GetSize (); i++) {
		CSubElement * p = (CSubElement *)m_vSubElements [i];

		p->GetElement ()->Increment ();
	}
}

bool CBarcodeElement::SetWidth (int n)
{
	LIMITSTRUCT lmt = m_lmtCX;

	if (m_nSymbology == kSymbologyDataMatrixEcc200 || m_nSymbology == kSymbologyDataMatrixGs1)
		lmt.m_dwMin = -2;

	if (FoxjetCommon::IsValid (n, lmt)) {
		m_nWidth = n;
		return true;
	}

	return false;
}

int CBarcodeElement::GetWidth () const
{
	return m_nWidth;
}

bool CBarcodeElement::SetHeight (int n)
{
	LIMITSTRUCT lmt = m_lmtCY;

	if (m_nSymbology == kSymbologyDataMatrixEcc200 || m_nSymbology == kSymbologyDataMatrixGs1)
		lmt.m_dwMin = -2;

	if (FoxjetCommon::IsValid (n, lmt)) {
		m_nHeight = n;
		return true;
	}

	return false;
}

int CBarcodeElement::GetHeight () const
{
	return m_nHeight;
}
