#ifndef __CLASSOFSERVICE_H__
#define __CLASSOFSERVICE_H__

namespace Ritz 
{
	namespace ClassOfService
	{
		typedef struct 
		{
			CString m_strType;
			CString m_strIndicator;
		} CLASSOFSERVICERECSTRUCT;

		extern LPCTSTR m_lpszTable;
		extern LPCTSTR m_lpszType;
		extern LPCTSTR m_lpszIndicator;
	}; //namespace ClassOfService

}; //namespace Ritz 

#endif //__CLASSOFSERVICE_H__
