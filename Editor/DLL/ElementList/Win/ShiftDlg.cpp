// ShiftDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ShiftElement.h"
#include "ShiftDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetElements;

/////////////////////////////////////////////////////////////////////////////
// CShiftDlg dialog


CShiftDlg::CShiftDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
					 const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	CBaseTextDlg(e, pa, IDD_SHIFTCODE, pList, pParent)
{
	//{{AFX_DATA_INIT(CShiftDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CShiftDlg::DoDataExchange(CDataExchange* pDX)
{
	CBaseTextDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CShiftDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BOOL CShiftDlg::OnInitDialog() 
{
	BOOL bResult = CBaseTextDlg::OnInitDialog();
	
	return bResult;
}


BEGIN_MESSAGE_MAP(CShiftDlg, CBaseTextDlg)
	//{{AFX_MSG_MAP(CShiftDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CShiftDlg message handlers

int CShiftDlg::BuildElement ()
{
	CShiftElement & e = GetElement ();

	e.SetOrientation (GetOrientation ());
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

const CShiftElement & CShiftDlg::GetElement () const
{
	const CBaseElement & e = CBaseTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CShiftElement)));
	return (CShiftElement &)e;
}

CShiftElement & CShiftDlg::GetElement ()
{
	CBaseElement & e = CBaseTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CShiftElement)));
	return (CShiftElement &)e;
}

void CShiftDlg::Get (const FoxjetElements::CShiftElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Get (e, pHead);

	m_nID			= e.GetID ();
	m_pt			= e.GetPos (pHead);
	m_nOrientation	= e.GetOrientation ();
	m_nWidth		= e.GetWidth ();
}

void CShiftDlg::Set (FoxjetElements::CShiftElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Set (e, pHead);

	e.SetID (m_nID);
	e.SetAlignment (m_align);
	e.SetPos (m_pt = e.GetPosInternal (m_pt), pHead, ALIGNMENT_ADJUST_SUBTRACT);
	e.SetOrientation (m_nOrientation);
	e.SetWidth (m_nWidth);
	e.SetFontName (m_strFont);
}
