#if !defined(AFX_COUNTDLG_H__471C7423_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_COUNTDLG_H__471C7423_8B3D_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CountDlg.h : header file
//

#include "BaseTextDlg.h"
#include "CountElement.h"
#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// CCountDlg dialog

class CCountDlg : public CBaseTextDlg
{
// Construction
public:
	CCountDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		const FoxjetCommon::CElementList * pList = NULL,
		CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCountDlg)
	BOOL		m_bLeadingZeros;
	int			m_nDigits;
	__int64		m_lCount;
	__int64		m_lMax;
	__int64		m_lMin;
	__int64		m_lIncrement;
	int			m_nPhotocell;
	BOOL		m_bIsPalCount;
	__int64		m_lPalCount;
	__int64		m_lPalIncrement;
	__int64		m_lPalMax;
	__int64		m_lPalMin;
	BOOL		m_bRollover;
	BOOL		m_bMaster;
	BOOL		m_bIrregularPalletSize;
	//}}AFX_DATA

	CString m_strName;

	virtual UINT GetDefCtrlID () const { return TXT_DIGITS; }

	void Get (const FoxjetElements::CCountElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);
	void Set (FoxjetElements::CCountElement & e, const FoxjetDatabase::HEADSTRUCT * pHead);

protected:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCountDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void UpdateUI ();

	virtual int BuildElement ();
	FoxjetElements::CCountElement & GetElement ();
	const FoxjetElements::CCountElement & GetElement () const;

	FoxjetElements::CCountElement * GetByName (const CString & strName, ULONG lIDExclude) const;

	// Generated message map functions
	//{{AFX_MSG(CCountDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnEnabled();
	afx_msg void OnIrregular ();
	afx_msg void OnClickCount(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangeName();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COUNTDLG_H__471C7423_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
