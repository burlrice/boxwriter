#if !defined(AFX_BARCODEPARAMDLG_H__95C13921_BCFE_4963_8B7B_714D91255994__INCLUDED_)
#define AFX_BARCODEPARAMDLG_H__95C13921_BCFE_4963_8B7B_714D91255994__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BarcodeParamDlg.h : header file
//

#include "Resource.h"
#include "BarcodeParams.h"
#include "BarcodeElement.h"
#include "PreviewCtrl.h"
#include "Database.h"
#include "TextElement.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CBarcodeParamDlg dialog

class CBarcodeParamDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CBarcodeParamDlg(FoxjetDatabase::HEADTYPE type, CWnd* pParent = NULL);   // standard constructor

	static void ResizePreview (CDialog * pDlg, int nPreview, int nLeft, int nBottom);

// Dialog Data
	//{{AFX_DATA(CBarcodeParamDlg)
	//}}AFX_DATA

	int		m_nSymbology;
	int		m_nBarWidth;	
	int		m_nBarHeight;
	int		m_nRatio;	
	int		m_nCheckSum;
	int		m_nMagPct;	
	int		m_nHorzBearer;
	int		m_nVertBearer;
	int		m_nQuietZone;

	ULONG	m_lID;
	CString m_strFont;
	int		m_nFontSize;
	int		m_nFontWidth;
	BOOL	m_bBold;
	BOOL	m_bItalic;
	int		m_nAlignment;

	bool	m_bStandardParam;

	const FoxjetDatabase::HEADTYPE m_type;
	FoxjetElements::TextElement::FONTORIENTATION m_orient;

	CArray <FoxjetElements::CBarcodeParams, FoxjetElements::CBarcodeParams &> m_vParams;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBarcodeParamDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();



	// Generated message map functions
	//{{AFX_MSG(CBarcodeParamDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	afx_msg void InvalidatePreview ();
	afx_msg void UpdateBarHeight ();
	afx_msg void OnOrientationClick();

	CBitmap m_bmpCaption [7];
	CBitmap m_bmpOrientation [2];

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BARCODEPARAMDLG_H__95C13921_BCFE_4963_8B7B_714D91255994__INCLUDED_)
