// ExpDateDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ExpDateElement.h"
#include "ExpDateDlg.h"
#include "Resource.h"
#include "ExpDateDatabaseDlg.h"
#include "ExpDateUserDlg.h"
#include "TemplExt.h"
#include "UserElement.h"
#include "DatabaseElement.h"
#include "Extern.h"

using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace ListGlobals;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExpDateDlg dialog


CExpDateDlg::CExpDateDlg(const CBaseElement & e, const CSize & pa, 
						 const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_bHoldStartDate (false),
	CDateTimeDlg(e, pa, IDD_EXPDATE, pList, pParent)
{
	//{{AFX_DATA_INIT(CExpDateDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CExpDateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDateTimeDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CExpDateDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	
	DDX_Text (pDX, TXT_EXPDAYS, m_lSpan);
	DDV_MinMaxLong (pDX, m_lSpan, 0, 25000); 
	// 25000 comes from the msdn documentation for
	// CTimeSpan( LONG lDays, int nHours, int nMins, int nSecs );

	DDX_DateTimeCtrl (pDX, DT_EXP, m_tmExp);
	DDX_Check (pDX, CHK_EXPROUND, m_bRoundUp);
	DDX_Check (pDX, CHK_EXPROUNDDOWN, m_bRoundDown);
	DDX_Check (pDX, CHK_HOLDSTARTDATE, m_bHoldStartDate);	
}


BEGIN_MESSAGE_MAP(CExpDateDlg, CDateTimeDlg)
	//{{AFX_MSG_MAP(CExpDateDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (RDO_NORMAL, OnOrientationClick)
	ON_BN_CLICKED (RDO_VERTICAL, OnOrientationClick)
	
	ON_BN_CLICKED (CHK_EXPROUND, OnRoundUpClick)
	ON_BN_CLICKED (CHK_EXPROUNDDOWN, OnRoundDownClick)

	ON_CBN_SELCHANGE (CB_PERIOD, OnPeriodChange)

	ON_BN_CLICKED (CHK_DATABASE, OnDatabaseClick)
	ON_BN_CLICKED (CHK_USER, OnUserClick)
	ON_BN_CLICKED (BTN_PROPERTIES, OnProperties)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExpDateDlg message handlers

BOOL CExpDateDlg::OnInitDialog() 
{
	CDateTimeCtrl * pDate = (CDateTimeCtrl *)GetDlgItem (DT_EXP);
	CComboBox * pPeriod = (CComboBox *)GetDlgItem (CB_PERIOD);
	struct 
	{
		CExpDateElement::PERIOD		m_period;
		UINT						m_nResID;
	} map [] = 
	{
		{ CExpDateElement::PERIOD_DAYS,		IDS_DAYS,			},
		{ CExpDateElement::PERIOD_WEEKS,	IDS_WEEKS,			},
		{ CExpDateElement::PERIOD_MONTHS,	IDS_MONTHS,			},
		{ CExpDateElement::PERIOD_YEARS,	IDS_YEARS,			},
	};

	ASSERT (pDate);
	ASSERT (pPeriod);

	BOOL bResult = CDateTimeDlg::OnInitDialog();

	pDate->SetFormat (_T ("HH:mm:ss"));

	for (int i = 0; i < (sizeof (map) / sizeof (map [0])); i++) {
		int nIndex = pPeriod->AddString (LoadString (map [i].m_nResID));

		pPeriod->SetItemData (nIndex, (DWORD)map [i].m_period);
		
		if (map [i].m_period == m_period)
			pPeriod->SetCurSel (nIndex);
	}

	if (pPeriod->GetCurSel () == CB_ERR)
		pPeriod->SetCurSel (0);

	{
		CButton * pUser = (CButton *)GetDlgItem (CHK_USER);
		CButton * pDatabase = (CButton *)GetDlgItem (CHK_DATABASE);
	
		ASSERT (pUser);
		ASSERT (pDatabase);

		switch (m_type)
		{
		case CExpDateElement::TYPE_DATABASE:
			pUser->SetCheck (0);
			pDatabase->SetCheck (1);
			break;
		case CExpDateElement::TYPE_USER:
			pUser->SetCheck (1);
			pDatabase->SetCheck (0);
			break;
		}

		OnDatabaseClick ();
		OnUserClick ();
	}

	OnOrientationClick ();
	OnPeriodChange ();

	return bResult;
}


int CExpDateDlg::BuildElement ()
{
	CExpDateElement & e = GetElement ();
	CString str;
	CSize size;

	GetDlgItemText (CB_DATETIMEFORMAT, str);
	e.SetDefaultData (str);
	e.SetOrientation (GetOrientation ());
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));

	if (CButton * p = (CButton *)GetDlgItem (CHK_ROLLOVER))
		e.SetRollover (p->GetCheck () == 1);
	
	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

CExpDateElement & CExpDateDlg::GetElement ()
{
	CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CExpDateElement)));
	return (CExpDateElement &)e;
}

const CExpDateElement & CExpDateDlg::GetElement () const
{
	const CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CExpDateElement)));
	return (CExpDateElement &)e;
}

bool CExpDateDlg::GetValues()
{
	CComboBox * pPeriod = (CComboBox *)GetDlgItem (CB_PERIOD);

	ASSERT (pPeriod);

	m_period = (CExpDateElement::PERIOD)pPeriod->GetItemData (pPeriod->GetCurSel ());

	return CDateTimeDlg::GetValues ();
}

void CExpDateDlg::OnRoundUpClick ()
{
	CButton * pUp = (CButton *)GetDlgItem (CHK_EXPROUND);
	CButton * pDown = (CButton *)GetDlgItem (CHK_EXPROUNDDOWN);

	ASSERT (pUp);
	ASSERT (pDown);

	if (pUp->GetCheck () == 1)
		pDown->SetCheck (0);
}

void CExpDateDlg::OnDatabaseClick ()
{
	CButton * p = (CButton *)GetDlgItem (CHK_DATABASE);

	ASSERT (p);

	if (p->GetCheck () == 1) {
		if (CButton * pOther = (CButton *)GetDlgItem (CHK_USER))
			pOther->SetCheck (0);

		if (m_strDSN.IsEmpty ()) {
			if (CDatabaseElement * pDef = DYNAMIC_DOWNCAST (CDatabaseElement, &defElements.GetElement (DATABASE))) {
				m_bPromptAtTaskStart	= pDef->GetPromptAtTaskStart ();
				m_strDSN				= pDef->GetDSN ();
				m_strTable				= pDef->GetTable ();
				m_strField				= pDef->GetField ();
				m_strKeyField			= pDef->GetKeyField ();
				m_strKeyValue			= pDef->GetKeyValue ();
				m_nSQLType				= pDef->GetSQLType ();
				m_nFormat				= pDef->GetFormatType ();
				m_strFormat				= pDef->GetFormat ();

				SendMessage (WM_COMMAND, MAKELONG (BTN_PROPERTIES, BN_CLICKED), (LPARAM)GetDlgItem (BTN_PROPERTIES)->GetSafeHwnd ());
			}
		}
	}

	if (CWnd * pProperties = GetDlgItem (BTN_PROPERTIES)) 
		pProperties->EnableWindow (GetCheck (this, CHK_DATABASE) || GetCheck (this, CHK_USER));
}

void CExpDateDlg::OnUserClick ()
{
	CButton * p = (CButton *)GetDlgItem (CHK_USER);

	ASSERT (p);

	if (p->GetCheck () == 1) {
		if (CButton * pOther = (CButton *)GetDlgItem (CHK_DATABASE))
			pOther->SetCheck (0);

		OnPeriodChange ();
	}

	if (CWnd * pProperties = GetDlgItem (BTN_PROPERTIES)) 
		pProperties->EnableWindow (GetCheck (this, CHK_DATABASE) || GetCheck (this, CHK_USER));
}

void CExpDateDlg::OnRoundDownClick ()
{
	CButton * pUp = (CButton *)GetDlgItem (CHK_EXPROUND);
	CButton * pDown = (CButton *)GetDlgItem (CHK_EXPROUNDDOWN);

	ASSERT (pUp);
	ASSERT (pDown);

	if (pDown->GetCheck () == 1)
		pUp->SetCheck (0);
}

void CExpDateDlg::OnPeriodChange ()
{
	CComboBox * pPeriod = (CComboBox *)GetDlgItem (CB_PERIOD);
	CButton * pUp = (CButton *)GetDlgItem (CHK_EXPROUND);
	CButton * pDown = (CButton *)GetDlgItem (CHK_EXPROUNDDOWN);
	bool bEnable = false;

	ASSERT (pUp);
	ASSERT (pDown);
	ASSERT (pPeriod);

	int nIndex = pPeriod->GetCurSel ();
	CExpDateElement::PERIOD period = (CExpDateElement::PERIOD)pPeriod->GetItemData (nIndex);

	if (nIndex != CB_ERR)
		bEnable = period != CExpDateElement::PERIOD_DAYS;
	
	pUp->EnableWindow (bEnable);
	pDown->EnableWindow (bEnable);

	if (!bEnable) {
		pUp->SetCheck (0);
		pDown->SetCheck (0);
	}
	else {
		pUp->SetCheck (m_bRoundUp ? 1 : 0);
		pDown->SetCheck (m_bRoundDown ? 1 : 0);
	}

	if (GetCurSelType () == CExpDateElement::TYPE_USER) {
		CExpDateElement::PERIOD period = (CExpDateElement::PERIOD)pPeriod->GetItemData (pPeriod->GetCurSel ());
		UINT n [] = { IDS_DAYS, IDS_WEEKS, IDS_MONTHS, IDS_YEARS };
		CString strPeriod = LoadString (n [period % ARRAYSIZE (n)]);
		CString strDatabasePrompt;
		
		strDatabasePrompt.Format (_T ("[%s].[%s].[%s]"),
			m_strDSN,
			m_strTable,
			m_strKeyField);

		if (m_strPrompt.IsEmpty () || !m_strPrompt.CompareNoCase (strDatabasePrompt)) {
			m_strPrompt.Format (_T ("%s [%s]:"), LoadString (IDS_EXPIRESIN), strPeriod);
			
			if (CUserElement * pUser = DYNAMIC_DOWNCAST (CUserElement, &defElements.GetElement (USER)))
				m_bPromptAtTaskStart = pUser->m_bPromptAtTaskStart;

			SendMessage (WM_COMMAND, MAKELONG (BTN_PROPERTIES, BN_CLICKED), (LPARAM)GetDlgItem (BTN_PROPERTIES)->GetSafeHwnd ());
		}

		for (int i = 0; i < ARRAYSIZE (n); i++) 
			m_strPrompt.Replace (LoadString (n [i]), strPeriod);
	}
}

void CExpDateDlg::Get (const FoxjetElements::CExpDateElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Get (e, pHead);

	m_nID = e.GetID ();
	m_pt = e.GetPos (pHead);
	m_strFmt = e.GetDefaultData();
	m_lSpan = e.GetSpan (CExpDateElement::SPAN_DAYS);
	m_tmExp = CTime (1970, 1, 1, 
		e.GetSpan (CExpDateElement::SPAN_HOURS),
		e.GetSpan (CExpDateElement::SPAN_MINUTES),
		e.GetSpan (CExpDateElement::SPAN_SECONDS));
	m_nOrientation	= e.GetOrientation ();
	m_nWidth		= e.GetWidth ();
	m_period		= e.GetPeriod ();
	m_bRoundUp		= e.GetRound () == CExpDateElement::EXP_ROUNDUP;
	m_bRoundDown	= e.GetRound () == CExpDateElement::EXP_ROUNDDOWN;
	m_bRollover		= e.GetRollover ();

	m_type				 = e.GetType ();
	m_bPromptAtTaskStart = e.GetPromptAtTaskStart ();
	m_strPrompt			 = e.GetPrompt ();
	m_strDSN			 = e.GetDSN	();
	m_strTable			 = e.GetTable ();
	m_strField			 = e.GetField ();
	m_strKeyField		 = e.GetKeyField ();
	m_strKeyValue		 = e.GetKeyValue ();
	m_nSQLType			 = e.GetSQLType ();
	m_nFormat			 = e.GetFormatType ();
	m_strFormat			 = e.GetFormat ();
	m_bHoldStartDate	 = e.GetHoldStartDate ();
}

void CExpDateDlg::Set (FoxjetElements::CExpDateElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Set (e, pHead);

	e.SetID (m_nID);
	e.SetDefaultData (m_strFmt);
	e.SetSpan (m_lSpan,					CExpDateElement::SPAN_DAYS);
	e.SetSpan (m_tmExp.GetHour (),		CExpDateElement::SPAN_HOURS);
	e.SetSpan (m_tmExp.GetMinute (),	CExpDateElement::SPAN_MINUTES);
	e.SetSpan (m_tmExp.GetSecond (),	CExpDateElement::SPAN_SECONDS);
	e.SetAlignment (m_align);
	e.SetPos (m_pt = e.GetPosInternal (m_pt), pHead, FoxjetCommon::ALIGNMENT_ADJUST_SUBTRACT);
	e.SetOrientation (m_nOrientation);
	e.SetWidth (m_nWidth);
	e.SetPeriod (m_period);
	e.SetRollover (m_bRollover ? true : false);
	e.SetFontName (m_strFont);

	if (m_bRoundDown)
		e.SetRound (CExpDateElement::EXP_ROUNDDOWN);
	else if (m_bRoundUp)
		e.SetRound (CExpDateElement::EXP_ROUNDUP);
	else
		e.SetRound (CExpDateElement::EXP_NOROUND);

	e.SetType				(m_type);
	e.SetPromptAtTaskStart	(m_bPromptAtTaskStart);
	e.SetPrompt				(m_strPrompt);
	e.SetDSN				(m_strDSN);
	e.SetTable				(m_strTable);
	e.SetField				(m_strField);
	e.SetKeyField			(m_strKeyField);
	e.SetKeyValue			(m_strKeyValue);
	e.SetSQLType			(m_nSQLType);
	e.SetFormatType			(m_nFormat);
	e.SetFormat				(m_strFormat);
	e.SetHoldStartDate		(m_bHoldStartDate);
}

CExpDateElement::TYPE CExpDateDlg::GetCurSelType () const
{
	if (CButton * p = (CButton *)GetDlgItem (CHK_DATABASE))
		if (p->GetCheck () == 1)
			return CExpDateElement::TYPE_DATABASE;

	if (CButton * p = (CButton *)GetDlgItem (CHK_USER))
		if (p->GetCheck () == 1)
			return CExpDateElement::TYPE_USER;

	return CExpDateElement::TYPE_NORMAL;
}

void CExpDateDlg::OnProperties ()
{
	switch (GetCurSelType ()) {
	case CExpDateElement::TYPE_DATABASE:
		{
			CExpDateDatabaseDlg dlg (GetLineID (), this);

			dlg.m_strDSN				= m_strDSN;
			dlg.m_strTable				= m_strTable;
			dlg.m_strField				= m_strField;
			dlg.m_strKeyField			= m_strKeyField;
			dlg.m_strKeyValue			= m_strKeyValue;
			dlg.m_bPromptAtTaskStart	= m_bPromptAtTaskStart;
			dlg.m_nSQLType				= m_nSQLType;
			dlg.m_nFormat				= m_nFormat;
			dlg.m_strFormat				= m_strFormat;

			if (dlg.DoModal () == IDOK) {
				m_strDSN				= dlg.m_strDSN;
				m_strTable				= dlg.m_strTable;
				m_strField				= dlg.m_strField;
				m_strKeyField			= dlg.m_strKeyField;
				m_strKeyValue			= dlg.m_strKeyValue;
				m_bPromptAtTaskStart	= dlg.m_bPromptAtTaskStart ? true : false;
				m_nSQLType				= dlg.m_nSQLType;
				m_nFormat				= dlg.m_nFormat;
				m_strFormat				= dlg.m_strFormat;
			}
		}
		break;
	case CExpDateElement::TYPE_USER:
		{
			CExpDateUserDlg dlg (this);

			dlg.m_bPromptAtTaskStart	= m_bPromptAtTaskStart;
			dlg.m_strPrompt				= m_strPrompt;

			if (dlg.DoModal () == IDOK) {
				m_bPromptAtTaskStart	= dlg.m_bPromptAtTaskStart ? true : false;
				m_strPrompt				= dlg.m_strPrompt;
			}
		}
		break;
	}
}


void CExpDateDlg::OnOK()
{
	m_type = GetCurSelType ();

	//switch (GetCurSelType ()) {
	//case CExpDateElement::TYPE_DATABASE:
	//	m_strPrompt				= _T ("");
	//	break;
	//case CExpDateElement::TYPE_USER:
	//	m_strDSN				= _T ("");
	//	m_strTable				= _T ("");
	//	m_strField				= _T ("");
	//	m_strKeyField			= _T ("");
	//	m_strKeyValue			= _T ("");
	//	m_nSQLType				= 0;
	//	m_nFormat				= 0;
	//	m_strFormat				= _T ("");
	//	break;
	//}

	CDateTimeDlg::OnOK ();
}
