// DbWizard.cpp : implementation file
//

#include "stdafx.h"
#include "DbWizard.h"
#include "EnTabCtrl.h"
#include "DbWizardDsnDlg.h"
#include "DbWizardFieldDlg.h"
#include "DbWizardTypesDlg.h"
#include "DbWizardKeyFieldDlg.h"
#include "DbWizardDbStartDlg.h"
#include "DbWizardFinishDlg.h"

using namespace FoxjetElements;

extern HINSTANCE hInstance;

////////////////////////////////////////////////////////////////////////////////////////////////////
IMPLEMENT_DYNAMIC(CWizardBasePage, CMFCPropertyPage)

CWizardBasePage::CWizardBasePage (CBaseWizardSheet * pParent, UINT nID)
:	CMFCPropertyPage (nID)
{
	m_psp.dwFlags |= PSH_NOAPPLYNOW;
	m_psp.dwFlags |= PSH_USECALLBACK;
	m_psp.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetPageProc;
	m_pdlgParent = pParent;
}

CWizardBasePage::~CWizardBasePage ()
{
}

BOOL CWizardBasePage::OnInitDialog ()
{
	CMFCPropertyPage::OnInitDialog ();
	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC(CDbWizardBasePage, CWizardBasePage)

CDbWizardBasePage::CDbWizardBasePage(CDbWizardDlg * pParent, UINT nID)
:	CWizardBasePage (pParent, nID)
{
	m_pdlgMain = pParent;
}

CDbWizardBasePage::~CDbWizardBasePage()
{
}

////////////////////////////////////////////////////////////////////////////////
// CDbWizardDlg

IMPLEMENT_DYNAMIC(CBaseWizardSheet, CMFCPropertySheet)

CBaseWizardSheet::CBaseWizardSheet(CWnd * pParent, UINT nID)
:	CMFCPropertySheet (nID, pParent, 0)
{
	m_hRestore = ::AfxGetResourceHandle ();
	::AfxSetResourceHandle (::hInstance);
	m_psh.dwFlags |= PSH_NOAPPLYNOW;
	m_psh.dwFlags |= PSH_USECALLBACK;
	m_psh.pfnCallback = FoxjetCommon::CEnPropertySheet::PropSheetProc;

	SetLook (CMFCPropertySheet::PropSheetLook_Tree, 150);

}

CBaseWizardSheet::~CBaseWizardSheet()
{
	::AfxSetResourceHandle (m_hRestore);
}


BEGIN_MESSAGE_MAP(CBaseWizardSheet, CMFCPropertySheet)
	ON_BN_CLICKED (BTN_BACK, OnBack)
	ON_COMMAND(IDOK, OnNext)
END_MESSAGE_MAP()



// CBaseWizardSheet message handlers


BOOL CBaseWizardSheet::OnInitDialog()
{
	CMFCPropertySheet::OnInitDialog();

	m_wndTree.ModifyStyle (0, TVS_SHOWSELALWAYS);
	//m_wndTree.EnableWindow (0);
	
	{
		VERIFY (m_btnBack.Create (LoadString (IDS_BACK), BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE | WS_TABSTOP, CRect (0, 0, 0, 0), this, BTN_BACK));
		CRect rcOK (0, 0, 0, 0), rcCancel (0, 0, 0, 0);
	
		if (CWnd * p = GetDlgItem (IDOK))
			p->GetWindowRect (rcOK);

		if (CWnd * p = GetDlgItem (IDCANCEL))
			p->GetWindowRect (rcCancel);

		int nGap = abs (rcOK.right - rcCancel.left);
		CRect rc = rcOK - CPoint (rcOK.Width () + nGap, 0);

		ScreenToClient (rc);
		m_btnBack.SetWindowPos (NULL, rc.left, rc.top, rc.Width (), rc.Height (), SWP_NOZORDER);
		m_btnBack.SetFont (GetFont ());
	}

	return TRUE;
}

void CBaseWizardSheet::OnNext ()
{
	OnOK ();
}

void CBaseWizardSheet::OnBack ()
{
	int n = GetActiveIndex ();

	if (n > 0)
		SetActivePage (n - 1);
}

void CBaseWizardSheet::OnOK()
{
	int n = GetActiveIndex ();

	if (n < (GetPageCount () - 1)) {
		if (CDbWizardBasePage * p = DYNAMIC_DOWNCAST (CDbWizardBasePage, GetActivePage ()))
			if (!p->CanAdvance ())
				return;

		SetActivePage (n + 1);
	}
	else 
		EndDialog (IDOK);
}


LRESULT CBaseWizardSheet::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	LRESULT lResult = CMFCPropertySheet::WindowProc(message, wParam, lParam);
		
	switch (message) {
	case PSM_SETCURSEL:
		{
			int n = GetActiveIndex ();

			if (CWnd * p = GetDlgItem (BTN_BACK))
				p->EnableWindow ((n > 0));

			if (CWnd * p = GetDlgItem (IDOK)) {
				bool bLast = (n == (GetPageCount () - 1));

				p->SetWindowText (LoadString (bLast ? IDS_FINISHED : IDS_NEXT));
				//p->EnableWindow (bLast ? TRUE : FALSE);
			}
		}

		break;
	}

	return lResult;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
IMPLEMENT_DYNAMIC(CDbWizardDlg, CBaseWizardSheet)

CDbWizardDlg::CDbWizardDlg (CWnd * pParent)
: 	m_pdb (NULL),
	CBaseWizardSheet (pParent, IDS_DATABASEWIZARD)
{
	AddPageToTree (NULL, m_pdlgDSN		= new CDbWizardDsnDlg (this));
	AddPageToTree (NULL, m_pdlgField	= new CDbWizardFieldDlg	 (this));
	AddPageToTree (NULL, m_pdlgKeyField	= new CDbWizardKeyFieldDlg (this));
	AddPageToTree (NULL, m_pdlgDbStart	= new CDbWizardDbStartDlg	 (this));
	AddPageToTree (NULL, m_pdlgFinish	= new CDbWizardFinishDlg	 (this));
}

CDbWizardDlg::~CDbWizardDlg ()
{
	delete m_pdlgDSN;
	delete m_pdlgField;
	delete m_pdlgKeyField;
	delete m_pdlgDbStart;
	delete m_pdlgFinish;
	
	if (m_pdb) 
		delete m_pdb;

	m_pdlgDSN		= NULL;
	m_pdlgField		= NULL;
	m_pdlgKeyField	= NULL;
	m_pdlgDbStart	= NULL;
	m_pdlgFinish	= NULL;
	m_pdb			= NULL;
}