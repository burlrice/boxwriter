// CountDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CountDlg.h"
#include "CountElement.h"
#include "ItiLibrary.h"
#include "Database.h"
#include "Extern.h"
#include "TemplExt.h"
#include "Debug.h"
#include "Utils.h"
#include "MsgBox.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace FoxjetElements;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCountDlg dialog

struct
{
	UINT m_nID;
	int m_nTab;
} static const map [] = 
{
	{ LBL_START, 			0,	},
	{ LBL_ROLLOVER, 		0,	},
	{ LBL_INCREMENT,		0,	},
	{ TXT_MIN,				0,	},
	{ TXT_MAX,				0,	},
	{ TXT_INCREMENT,		0,	},

	{ LBL_PAL_START,		1,	},
	{ LBL_PAL_ROLLOVER, 	1,	},
	{ LBL_PAL_INCREMENT,	1,	},
	{ CHK_ENABLED,			1,	},
	{ TXT_PALMIN,			1,	},
	{ TXT_PALMAX,			1,	},
	{ CHK_ROLLOVER,			1,	},
	{ TXT_PALINCREMENT,		1,	},
	{ TXT_PALQUANTITY,		1,	},
	{ LBL_PALQUANTITY,		1,	},
	{ CHK_IRREGULAR,		1,	},
};

CCountDlg::CCountDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
					 const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	CBaseTextDlg(e, pa, IDD_COUNT, pList, pParent)
{
	//{{AFX_DATA_INIT(CCountDlg)
	m_bLeadingZeros = FALSE;
	m_nDigits = 0;
	m_lCount = 0;
	m_lMax = ULONG_MAX;
	m_lMin = 0;
	m_lIncrement = 1;
	m_nPhotocell = 0;
	m_bIsPalCount = FALSE;
	m_lPalCount = 0;
	m_lPalIncrement = 0;
	m_lPalMax = 0;
	m_lPalMin = 0;
	m_bIrregularPalletSize = FALSE;
	//}}AFX_DATA_INIT
}

void CCountDlg::DoDataExchange(CDataExchange* pDX)
{
	const __int64 iMin = 0;
	const __int64 iMax = CCountElement::m_iLmtStart;

	CBaseTextDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCountDlg)
	DDX_Check(pDX, CHK_ZEROS, m_bLeadingZeros);
	DDX_Check(pDX, CHK_ENABLED, m_bIsPalCount);
	//}}AFX_DATA_MAP

	DDX_CBString (pDX, CB_NAME, m_strName);

	// # digits
	DDX_Text(pDX, TXT_DIGITS, m_nDigits);
	DDV_MinMaxInt (pDX, m_nDigits, CCountElement::MINDIGITS, GetNumberOfDigits (iMax));
	
	// min
	FoxjetCommon::DDX_Text(pDX, TXT_MIN, m_lMin);
	DDV_MinMaxUnsignedInt64 /* DDV_MinMaxDWord */ (pDX, TXT_MIN, m_lMin, iMin, iMax - 1);

	// max
	FoxjetCommon::DDX_Text(pDX, TXT_MAX, m_lMax);
	DDV_MinMaxUnsignedInt64 /* DDV_MinMaxDWord */ (pDX, TXT_MAX, m_lMax, m_lMin + 1, iMax);
	
	// inc
	FoxjetCommon::DDX_Text(pDX, TXT_INCREMENT, m_lIncrement);
	DDV_MinMaxInt64 /* DDV_MinMaxLong */ (pDX, TXT_INCREMENT, m_lIncrement, -iMax, iMax);
	if (pDX->m_bSaveAndValidate) {
		if (m_lIncrement == 0) {
			MsgBox (* this, LoadString (IDS_INVALIDVALUE), (LPCTSTR)NULL, MB_ICONINFORMATION);
			pDX->PrepareCtrl (TXT_INCREMENT);
			pDX->Fail ();
		}
	}


	// pal min
	FoxjetCommon::DDX_Text(pDX, TXT_PALMIN, m_lPalMin);

	// pal max
	FoxjetCommon::DDX_Text(pDX, TXT_PALMAX, m_lPalMax);
	
	// pal inc
	FoxjetCommon::DDX_Text(pDX, TXT_PALINCREMENT, m_lPalIncrement);

	if (m_bIsPalCount) { // don't bother to validate unless its a pallet count
		// pal min
		DDV_MinMaxUnsignedInt64 /* DDV_MinMaxDWord */ (pDX, TXT_PALMIN, m_lPalMin, iMin, iMax - 1);

		// pal max
		DDV_MinMaxUnsignedInt64 /* DDV_MinMaxDWord */ (pDX, TXT_PALMAX, m_lPalMax, m_lPalMin + 1, iMax);
		
		// pal inc
		DDV_MinMaxInt64 /* DDV_MinMaxLong */ (pDX, TXT_PALINCREMENT, m_lPalIncrement, -iMax, iMax);
		if (pDX->m_bSaveAndValidate) {
			if (m_lPalIncrement == 0) {
				MsgBox (* this, LoadString (IDS_INVALIDVALUE), (LPCTSTR)NULL, MB_ICONINFORMATION);
				pDX->PrepareCtrl (TXT_PALINCREMENT);
				pDX->Fail ();
			}
		}

		// units per pallet
		FoxjetCommon::DDX_Text(pDX, TXT_PALQUANTITY, m_lMax);
		DDV_MinMaxUnsignedInt64 /* DDV_MinMaxDWord */ (pDX, TXT_PALQUANTITY, m_lMax, 0, iMax);
		m_lMin = 0;
		m_lIncrement = 1;
	}

	DDX_Check (pDX, CHK_ROLLOVER, m_bRollover);
	DDX_Check (pDX, CHK_MASTER, m_bMaster);
	DDX_Check (pDX, CHK_IRREGULAR, m_bIrregularPalletSize);
}

BEGIN_MESSAGE_MAP(CCountDlg, CBaseTextDlg)
	//{{AFX_MSG_MAP(CCountDlg)
	ON_BN_CLICKED(CHK_ENABLED, OnEnabled)
	ON_BN_CLICKED(CHK_IRREGULAR, OnIrregular)
	ON_NOTIFY(NM_CLICK, TAB_COUNT, OnClickCount)
	ON_CBN_SELCHANGE(CB_NAME, OnSelchangeName)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCountDlg message handlers

BOOL CCountDlg::OnInitDialog() 
{
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (TAB_COUNT);
	ASSERT (pTab);

	BOOL bResult = CBaseTextDlg::OnInitDialog();

	pTab->InsertItem (0, LoadString (IDS_BOXCOUNT));
	pTab->InsertItem (1, LoadString (IDS_PALLETCOUNT));

#if __CUSTOM__ == __SW0828__
	pTab->DeleteItem (1);
	pTab->InsertItem (1, _T ("Lot count"));
#endif

	if (m_pAllLists) {
		CComboBox * pCB = (CComboBox *)GetDlgItem (CB_NAME);

		ASSERT (pCB);

		for (int nList = 0; nList < m_pAllLists->GetSize (); nList++) {
			if (FoxjetCommon::CElementList * pList = m_pAllLists->GetAt (nList)) {
				for (int nElement = 0; nElement < pList->GetSize (); nElement++) {
					if (CCountElement * pCount = DYNAMIC_DOWNCAST (CCountElement, &pList->GetObject (nElement))) {
						CString strName = pCount->GetName ();

						if (pCB->FindStringExact (-1, strName) == CB_ERR) {
							int nIndex = pCB->AddString (strName);

							if (!strName.CompareNoCase (m_strName))
								pCB->SetCurSel (nIndex);
						}
					}
				}
			}
		}

		if (pCB->GetCount () == 0)
			pCB->AddString (CCountElement::m_lpszDefName);

		if (pCB->GetCurSel () == CB_ERR) 
			pCB->SetCurSel (0);

		OnSelchangeName ();
	}

	if (m_bIsPalCount) 
		pTab->SetCurSel (1);

	UpdateUI ();
	OnEnabled ();

	return bResult;
}

void CCountDlg::OnIrregular ()
{
	if (CButton * p = (CButton *)GetDlgItem (CHK_IRREGULAR)) {
		if (CButton * pMaster = (CButton *)GetDlgItem (CHK_MASTER)) {
			if (p->GetCheck () == 1) 
				pMaster->SetCheck (1); 
			else
				pMaster->SetCheck (m_bMaster); 
		}
	}
}

void CCountDlg::OnEnabled() 
{
	CButton * p = (CButton *)GetDlgItem (CHK_ENABLED);
	ASSERT (p);
	bool bEnabled = p->GetCheck () == 1;
	int n [] = 
	{
		TXT_PALMIN,
		TXT_PALMAX,
		TXT_PALINCREMENT,
		TXT_PALQUANTITY,
		CHK_ROLLOVER,
		CHK_IRREGULAR,
	};
	
	for (int i = 0; i < ARRAYSIZE (n); i++)
		if (CWnd * pwnd = GetDlgItem (n [i]))
			pwnd->EnableWindow (bEnabled);
}

void CCountDlg::OnClickCount(NMHDR* pNMHDR, LRESULT* pResult) 
{
	UpdateUI ();
	*pResult = 0;
}

void CCountDlg::UpdateUI()
{
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (TAB_COUNT);
	ASSERT (pTab);
	int nSel = pTab->GetCurSel ();
	CString str;

	for (int i = 0; i < ARRAYSIZE (::map); i++) {
		CWnd * p = GetDlgItem (::map [i].m_nID);

		ASSERT (p);
		p->ShowWindow (::map [i].m_nTab == nSel ? SW_SHOW : SW_HIDE);
	}

#if __CUSTOM__ == __SW0828__
	{ // NOTE: had to change IDD_COUNT & this entire fct

		CStatic * p = (CStatic *)GetDlgItem (LBL_PAL_START);

		ASSERT (p);
		p->SetWindowText (_T ("Lot"));
	}
#endif

	int nMax = GetDlgItemInt (nSel == 0 ? TXT_MAX : TXT_PALQUANTITY);
	SetDlgItemInt (nSel == 0 ? TXT_PALQUANTITY : TXT_MAX, nMax);
}

int CCountDlg::BuildElement ()
{
	CCountElement & e = GetElement ();

	CString strCount, strMin, strMax, strIncrement, strPalCount;
	CString strPalMin, strPalMax, strPalIncrement, strDigits;
	CSize size;
	CButton * pZeros = (CButton *)GetDlgItem (CHK_ZEROS);
	CButton * pEnabled = (CButton *)GetDlgItem (CHK_ENABLED);
	
	GetDlgItemText (TXT_MIN, strMin);
	GetDlgItemText (TXT_MAX, strMax);
	GetDlgItemText (TXT_INCREMENT, strIncrement);
	GetDlgItemText (TXT_PALMIN, strPalMin);
	GetDlgItemText (TXT_PALMAX, strPalMax);
	GetDlgItemText (TXT_PALINCREMENT, strPalIncrement);
	GetDlgItemText (TXT_DIGITS, strDigits);

	e.SetLeadingZeros (pZeros->GetCheck () == 1 ? true : false);
	e.SetDigits (_ttoi (strDigits));
	e.EnablePalletCount (pEnabled->GetCheck () == 1 ? true : false);

	e.SetMin (_ttol (strMin));
	e.SetMax (_ttol (strMax));
	e.SetIncrement (_ttol (strIncrement));
	e.SetPalMin (_ttol (strPalMin));
	e.SetPalMax (_ttol (strPalMax));
	e.SetPalIncrement (_ttol (strPalIncrement));
	e.SetPalCount (_ttol (strPalCount));
	e.SetOrientation (GetOrientation ());
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

CCountElement & CCountDlg::GetElement ()
{
	const CBaseElement & e = CBaseTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CCountElement)));
	return (CCountElement &)e;
}

const CCountElement & CCountDlg::GetElement () const
{
	const CBaseElement & e = CBaseTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CCountElement)));
	return (CCountElement &)e;
}


CCountElement * CCountDlg::GetByName (const CString & strName, ULONG lIDExclude) const
{
	for (int nList = 0; nList < m_pAllLists->GetSize (); nList++) {
		if (FoxjetCommon::CElementList * pList = m_pAllLists->GetAt (nList)) {
			for (int nElement = 0; nElement < pList->GetSize (); nElement++) {
				if (CCountElement * pCount = DYNAMIC_DOWNCAST (CCountElement, &pList->GetObject (nElement))) {
					if (!strName.CompareNoCase (pCount->GetName ()) && pCount->GetID () != lIDExclude) {
						return pCount;
					}
				}
			}
		}
	}

	return NULL;
}

void CCountDlg::OnSelchangeName() 
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_NAME);
	const UINT nID = m_nID;
	const CPoint pt = m_pt;

	ASSERT (pCB);

	int nIndex = pCB->GetCurSel ();

	if (nIndex != -1) {
		CString strName;

		pCB->GetLBText (nIndex, strName);

		if (CCountElement * pCount = GetByName (strName, m_nID)) {
			Get (* pCount, &GetHead ());
			m_nID = nID;			
			m_pt = pt;

			if (CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (TAB_COUNT)) 
				pTab->SetCurSel (m_bIsPalCount ? 1 : 0);

			UpdateUI ();
			OnEnabled ();
			UpdateData (FALSE);
		}
	}
}

void CCountDlg::Get (const FoxjetElements::CCountElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{
	CElementDlg::Get (e, pHead);

	m_nID					= e.GetID ();
	m_pt					= e.GetPos (pHead);
	m_bLeadingZeros			= e.GetLeadingZeros ();
	m_nDigits				= e.GetDigits ();
	m_lCount				= e.GetCount ();
	m_lIncrement			= e.GetIncrement ();
	m_lMax					= e.GetMax ();
	m_lMin					= e.GetMin ();
	m_lPalCount				= e.GetPalCount ();
	m_lPalIncrement			= e.GetPalIncrement ();
	m_lPalMax				= e.GetPalMax ();
	m_lPalMin				= e.GetPalMin ();
	m_bIsPalCount			= e.IsPalletCount ();
	m_nOrientation			= e.GetOrientation ();
	m_nWidth				= e.GetWidth ();
	m_strName				= e.GetName ();
	m_bRollover				= e.GetRollover ();
	m_bMaster				= e.IsMaster ();
	m_bIrregularPalletSize	= e.IsIrregularPalletSize ();
}

void CCountDlg::Set (FoxjetElements::CCountElement & e, const FoxjetDatabase::HEADSTRUCT * pHead)
{

	e.Reset ();

	CElementDlg::Set (e, pHead);

	e.SetLeadingZeros (m_bLeadingZeros ? true : false);
	e.SetDigits (m_nDigits);
	e.EnablePalletCount (m_bIsPalCount ? true : false);

	bool bSetMin			= e.SetMin (m_lMin);
	bool bSetMax			= e.SetMax (m_lMax);
	bool bSetIncrement		= e.SetIncrement (m_lIncrement);
	bool bSetPalMin			= e.SetPalMin (m_lPalMin);
	bool bSetPalMax			= e.SetPalMax (m_lPalMax);
	bool bSetPalIncrement	= e.SetPalIncrement (m_lPalIncrement);
	bool bSetPalCount		= e.SetPalCount (m_lPalCount);

	e.SetAlignment (m_align);
	e.SetPos (m_pt = e.GetPosInternal (m_pt), pHead, ALIGNMENT_ADJUST_SUBTRACT);
	e.SetOrientation (m_nOrientation);
	e.SetWidth (m_nWidth);
	e.SetName (m_strName);
	e.SetRollover (m_bRollover ? true : false);
	e.SetMaster (m_bMaster ? true : false);
	e.SetIrregularPalletSize (m_bIrregularPalletSize);

	ASSERT (bSetIncrement);
	ASSERT (bSetMin);
	ASSERT (bSetMax);
	ASSERT (bSetPalIncrement);
	ASSERT (bSetPalMin);
	ASSERT (bSetPalMax);
	ASSERT (bSetPalCount);
}