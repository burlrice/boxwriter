#if !defined(AFX_CUSTOMFMTDLG_H__C18A9081_9064_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_CUSTOMFMTDLG_H__C18A9081_9064_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CustomFmtDlg.h : header file
//

#include "ListCtrlImp.h"
#include "Resource.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CCustomFmtDlg dialog

class CCustomFmtDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CCustomFmtDlg(ULONG lLineID = -1, CWnd* pParent = NULL);   // standard constructor
	virtual ~CCustomFmtDlg ();

	void GetData (CStringArray & v);
	void SetData (const CStringArray & v);

// Dialog Data
	//{{AFX_DATA(CCustomFmtDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCustomFmtDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	class CItem : public ItiLibrary::ListCtrlImp::CItem 
	{
	public:
		CItem (ULONG lLineID = -1, const CString & strFormat = _T (""));
		virtual ~CItem ();

		virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
		virtual CString GetDispText (int nColumn) const;

		CString m_strFormat;
		CString m_strExample;
		ULONG m_lLineID;
	};

	virtual void OnOK();

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lvi;
	ULONG m_lLineID;

	// Generated message map functions
	//{{AFX_MSG(CCustomFmtDlg)
	afx_msg void OnAdd();
	afx_msg void OnDelete();
	afx_msg void OnEdit();
	//}}AFX_MSG

	afx_msg void OnDblclkListCtrl(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL OnInitDialog( );

	CStringArray m_vFormats;

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CUSTOMFMTDLG_H__C18A9081_9064_11D4_8FC6_006067662794__INCLUDED_)
