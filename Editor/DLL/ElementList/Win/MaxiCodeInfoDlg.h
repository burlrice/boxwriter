#if !defined(AFX_MAXICODEINFODLG_H__EA87F461_F387_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_MAXICODEINFODLG_H__EA87F461_F387_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MaxiCodeInfoDlg.h : header file
//

#include "resource.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CMaxiCodeInfoDlg dialog

class CMaxiCodeInfoDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CMaxiCodeInfoDlg(CWnd* pParent = NULL);   // standard constructor

public:
// Dialog Data
	//{{AFX_DATA(CMaxiCodeInfoDlg)
	enum { IDD = IDD_MAXICODEINFO };
	CString	m_strClassOfSerice;
	CString	m_strCountryCode;
	CString	m_strPostalCode;
	int		m_nLength;
	//}}AFX_DATA
	int m_nMode;

	void AFXAPI DDV_ClassOfService (CDataExchange* pDX, const CString & str);
	void AFXAPI DDV_CountryCode (CDataExchange* pDX, const CString & str);
	void AFXAPI DDV_PostalCode (CDataExchange* pDX, const CString & str, int nMode);
	void AFXAPI DDV_Length (CDataExchange* pDX, int n);
	void AFXAPI DDXV_Mode (CDataExchange* pDX, int n);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMaxiCodeInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMaxiCodeInfoDlg)
	afx_msg void OnClassofservice();
	afx_msg void OnCountrycode();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAXICODEINFODLG_H__EA87F461_F387_11D4_8FC6_006067662794__INCLUDED_)
