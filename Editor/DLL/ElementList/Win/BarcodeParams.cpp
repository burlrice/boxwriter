// BarcodeParams.cpp: implementation of the CBarcodeParams class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "list.h"
#include "BarcodeParams.h"
#include "BarcodeElement.h"
#include "Utils.h"
#include "Debug.h"
#include "Database.h"
#include "Extern.h"
#include "BarcodeDlg.h"
#include "TemplExt.h"
#include "Edit.h"
#include "Parse.h"

using namespace FoxjetCommon;
using namespace FoxjetElements;
using namespace FoxjetElements::TextElement;
using namespace FoxjetDatabase;
using namespace ListGlobals;


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

static const CString strPrefix = _T ("BarcodeParam");

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

const FoxjetCommon::LIMITSTRUCT CBarcodeParams::m_lmtBearer		= { 0, 500	}; // mils
const FoxjetCommon::LIMITSTRUCT CBarcodeParams::m_lmtQuietZone	= { 0, 2000 }; // mils
const FoxjetCommon::LIMITSTRUCT CBarcodeParams::m_lmtBar		= { 1, 80	}; // pixels

////////////////////////////////////////////////////////////////////////////////

CUPCAParams::CUPCAParams (int nMag, int nBarHeight, 
						  int nSpace1, int nSpace2, int nSpace3, int nSpace4,
						  int nBar1, int nBar2, int nBar3, int nBar4,
						  int nFontWidth,
						  LPCTSTR lpszFont, int nSize, int nAlignment, bool bCustom)
:	CBarcodeParams (kSymbologyUPCA, 40, nBarHeight, 20, 1, nMag, 0, 0, 0, lpszFont, nSize, bCustom)
{
	m_nBar [0]		= nBar1;
	m_nBar [1]		= nBar2;
	m_nBar [2]		= nBar3;
	m_nBar [3]		= nBar4;

	m_nSpace [0]	= nSpace1;
	m_nSpace [1]	= nSpace2;
	m_nSpace [2]	= nSpace3;
	m_nSpace [3]	= nSpace4;

	m_nFontWidth	= nFontWidth;
}

////////////////////////////////////////////////////////////////////////////////


CC128Params::CC128Params (int nSymbology, int nMag, int nBarHeight, 
						  int nSpace1, int nSpace2, int nSpace3, int nSpace4,
						  int nBar1, int nBar2, int nBar3, int nBar4,
						  int nFontWidth,
						  LPCTSTR lpszFont, int nSize, int nAlignment, bool bCustom)
:	CBarcodeParams (nSymbology,		   40, nBarHeight, 20, 1, nMag, 0, 0, 0, lpszFont, nSize, bCustom)
{
	m_nBar [0]		= nBar1;
	m_nBar [1]		= nBar2;
	m_nBar [2]		= nBar3;
	m_nBar [3]		= nBar4;

	m_nSpace [0]	= nSpace1;
	m_nSpace [1]	= nSpace2;
	m_nSpace [2]	= nSpace3;
	m_nSpace [3]	= nSpace4;

	m_nFontWidth	= nFontWidth;
}
////////////////////////////////////////////////////////////////////////////////

CI2o5Params::CI2o5Params (int nMag, int nBarHeight, 
						  int nSpace1, int nSpace2, 
						  int nBar1, int nBar2,
						  int nHBearer,	int nVBearer, int nQuietzone,
						  bool bChecksum, int nFontWidth,
						  LPCTSTR lpszFont, int nSize, int nAlignment, bool bCustom)
:	CBarcodeParams (kSymbologyInter2of5, 40, nBarHeight, 20, 0, nMag, 0, 0, 0, lpszFont, nSize, bCustom)
{
	m_nBar [0]		= nBar1;
	m_nBar [1]		= nBar2;
	m_nBar [2]		= 1;
	m_nBar [3]		= 1;

	m_nSpace [0]	= nSpace1;
	m_nSpace [1]	= nSpace2;
	m_nSpace [2]	= 1;
	m_nSpace [3]	= 1;

	m_nHorzBearer	= nHBearer;
	m_nVertBearer	= nVBearer;
	m_nQuietZone	= nQuietzone;
	m_nFontWidth	= nFontWidth;

	m_nCheckSum = bChecksum;
}

CGTIN14Params::CGTIN14Params (int nMag, int nBarHeight, 
							  int nSpace1, int nSpace2, 
							  int nBar1, int nBar2,
							  int nHBearer,	int nVBearer, int nQuietzone,
							  bool bChecksum, int nFontWidth,
							  LPCTSTR lpszFont, int nSize, int nAlignment, bool bCustom)
:	CI2o5Params (nMag, nBarHeight, nSpace1, nSpace2, nBar1, nBar2, nHBearer, nVBearer, nQuietzone, bChecksum, nFontWidth, lpszFont, nSize, nAlignment, bCustom)
{
	m_nSymbology = kSymbologyGTIN14;
}

////////////////////////////////////////////////////////////////////////////////

CDataMatrixParams::CDataMatrixParams (int nSymbology, int nMag, int nWidthFactor, int nHeightFactor, 
									  LPCTSTR lpszFont, int nSize, int nFontWidth, bool bCustom)
:	CBarcodeParams (nSymbology, 0, 0, 0, 0, nMag, 0, 0, 0, lpszFont, nSize, bCustom)
{
	ASSERT (nSymbology == kSymbologyDataMatrixEcc200 || nSymbology == kSymbologyDataMatrixGs1 || nSymbology == kSymbologyQR);
	m_nBar [0]		= nWidthFactor;
	m_nBar [1]		= nHeightFactor;

	m_nBar [2] = m_nBar [3] = 0;
	
	for (int i = 0; i < ARRAYSIZE (m_nSpace); i++)
		m_nSpace [i] = 0;

	m_nFontWidth	= nFontWidth;
	m_nAlignment	= kCapAlignOff;
	m_nBarHeight	= nHeightFactor;
	m_nBarWidth		= nWidthFactor;
}

////////////////////////////////////////////////////////////////////////////////

CQrParams::CQrParams (int nMag, int nWidthFactor, int nHeightFactor, bool bCustom)
:	CDataMatrixParams (kSymbologyQR, nMag, nWidthFactor, nHeightFactor, m_lpszDefFont, 16, 0, bCustom)
{
	SetVersion (0);
	SetLevel (QR_ECLEVEL_L);
	SetHint (QR_MODE_8);
	SetCaseSensitive (true);
}

////////////////////////////////////////////////////////////////////////////////

CBarcodeParams::CBarcodeParams (int nSymbology, int nBarWidth, int nBarHeight, 
								int nRatio, int nCheckSum, int nMagPct, 
								int nHBearer, int nVBearer, int nQueitzone,
								LPCTSTR lpszFont, int nFontSize,
								bool bCustom)
:	m_lID			(-1),
	m_nSymbology	(nSymbology),
	m_nBarWidth		(nBarWidth),
	m_nBarHeight	(nBarHeight),
	m_nRatio		(nRatio),
	m_nCheckSum		(nCheckSum),
	m_nMagPct		(nMagPct),
	m_nHorzBearer	(nHBearer),
	m_nVertBearer	(nVBearer),
	m_nQuietZone	(nQueitzone),
	m_strFont		(lpszFont), 
	m_nSize			(nFontSize),
    m_nStyle		(kCapStyleNormal), 
	m_nAlignment	(kCapAlignBelowCenter),
	m_nFontWidth	(0),
	m_bCustom		(bCustom),
	m_orient		(NORMAL),
	m_nEncoding		(0),
	m_lLineID		(-1),
	m_bOptimizeFNC1	(true)
{
	m_nBar [0] = m_nSpace [0] = 4;
	m_nBar [1] = m_nSpace [1] = 8;
	m_nBar [2] = m_nSpace [2] = 12;
	m_nBar [3] = m_nSpace [3] = 16;

	m_qr.m_nVersion			= 0;
	m_qr.m_level			= QR_ECLEVEL_L;
	m_qr.m_hint				= QR_MODE_8;
	m_qr.m_bCaseSensitive	= true;
}

CBarcodeParams::CBarcodeParams (const CBarcodeParams & rhs)
:	m_lID					(rhs.m_lID),
	m_nSymbology			(rhs.m_nSymbology),
	m_nBarWidth				(rhs.m_nBarWidth),
	m_nBarHeight			(rhs.m_nBarHeight),
	m_nRatio				(rhs.m_nRatio),
	m_nCheckSum				(rhs.m_nCheckSum),
	m_nMagPct				(rhs.m_nMagPct),
	m_nHorzBearer			(rhs.m_nHorzBearer),
	m_nVertBearer			(rhs.m_nVertBearer),
	m_nQuietZone			(rhs.m_nQuietZone),
	m_strFont				(rhs.m_strFont),
	m_nSize					(rhs.m_nSize),
	m_nStyle				(rhs.m_nStyle),
	m_nAlignment			(rhs.m_nAlignment),
	m_nFontWidth			(rhs.m_nFontWidth),
	m_bCustom				(rhs.m_bCustom),
	m_orient				(rhs.m_orient),
	m_nEncoding				(rhs.m_nEncoding),
	m_lLineID				(rhs.m_lLineID)
{
	for (int i = 0; i < 4; i++) {
		m_nBar [i]		= rhs.m_nBar [i];
		m_nSpace [i]	= rhs.m_nSpace [i];
	}

	m_qr.m_nVersion			= rhs.m_qr.m_nVersion;
	m_qr.m_level			= rhs.m_qr.m_level;
	m_qr.m_hint				= rhs.m_qr.m_hint;
	m_qr.m_bCaseSensitive	= rhs.m_qr.m_bCaseSensitive;
}

CBarcodeParams & CBarcodeParams::operator = (const CBarcodeParams & rhs)
{
	if (this != &rhs) {
		m_lID					= rhs.m_lID;
		m_nSymbology			= rhs.m_nSymbology;
		m_nBarWidth				= rhs.m_nBarWidth;
		m_nBarHeight			= rhs.m_nBarHeight;
		m_nRatio				= rhs.m_nRatio;
		m_nCheckSum				= rhs.m_nCheckSum;
		m_nMagPct				= rhs.m_nMagPct;
		m_nHorzBearer			= rhs.m_nHorzBearer;
		m_nVertBearer			= rhs.m_nVertBearer;
		m_nQuietZone			= rhs.m_nQuietZone;
		m_strFont				= rhs.m_strFont;
		m_nSize					= rhs.m_nSize;
		m_nStyle				= rhs.m_nStyle;
		m_nAlignment			= rhs.m_nAlignment;
		m_nFontWidth			= rhs.m_nFontWidth;
		m_bCustom				= rhs.m_bCustom;
		m_orient				= rhs.m_orient;
		m_nEncoding				= rhs.m_nEncoding;
		m_lLineID				= rhs.m_lLineID;
		m_qr.m_nVersion			= rhs.m_qr.m_nVersion;
		m_qr.m_level			= rhs.m_qr.m_level;
		m_qr.m_hint				= rhs.m_qr.m_hint;
		m_qr.m_bCaseSensitive	= rhs.m_qr.m_bCaseSensitive;

		for (int i = 0; i < 4; i++) {
			m_nBar [i]		= rhs.m_nBar [i];
			m_nSpace [i]	= rhs.m_nSpace [i];
		}
	}

	return * this;
}


CBarcodeParams::~CBarcodeParams()
{

}


CString CBarcodeParams::ToString (const CVersion & ver) const
{
	CString str;

	str.Format (_T ("{%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d}"),
		::strPrefix,
		m_lID,
		m_nSymbology,
		m_nBarWidth,	
		m_nBarHeight,
		m_nRatio,	
		(m_nSymbology != kSymbologyUPCA) ? m_nCheckSum : (m_nCheckSum ? 0 : 1),	
		m_nMagPct,
		m_nHorzBearer,
		m_nVertBearer,
		m_nQuietZone,
		m_bCustom,
		m_strFont,
		m_nSize,
		m_nStyle,
		m_nAlignment,
		m_nBar [0],
		m_nBar [1],
		m_nBar [2],
		m_nBar [3],
		m_nSpace [0],
		m_nSpace [1],
		m_nSpace [2],
		m_nSpace [3],
		m_nFontWidth,
		(int)m_orient,
		m_nEncoding,
		m_lLineID,
		m_qr.m_nVersion,
		(int)m_qr.m_level,
		(int)m_qr.m_hint,
		m_qr.m_bCaseSensitive,
		m_bOptimizeFNC1 ? 1 : 0);

	//TRACEF (str);
	return str;
}

bool CBarcodeParams::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	CStringArray vActual;
	CString strToken [] = {
		_T (""),					// [0] Prefix (required)
		_T ("-1"),					// [1] ID
		_T ("0"),					// [2] Symbology
		_T ("40"),					// [3] BarWidth	
		_T ("32"),					// [4] BarHeight
		_T ("20"),					// [5] Ratio
		_T ("0"),					// [6] CheckSum
		_T ("100"),					// [7] MagPct
		_T ("0"),					// [8] Horz bearer
		_T ("0"),					// [9] Vert bearer
		_T ("0"),					// [10] Quiet zone
		_T ("0"),					// [11] custom mag
		_T ("Arial"),				// [12] font name
		_T ("10"),					// [13]	font size
		_T ("0"),					// [14] style
		_T ("2"),					// [15] alignment
		_T ("4"),					// [16] bar 1
		_T ("8"),					// [17] bar 2
		_T ("12"),					// [18] bar 3
		_T ("16"),					// [19] bar 4
		_T ("4"),					// [20] space 1
		_T ("8"),					// [21] space 2
		_T ("12"),					// [22] space 3
		_T ("16"),					// [23] space 4
		_T ("0"),					// [24] avg font width
		_T ("0"),					// [25] orientation
		_T ("0"),					// [26] encoding (DataMatrix)
		_T ("-1"),					// [27] line ID
		_T ("0"),					// [28] m_qr.m_nVersion
		_T ("0"),					// [29] m_qr.m_level
		_T ("3"),					// [30] m_qr.m_hint
		_T ("1"),					// [31] m_qr.m_bCaseSensitive
		_T ("0"),					// [32] m_bOptimizeFNC1;
	};
	const int nFields = ARRAYSIZE (strToken);
	int i;
	
	Tokenize (str, vActual);

	if (vActual.GetSize () < 1) {
		TRACEF (_T ("Too few tokens"));
		return false;
	}

	for (i = 0; i < min (vActual.GetSize (), nFields); i++)
		if (vActual [i].GetLength ())
			strToken [i] = vActual [i];

	if (strToken [0].CompareNoCase (::strPrefix) != 0) {
		TRACEF ("Wrong prefix \"" + strToken [0] + "\"");
		return false;
	}

	int nSymbology = _ttoi (strToken [2]);
	
	m_bCustom = _ttoi (strToken [11]) ? true : false;

	bool bResult = true;


	bResult &= SetID			(_tcstoul (strToken [1], NULL, 10));
	bResult &= SetSymbology		(nSymbology);
	bResult &= SetBarWidth		(_ttoi (strToken [3]), nSymbology);
	bResult &= SetBarHeight		(_ttoi (strToken [4]));
	bResult &= SetRatio			(_ttoi (strToken [5]), nSymbology);
	bResult &= SetCheckSum		(_ttoi (strToken [6]));
	bResult &= SetMagPct		(_ttoi (strToken [7]), nSymbology);
	bResult &= SetHorzBearer	(_ttoi (strToken [8]));
	bResult &= SetVertBearer	(_ttoi (strToken [9]));
	bResult &= SetQuietZone		(_ttoi (strToken [10]));
	bResult &= SetFont			(strToken [12]);
	bResult &= SetSize			(_ttoi (strToken [13]));
	bResult &= SetStyle			(_ttoi (strToken [14]));
	bResult &= SetAlignment		(_ttoi (strToken [15]));
	bResult &= SetBarPixels		(1, _ttoi (strToken [16]));
	bResult &= SetBarPixels		(2, _ttoi (strToken [17]));
	bResult &= SetBarPixels		(3, _ttoi (strToken [18]));
	bResult &= SetBarPixels		(4, _ttoi (strToken [19]));
	bResult &= SetSpacePixels	(1, _ttoi (strToken [20]));
	bResult &= SetSpacePixels	(2, _ttoi (strToken [21]));
	bResult &= SetSpacePixels	(3, _ttoi (strToken [22]));
	bResult &= SetSpacePixels	(4, _ttoi (strToken [23]));
	bResult &= SetFontWidth		(_ttoi (strToken [24]));
	bResult &= SetOrientation	((FONTORIENTATION)_ttoi (strToken [25]));
	bResult &= SetEncoding		(_ttoi (strToken [26]));
	bResult &= SetLineID		(_ttoi (strToken [27]));

	m_qr.m_nVersion			= _ttoi (strToken [28]);
	m_qr.m_level			= (QRecLevel)_ttoi (strToken [29]);
	m_qr.m_hint				= (QRencodeMode)_ttoi (strToken [30]);
	m_qr.m_bCaseSensitive	= _ttoi (strToken [31]);

	m_bOptimizeFNC1			= _ttoi (strToken [32]);

	if (nSymbology == kSymbologyUPCA) 
		m_nCheckSum = m_nCheckSum ? 0 : 1;

	return bResult;
}


void CBarcodeParams::FromString (CArray <CBarcodeParams, CBarcodeParams &> & v, const CString & str, 
						const FoxjetCommon::CVersion & ver)
{
	CStringArray vPackets;

	ParsePackets (str, vPackets);

	for (int i = 0; i < vPackets.GetSize (); i++) {
		CBarcodeParams b;

		if (b.FromString (vPackets [i], ver))
			v.Add (b);
	}
}

ULONG CBarcodeParams::GetID () const
{
	return m_lID;
}

bool CBarcodeParams::SetID (ULONG lID)
{
	m_lID = lID;
	return true;
}

int CBarcodeParams::GetSymbology () const
{
	return m_nSymbology;
}

bool CBarcodeParams::SetSymbology (int n) 
{
	if (n == 17)
		n++;

	if (CBarcodeElement::IsValidSymbology (n)) {
		m_nSymbology = n; 
		return true;
	}

	return false;
}

int CBarcodeParams::GetBarWidth () const
{
	return m_nBarWidth;
}

bool CBarcodeParams::SetBarWidth (int n, int nSymbology) 
{
	if (CBarcodeElement::IsValidBarWidth (nSymbology, n)) {
		m_nBarWidth = n;
		return true;
	}

	return false;
}

int CBarcodeParams::GetBarHeight () const
{
	return m_nBarHeight; 
}

bool CBarcodeParams::SetBarHeight (int n) 
{
	bool bValid = IsValid (n, CBarcodeElement::m_lmtBarHeight);

	if (GetOrientation () == VERTICAL)
		bValid = true;

	if (bValid) {
		m_nBarHeight = n;
		return true;
	}

	return false;
}

int CBarcodeParams::GetRatio () const
{
	return m_nRatio;
}

bool CBarcodeParams::SetRatio (int n, int nSymbology) 
{
	if (CBarcodeElement::IsValidRatio (nSymbology, n)) {
		m_nRatio = n;
		return true;
	}

	return false;
}

int CBarcodeParams::GetCheckSum () const
{
	return m_nCheckSum;
}

bool CBarcodeParams::SetCheckSum (int n) 
{
	m_nCheckSum = n;
	return true;
}

int CBarcodeParams::GetMagPct () const
{
	return m_nMagPct;
}

bool CBarcodeParams::SetMagPct (int n, int nSymbology) 
{
	if (CBarcodeElement::IsValidMagnification (nSymbology, n)) {
		m_nMagPct = n;
		return true;
	}

	return false;
}

int CBarcodeParams::GetHorzBearer () const
{
	return m_nHorzBearer;
}

bool CBarcodeParams::SetHorzBearer (int n)
{
	bool bValid = IsValid (n, m_lmtBearer);

	if (GetOrientation () == VERTICAL)
		bValid = true;

	if (bValid) {
		m_nHorzBearer = n;
		return true;
	}

	return false;
}

int CBarcodeParams::GetVertBearer () const
{
	return m_nVertBearer;
}

bool CBarcodeParams::SetVertBearer (int n)
{
	bool bValid = IsValid (n, m_lmtBearer);

	if (GetOrientation () == VERTICAL)
		bValid = true;

	if (bValid) {
		m_nVertBearer = n;
		return true;
	}

	return false;
}

int	CBarcodeParams::GetQuietZone () const
{
	return m_nQuietZone;
}

bool CBarcodeParams::SetQuietZone (int n)
{
	bool bValid = IsValid (n, m_lmtQuietZone);

	if (GetOrientation () == VERTICAL)
		bValid = true;

	if (bValid) {
		m_nQuietZone = n;
		return true;
	}

	return false;
}

void CBarcodeParams::SetCustom (bool b)
{
	m_bCustom = b;
}

bool CBarcodeParams::IsCustom () const
{
	return m_bCustom;
}

CString CBarcodeParams::GetFont () const
{
	return m_strFont;
}

bool CBarcodeParams::SetFont (const CString & str)
{
	m_strFont = str;
	return true;
}

int CBarcodeParams::GetSize () const
{
	return m_nSize;
}

bool CBarcodeParams::SetSize (int n)
{
	m_nSize = n;
	return true;
}

int	CBarcodeParams::GetStyle () const
{
	return m_nStyle;
}

bool CBarcodeParams::SetStyle (int n)
{
	m_nStyle = n;
	return true;
}

int	CBarcodeParams::GetAlignment () const
{
	return m_nAlignment;
}

bool CBarcodeParams::SetAlignment (int n)
{
	m_nAlignment = n;
	return true;
}

void CBarcodeParams::ClipTo (HEADTYPE type)
{
	if (GetOrientation () == VERTICAL)
		return;

	int nHeight = GetBarHeight ();
	int nMin = (int)CBarcodeElement::m_lmtBarHeight.m_dwMin;

	switch (type) {
	case UR2:
	case GRAPHICS_768_256:
	case GRAPHICS_768_256_L310:
		nHeight = BindTo (nHeight, nMin, 256);
		break;
	case GRAPHICS_384_128:
		nHeight = BindTo (nHeight, nMin, 128);
		break;
	case UR4:
		nHeight = BindTo (nHeight, nMin, 384);
		break;
	default:
		nHeight = BindTo (nHeight, nMin, 32);
		break;
	}

	SetBarHeight (nHeight);
}

int	CBarcodeParams::GetBarPixels (int nBar) const
{
	if (nBar >= 1 && nBar <= 4)
		return m_nBar [nBar - 1];

	return 0; //1;
}

bool CBarcodeParams::SetBarPixels (int nBar, int n)
{
	if (nBar >= 1 && nBar <= 4) {
		if (m_nSymbology == kSymbologyDataMatrixEcc200 || m_nSymbology == kSymbologyDataMatrixGs1 || m_nSymbology == kSymbologyQR) {
			if (nBar == 1) {
				if (n >= (int)CBarcodeElement::m_lmtWidthFactor.m_dwMin && n <= (int)CBarcodeElement::m_lmtWidthFactor.m_dwMax) {
					m_nBar [nBar - 1] = n;
					return true;
				}
			}
			else if (nBar == 2) {
				if (n >= (int)CBarcodeElement::m_lmtHeightFactor.m_dwMin && n <= (int)CBarcodeElement::m_lmtHeightFactor.m_dwMax) {
					m_nBar [nBar - 1] = n;
					return true;
				}
			}
			else {
				m_nBar [nBar - 1] = n;
				return true;
			}
		}
		else {
			if (n >= (int)m_lmtBar.m_dwMin && n <= (int)m_lmtBar.m_dwMax) {
				m_nBar [nBar - 1] = n;
				return true;
			}
		}
	}

	return false;
}

int	CBarcodeParams::GetSpacePixels (int nBar) const
{
	if (nBar >= 1 && nBar <= 4)
		return m_nSpace [nBar - 1];

	return 0; //1;
}

bool CBarcodeParams::SetSpacePixels (int nBar, int n)
{
	if (nBar >= 1 && nBar <= 4) {
		if (m_nSymbology == kSymbologyDataMatrixEcc200 || m_nSymbology == kSymbologyDataMatrixGs1 || m_nSymbology == kSymbologyQR) {
			m_nSpace [nBar - 1] = n;
			return true;
		}
		else {
			if (n >= (int)m_lmtBar.m_dwMin && n <= (int)m_lmtBar.m_dwMax) {
				m_nSpace [nBar - 1] = n;
				return true;
			}
		}
	}

	return false;
}

int CBarcodeParams::GetFontWidth () const
{
	return m_nFontWidth;
}

bool CBarcodeParams::SetFontWidth (int n)
{
	m_nFontWidth = n;
	return true;
}

bool CBarcodeParams::SetOrientation (FONTORIENTATION orient)
{
	m_orient = orient;
	return true;
}

FONTORIENTATION CBarcodeParams::GetOrientation () const
{
	return m_orient;
}

int CBarcodeParams::GetEncoding () const
{
	return m_nEncoding;
}

bool CBarcodeParams::SetEncoding (int nEncoding)
{
	if (nEncoding >= 0 && nEncoding < 6) {
		m_nEncoding = nEncoding;
		return true;
	}

	return false;
}

