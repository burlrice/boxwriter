// AiListDlg.cpp: implementation of the CAiListDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AiListDlg.h"
#include "Debug.h"

using namespace FoxjetCommon;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CAiListDlg::CAiListDlg(CWnd* pParent /*=NULL*/)
	: CSubElementListDlg(IDD_EDITAI, pParent)
{
	//{{AFX_DATA_INIT(CAiListDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CAiListDlg::CAiListDlg(UINT nIDTemplate, CWnd* pParent)
: CSubElementListDlg (nIDTemplate, pParent)
{
}

void CAiListDlg::DoDataExchange(CDataExchange* pDX)
{
	CSubElementListDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAiListDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAiListDlg, CSubElementListDlg)
	//{{AFX_MSG_MAP(CAiListDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAiListDlg message handlers

BOOL CAiListDlg::OnInitDialog()
{
	LVCOLUMN lvc;
	TCHAR szCol [100];

	lvc.mask = LVCF_TEXT;
	lvc.pszText = szCol;
	lvc.cchTextMax = 100;

	BOOL bResult = CSubElementListDlg::OnInitDialog ();

	if (m_lv->GetColumn (0, &lvc)) {
		_tcsncpy (szCol, LoadString (IDS_AI), 100);
		m_lv->SetColumn (0, &lvc);
	}

	if (m_lv->GetColumn (1, &lvc)) {
		_tcsncpy (szCol, LoadString (IDS_CONTENT), 100);
		m_lv->SetColumn (0, &lvc);
	}

	m_lv->SetColumnWidth (2, 0);
	m_lv->SetColumnWidth (3, 0);

	return bResult;
}

