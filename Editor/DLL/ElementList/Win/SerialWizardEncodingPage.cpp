#include "stdafx.h"
#include "SerialWizardPortsDlg.h"
#include "resource.h"
#include "Utils.h"
#include "WinMsg.h"
#include "Extern.h"
#include "Database.h"
#include "ComPropertiesDlg.h"
#include "Comm32.h"
#include "Debug.h"
#include "Parse.h"
#include "SerialSampleDlg.h"
#include "color.h"
#include <string>

using namespace FoxjetElements;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

IMPLEMENT_DYNAMIC(CSerialWizardEncodingPage, CWizardBasePage)


BEGIN_MESSAGE_MAP(CSerialWizardEncodingPage, CWizardBasePage)
END_MESSAGE_MAP()

CSerialWizardEncodingPage::CSerialWizardEncodingPage (CSerialWizardSheet * pParent)
:	m_nEncoding (0),
	CWizardBasePage (pParent, IDD_SERIAL_WIZARD_ENCODING)
{
}

CSerialWizardEncodingPage::~CSerialWizardEncodingPage ()
{
}

BOOL CSerialWizardEncodingPage::OnInitDialog()
{
	if (CSerialWizardSheet * pParent = DYNAMIC_DOWNCAST (CSerialWizardSheet, m_pdlgParent)) {
		SETTINGSSTRUCT s;
		FoxjetCommon::StdCommDlg::CCommItem comm;
		CString str;

		str.Format (_T ("COM%d"), pParent->m_pdlgPort->m_port.m_dwPort);

		GetSettingsRecord (ListGlobals::GetDB (), 0, str, s);
		comm.FromString (s.m_strData);

		if (CComboBox * p = (CComboBox *)GetDlgItem (CB_ENCODING)) {
			p->SetItemData (p->AddString (LoadString (IDS_NONE)), 0);
			p->SetItemData (p->AddString (_T ("1251")), 1251);
			p->SetItemData (p->AddString (_T ("1252")), 1252);
			p->SetItemData (p->AddString (_T ("GB2312")), 2312);			

			for (int i = 0; i < p->GetCount (); i++) {
				if (p->GetItemData (i) == (DWORD)comm.m_nEncoding) {
					p->SetCurSel (i);
					break;
				}
			}

			if (p->GetCurSel () == CB_ERR)
				p->SetCurSel (0);
		}
	}

	return CWizardBasePage::OnInitDialog ();
}

BOOL CSerialWizardEncodingPage::OnKillActive ()
{
	if (CComboBox * p = (CComboBox *)GetDlgItem (CB_ENCODING)) 
		m_nEncoding = p->GetItemData (p->GetCurSel ());

	return CWizardBasePage::OnKillActive ();
}