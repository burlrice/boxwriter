#if !defined(AFX_SUBELEMENTDLG_H__FE320561_8BCB_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_SUBELEMENTDLG_H__FE320561_8BCB_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SubElementDlg.h : header file
//

#include "SubElement.h"
#include "TextElement.h"
#include "List.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CSubElementDlg dialog


class CSubElementDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CSubElementDlg(const FoxjetElements::CSubElement &e, CWnd* pParent = NULL);   // standard constructor
	virtual ~CSubElementDlg ();

// Dialog Data
	//{{AFX_DATA(CSubElementDlg)
	CString	m_strData;
	CString	m_strDesc;
	CString	m_strID;
	CString	m_strMask;
	int		m_nType;
	//}}AFX_DATA

	bool	m_bAI;
	bool	m_bReadOnly;
	bool	m_bReadOnlyID;

	FoxjetCommon::CElementListArray * m_pAllLists;

	static const int m_nClassIDs [];

	const FoxjetElements::TextElement::CTextElement * GetElement (int nType = -1) const;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSubElementDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	FoxjetElements::TextElement::CTextElement * GetElement (int nType = -1);
	void AFXAPI DDV_SubElementID(CDataExchange *pDX, const CString & str,
		bool bAI, UINT nCtrlID);
	void AFXAPI DDV_SubElementData (CDataExchange* pDX, const CString & strData, 
		const CString & strMask, UINT nCtrlID, int nType);
	virtual void OnOK ();
	
	// Generated message map functions
	//{{AFX_MSG(CSubElementDlg)
	afx_msg void OnSelchangeElementtype();
	virtual BOOL OnInitDialog();
	afx_msg void OnProperties();
	afx_msg void OnBuildmask();
	//}}AFX_MSG

	ULONG m_lLineID;
	CArray <FoxjetElements::TextElement::CTextElement *, FoxjetElements::TextElement::CTextElement *> m_vSubElements;

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SUBELEMENTDLG_H__FE320561_8BCB_11D4_915E_00104BEF6341__INCLUDED_)
