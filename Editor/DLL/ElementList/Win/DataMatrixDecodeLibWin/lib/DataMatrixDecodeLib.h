
//#ifdef DATAMATRIXENCODEDLL_EXPORTS
//#define DATAMATRIXENCODEDLL_API __declspec(dllexport)
//#else
//#define DATAMATRIXENCODEDLL_API __declspec(dllimport)
//#endif

#include <windows.h>

BOOL __stdcall _DataMatrixDecodeImageFile(LPCTSTR lpImageFile, char *pResult, int *pSize,int nTimeOut);
BOOL __stdcall _DataMatrixDecodeBitmap(HBITMAP hImage, char *pResult, int *pSize,int nTimeOut);
//BOOL __stdcall _DataMatrixDecodeBitmapFile(char *pBmpFile, char *pResult, int *pSize,int nTimeOut);
BOOL __stdcall _DataMatrixDecodeBitmapFile(char *pBmpFile, char *pResult, int *pSize,int nThreshold,int nTimeOut);
HBITMAP __stdcall _LoadImageEx(LPCTSTR lpImageFile);
int __stdcall _GetFixedError();