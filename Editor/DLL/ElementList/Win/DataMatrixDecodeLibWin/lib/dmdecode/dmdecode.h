// dmdecode.h : main header file for the DMDECODE application
//

#if !defined(AFX_DMDECODE_H__AE2E2176_A8AC_40AB_917E_E87BDE2CB283__INCLUDED_)
#define AFX_DMDECODE_H__AE2E2176_A8AC_40AB_917E_E87BDE2CB283__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CDmdecodeApp:
// See dmdecode.cpp for the implementation of this class
//

class CDmdecodeApp : public CWinApp
{
public:
	CDmdecodeApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDmdecodeApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDmdecodeApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DMDECODE_H__AE2E2176_A8AC_40AB_917E_E87BDE2CB283__INCLUDED_)
