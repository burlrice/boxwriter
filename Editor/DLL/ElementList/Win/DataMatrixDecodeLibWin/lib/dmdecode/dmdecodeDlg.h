// dmdecodeDlg.h : header file
//

#if !defined(AFX_DMDECODEDLG_H__EE6A6F9A_B89B_4BDC_AC8F_63C375A8C2C6__INCLUDED_)
#define AFX_DMDECODEDLG_H__EE6A6F9A_B89B_4BDC_AC8F_63C375A8C2C6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CDmdecodeDlg dialog

class CDmdecodeDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CDmdecodeDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CDmdecodeDlg)
	enum { IDD = IDD_DMDECODE_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDmdecodeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDmdecodeDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DMDECODEDLG_H__EE6A6F9A_B89B_4BDC_AC8F_63C375A8C2C6__INCLUDED_)
