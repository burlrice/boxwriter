// SerialSampleDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SerialSampleDlg.h"
#include "afxdialogex.h"


// CSerialSampleDlg dialog

IMPLEMENT_DYNAMIC(CSerialSampleDlg, CDialogEx)

CSerialSampleDlg::CSerialSampleDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSerialSampleDlg::IDD, pParent)
{

}

CSerialSampleDlg::~CSerialSampleDlg()
{
}

void CSerialSampleDlg::DoDataExchange(CDataExchange* pDX)
{
	DDX_Text (pDX, TXT_DATA, m_strData);
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSerialSampleDlg, CDialogEx)
END_MESSAGE_MAP()


// CSerialSampleDlg message handlers
