#if !defined(AFX_DBFORMATDLG_H__8972B44E_DC59_4D90_AAE6_7E46523FB233__INCLUDED_)
#define AFX_DBFORMATDLG_H__8972B44E_DC59_4D90_AAE6_7E46523FB233__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DbFormatDlg.h : header file
//

#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CDbFormatDlg dialog

class CDbFormatDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CDbFormatDlg(ULONG lLineID, CWnd* pParent = NULL);   // standard constructor

	enum { DEFAULT = 0x00, INT = 0x01, FLOAT = 0x02, DATE_TIME = 0x04 };

// Dialog Data
	//{{AFX_DATA(CDbFormatDlg)
	int		m_nType;
	int		m_nLeading;
	int		m_nTailing;
	BOOL	m_bZeroes;
	//}}AFX_DATA

	const int m_lLineID;
	CString m_strFormat;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDbFormatDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();
	void UpdateUI();

	int		m_nRdoType;

	// Generated message map functions
	//{{AFX_MSG(CDbFormatDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDefault();
	afx_msg void OnBuild();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DBFORMATDLG_H__8972B44E_DC59_4D90_AAE6_7E46523FB233__INCLUDED_)
