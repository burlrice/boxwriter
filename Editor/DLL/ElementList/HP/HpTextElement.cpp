// TextElement.cpp: implementation of the CTextElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "list.h"
#include "HpTextElement.h"
#include "HpTextDlg.h"
#include "FieldDefs.h"
#include "Parse.h"
#include "Debug.h"
#include "Parse.h"
#include "Extern.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace HpElements;
using namespace FoxjetCommon;
using namespace FoxjetCommon::ElementFields;
using namespace FoxjetDatabase;
using namespace ListGlobals;

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace HpElementMenu
{
	using namespace HpElements;

#define DECLARE_CREATE(t) \
	FoxjetCommon::CBaseElement * CALLBACK Create##t (const FoxjetDatabase::HEADSTRUCT & head) \
	{ \
		return new ( ##t ) (head); \
	} \
	FoxjetCommon::CBaseElement * CALLBACK Copy##t (const FoxjetCommon::CBaseElement * pSrc) \
	{ \
		return new ( ##t ) (* ( ##t *)pSrc); \
	}

#define CREATEMAPENTRY(nType, lpszType, nMenuStrID, nMenuStatusStrID, classname, pfctEdit) \
	{ nType, lpszType, nMenuStrID, nMenuStatusStrID, Create##classname, Copy##classname, pfctEdit, }

	DECLARE_CREATE (CTextElement)
//	DECLARE_CREATE (CBitmapElement)
//	DECLARE_CREATE (CCountElement)
//	DECLARE_CREATE (CDateTimeElement)
//	DECLARE_CREATE (CExpDateElement)
//	DECLARE_CREATE (CUserElement)
//	DECLARE_CREATE (CShiftElement)
//	DECLARE_CREATE (CBarcodeElement)
//	DECLARE_CREATE (CDatabaseElement)
//	DECLARE_CREATE (CSerialElement)
//	DECLARE_CREATE (CLabelElement)

	typedef FoxjetCommon::CBaseElement * (CALLBACK * LPCREATEELEMENT) (const FoxjetDatabase::HEADSTRUCT & head);
	typedef FoxjetCommon::CBaseElement * (CALLBACK * LPCOPYELEMENT) (const FoxjetCommon::CBaseElement * pSrc);
	typedef bool (CALLBACK * LPEDITELEMENT) (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, CWnd * pParent, const CSize & pa, UNITS units, 
		bool bReadOnlyElement, bool bReadOnlyLocation, 
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);

	struct
	{
		ELEMENTTYPE			m_type;
		LPCTSTR				m_lpszType;
		UINT				m_nMenuStrID;
		UINT				m_nMenuStatusStrID;
		LPCREATEELEMENT		m_lpCreate;
		LPCOPYELEMENT		m_lpCopy;
		LPEDITELEMENT		m_lpEdit;
	} static const map [] = 
	{
		CREATEMAPENTRY (TEXT,				_T ("Text"),		IDS_TEXT,				IDS_MENUTEXT_,				CTextElement,		OnEditTextElement),
//		CREATEMAPENTRY (BMP,				_T ("Bitmap"),		IDS_BITMAP,				IDS_MENUBITMAP_,			CBitmapElement,		OnEditBitmapElement),
//		CREATEMAPENTRY (COUNT,				_T ("Count"),		IDS_COUNT,				IDS_MENUCOUNT_,				CCountElement,		OnEditCountElement),
//		CREATEMAPENTRY (DATETIME,			_T ("DateTime"),	IDS_DATETIME,			IDS_MENUDATETIME_,			CDateTimeElement,	OnEditDateTimeElement),
//		CREATEMAPENTRY (EXPDATE,			_T ("ExpDate"),		IDS_EXPDATE,			IDS_MENUEXPDATE_,			CExpDateElement,	OnEditExpDateElement),
//		CREATEMAPENTRY (USER,				_T ("User"),		IDS_USER,				IDS_MENUUSER_,				CUserElement,		OnEditUserElement),
//		CREATEMAPENTRY (SHIFTCODE,			_T ("Shift"),		IDS_SHIFT,				IDS_MENUSHIFT_,				CShiftElement,		OnEditShiftElement),
//		CREATEMAPENTRY (BARCODE,			_T ("Barcode"),		IDS_BARCODE,			IDS_MENUBARCODE_,			CBarcodeElement,	OnEditBarcodeElement),
//		CREATEMAPENTRY (DATABASE,			_T ("Database"),	IDS_DATABASE,			IDS_MENUDATABASE,			CDatabaseElement,	OnEditDatabaseElement),
//		CREATEMAPENTRY (SERIAL,				_T ("Serial"),		IDS_SERIAL,				IDS_MENUSERIAL_,			CSerialElement,		OnEditSerialElement),
//		CREATEMAPENTRY (LABEL,				_T ("Label"),		IDS_LABEL,				IDS_MENULABEL_,				CLabelElement,		OnEditLabelElement),
	};
	static const int nSize = (sizeof (map) / sizeof (map [0]));

	int GetIndex (int nType) {
		for (int i = 0; i < nSize; i++) 
			if ((int)map [i].m_type == nType)
				return i;

		return -1;
	}
}; //namespace HpElementMenu

////////////////////////////////////////////////////////////////////////////////////////////////////

ELEMENT_API bool HpElements::OnEditElement (FoxjetCommon::CBaseElement & e, 
											CWnd * pParent,  const CSize & pa, UNITS units,
											const FoxjetCommon::CElementList * pList, 
											bool bReadOnlyElement, bool bReadOnlyLocation, 
											FoxjetCommon::CElementListArray * pAllLists, FoxjetCommon::CElementArray * pUpdate)
{
	bool bResult = false;
	int nIndex = HpElementMenu::GetIndex (e.GetClassID ());

	if (nIndex != -1) {
		ASSERT (HpElementMenu::map [nIndex].m_lpEdit);

		bResult = (* HpElementMenu::map [nIndex].m_lpEdit) (e, pList, pParent, pa, units, 
			bReadOnlyElement, bReadOnlyLocation, pAllLists, pUpdate);
	}

	return bResult;
}

ELEMENT_API bool HpElements::IsValidElementType (int nType, const FoxjetDatabase::HEADSTRUCT & head)
{
	if (IsHpHead (head.m_nHeadType)) {
		switch (nType) {
		case TEXT:
			return true;
		}
	}

	return false;
}

ELEMENT_API FoxjetCommon::CBaseElement * HpElements::CreateElement (int nType, const FoxjetDatabase::HEADSTRUCT & head)
{
	CBaseElement * p = NULL;
	int nIndex = HpElementMenu::GetIndex (nType);

	if (nIndex != -1) {
		ASSERT (HpElementMenu::map [nIndex].m_lpCreate);

		p = (* HpElementMenu::map [nIndex].m_lpCreate) (head);

		ASSERT (p);
	}

	if (p)
		p->ClipTo (head);

	return p;
}

ELEMENT_API FoxjetCommon::CBaseElement * HpElements::CreateElement (HBITMAP hBmp, const FoxjetDatabase::HEADSTRUCT & head, const CString & strName)
{
	if (FoxjetDatabase::IsHpHead (head.m_nHeadType)) 
		return HpElements::CreateElement (hBmp, head, strName);

	CBaseElement * pResult = NULL;

	if (hBmp) {
/* TODO:
		const CBitmapElement & def = (CBitmapElement &)ListGlobals::defElements.GetElement (BMP);
		CString strDefFilePath = def.GetFilePath ();
		CString strDir = CBitmapElement::GetDefPath ();
		CString strPrefix = strName.GetLength () ? strName : _T ("Image");
		int nIndex = strDefFilePath.ReverseFind ((TCHAR)'\\');

		if (nIndex != -1)
			strDir = strDefFilePath.Left (nIndex + 1);

		CString strFilepath = CreateTempFilename (strDir, strPrefix, _T ("bmp"));
		
		if (SaveBitmap (hBmp, strFilepath)) {
			pResult = CreateElement (BMP, head);

			if (pResult) {
				CBitmapElement * pBmp = (CBitmapElement *)pResult;
				
				if (!pBmp->SetFilePath (strFilepath, head)) {
					delete pResult;
					pResult = NULL;
				}
			}
		}
*/
	}

	if (pResult)
		pResult->ClipTo (head);

	return pResult;
}

ELEMENT_API FoxjetCommon::CBaseElement * HpElements::CreateElement (const CString & str, const FoxjetDatabase::HEADSTRUCT & head)
{
//	const CBaseElement & def = ListGlobals::defElements.GetElement (TEXT);
	const CTextElement def = * (CTextElement *)CreateElement (TEXT, head); TRACEF (_T ("TODO: rem"));
	CBaseElement * pResult = HpElements::CopyElement (&def, &head, true);

	if (CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, pResult)) {
		p->SetDefaultData (str);
	}
	else {
		delete pResult;
		pResult = NULL;
	}

	if (pResult)
		pResult->ClipTo (head);

	return pResult;
}

ELEMENT_API FoxjetCommon::CBaseElement * HpElements::CopyElement (const FoxjetCommon::CBaseElement * pSrc, const FoxjetDatabase::HEADSTRUCT * pHead, bool bUseDefSize)
{
	CBaseElement * p = NULL;

	ASSERT (pSrc);

	int nType = pSrc->GetClassID ();
	int nIndex = HpElementMenu::GetIndex (nType);

	if (nIndex != -1) {
		ASSERT (HpElementMenu::map [nIndex].m_lpCopy);

		if (IsHpHead (pSrc->GetHead ().m_nHeadType))
			p = (* HpElementMenu::map [nIndex].m_lpCopy) (pSrc);
		else 
			p = (* HpElementMenu::map [nIndex].m_lpCreate) (pSrc->GetHead ());

		ASSERT (p);

		if (pHead)
			p->SetHead (* pHead);

		if (nType == BMP) {
		/* TODO:
			CBitmapElement * pBmp = (CBitmapElement *)p;

			if (pHead && bUseDefSize) {
				double dStretch [2] = { 1, 1 };
				CSize size = CBitmapElement::GetSize (pBmp->GetFilePath (), * pHead);

				size = CBaseElement::LogicalToThousandths (size, * pHead);

				CBaseElement::CalcStretch (* pHead, dStretch);

				size.cy = (int) ((double)size.cy * dStretch [1]);
				pBmp->SetSize (size, * pHead);
			}
		*/
		}
	}

	if (p && pHead)
		p->ClipTo (* pHead);

	return p;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool ELEMENT_API CALLBACK HpElements::OnEditTextElement (CBaseElement & element, 
														 const FoxjetCommon::CElementList * pList, 
														 CWnd * pParent, const CSize & pa, UNITS units,
														 bool bReadOnlyElement, bool bReadOnlyLocation,
														 FoxjetCommon::CElementListArray * pAllLists,
														 FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == TEXT);
	CTextElement & e = * (CTextElement *)&element;
	bool bStatus = false;
	HpElements::CTextDlg dlg (e, pa, pList, pParent);
	CWinApp * pApp = ::AfxGetApp ();
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;

	dlg.m_bReadOnly = bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units = units;

	dlg.m_pt			= e.GetPos (pHead);
	dlg.m_nID			= e.GetID ();
	dlg.m_bFlipH		= e.IsFlippedH ();
	dlg.m_bFlipV		= e.IsFlippedV ();
	dlg.m_bInverse		= e.IsInverse ();
	dlg.m_strText		= e.GetDefaultData ();
//	dlg.m_nOrientation	= e.GetOrientation ();
//	dlg.m_nWidth		= e.GetWidth ();
//	dlg.m_align			= e.GetParaAlign ();
//	dlg.m_nParagraphGap	= e.GetParaGap ();
	dlg.m_pAllLists		= pAllLists;

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), dlg.m_bPreview);

		e.SetPos (dlg.m_pt, pHead);
		e.SetID (dlg.m_nID);
//		e.SetFlippedH (dlg.m_bFlipH ? true : false);
//		e.SetFlippedV (dlg.m_bFlipV ? true : false);
//		e.SetInverse (dlg.m_bInverse ? true : false);
		e.SetDefaultData (dlg.m_strText);
//		e.SetFontName (dlg.m_strFont);
//		e.SetBold (dlg.m_lBold);
//		e.SetOrientation (dlg.m_nOrientation);
//		e.SetWidth (dlg.m_nWidth);
//		e.SetParaAlign (dlg.m_align);
//		e.SetParaGap (dlg.m_nParagraphGap);

		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bStatus = true;
	}

	if (dlg.IsDefault ())
		defElements.SetElement (element.GetClassID (), element.ToString (verApp), verApp);

	return bStatus;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

const FoxjetCommon::LIMITSTRUCT CTextElement::m_lmtFontSize			= { 5, 256					};
const FoxjetCommon::LIMITSTRUCT CTextElement::m_lmtString			= { 1,	lmtString.m_dwMax	};

IMPLEMENT_DYNAMIC (CTextElement, CBaseTextElement);

CTextElement::CTextElement (const FoxjetDatabase::HEADSTRUCT & head)
:	m_size (0, 0),
	CBaseTextElement (head)
{
}

CTextElement::CTextElement (const CTextElement & rhs)
:	m_fnt (rhs.m_fnt),
	m_strDefaultData (rhs.m_strDefaultData),
	m_strImageData (rhs.m_strImageData),
	m_size (rhs.m_size),
	CBaseTextElement (rhs)
{
}

CTextElement & CTextElement::operator = (const CTextElement & rhs)
{
	CBaseTextElement::operator = (rhs);

	if (this != &rhs) {
		m_fnt				= rhs.m_fnt;
		m_strDefaultData	= rhs.m_strDefaultData;
		m_strImageData		= rhs.m_strImageData;
		m_size				= rhs.m_size;
	}

	return * this;
}

CTextElement::~CTextElement ()
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////

static const ElementFields::FIELDSTRUCT fields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType,	_T ("")				),	// Prefix (required)
	ElementFields::FIELDSTRUCT (m_lpszID,			_T ("")				),	// ID (required)
	ElementFields::FIELDSTRUCT (m_lpszX,			_T ("")				),	// x (required)
	ElementFields::FIELDSTRUCT (m_lpszY,			_T ("")				),	// y (required)
	ElementFields::FIELDSTRUCT (m_lpszFlipH,		_T ("0")			),	// flip h
	ElementFields::FIELDSTRUCT (m_lpszFlipV,		_T ("0")			),	// flip v
	ElementFields::FIELDSTRUCT (m_lpszInverse,		_T ("0")			),	// inverse
	ElementFields::FIELDSTRUCT (m_lpszFontName,		_T ("Courier New")	),	// font name
	ElementFields::FIELDSTRUCT (m_lpszFontSize,		_T ("128")			),	// font size
	ElementFields::FIELDSTRUCT (m_lpszBold,			_T ("0")			),	// bold
	ElementFields::FIELDSTRUCT (m_lpszItalic,		_T ("0")			),	// italic
	ElementFields::FIELDSTRUCT (m_lpszData,			_T ("Text Element")	),	// data
//	ElementFields::FIELDSTRUCT (m_lpszOrient,		_T ("0")			),	// orientation
//	ElementFields::FIELDSTRUCT (m_lpszWidth,		_T ("1")			),	// font width
//	ElementFields::FIELDSTRUCT (m_lpszParaAlign,	_T ("0")			),	// paragraph alignment
//	ElementFields::FIELDSTRUCT (m_lpszParaGap,		_T ("0")			),	// paragraph gap
	ElementFields::FIELDSTRUCT (),
};

const FoxjetCommon::ElementFields::FIELDSTRUCT * CTextElement::GetFieldBuffer () const
{
	return ::fields;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

CString CTextElement::ToString (const CVersion & ver) const
{
	CStringArray v;
	
	v.Add (_T ("Text"));
	v.Add (FoxjetDatabase::ToString ((ULONG)GetID ()));
	v.Add (FoxjetDatabase::ToString (GetPos (NULL).x));
	v.Add (FoxjetDatabase::ToString (GetPos (NULL).y));
	v.Add (FoxjetDatabase::ToString (IsFlippedH ()));
	v.Add (FoxjetDatabase::ToString (IsFlippedV ()));
	v.Add (FoxjetDatabase::ToString (IsInverse ()));
	v.Add (m_fnt.m_strName);									
	v.Add (FoxjetDatabase::ToString ((int)m_fnt.m_nSize));		
	v.Add (FoxjetDatabase::ToString ((int)m_fnt.m_bBold));		
	v.Add (FoxjetDatabase::ToString ((int)m_fnt.m_bItalic));	
	v.Add (GetDefaultData ());									

	return FoxjetDatabase::ToString (v);
}

bool CTextElement::FromString (const CString & str, const CVersion & ver)
{
	CStringArray vstrToken;
	CPrinterFont f;

	Parse (str, ver, vstrToken);

	if (vstrToken [0].CompareNoCase (_T ("Text")) != 0){
		TRACEF ("Wrong prefix \"" + vstrToken [0] + "\"");
		return false;
	}
	
	f.m_strName = GetParam (vstrToken, 7);
	f.m_nSize	= (UINT)_tcstol (GetParam (vstrToken, 8), NULL, 10);
	f.m_bBold	= _ttoi (GetParam (vstrToken, 9)) ? true : false;
	f.m_bItalic = _ttoi (GetParam (vstrToken, 10)) ? true : false;

	bool bResult = true;

	bResult &= SetID ((UINT)_tcstol (GetParam (vstrToken, 1), NULL, 10));
	bResult &= SetPos (CPoint (
		(UINT)_tcstol (GetParam (vstrToken, 2), NULL, 10), 
		(UINT)_tcstol (GetParam (vstrToken, 3), NULL, 10)), NULL);
	bResult &= SetFlippedH (_ttoi (GetParam (vstrToken, 4)) ? true : false);
	bResult &= SetFlippedV (_ttoi (GetParam (vstrToken, 5)) ? true : false);
	bResult &= SetInverse (_ttoi (GetParam (vstrToken, 6)) ? true : false);
	bResult &= SetFont (f);
	bResult &= SetDefaultData (UnformatString (GetParam (vstrToken, 11)));
//	bResult &= SetOrientation ((FONTORIENTATION)_tcstol (GetParam (vstrToken, 12), NULL, 10));
//	bResult &= SetWidth (_ttoi (GetParam (vstrToken, 13)));
//	bResult &= SetParaAlign ((ALIGNMENT_PARAGRAPH)_ttoi (GetParam (vstrToken, 14)));
//	bResult &= SetParaGap (_ttoi (GetParam (vstrToken, 15)));

	return bResult;
}

CSize CTextElement::Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly, DRAWCONTEXT context)
{
	CRect rc (0, 0, 0, 0);
	const CString strImageData = GetImageData ();
	CFont fnt;

	HFONT hFont = ::CreateFont (m_fnt.m_nSize, 
		m_fnt.m_nSize / 6, // TODO 
		0, 0, FW_NORMAL, FALSE, FALSE, FALSE,
		DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, m_fnt.m_strName); 
	VERIFY (fnt.Attach (hFont));
	CFont * pFont = dc.SelectObject (&fnt);

	dc.DrawText (strImageData, &rc, DT_CALCRECT | DT_NOPREFIX);
	rc.bottom = rc.top + m_fnt.m_nSize;
	CSize sizePrinter (m_fnt.m_nSize, rc.Width ());

	{
		double dStretch [2] = { 1, 1 };
		CSize size = CSize (rc.Width (), rc.Height ());

		CalcStretch (head, dStretch);
		size.cx = (int)((double)size.cx * dStretch [0]);
		m_size = CBaseElement::LogicalToThousandths (size, head);
	}

	if (!bCalcSizeOnly) {
		dc.DrawText (strImageData, &rc, DT_NOPREFIX);
	}

	dc.SelectObject (pFont);

	if (context == FoxjetCommon::PRINTER) 
		return sizePrinter;
	else
		return m_size;
}

int CTextElement::GetClassID () const
{
	return TEXT;
}

bool CTextElement::SetDefaultData (const CString &strData)
{
	if (IsValid (strData.GetLength (), m_lmtString)) {
		SetRedraw ();
		m_strDefaultData = strData;
		return true;
	}

	return false;
}

CString CTextElement::GetDefaultData () const
{
	return m_strDefaultData;
}

CString CTextElement::GetImageData () const
{
	return m_strImageData;
}

int CTextElement::Build (BUILDTYPE type, bool bCanThrowException)
{
	m_strImageData = m_strDefaultData;
	return GetClassID ();
}

bool CTextElement::SetFlippedH (bool bFlip)
{
	return true;
}

bool CTextElement::SetFlippedV (bool bFlip)
{
	return true;
}

bool CTextElement::SetInverse (bool bInverse)
{
	return true;
}

CSize CTextElement::GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI) const
{
	return m_size;
}

bool CTextElement::SetFont (const CPrinterFont & f)
{
	m_fnt = f;
	return true;
}

bool CTextElement::GetFont (CPrinterFont & f) const
{
	f = m_fnt;
	return true;
}

