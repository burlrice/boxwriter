#if !defined(__HPTEXTDLG__)
#define __HPTEXTDLG__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TextDlg.h : header file
//

#include "resource.h"
#include "ElementDlg.h"
#include "HpTextElement.h"

/////////////////////////////////////////////////////////////////////////////
// CTextDlg dialog

namespace HpElements
{
	class CTextDlg : public CElementDlg
	{
	// Construction
	public:
		CTextDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
			const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   
		CTextDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
			UINT nID, const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   


	// Dialog Data
		//{{AFX_DATA(CTextDlg)
		enum { IDD = IDD_HPTEXT };
		CString	m_strText;
		//}}AFX_DATA
		int m_nParagraphGap;

	//	HpElements::CTextElement::ALIGNMENT_PARAGRAPH m_align;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CTextDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

		virtual UINT GetDefCtrlID () const { return TXT_TEXT; }

	// Implementation
	protected:

		static ULONG CALLBACK CTextDlg::CharMapFunc (LPVOID lpData);
		void UpdateUI();

		bool m_bCharMapRunning;

		// Generated message map functions
		//{{AFX_MSG(CTextDlg)
		virtual BOOL OnInitDialog();
		//}}AFX_MSG

		afx_msg void OnCharMap ();

		virtual int BuildElement ();
		const HpElements::CTextElement & GetElement () const;
		HpElements::CTextElement & GetElement ();

		DECLARE_MESSAGE_MAP()
	};
}; //namespace HpElements

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(__HPTEXTDLG__)
