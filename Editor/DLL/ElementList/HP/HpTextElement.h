// TextElement.h: interface for the CTextElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXTELEMENT_H__CE2B11D3_B474_48CE_9C22_CA5E4617DA8B__INCLUDED_)
#define AFX_TEXTELEMENT_H__CE2B11D3_B474_48CE_9C22_CA5E4617DA8B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "BaseElement.h"

namespace HpElements
{
	using namespace FoxjetElements;

	const FoxjetCommon::LIMITSTRUCT lmtString	= { 0,	255 };

	ELEMENT_API bool IsValidElementType (int nType, const FoxjetDatabase::HEADSTRUCT & head);
	ELEMENT_API FoxjetCommon::CBaseElement * CreateElement (int nType, const FoxjetDatabase::HEADSTRUCT & head);
	ELEMENT_API FoxjetCommon::CBaseElement * CreateElement (HBITMAP hBmp, const FoxjetDatabase::HEADSTRUCT & head, const CString & strName);
	ELEMENT_API FoxjetCommon::CBaseElement * CreateElement (const CString & str, const FoxjetDatabase::HEADSTRUCT & head);
	ELEMENT_API FoxjetCommon::CBaseElement * CopyElement (const FoxjetCommon::CBaseElement * pSrc, const FoxjetDatabase::HEADSTRUCT * pHead, bool bUseDefSize);
	ELEMENT_API bool OnEditElement (FoxjetCommon::CBaseElement & e, CWnd * pParent,  const CSize & pa, UNITS units,
		const FoxjetCommon::CElementList * pList, bool bReadOnlyElement, bool bReadOnlyLocation, 
		FoxjetCommon::CElementListArray * pAllLists, FoxjetCommon::CElementArray * pUpdate);

	bool ELEMENT_API CALLBACK OnEditTextElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation, FoxjetCommon::CElementListArray * pAllLists, 
		FoxjetCommon::CElementArray * pUpdate);

	class ELEMENT_API CTextElement : public FoxjetCommon::CBaseTextElement
	{
		DECLARE_DYNAMIC (CTextElement);

	public:
		CTextElement (const FoxjetDatabase::HEADSTRUCT & head);
		CTextElement (const CTextElement & rhs);
		CTextElement & operator = (const CTextElement & rhs);
		virtual ~CTextElement ();

		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);
		virtual CSize Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly = false, DRAWCONTEXT context = EDITOR);
		virtual int GetClassID () const;
		virtual CString GetDefaultData () const;
		virtual CString GetImageData () const;
		virtual bool SetDefaultData (const CString &strData);
 		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;
	
		virtual int Build (BUILDTYPE type = INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual CSize GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI = true) const;
		virtual bool SetFont (const FoxjetCommon::CPrinterFont & f);
		virtual bool GetFont (FoxjetCommon::CPrinterFont & f) const;

		virtual bool SetFlippedH (bool bFlip);
		virtual bool SetFlippedV (bool bFlip);
		virtual bool SetInverse (bool bInverse);

		CSize m_size;
		FoxjetCommon::CPrinterFont m_fnt;
		CString m_strDefaultData;
		CString m_strImageData;

		static const FoxjetCommon::LIMITSTRUCT m_lmtFontSize;
		static const FoxjetCommon::LIMITSTRUCT m_lmtString;
	};
}

#endif // !defined(AFX_TEXTELEMENT_H__CE2B11D3_B474_48CE_9C22_CA5E4617DA8B__INCLUDED_)
