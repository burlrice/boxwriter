#include "stdafx.h"
#include "list.h"
#include "HpTextElement.h"
#include "HpTextDlg.h"
#include "Debug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace HpElements;

/////////////////////////////////////////////////////////////////////////////
// CTextDlg dialog


CTextDlg::CTextDlg(const CBaseElement & e, const CSize & pa, 
				   const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_bCharMapRunning (false),
	m_nParagraphGap (0),
	CElementDlg(e, pa, CTextDlg::IDD, pList, pParent)
{
	//{{AFX_DATA_INIT(CTextDlg)
	//}}AFX_DATA_INIT
}

CTextDlg::CTextDlg(const CBaseElement & e, const CSize & pa, UINT nID, 
				   const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_bCharMapRunning (false),
	m_nParagraphGap (0),
	CElementDlg(e, pa, nID, pList, pParent)
{
}


void CTextDlg::DoDataExchange(CDataExchange* pDX)
{
	CElementDlg::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CTextDlg)
	//}}AFX_DATA_MAP

	DDX_Text(pDX, TXT_TEXT, m_strText);
	DDV_MinMaxChars (pDX, m_strText, TXT_TEXT, CTextElement::m_lmtString);
//	DDX_Radio (pDX, RDO_ALIGNLEFT, (int &)m_align);
//
//	DDX_Text(pDX, TXT_GAP, m_nParagraphGap);
//	DDV_MinMaxInt (pDX, m_nParagraphGap, CTextElement::m_lmtParagraphGap.m_dwMin,  CTextElement::m_lmtParagraphGap.m_dwMax);
}


BEGIN_MESSAGE_MAP(CTextDlg, CElementDlg)
	//{{AFX_MSG_MAP(CTextDlg)
	//}}AFX_MSG_MAP
//	ON_BN_CLICKED (BTN_CHARMAP, OnCharMap)
	ON_EN_CHANGE (TXT_TEXT, UpdateUI)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTextDlg message handlers

BOOL CTextDlg::OnInitDialog() 
{
	CElementDlg::OnInitDialog();
	UpdateUI ();

	return FALSE;
}

int CTextDlg::BuildElement ()
{
	CTextElement & e = GetElement ();
	CString strText, strFont;
	
	GetDlgItemText (TXT_TEXT, strText);
//	GetDlgItemText (CB_FONT, strFont);

//	e.SetFontName (strFont);
//	e.SetBold (GetDlgItemInt (TXT_BOLD));
//	e.SetFlippedH (GetCheck (CHK_FLIPH));
//	e.SetFlippedV (GetCheck (CHK_FLIPV));
//	e.SetInverse (GetCheck (CHK_INVERSE));
//	e.SetDefaultData (strText);
//	e.SetOrientation (GetOrientation ());
//	e.SetWidth (GetDlgItemInt (TXT_WIDTH));

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

CTextElement & CTextDlg::GetElement ()
{
	CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CTextElement)));
	return (CTextElement &)e;
}

const CTextElement & CTextDlg::GetElement () const
{
	const CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CTextElement)));
	return (CTextElement &)e;
}

void CTextDlg::OnCharMap ()
{
	DWORD dw;

	HANDLE hThread = ::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0,
		(LPTHREAD_START_ROUTINE)CharMapFunc, (LPVOID)this, 0, &dw);

	ASSERT (hThread != NULL);
}

void CTextDlg::UpdateUI()
{
/*
	if (::IsWindow(m_hWnd)) {
		CString str;
		static const UINT nAlign [] = 
		{
			RDO_ALIGNLEFT,	
			RDO_ALIGNCENTER,
			RDO_ALIGNRIGHT,	
			TXT_GAP,
		};

		GetDlgItemText (TXT_TEXT, str);
		bool bParagraph = CTextElement::IsParagraphMode (str);

		for (int i = 0; i < ARRAYSIZE (nAlign); i++) {
			if (CButton * p = (CButton *)GetDlgItem (nAlign [i])) {
				//p->ShowWindow (bParagraph ? SW_SHOW : SW_HIDE);
				p->EnableWindow (bParagraph);
			}
		}

		GetDlgItem (BTN_CHARMAP)->EnableWindow (m_bCharMapRunning ? FALSE : TRUE);
		GetDlgItem (IDOK)->EnableWindow (m_bCharMapRunning ? FALSE : TRUE);
		GetDlgItem (IDCANCEL)->EnableWindow (m_bCharMapRunning ? FALSE : TRUE);
	}
*/
}

ULONG CALLBACK CTextDlg::CharMapFunc (LPVOID lpData)
{
/*
	ASSERT (lpData);

	CTextDlg & dlg = * (CTextDlg *)lpData;
	CString strApp = _tgetenv (_T ("SystemRoot")) + CString (_T ("\\System32\\charmap.exe"));
	CString strINI = _tgetenv (_T ("SystemRoot")) + CString (_T ("\\win.ini"));
	CString strFont = dlg.GetElement ().GetFontName ();
	CString strCmdLine = strApp;
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	char szCmdLine [0xFF];

	_tcsncpy (szCmdLine, strCmdLine, 0xFF);
    memset (&pi, 0, sizeof(pi));
    memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	VERIFY (::WritePrivateProfileString (_T ("MSUCE"), _T ("Advanced"), _T ("1"),		strINI));
	VERIFY (::WritePrivateProfileString (_T ("MSUCE"), _T ("CodePage"), _T ("Unicode"), strINI));
	VERIFY (::WritePrivateProfileString (_T ("MSUCE"), _T ("Font"),		strFont,		strINI));

  BOOL bResult = ::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi); 

	dlg.UpdateUI ();

	if (!bResult) {
		MsgBox (dlg, LoadString (IDS_FAILEDTOLAUNCHCHARMAP));
	}
	else {
		bool bMore;
		
		dlg.m_bCharMapRunning = true;

		TRACEF ("begin edit");

		do {
			DWORD dwExitCode = 0;
			
			dlg.UpdateUI ();

			if (!GetExitCodeProcess (pi.hProcess, &dwExitCode))
				bMore = false;
			else if (!::IsWindow (dlg.m_hWnd))
				bMore = false;
			else {
				bMore = (dwExitCode == STILL_ACTIVE);
				WaitForSingleObjectEx(pi.hProcess, 3000, TRUE);
				TRACEF ("still waiting");
			}
		}
		while (bMore);

		TRACEF ("edit complete");
	}

	dlg.m_bCharMapRunning = false;
	dlg.UpdateUI ();
*/

	return 0;
}

