// DateTimeElement.h: interface for the CDateTimeElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATETIMEELEMENT_H__D6F81C88_5EE6_4EF9_B190_D160961986E3__INCLUDED_)
#define AFX_DATETIMEELEMENT_H__D6F81C88_5EE6_4EF9_B190_D160961986E3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IpBaseElement.h"
#include "fj_element.h"
#include "fj_datetime.h"

FORWARD_DECLARE (fjtext, FJTEXT)
FORWARD_DECLARE (fjdatetime, FJDATETIME)

namespace FoxjetIpElements
{
	class ELEMENT_API CDateTimeElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CDateTimeElement);
	public:
		CDateTimeElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CDateTimeElement (const CDateTimeElement & rhs);
		CDateTimeElement & operator = (const CDateTimeElement & rhs);
		virtual ~CDateTimeElement ();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		virtual void CreateImage ();
		virtual CString GetElementFontName () const;
		virtual bool SetFont (const FoxjetCommon::CPrinterFont & f);

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		DECLARE_MEMBERPOINTERACCESS (LPFJDATETIME, GetElement ()->pDesc)

		virtual LONG GetBold () const;
		virtual bool SetBold (LONG lValue);

		virtual int GetWidth () const;
		virtual bool SetWidth (int nValue);
		
		LONG GetGap () const;
		bool SetGap (LONG lValue);

		FJDTFORMAT GetFormat () const;
		bool SetFormat (FJDTFORMAT fmt);

		FJDTTYPE GetTimeType () const;
		bool SetTimeType (FJDTTYPE type);

		CHAR GetDelim () const;
		bool SetDelim (CHAR c);
		
		BOOL GetStringNormal () const;
		bool SetStringNormal (BOOL b);
		
		LONG GetHourACount () const;
		bool SetHourACount (LONG l);

		LONG GetLength () const;
		bool SetLength (LONG l);

		CHAR GetAlign () const;
		bool SetAlign (CHAR c);

		CHAR GetFill () const;
		bool SetFill (CHAR c);

		static const FoxjetCommon::LIMITSTRUCT m_lmtHourACount;
		static const FoxjetCommon::LIMITSTRUCT m_lmtLength;

	protected:
		LPFJTEXT GetText ();
		CLPFJTEXT GetText () const;
	};
}; //namespace FoxjetIpElements

#endif // !defined(AFX_DATETIMEELEMENT_H__D6F81C88_5EE6_4EF9_B190_D160961986E3__INCLUDED_)
