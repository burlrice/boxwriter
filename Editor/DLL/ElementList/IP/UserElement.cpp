// UserElement.cpp: implementation of the CUserElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UserElement.h"
#include "List.h"
#include "ElementDefaults.h"
#include "Edit.h"
#include "Debug.h"
#include "Extern.h"
#include "Database.h"
#include "Utils.h"
#include "fj_export.h"
#include "Parse.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace FoxjetIpElements;
using namespace ListGlobals;

IMPLEMENT_DYNAMIC (CUserElement, CTextElement);

const FoxjetCommon::LIMITSTRUCT CUserElement::m_lmtString = { 1,	100 };

CUserElement::CUserElement(LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head)
:	m_strPrompt (_T ("Enter user data: ")), 
	m_bPromptAtTaskStart (true),
	m_nMaxChars (15),
	CTextElement (pHead, head)
{
 	SetDefaultData (_T ("User element"));
}

CUserElement::CUserElement (const CUserElement & rhs)
:	m_strPrompt (rhs.m_strPrompt),
	m_bPromptAtTaskStart (rhs.m_bPromptAtTaskStart),
	m_nMaxChars (rhs.m_nMaxChars),
	CTextElement (rhs)
{
 	SetDefaultData (rhs.m_strDefaultData);
}

CUserElement & CUserElement::operator = (const CUserElement & rhs)
{
	CTextElement::operator = (rhs);

	if (this != & rhs) {
	 	SetDefaultData (rhs.m_strDefaultData);
		m_strPrompt				= rhs.m_strPrompt;
		m_bPromptAtTaskStart	= rhs.m_bPromptAtTaskStart;
		m_nMaxChars				= rhs.m_nMaxChars;
	}

	return * this;
}

CUserElement::~CUserElement()
{

}

CString CUserElement::ToString (const FoxjetCommon::CVersion & ver, int nType) 
{
	switch (nType) {
	case TEXT:
		Build ();
		Draw (* ::AfxGetMainWnd ()->GetDC (), FoxjetDatabase::GetDefaultHead (), true);
		return CTextElement::ToString (ver);
	case USER:	
		return ToString (ver);
	}

	ASSERT (IsKindOf (defElements.GetElement (nType).GetRuntimeClass ()));
	return "";
}

CString CUserElement::ToString (const FoxjetCommon::CVersion & ver) const
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated

	CString str;
	CPrinterFont f;
	
	GetFont (f);

	float x = (float)GetPos (&m_head).x / 1000.0F;

	str.Format (_T ("{User,%u,%f,%u,%d,%d,%d,%s,%u,%d,%d,%s,%d,%s,%d}"),
		GetID (),
		x, GetPos (NULL).y,
		IsFlippedH (),
		IsFlippedV (),
		IsInverse (),
		f.m_strName, 
		GetBold (),
		GetWidth (),
		GetGap (),
		FormatString (GetDefaultData ()),
		m_nMaxChars,
		FormatString (m_strPrompt),
		m_bPromptAtTaskStart);

	return str;
}

/*
{User,1,0.000000,16,0,0,0,us16x16,0,1,0,ONE,15,Enter user data: ,1}
{User,2,1.292000,16,1,0,0,us16x16,0,1,0,TWO,15,Enter user data: ,1}
{User,3,2.617000,16,0,1,0,us16x16,0,1,0,THREE,15,Enter user data: ,1}
{User,4,3.851000,16,1,1,0,us16x16,0,1,0,FOUR,15,Enter user data: ,1}
{User,5,5.189000,16,0,0,1,us16x16,0,1,0,FIVE,15,Enter user data: ,1}
{User,6,6.421000,16,0,0,0,us16x16,1,1,0,SIX,15,Enter user data: ,1}
*/

bool CUserElement::FromString (const CString & str, const FoxjetCommon::CVersion & ver)
{
	// NOTE: every time this is changed, Utils.dll (Convert.cpp) should be updated

	const int nFields = 15;
	CStringArray vActual, vDef;
	CPrinterFont f;
	CString strToken [nFields] = {
		_T (""),				// [0]	Prefix (required)
		_T (""),				// [1]	ID (required)
		_T (""),				// [2]	x (required)
		_T (""),				// [3]	y (required)
		_T ("0"),				// [4]	flip h
		_T ("0"),				// [5]	flip v
		_T ("0"),				// [6]	inverse
		_T ("us32x32"),			// [7]	font name
		_T ("0"),				// [8]	bold
		_T ("1"),				// [9]	width
		_T ("0"),				// [10]	gap
		_T ("User element"),	// [11]	data
		_T ("15"),				// [12]	max chars
		_T ("Enter user data"),	// [13]	prompt
		_T ("1"),				// [14]	prompt at task start
	};
	int i;

	Tokenize (str, vActual);

	if (vActual.GetSize () < 4) {
		TRACEF ("Too few tokens");
		return false;
	}

	Tokenize (ListGlobals::defElements.GetRegElementDefs (GetClassID (), ver), vDef);

	for (i = 1; i < min (vDef.GetSize (), nFields); i++)
		if (vDef [i].GetLength ())
			strToken [i] = vDef [i];

	for (i = 0; i < min (vActual.GetSize (), nFields); i++)
		if (vActual [i].GetLength ())
			strToken [i] = vActual [i];

	if (strToken [0].CompareNoCase (_T ("User")) != 0){
		TRACEF ("Wrong prefix \"" + strToken [0] + "\"");
		return false;
	}

	CString strTextElement = _T ("{");

	// must use CTextElement to parse position
	strTextElement += CString (_T ("Text"))					+ _T (",");	// [0]	Type
	strTextElement += _T ("\"") + strToken [1] + _T ("\"")  + _T (",");	// [1]	ID
	strTextElement += strToken [2]							+ _T (",");	// [2]	X
	strTextElement += strToken [3]							+ _T (",");	// [3]	Y
	strTextElement += CString (_T ("0"))					+ _T (",");	// [4]	Position
	strTextElement += strToken [8]							+ _T (",");	// [5]	Bold
	strTextElement += strToken [9]							+ _T (",");	// [6]	Width
	strTextElement += strToken [10]							+ _T (",");	// [7]	Gap
	strTextElement += strToken [4]							+ _T (",");	// [8]	FlipV
	strTextElement += strToken [5]							+ _T (",");	// [9]	FlipH
	strTextElement += strToken [6]							+ _T (",");	// [10]	Inverse
	strTextElement += _T ("\"") + strToken [7] + _T ("\"")	+ _T (",");	// [11]	Font
	strTextElement += _T ("\"")+ strToken [11]+ _T ("\"")	+ _T ("}");	// [12]	Data

	if (CTextElement::FromString (strTextElement, ver)) {
		m_strPrompt = UnformatString (strToken [13]);
		m_bPromptAtTaskStart = _ttoi (strToken [14]) ? true : false;

		return 
			SetFlippedH (_ttoi (strToken [4]) ? true : false) &&
			SetFlippedV (_ttoi (strToken [5]) ? true : false) &&
			SetInverse (_ttoi (strToken [6]) ? true : false) &&

			SetMaxChars ((UINT)_tcstol (strToken [12], NULL, 10)) && // note order
			SetDefaultData (UnformatString (strToken [11]));
	}

	return false;
}

bool CUserElement::SetDefaultData (const CString &strData)
{
	if (FoxjetCommon::IsValid (strData.GetLength (), CUserElement::m_lmtString)) 
		return CTextElement::SetDefaultData (strData);

	return false;
}

int CUserElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	m_strImageData = m_strDefaultData;
	CTextElement::Build ();
	return GetClassID ();
}

int CUserElement::GetMaxChars () const 
{
	return m_nMaxChars;
}

bool CUserElement::SetMaxChars (int nChars)
{
	if (FoxjetCommon::IsValid (nChars, CUserElement::m_lmtString)) {
		Invalidate ();
		m_nMaxChars = nChars;
		return true;
	}

	return false;
}

CString CUserElement::GetPrompt () const
{
	return m_strPrompt;
}

bool CUserElement::SetPrompt (const CString & str)
{
	if (str.GetLength () <= (int)m_lmtString.m_dwMax) {
		m_strPrompt = str;
		return true;
	}

	return false;
}

