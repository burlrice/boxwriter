#if !defined(AFX_EDITVALUEDLG_H__60449DC7_DB2B_4645_A274_55C7474D4BC0__INCLUDED_)
#define AFX_EDITVALUEDLG_H__60449DC7_DB2B_4645_A274_55C7474D4BC0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EditValueDlg.h : header file
//

#include "resource.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CEditValueDlg dialog

class CEditValueDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CEditValueDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEditValueDlg)
	enum { IDD = IDD_EDITVALUE };
	int		m_nIndex;
	CString	m_strValue;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditValueDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	static const FoxjetCommon::LIMITSTRUCT m_lmtString;

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditValueDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITVALUEDLG_H__60449DC7_DB2B_4645_A274_55C7474D4BC0__INCLUDED_)
