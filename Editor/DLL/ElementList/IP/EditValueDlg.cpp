// EditValueDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "EditValueDlg.h"
#include "fj_system.h"

using namespace FoxjetCommon;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditValueDlg dialog

const FoxjetCommon::LIMITSTRUCT CEditValueDlg::m_lmtString = { 1, FJDATETIME_STRING_SIZE };

CEditValueDlg::CEditValueDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(CEditValueDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditValueDlg)
	m_nIndex = 0;
	m_strValue = _T("");
	//}}AFX_DATA_INIT
}

void CEditValueDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditValueDlg)
	DDX_Text(pDX, TXT_INDEX, m_nIndex);
	//}}AFX_DATA_MAP

	DDX_Text(pDX, TXT_VALUE, m_strValue);
	DDV_MinMaxChars (pDX, m_strValue, TXT_VALUE, m_lmtString);
}


BEGIN_MESSAGE_MAP(CEditValueDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CEditValueDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditValueDlg message handlers
