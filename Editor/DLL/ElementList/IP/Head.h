// Head.h: interface for the CHead class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HEAD_H__218919D6_AB4C_41C3_8069_9231B288F66A__INCLUDED_)
#define AFX_HEAD_H__218919D6_AB4C_41C3_8069_9231B288F66A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "BaseElement.h"
#include "ElementApi.h"
#include "PrinterFont.h"
#include "Types.h"
#include "Database.h"

FORWARD_DECLARE (fjprinthead, FJPRINTHEAD);
FORWARD_DECLARE (fjfont, FJFONT);
FORWARD_DECLARE (fjmemstor, FJMEMSTOR);

namespace FoxjetCommon
{
	class ELEMENT_API CHead  
	{
	public:
		CHead(LPFJPRINTHEAD pHead);
		CHead (const CHead & rhs);
		CHead & operator = (const CHead & rhs);
		virtual ~CHead();

	public:
		bool GetFont (const CString & str, CPrinterFont & fnt) const;
		bool GetFont (const CString & str, LPFJFONT * pResult) const;
		bool GetFonts (CArray <CPrinterFont, CPrinterFont &> & v) const;

		operator LPFJPRINTHEAD () { return m_pFjHead; }
		operator const LPFJPRINTHEAD () const { return m_pFjHead; }
		LPFJPRINTHEAD operator -> () { return m_pFjHead; }
		const LPFJPRINTHEAD operator -> () const { return m_pFjHead; }

		operator FoxjetDatabase::HEADSTRUCT & () { return m_dbHead; }
		operator const FoxjetDatabase::HEADSTRUCT & () const { return m_dbHead; }

		LPFJPRINTHEAD m_pFjHead;
		FoxjetDatabase::HEADSTRUCT m_dbHead;

		typedef enum { FONTS_OK, FONTS_RELOAD, FONTS_CANCEL } FONTSTATE;

		static FONTSTATE InitFonts ();
		static void GetFontPaths (CStringArray & v);
		static void SetFonts (LPFJPRINTHEAD pHead);
		static void FreeFonts (LPFJPRINTHEAD pHead);
		static bool GetFonts (LPCFJPRINTHEAD pHead, CArray <CPrinterFont, CPrinterFont &> &v);

	protected:

	};
}; //namespace FoxjetCommon

#endif // !defined(AFX_HEAD_H__218919D6_AB4C_41C3_8069_9231B288F66A__INCLUDED_)
