// DynamicTextDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "DynamicTextDlg.h"
#include "DynamicTextElement.h"
#include "fj_dyntext.h"
#include "TemplExt.h"
#include "DynamicTableDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetIpElements;
using namespace FoxjetCommon;

/////////////////////////////////////////////////////////////////////////////
// CDynamicTextDlg dialog


CDynamicTextDlg::CDynamicTextDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa,
								 const FoxjetCommon::CElementList * pList, CWnd* pParent)
	: CElementDlg(e, pa, IDD_DYNAMICTEXT, pList, pParent)
{
	const CDynamicTextElement & text = GetElement ();
	const LPFJDYNTEXT pText = text.GetMember ();
	ASSERT (pText);

	m_lBold			= pText->lBoldValue;
	m_lWidth		= pText->lWidthValue;
	m_lGap			= pText->lGapValue;
	m_strFont		= pText->strFontName;
	m_lDynamicID	= pText->lDynamicID;
}


void CDynamicTextDlg::DoDataExchange(CDataExchange* pDX)
{
	CElementDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDynamicTextDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	DDX_CBString (pDX, CB_FONT, m_strFont);

	DDX_Text (pDX, TXT_BOLD, m_lBold);
	DDV_MinMaxInt (pDX, m_lBold, CIpBaseElement::m_lmtBold.m_dwMin, CIpBaseElement::m_lmtBold.m_dwMax); 

	DDX_Text (pDX, TXT_WIDTH, m_lWidth);
	DDV_MinMaxInt (pDX, m_lWidth, CIpBaseElement::m_lmtWidth.m_dwMin, CIpBaseElement::m_lmtWidth.m_dwMax); 

	DDX_Text (pDX, TXT_GAP, m_lGap);
	DDV_MinMaxInt (pDX, m_lGap, CIpBaseElement::m_lmtGap.m_dwMin, CIpBaseElement::m_lmtGap.m_dwMax); 

	DDX_Text (pDX, TXT_LENGTH, m_lLength);
	DDV_MinMaxInt (pDX, m_lLength, CDynamicTextElement::m_lmtLen.m_dwMin, CDynamicTextElement::m_lmtLen.m_dwMax); 

	DDX_Text (pDX, TXT_FIRSTCHAR, m_lFirstChar);
	DDV_MinMaxInt (pDX, m_lFirstChar, CDynamicTextElement::m_lmtLen.m_dwMin, CDynamicTextElement::m_lmtLen.m_dwMax); 

	DDX_Text (pDX, TXT_NUMBEROFCHARS, m_lNumChar);
	DDV_MinMaxInt (pDX, m_lNumChar, CDynamicTextElement::m_lmtLen.m_dwMin, CDynamicTextElement::m_lmtLen.m_dwMax); 

	DDX_Text (pDX, TXT_FILL, m_strFill);
}


BEGIN_MESSAGE_MAP(CDynamicTextDlg, CElementDlg)
	//{{AFX_MSG_MAP(CDynamicTextDlg)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(TXT_FILL, InvalidatePreview)
	ON_EN_CHANGE(TXT_LENGTH, InvalidatePreview)
	ON_EN_CHANGE(TXT_FIRSTCHAR, InvalidatePreview)
	ON_EN_CHANGE(TXT_NUMBEROFCHARS, InvalidatePreview)
	ON_CBN_SELCHANGE(CB_ALIGN, InvalidatePreview)
	ON_CBN_SELCHANGE(CB_DYNAMICID, InvalidatePreview)
	ON_BN_CLICKED(BTN_SELECT, OnSelect)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDynamicTextDlg message handlers

FoxjetIpElements::CDynamicTextElement & CDynamicTextDlg::GetElement ()
{
	CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CDynamicTextElement)));
	return (CDynamicTextElement &)e;
}

const FoxjetIpElements::CDynamicTextElement & CDynamicTextDlg::GetElement () const
{
	const CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CDynamicTextElement)));
	return (const CDynamicTextElement &)e;
}

LONG CDynamicTextDlg::GetCurSelDynamicID () const
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_DYNAMICID);

	ASSERT (pCB);

	return pCB->GetItemData (pCB->GetCurSel ());
}

int CDynamicTextDlg::BuildElement ()
{
	CDynamicTextElement & e = GetElement ();
	CString strText, strFont, strFill;
	CComboBox * pAlign = (CComboBox *)GetDlgItem (CB_ALIGN);

	ASSERT (pAlign);

	GetDlgItemText (TXT_TEXT, strText);
	GetDlgItemText (CB_FONT, strFont);
	GetDlgItemText (TXT_FILL, strFill);

	e.SetFontName (strFont);
	e.SetBold (GetDlgItemInt (TXT_BOLD));
	e.SetGap (GetDlgItemInt (TXT_GAP));
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));
	e.SetFlippedH (GetCheck (CHK_FLIPH));
	e.SetFlippedV (GetCheck (CHK_FLIPV));
	e.SetInverse (GetCheck (CHK_INVERSE));
	e.SetDynamicID (GetCurSelDynamicID ());

	e.SetLength (GetDlgItemInt (TXT_LENGTH));
	e.SetFirstChar (GetDlgItemInt (TXT_FIRSTCHAR));
	e.SetNumChar (GetDlgItemInt (TXT_NUMBEROFCHARS));
	e.SetAlign ((CHAR)pAlign->GetItemData (pAlign->GetCurSel ()));
	e.SetFill (strFill.GetLength () ? strFill [0] : 0);

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

BOOL CDynamicTextDlg::OnInitDialog() 
{
	CComboBox * pFont = (CComboBox *)GetDlgItem (CB_FONT);
	CComboBox * pID = (CComboBox *)GetDlgItem (CB_DYNAMICID);
	CComboBox * pAlign = (CComboBox *)GetDlgItem (CB_ALIGN);
	struct 
	{
		UINT	m_nID;
		CHAR	m_c;
	} static const alignmap [] = 
	{
		{ IDS_LEFT,		'L'	},
		{ IDS_RIGHT,	'R'	},
	};

	ASSERT (pFont);
	ASSERT (pID);
	ASSERT (pAlign);

	CElementDlg::OnInitDialog();

	CArray <CPrinterFont, CPrinterFont &> vFonts;
	const CHead & head = GetList ()->GetHead (0);
	head.GetFonts (vFonts);

	for (int i = 0; i < vFonts.GetSize (); i++) {
		CString str = vFonts [i].m_strName;
		int nIndex = pFont->AddString (str);

		if (str == m_strFont)
			pFont->SetCurSel (nIndex);
	}

	for (int i = (int)CDynamicTextElement::m_lmtID.m_dwMin; i <= (int)CDynamicTextElement::m_lmtID.m_dwMax; i++) {
		CString str;

		str.Format (_T ("%d"), i);
		int nIndex = pID->AddString (str);
		pID->SetItemData (nIndex, i);

		if (i == m_lDynamicID)
			pID->SetCurSel (nIndex);
	}

	for (int i = 0; i < ARRAYSIZE (alignmap); i++) {
		CHAR c = alignmap [i].m_c;
		CString str = LoadString (alignmap [i].m_nID);
		int nIndex = pAlign->AddString (str);

		pAlign->SetItemData (nIndex, c);

		if (m_cAlign == c)
			pAlign->SetCurSel (nIndex);
	}

	if (pID->GetCurSel () == CB_ERR)
		pID->SetCurSel (0);

	if (pAlign->GetCurSel () == CB_ERR)
		pAlign->SetCurSel (0);

	return FALSE;
}

void CDynamicTextDlg::OnOK()
{
	CComboBox * pAlign = (CComboBox *)GetDlgItem (CB_ALIGN);

	ASSERT (pAlign);

	m_lDynamicID = GetCurSelDynamicID ();
	ASSERT (m_lDynamicID != -1);

	m_cAlign = (CHAR)pAlign->GetItemData (pAlign->GetCurSel ());

	CElementDlg::OnOK ();
}

void CDynamicTextDlg::OnSelect() 
{
	CDynamicTableDlg dlg;

	dlg.m_lLineID = GetLineID ();
	dlg.m_lSelectedID = GetCurSelDynamicID ();

	if (dlg.DoModal () == IDOK) {
		CComboBox * pCB = (CComboBox *)GetDlgItem (CB_DYNAMICID);

		ASSERT (pCB);

		for (int i = 0; i < pCB->GetCount (); i++) {
			if (dlg.m_lSelectedID == (LONG)pCB->GetItemData (i)) {
				pCB->SetCurSel (i);
				break;
			}
		}
	}
}
