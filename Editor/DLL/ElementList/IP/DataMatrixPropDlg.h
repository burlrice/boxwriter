#if !defined(AFX_DATAMATRIXPROPDLG_H__13DAC5B8_B974_4A52_A2BF_94EFF03B0434__INCLUDED_)
#define AFX_DATAMATRIXPROPDLG_H__13DAC5B8_B974_4A52_A2BF_94EFF03B0434__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataMatrixPropDlg.h : header file
//

#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CDataMatrixPropDlg dialog

class CDataMatrixPropDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CDataMatrixPropDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDataMatrixPropDlg)
	enum { IDD = IDD_GLOBALDATAMATRIX };
	CString	m_strName;
	long	m_lHeight;
	long	m_lHeightBold;
	long	m_lWidth;
	long	m_lWidthBold;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataMatrixPropDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDataMatrixPropDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAMATRIXPROPDLG_H__13DAC5B8_B974_4A52_A2BF_94EFF03B0434__INCLUDED_)
