#if !defined(AFX_DATABASEDLG_H__8E88E130_EE7E_4ECC_9B47_B6E6DD917359__INCLUDED_)
#define AFX_DATABASEDLG_H__8E88E130_EE7E_4ECC_9B47_B6E6DD917359__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DatabaseDlg.h : header file
//

#include "DynDataPropDlg.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CDatabaseDlg dialog

class CDatabaseDlg : public FoxjetCommon::CEliteDlg, public FoxjetIpElements::CDatabaseParams 
{
// Construction
public:
	CDatabaseDlg(CWnd* pParent = NULL);   // standard constructor

	static const FoxjetCommon::LIMITSTRUCT m_lmtKey;

// Dialog Data
	//{{AFX_DATA(CDatabaseDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDatabaseDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	virtual void OnOK();
	void InitTables (const CString & strDSN, const CString & strTable = _T (""), const CString & strField = _T (""),
		const CString & strKeyField = _T ("")); 
	void InitFields (const CString & strDSN, const CString & strTable, const CString & strField = _T (""),
		const CString & strKeyField = _T (""));

	afx_msg void UpdateKeyField();
	afx_msg void OnDatabase ();

	// Generated message map functions
	//{{AFX_MSG(CDatabaseDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBrowse();
	afx_msg void OnSelchangeTable();
	afx_msg void OnSelchangeField();
	afx_msg void OnSelect();
	afx_msg void OnSelectKey();
	afx_msg void OnKey();
	afx_msg void OnSelchangeTab(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATABASEDLG_H__8E88E130_EE7E_4ECC_9B47_B6E6DD917359__INCLUDED_)
