// DynamicBarcodeElement.h: interface for the CDynamicBarcodeElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DYNAMICBARCODEELEMENT_H__2EAF1BBF_9637_4963_BBC3_56642BE552F8__INCLUDED_)
#define AFX_DYNAMICBARCODEELEMENT_H__2EAF1BBF_9637_4963_BBC3_56642BE552F8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

  
#include "IpBaseElement.h"
#include "PrinterFont.h"
#include "Types.h"
#include "fj_element.h"
#include "fj_dynbarcode.h"

FORWARD_DECLARE (fjdynbarcode,	FJDYNBARCODE)
FORWARD_DECLARE (fjsystem,		FJSYSTEM)
FORWARD_DECLARE (fjsysbarcode,	FJSYSBARCODE)
FORWARD_DECLARE (fjmessage,		FJMESSAGE)
 
namespace FoxjetIpElements
{
	class ELEMENT_API CDynamicBarcodeElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CDynamicBarcodeElement);

	public:
		CDynamicBarcodeElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CDynamicBarcodeElement (const CDynamicBarcodeElement & rhs);
		CDynamicBarcodeElement & operator = (const CDynamicBarcodeElement & rhs);
		virtual ~CDynamicBarcodeElement ();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual bool SetElement (LPFJELEMENT pNew);
		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		bool SetDefaultData (const CString &strData);

		virtual void CreateImage ();
		virtual CString GetElementFontName () const;

		virtual bool Validate ();
		virtual bool IsValid () const;

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		DECLARE_MEMBERPOINTERACCESS (LPFJDYNBARCODE, GetElement ()->pDesc)

	public:

		int GetDynamicID () const;
		bool SetDynamicID (LONG lID);

		bool GetOutputText () const;
		bool SetOutputText (bool bValue);

		LONG GetBold () const;
		bool SetBold (LONG lValue);
				
		LONG GetTextBold () const;
		bool SetTextBold (LONG lValue);
		
		virtual int GetWidth () const;
		virtual bool SetWidth (int nValue);
		
		LONG GetBcType () const;
		bool SetBcType (LONG lValue);
		
		CHAR GetTextAlign () const;
		bool SetTextAlign (CHAR cValue);

		LONG GetFirstChar () const;
		bool SetFirstChar (LONG l);

		LONG GetNumChar () const;
		bool SetNumChar (LONG l);

		const FJSYSBARCODE & GetGlobal () const;

	protected:
		virtual void Invalidate ();
		void LoadGlobalParams ();

		int m_nEncode;
	};
}; //namespace FoxjetIpElements

#endif // !defined(AFX_DYNAMICBARCODEELEMENT_H__2EAF1BBF_9637_4963_BBC3_56642BE552F8__INCLUDED_)
