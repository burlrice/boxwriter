#include "stdafx.h"

#include <afxdb.h>

#include "Edit.h"
#include "List.h"
#include "ElementDefaults.h"
#include "Debug.h"
#include "ItiLibrary.h"
#include "Database.h"
#include "Registry.h"
#include "Extern.h"
#include "TemplExt.h"

#include "fj_system.h"
#include "fj_label.h"
#include "fj_printhead.h"
#include "fj_font.h"

#include "TextDlg.h"
#include "DynamicTextDlg.h"
#include "BitmapDlg.h"
#include "CountDlg.h"
#include "DateTimeDlg.h"
#include "BarcodeDlg.h"
#include "DynamicBarcodeDlg.h"
#include "SelectLineDlg.h"
#include "LabelDlg.h"
#include "DaysDlg.h"
#include "TimeDlg.h"
#include "BitmapFontDlg.h"
#include "SystemBarcodeDlg.h"
#include "BitmapConfigDlg.h"
#include "Utils.h"
#include "WinMsg.h"
#include "DynamicTableDlg.h"
#include "DataMatrixDlg.h"
#include "SystemDataMatrixDlg.h"

#pragma warning(disable:4786) 

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace ListGlobals;
using namespace FoxjetIpElements;

extern CArray <LPFJSYSTEM, LPFJSYSTEM> vIpSystems;

static struct tm & Copy (struct tm & tmDest, const CTime & tmSrc);
static CTime & Copy (CTime & tmDest, const struct tm & tmSrc);
static ULONG GetLine (CWnd * pParent);

static ULONG GetLine (CWnd * pParent)
{
	ULONG lResult = -1;

 	if (::vIpSystems.GetSize () == 1)
		lResult = atol (::vIpSystems [0]->strID);
	else {
		CSelectLineDlg dlg (pParent);

		if (dlg.DoModal () == IDOK) 
			lResult = dlg.m_lLineID;
	}

	return lResult;
}

LPFJSYSTEM GetSystem (ULONG lLineID) 
{
	for (int i = 0; i < ::vIpSystems.GetSize (); i++) 
		if ((ULONG)atol (::vIpSystems [i]->strID) == lLineID)
			return ::vIpSystems [i];

	return NULL;
}

ELEMENT_API CString FoxjetCommon::GetBitmapEditorPath ()
{
	LPCTSTR lpszRoot = _tgetenv (_T ("SystemRoot"));
	CString strPath = CString (lpszRoot ? lpszRoot : _T ("%SystemRoot%")) + _T ("\\System32\\mspaint.exe");

	return FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER,
		ListGlobals::defElements.m_strRegSection + _T ("\\Bitmap"), 
		_T ("Editor"), strPath);
}

void CALLBACK FoxjetIpElements::OnDefineBitmapConfig (CWnd * pParent, const CVersion & version) 
{
	CBitmapConfigDlg dlg (pParent);

	dlg.m_strEditor = GetBitmapEditorPath ();
	dlg.m_strPath = CBitmapElement::GetDefPath ();

	if (dlg.DoModal () == IDOK) {
		FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER,
			ListGlobals::defElements.m_strRegSection + _T ("\\Bitmap"), 
			_T ("Editor"), dlg.m_strEditor);
		FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER,
			ListGlobals::defElements.m_strRegSection + _T ("\\Bitmap"), 
			_T ("Path"), dlg.m_strPath);
	}
}

static struct tm & Copy (struct tm & tmDest, const CTime & tmSrc)
{
	int nYear = tmSrc.GetYear () - 1900;

	tmDest.tm_sec	= tmSrc.GetSecond ();  
	tmDest.tm_min	= tmSrc.GetMinute ();  
	tmDest.tm_hour	= tmSrc.GetHour (); 
	tmDest.tm_mday	= tmSrc.GetDay (); 
	tmDest.tm_mon	= tmSrc.GetMonth ();  
	tmDest.tm_year	= nYear;
	tmDest.tm_wday	= tmSrc.GetDayOfWeek (); 
/*
	tmDest.tm_yday	= ? 
	tmDest.tm_isdst	= ?
*/

	return tmDest;
}

static CTime & Copy (CTime & tmDest, const struct tm & tmSrc)
{
	int nYear = tmSrc.tm_year + 1900;
	int nMonth = tmSrc.tm_mon + 1;
	int nHour = tmSrc.tm_hour - 1;

	tmDest = CTime (nYear, nMonth, tmSrc.tm_mday, 
		nHour, tmSrc.tm_min, tmSrc.tm_sec, tmSrc.tm_isdst);

	return tmDest;
}

bool CALLBACK FoxjetIpElements::OnDefineShiftCodes (CWnd * pParent, const CVersion & version) 
{
	TimeDlg::CTimeDlg dlg (pParent);
	COdbcDatabase & db = ListGlobals::GetDB ();
	const CString strSection = defElements.m_strRegSection + _T ("\\CTimeDlg");
	bool bResult = false;

	dlg.m_lLineID = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("LineID"), -1);

	if (dlg.DoModal () == IDOK) {
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("LineID"), dlg.m_lLineID);
		FoxjetDatabase::NotifySystemParamChange (db);
		bResult = true;
	}


	return bResult;
}

bool ELEMENT_API CALLBACK FoxjetIpElements::OnDefineDateTimeSettings (CWnd * pParent, const CVersion & version) 
{
	CDaysDlg dlg (pParent);
	COdbcDatabase & db = ListGlobals::GetDB ();
	const CString strSection = defElements.m_strRegSection + _T ("\\CDaysDlg");
	bool bResult = false;

	dlg.m_lLineID = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("LineID"), -1);

	if (dlg.DoModal () == IDOK) {
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("LineID"), dlg.m_lLineID);
		FoxjetDatabase::NotifySystemParamChange (db);
		bResult = true;
	}


	return bResult;
}

bool CALLBACK FoxjetIpElements::OnDefineDataMatrixParams (CWnd * pParent, const CVersion & version) 
{
	CSystemDataMatrixDlg dlg (pParent);
	COdbcDatabase & db = ListGlobals::GetDB ();
	const CString strSection = defElements.m_strRegSection + _T ("\\CSystemDataMatrixDlg");
	bool bResult = false;

	dlg.m_nHeadType		= (FoxjetDatabase::HEADTYPE)FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("HeadType"), HEADTYPE_FIRST);
	dlg.m_lLineID		= FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("LineID"), -1);

	BEGIN_TRANS (db);

	if (dlg.DoModal () == IDOK) {
		COMMIT_TRANS (db);
		
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("HeadType"), dlg.m_nHeadType);
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("LineID"),	dlg.m_lLineID);
		LoadBarcodeParams (db, version);

		bResult = true;
	}
	else {
		ROLLBACK_TRANS (db);
	}

	FoxjetDatabase::NotifySystemParamChange (db);

	return bResult;
}

bool CALLBACK FoxjetIpElements::OnDefineGlobalBarcodes (CWnd * pParent, const CVersion & version) 
{
	CSystemBarcodeDlg dlg (pParent);
	COdbcDatabase & db = ListGlobals::GetDB ();
	const CString strSection = defElements.m_strRegSection + _T ("\\CSystemBarcodeDlg");
	bool bResult = false;

	dlg.m_nHeadType		= (FoxjetDatabase::HEADTYPE)FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("HeadType"), HEADTYPE_FIRST);
	dlg.m_lLineID		= FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("LineID"), -1);
	dlg.m_nType			= FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("nType"), -1);

	BEGIN_TRANS (db);

	if (dlg.DoModal () == IDOK) {
		COMMIT_TRANS (db);
		
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("HeadType"), dlg.m_nHeadType);
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("LineID"),	dlg.m_lLineID);
		LoadBarcodeParams (db, version);
		FoxjetDatabase::NotifySystemParamChange (db);

		bResult = true;
	}
	else {
		ROLLBACK_TRANS (db);
	}


	return bResult;
}

bool CALLBACK FoxjetIpElements::OnDefineDynamic (CWnd * pParent, const CVersion & version) 
{
	bool bResult = false;
	CDynamicTableDlg dlg;

	if (dlg.DoModal () == IDOK) {
		CDynamicTableDlg::SaveDynamicData ();
		bResult = true;
	}

	return bResult;
}

bool CALLBACK FoxjetIpElements::OnEditTextElement (CBaseElement & element, 
												   const FoxjetCommon::CElementList * pList, 
												   CWnd * pParent, 
												   const CSize & pa, UNITS units,
												   bool bReadOnlyElement, bool bReadOnlyLocation,
												   FoxjetCommon::CElementListArray * pAllLists,
												   FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == TEXT);
	CTextElement & e = * (CTextElement *)&element;
	CTextDlg dlg (e, pa, pList, pParent);
	CWinApp * pApp = ::AfxGetApp ();
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	bool bStatus = false;
	const CPoint ptOriginal = e.GetPos (pHead);

	dlg.m_bReadOnly = bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units = units;

	dlg.m_pt		= ptOriginal;
	dlg.m_nID		= e.GetID ();
	dlg.m_bFlipH	= e.IsFlippedH ();
	dlg.m_bFlipV	= e.IsFlippedV ();
	dlg.m_bInverse	= e.IsInverse ();
	dlg.m_strData	= e.GetDefaultData ();
	dlg.m_strFont	= e.GetFontName ();
	dlg.m_lBold		= e.GetBold ();
	dlg.m_lWidth	= e.GetWidth ();
	dlg.m_lGap		= e.GetGap ();
	
	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), dlg.m_bPreview);

		if (dlg.m_pt != ptOriginal)
			e.SetPos (dlg.m_pt, pHead);
	
		e.SetID (dlg.m_nID);
		e.SetFlippedH (dlg.m_bFlipH ? true : false);
		e.SetFlippedV (dlg.m_bFlipV ? true : false);
		e.SetInverse (dlg.m_bInverse ? true : false);
		e.SetDefaultData (dlg.m_strData);
		e.SetFontName (dlg.m_strFont);
		e.SetBold (dlg.m_lBold);
		e.SetWidth (dlg.m_lWidth);
		e.SetGap (dlg.m_lGap);

		switch (dlg.m_nAlign) {
		case 0:	e.SetAlignment ('L');	break;
		case 1:	e.SetAlignment ('C');	break;
		case 2:	e.SetAlignment ('R');	break;
		}

		e.Build ();

		TRACEF (Format (dlg.m_strData, dlg.m_strData.GetLength ()));
		TRACEF (Format (e.GetDefaultData (), e.GetDefaultData ().GetLength ()));
		CString str = e.ToString (CVersion ());
		TRACEF (Format (str, str.GetLength ()));

		if (pUpdate)
			pUpdate->Add (&element);

		bStatus = true;
	}

	if (dlg.IsDefault ())
		defElements.SetElement (element.GetClassID (), element.ToString (verApp), verApp);

	return bStatus;
}

bool CALLBACK FoxjetIpElements::OnEditBitmapElement (CBaseElement & element, 
													 const FoxjetCommon::CElementList * pList, 
													 CWnd * pParent, 
													 const CSize & pa, UNITS units,
													 bool bReadOnlyElement, bool bReadOnlyLocation,
													 FoxjetCommon::CElementListArray * pAllLists,
													 FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (pList);
	ASSERT (element.GetClassID () == BMP);
	CBitmapElement & e = * (CBitmapElement *)&element;
	CBitmapDlg dlg (e, pa, pList, pParent);
	const FoxjetDatabase::HEADSTRUCT & head = pList->GetHead ();
	bool bStatus = false;
	const CPoint ptOriginal = e.GetPos (&head);
	CWinApp * pApp = ::AfxGetApp ();

	dlg.m_nID				= e.GetID ();
	dlg.m_bReadOnly			= bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units				= units;
	dlg.m_pt				= ptOriginal;
	dlg.m_strPath			= e.GetFilePath ();
	dlg.m_bFlipH			= e.IsFlippedH ();
	dlg.m_bFlipV			= e.IsFlippedV ();
	dlg.m_bInverse			= e.IsInverse ();
	dlg.m_lBold				= e.GetBold ();
	dlg.m_lWidth			= e.GetWidth ();

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), dlg.m_bPreview);

		e.SetID (dlg.m_nID);
		e.SetFilePath (dlg.m_strPath, head);
		
		if (dlg.m_pt != ptOriginal)
			e.SetPos (dlg.m_pt, &head);
	
		e.SetFlippedH (dlg.m_bFlipH ? true : false);
		e.SetFlippedV (dlg.m_bFlipV ? true : false);
		e.SetInverse (dlg.m_bInverse ? true : false);
		e.SetBold (dlg.m_lBold);
		e.SetWidth (dlg.m_lWidth);
		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bStatus = true;
	}

	if (dlg.IsDefault ())
		defElements.SetElement (element.GetClassID (), element.ToString (verApp), verApp);

	return bStatus;
}

bool CALLBACK FoxjetIpElements::OnEditCountElement (CBaseElement & element, 
													const FoxjetCommon::CElementList * pList, 
													CWnd * pParent, 
													const CSize & pa, UNITS units,
													bool bReadOnlyElement, bool bReadOnlyLocation,
													FoxjetCommon::CElementListArray * pAllLists,
													FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == COUNT);
	CCountElement & e = * (CCountElement *)&element;
	CCountDlg dlg (e, pa, pList, pParent);
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	bool bStatus = false;
	const CPoint ptOriginal = e.GetPos (pHead);
	CWinApp * pApp = ::AfxGetApp ();

	dlg.m_bReadOnly = bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units = units;
	dlg.m_nID			= e.GetID ();
	dlg.m_pt			= ptOriginal;
	dlg.m_lBold			= e.GetBold ();
	dlg.m_lWidth		= e.GetWidth ();
	dlg.m_lGap			= e.GetGap ();
	dlg.m_strFont		= e.GetFontName ();
	dlg.m_bRightAlign	= e.GetAlign () == 'R';
	dlg.m_strFill		= e.GetFill ();
	dlg.m_lLength		= e.GetLength ();
	dlg.m_lChange		= e.GetChange ();
	dlg.m_lCurRepeat	= e.GetCurRepeat ();
	dlg.m_lCurValue		= e.GetCurValue ();
	dlg.m_lLimit		= e.GetLimit ();
	dlg.m_lRepeat		= e.GetRepeat ();
	dlg.m_lStart		= e.GetStart ();
	dlg.m_bFlipH		= e.IsFlippedH ();
	dlg.m_bFlipV		= e.IsFlippedV ();
	dlg.m_bInverse		= e.IsInverse ();

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), dlg.m_bPreview);

		e.SetID (dlg.m_nID);

		if (dlg.m_pt != ptOriginal)
			e.SetPos (dlg.m_pt, pHead);
	
		e.SetBold (dlg.m_lBold);
		e.SetWidth (dlg.m_lWidth);
		e.SetGap (dlg.m_lGap);
		e.SetFontName (dlg.m_strFont);
		e.SetAlign (dlg.m_bRightAlign ? 'R' : 'L');
		e.SetFill (dlg.m_strFill.GetLength () ? dlg.m_strFill [0] : 0);
		e.SetLength (dlg.m_lLength);
		e.SetChange (dlg.m_lChange);
		e.SetCurRepeat (dlg.m_lCurRepeat);
		e.SetCurValue (dlg.m_lCurValue);
		e.SetLimit (dlg.m_lLimit);
		e.SetRepeat (dlg.m_lRepeat);
		e.SetStart (dlg.m_lStart);
		e.SetFlippedH (dlg.m_bFlipH ? true : false);
		e.SetFlippedV (dlg.m_bFlipV ? true : false);
		e.SetInverse (dlg.m_bInverse ? true : false);

		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bStatus = true;
	}

	if (dlg.IsDefault ())
		defElements.SetElement (element.GetClassID (), element.ToString (verApp), verApp);

	return bStatus;
}

bool CALLBACK FoxjetIpElements::OnEditDateTimeElement (CBaseElement & element, 
													   const FoxjetCommon::CElementList * pList, 
													   CWnd * pParent, 
													   const CSize & pa, UNITS units,
													   bool bReadOnlyElement, bool bReadOnlyLocation,
													   FoxjetCommon::CElementListArray * pAllLists,
													   FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == DATETIME);
	CDateTimeElement & e = * (CDateTimeElement *)&element;
	CDateTimeDlg dlg (e, pa, pList, pParent);
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	bool bResult = false;
	const CPoint ptOriginal = e.GetPos (pHead);
	CWinApp * pApp = ::AfxGetApp ();

	dlg.m_bReadOnly = bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units = units;
	dlg.m_pt				= ptOriginal;
	dlg.m_nID				= e.GetID ();
	dlg.m_bFlipH			= e.IsFlippedH ();
	dlg.m_bFlipV			= e.IsFlippedV ();
	dlg.m_bInverse			= e.IsInverse ();
	dlg.m_lBold				= e.GetBold ();
	dlg.m_lWidth			= e.GetWidth ();
	dlg.m_lGap				= e.GetGap ();
	dlg.m_strFont			= e.GetFontName ();
	dlg.m_bRightAlign		= e.GetAlign () == 'R';
	dlg.m_strDelim			= e.GetDelim ();
	dlg.m_strFill			= e.GetFill ();
	dlg.m_lLength			= e.GetLength ();
	dlg.m_lHourACount		= e.GetHourACount ();
	dlg.m_nFormat			= (int)e.GetFormat ();
	dlg.m_nType				= (int)e.GetTimeType ();
	dlg.m_bSpecialString	= !e.GetStringNormal ();

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), dlg.m_bPreview);

		e.SetID (dlg.m_nID);

		if (dlg.m_pt != ptOriginal)
			e.SetPos (dlg.m_pt, pHead);
	
		e.SetFlippedH (dlg.m_bFlipH ? true : false);
		e.SetFlippedV (dlg.m_bFlipV ? true : false);
		e.SetInverse (dlg.m_bInverse ? true : false);
		e.SetBold (dlg.m_lBold);
		e.SetWidth (dlg.m_lWidth);
		e.SetGap (dlg.m_lGap);
		e.SetFontName (dlg.m_strFont);
		e.SetAlign (dlg.m_bRightAlign ? 'R' : 'L');
		e.SetDelim (dlg.m_strDelim.GetLength () ? dlg.m_strDelim [0] : 0);
		e.SetFill (dlg.m_strFill.GetLength () ? dlg.m_strFill [0] : 0);
		e.SetLength (dlg.m_lLength);
		e.SetHourACount (dlg.m_lHourACount);
		e.SetFormat ((FJDTFORMAT)dlg.m_nFormat);
		e.SetTimeType ((FJDTTYPE)dlg.m_nType);
		e.SetStringNormal (!dlg.m_bSpecialString);

		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bResult = true;
	}

	if (dlg.IsDefault ())
		defElements.SetElement (element.GetClassID (), element.ToString (verApp), verApp);

	return bResult;
}



bool CALLBACK FoxjetIpElements::OnEditBarcodeElement (CBaseElement & element, 
													  const FoxjetCommon::CElementList * pList,
													  CWnd * pParent, const CSize & pa, UNITS units,
													  bool bReadOnlyElement, bool bReadOnlyLocation,
													  FoxjetCommon::CElementListArray * pAllLists,
													  FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == BARCODE);
	CBarcodeElement & e = * (CBarcodeElement *)&element;
	CBarcodeDlg dlg (e, pa, pList, pParent);
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	bool bResult = false;
	const CPoint ptOriginal = e.GetPos (pHead);
	CWinApp * pApp = ::AfxGetApp ();

	dlg.m_bReadOnly = bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units = units;
	dlg.m_pt					= ptOriginal;
	dlg.m_nID					= e.GetID ();
	dlg.m_bFlipH				= e.IsFlippedH ();
	dlg.m_bFlipV				= e.IsFlippedV ();
	dlg.m_bInverse				= e.IsInverse ();
	dlg.m_bOutputText			= e.GetOutputText ();
	dlg.m_dBold					= e.GetBold ();
	dlg.m_strText				= e.GetText ();
	dlg.m_dTextBold				= e.GetTextBold ();
	dlg.m_dWidth				= e.GetWidth ();
	dlg.m_nType					= e.GetBcType ();
	dlg.m_cAlign				= e.GetTextAlign ();

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), dlg.m_bPreview);

		LPFJSYSTEM pSystem = e.GetSystem ();
		LINESTRUCT line;
		TCHAR sz [1024] = { 0 };

		ASSERT (pSystem);

		::ZeroMemory (sz, sizeof (sz));
		VERIFY (GetLineRecord (atol (pSystem->strID), line));
		const CString strSection = defElements.m_strRegSection + _T ("\\") + line.m_strName;

		e.SetID (dlg.m_nID);
	
		if (dlg.m_pt != ptOriginal)
			e.SetPos (dlg.m_pt, pHead);
		
		e.SetFlippedH (dlg.m_bFlipH			? true : false);
		e.SetFlippedV (dlg.m_bFlipV			? true : false);
		e.SetInverse (dlg.m_bInverse		? true : false);
		e.SetOutputText (dlg.m_bOutputText	? true : false);
		e.SetBold (dlg.m_dBold);
		e.SetText (dlg.m_strText);
		e.SetTextBold (dlg.m_dTextBold);
		e.SetWidth (dlg.m_dWidth);
		e.SetBcType (dlg.m_nType);
		e.SetTextAlign (dlg.m_cAlign);

		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bResult = true;
	}

	if (dlg.IsDefault ())
		defElements.SetElement (element.GetClassID (), element.ToString (verApp), verApp);

	return bResult;
}

bool CALLBACK FoxjetIpElements::OnEditDynamicBarcodeElement (CBaseElement & element, 
															 const FoxjetCommon::CElementList * pList,
															 CWnd * pParent, const CSize & pa, UNITS units,
															 bool bReadOnlyElement, bool bReadOnlyLocation,
															 FoxjetCommon::CElementListArray * pAllLists,
															 FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == DYNAMIC_BARCODE);
	CDynamicBarcodeElement & e = * (CDynamicBarcodeElement *)&element;
	CDynamicBarcodeDlg dlg (e, pa, pList, pParent);
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	bool bResult = false;
	const CPoint ptOriginal = e.GetPos (pHead);
	CWinApp * pApp = ::AfxGetApp ();

	dlg.m_bReadOnly = bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units = units;
	dlg.m_pt					= ptOriginal;
	dlg.m_nID					= e.GetID ();
	dlg.m_bFlipH				= e.IsFlippedH ();
	dlg.m_bFlipV				= e.IsFlippedV ();
	dlg.m_bInverse				= e.IsInverse ();
	dlg.m_bOutputText			= e.GetOutputText ();
	dlg.m_dBold					= e.GetBold ();
	dlg.m_lDynamicID			= e.GetDynamicID ();
	dlg.m_nTextBold				= e.GetTextBold ();
	dlg.m_nWidth				= e.GetWidth ();
	dlg.m_nType					= e.GetBcType ();
	dlg.m_cAlign				= e.GetTextAlign ();
	dlg.m_lFirstChar			= e.GetFirstChar ();
	dlg.m_lNumChar				= e.GetNumChar ();

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), dlg.m_bPreview);

		LPFJSYSTEM pSystem = e.GetSystem ();
		LINESTRUCT line;
		TCHAR sz [1024] = { 0 };

		ASSERT (pSystem);

		::ZeroMemory (sz, sizeof (sz));
		VERIFY (GetLineRecord (atol (pSystem->strID), line));
		const CString strSection = defElements.m_strRegSection + _T ("\\") + line.m_strName;

		e.SetID (dlg.m_nID);
	
		if (dlg.m_pt != ptOriginal)
			e.SetPos (dlg.m_pt, pHead);
		
		e.SetFlippedH (dlg.m_bFlipH			? true : false);
		e.SetFlippedV (dlg.m_bFlipV			? true : false);
		e.SetInverse (dlg.m_bInverse		? true : false);
		e.SetOutputText (dlg.m_bOutputText	? true : false);
		e.SetBold (dlg.m_dBold);
		e.SetDynamicID (dlg.m_lDynamicID);
		e.SetTextBold (dlg.m_nTextBold);
		e.SetWidth (dlg.m_nWidth);
		e.SetBcType (dlg.m_nType);
		e.SetTextAlign (dlg.m_cAlign);
		e.SetFirstChar (dlg.m_lFirstChar);
		e.SetNumChar (dlg.m_lNumChar);

		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bResult = true;
	}

	if (dlg.IsDefault ())
		defElements.SetElement (element.GetClassID (), element.ToString (verApp), verApp);

	return bResult;
}

bool CALLBACK FoxjetIpElements::OnEditDataMatrixElement (CBaseElement & element, 
														 const FoxjetCommon::CElementList * pList,
														 CWnd * pParent, const CSize & pa, UNITS units,
														 bool bReadOnlyElement, bool bReadOnlyLocation,
														 FoxjetCommon::CElementListArray * pAllLists,
														 FoxjetCommon::CElementArray * pUpdate)
{
	bool bResult = false;
	ASSERT (element.GetClassID () == DATAMATRIX);
	CDataMatrixElement & e = * (CDataMatrixElement *)&element;
	CDataMatrixDlg dlg (e, pa, pList, pParent);
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	const CPoint ptOriginal = e.GetPos (pHead);
	CWinApp * pApp = ::AfxGetApp ();

	dlg.m_bReadOnly				= bReadOnlyElement;
	dlg.m_bReadOnlyLocation		= bReadOnlyLocation;
	dlg.m_units					= units;
	dlg.m_pt					= ptOriginal;
	dlg.m_nID					= e.GetID ();
	dlg.m_bFlipH				= e.IsFlippedH ();
	dlg.m_bFlipV				= e.IsFlippedV ();
	dlg.m_bInverse				= e.IsInverse ();

	dlg.m_strData				= e.GetDefaultData ();
	dlg.m_cx					= e.GetCX ();
	dlg.m_cy					= e.GetCY ();
	dlg.m_nType					= e.GetBcType ();
	dlg.m_lDynamicID			= e.GetDynamicID ();
	dlg.m_lFirstChar			= e.GetFirstChar ();
	dlg.m_lNumChar				= e.GetNumChar ();

	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), dlg.m_bPreview);

		LPFJSYSTEM pSystem = e.GetSystem ();
		LINESTRUCT line;
		TCHAR sz [1024] = { 0 };

		ASSERT (pSystem);

		::ZeroMemory (sz, sizeof (sz));
		VERIFY (GetLineRecord (atol (pSystem->strID), line));
		const CString strSection = defElements.m_strRegSection + _T ("\\") + line.m_strName;

		e.SetID (dlg.m_nID);
	
		if (dlg.m_pt != ptOriginal)
			e.SetPos (dlg.m_pt, pHead);
		
		e.SetFlippedH (dlg.m_bFlipH			? true : false);
		e.SetFlippedV (dlg.m_bFlipV			? true : false);
		e.SetInverse (dlg.m_bInverse		? true : false);

		e.SetDefaultData (dlg.m_strData);
		e.SetCX (dlg.m_cx);
		e.SetCY (dlg.m_cy);
		e.SetBcType (dlg.m_nType);
		e.SetDynamicID (dlg.m_lDynamicID);
		e.SetFirstChar (dlg.m_lFirstChar);
		e.SetNumChar (dlg.m_lNumChar);

		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bResult = true;
	}

	if (dlg.IsDefault ())
		defElements.SetElement (element.GetClassID (), element.ToString (verApp), verApp);

	return bResult;
}

bool CALLBACK FoxjetIpElements::OnEditDynamicTextElement (CBaseElement & element, 
														  const FoxjetCommon::CElementList * pList,
														  CWnd * pParent, const CSize & pa, UNITS units,
														  bool bReadOnlyElement, bool bReadOnlyLocation,
														  FoxjetCommon::CElementListArray * pAllLists,
														  FoxjetCommon::CElementArray * pUpdate)
{
	ASSERT (element.GetClassID () == DYNAMIC_TEXT);
	CDynamicTextElement & e = * (CDynamicTextElement *)&element;
	CDynamicTextDlg dlg (e, pa, pList, pParent);
	CWinApp * pApp = ::AfxGetApp ();
	const HEADSTRUCT * pHead = pList ? &pList->GetHead () : NULL;
	bool bStatus = false;
	const CPoint ptOriginal = e.GetPos (pHead);

	dlg.m_bReadOnly = bReadOnlyElement;
	dlg.m_bReadOnlyLocation = bReadOnlyLocation;
	dlg.m_units = units;

	dlg.m_pt			= ptOriginal;
	dlg.m_nID			= e.GetID ();
	dlg.m_bFlipH		= e.IsFlippedH ();
	dlg.m_bFlipV		= e.IsFlippedV ();
	dlg.m_bInverse		= e.IsInverse ();
	dlg.m_lDynamicID	= e.GetDynamicID ();
	dlg.m_strFont		= e.GetFontName ();
	dlg.m_lBold			= e.GetBold ();
	dlg.m_lWidth		= e.GetWidth ();
	dlg.m_lGap			= e.GetGap ();
	dlg.m_lLength		= e.GetLength ();
	dlg.m_lFirstChar	= e.GetFirstChar ();
	dlg.m_lNumChar		= e.GetNumChar ();
	dlg.m_cAlign		= e.GetAlign ();
	dlg.m_strFill		= e.GetFill ();
	
	if (pApp)
		dlg.m_bPreview = pApp->GetProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), 1) ? true : false;

	if (dlg.DoModal () == IDOK) {
		if (pApp)
			pApp->WriteProfileInt (_T ("Settings\\CTextDlg"), _T ("Preview"), dlg.m_bPreview);

		if (dlg.m_pt != ptOriginal)
			e.SetPos (dlg.m_pt, pHead);
	
		e.SetID (dlg.m_nID);
		e.SetFlippedH (dlg.m_bFlipH ? true : false);
		e.SetFlippedV (dlg.m_bFlipV ? true : false);
		e.SetInverse (dlg.m_bInverse ? true : false);
		e.SetDynamicID (dlg.m_lDynamicID);
		e.SetFontName (dlg.m_strFont);
		e.SetBold (dlg.m_lBold);
		e.SetWidth (dlg.m_lWidth);
		e.SetGap (dlg.m_lGap);
		e.SetLength (dlg.m_lLength);
		e.SetFirstChar (dlg.m_lFirstChar);
		e.SetNumChar (dlg.m_lNumChar);
		e.SetAlign (dlg.m_cAlign);
		e.SetFill (dlg.m_strFill.GetLength () ? dlg.m_strFill [0] : 0);

		e.Build ();

		if (pUpdate)
			pUpdate->Add (&element);
		
		bStatus = true;
	}

	if (dlg.IsDefault ())
		defElements.SetElement (element.GetClassID (), element.ToString (verApp), verApp);

	return bStatus;
}


LPFJSYSTEM ELEMENT_API FoxjetIpElements::FindSystem (ULONG lFindID)
{
	for (int i = 0; i < ::vIpSystems.GetSize (); i++) {
		if (LPFJSYSTEM pSystem = ::vIpSystems [i]) {
			ULONG lLineID = strtoul (pSystem->strID, NULL, 10);

			if (lFindID == lLineID) {
				return pSystem;
			}
		}
	}

	return NULL;
}
