// SelectLineDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "SelectLineDlg.h"
#include "Extern.h"
#include "ItiLibrary.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ItiLibrary;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace ListGlobals;

CSelectLineDlg::CItem::CItem (const FoxjetDatabase::LINESTRUCT & rhs)
{
	m_lID		= rhs.m_lID;
	m_strName	= rhs.m_strName;
	m_strDesc	= rhs.m_strDesc;
}

CSelectLineDlg::CItem::~CItem ()
{
}

CString CSelectLineDlg::CItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0:		
		str = m_strName;
		break;
	case 1:		
		str = m_strDesc;
		break;
	}

	return str;
}

/////////////////////////////////////////////////////////////////////////////
// CSelectLineDlg dialog


CSelectLineDlg::CSelectLineDlg(CWnd* pParent /*=NULL*/)
:	m_lLineID (-1),
	FoxjetCommon::CEliteDlg(CSelectLineDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectLineDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSelectLineDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectLineDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectLineDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CSelectLineDlg)
	ON_NOTIFY(NM_DBLCLK, LV_LINE, OnDblclkLine)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectLineDlg message handlers

BOOL CSelectLineDlg::OnInitDialog() 
{
	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;
	CArray <LINESTRUCT, LINESTRUCT &> vLines;

	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog ();

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_NAME), 80));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_DESC), 120));
	
	m_lv.Create (LV_LINE, vCols, this, defElements.m_strRegSection);	
	GetLineRecords (vLines);

	for (int i = 0; i < vLines.GetSize (); i++) 
		m_lv.InsertCtrlData (new CItem (vLines [i]));

	return bResult;
}

void CSelectLineDlg::OnDblclkLine(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnOK ();	
	*pResult = 0;
}

void CSelectLineDlg::OnOK()
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		CItem * p = (CItem *)m_lv.GetCtrlData (sel [0]);
		m_lLineID = p->m_lID;
		FoxjetCommon::CEliteDlg::OnOK ();
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}
