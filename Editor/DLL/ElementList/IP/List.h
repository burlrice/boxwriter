// List.h: interface for the CElementList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LIST_H__6CB62659_8AD9_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_LIST_H__6CB62659_8AD9_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __IPPRINTER__
	#error IpElementList\List.h: Wrong config
#endif

#include "define.h"
#include "ElementApi.h"
#include "CopyArray.h"
#include "BaseElement.h"
#include "Database.h"
#include "Head.h"
#include "AppVer.h"

FORWARD_DECLARE (fjlabel, FJLABEL)

typedef enum { 
	MIN_PRODLEN = 1000,	

	#if __CUSTOM__ == __SW0810__
		MAX_PRODLEN = 120000,
	#elif( __CUSTOM__ == __SW0881__ )
		MAX_PRODLEN = 27000,
	#else
		MAX_PRODLEN = 40000,
	#endif

	MIN_PRODHEIGHT = 1, MAX_PRODHEIGHT = INT_MAX
} LIMITS;

namespace FoxjetCommon 
{
	class CElementList;

	typedef FoxjetCommon::CCopyArray <FoxjetCommon::CElementList *, FoxjetCommon::CElementList *> CElementListArray;
	typedef FoxjetCommon::CCopyArray <FoxjetCommon::CBaseElement *, FoxjetCommon::CBaseElement *> CElementArray;

	ELEMENT_API int GetElementTypeCount ();
	ELEMENT_API int GetElementType (const CString & strPrefix);
	ELEMENT_API bool IsValidElementType (int nType, const FoxjetDatabase::HEADSTRUCT & head);
	ELEMENT_API CString GetElementResStr (int nIndex);
	ELEMENT_API CString GetElementMenuStr(int nIndex);
	ELEMENT_API bool LoadToolBar (CToolBar & bar);

	ELEMENT_API CBaseElement * CreateElement (int nType, const FoxjetDatabase::HEADSTRUCT & head);
	ELEMENT_API CBaseElement * CopyElement (const CBaseElement * pSrc, const FoxjetDatabase::HEADSTRUCT * pHead, bool bUseDefSize);

	ELEMENT_API bool OnEditElement (CBaseElement & e, 
		CWnd * pParent,
		const CSize & pa, UNITS units,
		const FoxjetCommon::CElementList * pList = NULL, 
		bool bReadOnlyElement = false, bool bReadOnlyLocation = false,
		CElementListArray * pAllLists = NULL,
		CElementArray * pUpdate = NULL);

	ELEMENT_API bool InitInstance (FoxjetDatabase::COdbcDatabase & db, const CVersion & ver);
	ELEMENT_API bool LoadSystemParams (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);
	ELEMENT_API bool ExitInstance (const CVersion & ver);
	ELEMENT_API CString GetDSN ();
	ELEMENT_API CString GetDatabaseName ();
	ELEMENT_API bool OpenDatabase (FoxjetDatabase::COdbcDatabase & database, const CString & strDSN, const CString & strFilename, bool bCanThrowException = true);

	ELEMENT_API bool IsDefineCmdEnabled (UINT nID);
	ELEMENT_API CString GetDefineMenuStr (UINT nID);
	ELEMENT_API void OnDefineCmd (UINT nID, CWnd * pParent, const CVersion & ver);
	ELEMENT_API CString GetDefineCmdStatusText (UINT nID);

	ELEMENT_API CRect GetSplashTextRect ();
	ELEMENT_API bool GetSplashBitmap (CBitmap & bmp);
	ELEMENT_API CString GetSplashText (const CVersion & ver);

	ELEMENT_API bool IsConfiningOn ();
	ELEMENT_API bool SetConfiningOn (bool bOn = true);

// TODO: these need to be relocated 
	ELEMENT_API bool DoPromptDocName (bool bOpen, CString & strFile, 
		CWnd * pParent, bool & bReadOnly);
	ELEMENT_API bool OnProperties (FoxjetDatabase::TASKSTRUCT & task, FoxjetDatabase::BOXSTRUCT & box, CWnd *pParent, bool bReadOnly);
	ELEMENT_API FoxjetCommon::CVersion GetDocVersion (const CString & strDoc);
//	ELEMENT_API CString GetDefaultDocName (int nDocCount);
//	ELEMENT_API CString GetDocTitle (const CString & strDoc);
//	ELEMENT_API CString GetDocName(const CString &strFilePath);

	// begin undocumented
	ELEMENT_API bool SaveBitmap(HBITMAP hBmp, const CString & strFile);
	ELEMENT_API CBaseElement * CreateElement (HBITMAP hBmp, const FoxjetDatabase::HEADSTRUCT & head, const CString & strName);
	ELEMENT_API CBaseElement * CreateElement (const CString & str, const FoxjetDatabase::HEADSTRUCT & head);
	ELEMENT_API bool DoFontDialog (CPrinterFont & fnt, int & nWidth, 
		const CString & strSampleText = _T ("AaBbYyZz"), CWnd * pParent = NULL);
	ELEMENT_API CString GetBitmapEditorPath ();
	// end undocumented

	ELEMENT_API UINT GetLineRecordCount ();
	ELEMENT_API ULONG GetLineID (const CString & str);
	ELEMENT_API bool GetLineRecord (ULONG lID, FoxjetDatabase::LINESTRUCT & cs);
	ELEMENT_API bool GetLineRecords (CArray <FoxjetDatabase::LINESTRUCT, FoxjetDatabase::LINESTRUCT &> & v);
	ELEMENT_API bool GetFirstLineRecord (FoxjetDatabase::LINESTRUCT & line);
	ELEMENT_API bool GetLineHeads (ULONG lID, CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & v);

	ELEMENT_API UINT GetHeadRecordCount ();
	ELEMENT_API bool GetHeadRecord (ULONG lID, FoxjetDatabase::HEADSTRUCT & hs);
	ELEMENT_API bool GetHeadRecords (CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & v);
	ELEMENT_API ULONG GetHeadID (const CString & strUID);
	ELEMENT_API ULONG GetHeadID (ULONG lLineID, const CString & strHead);
	ELEMENT_API bool GetFirstHeadRecord (FoxjetDatabase::HEADSTRUCT & head);

	ELEMENT_API bool IsValidFileTitle (const CString & str);
	ELEMENT_API CString GetAppTitle ();

	ELEMENT_API void ToString (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::HEADSTRUCT & h, const FoxjetDatabase::LINESTRUCT * pLine, int nSection, CString & strBuffer);
	ELEMENT_API bool GetPcRepeater (FoxjetDatabase::COdbcDatabase & db, ULONG lHeadID);
	ELEMENT_API void SetPcRepeater (FoxjetDatabase::COdbcDatabase & db, ULONG lHeadID, bool bUse);
//  TODO: rem //	ELEMENT_API DWORD GetSyncSettings (FoxjetDatabase::COdbcDatabase & db);
	ELEMENT_API void SetSaveDynData (FoxjetDatabase::COdbcDatabase & db, ULONG lHeadID, bool bUse);
	ELEMENT_API bool GetSaveDynData (FoxjetDatabase::COdbcDatabase & db, ULONG lHeadID);
	ELEMENT_API double GetPrintRes (const FoxjetDatabase::HEADSTRUCT & h);

	class ELEMENT_API CElementList : public CPtrArray  
	{
	public:
		CElementList (bool bAutoDelete = true);
		CElementList (const CElementList & rhs);
		CElementList & operator = (const CElementList & rhs);
		virtual ~CElementList();

		friend ELEMENT_API bool operator == (const CElementList & lhs, const CElementList& rhs);
		friend ELEMENT_API bool operator != (const CElementList & lhs, const CElementList& rhs);

		CBaseElement & operator [] (int nIndex);
		const CBaseElement & operator [] (int nIndex) const;

		CSize GetProductArea () const;
		bool SetProductArea (const CSize & pa, const CSize & sizePanel = CSize (0, 0));
		int GetNextID () const;

		virtual CBaseElement & GetObject (int nIndex);
		virtual const CBaseElement & GetObject (int nIndex) const;

		virtual void DeleteAllElements ();
		virtual CString ToString (const CVersion & ver, bool bBreakTokens = false, const CString & strName = _T ("")) const;
		virtual int FromString (const CString & str, CVersion & ver, bool bDeleteExisiting = true);

		UNITS GetUnits () const;
		bool SetUnits (UNITS units);

		inline const FoxjetDatabase::HEADSTRUCT & GetHead () const;
		bool SetHead (const FoxjetDatabase::HEADSTRUCT & head);

		bool Load (const CString & strFile, CVersion & ver, FoxjetDatabase::MESSAGESTRUCT *pMsg);
		bool Load (const CString & strFile, CVersion & ver);
		bool Store (const CString & strFile, const CVersion & ver) const;

		// TODO: document this
		int FindID (UINT nID) const;

		bool GetFonts (CArray <CPrinterFont, CPrinterFont &> & v) const;
		inline const CHead & GetHead (int) const;
		// end document 

		const FoxjetCommon::CVersion & GetVersion () const;
		bool SetVersion (const FoxjetCommon::CVersion & ver);

		bool SetLabel (LPFJLABEL pLabel);
		const LPFJLABEL GetLabel () const;

		const CString GetUserDefined () const;
		bool SetUserDefined (const CString & str);

		static int GetMaxElements ();
		bool IsMaxedOut () const;
		bool CanAdd (int nElements = 1) const;

		bool GetSnap () const { return m_bSnap; }
		void SetSnap (bool bSnap) { m_bSnap = bSnap; }

	protected:
		bool m_bAutoDelete;
		CSize m_pa;
		UNITS m_units;
		CHead m_head;
		bool m_bSnap;

	private:
		FoxjetCommon::CVersion m_version;
		LPFJLABEL m_pLabel;
	};
}; // FoxjetCommon 

inline const FoxjetDatabase::HEADSTRUCT & FoxjetCommon::CElementList::GetHead () const
{
	return m_head;
}

inline const FoxjetCommon::CHead & FoxjetCommon::CElementList::GetHead (int) const
{
	return m_head;
}

#endif // !defined(AFX_LIST_H__6CB62659_8AD9_11D4_8FC6_006067662794__INCLUDED_)
