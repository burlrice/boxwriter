#include "stdafx.h"
#include "ListImp.h"

#include "List.h"
#include "CopyArray.h"
#include "resource.h"
#include "Types.h"
#include "ElementDefaults.h"
#include "Debug.h"
#include "Edit.h"
#include "PropertiesDlg.h"
#include "Coord.h"
#include "Database.h"
#include "OdbcDatabase.h"
#include "Registry.h"
#include "Alias.h"
#include "FileExt.h"
#include "Extern.h"
#include "ItiLibrary.h"
#include "TemplExt.h"
#include "PropertiesDlg.h"
#include "Utils.h"
#include "Parse.h"
#include "fj_dyntext.h"


#include "TextElement.h"
#include "CountElement.h"
#include "DateTimeElement.h"
#include "BitmapElement.h"
#include "BarcodeElement.h"
#include "DynamicTextElement.h"
#include "DynamicBarcodeElement.h"
#include "DataMatrixElement.h"

//#include "HpTextElement.h"

#include "fj_font.h"
#include "fj_system.h"
#include "fj_printhead.h"
#include "fj_label.h"
#include "fj_export.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//#define __SHOW_TOKENS__

using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace ListGlobals;
using namespace FoxjetIpElements;

FORWARD_DECLARE (fjsystem, FJSYSTEM)
//FORWARD_DECLARE (fjprinthead, FJPRINTHEAD)

extern HINSTANCE hInstance;

namespace DefineMenu
{
	typedef void (CALLBACK * LPDEFINECMD) (CWnd * pParent, const FoxjetCommon::CVersion & ver);

	struct
	{
		UINT		m_nMenuStrID;
		UINT		m_nMenuStatusStrID;
		LPDEFINECMD	m_lpfct;
	} static const map [] = 
	{
		{ IDS_BITMAPCONFIG,		IDS_BITMAPCONFIG_STATUSTEXT,		OnDefineBitmapConfig,					},
		{ IDS_SHIFTCODES,		IDS_SHIFTCODES_STATUSTEXT,			(LPDEFINECMD)OnDefineShiftCodes,		},
		{ IDS_DATETIMESETTINGS,	IDS_DATETIMESETTINGS_STATUSTEXT,	(LPDEFINECMD)OnDefineDateTimeSettings,	},
		{ IDS_FONTS,			IDS_FONTS_STATUSTEXT,				(LPDEFINECMD)OnDefineFonts,				},
		{ IDS_GLOBALBARCODES,	IDS_GLOBALBARCODES_STATUSTEXT,		(LPDEFINECMD)OnDefineGlobalBarcodes,	},
//#if (__CUSTOM__ == __SW0865__)
		{ IDS_DATAMATRIXPARAMS,	IDS_DATAMATRIXPARAMS_STATUSTEXT,	(LPDEFINECMD)OnDefineDataMatrixParams,	},
//#endif
		{ IDS_DYNAMICCONFIG,	IDS_DYNAMICCONFIG_STATUSTEXT,		(LPDEFINECMD)OnDefineDynamic,			},
	};
	static const int nFirst = 1301;
	static const int nSize = (sizeof (map) / sizeof (map [0]));
}; //namespace DefineMenu

namespace ElementMenu
{
	using namespace FoxjetIpElements;

#define DECLARE_CREATE(t) \
	FoxjetCommon::CBaseElement * CALLBACK Create##t (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head) \
	{ \
		return new ( ##t ) (pHead, head); \
	} \
	FoxjetCommon::CBaseElement * CALLBACK Copy##t (const FoxjetCommon::CBaseElement * pSrc) \
	{ \
		return new ( ##t ) (* ( ##t *)pSrc); \
	}

#define CREATEMAPENTRY(nType, lpszType, nMenuStrID, nMenuStatusStrID, classname, pfctEdit) \
	{ nType, lpszType, nMenuStrID, nMenuStatusStrID, Create##classname, Copy##classname, pfctEdit, }

	DECLARE_CREATE (CTextElement)
	DECLARE_CREATE (CBitmapElement)
	DECLARE_CREATE (CCountElement)
	DECLARE_CREATE (CDateTimeElement)
	DECLARE_CREATE (CBarcodeElement)
	DECLARE_CREATE (CDynamicTextElement)
	DECLARE_CREATE (CDynamicBarcodeElement)
	DECLARE_CREATE (CDataMatrixElement)

	typedef FoxjetCommon::CBaseElement * (CALLBACK * LPCREATEELEMENT) (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
	typedef FoxjetCommon::CBaseElement * (CALLBACK * LPCOPYELEMENT) (const FoxjetCommon::CBaseElement * pSrc);
	typedef bool (CALLBACK * LPEDITELEMENT) (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, CWnd * pParent, const CSize & pa, UNITS units, 
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);

	struct
	{
		ELEMENTTYPE			m_type;
		LPCTSTR				m_lpszType;
		UINT				m_nMenuStrID;
		UINT				m_nMenuStatusStrID;
		LPCREATEELEMENT		m_lpCreate;
		LPCOPYELEMENT		m_lpCopy;
		LPEDITELEMENT		m_lpEdit;
	} static const map [] = 
	{
		CREATEMAPENTRY (TEXT,				_T ("Text"),		IDS_TEXT,		IDS_MENUTEXT_,		CTextElement,			OnEditTextElement),
		CREATEMAPENTRY (BMP,				_T ("Bitmap"),		IDS_BITMAP,		IDS_MENUBITMAP_,	CBitmapElement,			OnEditBitmapElement),
		CREATEMAPENTRY (COUNT,				_T ("Counter"),		IDS_COUNT,		IDS_MENUCOUNT_,		CCountElement,			OnEditCountElement),
		CREATEMAPENTRY (DATETIME,			_T ("DateTime"),	IDS_DATETIME,	IDS_MENUDATETIME_,	CDateTimeElement,		OnEditDateTimeElement),
		CREATEMAPENTRY (BARCODE,			_T ("Barcode"),		IDS_BARCODE,	IDS_MENUBARCODE_,	CBarcodeElement,		OnEditBarcodeElement),
		CREATEMAPENTRY (DATAMATRIX,			_T ("DataMatrix"),	IDS_DATAMATRIX,	IDS_MENUDATAMATRIX_,CDataMatrixElement,		OnEditDataMatrixElement),
		CREATEMAPENTRY (DYNAMIC_TEXT,		_T ("Dyntext"),		IDS_DYNTEXT,	IDS_MENUDYNTEXT_,	CDynamicTextElement,	OnEditDynamicTextElement),
		CREATEMAPENTRY (DYNAMIC_BARCODE,	_T ("Dynbarcode"),	IDS_DYNBARCODE,	IDS_MENUDYNBARCODE_,CDynamicBarcodeElement,	OnEditDynamicBarcodeElement),
	};
	static const int nSize = (sizeof (map) / sizeof (map [0]));

	int GetIndex (int nType) {
		for (int i = 0; i < nSize; i++) 
			if ((int)map [i].m_type == nType)
				return i;

		ASSERT (0);
		return -1;
	}
}; //namespace ElementMenu

CArray <LPFJSYSTEM, LPFJSYSTEM> vIpSystems;
static bool	bConfining	= true;

bool ParseProductArea (CElementList & vElements, const CStringArray & v, 
					   const CVersion & ver)
{
	CString strToken [] = { 
		_T (""),			// prefix
		_T ("10000"),		// width in 1000-ths of an inch
		_T ("1000"),		// height in 1000-ths of an inch
		_T ("0"),			// units
		_T ("0")			// head ID
	};

	if (v.GetSize () && v [0].CompareNoCase (_T ("ProdArea")) != 0) 
		return false;

	for (int i = 0; i < min (v.GetSize (), 5); i++)
		if (v [i].GetLength ())
			strToken [i] = v [i];

	CSize pa (
		(int)_tcstol (strToken [1], NULL, 10),
		(int)_tcstol (strToken [2], NULL, 10));
	vElements.SetProductArea (pa);
	vElements.SetUnits ((UNITS)(UINT)_tcstol (strToken [3], NULL, 10));
	ULONG lHeadID = (ULONG)_tcstol (strToken [4], NULL, 10);
	HEADSTRUCT h;

	if (GetHeadRecord (lHeadID, h))
		vElements.SetHead (h);
	
	#ifdef _DEBUG
	CString str;
	str.Format (_T ("Product area = (%u, %u), units = %u"), 
		vElements.GetProductArea ().cx, 
		vElements.GetProductArea ().cy, 
		(UINT)vElements.GetUnits ());
	//TRACEF (str);
	#endif //_DEBUG

	return true;
}

bool ParsePacket (FoxjetCommon::CElementList & vElements, const CString & str, 
				  const FoxjetCommon::CVersion & ver)
{
	bool bResult = false;

	#ifdef _DEBUG
	CString strDebug;
	//TRACEF ("ParsePacket (" + str + ")");
	#endif //_DEBUG

	CStringArray v;

	Tokenize (str, v);

	if (v.GetSize ()) {
		CString strKey = v [0];

		if (!strKey.CompareNoCase (_T ("ProdArea"))) 
			bResult = ParseProductArea (vElements, v, ver);
		else if (!strKey.CompareNoCase (_T ("FontDef")))
			bResult = ParseFontDef (vElements, v, ver);
		else if (!strKey.CompareNoCase (_T ("ElementDef"))) 
			bResult = ParseElementDef (vElements, str, ver);
		else if (!strKey.CompareNoCase (_T ("Ver"))) 
			bResult = ParseVersion (vElements, str);
		else if (!strKey.CompareNoCase (a2w (fj_LabelID))) 
			bResult = vElements.SetLabel (fj_LabelFromStr (w2a (str)));
	}

	return bResult;
}

bool ParseElementDef (FoxjetCommon::CElementList & vElements, const CString & str, 
					  const FoxjetCommon::CVersion & ver)
{
	//TRACEF ("CElementList::ParseElementDef (\"" + str + "\"");
	CStringArray v;

	Tokenize (str, v);

	if (v.GetSize () >= 2 && v [0].CompareNoCase (_T ("ElementDef")) != 0) 
		return false;

	int nType = FoxjetCommon::GetElementType (v [1]);

	if (nType != -1) {
		const CBaseElement & def = ListGlobals::defElements.GetElement (nType);
		CBaseElement * pTmp = CopyElement (&def, &vElements.GetHead (), false);
		CLongArray braces;
		CString strElement = str, strToken (_T ("ElementDef"));

		// remove the "ElementDef" token
		int nIndex = strElement.Find (strToken, 0);
		
		if (nIndex != -1) {
			strElement.Delete (nIndex, strToken.GetLength () + 1);
			//TRACEF (strElement);

			if (pTmp->FromString (strElement, ver)) {
				//TRACEF ("Setting default for " + pTmp->ToString (ver));
				return ListGlobals::defElements.SetElement (nType, * pTmp, ver);
			}
			else {
				//TRACEF ("Failed to set default for: " + GetElementResStr (nType));
			}
		}
		else {
			//TRACEF ("No \"ElementDef\" token");
		}

		delete pTmp;
	}
	else {
		//TRACEF ("Failed to set default for: " + v [2]);
	}

	return false;
}

bool ParseFontDef(FoxjetCommon::CElementList & vElements, const CStringArray & v, 
				  const FoxjetCommon::CVersion & ver)
{
	CString strToken [] = { 
		_T (""), 
		_T (""), 
		_T ("Courier New"), 
		_T ("30"), 
		_T ("1"), 
		_T ("0"),
	};

	if (v.GetSize () && v [0].CompareNoCase (_T ("FontDef")) != 0) 
		return false;

	for (int i = 0; i < min (v.GetSize (), 6); i++)
		if (v [i].GetLength ())
			strToken [i] = v [i];

	CString strPrefix = strToken [1];
	CString strName = strToken [2];
	int nSize = _ttoi (strToken [3]);
	bool bBold = _ttoi (strToken [4]) ? true : false;
	bool bItalic = _ttoi (strToken [5]) ? true : false;

	#ifdef _DEBUG
	CString strDebug;
	strDebug.Format (_T ("%s, %d, %d, %d"), strName, nSize, bBold, bItalic);
	//TRACEF (strDebug);
	#endif //_DEBUG

	if (strPrefix == _T ("")) { // set defs for all elements
		//TRACEF ("Setting font for all elements");
		ListGlobals::defElements.SetElementFonts (strName, nSize, bBold, bItalic, ver);
		return true;
	}
	else {
		int nType = FoxjetCommon::GetElementType (strPrefix);

		if (nType != -1) {
			const CBaseElement & obj = ListGlobals::defElements.GetElement (nType);

			if (obj.HasFont ()) {
				CPrinterFont fnt (strName, nSize, bBold, bItalic);
				CBaseTextElement * pTxt = (CBaseTextElement *)CopyElement (&obj, &vElements.GetHead (), false);
				bool bResult = false;

				if (pTxt->SetFont (fnt))
					bResult = ListGlobals::defElements.SetElement (nType, * pTxt, ver);

				delete pTxt;
				return bResult;
			}
		}
		else {
			//TRACEF ("Can't set font for " + strPrefix);
		}
	}

	return false;
}

bool ParseVersion (FoxjetCommon::CElementList & vElements, const CString & str)
{
	CVersion ver;

	if (ver.FromString (str)) 
		return vElements.SetVersion (ver);

	return false;
}

bool ParseTimestamp (FoxjetCommon::CElementList & vElements, const CString & str)
{
	return true;
}

int CALLBACK EnumFontFamExProc(ENUMLOGFONTEX *lpelfe,
							   NEWTEXTMETRICEX *lpntme, 
							   int FontType, LPARAM lParam)
{
	CStringArray * p = (CStringArray *)lParam;
	ASSERT (p);

	if (FontType & TRUETYPE_FONTTYPE) {
		CStringArray & v = * p;
		CString strFont = lpelfe->elfLogFont.lfFaceName;

		for (int i = 0; i < v.GetSize (); i++) 
			if (v [i] == strFont)
				return 1;

		p->Add (strFont);
	}

	return 1;
}

ELEMENT_API CString FoxjetCommon::GetAppTitle ()
{
	return _T ("BoxWriter NET");
}

ELEMENT_API int FoxjetCommon::GetElementType (const CString & strPrefix)
{
	for (int i = 0; i < ElementMenu::nSize; i++) 
		if (!strPrefix.CompareNoCase (ElementMenu::map [i].m_lpszType)) 
			return ElementMenu::map [i].m_type;

	return -1;
}

ELEMENT_API int FoxjetCommon::GetElementTypeCount ()
{
	return LAST - FIRST;
}

ELEMENT_API bool FoxjetCommon::IsValidElementType (int nType, const FoxjetDatabase::HEADSTRUCT & head)
{
//#if (__CUSTOM__ != __SW0865__)
	if (!CDataMatrixElement::IsDataMatrixEnabled () && nType == DATAMATRIX)
		return false;
//#endif

	//if (FoxjetDatabase::IsHpHead (head.m_nHeadType)) 
	//	return HpElements::IsValidElementType (nType, head);

	return nType >= 0 && nType <= GetElementTypeCount ();
}

ELEMENT_API CString FoxjetCommon::GetElementResStr(int nType)
{
	CString str;
	int nIndex = ElementMenu::GetIndex (nType);

	if (nIndex != -1) 
		str = LoadString (ElementMenu::map [nIndex].m_nMenuStrID);
	else {
		ASSERT (0);
	}

	return str;
}

ELEMENT_API CString FoxjetCommon::GetElementMenuStr(int nType)
{
	CString str;
	int nIndex = ElementMenu::GetIndex (nType);

	if (nIndex != -1) 
		str = LoadString (ElementMenu::map [nIndex].m_nMenuStatusStrID);
	else {
		ASSERT (0);
	}

	return str;
}

LPFJPRINTHEAD GetHead (ULONG lHeadID)
{
	if (lHeadID == -1) {
		// default to first head in first group
		for (int i = 0; i < ::vIpSystems.GetSize (); i++) {
			LPFJSYSTEM pSystem = ::vIpSystems [i];

			for (int j = 0; j < FJSYS_NUMBER_PRINTERS; j++) 
				if (LPFJPRINTHEAD p = pSystem->gmMembers [j].pfph) 
					return p;
		}
	}
	else {
		CString strID;

		strID.Format (_T ("%d"), lHeadID);

		for (int i = 0; i < ::vIpSystems.GetSize (); i++) {
			LPFJSYSTEM pSystem = ::vIpSystems [i];

			for (int j = 0; j < FJSYS_NUMBER_PRINTERS; j++) 
				if (LPFJPRINTHEAD p = pSystem->gmMembers [j].pfph) 
					if (p->strID == strID)
						return p;
		}
	}

	return NULL;
}

ELEMENT_API FoxjetCommon::CBaseElement * FoxjetCommon::CreateElement (int nType, const FoxjetDatabase::HEADSTRUCT & head)
{
	//if (FoxjetDatabase::IsHpHead (head.m_nHeadType)) 
	//	return HpElements::CreateElement (nType, head);

	CBaseElement * p = NULL;
	int nIndex = ElementMenu::GetIndex (nType);
	LPFJPRINTHEAD pHead = GetHead (head.m_lID);

	ASSERT (pHead);

	if (nIndex != -1) {
		ASSERT (ElementMenu::map [nIndex].m_lpCreate);

		p = (* ElementMenu::map [nIndex].m_lpCreate) (pHead, head);

		ASSERT (p);
	}

	if (p)
		p->ClipTo (head);

	return p;
}

ELEMENT_API FoxjetCommon::CBaseElement * FoxjetCommon::CreateElement (HBITMAP hBmp, const FoxjetDatabase::HEADSTRUCT & head, 
																	  const CString & strName)
{
	//if (FoxjetDatabase::IsHpHead (head.m_nHeadType)) 
	//	return HpElements::CreateElement (hBmp, head, strName);

	CBaseElement * pResult = NULL;

	if (hBmp) {
		const CBitmapElement & def = (CBitmapElement &)ListGlobals::defElements.GetElement (BMP);
		CString strDefFilePath = def.GetFilePath ();
		CString strDir = CBitmapElement::GetDefPath ();
		CString strPrefix = strName.GetLength () ? strName : _T ("Image");
		int nIndex = strDefFilePath.ReverseFind ((TCHAR)'\\');

		if (nIndex != -1)
			strDir = strDefFilePath.Left (nIndex + 1);

		CString strFilepath = CreateTempFilename (strDir, strPrefix, _T ("bmp"));
		
		if (SaveBitmap (hBmp, strFilepath)) {
			pResult = CreateElement (BMP, head);

			if (pResult) {
				CBitmapElement * pBmp = (CBitmapElement *)pResult;
				
				if (!pBmp->SetFilePath (strFilepath, head)) {
					delete pResult;
					pResult = NULL;
				}
			}
		}
	}

	if (pResult)
		pResult->ClipTo (head);

	return pResult;
}

ELEMENT_API FoxjetCommon::CBaseElement * FoxjetCommon::CreateElement (const CString & str, const FoxjetDatabase::HEADSTRUCT & head)
{
	//if (FoxjetDatabase::IsHpHead (head.m_nHeadType)) 
	//	return HpElements::CreateElement (str, head);

	const CBaseElement & def = ListGlobals::defElements.GetElement (TEXT);
	CBaseElement * pResult = CopyElement (&def, &head, true);

	if (CTextElement * p = DYNAMIC_DOWNCAST (CTextElement, pResult)) {
		p->SetDefaultData (str);
	}
	else {
		delete pResult;
		pResult = NULL;
	}

	if (pResult)
		pResult->ClipTo (head);

	return pResult;
}

ELEMENT_API FoxjetCommon::CBaseElement * FoxjetCommon::CopyElement (const FoxjetCommon::CBaseElement * pSrc, const FoxjetDatabase::HEADSTRUCT * pHead, bool bUseDefSize)
{
	//if (pHead) {
	//	if (FoxjetDatabase::IsHpHead (pHead->m_nHeadType)) 
	//		return HpElements::CopyElement (pSrc, pHead, bUseDefSize);
	//}
	//else if (pSrc) {
	//	if (FoxjetDatabase::IsHpHead (pSrc->GetHead ().m_nHeadType)) 
	//		return HpElements::CopyElement (pSrc, &pSrc->GetHead (), bUseDefSize);
	//}

	CBaseElement * p = NULL;

	ASSERT (pSrc);

	int nType = pSrc->GetClassID ();
	int nIndex = ElementMenu::GetIndex (nType);

	if (nIndex != -1) {
		ASSERT (ElementMenu::map [nIndex].m_lpCopy);

		p = (* ElementMenu::map [nIndex].m_lpCopy) (pSrc);

		ASSERT (p);

		if (pHead)
			p->SetHead (* pHead);
	}

	if (p && pHead)
		p->ClipTo (* pHead);

	return p;
}

ELEMENT_API bool FoxjetCommon::LoadToolBar (CToolBar & bar) 
{
	return bar.LoadToolBar (IDR_IPELEMENTBAR_v120) ? true : false;
}

/* TODO: rem
ELEMENT_API bool FoxjetCommon::OpenDatabase (FoxjetDatabase::COdbcDatabase & database, const CString & strDSN, const CString & strFilename)
{
	try {
		if (database.OpenEx (_T ("DSN=") + strDSN, CDatabase::noOdbcDialog))
			return true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	CString strFilepath = GetHomeDir () + _T ("\\") + strFilename;
	
	if (::GetFileAttributes (strFilepath) == -1) {
		HINSTANCE hModule = ::hInstance; 
		bool bResult = false;

		if (HRSRC hRSRC = ::FindResource (hModule, MAKEINTRESOURCE (IDR_MKNETDB), _T ("MSAccess"))) {
			DWORD dwSize = ::SizeofResource (hModule, hRSRC);

			if (HGLOBAL hGlobal = ::LoadResource (hModule, hRSRC)) {
				if (LPVOID lpData = ::LockResource (hGlobal)) {
					if (FILE * fp = _tfopen (strFilepath, _T ("wb"))) {
						if (fwrite (lpData, dwSize, 1, fp) == 1) {
							bResult = true;
							FoxjetDatabase::WriteProfileInt (_T ("SOFTWARE\\FoxJet\\Driver"), _T ("DB"), 1);
						}
		
						fclose (fp);
					}
				}
			}
		}

		if (!bResult) {
			CString str;

			str.Format (LoadString (IDS_CANTCREATEDB),
				strFilepath);

			MsgBox (str, MB_ICONERROR);
		}
	}

	return FoxjetDatabase::OpenDatabase (database, strDSN, strFilename);
}
*/
ELEMENT_API bool FoxjetCommon::OpenDatabase (COdbcDatabase & database, const CString & strDSN, const CString & strFilename, bool bCanThrowException)
{
	CString strFilepath = GetHomeDir () + _T ("\\") + strFilename;
	CString strKey = _T ("Software\\Foxjet\\MarksmanELITE\\ODBC\\") + strDSN;
	CString str = strDSN;
	CString strUID = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("UID"), _T ("")); 
	CString strPWD = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("PWD"), _T ("")); 

	TRACEF (_T ("IsGojo: ") + FoxjetDatabase::ToString (IsGojo ()));
	TRACEF (_T ("IsNetworked: ") + FoxjetDatabase::ToString (IsGojo ()));

	if (IsNetworked ()) {
		if (strUID.GetLength () && strPWD.GetLength ())
			str += _T (";UID=") + strUID + _T (";PWD=") + strPWD + _T (";");

		try {
			if (database.OpenEx (_T ("DSN=") + str, CDatabase::noOdbcDialog)) {
				UpgradeDatabase (database, strDSN);
				return true;
			}
		}
		catch (CDBException * e)		{ if (bCanThrowException) HANDLEEXCEPTION (e); return false; }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}
	
	if (::GetFileAttributes (strFilepath) == -1) {
		bool bACCDB = FoxjetDatabase::IsAccdb () || FoxjetDatabase::IsAccdb (strFilepath);
		HINSTANCE hModule = ::hInstance; 
		bool bResult = false;
		UINT nID = bACCDB ? IDR_MKNETACCDB : IDR_MKNETDB;

		TRACEF (CString (_T ("Creating database: ")) + (bACCDB ? _T ("IDR_MKNETACCDB: ") : _T ("IDR_MKNETDB: ") ) + strFilepath);

		if (HRSRC hRSRC = ::FindResource (hModule, MAKEINTRESOURCE (nID), _T ("MSAccess"))) {
			DWORD dwSize = ::SizeofResource (hModule, hRSRC);

			if (HGLOBAL hGlobal = ::LoadResource (hModule, hRSRC)) {
				if (LPVOID lpData = ::LockResource (hGlobal)) {
					if (FILE * fp = _tfopen (strFilepath, _T ("wb"))) {
						if (fwrite (lpData, dwSize, 1, fp) == 1) {
							bResult = true;		
							FoxjetDatabase::WriteProfileInt (_T ("SOFTWARE\\FoxJet\\Driver"), _T ("DB"), 1);
						}

						fclose (fp);
					}
				}
			}
		}

		if (!bResult) {
			CString str;

			str.Format (LoadString (IDS_CANTCREATEDB),
				strFilepath);

			MsgBox (str, MB_ICONERROR);
		}
	}

	return FoxjetDatabase::OpenDatabase (database, strDSN, strFilename);
}

ELEMENT_API bool FoxjetCommon::InitInstance (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	ASSERT (db.IsOpen ());

	if (!SetDB (db))
		return false;

	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	int nHeads = 0;

	if (!InitDatabase (db))
		return false;

	{
		WSADATA wsaData;

		if (::WSAStartup (MAKEWORD (2,1), &wsaData) != 0) {
			TRACEF ("WSAStartup failed: " + FormatMessage (GetLastError ()));
		}
	}

	fj_initWinDynData ();

	if (GetLineRecords (vLines)) {
		for (int i = 0; i < vLines.GetSize (); i++) {
			LINESTRUCT & line = vLines [i];
			LPFJSYSTEM pSystem = fj_SystemNew ();
			CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

			ASSERT (pSystem);
			::vIpSystems.Add (pSystem);
			sprintf (pSystem->strID, "%d", line.m_lID);

			FoxjetDatabase::GetLineHeads (db, vLines [i].m_lID, vHeads);

			for (int j = 0; j < min (vHeads.GetSize (), FJSYS_NUMBER_PRINTERS); j++, nHeads++) {
				LPFJPRINTHEAD pHead = NULL;
				CString strID;

				VERIFY (pHead = pSystem->gmMembers [j].pfph = fj_PrintHeadNew ());
				strID.Format (_T ("%d"), vHeads [j].m_lID);
				strncpy (pSystem->gmMembers [i].strID, w2a (strID), FJPH_ID_SIZE);
				strncpy (pHead->strID, w2a (strID), FJPH_ID_SIZE);
				pHead->pfsys = pSystem;
			}
			
			VERIFY (pSystem->pfl = fj_LabelNew ());
		}

		if (!nHeads) {
			TRACEF ("no heads in database");
			return false;
		}
	}
	else {
		TRACEF ("no lines in database");
		return false;
	}

	LoadSystemParams (db, ver);

	if (CHead::InitFonts () == CHead::FONTS_CANCEL)
		::exit (0);

	for (int i = 0; i < ::vIpSystems.GetSize (); i++) {
		LPFJSYSTEM pSystem = ::vIpSystems [i];

		for (int j = 0; j < FJSYS_NUMBER_PRINTERS; j++) {
			if (LPFJPRINTHEAD pHead = pSystem->gmMembers [j].pfph)
				CHead::SetFonts (pHead);
		}
	}

	DefaultCustomDateTimeFmts (defElements.m_strRegSection);
	ListGlobals::defElements.LoadRegDefs (ver);

#ifdef _DEBUG
	{ 
		const HEADSTRUCT & head = GetDefaultHead ();

		for (int i = 0; i < ElementMenu::nSize; i++) {
			CBaseElement * pBase = CreateElement (ElementMenu::map [i].m_type, head);
			ASSERT (pBase);

			CIpBaseElement * p = DYNAMIC_DOWNCAST (CIpBaseElement, pBase);
			ASSERT (p);

			CString strPrefix = p->GetTypeString ();
			//TRACEF (strPrefix + " : " + ElementMenu::map [i].m_lpszType);

			if (strPrefix.CompareNoCase (ElementMenu::map [i].m_lpszType) != 0) {
				LPFJPRINTHEAD pHead = GetHead (head.m_lID);

				ASSERT (pHead);

				ASSERT (ElementMenu::map [i].m_lpCreate);
				CBaseElement * pDerivative = (* ElementMenu::map [i].m_lpCreate) (pHead, head);

				ASSERT (pDerivative);
				ASSERT (pDerivative->IsKindOf (p->GetRuntimeClass ()));

				delete pDerivative;
			}

			delete pBase;
		}
	}
#endif //_DEBUG

	return true;
}

ELEMENT_API bool FoxjetCommon::LoadSystemParams (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver)
{
	for (int i = 0; i < ::vIpSystems.GetSize (); i++) {
		if (LPFJSYSTEM pSystem = ::vIpSystems [i]) {
			ULONG lLineID = strtoul (pSystem->strID, NULL, 10);
			SETTINGSSTRUCT time, days;

			days.m_strData = 
				_T ("DAYS=\"") + LoadString (IDS_SUNDAY) + _T ("\",\"") + LoadString (IDS_MONDAY) + _T ("\",\"") + LoadString (IDS_TUESDAY) + _T ("\",\"") + LoadString (IDS_WEDNESDAY) + _T ("\",\"") + LoadString (IDS_THURSDAY) + _T ("\",\"") + LoadString (IDS_FRIDAY) + _T ("\",\"") + LoadString (IDS_SATURDAY) + _T ("\";")
				_T ("MONTHS=\"") + LoadString (IDS_JANUARY) + _T ("\",\"") + LoadString (IDS_FEBRUARY) + _T ("\",\"") + LoadString (IDS_MARCH) + _T ("\",\"") + LoadString (IDS_APRIL) + _T ("\",\"") + LoadString (IDS_MAY) + _T ("\",\"") + LoadString (IDS_JUNE) + _T ("\",\"") + LoadString (IDS_JULY) + _T ("\",\"") + LoadString (IDS_AUGUST) + _T ("\",\"") + LoadString (IDS_SEPTEMBER) + _T ("\",\"") + LoadString (IDS_OCTOBER) + _T ("\",\"") + LoadString (IDS_NOVEMBER) + _T ("\",\"") + LoadString (IDS_DECEMBER) + _T ("\";")
				_T ("DAYS_SPECIAL=\"Sonntag\",\"Montag\",\"Dienstag\",\"Mittwoch\",\"Donnerstag\",\"Freitag\",\"Samstag\";")
				_T ("MONTHS_SPECIAL=\"Januar\",\"Februar\",\"Marz\",\"April\",\"Mai\",\"Juni\",\"Juli\",\"August\",\"September\",\"Oktober\",\"November\",\"Dezember\";")
				_T ("HOURS=\"A\",\"B\",\"C\",\"D\",\"E\",\"F\",\"G\",\"H\",\"I\",\"J\",\"K\",\"L\",\"M\",\"N\",\"O\",\"P\",\"Q\",\"R\",\"S\",\"T\",\"U\",\"V\",\"W\",\"X\";")
				_T ("AMPM=\" AM\",\" PM\";");
			time.m_strData = 
				_T ("SYSTEM_TIME=2002/06/18 16:07:35;TIME_ZONE=-6.0;OBSERVE_USA_DST=T;")
				_T ("FIRST_DAY=1;DAYS_WEEK1=4;WEEK53=53;")
				_T ("ROLLOVER_HOURS=0.0;")
				_T ("SHIFT1=7:0;SHIFT2=15:0;SHIFT3=23:0;SHIFTCODE1=") + LoadString (IDS_FIRST) + _T (";SHIFTCODE2=") + LoadString (IDS_SECOND) + _T (";SHIFTCODE3=") + LoadString (IDS_THIRD) + _T (";");

			GetSettingsRecord (db, lLineID, ListGlobals::Keys::m_strTime,	time);
			GetSettingsRecord (db, lLineID, ListGlobals::Keys::m_strDays,	days);

			VERIFY (fj_SystemFromString (pSystem, w2a (time.m_strData)));
			VERIFY (fj_SystemFromString (pSystem, w2a (days.m_strData)));
		}
	}

	LoadBarcodeParams (db, ver);

	return true;
}

ELEMENT_API bool FoxjetCommon::ExitInstance (const FoxjetCommon::CVersion & ver)
{
	for (int i = 0; i < ::vIpSystems.GetSize (); i++) {
		LPFJSYSTEM pSystem = ::vIpSystems [i];

		for (int j = 0; j < FJSYS_NUMBER_PRINTERS; j++)
			if (LPFJPRINTHEAD pHead = pSystem->gmMembers [j].pfph) 
				CHead::FreeFonts (pHead);

		fj_SystemDestroy (pSystem);
	}

	::vIpSystems.RemoveAll ();

	return true;
}

ELEMENT_API CString FoxjetCommon::GetDSN ()
{
	return _T ("MarksmanNET");
}

ELEMENT_API CString FoxjetCommon::GetDatabaseName ()
{
	return _T ("MarksmanNET.mdb");
}

ELEMENT_API bool FoxjetCommon::OnEditElement (FoxjetCommon::CBaseElement & e, 
											  CWnd * pParent, 
											  const CSize & pa, UNITS units,
											  const FoxjetCommon::CElementList * pList, 
											  bool bReadOnlyElement, 
											  bool bReadOnlyLocation,
											  CElementListArray * pAllLists,
											  CElementArray * pUpdate)
{
	//if (IsHpHead (e.GetHead ().m_nHeadType)) 
	//	return HpElements::OnEditElement (e, pParent, pa, units, pList, bReadOnlyElement, bReadOnlyLocation, pAllLists, pUpdate);

	bool bResult = false;
	int nIndex = ElementMenu::GetIndex (e.GetClassID ());

	if (nIndex != -1) {
		ASSERT (ElementMenu::map [nIndex].m_lpEdit);

		bResult = (* ElementMenu::map [nIndex].m_lpEdit) (e, pList, pParent, pa, units, bReadOnlyElement, bReadOnlyLocation, pAllLists, pUpdate);
	}

	return bResult;
}

ELEMENT_API bool FoxjetCommon::IsDefineCmdEnabled (UINT nID)
{
	const UINT nLower = DefineMenu::nFirst;
	UINT nUpper = nLower + DefineMenu::nSize;

	return (nID >= nLower && nID < nUpper);
}

ELEMENT_API CString FoxjetCommon::GetDefineMenuStr (UINT nID)
{
	CString str;

	if (IsDefineCmdEnabled (nID)) 
		str = LoadString (DefineMenu::map [nID - DefineMenu::nFirst].m_nMenuStrID);

	return str;
}

// TODO: documenting this will require updating the
// "FoxjetCommon" section and "Adding a define cmd" section
ELEMENT_API CString FoxjetCommon::GetDefineCmdStatusText (UINT nID)
{
	CString str;

	if (IsDefineCmdEnabled (nID)) 
		str = LoadString (DefineMenu::map [nID - DefineMenu::nFirst].m_nMenuStatusStrID);

	return str;
}

ELEMENT_API void FoxjetCommon::OnDefineCmd (UINT nID, CWnd * pParent, 
											const FoxjetCommon::CVersion & ver)
{
	if (IsDefineCmdEnabled (nID)) {
		int nIndex = nID - DefineMenu::nFirst;
		
		ASSERT (nIndex >= 0);
		ASSERT (nIndex < DefineMenu::nSize);
		ASSERT (DefineMenu::map [nIndex].m_lpfct);

		(* DefineMenu::map [nIndex].m_lpfct) (pParent, ver);
	}
	else { ASSERT (0); }
}

ELEMENT_API CRect FoxjetCommon::GetSplashTextRect ()
{
	// coords are relative to the upper-left corner
	// of the bitmap loaded in GetSplashBitmap (CBitmap &)
	int x = 10;
	int y = 80;
	int cx = 260;
	int cy = 150;
/*
	int x = 250;
	int y = 10;
	int cx = 180;
	int cy = 200;
*/
	return CRect (x, y, x + cx, y + cy);
}

ELEMENT_API bool FoxjetCommon::GetSplashBitmap (CBitmap & bmp)
{
	UINT nID = IDB_FOXJET;

	bool bResult = bmp.LoadBitmap (nID) ? true : false;
	ASSERT (bResult);
	return bResult;
}

ELEMENT_API CString FoxjetCommon::GetSplashText (const FoxjetCommon::CVersion & ver)
{
	return GetAppTitle ();
}

ELEMENT_API bool FoxjetCommon::IsConfiningOn ()
{
	return ::bConfining;
}

ELEMENT_API bool FoxjetCommon::SetConfiningOn (bool bOn)
{
	::bConfining = bOn;
	return true;
}

ELEMENT_API bool FoxjetCommon::DoPromptDocName (bool bOpen, CString & strFile, 
												CWnd * pParent, bool & bReadOnly)
{
	ASSERT (0);
	return false;
/*
	CMessageDlg dlg (bOpen, FoxjetFile::GetFileTitle (strFile), bReadOnly, 
		&(ListGlobals::database), pParent);
	
	if (dlg.DoModal () == IDOK) {
		strFile = dlg.GetFilename () + _T (".") + 
			FoxjetFile::GetFileExt (FoxjetFile::MESSAGE);
		bReadOnly = dlg.GetReadOnly ();

		if (!bOpen) {  // saving
			MESSAGESTRUCT msg;
			CString strHead = FoxjetFile::GetFileHead (strFile);
			CString strMsg = FoxjetFile::GetFileTitle (strFile);
			COdbcDatabase & db = ListGlobals::database;

			if (GetMessageRecord (db, strMsg, strHead, msg)) {
				msg.m_strDesc = dlg.m_strDesc;
				return UpdateMessageRecord (db, msg);
			}
			else {
				msg.m_lHeadID = GetHeadIDByName (db, strHead);
				msg.m_strName = strMsg;
				msg.m_strDesc = dlg.m_strDesc;

				bool bAdd = AddMessageRecord (ListGlobals::database, msg);
				ASSERT (bAdd);
				return false; // doc mgr must save the new doc's m_strData
			}
		}
		else 
			// opening, only need to change strFile & bReadOnly.
			// doc mgr will do the rest
			return true; 
	}
	return false;
*/
}

ELEMENT_API bool FoxjetCommon::OnProperties (FoxjetDatabase::TASKSTRUCT & task, FoxjetDatabase::BOXSTRUCT & box, CWnd *pParent, bool bReadOnly)
{
	CPropertiesDlg dlg (pParent);
	CStringArray vLabel;
	LPFJLABEL pLabel = NULL;
	bool bResult = false;

	dlg.m_bReadOnly		= bReadOnly;
	dlg.m_strName		= task.m_strName;
	dlg.m_strDesc		= task.m_strDesc;
	dlg.m_strDownload	= task.m_strDownload;
	dlg.m_lLineID		= task.m_lLineID;
	dlg.m_lBoxID		= box.m_lID;
	
	for (int i = 0; i < task.m_vMsgs.GetSize (); i++) {
		CString str = task.m_vMsgs [i].m_strData;

		ExtractPackets (fj_LabelID, str, vLabel);
		dlg.m_vMsgs.Add (str);
	}

	if (vLabel.GetSize ()) 
		VERIFY (pLabel = fj_LabelFromStr (w2a (vLabel [0])));
	else
		pLabel = fj_LabelNew ();

	dlg.m_lExpValue		= pLabel->lExpValue;
	dlg.m_lExpUnits		= pLabel->lExpUnits;
	dlg.m_bExpRoundUp	= pLabel->bExpRoundUp;
	dlg.m_bExpRoundDown	= pLabel->bExpRoundDown;

	if (dlg.DoModal () == IDOK) {
		char szLabel [512];
		
		pLabel->lExpValue		= dlg.m_lExpValue;
		pLabel->lExpUnits		= (FJLABELEXP)dlg.m_lExpUnits;
		pLabel->bExpRoundUp		= dlg.m_bExpRoundUp;
		pLabel->bExpRoundDown	= dlg.m_bExpRoundDown;

		fj_LabelToStr (pLabel, szLabel);
		TRACEF (szLabel);

		ASSERT (!bReadOnly);

		// update FJLABEL 
		for (int i = 0; i < task.m_vMsgs.GetSize (); i++) {
			CString str = SetPackets (a2w (szLabel), task.m_vMsgs [i].m_strData);

			task.m_vMsgs [i].m_strData = str;
		}

		LPFJSYSTEM pSystem = GetSystem (task.m_lLineID);
		ASSERT (pSystem);
		ASSERT (pSystem->pfl);

		task.m_strDesc		= dlg.m_strDesc;
		task.m_strDownload	= dlg.m_strDownload;
		box.m_lID			= dlg.m_lBoxID;

		bResult = true;
	}

	if (pLabel)
		fj_LabelDestroy (pLabel);

	return bResult;
}

/*
ELEMENT_API CString FoxjetCommon::GetDocName(const CString & str)
{
	int nCount = 0;

	for (int i = str.GetLength () - 1; i >= 0; i--) {
		if (str [i] == (TCHAR)'\\') {
			nCount++;

			if (nCount == 2) 
				return str.Mid (i + 1);
		}
	}
	
	return str;
}

ELEMENT_API CString FoxjetCommon::GetDefaultDocName (int nDocCount)
{
	CString str;
	str.Format (_T ("%s\\%s%d"), 
		CLibraryDlg::GetDefaultLibrary (&(ListGlobals::database)), 
		(LPCTSTR)LoadString (IDS_DEFAULTTASK), nDocCount);
	return str;
}

ELEMENT_API CString FoxjetCommon::GetDocTitle (const CString & strDoc)
{
	CString str = GetDocName (strDoc);
	return CMessageDlg::GetMessageName (str);
}
*/

ELEMENT_API FoxjetCommon::CVersion FoxjetCommon::GetDocVersion (const CString & strDoc)
{
ASSERT ("strDoc should be the head's uid" == 0);

	CVersion version;
	COdbcDatabase & db = GetDB ();
	CString strSQL, strDocName = FoxjetFile::GetRelativeName (strDoc);
	CString strMessage = FoxjetFile::GetFileTitle (strDocName);
	CString strHead = FoxjetFile::GetFileHead (strDocName);
	ULONG lHeadID = GetHeadID (strHead);
	MESSAGESTRUCT msg;

	if (GetMessageRecord (db, strMessage, lHeadID, msg)) {
		CString str = msg.m_strData;
		CLongArray braces = CountChars (str, (TCHAR)'{');

		for (int i = 0; i < braces.GetSize (); i++) {
			CVersion ver;
			CString strVer = str.Mid (braces [i]);

			if (ver.FromString (strVer)) {
				version = ver;
				break;
			}
		}
	}

	return version;
}

UINT ForceUniqueID (const FoxjetCommon::CElementList & v, FoxjetCommon::CBaseElement & e)
{
	if (v.FindID (e.GetID ()) != -1)
		e.SetID (v.GetNextID ());

	return e.GetID ();
}

void ChangeConcatID (CMap <int, int, UINT, UINT> & concat,
					 const CMap <UINT, UINT, UINT, UINT> & id)
{
	for (POSITION pos = id.GetStartPosition(); pos != NULL; ) {
		UINT nOldID = -1;
		UINT nNewID = -1;

		id.GetNextAssoc (pos, nOldID, nNewID);
		ChangeConcatID (concat, nOldID, nNewID);
	}
}

bool ChangeConcatID (CMap <int, int, UINT, UINT> & map, UINT nOldID, UINT nNewID)
{
	for (POSITION pos = map.GetStartPosition(); pos != NULL; ) {
		UINT nConcatID = -1;
		int nIndex = -1;
		
		map.GetNextAssoc (pos, nIndex, nConcatID);

		if (nConcatID == nOldID) {
			map.SetAt (nIndex, nNewID);
			return true;
		}
	}

	return false;
}

ELEMENT_API bool FoxjetCommon::IsValidFileTitle (const CString & str)
{
	if (str.GetLength () > FJMESSAGE_NAME_SIZE)
		return false;

	for (int i = 0; i < str.GetLength (); i++) {
		TCHAR c = str [i];

		if (!_istalnum (c))
			return false;
	}

	return FoxjetFile::IsValidFileTitle (str);
}
