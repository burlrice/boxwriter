// DynamicBarcodeElement.cpp: implementation of the CDynamicBarcodeElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DynamicBarcodeElement.h"
#include "BarcodeElement.h"
#include "DynamicTextElement.h"
#include "Types.h"
#include "List.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"
#include "Database.h"
#include "Resource.h"
#include "TemplExt.h"
#include "DynamicTableDlg.h"

#include "fj_system.h"
#include "fj_printhead.h"
#include "fj_element.h"
#include "BarCode\barcode.h"
#include "fj_dynbarcode.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_font.h"
#include "fj_export.h"
#include "fj_message.h"
#include "fj_text.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetIpElements;
using namespace Color;
using namespace FoxjetCommon::ElementFields;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNAMIC (CDynamicBarcodeElement, CIpBaseElement)

CDynamicBarcodeElement::CDynamicBarcodeElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head)
:	m_nEncode (-1),
	CIpBaseElement (fj_ElementDynBarCodeNew (), pHead, head)
{
	LPFJDYNBARCODE p = GetMember ();

	p->lHRBold = 1;

	if (p->pBC) {
		p->pBC->flags   = 28;
		p->pBC->flags   = BARCODE_128 | BARCODE_NO_CHECKSUM;
	}
}

CDynamicBarcodeElement::CDynamicBarcodeElement (const CDynamicBarcodeElement & rhs)
:	m_nEncode (-1),
	CIpBaseElement (rhs)
{
}

CDynamicBarcodeElement & CDynamicBarcodeElement::operator = (const CDynamicBarcodeElement & rhs)
{
	CIpBaseElement::operator = (rhs);

	if (this != &rhs) {
		m_nEncode  = -1;
	}

	return * this;
}

CDynamicBarcodeElement::~CDynamicBarcodeElement ()
{
}

int CDynamicBarcodeElement::Build (BUILDTYPE type, bool bCanThrowException)
{
	return GetClassID ();
}

CString CDynamicBarcodeElement::GetImageData () const
{
	return CString ('0', 10);
}

CString CDynamicBarcodeElement::GetDefaultData () const
{
	return GetImageData ();
}

bool CDynamicBarcodeElement::SetDefaultData (const CString &strData)
{
	return false;
}

void CDynamicBarcodeElement::LoadGlobalParams () 
{
	LPFJDYNBARCODE pBarcode = GetMember ();
	LPFJSYSTEM pSystem = GetSystem ();

	ASSERT (pSystem);

	CString strSettings = CBarcodeElement::GetBarcodeSettings (ListGlobals::GetDB (), GetLineID (), GetHeadType ()); 
	VERIFY (fj_SystemFromString (pSystem, w2a (strSettings)));
}
#include "fj_dyntext.h"

void CDynamicBarcodeElement::CreateImage () 
{
	using FoxjetIpElements::CTableItem;

	if (IsValid ()) {
		LPFJDYNBARCODE pBarcode = GetMember ();
		LPFJSYSTEM pSystem = GetSystem ();

		ASSERT (pSystem);

		LoadGlobalParams ();

		CArray <CTableItem, CTableItem &> vItems;

		CDynamicTableDlg::GetDynamicData (GetLineID (), vItems);

		for (int i = 0; i < vItems.GetSize (); i++) {
			CTableItem item = vItems [i];

			if (item.m_lID == GetDynamicID ()) {
				TRACEF (item.GetSqlResult ());
				break;
			}
		}

		CIpBaseElement::CreateImage ();
	}
}

CString CDynamicBarcodeElement::GetElementFontName () const
{
	return BARCODE_FONT; 
}

bool CDynamicBarcodeElement::SetElement (LPFJELEMENT pNew)
{
	if (pNew) {
		ASSERT (pNew);
		ASSERT (pNew->pActions->fj_DescGetType () == FJ_TYPE_DYNBARCODE);
		ASSERT (pNew->pDesc);

		Invalidate ();
	}

	return CIpBaseElement::SetElement (pNew);
}

bool CDynamicBarcodeElement::GetOutputText () const
{
	LPFJDYNBARCODE p = GetMember ();
	
	return (p->bHROutput) ? true : false;
}

bool CDynamicBarcodeElement::SetOutputText (bool bValue)
{
	LPFJDYNBARCODE p = GetMember ();

	p->bHROutput = bValue;
	Invalidate ();

	return true;
}

int CDynamicBarcodeElement::GetDynamicID () const
{
	LPFJDYNBARCODE p = GetMember ();
	
	return p->lDynamicID + 1;
}

bool CDynamicBarcodeElement::SetDynamicID (LONG lID)
{
	if (FoxjetCommon::IsValid (lID, CDynamicTextElement::m_lmtID)) {
		LPFJDYNBARCODE p = GetMember ();
		
		p->lDynamicID = lID - 1;
		return true;
	}

	return false;
}

LONG CDynamicBarcodeElement::GetBold () const
{
	return GetMember ()->lBoldValue;
}

bool CDynamicBarcodeElement::SetBold (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, m_lmtBold)) {
		GetMember ()->lBoldValue = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

LONG CDynamicBarcodeElement::GetTextBold () const
{
	return GetMember ()->lHRBold;
}

bool CDynamicBarcodeElement::SetTextBold (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, m_lmtBold)) {
		GetMember ()->lHRBold = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

int CDynamicBarcodeElement::GetWidth () const
{
	return GetMember ()->lWidthValue;
}

bool CDynamicBarcodeElement::SetWidth (int nValue)
{
/* width is not used 
	if (FoxjetCommon::IsValid (nValue, m_lmtWidth)) {
		GetMember ()->lWidthValue = nValue;
		Invalidate ();
		return true;
	}
*/

	return false;
}


LONG CDynamicBarcodeElement::GetBcType () const
{
	return GetMember ()->lSysBCIndex;
}

bool CDynamicBarcodeElement::SetBcType (LONG lValue)
{
	LPFJDYNBARCODE p = GetMember ();

	if (lValue >= 0 && lValue < FJSYS_BARCODES) {
		p->lSysBCIndex = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

CHAR CDynamicBarcodeElement::GetTextAlign () const
{
	return GetMember ()->cHRAlign;
}

bool CDynamicBarcodeElement::SetTextAlign (CHAR cValue)
{
	if (cValue == 'L' || cValue == 'R' || cValue == 'C') {
		GetMember ()->cHRAlign = cValue;
		Invalidate ();
		return true;
	}

	return false;
}

void CDynamicBarcodeElement::Invalidate ()
{
	LoadGlobalParams ();
	m_nEncode = -1;
	CIpBaseElement::Invalidate ();
}

bool CDynamicBarcodeElement::Validate ()
{
	CString str = GetImageData ();

	LoadGlobalParams ();
	const FJSYSBARCODE & global = GetGlobal ();
	const ULONG lBCFlags = global.lBCFlags;
	
	m_nEncode = -1;

	if (Barcode_Item * p = Barcode_Create (w2a (str))) {
		m_nEncode = Barcode_Encode (p, lBCFlags);
		Barcode_Delete (p);
	}

	if (m_nEncode == -1) {
		// try to reimage again with default data

		str = fj_DynBarCodeDefData;
		TRACEF (str);

		if (Barcode_Item * p = Barcode_Create (w2a (str))) {
			m_nEncode = Barcode_Encode (p, lBCFlags);
			Barcode_Delete (p);
		}
	}

	return IsValid ();
}

bool CDynamicBarcodeElement::IsValid () const
{
	return m_nEncode == 0;
}


const FJSYSBARCODE & CDynamicBarcodeElement::GetGlobal () const
{
	LPCFJSYSTEM pSystem = GetSystem ();
	LPCFJDYNBARCODE p = GetMember ();
	
	ASSERT (pSystem);

	LONG lIndex = p->lSysBCIndex;
	ASSERT (lIndex >= 0 && lIndex < FJSYS_BARCODES);

	return pSystem->bcArray [lIndex];
}

static const ElementFields::FIELDSTRUCT lpszFields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType, 	_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszID,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszX,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszY,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszPosition,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszBold,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszWidth,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszFlipV,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFlipH,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszInverse,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszSysBCIndex,	_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszHROutput,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszHRAlign,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszHRBold,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszData,			_T ("0")),	
	ElementFields::FIELDSTRUCT (),
};

const FoxjetCommon::ElementFields::FIELDSTRUCT * CDynamicBarcodeElement::GetFieldBuffer () const
{
	return ::lpszFields;
}

LONG CDynamicBarcodeElement::GetFirstChar () const
{
	return GetMember ()->lFirstChar;
}

bool CDynamicBarcodeElement::SetFirstChar (LONG l)
{
	if (FoxjetCommon::IsValid (l, CDynamicTextElement::m_lmtLen)) {
		GetMember ()->lFirstChar = l;
		return true;
	}

	return false;
}

LONG CDynamicBarcodeElement::GetNumChar () const
{
	return GetMember ()->lNumChar;
}

bool CDynamicBarcodeElement::SetNumChar (LONG l)
{
	if (FoxjetCommon::IsValid (l, CDynamicTextElement::m_lmtLen)) {
		GetMember ()->lNumChar = l;
		return true;
	}

	return false;
}

