#if !defined(AFX_BARCODEPARAMDLG_H__01981D48_2272_4BF1_B73F_470BF1455426__INCLUDED_)
#define AFX_BARCODEPARAMDLG_H__01981D48_2272_4BF1_B73F_470BF1455426__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BarCodeParamDlg.h : header file
//

#include "Resource.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CBarcodeParamDlg dialog

class CBarcodeParamDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	virtual void OnOK();
	CBarcodeParamDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBarcodeParamDlg)
	enum { IDD = IDD_GLOBALBARCODE };
	int		m_nHeight;
	long	m_lHorzBearer;
	CString	m_strName;
	long	m_lVertBearer;
	long	m_lQuietZone;
	BOOL	m_bChecksum;
	//}}AFX_DATA

	LONG	m_lBCFlags;
	LONG	m_lBarVal [5];

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBarcodeParamDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual BOOL OnInitDialog ();

	// Generated message map functions
	//{{AFX_MSG(CBarcodeParamDlg)
	//}}AFX_MSG
	afx_msg void OnSelchangeType ();
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BARCODEPARAMDLG_H__01981D48_2272_4BF1_B73F_470BF1455426__INCLUDED_)
