#if !defined(AFX_DYNAMICBARCODEDLG_H__16C594D2_F221_497A_B341_B89187DE3A32__INCLUDED_)
#define AFX_DYNAMICBARCODEDLG_H__16C594D2_F221_497A_B341_B89187DE3A32__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DynamicBarcodeDlg.h : header file
//

#include "ElementDlg.h"
#include "DynamicBarcodeElement.h"

/////////////////////////////////////////////////////////////////////////////
// CDynamicBarcodeDlg dialog

class CDynamicBarcodeDlg : public CElementDlg
{
public:
	CDynamicBarcodeDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa,
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDynamicBarcodeDlg)
	enum { IDD = IDD_DYNAMICBARCODE };
	BOOL	m_bOutputText;
	int		m_dBold;
	int		m_nTextBold;
	int		m_nWidth;
	//}}AFX_DATA

	int		m_nType;
	CHAR	m_cAlign;
	LONG	m_lDynamicID;
	LONG	m_lFirstChar;
	LONG	m_lNumChar;

	virtual UINT GetDefCtrlID () const { return CB_DYNAMICID; }
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDynamicBarcodeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();
	FoxjetIpElements::CDynamicBarcodeElement & GetElement ();
	const FoxjetIpElements::CDynamicBarcodeElement & GetElement () const;

	virtual int BuildElement ();
	LONG GetCurSelDynamicID () const;

	// Generated message map functions
	//{{AFX_MSG(CDynamicBarcodeDlg)
	afx_msg void OnAscii();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeType();
	//}}AFX_MSG

	afx_msg void OnSelect();

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DYNAMICBARCODEDLG_H__16C594D2_F221_497A_B341_B89187DE3A32__INCLUDED_)
