// DataMatrixElement.cpp: implementation of the CDataMatrixElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "list.h"
#include "DataMatrixElement.h"
#include "Types.h"
#include "List.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"
#include "Database.h"
#include "Resource.h"
#include "TemplExt.h"

#include "fj_system.h"
#include "fj_printhead.h"
#include "fj_element.h"
#include "fj_datamatrix.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_font.h"
#include "fj_export.h"
#include "fj_message.h"
#include "fj_text.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetIpElements;
using namespace Color;
using namespace FoxjetCommon::ElementFields;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNAMIC (CDataMatrixElement, CIpBaseElement)

const FoxjetCommon::LIMITSTRUCT CDataMatrixElement::m_lmtCX				= { 8,	144	}; 
const FoxjetCommon::LIMITSTRUCT CDataMatrixElement::m_lmtCY				= { 8,	144	}; 
const FoxjetCommon::LIMITSTRUCT CDataMatrixElement::m_lmtHeightFactor	= { 0,	144	}; 
const FoxjetCommon::LIMITSTRUCT CDataMatrixElement::m_lmtWidthFactor	= { 0,	144	}; 

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDataMatrixElement::CDataMatrixElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head)
:	CIpBaseElement (fj_ElementDataMatrixNew (), pHead, head)
{
}

CDataMatrixElement::CDataMatrixElement (const CDataMatrixElement & rhs)
:	CIpBaseElement (rhs)
{
}

CDataMatrixElement & CDataMatrixElement::operator = (const CDataMatrixElement & rhs)
{
	CIpBaseElement::operator = (rhs);

	if (this != &rhs) {
	}

	return * this;
}

CDataMatrixElement::~CDataMatrixElement ()
{
}

int CDataMatrixElement::Build (BUILDTYPE type, bool bCanThrowException)
{
	return GetClassID ();
}

bool CDataMatrixElement::SetDefaultData(const CString &strData)
{
	strncpy (GetMember ()->strText, w2a (strData), FJDATAMATRIX_SIZE);
	Invalidate ();
	return true;
}

void CDataMatrixElement::CreateImage ()
{
	CIpBaseElement::CreateImage ();
}

CString CDataMatrixElement::GetImageData () const
{
	return a2w (GetMember ()->strText);
}

CString CDataMatrixElement::GetDefaultData () const
{
	return a2w (GetMember ()->strText);
}

LONG CDataMatrixElement::GetCX () const
{
	return GetMember ()->cx;
}

bool CDataMatrixElement::SetCX (LONG l)
{
	GetMember ()->cx = l;
	Invalidate ();
	return true;
}

LONG CDataMatrixElement::GetCY () const
{
	return GetMember ()->cy;
}

bool CDataMatrixElement::SetCY (LONG l)
{
	GetMember ()->cy = l;
	Invalidate ();
	return true;
}

LONG CDataMatrixElement::GetBcType () const
{
	return GetMember ()->lSysBCIndex;
}

bool CDataMatrixElement::SetBcType (LONG lValue)
{
	GetMember ()->lSysBCIndex = lValue;
	Invalidate ();
	return true;
}

int CDataMatrixElement::GetDynamicID () const
{
	return GetMember ()->lDynIndex;
}

bool CDataMatrixElement::SetDynamicID (LONG lID)
{
	GetMember ()->lDynIndex = lID;
	Invalidate ();
	return true;
}

LONG CDataMatrixElement::GetFirstChar () const
{
	return GetMember ()->lDynFirstChar;
}

bool CDataMatrixElement::SetFirstChar (LONG l)
{
	GetMember ()->lDynFirstChar = l;
	Invalidate ();
	return true;
}

LONG CDataMatrixElement::GetNumChar () const
{
	return GetMember ()->lDynNumChar;
}

bool CDataMatrixElement::SetNumChar (LONG l)
{
	GetMember ()->lDynNumChar = l;
	Invalidate ();
	return true;
}

static const ElementFields::FIELDSTRUCT lpszFields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType, 	_T ("DataMatrix")),
	ElementFields::FIELDSTRUCT (m_lpszID,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszX,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszY,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszPosition,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszFlipV,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFlipH,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszInverse,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszSysBCIndex,	_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszWidth,		_T ("16")),	
	ElementFields::FIELDSTRUCT (m_lpszHeight,		_T ("16")),	
	ElementFields::FIELDSTRUCT (m_lpszData,			_T ("")),	
	ElementFields::FIELDSTRUCT (m_lpszDynFirstChar,	_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszDynNumChar,	_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszDynIndex,		_T ("0")),	
	ElementFields::FIELDSTRUCT (),	
};

const FoxjetCommon::ElementFields::FIELDSTRUCT * CDataMatrixElement::GetFieldBuffer () const
{
	return ::lpszFields;
}

static bool bEnableDatamatrix = true;

bool CDataMatrixElement::IsDataMatrixEnabled ()
{
	return ::bEnableDatamatrix;
}

void CDataMatrixElement::EnableDataMatrix (bool bEnable)
{
	::bEnableDatamatrix = bEnable;
}
