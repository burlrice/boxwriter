#if !defined(AFX_BITMAPFONTDLG_H__CD3D9181_8CB1_4936_B9B9_8BF6D4CFBA46__INCLUDED_)
#define AFX_BITMAPFONTDLG_H__CD3D9181_8CB1_4936_B9B9_8BF6D4CFBA46__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BitmapFontDlg.h : header file
//

#include "Resource.h"
#include "Head.h"
#include "TextElement.h"
#include "ElementApi.h"
#include "PreviewCtrl.h"
#include "Utils.h"

FORWARD_DECLARE (fjfont, FJFONT);
FORWARD_DECLARE (fjprinthead, FJPRINTHEAD);

/////////////////////////////////////////////////////////////////////////////
// CBitmapFontDlg dialog

class ELEMENT_API CBitmapFontDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CBitmapFontDlg (FoxjetCommon::CHead & head, const CString & strFont, CWnd* pParent = NULL);   // standard constructor

public:									
	CString								m_strSample;
	int									m_nSampleWidth;
	CString								m_strFont;
	bool								m_bEditFontDir;
	bool								m_bAppInitialized;

	static bool CompareFonts (const LPVOID pLhs, const LPVOID pRhs);

// Dialog Data
	//{{AFX_DATA(CBitmapFontDlg)
	enum { IDD = IDD_BITMAPFONT };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBitmapFontDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnCancel ();
	void UpdateUI();
	static CString GetFontType (LONG lType);
	virtual void OnOK();
	void Refresh ();
	void InvalidatePreview();
	CString GetCurSelDirs () const;
	CString GetKey () const;
	CString GetSection () const;

	CStringArray						m_vDirs;
	CPreviewCtrl						m_wndPreview;
	FoxjetIpElements::CTextElement		m_element;
	FoxjetCommon::CHead *				m_pHead;
	CString								m_strFontDirs;

	// Generated message map functions
	//{{AFX_MSG(CBitmapFontDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeFont();
	afx_msg void OnAdd();
	afx_msg void OnDelete();
	afx_msg void OnEdit();
	afx_msg void OnDblclkDir();
	afx_msg void OnFit();
	afx_msg void OnChangeSample();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BITMAPFONTDLG_H__CD3D9181_8CB1_4936_B9B9_8BF6D4CFBA46__INCLUDED_)
