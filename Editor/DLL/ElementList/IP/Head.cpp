// Head.cpp: implementation of the CHead class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Head.h"
#include "Extern.h"
#include "fj_memstorage.h"
#include "fj_font.h"
#include "fj_printhead.h"
#include "fj_mem.h"
#include "fj_memstorage.h"
#include "fj_system.h"
#include "fj_export.h"
#include "Debug.h"
#include "Extern.h"
#include "Registry.h"
#include "Edit.h"
#include "BitmapFontDlg.h"
#include "Utils.h"
#include "Parse.h"

using namespace FoxjetDatabase;
using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace FoxjetIpElements;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////////////////////////////
static class CFontMemStore
{
public:
	CFontMemStore ();
	virtual ~CFontMemStore ();

	bool LoadFonts ();

	LPFJMEMSTOR GetMemStore ();
	LPCFJMEMSTOR GetMemStore () const;

	operator LPFJMEMSTOR () { return GetMemStore (); }
	operator const LPCFJMEMSTOR () const { return GetMemStore (); }

	LPFJMEMSTOR operator -> () { return GetMemStore (); }
	LPCFJMEMSTOR operator -> () const { return GetMemStore (); }

	CArray <LPFJFONT, LPFJFONT> m_vFonts;

protected:
	LPFJMEMSTOR m_pMemStore;

} fontMemStore;

CFontMemStore::CFontMemStore ()
:	m_pMemStore (NULL)
{
	LONG lSize = 0x00200000;
	LPBYTE pMemStoreBuffer = (LPBYTE)fj_calloc (1, lSize);

	ASSERT (pMemStoreBuffer);
	VERIFY (m_pMemStore = fj_MemStorNew (pMemStoreBuffer, lSize));
	m_pMemStore->bDoNotFreeBuffer = FALSE;
}

CFontMemStore::~CFontMemStore ()
{
	for (int i = 0; i < m_vFonts.GetSize (); i++) 
		fj_FontDestroy (m_vFonts [i]);

	m_vFonts.RemoveAll ();
	fj_MemStorDestroy (m_pMemStore);
	m_pMemStore = NULL;
}

LPFJMEMSTOR CFontMemStore::GetMemStore ()
{
	ASSERT (m_pMemStore);
	return m_pMemStore;
}

LPCFJMEMSTOR CFontMemStore::GetMemStore () const
{
	ASSERT (m_pMemStore);
	return m_pMemStore;
}

bool CFontMemStore::LoadFonts ()
{
	bool bResult = false;
	const CString strSuffix = _T ("\\*.fjf");
	WIN32_FIND_DATA fd;
	CStringArray v;

	CHead::GetFontPaths (v);
	ASSERT (m_pMemStore);

	fj_MemStorClear (m_pMemStore);

	for (int i = 0; i < v.GetSize (); i++) {
		CString strPath = v [i];

		if (strPath.Find (strSuffix) != -1)
			strPath.Replace (strSuffix, _T (""));

		if (strPath [strPath.GetLength () - 1] != '\\')
			strPath += '\\';

		HANDLE hFile = ::FindFirstFile (strPath + strSuffix, &fd);

		if (hFile != INVALID_HANDLE_VALUE) {
			do {
				CString strFile = fd.cFileName;

				if (fd.nFileSizeHigh) {
					TRACEF ("File: '" + strFile + "' too large to handle");
					continue;
				}

				if (FILE * fp = _tfopen (strPath + strFile, _T ("rb"))) {
					int nSize = fd.nFileSizeLow;
					BYTE * pBuffer = new BYTE [nSize];

					::ZeroMemory (pBuffer, nSize);
					int nRead = fread (pBuffer, 1, nSize, fp);

					fclose (fp);

					if (LPFJFONT pFont = fj_FontFromBuffer (pBuffer)) {
						ASSERT (pFont->pBuffer);
						ASSERT (pFont->pWBuffer);

						if (fj_FontAddToMemstor (m_pMemStore, pFont)) {
							m_vFonts.Add (pFont);
							TRACEF ("Loaded: " + strFile);
							bResult = true;
						}
						else
							fj_FontDestroy (pFont);
					}
					else
						TRACEF ("Load failed: " + strFile);

					delete pBuffer;
				}
			}
			while (::FindNextFile (hFile, &fd));
		}

		::FindClose (hFile);
	}

	return bResult;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
extern CArray <LPFJSYSTEM, LPFJSYSTEM> vIpSystems;

static LPFJPRINTHEAD GetFirstHead ()
{
	for (int i = 0; i < ::vIpSystems.GetSize (); i++) 
		for (int j = 0; j < FJSYS_NUMBER_PRINTERS; j++) 
			if (LPFJPRINTHEAD p = ::vIpSystems [i]->gmMembers [j].pfph) 
				return p;

	return NULL;
}

bool ELEMENT_API CALLBACK FoxjetIpElements::OnDefineFonts (CWnd * pParent, const CVersion & version) 
{
	LPFJPRINTHEAD pHead = GetFirstHead ();
	ASSERT (pHead);
	CHead head (pHead);

	LPSTR * p = fj_MemStorFindAllElements (::fontMemStore, fj_FontID);

	if ((* p) == NULL) {
		pHead->pMemStor = pHead->pMemStorEdit = ::fontMemStore;
		fj_PrintHeadLoadTestFonts (pHead);
	}

	CBitmapFontDlg dlg (head, _T (""), pParent);

	dlg.m_bEditFontDir = true;

	// TODO: must restart because heads never get initialized

	return (dlg.DoModal () == IDOK);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////


CHead::CHead(LPFJPRINTHEAD pHead)
:	m_pFjHead (pHead)
{
	ASSERT (m_pFjHead);
	
	ULONG lHeadID = strtoul (m_pFjHead->strID, NULL, 10);
	FoxjetDatabase::COdbcDatabase & database = ListGlobals::GetDB ();
	
	ASSERT (lHeadID > 0);
	VERIFY (GetHeadRecord (lHeadID, m_dbHead));
	m_pFjHead->pfphy->lChannels = m_dbHead.GetChannels ();
}

CHead::CHead (const CHead & rhs)
:	m_pFjHead (rhs.m_pFjHead),
	m_dbHead (rhs.m_dbHead)
{
}

CHead & CHead::operator = (const CHead & rhs)
{
	if (this != &rhs) {
		m_pFjHead = rhs.m_pFjHead;
		m_dbHead = rhs.m_dbHead;
	}

	return * this;
}

CHead::~CHead()
{
}

void CHead::GetFontPaths (CStringArray & v)
{
	CString strDef = GetHomeDir () + _T ("\\Fonts");

	const CString strSection = defElements.m_strRegSection + _T ("\\Fonts");
	const CString strSuffix = _T ("\\*.fjf");
	CString strFontPaths = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strSection, _T ("Path"), 
		strDef);

	Tokenize (strFontPaths, v, ';');

	for (int i = 0; i < v.GetSize (); i++) {
		CString strPath = v [i];

		if (strPath.Find (strSuffix) != -1)
			strPath.Replace (strSuffix, _T (""));

		if (strPath [strPath.GetLength () - 1] != '\\')
			strPath += '\\';

		v [i] = strPath;
	}
}

CHead::FONTSTATE CHead::InitFonts ()
{	
	FONTSTATE result = FONTS_OK;

	while (!::fontMemStore.LoadFonts ()) {
		CStringArray v;
		CString str = LoadString (IDS_NOFONTSLOADED);

		result = FONTS_RELOAD;
		CHead::GetFontPaths (v);

		for (int i = 0; i < v.GetSize (); i++) 
			str += _T ("\n") + v [i];

		TRACEF (str);
		MsgBox (str, LoadString (IDS_WARNING), MB_OK | MB_ICONSTOP);

//		if (!OnDefineFonts (::AfxGetMainWnd (), CVersion ()))
			return FONTS_CANCEL;
	}

	return result;
}

void CHead::SetFonts (LPFJPRINTHEAD pHead)
{
	int nCount = ::fontMemStore.m_vFonts.GetSize ();

	pHead->papffSelected = new LPFJFONT [nCount + 1];
	::ZeroMemory (pHead->papffSelected, sizeof (LPFJFONT) * (nCount + 1));

	for (int i = 0; i < nCount; i++) 
		pHead->papffSelected [i] = ::fontMemStore.m_vFonts [i];
}

void CHead::FreeFonts (LPFJPRINTHEAD pHead)
{
	if (pHead->papffSelected) {
		delete [] pHead->papffSelected;
		pHead->papffSelected = NULL;
	}
}

bool CHead::GetFonts (CArray <CPrinterFont, CPrinterFont &> &v) const
{
	const int nMax = m_dbHead.GetChannels ();
	CString strBarcode32 = fj_BarCodeFont32;
	CString strBarcode256 = fj_BarCodeFont256;

	strBarcode32.MakeLower ();
	strBarcode256.MakeLower ();
	v.RemoveAll ();

	if (IsHpHead (m_dbHead.m_nHeadType)) {
		CPrinterFont f;
		f.m_strName = "TODO: hp font";
		v.Add (f);
	}
	else {
		for (int i = 0; m_pFjHead->papffSelected [i]; i++) {
			LPFJFONT pFont = m_pFjHead->papffSelected [i];
			CString strName = pFont->strName;

			strName.MakeLower ();

			if (strName.Find (strBarcode32) != -1 ||
				strName.Find (strBarcode256) != -1)
			{
				continue;
			}

			// only submit fonts that can fit on this head
			if (pFont->lHeight <= nMax)
				v.Add (CPrinterFont (pFont->strName));
		}
	}

	return v.GetSize () > 0;
}

bool CHead::GetFonts (LPCFJPRINTHEAD pHead, CArray <CPrinterFont, CPrinterFont &> &v)
{
	HEADSTRUCT dbHead;

	VERIFY (FoxjetDatabase::GetHeadRecord (GetDB (), atol (pHead->strID), dbHead));
	const int nMax = dbHead.GetChannels ();

	v.RemoveAll ();

	if (pHead->papffSelected) {
		for (int i = 0; pHead->papffSelected [i]; i++) {
			LPFJFONT pFont = pHead->papffSelected [i];

			// only submit fonts that can fit on this head
			if (pFont->lHeight <= nMax)
				v.Add (CPrinterFont (pFont->strName));
		}
	}

	return v.GetSize () > 0;
}

bool CHead::GetFont(const CString &str, CPrinterFont &fnt) const
{
	for (int i = 0; m_pFjHead->papffSelected [i]; i++) {
		LPFJFONT pFont = m_pFjHead->papffSelected [i];

		if (pFont->strName == str) {
			fnt.m_strName = pFont->strName;
			fnt.m_nSize = pFont->lHeight;
			fnt.m_bBold = false;
			fnt.m_bItalic = false;
			return true;
		}
	}

	return false;
}

bool CHead::GetFont(const CString &str, LPFJFONT * pResult) const
{
	for (int i = 0; m_pFjHead->papffSelected [i]; i++) {
		LPFJFONT pFont = m_pFjHead->papffSelected [i];

		if (pFont->strName == str) {
			* pResult = pFont;
			return true;
		}
	}

	return false;
}
