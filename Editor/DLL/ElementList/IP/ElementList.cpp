// ElementList.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"

#include <afxdllx.h>
#include <shlwapi.h>

#include "Debug.h"
#include "DllVer.h"
#include "AppVer.h"
#include "Database.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static AFX_EXTENSION_MODULE ElementListDLL = { NULL, NULL };
HINSTANCE hInstance = NULL;

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACEF (_T ("IPELEMENTLIST.DLL Initializing!\n"));
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(ElementListDLL, hInstance)) {
			MESSAGEBOX (_T ("AfxInitExtensionModule failed"));
			return 0;
		}

		// Insert this DLL into the resource chain
		// NOTE: If this Extension DLL is being implicitly linked to by
		//  an MFC Regular DLL (such as an ActiveX Control)
		//  instead of an MFC application, then you will want to
		//  remove this line from DllMain and put it in a separate
		//  function exported from this Extension DLL.  The Regular DLL
		//  that uses this Extension DLL should then explicitly call that
		//  function to initialize this Extension DLL.  Otherwise,
		//  the CDynLinkLibrary object will not be attached to the
		//  Regular DLL's resource chain, and serious problems will
		//  result.

		::hInstance = hInstance;
		new CDynLinkLibrary(ElementListDLL);
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("IPELEMENTLIST.DLL Terminating!\n");
		// Terminate the library before destructors are called
		AfxTermExtensionModule(ElementListDLL);
	}
	return 1;   // ok
}

HINSTANCE GetInstanceHandle ()
{
	return ::hInstance;
}

void CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer)
{
	if (lpVer) {
		* lpVer = FoxjetCommon::verApp; // memcpy (lpVer, &FoxjetCommon::verApp, sizeof (FoxjetCommon::VERSIONSTRUCT));
	}
}

HRESULT CALLBACK DllGetVersion (DLLVERSIONINFO * pdvi)
{
	FoxjetCommon::VERSIONSTRUCT ver;

	fj_GetDllVersion (&ver);

	if (pdvi) {
		pdvi->dwMajorVersion	= ver.m_nMajor;
		pdvi->dwMinorVersion	= ver.m_nMinor;
		pdvi->dwBuildNumber		= ver.m_nInternal;
		pdvi->dwPlatformID		= DLLVER_PLATFORM_WINDOWS;
	}

	return NOERROR;
}

CString LoadString (UINT nID)
{
	return FoxjetDatabase::LoadString (nID, _T ("IpElementList.dll"));
}

