// BitmapConfigDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "BitmapConfigDlg.h"
#include "BitmapElement.h"

using namespace FoxjetIpElements;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBitmapConfigDlg dialog


CBitmapConfigDlg::CBitmapConfigDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(CBitmapConfigDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBitmapConfigDlg)
	m_strEditor = _T("");
	m_strPath = _T("");
	//}}AFX_DATA_INIT
}


void CBitmapConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBitmapConfigDlg)
	DDX_Text(pDX, TXT_EDITOR, m_strEditor);
	DDX_Text(pDX, TXT_PATH, m_strPath);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBitmapConfigDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CBitmapConfigDlg)
	ON_BN_CLICKED(BTN_BROWSEEDITOR, OnBrowseeditor)
	ON_BN_CLICKED(BTN_BROWSEPATH, OnBrowsepath)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBitmapConfigDlg message handlers

void CBitmapConfigDlg::OnBrowseeditor() 
{
	CString str;

	GetDlgItemText (TXT_EDITOR, str);

	CFileDialog dlg (TRUE, _T ("*.exe"), str, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("Applications (*.exe)|*.exe|All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) 
		SetDlgItemText (TXT_EDITOR, dlg.GetPathName ());
}

void CBitmapConfigDlg::OnBrowsepath() 
{
	CString str = CBitmapElement::GetDefFilename ();

	CFileDialog dlg (TRUE, _T ("*.bmp"), str, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("Bitmaps (*.bmp)|*.bmp|All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		CString strPath = dlg.GetPathName ();
		int nIndex = strPath.ReverseFind ('\\');

		if (nIndex != -1) {
			CString str = strPath.Left (nIndex) + '\\';
			SetDlgItemText (TXT_PATH, str);
		}
	}
}
