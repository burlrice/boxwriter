// PrinterFont.h: interface for the CPrinterFont class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PRINTERFONT_H__6CB62663_8AD9_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_PRINTERFONT_H__6CB62663_8AD9_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ElementApi.h"

namespace FoxjetCommon {
	class ELEMENT_API CPrinterFont  
	{
	public:
		CPrinterFont (const CString & strName = _T ("Arial"), UINT nSize = 30,
			bool bBold = true, bool bItalic = false);
		CPrinterFont (const LOGFONT & lf);
		CPrinterFont (const CPrinterFont & rhs);
		CPrinterFont & operator = (const CPrinterFont & rhs);
		virtual ~CPrinterFont();

		CString m_strName;
		UINT m_nSize;
		bool m_bBold;
		bool m_bItalic;
	};
}; // FoxjetCommon

inline bool operator == (const FoxjetCommon::CPrinterFont & rhs, const FoxjetCommon::CPrinterFont & lhs)
{
	return 
		(rhs.m_strName == lhs.m_strName) &&
		(rhs.m_nSize == lhs.m_nSize) &&
		(rhs.m_bBold == lhs.m_bBold) &&
		(rhs.m_bItalic == lhs.m_bItalic);
}

inline bool operator != (const FoxjetCommon::CPrinterFont & rhs, const FoxjetCommon::CPrinterFont & lhs)
{
	return !(rhs == lhs);
}

#endif // !defined(AFX_PRINTERFONT_H__6CB62663_8AD9_11D4_8FC6_006067662794__INCLUDED_)
