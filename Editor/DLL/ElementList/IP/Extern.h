#ifndef __ELEMENTLIST_EXTERN_H__
#define __ELEMENTLIST_EXTERN_H__

#include "OdbcDatabase.h"
#include "ElementDefaults.h"
#include "Head.h"

namespace ListGlobals
{
	namespace Keys
	{
		static const CString m_strBarcode		= _T ("IP Barcode");
		static const CString m_strTime			= _T ("IP Time");
		static const CString m_strDays			= _T ("IP Days");
		static const CString m_strDataMatrix	= _T ("IP DataMatrix");
	}; //namespace Keys

	extern ELEMENT_API FoxjetCommon::CElementDefaults defElements;

	ELEMENT_API FoxjetDatabase::COdbcDatabase & GetDB ();
	ELEMENT_API bool SetDB (FoxjetDatabase::COdbcDatabase & db);
}; //namespace ListGlobals

#endif //__ELEMENTLIST_EXTERN_H__
