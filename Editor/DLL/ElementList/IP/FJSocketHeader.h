#if !defined( _FJSOCKETHEADER_H__)
#define  _FJSOCKETHEADER_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define FJSH_BUFSIZE 12


class CFJSocketHeader
{
public:
	CFJSocketHeader();
	~CFJSocketHeader();
public:
	void SetBuffer(BYTE* pBuf);
	UINT GetBufferSize();
	BYTE* GetBuffer();

	void SetDataLength (ULONG lDLen);
	ULONG GetDataLength();
	void SetSocketCMD(ULONG lCmd);
	ULONG GetSocketCMD();
	void SetBachID(ULONG lID);
	ULONG GetBachID();
private:
	ULONG m_BachID;
	ULONG m_DataLength;
	ULONG m_SocketCmd;
	BYTE* pBuffer;

	struct fj_socket_header
	{
		UCHAR	dCmd[4];
		UCHAR	dBatchID[4];
		UCHAR	dDataLenght[4];
	};// FJ_SOCKET_HEADER;
};
#endif	//!defined( _FJSOCKETHEADER_H__)
