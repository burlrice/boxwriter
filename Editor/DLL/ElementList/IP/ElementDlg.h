#if !defined(AFX_ELEMENTDLG_H__6CB62658_8AD9_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_ELEMENTDLG_H__6CB62658_8AD9_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ElementDlg.h : header file
//

#include "resource.h"
#include "List.h"
#include "BaseElement.h"
#include "PreviewCtrl.h"
#include "Utils.h"

void DDV_ID (CDataExchange * pDX, UINT nElementID, const FoxjetCommon::CBaseElement & e, 
	const FoxjetCommon::CElementList & v, UINT nCtrlID);

/////////////////////////////////////////////////////////////////////////////
// CElementDlg dialog

class CElementDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CElementDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		UINT nDlgID, const FoxjetCommon::CElementList * pList,
		CWnd* pParent = NULL);   // standard constructor

	virtual ~CElementDlg ();

	CPoint m_pt;
	UNITS m_units;
	bool m_bReadOnly;
	bool m_bReadOnlyLocation;
	bool m_bReadOnlyID;
	UINT m_nID;
	BOOL m_bFlipH;
	BOOL m_bFlipV;
	BOOL m_bInverse;
	BOOL m_bPreview;

	FoxjetCommon::CElementListArray * m_pAllLists;

	void SetDlgItemText (int nID, LPCTSTR lpszString);
	void SetDlgItemInt (int nID, UINT nValue, BOOL bSigned = TRUE);

	const CSize & GetPA () const { return m_pa; }

	virtual UINT GetDefCtrlID () const { return -1; }

	bool IsDefault () const { return m_bSetDefault; }

	static bool GetCheck (const CWnd * pWnd, UINT nID);

private:
	const CSize m_pa;
	ULONG m_lLineID;

protected:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CElementDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

	const FoxjetDatabase::HEADSTRUCT & GetHead () const;
	ULONG GetLineID () const;
	afx_msg void OnClickPreview();
	afx_msg void InvalidatePreview();
	afx_msg void OnDefault ();
	CString GetCtrlText (UINT nID) const;

	virtual CSize GetElementSize ();
	virtual int BuildElement () = 0; 
	virtual void OnOK ();

	bool IsPreviewEnabled() const;
	FoxjetCommon::CBaseElement & GetElement ();
	const FoxjetCommon::CBaseElement & GetElement () const;

	// Generated message map functions
	//{{AFX_MSG(CElementDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	//void AFXAPI DDV_SizeInThousandths (CDataExchange *pDX, const CSize & size,
	//	const CSize & min, const CSize & max, UINT nWidthCtrlID, UINT nHeightCtrlID);

private:
	FoxjetCommon::CBaseElement * m_pElement;
	const FoxjetCommon::CElementList * m_pList;

protected:
	bool GetCheck (UINT nID) const;
	int GetCtrlFontSize (UINT nID);
	const FoxjetCommon::CElementList * GetList ();

	CPreviewCtrl m_wndPreview;
	CFont m_fntCtrl;
	bool m_bSetDefault;

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ELEMENTDLG_H__6CB62658_8AD9_11D4_8FC6_006067662794__INCLUDED_)
