// TextDlg.cpp: implementation of the CTextDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TextDlg.h"
#include "TextElement.h"
#include "fj_element.h"
#include "fj_text.h"
#include "fj_printhead.h"
#include "fj_font.h"
#include "fj_export.h"
#include "Head.h"
#include "Debug.h"

using namespace FoxjetCommon;
using namespace FoxjetIpElements;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTextDlg::CTextDlg(const FoxjetCommon::CBaseElement & e, 
				   const CSize & pa, const FoxjetCommon::CElementList * pList, 
				   CWnd* pParent)
:	CElementDlg(e, pa, CTextDlg::IDD, pList, pParent)
{
	const CTextElement & text = GetElement ();
	const LPFJTEXT pText = text.GetMember ();
	ASSERT (pText);

	m_lBold		= pText->lBoldValue;
	m_lWidth	= pText->lWidthValue;
	m_lGap		= pText->lGapValue;
	m_strFont	= pText->strFontName;
	m_nAlign	= 0;
	
	switch (pText->cAlign) {
	case 'C': m_nAlign = 1;	break;
	case 'R': m_nAlign = 2;	break;
	}

	//{{AFX_DATA_INIT(CTextDlg)
	//}}AFX_DATA_INIT
}

CTextDlg::CTextDlg(const FoxjetCommon::CBaseElement & e, 
				   const CSize & pa, UINT nDlgID,
				   const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	
	CElementDlg(e, pa, nDlgID, pList, pParent)
{
}


void CTextDlg::DoDataExchange(CDataExchange* pDX)
{
	CElementDlg::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CTextDlg)
	//}}AFX_DATA_MAP
	
	DDX_Text(pDX, TXT_TEXT, m_strData);
	//TRACEF (Format (m_strData, m_strData.GetLength ()));

	DDV_MinMaxChars (pDX, m_strData, TXT_TEXT, CTextElement::m_lmtString);

	DDX_CBString (pDX, CB_FONT, m_strFont);

	DDX_Text (pDX, TXT_BOLD, m_lBold);
	DDV_MinMaxInt (pDX, m_lBold, CIpBaseElement::m_lmtBold.m_dwMin, CIpBaseElement::m_lmtBold.m_dwMax); 

	DDX_Text (pDX, TXT_WIDTH, m_lWidth);
	DDV_MinMaxInt (pDX, m_lWidth, CIpBaseElement::m_lmtWidth.m_dwMin, CIpBaseElement::m_lmtWidth.m_dwMax); 

	DDX_Text (pDX, TXT_GAP, m_lGap);
	DDV_MinMaxInt (pDX, m_lGap, CIpBaseElement::m_lmtGap.m_dwMin, CIpBaseElement::m_lmtGap.m_dwMax); 

	DDX_Radio (pDX, RDO_ALIGNLEFT, m_nAlign);
}


BEGIN_MESSAGE_MAP(CTextDlg, CElementDlg)
	//{{AFX_MSG_MAP(CTextDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (RDO_ALIGNCENTER, InvalidatePreview)
	ON_BN_CLICKED (RDO_ALIGNRIGHT, InvalidatePreview)
	ON_BN_CLICKED (RDO_ALIGNLEFT, InvalidatePreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTextDlg message handlers


BOOL CTextDlg::OnInitDialog() 
{
	CComboBox * pFont = (CComboBox *)GetDlgItem (CB_FONT);

	ASSERT (pFont);

	CElementDlg::OnInitDialog();

	CArray <CPrinterFont, CPrinterFont &> vFonts;
	const CHead & head = GetList ()->GetHead (0);
	head.GetFonts (vFonts);

	for (int i = 0; i < vFonts.GetSize (); i++) {
		CString str = vFonts [i].m_strName;
		int nIndex = pFont->AddString (str);

		if (str == m_strFont)
			pFont->SetCurSel (nIndex);
	}

	return FALSE;
}

int CTextDlg::BuildElement ()
{
	CTextElement & e = GetElement ();
	CString strText, strFont;

	GetDlgItemText (TXT_TEXT, strText);
	GetDlgItemText (CB_FONT, strFont);

	e.SetFontName (strFont);
	e.SetBold (GetDlgItemInt (TXT_BOLD));
	e.SetGap (GetDlgItemInt (TXT_GAP));
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));
	e.SetFlippedH (GetCheck (CHK_FLIPH));
	e.SetFlippedV (GetCheck (CHK_FLIPV));
	e.SetInverse (GetCheck (CHK_INVERSE));
	e.SetDefaultData (strText);

	if (GetCheck (RDO_ALIGNCENTER))
		e.SetAlignment ('C'); 
	else if (GetCheck (RDO_ALIGNRIGHT))
		e.SetAlignment ('R'); 
	else
		e.SetAlignment ('L'); 

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

CTextElement & CTextDlg::GetElement ()
{
	CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CTextElement)));
	return (CTextElement &)e;
}

const CTextElement & CTextDlg::GetElement () const
{
	const CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CTextElement)));
	return (CTextElement &)e;
}
