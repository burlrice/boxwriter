// ElementDefaults.h: interface for the CElementDefaults class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ELEMENTDEFAULTS_H__EB1DC861_8310_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_ELEMENTDEFAULTS_H__EB1DC861_8310_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "List.h"
#include "ElementApi.h"

namespace FoxjetCommon {
	class ELEMENT_API CElementDefaults  
	{
	public:
		CElementDefaults();
		virtual ~CElementDefaults();

		const CBaseElement & GetElement (int nType) const;
		bool SetElement (int nType, const CString & str, const CVersion & ver);
		bool SetElement (int nType, const CBaseElement & obj, const CVersion & ver);

		void SaveRegDefs (const CVersion & ver);
		void LoadRegDefs (const CVersion & ver);
		bool LoadRegElementDefs (int nType, const CVersion & ver);
		bool SaveRegElementDefs (int nType, const CVersion & ver);
		CString GetRegElementDefs (int nType, const CVersion & ver) const;
		void SetElementFonts (const CString & strFont, int nSize,
			bool bBold, bool bItalic, const CVersion & ver);
		
		bool Reset (const CVersion & ver);

		UNITS GetUnits () const;
		bool SetUnits (UNITS units);
		
		CSize GetProductArea () const;
		bool SetProductArea (const CSize & pa);

		bool SetHeadID (ULONG lHeadID);
		ULONG GetHeadID () const;

		CString m_strRegSection;

		bool GetOverlappedWarning () const;
		void SetOverlappedWarning (bool bWarn);

	protected:
		class CDefault 
		{
		public:
			CDefault (int nClassID = FIRST);
			CDefault (const CDefault & rhs);
			virtual ~CDefault ();

			CDefault & operator = (const CDefault & rhs);

			const FoxjetCommon::CBaseElement & GetElement () const; 
			FoxjetCommon::CBaseElement & GetElement (); 

			int GetClassID () const { return m_nClassID; }
			int GetClassID () { return m_nClassID; }

			operator const FoxjetCommon::CBaseElement & () const { return GetElement (); }
			operator FoxjetCommon::CBaseElement & () { return GetElement (); }

			const FoxjetCommon::CBaseElement & operator-> () const { return GetElement (); }
			FoxjetCommon::CBaseElement & operator-> () { return GetElement (); }

			operator const int () const { return GetClassID (); }
			operator int () { return GetClassID (); }

		protected:
			FoxjetCommon::CBaseElement *	m_pElement;
			int								m_nClassID;
		};

		void Init ();
		void Free ();

		CBaseElement & GetElement (int nType, int);

		CSize m_paDef;
		CArray <CDefault *, CDefault *> m_vDefElements;
		bool m_bOverlapped;
	};
}; // FoxjetCommon

#endif // !defined(AFX_ELEMENTDEFAULTS_H__EB1DC861_8310_11D4_8FC6_006067662794__INCLUDED_)
