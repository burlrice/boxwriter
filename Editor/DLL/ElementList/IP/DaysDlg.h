#if !defined(AFX_DAYSDLG_H__83B81D34_867B_4EB7_BEEC_0F5D8F9A645C__INCLUDED_)
#define AFX_DAYSDLG_H__83B81D34_867B_4EB7_BEEC_0F5D8F9A645C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DaysDlg.h : header file
//

#include "Resource.h"
#include "ListCtrlImp.h"
#include "ElementApi.h"
#include "Utils.h"

FORWARD_DECLARE (fjsystem, FJSYSTEM)

/////////////////////////////////////////////////////////////////////////////
// CDaysDlg dialog

namespace FoxjetIpElements
{
	class ELEMENT_API CDaysDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CDaysDlg(CWnd* pParent = NULL);   // standard constructor
		virtual ~CDaysDlg ();

	// Dialog Data
		//{{AFX_DATA(CDaysDlg)
		enum { IDD = IDD_DAYS };
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		ULONG	m_lLineID;
		float	m_fRolloverHours;
		long	m_lWeek1;
		long	m_lWeek53;
		long	m_lWeekDay;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CDaysDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

		static const FoxjetCommon::LIMITSTRUCT m_lmtRollover;
		static const FoxjetCommon::LIMITSTRUCT m_lmtWeek1;
		static const FoxjetCommon::LIMITSTRUCT m_lmtWeek53;
		static const FoxjetCommon::LIMITSTRUCT m_lmtWeekDay;

	// Implementation
	protected:
		virtual void OnOK();

		class CItem : public ItiLibrary::ListCtrlImp::CItem
		{
		public:
			CItem (const CString & str, int nIndex);
			virtual ~CItem ();

			virtual CString GetDispText (int nColumn) const;
			virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;

			CString m_str;
			int m_nIndex;
		};

		int GetTab () const;
		LPFJSYSTEM GetCurSystem () const;
		bool Apply();

		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv [6];
		CArray <LPFJSYSTEM, LPFJSYSTEM> m_vSystems;

		// Generated message map functions
		//{{AFX_MSG(CDaysDlg)
		afx_msg void OnSelchangeType(NMHDR* pNMHDR, LRESULT* pResult);
		virtual BOOL OnInitDialog();
		afx_msg void OnDblclkType(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnEdit();
		afx_msg void OnSelchangeLine();
		afx_msg void OnChange();
		afx_msg void OnApply();
	//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace FoxjetIpElements

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DAYSDLG_H__83B81D34_867B_4EB7_BEEC_0F5D8F9A645C__INCLUDED_)
