#if !defined(AFX_BITMAPCONFIGDLG_H__B238C54C_CB04_411C_BE7A_E0E96B4B45F4__INCLUDED_)
#define AFX_BITMAPCONFIGDLG_H__B238C54C_CB04_411C_BE7A_E0E96B4B45F4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BitmapConfigDlg.h : header file
//

#include "resource.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CBitmapConfigDlg dialog

class CBitmapConfigDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CBitmapConfigDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBitmapConfigDlg)
	enum { IDD = IDD_BITMAPCONFIG };
	CString	m_strEditor;
	CString	m_strPath;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBitmapConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBitmapConfigDlg)
	afx_msg void OnBrowseeditor();
	afx_msg void OnBrowsepath();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BITMAPCONFIGDLG_H__B238C54C_CB04_411C_BE7A_E0E96B4B45F4__INCLUDED_)
