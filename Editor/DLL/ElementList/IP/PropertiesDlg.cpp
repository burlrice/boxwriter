// PropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "List.h"
#include "PropertiesDlg.h"
#include "Extern.h"
#include "Database.h"
#include "Resource.h"
#include "fj_label.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ListGlobals;
using namespace ItiLibrary;

CPropertiesDlg::CItem::CItem (const FoxjetCommon::CElementList & list)
:	m_strVersion (list.GetVersion ().Format (false)),
	m_strHead (list.GetHead ().m_strName)
{
}

CPropertiesDlg::CItem::~CItem ()
{
}

CString CPropertiesDlg::CItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:		return m_strHead;
	case 1:		return m_strVersion;
	}

	return CString (_T (""));
}

/////////////////////////////////////////////////////////////////////////////
// CPropertiesDlg dialog

CPropertiesDlg::CPropertiesDlg(CWnd* pParent /*=NULL*/)
:	m_lBoxID (-1),
	m_lLineID (-1),
	m_bReadOnly (false),
	m_lExpValue (0),
	m_lExpUnits (FJ_LABEL_EXP_DAYS),
	m_bExpRoundUp (FALSE),
	m_bExpRoundDown (FALSE),
	FoxjetCommon::CEliteDlg(CPropertiesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPropertiesDlg)
	m_strDesc = _T("");
	m_strDownload = _T("");
	m_strName = _T("");
	//}}AFX_DATA_INIT
}


void CPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropertiesDlg)
	DDX_Text(pDX, TXT_DESC, m_strDesc);
	DDX_Text(pDX, TXT_DOWNLOADSTRING, m_strDownload);
	DDX_Text(pDX, TXT_NAME, m_strName);
	//}}AFX_DATA_MAP

	DDX_Text (pDX, TXT_EXPVALUE, m_lExpValue);
	DDX_Check (pDX, CHK_EXPROUNDUP, m_bExpRoundUp);
	DDX_Check (pDX, CHK_EXPROUNDDOWN, m_bExpRoundDown);
}


BEGIN_MESSAGE_MAP(CPropertiesDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CPropertiesDlg)
	ON_BN_CLICKED(CHK_EXPROUNDUP, OnExproundup)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(CHK_EXPROUNDDOWN, OnExprounddown)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropertiesDlg message handlers

BOOL CPropertiesDlg::OnInitDialog() 
{
	struct
	{
		LONG m_lExpUnits;
		UINT m_nID;
	} const exp [] = 
	{
		{ FJ_LABEL_EXP_DAYS,		ID_FJ_LABEL_EXP_DAYS	},
		{ FJ_LABEL_EXP_WEEKS,		ID_FJ_LABEL_EXP_WEEKS	},
		{ FJ_LABEL_EXP_MONTHS,		ID_FJ_LABEL_EXP_MONTHS	},
		{ FJ_LABEL_EXP_YEARS,		ID_FJ_LABEL_EXP_YEARS	},
	};
	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;
	CArray <BOXSTRUCT, BOXSTRUCT &> vBoxes;
	CComboBox * pBox = (CComboBox *)GetDlgItem (CB_BOX);
	CComboBox * pUnits = (CComboBox *)GetDlgItem (CB_EXPUNITS);

	GetDlgItem (IDOK)->EnableWindow (!m_bReadOnly);

	ASSERT (pBox);
	ASSERT (pUnits);
	
	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();
	
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_HEAD)));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_VERSION_)));

/* TODO: rem
	m_lv.Create (LV_MESSAGE, vCols, this, defElements.m_strRegSection);
	
	for (int i = 0; i < m_vMsgs.GetSize (); i++) {
		CElementList l;

		l.FromString (m_vMsgs [i], CVersion ());
		m_lv.InsertCtrlData (new CItem (l));
	}
*/

	GetBoxRecords (GetDB (), m_lLineID, vBoxes);

	for (int i = 0; i < vBoxes.GetSize (); i++) {
		int nIndex = pBox->AddString (vBoxes [i].m_strName);
		pBox->SetItemData (nIndex, vBoxes [i].m_lID);

		if (vBoxes [i].m_lID == m_lBoxID)
			pBox->SetCurSel (nIndex);
	}

	for (int i = 0; i < sizeof (exp) / sizeof (exp [0]); i++) {
		int nIndex = pUnits->AddString (LoadString (exp [i].m_nID));
		pUnits->SetItemData (nIndex, exp [i].m_lExpUnits);

		if (m_lExpUnits == exp [i].m_lExpUnits)
			pUnits->SetCurSel (nIndex);
	}

	return bResult;
}

void CPropertiesDlg::OnOK()
{
	CComboBox * pBox = (CComboBox *)GetDlgItem (CB_BOX);
	CComboBox * pUnits = (CComboBox *)GetDlgItem (CB_EXPUNITS);

	ASSERT (pBox);
	ASSERT (pUnits);

	int nIndex = pBox->GetCurSel ();
	ASSERT (nIndex != CB_ERR);
	m_lBoxID = pBox->GetItemData (nIndex);

	nIndex = pUnits->GetCurSel ();
	ASSERT (nIndex != CB_ERR);
	m_lExpUnits = pUnits->GetItemData (nIndex);

	FoxjetCommon::CEliteDlg::OnOK ();
}

void CPropertiesDlg::OnExproundup() 
{
	CButton * pUp	= (CButton *)GetDlgItem (CHK_EXPROUNDUP);
	CButton * pDown = (CButton *)GetDlgItem (CHK_EXPROUNDDOWN);

	ASSERT (pUp);
	ASSERT (pDown);

	if (pUp->GetCheck () == 1)
		pDown->SetCheck (0);
}

void CPropertiesDlg::OnExprounddown() 
{
	CButton * pUp	= (CButton *)GetDlgItem (CHK_EXPROUNDUP);
	CButton * pDown = (CButton *)GetDlgItem (CHK_EXPROUNDDOWN);

	ASSERT (pUp);
	ASSERT (pDown);

	if (pDown->GetCheck () == 1)
		pUp->SetCheck (0);
}
