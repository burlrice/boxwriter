#if !defined(AFX_SYSTEMDATAMATRIXDLG_H__59BAB859_36C4_4EEF_AC95_AE88F36A4A5C__INCLUDED_)
#define AFX_SYSTEMDATAMATRIXDLG_H__59BAB859_36C4_4EEF_AC95_AE88F36A4A5C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SystemDataMatrixDlg.h : header file
//

#include "Resource.h"
#include "ListCtrlImp.h"
#include "Utils.h"

FORWARD_DECLARE (fjsysdatamatrix, FJSYSDATAMATRIX)
FORWARD_DECLARE (fjsystem, FJSYSTEM)

/////////////////////////////////////////////////////////////////////////////
// CSystemDataMatrixDlg dialog

namespace FoxjetIpElements
{
	class ELEMENT_API CSystemDataMatrixDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CSystemDataMatrixDlg(CWnd* pParent = NULL);   // standard constructor
		virtual ~CSystemDataMatrixDlg ();

	// Dialog Data
		//{{AFX_DATA(CSystemDataMatrixDlg)
		enum { IDD = IDD_SYSTEMDATAMATRIX };
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

			FoxjetDatabase::HEADTYPE	m_nHeadType;
			ULONG						m_lLineID;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CSystemDataMatrixDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		class CBarcodeItem : public ItiLibrary::ListCtrlImp::CItem
		{
		public:
			CBarcodeItem (LPFJSYSDATAMATRIX pBC, int nIndex);
			CBarcodeItem (int nIndex, LONG lHeight, LONG lHeightBold, LONG lWidth, LONG lWidthBold);
			virtual ~CBarcodeItem ();

			virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
			virtual CString GetDispText (int nColumn) const;

			CString m_strName;
			LONG    m_lHeight;
			LONG    m_lHeightBold;
			LONG    m_lWidth;
			LONG    m_lWidthBold;
			int		m_nIndex;
		};

		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
		bool m_bUpdated;

		LPFJSYSTEM GetCurSystem () const;
		int GetCurSelLineIndex () const;
		FoxjetDatabase::HEADTYPE GetCurSelHeadType() const;
		void AssignDefs (CBarcodeItem * pvDefs, int nParams, FoxjetDatabase::HEADTYPE type, bool bSetName = true);

		// Generated message map functions
		//{{AFX_MSG(CSystemDataMatrixDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnSelchangeSystem();
		afx_msg void OnSelchangeheadtype();
		afx_msg void OnDblclkBarcode(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnLoad();
		afx_msg void OnEdit();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SYSTEMDATAMATRIXDLG_H__59BAB859_36C4_4EEF_AC95_AE88F36A4A5C__INCLUDED_)
