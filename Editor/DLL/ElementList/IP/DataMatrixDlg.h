#if !defined(AFX_DATAMATRIXDLG_H__8FAA79AC_47B8_4DAF_BD89_7FDD076EA2A1__INCLUDED_)
#define AFX_DATAMATRIXDLG_H__8FAA79AC_47B8_4DAF_BD89_7FDD076EA2A1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataMatrixDlg.h : header file
//

#include "ElementDlg.h"
#include "DataMatrixElement.h"

/////////////////////////////////////////////////////////////////////////////
// CDataMatrixDlg dialog

class CDataMatrixDlg : public CElementDlg
{
// Construction
public:
	CDataMatrixDlg (const FoxjetCommon::CBaseElement & e, const CSize & pa,
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);

// Dialog Data
	//{{AFX_DATA(CDataMatrixDlg)
	enum { IDD = IDD_DATAMATRIX };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	CString m_strData;
	int m_cx;
	int m_cy;
	int m_nType;
	long m_lDynamicID;
	long m_lFirstChar;
	long m_lNumChar;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataMatrixDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();
	FoxjetIpElements::CDataMatrixElement & GetElement ();
	const FoxjetIpElements::CDataMatrixElement & GetElement () const;

	virtual int BuildElement ();

	// Generated message map functions
	//{{AFX_MSG(CDataMatrixDlg)
	afx_msg void OnSelect();
	afx_msg void OnSelchangeDynamicid();
	afx_msg void OnSelchangeType();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAMATRIXDLG_H__8FAA79AC_47B8_4DAF_BD89_7FDD076EA2A1__INCLUDED_)
