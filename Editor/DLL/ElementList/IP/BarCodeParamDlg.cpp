// BarCodeParamDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "BarCodeParamDlg.h"
#include "barcode\barcode.h"
#include "BarcodeElement.h"
#include "TemplExt.h"

using namespace FoxjetIpElements;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CBarcodeParamDlg dialog


CBarcodeParamDlg::CBarcodeParamDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(CBarcodeParamDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBarcodeParamDlg)
	m_nHeight = 10;
	m_lHorzBearer = 1;
	m_strName = _T("");
	m_lVertBearer = 1;
	m_lQuietZone = 0;
	//}}AFX_DATA_INIT

	m_lBCFlags		= BARCODE_I25;
	m_lBarVal [0]	= 1;
	m_lBarVal [1]	= 1;
	m_lBarVal [2]	= 1;
	m_lBarVal [3]	= 1;
}


void CBarcodeParamDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CBarcodeParamDlg)
	DDX_Text(pDX, TXT_HEIGHT, m_nHeight);
	DDX_Text(pDX, TXT_HORIZONTAL, m_lHorzBearer);
	DDX_Text(pDX, TXT_NAME, m_strName);
	DDX_Text(pDX, TXT_VERTICAL, m_lVertBearer);
	DDX_Text(pDX, TXT_QUIETZONE, m_lQuietZone);
	DDX_Check (pDX, CHK_CHECKSUM, m_bChecksum);
	//}}AFX_DATA_MAP

	DDX_Text (pDX, TXT_BAR0,		m_lBarVal [0]);
	DDX_Text (pDX, TXT_BAR1,		m_lBarVal [1]);
	DDX_Text (pDX, TXT_BAR2,		m_lBarVal [2]);
	DDX_Text (pDX, TXT_BAR3,		m_lBarVal [3]);
	DDX_Text (pDX, TXT_BAR4,		m_lBarVal [4]);
}


BEGIN_MESSAGE_MAP(CBarcodeParamDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CBarcodeParamDlg)
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE (CB_TYPE, OnSelchangeType)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBarcodeParamDlg message handlers

void CBarcodeParamDlg::OnSelchangeType ()
{
	UINT nID [] = 
	{
		TXT_QUIETZONE,
		CHK_CHECKSUM,
		TXT_HORIZONTAL,
		TXT_VERTICAL,
	};
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);

	ASSERT (pType);

	LONG lType = pType->GetItemData (pType->GetCurSel ());
	bool bUPCEAN = lType == BARCODE_EAN || lType == BARCODE_UPC;

	for (int i = 0; i < ARRAYSIZE (nID); i++) {
		CWnd * p = GetDlgItem (nID [i]);

		ASSERT (p);

		p->EnableWindow (!bUPCEAN);
	}

	if (bUPCEAN) {
		CButton * pCheck = (CButton *)GetDlgItem (CHK_CHECKSUM);

		ASSERT (pCheck);

		pCheck->SetCheck (0);

		SetDlgItemText (TXT_QUIETZONE, _T ("0"));
		SetDlgItemText (TXT_HORIZONTAL, _T ("0"));
		SetDlgItemText (TXT_VERTICAL, _T ("0"));
	}
	else
		UpdateData (FALSE);
}

BOOL CBarcodeParamDlg::OnInitDialog ()
{
	const LONG lType [] = 
	{
		BARCODE_EAN,	
		BARCODE_UPC,	
		BARCODE_I25,	
		BARCODE_39,		
		BARCODE_128,
		BARCODE_EAN128, // __SW0842__
		
//		BARCODE_ISBN,	
//		BARCODE_128C,	
//		BARCODE_128B,	
//		BARCODE_128RAW,	
//		BARCODE_CBR,	
//		BARCODE_MSI,	
//		BARCODE_PLS,	
	};
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);

	ASSERT (pType);

	FoxjetCommon::CEliteDlg::OnInitDialog ();

	for (int i = 0; i < (sizeof (lType) / sizeof (lType [0])); i++) {
		CString str = CBarcodeElement::GetBcType (lType [i]);

		int nIndex = pType->AddString (str);
		pType->SetItemData (nIndex, lType [i]);

		if (m_lBCFlags == lType [i])
			pType->SetCurSel (nIndex);
	}


/* TODO: rem
	if (CWnd * p = GetDlgItem (TXT_NAME))
		p->EnableWindow (TRUE);
*/

	if (pType->GetCurSel () == CB_ERR)
		pType->SetCurSel (0);

	OnSelchangeType ();

	return TRUE;
}

void CBarcodeParamDlg::OnOK()
{
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);

	ASSERT (pType);

	int nIndex = pType->GetCurSel ();

	ASSERT (nIndex != CB_ERR);

	m_lBCFlags = pType->GetItemData (nIndex);

	FoxjetCommon::CEliteDlg::OnOK ();
}
