// BitmapElement.h: interface for the CBitmapElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BITMAPELEMENT_H__C2E2617F_4DEA_4F47_AF3A_79E8D110D6BA__INCLUDED_)
#define AFX_BITMAPELEMENT_H__C2E2617F_4DEA_4F47_AF3A_79E8D110D6BA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IpBaseElement.h"
#include "fj_element.h"

FORWARD_DECLARE (fjbitmap, FJBITMAP)
FORWARD_DECLARE (fjmemstor, FJMEMSTOR)

namespace FoxjetIpElements
{
	class ELEMENT_API CBitmapElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CBitmapElement);
	public:
		CBitmapElement(LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CBitmapElement (const CBitmapElement & rhs);
		CBitmapElement & operator = (const CBitmapElement & rhs);
		virtual ~CBitmapElement();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		virtual void CreateImage ();
		virtual CString GetElementFontName () const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		void Free ();
		bool SetFilePath (const CString &strData, const FoxjetDatabase::HEADSTRUCT & head);
		CString GetFilePath () const;

		DECLARE_MEMBERPOINTERACCESS (LPFJBITMAP, GetElement ()->pDesc)

		LONG GetBold () const;
		bool SetBold (LONG lValue);

		virtual int GetWidth () const;
		virtual bool SetWidth (int nValue);
		
		static PBYTE LoadBitmap (const CString & strFile);
		static bool FileExists (const CString & strFile);

		static CString GetDefPath ();
		static CString GetDefFilename ();

		static CString AddSuffix (const CString & strFile);
		static CString RemoveSuffix (const CString & strFile);

		static const CString m_strSuffix;

	protected:
		virtual void Invalidate ();
		void LoadDefResBitmap(const FoxjetDatabase::HEADSTRUCT & head);

	public: 
		LPFJMEMSTOR m_pMemStore;
	};
}; //namespace FoxjetIpElements

#endif // !defined(AFX_BITMAPELEMENT_H__C2E2617F_4DEA_4F47_AF3A_79E8D110D6BA__INCLUDED_)
