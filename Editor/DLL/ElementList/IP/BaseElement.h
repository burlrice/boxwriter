// BaseElement.h: interface for the CBaseElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BASEELEMENT_H__D7368E0A_8A3A_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_BASEELEMENT_H__D7368E0A_8A3A_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>

#include "ElementApi.h"
#include "Version.h"
#include "PrinterFont.h"
#include "CopyArray.h"
#include "Database.h"
#include "Utils.h"

namespace FoxjetCommon {
	class ELEMENT_API CElementList;

	typedef enum 
	{
		EXCEPTION_GENERIC = 1,
		EXCEPTION_DATABASE,
		EXCEPTION_MEMORY,
	} EXCEPTIONTYPE;

	class ELEMENT_API CElementException : public CException
	{
	public:
		CElementException (EXCEPTIONTYPE type, const CString & str = _T (""));
		CElementException (CException * e);
		
		virtual int ReportError (UINT nType = MB_OK, UINT nMessageID = 0);

		int GetType () const { return m_type; }
		virtual BOOL GetErrorMessage( LPTSTR lpszError, UINT nMaxError, PUINT pnHelpContext = NULL );
		CString GetErrorMessage ();
		
		static EXCEPTIONTYPE GetType (const CException * e);
		static CString GetDescription (EXCEPTIONTYPE type);

	protected:
		CString m_str;
		EXCEPTIONTYPE m_type;
	};


	typedef enum { EDITOR = 0, PRINTER } DRAWCONTEXT;
	typedef enum { INITIAL = 0, IMAGING } BUILDTYPE;
	typedef enum { ALIGNMENT_LEFT = 0, ALIGNMENT_CENTER, ALIGNMENT_RIGHT } ALIGNMENT;
	typedef enum { ALIGNMENT_ADJUST_NONE = 0, ALIGNMENT_ADJUST_SUBTRACT, ALIGNMENT_ADJUST_ADD } ALIGNMENT_ADJUST;

	class ELEMENT_API CBaseElement : public CObject
	{
		DECLARE_DYNAMIC (CBaseElement);

	public:

		CBaseElement (const FoxjetDatabase::HEADSTRUCT & head);
		CBaseElement (const CBaseElement & rhs);
		CBaseElement & operator = (const CBaseElement & rhs);
		virtual ~CBaseElement ();

		virtual CString ToString (const CVersion & ver) const = 0;
		virtual bool FromString (const CString & str, const CVersion & ver) = 0;
		virtual CSize Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly = false, FoxjetCommon::DRAWCONTEXT context = EDITOR) = 0;
		virtual int GetClassID () const = 0;
		virtual CString GetDefaultData () const = 0;
		virtual CString GetImageData () const = 0;

		virtual int Build (BUILDTYPE type = INITIAL, bool bCanThrowException = false) = 0;
		// throws CElementException

		virtual bool SetPos (const CPoint & ptSet, const FoxjetDatabase::HEADSTRUCT * pHead, ALIGNMENT_ADJUST adj = ALIGNMENT_ADJUST_NONE);
		CPoint GetPosInternal (ALIGNMENT_ADJUST adj = ALIGNMENT_ADJUST_NONE) const;
		CPoint GetPosInternal (CPoint pt, ALIGNMENT_ADJUST adj = ALIGNMENT_ADJUST_NONE) const;
		CPoint GetPosInternal (CPoint pt, const CSize & size, ALIGNMENT_ADJUST adj = ALIGNMENT_ADJUST_NONE) const;
		CPoint AdjustPos (const FoxjetDatabase::HEADSTRUCT & head, ALIGNMENT_ADJUST adj = ALIGNMENT_ADJUST_NONE);
		virtual CPoint GetPos (const FoxjetDatabase::HEADSTRUCT * pHead) const;
		virtual ALIGNMENT GetAlignment (int) const { return m_alignment; }
		virtual bool SetAlignment (ALIGNMENT align, int) { m_alignment = align;  return true; }
		virtual bool Move (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT & head);
		virtual bool SetXPos (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT * pHead);
		virtual bool SetYPos (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT * pHead);

		virtual bool SetSize (const CSize & size, const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI = true) { return false; }
		virtual CSize GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI = true) const = 0;
		virtual void ClipTo (const FoxjetDatabase::HEADSTRUCT & head);

		virtual bool IsResizable () const { return false; }
		virtual bool IsMovable () const { return true; }
		virtual bool IsLockAspectRatio () const { return false; }
		virtual bool IsFlippedH () const;
		virtual bool IsFlippedV () const;
		virtual bool IsInverse () const;
		virtual bool IsPrintable () const { return true; }
		
		virtual bool SetResizable (bool bResizable) { return false; }
		virtual bool SetMovable (bool bMovable) { return false; }
		virtual bool SetFlippedH (bool bFlip);
		virtual bool SetFlippedV (bool bFlip);
		virtual bool SetInverse (bool bInverse);

		virtual bool HasFont () const { return false; }

		virtual UINT GetID () const;
		virtual bool SetID (UINT nID);

		virtual CString GetFieldName (int nIndex) const;
		virtual CString GetFieldDef (int nIndex) const; 
		virtual int GetFieldIndex (const CString & str) const;
		virtual int GetFieldCount () const;
		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		CRect GetWindowRect (const FoxjetDatabase::HEADSTRUCT & head) const;
		bool SetWindowRect (const CRect & rc, const FoxjetDatabase::HEADSTRUCT & head);
		bool SetWindowRect (const CSize & size, const FoxjetDatabase::HEADSTRUCT & head);

		virtual bool SetRedraw (bool bRedraw = true);
		bool GetRedraw ();

		virtual int GetWidth () const { return -1; }
		virtual bool SetWidth (int nWidth) { return false; }

		ULONG GetLineID () const { return m_lLineID; }
		FoxjetDatabase::HEADTYPE GetHeadType () const { return m_head.m_nHeadType; }
		virtual bool SetHead (const FoxjetDatabase::HEADSTRUCT & head);
		const FoxjetDatabase::HEADSTRUCT & GetHead () const;

		virtual void OnCreateNew (const FoxjetDatabase::HEADSTRUCT & head, const CString & strTask) { }
		virtual void OnDestroyNew () { }

		bool GetSnap () const { return m_bSnap; }
		void SetSnap (bool bSnap) { m_bSnap = bSnap; }

		void Parse (const CString & str, const CVersion & ver, CStringArray & v);

		static void CalcStretch (const FoxjetDatabase::HEADSTRUCT & head, double dStretch [2]);
		static CPoint LogicalToThousandths (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT & head);
		static CPoint ThousandthsToLogical (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT & head);
		static int StretchByAngle (int cy, double dAngle);

		static float CalcPrintRes (const FoxjetDatabase::HEADSTRUCT & head);
		static CSize GetValveDotSize ();
		static CSize GetValveDotSize (const FoxjetDatabase::HEADSTRUCT & head);

	protected:

		CPoint						m_pt;
		bool						m_bFlippedH;
		bool						m_bFlippedV;
		bool						m_bInverse;
		UINT						m_nID;
		bool						m_bRedraw;
		FoxjetDatabase::HEADSTRUCT	m_head;
		ULONG						m_lLineID;
		bool						m_bSnap;
		ALIGNMENT					m_alignment;
		CRect						m_rcLast;


	private:
		static UINT nNextID;
	};

	class ELEMENT_API CBaseTextElement : public CBaseElement
	{
		DECLARE_DYNAMIC (CBaseTextElement);

	public:
		CBaseTextElement(const FoxjetDatabase::HEADSTRUCT & head);
		CBaseTextElement (const CBaseTextElement & rhs);
		CBaseTextElement & operator = (const CBaseTextElement & rhs);
		virtual ~CBaseTextElement();

		virtual bool SetFont (const CPrinterFont & f) = 0;
		virtual bool GetFont (CPrinterFont & f) const = 0;

		virtual bool SetFontName (const CString & strName);
		virtual CString GetFontName () const;

		virtual bool SetFontSize (UINT nSize);
		virtual UINT GetFontSize () const;

		virtual bool SetFontBold (bool bBold);
		virtual bool GetFontBold () const;

		virtual bool SetFontItalic (bool bItalic);
		virtual bool GetFontItalic () const;

		virtual bool HasFont () const { return true; }
	};
}; // FoxjetCommon

#endif // !defined(AFX_BASEELEMENT_H__D7368E0A_8A3A_11D4_8FC6_006067662794__INCLUDED_)
