#if !defined(AFX_DYNAMICTEXTDLG_H__067CB917_FEA9_437B_8B39_2D169F3C35A1__INCLUDED_)
#define AFX_DYNAMICTEXTDLG_H__067CB917_FEA9_437B_8B39_2D169F3C35A1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DynamicTextDlg.h : header file
//

#include "resource.h"
#include "DynamicTextElement.h"
#include "ElementDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CDynamicTextDlg dialog

class CDynamicTextDlg : public CElementDlg
{
// Construction
public:
	virtual void OnOK();
	CDynamicTextDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa,
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDynamicTextDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	LONG	m_lDynamicID;
	LONG	m_lBold;
	LONG	m_lWidth;
	LONG	m_lGap;
	CString m_strFont;
	LONG	m_lLength;
	LONG	m_lFirstChar;
	LONG	m_lNumChar;
	CHAR	m_cAlign;
	CString m_strFill;


	virtual UINT GetDefCtrlID () const { return CB_DYNAMICID; }


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDynamicTextDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	LONG GetCurSelDynamicID () const;
	FoxjetIpElements::CDynamicTextElement & GetElement ();
	const FoxjetIpElements::CDynamicTextElement & GetElement () const;

	virtual int BuildElement ();

	// Generated message map functions
	//{{AFX_MSG(CDynamicTextDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	
	afx_msg void OnSelect();

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DYNAMICTEXTDLG_H__067CB917_FEA9_437B_8B39_2D169F3C35A1__INCLUDED_)
