// TextElement.cpp: implementation of the CTextElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TextElement.h"
#include "List.h"
#include "Debug.h"
#include "Color.h"
#include "Extern.h"
#include "TemplExt.h"

#include "fj_printhead.h"
#include "fj_element.h"
#include "fj_text.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_font.h"
#include "fj_export.h"

using namespace FoxjetCommon;
using namespace FoxjetIpElements;
using namespace Color;
using namespace FoxjetCommon::ElementFields;


IMPLEMENT_DYNAMIC (CTextElement, CIpBaseElement);

const FoxjetCommon::LIMITSTRUCT CTextElement::m_lmtFontSize	= { 5,	256					};
const FoxjetCommon::LIMITSTRUCT CTextElement::m_lmtString	= { 1,	FJTEXT_SIZE			};

CTextElement::CTextElement(LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head)
:	CIpBaseElement (fj_ElementTextNew (), pHead, head)
{
 	SetDefaultData (_T ("Text element"));
}

CTextElement::CTextElement (const CTextElement & rhs)
: 	CIpBaseElement (rhs)
{
 	SetDefaultData (rhs.m_strDefaultData);
}

CTextElement & CTextElement::operator = (const CTextElement & rhs)
{
	CIpBaseElement::operator = (rhs);

	if (this != &rhs) 
	 	SetDefaultData (rhs.m_strDefaultData);

	return * this;
}

CTextElement::~CTextElement()
{
}

CString CTextElement::GetElementFontName () const
{
	LPFJTEXT p = GetMember ();
	return p->strFontName;
}

void CTextElement::CreateImage () 
{
	LPFJTEXT pText = GetMember ();
	CString str = GetImageData ();

	ASSERT (pText->strText);
	ASSERT (AfxIsValidAddress (pText->strText, FJTEXT_SIZE));

	str.Replace (_T ("\\n"), _T ("\n"));
	strncpy (pText->strText, w2a (str), FJTEXT_SIZE);

	for (int i = 0; i < (int)strlen (pText->strText); i++) {
		LONG lChar = pText->strText [i] & 0xff;

		if (lChar != '\n' && lChar != '\r') {
			// if the font doesn't contain a char, set it to the closest one
			pText->strText [i] = (char)BindTo (lChar, m_pFont->lFirstChar, m_pFont->lLastChar);
		}
	}

	pText->pff = m_pFont;
	strcpy (pText->strFontName, m_pFont->strName);    

	CIpBaseElement::CreateImage ();
}

int CTextElement::Build(BUILDTYPE type, bool bCanThrowException)
{
	m_strImageData = m_strDefaultData;
	return GetClassID ();
}

CString CTextElement::GetImageData () const
{
	return m_strImageData;
}

CString CTextElement::GetDefaultData() const
{
	return m_strDefaultData;
}

bool CTextElement::SetDefaultData(const CString &strData)
{
	strncpy (GetMember ()->strText, w2a (strData), FJTEXT_SIZE);
	m_strDefaultData = strData;
	Invalidate ();
	return true;
}

LONG CTextElement::GetBold () const
{
	return GetMember ()->lBoldValue;
}

bool CTextElement::SetBold (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, CIpBaseElement::m_lmtBold)) {
		GetMember ()->lBoldValue = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

int CTextElement::GetWidth () const
{
	return GetMember ()->lWidthValue;
}

bool CTextElement::SetWidth (int nValue)
{
	if (FoxjetCommon::IsValid (nValue, CIpBaseElement::m_lmtWidth)) {
		GetMember ()->lWidthValue = nValue;
		Invalidate ();
		return true;
	}

	return false;
}

LONG CTextElement::GetGap () const
{
	return GetMember ()->lGapValue;
}

bool CTextElement::SetGap (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, CIpBaseElement::m_lmtGap)) {
		GetMember ()->lGapValue = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

bool CTextElement::SetElement (LPFJELEMENT pNew)
{
	if (pNew) {
		ASSERT (pNew);
		ASSERT (pNew->pActions->fj_DescGetType () == FJ_TYPE_TEXT);
		ASSERT (pNew->pDesc);

		LPFJTEXT p = (LPFJTEXT)pNew->pDesc;
		SetDefaultData (p->strText);
	}

	return CIpBaseElement::SetElement (pNew);
}

bool CTextElement::SetFont (const FoxjetCommon::CPrinterFont & f)
{
	strncpy (GetMember ()->strFontName, w2a (f.m_strName), FJFONT_NAME_SIZE);
	return CIpBaseElement::SetFont (f);
}


static const ElementFields::FIELDSTRUCT lpszFields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType, 	_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszID,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszX,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszY,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszPosition,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszBold,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszIPWidth,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszIPGap,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszFlipV,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFlipH,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszInverse,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFontName,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszData,			_T ("0")),	
	ElementFields::FIELDSTRUCT (),
};

const FoxjetCommon::ElementFields::FIELDSTRUCT * CTextElement::GetFieldBuffer () const
{
	return ::lpszFields;
}

CHAR CTextElement::GetAlignment () const
{
	return GetMember ()->cAlign;
}

bool CTextElement::SetAlignment (CHAR cAlign)
{
	GetMember ()->cAlign = cAlign;
	SetRedraw ();
	return true;
}
