// DataMatrixElement.h: interface for the CDataMatrixElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATAMATRIXELEMENT_H__5E40C719_4D48_47C2_ABBC_9FD860C4A6F9__INCLUDED_)
#define AFX_DATAMATRIXELEMENT_H__5E40C719_4D48_47C2_ABBC_9FD860C4A6F9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IpBaseElement.h"
#include "Types.h"
#include "fj_element.h"
#include "fj_datamatrix.h"

FORWARD_DECLARE (fjdatamatrix, FJDATAMATRIX)

namespace FoxjetIpElements
{
	class ELEMENT_API CDataMatrixElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CDataMatrixElement);

	public:
		CDataMatrixElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CDataMatrixElement (const CDataMatrixElement & rhs);
		CDataMatrixElement & operator = (const CDataMatrixElement & rhs);
		virtual ~CDataMatrixElement ();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual bool SetDefaultData(const CString &strData);
		virtual void CreateImage ();
		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		virtual CString GetElementFontName () const { return _T (""); }
		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;
		
		LONG GetCX () const;
		bool SetCX (LONG l);

		LONG GetCY () const;
		bool SetCY (LONG l);

		LONG GetBcType () const;
		bool SetBcType (LONG lValue);

		int GetDynamicID () const;
		bool SetDynamicID (LONG lID);

		LONG GetFirstChar () const;
		bool SetFirstChar (LONG l);

		LONG GetNumChar () const;
		bool SetNumChar (LONG l);

		static const FoxjetCommon::LIMITSTRUCT m_lmtCX;
		static const FoxjetCommon::LIMITSTRUCT m_lmtCY;
		static const FoxjetCommon::LIMITSTRUCT m_lmtHeightFactor;
		static const FoxjetCommon::LIMITSTRUCT m_lmtWidthFactor;

		static CString GetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
			ULONG lLineID, FoxjetDatabase::HEADTYPE type);
		static bool SetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
			ULONG lLineID, FoxjetDatabase::HEADTYPE type, const CString & strSettings);
		static bool IsDataMatrixEnabled ();
		static void EnableDataMatrix (bool bEnable);

		DECLARE_MEMBERPOINTERACCESS (LPFJDATAMATRIX, GetElement ()->pDesc)
	};
}; //FoxjetIpElements

#endif // !defined(AFX_DATAMATRIXELEMENT_H__5E40C719_4D48_47C2_ABBC_9FD860C4A6F9__INCLUDED_)
