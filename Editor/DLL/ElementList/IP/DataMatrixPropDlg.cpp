// DataMatrixPropDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "list.h"
#include "DataMatrixPropDlg.h"
#include "DataMatrixElement.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetIpElements;

/////////////////////////////////////////////////////////////////////////////
// CDataMatrixPropDlg dialog


CDataMatrixPropDlg::CDataMatrixPropDlg(CWnd* pParent /*=NULL*/)
:	FoxjetCommon::CEliteDlg(CDataMatrixPropDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDataMatrixPropDlg)
	m_strName = _T("");
	m_lHeight = 0;
	m_lHeightBold = 0;
	m_lWidth = 0;
	m_lWidthBold = 0;
	//}}AFX_DATA_INIT
}


void CDataMatrixPropDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDataMatrixPropDlg)
	//}}AFX_DATA_MAP
	DDX_Text(pDX, TXT_NAME, m_strName);

	DDX_Text(pDX, TXT_HEIGHT, m_lHeight);
	DDV_MinMaxLong(pDX, m_lHeight, CDataMatrixElement::m_lmtHeightFactor.m_dwMin + 1, CDataMatrixElement::m_lmtHeightFactor.m_dwMax);
	
	DDX_Text(pDX, TXT_HEIGHTBOLD, m_lHeightBold);
	DDV_MinMaxLong(pDX, m_lHeightBold, CDataMatrixElement::m_lmtHeightFactor.m_dwMin, CDataMatrixElement::m_lmtHeightFactor.m_dwMax);
	
	DDX_Text(pDX, TXT_WIDTH, m_lWidth);
	DDV_MinMaxLong(pDX, m_lWidth, CDataMatrixElement::m_lmtWidthFactor.m_dwMin + 1, CDataMatrixElement::m_lmtWidthFactor.m_dwMax);
	
	DDX_Text(pDX, TXT_WIDTHBOLD, m_lWidthBold);
	DDV_MinMaxLong(pDX, m_lWidthBold, CDataMatrixElement::m_lmtWidthFactor.m_dwMin + 1, CDataMatrixElement::m_lmtWidthFactor.m_dwMax);
}


BEGIN_MESSAGE_MAP(CDataMatrixPropDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDataMatrixPropDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataMatrixPropDlg message handlers

BOOL CDataMatrixPropDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
