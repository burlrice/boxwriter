// DynamicTableDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "DynamicTableDlg.h"
#include "Parse.h"
#include "Utils.h"
#include "TemplExt.h"
#include "DynDataPropDlg.h"
#include "DynamicTextElement.h"
#include "DynamicBarcodeElement.h"
#include "Resource.h"
#include "Extern.h"
#include "Database.h"
#include "Debug.h"
#include "OdbcDatabase.h"
#include "OdbcRecordset.h"
#include "Parse.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetIpElements;
using namespace ListGlobals;
using namespace ItiLibrary;

using FoxjetIpElements::CTableItem;

static const LPCTSTR lpszKey = _T ("DynTbl");
static const LPCTSTR lpszDynData = _T ("Dynamic data");

const CString CDynamicTableDlg::m_strSkip = _T (",");

CTableItem::CTableItem (LONG lID, bool bPrompt, const CString & strPrompt, const CString & strData)
:	m_lID (lID),
	m_bPrompt (bPrompt),
	m_strPrompt (strPrompt),
	m_bUseDatabase (false),
	m_strData (strData)
{
}

CTableItem::CTableItem (const CTableItem & rhs)
:	m_lID (rhs.m_lID),
	m_bPrompt (rhs.m_bPrompt),
	m_strPrompt (rhs.m_strPrompt),
	m_strData (rhs.m_strData),
	m_bUseDatabase (rhs.m_bUseDatabase),
	m_params (rhs.m_params)
{
}

CTableItem & CTableItem::operator = (const CTableItem & rhs)
{
	if (&rhs != this) {
		m_lID			= rhs.m_lID;
		m_bPrompt		= rhs.m_bPrompt;
		m_strPrompt		= rhs.m_strPrompt;
		m_strData		= rhs.m_strData;
		m_bUseDatabase	= rhs.m_bUseDatabase;
		m_params		= rhs.m_params;
	}

	return * this;
}

CTableItem::~CTableItem ()
{
}

CString CTableItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0:		str.Format (_T ("%d"), m_lID);			break;
	case 1:		str = GetPrompt ();						break;
	case 2:		str = GetDefData ();					break;
	}

	return str;
}

CString CTableItem::ToString () const
{
	CStringArray v;

	v.Add (::lpszKey);											// 0
	v.Add (m_strPrompt);										// 1
	v.Add (m_strData);											// 2
	v.Add (FoxjetDatabase::ToString (m_lID));					// 3
	v.Add (FoxjetDatabase::ToString ((int)m_bPrompt));			// 4
	v.Add (FoxjetDatabase::ToString ((int)m_bUseDatabase));		// 5
	v.Add (m_params.m_strDSN);									// 6
	v.Add (m_params.m_strTable);								// 7
	v.Add (m_params.m_strField);								// 8
	v.Add (m_params.m_strKeyField);								// 9
	v.Add (m_params.m_strKeyValue);								// 10
	v.Add (FoxjetDatabase::ToString (m_params.m_nRow));			// 11
	v.Add (FoxjetDatabase::ToString ((int)m_params.m_bSQL));	// 12
	v.Add (m_params.m_strSQL);									// 13
	v.Add (FoxjetDatabase::ToString (m_params.m_nSQLType));		// 14

	return FoxjetDatabase::ToString (v);
}

bool CTableItem::FromString (const CString & str)
{
	CStringArray v;

	FoxjetDatabase::FromString (str, v);

	if (v.GetSize () >= 5) {
		if (!v [0].CompareNoCase (::lpszKey)) {
			int nIndex = 1;

			m_strPrompt					=			v [nIndex++];
			m_strData					=			v [nIndex++];
			m_lID						= _tcstoul (v [nIndex++], NULL, 10);
			m_bPrompt					= _tcstoul (v [nIndex++], NULL, 10) ? true : false;

			if (v.GetSize () >= 14) {
				m_bUseDatabase			= _tcstoul (v [nIndex++], NULL, 10) ? true : false;
				m_params.m_strDSN		=			v [nIndex++];
				m_params.m_strTable		=			v [nIndex++];
				m_params.m_strField		=			v [nIndex++];
				m_params.m_strKeyField	=			v [nIndex++];
				m_params.m_strKeyValue	=			v [nIndex++];
				m_params.m_nRow			= _tcstoul (v [nIndex++], NULL, 10);
				m_params.m_bSQL			= _tcstoul (v [nIndex++], NULL, 10) ? true : false;
				m_params.m_strSQL		=			v [nIndex++];
				m_params.m_nSQLType = _ttoi (GetParam (v, nIndex++, _T ("0")));
			}

			return true;
		}
	}

	return false; 
}

bool CTableItem::UsesKeyField () const
{
	return m_bUseDatabase && m_params.m_strKeyField.GetLength ();
}

CString CTableItem::GetPrompt () const
{
/*
	if (UsesKeyField ()) {
		CString str;

		str.Format ("[%s].[%s].[%s]",
			m_params.m_strDSN,
			m_params.m_strTable,
			m_params.m_strKeyField);

		return str;
	}
	else
*/
		return m_strPrompt;				
}

CString CTableItem::GetDefData () const
{
//	return UsesKeyField () ? m_params.m_strKeyValue : m_strData;
	return m_strData;
}

CString CTableItem::CreateSQL () const
{
	CString strSQL = m_params.m_strSQL;
	
	if (!m_params.m_bSQL) {
		strSQL = _T ("SELECT * FROM [") + m_params.m_strTable + _T ("] ");

		if (m_params.m_strKeyValue.GetLength () || m_params.m_strKeyField.GetLength ()) {
			CString strDelim;

			switch (m_params.m_nSQLType) {
				case SQL_DATE:
				case SQL_TIME:
				case SQL_TIMESTAMP:
					strDelim = _T ("#"); 
					break;
				default:
					if (IsSqlString (m_params.m_nSQLType))
						strDelim = _T ("'"); 
				//case SQL_CHAR:
				//case SQL_VARCHAR:
				//case SQL_LONGVARCHAR:
				//	strDelim = _T ("'"); 
					break;
			}

			strSQL += _T (" WHERE [") + m_params.m_strKeyField + _T ("]=") + strDelim + m_params.m_strKeyValue + strDelim;
		}
	}

	return strSQL;
}

CString CTableItem::GetSqlResult () const
{
	const CString strSQL = CreateSQL ();	
	CString strResult;
	
	try {
		COdbcDatabase db (_T (__FILE__), __LINE__);

		if (db.Open (m_params.m_strDSN, FALSE, TRUE)) {
			COdbcRecordset rst (db);
						
			if (rst.Open (strSQL, CRecordset::snapshot, CRecordset::readOnly | CRecordset::noDirtyFieldCheck)) {
				if (m_params.m_nRow) {
					rst.Move (m_params.m_nRow, SQL_FETCH_ABSOLUTE);
					rst.SetAbsolutePosition (m_params.m_nRow);
				}

				if (!m_params.m_bSQL)
					strResult = (CString)rst.GetFieldValue (m_params.m_strField);
				else
					strResult = (CString)rst.GetFieldValue ((short)0);

				rst.Close ();
			}

			db.Close ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); /* throw e; */ }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); /* throw e; */ }

	return strResult;
}

/////////////////////////////////////////////////////////////////////////////
// CDynamicTableDlg dialog


CDynamicTableDlg::CDynamicTableDlg(CWnd* pParent /*=NULL*/)
:	m_lSelectedID (-1),
	m_lLineID (-1),
	FoxjetCommon::CEliteDlg(IDD_DYNTABLE, pParent)
{
	//{{AFX_DATA_INIT(CDynamicTableDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDynamicTableDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDynamicTableDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDynamicTableDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDynamicTableDlg)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_NOTIFY(NM_DBLCLK, LV_DATA, OnDblclkData)
	ON_NOTIFY(LVN_ITEMCHANGED, LV_DATA, OnItemchangedData)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(BTN_APPLY, OnApply)
	ON_CBN_SELCHANGE(CB_LINE, OnSelchangeLine)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDynamicTableDlg message handlers

BOOL CDynamicTableDlg::OnInitDialog() 
{
	using ListCtrlImp::CColumn;

	CString strSection = defElements.m_strRegSection + _T ("\\CDynamicTableDlg");
	CArray <CColumn, CColumn> vCols;
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CArray <LINESTRUCT, LINESTRUCT &> vLines;

	ASSERT (pLine);

	GetLineRecords (GetDB (), vLines);

	FoxjetCommon::CEliteDlg::OnInitDialog();
	
	vCols.Add (CColumn (LoadString (IDS_ID), 40));
	vCols.Add (CColumn (LoadString (IDS_PROMPT)));
	vCols.Add (CColumn (LoadString (IDS_DEFAULTDATA)));

	m_lv.Create (LV_DATA, vCols, this, defElements.m_strRegSection);
	m_lv->SetExtendedStyle (LVS_EX_CHECKBOXES | m_lv->GetExtendedStyle ());

	for (int i = 0; i < vLines.GetSize (); i++) {
		LINESTRUCT line = vLines [i];
		int nIndex = pLine->AddString (line.m_strName);

		pLine->SetItemData (nIndex, line.m_lID);

		if (m_lLineID == line.m_lID)
			pLine->SetCurSel (nIndex);
	}

	if (pLine->GetCurSel () == CB_ERR)
		pLine->SetCurSel (0);

	if (m_lSelectedID != -1) {
		UINT nID [] = 
		{
			BTN_EDIT,
			BTN_APPLY,
			CB_LINE,
			LBL_LINE,
		};

		for (int i = 0; i < ARRAYSIZE (nID); i++)
			if (CWnd * p = GetDlgItem (nID [i]))
				p->ShowWindow (SW_HIDE);
	}

	OnSelchangeLine ();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDynamicTableDlg::OnApply() 
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CItemArray v;

	ASSERT (pLine);

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (CTableItem * p = (CTableItem *)m_lv.GetCtrlData (i)) {
			CTableItem item = * p;
			
			item.m_bPrompt = m_lv->GetCheck (i) ? true : false;
			v.Add (item);
		}
	}

	CLongArray sel = GetSelectedItems (m_lv);

	m_lLineID = pLine->GetItemData (pLine->GetCurSel ());
	VERIFY (SetDynamicData (m_lLineID, v));

	if (sel.GetSize () > 0) {
		CTableItem * p = (CTableItem *)m_lv.GetCtrlData (sel [0]);
	
		ASSERT (p);
		m_lSelectedID = p->m_lID;
	}

	if (CWnd * p = GetDlgItem (BTN_APPLY))
		p->EnableWindow (FALSE);
}

void CDynamicTableDlg::OnEdit() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		CTableItem * p = (CTableItem *)m_lv.GetCtrlData (sel [0]);

		ASSERT (p);

		if (m_lSelectedID != (LONG)-1) {
			m_lSelectedID = p->m_lID;
			OnOK ();
			return;
		}

		CDynDataPropDlg dlg (this);

		dlg.m_bPrompt		= p->m_bPrompt;
		dlg.m_lID			= p->m_lID;
		dlg.m_strDefData	= p->m_strData;
		dlg.m_strPrompt		= p->m_strPrompt;
		dlg.m_bUseDatabase	= p->m_bUseDatabase;
		dlg.m_params		= p->m_params;

		if (dlg.DoModal () == IDOK) {
			p->m_bPrompt		= dlg.m_bPrompt ? true : false;
			p->m_lID			= dlg.m_lID;
			p->m_strData		= dlg.m_strDefData;
			p->m_strPrompt		= dlg.m_strPrompt;
			p->m_bUseDatabase	= dlg.m_bUseDatabase ? true : false;
			p->m_params			= dlg.m_params;

			m_lv.UpdateCtrlData (p);
			m_lv->SetCheck (sel [0], p->m_bPrompt);

			if (CWnd * p = GetDlgItem (BTN_APPLY))
				p->EnableWindow (TRUE);
		}
	}
	else 
		MessageBox (LoadString (IDS_NOTHINGSELECTED));
}

void CDynamicTableDlg::OnDblclkData(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();

	if (pResult)
		* pResult = 0;
}

void CDynamicTableDlg::OnOK()
{
	OnApply ();
	FoxjetCommon::CEliteDlg::OnOK ();
}

void CDynamicTableDlg::OnItemchangedData(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if (LPNMLISTVIEW pItem = (LPNMLISTVIEW)pNMHDR) {
		if (pItem->uNewState != pItem->uOldState) {
			if (CTableItem * p = (CTableItem *)m_lv.GetCtrlData (pItem->iItem)) {
				p->m_bPrompt = m_lv->GetCheck (pItem->iItem) ? true : false;
				m_lv.UpdateCtrlData (p);
			}
		}
	}
	
	if (pResult)
		* pResult = 0;
}

bool CDynamicTableDlg::GetItem (const CItemArray & vItems, const FoxjetCommon::CBaseElement * pFind, CTableItem & result)
{
	int nIndex = -1;

	if (CDynamicTextElement * p = DYNAMIC_DOWNCAST (CDynamicTextElement, pFind)) 
		nIndex = (int)p->GetDynamicID ();
	else if (CDynamicBarcodeElement * p = DYNAMIC_DOWNCAST (CDynamicBarcodeElement, pFind)) 
		nIndex = (int)p->GetDynamicID ();

	for (int i = 0; i < vItems.GetSize (); i++) {
		CTableItem item = vItems [i];

		if (item.m_lID == nIndex)
			result = item;

		return true;
	}

	return false;
}

CString CDynamicTableDlg::GetPrompt (const CItemArray & vItems, const FoxjetCommon::CBaseElement * pFind)
{
	CTableItem item;

	if (GetItem (vItems, pFind, item))
		return item.GetPrompt ();

	return _T ("");
}

CString CDynamicTableDlg::GetDefaultData (const CItemArray & vItems, const FoxjetCommon::CBaseElement * pFind)
{
	CTableItem item;

	if (GetItem (vItems, pFind, item))
		return item.m_strData;

	return _T ("");
}

FoxjetIpElements::CTableItem CDynamicTableDlg::GetDynamicData (const CItemArray & vItems, int nIndex)
{
	FoxjetIpElements::CTableItem result;

	ASSERT (nIndex > 0);

	for (int i = 0; i < vItems.GetSize (); i++) {
		const CTableItem & item = vItems [i];

		if ((int)item.m_lID == nIndex) {
			result = vItems [i];
			break;
		}
	}

	return result;
}

void CDynamicTableDlg::OnSelchangeLine ()
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CTableItem * pSelected = NULL;

	ASSERT (pLine);
	
	ULONG lLineID = pLine->GetItemData (pLine->GetCurSel ());
	CItemArray vItems;
	
	m_lv.DeleteCtrlData ();
	GetDynamicData (lLineID, vItems);
	
	for (int i = 0; i < vItems.GetSize (); i++) {
		CTableItem * p = new CTableItem (vItems [i]);

		if (!p->m_lID)
			p->m_lID = i + 1;

		m_lv.InsertCtrlData (p);
		int nIndex = m_lv.GetDataIndex (p);
		m_lv->SetCheck (nIndex, p->m_bPrompt);

		if (p->m_lID == m_lSelectedID) {
			m_lv->SetItemState (nIndex, m_lv->GetItemState (nIndex, 0xFFFF) | (LVIS_SELECTED | LVIS_FOCUSED), 0xFFFF);
			pSelected = p;
		}
	}

	if (pSelected)
		m_lv->EnsureVisible (m_lv.GetDataIndex (pSelected), FALSE);

	if (CWnd * p = GetDlgItem (BTN_APPLY))
		p->EnableWindow (FALSE);

	if (GetSelectedItems (m_lv).GetSize () == 0) 
		Select (m_lv, 0);
}

int CDynamicTableDlg::Find (const CItemArray & v, LONG lID)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i].m_lID == lID)
			return i;

	return -1;
}

////////////////////////////////////////////////////////////////////////////////

static CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> vDynData;

static bool GetDynData (ULONG lLineID, SETTINGSSTRUCT & s)
{
	for (int i = 0; i < ::vDynData.GetSize (); i++) {
		if (::vDynData [i].m_lLineID == lLineID) {
			s = ::vDynData [i];
			return true;
		}
	}

	return false;
}

static bool SetDynData (ULONG lLineID, SETTINGSSTRUCT & s)
{
	for (int i = 0; i < ::vDynData.GetSize (); i++) {
		if (::vDynData [i].m_lLineID == lLineID) {
			::vDynData.SetAt (i, s);
			return true;
		}
	}

	::vDynData.Add (s);

	return true;
}

void CDynamicTableDlg::SaveDynamicData ()
{
	for (int i = 0; i < ::vDynData.GetSize (); i++) 
		if (!UpdateSettingsRecord (GetDB (), ::vDynData [i]))
			AddSettingsRecord (GetDB (), ::vDynData [i]);
}

int CDynamicTableDlg::GetDynamicData (ULONG lLineID, CItemArray & vItems)
{
	SETTINGSSTRUCT s;
	CStringArray v;

	vItems.RemoveAll ();

	if (!GetDynData (lLineID, s))
		GetSettingsRecord (GetDB (), lLineID, lpszDynData, s);
	
	FromString (s.m_strData, v);

	for (int i = 0; i < v.GetSize (); i++) {
		CString str = v [i];
		FoxjetIpElements::CTableItem item;

		if (item.FromString (str)) 
			vItems.Add (item);
	}

	// default it if it's not there
	for (int i = v.GetSize (); i < (int)CDynamicTextElement::m_lmtID.m_dwMax; i++) {
		FoxjetIpElements::CTableItem item;

		item.m_lID = i + 1;
		vItems.Add (item);
	}

	{
		for (int i = 0; i < vItems.GetSize (); i++) {
			vItems [i].m_lID = i + 1; // force IDs to be sequential
			//TRACEF (vItems [i].ToString ());
		}
	}

	return vItems.GetSize ();
}

bool CDynamicTableDlg::SetDynamicData (ULONG lLineID, CItemArray & vItems)
{
	bool bResult = false;
	SETTINGSSTRUCT s;
	CStringArray v;

	for (int i = 0; i < vItems.GetSize (); i++) 
		v.Add (vItems [i].ToString ());

	s.m_lLineID = lLineID;
	s.m_strKey	= lpszDynData;
	s.m_strData = ToString (v);

	VERIFY (SetDynData (lLineID, s));
	return true;
}
