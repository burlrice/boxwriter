#if !defined(AFX_DYNAMICTABLEDLG_H__9BEEF2BA_F044_4949_B1BD_A130E9210C4B__INCLUDED_)
#define AFX_DYNAMICTABLEDLG_H__9BEEF2BA_F044_4949_B1BD_A130E9210C4B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DynamicTableDlg.h : header file
//

#include "ListCtrlImp.h"
#include "fj_defines.h"
#include "BaseElement.h"
#include "DynDataPropDlg.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CDynamicTableDlg dialog

namespace FoxjetIpElements
{
	using FoxjetIpElements::CDatabaseParams;

	class ELEMENT_API CTableItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CTableItem (LONG lID = 0, bool bPrompt = false, const CString & strPrompt = _T (""), const CString & strDefData = _T (""));
		CTableItem (const CTableItem & rhs);
		CTableItem & operator = (const CTableItem & rhs);
		virtual ~CTableItem ();

		virtual CString GetDispText (int nColumn) const;

		CString ToString () const;
		bool FromString (const CString & str);

		CString GetPrompt () const;
		bool UsesKeyField () const;
		CString GetDefData () const;

		CString CreateSQL () const;

		CString GetSqlResult () const;
		// throw CDBException, CMemoryException

		LONG			m_lID;
		bool			m_bPrompt;
		CString			m_strPrompt;
		CString			m_strData;
		bool			m_bUseDatabase;
		CDatabaseParams	m_params;
	};

	typedef CArray <CTableItem, CTableItem &> CItemArray;

	class ELEMENT_API CDynamicTableDlg : public FoxjetCommon::CEliteDlg
	{
	public:

		static CString GetPrompt (const CItemArray & vItems, const FoxjetCommon::CBaseElement * pFind);
		static CString GetDefaultData (const CItemArray & vItems, const FoxjetCommon::CBaseElement * pFind);
		static bool GetItem (const CItemArray & vItems, const FoxjetCommon::CBaseElement * pFind, CTableItem & result);

		static CTableItem GetDynamicData (const CItemArray & vItems, int nIndex);
		static int GetDynamicData (ULONG lLineID, CItemArray & vItems);
		static bool SetDynamicData (ULONG lLineID, CItemArray & vItems);

		static void SaveDynamicData (); // sw0849
		static int Find (const CItemArray & v, LONG lID);

		static const CString m_strSkip;

		LONG m_lSelectedID;
		ULONG m_lLineID;

	public:
		virtual void OnOK();

	// Construction
		CDynamicTableDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CDynamicTableDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA


	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CDynamicTableDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

		// Generated message map functions
		//{{AFX_MSG(CDynamicTableDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnEdit();
		afx_msg void OnDblclkData(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnItemchangedData(NMHDR* pNMHDR, LRESULT* pResult);
		//}}AFX_MSG

		afx_msg void OnApply();
		afx_msg void OnSelchangeLine ();
		
		DECLARE_MESSAGE_MAP()
	};
}; //namespace FoxjetIpElements

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DYNAMICTABLEDLG_H__9BEEF2BA_F044_4949_B1BD_A130E9210C4B__INCLUDED_)
