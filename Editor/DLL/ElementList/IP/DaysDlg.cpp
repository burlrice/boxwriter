// DaysDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "DaysDlg.h"
#include "ItiLibrary.h"
#include "EditValueDlg.h"
#include "Extern.h"
#include "fj_system.h"
#include "Extern.h"
#include "Debug.h"
#include "ListImp.h"

using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace FoxjetIpElements;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CArray <LPFJSYSTEM, LPFJSYSTEM> vIpSystems;

static CString strDay [7];
static CString strDaySpecial [7];
static CString strMonth [12];
static CString strMonthSpecial [12];
static CString strAMPM [2];
static CString strHour [24];
struct
{
	UINT		m_nStrID;
	UINT		m_nCtrlID;
	CString *	m_pstrData;
	int			m_nSize;
} static map [] = 
{
	{ IDS_DAYS,				LV_TYPE1,	::strDay,			7	},
	{ IDS_MONTHS,			LV_TYPE2,	::strMonth,			12	},
	{ IDS_DAYS_SPECIAL,		LV_TYPE3,	::strDaySpecial,	7	},
	{ IDS_MONTHS_SPECIAL,	LV_TYPE4,	::strMonthSpecial,	12	},
	{ IDS_HOURS,			LV_TYPE5,	::strHour,			24	},
	{ IDS_AMPM,				LV_TYPE6,	::strAMPM,			2	},
};
static const int nTabs = sizeof (map) / sizeof (map [0]);

/////////////////////////////////////////////////////////////////////////////

CDaysDlg::CItem::CItem (const CString & str, int nIndex)
:	m_str (str), 
	m_nIndex (nIndex)
{
}

CDaysDlg::CItem::~CItem ()
{
}

CString CDaysDlg::CItem::GetDispText (int nColumn) const
{
	CString str;

	switch (nColumn) {
	case 0: 
		str.Format (_T ("%d"), m_nIndex);
		break;
	case 1:
		str = m_str;
	}

	return str;
}

int CDaysDlg::CItem::Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const
{
	CItem & cmp = (CItem &)rhs;

	switch (nColumn) {
	case 0:		return m_nIndex - cmp.m_nIndex;
	case 1:		return m_str.Compare (cmp.m_str);
	}

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CDaysDlg dialog

const FoxjetCommon::LIMITSTRUCT CDaysDlg::m_lmtRollover = { -12,	12	};
const FoxjetCommon::LIMITSTRUCT CDaysDlg::m_lmtWeek1	= { 1,		7	};
const FoxjetCommon::LIMITSTRUCT CDaysDlg::m_lmtWeekDay	= { 1,		7	};
const FoxjetCommon::LIMITSTRUCT CDaysDlg::m_lmtWeek53	= { 52,		53	};

CDaysDlg::CDaysDlg(CWnd* pParent /*=NULL*/)
:	m_lLineID (-1),
	FoxjetCommon::CEliteDlg(CDaysDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDaysDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CDaysDlg::~CDaysDlg ()
{
	for (int i = 0; i < m_vSystems.GetSize (); i++) 
		if (LPFJSYSTEM pSystem = m_vSystems [i]) 
			fj_SystemDestroy (pSystem);

	m_vSystems.RemoveAll ();
}

void CDaysDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDaysDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	DDX_Text(pDX, TXT_WEEKDAY, m_lWeekDay);
	DDV_MinMaxInt (pDX, m_lWeekDay, m_lmtWeekDay.m_dwMin, m_lmtWeekDay.m_dwMax); 

	DDX_Text(pDX, TXT_WEEK1, m_lWeek1);
	DDV_MinMaxInt (pDX, m_lWeek1, m_lmtWeek1.m_dwMin, m_lmtWeek1.m_dwMax); 

	DDX_Text(pDX, TXT_WEEK53, m_lWeek53);
	DDV_MinMaxInt (pDX, m_lWeek53, m_lmtWeek53.m_dwMin, m_lmtWeek53.m_dwMax); 

	DDX_Text(pDX, TXT_ROLLOVER, m_fRolloverHours);
	DDV_MinMaxInt (pDX, (int)m_fRolloverHours, m_lmtRollover.m_dwMin, m_lmtRollover.m_dwMax); 
}


BEGIN_MESSAGE_MAP(CDaysDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDaysDlg)
	ON_NOTIFY(TCN_SELCHANGE, TAB_TYPE, OnSelchangeType)
	ON_NOTIFY(NM_DBLCLK, LV_TYPE1, OnDblclkType)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_CBN_SELCHANGE(CB_LINE, OnSelchangeLine)
	ON_NOTIFY(NM_DBLCLK, LV_TYPE2, OnDblclkType)
	ON_NOTIFY(NM_DBLCLK, LV_TYPE3, OnDblclkType)
	ON_NOTIFY(NM_DBLCLK, LV_TYPE4, OnDblclkType)
	ON_NOTIFY(NM_DBLCLK, LV_TYPE5, OnDblclkType)
	ON_NOTIFY(NM_DBLCLK, LV_TYPE6, OnDblclkType)
	ON_BN_CLICKED(BTN_APPLY, OnApply)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(TXT_WEEKDAY, OnChange)
	ON_EN_CHANGE(TXT_WEEK1, OnChange)
	ON_EN_CHANGE(TXT_WEEK53, OnChange)
	ON_EN_CHANGE(TXT_ROLLOVER, OnChange)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDaysDlg message handlers

void CDaysDlg::OnSelchangeType(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int nTab = GetTab ();
	struct 
	{
		int m_nTab;
		UINT m_nLVID;
	} map [] = 
	{
		{ 0,	LV_TYPE1	},
		{ 1,	LV_TYPE2	},
		{ 2,	LV_TYPE3	},
		{ 3,	LV_TYPE4	},
		{ 4,	LV_TYPE5	},
		{ 5,	LV_TYPE6	},
		{ -1,	-1			},
	};
	
	for (int i = 0; map [i].m_nTab != -1; i++) {
		CWnd * p = GetDlgItem (map [i].m_nLVID);

		ASSERT (p);
		p->ShowWindow ((map [i].m_nTab == nTab) ? SW_SHOW : SW_HIDE);
	}
	
	if (pResult)
		* pResult = 0;
}

BOOL CDaysDlg::OnInitDialog() 
{
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (TAB_TYPE);
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;
	COdbcDatabase & db = ListGlobals::GetDB ();

	ASSERT (pTab);
	ASSERT (pLine);
	
	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();
	
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_INDEX), 40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_VALUE), 120));

	for (int i = 0; i < ::nTabs; i++) {
		pTab->InsertItem (i, LoadString (::map [i].m_nStrID));
		ASSERT (GetDlgItem (::map [i].m_nCtrlID));
		m_lv [i].Create (::map [i].m_nCtrlID, vCols, this, defElements.m_strRegSection);	
	}

	GetLineRecords (vLines);

	for (int i = 0; i < vLines.GetSize (); i++) {
		LPFJSYSTEM pSystem = fj_SystemNew ();
		LINESTRUCT line = vLines [i];
		int nIndex = pLine->AddString (line.m_strName);

		sprintf (pSystem->strID, "%d", line.m_lID);
		m_vSystems.Add (pSystem);
		pLine->SetItemData (nIndex, m_vSystems.GetSize () - 1);

		if (m_lLineID == line.m_lID) 
			pLine->SetCurSel (nIndex);
	}

	if (pLine->GetCurSel () == CB_ERR && pLine->GetCount ())
		pLine->SetCurSel (0);

	pTab->SetCurSel (0);
	OnSelchangeType (NULL, NULL);
	OnSelchangeLine ();

	return bResult;
}

LPFJSYSTEM CDaysDlg::GetCurSystem () const
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);

	ASSERT (pLine);

	int nIndex = pLine->GetCurSel ();

	if (nIndex != CB_ERR) 
		return m_vSystems [pLine->GetItemData (nIndex)];

	return NULL;
}

int CDaysDlg::GetTab() const
{
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (TAB_TYPE);

	ASSERT (pTab);
	return pTab->GetCurSel ();
}

void CDaysDlg::OnDblclkType(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();
	*pResult = 0;
}

void CDaysDlg::OnEdit() 
{
	int nTab = GetTab ();
	ListCtrlImp::CListCtrlImp & lv = m_lv [nTab];
	CLongArray sel = GetSelectedItems (lv);
	LPFJSYSTEM pSystem = GetCurSystem ();

	if (pSystem && sel.GetSize ()) {
		CEditValueDlg dlg (this);
		CItem * p = (CItem *)lv.GetCtrlData (sel [0]);

		dlg.m_nIndex = p->m_nIndex;
		dlg.m_strValue = p->m_str;

		if (dlg.DoModal () == IDOK) {
			int nIndex = p->m_nIndex - 1;

			p->m_str = dlg.m_strValue;
			lv.UpdateCtrlData (p);

			switch (nTab) {
			case 0:	// days
				strncpy (pSystem->aDayN [nIndex], w2a (p->m_str), FJDATETIME_STRING_SIZE);
				break;
			case 1:	// months
				strncpy (pSystem->aMonthN [nIndex], w2a (p->m_str), FJDATETIME_STRING_SIZE);
				break;
			case 2:	// days special
				strncpy (pSystem->aDayS [nIndex], w2a (p->m_str), FJDATETIME_STRING_SIZE);
				break;
			case 3:	// months special
				strncpy (pSystem->aMonthS [nIndex], w2a (p->m_str), FJDATETIME_STRING_SIZE);
				break;
			case 4:	// hours
				strncpy (pSystem->aHour [nIndex], w2a (p->m_str), FJDATETIME_STRING_SIZE);
				break;
			case 5:	// am/pm
				strncpy (pSystem->aAMPM [nIndex], w2a (p->m_str), FJDATETIME_STRING_SIZE);
				break;
			}

			ASSERT (GetDlgItem (BTN_APPLY));
			GetDlgItem (BTN_APPLY)->EnableWindow (TRUE);
		}
	}
	else
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
}

void CDaysDlg::OnOK()
{
	if (Apply ())
		FoxjetCommon::CEliteDlg::OnOK ();
}

void CDaysDlg::OnSelchangeLine() 
{
	if (LPFJSYSTEM pSystem = GetCurSystem ()) {
		COdbcDatabase & db = ListGlobals::GetDB ();
		SETTINGSSTRUCT time;
		ULONG lLineID = strtoul (pSystem->strID, NULL, 10);
		
		for (int i = 0; i < m_vSystems.GetSize (); i++) {
			LPFJSYSTEM pLocal = m_vSystems [i];
			ULONG lID = strtoul (pLocal->strID, NULL, 10);

			if (LPCFJSYSTEM pGlobal = GetSystem (lID)) {
				char sz [1024] = { 0 };

				// pLocal = pGlobal 

				fj_SystemTimeToString (pGlobal, sz);
				VERIFY (fj_SystemFromString (pLocal, sz));
					
				fj_SystemDateStringsToString (pGlobal, sz);
				VERIFY (fj_SystemFromString (pLocal, sz));
			}
		}

		for (int i = 0; i < 7; i++) {
			::strDay [i]		= pSystem->aDayN [i];
			::strDaySpecial [i]	= pSystem->aDayS [i];
		}

		for (int i = 0; i < 12; i++) {
			::strMonth [i]			= pSystem->aMonthN [i];
			::strMonthSpecial [i]	= pSystem->aMonthS [i];
		}

		for (int i = 0; i < 2; i++) 
			::strAMPM [i] = pSystem->aAMPM [i];

		for (int i = 0; i < 24; i++) 
			::strHour [i] = pSystem->aHour [i];

		GetSettingsRecord (db, lLineID, ListGlobals::Keys::m_strTime, time);
		VERIFY (fj_SystemFromString (pSystem, w2a (time.m_strData)));

		m_lWeekDay			= pSystem->lWeekDay;
		m_lWeek1			= pSystem->lWeek1;
		m_lWeek53			= pSystem->lWeek53;
		m_fRolloverHours	= pSystem->fRolloverHours;

		ASSERT (GetDlgItem (BTN_APPLY));
		GetDlgItem (BTN_APPLY)->EnableWindow (FALSE);
		UpdateData (FALSE);
	}
	
	for (int i = 0; i < ::nTabs; i++) {
		m_lv [i].DeleteCtrlData ();

		for (int j = 0; j < map [i].m_nSize; j++)
			m_lv [i].InsertCtrlData (new CItem (map [i].m_pstrData [j], j + 1));
	}
}

void CDaysDlg::OnChange() 
{
	ASSERT (GetDlgItem (BTN_APPLY));
	GetDlgItem (BTN_APPLY)->EnableWindow (TRUE);
}

bool CDaysDlg::Apply()
{
	char sz [1024] = { 0 };
	SETTINGSSTRUCT s;

	if (!UpdateData (TRUE))
		return false;

	LPFJSYSTEM pLocal = GetCurSystem ();
	ASSERT (pLocal);

	m_lLineID = strtoul (pLocal->strID, NULL, 10);

	COdbcDatabase & db = ListGlobals::GetDB ();
	LPFJSYSTEM pGlobal = GetSystem (m_lLineID);
	ASSERT (pGlobal);

	fj_SystemDateStringsToString (pLocal, sz);
	s.m_lLineID = m_lLineID;
	s.m_strKey	= ListGlobals::Keys::m_strDays;
	s.m_strData = sz;

	if (!UpdateSettingsRecord (GetDB (), s))
		VERIFY (AddSettingsRecord (GetDB (), s));

	for (int i = 0; i < 7; i++) {
		strncpy (pGlobal->aDayN [i], pLocal->aDayN [i], FJDATETIME_STRING_SIZE);
		strncpy (pGlobal->aDayS [i], pLocal->aDayS [i], FJDATETIME_STRING_SIZE);
	}

	for (int i = 0; i < 12; i++) {
		strncpy (pGlobal->aMonthN [i], pLocal->aMonthN [i], FJDATETIME_STRING_SIZE);
		strncpy (pGlobal->aMonthS [i], pLocal->aMonthS [i], FJDATETIME_STRING_SIZE);
	}

	for (int i = 0; i < 2; i++) 
		strncpy (pGlobal->aAMPM [i], pLocal->aAMPM [i], FJDATETIME_STRING_SIZE);

	for (int i = 0; i < 24; i++) 
		strncpy (pGlobal->aHour [i], pLocal->aHour [i], FJDATETIME_STRING_SIZE);

	pGlobal->lWeekDay		= m_lWeekDay;
	pGlobal->lWeek1			= m_lWeek1;
	pGlobal->lWeek53		= m_lWeek53;
	pGlobal->fRolloverHours	= m_fRolloverHours;

	fj_SystemTimeToString (pGlobal, sz);

	s.m_lLineID = atol (pGlobal->strID);
	s.m_strKey	= ListGlobals::Keys::m_strTime;
	s.m_strData = sz;

	if (!UpdateSettingsRecord (GetDB (), s))
		VERIFY (AddSettingsRecord (GetDB (), s));

	ASSERT (GetDlgItem (BTN_APPLY));
	GetDlgItem (BTN_APPLY)->EnableWindow (FALSE);

	return true;
}

void CDaysDlg::OnApply() 
{
	Apply ();
}
