#include "stdafx.h"
#include "fj_export.h"
#include "fj_element.h"

const CHAR fj_FontID[]				= "Font";
const CHAR fj_LabelID[]				= "Label";
const CHAR fj_BarCodeID[]			= "Barcode";
const CHAR fj_DynBarCodeDefData[]	= "000000";
const CHAR fj_BarCodeFont32[]		= "barcode";
const CHAR fj_BarCodeFont256[]		= "barcode256";

extern "C" 
{
	FJDLLExport VOID fj_TextToStr( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_TextFromStr( LPFJELEMENT pfe, LPCSTR pStr );

	FJDLLExport VOID fj_BarCodeToStr( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_BarCodeFromStr( LPFJELEMENT pfe, LPSTR pStr );

	FJDLLExport VOID fj_BitmapToString( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_BitmapFromString( LPFJELEMENT pfe, LPBYTE pBuff );

	FJDLLExport VOID fj_DynimageToString( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_DynimageFromString( LPFJELEMENT pfe, LPBYTE pBuff );

	FJDLLExport VOID fj_DatetimeToStr( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_DatetimeFromStr( LPFJELEMENT pfe, LPCSTR pStr );

	FJDLLExport VOID fj_CounterToStr( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_CounterFromStr( LPFJELEMENT pfe, LPCSTR pStr );

	FJDLLExport VOID fj_DynTextToStr( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_DynTextFromStr( LPFJELEMENT pfe, LPCSTR pStr );

	FJDLLExport VOID fj_DynBarCodeToStr( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_DynBarCodeFromStr( LPFJELEMENT pfe, LPCSTR pStr );

	FJDLLExport VOID fj_DataMatrixToStr( CLPCFJELEMENT pfe, LPSTR pStr );
	FJDLLExport VOID fj_DataMatrixFromStr( LPFJELEMENT pfe, LPCSTR pStr );
}

void BarCode_FromString (LPFJELEMENT pfe, LPCSTR pStr )
{
	CString str (pStr);
	fj_BarCodeFromStr (pfe, w2a (str));
}

void Bitmap_FromString (LPFJELEMENT pfe, LPCSTR pStr )
{
	CString str (pStr);
	//fj_BitmapFromString (pfe, (LPBYTE)str.GetBuffer (str.GetLength ()));
	fj_BitmapFromString (pfe, (LPBYTE)(char *)w2a (str));
}

LPPFROMSTRINGFCT GetFromStringFct (ELEMENTTYPE type)
{
	switch (type) {
	case TEXT:				return fj_TextFromStr;
	case BARCODE:			return BarCode_FromString;
	case BMP:				return Bitmap_FromString;
	case DATETIME:			return fj_DatetimeFromStr;
	case COUNT:				return fj_CounterFromStr;
	case DYNAMIC_TEXT:		return fj_DynTextFromStr;
	case DYNAMIC_BARCODE:	return fj_DynBarCodeFromStr;
	case DATAMATRIX:		return fj_DataMatrixFromStr;
	}

	ASSERT (0);
	return NULL;
}

LPPTOSTRINGFCT GetToStringFct (ELEMENTTYPE type)
{
	switch (type) {
	case TEXT:				return fj_TextToStr;
	case BARCODE:			return fj_BarCodeToStr;
	case BMP:				return fj_BitmapToString;
	case DATETIME:			return fj_DatetimeToStr;
	case COUNT:				return fj_CounterToStr;
	case DYNAMIC_TEXT:		return fj_DynTextToStr;
	case DYNAMIC_BARCODE:	return fj_DynBarCodeToStr;
	case DATAMATRIX:		return fj_DataMatrixToStr;
	}

	ASSERT (0);
	return NULL;
}

