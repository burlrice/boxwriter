#include "stdafx.h"
#include "List.h"
#include "Database.h"
#include "Extern.h"
#include "TemplExt.h"
#include "ListImp.h"
#include "Debug.h"

#include "fj_font.h"
#include "fj_system.h"
#include "fj_printhead.h"
#include "fj_label.h"
#include "fj_export.h"


using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ListGlobals;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

static CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
static CArray <LINESTRUCT, LINESTRUCT &> vLines;
extern CArray <LPFJSYSTEM, LPFJSYSTEM> vIpSystems;

bool InitDatabase (FoxjetDatabase::COdbcDatabase & database)
{
	DBMANAGETHREADSTATE (database);

	::vLines.RemoveAll ();
	::vHeads.RemoveAll ();

	if (!FoxjetDatabase::GetLineRecords (database, ::vLines)) {
		TRACEF ("no lines in database");
		return false;
	}

	if (!FoxjetDatabase::GetHeadRecords (database, ::vHeads)) {
		TRACEF ("no heads in database");
		return false;
	}


	return true;
}

// these functions have identical signatures to their counterparts in
// the "Database" project, except the "COdbcDatabase *" param.
// InitDatabase caches the data here to avoid hitting the database
// while printing

ELEMENT_API UINT FoxjetCommon::GetLineRecordCount ()
{
	return ::vLines.GetSize ();
}

ELEMENT_API ULONG FoxjetCommon::GetLineID (const CString & str)
{
	for (int i = 0; i < ::vLines.GetSize (); i++) {
		const LINESTRUCT & line = ::vLines [i];

		if (!line.m_strName.CompareNoCase (str))
			return line.m_lID;
	}

	return NOTFOUND;
}

ELEMENT_API bool FoxjetCommon::GetLineRecord (ULONG lID, FoxjetDatabase::LINESTRUCT & cs)
{
	for (int i = 0; i < ::vLines.GetSize (); i++) {
		const LINESTRUCT & line = ::vLines [i];

		if (line.m_lID == lID) {
			cs = line;
			return true;
		}
	}

	return false;
}

ELEMENT_API bool FoxjetCommon::GetLineRecords (CArray <FoxjetDatabase::LINESTRUCT, FoxjetDatabase::LINESTRUCT &> & v)
{
	Copy (v, ::vLines);

	return v.GetSize () > 0;
}

ELEMENT_API bool FoxjetCommon::GetFirstLineRecord (FoxjetDatabase::LINESTRUCT & line)
{
	if (::vLines.GetSize ()) {
		line = ::vLines [0];
		return true;
	}

	return false;
}

ELEMENT_API bool FoxjetCommon::GetLineHeads (ULONG lID, CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & v)
{
	if (LPFJSYSTEM pSystem = GetSystem (lID)) {
		for (int j = 0; j < FJSYS_NUMBER_PRINTERS; j++) {
			if (LPFJPRINTHEAD p = pSystem->gmMembers [j].pfph) {
				HEADSTRUCT head;
				ULONG lHeadID = strtoul (p->strID, NULL, 10);

				VERIFY (GetHeadRecord (lHeadID, head));
				v.Add (head);
			}
		}
	}

	return v.GetSize () > 0;
}

ELEMENT_API UINT FoxjetCommon::GetHeadRecordCount ()
{
	return ::vHeads.GetSize ();
}

ELEMENT_API bool FoxjetCommon::GetHeadRecord (ULONG lID, FoxjetDatabase::HEADSTRUCT & hs)
{
	for (int i = 0; i < ::vHeads.GetSize (); i++) {
		HEADSTRUCT & head = ::vHeads [i];

		if (head.m_lID == lID) {
			hs = head;
			return true;
		}
	}

	return false;
}

ELEMENT_API bool FoxjetCommon::GetHeadRecords (CArray <FoxjetDatabase::HEADSTRUCT, FoxjetDatabase::HEADSTRUCT &> & v)
{
	Copy (v, ::vHeads);

	return v.GetSize () > 0;
}

ELEMENT_API ULONG FoxjetCommon::GetHeadID (const CString & strUID)
{
	for (int i = 0; i < ::vHeads.GetSize (); i++) {
		HEADSTRUCT & head = ::vHeads [i];

		if (!head.m_strUID.CompareNoCase (strUID))
			return head.m_lID;
	}

	return NOTFOUND;
}

ELEMENT_API ULONG FoxjetCommon::GetHeadID (ULONG lLineID, const CString & strHead)
{
	for (int i = 0; i < ::vHeads.GetSize (); i++) {
		HEADSTRUCT & head = ::vHeads [i];

		if (!head.m_strName.CompareNoCase (strHead))
			return head.m_lID;
	}

	return NOTFOUND;
}

ELEMENT_API bool FoxjetCommon::GetFirstHeadRecord (FoxjetDatabase::HEADSTRUCT & head)
{
	if (::vHeads.GetSize ()) {
		head = ::vHeads [0];
		return true;
	}

	return false;
}
