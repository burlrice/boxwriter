#ifndef __EDIT_H__
#define __EDIT_H__

#include "define.h"

#include <afxwin.h>
#include "List.h"
#include "BaseElement.h"
#include "CopyArray.h"

FORWARD_DECLARE (fjsystem, FJSYSTEM)

namespace FoxjetIpElements
{
	bool ELEMENT_API CALLBACK OnEditTextElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditBitmapElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditCountElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditDateTimeElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditBarcodeElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditDynamicTextElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditDynamicBarcodeElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);
	bool ELEMENT_API CALLBACK OnEditDataMatrixElement (FoxjetCommon::CBaseElement & element, 
		const FoxjetCommon::CElementList * pList, 
		CWnd * pParent, const CSize & pa, UNITS units,
		bool bReadOnlyElement, bool bReadOnlyLocation,
		FoxjetCommon::CElementListArray * pAllLists,
		FoxjetCommon::CElementArray * pUpdate);

	void ELEMENT_API CALLBACK OnDefineBitmapConfig (CWnd * pParent, const FoxjetCommon::CVersion & version);
	bool ELEMENT_API CALLBACK OnDefineFonts (CWnd * pParent, const FoxjetCommon::CVersion & version);
	bool ELEMENT_API CALLBACK OnDefineGlobalBarcodes (CWnd * pParent, const FoxjetCommon::CVersion & version); 
	bool ELEMENT_API CALLBACK OnDefineShiftCodes (CWnd * pParent, const FoxjetCommon::CVersion & version); 
	bool ELEMENT_API CALLBACK OnDefineDateTimeSettings  (CWnd * pParent, const FoxjetCommon::CVersion & version); 
	bool ELEMENT_API CALLBACK OnDefineDynamic (CWnd * pParent, const FoxjetCommon::CVersion & version); 
	bool ELEMENT_API CALLBACK OnDefineDataMatrixParams (CWnd * pParent, const FoxjetCommon::CVersion & version); 

	void ELEMENT_API LoadBarcodeParams (FoxjetDatabase::COdbcDatabase & db, const FoxjetCommon::CVersion & ver);

	LPFJSYSTEM ELEMENT_API FindSystem (ULONG lLineID);
}; // namespace FoxjetIpElements

void CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer);

#endif //__EDIT_H__