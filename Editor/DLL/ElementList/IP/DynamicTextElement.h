// DynamicTextElement.h: interface for the CDynamicTextElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DYNAMICTEXTELEMENT_H__9955271E_0354_4897_AF50_AEF68CE73D4D__INCLUDED_)
#define AFX_DYNAMICTEXTELEMENT_H__9955271E_0354_4897_AF50_AEF68CE73D4D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TextElement.h"
#include "Types.h"
#include "fj_element.h"

FORWARD_DECLARE (fjdyntext, FJDYNTEXT)

namespace FoxjetIpElements
{
	class ELEMENT_API CDynamicTextElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CDynamicTextElement);

	public:

		CDynamicTextElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CDynamicTextElement (const CDynamicTextElement & rhs);
		CDynamicTextElement & operator = (const CDynamicTextElement & rhs);
		virtual ~CDynamicTextElement ();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual bool SetDefaultData(const CString &strData);
		virtual bool SetElement (LPFJELEMENT pNew);
		virtual void CreateImage ();
		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		virtual CString GetElementFontName () const;
		virtual bool SetFont (const FoxjetCommon::CPrinterFont & f);


		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		virtual LONG GetBold () const;
		virtual bool SetBold (LONG lValue);

		virtual int GetWidth () const;
		virtual bool SetWidth (int nValue);
		
		LONG GetGap () const;
		bool SetGap (LONG lValue);

		int GetDynamicID () const;
		bool SetDynamicID (LONG lID);

		LONG GetLength () const;
		bool SetLength (LONG l);

		LONG GetFirstChar () const;
		bool SetFirstChar (LONG l);

		LONG GetNumChar () const;
		bool SetNumChar (LONG l);

		CHAR GetAlign () const;
		bool SetAlign (CHAR c);

		CHAR GetFill () const;
		bool SetFill (CHAR c);
		
		DECLARE_MEMBERPOINTERACCESS (LPFJDYNTEXT, GetElement ()->pDesc)

		static const FoxjetCommon::LIMITSTRUCT m_lmtID;
		static const FoxjetCommon::LIMITSTRUCT m_lmtLen;

	};
}; //namespace FoxjetIpElements

#endif // !defined(AFX_DYNAMICTEXTELEMENT_H__9955271E_0354_4897_AF50_AEF68CE73D4D__INCLUDED_)
