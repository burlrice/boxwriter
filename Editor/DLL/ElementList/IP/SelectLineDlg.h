#if !defined(AFX_SELECTLINEDLG_H__DBC27DEF_E806_4AB8_8C3F_69090555EF6F__INCLUDED_)
#define AFX_SELECTLINEDLG_H__DBC27DEF_E806_4AB8_8C3F_69090555EF6F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectLineDlg.h : header file
//

#include "ListCtrlImp.h"
#include "Database.h"
#include "Resource.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CSelectLineDlg dialog

class CSelectLineDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CSelectLineDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelectLineDlg)
	enum { IDD = IDD_SELECTLINE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


	ULONG m_lLineID;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectLineDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();

	class CItem : 
		public ItiLibrary::ListCtrlImp::CItem, 
		public FoxjetDatabase::LINESTRUCT
	{
	public:
		CItem (const FoxjetDatabase::LINESTRUCT & rhs);
		virtual ~CItem ();

		virtual CString GetDispText (int nColumn) const;
	};

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

	// Generated message map functions
	//{{AFX_MSG(CSelectLineDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkLine(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTLINEDLG_H__DBC27DEF_E806_4AB8_8C3F_69090555EF6F__INCLUDED_)
