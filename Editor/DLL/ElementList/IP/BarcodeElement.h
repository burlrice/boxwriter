// BarcodeElement.h: interface for the CBarcodeElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BARCODEELEMENT_H__24FAA1ED_A437_4C72_8364_7284B782E3C5__INCLUDED_)
#define AFX_BARCODEELEMENT_H__24FAA1ED_A437_4C72_8364_7284B782E3C5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IpBaseElement.h"
#include "PrinterFont.h"
#include "Types.h"
#include "fj_element.h"
#include "fj_barcode.h"

FORWARD_DECLARE (fjbarcode,		FJBARCODE)
FORWARD_DECLARE (fjsystem,		FJSYSTEM)
FORWARD_DECLARE (fjsysbarcode,	FJSYSBARCODE)
FORWARD_DECLARE (fjmessage,		FJMESSAGE)
 
namespace FoxjetIpElements
{
	class ELEMENT_API CBarcodeElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CBarcodeElement);

	public:
		CBarcodeElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CBarcodeElement (const CBarcodeElement & rhs);
		CBarcodeElement & operator = (const CBarcodeElement & rhs);
		virtual ~CBarcodeElement ();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		bool SetDefaultData (const CString &strData);

		virtual void CreateImage ();
		virtual CString GetElementFontName () const;
		virtual bool SetElement (LPFJELEMENT pNew);

		virtual bool Validate ();
		virtual bool IsValid () const;

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		DECLARE_MEMBERPOINTERACCESS (LPFJBARCODE, GetElement ()->pDesc)

	public:

		bool GetOutputText () const;
		bool SetOutputText (bool bValue);

		LONG GetBold () const;
		bool SetBold (LONG lValue);
		
		CString GetText () const;
		bool SetText (const CString & strValue);
		
		LONG GetTextBold () const;
		bool SetTextBold (LONG lValue);
		
		virtual int GetWidth () const;
		virtual bool SetWidth (int nValue);
		
		LONG GetBcType () const;
		bool SetBcType (LONG lValue);
		
		CHAR GetTextAlign () const;
		bool SetTextAlign (CHAR cValue);

		const FJSYSBARCODE & GetGlobal () const;

		static bool IsValidGlobalIndex (int nIndex);

		static CString GetBcType (LONG lBCFlags);
		static CString GetBcName (LPFJSYSTEM pSystem, int nIndex);
		static CString GetBcType (LPFJSYSTEM pSystem, int nIndex);

		static CString GetName (LPFJSYSTEM pSystem, int nIndex);
		static bool SetName (LPFJSYSTEM pSystem, int nIndex, const CString & str);
		
		static LONG GetVerticalBearer (LPFJSYSTEM pSystem, int nIndex);
		static bool SetVerticalBearer (LPFJSYSTEM pSystem, int nIndex, LONG lValue);
		
		static LONG GetHorizontalBearer (LPFJSYSTEM pSystem, int nIndex);
		static bool SetHorizontalBearer (LPFJSYSTEM pSystem, int nIndex, LONG lValue);
		
		static LONG GetBarValue (LPFJSYSTEM pSystem, int nGlobalIndex, int nBarIndex);
		static bool SetBarValue (LPFJSYSTEM pSystem, int nGlobalIndex, int nBarIndex, LONG lValue);

		static bool GetChecksum (LPFJSYSTEM pSystem, int nIndex);
		static bool SetChecksum (LPFJSYSTEM pSystem, int nIndex, bool bValue);

		static LONG GetHeight (LPFJSYSTEM pSystem, int nIndex);
		static bool SetHeight (LPFJSYSTEM pSystem, int nIndex, LONG lValue);
		
		static LONG GetQuietZone (LPFJSYSTEM pSystem, int nIndex);
		static bool SetQuietZone (LPFJSYSTEM pSystem, int nIndex, LONG lValue);

		static LONG GetBCFlags (LPFJSYSTEM pSystem, int nIndex);
		static bool SetBCFlags (LPFJSYSTEM pSystem, int nIndex, LONG lValue);

		static CString GetMagName (int nIndex);
		static CString GetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
			ULONG lLineID, FoxjetDatabase::HEADTYPE type);
		static bool SetBarcodeSettings (FoxjetDatabase::COdbcDatabase & database, 
			ULONG lLineID, FoxjetDatabase::HEADTYPE type, const CString & strSettings);
		static CString GetHeadTypeString (FoxjetDatabase::HEADTYPE type);

		static CString GetDefaultData (ULONG lType);
	
		static const FoxjetCommon::LIMITSTRUCT m_lmtString;

	protected:
		virtual void Invalidate ();
		void LoadGlobalParams ();

		int m_nEncode;
	};
}; //namespace FoxjetIpElements

#endif // !defined(AFX_BARCODEELEMENT_H__24FAA1ED_A437_4C72_8364_7284B782E3C5__INCLUDED_)
