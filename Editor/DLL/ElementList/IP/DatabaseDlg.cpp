// DatabaseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "DatabaseDlg.h"
#include "TextElement.h"
#include "Debug.h"
#include "OdbcDatabase.h"
#include "OdbcTable.h"
#include "OdbcRecordset.h"
#include "ElementDlg.h"
#include "DbFieldDlg.h"
#include "Extern.h"
#include "Parse.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ListGlobals;
using namespace FoxjetIpElements;
using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace FoxjetUtils;

/////////////////////////////////////////////////////////////////////////////
// CDatabaseDlg dialog

const FoxjetCommon::LIMITSTRUCT CDatabaseDlg::m_lmtKey = { 0,	CTextElement::m_lmtString.m_dwMax };

CDatabaseDlg::CDatabaseDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(IDD_DATABASE, pParent)
{
	//{{AFX_DATA_INIT(CDatabaseDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

void CDatabaseDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDatabaseDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate)
		UpdateKeyField ();

	DDX_Text (pDX, TXT_DSN, m_strDSN);
	DDX_Text (pDX, TXT_ROW, m_nRow);

	DDX_Text (pDX, TXT_KEYVALUE, m_strKeyValue);
	DDV_MinMaxChars (pDX, m_strKeyValue, TXT_KEYVALUE, m_lmtKey);

	DDX_Text (pDX, TXT_SQL, m_strSQL);
}


BEGIN_MESSAGE_MAP(CDatabaseDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDatabaseDlg)
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_CBN_SELCHANGE(CB_TABLE, OnSelchangeTable)
	ON_CBN_SELCHANGE(CB_FIELD, OnSelchangeField)
	ON_BN_CLICKED(BTN_SELECT, OnSelect)
	ON_BN_CLICKED(BTN_SELECTKEY, OnSelectKey)
	ON_BN_CLICKED(CHK_KEY, OnKey)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB, OnSelchangeTab)
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE(CB_KEYFIELD, UpdateKeyField)
END_MESSAGE_MAP()

/* TODO: rem
int CDatabaseDlg::BuildElement ()
{
	CDatabaseElement & e = GetElement ();
	CString strDSN, strTable, strField, strKeyField, strKeyValue, strSQL;
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	CComboBox * pKeyField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB);

	ASSERT (pTable);
	ASSERT (pField);
	ASSERT (pKeyField);
	ASSERT (pTab);

	GetDlgItemText (TXT_DSN, strDSN);
	pTable->GetLBText (pTable->GetCurSel (), strTable);
	pField->GetLBText (pField->GetCurSel (), strField);
	GetDlgItemText (TXT_SQL, strSQL);

	if (CElementDlg::GetCheck (this, CHK_KEY)) {
		GetDlgItemText (TXT_KEYVALUE, strKeyValue);
		pKeyField->GetLBText (pKeyField->GetCurSel (), strKeyField);
	}

	e.SetDSN (strDSN);
	e.SetTable (strTable);
	e.SetField (strField);
	e.SetKeyField (strKeyField);
	e.SetKeyValue (strKeyValue);
	e.SetRow (GetDlgItemInt (TXT_ROW));
	e.SetSQL (pTab->GetCurSel () == 1, strSQL); 
	e.SetOrientation (GetOrientation ());
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));

	CString strError;

	try {
		e.Validate ();
	}
	catch (CDBException * e)		{ strError = HANDLEEXCEPTION_TRACEONLY (e); }
	catch (CMemoryException * e)	{ strError = HANDLEEXCEPTION_TRACEONLY (e); }

	if (strError.GetLength ()) {
		strError = e.CreateSQL () + _T ("\n\n") + strError;
		MsgBox (* this, strError, LoadString (IDS_ERROR), MB_ICONERROR);
		return UNKNOWN;
	}

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}
*/

BOOL CDatabaseDlg::OnInitDialog() 
{
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB);

	ASSERT (pTab);
	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();

	pTab->InsertItem (0, LoadString (IDS_BUILD));
	pTab->InsertItem (1, LoadString (IDS_GENERAL));

	InitTables (m_strDSN, m_strTable, m_strField, m_strKeyField);

	if (m_strKeyField.GetLength () || m_strKeyValue.GetLength ()) {
		CButton * p = (CButton *)GetDlgItem (CHK_KEY);

		ASSERT (p);
		p->SetCheck (1);
	}

	if (m_bSQL)
		pTab->SetCurSel (1);

	OnKey ();
	OnSelchangeTab (NULL, NULL);

	return bResult;
}

void CDatabaseDlg::OnBrowse() 
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	COdbcDatabase db (_T (__FILE__), __LINE__);

	ASSERT (pTable);

	try {
		if (db.Open (NULL)) {
			CString strDSN = Extract (db.GetConnect (), _T ("DSN="), _T (";"));

			if (!db.GetTableCount ()) {
				CString str;

				str.Format (LoadString (IDS_DSNHASNOTABLES), strDSN);
				MsgBox (* this, str);
			}
			else {
				SetDlgItemText (TXT_DSN, strDSN);
				InitTables (strDSN);				
			}

			db.Close ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
}

void CDatabaseDlg::InitTables (const CString & strDSN, const CString & strTable, const CString & strField, 
							   const CString & strKeyField) 
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);

	ASSERT (pTable);
	pTable->ResetContent ();

	if (strDSN.GetLength ()) {
		COdbcDatabase db (_T (__FILE__), __LINE__);

		try {
			CString strTableTmp (strTable);

			db.Open (strDSN, FALSE, TRUE); 

			for (int i = 0; i < db.GetTableCount (); i++) {
				CString strName = db.GetTable (i)->GetName ();
				int nIndex = pTable->AddString (strName);

				if (!strName.CompareNoCase (strTable)) 
					pTable->SetCurSel (nIndex);
			}

			if (pTable->GetCurSel () == CB_ERR) {
				pTable->SetCurSel (0);

				if (pTable->GetLBTextLen (0) > 0)
					pTable->GetLBText (0, strTableTmp);
			}

			db.Close ();
			InitFields (strDSN, strTableTmp, strField, strKeyField);
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}
}

void CDatabaseDlg::InitFields (const CString & strDSN, const CString & strTable, const CString & strField,
							   const CString & strKeyField) 
{
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	CComboBox * pKeyField = (CComboBox *)GetDlgItem (CB_KEYFIELD);

	ASSERT (pField);
	ASSERT (pKeyField);

	pField->ResetContent ();
	pKeyField->ResetContent ();

	if (strDSN.GetLength ()) {
		COdbcDatabase db (_T (__FILE__), __LINE__);

		try {
			db.Open (strDSN, FALSE, TRUE); 

			COdbcRecordset rst (db);
			CString strSQL;

			strSQL.Format (_T ("SELECT * FROM [%s]"),
				strTable);

			if (rst.Open (strSQL, CRecordset::forwardOnly)) {
				for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
					CODBCFieldInfo info;

					rst.GetODBCFieldInfo (i, info);
					CString strName = info.m_strName;
					int nIndex = pField->AddString (strName);

					if (!strName.CompareNoCase (strField))
						pField->SetCurSel (nIndex);

					nIndex = pKeyField->AddString (strName);
					pKeyField->SetItemData (nIndex, info.m_nSQLType);

					TRACEF (strName + _T (": ") + ToString (info.m_nSQLType));

					if (!strName.CompareNoCase (strKeyField))
						pKeyField->SetCurSel (nIndex);
				}

				rst.Close ();

				if (pField->GetCurSel () == CB_ERR) {
					pField->SetCurSel (0);
					SetDlgItemInt (TXT_ROW, 1);
				}

				if (pKeyField->GetCurSel () == CB_ERR) 
					pKeyField->SetCurSel (0);

				UpdateKeyField ();
			}

			db.Close ();
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}
}

void CDatabaseDlg::OnSelchangeTable() 
{
	CString strDSN, strTable;	
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);

	ASSERT (pTable);
	GetDlgItemText (TXT_DSN, strDSN);

	if (pTable->GetLBTextLen (pTable->GetCurSel ()) > 0)
		pTable->GetLBText (pTable->GetCurSel (), strTable);

	InitFields (strDSN, strTable);
	SetDlgItemInt (TXT_ROW, 1);
}

void CDatabaseDlg::OnSelchangeField() 
{
}

void CDatabaseDlg::OnOK()
{
	if (!UpdateData ())
		return;

	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	CComboBox * pKeyField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB);

	ASSERT (pTable);
	ASSERT (pField);
	ASSERT (pKeyField);
	ASSERT (pTab);

	int nIndex = pTable->GetCurSel ();

	if (nIndex == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOTABLESELECTED));
		return;
	}

	pTable->GetLBText (nIndex, m_strTable);

	nIndex = pField->GetCurSel ();

	if (nIndex == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOFIELDSELECTED));
		return;
	}

	pField->GetLBText (nIndex, m_strField);

	if (CElementDlg::GetCheck (this, CHK_KEY)) {
		nIndex = pKeyField->GetCurSel ();

		if (nIndex != CB_ERR) 
			pKeyField->GetLBText (nIndex, m_strKeyField);
	}
	else {
		m_strKeyField = _T ("");
		SetDlgItemText (TXT_KEYVALUE, m_strKeyValue = _T (""));
	}

	m_bSQL = pTab->GetCurSel () == 1;

	FoxjetCommon::CEliteDlg::OnOK ();
}

void CDatabaseDlg::OnSelect() 
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_FIELD);
	CInternalFormatting opt (false);
	CDbFieldDlg dlg (defElements.m_strRegSection, this);
	

	ASSERT (pTable);
	ASSERT (pField);

	GetDlgItemText (TXT_DSN, dlg.m_strDSN);

	if (pTable->GetLBTextLen (pTable->GetCurSel ()) > 0)
		pTable->GetLBText (pTable->GetCurSel (), dlg.m_strTable);
	
	if (pField->GetLBTextLen (pField->GetCurSel ()) > 0)
		pField->GetLBText (pField->GetCurSel (), dlg.m_strField);

	dlg.m_nRow = 0; //GetDlgItemInt (TXT_ROW);
	dlg.m_nMaxRows = 100;
	
	if (dlg.DoModal () == IDOK) {
		pField->SetCurSel (pField->FindStringExact (-1, dlg.m_strField));
		SetDlgItemInt (TXT_ROW, 0); //dlg.m_nRow);
	}
}

void CDatabaseDlg::OnSelectKey() 
{
	CComboBox * pTable = (CComboBox *)GetDlgItem (CB_TABLE);
	CComboBox * pField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CInternalFormatting opt (false);
	CDbFieldDlg dlg (defElements.m_strRegSection, this);
	
	ASSERT (pTable);
	ASSERT (pField);

	GetDlgItemText (TXT_DSN, dlg.m_strDSN);

	if (pTable->GetLBTextLen (pTable->GetCurSel ()) > 0)
		pTable->GetLBText (pTable->GetCurSel (), dlg.m_strTable);

	if (pField->GetLBTextLen (pField->GetCurSel ()) > 0)
		pField->GetLBText (pField->GetCurSel (), dlg.m_strField);

	dlg.m_nRow = 0; //GetDlgItemInt (TXT_ROW);
	dlg.m_nMaxRows = 100;
	GetDlgItemText (TXT_KEYVALUE, dlg.m_strSelected);
	
	if (dlg.DoModal () == IDOK) {
		SetDlgItemText (TXT_KEYVALUE, dlg.m_strSelected);
		pField->SetCurSel (pField->FindStringExact (-1, dlg.m_strField));
		SetDlgItemInt (TXT_ROW, 0); //dlg.m_nRow);
		UpdateKeyField ();
	}
}

void CDatabaseDlg::OnKey() 
{
	bool bUse = CElementDlg::GetCheck (this, CHK_KEY);

	GetDlgItem (CB_KEYFIELD)->EnableWindow (bUse);	
	GetDlgItem (TXT_KEYVALUE)->EnableWindow (bUse);	
	GetDlgItem (BTN_SELECTKEY)->EnableWindow (bUse);	
}

void CDatabaseDlg::OnSelchangeTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CTabCtrl * pTab = (CTabCtrl *)GetDlgItem (IDC_TAB);
	struct
	{
		CWnd *	m_pCtrl;
		int		m_nTab;
	} map [] = 
	{
		{ GetDlgItem (CB_TABLE),		0 },
		{ GetDlgItem (LBL_TABLE),		0 },
		{ GetDlgItem (CB_FIELD),		0 },
		{ GetDlgItem (LBL_FIELD),		0 },
		{ GetDlgItem (BTN_SELECT),		0 },
		{ GetDlgItem (GRP_KEY),			0 },
		{ GetDlgItem (CB_KEYFIELD),		0 },
		{ GetDlgItem (LBL_KEYFIELD),	0 },
		{ GetDlgItem (TXT_KEYVALUE),	0 },
		{ GetDlgItem (LBL_KEYVALUE),	0 },
		{ GetDlgItem (BTN_SELECTKEY),	0 },
		{ GetDlgItem (CHK_KEY),			0 },

		{ GetDlgItem (TXT_SQL),			1 },
		{ GetDlgItem (LBL_SQL),			1 },
	};
	int nCount = sizeof (map) / sizeof (map [0]);

	ASSERT (pTab);

	int nTab = pTab->GetCurSel ();

	for (int i = 0; i < nCount; i++) {
		ASSERT (map [i].m_pCtrl);
		map [i].m_pCtrl->ShowWindow (map [i].m_nTab == nTab ? SW_SHOW : SW_HIDE);
	}

	if (pResult)
		* pResult = 0;
}

void CDatabaseDlg::UpdateKeyField() 
{
	CComboBox * pKeyField = (CComboBox *)GetDlgItem (CB_KEYFIELD);
	CString strKeyField, strKeyValue;

	ASSERT (pKeyField);

	ASSERT (GetDlgItem (TXT_KEYVALUE));
	GetDlgItemText (TXT_KEYVALUE, strKeyValue);

	if (!strKeyValue.GetLength ()) 
		return;

	int nIndex = pKeyField->GetCurSel ();

	if (pKeyField->GetLBTextLen (nIndex) > 0)
		pKeyField->GetLBText (nIndex, strKeyField);

	m_nSQLType = pKeyField->GetItemData (nIndex);
}
