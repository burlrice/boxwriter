// LabelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "LabelDlg.h"
#include "fj_label.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;

/////////////////////////////////////////////////////////////////////////////
// CLabelDlg dialog


CLabelDlg::CLabelDlg(CWnd* pParent /*=NULL*/)
:	m_lUnits (FJ_LABEL_EXP_DAYS),
	FoxjetCommon::CEliteDlg(CLabelDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLabelDlg)
	m_bRound = FALSE;
	m_lValue = 0;
	//}}AFX_DATA_INIT
}


void CLabelDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLabelDlg)
	DDX_Check(pDX, CHK_ROUND, m_bRound);
	DDX_Text(pDX, TXT_VALUE, m_lValue);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLabelDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CLabelDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLabelDlg message handlers

BOOL CLabelDlg::OnInitDialog() 
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_UNITS);
	struct
	{
		ULONG m_lUnit;
		UINT m_nID;
	} static const map [] =
	{
		{ FJ_LABEL_EXP_DAYS,		IDS_DAYS,	},   
		{ FJ_LABEL_EXP_WEEKS,		IDS_WEEKS,	},
		{ FJ_LABEL_EXP_MONTHS,		IDS_MONTHS,	},
		{ FJ_LABEL_EXP_YEARS,		IDS_YEARS,	},
		{ -1,						-1,	},
	};

	ASSERT (pCB);
	
	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();
	
	for (int i = 0; map [i].m_nID != -1; i++) {
		int nIndex = pCB->AddString (LoadString (map [i].m_nID));
		pCB->SetItemData (nIndex, map [i].m_lUnit);

		if (((FJLABELEXP)map [i].m_lUnit) == m_lUnits)
			pCB->SetCurSel (nIndex);
	}

	return bResult;
}

void CLabelDlg::OnOK()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_UNITS);

	ASSERT (pCB);
	int nIndex = pCB->GetCurSel ();

	if (nIndex == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOUNITSSELCTED));
		return;
	}

	m_lUnits = (FJLABELEXP)pCB->GetItemData (nIndex);
	FoxjetCommon::CEliteDlg::OnOK ();
}
