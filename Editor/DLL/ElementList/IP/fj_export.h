#ifndef __FJEXPORT_H__
#define __FJEXPORT_H__

#include "Types.h"
#include "ElementApi.h"
#include "BarCode\barcode.h"
#include "fj_element.h"
#include "fj_system.h"

FORWARD_DECLARE (fjelement, FJELEMENT)

#ifdef FJDLLExport
	#undef	FJDLLExport
	#define FJDLLExport   __declspec(dllimport)
#endif

extern const CHAR fj_FontID[];
extern const CHAR fj_LabelID[];
extern const CHAR fj_BarCodeID[];
extern const CHAR fj_DynBarCodeDefData[];
extern const CHAR fj_BarCodeFont32[];
extern const CHAR fj_BarCodeFont256[];

extern "C" 
{	
// TODO //	FJDLLExport BOOL fj_bmpCheckFormat( LPBYTE pbmp );
}


typedef VOID (* PFROMSTRINGFCT) (LPFJELEMENT pfe, LPCSTR pStr);
typedef PFROMSTRINGFCT LPPFROMSTRINGFCT;  

typedef VOID (* PTOSTRINGFCT) (CLPCFJELEMENT pfe, LPSTR pStr);
typedef PTOSTRINGFCT LPPTOSTRINGFCT;  

LPPFROMSTRINGFCT GetFromStringFct (ELEMENTTYPE type);
LPPTOSTRINGFCT GetToStringFct (ELEMENTTYPE type);

#endif //__FJEXPORT_H__
