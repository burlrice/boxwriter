// UserDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "UserElement.h"
#include "UserDlg.h"
#include "fj_export.h"
#include "Utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetIpElements;

/////////////////////////////////////////////////////////////////////////////
// CUserDlg dialog


CUserDlg::CUserDlg(const CBaseElement & e, const CSize & pa, 
				   const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_nMaxChars (15),
	CTextDlg(e, pa, IDD_USERPROMPTED, pList, pParent)
{
	//{{AFX_DATA_INIT(CUserDlg)
	m_bPrompt = FALSE;
	m_strData = _T("");
	m_strPrompt = _T("");
	//}}AFX_DATA_INIT
}


void CUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CTextDlg::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CUserDlg)
	//}}AFX_DATA_MAP

	DDX_Check(pDX, CHK_PROMPT, m_bPrompt);

	DDX_Text(pDX, TXT_MAXCHARS, m_nMaxChars);
	DDV_MinMaxInt (pDX, m_nMaxChars, CUserElement::m_lmtString.m_dwMin, CUserElement::m_lmtString.m_dwMax);

	DDX_Text(pDX, TXT_TEXT, m_strData);
	DDV_MinMaxChars (pDX, m_strData, TXT_TEXT, CUserElement::m_lmtString.m_dwMin, m_nMaxChars);

	DDX_Text(pDX, TXT_PROMPT, m_strPrompt);
	DDV_MinMaxChars (pDX, m_strPrompt, TXT_PROMPT, CUserElement::m_lmtString); 
	
}

BOOL CUserDlg::OnInitDialog() 
{
	BOOL bResult = CTextDlg::OnInitDialog();

	if (m_strData == CString ('W', m_nMaxChars)) {
		CButton * pDef = (CButton *)GetDlgItem (CHK_DEFAULT);

		ASSERT (pDef);
		pDef->SetCheck (1);
	}

	OnDefaultClick ();
	
	return bResult;
}


BEGIN_MESSAGE_MAP(CUserDlg, CTextDlg)
	//{{AFX_MSG_MAP(CUserDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_BN_CLICKED (CHK_DEFAULT, OnDefaultClick)
	ON_EN_CHANGE (TXT_MAXCHARS, SetDefaultData)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserDlg message handlers

int CUserDlg::BuildElement()
{
	CUserElement & e = GetElement ();
	CString strText, strFont;

	GetDlgItemText (TXT_TEXT, strText);
	GetDlgItemText (CB_FONT, strFont);

	e.SetFontName (strFont);
	e.SetBold (GetDlgItemInt (TXT_BOLD));
	e.SetGap (GetDlgItemInt (TXT_GAP));
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));
	e.SetFlippedH (GetCheck (CHK_FLIPH));
	e.SetFlippedV (GetCheck (CHK_FLIPV));
	e.SetInverse (GetCheck (CHK_INVERSE));
	e.SetMaxChars (GetDlgItemInt (TXT_MAXCHARS));
	e.SetDefaultData (strText);

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

CUserElement & CUserDlg::GetElement ()
{
	const CBaseElement & e = CTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CUserElement)));
	return (CUserElement &)e;
}

const CUserElement & CUserDlg::GetElement () const
{
	const CBaseElement & e = CTextDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CUserElement)));
	return (CUserElement &)e;
}

void CUserDlg::SetDefaultData ()
{
	CButton * pDef = (CButton *)GetDlgItem (CHK_DEFAULT);

	ASSERT (pDef);

	if (pDef->GetCheck () == 1) {
		CString str;

		GetDlgItemText (TXT_MAXCHARS, str);
		SetDlgItemText (TXT_TEXT, CString ('W', _ttol (str)));
	}
}

void CUserDlg::OnDefaultClick ()
{
	CButton * pDef = (CButton *)GetDlgItem (CHK_DEFAULT);
	CWnd * pData = GetDlgItem (TXT_TEXT);

	ASSERT (pDef);
	ASSERT (pData);

	bool bDef = pDef->GetCheck () == 1;

	SetDefaultData ();
	pData->EnableWindow (!bDef);

	if (!bDef)
		SetDlgItemText (TXT_TEXT, m_strData);
}