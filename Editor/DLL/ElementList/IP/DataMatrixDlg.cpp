// DataMatrixDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "resource.h"
#include "DataMatrixDlg.h"
#include "DynamicTextElement.h"
#include "DynamicTableDlg.h"
#include "fj_system.h"
#include "BarCode\iec16022ecc200.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetIpElements;

/////////////////////////////////////////////////////////////////////////////
// CDataMatrixDlg dialog


CDataMatrixDlg::CDataMatrixDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa,
							   const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	CElementDlg(e, pa, CDataMatrixDlg::IDD, pList, pParent)
{
	//{{AFX_DATA_INIT(CDataMatrixDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDataMatrixDlg::DoDataExchange(CDataExchange* pDX)
{
	CComboBox * pDynID = (CComboBox *)GetDlgItem (CB_DYNAMICID);
	long lDynamicID = 0;
	int nIndex;

	ASSERT (pDynID);

	if ((nIndex = pDynID->GetCurSel ()) != CB_ERR) 
		lDynamicID = (long)pDynID->GetItemData (nIndex);

	CElementDlg::DoDataExchange(pDX);

	DDX_Text (pDX, TXT_TEXT, m_strData);
	if (lDynamicID == 0) DDV_MinMaxChars (pDX, m_strData, TXT_TEXT, 1, FJDATAMATRIX_SIZE);

	DDX_Text (pDX, TXT_CX, m_cx);
	DDV_MinMaxInt (pDX, m_cx, CDataMatrixElement::m_lmtCX.m_dwMin, CDataMatrixElement::m_lmtCX.m_dwMax); 

	DDX_Text (pDX, TXT_CY, m_cy);
	DDV_MinMaxInt (pDX, m_cy, CDataMatrixElement::m_lmtCY.m_dwMin, CDataMatrixElement::m_lmtCY.m_dwMax); 

	DDX_Text (pDX, TXT_FIRSTCHAR, m_lFirstChar);
	if (lDynamicID > 0) DDV_MinMaxInt (pDX, m_lFirstChar, CDynamicTextElement::m_lmtLen.m_dwMin, CDynamicTextElement::m_lmtLen.m_dwMax); 

	DDX_Text (pDX, TXT_NUMBEROFCHARS, m_lNumChar);
	if (lDynamicID > 0) DDV_MinMaxInt (pDX, m_lNumChar, CDynamicTextElement::m_lmtLen.m_dwMin, CDynamicTextElement::m_lmtLen.m_dwMax + 1); 

	if (pDX->m_bSaveAndValidate) {
		char * pEncoding = NULL;
		char strText [FJDATAMATRIX_SIZE+1] = { 0 };
		int nLen = 0, nLenP = 0, nMaxLen = 0, nEccLen = 0;
		int cx = m_cx, cy = m_cy;

		strncpy (strText, w2a (m_strData), FJDATAMATRIX_SIZE);
		nLen = strlen (strText);
		UCHAR * pGrid = iec16022ecc200 (&cx, &cy, &pEncoding, nLen, (PUCHAR)strText, 
			&nLenP, &nMaxLen, &nEccLen);

		if (!pGrid) {
			MsgBox (* this, LoadString (IDS_DATAMATRIXFAILED), NULL, MB_ICONINFORMATION);
			pDX->Fail ();
		}
	}
}


BEGIN_MESSAGE_MAP(CDataMatrixDlg, CElementDlg)
	//{{AFX_MSG_MAP(CDataMatrixDlg)
	ON_BN_CLICKED(BTN_SELECT, OnSelect)
	ON_CBN_SELCHANGE(CB_DYNAMICID, OnSelchangeDynamicid)
	ON_CBN_SELCHANGE(CB_TYPE, OnSelchangeType)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(TXT_TEXT, InvalidatePreview)
	ON_EN_CHANGE(TXT_CX, InvalidatePreview)
	ON_EN_CHANGE(TXT_CY, InvalidatePreview)
	ON_EN_CHANGE(TXT_FIRSTCHAR, InvalidatePreview)
	ON_EN_CHANGE(TXT_NUMBEROFCHARS, InvalidatePreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDataMatrixDlg message handlers

////////////////////////////////////////////////////////////////////////////////

CDataMatrixElement & CDataMatrixDlg::GetElement ()
{
	CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CDataMatrixElement)));
	return (CDataMatrixElement &)e;
}

const CDataMatrixElement & CDataMatrixDlg::GetElement () const
{
	const CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CDataMatrixElement)));
	return (CDataMatrixElement &)e;
}

int CDataMatrixDlg::BuildElement ()
{
	CDataMatrixElement & e = GetElement ();
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	CComboBox * pDynID = (CComboBox *)GetDlgItem (CB_DYNAMICID);
	int nType = 0;
	long lDynamicID = 0;
	int nIndex;
	int nResult = -1;

	ASSERT (pType);
	ASSERT (pDynID);

	if ((nIndex = pType->GetCurSel ()) != CB_ERR) 
		nType = (int)pType->GetItemData (nIndex);

	if ((nIndex = pDynID->GetCurSel ()) != CB_ERR) 
		lDynamicID = (long)pDynID->GetItemData (nIndex);

	e.SetFlippedH (GetCheck (CHK_FLIPH));
	e.SetFlippedV (GetCheck (CHK_FLIPV));
	e.SetInverse (GetCheck (CHK_INVERSE));

	e.SetDefaultData (GetCtrlText (TXT_TEXT));
	e.SetCX (_ttol (GetCtrlText (TXT_CX)));
	e.SetCY (_ttol (GetCtrlText (TXT_CY)));
	e.SetBcType (nType);
	e.SetDynamicID (lDynamicID);
	e.SetFirstChar (_ttol (GetCtrlText (TXT_FIRSTCHAR)));
	e.SetNumChar (_ttol (GetCtrlText (TXT_NUMBEROFCHARS)));

	nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

////////////////////////////////////////////////////////////////////////////////

void CDataMatrixDlg::OnOK()
{
	CComboBox * pDynID = (CComboBox *)GetDlgItem (CB_DYNAMICID);
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);

	ASSERT (pDynID);
	ASSERT (pType);

	m_lDynamicID = pDynID->GetItemData (pDynID->GetCurSel ());
	m_nType = pType->GetItemData (pType->GetCurSel ());

	CElementDlg::OnOK ();
}

void CDataMatrixDlg::OnSelect() 
{
	CDynamicTableDlg dlg;
	CComboBox * pDynID = (CComboBox *)GetDlgItem (CB_DYNAMICID);

	ASSERT (pDynID);

	dlg.m_lLineID = GetLineID ();
	dlg.m_lSelectedID = pDynID->GetItemData (pDynID->GetCurSel ());

	if (dlg.DoModal () == IDOK) {
		for (int i = 0; i < pDynID->GetCount (); i++) {
			if (dlg.m_lSelectedID == (LONG)pDynID->GetItemData (i)) {
				pDynID->SetCurSel (i);
				break;
			}
		}
	}

	OnSelchangeDynamicid ();
	InvalidatePreview ();
}

void CDataMatrixDlg::OnSelchangeDynamicid() 
{
	CComboBox * pDynID = (CComboBox *)GetDlgItem (CB_DYNAMICID);
	CString strData = m_strData;

	ASSERT (pDynID);

	long lDynamicID = pDynID->GetItemData (pDynID->GetCurSel ());

	if (CWnd * p = GetDlgItem (BTN_SELECT))
		p->EnableWindow (lDynamicID != 0);
	if (CWnd * p = GetDlgItem (TXT_FIRSTCHAR))
		p->EnableWindow (lDynamicID != 0);
	if (CWnd * p = GetDlgItem (TXT_NUMBEROFCHARS))
		p->EnableWindow (lDynamicID != 0);

	if (lDynamicID != 0) {
		CItemArray vItems;

		CDynamicTableDlg::GetDynamicData (GetLineID (), vItems);

		if (vItems.GetSize () > lDynamicID)
			strData = vItems [lDynamicID - 1].m_strData;
	}

	if (CWnd * p = GetDlgItem (TXT_TEXT)) {
		p->EnableWindow (lDynamicID == 0);
		p->SetWindowText (strData);
	}
}

void CDataMatrixDlg::OnSelchangeType() 
{
/*
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);

	ASSERT (pType);
	
	int nIndex = (int)pType->GetItemData (nIndex = pType->GetCurSel ());
*/
	InvalidatePreview ();
}

BOOL CDataMatrixDlg::OnInitDialog() 
{
	CComboBox * pDynID = (CComboBox *)GetDlgItem (CB_DYNAMICID);
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);

	ASSERT (pDynID);
	ASSERT (pType);

	CElementDlg::OnInitDialog();
	
	for (int i = (int)CDynamicTextElement::m_lmtID.m_dwMin - 1; i <= (int)CDynamicTextElement::m_lmtID.m_dwMax; i++) {
		CString str;

		if (i == 0)
			str = LoadString (IDS_NOTUSED);
		else
			str.Format (_T ("%d"), i);

		int nIndex = pDynID->AddString (str);
		pDynID->SetItemData (nIndex, i);

		if (i == m_lDynamicID)
			pDynID->SetCurSel (nIndex);
	}

	for (int i = 0; i < FJSYS_BARCODES; i++) {
		const FJSYSDATAMATRIX & bc = GetElement ().GetSystem ()->bcDataMatrixArray [i];
		int nIndex = pType->AddString (a2w (bc.strName));
		pType->SetItemData (nIndex, i);

		if (m_nType == i)
			pType->SetCurSel (i);
	}

	if (pDynID->GetCurSel () == CB_ERR)
		pDynID->SetCurSel (0);
	
	if (pType->GetCurSel () == CB_ERR)
		pType->SetCurSel (0);

	OnSelchangeDynamicid ();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
