// SystemBarcodeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "SystemBarcodeDlg.h"
#include "fj_system.h"
#include "fj_barcode.h"
#include "BarCode\barcode.h"
#include "BarcodeParamDlg.h"
#include "BarcodeElement.h"
#include "Extern.h"
#include "Database.h"
#include "Debug.h"
#include "TemplExt.h"
#include "Registry.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetIpElements;
using namespace ListGlobals;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////

using namespace FoxjetIpElements;

class CSystem
{
public:
	CSystem (const LPFJSYSTEM pSystem = NULL, const CString & strType = _T (""));
	CSystem (const CSystem & rhs);
	CSystem & operator = (const CSystem & rhs);
	virtual ~CSystem ();

	static bool CompareTypes (const LPVOID pLhs, const LPVOID pRhs);

	LPFJSYSTEM					m_pSystem;
	CString						m_strType;
	FoxjetDatabase::LINESTRUCT	m_line;
};

typedef CArray <CSystem *, CSystem *> CSystemArray;
static CArray <CSystemArray *, CSystemArray *> vSystems;

/////////////////////////////////////////////////////////////////////////////////////////

CSystemBarcodeDlg::CBarcodeItem::CBarcodeItem (LPFJSYSBARCODE pBC, int nIndex)
{
	ASSERT (pBC);

	m_strName		= pBC->strName;
	m_lBCFlags		= pBC->lBCFlags;
	m_lBCHeight		= pBC->lBCHeight;
	m_bCheckDigit	= pBC->bCheckDigit ? true : false;
	m_lHor_Bearer	= pBC->lHor_Bearer;
	m_lVer_Bearer	= pBC->lVer_Bearer;
	m_lQuietZone	= pBC->lQuietZone;
	m_nIndex		= nIndex;

	for (int i = 0; i < 5; i++)
		m_lBarVal [i]	= pBC->lBarVal [i];
}

CSystemBarcodeDlg::CBarcodeItem::CBarcodeItem (int nType, int nIndex, int nHeight, bool bChecksum,
							int nHBearer, int nVBearer, int nQuietzone, 
							int nNarrowbar, int nBleed1, int nBleed2, int nBleed3, int nBleed4)
{
//	m_strName		= 
	m_lBCFlags		= nType;
	m_lBCHeight		= nHeight;
	m_bCheckDigit	= bChecksum;
	m_lHor_Bearer	= nHBearer;
	m_lVer_Bearer	= nVBearer;
	m_lQuietZone	= nQuietzone;
	m_nIndex		= nIndex;

	m_lBarVal [0]	= nNarrowbar;
	m_lBarVal [1]	= nBleed1;
	m_lBarVal [2]	= nBleed2;
	m_lBarVal [3]	= nBleed3;
	m_lBarVal [4]	= nBleed4;
}

CSystemBarcodeDlg::CBarcodeItem::~CBarcodeItem ()
{
}

int CSystemBarcodeDlg::CBarcodeItem::Compare (const ItiLibrary::ListCtrlImp::CItem & cmp, int nColumn) const
{
	const CBarcodeItem & rhs = (CBarcodeItem &)cmp;

	switch (nColumn) {
	case 0:		return m_lBCFlags		- rhs.m_lBCFlags;
	case 1:		return m_strName.CompareNoCase (rhs.m_strName);
	case 2:		return m_lBCHeight		- rhs.m_lBCHeight;
	case 3:		return m_bCheckDigit	- rhs.m_bCheckDigit;
	case 4:		return m_lHor_Bearer	- rhs.m_lHor_Bearer;
	case 5:		return m_lVer_Bearer	- rhs.m_lVer_Bearer;
	case 6:		return m_lQuietZone		- rhs.m_lQuietZone;
	case 7:		return m_lBarVal [0]	- rhs.m_lBarVal [0];
	case 8:		return m_lBarVal [1]	- rhs.m_lBarVal [1];
	case 9:		return m_lBarVal [2]	- rhs.m_lBarVal [2];
	case 10:	return m_lBarVal [3]	- rhs.m_lBarVal [3];
	case 11:	return m_lBarVal [4]	- rhs.m_lBarVal [4];
	}

	return 0;
}

CString CSystemBarcodeDlg::CBarcodeItem::GetDispText (int nColumn) const
{
	CString str;
	struct 
	{
		int m_nResID;
		int m_nType;
	} static const map [] =
	{
		{ IDS_BARCODE_ANY,		BARCODE_ANY,	},
		{ IDS_BARCODE_EAN,		BARCODE_EAN,	},
		{ IDS_BARCODE_UPC,		BARCODE_UPC,   	},
		{ IDS_BARCODE_ISBN,		BARCODE_ISBN,  	},
		{ IDS_BARCODE_39,		BARCODE_39,    	},
		{ IDS_BARCODE_128,		BARCODE_128,   	},
		{ IDS_BARCODE_128C,		BARCODE_128C,  	},
		{ IDS_BARCODE_128B,		BARCODE_128B,  	},
		{ IDS_BARCODE_I25,		BARCODE_I25,   	},
		{ IDS_BARCODE_128RAW,	BARCODE_128RAW,	},
		{ IDS_BARCODE_CBR,		BARCODE_CBR,   	},
		{ IDS_BARCODE_MSI,		BARCODE_MSI,   	},
		{ IDS_BARCODE_PLS,		BARCODE_PLS,    },		
		{ IDS_BARCODE_EAN128,	BARCODE_EAN128,	}, // __SW0842__
	};

	switch (nColumn) {
	case 0:
		{
			for (int i = 0; i < sizeof (map) / sizeof (map [i]); i++) {
				if ((m_lBCFlags & BARCODE_ENCODING_MASK) == map [i].m_nType) {
					str = LoadString (map [i].m_nResID);
					break;
				}
			}
		}
		break;
	case 1:		return m_strName;
	case 2:		str.Format (_T ("%d"), m_lBCHeight);		break;
	case 3:		return LoadString (m_bCheckDigit ? IDS_TRUE : IDS_FALSE);
	case 4:		str.Format (_T ("%d"), m_lHor_Bearer);		break;
	case 5:		str.Format (_T ("%d"), m_lVer_Bearer);		break;
	case 6:		str.Format (_T ("%d"), m_lQuietZone);		break;
	case 7:		str.Format (_T ("%d"), m_lBarVal [0]);		break;
	case 8:		str.Format (_T ("%d"), m_lBarVal [1]);		break;
	case 9:		str.Format (_T ("%d"), m_lBarVal [2]);		break;
	case 10:	str.Format (_T ("%d"), m_lBarVal [3]);		break;
	case 11:	str.Format (_T ("%d"), m_lBarVal [4]);		break;
	}

	return str;
}

/////////////////////////////////////////////////////////////////////////////

CSystem::CSystem (const LPFJSYSTEM pSystem, const CString & strType)
:	m_pSystem (pSystem),
	m_strType (strType)
{
}

CSystem::CSystem (const CSystem & rhs)
:	m_pSystem (rhs.m_pSystem),
	m_strType (rhs.m_strType),
	m_line (rhs.m_line)
{
}

CSystem & CSystem::operator = (const CSystem & rhs)
{
	if (this != &rhs) {
		m_pSystem	= rhs.m_pSystem;
		m_strType	= rhs.m_strType;
		m_line		= rhs.m_line;
	}

	return * this;
}

CSystem::~CSystem ()
{
}

bool CSystem::CompareTypes (const LPVOID pLhs, const LPVOID pRhs)
{
	CSystem * pSysLhs = (CSystem *)pLhs;
	CSystem * pSysRhs = (CSystem *)pRhs;

	if (pSysLhs && pSysRhs) 
		return pSysLhs->m_strType.CompareNoCase (pSysRhs->m_strType) == 0;

	return false;
}

/////////////////////////////////////////////////////////////////////////////
// CSystemBarcodeDlg dialog

CSystemBarcodeDlg::CSystemBarcodeDlg(CWnd* pParent /*=NULL*/)
:	
	m_nHeadType	(HEADTYPE_FIRST),
	m_lLineID (-1),
	m_bUpdated (false),
	m_nType (-1),
	FoxjetCommon::CEliteDlg(CSystemBarcodeDlg::IDD, pParent)
{
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CArray <HEADTYPE, HEADTYPE> vTypes;
	CStringArray vTypeNames;
	COdbcDatabase & db = GetDB ();

	GetLineRecords (vLines);

	// some types may share settings.  figure out how many don't
	for (int nType = HEADTYPE_FIRST; nType <= HEADTYPE_LAST; nType++) {
		CString str = CBarcodeElement::GetHeadTypeString ((HEADTYPE)nType);

		if (Find (vTypeNames, str, false) == -1) {
			TRACEF (str);
			vTypeNames.Add (str);
			vTypes.Add ((HEADTYPE)nType);
		}
	}

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		const LINESTRUCT line = vLines [nLine];
		const ULONG lLineID = line.m_lID;
		CSystemArray * pSysArray = new CSystemArray ();

		for (int i = 0; i < vTypes.GetSize (); i++) {
			const HEADTYPE type = vTypes [i];
			const CString strType = CBarcodeElement::GetHeadTypeString (type);
			CString str = CBarcodeElement::GetBarcodeSettings (db, lLineID, type);
			LPFJSYSTEM pSystem = fj_SystemNew ();

			VERIFY (fj_SystemFromString (pSystem, w2a (str)));
			sprintf (pSystem->strID, "%d", line.m_lID);
			TRACEF (_T ("Creating temp FJSYSTEM: ") + line.m_strName + _T (", ") + strType);

			CSystem * p = new CSystem (pSystem, strType);
			p->m_line = line;
			pSysArray->Add (p);
		}

		::vSystems.Add (pSysArray);
	}
}

CSystemBarcodeDlg::~CSystemBarcodeDlg ()
{
	for (int nLine = 0; nLine < ::vSystems.GetSize (); nLine++) {
		if (CSystemArray * pSysArray = ::vSystems [nLine]) {
			for (int i = 0; i < pSysArray->GetSize (); i++) {
				if (CSystem * p = pSysArray->GetAt (i)) {
					TRACEF ("Destroying temp FJSYSTEM: " + p->m_line.m_strName + ", " + p->m_strType);
					fj_SystemDestroy (p->m_pSystem);
					delete p;
				}
			}

			pSysArray->RemoveAll ();
			delete pSysArray;
		}
	}

	::vSystems.RemoveAll ();
}

void CSystemBarcodeDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSystemBarcodeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSystemBarcodeDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CSystemBarcodeDlg)
	ON_CBN_SELCHANGE(CB_SYSTEM, OnSelchangeSystem)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_CBN_SELCHANGE(CB_TYPE, OnSelchangeSystem)
	ON_BN_CLICKED(BTN_LOAD, OnLoad)
	//}}AFX_MSG_MAP
	ON_NOTIFY(NM_DBLCLK, LV_BARCODE, OnDblclickBarcode)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSystemBarcodeDlg message handlers

BOOL CSystemBarcodeDlg::OnInitDialog() 
{
	using namespace ItiLibrary;
	using namespace FoxjetDatabase;

	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_SYSTEM);	
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);	
	COdbcDatabase & db = GetDB ();
	
	ASSERT (pLine);
	ASSERT (pType);
	
	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();

	for (int nLine = 0; nLine < ::vSystems.GetSize (); nLine++) {
		if (CSystemArray * pSysArray = ::vSystems [nLine]) {
			if (pSysArray->GetSize ()) {
				if (CSystem * p = pSysArray->GetAt (0)) {
					int nIndex = pLine->AddString (p->m_line.m_strName);
					pLine->SetItemData (nIndex, nLine);

					if (p->m_line.m_lID == m_lLineID)
						pLine->SetCurSel (nIndex);
				}
			}
		}
	}

	for (int nType = HEADTYPE_FIRST; nType <= HEADTYPE_LAST; nType++) {
		CString str = CBarcodeElement::GetHeadTypeString ((HEADTYPE)nType);

		if (pType->FindString (-1, str) == CB_ERR) {
			int nIndex = pType->AddString (str);

			pType->SetItemData (nIndex, nType);

			if ((HEADTYPE)nType == m_nHeadType)
				pType->SetCurSel (nIndex);
		}
	}

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_BCFLAGS),		100));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_NAME),		65));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_HEIGHT),		40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_CHECKDIGIT),	40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_HORZBEARER),	40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_VERTBEARER),	40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_QUIETZONE),	40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_BARVAL0),		40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_BARVAL1),		40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_BARVAL2),		40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_BARVAL3),		40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_BARVAL4),		40));

	m_lv.Create (LV_BARCODE, vCols, this, defElements.m_strRegSection);	

	if (pLine->GetCurSel () == CB_ERR && pLine->GetCount ())
		pLine->SetCurSel (0);
	
	if (pType->GetCurSel () == CB_ERR && pType->GetCount ())
		pType->SetCurSel (0);

	OnSelchangeSystem ();

	if (m_nType != -1) {
		m_lv->SetItemState (m_nType, LVIS_SELECTED | LVIS_FOCUSED, 0xFFFF);
		m_lv->EnsureVisible (m_nType, FALSE);
	}

	return bResult;
}

void CSystemBarcodeDlg::OnSelchangeSystem() 
{
	m_lv.DeleteCtrlData ();

	if (LPFJSYSTEM pSystem = GetCurSystem ()) {
		m_lLineID = strtoul (pSystem->strID, NULL, 10);
		m_nHeadType = GetCurSelHeadType ();

		for (int i = 0; i < FJSYS_BARCODES; i++) 
			m_lv.InsertCtrlData (new CBarcodeItem (&(pSystem->bcArray [i]), i));
	}

	OnSelchangeheadtype ();
}

void CSystemBarcodeDlg::OnDblclickBarcode(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();

	if (pResult)
		* pResult = 0;
}

void CSystemBarcodeDlg::OnEdit() 
{
	CBarcodeParamDlg dlg (this);
	LPFJSYSTEM pSystem = GetCurSystem ();
	HEADTYPE type = GetCurSelHeadType ();
	CLongArray sel = ItiLibrary::GetSelectedItems (m_lv.GetListCtrl ());

	if (!pSystem) {
		MsgBox (* this, LoadString (IDS_NOSYSTEMSELECTED));
		return;
	}

	if (sel.GetSize ()) {
		CBarcodeItem * p = (CBarcodeItem *)m_lv.GetCtrlData (sel [0]);

		ASSERT (p);
		int nBarcode = p->m_nIndex;

		dlg.m_strName			= p->m_strName;
		dlg.m_lBCFlags			= p->m_lBCFlags;
		dlg.m_lVertBearer		= p->m_lVer_Bearer;
		dlg.m_lHorzBearer		= p->m_lHor_Bearer;
		dlg.m_nHeight			= p->m_lBCHeight;
		dlg.m_lQuietZone		= p->m_lQuietZone;
		dlg.m_bChecksum			= p->m_bCheckDigit;

		for (int i = 0; i < 5; i++)
			dlg.m_lBarVal [i]	= p->m_lBarVal [i];

		if (dlg.DoModal () == IDOK) {
			char sz [1024] = { 0 };
			ULONG lLineID = atol (pSystem->strID);
			SETTINGSSTRUCT s;

			CBarcodeElement::SetName				(pSystem, nBarcode,	dlg.m_strName);
			CBarcodeElement::SetVerticalBearer		(pSystem, nBarcode,	dlg.m_lVertBearer);
			CBarcodeElement::SetHorizontalBearer	(pSystem, nBarcode,	dlg.m_lHorzBearer);
			CBarcodeElement::SetHeight				(pSystem, nBarcode,	dlg.m_nHeight);
			CBarcodeElement::SetBCFlags				(pSystem, nBarcode,	dlg.m_lBCFlags);
			CBarcodeElement::SetQuietZone			(pSystem, nBarcode,	dlg.m_lQuietZone);
			CBarcodeElement::SetChecksum			(pSystem, nBarcode,	dlg.m_bChecksum ? true : false);

			p->m_strName		= dlg.m_strName;
			p->m_lBCHeight		= dlg.m_nHeight;
			p->m_lBCFlags		= dlg.m_lBCFlags;
			p->m_bCheckDigit	= dlg.m_bChecksum ? true : false;
			p->m_lHor_Bearer	= dlg.m_lHorzBearer;
			p->m_lVer_Bearer	= dlg.m_lVertBearer;
			p->m_lQuietZone		= dlg.m_lQuietZone;

			for (int i = 0; i < 5; i++) {
				CBarcodeElement::SetBarValue (pSystem, nBarcode, i, dlg.m_lBarVal [i]);
				p->m_lBarVal [i] = dlg.m_lBarVal [i];
			}

			fj_SystemBarCodesToString (pSystem, sz);
			s.m_lLineID = lLineID;
			s.m_strKey	= ListGlobals::Keys::m_strBarcode;
			s.m_strData = a2w (sz);

			VERIFY (CBarcodeElement::SetBarcodeSettings (GetDB (), lLineID, type, a2w (sz)));

			m_lv.UpdateCtrlData (p);
			m_bUpdated = true;
		}
	}
	else 
		MsgBox (* this, LoadString (IDS_NOBARCODESELECTED));
}

LPFJSYSTEM CSystemBarcodeDlg::GetCurSystem () const
{
	int nLine = GetCurSelLineIndex ();
	HEADTYPE nType = GetCurSelHeadType ();
	CString strType = CBarcodeElement::GetHeadTypeString (nType);

	if (nLine >= 0 && nLine < ::vSystems.GetSize ()) {
		if (CSystemArray * pSysArray = ::vSystems [nLine]) {
			for (int i = 0; i < pSysArray->GetSize (); i++) {
				if (CSystem * p = pSysArray->GetAt (i)) {
					if (!p->m_strType.CompareNoCase (strType)) {
						return p->m_pSystem;
					}
				}
			}
		}
	}

	return NULL;
}

int CSystemBarcodeDlg::GetCurSelLineIndex () const
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_SYSTEM);	
	
	ASSERT (pCB);

	int nIndex = pCB->GetCurSel ();

	if (nIndex != CB_ERR) 
		return pCB->GetItemData (nIndex);

	return -1;
}


HEADTYPE CSystemBarcodeDlg::GetCurSelHeadType() const
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_TYPE);	
	
	ASSERT (pCB);

	int nIndex = pCB->GetCurSel ();

	if (nIndex != CB_ERR) 
		return (HEADTYPE)pCB->GetItemData (nIndex);

	return HEADTYPE_FIRST;
}

void CSystemBarcodeDlg::OnCancel()
{
	FoxjetCommon::CEliteDlg::OnCancel ();
}

void CSystemBarcodeDlg::OnSelchangeheadtype () 
{
	HEADTYPE type = GetCurSelHeadType ();
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_DEFS);
	const CString strSection = defElements.m_strRegSection + _T ("\\CSystemBarcodeDlg");
	int nLastDef = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("nDef"), 0);
	struct 
	{
		HEADTYPE	m_type;
		UINT		m_nID;
	} static const map [] = 
	{
		{ HEADTYPE_FIRST,	IDS_DEFBCPARAM1 },
		{ GRAPHICS_768_256, IDS_DEFBCPARAM2 },
		{ GRAPHICS_768_256, IDS_DEFBCPARAM3 },
		{ GRAPHICS_768_256, IDS_DEFBCPARAM4 },
		{ GRAPHICS_768_256, IDS_DEFBCPARAM5 },
	};

	ASSERT (pCB);

	pCB->ResetContent ();

	for (int i = 0; i < (sizeof (map) / sizeof (map [0])); i++) {
		if (map [i].m_type == type) {
			int nIndex = pCB->AddString (LoadString (map [i].m_nID));
			pCB->SetItemData (nIndex, i);

			if (i == nLastDef)
				pCB->SetCurSel (nIndex);
		}
	}

	if (pCB->GetCurSel () == CB_ERR)
		pCB->SetCurSel (0);
}

void CSystemBarcodeDlg::AssignDefs (CBarcodeItem * pvDefs, int nParams, HEADTYPE type, bool bSetName) 
{
	ASSERT (pvDefs);

	LPFJSYSTEM pSystem = GetCurSystem ();
	char sz [1024] = { 0 };
	ULONG lLineID = atol (pSystem->strID);
	SETTINGSSTRUCT s;

	for (int i = 0; i < nParams; i++) {
		CBarcodeItem * p = (CBarcodeItem *)m_lv.GetCtrlData (i);
		const CString strName = p->m_strName;

		* p = pvDefs [i];
		
		if (bSetName) {
//			p->m_strName = strName;
			p->m_strName = CBarcodeElement::GetMagName (i);
		}

		m_lv.UpdateCtrlData (p);

		CBarcodeElement::SetName				(pSystem, p->m_nIndex,	p->m_strName);
		CBarcodeElement::SetVerticalBearer		(pSystem, p->m_nIndex,	p->m_lVer_Bearer);
		CBarcodeElement::SetHorizontalBearer	(pSystem, p->m_nIndex,	p->m_lHor_Bearer);
		CBarcodeElement::SetHeight				(pSystem, p->m_nIndex,	p->m_lBCHeight);
		CBarcodeElement::SetBCFlags				(pSystem, p->m_nIndex,	p->m_lBCFlags);
		CBarcodeElement::SetQuietZone			(pSystem, p->m_nIndex,	p->m_lQuietZone);
		CBarcodeElement::SetChecksum			(pSystem, p->m_nIndex,	p->m_bCheckDigit);

		for (int i = 0; i < 5; i++) 
			CBarcodeElement::SetBarValue (pSystem, p->m_nIndex, i, p->m_lBarVal [i]);

		m_lv.UpdateCtrlData (p);
	}

	fj_SystemBarCodesToString (pSystem, sz);
	s.m_lLineID = lLineID;
	s.m_strKey	= ListGlobals::Keys::m_strBarcode;
	s.m_strData = a2w (sz);

	VERIFY (CBarcodeElement::SetBarcodeSettings (GetDB (), lLineID, type, a2w (sz)));
	m_bUpdated = true;
}

void CSystemBarcodeDlg::OnLoad() 
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_DEFS);
	HEADTYPE type = GetCurSelHeadType ();
	const CString strSection = defElements.m_strRegSection + _T ("\\CSystemBarcodeDlg");

	ASSERT (pCB);
	int nDefs = pCB->GetItemData (pCB->GetCurSel ());

	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("nDef"), nDefs);

	int nResult = MsgBox (* this, LoadString (IDS_CONFIRMOVERWRITEPARAMS), 
		LoadString (IDS_CONFIRM), MB_YESNO);

	if (nResult == IDNO)
		return;

	switch (nDefs) {
	case 0: // 352 Print Head at 150 dpi (V300 Ink):
		{
			CBarcodeItem defs [] = 
			{
				CBarcodeItem (BARCODE_I25,	0, 32,	1,	2,	15,	90,	37,	1,	4,	0,	0),
				CBarcodeItem (BARCODE_I25,	1, 32,	1,	2,	15,	85,	34,	1,	4,	0,	0),
				CBarcodeItem (BARCODE_I25,	2, 32,	1,	2,	15,	78,	30,	1,	4,	0,	0),
				CBarcodeItem (BARCODE_I25,	3, 32,	1,	2,	15,	75,	26,	1,	4,	0,	0),
				CBarcodeItem (BARCODE_I25,	4, 32,	1,	2,	15,	50,	24,	1,	4,	0,	0),
			};

			AssignDefs (defs, ARRAYSIZE (defs), type);
		}
		break;
	case 1: // 768 Print Head at 200 dpi (ScanTrue II Ink):
		{
			CBarcodeItem defs [] = 
			{
				CBarcodeItem (BARCODE_I25,	0,	124,	1,	10,	30,	120,	37,	0,	0,	0,	0),
				CBarcodeItem (BARCODE_I25,	1,	124,	1,	10,	30,	115,	33,	0,	0,	0,	0),
				CBarcodeItem (BARCODE_I25,	2,	124,	1,	10,	20,	110,	30,	0,	0,	0,	0),
				CBarcodeItem (BARCODE_I25,	3,	124,	1,	10,	20,	100,	25,	0,	0,	0,	0),
				CBarcodeItem (BARCODE_I25,	4,	124,	1,	10,	20,	80,		22,	0,	0,	0,	0),
			};

			AssignDefs (defs, ARRAYSIZE (defs), type);
		}
		break;
	case 2: // 768 Print Head at 300 dpi (ScanTrue II Ink)
		{
			CBarcodeItem defs [] = 
			{
				CBarcodeItem (BARCODE_I25,	0,	124,	1,	12,	40,	140,	40,	1,	1,	2,	1),
				CBarcodeItem (BARCODE_I25,	1,	124,	1,	12,	30,	130,	36,	1,	1,	2,	1),
				CBarcodeItem (BARCODE_I25,	2,	124,	1,	12,	20,	120,	32,	1,	1,	2,	1),
				CBarcodeItem (BARCODE_I25,	3,	124,	1,	12,	20,	115,	28,	1,	1,	2,	1),
				CBarcodeItem (BARCODE_I25,	4,	124,	1,	12,	20,	80,		25,	1,	1,	2,	1),
			};
			AssignDefs (defs, ARRAYSIZE (defs), type);
		}
		break;
	case 3: // Code 128 at 300 dpi
		{
			CBarcodeItem defs [] = 
			{
				CBarcodeItem (BARCODE_128,	0,	100,	1,	0,	0,	140,	40,	1,	1,	1,	1),
				CBarcodeItem (BARCODE_128,	1,	100,	1,	0,	0,	120,	32,	1,	1,	1,	1),
				CBarcodeItem (BARCODE_128,	2,	100,	1,	0,	0,	80,		24,	1,	1,	0,	0),
				CBarcodeItem (BARCODE_128,	3,	100,	1,	0,	0,	75,		20,	1,	1,	0,	0),
			};

			AssignDefs (defs, ARRAYSIZE (defs), type);
		}
		break;
	case 4: // UPC 
		{
			CBarcodeItem defs [] = 
			{
				CBarcodeItem (BARCODE_UPC,	0,	124,	0,	0,	0,	0,		20,	1,	2,	1,	1),
				CBarcodeItem (BARCODE_UPC,	1,	124,	0,	0,	0,	0,		21,	1,	2,	1,	1),
				CBarcodeItem (BARCODE_UPC,	2,	124,	0,	0,	0,	0,		22,	1,	2,	1,	1),
				CBarcodeItem (BARCODE_UPC,	3,	124,	0,	0,	0,	0,		23,	1,	2,	1,	1),
				CBarcodeItem (BARCODE_UPC,	4,	124,	0,	0,	0,	0,		24,	1,	2,	1,	1),
				CBarcodeItem (BARCODE_UPC,	5,	124,	0,	0,	0,	0,		25,	1,	2,	1,	1),
				CBarcodeItem (BARCODE_UPC,	6,	124,	0,	0,	0,	0,		26,	1,	2,	1,	1),
			};
			UINT nMag [] = 
			{
				154,
				162,
				170,
				177,
				185,
				192,
				200,
			};

			ASSERT (ARRAYSIZE (defs) == ARRAYSIZE (nMag));

			for (int i = 0; i < ARRAYSIZE (defs); i++) 
				defs [i].m_strName.Format (_T ("%d %%"), nMag [i]);

			AssignDefs (defs, ARRAYSIZE (defs), type, false);
		}
		break;
	}
}
