// PreviewCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "PreviewCtrl.h"
#include "IpBaseElement.h"
#include "Color.h"
#include "Resource.h"
#include "Debug.h"
#include "TemplExt.h"
#include "AppVer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Color;
using namespace FoxjetCommon;
using namespace FoxjetIpElements;

/////////////////////////////////////////////////////////////////////////////
// CPreviewCtrl

CPreviewCtrl::CPreviewCtrl()
:	m_pElement(NULL),
	m_pHead (NULL),
	m_sizeElement (0, 0),
	m_dZoom (1.0),
	m_pBmp (NULL)
{
	m_nMapMode = MM_TEXT;
}

CPreviewCtrl::~CPreviewCtrl()
{
	Free ();
}


BEGIN_MESSAGE_MAP(CPreviewCtrl, CScrollView)
	//{{AFX_MSG_MAP(CPreviewCtrl)
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEACTIVATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CPreviewCtrl message handlers

double CPreviewCtrl::CalcFitZoom ()
{
	CRect rc;
	double dStretch [2] = { 1.0, 1.0 };

	ASSERT (m_pHead);
	
	if (CIpBaseElement * p = DYNAMIC_DOWNCAST (CIpBaseElement, m_pElement)) {
		p->CalcStretch (* m_pHead, dStretch);
	}

	GetClientRect (rc);

	double dZoom [2] = 
	{
		(double)rc.Width () / ((double)m_sizeElement.cx * dStretch [0]),
		(double)rc.Height () / ((double)m_sizeElement.cy * dStretch [1]),
	};

	return min (dZoom [0], dZoom [1]);
}

bool CPreviewCtrl::SetZoom (double dZoom)
{
	if (dZoom != m_dZoom) {
		m_dZoom = BindTo (dZoom, 0.01, 20.0);
		Invalidate ();
		RedrawWindow ();
	}

	return true;
}

void CPreviewCtrl::Free ()
{
	if (m_pBmp) {
		delete m_pBmp;
		m_pBmp = NULL;
	}
}

void CPreviewCtrl::Invalidate (BOOL bErase)
{
	Free ();

	CScrollView::Invalidate (bErase);
}

void CPreviewCtrl::OnDraw (CDC* pDC)
{
	if (CIpBaseElement * p = DYNAMIC_DOWNCAST (CIpBaseElement, m_pElement)) {
		CRect rc;

		GetClientRect (rc);
		pDC->FillSolidRect (rc, Color::rgbDarkGray);

		if (!IsWindowEnabled ()) 
			SetScrollSizes (MM_TEXT, rc.Size ());
		else {
			ASSERT (m_pHead);

			if (!p->IsValid ()) 
				p->Validate ();

			if (p->IsValid ()) {
				CDC dcMem;
				double dStretch [2] = { 1.0, 1.0 };
				const double dFactor = !m_pHead->m_bDigiNET ? 1.0 : ((double)pDC->GetDeviceCaps (LOGPIXELSX) / m_pHead->GetRes ());
				
				dcMem.CreateCompatibleDC (pDC);
				p->CalcStretch (* m_pHead, dStretch);

				m_sizeElement = CBaseElement::ThousandthsToLogical (p->GetSize (* m_pHead), * m_pHead);
				m_sizeElement.cx = (int)((double)m_sizeElement.cx * dFactor);

				CSize sizePrev = GetTotalSize ();
				CSize sizeZoom = m_sizeElement;

				sizeZoom.cx = (int)((double)sizeZoom.cx * dStretch [0] * m_dZoom / dFactor);
				sizeZoom.cy = (int)((double)sizeZoom.cy * dStretch [1] * m_dZoom);

				#ifdef __WYSIWYGFIX__
				sizeZoom.cx = (int)((double)m_sizeElement.cx * m_dZoom / dFactor);
				sizeZoom.cy = (int)((double)m_sizeElement.cy * m_dZoom);
				#endif

				if (!m_pBmp) {
					CSize stretch = m_sizeElement;
					CDC dcElement;
					CBitmap bmpElement;

					stretch.cx = (int)((double)stretch.cx * dStretch [0]);
					stretch.cy = (int)((double)stretch.cy * dStretch [1]);

					if (sizeZoom != sizePrev)
						SetScrollSizes (MM_TEXT, sizeZoom);

					dcElement.CreateCompatibleDC (pDC);

					m_pBmp = new CBitmap ();

					m_pBmp->CreateCompatibleBitmap (pDC, sizeZoom.cx, sizeZoom.cy);
					bmpElement.CreateCompatibleBitmap (pDC, m_sizeElement.cx, m_sizeElement.cy);

					CBitmap * pOldMem = dcMem.SelectObject (m_pBmp);
					CBitmap * pOldElement = dcElement.SelectObject (&bmpElement);

					::BitBlt (dcMem, 0, 0, sizeZoom.cx, sizeZoom.cy, NULL, 0, 0, WHITENESS);
					::BitBlt (dcElement , 0, 0, m_sizeElement.cx, m_sizeElement.cy, NULL, 0, 0, WHITENESS);

					p->Draw (dcElement, * m_pHead);

					dcMem.StretchBlt (0, 0, sizeZoom.cx, sizeZoom.cy, 
						&dcElement, 0, 0, m_sizeElement.cx, m_sizeElement.cy, SRCCOPY);

					dcElement.SelectObject (pOldElement);
					dcMem.SelectObject (pOldMem);
				}

				if (m_pBmp) {
					CBitmap * pBmp = dcMem.SelectObject (m_pBmp);
					DIBSECTION ds;

					::ZeroMemory (&ds, sizeof (ds));
					VERIFY (m_pBmp->GetObject (sizeof (ds), &ds));

					pDC->StretchBlt (0, 0, 
						(int)((double)sizeZoom.cx),
						(int)((double)sizeZoom.cy),
						&dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, SRCCOPY);

					dcMem.SelectObject (pBmp);
				}
			}
			else {
				CRect rc;

				GetClientRect (rc);
				pDC->DrawText (LoadString (IDS_INVALIDDATA), rc, 0);
			}
		}
	}
}

bool CPreviewCtrl::Create (CWnd * pParent, const CRect & rc, const FoxjetCommon::CBaseElement * pElement,
						   const FoxjetDatabase::HEADSTRUCT * pHead)
{
	if (CScrollView::Create (NULL, NULL, 
		WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL,
		rc, pParent, 0))
	{
		VERIFY (m_pElement = pElement);
		VERIFY (m_pHead = pHead);

		return true;
	}

	return false;
}

BOOL CPreviewCtrl::OnEraseBkgnd(CDC* pDC) 
{
	CRect rc;

	GetClientRect (rc);
	pDC->FillSolidRect (rc, Color::rgbDarkGray);

	return TRUE;
}

void CPreviewCtrl::PostNcDestroy() 
{
	CWnd::PostNcDestroy();
}

int CPreviewCtrl::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message) 
{
	// NOTE: do NOT call CScrollView::OnMouseActivate
	return CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
}
