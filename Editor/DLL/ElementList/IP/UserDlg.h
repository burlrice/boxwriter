#if !defined(AFX_USERDLG_H__471C7428_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_USERDLG_H__471C7428_8B3D_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserDlg.h : header file
//

#include "TextDlg.h"
#include "UserElement.h"

/////////////////////////////////////////////////////////////////////////////
// CUserDlg dialog

class CUserDlg : public CTextDlg
{
// Construction
public:
	CUserDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CUserDlg)
	BOOL	m_bPrompt;
	CString	m_strData;
	CString	m_strPrompt;
	//}}AFX_DATA

	UINT	m_nMaxChars;

protected:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserDlg)
	protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual int BuildElement();
	FoxjetIpElements::CUserElement & GetElement ();
	const FoxjetIpElements::CUserElement & GetElement () const;
	
	afx_msg void SetDefaultData ();
	afx_msg void OnDefaultClick ();

	// Generated message map functions
	//{{AFX_MSG(CUserDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERDLG_H__471C7428_8B3D_11D4_915E_00104BEF6341__INCLUDED_)
