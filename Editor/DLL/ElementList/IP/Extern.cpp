#include "stdafx.h"
#include "Extern.h"


ELEMENT_API FoxjetCommon::CElementDefaults ListGlobals::defElements;

static FoxjetDatabase::COdbcDatabase * pDatabase = NULL;

ELEMENT_API FoxjetDatabase::COdbcDatabase & ListGlobals::GetDB ()
{
	ASSERT (::pDatabase);
	return * ::pDatabase;
}

ELEMENT_API bool ListGlobals::SetDB (FoxjetDatabase::COdbcDatabase & db)
{
	ASSERT (::pDatabase == NULL);
	::pDatabase = &db;
	return true;
}

