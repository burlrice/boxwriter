// DateTimeDLg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "DateTimeDLg.h"
#include "Resource.h"
#include "Debug.h"

#include "fj_element.h"
#include "fj_datetime.h"
#include "fj_text.h"
#include "fj_printhead.h"
#include "fj_font.h"
#include "fj_export.h"

typedef struct tagIDMAPSTRUCT
{
	tagIDMAPSTRUCT (UINT nResID, int nValue) : m_nResID (nResID), m_nValue (nValue) { }
	UINT m_nResID;
	int m_nValue;
} IDMAPSTRUCT;

static const tagIDMAPSTRUCT end (-1, -1);

static bool operator == (const IDMAPSTRUCT & lhs, const IDMAPSTRUCT & rhs)
{
	return 
		lhs.m_nResID == rhs.m_nResID &&
		lhs.m_nValue == rhs.m_nValue;
}

static bool operator != (const IDMAPSTRUCT & lhs, const IDMAPSTRUCT & rhs)
{
	return !(lhs == rhs);
}


#define MAKEIDMAPSTRUCT(n) IDMAPSTRUCT (IDS_##n, DT_##n)

using namespace FoxjetCommon;
using namespace FoxjetIpElements;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDateTimeDlg dialog


CDateTimeDlg::CDateTimeDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa,
						   const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	CElementDlg(e, pa, CDateTimeDlg::IDD, pList, pParent)
{
	const CDateTimeElement & element = GetElement ();
	const LPFJDATETIME p = element.GetMember ();
	ASSERT (p);

	m_lBold				= p->lBoldValue;
	m_lWidth			= p->lWidthValue;
	m_lGap				= p->lGapValue;
	m_strFont			= p->strFontName;
	m_bRightAlign		= p->cAlign == 'R' ? true : false;
	m_strDelim			= CString (p->cDelim);
	m_strFill			= CString (p->cFill);
	m_lLength			= p->lLength;
	m_lHourACount		= p->lHourACount;
	m_nFormat			= (int)p->lFormat;
	m_nType				= (int)p->lTimeType;
	m_strFont			= element.GetFontName ();
	m_bSpecialString	= !p->bStringNormal;
}


void CDateTimeDlg::DoDataExchange(CDataExchange* pDX)
{
	CElementDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDateTimeDlg)
	//}}AFX_DATA_MAP
	DDX_CBString (pDX, CB_FONT, m_strFont);

	DDX_Text (pDX, TXT_BOLD, m_lBold);
	DDV_MinMaxInt (pDX, m_lBold, CIpBaseElement::m_lmtBold.m_dwMin, CIpBaseElement::m_lmtBold.m_dwMax); 

	DDX_Text (pDX, TXT_WIDTH, m_lWidth);
	DDV_MinMaxInt (pDX, m_lWidth, CIpBaseElement::m_lmtWidth.m_dwMin, CIpBaseElement::m_lmtWidth.m_dwMax); 

	DDX_Text (pDX, TXT_GAP, m_lGap);
	DDV_MinMaxInt (pDX, m_lGap, CIpBaseElement::m_lmtGap.m_dwMin, CIpBaseElement::m_lmtGap.m_dwMax); 

	DDX_Check (pDX, CHK_RIGHTALIGN, m_bRightAlign);
	DDX_Text(pDX, TXT_DELIM, m_strDelim);
	DDX_Text(pDX, TXT_FILL, m_strFill);

	DDX_Text(pDX, TXT_LENGTH, m_lLength);
	DDV_MinMaxInt (pDX, m_lLength, CDateTimeElement::m_lmtLength.m_dwMin, CDateTimeElement::m_lmtLength.m_dwMax); 
	
	DDX_Text(pDX, TXT_HOURACOUNT, m_lHourACount);
	DDV_MinMaxInt (pDX, m_lHourACount, CDateTimeElement::m_lmtHourACount.m_dwMin, CDateTimeElement::m_lmtHourACount.m_dwMax); 

	DDX_Check (pDX, CHK_STRINGNORMAL, m_bSpecialString);
}


BEGIN_MESSAGE_MAP(CDateTimeDlg, CElementDlg)
	//{{AFX_MSG_MAP(CDateTimeDlg)
	ON_CBN_SELCHANGE(CB_FORMAT, OnSelchangeFormat)
	//}}AFX_MSG_MAP
    ON_CBN_SELCHANGE(CB_TYPE, InvalidatePreview)
	ON_EN_CHANGE(TXT_LENGTH, InvalidatePreview)
	ON_BN_CLICKED(CHK_RIGHTALIGN, InvalidatePreview)
	ON_EN_CHANGE(TXT_FILL, InvalidatePreview)
	ON_EN_CHANGE(TXT_DELIM, InvalidatePreview)
	ON_EN_CHANGE(TXT_HOURACOUNT, InvalidatePreview)
	ON_BN_CLICKED(CHK_SPECIAL, InvalidatePreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDateTimeDlg message handlers

BOOL CDateTimeDlg::OnInitDialog()
{
	CComboBox * pFont = (CComboBox *)GetDlgItem (CB_FONT);

	ASSERT (pFont);

	BOOL bResult = CElementDlg::OnInitDialog();

	CArray <CPrinterFont, CPrinterFont &> vFonts;
	const CHead & head = GetList ()->GetHead (0);
	head.GetFonts (vFonts);

	for (int i = 0; i < vFonts.GetSize (); i++) {
		CString str = vFonts [i].m_strName;
		int nIndex = pFont->AddString (str);

		if (str == m_strFont)
			pFont->SetCurSel (nIndex);
	}

	InitTypeCtrl ();
	InitFormatCtrl ();
	InvalidatePreview ();
	OnSelchangeFormat ();

	return bResult;
}

int CDateTimeDlg::BuildElement ()
{
	CDateTimeElement & e = GetElement ();
	CComboBox * pFormat = (CComboBox *)GetDlgItem (CB_FORMAT);
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	CString str, strFont;
	TCHAR cDelim = 0, cFill = 0;

	GetDlgItemText (TXT_FILL, str);

	if (str.GetLength ())
		cFill = str [0];

	GetDlgItemText (TXT_DELIM, str);

	if (str.GetLength ())
		cDelim = str [0];

	ASSERT (pFormat);
	ASSERT (pType);

	GetDlgItemText (CB_FONT, strFont);

	e.SetFontName (strFont);
	e.SetTimeType ((FJDTTYPE)pType->GetItemData (pType->GetCurSel ()));		
	e.SetFormat ((FJDTFORMAT)pFormat->GetItemData (pFormat->GetCurSel ()));	
	e.SetBold (GetDlgItemInt (TXT_BOLD));
	e.SetGap (GetDlgItemInt (TXT_GAP));
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));
	e.SetFlippedH (GetCheck (CHK_FLIPH));
	e.SetFlippedV (GetCheck (CHK_FLIPV));
	e.SetInverse (GetCheck (CHK_INVERSE));
	e.SetAlign (GetCheck (CHK_RIGHTALIGN) ? 'R' : 'L');
	e.SetDelim ((char)cDelim);
	e.SetFill ((char)cFill);
	e.SetLength	 (GetDlgItemInt (TXT_LENGTH));
	e.SetHourACount	(GetDlgItemInt (TXT_HOURACOUNT));
	e.SetStringNormal (!GetCheck (CHK_SPECIAL));

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

CDateTimeElement & CDateTimeDlg::GetElement ()
{
	CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CDateTimeElement)));
	return (CDateTimeElement &)e;
}

const CDateTimeElement & CDateTimeDlg::GetElement () const
{
	const CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CDateTimeElement)));
	return (CDateTimeElement &)e;
}

void CDateTimeDlg::InitFormatCtrl()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_FORMAT);
	IDMAPSTRUCT id [] = 
	{
		MAKEIDMAPSTRUCT (DATE_YYYYMMDD),
		MAKEIDMAPSTRUCT (DATE_DDMMYYYY),
		MAKEIDMAPSTRUCT (DATE_MMDDYYYY),
		MAKEIDMAPSTRUCT (DATE_YYYY),
		MAKEIDMAPSTRUCT (DATE_MM),
		MAKEIDMAPSTRUCT (DATE_DD),
//		MAKEIDMAPSTRUCT (DATE_YEAR2),
		MAKEIDMAPSTRUCT (DATE_YYMMDD),
		MAKEIDMAPSTRUCT (DATE_DDMMYY),
		MAKEIDMAPSTRUCT (DATE_MMDDYY),
		MAKEIDMAPSTRUCT (DATE_YY),
		MAKEIDMAPSTRUCT (DATE_DAYWK),
		MAKEIDMAPSTRUCT (DATE_WK),
		MAKEIDMAPSTRUCT (DATE_JULIAN),
		MAKEIDMAPSTRUCT (DATE_MONTHA),
		MAKEIDMAPSTRUCT (DATE_DAYWKA),
		MAKEIDMAPSTRUCT (TIME_HHMMSS24),
		MAKEIDMAPSTRUCT (TIME_HHMMSS12),
		MAKEIDMAPSTRUCT (TIME_HHMMSS12M),
		MAKEIDMAPSTRUCT (TIME_HHMM24),
		MAKEIDMAPSTRUCT (TIME_HHMM12),
		MAKEIDMAPSTRUCT (TIME_HHMM12M),
		MAKEIDMAPSTRUCT (TIME_HH),
		MAKEIDMAPSTRUCT (TIME_MM),
		MAKEIDMAPSTRUCT (TIME_SS),
		MAKEIDMAPSTRUCT (TIME_AMPM),
		MAKEIDMAPSTRUCT (TIME_HOURA),
		MAKEIDMAPSTRUCT (TIME_SHIFT),
		::end
	};

	ASSERT (pCB);

	for (int i = 0; id [i] != end; i++) {
		int nIndex = pCB->AddString (LoadString (id [i].m_nResID));
		pCB->SetItemData (nIndex, id [i].m_nValue);

		if (m_nFormat == id [i].m_nValue)
			pCB->SetCurSel (nIndex);
	}

	if (pCB->GetCurSel () == CB_ERR)
		pCB->SetCurSel (0);
}


void CDateTimeDlg::InitTypeCtrl()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_TYPE);
	IDMAPSTRUCT id [] = 
	{
		MAKEIDMAPSTRUCT (TIME_NORMAL),
		MAKEIDMAPSTRUCT (TIME_EXPIRATION),
		MAKEIDMAPSTRUCT (TIME_ROLLOVER),
		MAKEIDMAPSTRUCT (TIME_ROLL_EXP),
		::end
	};

	ASSERT (pCB);

	for (int i = 0; id [i] != end; i++) {
		int nIndex = pCB->AddString (LoadString (id [i].m_nResID));
		pCB->SetItemData (nIndex, id [i].m_nValue);

		if (m_nType == id [i].m_nValue)
			pCB->SetCurSel (nIndex);
	}
}

void CDateTimeDlg::OnOK()
{
	CComboBox * pFormat = (CComboBox *)GetDlgItem (CB_FORMAT);
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);

	ASSERT (pFormat);
	ASSERT (pType);

	ASSERT (pFormat->GetCurSel () != CB_ERR);
	ASSERT (pType->GetCurSel () != CB_ERR);

	m_nFormat = pFormat->GetItemData (pFormat->GetCurSel ());
	m_nType = pType->GetItemData (pType->GetCurSel ());

	CElementDlg::OnOK ();
}

void CDateTimeDlg::OnSelchangeFormat() 
{
	CComboBox * pFormat = (CComboBox *)GetDlgItem (CB_FORMAT);
	bool bHourACount = false;
	bool bSpecial = false;

	ASSERT (pFormat);
	int nIndex = pFormat->GetCurSel ();

	if (nIndex != CB_ERR) {
		FJDTFORMAT format = (FJDTFORMAT)pFormat->GetItemData (nIndex);

		bHourACount = format == DT_TIME_HOURA;
		bSpecial = HasSpecialCode (format);
	}

	ASSERT (GetDlgItem (TXT_HOURACOUNT));
	GetDlgItem (TXT_HOURACOUNT)->EnableWindow (bHourACount);

	ASSERT (GetDlgItem (CHK_SPECIAL));
	GetDlgItem (CHK_SPECIAL)->EnableWindow (bSpecial);

	InvalidatePreview ();
}

bool CDateTimeDlg::HasSpecialCode (int nType)
{
	const int nSpecial [] = {
		DT_DATE_DAYWKA,	
		DT_DATE_MONTHA,	
	};
	const int nSize = sizeof (nSpecial) / sizeof (nSpecial [0]);

	for (int i = 0; i < nSize; i++) 
		if (nSpecial [i] == nType)
			return true;

	return false;
}
