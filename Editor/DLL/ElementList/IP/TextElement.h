// TextElement.h: interface for the CTextElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TextElement_H__E0AB60D7_7AF8_4943_A4DC_CBDE2AF417FF__INCLUDED_)
#define AFX_TextElement_H__E0AB60D7_7AF8_4943_A4DC_CBDE2AF417FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IpBaseElement.h"
#include "PrinterFont.h"
#include "Types.h"
#include "fj_element.h"

FORWARD_DECLARE (fjtext, FJTEXT)

namespace FoxjetIpElements
{
	class ELEMENT_API CTextElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CTextElement);

	public:
		CTextElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CTextElement (const CTextElement & rhs);
		CTextElement & operator = (const CTextElement & rhs);
		virtual ~CTextElement ();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		virtual void CreateImage ();
		virtual CString GetElementFontName () const;
		virtual bool SetFont (const FoxjetCommon::CPrinterFont & f);

		virtual bool SetDefaultData (const CString &strData);

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		DECLARE_MEMBERPOINTERACCESS (LPFJTEXT, GetElement ()->pDesc)

		virtual LONG GetBold () const;
		virtual bool SetBold (LONG lValue);

		virtual int GetWidth () const;
		virtual bool SetWidth (int nValue);
		
		LONG GetGap () const;
		bool SetGap (LONG lValue);

		CHAR GetAlignment () const;
		bool SetAlignment (CHAR cAlign);

		static const FoxjetCommon::LIMITSTRUCT m_lmtString; 
		static const FoxjetCommon::LIMITSTRUCT m_lmtFontSize;

	protected:
		virtual bool SetElement (LPFJELEMENT pNew);

		CString m_strDefaultData;
		CString m_strImageData;
	};
}; //namespace FoxjetIpElements

#endif // !defined(AFX_TextElement_H__E0AB60D7_7AF8_4943_A4DC_CBDE2AF417FF__INCLUDED_)
