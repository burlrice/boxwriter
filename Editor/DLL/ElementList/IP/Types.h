#ifndef __TYPES_H__
#define __TYPES_H__

/*
from fj_element.h
	FJ_TYPE_TEXT		    // 1 text
	FJ_TYPE_BARCODE		    // 2 bar code
	FJ_TYPE_BITMAP		    // 3 bitmap
	FJ_TYPE_DYNIMAGE	    // 4 dynamic image	
	FJ_TYPE_DATETIME	    // 5 date time	
	FJ_TYPE_COUNTER		    // 6 counter
	FJ_TYPE_DYNTEXT			// 7 dynamic text
	FJ_TYPE_DYNBARCODE		// 8 dynamic barcode
	FJ_TYPE_DATAMATRIX		// 9 data matrix
*/

typedef enum {
	UNKNOWN						= -1,
	FIRST						= 0,
	TEXT						= FIRST,
	BMP,						
	COUNT,
	DATETIME,
	BARCODE,
	DATAMATRIX,
	DYNAMIC_TEXT,
	DYNAMIC_BARCODE,
	USER,
	LAST						= DYNAMIC_BARCODE + 1,
} ELEMENTTYPE;

#define FORWARD_DECLARE(fwd_class_name, real_class_name) \
	struct fwd_class_name; \
	typedef struct fwd_class_name real_class_name, FAR * LP##real_class_name, FAR * const CLP##real_class_name; \
	typedef const struct fwd_class_name FAR * LPC##real_class_name, FAR * const CLPC##real_class_name; 

#endif //__TYPES_H__
