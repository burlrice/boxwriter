// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^//                                                             
// Copyright � 1999 Informatics, Inc. All rights reserved. //
//
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^//                                                             

//////////////////////////////////////////////////////////
//
// ---------------------------------------------------------------
// NOTICE   : Use of this software is subject to acceptance of the
//			  Informatics, Inc. Software License Agreement with
//			  Hewlett Packard of Korea.  Informatics and Wasp
//			  are trademarks of Informatics, Inc.  This software
//			  is owned by Informatics, Inc. and is protected by
//			  United States copyright laws and international
//			  treaty provisions. �1999.  All rights reserved.
// ---------------------------------------------------------------
//
// FILENAME :	bcxl.h
//
// OWNER    :	Informatics, Inc., Plano, Texas USA (www.Informatics-Inc.com)
//
// AUTHOR   :	W.Bailey
//
//////////////////////////////////////////////////////////



/////////////////////////////////////
//
// FILENAME: BCXL.h
//
/////////////////////////////////////
#ifndef BCXL_H
#define BCXL_H                                                             

//////////////////////////////////////////
/*
#define kSymbologyFirst    			0  // must be the same number as the lowest one
#define kSymbologyCode39Regular    	0  // Code 3 of 9 standard set, DO NOT USE THIS ONE!
#define kSymbologyCode39Full		1  // Code 3 of 9 Full ASCII set....USE THIS ONE!
#define kSymbologyInter2of5			2  // Interleave 2of5
#define kSymbologyCodabar			3  // Codabar
#define kSymbologyMSI				4  // MSI
#define kSymbologyCode93    		5  // Code 93, full ASCII set
#define kSymbologyUPCA				6  // UPC A
#define kSymbologyUPCE				7  // UPC E
#define kSymbologyPostNet			8  // PostNet
#define kSymbologyEANJAN_8			9  // EAN/JAN 8
#define kSymbologyEANJAN_13			10 // EAN/JAN 13
#define kSymbologyCode128			11 // Code128                                  // 9606240
#define kSymbologyCode39Standard	12 // Standard Code39                          // 9710310
#define kSymbologyPDF417			13 // PDF417
#define kSymbologyMaxiCode			14 // MaxiCode
#define kSymbologyDataMatrixEcc200	15 // ECC 200 DataMatrix
#define kSymbologyLOGMARS			17 // LOGMARS
#define kSymbologyEAN128			18
#define kSymbologyDataMatrixGs1		19 // GS1 DataMatrix
#define kSymbologyLast    			19 // must be the same number as the highest one
*/
#define kSymbologyFirst    			0  // must be the same number as the lowest one
#define kSymbologyCode39Regular    	0  // Code 3 of 9 standard set, DO NOT USE THIS ONE!
#define kSymbologyCode39Full		1  // Code 3 of 9 Full ASCII set....USE THIS ONE!
#define kSymbologyInter2of5			2  // Interleave 2of5
#define kSymbologyCodabar			3  // Codabar
#define kSymbologyMSI				4  // MSI
#define kSymbologyCode93    		5  // Code 93, full ASCII set
#define kSymbologyUPCA				6  // UPC A
#define kSymbologyUPCE				7  // UPC E
#define kSymbologyPostNet			8  // PostNet
#define kSymbologyEANJAN_8			9  // EAN/JAN 8
#define kSymbologyEANJAN_13			10 // EAN/JAN 13
#define kSymbologyCode128			11 // Code128                                  // 9606240
#define kSymbologyCode39Standard	12 // Standard Code39                          // 9710310
#define kSymbologyPDF417			13 // PDF417
#define kSymbologyMaxiCode			14 // MaxiCode
#define kSymbologyDataMatrixEcc200	15 // ECC 200 DataMatrix
#define kSymbologyLOGMARS			16 // LOGMARS
#define kSymbologyEAN128			18
#define kSymbologyDataMatrixGs1		19 // GS1 DataMatrix
#define kSymbologyGTIN14			20 
#define kSymbologyLast    			20 // must be the same number as the highest one

#define kSymbologyNormalText    	22
#define kSymbologyUnknown	    	999

//////////////////////////////////////////
#define kResTypeLow    	0  // low resolution
#define kResTypeMed		1  // medium resolution
#define kResTypeHigh	2  // high resolution

//////////////////////////////////////////
#define kCaptionTypeNone    	0  // no caption
#define kCaptionTypeBelow		1  // caption below

///////////////////
#ifndef _2DSTRUCTS
#define _2DSTRUCTS
/* structure defining PDF417 specifics */
typedef struct tagPDF417INFO
{
    int nCols;            		// number of columns (1-30)
    int nRows;                	// number of rows (3-90)
    int nErrLevel;              // error correction level (0-8)
    BOOL bTruncate;             // TRUE to not print right row indicator and stop pattern
    int nDataLen;             	// number of characters in input data (if zero, then use strlen())
}PDF417INFO;

/* structure defining MaxiCode specifics */
typedef struct tagMAXICODEINFO
{
    int nMode;            		// mode 2,3,4,or 5
    LPSTR sPostalCode;       	// up to 9 digit postal code (only used in modes 2 and 3)
    LPSTR sCountryCode;       	// 3 digit country code (only used in modes 2 and 3)
    LPSTR sClassOfService;    	// 3 digit class of service (only used in modes 2 and 3)
    int nDataLen;             	// number of characters in input data (if zero, then use strlen())
}MAXICODEINFO;

/* structure defining DataMatrix specifics */
typedef struct tagDATAMATRIXINFO
{
    int nDataLen;             	// number of characters in input data (if zero, then use strlen())
}DATAMATRIXINFO;
#endif
////////////////
                                                                     
///////////////////////////////////////////////////////////////////////////
extern "C" int WINAPI EXPORT BCXL_VersionMajor();
extern "C" int WINAPI EXPORT BCXL_VersionMinor();
extern "C" int WINAPI EXPORT BCXL_VersionRevision();

//////////////////////////////////////////////////////////////////////////                                          
extern "C" int WINAPI EXPORT BC_FontTranslate(
		int symtype,				// one of the kSymbologyXXX defs above...
		char* instring,			    // Human readable ASCII input string
		int bCheck,					// Check Digit flag (0=false, 1 = true)
		char* outstring,			// the storage for the output translated string
		int nOutStringMax,			// max size of out string
		int* nOutStringReturned );	// actual size of out string (returned)
//////////////////////////////////////////////////////////////////////////                                          
extern "C" int WINAPI EXPORT BC_PDFFontTranslate(
		char* instring,				// Human readable ASCII input string
		char* outstring,			// the storage for the output translated string
		int nOutStringMax,			// max size of out string
		int* nOutStringReturned,	// actual size of out string (returned)
		PDF417INFO* pPdfInfo);		// pointer to a structure that keeps info specific to PDf417.
//////////////////////////////////////////////////////////////////////////                                          
extern "C" int WINAPI EXPORT BC_DataMatrixFontTranslate(
		char * instring,			// Human readable ASCII input string
		char * outstring,			// the storage for the output translated string
		int nOutStringMax,			// max size of out string
		int * nOutStringReturned,	// actual size of out string (returned)
		DATAMATRIXINFO* pDMInfo);	// pointer to a structure that keeps info specific to Datamatrix
/////////////////////////////////////////////////////////////////////////
extern "C" int WINAPI EXPORT BC_FontGetFaceName(
		char* sFaceNamePrefix,		// the face name prefix "WASP ", "XYZ "
		int nSymType,				// one of the kSymbologyXXX defs...
		int nResType,				// one of the kResTypeXXX defs...
		int bCaption,				// true/false
		char* outstring,
		int nOutStringMax,			// max size of out string
		int* nOutStringReturned );	// actual size of out string (returned)

#endif                                          
