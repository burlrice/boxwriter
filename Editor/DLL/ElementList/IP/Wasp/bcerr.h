// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^//      
// Copyright � 1996 Informatics, Inc. All rights reserved.    //
//                                                            //
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^//

/////////////////////////////////////
//
// FILENAME: BCERR.h
//
/////////////////////////////////////


#ifndef BCERR_H
#define BCERR_H
#ifdef _CRW
#define EXPORT 
//#include <afxwin.h>
#else
#include <afxwin.h>
#endif

//////////////////////////////////////////////////////////////////
// The following lines define error messages returned by the 
// translator and drawing dll's.  Any new error messages are added
// here. In addition, any errors that could potentially be returned 
// in Crystal Reports need to be added to ErrorTable below.
// (m.mchenry 5/18/98)
///////////////////////////////////////////////////////////////////
/*Error 32500 */
#define kERRDESC_INCOMPLETE_UPCA "ERROR: Incomplete number of UPC-A characters, must be 11, 13 (2 Digit supplement) or 16 (5 Digit supplement) digits."
/*Error 32501 */
#define kERRDESC_INCOMPLETE_UPCE "ERROR: Incomplete number of UPC-E characters, must be 6, 8 (2 Digit supplement) or 11 (5 Digit supplement) digits."
/*Error 32502 */
#define kERRDESC_INCOMPLETE_EANJAN_8 "ERROR: Incomplete number of EAN/JAN characters, must be 7, 9 (2 Digit supplement) or 12 (5 Digit supplement) digits."
/*Error 32503 */
#define kERRDESC_INCOMPLETE_EANJAN_13 "ERROR: Incomplete number of EAN/JAN characters, must be 12, 14 (2 Digit supplement) or 17 (5 Digit supplement) digits."
/*Error 32504 */
#define kERRDESC_INCOMPLETE_POSTNET "ERROR: Incomplete number of PostNet characters, must be 5, 9 (Zip+4) or 11 (Zip+4+DP) digits."
/*Error 32505 */
#define kERRDESC_BAD_CHAR "ERROR: Illegal character for this symbology."
/*Error 32506 */
#define kERRDESC_BAD_CHAR_ASCII_0_127 "ERROR: Illegal character for this symbology.  Character must be in ASCII range 0...127."
/*Error 32507 */
#define kERRDESC_BAD_CHAR_NUMERIC "ERROR: Illegal character for this symbology.  Character must be numeric."
/*Error 32508 */
#define kERRDESC_FIRST_CHAR_ZERO "ERROR: Illegal character for this symbology.  First digit must be a '0'."
/*Error 32509*/
#define kERRDESC_UPCE_MISSING_ZERO "ERROR: Missing a required placement of one or more zeros for UPC-E."
/*Error 32510 */
#define kERRDESC_TOO_MANY "ERROR: Too many characters for this symbology."
/*Error 32511 */
#define kERRDESC_UNKNOWN_TYPE "ERROR: Unknown symbology type."
/*Error 32512 */
#define kERRDESC_MSG_LEN "ERROR: Returned string too long for given buffer."
/*Error 32513 */
#define kERRDESC_SOFTWARE_LOGIC	"ERROR: Software Logic error, contact manufacturer."
/////////////////////////////////////////////////////////////////////			
//// the below errors are ones that are used within waspbc16.dll
/////////////////////////////////////////////////////////////////////			
/*Error 32514 */
#define kERRDESC_INVALID_SYMBOL	"ERROR: Bar code value does not translate into a valid symbol for the current symbology."
/*Error 32515 */
#define kERRDESC_DC_ATTACH "ERROR: Invalid device context specified."
/*Error 32516 */
#define kERRDESC_INVALID_SYMBOLOGY "ERROR: Invalid bar code symbology specified.  Check documentation for valid range."
/*Error 32517 */
#define kERRDESC_INVALID_RATIO "ERROR: Invalid bar code ratio specified.  Check documentation for valid range."
/*Error 32518 */
#define kERRDESC_INVALID_ROTATION "ERROR: Invalid rotation specified.  Check documentation for valid range."
/*Error 32519 */
#define kERRDESC_INVALID_MAG "ERROR: Invalid magnification specified.  Check documentation for valid range."
/*Error 32520 */
#define kERRDESC_INVALID_UNITS "ERROR: Invalid units specified.  Check documentation for valid range."
/*Error 32521 */
#define kERRDESC_INVALID_BAR_WIDTH "ERROR: Invalid bar width specified.  Check documentation for valid range."
/*Error 32522 */
#define kERRDESC_INVALID_BAR_HEIGHT	"ERROR: Invalid bar height specified.  Check documentation for valid range."
/*Error 32523 */
#define kERRDESC_FONT_CREATE "ERROR: An error occurred while attempting to create the specified font."
/*Error 32524 */
#define kERRDESC_FONT_SELECT "ERROR: An error occurred while attempting to select the specified font."
/*Error 32525 */
#define kERRDESC_TEXTOUT "ERROR: An error occurred while attempting to draw the caption text."
/*Error 32526 */
#define kERRDESC_XLATEMAP "ERROR: Software Logic error, unexpected character encountered in translation output string."
/*Error 32527 */
#define kERRDESC_DEMO_CHAR "ERROR: Demo version of this software supports alphanumeric characters in the ranges: 0...4, A...E, a...e"
/*Error 32528 */
#define kERRDESC_DEMO_NONFEATURE "ERROR: Check digit feature is unsupported in Demo version of the software."
/*Error 32529 */
#define kERRDESC_MEMALLOC "ERROR: Out of Memory."
/*Error 32530 */
#define kERRDESC_DLL_LOAD "ERROR: A required DLL could not be loaded."
/*Error 32531 */
#define kERRDESC_PDF417_ROWCOL_FIT "ERROR: The specified data will not fit in a PDF417 symbol."
/*Error 32532 */
#define kERRDESC_PDF417_EDAC "ERROR: Error generating EDAC for a PDF417 symbol."
/*Error 32533 */
#define kERRDESC_PDF417_GENDATA "ERROR: Error generating data codewords for a PDF417 symbol."
/*Error 32534 */
#define kERRDESC_MAXICODE_INVALID_CHAR "ERROR: Error generating symbol character for a Maxicode symbol."
/*Error 32535 */
#define kERRDESC_MAXICODE_TOO_MANY_CHARS "ERROR: The data characters would not fit in the symbol. "
/*Error 32536 */
#define	kERRDESC_MAXICODE_INVALID_MODE "ERROR: Error setting the Maxicode mode. Range is 2 through 6."
/*Error 32537 */
#define kERRDESC_MAXICODE_INVALID_NUM_CODEWORDS "ERROR: Error generating the primary and secondary messages."
/*Error 32538 */
#define kERRDESC_MAXICODE_DRAWBULLSEYE "ERROR: Error drawing the MaxiCode center bullseye pattern."
/*Error 32539 */
#define kERRDESC_MAXICODE_DRAWMODULE "ERROR: Error drawing the MaxiCode modules."
/*Error 32540 */
#define kERRDESC_NO_2D_SUPPORT "ERROR: Two-dimensional symbologies are not supported in this version."
/*Error 32541 */
#define kERRDESC_STRLEN_ZERO "ERROR: String length of zero."
/*Error 32542 */
#define kERRDESC_CLIPBOARD_OPEN "ERROR: Clipboard could not be opened."
/*Error 32543 */
#define kERRDESC_CLIPBOARD_EMPTY "ERROR: Clipboard could not be emptied."
/*Error 32544 */
#define kERRDESC_CLIPBOARD_REG_FORMAT "ERROR: Clipboard format could not be registered.."
/*Error 32545 */
#define kERRDESC_CLIPBOARD_SET_DATA "ERROR: Data could not be placed on the Clipboard."
/*Error 32546*/
#define kERRDESC_DATAMATRIX_GEN_EDAC "ERROR: Error generating EDAC for Data Matrix. Contact Manufacturer."
/*Error 32547 */
#define kERRDESC_DATAMATRIX_DRAWMODULE "ERROR: Error drawing the Data Matrix data modules. Contact manufacturer."
/*Error 32548 */
#define kERRDESC_DATAMATRIX_FINDER "ERROR: Error placing the Data Matrix finder pattern. Contact manufacturer."
/*Error 32549 */
#define kERRDESC_DATAMATRIX_PLACEDATA "ERROR: Error placing the Data Matrix data modules. Contact manufacturer."
/*Error 32550 */
#define kERRDESC_DATAMATRIX_LOOKAHEAD "ERROR: Error determining the encodation mode for the Data Matrix symbol. Contact manufacturer."
/*Error 32551 */
#define kERRDESC_DATAMATRIX_X12 "ERROR: Error encoding the data in X12 encodation mode for the Data Matrix symbol. Contact manufacturer."
/*Error 32552 */
#define kERRDESC_DATAMATRIX_EDIFACT "ERROR: Error encoding the data in EDIFACT encodation mode for the Data Matrix symbol. Contact manufacturer."
/*Error 32553 */
#define kERRDESC_DATAMATRIX_C40_TEXT "ERROR: Error encoding the data in C40 or Text encodation mode for the Data Matrix symbol. Contact manufacturer."
/*Error 32554 */
#define kERRDESC_DATAMATRIX_ENCODE "ERROR: Error encoding the data for the Data Matrix symbol. Contact manufacturer."
/*Error 32555 */
#define kERRDESC_DATAMATRIX_SYMBOL_SIZE "ERROR: Error determining the symbol size for the Data Matrix symbol. Decrease the length of the input data."
/*Error 32556 */
#define kERRDESC_INVALID_ALIGNMENT "ERROR: Invalid caption alignment specified.  Check documentation for valid range."
#define kERRDESC_UNKNOWN "ERROR: Unknown error."


/////////////////////////////////////////////////////////////////////
enum BC_Errors {                        /* enumerate error numbers */
	kERR_START_VALUE            = 32500,
        /* Visual Basic dictates that client errors start at 32767
           and go down, so we'll start at 32500 and go up...   :)
         */
    kERR_INCOMPLETE_UPCA        = kERR_START_VALUE,
        /* Description:
             "Incomplete number of UPC-A characters, must be 11, 13
             (2 Digit supplement) or 16 (5 Digit supplement) digits."
           Resolution:
             UPC-A has a fixed number of digits. Add or remove digits
             to form a valid UPC-A symbol.
         */
    kERR_INCOMPLETE_UPCE,
         /* Description:
              "Incomplete number of UPC-E characters, must be 6, 8
              (2 Digit supplement) or 11 (5 Digit supplement) digits."
            Resolution:
              UPC-E has a fixed number of digits. Add or remove digits
              to form a valid UPC-E symbol.
          */
    kERR_INCOMPLETE_EANJAN_8,
        /* Description:
             "Incomplete number of EAN/JAN characters, must be 7, 9
             (2 Digit supplement) or 12 (5 Digit supplement) digits."
           Resolution:
             EAN/JAN-8 has a fixed number of digits. Add or remove digits
             to form a valid EAN/JAN-8 symbol.
         */
    kERR_INCOMPLETE_EANJAN_13,
        /* Description:
             "Incomplete number of EAN/JAN characters, must be 12, 14
             (2 Digit supplement) or 17 (5 Digit supplement) digits."
           Resolution:
             EAN/JAN-13 has a fixed number of digits. Add or remove digits
             to form a valid EAN/JAN-13 symbol.
         */
    kERR_INCOMPLETE_POSTNET,
        /* Description:
             "Incomplete number of PostNet characters, must be 5, 9
             (Zip+4) or 11 (Zip+4+DP) digits."
           Resolution:
             PostNet has a fixed number of digits. Add or remove digits
             to form a valid PostNet symbol.
         */
    kERR_BAD_CHAR,                        
        /* Description:
             "Illegal character for this symbology."
           Resolution:
             The currently selected symbology does not support
             one or more characters in the input string. Specify valid
             input characters.
         */
    kERR_BAD_CHAR_ASCII_0_127,            
        /* Description:
             "Illegal character for this symbology.  Character must
             be in ASCII range 0...127."
           Resolution:
             The currently selected symbology supports only ASCII
             characters in the range 0...127. Specify valid input
             characters.
         */
    kERR_BAD_CHAR_NUMERIC,
        /* Description:
             "Illegal character for this symbology.  Character must
             be numeric."
           Resolution:
             The selected symbology only supports numeric characters.
             If a non-numeric is required in the bar code, a different
             symbology must be selected.
         */
    kERR_FIRST_CHAR_ZERO,
        /* Description:
             "Illegal character for this symbology.  First digit must
             be a '0'."
           Resolution:
             Only a number system character of zero is supported for
             UPC-E symbols.
         */
    kERR_UPCE_MISSING_ZERO,
        /* Description:
             "Missing a required placement of one or more zeros for
             UPC-E."
           Resolution:
             A UPC-E symbol is a UPC-A symbol with consecutive zeros
             suppressed to form a shorter symbol. The specified digits
             cannot be suppressed to form a valid UPC-E symbol. Check
             your input string to verify the proper placement of the
             zeros.
         */
    kERR_TOO_MANY,
        /* Description:
             "Too many characters for this symbology."
           Resolution:
             You have specified more than the maximum characters for the
             current symbology. Reduce the number of characters.
         */
    kERR_UNKNOWN_TYPE,
        /* Description:
             "Software Logic error, unknown symbology type."
           Resolution:
             Contact manufacturer.
         */
    kERR_MSG_LEN,
        /* Description:
             "Returned string too long for given buffer."
           Resolution:
             Specify a larger buffer for the returned error string.
         */
    kERR_SOFTWARE_LOGIC,
        /* Description:
             "Software Logic error, contact manufacturer."
           Resolution:
             Contact manufacturer.
         */
    kERR_INVALID_SYMBOL,
        /* Description:
             "Bar code value does not translate into a valid symbol
             for the current symbology."
           Resolution:
             Contact manufacturer.
         */
    kERR_DC_ATTACH,
        /* Description:
             "Invalid device context specified."
           Resolution:
             Specify a valid device context.
         */
    kERR_INVALID_SYMBOLOGY,
        /* Description:
             "Invalid bar code symbology specified. Check documentation
             for valid range."
           Resolution:
             Specify a valid symbology.
         */
    kERR_INVALID_RATIO,
        /* Description:
             "Invalid bar code ratio specified. Check documentation for
             valid range."
           Resolution:
             Specify a valid wide-to-narrow bar ratio.
         */
    kERR_INVALID_ROTATION,
        /* Description:
             "Invalid rotation specified.  Check documentation for valid
             range."
           Resolution:
             Specify a valid rotation index. 
         */
    kERR_INVALID_MAG,
        /* Description:
             "Invalid magnification specified.  Check documentation for
             valid range."
           Resolution:
             Specify a valid magnification percentage. 100% is nominal.
         */
    kERR_INVALID_UNITS,
        /* Description:
             "Invalid units specified.  Check documentation for valid
             range."
           Resolution:
             Specify valid units.
         */
    kERR_INVALID_BAR_WIDTH,
        /* Description:
             "Invalid bar width specified.  Check documentation for
             valid range."
           Resolution:
             Specify a valid bar width. Note: The bar width for UPC-A,
             UPC-E, EAN-13, and EAN-8 symbolgies is specified as a
             percentage of nominal (where nominal is 100%).
         */
    kERR_INVALID_BAR_HEIGHT,
        /* Description:
             "Invalid bar height specified.  Check documentation for
             valid range."
           Resolution:
             Specify a valid bar height. 
         */
    kERR_FONT_CREATE,
        /* Description:
             "An error occurred while attempting to create the specified
             font."
           Resolution:
             Close other applications to free up resources and try again.
         */
    kERR_FONT_SELECT,
        /* Description:
             "An error occurred while attempting to select the
             specified font."
           Resolution:
             Close other applications to free up resources and try again.
         */
    kERR_TEXTOUT,
        /* Description:
             "An error occurred while attempting to draw the caption
             text."
           Resolution:
             Close other applications to free up resources and try again.
         */
    kERR_XLATEMAP,
        /* Description:
             "Software Logic error, unexpected character encountered in
             translation output string."
           Resolution:
             The Translation DLL returned a character that is not
             recognized by the drawing DLL for the given symbology.
             This error should not occur under normal operating
             conditions. Consult the manufacturer.
         */
    kERR_DEMO_CHAR,
        /* Description:
             "Given character is not supported in Demo version of the
             software."
           Resolution:
             The purchased version of the product is fully functional.
         */
    kERR_DEMO_NONFEATURE_CD,
        /* Description:
             "Check digit feature is unsupported in Demo version of the software."
           Resolution:
             The purchased version of the product is fully functional.
         */
    kERR_MEMALLOC,
        /* Description:
             "Out of Memory."
           Resolution:
             The Drawing DLL was unable to allocate a necessary block
             of memory. Close other applications to free up available
             memory.
         */
    kERR_DLL_LOAD,
        /* Description:
             "A required DLL could not be loaded."
           Resolution:
             All the DLLs for this product should be in the Windows 
			 system directory or accessible via the path.To ensure that
			 all components are installed, re-install the product.
         */
    kERR_PDF417_ROWCOL_FIT,
        /* Description:
             "The specified data will not fit in a PDF417 symbol."
           Resolution:
             The maximum size of a PDF417 symbol is approximately 2700
             characters, if error correction level zero is used. The 
             higher the error correction level, the fewer data characters
             can be used. Decrease the error correction level, or split
             your data into 2 or more separate bar codes.
         */
    kERR_PDF417_EDAC,
        /* Description:
             "Error generating EDAC for a PDF417 symbol."
           Resolution:
             Contact manufacturer.
         */
    kERR_PDF417_GENDATA,
        /* Description:
             "Error generating data codewords for a PDF417 symbol."
           Resolution:
             Contact manufacturer.
         */
    kERR_MAXICODE_INVALID_CHAR,
        /* Description:
             "Error generating symbol character for a Maxicode symbol."
           Resolution:
             Contact manufacturer.
         */
	kERR_MAXICODE_TOO_MANY_CHARS,
        /* Description:
             "The data characters would not fit in the symbol."
           Resolution:
             Try decreasing the length of the input string or changing
             the mode to 4 to increase the capacity.
         */
	kERR_MAXICODE_INVALID_MODE,
        /* Description:
             "Error setting the Maxicode mode."
           Resolution:
             Specify a valid mode. Range is 2 through 6.
         */
	kERR_MAXICODE_INVALID_NUM_CODEWORDS,
        /* Description:
             "Error generating the primary and secondary messages."
           Resolution:
             Contact manufacturer.
         */
	kERR_MAXICODE_DRAWBULLSEYE,
        /* Description:
             "Error drawing the MaxiCode center bullseye pattern."
           Resolution:
             Contact manufacturer.
         */
	kERR_MAXICODE_DRAWMODULE,
        /* Description:
             "Error drawing the MaxiCode modules."
           Resolution:
             Contact manufacturer.
         */
	kERR_NO_2D_SUPPORT,
        /* Description:
             "Two-dimensional symbologies are not supported."
           Resolution:
             Contact manufacturer for upgrade to two dimensional version of the product.
         */
	kERR_STRLEN_ZERO,
	kERR_CLIPBOARD_OPEN,
	kERR_CLIPBOARD_EMPTY,
	kERR_CLIPBOARD_REG_FORMAT,
	kERR_CLIPBOARD_SET_DATA,
	kERR_DATAMATRIX_GEN_EDAC,
        /* Description:
             "Error generating EDAC for Data Matrix."
           Resolution:
             Contact manufacturer.
         */
	kERR_DATAMATRIX_DRAWMODULE,
        /* Description:
             "Error drawing the Data Matrix data modules."
           Resolution:
             Contact manufacturer.
         */
	kERR_DATAMATRIX_FINDER,
        /* Description:
             "Error placing the Data Matrix finder pattern."
           Resolution:
             Contact manufacturer.
         */
	kERR_DATAMATRIX_PLACEDATA,
        /* Description:
             "Error placing the Data Matrix data modules."
           Resolution:
             Contact manufacturer.
         */
	kERR_DATAMATRIX_LOOKAHEAD,
        /* Description:
             "Error determining the encodation mode for the Data Matrix symbol."
           Resolution:
             Contact manufacturer.
         */
	kERR_DATAMATRIX_X12,
        /* Description:
             "Error encoding the data in X12 encodation mode for the Data Matrix symbol."
           Resolution:
             Contact manufacturer.
         */
	kERR_DATAMATRIX_EDIFACT,
        /* Description:
             "Error encoding the data in EDIFACT encodation mode for the Data Matrix symbol."
           Resolution:
             Contact manufacturer.
         */
	kERR_DATAMATRIX_C40_TEXT,
        /* Description:
             "Error encoding the data in C40 or Text encodation mode for the Data Matrix symbol."
           Resolution:
             Contact manufacturer.
         */
	kERR_DATAMATRIX_ENCODE,
        /* Description:
             "Error encoding the data for the Data Matrix symbol."
           Resolution:
             Contact manufacturer.
         */
	kERR_DATAMATRIX_SYMBOL_SIZE,
        /* Description:
             "Error determining the symbol size for the Data Matrix symbol."
           Resolution:
             Decrease the size of the input data.
         */
	kERR_INVALID_ALIGNMENT,
        /* Description:
             "Invalid caption alignment specified.  Check documentation for valid range."
           Resolution:
             Specify a valid caption alignment index. 
         */
    kERR_LAST,                    /* add new ones down here */

};


///////////////////////////////////////////////////////////////////////////
extern "C" int WINAPI EXPORT BC_StrErr(
        int error_num,            // input error number
        char * outstr,            // output error description string
        int nOutMax);             // max size of outstr

#endif                                          
