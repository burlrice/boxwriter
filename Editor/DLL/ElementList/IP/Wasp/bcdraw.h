#ifndef BCDRAW_H
#define BCDRAW_H

/* caption alignment values */
#define kCapAlignOff          0
#define kCapAlignBelowLeft    1
#define kCapAlignBelowCenter  2
#define kCapAlignBelowRight   3
#define kCapAlignAboveLeft    4
#define kCapAlignAboveCenter  5
#define kCapAlignAboveRight   6
                              
/* caption style values */
#define kCapStyleNormal       0
#define kCapStyleBold         1
#define kCapStyleItalic       2
#define kCapStyleUnderline    4
#define kCapStyleStrikeout    8

/* units */
#define kUnitsText        1
#define kUnitsLoMetric    2
#define kUnitsHiMetric    3
#define kUnitsLoEnglish   4
#define kUnitsHiEnglish   5
#define kUnitsTwips       6

/* rotation values */
#define kRotate_0    0
#define kRotate_90   1
#define kRotate_180  2
#define kRotate_270  3
                              
/* structure defining the bar code caption */
typedef struct tagTEXTINFO
{
    LPSTR sText;                // text string
    LPSTR sFaceName;            // Font face name
    int nSize;                  // Font point size 
    int nStyle;                 // style (bold, italics, etc.)
    int nAlignment;             // location of caption
    COLORREF lColor;            // text color
}TEXTINFO;

/* structure defining the bar code */
typedef struct tagBARCODEINFO
{
    LPSTR sBcHrText;            // barcode human-readable text string
    int nSymbology;             // barcode symbology
    int nUnits;                 // units of measure (i.e. .001 inches, .01 mm, etc.)
    float fAutoHeight;          // input auto-size height of entire barcode (including caption)
    float fLeft;                // coordinate position specifying the left (X) of the barcode
    float fTop;                 // coordinate position specifying the top (Y) of the barcode
    float fDrawnWidth;          // [returned] width of entire barcode (including caption)
    float fDrawnHeight;         // [returned] height of entire barcode (including caption) 
    float fBarWidth;            // width of narrow bar
    float fBarHeight;           // height of bar
    int nRatio;                 // wide-to-narrow bar width ratio       
    int nCheckSum;              // checksum character addition
    int nRotation;              // rotation
    int nMagPct;                // magnification percentage relative to 1X (e.g. 100 = 100% = 1X)
    COLORREF crForeColor;       // foreground color (bars)
    COLORREF crBackColor;       // background color (spaces)
    TEXTINFO caption;           // structure containing barcode caption information
}BARCODEINFO;

#ifndef _2DSTRUCTS
#define _2DSTRUCTS
/* structure defining PDF417 specifics */
typedef struct tagPDF417INFO
{
    int nCols;            		// number of columns (1-30)
    int nRows;                	// number of rows (3-90)
    int nErrLevel;              // error correction level (0-8)
    BOOL bTruncate;             // TRUE to not print right row indicator and stop pattern
    int nDataLen;             	// number of characters in input data (if zero, then use strlen())
}PDF417INFO;

/* structure defining MaxiCode specifics */
typedef struct tagMAXICODEINFO
{
    int nMode;            		// mode 2,3,4,or 5
    LPSTR sPostalCode;       	// up to 9 digit postal code (only used in modes 2 and 3)
    LPSTR sCountryCode;       	// 3 digit country code (only used in modes 2 and 3)
    LPSTR sClassOfService;    	// 3 digit class of service (only used in modes 2 and 3)
    int nDataLen;             	// number of characters in input data (if zero, then use strlen())
}MAXICODEINFO;

/* structure defining DataMatrix specifics */
typedef struct tagDATAMATRIXINFO
{
    int nDataLen;             	// number of characters in input data (if zero, then use strlen())
}DATAMATRIXINFO;
#endif

typedef struct tagBCDATA
{
	BARCODEINFO bcInfo;
	PDF417INFO pdfInfo;
	MAXICODEINFO maxiInfo;
	DATAMATRIXINFO dmtxInfo;
}BCDATA;

/* 
 * external prototypes 
 */
extern "C" int WINAPI EXPORT BC_DrawBarCodeExample(
    HDC hDC);                   // device context on which to draw

extern "C" int WINAPI EXPORT BC_DrawBarCode(
    HDC hDC,                    // device context on which to draw
    BARCODEINFO* pBc);          // structure defining attributes of the bar code

extern "C" int WINAPI EXPORT BC_DrawPDF417(
	HDC hDC, 
	BARCODEINFO* pBcInfo,
	PDF417INFO* pPdfInfo);

extern "C" int WINAPI EXPORT BC_DrawMaxiCode(
	HDC hDC, 
	BARCODEINFO* pBcInfo,
	MAXICODEINFO* pMaxiInfo);

extern "C" int WINAPI EXPORT BC_DrawDataMatrix(
	HDC hDC, 
	BARCODEINFO* pBcInfo,
	DATAMATRIXINFO* pDmtxInfo);

extern "C" void WINAPI EXPORT BC_DrawStrErr(
    int nErrCode,               // error number
    LPSTR sErrMsg,              // error description string
    int nCount);                // allocated size (in bytes) of error description string

extern "C" unsigned long WINAPI EXPORT BC_DrawGrid(
    HDC hDC,                    // device context on which to draw
    int nDiv,                   // number of divisions per inch
    COLORREF crDot);            // color of dots

extern "C" unsigned long WINAPI EXPORT BC_WriteBmp(
	LPCTSTR sFileName,			// fully qualified path name of file to write
	LPCTSTR sPrinterName,		// name of printer to get resolution information from
	BCDATA* pBcData);			// bar code data structure

extern "C" int WINAPI EXPORT BCDRAW_VersionMajor();

extern "C" int WINAPI EXPORT BCDRAW_VersionMinor();

extern "C" int WINAPI EXPORT BCDRAW_VersionRevision();

// C++ Only Bar code functions
#ifdef __cplusplus

extern "C" int WINAPI EXPORT BC_DrawBarCodeExample2(
    CDC *pDC );                   // device context on which to draw

extern "C" int WINAPI EXPORT BC_DrawBarCode2(
    CDC *pDC,                    // device context on which to draw
    BARCODEINFO* pBc);          // structure defining attributes of the bar code

extern "C" int WINAPI EXPORT BC_DrawPDF4172(
	CDC *pDC, 
	BARCODEINFO* pBcInfo,
	PDF417INFO* pPdfInfo);

extern "C" int WINAPI EXPORT BC_DrawMaxiCode2(
	CDC *pDC, 
	BARCODEINFO* pBcInfo,
	MAXICODEINFO* pMaxiInfo);

extern "C" int WINAPI EXPORT BC_DrawDataMatrix2(
	CDC *pDC, 
	BARCODEINFO* pBcInfo,
	DATAMATRIXINFO* pDmtxInfo);

extern "C" unsigned long WINAPI EXPORT BC_DrawGrid2(
    CDC *pDC,                    // device context on which to draw
    int nDiv,                   // number of divisions per inch
    COLORREF crDot);            // color of dots

#endif

#endif 
