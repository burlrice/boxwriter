#if !defined(AFX_SYNCDLG_H__01A711FA_6610_4213_941C_5A1B90B86F45__INCLUDED_)
#define AFX_SYNCDLG_H__01A711FA_6610_4213_941C_5A1B90B86F45__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SyncDlg.h : header file
//

#include "ElementApi.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CSyncDlg dialog

namespace FoxjetIpElements
{
	class ELEMENT_API CSyncDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CSyncDlg(DWORD dwDefaults, DWORD dwSync, CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CSyncDlg)
		BOOL	m_bFonts;
		BOOL	m_bMessages;
		//}}AFX_DATA

		BOOL m_bAllHeads;
		BOOL m_bDelete;

		CString m_strPrompt;

		typedef enum
		{
			SYNC_FONTS				= 0x00000001,
			SYNC_MESSAGES			= 0x00000002,
			SYNC_DELETE_MEMSTORE	= 0x00000004,
			SYNC_SHOW_ALLHEADS		= 0x20000000,
			SYNC_SHOW_DELETE		= 0x40000000,
			SYNC_NO_PROMPT			= 0x80000000,
			SYNC_NOTIFY_COMPLETE	= 0x00000010,
		} SYNC_TYPE;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CSyncDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		const DWORD m_dwDefaults;
		const DWORD m_dwSync;

		// Generated message map functions
		//{{AFX_MSG(CSyncDlg)
		virtual BOOL OnInitDialog();
	afx_msg void OnDeleteall();
	//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace FoxjetIpElements

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SYNCDLG_H__01A711FA_6610_4213_941C_5A1B90B86F45__INCLUDED_)
