// BitmapFontDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BitmapFontDlg.h"
#include "List.h"
#include "Color.h"
#include "TemplExt.h"
#include "Debug.h"
#include "Registry.h"
#include "Extern.h"
#include "Utils.h"
#include "Database.h"
#include "Parse.h"

#include "fj_system.h"
#include "fj_printhead.h"
#include "fj_font.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ListGlobals;
using namespace FoxjetIpElements;

extern CArray <LPFJSYSTEM, LPFJSYSTEM> vIpSystems;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define SS_PREVIEW 0

bool CBitmapFontDlg::CompareFonts (const LPVOID pLhs, const LPVOID pRhs)
{
	const LPFJFONT lhs = (LPFJFONT)pLhs;
	const LPFJFONT rhs = (LPFJFONT)pRhs;
	return strncmp (lhs->strName, rhs->strName, FJFONT_NAME_SIZE) == 0;
}

ELEMENT_API bool FoxjetCommon::DoFontDialog (CPrinterFont & f, int & nWidth,
											 const CString & strSampleText, 
											 CWnd * pParent)
{
	CBitmapFontDlg * pDlg = NULL;
	CString strID;
	LPFJPRINTHEAD pHead = NULL;
	ULONG lHeadID = -1;

	strID.Format (_T ("%d"), lHeadID);

	for (int i = 0; i < ::vIpSystems.GetSize (); i++) {
		LPFJSYSTEM pSystem = ::vIpSystems [i];

		for (int j = 0; j < FJSYS_NUMBER_PRINTERS; j++) {
			if (pHead = pSystem->gmMembers [j].pfph) { 
				if (pHead->strID == strID || lHeadID == -1) {
					CBitmapFontDlg dlg (CHead (pHead), f.m_strName, pParent);

					if (strSampleText.GetLength ())
						dlg.m_strSample = strSampleText;
					
					if (dlg.DoModal () == IDOK) {
						for (int i = 0; pHead->papffSelected [i]; i++) {
							LPFJFONT pFont = pHead->papffSelected [i];
							
							ASSERT (pFont);

							if (dlg.m_strFont == pFont->strName) {
								f.m_strName = pFont->strName;
								f.m_nSize = pFont->lHeight;
								f.m_bBold = false;
								f.m_bItalic = false;
								
								return true;
							}
						}
					}

					return false;
				}
			}
		}
	}

	
	return false;
}

/////////////////////////////////////////////////////////////////////////////
// CBitmapFontDlg dialog


CBitmapFontDlg::CBitmapFontDlg(FoxjetCommon::CHead & head, const CString & strFont, CWnd* pParent /*=NULL*/)
:	m_strFont (strFont),
	m_strSample (_T ("AaBbYyZz 123")),
	m_nSampleWidth (3),
	m_pHead (&head),
	m_element (head, head),
	m_bEditFontDir (false),
	m_bAppInitialized (true),
	FoxjetCommon::CEliteDlg(CBitmapFontDlg::IDD, pParent)
{
	m_strFontDirs = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, 
		GetSection (), GetKey (), GetHomeDir () + _T ("\\Fonts"));
}

void CBitmapFontDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBitmapFontDlg)
	//}}AFX_DATA_MAP

	DDX_Text (pDX, TXT_SAMPLE, m_strSample);
	DDX_Text (pDX, TXT_SAMPLEWIDTH, m_nSampleWidth);
}


BEGIN_MESSAGE_MAP(CBitmapFontDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CBitmapFontDlg)
	ON_CBN_SELCHANGE(CB_FONT, OnSelchangeFont)
	ON_BN_CLICKED(BTN_ADD, OnAdd)
	ON_BN_CLICKED(BTN_DELETE, OnDelete)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_LBN_DBLCLK(LB_FONTDIR, OnDblclkDir)
	ON_BN_CLICKED(CHK_FIT, OnFit)
	ON_EN_CHANGE(TXT_SAMPLE, OnChangeSample)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(TXT_SAMPLEWIDTH, OnChangeSample)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBitmapFontDlg message handlers

void CBitmapFontDlg::Refresh ()
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_FONT);
	CListBox * pLB = (CListBox *)GetDlgItem (LB_FONTDIR);
	CStringArray vDirs;
	CArray <LPFJFONT, const LPFJFONT> vFonts;

	ASSERT (pCB);
	ASSERT (pLB);

	CString strFontDirs = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, 
		GetSection (), GetKey (), GetHomeDir () + _T ("\\Fonts"));
	Tokenize (strFontDirs, vDirs, ';');

	pLB->ResetContent ();
	pCB->ResetContent ();

	for (int i = 0; i < vDirs.GetSize (); i++) 
		pLB->AddString (vDirs [i]);

	for (int i = 0; m_pHead->m_pFjHead->papffSelected [i]; i++) {
		if (const LPFJFONT pFont = m_pHead->m_pFjHead->papffSelected [i]) {
			if (Find <LPFJFONT, const LPFJFONT> (vFonts, pFont, CBitmapFontDlg::CompareFonts) == -1) {
				TRACEF (pFont->strName);
				vFonts.Add (pFont);
			}
		}
	}

	for (int i = 0; i < vFonts.GetSize (); i++) {
		const LPFJFONT pFont = vFonts [i];
		int nIndex = pCB->AddString (a2w (pFont->strName));
		pCB->SetItemData (nIndex, (DWORD)pFont);
	}

	if (pCB->SelectString (-1, m_strFont) == CB_ERR)
		pCB->SetCurSel (0);

	OnSelchangeFont ();
}

BOOL CBitmapFontDlg::OnInitDialog() 
{
	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();

	if (CWnd * pPreview = GetDlgItem (IDC_PREVIEW)) {
		CRect rc;
		pPreview->GetWindowRect (&rc);
		pPreview->ShowWindow (SW_HIDE);

		ScreenToClient (&rc);

		VERIFY (m_wndPreview.Create (this, rc, &m_element, &(m_pHead->m_dbHead)));
		InvalidatePreview ();
	}

	Refresh ();

	return bResult;
}

void CBitmapFontDlg::InvalidatePreview()
{
	CDC dcMem;
	CString strFont;
	HEADSTRUCT & head = m_pHead->m_dbHead;

	GetDlgItemText (CB_FONT, strFont);

	dcMem.CreateCompatibleDC (GetDC ());

	m_element.SetFontName (strFont);
	m_element.SetWidth (GetDlgItemInt (TXT_SAMPLEWIDTH));
	m_element.SetDefaultData (m_strSample);
	m_element.Build ();
	m_element.Draw (dcMem, head, true);

	m_wndPreview.Invalidate ();
	m_wndPreview.RedrawWindow ();
	OnFit ();
}

void CBitmapFontDlg::OnSelchangeFont() 
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_FONT);
	ASSERT (pCB);
	int nIndex = pCB->GetCurSel ();

	if (nIndex != CB_ERR) {
		if (LPFJFONT pFont = (LPFJFONT)pCB->GetItemData (nIndex)) {
			SetDlgItemText (TXT_TYPE, GetFontType (pFont->lType));
			SetDlgItemInt (TXT_HEIGHT, pFont->lHeight);
			SetDlgItemInt (TXT_WIDTH, pFont->lWidth);
			SetDlgItemInt (TXT_FIRSTCHAR, pFont->lFirstChar);
			SetDlgItemInt (TXT_LASTCHAR, pFont->lLastChar);
			InvalidatePreview ();
		}
	}
}


CString CBitmapFontDlg::GetFontType(LONG lType)
{
	switch (lType) {
	case FJFONT_BITMAP:		return LoadString (IDS_BITMAP);
	case FJFONT_TRUETYPE:	return LoadString (IDS_TRUETYPE);
	}

	return _T ("");
}

void CBitmapFontDlg::OnAdd() 
{
	int nResult = IDYES;

	if (m_bAppInitialized) {
		CString str = LoadString (IDS_MUSTRESTART) + _T ("\n") + 
			LoadString (IDS_CONTINUE);
		nResult = MsgBox (* this, str, LoadString (IDS_CONFIRM), MB_YESNO);
	}

	if (nResult == IDYES) {
		CFileDialog dlg (TRUE, _T ("*.fjf"), _T (""), 
			OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
			_T ("FoxJet font files (*.fjf)|*.fjf||"), this);

		if (dlg.DoModal () == IDOK) {
			CString strPath = dlg.GetPathName ();
			CListBox * pLB = (CListBox *)GetDlgItem (LB_FONTDIR);

			ASSERT (pLB);
			strPath.Replace (_T ("\\") + dlg.GetFileName (), _T (""));

			for (int i = 0; i < pLB->GetCount (); i++) {
				CString str;
				
				pLB->GetText (i, str);

				if (!str.CompareNoCase (strPath))
					return;
			}

			pLB->AddString (strPath);
		}
	}
}

void CBitmapFontDlg::OnDelete() 
{
	CListBox * pLB = (CListBox *)GetDlgItem (LB_FONTDIR);

	ASSERT (pLB);
	int nIndex = pLB->GetCurSel ();

	if (nIndex == LB_ERR) {
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
		return;
	}

	int nResult = IDYES;

	if (m_bAppInitialized) {
		CString str = LoadString (IDS_MUSTRESTART) + _T ("\n") + 
			LoadString (IDS_CONFIRMELEMENTDELETE);
		nResult = MsgBox (* this, str, LoadString (IDS_CONFIRM), MB_YESNO);
	}

	if (nResult == IDYES) 
		pLB->DeleteString (nIndex);
}

CString CBitmapFontDlg::GetKey () const
{
	return _T ("Path");
}

CString CBitmapFontDlg::GetSection () const
{
	return defElements.m_strRegSection + _T ("\\Fonts");
}

void CBitmapFontDlg::OnEdit() 
{
	CListBox * pLB = (CListBox *)GetDlgItem (LB_FONTDIR);
	CString strPath;
	int nResult = IDYES;

	ASSERT (pLB);
	int nIndex = pLB->GetCurSel ();

	if (nIndex == LB_ERR) {
		MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
		return;
	}

	if (m_bAppInitialized) {
		CString str = LoadString (IDS_MUSTRESTART) + _T ("\n") + 
			LoadString (IDS_CONTINUE);
		nResult = MsgBox (* this, str, LoadString (IDS_CONFIRM), MB_YESNO);
	}

	pLB->GetText (nIndex, strPath);

	if (nResult == IDYES) {
		CFileDialog dlg (TRUE, _T ("*.fjf"), strPath, 
			OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
			_T ("FoxJet font files (*.fjf)|*.fjf||"), this);

		if (dlg.DoModal () == IDOK) {
			strPath = dlg.GetPathName ();
			strPath.Replace (_T ("\\") + dlg.GetFileName (), _T (""));

			for (int i = 0; i < pLB->GetCount (); i++) {
				CString str;
				
				pLB->GetText (i, str);

				if (!str.CompareNoCase (strPath))
					return;
			}

			pLB->DeleteString (nIndex);
			pLB->InsertString (nIndex, strPath);
			pLB->SetCurSel (nIndex);
		}
	}
}

void CBitmapFontDlg::OnDblclkDir() 
{
	OnEdit ();	
}

LRESULT CBitmapFontDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if (::IsWindow (m_hWnd) && ::IsWindowVisible (m_hWnd))
		UpdateUI ();
	
	return FoxjetCommon::CEliteDlg::WindowProc(message, wParam, lParam);
}

void CBitmapFontDlg::UpdateUI()
{
	CButton * pAdd		= (CButton *)GetDlgItem (BTN_ADD);
	CButton * pEdit		= (CButton *)GetDlgItem (BTN_EDIT);
	CButton * pDelete	= (CButton *)GetDlgItem (BTN_DELETE);
	CListBox * pLB		= (CListBox *)GetDlgItem (LB_FONTDIR);

	ASSERT (pAdd);
	ASSERT (pEdit);
	ASSERT (pDelete);
	ASSERT (pLB);

	int nIndex = pLB->GetCurSel ();
	int nCount = pLB->GetCount ();

	pAdd->EnableWindow (m_bEditFontDir);
	pEdit->EnableWindow (m_bEditFontDir && nIndex != CB_ERR);
	pDelete->EnableWindow (m_bEditFontDir && nIndex != CB_ERR && nCount > 1);
}

CString CBitmapFontDlg::GetCurSelDirs () const
{
	CListBox * pLB = (CListBox *)GetDlgItem (LB_FONTDIR);
	CString strFontDirs;
	
	ASSERT (pLB);

	for (int i = 0; i < pLB->GetCount (); i++) {
		CString str;

		pLB->GetText (i, str);
		strFontDirs += str + ';';
	}

	return strFontDirs;
}

void CBitmapFontDlg::OnOK()
{
	if (m_bEditFontDir) {
		FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, GetSection (), 
			GetKey (), GetCurSelDirs ());
		FoxjetCommon::CEliteDlg::OnOK ();
	}
	else {
		CComboBox * pCB = (CComboBox *)GetDlgItem (CB_FONT);
		ASSERT (pCB);
		int nIndex = pCB->GetCurSel ();

		if (nIndex != CB_ERR) {
			pCB->GetLBText (nIndex, m_strFont);
			FoxjetCommon::CEliteDlg::OnOK ();
		}
		else
			MsgBox (* this, LoadString (IDS_NOTHINGSELECTED));
	}
}

void CBitmapFontDlg::OnCancel()
{
	FoxjetCommon::CEliteDlg::OnCancel ();
}

void CBitmapFontDlg::OnFit() 
{
	CButton * pFit = (CButton *)GetDlgItem (CHK_FIT);

	ASSERT (pFit);

	if (pFit->GetCheck () == 1) 
		m_wndPreview.SetZoom (m_wndPreview.CalcFitZoom ());
	else
		m_wndPreview.SetZoom (1.0);
}

void CBitmapFontDlg::OnChangeSample() 
{
	GetDlgItemText (TXT_SAMPLE, m_strSample);
	InvalidatePreview ();
}
