#if !defined(AFX_COUNTDLG_H__62D155B2_42BE_4CE1_9492_454A5B0378B9__INCLUDED_)
#define AFX_COUNTDLG_H__62D155B2_42BE_4CE1_9492_454A5B0378B9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CountDLg.h : header file
//

#include "ElementDlg.h"
#include "CountElement.h"

/////////////////////////////////////////////////////////////////////////////
// CCountDlg dialog

class CCountDlg : public CElementDlg
{
// Construction
public:
	CCountDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa,
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCountDlg)
	enum { IDD = IDD_COUNT };
	BOOL	m_bRightAlign;
	long	m_lBold;
	long	m_lChange;
	long	m_lCurRepeat;
	long	m_lCurValue;
	CString m_strFill;
	long	m_lGap;
	long	m_lLimit;
	long	m_lRepeat;
	long	m_lStart;
	long	m_lWidth;
	long	m_lLength;
	//}}AFX_DATA

	CString m_strFont;

	virtual UINT GetDefCtrlID () const { return TXT_LENGTH; }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCountDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	static int CalcMaxLen (DWORD dwMax);

	virtual BOOL OnInitDialog();
	virtual int BuildElement ();
	FoxjetIpElements::CCountElement & GetElement ();
	const FoxjetIpElements::CCountElement & GetElement () const;

	// Generated message map functions
	//{{AFX_MSG(CCountDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COUNTDLG_H__62D155B2_42BE_4CE1_9492_454A5B0378B9__INCLUDED_)
