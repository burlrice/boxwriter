// BarcodeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "BarcodeDlg.h"
#include "BarCode\Barcode.h"
#include "fj_export.h"
#include "fj_system.h"
#include "BarcodeParamDlg.h"
#include "Coord.h"
#include "Registry.h"
#include "Extern.h"
#include "Edit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetIpElements;

/////////////////////////////////////////////////////////////////////////////
// CBarcodeDlg dialog


CBarcodeDlg::CBarcodeDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa,
						 const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_bOutputText (FALSE),
	m_dBold (1),
	m_dTextBold (1),
	m_dWidth (1),
	m_nType (0),
	m_cAlign ('C'),
	CElementDlg(e, pa, CBarcodeDlg::IDD, pList, pParent)
{
	//{{AFX_DATA_INIT(CBarcodeDlg)
	//}}AFX_DATA_INIT
}


void CBarcodeDlg::DoDataExchange(CDataExchange* pDX)
{
	CElementDlg::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CBarcodeDlg)
	DDX_Check(pDX, CHK_ASCII, m_bOutputText);
	//}}AFX_DATA_MAP

	DDX_Text(pDX, TXT_BOLD, m_dBold);
	DDV_MinMaxInt (pDX, m_dBold, CBarcodeElement::m_lmtBold.m_dwMin, CBarcodeElement::m_lmtBold.m_dwMax); 

	DDX_Text(pDX, TXT_TEXT, m_strText);
	DDV_MinMaxChars (pDX, m_strText, TXT_TEXT, CBarcodeElement::m_lmtString); 
	
	DDX_Text(pDX, TXT_TEXTBOLD, m_dTextBold);
	DDV_MinMaxInt (pDX, m_dTextBold, CBarcodeElement::m_lmtBold.m_dwMin, CBarcodeElement::m_lmtBold.m_dwMax); 
	
	DDX_Text (pDX, TXT_WIDTH, m_dWidth);
	DDV_MinMaxInt (pDX, m_dWidth, CBarcodeElement::m_lmtWidth.m_dwMin, CBarcodeElement::m_lmtWidth.m_dwMax); 
}


BEGIN_MESSAGE_MAP(CBarcodeDlg, CElementDlg)
	//{{AFX_MSG_MAP(CBarcodeDlg)
	ON_BN_CLICKED(CHK_ASCII, OnAscii)
	ON_CBN_SELCHANGE(CB_TYPE, OnSelchangeType)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(CHK_EPS, InvalidatePreview)
	ON_BN_CLICKED(CHK_PS, InvalidatePreview)
	ON_BN_CLICKED(CHK_NOHEADERS, InvalidatePreview)
	ON_BN_CLICKED(CHK_CHECKSUM, InvalidatePreview)
	ON_BN_CLICKED(CHK_CORRECT, InvalidatePreview)
    ON_CBN_SELCHANGE(CB_ALIGNMENT, InvalidatePreview)
	ON_EN_CHANGE(TXT_TEXT, InvalidatePreview)
	ON_EN_CHANGE(TXT_TEXTBOLD, InvalidatePreview)
	ON_EN_CHANGE(TXT_BOLD, InvalidatePreview)
	ON_EN_CHANGE(TXT_WIDTH, InvalidatePreview)
	ON_EN_CHANGE(TXT_TEXTBOLD, InvalidatePreview)
	ON_BN_CLICKED(BTN_PARAMS, OnEditParams)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBarcodeDlg message handlers
CBarcodeElement & CBarcodeDlg::GetElement ()
{
	CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CBarcodeElement)));
	return (CBarcodeElement &)e;
}

const CBarcodeElement & CBarcodeDlg::GetElement () const
{
	const CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CBarcodeElement)));
	return (CBarcodeElement &)e;
}

int CBarcodeDlg::BuildElement ()
{
	CBarcodeElement & e = GetElement ();
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	CComboBox * pAlign = (CComboBox *)GetDlgItem (CB_ALIGNMENT);
	int nType = BARCODE_ANY;
	CHAR cAlign = 'C';
	int nIndex;
	int nResult = -1;

	ASSERT (pType);
	ASSERT (pAlign);

	if ((nIndex = pType->GetCurSel ()) != CB_ERR) 
		nType = (int)pType->GetItemData (nIndex);

	if ((nIndex = pAlign->GetCurSel ()) != CB_ERR) 
		cAlign = (CHAR)pAlign->GetItemData (nIndex);

	e.SetFlippedH (GetCheck (CHK_FLIPH));
	e.SetFlippedV (GetCheck (CHK_FLIPV));
	e.SetInverse (GetCheck (CHK_INVERSE));

	e.SetOutputText (GetCheck (CHK_ASCII));
	e.SetBold (_ttol (GetCtrlText (TXT_BOLD)));
	e.SetText (GetCtrlText (TXT_TEXT));
	e.SetTextBold (_ttol (GetCtrlText (TXT_TEXTBOLD)));
	e.SetWidth (_ttol (GetCtrlText (TXT_WIDTH)));

	e.SetBcType (nType);
	e.SetTextAlign (cAlign);

	nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

void CBarcodeDlg::OnAscii() 
{
	bool bText = GetCheck (CHK_ASCII);

	ASSERT (GetDlgItem (CB_ALIGNMENT));
	ASSERT (GetDlgItem (TXT_TEXTBOLD));

	GetDlgItem (CB_ALIGNMENT)->EnableWindow (bText);
	GetDlgItem (TXT_TEXTBOLD)->EnableWindow (bText);

	InvalidatePreview ();
}

BOOL CBarcodeDlg::OnInitDialog() 
{
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	CComboBox * pAlign = (CComboBox *)GetDlgItem (CB_ALIGNMENT);
	struct 
	{
		CHAR m_c;
		UINT m_nID;
	} static const alignmap [] = 
	{
		{ 'L',	IDS_LEFT	},
		{ 'R',	IDS_RIGHT	},
		{ 'C',	IDS_CENTER	},
		{ 0,	-1			},
	};

	ASSERT (pType);
	ASSERT (pAlign);

	BOOL bResult = CElementDlg::OnInitDialog();
	
	for (int i = 0; i < FJSYS_BARCODES; i++) {
		const FJSYSBARCODE & bc = GetElement ().GetSystem ()->bcArray [i];
		int nIndex = pType->AddString (a2w (bc.strName));
		pType->SetItemData (nIndex, i);

		if (m_nType == i)
			pType->SetCurSel (i);
	}

	for (int i = 0; alignmap [i].m_nID != -1; i++) {
		int nIndex = pAlign->AddString (LoadString (alignmap [i].m_nID));
		pAlign->SetItemData (nIndex, alignmap [i].m_c);

		if (alignmap [i].m_c == m_cAlign)
			pAlign->SetCurSel (nIndex);
	}

	if (pType->GetCurSel () == CB_ERR)
		pType->SetCurSel (0);

	if (pAlign->GetCurSel () == CB_ERR)
		pAlign->SetCurSel (0);

	OnAscii ();
	OnSelchangeType ();

	return bResult;
}

void CBarcodeDlg::OnOK()
{
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	CComboBox * pAlign = (CComboBox *)GetDlgItem (CB_ALIGNMENT);
	int nIndex;

	ASSERT (pType);
	ASSERT (pAlign);

	if ((nIndex = pType->GetCurSel ()) == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOTYPESELECTED));
		return;
	}
	else
		m_nType = (int)pType->GetItemData (nIndex);

	if ((nIndex = pAlign->GetCurSel ()) == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOALIGNMENTSELECTED));
		return;
	}
	else
		m_cAlign = (CHAR)pAlign->GetItemData (nIndex);

	if (!GetElement ().Validate ())
		MsgBox (* this, LoadString (IDS_INVALIDDATA));
	else
		CElementDlg::OnOK ();
}


void CBarcodeDlg::OnSelchangeType() 
{
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	LPFJSYSTEM pSystem = GetElement ().GetSystem ();

	ASSERT (pType);
	
	int nIndex = (int)pType->GetItemData (nIndex = pType->GetCurSel ());
	CString str = CBarcodeElement::GetBcType (pSystem, nIndex);

	SetDlgItemText (TXT_SYMBOLOGY, str);
	InvalidatePreview ();
}

void CBarcodeDlg::OnEditParams ()
{
	const CString strSection = ListGlobals::defElements.m_strRegSection + _T ("\\CSystemBarcodeDlg");
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);

	ASSERT (pType);

	int nType = (int)pType->GetItemData (pType->GetCurSel ());

	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("LineID"),		GetLineID ());
	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("HeadType"),	GetHead ().m_nHeadType);
	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("nType"),		nType);

	FoxjetIpElements::OnDefineGlobalBarcodes (this, verApp);

	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("nType"),	-1);
}