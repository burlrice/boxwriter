#include "stdafx.h"
#include "List.h"

using namespace FoxjetCommon;


/*** begin: stolen code ***/
// this code was copied from the dibapi & dibutil samples in msdn
#define BFT_BITMAP 0x4d42   /* 'BM' */
#define IS_WIN30_DIB(lpbi)  ((*(LPDWORD)(lpbi)) == sizeof(BITMAPINFOHEADER)) 
#define WIDTHBYTES(bits)    (((bits) + 31) / 32 * 4) 

DECLARE_HANDLE(HDIB); 

static bool WriteDIB (const CString & strFile, HDIB hdib);
static HANDLE DibFromBitmap (HBITMAP hbm, DWORD biStyle, WORD biBits, 
							 HPALETTE hpal); 
static WORD WINAPI PaletteSize(LPSTR lpbi);
static WORD WINAPI DIBNumColors(LPSTR lpbi); 
/*** end: stolen code ***/

namespace FoxjetCommon {
	ELEMENT_API bool SaveBitmap(HBITMAP hBmp, const CString & strFile)
	{
		// this code was copied from the dibapi & dibutil samples in msdn
		HDIB hdibCurrent = (HDIB)DibFromBitmap (hBmp, BI_RGB, 0, NULL);

		
		CWinApp * pApp = ::AfxGetApp ();
		
		if (pApp)
			pApp->BeginWaitCursor ();

		BOOL bWriteDIB = WriteDIB (strFile, hdibCurrent);
		
		if (pApp)
			pApp->EndWaitCursor ();

		::GlobalFree (hdibCurrent);

		return bWriteDIB ? true : false;
	}
}

/*** begin: stolen code ***/
static bool WriteDIB (const CString & strFile, HDIB hdib)
{
    if (!hdib)
        return FALSE;

	if (FILE * fp = _tfopen (strFile, _T ("w"))) {
		BITMAPFILEHEADER    hdr;
		LPBITMAPINFOHEADER  lpbi;

		lpbi = (BITMAPINFOHEADER *)::GlobalLock (hdib);

		// Fill in the fields of the file header 
		hdr.bfType      = BFT_BITMAP;
		hdr.bfSize      = GlobalSize (hdib) + sizeof (BITMAPFILEHEADER);
		hdr.bfReserved1 = 0;
		hdr.bfReserved2 = 0;
		hdr.bfOffBits   = (DWORD)sizeof (BITMAPFILEHEADER) + lpbi->biSize + ::PaletteSize ((LPSTR)lpbi);

		// Write the file header 
		fwrite ((LPSTR)&hdr, sizeof (BITMAPFILEHEADER), 1, fp);

		// Write the DIB header and the bits 
		fwrite ((LPSTR)lpbi, GlobalSize (hdib), 1, fp);

		::GlobalUnlock (hdib);
		fclose (fp);
		return true;
	}

    return false;
}

static HANDLE DibFromBitmap (HBITMAP hbm, DWORD biStyle, WORD biBits, 
							 HPALETTE hpal) 
{
    BITMAP bm;
    BITMAPINFOHEADER bi;
    BITMAPINFOHEADER FAR *lpbi;
    DWORD dwLen;
    HANDLE hdib;
    HANDLE h;
    HDC hdc;

    if (!hbm)
        return NULL;

    if (hpal == NULL)
        hpal = (HPALETTE)GetStockObject(DEFAULT_PALETTE);

	::ZeroMemory (&bm, sizeof (bm));
    VERIFY (GetObject(hbm,sizeof(bm),(LPSTR)&bm));

    if (biBits == 0)
        biBits =  bm.bmPlanes * bm.bmBitsPixel;

    bi.biSize           = sizeof(BITMAPINFOHEADER);
    bi.biWidth          = bm.bmWidth;
    bi.biHeight         = bm.bmHeight;
    bi.biPlanes         = 1;
    bi.biBitCount       = biBits;
    bi.biCompression    = biStyle;
    bi.biSizeImage      = 0;
    bi.biXPelsPerMeter  = 0;
    bi.biYPelsPerMeter  = 0;
    bi.biClrUsed        = 0;
    bi.biClrImportant   = 0;

    dwLen  = bi.biSize + PaletteSize((LPSTR)&bi);

    hdc = GetDC(NULL);
    hpal = SelectPalette(hdc,hpal,FALSE);
    RealizePalette(hdc);

    hdib = GlobalAlloc(GHND,dwLen);

    if (!hdib)
    {
        SelectPalette(hdc,hpal,FALSE);
        ReleaseDC(NULL,hdc);
        return NULL;
    }

    lpbi = (BITMAPINFOHEADER *)GlobalLock(hdib);

    *lpbi = bi;

    /*  call GetDIBits with a NULL lpBits param, so it will calculate the
     *  biSizeImage field for us
     */
    GetDIBits(hdc, hbm, 0, (WORD)bi.biHeight,
    NULL, (LPBITMAPINFO)lpbi, DIB_RGB_COLORS);

    bi = *lpbi;
    GlobalUnlock(hdib);

    /* If the driver did not fill in the biSizeImage field, make one up */
    if (bi.biSizeImage == 0)
    {
        bi.biSizeImage = WIDTHBYTES((DWORD)bm.bmWidth * biBits) * bm.bmHeight;

        if (biStyle != BI_RGB)
            bi.biSizeImage = (bi.biSizeImage * 3) / 2;
    }

    /*  realloc the buffer big enough to hold all the bits */
    dwLen = bi.biSize + PaletteSize((LPSTR)&bi) + bi.biSizeImage;
    if (h = GlobalReAlloc(hdib,dwLen,0))
        hdib = h;
    else
    {
        GlobalFree(hdib);
        hdib = NULL;

        SelectPalette(hdc,hpal,FALSE);
        ReleaseDC(NULL,hdc);
        return hdib;
    }

    /*  call GetDIBits with a NON-NULL lpBits param, and actualy get the
     *  bits this time
     */
    lpbi = (BITMAPINFOHEADER *)GlobalLock(hdib);

    if (GetDIBits( hdc,
        hbm,
        0,
        (WORD)bi.biHeight,
        (LPSTR)lpbi + (WORD)lpbi->biSize + PaletteSize((LPSTR)lpbi),
        (LPBITMAPINFO)lpbi, DIB_RGB_COLORS) == 0)
    {
        GlobalUnlock(hdib);
        hdib = NULL;
        SelectPalette(hdc,hpal,FALSE);
        ReleaseDC(NULL,hdc);
        return NULL;
    }

    bi = *lpbi;
    GlobalUnlock(hdib);

    SelectPalette(hdc,hpal,FALSE);
    ReleaseDC(NULL,hdc);
    return hdib;
}

static WORD WINAPI PaletteSize(LPSTR lpbi) 
{ 
   /* calculate the size required by the palette */ 
   if (IS_WIN30_DIB (lpbi)) 
      return (WORD)(::DIBNumColors(lpbi) * sizeof(RGBQUAD)); 
   else 
      return (WORD)(::DIBNumColors(lpbi) * sizeof(RGBTRIPLE)); 
} 

static WORD WINAPI DIBNumColors(LPSTR lpbi) 
{ 
    WORD wBitCount;  // DIB bit count 

    /*  If this is a Windows-style DIB, the number of colors in the 
     *  color table can be less than the number of bits per pixel 
     *  allows for (i.e. lpbi->biClrUsed can be set to some value). 
     *  If this is the case, return the appropriate value. 
     */ 

    if (IS_WIN30_DIB(lpbi)) 
    { 
        DWORD dwClrUsed; 

        dwClrUsed = ((LPBITMAPINFOHEADER)lpbi)->biClrUsed; 
        if (dwClrUsed != 0) 
            return (WORD)dwClrUsed; 
    } 

    /*  Calculate the number of colors in the color table based on 
     *  the number of bits per pixel for the DIB. 
     */ 
    if (IS_WIN30_DIB(lpbi)) 
        wBitCount = ((LPBITMAPINFOHEADER)lpbi)->biBitCount; 
    else 
        wBitCount = ((LPBITMAPCOREHEADER)lpbi)->bcBitCount; 

    /* return number of colors based on bits per pixel */ 
    switch (wBitCount) 
    { 
        case 1:		return 2; 
        case 4:		return 16; 
        case 8:		return 256; 
        default:	return 0; 
    } 
} 
/*** end: stolen code ***/
