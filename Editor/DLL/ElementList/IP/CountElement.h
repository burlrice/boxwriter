// CountElement.h: interface for the CCountElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COUNTELEMENT_H__D6F81C88_5EE6_4EF9_B190_D160961986E3__INCLUDED_)
#define AFX_COUNTELEMENT_H__D6F81C88_5EE6_4EF9_B190_D160961986E3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IpBaseElement.h"
#include "fj_element.h"

FORWARD_DECLARE (fjtext, FJTEXT)
FORWARD_DECLARE (fjcounter, FJCOUNTER)

namespace FoxjetIpElements
{
	class ELEMENT_API CCountElement : public CIpBaseElement  
	{
		DECLARE_DYNAMIC (CCountElement);
	public:
		CCountElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CCountElement (const CCountElement & rhs);
		CCountElement & operator = (const CCountElement & rhs);
		virtual ~CCountElement ();

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual CString GetImageData () const;
		virtual CString GetDefaultData () const;
		virtual void CreateImage ();
		virtual CString GetElementFontName () const;
		virtual bool SetFont (const FoxjetCommon::CPrinterFont & f);

		virtual const FoxjetCommon::ElementFields::FIELDSTRUCT * GetFieldBuffer () const;

		DECLARE_MEMBERPOINTERACCESS (LPFJCOUNTER, GetElement ()->pDesc)

		virtual LONG GetBold () const;
		virtual bool SetBold (LONG lValue);

		virtual int GetWidth () const;
		virtual bool SetWidth (int nValue);
		
		LONG GetGap () const;
		bool SetGap (LONG lValue);

		LONG GetChange () const;
		bool SetChange (LONG l);

		LONG GetRepeat () const;
		bool SetRepeat (LONG l);

		LONG GetStart () const;
		bool SetStart (LONG l);

		LONG GetLimit () const;
		bool SetLimit (LONG l);

		LONG GetCurValue () const;
		bool SetCurValue (LONG l);

		LONG GetCurRepeat () const;
		bool SetCurRepeat (LONG l);

		LONG GetLength () const;
		bool SetLength (LONG l);

		CHAR GetAlign () const;
		bool SetAlign (CHAR c);

		CHAR GetFill () const;
		bool SetFill (CHAR c);

		static const FoxjetCommon::LIMITSTRUCT m_lmtRepeat;
		static const FoxjetCommon::LIMITSTRUCT m_lmtLimit;
		static const FoxjetCommon::LIMITSTRUCT m_lmtStart;
		static const FoxjetCommon::LIMITSTRUCT m_lmtLength;
		static const FoxjetCommon::LIMITSTRUCT m_lmtChange;
		static const FoxjetCommon::LIMITSTRUCT m_lmtCurValue;
		static const FoxjetCommon::LIMITSTRUCT m_lmtCurRepeat;

	protected:
		LPFJTEXT GetText ();
		CLPFJTEXT GetText () const;
	};
}; //namespace FoxjetIpElements

#endif // !defined(AFX_COUNTELEMENT_H__D6F81C88_5EE6_4EF9_B190_D160961986E3__INCLUDED_)
