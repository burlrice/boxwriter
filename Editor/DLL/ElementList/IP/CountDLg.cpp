// CountDLg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "CountElement.h"
#include "CountDlg.h"
#include "fj_element.h"
#include "fj_counter.h"
#include "fj_text.h"
#include "fj_printhead.h"
#include "fj_font.h"
#include "fj_export.h"
#include "Resource.h"

using namespace FoxjetCommon;
using namespace FoxjetIpElements;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCountDlg dialog


CCountDlg::CCountDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa,
					 const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	CElementDlg(e, pa, CCountDlg::IDD, pList, pParent)
{
	const CCountElement & element = GetElement ();
	const LPFJCOUNTER p = element.GetMember ();
	ASSERT (p);

	m_lBold			= p->lBoldValue;
	m_lWidth		= p->lWidthValue;
	m_lGap			= p->lGapValue;
	m_strFont		= p->strFontName;
	m_bRightAlign	= p->cAlign == 'R' ? TRUE : FALSE;
	m_strFill		= CString (p->cFill, 1);
	m_lLength		= p->lLength;
	m_lChange		= p->lChange;
	m_lCurRepeat	= p->lCurRepeat;
	m_lCurValue		= p->lCurValue;
	m_lLimit		= p->lLimit;
	m_lRepeat		= p->lRepeat;
	m_lStart		= p->lStart;
}


void CCountDlg::DoDataExchange(CDataExchange* pDX)
{
	CElementDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCountDlg)
	//}}AFX_DATA_MAP

	DDX_Check (pDX, CHK_RIGHTALIGN, m_bRightAlign);
	
	DDX_Text(pDX, TXT_CHANGE, m_lChange);
	DDV_MinMaxInt (pDX, m_lChange, CCountElement::m_lmtChange.m_dwMin, CCountElement::m_lmtChange.m_dwMax); 
	
	DDX_Text(pDX, TXT_CURREPEAT, m_lCurRepeat);
	DDV_MinMaxInt (pDX, m_lCurRepeat, CCountElement::m_lmtCurRepeat.m_dwMin, CCountElement::m_lmtCurRepeat.m_dwMax); 
	
	DDX_Text(pDX, TXT_CURVALUE, m_lCurValue);
	DDV_MinMaxInt (pDX, m_lCurValue, CCountElement::m_lmtCurValue.m_dwMin, CCountElement::m_lmtCurValue.m_dwMax); 
	
	DDX_Text(pDX, TXT_FILL, m_strFill);
	
	DDX_Text(pDX, TXT_LIMIT, m_lLimit);
	DDV_MinMaxInt (pDX, m_lLimit, CCountElement::m_lmtLimit.m_dwMin, CCountElement::m_lmtLimit.m_dwMax); 
	
	DDX_Text(pDX, TXT_REPEAT, m_lRepeat);
	DDV_MinMaxInt (pDX, m_lRepeat, CCountElement::m_lmtRepeat.m_dwMin, CCountElement::m_lmtRepeat.m_dwMax); 
	
	DDX_Text(pDX, TXT_START, m_lStart);
	DDV_MinMaxInt (pDX, m_lStart, CCountElement::m_lmtStart.m_dwMin, CCountElement::m_lmtStart.m_dwMax); 

	DDX_CBString (pDX, CB_FONT, m_strFont);

	DDX_Text (pDX, TXT_BOLD, m_lBold);
	DDV_MinMaxInt (pDX, m_lBold, CCountElement::m_lmtBold.m_dwMin, CCountElement::m_lmtBold.m_dwMax); 

	DDX_Text (pDX, TXT_WIDTH, m_lWidth);
	DDV_MinMaxInt (pDX, m_lWidth, CCountElement::m_lmtWidth.m_dwMin, CCountElement::m_lmtWidth.m_dwMax); 

	DDX_Text (pDX, TXT_GAP, m_lGap);
	DDV_MinMaxInt (pDX, m_lGap, CCountElement::m_lmtGap.m_dwMin, CCountElement::m_lmtGap.m_dwMax); 

	DDX_Text(pDX, TXT_LENGTH, m_lLength);
	DDV_MinMaxInt (pDX, m_lLength, CCountElement::m_lmtLength.m_dwMin, CCountElement::m_lmtLength.m_dwMax); 
}


BEGIN_MESSAGE_MAP(CCountDlg, CElementDlg)
	//{{AFX_MSG_MAP(CCountDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(TXT_CHANGE, InvalidatePreview)
	ON_EN_CHANGE(TXT_REPEAT, InvalidatePreview)
	ON_EN_CHANGE(TXT_START, InvalidatePreview)
	ON_EN_CHANGE(TXT_LIMIT, InvalidatePreview)
	ON_EN_CHANGE(TXT_CURVALUE, InvalidatePreview)
	ON_EN_CHANGE(TXT_CURREPEAT, InvalidatePreview)
	ON_EN_CHANGE(TXT_LENGTH, InvalidatePreview)
	ON_EN_CHANGE(TXT_FILL, InvalidatePreview)
	ON_BN_CLICKED(CHK_RIGHTALIGN, InvalidatePreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCountDlg message handlers

BOOL CCountDlg::OnInitDialog()
{
	CComboBox * pFont = (CComboBox *)GetDlgItem (CB_FONT);

	ASSERT (pFont);

	BOOL bResult = CElementDlg::OnInitDialog();

	CArray <CPrinterFont, CPrinterFont &> vFonts;
	const CHead & head = GetList ()->GetHead (0);
	head.GetFonts (vFonts);

	for (int i = 0; i < vFonts.GetSize (); i++) {
		CString str = vFonts [i].m_strName;
		int nIndex = pFont->AddString (str);

		if (str == m_strFont)
			pFont->SetCurSel (nIndex);
	}

	return bResult;
}

int CCountDlg::BuildElement ()
{
	CCountElement & e = GetElement ();
	CComboBox * pFormat = (CComboBox *)GetDlgItem (CB_FORMAT);
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	CString str, strFont;
	TCHAR cFill = 0;

	GetDlgItemText (TXT_FILL, str);

	if (str.GetLength ())
		cFill = str [0];

	ASSERT (pFormat);
	ASSERT (pType);

	GetDlgItemText (CB_FONT, strFont);

	e.SetFontName (strFont);
	e.SetBold (GetDlgItemInt (TXT_BOLD));
	e.SetGap (GetDlgItemInt (TXT_GAP));
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));
	e.SetFlippedH (GetCheck (CHK_FLIPH));
	e.SetFlippedV (GetCheck (CHK_FLIPV));
	e.SetInverse (GetCheck (CHK_INVERSE));
	e.SetAlign (GetCheck (CHK_RIGHTALIGN) ? 'R' : 'L');
	e.SetFill ((char)cFill);
	e.SetLength	 (GetDlgItemInt (TXT_LENGTH));
	e.SetChange (GetDlgItemInt (TXT_CHANGE));
	e.SetCurRepeat (GetDlgItemInt (TXT_CURREPEAT));
	e.SetCurValue (GetDlgItemInt (TXT_CURVALUE));
	e.SetLimit (GetDlgItemInt (TXT_LIMIT));
	e.SetRepeat (GetDlgItemInt (TXT_REPEAT));
	e.SetStart (GetDlgItemInt (TXT_START));

	int nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

CCountElement & CCountDlg::GetElement ()
{
	CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CCountElement)));
	return (CCountElement &)e;
}

const CCountElement & CCountDlg::GetElement () const
{
	const CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CCountElement)));
	return (CCountElement &)e;
}
