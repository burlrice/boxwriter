#if !defined(AFX_LABELDLG_H__BEB7CE40_C108_423E_9B9F_169EE487B1AF__INCLUDED_)
#define AFX_LABELDLG_H__BEB7CE40_C108_423E_9B9F_169EE487B1AF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LabelDlg.h : header file
//

#include "Resource.h"
#include "fj_label.h"
#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CLabelDlg dialog

class CLabelDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CLabelDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLabelDlg)
	enum { IDD = IDD_LABEL };
	BOOL	m_bRound;
	long	m_lValue;
	//}}AFX_DATA

	FJLABELEXP m_lUnits;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLabelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();

	// Generated message map functions
	//{{AFX_MSG(CLabelDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LABELDLG_H__BEB7CE40_C108_423E_9B9F_169EE487B1AF__INCLUDED_)

