#if !defined(AFX_TIMEDLG_H__D4AF9651_5F43_4FED_B298_9BE54A62DA0F__INCLUDED_)
#define AFX_TIMEDLG_H__D4AF9651_5F43_4FED_B298_9BE54A62DA0F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// timedlg.h : header file
//

#include "Resource.h"
#include "ElementApi.h"
#include "Utils.h"

FORWARD_DECLARE (fjsystem, FJSYSTEM)

/////////////////////////////////////////////////////////////////////////////
// CTimeDlg dialog

namespace FoxjetIpElements
{
	namespace TimeDlg
	{
		class ELEMENT_API CShiftCode
		{
		public:
			CShiftCode (LPFJSYSTEM pSystem = NULL);
			CShiftCode (const CShiftCode & rhs);
			CShiftCode & operator = (const CShiftCode & rhs);
			virtual ~CShiftCode ();

			ULONG			m_lLineID;
			COleDateTime	m_dtShift1;
			COleDateTime	m_dtShift2;
			COleDateTime	m_dtShift3;
			CString			m_strShiftCode1;
			CString			m_strShiftCode2;
			CString			m_strShiftCode3;
		};

		class ELEMENT_API CTimeDlg : public FoxjetCommon::CEliteDlg
		{
		// Construction
		public:
			virtual void OnOK();
			CTimeDlg(CWnd* pParent = NULL);   // standard constructor

		// Dialog Data
			//{{AFX_DATA(CTimeDlg)
			enum { IDD = IDD_TIME };
			//}}AFX_DATA

			ULONG m_lLineID;

		// Overrides
			// ClassWizard generated virtual function overrides
			//{{AFX_VIRTUAL(CTimeDlg)
			protected:
			virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
			//}}AFX_VIRTUAL

			static const FoxjetCommon::LIMITSTRUCT m_lmtShift;

		// Implementation
		protected:
			CShiftCode GetCurSelShiftCode () const;
			bool SetCurSelShiftCode (const CShiftCode & s);
			bool UpdateSystem (LPFJSYSTEM pSystem, const CShiftCode & code);
			void InitShiftCodes ();
			static ULONG TimeToLong (const COleDateTime & tm);

			CArray <CShiftCode, CShiftCode &> m_vCodes;

			// Generated message map functions
			//{{AFX_MSG(CTimeDlg)
			virtual BOOL OnInitDialog();
			afx_msg void OnSelchangeLine();
			afx_msg void OnApply();
			afx_msg void OnDatetimechangeShift(NMHDR* pNMHDR, LRESULT* pResult);
		//}}AFX_MSG

			afx_msg void EnableApply ();

			DECLARE_MESSAGE_MAP()
		};
	}; //namespace TimeDlg
}; //namespace FoxjetIpElements

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TIMEDLG_H__D4AF9651_5F43_4FED_B298_9BE54A62DA0F__INCLUDED_)
