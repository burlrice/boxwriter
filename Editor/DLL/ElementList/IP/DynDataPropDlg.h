#if !defined(AFX_DYNDATAPROPDLG_H__4553FF94_4165_4EE8_A8C3_B750ACD2362B__INCLUDED_)
#define AFX_DYNDATAPROPDLG_H__4553FF94_4165_4EE8_A8C3_B750ACD2362B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DynDataPropDlg.h : header file
//

#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CDynDataPropDlg dialog

namespace FoxjetIpElements
{
	class CDatabaseParams
	{
	public:
		CDatabaseParams ();
		CDatabaseParams (const CDatabaseParams & rhs);
		CDatabaseParams & operator = (const CDatabaseParams & rhs);
		virtual ~CDatabaseParams ();

		CString m_strDSN;
		CString m_strTable;
		CString m_strField;
		CString m_strKeyField;
		CString m_strKeyValue;
		int m_nRow;
		bool m_bSQL;
		CString m_strSQL;
		int m_nSQLType;
	};

	class ELEMENT_API CDynDataPropDlg : public FoxjetCommon::CEliteDlg
	{
	public:
	// Construction

	public:
		CDynDataPropDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CDynDataPropDlg)
		long	m_lID;
		BOOL	m_bPrompt;
		CString	m_strDefData;
		CString	m_strPrompt;
		//}}AFX_DATA

		BOOL			m_bUseDatabase;
		CDatabaseParams m_params;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CDynDataPropDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual BOOL OnInitDialog();

		afx_msg void OnDatabase ();
		afx_msg void OnCheckDatabase ();

		// Generated message map functions
		//{{AFX_MSG(CDynDataPropDlg)
			// NOTE: the ClassWizard will add member functions here
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace FoxjetIpElements


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DYNDATAPROPDLG_H__4553FF94_4165_4EE8_A8C3_B750ACD2362B__INCLUDED_)
