// ElementDlg.cpp : implementation file
//

#include "stdafx.h"

#include <math.h>

#include "ElementDlg.h"
#include "Coord.h"
#include "Color.h"
#include "Debug.h"
#include "TextElement.h"
#include "TemplExt.h"
#include "Utils.h"
#include "Extern.h"

using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define SS_PREVIEW 100

void DDV_ID (CDataExchange * pDX, UINT nElementID, const CBaseElement & e, const CElementList & v, UINT nCtrlID)
{
	if (pDX->m_bSaveAndValidate) {
		int nFind = v.FindID (nElementID);

		if (e.GetID () != nElementID && nFind != -1) {
			CString str;
			str.Format (LoadString (IDS_IDEXISTS), nElementID);
			MsgBox (* pDX->m_pDlgWnd, str);
			pDX->PrepareEditCtrl (nCtrlID);
			pDX->Fail ();
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CElementDlg dialog

CElementDlg::CElementDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
						 UINT nDlgID, const FoxjetCommon::CElementList * pList,
						 CWnd * pParent)
:	m_pt (0, 0), 
	m_bReadOnly (false), 
	m_bReadOnlyID (false), 
	m_bReadOnlyLocation (false), 
	m_pa (pa), 
	m_pElement (CopyElement (&e, NULL, false)), 
	m_nID (0),
	m_pList (pList),
	m_bFlipH (e.IsFlippedH ()),
	m_bFlipV (e.IsFlippedV ()),
	m_bInverse (e.IsInverse ()),
	m_bPreview (true),
	m_lLineID (-1),
	m_pAllLists (NULL),
	m_bSetDefault (false),
	FoxjetCommon::CEliteDlg (nDlgID, pParent)
{
	ASSERT (m_pElement);

	const HEADSTRUCT & h = GetHead ();
	PANELSTRUCT p;

	if (GetPanelRecord (GetDB (), h.m_lPanelID, p))
		m_lLineID = p.m_lLineID;
}

CElementDlg::~CElementDlg ()
{
	delete m_pElement;
}

void CElementDlg::DoDataExchange(CDataExchange* pDX)
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();

	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	
	DDX_Coord (pDX, TXT_LOCATIONX, m_pt.x, m_units, head, true);
	DDX_Coord (pDX, TXT_LOCATIONY, m_pt.y, m_units, head, false);
	DDX_Text (pDX, TXT_ID, m_nID);

	if (GetDlgItem (CHK_FLIPH))		DDX_Check (pDX, CHK_FLIPH, m_bFlipH);
	if (GetDlgItem (CHK_FLIPV))		DDX_Check (pDX, CHK_FLIPV, m_bFlipV);
	if (GetDlgItem (CHK_INVERSE))	DDX_Check (pDX, CHK_INVERSE, m_bInverse);
	if (GetDlgItem (CHK_PREVIEW))	DDX_Check (pDX, CHK_PREVIEW, m_bPreview);

	if (m_pList)
		DDV_ID (pDX, m_nID, GetElement (), * m_pList, TXT_ID);
}


BEGIN_MESSAGE_MAP(CElementDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CElementDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(CHK_PREVIEW, OnClickPreview)
	ON_BN_CLICKED(CHK_FLIPH, InvalidatePreview)
	ON_BN_CLICKED(CHK_FLIPV, InvalidatePreview)
	ON_BN_CLICKED(CHK_INVERSE, InvalidatePreview)
	ON_CBN_SELCHANGE(CB_FONT, InvalidatePreview)
	ON_EN_CHANGE(TXT_TEXT, InvalidatePreview)
	ON_EN_CHANGE(TXT_BOLD, InvalidatePreview)
	ON_EN_CHANGE(TXT_WIDTH, InvalidatePreview)
	ON_EN_CHANGE(TXT_GAP, InvalidatePreview)
	ON_BN_CLICKED (BTN_DEFAULT, OnDefault)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CElementDlg message handlers

BOOL CElementDlg::OnInitDialog() 
{
	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();

	ASSERT (GetDlgItem (IDOK));
	ASSERT (GetDlgItem (TXT_LOCATIONX));
	ASSERT (GetDlgItem (TXT_LOCATIONY));
	ASSERT (GetDlgItem (TXT_ID));

	if (m_bReadOnly)
		GetDlgItem (IDOK)->EnableWindow (false);

	if (m_bReadOnlyLocation) {
		GetDlgItem (TXT_LOCATIONX)->EnableWindow (false);
		GetDlgItem (TXT_LOCATIONY)->EnableWindow (false);
	}

	if (m_bReadOnlyID) 
		GetDlgItem (TXT_ID)->EnableWindow (false);

	if (CWnd * pPreview = GetDlgItem (IDC_PREVIEW)) {
		CRect rc;
		pPreview->GetWindowRect (&rc);

		ScreenToClient (&rc);

		ASSERT (m_pElement);
		ASSERT (m_pList);
		VERIFY (m_wndPreview.Create (this, rc, m_pElement, &m_pList->GetHead ()));
		InvalidatePreview ();
	}

	if (CWnd * p = GetDlgItem (TXT_ID)) 
		p->EnableWindow (FALSE);

	if (CWnd * p = GetDlgItem (GetDefCtrlID ())) {
		GotoDlgCtrl (p);
		return FALSE;
	}

	return bResult;
}

CSize CElementDlg::GetElementSize ()
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();

	ASSERT (m_pElement); 
	BuildElement ();
	m_pElement->Draw (* GetDC (), head, true);
	CSize size = m_pElement->GetWindowRect (head).Size ();

	return size;
}

void CElementDlg::OnOK()
{
	if (!(m_bReadOnly || m_bReadOnlyLocation) && IsConfiningOn ()) {
		if (BuildElement () == UNKNOWN)
			return;
	}

	const HEADSTRUCT & head = GetHead ();
	CSize size = CBaseElement::ThousandthsToLogical (GetElement ().GetWindowRect (head).Size (), head);
	int nHeight = head.Height ();

	if (size.cx > m_pa.cx) {
		MsgBox (* this, LoadString (IDS_ELEMENTTOOLONG));
		return;
	}
	else if (size.cy > nHeight) {
		MsgBox (* this, LoadString (IDS_ELEMENTTOOTALL));
		return;
	}

	ASSERT (!m_bReadOnly);
	FoxjetCommon::CEliteDlg::OnOK ();
}

bool CElementDlg::IsPreviewEnabled() const
{
	if (CButton * p = (CButton *)GetDlgItem (CHK_PREVIEW))
		return p->GetCheck () == 1;

	return false;
}

void CElementDlg::OnClickPreview ()
{
	bool bEnable = IsPreviewEnabled ();

	m_wndPreview.EnableWindow (bEnable);
	
	if (!bEnable) {
		m_wndPreview.Invalidate ();
		m_wndPreview.RedrawWindow ();
	}
	else
		InvalidatePreview ();
}

void CElementDlg::InvalidatePreview()
{
	TRACEF ("InvalidatePreview ()");

	if (IsPreviewEnabled ()) {
		CDC dcMem;

		BuildElement ();
		
		dcMem.CreateCompatibleDC (GetDC ());
		CSize size = CBaseElement::ThousandthsToLogical (GetElement ().Draw (dcMem, GetHead (), true), GetHead ());

#ifdef __WYSIWYGFIX__
		CRect rc;

		m_wndPreview.GetWindowRect (rc);

		double dZoom [2] = 
		{
			(double)rc.Width () / (double)size.cx,
			(double)rc.Height () / (double)size.cy,
		};

		m_wndPreview.SetZoom (min (dZoom [0], dZoom [1]));
#endif

		m_wndPreview.Invalidate ();
		m_wndPreview.RedrawWindow ();
	}
}

FoxjetCommon::CBaseElement & CElementDlg::GetElement ()
{
	ASSERT (m_pElement);
	return * m_pElement;
}

const FoxjetCommon::CBaseElement & CElementDlg::GetElement () const
{
	ASSERT (m_pElement);
	return * m_pElement;
}

bool CElementDlg::GetCheck(UINT nID) const
{
	return GetCheck (this, nID);
}

bool CElementDlg::GetCheck(const CWnd * pWnd, UINT nID)
{
	bool bResult = false;

	if (CButton * p = (CButton *)pWnd->GetDlgItem (nID))
		bResult = p->GetCheck () == 1;

	return bResult;
}

const FoxjetDatabase::HEADSTRUCT & CElementDlg::GetHead() const
{
	ASSERT (m_pList);
	return m_pList->GetHead ();
}

ULONG CElementDlg::GetLineID () const
{
	return m_lLineID;
}

void CElementDlg::SetDlgItemText (int nID, LPCTSTR lpszString)
{
	CEdit * p = (CEdit *)GetDlgItem (nID);

	ASSERT (p);
	CWnd::SetDlgItemText (nID, lpszString);
	p->SetModify (TRUE);
}

void CElementDlg::SetDlgItemInt (int nID, UINT nValue, BOOL bSigned)
{
	CEdit * p = (CEdit *)GetDlgItem (nID);

	ASSERT (p);
	CWnd::SetDlgItemInt (nID, nValue, bSigned);
	p->SetModify (TRUE);
}

CString CElementDlg::GetCtrlText (UINT nID) const
{
	CString str;
	GetDlgItemText (nID, str);
	return str;
}

const FoxjetCommon::CElementList * CElementDlg::GetList () 
{ 
	ASSERT (m_pList); 
	return m_pList; 
}

int CElementDlg::GetCtrlFontSize (UINT nID)
{
	int cy = 11;

	if (CEdit * p = (CEdit *)GetDlgItem (nID)) {
		LOGFONT lf;

		if (CFont * pFont = p->GetFont ()) {
			pFont->GetObject (sizeof (lf), &lf);
			cy = lf.lfHeight;
		}
		else if (CFont * pFont = GetFont ()) {
			pFont->GetObject (sizeof (lf), &lf);
			cy = lf.lfHeight;
		}
	}

	return cy;
}

void CElementDlg::OnDefault ()
{
	if (MsgBox (* this, LoadString (IDS_SETDEFAULT), LoadString (IDS_CONFIRM), MB_YESNO | MB_ICONINFORMATION) == IDYES) 
		m_bSetDefault = true;
}