// BitmapDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "BitmapDlg.h"
#include "Color.h"
#include "Debug.h"
#include "Utils.h"

#include "fj_element.h"
#include "fj_bitmap.h"
#include "fj_printhead.h"
#include "fj_font.h"
#include "fj_export.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


using namespace FoxjetCommon;
using namespace FoxjetIpElements;

/////////////////////////////////////////////////////////////////////////////
// CBitmapDlg dialog

CBitmapDlg::CBitmapDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa,
		const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_dwProcessId (0),
	m_bValid (false),
	m_hbmpOriginal (NULL),
	CElementDlg(e, pa, CBitmapDlg::IDD, pList, pParent)
{
	//{{AFX_DATA_INIT(CBitmapDlg)
	m_lBold = 0;
	m_strPath = _T("");
	m_lWidth = 0;
	//}}AFX_DATA_INIT
}


void CBitmapDlg::DoDataExchange(CDataExchange* pDX)
{
	CElementDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBitmapDlg)
	//}}AFX_DATA_MAP

	DDX_Text(pDX, TXT_TEXT, m_strPath);

	DDX_Text (pDX, TXT_BOLD, m_lBold);
	DDV_MinMaxInt (pDX, m_lBold, CBitmapElement::m_lmtBold.m_dwMin, CBitmapElement::m_lmtBold.m_dwMax); 

	DDX_Text (pDX, TXT_WIDTH, m_lWidth);
	DDV_MinMaxInt (pDX, m_lWidth, CBitmapElement::m_lmtWidth.m_dwMin, CBitmapElement::m_lmtWidth.m_dwMax); 
}


BEGIN_MESSAGE_MAP(CBitmapDlg, CElementDlg)
	//{{AFX_MSG_MAP(CBitmapDlg)
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_WM_DRAWITEM()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(CHK_FLIPH, CElementDlg::InvalidatePreview)
	ON_BN_CLICKED(CHK_FLIPV, CElementDlg::InvalidatePreview)
	ON_BN_CLICKED(CHK_INVERSE, CElementDlg::InvalidatePreview)
	ON_EN_CHANGE(TXT_TEXT, CElementDlg::InvalidatePreview)
	ON_EN_CHANGE(TXT_BOLD, CElementDlg::InvalidatePreview)
	ON_EN_CHANGE(TXT_WIDTH, CElementDlg::InvalidatePreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBitmapDlg message handlers

void CBitmapDlg::OnBrowse() 
{
	while (1) {
		const CBitmapElement & e = GetElement ();
		CString strPath = e.GetFilePath ();

		CFileDialog dlg (TRUE, _T ("*.bmp"), strPath, 
			OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
			_T ("Bitmaps (*.bmp)|*.bmp||"), this);

		if (dlg.DoModal () == IDOK) {
			const CString strFile = dlg.GetPathName ();
			const CString strDefPath = CBitmapElement::GetDefPath ();
			const CString strSelectedPath = strFile.Left (strDefPath.GetLength ());

			if (strSelectedPath.CompareNoCase (strDefPath) == 0) {
				if (PBYTE pFile = CBitmapElement::LoadBitmap (strFile)) {
					SetDlgItemText (TXT_TEXT, strFile);
					InvalidatePreview ();
					delete pFile;
					return;
				}
				else {
					CString str;

					str.Format (LoadString (IDS_LOADBITMAPFAILED), strFile);
					MsgBox (* this, str, LoadString (IDS_WARNING), MB_OK | MB_ICONINFORMATION);
				}
			}
			else {
				CString str;

				str.Format (LoadString (IDS_CANTCHANGEDIR), strDefPath);
				MsgBox (* this, str);
			}
		}
		else
			return; // cancelled
	}
}

void CBitmapDlg::OnEdit() 
{
	if (m_dwProcessId > 0)
		FoxjetCommon::ActivateProcess (m_dwProcessId);
	else {
		DWORD dw;
		CString strFile = GetElement ().GetFilePath ();

		ASSERT (m_hbmpOriginal == NULL);
		VERIFY (m_hbmpOriginal = (HBITMAP)::LoadImage (GetInstanceHandle (),
				strFile, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION));

		HANDLE hThread = ::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0,
			(LPTHREAD_START_ROUTINE)WaitForEdit, (LPVOID)this, 0, &dw);
		ASSERT (hThread != NULL);
	}
}

ULONG CALLBACK CBitmapDlg::WaitForEdit (LPVOID lpData)
{
	ASSERT (lpData);

	CBitmapDlg & dlg = * (CBitmapDlg *)lpData;
	CBitmapElement & e = dlg.GetElement ();
	CString strApp = GetBitmapEditorPath ();
	CString strCmdLine = strApp + _T (" \"") + e.GetFilePath () + _T ("\"");
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [0xFF];

	_tcsncpy (szCmdLine, strCmdLine, 0xFF);
    memset (&pi, 0, sizeof(pi));
    memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	BOOL bResult = ::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi); 

	dlg.UpdateUI ();

	if (!bResult) {
		MsgBox (dlg, LoadString (IDS_FAILEDTOLANUCHEDITOR));
	}
	else {
		bool bMore;
		
		dlg.m_dwProcessId = pi.dwProcessId;

		TRACEF ("begin edit");

		do {
			DWORD dwExitCode = 0;
			
			dlg.UpdateUI ();

			if (!GetExitCodeProcess (pi.hProcess, &dwExitCode))
				bMore = false;
			else {
				bMore = (dwExitCode == STILL_ACTIVE);
				WaitForSingleObjectEx(pi.hProcess, 3000, TRUE);
				TRACEF ("still waiting");
			}
		}
		while (bMore);

		TRACEF ("edit complete");
	}

	dlg.m_dwProcessId = 0;
	dlg.ReloadBitmap ();
	dlg.UpdateUI ();

	return 0;
}

void CBitmapDlg::ReloadBitmap ()
{
	const FoxjetDatabase::HEADSTRUCT & head = GetHead ();
	CBitmapElement * pElement = (CBitmapElement *)CopyElement (&GetElement (), &head, false);
	bool bInvalid = false;
	CString strMsg;

	ASSERT (pElement);
	CBitmapElement & e = * pElement;
	CString strFile = e.GetFilePath ();

	if (!e.SetFilePath (strFile, head)) {
		strMsg.Format (LoadString (IDS_LOADBITMAPFAILED), strFile);
		bInvalid = true;
	}
	else {
		CDC dcMem;

		dcMem.CreateCompatibleDC (GetDC ());
		e.Build ();
		e.Draw (dcMem, head);

		CSize sizeBmp = CBaseElement::ThousandthsToLogical (e.GetWindowRect (head).Size (), head);
		CSize sizePA = GetPA ();
		int nHeight = head.Height ();

		if (sizeBmp.cx > sizePA.cx) {
			strMsg = LoadString (IDS_ELEMENTTOOLONG);
			bInvalid = true;
		}
		else if (sizeBmp.cy > nHeight) {
			strMsg = LoadString (IDS_ELEMENTTOOTALL);
			bInvalid = true;
		}
	}

	if (bInvalid) {
		if (m_hbmpOriginal) {
			CString str = LoadString (IDS_RESTORINGOLDBITMAP) + strMsg;
			MsgBox (* this, str);

			VERIFY (SaveBitmap (m_hbmpOriginal, strFile));
		}
	}

	if (m_hbmpOriginal) {
		::DeleteObject (m_hbmpOriginal);
		m_hbmpOriginal = NULL;
	}

	delete pElement;

	InvalidatePreview ();
}

void CBitmapDlg::OnOK()
{
	if (!m_dwProcessId) {
		CString strFile;
		CBitmapElement & e = GetElement ();

		GetDlgItemText (TXT_TEXT, strFile);
	
		if (!e.SetFilePath (strFile, GetHead ())) {
			CString str;

			str.Format (LoadString (IDS_LOADBITMAPFAILED), strFile);
			MsgBox (* this, str, LoadString (IDS_WARNING), MB_OK | MB_ICONINFORMATION);
		}
		else
			CElementDlg::OnOK ();
	}
}

void CBitmapDlg::OnCancel()
{
	if (!m_dwProcessId)
		CElementDlg::OnCancel ();
}

void CBitmapDlg::UpdateUI()
{
	UINT nIDs [] = {
		BTN_BROWSE,
		//BTN_EDIT,
		TXT_ID,
		TXT_BOLD,
		TXT_WIDTH,
		CHK_FLIPH,
		CHK_FLIPV,
		CHK_INVERSE,
		TXT_LOCATIONX,
		TXT_LOCATIONY,
		IDOK,
		IDCANCEL,
		CHK_PREVIEW,
		-1
	};

	for (int i = 0; nIDs [i] != -1; i++)
		GetDlgItem (nIDs [i])->EnableWindow (!m_dwProcessId);
}

CBitmapElement & CBitmapDlg::GetElement ()
{
	CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CBitmapElement)));
	return (CBitmapElement &)e;
}

const CBitmapElement & CBitmapDlg::GetElement () const
{
	const CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CBitmapElement)));
	return (const CBitmapElement &)e;
}

int CBitmapDlg::BuildElement ()
{
	CBitmapElement & e = GetElement ();
	CString strFile;

	GetDlgItemText (TXT_TEXT, strFile);

	m_bValid = e.SetFilePath (strFile, GetHead ());
	e.SetBold (GetDlgItemInt (TXT_BOLD));
	e.SetWidth (GetDlgItemInt (TXT_WIDTH));
	e.SetFlippedH (GetCheck (CHK_FLIPH));
	e.SetFlippedV (GetCheck (CHK_FLIPV));
	e.SetInverse (GetCheck (CHK_INVERSE));

	return e.Build ();
}

void CBitmapDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (m_bValid)
		CElementDlg::OnDrawItem (nIDCtl, lpDrawItemStruct);
	else {
		DRAWITEMSTRUCT & dis = * lpDrawItemStruct;
		CDC dc;
		CRect rc = dis.rcItem;
		CString str;
		CString strFile;

		dc.Attach (dis.hDC);
		dc.FillSolidRect (rc, Color::rgbDarkGray);
		GetDlgItemText (TXT_TEXT, strFile);
		str.Format (LoadString (IDS_LOADBITMAPFAILED), strFile);
		dc.TextOut (0, 0, str);
		dc.Detach ();
	}
}
