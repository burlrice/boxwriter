#if !defined(AFX_BITMAPDLG_H__3B591347_A340_419F_9EB1_C0C627D6CFD3__INCLUDED_)
#define AFX_BITMAPDLG_H__3B591347_A340_419F_9EB1_C0C627D6CFD3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BitmapDlg.h : header file
//

#include "resource.h"
#include "ElementDlg.h"
#include "BitmapElement.h"

/////////////////////////////////////////////////////////////////////////////
// CBitmapDlg dialog

class CBitmapDlg : public CElementDlg
{
// Construction
public:
	CBitmapDlg (const FoxjetCommon::CBaseElement & e, const CSize & pa,
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   

// Dialog Data
	//{{AFX_DATA(CBitmapDlg)
	enum { IDD = IDD_BITMAP };
	long	m_lBold;
	CString	m_strPath;
	long	m_lWidth;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBitmapDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	FoxjetIpElements::CBitmapElement & GetElement ();
	const FoxjetIpElements::CBitmapElement & GetElement () const;
	virtual int BuildElement ();

	void UpdateUI();
	virtual void OnCancel();
	virtual void OnOK();
	static ULONG CALLBACK WaitForEdit (LPVOID lpData);
	void ReloadBitmap ();
	
	DWORD m_dwProcessId;
	bool m_bValid;
	HBITMAP m_hbmpOriginal;

	// Generated message map functions
	//{{AFX_MSG(CBitmapDlg)
	afx_msg void OnBrowse();
	afx_msg void OnEdit();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BITMAPDLG_H__3B591347_A340_419F_9EB1_C0C627D6CFD3__INCLUDED_)
