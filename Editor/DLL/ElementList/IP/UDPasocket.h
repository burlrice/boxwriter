#if !defined(AFX_UDPASOCKET_H__265049BA_7FBB_4354_A9E2_4BC3BEA70BC7__INCLUDED_)
#define AFX_UDPASOCKET_H__265049BA_7FBB_4354_A9E2_4BC3BEA70BC7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UDPasocket.h : header file
//

#include <afxsock.h>

#include "ElementApi.h"

#define FJ_UDP_PORT 1281

namespace FoxjetIpElements
{
	typedef BOOL (CALLBACK * LPFINDHEADSFCT) (const CString & str, CWnd * pParent);

	/////////////////////////////////////////////////////////////////////////////
	// CUDPasocket command target

	class ELEMENT_API CUDPasocket : public CAsyncSocket
	{
	// Operations
	public:
		CUDPasocket (LPFINDHEADSFCT pfct, CWnd * pParent);
		virtual ~CUDPasocket();

	// Overrides
	public:
		void SendInfo();
		void ProcessData(int iRead);
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CUDPasocket)
		public:
		virtual void OnReceive(int nErrorCode);
		virtual void OnSend(int nErrorCode);
		virtual void OnClose(int nErrorCode);
		//}}AFX_VIRTUAL

		// Generated message map functions
		//{{AFX_MSG(CUDPasocket)
			// NOTE - the ClassWizard will add and remove member functions here.
		//}}AFX_MSG

	// Implementation
	protected:
		LPFINDHEADSFCT m_pfct;

		UCHAR * m_pReceiveBuffer;
		CWnd * m_pParent;
	};
}; //namespace FoxjetIpElements

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UDPASOCKET_H__265049BA_7FBB_4354_A9E2_4BC3BEA70BC7__INCLUDED_)
