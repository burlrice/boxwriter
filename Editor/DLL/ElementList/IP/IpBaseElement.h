// IpBaseElement.h: interface for the CIpBaseElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IPBASEELEMENT_H__B02BD07D_0F4F_4234_90A0_3DF59E7F8CF6__INCLUDED_)
#define AFX_IPBASEELEMENT_H__B02BD07D_0F4F_4234_90A0_3DF59E7F8CF6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "BaseElement.h"
#include "Types.h"
#include "Utils.h"

#define DECLARE_MEMBERPOINTERACCESS(class_pointer_name, member_pointer_name) \
	public: \
		const class_pointer_name GetMember () const \
		{ \
			const class_pointer_name p = (const class_pointer_name)member_pointer_name; \
			ASSERT (p); \
			return p; \
		} \
		operator const class_pointer_name () const { return GetMember (); } 

FORWARD_DECLARE (fjprinthead,	FJPRINTHEAD)
FORWARD_DECLARE (fjelement,		FJELEMENT)
FORWARD_DECLARE (fjfont,		FJFONT)
FORWARD_DECLARE (fjsystem,		FJSYSTEM)
FORWARD_DECLARE (fjlabel,		FJLABEL)
FORWARD_DECLARE (fjimage,		FJIMAGE)

namespace FoxjetIpElements
{
	class ELEMENT_API CIpBaseElement : public FoxjetCommon::CBaseTextElement
	{
		DECLARE_DYNAMIC (CIpBaseElement)
	public:
		CIpBaseElement (LPFJELEMENT pElement, LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CIpBaseElement (const CIpBaseElement & rhs);
		CIpBaseElement & operator = (const CIpBaseElement & rhs);
		virtual ~CIpBaseElement();

	public:
		virtual CSize Draw (CDC & dc, const FoxjetDatabase::HEADSTRUCT & head, bool bCalcSizeOnly = false, FoxjetCommon::DRAWCONTEXT context = FoxjetCommon::EDITOR);
		virtual int GetClassID () const;
		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);

		virtual bool SetPos (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT * pHead, FoxjetCommon::ALIGNMENT_ADJUST adj = FoxjetCommon::ALIGNMENT_ADJUST_NONE);
		virtual CPoint GetPos (const FoxjetDatabase::HEADSTRUCT * pHead) const;
		virtual bool Move (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT & head);
		virtual bool SetXPos (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT * pHead);
		virtual bool SetYPos (const CPoint & pt, const FoxjetDatabase::HEADSTRUCT * pHead);

		virtual CSize GetSize (const FoxjetDatabase::HEADSTRUCT & head, bool bHeadDPI = true) const;
		virtual void ClipTo (const FoxjetDatabase::HEADSTRUCT & head);
		virtual bool SetRedraw (bool bRedraw = true);

		virtual bool IsFlippedH () const;
		virtual bool IsFlippedV () const;
		virtual bool IsInverse () const;

		virtual bool SetFlippedH (bool bFlip);
		virtual bool SetFlippedV (bool bFlip);
		virtual bool SetInverse (bool bInverse);

		virtual UINT GetID () const;
		virtual bool SetID (UINT nID); 

		virtual bool HasFont () const;
		virtual bool SetFont (const FoxjetCommon::CPrinterFont & f);
		virtual bool GetFont (FoxjetCommon::CPrinterFont & f) const;
		virtual LONG GetBold () const;
		virtual bool SetBold (LONG l);
		virtual CString GetElementFontName () const = 0;

		virtual bool Validate ();
		virtual bool IsValid () const { return true; }

		LPCFJPRINTHEAD GetHead () const { return m_pHead; }
		LPFJPRINTHEAD GetHead () { return m_pHead; }

		LPCFJSYSTEM GetSystem () const;
		LPFJSYSTEM GetSystem ();

		LPCFJLABEL GetLabel () const;
		bool SetLabel (LPFJLABEL pLabel);

		LPVOID New ();
		LPVOID Dup (LPVOID pfd);
		void Destroy ();
		virtual void CreateImage ();
		void PhotocellBuild () const;

		long GetType () const;
		CString GetTypeString () const;

		static CSize GetImageSize (LPCFJIMAGE pfi);
		static LPBYTE ImageIpToWinRotate (LPCFJIMAGE pfi, LPBYTE pSrcBuffer, CSize & size);
		static LPBYTE ImageIpToWin (LPCFJIMAGE pfi, LPBYTE pSrcBuffer, CSize & size);

		static void AdjustYPos (LPFJELEMENT p, const FoxjetDatabase::HEADSTRUCT & head);
		
		static const FoxjetCommon::LIMITSTRUCT m_lmtBold;
		static const FoxjetCommon::LIMITSTRUCT m_lmtWidth;
		static const FoxjetCommon::LIMITSTRUCT m_lmtGap;
		static const FoxjetCommon::LIMITSTRUCT m_lmtString;

	protected:
		const LPFJELEMENT GetElement () const;
		LPFJELEMENT GetElement ();

		virtual bool SetElement (LPFJELEMENT pNew);
		virtual CBitmap * CreateBitmap (CDC & dc, FoxjetCommon::DRAWCONTEXT context);
		virtual void Invalidate ();

		LPFJPRINTHEAD m_pHead;
		LPFJELEMENT m_pElement;
		LPFJFONT m_pFont;
		LPFJLABEL m_pLabel;
		CBitmap * m_pBmp;
		FoxjetCommon::DRAWCONTEXT m_lastContext;
		LONG m_lLastBold;
	};


}; //namespace FoxjetIpElements


#endif // !defined(AFX_IPBASEELEMENT_H__B02BD07D_0F4F_4234_90A0_3DF59E7F8CF6__INCLUDED_)
