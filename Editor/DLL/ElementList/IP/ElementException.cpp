#include "stdafx.h"
#include "List.h"
#include "BaseElement.h"
#include "Debug.h"
#include "Types.h"
#include "Extern.h"
#include "Resource.h"
#include "Database.h"

using namespace ListGlobals;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CElementException::CElementException (EXCEPTIONTYPE type, const CString & str)
:	m_type (type),
	CException ()
{
	m_str = GetDescription (type) + _T ("\n") + str;
}

CElementException::CElementException (CException * e)
:	m_type (EXCEPTION_GENERIC),
	CException ()
{
	m_type = GetType (e);
	m_str = GetDescription (m_type) + _T ("\n") + FoxjetDatabase::FormatException (e);
}

BOOL CElementException::GetErrorMessage (LPTSTR lpszError, UINT nMaxError, PUINT pnHelpContext)
{
	if (lpszError) {
		_tcsncpy (lpszError, m_str, nMaxError);
		return true;
	}

	return false;
}

CString CElementException::GetErrorMessage ()
{
	return m_str;
}

int CElementException::ReportError (UINT nType, UINT nMessageID)
{
	return MsgBox (m_str, _T (""), nType);
}

EXCEPTIONTYPE CElementException::GetType (const CException * e)
{
	if (e->IsKindOf (RUNTIME_CLASS (CDBException)))
		return EXCEPTION_DATABASE;
	else if (e->IsKindOf (RUNTIME_CLASS (COdbcException)))
		return EXCEPTION_DATABASE;
	else if (e->IsKindOf (RUNTIME_CLASS (CMemoryException)))
		return EXCEPTION_MEMORY;
	else
		return EXCEPTION_GENERIC;
}

CString CElementException::GetDescription (EXCEPTIONTYPE type)
{
	struct 
	{
		UINT			m_nResID;
		EXCEPTIONTYPE	m_type;
	} map [] = 
	{
		{ IDS_EXCEPTION_GENERIC,	EXCEPTION_GENERIC,	},
		{ IDS_EXCEPTION_DATABASE,	EXCEPTION_DATABASE,	},
		{ IDS_EXCEPTION_MEMORY,		EXCEPTION_MEMORY,	},
	};

	for (int i = 0; i < (sizeof (map) / sizeof (map [i])); i++) 
		if (map [i].m_type == type) 
			return LoadString (map [i].m_nResID);

	return _T ("");
}