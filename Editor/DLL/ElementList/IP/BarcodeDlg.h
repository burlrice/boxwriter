#if !defined(AFX_BARCODEDLG_H__05C306EB_F112_4E1E_92AE_B7B1AC2BED2D__INCLUDED_)
#define AFX_BARCODEDLG_H__05C306EB_F112_4E1E_92AE_B7B1AC2BED2D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BarcodeDlg.h : header file
//

#include "ElementDlg.h"
#include "BarcodeElement.h"

/////////////////////////////////////////////////////////////////////////////
// CBarcodeDlg dialog

class CBarcodeDlg : public CElementDlg
{
// Construction
public:
	CBarcodeDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa,
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBarcodeDlg)
	enum { IDD = IDD_BARCODE };
	BOOL	m_bOutputText;
	int		m_dBold;
	CString	m_strText;
	int		m_dTextBold;
	int		m_dWidth;
	//}}AFX_DATA

	int		m_nType;
	CHAR	m_cAlign;


	virtual UINT GetDefCtrlID () const { return TXT_TEXT; }
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBarcodeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();
	FoxjetIpElements::CBarcodeElement & GetElement ();
	const FoxjetIpElements::CBarcodeElement & GetElement () const;

	virtual int BuildElement ();

	// Generated message map functions
	//{{AFX_MSG(CBarcodeDlg)
	afx_msg void OnAscii();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeType();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	afx_msg void OnEditParams();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BARCODEDLG_H__05C306EB_F112_4E1E_92AE_B7B1AC2BED2D__INCLUDED_)
