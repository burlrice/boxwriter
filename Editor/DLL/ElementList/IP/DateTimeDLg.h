#if !defined(AFX_DATETIMEDLG_H__36667F6D_130F_4068_9EE1_28CAA694FED4__INCLUDED_)
#define AFX_DATETIMEDLG_H__36667F6D_130F_4068_9EE1_28CAA694FED4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DateTimeDLg.h : header file
//

#include "ElementDlg.h"
#include "DateTimeElement.h"

/////////////////////////////////////////////////////////////////////////////
// CDateTimeDlg dialog

class CDateTimeDlg : public CElementDlg
{
// Construction
public:
	CDateTimeDlg (const FoxjetCommon::CBaseElement & e, const CSize & pa,
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   

// Dialog Data
	//{{AFX_DATA(CDateTimeDlg)
	enum { IDD = IDD_DATETIME };
	long	m_lBold;
	BOOL	m_bRightAlign;
	CString	m_strDelim;
	CString	m_strFill;
	long	m_lGap;
	long	m_lLength;
	long	m_lHourACount;
	long	m_lWidth;
	int		m_nFormat;
	int		m_nType;
	//}}AFX_DATA

	CString m_strFont;
	BOOL m_bSpecialString;

	virtual UINT GetDefCtrlID () const { return CB_FORMAT; }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDateTimeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();
	void InitTypeCtrl();
	void InitFormatCtrl();
	virtual BOOL OnInitDialog();
	virtual int BuildElement ();
	FoxjetIpElements::CDateTimeElement & GetElement ();
	const FoxjetIpElements::CDateTimeElement & GetElement () const;
	static bool HasSpecialCode (int nType);

	// Generated message map functions
	//{{AFX_MSG(CDateTimeDlg)
	afx_msg void OnSelchangeFormat();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATETIMEDLG_H__36667F6D_130F_4068_9EE1_28CAA694FED4__INCLUDED_)
