// timedlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "timedlg.h"
#include "fj_defines.h"
#include "fj_system.h"
#include "Extern.h"
#include "Edit.h"
#include "Database.h"
#include "Extern.h"
#include "Debug.h"
#include "Utils.h"
#include "Parse.h"

using namespace FoxjetIpElements;
using namespace FoxjetDatabase;
using namespace ListGlobals;
using namespace FoxjetCommon;

extern CArray <LPFJSYSTEM, LPFJSYSTEM> vIpSystems;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetIpElements;
using namespace TimeDlg;

/////////////////////////////////////////////////////////////////////////////
CShiftCode::CShiftCode (LPFJSYSTEM pSystem)
:	m_lLineID			(-1),
	m_dtShift1			(COleDateTime (2003, 1, 1, 7,	0, 0)),
	m_dtShift2			(COleDateTime (2003, 1, 1, 15,	0, 0)),
	m_dtShift3			(COleDateTime (2003, 1, 1, 23,	0, 0))
{
	if (pSystem) {
		const int nShift [3][2] = 
		{
			{ pSystem->lShift1 / 60,	pSystem->lShift1 - nShift [0][0] * 60 },
			{ pSystem->lShift2 / 60,	pSystem->lShift2 - nShift [1][0] * 60 },
			{ pSystem->lShift3 / 60,	pSystem->lShift3 - nShift [2][0] * 60 },
		};

		m_lLineID				= atol (pSystem->strID);
		m_strShiftCode1			= pSystem->sShiftCode1;
		m_strShiftCode2			= pSystem->sShiftCode2;
		m_strShiftCode3			= pSystem->sShiftCode3;
		m_dtShift1         		= COleDateTime (2003, 1, 1, nShift [0][0], nShift [0][1], 0);
		m_dtShift2         		= COleDateTime (2003, 1, 1, nShift [1][0], nShift [1][1], 0);
		m_dtShift3         		= COleDateTime (2003, 1, 1, nShift [2][0], nShift [2][1], 0);
	}
}

CShiftCode::CShiftCode (const CShiftCode & rhs)
:	m_lLineID			(rhs.m_lLineID),
	m_dtShift1         	(rhs.m_dtShift1),
	m_dtShift2         	(rhs.m_dtShift2),
	m_dtShift3         	(rhs.m_dtShift3),
	m_strShiftCode1		(rhs.m_strShiftCode1),
	m_strShiftCode2		(rhs.m_strShiftCode2),
	m_strShiftCode3		(rhs.m_strShiftCode3)
{
}

CShiftCode & CShiftCode::operator = (const CShiftCode & rhs)
{
	if (this != &rhs) {
		m_lLineID				= rhs.m_lLineID;
		m_dtShift1         		= rhs.m_dtShift1;
		m_dtShift2         		= rhs.m_dtShift2;
		m_dtShift3         		= rhs.m_dtShift3;
		m_strShiftCode1			= rhs.m_strShiftCode1;	
		m_strShiftCode2			= rhs.m_strShiftCode2;	
		m_strShiftCode3			= rhs.m_strShiftCode3;	
	}

	return * this;
}

CShiftCode::~CShiftCode ()
{
}

/////////////////////////////////////////////////////////////////////////////
// CTimeDlg dialog

const FoxjetCommon::LIMITSTRUCT CTimeDlg::m_lmtShift = { 1, FJSYS_SHIFT_CODE_SIZE };

CTimeDlg::CTimeDlg(CWnd* pParent /*=NULL*/)
:	m_lLineID (-1),
	FoxjetCommon::CEliteDlg(CTimeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTimeDlg)
	//}}AFX_DATA_INIT
}

CShiftCode CTimeDlg::GetCurSelShiftCode () const
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CShiftCode s;

	ASSERT (pLine);

	int nIndex = pLine->GetCurSel ();

	if (nIndex != CB_ERR) {
		int nShiftCode = pLine->GetItemData (nIndex);

		s = m_vCodes [nShiftCode];
	}

	return s;
}

bool CTimeDlg::SetCurSelShiftCode (const CShiftCode & s)
{
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);

	ASSERT (pLine);

	int nIndex = pLine->GetCurSel ();

	if (nIndex != CB_ERR) {
		int nShiftCode = pLine->GetItemData (nIndex);

		m_vCodes.SetAt (nShiftCode, CShiftCode (s));
		return true;
	}

	return false;
}

void CTimeDlg::DoDataExchange(CDataExchange* pDX)
{
	CShiftCode s = GetCurSelShiftCode ();

	struct
	{
		CDateTimeCtrl * m_pCtrl;
		COleDateTime * m_pMember;
	} map [3] = 
	{
		{ (CDateTimeCtrl *)GetDlgItem (DT_SHIFT1), &s.m_dtShift1, },
		{ (CDateTimeCtrl *)GetDlgItem (DT_SHIFT2), &s.m_dtShift2, },
		{ (CDateTimeCtrl *)GetDlgItem (DT_SHIFT3), &s.m_dtShift3, },
	};

	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTimeDlg)
	//}}AFX_DATA_MAP

	DDX_Text(pDX, TXT_SHIFTCODE1,	s.m_strShiftCode1);
	DDV_MinMaxChars (pDX, s.m_strShiftCode1, TXT_SHIFTCODE1, m_lmtShift);

	DDX_Text(pDX, TXT_SHIFTCODE2,	s.m_strShiftCode2);
	DDV_MinMaxChars (pDX, s.m_strShiftCode2, TXT_SHIFTCODE2, m_lmtShift);
	
	DDX_Text(pDX, TXT_SHIFTCODE3,	s.m_strShiftCode3);
	DDV_MinMaxChars (pDX, s.m_strShiftCode3, TXT_SHIFTCODE3, m_lmtShift);
	
	if (pDX->m_bSaveAndValidate) {
		COleDateTime * pdt [3] = 
		{
			map [0].m_pMember,
			map [1].m_pMember,
			map [2].m_pMember,
		};

		for (int i = 0; i < 3; i++) {
			ASSERT (map [i].m_pCtrl);
			ASSERT (map [i].m_pMember);

			VERIFY (map [i].m_pCtrl->GetTime (* pdt [i]));
		}

		ULONG lShift1 = TimeToLong (* pdt [0]);
		ULONG lShift2 = TimeToLong (* pdt [1]);
		ULONG lShift3 = TimeToLong (* pdt [2]);

		if (lShift1 >= lShift2) {
			MsgBox (* this, LoadString (IDS_SHIFTCODESOUTOFORDER1), NULL, MB_ICONWARNING);
			pDX->PrepareCtrl (DT_SHIFT1);
			pDX->Fail ();
		}
		else if (lShift2 >= lShift3) {
			MsgBox (* this, LoadString (IDS_SHIFTCODESOUTOFORDER2), NULL, MB_ICONWARNING);
			pDX->PrepareCtrl (DT_SHIFT2);
			pDX->Fail ();
		}

		VERIFY (SetCurSelShiftCode (s));
	}
	else {
		for (int i = 0; i < 3; i++) {
			ASSERT (map [i].m_pCtrl);
			ASSERT (map [i].m_pMember);
			map [i].m_pCtrl->SetTime (* map [i].m_pMember);
		}
	}
}

BEGIN_MESSAGE_MAP(CTimeDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CTimeDlg)
	ON_CBN_SELCHANGE(CB_LINE, OnSelchangeLine)
	ON_BN_CLICKED(BTN_APPLY, OnApply)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE (TXT_SHIFTCODE1,	EnableApply)
	ON_EN_CHANGE (TXT_SHIFTCODE2,	EnableApply)
	ON_EN_CHANGE (TXT_SHIFTCODE3,	EnableApply)
	ON_NOTIFY(DTN_DATETIMECHANGE, DT_SHIFT1, OnDatetimechangeShift)
	ON_NOTIFY(DTN_DATETIMECHANGE, DT_SHIFT2, OnDatetimechangeShift)
	ON_NOTIFY(DTN_DATETIMECHANGE, DT_SHIFT3, OnDatetimechangeShift)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTimeDlg message handlers

BOOL CTimeDlg::OnInitDialog() 
{
	using namespace FoxjetDatabase;

	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_LINE);
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	
	ASSERT (pLine);
	
	GetLineRecords (vLines);

	for (int i = 0; i < vLines.GetSize (); i++) {
		LINESTRUCT line = vLines [i];

		if (LPFJSYSTEM pSystem = FindSystem (line.m_lID)) {
			int nIndex = pLine->AddString (line.m_strName);

			m_vCodes.Add (CShiftCode (pSystem));
			pLine->SetItemData (nIndex, m_vCodes.GetSize () - 1);

			if (m_lLineID == line.m_lID)
				pLine->SetCurSel (nIndex);
		}
	}

	if (pLine->GetCurSel () == CB_ERR && pLine->GetCount ())
		pLine->SetCurSel (0);

	BOOL bResult = FoxjetCommon::CEliteDlg::OnInitDialog();
	InitShiftCodes ();

	ASSERT (GetDlgItem (BTN_APPLY));
	GetDlgItem (BTN_APPLY)->EnableWindow (FALSE);

	return bResult;
}

void CTimeDlg::InitShiftCodes ()
{
	const CShiftCode s = GetCurSelShiftCode ();
	struct 
	{
		CDateTimeCtrl *			m_pCtrl;
		const COleDateTime *	m_pDT;
	} shiftmap [] =
	{
		{ (CDateTimeCtrl *)GetDlgItem (DT_SHIFT1),	&s.m_dtShift1, },
		{ (CDateTimeCtrl *)GetDlgItem (DT_SHIFT2),	&s.m_dtShift2, },
		{ (CDateTimeCtrl *)GetDlgItem (DT_SHIFT3),	&s.m_dtShift3, },
	};

	for (int i = 0; i < (sizeof (shiftmap) / sizeof (shiftmap [0])); i++) {
		ASSERT (shiftmap [i].m_pCtrl);
		ASSERT (shiftmap [i].m_pDT);

		shiftmap [i].m_pCtrl->SetFormat (_T ("HH:mm"));
	}
}

void CTimeDlg::OnSelchangeLine() 
{
	UpdateData (FALSE);
	ASSERT (GetDlgItem (BTN_APPLY));
	GetDlgItem (BTN_APPLY)->EnableWindow (FALSE);
}

void CTimeDlg::OnApply() 
{
	if (UpdateData (TRUE)) {
		CShiftCode s = GetCurSelShiftCode ();
		LPFJSYSTEM pSystem = FindSystem (s.m_lLineID);

		ASSERT (pSystem);
		VERIFY (UpdateSystem (pSystem, s));

		ASSERT (GetDlgItem (BTN_APPLY));
		GetDlgItem (BTN_APPLY)->EnableWindow (FALSE);
	}
}

bool CTimeDlg::UpdateSystem (LPFJSYSTEM pSystem, const CShiftCode & code)
{
	SETTINGSSTRUCT s;
	char sz [512] = { 0 };
	COdbcDatabase & db = GetDB ();

	pSystem->lShift1			= (code.m_dtShift1.GetHour () * 60) + code.m_dtShift1.GetMinute ();
	pSystem->lShift2			= (code.m_dtShift2.GetHour () * 60) + code.m_dtShift2.GetMinute ();
	pSystem->lShift3			= (code.m_dtShift3.GetHour () * 60) + code.m_dtShift3.GetMinute ();
	strncpy (pSystem->sShiftCode1, w2a (code.m_strShiftCode1), FJSYS_SHIFT_CODE_SIZE);
	strncpy (pSystem->sShiftCode2, w2a (code.m_strShiftCode2), FJSYS_SHIFT_CODE_SIZE);
	strncpy (pSystem->sShiftCode3, w2a (code.m_strShiftCode3), FJSYS_SHIFT_CODE_SIZE);

	fj_SystemTimeToString (pSystem, sz);
	TRACEF (sz);

	// strip off photocell params here
	const CString strToken = _T ("FIRST_DAY=");
	const CString strData = strToken + Extract (a2w (sz), strToken);
	TRACEF (strData);

	s.m_lLineID = code.m_lLineID;
	s.m_strKey	= ListGlobals::Keys::m_strTime;
	s.m_strData = strData;

	if (!UpdateSettingsRecord (db, s))
		return AddSettingsRecord (db, s);

	return true;
}

void CTimeDlg::OnOK()
{
	if (UpdateData (TRUE)) {
		CShiftCode code = GetCurSelShiftCode ();
		LPFJSYSTEM pSystem = FindSystem (code.m_lLineID);
	
		ASSERT (pSystem);
		VERIFY (UpdateSystem (pSystem, code));
		m_lLineID = code.m_lLineID;

		FoxjetCommon::CEliteDlg::OnOK ();
	}
}

void CTimeDlg::EnableApply ()
{
	ASSERT (GetDlgItem (BTN_APPLY));
	GetDlgItem (BTN_APPLY)->EnableWindow (TRUE);
}

void CTimeDlg::OnDatetimechangeShift(NMHDR* pNMHDR, LRESULT* pResult) 
{
	EnableApply ();

	if (pResult) 
		* pResult = 0;
}

ULONG CTimeDlg::TimeToLong (const COleDateTime & tm)
{
	return (tm.GetHour () * 10000) + (tm.GetMinute () * 100) + tm.GetSecond ();
}
