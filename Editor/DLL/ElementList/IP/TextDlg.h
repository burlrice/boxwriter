// TextDlg.h: interface for the CTextDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TextDLG_H__92D783BC_3D25_46C2_95D7_9083250D35C4__INCLUDED_)
#define AFX_TextDLG_H__92D783BC_3D25_46C2_95D7_9083250D35C4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"
#include "ElementDlg.h"
#include "TextElement.h"

class CTextDlg : public CElementDlg  
{
// Construction
public:
	CTextDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa,
		const FoxjetCommon::CElementList * pList = NULL, CWnd* pParent = NULL);   
	CTextDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa, 
		UINT nDlgID, const FoxjetCommon::CElementList * pList = NULL, 
		CWnd* pParent = NULL);   

// Dialog Data
	//{{AFX_DATA(CTextDlg)
	enum { IDD = IDD_TEXT };
	CString	m_strData;
	//}}AFX_DATA

	LONG	m_lBold;
	LONG	m_lWidth;
	LONG	m_lGap;
	CString m_strFont;
	int		m_nAlign;

	virtual UINT GetDefCtrlID () const { return TXT_TEXT; }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTextDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	FoxjetIpElements::CTextElement & GetElement ();
	const FoxjetIpElements::CTextElement & GetElement () const;

	virtual int BuildElement ();

	// Generated message map functions
	//{{AFX_MSG(CTextDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_TextDLG_H__92D783BC_3D25_46C2_95D7_9083250D35C4__INCLUDED_)
