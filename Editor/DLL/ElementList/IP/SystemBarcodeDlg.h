#if !defined(AFX_SYSTEMBARCODEDLG_H__B329C185_0D1A_43B2_AF36_B7D24FF1347E__INCLUDED_)
#define AFX_SYSTEMBARCODEDLG_H__B329C185_0D1A_43B2_AF36_B7D24FF1347E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SystemBarcodeDlg.h : header file
//

#include "Resource.h"
#include "ListCtrlImp.h"
#include "Utils.h"

FORWARD_DECLARE (fjsysbarcode, FJSYSBARCODE)
FORWARD_DECLARE (fjsystem, FJSYSTEM)

/////////////////////////////////////////////////////////////////////////////
// CSystemBarcodeDlg dialog

namespace FoxjetIpElements
{
	class ELEMENT_API CSystemBarcodeDlg : public FoxjetCommon::CEliteDlg
	{
	// Construction
	public:
		CSystemBarcodeDlg(CWnd* pParent = NULL);   // standard constructor
		virtual ~CSystemBarcodeDlg ();

	// Dialog Data
		//{{AFX_DATA(CSystemBarcodeDlg)
		enum { IDD = IDD_SYSTEMBARCODE };
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA

		FoxjetDatabase::HEADTYPE	m_nHeadType;
		ULONG						m_lLineID;
		int							m_nType;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CSystemBarcodeDlg)
	public:
	protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
	protected:

		class CBarcodeItem : public ItiLibrary::ListCtrlImp::CItem
		{
		public:
			CBarcodeItem (LPFJSYSBARCODE pBC, int nIndex);
			CBarcodeItem (int nType, int nIndex, int nHeight, bool bChecksum,
				int nHBearer, int nVBearer, int nQuietzone, int nNarrowbar,
				int nBleed1, int nBleed2, int nBleed3, int nBleed4);
			virtual ~CBarcodeItem ();

			virtual int Compare (const ItiLibrary::ListCtrlImp::CItem & rhs, int nColumn) const;
			virtual CString GetDispText (int nColumn) const;

			CString m_strName;
			int		m_lBCFlags;
			int		m_lBCHeight;
			bool	m_bCheckDigit;
			long	m_lHor_Bearer;
			long	m_lVer_Bearer;
			long	m_lQuietZone;
			long	m_lBarVal [5];
			int		m_nIndex;
		};

		virtual void OnCancel ();

		ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
		bool m_bUpdated;

		FoxjetDatabase::HEADTYPE GetCurSelHeadType () const;
		int GetCurSelLineIndex () const;
		LPFJSYSTEM GetCurSystem () const;
		void AssignDefs (CBarcodeItem * pvDefs, int nParams, FoxjetDatabase::HEADTYPE type, bool bSetName = true);

		// Generated message map functions
		//{{AFX_MSG(CSystemBarcodeDlg)
		virtual BOOL OnInitDialog();
		afx_msg void OnSelchangeSystem();
		afx_msg void OnEdit();
		afx_msg void OnDblclickBarcode(NMHDR* pNMHDR, LRESULT* pResult);
		afx_msg void OnSelchangeheadtype();
		afx_msg void OnLoad();
	//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace FoxjetIpElements

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SYSTEMBARCODEDLG_H__B329C185_0D1A_43B2_AF36_B7D24FF1347E__INCLUDED_)
