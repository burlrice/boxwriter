// List.cpp: implementation of the CElementList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "List.h"
#include "CopyArray.h"
#include "resource.h"
#include "Debug.h"
#include "Edit.h"
#include "Coord.h"
#include "Database.h"
#include "OdbcDatabase.h"
#include "Registry.h"
#include "Alias.h"
#include "FileExt.h"
#include "Extern.h"
#include "ListImp.h"
#include "ItiLibrary.h"
#include "TextElement.h"
#include "Compare.h"
#include "Utils.h"
#include "fj_system.h"
#include "fj_printhead.h"
#include "fj_label.h"
#include "fj_export.h"
#include "Parse.h"
#include "fj_socket.h"
#include "DynamicTableDlg.h"
#include "NetCommDlg.h"
#include "BarcodeElement.h"
#include "DataMatrixElement.h"
#include "StrobeDlg.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;
using namespace FoxjetIpElements;

extern CArray <LPFJSYSTEM, LPFJSYSTEM> vIpSystems;

ELEMENT_API bool FoxjetCommon::operator == (const CElementList & lhs, const CElementList& rhs)
{
	const CString strLhs = lhs.ToString (lhs.m_version);
	const CString strRhs = rhs.ToString (rhs.m_version);

	TRACEF ("Comparing: \n\t[" + strLhs + "]\n\t[" + strRhs + "]");
	return strLhs == strRhs;
}

ELEMENT_API bool FoxjetCommon::operator != (const CElementList & lhs, const CElementList& rhs)
{
	return !(lhs == rhs);
}

static LPFJPRINTHEAD GetDefHead ()
{
	for (int i = 0; i < ::vIpSystems.GetSize (); i++) {
		for (int j = 0; j < FJSYS_NUMBER_PRINTERS; j++) {
			if (LPFJPRINTHEAD pHead = ::vIpSystems [i]->gmMembers [j].pfph) 
				return pHead;
		}
	}

	ASSERT (0);
	return NULL;
}

CElementList::CElementList(bool bAutoDelete)
:	m_pa (10000, 4000), m_bAutoDelete (bAutoDelete), 
	m_units (INCHES), 
	m_head (GetDefHead ()),
	m_pLabel (fj_LabelNew ()),
	m_bSnap (true),
	CPtrArray ()
{
	fj_GetDllVersion (m_version);
}

CElementList::CElementList (const CElementList & rhs)
:	m_pa (rhs.m_pa),
	m_bAutoDelete (rhs.m_bAutoDelete),
	m_units (rhs.m_units),
	m_head (rhs.m_head),
	m_pLabel (NULL),
	m_bSnap (rhs.m_bSnap),
	m_version (rhs.m_version)
{
	for (int i = 0; i < rhs.GetSize (); i++) {
		const CBaseElement & e = rhs.GetObject (i);
		CBaseElement * p = CopyElement (&e, &GetHead (), false);
		ASSERT (p);
		Add (p);
	}

	SetUserDefined (rhs.GetUserDefined ());
}

CElementList & CElementList::operator = (const CElementList & rhs)
{
	if (this != &rhs) {
		m_pa				= rhs.m_pa;
		m_bAutoDelete		= rhs.m_bAutoDelete;
		m_units				= rhs.m_units;
		m_head				= rhs.m_head;
		m_version			= rhs.m_version;
		m_bSnap				= rhs.m_bSnap;

		DeleteAllElements ();

		SetUserDefined (rhs.GetUserDefined ());

		for (int i = 0; i < rhs.GetSize (); i++) {
			const CBaseElement & e = rhs.GetObject (i);
			CBaseElement * p = CopyElement (&e, &GetHead (), false);
			ASSERT (p);
			Add (p);
		}
	}

	return * this;
}

CElementList::~CElementList()
{
	SetLabel (NULL);

	if (m_bAutoDelete) 
		DeleteAllElements ();
}

bool CElementList::SetLabel (LPFJLABEL pLabel)
{
	ULONG lLineID = -1;

	if (m_pLabel) {
		fj_LabelDestroy (m_pLabel);
		m_pLabel = NULL;
	}

	if (pLabel) 
		m_pLabel = pLabel;

	for (int i = 0; i < GetSize (); i++) {
		CIpBaseElement * p = DYNAMIC_DOWNCAST (CIpBaseElement, &GetObject (i));

		ASSERT (p);
		p->SetLabel (m_pLabel);

		lLineID = p->GetLineID ();
	}

	return m_pLabel != NULL;
}
 
const LPFJLABEL CElementList::GetLabel () const
{
	ASSERT (m_pLabel);
	return m_pLabel;
}

CBaseElement & CElementList::operator [] (int nIndex)
{
	return GetObject (nIndex);
}

const CBaseElement & CElementList::operator [] (int nIndex) const
{
	return GetObject (nIndex);
}

CBaseElement & CElementList::GetObject (int nIndex)
{
	#ifdef _DEBUG
	ASSERT (nIndex >= 0 && nIndex < GetSize ());
	ASSERT (GetAt (nIndex) != NULL);
	#endif //_DEBUG

	return * (CBaseElement *)GetAt (nIndex);
}

const CBaseElement & CElementList::GetObject (int nIndex) const
{
	#ifdef _DEBUG
	ASSERT (nIndex >= 0 && nIndex < GetSize ());
	ASSERT (GetAt (nIndex) != NULL);
	#endif //_DEBUG

	return * (CBaseElement *)GetAt (nIndex);
}

void CElementList::DeleteAllElements()
{
	for (int i = 0; i < GetSize (); i++) {
		CBaseElement * p = &GetObject (i);
		delete p;
	}

	RemoveAll ();
}

CSize CElementList::GetProductArea () const
{
	return m_pa;
}

bool CElementList::SetProductArea (const CSize & pa, const CSize & sizePanel)
{
	if ((pa.cx >= MIN_PRODLEN && pa.cx <= MAX_PRODLEN) &&
		(pa.cy >= MIN_PRODHEIGHT && pa.cy <= MAX_PRODHEIGHT))
	{
		m_pa = pa;

		if (m_pLabel && sizePanel != CSize (0, 0)) {
			m_pLabel->fBoxWidth = (float)sizePanel.cx / 1000.0F;
			m_pLabel->fBoxHeight = (float)sizePanel.cy / 1000.0F;
		}

		return true;
	}

	return false;
}

// returns the number of tokens successfully parsed
int CElementList::FromString(const CString & str, FoxjetCommon::CVersion & ver, bool bDeleteExisiting)
{
	int nCount = 0;
	CMap <UINT, UINT, UINT, UINT> id;
	CStringArray v;

	ParsePackets (str, v);

	if (bDeleteExisiting)
		DeleteAllElements ();

	fj_GetDllVersion ((LPVERSIONSTRUCT)m_version); 
	ver = m_version;

	for (int i = 0; i < v.GetSize (); i++) {
		CStringArray vPacket;
		//CString strSeg = Convert (v [i], m_version);
		CString & strSeg = v [i];

		#if defined( __SHOW_PARSE__ ) && defined( _DEBUG )
		TRACEF ("segment: \"" + strSeg + "\"");
		#endif

		Tokenize (strSeg, vPacket);

		if (vPacket.GetSize ()) { 
			CString & strToken = vPacket [0];
			int nType = GetElementType (strToken);

			if (nType != -1) {
				// note that embedded elements must have
				// a different prefix than a normal
				// element for this to work
				const CBaseElement & def = ListGlobals::defElements.GetElement (nType);
				CBaseElement * p = CopyElement (&def, &GetHead (), false);
				
				p->SetSnap (GetSnap ());

				if (!p->FromString (strSeg, m_version)) {
					TRACEF ("FromString failed: " + strSeg);
					delete p;
				}
				else {
					UINT nOldID = p->GetID ();
					UINT nNewID = ForceUniqueID (* this, * p);

					if (nOldID != nNewID)
						id.SetAt (nOldID, nNewID);

					ASSERT (p);
					Add (p);
					nCount++;
				}
			}
			else if (ParsePacket (* this, strSeg, ver))
				nCount++;
			else {
				if (strSeg.Find (_T ("Timestamp")) == -1)
					TRACEF ("Could not parse: " + strSeg);
			}
		}
	}

/*
	if (!nCount) {
		CBitmappedTextElement * pText = (CBitmappedTextElement *)CreateElement (TEXT);

		if (pText) {
			pText->SetDefaultData (str);
			Add (pText);
			nCount++;
		}
		else
			delete pText;
	}
*/

	return nCount;
}

CString CElementList::ToString(const FoxjetCommon::CVersion &ver, bool bBreakTokens, const CString & strName) const
{
	CString str;
	CStringArray v;

	v.Add (_T ("Timestamp"));
	v.Add (CTime::GetCurrentTime ().Format (_T ("%c")));

	str.Format (_T ("%s{ProdArea,%u,%u,%u,%d}"), 
		(bBreakTokens ? _T ("\n") : _T ("")), 
		GetProductArea ().cx, GetProductArea ().cy,
		(UINT)GetUnits (), GetHead ().m_lID);

	if (m_pLabel) {
		char szLabel [512] = { 0 };

		strncpy (m_pLabel->strName, w2a (strName), FJLABEL_NAME_SIZE);
		fj_LabelToStr (m_pLabel, szLabel);
		str = szLabel + str;

		if (bBreakTokens)
			str += (TCHAR)'\n';
	}

	str = ver.ToString () + FoxjetDatabase::ToString (v) + str;

	if (bBreakTokens)
		str += (TCHAR)'\n';

	for (int i = 0; i < GetSize (); i++) {
		const CBaseElement & e = GetObject (i);
		const CString strElement = Convert (e.ToString (m_version), m_version, ver);

		str += strElement;

		if (bBreakTokens)
			str += (TCHAR)'\n';
	}

	return str;
//	return _T ("{") + str + _T ("}");
}

bool CElementList::SetUserDefined (const CString & str)
{
	CStringArray v;
	bool bResult = false;

	ParsePackets (str, v);

	for (int i = 0; i < v.GetSize (); i++) {
		CString strKey = GetKey (v [i]);

		if (!strKey.CompareNoCase (a2w (fj_LabelID))) {
			if (ParsePacket (* this, v [i], m_version))
				bResult |= true;
		}
		else if (!strKey.CompareNoCase (_T ("Timestamp"))) 
			bResult |= ParseTimestamp (* this, str);
	}

	return bResult;
}

const CString CElementList::GetUserDefined () const
{
	char sz [512] = { 0 };

	if (m_pLabel) 
		fj_LabelToStr (m_pLabel, sz);

	return a2w (sz);
}

UNITS CElementList::GetUnits () const
{
	return m_units;
}

bool CElementList::SetUnits (UNITS units)
{
	m_units = units;
	return true;
}

bool CElementList::GetFonts (CArray <CPrinterFont, CPrinterFont &> & v) const
{
	return m_head.GetFonts (v);
}

bool CElementList::SetHead (const FoxjetDatabase::HEADSTRUCT & head)
{
	CString strID;

	strID.Format (_T ("%d"), head.m_lID);

	// make sure the head exists
	for (int i = 0; i < ::vIpSystems.GetSize (); i++) {
		for (int j = 0; j < FJSYS_NUMBER_PRINTERS; j++) {
			if (LPFJPRINTHEAD pHead = ::vIpSystems [i]->gmMembers [j].pfph) {
				if (pHead->strID == strID) {
					m_head = CHead (pHead);
					return true;
				}
			}
		}
	}

	return false;
}

int CElementList::GetNextID() const
{
	UINT nID = 0;

	for (int i = 0; i < GetSize (); i++) 
		nID = max (GetObject (i).GetID (), nID);

	return nID + 1;
}


bool CElementList::Load (const CString & strFile, CVersion & ver, FoxjetDatabase::MESSAGESTRUCT *pMsg) 
{
	if (pMsg != NULL) {
		FromString (pMsg->m_strData, ver);
		ver = m_version;
	}
	else 
		return Load (strFile, ver);

	return true;
}

bool CElementList::Load (const CString & strFile, CVersion & ver)
{
	using namespace FoxjetFile;

	DOCTYPE type = GetFileType (strFile);

	if (type == MESSAGE || type == UNKNOWNDOCTYPE) {
		CString strMessage = FoxjetFile::GetFileTitle (strFile);
		CString strHead = FoxjetFile::GetFileHead (strFile);
		FoxjetDatabase::COdbcDatabase & database = ListGlobals::GetDB ();
		ULONG lHeadID = GetHeadID (strHead);
		MESSAGESTRUCT msg;

		TRACEF (CString ("\nLoad : ") +
			"\n\tstrFile    = " + strFile +
			"\n\tstrMessage = " + strMessage +
			"\n\tstrHead    = " + strHead);

		// TODO: strMessage should be the head's uid

		if (GetMessageRecord (database, strMessage, lHeadID, msg)) {
			FromString (msg.m_strData, ver);
			ver = m_version;
			return true;
		}
	}
	return false;
}

bool CElementList::Store (const CString & strFile, const CVersion & ver) const
{
	using namespace FoxjetFile;

	DOCTYPE type = GetFileType (strFile);

	if (type == MESSAGE || type == UNKNOWNDOCTYPE) {
		CString strMessage = GetFileTitle (strFile);
		CString strHead = GetFileHead (strFile);
		FoxjetDatabase::COdbcDatabase & database = ListGlobals::GetDB ();
		ULONG lHeadID = GetHeadID (strHead);
		MESSAGESTRUCT msg;

		TRACEF (CString ("\nStore : ") +
			"\n\tstrFile    = " + strFile +
			"\n\tstrHead    = " + strHead +
			"\n\tstrMessage = " + strMessage);
		ASSERT (lHeadID != -1);

		ASSERT ("strMessage should be the head's uid" == 0);

		if (GetMessageRecord (database, strMessage, lHeadID, msg)) {
			msg.m_strData = ToString (ver);

			return UpdateMessageRecord (database, msg);
		}
		else { 
			// it doesn't exist in the database, so add it
			msg.m_lHeadID = lHeadID;
			msg.m_strName = strMessage;
			msg.m_strData = ToString (ver);

			return AddMessageRecord (database, msg);
		}
	}

	return false;
}


int CElementList::FindID(UINT nID) const
{
	for (int i = 0; i < GetSize (); i++) 
		if (GetObject (i).GetID () == nID)
			return i;

	return -1;
}

const FoxjetCommon::CVersion & CElementList::GetVersion () const
{
	return m_version;
}

bool CElementList::SetVersion (const FoxjetCommon::CVersion & ver)
{
//	ASSERT (ver.IsSynonym (GetElementListVersion ()));

	m_version = GetElementListVersion ();
	return true;
}

int CElementList::GetMaxElements () 
{
	return 100;
}

bool CElementList::IsMaxedOut () const 
{ 
	return !CanAdd (0);
}

bool CElementList::CanAdd (int nElements) const
{
	int nSize = GetSize () + nElements;
	int nMax = GetMaxElements ();
	bool bResult = (nSize <= nMax);

	return bResult;
}


ELEMENT_API void FoxjetCommon::ToString (FoxjetDatabase::COdbcDatabase & db, const FoxjetDatabase::HEADSTRUCT & h, const FoxjetDatabase::LINESTRUCT * pLine, int nSection, CString &strBuffer)
{
	const TCHAR cBreak = 0x0c;
	float fEncoderWheel = (float)h.m_nHorzRes;
	int nEncoderDivisor = h.m_nEncoderDivisor;
	float fPrintRes = (float)((fEncoderWheel * 2.0) / (float)nEncoderDivisor);
	long lAutoPrint = (long)(fPrintRes * (float)h.m_dPhotocellRate);

	// set info into system. UNITS=ENGLISH_INCHES;ENCODER=T;ENCODER_COUNT=n.n;CONVEYOR_SPEED=n.n;
	//		PRINT_RESOLUTION=n.n;INK_SIZE=n.n;INK_COST=n.n;

	// get info of this print head. returned with PRINTER_ID=abc;LONGNAME=abc;SHORTNAME=abc;SERIAL=abc;
	//		DIRECTION=RIGHT_TO_LEFT;SLANTANGLE=n.n;HEIGHT=n.n;DISTANCE=n.n;UNITS=ENGLISH_INCHES;
	//		MSGSTORE_SIZE=n; 
	CString sKeyVal;
	CTime CurTime = CTime::GetCurrentTime();
	switch ( nSection )
	{
		case IPC_SET_PH_TYPE:
			switch (h.m_nHeadType) {
				case FoxjetDatabase::UJ2_352_32:				strBuffer.Format (_T ("%i"), 0);	break;
				case FoxjetDatabase::UJ2_96_32:					strBuffer.Format (_T ("%i"), 2);	break;
				case FoxjetDatabase::UJ2_192_32:				strBuffer.Format (_T ("%i"), 1);	break;
				case FoxjetDatabase::ALPHA_CODER:				strBuffer.Format (_T ("%i"), 6);	break;
				default:
				case FoxjetDatabase::GRAPHICS_768_256:			strBuffer.Format (_T ("%i"), 3);	break;
				case FoxjetDatabase::GRAPHICS_384_128:			strBuffer.Format (_T ("%i"), 4);	break;
				case FoxjetDatabase::UJ2_192_32NP:				strBuffer.Format (_T ("%i"), 5);	break;

				case FoxjetDatabase::GRAPHICS_768_256_L330:		strBuffer.Format (_T ("%i"), 7);	break;
				case FoxjetDatabase::GRAPHICS_768_256_L310:		strBuffer.Format (_T ("%i"), 8);	break;
				case FoxjetDatabase::GRAPHICS_768_256_0:		strBuffer.Format (_T ("%i"), 9);	break;
			}
			break;
		case IPC_SET_PRINTER_INFO:
			{
				sKeyVal.Format (_T ("DIRECTION=%s;"), (h.m_nDirection == FoxjetDatabase::LTOR ? _T( "LEFT_TO_RIGHT" ) : _T ("RIGHT_TO_LEFT" )));
				strBuffer += sKeyVal;		//TRACEF (sKeyVal);
				sKeyVal.Format (_T ("SLANTVALUE=%i;"), CalculateNFactor (h.m_dHeadAngle, fPrintRes));
				strBuffer += sKeyVal;		TRACEF (sKeyVal);
				sKeyVal.Format (_T ("SLANTANGLE=%0.1f;"), h.m_dHeadAngle);
				strBuffer += sKeyVal;		TRACEF (sKeyVal);
				sKeyVal.Format (_T ("DISTANCE=%0.1f;"), (( double )h.m_lPhotocellDelay / 1000.0));
				strBuffer += sKeyVal;
				sKeyVal.Format (_T ("HEIGHT=%0.1f;"), ((double)h.m_lRelativeHeight/ 1000.0));
				strBuffer += sKeyVal;

				sKeyVal.Format (_T ("PHOTOCELL_BACK=%c;"), ( (h.m_nPhotocell == FoxjetDatabase::INTERNAL_PC ) ) ? 'T':'F');
				strBuffer += sKeyVal;		//TRACEF (sKeyVal);
				sKeyVal.Format (_T ("DOUBLE_PULSE=%c;"), h.m_bDoublePulse ? 'T' : 'F');
				strBuffer += sKeyVal;		//TRACEF (sKeyVal);

				{ // pc repeater
					bool bInternal = GetPcRepeater (db, h.m_lID);
					
					sKeyVal.Format (_T ("PHOTOCELL_INTERNAL=%c;"), bInternal ? 'T':'F');
					strBuffer += sKeyVal;		//TRACEF (sKeyVal);

					sKeyVal.Format (_T ("OP_AUTOPRINT=%u;"), lAutoPrint); 
					strBuffer += sKeyVal;		//TRACEF (sKeyVal);
				}

				bool bSaveDynData = GetSaveDynData (db, h.m_lID);
				sKeyVal.Format (_T ("SAVE_DYN_DATA=%s;"), bSaveDynData ? _T ("T") : _T ("F")); 
				strBuffer += sKeyVal;		//TRACEF (sKeyVal);

				{ // SERIAL_PORT
					using namespace FoxjetIpElements;
					using namespace FoxjetCommon;
					using FoxjetIpElements::CTableItem;
					using FoxjetIpElements::CItemArray;

					//bool bVarData = false;
					CString str;
					//CItemArray vItems;
					NetCommDlg::CCommItem item;
					LPCTSTR lpsz [] = // note: must correspond to CNetCommDlg::CCommItem::USE
					{
						_T ("NONE"), 
						_T ("VARDATA"), 
						_T ("DYNMEM"), 
						_T ("SCANNER_LABEL"), 
						_T ("INFO"), 
					};

					item.FromString (h.m_strSerialParams);
					//CDynamicTableDlg::GetDynamicData (m_BoxPanel.m_lLineID, vItems);

					str.Format (_T ("SERIAL_PORT=%s;"), lpsz [item.m_use % ARRAYSIZE (lpsz)]);
					strBuffer += str;			//TRACEF (str);

					str.Format (_T ("SCANNER_USE_ONCE=%s;"), item.m_bScannerUseOnce ? _T ("T") : _T ("F")); 
					strBuffer += str;			//TRACEF (str);

					str.Format (_T ("BAUD_RATE=%d;"), item.m_dwBaudRate); 
					strBuffer += str;			//TRACEF (str);

					str.Format (_T ("SAVE_SERIAL_DATA=%s;"), item.m_bSaveSerial? _T ("T") : _T ("F")); 
					strBuffer += str;			TRACEF (str);
				}

				sKeyVal.Format (_T ("OP_HEAD_INVERT=%c;"), h.m_bInverted ? 'T' : 'F'); 
				strBuffer += sKeyVal;			//TRACEF (sKeyVal);

				if (pLine) { // set up APS
					bool bDisabled = pLine->m_dAMS_Interval == 0 ? true : false;
					int n [7] = { 0 };
					CString str, strTmp;

					str.Format (_T ("AMS_INTERVAL=%1.1f;"), pLine->m_dAMS_Interval);		//TRACEF (str);

					switch (h.m_nHeadType) {
						case FoxjetDatabase::UJ2_192_32:
						case FoxjetDatabase::UJ2_192_32NP:
						case FoxjetDatabase::UJ2_96_32:
						case FoxjetDatabase::UJ2_352_32:
						case FoxjetDatabase::ALPHA_CODER:
							n [0] = pLine->m_nAMS32_A1;
							n [1] = pLine->m_nAMS32_A2;
							n [2] = pLine->m_nAMS32_A3;
							n [3] = pLine->m_nAMS32_A4;
							n [4] = pLine->m_nAMS32_A5;
							n [5] = pLine->m_nAMS32_A6;
							n [6] = pLine->m_nAMS32_A7;
							break;
						case FoxjetDatabase::GRAPHICS_384_128:
						case FoxjetDatabase::GRAPHICS_768_256:
							n [0] = pLine->m_nAMS256_A1;
							n [1] = pLine->m_nAMS256_A2;
							n [2] = pLine->m_nAMS256_A3;
							n [3] = pLine->m_nAMS256_A4;
							n [4] = pLine->m_nAMS256_A5;
							n [5] = pLine->m_nAMS256_A6;
							n [6] = pLine->m_nAMS256_A7;
							break;
					}

					strTmp.Format (_T ("OP_AMS=%c;"), bDisabled ? 'F' : 'T');	strBuffer += strTmp;	//TRACEF (strTmp);
					strTmp.Format (_T ("OP_AMSA1=%d;"), n [0]);					strBuffer += strTmp;	//TRACEF (strTmp);
					strTmp.Format (_T ("OP_AMSA2=%d;"), n [1]);					strBuffer += strTmp;	//TRACEF (strTmp);
					strTmp.Format (_T ("OP_AMSA3=%d;"), n [2]);					strBuffer += strTmp;	//TRACEF (strTmp);
					strTmp.Format (_T ("OP_AMSA4=%d;"), n [3]);					strBuffer += strTmp;	//TRACEF (strTmp);
					strTmp.Format (_T ("OP_AMSA5=%d;"), n [4]);					strBuffer += strTmp;	//TRACEF (strTmp);
					strTmp.Format (_T ("OP_AMSA6=%d;"), n [5]);					strBuffer += strTmp;	//TRACEF (strTmp);
					strTmp.Format (_T ("OP_AMSA7=%d;"), n [6]);					strBuffer += strTmp;	//TRACEF (strTmp);
				}

				{ // standby settings
					SETTINGSSTRUCT s;
					CString str;

					s.m_lLineID = 0;
					s.m_strKey.Format (_T ("%d, Standby mode"), h.m_lID);
					s.m_strData = _T ("0");

					GetSettingsRecord (db, s.m_lLineID, s.m_strKey, s);
					int n = _tcstoul (s.m_strData, NULL, 10);

					str.Format (_T ("STANDBY_INTERVAL=%1.1f;"), (double)n / 60.0); 
					strBuffer += str;
				}
			}

			break;
		case IPC_SET_SYSTEM_INFO: 
			{
				sKeyVal.Format (_T ("UNITS=ENGLISH_INCHES;TIME=24;INTERNAL_CLOCK=T;"));
				strBuffer += sKeyVal;
				sKeyVal.Format (_T ("SYSTEM_TIME=%s;"), CurTime.Format (_T ("%Y/%m/%d %H:%M:%S")));
				strBuffer += sKeyVal;

				if (h.m_bDigiNET) {
					sKeyVal.Format (_T ("ENCODER_COUNT=%0.1f;"), GetPrintRes (h));
					strBuffer += sKeyVal;		TRACEF (sKeyVal);
					sKeyVal.Format (_T ("PRINT_RESOLUTION=%0.1f;"), (double)h.m_nHorzRes);
					strBuffer += sKeyVal;		TRACEF (sKeyVal);
				}
				else {
					sKeyVal.Format (_T ("PRINT_RESOLUTION=%0.1f;"), fPrintRes);
					strBuffer += sKeyVal;		TRACEF (sKeyVal);
				}

				sKeyVal.Format (_T ("CONVEYOR_SPEED=%0.1f;"), (float) ( (float)h.m_nIntTachSpeed / 5.0) );	// Conveyor Speed is in inches/second.
				strBuffer += sKeyVal;		TRACEF (sKeyVal);
				sKeyVal.Format (_T ("ENCODER=%c;"), (h.m_nEncoder == FoxjetDatabase::INTERNAL_ENC) ? 'F':'T');
				strBuffer += sKeyVal;		TRACEF (sKeyVal);
				sKeyVal.Format (_T ("ENCODER_WHEEL=%0.1f;"), (float)(h.m_nHorzRes));
				strBuffer += sKeyVal;		TRACEF (sKeyVal);
				sKeyVal.Format (_T ("ENCODER_DIVISOR=%i;"), h.m_nEncoderDivisor);
				strBuffer += sKeyVal;		TRACEF (sKeyVal);
				sKeyVal.Format (_T ("APP_VER=%s;"), CVersion (FoxjetCommon::verApp).ToString ());
				strBuffer += sKeyVal;		//TRACEF (sKeyVal);

				if (pLine) {
					FoxjetDatabase::SETTINGSSTRUCT time;
					FoxjetDatabase::SETTINGSSTRUCT date;

					FoxjetIpElements::LoadBarcodeParams (db, verApp);
					CString strBarcodeSettings	= FoxjetIpElements::CBarcodeElement::GetBarcodeSettings (db, pLine->m_lID, h.m_nHeadType);
					strBarcodeSettings	+= cBreak;
					strBarcodeSettings	+= FoxjetIpElements::CDataMatrixElement::GetBarcodeSettings (db, pLine->m_lID, h.m_nHeadType);
					FoxjetDatabase::GetSettingsRecord (db, pLine->m_lID, ListGlobals::Keys::m_strTime, time);
					FoxjetDatabase::GetSettingsRecord (db, pLine->m_lID, ListGlobals::Keys::m_strDays, date);

					strBuffer += time.m_strData + cBreak;
					strBuffer += date.m_strData + cBreak;
					strBuffer += strBarcodeSettings + cBreak;

					{
						CString strTime;

						do {
							strTime = Extract (strBuffer, _T ("SYSTEM_TIME="), _T (";"));
							strBuffer.Replace (_T ("SYSTEM_TIME=") + strTime + _T (";"), _T (""));
						} while (strTime.GetLength ());
					}
				}
			}		
			break;
		case IPC_GET_PRINTER_MODE:
		case IPC_SET_PRINTER_MODE:
			{
				CArray <CStrobeItem, CStrobeItem &> v;

				CStrobeDlg::Load (db, v);

				for (int i = 0; i < v.GetSize (); i++) {
					CStrobeItem & e = v [i];
					HEADERRORTYPE type = CStrobeItem::GetErrorType (e.m_strName);
					CString str, strFmt;

					switch (type) {
					case HEADERROR_READY:		strFmt = _T ("STROBE_MODE_READY_G=%s;STROBE_MODE_READY_Y=%s;STROBE_MODE_READY_R=%s;");			break;
					case HEADERROR_APSCYCLE:	strFmt = _T ("STROBE_MODE_APS_G=%s;STROBE_MODE_APS_Y=%s;STROBE_MODE_APS_R=%s;");				break;
					case HEADERROR_LOWTEMP:		strFmt = _T ("STROBE_MODE_LOWTEMP_G=%s;STROBE_MODE_LOWTEMP_Y=%s;STROBE_MODE_LOWTEMP_R=%s;");	break;
					case HEADERROR_HIGHVOLTAGE:	strFmt = _T ("STROBE_MODE_HV_G=%s;STROBE_MODE_HV_Y=%s;STROBE_MODE_HV_R=%s;");					break;
					case HEADERROR_LOWINK:		strFmt = _T ("STROBE_MODE_LOWINK_G=%s;STROBE_MODE_LOWINK_Y=%s;STROBE_MODE_LOWINK_R=%s;");		break;
					case HEADERROR_OUTOFINK:	strFmt = _T ("STROBE_MODE_OUTOFINK_G=%s;STROBE_MODE_OUTOFINK_Y=%s;STROBE_MODE_OUTOFINK_R=%s;");	break;
					}

					str.Format (strFmt, 
						CStrobeItem::GetStrobeState (e.m_green),
						CStrobeItem::GetStrobeState (e.m_yellow),
						CStrobeItem::GetStrobeState (e.m_red));
					str.Replace (CStrobeItem::GetStrobeState (STROBESTATE_FLASH), CStrobeItem::GetStrobeState (STROBESTATE_ON));
					str.MakeUpper ();
					//TRACEF (str);
					strBuffer += str;
				}

				//TRACEF (strBuffer);
			}
			break;
		default:
			ASSERT (0);
	}
}

static const LPCTSTR lpszPcRepeater = _T ("PC repeater, %d");
ELEMENT_API bool FoxjetCommon::GetPcRepeater (FoxjetDatabase::COdbcDatabase & db, ULONG lHeadID)
{
	SETTINGSSTRUCT pc;

	pc.m_lLineID = 0;
	pc.m_strKey.Format (lpszPcRepeater, lHeadID);
	
	GetSettingsRecord (db, pc.m_lLineID, pc.m_strKey, pc);

	return _ttoi (pc.m_strData) ? true : false;
}

ELEMENT_API void FoxjetCommon::SetPcRepeater (FoxjetDatabase::COdbcDatabase & db, ULONG lHeadID, bool bUse)
{
	DBMANAGETHREADSTATE (db);
	SETTINGSSTRUCT pc;

	pc.m_lLineID = 0;
	pc.m_strKey.Format (lpszPcRepeater, lHeadID);
	pc.m_strData.Format (_T ("%d"), bUse);
	
	if (!UpdateSettingsRecord (db, pc))
		VERIFY (AddSettingsRecord (db, pc));
}

static const LPCTSTR lpszSaveDynData = _T ("SaveDynData, %d");
ELEMENT_API void FoxjetCommon::SetSaveDynData (FoxjetDatabase::COdbcDatabase & db, ULONG lHeadID, bool bUse)
{
	DBMANAGETHREADSTATE (db);
	SETTINGSSTRUCT pc;

	pc.m_lLineID = 0;
	pc.m_strKey.Format (lpszSaveDynData, lHeadID);
	pc.m_strData.Format (_T ("%d"), bUse);
	
	if (!UpdateSettingsRecord (db, pc))
		VERIFY (AddSettingsRecord (db, pc));
}

ELEMENT_API bool FoxjetCommon::GetSaveDynData (FoxjetDatabase::COdbcDatabase & db, ULONG lHeadID)
{
	SETTINGSSTRUCT pc;

	pc.m_lLineID = 0;
	pc.m_strKey.Format (lpszSaveDynData, lHeadID);
	
	GetSettingsRecord (db, pc.m_lLineID, pc.m_strKey, pc);

	return _ttoi (pc.m_strData) ? true : false;
}

ELEMENT_API double FoxjetCommon::GetPrintRes (const FoxjetDatabase::HEADSTRUCT & h)
{
	double dResult = 0;

	switch (h.m_nEncoderDivisor) {
		case 1:	dResult = (double)h.m_nHorzRes;		break; // 300
		default:
		case 3:	dResult = (double)h.m_nHorzRes / 1.5;	break; // 200
		case 2:	dResult = (double)h.m_nHorzRes / 2.0;	break; // 150
	}

	return dResult;
}

