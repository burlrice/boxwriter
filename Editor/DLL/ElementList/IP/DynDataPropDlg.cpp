// DynDataPropDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "Resource.h"
#include "DynDataPropDlg.h"
#include "DatabaseDlg.h"
#include "Extern.h"
#include "TemplExt.h"
#include "Debug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ListGlobals;
using namespace FoxjetIpElements;

/////////////////////////////////////////////////////////////////////////////

using FoxjetIpElements::CDatabaseParams;

CDatabaseParams::CDatabaseParams ()
{
	m_strDSN		= FoxjetCommon::GetDSN ();
	m_strTable		= _T ("Messages");
	m_strField		= _T ("ID");
	m_nRow			= 0;
	m_bSQL			= false;
	m_nSQLType		= 0;
}

CDatabaseParams::CDatabaseParams (const CDatabaseParams & rhs)
{
	m_strDSN		= rhs.m_strDSN;
	m_strTable		= rhs.m_strTable;
	m_strField		= rhs.m_strField;
	m_strKeyField	= rhs.m_strKeyField;
	m_strKeyValue	= rhs.m_strKeyValue;
	m_nRow			= rhs.m_nRow;
	m_bSQL			= rhs.m_bSQL;
	m_strSQL		= rhs.m_strSQL;
	m_nSQLType		= rhs.m_nSQLType;
}

CDatabaseParams & CDatabaseParams::operator = (const CDatabaseParams & rhs)
{
	if (this != &rhs) {
		m_strDSN		= rhs.m_strDSN;
		m_strTable		= rhs.m_strTable;
		m_strField		= rhs.m_strField;
		m_strKeyField	= rhs.m_strKeyField;
		m_strKeyValue	= rhs.m_strKeyValue;
		m_nRow			= rhs.m_nRow;
		m_bSQL			= rhs.m_bSQL;
		m_strSQL		= rhs.m_strSQL;
		m_nSQLType		= rhs.m_nSQLType;
	}

	return * this;
}

CDatabaseParams::~CDatabaseParams ()
{
}

/////////////////////////////////////////////////////////////////////////////
// CDynDataPropDlg dialog


CDynDataPropDlg::CDynDataPropDlg(CWnd* pParent /*=NULL*/)
	: FoxjetCommon::CEliteDlg(IDD_DYNDATAPROPERTIES, pParent)
{
	//{{AFX_DATA_INIT(CDynDataPropDlg)
	m_lID = 0;
	m_bPrompt = FALSE;
	m_strDefData = _T("");
	m_strPrompt = _T("");
	//}}AFX_DATA_INIT

	m_bUseDatabase = FALSE;
}


void CDynDataPropDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDynDataPropDlg)
	DDX_Text(pDX, TXT_ID, m_lID);
	DDX_Check(pDX, CHK_PROMPT, m_bPrompt);
	DDX_Text(pDX, TXT_DEFDATA, m_strDefData);
	DDX_Text(pDX, TXT_PROMPT, m_strPrompt);
	//}}AFX_DATA_MAP

	DDX_Check(pDX, CHK_DATABASE, m_bUseDatabase);
}


BEGIN_MESSAGE_MAP(CDynDataPropDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CDynDataPropDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(BTN_DATABASE, OnDatabase)
	ON_BN_CLICKED(CHK_DATABASE, OnCheckDatabase)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDynDataPropDlg message handlers

BOOL CDynDataPropDlg::OnInitDialog()
{
	FoxjetCommon::CEliteDlg::OnInitDialog();

	// TODO: where does check state come from?

	OnCheckDatabase ();

	return TRUE;
}

void CDynDataPropDlg::OnCheckDatabase ()
{
	CButton * pCheck = (CButton *)GetDlgItem (CHK_DATABASE);
	struct
	{
		UINT	m_nID;
		int		m_nEnabledState;
	} static const map [] = 
	{
//		{ TXT_PROMPT,	0	},
//		{ TXT_DEFDATA,	0	},
		{ BTN_DATABASE,	1	},
	};

	ASSERT (pCheck);

	for (int i = 0; i < ARRAYSIZE (map); i++)
		if (CWnd * p = GetDlgItem (map [i].m_nID))
			p->EnableWindow (pCheck->GetCheck () == map [i].m_nEnabledState);
}

void CDynDataPropDlg::OnDatabase ()
{
	CDatabaseDlg dlg (this);

	dlg.CDatabaseParams::operator = (m_params);

	if (dlg.DoModal () == IDOK) {
		m_params = dlg;

		if (m_params.m_strKeyValue.GetLength ())
			SetDlgItemText (TXT_DEFDATA, m_params.m_strKeyValue);
	}
}
