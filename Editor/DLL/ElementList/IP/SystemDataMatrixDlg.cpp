// SystemDataMatrixDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "SystemDataMatrixDlg.h"
#include "SystemBarcodeDlg.h"
#include "fj_system.h"
#include "Extern.h"
#include "Database.h"
#include "Debug.h"
#include "TemplExt.h"
#include "Registry.h"
#include "BarcodeElement.h"
#include "DataMatrixPropDlg.h"
#include "DataMatrixElement.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace ListGlobals;
using namespace FoxjetIpElements;
using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////

class CSystem
{
public:
	CSystem (const LPFJSYSTEM pSystem = NULL, const CString & strType = _T (""));
	CSystem (const CSystem & rhs);
	CSystem & operator = (const CSystem & rhs);
	virtual ~CSystem ();

	static bool CompareTypes (const LPVOID pLhs, const LPVOID pRhs);

	LPFJSYSTEM					m_pSystem;
	CString						m_strType;
	FoxjetDatabase::LINESTRUCT	m_line;
};

typedef CArray <CSystem *, CSystem *> CSystemArray;
static CArray <CSystemArray *, CSystemArray *> vSystems;

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////

CSystemDataMatrixDlg::CBarcodeItem::CBarcodeItem (LPFJSYSDATAMATRIX pBC, int nIndex)
{
	ASSERT (pBC);

	m_strName		= pBC->strName;
	m_lHeight		= pBC->lHeight;
	m_lHeightBold	= pBC->lHeightBold;
	m_lWidth		= pBC->lWidth;
	m_lWidthBold	= pBC->lWidthBold;
	m_nIndex		= nIndex;
}

CSystemDataMatrixDlg::CBarcodeItem::CBarcodeItem (int nIndex, LONG lHeight, LONG lHeightBold, LONG lWidth, LONG lWidthBold)
{
//	m_strName		= 
	m_lHeight		= lHeight;
	m_lHeightBold	= lHeightBold;
	m_lWidth		= lWidth;
	m_lWidthBold	= lWidthBold;
}

CSystemDataMatrixDlg::CBarcodeItem::~CBarcodeItem ()
{
}

int CSystemDataMatrixDlg::CBarcodeItem::Compare (const ItiLibrary::ListCtrlImp::CItem & cmp, int nColumn) const
{
	const CBarcodeItem & rhs = (CBarcodeItem &)cmp;

	switch (nColumn) {
	case 0:		return m_strName.CompareNoCase (rhs.m_strName);
	case 1:		return m_lHeight		- rhs.m_lHeight;
	case 2:		return m_lHeightBold	- rhs.m_lHeightBold;
	case 3:		return m_lWidth			- rhs.m_lWidth;
	case 4:		return m_lWidthBold		- rhs.m_lWidthBold;
	}

	return 0;
}

CString CSystemDataMatrixDlg::CBarcodeItem::GetDispText (int nColumn) const
{
	CString str;
	switch (nColumn) {
	case 0:		return m_strName;
	case 1:		str.Format (_T ("%d"), m_lHeight);			break;
	case 2:		str.Format (_T ("%d"), m_lHeightBold);		break;
	case 3:		str.Format (_T ("%d"), m_lWidth);			break;
	case 4:		str.Format (_T ("%d"), m_lWidthBold	);		break;
	}

	return str;
}

/////////////////////////////////////////////////////////////////////////////
// CSystemDataMatrixDlg dialog


CSystemDataMatrixDlg::CSystemDataMatrixDlg(CWnd* pParent /*=NULL*/)
:	
	m_nHeadType	(HEADTYPE_FIRST),
	m_lLineID (-1),
	m_bUpdated (false),
	FoxjetCommon::CEliteDlg(CSystemDataMatrixDlg::IDD, pParent)
{
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	CArray <HEADTYPE, HEADTYPE> vTypes;
	CStringArray vTypeNames;
	COdbcDatabase & db = GetDB ();

	GetLineRecords (vLines);

	// some types may share settings.  figure out how many don't
	for (int nType = HEADTYPE_FIRST; nType <= HEADTYPE_LAST; nType++) {
		CString str = CBarcodeElement::GetHeadTypeString ((HEADTYPE)nType);

		if (Find (vTypeNames, str, false) == -1) {
			TRACEF (str);
			vTypeNames.Add (str);
			vTypes.Add ((HEADTYPE)nType);
		}
	}

	for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
		const LINESTRUCT line = vLines [nLine];
		const ULONG lLineID = line.m_lID;
		CSystemArray * pSysArray = new CSystemArray ();

		for (int i = 0; i < vTypes.GetSize (); i++) {
			const HEADTYPE type = vTypes [i];
			const CString strType = CBarcodeElement::GetHeadTypeString (type);
			CString str = CDataMatrixElement::GetBarcodeSettings (db, lLineID, type);
			LPFJSYSTEM pSystem = fj_SystemNew ();

			VERIFY (fj_SystemFromString (pSystem, w2a (str)));
			sprintf (pSystem->strID, "%d", line.m_lID);
			TRACEF (_T ("Creating temp FJSYSTEM: ") + line.m_strName + _T (", ") + strType);

			CSystem * p = new CSystem (pSystem, strType);
			p->m_line = line;
			pSysArray->Add (p);
		}

		::vSystems.Add (pSysArray);
	}
}

CSystemDataMatrixDlg::~CSystemDataMatrixDlg ()
{
	for (int nLine = 0; nLine < ::vSystems.GetSize (); nLine++) {
		if (CSystemArray * pSysArray = ::vSystems [nLine]) {
			for (int i = 0; i < pSysArray->GetSize (); i++) {
				if (CSystem * p = pSysArray->GetAt (i)) {
					TRACEF ("Destroying temp FJSYSTEM: " + p->m_line.m_strName + ", " + p->m_strType);
					fj_SystemDestroy (p->m_pSystem);
					delete p;
				}
			}

			pSysArray->RemoveAll ();
			delete pSysArray;
		}
	}

	::vSystems.RemoveAll ();
}

void CSystemDataMatrixDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSystemDataMatrixDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSystemDataMatrixDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CSystemDataMatrixDlg)
	ON_CBN_SELCHANGE(CB_SYSTEM, OnSelchangeSystem)
	ON_CBN_SELCHANGE(CB_TYPE, OnSelchangeheadtype)
	ON_NOTIFY(NM_DBLCLK, LV_BARCODE, OnDblclkBarcode)
	ON_BN_CLICKED(BTN_LOAD, OnLoad)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSystemDataMatrixDlg message handlers

BOOL CSystemDataMatrixDlg::OnInitDialog() 
{
	using namespace ItiLibrary;
	using namespace FoxjetDatabase;

	CArray <ListCtrlImp::CColumn, ListCtrlImp::CColumn> vCols;
	CComboBox * pLine = (CComboBox *)GetDlgItem (CB_SYSTEM);	
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);	
	COdbcDatabase & db = GetDB ();
	
	ASSERT (pLine);
	ASSERT (pType);
	
	FoxjetCommon::CEliteDlg::OnInitDialog();

	for (int nLine = 0; nLine < ::vSystems.GetSize (); nLine++) {
		if (CSystemArray * pSysArray = ::vSystems [nLine]) {
			if (pSysArray->GetSize ()) {
				if (CSystem * p = pSysArray->GetAt (0)) {
					int nIndex = pLine->AddString (p->m_line.m_strName);
					pLine->SetItemData (nIndex, nLine);

					if (p->m_line.m_lID == m_lLineID)
						pLine->SetCurSel (nIndex);
				}
			}
		}
	}

	for (int nType = HEADTYPE_FIRST; nType <= HEADTYPE_LAST; nType++) {
		CString str = CBarcodeElement::GetHeadTypeString ((HEADTYPE)nType);

		if (pType->FindString (-1, str) == CB_ERR) {
			int nIndex = pType->AddString (str);

			pType->SetItemData (nIndex, nType);

			if ((HEADTYPE)nType == m_nHeadType)
				pType->SetCurSel (nIndex);
		}
	}

	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_NAME),				65));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_DMHEIGHT),			40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_DMHEIGHTBOLD),		40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_DMWIDTH),			40));
	vCols.Add (ListCtrlImp::CColumn (LoadString (IDS_DMWIDTHBOLD),		40));

	m_lv.Create (LV_BARCODE, vCols, this, defElements.m_strRegSection);	

	if (pLine->GetCurSel () == CB_ERR && pLine->GetCount ())
		pLine->SetCurSel (0);
	
	if (pType->GetCurSel () == CB_ERR && pType->GetCount ())
		pType->SetCurSel (0);

	OnSelchangeSystem ();

	return FALSE;
}

void CSystemDataMatrixDlg::OnSelchangeSystem() 
{
	m_lv.DeleteCtrlData ();

	if (LPFJSYSTEM pSystem = GetCurSystem ()) {
		m_lLineID = strtoul (pSystem->strID, NULL, 10);
		m_nHeadType = GetCurSelHeadType ();

		for (int i = 0; i < FJSYS_BARCODES; i++) 
			m_lv.InsertCtrlData (new CBarcodeItem (&(pSystem->bcDataMatrixArray [i]), i));
	}

	OnSelchangeheadtype ();
}

void CSystemDataMatrixDlg::OnSelchangeheadtype() 
{
	HEADTYPE type = GetCurSelHeadType ();
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_DEFS);
	const CString strSection = defElements.m_strRegSection + _T ("\\CSystemDataMatrixDlg");
	int nLastDef = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, strSection, _T ("nDef"), 0);
	struct 
	{
		HEADTYPE	m_type;
		UINT		m_nID;
	} static const map [] = 
	{
		{ HEADTYPE_FIRST,	IDS_DEFBCPARAM1 },
		{ GRAPHICS_768_256, IDS_DEFBCPARAM2 },
//		{ GRAPHICS_768_256, IDS_DEFBCPARAM3 },
//		{ GRAPHICS_768_256, IDS_DEFBCPARAM4 },
//		{ GRAPHICS_768_256, IDS_DEFBCPARAM5 },
	};

	ASSERT (pCB);

	pCB->ResetContent ();

	for (int i = 0; i < (sizeof (map) / sizeof (map [0])); i++) {
		if (map [i].m_type == type) {
			int nIndex = pCB->AddString (LoadString (map [i].m_nID));
			pCB->SetItemData (nIndex, i);

			if (i == nLastDef)
				pCB->SetCurSel (nIndex);
		}
	}

	if (pCB->GetCurSel () == CB_ERR)
		pCB->SetCurSel (0);
}

void CSystemDataMatrixDlg::OnDblclkBarcode(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();

	if (pResult)
		* pResult = 0;
}

LPFJSYSTEM CSystemDataMatrixDlg::GetCurSystem () const
{
	int nLine = GetCurSelLineIndex ();
	HEADTYPE nType = GetCurSelHeadType ();
	CString strType = CBarcodeElement::GetHeadTypeString (nType);

	if (nLine >= 0 && nLine < ::vSystems.GetSize ()) {
		if (CSystemArray * pSysArray = ::vSystems [nLine]) {
			for (int i = 0; i < pSysArray->GetSize (); i++) {
				if (CSystem * p = pSysArray->GetAt (i)) {
					if (!p->m_strType.CompareNoCase (strType)) {
						return p->m_pSystem;
					}
				}
			}
		}
	}

	return NULL;
}

int CSystemDataMatrixDlg::GetCurSelLineIndex () const
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_SYSTEM);	
	
	ASSERT (pCB);

	int nIndex = pCB->GetCurSel ();

	if (nIndex != CB_ERR) 
		return pCB->GetItemData (nIndex);

	return -1;
}

HEADTYPE CSystemDataMatrixDlg::GetCurSelHeadType() const
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_TYPE);	
	
	ASSERT (pCB);

	int nIndex = pCB->GetCurSel ();

	if (nIndex != CB_ERR) 
		return (HEADTYPE)pCB->GetItemData (nIndex);

	return HEADTYPE_FIRST;
}

void CSystemDataMatrixDlg::OnEdit() 
{
	LPFJSYSTEM pSystem = GetCurSystem ();
	HEADTYPE type = GetCurSelHeadType ();
	CLongArray sel = ItiLibrary::GetSelectedItems (m_lv.GetListCtrl ());

	if (!pSystem) {
		MsgBox (* this, LoadString (IDS_NOSYSTEMSELECTED));
		return;
	}

	if (sel.GetSize ()) {
		CDataMatrixPropDlg dlg (this);
	
		CBarcodeItem * p = (CBarcodeItem *)m_lv.GetCtrlData (sel [0]);

		ASSERT (p);
		int nBarcode = p->m_nIndex;

		dlg.m_strName			= p->m_strName;
		dlg.m_lHeight			= p->m_lHeight;
		dlg.m_lHeightBold		= p->m_lHeightBold;
		dlg.m_lWidth			= p->m_lWidth;
		dlg.m_lWidthBold		= p->m_lWidthBold;

		if (dlg.DoModal () == IDOK) {
			char sz [1024] = { 0 };
			ULONG lLineID = atol (pSystem->strID);
			SETTINGSSTRUCT s;

			if (CBarcodeElement::IsValidGlobalIndex (nBarcode)) {
				LPFJSYSDATAMATRIX p = &pSystem->bcDataMatrixArray [nBarcode];

				strncpy (p->strName, w2a (dlg.m_strName), FJSYS_BARCODE_NAME_SIZE);
				p->lHeight			= dlg.m_lHeight;
				p->lHeightBold		= dlg.m_lHeightBold;
				p->lWidth			= dlg.m_lWidth;
				p->lWidthBold		= dlg.m_lWidthBold;
			}

			p->m_strName			= dlg.m_strName;
			p->m_lHeight			= dlg.m_lHeight;
			p->m_lHeightBold		= dlg.m_lHeightBold;
			p->m_lWidth				= dlg.m_lWidth;
			p->m_lWidthBold			= dlg.m_lWidthBold;

			fj_SystemDataMatrixToString (pSystem, sz);
			//TRACEF (sz);
			s.m_lLineID = lLineID;
			s.m_strKey	= ListGlobals::Keys::m_strDataMatrix;
			s.m_strData = a2w (sz);

			VERIFY (CDataMatrixElement::SetBarcodeSettings (GetDB (), lLineID, type, a2w (sz)));

			m_lv.UpdateCtrlData (p);
			m_bUpdated = true;
		}
	}
	else 
		MsgBox (* this, LoadString (IDS_NOBARCODESELECTED));
}

void CSystemDataMatrixDlg::OnLoad() 
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_DEFS);
	HEADTYPE type = GetCurSelHeadType ();
	const CString strSection = defElements.m_strRegSection + _T ("\\CSystemBarcodeDlg");

	ASSERT (pCB);
	int nDefs = pCB->GetItemData (pCB->GetCurSel ());

	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, strSection, _T ("nDef"), nDefs);

	int nResult = MsgBox (* this, LoadString (IDS_CONFIRMOVERWRITEPARAMS), 
		LoadString (IDS_CONFIRM), MB_YESNO);

	if (nResult == IDNO)
		return;

	switch (nDefs) {
	case 0:
		{
			CBarcodeItem defs [] = 
			{
				CBarcodeItem (0, 1, 12, 1, 40),
				CBarcodeItem (1, 1, 12, 1, 40),
				CBarcodeItem (2, 1, 12, 1, 40),
				CBarcodeItem (3, 1, 12, 1, 40),
				CBarcodeItem (4, 1, 12, 1, 40),
			};

			AssignDefs (defs, ARRAYSIZE (defs), type);
		}
		break;
	case 1:
		{
			CBarcodeItem defs [] = 
			{
				CBarcodeItem (0, 1, 12, 1, 40),
				CBarcodeItem (1, 1, 12, 1, 40),
				CBarcodeItem (2, 1, 12, 1, 40),
				CBarcodeItem (3, 1, 12, 1, 40),
				CBarcodeItem (4, 1, 12, 1, 40),
			};

			AssignDefs (defs, ARRAYSIZE (defs), type);
		}
		break;
	}
}

void CSystemDataMatrixDlg::AssignDefs (CBarcodeItem * pvDefs, int nParams, HEADTYPE type, bool bSetName) 
{
	ASSERT (pvDefs);

	LPFJSYSTEM pSystem = GetCurSystem ();
	char sz [1024] = { 0 };
	ULONG lLineID = atol (pSystem->strID);
	SETTINGSSTRUCT s;

	for (int i = 0; i < nParams; i++) {
		CBarcodeItem * p = (CBarcodeItem *)m_lv.GetCtrlData (i);
		const CString strName = p->m_strName;

		* p = pvDefs [i];
		
		if (bSetName) {
//			p->m_strName = strName;
			p->m_strName = CBarcodeElement::GetMagName (i);
		}

		m_lv.UpdateCtrlData (p);

		if (CBarcodeElement::IsValidGlobalIndex (p->m_nIndex)) {
			LPFJSYSDATAMATRIX pSet = &pSystem->bcDataMatrixArray [p->m_nIndex];

			strncpy (pSet->strName, w2a (p->m_strName), FJSYS_BARCODE_NAME_SIZE);
			pSet->lHeight			= p->m_lHeight;
			pSet->lHeightBold		= p->m_lHeightBold;
			pSet->lWidth			= p->m_lWidth;
			pSet->lWidthBold		= p->m_lWidthBold;
		}

		m_lv.UpdateCtrlData (p);
	}

	fj_SystemDataMatrixToString (pSystem, sz);
	//TRACEF (sz);
	s.m_lLineID = lLineID;
	s.m_strKey	= ListGlobals::Keys::m_strDataMatrix;
	s.m_strData = a2w (sz);

	VERIFY (CDataMatrixElement::SetBarcodeSettings (GetDB (), lLineID, type, a2w (sz)));
	m_bUpdated = true;
}
