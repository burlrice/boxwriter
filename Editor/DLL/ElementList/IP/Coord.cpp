// Coord.cpp: implementation of the CCoord class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Coord.h"
#include "Resource.h"
#include "Debug.h"
#include "Compare.h"
#include "Utils.h"

using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

////////////////////////////////////////////////////////////////////////////////

static void DDV_Coord (CDataExchange * pDX, int nIDC, const FoxjetCommon::CCoord & value, 
					   const FoxjetDatabase::HEADSTRUCT & head, DWORD dwType, 
					   const FoxjetCommon::CCoord & coordMin, 
					   const FoxjetCommon::CCoord & coordMax);

////////////////////////////////////////////////////////////////////////////////

ELEMENT_API CRect FoxjetCommon::operator * (const CRect & rc, double dFactor)
{
	return CRect (
		(int)((double)rc.left * dFactor),
		(int)((double)rc.top * dFactor),
		(int)((double)rc.right * dFactor),
		(int)((double)rc.bottom * dFactor));
}

ELEMENT_API CRect FoxjetCommon::operator * (double dFactor, const CRect & rc)
{
	return rc * dFactor;
}

ELEMENT_API CRect FoxjetCommon::operator / (const CRect & rc, double dFactor)
{
	ASSERT (dFactor != 0);

	return CRect (
		(int)((double)rc.left / dFactor),
		(int)((double)rc.top / dFactor),
		(int)((double)rc.right / dFactor),
		(int)((double)rc.bottom / dFactor));
}


ELEMENT_API CPoint FoxjetCommon::operator * (const CPoint & pt, double dFactor)
{
	return CPoint (
		(int)((double)pt.x * dFactor),
		(int)((double)pt.y * dFactor));
}

ELEMENT_API CPoint FoxjetCommon::operator * (double dFactor, const CPoint & pt)
{
	return pt * dFactor;
}

ELEMENT_API CPoint FoxjetCommon::operator / (const CPoint & pt, double dFactor)
{
	ASSERT (dFactor != 0);

	return CPoint (
		(int)((double)pt.x / dFactor),
		(int)((double)pt.y / dFactor));
}

//////////////////////////////////////////////////////////////////////

// "value"'s units are thousandths of an inch
// "units" specifies the units to be displayed in the edit ctrl
// note: nIDC MUST specify an edit control
ELEMENT_API void FoxjetCommon::DDX_Coord (CDataExchange * pDX, int nIDC, 
										  long & value, UNITS units, const FoxjetDatabase::HEADSTRUCT & head,
										  bool bXComponent)
{
	ASSERT (pDX->m_pDlgWnd);
	CWnd * pWnd = pDX->m_pDlgWnd->GetDlgItem (nIDC);
	ASSERT (pWnd);
	//ASSERT (pWnd->IsKindOf (RUNTIME_CLASS (CEdit)));
	CEdit & edit = * (CEdit *)pWnd;

	if (!pDX->m_bSaveAndValidate) {
		CPoint pt (0, 0);

		if (bXComponent) 
			pt.x = value;
		else
			pt.y = value;

		CCoord c (CBaseElement::ThousandthsToLogical (pt, head), units, head);

		if (IsValveHead (head))
			if (units == HEAD_PIXELS) 
				c.m_x = ((double)value / 1000.0) * head.GetRes ();

		edit.SetWindowText (bXComponent ? c.FormatX () : c.FormatY ());
	}
	else {
		if (edit.GetModify ()) {
			CString str;
			edit.GetWindowText (str);
			double d = FoxjetDatabase::_ttof (str);

			if (units == HEAD_PIXELS) {
				if (bXComponent) {
					if (IsValveHead (head))
						value = (int)(d * 1000.0 * (1.0 / head.GetRes ()));
					else
						value = (int)(d * (1000.0 / (double)head.m_nEncoderDivisor) * (1.0 / head.GetRes ()));
				}
				else 
					value = (int)ceil (d * 1000.0 * (head.GetSpan () / head.GetChannels ()));
			}
			else {
				if (bXComponent) {
					CPoint pt = CBaseElement::LogicalToThousandths (
						CCoord::CoordToPoint (d, 0.0, units, head), head);
					value = pt.x;
				}
				else {
					CPoint pt = CBaseElement::LogicalToThousandths (
						CCoord::CoordToPoint (0.0, d, units, head), head);
					value = pt.y;
				}
			}
		}
	}
}

ELEMENT_API void FoxjetCommon::DDV_Coord (CDataExchange * pDX, int nIDC, ULONG x, ULONG y, UNITS units,
										  const FoxjetDatabase::HEADSTRUCT & head, DWORD dwType, 
										  const FoxjetCommon::CCoord & coordMin, 
										  const FoxjetCommon::CCoord & coordMax)
{
	// note: coordMin & coordMax are always in 1000ths

	if (!pDX->m_bSaveAndValidate) 
		return;

	if (units != HEAD_PIXELS) {
		CCoord cMin (coordMin); 
		CCoord cMax (coordMax); 
		CCoord c (x, y, units, head);

		c.m_x = x;
		c.m_y = y;

		if (units == CENTIMETERS || units == METERS) {
			CPoint ptMin	= CBaseElement::ThousandthsToLogical (CPoint ((int)coordMin.m_x, (int)coordMin.m_y), head);
			CPoint ptMax	= CBaseElement::ThousandthsToLogical (CPoint ((int)coordMax.m_x, (int)coordMax.m_y), head);
			CPoint pt		= CBaseElement::ThousandthsToLogical (CPoint (x, y), head);
			
			cMin	= CCoord (ptMin, units, head);
			cMax	= CCoord (ptMax, units, head);
			c		= CCoord (pt, units, head);
		}

		::DDV_Coord (pDX, nIDC, c, head, dwType, cMin, cMax);
		return;
	}

	bool bBoth = ((dwType & DDVCOORDXBOTH) & (dwType & DDVCOORDYBOTH)) ? true : false;
	CCoord fmtMin (coordMin);
	CCoord fmtMax (coordMax);
	CString str;
	CPoint ptValue = CBaseElement::ThousandthsToLogical (CPoint (x, y), head); 
	double dFactor = (double)head.GetRes () / (double)head.GetMaxRes ();
	double dValue [2] =
	{
		CCoord (ptValue, units, head).m_x * dFactor,
		CCoord (ptValue, units, head).m_y * dFactor,
	};

	CPoint ptMin = CBaseElement::ThousandthsToLogical (CPoint ((int)coordMin.m_x, (int)coordMin.m_y), head); 
	CPoint ptMax = CBaseElement::ThousandthsToLogical (CPoint ((int)coordMax.m_x, (int)coordMax.m_y), head); 

	double dMin [2] = 
	{
		CCoord (ptMin, units, head).m_x * dFactor,
		CCoord (ptMin, units, head).m_y * dFactor,
	};
	double dMax [2] = 
	{
		CCoord (ptMax, units, head).m_x * dFactor,
		CCoord (ptMax, units, head).m_y * dFactor,
	};

	if (!IsValveHead (head)) {
		ASSERT (ARRAYSIZE (dValue) == ARRAYSIZE (dMin));
		ASSERT (ARRAYSIZE (dValue) == ARRAYSIZE (dMax));

		for (int i = 0; i < ARRAYSIZE (dValue); i++) {
			dValue [i]	*= (double)head.m_nEncoderDivisor;
			dMin [i]	*= (double)head.m_nEncoderDivisor;
			dMax [i]	*= (double)head.m_nEncoderDivisor;
		}
	}

	fmtMin.m_x = dMin [0];
	fmtMin.m_y = dMin [1];
	fmtMax.m_x = dMax [0];
	fmtMax.m_y = dMax [1];

	if (dwType & DDVCOORDXMIN) {
		if (dValue [0] < dMin [0]) {
			str.Format (LoadString (IDS_DDVCOORDMIN), bBoth ? fmtMin.Format () : fmtMin.FormatX ());
			MsgBox (* pDX->m_pDlgWnd, str, NULL, MB_ICONEXCLAMATION);
			pDX->PrepareEditCtrl (nIDC);
			pDX->Fail ();
			return;
		}
	}

	if (dwType & DDVCOORDXMAX) {
		if (dValue [0] > dMax [0]) {
			str.Format (LoadString (IDS_DDVCOORDMAX), bBoth ? fmtMax.Format () : fmtMax.FormatX ());
			MsgBox (* pDX->m_pDlgWnd, str, NULL, MB_ICONEXCLAMATION);
			pDX->PrepareEditCtrl (nIDC);
			pDX->Fail ();
			return;
		}
	}

	if (dwType & DDVCOORDYMIN) {
		if (dValue [1] < dMin [1]) {
			str.Format (LoadString (IDS_DDVCOORDMIN), bBoth ? fmtMin.Format () : fmtMin.FormatY ());
			MsgBox (* pDX->m_pDlgWnd, str, NULL, MB_ICONEXCLAMATION);
			pDX->PrepareEditCtrl (nIDC);
			pDX->Fail ();
			return;
		}
	}

	if (dwType & DDVCOORDYMAX) {
		if (dValue [1] > dMax [1]) {
			str.Format (LoadString (IDS_DDVCOORDMAX), bBoth ? fmtMax.Format () : fmtMax.FormatY ());
			MsgBox (* pDX->m_pDlgWnd, str, NULL, MB_ICONEXCLAMATION);
			pDX->PrepareEditCtrl (nIDC);
			pDX->Fail ();
			return;
		}
	}
}

void DDV_Coord (CDataExchange * pDX, int nIDC, const FoxjetCommon::CCoord & value, 
				const FoxjetDatabase::HEADSTRUCT & head, DWORD dwType, 
				const FoxjetCommon::CCoord & coordMin, 
				const FoxjetCommon::CCoord & coordMax)
{
	ASSERT (value.GetUnits () != HEAD_PIXELS);

	if (pDX->m_bSaveAndValidate) {
		UNITS units = value.GetUnits ();
		bool bBoth = ((dwType & DDVCOORDXBOTH) & (dwType & DDVCOORDYBOTH)) ? true : false;
		CCoord fmtMin (coordMin);
		CCoord fmtMax (coordMax);
		CString str;

		switch (units) {
		case FEET:
		case INCHES:
			fmtMin.m_x = coordMin.m_x / 1000.0;
			fmtMin.m_y = coordMin.m_y / 1000.0;
			fmtMax.m_x = coordMax.m_x / 1000.0;
			fmtMax.m_y = coordMax.m_y / 1000.0;
			break;
		}

		switch (units) {
		case FEET:
			ASSERT (coordMin.GetUnits () == FEET);
			ASSERT (coordMax.GetUnits () == FEET);

			fmtMin.m_x /= 12.0;
			fmtMin.m_y /= 12.0;
			fmtMax.m_x /= 12.0;
			fmtMax.m_y /= 12.0;
			break;
		/*
		case METERS:
			ASSERT (coordMin.GetUnits () == METERS);
			ASSERT (coordMax.GetUnits () == METERS);

			fmtMin.m_x /= 10.0;
			fmtMin.m_y /= 10.0;
			fmtMax.m_x /= 10.0;
			fmtMax.m_y /= 10.0;
			break;
		*/
		}

		if (dwType & DDVCOORDXMIN) {
			if (value.m_x < coordMin.m_x) {
				str.Format (LoadString (IDS_DDVCOORDMIN), bBoth ? fmtMin.Format () : fmtMin.FormatX ());
				MsgBox (* pDX->m_pDlgWnd, str, NULL, MB_ICONEXCLAMATION);
				pDX->PrepareEditCtrl (nIDC);
				pDX->Fail ();
				return;
			}
		}

		if (dwType & DDVCOORDXMAX) {
			if (value.m_x > coordMax.m_x) {
				str.Format (LoadString (IDS_DDVCOORDMAX), bBoth ? fmtMax.Format () : fmtMax.FormatX ());
				MsgBox (* pDX->m_pDlgWnd, str, NULL, MB_ICONEXCLAMATION);
				pDX->PrepareEditCtrl (nIDC);
				pDX->Fail ();
				return;
			}
		}

		if (dwType & DDVCOORDYMIN) {
			if (value.m_y < coordMin.m_y) {
				str.Format (LoadString (IDS_DDVCOORDMIN), bBoth ? fmtMin.Format () : fmtMin.FormatY ());
				MsgBox (* pDX->m_pDlgWnd, str, NULL, MB_ICONEXCLAMATION);
				pDX->PrepareEditCtrl (nIDC);
				pDX->Fail ();
				return;
			}
		}

		if (dwType & DDVCOORDYMAX) {
			if (value.m_y > coordMax.m_y) {
				str.Format (LoadString (IDS_DDVCOORDMAX), bBoth ? fmtMax.Format () : fmtMax.FormatY ());
				MsgBox (* pDX->m_pDlgWnd, str, NULL, MB_ICONEXCLAMATION);
				pDX->PrepareEditCtrl (nIDC);
				pDX->Fail ();
				return;
			}
		}
	}
}
//////////////////////////////////////////////////////////////////////

static CPoint GetPixelsPerInch ()
{
	HDC hDC = ::GetDC (NULL);
	int n [2] = { ::GetDeviceCaps (hDC, LOGPIXELSX), ::GetDeviceCaps (hDC, LOGPIXELSY) };

	::ReleaseDC (NULL, hDC);

	return CPoint (n [0], n [1]);
}

static const CPoint ptPixels = GetPixelsPerInch ();

const long double dPixelsPerUnit [3][2] = 
{
	{ ptPixels.x,						ptPixels.y							}, // INCHES
	{ (int)((double)ptPixels.y / 2.54),	(int)((double)ptPixels.y / 2.54)	}, // CENTIMETERS
	{ ptPixels.x,						ptPixels.y							}, // HEAD_PIXELS
};
/*
const double dPixelsPerUnit [3][2] = {
	{ 153.8,			153.8			}, // INCHES
	{ (153.8 / 2.54),	(153.8 / 2.54)	}, // CENTIMETERS
	{ 200,				200				}, // HEAD_PIXELS
//	{ 153.8,			0.655 * 153.8			}, // INCHES
//	{ (153.8 / 2.54),	0.63 * (153.8 / 2.54)	}, // CENTIMETERS
//	{ 200,				0.95 * 200				}, // HEAD_PIXELS
};
*/


double CCoord::GetPixelsPerXUnit (UNITS units, const DB::HEADSTRUCT & head)
{
	//long double dStretch = ((long double)head.m_nHorzRes / (long double)head.m_nEncoderDivisor) / (long double)dPixelsPerUnit [HEAD_PIXELS][0];
	long double dStretch = ((long double)head.GetRes ()) / (long double)dPixelsPerUnit [HEAD_PIXELS][0];

#ifdef __WYSIWYGFIX__
	dStretch = 1; 
#endif

	if (units == HEAD_PIXELS) {
		double dPixel	= ::dPixelsPerUnit [2][0];
		double dDot		= (double)head.GetRes ();

		if (FoxjetDatabase::IsValveHead (head)) 
			return (dPixel / dDot) * 200.0;
		else
			return (dPixel / dDot) * ((96.0 / (double)head.m_nEncoderDivisor) * 3.0);
	}

	switch (units) {
	case FEET:		case INCHES:		return ::dPixelsPerUnit [0][0] * dStretch;
	case METERS:	case CENTIMETERS:	return ::dPixelsPerUnit [1][0] * dStretch;
	case HEAD_PIXELS:					return ::dPixelsPerUnit [2][0] * dStretch;
	}

	ASSERT (0);
	return 0;
}

double CCoord::GetPixelsPerYUnit (UNITS units, const DB::HEADSTRUCT & head)
{
	long double dStretch = 1.0;
//dStretch = ((long double)head.m_nHorzRes / (long double)head.m_nEncoderDivisor) / (long double)dPixelsPerUnit [HEAD_PIXELS][0];

	if (units == HEAD_PIXELS) {
		double dPixel	= ::dPixelsPerUnit [2][0];
		double dDot		= (double)head.GetChannels () / (double)head.GetSpan ();
		double dHeight	= head.Height ();

		if (FoxjetDatabase::IsValveHead (head))
			return (dPixel * dDot) / 3.10; // made up number
		else
			return (dPixel * dDot) / 20.48; // made up number
	}

	switch (units) {
	case FEET:		case INCHES:		return ::dPixelsPerUnit [0][1] * dStretch;
	case METERS:	case CENTIMETERS:	return ::dPixelsPerUnit [1][1] * dStretch;
	case HEAD_PIXELS:					return ::dPixelsPerUnit [2][1] * dStretch;
	}

	ASSERT (0);
	return 0;
}


const CCoord CCoord::m_coordMin = CCoord (0, 0);
const CCoord CCoord::m_coordMax = CCoord (INT_MAX, INT_MAX);

CCoord::CCoord (double x, double y)
:	m_x (x), 
	m_y (y), 
	m_units (HEAD_PIXELS)
{
}

CCoord::CCoord()
:	m_x (0), 
	m_y (0), 
	m_units (HEAD_PIXELS)
{
}

CCoord::CCoord (const CCoord & rhs)
:	m_x (rhs.m_x),
	m_y (rhs.m_y),
	m_units (rhs.m_units)
{
}

CCoord & CCoord::operator = (const CCoord & rhs)
{
	if (this != &rhs) {
		m_x = rhs.m_x;
		m_y = rhs.m_y;
		m_units = rhs.m_units;
	}

	return * this;
}

CCoord::CCoord(double x, double y, UNITS units, const FoxjetDatabase::HEADSTRUCT & head, double dZoom)
:	m_x (x), 
	m_y (y), 
	m_units (units)
{
	ASSERT (dZoom > 0.0);
	Translate (dZoom, head);
}

CCoord::CCoord(const CPoint & pt, UNITS units, const FoxjetDatabase::HEADSTRUCT & head, double dZoom)
:	m_x ((double)pt.x), 
	m_y ((double)pt.y), 
	m_units (units)
{
	ASSERT (dZoom > 0.0);
	Translate (dZoom, head);
}

CCoord::~CCoord()
{

}

void CCoord::Translate (double dZoom, const FoxjetDatabase::HEADSTRUCT & head) 
{	
	double x = (m_x / dZoom), y = (m_y / dZoom);

	if (m_units != HEAD_PIXELS) {
		long double d [2] = 
		{
			(long double)GetPixelsPerXUnit (m_units, head),
			(long double)GetPixelsPerYUnit (m_units, head),
		};

		m_x = (long double)x / d [0];
		m_y = (long double)y / d [1];
	}
	else {
		//int nMaxRes = head.GetMaxRes ();
		double dPixel [2] =
		{
			::dPixelsPerUnit [2][0],
			::dPixelsPerUnit [2][0],
		};
		double dDot	[2]	= 
		{
			(double)head.GetRes (),
			(double)head.GetChannels () / (double)head.GetSpan (),
		};

		if (FoxjetDatabase::IsValveHead (head)) {
			//dDot [0] *= ((double)nMaxRes / dDot [0]);

			m_x = (long double)x / (dPixel [0] / dDot [0]);
			m_y = (long double)y / (dPixel [1] / dDot [1]);
		}
		else {
			m_x = ((long double)x / ((dPixel [0] / dDot [0]) / (double)head.m_nEncoderDivisor));
			m_y = (long double)y / (dPixel [1] / dDot [1]);
		}
	}

	switch (m_units) {
	case FEET:
		m_x /= 12.0;
		m_y /= 12.0;
		break;
	case METERS:
		m_x /= 100.0;
		m_y /= 100.0;
		break;
	}
}

CString CCoord::FormatX() const
{
	CString str;
	static CString strUnits [] = {
		LoadString (IDS_HP),
		LoadString (IDS_IN),
		LoadString (IDS_CM),
		LoadString (IDS_FEET),
		LoadString (IDS_METERS),
	};
	
	switch (m_units) {
	case HEAD_PIXELS:
		str.Format (_T ("%u %s"), Round (m_x), (LPCTSTR)strUnits [0]); 
		break;
	case INCHES:
		str.Format (_T ("%.3lf %s"), m_x, (LPCTSTR)strUnits [1]);
		break;
	case CENTIMETERS:
		str.Format (_T ("%.3lf %s"), m_x, (LPCTSTR)strUnits [2]);
		break;
	case FEET:
		str.Format (_T ("%.3lf %s"), m_x, (LPCTSTR)strUnits [3]);
		break;
	case METERS:
		str.Format (_T ("%.3lf %s"), m_x, (LPCTSTR)strUnits [4]);
		break;
	}

	return str;
}

CString CCoord::FormatY() const
{
	CString str;
	static CString strUnits [] = {
		LoadString (IDS_HP),
		LoadString (IDS_IN),
		LoadString (IDS_CM),
		LoadString (IDS_FEET),
		LoadString (IDS_METERS),
	};

	switch (m_units) {
	case HEAD_PIXELS:
		str.Format (_T ("%u %s"), Round (m_y), (LPCTSTR)strUnits [0]); 
		break;
	case INCHES:
		str.Format (_T ("%.3lf %s"), m_y, (LPCTSTR)strUnits [1]);
		break;
	case CENTIMETERS:
		str.Format (_T ("%.3lf %s"), m_y, (LPCTSTR)strUnits [2]);
		break;
	case FEET:
		str.Format (_T ("%.3lf %s"), m_y, (LPCTSTR)strUnits [3]);
		break;
	case METERS:
		str.Format (_T ("%.3lf %s"), m_y, (LPCTSTR)strUnits [4]);
		break;
	}

	return str;
}

CString CCoord::Format() const
{
	return _T ("(") + FormatX () + _T (", ") + FormatY () + _T (")");
}

CPoint CCoord::CoordToPoint(const CCoord &pt, UNITS units, const DB::HEADSTRUCT & head)
{
	long double x = pt.m_x, y = pt.m_y;
	CPoint ptResult;

	switch (pt.m_units) {
	case FEET:
		x *= 12.0;
		y *= 12.0;
		break;
	case METERS:
		x *= 100.0;
		y *= 100.0;
		break;
	}

	if (units != HEAD_PIXELS) {
		ptResult.x = (int)(x * (long double)GetPixelsPerXUnit (units, head));
		ptResult.y = (int)(y * (long double)GetPixelsPerYUnit (units, head));
	}
	else {
		//int nMaxRes = head.GetMaxRes ();
		double dPixel [2] =
		{
			::dPixelsPerUnit [2][0],
			::dPixelsPerUnit [2][0],
		};
		double dDot	[2]	= 
		{
			(double)head.GetRes (),
			(double)head.GetChannels () / (double)head.GetSpan (),
		};

		if (FoxjetDatabase::IsValveHead (head)) {
			ptResult.x = (int)(x * (dPixel [0] / dDot [0]));
			//ptResult.x = DivideAndRound ((double)ptResult.x, ((double)nMaxRes / dDot [0]));

			ptResult.y = (int)(y * (dPixel [1] / dDot [1]));
		}
		else {
			ptResult.x = (int)(x * ((dPixel [0] / dDot [0]) / (double)head.m_nEncoderDivisor));
			//ptResult.x = DivideAndRound ((double)ptResult.x, ((double)nMaxRes / dDot [0]));

			ptResult.y = (int)(y * (dPixel [1] / dDot [1]));
		}
	}

	return ptResult;
}

CPoint CCoord::CoordToPoint (const CPoint & pt, UNITS units, const DB::HEADSTRUCT & head)
{
	return CoordToPoint (pt.x, pt.y, units, head);
}

CPoint CCoord::CoordToPoint (double x, double y, UNITS units, const DB::HEADSTRUCT & head)
{
	CCoord c (0, 0, units, head, 1.0);
	c.m_x = x;
	c.m_y = y;
	return CoordToPoint (c, units, head);
}
