// DynamicTextElement.cpp: implementation of the CDynamicTextElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "list.h"
#include "DynamicTextElement.h"
#include "Utils.h"
#include "DynamicTableDlg.h"
#include "Debug.h"

#include "fj_dyntext.h"
#include "fj_font.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetIpElements;
using namespace FoxjetCommon;
using namespace FoxjetCommon::ElementFields;

IMPLEMENT_DYNAMIC (CDynamicTextElement, CIpBaseElement);

const FoxjetCommon::LIMITSTRUCT CDynamicTextElement::m_lmtID	= { 1,	FJMESSAGE_DYNAMIC_SEGMENTS };
const FoxjetCommon::LIMITSTRUCT CDynamicTextElement::m_lmtLen	= { 0,	FJTEXT_SIZE };

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDynamicTextElement::CDynamicTextElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head)
:	CIpBaseElement (fj_ElementDynTextNew (), pHead, head)
{
}

CDynamicTextElement::CDynamicTextElement (const CDynamicTextElement & rhs)
: 	CIpBaseElement (rhs)
{
	SetDynamicID (rhs.GetDynamicID ());
}

CDynamicTextElement & CDynamicTextElement::operator = (const CDynamicTextElement & rhs)
{
	CIpBaseElement::operator = (rhs);

	if (this != &rhs) 
		SetDynamicID (rhs.GetDynamicID ());

	return * this;
}

CDynamicTextElement::~CDynamicTextElement ()
{
}

int CDynamicTextElement::Build (BUILDTYPE type, bool bCanThrowException)
{
	return GetClassID ();
}

int CDynamicTextElement::GetDynamicID () const
{
	return GetMember ()->lDynamicID + 1;
}

bool CDynamicTextElement::SetDynamicID (LONG lID)
{
	if (FoxjetCommon::IsValid (lID, m_lmtID)) {
		GetMember ()->lDynamicID = lID - 1;
		return true;
	}

	return false;
}

bool CDynamicTextElement::SetDefaultData(const CString &strData)
{
	Invalidate ();
	return false;
}

bool CDynamicTextElement::SetElement (LPFJELEMENT pNew)
{
	if (pNew) {
		ASSERT (pNew);
		ASSERT (pNew->pActions->fj_DescGetType () == FJ_TYPE_DYNTEXT);
		ASSERT (pNew->pDesc);
	}

	return CIpBaseElement::SetElement (pNew);
}

void CDynamicTextElement::CreateImage () 
{
	using FoxjetIpElements::CTableItem;

	LPFJDYNTEXT pText = GetMember ();
	CArray <CTableItem, CTableItem &> vItems;

	CDynamicTableDlg::GetDynamicData (GetLineID (), vItems);

	for (int i = 0; i < vItems.GetSize (); i++) {
		CTableItem item = vItems [i];

		if (item.m_lID == GetDynamicID ()) {
			TRACEF (item.GetSqlResult ());
			break;
		}
	}

	pText->pff = m_pFont;
	strcpy (pText->strFontName, m_pFont->strName);    

	CIpBaseElement::CreateImage (); 
}

LONG CDynamicTextElement::GetLength () const
{
	return GetMember ()->lLength;
}

bool CDynamicTextElement::SetLength (LONG l)
{
	if (FoxjetCommon::IsValid (l, m_lmtLen)) {
		GetMember ()->lLength = l;
		return true;
	}

	return false;
}

LONG CDynamicTextElement::GetFirstChar () const
{
	return GetMember ()->lFirstChar;
}

bool CDynamicTextElement::SetFirstChar (LONG l)
{
	if (FoxjetCommon::IsValid (l, m_lmtLen)) {
		GetMember ()->lFirstChar = l;
		return true;
	}

	return false;
}

LONG CDynamicTextElement::GetNumChar () const
{
	return GetMember ()->lNumChar;
}

bool CDynamicTextElement::SetNumChar (LONG l)
{
	if (FoxjetCommon::IsValid (l, m_lmtLen)) {
		GetMember ()->lNumChar = l;
		return true;
	}

	return false;
}

CHAR CDynamicTextElement::GetAlign () const
{
	return GetMember ()->cAlign;
}

bool CDynamicTextElement::SetAlign (CHAR c)
{
	GetMember ()->cAlign = c;
	return true;
}

CHAR CDynamicTextElement::GetFill () const
{
	return GetMember ()->cFill;
}

bool CDynamicTextElement::SetFill (CHAR c)
{
	GetMember ()->cFill = c;
	return true;
}

static const ElementFields::FIELDSTRUCT lpszFields [] =
{
	ElementFields::FIELDSTRUCT (m_lpszElementType, 	_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszID,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszX,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszY,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszPosition,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszBold,			_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszIPWidth,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszIPGap,		_T ("0")),
	ElementFields::FIELDSTRUCT (m_lpszFlipV,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFlipH,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszInverse,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszFontName,		_T ("0")),	
	ElementFields::FIELDSTRUCT (m_lpszData,			_T ("0")),	
	ElementFields::FIELDSTRUCT (),
};

const FoxjetCommon::ElementFields::FIELDSTRUCT * CDynamicTextElement::GetFieldBuffer () const
{
	return ::lpszFields;
}

LONG CDynamicTextElement::GetBold () const
{
	return GetMember ()->lBoldValue;
}

bool CDynamicTextElement::SetBold (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, CIpBaseElement::m_lmtBold)) {
		GetMember ()->lBoldValue = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

int CDynamicTextElement::GetWidth () const
{
	return GetMember ()->lWidthValue;
}

bool CDynamicTextElement::SetWidth (int nValue)
{
	if (FoxjetCommon::IsValid (nValue, CIpBaseElement::m_lmtWidth)) {
		GetMember ()->lWidthValue = nValue;
		Invalidate ();
		return true;
	}

	return false;
}

LONG CDynamicTextElement::GetGap () const
{
	return GetMember ()->lGapValue;
}

bool CDynamicTextElement::SetGap (LONG lValue)
{
	if (FoxjetCommon::IsValid (lValue, CIpBaseElement::m_lmtGap)) {
		GetMember ()->lGapValue = lValue;
		Invalidate ();
		return true;
	}

	return false;
}

bool CDynamicTextElement::SetFont (const FoxjetCommon::CPrinterFont & f)
{
	strncpy (GetMember ()->strFontName, w2a (f.m_strName), FJFONT_NAME_SIZE);
	return CIpBaseElement::SetFont (f);
}

CString CDynamicTextElement::GetElementFontName () const
{
	return GetMember ()->strFontName;
}

CString CDynamicTextElement::GetImageData () const
{
	return CString ('W', GetNumChar ());
}

CString CDynamicTextElement::GetDefaultData() const
{
	return GetImageData ();
}

