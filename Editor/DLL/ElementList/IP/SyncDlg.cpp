// SyncDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "list.h"
#include "SyncDlg.h"
#include "TemplExt.h"
#include "Extern.h"
#include "FieldDefs.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetIpElements;
using namespace FoxjetDatabase;
using namespace ListGlobals;

/////////////////////////////////////////////////////////////////////////////
// CSyncDlg dialog


CSyncDlg::CSyncDlg(DWORD dwDefaults, DWORD dwSync, CWnd* pParent /*=NULL*/)
:	m_dwDefaults (dwDefaults),
	m_dwSync (dwSync),
	FoxjetCommon::CEliteDlg (IDD_SYNCHRONIZE, pParent)
{
	//{{AFX_DATA_INIT(CSyncDlg)
	//}}AFX_DATA_INIT
	m_bFonts	= (m_dwDefaults & SYNC_FONTS)			? true : false;
	m_bMessages	= (m_dwDefaults & SYNC_MESSAGES)		? true : false;

	m_bAllHeads	= false;
	m_bDelete	= false;
}


void CSyncDlg::DoDataExchange(CDataExchange* pDX)
{
	FoxjetCommon::CEliteDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSyncDlg)
	DDX_Check(pDX, CHK_FONTS, m_bFonts);
	DDX_Check(pDX, CHK_MESSAGES, m_bMessages);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, CHK_ALL, m_bAllHeads);
	DDX_Check(pDX, CHK_DELETEALL, m_bDelete);
}


BEGIN_MESSAGE_MAP(CSyncDlg, FoxjetCommon::CEliteDlg)
	//{{AFX_MSG_MAP(CSyncDlg)
	ON_BN_CLICKED(CHK_DELETEALL, OnDeleteall)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSyncDlg message handlers

BOOL CSyncDlg::OnInitDialog() 
{
	FoxjetCommon::CEliteDlg::OnInitDialog();

	if (m_dwSync & SYNC_FONTS)
		if (CWnd * p = GetDlgItem (CHK_FONTS))
			p->SetWindowText (LoadString (IDS_SYNCFONTS));

	if (CButton * p = (CButton *)GetDlgItem (CHK_MESSAGES)) {
		SETTINGSSTRUCT s;

		if (m_dwSync & SYNC_MESSAGES)
			p->SetWindowText (LoadString (IDS_SYNCMESSAGES));

		GetSettingsRecord (GetDB (), FoxjetDatabase::Globals::m_lGlobalID, FoxjetDatabase::Globals::m_lpszLean, s);
		bool bSingleMsgMode = _ttoi (s.m_strData) ? true : false;
		
		if (bSingleMsgMode) {
			p->SetCheck (0);
			p->EnableWindow (FALSE);
		}
	}

	if (CWnd * p = GetDlgItem (CHK_DELETEALL))
		p->ShowWindow ((m_dwDefaults & SYNC_SHOW_DELETE) ? SW_SHOW : SW_HIDE);

	if (CWnd * p = GetDlgItem (CHK_ALL))
		p->ShowWindow ((m_dwDefaults & SYNC_SHOW_ALLHEADS) ? SW_SHOW : SW_HIDE);
	
	if (m_strPrompt.GetLength ())
		SetDlgItemText (LBL_MSG, m_strPrompt);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSyncDlg::OnDeleteall() 
{
	UINT nID [] = 
	{
		CHK_MESSAGES,
		CHK_FONTS,
	};
	CButton * pDelete = (CButton *)GetDlgItem (CHK_DELETEALL);

	ASSERT (pDelete);
	
	bool bDelete = pDelete->GetCheck () == 1;

	m_bFonts	= bDelete ? true : ((m_dwDefaults & SYNC_FONTS)			? true : false);
	m_bMessages	= bDelete ? true : ((m_dwDefaults & SYNC_MESSAGES)		? true : false);
	m_bDelete	= bDelete;

	for (int i = 0; i < ARRAYSIZE (nID); i++)
		if (CWnd * p = GetDlgItem (nID [i]))
			p->EnableWindow (!bDelete);

	UpdateData (FALSE);
}
