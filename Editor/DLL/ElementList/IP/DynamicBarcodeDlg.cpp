// DynamicBarcodeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "list.h"
#include "DynamicBarcodeDlg.h"
#include "DynamicBarcodeElement.h"
#include "DynamicTextElement.h"
#include "BarcodeElement.h"
#include "fj_export.h"
#include "fj_system.h"
#include "BarcodeParamDlg.h"
#include "Coord.h"
#include "DynamicTableDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetIpElements;

/////////////////////////////////////////////////////////////////////////////
// CDynamicBarcodeDlg dialog


CDynamicBarcodeDlg::CDynamicBarcodeDlg(const FoxjetCommon::CBaseElement & e, const CSize & pa,
						 const FoxjetCommon::CElementList * pList, CWnd* pParent)
:	m_bOutputText (FALSE),
	m_dBold (1),
	m_nTextBold (1),
	m_nWidth (1),
	m_nType (0),
	m_cAlign ('C'),
	m_lDynamicID (1),
	m_lFirstChar (0),
	m_lNumChar (1),
	CElementDlg(e, pa, CDynamicBarcodeDlg::IDD, pList, pParent)
{
	//{{AFX_DATA_INIT(CDynamicBarcodeDlg)
	//}}AFX_DATA_INIT

	const CDynamicBarcodeElement & barcode = GetElement ();
	const LPFJDYNBARCODE p = barcode.GetMember ();
	ASSERT (p);

	m_bOutputText	= p->bHROutput;
	m_nTextBold		= p->lHRBold;
	m_cAlign		= p->cHRAlign;
	m_nWidth		= p->lWidthValue;
	m_lDynamicID	= p->lDynamicID;
}


void CDynamicBarcodeDlg::DoDataExchange(CDataExchange* pDX)
{
	CElementDlg::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CDynamicBarcodeDlg)
	DDX_Check(pDX, CHK_ASCII, m_bOutputText);
	//}}AFX_DATA_MAP

	DDX_Text(pDX, TXT_BOLD, m_dBold);
	DDV_MinMaxInt (pDX, m_dBold, CDynamicBarcodeElement::m_lmtBold.m_dwMin, CDynamicBarcodeElement::m_lmtBold.m_dwMax); 
	
	DDX_Text(pDX, TXT_TEXTBOLD, m_nTextBold);
	DDV_MinMaxInt (pDX, m_nTextBold, CDynamicBarcodeElement::m_lmtBold.m_dwMin, CDynamicBarcodeElement::m_lmtBold.m_dwMax); 
	
	DDX_Text (pDX, TXT_WIDTH, m_nWidth);
	DDV_MinMaxInt (pDX, m_nWidth, CDynamicBarcodeElement::m_lmtWidth.m_dwMin, CDynamicBarcodeElement::m_lmtWidth.m_dwMax); 

	DDX_Text (pDX, TXT_FIRSTCHAR, m_lFirstChar);
	DDV_MinMaxInt (pDX, m_lFirstChar, CDynamicTextElement::m_lmtLen.m_dwMin, CDynamicTextElement::m_lmtLen.m_dwMax); 

	DDX_Text (pDX, TXT_NUMBEROFCHARS, m_lNumChar);
	DDV_MinMaxInt (pDX, m_lNumChar, CDynamicTextElement::m_lmtLen.m_dwMin, CDynamicTextElement::m_lmtLen.m_dwMax); 
}


BEGIN_MESSAGE_MAP(CDynamicBarcodeDlg, CElementDlg)
	//{{AFX_MSG_MAP(CDynamicBarcodeDlg)
	ON_BN_CLICKED(CHK_ASCII, OnAscii)
	ON_CBN_SELCHANGE(CB_TYPE, OnSelchangeType)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(CHK_EPS, InvalidatePreview)
	ON_BN_CLICKED(CHK_PS, InvalidatePreview)
	ON_BN_CLICKED(CHK_NOHEADERS, InvalidatePreview)
	ON_BN_CLICKED(CHK_CHECKSUM, InvalidatePreview)
	ON_BN_CLICKED(CHK_CORRECT, InvalidatePreview)
    ON_CBN_SELCHANGE(CB_ALIGNMENT, InvalidatePreview)
    ON_CBN_SELCHANGE(CB_DYNAMICID, InvalidatePreview)
	ON_EN_CHANGE(TXT_TEXTBOLD, InvalidatePreview)
	ON_EN_CHANGE(TXT_BOLD, InvalidatePreview)
	ON_EN_CHANGE(TXT_WIDTH, InvalidatePreview)
	ON_EN_CHANGE(TXT_TEXTBOLD, InvalidatePreview)
	ON_BN_CLICKED(BTN_SELECT, OnSelect)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDynamicBarcodeDlg message handlers
CDynamicBarcodeElement & CDynamicBarcodeDlg::GetElement ()
{
	CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CDynamicBarcodeElement)));
	return (CDynamicBarcodeElement &)e;
}

const CDynamicBarcodeElement & CDynamicBarcodeDlg::GetElement () const
{
	const CBaseElement & e = CElementDlg::GetElement ();
	ASSERT (e.IsKindOf (RUNTIME_CLASS (CDynamicBarcodeElement)));
	return (CDynamicBarcodeElement &)e;
}

int CDynamicBarcodeDlg::BuildElement ()
{
	CDynamicBarcodeElement & e = GetElement ();
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	CComboBox * pAlign = (CComboBox *)GetDlgItem (CB_ALIGNMENT);
	int nType = BARCODE_ANY;
	CHAR cAlign = 'C';
	int nIndex;
	int nResult = -1;

	ASSERT (pType);
	ASSERT (pAlign);

	if ((nIndex = pType->GetCurSel ()) != CB_ERR) 
		nType = (int)pType->GetItemData (nIndex);

	if ((nIndex = pAlign->GetCurSel ()) != CB_ERR) 
		cAlign = (CHAR)pAlign->GetItemData (nIndex);

	e.SetFlippedH (GetCheck (CHK_FLIPH));
	e.SetFlippedV (GetCheck (CHK_FLIPV));
	e.SetInverse (GetCheck (CHK_INVERSE));

	e.SetOutputText (GetCheck (CHK_ASCII));
	e.SetBold (_ttol (GetCtrlText (TXT_BOLD)));
	e.SetDynamicID (GetCurSelDynamicID ());
	e.SetTextBold (_ttol (GetCtrlText (TXT_TEXTBOLD)));
	e.SetWidth (_ttol (GetCtrlText (TXT_WIDTH)));
	e.SetFirstChar (GetDlgItemInt (TXT_FIRSTCHAR));
	e.SetNumChar (GetDlgItemInt (TXT_NUMBEROFCHARS));

	e.SetBcType (nType);
	e.SetTextAlign (cAlign);

	nResult = e.Build ();
	e.Draw (* GetDC (), GetHead (), true);
	return nResult;
}

void CDynamicBarcodeDlg::OnAscii() 
{
	bool bText = GetCheck (CHK_ASCII);

	ASSERT (GetDlgItem (CB_ALIGNMENT));
	ASSERT (GetDlgItem (TXT_TEXTBOLD));

	GetDlgItem (CB_ALIGNMENT)->EnableWindow (bText);
	GetDlgItem (TXT_TEXTBOLD)->EnableWindow (bText);

	InvalidatePreview ();
}

BOOL CDynamicBarcodeDlg::OnInitDialog() 
{
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	CComboBox * pAlign = (CComboBox *)GetDlgItem (CB_ALIGNMENT);
	CComboBox * pID = (CComboBox *)GetDlgItem (CB_DYNAMICID);
	struct 
	{
		CHAR m_c;
		UINT m_nID;
	} static const alignmap [] = 
	{
		{ 'L',	IDS_LEFT	},
		{ 'R',	IDS_RIGHT	},
		{ 'C',	IDS_CENTER	},
		{ 0,	-1			},
	};

	ASSERT (pType);
	ASSERT (pAlign);
	ASSERT (pID);

	BOOL bResult = CElementDlg::OnInitDialog();
	
	for (int i = 0; i < FJSYS_BARCODES; i++) {
		const FJSYSBARCODE & bc = GetElement ().GetSystem ()->bcArray [i];
		int nIndex = pType->AddString (a2w (bc.strName));
		pType->SetItemData (nIndex, i);

		if (m_nType == i)
			pType->SetCurSel (i);
	}

	for (int i = 0; alignmap [i].m_nID != -1; i++) {
		int nIndex = pAlign->AddString (LoadString (alignmap [i].m_nID));
		pAlign->SetItemData (nIndex, alignmap [i].m_c);

		if (alignmap [i].m_c == m_cAlign)
			pAlign->SetCurSel (nIndex);
	}

	for (int i = (int)CDynamicTextElement::m_lmtID.m_dwMin; i <= (int)CDynamicTextElement::m_lmtID.m_dwMax; i++) {
		CString str;

		str.Format (_T ("%d"), i);
		int nIndex = pID->AddString (str);
		pID->SetItemData (nIndex, i);

		if (i == m_lDynamicID)
			pID->SetCurSel (nIndex);
	}

	if (pType->GetCurSel () == CB_ERR)
		pType->SetCurSel (0);

	if (pAlign->GetCurSel () == CB_ERR)
		pAlign->SetCurSel (0);

	if (pID->GetCurSel () == CB_ERR)
		pID->SetCurSel (0);

	OnAscii ();
	OnSelchangeType ();

	return bResult;
}

LONG CDynamicBarcodeDlg::GetCurSelDynamicID () const
{
	CComboBox * pCB = (CComboBox *)GetDlgItem (CB_DYNAMICID);

	ASSERT (pCB);

	return pCB->GetItemData (pCB->GetCurSel ());
}

void CDynamicBarcodeDlg::OnOK()
{
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	CComboBox * pAlign = (CComboBox *)GetDlgItem (CB_ALIGNMENT);
	int nIndex;

	m_lDynamicID = GetCurSelDynamicID ();

	ASSERT (pType);
	ASSERT (pAlign);
	ASSERT (m_lDynamicID != -1);

	if ((nIndex = pType->GetCurSel ()) == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOTYPESELECTED));
		return;
	}
	else
		m_nType = (int)pType->GetItemData (nIndex);

	if ((nIndex = pAlign->GetCurSel ()) == CB_ERR) {
		MsgBox (* this, LoadString (IDS_NOALIGNMENTSELECTED));
		return;
	}
	else
		m_cAlign = (CHAR)pAlign->GetItemData (nIndex);

	if (!GetElement ().Validate ())
		MsgBox (* this, LoadString (IDS_INVALIDDATA));
	else
		CElementDlg::OnOK ();
}

void CDynamicBarcodeDlg::OnSelchangeType() 
{
	CComboBox * pType = (CComboBox *)GetDlgItem (CB_TYPE);
	LPFJSYSTEM pSystem = GetElement ().GetSystem ();

	ASSERT (pType);
	
	int nIndex = (int)pType->GetItemData (nIndex = pType->GetCurSel ());
	CString str = CBarcodeElement::GetBcType (pSystem, nIndex);

	SetDlgItemText (TXT_SYMBOLOGY, str);
	InvalidatePreview ();
}

void CDynamicBarcodeDlg::OnSelect() 
{
	CDynamicTableDlg dlg;

	dlg.m_lLineID = GetLineID ();
	dlg.m_lSelectedID = GetCurSelDynamicID ();

	if (dlg.DoModal () == IDOK) {
		CComboBox * pCB = (CComboBox *)GetDlgItem (CB_DYNAMICID);

		ASSERT (pCB);

		for (int i = 0; i < pCB->GetCount (); i++) {
			if (dlg.m_lSelectedID == (LONG)pCB->GetItemData (i)) {
				pCB->SetCurSel (i);
				break;
			}
		}
	}
}
