// UDPasocket.cpp : implementation file
//

#include "stdafx.h"
#include "UDPasocket.h"
#include "FJSocketHeader.h"
#include "FJ_SOCKET.H"
#include "Debug.h"
#include "Parse.h"

#define UDP_BUF_SIZE 1024

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetIpElements;

/////////////////////////////////////////////////////////////////////////////
// CUDPasocket

CUDPasocket::CUDPasocket (LPFINDHEADSFCT pfct, CWnd * pParent)
:	m_pReceiveBuffer (NULL),
	m_pfct (pfct),
	m_pParent (pParent)
{
	m_pReceiveBuffer = new UCHAR[UDP_BUF_SIZE];

	memset(m_pReceiveBuffer,0,UDP_BUF_SIZE);
}

CUDPasocket::~CUDPasocket()
{
	if (NULL != m_pReceiveBuffer) 
		delete [] m_pReceiveBuffer;
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CUDPasocket, CAsyncSocket)
	//{{AFX_MSG_MAP(CUDPasocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CUDPasocket member functions

void CUDPasocket::OnReceive(int nErrorCode) 
{
	int iRead;
	CString socketAddr;
	UINT	socketPort;
	CFJSocketHeader fjHeader;
	UCHAR* pTempBuf = NULL;

	iRead = ReceiveFrom(m_pReceiveBuffer,UDP_BUF_SIZE,socketAddr,socketPort);
	
	{
		CString str = FoxjetDatabase::ToString (iRead) + _T (": ");

		for (int i = 0; i < iRead; i++)
			str += (m_pReceiveBuffer [i] >= 32 && m_pReceiveBuffer [i] <= 127) ? (TCHAR)m_pReceiveBuffer [i] : (TCHAR)'.';

		TRACEF (str);
	}

	switch (iRead)
	{
	case 0:
		Close();
		break;
	case SOCKET_ERROR:
		if (GetLastError() != WSAEWOULDBLOCK) 
		{
			if (GetLastError() != WSAEMSGSIZE)
			{
				CString TmpStr;
				TmpStr.Format(_T ("UDP OnReceive error: %d"), GetLastError());
				TRACEF (TmpStr);
			}
			else
			{
				TRACEF (_T ("The datagram was too large and was truncated"));
				ProcessData(UDP_BUF_SIZE);
				//CString szTemp(m_pReceiveBuffer);
			}
		}
		break;

	default:
		if (iRead != SOCKET_ERROR && iRead != 0)
		{
			fjHeader.SetBuffer(m_pReceiveBuffer);
			if (fjHeader.GetSocketCMD() == IPC_SET_GROUP_INFO)
				ProcessData(iRead);
		}

	}
	CAsyncSocket::OnReceive(nErrorCode);
}

void CUDPasocket::ProcessData(int iRead)
{//GROUP_NAME="abc";GROUP_COUNT=n;GROUP_IDS="aaa","bbb","ccc","ddd";GROUP_IPADDRS=n.n.n.n,n.n.n.n,n.n.n.n;
	CFJSocketHeader fjHeader;
	CString str (m_pReceiveBuffer + fjHeader.GetBufferSize());

	if (m_pfct) 
		if (!(* m_pfct) (str, m_pParent))
			Close ();
}

void CUDPasocket::OnSend(int nErrorCode) 
{
	SendInfo();
	CAsyncSocket::OnSend(nErrorCode);
}

void CUDPasocket::OnClose(int nErrorCode) 
{
//	((CHeadConfigDlg *)m_pDlg)->m_pUDPSocket = NULL;
	
	CAsyncSocket::OnClose(nErrorCode);
}

void CUDPasocket::SendInfo()
{
	CFJSocketHeader fjHeader;
	fjHeader.SetBachID(1);
	fjHeader.SetDataLength(0);
	fjHeader.SetSocketCMD(IPC_GET_GROUP_INFO);
	int dwBytes;

	SOCKADDR_IN remoteAddr;
	remoteAddr.sin_family = AF_INET;
	remoteAddr.sin_port = htons(FJ_UDP_PORT);
	remoteAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
	dwBytes = SendTo(fjHeader.GetBuffer(),fjHeader.GetBufferSize(), (const SOCKADDR* )&remoteAddr, sizeof(remoteAddr));
	if (dwBytes == SOCKET_ERROR)
	{
		if (GetLastError() != WSAEWOULDBLOCK)
		{
			TCHAR szError[256];
			wsprintf(szError, _T ("Server Socket failed to send: %d"), GetLastError());
			Close();
			TRACEF (szError);
		}

	}

}
