// UserElement.h: interface for the CUserElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_USERELEMENT_H__6CB62662_8AD9_11D4_8FC6_006067662794__INCLUDED_)
#define AFX_USERELEMENT_H__6CB62662_8AD9_11D4_8FC6_006067662794__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TextElement.h"
#include "Types.h"

namespace FoxjetIpElements
{
	class ELEMENT_API CUserElement : public CTextElement  
	{
		DECLARE_DYNAMIC (CUserElement);

	public:
		CUserElement (LPFJPRINTHEAD pHead, const FoxjetDatabase::HEADSTRUCT & head);
		CUserElement (const CUserElement & rhs);
		CUserElement & operator = (const CUserElement & rhs);
		virtual ~CUserElement();

		virtual CString ToString (const FoxjetCommon::CVersion & ver) const;
		virtual bool FromString (const CString & str, const FoxjetCommon::CVersion & ver);
		virtual int GetClassID () const { return USER; }

		virtual int Build (FoxjetCommon::BUILDTYPE type = FoxjetCommon::INITIAL, bool bCanThrowException = false);
		// throws CElementException

		virtual bool SetDefaultData (const CString &strData);

		CString ToString (const FoxjetCommon::CVersion & ver, int nType);

		int GetMaxChars () const;
		bool SetMaxChars (int nMax);

		CString GetPrompt () const;
		bool SetPrompt (const CString & str);

		bool	m_bPromptAtTaskStart;

		static const FoxjetCommon::LIMITSTRUCT m_lmtString;

	protected:
		CString m_strPrompt;
		int		m_nMaxChars;
	};
}; //namespace FoxjetIpElements

#endif // !defined(AFX_USERELEMENT_H__6CB62662_8AD9_11D4_8FC6_006067662794__INCLUDED_)
