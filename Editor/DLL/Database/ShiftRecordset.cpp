// ShiftRecordset.cpp : implementation file
//

#include "stdafx.h"
#include "OdbcRecordset.h"
#include "Database.h"
#include "Debug.h"
#include "FieldDefs.h"
#include "Compare.h"
#include "Serialize.h"
#include "Parse.h"

extern HINSTANCE hInstance;

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Shifts;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)

FoxjetDatabase::tagSHIFTSTRUCT::tagSHIFTSTRUCT ()
{
}

FoxjetDatabase::tagSHIFTSTRUCT::tagSHIFTSTRUCT (const tagSHIFTSTRUCT & rhs)
:	m_lID		(rhs.m_lID),
	m_lLineID	(rhs.m_lLineID),
	m_strName	(rhs.m_strName),
	m_dtTime	(rhs.m_dtTime),
	m_strCode	(rhs.m_strCode)
{
}

tagSHIFTSTRUCT & FoxjetDatabase::tagSHIFTSTRUCT::operator = (const tagSHIFTSTRUCT & rhs)
{
	if (this != &rhs) {
		m_lID		= rhs.m_lID;
		m_lLineID	= rhs.m_lLineID;
		m_strName	= rhs.m_strName;
		m_dtTime	= rhs.m_dtTime;
		m_strCode	= rhs.m_strCode;
	}

	return * this;
}

FoxjetDatabase::tagSHIFTSTRUCT::~tagSHIFTSTRUCT ()
{
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::SHIFTSTRUCT & lhs, const FoxjetDatabase::SHIFTSTRUCT & rhs)
{
	return (
		lhs.m_lID			== rhs.m_lID &&
		lhs.m_lLineID		== rhs.m_lLineID &&
		lhs.m_dtTime		== rhs.m_dtTime &&
		lhs.m_strName		== rhs.m_strName &&
		lhs.m_strCode		== rhs.m_strCode);
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::SHIFTSTRUCT & lhs, const FoxjetDatabase::SHIFTSTRUCT & rhs)
{
	return !(rhs == lhs);
}

DB_API const FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator >> (const FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::SHIFTSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		s.m_lID			= (long)rst.GetFieldValue			(m_lpszID);
		s.m_lLineID		= (long)rst.GetFieldValue			(m_lpszLineID);
		s.m_dtTime		= (COleDateTime)rst.GetFieldValue	(m_lpszTime).ToOleDateTime ();
		s.m_strName		= (CString)rst.GetFieldValue		(m_lpszName);
		s.m_strCode		= (CString)rst.GetFieldValue		(m_lpszCode);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator << (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::SHIFTSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());
	
	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	CStringArray name, value;

	if (s.m_lID == -1) 
		rst.SetEditMode (COdbcRecordset::addnew);

	name.Add (m_lpszLineID);		value.Add (ToString ((long)s.m_lLineID));
	name.Add (m_lpszTime);			value.Add (s.m_dtTime.Format (MDB_DATE_STRFTIME));
	name.Add (m_lpszName);			value.Add (s.m_strName);
	name.Add (m_lpszCode);			value.Add (s.m_strCode);

	CString strSQL = FormatSQL (rst, m_lpszTable, name, value, m_lpszID, s.m_lID);
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

	if (s.m_lID == -1)
		s.m_lID = GetMaxID (rst.GetOdbcDatabase (), m_lpszTable, m_lpszID);
/*
	try {
		if (rst.IsAddNew ())
			rst.SetFieldValue (m_lpszID,	(long)GetNextID (rst.GetOdbcDatabase (), m_lpszTable));

		rst.SetFieldValue (m_lpszLineID,	(long) s.m_lLineID);
		rst.SetFieldValue (m_lpszTime,		s.m_dtTime);
		rst.SetFieldValue (m_lpszName,		s.m_strName);
		rst.SetFieldValue (m_lpszCode,		s.m_strCode);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/

	return rst;
}

DB_API bool FoxjetDatabase::GetShiftRecord (COdbcDatabase & database, ULONG lID, SHIFTSTRUCT &s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u"), 
			m_lpszTable, m_lpszID, lID);
		rst.Open (str);

		if (!rst.IsEOF ()) {
			rst >> s;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}


DB_API bool FoxjetDatabase::AddShiftRecord (COdbcDatabase & database, SHIFTSTRUCT &s)
{
	DBMANAGETHREADSTATE (database);
	bool bResult = false;

	try {
		COdbcRecordset rst (&database);

		s.m_lID = -1;
		rst << s;

		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);

		rst.Open (str, CRecordset::dynaset);
		rst.AddNew ();
		rst << s;
		rst.Update ();
		rst.MoveLast ();
		s.m_lID = (long)rst.GetFieldValue (m_lpszID);
		rst.Close ();
		*/

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::UpdateShiftRecord (COdbcDatabase & database, SHIFTSTRUCT &s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);

		rst << s;
		bResult = true;

		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, s.m_lID);
		rst.Open (str, CRecordset::dynaset);

		if (!rst.IsEOF ()) {
			rst.Edit ();
			rst << s;
			rst.Update ();
			bResult = true;
		}
		rst.Close ();
		*/
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::DeleteShiftRecord (COdbcDatabase & database, ULONG lID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		CString strSQL;
		
		strSQL.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%u;"),
			m_lpszTable, m_lpszID, lID);
		database.ExecuteSQL (strSQL);
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}


DB_API bool FoxjetDatabase::GetShiftRecords (COdbcDatabase & database, ULONG lLineID, CArray <SHIFTSTRUCT, SHIFTSTRUCT> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	v.RemoveAll ();

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszLineID, lLineID);
		rst.Open (str, CRecordset::dynaset);

		while (!rst.IsEOF ()) {
			SHIFTSTRUCT s;
			rst >> s;
			v.Add (s);
			rst.MoveNext ();
		}
		rst.Close ();
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

