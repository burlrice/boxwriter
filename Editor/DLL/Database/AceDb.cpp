#include "stdafx.h"
#include "Debug.h"
#include "Database.h"
#include <afx.h>

#import "C:\Program Files (x86)\Common Files\System\ado\msado28.tlb" no_namespace \
	rename( "EOF", "adoEOF" ) \
	rename( "LockTypeEnum", "adoLockTypeEnum" ) \
	rename( "DataTypeEnum", "adoDataTypeEnum") \
	rename( "FieldAttributeEnum", "FieldAttributeEnumado") \
	rename( "EditModeEnum", "adoEditModeEnum") \
	rename( "RecordStatusEnum", "adoRecordStatusEnum") \
	rename( "ParameterDirectionEnum", "adoParameterDirectionEnum") 

#import "C:\Program Files (x86)\Common Files\System\ado\msjro.dll" no_namespace \
	rename( "ReplicaTypeEnum", "adoReplicaTypeEnum" ) \

//#import "C:\Program Files (x86)\Common Files\Microsoft Shared\DAO\dao360.dll" rename( "EOF", "daoEOF" )

#import <C:\\Program Files (x86)\\Common Files\\Microsoft Shared\\OFFICE14\\ACEDAO.dll>  \
	rename( "EOF", "AdoNSEOF" ) \
	auto_rename

bool CompactDatabase (const CString & strSrc, const CString & strDest)
{
	bool bResult = false;

	::CoInitialize (0);

	if (FoxjetDatabase::IsAccdb (strSrc)) {
		DAO::_DBEngine* pEngine = NULL;

		HRESULT hr = CoCreateInstance (
			__uuidof(DAO::DBEngine),
			NULL,
			CLSCTX_ALL,
			IID_IDispatch,
			(LPVOID*)&pEngine);

		if (SUCCEEDED(hr) && pEngine)
		{
			try 
			{
				pEngine->CompactDatabase (_bstr_t (strSrc), _bstr_t (strDest));
				bResult = true;
			}
			catch(_com_error &e) { throw (e);  }

			pEngine->Release ();
		}
	}
	else {
		CString strSourceConnection = _T ("Data Source=") + strSrc		+ _T (";");
		CString strDestConnection	= _T ("Data Source=") + strDest		+ _T (";");
	
		TRACEF (strSourceConnection);
		TRACEF (strDestConnection);

		try
		{
			IJetEnginePtr jet(__uuidof(JetEngine));
			jet->CompactDatabase (_bstr_t (strSourceConnection), _bstr_t (strDestConnection));
			bResult = true;
		}
		catch(_com_error &e) { throw (e);  }
	}

	::CoUninitialize ();

	return bResult;
}