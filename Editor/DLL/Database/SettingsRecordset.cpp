// HeadRecordset.cpp : implementation file
//

#include "stdafx.h"
#include "Database.h"
#include "Debug.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Serialize.h"
#include "compare.h"
#include "Parse.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Settings;

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)


FoxjetDatabase::tagSETTINGSSTRUCT::tagSETTINGSSTRUCT ()
{
}

FoxjetDatabase::tagSETTINGSSTRUCT::tagSETTINGSSTRUCT (const tagSETTINGSSTRUCT & rhs)
:	m_lLineID	(rhs.m_lLineID),
	m_strKey	(rhs.m_strKey),
	m_strData	(rhs.m_strData)
{
}

tagSETTINGSSTRUCT & FoxjetDatabase::tagSETTINGSSTRUCT::operator = (const tagSETTINGSSTRUCT & rhs)
{
	if (this != &rhs) {
		m_lLineID	= rhs.m_lLineID;
		m_strKey	= rhs.m_strKey;
		m_strData	= rhs.m_strData;
	}

	return * this;
}

FoxjetDatabase::tagSETTINGSSTRUCT::~tagSETTINGSSTRUCT ()
{
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::SETTINGSSTRUCT & lhs, const FoxjetDatabase::SETTINGSSTRUCT & rhs)
{
	return
		lhs.m_lLineID	== rhs.m_lLineID	 &&
		lhs.m_strKey	== rhs.m_strKey		 &&
		lhs.m_strData	== rhs.m_strData;
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::SETTINGSSTRUCT & lhs, const FoxjetDatabase::SETTINGSSTRUCT & rhs)
{
	return !(rhs == lhs);
}

DB_API const COdbcRecordset & FoxjetDatabase::operator >> (const COdbcRecordset & rst, SETTINGSSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		s.m_lLineID	= (long)rst.GetFieldValue (m_lpszLineID);
		s.m_strKey	= (CString)rst.GetFieldValue (m_lpszKey);
		s.m_strData	= (CString)rst.GetFieldValue (m_lpszData);
//TRACEF (ToString (s.m_lLineID) + _T (": ") + s.m_strKey);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API COdbcRecordset & FoxjetDatabase::operator << (COdbcRecordset & rst, SETTINGSSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	if (s.m_lLineID > 7) {
		TRACEF (s.m_strKey);
		int nBreak = 0;
	}

	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	CStringArray name, value;
	CString strWhere, strKeyRevised (s.m_strKey);

	strKeyRevised.Replace (_T ("'"), _T (""));
	strWhere.Format (_T (" WHERE [%s]=%u AND [%s]='%s';"), 
		m_lpszLineID, s.m_lLineID,
		m_lpszKey, strKeyRevised);

	if (!s.m_strData.GetLength ())
		s.m_strData += _T (" ");
			
	name.Add (m_lpszLineID);		value.Add (ToString ((long)s.m_lLineID));
	name.Add (m_lpszKey);			value.Add (s.m_strKey);
	name.Add (m_lpszData);			value.Add (s.m_strData);

	CString strSQL = FormatSQL (rst, m_lpszTable, name, value, _T (""), 0) + (!rst.IsAddNew () ? strWhere : _T (""));
	TRACEF (strSQL);
	
	if (!rst.GetOdbcDatabase ().ExecuteSQL (strSQL)) {
		rst.SetEditMode (COdbcRecordset::addnew);
		strSQL = FormatSQL (rst, m_lpszTable, name, value, _T (""), 0);
		TRACEF (strSQL);
		int nResult = rst.GetOdbcDatabase ().ExecuteSQL (strSQL);
		ASSERT (nResult > 0);
	}

	/*
	try {
		rst.SetFieldValue (m_lpszLineID,	(long)s.m_lLineID);
		rst.SetFieldValue (m_lpszKey,		s.m_strKey);
		rst.SetFieldValue (m_lpszData,		s.m_strData);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
	*/

	return rst;
}


DB_API bool FoxjetDatabase::AddSettingsRecord (COdbcDatabase & database, SETTINGSSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	ASSERT (database.IsOpen ());

	if (GetSettingsRecord (database, s.m_lLineID, s.m_strKey, SETTINGSSTRUCT ()))
		bResult = false;
	else {
		try {
			COdbcRecordset rst (&database);

			/* TODO: rem
			CString str;

			str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);
			*/

			rst << s;

			/* TODO: rem
			rst.Open (str, CRecordset::dynaset);
			rst.AddNew ();
			rst << s;
			rst.Update ();
			rst.Close ();
			*/

			bResult = true;
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	return bResult;
}

DB_API bool FoxjetDatabase::GetSettingsRecord (COdbcDatabase & database, ULONG lLineID, const CString & strKey,
											   SETTINGSSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);
		CString str, strKeyRevised (strKey);

		strKeyRevised.Replace (_T ("'"), _T (""));

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u AND [%s]='%s';"), 
			m_lpszTable, 
			m_lpszLineID, lLineID,
			m_lpszKey, strKeyRevised);
		rst.Open (str);

		if (!rst.IsEOF ()) {
			rst >> s;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetSettingsRecords (COdbcDatabase & database, ULONG lLineID, CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);
		CString str;

		if (lLineID == ALL)
			str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);
		else
			str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%d;"), 
				m_lpszTable, m_lpszLineID, lLineID);
		
		rst.Open (str);

		while (!rst.IsEOF ()) {
			SETTINGSSTRUCT s;

			rst >> s;
			v.Add (s);
			
			rst.MoveNext ();
		}

		rst.Close ();
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::UpdateSettingsRecord (COdbcDatabase & database, SETTINGSSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	CString str, strKey (s.m_strKey);

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);

		rst << s;
		bResult = true;
		
		/* TODO: rem
		strKey.Replace (_T ("'"), _T (""));

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u AND [%s]='%s';"), 
			m_lpszTable, 
			m_lpszLineID, s.m_lLineID,
			m_lpszKey, strKey);
		//TRACEF (str);
		rst.Open (str, CRecordset::dynaset);

		if (!rst.IsEOF ()) {
			if (!s.m_strData.GetLength ())
				s.m_strData += _T (" ");

			rst.Edit ();
			rst << s;
			rst.Update ();
			bResult = true;
		}

		rst.Close ();
		*/
	}
	catch (CDBException * e)		{ 
		TRACEF (strKey + _T (": ") + s.m_strData); HANDLEEXCEPTION (e); 
	}
	catch (CMemoryException * e)	{ TRACEF (strKey + _T (": ") + s.m_strData); HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::DeleteSettingsRecord (COdbcDatabase & database, ULONG lLineID, const CString & strKey)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		CString strSQL;

		strSQL.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%u AND [%s]='%s';"), 
			m_lpszTable, 
			m_lpszLineID, lLineID,
			m_lpszKey, strKey);
		TRACEF (strSQL);
		database.ExecuteSQL (strSQL);

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}
