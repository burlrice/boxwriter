// MessageRecordset.cpp : implementation file
//

#include "stdafx.h"
#include "OdbcRecordset.h"
#include "Database.h"
#include "Debug.h"
#include "FieldDefs.h"
#include "Serialize.h"
#include "compare.h"
#include "OdbcBulkRecordset.h"
#include "Parse.h"

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Messages;

extern HINSTANCE hInstance;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)

FoxjetDatabase::tagMESSAGESTRUCT::tagMESSAGESTRUCT ()
:	m_lID			(-1),
	m_lHeadID		(-1),
	m_lTaskID		(-1),
	m_lHeight		(0),
	m_bIsUpdated	(true)
{
//TRACEF ("tagMESSAGESTRUCT::tagMESSAGESTRUCT ()");
}

FoxjetDatabase::tagMESSAGESTRUCT::tagMESSAGESTRUCT (const tagMESSAGESTRUCT & rhs)
:	m_lID			(rhs.m_lID),
	m_lHeadID		(rhs.m_lHeadID),
	m_lTaskID		(rhs.m_lTaskID),
	m_strName		(rhs.m_strName),
	m_strDesc		(rhs.m_strDesc),
	m_strData		(rhs.m_strData),
	m_lHeight		(rhs.m_lHeight),
	m_bIsUpdated	(rhs.m_bIsUpdated)
{
//TRACEF ("tagMESSAGESTRUCT::tagMESSAGESTRUCT (const tagMESSAGESTRUCT & rhs)");
}

tagMESSAGESTRUCT & FoxjetDatabase::tagMESSAGESTRUCT::operator = (const tagMESSAGESTRUCT & rhs)
{
//TRACEF ("operator = (const tagMESSAGESTRUCT & rhs)");
	if (this != &rhs) {
		m_lID			= rhs.m_lID;
		m_lHeadID		= rhs.m_lHeadID;
		m_lTaskID		= rhs.m_lTaskID;
		m_strName		= rhs.m_strName;
		m_strDesc		= rhs.m_strDesc;
		m_strData		= rhs.m_strData;
		m_lHeight		= rhs.m_lHeight;
		m_bIsUpdated	= rhs.m_bIsUpdated;
	}

	return * this;
}

FoxjetDatabase::tagMESSAGESTRUCT::~tagMESSAGESTRUCT ()
{
//TRACEF ("~tagMESSAGESTRUCT ()");
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::MESSAGESTRUCT & lhs, const FoxjetDatabase::MESSAGESTRUCT & rhs)
{
	return
		lhs.m_lID			== rhs.m_lID &&
		lhs.m_lHeadID		== rhs.m_lHeadID &&
		lhs.m_lTaskID		== rhs.m_lTaskID &&
		lhs.m_strName		== rhs.m_strName &&
		lhs.m_strDesc		== rhs.m_strDesc &&
		lhs.m_strData		== rhs.m_strData &&
		lhs.m_lHeight		== rhs.m_lHeight &&
		lhs.m_bIsUpdated	== rhs.m_bIsUpdated;
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::MESSAGESTRUCT & lhs, const FoxjetDatabase::MESSAGESTRUCT & rhs)
{
	return !(rhs == lhs);
}

DB_API const FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator >> (const FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::MESSAGESTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		s.m_lID			= (long)rst.GetFieldValue		(m_lpszID);
		s.m_lHeadID		= (long)rst.GetFieldValue		(m_lpszHeadID);
		s.m_lTaskID		= (long)rst.GetFieldValue		(m_lpszTaskID);
		s.m_strName		= (CString)rst.GetFieldValue	(m_lpszName);
		s.m_strDesc		= (CString)rst.GetFieldValue	(m_lpszDesc);
		s.m_strData		= (CString)rst.GetFieldValue	(m_lpszData);
		s.m_lHeight		= (long)rst.GetFieldValue		(m_lpszHeight);
		s.m_bIsUpdated	= (bool)rst.GetFieldValue		(m_lpszIsUpdated);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API const FoxjetDatabase::COdbcBulkRecordset & FoxjetDatabase::operator >> (const FoxjetDatabase::COdbcBulkRecordset & rst, CArray <FoxjetDatabase::MESSAGESTRUCT, FoxjetDatabase::MESSAGESTRUCT &> & v)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		for (int i = 0; i < (int)rst.GetRowsFetched (); i++) {
			MESSAGESTRUCT s;

			s.m_lID			= (long)rst.GetFieldValue		(m_lpszID,			i);
			s.m_lHeadID		= (long)rst.GetFieldValue		(m_lpszHeadID,		i);
			s.m_lTaskID		= (long)rst.GetFieldValue		(m_lpszTaskID,		i);
			s.m_strName		= (CString)rst.GetFieldValue	(m_lpszName,		i);
			s.m_strDesc		= (CString)rst.GetFieldValue	(m_lpszDesc,		i);
			s.m_strData		= (CString)rst.GetFieldValue	(m_lpszData,		i);
			s.m_lHeight		= (long)rst.GetFieldValue		(m_lpszHeight,		i);
			s.m_bIsUpdated	= (bool)rst.GetFieldValue		(m_lpszIsUpdated,	i);

			v.Add (s);
		}
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator << (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::MESSAGESTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	CStringArray name, value;

	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	if (s.m_lID == -1) 
		rst.SetEditMode (COdbcRecordset::addnew);

	name.Add (m_lpszHeadID);	   value.Add (ToString ((long)s.m_lHeadID));
	name.Add (m_lpszTaskID);	   value.Add (ToString ((long)s.m_lTaskID));
	name.Add (m_lpszName);		   value.Add (s.m_strName);
	name.Add (m_lpszDesc);		   value.Add (s.m_strDesc);
	name.Add (m_lpszData);		   value.Add (s.m_strData);
	name.Add (m_lpszHeight);	   value.Add (ToString ((long)s.m_lHeight));
	name.Add (m_lpszIsUpdated);	   value.Add (_T ("1"));

	CString strSQL = FormatSQL (rst, m_lpszTable, name, value, m_lpszID, s.m_lID);
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

	if (s.m_lID == -1)
		s.m_lID = GetMaxID (rst.GetOdbcDatabase (), m_lpszTable, m_lpszID);
/*
	try {
		if (rst.IsAddNew ())
			rst.SetFieldValue (m_lpszID,	(long)GetNextID (rst.GetOdbcDatabase (), m_lpszTable));

		rst.SetFieldValue (m_lpszHeadID,	(long)s.m_lHeadID);
		rst.SetFieldValue (m_lpszTaskID,	(long)s.m_lTaskID);
		rst.SetFieldValue (m_lpszName,		(CString)s.m_strName);
		rst.SetFieldValue (m_lpszDesc,		(CString)s.m_strDesc);
		rst.SetFieldValue (m_lpszData,		(CString)s.m_strData);
		rst.SetFieldValue (m_lpszHeight,	(long)s.m_lHeight);
		rst.SetFieldValue (m_lpszIsUpdated,	true);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/

	return rst;
}


DB_API UINT FoxjetDatabase::GetMessageRecordCount (FoxjetDatabase::COdbcDatabase & database, ULONG lHeadID)
{
	DBMANAGETHREADSTATE (database);

	UINT nCount = 0;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);
		CString str = _T ("SELECT * FROM [") + CString (m_lpszTable) + ("];");

		if (lHeadID != ALL)
			str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
				m_lpszTable, m_lpszHeadID, lHeadID);

		rst.Open (str);
		nCount = rst.CountRecords ();
		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return nCount;
}

DB_API ULONG FoxjetDatabase::GetMessageID (COdbcDatabase & database, 
										   const CString & strMsg, 
										   const CString & strHead,
										   ULONG lPrinterID)
{
	DBMANAGETHREADSTATE (database);

	ULONG lResult = NOTFOUND;

	ASSERT (database.IsOpen ());

	try {
		HEADSTRUCT head;
		ULONG lHeadID = GetHeadID (database, strHead, lPrinterID);

		if (GetHeadRecord (database, lHeadID, head)) {
			COdbcRecordset rst (&database);
			CString str;

			str.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s' AND [%s]=%u"), 
				m_lpszTable, m_lpszName, strMsg, m_lpszHeadID, head.m_lID);
			//TRACEF (str);
			rst.Open (str);

			if (!rst.IsEOF ()) 
				lResult = (long)rst.GetFieldValue (m_lpszID);

			rst.Close ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return lResult;
}

DB_API bool FoxjetDatabase::GetMessageRecord (COdbcDatabase & database, 
											  const CString & strMessage, 
											  const CString & strHead, 
											  MESSAGESTRUCT & msg)
{
	DBMANAGETHREADSTATE (database);
	
	ULONG lHeadID = GetHeadID (database, strHead, ALL);

	bool bResult = GetMessageRecord (database, strMessage, lHeadID, msg);

	return bResult;
}

DB_API bool FoxjetDatabase::GetMessageRecord (COdbcDatabase & database, 
											  const CString & strMessage,
											  ULONG lHeadID,
											  MESSAGESTRUCT & msg)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s' AND [%s]=%u"), 
			m_lpszTable, 
			m_lpszName, strMessage, 
			m_lpszHeadID, lHeadID);
		rst.Open (str);

		if (!rst.IsEOF ()) {
			rst >> msg;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetMessageRecord (COdbcDatabase & database, 
											  ULONG lID, MESSAGESTRUCT & msg)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, lID);
		rst.Open (str);

		if (!rst.IsEOF ()) {
			rst >> msg;
			bResult = true;
		}

		rst.Close ();
	}
	
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::AddMessageRecord (COdbcDatabase & database, 
											  MESSAGESTRUCT & msg)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);

		msg.m_lID = -1;
		rst << msg;

		/* TODO: rem
		CString strSQL;

		strSQL.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);
		rst.Open (strSQL, CRecordset::dynaset);
		rst.AddNew ();
		rst << msg;
		rst.Update ();
		rst.MoveLast ();
		msg.m_lID = (long)rst.GetFieldValue (m_lpszID);
		rst.Close ();
		*/

		bResult = true;
	}
 	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::UpdateMessageRecord (COdbcDatabase & database, MESSAGESTRUCT & msg)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (database);

		rst << msg;
		bResult = true;

		/* TODO: rem
		CString strSQL;

		strSQL.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), m_lpszTable, m_lpszID, msg.m_lID);
		rst.Open (strSQL, CRecordset::dynaset);

		if (!rst.IsEOF ()) {
			rst.Edit ();
			rst << msg;
			rst.Update ();
			bResult = true;
		}

		rst.Close ();
		*/
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::DeleteMessageRecord (COdbcDatabase & database, 
												 ULONG lID)
{
	DBMANAGETHREADSTATE (database);

	CString strSQL;
	bool bResult = false;

	
	ASSERT (database.IsOpen ());

	strSQL.Format (
		_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
		m_lpszTable, m_lpszID, lID);

	try {
		database.ExecuteSQL (strSQL);
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetMessageRecords (COdbcDatabase & database, 
											   ULONG lHeadID, 
											   CArray <MESSAGESTRUCT, MESSAGESTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	v.RemoveAll ();

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszHeadID, lHeadID);
		rst.Open (str);

		while (!rst.IsEOF ()) {
			MESSAGESTRUCT msg;

			rst >> msg;
			v.Add (msg);
			rst.MoveNext ();
		}
		
		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetMessageRecords (COdbcDatabase & database, 
											   ULONG lHeadID, 
											   bool bIsUpdated, 
											   CArray <MESSAGESTRUCT, MESSAGESTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	v.RemoveAll ();

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u AND [%s]=%s;"), 
			m_lpszTable, 
			m_lpszHeadID, lHeadID,
			m_lpszIsUpdated, bIsUpdated ? "True" : "False");
		rst.Open (str);

		while (!rst.IsEOF ()) {
			MESSAGESTRUCT msg;

			rst >> msg;
			v.Add (msg);
			rst.MoveNext ();
		}
		
		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetMessageRecords (COdbcDatabase & database, 
											   const CString & strHead, 
											   CArray <MESSAGESTRUCT, MESSAGESTRUCT &> & v)
{
	// TODO: need line id
	DBMANAGETHREADSTATE (database);

	ULONG lHeadID = GetHeadID (database, strHead, ALL);
	bool bResult = GetMessageRecords (database, lHeadID, v);

	return bResult;
}

DB_API bool FoxjetDatabase::SetMessageRecordModified (COdbcDatabase & database, ULONG lID, bool bModified)
{
	DBMANAGETHREADSTATE (database);

	CString str;
	bool bResult = false;

	str.Format (_T ("UPDATE [%s] SET [%s].[%s] = %s WHERE [%s].[%s]=%u;"),
		m_lpszTable,
		m_lpszTable,
		m_lpszIsUpdated,
		bModified ? _T ("True") : _T ("False"),
		m_lpszTable,
		m_lpszID,
		lID);

	try {
		database.ExecuteSQL (str);
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}