// OdbcBulkRecordset.h: interface for the COdbcBulkRecordset class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ODBCBULKRECORDSET_H__0DC0101F_D060_425B_A03C_9B0E2C786912__INCLUDED_)
#define AFX_ODBCBULKRECORDSET_H__0DC0101F_D060_425B_A03C_9B0E2C786912__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "OdbcRecordset.h"

namespace FoxjetDatabase
{
	class DB_API COdbcBulkRecordset : public COdbcRecordset  
	{
		DECLARE_DYNAMIC(COdbcBulkRecordset)

	public:
		COdbcBulkRecordset (COdbcDatabase & database);
		COdbcBulkRecordset (COdbcDatabase * pDatabase);
		virtual ~COdbcBulkRecordset();

		ULONG CountRecords ();

		virtual BOOL Open (LPCTSTR lpszSQL, UINT nOpenType = CRecordset::snapshot, 
			DWORD dwOptions = CRecordset::useMultiRowFetch); 
		virtual BOOL Open (UINT nOpenType = AFX_DB_USE_DEFAULT_TYPE, 
			LPCTSTR lpszSQL = NULL, DWORD dwOptions = CRecordset::useMultiRowFetch);
		// throw CDBException, CMemoryException 

		virtual void DoBulkFieldExchange(CFieldExchange* pFX);
		virtual void Close ();

		void GetFieldValue (LPCTSTR lpszName, int nRow, COdbcVariant & v) const;
		void GetFieldValue (short nIndex, int nRow, COdbcVariant & v) const;
		COdbcVariant GetFieldValue (LPCTSTR lpszName, int nRow) const;
		COdbcVariant GetFieldValue (short nIndex, int nRow) const;
		// throw CDBException

		static const CString m_strBadRow;

	protected:
		COdbcBulkRecordset ();
		COdbcBulkRecordset (const COdbcBulkRecordset &);
		COdbcBulkRecordset & operator = (const COdbcBulkRecordset &);

		void **		m_ppvData; 
		void **		m_ppvLength;
		int			m_nAllocatedFields; 
	};
}; //namespace FoxjetDatabase

#endif // !defined(AFX_ODBCBULKRECORDSET_H__0DC0101F_D060_425B_A03C_9B0E2C786912__INCLUDED_)
