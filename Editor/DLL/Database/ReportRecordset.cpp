// ReportRecordset.cpp : implementation file
//

#include "stdafx.h"

#include <math.h>

#include "Database.h"
#include "Debug.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Serialize.h"
#include "compare.h"
#include "Resource.h"
#include "Parse.h"
#include "TemplExt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Reports;

extern HINSTANCE hInstance;

struct
{
	REPORTTYPE	m_type;
	UINT		m_nID;
} map [] = 
{
	{ REPORT_TASK_CREATE,			 IDS_CREATETASK			},
	{ REPORT_TASK_MODIFY,			 IDS_MODIFYTASK			},
	{ REPORT_TASK_DESTROY,			 IDS_DESTROYTASK		},
	{ REPORT_TASK_START,			 IDS_STARTTASK			},
	{ REPORT_TASK_START_REMOTE,		 IDS_STARTTASKREMOTE	},
	{ REPORT_TASK_START_POWERUP,	 IDS_STARTTASKPOWERUP	},
	{ REPORT_TASK_IDLE,				 IDS_IDLETASK			},
	{ REPORT_TASK_IDLE_ONERROR,		 IDS_IDLETASK_ONERROR	},
	{ REPORT_TASK_RESUME,			 IDS_RESUMETASK			},
	{ REPORT_TASK_STOP,				 IDS_STOPTASK			},
	{ REPORT_AMS_CYCLE,				 IDS_AMSCYCLE			},
	{ REPORT_EXECSTART,				 IDS_EXECSTART			},
	{ REPORT_EXECSTOP,				 IDS_EXECSTOP			},
	{ REPORT_KEYFIELDSTART,			 IDS_KEYFIELDSTART		},
	{ REPORT_STROBESTATE,			 IDS_STROBESTATE		}, 
	{ REPORT_TIMECHANGE,			 IDS_REPORT_TIMECHANGE	},
	{ REPORT_INKCODE,				 IDS_REPORT_INKCODE		},
	{ REPORT_DIAGNOSTIC,			 IDS_REPORT_DIAGNOSTIC	},
	{ REPORT_ALL,					 IDS_ALL				}, // this one should always be the last
};


// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)

FoxjetDatabase::tagREPORTSTRUCT::tagREPORTSTRUCT ()
:	m_dtTime (COleDateTime::GetCurrentTime ()),
	m_lType (EDITOR),
	m_lPrinterID (0)
{
}

FoxjetDatabase::tagREPORTSTRUCT::tagREPORTSTRUCT (const tagREPORTSTRUCT & rhs)
:	m_dtTime		(rhs.m_dtTime),
	m_strAction		(rhs.m_strAction),
	m_strUsername	(rhs.m_strUsername),
	m_strCounts		(rhs.m_strCounts),
	m_strLine		(rhs.m_strLine),
	m_strTaskName	(rhs.m_strTaskName),
	m_lPrinterID	(rhs.m_lPrinterID)
{
	if (rhs.m_pmapUser.get())
		m_pmapUser.reset (new std::map<std::wstring, std::wstring>(*rhs.m_pmapUser.get()));
}

tagREPORTSTRUCT & FoxjetDatabase::tagREPORTSTRUCT::operator = (const tagREPORTSTRUCT & rhs)
{
	if (this != &rhs) {
		m_dtTime		= rhs.m_dtTime;
		m_strAction		= rhs.m_strAction;
		m_strUsername	= rhs.m_strUsername;
		m_strCounts		= rhs.m_strCounts;
		m_strLine		= rhs.m_strLine;
		m_strTaskName	= rhs.m_strTaskName;
		m_lPrinterID	= rhs.m_lPrinterID;

		if (rhs.m_pmapUser.get())
			m_pmapUser.reset(new std::map<std::wstring, std::wstring>(*rhs.m_pmapUser.get()));
	}

	return * this;
}

FoxjetDatabase::tagREPORTSTRUCT::~tagREPORTSTRUCT ()
{
}

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)

FoxjetDatabase::tagBOXUSAGESTRUCT::tagBOXUSAGESTRUCT ()
{
}

FoxjetDatabase::tagBOXUSAGESTRUCT::tagBOXUSAGESTRUCT (const tagBOXUSAGESTRUCT & rhs)
:	m_task (rhs.m_task),
	m_line (rhs.m_line)
{
}

tagBOXUSAGESTRUCT & FoxjetDatabase::tagBOXUSAGESTRUCT::operator = (const tagBOXUSAGESTRUCT & rhs)
{
	if (this != &rhs) {
		m_task = rhs.m_task;
		m_line = rhs.m_line;
	}

	return * this;
}

FoxjetDatabase::tagBOXUSAGESTRUCT::~tagBOXUSAGESTRUCT ()
{
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::REPORTSTRUCT & lhs, const FoxjetDatabase::REPORTSTRUCT & rhs)
{
	return
		lhs.m_dtTime		== rhs.m_dtTime &&
		lhs.m_strAction		== rhs.m_strAction &&
		lhs.m_strUsername	== rhs.m_strUsername &&
		lhs.m_strCounts		== rhs.m_strCounts &&
		lhs.m_strLine		== rhs.m_strLine &&
		lhs.m_strTaskName	== rhs.m_strTaskName &&
		lhs.m_lType			== rhs.m_lType &&
		lhs.m_lPrinterID	== rhs.m_lPrinterID;
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::REPORTSTRUCT & lhs, const FoxjetDatabase::REPORTSTRUCT & rhs)
{
	return !(rhs == lhs);
}

DB_API void FoxjetDatabase::GetReportUserColumns(COdbcDatabase& database, CStringArray& v)
{
	try
	{
		COdbcRecordset rst(database);
		const LPCTSTR lpszColumns[] =
		{
			Reports::m_lpszTime,
			Reports::m_lpszAction,
			Reports::m_lpszUsername,
			Reports::m_lpszCounts,
			Reports::m_lpszLine,
			Reports::m_lpszTaskName,
			Reports::m_lpszType,
			Reports::m_lpszPrinterID,
		};
		std::vector<std::wstring> vstrCols;

		for each (auto col in lpszColumns)
			vstrCols.push_back(col);

		rst.Open(CString(_T("SELECT * FROM ")) + Reports::m_lpszTable);

		for (short i = 0; i < rst.GetODBCFieldCount(); i++) {
			CString str = FormatReportUserColumnName(rst.GetFieldName(i));

			if (Find(vstrCols, (std::wstring)str) == -1)
				v.Add(str);
		}
	}
	catch (CDBException * e) { HANDLEEXCEPTION(e); }
	catch (CMemoryException * e) { HANDLEEXCEPTION(e); }
}

DB_API const COdbcRecordset & FoxjetDatabase::operator >> (const COdbcRecordset & rst, 
														   REPORTSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		CStringArray v;

		GetReportUserColumns(* (COdbcDatabase *)(LPVOID)&rst.GetOdbcDatabase(), v);

		s.m_dtTime			= (COleDateTime)rst.GetFieldValue (m_lpszTime).ToOleDateTime();
		s.m_strAction		= (CString)rst.GetFieldValue (m_lpszAction);
		s.m_strUsername		= (CString)rst.GetFieldValue (m_lpszUsername);
		s.m_strCounts		= (CString)rst.GetFieldValue (m_lpszCounts);
		s.m_strLine			= (CString)rst.GetFieldValue (m_lpszLine);
		s.m_strTaskName		= (CString)rst.GetFieldValue (m_lpszTaskName);
		s.m_lType			= (LONG)rst.GetFieldValue (m_lpszType);

		if (rst.GetFieldIndex (m_lpszPrinterID) != -1)
			s.m_lPrinterID	= (LONG)rst.GetFieldValue (m_lpszPrinterID);

		int nUserIndex = rst.GetODBCFieldCount() - v.GetSize();

		if (nUserIndex > 0) {
			auto* pmap = new std::map<std::wstring, std::wstring>();
			auto& map = *pmap;

			s.m_pmapUser.reset(pmap);
			
			for (short i = nUserIndex; i < rst.GetODBCFieldCount(); i++) 
				map[(LPCTSTR)FormatReportUserColumnName(rst.GetFieldName(i))] = (LPCTSTR)(CString)rst.GetFieldValue(i);
		}
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API CString FoxjetDatabase::FormatReportUserColumnName(CString str)
{
	const CString strReplace[] = { _T("["), _T("]") };

	int nStart = str.ReverseFind('[');
	int nEnd = str.ReverseFind(']');

	if (nStart != -1 && nEnd != -1) 
		if (nStart < nEnd) 
			str = str.Mid(nStart + 1, nEnd - nStart - 1);

	for each (auto s in strReplace)
		str.Replace(s, _T("_"));

	return str;
}

DB_API COdbcRecordset & FoxjetDatabase::operator << (COdbcRecordset & rst, REPORTSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	CStringArray name, value;

	name.Add (m_lpszTime);			value.Add (s.m_dtTime.Format (MDB_DATE_STRFTIME));
	name.Add (m_lpszAction);		value.Add (s.m_strAction);
	name.Add (m_lpszUsername);		value.Add (s.m_strUsername);
	name.Add (m_lpszCounts);		value.Add (s.m_strCounts);
	name.Add (m_lpszLine);			value.Add (s.m_strLine);
	name.Add (m_lpszTaskName);		value.Add (s.m_strTaskName);
	name.Add (m_lpszType);			value.Add (ToString (s.m_lType));

	if (rst.GetFieldIndex (m_lpszPrinterID) != -1) 
		name.Add (m_lpszPrinterID);			value.Add (ToString (s.m_lPrinterID));

	if (s.m_pmapUser.get()) {
		CStringArray vUserCols;

		GetReportUserColumns(rst.GetOdbcDatabase(), vUserCols);

		for (auto i = s.m_pmapUser.get()->begin(); i != s.m_pmapUser.get()->end(); i++) {
			if (Find(vUserCols, i->first.c_str()) != -1) {
				name.Add(FormatReportUserColumnName(i->first.c_str()));
				value.Add(i->second.c_str());
			}
		}
	}

	rst.SetEditMode (COdbcRecordset::addnew);
	CString strSQL = FormatSQL (rst, m_lpszTable, name, value, _T (""), 0);
	TRACEF(strSQL);
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

	return rst;
}

static CString GetAction (REPORTTYPE type)
{
	int i;

	for (i = 0; ::map [i].m_type != REPORT_ALL; i++)
		if (type == ::map [i].m_type)
			return LoadString (::map [i].m_nID);

	return LoadString (::map [i].m_nID);
}


static bool bPurge = false;
static int nDays = 30;

DB_API void FoxjetDatabase::EnablePrintReportPurge (bool bEnable, int nDays)
{
	::bPurge = bEnable;
	::nDays = nDays;
}

DB_API bool FoxjetDatabase::AddPrinterReportRecord (COdbcDatabase & database, REPORTTYPE type, REPORTSTRUCT & s, ULONG lPrinterID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	CString strAction = GetAction (type);
	CString str;

	str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);
	s.m_strAction	= strAction;
	s.m_lType		= PRINTER;
	s.m_lPrinterID	= lPrinterID;

	try {
		COdbcRecordset rst (&database);

		rst << s;

		bResult = true;

		if (::bPurge) {
			CString strSQL;

			if (database.HasTable (Printers::m_lpszTable)) 
				strSQL.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%lu AND (DateDiff ('d', [%s], Now ()) >= %d);"), 
					m_lpszTable, m_lpszPrinterID, lPrinterID, m_lpszTime, ::nDays);
			else
				strSQL.Format (_T ("DELETE * FROM [%s] WHERE (DateDiff ('d', [%s], Now ()) >= %d);"), m_lpszTable, m_lpszTime, ::nDays);

			TRACEF (strSQL);
			database.ExecuteSQL (strSQL);
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::AddReportRecord (COdbcDatabase & database, REPORTTYPE type, REPORTSTRUCT & s, ULONG lPrinterID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	CString strAction = GetAction (type);
	CString str;

	str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);

	if (s.m_strAction.Find (strAction) == -1)
		s.m_strAction = _T ("[") + strAction + _T ("] ") + s.m_strAction;

	s.m_strAction = strAction;
	s.m_lPrinterID = lPrinterID;

	try {
		COdbcRecordset rst (&database);

		rst << s;

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetPrinterReportRecords (COdbcDatabase & database, CString strLine, CArray <REPORTSTRUCT, REPORTSTRUCT &> & v, ULONG lPrinterID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	CString str;

	if (strLine.GetLength ()) {
		if (database.HasTable (Printers::m_lpszTable)) 
			str.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s' AND [%s]=%lu AND [%s]=%lu;"), 
				m_lpszTable, 
				m_lpszLine,			strLine, 
				m_lpszType,			PRINTER,
				m_lpszPrinterID,	lPrinterID);
				//m_lpszTime, m_lpszAction);
		else
			str.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s' AND [%s]=%lu;"), 
				m_lpszTable, 
				m_lpszLine,		strLine, 
				m_lpszType,		PRINTER); 
				//m_lpszTime, m_lpszAction);
	}
	else {
		if (database.HasTable (Printers::m_lpszTable)) 
			str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%lu AND [%s]=%lu ORDER BY [%s], [%s] ASC;"), 
				m_lpszTable, 
				m_lpszType,			PRINTER,
				m_lpszPrinterID,	lPrinterID,
				m_lpszLine,			m_lpszTime);
		else
			str.Format (_T ("SELECT * FROM [%s] WHERE AND [%s]=%lu ORDER BY [%s], [%s] ASC;"), 
				m_lpszTable, 
				m_lpszType,		PRINTER,
				m_lpszLine,		m_lpszTime);
	}

	v.RemoveAll ();

	try {
		COdbcRecordset rst (&database);

		TRACEF (str);
		rst.Open (str);

		while (!rst.IsEOF ()) {
			REPORTSTRUCT s;

			rst >> s;
			v.Add (s);

			rst.MoveNext ();
		}

		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetReportRecords (COdbcDatabase & database, REPORTTYPE type, CArray <REPORTSTRUCT, REPORTSTRUCT &> & v, ULONG lPrinterID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	CString strAction = GetAction (type);
	CString str;

	if (database.HasTable (Printers::m_lpszTable)) {
		if (type == REPORT_ALL)
			str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%lu;"), m_lpszTable, m_lpszPrinterID, lPrinterID);
		else
			str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%lu AND [%s] LIKE '%%%s%%';"), 
				m_lpszTable, m_lpszPrinterID, lPrinterID, m_lpszAction, strAction);
	}
	else {
		if (type == REPORT_ALL)
			str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);
		else
			str.Format (_T ("SELECT * FROM [%s] WHERE [%s] LIKE '%%%s%%';"), 
				m_lpszTable, m_lpszAction, strAction);
	}

	v.RemoveAll ();

	try {
		COdbcRecordset rst (&database);

		rst.Open (str);

		while (!rst.IsEOF ()) {
			REPORTSTRUCT s;

			rst >> s;
			v.Add (s);

			rst.MoveNext ();
		}

		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetReportRecords (COdbcDatabase & database, REPORTTYPE type, const COleDateTime & dtAfter,
											  const COleDateTime & dtBefore, ULONG lLineID, 
											  CArray <REPORTSTRUCT, REPORTSTRUCT &> & v, ULONG lPrinterID)
{
	bool bResult = false;

	try {
		COdbcRecordset rst (&database);
		CString str;
		CString strAction = GetAction (type);

		if (database.HasTable (Printers::m_lpszTable)) {
			if (lLineID == ALL) {
				if (type == REPORT_ALL)
					str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%lu AND [%s] > #%s# AND [%s] < #%s# ORDER BY [%s], [%s];"),
						m_lpszTable,
						m_lpszPrinterID, lPrinterID,
						m_lpszTime, dtAfter.Format (MDB_DATE_STRFTIME),
						m_lpszTime, dtBefore.Format (MDB_DATE_STRFTIME),
						m_lpszLine, m_lpszTime);
				else
					str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%lu AND [%s] Like '%%%s%%' AND [%s] > #%s# AND [%s] < #%s# ORDER BY [%s], [%s];"),
						m_lpszTable,
						m_lpszPrinterID, lPrinterID,
						m_lpszAction, strAction,
						m_lpszTime, dtAfter.Format (MDB_DATE_STRFTIME),
						m_lpszTime, dtBefore.Format (MDB_DATE_STRFTIME),
						m_lpszLine, m_lpszTime);
			}
			else {
				LINESTRUCT line;

				if (!GetLineRecord (database, lLineID, line))
					return false;

				if (type == REPORT_ALL)
					str.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s' AND [%s] > #%s# AND [%s] < #%s# ORDER BY [%s], [%s];"),
						m_lpszTable,
						m_lpszLine, line.m_strName,
						m_lpszTime, dtAfter.Format (MDB_DATE_STRFTIME),
						m_lpszTime, dtBefore.Format (MDB_DATE_STRFTIME),
						m_lpszLine, m_lpszTime);
				else
					str.Format (_T ("SELECT * FROM [%s] WHERE [%s] Like '%%%s%%' AND [%s]='%s' AND [%s] > #%s# AND [%s] < #%s# ORDER BY [%s], [%s];"),
						m_lpszTable,
						m_lpszAction, strAction,
						m_lpszLine, line.m_strName,
						m_lpszTime, dtAfter.Format (MDB_DATE_STRFTIME),
						m_lpszTime, dtBefore.Format (MDB_DATE_STRFTIME),
						m_lpszLine, m_lpszTime);
			}
		}
		else {
			if (lLineID == ALL) {
				if (type == REPORT_ALL)
					str.Format (_T ("SELECT * FROM [%s] WHERE [%s] > #%s# AND [%s] < #%s# ORDER BY [%s], [%s];"),
						m_lpszTable,
						m_lpszTime, dtAfter.Format (MDB_DATE_STRFTIME),
						m_lpszTime, dtBefore.Format (MDB_DATE_STRFTIME),
						m_lpszLine, m_lpszTime);
				else
					str.Format (_T ("SELECT * FROM [%s] WHERE [%s] Like '%%%s%%' AND [%s] > #%s# AND [%s] < #%s# ORDER BY [%s], [%s];"),
						m_lpszTable,
						m_lpszAction, strAction,
						m_lpszTime, dtAfter.Format (MDB_DATE_STRFTIME),
						m_lpszTime, dtBefore.Format (MDB_DATE_STRFTIME),
						m_lpszLine, m_lpszTime);
			}
			else {
				LINESTRUCT line;

				if (!GetLineRecord (database, lLineID, line))
					return false;

				if (type == REPORT_ALL)
					str.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s' AND [%s] > #%s# AND [%s] < #%s# ORDER BY [%s], [%s];"),
						m_lpszTable,
						m_lpszLine, line.m_strName,
						m_lpszTime, dtAfter.Format (MDB_DATE_STRFTIME),
						m_lpszTime, dtBefore.Format (MDB_DATE_STRFTIME),
						m_lpszLine, m_lpszTime);
				else
					str.Format (_T ("SELECT * FROM [%s] WHERE [%s] Like '%%%s%%' AND [%s]='%s' AND [%s] > #%s# AND [%s] < #%s# ORDER BY [%s], [%s];"),
						m_lpszTable,
						m_lpszAction, strAction,
						m_lpszLine, line.m_strName,
						m_lpszTime, dtAfter.Format (MDB_DATE_STRFTIME),
						m_lpszTime, dtBefore.Format (MDB_DATE_STRFTIME),
						m_lpszLine, m_lpszTime);
			}
		}

		TRACEF (str);
		rst.Open (str);

		while (!rst.IsEOF ()) {
			REPORTSTRUCT s;

			rst >> s;
			v.Add (s);

			rst.MoveNext ();
		}

		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::ClearPrinterReportRecords (COdbcDatabase & database, CString strLine, ULONG lPrinterID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		CString strSQL;

		if (database.HasTable (Printers::m_lpszTable)) 
			strSQL.Format (_T ("DELETE * FROM [%s] WHERE [%s]='%s' AND [%s]=%lu;"), 
				m_lpszTable, 
				m_lpszLine,
				strLine,
				m_lpszPrinterID, lPrinterID);
		else 
			strSQL.Format (_T ("DELETE * FROM [%s] WHERE [%s]='%s';"), 
				m_lpszTable, 
				m_lpszLine,
				strLine);

		database.ExecuteSQL (strSQL);
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::ClearReportRecords (COdbcDatabase & database, ULONG lPrinterID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		CString strSQL;
		CStringArray vUserColumns;
		
		if (database.HasTable (Printers::m_lpszTable)) 
			strSQL.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%lu;"), m_lpszTable, m_lpszPrinterID, lPrinterID);
		else
			strSQL.Format (_T ("DELETE * FROM [%s];"), m_lpszTable);

		database.ExecuteSQL (strSQL);
		bResult = true;

		GetReportUserColumns(database, vUserColumns);

		for (int i = 0; i < vUserColumns.GetSize(); i++) {
			strSQL.Format(_T("ALTER TABLE [%s] DROP [%s]"), Reports::m_lpszTable, vUserColumns[i]);
			database.ExecuteSQL(strSQL);
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetBoxUsage (COdbcDatabase & database, ULONG lBoxID, ULONG lLineID, CArray <BOXUSAGESTRUCT, BOXUSAGESTRUCT &> & v)
{
	using namespace FoxjetDatabase::Lines;
	using namespace FoxjetDatabase::Tasks;

	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	CString strSQL;

	if (lLineID == ALL) {
		strSQL.Format (
			_T ("SELECT [%s].* FROM [%s] INNER JOIN [%s] ON [%s].[%s] = [%s].[%s] WHERE [%s].[%s]=%d;"),
			Tasks::m_lpszTable,
			Lines::m_lpszTable, 
			Tasks::m_lpszTable,
			Lines::m_lpszTable, Lines::m_lpszID,
			Tasks::m_lpszTable, Tasks::m_lpszLineID,
			Tasks::m_lpszTable, Tasks::m_lpszBoxID,
			lBoxID);
	}
	else {
		strSQL.Format (
			_T ("SELECT * FROM [%s] WHERE [%s]=%d AND [%s]=%d;"),
			Tasks::m_lpszTable,		
			Tasks::m_lpszBoxID,
			lBoxID, 
			Tasks::m_lpszLineID,
			lLineID);
	}

	try {
		COdbcRecordset rst (database);
		
		rst.Open (strSQL);

		while (!rst.IsEOF ()) {
			BOXUSAGESTRUCT b;

			rst >> b.m_task;
			GetLineRecord (database, b.m_task.m_lLineID, b.m_line);
			v.Add (b);

			rst.MoveNext ();
		}

		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}
