
#include "stdafx.h"
#include "Parse.h"
#include "TemplExt.h"
#include "Parse.h"
#include "Debug.h"

using namespace FoxjetDatabase;
using namespace FoxjetCommon;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

static const LPCTSTR lpszVALVESTRUCT			= _T ("VALVESTRUCT");

////////////////////////////////////////////////////////////////////////////////

CString DB_API FoxjetDatabase::ToString (ULONG lVal) 
{
	CString str;
	str.Format (_T ("%u"), lVal);
	return str;
}

CString DB_API FoxjetDatabase::ToString (int nVal) 
{
	CString str;
	str.Format (_T ("%d"), nVal);
	return str;
}

CString DB_API FoxjetDatabase::ToString (bool bVal) 
{
	return bVal ? _T ("1") : _T ("0");
}

CString DB_API FoxjetDatabase::ToString (double dVal) 
{
	CString str;
	str.Format (_T ("%f"), dVal);

	if (str.Find (_T (".")) != -1)
		while (str.GetLength () > 2 && str [str.GetLength () - 2] != '.' && str [str.GetLength () - 1] == '0')
			str.Delete (str.GetLength () - 1);

	return str;
}

CString DB_API FoxjetDatabase::ToString (__int64 iVal)
{
	CString str;
	str.Format (_T ("%I64d"), iVal);
	return str;
}

CString DB_API FoxjetDatabase::ToString (unsigned __int64 iVal)
{
	CString str;
	str.Format (_T ("%I64u"), iVal);
	return str;
}

DB_API CString FoxjetDatabase::Tokenize(const CString &strInput, CStringArray &v, TCHAR cDelim, bool bTrim)
{
	#if defined( _DEBUG ) && defined( __TRACE_TOFROMSTRING__ )
	TRACEF ("Tokenize (" + strInput + ")");
	#endif //_DEBUG && __TRACE_TOFROMSTRING__
	
	CString str = bTrim ? Trim (strInput) : strInput;
	CLongArray commas = CountChars (str, cDelim);
	int nIndex = 0;

	v.RemoveAll ();

	for (int i = 0; i < commas.GetSize (); i++) {
		int nLen = commas [i] - nIndex;
		v.Add (str.Mid (nIndex, nLen));
		nIndex = commas [i] + 1;
	}

	CString strRemainder = str.Mid (nIndex);

	if (strRemainder.GetLength ())
		v.Add (strRemainder);
	else {
		if (strInput.GetLength () && strInput [strInput.GetLength () - 1] == cDelim)
			v.Add (_T (""));
	}

	#if defined( _DEBUG ) && defined( __SHOW_TOKENS__ )
	{
		CString strDebug;

		for (int i = 0; i < v.GetSize (); i++) {
			strDebug.Format ("v [%u] = \"%s\"%s", 
				i, v [i], (i + 1 == v.GetSize ()) ? "\n" : "");
			TRACEF (strDebug);
		}
	}
	#endif //_DEBUG && __SHOW_TOKENS__

	return v.GetSize () ? v [0] : _T ("");
}

DB_API FoxjetCommon::CLongArray FoxjetDatabase::CountChars (const CString & str, TCHAR c)
{
	CLongArray index;

	//{ CString str; str.Format (_T ("CountChars: %d (%c)"), c, c); TRACEF (str); }

	for (int i = 0; i < str.GetLength (); i++) {
		TCHAR cCur = str [i];

		//{ CString str; str.Format (_T ("[%03d]: %d (%c)"), i, cCur, cCur); TRACEF (str); }

		if (cCur == '\\') {
			i++;
			continue;
		}
		else if (cCur == c) 
			index.Add (i);
	}
/*
	TCHAR cCur, cPrev = '\0';

	for (int i = 0; i < str.GetLength (); i++) {
		cCur = str [i];

		if (cCur == '\\') {
			i++;
			continue;
		}
		else if (cCur == c) 
			index.Add (i);

		cPrev = cCur;
	}
*/

	return index;
}

DB_API CString FoxjetDatabase::FormatString (const CString &str, bool bFilename)
{
	CString strResult (str);
	TCHAR cDelim [] = { '\\', ',', '{', '}', 0 }; // the '\' char must be first

	if (!bFilename) {
		strResult.Replace (_T ("\\u"), _T ("<u>"));
		strResult.Replace (_T ("\\U"), _T ("<U>"));
		strResult.Replace (_T ("\r\n"), _T ("<CR>"));
	}

	for (int i = 0; cDelim [i] != 0; i++) {
		int nIndex = 0;
	
		do {
			nIndex = strResult.Find (cDelim [i], nIndex);

			if (nIndex != -1) {
				strResult.Insert (nIndex, '\\');
				nIndex += 2;
			}
		}
		while (nIndex != -1);
	}


	if (!bFilename) {
		for (int i = strResult.GetLength () - 1; i >= 0; i--) {
			TCHAR c = strResult [i];

			if (c > 255) {
				CString strEncode;
				//int a = HIBYTE (c);
				//int b = LOBYTE (c);

				strEncode.Format (_T ("\\u%04X"), c);
				//strEncode.Format (_T ("\\C%c%c"), a, b);

				strResult.Delete (i);
				strResult.Insert (i, strEncode);
			}
		}
	}

//	TRACEF (str + " --> " + strResult);

	return strResult;
}

DB_API CString FoxjetDatabase::UnformatString(const CString &strSrc)
{
	CString strResult, str (strSrc);

	for (int i = 0; i < str.GetLength (); i++) {
		if (str [i] == '\\') {
			i++;

			if (i >= str.GetLength ())
				break;

			if ((i + 4) < str.GetLength ()) {
				if (str [i] == 'u' || str [i] == 'U') {
					CString strEncode = str.Mid (i + 1, 4);
					TCHAR cEncode = (TCHAR)_tcstoul (strEncode, NULL, 16);
					wchar_t psz [] = { (wchar_t)_tcstoul (strEncode, NULL, 16), 0 };

					//TRACEF (CString (cEncode));
					//TRACEF (std::string (1, cEncode).c_str ());
					//TRACEF (std::wstring (1, cEncode).c_str ());
					//TRACEF (strEncode + _T (" --> ") + (CString)wstrtostr ((std::wstring)CString (cEncode)).c_str () + _T (": ") + strResult);
					//TRACEF (psz);

					strResult += CString (cEncode);
					//strResult += psz;
					i += 4;
					continue;
				}
			}

			if ((i + 2) < str.GetLength ()) {
				if (str [i] == 'c' || str [i] == 'C') {
					CString strEncode = str.Mid (i + 1, 2);
					int a = strEncode [0];
					int b = strEncode [1];
					TCHAR cEncode = MAKEWORD (b, a);

					strResult += cEncode;
					i += 2;
					continue;
				}
			}
		}

		strResult += str [i];
	}

	strResult.Replace (_T ("<CR>"), _T ("\r\n"));
	strResult.Replace (_T ("<u>"), _T ("\\u"));
	strResult.Replace (_T ("<U>"), _T ("\\U"));

	//TRACEF (strSrc + " --> " + strResult);

	return strResult;
}

DB_API CString FoxjetDatabase::Trim (CString str)
{
	str.TrimLeft ();
	str.TrimRight ();

	if (str.GetLength () && str [0] == '{')
		str.Delete (0);

	if (str.GetLength () == 1 && str [0] == '}')
		str.Delete (0);

	int nLen = str.GetLength ();

	if (str.GetLength () > 2 && str [nLen - 2] != '\\' && str [nLen - 1] == '}')
		str.Delete (str.GetLength () - 1);

	return str;
}

DB_API CString FoxjetDatabase::Extract (const CString & strParse, const CString & strBegin)
{
	CString str;
	int nIndex = strParse.Find (strBegin);

	if (nIndex != -1) 
		str = strParse.Mid (nIndex + strBegin.GetLength ());

	return str;
}

DB_API CString FoxjetDatabase::Extract (const CString & strParseIn, const CString & strBegin, const CString & strEnd, bool bInclusive)
{
	CString strParse = strParseIn + (strEnd == _T (" ") ? _T (" ") : _T (""));
	CString str;
	int nIndex [2] = 
	{
		strParse.Find (strBegin),

		// start looking for strEnd after strBegin
		strParse.Find (strEnd, (nIndex [0] == -1) ? 0 : nIndex [0] + strBegin.GetLength ()), 
	};

	if (nIndex [0] != -1 && nIndex [1] != -1) {
		int nStart = nIndex [0] + strBegin.GetLength ();
		int nLen = nIndex [1] - nStart;

		if (nLen > 0)
			str = strParse.Mid (nStart, nLen);
	}

	if (bInclusive)
		return strBegin + str + strEnd;
	else
		return str;
}

DB_API CString FoxjetDatabase::ToString (const CStringArray & v) 
{
	CString str;

	for (int i = 0; i < v.GetSize (); i++) {
		str += FormatString (v [i]);// + _T (",");

		if ((i + 1) < v.GetSize ())
			str += _T (",");
	}

	return _T ("{") + str + _T ("}");
}

DB_API int FoxjetDatabase::FromString (const CString & strInput, CStringArray & v)
{
	const int nCount = v.GetSize ();
	CString str = Trim (strInput);

	Tokenize (str, v);

	int nSize = v.GetSize ();

	if (strInput.Find (_T ("{BITMAP,")) == 0) {
		v [1] = UnformatString (v [1]);
		return v.GetSize ();
	}

	for (int i = nCount; i < v.GetSize (); i++) 
		v [i] = UnformatString (v [i]);

	return v.GetSize () - nCount;
}


DB_API void FoxjetDatabase::ParsePackets (const CString & str, CStringArray & v)
{
	CLongArray vLeft = CountChars (str, '{');
	CLongArray vRight = CountChars (str, '}');
	CLongArray vDelim = MergeAcending (vLeft, vRight);
	int nCount = 0;

	for (int i = 0; i < vDelim.GetSize (); i++) {
		int nIndex = vDelim [i];

		if (str [nIndex] == '{')
			nCount++;
		else if (str [nIndex] == '}')
			nCount--;

		if (nCount == 0) {
			int nFirst = vLeft [0] + 1;
			int nLast = nIndex;
			int nLen = nLast - nFirst;
			CString strSegment = str.Mid (nFirst, nLen);

			v.Add (strSegment);

			if (nLast < str.GetLength () - 1) {
				CString strRemainder = str.Mid (nLast + 1);
				
				ParsePackets (strRemainder, v);
			}

			return;
		}
	}
}

DB_API CString FoxjetDatabase::GetKey (const CString & strPacket)
{
	CStringArray v;
	CString str;

	Tokenize (strPacket, v);

	if (v.GetSize ())
		str = v [0];

	return str;
}

DB_API void FoxjetDatabase::ExtractPackets (const CString & strKey, const CString & str, CStringArray & v)
{
	CStringArray vTmp;

	ParsePackets (str, vTmp);

	for (int i = 0; i < vTmp.GetSize (); i++) 
		if (!strKey.CompareNoCase (GetKey (vTmp [i])))
			v.Add (vTmp [i]);
}

DB_API CString FoxjetDatabase::SetPackets (const CString & strPacket, const CString & strData)
{
	CString str;
	CStringArray vPackets;
	const CString strKey = GetKey (strPacket);
	bool bFound = false;

	ParsePackets (strData, vPackets);

	for (int i = 0; i < vPackets.GetSize (); i++) {
		if (!strKey.CompareNoCase (GetKey (vPackets [i]))) { 
			str += strPacket;
			bFound = true;
		}
		else 
			str += '{' + vPackets [i] + '}';
	}

	if (!bFound)
		str += strPacket;

	return str;
}

DB_API CString FoxjetDatabase::GetParam (const CStringArray & v, int nIndex, const CString & strDef)
{
	CString strResult = strDef;

	if (nIndex >= 0 && nIndex < v.GetSize ()) 
		strResult = v [nIndex];

	return strResult;
}

////////////////////////////////////////////////////////////////////////////////

CString DB_API FoxjetDatabase::ToString (const FoxjetDatabase::VALVESTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszVALVESTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lID));
	v.Add (FoxjetDatabase::ToString (s.m_lCardID));
	v.Add (FoxjetDatabase::ToString (s.m_lPhotocellDelay));
	v.Add (FoxjetDatabase::ToString (s.m_bEnabled));
	v.Add (FoxjetDatabase::ToString (s.m_bReversed));
	v.Add (FoxjetDatabase::ToString ((long)s.m_type));
	v.Add (FoxjetDatabase::ToString (s.m_lHeight));
	v.Add (FoxjetDatabase::ToString (s.m_lPanelID));
	v.Add (FormatString (s.m_strName));
	v.Add (FoxjetDatabase::ToString (s.m_nIndex));
	v.Add (FoxjetDatabase::ToString (s.m_nLinked));
	v.Add (FoxjetDatabase::ToString (s.m_bUpsidedown));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::VALVESTRUCT & s)
{
	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszVALVESTRUCT)) {
		s.m_lID					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lCardID				= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lPhotocellDelay		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_bEnabled			= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_bReversed			= _tcstoul (GetParam (v, nIndex++), NULL, 10) ? true : false;
		s.m_type				= (VALVETYPE)_tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lHeight				= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lPanelID			= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strName				= UnformatString (GetParam (v, nIndex++));
		s.m_nIndex				= _ttoi (GetParam (v, nIndex++));
		s.m_nLinked				= _ttoi (GetParam (v, nIndex++));
		s.m_bUpsidedown			= _tcstoul (GetParam (v, nIndex++), NULL, 10) ? true : false;

		return true;
	}

	return false; 
}

DB_API CString FoxjetDatabase::TokenizeCmdLine (const CString &strInput, CStringArray &v)
{
	CString strSeg, str (strInput);

	while (str.GetLength ()) {
		TCHAR c = str [0];

		if (c == '\"') {
			CString strTmp = Extract (str, _T ("\""), _T ("\""));
			
			if (strTmp.GetLength ()) {
				int nLen = strTmp.GetLength () + 2;

				strSeg += _T ("\"") + strTmp + _T ("\"");
	
				if (nLen <= str.GetLength ())
					str.Delete (0, nLen);
				
				v.Add (strSeg);
				strSeg.Empty ();
			}

			continue;
		}
		else if (c == ' ') {
			if (strSeg.GetLength ()) {
				v.Add (strSeg);
				strSeg.Empty ();
			}
		}
		else 
			strSeg += c;

		str.Delete (0);
	}

	if (strSeg.GetLength ())
		v.Add (strSeg);

	return v.GetSize () ? v [0] : _T ("");
}

DB_API std::string FoxjetDatabase::UnicodeToUTF8(const std::wstring &wstr)
{
    if( wstr.empty() ) return std::string();
    int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
    std::string strTo( size_needed, 0 );
    WideCharToMultiByte                  (CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
    return strTo;
}

DB_API std::wstring FoxjetDatabase::UTF8ToUnicode(const std::string &str)
{
    if( str.empty() ) return std::wstring();
    int size_needed = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
    std::wstring wstrTo( size_needed, 0 );
    MultiByteToWideChar                  (CP_UTF8, 0, &str[0], (int)str.size(), &wstrTo[0], size_needed);
    return wstrTo;
}

DB_API CString FoxjetDatabase::UrlDecode (const CString & str)
{
	CString strResult (str);

	for (int i = 0; i < 256; i++) {
		CString strTmp;

		strTmp.Format (_T ("%%%02X"), i);
		
		strResult.Replace (strTmp, CString ((TCHAR)i));
	}

	for (int i = 0; i < 256; i++) {
		CString strTmp;

		strTmp.Format (_T ("%%%X"), i);

		strResult.Replace (strTmp, CString ((TCHAR)i));
	}

	return strResult;
}

DB_API std::vector <std::wstring> FoxjetDatabase::UrlDecode (const std::wstring & str)
{
	std::vector <std::wstring> v;
	CString s = str.c_str ();
	int n = s.Find (_T ("?"));

	if (n != -1) {
		CString a = s.Left (n);
		CString b = s.Mid (n + 1);
		std::vector <std::wstring> vTmp = explode (std::wstring (b), '&');

		v.push_back (std::wstring (a));

		for (int i = 0; i < vTmp.size (); i++)
			v.push_back (vTmp [i]);
	}

	return v;
}
