// LineRecordset.cpp : implementation file
//

// sw0867

#include "stdafx.h"
#include "Database.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Debug.h"
#include "Serialize.h"
#include "compare.h"
#include "Resource.h"
#include "Parse.h"
#include "DbImp.h"
#include "Winsock2.h"
#include "CopyArray.h"

using namespace FoxjetDatabase;
using namespace FoxjetCommon;
using namespace FoxjetDatabase::Printers;

extern HINSTANCE hInstance;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)

DB_API CString FoxjetDatabase::GetComputerName ()
{ 
	TCHAR sz [128] = { 0 };
	DWORD dwLen = ARRAYSIZE (sz);

	::GetComputerName (sz, &dwLen);
	return sz;
}

DB_API CString FoxjetDatabase::GetIpAddress () 
{ 
	WSADATA wsaData;
	char szName [255] = { 0 };
	PHOSTENT hostinfo;
	WORD wVersionRequested = MAKEWORD (1, 1);

	if (::WSAStartup (wVersionRequested, &wsaData) == 0) {
		if (::gethostname (szName, sizeof (szName)) == 0) {
			//TRACEF (szName);

			if ((hostinfo = ::gethostbyname (szName)) != NULL) {
				if (hostinfo->h_addr_list [0]) {
					CString str = /*a2w*/ (inet_ntoa (*(struct in_addr *)hostinfo->h_addr_list [0]));
					
					//TRACEF (str);
					return str;
				}
			}
		}
	}

	return _T ("");
}

DB_API std::vector <std::wstring> FoxjetDatabase::GetMacAddresses ()
{
	std::vector <std::wstring> v;
    IP_ADAPTER_INFO *info = NULL;
    DWORD size = 0;

    ::GetAdaptersInfo (info, &size);
    info = (IP_ADAPTER_INFO *)malloc(size);

    ::GetAdaptersInfo (info, &size);

    for (IP_ADAPTER_INFO * pos = info; pos; pos = pos->Next) {
		std::vector <std::wstring> vAddr;

        for (int i = 0; i < pos->AddressLength; i++) {
			CString str;

			str.Format (_T ("%2.2x"), pos->Address [i]);
			vAddr.push_back (std::wstring (str));
		}

		v.push_back (implode (vAddr, std::wstring (_T (":"))));
    }

    free(info);
	return v;
}


////////////////////////////////////////////////////////////////////////////////


FoxjetDatabase::tagPRINTERSTRUCT::tagPRINTERSTRUCT ()
:	m_lID			(0),
	m_bMaster		(false),
	m_type			(MATRIX)
{
}

FoxjetDatabase::tagPRINTERSTRUCT::tagPRINTERSTRUCT (const tagPRINTERSTRUCT & rhs)
:	m_lID			(rhs.m_lID),
	m_strName		(rhs.m_strName),
	m_strAddress	(rhs.m_strAddress),
	m_bMaster		(rhs.m_bMaster),
	m_type			(rhs.m_type)
{
}

tagPRINTERSTRUCT & FoxjetDatabase::tagPRINTERSTRUCT::operator = (const tagPRINTERSTRUCT & rhs)
{
	if (this != &rhs) {
		m_lID			= rhs.m_lID;
		m_strName		= rhs.m_strName;
		m_strAddress	= rhs.m_strAddress;
		m_bMaster		= rhs.m_bMaster;
		m_type			= rhs.m_type;
	}

	return * this;
}

FoxjetDatabase::tagPRINTERSTRUCT::~tagPRINTERSTRUCT ()
{
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::PRINTERSTRUCT & lhs, const FoxjetDatabase::PRINTERSTRUCT & rhs)
{
	bool bResult = true;

	bResult &= (lhs.m_lID			== rhs.m_lID); 
	bResult &= (lhs.m_strName		== rhs.m_strName);
	bResult &= (lhs.m_strAddress	== rhs.m_strAddress);
	bResult &= (lhs.m_bMaster		== rhs.m_bMaster);
	bResult &= (lhs.m_type			== rhs.m_type);

	return bResult;
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::PRINTERSTRUCT & lhs, const FoxjetDatabase::PRINTERSTRUCT & rhs)
{
	return !(rhs == lhs);
}

DB_API const FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator >> (const FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::PRINTERSTRUCT & ls)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	if (!rst.GetOdbcDatabase ().HasTable (Printers::m_lpszTable)) 
		return rst;

	try {
		ls.m_lID		= (long)rst.GetFieldValue (m_lpszID);
		ls.m_strName	= (CString)rst.GetFieldValue (m_lpszName);
		ls.m_strAddress	= (CString)rst.GetFieldValue (m_lpszAddress);
		ls.m_bMaster	= (bool)rst.GetFieldValue (m_lpszMaster);
		ls.m_type		= (PRINTERTYPE)(long)rst.GetFieldValue (m_lpszType);

		if (!ls.m_strAddress.CompareNoCase (_T ("0.0.0.0"))) 
			ls.m_strName = LoadString (IDS_MASTERCONFIG);

	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator << (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::PRINTERSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	if (!rst.GetOdbcDatabase ().HasTable (Printers::m_lpszTable)) 
		return rst;

	CStringArray name, value;

	if (s.m_lID == -1) 
		rst.SetEditMode (COdbcRecordset::addnew);

	name.Add (m_lpszName);			value.Add (s.m_strName);
	name.Add (m_lpszAddress);		value.Add (s.m_strAddress);
	name.Add (m_lpszMaster);		value.Add (ToString (s.m_bMaster));
	name.Add (m_lpszType);			value.Add (ToString ((long)s.m_type));

	CString strSQL = FormatSQL (rst, m_lpszTable, name, value, m_lpszID, s.m_lID);
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

	if (s.m_lID == -1)
		s.m_lID = GetMaxID (rst.GetOdbcDatabase (), m_lpszTable, m_lpszID);
/*
	try {
		if (rst.IsAddNew ())
			rst.SetFieldValue (m_lpszID,	(long)GetNextID (rst.GetOdbcDatabase (), m_lpszTable));

		rst.SetFieldValue (m_lpszName,		ls.m_strName);
		rst.SetFieldValue (m_lpszAddress,	ls.m_strAddress);
		rst.SetFieldValue (m_lpszMaster,	ls.m_bMaster);
		rst.SetFieldValue (m_lpszType,		(long)ls.m_type);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/

	return rst;
}

DB_API bool FoxjetDatabase::GetPrinterRecord (COdbcDatabase & database, ULONG lID, PRINTERSTRUCT & cs)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	if (!database.HasTable (Printers::m_lpszTable)) 
		return false;

	try {
		COdbcRecordset rst (database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, lID);
		rst.Open (str);
		
		if (!rst.IsEOF ()) {
			rst >> cs;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::AddPrinterRecord (COdbcDatabase & database, PRINTERSTRUCT & cs)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	if (!database.HasTable (Printers::m_lpszTable)) 
		return false;

	try {
		CString str;

//		str.Format (_T ("SELECT * FROM [%s] ORDER BY [%s] DESC;"), m_lpszTable, m_lpszID);
		str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);

		{
			COdbcRecordset rst (&database);

			cs.m_lID = -1;
			rst << cs;

			/* TODO: rem
			rst.Open (str, CRecordset::dynaset);
			rst.AddNew ();
			rst << cs;
			rst.Update ();
			rst.Close ();
			*/
		}

		/*
		{
			COdbcRecordset rst (&database);

			rst.Open (str);
			ASSERT (!rst.IsEOF ());
			cs.m_lID = (long)rst.GetFieldValue (m_lpszID);
			TRACEF (ToString (cs.m_lID));
		}
		*/

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::UpdatePrinterRecord (COdbcDatabase & database, PRINTERSTRUCT & cs)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	if (!database.HasTable (Printers::m_lpszTable)) 
		return false;

	try {
		COdbcRecordset rst (&database);
		PRINTERSTRUCT s (cs);

		rst << cs;
		bResult = true;
		
		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, cs.m_lID);
		rst.Open (str, CRecordset::dynaset);

		if (!rst.IsEOF ()) {
			rst.Edit ();
			rst << cs;
			rst.Update ();
			bResult = true;
		}
		*/
		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::DeletePrinterRecord (COdbcDatabase & database, ULONG lID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	if (!database.HasTable (Printers::m_lpszTable)) 
		return false;

	try {
		CString strSQL;
		CLongArray vLines;
		COdbcRecordset rst (database);

		strSQL.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			Lines::m_lpszTable, Lines::m_lpszPrinterID, lID);

		rst.Open (COdbcRecordset::snapshot, strSQL);

		while (!rst.IsEOF ()) {
			ULONG lLineID = (long)rst.GetFieldValue (Lines::m_lpszID);
			vLines.Add (lLineID);
			rst.MoveNext ();
		}

		for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
			CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

			GetLineHeads (database, vLines [nLine], vHeads);
			
			for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) 
				VERIFY (DeleteHeadRecord (database, vHeads [nHead].m_lID));

			VERIFY (DeleteLineRecord (database, vLines [nLine]));

			strSQL.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
				Settings::m_lpszTable, Settings::m_lpszLineID, vLines [nLine]);
			TRACEF (strSQL);
			database.ExecuteSQL (strSQL);
		}

		strSQL.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, lID);
		TRACEF (strSQL);
		database.ExecuteSQL (strSQL);

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetPrinterRecords (COdbcDatabase & database, CArray <PRINTERSTRUCT, PRINTERSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	CArray <PRINTERSTRUCT, PRINTERSTRUCT &> vTmp;
	const CString strConfig = LoadString (IDS_MASTERCONFIG);
	bool bResult = false;

	if (!database.HasTable (Printers::m_lpszTable)) 
		return false;

	v.RemoveAll ();
	
	try {
		COdbcRecordset rst (database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] ORDER BY [%s] ASC;"), m_lpszTable, m_lpszName);
		rst.Open (COdbcRecordset::snapshot, str);

		while (!rst.IsEOF ()) {
			PRINTERSTRUCT p;
			rst >> p;

			if (!p.m_strName.CompareNoCase (strConfig) || !p.m_strAddress.CompareNoCase (_T ("0.0.0.0"))) 
				v.Add (p);
			else
				vTmp.Add (p);

			rst.MoveNext ();
		}

		rst.Close ();
		v.Append (vTmp);
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

