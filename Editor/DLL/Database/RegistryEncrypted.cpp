#include "stdafx.h"
#include "Registry.h"
#include "Debug.h"
#include "Parse.h"
#include "AnsiString.h"
#include "NtReg.h"
#include "tomcrypt.h"
#include "TemplExt.h"
#include <iterator>
#include <algorithm>
#include "FileExt.h"
#include "Rtl.h"
#include "UnicodeString.h"
#include "Parse.h"
#include "DbTypeDefs.h"

#undef OCSP_REQUEST
#undef X509_NAME
#undef OCSP_RESPONSE

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;



//////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
void NtFindHiddenKeys (const CUnicodeString & ustrSection);
#endif

void NtEnumerateValues (const CUnicodeString & ustrSection, std::vector <CUnicodeString> & v); // TODO: move
void NtEnumerateKeys (const CUnicodeString & ustrSection, std::vector <CUnicodeString> & v); // TODO: move
bool NtDeleteRegKey (const CUnicodeString & ustrSection); // TODO: move
void NtTraceRegKey (const CUnicodeString & ustrSection, int nDepth = 1); // TODO: move
static bool NtCanOpen (const CString & strKey);
static CString NtTranslate (const CString & strSection);
static BOOL LookupSID (CString &csSID);
static BOOL GetTextualSid(PSID pSid, LPTSTR szTextualSid, LPDWORD dwBufferLen);
static bool IsEncoded (const CString & str);
NTSTATUS NtWriteProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szData); // TODO: move
CString NtGetProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szDefault); // TODO: move

//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////

typedef struct tagRSA_KEY
{
	tagRSA_KEY () : m_pData (NULL), m_lLen (0), m_bValid (false) { }

	BYTE * m_pData;
	ULONG m_lLen;
	bool m_bValid;
} RSA_KEY;

static RSA_KEY keyPublic;
static RSA_KEY keyPrivate;
static CString gstrSID;


static CString gstrRegRootKey = _T ("\\Registry\\Machine\\");
static bool bHKEY_CURRENT_USER = false;

#ifdef _DEBUG
static const BYTE nPrivateKey [] = 
{
	0x30, 0x82, 0x04, 0xA2, 0x02, 0x01, 0x00, 0x02, 	0x82, 0x01, 0x01, 0x00, 0xD5, 0x69, 0x86, 0x36, 
	0xAA, 0x89, 0x55, 0xF4, 0x37, 0x49, 0x7C, 0xD3, 	0x9D, 0x1C, 0xDE, 0xB0, 0xE4, 0x66, 0xD4, 0xE9, 
	0x14, 0xD5, 0x7D, 0x56, 0xAE, 0x07, 0xAD, 0xB5, 	0x90, 0xDB, 0x55, 0xAB, 0x9C, 0x85, 0x62, 0x9A, 
	0xD2, 0xDF, 0x92, 0xA2, 0x69, 0xF3, 0x07, 0xB8, 	0xE8, 0x87, 0x15, 0x73, 0x2D, 0x23, 0x36, 0x8E, 
	0x8F, 0x9F, 0x1C, 0xD7, 0xC6, 0xC6, 0x7D, 0xAF, 	0xA9, 0x7D, 0x76, 0xEB, 0xD8, 0x5A, 0x2A, 0x88, 
	0xA6, 0xD2, 0xF3, 0x4B, 0x69, 0xD6, 0x97, 0xEB, 	0x8C, 0x50, 0xB8, 0x1D, 0xD1, 0xB4, 0xA6, 0x11, 
	0xBF, 0x58, 0x88, 0x72, 0x70, 0x40, 0x41, 0x7A, 	0xC4, 0x2F, 0xBB, 0x08, 0x88, 0x67, 0xEF, 0xF1, 
	0x05, 0xDC, 0x51, 0xD5, 0xDA, 0x41, 0xCB, 0x92, 	0xF1, 0xF9, 0x50, 0x3C, 0x42, 0xAB, 0xF7, 0x8A, 
	0xF1, 0xD4, 0xBF, 0x6C, 0x0C, 0xCB, 0x36, 0xFB, 	0x54, 0x86, 0x60, 0xC9, 0x41, 0xE7, 0xB1, 0x37, 
	0x67, 0xFA, 0xDD, 0x2F, 0x6A, 0x2B, 0x39, 0x12, 	0x2A, 0x6B, 0x8C, 0x2D, 0xDE, 0xEE, 0x9B, 0x06, 
	0x60, 0x5B, 0xF7, 0x33, 0x60, 0x22, 0x4B, 0x82, 	0x35, 0x8F, 0x91, 0xBE, 0xDF, 0xB8, 0xBA, 0x94, 
	0xF9, 0xB7, 0x6A, 0xE7, 0x16, 0x9B, 0xAD, 0x34, 	0xD0, 0x5F, 0x98, 0x7A, 0x75, 0xAB, 0x53, 0xD9, 
	0x0F, 0x38, 0x7D, 0xA9, 0x94, 0x74, 0x2D, 0xDD, 	0xB4, 0x37, 0x45, 0x5D, 0x6C, 0x04, 0x6E, 0x60, 
	0xAB, 0x9E, 0x0E, 0x88, 0x33, 0xB5, 0xBE, 0xE3, 	0xAB, 0x39, 0xF3, 0x9D, 0x7B, 0xAD, 0x96, 0x35, 
	0xA8, 0x78, 0x8B, 0x37, 0x62, 0x51, 0x48, 0x11, 	0xBB, 0x06, 0xB8, 0xA2, 0xE0, 0x94, 0xCC, 0xEC, 
	0x95, 0x16, 0x15, 0x48, 0x66, 0x65, 0xCB, 0x31, 	0x48, 0x44, 0xFF, 0x85, 0x6D, 0x00, 0x21, 0x23, 
	0x79, 0x3E, 0x9D, 0x75, 0xDD, 0x5E, 0x85, 0xDE, 	0x0A, 0xDE, 0x8C, 0x03, 0x02, 0x03, 0x01, 0x00, 
	0x01, 0x02, 0x82, 0x01, 0x00, 0x03, 0x72, 0xD0, 	0x13, 0x64, 0x10, 0x12, 0x0E, 0x7F, 0x32, 0x35, 
	0xCD, 0x29, 0x95, 0xC4, 0xD7, 0x70, 0xE9, 0x64, 	0x79, 0x5B, 0xB7, 0xD3, 0x6D, 0xAA, 0x24, 0x3F, 
	0x9F, 0x56, 0x76, 0xE4, 0xDC, 0x80, 0x0B, 0xFB, 	0xBA, 0xEA, 0xBB, 0x68, 0x20, 0x2D, 0xBC, 0x1A, 
	0x9C, 0xD7, 0x4D, 0x0F, 0x80, 0xAB, 0xF3, 0x0C, 	0xB7, 0xA3, 0x04, 0x7C, 0x2F, 0xD4, 0x6B, 0x72, 
	0xAE, 0x7A, 0xFB, 0xA7, 0x07, 0x4A, 0x98, 0xB2, 	0x65, 0x40, 0xE4, 0xD9, 0xBF, 0x97, 0x37, 0x3B, 
	0x7F, 0x37, 0xE6, 0x3A, 0x03, 0x69, 0x57, 0x14, 	0xF3, 0xE6, 0x76, 0xA4, 0xC5, 0xCC, 0xA0, 0x8D, 
	0x2E, 0x51, 0xAB, 0x3F, 0xC7, 0x74, 0x84, 0xD1, 	0x70, 0xC0, 0x9A, 0x7D, 0xCC, 0x09, 0xD9, 0xD4, 
	0x53, 0x4C, 0x8B, 0xAD, 0xCB, 0x3B, 0x7C, 0x91, 	0xE4, 0x71, 0xE1, 0xF1, 0x31, 0xEA, 0xC7, 0x01, 
	0xDD, 0x6F, 0xD1, 0x38, 0x11, 0x20, 0xEB, 0xC2, 	0x40, 0x6D, 0xBE, 0xA2, 0x3F, 0xF6, 0x95, 0x3D, 
	0x5F, 0x22, 0x78, 0xE2, 0xDF, 0xE1, 0xD5, 0xAB, 	0xD7, 0xD5, 0xBD, 0x21, 0x66, 0x57, 0x04, 0x4F, 
	0xC2, 0x1F, 0x92, 0xFC, 0x41, 0x70, 0xF9, 0x6E, 	0xAC, 0xEF, 0x98, 0x5D, 0x63, 0xE9, 0x83, 0x12, 
	0x9B, 0x26, 0xEC, 0x07, 0x78, 0xB3, 0x63, 0xEF, 	0x20, 0x54, 0x88, 0x4A, 0xBE, 0x49, 0x31, 0x56, 
	0x56, 0xE6, 0x45, 0xBC, 0x84, 0x2E, 0x59, 0x7B, 	0x1F, 0xA5, 0x0B, 0xCE, 0x2E, 0xAB, 0x05, 0xBD, 
	0xA0, 0x62, 0x56, 0x92, 0x09, 0xA8, 0x7D, 0xF6, 	0x5C, 0x86, 0x32, 0x07, 0xA0, 0x0A, 0x6D, 0x83, 
	0x97, 0xED, 0x26, 0x7A, 0x7A, 0x65, 0xE0, 0x9C, 	0xCD, 0x54, 0x81, 0xE9, 0xAF, 0x93, 0xDC, 0xD0, 
	0x79, 0xA7, 0x69, 0x21, 0xB3, 0xFD, 0x05, 0x7D, 	0xCE, 0xE3, 0x63, 0xAA, 0x05, 0x1D, 0x22, 0xE2, 
	0x98, 0x87, 0x04, 0xDF, 0xC1, 0x02, 0x81, 0x81, 	0x00, 0xF1, 0x14, 0xD1, 0xE2, 0x95, 0x10, 0x64, 
	0x42, 0xC8, 0x86, 0xDE, 0x39, 0xEB, 0x2B, 0x5B, 	0x3B, 0x2A, 0x9A, 0xF8, 0x20, 0x31, 0xF6, 0xEA, 
	0xA7, 0x75, 0x96, 0x6C, 0x92, 0x5F, 0x81, 0xEB, 	0x5C, 0xFE, 0xFE, 0xB1, 0x4E, 0xFA, 0xD8, 0x51, 
	0x13, 0x37, 0xEB, 0xEC, 0xAB, 0x99, 0x3C, 0x14, 	0xAD, 0x40, 0x42, 0x96, 0x02, 0x59, 0x17, 0xB2, 
	0x96, 0xAC, 0x9D, 0xF5, 0x68, 0xFC, 0x92, 0x32, 	0xFA, 0x75, 0x3C, 0xE9, 0xEF, 0x16, 0xFA, 0x86, 
	0x8D, 0x33, 0x58, 0xB8, 0xF8, 0xF6, 0xA0, 0x54, 	0xFA, 0x33, 0x01, 0x8D, 0x20, 0x82, 0x86, 0x97, 
	0x57, 0x0F, 0x7A, 0x7A, 0x06, 0xA1, 0xEC, 0x22, 	0x88, 0x79, 0x54, 0x9F, 0x4C, 0x8B, 0x9A, 0xDD, 
	0x95, 0x07, 0x0E, 0x10, 0xC4, 0xFF, 0x55, 0x01, 	0x43, 0x0F, 0x64, 0x2D, 0x5B, 0x30, 0x78, 0xC7, 
	0x92, 0xE1, 0x22, 0x13, 0x1B, 0x08, 0xE7, 0x74, 	0xA1, 0x02, 0x81, 0x81, 0x00, 0xE2, 0x9E, 0x5F, 
	0xA6, 0x0C, 0x6F, 0x30, 0x04, 0x5C, 0xB2, 0xA0, 	0xDB, 0xAB, 0x4D, 0x67, 0x27, 0x3B, 0x52, 0xFD, 
	0x2F, 0xC5, 0x3F, 0x4D, 0xF1, 0x96, 0x43, 0xA5, 	0x4C, 0x57, 0x5B, 0x49, 0xEF, 0x7A, 0xFC, 0x5B, 
	0x60, 0x09, 0xAD, 0x4E, 0x75, 0x9D, 0xA9, 0xAE, 	0x1C, 0x10, 0x6A, 0xBB, 0x77, 0x6C, 0xDA, 0xF5, 
	0x5F, 0xDB, 0xEE, 0x34, 0x17, 0x07, 0xD6, 0x55, 	0x68, 0x3D, 0xA7, 0xC8, 0x31, 0x70, 0xB2, 0x5F, 
	0x2F, 0x8B, 0x95, 0xF0, 0x4B, 0xEF, 0x19, 0xE7, 	0xAB, 0x60, 0xF8, 0x4A, 0x37, 0x57, 0x44, 0x9B, 
	0x75, 0x39, 0xA2, 0x38, 0xFF, 0x02, 0xEE, 0x7D, 	0x70, 0x34, 0xCE, 0x49, 0x80, 0x92, 0x84, 0xE8, 
	0xBE, 0x6B, 0xCF, 0x4B, 0xA5, 0x75, 0xF8, 0xA4, 	0xCB, 0x8E, 0x9C, 0x15, 0x31, 0xFE, 0x7D, 0x88, 
	0xD7, 0xB5, 0x2D, 0xEC, 0xE7, 0xC8, 0x43, 0x48, 	0x1D, 0xC0, 0x99, 0x5A, 0x23, 0x02, 0x81, 0x80, 
	0x6D, 0x7C, 0x1A, 0x11, 0x7F, 0xC6, 0xF0, 0xF1, 	0x8F, 0x66, 0x08, 0x98, 0x5F, 0x1F, 0xD2, 0x8F, 
	0xDD, 0x3A, 0x81, 0xC3, 0x05, 0x4D, 0xF0, 0xA5, 	0x5A, 0x76, 0x95, 0x1A, 0x28, 0x7B, 0xB5, 0x2B, 
	0x07, 0xB7, 0x1D, 0x49, 0x14, 0x0C, 0x97, 0x4D, 	0x56, 0x11, 0x33, 0xEB, 0x7E, 0x6C, 0xD5, 0xAB, 
	0x14, 0xCA, 0xE8, 0x9B, 0x6A, 0x60, 0xA3, 0xBF, 	0x5F, 0xE7, 0x60, 0xA3, 0x6C, 0x48, 0xED, 0x13, 
	0xE7, 0x98, 0x61, 0x59, 0x2E, 0xA3, 0x59, 0xE5, 	0xD8, 0x52, 0xCB, 0xB6, 0x6D, 0x32, 0xC5, 0x44, 
	0xD7, 0x53, 0x36, 0x07, 0x74, 0x9F, 0xB1, 0x2D, 	0x5D, 0x9F, 0x2E, 0xB9, 0xC2, 0x61, 0x9E, 0x11, 
	0x42, 0xA0, 0xD6, 0xB3, 0x89, 0x97, 0x2C, 0x58, 	0x58, 0xDA, 0xDF, 0x6C, 0xC7, 0xD4, 0x8F, 0xFD, 
	0x27, 0x89, 0x44, 0x3B, 0xEC, 0x98, 0x7D, 0x72, 	0x4C, 0x02, 0x10, 0x91, 0x2F, 0x8B, 0x05, 0xE1, 
	0x02, 0x81, 0x80, 0x62, 0xB3, 0x97, 0xF5, 0x3A, 	0x75, 0x32, 0xF4, 0xB3, 0x6B, 0x19, 0xA4, 0x93, 
	0x77, 0xA2, 0xF5, 0xF3, 0x60, 0xB1, 0xE5, 0x9C, 	0x83, 0x75, 0xEF, 0x43, 0x03, 0x33, 0x7B, 0x8C, 
	0x90, 0xB6, 0x23, 0x17, 0xED, 0xD1, 0xDE, 0x82, 	0x58, 0x79, 0x53, 0xC0, 0xD7, 0xA6, 0x7D, 0x92, 
	0xA8, 0xE9, 0xFD, 0x99, 0xEB, 0xC3, 0xF3, 0x7C, 	0x56, 0xC3, 0x59, 0x44, 0x90, 0x95, 0xA2, 0x51, 
	0x38, 0x42, 0x79, 0x46, 0x1C, 0x6A, 0x58, 0xCA, 	0xF3, 0x03, 0x36, 0x0F, 0x6D, 0xB8, 0xF9, 0x6B, 
	0xC2, 0xB8, 0x90, 0x61, 0xF1, 0xFF, 0xC3, 0x8E, 	0xD6, 0x98, 0xFC, 0x78, 0xCA, 0x66, 0xD7, 0xF8, 
	0x27, 0xE0, 0xDB, 0x9E, 0xF5, 0x44, 0x69, 0xAB, 	0x8B, 0x1D, 0xE7, 0x72, 0xFA, 0x58, 0xCC, 0x4F, 
	0xD0, 0xAB, 0x6A, 0x9A, 0xED, 0x12, 0x2C, 0xE1, 	0x3C, 0x9F, 0x72, 0x54, 0x46, 0xF9, 0xA1, 0xFB, 
	0xC0, 0xCF, 0x01, 0x02, 0x81, 0x80, 0x2B, 0xAE, 	0x9D, 0x3A, 0xF4, 0x4E, 0xE2, 0x2A, 0x9E, 0x93, 
	0xA3, 0x89, 0x83, 0x36, 0xD8, 0x1D, 0xB2, 0x48, 	0xE7, 0x00, 0x9F, 0x42, 0xB0, 0x0C, 0xE3, 0x9D, 
	0x9F, 0x80, 0x99, 0xC2, 0xCF, 0x22, 0xBC, 0xB9, 	0xAB, 0xB4, 0x8A, 0xDB, 0x1E, 0x38, 0x7D, 0xAD, 
	0xC5, 0xE2, 0x15, 0xD6, 0x84, 0x12, 0xAB, 0x75, 	0xC8, 0x66, 0xC7, 0x80, 0xB8, 0x30, 0xFA, 0x9E, 
	0x2F, 0x62, 0xE8, 0xA3, 0x9C, 0xF0, 0x63, 0xC2, 	0x6F, 0xE9, 0x05, 0xF5, 0xE5, 0xA8, 0xC6, 0x65, 
	0x12, 0x5A, 0x37, 0x6D, 0x14, 0x97, 0xF2, 0xA9, 	0xEB, 0x64, 0x5E, 0x83, 0x34, 0x5F, 0xA6, 0xB6, 
	0xAD, 0x3E, 0x91, 0xEC, 0x87, 0xF4, 0xDB, 0x4E, 	0x62, 0xD6, 0xB9, 0x4C, 0x1A, 0x83, 0x90, 0xA4, 
	0x89, 0x0A, 0x18, 0xF0, 0x28, 0xD9, 0x0B, 0xE7, 	0x5C, 0x17, 0x90, 0x69, 0x64, 0xF8, 0xC1, 0x8E, 
	0x51, 0x40, 0x90, 0x14, 0x88, 0xCB, 
};

static void Save (const std::vector <std::wstring> & v, const CString & strFile)
{
	if (FILE * fp = _tfopen (strFile, _T ("w"))) {
		CString str;

		for (int i = 0; i < v.size (); i++) {
			BYTE n = _tcstoul (v [i].c_str (), NULL, 2);
			
			str += ToHexString (n) + _T (", ");

			if (((i + 1) % 16) == 0)		str += _T ("\n"); 
			else if (((i + 1) % 8) == 0) 	str += _T ("\t"); 
		}

		TRACEF (strFile);
		fwrite (w2a (str), str.GetLength (), 1, fp);
		fclose (fp);
	}
}

static void Save (LPBYTE p, DWORD dwLen, const CString & strFile)
{
	if (FILE * fp = _tfopen (strFile, _T ("w"))) {
		CString str;

		for (int i = 0; i < dwLen; i++) {
			str += ToHexString (p [i]) + _T (", ");

			if (((i + 1) % 16) == 0)		str += _T ("\n"); 
			else if (((i + 1) % 8) == 0) 	str += _T ("\t"); 
		}

		TRACEF (strFile);
		fwrite (w2a (str), str.GetLength (), 1, fp);
		fclose (fp);
	}
}

static void GenerateRsaKey (const CString & strPrivateFile, const CString & strPublicFile)
{
	rsa_key key = { 0 };
	const int prng_idx = register_prng (&sprng_desc);
	const int bitsize = 2048;

	ltc_mp = ltm_desc;

	int err = rsa_make_key(NULL, prng_idx, bitsize/8, 65537, &key);
	
	if (err == CRYPT_OK) {
		unsigned char out[bitsize * 5 / 8]; // guesstimate
		unsigned long outlen = sizeof(out);

		err = rsa_export (out, &outlen, PK_PRIVATE, &key);

		if (err == CRYPT_OK) 
			Save (out, outlen, strPrivateFile); 

		outlen = sizeof(out);
		err = rsa_export(out, &outlen, PK_PUBLIC, &key);

		if (err == CRYPT_OK) 
			Save (out, outlen, strPublicFile); 
	}

	rsa_free(&key);
}
#endif

static const BYTE nPrivateKeyScrambled [] = 
{
	0xD3, 0x11, 0x28, 0x09, 0x02, 0x8A, 0x71, 0x83, 	0x1F, 0x26, 0x96, 0x09, 0xE8, 0x3A, 0xE7, 0xD0, 
	0x9B, 0x14, 0x0F, 0x18, 0x50, 0x91, 0x25, 0x09, 	0xC1, 0x58, 0x32, 0x9D, 0x6B, 0x46, 0x72, 0xDB, 
	0x2F, 0xE1, 0x37, 0x89, 0x7C, 0xB5, 0x6D, 0x65, 	0xFA, 0x2C, 0xC1, 0x7A, 0x26, 0xD7, 0x95, 0x4F, 
	0xE9, 0x28, 0xB6, 0xEC, 0x5A, 0x48, 0xA6, 0x63, 	0x15, 0xA7, 0xAF, 0xA0, 0x97, 0xF6, 0x43, 0xC6, 
	0x0F, 0x39, 0xC5, 0x17, 0x46, 0xF4, 0x79, 0x5F, 	0x0C, 0x1D, 0x01, 0xE3, 0x66, 0x13, 0xAE, 0xD5, 
	0x48, 0x21, 0x6B, 0xA8, 0x47, 0xA3, 0xB5, 0xBE, 	0x1C, 0x78, 0xDB, 0x51, 0x2D, 0xD5, 0x9D, 0x3D, 
	0x44, 0xF3, 0x43, 0x99, 0x01, 0xF9, 0xB9, 0xC7, 	0x30, 0x0D, 0x42, 0xF9, 0x00, 0xE7, 0x12, 0x4D, 
	0xB8, 0x1B, 0x6C, 0xC1, 0x91, 0xC5, 0xC9, 0x79, 	0x54, 0x47, 0x72, 0x2F, 0x5C, 0xB9, 0x75, 0xD4, 
	0x01, 0x81, 0x40, 0x80, 0xF3, 0x03, 0xDF, 0x85, 	0x9F, 0x62, 0x2A, 0x4E, 0xF9, 0x3C, 0x87, 0x34, 
	0x48, 0xB7, 0x59, 0x56, 0xD5, 0x0B, 0xF2, 0x33, 	0x1A, 0x5F, 0x4E, 0xE7, 0xB8, 0xD1, 0xD5, 0x96, 
	0x22, 0xAF, 0x79, 0xDB, 0x07, 0xE4, 0x1F, 0xEB, 	0x66, 0x53, 0x1E, 0x3F, 0x19, 0x6B, 0x71, 0xC3, 
	0xFF, 0x8F, 0x86, 0x09, 0x1D, 0x43, 0xD6, 0x9F, 	0x1D, 0xB6, 0xF0, 0x6C, 0xC0, 0xCF, 0xCA, 0x1A, 
	0x56, 0x38, 0x62, 0x9E, 0x42, 0x1C, 0x8A, 0x45, 	0xA9, 0x09, 0x22, 0x9A, 0xC3, 0x6A, 0x3E, 0xCF, 
	0xC3, 0xD7, 0x99, 0xBF, 0x97, 0x15, 0x49, 0xBE, 	0x65, 0xEB, 0x03, 0xCA, 0x9E, 0x1A, 0x41, 0x7B, 
	0x8B, 0xB7, 0xE8, 0xC4, 0x6D, 0x09, 0x31, 0xDE, 	0xCC, 0xC0, 0xC2, 0xF7, 0xAE, 0xC1, 0x39, 0xA7, 
	0x8D, 0x06, 0xCF, 0xAF, 0x45, 0xEE, 0xC9, 0x25, 	0x98, 0xD6, 0xCD, 0x2F, 0x4C, 0xAE, 0x5C, 0xAF, 
	0xE9, 0xCD, 0x46, 0x01, 0x81, 0x40, 0x87, 0xA0, 	0xD1, 0xF4, 0x89, 0x08, 0x40, 0x32, 0x4E, 0xBE, 
	0x19, 0x37, 0xDC, 0x22, 0x91, 0xE4, 0xBF, 0xF1, 	0x2B, 0xE3, 0x36, 0xFB, 0x5B, 0x1A, 0x1A, 0x34, 
	0xE9, 0x91, 0xCD, 0x6B, 0x05, 0x42, 0x88, 0x79, 	0x86, 0x43, 0x9D, 0x74, 0xF9, 0xBA, 0xB4, 0x8D, 
	0xF9, 0x2E, 0xE0, 0x6C, 0xCA, 0xEB, 0x22, 0xA3, 	0x4C, 0xB6, 0x6D, 0xD3, 0x4A, 0x1B, 0xA7, 0x9A, 
	0xC5, 0x74, 0x9A, 0x86, 0x19, 0xE7, 0xC8, 0xB7, 	0x12, 0x36, 0xC5, 0x06, 0xE7, 0xFA, 0xFD, 0xC5, 
	0x06, 0x56, 0xD9, 0x17, 0x53, 0x28, 0xD5, 0xAB, 	0x36, 0x7E, 0xD7, 0xCC, 0x88, 0x6A, 0xB2, 0xE9, 
	0x30, 0x28, 0x92, 0xB8, 0xED, 0xE0, 0xD4, 0xAD, 	0xDE, 0x14, 0x58, 0xA9, 0x6E, 0x5A, 0xA5, 0x0F, 
	0xB2, 0xA0, 0xC3, 0x81, 0x5C, 0xBB, 0xF1, 0x4B, 	0xF8, 0xFA, 0x19, 0x10, 0x66, 0x8F, 0x8F, 0x0F, 
	0x63, 0xFE, 0x88, 0x58, 0x3E, 0xB6, 0x01, 0x81, 	0x40, 0xC4, 0x5A, 0x99, 0x03, 0xB8, 0x12, 0xC2, 
	0x13, 0xE7, 0x37, 0xB4, 0xAD, 0xEB, 0x11, 0xBE, 	0x7F, 0x8C, 0xA8, 0x39, 0x71, 0xD3, 0x25, 0x1F, 
	0xAE, 0xA5, 0xD2, 0xF3, 0xD6, 0x7D, 0x17, 0x21, 	0x49, 0x01, 0x92, 0x73, 0x2C, 0x0E, 0xBE, 0x77, 
	0x40, 0xFF, 0x1C, 0x45, 0x9C, 0xAE, 0xD9, 0x22, 	0xEA, 0xEC, 0x52, 0x1F, 0x06, 0xD5, 0xE7, 0x98, 
	0xF7, 0xD2, 0x0F, 0xA9, 0xD1, 0xF4, 0xFA, 0x4D, 	0x0E, 0x8C, 0x13, 0xE5, 0xBC, 0x16, 0xAA, 0x6B, 
	0xE0, 0xE8, 0x2C, 0x77, 0xDB, 0xFA, 0xAF, 0x5B, 	0x36, 0xEE, 0xDD, 0x56, 0x08, 0x38, 0x75, 0x95, 
	0xB9, 0xAE, 0x72, 0xB5, 0x90, 0x06, 0xDA, 0x3F, 	0x5E, 0xF7, 0x92, 0xDA, 0xEA, 0x32, 0xA5, 0xC2, 
	0x69, 0x8F, 0xB2, 0xFC, 0xA3, 0xF4, 0xBF, 0x4A, 	0xDC, 0xE4, 0xE6, 0xB2, 0xD5, 0xDB, 0x05, 0x4D, 
	0x3A, 0x20, 0x0C, 0xF6, 0x30, 0x65, 0xFA, 0x79, 	0x47, 0x00, 0x81, 0x81, 0x40, 0x85, 0x2E, 0xE7, 
	0x10, 0xD8, 0xC8, 0x44, 0x87, 0x49, 0xE3, 0x1E, 	0x0C, 0xDA, 0xB4, 0x26, 0xF0, 0xC2, 0x80, 0xAA, 
	0xFF, 0x23, 0x08, 0x70, 0xE0, 0xA9, 0xBB, 0x59, 	0xD1, 0x32, 0xF9, 0x2A, 0x9E, 0x11, 0x44, 0x37, 
	0x85, 0x60, 0x5E, 0x5E, 0xF0, 0xEA, 0xE9, 0x61, 	0x41, 0x04, 0xB1, 0x80, 0x33, 0x5F, 0x2A, 0x05, 
	0x6F, 0x1F, 0x1D, 0x1A, 0xCC, 0xB1, 0x61, 0x5F, 	0x68, 0xF7, 0x97, 0x3C, 0xAE, 0x5F, 0x4C, 0x49, 
	0x3F, 0x16, 0xAF, 0xB9, 0x35, 0x69, 0x4D, 0xE8, 	0x9A, 0x40, 0x69, 0x42, 0x02, 0xB5, 0x28, 0x3C, 
	0x99, 0xD5, 0x37, 0xD7, 0xEC, 0xC8, 0x8A, 0x1B, 	0x5F, 0x72, 0x8D, 0x7F, 0x7F, 0x3A, 0xD7, 0x81, 
	0xFA, 0x49, 0x36, 0x69, 0xAE, 0xE5, 0x57, 0x6F, 	0x8C, 0x04, 0x1F, 0x59, 0x54, 0xDC, 0xDA, 0xD4, 
	0xD7, 0x9C, 0x7B, 0x61, 0x13, 0x42, 0x26, 0x08, 	0xA9, 0x47, 0x8B, 0x28, 0x8F, 0x00, 0x81, 0x81, 
	0x40, 0x83, 0xFB, 0x20, 0xE1, 0x19, 0x47, 0x44, 	0xB8, 0xA0, 0x55, 0xC6, 0xC7, 0x73, 0xBE, 0xA0, 
	0xBF, 0xCD, 0x84, 0x96, 0xE5, 0x9E, 0x0B, 0x3B, 	0xC9, 0xF5, 0x97, 0x81, 0x2A, 0xB3, 0x39, 0x07, 
	0xA6, 0x5E, 0x5E, 0x64, 0xB7, 0xE9, 0xC1, 0xB6, 	0x50, 0x05, 0xE0, 0x4C, 0x61, 0x3A, 0x6F, 0xBE, 
	0x15, 0x90, 0x49, 0x6A, 0x46, 0x05, 0xBD, 0xA0, 	0xD5, 0x74, 0x73, 0xD0, 0xA5, 0xF8, 0xDE, 0x9A, 
	0x74, 0x21, 0x3D, 0xA2, 0x67, 0x6A, 0x6A, 0x8C, 	0x92, 0x7D, 0x52, 0x11, 0x2A, 0x04, 0xF7, 0xC6, 
	0xCD, 0x1E, 0xE0, 0x37, 0x64, 0xD9, 0x48, 0xC1, 	0x97, 0xC6, 0xBA, 0x19, 0xF7, 0x35, 0x76, 0x9F, 
	0x0E, 0x82, 0x3F, 0x49, 0xF8, 0x43, 0xF2, 0x20, 	0xEA, 0x66, 0x84, 0xBD, 0xAB, 0xEB, 0xD5, 0xAB, 
	0x87, 0xFB, 0x47, 0x1E, 0x44, 0xFA, 0xBC, 0xA9, 	0x6F, 0xFC, 0x45, 0x7D, 0xB6, 0x02, 0x43, 0xD7, 
	0x04, 0x88, 0x1C, 0x8B, 0xF6, 0xBB, 0x80, 0xE3, 	0x57, 0x8C, 0x8F, 0x87, 0x8E, 0x27, 0x89, 0x3E, 
	0xDC, 0xD3, 0xB5, 0xD1, 0x32, 0xCA, 0x2B, 0x9B, 	0x90, 0x33, 0xBE, 0x59, 0x03, 0x0E, 0x8B, 0x21, 
	0x2E, 0xE3, 0xFC, 0xD5, 0x8A, 0x74, 0xB1, 0x05, 	0x33, 0xA3, 0x25, 0x6E, 0x67, 0xCF, 0x28, 0xEA, 
	0x96, 0xC0, 0x5C, 0x67, 0xEC, 0xFE, 0xDC, 0xEC, 	0xE9, 0xFD, 0x9B, 0x27, 0x02, 0xA6, 0x4D, 0x19, 
	0x52, 0xE0, 0xE5, 0xDF, 0x5E, 0x75, 0x4E, 0xD6, 	0x2B, 0xF4, 0x3E, 0x20, 0xC5, 0xED, 0x30, 0xCF, 
	0xD5, 0x01, 0xF0, 0xB2, 0xEB, 0x39, 0x58, 0x3D, 	0xB4, 0x04, 0x16, 0xDD, 0x57, 0x5D, 0xDF, 0xD0, 
	0x01, 0x3B, 0x27, 0x6E, 0x6A, 0xF9, 0xFC, 0x24, 	0x55, 0xB6, 0xCB, 0xED, 0xDA, 0x9E, 0x26, 0x97, 
	0x0E, 0xEB, 0x23, 0xA9, 0x94, 0xB3, 0xAC, 0x4C, 	0xFE, 0x70, 0x48, 0x08, 0x26, 0xC8, 0x0B, 0x4E, 
	0xC0, 0x00, 0x80, 0x41, 0x40, 0x80, 0x00, 0x80, 	0xC0, 0x40, 0xC0, 0x31, 0x7B, 0x50, 0x7B, 0xA1, 
	0x7A, 0xBB, 0xAE, 0xB9, 0x7C, 0x9E, 0xC4, 0x84, 	0x00, 0xB6, 0xA1, 0xFF, 0x22, 0x12, 0x8C, 0xD3, 
	0xA6, 0x66, 0x12, 0xA8, 0x68, 0xA9, 0x37, 0x33, 	0x29, 0x07, 0xA2, 0x1D, 0x60, 0xDD, 0x88, 0x12, 
	0x8A, 0x46, 0xEC, 0xD1, 0x1E, 0x15, 0xAC, 0x69, 	0xB5, 0xDE, 0xB9, 0xCF, 0x9C, 0xD5, 0xC7, 0x7D, 
	0xAD, 0xCC, 0x11, 0x70, 0x79, 0xD5, 0x06, 0x76, 	0x20, 0x36, 0xBA, 0xA2, 0xEC, 0x2D, 0xBB, 0xB4, 
	0x2E, 0x29, 0x95, 0xBE, 0x1C, 0xF0, 0x9B, 0xCA, 	0xD5, 0xAE, 0x5E, 0x19, 0xFA, 0x0B, 0x2C, 0xB5, 
	0xD9, 0x68, 0xE7, 0x56, 0xED, 0x9F, 0x29, 0x5D, 	0x1D, 0xFB, 0x7D, 0x89, 0xF1, 0xAC, 0x41, 0xD2, 
	0x44, 0x06, 0xCC, 0xEF, 0xDA, 0x06, 0x60, 0xD9, 	0x77, 0x7B, 0xB4, 0x31, 0xD6, 0x54, 0x48, 0x9C, 
	0xD4, 0x56, 0xF4, 0xBB, 0x5F, 0xE6, 0xEC, 0x8D, 	0xE7, 0x82, 0x93, 0x06, 0x61, 0x2A, 0xDF, 0x6C, 
	0xD3, 0x30, 0x36, 0xFD, 0x2B, 0x8F, 0x51, 0xEF, 	0xD5, 0x42, 0x3C, 0x0A, 0x9F, 0x8F, 0x49, 0xD3, 
	0x82, 0x5B, 0xAB, 0x8A, 0x3B, 0xA0, 0x8F, 0xF7, 	0xE6, 0x11, 0x10, 0xDD, 0xF4, 0x23, 0x5E, 0x82, 
	0x02, 0x0E, 0x4E, 0x11, 0x1A, 0xFD, 0x88, 0x65, 	0x2D, 0x8B, 0xB8, 0x1D, 0x0A, 0x31, 0xD7, 0xE9, 
	0x6B, 0x96, 0xD2, 0xCF, 0x4B, 0x65, 0x11, 0x54, 	0x5A, 0x1B, 0xD7, 0x6E, 0xBE, 0x95, 0xF5, 0xBE, 
	0x63, 0x63, 0xEB, 0x38, 0xF9, 0xF1, 0x71, 0x6C, 	0xC4, 0xB4, 0xCE, 0xA8, 0xE1, 0x17, 0x1D, 0xE0, 
	0xCF, 0x96, 0x45, 0x49, 0xFB, 0x4B, 0x59, 0x46, 	0xA1, 0x9C, 0xD5, 0xAA, 0xDB, 0x09, 0xAD, 0xB5, 
	0xE0, 0x75, 0x6A, 0xBE, 0xAB, 0x28, 0x97, 0x2B, 	0x66, 0x27, 0x0D, 0x7B, 0x38, 0xB9, 0xCB, 0x3E, 
	0x92, 0xEC, 0x2F, 0xAA, 0x91, 0x55, 0x6C, 0x61, 	0x96, 0xAB, 0x00, 0x80, 0x80, 0x41, 0x40, 0x00, 
	0x80, 0x40, 0x45, 0x20, 0x41, 0x0C, 
};

static bool IsEncoded (const CString & str)
{
	if (str.GetLength () > 3) 
		return str.Right (2) == _T ("==") ? true : false;

	return false;
}


DB_API void FoxjetDatabase::Encrypted::RSA::SetPublicKey (const LPBYTE p, DWORD dwLen)
{
	if (::keyPublic.m_pData) {
		delete [] ::keyPublic.m_pData;
		::keyPublic.m_pData = NULL;
	}

	::keyPublic.m_pData = new BYTE [dwLen];
	::keyPublic.m_lLen = dwLen;
	memcpy (::keyPublic.m_pData, p, dwLen);
}

DB_API bool FoxjetDatabase::Encrypted::RSA::IsEnabled ()
{
	return ::keyPrivate.m_bValid;
}

DB_API void FoxjetDatabase::Encrypted::RSA::SetPrivateKey (const LPBYTE p, DWORD dwLen)
{
	CString strPlain;
	const CString strEncrypted = 
		_T ("A3EEpDmZ0FkvwzMGTu9KENhOCHXgVVtHRpjaS9GiUvcmzOhl/V9ltqkBCpYbp") 
		_T ("C56T/J4nXajK9uMLnGVhacLRuYPlwKi3mGrWs7Xehk2Eg9T2Gsj/w0BUJOWZE") 
		_T ("z2YPQuH69TesQw43AX2MpOBIbDln/zMzgiE744MhMe/OIAFcr6G178giXuP9l") 
		_T ("VCoo+aTB/7U6/W0Ye+TxsZ0vsN6q9AMYFLcudcCVm2xMupFrPPPbkj9Nc3b4W") 
		_T ("d1iDubdOEQxH3TRc2EYVC+So4S7+eMb643UwYZpl4tJybag2jSQnhkiMcOSyV") 
		_T ("NjAmr0xIK5qrQMMSMBCppCoFu2HRrOIJcBsuQ==");

	if (::keyPrivate.m_pData) {
		delete [] ::keyPrivate.m_pData;
		::keyPrivate.m_pData = NULL;
	}

	::keyPrivate.m_pData = new BYTE [dwLen];
	::keyPrivate.m_lLen = dwLen;
	memcpy (::keyPrivate.m_pData, p, dwLen);
	::keyPrivate.m_bValid = true;

	for (TCHAR c = ' '; c < (TCHAR)(' ' + 45); c++)
		strPlain += c;

	strPlain = FoxjetDatabase::Encrypted::InkCodes::Scramble (strPlain);

	const CString strDecrypted = RSA::Decrypt (strEncrypted);

	::keyPrivate.m_bValid = strDecrypted == strPlain;

	if (::keyPrivate.m_bValid) {
		VERIFY (RTL::ghlib = ::GetModuleHandle (_T ("ntdll.dll")));


		{
			using namespace FoxjetDatabase::Encrypted::InkCodes;
			
			INK_CODE & range = * (INK_CODE *)(LPVOID)&CInkCode::m_range;
			range = INK_CODE ();

			//ENABLE_PASSWORD & enable = * (ENABLE_PASSWORD *)(LPVOID)&CEnablePassword::m_range;
			//enable = ENABLE_PASSWORD ();

			//DISABLE_PASSWORD & disable = * (DISABLE_PASSWORD *)(LPVOID)&CDisablePassword::m_range;
			//disable = DISABLE_PASSWORD ();
		}

		if (!RTL::ghlib)
			return;

		VERIFY (RTL::RtlInitString					= (LPRTLINITSTRING)GetProcAddress					(RTL::ghlib, "RtlInitString"));
		VERIFY (RTL::RtlInitAnsiString				= (LPRTLINITANSISTRING)GetProcAddress				(RTL::ghlib, "RtlInitAnsiString"));
		VERIFY (RTL::RtlInitUnicodeString			= (LPRTLINITUNICODESTRING)GetProcAddress			(RTL::ghlib, "RtlInitUnicodeString"));
		VERIFY (RTL::RtlAnsiStringToUnicodeString	= (LPRTLANSISTRINGTOUNICODESTRING)GetProcAddress	(RTL::ghlib, "RtlAnsiStringToUnicodeString"));
		VERIFY (RTL::RtlUnicodeStringToAnsiString	= (LPRTLUNICODESTRINGTOANSISTRING)GetProcAddress	(RTL::ghlib, "RtlUnicodeStringToAnsiString"));
		VERIFY (RTL::RtlFreeAnsiString				= (LPRTLFREEANSISTRING)GetProcAddress				(RTL::ghlib, "RtlFreeAnsiString"));
		VERIFY (RTL::RtlFreeUnicodeString			= (LPRTLFREEUNICODESTRING)GetProcAddress			(RTL::ghlib, "RtlFreeUnicodeString"));
		VERIFY (RTL::RtlCopyUnicodeString			= (LPRTLCOPYUNICODESTRING)GetProcAddress			(RTL::ghlib, "RtlCopyUnicodeString"));

	//	VERIFY (RTL::RtlFreeString					= (LPRTLFREESTRING)GetProcAddress					(RTL::ghlib, "RtlFreeString"));
	//	VERIFY (RTL::RtlAllocateHeap				= (LPRTLALLOCATEHEAP)GetProcAddress					(RTL::ghlib, "RtlAllocateHeap"));
	//	VERIFY (RTL::RtlFreeHeap					= (LPRTLFREEHEAP)GetProcAddress						(RTL::ghlib, "RtlFreeHeap"));

		VERIFY (RTL::NtOpenThread					= (LPNTOPENTHREAD)GetProcAddress					(RTL::ghlib, "NtOpenThread"));
		VERIFY (RTL::NtCreateKey					= (LPNTCREATEKEY)GetProcAddress						(RTL::ghlib, "NtCreateKey"));
		VERIFY (RTL::NtOpenKey						= (LPNTOPENKEY)GetProcAddress						(RTL::ghlib, "NtOpenKey"));
		VERIFY (RTL::NtFlushKey						= (LPNTFLUSHKEY)GetProcAddress						(RTL::ghlib, "NtFlushKey"));
		VERIFY (RTL::NtDeleteKey					= (LPNTDELETEKEY)GetProcAddress						(RTL::ghlib, "NtDeleteKey"));
		VERIFY (RTL::NtDeleteValueKey				= (LPNTDELETEVALUEKEY)GetProcAddress				(RTL::ghlib, "NtDeleteValueKey"));
	

		VERIFY (RTL::NtQueryKey						= (LPNTQUERYKEY)GetProcAddress						(RTL::ghlib, "NtQueryKey"));
		VERIFY (RTL::NtEnumerateKey					= (LPNTENUMERATEKEY)GetProcAddress					(RTL::ghlib, "NtEnumerateKey"));
		VERIFY (RTL::NtClose						= (LPNTCLOSE)GetProcAddress							(RTL::ghlib, "NtClose"));
		VERIFY (RTL::NtSetValueKey					= (LPNTSETVALUEKEY)GetProcAddress					(RTL::ghlib, "NtSetValueKey"));
		VERIFY (RTL::NtSetInformationKey			= (LPNTSETINFORMATIONKEY)GetProcAddress				(RTL::ghlib, "NtSetInformationKey"));
		VERIFY (RTL::NtQueryValueKey				= (LPNTQUERYVALUEKEY)GetProcAddress					(RTL::ghlib, "NtQueryValueKey"));
		VERIFY (RTL::NtEnumerateValueKey			= (LPNTENUMERATEVALUEKEY)GetProcAddress				(RTL::ghlib, "NtEnumerateValueKey"));
		VERIFY (RTL::NtDeleteValueKey				= (LPNTDELETEVALUEKEY)GetProcAddress				(RTL::ghlib, "NtDeleteValueKey"));
		VERIFY (RTL::NtRenameKey					= (LPNTRENAMEKEY)GetProcAddress						(RTL::ghlib, "NtRenameKey"));
		VERIFY (RTL::NtQueryMultipleValueKey		= (LPNTQUERYMULTIPLEVALUEKEY)GetProcAddress			(RTL::ghlib, "NtQueryMultipleValueKey"));
		VERIFY (RTL::NtNotifyChangeKey				= (LPNTNOTIFYCHANGEKEY)GetProcAddress				(RTL::ghlib, "NtNotifyChangeKey"));
		VERIFY (RTL::NtCreateFile					= (LPNTCREATEFILE)GetProcAddress					(RTL::ghlib, "NtCreateFile"));
		VERIFY (RTL::NtOpenProcessToken				= (LPNTOPENPROCESSTOKEN)GetProcAddress				(RTL::ghlib, "NtOpenProcessToken"));
		VERIFY (RTL::NtAdjustPrivilegesToken		= (LPNTADJUSTPRIVILEGESTOKEN)GetProcAddress			(RTL::ghlib, "NtAdjustPrivilegesToken"));
		VERIFY (RTL::NtQueryInformationToken		= (LPNTQUERYINFORMATIONTOKEN)GetProcAddress			(RTL::ghlib, "NtQueryInformationToken"));
		VERIFY (RTL::NtRestoreKey					= (LPNTRESTOREKEY)GetProcAddress					(RTL::ghlib, "NtRestoreKey"));
		VERIFY (RTL::NtSaveKey						= (LPNTSAVEKEY)GetProcAddress						(RTL::ghlib, "NtSaveKey"));
		VERIFY (RTL::NtLoadKey						= (LPNTLOADKEY)GetProcAddress						(RTL::ghlib, "NtLoadKey"));
		VERIFY (RTL::NtLoadKey2						= (LPNTLOADKEY2)GetProcAddress						(RTL::ghlib, "NtLoadKey2"));
		VERIFY (RTL::NtReplaceKey					= (LPNTREPLACEKEY)GetProcAddress					(RTL::ghlib, "NtReplaceKey"));
		VERIFY (RTL::NtUnloadKey					= (LPNTUNLOADKEY)GetProcAddress						(RTL::ghlib, "NtUnloadKey"));

		VERIFY (LookupSID (::gstrSID));
		::gstrRegRootKey = _T ("\\Registry\\User\\") + ::gstrSID + _T ("\\SOFTWARE");
		TRACEF (::gstrRegRootKey);

		//#ifdef _DEBUG
		//const CString str = _T ("\\Registry\\Machine\\SOFTWARE\\");
		//TRACEF (RSA::Encrypt (str));
		//ASSERT (RSA::Decrypt (::gstrRegRootKey) == str);
		//#endif
	}
}

DB_API CString FoxjetDatabase::Encrypted::RSA::Encrypt (const CString & str)
{
	CString strResult = str;

	if (::keyPrivate.m_bValid) {
		rsa_key key = { 0 };

		ltc_mp = ltm_desc;

		if (find_hash ("sha1") == -1)
			if (register_hash (&sha1_desc) == -1) 
				TRACEF ("Error registering sha1");

		const int prng_idx = register_prng (&sprng_desc);
		const int hash_idx = find_hash ("sha1");

		if ((prng_idx >= 0)  && (hash_idx >= 0)) {
			int err = rsa_import (::keyPrivate.m_pData, ::keyPrivate.m_lLen, &key);

			if (err == CRYPT_OK) {
				ULONG lLenIn	= str.GetLength () + 1;
				ULONG lLenOut	= lLenIn * (1 << 6);
				std::unique_ptr <UCHAR> pszIn (new UCHAR [lLenIn]);
				std::unique_ptr <UCHAR> pszOut (new UCHAR [lLenOut]);

				memset (pszOut.get (), 0, lLenOut);
				memset (pszIn.get (), 0, lLenIn);
				memcpy (pszIn.get (), w2a (str), str.GetLength ());

				if ((err = rsa_encrypt_key (pszIn.get (), lLenIn, pszOut.get (), &lLenOut, NULL, 0, NULL, prng_idx, hash_idx, &key)) == CRYPT_OK) {
					std::unique_ptr <UCHAR> psz64 (new BYTE [lLenOut * 4]);

					memset (psz64.get (), 0, lLenOut * 4);
					int n = EncodeBase64 (pszOut.get (), lLenOut, psz64.get (), lLenOut * 4);
					psz64.get () [n] = 0;
					strResult = psz64.get ();
					strResult.Replace (CString ((TCHAR)0x0a), _T (""));
					strResult.Replace (CString ((TCHAR)0x0d), _T (""));
				}
				else 
					TRACEF (_T ("rsa_encrypt_key: ") + a2w (error_to_string(err)));
			}
		}

		rsa_free(&key);
	}

	return strResult;
}

DB_API CString FoxjetDatabase::Encrypted::RSA::Decrypt (const CString & str)
{
	CString strIn (str);
	CString strResult;

	if (::keyPrivate.m_bValid) {
		ULONG lLenIn = str.GetLength ();
		std::unique_ptr <UCHAR> psz64 (new UCHAR [lLenIn + 1]);

		memset (psz64.get (), 0, lLenIn);

		strIn.Replace (_T ("\n"), _T (""));
		strIn.Replace (_T ("\r"), _T (""));
		lLenIn = FoxjetDatabase::DecodeBase64 ((UCHAR *)(LPCSTR)w2a (strIn), psz64.get (), lLenIn);

		if (lLenIn > 0) {
			rsa_key key = { 0 };

			ltc_mp = ltm_desc;

			if (find_hash ("sha1") == -1)
				if (register_hash (&sha1_desc) == -1) 
					TRACEF ("Error registering sha1");

			const int prng_idx = register_prng (&sprng_desc);
			int hash_idx = find_hash ("sha1");

			if ((prng_idx >= 0)  && (hash_idx >= 0)) {
				int err = rsa_import (::keyPrivate.m_pData, ::keyPrivate.m_lLen, &key);

				if (err == CRYPT_OK) {
					int res = 0;
					ULONG lLenOut = lLenIn;
					std::unique_ptr <UCHAR> pszOut (new UCHAR [lLenOut + 1]);
					
					memset (pszOut.get (), 0, lLenIn + 1);

					if ((err = rsa_decrypt_key (psz64.get (), lLenIn, pszOut.get (), &lLenOut, NULL, 0, hash_idx, &res, &key)) == CRYPT_OK) {
						strResult = a2w (pszOut.get ());
					}
					else 
						TRACEF (_T ("rsa_decrypt_key: ") + a2w (error_to_string(err)));
				}
			}

			rsa_free (&key);
		}
	}

	return strResult;
}

//////////////////////////////////////////////////////////////////////////////////////////


void DB_API Encrypted::Init ()
{
}

void DB_API Encrypted::Free ()
{
	if (::keyPrivate.m_pData) {
		delete [] ::keyPrivate.m_pData;
		::keyPrivate.m_pData = NULL;
	}

	if (::keyPublic.m_pData) {
		delete [] ::keyPublic.m_pData;
		::keyPublic.m_pData = NULL;
	}

	RTL::Free ();
}

DB_API unsigned __int64 Encrypted::GetProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, unsigned __int64 nDefault)
{
	return _tcstoui64 (Encrypted::GetProfileString (szSection, szKey, ToString (nDefault)), NULL, 10);
}

DB_API bool Encrypted::WriteProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, unsigned __int64 nData)
{
	return Encrypted::WriteProfileString (szSection, szKey, ToString (nData));
}




DB_API __int64 Encrypted::GetProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, __int64 nDefault)
{
	return _tcstoi64 (Encrypted::GetProfileString (szSection, szKey, ToString (nDefault)), NULL, 10);
}

DB_API bool Encrypted::WriteProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, __int64 nData)
{
	return Encrypted::WriteProfileString (szSection, szKey, ToString (nData));
}





DB_API std::vector <std::wstring> Encrypted::explode (CString str)
{
	static const char cDelim = (char)0x01;
	std::wstring s (NtTranslate (str));
	std::replace (s.begin(), s.end(), '\\', cDelim);
	return ::explode (CString (s.c_str ()), (TCHAR)cDelim);
}

DB_API bool Encrypted::WriteProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szData)
{
	NTSTATUS status = 0;

	if (!RTL::IsValid () || !::keyPrivate.m_bValid)
		return false;

	CString strSection = NtTranslate (szSection);

	if (strSection.GetLength ()) {
		if (strSection.Find (_T ("\\Registry")) == -1) {
			if (strSection [0] == '\\')
				strSection = ::gstrRegRootKey + strSection;
			else
				strSection = ::gstrRegRootKey + _T ("\\") + strSection;
		}
	}

	//TRACER (_T ("[ink code] ") + CString (szSection) + _T ("\\") + CString (szKey) + _T (": ") + CString (szData));

	bool bResult = NT_SUCCESS (NtWriteProfileString (strSection, szKey, szData));

	return bResult;
}

static CMap <HWND, HWND, UINT, UINT> mapWriteNotify;

DB_API bool Encrypted::SetNotify (HWND hwnd, UINT nMsg)
{
	if (nMsg) 
		::mapWriteNotify.SetAt (hwnd, nMsg);
	else 
		::mapWriteNotify.RemoveKey (hwnd);

	return true;
}

static const CTime tmStart (2017, 6, 4, 1, 0, 0);

static long CountWeeks (const CTime & tmCount)
{
	CTimeSpan tm = tmCount - tmStart + CTimeSpan (1, 0, 0, 0);
	int nDays = tm.GetDays ();

	TRACEF (tm.Format (_T ("%D %H:%M:%S")));
	TRACEF (ToString (nDays) + _T (": ") + ::tmStart.Format (_T ("::tmStart: %c, ")) + tmCount.Format (_T ("tmCount: %c")));

	long lResult = (int)ceil (nDays / 7.0);

	return lResult;
	//int nWeeks = 0;

	//for (CTime tm = ::tmStart; tm < tmCount; tm += CTimeSpan (7, 0, 0, 0)) 
	//	nWeeks++;

	//return nWeeks;
}

static long DiffWeeks (const CTime & tmStart, const CTime & tm)
{
	ULONG lStart = CountWeeks (tmStart);
	ULONG lCurrent = CountWeeks (tm);
	return lCurrent - lStart + 2;
}

static CString GetDisablePassword (const CTime * ptm)
{
	const CTimeSpan day (1, 0, 0, 0);

	//#define __GENERATE_DISABLE_PASSWORDS__

	#ifdef __GENERATE_DISABLE_PASSWORDS__
	{
		CStringArray v;
		int nLastWeek = -1;
		CTime tm = ::tmStart;

		while (tm.GetDayOfWeek () != 1) // advance to the first day of the next week
			tm += day;

		srand (time (NULL));

		for ( ; v.GetSize () < (53 * 3); tm += day) {
			CTimeSpan tmDiff = tm - ::tmStart;
			int nWeek = std::stoi ((std::string)w2a (tm.Format (_T ("%U"))));

			if (nLastWeek != nWeek) {

				{
					bool bFound = true;

					while (bFound) {
						CString base32 = _T ("234567ABCDEFGHIJKLMNOPQRSTUVWXYZ");
						CString strCode;

						for (int i = 0; i < 6; i++) {
							int nIndex = rand () % base32.GetLength ();
							strCode +=  base32 [nIndex];
							base32.Delete (nIndex);
						}

						bFound = Find (v, strCode, false) == -1 ? false : true;

						if (!bFound) {
							CString str;

							v.Add (strCode);
							
							str.Format (_T ("_T (\"%s\"), // [%03d] %s \t%s"), 
								FoxjetDatabase::Encrypted::RSA::Encrypt (strCode), 
								v.GetSize () - 1, 
								strCode, 
								tm.Format (_T ("%b %d %Y [week %U]")));
							TRACEF (str);
						}
					}
				}

				nLastWeek = nWeek;
			}
		}
	}

	return false;
	#endif //__GENERATE_DISABLE_PASSWORDS__

	/*
		{
			const LPCTSTR lpsz [] = 
			{
				_T ("UWHD3K"),
				_T ("XHEDLM"),
				_T ("7BPK2J"),
				_T ("CGVJOU"),
				_T ("SK3IG6"),
				_T ("3QFLS6"),
				_T ("FAM2SD"),
				_T ("E4ADX5"),
				_T ("YCEDZV"),
				_T ("TOXCES"),
				_T ("2CKJFS"),
				_T ("3ZFSPR"),
				_T ("VCEJSO"),
				_T ("4EVNWX"),
				_T ("BHJ4AY"),
				_T ("NZDXEU"),
				_T ("IWBPKE"),
				_T ("IYHG73"),
				_T ("X3YHGJ"),
				_T ("WLI42O"),
				_T ("GPFDLU"),
				_T ("3KLFBR"),
				_T ("JUBM6A"),
				_T ("EIQUW7"),
				_T ("XQPRA5"),
				_T ("QAX52W"),
				_T ("ZSQXGU"),
				_T ("4UPO7C"),
				_T ("XNA5FK"),
				_T ("TBFO4Z"),
				_T ("74RE2G"),
				_T ("VI5AOU"),
				_T ("XEDTYR"),
				_T ("GHXPKY"),
				_T ("EZBUIY"),
				_T ("2JMEIW"),
				_T ("36QGYR"),
				_T ("7FA2PJ"),
				_T ("HT6WIJ"),
				_T ("LAVMGH"),
				_T ("3PVCNJ"),
				_T ("GBPKYL"),
				_T ("2KOET5"),
				_T ("2H64N3"),
				_T ("FPUXL2"),
				_T ("2F7D3E"),
				_T ("EIQNRO"),
				_T ("QNOJDA"),
				_T ("XVAD3W"),
				_T ("L3ZH6Y"),
				_T ("7IUCDZ"),
				_T ("IEYNZV"),
				_T ("ISUV2H"),
				_T ("7JLET6"),
				_T ("ZXESD2"),
				_T ("QNA7GM"),
				_T ("6BLMRS"),
				_T ("PU4AHQ"),
				_T ("U5TEF4"),
				_T ("NSZHPR"),
				_T ("XE45QM"),
				_T ("ID2VQC"),
				_T ("I32C7J"),
				_T ("WS7O2B"),
				_T ("C3FVEB"),
				_T ("NVICA4"),
				_T ("7NMQAF"),
				_T ("WPHKUM"),
				_T ("FQ75VJ"),
				_T ("SYPFJV"),
				_T ("WOJ5TF"),
				_T ("MAKR6Y"),
				_T ("XL64VF"),
				_T ("BUTJCF"),
				_T ("AZCNYO"),
				_T ("NX2RA5"),
				_T ("FNIY2C"),
				_T ("XNUF5H"),
				_T ("E4YFCW"),
				_T ("6MFUD7"),
				_T ("PMGRE2"),
				_T ("FMY5DG"),
				_T ("43REBM"),
				_T ("5IZ43N"),
				_T ("KVFDRJ"),
				_T ("ROKUBW"),
				_T ("WQH6P7"),
				_T ("XRBIA6"),
				_T ("6S4ZVH"),
				_T ("VYW2MX"),
				_T ("V3XNPU"),
				_T ("RIX6SP"),
				_T ("IKVCLZ"),
				_T ("5IKZNQ"),
				_T ("P5YRDJ"),
				_T ("CXIK5M"),
				_T ("X5S7LM"),
				_T ("LSK5AR"),
				_T ("OWXTJC"),
				_T ("VLPQSO"),
				_T ("QFBAUC"),
				_T ("V73ECM"),
				_T ("SNPA3Z"),
				_T ("JCA5FP"),
				_T ("M3AJ7P"),
				_T ("DN5OBT"),
				_T ("LQ6KIG"),
				_T ("S5Q2PD"),
				_T ("X673L2"),
				_T ("TSFYXN"),
				_T ("BGYAWH"),
				_T ("YZUHR4"),
				_T ("RDO4EQ"),
				_T ("XQNVUO"),
				_T ("OUS5DM"),
				_T ("VK5XT3"),
				_T ("PCUK3M"),
				_T ("UWDXV6"),
				_T ("BCUMQO"),
				_T ("25MOGE"),
				_T ("YOHGIJ"),
				_T ("CYHELW"),
				_T ("3UTL5P"),
				_T ("IYQ6UM"),
				_T ("KOZQ45"),
				_T ("RITAEJ"),
				_T ("TKUA4Q"),
				_T ("ZCNU7X"),
				_T ("VX4F6Z"),
				_T ("AMD6V7"),
				_T ("T5QI63"),
				_T ("X2IRYG"),
				_T ("5QYUR6"),
				_T ("GZ6537"),
				_T ("32GYB5"),
				_T ("BVN7ME"),
				_T ("3UQLYJ"),
				_T ("M3JVI2"),
				_T ("4ZMIJ5"),
				_T ("L7ZRDX"),
				_T ("26PSEI"),
				_T ("P3NT7Z"),
				_T ("4SA5VN"),
				_T ("5LN3RM"),
				_T ("4SBLQF"),
				_T ("A7T6WZ"),
				_T ("WG2AJB"),
				_T ("RZCEBD"),
				_T ("UT5GHY"),
				_T ("FHSPJO"),
				_T ("Z2CDR3"),
				_T ("XHNEYQ"),
				_T ("LUPEHW"),
				_T ("XOQUZT"),
				_T ("G5SYTJ"),
				_T ("THO5C4"),
				_T ("6REHDL"),
				_T ("EU32WZ"),
				_T ("UYGZVL"),
			};
			const CTime tmStart (2017, 6, 4, 0, 0, 0);
			const CTimeSpan day = CTimeSpan (1, 0, 0, 0);
			int nLastWeek = std::stoi ((std::string)w2a (tmStart.Format (_T ("%U"))));
			int nCode = 0;

			ASSERT (Encrypted::InkCodes::IsValidDisablePassword (_T ("XHEDLM")));

			const CTime tmEnd (2020, 6, 14, 0, 0, 0);

			for (CTime tm = tmStart; tm <= tmEnd; ) {
				CString strCode = lpsz [nCode];

				for (int n = 0; n < 7; n++) {
					CString str = GetDisablePassword (&tm);
					ASSERT (str.CompareNoCase (strCode) == 0);
					// ASSERT (Encrypted::InkCodes::IsValidDisablePassword (strCode, &tm));
					tm += day;
				}

				nCode++;

			}
		}
	*/

	static const LPCTSTR lpsz [] = 
	{
		_T ("gn7EkIJIrgVnfYGksa3rgbCYKkLR47gVpyLt4jc7893Stubh4MGzBbHaInPYUzoM1k4rmUHQA4yweBwtkuHHjGI5ad7t/2ZXHsJFT+HaD4yJ1/gCcm4zBqRaSIFEEPWfTnS/apwZPnWcn0k0Zh8IfSu/QaeWpu/QSakqZ96ByKQlRs9q2D8yjSOX6MtH8dFbme4U7BUOvsn/QoLSi6Zfs+j8N/Jz2nSpG3hY9Wz5OU1KiFSsv6YWYwdYhz3ff4743vDqC5hWKsdgRCPljhvVld3E+mLWmsE3Fn6RvG2McfJ3cipGLFfNYORhEu1lyBEMlq5BngIHs9nRjVj4vxvcRQ=="), // [000] UWHD3K 	Jun 04 2017 [week 23]
		_T ("E381yJshLemrYv2HqJFAfmul+5/bCMDxhAk3vbF/pLWJuzSkIkS75ShK9R9cb8II6j2SyADwNJMWnikuKNZpcMoU4El1o07kPPDO0ApErBrWtkR7frpKh2dp2Rl4+oE4ysXkPGG8jw047OyxcENDlGs9BJ0uM50GcEPZlLex1FgWf/CJZG/6isHdw43fQoJtthFvXDv8/XnbKTMVIJtlu4rSz10brs64NVzANJBBjaMBePH/ZixTQ6oeVMrnZtUNhjcreJ8wobrUFeIBzTX6ZYLGcmtlYGLQy0rHbrtNE04EpykK5s2d+3ETQ0+yoDep3Gm8UC/1FSMWnb3qzm2HEg=="), // [001] XHEDLM 	Jun 11 2017 [week 24]
		_T ("fx4tSMZbCPQT0d9MuIwtRV+b9we2ACJYEKJ8OIPYMek18vvTBWO1jqT1OqCixEaBdbCejPlLHC0M0FXW5KV7TLNsjWTF0okd+eWcJVngHxZecG9alIFf7kWPT4SdPEl7dmF8JiphYFhxgYUy8Qz9FHXvGJcy5L/Kj9ZBMkhFjJ9BqaJpnTl2xdalelbTuJUNaNxDr71Wq76UasW6xGtpLyalwCBIP9xxhgHC+1UkSl4OCo4OKXBcG0Y+iFhAZLZ+pF59VhLshL8gKMpvG8Dz771T2V3dnuNMLnBXLmg93I0RFZR2838yquMFHicFmJICJjReB6myjhI23DLZJ9i4Yw=="), // [002] 7BPK2J 	Jun 18 2017 [week 25]
		_T ("lOmjybFtjzAb+H3FSfDH7ypS96Z0cPJiCLYK7fo2OQyXP4k6drCNlyTlqDSLWbj3Ctu0EUqFlxY44Bofa2BLJ115wzWUOsOd7gntoT3Pv77eeYPYJdBQvZ4cfumi+TacjttA8py2ZMBnnvcqJwDKbXOgIaDFL31wYu0TUQjQkDavsp7JVhVdUiFz0YPlIyFz7Jr7Mxg8oLTyq+c6TYDpiJyljyIOU+cqJC88VbR0zQhQDlDJKsW2W8qRH885FcOuxx81DIlYI8Pl2sCiazDGJmWAMMAdjsNZBPfrU5TEN/ru/uvhcL8ZysOsxvVBYWfK1gVjSUyCEkxD3Ee7ixZRFg=="), // [003] CGVJOU 	Jun 25 2017 [week 26]
		_T ("yhwZytaoy2t0hpkBqfPEFUytuaifDyNo0XrXhkmv2PgwRJ0VtWcffyQhFLzIhSuH6XHSlcqH9cU08LKuVunAoKE6ixaGS4xsjQCGTPqMRFJUjdpvjnRXVuus6EUHatgz1PImnJTD3+hCfQujm6gHqa/FB5ji8ntCqRZAwAxUbloKku+wX0P1+nPKWWIjDDK/NuqVqeJXlxygzwQ6TNQFmjLcFNG70ldi0Yahx+FlQjLdVRByvq9YogIKWniw/Pkkx5kgV8seuBOew624GOJFGKH/7mOh0NhKl+RBV490V3Vc8qduHvN7AahmhVmOORxhjSFVv1kgIHF3P/87hce4aw=="), // [004] SK3IG6 	Jul 02 2017 [week 27]
		_T ("R72rP/mUM0imMM9mmtVx2BjZ417Ig2cQxizfcsmBMtgR87wpsYrFHRn2FZJ7AHEbgtnDGDmq3Ac7fCmdLEhgWGYE6V5337rweia2nUMwoIRnZGY1SFvKLdcfkrZVhmyX5XHo+PKyrJt3jqO3mFdgn9FoxP+mLHcjn7cfehhyGFlMYUWCAknbljege+MC7M0K2oFQDGyPBsaiEIp135l5GnMBRbEYtB02aV7P9oARq8wHyi7gBFLUOEQWbog2IEnNSnuJM8dnNWbEyV/D+KI7736mZCAiuEeBRlRoimaz7cLSQr9aylwCgZvZwCOzinnlCAU9AmYuVZKE6lkwmms4QA=="), // [005] 3QFLS6 	Jul 09 2017 [week 28]
		_T ("wZqDkhyCFCoP/Ucs6VOwGoU/aBJcJzC2u4dgt4efxZFoex3/0ACyUn5VgSWcf+t4ZG3N5jxTVPEQPhpS1pOdERhajjWT4/ZXJRj871/Gi3iqdC3cVrRWMwHV2BIYsyp31fXu7JBbxWVjOy4Qd8M6wcTjoONmhhEIokUThbRHattpV+UCAxUN0uHyYNHgz6LO+FrX16m/hvXeM2cmkOCrw2AcY8cEKkkeWuTwaqibbyPwb3AHdoodqR0jcX9DemOehdjNA8W3N9hZglynag/IwxCn9y6xAI7H2WN3SDBLKeNA2K5wsk4JogvcqeS35UvwC7X2vF2pI7VFPfpUWCaWYQ=="), // [006] FAM2SD 	Jul 16 2017 [week 29]
		_T ("GyEsTshqBt9eSET5SrY6A/G9YkA5nXZmgdCqXJu6fT/6fVIhOaEXDMn9A+0jDVRIABbvXC89ynfJ9po+akKAgd2XJej8b9zX18SSBC0SUbd7MTsPGZHUPwJMvqXUPyyohZ5Jhm5Me2JDgt8S0ObjPCGo9e1TwHkKB17MpbRssFpXbEg+KpeCh5Swc/Xpf7XMEiIU15dnRAvgjB4IwCTv3JSnYnaPIM+XRODZaAt3RLfLzmiMdJcYzmCpy0lEQJ/wkDyJIOVOKXsAQmtyECA3tiZAJRo/SVdlmqko0OEyIT6xTTVbOzpSqF7+zma9OspcmtDVSfTmkmuXrkRNOR7vEw=="), // [007] E4ADX5 	Jul 23 2017 [week 30]
		_T ("wmCLaZePAdH3ZMc/p2KugARpQVBYZ6bcWUZqaBu37vpjlLzq7GY8QJ2ufm5BHCn4QpyMukXMYKL+gUOfjcwp8+WJGU96gCeZlAtkIE2PDtVOfeRQ9cTTkm8zCyluL+lY1kW2wT4vxjWmnUBZI2lEx92FL/tuDjV7o4EPYR3zYjoSsGoCyXqVUbHqmXej+AXvsfb1urANSuGo9IZBNiWqD/G2/x9r7wXjujYNxBBrkqtMLHVTFIujCsqTsF+kHg7W+JaJNakCP1XpOOF9KuA6udiZCl4GqUdD0KIVxL+8nGoH3MOhFoYpUJOSPovxp9bPcSEh3GfCvyhQ2aP0ft5z4Q=="), // [008] YCEDZV 	Jul 30 2017 [week 31]
		_T ("RTFr1GigiZx9u69mK7orV9aLelPW7e17qWPnMLpM5v2GnrbFsvgbehYRUZJH0nFwmmy8KMdo8zLRJ7CNzIX8UUk5/bswaFZtRoJh2WOTPCVlNzDqHI2IAZ++R2n29NJMseRnqd6Vwamd9gkAVE/hrLOVTD5/FiO6RmjFEdobUi7e/KQyLh+tXOZU4eLv0u4nr8F4eB+MzBos3mopcS7fzgrITqn+CgSAn3vukKoasJcYlpDuH2cXIuyAxoyFmm+MafmVXbuBg58dddcAHayoIK9pvPw/UasXFSCPUmFoNijVdm62pPu6Xl7xxOt8XV5tNZL/wDgpvcc0V0CqAzj62w=="), // [009] TOXCES 	Aug 06 2017 [week 32]
		_T ("TqOG31Zbz4VhqJhk5jRYIHh3Uhqso+LDq9iKLoh4YLSCa/fvuZob2wSLv0QVt7+hp4CfHoT+VMg+kvJ0Ob/+5tlO/g6m4dHHLU/SZ43gYxQPIkPclGRLbEuioLeGcOYiB2cNrulLoRRdpPz/8jJiX43XXbp34nlz6EqjcoQ+rDzSiELBQhqzVQZR/cEV0Mkg0J2pQwM1N7uubfTcyEy7jdEKUAmolXN0rT8CAReks/87HEa4fdvrj4jk71XwzaEv/9BPoyewF7JTVxISt338BWJEmeKbWcYKaEzJGb9gyY3ZTZZW5ChcDKRSOiGufYa5ZyU7cC5fCxlCKf8fDmmdiQ=="), // [010] 2CKJFS 	Aug 13 2017 [week 33]
		_T ("Zr2S6jOvGmvZr7KM3sOwDB8lq9IMJrNLMZAE0WDJtnvlRXtuEl1Cc5HhOGDw1dMiUqZCTMjMbN4yzvwJCPrwvtv0I70a1J0lbzAZrN4a6GdwK4G39l1jPB12Z7eqHzXI0Xxm12K0OwFlxbnT0djIrCkTwYSJ+RkkX2tS5u7FEml8ul4TDjuJzwvVaFRd+7mFQZpIVATVLy4lhyNMOzQ6pwMjj+rCNXcWraVEMnyP32+5q3EA5aDtcx9CpYLnHBYIx7hTozzPfmFOJoLrGSJq3nM6iD/QRVCquo4FXNRanV/ntQJGpVn+L92VNnsKLVqu7obVp4MxpFb+fI7DNfUcUQ=="), // [011] 3ZFSPR 	Aug 20 2017 [week 34]
		_T ("T6wlOrUxdxwm4pDwXYmdUp6vqt4QjCFE+IzjhBvxQC1/F+lFdEu0Iez9EwRQUpQjNQlg53tVhSVFpiF0dsMtzrH5R2tePH2sO3/s1KoaYTEm00YWyAqfMMQf0K9HkXjB/d2OTd1x8XwsnVj100+V0y+7Pg/TX1+gUD7z8LLQ6nTZofac3L6e0V8+jpYpEA8nnQIDLDOtNBbzPP41Rye62Y0uoI83kwOkF/jl6+2q2Pk+KmeHZDNohTue3jPnjWrkTVr0PzqjtRSkDdsD7DzRU/qJ+Bp172UO3OF0drixyPZS7LENR+wJE5DVEiWYx9d78J0K1yCxbTBaMkfpd4imNg=="), // [012] VCEJSO 	Aug 27 2017 [week 35]
		_T ("QH54J0QF2bfYCqzSQFHBkfKyM6nNlHeuP5yeb1cJA3AaGJBO8+R8f6BQiPNE2KHccbHtjJrFRA1iqvy0t3OOj/muqIi7x+MfK4sO/4chnwoLqUGDqTWO0m+c+Q1pIbF8YIz93vapJ28AAYrtRlN9LREAvNrWx3DJhPDrY+36o/2kg0twRQ+EtMn3JeSdOdgNDrmGUuQlpdrWw9t8QiQj9eu3va5ne2Mhd/lX+MEkiwWQXIepeySPMDmp6srNSYWAkEXZKNZ4cHPg4AiJFwQzIYkKopVKTQ5Xu9bPb2JerSaRohlAiLoSPXW8NOaDSuIA2e8p8syaV1Pc38ON96B6KQ=="), // [013] 4EVNWX 	Sep 03 2017 [week 36]
		_T ("ktcNNpCe0jntU8Ds8S1ptsJcWvABhbL1eJ7swoQXhC1mR5c+YnoRgP8ghC4w1SJ0zTHGOpNCqI4peHLNlevgeG8dvQ8n0at7XzjoBb54u+EaWs1yvnfZL6nQ1zCKBc04yjs3xYRKAVS6lRP+6qJlZXpEQLT/u8g9PRiIdge2/f+MT7aK5gdN7pfM9MM/UZxWbSZdqwrB6qXLuHQ8W1wUNeL4DW6a0rGzhkWMyaMwma9zKvrMo+5gRmU+xQq4Frovm3+h+9lAgLo78siL8WQDJtQPzQecKjv4ubfCJmPS/nUTI/1/AjGt9qEQfcFLdXkGlgwVxxdJhBI3UvDcRsCKKg=="), // [014] BHJ4AY 	Sep 10 2017 [week 37]
		_T ("yblHwkAM5cLRIPAusid2T61q2o8uI19XLVxAWLje5QxXnQZ6dVo2gdS6lESk7WRz+mEPlU99l5a8ofgpdkjH/UmhT0vfe5iHdTIeRcAuTEPoTVnXXi351EsFcnMayi+kxgZJai84DG+P5vEphYKCrGAUVr9BhAZIn7zlurIx5E+MpF1BuTxYwT3jmbRvG3iqqePDvAEJNeOacfNVyd49YAmh2CQPe6VKn8xJxwN/rPOX2g/WiyaoTYO9y8PTH4iyYQ5g8HiWQyoFH6BGS8GariSeC7Q8JYJX4eXhLBAnk/Rv/8B0D2b09dIgZgL8c08BlZsdc2iOpGaE+ng/cpwhPA=="), // [015] NZDXEU 	Sep 17 2017 [week 38]
		_T ("LI7Ng2ZAhBQ7Hq344aic9JJHv1N5Eqpbhga9hmQdRCe5+QiEQVOcb8xx+ZNk+G+K+ISSh1qqRlHR9D2JNCzUFrqUEPiyosq+G2A9kLU7pwONtgMNZNs2fgJSEmvxqrCUaEKhpr3nDFEF+MzYbDNB7XG3o51duhrbI98xY6J6c6dHTZQrNhEDRHZq6cBBq1gcI6YvTN9E+CQgKwb8yV2YX1CvBat8mW6kqirhd0IDLP1a7/7x4wKKYeUT0bhrslnjtCerP+/GDA6cEW6LBRZ3vNQ3FUi6yAdUd4Gh208hMFcF9pIZz61Ni67VhjuAzVctNGkDsZHwfRz447uHvdc8mQ=="), // [016] IWBPKE 	Sep 24 2017 [week 39]
		_T ("ohOiWftAzccKOSpZ5BKyI6dMaFr5la5WZFMFp2kLe+JH15qJVpteZgWeiihpvdNTWpCei67avEy480Uxi+4nc5Ltp/OY8JYqSC7ESOiQuOPYLs1b2x/S4T4giWKAncn8Hl4NO4Wx65XEiMhuobHb1vJzeVC0pegPGjufMTWhefUkva6hFEFFVVliM1pkmD0sMdJZM9wJuq1rRi2Cn5WoWWs3Li7X8NrgByfBRijOluglAXLk1D7OhsMtKHihwHaA8Gc6nQvXpOFUgxVX7Vav6GTgpkH2R2eKJT79fbOi1GbcSU2oJ+r5Lf07VA8NG1xrrRM6+VsADfZPsfcx/r0GMA=="), // [017] IYHG73 	Oct 01 2017 [week 40]
		_T ("R7Wn5EPC16DWdtvtyfz9S6u595qRlueYYLHBcp94i/9ryBMdf6lUYHWltt+rmZ3+JMDgL619tkzNcMIO3oQHhFPj6d9kTV1Wo5T9OX8EmfZWIOEtbtj5Ovzg8JDbb8gSU1T3iAZR6W2JUmbWuka55yPWqyy7vXt0JXmX4NoS8ld2zEOjuoLib02fBmAuynFdFvALRk97HgnxfI4RkF+IrcdRQ9dJzeU1iMaumM/X3OQ1smfRlwm1+Cp2C1T3UjxUn4BbRh/V693YRW9fnmFzbtjj/EG+nlckjzusafO9lcEqkzJWHEZpQXHRRYG3oosNoGPEgis4Aau0Z6uWerr9tQ=="), // [018] X3YHGJ 	Oct 08 2017 [week 41]
		_T ("fUA/PwA92acOFDChPuOHI7R/0wuYfDB3CjdEnV5JznfW0m4nym/vd/EqsoUAc1sDt5rIRHBDqy8ufzcm30kb0FFDV1U0GLeHcNklfOe2RpDByX+9ptn5am3/O/xmrVa6Zz3m00I1SENwRBZauv2ejmIpcGHWY4JgcBE1Z9fmkngV68+6veY7OouwKSxFyanbJPh0eStbmJdV8k/I62gv9MJw5Yt3Cs+VMmj22YlrAk+qoyDc7DmCqqNv83hFt6dIapulCXeuhH7h5I1+2SpVHv5yUcU4SuJWp6IxKprol0MDD4A4cOLySEq4RRWnJjqjrCMjGyNYp5x32ar0xg0sQA=="), // [019] WLI42O 	Oct 15 2017 [week 42]
		_T ("MULFMBlj7ykINKLPg5820BmocCV9R24fDH+No6ptQDh2x3KSqwCdTc3N+mULh2cmNCPNsAHSjDmWxvA+1nfdCLj/nYH5gd+8Qs9vG+OQsTEU7sr9JscTWHpN5nsRdFX34RkQl2ECLTQC2gfBYnoBFDUwk0+Px0NUJSt/Mypz9L7DEa2LjmpBKAwk9sSsk1txNpDnzojQAJwh/JbbvUeE+uK7YQ5lE+ZnzAXLXDmoR84xF+bYl8yQ1dNAUnmPCZmL183ZpNSqSwu/HlUqBm5CXnxxP6x+Dmn4SpPQn2qLVljfLZCph8FqbbTAa48IJUcGwGbadfQcNVt2wzCCLRSuHQ=="), // [020] GPFDLU 	Oct 22 2017 [week 43]
		_T ("dplN0PE+mRsjG8d8my2urk7BNGL0Gdeiggw2ITahwxIueAIAWFX6ToI7gpHCbO8DDTV9Dv0DRTBBiIACg5j69ndPKcePvmPBkvZknJeNcvyMSHWQM9eZsaI+2euI+jhlRouNZMbYWt/Q/Kn/+Ay9DdF7vM5uSdgeyMgFL58BlI8YKqvQcAgz3mYmFNGaVMXdKeEF18T4smpWTIfodiVVtz9jXGe4EJje49XScqShfnvjGE3KcJNMhvE3NKLpDu0PxYJumfWq8WXGaMYeLToIIVRIfGVlhPvpV2zjg2CFc7/RVllByIbzbmtzVCB1xpYnVMa8M0gpwwf1gG2u/xq0Gw=="), // [021] 3KLFBR 	Oct 29 2017 [week 44]
		_T ("XLzpvJUc1+UaxxVmNMOqWxUlOoPrjW3aLFAWBJRzV7U+HKTAjnSg+GUYFgJ4+ijjZgmc2o7rCEFE7/7nJiNaYPX3Haswt8b6dTqz1cgBJr73i8ce51MOtslq2kWQCy3dXrAV/VYRbqMjIQMALFdiSEx5Wnh92oXktX22yJnBki0gSTU65c3kzNSxlGZu0UsQfaHWVv47kEATuUeYMDADS5opMZDfBLvi6tr8+RwrVGj6DerGRHnfOjq58njlMYbI8gQYkwWOnMBGyD1JmO6eZ3Jx/SbRiFESfFUpQvhgfTUi9UoQVL1hujHQ2itMsXlZvWmUIlryfFxiyWho5tDrEw=="), // [022] JUBM6A 	Nov 05 2017 [week 45]
		_T ("vi+KT+jleAORgxs1ELLVySRK2Qi0pJh+SSOVaAcqZ6oUQUdf2S+s+NCXSlUuLLD/qD72gTF1B9QKSQvNMZzyGTnkR7sNzR0gq/xADa1ifRJ3Tb7H+652RcA+wVgJIjdhkjjmofqdEz5TQEt6j9sOzU8ZQSUukjEiCGWi4cy9sBb/5vhtfryfqq5KjKD7vUsSsZhgfK9fEoz5jEltulcRK1N0NHP2e8eLUu8cwvGyvDmf4OA5OBoc22Q0Oe6mAFnsFJAbu5fKac1ZYZgXjmdHHZ3h0LF5vhb+is4PwiaCpNzTbjT8CP8XSKTE4xQL3bEgq2NbE+BP9c1o96t4DwXgVQ=="), // [023] EIQUW7 	Nov 12 2017 [week 46]
		_T ("acgcnuVPf1h5DqYLwkwZacw3a9iPTluvyoXRNtJ9wODuUBOTkftJkLG4/JRmWuj7tCHkgLPuZ/SxpMv8oe+czYSmkgbmJdVg0i4HrTKXVEwhnqi9jZKpzfXwy8+88gkw0XpeP4ZeLaBihSmiUDzZMTZ02Bx/2gZpFa+747G1QvCHWZKiWCgZWLg3yqxT7vTJ7d1tcqo0CXLx014bpvSyjWOSBk7OY8K2OlMFyXuJsAE62HyxsToao5fcmAxAqL7AVGmrxCkkDVDuppUWbHiB3iUy/tSPBSeGBprGSaVFj9Ze39IZXdCck9eu0oqnZH93kml9RjDrLIQo8NYZCOddvA=="), // [024] XQPRA5 	Nov 19 2017 [week 47]
		_T ("eEBoZqCNip/8zZufb3NScPQ5ZNIVaC+RFcCI/LTB6uRi94dzegjK1vVGYWOro1qrn+Xoi/kDQpr5xpmLpr4GJcOSgkhRguYWGKNZE1oVerKJPYQYdDtETGR2ewkDSIXsoLXnOGRh8yXDK9U9HWqiHVef0L+Qlwml6kLKzmweH/S5OhNSd1tRO1SRV/rt0XY5YbguPKy+CneZt3ww5tFLoza5Msl3975XzfHwuAPS5Xd7D4UwR6qDgMiik71SxWEtze9yA7BYYy1TsHbyurQKlVkJBKG4zJcH49qY7myB6MAVJP2sxvBxRKabb92fcLdf0x+OriZpXjNNB8RE2t6nTw=="), // [025] QAX52W 	Nov 26 2017 [week 48]
		_T ("jxln2Hd1ka6LVGmHFK/k6wE4ltOmG/MHLyKClAZk2it7Xed0xEWlUe2QGaWthhFMozoyl/8vyM5FL2HzYd9ICFl/SXJkS1vHWFgjdfqtu+OH8VpMfLBss0XJigVz6h7YwZBgVlDm3YrX5nOe9WKpNBlAsu+Rf+SgfqVYHi8OzYgpdT5+8HNOVACzgd4wXQgqhShxUtSQXvs2cwtqPilm7JPuTTzOizPbUYO5Bi4CkTmHQvxFz6k9bgWtYUbF9ygno2VfFlxWhcn/TWq1So444n0UBw3WrSHjUfedqkqMGZsTsl00LUcbqo3tLU7qbiQarLdE+0KPTpGGKO5qmnEiiQ=="), // [026] ZSQXGU 	Dec 03 2017 [week 49]
		_T ("DSspVWk50QW3XPJoRImHEPKO5xQBuxRrZ0M9o+C1zfjoPKUqD5M23xrmFeEqcaYwAK0vq4iYMqNUgIZ24hFKvBoIuy3Ess4VQR6Jl9+2L1Cb4v4CEeOpTYlq76OrBkx61zLbExAz02SSUbahqcxIRP/bPdPDRVnY75ymu7fMELPyv/Nz6uI3CdZiUDjTsp6WPQEgplEfJut5VWVvvHMMhC/CE1rg/JgLOZk+KXnGwh7CBovOVhXrgbQ0ZwTjj65xqL3hUj5S9eRC6CtR+SchIJzmJMLB1SAdOnO/7xLvGq0VTHNP8tgBANVwd8PMSOs4lBtYFGaHg7s4l7XR9UsP6w=="), // [027] 4UPO7C 	Dec 10 2017 [week 50]
		_T ("EMLaGuE5u8KDNYxmEdt/aDkpnKcCf6sp9SWorcjzffqduf9EnlKXV0Jp0NSpTbVWKtfF/qDEBNURWnt7lHX/DqtdLXYEIPf/lGhXtYJ8A6n/invBgd3cPjsHGfvewnANPBbbvtzd//xRU4c1hfA9rGMzqUbE9zlyKnIpC3rezbEYqiLN8SIbU2Vsq89JHdO+aozACCzciMGRnhfXl/IY/DMQTUYD2ACviAyG1RNCVr7ZKZUSVPKQB+j8S6GdWXYCTERDkinQU85ARtIi7RKORei9nKsV6QKUTwZlUoM6S7Q6iHNDAdl6PhewK0GWlwa04HxAJFFduUO3fHEs7v3yfQ=="), // [028] XNA5FK 	Dec 17 2017 [week 51]
		_T ("iJYTDYlrHdfoeI7el6MeJnlXrtUCFcHgqSh3d/S1u93AwGnh/2QT6CdYosKCykd2snKkUyt19bZie5eR/pDDyV9KspUVWJjzcHyhsWdWabCpG0NjtJjYre2Rfg2jvjEYQn3VT9qmbG4SVUHvpbjzOkGUxykTYqTP/YvYN6mTjZdrCjWvyw3EP5deThPw5IU5A7j9rs6lhJ1/lvtUjRfLGyrR+kIbUHlmVsFRiKL9aYIsrkEMWeuG2nXHvHkNBMjORcrPEY//vgACeOe8Ob8Y4gaqhpBAAJzwPC22D7qdeO3N2iUnWNgyZhJyt1KDNrCWvo5KUmpTq/cu5VxuTqnbsg=="), // [029] TBFO4Z 	Dec 24 2017 [week 52]
		_T ("i1Lxq2hCtthKNW30U6uA4r4Yd+BaZrS0ZP00o6h/7fy0Z7LRHAI+wxavM4EMroIOVM6hktv20qQYvrSzwydO9cPgnL0v7/h0qhGaDaoWnoHoH2QiPARf3qwA6tXoJpYy4H1JgOdfmZte1GG9x26lCPMYqTMqRwnx3SJA2EFpqsAz9YEL5nnwFYp8DzrPrIXLbV8vt7kH2oQX8CwWXprsuys8JQAJbcFPLbgVlP3uXjJMcwlBNSZpeuU4rKldSJpTSTq1qs2eI6RTOK06ETjeyfdbwjC8H7FihSPlLwAkosiFBwDfR2Ybhv3sS6N40ta5SpUQgXZPmyiHDLrwgLlc5w=="), // [030] 74RE2G 	Dec 31 2017 [week 53]
		_T ("k5HguvH39SWtR+p1mtSuEwLrum/0u8zP+2Rmd3VheMLWC4+lbZ4Yk/ePAt6b1Bz7/m6Qo3Z9gq4L31MlU7OSCfYF+dosgi8eomgo6Ux7TTfI0zqI4SjGV5ZuLfDxbcNsXMaMPaSKDgbh42pkf/sCyNMP2aobUAO171aXKWucVjGWTkUU4b+ojRg+yKJJgT/sGls53aorJI+X4d8mFRo2P3opwOPBi2IPEC2896mE1cthYVqQAXSTV88SPszCp+qFVRwy5310iYflb5GiYfVS76/R4AxWulNeQ5hvkCt8lCUELuZTETtiLK8vhuWe06J5gZ0FlNBS6hW1UGmT58acYw=="), // [031] VI5AOU 	Jan 01 2018 [week 00]
		_T ("MwU7A20Uqe7iw1129cjCAqCo3O/jGmb0WJfnO1W+q7dtTaXKt3uUXz9bxzzOFwzwdsjfgtmJ9y9TZwrggtYNrptZRBaYfjPH9lNX7Z65nhL7QJbKX07Bt8e9hERFs1xXz+iT4VQ2SAU5Y3mrpFHid1mmy8RWJha6plghpCO+brhxDeN72CGJbgYPmtsomrgNQftN4jgFFA1W9fXUilPbeKfH9eV9RZrlfB7FzEwKvOaV+Tra9y6krTGsmeoyEqi444dJzywYmhHI7+VaGlx/2Tdc2wA7Jr5I06sjLRCSylbqlikHMXvH2Pj5LAwYlZkmtr0LAiKHnHQlVrdqcnMpYw=="), // [032] XEDTYR 	Jan 07 2018 [week 01]
		_T ("A0tueN8D8wqHAA5uKHAyRLa5I8RXCUKC8hDQf0x7u6XlruD7rDIU8U6wuOMJET7z3bhYvDa0NVaDvnH+DL0TXVjrTIi4KH3j8QG0wzHicv3dVpG8hrO4aPq7aSG0rmGM/tC2MWhTKEqLesVckTxuYTyZPziLIUkWtCLX5k5QRZq3icVnk862FCMo6Y4KNKPmIFOWguHv7kIX+sX1tM9bPerlMF/FunavBXkvYbYL/2UnTXVEKYAovkSecR7ZiHThuLqsLABFhjK5FWTHEsjrm5s1KDzK54hSTQCgCKFlV+ntwwoWs3sJaW++lSCS8WeLFYZGbqimDXjsN0qS6uBG+A=="), // [033] GHXPKY 	Jan 14 2018 [week 02]
		_T ("TsHi+dg/OmWEhyjUVYc7isc6Rli20XsVV9r/uUA02fFSllleC2bRRb/6DYoXjGL9l1NrIFHD8TIWDusJWHinYUakYuY1praPYf+enZleEEl6XZI1Z88YQahKMAV+REe0ZZS1e9P9TLHtmOVzgo/R5OUgubQQUhjBJouP6hUV3SCkRYxkRy7w95SIv/2cg4r+kkBTVbahIEuDRx9RrRuZhisYhWPYogtME5zVUVnExxdGcp+3syobaHgTxJwF6DVJtSEzrZV/S/Wp29lucZpq4ksd7HNnP0znIeKG2oVRouOWYkOsdQWKOElQ09NnTsB0VsFzhAfTu4zFb8fINzjmpg=="), // [034] EZBUIY 	Jan 21 2018 [week 03]
		_T ("pDc3izDFjNabIcV/BxYS8v+xK+CQa3b8xYPMrJ2/6lddgMJjk6b+wR0F6Aq63tzO/v6TmTo5VUX0SMPNrkxNRPkBFz2eF6SNNNlC/JNcNiw6J9HxZeZXi4Q1Mk/pJzW2v7+tkR2FAVci+cq7PwA9UHQewlL2bIgqKM9y2bw0hqEgJJhhf18BkqV2s9+6swYAhMw0Tnz8tFuCNcIqid0NPD3auchjTw5UBwuQQfDPa4j6Vkyckli5qQfXQ4LIBtVSFtcoag5qgk6qav4p9S9f03GvbPsqpM/n0lFacUlvjR2xttChcGITbiH84nblAfLibOWHB8sZLCBvQnvY60G2Wg=="), // [035] 2JMEIW 	Jan 28 2018 [week 04]
		_T ("qqAJYR4N819Bd8HoLo9YacxnceVmoZDxhU0ah2vjVmk66dTDG5AnDZrwj5DiJ2/FCConArvDB7pZ9MXdJ9WcUVt4E1OBx01bH65OhfER2kUb3+uwp6B+9oaNa34vh0sL3+Y1t8UlHe6y1p63VxkN13FdZqcgdDvAt2JIaZbOe/RTQWjBo2i087k0BuMa6Ypcm6WMfYOi9qPT1PXK99hrZt4A/9bfWLZREpFukoLRRAe9d1KprbaFbPFbYU7kFUUpfqwRQdaj+/wdLVrDNUk/1oYzN/4xQwbtXrXdiYt2rcAvd3SpMaz9iOWwUbb8N/5Znce6DBqfo26M6rfsXm5iGw=="), // [036] 36QGYR 	Feb 04 2018 [week 05]
		_T ("e5vl0XC1Uk6FF0+dkzcLKQfUy25xebYIMZA2oOOqhHuqDaGYdCx07tY2By6J4SQdphG9YW66HIpw1flZvPXiLB17HJ6BPEBHa6eBHQijyfP/SgWDQFLtuI5iACbxd52b7xS2Vl5OApHjcm+tZXBlveOQWHJB4RvcA/6JKDo4kccTlV6BgN/KyiVtdhlKW4XA80/Hf9S+iVdyo8JroH9A64JSNPLdxcHmUHJKyZlMKzZ4ksELaSvGT54zjzYwffMgHmOb8+n9SY1Mu4+C3WnGOASfwl1BVqNgwvYZuvvyrgTkHvbQILdPEZq8Umr/T0ARfiSEN1bWK7G62/dZb8kmXw=="), // [037] 7FA2PJ 	Feb 11 2018 [week 06]
		_T ("XSF/yGdk5AXP+2W88Cr76Yqezsu44rwQXZUzase3byBeEZH8aDoyXMPulMYgDkSsDFfkiJVIkncdqGqyd9A8d86e7XxmV3q5abnOTwipw/Io9exbzOHSuc67RXpc+tOTl7tdfcU0G4gNOOdUzshXGKv7V3iNTZUc++Hod4AHSjdceF8Y++hhzgFzeV7n0s+C6evQMxxAVwckwjZLPcCqqj6zd1JzbPBUq6pMjibPEQnzp4yeTfKnG0+CfZQUSLlQfRd+9gZaGXy4QQBCvHjxwpaMYDnL7iMzn6yTsDxcNRHtnBa2CvbnGT2rAS9TlUffUrt7wetkb4TvuYu00S6v3g=="), // [038] HT6WIJ 	Feb 18 2018 [week 07]
		_T ("Qtbw9m+HbvvChqaeT/dJWtebsRmauCcDgla+GQfPHxxe7P7YqHfXzuxkN39g+pwi4EIJc0K4FBXYC7kwASybZwAWChis8Or8IwXSn5UfAenzEOzVtzVomfS+dW6TND+lHMpDwMXGQ3YVXJ25mFdvTpbUzkGi1G0Jr9oylfr1WugXsA26RwjxQ32LcAQP0889nUStVO8cZBmahNTsnZrdWk4EQe9b0a10EE+jkjpKTvDKzUNTabvxYnBIPiOm6HYZ7R/wSvqJfEVHF3OomUnBorcXHqnH/j6Z6pn73Agvr6U4mwMIafy9GC8A+Mc2lYl2SGHAxe04QvldbhTeHaHZQg=="), // [039] LAVMGH 	Feb 25 2018 [week 08]
		_T ("vkALTFKCpjG7dVr+ePT4djszdRH0TRnjYhs2vkF8zwhG4fBeLNoqUWd705D9Xp0rwdMekzAnEftQCDwK8eUk1+xXBMV6k9n+DFZE4ovaYUaWp5bt/iCDHnuuWLKFOj8KEY88yiv4XrRNF4WWaW+1MKvOaWnQhj20sjnt+kITPkt0euZ3Xykb8Q6CaNSaFeY1itOi5gDTj50bHqT3F0TN0GbK+tZvlIIbZv+CWN2w5C26XJuUiewWRxNoFbVkuGpu7AxP1Ht2sBK2wgRiivzvc8WOmcnf3NiYLME3+RFQQDsou7/qINwR5cXdg6z/vKtB1iCUGnC7dt1MlyTkg/JRTw=="), // [040] 3PVCNJ 	Mar 04 2018 [week 09]
		_T ("GBscL8mjY/vuAKytx4ggoLLoEeHjoCjBrlpSD0RcaWx0oWP68RkwTR9zoga2i06l6bfFlQbCGmu6F7nMzVuXonADdbtvZawVCB2AytnRmDVerXjh3oz08s8n36ZJ9806izgUpD2IzHMNohvoQZYTuDDhiblo5s7COsRyunh6gq/G+Zi1JoZeCjnhlGUXykAZjW+HYJBJ6OlbqF4eMDcieVbAJjKZ1KR/+amN0Q0H8VIjJD4bvk35VR/cBREZRSGUVs/qPXoB864SPSCrCYIbzzjSMUnuwqsezD13fQcE1Z+0wg32S1D3t72kt9+q2JVwtMRhXaxqxI9O9QLkSu4uoQ=="), // [041] GBPKYL 	Mar 12 2018 [week 10]
		_T ("sltNvNpoStPk/cwpMoHDs5gawwq8y80krZ9M4ELc1cOrdISBQu0OmgjgLLmlgKQWtTKvCD1gNj5eIauLEMAGD5iw/ndg+su5+lipwKi0peN1aNdcJu+OkSSlOGY7yLYLIkqvszipz+YvmV0ZogrA1aHuyuynNsNEhtbpOCIr+8th0bIIld6H9cnMKseK2ySFa6BtrYwRA2vCeAtsxj6rOzKliQKXq9lMYoiOwU3O7kTvOAQb3TIcttnUoTfCjgYyvQU/MC6W4VyNDjVkH6XNPqMDsf6xd9UXkjkm6MUiOxs0ZZd/r1Tgyt1TPRsLRp/r8hnZwsVw4M10xT94qvPrKA=="), // [042] 2KOET5 	Mar 18 2018 [week 11]
		_T ("QTs6rFGekcTlXAAJjaLjkrj1yi1fmntv9gIm0e9YlFu4wJd0+fo7jQCJ8THrWm/eH4zFBwA+NjS7LQ97xEqT9fcwmqxLRHwbDFLZEysyRtxqvueX8InGKqWwUAOW9wqjVUV3efFE2H3KuSph4hFXrxp/WUYywhRdf+Z6kLixCsNZ2EDQEI5R55J5k+Ip4f3euHCMu7bwL7y7pJjFSws2BAtzUVltWpIozW9v4Sj3+MdwmHovSgSMHvmB1lx7O/aWEZzI3jORCYbMXmJ90Bb5IHQQxitbqgNEvMIyReAe1mIae0wZhTBU6pShZznqaJCHNehOuWhAy/gT0E089LI4ew=="), // [043] 2H64N3 	Mar 25 2018 [week 12]
		_T ("bxVOYxQ0FJIWTcWn1eRjqs9K7ib85qi5KLyuWzM1wt54+bfg83rYL+88700Y9fFnHblDT0r9jx5IfH9aVa3QrKPRMBqZISSyWTxJSXqemIU1EA1GozpjNkw78zX3yhXjhkoF2Ias8K6Hl+QY/jCHhg5tbL/8i9zLacQwh8HU4rjsL29dbZZl6RFYdGrlsJsHNm9ELvlwcTzvjlECyXPsKLemIngNis6WdZLzthYkXDweVuMcybHuhuSuu7aMCK//H8ytYy8o2thozkHsEMImuGo2Iz5gkiXgQA784BGUYhBlVSwdeUD10/uiuiYTNvJEZzOUOZzBowIaxj8+SIHgWg=="), // [044] FPUXL2 	Apr 01 2018 [week 13]
		_T ("ztofJV+J+Pi0/vStVCnG5iQ3EsAcRwByCbqv8duOZKJZxmx9bohuw09FFg9YKlo2GzILjQYsJ/Xmg4EtPH/L68FvS4sqDXBokE01R7Un4qdF6LopFkKLsHYjqKi7mdpsOZmOHjtK2EJzbMi3pEkmnqTOPDlLo/sGj54S7j9B31Ym1XSpLF/NpsfBTUVaqHUQHbTOMkTsiWD1PuG/DwWPrr0FO2TuKYr8AqHSAL5Fyz0Y5mKen1h7EKowLL2o8L1kWTyTsr58Glb1gI8YUEqhbq4shmLpZu7ZQf4K1gxkJZFtP9dcWRoLHaDnGrRLbY3XaUtpKjHc632QZcE7Y54A1A=="), // [045] 2F7D3E 	Apr 08 2018 [week 14]
		_T ("DIv9DcGIRv8GVktlpop2UsH3jl08gIrU7nQSFTgJi+HeUh5/XWlt7HOxd3taO9QK6VDIkpnVo3gPxOeR37ISoGd/88xUq01pPvo3P8oKDWzI2IKaAUx9HcBjVO1dzA8W0DsGKRa31tAjF+Q4N+GLyOImAnO2BYdHmn99yyma9oqS/4DCV/h/w1imo6jWgoDQ/B3bjdv6h6ef6MPbeXPh9hMGagDXVd/bssW9GJwz9dGPdeF2hVH+W/Sze4RMKwgj+GiawW2Pc9P8JN9D5RjsewPfFvzCtgiucgJidMsiYcrVnJVFoNcYJ4AMhPtn/6cPfPIKstsZLeMqlVoJR1HxKQ=="), // [046] EIQNRO 	Apr 15 2018 [week 15]
		_T ("LtmfNrHs9WJrjqIlCzCh0ZaHapf2AR8x8pv2cW9w//o6MKlsttIIgysfb2qY1QeWw2WtbbUWzmTrFQKEAhha81RKlEK6U3SwN3rbnNdXJvtN/FoGoN06ypSeTzUOhg5vvzs6eekYmwPZS8rAWMPux0D4yCEtHP5ZJVRRpJrcYXDJpU/c9hIzkQcXGut+6xvJfTwnaTZSml6mH+D+RPp11GSc8usNJ0gjRzA80QCVoFm2/AmMhWWiZppEpo978YeId7Q1iowCLAG2Fpa7K8ZDo5gCGmPCIkF15cLdjqE7mfN2GVEjHyvGYG4JOuagO/Rm5VccjTAZzDx+0jv3T7Y99g=="), // [047] QNOJDA 	Apr 22 2018 [week 16]
		_T ("gTCfZITKlOWuIZtuJ2tq2lOonw627v8REktPRjaE8FwS0AkU5a7f7TNVpuwB2e9N/djbwMNmDPfNeWo+ddzHckRJ3X61TtpYeUhGRhf/U8jdNY3T3AGkHE+yZfQzXw5dZXZ3TjiPE9TFOqsNza+jucjLOgptssWCS1hCeqij22DFLdx0Sm9n8D4LfND8LWPx73YskPSxnTmpQI5ybWlwF9yopVDI8ZxDQozWsNeks1pvEJFFXTqiM3gYzCNNsHKe0Fad9VgOH0r3MCAK+Ugsp3ekvCJkGNrN34SoIK1LTKYXVMnlFc3b+QghezCFpM6sDDsGMDKAUcU0FUnqCCAyXw=="), // [048] XVAD3W 	Apr 29 2018 [week 17]
		_T ("co6GmQFMLWCAYhzgBmg/u/28GqRQW93QlvBwdMX4DuBaeYf8odCapT0xjoNtGsvMUHvd9x2Yg0JUeavNOH4aDQUDkRLoEdfO4VjtsYu2rrjFgJtxYX1M+LY+2W4EhDn2gNCH3x9SV6Xgf9y9zZC/PHloZ2/eu5ewG4VMWfYomuIWdToHFzhmzWgCOjwkBnhLn7AuqRsu8ThWr87/Np49SfD/gUnYKThL2BUDf4TSux8bw39JEfsa9s7bhrYuFjgx86uBbWdablypf0YDRK5eJz3R0UUfIQeYersQOktyrpQyGpqAL19MYo8HXjOn4NP9RTEkgocqQJ3BkjDQkywGVg=="), // [049] L3ZH6Y 	May 06 2018 [week 18]
		_T ("PNyVY7BRxE5g3IfR6oiB5eBXzCsMUXuVMPmX0GO0JAox/vV0UWKQmKK++busANgqqpUqwzbauGOCdSYWnbVyjG4KbXEudQ1pagRiA1cecq5RIlLGuRdferVexykVlLen0KKPGI/k61DMz4EtUMrseidZ4OW3NbvpVdWn1MR47VfKgmS+XiBtvJUYunioRX4fAdvjj+SNZcn/k2tuSeCVmbaI2L+0mtCT3KeEMxWBN9NWyl2iey2nFh+Tiy2VdyhrxdgCHnyHEO8qWUyPMk0D2YpjbD/4OYL/BfM+NS6WbzQXEVOsY1KT9OkP/HZVNve3T2ntaKR7fyNflZLiNdWmOg=="), // [050] 7IUCDZ 	May 13 2018 [week 19]
		_T ("UTns3zNSnS107LWug+73jWe6CQ9QfOlK0dHzx6FAwswBAPUkCOQlBnN7Xx+yyP/Zm5fJA6bb+JoJa82q6qSdNKkkCuFEUpt4q84Dz9Wys4WQbkiBAfT9ZR/S45CpAGwb+Q/rx8JDVQE5AR/ATNemQt6WwoM9/tJ1dRm5aKhJtirkVECVQ7bel6GjnGM9TgctdZvtQqUatGbwUKDccV4YUM+i6+G/cpg77IY9OE31zBN1nBcDXLxHZbcvPuLZ++lDw94ct47hYWEe/1lgR46/Vtff+hJTKdP9C3InTwuvKPfJGUINY8i422ooygct7eZct8ZiwAe5b+eNGYNAPz8vlg=="), // [051] IEYNZV 	May 20 2018 [week 20]
		_T ("bKV4BSeo39JznwrscvVLKTIszJx4aKNUsQCRusqkALapKOvh1c/DOL4eATZkNKzTy8Pt46vYa4Y13REZ5XyD5kFAPdFZYG/dBfLnfTtICj4qDWgTlmiRnnp8TrZMkKluNxMkoN7n7UWYhQyEJD5tz1p6d5AkFug+o+NYS8cgy+zA0uhdTw9NvBYjIri4IKsyBo7tj/Saep4I/xTmhzd8dsU8BSm1SHsxMIHFsNEk3ai/pw6RkTw5l2pa6ycxz09yqWhkQoAXAUrHSq5JO5FNkFF11BO4mk/NH96gCVOIEWYIWY9rse/PBKDI9emDWAJqYeLv+AoGBxv7DZfpaulD3w=="), // [052] ISUV2H 	May 27 2018 [week 21]
		_T ("l2z8gnYhQ6S3W24+zsLqvj4WffgA/Sn/kwfXvY6kezeaXt86cpImrDShIfgLAWsyn+NgrmxmBImPQFjbiDMErO8HEhlL8jyY9W5H27B3Wgr1UGPaAt2OjzTccnDnbinb8TOTgafrYkRUc9+xGnl66hvk+Q5hu0D8y9S30z4ugjfeal3TS+gsYhOiDfhuK4h66sdO2wJZMW5l2cKOtGMJm/aZnL05AvlCy4J5o9s9NoRGGDMIgKiYfTOthSoZU6txT3UfYxzje84A2SseGNhGGkThEB+Nelxia9frxlhwWcNSYVIHQ34ZSO8R+oiGk2vhmAXefE0oMRdqrfIjcx6xHQ=="), // [053] 7JLET6 	Jun 03 2018 [week 22]
		_T ("rRwDEiXdTtKWZrk08aaKp4V7JSv2hLNauFard7vDJMuVfY0NScoQBXI23lFrR1WIkCIb8CEHKKh+RRnYfhx7IEuLWMKIS8XeZr9mVZGd8nF30GqD3ljkXyfzRW0E/4OnxlpRBa7iF8nPvIuHx51w0DQU3jecabPL5eXi5plo8oXa6MFLcyUI8WKobjG8V7KEbGnYPsmquAQMPNpN5Pw2uD6FNBK4381AfpQ4zSWP39Wk5sxcRT3oc7L7fyXzmVIEwS9Zjb7rvqMpd8SL1xxlte+WvMYCoafLl3sBtwdKcfD5RTJ2TsbrQBo4l53sW83SJVqHRUaA6Oc0BWlwvJDQ2w=="), // [054] ZXESD2 	Jun 10 2018 [week 23]
		_T ("T77IkQbSx/bR3UAzAZsdiMoM5m4rQ0XBaSTlGXVOINcvPM3sH9XrqwUcgLSu/y3VkhEQ3dSV7aKDoahIfl2mI2iX3326Voxhi1XfoVbu5x3GqWe6EROi053t6pSy1RkAKxU3scYubyK5LVDa19zsuIEiSuYuZWvKMEe/2/wCF2EO6FDfxOSBi6kJ4xKBRfzY541QXwHZmqE/xmlnaBiJWJff7sXSUnFve9OIGhHeszHhIQ7k/IyyVT3ou9fGDVsGeSRJgm6Yp3qppKk/Nl2yEGKnO1YvHcZEkESHo2JkiD/ddUU4N2GSsDfTCl10Bf/ioTB3N/feWlLWyTwQGbJemw=="), // [055] QNA7GM 	Jun 17 2018 [week 24]
		_T ("AGZ5Xs65ngl8XxniTun8AtP899+L6ziEezCHshOJ5ZII6i5lSIB44vYTAhCXfFgRT5g0qEINeoiigqP7M2DC/8yjUCSuX5UEY/UXhM4BfP2lOHBb4yzSecDcGEli73mmygXjDvR1NsHpjIdNcrVH6Mh17sV/L2vum3OOXV12cwdebl/vqUKkwj+VkvwN5L0DFC4AxiUccTjpMbHzml2fPO4ekhuouIYdYWe5y7aKCEYg9u3wg8D8ehZn0LzlD3K+EG4MdegTXFKW9gMmEQ6ylUJPQHIVan/9ai2qqxBMOewSsfuva5+o1X89yFk0je7zlgoPQIA1UH980BJsAx51ZQ=="), // [056] 6BLMRS 	Jun 24 2018 [week 25]
		_T ("uY6xMICJNkpYesYJPn9NWlEt3yn/4LjrBM5YknRxcs7sNYOQgafR+ImqylUuYh/lqhGSng6uZ9Ua0QPXP+lBYZ8ljjHlMu5swzZ8cZ0BxMwcz0IIwMar+AGPaYYpnVYyg3UCTIkV93l7jqKIik5NPHuI64sd7D9OL7MJBhlk/BPXxNRK/8r2KGyeE6ByQJjWRuHYJCFWwiM06apVhynLcKdIlEgC/xf+SwbHxCGmRAIEqhHGQy3iLXQYeVjgm28HjH0uDBEn3v4RiiEQv9AwPuo0OscCHSCjb1I7Da3x8PY3VfjAJNJ/jxdxqQEfw/exILUQKQgAcZHRzCKwOkTPEA=="), // [057] PU4AHQ 	Jul 01 2018 [week 26]
		_T ("npyOv6xN4HdRPCJmT4k53Y7FEMHtaMzETVP88g9VGfEbMptLWph+p+THPxM2R+64vVnGGuKWXOC4A76FP/4Sb5zVuCwHBoqcmsHiezNekEIGBAi2cfH73v5NSH3k5Y0edJTfI3pruVVFvH6UeIf1kVvVZqG8WDxapUfYkzgOJifOQDWCEn+iUmJW/+l8Gk/jHiF9oJnKPnJU8XxSdqBVGuQDAhCL25j/CICFtb2JC179FKvuUGwtwrnOmRdFvCsKTyHlBbBtd+MADUYJmH72t80J2I8w0Dc3nXkQcXXkNzhtINt6plxadgsLYinzOV0le4m97kXSZgUIwIX3ZzSYMQ=="), // [058] U5TEF4 	Jul 08 2018 [week 27]
		_T ("FcTGUZi8Vse64RPUS6fdrte19f801BqJoBm+X6Sg/bNm7rBgggGA0zlY6mFqbPV6tk1iV9S1IyEzbqs0lgOqrhNJdIkIWMzUkcaem/31cgDMqA6hQYRCQQy6/OsjUeKA5C83Wk5NKqCjLY9QYF08/U0H0Lm5AflcNFFdwiKbd0IVb2A8nAOrkzrkz0wn3dNN1n/aH2kv6iz3dAOhYmvWbjCX9ritqJxFX6MukAdt3RQUv+zOVc3eQryhDHB4Aev02n7OcIT/8kn/HSbb5pISjv17So7bDzLl5uZm86ia9Bd2vCd8XOa+759Z8nqR8pKkpelXbT3ke1ZYU6UYyt18+g=="), // [059] NSZHPR 	Jul 15 2018 [week 28]
		_T ("YnTmZjDOKihFxjaszpnKmL4X2qhpz2g4lcm0/MpFGw5ALrGBK/Nuhian/5OWjPNPqlaXpnGNUce9BxXtzoHswaFOXe2w7wnrswBNst42YqZf5RTqb8q6r67lva8KuC+RC+KM2082ouT6yB6v75xhv0j6tU699dNfyFirmqTwmJcZhNjIqYbSy/vEt7Zdg1dY3EjTDV6m8zoBUy8lzv15K++9x2xlcwG3QK8T3EzpOBaqhN3drz+dSxRvN0Q1wQX2SwBS54spKp9P87jBoDX2udbx0BI4aC8vG1oR+8woZDeynB2yRJpES1VH5UCXeHU9NM8BITCUa4yyVcpIYSU0NQ=="), // [060] XE45QM 	Jul 22 2018 [week 29]
		_T ("lhTATpKTcXgMBGcTod7pZ+6pr+vowCTYf8XGWUWhb2xi8MfusynelRySgNAuHqmima3UCRQloFdNXalfl9mInntBbz0wb1yOryvLeEWBjvg8lyx1C3/OZh63GANq9a1LVK8PxucAPq1SYA1umXofAmmG928lA89Xcwe3iOnatOo8iYiolL1IN9CMDUejJMhewpbod6PvJhtFI3u+DtwQ0wc5qGfcSFQHpaaO3zTEVDAtts85HBHi4e+g5YqwwIsigttoL46nNkuAaCr+iH5MMiON6xuT6OWC7LbL1ZYg2kwsjBCbj0cDDiTVGczb9e7oxSUhzacfcrhy5a3/fwGWWw=="), // [061] ID2VQC 	Jul 29 2018 [week 30]
		_T ("tSJLl3WmBljYvrIiSksz6jrNk3xHnYF57GeJX/44brq2c/hUwpjGPKsl/IQUpGZ9BeHAqJbQz+3eoLZ2RfBsb9Z2PlldejD8fXGJut5h7MK3QDtqGFFfIL3Qsnrw2VxAzaEH4wn6N0SGVFAcMvReoRAa8CiqELcRanvXLwImTldUbVCSsjnBb48S9yRE0oDWwIHHfBrCU26k4b8m+7G6Zo4xmekpM3vbDyFNYuTF6UG9I5fF/KPbymzMQLuFUxm/C98uVt8e6cbDcleP0g1X2p7dTEATFi2Rt/Ly+izcxk4Fz/r5zYSX9SsCXnc1W0HxJ7y7dOLHVqjj2ack1Bau1Q=="), // [062] I32C7J 	Aug 05 2018 [week 31]
		_T ("gHDSnh67JlFFgIiAKxID2S+VKrJ2HYdplNcvs5OU9gQpUVUaLdlLypdXjNEzIEXAEOq6YMfDCZLRuKyg8fWAcFw1cblJ8yx2TKni9z3EvVdJ980dTBKFCHxm/8jUOjNoxr6lsZONAJgwaVrSXeQ96uCCOzNJA+YiSIRXtVI8s44dppCZIl/G89qcWkJYiO7Ia8b1hZmtRsya5Ys/KoSaFgGYzEFt0xqetuuFZnpEV+DHqhAvbvWV0kZFMx7J/TUz1/7StsmUvoAtzNIeTBBl0hzK3kY9wY1T9jdhxSktmv+Je7UZKZA+rJZ7B8gwqhO3zNZ7GgRmKaRVgfATeMWfgg=="), // [063] WS7O2B 	Aug 12 2018 [week 32]
		_T ("DD+EPvVIYSAeeg5Eilpm89c61cko2eZPHGlSwvaNsVgH/SQ5FG66AZikW8iie2zraFZqDPZmN78l9BPN3LqHKdZR+0c7qq42P6iJ5wDyjSW1Lfps6shCsRMGOlM9YJQqwapNl5oPHCjEugFL6Svgss/lNUGDlx9aZSQSw/qilY5/PXhm/DvJlh4nWFg0qJLgXaHiEO0NhlG3D5dachIQjGelhv+2Bqp1sGd2U6LkrO+mf2svv65Nv0wV2jrfAZY/yiAwTMi/dOhzjB3Z2appr964Ad9cfKci8KKYDn+0TejHhmTMmIRDdMbxHiDbZsn9KcZNxOkldGl+cyuP3OukDw=="), // [064] C3FVEB 	Aug 19 2018 [week 33]
		_T ("tlvZXeW/gvKG8knHWJauQtXWlWblTNsHI99vQ2VfGLgTVQ906FBEvY6hwXzxMEqKGVWDXP9y6cCMFAKeL+9nfMH1YczNa1P2rs/1mMdETmGjuIE8PkveObg3+p7vHHv3Znx0rY8XiERQVqBJE4wVP4/ErimCjht5SsjkL7Gh7DV7I4EpLffKZmpaDDaqqTvwqCeTqwk0KgL4oomSuHVF7bwblL1Pgd2XBtMR2o6SRL+p87o4rkFiwkZq8fcNT6LrT71sL8uDxPv7KkGBmQlEYDSCteYDZ2jD/J8QZQt798xH42K25warMWAVmSw0If2xKURRoXANTHwgWJMNSuUL+g=="), // [065] NVICA4 	Aug 26 2018 [week 34]
		_T ("PztWErjfagwcRlYzFIvLFxNuvdaUN1+wYUW/kcHukw/3qCVGNyW8fz9dQ5dHoSDDWN1TNEF774+O2r/pBixCCQspLubA0skMCNu4zpievYhQ1g8yXUX7uN4ND77xBJjDcEtAR4MT6oORrXF89H48QiIGmGSt06QHQNJNdIvd8552I3bMUsVN2HvJaSxHNmM8tlcmW8CBJiXQ32O/4dZk41uu0D7jgzOxW6Ki6Cl+BRTmWPm9qY+Ua4f61TGxxStNUVQGWjBT8AeeKaRxddE1pP16WArFTAAuBP8OotCAICxk//nFMPkcQaKRIek+q2R2r39mcA87k6Jh126eK3jGkA=="), // [066] 7NMQAF 	Sep 02 2018 [week 35]
		_T ("VV9zTq4nNzdoX2oK0frh3cmUQnb1N/9gMaszrH2AgFcEl6hUW7wQ4Tx+8bGd8VkxEEZ7S7Zb5NoghEG8qHRX29QXpwgoj6QW7AhsVTXRzsXl3Hk/88E9Pq0Fx8ZrasB5A3QKIA5WBNgwg5+YATDROzJpsCu1hxl3SO2Kl+9Fc8A0gGXliyXZPV91SUXVW9m7bm1E9kJmQGpUtZ1e0FwkpZTsPrRW5y+KVBcjwrU9EC192i0dxAWEAVpZPm9urhtgHKAAL7wfuUNDLU2Lhq9OhSqaKJ4oCOZvndBWq+GCoxAe+a0oaLrdpAcN1VKWFisIFwfdk/4R8pKbd7E4VFl1tg=="), // [067] WPHKUM 	Sep 09 2018 [week 36]
		_T ("xXc/4lf9ELe4cK+PKm52X1VAIXvGA3WAvzXe4+53YEKQCCeAjjNkLHgK2XOOuwqJN1iw7ORBXWL+jvoix+SuJo+hWUbQWD5UAsOuKyQzX2hnFWaQxSQA7loTY2FSYle0/QK/b8+sGMr02r+fkat8zsZg9WKCZL9mVYwv74bwxH0Qm9IqPbL/cUsOv0u/SO5cOyRjuAf14ktmy08/jStKsgx/U09lwuxvPh2u4vfbWBSz56yC6LQ+qPsHiPZiX2Jr2w2yqQdWoncVIoh9QkCgokFrqApGkGg1kaXE37SLVdpOgGc0KfejB34nH6RUOIivX+wQfTTkvpYB1mZhSA5YyA=="), // [068] FQ75VJ 	Sep 16 2018 [week 37]
		_T ("zXyaaHGzsz8DAe3JBsq9HBuztQaM1VDwVL5qW8UDoLdxOHeYHjU3nd4HVl2l9F/qD3PKUjy5q+b9Ok9izR5gkYuU/yjEpz9IX9edcPmRUKmsrr/xK6zh+nRKXu6CZWMTth4J+hYWDZ2FvFJ9CF3fT1+nMVyLH4XVIKX+uBbuysxCKETgIK2h3FLxu9PNy18bjQ3+3B6Q7fCNYYuHwg5cR6XshMbfsXqX8KlIeMC/jUS+kMaTb5NSFSjBRXNPjO9Hr3sI5/+YHtM6/YMcTkR8zauq5PKA5BFLl1r6YqsPsTQEPEgPBE2cLYn6w46uPgUdRBlp2O/ToGec4Tup0U+daw=="), // [069] SYPFJV 	Sep 23 2018 [week 38]
		_T ("UBazsGfWLc14+gQ8s/1sL1lS+1Y/PMN9IfQcYuUm9pHxxPZSi+FSxIceQp4FQmw58NlFPjUAZkUarG6X4vaoNZr/RXHaP790xuNcrJHue30Y4wxwxEj0XIEeVcwyhTLFk/BlvaKwEeqUx3tqeROdyexsWCQu8bQLdr6F+8diD2FxLuHaqke+GyD5Dg/Cshw3Ba3DILSMVxLYU2Mf72JyWEjJTMOZdie/PD2MGnJVfGn6IzAD1Ek+y1EQN4N9H14a4wiEcrRfvD5F9HafnuQzyFFURdTUGsxIYt/JqTjSZqJ70mtebSY2Ci6jIxktVlCfwR+s5pRh04E33BvCdOyz3Q=="), // [070] WOJ5TF 	Sep 30 2018 [week 39]
		_T ("UIlbLuqa+EXwI+MZtxdmUcFMSKlDIJGeqWPuOqy4sJ+iBUfCaVHg7EkXOOG9pF5tMHmaYO4mD7dwtpMVRGWq0i4tDGNFPNvHt+7NcNClpnCp78eRQGApMAIkzi8nIk+rrKB+bFeHytaSkQRbr3ErF+vDbkz79j//4J6y3yUOegjtWJMqsQ7u4XJyapS40M92v3ZWv5+yyNtM801WLsR//sWZ+suQyaGpPh/o1jWxOpRgpugGDdEJbLj+sIk83ioRvO+Jwi71j0ORz0lq45A9NkvihytMrVrZF0IJnhuXLibgiym3ME+8tdEX+rjkocjOSzt1iyP71/MFGF/IMGacBw=="), // [071] MAKR6Y 	Oct 07 2018 [week 40]
		_T ("IUIrMzO8vgojKaVDWn0SPYDNH9P/g+SWoxUUX5ZHE4mY4/wOFpZIJwJHDrGyvBd+/VUonq4x0swG0opcXfC8W4duPKfqZ2xIjVQAgWmQn9GyCEO/U0Q4XQwxW3HHerIcusiEgXYoXejf2Akjp6YsDU49iBQyeG8qfDoVaOkidc6K4tQWqYLpcPCxm1B6+CD0ljqcY3kATnNC16AYvgcqVX7xZTB0s17V++aZvv8q/fvRaQnfY44zRJk8k6sgclKgLBKyHuExF0FmtVb+tmBq6RMHaqbHhO6n5rtcQ2D8enH0U5mgwNIADioZu9phj5y5jfejMvL3nyj+8fs6TR74PQ=="), // [072] XL64VF 	Oct 14 2018 [week 41]
		_T ("XG2ZLLVJL0BcqSr2C7dlAkRUH1YeNR9AINnr4pjzGrT63vtg/Hg40H6y2ZHRJuLuWXhycOvSXqazVdLiSXftbbOTRh26uJX4IObBWP6iROK7u6gJjacxLusJc3/rHo85JXBhZaVAlzYNuwB5aECwDJ/H1ifotymDfvxs+p/v/95qPaxn30hsOUNobcGUQPdl5rOEGgyZcwGYKE/k83RPTH+M7mKEMAmu1h6svQsoW8VVmPCWDGSVDjvjSxZ+qRpoBu1CeaoP4W0tLX5uh4Hup3umwn0SW/blYLxjxGn+CzdeIzBvbOLz9W4ebuGHcf/LmjgGdyNDLgFo8TcjrLEAIw=="), // [073] BUTJCF 	Oct 21 2018 [week 42]
		_T ("RNlhBcO0qbud3yOnOn/m50MyOCkRx+nljXRVfk00LOhK697EL7IMVRkyh1Rn6kurJ3ecemM9/dgRcQWglsdX8c0MhRyX1GgkNRPuDVIH0JVid0VGGGJLZftYOdTOARnMh1+49OFyxV0cSmTDdsVI95KNjoOaihRKZYgf6Hk5FVj5IUoLji8HNNYG1JwPD80klmuqhZ8sb718Ae5ArVbxzCRjsx7U/3X+fdmgjWGxVOO0vPVsQzigJZWV8Qgc1q5oCfWj/9sBGQVfpFahPC2S1Ogc3V5lPJJWIeKiNx7y6I94j5Z77Cj35v1EPD7mMpNKhHEfMDtxey680qF0Y28xOQ=="), // [074] AZCNYO 	Oct 28 2018 [week 43]
		_T ("bz6MH6lROvF5MyrIfe0mig9Y+awUj4f7+KTMA5gqLkNwCTIHRdRsuF6S9WsfeqObJerUlFu9bkSriSKdGy/ph7s82eH0PkEInx7pjQAYPBOW1nmr2GRh6BcaA0XXod4jnu50HKWq9idSCStu5emvdQBN1VAFGk6xo+AY5t8Vvw+ooZHMoAKTcFswjF2JKM1Y0g2nvuHStVA8iIEwqQa+YvhAKxTSRHp6GmRHJe8THleRst3C6mU8FnwVXfJj0BkS/jCDOIsp8vCCOhUqm28f01OCSxWeE6D56bNqjHttPnB9YOJTnxs0E+zDC7VbHxe9zJYU7BmYrz9MoPJ1GUV0Tw=="), // [075] NX2RA5 	Nov 04 2018 [week 44]
		_T ("UR//ZOD/tfCiZo68hgDcX37eX+NPMl8MiG5xvdQ9tT3DhTwcFznb/dYZeAHpP7069uneBfgJQ+L0/56bL5CfK4+uDH+GYD8dmPNt6CWXFQ1YfKuTixEK9TrQn7Zxp9HC8OS5jVDWvVy/HmkIFy0V9eMfIfL4YJoc8OsGTCLB5bhcMrz8XxBW42HlRBY6qk6c0RzygPZaDlX92thirw31fYRWxNY9gMipSxa/9widAErnY6ttawxShthC5y//ERxCWCsp8bu3I9G9y5O3E2wMIN9a7TNWfewwgCp14hXJKyLT1kBt4bToKn+i5G780KfadiBJklUdBwwKEgFYRvna+g=="), // [076] FNIY2C 	Nov 11 2018 [week 45]
		_T ("Pt7NGi12GVdVJHLfz6eF0fMME/HCMZi7FZ7XM00+P0uLkGL3QKFhSLuv9hj+HMhUjA5ru1qTJBc2xXb5ImbbeKma8b5uDeU+20lMvd54x1iBy5ffqbbe3qTFzvfAOZ01lGf7VZfDxlXVjMD1WQJlUhe+OGvjALvpG09rScnonQzQQXVEQ8+WlYjol4IlWiu7nxF+9cfdECwzC6kieaB4i0b022+IKRmNS7u5YlI+LtbN7N+gQ0RFH+q4a1nh90I+Ta+eDKml1+ZSeF3iJv64ejUhA6Ba9WJgfiFF+utvpD8epgLqnvfQUZPLW96pXBauPS1arnsyic1lKJMHiF7O2Q=="), // [077] XNUF5H 	Nov 18 2018 [week 46]
		_T ("W3nF0vik+ntPZnOzTIndTc4WRkUK6yRHxzHn4WDfUbROuEdcxXXmidhCALfcaZLgfU38zgDvHkCy6eAMddI+mLkjB68sxWu3Ap2ZKirAqhJRg2cOetTQlqg1qCjvDvNAFaq/7yv1TQ2Sn//qjv618b16/pzql2yBGTudW8Ux6UNAhhW3kbwpEBoMDFBfQ6eNNvwn7PHP2zR/UzDzkx7BFAfG9JvQCpemL13p0yH0VEIcymaqF8JYYQH8TFvHCNFFVIkhwLvawgrRJlLPndslVxsJPpBbAtbeXCf+gdH8cNSI7Sno047lCNIEeT9sf22fGwuli0fmcG3tnP0OgvcByQ=="), // [078] E4YFCW 	Nov 25 2018 [week 47]
		_T ("CXzTclSjqLRL8YJKxmyP1K/5T5szcuPkG41AL2UreHVSefI7DX4cS5c66oWExGpVOXcRult6ePW+H920Xp9JnHjZT7eC100YYAKv0iSUvGc5RElDQaY1p1TcJV915u0whhNddxnF1G9KwSEeagnf25rb7YhYZqcuuM+IS9c0kj9bSgy9r1bdkdrXm210XaxVKIu0lM8xH1StSA1XKopjwzmQi+94IwsSqyL4TgwWQU/EvjFMzbcO/5IY3YSLSYp2XKMD5WgLse9Vb3pcrSVfjr/lT1HrnKkF3UkbeeU1Htta3je9wXJK6upUmfGiXPeBuQr7uT8TGDOaMZV0Mfc6+Q=="), // [079] 6MFUD7 	Dec 02 2018 [week 48]
		_T ("CTbbkUwae8TzylLQ0Q/oj+AeMtiuNH/VS98UdsVfd2QjM7OvbO5UlV4FMrixo2RD0uosnIUXXWz7js5MiA14ibpXsDfeA0TJ2PCUfGa0BYTz76Fc7l+SzUISz6HEJyYFo1E+AjectASLDGxBJBqij9nRNGGArWC5YgOdoUFIRFYp1yuG7yN47U65cO4TLIyMvrRwiINhc2sO1mprhAd/P3U9tEtSBYMrYjx+CqDkXA7Zl0qbiXmbSEKXXlZLcoTXizgctWw09zpba6eoGY2ANlV1yY1piVMWLDtwtU5xArkH7XpL4IN7ffikFRiP3Isd+BhEubaQnh1+L0lgVfOX1w=="), // [080] PMGRE2 	Dec 09 2018 [week 49]
		_T ("JopH9wbPEdE2MxcKeUIGY2QWFNOkbegmqbusb9RRKY1bUzZoGhFmZuPjUXL1lcnac0mG50cIlHrZkHitl64A4aIbcuk6Bf9zXJGTllJ6H1jDDUqWff2CcLGQvyFSahZR2KIV7Ny6vh9968VRzoEEHRwTWKnESA5VG4tXrPmS6Cu3tmNMLG9uFb2LOl5ZYgC+71aKsJcy7vxnpQzuRy1V3oGwXeJKon7lmnls93g+dQAS7o6512O2PdeTRdvKmllbYyKs6TAYrA8cWwmGN1a9rPX3wUiIv56/1TzkMiz6fFk7i3iotkt96s9WF5hT+J+j0iutmlBRvLSB3V5gj6Ldzw=="), // [081] FMY5DG 	Dec 16 2018 [week 50]
		_T ("BxRmheZoXLvph8D/oobpcSfwaIpBowCb18otKS+C9mh4aQFV+m1FvBfRR9sUmCxhnOax/OxcH/6IfW5GV2fy6I3ptd7GQgE92JqrFHkERyxEZ0gzB4nBHGV0q6eq6g64PeJXJa5fsF/Ftlm+TnyyR7G0surDDRiHhMQ/f3Sa/TgSdqepL2CSHeyywdHHAEwBr+YoJOgrce3JDxZ0+SnUYTQSdn8/E/16VRuvb39APXyLkwqHk4VGAfOXAjpF4B6anOECGcOMznGiWoUaUCPC5rt2en0VV/hKOJo3Y7pKuX3j4X25IXEHis0Re2TZQ9U9H9ruutEaVyZbyXSy44KaTw=="), // [082] 43REBM 	Dec 23 2018 [week 51]
		_T ("BXbywXWMuohYyUVfAfq+84r0XZkJqTb9fZnh1OeXc46fqU4WIGZCab3caaVS3jqNtQ9q8+5euZXP6mytSyKRpN23cMLIqSVPXfqxoLruHAlYUd0A2y1boEsNhpgURdUkzRXykbB8qIwfhnw7xs7oVQ+Zk4h14FaWtPBZBLF9dVRerpjehfTqidQZoi8p5UFkpz6FXI42KfPSF/0eRixMJVY8rt3oUFKTKCCHHhAMg3iQAthxU3g69UsSFbw6B4jF1QveHdytbEVg9y3SwZW5yexWi6TOsGAFx9NOnVu4zSpY3V/VgEPvt88SSIcABpdLXAReR52ICWWg0RkQABfFBQ=="), // [083] 5IZ43N 	Dec 30 2018 [week 52]
		_T ("e2pi6J0q7hOVCRXp5XkHnDlnADBKZlrvAnF6H4PDoAlEMuAu8KMqI/Bb9m/WlHmeJl9faamgJggjoURDQvTvBefPrFNtdlRD1nQvByakSXV7y8LuHHbn0Co7ZupJk8GjudD9UAHk89H0L0slwHBEp2DYGE2cLbhbDb/I2aL+S0hVMm4sRrJx+uUsrTM4JnzKMbw4MQnM8y1EJktoOe4qtFeLpfr8zRdfedxcBtHrUL7OQs3j1pTJ+mAr7Dael+KHDoiIjWgCWUJaTbSsH8cn/sreawcK4Vwk9//dxwPihDMmmsTwxXqRAZOzXlwRQ3tgcZKsEMKhuvoTdJFh9bjsNQ=="), // [084] KVFDRJ 	Jan 01 2019 [week 00]
		_T ("bduNI8OHCmXVKGg29EVJUMn/pKP1JtulIJ5h5Xapyfz3M3m+rLNTIuMqqXrG+uT2fQ9VYZo2l/52d17lJFPR4Ys4Fk4YMsTzfYrmBUMrbKieVPkUOFUcuQKJrdSL7rJZwwthvJOtd1Ee54abDQ6vrkbf4FQRbL6F/lf/AZpvvwSjyBqnFo6SangsnCGieueZ30cwajK0YIFh0JtUGdAHdL6lRkRz3Aj4bMbMIDjAkhtIgh1FxPSdAu2Oo9fQ7zA/hejrTqRoh4uYCMKq1aTWX4sIlO39Iq3t5QLUXj2fiFFqUQsrdMHJUSJpB0R+mpgR0UOb1xzIvf+T/SGLxSDuSA=="), // [085] ROKUBW 	Jan 06 2019 [week 01]
		_T ("h2hKeCQe0x6HzhtDirefTi/6UIES8eWV7Naa/AlXAX+aIoF+6raeDfnnXtPH4N3MkcUArnbNh/4CnGuVlbaCJd7vMVqB+FT0JbsatuZ+S0HqqLxDOOQuYDMUMbxsKgbQELI+0n/77O1B5DVkYjISDPqBE4GRjK6GQm2e98sz1vg2GU/DYDEGB4ukLsKT1LmFZShN/UjUGRDbQ7gy3eYs60qs0XG7XhXR1nEdkm8zajDNnkebJz8YUQcujtIiEJFFfisPMab+QBz6S15VBtr95mj8RrK99UeA2VmSGXD6vFkbtpqqXIdfyptBVX/hlsu4dJ9d0dV4pM3uQgI7ysYdgw=="), // [086] WQH6P7 	Jan 13 2019 [week 02]
		_T ("ZIs3r5OE8i5FMPBJVmzhz21QhpirDYBF5/gsiuDRFuDg/AoQb2NrCfqufVgD+UN8kviepHRcrdZyeJUfh4RY0Nhij9xU24fP2wu1wKbWKBVZpBKFfWBfF9XTYOh/pbVuc8bLUtPhW8b1xkvA5EZmGosrHjGyhb1QI2PQj2TLwkiBQnxmMhS/d+han59n/j1ZOGhzzZHfq/lNItG+W29+/vitucZjRrRcfvc35a+v/lf0rppkle03H5hfATAt76elQ0ex3FKPAub1IR1D6Qokmqm94mgzcT6KD1Ehj/XfHfIXTMUAMM9/eEKcRX2jW6Oj4OJzk0QoBjJUcbM+eo6hEg=="), // [087] XRBIA6 	Jan 20 2019 [week 03]
		_T ("v8Nj1mGjRmvVfGjd+ClWY0OhMphFbou7CzhCSJRhdeDALaHYYCnalzyaoBWtPV8O5vtZRfUWI4l0q+sSSMXwIXVuK9BRTbNDSEu1H+U5M/wHe8YIKdvJv0Zr+NJjCVwGEdl0+3l2K9KMMPzcqlFDSz2cVdVjpanPLj+KpvVFD1inGYZzczEj4zSeuG9xedvNa4gt6OG9RI0WA95Q9JKUrSw2gV8QTk2r07u/BMNDbzhXBuMWMrWXUGyBSIq+I5sYndZHSR020zY5FHDgC+MHImigzNvWfUQJmEbsLFalsIkd+uggMBUmwTQrKRAGYsULs2t6YojREwVLw4w20NWsbA=="), // [088] 6S4ZVH 	Jan 27 2019 [week 04]
		_T ("QY9xh2fojjJvr8MUy+XgukSFnWFtidrwN1NG/CyOEJy3B4BgWsEG5cYsmy+USZdsT30dU2senjTRms6hTudOB84nBbtbaBeIDkFYN6b38L7P0fsVuXA6qy4bzFf9hyp75T+g2hvHPWN5GKg2oShk1buaHyyg3fDmSWUOF2I5uBrGnnWkPuLsbg9QOA8DG/aBKlLEtWVj+q7qzitMzL92KUDJcG/Ng1sgcRwDKBda2xV/xM2LCwSCXJBA+dJszngITQLOTNEgreiyJPYa9mGkz+eunZmAD7GVIkmKQ6oH+KLIw44EHWW8Wn9TEJgDB4rKvFSNoF3XsV8zP4hH+19rZQ=="), // [089] VYW2MX 	Feb 03 2019 [week 05]
		_T ("zuqIUyYnfrT+tI0JdPijMuIFNFA0OXNbnwjJLlJwTV7J9dyCKAuUVfAaxPaBKa0UIKiGlrgx/8zzj29NkKfUwZOA7D5w26b+chF9Rph9rCryk27zNiyDF48x/LxbZvGKBG95WuYF8tHYqyatnpvu9N2RVlLhgJ0V4iWL45De0iVFmALDfZATl27z/lbs+3tbZkGgzjGz4Qf8Ayai4z5PP+yiWhYDH7tE8kwok9v+scyMj2zmQFL741ENjZbe94DUF41VKW0BVThtYn/uCDUyQF+ruxgz12vKZshiyoQbsjnsvc2/RTR7ljEEoZVPtKqM1z8INEV4JAi7c4PY4zxDBw=="), // [090] V3XNPU 	Feb 10 2019 [week 06]
		_T ("mt20C5bSwF10OUu/ilUuhiEnvKN3XQ2mSL3m/oQsA7KPImFRxHu8lk32G3rtdfWh4A2WSDwtWgq4KMmuXsvt62bmyJpso5xV3Bx+ZajgkymBOjNHUhK3ZgzaF6M4KLoy2AH1R5qw9A0+HND9/ZXnfglcu35B7e5UcczNVmLHsy3qQCcqeUVgAL2jJCGTB+OFLkA4+qBaW029RcqFxXQI84cgJj3EBkXwjeGCFAi7Lg3g0xo+r6zArFXPw+L62GWhhuau+Sp5RBnLh/8Lu2Yq5O091J53M0sETiktEiWxcOtjGkxzAOT5hP3F5PoIc5ocrafOWLWCfQ/vTAk5XlTz5A=="), // [091] RIX6SP 	Feb 17 2019 [week 07]
		_T ("MqhISR0gSMaAgChp2KWpQ/i7uwoZK91USUivgweMyN8jEpzy73vgLeLiRm2/M+WWZGHja/l64b6hzJ/T8Ui79kRt35MXNK5Q10l1z0K+0LOpDauFqTOpQnHm6ABiBnzhIwGHfdAToyoBUtYvQkdNVdof33fZZGxGj8/ZHOVNv7kttqiaZez10uAYkXCxi2NziyvaYjqEN62JZXnulNp7RC+xhEn4AsuuDvHEXT1/I+ktL09TKlRiG3giwZVmz1l+EV2kpd+/SlywPnDZhqcgqYOQB8nNKoSCvq4IDcRhkEL5QICATIh5w6AkQy+rkiP6/swTNTEeAHzNRkA+6mj0UQ=="), // [092] IKVCLZ 	Feb 24 2019 [week 08]
		_T ("pFd2y3u9Zc5//P9NqhNQOvIPAc+oSbCTzobKnqv5CTFCPpoWCWGcjN7TbY/39ZaoKqym67WFgOqm3sa+TOQgJe1XY3b7KyFiGlwZSMX80zahv89rv34MWbW7SK65KWCUnN7Od3IbB07WFxf6KsfBkFHD10ymxTm45UkNGl5a7RQT66kuKNcEWO/QRO2Pa8uYuj97T7gn/sgyw2wLoswAvyTdSxntKcAumphUWMsRPpzn/Dh013Xec8gN2EwhhOR0PZgch0htb2hzXCB5+kLvLuw0Ud/LuL7GVpcvWWRlouYvwPhF5W0xpzqMFJLpMDzR8sVFfyLFK77MduYDQECpdQ=="), // [093] 5IKZNQ 	Mar 03 2019 [week 09]
		_T ("U55+2qs8OyBxGWxDV8VedyuF+fZ+riZbBLkaDRrlj2WPfk3yAP+8OQ6IQ0xKQR65ylYoyMUqSK7Zb6zY/5oTwjaHS36kwdH2d+0Pl1V1369ChqoJMJPxXh9EQwEqn1CSaRU+HSxSzuER6WL0qcp/Km5NMvNg1dcZuNNWvLtV2B6bPbKdZHtsf7bavnziuFZ6ObRsH24L0saMBS/D97fvy9H7rx30mTdqnjcMnA6mrTtAgIywXFqlfGx/5E/h7Dvk0pQnA0zBoaKD8NtiVooAyCBIAYK55R8mjX1Jikt+B0A1+PcfcZ4lrwHUDG+dSSavFrGiZebPA9zUskMt4a/9UA=="), // [094] P5YRDJ 	Mar 11 2019 [week 10]
		_T ("Z61Rt8ZQrVZmLAubzWVf8btgTRQZgFjI/bznUD1EUC6HqNkAspNKaPF4xyO3LBOuPdxZxDRO0RKuJg1ognJg7t+erNH/tyTrkxWdmTj+yAOSmZxqBFM/RmBnfJA9MXud6TRQRu2Wrmxh9lyBhDuLYYUpDxmuITh1JlaY8RJKxJ5FYg2JhyZC093h9Esoi48GZsGjYh58my5ez5OKGnObrmtUgtKIWLlAYUB8Ja093kwQsXbLX0pCabRyweZMuWjsEuha0GdqIDkjY4EcOEe7k8tulxwLYoApnrOqwglhTmO5VxY3nySh23T1FH0uhEpiNCjlysF++XX3ebraxchchw=="), // [095] CXIK5M 	Mar 17 2019 [week 11]
		_T ("CFrwZ2xMIz5uGQfazsWtsSzDy+4xC5pyVbePzM0lgmDcuy9u+p0TSN6Vtpc96LKFVY9b95V7nCZ7yywFq28SmLfDMVb+G4go27arBeA+NJdLD6Igc7srm9dcdLQ4N0yCVpOSqUCH0GxY6geoeEMkAWDwUqKTfJMrCTePm0ETaPywzx+gRPxbtnv3Q78N32OXxWxSIFw7zAdmVwLSOIeydgXZBj+I0lGMH+acmxVrsnEovsRNTN5QJiUuQiu2CctoYeFuoYFbLcWxVVwM6vpuc7W4tS6x+1ADIqQ/mlnFErKA0kanaiKIUbkpagSaoR+uvYU3bmV2IJhXSTFGcokB/A=="), // [096] X5S7LM 	Mar 24 2019 [week 12]
		_T ("TtB6XLsV3giwoGf6CcgXiNiAT/vVF+Yzpb/V50ZkS658QHCG4qjVoTimmr2udkxryB116657Y60OWCGo+nSkHaRlMzuqTFbmkNlwGd7TTE797FTEEHjqkOQ1MxS95qHpMTSuDk9rqq5S8sjGt4BpCLRxnFGl+t5RmKCbMmjsxhOL6cphu57ng1X6OoWzxs1XTeT5GbxeV22GublnC/pRsJhNPpcIv3kEM//WUV4H/PFnHI7POKOhpAE7O0e37elZNDgEBlLzgKzPh68K8AIWTWS2/v5VOmtr3G8/w0BGgoEDl9MAVz92iZxYwvDwQ/2Oxb55o7xC3urLp/+OofIUqA=="), // [097] LSK5AR 	Mar 31 2019 [week 13]
		_T ("MyeP72FiT87WIKXcrDy1p3IIjQCFuGr2EJFBwqaFfpsVLNSrPaGzLCJgPn3QlTB84BHE1m0vQDUPDOftPcmvAAivGk5EIlR6R0pfSTY6G1wQITEKc21ImrMnzockT72Qc4CWLGlHNQ2i8VIN78miVF8+FfDjc69jMTZmxToODco8XND9XdkEvOd62+TQ7/+7n2u12OjaTHbpLe1a9VJAoS92sZ/GQw6bJepzJrk+t4kyYTmEULhZnQK86v7Yp6zK2sO+Q0ZBtED28f2cMkDI3hsVubNVmPm5L6txGGVGhRI0eUp0FAllOryOSGYDosRoApGGVT47yV7DKqn9iMcIZQ=="), // [098] OWXTJC 	Apr 07 2019 [week 14]
		_T ("Bo3gs5msJ4OCwpqvTiF+IVNlmJbB1aKaAhhzraz7cCxt4ZIl28BKyS7tnG+/t0TbzGORN3dPWxGAN+TRp22aR5szveV/ngxoy7O5LP2U4VCx0BYrq33gxaxRQhY7SSiYFViNtLeYUYY4mBlTxbPhhBhrUDz6YTQo+dQ5mvVthKT1VDi3cGyUK/epIGvcVFE0Ev4j4zMB9M3jNsoeIBv5iBqhOEUsQbX/v2NjqsMzusHely+0k5/iyJQ9LlRX8Hv7319DknXvbx+aCrTACLdBx0plvjO/oLvsXRldffCJ52z9R3ekuh3xynN5DxW++J4TABKi4GZNy+Fd6CCYJJGQww=="), // [099] VLPQSO 	Apr 14 2019 [week 15]
		_T ("aaC5gH/+6aNFa62ruRD0Uv62NgWgMa90vKubn5JTYLZESAKcj+8Q+huD2UPLKljPxu8lv6noPk7Sl74JLDoABL3uele2WbaVjY8ODeTFNyDe6RUW3eHH6Ho53ucw6YgJIsfJ71R+9Au9xSUAY55XJtxw6G7Of3tLKp3X9JT9AZIkj+vxIgObw+6DtDVhI8ZIp+ifSHj0IdqX8bWkagEYnZC4I9h8dkwkTfw4P2F56kbkRX2taoJBLSxd322p+yYHxZvCZzFu3pFYFzApOaq0CNePhAvC7LJZEmwIYzGLTxz5YlTNqyWP0XbVJJaWDKBDNdsygk6L3AZOIEmHYHdRMw=="), // [100] QFBAUC 	Apr 21 2019 [week 16]
		_T ("ztScUQWGfwiirFTK1Tj6y8O5jjW/jp3f8M396boNnXF4QfGF8E3pIlXHcEZZoHdgTKKGznPLGzlYUX/i66VNliuh1tU2ubLX80lJOPcWYIH/axqCg8/jioBT+f+VSp5GGKdZV1iVhbZF6qzmo/Ddz8DIfLQqNI4+XwELpFmSp9rHYYFDtDUPaCkkc0aBBLn9xONn/E3tyVPp84wQEntgAolLCSIKC4thK6oNepYFWEzHzbW1FvPxeXu9OxUsP/dVNuubNidHgfZNHLqvbfOxHjcbZz8k+aF/z9NbNRsHwUTUeI4ain8tpovrgjsLrp55s/VVfRabrna30Rala3jUjQ=="), // [101] V73ECM 	Apr 28 2019 [week 17]
		_T ("KPgZbJXAIv0q5+zC8FGr3KGvq7U+YMZqjrf/7eRz8eJFcSp6USxAMIopCjxVVZ3H9eRRN7lSzQPCS/E7VG9gV2DPdkcT/T6RCtrNHwcYyLNZ7vZwjPdRrYzBMb13v6GwyFDV4JKoybzqGhhvUjpnG+ulNBzfM5zAjgN9+/hdiS1gCuET0YjK5UWoTlhm132KbCswE7GP3EtLca/ktLqIGVSiPEZM+qsuo5sAfVYBJOHytK7EO+FhwzgF0QbvVYtw1xeMFyO5YqN1WhVy+zOjUUmBzrTag35cLjuw2UyVJdrV/mCzAupY+aaKcpsowe5Zxeed764/KD4fC2yB5lAXoA=="), // [102] SNPA3Z 	May 05 2019 [week 18]
		_T ("muLpW6ndVuHc8fR2W/NuS4wmadPZ+xgy7H6pvVoeAXA4juXgoEwLLsL/GHsFryLQ/5Unb0xSJlEHYLmKNNQm0zJviyediFS9oeQRfRkk7iv6IwqFwwSyOipyZNME2ug0QDM4PvbaaQhaQwApgB5rGjpyL7lTuaMyQ3xAFuthiaunpm2lRTof4wz15NCGUnnoyDQdsl9gQLKAuxdBgQfLhOXUZYgtlVHVTDyJmTvbxS58KH43jzG3dMcNxxiEMy5jXJ0Wa9ZYkM2V5HnQKJpGOJW7GLEets78BB2RbvOuUR6mdFjhA4SNQX0mo57KulOJR62UGKeCFmDvQYMLv9pLcw=="), // [103] JCA5FP 	May 12 2019 [week 19]
		_T ("odITC70+oRePAXNwrfLJRN7+UoEElJIo42dXGc5CWKV8/6CONEv4Zss9YG2h1BfLWkT0O75B+D9NY2FxdCT3ybJb5V0nG8Ro8FgtlYE3zX32S6WIrqVIVD3Dcw7mvxGOU9hkArhyZoRti9h4gyWRwd7SI7gOKyfOAP24e3IK0Y0V7PGdPu+OOOSbuncX136Oxf9kXuS1wNo1zOl0AhQFC1w7X2AXkuziFt1+nOYgXDY1rUbA5gJigMx3hSVNwVBEbdXp9sO9zRE/wL+eOEL63v5ZaEhdNbYjLWr9VWqoepsUyvdovhGoUH9zXCjJB5CGPH4264lEL5wLnoAzh0qV9A=="), // [104] M3AJ7P 	May 19 2019 [week 20]
		_T ("cqMUYgFNz8jCPpB6i0/U/ykdwAI/gBz2ivolyCnbVaWtgFEzFFOFH6ohVfgA3bCXNplngYxtlso3RYrHGYpUjOpHCV028aKJJdXxJkR6YCg5bwsgavl6fHN/5hMc4AwZTipjVrOPSsTJKoJIRl5/b+cKiMld6eKNW/TlgUvChkRZdisNXFhEzfJJUh89dd8rstABdOzMzu9Uyihs7NnBFGRBQAaUKCrnnnPi3fpifxXzf3SkbkWgTMUguse77Xn8689HKuc/uPb9IczNQeeLLuofdpDnP7JC45Hikg/4Z6Z4HzmS4jcQADCXU8rh82NtFyvqzsiBXdNgKhTgcWLWYA=="), // [105] DN5OBT 	May 26 2019 [week 21]
		_T ("xmEANbgFJyVg7nYGsv1Ew/pnewwp8pYB/AsoqX7rNpH09p6LwgS5YkN+VEgWWQmbncsEBCx4R4SJFoHmCx+JDg3shiUfR7SsAYXMacW1ckVEs9XNKVMzMli7Xzdt5nVOELF9eMYHCUYxXo8RHPamrXrtuHxVgeM8IrsKgL+tPoEug1uHE/ODrKBg1nQUce17TyCs3KGWtOvV39xoVo6o1ip1GbtezfCJ+6xbyAHTkBAG2dTNeMCktyX+dR2ucUhFr2WMo+kiKCDpYe1ciOhIkH42Xo1gTd6dX5Vb8VLjpm4DzftoCCEvUfZMJapUtfZ+xY2gNDjU1OL/upwWvxsGIQ=="), // [106] LQ6KIG 	Jun 02 2019 [week 22]
		_T ("QCsHsdhdV5cfrVmNIaWQx9Si+bd2zlw6nYQvga/Nbg/XoNN2XhviOs77rlwVS041Dh8snMw3ohcosG++SMKzdCeGsfHqaNS7dDEtavEFY/JgZP4zTHRHRKtBlaWHKCwx63MkypEYbxP9p/nvO+Oc1QZJ2VVGqAzJFnuGhqyhW6T7RWuZibg0GLB9IKQwNaEn4SJc+twB1+GvhW5FsSv7YcUV+mV13MG9ty737nUXiYBjEkiT+EL6Wc3xg7Dmwxj9ij51kGuoFacxOprmxmfzJpKOHRHfkSXndACsBWB+UgXIJpLyIGQMrHQRcm7TvlYaaLtAD5IPAMEMpU+CoC0HkA=="), // [107] S5Q2PD 	Jun 09 2019 [week 23]
		_T ("H2TngkFn4xnIXfE5gh5JGTL+2GO8GusogW3mDled2jZiNq+z5ZDLJTbhwj7EuTu8jHwyq4sJD5a5VX4KFeQVQgGwMa2jd1LFvzrwAYYY52b/H8fC+ymsBvXZ9FY3rvFx4Fb09GIwmq4xvutFORJRxBDcbBb0An3nH+El2G/nD9yu90qgGTq0l4QHbbJnnDQ0MM/AWD58HhWkc5WBZeJx62e2GcSk6G3/VULlh3KNlxsc+x4Ok/tQqZD/JEaM2sCQI7muPzrZ46sFT6juf7YwJLgGAaJzf1duEg0hMZYVahdjk26xt0dNB5evJPPTeFla7SwaXrfXgBTmPZPPUakuaw=="), // [108] X673L2 	Jun 16 2019 [week 24]
		_T ("WCvdtaLLd0jq0+XoLpOxPdFd4Flji0QI+86MmL8E/h19eQCPF51mIAlOtrsStarwIX0NQzTcI9ixkfTXTeQM+91R4+pGEB+MYvygfgZHQtyi+XA9HbYeXOvpWIofksbRHDIbwD85YDjDFiaxEogyENepoHJAoLNlSJ2GGpuRiXQVyT/y1uRWkMOr/lz0CMeJaxMOR17AbmDf4GN5pV4UikERRR6ns27iNse5RyGpX6AzxVb83MDvuuW4naT2ctlj0gqJ8G4x7uxAd+dMffBkhgNVV+Q1zoEw4EnuGbAxD2jxe7o7Mzp7xezPvivSJRjpb060/riXb0MjKXM141/rnw=="), // [109] TSFYXN 	Jun 23 2019 [week 25]
		_T ("A/tCytyYmLTEnhzve/L6QrmfdG1J+dIu+KUbtqMzy1Itfr1eU8xtgJcZyv4rkFfcvP4gzPxXDKfRYbrmpINdV1tBW42y95UNDdeuK8J/rejF23yYYqrLY9jV/bqeBgp1GDenJ+6cTFPE/ypjLl1WQtzMRZa80Keb24dAZfFYSO76J7Qft02HW51n25wNTgmpL5O8fPa/ESOdLvRwKpFU4x8diUnGYbW/V/YQIg7AE7SIpTjx1Up2OdrNBwXObiVAzmdWG1sulrFtxgIjxt7FGxSlI7FxFciYhxBCvCbLyH1d0S8i5bSfdTYhUPwv1/YZ383ult9ImzphtXsw9yqolg=="), // [110] BGYAWH 	Jun 30 2019 [week 26]
		_T ("Yo+6rRVCpZPEoXcN1TN+bZB5vTgN/bU+pUVsMvTtAPsZQ5UfcDaPrNbzi5/7egp3hcrxoWxUOCCRJAjErtWzDovdD4Yk7sc2Yz1XI/YjRfrPkyRfhxPjE3tS1k/82y9HUIx6Tj/Xj9NYRd19iA6nX916jpPSRWdJ9lxTHbaibVF5ExinjDc2vcUyETLnMEdjMpAJeKbURg5VgVI/xWNdI4RGBKKVeEdusuoYKLSudrkeLlc++EBBVWpQ4HI8xdNDMw87Rx09FlXVczLgCpETD/45sfsZC9rBHKJIAG2PA6us91d7hvnEBLXWVanI8axYZd7A8x+OQW4PHM6hqky0OQ=="), // [111] YZUHR4 	Jul 07 2019 [week 27]
		_T ("HqC4ktRJzpRJuWPRUbTS82OdzxaNzreX9gqHXS5tGvmYrB8mQHktJc+NN22jKHFiTVVYkasozAezBs030+FFGHfIMJwqC4V3AYc54uUVBRIkL3YA2w98IVCUKKkxIRn1SYeHyTReuwfivsDb4/vCxH3+0gAV5N+9Waz4hud9QQ+yF5fkF1jZ8Lb6HSrsTSLhYAVcaU8r62uEfG4eS8TKxQp0/BJauT6ZAXTrguaaYOlqkW8gBN6ILLBpIa9I0AqRh3GTTXGGf3Pqvv6+2NppILu5ELa5TYC5AoimJthlz1Yrc0Hx8X0pefYO2P9me6AxB1Pntf6OcRueTAkxdyfaEA=="), // [112] RDO4EQ 	Jul 14 2019 [week 28]
		_T ("Pz6BMEy0T9Y7FyOhfF2iyEBd47ERj+IPGpVOYA6gWZUlLKsKj8M8FICUCOz0ZyEv+gI/JockXDfCA9CczKpT3OJwqT+CBJhxKX1qJEl+TLNYRNk4QGNVZ9ZwgJTOxb9NHYfCnFtezWgfjXTZtr1NWuMpyJNxbRNqZyPqHq+ZeKYGqnCxaT66oztgW23mt06WSKJf/1BRrnnArmz9Vc7/FErvttEfSA7+xE6iqyApgjJFBrs34lgn+oWrziLHB2F0psLNGhtEIEg1BlvQBmRQJkMEpnHf5hU94z9PC3SLTj0t9RBx55MBTwGKRFmnRetxkhfrz5Qo5PzWMv33nC9BPw=="), // [113] XQNVUO 	Jul 21 2019 [week 29]
		_T ("feFtyel8c35JA/3rPL9AFXdtsVFvQ6d3Xz2hAYsVoZDFTXGMeKldvFROhZcMqOB64SHCJImJv3YyLRS+IwAZNWohScRgUJ/ziQ7k/fpTHaRbSxYH9k+Qklf89B1KkQVfO1u/ftKIwZfin+AP5otIV4j4XqnaJHXoGnD2Iol//weX4VKiBiW9Gnoa8+0uUl5WofDFms0YncVBLgP3DaxtHBxMGtXpjAJ4Up/y/5jfEPZHY65EKVS/06TnkaQP8VkghJgGRWskCC/KVYz+uioHYKU4J6qvFk9flYXVCA8yfEIUqcOXUNwTRSw+sGSIIKVwWuUQTACO4FcEe1BrPPVaYQ=="), // [114] OUS5DM 	Jul 28 2019 [week 30]
		_T ("XXrkEgb0JCGNjKwupxzDfsLTLnWwHIDEyGIc+HTzVYboGO6PT7dIDRJxk9RdcxOmbQfNS5ZOgRW5Kp6MxFcPKDgHfDOTjSk3dVsJEsifLgccbHSjU1rTCO6Rh3scBzq+PRLXyfpOAftNMI4z06VPpCHjq9B/4GQ5ZBLvjlaaKYoW8Ev4dV/aAENn0MUe9Ymiq1sQYHyDi/0DlCvRsNyEI4DKoVnnS1V/IOwr3+OdP2IIyJ2ckTLWPpcJJh1rLjQo+t8/ALnUOv3g4bdqBEto9ZpTKvaMwOWJWl74BwK+bMvM7yemJ/gorr6dqgX0rMPF9Hm1Phgb7UwropsDBcxLtw=="), // [115] VK5XT3 	Aug 04 2019 [week 31]
		_T ("Iz6/Badl4awPsf6MbqFG/953h5saR+wbMxGlHoDWN94dkTXGfHK5srT4zpXonBYKHgDqpDY7ML66C7pUCqLd9NGWCyvLFCIEdN1uNQ1hCjwPsRgrGMAXOEaXYziKoQoZtN97N9wyIQA7sD/zXJbAa0BD7wTySHzQI1VXcC4SLjmf9rPJe5yB5geva20lc2iN5ZlxaalWU9J1D5spZye4WLB0TCzTH8EjgdJgjytMS9pHtV3pFOEPlDYhpxDr56GVOOy/GyeLiygGKdzJGZzNUKIYCnv4B0m+ne8XkpFa9NA6116BNPQp/sLEHA7V/xSnxHat2Zi4DnpLdOPcIK1/qA=="), // [116] PCUK3M 	Aug 11 2019 [week 32]
		_T ("WBHY3cGzP/GvcmpP5canxt2qjhZVARIpqc6MUtjpcPCre0sytjv1cy7U9nT1HcZnGqyYefyaL5LZqeggexqcAsQEZhhGvvMJZrGkxM3NIr0F86wkaFcA2jGdk0bICARvS5mDmjqRUlfaWlDel8TB0vYKlhd9TH3IPW0kWYaLJdbIUZlJV8sRlLs4LZC4nFLJ7UuVcHcVtsQP7SQNO2LaM3uS54VcY+ejHk6WHOZM6wjhT6q3hXzMA1DNiopa1kJujwiqvxOvo+Ltg81GFsfvjo3bGhn5LMJtzI8MfBPvSXCMZ9P2Hjee4nRVVmmZQ4Xgg23lNpRB0mq1/z+vCggjow=="), // [117] UWDXV6 	Aug 18 2019 [week 33]
		_T ("qj9KY71+vIJ1xaybbA70CTB1xNe00k51/3sbOhZK15ZKVzJ82loy0O1mEKgaKZ5aGEXopRFexeDrYdsCpUG/UFVuKzbGbwFUM/wqmHdmbBDFNH56YymkUpQxhFg47ojHs3LQR3OsNr0PzmEPkNLcwH6+cdCQweeQIZEO7PNwSwRx5RADOgj61Q45seo15C7nDjgVsXOjMOuY0ELF9Bgn7NNqpTu3cq9okRABrGAvRQ+LLXneWldgOYDHpEgDJN3V4yCffdQg3QBUxo9XJs5RKKRYlFdOOgE6sB44HbjNGGldZdswbY/CWzMde51ykixXDj8L3IT9kyCc8RTKvT1iyQ=="), // [118] BCUMQO 	Aug 25 2019 [week 34]
		_T ("W91dWrbAwjlEX8izxKIOh5GmHQnyqiqVLNjRrg7qWZHPCOW7ExujrTckx2nQYmArURvcDLj2tOLoogXoFP16cz5YEdhvmyEbssu8DssEXs4gX/N95AKKPRDHSzvpfxsLeORhyUQQAaUNwPaLpax3p2H7puPRCgSCqabi0kE5FDr2MUvz+TxzWyXg9f5G3/oL8ZoRw8XALiGMESxr/irT/7JqqwBs8/3FQTNXxjspSHilV/RNSAajEkvMnBNHNS+YsAw1NFsF/oKTRpad7yJ/WU0wtJueCwmA2vx/LMhDpr1vZbtCbn5t385+TVPq5XOOvf3kRAw2D5TS/m2IwW9kRA=="), // [119] 25MOGE 	Sep 01 2019 [week 35]
		_T ("NmN5f+RR3QqwunXovlJJwqTIS/+nUeBBupuA6zLzxkgGIFOKrACOPg9JzZ+faOhSTenpmDmsIX6SBUFDnr8/E+cocSk9OGt43QpO+8QzNZauNKEVJPDSnc/QQsm8tfrVVqkpQgTshm0H4fPuq95M2JgWIEKYtouODd9MQqwyC9n2Jh9KtFyGIV1X54b6L1R03wL8UjHS7H/AygJjC5CcarVD8N+dL/oF5OLxixQU5LQRSSBvOCQ/swmpBto2FSvO1D4aaGZ6ste33k3ejXVAmaY8OyTi+zjQT7xKLk+ydkQJC0dMHTuX8pzWez2fx0+nWQBrHwp7Tv8jdfeQjabAeA=="), // [120] YOHGIJ 	Sep 08 2019 [week 36]
		_T ("AlKAvxCNZMldR+79/r3GWSGO6zK1hPE62LTQRcyzxSYiHUJVWqpkJrZ3w169wiTwXZ/ThAoXQvwmOha8nirEZs2sp3caCEcSeCvbbRweaB8Xt3qY92PcVICloYQFIx0U+lqVrOPgCK6RLtUyvs8GdVc5nT2lIgPne95h8kdyIYgcvW7DEjzMQu4StALmHuTV3IWE7aM1WR02yHKyzGdLmWgkD+WmWpwY6q7d2HV8nwI9ypawI6pIU2VyzxZVrORT/v98G1d8TVIeWwDm9nlvJzO+3YWeA8GyEDOrXpbnYYlx0sLw3kXh/Cz86TMxATLxPuD8rgdWv5VELoxTxTHqVQ=="), // [121] CYHELW 	Sep 15 2019 [week 37]
		_T ("m71/SwHMJIortPXOUT2xTdk3WTYjGGSsZeer0UzJYob8UN51Y7iKoP0VhA7qnkh5dOO9eMXeP/V1f8j3b+e08kkVfaEC1OBDccMJbwJPkMYfuy7mwUgeRxlzKCyUEHzAqPU4yJzu6Ktgo2IvleCwqPAqY71MFaBd3fJ3byfSi3FYIvPuoD/MQJSeySPWDbZC0ym4xZCJdtpcKHbNCDWKk048vGbhpW4ELPdbPulTWi3qtYJNpneBdBbjl4yEl3fJrEuau7CGqRrCSXFaFJ2l4r5LmTZ65ydbV0YpIX0buzQZViSgzfmtQDcfXNoE2xfOojk2HWlG6vGJwUSAEk8q+A=="), // [122] 3UTL5P 	Sep 22 2019 [week 38]
		_T ("pDgLQcOlSdZCdkwZeIvFBdVtRdDGN/dy+uoQB+ztTbicXCjwDLXXVXtI4UZtcs7O7uFNQodt7CKVTLCJzlBtA0lQn5HMNe1/MliNsZAqtXVYPmGDru5tCBXoKUREmEI2RMj+loRD/Xvi18rIibEu5UrWbOPT+WCnLAPOchVgyYSbsEVHajG6lmTWB/A/kuzcHK1Hm2Jc3W+LzyjSVI5G2tSAIXFgCImKqNEFjZlMfWjldGM0oe2etcn+m47ik1VdBujvI70S2E6C+M1RAmlk/A3hdKnRvnAPKKRSM53YBCBK19JbJkWII2U45uj5SUALiE7FboTlln8OK9djVboJdQ=="), // [123] IYQ6UM 	Sep 29 2019 [week 39]
		_T ("Rsd/ZBYlm2Y2VTPjNrx3H05Xmb46N4IuKaTOO4X/mOj/B2daWgr3/jEP8Ms9yBbzo4pZYJzVfOjKpJehPRpUwfWmhe2pb2SZ2Ggk0g7ALnpYcH46/1tX8/0K8k/Uis6MYOmOdLkbLwbE3bAD17CZpf877Rkpnc7RYMPlvzRGal/1kRFvhyYRFFjmSs1j0bGA5YrG1bvJMKz09cqgxBpyPTrr+iuH+bX425C+pk4GPI4Ugyf5EirVSxIVTgCi16SP9Jrz25e2iqL/EB3Ryz96P54LzJL2GTRbnf5iI0Vov+4sDlO+lMITk3kEMPWZdx7GcXtuCs3KHxItPhYTLWeoZw=="), // [124] KOZQ45 	Oct 06 2019 [week 40]
		_T ("eFAyfCpjU4IV68zcpdMGMDogWCXKU3vptXdv3X5kOqdJF9n64z/YHVgh4/NFfdEBa9yz3Uym/5aVhmUVXRtawHVrI1YUg1WUuQ9bwYnPiIk52dMyV61dWncdyXYPgFw2OZ1lZIxkY2zZSxMDvizU2XP7Q7A74uZQC/TrghQ6fb2piCSf0To6gR5KyKVhggPXNAPtE5G7uxg2u1RKMNu5Th+bupzeZqmdgv5cr2kLK7Eqtro/Wsl3+TVtLfwuN6ee/mqcIVIZ7YsGs1KzOXK7loHSxH2R/nKqrluxYmS4RFzCvI7q8aeyySFr6oTxT54WOlVbxJLUc1aWbcBm5uvNBg=="), // [125] RITAEJ 	Oct 13 2019 [week 41]
		_T ("ldBo02XDppyf42YhrJ0R2qutGmIE42x8r8smT8K/956Gp4ydTm2JvbWdFFqtLEUObbFO7q7zw0WrFfLOexvUyw2XqMULDxl7p2I/QQycVZ66nk7OYr2jTIqjlcuP+Lo6CwwNpGibASL1t1UD1sgKCIUAu58bjiKWl+HqYhwzp6X7K0h7SKMS4kdVbyLtvAVRqhhDYo5lf+QyQibT1IlZ0kvgmUaE+LkN5lo6vAUQnzgnmIUbJPVoFT4a5GMQ4TuSJuE6xuFTqW3JDTypTfy/QghMQgJ07WtvQopAYBTzhByiQPu/GZTGA+GlwiBzeaARqS6SqJ3DgCMbbQKEdKSHbw=="), // [126] TKUA4Q 	Oct 20 2019 [week 42]
		_T ("vnTGMcZeC+CcWptzopiTpJz11P9WSG9r1vA5fLDyeqAuuxFHYgW69pIDalikbW9wSs4XZwWT8W/aGtixDrZgeEmDmKNR8IvFhFD3D1P9/pjgdDaclI6qpzsvVU8mKLzszV/+pwub+oNJvkCmVHnlq8QqtBwvEZf58l+ogfJuVaLxh5NElCcm/8LIpDaFRHYPK0xt88owO0yMkFJ4sxOsP3VRa275f98MW1eW2wUDJ3o9ic8jb4Sqev1TSVt3c4XZZ3OC9PtVIVxinRS2bOYQ/Uc36kk6rViNRWy9xPhdbMG6qtONDM0wBwN1mH/kTRNcjf42N66CxdCHcgChYl66wA=="), // [127] ZCNU7X 	Oct 27 2019 [week 43]
		_T ("OokEd2J4vNO0eEwCWY6uvbitTbGqCYfemt/VEjgflhOQrScgVv7psM1QL2Y+EZwmRPY0ey7Zc2pF15Y35WezPgrAMBWUA89bIZfTnDiPo73BCHpnB9AmlS+88PYssPJLugyfbNZAjBFB/qj2q9d3LhxcHo9f5kinDlVnJusRBqh8fjzFofa5zoc61f+/TPjifOJ4uq+KfNVYlqmz7rvZDTEpWKCY7WQreJ1YxPEXIuYZpqX+bltk8vpE/VYjbMDGMfmWEP0Lsr3F8utc5bVOh7Tms2Ww1R7DzyuCfwOnm62oXSm0W9jikN3wPEzfETw3uGly3afrycc6sglJQoqpug=="), // [128] VX4F6Z 	Nov 03 2019 [week 44]
		_T ("0Ss0cp0TmzVG/Cz1jTG0eBR/3/vUepfk04uI9lVdacFtaKEyONF5pRj74gsee9R3reSZrcpWp7vAmlswSY/OmdP1p8poPA4hXeWuFOjgoQdyC/7A9jtmO5kFpZ3ViimSyAZo4r9+JxcI19znP89JSN7khxao3D2hOB+yKSA6kbTDSkHt5nFqiv69Sof7UXgMuIbBclY/5DZcKU3v3L5dPQXv1cQBcGxxdvn4ZJmDkKaRQaEKJfPZHGI2DS+ljPPdNuU64TraQHSpUowtPi4SE83lfhXXvR7/v1lEwo87tTdZudqr27AMrLk0ajidihh7zUcNKp2+O5BZi8BXsJgLnw=="), // [129] AMD6V7 	Nov 10 2019 [week 45]
		_T ("df7BTjIuMoY5qeGzII8NlwK2E3SUJITvg0tJ3oI+Vp0lIrzWXVi7nQTzAqJr1ROwOkTheleoqKzGXPPWz55eNTqAUq/Tt7zZmx4ZC6QfLw6umP06rQQP3IjgNwoWf/TGaxpmUxUT+SE0/rT1TL8J0nPQuSOXuMPC9PhV8427NU3CvIsSJLatsfExVp3PRxYJZlbx3I9ZZjKy25OndCAdlPa4IBUgMxUo7fE8tr2rfPx1SPMllLq9D75qWfUKQB4B4IoUJA4FK+hbZbRbYIXrLWcdq4P7ybPZksPjz/RU/9iGbWi8TRTdjxSkwuWNbfnS46eQ9HgpZ+YO5lT0d+GREw=="), // [130] T5QI63 	Nov 17 2019 [week 46]
		_T ("QKl4KRZvI32dVB5pvCNmuZz9TeEW305m+ezh1Ngr+gnzl/mTtZNjQ3EK8PbvuA25Q7sbtta4p/owX6FIRmBYfCuS+Vjg34ynjKRN0R2nEVQ9XNvCdGSOa/dqNfGYSPD+5gx2NW25OIHqZXnSMbW9o72N1xe415Y+WNWmuHj+FeVZCvc9qRrs7zR+Omib2FyO1bKCugpmPt8wMGv3h0KSBtyv+g0o3+hzT6/v8DX8DiGmL/UmGr1dJC1BQGpzusc8Gd9RG1BdHq0BFtUuxtB+QOEkpeTJAL1X9jaPsPxg8iYUBbA8EeC04aaDeVEc7F5KHxBbtgRFNsIgNGuhwCeVHw=="), // [131] X2IRYG 	Nov 24 2019 [week 47]
		_T ("rNfA5Be/An8qeUQJWSuhC2+EPdkyN9+KNNxIOcikNLAES/F8yCe3wjeVlushKZWbjzK0XNhZ5scaVAUNeM2bqaKrQRRECxQeVI+kyISzhluu4gJOc1YXABYJdDvW09tlwkKwoyK3y6T+NPKS+TCq48A5S4tWjqilFx1zJU1boHWskDJvjTSJ9UOQuAZDoObQ//KmQKRlcHZ7i1hpCVk2rjkUn2S9gZWEe8g1V+X8fuGwJKemTslzCWSLGgISBOKOgdyO9EDIr1bcsmT2ZS+Z1iRrpiRQP6CIIQDyDovZYBwPMqmhCzeSSue67dHmQAchQuK8TO01vVsrLRmE/dgJzA=="), // [132] 5QYUR6 	Dec 01 2019 [week 48]
		_T ("RU173cUZIkMgfDXLnH25UHgO2Md9UD7ee9BClV9R44Fp0lt+y76XiwdKLH0eekEFm+B6S6ETgetHO8HR/WsLvasnZp8VDceaUBvS5R08lms5SW2ZEtGC2wpfXa2Ngu1pCj0buJU7cK+JNP5rteXn4uWdfSipyCrVKeLtPpPSRRqKcfXS4MDTVdB2JXanaCAfz/KHzPCH32/6luoLQX81bvoWrwl5nf0TnNF8s6fo6ZykSivZgqfe3QV0jtV2CD8V4in7y/Uoo58HXM+Uz6dBwo/MGH73W4OhCWmbtynShxy16ctAf8KQL7iQID2ccZbKKsFx+cUHzBQLpCkLFDlrDA=="), // [133] GZ6537 	Dec 08 2019 [week 49]
		_T ("T+28INxEm+1LIs99q4VotRLhtEHjdZtE02qvGj4oC8p0PGJtNc6ADr5Vi9gxNNuvTIEa4z4qiXQlkAykYcaBbFUTuIcizroDFMbJYtEdfmzH9BApPefVIbuOvPQ8DqDYiaZZD1vJnhN14LBVK26O9MB34kPKMxdp6GAdFYIelVBugX+vRI9tyoRUogLgMYjt8fXBTBRFjFjYxwxBGLWIguXMOke6C/0GDnLNPYdAsegDmy7xwwnjvygE09IYmvzF7LvdCWd0mTsRs40rD2GMJkGRGk0bwOkKlByqLGQjmnI8Mw/x+eosM57PUacFjVI8L24xjCZZHTtBNlTGNLAHIQ=="), // [134] 32GYB5 	Dec 15 2019 [week 50]
		_T ("JC0Wve5Wksk4N2bTi74hCZ5q2uL7QegACZv5lps6ITlonfchDMXcATqSSlUcBQMTnM2HYSDddmGxvB7uwNF7lis+TCOKJbhgWGeHyYLSUvuqXuc6YzQ97bIBgtxdgDxWNXKfW7QkDLd/7tgVmUZeyc8lsUL1qpgxtZR1fYiVFwZpUMzSi6MIA+sanMLn3cihvGFD3hd1ijp65Eko8YlOZ/iqjt7Wi8zrniyiCVeJOf5pnfwy1TcSxJsEqEflZF0GC0VcNDL10RNbSTYXYD3CIJk6deGxi8GXvX7v4/EgUXvds1aDlPL6JW8ZpUASYphmbhDW3f8RdZQV5LTSsZJ/+w=="), // [135] BVN7ME 	Dec 22 2019 [week 51]
		_T ("zZ8wbqvNctc89Aw0nLprpVG/86DhAfpIu4qV42iIaTWXy/aaUX9taGB69cdoFHJj46HdrF8nkT9eT9HRTF6p3x2zylrcM8VVeNTxFilMKpZFdoRAWlz7lgDG1Qm4TnGdMoGpCzdlgHdO4R42zfSXC8L46f2ikpimDlf2Xb41M75fupw7wS5ddelFCoRlhQpdFevqO7GUkek2NxtVpA9lX7aD48X3tq8yFSiaujJqeDFotHmP8npLwHsRUydUgDuMw4WEryYTSwjBx1Cv7KUwWRF7IBLkEBVIZ+5V+VjxiFKzqSWEx4oASkJoNCNMIG1zDXbIYZsMBjtCTlptwgihlA=="), // [136] 3UQLYJ 	Dec 29 2019 [week 52]
		_T ("o4/25mHTLl8kV6RJYRMXNbODRWgt1xYXSk+VVGvMfyn9A8EOmF7RmmtXjE3pKJ6FCfNVoCnFxX0kEZsv6D9PD1LLEC2Xs3szkTYXgn/njXr1mJAuOlYCRi3pNCNtcLFMC9QFboMBdIyTGOc3p+WZuAIq/FD16vR04cDiUYAfsuMfSbxbVkfnBU24oDVsKY3AINp75eYwBWix0GlwNwMI+nys2e+P4VNArghCvWqZKbpeWa6vZqESt/v1A1dlNR7BWUYEynyMTCx9DYycL+jrcnlJV7PGw4Df6odZubQ++GWKI9ybvbSEweBof3vjF0kerCbFmHlTD9LRNPXcxxkzdA=="), // [137] M3JVI2 	Jan 01 2020 [week 00]
		_T ("Syn/bYv9Chu5SXUZP/BHRD47kyjVl+p5QHX6digU33d7QyHXFOmJwWPYev8XW1KvUssh1eCh21xSM+eZqBDusXp2j2FnST3UXonr7r0OalkRijdYBk7Qj7vFMMLiQFu2NrYhEzHzFZxD37RJIq/H/2z+zeic+uXx6mIuXv7DdRuDhAlsYZr6NKmukeN5knyJi6HVM3V2f/XBohD8UO53NCRClnyMBv4xI/GbEHytVZ5QEDXMZDRgIrMOZqp3CwFQmXvjlu3TXEKqHWuawUBYJPEKQqhU1SGOBEvA8i8c8CcRj+J2MyN8H2x2M3pavtfQmqMaGYuFy1y2VBaAiN1aCg=="), // [138] 4ZMIJ5 	Jan 05 2020 [week 01]
		_T ("jCGrtnm5Fw07VM0M1hqEqYj6HhjG85MfbPYLyG+WWajZpETjskVB7Jrrqjuxxh+dkh8YwtjOl7lOGPK823JpWIpT+hr8UwxfmRYZFhhj+SZ3dqCbmkd3qFPyK8PIG/D//iSlfH8arBZ5lB+wm0FmNk9WjYS9NxNI3wdUp6ftQ4WIayaa1+8dbzpgjceSAhGogHlHKNfaLBnUSMBY8I/YHi2yALXm+vUaKKsicG6mpNRDrfnta4faJfyGTYS1R/cyBLcSWF6RA+mRynVI8LaMq5bhcEdy5o+UEf1srhh/KQde11Sq1R5tZ7V9RMAVTxJXEbnfqDB4mTQJ7sXcsAUCAQ=="), // [139] L7ZRDX 	Jan 12 2020 [week 02]
		_T ("mSk9/z1fbwoL1tcmUXlHeC+MMFUwkCyvANP6ncbKCotrMTBGSuxwlXPuc9f7s4XnxVsYs8IE9Q8/DMsdiZrQCE4+9kfpOt/1IrSOMjUokEkecU6qhvBpwiwCw46Z49i82MGWkpHJJ4daAiBOOR/XE60TTKVKYvAk2iMj+Wti3KxLmVGs+gtq1L4vTyIMcloy7bQKd+cgGW/QbTzCFDHAxDEKUW6VSEjh9hMBxSqWFsi9/1J/TbsUXGWE5jrlSfcXrYurqk/Nx7/UJa9j55kMi09mYAoW2U6bzTfBlv3ukcugVUYNuda4QWs02JVJdxZvYxCzm01vr6L0AtlD9O0HNQ=="), // [140] 26PSEI 	Jan 19 2020 [week 03]
		_T ("SetDWKl/SRCyOZDn0hj23jox5GV6HXzcgn8/fjPhCsY8qRacDzUCQjsDvD2wDADWLWpqq8CHWyOIVDxrQDNX/JSG8VdKTFTHBLGJayrEH5QNuUurtg02P5JZJS/qj25H7BQP8aqID0xWgM2lFixphsZjNbswIu4qrY5x8bZSJbeuWysgz2rZMBist80zBMKElDnpFoFBIlz7RAWtSFcpgESZ5u+DCk0AGGh4UsxSpypdpiGGGsO9FMhRjM06gHHv7Szy4AsHulfcYkGdwct3Mo4q2GlsppVptmtYHfdUXa993O6rY6HCk2dgQYS0KpI4WGsBaYVpwZ4s27eLUmbjww=="), // [141] P3NT7Z 	Jan 26 2020 [week 04]
		_T ("m0EttXbFef+smoigMMt9LN9yYi2Addna9wq2AJT1WtvB193o2etnEoibDElq+Nuh8OkzzJB1QlDevcJZzor86CQgKE4Lfo6vDn0bLhZGO9N0buXxW1C8lB+SnILBSoQIHY1IeraBJrkn9vYEEuD5A0Beu/W9k6ryp5EmqBxxRHHjRmcM1EbahRfRvZ3dRC+oCTAWlhUF6VSiKQdCcb2mdZow6TERWNNZrtB6bwxMuaVZLufYX69OggdV0uLJ9nN250W3Xi4vgVTTcJrshqFQnpfuaFXEhSh7Rg1VP3Petn8SexEWOuIFpQh3knbYsHOcXlynSrIL0q4PvJciNTNL6Q=="), // [142] 4SA5VN 	Feb 02 2020 [week 05]
		_T ("Y6uSzMWAXktnFwzCmm3grtMKiaR7jnfWsvJ0FQs1wb11SHnXJfP58Q7t/ObIqY22KnZmd58CHhJhbTzLRPclk4mD145Q2J28nsc2l5fzDBrTmf7TemWG1arJU5Cfx/S3Y7EUb0A1nYZDvC5pNpYDoLe2Q+q9/vyfrwvF6be/aMMDluXQ0t8FkSKqGm28jJAMJnMNhQeT9O9a2mALr66PT6Xwsl7sxAQAqPbY4jkRx+dTwr5bauc0GlRXlrpt/H6bQg7G1513Qk+XnSEM+EfmJyV1hk9QE8o+kfWMtJP2e/i4ytZAS6bvVjE0PRoyVjKkRRert6cLwKZ+aGTR0dDrrw=="), // [143] 5LN3RM 	Feb 09 2020 [week 06]
		_T ("xDGHXUMLMP6C7yZkprGjxDu3Fvi/O7c65Y1JMByrakk1RIqm99ngVB66nc3KfGd8DyMn9Fw74Q1TDFv+aN+yM4XIdfPJzaDjUplqbtwr5huY08fAfJCyukzNzEEx0Q3rtidB3ZnBes4RdtqS8Ju0cYqdIZY/kx/aHkqeQVzVBblVYPPa49rQyzAJtQ6jzoClNEcC3hAUQTJW2I10dVue7Q/eOUuoPzUk035KkcvXzXxsEGd0vH4K3IMVyaFJemsDDFSGSTEhVa2Hdot4eRTC24tVLdjPmtftKeiCMP/lSttwMAuu6bwkHbiQH7Ut9UFz+hC08KozrCEFecWPd76Mmg=="), // [144] 4SBLQF 	Feb 16 2020 [week 07]
		_T ("QssHChhYRlmyqPaqRGHhUG4NJ4nPYS+zebXIh9u7Uu+wlGhprm0esd+iuF/F16sT4Q6E05SImGBCXeQ16h9c2dun+plhWsHN78tt0OI8QYjNgNpgQ0/1OLp5CTe5rRWlI7ma5a4GXH4g1XW5Q+GH2FRYiHhA8EIvdp4Jz2881eFJP8e8bCD3m7NNbdVhLUK/Za44DVf0sF948HEfpvaLcA0868j9W97JjMT0YVucTj9L6gmjPwFV9ZjRFvAt8PQPuwQrbOraZltE7yb311y6Oc0Awe4ZEgd/3dZfhRN3RKiLYxi142GHcoTU7uU18qDtk0ouuWR7x1gUPUAvOJ8wYQ=="), // [145] A7T6WZ 	Feb 23 2020 [week 08]
		_T ("KwJMbqTN/+5FidiC2kCUc5JaQKQZWLBKinO2Qqj2S+mqY5+vdhcB2iDl8C+17fhb8CSiSDrY5jlVUfMBBQFVY16CWZ/KrnqJRm/Vm8FPtiCCmFHkC79333z8M3qlAT0fLrOPLTuSPCtByzwom9jCOq1H09K/YAK055J+y7N1iUVK/EScPulwpsbuVEeAQdUvHHsdPk0kr+VEBttEEwdbpRk5C0aixFF+KAyHH+aqTGM17Y0RKT8Graf4IbOdoFuk0dmlZRaD8KLMwIkK0F5ngmWb5RBlrkFZ/LqP1q2WriWz+XuvDruhyq05LwKkBo7nknIx8gXv6tJNNrPvYwbfGQ=="), // [146] WG2AJB 	Mar 01 2020 [week 09]
		_T ("Qj0ACR1I8B3DYLVd0gCX4lmMDIMX/kgfqTj8njmJOM9NyyEk2qdsy8NBsPyiM6Fd0HXiKg6QwImxBQfebUridw6HeF7IAtWfrAcqBjPl+4IjevLvLqzalmkOsSrboxyj2cPpxDvlklM/aTPmwoMN0DV1awDdlKWwNrkWkhm/COqggJqfsgeSLAyw2IsgCCqGbOhvcXeMxhnl3gaHMM7KY9PsVE/27yGETKo3rGdpXxPMByirDZlPd5R7ktTt0nEtlRSbloh4qAP+MYMqfcUWoLsPTBh/avCRjM1yM2SkaRTABZxWwZIRs7eNUmtp83GkEfmyStIYo62F3E+tXfgPWQ=="), // [147] RZCEBD 	Mar 09 2020 [week 10]
		_T ("pcnffY1E5ft37dbViymMO8H0jJdrZtaugMsWsfWQxj5Akle22JrjVkLXpUyUqQgb6Mgnf5hjm28/2QL5mi3hwtQNyBTLdQgtlt7Wo/QwvNUNFYL0uR1i275iiVU3KZvAO6Nw19QeqA8uESzOSHwfhUozFClXFlpAyNhmuGGk3RvPVERL0toBxQqFIv5iJL1Li+YbXNMdIpDaORQ15dI1Cst9emscxOqHG5mSbexwvzvkWlnbApq9LPha+a9gNtKtdTb4b5O3ax2fzKT6jxT+3FAs1afC6mT9WSNirsKAecQfm3ELxToOuZ2GxTmdavUdB/cNyZJB9AttpsNRHemCEQ=="), // [148] UT5GHY 	Mar 15 2020 [week 11]
		_T ("bu2FUnQxtX8wrj8QR7AqB3roULUGM4OBFMV8ULFi+kJn6p5/etvIgfHg3uIsyXFWelvnfWmRaIYXTNzH85p+xmhjPYUmErKHLKoRt+nOA+Jf3jTXNCsGUB19t5TW1wY4yBYNRylwKkmbUGs9OIE2lnkMltv80ofChQqNeyduVcq8CyFLBz98hqk+oDFok/h1F0F+A+tNN9ZFdyxPxCs6jfRuQwwRpV1aMmUqo9+nRdcF9S9U6OnXqvydE3MjjVvMpL4dq+UgKeX3qu7ceQxC0kXXdrxMSfJTdg9l/iXQwjOtPNMdRZPsBwP6gQd3Msz3n7zSg8kSnm3+OQ5xT2bfNg=="), // [149] FHSPJO 	Mar 22 2020 [week 12]
		_T ("pfKN/1rLERXb8LkhvZRfNOfMiMYUSOX8+JEg/5K2aLgR2wqP+egkfRAr+9v1aUF2jl4XdK1m0CktXnZVaUsl7iXs22vwALriJfYu8cNgNcnQk7K39vOg9DK03sRbWedZVk5iS+QPO2RVyIV54vsy4YqgfAqEUemEAV+vLGZ4nlvFp9Za1OlC5CeUJ18F0tvaGvXk2mpoF5unC2UZ5E110ROXQkQQ6tnVe4s1wCwHrJyGfjs1qTZzu/2ivA63J1y6SwQ/t0btQkcpYyGWbOiF14xyiytIRFxqBKRxUQ48p4ljJl9BBmCfpg5kOUlS6mddV2StZhaJg+8M9o51WmhwHA=="), // [150] Z2CDR3 	Mar 29 2020 [week 13]
		_T ("jklgBpHW7ld13dHWvloJibr75vgrLZUWKzLA/XqrHt8tufiEvFGRzKtOdnF7CSJ8RcJLGoktvJhplKutt1cmgv7re+1YsINTEEjR4seSLYD9byhDz8e58Y6rUGCZKhbg3bgFQvQQ+7xI2SK1t4JxxCZFAY0kWLNHBHTfpKNlEXcCebMDRBmR5gVomAw7yL5thN8S78rJDF6ZuYwnkPQSj3mbZjZtHBrX4KxHCAlgN8vINUkm/POFzMYEK4OMd39VFgCcidUGlNVmd0n1VYR1cLDil17iQnjLrdNj3pp2kJoM4wRkpBpoBjzaqUpBaeH0ncdyP2WCUw1L6wJl7YIXDQ=="), // [151] XHNEYQ 	Apr 05 2020 [week 14]
		_T ("SKHWAdXLinhqD4Ox5nSsxzNAzgCAF2EsvHZhrzyDNlAwnAfEf/qB5kamVQCInwa+4iYS0u2ya0qN8sIAdjKt4Twv36aWvC79pNW6s8h3ei5R4jUxtsCCLnt8UO24pqg9lPhDeVrqTpH+MFPJiSiIPFMVSLMtMiB4YPEaom8jyyzvEVAEoa4007H5Ym2SYqJytMgBDZKyFBq7FvgJ3TTE6s9vfvxfLGkDeQYDB+LsdDZGL8USnijxr1pbTkOda51H+XIU1HT6f5R2u9vy0zaGW0xagdAmhC03ZAvFFmLNZrwOa+GzMGb8GGnar/c/pYylQxlOn7lBFq2fjclnWjsLQA=="), // [152] LUPEHW 	Apr 12 2020 [week 15]
		_T ("VhGJiME/49qaJvBnR6hSfF8RF2c/67u9o0Kx1w7Mj6aqPHmxRJMDI/fjowPpbixylyOZy3EWkH91kxnR8fRtk6SXHDO/K3W+1YzyJjl3W67m7Zjbs4j60NgPl4BkfEPGNYZ3WNaZ66yWxBqSErupUNacMQpzdomlUMB6UrUvbQtDG6cg4ajx6ff+JMzeeMKappf9HdY7jOSjd3Y2KiJjVwOnMEzJhNwTxGE2wEB8YSxw/m8DkgoAfBWpLPsPX8NFyrwQsl1XCkTc7J9j1Ykeh8bhctXcYlVLPlLfzqO+Y72auEJXzD6w28XONwesVAxGTtfcicppKwctVqW/R/DptQ=="), // [153] XOQUZT 	Apr 19 2020 [week 16]
		_T ("ga0hZeLLHIn8QPq5S65mxdKIpm3iD7VVdGbrABJS71WbByJHcvVT42VUZ6jKTVz18QSnz7pGZ/2/e9uCp1RLD60WXQoLQSITVssf/0iYW4ddXzZqsBjdMLWhV9+VveTgVUa0rI4G+9FPX43dW8yGIiUiFm0ITZRICPHpLVhm7qcJetdh1GOcL+MjeAV7TpEKmmXVKlbKYpkZJwhBWG3CEbykzdNtARFcbyjmKT25MJ/5PppuopRP6KqtoWBaXpIWYTs2BnQo4vyhDJvZXm45EOUGxPc1+9qfVoFBpEeOJmg820P61tXgQFYOVa0QrwVYda8dDkVY4yvx98elOVqFWA=="), // [154] G5SYTJ 	Apr 26 2020 [week 17]
		_T ("qFkCC17F+Ebl7eavun4SPn22z93EFavcw7eLD6TjEEFaptATEG9KBnE6FG5he86E+8Uo4+cpV/zYdqKx7PvobLXl6XcNVl8pO2fFaAt+bNq0hn1zf8Ee43UMAxN4EEewdayumnF7gYkSPAyd0lLRU0b6xAzv+rHZNixgpzDKOFnvEmvmWBA1cz3supqsF4A2WcHEcMYRL7d3tEL37wXmyh9iZ+Xoq2pdULtrURGsqYrPDHU7E8Pm9Qd0o56zoBaIzJ9NWS8/1KaFtMQ6zQtYaXDTv9pbKUeKqKBipTPQcLsnqzG1bdAOXFeExHDMPk2ORnXZWhMMeO93d2PoWJX22w=="), // [155] THO5C4 	May 03 2020 [week 18]
		_T ("FFBI8ugHYpqiHYoS1UCMKf7fwownilpGOuaQEfyc9FoFmqBXd9D6/BHb3Foejn1H/Z0Kson3fsxJFo9uViueVUIpyl8qB4IKzEOyWa0JEd/c0cQfA0Yh6QB3HsYZaCu2Ie26NiZsa/PmIifVBtoIGujJXHVF13HIbebll0/uFG95eo1PecVMwzkuF0OjRFcOqswc7jTXbos/l4X6oPEB7RF5ouv6M1ySBjspatmAn7LkHpSFSj7kwTXY1dcNd3ja8SGWOf07Nh5z2KYAQUrz5/3S0A88naTFkoQ6rzWkWd7AFyODj1eiK0LUlq5RieXrKJ/slVZ6q3Rg6bB53xIydw=="), // [156] 6REHDL 	May 10 2020 [week 19]
		_T ("DZ6YC/R2SUBqLzndbfw8TOvbEwr2TiH2iHTeQ33f6w/vkwWT0oIysib5JONO9xQAnvEsFHJdOlNfe/m71CrVjDRzA1bE3Oo72sUhYEgAjPh+D5l9y7wAGJyVTRB1FyJXZgipLWptXBoYHxreJD6gzkiy24eAdAdCbZb+1gh96xJ7dF+/HjNPvsT7xR8VEVJOqnM64D2L2lys3nkQ2WAXuo5UabuXQvI48TVAaAq9y4k2uj0HY+a6d8FgGJwMvU08LmohQxzSLfGrln7oWmXZmGdBHRMYezYQtyYgBUJPuXcx7yf6FLAl9XixFSmzx+JwFrE39HENbNvY+cbNY0+ulw=="), // [157] EU32WZ 	May 17 2020 [week 20]
		_T ("YxzzUEnBtMNFPiJ9xY+A4v5rXgML2evuRMkH6u2LK/iEw51nwLu4BFJ7rBGSvqXMRJYsNMrjs7ImRUFT57kvhOoQLkJRFHHVszsHQWwzviYrlWB5bB2Pdq8PDe9mcjKI1htx0O5glWcpTrUrFiVpZdSWEgkNRuFmN93zt9MhNEsGBpZQ8FmHV3bWl/1DIynzBz9JiKlf2a7WmXCZrGPOR9iGfcM2ILaPEbD5hycHQkaudkEJe94TUvTtVvzz9NOpuV26JJ/6MHfsb0OKi9AWPkD9wN583HpHgiojQKHQCqk4dGSLqHsCZjlAM4RYIjhb2kYiNLKG8IApr+yVd+fX0A=="), // [158] UYGZVL 	May 24 2020 [week 21]
	};																																																																																										 

	#ifdef _DEBUG
		const CTime tm = ptm ? * ptm : CTime::GetCurrentTime ();
	#else
		const CTime tm = CTime::GetCurrentTime ();
	#endif

	int nIndex = DiffWeeks (::tmStart, tm);

	TRACEF (ToString (nIndex) + _T (": ") + ::tmStart.Format (_T ("::tmStart: %c, ")) + tm.Format (_T ("tm: %c")));

	if (nIndex >= 0 && nIndex < ARRAYSIZE (lpsz)) {
		CString str = Encrypted::RSA::Decrypt (lpsz [nIndex]);

		TRACEF (str);

		return str;

		/*
		CString strCode = RSA::Decrypt (lpsz [nIndex]);

		#ifdef _DEBUG
		TRACEF (_T ("weeks: ") + ToString (CountWeeks (::tmStart)) + _T (", ") + ToString (CountWeeks (tm)) + _T (" \t\t") +
			::tmStart.Format (_T ("::tmStart: %b %d %Y [week %U], ")) + tm.Format (_T ("tm: %b %d %Y [week %U] ")) + str + _T (" [") + strCode + _T ("]"));

		if (strCode != str) {
			ASSERT (0);
			DiffWeeks (::tmStart, tm);
		}
		#endif //_DEBUG


		return strCode.CompareNoCase (str) == 0;
		*/
	}

	return _T ("");
}

NTSTATUS NtWriteProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szData)
{
	if (!RTL::IsValid ())
		return false;

	//NtDeleteRegKey ((CString)szSection);
	//NtTraceRegKey ((CString)_T ("\\Registry\\Machine\\SOFTWARE\\Foxjet<NULL>"), 10);
	//NtTraceRegKey ((CString)szSection, 10);

	const int nStart = 2;
	std::vector <std::wstring> v = Encrypted::explode (szSection);
	CString strSection = CString (implode (std::vector <std::wstring> (&v [0], &v [nStart]), std::wstring (_T ("\\"))).c_str ());
	NTSTATUS status = -1;

	for (int i = nStart; i < v.size (); i++) {
		strSection += _T ("\\") + CString (v [i].c_str ());

		HANDLE hKey = NULL;
		CUnicodeString ustr (strSection);
		OBJECT_ATTRIBUTES obj;

		obj.Length = sizeof(OBJECT_ATTRIBUTES);
		obj.RootDirectory = NULL;
		obj.ObjectName = &ustr;
		obj.Attributes = OBJ_CASE_INSENSITIVE;
		obj.SecurityDescriptor = NULL;
		obj.SecurityQualityOfService = NULL;

		//TRACEF ((CString)ustr);
		status = RTL::NtOpenKey (&hKey, KEY_ALL_ACCESS, &obj);

		if (!NT_SUCCESS (status)) {
			DWORD dwDisposition = 0;

			//TRACEF (FORMATMESSAGE (status));
			//TRACEF ((CString)uStrSection);
			status = RTL::NtCreateKey (&hKey, KEY_ALL_ACCESS, &obj, 0, NULL, REG_OPTION_NON_VOLATILE, &dwDisposition);

			if (!NT_SUCCESS (status)) 
				TRACEF (FORMATMESSAGE (status));
		}

		if (hKey)
			RTL::NtClose (hKey);
	}

	if (szData != NULL) {
		if (NT_SUCCESS (status)) {
			HANDLE hKey = NULL;
			CUnicodeString ustrSection (strSection);
			OBJECT_ATTRIBUTES obj;

			obj.Length = sizeof(OBJECT_ATTRIBUTES);
			obj.RootDirectory = NULL;
			obj.ObjectName = &ustrSection;
			obj.Attributes = OBJ_CASE_INSENSITIVE;
			obj.SecurityDescriptor = NULL;
			obj.SecurityQualityOfService = NULL;

			//TRACEF ((CString)ustrSection);
			NTSTATUS status = RTL::NtOpenKey (&hKey, KEY_ALL_ACCESS, &obj);

			if (NT_SUCCESS (status)) {
				CString strData = szData; 
				int nLength = strData.GetLength () + 1;
				//WCHAR * pw = new WCHAR [nLength + 2];
				std::unique_ptr <WCHAR> pw (new WCHAR [nLength + 2]);
				CUnicodeString ustrKey (szKey);

				//TRACEF ((CString)ustrKey);
				memset (pw.get (), 0, (nLength + 2) * sizeof (wchar_t));

				for (int i = 0; i < nLength; i++)
					pw.get () [i] = strData [i];

				status = RTL::NtSetValueKey (hKey, &ustrKey, 0, REG_SZ, pw.get (), (ULONG)nLength * sizeof(WCHAR));

				if (!NT_SUCCESS (status)) 
					TRACEF (FORMATMESSAGE (status));

				//delete [] pw;
			}

			if (hKey)
				RTL::NtClose (hKey);
		}
	}

	for (POSITION pos = ::mapWriteNotify.GetStartPosition (); pos; ) {
		HWND hwnd = NULL;
		UINT nMsg = 0;

		::mapWriteNotify.GetNextAssoc (pos, hwnd, nMsg);

		if (hwnd && nMsg)
			while (!::PostMessage (hwnd, nMsg, 0, 0))
				::Sleep (1);
	}

	#if 0 //defined( _DEBUG )
	if (NT_SUCCESS (status)) {
		TRACEF (CString (_T ("NtWriteProfileString: \t")) + szSection + CString (_T ("\t\\")) + CString (_T ("\t")) + szKey + CString (_T (":\t\"")) + szData + CString ("\""));
		NtTraceRegKey ((CString)szSection, 10);
		int nBreak = 0;
	}
	#endif

	return status;
}

DB_API CString Encrypted::GetProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szDefault)
{
	NTSTATUS status = 0;

	if (!RTL::IsValid () || !::keyPrivate.m_bValid)
		return szDefault;

	CString strSection (szSection);

	if (strSection.GetLength ()) {
		if (strSection.Find (_T ("\\Registry")) == -1) {
			if (strSection [0] == '\\')
				strSection = ::gstrRegRootKey + strSection;
			else
				strSection = ::gstrRegRootKey + _T ("\\") + strSection;
		}
	}

	return NtGetProfileString (strSection, szKey, szDefault);
}

CString NtGetProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szDefault)
{
	CString strResult = szDefault;

	if (!RTL::IsValid ())
		return strResult;

	const int nStart = 3;
	int nFound = nStart;
	std::vector <std::wstring> v = Encrypted::explode (szSection);
	CString strSection = CString (implode (std::vector <std::wstring> (&v [0], &v [nStart]), std::wstring (_T ("\\"))).c_str ());
	NTSTATUS status = -1;

	for (int i = nStart; i < v.size (); i++) {
		std::vector <CUnicodeString> vKeys;
		CUnicodeString ustrSection (strSection);
		CString strKey = v [i].c_str ();
		bool bFound = false;

		//TRACEF (ToString (i) + _T (": ") + (CString)ustrSection);
		NtEnumerateKeys (ustrSection, vKeys);

		for (int j = 0; !bFound && j < vKeys.size (); j++) {
			CString str = (CString)vKeys [j];

			if (IsEncoded (str))
				str = FoxjetDatabase::Encrypted::RSA::Decrypt (str);

			if (str == strKey) {
				strSection += _T ("\\") + str;
				bFound = true;
				nFound++;
			}
		}

		if (!bFound)
			break;
	}

	if (nFound == v.size ()) {
		const DWORD dwType = REG_SZ;
		HANDLE hKey = NULL;
		CUnicodeString uStrSection (strSection);
		OBJECT_ATTRIBUTES obj;

		obj.Length = sizeof(OBJECT_ATTRIBUTES);
		obj.RootDirectory = NULL;
		obj.ObjectName = &uStrSection;
		obj.Attributes = OBJ_CASE_INSENSITIVE;
		obj.SecurityDescriptor = NULL;
		obj.SecurityQualityOfService = NULL;

		NTSTATUS status = RTL::NtOpenKey (&hKey, KEY_READ, &obj);
	
		//TRACEF ((CString)uStrSection);

		if (NT_SUCCESS (status)) {
			DWORD dwDataSize = 0;
			CUnicodeString uKey ((CString)szKey);
			//BYTE * pBuffer = NULL;
			std::unique_ptr <BYTE> pBuffer;

			//TRACEF (CString (szKey) + _T (": ") + (CString)uKey);
			status = STATUS_SUCCESS;

			if (RTL::NtQueryValueKey (hKey, &uKey, KeyValuePartialInformation, NULL, 0, &dwDataSize) == STATUS_BUFFER_OVERFLOW) {
				do {
					//pBuffer = new BYTE [dwDataSize + 1024 + sizeof(WCHAR)]; 
					pBuffer.reset (new BYTE [dwDataSize + 1024 + sizeof (WCHAR)]);
					ASSERT (pBuffer.get ());

					if (pBuffer.get ()) 
						status = RTL::NtQueryValueKey (hKey, &uKey, KeyValuePartialInformation, pBuffer.get (), dwDataSize, &dwDataSize);

				} 
				while (status == STATUS_BUFFER_OVERFLOW);
			}
			else
			{
				//pBuffer = new BYTE [dwDataSize + 1024];
				pBuffer.reset (new BYTE [dwDataSize + 1024]);
				ASSERT (pBuffer.get ());

				if (pBuffer.get ()) 
					status = RTL::NtQueryValueKey (hKey, &uKey, KeyValuePartialInformation, pBuffer.get (), dwDataSize, &dwDataSize);
			}

			if (NT_SUCCESS (status) && pBuffer) {
				KEY_VALUE_PARTIAL_INFORMATION *info = (KEY_VALUE_PARTIAL_INFORMATION *)pBuffer.get ();

				strResult.Empty ();

				for (int i = 0; i < (int)(info->DataLength); i++) {
					char sz [2];

					_snprintf (sz, sizeof (sz), "%c", info->Data [i]);
					strResult += sz;
				}

				if (IsEncoded (strResult))
					strResult = FoxjetDatabase::Encrypted::RSA::Decrypt (strResult);
			}

			//if (pBuffer) 
			//	delete [] pBuffer;

			RTL::NtClose (hKey);
		}
	}

	return strResult;
}

#ifdef _DEBUG
DB_API void Encrypted::Trace (LPCTSTR lpszSection, int nDepth)
{
	CString str = NtTranslate (lpszSection);

	NtTraceRegKey (str, nDepth);
}
#endif

static CString NtTranslate (const CString & strSection)
{
	CString str = strSection;

	if (bHKEY_CURRENT_USER) {
		// \Registry\User\S-1-5-21-4214632267-836034253-4094818220-1001\SOFTWARE
		// \Registry\Machine\SOFTWARE\Foxjet<NULL>
	
		CString strCurrentUser	= str;
		CString strLocalMachine = ::gstrRegRootKey;

		strCurrentUser.Replace (_T ("\\"), _T ("/"));
		strLocalMachine.Replace (_T ("\\"), _T ("/"));

		std::vector <std::wstring> vLocalMachine = explode (strCurrentUser, (TCHAR)'/');
		std::vector <std::wstring> vCurrentUser = explode (strLocalMachine, (TCHAR)'/');
		std::vector <std::wstring> v;

		//TRACEF (::gstrRegRootKey);
		//TRACEF (str);

		if (vCurrentUser.size ()) {
			const CString strLast = vCurrentUser [vCurrentUser.size () - 1].c_str ();

			for (int i = 0; i < vLocalMachine.size (); i++) {
				v.push_back (vLocalMachine [i]);
				//TRACEF (implode (v, std::wstring (_T ("\\"))).c_str ());

				if (_tcsicmp (vLocalMachine [i].c_str (), strLast) == 0)
					break;
			}
		}

		CString strReplace = implode (v, std::wstring (_T ("\\"))).c_str ();
		
		//TRACEF (strReplace);
		//TRACEF (str);
		str.Replace (strReplace, ::gstrRegRootKey);
		//TRACEF (str);
	}

	return str;
}

DB_API void Encrypted::Delete (LPCTSTR lpszSection)
{
	CString str = NtTranslate (lpszSection);

	#ifdef _DEBUG
	Trace (str, 10);
	#endif

	NtDeleteRegKey (str);

	#ifdef _DEBUG
	Trace (str, 10);
	#endif
}

DB_API CString Encrypted::Enumerate (LPCTSTR lpszSection, std::vector <std::wstring> & v, DWORD dwFlags) 
{
	std::vector <CUnicodeString> vKeys;
	CString str = NtTranslate (lpszSection);

	if (dwFlags & ENUMERATE_VALUES) 
		NtEnumerateValues (str, vKeys);

	if (dwFlags & ENUMERATE_KEYS) 
		NtEnumerateKeys (str, vKeys);

	for (int i = 0; i < vKeys.size (); i++) {
		CString str = (CString)vKeys [i];

		if (IsEncoded (str))
			str = FoxjetDatabase::Encrypted::RSA::Decrypt (str);

		v.push_back (std::wstring (str));
	}

	return str;
}


DB_API CString Encrypted::GetInkCodes ()
{
	CString strKey = FoxjetDatabase::GetCmdlineValue (::GetCommandLine (), _T ("/enum="));
	CString strResult;

	if (strKey.GetLength ()) {
		std::vector <std::wstring> v;
		std::vector <std::wstring> vMAC = FoxjetDatabase::GetMacAddresses ();

		strResult += _T ("Warning threshold: ") + ToString (INK_CODE_THRESHOLD_WARNING) + _T ("%\n");
		strResult += _T ("Error threshold: ") + ToString (INK_CODE_THRESHOLD_ERROR) + _T ("%\n");

		strResult += _T ("\n\n\n");

		for (int i = 0; i < vMAC.size (); i++) 
			strResult += CString (vMAC [i].c_str ()) + _T ("\n");

		strResult += _T ("\n\n\n");

		FoxjetDatabase::Encrypted::Enumerate (strKey, v);

		for (int i = 0; i < v.size (); i++) {
			std::vector <std::wstring> vSub;
			CString strInkCode = CString (v [i].c_str ());
			CString strSubKey = strKey + _T ("\\") + strInkCode;
				
			strInkCode.Replace (_T ("<NULL>"), _T (""));
			strResult += strInkCode;

			FoxjetDatabase::Encrypted::Enumerate (strSubKey, vSub);

			for (int j = 0; j < vSub.size (); j++) {
				CString strValue = CString (vSub [j].c_str ());

				strResult += _T ("\n\t") 
					+ strValue + _T (": ") 
					+ FoxjetDatabase::Encrypted::GetProfileString (strSubKey, strValue, _T (""));
			}

			strResult += _T ("\n");
		}

		strResult += _T ("\n");
	}

	return strResult;
}

DB_API CString Encrypted::GetCommandLine (CString str)
{
	CString strHide [] = 
	{
		_T ("/enum="),
		_T ("/delete="),
	};

	if (str.IsEmpty ())
		str = ::GetCommandLine ();

	for (int i = 0; i < ARRAYSIZE (strHide); i++) 
		str.Replace (strHide [i] + _T ("\"") + FoxjetDatabase::GetCmdlineValue (str, strHide [i]) + _T ("\""), _T (""));

	return str;
}

DB_API bool Encrypted::IsValidInkCode (const std::vector <std::wstring> & vRegistry, const CString & str)
{
	using namespace FoxjetDatabase::Encrypted::InkCodes;

	CInkCode c;
	CTime tmNow = CTime::GetCurrentTime ();

	/*
	{ // TODO: rem
		bool bInvalid = false;

		TRACER (_T ("[ink code] \"") + str + _T ("\""));

		if (!c.Decode (str)) {
			TRACER (_T ("[ink code] Decode failed: \"") + str + _T ("\""));
			bInvalid = true;
		}

		if (!c.IsValid (&tmNow)) {
			TRACER (_T ("[ink code] IsValid failed: ") + str + tmNow.Format (_T (" [%c]")));
			bInvalid = true;
		}

		for (int i = 0; i < vRegistry.size (); i++) {
			CString strCmp = vRegistry [i].c_str ();

			strCmp.Replace (_T ("<NULL>"), _T (""));

			if (!str.CompareNoCase (strCmp)) {
				TRACER (_T ("[ink code] \"") + str + _T ("\" already used: \"") + strCmp + _T ("\""));
				bInvalid = true;
				break;
			}
		}

		if (!bInvalid)
			TRACER (_T ("[ink code] accepted: \"") + str + _T ("\""));

		keybd_event (VK_LWIN, 0, 0, 0);
		keybd_event (VK_LWIN, 0, KEYEVENTF_KEYUP, 0);

	}
	*/

	if (!c.Decode (str))
		return false;

	if (!c.IsValid (&tmNow))
		return false;

	for (int i = 0; i < vRegistry.size (); i++) {
		CString strCmp = vRegistry [i].c_str ();

		strCmp.Replace (_T ("<NULL>"), _T (""));

		if (!str.CompareNoCase (strCmp)) 
			return false;
	}

	return true;
}

static BOOL GetTextualSid(PSID pSid, LPTSTR szTextualSid, LPDWORD dwBufferLen) 
{
	PSID_IDENTIFIER_AUTHORITY psia;
	DWORD dwSubAuthorities;
	DWORD dwSidRev = SID_REVISION;
	DWORD dwCounter;
	DWORD dwSidSize;
	NTSTATUS m_NtStatus;

	BOOL bReturn = FALSE;
    try {{
		//
		// Test if SID passed in is valid.
		if(!IsValidSid(pSid)) {
            goto leave;
		}

		// Obtain SidIdentifierAuthority.
		psia = GetSidIdentifierAuthority(pSid);

		// Obtain sidsubauthority count.
		dwSubAuthorities = *GetSidSubAuthorityCount(pSid);

		// Compute buffer length.
		// S-SID_REVISION- + identifierauthority- + subauthorities- + NULL
		dwSidSize = (15 + 12 + (12 * dwSubAuthorities) + 1) * sizeof(TCHAR);

		// Check provided buffer length.
		// If not large enough, indicate proper size and setlasterror
		if (*dwBufferLen < dwSidSize) {
			*dwBufferLen = dwSidSize;
			SetLastError(ERROR_INSUFFICIENT_BUFFER);
			m_NtStatus = ERROR_INSUFFICIENT_BUFFER;
            goto leave;
		}

		// Prepare S-SID_REVISION-.
		dwSidSize = wsprintf(szTextualSid, TEXT("S-%lu-"), dwSidRev);

		// Prepare SidIdentifierAuthority.
		if ((psia->Value[0] != 0) || (psia->Value[1] != 0)) {
			dwSidSize += wsprintf(szTextualSid + lstrlen(szTextualSid),
									TEXT("0x%02hx%02hx%02hx%02hx%02hx%02hx"),
									(USHORT) psia->Value[0],
									(USHORT) psia->Value[1],
									(USHORT) psia->Value[2],
									(USHORT) psia->Value[3],
									(USHORT) psia->Value[4],
									(USHORT) psia->Value[5]);
	   
		} 
		else {
			dwSidSize += wsprintf(szTextualSid + lstrlen(szTextualSid),
									TEXT("%lu"),
									(ULONG) (psia->Value[5]      ) +
									(ULONG) (psia->Value[4] <<  8) +
									(ULONG) (psia->Value[3] << 16) +
									(ULONG) (psia->Value[2] << 24));
		}

		// Loop through SidSubAuthorities.
		for (dwCounter = 0; dwCounter < dwSubAuthorities; dwCounter++) {
			dwSidSize += wsprintf(szTextualSid + dwSidSize, TEXT("-%lu"), *GetSidSubAuthority(pSid, dwCounter));
		}
		bReturn = TRUE;
		//
    } leave:;
    } catch(...) {}

	return bReturn;
}

static BOOL LookupSID (CString &csSID)
{
	HANDLE		hToken			= NULL;
	PTOKEN_USER	ptgUser			= NULL;
	DWORD		cbBuffer		= 0;
	LPTSTR		szTextualSid	= NULL;
	DWORD		cbSid			= 36;
	TOKEN_INFORMATION_CLASS tic = TokenUser;

	CString csError = _T("Error");
	csSID = csError;

	// Obtain current process token.
	NTSTATUS m_NtStatus = RTL::NtOpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hToken);
	if(!NT_SUCCESS(m_NtStatus)) {
		TRACEF (FORMATMESSAGE (m_NtStatus));
		return FALSE;
	}

	// Obtain user identified by current process's access token.
	// Basically, query info in the token ;-)
	m_NtStatus = RTL::NtQueryInformationToken(hToken, 
										 tic,
										 ptgUser, 
										 cbBuffer, 
										 &cbBuffer);
	if(!NT_SUCCESS(m_NtStatus)) {
		//
		ptgUser = (PTOKEN_USER)RTL::RtlAllocateHeap(GetProcessHeap(), HEAP_ZERO_MEMORY, cbBuffer);
		if (!ptgUser) {
			TRACEF (FORMATMESSAGE (m_NtStatus));
			goto SIDEnd;
		}

		m_NtStatus = RTL::NtQueryInformationToken(hToken, 
											 tic,
											 ptgUser, 
											 cbBuffer, 
											 &cbBuffer); 
		if(!NT_SUCCESS(m_NtStatus)) {
			TRACEF (FORMATMESSAGE (m_NtStatus));
			goto SIDEnd;
		}
	}

	cbSid = 512;
	szTextualSid = (LPTSTR)RTL::RtlAllocateHeap(GetProcessHeap(), HEAP_ZERO_MEMORY, cbSid);
	if (!szTextualSid) {
		TRACEF (FORMATMESSAGE (m_NtStatus));
		goto SIDEnd;
	}

	// Obtain the textual representation of the SID.
	if (!GetTextualSid( ptgUser->User.Sid,	// user binary Sid
						szTextualSid,		// buffer for TextualSid
						&cbSid))			// size/required buffer
	{
		m_NtStatus = GetLastError();
		TRACEF (FORMATMESSAGE (m_NtStatus));
		goto SIDEnd;
	}

	// the TextualSid representation.
	csSID.Format(_T("%s"), szTextualSid);
   

SIDEnd:

	// Free resources.
	if (hToken) {
		RTL::NtClose(hToken);
	}

	if (ptgUser) {
		RTL::RtlFreeHeap(GetProcessHeap(), 0, ptgUser);
	}

	if (szTextualSid) {
		RTL::RtlFreeHeap(GetProcessHeap(), 0, szTextualSid);
	}

	return TRUE;
}

void NtEnumerateKeys (const CUnicodeString & ustrSection, std::vector <CUnicodeString> & v) // TODO: move
{
	HANDLE hKey = NULL;
	CUnicodeString ustr = ustrSection;
	OBJECT_ATTRIBUTES obj;

	if (!RTL::IsValid ())
		return;

	obj.Length = sizeof(OBJECT_ATTRIBUTES);
	obj.RootDirectory = NULL;
	obj.ObjectName = &ustr;
	obj.Attributes = OBJ_CASE_INSENSITIVE;
	obj.SecurityDescriptor = NULL;
	obj.SecurityQualityOfService = NULL;

	//TRACEF ((CString)ustrSection);
	NTSTATUS status = RTL::NtOpenKey (&hKey, KEY_READ, &obj);
	
	if (NT_SUCCESS (status)) {
		DWORD dw = 0;
		CHAR sz [2048] = { 0 };
		
		for (int i = 0; NT_SUCCESS (status = RTL::NtEnumerateKey (hKey, i, KeyBasicInformation, sz, sizeof (sz), &dw)); i++) {
			PKEY_BASIC_INFORMATION pInfo = (PKEY_BASIC_INFORMATION)sz;
			
			if (pInfo && pInfo->NameLength) 
				v.push_back (CUnicodeString (pInfo->Name, pInfo->NameLength));

			memset (sz, 0, sizeof (sz));
		}

		RTL::NtClose (hKey);
	}
}

void NtEnumerateValues (const CUnicodeString & ustrSection, std::vector <CUnicodeString> & v) // TODO: move
{
	HANDLE hKey = NULL;
	CUnicodeString ustr = ustrSection;
	OBJECT_ATTRIBUTES obj;

	if (!RTL::IsValid ())
		return;

	obj.Length = sizeof(OBJECT_ATTRIBUTES);
	obj.RootDirectory = NULL;
	obj.ObjectName = &ustr;
	obj.Attributes = OBJ_CASE_INSENSITIVE;
	obj.SecurityDescriptor = NULL;
	obj.SecurityQualityOfService = NULL;

	//TRACEF ((CString)ustrSection);
	NTSTATUS status = RTL::NtOpenKey (&hKey, KEY_READ, &obj);
	
	if (NT_SUCCESS (status)) {
		DWORD dw = 0;
		CHAR sz [2048] = { 0 };
		
		for (int i = 0; NT_SUCCESS (status = RTL::NtEnumerateValueKey (hKey, i, KeyValueBasicInformation, sz, sizeof (sz), &dw)); i++) {
			PKEY_VALUE_BASIC_INFORMATION pInfo = (PKEY_VALUE_BASIC_INFORMATION)sz;

			if (pInfo && pInfo->NameLength) 
				v.push_back (CUnicodeString (pInfo->Name, pInfo->NameLength));

			memset (sz, 0, sizeof (sz));
		}

		RTL::NtClose (hKey);
	}
}


bool NtDeleteRegKey (const CUnicodeString & ustrSection)
{
	bool bResult = false;
	HANDLE hKey = NULL;
	CUnicodeString ustr = ustrSection;
	OBJECT_ATTRIBUTES obj;

	if (!RTL::IsValid ())
		return false;

	{
		std::vector <CUnicodeString> v;

		NtEnumerateKeys (ustr, v);
		NtEnumerateValues (ustr, v);

		for (int i = 0; i < v.size (); i++) {
			CUnicodeString ustrSubkey = (CString)ustr + _T ("\\") + (CString)v [i];

			NtDeleteRegKey (ustrSubkey);
		}
	}

	obj.Length = sizeof(OBJECT_ATTRIBUTES);
	obj.RootDirectory = NULL;
	obj.ObjectName = &ustr;
	obj.Attributes = OBJ_CASE_INSENSITIVE;
	obj.SecurityDescriptor = NULL;
	obj.SecurityQualityOfService = NULL;

	NTSTATUS status = RTL::NtOpenKey (&hKey, GENERIC_ALL /* KEY_ALL_ACCESS */ , &obj);
	//TRACEF ((CString)ustr);
	
	// NtDeleteValueKey

	if (!NT_SUCCESS (status)) 
		TRACEF (FORMATMESSAGE (status));
	else {
		status = RTL::NtDeleteKey(hKey);

		if (!NT_SUCCESS (status)) 
			TRACEF (FORMATMESSAGE (status));

		bResult = NT_SUCCESS (status) ? true : false;
	}

	if (hKey)
		RTL::NtClose (hKey);

	return bResult;
}

#ifdef _DEBUG
void NtFindHiddenKeys (const CUnicodeString & ustrSection)
{
	if (!RTL::IsValid ())
		return;

	CUnicodeString ustr = ustrSection;
	std::vector <CUnicodeString> v;

	if (CString (ustr).Find (_T ("<NULL>")) != -1)
		TRACEF (CString (ustr));

	NtEnumerateKeys (ustr, v);
	NtEnumerateValues (ustr, v);

	for (int i = 0; i < v.size (); i++) {
		CUnicodeString ustrSubkey = (CString)ustr + _T ("\\") + (CString)v [i];

		if (CString (ustrSubkey).Find (_T ("<NULL>")) != -1)
			TRACEF ((CString)ustrSubkey);

		NtFindHiddenKeys (ustrSubkey);
	}
}
#endif


void NtTraceRegKey (const CUnicodeString & ustrSection, int nDepth) // TODO: move
{
	if (!RTL::IsValid () || nDepth <= 0)
		return;

	CUnicodeString ustr = ustrSection;
	std::vector <CUnicodeString> v;
	HANDLE hKey = NULL;
	OBJECT_ATTRIBUTES obj;

	obj.Length = sizeof(OBJECT_ATTRIBUTES);
	obj.RootDirectory = NULL;
	obj.ObjectName = &ustr;
	obj.Attributes = OBJ_CASE_INSENSITIVE;
	obj.SecurityDescriptor = NULL;
	obj.SecurityQualityOfService = NULL;

	NTSTATUS status = RTL::NtOpenKey (&hKey, KEY_READ, &obj);

	if (NT_SUCCESS (status)) {
		//TRACEF ((CString)ustr);

		NtEnumerateKeys (ustr, v);
		NtEnumerateValues (ustr, v);

		for (int i = 0; i < v.size (); i++) {
			CUnicodeString ustrSubkey = (CString)ustr + _T ("\\") + (CString)v [i];
			std::vector <std::wstring> vKey = Encrypted::explode ((CString)ustrSubkey);

			if (vKey.size ()) {
				CString strKey = vKey [vKey.size () - 1].c_str ();
				CString strSection = implode (std::vector <std::wstring> (vKey.begin (), vKey.end () - 1), std::wstring (_T ("\\"))).c_str ();

				//TRACEF ((CString)ustr);
				//TRACEF ((CString)v [i]);
				//TRACEF ((CString)ustrSubkey);
				//TRACEF ((CString)ustrSection);
				//TRACEF (strSection + _T (" ") + strKey);

				CString str = (CString)ustrSubkey;
				CString strValue = NtGetProfileString (strSection, strKey, _T (""));

				if (strValue.GetLength ())
					str += _T ("=") + strValue;
				TRACEF (str);

				//TRACEF ("");
				NtTraceRegKey (ustrSubkey, nDepth - 1);
			}
		}
	}
}

static CString GetSubKey ()
{
	// \ink codes<NULL>
	return FoxjetDatabase::Encrypted::RSA::Decrypt (
		_T ("H7tzj/O3Wb+iabKs8tVYk8smENwWS83q0TzY146IgH8ZdgC/qGmGlQpd8wks")
		_T ("AarjOLpo/ZdDUSbwo9r71MDopYjom1oTYmqW1PZa9ROELbxAXOCiq/JSn0tC")
		_T ("OAAbbIcEe5x90g78/AnaPhtlzuQuwl3mhSE7BiMbSsOyR4PIoBaiJ40Jtgfr")
		_T ("3mlqDaTLBQJG+xy5FXTX8sPNHFeHNAnaCukN0WMarKQCLyNf34Ua1gQso0TC")
		_T ("cWDBQvj7be9w22QwyOfc1T0goP5EKUg+KLnSSTO32Q+T7XaoZ6r3BswSapjX")
		_T ("b9FLUTO8dcTJ5rUXFupvm3xbPdrhiJM589NN8LKgZw=="));
}

static CString GetEnabled ()
{
	//Enabled
	return FoxjetDatabase::Encrypted::RSA::Decrypt (
		_T ("HuHkqRI3n9our5MWL7Qk+obBgBSZrvpn/PHIwspm2P3iv2ANO/eiJsIAFCl5")
		_T ("UjB/TnvYdkMy4FdAwIPbHFSL7aGLXTd+odFfVtPSf+ttiJfF/Wd6dOohVT4p")
		_T ("2W6lKJkcV8GLsBPO0HV8aIItQpP5LgU1OnSZECITdQFvsqDcjvhDI0z1auVb")
		_T ("/vVRoa78v8dhKUGVTbLlqk2GVt/fRGKwtLwrdgE4rCb8yedcPlEcLIjYUolz")
		_T ("VYydqhO4tpGEtQO11k9+kYMTpZD23dUwVQpLQeW6/SEzgbp/MKPa3L/yaXQY")
		_T ("/eBsJ/BRf/xzZuNmrWmMQaibvOd8VL4KlECDvw+i6A=="));
}

static CString GetLockTime ()
{
	// Lock time
	return FoxjetDatabase::Encrypted::RSA::Decrypt (
		_T ("u1KI0DHD3Tu83RaIaHZzyvQVKbEL/ktNKuUR4MS+rF1QLxpAnMAsBiIjq")
		_T ("O7afccnFTB6Ew8gODCrOLY9PE5HGn07p0KVIGgaDXYmJwcmtO7JP37nZ/e/T")
		_T ("BtzO3p+09tcA+/V4j2nQuTFRPzDp/gLZl5Iptz2tnffKW+ggSeT/VDuCZ6Us")
		_T ("UrELSn5WMAkzMT/vx3FWXlL8y9zX+wXn9CJqv5Y/CLFX4XXbtEM4df9OtxWK")
		_T ("UXRwqhu18dXVrIBuYwCWwALUvAEiiM67WG5wCW0k5pIo3cD9O56Fnkv19a/P")
		_T ("JmLtnAGRemlljLB8ZtJHmApjIRA1j5S8vsF0YHKOZQa2g=="));
}

DB_API void Encrypted::InkCodes::SetLockout (const CTime * ptm)
{
	CString strKey = GetRegRootSection (); //GetRegSection ();

	//strKey.Replace (GetSubKey (), _T (""));

	if (strKey.GetLength ()) {
		if (ptm)
			Encrypted::WriteProfileString (strKey, GetLockTime (), ptm->Format (_T ("%c")));
		else
			Encrypted::WriteProfileString (strKey, GetLockTime (), _T (""));
	}
}

DB_API bool Encrypted::InkCodes::GetLockout (CTime & tm)
{
	CString strKey = GetRegRootSection (); //GetRegSection ();

	//strKey.Replace (GetSubKey (), _T (""));

	if (strKey.GetLength ()) {
		const CString strMissing = _T ("[missing]");
		CString str = Encrypted::GetProfileString (strKey, GetLockTime (), strMissing);

		if (str.GetLength () && str != strMissing) {
			COleDateTime tmParse;

			if (tmParse.ParseDateTime (str)) {
				tm = CTime (tmParse.GetYear (),
					tmParse.GetMonth (),
					tmParse.GetDay (),
					tmParse.GetHour (),
					tmParse.GetMinute (),
					tmParse.GetSecond ());
				return true;
			}
		}
	}

	return false;
}

DB_API void Encrypted::InkCodes::Enable (bool bEnable)
{
	CString strKey = GetRegRootSection (); //GetRegSection ();

	//strKey.Replace (GetSubKey (), _T (""));

	if (strKey.GetLength ()) 
		Encrypted::WriteProfileString (strKey, GetEnabled (), bEnable ? _T ("1") : _T ("0"));
}

DB_API bool Encrypted::InkCodes::IsEnabled ()
{
	CString strResult, strKey = GetRegRootSection (); //GetRegSection ();

	//strKey.Replace (GetSubKey (), _T (""));

	if (strKey.GetLength ())
		strResult = Encrypted::GetProfileString (strKey, GetEnabled (), _T ("0"));

	return _ttoi (strResult) ? true : false;
}

static bool NtCanOpen (const CString & strKey)
{
	bool bResult = false;
	HANDLE hKey = NULL;
	CUnicodeString ustr = strKey;
	OBJECT_ATTRIBUTES obj;

	obj.Length = sizeof(OBJECT_ATTRIBUTES);
	obj.RootDirectory = NULL;
	obj.ObjectName = &ustr;
	obj.Attributes = OBJ_CASE_INSENSITIVE;
	obj.SecurityDescriptor = NULL;
	obj.SecurityQualityOfService = NULL;

	NTSTATUS status = RTL::NtOpenKey (&hKey, GENERIC_ALL, &obj);
	//TRACEF ((CString)ustr);
	
	// NtDeleteValueKey

	if (NT_SUCCESS (status)) 
		bResult = true;
	
	if (hKey)
		RTL::NtClose (hKey);
	
	return bResult;
}

DB_API CString Encrypted::InkCodes::GetRegRootSection ()
{
	CString strKey = GetRegSection ();

	strKey.Replace (GetSubKey (), _T (""));

	return strKey;
}

DB_API CString Encrypted::InkCodes::GetRegSection ()
{
	static bool bInit = false;

	//_T ("\\Registry\\Machine\\SOFTWARE")
	static const CString strRegistryMachineSOFTWARE = 
		FoxjetDatabase::Encrypted::RSA::Decrypt (
			_T ("apkdtFvrGRSJsxD+lYFoLLXpSMjvehmjrIdYa1bteZv+ekktE2NV+2Ma7Y5yvO5PjwKWi1roQ")
			_T ("/CGnI/Z6Rt9Gfi8ljBFxgjOdcfvI+grHnyXusRtN6Ayh4+FFw/pzI8BurgSH5IVn6GtgqRFlO")
			_T ("/KVRGmcZVgCMl4eKIFY+czzybK3Ox+/21m85ihCPyKjIrjIQgd82/CZPg5hi7pYEoZT7wnlFZ")
			_T ("JsrivTZFZR7zMrdMkSWLE8T06CI4Wa14HMAdUJNWGxo1KLcFDiyPL7dJZjTRosPEyYWy2bK7u")
			_T ("jEtwuXrt+dR4Gk1WDB0eVLp+y5iJ8oUYcAdTYA9eTyjBhVanuw=="));

	static const CString strFoxjetSettingsInkCodes = 
		//_T ("\\Foxjet<NULL>\\Settings<NULL>"))
		FoxjetDatabase::Encrypted::RSA::Decrypt (
			_T ("WLnnDS/48E123kf3kk6Ybj6ZoXcZzGV9yw8DV24TAt8uPmnOK6xxzzg//5s2owe1JalJYfGHi")
			_T ("lF1nqhygVFkaaKY52YS3Od+FQiP8DOA8KXb4gwbI8/PTVZrGDZk108NJ709nz1r2ErJCegPdw")
			_T ("mU3arYxzmh8NnRyfgvpwvm6cW+/Pv8h0Q/Dw37qieMfaO+EuC6n2kZBof2/8AznCFrTBNWK+/")
			_T ("ba6dPVD7ht0FXhJXeSAfuFCa/RB6iubsQ1ZhPL5diQPWREdiVIj822Rll9Sa6bXtE3XyeAXcQ")
			_T ("/d5qhnXS5CnjPcZBhQbQAGzbIIxmG/jovKA7D5/HqCn/3g77QQ==")) +
		// _T ("\\ink codes<NULL>")
		GetSubKey ();

	static CString strResult = strRegistryMachineSOFTWARE + strFoxjetSettingsInkCodes;

	if (!RTL::IsValid ())
		return _T ("");

	if (!bInit) {
		TRACEF (strResult);
	
		if (!NtCanOpen (strRegistryMachineSOFTWARE)) {
			strResult = ::gstrRegRootKey + strFoxjetSettingsInkCodes;

			VERIFY (NtCanOpen (::gstrRegRootKey));
			::bHKEY_CURRENT_USER = true;
		}
		
		bInit = true;
	}

	return strResult;

	/*
	// \Registry\Machine\SOFTWARE\Foxjet<NULL>\Settings<NULL>\ink codes<NULL>
	return FoxjetDatabase::Encrypted::RSA::Decrypt (
		_T ("a35IZvL+2c7L2Khn/sSvGdJwoqphF7yZVZGHgjQmNZHoa8TXxtocR6lIhW4/")
		_T ("BCve7A8rSLEPrJwX4hej/c/80laGEBp0nMlsNepO5WinDt6GwhU4O2Ys4iqf")
		_T ("aSudZ4jeRjIj+uwdx/mDOaXi2tUcXfdIMXvdw/M49nLxfz9CkJHJ6m4HGxs3")
		_T ("FU0PzTOmnuCXHBDJAZGAkIIMei0ihFjToMMiygXw9HldKXXsdDiJfFSsV02g")
		_T ("mVYsYWNsN7LHgjERJwhe3GrpDCbiFFG7t+mwaxjc4X6zU4sX31O8FwF1BGse")
		_T ("u3i3l4g/u8GT2DgJhj7hWOhvmr05Uf9B9dyI/lI4sQ=="));
	*/
}


DB_API CString Encrypted::InkCodes::GetEnablePassword ()
{
	// tn6ur3
	return FoxjetDatabase::Encrypted::RSA::Decrypt (
		_T ("Msc71j35zwQUETKpqBTHiFeoVugGGGurwRVDr+Y9QOTrQ6iXCFjPtksgCGVob")
		_T ("iWSrqEjQLyr5M3JyiyWoiPymrkqIjM8CCpSoSy3cqHK6VsTouv44cZBHQOCx4")
		_T ("DMDN6cBVL4KX/y8aWeWgkobBAmZFohQJpwc402RJGElQjwPlRuEmNPdaPwH2v")
		_T ("WFDTjhwhiTlQ5BDfkJZQrYJp7stsw+qgzhXOEbBtHbHeEJol6SPISo79pGP6e")
		_T ("RGtYDQrSXSlivlYdd8Vlla8DAbjoK9fHKDtkj5wxsVjbPsw2Gbm3NuWDrP0En")
		_T ("gfDt79rxLhnUaVWVLDQb2CZ1qihytCjC6bmJg=="));
}

DB_API CString Encrypted::InkCodes::GetDisablePassword ()
{
	return ::GetDisablePassword (&CTime::GetCurrentTime ());

	//// 0aw3j1
	//return FoxjetDatabase::Encrypted::RSA::Decrypt (
	//	_T ("N6yuOvgBJ4W+UEHocrKqwaTyl3HbUX9NS2/pVdSPxTkCBFUpACyD4wNGj31dS")
	//	_T ("zCWSHdzJ9FQT7GnUnUpTQ+czZwyrdEKeXoiVd0ccVDY5wyqsMS3GB7g+LPzH2")
	//	_T ("QiJJqRVmpHq/yzZE4Bbc4lzn1NA1DDrm2DVyM5kCpbVcftRvaRfM3Xx2fWeSa")
	//	_T ("6w8gmQBorzULWMJ7uDL7pJVHG79YkgXrYZF3pM9hTsZZiKlmEh8I2Klz1aA8S")
	//	_T ("haQllupDldgbtzwP6no9qwhnWBSadCFGAc55oSBXOqOIvtQDEQn9WgnYYP/Kx")
	//	_T ("YMFSuZll9KvPFBb162RtIRspka5jdRXwtlhJA==")
	//);
}

DB_API void Encrypted::InkCodes::Restore (const CString & strFile)
{
	CString str = FoxjetFile::ReadDirect (strFile);

	if (str.GetLength ()) {
		str.Replace (_T ("\r"), _T (""));
		std::vector <std::wstring> vDecrypted, vEncrypted = ::explode (std::wstring (str), '\n');
		std::vector <std::wstring> vMAC = GetMacAddresses ();
		bool bMatch = false;

		for (int n = 0; n < vEncrypted.size (); n++) {
			CString strDecrypted = FoxjetDatabase::Encrypted::RSA::Decrypt (vEncrypted [n].c_str ());
			CString s = Extract (strDecrypted, _T ("MAC="));

			if (!s.GetLength ())
				vDecrypted.push_back (std::wstring (strDecrypted));
			else {
				for (int i = 0; !bMatch && i < vMAC.size (); i++) 
					if (!s.CompareNoCase (vMAC [i].c_str ()))
						bMatch = true;
			}
		}

		#ifdef _DEBUG
		for (int n = 0; n < vDecrypted.size (); n++) {
			TRACEF (vDecrypted [n].c_str ());
		}
		#endif

		if (bMatch) {
			const CString strRootKey = GetRegRootSection ();

			for (int n = 0; n < vDecrypted.size (); n++) {
				CString s = vDecrypted [n].c_str ();
				CStringArray v;
				CString strSection, strKey;

				//TRACEF (s);
				FromString (s, v);

				if (v.GetSize ()) {
					strKey = v [0];

					if (strKey.GetLength () && strKey [0] == '\\')
						strKey.Delete (0);

					std::vector <std::wstring> vKey = Encrypted::explode (strRootKey + _T ("\\") + strKey);

					if (vKey.size ()) {
						strKey = vKey [vKey.size () - 1].c_str ();
						strSection = implode (std::vector <std::wstring> (vKey.begin (), vKey.end () - 1), std::wstring (_T ("\\"))).c_str ();
					}
				}

				if (!strSection.CompareNoCase (strRootKey) && !GetSubKey ().CompareNoCase (_T ("\\") + strKey))
					continue;

				if (v.GetSize () == 1) {

					VERIFY (Encrypted::WriteProfileString (strSection, strKey, _T ("")));

					#ifdef _DEBUG
					//CString strDebug = Encrypted::GetProfileString (strSection, strKey, _T ("[$_error]"));
					//ASSERT (strDebug == _T (""));
					#endif //_DEBUG
				}
				else if (v.GetSize () == 2) {
					CString strValue = v [1];

					VERIFY (Encrypted::WriteProfileString (strSection, strKey, strValue));

					#ifdef _DEBUG
					CString strDebug = Encrypted::GetProfileString (strSection, strKey, _T ("[$_error]"));
					ASSERT (strDebug == strValue);
					#endif //_DEBUG
				}

				//#ifdef _DEBUG
				//FoxjetDatabase::Encrypted::Trace (strRootKey, 20);
				//int nBreak = 0;
				//#endif //_DEBUG
			}

			#ifdef _DEBUG
			FoxjetDatabase::Encrypted::Trace (strRootKey, 10);
			#endif //_DEBUG
		}

		::DeleteFile (strFile);
	}
}
