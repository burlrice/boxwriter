// OdbcDatabase.h: interface for the COdbcDatabase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ODBCDATABASE_H__D7179393_A09F_44B4_83C8_C18F476228A2__INCLUDED_)
#define AFX_ODBCDATABASE_H__D7179393_A09F_44B4_83C8_C18F476228A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxdb.h>
#include <afxmt.h>
#include <afxtempl.h>
#include "DbApi.h"
#include "DiagnosticSingleLock.h"

//#define _DEBUG_DBMUTEX

namespace FoxjetDatabase
{
	typedef void (CALLBACK * PDISPLAYSTATUS)(const CString & str, bool bDisplay);
	typedef PDISPLAYSTATUS LPDISPLAYSTATUS;  

	class DB_API COdbcTable;
	class DB_API COdbcRecordset;
	class DB_API COdbcBulkRecordset;
	class DB_API COdbcDatabase;

	class DB_API COdbcCache
	{
	public:
		COdbcCache (COdbcDatabase * p, const CString & strKey);
		COdbcCache (COdbcRecordset * p, const CString & strKey);
		COdbcCache (const COdbcCache &);				
		COdbcCache & operator = (const COdbcCache &);	
		virtual ~COdbcCache ();

		COdbcDatabase & GetDB ();
		COdbcRecordset & GetRst ();
		int GetActiveCount () const;
		CString GetDSN () const;
		CString GetKey () const { return m_strKey; }

		COdbcDatabase * m_pDB;
		COdbcRecordset * m_pRst;

	protected:
		CString m_strKey;
	};

	class DB_API COdbcDatabase : public CDatabase  
	{
		friend class COdbcCache;

	public:
		COdbcDatabase(LPCTSTR lpszFile, ULONG lLine);
		virtual ~COdbcDatabase();
	
	public:
		void CloseAllRecordsets();

		virtual BOOL OpenEx (LPCTSTR lpszConnectString, DWORD dwOptions = 0);
		BOOL OpenEx(LPCTSTR lpszConnectString, DWORD dwOptions, int);
		BOOL Connect(DWORD dwOptions, int);
		virtual void Close ();
		int GetTableCount () const;
		const COdbcTable * GetTable (int nIndex) const;

		int ExecuteSQL (LPCTSTR lpszSQL); //throw( CDBException )
		CString GetSqlServerDatabaseName ();
		
		CString FormatSQL (const CString & strSQL);

		bool HasTable (const CString & str) const;
		bool IsTransactionPending () const { return m_bTransaction; }

		//CMutex m_mutex;
		CDiagnosticCriticalSection m_cs;
		const CString m_strFile;
		const ULONG m_lLine;

		CString GetDriver () const;
		void CopyTables (CPtrArray & v);
		//void GetTables (const CString & strType, CPtrArray & v);

		bool IsOracle ();
		bool IsSqlServer ();
		static bool IsSqlServer (const CString & strDriver);
		bool IsQunect ();
		bool IsSage ();
		bool IsProgress ();

		UINT GetOpenType ();

		static void FreeTables (CPtrArray & v);
		static void GetDsnEntries (CStringArray & v);
		static CString GetFilePath (const CString & strDSN);


		static COdbcCache Find (const CString & strDSN);
		static COdbcCache Find (COdbcDatabase & db, const CString & strDSN, const CString & strKey);
		static COdbcCache Find (COdbcDatabase & db, const CString & strKey);

		static bool FreeRstCache (const CString & strSQL = _T (""));
		static void FreeDsnCache ();
		static void PurgeDsnCache (const CString & strExceptDSN);
		static bool IsLocal ();

		static void RegisterDisplayFct (LPDISPLAYSTATUS lpFct);
		static bool OpenDB (COdbcDatabase * p, const CString & strDSN);
		static void CloseDB (COdbcDatabase * p);

		static CString CreateSQL (FoxjetDatabase::COdbcDatabase * pdb, const CString & strDSN, const CString & strTable, 
			const CString & strField, const CString & strKeyField, const CString & strKeyValue,
			int nSQLType, bool bSW0876 = false);
		static int FindField (const CString & strDSN, const CString & strTable, const CString & strField);
		static int FindTable (const CString & strDSN, const CString & strTable);
		static void CALLBACK DisplayStatus (const CString & strDisplay, bool bDisplay);

		static COdbcDatabase * GetDB (DWORD dwThreadID, LPCTSTR lpszDSN = _T (""));
		static CString GetAppDsn ();
		static void SetAppDsn (const CString & strDSN);
		static int GetInstanceCount ();
		static void TraceInstances ();
		static void ClearStatus ();

		DWORD m_dwOwnerThreadID;
		const DWORD m_dwCreatedThreadID;
		bool m_bTransaction;

	protected:
		static bool OpenDB (COdbcDatabase * p, const CString & strDSN, int);
		void GetTables ();
		// throws CDBException, CMemoryException

		void FreeTables ();

		CPtrArray m_vTables;
		CString m_strDriver;
		int m_nActive;

		static LPDISPLAYSTATUS m_lpFct;
		static int m_nInstances;
	};
}; //namespace FoxjetDatabase

// #define DBMANAGETHREADSTATE(db)		CDiagnosticSingleLock _lock ((CSyncObject *)(LPVOID)&(db).m_cs, TRUE, _T (__FILE__), __LINE__, -1, _T (#db))
#define DBMANAGETHREADSTATE(db)		LOCK(* (CDiagnosticCriticalSection  *)(LPVOID)&((db).m_cs))

#endif // !defined(AFX_ODBCDATABASE_H__D7179393_A09F_44B4_83C8_C18F476228A2__INCLUDED_)
