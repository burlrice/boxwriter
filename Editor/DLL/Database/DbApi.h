#ifndef __DBAPI_H__
#define __DBAPI_H__

#ifdef __BUILD_DATABASE__
	#define DB_API __declspec(dllexport)
#else
	#define DB_API __declspec(dllimport)
#endif //__BUILD_DATABASE__

#endif //#ifndef __DBAPI_H__
