#ifndef __SERIALIZE_H__
#define __SERIALIZE_H__

#pragma once

#include "Database.h"

namespace FoxjetDatabase
{
	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, BOXSTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, BOXSTRUCT & s);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, HEADSTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, HEADSTRUCT & s);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, LINESTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, LINESTRUCT & s);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, PANELSTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, PANELSTRUCT & s);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, MESSAGESTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, MESSAGESTRUCT & s);
	DB_API const COdbcBulkRecordset & operator >> (const COdbcBulkRecordset & rst, CArray <MESSAGESTRUCT, MESSAGESTRUCT &> & v);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, TASKSTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, TASKSTRUCT & s);
	DB_API const COdbcBulkRecordset & operator >> (const COdbcBulkRecordset & rst, CArray <TASKSTRUCT, TASKSTRUCT &> & v);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, BOXLINKSTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, BOXLINKSTRUCT & s);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, IMAGESTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, IMAGESTRUCT & s);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, OPTIONSSTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, OPTIONSSTRUCT & s);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, OPTIONSLINKSTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, OPTIONSLINKSTRUCT & s);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, SECURITYGROUPSTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, SECURITYGROUPSTRUCT & s);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, USERSTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, USERSTRUCT & s);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, SHIFTSTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, SHIFTSTRUCT & s);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, REPORTSTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, REPORTSTRUCT & s);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, BCSCANSTRUCT & s);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, BCSCANSTRUCT & s);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, DELETEDSTRUCT & ls);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, DELETEDSTRUCT & ls);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, SETTINGSSTRUCT & ls);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, SETTINGSSTRUCT & ls);
	// throw CDBException, CMemoryException 

	DB_API const COdbcRecordset & operator >> (const COdbcRecordset & rst, PRINTERSTRUCT & ls);
	DB_API COdbcRecordset & operator << (COdbcRecordset & rst, PRINTERSTRUCT & ls);
	// throw CDBException, CMemoryException 

}; //namespace FoxjetDatabase

#endif //__SERIALIZE_H__
