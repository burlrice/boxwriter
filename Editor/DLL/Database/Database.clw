; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
ClassCount=15
ResourceCount=6
NewFileInclude1=#include "stdafx.h"
Class1=CTaskRecordset
LastClass=CMsgBoxDlg
LastTemplate=CDialog
Class2=CLinkRecordset
Class3=CHeadRecordset
Class4=CPhotocellRecordset
Class5=CConveyorRecordset
Class6=CMessageRecordset
Class7=CLibraryRecordset
Class8=CShiftRecordset
Class9=CTestRst
Class10=CTmpRst
Class11=COdbcColumns
Class12=COdbcTables
Class13=CCompactDlg
Resource1=IDD_COMPACT
Class14=CBackupDlg
Resource2=IDD_DATABASEPARAMS_MATRIX
Resource3=IDD_BACKUP_SM
Resource4=IDD_BACKUP
Resource5=IDD_DATABASEPARAMS
Class15=CMsgBoxDlg
Resource6=IDD_STATUS

[CLS:CTaskRecordset]
Type=0
HeaderFile=TaskRecordset.h
ImplementationFile=TaskRecordset.cpp
BaseClass=CRecordset
Filter=N
VirtualFilter=r

[DB:CTaskRecordset]
DB=1
DBType=ODBC
ColumnCount=4
Column1=[ID], 4, 4
Column2=[Name], 12, 100
Column3=[LibraryID], 4, 4
Column4=[Desc], -1, 2147483647

[CLS:CLinkRecordset]
Type=0
HeaderFile=LinkRecordset.h
ImplementationFile=LinkRecordset.cpp
BaseClass=CRecordset
Filter=N
VirtualFilter=r

[DB:CLinkRecordset]
DB=1
DBType=ODBC
ColumnCount=3
Column1=[TaskID], 4, 4
Column2=[MessageID], 4, 4
Column3=[Head], 4, 4

[CLS:CHeadRecordset]
Type=0
HeaderFile=HeadRecordset.h
ImplementationFile=HeadRecordset.cpp
BaseClass=CRecordset
Filter=N
VirtualFilter=r

[DB:CHeadRecordset]
DB=1
DBType=ODBC
ColumnCount=15
Column1=[ID], 4, 4
Column2=[ConveyorID], 4, 4
Column3=[IPAddress], 12, 30
Column4=[ExtEnc], -7, 1
Column5=[ConveyorSpeed], 5, 2
Column6=[Direction], -6, 1
Column7=[Photocell], -6, 1
Column8=[Channels], 5, 2
Column9=[HorzRes], 4, 4
Column10=[PrintDelay], 4, 4
Column11=[Master], -7, 1
Column12=[Enabled], -7, 1
Column13=[x], 8, 8
Column14=[y], 8, 8
Column15=[z], 8, 8

[CLS:CPhotocellRecordset]
Type=0
HeaderFile=PhotocellRecordset.h
ImplementationFile=PhotocellRecordset.cpp
BaseClass=CRecordset
Filter=N
VirtualFilter=r

[DB:CPhotocellRecordset]
DB=1
DBType=ODBC
ColumnCount=3
Column1=[ID], 4, 4
Column2=[Enabled], -7, 1
Column3=[Order], 4, 4

[CLS:CConveyorRecordset]
Type=0
HeaderFile=ConveyorRecordset.h
ImplementationFile=ConveyorRecordset.cpp
BaseClass=CRecordset
Filter=N
VirtualFilter=r

[DB:CConveyorRecordset]
DB=1
DBType=ODBC
ColumnCount=2
Column1=[ID], 4, 4
Column2=[Name], 12, 100

[CLS:CMessageRecordset]
Type=0
HeaderFile=MessageRecordset.h
ImplementationFile=MessageRecordset.cpp
BaseClass=CRecordset
Filter=N
VirtualFilter=r

[DB:CMessageRecordset]
DB=1
DBType=ODBC
ColumnCount=5
Column1=[ID], 4, 4
Column2=[LibraryID], 4, 4
Column3=[Name], 12, 100
Column4=[Desc], -1, 2147483647
Column5=[Data], -1, 2147483647

[CLS:CLibraryRecordset]
Type=0
HeaderFile=LibraryRecordset.h
ImplementationFile=LibraryRecordset.cpp
BaseClass=CRecordset
Filter=N
VirtualFilter=r

[DB:CLibraryRecordset]
DB=1
DBType=ODBC
ColumnCount=3
Column1=[ID], 4, 4
Column2=[Name], 12, 100
Column3=[Desc], -1, 2147483647

[DB:CShiftRecordset]
DB=1
DBType=ODBC
ColumnCount=4
Column1=[ID], 4, 4
Column2=[Name], 12, 100
Column3=[tm], 11, 16
Column4=[Code], 12, 100

[CLS:CShiftRecordset]
Type=0
HeaderFile=ShiftRecordset.h
ImplementationFile=ShiftRecordset.cpp
BaseClass=CRecordset
Filter=N
VirtualFilter=r

[CLS:CTestRst]
Type=0
HeaderFile=TestRst.h
ImplementationFile=TestRst.cpp
BaseClass=CRecordset
Filter=N
VirtualFilter=r

[DB:CTestRst]
DB=1
DBType=ODBC
ColumnCount=5
Column1=[ID], 4, 4
Column2=[LibraryID], 4, 4
Column3=[Name], 12, 100
Column4=[Desc], -1, 2147483647
Column5=[Data], -1, 2147483647

[CLS:CTmpRst]
Type=0
HeaderFile=TmpRst.h
ImplementationFile=TmpRst.cpp
BaseClass=CRecordset
Filter=N
VirtualFilter=r
LastObject=CTmpRst

[DB:CTmpRst]
DB=1
DBType=ODBC
ColumnCount=4
Column1=[Libraries.Name], 12, 100
Column2=[Tasks.Name], 12, 100
Column3=[Messages.Name], 12, 100
Column4=[Data], -1, 2147483647

[CLS:COdbcColumns]
Type=0
HeaderFile=OdbcColumns.h
ImplementationFile=OdbcColumns.cpp
BaseClass=CRecordset
Filter=N
VirtualFilter=r

[DB:COdbcColumns]
DB=1
DBType=ODBC
ColumnCount=20
Column1=[Text], 12, 100
Column2=[Memo], -1, 2147483647
Column3=[Number Byte], -6, 1
Column4=[Number Integer], 5, 2
Column5=[Number Long Integer], 4, 4
Column6=[Number Single], 7, 4
Column7=[Number Double], 8, 8
Column8=[NUmber Replication ID], -11, 16
Column9=[Date/Time], 11, 16
Column10=[Currency General Number], 2, 21
Column11=[Currency Currency], 2, 21
Column12=[Currency Fixed], 2, 21
Column13=[Currency Standard], 2, 21
Column14=[Currency Percent], 2, 21
Column15=[Currency Scientific], 2, 21
Column16=[AutoNumber Long Integer], 4, 4
Column17=[AutoNumber Replication ID], -11, 16
Column18=[Yes/No], -7, 1
Column19=[OLE Object], -4, 1073741823
Column20=[Hyperlink], -1, 2147483647

[CLS:COdbcTables]
Type=0
HeaderFile=OdbcTables.h
ImplementationFile=OdbcTables.cpp
BaseClass=CRecordset
Filter=N
VirtualFilter=r

[DB:COdbcTables]
DB=1
DBType=ODBC
ColumnCount=20
Column1=[Text], 12, 100
Column2=[Memo], -1, 2147483647
Column3=[Number Byte], -6, 1
Column4=[Number Integer], 5, 2
Column5=[Number Long Integer], 4, 4
Column6=[Number Single], 7, 4
Column7=[Number Double], 8, 8
Column8=[NUmber Replication ID], -11, 16
Column9=[Date/Time], 11, 16
Column10=[Currency General Number], 2, 21
Column11=[Currency Currency], 2, 21
Column12=[Currency Fixed], 2, 21
Column13=[Currency Standard], 2, 21
Column14=[Currency Percent], 2, 21
Column15=[Currency Scientific], 2, 21
Column16=[AutoNumber Long Integer], 4, 4
Column17=[AutoNumber Replication ID], -11, 16
Column18=[Yes/No], -7, 1
Column19=[OLE Object], -4, 1073741823
Column20=[Hyperlink], -1, 2147483647

[DLG:IDD_COMPACT]
Type=1
Class=CCompactDlg
ControlCount=1
Control1=TXT_TEXT,edit,1342244996

[CLS:CCompactDlg]
Type=0
HeaderFile=CompactDlg.h
ImplementationFile=CompactDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CCompactDlg

[DLG:IDD_BACKUP]
Type=1
Class=CBackupDlg
ControlCount=7
Control1=IDC_STATIC,static,1342308352
Control2=TXT_FILENAME,edit,1350633600
Control3=IDC_STATIC,static,1342308352
Control4=TXT_FILEPATH,edit,1350633600
Control5=IDOK,button,1342242817
Control6=IDCANCEL,button,1342242816
Control7=BTN_BROWSE,button,1342242816

[CLS:CBackupDlg]
Type=0
HeaderFile=BackupDlg.h
ImplementationFile=BackupDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=BTN_BROWSE
VirtualFilter=dWC

[DLG:IDD_DATABASEPARAMS]
Type=1
Class=?
ControlCount=11
Control1=CHK_TABLE,button,1342255107
Control2=CHK_VIEW,button,1342255107
Control3=CHK_SYSTEMTABLE,button,1342255107
Control4=CHK_GLOBALTEMPORARY,button,1342255107
Control5=CHK_LOCALTEMPORARY,button,1342255107
Control6=CHK_ALIAS,button,1342255107
Control7=CHK_SYNONYM,button,1342255107
Control8=IDC_STATIC,static,1342308352
Control9=TXT_PARAMS,edit,1350631552
Control10=IDOK,button,1342242817
Control11=IDCANCEL,button,1342242816

[DLG:IDD_STATUS]
Type=1
Class=?
ControlCount=1
Control1=322,edit,1342244997

[DLG:IDD_BACKUP_SM]
Type=1
Class=?
ControlCount=7
Control1=IDC_STATIC,static,1342308352
Control2=TXT_FILENAME,edit,1350633600
Control3=IDC_STATIC,static,1342308352
Control4=TXT_FILEPATH,edit,1350633600
Control5=IDOK,button,1342242817
Control6=IDCANCEL,button,1342242816
Control7=BTN_BROWSE,button,1342242816

[CLS:CMsgBoxDlg]
Type=0
HeaderFile=MsgBoxDlg.h
ImplementationFile=MsgBoxDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDYES

[DLG:IDD_DATABASEPARAMS_MATRIX]
Type=1
Class=?
ControlCount=11
Control1=CHK_TABLE,button,1342246915
Control2=CHK_VIEW,button,1342246915
Control3=CHK_SYSTEMTABLE,button,1342246915
Control4=CHK_GLOBALTEMPORARY,button,1342246915
Control5=CHK_LOCALTEMPORARY,button,1342246915
Control6=CHK_ALIAS,button,1342246915
Control7=CHK_SYNONYM,button,1342246915
Control8=IDC_STATIC,static,1342308352
Control9=TXT_PARAMS,edit,1350631552
Control10=IDCANCEL,button,1342242816
Control11=IDOK,button,1342242817

