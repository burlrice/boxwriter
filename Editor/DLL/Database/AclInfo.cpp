#include "stdafx.h"
#include <iostream>
#include <Accctrl.h>
#include <Aclapi.h>
#include "ACLInfo.h"
#include "Debug.h"
#include "Parse.h"

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::ACL;

#define READ_PERMISSIONS (FILE_READ_DATA | \
						  FILE_READ_ATTRIBUTES)
#define WRITE_PERMISSIONS (FILE_WRITE_DATA | \
						   FILE_APPEND_DATA | \
						   FILE_WRITE_ATTRIBUTES | \
						   FILE_WRITE_EA)
#define EXECUTE_PERMISSIONS (FILE_READ_DATA | \
							 FILE_EXECUTE)

DB_API std::wstring FoxjetDatabase::ACL::to_string(const FoxjetDatabase::ACL::MOD & mod)
{
	std::wstring str;
	int n[] = { mod.owner, mod.group, mod.other };

	for (int i = 0; i < ARRAYSIZE(n); i++) {
		str += n[i] & R ? _T ("r") : _T ("-");
		str += n[i] & W ? _T ("w") : _T ("-");
		str += n[i] & X ? _T ("x") : _T ("-");
		str += _T (" ");
	}
	
	return str.substr (0, str.size () - 1);
}

DB_API FoxjetDatabase::ACL::MOD FoxjetDatabase::ACL::GetAccess(LPCTSTR lpszFile)
{
	FoxjetDatabase::ACL::MOD result = { 0 };

#define ACL_SIZE 1024
#define DOM_SIZE 1024
#define ACCT_NAME_SIZE 1024

	LPCTSTR lpFileName = lpszFile;
	wchar_t UsrNm[1024] = { 0 };
	wchar_t GrpNm[1024] = { 0 };

	PSECURITY_DESCRIPTOR pSD = NULL;
	DWORD LenNeeded = 0, PBits = 0, iAce;
	BOOL DaclF, AclDefF, OwnerDefF, GroupDefF;
	BYTE DAcl[ACL_SIZE] = { 0 };
	PACL pAcl = (PACL)&DAcl;
	ACL_SIZE_INFORMATION ASizeInfo;
	PACCESS_ALLOWED_ACE pAce;
	BYTE AType;
	HANDLE ProcHeap = GetProcessHeap();
	PSID pOwnerSid, pGroupSid;
	TCHAR RefDomain[2][DOM_SIZE] = { 0 };
	DWORD RefDomCnt[] = { DOM_SIZE, DOM_SIZE };
	DWORD AcctSize[] = { ACCT_NAME_SIZE, ACCT_NAME_SIZE };
	SID_NAME_USE sNamUse[] = { SidTypeUser, SidTypeGroup };

	/* Get the required size for the security descriptor. */
	GetFileSecurity(lpFileName,
		OWNER_SECURITY_INFORMATION | GROUP_SECURITY_INFORMATION |
		DACL_SECURITY_INFORMATION, pSD, 0, &LenNeeded);

	if (LenNeeded) {
		pSD = HeapAlloc(ProcHeap, HEAP_GENERATE_EXCEPTIONS, LenNeeded);
		GetFileSecurity(lpFileName, OWNER_SECURITY_INFORMATION |
			GROUP_SECURITY_INFORMATION | DACL_SECURITY_INFORMATION,
			pSD, LenNeeded, &LenNeeded);
		GetSecurityDescriptorDacl(pSD, &DaclF, &pAcl, &AclDefF);
		GetAclInformation(pAcl, &ASizeInfo,
			sizeof(ACL_SIZE_INFORMATION), AclSizeInformation);
		PBits = 0; /* Compute the permissions from the ACL. */
		for (iAce = 0; iAce < ASizeInfo.AceCount; iAce++) {
			GetAce(pAcl, iAce, (LPVOID*)&pAce);
			AType = pAce->Header.AceType;
			if (AType == ACCESS_ALLOWED_ACE_TYPE)
				PBits |= (0x1 << (8 - iAce));
		}
		/* Find the name of the owner and owning group. */
		GetSecurityDescriptorOwner(pSD, &pOwnerSid, &OwnerDefF);
		GetSecurityDescriptorGroup(pSD, &pGroupSid, &GroupDefF);
		LookupAccountSid(NULL, pOwnerSid, UsrNm, &AcctSize[0],
			RefDomain[0], &RefDomCnt[0], &sNamUse[0]);
		LookupAccountSid(NULL, pGroupSid, GrpNm, &AcctSize[1],
			RefDomain[1], &RefDomCnt[1], &sNamUse[1]);
	}

	{
		char sz[32] = { 0 };
		sprintf(sz, "%o", PBits);

		result.owner = sz[0] - '0';
		result.group = sz[1] - '0';
		result.other = sz[2] - '0';
	}

	return result;
}

// Constructor
CACLInfo::CACLInfo(_bstr_t bstrPath)
{
	m_sAceList = NULL;
	m_bstrPath = bstrPath;
}

// Destructor
CACLInfo::~CACLInfo(void)
{
	// Free ace_list structure
	ClearAceList();
}

// Free the nodes of ace_list
void CACLInfo::ClearAceList()
{
	ace_list* pList = m_sAceList;
	ace_list* pNext;
	while(NULL != pList)
	{
		pNext = pList->next;
		free(pList);
		pList = pNext;
	}

	m_sAceList = NULL;
}

HRESULT CACLInfo::Query()
{
	BOOL bSuccess = TRUE;
	BYTE* pSecDescriptorBuf;
	DWORD dwSizeNeeded = 0;

	// clear any previously queried information
	ClearAceList();

	// Find out size of needed buffer for security descriptor with DACL
	// DACL = Discretionary Access Control List
	bSuccess = GetFileSecurityW((BSTR)m_bstrPath,
								DACL_SECURITY_INFORMATION,
								NULL,
								0,
								&dwSizeNeeded);

	if (0 == dwSizeNeeded)
	{
		TRACEF(FormatMessage(::GetLastError()));
		return E_FAIL;
	}
	pSecDescriptorBuf = new BYTE[dwSizeNeeded];

	// Retrieve security descriptor with DACL information
	bSuccess = GetFileSecurityW((BSTR)m_bstrPath,
								DACL_SECURITY_INFORMATION,
								pSecDescriptorBuf,
								dwSizeNeeded,
								&dwSizeNeeded);

	// Check if we successfully retrieved security descriptor with DACL information
	if (!bSuccess)
	{
		DWORD dwError = GetLastError();
		TRACEF("Failed to get file security information (" + FoxjetDatabase::ToString (dwError) + ")");
		return E_FAIL;
	}
	
	// Getting DACL from Security Descriptor
	PACL pacl;
	BOOL bDaclPresent, bDaclDefaulted;
	bSuccess = GetSecurityDescriptorDacl((SECURITY_DESCRIPTOR*)pSecDescriptorBuf,
										 &bDaclPresent, &pacl, &bDaclDefaulted);
	
	// Check if we successfully retrieved DACL
	if (!bSuccess)
	{
		DWORD dwError = GetLastError();
		TRACEF(FormatMessage(::GetLastError()));
		TRACEF ("Failed to retrieve DACL from security descriptor (" + FoxjetDatabase::ToString (dwError) + ")");
		return E_FAIL;
	}
	
	// Check if DACL present in security descriptor
	if (!bDaclPresent)
	{
		TRACEF(FormatMessage(::GetLastError()));
		TRACEF ("DACL was not found.");
		return E_FAIL;
	}

	// DACL for specified file was retrieved successfully
	// Now, we should fill in the linked list of ACEs
	// Iterate through ACEs (Access Control Entries) of DACL
	for (USHORT i = 0; i < pacl->AceCount; i++)
	{
		LPVOID pAce;
		bSuccess = GetAce(pacl, i, &pAce);
		if (!bSuccess)
		{
			DWORD dwError = GetLastError();
			TRACEF(FormatMessage(::GetLastError()));
			TRACEF ("Failed to get ace " + FoxjetDatabase::ToString (i) + " (" + FoxjetDatabase::ToString (dwError) + ")");
			continue;
		}
		HRESULT hr = AddAceToList((ACE_HEADER*)pAce);
		if (FAILED(hr))
		{
			TRACEF(FormatMessage(::GetLastError()));
			TRACEF ("Failed to add ace " + FoxjetDatabase::ToString (i) + "to list");
			continue;
		}
	}
	return S_OK;
}

HRESULT CACLInfo::AddAceToList(ACE_HEADER* pAce)
{
	ace_list* pNewAce = (ace_list*)malloc(sizeof(ace_list));
	if (NULL == pNewAce)
	{
		return E_OUTOFMEMORY;
	}

	// Check Ace type and update new list entry accordingly
	switch (pAce->AceType)
	{
		case ACCESS_ALLOWED_ACE_TYPE:
		{
			pNewAce->bAllowed = TRUE;
			break;
		}
		case ACCESS_DENIED_ACE_TYPE:
		{
			pNewAce->bAllowed = FALSE;
			break;
		}
	}
	// Update the remaining fields
	// We add new entry to the head of list
	pNewAce->pAce = pAce;
	pNewAce->next = m_sAceList;

	m_sAceList = pNewAce;

	return S_OK;
}

/*
template <typename T>
static T _min(T lhs, T rhs)
{
	bool r = (lhs & R) & (rhs & R);
	bool w = (lhs & W) & (rhs & W);
	bool x = (lhs & X) & (rhs & X);

	return (r ? R : 0) | (w ? W : 0) | (x ? X : 0);
}

template <typename T>
static T _max(T lhs, T rhs)
{
	bool r = (lhs & R) | (rhs & R);
	bool w = (lhs & W) | (rhs & W);
	bool x = (lhs & X) | (rhs & X);

	return (r ? R : 0) | (w ? W : 0) | (x ? X : 0);
}
*/

DB_API MOD FoxjetDatabase::ACL::_min(const FoxjetDatabase::ACL::MOD & lhs, const FoxjetDatabase::ACL::MOD & rhs)
{
	FoxjetDatabase::ACL::MOD result;

	result.group = min /* ::_min */ (lhs.group, rhs.group);
	result.owner = min /* ::_min */ (lhs.owner, rhs.owner);
	result.other = min /* ::_min */ (lhs.other, rhs.other);

	return result;
}

DB_API MOD FoxjetDatabase::ACL::_max(const FoxjetDatabase::ACL::MOD & lhs, const FoxjetDatabase::ACL::MOD & rhs)
{
	FoxjetDatabase::ACL::MOD result;

	result.group = max /* ::_max */ (lhs.group, rhs.group);
	result.owner = max /* ::_max */ (lhs.owner, rhs.owner);
	result.other = max /* ::_max */ (lhs.other, rhs.other);

	return result;
}

PERMISSION CACLInfo::GetPermissions(std::wstring & strName) const
{
	std::vector <PERMISSION> v = GetPermissions ();
	PERMISSION result;

	result.m_mod.owner = result.m_mod.group = result.m_mod.other = 0;

	/*
	#ifdef _DEBUG
	for (int i = 0; i < v.size(); i++) {
		if (!_tcsicmp(v[i].m_strName.c_str(), strName.c_str())) {
			TCHAR sz[256] = { 0 };

			_stprintf(sz, _T("%d%d%d [%s]: %s\\%s"), v[i].m_mod.owner, v[i].m_mod.group, v[i].m_mod.other, to_string((__int64)v[i].m_mod).c_str(),
				v[i].m_strDomain.c_str(),
				v[i].m_strName.c_str());
			TRACEF(sz);
		}
	}
	#endif
	*/

	for (int i = 0; i < v.size(); i++) {
		if (!_tcsicmp(v[i].m_strName.c_str(), strName.c_str())) {
			PERMISSION p = v[i];

			if (result.m_strName.empty())
				result = p;
			else 
				result.m_mod = _max (result.m_mod, p.m_mod);
		}
	}

	//TRACEF (to_string (result.m_mod));

 	return result;
}

std::vector <PERMISSION> CACLInfo::GetPermissions() const
{
	std::vector <PERMISSION> result;

	if (m_sAceList) {
		ACE_HEADER* pAce;
		SID* pAceSid;
		ACCESS_MASK maskPermissions;
		ace_list* pList = m_sAceList;

		while (NULL != pList)
		{
			pAce = pList->pAce;
			if (pList->bAllowed)
			{
				ACCESS_ALLOWED_ACE* pAllowed = (ACCESS_ALLOWED_ACE*)pAce;
				pAceSid = (SID*)(&(pAllowed->SidStart));
				maskPermissions = pAllowed->Mask;
			}
			else
			{
				ACCESS_DENIED_ACE* pDenied = (ACCESS_DENIED_ACE*)pAce;
				pAceSid = (SID*)(&(pDenied->SidStart));
				maskPermissions = pDenied->Mask;
			}
			DWORD dwCbName = 0;
			DWORD dwCbDomainName = 0;
			SID_NAME_USE SidNameUse;
			TCHAR bufName[MAX_PATH];
			TCHAR bufDomain[MAX_PATH];
			dwCbName = sizeof(bufName);
			dwCbDomainName = sizeof(bufDomain);


			BOOL bSuccess = LookupAccountSid(NULL,
				pAceSid,
				bufName,
				&dwCbName,
				bufDomain,
				&dwCbDomainName,
				&SidNameUse);
			if (!bSuccess)
			{
				TRACEF ("Failed to get account for SID");
				break;
			}

			if (pList->bAllowed) {
				PERMISSION p;

				p.m_mod = GetAccess(std::wstring(m_bstrPath, SysStringLen(m_bstrPath)).c_str());

				if (dwCbDomainName > 0)
					p.m_strDomain = bufDomain;

				if (dwCbName > 0)
					p.m_strName = bufName;

				if ((maskPermissions & READ_PERMISSIONS) == READ_PERMISSIONS)
					p.m_mod.other |= R;

				if ((maskPermissions & WRITE_PERMISSIONS) == WRITE_PERMISSIONS)
					p.m_mod.other |= W;

				if ((maskPermissions & EXECUTE_PERMISSIONS) == EXECUTE_PERMISSIONS)
					p.m_mod.other |= X;

				result.push_back(p);
			}

			pList = pList->next;
		}
	}

	return result;
}


void CACLInfo::Output(ostream& os)
{
	if (NULL == m_sAceList)
	{
		os << "No ACL Info\n";
		return;
	}
	ACE_HEADER* pAce;
	SID* pAceSid;
	ACCESS_MASK maskPermissions;
	ace_list* pList = m_sAceList;
	// Iterate through ACEs list and
	// out put information
	while(NULL != pList)
	{
		pAce = pList->pAce;
		if (pList->bAllowed)
		{
			ACCESS_ALLOWED_ACE* pAllowed = (ACCESS_ALLOWED_ACE*)pAce;
			pAceSid = (SID*)(&(pAllowed->SidStart));
			maskPermissions = pAllowed->Mask;
		}
		else
		{
			ACCESS_DENIED_ACE* pDenied = (ACCESS_DENIED_ACE*)pAce;
			pAceSid = (SID*)(&(pDenied->SidStart));
			maskPermissions = pDenied->Mask;
		}
		DWORD dwCbName = 0;
		DWORD dwCbDomainName = 0;
		SID_NAME_USE SidNameUse;
		TCHAR bufName[MAX_PATH];
		TCHAR bufDomain[MAX_PATH];
		dwCbName = sizeof(bufName);
		dwCbDomainName = sizeof(bufDomain);

		// Get account name for SID
		BOOL bSuccess = LookupAccountSid(NULL,
							pAceSid,
							bufName,
							&dwCbName,
							bufDomain,
							&dwCbDomainName,
							&SidNameUse);
		if (!bSuccess)
		{
			cout << "Failed to get account for SID";
			continue;
		}

		// Output Account info (in NT4 style: domain\user)
		if (pList->bAllowed)
		{
			os << "Allowed to: ";
		}
		else
		{
			os << "Denied from: ";
		}

		if (dwCbDomainName > 0)
		{
			os << bufDomain << "\\";
		}
		if (dwCbName > 0)
		{
			os << bufName;
		}

		// Output permissions (Read/Write/Execute)
		os << " [";

		// For Allowed aces
		if (pList->bAllowed)
		{
			// Read Permissions
			if ((maskPermissions & READ_PERMISSIONS) == READ_PERMISSIONS)
			{
				os << "R";
			}
			else
			{
				os << " ";
			}
			// Write permissions
			if ((maskPermissions & WRITE_PERMISSIONS) == WRITE_PERMISSIONS)
			{
				os << "W";
			}
			else
			{
				os << " ";
			}
			// Execute Permissions
			if ((maskPermissions & EXECUTE_PERMISSIONS) == EXECUTE_PERMISSIONS)
			{
				os << "X";
			}
			else
			{
				os << " ";
			}
		}
		else
		// Denied Ace permissions
		{
			// Read Permissions
			if ((maskPermissions & READ_PERMISSIONS) != 0)
			{
				os << "R";
			}
			else
			{
				os << " ";
			}
			// Write permissions
			if ((maskPermissions & WRITE_PERMISSIONS) != 0)
			{
				os << "W";
			}
			else
			{
				os << " ";
			}
			// Execute Permissions
			if ((maskPermissions & EXECUTE_PERMISSIONS) != 0)
			{
				os << "X";
			}
			else
			{
				os << " ";
			}
		}
		os << "]" << endl;
		pList = pList->next;

	}
	return;
}

DWORD AddAceToObjectsSecurityDescriptor(
	LPTSTR pszObjName,          // name of object
	SE_OBJECT_TYPE ObjectType,  // type of object
	LPTSTR pszTrustee,          // trustee for new ACE
	TRUSTEE_FORM TrusteeForm,   // format of trustee structure
	DWORD dwAccessRights,       // access mask for new ACE
	ACCESS_MODE AccessMode,     // type of ACE
	DWORD dwInheritance         // inheritance flags for new ACE
)
{
	DWORD dwRes = 0;
	PACL pOldDACL = NULL, pNewDACL = NULL;
	PSECURITY_DESCRIPTOR pSD = NULL;
	EXPLICIT_ACCESS ea;

	if (NULL == pszObjName)
		return ERROR_INVALID_PARAMETER;

	// Get a pointer to the existing DACL.

	dwRes = GetNamedSecurityInfo(pszObjName, ObjectType,
		DACL_SECURITY_INFORMATION,
		NULL, NULL, &pOldDACL, NULL, &pSD);
	if (ERROR_SUCCESS != dwRes) {
		TRACEF(FormatMessage(dwRes));
		TRACEF(FormatMessage(::GetLastError()));
		goto Cleanup;
	}

	// Initialize an EXPLICIT_ACCESS structure for the new ACE. 

	ZeroMemory(&ea, sizeof(EXPLICIT_ACCESS));
	ea.grfAccessPermissions = dwAccessRights;
	ea.grfAccessMode = AccessMode;
	ea.grfInheritance = dwInheritance;
	ea.Trustee.TrusteeForm = TrusteeForm;
	ea.Trustee.ptstrName = pszTrustee;

	// Create a new ACL that merges the new ACE
	// into the existing DACL.

	dwRes = SetEntriesInAcl(1, &ea, pOldDACL, &pNewDACL);
	if (ERROR_SUCCESS != dwRes) {
		TRACEF(FormatMessage(dwRes));
		TRACEF(FormatMessage(::GetLastError ()));
		goto Cleanup;
	}

	// Attach the new ACL as the object's DACL.

	dwRes = SetNamedSecurityInfo(pszObjName, ObjectType,
		DACL_SECURITY_INFORMATION,
		NULL, NULL, pNewDACL, NULL);
	if (ERROR_SUCCESS != dwRes) {
		TRACEF(FormatMessage(dwRes));
		TRACEF(FormatMessage(::GetLastError()));
		goto Cleanup;
	}

Cleanup:

	if (pSD != NULL)
		LocalFree((HLOCAL)pSD);
	if (pNewDACL != NULL)
		LocalFree((HLOCAL)pNewDACL);

	return dwRes;
}




bool CACLInfo::SetFilePermission(const std::wstring & strName, DWORD dwPermissions)
{
	std::wstring strPath(m_bstrPath, SysStringLen(m_bstrPath));
	TCHAR * pszObject = new TCHAR[strPath.length() * 2];
	TCHAR * pszName = new TCHAR[strName.length() * 2];

	_tcscpy(pszObject, strPath.c_str());
	_tcscpy(pszName, strName.c_str());

	DWORD dw = AddAceToObjectsSecurityDescriptor(
		pszObject,
		SE_FILE_OBJECT,
		pszName,
		TRUSTEE_IS_NAME,
		dwPermissions, //STANDARD_RIGHTS_READ | STANDARD_RIGHTS_EXECUTE,
		GRANT_ACCESS,
		NO_INHERITANCE);

	delete[] pszObject;
	delete[] pszName;

	return dw == 0;
}