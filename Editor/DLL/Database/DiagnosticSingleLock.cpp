#include "stdafx.h"
#include "AnsiString.h"
#include "DiagnosticSingleLock.h"
#include "Database.h"
#include "Parse.h"
#include "Debug.h"
#include "StackWalker.h"
#include "WinMsg.h"

static const DWORD						dwInternalTimeout = INFINITE;
static DWORD							g_dwThreadID = 0;
//static __declspec( thread ) int			g_nTlsIndex = 0;  

static std::string Format (const std::string & strFile, ULONG lLine);

class CDiagnosticDataThread : public CWinThread
{
	CDiagnosticDataThread ();

	DECLARE_DYNCREATE(CDiagnosticDataThread)

public:
	virtual BOOL PreTranslateMessage (MSG * pMsg);
	virtual BOOL InitInstance ();
	virtual int Run ();
	virtual int ExitInstance ();

	CString Trace (LPCTSTR lpszFile, ULONG lLine);

	enum 
	{
		TM_TRACE = WM_APP + 1,
		TM_SET_INTERVAL,
		//TM_SET_LOCK_NAME,
		TM_SCHEDULE_PUSH,
		TM_COMMIT_PUSH,
		TM_SCHEDULE_POP,
		TM_COMMIT_POP,
		TM_REGISTER_THREAD_NAME,
		TM_ENUM_REGISTERED_THREADS,
		TM_REGISTER_CRITICAL_SECTION,
		TM_UNREGISTER_CRITICAL_SECTION,
		TM_STROKE_WATCHDOG,
	};

	static void Post (UINT nMsg, WPARAM wParam, LPARAM lParam, LPCTSTR lpszFile, ULONG lLine) 
	{
		if (::g_dwThreadID)
			while (!::PostThreadMessage (::g_dwThreadID, nMsg, wParam, lParam))
				::Sleep (1);
	}

	typedef struct
	{
		CString m_strResult;
		CString	m_strFile;
		ULONG	m_lLine;
		HANDLE	m_hFinished;
	} TRACE_STRUCT;

//#pragma pack(1)
//	typedef struct
//	{
//		DWORD		m_dwObject;
//		WORD		m_wThreadID;
//		std::string	m_strName;
//		std::string	m_strFile;
//		__int16		m_lLine;
//		std::string	m_strState;
//		__int16		m_nIndex;
//	} PUSH_STRUCT;
//#pragma pop

	//CString Trace (CDiagnosticCriticalSection* pObject, std::map <std::wstring, DWORD> & mapLockName);
	static CString Trace (const CString & str, const CString & strFile, ULONG lLine);
	CString GetName (CDiagnosticCriticalSection* pObject, std::map <std::wstring, DWORD> & mapLockName);
	std::vector <DWORD> GetThreadIds (std::map <DWORD, std::map <DWORD, std::map <int, std::string>>> & mapRef);
	std::vector <DWORD> EnumLockAddresses(std::map <DWORD, std::map <DWORD, std::map <int, std::string>>> & mapRef);
	std::vector <DWORD> EnumThreadRefs(DWORD dwLockObjAddress, std::map <DWORD, std::map <DWORD, std::map <int, std::string>>> & mapRef);
	CString GetObjectState (DWORD dwLockObjectAddr);

	std::string GetLockCount (CDiagnosticCriticalSection * pObject, DWORD dwThreadID);

protected:
	//std::map <DWORD, std::map <DWORD, std::map <int, std::string>>>	m_mapRef;
	//std::map <std::wstring, DWORD>									m_mapLockName;
	//std::map <DWORD, std::map <int, std::string>>						m_mapThreadStack;
	std::map <DWORD, std::wstring>										m_mapThreadName;
	std::map <CDiagnosticCriticalSection *, __int64>					m_mapCriticalSectionName;
	std::map <DWORD, std::map <DWORD, DWORD>>							m_mapLocked;
	CTime																m_tmWatchdog;
	CTime																m_tmDiagnostic;
	int																	m_nInvertvalSeconds;
};

#define POST(n,w,l) CDiagnosticDataThread::Post ((n), (w), (l), _T (__FILE__), __LINE__)

IMPLEMENT_DYNCREATE(CDiagnosticDataThread, CWinThread)

CDiagnosticDataThread::CDiagnosticDataThread ()
:	m_tmWatchdog (CTime::GetCurrentTime ()),
	m_tmDiagnostic (CTime::GetCurrentTime ()),
	m_nInvertvalSeconds (30)
{
}

BOOL CDiagnosticDataThread::PreTranslateMessage (MSG * pMsg)
{
	bool bHandled = false;

	if (pMsg) {
		switch (pMsg->message) {
		case TM_TRACE:
			{
				if (TRACE_STRUCT * p = (TRACE_STRUCT *)pMsg->wParam) {
					p->m_strResult = Trace (p->m_strFile, p->m_lLine);
					::SetEvent (p->m_hFinished);
				}
			}
			break;
		case TM_SET_INTERVAL:
			m_nInvertvalSeconds = (int)pMsg->wParam;
			break;
		case TM_SCHEDULE_PUSH:
			{
				DWORD dwObjAddr = (DWORD)pMsg->wParam;
				DWORD dwThreadID = pMsg->lParam;
				m_mapLocked [dwObjAddr][dwThreadID]++;
			}
			break;
		case TM_COMMIT_PUSH:
			{
				DWORD dwObjAddr = (DWORD)pMsg->wParam;
				DWORD dwThreadID = pMsg->lParam;
				m_mapLocked [dwObjAddr][dwThreadID]++;
			}
			break;
		case TM_SCHEDULE_POP:
			{
				DWORD dwObjAddr = (DWORD)pMsg->wParam;
				DWORD dwThreadID = pMsg->lParam;
				m_mapLocked [dwObjAddr][dwThreadID]--;
			}
			break;
		case TM_COMMIT_POP:
			{
				DWORD dwObjAddr = (DWORD)pMsg->wParam;
				DWORD dwThreadID = pMsg->lParam;
				m_mapLocked [dwObjAddr][dwThreadID]--;
			}
			break;
		/*
		case TM_SET_LOCK_NAME:
			{
				if (CString * p = (CString *)pMsg->wParam) {
					m_mapLockName [(std::wstring)(* p)] = (DWORD)pMsg->lParam;
					delete p;
				}
			}
			break;
		case TM_SCHEDULE_PUSH:
			{
				if (PUSH_STRUCT * p = (PUSH_STRUCT *)pMsg->wParam) {
					{
						std::map <int, std::string> & v = m_mapRef [p->m_dwObject] [p->m_wThreadID];
						//m_mapLockName [(std::wstring)p->m_strName] = p->m_dwObject;
						v [p->m_nIndex] = (Format (p->m_strFile, p->m_lLine));
					}
					{
						std::string s = Format (p->m_strFile, p->m_lLine) + ":\t\t\t" + p->m_strState;
						std::map <int, std::string> & v = m_mapThreadStack [p->m_wThreadID];
						s += GetLockCount (p->m_dwObject, p->m_wThreadID);
						v [p->m_nIndex] = s;
					}
					delete p;
				}
			}
			break;
		case TM_COMMIT_PUSH:
			{
				if (PUSH_STRUCT * p = (PUSH_STRUCT *)pMsg->wParam) {
					std::string s = Format (p->m_strFile, p->m_lLine) + ":\t\t\t" + p->m_strName;
					std::map <int, std::string> & v = m_mapThreadStack [p->m_wThreadID];

					s += "      " + GetLockCount (p->m_dwObject, p->m_wThreadID);
					v [p->m_nIndex] = s;

					m_tmWatchdog = CTime::GetCurrentTime ();
					delete p;
				}
			}
			break;
		case TM_SCHEDULE_POP:
			{
				if (PUSH_STRUCT * p = (PUSH_STRUCT *)pMsg->wParam) {
					std::map <int, std::string> & v = m_mapRef [p->m_dwObject] [(DWORD)p->m_wThreadID];
					int nIndex = p->m_nIndex; //Find (v, Format (p->m_strFile, p->m_lLine));

					ASSERT (nIndex != -1);

					if (nIndex != -1)
						v.erase (nIndex);

					{
						std::string s = Format (p->m_strFile, p->m_lLine) + ":\t\t\t" + p->m_strName + " [--] " 
							+ GetLockCount (p->m_dwObject, p->m_wThreadID);
						std::map <int, std::string> & v = m_mapThreadStack [p->m_wThreadID];
						v [p->m_nIndex] = s;
					}
					delete p;
				}
			}
			break;
		case TM_COMMIT_POP:
			{
				if (PUSH_STRUCT * p = (PUSH_STRUCT *)pMsg->wParam) {
					std::map <int, std::string> & v = m_mapThreadStack [(DWORD)p->m_wThreadID];
					v.erase (p->m_nIndex); //v.pop_back ();
					m_tmWatchdog = CTime::GetCurrentTime ();
					delete p;
				}
			}
			break;
		*/
		case TM_REGISTER_THREAD_NAME:
			{
				if (DWORD dwThreadID = (DWORD)pMsg->wParam) {
					if (CString * p = (CString *)pMsg->lParam) {
						m_mapThreadName [dwThreadID] = * p;
						delete p;
					}
					else {
						if (m_mapThreadName.find (dwThreadID) == m_mapThreadName.end ())
							m_mapThreadName [dwThreadID] = _T ("");
					}
				}
			}
			break;
		case TM_ENUM_REGISTERED_THREADS:
			{
				for (std::map <DWORD, std::wstring>::iterator i = m_mapThreadName.begin (); i != m_mapThreadName.end (); i++) {
					DWORD dwThreadID = i->first;
					DWORD dwResult = 0;

					if (i->second.length ())
						::SendMessageTimeout (HWND_BROADCAST, WM_DEBUG_REGISTER_THREAD, (WPARAM)::GetCurrentProcessId (), (LPARAM)dwThreadID, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);
				}
			}
			break;
		case TM_REGISTER_CRITICAL_SECTION:
			if (CDiagnosticCriticalSection * p = (CDiagnosticCriticalSection *)pMsg->wParam) 
				m_mapCriticalSectionName [p]++;
			break;
		case TM_UNREGISTER_CRITICAL_SECTION:
			if (CDiagnosticCriticalSection * p = (CDiagnosticCriticalSection *)pMsg->wParam) 
				m_mapCriticalSectionName.erase (p);
			if (HANDLE h = (HANDLE)pMsg->lParam)
				::SetEvent (h);
			break;
		case TM_STROKE_WATCHDOG:
			m_tmWatchdog = CTime::GetCurrentTime ();
			break;
		}
	}

	return CWinThread::PreTranslateMessage (pMsg);
}


int CDiagnosticDataThread::Run ()
{
	HANDLE hTrace = ::CreateEvent (NULL, TRUE, FALSE, _T ("Foxjet::CDiagnosticDataThread::Trace"));

	while (1) {
		MSG msg;
		bool bTrace = false;

		while (::PeekMessage (&msg, NULL, NULL, NULL, PM_NOREMOVE)) 
			if (!PumpMessage ())
				return ExitInstance();

		CTimeSpan tm = CTime::GetCurrentTime () - m_tmDiagnostic;

		DWORD dwWait = ::WaitForSingleObject (hTrace, 100);

		if (dwWait == WAIT_OBJECT_0) {
			::ResetEvent (hTrace);
			bTrace = true;
		}

		if (bTrace || tm.GetTotalSeconds () >= m_nInvertvalSeconds) {
			m_tmDiagnostic = CTime::GetCurrentTime ();
			::OutputDebugString (Trace (_T (__FILE__), __LINE__));
		}
	}

	return ExitInstance();
}

BOOL CDiagnosticDataThread::InitInstance ()
{
	CWinThread::InitInstance ();

	return TRUE;
}

int CDiagnosticDataThread::ExitInstance()
{
	return CWinThread::ExitInstance ();
}

std::string CDiagnosticDataThread::GetLockCount (CDiagnosticCriticalSection * pObject, DWORD dwThreadID)
{
	CString str;

	if (pObject) {
		str.Format (_T ("0x%p [%-45s]: LockCount: %d, OwningThread: %d"), 
			pObject,
			pObject->m_strName,
			pObject->m_sect.LockCount,
			pObject->m_sect.OwningThread);

		if ((DWORD)pObject->m_sect.OwningThread != dwThreadID) 
			str += _T (" (") + CString (m_mapThreadName [(DWORD)pObject->m_sect.OwningThread].c_str ()) + _T (")");

		{
			std::vector <std::wstring> v;
		
			for (std::map <DWORD, DWORD>::iterator i = m_mapLocked [(DWORD)pObject].begin (); i != m_mapLocked [(DWORD)pObject].end (); i++) {
				if (i->second > 0) 
				{
					CString s;

					s.Format (_T ("%d(%d)"), i->first, i->second);

					if (((DWORD)pObject->m_sect.OwningThread != 0) && (i->first != (DWORD)pObject->m_sect.OwningThread) && (i->second > 0))
						str += _T (" *****");

					v.push_back ((LPCTSTR)s);
				}
			}

			if (v.size ()) 
				str += CString (_T (" locks: ")) + implode (v, std::wstring (_T (","))).c_str ();
		}
	}

	return w2a (str);
}


CString CDiagnosticDataThread::Trace (LPCTSTR lpszFile, ULONG lLine)
{
	CString strResult;
	
	if (CTimeSpan (CTime::GetCurrentTime () - m_tmWatchdog).GetTotalSeconds () >= 30) 
		strResult += _T ("[dead]\n");

	//std::vector <DWORD> vThreads = GetThreadIds (m_mapRef);

	CString str;
	CStringArray v;

	for (std::map <DWORD, std::wstring>::iterator i = m_mapThreadName.begin(); i != m_mapThreadName.end(); i++) {
		DWORD dwThreadID = i->first;

		str.Format (_T ("%s[%d]"), i->second.c_str (), dwThreadID);
		v.Add (str);
	}

	strResult += Trace (_T ("known threads: ") + implode (v), lpszFile, lLine);
	strResult += Trace (_T ("critical sections: "), lpszFile, lLine);
	
	for (std::map <CDiagnosticCriticalSection *, __int64>::iterator i = m_mapCriticalSectionName.begin (); i != m_mapCriticalSectionName.end (); i++) {
		if (CDiagnosticCriticalSection * pObject = (CDiagnosticCriticalSection *)i->first) {
			strResult += Trace (CString (_T ("\t")) + GetLockCount (i->first, 0).c_str (), lpszFile, lLine);
			//vThreads.push_back ((DWORD)pObject->m_sect.OwningThread);
		}
	}

	/*
	std::vector <DWORD> vAddr = EnumLockAddresses(m_mapRef);

	for (int n = 0; n < vAddr.size (); n++) {
		std::vector <DWORD> vRef = EnumThreadRefs (vAddr [n], m_mapRef);

		if (vRef.size () > 1) {
			//StackWalker s;

			strResult += Trace (_T ("possible deadlock condition: "), lpszFile, lLine);
			break;
		}
	}

	for (int nThread = 0; nThread < vThreads.size (); nThread++) {
		DWORD dwThreadID = vThreads [nThread];
		CStringArray vObjects;

		str.Format (_T ("%s[%d]"), m_mapThreadName [dwThreadID].c_str (), dwThreadID);
		strResult += Trace (str, lpszFile, lLine);

		for (int i = 0; i < m_mapThreadStack [dwThreadID].size (); i++) 
			strResult += m_mapThreadStack [dwThreadID][i].c_str () + CString (_T ("\n"));
	}
	*/

	return strResult;
}

/* TODO: rem
CString CDiagnosticDataThread::Trace (CDiagnosticCriticalSection* pObject, std::map <std::wstring, DWORD> & mapLockName)
{
	CStringArray v;

	for (std::map<DWORD, std::map <DWORD, std::map <int, std::wstring>>>::iterator i = m_mapRef.begin(); i != m_mapRef.end(); i++) {
		if (i->first == (DWORD)pObject) {
			for (std::map <DWORD, std::vector <std::wstring>>::iterator j = i->second.begin (); j != i->second.end(); j++) {
				if (j->second.size ()) {
					CString s;

					s.Format (_T ("%d:%d"), j->first, j->second.size ());
					v.Add (s);
				}
			}
		}
	}

	return _T ("[") + GetName (pObject, mapLockName) + CString (_T ("]")) + (v.GetSize () ? implode (v) : _T ("[free]"));
}
*/


CString CDiagnosticDataThread::Trace (const CString & str, const CString & strFile, ULONG lLine)
{
	//#ifdef _DEBUG
	//	//CDebug::Trace (_T ("[sync] ") + str, true, strFile, lLine);
	//#else
	//	TraceReleaseJustified (_T ("[sync] ") + str, strFile, lLine);
	//#endif

	//::Trace (_T ("[sync] ") + str, strFile, lLine);
	CString s;

	s.Format (_T ("%s(%d): [BW][sync] %s\n"), strFile, lLine, str);
	
	return s;
}

CString CDiagnosticDataThread::GetName (CDiagnosticCriticalSection* pObject, std::map <std::wstring, DWORD> & mapLockName)
{
	CStringArray v;

	CString str;
	const DWORD dw = (DWORD)pObject;

	str.Format (_T ("0x%p"), pObject);
	v.Add (str);

	for (std::map <std::wstring, DWORD>::iterator i = mapLockName.begin (); i != mapLockName.end(); i++) 
		if (i->second == dw)
			v.Add (i->first.c_str ());

	return implode (v);
}

std::vector <DWORD> CDiagnosticDataThread::GetThreadIds (std::map <DWORD, std::map <DWORD, std::map <int, std::string>>> & mapRef)
{
	ULONG lCount = 0;
	std::vector <DWORD> v;

	for (std::map<DWORD, std::map <DWORD, std::map <int, std::string>>>::iterator i = mapRef.begin(); i != mapRef.end(); i++) {
		DWORD dwLockObjAddress = i->first;

		for (std::map <DWORD, std::map <int, std::string>>::iterator j = i->second.begin (); j != i->second.end(); j++) {
			DWORD dwThreadID = j->first;
			long lCount = j->second.size ();

			ASSERT (lCount >= 0);

			if (lCount > 0)
				if (Find (v, dwThreadID) == -1)
					v.push_back (dwThreadID);
		}
	}

	return v;
}

std::vector <DWORD> CDiagnosticDataThread::EnumLockAddresses(std::map <DWORD, std::map <DWORD, std::map <int, std::string>>> & mapRef)
{
	std::vector <DWORD> v;

	for (std::map<DWORD, std::map <DWORD, std::map <int, std::string>>>::iterator i = mapRef.begin(); i != mapRef.end(); i++) {
		DWORD dwLockObjAddress = i->first;

		if (Find (v, dwLockObjAddress) == -1)
			v.push_back (dwLockObjAddress);
	}

	return v;
}

std::vector <DWORD> CDiagnosticDataThread::EnumThreadRefs(DWORD dwLockObjAddress, std::map <DWORD, std::map <DWORD, std::map <int, std::string>>> & mapRef)
{
	std::vector <DWORD> v;

	for (std::map<DWORD, std::map <DWORD, std::map <int, std::string>>>::iterator i = mapRef.begin(); i != mapRef.end(); i++) {
		
		if (dwLockObjAddress == i->first) {
			for (std::map <DWORD, std::map <int, std::string>>::iterator j = i->second.begin (); j != i->second.end(); j++) {
				DWORD dwThreadID = j->first;
				long lCount = j->second.size ();

				if (lCount >= 1) 
					if (Find (v, dwThreadID) == -1)
						v.push_back (dwThreadID);
			}
		}
	}

	return v;
}


CString CDiagnosticDataThread::GetObjectState (DWORD dwLockObjectAddr)
{
	CString strState = _T ("[unknown]");

	if (CCriticalSection * pCriticalSection = (CCriticalSection *)dwLockObjectAddr) {
		CString str;

		strState.Format (_T ("m_hObject: 0x%p"), pCriticalSection->m_hObject);
		str.Format (_T ("LockCount: %d, RecursionCount: %d, OwningThread: %d, LockSemaphore: 0x%p, SpinCount: %d"), 
			pCriticalSection->m_sect.LockCount,
			pCriticalSection->m_sect.RecursionCount,
			pCriticalSection->m_sect.OwningThread,
			pCriticalSection->m_sect.LockSemaphore,
			pCriticalSection->m_sect.SpinCount);
		strState += _T (", ") + str;

		//PRTL_CRITICAL_SECTION_DEBUG DebugInfo;
		//LONG LockCount;
		//LONG RecursionCount;
		//HANDLE OwningThread;        // from the thread's ClientId->UniqueThread
		//HANDLE LockSemaphore;
		//ULONG_PTR SpinCount;        // force size on 64-bit systems when packed

		if (HANDLE hObject = pCriticalSection->m_hObject) {
			DWORD dwWait = ::WaitForSingleObject (hObject, 0);

			switch (dwWait) {
			case WAIT_OBJECT_0:		strState += _T (" WAIT_OBJECT_0 ");	break;
			case WAIT_TIMEOUT:		strState += _T (" WAIT_TIMEOUT  ");	break;
			case WAIT_ABANDONED:	strState += _T (" WAIT_ABANDONED");	break;
			}
		}
	}

	return strState;
}

//////////////////////////////////////////////////////////////////////////////////////////

CDiagnosticCriticalSection::CDiagnosticCriticalSection (const CString & strName)
:	m_hMutex (NULL),
	m_strName (strName),
	CCriticalSection ()
{
#if 0
	CString str;
	m_hMutex = ::CreateMutex (0, FALSE, strName);
	str.Format (_T ("0x%p: %s"), m_hMutex, strName);
	TRACEF (str);
#endif
}

CDiagnosticCriticalSection::~CDiagnosticCriticalSection ()
{
	if (::g_dwThreadID) {
		static __int64 nCalls = 0;

		if (HANDLE h = ::CreateEvent (NULL, FALSE, FALSE, std::wstring (_T ("TM_UNREGISTER_CRITICAL_SECTION:") + std::to_wstring (nCalls++)).c_str ())) {
			POST (CDiagnosticDataThread::TM_UNREGISTER_CRITICAL_SECTION, (WPARAM)this, (LPARAM)h);
			DWORD dwWait = ::WaitForSingleObject (h, 1000);
			ASSERT (dwWait == WAIT_OBJECT_0);
			::CloseHandle (h);
		}
	}

	if (m_hMutex) {
		::CloseHandle (m_hMutex);
		m_hMutex = NULL;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////

CDiagnosticSingleLock::CDiagnosticSingleLock (CDiagnosticCriticalSection* pObject, BOOL bInitialLock, LPCTSTR lpszFile, ULONG lLine, LPCTSTR lpszName)
:	m_pObject (pObject),
	m_strFile (lpszFile),
	m_strName (lpszName),
	m_lLine (lLine),
	m_bLocked (false),
	CSingleLock (Init (pObject, bInitialLock, lpszFile, lLine, lpszName), FALSE /* bInitialLock */)
{
	// all code goes in Init (to check state before CSingleLock constructor is called, and possibly locked)

	if (bInitialLock)
		Lock();
}

CDiagnosticCriticalSection * CDiagnosticSingleLock::Init (CDiagnosticCriticalSection* pObject, BOOL bInitialLock, LPCTSTR lpszFile, ULONG lLine, LPCTSTR lpszName)
{
	// can't rely on any member variables being set by constructor yet
	m_dwThreadID = ::GetCurrentThreadId ();
	ASSERT (lpszName);

	return pObject;
}


CDiagnosticSingleLock::~CDiagnosticSingleLock () 
{
	if (IsLocked ())
		Unlock ();
}

void CDiagnosticSingleLock::Init (int nInvertvalSeconds)
{	
	if (CDiagnosticDataThread * p = (CDiagnosticDataThread *)AfxBeginThread (RUNTIME_CLASS (CDiagnosticDataThread))) {
		CString str;
		::g_dwThreadID = ::GetThreadId (p->m_hThread);
		POST (CDiagnosticDataThread::TM_SET_INTERVAL, (WPARAM)nInvertvalSeconds, 0);
	}
}

void CDiagnosticSingleLock::Exit ()
{
	::g_dwThreadID = 0;
}

BOOL CDiagnosticSingleLock::IsLocked () 
{
	if (m_pObject) {
		if (m_pObject->m_hMutex) {
			return m_bLocked ? TRUE : FALSE;
		}
	}

	return CSingleLock::IsLocked ();
}


BOOL CDiagnosticSingleLock::Lock (DWORD dwTimeOut)
{
	CTime tmStart = CTime::GetCurrentTime ();

	ASSERT (!IsLocked ());

	POST (CDiagnosticDataThread::TM_REGISTER_CRITICAL_SECTION, (WPARAM)m_pObject, 0);
	
	if (m_pObject) {

		//ASSERT (!m_pObject->m_strName.IsEmpty ());
		
		while ((DWORD)m_pObject->m_sect.OwningThread != 0 && (DWORD)m_pObject->m_sect.OwningThread != m_dwThreadID) {
			CTimeSpan tm = CTime::GetCurrentTime () - tmStart;

			if (tm.GetTotalSeconds () > 10) {
				CString str;

				str.Format (_T ("******************** 0x%p: OwningThread: %d, current thread: %d"), 
					m_pObject,
					m_pObject->m_sect.OwningThread,
					m_dwThreadID);
				::OutputDebugString (CDiagnosticDataThread::Trace (str, m_strFile, m_lLine));
				break;
			}

			::Sleep (100);
		}
	}

	SchedulePush ();

	if (m_pObject) {
		if (m_pObject->m_hMutex) {
			DWORD dwWait = ::WaitForSingleObject (m_pObject->m_hMutex, dwTimeOut);

			if (dwWait == WAIT_OBJECT_0) {
				m_bLocked = true;
				Push ();
				return true;
			}

			if (dwWait == WAIT_ABANDONED)	TRACEF ("WAIT_ABANDONED");
			if (dwWait == WAIT_TIMEOUT)		TRACEF ("WAIT_TIMEOUT");
			if (dwWait == WAIT_FAILED)		TRACEF ("WAIT_FAILED");

			ASSERT (0);

			return false;
		}
	}

	CTimeSpan tm = CTime::GetCurrentTime () - tmStart;
	int nSeconds = tm.GetSeconds ();
	bool bResult = CSingleLock::Lock (dwTimeOut);
	
	if (IsLocked ())
		Push ();
	else
		ASSERT (0);

	return bResult;
}

BOOL CDiagnosticSingleLock::Unlock()
{
	SchedulePop ();

	if (m_pObject) {
		if (m_pObject->m_hMutex) {
			if (::ReleaseMutex (m_pObject->m_hMutex)) {
				m_bLocked = false;
				Pop ();
				return true;
			}

			return false;
		}
	}

	bool bResult = CSingleLock::Unlock ();

	if (!IsLocked ())
		Pop ();
	else 
		ASSERT (0);

	return bResult;
}
 
BOOL CDiagnosticSingleLock::Unlock(LONG lCount, LPLONG lPrevCount)
{
	ASSERT (0);
	return FALSE;

#if 0
	std::vector <std::wstring> & v = ::g_mapRef [(DWORD)m_pObject] [::GetCurrentThreadId ()];
	int nIndex = Find (v, Format ((std::wstring)m_strFile, m_lLine));

	ASSERT (nIndex != -1);
	::g_tmWatchdog = CTime::GetCurrentTime ();

	if (nIndex != -1)
		v.erase (v.begin () + nIndex);

	SchedulePop ();

	bool bResult = CSingleLock::Unlock (lCount, lPrevCount);

	if (bResult)
		Pop ();

	return bResult;
#endif
}

void CDiagnosticSingleLock::SchedulePush ()
{
	POST (CDiagnosticDataThread::TM_SCHEDULE_PUSH, (WPARAM)m_pObject, m_dwThreadID);
}

void CDiagnosticSingleLock::Push ()
{
	POST (CDiagnosticDataThread::TM_STROKE_WATCHDOG, 0, 0);
	POST (CDiagnosticDataThread::TM_REGISTER_THREAD_NAME, (WPARAM)::GetCurrentThreadId (), 0);
	POST (CDiagnosticDataThread::TM_COMMIT_PUSH, (WPARAM)m_pObject, m_dwThreadID);
}

void CDiagnosticSingleLock::EnumRegisteredThreads ()
{
	POST (CDiagnosticDataThread::TM_ENUM_REGISTERED_THREADS, 0, 0);
}

void CDiagnosticSingleLock::SchedulePop ()
{
	POST (CDiagnosticDataThread::TM_SCHEDULE_POP, (WPARAM)m_pObject, m_dwThreadID);
}

void CDiagnosticSingleLock::Pop ()
{
	POST (CDiagnosticDataThread::TM_STROKE_WATCHDOG, 0, 0);
	POST (CDiagnosticDataThread::TM_REGISTER_THREAD_NAME, (WPARAM)::GetCurrentThreadId (), 0);
	POST (CDiagnosticDataThread::TM_COMMIT_POP, (WPARAM)m_pObject, m_dwThreadID);
}


void CDiagnosticSingleLock::RegisterThreadName (const CString & strName, DWORD dwThreadID) 
{ 
	DWORD dwResult = 0;
	
	POST (CDiagnosticDataThread::TM_REGISTER_THREAD_NAME, (WPARAM)dwThreadID, (LPARAM)new CString (strName));
	::SendMessageTimeout (HWND_BROADCAST, WM_DEBUG_REGISTER_THREAD, (WPARAM)::GetCurrentProcessId (), (LPARAM)dwThreadID, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);
}

CString CDiagnosticSingleLock::TraceAll (LPCTSTR lpszFile, ULONG lLine, bool bShowCallstack)
{
	CString str = CTime::GetCurrentTime ().Format (_T ("TM_TRACE %c"));
	CDiagnosticDataThread::TRACE_STRUCT trace;

	trace.m_strFile		= lpszFile;
	trace.m_lLine		= lLine;
	trace.m_hFinished	= ::CreateEvent (NULL, TRUE, FALSE, str);

	POST (CDiagnosticDataThread::TM_TRACE, (WPARAM)&trace, 0);
	::WaitForSingleObject (trace.m_hFinished, INFINITE);
	::CloseHandle (trace.m_hFinished);

	return trace.m_strResult;
}

static std::string Format (const std::string & strFile, ULONG lLine)
{
	CString str;

	str.Format (_T ("%s(%d)"), a2w (strFile.c_str ()), lLine);

	return w2a (str);
}

#if 0 // TODO: rem
CString CDiagnosticSingleLock::TraceAll (LPCTSTR lpszFile, ULONG lLine, bool bShowCallstack)
{
	CString strResult;
	std::map <DWORD, std::map <DWORD, std::vector <std::wstring>>>	mapRef;
	std::map <std::wstring, DWORD>									mapLockName;
	std::map <DWORD, std::wstring>									mapThreadName;
	std::map <DWORD, std::vector <std::wstring>>					mapThreadStack;

	{
		//LOCK_INTERNAL (csInternal);

		mapRef			= ::g_mapRef;
		mapLockName		= ::g_mapLockName;
		mapThreadName	= ::g_mapThreadName;
		mapThreadStack	= ::g_mapThreadStack;
	}

	std::vector <DWORD> vThreads = GetThreadIds (mapRef);
	CString str;
	CStringArray v;

	for (std::map <DWORD, std::wstring>::iterator i = mapThreadName.begin(); i != mapThreadName.end(); i++) {
		DWORD dwThreadID = i->first;

		str.Format (_T ("%s[%d]"), i->second.c_str (), dwThreadID);
		v.Add (str);
	}

	CTimeSpan tm = CTime::GetCurrentTime () - GetWatchdog ();
	strResult += Trace (_T ("known threads: ") + implode (v), lpszFile, lLine);
	strResult += Trace (CTime::GetCurrentTime ().Format (_T ("%c: elapsed: ")) + tm.Format (_T ("%H:%M:%S")), lpszFile, lLine);

	for (int nThread = 0; nThread < vThreads.size (); nThread++) {
		DWORD dwThreadID = vThreads [nThread];
		CStringArray vObjects;

		str.Format (_T ("%s[%d]"), mapThreadName [dwThreadID].c_str (), dwThreadID);
		strResult += Trace (str, lpszFile, lLine);

		for (int i = 0; i < mapThreadStack [dwThreadID].size (); i++) 
			strResult += mapThreadStack [dwThreadID][i].c_str () + CString (_T ("\n"));
	}

	std::vector <DWORD> vAddr = EnumLockAddresses(mapRef);

	for (int n = 0; n < vAddr.size (); n++) {
		std::vector <DWORD> vRef = EnumThreadRefs (vAddr [n], mapRef);

		if (vRef.size () > 1) {
			//StackWalker s;

			strResult += Trace (_T ("possible deadlock condition: "), lpszFile, lLine);
			break;
		}
	}

	#if 0
	for (int nThread = 0; nThread < vThreads.size (); nThread++) {
		DWORD dwThreadID = vThreads [nThread];
		CStringArray vObjects;

		str.Format (_T ("%s[%d]"), mapThreadName [dwThreadID].c_str (), dwThreadID);
		strResult += Trace (str, lpszFile, lLine);

		for (int i = 0; i < mapThreadStack [dwThreadID].size (); i++) {
			str.Format (_T ("\t\tthread stack[%d]: %s"), i, mapThreadStack [dwThreadID][i].c_str ());
			strResult += Trace (str, lpszFile, lLine);
		}

		for (std::map<DWORD, std::map <DWORD, std::vector <std::wstring>>>::iterator i = mapRef.begin(); i != mapRef.end(); i++) {
			DWORD dwLockObjAddress = i->first;

			for (std::map <DWORD, std::vector <std::wstring>>::iterator j = i->second.begin (); j != i->second.end(); j++) {
				//DWORD dwThreadID = j->first;
				//long lCount = j->second.size ();

				if (dwThreadID == j->first && j->second.size ()) {
					str.Format (_T ("\t%s(%d)"), GetName ((CDiagnosticCriticalSection *)dwLockObjAddress, mapLockName), j->second.size ());
					strResult += Trace (str, lpszFile, lLine);

					//strResult += Trace (_T ("\t\t") + GetObjectState (dwLockObjAddress), lpszFile, lLine);

					for (int n = 0; n < j->second.size (); n++) {
						str.Format (_T ("\t\t%s"), j->second [n].c_str ());
						strResult += Trace (str, lpszFile, lLine);
					}
				}
			}
		}
	}

	std::vector <DWORD> vAddr = EnumLockAddresses(mapRef);

	for (int n = 0; n < vAddr.size (); n++) {
		std::vector <DWORD> vRef = EnumThreadRefs (vAddr [n], mapRef);

		if (vRef.size () > 1) {
			//StackWalker s;

			strResult += Trace (_T ("possible deadlock condition: "), lpszFile, lLine);

			/*
			for (int i = 0; i < vRef.size (); i++) {
				CString strState = GetObjectState (vAddr [n]);

				str.Format (_T ("\t%s[%d]: (%s) %s"), mapThreadName [vRef [i]].c_str (), vRef [i], strState, GetName ((CDiagnosticCriticalSection *)vAddr [n], mapLockName));
				strResult += Trace (str, lpszFile, lLine);
				//strResult += Trace (_T ("\t") + ToString (vRef [i]) + _T (":") + GetName ((CDiagnosticCriticalSection *)vAddr [n]), lpszFile, lLine);

				for (std::map<DWORD, std::map <DWORD, std::vector <std::wstring>>>::iterator i = mapRef.begin(); i != mapRef.end(); i++) {
					DWORD dwLockObjAddress = i->first;

					if (vAddr [n] == dwLockObjAddress) {
						for (std::map <DWORD, std::vector <std::wstring>>::iterator j = i->second.begin (); j != i->second.end(); j++) {
							for (int i = 0; i < j->second.size (); i++) {
								str.Format (_T ("\t\t%s"), j->second [i].c_str ());
								strResult += Trace (str, lpszFile, lLine);
							}
						}
					}
				}
			}
			*/

			/*
			if (bShowCallstack) {
				for (int i = 0; i < vRef.size (); i++) {
					DWORD dwThreadID = vRef [i];

					str.Format (_T ("\t%s[%d] call stack:"), mapThreadName [dwThreadID].c_str (), dwThreadID);
					strResult += Trace (str, lpszFile, lLine);

					if (HANDLE hThread = ::OpenThread (THREAD_ALL_ACCESS, FALSE, dwThreadID)) {
						if (dwThreadID == GetCurrentThreadId ())
							hThread = GetCurrentThread ();

						s.ShowCallstack (_T (__FILE__), __LINE__, hThread);
						std::vector <std::wstring> v = explode (s.GetOutput (), (TCHAR)'\n');

						for (int i = 0; i < v.size (); i++)
							strResult += Trace (_T ("\t\t") + (CString)v [i].c_str (), lpszFile, lLine);

						::CloseHandle (hThread);
					}
				}
			}
			*/
		}
	}
	#endif

	return strResult;
}
#endif

