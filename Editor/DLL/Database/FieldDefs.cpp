#include "stdafx.h"
#include "FieldDefs.h"
#include <tchar.h>

// DO NOT put the '[' or ']' chars in the names

DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszTable						= _T ("Lines");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszID							= _T ("ID");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszName						= _T ("Name");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszDesc						= _T ("Desc");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszMaintenance					= _T ("Maintenance");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszNoRead						= _T ("NoRead");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszResetScanInfo				= _T ("ResetScanInfo");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszMaxConsecutiveNoReads		= _T ("MaxConsecutiveNoReads");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszBufferOffset				= _T ("BufferOffset");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszSignificantChars			= _T ("SignificantChars");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAuxBoxIp					= _T ("AuxBoxIp");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAuxBoxEnabled				= _T ("AuxBoxEnabled");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszSerialParams1				= _T ("SerialParams1");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszSerialParams2				= _T ("SerialParams2");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszSerialParams3				= _T ("SerialParams3");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS32_A1					= _T ("AMS32_A1");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS32_A2					= _T ("AMS32_A2");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS32_A3					= _T ("AMS32_A3");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS32_A4					= _T ("AMS32_A4");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS32_A5					= _T ("AMS32_A5");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS32_A6					= _T ("AMS32_A6");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS32_A7					= _T ("AMS32_A7");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS256_A1					= _T ("AMS256_A1");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS256_A2					= _T ("AMS256_A2");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS256_A3					= _T ("AMS256_A3");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS256_A4					= _T ("AMS256_A4");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS256_A5					= _T ("AMS256_A5");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS256_A6					= _T ("AMS256_A6");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS256_A7					= _T ("AMS256_A7");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszAMS_Interval				= _T ("AMS_Interval");
DB_API LPCTSTR FoxjetDatabase::Lines::m_lpszPrinterID					= _T ("PrinterID");

DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszTable				= _T ("Heads");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszID					= _T ("ID");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszPanelID				= _T ("PanelID");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszName				= _T ("Name");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszUID					= _T ("UID");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszExtEnc				= _T ("Encoder");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszDirection			= _T ("Direction");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszPhotocellDelay		= _T ("PhotocellDelay");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszHorzRes				= _T ("HorzRes");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszEnabled				= _T ("Enabled");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszRelativeHeight		= _T ("RelativeHeight");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszChannels			= _T ("Channels");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszHeadAngle			= _T ("Angle");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszHeadType			= _T ("HeadType");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszPhotocell			= _T ("Photocell");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszSharePhotocell		= _T ("SharePhotocell");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszShareEncoder		= _T ("ShareEncoder");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszInverted			= _T ("Inverted");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszRemoteHead			= _T ("RemoteHead");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszMasterHead			= _T ("MasterHead");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszIntTachSpeed		= _T ("IntTachSpeed");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszNozzleSpan			= _T ("NozzleSpan");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszPhotocellRate		= _T ("PhotocellRate");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszSerialParams		= _T ("SerialParams");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszEncoderDivisor		= _T ("EncoderDivisor");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszDoublePulse			= _T ("DoublePulse");
DB_API LPCTSTR FoxjetDatabase::Heads::m_lpszDigiNET				= _T ("DigiNET");
																
DB_API LPCTSTR FoxjetDatabase::Tasks::m_lpszTable				= _T ("Tasks");
DB_API LPCTSTR FoxjetDatabase::Tasks::m_lpszID					= _T ("ID");
DB_API LPCTSTR FoxjetDatabase::Tasks::m_lpszLineID				= _T ("LineID");
DB_API LPCTSTR FoxjetDatabase::Tasks::m_lpszBoxID				= _T ("BoxID");
DB_API LPCTSTR FoxjetDatabase::Tasks::m_lpszName				= _T ("Name");
DB_API LPCTSTR FoxjetDatabase::Tasks::m_lpszDesc				= _T ("Desc");
DB_API LPCTSTR FoxjetDatabase::Tasks::m_lpszDownload			= _T ("DownloadString");
DB_API LPCTSTR FoxjetDatabase::Tasks::m_lpszTransformation		= _T ("Transformation");
																
DB_API LPCTSTR FoxjetDatabase::Messages::m_lpszTable			= _T ("Messages");
DB_API LPCTSTR FoxjetDatabase::Messages::m_lpszID				= _T ("ID");
DB_API LPCTSTR FoxjetDatabase::Messages::m_lpszHeadID			= _T ("HeadID");
DB_API LPCTSTR FoxjetDatabase::Messages::m_lpszTaskID			= _T ("TaskID");
DB_API LPCTSTR FoxjetDatabase::Messages::m_lpszName				= _T ("Name");
DB_API LPCTSTR FoxjetDatabase::Messages::m_lpszDesc				= _T ("Desc");
DB_API LPCTSTR FoxjetDatabase::Messages::m_lpszData				= _T ("Data");
DB_API LPCTSTR FoxjetDatabase::Messages::m_lpszHeight			= _T ("Height");
DB_API LPCTSTR FoxjetDatabase::Messages::m_lpszIsUpdated		= _T ("IsUpdated");
																
DB_API LPCTSTR FoxjetDatabase::Boxes::m_lpszTable				= _T ("Boxes");
DB_API LPCTSTR FoxjetDatabase::Boxes::m_lpszID					= _T ("ID");
DB_API LPCTSTR FoxjetDatabase::Boxes::m_lpszName				= _T ("Name");
DB_API LPCTSTR FoxjetDatabase::Boxes::m_lpszDesc				= _T ("Desc");
DB_API LPCTSTR FoxjetDatabase::Boxes::m_lpszWidth				= _T ("Width");
DB_API LPCTSTR FoxjetDatabase::Boxes::m_lpszHeight				= _T ("Height");
DB_API LPCTSTR FoxjetDatabase::Boxes::m_lpszLength				= _T ("Length");
																
DB_API LPCTSTR FoxjetDatabase::BoxToLine::m_lpszTable			= _T ("BoxToLine");
DB_API LPCTSTR FoxjetDatabase::BoxToLine::m_lpszLineID			= _T ("LineID");
DB_API LPCTSTR FoxjetDatabase::BoxToLine::m_lpszBoxID			= _T ("BoxID");
																
DB_API LPCTSTR FoxjetDatabase::Panels::m_lpszTable				= _T ("Panels");
DB_API LPCTSTR FoxjetDatabase::Panels::m_lpszID					= _T ("ID");
DB_API LPCTSTR FoxjetDatabase::Panels::m_lpszLineID				= _T ("LineID");
DB_API LPCTSTR FoxjetDatabase::Panels::m_lpszNumber				= _T ("Number");
DB_API LPCTSTR FoxjetDatabase::Panels::m_lpszName				= _T ("Name");
DB_API LPCTSTR FoxjetDatabase::Panels::m_lpszDirection			= _T ("Direction");
DB_API LPCTSTR FoxjetDatabase::Panels::m_lpszOrientation		= _T ("Orientation");
																
DB_API LPCTSTR FoxjetDatabase::Images::m_lpszTable				= _T ("Images");
DB_API LPCTSTR FoxjetDatabase::Images::m_lpszBoxID				= _T ("BoxID");
DB_API LPCTSTR FoxjetDatabase::Images::m_lpszPanel				= _T ("Panel");
DB_API LPCTSTR FoxjetDatabase::Images::m_lpszImage				= _T ("Image");
DB_API LPCTSTR FoxjetDatabase::Images::m_lpszAttributes			= _T ("Attributes");
																
DB_API LPCTSTR FoxjetDatabase::Options::m_lpszTable				= _T ("Options");
DB_API LPCTSTR FoxjetDatabase::Options::m_lpszID				= _T ("ID");
DB_API LPCTSTR FoxjetDatabase::Options::m_lpszOptionDesc		= _T ("OptionDesc");
																
DB_API LPCTSTR FoxjetDatabase::Users::m_lpszTable				= _T ("Users");
DB_API LPCTSTR FoxjetDatabase::Users::m_lpszID					= _T ("ID");
DB_API LPCTSTR FoxjetDatabase::Users::m_lpszFirstName			= _T ("FirstName");
DB_API LPCTSTR FoxjetDatabase::Users::m_lpszLastName			= _T ("LastName");
DB_API LPCTSTR FoxjetDatabase::Users::m_lpszUserName			= _T ("UserName");
DB_API LPCTSTR FoxjetDatabase::Users::m_lpszPassW				= _T ("PassW");
DB_API LPCTSTR FoxjetDatabase::Users::m_lpszSecurityGroupID		= _T ("SecurityGroupID");
																
DB_API LPCTSTR FoxjetDatabase::SecurityGroups::m_lpszTable		= _T ("SecurityGroups");
DB_API LPCTSTR FoxjetDatabase::SecurityGroups::m_lpszID			= _T ("ID");
DB_API LPCTSTR FoxjetDatabase::SecurityGroups::m_lpszName		= _T ("Name");
															
DB_API LPCTSTR FoxjetDatabase::OptionsToGroup::m_lpszTable		= _T ("OptionsToGroup");
DB_API LPCTSTR FoxjetDatabase::OptionsToGroup::m_lpszGroupID	= _T ("GroupsID");
DB_API LPCTSTR FoxjetDatabase::OptionsToGroup::m_lpszOptionsID	= _T ("OptionsID");
															
DB_API LPCTSTR FoxjetDatabase::Shifts::m_lpszTable				= _T ("Shifts");
DB_API LPCTSTR FoxjetDatabase::Shifts::m_lpszID					= _T ("ID");
DB_API LPCTSTR FoxjetDatabase::Shifts::m_lpszLineID				= _T ("LineID");
DB_API LPCTSTR FoxjetDatabase::Shifts::m_lpszName				= _T ("Name");
DB_API LPCTSTR FoxjetDatabase::Shifts::m_lpszTime				= _T ("StartTime");
DB_API LPCTSTR FoxjetDatabase::Shifts::m_lpszCode				= _T ("Code");
																
DB_API LPCTSTR FoxjetDatabase::Reports::m_lpszTable				= _T ("Reports");
DB_API LPCTSTR FoxjetDatabase::Reports::m_lpszTime				= _T ("Time");
DB_API LPCTSTR FoxjetDatabase::Reports::m_lpszAction			= _T ("Action");
DB_API LPCTSTR FoxjetDatabase::Reports::m_lpszUsername			= _T ("Username");
DB_API LPCTSTR FoxjetDatabase::Reports::m_lpszCounts			= _T ("Counts");
DB_API LPCTSTR FoxjetDatabase::Reports::m_lpszLine				= _T ("Line");
DB_API LPCTSTR FoxjetDatabase::Reports::m_lpszTaskName			= _T ("TaskName");
DB_API LPCTSTR FoxjetDatabase::Reports::m_lpszType				= _T ("Type");
DB_API LPCTSTR FoxjetDatabase::Reports::m_lpszPrinterID			= _T ("PrinterID");

DB_API LPCTSTR FoxjetDatabase::BarcodeScans::m_lpszTable		= _T("BarcodeScans");
DB_API LPCTSTR FoxjetDatabase::BarcodeScans::m_lpszID			= _T("ID");
DB_API LPCTSTR FoxjetDatabase::BarcodeScans::m_lpszLineID		= _T("LineID");
DB_API LPCTSTR FoxjetDatabase::BarcodeScans::m_lpszTime			= _T("Time");
DB_API LPCTSTR FoxjetDatabase::BarcodeScans::m_lpszScanner		= _T("Scanner");
DB_API LPCTSTR FoxjetDatabase::BarcodeScans::m_lpszTaskname		= _T("Taskname");
DB_API LPCTSTR FoxjetDatabase::BarcodeScans::m_lpszBarcode		= _T("Barcode");
DB_API LPCTSTR FoxjetDatabase::BarcodeScans::m_lpszTotalScans	= _T("TotalScans");
DB_API LPCTSTR FoxjetDatabase::BarcodeScans::m_lpszGoodScans	= _T("GoodScans");

DB_API LPCTSTR FoxjetDatabase::Deleted::m_lpszTable				= _T ("Deleted");
DB_API LPCTSTR FoxjetDatabase::Deleted::m_lpszID				= _T ("ID");
DB_API LPCTSTR FoxjetDatabase::Deleted::m_lpszLineID			= _T ("LineID");
DB_API LPCTSTR FoxjetDatabase::Deleted::m_lpszTaskName			= _T ("TaskName");
DB_API LPCTSTR FoxjetDatabase::Deleted::m_lpszData				= _T ("Data");

DB_API LPCTSTR FoxjetDatabase::Settings::m_lpszTable			= _T ("Settings");
DB_API LPCTSTR FoxjetDatabase::Settings::m_lpszLineID			= _T ("LineID");
DB_API LPCTSTR FoxjetDatabase::Settings::m_lpszKey				= _T ("Key");
DB_API LPCTSTR FoxjetDatabase::Settings::m_lpszData				= _T ("Data");

DB_API LPCTSTR FoxjetDatabase::Printers::m_lpszTable			= _T ("Printers");
DB_API LPCTSTR FoxjetDatabase::Printers::m_lpszID				= _T ("ID");
DB_API LPCTSTR FoxjetDatabase::Printers::m_lpszName				= _T ("Name");
DB_API LPCTSTR FoxjetDatabase::Printers::m_lpszAddress			= _T ("Address");
DB_API LPCTSTR FoxjetDatabase::Printers::m_lpszMaster			= _T ("Master");
DB_API LPCTSTR FoxjetDatabase::Printers::m_lpszType				= _T ("Type");

DB_API LPCTSTR FoxjetDatabase::Globals::m_lpszCoupled			= _T ("Coupled");
DB_API LPCTSTR FoxjetDatabase::Globals::m_lpszLean				= _T ("Lean");
DB_API ULONG FoxjetDatabase::Globals::m_lGlobalID				= 0;
DB_API LPCTSTR FoxjetDatabase::Globals::m_lpszSerialDownload	= _T ("Serial download");
DB_API LPCTSTR FoxjetDatabase::Globals::m_lpszResetCounts		= _T ("Reset counts on restart");
DB_API LPCTSTR FoxjetDatabase::Globals::m_lpszRequireLogin		= _T ("Scan & shoot - require login");
