#ifdef __DATABASE_INLINE__

#include <math.h>

namespace FoxjetDatabase
{

	inline long double FoxjetDatabase::CalcInkUsage (HEADTYPE head, INKTYPE ink, ULONG lPixels)
	{
		long lInkDrop = 1;

		/*
			UJ2_352_32			5mL/429pL		= 11,655,011 (dots inklow->inkout)
			ALPHA_CODER			5mL/(7 * 56)pL	= 12,755,102 (dots inklow->inkout)
			UJ2_192_32			5mL/234pL		= 21,367,521 (dots inklow->inkout)
			UJ2_192_32NP		5ml/(6 * 48)pL	= 17,361,111 (dots inklow->inkout)
			UJ2_96_32			5mL/189pL		= 26,455,026 (dots inklow->inkout)
			GRAPHICS_384_128	5mL/132pL		= 37,878,787 (dots inklow->inkout)
			GRAPHICS_768_256	5mL/132pL		= 37,878,787 (dots inklow->inkout)
		*/

		switch (head) {
		case UJ2_352_32:
			lInkDrop = 429;
			break;
		case ALPHA_CODER:
			lInkDrop = 7 * 56;
			break;
		case UJ2_192_32:
			lInkDrop = 234;
			break;
		case UJ2_192_32NP:
			lInkDrop = 6 * 48;
			/*
			{
				const double dMaxPicoLiters = (GetInkLowVolume (head) / (0.000000000001 * 1000.0));
				lInkDrop = (long)(dMaxPicoLiters / 34000000.0);
			}
			*/
			break;
		case UJ2_96_32:
			lInkDrop = 189;
			break;
		case GRAPHICS_768_256:
			lInkDrop = 120; //37;  //lInkDrop = 141; //47; //132;
			break;
		case GRAPHICS_384_128:
			lInkDrop = 120; //40;  //lInkDrop = 141; //47; //132;
			break;
		case IV_72:
			// 220,000,000 pixels in 5 gallons (5 gallons = 18.92706 liters)
			// (18.92706 / 0.000000000001) / 220,000,000
			lInkDrop = (long)((18.92706 / 0.000000000001) / 220000000.0);
			break;
		}

	/* TODO: rem
		fj_print_head_CountsAdd ( &(pBRAM->countInkLow)   , &rc );

		d  = (DOUBLE)pBRAM->countInkLow.lDotsHi * (DOUBLE)1000000.0;
		d += (DOUBLE)pBRAM->countInkLow.lDotsLo;
		d *= (DOUBLE)pfph->pfphy->lInkDrop*(DOUBLE).000000001;

		pBRAM->dInkLowML = d;
	*/
		
		long double dResult = (long double)lPixels;

		dResult *= (long double)lInkDrop * (long double)0.000000001; 

		return dResult;
	}

////////////////////////////////////////////////////////////////////////////////////////////////

	inline void tagTASKSTRUCT::SetID (ULONG lID)
	{
		m_lID = lID;

		for (int i = 0; i < m_vMsgs.GetSize (); i++) 
			m_vMsgs [i].m_lTaskID = lID;
	}

	inline CSize TASKSTRUCT::GetPanelSize (const CArray <FoxjetDatabase::PANELSTRUCT, FoxjetDatabase::PANELSTRUCT &> & v, ULONG lPanelID, const FoxjetDatabase::BOXSTRUCT & box) const
	{
		ULONG x = 0, y = 0, z = 0;
		PANELSTRUCT panel;
		const COrientation orient (HIWORD (m_lTransformation) + 1, (ORIENTATION)LOWORD (m_lTransformation));

		for (int i = 0; i < v.GetSize (); i++) {
			if (v [i].m_lID == lPanelID) {
				panel = v [i];
				break;
			}
		}

		const COrientation map = MapPanel (panel.m_nNumber, orient );
		int nPanel = map.m_nPanel;

		if (nPanel >= 1 && nPanel <= 6) {
			const ULONG w = box.m_lWidth;
			const ULONG l = box.m_lLength;
			const ULONG h = box.m_lHeight;
			static const int nFaceIndex [6] = 
			{
				0, // face 1
				2, // face 2
				0, // face 3
				2, // face 4
				4, // face 5
				4, // face 6
			};
			int nIndex = nFaceIndex [nPanel - 1];
			const ULONG lTable [6][3] = 
			{
				{ w, l, h }, // 0
				{ w, h, l }, // 1
				{ l, w, h }, // 2
				{ l, h, w }, // 3
				{ h, l, w }, // 4
				{ h, w, l }, // 5
			};

			if (map.m_orient == ROT_90 || map.m_orient == ROT_270)
				nIndex++;

			ASSERT (nIndex >= 0 && nIndex < 6);

			x = lTable [nIndex][0];
			y = lTable [nIndex][1];
			z = lTable [nIndex][2];
		}

		return CSize (y, z);
	}

}; // namespace FoxjetDatabase

#endif //__DATABASE_INLINE__









