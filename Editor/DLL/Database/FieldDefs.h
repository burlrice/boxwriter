#ifndef __FEILDDEFS_H__
#define __FEILDDEFS_H__

#pragma once

#include "DbApi.h"

namespace FoxjetDatabase
{
	namespace BarcodeScans
	{
		extern DB_API LPCTSTR m_lpszID;
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszTime;
		extern DB_API LPCTSTR m_lpszLineID;
		extern DB_API LPCTSTR m_lpszScanner;
		extern DB_API LPCTSTR m_lpszTaskname;
		extern DB_API LPCTSTR m_lpszBarcode;
		extern DB_API LPCTSTR m_lpszTotalScans;
		extern DB_API LPCTSTR m_lpszGoodScans;
	};

	namespace Boxes
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszID;
		extern DB_API LPCTSTR m_lpszName;
		extern DB_API LPCTSTR m_lpszDesc;
		extern DB_API LPCTSTR m_lpszWidth;
		extern DB_API LPCTSTR m_lpszHeight;
		extern DB_API LPCTSTR m_lpszLength;
	};

	namespace BoxToLine
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszLineID;
		extern DB_API LPCTSTR m_lpszBoxID;
	};

	namespace Deleted
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszID;
		extern DB_API LPCTSTR m_lpszLineID;
		extern DB_API LPCTSTR m_lpszTaskName;
		extern DB_API LPCTSTR m_lpszData;
	};

	namespace Heads
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszID;
		extern DB_API LPCTSTR m_lpszPanelID;
		extern DB_API LPCTSTR m_lpszName;
		extern DB_API LPCTSTR m_lpszUID;
		extern DB_API LPCTSTR m_lpszRelativeHeight;
		extern DB_API LPCTSTR m_lpszChannels;
		extern DB_API LPCTSTR m_lpszHorzRes;
		extern DB_API LPCTSTR m_lpszPhotocellDelay;
		extern DB_API LPCTSTR m_lpszExtEnc;
		extern DB_API LPCTSTR m_lpszEnabled;
		extern DB_API LPCTSTR m_lpszDirection;
		extern DB_API LPCTSTR m_lpszHeadAngle;
		extern DB_API LPCTSTR m_lpszHeadType;
		extern DB_API LPCTSTR m_lpszPhotocell;
		extern DB_API LPCTSTR m_lpszSharePhotocell;
		extern DB_API LPCTSTR m_lpszShareEncoder;
		extern DB_API LPCTSTR m_lpszInverted;
		extern DB_API LPCTSTR m_lpszRemoteHead;
		extern DB_API LPCTSTR m_lpszMasterHead;
		extern DB_API LPCTSTR m_lpszIntTachSpeed;
		extern DB_API LPCTSTR m_lpszNozzleSpan;
		extern DB_API LPCTSTR m_lpszPhotocellRate;
		extern DB_API LPCTSTR m_lpszSerialParams;
		extern DB_API LPCTSTR m_lpszEncoderDivisor;
		extern DB_API LPCTSTR m_lpszDoublePulse;
		extern DB_API LPCTSTR m_lpszDigiNET;
	};

	namespace Images
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszBoxID;
		extern DB_API LPCTSTR m_lpszPanel;
		extern DB_API LPCTSTR m_lpszImage;
		extern DB_API LPCTSTR m_lpszAttributes;
	}

	namespace Lines
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszID;
		extern DB_API LPCTSTR m_lpszName;
		extern DB_API LPCTSTR m_lpszDesc;
		extern DB_API LPCTSTR m_lpszMaintenance;
		extern DB_API LPCTSTR m_lpszNoRead;
		extern DB_API LPCTSTR m_lpszResetScanInfo;
		extern DB_API LPCTSTR m_lpszMaxConsecutiveNoReads;
		extern DB_API LPCTSTR m_lpszBufferOffset;
		extern DB_API LPCTSTR m_lpszSignificantChars;
		extern DB_API LPCTSTR m_lpszAuxBoxIp;
		extern DB_API LPCTSTR m_lpszAuxBoxEnabled;
		extern DB_API LPCTSTR m_lpszSerialParams1;
		extern DB_API LPCTSTR m_lpszSerialParams2;
		extern DB_API LPCTSTR m_lpszSerialParams3;
		extern DB_API LPCTSTR m_lpszAMS32_A1;
		extern DB_API LPCTSTR m_lpszAMS32_A2;
		extern DB_API LPCTSTR m_lpszAMS32_A3;
		extern DB_API LPCTSTR m_lpszAMS32_A4;
		extern DB_API LPCTSTR m_lpszAMS32_A5;
		extern DB_API LPCTSTR m_lpszAMS32_A6;
		extern DB_API LPCTSTR m_lpszAMS32_A7;
		extern DB_API LPCTSTR m_lpszAMS256_A1;
		extern DB_API LPCTSTR m_lpszAMS256_A2;
		extern DB_API LPCTSTR m_lpszAMS256_A3;
		extern DB_API LPCTSTR m_lpszAMS256_A4;
		extern DB_API LPCTSTR m_lpszAMS256_A5;
		extern DB_API LPCTSTR m_lpszAMS256_A6;
		extern DB_API LPCTSTR m_lpszAMS256_A7;
		extern DB_API LPCTSTR m_lpszAMS_Interval;
		extern DB_API LPCTSTR m_lpszPrinterID;
	};

	namespace Messages
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszID;
		extern DB_API LPCTSTR m_lpszHeadID;
		extern DB_API LPCTSTR m_lpszTaskID;
		extern DB_API LPCTSTR m_lpszName;
		extern DB_API LPCTSTR m_lpszDesc;
		extern DB_API LPCTSTR m_lpszData;
		extern DB_API LPCTSTR m_lpszHeight;
		extern DB_API LPCTSTR m_lpszIsUpdated;
	}; 

	namespace Options
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszID;
		extern DB_API LPCTSTR m_lpszOptionDesc;
	};

	namespace OptionsToGroup
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszGroupID;
		extern DB_API LPCTSTR m_lpszOptionsID;
	};

	namespace Panels
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszID;
		extern DB_API LPCTSTR m_lpszLineID;
		extern DB_API LPCTSTR m_lpszNumber;
		extern DB_API LPCTSTR m_lpszName;
		extern DB_API LPCTSTR m_lpszDirection;
		extern DB_API LPCTSTR m_lpszOrientation;
	};

	namespace Reports
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszTime;
		extern DB_API LPCTSTR m_lpszAction;
		extern DB_API LPCTSTR m_lpszUsername;
		extern DB_API LPCTSTR m_lpszCounts;
		extern DB_API LPCTSTR m_lpszLine;
		extern DB_API LPCTSTR m_lpszTaskName;
		extern DB_API LPCTSTR m_lpszType;
		extern DB_API LPCTSTR m_lpszPrinterID;
	};

	namespace SecurityGroups
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszID;
		extern DB_API LPCTSTR m_lpszName;
	};

	namespace Settings
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszLineID;
		extern DB_API LPCTSTR m_lpszKey;
		extern DB_API LPCTSTR m_lpszData;
	};

	namespace Shifts
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszID;
		extern DB_API LPCTSTR m_lpszLineID;
		extern DB_API LPCTSTR m_lpszName;
		extern DB_API LPCTSTR m_lpszTime;
		extern DB_API LPCTSTR m_lpszCode;
	};

	namespace Tasks
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszID;
		extern DB_API LPCTSTR m_lpszLineID;
		extern DB_API LPCTSTR m_lpszBoxID;
		extern DB_API LPCTSTR m_lpszName;
		extern DB_API LPCTSTR m_lpszDesc;
		extern DB_API LPCTSTR m_lpszDownload;
		extern DB_API LPCTSTR m_lpszTransformation;
	}; 

	namespace Users
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszID;
		extern DB_API LPCTSTR m_lpszSecurityGroupID;
		extern DB_API LPCTSTR m_lpszFirstName;
		extern DB_API LPCTSTR m_lpszLastName;
		extern DB_API LPCTSTR m_lpszUserName;
		extern DB_API LPCTSTR m_lpszPassW;
	};

	namespace Printers
	{
		extern DB_API LPCTSTR m_lpszTable;
		extern DB_API LPCTSTR m_lpszID;
		extern DB_API LPCTSTR m_lpszName;
		extern DB_API LPCTSTR m_lpszAddress;
		extern DB_API LPCTSTR m_lpszMaster;
		extern DB_API LPCTSTR m_lpszType;
	};

	namespace Globals
	{
		extern DB_API LPCTSTR	m_lpszCoupled;
		extern DB_API LPCTSTR	m_lpszLean;
		extern DB_API ULONG		m_lGlobalID;
		extern DB_API LPCTSTR	m_lpszSerialDownload;
		extern DB_API LPCTSTR	m_lpszResetCounts;
		extern DB_API LPCTSTR	m_lpszRequireLogin;
	};

}; //namespace FoxjetDatabase


#endif //__FEILDDEFS_H__
