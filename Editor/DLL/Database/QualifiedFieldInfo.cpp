#include "stdafx.h"
#include "OdbcRecordset.h"
#include "Debug.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;

CQualifiedFieldInfo::CQualifiedFieldInfo ()
:	CODBCFieldInfo ()
{
	::ZeroMemory (&m_value, sizeof (m_value));
	m_value.m_dwType = DBVT_NULL;
}

CQualifiedFieldInfo::CQualifiedFieldInfo (const CQualifiedFieldInfo & rhs)
:	m_strTable (rhs.m_strTable), 
	m_value (rhs.m_value),
	CODBCFieldInfo (rhs)
{
}

CQualifiedFieldInfo & CQualifiedFieldInfo::operator = (const CQualifiedFieldInfo & rhs)
{
	CODBCFieldInfo::operator = (rhs);

	if (this != &rhs) {
		m_strTable = rhs.m_strTable;
		m_value = rhs.m_value;
	}

	return * this;
}

CQualifiedFieldInfo::~CQualifiedFieldInfo ()
{
}

