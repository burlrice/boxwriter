// CompactDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "CompactDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////
// CCompactDlg dialog


CCompactDlg::CCompactDlg(CWnd* pParent /*=NULL*/)
:	CDialog(IDD_COMPACT, pParent)
{
	//{{AFX_DATA_INIT(CCompactDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCompactDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCompactDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCompactDlg, CDialog)
	//{{AFX_MSG_MAP(CCompactDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCompactDlg message handlers
