#ifndef __MSGBOX_H__
#define __MSGBOX_H__

#include "DbApi.h"

namespace FoxjetMessageBox 
{
	DB_API void SetAfxMainWndValid (bool b);

	DB_API int MsgBox (LPCTSTR lpszText, LPCTSTR lpszCaption = _T (""), UINT nType = MB_OK);
	DB_API int MsgBox (HWND hWnd, LPCTSTR lpText, LPCTSTR lpCaption = _T (""), UINT uType = MB_OK);
	DB_API int MsgBox (CWnd & wnd, LPCTSTR lpszText, LPCTSTR lpszCaption = _T (""), UINT nType = MB_OK);

	DB_API int MsgBox (LPCTSTR lpszText, UINT nType, UINT nIDHelp = 0);
	DB_API int MsgBox (HWND hWnd, LPCTSTR lpszText, UINT nType = MB_OK, UINT nIDHelp = 0);
	//DB_API int MsgBox (CWnd & wnd, LPCTSTR lpszText, UINT nType, UINT nIDHelp);

	//DB_API int MsgBox (UINT nIDPrompt, UINT nType = MB_OK, UINT nIDHelp = (UINT)-1);
	//DB_API int MsgBox (HWND hWnd, UINT nIDPrompt, UINT nType = MB_OK, UINT nIDHelp = (UINT)-1);
	//DB_API int MsgBox (CWnd & wnd, UINT nIDPrompt, UINT nType = MB_OK, UINT nIDHelp = (UINT)-1);

}; //namespace FoxjetMessageBox

#endif //__MSGBOX_H__