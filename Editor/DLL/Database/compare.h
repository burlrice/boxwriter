#ifndef __COMPARE_H__
#define __COMPARE_H__

#pragma once

#include "Database.h"
#include "DbApi.h"

namespace FoxjetDatabase
{
	DB_API bool operator == (const IMAGESTRUCT & lhs, const IMAGESTRUCT & rhs);
	DB_API bool operator != (const IMAGESTRUCT & lhs, const IMAGESTRUCT & rhs);

	DB_API bool operator == (const BOXSTRUCT & lhs, const BOXSTRUCT & rhs);
	DB_API bool operator != (const BOXSTRUCT & lhs, const BOXSTRUCT & rhs);

	DB_API bool operator == (const BOXLINKSTRUCT & lhs, const BOXLINKSTRUCT & rhs);
	DB_API bool operator != (const BOXLINKSTRUCT & lhs, const BOXLINKSTRUCT & rhs);

	DB_API bool operator == (const LINESTRUCT & lhs, const LINESTRUCT & rhs);
	DB_API bool operator != (const LINESTRUCT & lhs, const LINESTRUCT & rhs);

	DB_API bool operator == (const PANELSTRUCT & lhs, const PANELSTRUCT & rhs);
	DB_API bool operator != (const PANELSTRUCT & lhs, const PANELSTRUCT & rhs);

	DB_API bool operator == (const MESSAGESTRUCT & lhs, const MESSAGESTRUCT & rhs);
	DB_API bool operator != (const MESSAGESTRUCT & lhs, const MESSAGESTRUCT & rhs);

	DB_API bool operator == (const TASKSTRUCT & lhs, const TASKSTRUCT & rhs);
	DB_API bool operator != (const TASKSTRUCT & lhs, const TASKSTRUCT & rhs);

	DB_API bool operator == (const HEADSTRUCT & lhs, const HEADSTRUCT & rhs);
	DB_API bool operator != (const HEADSTRUCT & lhs, const HEADSTRUCT & rhs);

	DB_API bool operator == (const OPTIONSSTRUCT & lhs, const OPTIONSSTRUCT & rhs);
	DB_API bool operator != (const OPTIONSSTRUCT & lhs, const OPTIONSSTRUCT & rhs);

	DB_API bool operator == (const SECURITYGROUPSTRUCT & lhs, const SECURITYGROUPSTRUCT & rhs);
	DB_API bool operator != (const SECURITYGROUPSTRUCT & lhs, const SECURITYGROUPSTRUCT & rhs);

	DB_API bool operator == (const USERSTRUCT & lhs, const USERSTRUCT & rhs);
	DB_API bool operator != (const USERSTRUCT & lhs, const USERSTRUCT & rhs);

	DB_API bool operator == (const SHIFTSTRUCT & lhs, const SHIFTSTRUCT & rhs);
	DB_API bool operator != (const SHIFTSTRUCT & lhs, const SHIFTSTRUCT & rhs);

	DB_API bool operator == (const REPORTSTRUCT & lhs, const REPORTSTRUCT & rhs);
	DB_API bool operator != (const REPORTSTRUCT & lhs, const REPORTSTRUCT & rhs);

	DB_API bool operator == (const BCSCANSTRUCT & lhs, const BCSCANSTRUCT & rhs);
	DB_API bool operator != (const BCSCANSTRUCT & lhs, const BCSCANSTRUCT & rhs);

	DB_API bool operator == (const DELETEDSTRUCT & lhs, const DELETEDSTRUCT & rhs);
	DB_API bool operator != (const DELETEDSTRUCT & lhs, const DELETEDSTRUCT & rhs);

	DB_API bool operator == (const SETTINGSSTRUCT & lhs, const SETTINGSSTRUCT & rhs);
	DB_API bool operator != (const SETTINGSSTRUCT & lhs, const SETTINGSSTRUCT & rhs);

	DB_API  bool operator == (const COrientation & lhs, const COrientation & rhs);
	DB_API  bool operator != (const COrientation & lhs, const COrientation & rhs);

	DB_API bool operator == (const PRINTERSTRUCT & lhs, const PRINTERSTRUCT & rhs);
	DB_API bool operator != (const PRINTERSTRUCT & lhs, const PRINTERSTRUCT & rhs);
}; //namespace FoxjetDatabase

#endif //__COMPARE_H__
