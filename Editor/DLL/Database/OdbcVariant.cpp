#include "stdafx.h"
#include "OdbcRecordset.h"
#include "Database.h"
#include "Debug.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;

static bool Copy (COdbcVariant & lhs, const COdbcVariant & rhs, bool bRealloc);
static double _ttod (const TCHAR * string);

template <class T> 
static const T Constrict (const T min, T value, const T max)
{
	if (value < min)
		value = min;
	else if (value > max)
		value = max;

	return value;
}

static double _ttod (const TCHAR * string)
{
	double d = 0;
	_stscanf (string, _T ("%f"), &d);
	return d;
}

COdbcVariant::COdbcVariant (bool bVal)
{
	m_dwType = DBVT_BOOL;
	m_boolVal = bVal;
}

COdbcVariant::COdbcVariant (BYTE chVal)
{
	m_dwType = DBVT_UCHAR;
	m_chVal = chVal;
}

COdbcVariant::COdbcVariant (int iVal)
{
	m_dwType = DBVT_SHORT;
	m_iVal = iVal;
}

COdbcVariant::COdbcVariant (long lVal)
{
	m_dwType = DBVT_LONG;
	m_lVal = lVal;
}

COdbcVariant::COdbcVariant (float fltVal)
{
	m_dwType = DBVT_SINGLE;
	m_fltVal = fltVal;
}

COdbcVariant::COdbcVariant (double dblVal)
{
	m_dwType = DBVT_DOUBLE;
	m_dblVal = dblVal;
}

COdbcVariant::COdbcVariant (const TIMESTAMP_STRUCT & tm)
{
	::ZeroMemory (this, sizeof (COdbcVariant));
	SetType (DBVT_DATE);
	ASSERT (m_pdate);
	::CopyMemory (m_pdate, &tm, sizeof (TIMESTAMP_STRUCT));
}

COdbcVariant::COdbcVariant (const CTime & tm)
{
	::ZeroMemory (this, sizeof (COdbcVariant));
	SetType (DBVT_DATE);
	TIMESTAMP_STRUCT t;
	
	t.year		= tm.GetYear ();
	t.month		= tm.GetMonth ();
	t.day		= tm.GetDay ();
	t.hour		= tm.GetHour ();
	t.minute	= tm.GetMinute ();
	t.second	= tm.GetSecond ();
	t.fraction	= 0;
	ASSERT (m_pdate);
	::CopyMemory (m_pdate, &t, sizeof (TIMESTAMP_STRUCT));
}

COdbcVariant::COdbcVariant (const COleDateTime & tm)
{
	::ZeroMemory (this, sizeof (COdbcVariant));
	SetType (DBVT_DATE);
	TIMESTAMP_STRUCT t;
	
	t.year		= tm.GetYear ();
	t.month		= tm.GetMonth ();
	t.day		= tm.GetDay ();
	t.hour		= tm.GetHour ();
	t.minute	= tm.GetMinute ();
	t.second	= tm.GetSecond ();
	t.fraction	= 0;
	ASSERT (m_pdate);
	::CopyMemory (m_pdate, &t, sizeof (TIMESTAMP_STRUCT));
}

COdbcVariant::COdbcVariant (const CString & str, DWORD dwType)
{
	::ZeroMemory (this, sizeof (COdbcVariant));
	SetType (dwType);
	bool bFromString = FromString (str, dwType);
	ASSERT (bFromString);
}

COdbcVariant::COdbcVariant (const COdbcVariant & rhs)
{
	::ZeroMemory (this, sizeof (COdbcVariant));
	Copy (* this, rhs);
}

COdbcVariant & COdbcVariant::operator = (const COdbcVariant & rhs)
{
	if (this != &rhs) 
		Copy (* this, rhs);

	return * this;
}

COdbcVariant::~COdbcVariant ()
{
	Free ();
}

void COdbcVariant::SetType (DWORD dwType)
{
	if (dwType != m_dwType) {
		Free ();
		m_dwType = dwType;

		switch (m_dwType) {
		case DBVT_DATE:		
			ASSERT (m_pdate == NULL);
			m_pdate = new TIMESTAMP_STRUCT;
			::ZeroMemory (m_pdate, sizeof (TIMESTAMP_STRUCT));
			break;
		case DBVT_STRING:
			ASSERT (m_pstring == NULL);
			m_pstring = new CString ();
			break;
		case DBVT_BINARY:	
			ASSERT (m_pbinary == NULL);
			m_pbinary = new CByteArray;
			break;
		}
	}
}

void COdbcVariant::Copy (COdbcVariant & lhs, const COdbcVariant & rhs)
{
	::Copy (lhs, rhs, true);
}

void COdbcVariant::Free ()
{
	switch (m_dwType) {
	case DBVT_DATE:		
		if (m_pdate) 
			delete m_pdate;
		break;
	case DBVT_STRING:
		if (m_pstring) 
			delete m_pstring;
		break;
	case DBVT_BINARY:	
		if (m_pbinary) 
			delete m_pbinary;
		break;
	}

	::ZeroMemory (this, sizeof (COdbcVariant));
	m_dwType = DBVT_NULL;
}

CString COdbcVariant::FormatUnicode (const CString & str)
{
	if (!FoxjetDatabase::FormatUnicode ())
		return str;

	CString strResult (str);

	strResult.Replace (_T ("\\u"), _T ("<u>"));
	strResult.Replace (_T ("\\U"), _T ("<U>"));

	for (int i = strResult.GetLength () - 1; i >= 0; i--) {
		TCHAR c = strResult [i];

		if (c > 255) {
			CString strEncode;
			//int a = HIBYTE (c);
			//int b = LOBYTE (c);

			strEncode.Format (_T ("\\u%04X"), c);
			//strEncode.Format (_T ("\\C%c%c"), a, b);

			strResult.Delete (i);
			strResult.Insert (i, strEncode);
		}
	}

	return strResult;
}

CString COdbcVariant::UnformatUnicode (const CString & strSrc)
{
	if (!FoxjetDatabase::FormatUnicode ())
		return strSrc;

	CString strResult, str (strSrc);

	for (int i = 0; i < str.GetLength (); i++) {
		if (str [i] == '\\') {
			i++;

			if (i >= str.GetLength ())
				break;

			if ((i + 4) < str.GetLength ()) {
				if (str [i] == 'u' || str [i] == 'U') {
					CString strEncode = str.Mid (i + 1, 4);
					TCHAR cEncode = (TCHAR)_tcstoul (strEncode, NULL, 16);
					strResult += cEncode;
					i += 4;
					continue;
				}
			}

			if ((i + 2) < str.GetLength ()) {
				if (str [i] == 'c' || str [i] == 'C') {
					CString strEncode = str.Mid (i + 1, 2);
					int a = strEncode [0];
					int b = strEncode [1];
					TCHAR cEncode = MAKEWORD (b, a);

					strResult += cEncode;
					i += 2;
					continue;
				}
			}

			strResult += str [i - 1];
		}

		strResult += str [i];
	}

	strResult.Replace (_T ("<u>"), _T ("\\u"));
	strResult.Replace (_T ("<U>"), _T ("\\U"));

	return strResult;
}

CString COdbcVariant::ToString () const
{
	CString strValue;

	switch (m_dwType) {
	case DBVT_BOOL:
		strValue.Format (_T ("%d"), m_boolVal);
		break;
	case DBVT_UCHAR:
		strValue.Format (_T ("%u"), m_chVal);
		break;
	case DBVT_SHORT:
		strValue.Format (_T ("%u"), m_iVal);
		break;
	case DBVT_LONG:
		strValue.Format (_T ("%u"), m_lVal);
		break;
	case DBVT_SINGLE:
		strValue.Format (_T ("%f"), m_fltVal);
		break;
	case DBVT_DOUBLE:
		strValue.Format (_T ("%f"), m_dblVal);
		break;
	case DBVT_DATE: 
		if (m_pdate) {
			strValue.Format (_T ("%d/%d/%d %02d:%02d:%02d"),
				m_pdate->year,
				m_pdate->month,
				m_pdate->day,
				m_pdate->hour,
				m_pdate->minute,
				m_pdate->second);
		}
		break;
	case DBVT_STRING:
		if (m_pstring) {
			strValue = UnformatUnicode (* m_pstring);

			/*
			#ifdef _DEBUG
			if (strValue != * m_pstring) {
				TRACEF (strValue);
				TRACEF (* m_pstring);
			}
			#endif
			*/
		}
		break;
	case DBVT_BINARY:
		if (m_pbinary) {
			for (int i = 0; i < m_pbinary->GetSize (); i++) {
				BYTE ch = m_pbinary->GetAt (i);
				CString str;

				str.Format (_T ("%c"), ch + 0x20);
				strValue += str;
			}
		}
		break;
	}

	return strValue;
}

bool COdbcVariant::FromString(const CString &str, DWORD dwVarType)
{
	bool bResult = false;
	SetType (dwVarType == DBVT_NULL ? DBVT_STRING : dwVarType);

	switch (m_dwType) {
	case DBVT_BOOL:
		m_boolVal = _ttoi (str) ? TRUE : FALSE;
		bResult = true;
		break;
	case DBVT_UCHAR:
		m_chVal = _ttoi (str);
		bResult = true;
		break;
	case DBVT_SHORT:
		m_iVal = _ttoi (str);
		bResult = true;
		break;
	case DBVT_LONG:
		m_lVal = _ttol (str);
		bResult = true;
		break;
	case DBVT_SINGLE:
		m_fltVal = FoxjetDatabase::_ttof (str);
		bResult = true;
		break;
	case DBVT_DOUBLE:
		m_dblVal = _ttod (str);
		bResult = true;
		break;
	case DBVT_DATE: 
		ASSERT (m_pdate);

		try {
			COleDateTime t;

			if (t.ParseDateTime (str)) {
				m_pdate->year		= t.GetYear ();
				m_pdate->month		= t.GetMonth ();
				m_pdate->day		= t.GetDay ();
				m_pdate->hour		= t.GetHour ();
				m_pdate->minute		= t.GetMinute ();
				m_pdate->second		= t.GetSecond ();
				m_pdate->fraction	= 0;
			}

			bResult = true;
		}
		catch (COleException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		break;
	case DBVT_STRING:
		ASSERT (m_pstring);
		* m_pstring = FormatUnicode (str);

		/*
		#ifdef _DEBUG
		if (* m_pstring != str) {
			TRACEF (* m_pstring);
			TRACEF (str);
		}
		#endif
		*/

		bResult = true;
		break;
	case DBVT_BINARY: 
		ASSERT (m_pbinary);
		m_pbinary->RemoveAll ();

		for (int i = 0; i < str.GetLength (); i++)
			m_pbinary->Add (str [i] - 0x20);
		break;
	}

	return bResult;
}

bool COdbcVariant::CopyInPlace (const COdbcVariant & rhs)
{
	if (m_dwType == rhs.m_dwType)
		return ::Copy (* this, rhs, false);
	else {
		CString str = rhs.ToString ();
		COdbcVariant v;

		if (v.FromString (str, m_dwType)) 
			return ::Copy (* this, v, false);
	}
	
	return false;
}

static bool Copy (COdbcVariant & lhs, const COdbcVariant & rhs, bool bRealloc)
{
	bool bResult = false;

	if (bRealloc) {
		lhs.Free ();
		lhs.m_dwType = rhs.m_dwType;

		switch (lhs.m_dwType) {
		case DBVT_DATE: 
			lhs.m_pdate = new TIMESTAMP_STRUCT;
			::ZeroMemory (lhs.m_pdate, sizeof (TIMESTAMP_STRUCT));
			break;
		case DBVT_STRING:
			lhs.m_pstring = new CString;
			break;
		case DBVT_BINARY: 
			lhs.m_pbinary = new CByteArray;
			break;
		}
	}
	else
		ASSERT (lhs.m_dwType == rhs.m_dwType);

	if (lhs.m_dwType == rhs.m_dwType) {
		switch (lhs.m_dwType) {
		case DBVT_BOOL:		
		case DBVT_UCHAR:	
		case DBVT_SHORT:	
		case DBVT_LONG:		
		case DBVT_SINGLE:	
		case DBVT_DOUBLE:	
			::CopyMemory (&lhs, &rhs, sizeof (COdbcVariant));
			bResult = true;
			break;
		case DBVT_DATE:		
			ASSERT (lhs.m_pdate);

			if (rhs.m_pdate)
				::CopyMemory (lhs.m_pdate, rhs.m_pdate, sizeof (TIMESTAMP_STRUCT));
			else
				::ZeroMemory (lhs.m_pdate, sizeof (TIMESTAMP_STRUCT));

			bResult = true;
			break;
		case DBVT_STRING:	
			ASSERT (lhs.m_pstring);

			if (rhs.m_pstring)
				* lhs.m_pstring = * rhs.m_pstring;

			bResult = true;
			break;
		case DBVT_BINARY:	
			ASSERT (lhs.m_pbinary);
			lhs.m_pbinary->RemoveAll ();

			if (rhs.m_pbinary) {
				for (int i = 0; i < rhs.m_pbinary->GetSize (); i++)
					lhs.m_pbinary->Add (rhs.m_pbinary->GetAt (i));
			}

			break;
		}
	}

	return bResult;
}

CTime COdbcVariant::ToTime () const
{
	COleDateTime t = ToOleDateTime ();
	return CTime (
		Constrict (1970, t.GetYear (), 2038), 
		Constrict (1, t.GetMonth (), 12),
		Constrict (1, t.GetMinute (), 31),
		t.GetHour (), t.GetMinute (), t.GetSecond ());
}

COleDateTime COdbcVariant::ToOleDateTime () const
{
	COleDateTime t (100, 0, 0, 0, 0, 0);

	try {
		t.ParseDateTime (ToString ());
	}
	catch (COleException * e)		{ THROW (e); }
	catch (CMemoryException * e)	{ THROW (e); }

	return t;
}