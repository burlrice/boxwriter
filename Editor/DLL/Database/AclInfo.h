#pragma once

#ifndef _ACL_INFO_CLASS_H
#define _ACL_INFO_CLASS_H

#include "DbApi.h"
#include <comdef.h>
#include <string>

namespace FoxjetDatabase
{
	namespace ACL
	{
		typedef enum
		{
			R = 4, // read
			W = 2, // write
			X = 1, // execute
		};

		#pragma pack (1)
		typedef struct
		{
			unsigned __int8 owner : 3;
			unsigned __int8 group : 3;
			unsigned __int8 other : 3;
		} MOD;
		#pragma pop


		DB_API std::wstring to_string(const MOD & mod);
		DB_API MOD GetAccess(LPCTSTR lpszFile);
		DB_API MOD _min(const MOD & lhs, const MOD & rhs);
		DB_API MOD _max(const MOD & lhs, const MOD & rhs);

		using namespace std;

		struct ace_list
		{
			ACE_HEADER*		pAce;
			BOOL			bAllowed;
			ace_list*		next;
		};

		typedef struct
		{
			std::wstring	m_strDomain;
			std::wstring	m_strName;
			MOD				m_mod;
		} PERMISSION;

		class DB_API CACLInfo
		{
		public:
			// construction / destruction

			// constructs a new CACLInfo object
			// bstrPath - path for which ACL info should be queried
			CACLInfo(_bstr_t bstrPath);
			virtual ~CACLInfo(void);

			// Queries NTFS for ACL Info of the file/directory
			HRESULT Query();

			// Outputs ACL info in Human-readable format
			// to supplied output stream
			void Output(ostream& os);

			std::vector <PERMISSION> GetPermissions () const;
			PERMISSION GetPermissions(std::wstring & strName) const;

			bool SetFilePermission(const std::wstring & strName, DWORD dwPermissions);

		private:
			// Private methods
			void ClearAceList();
			HRESULT AddAceToList(ACE_HEADER* pAce);

		private:
			// Member variables
			_bstr_t		m_bstrPath;		// path
			ace_list*	m_sAceList;		// list of Access Control Entries
		};
	};
};

#endif // _ACL_INFO_CLASS_H