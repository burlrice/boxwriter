// BoxRecordset.cpp : implementation file
//

#include "stdafx.h"
#include "Database.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Debug.h"
#include "Serialize.h"
#include "compare.h"
#include "DbImp.h"
#include "Parse.h"

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Panels;

extern HINSTANCE hInstance;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)

FoxjetDatabase::tagPANELSTRUCT::tagPANELSTRUCT ()
:	m_lID			(NOTFOUND),
	m_lLineID		(NOTFOUND),
	m_nNumber		(1),
	m_direction		(LTOR),
	m_orientation	(ROT_0)
{
}

FoxjetDatabase::tagPANELSTRUCT::tagPANELSTRUCT (const tagPANELSTRUCT & rhs)
:	m_lID			(rhs.m_lID),
	m_lLineID		(rhs.m_lLineID),
	m_strName		(rhs.m_strName),
	m_nNumber		(rhs.m_nNumber),
	m_direction		(rhs.m_direction),
	m_orientation	(rhs.m_orientation)
{
}

tagPANELSTRUCT & FoxjetDatabase::tagPANELSTRUCT::operator = (const tagPANELSTRUCT & rhs)
{
	if (this != &rhs) {
		m_lID			= rhs.m_lID;
		m_lLineID		= rhs.m_lLineID;
		m_strName		= rhs.m_strName;
		m_nNumber		= rhs.m_nNumber;
		m_direction		= rhs.m_direction;
		m_orientation	= rhs.m_orientation;
	}

	return * this;
}

FoxjetDatabase::tagPANELSTRUCT::~tagPANELSTRUCT ()
{
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::PANELSTRUCT & lhs, const FoxjetDatabase::PANELSTRUCT & rhs)
{
	return
		lhs.m_lID			== rhs.m_lID &&
		lhs.m_lLineID		== rhs.m_lLineID &&
		lhs.m_strName		== rhs.m_strName &&
		lhs.m_nNumber		== rhs.m_nNumber &&
		lhs.m_direction		== rhs.m_direction &&
		lhs.m_orientation	== rhs.m_orientation;
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::PANELSTRUCT & lhs, const FoxjetDatabase::PANELSTRUCT & rhs)
{
	return !(rhs == lhs);
}

DB_API const COdbcRecordset & FoxjetDatabase::operator >> (const COdbcRecordset & rst, PANELSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		s.m_lID			= (long)rst.GetFieldValue				(m_lpszID);
		s.m_lLineID		= (long)rst.GetFieldValue				(m_lpszLineID);
		s.m_direction	= (DIRECTION)(int)rst.GetFieldValue		(m_lpszDirection);
		s.m_nNumber		= (int)rst.GetFieldValue				(m_lpszNumber);
		s.m_orientation	= (ORIENTATION)(int)rst.GetFieldValue	(m_lpszOrientation);
		s.m_strName		= (CString)rst.GetFieldValue			(m_lpszName);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API COdbcRecordset & FoxjetDatabase::operator << (COdbcRecordset & rst, PANELSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	CStringArray name, value;

	if (s.m_lID == -1) 
		rst.SetEditMode (COdbcRecordset::addnew);

	name.Add (m_lpszLineID);			value.Add (ToString ((long)s.m_lLineID));
	name.Add (m_lpszDirection);			value.Add (ToString ((int)s.m_direction));
	name.Add (m_lpszNumber);			value.Add (ToString ((int)s.m_nNumber));
	name.Add (m_lpszOrientation);		value.Add (ToString ((int)s.m_orientation));
	name.Add (m_lpszName);				value.Add (s.m_strName);

	CString strSQL = FormatSQL (rst, m_lpszTable, name, value, m_lpszID, s.m_lID);
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

	if (s.m_lID == -1)
		s.m_lID = GetMaxID (rst.GetOdbcDatabase (), m_lpszTable, m_lpszID);
/*
	try {
		if (rst.IsAddNew ())
			rst.SetFieldValue (m_lpszID,	(long)GetNextID (rst.GetOdbcDatabase (), m_lpszTable));

		rst.SetFieldValue (m_lpszLineID,		(long)s.m_lLineID);
		rst.SetFieldValue (m_lpszDirection,		(int)s.m_direction);
		rst.SetFieldValue (m_lpszNumber,		(int)s.m_nNumber);
		rst.SetFieldValue (m_lpszOrientation,	(int)s.m_orientation);
		rst.SetFieldValue (m_lpszName,			s.m_strName);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/

	return rst;
}


DB_API bool FoxjetDatabase::GetPanelRecord (COdbcDatabase & database, ULONG lID, PANELSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u"), 
			m_lpszTable, m_lpszID, lID);
		
		rst.Open (str);

		if (!rst.IsEOF ()) {
			rst >> s;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::AddPanelRecord (COdbcDatabase & database, PANELSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = true;

	try {
		COdbcRecordset rst (&database);

		s.m_lID = -1;
		rst << s;

		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s]"), m_lpszTable);
		rst.Open (str, CRecordset::dynaset);
		rst.AddNew ();
		rst << s;
		rst.Update ();
		rst.MoveLast ();
		s.m_lID = (long)rst.GetFieldValue (m_lpszID);
		rst.Close ();
		*/
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::UpdatePanelRecord (COdbcDatabase & database, PANELSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);

		rst << s;

		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u"), 
			m_lpszTable, m_lpszID, s.m_lID);
		rst.Open (str, CRecordset::dynaset);
		rst.Edit ();
		rst << s;
		rst.Update();
		rst.Close ();
		*/

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::DeletePanelRecord (COdbcDatabase & database, ULONG lID)
{
	DBMANAGETHREADSTATE (database);

	CString str;
	bool bResult = false;
	
	str.Format (
		_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
		m_lpszTable, m_lpszID, lID);

	try {
		database.ExecuteSQL (str);
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetPanelRecords (COdbcDatabase & database, ULONG lLineID, CArray <PANELSTRUCT, PANELSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	v.RemoveAll ();

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u"), 
			m_lpszTable, m_lpszLineID, lLineID);
		rst.Open (str);

		while (!rst.IsEOF ()) {
			PANELSTRUCT s;
			rst >> s;
			v.Add (s);
			rst.MoveNext ();
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return v.GetSize () > 0;
}

DB_API bool FoxjetDatabase::GetPanelHeads (COdbcDatabase & database, ULONG lPanelID, CArray <HEADSTRUCT, HEADSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	v.RemoveAll ();

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u"), 
			Heads::m_lpszTable, Heads::m_lpszPanelID, lPanelID);
		rst.Open (str);

		while (!rst.IsEOF ()) {
			HEADSTRUCT s;
			rst >> s;
			v.Add (s);
			rst.MoveNext ();
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return v.GetSize () > 0;
}
