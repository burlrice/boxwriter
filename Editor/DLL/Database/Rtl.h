#pragma once

#include "NtReg.h"

namespace RTL
{
	void Free ();
	bool IsValid ();

	NTSTATUS RtlAllocateHeap (PVOID HeapHandle, ULONG Flags, ULONG Size);
	NTSTATUS RtlFreeHeap (PVOID HeapHandle, ULONG Flags, PVOID MemoryPointer);

	extern LPRTLINITSTRING					RtlInitString;
	extern LPRTLINITANSISTRING				RtlInitAnsiString;
	extern LPRTLINITUNICODESTRING			RtlInitUnicodeString;
	extern LPRTLANSISTRINGTOUNICODESTRING	RtlAnsiStringToUnicodeString;
	extern LPRTLUNICODESTRINGTOANSISTRING	RtlUnicodeStringToAnsiString;
	//extern LPRTLFREESTRING				RtlFreeString;
	extern LPRTLFREEANSISTRING				RtlFreeAnsiString;
	extern LPRTLFREEUNICODESTRING			RtlFreeUnicodeString;
	extern LPRTLCOPYUNICODESTRING			RtlCopyUnicodeString;

	extern LPNTCREATEKEY					NtCreateKey;
	extern LPNTOPENKEY						NtOpenKey;
	extern LPNTDELETEKEY					NtDeleteKey;
	extern LPNTFLUSHKEY						NtFlushKey;
	extern LPNTQUERYKEY						NtQueryKey;
	extern LPNTENUMERATEKEY					NtEnumerateKey;
	extern LPNTSETVALUEKEY					NtSetValueKey;
	extern LPNTSETINFORMATIONKEY			NtSetInformationKey;
	extern LPNTQUERYVALUEKEY				NtQueryValueKey;
	extern LPNTENUMERATEVALUEKEY			NtEnumerateValueKey;
	extern LPNTDELETEVALUEKEY				NtDeleteValueKey;
	extern LPNTQUERYMULTIPLEVALUEKEY		NtQueryMultipleValueKey;
	extern LPNTRENAMEKEY					NtRenameKey;
	extern LPNTSAVEKEY						NtSaveKey;
	extern LPNTRESTOREKEY					NtRestoreKey;
	extern LPNTLOADKEY						NtLoadKey;
	extern LPNTLOADKEY2						NtLoadKey2;
	extern LPNTREPLACEKEY					NtReplaceKey;
	extern LPNTUNLOADKEY					NtUnloadKey;
	extern LPNTCLOSE						NtClose;
	extern LPNTNOTIFYCHANGEKEY				NtNotifyChangeKey;
	extern LPNTOPENTHREAD					NtOpenThread;
	extern LPNTCREATEFILE					NtCreateFile;
	extern LPNTOPENPROCESSTOKEN				NtOpenProcessToken;
	extern LPNTADJUSTPRIVILEGESTOKEN		NtAdjustPrivilegesToken;
	extern LPNTQUERYINFORMATIONTOKEN		NtQueryInformationToken;

	extern HINSTANCE						ghlib;
};
