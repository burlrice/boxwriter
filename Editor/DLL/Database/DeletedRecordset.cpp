// DeletedRecordset.cpp : implementation file
//

#include "stdafx.h"
#include "Database.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Debug.h"
#include "Serialize.h"
#include "compare.h"
#include "Parse.h"

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Deleted;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)

FoxjetDatabase::tagDELETEDSTRUCT::tagDELETEDSTRUCT ()
{
}

FoxjetDatabase::tagDELETEDSTRUCT::tagDELETEDSTRUCT (const tagDELETEDSTRUCT & rhs)
:	m_lID			(rhs.m_lID),
	m_lLineID		(rhs.m_lLineID),
	m_strTaskName	(rhs.m_strTaskName),
	m_strData		(rhs.m_strData)
{
}

tagDELETEDSTRUCT & FoxjetDatabase::tagDELETEDSTRUCT::operator = (const tagDELETEDSTRUCT & rhs)
{
	if (this != &rhs) {
		m_lID			= rhs.m_lID;
		m_lLineID		= rhs.m_lLineID;
		m_strTaskName	= rhs.m_strTaskName;
		m_strData		= rhs.m_strData;
	}

	return * this;
}

FoxjetDatabase::tagDELETEDSTRUCT::~tagDELETEDSTRUCT ()
{
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::DELETEDSTRUCT & lhs, const FoxjetDatabase::DELETEDSTRUCT & rhs)
{
	return
		lhs.m_lID			== rhs.m_lID && 
		lhs.m_lLineID		== rhs.m_lLineID && 
		lhs.m_strTaskName	== rhs.m_strTaskName &&
		lhs.m_strData		== rhs.m_strData;
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::DELETEDSTRUCT & lhs, const FoxjetDatabase::DELETEDSTRUCT & rhs)
{
	return !(rhs == lhs);
}

DB_API const FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator >> (const FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::DELETEDSTRUCT & ls)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		ls.m_lID			= (long)rst.GetFieldValue (m_lpszID);
		ls.m_lLineID		= (long)rst.GetFieldValue (m_lpszLineID);
		ls.m_strTaskName	= (CString)rst.GetFieldValue (m_lpszTaskName);
		ls.m_strData		= (CString)rst.GetFieldValue (m_lpszData);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator << (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::DELETEDSTRUCT & ls)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	CStringArray name, value;

	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	if (ls.m_lID == -1) 
		rst.SetEditMode (COdbcRecordset::addnew);

	name.Add (m_lpszLineID);			value.Add (ToString ((long)ls.m_lLineID));
	name.Add (m_lpszTaskName);			value.Add (ls.m_strTaskName);
	name.Add (m_lpszData);				value.Add (ls.m_strData);

	CString strSQL = FormatSQL (rst, m_lpszTable, name, value, m_lpszID, ls.m_lID);
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

	if (ls.m_lID == -1)
		ls.m_lID = GetMaxID (rst.GetOdbcDatabase (), m_lpszTable, m_lpszID);
/*
	try {
		if (rst.IsAddNew ())
			rst.SetFieldValue (m_lpszID,	(long)GetNextID (rst.GetOdbcDatabase (), m_lpszTable));

		rst.SetFieldValue (m_lpszLineID,		(long)ls.m_lLineID);
		rst.SetFieldValue (m_lpszTaskName,		ls.m_strTaskName);
		rst.SetFieldValue (m_lpszData,			ls.m_strData);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/
	return rst;
}

DB_API bool FoxjetDatabase::AddDeletedRecord (COdbcDatabase & database, DELETEDSTRUCT & s) 
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);
		CString str;

		s.m_lID = -1;
		rst << s;
		/* TODO: rem
		str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);
		rst.Open (str, CRecordset::dynaset);
		
		rst.AddNew ();
		rst << s;
		rst.Update ();

		rst.MoveLast ();
		s.m_lID = (long)rst.GetFieldValue (m_lpszID);
		
		rst.Close ();
		*/

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetDeletedRecords (COdbcDatabase & database, ULONG lLineID, CArray <DELETEDSTRUCT, DELETEDSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%d;"), 
			m_lpszTable, m_lpszLineID, lLineID);
		rst.Open (str, CRecordset::snapshot);

		while (!rst.IsEOF ()) {
			DELETEDSTRUCT s;

			rst >> s;
			v.Add (s);

			rst.MoveNext ();
		}

		rst.Close ();

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetDeletedRecords (COdbcDatabase & database, CArray <DELETEDSTRUCT, DELETEDSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);
		rst.Open (str, CRecordset::snapshot);

		while (!rst.IsEOF ()) {
			DELETEDSTRUCT s;

			rst >> s;
			v.Add (s);

			rst.MoveNext ();
		}

		rst.Close ();

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::DeleteDeletedRecord (COdbcDatabase & database, ULONG lID)
{
	DBMANAGETHREADSTATE (database);

	CString str;
	bool bResult = false;
	
	if (lID == ALL)
		str.Format (_T ("DELETE * FROM [%s];"), m_lpszTable);
	else
		str.Format (
			_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, lID);

	try {
		database.ExecuteSQL (str);
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

