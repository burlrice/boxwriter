#ifndef __DATABASE_H__
#define __DATABASE_H__

#pragma once

#include "DbApi.h"

#include <string>
#include <afx.h>
#include <comdef.h>
#include <afxtempl.h>
#include <afxdb.h>
#include <vector>
#include "OdbcDatabase.h"
#include "AppVer.h"
#include "DbTypeDefs.h"
#include "DevGuids.h"
#include "MsgBox.h"
#include "CopyArray.h"

#include <lmcons.h>
#include <Lmapibuf.h>
#include <Lmuse.h>


#define DECLARE_CONSTRUCTORS(name) \
		name(); \
		name (const name & rhs); \
		name & operator = (const name & rhs); \
		virtual ~name (); 

#ifndef PI
	#define PI			3.1415926535897932384626433832795
#endif

#define DEG2RAD( n )	n * ((PI) / 180.00)			// degrees to radians
#define RAD2DEG( n )	((n * 180) / (PI))			// radian to degrees

#define MDB_DATE_STRFTIME _T ("%m/%d/%Y %I:%M:%S %p")

namespace FoxjetDatabase 
{
	typedef struct DB_API tagBOXSTRUCT
	{
		DECLARE_CONSTRUCTORS (tagBOXSTRUCT)

		ULONG	m_lID;
		CString m_strName;
		CString m_strDesc;
		ULONG	m_lWidth;
		ULONG	m_lLength;
		ULONG	m_lHeight;
	} BOXSTRUCT;


	typedef struct DB_API tagIMAGESTRUCT
	{
		DECLARE_CONSTRUCTORS (tagIMAGESTRUCT)

		ULONG			m_lBoxID;
		UINT			m_nPanel;
		CString			m_strImage;
		IMAGEATTRIBUTES	m_lAttributes;
	} IMAGESTRUCT;

	typedef struct DB_API tagBOXLINKSTRUCT
	{
		DECLARE_CONSTRUCTORS (tagBOXLINKSTRUCT)

		ULONG m_lLineID;
		ULONG m_lBoxID;
	} BOXLINKSTRUCT;

	typedef struct DB_API tagLINESTRUCT
	{
		DECLARE_CONSTRUCTORS (tagLINESTRUCT)

		ULONG			m_lID;
		ULONG			m_lPrinterID;
		CString			m_strName;
		CString			m_strDesc;
		COleDateTime	m_tmMaintenance;
		CString			m_strNoRead;
		BOOL			m_bResetScanInfo;
		INT				m_nMaxConsecutiveNoReads;
		INT				m_nBufferOffset;
		INT				m_nSignificantChars;
		CString			m_strAuxBoxIp;
		BOOL			m_bAuxBoxEnabled;
		CString			m_strSerialParams1;
		CString			m_strSerialParams2;
		CString			m_strSerialParams3;
		INT				m_nAMS32_A1;
		INT				m_nAMS32_A2;
		INT				m_nAMS32_A3;
		INT				m_nAMS32_A4;
		INT				m_nAMS32_A5;
		INT				m_nAMS32_A6;
		INT				m_nAMS32_A7;
		INT				m_nAMS256_A1;
		INT				m_nAMS256_A2;
		INT				m_nAMS256_A3;
		INT				m_nAMS256_A4;
		INT				m_nAMS256_A5;
		INT				m_nAMS256_A6;
		INT				m_nAMS256_A7;
		double			m_dAMS_Interval;
	} LINESTRUCT;

	typedef struct DB_API tagPANELSTRUCT
	{
		DECLARE_CONSTRUCTORS (tagPANELSTRUCT)

		ULONG		m_lID;
		ULONG		m_lLineID;
		CString		m_strName;
		UINT		m_nNumber;
		DIRECTION	m_direction;
		ORIENTATION m_orientation;
	} PANELSTRUCT;

	typedef struct DB_API tagMESSAGESTRUCT
	{
		DECLARE_CONSTRUCTORS (tagMESSAGESTRUCT)

		ULONG	m_lID;
		ULONG	m_lHeadID;
		ULONG	m_lTaskID;
		CString m_strName;
		CString m_strDesc;
		CString m_strData;
		ULONG	m_lHeight;
		bool	m_bIsUpdated;
	} MESSAGESTRUCT;

	typedef struct DB_API tagTASKSTRUCT 
	{
		DECLARE_CONSTRUCTORS (tagTASKSTRUCT)

		ULONG									m_lID;
		ULONG									m_lLineID; 
		ULONG									m_lBoxID; 
		CString									m_strName;
		CString									m_strDesc;
		CString									m_strDownload;
		ULONG									m_lTransformation;
		CString									m_strNotes;
		CArray <MESSAGESTRUCT, MESSAGESTRUCT &> m_vMsgs; 

		void Free ();
		
		// sets the id's for this task and all the entris in m_vLinks
		void SetID (ULONG lID); 

		CSize GetPanelSize (const CArray <FoxjetDatabase::PANELSTRUCT, FoxjetDatabase::PANELSTRUCT &> & v, ULONG lPanelID, const BOXSTRUCT & box) const;

	} TASKSTRUCT;

	class DB_API COdbcException : public CException 
	{
		DECLARE_DYNAMIC (COdbcException);

	public:
		COdbcException (const CString & str = _T (""));

		const CString m_strError;
	};

	typedef struct DB_API tagVALVESTRUCT
	{
		DECLARE_CONSTRUCTORS (tagVALVESTRUCT)

		ULONG			m_lID;
		ULONG			m_lCardID;				// head ID
		ULONG			m_lPanelID;
		ULONG			m_lPhotocellDelay;
		ULONG			m_lHeight;
		BOOL			m_bEnabled;
		BOOL			m_bReversed;
		VALVETYPE		m_type;		
		CString			m_strName;
		int				m_nIndex;
		int				m_nLinked;
		BOOL			m_bUpsidedown;

		CString			GetName () const;
		int				Height () const;
		bool			IsLinked () const { return m_nLinked > 0; }

	} VALVESTRUCT;

	typedef CArray <FoxjetDatabase::VALVESTRUCT, FoxjetDatabase::VALVESTRUCT &> CValveArray;

	typedef struct DB_API tagHEADSTRUCT
	{
		DECLARE_CONSTRUCTORS (tagHEADSTRUCT)

		ULONG		m_lID;
		ULONG		m_lPanelID;
		CString		m_strName;
		CString		m_strUID;
		ULONG		m_lRelativeHeight;
		UINT		m_nChannels;
		UINT		m_nHorzRes;			// This is really the Physical Encoder DPI CGH 07-31-02
		ULONG		m_lPhotocellDelay;
		int			m_nEncoder;
		int			m_nDirection;
		BOOL		m_bEnabled;
		BOOL		m_bInverted;
		double		m_dHeadAngle;
		int			m_nPhotocell;
		int			m_nSharePhoto;
		int			m_nShareEnc;
		HEADTYPE	m_nHeadType;		
		BOOL		m_bRemoteHead;
		BOOL		m_bMasterHead;
		UINT		m_nIntTachSpeed;	// Internal Tach (Encoder) Speed.	// CGH 20020731
		double		m_dNozzleSpan;		// Span distance between outside and out side of the nozzles. // CGH 20020731
		double		m_dPhotocellRate;
		CString		m_strSerialParams;
		int			m_nEncoderDivisor;
		BOOL		m_bDoublePulse;
		BOOL		m_bUpsidedown;
		ULONG		m_lLinkedTo;
		bool		m_bDigiNET;

		int			Height () const;
		int			GetChannels () const;
		double		GetRes () const;
		double		GetMaxRes () const;
		double		GetSpan () const;
		void		WriteProfile (HKEY hKey, const CString & strSection, ULONG lLineID);
		bool		IsUSB () const;

		CArray <VALVESTRUCT, VALVESTRUCT &> m_vValve;

	} HEADSTRUCT;

	typedef struct DB_API tagOPTIONSSTRUCT 
	{
		DECLARE_CONSTRUCTORS (tagOPTIONSSTRUCT)

		ULONG		m_lID;
		CString		m_strOptionDesc;
	} OPTIONSSTRUCT;

	typedef struct DB_API tagOPTIONSLINKSTRUCT 
	{
		DECLARE_CONSTRUCTORS (tagOPTIONSLINKSTRUCT)

		long		m_lOptionsID;
		long		m_lGroupID;
	} OPTIONSLINKSTRUCT;

	typedef struct DB_API tagUSERSTRUCT 
	{
		DECLARE_CONSTRUCTORS (tagUSERSTRUCT)

		ULONG		m_lID;
		CString		m_strFirstName;
		CString		m_strLastName;
		CString		m_strUserName;
		CString		m_strPassW;
		ULONG		m_lSecurityGroupID;
	} USERSTRUCT;

	typedef struct DB_API tagSECURITYGROUPSTRUCT 
	{
		DECLARE_CONSTRUCTORS (tagSECURITYGROUPSTRUCT)

		ULONG		m_lID;
		CString		m_strName;
	} SECURITYGROUPSTRUCT;

	typedef struct DB_API tagSHIFTSTRUCT
	{
		DECLARE_CONSTRUCTORS (tagSHIFTSTRUCT)

		ULONG			m_lID;
		ULONG			m_lLineID;
		CString			m_strName;
		COleDateTime	m_dtTime;
		CString			m_strCode;
	} SHIFTSTRUCT;

	typedef struct DB_API tagREPORTSTRUCT
	{
		DECLARE_CONSTRUCTORS (tagREPORTSTRUCT)

		COleDateTime	m_dtTime;
		CString			m_strAction;
		CString			m_strUsername;
		CString			m_strCounts;
		CString			m_strLine;
		CString			m_strTaskName;
		LONG			m_lType;	
		LONG			m_lPrinterID;
		std::unique_ptr<std::map<std::wstring, std::wstring> > m_pmapUser;
	} REPORTSTRUCT;

	typedef struct DB_API tagBCSCANSTRUCT
	{
		DECLARE_CONSTRUCTORS (tagBCSCANSTRUCT)

		ULONG			m_lID;
		COleDateTime	m_dtTime;
		ULONG			m_LineID;
		CString			m_strScanner;
		CString			m_strTaskname;
		CString			m_strBarcode;
		LONG			m_lTotalScans;
		LONG			m_lGoodScans;
	} BCSCANSTRUCT;

	typedef struct DB_API tagBOXUSAGESTRUCT
	{
		DECLARE_CONSTRUCTORS (tagBOXUSAGESTRUCT)

		TASKSTRUCT	m_task;
		LINESTRUCT	m_line;
	} BOXUSAGESTRUCT;

	typedef struct DB_API tagDELETEDSTRUCT
	{
		DECLARE_CONSTRUCTORS (tagDELETEDSTRUCT)

		ULONG		m_lID;
		ULONG		m_lLineID;
		CString		m_strTaskName;
		CString		m_strData;
	} DELETEDSTRUCT;

	typedef struct DB_API tagSETTINGSSTRUCT
	{
		DECLARE_CONSTRUCTORS (tagSETTINGSSTRUCT)

		ULONG		m_lLineID;
		CString		m_strKey;
		CString		m_strData;
	} SETTINGSSTRUCT;

	typedef struct DB_API tagPRINTERSTRUCT
	{
		DECLARE_CONSTRUCTORS (tagPRINTERSTRUCT)

		ULONG		m_lID;
		CString		m_strName;
		CString		m_strAddress;
		bool		m_bMaster;
		PRINTERTYPE m_type;
	} PRINTERSTRUCT;

	class DB_API COrientation
	{
	public:
		COrientation (UINT nPanel, ORIENTATION orient);
		COrientation (const COrientation & rhs);
		COrientation (const PANELSTRUCT & rhs);
		COrientation & operator = (const COrientation & rhs);
		virtual ~COrientation ();

		UINT			m_nPanel;
		ORIENTATION		m_orient;
	};

	class DB_API CUseInfo2
	{
	public:
		CUseInfo2 ();
		CUseInfo2 (USE_INFO_2 * );

		CString m_strLocal;
		CString m_strRemote;
		CString m_strPassword;
		DWORD	m_dwStatus;
		DWORD	m_dwAsg_type;
		DWORD	m_dwRefcount;
		DWORD	m_dwUsecount;
		CString m_strUsername;
		CString m_strDomainname;
	};

	long DB_API GetID (const FoxjetDatabase::LINESTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::BOXSTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::TASKSTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::HEADSTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::PANELSTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::OPTIONSSTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::USERSTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::SECURITYGROUPSTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::SHIFTSTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::DELETEDSTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::BCSCANSTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::BOXLINKSTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::OPTIONSLINKSTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::REPORTSTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::SETTINGSSTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::IMAGESTRUCT & s);
	long DB_API GetID (const FoxjetDatabase::PRINTERSTRUCT & s); 

	void DB_API _SetID (FoxjetDatabase::LINESTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::BOXSTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::TASKSTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::HEADSTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::PANELSTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::OPTIONSSTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::USERSTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::SECURITYGROUPSTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::SHIFTSTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::DELETEDSTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::BCSCANSTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::BOXLINKSTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::OPTIONSLINKSTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::REPORTSTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::SETTINGSSTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::IMAGESTRUCT & s, ULONG lID);
	void DB_API _SetID (FoxjetDatabase::PRINTERSTRUCT & s, ULONG lID); 

	CString DB_API GetIDField (const CString & strTable);
	CString DB_API GetIDField (const FoxjetDatabase::LINESTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::BOXSTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::TASKSTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::HEADSTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::PANELSTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::OPTIONSSTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::USERSTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::SECURITYGROUPSTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::SHIFTSTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::DELETEDSTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::BCSCANSTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::BOXLINKSTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::OPTIONSLINKSTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::REPORTSTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::SETTINGSSTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::IMAGESTRUCT & s);
	CString DB_API GetIDField (const FoxjetDatabase::PRINTERSTRUCT & s); 

	DB_API void EnumMappedDrives (CArray <CUseInfo2, CUseInfo2 &> & v);

	DB_API bool IsValidLicense (COdbcDatabase & database, const CString & strLicense);
	DB_API bool DeleteDSN (const CString & strDSN, const CString & strDriver = SQL_MDB_DRIVER);
	DB_API bool OpenDatabase (COdbcDatabase & database, const CString & strDSN, const CString & strFilename);
	DB_API bool BeginTrans (COdbcDatabase & database, LPCTSTR lpszFile, ULONG lLine);
	DB_API bool CommitTrans (COdbcDatabase & database, LPCTSTR lpszFile, ULONG lLine);
	DB_API bool RollbackTrans (COdbcDatabase & database, LPCTSTR lpszFile, ULONG lLine);
	DB_API void UpgradeDatabase (COdbcDatabase & database, const CString & strDSN);
	DB_API bool IsAccdb ();
	DB_API bool IsAccdb (const CString & strFilename);
	DB_API void TraceSQLInstallerError (LPCTSTR lpszFile, ULONG lLine);
	DB_API bool IsValve ();

	DB_API UINT GetMaxHeads (COdbcDatabase & database, const CString & strLicense);
	DB_API UINT GetHeadRecordCount (COdbcDatabase & database);
	DB_API bool GetHeadRecord (COdbcDatabase & database, ULONG lID, HEADSTRUCT & hs);
	DB_API bool GetHeadRecords (COdbcDatabase & database, CArray <HEADSTRUCT, HEADSTRUCT &> & v);
	DB_API bool GetHeadUnownedRecords (COdbcDatabase & database, CArray <HEADSTRUCT, HEADSTRUCT &> & v);
	DB_API ULONG GetHeadID (COdbcDatabase & database, const CString & strUID, ULONG lPrinterID);
	DB_API ULONG GetHeadID (COdbcDatabase & database, ULONG lLineID, const CString & strHead);
	DB_API bool AddHeadRecord (COdbcDatabase & database, HEADSTRUCT & hs);
	DB_API bool UpdateHeadRecord (COdbcDatabase & database, HEADSTRUCT & hs);
	DB_API bool DeleteHeadRecord (COdbcDatabase & database, ULONG lID);
	DB_API bool GetFirstHeadRecord (COdbcDatabase & database, HEADSTRUCT & head);
	DB_API const HEADSTRUCT & GetDefaultHead ();
	DB_API int GetHeadChannels (HEADTYPE type);
	inline long double CalcInkUsage (HEADTYPE head, INKTYPE ink, ULONG lPixels); 
	DB_API long double GetInkLowVolume (HEADTYPE head, INKTYPE ink); 

	DB_API int GetMaxFontSize (COdbcDatabase & database, int nHead);
	DB_API int GetMinFontSize (COdbcDatabase & database, int nHead);

	DB_API UINT GetMaxLines (COdbcDatabase & database, const CString & strLicense);
	DB_API UINT GetLineRecordCount (COdbcDatabase & database);
	DB_API ULONG GetLineID (COdbcDatabase & database, const CString & str, ULONG lPrinterID);
	DB_API bool GetLineRecord (COdbcDatabase & database, ULONG lID, LINESTRUCT & cs);
	DB_API bool AddLineRecord (COdbcDatabase & database, LINESTRUCT & cs);
	DB_API bool UpdateLineRecord (COdbcDatabase & database, LINESTRUCT & cs);
	DB_API bool DeleteLineRecord (COdbcDatabase & database, ULONG lID);
	DB_API bool GetLineRecords (COdbcDatabase & database, CArray <LINESTRUCT, LINESTRUCT &> & v);
	DB_API bool GetLineHeads (COdbcDatabase & database, ULONG lID, CArray <HEADSTRUCT, HEADSTRUCT &> & v);
	DB_API bool GetPrinterLines (COdbcDatabase & database, ULONG lPrinterID, CArray <LINESTRUCT, LINESTRUCT &> & v);
	DB_API ULONG GetPrinterID (COdbcDatabase & database);
	DB_API bool GetFirstLineRecord (COdbcDatabase & database, LINESTRUCT & line, ULONG lPrinterID);
	DB_API bool GetPrinterHeads (COdbcDatabase & database, ULONG lPrinterID, CArray <HEADSTRUCT, HEADSTRUCT &> & v); 
	DB_API void SetPrinterID (ULONG lID); 
	DB_API ULONG GetMasterPrinterID (COdbcDatabase & database); // sw0867
	DB_API void Remap (COdbcDatabase & db, TASKSTRUCT & task, ULONG lPrinterID, ULONG lMasterID); // sw0867
	DB_API void Unmap (COdbcDatabase & db, TASKSTRUCT & task, ULONG lPrinterID, ULONG lMasterID); // sw0867
	DB_API ULONG GetHeadMapping (COdbcDatabase & db, const HEADSTRUCT & head, ULONG lPrinterID); // sw0867
	DB_API void LoadLineMapping (COdbcDatabase & db);
	DB_API ULONG GetMasterLineID(ULONG lLineID);

	DB_API ULONG GetMaxTasks ();
	DB_API UINT GetTaskRecordCount (COdbcDatabase & database, ULONG lLineID = ALL);
	DB_API ULONG GetTaskID (COdbcDatabase & database, const CString & strTask, const CString & strLine, ULONG lPrinterID);
	DB_API bool GetTaskRecord (COdbcDatabase & database, const CString & strTask, ULONG lLineID, TASKSTRUCT & task);
	DB_API bool GetTaskRecord (COdbcDatabase & database, const CString & strTask, const CString & strLine, ULONG lPrinterID, TASKSTRUCT & task);
	DB_API bool GetTaskRecord (COdbcDatabase & database, ULONG lID, TASKSTRUCT & task);
	DB_API bool AddTaskRecord (COdbcDatabase & database, TASKSTRUCT & task);
		// throws COdbcException
	DB_API bool UpdateTaskRecord (COdbcDatabase & database, TASKSTRUCT & task);
	DB_API bool UpdateMessages (COdbcDatabase & database, TASKSTRUCT & s);
		// throws CDBException, CMemoryException
	DB_API bool DeleteTaskRecord (COdbcDatabase & database, ULONG lID);
	DB_API bool GetTaskRecords (COdbcDatabase & database, ULONG lLineID, CArray <TASKSTRUCT, TASKSTRUCT &> & v);
	DB_API bool GetTaskRecords (COdbcDatabase & database, ULONG lLineID, CArray <TASKSTRUCT *, TASKSTRUCT *> & v); // faster, but must call Free
	DB_API void Free (CArray <TASKSTRUCT *, TASKSTRUCT *> & v);

	DB_API UINT GetMessageRecordCount (COdbcDatabase & database, ULONG lHeadID = ALL);
	DB_API ULONG GetMessageID (COdbcDatabase & database, const CString & strMsg, const CString & strHead, ULONG lPrinterID);
	DB_API bool GetMessageRecord (COdbcDatabase & database, const CString & strMessage, const CString & strHead, MESSAGESTRUCT & msg);
	DB_API bool GetMessageRecord (COdbcDatabase & database, const CString & strMessage, ULONG lHeadID, MESSAGESTRUCT & msg);
	DB_API bool GetMessageRecord (COdbcDatabase & database, ULONG lID, MESSAGESTRUCT & msg);
	DB_API bool AddMessageRecord (COdbcDatabase & database, MESSAGESTRUCT & msg);
	DB_API bool UpdateMessageRecord (COdbcDatabase & database, MESSAGESTRUCT & msg);
	DB_API bool DeleteMessageRecord (COdbcDatabase & database, ULONG lID);
	DB_API bool GetMessageRecords (COdbcDatabase & database, ULONG lHeadID, CArray <MESSAGESTRUCT, MESSAGESTRUCT &> & v);
	DB_API bool GetMessageRecords (COdbcDatabase & database, ULONG lHeadID, bool bIsUpdated, CArray <MESSAGESTRUCT, MESSAGESTRUCT &> & v);
	DB_API bool GetMessageRecords (COdbcDatabase & database, const CString & strHead, CArray <MESSAGESTRUCT, MESSAGESTRUCT &> & v);
	DB_API bool SetMessageRecordModified (COdbcDatabase & database, ULONG lID, bool bModified);

	DB_API UINT GetBoxRecordCount (COdbcDatabase & database, ULONG lLineID);
	DB_API bool GetBoxRecord (COdbcDatabase & database, ULONG lID, BOXSTRUCT & s);
	DB_API ULONG GetBoxID (COdbcDatabase & database, const CString & strName);
	DB_API bool AddBoxRecord (COdbcDatabase & database, BOXSTRUCT & s, ULONG lLineID = ALL);
	DB_API bool UpdateBoxRecord (COdbcDatabase & database, BOXSTRUCT & s);
	DB_API bool DeleteBoxRecord (COdbcDatabase & database, ULONG lID);
	DB_API bool GetBoxRecords (COdbcDatabase & database, ULONG lLineID, CArray <BOXSTRUCT, BOXSTRUCT &> & v);
	DB_API bool GetNetworkedBoxRecords (COdbcDatabase & database, ULONG lLineID, CArray <BOXSTRUCT, BOXSTRUCT &> & v);
	DB_API bool GetFirstBoxRecord (COdbcDatabase & database, ULONG lLineID, BOXSTRUCT & box);
	DB_API COrientation MapPanel (UINT nPanel, const COrientation & o);

	DB_API bool GetPanelRecord (COdbcDatabase & database, ULONG lID, PANELSTRUCT & s);
	DB_API bool AddPanelRecord (COdbcDatabase & database, PANELSTRUCT & s);
	DB_API bool UpdatePanelRecord (COdbcDatabase & database, PANELSTRUCT & s);
	DB_API bool DeletePanelRecord (COdbcDatabase & database, ULONG lID);
	DB_API bool GetPanelRecords (COdbcDatabase & database, ULONG lLineID, CArray <PANELSTRUCT, PANELSTRUCT &> & v);
	DB_API bool GetPanelHeads (COdbcDatabase & database, ULONG lPanelID, CArray <HEADSTRUCT, HEADSTRUCT &> & v);
//	DB_API UINT GetBoxRecordCount (COdbcDatabase & database, ULONG lLineID);
//	DB_API bool GetFirstBoxRecord (COdbcDatabase & database, BOXSTRUCT & line);

	DB_API bool GetImageRecord (COdbcDatabase & database, ULONG lBoxID, UINT nPanel, IMAGESTRUCT & s);
	DB_API bool AddImageRecord (COdbcDatabase & database, IMAGESTRUCT & s);
	DB_API bool UpdateImageRecord (COdbcDatabase & database, IMAGESTRUCT & s);
	DB_API bool DeleteImageRecord (COdbcDatabase & database, ULONG lBoxID, UINT nPanel = ALL);
	DB_API bool GetImageRecords (COdbcDatabase & database, ULONG lBoxID, CArray <IMAGESTRUCT, IMAGESTRUCT &> & v);

	DB_API bool GetOptionsRecord (COdbcDatabase & database, ULONG lID, OPTIONSSTRUCT & s);
	DB_API bool GetOptionsRecords (COdbcDatabase & database, CArray <OPTIONSSTRUCT, OPTIONSSTRUCT &> & v);

	DB_API bool GetUserRecord (COdbcDatabase & database, ULONG lID, USERSTRUCT & s);
	DB_API bool GetUserRecord (COdbcDatabase & database, CString sUserName, USERSTRUCT & s);
	DB_API bool AddUserRecord (COdbcDatabase & database, USERSTRUCT & s);
	DB_API bool UpdateUserRecord (COdbcDatabase & database, USERSTRUCT & s);
	DB_API bool DeleteUserRecord (COdbcDatabase & database, ULONG lID);
	DB_API bool GetUserRecords (COdbcDatabase & database, CArray <USERSTRUCT, USERSTRUCT &> & v);

	DB_API bool GetGroupsRecord (COdbcDatabase & database, ULONG lID, SECURITYGROUPSTRUCT & s);
	DB_API bool AddGroupsRecord (COdbcDatabase & database, SECURITYGROUPSTRUCT & s);
	DB_API bool UpdateGroupsRecord (COdbcDatabase & database, SECURITYGROUPSTRUCT & s);
	DB_API bool DeleteGroupsRecord (COdbcDatabase & database, ULONG lID);
	DB_API bool GetGroupsRecords (COdbcDatabase & database, CArray <SECURITYGROUPSTRUCT, SECURITYGROUPSTRUCT &> & v);
	DB_API bool SetOptionsLinks (COdbcDatabase & database, ULONG lID, CArray <OPTIONSLINKSTRUCT, OPTIONSLINKSTRUCT &> & v);
	DB_API bool GetOptionsLinks (COdbcDatabase & database, ULONG lID, CArray <OPTIONSLINKSTRUCT, OPTIONSLINKSTRUCT &> & v);

	DB_API bool GetShiftRecord (COdbcDatabase & database, ULONG lID, SHIFTSTRUCT &s);
	DB_API bool AddShiftRecord (COdbcDatabase & database, SHIFTSTRUCT &s);
	DB_API bool UpdateShiftRecord (COdbcDatabase & database, SHIFTSTRUCT &s);
	DB_API bool DeleteShiftRecord (COdbcDatabase & database, ULONG lID);
	DB_API bool GetShiftRecords (COdbcDatabase & database, ULONG lLineID, CArray <SHIFTSTRUCT, SHIFTSTRUCT> & v);

	DB_API bool AddReportRecord (COdbcDatabase & database, REPORTTYPE type, REPORTSTRUCT & s, ULONG lPrinterID);
	DB_API ULONG AddDefaultPrinter (COdbcDatabase & database);
	DB_API bool AddPrinterReportRecord (COdbcDatabase & database, REPORTTYPE type, REPORTSTRUCT & s, ULONG lPrinterID);
	DB_API bool GetReportRecords (COdbcDatabase & database, REPORTTYPE type, CArray <REPORTSTRUCT, REPORTSTRUCT &> & v, ULONG lPrinterID);
	DB_API bool GetPrinterReportRecords (COdbcDatabase & database, CString strLine, CArray <REPORTSTRUCT, REPORTSTRUCT &> & v, ULONG lPrinterID);
	DB_API bool GetReportRecords (COdbcDatabase & database, REPORTTYPE type, const COleDateTime & dtAfter, 
		const COleDateTime & dtBefore, ULONG lLineID, CArray <REPORTSTRUCT, REPORTSTRUCT &> & v, ULONG lPrinterID);
	DB_API bool ClearReportRecords (COdbcDatabase & database, ULONG lPrinterID);
	DB_API bool ClearPrinterReportRecords (COdbcDatabase & database, CString strLine, ULONG lPrinterID);
	DB_API bool GetBoxUsage (COdbcDatabase & database, ULONG lBoxID, ULONG lLineID, CArray <BOXUSAGESTRUCT, BOXUSAGESTRUCT &> & v);
	DB_API void EnablePrintReportPurge (bool bEnable, int nDays);
	DB_API CString FormatReportUserColumnName(CString str);
	DB_API void GetReportUserColumns(COdbcDatabase& database, CStringArray& v);

	DB_API bool AddScanRecord (COdbcDatabase & database, BCSCANSTRUCT & s);
	DB_API bool UpdateScanRecord (COdbcDatabase & database, BCSCANSTRUCT & s);
	DB_API bool GetScanRecords (COdbcDatabase & database, ULONG lLineID, CArray <BCSCANSTRUCT, BCSCANSTRUCT &> & v);
	DB_API bool ClearScanRecords (COdbcDatabase & database, ULONG lLineID);

	DB_API bool AddDeletedRecord (COdbcDatabase & database, DELETEDSTRUCT & s);
	DB_API bool GetDeletedRecords (COdbcDatabase & database, ULONG lLineID, CArray <DELETEDSTRUCT, DELETEDSTRUCT &> & v);
	DB_API bool GetDeletedRecords (COdbcDatabase & database, CArray <DELETEDSTRUCT, DELETEDSTRUCT &> & v);
	DB_API bool DeleteDeletedRecord (COdbcDatabase & database, ULONG lID = ALL);

	DB_API bool AddSettingsRecord (COdbcDatabase & database, SETTINGSSTRUCT & s);
	DB_API bool GetSettingsRecord (COdbcDatabase & database, ULONG lLineID, const CString & strKey, SETTINGSSTRUCT & s);
	DB_API bool GetSettingsRecords (COdbcDatabase & database, ULONG lLineID, CArray <SETTINGSSTRUCT, SETTINGSSTRUCT &> & v);
	DB_API bool UpdateSettingsRecord (COdbcDatabase & database, SETTINGSSTRUCT & s);
	DB_API bool DeleteSettingsRecord (COdbcDatabase & database, ULONG lLineID, const CString & strKey);

	DB_API bool GetPrinterRecord (COdbcDatabase & database, ULONG lID, PRINTERSTRUCT & cs);
	DB_API bool AddPrinterRecord (COdbcDatabase & database, PRINTERSTRUCT & cs);
	DB_API bool UpdatePrinterRecord (COdbcDatabase & database, PRINTERSTRUCT & cs);
	DB_API bool DeletePrinterRecord (COdbcDatabase & database, ULONG lID);
	DB_API bool GetPrinterRecords (COdbcDatabase & database, CArray <PRINTERSTRUCT, PRINTERSTRUCT &> & v);

	DB_API CString HandleException (_com_error & e, LPCTSTR pszFile, ULONG nLine, bool bMsgBox);
	DB_API CString HandleException (CException * p, LPCTSTR pszFile, ULONG nLine, bool bMsgBox);
	DB_API CString FormatException (CException * p, LPCTSTR pszFile = NULL, ULONG nLine = 0);
	DB_API void SetExceptionCount (const CString & str, int nCount = 0);

	DB_API CString GetHomeDir ();
	DB_API int GetFiles (const CString & strDir, const CString & strFile, CStringArray & v);
	DB_API CString MakeRelativePath (const CString & strPath);
	DB_API CString MakeAbsolutePath (const CString & strPath);
	DB_API BOOL EnablePrivilege (LPCTSTR lpName);

	DB_API void DeleteAllRecords (COdbcDatabase & database);

	DB_API bool IsValveHead (const FoxjetDatabase::HEADSTRUCT & head);
	DB_API bool IsValveHead (FoxjetDatabase::HEADTYPE type);

	DB_API bool IsHpHead (const FoxjetDatabase::HEADSTRUCT & head);
	DB_API bool IsHpHead (FoxjetDatabase::HEADTYPE type);

	DB_API int GetValveChannels (FoxjetDatabase::VALVETYPE type);
	DB_API int GetValveHeight (FoxjetDatabase::VALVETYPE type);

	DB_API int DivideAndRound (double a, double b);
	DB_API int Round (double a, int nBoundary = 1);
	DB_API CString CreateTempFilename (const CString & strDir, const CString & strPrefix, const CString & strSuffix);
	DB_API bool Compact (const CString & strFile, const CString & strDSN, bool bForce = false);
	DB_API CString GetDAOVersion ();
	DB_API void GetBackupPath (CString & strPath, CString & strTitle, CString strDSN = _T (""), bool bNext = false);
	DB_API void OnDefineBackupPath (CWnd * pParent);
	DB_API void DoBackup (COdbcDatabase & db);
	DB_API void CreateDir (CString strFullPath);

	DB_API bool IsDriverInstalled (const GUID & guid);
	DB_API void SetDriver (const GUID & guid);

	DB_API CString GetComputerName ();
	DB_API CString GetIpAddress ();
	DB_API std::vector <std::wstring> GetMacAddresses ();

	DB_API CString Encrypt (const CString & str);
	DB_API bool ComparePasswords (const CString & strDB, const CString & strInput);

	DB_API void NotifySystemParamChange (COdbcDatabase & db);
	
	void DB_API SetUsername (const CString & str);
	CString DB_API GetUsername ();
	bool DB_API LoginPermits (FoxjetDatabase::COdbcDatabase & db, const CString & strUser, UINT nID);

	DB_API bool IsHighResDisplay ();
	DB_API void SetHighResDisplay (bool bValue);
	DB_API bool IsDebugDisplayAttached ();
	DB_API int GetListCtrlItemHeight (); 
	DB_API int GetTreeCtrlItemHeight (); 
	DB_API bool IsStartMenuOpen ();
	
	DB_API void SetFormatUnicode (bool bFormat);
	DB_API bool FormatUnicode ();

	class DB_API CInternalFormatting
	{
	public:
		CInternalFormatting (bool bFormat = false)
		{
			m_bFormat = FoxjetDatabase::FormatUnicode ();
			FoxjetDatabase::SetFormatUnicode (bFormat);		
		}

		virtual ~CInternalFormatting()
		{
			FoxjetDatabase::SetFormatUnicode (m_bFormat);		
		}

	private:
		bool m_bFormat;
	};

	typedef struct
	{
		UINT m_nDialogID;
		UINT m_nCtrlID;
	}
	DIALOG_SEARCH_STRUCT;

	DB_API CString LoadString(UINT nID, LPCTSTR lpszModule);
	DB_API FoxjetCommon::CCopyArray <UINT, UINT> GetStringResourceIDs (const CString & strModule);
	DB_API FoxjetCommon::CCopyArray <UINT, UINT> GetDialogResourceIDs (const CString & strModule);
	DB_API UINT GetIdFromStringResource (const CString & strFind, const CString & strModule, bool bIgnorePunctuation = false);
	DB_API DIALOG_SEARCH_STRUCT GetIdFromDialogResource (const CString & strFind, const CString & strModule);
	DB_API CString GetStringFromDialogResource (const DIALOG_SEARCH_STRUCT & find, const CString & strModule);

	DB_API CString GetLangExt ();
	DB_API CString GetLangDir (const CString & strDir = GetLangExt ());
	DB_API LCID GetLcid (const CString & strLcid);
	DB_API CString GetLcid (LCID lcid);
	DB_API CString GetLanguage (const CString & strLcid);

	DB_API INT_PTR CALLBACK HiddenDialogProc (HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
	DB_API CString RemoveWhitespace (const CString & str);
	DB_API CString RemovePunctuation (CString str);

	DB_API bool IsRunning (LPCTSTR lpsz);
	DB_API bool IsWin7 ();
	DB_API bool IsPrintMonitor ();
	DB_API bool IsTimingEnabled ();

	DB_API ULONG GetMaxID (FoxjetDatabase::COdbcDatabase & db, const CString & strTable, const CString & strField = _T ("ID"));
	DB_API ULONG GetNextID (FoxjetDatabase::COdbcDatabase & db, const CString & strTable, const CString & strField = _T ("ID"));
	DB_API CString Format (const CString & str);
	DB_API CString FormatSQL (COdbcRecordset & rst, const CString & strTable, const CStringArray & name, const CStringArray & value, const CString & strID, ULONG lID);
	DB_API bool IsSqlString (DWORD dwSqlType);
	DB_API HWND FindEditor ();
	DB_API HWND FindControl ();

	class DB_API CInstance
	{
	public:
		CInstance ();		
		virtual ~CInstance ();

		void Create (LPCTSTR lpsz);
		
		static bool Elevate ();

	protected:
		HANDLE m_h;
	};

	CString DB_API ToString (const FoxjetDatabase::BOXSTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::BOXSTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::IMAGESTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::IMAGESTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::BOXLINKSTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::BOXLINKSTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::LINESTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::LINESTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::PANELSTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::PANELSTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::MESSAGESTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::MESSAGESTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::TASKSTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::TASKSTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::HEADSTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::HEADSTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::OPTIONSSTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::OPTIONSSTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::OPTIONSLINKSTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::OPTIONSLINKSTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::USERSTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::USERSTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::SECURITYGROUPSTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::SECURITYGROUPSTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::SHIFTSTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::SHIFTSTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::REPORTSTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::REPORTSTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::BCSCANSTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::BCSCANSTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::DELETEDSTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::DELETEDSTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::SETTINGSSTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::SETTINGSSTRUCT & s);

	CString DB_API ToString (const FoxjetDatabase::PRINTERSTRUCT & s); 
	bool DB_API FromString (const CString & str, FoxjetDatabase::PRINTERSTRUCT & s);
	
	DB_API float _ttof (const TCHAR * string);

	DB_API void SetNetworked (bool b);
	DB_API bool IsNetworked ();
	DB_API void SetGojo (bool b);
	DB_API bool IsGojo ();
	DB_API bool IsSunnyvale ();
	DB_API bool IsSelectEffectivityDate ();
	DB_API void SetSelectEffectivityDate (bool bSet, const CTime & tm = CTime::GetCurrentTime ());
	DB_API CTime GetEffectivityDate ();
	DB_API ULONG GetSynonymPhcAddr (ULONG lAddr);
	DB_API ULONG GetSynonymUsbAddr (ULONG lAddr);
	DB_API ULONG CompareHeadConfigs (COdbcDatabase & db, ULONG lMasterPrinterID, ULONG lPrinterID);
	DB_API ULONG CompareHeadConfigs (COdbcDatabase & db, CArray <HEADSTRUCT, HEADSTRUCT &> & vMaster, ULONG lPrinterID);
	DB_API bool IsControl (bool bOnCmdLine = true);
	DB_API bool IsConfig (bool bOnCmdLine = true);
	DB_API bool IsMatrix ();
	DB_API DWORD GetFileSize (const CString & strFile);
	DB_API void UpdateNetworkedHeads (COdbcDatabase & db, const HEADSTRUCT & head);
	DB_API ULONG GetFirstLineID (COdbcDatabase & db, ULONG lPrinterID = -1);
	DB_API void Restore (CWnd * pWnd, const CString & strCmdLine);


	DB_API CString Format (const LPBYTE lpBuff, ULONG lLen);
	DB_API CString Format (const TCHAR * lpBuff, ULONG lLen);

	DB_API int Unformat (const CString & str, LPBYTE lpBuffer);
	DB_API int Unformat (const CString & str, TCHAR * lpBuffer);

	DB_API int EncodeBase64 (unsigned char * pbInput, int nLen, unsigned char * pbOutput, int nMaxSize);
	DB_API int DecodeBase64 (unsigned char * pbInput, unsigned char * pbOutput, int nMaxSize);

	DB_API bool IsLeapYear (int nYear);
	DB_API int GetDaysInMonth (int nMonth, int nYear);

	DB_API bool IsUltraResEnabled ();
	DB_API void EnableUltraRes (bool b);

	inline /* DB_API */ void IsInstallerInvoked ()
	{
		CString str = ::GetCommandLine ();
		
		str.MakeLower ();

		if (str.Find (_T ("/version")) != -1) {
			long nResult = 0;
			
			nResult += (__MAJOR__			* 100000000);
			nResult += (__MINOR__			* 1000000);
			nResult += (__CUSTOM__			* 100);
			nResult += (__CUSTOM_MINOR__	* 10);
			nResult += (__BUILD__			* 1);

			exit (nResult);
		}
	}
}; //namespace FoxjetDatabase

inline void Repaint (CWnd & wnd) { wnd.Invalidate (FALSE); wnd.RedrawWindow (NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW); /* wnd.RedrawWindow (); */ /* wnd.UpdateWindow (); */ }
inline void Repaint (CWnd * p) { if (p) Repaint (* p); }
#define REPAINT(wnd) Repaint ((wnd));


#define ISVALVE()						FoxjetDatabase::IsValve () //FoxjetDatabase::IsDriverInstalled (GUID_INTERFACE_IVPHC)

#define BEGIN_TRANS(db)					FoxjetDatabase::BeginTrans (db, _T (__FILE__), __LINE__)
#define COMMIT_TRANS(db)				FoxjetDatabase::CommitTrans (db, _T (__FILE__), __LINE__)
#define ROLLBACK_TRANS(db)				FoxjetDatabase::RollbackTrans (db, _T (__FILE__), __LINE__)
#define HANDLEEXCEPTION(e)				FoxjetDatabase::HandleException (e, _T (__FILE__), __LINE__, true)
#define HANDLEEXCEPTION_TRACEONLY(e)	FoxjetDatabase::HandleException (e, _T (__FILE__), __LINE__, false)
#define TRACE_SQLERROR()				FoxjetDatabase::TraceSQLInstallerError (_T (__FILE__), __LINE__)

#ifndef __DATABASE_INLINE__
	#define __DATABASE_INLINE__
	#include "Database.inl"
#endif //__DATABASE_INLINE__

#endif //__DATABASE_H__