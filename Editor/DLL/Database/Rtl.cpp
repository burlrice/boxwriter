#include "StdAfx.h"
#include "Rtl.h"

using namespace RTL;

LPRTLINITSTRING					RTL::RtlInitString					= NULL;
LPRTLINITANSISTRING				RTL::RtlInitAnsiString				= NULL;
LPRTLINITUNICODESTRING			RTL::RtlInitUnicodeString			= NULL;
LPRTLANSISTRINGTOUNICODESTRING	RTL::RtlAnsiStringToUnicodeString	= NULL;
LPRTLUNICODESTRINGTOANSISTRING	RTL::RtlUnicodeStringToAnsiString	= NULL;
//LPRTLFREESTRING				RTL::RtlFreeString					= NULL;
LPRTLFREEANSISTRING				RTL::RtlFreeAnsiString				= NULL;
LPRTLFREEUNICODESTRING			RTL::RtlFreeUnicodeString			= NULL;
LPRTLCOPYUNICODESTRING			RTL::RtlCopyUnicodeString			= NULL;

LPNTCREATEKEY					RTL::NtCreateKey					= NULL;
LPNTOPENKEY						RTL::NtOpenKey						= NULL;
LPNTDELETEKEY					RTL::NtDeleteKey					= NULL;
LPNTFLUSHKEY					RTL::NtFlushKey						= NULL;
LPNTQUERYKEY					RTL::NtQueryKey						= NULL;
LPNTENUMERATEKEY				RTL::NtEnumerateKey					= NULL;
LPNTSETVALUEKEY					RTL::NtSetValueKey					= NULL;
LPNTSETINFORMATIONKEY			RTL::NtSetInformationKey			= NULL;
LPNTQUERYVALUEKEY				RTL::NtQueryValueKey				= NULL;
LPNTENUMERATEVALUEKEY			RTL::NtEnumerateValueKey			= NULL;
LPNTDELETEVALUEKEY				RTL::NtDeleteValueKey				= NULL;
LPNTQUERYMULTIPLEVALUEKEY		RTL::NtQueryMultipleValueKey		= NULL;
LPNTRENAMEKEY					RTL::NtRenameKey					= NULL;
LPNTSAVEKEY						RTL::NtSaveKey						= NULL;
LPNTRESTOREKEY					RTL::NtRestoreKey					= NULL;
LPNTLOADKEY						RTL::NtLoadKey						= NULL;
LPNTLOADKEY2					RTL::NtLoadKey2						= NULL;
LPNTREPLACEKEY					RTL::NtReplaceKey					= NULL;
LPNTUNLOADKEY					RTL::NtUnloadKey					= NULL;
LPNTCLOSE						RTL::NtClose						= NULL;
LPNTNOTIFYCHANGEKEY				RTL::NtNotifyChangeKey				= NULL;
LPNTOPENTHREAD					RTL::NtOpenThread					= NULL;
LPNTCREATEFILE					RTL::NtCreateFile					= NULL;
LPNTOPENPROCESSTOKEN			RTL::NtOpenProcessToken				= NULL;
LPNTADJUSTPRIVILEGESTOKEN		RTL::NtAdjustPrivilegesToken		= NULL;
LPNTQUERYINFORMATIONTOKEN		RTL::NtQueryInformationToken		= NULL;

HINSTANCE						RTL::ghlib							= NULL;


void RTL::Free ()
{
	if (::ghlib) {
		::FreeLibrary (::ghlib);
		::ghlib = NULL;
	}
}

bool RTL::IsValid ()
{ 
	return ghlib != NULL ? true : false; 
}


NTSTATUS RTL::RtlAllocateHeap (PVOID HeapHandle, ULONG Flags, ULONG Size)
{
	BYTE * p = new BYTE [Size];

	memset (p, 0, Size);

	return (NTSTATUS)p;
}

NTSTATUS RTL::RtlFreeHeap (PVOID HeapHandle, ULONG Flags, PVOID MemoryPointer)
{
	delete [] (BYTE *)MemoryPointer;
	return 0;
}


