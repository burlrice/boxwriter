#include "stdafx.h"
#include "AnsiString.h"

#include <shlwapi.h>
#include <setupapi.h>
#include <afxsock.h>

#include "Database.h"
#include "Registry.h"
#include "Resource.h"
#include "Debug.h"
#include "DllVer.h"
#include "Parse.h"
#include "ODBCInst.h"
#include "AppVer.h"
#include "OdbcTable.h"
#include "FileExt.h"
#include "fj_defines.h"
#include "CompactDlg.h"
#include "BackupDlg.h"
#include "OdbcRecordset.h"
#include "CompactDlg.h"
#include "..\..\Control\Host.h"
#include "FieldDefs.h"
#include "OdbcTables.h"
#include "Parse.h"
#include "CopyArray.h"
#include "Serialize.h"
#include "StackWalker.h"
#include "Registry.h"
#include "DiagnosticSingleLock.h"

bool CompactDatabase (const CString & strSrc, const CString & strDest);

using namespace FoxjetDatabase;
using namespace FoxjetCommon;

HINSTANCE hInstance = NULL;
static const LPCTSTR lpszRegSection		= _T ("Software\\FoxJet\\Database\\");
static const LPCTSTR lpszDbSize			= _T ("DB size");

static const GUID guidNull	= { 0, 0, 0, { 0, 0, 0, 0, 0, 0, 0, 0 } };
static GUID guidDriver		= ::guidNull;
static const CString gstrLocal = _T ("_LOCAL");


static CCompactDlg * pdlg = NULL;
static CStringArray vDisplay;
static DIAGNOSTIC_CRITICAL_SECTION (csDatabaseCache);
static CArray <COdbcDatabase *, COdbcDatabase *> vDbCache;
static CMapStringToString gmapDsn;
static CMapStringToPtr vRstCache;		
static CMapStringToPtr vFieldIndex;		

static CString GetDsn (const CString & strDSN)
{
	CString str = strDSN;

	::gmapDsn.Lookup (strDSN, str);

	return str;
}

long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::LINESTRUCT & s)										{ return s.m_lID;	}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::BOXSTRUCT & s)											{ return s.m_lID;	}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::TASKSTRUCT & s)										{ return s.m_lID;	}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::HEADSTRUCT & s)										{ return s.m_lID;	}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::PANELSTRUCT & s)										{ return s.m_lID;	}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::OPTIONSSTRUCT & s)										{ return s.m_lID;	}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::USERSTRUCT & s)										{ return s.m_lID;	}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::SECURITYGROUPSTRUCT & s)								{ return s.m_lID;	}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::SHIFTSTRUCT & s)										{ return s.m_lID;	}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::DELETEDSTRUCT & s)										{ return s.m_lID;	}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::BCSCANSTRUCT & s)										{ return s.m_lID;	}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::BOXLINKSTRUCT & s)										{ return 0;			}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::OPTIONSLINKSTRUCT & s)									{ return 0;			}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::REPORTSTRUCT & s)										{ return 0;			}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::SETTINGSSTRUCT & s)									{ return 0;			}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::IMAGESTRUCT & s)										{ return 0;			}
long DB_API FoxjetDatabase::GetID (const FoxjetDatabase::PRINTERSTRUCT & s)										{ return s.m_lID;	} 
																																						
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::LINESTRUCT & s, ULONG lID)									{ s.m_lID = lID; }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::BOXSTRUCT & s, ULONG lID)									{ s.m_lID = lID; }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::TASKSTRUCT & s, ULONG lID)									{ s.m_lID = lID; }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::HEADSTRUCT & s, ULONG lID)									{ s.m_lID = lID; }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::PANELSTRUCT & s, ULONG lID)									{ s.m_lID = lID; }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::OPTIONSSTRUCT & s, ULONG lID)								{ s.m_lID = lID; }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::USERSTRUCT & s, ULONG lID)									{ s.m_lID = lID; }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::SECURITYGROUPSTRUCT & s, ULONG lID)							{ s.m_lID = lID; }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::SHIFTSTRUCT & s, ULONG lID)									{ s.m_lID = lID; }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::DELETEDSTRUCT & s, ULONG lID)								{ s.m_lID = lID; }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::BCSCANSTRUCT & s, ULONG lID)								{ s.m_lID = lID; }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::BOXLINKSTRUCT & s, ULONG lID)								{ }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::OPTIONSLINKSTRUCT & s, ULONG lID)							{ }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::REPORTSTRUCT & s, ULONG lID)								{ }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::SETTINGSSTRUCT & s, ULONG lID)								{ }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::IMAGESTRUCT & s, ULONG lID)									{ }
void DB_API FoxjetDatabase::_SetID (FoxjetDatabase::PRINTERSTRUCT & s, ULONG lID)								{ s.m_lID = lID; }

CString DB_API FoxjetDatabase::GetIDField (const CString & strTable)
{
			if (!strTable.CompareNoCase (Lines::m_lpszTable))			return Lines::m_lpszID;
	else	if (!strTable.CompareNoCase (Boxes::m_lpszTable))			return Boxes::m_lpszID;
	else	if (!strTable.CompareNoCase (Tasks::m_lpszTable))			return Tasks::m_lpszID;
	else	if (!strTable.CompareNoCase (Messages::m_lpszTable))		return Messages::m_lpszID;
	else	if (!strTable.CompareNoCase (Heads::m_lpszTable))			return Heads::m_lpszID;
	else	if (!strTable.CompareNoCase (Panels::m_lpszTable))			return Panels::m_lpszID;
	else	if (!strTable.CompareNoCase (Options::m_lpszTable))			return Options::m_lpszID;
	else	if (!strTable.CompareNoCase (Users::m_lpszTable))			return Users::m_lpszID;
	else	if (!strTable.CompareNoCase (SecurityGroups::m_lpszTable))	return SecurityGroups::m_lpszID;
	else	if (!strTable.CompareNoCase (Shifts::m_lpszTable))			return Shifts::m_lpszID;
	else	if (!strTable.CompareNoCase (Deleted::m_lpszTable))			return Deleted::m_lpszID;
	else	if (!strTable.CompareNoCase (BarcodeScans::m_lpszTable))	return BarcodeScans::m_lpszID;
	else	if (!strTable.CompareNoCase (Printers::m_lpszTable))		return Printers::m_lpszID;

	return _T ("");
}

CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::LINESTRUCT & s)								{ return GetIDField (Lines::m_lpszTable);			}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::BOXSTRUCT & s)									{ return GetIDField (Boxes::m_lpszTable);			}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::TASKSTRUCT & s)								{ return GetIDField (Tasks::m_lpszTable);			}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::HEADSTRUCT & s)								{ return GetIDField (Heads::m_lpszTable);			}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::PANELSTRUCT & s)								{ return GetIDField (Panels::m_lpszTable);			}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::OPTIONSSTRUCT & s)								{ return GetIDField (Options::m_lpszTable);			}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::USERSTRUCT & s)								{ return GetIDField (Users::m_lpszTable);			}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::SECURITYGROUPSTRUCT & s)						{ return GetIDField (SecurityGroups::m_lpszTable);	}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::SHIFTSTRUCT & s)								{ return GetIDField (Shifts::m_lpszTable);			}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::DELETEDSTRUCT & s)								{ return GetIDField (Deleted::m_lpszTable);			}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::BCSCANSTRUCT & s)								{ return GetIDField (BarcodeScans::m_lpszTable);	}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::BOXLINKSTRUCT & s)								{ return GetIDField (BoxToLine::m_lpszTable);		}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::OPTIONSLINKSTRUCT & s)							{ return GetIDField (Options::m_lpszTable);			}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::REPORTSTRUCT & s)								{ return GetIDField (Reports::m_lpszTable);			}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::SETTINGSSTRUCT & s)							{ return GetIDField (Settings::m_lpszTable);		}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::IMAGESTRUCT & s)								{ return GetIDField (Images::m_lpszTable);			}
CString DB_API FoxjetDatabase::GetIDField (const FoxjetDatabase::PRINTERSTRUCT & s)								{ return GetIDField (Printers::m_lpszTable);		}

////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
inline void TraceThreadState (LPCTSTR lpszFile, ULONG lLine)
{
	static CStringArray v;
	DWORD dw = ::GetCurrentThreadId ();
	CString str;
	TCHAR sz [MAX_PATH] = { 0 };

	::GetModuleFileName (NULL, sz, ARRAYSIZE (sz));
	CString strFile = sz;
	int nIndex = strFile.ReverseFind ('\\');

	if (nIndex != -1)
		strFile = strFile.Mid (nIndex + 1);

	str.Format (_T ("csDatabaseCache:0x%p"), &::csDatabaseCache);
	if (Find (v, str) == -1) v.Add (str);

	str.Format (_T ("vDbCache:0x%p"), &::vDbCache);
	if (Find (v, str) == -1) v.Add (str);

	str.Format (_T ("vRstCache:0x%p"), &::vRstCache);
	if (Find (v, str) == -1) v.Add (str);

	str.Format (_T ("%d"), ::GetCurrentThreadId ());
	if (Find (v, str) == -1) v.Add (str);

	str = _T (",[") + ToString (::vDbCache.GetSize ()) + _T (",") + ToString (::vRstCache.GetSize ()) + _T ("]");

	TraceRelease (implode (v) + str, lpszFile, lLine);
	int n = 0;
}
#define TRACETHREADSTATE() TraceThreadState (_T (__FILE__), __LINE__)

//////////////////////////////////////////////////////////////////////

static inline bool IsHeader (const CString & strSrc, const CString & strToken)
{
	CString str [] =
	{
		_T ("{") + CString (strToken) + _T (","),
		CString (strToken) + _T (","),
	};

	return !_tcsnicmp (str [0], strSrc, str [0].GetLength ()) || !_tcsnicmp (str [1], strSrc, str [1].GetLength ());
}

static class CExceptionHandler
{
public:
	CExceptionHandler ();
	virtual ~CExceptionHandler ();

	CString HandleException (CException * p, LPCTSTR pszFile, ULONG nLine, bool bMsgBox);
	CString HandleException (_com_error & e, LPCTSTR pszFile, ULONG nLine, bool bMsgBox);

private:
	CRITICAL_SECTION m_csException;

} exceptionHandler;

CExceptionHandler::CExceptionHandler ()
{
	::InitializeCriticalSection (&(::exceptionHandler.m_csException));
}

CExceptionHandler::~CExceptionHandler ()
{
	::DeleteCriticalSection (&(::exceptionHandler.m_csException));
}

DB_API CString FoxjetDatabase::HandleException (CException * p, LPCTSTR pszFile, ULONG nLine, bool bMsgBox)
{
	return ::exceptionHandler.HandleException (p, pszFile, nLine, bMsgBox);
}

DB_API CString FoxjetDatabase::HandleException (_com_error & e, LPCTSTR pszFile, ULONG nLine, bool bMsgBox)
{
	return ::exceptionHandler.HandleException (e, pszFile, nLine, bMsgBox);
}

static CMapStringToString mapExceptions;

DB_API void FoxjetDatabase::SetExceptionCount (const CString & str, int nCount)
{
	CString strSet (str);

	if (str.GetLength ())
		::mapExceptions.SetAt (strSet, ToString (nCount));
	else
		::mapExceptions.RemoveAll ();
}

DB_API CString FoxjetDatabase::FormatException (CException * p, LPCTSTR pszFile, ULONG nLine)
{
	CString str;
	CString strClass = _T ("[Unknown]");

	if (p) {
		ASSERT (p);
		const UINT nSize = 512;
		TCHAR szMsg [nSize];

		strClass = a2w (p->GetRuntimeClass ()->m_lpszClassName);
		p->GetErrorMessage (szMsg, nSize, NULL);

		#if 1 //def _DEBUG
		CRuntimeClass * pClass = p->GetRuntimeClass ();
		CString strMsg (szMsg);

		if (p->IsKindOf (RUNTIME_CLASS (CDaoException))) {
			CDaoException * pDao = (CDaoException *)p;

			if (pDao->m_pErrorInfo) {
				CString str;

				str.Format (
					_T ("\n")
					_T ("m_lErrorCode = %u (0x%08X)\n")
					_T ("m_strSource = %s\n")
					_T ("m_strDescription = %s\n")
					_T ("m_strHelpFile = %s\n")
					_T ("m_lHelpContext = %u (0x%08X)"),
					pDao->m_pErrorInfo->m_lErrorCode,	pDao->m_pErrorInfo->m_lErrorCode,
					pDao->m_pErrorInfo->m_strSource,
					pDao->m_pErrorInfo->m_strDescription,
					pDao->m_pErrorInfo->m_strHelpFile,
					pDao->m_pErrorInfo->m_lHelpContext, pDao->m_pErrorInfo->m_lHelpContext);
				strMsg += str;
			}
		}
		else if (p->IsKindOf (RUNTIME_CLASS (CDBException))) {
			CDBException * pDB = (CDBException *)p;
			CString str, strRetCode = "Unknown";

			struct EXECEPTIONSTRUCT
			{
				EXECEPTIONSTRUCT (RETCODE nCode, LPCTSTR lpsz) 
					: m_nCode (nCode), m_str (lpsz) 
				{ 
				}

				RETCODE m_nCode;
				CString m_str;
			} static const e [] = 
			{
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_API_CONFORMANCE,		_T ("The driver for a CDatabase::OpenEx or CDatabase::Open call does not conform to required ODBC API Conformance level 1 (SQL_OAC_LEVEL1).")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_CONNECT_FAIL,			_T ("Connection to the data source failed. You passed a NULL CDatabase pointer to your recordset constructor and the subsequent attempt to create a connection based on GetDefaultConnect failed. ")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_DATA_TRUNCATED,			_T ("You requested more data than you have provided storage for. For information on increasing the provided data storage for CString or CByteArray data types, see the nMaxLength argument for RFX_Text and RFX_Binary under �Macros and Globals.�")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_DYNASET_NOT_SUPPORTED,	_T ("A call to CRecordset::Open requesting a dynaset failed. Dynasets are not supported by the driver.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_EMPTY_COLUMN_LIST,		_T ("You attempted to open a table (or what you gave could not be identified as a procedure call or SELECT statement) but there are no columns identified in record field exchange (RFX) function calls in your DoFieldExchange override.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_FIELD_SCHEMA_MISMATCH,	_T ("The type of an RFX function in your DoFieldExchange override is not compatible with the column data type in the recordset.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_ILLEGAL_MODE,			_T ("You called CRecordset::Update without previously calling CRecordset::AddNew or CRecordset::Edit. ")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_LOCK_MODE_NOT_SUPPORTED,_T ("Your request to lock records for update could not be fulfilled because your ODBC driver does not support locking.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_MULTIPLE_ROWS_AFFECTED,	_T ("You called CRecordset::Update or Delete for a table with no unique key and changed multiple records.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_NO_CURRENT_RECORD,		_T ("You attempted to edit or delete a previously deleted record. You must scroll to a new current record after a deletion.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_NO_POSITIONED_UPDATES,	_T ("Your request for a dynaset could not be fulfilled because your ODBC driver does not support positioned updates.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_NO_ROWS_AFFECTED,		_T ("You called CRecordset::Update or Delete, but when the operation began the record could no longer be found.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_ODBC_LOAD_FAILED,		_T ("An attempt to load the ODBC.DLL failed; Windows could not find or could not load this DLL. This error is fatal.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_ODBC_V2_REQUIRED,		_T ("Your request for a dynaset could not be fulfilled because a Level 2-compliant ODBC driver is required.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_RECORDSET_FORWARD_ONLY,	_T ("An attempt to scroll did not succeed because the data source does not support backward scrolling.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_SNAPSHOT_NOT_SUPPORTED,	_T ("A call to CRecordset::Open requesting a snapshot failed. Snapshots are not supported by the driver. (This should only occur when the ODBC cursor library � ODBCCURS.DLL � is not present.)")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_SQL_CONFORMANCE,		_T ("The driver for a CDatabase::OpenEx or CDatabase::Open call does not conform to the required ODBC SQL Conformance level of \"Minimum\" (SQL_OSC_MINIMUM).")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_SQL_NO_TOTAL,			_T ("The ODBC driver was unable to specify the total size of a CLongBinary data value. The operation probably failed because a global memory block could not be preallocated.")),
				EXECEPTIONSTRUCT (AFX_SQL_ERROR_RECORDSET_READONLY,		_T ("You attempted to update a read-only recordset, or the data source is read-only. No update operations can be performed with the recordset or the CDatabase object it is associated with.")),
				EXECEPTIONSTRUCT (SQL_ERROR,							_T ("Function failed. The error message returned by ::SQLError is stored in the m_strError data member.")),
				EXECEPTIONSTRUCT (SQL_INVALID_HANDLE,					_T ("Function failed due to an invalid environment handle, connection handle, or statement handle. This indicates a programming error. No additional information is available from ::SQLError.")),
				EXECEPTIONSTRUCT (-1,									NULL),
			};

			for (int i = 0; e [i].m_nCode != -1; i++) {
				if (pDB->m_nRetCode == e [i].m_nCode) {
					strRetCode = e [i].m_str;
					break;
				}
			}

			str.Format (
				_T ("\n")
				_T ("m_nRetCode = %u (0x%p) [%s]\n")
				_T ("m_strError = %s\n")
				_T ("m_strStateNativeOrigin = %s"),
				pDB->m_nRetCode, pDB->m_nRetCode, strRetCode,
				pDB->m_strError,
				pDB->m_strStateNativeOrigin);
			strMsg += str;
		}

		str.Format (
			_T ("%s\n\n")
			_T ("exception class name: %s\n")
			_T ("%s(%u):"), 
			(LPCTSTR)strMsg, 
			a2w (pClass->m_lpszClassName), 
			pszFile, nLine);
		#else
		str = szMsg;
		#endif //_DEBUG
	}
	else 
		str = _T ("p == NULL");

	return str;
}

static CString GetStackTrace (LPCTSTR lpszFile, ULONG lLine)
{
	StackWalker s;
	s.ShowCallstack (lpszFile, lLine);
	CString str = s.GetOutput ();
	CString strFile;

	#ifdef _DEBUG
		strFile.Format (_T ("%s(%d): "), lpszFile, lLine);
	#else
	{
		CString s = lpszFile;
		int n = s.ReverseFind ('\\');
		
		if (n != -1)
			s = s.Mid (n + 1);

		strFile.Format (_T ("%s(%d): "), s, lLine);
	}
	#endif

	str.Replace (_T ("\n"), _T ("\n") + strFile);
	str.Trim ();

	return _T ("Call stack: ") + CString (str.IsEmpty () ? _T ("") : str);
}

CString CExceptionHandler::HandleException (_com_error & e, LPCTSTR pszFile, ULONG nLine, bool bMsgBox) 
{
	CString str, strClass = _T ("_com_error");
	::EnterCriticalSection (&(::exceptionHandler.m_csException));

	str.Format (_T ("%s\n\n%s(%d)"), (LPCTSTR)e.Description (), pszFile, nLine);
	str += _T ("\n") + GetStackTrace (pszFile, nLine);

	if (FILE * fp = _tfopen (_T ("Exceptions.log"), _T ("a"))) {
		CString strLog;

		strLog.Format (_T ("[%s] %s\n%s\n[---------------------]\n\n\n"), 
			CTime::GetCurrentTime ().Format (_T ("%c")), 
			strClass,
			str);
		fwrite (w2a (strLog), strLog.GetLength (), 1, fp);
		fclose (fp);
	}

	//#ifdef _DEBUG
	{
		CString strDebug;
		strDebug.Format (_T ("%s(%u): [BW] %s\n"), pszFile, nLine, str);
		::OutputDebugString (strDebug);
	}
	//#endif

	if (bMsgBox)
		MsgBox (str, MB_ICONSTOP);

	::LeaveCriticalSection (&(::exceptionHandler.m_csException));
	return str;
}

CString CExceptionHandler::HandleException (CException * p, LPCTSTR pszFile, ULONG nLine, bool bMsgBox) 
{
	::EnterCriticalSection (&(::exceptionHandler.m_csException));

	CString strClass = _T ("[Unknown]");
	CString str = FormatException (p, pszFile, nLine);
	CString strMsg;

	{
		CString strCount;
		const UINT nSize = 512;
		TCHAR szMsg [nSize];

		p->GetErrorMessage (szMsg, nSize, NULL);
		strMsg = szMsg;

		::mapExceptions.Lookup (strMsg, strCount);
		::mapExceptions.SetAt (strMsg, ToString (_ttoi (strCount) + 1));

		#ifdef _DEBUG
		{
			CString strTmp;
			
			BOOL b = ::mapExceptions.Lookup (strMsg, strTmp);
			TRACEF (CString (b ? _T ("Lookup: ") : _T ("Lookup failed: ")) + strTmp + _T (": ") + strMsg);
		}
		#endif
	}

	if (p) {
		strClass = a2w (p->GetRuntimeClass ()->m_lpszClassName);
		p->Delete ();
	}

	str += _T ("\n") + GetStackTrace (pszFile, nLine);

	//#ifdef _DEBUG
	{
		CString strDebug;
		strDebug.Format (_T ("%s(%u): [BW] %s"), pszFile, nLine, str);
		::OutputDebugString (strDebug);
	}
	//#endif //_DEBUG

	#ifdef _DEBUG
	if (FILE * fp = _tfopen (_T ("Exceptions.log"), _T ("a"))) {
		CString strLog;

		strLog.Format (_T ("[%s] %s\n%s\n[---------------------]\n\n\n"), 
			CTime::GetCurrentTime ().Format (_T ("%c")), 
			strClass,
			str);
		fwrite (w2a (strLog), strLog.GetLength (), 1, fp);
		fclose (fp);
	}
	#endif //_DEBUG

	if (!strClass.CompareNoCase (_T ("CDBException")) || !strClass.CompareNoCase (_T ("CDaoException"))) {
		CString strCmdLine = ::GetCommandLine ();

		strCmdLine.MakeLower ();

		if (strCmdLine.Find (_T ("/networked")) != -1 && strCmdLine.Find (_T ("/dsn_local")) != -1) {
			if (str.Find (_T ("not a valid path")) != -1) {
				DWORD dwResult = 0;
				CString strCount;
		
				::mapExceptions.Lookup (strMsg, strCount);
				int nCount = _ttoi (strCount);
				
				if (nCount <= 1) {
					TRACEF (_T ("ignoring: [") + ToString (nCount) + _T ("]: ") + (str.GetLength () > 50 ? str.Left (50) : str));
					bMsgBox = false;
				}

				LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_DATABASECONNECTIONLOST, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);	
			}
		}
	}

	if (bMsgBox)
		MsgBox (str, MB_ICONSTOP);

	::LeaveCriticalSection (&(::exceptionHandler.m_csException));
	return str;
}

BOOL APIENTRY DllMain (HANDLE hModule, DWORD dwReason, LPVOID lpReserved)
{
	HRESULT hInit = S_OK;
	struct
	{
		HRESULT m_h;
		LPCTSTR m_lpsz;
	}
	static const map [] = 
	{
		{ S_OK,					_T ("S_OK"),				}, 
		{ S_FALSE,				_T ("S_FALSE"),				}, 
		{ OLE_E_WRONGCOMPOBJ,	_T ("OLE_E_WRONGCOMPOBJ"),	}, 
		{ RPC_E_CHANGED_MODE,	_T ("RPC_E_CHANGED_MODE"),	}, 
	};

	switch (dwReason) {
	case DLL_THREAD_ATTACH:
		if (!::hInstance) {
			hInit = ::OleInitialize(NULL);
			FoxjetDatabase::Encrypted::Init ();

			switch (hInit) {
			case E_INVALIDARG:
			case E_OUTOFMEMORY: 
			case E_UNEXPECTED:
				for (int i = 0; i < ARRAYSIZE (map); i++) 
					if (map [i].m_h == hInit)
						MESSAGEBOX (map [i].m_lpsz);

				MESSAGEBOX (_T ("OLE Automation Init Failed"));
				return FALSE;
			}
		}

		::hInstance = (HINSTANCE)hModule;
		break;
	case DLL_PROCESS_ATTACH:
		if (!::hInstance) {
			hInit = ::OleInitialize(NULL);
			FoxjetDatabase::Encrypted::Init ();
		
			switch (hInit) {
			case E_INVALIDARG:
			case E_OUTOFMEMORY: 
			case E_UNEXPECTED:
				for (int i = 0; i < ARRAYSIZE (map); i++) 
					if (map [i].m_h == hInit)
						MESSAGEBOX (map [i].m_lpsz);

				MESSAGEBOX (_T ("OLE Automation Init Failed"));
				return FALSE;
			}
		}

		::hInstance = (HINSTANCE)hModule;
		break;
	case DLL_THREAD_DETACH:
		::OleUninitialize();
		break;
	case DLL_PROCESS_DETACH:
		#ifndef _DEBUG
		::OleUninitialize();
		#endif
		break;
	}

    return TRUE;
}

void CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer)
{
	if (lpVer) 
		* lpVer = FoxjetCommon::verApp; // memcpy (lpVer, &FoxjetCommon::verApp, sizeof (FoxjetCommon::VERSIONSTRUCT));
}

HRESULT CALLBACK DllGetVersion (DLLVERSIONINFO * pdvi)
{
	FoxjetCommon::VERSIONSTRUCT ver;

	fj_GetDllVersion (&ver);

	if (pdvi) {
		pdvi->dwMajorVersion	= ver.m_nMajor;
		pdvi->dwMinorVersion	= ver.m_nMinor;
		pdvi->dwBuildNumber		= ver.m_nInternal;
		pdvi->dwPlatformID		= DLLVER_PLATFORM_WINDOWS;
	}

	return NOERROR;
}

DB_API bool FoxjetDatabase::IsValidLicense (COdbcDatabase & database, const CString & strLicense)
{
	return true; // TODO
}

DB_API UINT FoxjetDatabase::GetMaxHeads (COdbcDatabase & database, const CString & strLicense)
{
	TRACEF (strLicense);

	return MAX_HEADS;
}

DB_API UINT FoxjetDatabase::GetMaxLines (COdbcDatabase & database, const CString & strLicense)
{
	TRACEF (strLicense);

	if (!IsValidLicense (database, strLicense))
		return 1;

#ifdef __IPPRINTER__
//	#if __CUSTOM__ == __SW0818__
		return 50;
//	#else
//		return 8;
//	#endif
#else
	return 2;
#endif	
}


DB_API bool FoxjetDatabase::DeleteDSN (const CString & strDSN, const CString& strDriver)
{
	std::wstring wstrDriver = strDriver.GetLength () ? strDriver : SQL_MDB_DRIVER;
	std::wstring wstrAttributes = (_T("DSN=") + strDSN);

	if (SQLConfigDataSource(NULL, ODBC_REMOVE_SYS_DSN, wstrDriver.c_str (), wstrAttributes.c_str())) {
		CString strSection = lpszRegSection + strDSN;
		HKEY hKey;

		if (::RegCreateKey(HKEY_LOCAL_MACHINE, strSection, &hKey) == ERROR_SUCCESS) {
			::RegDeleteValue(hKey, _T("Connect"));
			::RegCloseKey(hKey);
		}

		return true;
	}
	else
		TRACE_SQLERROR();

#ifdef _DEBUG
	WORD	iError = 1;
	DWORD	pfErrorCode;
	TCHAR	lpszErrorMsg[301];
	WORD	cbErrorMsgMax = 300;
	RETCODE	rc;

	while (SQL_NO_DATA != (rc = SQLInstallerError (iError++, &pfErrorCode, lpszErrorMsg, cbErrorMsgMax, NULL)))
		if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
			TRACEF ((CString)CAnsiString (lpszErrorMsg));

#endif //_DEBUG
	
	return false;
}

static ULONG GetPanelID (CArray <PANELSTRUCT, PANELSTRUCT &> & v, UINT nNumber)
{
	for (int i = 0; i < v.GetSize (); i++) 
		if (v [i].m_nNumber == nNumber)
			return v [i].m_lID;

	return -1;
}

static int FindByPrinterID (CArray <LINESTRUCT, LINESTRUCT &> & v, ULONG lPrinterID)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i].m_lPrinterID == lPrinterID)
			return i;

	return -1;
}

static int Find (CArray <LINESTRUCT, LINESTRUCT &> & v, ULONG lID)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (v [i].m_lID == lID)
			return i;

	return -1;
}

void UpdateLinesWithPrinterID (COdbcDatabase & database, ULONG lPrinterID)
{
	DBMANAGETHREADSTATE (database);

	try
	{
		COdbcRecordset rst (database);
					
		rst.Open (_T ("SELECT * FROM ") + CString (Lines::m_lpszTable));

		if (rst.GetFieldIndex (Lines::m_lpszPrinterID) == -1) {
			CString str;

			rst.Close ();
			str.Format (_T ("ALTER TABLE [%s] ADD [%s] INTEGER;"), Lines::m_lpszTable, Lines::m_lpszPrinterID);
			TRACEF (str);
			database.ExecuteSQL (str);

			str.Format (_T ("UPDATE [%s] SET [%s]=%d;"), 
				Lines::m_lpszTable,
				Lines::m_lpszPrinterID,
				lPrinterID);
			TRACEF (str);
			database.ExecuteSQL (str);
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
}


DB_API ULONG FoxjetDatabase::AddDefaultPrinter (COdbcDatabase & database)
{
	DBMANAGETHREADSTATE (database);

	PRINTERSTRUCT p;

	p.m_strName		= LoadString (IDS_MASTERCONFIG);
	p.m_strAddress	= _T ("0.0.0.0");
	p.m_bMaster		= true;
	p.m_type		= MATRIX;

	if (!IsNetworked ()) {
		p.m_strName		= GetComputerName ();
		p.m_strAddress	= GetIpAddress ();
		p.m_bMaster		= true;
		p.m_type		= IsMatrix () ? MATRIX : ELITE;
	}

	try
	{
		CString str;

		str.Format (_T ("UPDATE [%s] SET [%s]=%d;"), 
			Printers::m_lpszTable,
			Printers::m_lpszMaster,
			0);
		TRACEF (str);
		database.ExecuteSQL (str);
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	if (!IsNetworked ()) {
		VERIFY (AddPrinterRecord (database, p));

		UpdateLinesWithPrinterID (database, p.m_lID);

		try
		{
			CString str;

			str.Format (_T ("UPDATE [%s] SET [%s]=%d;"), 
				Lines::m_lpszTable,
				Lines::m_lpszPrinterID,
				p.m_lID);
			TRACEF (str);
			database.ExecuteSQL (str);

		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}
	else {
		CArray <LINESTRUCT, LINESTRUCT &> vLines;
		CArray <PRINTERSTRUCT, PRINTERSTRUCT &> vPrinters;
		CMap <ULONG, ULONG, ULONG, ULONG> vLineMapping;
		const CString strAddr = GetIpAddress ();

		GetLineRecords (database, vLines);
		GetPrinterRecords (database, vPrinters);
		int nPrinters = vPrinters.GetSize ();

		VERIFY (AddPrinterRecord (database, p));

		if (nPrinters == 0) {
			CString str;

			str.Format (_T ("UPDATE [%s] SET [%s]=%d;"), 
				Lines::m_lpszTable,
				Lines::m_lpszPrinterID,
				p.m_lID);
			TRACEF (str);
			database.ExecuteSQL (str);
		}
		else if (nPrinters == 1) {
			if (vLines.GetSize ()) {
				try {
					for (int i = 0; i < vLines.GetSize (); i++) {
						LINESTRUCT line = vLines [i];
						const ULONG lLineID = line.m_lID;
						CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;
						CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

						GetLineHeads (database, lLineID, vHeads);
						line.m_lPrinterID = p.m_lID;

						VERIFY (AddLineRecord (database, line));
						GetPanelRecords (database, line.m_lID, vPanels);
						const ULONG lMasterLineID = line.m_lID;
						vLineMapping.SetAt (lLineID, lMasterLineID);

						for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
							HEADSTRUCT head = vHeads [nHead];
							PANELSTRUCT panel;

							VERIFY (GetPanelRecord (database, head.m_lPanelID, panel));
							head.m_lPanelID = GetPanelID (vPanels, panel.m_nNumber);
							VERIFY (AddHeadRecord (database, head));
						}
					}
				}
				catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
				catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

				/*
				for (int nPrinters = 0; nPrinters < vPrinters.GetSize (); nPrinters++) {
					ULONG lPrinterID = vPrinters [nPrinters].m_lID;
					CArray <LINESTRUCT, LINESTRUCT &> vLines;

					GetPrinterLines (database, lPrinterID, vLines);

					for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
						CString str;
						ULONG lLineID = vLines [nLine].m_lID;
						ULONG lMasterLineID = -1;

						VERIFY (vLineMapping.Lookup (lLineID, lMasterLineID));
						ASSERT (lMasterLineID != -1);

						str.Format (_T ("Update [%s] SET [%s].[%s]=%d WHERE [%s].[%s]=%d;"), 
							Settings::m_lpszTable, 
							Settings::m_lpszTable, Settings::m_lpszLineID, lMasterLineID,
							Settings::m_lpszTable, Settings::m_lpszLineID, lLineID);
						TRACEF (str);
						database.ExecuteSQL (str);
					}
				}
				*/
			}
			else {
				for (int i = 0; i < vPrinters.GetSize (); i++) {
					ULONG lPrinterID = vPrinters [i].m_lID;

					int nIndex = FindByPrinterID (vLines, lPrinterID);

					if (nIndex != -1)
						vLines.RemoveAt (nIndex);
				}
			}
		}
		else {
			const CString strConfig = LoadString (IDS_MASTERCONFIG);

			for (int i = 0; i < vPrinters.GetSize (); i++) {
				PRINTERSTRUCT & printer = vPrinters [i];

				if (printer.m_strName.CompareNoCase (strConfig) != 0 && printer.m_strAddress.CompareNoCase (_T ("0.0.0.0")) != 0) {
					GetPrinterLines (database, printer.m_lID, vLines);
					break;
				}
			}

			if (!vLines.GetSize ()) 
				GetLineRecords (database, vLines);

			if (vLines.GetSize ()) {
				for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
					LINESTRUCT line = vLines [nLine];
					CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
					CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;

					GetLineHeads (database, line.m_lID, vHeads);
					line.m_lPrinterID = p.m_lID;

					VERIFY (AddLineRecord (database, line)); 
					GetPanelRecords (database, line.m_lID, vPanels);

					for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
						HEADSTRUCT head = vHeads [nHead];
						ULONG lAddr = _tcstoul (head.m_strUID, NULL, 16);
						PANELSTRUCT panel;

						VERIFY (GetPanelRecord (database, head.m_lPanelID, panel));
						head.m_lPanelID = GetPanelID (vPanels, panel.m_nNumber);
						head.m_strUID.Format (_T ("%X"), GetSynonymUsbAddr (lAddr)); //dlgCardType.m_nType == CCardTypeDlg::CARD_USB ? GetSynonymUsbAddr (lAddr) : GetSynonymPhcAddr (lAddr));
						VERIFY (AddHeadRecord (database, head));
					}
				}
			}
			else {
				LINESTRUCT line;
				HEADSTRUCT head;
				CArray <PANELSTRUCT, PANELSTRUCT &> vPanels;
				LPCTSTR lpszLine = _T ("{LINESTRUCT,2,LINE0001,Test line 1,12:00,NO READ,1,3,0,5,10.1.2.50,0,9600\\,8\\,N\\,1\\,0,9600\\,8\\,N\\,1\\,0,9600\\,8\\,N\\,1\\,0,3,1000,10,1000,2500,3,20,3,1000,28,1000,5500,2,100,24.000000,1246576930}");
				LPCTSTR lpszHead = _T ("{HEADSTRUCT,37,7,Head 1,1,0,256,300,2000,0,0,1,0,90.000000,0,0,1,4,0,1,120,4.000000,24.000000,57600\\,8\\,N\\,1\\,0,2,1}");

				FoxjetDatabase::FromString (lpszLine, line);
				FoxjetDatabase::FromString (lpszHead, head);

				line.m_lPrinterID = p.m_lID;
				VERIFY (AddLineRecord (database, line)); 
				GetPanelRecords (database, line.m_lID, vPanels);

				ULONG lAddr = _tcstoul (head.m_strUID, NULL, 16);
				bool bUSB = lAddr < 0x300;

				ASSERT (vPanels.GetSize ());
				PANELSTRUCT panel = vPanels [0];
				head.m_lPanelID = GetPanelID (vPanels, panel.m_nNumber);
				head.m_strUID = _T ("1");
				VERIFY (AddHeadRecord (database, head));
			}
		}
	}

	return p.m_lID;
}

extern CMap <ULONG, ULONG, UINT, UINT> mapMasterEncoderDivisor;

DB_API void FoxjetDatabase::UpgradeDatabase (COdbcDatabase & database, const CString & strDSN)
{
	if (database.IsSqlServer ())
		return;

	try {
		COdbcRecordset rst (database);
		bool bUpdate = false;
			
		rst.Open (_T ("SELECT * FROM ") + CString (Settings::m_lpszTable));

		/* 
		current Settings table
		[0]: LineID SQL_INTEGER scale: 0, precision: 10, nullability: 1
		[1]: Key SQL_LONGVARCHAR scale: 0, precision: 2147483647, nullability: 1
		[2]: Data SQL_LONGVARCHAR scale: 0, precision: 2147483647, nullability: 1

		old Settings table
		[0]: LineID SQL_INTEGER scale: 0, precision: 10, nullability: 1
		[1]: Key SQL_VARCHAR scale: 0, precision: 50, nullability: 1
		[2]: Data SQL_LONGVARCHAR scale: 0, precision: 2147483647, nullability: 1
		*/

		for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
			CODBCFieldInfo info;

			rst.GetODBCFieldInfo (i, info);

			if (!info.m_strName.CompareNoCase (Settings::m_lpszKey)) {
				bUpdate = info.m_nSQLType == SQL_VARCHAR;
				break;
			}
		}

		rst.Close ();

		if (bUpdate) {
			CString str;

			str.Format (_T ("ALTER TABLE %s DROP CONSTRAINT [%s];"), Settings::m_lpszTable, Settings::m_lpszKey);
			TRACER (str);
			database.ExecuteSQL (str);

			str.Format (_T ("ALTER TABLE %s ALTER COLUMN [%s] MEMO;"), Settings::m_lpszTable, Settings::m_lpszKey);
			TRACER (str);
			database.ExecuteSQL (str);
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	try
	{
		COdbcRecordset rst (database);
		bool bUpdate = false;
			
		rst.Open (_T ("SELECT * FROM ") + CString (Deleted::m_lpszTable));

		if (rst.GetFieldIndex (Deleted::m_lpszData) == -1) {
			CString strTmp;

			rst.Close ();
			strTmp.Format (_T ("ALTER TABLE [%s] ADD [%s] MEMO;"), Deleted::m_lpszTable, Deleted::m_lpszData);
			TRACER (strTmp);
			database.ExecuteSQL (strTmp);
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	try
	{ //if (IsNetworked ()) 
		if (COdbcDatabase::FindTable (strDSN, Printers::m_lpszTable) == -1) {
			CString str;
			
			str.Format (
				_T ("CREATE TABLE %s (")
				_T (" %s AUTOINCREMENT PRIMARY KEY")
				_T (", %s MEMO")
				_T (", %s MEMO")
				_T (", %s YESNO")
				_T (", %s INTEGER")
				_T (");"), 
				Printers::m_lpszTable,
				Printers::m_lpszID,
				Printers::m_lpszName,
				Printers::m_lpszAddress,
				Printers::m_lpszMaster,
				Printers::m_lpszType);
			TRACER (str);
			database.ExecuteSQL (str);

			if (IsNetworked ())
				AddDefaultPrinter (database);

			UpdateLinesWithPrinterID (database, GetMasterPrinterID (database));
		}
		else {
			COdbcRecordset rst (database);
			bool bUpdate = false;
				
			rst.Open (_T ("SELECT * FROM ") + CString (Printers::m_lpszTable));

			if (rst.GetFieldIndex (Printers::m_lpszType) == -1) {
				CString str;

				rst.Close ();
				str.Format (_T ("ALTER TABLE [%s] ADD [%s] INTEGER;"), Printers::m_lpszTable, Printers::m_lpszType);
				TRACER (str);
				database.ExecuteSQL (str);

				str.Format (_T ("UPDATE [%s] SET [%s]=%d;"), 
					Printers::m_lpszTable,
					Printers::m_lpszType,
					MATRIX);
				TRACER (str);
				database.ExecuteSQL (str);
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	try
	{
		if (!IsConfig ()) {
			const ULONG lPrinterID = GetMasterPrinterID (database);

			{
				COdbcRecordset rst (database);
					
				rst.Open (_T ("SELECT * FROM ") + CString (Reports::m_lpszTable));

				if (rst.GetFieldIndex (Reports::m_lpszPrinterID) == -1) {
					CString str;

					rst.Close ();
					str.Format (_T ("ALTER TABLE [%s] ADD [%s] INTEGER;"), Reports::m_lpszTable, Reports::m_lpszPrinterID);
					TRACER (str);
					database.ExecuteSQL (str);

					str.Format (_T ("UPDATE [%s] SET [%s]=%d;"), 
						Reports::m_lpszTable,
						Reports::m_lpszPrinterID,
						lPrinterID);
					TRACER (str);
					database.ExecuteSQL (str);
				}
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	if (IsNetworked ()) {
		CString str;
		CArray <LINESTRUCT, LINESTRUCT &> vLines;
		ULONG lMasterID = GetMasterPrinterID (database);
		CMap <ULONG, ULONG, ULONG, ULONG> mapLineID;

		GetPrinterLines (database, lMasterID, vLines);

		if (vLines.GetSize ()) {
			{
				COdbcRecordset rst (&database);

				str.Format (_T ("SELECT * FROM [%s] WHERE (NOT ("), Tasks::m_lpszTable);

				for (int i = 0; i < vLines.GetSize (); i++) { 
					CString strTmp;

					strTmp.Format (_T ("[%s].[%s]=%d"), Tasks::m_lpszTable, Tasks::m_lpszLineID, vLines [i].m_lID);
					str += strTmp;

					if ((i + 1) < vLines.GetSize ())
						str += _T (" OR ");
				}

				str += _T ("));");
				//str.Format (_T ("SELECT * FROM [%s];"), Tasks::m_lpszTable);
				TRACER (str);

				try {
					rst.Open (str, CRecordset::dynaset);

					while (!rst.IsEOF ()) {
						CArray <LINESTRUCT, LINESTRUCT &> vUnmappedLines;
						LINESTRUCT line;
						TASKSTRUCT task;
						PRINTERSTRUCT printer;

						rst >> task;
						TRACER (ToString (task));

						GetLineRecord (database, task.m_lLineID, line);
						GetPrinterRecord (database, line.m_lPrinterID, printer);
						GetPrinterLines (database, printer.m_lID, vUnmappedLines);
						int nIndex = Find (vUnmappedLines, line.m_lID);

						ASSERT (nIndex >= 0);
						ASSERT (nIndex < vLines.GetSize ());

						if (nIndex >= 0 && nIndex < vLines.GetSize ()) {
							line = vLines [nIndex];

							{
								const ULONG lFrom = line.m_lID;
								const ULONG lTo = task.m_lLineID;
								ULONG lTmp;

								if (mapLineID.Lookup (lFrom, lTmp))
									ASSERT (lTo == lTmp);

								mapLineID.SetAt (lFrom, lTo);
							}
						}

						rst.MoveNext ();
					}

					rst.Close ();
				}
				catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
				catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
			}

			try {
				PRINTERSTRUCT master;
				CArray <HEADSTRUCT, HEADSTRUCT &> vMasterHeads;
				CArray <PRINTERSTRUCT, PRINTERSTRUCT &> vPrinters;

				GetPrinterRecord (database, lMasterID, master);
				GetPrinterRecords (database, vPrinters);
				GetPrinterHeads (database, master.m_lID, vMasterHeads);

				for (int i = 0; i < vPrinters.GetSize (); i++) {
					PRINTERSTRUCT & p = vPrinters [i];

					if (p.m_lID != master.m_lID) {
						CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

						GetPrinterHeads (database, p.m_lID, vHeads);

						ASSERT (vHeads.GetSize () == vMasterHeads.GetSize ());

						if (vHeads.GetSize () == vMasterHeads.GetSize ()) {
							for (int i = 0; i < vHeads.GetSize (); i++) {
								ULONG lFrom = vHeads [i].m_lID;
								ULONG lTo	= vMasterHeads [i].m_lID;

								str.Format (_T ("UPDATE [%s] SET [%s].[%s]=%d WHERE [%s].[%s]=%d"), 
									Messages::m_lpszTable, 
									Messages::m_lpszTable, Messages::m_lpszHeadID, lTo,
									Messages::m_lpszTable, Messages::m_lpszHeadID, lFrom);
								TRACER (str);
								//MsgBox (str);
								database.ExecuteSQL (str);
							}
						}
					}
				}
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

					
			try {
				for (POSITION pos = mapLineID.GetStartPosition (); pos; ) {
					ULONG lFrom = -1, lTo = -1;

					mapLineID.GetNextAssoc (pos, lTo, lFrom);

					if (lFrom != -1 && lTo != -1 && lFrom != lTo) {
						CString str;
						str.Format (_T ("UPDATE [%s] SET [%s]=%d WHERE [%s]=%d"), 
							Tasks::m_lpszTable, 
							Tasks::m_lpszLineID, lTo,
							Tasks::m_lpszLineID, lFrom);
						TRACER (str);
						//MsgBox (str);
						database.ExecuteSQL (str);
					}
				}
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
		}
	}


	try
	{
		{
			CArray <PRINTERSTRUCT, PRINTERSTRUCT &> vPrinters;
			const CString strAddr = GetIpAddress ();

			GetPrinterRecords (database, vPrinters);

			for (int nPrinter = 0; nPrinter < vPrinters.GetSize (); nPrinter++) { 
				CString strAddr2 = vPrinters [nPrinter].m_strAddress;

				if (!strAddr.CompareNoCase (strAddr2)) {
					LPCTSTR lpszKeys [] = 
					{
						_T ("CScanReportDlg::AutoExport"),
						_T ("CPrintReportDlg::AutoExport"),
						_T ("Control::print report"),
						_T ("Control::scan report"),
					};
					CString str;

					for (int i = 0; i < ARRAYSIZE (lpszKeys); i++) {
						str.Format (_T ("UPDATE [%s] SET [%s]=%d WHERE [%s]=%d AND [%s]='%s';"), 
							Settings::m_lpszTable,
							Settings::m_lpszLineID,
							vPrinters [nPrinter].m_lID,
							Settings::m_lpszLineID, 0,
							Settings::m_lpszKey, lpszKeys [i]);
						TRACER (str);
						database.ExecuteSQL (str);
					}

					break;
				}
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	{
		if (::mapMasterEncoderDivisor.IsEmpty ()) {
			ULONG lMasterID = GetMasterPrinterID (database);
			CArray <HEADSTRUCT, HEADSTRUCT &> v;

			GetPrinterHeads (database, lMasterID, v);

			for (int i = 0; i < v.GetSize (); i++) {
				ULONG lUID = _tcstoul (v [i].m_strUID, NULL, 16);
				UINT nEncoderDivisor = v [i].m_nEncoderDivisor;

				::mapMasterEncoderDivisor.SetAt (lUID, nEncoderDivisor);
			}
		}
	}
}

DB_API bool FoxjetDatabase::IsAccdb ()
{
	CString str = ::GetCommandLine ();

	str.MakeUpper ();

	return str.Find (_T ("/ACCDB")) != -1 ? true : false;
}

DB_API bool FoxjetDatabase::IsAccdb (const CString & strFilename)
{
	CString str = strFilename;

	str.MakeUpper ();

	return str.Find (_T (".ACCDB")) != -1 ? true : false;
}

DB_API void FoxjetDatabase::TraceSQLInstallerError (LPCTSTR lpszFile, ULONG lLine)
{
	DWORD dw = 0;
	TCHAR sz [256] = { 0 };  

	SQLInstallerError (1, &dw, sz, ARRAYSIZE (sz), NULL);

	#ifdef _DEBUG
	CDebug::Trace (sz, true, lpszFile, lLine);
	#else
	CString str;

	str.Format (_T ("%s(%d): [BW] %s"), lpszFile, lLine, sz);
	::OutputDebugString (str);
	#endif
}

DB_API bool FoxjetDatabase::OpenDatabase (COdbcDatabase & database, const CString & strDSN, const CString & strFilename)
{
	static CMapStringToString map;
	CString strConnect;
	int nTries = 0;
	bool bOpen = false;
	const CString strDir = GetHomeDir (); 
	const CString strFilepath = strDir + _T ("\\") + strFilename;

	if (database.IsOpen ()) 
		database.Close ();

	if (!strConnect.GetLength ()) 
		strConnect = FoxjetDatabase::GetProfileString (
			lpszRegSection + strDSN, _T ("Connect"),
			_T ("DSN=") + strDSN);

	while (!bOpen && nTries < 2) {
		try {
			database.OpenEx (strConnect, CDatabase::noOdbcDialog);
			strConnect = database.GetConnect ();
			bOpen = true;
		}
		catch (CDBException * e) { 
			// database.OpenEx () failed

			// possible cause for failure is looking for db in c:\foxjet\ip when it is in c:\foxjet

			HANDLEEXCEPTION_TRACEONLY (e); 

			if (::GetFileAttributes (strFilepath) == -1) { // it ain't there
				CString str;

				str.Format (LoadString (IDS_DATABASEERROR), strFilepath);

				MsgBox (str, ::AfxGetAppName (), MB_ICONSTOP | MB_OK);
				::exit (-1);
				return false; 
			}

			TCHAR sz [512];
			TCHAR szAttr [512];
			bool bACCDB = IsAccdb (strFilename);

			if (!bACCDB) {
				LPCTSTR lpszFormat = 
					_T ("DSN=%s~ ")					// dsn
					_T ("DBQ=%s\\%s~ ")				// file
					_T ("Description=%s~ ")			// dsn
					_T ("FileType=MicrosoftAccess~ ")
					_T ("DataDirectory=%s~ ")
					_T ("MaxScanRows=20~ ")
					_T ("DefaultDir=%s~ ")
					_T ("FIL=MS Access;~ ")
					_T ("MaxBufferSize=2048~ ")
					_T ("MaxScanRows=8~ ")
					_T ("PageTimeout=5~ ")
					_T ("Threads=3~ ");

				_stprintf (sz, lpszFormat, 
					strDSN, 
					strDir, strFilename,
					strDSN,
					strDir, 
					strDir);
			}
			else {
				LPCTSTR lpszFormat = 
					_T ("DSN=%s~ ")					// dsn
					_T ("DBQ=%s\\%s~ ");			// file

				_stprintf (sz, lpszFormat, 
					strDSN, 
					strDir, strFilename,
					strDSN,
					strDir, 
					strDir);
			}

			int nLen = _tcsclen (sz);

			_tcscpy (szAttr, (sz));
			TRACEF (szAttr);

			for (int i = 0; i < nLen; i++)
				if (szAttr [i] == '~')
					szAttr [i] = '\0';

			if (!SQLConfigDataSource (NULL, ODBC_ADD_SYS_DSN, bACCDB ? SQL_ACCDB_DRIVER : SQL_MDB_DRIVER, szAttr))
				TRACE_SQLERROR ();

			strConnect = _T ("DSN=") + strDSN;
			FoxjetDatabase::WriteProfileString (lpszRegSection + strDSN, _T ("Connect"), strConnect);
		}
		catch (CMemoryException * e) { HANDLEEXCEPTION (e); }

		nTries++;
	}

	if (bOpen) {
		CString strCount;

		map.Lookup (strDSN, strCount);
		map.SetAt (strDSN, ToString (_ttoi (strCount) + 1));

		if (!_ttoi (strCount))
			UpgradeDatabase (database, strDSN);
	}

	return bOpen;
}

DB_API bool FoxjetDatabase::BeginTrans (COdbcDatabase & database, LPCTSTR lpszFile, ULONG lLine)
{
	if (database.IsOpen ()) {
		database.CloseAllRecordsets ();

		if (database.BeginTrans ()) {
			database.m_bTransaction = true;
			return true;
		}

		#ifdef _DEBUG
		else {
			CString str;
			str.Format (_T ("BeginTrans () failed on [%s]\n\nFile: %s\nLine: %u"),
				database.GetConnect (), lpszFile, lLine);
			MsgBox (str, (LPCTSTR)NULL);
			CDebug::Trace (_T ("COdbcDatabase::BeginTrans () failed"), true, lpszFile, lLine);
		}
		#endif //_DEBUG
	}
	#ifdef _DEBUG
	else
		CDebug::Trace (_T ("database is not open"), true, lpszFile, lLine);
	#endif //_DEBUG

	return false;
}

DB_API bool FoxjetDatabase::CommitTrans (COdbcDatabase & database, LPCTSTR lpszFile, ULONG lLine)
{
	if (database.IsOpen ()) {
		if (database.CommitTrans ()) {
			database.m_bTransaction = false;
			return true;
		}

		#ifdef _DEBUG
		else {
			CString str;
			str.Format (_T ("CommitTrans () failed on [%s]\n\nFile: %s\nLine: %u"),
				database.GetConnect (), lpszFile, lLine);
			MsgBox (str);
			CDebug::Trace (_T ("COdbcDatabase::CommitTrans () failed"), true, lpszFile, lLine);
		}
		#endif //_DEBUG
	}
	#ifdef _DEBUG
	else
		CDebug::Trace (_T ("database is not open"), true, lpszFile, lLine);
	#endif //_DEBUG

	return false;
}

DB_API bool FoxjetDatabase::RollbackTrans (COdbcDatabase & database, LPCTSTR lpszFile, ULONG lLine)
{
	if (database.IsOpen ()) {
		if (database.Rollback ()) {
			database.m_bTransaction = false;
			return true;
		}
		#ifdef _DEBUG
		else {
			CString str;
			str.Format (_T ("BeginTrans () failed on [%s]\n\nFile: %s\nLine: %u"),
				database.GetConnect (), lpszFile, lLine);
			MsgBox (str);
			CDebug::Trace (_T ("COdbcDatabase::Rollback () failed"), true, lpszFile, lLine);
		}
		#endif //_DEBUG
	}
	#ifdef _DEBUG
	else
		CDebug::Trace (_T ("database is not open"), true, lpszFile, lLine);
	#endif //_DEBUG

	return false;
}

DB_API CString FoxjetDatabase::GetHomeDir ()
{
	TCHAR sz [MAX_PATH] = { 0 };

	HMODULE hModule = ::GetModuleHandle (NULL);
	::GetModuleFileName (hModule, sz, ARRAYSIZE (sz) - 1);

	const CString strPath (sz);
	CString strPathLower (sz);
	strPathLower.MakeLower ();

	VERIFY (::GetFileTitle (strPath, sz, ARRAYSIZE (sz) - 1) == 0);
	
	CString strTitle (sz);
	strTitle.MakeLower ();

	int nIndex = strPathLower.Find (strTitle);

	ASSERT (nIndex != -1);
	CString str = strPath.Left (nIndex - 1);

	return str;
}

DB_API int FoxjetDatabase::GetFiles (const CString & strDir, const CString & strFile, CStringArray & v)
{
	if (strFile.Find (_T ("*")) == -1 && strFile != _T ("*.*")) {
		CStringArray vTmp;

		GetFiles (strDir, _T ("*.*"), vTmp);

		for (int i = 0; i < vTmp.GetSize (); i++) {
			CString str = vTmp [i];

			if (str.Replace (_T ("\\") + strFile, _T ("")))
				v.Add (vTmp [i]);
		}

		return v.GetSize ();
	}

	int nFiles = 0;
	WIN32_FIND_DATA fd;
	const CString str = strDir + _T ("\\") + strFile;
	HANDLE hFind = ::FindFirstFile (str, &fd);
	bool bMore = hFind != INVALID_HANDLE_VALUE;

	//TRACEF (str);

	while (bMore) {
		CString str = strDir;
		
		if (_tcscmp (fd.cFileName, _T (".")) != 0 && _tcscmp (fd.cFileName, _T (".."))) {
			if (str [str.GetLength () - 1] != '\\')
				str += '\\';

			str += fd.cFileName;

			v.Add (str);
			nFiles++;

			if (::GetFileAttributes (str) & FILE_ATTRIBUTE_DIRECTORY) 
				nFiles += GetFiles (strDir + _T ("\\") + fd.cFileName, strFile, v);
		}

		ZeroMemory (&fd, sizeof (fd));
		bMore = ::FindNextFile (hFind, &fd) ? true : false;
	}

	::FindClose (hFind);

	return nFiles;
}

DB_API BOOL FoxjetDatabase::EnablePrivilege (LPCTSTR lpName)
{
	HANDLE hToken;
	LUID sedebugnameValue;
	TOKEN_PRIVILEGES tp;

	if (!::OpenProcessToken (GetCurrentProcess (), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
		return FALSE;

 

	if (!::LookupPrivilegeValue (NULL, lpName, &sedebugnameValue)) {
		::CloseHandle( hToken );
	
		return FALSE;
	}

	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = sedebugnameValue;
	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	if (!::AdjustTokenPrivileges (hToken, FALSE, &tp, sizeof(tp), NULL, NULL)) {
		::CloseHandle( hToken );

		return FALSE;
	}

	::CloseHandle (hToken);

	return TRUE;

}

DB_API CString FoxjetDatabase::MakeRelativePath (const CString & strPath)
{
	const CString strHome = FoxjetDatabase::GetHomeDir () + _T ("\\");
	CString str (strPath);

	str.TrimLeft ();
	str.TrimRight ();

	if (strPath.Find (strHome) == 0) {
		str.Delete (0, strHome.GetLength ());
		str = _T (".\\") + str;
	}

	return str;
}

DB_API CString FoxjetDatabase::MakeAbsolutePath (const CString & strPath)
{
	const CString strHome = _T (".\\");
	CString str (strPath);

	str.TrimLeft ();
	str.TrimRight ();

	if (strPath.Find (strHome) == 0) {
		str.Delete (0, strHome.GetLength ());
		str = FoxjetDatabase::GetHomeDir () + _T ("\\") + str;
	}

	return str;
}

DB_API void FoxjetDatabase::DeleteAllRecords (COdbcDatabase & db)
{
	LPCTSTR lpszExclude [] = 
	{
		_T ("ClassOfService"),
		_T ("CountryCodes"),
	};

	try {
		for (int i = 0; i < db.GetTableCount (); i++) {
			CString str;
			const COdbcTable * pTable = db.GetTable (i);
			bool bSkip = false;

			for (int j = 0; j < ARRAYSIZE (lpszExclude); j++) {
				if (!pTable->GetName ().CompareNoCase (lpszExclude [j])) {
					bSkip = true;
					break;
				}
			}

			if (!pTable->GetType ().CompareNoCase (_T ("TABLE")) && !bSkip) {
				str.Format (_T ("DELETE * FROM [%s]"), pTable->GetName ());
				TRACEF (str);
				db.ExecuteSQL (str);
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
}

DB_API bool FoxjetDatabase::IsValveHead (const FoxjetDatabase::HEADSTRUCT & head)
{
	return IsValveHead (head.m_nHeadType);
}

DB_API bool FoxjetDatabase::IsValveHead (FoxjetDatabase::HEADTYPE type)
{
	switch (type) {
	case IV_72:
		return true;
	}

	return false;
}

DB_API bool FoxjetDatabase::IsHpHead (const FoxjetDatabase::HEADSTRUCT & head)
{
	return IsHpHead (head.m_nHeadType);
}

DB_API bool FoxjetDatabase::IsHpHead (FoxjetDatabase::HEADTYPE type)
{
#ifdef __HPHEAD__
	switch (type) {
	case HPHEAD:
		return true;
	}
#endif

	return false;
}

DB_API int FoxjetDatabase::GetValveChannels (FoxjetDatabase::VALVETYPE type)
{
	switch (type) {
	case IV_12_9:	
	case IV_78_9:		return 9;
	case IV_10_18:	
	case IV_20_18:		return 18;
	case IV_LYNX_32:	return 32;
	}

	ASSERT (0);
	return 0;
}

DB_API int FoxjetDatabase::GetValveHeight (FoxjetDatabase::VALVETYPE type)
{
	switch (type) {
	case IV_12_9:		return  500;
	case IV_78_9:		return  875; // 7/8"
	case IV_10_18:		return 1000;
	case IV_20_18:		return 2000;
	case IV_LYNX_32:	return  961;
	}

	ASSERT (0);
	return 0;
}

DB_API int FoxjetDatabase::DivideAndRound (double a, double b)
{
	double dRem = (fmod (a, b) / b) * 100.0;
	int nResult = (int)(a / b);

	if ((int)(dRem * 10) >= 500)
		nResult++;

	return nResult;
}

DB_API int FoxjetDatabase::Round (double a, int nBoundary)
{
	return DivideAndRound (a, nBoundary) * nBoundary;
}

DB_API CString FoxjetDatabase::CreateTempFilename (const CString & strDir, const CString & strPrefix, const CString & strSuffix)
{
	CString strRealDir = strDir;
	CString str;
	int nIndex = strDir.ReverseFind ((TCHAR)'\\');
	FILE * fp = NULL;

	if (strDir.Right (1) != (TCHAR)'\\')
		strRealDir += (TCHAR)'\\';

	for (int i = 0; i < INT_MAX; i++) {
		str.Format (_T ("%s%s%p.%s"), strRealDir, strPrefix, i, strSuffix);

		if ((fp = fopen (CAnsiString (str), "r")) == NULL)
			return str;
		else
			fclose (fp);
	}

	ASSERT (0);
	return str;
}

DB_API void FoxjetDatabase::SetDriver (const GUID & guid)
{
	::guidDriver = guid;
}

DB_API bool FoxjetDatabase::IsDriverInstalled (const GUID & guid)
{
#ifdef __IPPRINTER__
	return guid != GUID_INTERFACE_IVPHC ? true : false;
#endif

	if (::guidDriver != ::guidNull)
		return guid == ::guidDriver ? true : false;

	bool bInstalled = false;
	GUID g = guid;
	HDEVINFO info = SetupDiGetClassDevs (&g, NULL, NULL, DIGCF_PRESENT | DIGCF_INTERFACEDEVICE);

	if (info != INVALID_HANDLE_VALUE) {
		SP_DEVICE_INTERFACE_DATA ifdata;

		ifdata.cbSize = sizeof(ifdata);

		for (DWORD devindex = 0; ::SetupDiEnumDeviceInterfaces (info, NULL, &g, devindex, &ifdata); ++devindex) {
			DWORD needed;
	
			::SetupDiGetDeviceInterfaceDetail (info, &ifdata, NULL, 0, &needed, NULL);

			PSP_INTERFACE_DEVICE_DETAIL_DATA detail = (PSP_INTERFACE_DEVICE_DETAIL_DATA) malloc(needed);
			SP_DEVINFO_DATA did = {sizeof(SP_DEVINFO_DATA)};
			detail->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);

			if (::SetupDiGetDeviceInterfaceDetail (info, &ifdata, detail, needed, NULL, &did)) 
				bInstalled = true;

			free ((PVOID)detail);

			if (bInstalled)
				break;

		}
	}
	
   ::SetupDiDestroyDeviceInfoList (info);
   
   return bInstalled;
}

typedef struct
{
	CString m_strName;
	CTime m_tm;
} FILESTRUCT;

void Insert (CArray <FILESTRUCT, FILESTRUCT &> & v, FILESTRUCT & f)
{
	for (int i = 0; i < v.GetSize (); i++) {
		if (v [i].m_tm <= f.m_tm) {
			v.InsertAt (i, f);
			return;
		}
	}

	v.Add (f);
}

static void GetBackupFilenames (const CString & strPath, const CString & strDefTitle, CArray <FILESTRUCT, FILESTRUCT &> & vFiles)
{
	CStringArray v;
	CString strTmp (strDefTitle);
	int nMax = 0;

	int nIndex = strTmp.ReverseFind ('.');

	if (nIndex != -1)
		strTmp.Insert (nIndex, _T ("*"));
	else
		strTmp += _T ("*");

	WIN32_FIND_DATA fd;
	const CString str = strPath + _T ("\\") + strTmp;
	HANDLE hFind = ::FindFirstFile (str, &fd);
	bool bMore = hFind != INVALID_HANDLE_VALUE;

	TRACEF (str);

	while (bMore) {
		CString str = strPath;
		
		if (_tcscmp (fd.cFileName, _T (".")) != 0 && _tcscmp (fd.cFileName, _T (".."))) {
			FILETIME ft;
			FILESTRUCT f;

			ZeroMemory (&ft, sizeof (ft));

			if (str [str.GetLength () - 1] != '\\')
				str += '\\';

			str += fd.cFileName;
			f.m_strName = str;
			
			HANDLE hFile = ::CreateFile (f.m_strName, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);

			if (hFile && hFile != INVALID_HANDLE_VALUE) {
				GetFileTime (hFile, &ft, NULL, NULL);
				f.m_tm = CTime (ft);
				::CloseHandle (hFile);
			}

			Insert (vFiles, f);
		}

		ZeroMemory (&fd, sizeof (fd));
		bMore = ::FindNextFile (hFind, &fd) ? true : false;
	}

	::FindClose (hFind);
}

DB_API void FoxjetDatabase::CreateDir (CString strFullPath)
{
	int nIndex = 0;
	int nLevel = 0;

	//ASSERT (strFullPath.Find (_T ('.')) == -1);  // could have '.' in folder name

	{
		DWORD dw = ::GetFileAttributes (strFullPath);

		if (!(dw & FILE_ATTRIBUTE_DIRECTORY) && dw != -1)
			::DeleteFile (strFullPath);				
	}

	if (strFullPath [strFullPath.GetLength () - 1] != '\\')
		strFullPath += '\\';

	//TRACER (_T ("CreateDir: ") + strFullPath);

	while ((nIndex = strFullPath.Find (_T ("\\"), nIndex)) != -1) {
		CString str = strFullPath.Left (nIndex);

		//TRACER (str);
		nIndex++;
		nLevel++;

		if (::GetFileAttributes (str) == -1) {
			TRACER (_T ("CreateDirectory: ") + str);
			
			if (!::CreateDirectory (str, NULL))
				TRACER (_T ("CreateDirectory failed: ") + FormatMessage (::GetLastError ()));
	
			if (::GetFileAttributes (str) == -1) 
				TRACER (_T ("CreateDirectory failed: ") + FormatMessage (::GetLastError ()));
		}
	}
}

static void CleanupBackups (const CString & strPath)
{
	CString strTitle;
	CArray <FILESTRUCT, FILESTRUCT &> vFiles;

	GetBackupFilenames (strPath, strTitle, vFiles);

	while (vFiles.GetSize () > 20) {
		int nIndex = vFiles.GetSize () - 1;

		TRACEF (_T ("Delete: ") + vFiles [nIndex].m_strName);
		::DeleteFile (vFiles [nIndex].m_strName);
		vFiles.RemoveAt (nIndex);
	}
}

DB_API void FoxjetDatabase::GetBackupPath (CString & strPath, CString & strTitle, CString strDSN, bool bNext)
{
	const CString strKey = _T ("Software\\Foxjet\\Database");

	if (strTitle.IsEmpty ()) {
		#ifdef __WINPRINTER__
		strTitle = strDSN = ISVALVE () ? _T ("MarksmanVX") : _T ("MarksmanELITE");
		#else
		strTitle = strDSN = _T ("MarksmanNET");
		#endif
	}

	const CString strOriginalTitle = strTitle;
	strDSN = _T ("Backup");
	CString strDefPath = GetHomeDir () + _T ("\\") + strDSN;

	if (IsNetworked ()) {
		CString strDBQ = FoxjetDatabase::ODBC::GetProfileString (strDSN, _T ("DBQ"));
		int nIndex = strDBQ.ReverseFind ('\\');

		if (nIndex != -1)
			strDefPath = strDBQ.Left (nIndex + 1) + strDSN;
	}
	
	strPath = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("BackupPath"), strDefPath);
	strTitle = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("BackupTitle"), strTitle + _T (".backup.mdb"));

	{
		int nIndex = strTitle.Find (_T ("."));

		if (nIndex != -1)
			strTitle = strTitle.Left (nIndex) + _T (".backup.mdb");
	}

	{
		CArray <FILESTRUCT, FILESTRUCT &> vFiles;
		int nMax = 0;

		GetBackupFilenames (strPath, strTitle, vFiles);

		for (int i = 0; i < vFiles.GetSize (); i++) {
			int n = 0;
			CString str = vFiles [i].m_strName;

			TRACEF (vFiles [i].m_tm.Format (_T ("[%H:%M:%S %m/%d/%Y]: ")) + vFiles [i].m_strName);

			int nIndex = str.ReverseFind (_T ('\\'));

			str = str.Mid (nIndex + 1);

			while (str.GetLength () && str [0] != '.')
				str.Delete (0);

			if (_stscanf (str, _T (".backup_%d.mdb"), &n) == 1)
				nMax = max (nMax, n);
		}
		

		if (vFiles.GetSize ()) {
			int nIndex = vFiles [0].m_strName.ReverseFind ('\\');

			TRACEF (vFiles [0].m_strName);

			if (nIndex != -1) {
				strTitle = vFiles [0].m_strName.Mid (nIndex + 1);
				TRACEF (strTitle);
			}
		}
		else {
			strTitle.Format (_T ("%s.backup_%03d.mdb"), strOriginalTitle, nMax + 1);
			TRACEF (strTitle);
		}

		if (bNext) {
			strTitle.Format (_T ("%s.backup_%03d.mdb"), strOriginalTitle, nMax + 1);
			TRACEF (strTitle);
		}
	}
}

DB_API void FoxjetDatabase::DoBackup (COdbcDatabase & db)
{
	using namespace FoxjetDatabase;

	const CString strSrc = FoxjetDatabase::Extract (db.GetConnect (), _T ("DBQ="), _T (";"));
	const CString strDSN = FoxjetDatabase::Extract (db.GetConnect (), _T ("DSN="), _T (";"));
	const bool bOpen = db.IsOpen () ? true : false;
	ULONG lPrinterID = GetPrinterID (db);
	
	if (!db.IsSqlServer ()) {
		CString strPath, strTitle;
		TCHAR sz [MAX_PATH] = { 0 };

		GetFileTitle (strSrc, sz, ARRAYSIZE (sz));
		strTitle = sz;
		int nIndex = strTitle.Find (_T ("."));

		if (nIndex != -1)
			strTitle.Delete (nIndex, strTitle.GetLength () - nIndex);

		GetBackupPath (strPath, strTitle, strDSN, true);

		TRACE_PATH (strPath);

		CreateDir (strPath);

		if (::GetFileAttributes (strPath) == -1) {
			CString str;

			str.Format (LoadString (IDS_BACKUPDIRFAILED), strPath + _T ("\n") + FormatMessage (::GetLastError ()));
			MsgBox (str, LoadString (IDS_ERROR), MB_ICONERROR);
			return;
		}

		CString strDest = strPath + _T ("\\") + strTitle;

		TRACE_PATH (strSrc);
		TRACE_PATH (strDest);

		DWORD dwSize = FoxjetDatabase::GetFileSize (strSrc);

		if (FILE * fpSrc = _tfopen (strSrc, _T ("rb"))) {
			if (FILE * fpDest = _tfopen (strDest, _T ("wb"))) {
				BYTE * p = new BYTE [dwSize];

				size_t n = fread (p, dwSize, 1, fpSrc);
				fwrite (p, dwSize, 1, fpDest);

				fclose (fpDest);
				delete p;
			}
			else {
				CString str;

				str.Format (_T ("%s(%d): _tfopen failed: %s [%d bytes]"), _T (__FILE__), __LINE__, strSrc, dwSize);
				MsgBox (str, LoadString (IDS_ERROR), MB_ICONERROR);
			}

			fclose (fpSrc);
		}
		else {
			CString str;

			str.Format (_T ("%s(%d): _tfopen failed: %s [%d bytes]"), _T (__FILE__), __LINE__, strSrc, dwSize);
			MsgBox (str, LoadString (IDS_ERROR), MB_ICONERROR);
		}

		/*
		if (!::CopyFile (strSrc, strDest, FALSE)) {
			CString str;

			str.Format (LoadString (IDS_COPYFAILED), strSrc, strDest);
			str += _T ("\n") + FormatMessage (::GetLastError ());
			MsgBox (str, LoadString (IDS_ERROR), MB_ICONERROR);
		}
		*/

		CleanupBackups (strPath);
	}
}

DB_API void FoxjetDatabase::OnDefineBackupPath (CWnd * pParent)
{
	const CString strKey = _T ("Software\\Foxjet\\Database");
	CString strPath, strTitle;
	CString strDSN;
	CBackupDlg dlg;

	HRSRC hResource = ::FindResource(::hInstance, MAKEINTRESOURCE(IDD_BACKUP), RT_DIALOG);
	HGLOBAL hTemplate = LoadResource(::hInstance, hResource);
	VERIFY (dlg.InitModalIndirect (hTemplate, pParent));
	FreeResource(hTemplate);

	if (IsNetworked ())
		strTitle = strDSN = Extract (COdbcDatabase::GetDB (::GetCurrentThreadId ())->GetConnect (), _T ("DSN="), _T (";"));

	GetBackupPath (strPath, strTitle, strDSN);

	dlg.m_strPath = strPath;
	dlg.m_strTitle = strTitle;

	if (dlg.DoModal () == IDOK) {
		FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("BackupPath"), dlg.m_strPath);
		FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("BackupTitle"), dlg.m_strTitle);
	}
}

static CString GetSize (const CString & strDSN)
{
	CString strResult;

	if (!IsNetworked ()) {
		const CString strKey = _T ("Software\\Foxjet\\Database\\") + strDSN;

		strResult = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("size"), _T (""));
	}
	else {
		try {
			COdbcDatabase db (_T (__FILE__), __LINE__);
			SETTINGSSTRUCT s;

			db.Open (strDSN, FALSE, TRUE, NULL, FALSE); 

			if (GetSettingsRecord (db, 0, _T ("Database::size"), s))
				strResult = s.m_strData;

			db.Close ();
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	return strResult;
}

static void SetSize (const CString & strDSN, const CString & strSize)
{
	if (!IsNetworked ()) {
		const CString strKey = _T ("Software\\Foxjet\\Database\\") + strDSN;
	
		FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("size"), strSize);
	}
	else {
		try {
			COdbcDatabase db (_T (__FILE__), __LINE__);
			SETTINGSSTRUCT s;

			db.OpenEx (_T ("DSN=") + strDSN, CDatabase::noOdbcDialog); 
			
			s.m_lLineID = 0;
			s.m_strKey = _T ("Database::size");
			s.m_strData = strSize;

			if (!UpdateSettingsRecord (db, s))
				VERIFY (AddSettingsRecord (db, s));

			db.Close ();
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}
}

static CString FormatVersion (int nMajor, int nMinor)
{
	CString str;
	
	str.Format (_T ("%d.%02d"), nMajor, nMinor);
	return str;
}

DB_API bool FoxjetDatabase::Compact (const CString & strDBPath, const CString & strDSN, bool bForce)
{
	DECLARETRACECOUNT (20);
	const CString strKey = _T ("Software\\Foxjet\\Database\\") + strDSN;
	DWORD dwFileSize = 0;
	bool bCompact = false;
	CString strPath, strTitle;
	//CString strDBQ = FoxjetDatabase::GetProfileString (HKEY_LOCAL_MACHINE, _T ("SOFTWARE\\ODBC\\ODBC.INI\\") + strDSN, _T ("DBQ"), _T (""));
	CString strDBQ = FoxjetDatabase::ODBC::GetProfileString (strDSN, _T ("DBQ"));

	strDBQ.MakeLower ();
	TRACEF (strDBQ);

	if (strDBQ.Find (_T (".mdb")) != -1) {
		try {
			if (!AfxDaoGetEngine ())
				AfxDaoInit ();

			if (DAODBEngine * p = AfxDaoGetEngine ()) {
				BSTR bstr;

				p->get_Version (&bstr);
				CString str = CString (bstr);
				TRACER (_T ("FoxjetDatabase::Compact: DAO version: ") + str);

				if (!str.GetLength ())
					return false;
			}
		}
		catch (CDaoException * e) { HANDLEEXCEPTION_TRACEONLY (e);  return false; }
	}

	GetBackupPath (strPath, strTitle);

	{
		CString strDest = strPath + '\\' + strTitle;
		CCompactDlg dlg;
		CString strStatus, strProgress = _T ("Creating backup...");

		HRSRC hResource = ::FindResource(::hInstance, MAKEINTRESOURCE(IDD_COMPACT), RT_DIALOG);
		HGLOBAL hTemplate = LoadResource(::hInstance, hResource);
		VERIFY (dlg.CreateIndirect(hTemplate));
		FreeResource(hTemplate);
		
		dlg.ShowWindow (SW_SHOW);
		dlg.BringWindowToTop ();
		dlg.CenterWindow ();
		dlg.UpdateWindow ();

		if (::GetFileAttributes (strPath) == -1) {
			strStatus = _T ("CreateDirectory: ") + strPath;
			TRACER (strStatus);
			strProgress += _T ("\r\n") + strStatus; dlg.SetDlgItemText (TXT_TEXT, strProgress); dlg.UpdateWindow ();

			if (!::CreateDirectory (strPath, NULL)) {
				CString str;
				str.Format (LoadString (IDS_BACKUPDIRFAILED), strPath);
				MsgBox (str, MB_ICONERROR);
				return false;
			}
		}

		if (::GetFileAttributes (strDest) == -1) {
			strStatus = _T ("CopyFile: ") + strDBPath + _T (" --> ") + strDest;
			TRACER (strStatus);
			strProgress += _T ("\r\n") + strStatus; dlg.SetDlgItemText (TXT_TEXT, strProgress); dlg.UpdateWindow ();
			
			if (!::CopyFile (strDBPath, strDest, FALSE)) {
				CString str;
				str.Format (LoadString (IDS_BACKUPFAILED), strDest);
				MsgBox (str, MB_ICONERROR);
				return false;
			}

			return true;
		}
	}

	HANDLE hFile = ::CreateFile (strDBPath, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);

	if (hFile && hFile != INVALID_HANDLE_VALUE) {
		DWORD dwHighSize = 0;
		
		dwFileSize = ::GetFileSize (hFile, &dwHighSize);
		::CloseHandle (hFile);
	}

	{
		CString str = ::GetCommandLine ();

		str.MakeLower ();

		if (str.Find (_T ("/compact")) != -1)
			bForce = true;
	}

	if (bForce) 
		bCompact = true;
	else {		
		CString strSize = GetSize (strDSN);
		DWORD dwSize = _tcstoul (strSize, NULL, 16);

		if (!dwSize) {
			strSize.Format (_T ("%p"), dwFileSize);
			SetSize (strDSN, strSize); 
		}
		else {
			if (dwSize > 0 && dwFileSize > 0) {
				int n = (int)floor ((((double)dwFileSize - (double)dwSize) / (double)dwFileSize) * 100.0);

				if (n >= 25) {
					CString str;

					str.Format (LoadString (IDS_COMPACTDB), n);

					if (MsgBox (str, MB_YESNO | MB_ICONINFORMATION) == IDNO)
						return true;
					else 
						bCompact = true;
				}
				else 
					return true;
			}
		}
	}

	bool bResult = false;

	if (bCompact) {
		strTitle.Empty ();
		GetBackupPath (strPath, strTitle, _T (""), true);

		{
			CString str = GetDAOVersion ();
			int nMajor = 0, nMinor = 0;

			TRACEF (str);
			_stscanf (str, _T ("%d.%d"), &nMajor, &nMinor);

			bool bValid = FormatVersion (nMajor, nMinor) >= FormatVersion (3, 51); //(nMajor == 3 && nMinor >= 51);

			if (!bValid) {
				MsgBox (LoadString (IDS_WRONGDAOVERSION), MB_ICONERROR);
				return false;
			}
		}

		TCHAR szTitle [MAX_PATH] = { 0 };
		CString strDest = strDBPath;
		CString strBackup = strPath + '\\' + strTitle;
		CString strExt = _T (".") + FoxjetFile::GetFileExt (strDBPath);
		CCompactDlg dlg;
		CString strProgress = LoadString (IDS_COMPACT);

		HRSRC hResource = ::FindResource(::hInstance, MAKEINTRESOURCE(IDD_COMPACT), RT_DIALOG);
		HGLOBAL hTemplate = LoadResource(::hInstance, hResource);
		VERIFY (dlg.CreateIndirect(hTemplate));
		FreeResource(hTemplate);
		
		dlg.ShowWindow (SW_SHOW);
		dlg.BringWindowToTop ();
		dlg.CenterWindow ();
		dlg.UpdateWindow ();

		dlg.SetDlgItemText (TXT_TEXT, strProgress);  dlg.UpdateWindow ();

		{
			::GetFileTitle (strDBPath, szTitle, ARRAYSIZE (szTitle) - 1);
			CString strTitle (szTitle);
			
			if (strTitle.Find (strExt) == -1)
				strTitle += strExt;

			//strBackup.Replace (strTitle, _T ("backup_") + strTitle);
			strDest.Replace (strTitle, _T (""));
			strTitle.Replace (strExt, _T (""));
			strExt.Replace (_T ("."), _T (""));
			strDest = CreateTempFilename (strDest, strTitle, strExt);
		}

		MARKTRACECOUNT ();

		#ifdef _DEBUG
		int n = ::afxTraceFlags;
		::afxTraceFlags |= ::traceDatabase;
		#endif //_DEBUG

		try 
		{
			CString strStatus;

			strStatus =  _T ("CopyFile: ") + strDBPath + _T (" --> ") + strBackup;				
			MARKTRACECOUNT ();
			TRACER (strStatus);
			strProgress += _T ("\r\n") + strStatus; dlg.SetDlgItemText (TXT_TEXT, strProgress); dlg.UpdateWindow ();
			if (!::CopyFile (strDBPath, strBackup, FALSE)) {
				CString str;
				str.Format (LoadString (IDS_COPYFAILED), strDBPath, strBackup);
				MsgBox (str, MB_ICONERROR);
				return false;
			}

			{
				HANDLE hFile = ::CreateFile (strDBPath, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);

				if (hFile && hFile != INVALID_HANDLE_VALUE) {
					CTime time = CTime::GetCurrentTime ();
					SYSTEMTIME timeDest;
					FILETIME ft;

					time.GetAsSystemTime(timeDest);
					::SystemTimeToFileTime(&timeDest, &ft);
					::SetFileTime (hFile, &ft, NULL, NULL);
					::CloseHandle (hFile);
				}
			}

			if (!::DeleteFile (strDest)) {
			}
			MARKTRACECOUNT ();
			
			strStatus = _T ("CompactDatabase: ") + strDBPath + _T (" --> ") + strDest;
			TRACER (strStatus); 
			strProgress += _T ("\r\n") + strStatus; dlg.SetDlgItemText (TXT_TEXT, strProgress); dlg.UpdateWindow ();
	
//			if (!CompactDatabase (strDBPath, strDest))
//				CDaoWorkspace::CompactDatabase (strDBPath, strDest);								
			{
				bool bCompactDatabase = false;
			
				try
				{
					bCompactDatabase = CompactDatabase (strDBPath, strDest);
				}
				catch(_com_error &e) { HANDLEEXCEPTION_TRACEONLY (e); }

				if (!bCompactDatabase)
					CDaoWorkspace::CompactDatabase (strDBPath, strDest);
			}

			MARKTRACECOUNT ();

			strStatus = _T ("CopyFile: ") + strDest + " --> " + strDBPath;
			TRACER (strStatus); 
			strProgress += _T ("\r\n") + strStatus; dlg.SetDlgItemText (TXT_TEXT, strProgress); dlg.UpdateWindow ();
			if (!::CopyFile (strDest, strDBPath, FALSE)) {
				CString str;
				str.Format (LoadString (IDS_COPYFAILED), strDest, strDBPath);
				MsgBox (str, MB_ICONERROR);
				return false;
			}
			MARKTRACECOUNT ();

			strStatus = _T ("DeleteFile: ") + strDest;
			TRACER (strStatus);
			strProgress += _T ("\r\n") + strStatus; dlg.SetDlgItemText (TXT_TEXT, strProgress); dlg.UpdateWindow ();
			if (!::DeleteFile (strDest)) {
			}
			MARKTRACECOUNT ();

			{
				HANDLE hFile = ::CreateFile (strDBPath, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);

				if (hFile && hFile != INVALID_HANDLE_VALUE) {
					DWORD dwHighSize = 0;
					CString str;
					
					dwFileSize = ::GetFileSize (hFile, &dwHighSize);
					str.Format (_T ("%p"), dwFileSize);

					::CloseHandle (hFile);
					SetSize (strDSN, str);
				}
			}

			CleanupBackups (strPath);

			bResult = true;
		}
		catch (CDaoException * e)		{ HANDLEEXCEPTION (e); }
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		#ifdef _DEBUG
		::afxTraceFlags = n;
		#endif //_DEBUG
	}

	return bResult;
}

long double FoxjetDatabase::GetInkLowVolume (HEADTYPE head, INKTYPE ink)
{
	switch (head) {
	case UJ2_192_32NP:
		return FJSYS_MAX_INKLOW_UJ2_192_32NP_ML;
	}

	return FJSYS_MAX_INKLOW_ML;
}

DB_API CString FoxjetDatabase::GetDAOVersion ()
{
	CString str;
	
	try {
		if (!AfxDaoGetEngine ())
			AfxDaoInit ();

		if (DAODBEngine * p = AfxDaoGetEngine ()) {
			BSTR bstr;

			p->get_Version (&bstr);
			str = CString (bstr);

			if (str.Find (_T (".")) != -1) {
				const CString strTmp = _T ("x.xx");

				while (str.GetLength () < strTmp.GetLength ())
					str += _T ("0");
			}
		}
	}
	catch (CDaoException * e) { HANDLEEXCEPTION (e); }

	return str;
}

void CALLBACK COdbcDatabase::DisplayStatus (const CString & strDisplay, bool bDisplay)
{
	if (bDisplay)
		::vDisplay.Add (strDisplay);
	else {
		for (int i = ::vDisplay.GetSize () - 1; i >= 0; i--) 
			if (strDisplay.CompareNoCase (_T ("[purge]")) == 0 || strDisplay.CompareNoCase (::vDisplay [i]) == 0)
				::vDisplay.RemoveAt (i);
	}

	CString str;

	for (int i = 0; i < ::vDisplay.GetSize (); i++)
		str += ::vDisplay [i] + _T ("\r\n");

	//TRACEF (FoxjetDatabase::ToString (::vDisplay.GetSize ()) + _T (": ") + CString (bDisplay ? _T ("[display]\n") : _T ("[hide]\n")) + str);

	if (::vDisplay.GetSize () == 0) {
		if (::pdlg) {
			//::pdlg->ShowWindow (SW_HIDE);
			//::pdlg->CloseWindow ();
			delete ::pdlg;
			::pdlg = NULL;
		}
	}
	else {
		if (!::pdlg) {
			::pdlg = new CCompactDlg;

			//HINSTANCE hInstance = GetInstanceHandle (); 
			HRSRC hResource = ::FindResource (::hInstance, MAKEINTRESOURCE (IDD_STATUS), RT_DIALOG); 
			HGLOBAL hTemplate = LoadResource (::hInstance, hResource); 
			VERIFY (::pdlg->CreateIndirect (hTemplate)); 
			::FreeResource (hTemplate); 
			::Sleep (100);
		}

		::pdlg->SetDlgItemText (TXT_TEXT, str);  
		::pdlg->ShowWindow (SW_SHOW); 
		::pdlg->CenterWindow (); 
		::pdlg->UpdateWindow (); 
		::Sleep (1);
	}
}

int COdbcDatabase::FindTable (const CString & strDSN, const CString & strTable)
{
	LOCK (::csDatabaseCache);

	int nIndex = -1;

	try {
		COdbcCache db = COdbcDatabase::Find (strDSN);

		for (int i = 0; i < db.GetDB ().GetTableCount (); i++) {
			CString strName = db.GetDB ().GetTable (i)->GetName ();

			if (!strName.CompareNoCase (strTable)) {
				nIndex = i;
				break;
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }

	return nIndex;
}

int COdbcDatabase::FindField (const CString & strDSN, const CString & strTable, const CString & strField)
{
	LOCK (::csDatabaseCache);

	int nIndex = -1;

	try {
		COdbcCache db = COdbcDatabase::Find (strDSN);

		int nTable = FindTable (strDSN, strTable);

		if (nTable != -1) {
			CString strSQL = _T ("SELECT * FROM [") + strTable + _T ("]");
			COdbcCache rst = COdbcDatabase::Find (db.GetDB (), strDSN, strSQL);
			CString strKey = strDSN + _T (":") + strSQL;

			if (!::vFieldIndex.Lookup (strKey, (void *&)nIndex)) {
//				TRACEF (strKey);

//				if (rst.Open (strSQL, CRecordset::forwardOnly)) {
					for (int i = 0; i < rst.GetRst ().GetODBCFieldCount (); i++) {
						CODBCFieldInfo info;

						rst.GetRst ().GetODBCFieldInfo (i, info);
						CString strName = info.m_strName;

						if (!strName.CompareNoCase (strField)) {
							nIndex = i;
							::vFieldIndex.SetAt (strKey, (void *&)nIndex);
							break;
						}
					}

//					rst.Close ();
//				}
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }

	return nIndex;
}

bool COdbcDatabase::IsOracle ()
{
	CString strDriver = GetDriver ();

	return strDriver.CompareNoCase (_T ("SQORA32.DLL")) == 0;
}

bool COdbcDatabase::IsSqlServer (const CString & strDriver)
{
	static const LPCTSTR lpsz [] =
	{
		_T ("sqlncli.dll"),
		_T ("sqlncli10.dll"),
		_T ("SQLSRV32.dll"),
		_T ("sqlncli11.dll"),
	};
	CString str = strDriver;

	if (str.Find (_T ("\\")) != -1) {
		CString s = str;

		s.Replace (_T ("\\"), _T ("/"));
	
		std::vector <std::wstring> v = explode (std::wstring (s), '/');

		if (v.size ())
			str = v.back ().c_str ();
	}

	for (int i = 0; i < ARRAYSIZE (lpsz); i++)
		if (str.CompareNoCase (lpsz [i]) == 0)
			return true;

	return false;
}

bool COdbcDatabase::IsSqlServer ()
{
	return IsSqlServer (GetDriver ());
}

bool COdbcDatabase::IsProgress ()
{
	CString str = GetDriver ();

	for (int i = 0; i <= 9; i++)
		str.Replace (CString ((TCHAR)('0' + i)), _T (""));

	return str.CompareNoCase (_T ("pgoe.dll")) == 0 ? true : false;
}

bool COdbcDatabase::IsQunect ()
{
	CString strDriver = GetDriver ();
	static const LPCTSTR lpsz [] =
	{
		_T ("qdbodbc.dll"),
	};

	for (int i = 0; i < ARRAYSIZE (lpsz); i++)
		if (strDriver.CompareNoCase (lpsz [i]) == 0)
			return true;

	return false;
}

bool COdbcDatabase::IsSage ()
{
	CString str = Extract (GetConnect (), _T ("Description="), _T (";"));
	static const LPCTSTR lpsz [] =
	{
		_T ("MAS 90 4.0"),
	};

	str.MakeUpper ();

	for (int i = 0; i < ARRAYSIZE (lpsz); i++)
		if (str.Find (lpsz [i]) != -1)
			return true;

	return false;
}

UINT COdbcDatabase::GetOpenType ()
{
	return (IsQunect () || IsSage ()) ? CRecordset::forwardOnly : CRecordset::snapshot;
}


CString COdbcDatabase::CreateSQL (FoxjetDatabase::COdbcDatabase * pdb,
								  const CString & strDSN, const CString & strTable, const CString & strField,
								  const CString & strKeyField, const CString & strKeyValue, 
								  int nSQLType, bool bSW0867)
{
	CString strSQL = _T ("SELECT * FROM [") + strTable + _T ("] ");

	if (strKeyValue.GetLength () || strKeyField.GetLength ()) {
		CString strDelim, strTmp;

		switch (nSQLType) { 
			case SQL_DATE:
			case SQL_TIME:
			case SQL_TIMESTAMP:
				strDelim = _T ("#");

				if (pdb) {
					if (pdb->IsOracle () || pdb->IsSqlServer ()) {
						strDelim = _T ("'");
					}
				}
				break;
			//case SQL_CHAR:
			//case SQL_VARCHAR:
			//case SQL_LONGVARCHAR:
			//case SQL_WCHAR: // synonymous with SQL_C_WCHAR
			//case SQL_WVARCHAR:
			//case SQL_WLONGVARCHAR:
			//	strDelim = _T ("\'");
			//	break;
			default:
				if (IsSqlString (nSQLType))
					strDelim = _T ("'");
				break;
		}

		//strSQL += _T (" WHERE [") + strKeyField + _T ("]=") + strDelim + strKeyValue + strDelim;
		strTmp.Format (_T (" WHERE [%s]=%s%s%s"), strKeyField, strDelim, strKeyValue, strDelim);
		strSQL += strTmp;
	}

	if (bSW0867) { // sw0867
		SETTINGSSTRUCT s;
		CStringArray v;
		CString strDate;
		CTime tmNow = CTime::GetCurrentTime ();

		if (FoxjetDatabase::IsSelectEffectivityDate ()) 
			tmNow = FoxjetDatabase::GetEffectivityDate ();

		GetSettingsRecord (* GetDB (::GetCurrentThreadId ()), 0, "sw0858", s);
		FoxjetDatabase::FromString (s.m_strData, v);
	
		int i = 0;
		CString strDbDSN		= GetParam (v, i++);
		CString strDbTable		= GetParam (v, i++);
		CString strDbKeyField	= GetParam (v, i++);
		CString strDbTaskField	= GetParam (v, i++);
		int nDbSQLType			= _ttoi (GetParam (v, i++));
		CString strDbStartField	= GetParam (v, i++);
		CString strDbEndField	= GetParam (v, i++);
		CString strDbOffset		= GetParam (v, i++);

		if (!strDSN.CompareNoCase (strDbDSN) && 
			!strTable.CompareNoCase (strDbTable)) 
		{
			ASSERT (pdb);

			if (pdb->IsSqlServer ()) 
				strDate.Format (_T (" AND [%s].[%s]<='%s' AND [%s].[%s]>='%s'"),
					strDbTable, strDbStartField, tmNow.Format (_T ("%m/%d/%Y")),
					strDbTable, strDbEndField, tmNow.Format (_T ("%m/%d/%Y")));
			else
				strDate.Format (_T (" AND [%s].[%s]<=#%s# AND [%s].[%s]>=#%s#"),
					strDbTable, strDbStartField, tmNow.Format (_T ("%m/%d/%Y")),
					strDbTable, strDbEndField, tmNow.Format (_T ("%m/%d/%Y")));

#ifdef _DEBUG
			if (pdb->IsSqlServer ()) 
				strDate.Format (_T (" AND [%s].[%s]<='%s' AND [%s].[%s]>='%s'"),
					strDbTable, strDbStartField, tmNow.Format (_T ("%m/%d/%Y")),
					strDbTable, strDbEndField, tmNow.Format (_T ("%m/%d/%Y")));
			else
				strDate.Format (_T (" AND [%s].[%s]<=#%s# AND [%s].[%s]>=#%s#"),
					strDbTable, strDbStartField, tmNow.Format (_T ("%m/%d/%Y")),
					strDbTable, strDbEndField, tmNow.Format (_T ("%m/%d/%Y")));
#endif //_DEBUG

			strSQL += strDate;
			TRACEF (strSQL);
		}
	}

	return strSQL;
}

////////////////////////////////////////////////////////////////////////////////
COdbcCache::COdbcCache (COdbcDatabase * p, const CString & strKey)
:	m_pDB (p),
	m_pRst (NULL),
	m_strKey (strKey)
{
	p->m_nActive++;
}

COdbcCache::COdbcCache (COdbcRecordset * p, const CString & strKey)
:	m_pDB (NULL),
	m_pRst (p),
	m_strKey (strKey)
{
	p->m_nActive++;
}

COdbcCache::COdbcCache (const COdbcCache & rhs)
:	m_pDB (rhs.m_pDB),
	m_pRst (rhs.m_pRst),
	m_strKey (rhs.m_strKey)
{
}

COdbcCache & COdbcCache::operator = (const COdbcCache & rhs)
{
	if (this != &rhs) {
		m_pDB		= rhs.m_pDB;
		m_pRst		= rhs.m_pRst;
		m_strKey	= rhs.m_strKey;
	}
	
	return * this;
}

COdbcCache::~COdbcCache ()
{
	if (m_pDB)
		m_pDB->m_nActive--;

	if (m_pRst)
		m_pRst->m_nActive--;
}

COdbcDatabase & COdbcCache::GetDB () 
{ 
	ASSERT (m_pDB); 
	return * m_pDB; 
}

COdbcRecordset & COdbcCache::GetRst () 
{ 
	ASSERT (m_pRst); 
	return * m_pRst; 
}

int COdbcCache::GetActiveCount () const
{
	if (m_pDB)
		return m_pDB->m_nActive;
	else if (m_pRst)
		return m_pRst->m_nActive;
	else
		return 0;
}

CString COdbcCache::GetDSN () const 
{ 
	return !m_pDB ? _T ("") : FoxjetDatabase::Extract (m_pDB->GetConnect (), _T ("DSN="), _T (";")); 
} 

////////////////////////////////////////////////////////////////////////////////

LPDISPLAYSTATUS COdbcDatabase::m_lpFct = NULL;

void COdbcDatabase::RegisterDisplayFct (LPDISPLAYSTATUS lpFct)
{
	COdbcDatabase::m_lpFct = lpFct;
}


bool COdbcDatabase::OpenDB (COdbcDatabase * p, const CString & strDsnIn)
{
	CString strDSN = strDsnIn;

	::gmapDsn.Lookup (strDsnIn, strDSN);

	if (OpenDB (p, strDSN, 0)) 
		return true;
	else {
		if (OpenDB (p, strDsnIn + ::gstrLocal, 0)) {
			::gmapDsn.SetAt (strDsnIn, strDsnIn + ::gstrLocal);

			return true;
		}
	}

	return false;
}

bool COdbcDatabase::OpenDB (COdbcDatabase * p, const CString & strDsnIn, int)
{
	static CMapStringToString map;
	CStringArray vConnect;
	CString strDSN = strDsnIn;

	{
		CString str;

		if (map.Lookup (strDSN, str)) {
			TRACEF (strDSN + _T (" --> ") + str);
			vConnect.Add (str);
		}
	}

	vConnect.Add (_T (";DSN=") + strDSN + _T (";"));
	vConnect.Add (_T (";DSN=") + strDSN);
	vConnect.Add (_T ("DSN=") + strDSN);
	vConnect.Add (_T ("DSN=") + strDSN + _T (";"));
	//vConnect.Add (strDSN);

	int nTries = 0;
	const CString strKey = _T ("Software\\Foxjet\\MarksmanELITE\\ODBC\\") + strDSN;

	{
		CString strUID = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("UID"), _T ("")); 
		CString strPWD = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("PWD"), _T ("")); 

		if (strUID.GetLength () && strPWD.GetLength ()) {
			for (int nLevel = 0; nLevel < vConnect.GetCount (); nLevel++) {
				CString str = _T ("UID=") + strUID + _T (";PWD=") + strPWD + _T (";");

				if (vConnect [nLevel] [vConnect [nLevel].GetLength () - 1] != ';')
					vConnect [nLevel] += ';';

				vConnect [nLevel] += str;
			}
		}
	}

	for (int nLevel = 0; nLevel < vConnect.GetCount (); nLevel++) {
		try
		{
//			DWORD dwOptions = CDatabase::noOdbcDialog; // CDatabase::useCursorLib;
			p->SetLoginTimeout (5);
			TRACEF (vConnect [nLevel]);
			VERIFY (p->OpenEx (vConnect [nLevel], CDatabase::noOdbcDialog)); //VERIFY (p->Open (vConnect [nLevel], FALSE, TRUE, _T("ODBC;"), FALSE));
			TRACEF (strDSN + _T (": ") + p->GetDriver ());
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("connect"), p->GetConnect ()); 
			map.SetAt (strDSN, vConnect [nLevel]);
			
			return true;
		}
		catch (CDBException * e)		
		{ 
			HKEY h [] = { HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE };
			bool bTryAgain = false;

			TRACE_SQLERROR ();

			VERIFY (EnablePrivilege (SE_RESTORE_NAME));
			VERIFY (EnablePrivilege (SE_BACKUP_NAME));

			for (int i = 0; i < ARRAYSIZE (h); i++) {
				const CString strKey = _T ("SOFTWARE\\ODBC\\ODBC.INI\\") + strDSN;
				CString strFIL = FoxjetDatabase::GetProfileString (h [i], strKey, _T ("FIL"), _T (""));

				strFIL.MakeLower ();

				if (strFIL.Find (_T ("excel")) != -1) {
					BYTE nReadOnly = FoxjetDatabase::GetProfileByte (h [i], strKey, _T ("ReadOnly"), -1);

					if ((nReadOnly != (BYTE)-1)) {
						nReadOnly = nReadOnly ? 0 : 1;
						FoxjetDatabase::WriteProfileByte (h [i], strKey, _T ("ReadOnly"), nReadOnly);
						TRACEF (ToString (FoxjetDatabase::GetProfileByte (h [i], strKey, _T ("ReadOnly"), -1)));
						bTryAgain = true;
					}
				}
			}

			if (bTryAgain && nTries++ < 2) {
				TRACEF (e->m_strError); 
				e->Delete (); 
				nLevel--;
				continue;
			}

			nTries = 0;

			if (nLevel >= (vConnect.GetSize () - 1)) {
				bool bTraceOnly = false;

				e->m_strError = _T ("[") + strDSN + _T ("]\n\n") + e->m_strError;

				if (strDSN.Find (::gstrLocal) == -1) {
					CString strLocalKey = _T ("Software\\ODBC\\ODBC.INI\\") + strDSN + ::gstrLocal;
					CString strLocalDBQ = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strLocalKey, _T ("DBQ"), _T (""));

					TRACEF (strLocalKey + _T (": ") + strLocalDBQ);

					//if (strLocalDBQ.GetLength ())
					//	bTraceOnly = true;
				}
				
				if (bTraceOnly)
					HANDLEEXCEPTION_TRACEONLY (e); 
				else {
					for (int i = 0; i < vConnect.GetSize (); i++)
						TRACEF (vConnect [i]);

					HANDLEEXCEPTION (e); 
				}
			}
			else {
				TRACEF (e->m_strError); 
				e->Delete (); 
				//HANDLEEXCEPTION_TRACEONLY (e); 
			}
		}
		catch (CMemoryException * e) { HANDLEEXCEPTION (e); }
	}

	return false;
}

void COdbcDatabase::CloseDB (COdbcDatabase * pDB)
{
	LOCK (::csDatabaseCache);

	if (pDB) {
		pDB->Close ();
		TRACEF ("Close: " + pDB->GetConnect ());
	}

	/*
	for (int i = 0; i < ::vDbCache.GetSize (); i++) {
		if (COdbcDatabase * p = ::vDbCache [i]) {
			if (p == pDB) {
				::vDbCache.RemoveAt (i);
				return;
			}
		}
	}
	*/
}


COdbcCache COdbcDatabase::Find (const CString & strDSN)
{
	LOCK (::csDatabaseCache);

	const CString strFind = _T ("DSN=") + strDSN + _T (";");

	for (int i = 0; i < ::vDbCache.GetSize (); i++) {
		if (COdbcDatabase * p = ::vDbCache [i]) {
			if (p->GetConnect ().Find (strFind) != -1) {
				return COdbcCache (p, strDSN);
			}
		}
	}


	const CString strStatus = LoadString (IDS_OPENING) + GetDsn (strDSN) + _T ("...");
	COdbcDatabase * p = new COdbcDatabase (_T (__FILE__), __LINE__);

	try {
		/*
		CString strKey = _T ("Software\\Foxjet\\MarksmanELITE\\ODBC\\") + strDSN;
		CString str = strDSN;
		CString strUID = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("UID"), _T ("")); 
		CString strPWD = FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, strKey, _T ("PWD"), _T ("")); 

		if (strUID.GetLength () && strPWD.GetLength ())
			str += _T (";UID=") + strUID + _T (";PWD=") + strPWD + _T (";");
		*/

		if (m_lpFct) 
			(* m_lpFct) (strStatus, true);

		/*
		VERIFY (p->Open (str, FALSE, TRUE));
		//VERIFY (p->OpenEx (_T (";DSN=") + strDSN + _T (";"), CDatabase::noOdbcDialog));
		TRACEF (strDSN + _T (": ") + p->GetDriver ());
		*/

		if (OpenDB (p, strDSN))
			::vDbCache.Add (p);
	}
	catch (CDBException * e)		
	{ 
		e->m_strError = _T ("[") + strDSN + _T ("]\n\n") + e->m_strError;
		HANDLEEXCEPTION (e); 

		/*
		CDBException * pCopy = new CDBException ();

		pCopy->m_nRetCode				= e->m_nRetCode;
		pCopy->m_strError				= strDSN + _T ("\n\n") + e->m_strError;
		pCopy->m_strStateNativeOrigin	= e->m_strStateNativeOrigin;

		HANDLEEXCEPTION_TRACEONLY (pCopy); 
		throw (e); 
		*/
	}
	catch (CMemoryException * e)	
	{ 
		HANDLEEXCEPTION (e);
		/*
		HANDLEEXCEPTION_TRACEONLY (e); 
		throw (new CMemoryException ()); 
		*/
	}

	if (m_lpFct) 
		(* m_lpFct) (strStatus, false);

	return COdbcCache (p, strDSN);
}

#define TRACECACHE() \
{ \
	LOCK (::csDatabaseCache); \
	CString str; \
	int i = 0; \
	\
	str.Format (_T ("vRstCache (%d): 0x%p"), (::vRstCache.GetCount ()), &(::vRstCache)); \
	\
	for (POSITION pos = ::vRstCache.GetStartPosition (); pos; ) { \
		COdbcRecordset * p = NULL; \
		CString strSQL, strTmp, strSQLRst; \
		\
		::vRstCache.GetNextAssoc (pos, strSQL, (void *&)p); \
		strSQLRst = (p ? p->GetDefaultSQL () : "NULL"); \
		strSQL.Replace (_T ("\n"), _T ("")); \
		strSQL.Replace (_T ("\r"), _T ("")); \
		strSQLRst.Replace (_T ("\n"), _T ("")); \
		strSQLRst.Replace (_T ("\r"), _T ("")); \
		strTmp.Format (_T ("\n\t[%d, 0x%p]: [0x%p] %s [%s]"), i, pos, p, strSQL, strSQLRst); \
		str += strTmp; \
		i++; \
	} \
	\
	TRACEF (str); \
}


COdbcCache COdbcDatabase::Find (COdbcDatabase & db, const CString & strSQL)
{
	CString strDSN = Extract (db.GetConnect (), _T ("DSN="), _T (";"));

	return COdbcDatabase::Find (db, strDSN, strSQL);
}

static CString FormatKey (CString strKey)
{
	strKey.Replace (_T ("\r"), _T (""));
	strKey.Replace (_T ("\n"), _T (""));

	strKey.TrimLeft ();
	strKey.TrimRight ();

	int nLen = strKey.GetLength () - 1;

	if (nLen > 0)
		if (strKey [nLen] == ';')
			strKey.Delete (nLen);

	return strKey;
}

COdbcCache COdbcDatabase::Find (COdbcDatabase & db, const CString & strDSN, const CString & strSQLFind)
{
	LOCK (::csDatabaseCache);

	COdbcRecordset * p = NULL;
	CString strSQL = db.FormatSQL (strSQLFind);
	//CString strKey = db.GetDatabaseName () + _T (":") + strSQL;
	CString strKey = FormatKey (strDSN + _T (":") + strSQL);

	//TRACECACHE ();

	//TRACEF ("Find: \"" + strKey + "\"");

	//TRACETHREADSTATE ();

	if (!::vRstCache.Lookup (strKey, (void *&)p)) {
		const CString strStatus = LoadString (IDS_OPENING) + GetDsn (strDSN) + _T (": ") + strSQL + _T ("...");
	
		if (m_lpFct) 
			(* m_lpFct) (strStatus, true);
	
		p = new COdbcRecordset (db);

		try {
			DWORD dwOptions = CRecordset::readOnly | CRecordset::noDirtyFieldCheck;

			VERIFY (p->Open (strSQL, db.GetOpenType (), dwOptions));
			//{ CString str; str.Format ("[proc: 0x%p, thread: 0x%p, hInst: 0x%p] Opened: %s", ::GetCurrentProcessId (), ::GetCurrentThreadId (), GetInstanceHandle (), strSQL); TRACEF (str); }

			if (p->GetDefaultSQL () != strSQL) {
				TRACEF ("***** key changed: " + p->GetDefaultSQL () + " [" + strKey + "]");
				strKey = strDSN + _T (":") + p->GetDefaultSQL ();
				TRACEF (strKey);
			}

			::vRstCache.SetAt (strKey, (void *&)p);
			//TRACECACHE ();
			//int n = 0;
		}
		catch (CDBException * e)		{ if (m_lpFct) (* m_lpFct) (strStatus, false); delete p; throw (e); }
		catch (CMemoryException * e)	{ if (m_lpFct) (* m_lpFct) (strStatus, false); delete p; throw (e); }

		if (m_lpFct) 
			(* m_lpFct) (strStatus, false);
	}

	ASSERT (p);	

	return COdbcCache (p, strKey);
}

bool COdbcDatabase::FreeRstCache (const CString & strKeyRemove)
{
	LOCK (::csDatabaseCache);
	CString strKey = FormatKey (strKeyRemove);

	//TRACETHREADSTATE ();

	if (strKey.IsEmpty ()) {
		for (POSITION pos = ::vRstCache.GetStartPosition (); pos; ) {
			COdbcRecordset * p = NULL;
			CString str;
		
			::vRstCache.GetNextAssoc (pos, str, (void *&)p);

			if (p) {
				try 
				{
					p->Close ();
					delete p;
				}
				catch (CDBException * e)		{ HANDLEEXCEPTION /* _TRACEONLY */ (e); }
				catch (CMemoryException * e)	{ HANDLEEXCEPTION /* _TRACEONLY */ (e); }
			}
		}

		::vRstCache.RemoveAll ();
		::vFieldIndex.RemoveAll ();
		return true;
	}
	else {
		for (POSITION pos = ::vRstCache.GetStartPosition (); pos; ) {
			COdbcRecordset * p = NULL;
			CString str;
		
			::vRstCache.GetNextAssoc (pos, str, (void *&)p);

			if (p) {
				//TRACEF (strKey);
				//TRACEF (str);
				//TRACEF (p->GetDSN () + _T (":") + p->GetSQL ());
				//ASSERT (p->GetSQL () == str);
				//CString strSQL = FormatKey (p->GetDSN () + _T (":") + p->GetSQL ());
				//CString strSQLDefault = FormatKey (p->GetDSN () + _T (":") + p->GetDefaultSQL ());

				if (!strKey.CompareNoCase (str)) { // || !strKey.CompareNoCase (strSQL) || !strKey.CompareNoCase (strSQLDefault)) {
					if (p->GetActive () == 0) {
						try 
						{
							//TRACEF (ToString (p->GetActive ()));
							//TRACEF (_T ("**************************************** ::vRstCache.RemoveKey: ") + str);
							VERIFY (::vRstCache.RemoveKey (str));
							p->Close ();
							delete p;
							return true;
						}
						catch (CDBException * e)		{ HANDLEEXCEPTION /* _TRACEONLY */ (e); }
						catch (CMemoryException * e)	{ HANDLEEXCEPTION /* _TRACEONLY */ (e); }
					}
				}
			}
		}

		//TRACEF (strKey);
		//TRACECACHE ();
		//int n = 0;
	}

	return false;
}

void COdbcDatabase::PurgeDsnCache (const CString & strExceptDSN)
{
	LOCK (::csDatabaseCache);

	//TRACETHREADSTATE ();

	if (::vDbCache.GetSize () > 1) {
		int nRstCache = ::vRstCache.GetSize ();
		int nFieldIndex = ::vFieldIndex.GetSize ();
		int nDbCache = ::vDbCache.GetSize ();

		//TRACECACHE ()

		for (int i = ::vDbCache.GetSize () - 1; i > 0; i--) {
			if (COdbcDatabase * pDB = ::vDbCache [i]) {
				if (pDB->m_nActive <= 0) {
					CString strDSN = FoxjetDatabase::Extract (pDB->m_strConnect, _T ("DSN="), _T (";"));

					if (strExceptDSN.CompareNoCase (strDSN) != 0) {
						const CString strKey = strExceptDSN + _T (":");

						for (POSITION pos = ::vRstCache.GetStartPosition (); pos; ) {
							COdbcRecordset * pRst = NULL;
							CString str;
		
							::vRstCache.GetNextAssoc (pos, str, (void *&)pRst);

							if (pRst) {
								if (str.Find (strKey) == -1) {
									if (pRst->GetActive () <= 0) {
										try 
										{
											TRACEF (str);
											::vRstCache.RemoveKey (str);
											::vFieldIndex.RemoveKey (str);
											pRst->Close ();
											delete pRst;
										}
										catch (CDBException * e)		{ HANDLEEXCEPTION /* _TRACEONLY */ (e); }
										catch (CMemoryException * e)	{ HANDLEEXCEPTION /* _TRACEONLY */ (e); }
									}
								}
							}
						}

						try {
							#ifdef _DEBUG
							CDebug::Trace (pDB->GetConnect (), true, pDB->m_strFile, pDB->m_lLine);
							#endif 

							::vDbCache.RemoveAt (i);
							pDB->Close ();
							delete pDB;
						}
						catch (CDBException * e)		{ HANDLEEXCEPTION /* _TRACEONLY */ (e); }
						catch (CMemoryException * e)	{ HANDLEEXCEPTION /* _TRACEONLY */ (e); }
					}
				}
			}
		}

		if (nRstCache != ::vRstCache.GetSize () ||
			nFieldIndex != ::vFieldIndex.GetSize () ||
			nDbCache != ::vDbCache.GetSize ()) 
		{
			TRACECACHE ()
		}
	}
}

void COdbcDatabase::FreeDsnCache ()
{
	LOCK (::csDatabaseCache);

	FreeRstCache ();

	for (int i = 0; i < ::vDbCache.GetSize (); i++) {
		if (COdbcDatabase * p = ::vDbCache [i]) {
			#ifdef _DEBUG
			CDebug::Trace (p->GetConnect (), true, p->m_strFile, p->m_lLine);
			#endif 

			try {
				p->Close ();
				delete p;
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION /* _TRACEONLY */ (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION /* _TRACEONLY */ (e); }
		}
	}

	::gmapDsn.RemoveAll ();
	::vDbCache.RemoveAll ();
}

bool COdbcDatabase::IsLocal ()
{
	return ::gmapDsn.GetSize () > 0 ? true : false;
}



int COdbcDatabase::ExecuteSQL (LPCTSTR lpszSQL)
{
	CString strSQL = FormatSQL (lpszSQL);
//	CDatabase::ExecuteSQL (FormatSQL (COdbcVariant::FormatUnicode (lpszSQL)));
	RETCODE nRetCode;
	HSTMT hstmt;
	SQLINTEGER nResult = 0;

	//TRACER (strSQL);

	ENSURE_VALID(this);
	ENSURE_ARG(AfxIsValidString(strSQL));

	AFX_SQL_SYNC(::SQLAllocStmt(m_hdbc, &hstmt));
	if (!CheckHstmt(nRetCode, hstmt))
		AfxThrowDBException(nRetCode, this, hstmt);

	TRY
	{
		OnSetOptions(hstmt);

		// Give derived CDatabase classes option to use parameters
		BindParameters(hstmt);

		LPTSTR pszSQL = const_cast<LPTSTR>((LPCTSTR)strSQL);
		AFX_ODBC_CALL(::SQLExecDirect(hstmt, reinterpret_cast<SQLTCHAR *>(pszSQL), SQL_NTS));
		if (!CheckHstmt(nRetCode, hstmt))
			AfxThrowDBException(nRetCode, this, hstmt);

		AFX_ODBC_CALL(::SQLRowCount (hstmt, &nResult));

		SWORD nResultColumns;
		do
		{
			AFX_ODBC_CALL(::SQLNumResultCols(hstmt, &nResultColumns));
			if(!CheckHstmt(nRetCode, hstmt))
				AfxThrowDBException(nRetCode, this, hstmt);

			if (nResultColumns != 0)
				do
				{
					AFX_ODBC_CALL(::SQLFetch(hstmt));
					if(!CheckHstmt(nRetCode, hstmt))
						AfxThrowDBException(nRetCode, this, hstmt);
				} while (nRetCode != SQL_NO_DATA_FOUND);
			AFX_ODBC_CALL(::SQLMoreResults(hstmt));
			if(!CheckHstmt(nRetCode, hstmt))
				AfxThrowDBException(nRetCode, this, hstmt);
		} while (nRetCode != SQL_NO_DATA_FOUND);
	}
	CATCH_ALL(e)
	{
		::SQLCancel(hstmt);
		AFX_SQL_SYNC(::SQLFreeStmt(hstmt, SQL_DROP));
		THROW_LAST();
	}
	END_CATCH_ALL

	AFX_SQL_SYNC(::SQLFreeStmt(hstmt, SQL_DROP));

	GetTables ();

	return nResult;
}

CString COdbcDatabase::GetSqlServerDatabaseName ()
{
	CString str = Extract (GetConnect (), _T ("DATABASE="), _T (";"));

	if (!str.GetLength ())
		str = Extract (GetConnect (), _T ("DATABASE="));

	if (!str.GetLength ())
		str = Extract (GetConnect (), _T ("Description="), _T (";"));

	return str;
}

CString COdbcDatabase::FormatSQL (const CString & strSQL)
{
	CString strResult (strSQL);

	if (IsSqlServer ()) {
		CString strDSN;
		
		strDSN.Format (_T ("[%s].[dbo]."), GetSqlServerDatabaseName ());
		
		strResult.Replace (_T ("DELETE *"), _T ("DELETE "));
		strResult.Replace (_T ("=True"), _T ("='True'"));
		strResult.Replace (_T ("= True"), _T ("='True'"));
		strResult.Replace (_T ("=False"), _T ("='False'"));
		strResult.Replace (_T ("= False"), _T ("='False'"));
		strResult.Replace (_T ("CStr"), _T (""));

		ASSERT (FindNoCase (strResult, _T ("DELETE *")) == -1);
		ASSERT (FindNoCase (strResult, _T ("CStr")) == -1);

		for (int i = 0; i < GetTableCount (); i++) {
			if (const COdbcTable * p = GetTable (i)) {
				CString strName = _T ("[") + p->GetName () + _T ("]");
				int nIndex = 0;

				while ((nIndex = strResult.Find (strName, nIndex)) != -1) {
					int nLen = strName.GetLength () + strDSN.GetLength ();
					bool bInsert = true;

					if ((nIndex - strDSN.GetLength ()) >= 0) {
						CString str = strResult.Mid (nIndex - nLen, nLen);
						
						bInsert = str.Find (strDSN) == -1;
					}

					if (bInsert) {
						strResult.Insert (nIndex, strDSN);
						nIndex += strDSN.GetLength ();
					}

					nIndex += strName.GetLength ();
				}
			}
		}
	}


	if (IsProgress ()) {
		const CString strPrefix = _T ("[PUB].");

		for (int i = 0; i < GetTableCount (); i++) {
			if (const COdbcTable * p = GetTable (i)) {
				CString strName = _T ("[") + p->GetName () + _T ("]");
				int nIndex = 0;

				while ((nIndex = strResult.Find (strName, nIndex)) != -1) {
					int nLen = strName.GetLength () + strPrefix.GetLength ();
					bool bInsert = true;

					if ((nIndex - strPrefix.GetLength ()) >= 0) {
						CString str = strResult.Mid (nIndex - nLen, nLen);
						
						bInsert = str.Find (strPrefix) == -1;
					}

					if (bInsert) {
						strResult.Insert (nIndex, strPrefix);
						nIndex += strPrefix.GetLength ();
					}

					nIndex += strName.GetLength ();
				}
			}
		}

		#ifdef _DEBUG
		if (strResult.Find (_T ("SELECT")) != -1 && strResult.Find (_T ("WHERE")) == -1) {
			if (strResult.Find (_T ("SELECT TOP 10")) == -1) {
				strResult.Replace (_T ("SELECT"), _T ("SELECT TOP 10"));
			}
		}
		#endif
	}

	return strResult;
}

////////////////////////////////////////////////////////////////////////////////

DB_API void FoxjetDatabase::NotifySystemParamChange (COdbcDatabase & db)
{
	CArray <PRINTERSTRUCT, PRINTERSTRUCT &> vPrinters;
	LPARAM lParam = 0;

	if (CWnd * p = ::AfxGetMainWnd ())
		lParam = (LPARAM)p->m_hWnd;

	GetPrinterRecords (db, vPrinters);
	
	if (HWND hwnd = FindEditor ())
		::PostMessage (hwnd, WM_SYSTEMPARAMCHANGE, 0, lParam);

	CString strCmdLine = ::GetCommandLine ();
	strCmdLine.MakeUpper ();
	bool bNetworked = (_tcsstr (strCmdLine, _T ("/NETWORKED")) != NULL ? true : false) || FoxjetDatabase::IsNetworked ();

	{
		TCHAR sz [MAX_PATH] = { 0 };

		GetModuleFileName (NULL, sz, ARRAYSIZE (sz));
		CString str = sz;

		str.MakeLower ();

		if (str.Find (_T ("mkdraw.exe")) != -1) 
			if (HWND hwnd = FindControl ())
				::PostMessage (hwnd, WM_SYSTEMPARAMCHANGE, 0, lParam);
	}

	/*
	bool bNetworked = vPrinters.GetSize () > 0;

	::PostMessage (HWND_BROADCAST, WM_SYSTEMPARAMCHANGE, 0, lParam);
	*/

	if (bNetworked) 
	{
		CAsyncSocket sock;
		int nFlag, nPort = BW_HOST_BROADCAST_PORT;
		int nSockType = SOCK_DGRAM;
		CStringArray v, vResult;
		CString strInstance = ToString ((ULONG)lParam);
		CString strIpAddr = FoxjetDatabase::GetIpAddress ();

		v.Add (BW_HOST_SYSTEMPARAMCHANGE);
		v.Add (strInstance);
		v.Add (strIpAddr);
		CString str = FoxjetDatabase::ToString (v);

		TRACEF (str);

		if (!sock.Create (nPort, nSockType)) {
			nPort = 0;
			VERIFY (sock.Create (nPort, nSockType));
		}

		nFlag = 1;
		VERIFY (sock.SetSockOpt (SO_BROADCAST, &nFlag, sizeof (int)));

		nFlag = 1;
		VERIFY (sock.SetSockOpt (SO_REUSEADDR, &nFlag, sizeof (int)));

		SOCKADDR_IN remoteAddr;
		remoteAddr.sin_family = AF_INET;
		remoteAddr.sin_port = htons (BW_HOST_BROADCAST_PORT);
		remoteAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);

		DWORD dwBytes = sock.SendTo (w2a (str), str.GetLength (), (const SOCKADDR* )&remoteAddr, sizeof(remoteAddr));
	}
}

static CString gstrUser = _T ("");

void DB_API FoxjetDatabase::SetUsername (const CString & str)
{
	::gstrUser = str;
}

CString DB_API FoxjetDatabase::GetUsername ()
{
	if (::gstrUser.GetLength ())
		return ::gstrUser;

	CString strCmdLine = ::GetCommandLine () + CString (_T (" "));
	
	::gstrUser = Extract (strCmdLine, _T ("/user=\""), _T ("\""));

	if (::gstrUser.GetLength ())
		::gstrUser = Extract (strCmdLine, _T ("/user="), _T (" "));

	return ::gstrUser;
}


bool DB_API FoxjetDatabase::LoginPermits (FoxjetDatabase::COdbcDatabase & db, const CString & strUser, UINT nID)
{
	bool bAllow = false;

	if (!strUser.CompareNoCase (_T ("root")))
		return true;

	try {
		CString strSQL;
		COdbcRecordset rst (db);

		strSQL.Format (_T ("SELECT [%s].*, [%s].* FROM ([%s] INNER JOIN [%s] ON [%s].[%s] = [%s].[%s]) INNER JOIN [%s] ON [%s].[%s] = [%s].[%s] WHERE [%s].[%s]='%s' AND [%s].[%s]=%d;"), 
			Users::m_lpszTable, Options::m_lpszTable, Users::m_lpszTable, OptionsToGroup::m_lpszTable, Users::m_lpszTable, Users::m_lpszSecurityGroupID, 
			OptionsToGroup::m_lpszTable, OptionsToGroup::m_lpszGroupID, Options::m_lpszTable, OptionsToGroup::m_lpszTable, OptionsToGroup::m_lpszOptionsID, 
			Options::m_lpszTable, Options::m_lpszID, Users::m_lpszTable, Users::m_lpszUserName, strUser,
			Options::m_lpszTable, Options::m_lpszID, nID);
		
		//TRACEF (strSQL);
		rst.Open (strSQL);

		if (!rst.IsEOF ()) 
			bAllow = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bAllow;
}

#ifdef __WINPRINTER__
	static int nHighResDisplay = 1;
#else
	static int nHighResDisplay = 0;
#endif

DB_API bool FoxjetDatabase::IsStartMenuOpen ()
{
	struct
	{
		LPCTSTR m_lpsz;
		bool m_bWin7;
	} static const map [] = 
	{
		{ _T ("Shell_TrayWnd"),				false,	},
		{ _T ("DV2ControlHost"), 			true,	},
		{ _T ("NotifyIconOverflowWindow"),	true,	},
	};
	static const bool bWin7 = IsWin7 ();

	if (CWnd * pForeground = CWnd::GetForegroundWindow ()) {
		if (CWnd * p = pForeground->GetTopLevelOwner ()) {
			TCHAR sz [512] = { 0 };

			::GetClassName (p->m_hWnd, sz, ARRAYSIZE (sz) - 1); 
			//TRACEF (sz);

			for (int i = 0; i < ARRAYSIZE (map); i++)
				if (map [i].m_bWin7 == bWin7)
					if (!_tcscmp (sz, map [i].m_lpsz))
						return true;
		}
	}

	return false;
}

DB_API void FoxjetDatabase::SetHighResDisplay (bool bValue)
{
	::nHighResDisplay = bValue ? 1 : 0;
}

DB_API int FoxjetDatabase::GetTreeCtrlItemHeight ()
{
	return (int)((double)GetListCtrlItemHeight () * 1.3);
}

DB_API int FoxjetDatabase::GetListCtrlItemHeight ()
{
	return IsControl () ? 32 : 16; //(IsMatrix () && IsControl ()) ? 32 : 16; 
}

DB_API bool FoxjetDatabase::IsDebugDisplayAttached ()
{
	bool bResult = false;

	#ifdef _DEBUG
	if (HMODULE hDLL = ::LoadLibrary (_T ("user32.dll"))) {
		typedef struct _DISPLAY_DEVICE {
		  DWORD cb;
		  WCHAR DeviceName[32];
		  WCHAR DeviceString[128];
		  DWORD StateFlags;
		  WCHAR DeviceID[128];
		  WCHAR DeviceKey[128];
		} DISPLAY_DEVICE, *PDISPLAY_DEVICE;
		typedef int (CALLBACK * LPFCT) (LPCTSTR lpDevice, DWORD iDevNum, PDISPLAY_DEVICE lpDisplayDevice, DWORD dwFlags);

		if (LPFCT lp = (LPFCT)GetProcAddress (hDLL, "EnumDisplayDevicesW")) {
			for (int i = 0; ; i++) {
				DISPLAY_DEVICE d = { sizeof (d) };
				DEVMODE dm = { sizeof (dm) };

				if (!(* lp) (NULL, i, &d, 0))
					break;

				TRACEF (
					CString (_T ("\n\tDeviceName:   ")) + d.DeviceName + 
					CString (_T ("\n\tDeviceString: ")) + d.DeviceString + 
					CString (_T ("\n\tDeviceID:     ")) + d.DeviceID + 
					CString (_T ("\n\tDeviceKey:    ")) + d.DeviceKey);

				::EnumDisplaySettings (d.DeviceName, ENUM_CURRENT_SETTINGS, &dm);
				TRACEF (
					CString (_T ("\n\t\tdmBitsPerPel:       ")) + ToString (dm.dmBitsPerPel) + 
					CString (_T ("\n\t\tdmPelsWidth:        ")) + ToString (dm.dmPelsWidth) +
					CString (_T ("\n\t\tdmPelsHeight:       ")) + ToString (dm.dmPelsHeight) +
					CString (_T ("\n\t\tdmDisplayFlags:     ")) + ToString (dm.dmDisplayFlags) +
					CString (_T ("\n\t\tdmDisplayFrequency: ")) + ToString (dm.dmDisplayFrequency));

				if (!_tcsicmp (d.DeviceString, _T ("DisplayLink Graphics")) && 
					(dm.dmPelsWidth > 0 && dm.dmPelsWidth <= 1024) && 
					(dm.dmPelsHeight > 0 && dm.dmPelsHeight <= 600)) 
				{
					bResult = true;
				}
			}
		}

		::FreeLibrary (hDLL);
	}
	#endif

	return bResult;
}

DB_API bool FoxjetDatabase::IsHighResDisplay ()
{
	if (::nHighResDisplay == -1) {
		int nMin = 1000;

		int cx = ::GetSystemMetrics (SM_CXSCREEN);
		int cy = ::GetSystemMetrics (SM_CYSCREEN);

		// cx: 1680, cy: 1050 on desktop dev
		// 1024 x 1080 on production controller

		::nHighResDisplay = (cx >= nMin && cy >= nMin) ? 1 : 0;
	}

	return ::nHighResDisplay == 0 ? false : true;
}

DB_API CString FoxjetDatabase::GetLangExt ()
{
	return LoadString (IDS_LANG_EXT);
}

DB_API CString FoxjetDatabase::GetLangDir (const CString & strExt)
{
	struct
	{
		LPCTSTR m_lpszExt;
		LPCTSTR m_lpszDir;
	} 
	static const map [] = 
	{	
		{ _T ("en"), _T ("English"),	},
		{ _T ("sp"), _T ("Spanish"),	},
		{ _T ("po"), _T ("Portuguese"), },
		{ _T ("nl"), _T ("Dutch"),		},
		{ _T ("ru"), _T ("Russian"),	},
		{ _T ("hu"), _T ("Hungarian"),	},
	};

	for (int i = 0; i < ARRAYSIZE (map); i++)
		if (!strExt.CompareNoCase (map [i].m_lpszExt))
			return map [i].m_lpszDir;

	return map [0].m_lpszDir;
}

DB_API LCID FoxjetDatabase::GetLcid (const CString & strLcid)
{
	LCID lcid = 0;

	GetLocaleInfoEx(strLcid, LOCALE_RETURN_NUMBER | LOCALE_ILANGUAGE, (LPWSTR)&lcid, sizeof(lcid));

	return lcid;
}

DB_API CString FoxjetDatabase::GetLcid (LCID lcid)
{
	TCHAR sz [255] = { 0 };

	LCIDToLocaleName (lcid, sz, ARRAYSIZE (sz), 0);

	return sz;
}

DB_API CString FoxjetDatabase::GetLanguage (const CString & strLcid)
{
	TCHAR sz [255] = { 0 };

	GetLocaleInfoEx(strLcid, LOCALE_SLOCALIZEDDISPLAYNAME, (LPWSTR)sz, ARRAYSIZE (sz));

	return sz;
}


static bool bFormatUnicode = true;

DB_API void FoxjetDatabase::SetFormatUnicode (bool bFormat)
{
	::bFormatUnicode = bFormat;
}

DB_API bool FoxjetDatabase::FormatUnicode ()
{
	return ::bFormatUnicode;
}


DB_API bool FoxjetDatabase::IsRunning (LPCTSTR lpsz)
{
	HANDLE h = ::CreateMutex (NULL, FALSE, lpsz);
	bool bResult = GetLastError () == ERROR_ALREADY_EXISTS || GetLastError () == ERROR_ACCESS_DENIED;
	::CloseHandle (h);
	return bResult;
}

DB_API bool FoxjetDatabase::IsWin7 ()
{
	OSVERSIONINFO osver = { sizeof (osver) };

	::GetVersionEx (&osver);

	/*
	XP
	D-FOXJET
	dwOSVersionInfoSize: 148
	dwMajorVersion:      5
	dwMinorVersion:      1
	dwBuildNumber:       2600
	dwPlatformId:        2
	szCSDVersion:        Service Pack 3

	Win7
	ELITE7
	dwOSVersionInfoSize: 148
	dwMajorVersion:      6
	dwMinorVersion:      1
	dwBuildNumber:       7601
	dwPlatformId:        2
	szCSDVersion:        Service Pack 1

	Vista
	VISTALAPTOP
	dwOSVersionInfoSize: 148
	dwMajorVersion:      6
	dwMinorVersion:      0
	dwBuildNumber:       6002
	dwPlatformId:        2
	szCSDVersion:        Service Pack 2
	*/

	return (osver.dwMajorVersion >= 6 && osver.dwMinorVersion > 0);
}

DB_API bool FoxjetDatabase::IsPrintMonitor ()
{
	CString str = ::GetCommandLine ();
	CString strFind = GetHomeDir () + _T ("\\printmonitor.exe");

	strFind.MakeLower ();
	str.MakeLower ();

	return (str.Find (strFind) != -1) ? true : false;
}

DB_API ULONG FoxjetDatabase::GetMaxID (FoxjetDatabase::COdbcDatabase & db, const CString & strTable, const CString & strField)
{
	ULONG lResult = 1;

	try {
		CString str;
		COdbcRecordset rst (db);

		//str.Format (_T ("SELECT MAX (%s) FROM [%s]"), strField, strTable);
		str.Format (_T ("SELECT [%s] FROM [%s] ORDER BY [%s] DESC"), strField, strTable, strField);
		rst.Open (str);

		if (!rst.IsEOF ()) 
			lResult = (long)rst.GetFieldValue ((short)0);
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return lResult;
}

DB_API ULONG FoxjetDatabase::GetNextID (FoxjetDatabase::COdbcDatabase & db, const CString & strTable, const CString & strField)
{
	return GetMaxID (db, strTable, strField) + 1;
}

CInstance::CInstance () 
: m_h (NULL) 
{ 
}

CInstance::~CInstance ()
{
	if (m_h) {
		::CloseHandle (m_h);
		m_h = NULL;
	}
}

void CInstance::Create (LPCTSTR lpsz)
{
	m_h = ::CreateMutex (NULL, FALSE, lpsz);
}


bool CInstance::Elevate ()
{
	if (IsWin7 ()) {
		CString str = ::GetCommandLine ();

		if (str.Find (_T ("/requireAdministrator")) == -1) {
			SHELLEXECUTEINFO sei = { 0 };
			TCHAR sz [MAX_PATH] = { 0 };

			::GetModuleFileName (NULL, sz, ARRAYSIZE (sz));
			str.Replace ('"' + CString (sz) + '"', _T (""));
			str.Replace (sz, _T (""));
			str += CString (_T (" /requireAdministrator"));

			sei.cbSize = sizeof (sei);
			sei.lpVerb = _T ("runas");
			sei.lpFile = sz;
			sei.lpParameters = str;
			sei.nShow = SW_NORMAL;

			::ShellExecuteEx (&sei);
			TRACEF (sz);
			TRACEF (str);
			TRACEF (_T ("exit (0);"));
			exit (0);

			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

static bool bNetworked = false;

DB_API void FoxjetDatabase::SetNetworked (bool b)
{
	::bNetworked = b;
}

DB_API bool FoxjetDatabase::IsNetworked ()
{
	return ::bNetworked;
}

static bool bSelectEffectivityDate = false;
static CTime tmSelectEffectivityDate = CTime::GetCurrentTime ();
static bool bGojo = false;

DB_API void FoxjetDatabase::SetGojo (bool b)
{
	::bGojo = b;

	if (::bGojo)
		SetNetworked (true);
}

DB_API bool FoxjetDatabase::IsGojo ()
{
	return ::bGojo;
}

DB_API bool FoxjetDatabase::IsSunnyvale ()
{
	static bool * pb = NULL;

	if (!pb) {
		CString str = ::GetCommandLine ();

		str.MakeLower ();

		pb = new bool;
		* pb = str.Find (_T ("/sunnyvale")) != -1 ? true : false;
	}

	return pb ? * pb : false;
}

DB_API bool FoxjetDatabase::IsSelectEffectivityDate ()
{
	return ::bSelectEffectivityDate;
}

DB_API void FoxjetDatabase::SetSelectEffectivityDate (bool bSet, const CTime & tm)
{
	::bSelectEffectivityDate = bSet;
	::tmSelectEffectivityDate = tm;
}

DB_API CTime FoxjetDatabase::GetEffectivityDate ()
{
	return ::tmSelectEffectivityDate;
}

////////////////////////////////////////////////////////////////////////////////

using namespace FoxjetMessageBox;

static bool bAfxMainWndValid = false;

DB_API void FoxjetMessageBox::SetAfxMainWndValid (bool b)
{
	::bAfxMainWndValid = b;
}

DB_API int FoxjetMessageBox::MsgBox (HWND hWnd, LPCTSTR lpText, LPCTSTR lpCaption, UINT uType)
{
	CString strCaption = (lpCaption && _tcslen (lpCaption)) ? lpCaption : ::AfxGetAppName ();

	//if (IsMatrix ()) 
	{
		if (HMODULE hLib = ::LoadLibrary (_T ("Utils.dll"))) {
			typedef int (CALLBACK * LPMSGBOX) (HWND hWnd, const CString & strMessage, const CString & strTitle, UINT nType);

			if (LPMSGBOX lpfct = (LPMSGBOX)GetProcAddress (hLib, CAnsiString (_T ("DoMsgBox")))) {
				return (* lpfct) (hWnd, lpText, strCaption, uType);
			}
		}
	}

	return ::MessageBox (hWnd, lpText, strCaption, uType);
}

DB_API int FoxjetMessageBox::MsgBox (CWnd & wnd, LPCTSTR lpszText, LPCTSTR lpszCaption, UINT nType)
{
	return MsgBox (wnd.m_hWnd ? (::AfxGetMainWnd () ? ::AfxGetMainWnd ()->m_hWnd : NULL) : NULL, lpszText, lpszCaption, nType);
}

DB_API int FoxjetMessageBox::MsgBox (LPCTSTR lpszText, LPCTSTR lpszCaption, UINT nType)
{
	return MsgBox (::AfxGetMainWnd () ? ::AfxGetMainWnd ()->m_hWnd : NULL, lpszText, lpszCaption, nType);
}



DB_API int FoxjetMessageBox::MsgBox (LPCTSTR lpszText, UINT nType, UINT nIDHelp)
{
	HWND hwnd = ::AfxGetMainWnd () ? ::AfxGetMainWnd ()->m_hWnd : NULL;

	if (!::bAfxMainWndValid) 
		return ::MessageBox (NULL, lpszText, ::AfxGetAppName (), nType);
		
	if (!::IsWindow (hwnd) || !::IsWindowVisible (hwnd))
		hwnd = NULL;

	return MsgBox (hwnd, lpszText, _T (""), nType);
}

DB_API int FoxjetMessageBox::MsgBox (HWND hWnd, LPCTSTR lpszText, UINT nType, UINT nIDHelp)
{
	return MsgBox (hWnd, lpszText, _T (""), nType);
}
/*
DB_API int FoxjetMessageBox::MsgBox (CWnd & wnd, LPCTSTR lpszText, UINT nType, UINT nIDHelp)
{
	return MsgBox (wnd.m_hWnd, lpszText, _T (""), nType);
}
*/


/*

calling LoadString from another dll is a problem

DB_API int FoxjetMessageBox::MsgBox (UINT nIDPrompt, UINT nType, UINT nIDHelp)
{
	return MsgBox (* ::AfxGetMainWnd (), LoadString (nIDPrompt), _T (""), nType);
}

DB_API int FoxjetMessageBox::MsgBox (HWND hWnd, UINT nIDPrompt, UINT nType, UINT nIDHelp)
{
	return MsgBox (hWnd, LoadString (nIDPrompt), _T (""), nType);
}

DB_API int FoxjetMessageBox::MsgBox (CWnd & wnd, UINT nIDPrompt, UINT nType, UINT nIDHelp)
{
	return MsgBox (wnd.m_hWnd, LoadString (nIDPrompt), _T (""), nType);
}
*/

DB_API bool FoxjetDatabase::IsControl (bool bOnCmdLine)
{
	static bool bInit = false;
	static bool bResult [2] = { false, false };

	if (!bInit) {
		TCHAR sz [MAX_PATH] = { 0 };
		TCHAR szTitle [MAX_PATH] = { 0 };

		::GetModuleFileName (NULL, sz, ARRAYSIZE (sz) - 1);
		::GetFileTitle (sz, szTitle, ARRAYSIZE (szTitle) - 1);
		CString strTitle = szTitle;
		CString strCmdLine = ::GetCommandLine ();
		strTitle.MakeLower ();
		strCmdLine.MakeLower ();
		bInit = true;
		bResult [0] = strTitle.Find (_T ("control")) != -1;
		bResult [1] = strCmdLine.Find (_T ("/control")) != -1;
	}

	return bResult [0] || (bOnCmdLine ? bResult [1] : false);
}

DB_API bool FoxjetDatabase::IsConfig (bool bOnCmdLine)
{
	static bool bInit = false;
	static bool bResult [2] = { false, false };

	if (!bInit) {
		TCHAR sz [MAX_PATH] = { 0 };
		TCHAR szTitle [MAX_PATH] = { 0 };

		::GetModuleFileName (NULL, sz, ARRAYSIZE (sz) - 1);
		::GetFileTitle (sz, szTitle, ARRAYSIZE (szTitle) - 1);
		CString strTitle = szTitle;
		CString strCmdLine = ::GetCommandLine ();
		strTitle.MakeLower ();
		strCmdLine.MakeLower ();
		bInit = true;
		bResult [0] = strTitle.Find (_T ("config")) != -1;
		bResult [1] = strCmdLine.Find (_T ("/config")) != -1;
	}

	return bResult [0] || (bOnCmdLine ? bResult [1] : false);
}

DB_API bool FoxjetDatabase::IsMatrix ()
{
	return !FoxjetDatabase::IsHighResDisplay (); 
}

DB_API DWORD FoxjetDatabase::GetFileSize (const CString & strFile)
{
	DWORD dw = 0;

    if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
		fseek (fp, 0, SEEK_END);
		dw = ftell (fp);
		fclose (fp);
	}

	return dw;
/*
	HANDLE hFile = ::CreateFile (strFile, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);
	DWORD dwFileSize = 0;

	if (hFile && hFile != INVALID_HANDLE_VALUE) {
		DWORD dwHighSize = 0;
		
		dwFileSize = ::GetFileSize (hFile, &dwHighSize);
		::CloseHandle (hFile);
	}

	return dwFileSize;
*/
}

DB_API void FoxjetDatabase::UpdateNetworkedHeads (COdbcDatabase & db, const HEADSTRUCT & head)
{
	CArray <PRINTERSTRUCT, PRINTERSTRUCT &> vPrinters;
	const ULONG lFindAddr = _tcstoul (head.m_strUID, NULL, 16);

	GetPrinterRecords (db, vPrinters);

	for (int nPrinter = 0; nPrinter < vPrinters.GetSize (); nPrinter++) {
		CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;
		ULONG lPrinterID = vPrinters [nPrinter].m_lID;
		
		GetPrinterHeads (db, lPrinterID, vHeads); 

		for (int nHead = 0; nHead < vHeads.GetSize (); nHead++) {
			HEADSTRUCT & h = vHeads [nHead];
			CString strUID = h.m_strUID;
			ULONG lAddr = _tcstoul (strUID, NULL, 16);

			lAddr = head.IsUSB () ? GetSynonymUsbAddr (lAddr) : GetSynonymPhcAddr (lAddr);

			if (lFindAddr == lAddr) {
				h.m_strName			= head.m_strName;
				h.m_nHeadType		= head.m_nHeadType;
				h.m_nEncoderDivisor = head.m_nEncoderDivisor;
				h.m_nHorzRes		= head.m_nHorzRes;
				h.m_dNozzleSpan		= head.m_dNozzleSpan;

				VERIFY (UpdateHeadRecord (db, h));
			}
		}
	}
}


////////////////////////////////////////////////////////////////////////////////

static const LPCTSTR lpszBOXSTRUCT				= _T ("BOXSTRUCT");
static const LPCTSTR lpszIMAGESTRUCT			= _T ("IMAGESTRUCT");
static const LPCTSTR lpszBOXLINKSTRUCT			= _T ("BOXLINKSTRUCT");
static const LPCTSTR lpszLINESTRUCT				= _T ("LINESTRUCT");
static const LPCTSTR lpszPANELSTRUCT			= _T ("PANELSTRUCT");
static const LPCTSTR lpszMESSAGESTRUCT			= _T ("MESSAGESTRUCT");
static const LPCTSTR lpszTASKSTRUCT				= _T ("TASKSTRUCT");
static const LPCTSTR lpszHEADSTRUCT				= _T ("HEADSTRUCT");
static const LPCTSTR lpszOPTIONSSTRUCT			= _T ("OPTIONSSTRUCT");
static const LPCTSTR lpszOPTIONSLINKSTRUCT		= _T ("OPTIONSLINKSTRUCT");
static const LPCTSTR lpszUSERSTRUCT				= _T ("USERSTRUCT");
static const LPCTSTR lpszSECURITYGROUPSTRUCT	= _T ("SECURITYGROUPSTRUCT");
static const LPCTSTR lpszSHIFTSTRUCT			= _T ("SHIFTSTRUCT");
static const LPCTSTR lpszREPORTSTRUCT			= _T ("REPORTSTRUCT");
static const LPCTSTR lpszBCSCANSTRUCT			= _T ("BCSCANSTRUCT");
static const LPCTSTR lpszDELETEDSTRUCT			= _T ("DELETEDSTRUCT");
static const LPCTSTR lpszSETTINGSSTRUCT			= _T ("SETTINGSSTRUCT");
static const LPCTSTR lpszPRINTERSTRUCT			= _T ("PRINTERSTRUCT");
static const LPCTSTR lpszBITMAP					= _T ("BITMAP");


CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::BOXSTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszBOXSTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lID));
	v.Add (							(s.m_strName));
	v.Add (							(s.m_strDesc));
	v.Add (FoxjetDatabase::ToString (s.m_lWidth));
	v.Add (FoxjetDatabase::ToString (s.m_lLength));
	v.Add (FoxjetDatabase::ToString (s.m_lHeight));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::BOXSTRUCT & s)
{
	if (!IsHeader (str, ::lpszBOXSTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszBOXSTRUCT)) {
		s.m_lID		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strName	=			GetParam (v, nIndex++);
		s.m_strDesc	=			GetParam (v, nIndex++);
		s.m_lWidth	= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lLength	= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lHeight	= _tcstoul (GetParam (v, nIndex++), NULL, 10);

		return true;
	}

	return false; 
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::IMAGESTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszIMAGESTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lBoxID));
	v.Add (FoxjetDatabase::ToString ((ULONG)s.m_nPanel));
	v.Add (							(s.m_strImage));
	v.Add (FoxjetDatabase::ToString ((ULONG)s.m_lAttributes));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::IMAGESTRUCT & s)
{
	if (!IsHeader (str, ::lpszIMAGESTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszIMAGESTRUCT)) {
		s.m_lBoxID		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nPanel		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strImage	=			GetParam (v, nIndex++);
		s.m_lAttributes	= (IMAGEATTRIBUTES)_tcstoul (GetParam (v, nIndex++), NULL, 10);

		return true;
	}

	return false; 
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::BOXLINKSTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszBOXLINKSTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lLineID));
	v.Add (FoxjetDatabase::ToString (s.m_lBoxID));	

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::BOXLINKSTRUCT & s)
{
	if (!IsHeader (str, ::lpszBOXLINKSTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszBOXLINKSTRUCT)) {
		s.m_lLineID		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lBoxID		= _tcstoul (GetParam (v, nIndex++), NULL, 10);

		return true;
	}

	return false; 
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::LINESTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszLINESTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lID));
	v.Add (							(s.m_strName));
	v.Add (							(s.m_strDesc));
	v.Add (s.m_tmMaintenance.Format (_T ("%H:%M")));
	v.Add (							(s.m_strNoRead));
	v.Add (FoxjetDatabase::ToString (s.m_bResetScanInfo));
	v.Add (FoxjetDatabase::ToString (s.m_nMaxConsecutiveNoReads));
	v.Add (FoxjetDatabase::ToString (s.m_nBufferOffset));
	v.Add (FoxjetDatabase::ToString (s.m_nSignificantChars));
	v.Add (							(s.m_strAuxBoxIp));
	v.Add (FoxjetDatabase::ToString (s.m_bAuxBoxEnabled));
	v.Add (							(s.m_strSerialParams1));
	v.Add (							(s.m_strSerialParams2));
	v.Add (							(s.m_strSerialParams3));
	v.Add (FoxjetDatabase::ToString (s.m_nAMS32_A1));
	v.Add (FoxjetDatabase::ToString (s.m_nAMS32_A2));
	v.Add (FoxjetDatabase::ToString (s.m_nAMS32_A3));
	v.Add (FoxjetDatabase::ToString (s.m_nAMS32_A4));
	v.Add (FoxjetDatabase::ToString (s.m_nAMS32_A5));
	v.Add (FoxjetDatabase::ToString (s.m_nAMS32_A6));
	v.Add (FoxjetDatabase::ToString (s.m_nAMS32_A7));
	v.Add (FoxjetDatabase::ToString (s.m_nAMS256_A1));
	v.Add (FoxjetDatabase::ToString (s.m_nAMS256_A2));
	v.Add (FoxjetDatabase::ToString (s.m_nAMS256_A3));
	v.Add (FoxjetDatabase::ToString (s.m_nAMS256_A4));
	v.Add (FoxjetDatabase::ToString (s.m_nAMS256_A5));
	v.Add (FoxjetDatabase::ToString (s.m_nAMS256_A6));
	v.Add (FoxjetDatabase::ToString (s.m_nAMS256_A7));
	v.Add (FoxjetDatabase::ToString (s.m_dAMS_Interval));
	v.Add (FoxjetDatabase::ToString (s.m_lPrinterID));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::LINESTRUCT & s)
{
	if (!IsHeader (str, ::lpszLINESTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszLINESTRUCT)) {
		s.m_lID							= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strName						=			GetParam (v, nIndex++);
		s.m_strDesc						=			GetParam (v, nIndex++);
		const CString strMaintenance	=			GetParam (v, nIndex++);
		s.m_strNoRead					=			GetParam (v, nIndex++);
		s.m_bResetScanInfo				= _tcstoul (GetParam (v, nIndex++), NULL, 10) ? true : false;
		s.m_nMaxConsecutiveNoReads		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nBufferOffset				= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nSignificantChars			= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strAuxBoxIp					=			GetParam (v, nIndex++);
		s.m_bAuxBoxEnabled				= _tcstoul (GetParam (v, nIndex++), NULL, 10) ? true : false;
		s.m_strSerialParams1			=			GetParam (v, nIndex++);
		s.m_strSerialParams2			=			GetParam (v, nIndex++);
		s.m_strSerialParams3			=			GetParam (v, nIndex++);
		s.m_nAMS32_A1					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nAMS32_A2					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nAMS32_A3					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nAMS32_A4					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nAMS32_A5					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nAMS32_A6					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nAMS32_A7					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nAMS256_A1					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nAMS256_A2					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nAMS256_A3					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nAMS256_A4					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nAMS256_A5					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nAMS256_A6					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nAMS256_A7					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_dAMS_Interval				= FoxjetDatabase::_ttof (GetParam (v, nIndex++));
		s.m_lPrinterID					= _tcstoul (GetParam (v, nIndex++), NULL, 10);

		try {
			s.m_tmMaintenance.ParseDateTime (strMaintenance);
		}
		catch (COleException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		return true;
	}

	return false; 
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::PANELSTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszPANELSTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lID));
	v.Add (FoxjetDatabase::ToString (s.m_lLineID));
	v.Add (							(s.m_strName));
	v.Add (FoxjetDatabase::ToString ((long)s.m_nNumber));
	v.Add (FoxjetDatabase::ToString ((ULONG)s.m_direction));
	v.Add (FoxjetDatabase::ToString ((ULONG)s.m_orientation));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::PANELSTRUCT & s)
{
	if (!IsHeader (str, ::lpszPANELSTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszPANELSTRUCT)) {
		s.m_lID			= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lLineID		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strName		=			GetParam (v, nIndex++);
		s.m_nNumber		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_direction	= (DIRECTION)_tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_orientation	= (ORIENTATION)_tcstoul (GetParam (v, nIndex++), NULL, 10);

		return true;
	}

	return false; 
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::MESSAGESTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszMESSAGESTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lID));
	v.Add (FoxjetDatabase::ToString (s.m_lHeadID));
	v.Add (FoxjetDatabase::ToString (s.m_lTaskID));
	v.Add (							(s.m_strName));
	v.Add (							(s.m_strDesc));
	v.Add (							(s.m_strData));
	v.Add (FoxjetDatabase::ToString (s.m_lHeight));
	v.Add (FoxjetDatabase::ToString (s.m_bIsUpdated));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::MESSAGESTRUCT & s)
{
	if (!IsHeader (str, ::lpszMESSAGESTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszMESSAGESTRUCT)) {
		s.m_lID			= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lHeadID		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lTaskID		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strName		=			GetParam (v, nIndex++);
		s.m_strDesc		=			GetParam (v, nIndex++);
		s.m_strData		=			GetParam (v, nIndex++);
		s.m_lHeight		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_bIsUpdated	= _tcstoul (GetParam (v, nIndex++), NULL, 10) ? true : false;

		TRACEF (s.m_strData);

		for (int i = s.m_strData.GetLength () - 1; i >= 0; i--) {
			TCHAR c = s.m_strData [i];

			if (c > 255) {
				CString strEncode;

				strEncode.Format (_T ("\\u%04X"), c);
				s.m_strData.Delete (i);
				s.m_strData.Insert (i, strEncode);
			}
		}

		TRACEF (s.m_strData);

		return true;
	}

	return false; 
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::TASKSTRUCT & s)
{
	CStringArray v;
	CString str;

	v.Add (::lpszTASKSTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lID));
	v.Add (FoxjetDatabase::ToString (s.m_lLineID));
	v.Add (FoxjetDatabase::ToString (s.m_lBoxID));
	v.Add (							(s.m_strName));
	v.Add (							(s.m_strDesc));
	v.Add (							(s.m_strDownload));
	v.Add (FoxjetDatabase::ToString (s.m_lTransformation));
	v.Add (FormatString (s.m_strNotes));
	
	for (int i = 0; i < v.GetSize (); i++) 
		str += v [i] + _T (",");

	for (int i = 0; i < s.m_vMsgs.GetSize (); i++) 
		str += ToString (s.m_vMsgs [i]);

	return _T ("{") + str + _T ("}");
}

bool DB_API FoxjetDatabase::FromString (const CString & strFrom, FoxjetDatabase::TASKSTRUCT & t)
{
	if (!IsHeader (strFrom, ::lpszTASKSTRUCT))
		return false;

	const int nFields = 9;
	CStringArray v, vMsgs;
	CString str (strFrom);
	CString strToken [nFields] = { _T ("") };
	CLongArray delim = CountChars (strFrom, '{');
	int i;
	
	if (delim.GetSize () > 1) {
		int nIndex = delim [1];
		CString strMsgs = strFrom.Mid (nIndex);

		str = strFrom.Left (nIndex);
		ParsePackets (strMsgs, vMsgs);
	}

	if (vMsgs.GetSize () > 4)
		int nBreak = 0;

	Tokenize (str, v);

	if (v [0].CompareNoCase (::lpszTASKSTRUCT) != 0) {
		//TRACEF ("Wrong prefix \"" + strToken [0] + "\"");
		return false;
	}

	if (v.GetSize () < nFields) {
		TRACEF (_T ("Wrong number of tokens"));
		return false;
	}

	for (i = 0; i < min (v.GetSize (), nFields); i++) 
		if (v [i].GetLength ()) 
			strToken [i] = v [i];

	int nIndex = 1;

	t.m_lID				= _tcstoul (strToken [nIndex++], NULL, 10);
	t.m_lLineID			= _tcstoul (strToken [nIndex++], NULL, 10);
	t.m_lBoxID			= _tcstoul (strToken [nIndex++], NULL, 10);
	t.m_strName			=			strToken [nIndex++];
	t.m_strDesc			=			strToken [nIndex++];
	t.m_strDownload		=			strToken [nIndex++];
	t.m_lTransformation	= _tcstoul (strToken [nIndex++], NULL, 10);
	t.m_strNotes		= UnformatString (strToken [nIndex++]);

	ASSERT (nFields <= nIndex);

	t.m_vMsgs.RemoveAll ();

	for (i = 0; i < vMsgs.GetSize (); i++) {
		CString strMsg = _T ("{") + vMsgs [i] + _T ("}");
		MESSAGESTRUCT m;

		TRACEF (strMsg);

		if (FromString (strMsg, m))
			t.m_vMsgs.Add (m);
		else
			TRACEF (_T ("FromString failed: ") + strMsg);
	}

	if (t.m_vMsgs.GetSize () != vMsgs.GetSize ()) {
		ASSERT (0);
		TRACEF (ToString (t.m_vMsgs.GetSize ()));
		TRACEF (ToString (vMsgs.GetSize ()));
	}

	return true; //t.m_vMsgs.GetSize () == vMsgs.GetSize ();
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::HEADSTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszHEADSTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lID));
	v.Add (FoxjetDatabase::ToString (s.m_lPanelID));
	v.Add (							(s.m_strName));
	v.Add (							(s.m_strUID));
	v.Add (FoxjetDatabase::ToString (s.m_lRelativeHeight));
	v.Add (FoxjetDatabase::ToString ((int)s.m_nChannels));
	v.Add (FoxjetDatabase::ToString ((int)s.m_nHorzRes));
	v.Add (FoxjetDatabase::ToString (s.m_lPhotocellDelay));
	v.Add (FoxjetDatabase::ToString (s.m_nEncoder));
	v.Add (FoxjetDatabase::ToString (s.m_nDirection));
	v.Add (FoxjetDatabase::ToString (s.m_bEnabled));
	v.Add (FoxjetDatabase::ToString (s.m_bInverted));
	v.Add (FoxjetDatabase::ToString (s.m_dHeadAngle));
	v.Add (FoxjetDatabase::ToString (s.m_nPhotocell));
	v.Add (FoxjetDatabase::ToString (s.m_nSharePhoto));
	v.Add (FoxjetDatabase::ToString (s.m_nShareEnc));
	v.Add (FoxjetDatabase::ToString ((int)s.m_nHeadType));
	v.Add (FoxjetDatabase::ToString (s.m_bRemoteHead));
	v.Add (FoxjetDatabase::ToString (s.m_bMasterHead));
	v.Add (FoxjetDatabase::ToString ((int)s.m_nIntTachSpeed));
	v.Add (FoxjetDatabase::ToString (s.m_dNozzleSpan));
	v.Add (FoxjetDatabase::ToString (s.m_dPhotocellRate));
	v.Add (							(s.m_strSerialParams));
	v.Add (FoxjetDatabase::ToString (s.m_nEncoderDivisor));
	v.Add (FoxjetDatabase::ToString (s.m_bDoublePulse));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::HEADSTRUCT & s)
{
	if (!IsHeader (str, ::lpszHEADSTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszHEADSTRUCT)) {
		s.m_lID				= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lPanelID		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strName			=			GetParam (v, nIndex++);
		s.m_strUID			=			GetParam (v, nIndex++);
		s.m_lRelativeHeight	= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nChannels		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nHorzRes		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lPhotocellDelay	= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nEncoder		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nDirection		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_bEnabled		= _tcstoul (GetParam (v, nIndex++), NULL, 10) ? true : false;
		s.m_bInverted		= _tcstoul (GetParam (v, nIndex++), NULL, 10) ? true : false;
		s.m_dHeadAngle		= FoxjetDatabase::_ttof (GetParam (v, nIndex++));
		s.m_nPhotocell		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nSharePhoto		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nShareEnc		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_nHeadType		= (HEADTYPE)_tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_bRemoteHead		= _tcstoul (GetParam (v, nIndex++), NULL, 10) ? true : false;
		s.m_bMasterHead		= _tcstoul (GetParam (v, nIndex++), NULL, 10) ? true : false;
		s.m_nIntTachSpeed	= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_dNozzleSpan		= FoxjetDatabase::_ttof (GetParam (v, nIndex++));
		s.m_dPhotocellRate	= FoxjetDatabase::_ttof (GetParam (v, nIndex++));
		s.m_strSerialParams	=			GetParam (v, nIndex++);
		s.m_nEncoderDivisor	= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_bDoublePulse	= _tcstoul (GetParam (v, nIndex++), NULL, 10);

		return true;
	}

	return false; 
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::OPTIONSSTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszOPTIONSSTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lID));
	v.Add (							(s.m_strOptionDesc));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::OPTIONSSTRUCT & s)
{
	if (!IsHeader (str, ::lpszOPTIONSSTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszOPTIONSSTRUCT)) {
		s.m_lID				= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strOptionDesc	=			GetParam (v, nIndex++);
		
		return true;
	}

	return false; 
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::OPTIONSLINKSTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszOPTIONSLINKSTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lOptionsID));
	v.Add (FoxjetDatabase::ToString (s.m_lGroupID));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::OPTIONSLINKSTRUCT & s)
{
	if (!IsHeader (str, ::lpszOPTIONSLINKSTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszOPTIONSLINKSTRUCT)) {
		s.m_lOptionsID		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lGroupID		= _tcstoul (GetParam (v, nIndex++), NULL, 10);

		return true;
	}

	return false; 
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::USERSTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszUSERSTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lID));
	v.Add (							(s.m_strFirstName));
	v.Add (							(s.m_strLastName));
	v.Add (							(s.m_strUserName));
	v.Add (							(s.m_strPassW));
	v.Add (FoxjetDatabase::ToString (s.m_lSecurityGroupID));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::USERSTRUCT & s)
{
	if (!IsHeader (str, ::lpszUSERSTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszUSERSTRUCT)) {
		s.m_lID					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strFirstName		=			GetParam (v, nIndex++);
		s.m_strLastName			=			GetParam (v, nIndex++);
		s.m_strUserName			=			GetParam (v, nIndex++);
		s.m_strPassW			=			GetParam (v, nIndex++);
		s.m_lSecurityGroupID	= _tcstoul (GetParam (v, nIndex++), NULL, 10);

		return true;
	}

	return false; 
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::SECURITYGROUPSTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszSECURITYGROUPSTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lID));
	v.Add (							(s.m_strName));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::SECURITYGROUPSTRUCT & s)
{
	if (!IsHeader (str, ::lpszSECURITYGROUPSTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszSECURITYGROUPSTRUCT)) {
		s.m_lID		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strName	=			GetParam (v, nIndex++);

		return true;
	}

	return false; 
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::SHIFTSTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszSHIFTSTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lID));
	v.Add (FoxjetDatabase::ToString (s.m_lLineID));
	v.Add (							(s.m_strName));
	v.Add (s.m_dtTime.Format (MDB_DATE_STRFTIME));
	v.Add (							(s.m_strCode));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::SHIFTSTRUCT & s)
{
	if (!IsHeader (str, ::lpszSHIFTSTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszSHIFTSTRUCT)) {
		s.m_lID					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lLineID				= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strName				=			GetParam (v, nIndex++);
		const CString strTime	=			GetParam (v, nIndex++);
		s.m_strCode				=			GetParam (v, nIndex++);

		try {
			s.m_dtTime.ParseDateTime (strTime);
		}
		catch (COleException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		return true;
	}

	return false; 
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::REPORTSTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszREPORTSTRUCT);
	v.Add (s.m_dtTime.Format (MDB_DATE_STRFTIME));
	v.Add (							(s.m_strAction));
	v.Add (							(s.m_strUsername));
	v.Add (							(s.m_strCounts));
	v.Add (							(s.m_strLine));
	v.Add (							(s.m_strTaskName));
	v.Add (FoxjetDatabase::ToString (s.m_lType));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::REPORTSTRUCT & s)
{
	if (!IsHeader (str, ::lpszREPORTSTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszREPORTSTRUCT)) {
		const CString strTime	=			GetParam (v, nIndex++);
		s.m_strAction			=			GetParam (v, nIndex++);
		s.m_strUsername			=			GetParam (v, nIndex++);
		s.m_strCounts			=			GetParam (v, nIndex++);
		s.m_strLine				=			GetParam (v, nIndex++);
		s.m_strTaskName			=			GetParam (v, nIndex++);
		s.m_lType				= _tcstoul (GetParam (v, nIndex++), NULL, 10);

		try {
			s.m_dtTime.ParseDateTime (strTime);
		}
		catch (COleException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		return true;
	}

	return false; 
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::BCSCANSTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszBCSCANSTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lID));
	v.Add (s.m_dtTime.Format (MDB_DATE_STRFTIME));
	v.Add (FoxjetDatabase::ToString (s.m_LineID));
	v.Add (							(s.m_strScanner));
	v.Add (							(s.m_strTaskname));
	v.Add (							(s.m_strBarcode));
	v.Add (FoxjetDatabase::ToString (s.m_lTotalScans));
	v.Add (FoxjetDatabase::ToString (s.m_lGoodScans));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::BCSCANSTRUCT & s)
{
	if (!IsHeader (str, ::lpszBCSCANSTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszBCSCANSTRUCT)) {
		s.m_lID					= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		const CString strTime	=			GetParam (v, nIndex++);
		s.m_LineID				= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strScanner			=			GetParam (v, nIndex++);
		s.m_strTaskname			=			GetParam (v, nIndex++);
		s.m_strBarcode			=			GetParam (v, nIndex++);
		s.m_lTotalScans			= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lGoodScans			= _tcstoul (GetParam (v, nIndex++), NULL, 10);

		try {
			s.m_dtTime.ParseDateTime (strTime);
		}
		catch (COleException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		return true;
	}

	return false; 
}



CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::DELETEDSTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszDELETEDSTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lID));
	v.Add (FoxjetDatabase::ToString (s.m_lLineID));
	v.Add (							(s.m_strTaskName));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::DELETEDSTRUCT & s)
{
	if (!IsHeader (str, ::lpszDELETEDSTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszDELETEDSTRUCT)) {
		s.m_lID			= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_lLineID		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strTaskName	=			GetParam (v, nIndex++);

		return true;
	}

	return false; 
}

CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::SETTINGSSTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszSETTINGSSTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lLineID));
	v.Add (							(s.m_strKey));
	v.Add (							(s.m_strData));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::SETTINGSSTRUCT & s)
{
	if (!IsHeader (str, ::lpszSETTINGSSTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszSETTINGSSTRUCT)) {
		s.m_lLineID		= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strKey		=			GetParam (v, nIndex++);
		s.m_strData		=			GetParam (v, nIndex++);

		return true;
	}

	return false; 
}


CString DB_API FoxjetDatabase::ToString  (const FoxjetDatabase::PRINTERSTRUCT & s)
{
	CStringArray v;

	v.Add (::lpszPRINTERSTRUCT);
	v.Add (FoxjetDatabase::ToString (s.m_lID));
	v.Add (							(s.m_strName));
	v.Add (							(s.m_strAddress));
	v.Add (FoxjetDatabase::ToString (s.m_bMaster));

	return FoxjetDatabase::ToString (v);
}

bool DB_API FoxjetDatabase::FromString (const CString & str, FoxjetDatabase::PRINTERSTRUCT & s)
{
	if (!IsHeader (str, ::lpszPRINTERSTRUCT))
		return false;

	CStringArray v;
	int nIndex = 0;

	FoxjetDatabase::FromString (str, v);

	if (!GetParam (v, nIndex++).CompareNoCase (::lpszPRINTERSTRUCT)) {
		s.m_lID			= _tcstoul (GetParam (v, nIndex++), NULL, 10);
		s.m_strName		=			GetParam (v, nIndex++);
		s.m_strAddress	=			GetParam (v, nIndex++);
		s.m_bMaster		= _ttoi	   (GetParam (v, nIndex++)) ? true : false;

		return true;
	}

	return false; 
}

DB_API float FoxjetDatabase::_ttof (const TCHAR * string)
{
	float f = 0;
	_stscanf (string, _T ("%f"), &f);
	return f;
}

DB_API ULONG FoxjetDatabase::GetFirstLineID (COdbcDatabase & db, ULONG lPrinterID)
{
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	ULONG lResult = 0;

	if (lPrinterID == -1)
		lPrinterID = GetMasterPrinterID (db);

	GetPrinterLines (db, lPrinterID, vLines);

	if (vLines.GetSize ())
		lResult = vLines [0].m_lID;

	return lResult;
}

CUseInfo2::CUseInfo2 ()
{
}

CUseInfo2::CUseInfo2 (USE_INFO_2 * p)
:	m_strLocal		(p->ui2_local),
	m_strRemote		(p->ui2_remote),
	m_strPassword	(p->ui2_password),
	m_dwStatus		(p->ui2_status),
	m_dwAsg_type	(p->ui2_asg_type),
	m_dwRefcount	(p->ui2_refcount),
	m_dwUsecount	(p->ui2_usecount),
	m_strUsername	(p->ui2_username),
	m_strDomainname (p->ui2_domainname)
{
}

DB_API void FoxjetDatabase::EnumMappedDrives (CArray <CUseInfo2, CUseInfo2 &> & v)
{
   USE_INFO_2 * BufPtr = NULL, * pth = NULL;
   NET_API_STATUS  res;
   LPTSTR lpszServer = NULL;
   DWORD er = 0, tr = 0, resume = 0;

   do {

	  res = NetUseEnum (lpszServer, 2, (LPBYTE *)&BufPtr, -1, &er, &tr, &resume);

	  // If the call succeeds,

	  if (res == ERROR_SUCCESS || res == ERROR_MORE_DATA) {

		 pth = BufPtr;

		 for(int i = 1; i <= (int)er; i++) {
			 CUseInfo2 s (pth);
			 v.Add (s);
			 pth++;
		 }
	  }
	  else
		 TRACEF (_T ("NetUseEnum failed: ") + ToString (res));

   }
   while (res == ERROR_MORE_DATA); 

   NetApiBufferFree (BufPtr);
}


DB_API CString FoxjetDatabase::Format (const CString & str)
{
	CString strResult = str;

	//strResult.Replace (_T ("\""), _T ("\"\""));
	strResult.Replace (_T ("'"), _T ("''"));

	if (!strResult.CompareNoCase (_T ("false")))
		strResult = _T ("0");
	else if (!strResult.CompareNoCase (_T ("true")))
		strResult = _T ("-1");

	return strResult;
}

#ifdef _DEBUG
static CString GetVariantType (DWORD dwVarType)
{
	switch (dwVarType) {
	case DBVT_NULL:		return _T ("DBVT_NULL");	break;
	case DBVT_BOOL:		return _T ("DBVT_BOOL");	break;
	case DBVT_UCHAR:	return _T ("DBVT_UCHAR");	break;
	case DBVT_SHORT:	return _T ("DBVT_SHORT");	break;
	case DBVT_LONG:		return _T ("DBVT_LONG");	break;
	case DBVT_SINGLE:	return _T ("DBVT_SINGLE");	break;
	case DBVT_DOUBLE:	return _T ("DBVT_DOUBLE");	break;
	case DBVT_DATE:		return _T ("DBVT_DATE");	break;
	case DBVT_STRING:	return _T ("DBVT_STRING");	break;
	case DBVT_BINARY:	return _T ("DBVT_BINARY");	break;
	default:			return _T ("Unknown");		break;
	}
}
#endif //_DEBUG

DB_API bool FoxjetDatabase::IsSqlString (DWORD dwSqlType)
{
	switch (dwSqlType) {
	case SQL_CHAR:
	case SQL_VARCHAR:
	case SQL_LONGVARCHAR:
	case SQL_WCHAR:
	case SQL_WVARCHAR:
	case SQL_WLONGVARCHAR:
	case SQL_DATETIME:
	case SQL_TYPE_DATE:
	case SQL_TYPE_TIME:
	case SQL_TYPE_TIMESTAMP:
	//case SQL_DATE:
	case SQL_TIME:
	case SQL_TIMESTAMP:
		return true;
	}

	return false;
}

DB_API CString FoxjetDatabase::FormatSQL (COdbcRecordset & rst, const CString & strTable, const CStringArray & name_in, const CStringArray & value_in,
	const CString & strID, ULONG lID)
{
	CString strSQL;
	CStringArray name, value;

	value.Append (value_in);
	name.Append (name_in);

	ASSERT (name.GetSize () == value.GetSize ());

	//rst.DebugDump ();

	if (rst.IsAddNew ()) {
		CString strName;
		CString strValue;		

		if (rst.GetOdbcDatabase ().IsSqlServer ()) {
			CString strIDField = GetIDField (strTable);

			if (strIDField.GetLength ()) {
				ULONG lNextID = (long)GetNextID (rst.GetOdbcDatabase (), strTable);

				ASSERT (strID.GetLength ());
				ASSERT (!strID.CompareNoCase (strIDField));
				ASSERT (lID == -1);

				ASSERT (Find (name, strID) == -1);

				name.Add (strID);
				value.Add (ToString (lNextID));
			}
		}

		for (int i = 0; i < name.GetSize (); i++) {
			CString str;

			{
				str.Format (_T ("\"%s\""), Format (name [i]));

				if (i < (name.GetSize () - 1))
					str += _T (", ");

				strName += str;
			}

			{
				CODBCFieldInfo info;
			
				VERIFY (rst.GetFieldInfo (name [i], info));
				
				if (IsSqlString (info.m_nSQLType)) {
					if (!value[i].GetLength())
						str = _T("' '"); // _T("Null");
					else
						str = _T ("'") + Format (value [i]) + _T ("'");
				}
				else
					str = Format (value [i]);

				if (i < (name.GetSize () - 1))
					str += _T (", ");

				strValue += str;
			}
		}

		strSQL.Format (_T ("INSERT INTO %s (%s) VALUES (%s);"), strTable, strName, strValue);
	}
	else {
		CString strSet;

		for (int i = 0; i < name.GetSize (); i++) {
			CString str;
			CODBCFieldInfo info;
			
			VERIFY (rst.GetFieldInfo (name [i], info));
			str.Format (_T ("\"%s\"="), name [i]);

			if (IsSqlString (info.m_nSQLType)) {
				if (!value [i].GetLength ()) 
					str += _T ("Null");
				else
					str += _T ("'") + Format (value [i]) + _T ("'");
			}
			else
				str += Format (value [i]);

			if (i < (name.GetSize () - 1))
				str += _T (", ");

			strSet += str;
		}

		if (strID.GetLength ())
			strSQL.Format (_T ("UPDATE %s SET %s WHERE [%s]=%d;"), strTable, strSet, strID, lID);
		else
			strSQL.Format (_T ("UPDATE %s SET %s "), strTable, strSet);
	}

	TRACEF (strSQL);
	return strSQL;
}

static BOOL CALLBACK EnumEditorThreadWndProc (HWND hwnd, LPARAM lParam)
{
	CArray <HWND, HWND> & v = * (CArray <HWND, HWND> *)lParam;

	v.Add (hwnd);

	return TRUE;
}

DB_API HWND FoxjetDatabase::FindEditor ()
{
	CArray <HWND, HWND> v;

	//#define DEBUG_FINDEDITOR

	#ifdef DEBUG_FINDEDITOR
	{ CString str; str.Format (_T ("%s(%d): EnumWindows"), _T (__FILE__), __LINE__); TRACER (str); }
	#endif

	::EnumWindows (EnumEditorThreadWndProc, (LPARAM)&v);

	#ifdef DEBUG_FINDEDITOR
	{ CString str; str.Format (_T ("%s(%d): v.GetSize (): %d"), _T (__FILE__), __LINE__, v.GetSize ()); TRACER (str); }
	#endif

	if (!v.GetSize ()) {
		#ifdef DEBUG_FINDEDITOR
		{ CString str; str.Format (_T ("%s(%d): EnumWindows"), _T (__FILE__), __LINE__); TRACER (str); }
		#endif

		::EnumWindows (EnumEditorThreadWndProc, (LPARAM)&v);

		#ifdef DEBUG_FINDEDITOR
		{ CString str; str.Format (_T ("%s(%d): v.GetSize (): %d"), _T (__FILE__), __LINE__, v.GetSize ()); TRACER (str); }
		#endif
	}

	for (int i = 0; i < v.GetSize (); i++) {
		HWND hwnd = v [i];
		DWORD dwResult = 0;

		LRESULT lResult = ::SendMessageTimeout (hwnd, WM_ISEDITORRUNNING, 1, 0, SMTO_BLOCK | SMTO_ABORTIFHUNG, 100, &dwResult);

		#ifdef DEBUG_FINDEDITOR
		TCHAR sz [512] = { 0 };
		::GetWindowText (hwnd, sz, ARRAYSIZE (sz));
		{ CString str; str.Format (_T ("%s(%d): 0x%p %s, %s"), _T (__FILE__), __LINE__, hwnd, sz, GetClassName (hwnd)); TRACER (str); }
		#endif

		if (dwResult == WM_ISEDITORRUNNING) {
			#ifdef DEBUG_FINDEDITOR
			{ CString str; str.Format (_T ("%s(%d): %d"), _T (__FILE__), __LINE__, dwResult); TRACER (str); }
			TRACER (_T ("\n")); 
			#endif
			return hwnd;
		}
	}

	#ifdef DEBUG_FINDEDITOR
	{ CString str; str.Format (_T ("%s(%d): return NULL\n"), _T (__FILE__), __LINE__); TRACER (str); }
	#endif

	return NULL;
}

DB_API HWND FoxjetDatabase::FindControl ()
{
	CArray <HWND, HWND> v;

	::EnumWindows (EnumEditorThreadWndProc, (LPARAM)&v);

	if (!v.GetSize ()) {
		::EnumWindows (EnumEditorThreadWndProc, (LPARAM)&v);
	}

	for (int i = 0; i < v.GetSize (); i++) {
		HWND hwnd = v [i];
		DWORD dwResult = 0;

		LRESULT lResult = ::SendMessageTimeout (hwnd, WM_ISPRINTERRUNNING, 1, 0, SMTO_BLOCK | SMTO_ABORTIFHUNG, 100, &dwResult);

		if (HWND h = (HWND)dwResult) {
			if (::IsWindow (h)) {
				TCHAR sz [128] = { 0 };

				::GetWindowText (h, sz, ARRAYSIZE (sz));
				TRACEF (sz);
				return hwnd;
			}
		}
	}

	return NULL;
}

DB_API void FoxjetDatabase::Restore (CWnd * pWnd, const CString & strCmdLine)
{
	CString str = _T ("\"") + GetHomeDir () + _T ("\\Restore.exe\" ") + strCmdLine + _T (" ");
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [MAX_PATH * 2];

	{
		TCHAR sz [MAX_PATH] = { 0 };

		::GetModuleFileName (::GetModuleHandle (NULL), sz, ARRAYSIZE (sz) - 1);
		CString strCmd = ::GetCommandLine ();

		strCmd.Delete (0, _tcslen (sz) + 2);
		str += _T (" ") + strCmd;
	}

	_tcsncpy (szCmdLine, str, ARRAYSIZE (szCmdLine) - 1);
    memset (&pi, 0, sizeof(pi));
    memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	TRACER (szCmdLine);

	if (!::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) 
		MsgBox (* pWnd, FormatMessage (GetLastError ()));
}

#define MAX_MIME_LINE_LEN 76

static const std::string base64_chars = 
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";

static inline bool is_base64(TCHAR c) 
{
  return (isalnum(c) || (c == '+') || (c == '/'));
}

std::string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len) 
{
  std::string ret;
  int i = 0, j = 0;
  unsigned char char_array_3[3], char_array_4[4];

  while (in_len--)
	{
    char_array_3[i++] = *(bytes_to_encode++);
    if (i == 3) 
		{
      char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
      char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
      char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
      char_array_4[3] = char_array_3[2] & 0x3f;

      for(i = 0; (i <4) ; i++)
        ret += base64_chars[char_array_4[i]];
      i = 0;
    }
  }

  if (i)
  {
    for(j = i; j < 3; j++)
      char_array_3[j] = '\0';

    char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
    char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
    char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
    char_array_4[3] = char_array_3[2] & 0x3f;

    for (j = 0; (j < i + 1); j++)
      ret += base64_chars[char_array_4[j]];

    while((i++ < 3))
      ret += '=';

  }

  return ret;

}

std::string base64_decode(std::string const& encoded_string) 
{
  int in_len = encoded_string.size();
  int i = 0, j = 0, in_ = 0;
  unsigned char char_array_4[4], char_array_3[3];
  std::string ret;

  while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) 
	{
    char_array_4[i++] = encoded_string[in_]; in_++;
    if (i ==4) {
      for (i = 0; i <4; i++)
        char_array_4[i] = base64_chars.find(char_array_4[i]);

      char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
      char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
      char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

      for (i = 0; (i < 3); i++)
        ret += char_array_3[i];
      i = 0;
    }
  }

  if (i) 
	{
    for (j = i; j <4; j++)
      char_array_4[j] = 0;

    for (j = 0; j <4; j++)
      char_array_4[j] = base64_chars.find(char_array_4[j]);

    char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
    char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
    char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

    for (j = 0; (j < i - 1); j++) 
			ret += char_array_3[j];
  }

  return ret;
}

DB_API int FoxjetDatabase::EncodeBase64 (unsigned char * pbInput, int nLen, unsigned char * pbOutput, int nMaxSize)
{
/*
	std::string str = base64_encode (pbInput, nLen);
	int nResult = min (str.length (), nMaxSize);
	memcpy (pbOutput, str.c_str (), nResult);
	return nResult;
*/

	static const char* s_Base64Table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	
	bool bAddLineBreak = true;
	unsigned char* pbOutStart = pbOutput;
	unsigned char* pbOutEnd = pbOutput + nMaxSize;
	int nFrom, nLineLen = 0;
	unsigned char chHigh4bits = 0;

	for (nFrom=0; nFrom<nLen; nFrom++)
	{
		if (pbOutput >= pbOutEnd)
			break;

		unsigned char ch = pbInput[nFrom];
		switch (nFrom % 3)
		{
		case 0:
			*pbOutput++ = s_Base64Table[ch >> 2];
			chHigh4bits = (ch << 4) & 0x30;
			break;

		case 1:
			*pbOutput++ = s_Base64Table[chHigh4bits | (ch >> 4)];
			chHigh4bits = (ch << 2) & 0x3c;
			break;

		default:
			*pbOutput++ = s_Base64Table[chHigh4bits | (ch >> 6)];
			if (pbOutput < pbOutEnd)
			{
				*pbOutput++ = s_Base64Table[ch & 0x3f];
				nLineLen++;
			}
		}

		nLineLen++;
		if (bAddLineBreak && nLineLen >= MAX_MIME_LINE_LEN && pbOutput+2 <= pbOutEnd)
		{
			//*pbOutput++ = '\r';
			*pbOutput++ = '\n';
			nLineLen = 0;
		}
	}

	if (nFrom % 3 != 0 && pbOutput < pbOutEnd)	// 76 = 19 * 4, so the padding wouldn't exceed 76
	{
		*pbOutput++ = s_Base64Table[chHigh4bits];
		int nPad = 4 - (nFrom % 3) - 1;
		if (pbOutput+nPad <= pbOutEnd)
		{
			::memset(pbOutput, '=', nPad);
			pbOutput += nPad;
		}
	}
	if (bAddLineBreak && nLineLen != 0 && pbOutput+2 <= pbOutEnd)	// add CRLF
	{
		//*pbOutput++ = '\r';
		*pbOutput++ = '\n';
	}
	return (int)(pbOutput - pbOutStart);
}

DB_API int FoxjetDatabase::DecodeBase64 (unsigned char * pbInput, unsigned char * pbOutput, int nMaxSize)
{
	std::string str = base64_decode (std::string ((char *)pbInput));
	int nResult = min (str.length (), nMaxSize);
	memcpy (pbOutput, str.c_str (), nResult);

	return nResult;
}

static const LPCTSTR lpszAscii [] =
{
	_T ("<NULL>"),	// 00
	_T ("<SOH>"), 	// 01
	_T ("<STX>"),	// 02
	_T ("<ETX>"),	// 03
	_T ("<EOT>"),	// 04
	_T ("<ENQ>"),	// 05
	_T ("<ACK>"),	// 06
	_T ("<BEL>"),	// 07
	_T ("<BS>"),	// 08
	_T ("<HT>"),	// 09
	_T ("<LF>"),	// 10
	_T ("<VI>"),	// 11
	_T ("<FF>"),	// 12
	_T ("<CR>"),	// 13
	_T ("<SO>"),	// 14
	_T ("<SI>"),	// 15
	_T ("<SLE>"),	// 16
	_T ("<DC1>"),	// 17
	_T ("<DC2>"),	// 18
	_T ("<DC3>"),	// 19
	_T ("<DC4>"),	// 20
	_T ("<NAK>"),	// 21
	_T ("<SYN>"),	// 22
	_T ("<ETB>"),	// 23
	_T ("<CAN>"),	// 24
	_T ("<EM>"),	// 25
	_T ("<SIB>"),	// 26
	_T ("<ESC>"),	// 27
	_T ("<FS>"),	// 28
	_T ("<GS>"),	// 29
	_T ("<RS>"),	// 30
	_T ("<US>"),	// 31
};


DB_API CString FoxjetDatabase::Format (const LPBYTE lpBuff, ULONG lLen)
{
	CString strData;

	for (ULONG lChar = 0; lChar < lLen; lChar++) {
		TCHAR c = lpBuff [lChar];

		if (c >= 0 && c < ARRAYSIZE (::lpszAscii)) 
			strData += ::lpszAscii [c];
		else 
			strData += c;
	}

	return strData;
}

DB_API CString FoxjetDatabase::Format (const TCHAR * lpBuff, ULONG lLen)
{
	CString strData;

	for (ULONG lChar = 0; lChar < lLen; lChar++) {
		TCHAR c = lpBuff [lChar];

		if (c >= 0 && c < ARRAYSIZE (::lpszAscii)) 
			strData += ::lpszAscii [c];
		else 
			strData += c;
	}

	return strData;
}

DB_API int FoxjetDatabase::Unformat (const CString & str, LPBYTE lpBuffer)
{
	CString strData (str);
	int nResult = 0;
	LPBYTE lp = lpBuffer;

	while (strData.GetLength ()) {
		bool bReplaced = false;

		for (int i = 0; i < ARRAYSIZE (::lpszAscii); i++) {
			const LPCTSTR lpszFind = ::lpszAscii [i];
			int nLen = _tcslen (lpszFind);

			if (strData.GetLength () >= nLen) {
				if (!_tcsncmp (strData, lpszFind, nLen)) {
					* lp++ = i;
					nResult++;
					strData.Delete (0, nLen);
					bReplaced = true;
				}
			}
		}

		if (!bReplaced) {
			* lp++ = (BYTE)strData [0];
			nResult++;
			strData.Delete (0, 1);
		}
	}

	* lp++ = 0;

	return nResult;
}

DB_API int FoxjetDatabase::Unformat (const CString & str, TCHAR * lpBuffer)
{
	CString strData (str);
	int nResult = 0;
	TCHAR * lp = lpBuffer;

	while (strData.GetLength ()) {
		bool bReplaced = false;

		for (int i = 0; i < ARRAYSIZE (::lpszAscii); i++) {
			const LPCTSTR lpszFind = ::lpszAscii [i];
			int nLen = _tcslen (lpszFind);

			if (strData.GetLength () >= nLen) {
				if (!_tcsncmp (strData, lpszFind, nLen)) {
					* lp++ = i;
					nResult++;
					strData.Delete (0, nLen);
					bReplaced = true;
				}
			}
		}

		if (!bReplaced) {
			* lp++ = strData [0];
			nResult++;
			strData.Delete (0, 1);
		}
	}

	* lp++ = 0;

	return nResult;
}

DB_API bool FoxjetDatabase::IsLeapYear (int nYear)
{
	/*
	if (year is not divisible by 4) then (it is a common year)
	else if (year is not divisible by 100) then (it is a leap year)
	else if (year is not divisible by 400) then (it is a common year)
	else (it is a leap year)
	*/

	if ((nYear % 4) != 0) 
		return false;
	else if ((nYear % 100) != 0) 
		return true;
	else if ((nYear % 400) != 0) 
		return true;
		
	return true;
}

DB_API int FoxjetDatabase::GetDaysInMonth (int nMonth, int nYear)
{
	int n [12] = 
	{
		31,
		!IsLeapYear (nYear) ? 28 : 29,
		31,
		30,
		31,
		30,
		31,
		31,
		30,
		31,
		30,
		31,
	};

	ASSERT (nMonth >= 1 && nMonth <= 12);

	return n [nMonth - 1];
}

static bool bUniRes = false;

DB_API bool FoxjetDatabase::IsUltraResEnabled ()
{
	return ::bUniRes;
}

DB_API void FoxjetDatabase::EnableUltraRes (bool b)
{
	::bUniRes = b;
}

DB_API bool FoxjetDatabase::IsTimingEnabled ()
{
	static bool * pb = NULL;

	if (!pb) {
		CString str = ::GetCommandLine ();

		str.MakeLower ();
		pb = new bool;

		* pb = str.Find (_T ("/timing")) != -1 ? true : false;
	}

	return * pb;
}

DB_API bool FoxjetDatabase::IsValve () 
{
	static bool * pb = NULL;

	if (!pb) {
		CString str = ::GetCommandLine ();

		str.MakeLower ();
		pb = new bool;
		* pb = str.Find (_T ("/iv")) != -1 ? true : false;
	}

	ASSERT (pb);

	return (* pb) || FoxjetDatabase::IsDriverInstalled (GUID_INTERFACE_IVPHC);
}