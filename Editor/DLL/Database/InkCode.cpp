#include "stdafx.h"
#include "Registry.h"
#include "Debug.h"
#include "Parse.h"
#include "AnsiString.h"
#include "zlib.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Encrypted::InkCodes;

const std::string FoxjetDatabase::base32 = "234567ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const std::string FoxjetDatabase::base36 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

static std::vector <int> vPrime;

static void InitPrimes (std::vector <int> & v, int n)
{
	if (Encrypted::RSA::IsEnabled ()) {
		if (v.size () == 0) {
			const ULONG lMaxSqrt = sqrt (pow (2.0, n));
			const ULONG lExponentMax = pow (2.0, n);

			DECLARETRACECOUNT (20);
			MARKTRACECOUNT ();

			v.push_back (2);

			//for (int lTest = 2; lTest < lExponentMax; lTest++) {
			for (int lTest = 3; lTest < lExponentMax; lTest += 2) {
				ULONG n;

				//for (n = 2; n <= lTest - 1; n++) {
				for (n = 3; n <= lTest - 1; n += 2) {
					if ((lTest % n) == 0)
						break;
				}

				if (n == lTest) 
					v.push_back (lTest);
			}

			MARKTRACECOUNT ();
			TRACEF (ToString ((int)v.size ()));
			TRACECOUNTARRAY (); 
			int nBreak = 0;
		}
	}
}

static ULONG CalcPrime (int nExponent)
{
	switch (nExponent) {
	case 18:
		return 262139;
	}

	//CEnablePassword::Init (::vPrime, nExponent);
	InitPrimes (::vPrime, nExponent);

	if (::vPrime.size ())
		return ::vPrime [::vPrime.size () - 1];
}

FoxjetDatabase::Encrypted::InkCodes::tagINK_CODE::tagINK_CODE ()
{ 	
	m_nManufacturer = 0;
	m_nDay			= 0;
	m_nSerialNumber = 0;
	m_nExpPeriod	= 0;
	m_nCRC			= 0;

	if (FoxjetDatabase::Encrypted::RSA::IsEnabled ()) {
		m_nManufacturer = 1;
		m_nDay			= 14;
		m_nSerialNumber = 10;
		m_nExpPeriod	= 2;
		m_nCRC			= 18; 
	}
}

#define IMPLEMENT_INK_CODE_MEMBER(s) \
	bool FoxjetDatabase::Encrypted::InkCodes::CInkCode::Set##s (unsigned __int32 n) \
	{ \
		static const unsigned __int32 lMax = (unsigned __int32)pow (2.0, (double)m_range.m_n##s) - 1;  \
		 \
		if (n >= 0 && n <= lMax) { \
			m_code.m_n##s = n; \
			return true; \
		} \
		 \
		return false; \
	} \
	\
	unsigned __int32 FoxjetDatabase::Encrypted::InkCodes::CInkCode::Get##s () const { return m_code.m_n##s; }

IMPLEMENT_INK_CODE_MEMBER (Manufacturer);
IMPLEMENT_INK_CODE_MEMBER (Day);
IMPLEMENT_INK_CODE_MEMBER (SerialNumber);
IMPLEMENT_INK_CODE_MEMBER (ExpPeriod);
IMPLEMENT_INK_CODE_MEMBER (CRC);

const CTime CInkCode::m_tmBase			= CTime (2015, 1, 1, 0, 0, 0, 0);
const INK_CODE CInkCode::m_range		= INK_CODE ();
const CString CInkCode::m_strFormatDay	= _T ("%m/%d/%Y");
const CString CInkCode::m_strFormat		= _T ("%H:%M:%S ") + CInkCode::m_strFormatDay;

FoxjetDatabase::Encrypted::InkCodes::CInkCode::CInkCode ()
{
	memset (&m_code, 0, sizeof (m_code));
}

FoxjetDatabase::Encrypted::InkCodes::CInkCode::CInkCode (const CInkCode & rhs)
{
	memcpy (&m_code, &rhs.m_code, sizeof (m_code));
}

FoxjetDatabase::Encrypted::InkCodes::CInkCode::~CInkCode ()
{
}

FoxjetDatabase::Encrypted::InkCodes::CInkCode & FoxjetDatabase::Encrypted::InkCodes::CInkCode::operator = (const FoxjetDatabase::Encrypted::InkCodes::CInkCode & rhs)
{
	if (this != &rhs) {
		memcpy (&m_code, &rhs.m_code, sizeof (m_code));
	}

	return * this;
}

CString FoxjetDatabase::Encrypted::InkCodes::CInkCode::ToString () const
{
	CString str;
	CTime tm = m_tmBase + CTimeSpan (m_code.m_nDay, 0, 0, 0);
	LPCTSTR lpszMfg [] = { _T ("MFG_FOXJET"), _T ("MFG_DIAGRAPH"), };
	LPCTSTR lpszExp [] = { _T ("invalid"), _T ("EXP_PERIOD_12"), _T ("EXP_PERIOD_18"), _T ("EXP_PERIOD_24"), };

	str.Format (_T ("[%s] [%s] [serial: %04d] [%s] [crc: %s]"),
		lpszMfg [m_code.m_nManufacturer],	
		tm.Format (m_strFormatDay),			
		m_code.m_nSerialNumber,	
		lpszExp [m_code.m_nExpPeriod],	
		ToHexString (m_code.m_nCRC));

	return str;
}

unsigned __int32 FoxjetDatabase::Encrypted::InkCodes::CInkCode::CalcCRC (const CInkCode & c)
{
	struct
	{
		unsigned __int32 m_nBits;
		unsigned __int32 m_nValue;
	}
	map [] = 
	{
		{ c.m_range.m_nManufacturer,	c.m_code.m_nManufacturer,	},
		{ c.m_range.m_nDay,				c.m_code.m_nDay,			},
		{ c.m_range.m_nSerialNumber,	c.m_code.m_nSerialNumber,	},
		{ c.m_range.m_nExpPeriod,		c.m_code.m_nExpPeriod,		},
		{ c.m_range.m_nCRC,				c.m_code.m_nCRC,			},
	};
	CString strBin;
	int nBits = 0;

	for (int i = 0; i < ARRAYSIZE (map) - 1; i++) {
		CString str = ::ToBinString (map [i].m_nValue);

		str.Replace (_T (" "), _T (""));
		strBin += str.Right (map [i].m_nBits);
		nBits += map [i].m_nBits;
	}

	//TRACEF (::ToString (strBin.GetLength ()) + _T (": ") + strBin);
	ASSERT (strBin.GetLength () == nBits);

	unsigned __int32 nData = _tcstoul (strBin, NULL, 2);
	unsigned __int32 nCRC = crc32 (0L, Z_NULL, 0);
	static const unsigned __int32 nMax = CalcPrime (c.m_range.m_nCRC);

	ASSERT (sizeof (nData) >= (int)ceil ((double)nBits / 8.0));

	for (unsigned __int8 * p = (unsigned __int8 *)&nData, i = 0; i < sizeof (nData); i++) 
		nCRC = crc32 (nCRC, &p [i], sizeof (p [i]));

	if (!nMax)
		return 0;

	return nCRC % nMax;
}

CString FoxjetDatabase::Encrypted::InkCodes::CInkCode::GetCRC (const CInkCode & c)
{
	unsigned __int32 nCRC = CalcCRC (c);
	CString str = ::ToBinString (nCRC);

	str.Replace (_T (" "), _T (""));
	return str.Right (c.m_range.m_nCRC);
}

static long DateDiff (const CTime & tm1, const CTime & tm2) 
{
	long n1 = (tm1.GetYear () * 12) + tm1.GetMonth ();
	long n2 = (tm2.GetYear () * 12) + tm2.GetMonth ();

	return n1 - n2;
}

static int GetDstOffset (const CTime & now, const CTime & exp)
{
	struct tm tmNow;
	struct tm tmExp;

	now.GetLocalTm (&tmNow); //GetGmtTm always returns DST = 0
	exp.GetLocalTm (&tmExp);

	mktime (&tmNow);
	mktime (&tmExp);

	return tmNow.tm_isdst - tmExp.tm_isdst;
}

bool FoxjetDatabase::Encrypted::InkCodes::CInkCode::IsValid (const CTime * ptm) const
{
	if (FoxjetDatabase::Encrypted::InkCodes::CInkCode::IsValid (ptm, GetDay (), GetExpPeriod ())) {
		unsigned __int32 nThis = m_code.m_nCRC;
		unsigned __int32 nCalc = CalcCRC (* this);

		return nThis == nCalc;
	}

	return false;
}

bool FoxjetDatabase::Encrypted::InkCodes::CInkCode::IsValid (const CTime * ptm, __int32 nDay, __int32 nExpPeriod)
{
	ULONG lSpan = 0;

	switch (nExpPeriod) {
	default:	return false;
	case EXP_PERIOD_1:  lSpan = 1;		break;
	case EXP_PERIOD_12: lSpan = 12;		break;
	case EXP_PERIOD_18: lSpan = 18;		break;
	case EXP_PERIOD_24: lSpan = 24;		break;
	}

	if (ptm) {
		const CTime tmDay = CInkCode::m_tmBase + CTimeSpan (nDay, 0, 0, 0);

		const CTime tmNow = ptm ? * ptm : CTime::GetCurrentTime ();
		CTime tmTmp (tmDay.GetYear (), tmDay.GetMonth (), tmDay.GetDay (), 23, 23, 59);
		struct tm mtm;

		//TRACEF (tmDay.Format (CInkCode::m_strFormat));
		//TRACEF (tmTmp.Format (CInkCode::m_strFormat));

		tmTmp.GetLocalTm (&mtm); 
		mtm.tm_mon += lSpan;
		tmTmp = CTime (mktime (&mtm));

		//TRACEF (tmTmp.Format (CInkCode::m_strFormat));

		if (int nDstOffset = GetDstOffset (tmNow, tmTmp)) 
			tmTmp += CTimeSpan (0, nDstOffset, 0, 0);

		//TRACEF (tmTmp.Format (CInkCode::m_strFormat));

		long nDiff = DateDiff (tmTmp, tmDay);

		// if we added one month to 1/31 and got 3/2, roll back until 
		// we hit the last day of the previous month (2/29)
		while (nDiff > (long)lSpan) {
			tmTmp -= CTimeSpan (1, 0, 0, 0);

			//TRACEF (tmTmp.Format (CInkCode::m_strFormat));

			nDiff = DateDiff (tmTmp, tmNow);
		}

		//TRACEF (tmTmp.Format (CInkCode::m_strFormat));

		const CTime tmStart = CTime (tmDay.GetYear (), tmDay.GetMonth (), tmDay.GetDay (), 0, 0, 0);
		const CTime tmEnd = CTime (tmTmp.GetYear (), tmTmp.GetMonth (), tmTmp.GetDay (), 23, 59, 59);

		//TRACEF (tmNow.Format (CInkCode::m_strFormat) + _T (" [") + tmStart.Format (CInkCode::m_strFormat) + _T (", ") + tmEnd.Format (CInkCode::m_strFormat) + _T ("]"));

		#if 0 //def _DEBUG
		{
			CTimeSpan tmSpan = tmEnd - tmStart;
			int nDays = tmSpan.GetDays ();
			double dSpan = ((double)nDays / 365.0) * 10;
			int nDebug = FoxjetDatabase::Round (dSpan);

			//TRACEF (::ToString (nDebug) + _T (" [") + ::ToString (tmSpan.GetDays ()) + _T ("]"));

			switch (nExpPeriod) {
			case EXP_PERIOD_12: ASSERT (nDebug == 10); break;
			case EXP_PERIOD_18: ASSERT (nDebug == 15); break;
			case EXP_PERIOD_24: ASSERT (nDebug == 20); break;
			}
		}
		#endif

		//TRACEF (tmNow.Format (_T ("%c ")) + tmStart.Format (_T ("[%c, ")) + tmEnd.Format (_T ("%c]")));

		if (tmNow < tmStart || tmNow > tmEnd)
			return false;

		return true;
	}

	return false;
}

static const int nScramble30 [] = 
{
	16, 10, 26, 24, 1, 5, 25, 23, 6, 3, 22, 4, 11, 29, 15, 2, 12, 21, 28, 27, 19, 14, 13, 0, 17, 18, 8, 9, 7, 20,
};

static const int nScramble45 [] = 
{
	11, 28, 18, 20, 30, 33, 10, 16, 8, 26, 36, 21, 34, 27, 22, 24, 12, 43, 9, 29, 7, 32, 40, 14, 44, 4, 39, 23, 41, 13, 42, 31, 17, 3, 6, 25, 35, 15, 5, 19, 37, 0, 1, 2, 38, 
};


DB_API inline CString FoxjetDatabase::Encrypted::InkCodes::Scramble (const CString & str)
{
	CString strResult (' ', str.GetLength ());

	ASSERT (ARRAYSIZE (nScramble30) == 30);
	ASSERT (ARRAYSIZE (nScramble45) == 45);
	ASSERT (str.GetLength () == ARRAYSIZE (nScramble30) || str.GetLength () == ARRAYSIZE (nScramble45));

	const int * pn = NULL;

	if (str.GetLength () == ARRAYSIZE (nScramble30))
		pn = nScramble30;
	else if (str.GetLength () == ARRAYSIZE (nScramble45))
		pn = nScramble45;

	ASSERT (pn);

	if (pn) {
		for (int i = 0; i < str.GetLength (); i++) 
			strResult.SetAt (pn [i], str [i]);
	}

	return strResult;
}

DB_API inline CString FoxjetDatabase::Encrypted::InkCodes::Unscramble (const CString & str)
{
	CString strResult (' ', str.GetLength ());

	ASSERT (ARRAYSIZE (nScramble30) == 30);
	ASSERT (ARRAYSIZE (nScramble45) == 45);
	ASSERT (str.GetLength () == ARRAYSIZE (nScramble30) || str.GetLength () == ARRAYSIZE (nScramble45));

	const int * pn = NULL;

	if (str.GetLength () == ARRAYSIZE (nScramble30))
		pn = nScramble30;
	else if (str.GetLength () == ARRAYSIZE (nScramble45))
		pn = nScramble45;

	ASSERT (pn);

	if (pn) {
		for (int i = 0; i < str.GetLength (); i++) 
			strResult.SetAt (i, str [pn [i]]);
	}

	return strResult;
}

CString FoxjetDatabase::Encrypted::InkCodes::CInkCode::ToBinString () const
{
	CString strResult;
	struct
	{
		unsigned __int32 m_nBits;
		unsigned __int32 m_nValue;
	}
	map [] = 
	{
		{ m_range.m_nManufacturer,	m_code.m_nManufacturer,	},
		{ m_range.m_nDay,			m_code.m_nDay,			},
		{ m_range.m_nSerialNumber,	m_code.m_nSerialNumber,	},
		{ m_range.m_nExpPeriod,		m_code.m_nExpPeriod,	},
		{ m_range.m_nCRC,			m_code.m_nCRC,			},
	};

	if (!FoxjetDatabase::Encrypted::RSA::IsEnabled ())
		return _T ("");

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		CString str = ::ToBinString (map [i].m_nValue);

		str.Replace (_T (" "), _T (""));
		strResult += str.Right (map [i].m_nBits);
	}

	return Scramble (strResult);
}

bool FoxjetDatabase::Encrypted::InkCodes::CInkCode::FromBinString (const CString & strIn, const CTime * ptm)
{
	struct
	{
		unsigned __int32 m_nBits;
		unsigned __int32 m_nValue;
	}
	map [] = 
	{
		{ m_range.m_nManufacturer,	m_code.m_nManufacturer,	},
		{ m_range.m_nDay,			m_code.m_nDay,			},
		{ m_range.m_nSerialNumber,	m_code.m_nSerialNumber,	},
		{ m_range.m_nExpPeriod,		m_code.m_nExpPeriod,	},
		{ m_range.m_nCRC,			m_code.m_nCRC,			},
	};
	int nBits = 0;

	if (!FoxjetDatabase::Encrypted::RSA::IsEnabled ())
		return false;

	CString str = Unscramble (strIn);

	for (int i = 0; i < ARRAYSIZE (map); i++) 
		nBits += map [i].m_nBits;

	if (str.GetLength () == nBits) {
		int nIndex = 0;

		for (int i = 0; i < ARRAYSIZE (map); i++) {
			CString s = str.Mid (nIndex, map [i].m_nBits);
			
			map [i].m_nValue = _tcstoul (s, NULL, 2);
			nIndex += map [i].m_nBits;
		}

		CInkCode tmp;

		if (tmp.SetManufacturer (map [0].m_nValue) &&
			tmp.SetDay			(map [1].m_nValue) &&
			tmp.SetSerialNumber (map [2].m_nValue) &&
			tmp.SetExpPeriod	(map [3].m_nValue) &&
			tmp.SetCRC			(map [4].m_nValue))
		{
			unsigned __int32 nCRC = CalcCRC (tmp);

			if (nCRC == tmp.GetCRC ()) {
				* this = tmp;

				return ptm ? IsValid (ptm) : true;
			}
		}
	}

	return false;
}



CString FoxjetDatabase::Encrypted::InkCodes::CInkCode::ToDebugString () const
{
	CString strResult;
	struct
	{
		unsigned __int32 m_nBits;
		unsigned __int32 m_nValue;
	}
	map [] = 
	{
		{ m_range.m_nManufacturer,	m_code.m_nManufacturer,	},
		{ m_range.m_nDay,			m_code.m_nDay,			},
		{ m_range.m_nSerialNumber,	m_code.m_nSerialNumber,	},
		{ m_range.m_nExpPeriod,		m_code.m_nExpPeriod,	},
		{ m_range.m_nCRC,			m_code.m_nCRC,			},
	};
	TCHAR c = 'a';

	if (!FoxjetDatabase::Encrypted::RSA::IsEnabled ())
		return _T ("");

	for (int i = 0; i < ARRAYSIZE (map); i++) 
		strResult += CString (c++, map [i].m_nBits);

	return strResult;
}

CString FoxjetDatabase::Encrypted::InkCodes::CInkCode::Encode () 
{
	CString str;

	if (!FoxjetDatabase::Encrypted::RSA::IsEnabled ())
		return _T ("");

	SetCRC ();

	CString strBin = ToBinString ();

	strBin.Replace (_T (" "), _T (""));

	while (strBin.GetLength () > 0) {
		CString s = strBin.Left (5);
				
		//TRACEF (s + _T (" ") + strBin);
		strBin.Delete (0, 5);
		int nIndex = _tcstoul (s, NULL, 2);
		str += base32 [nIndex];
	}

	while (str.GetLength () < 9)
		str = '0' + str;

	//str.Insert (6, '-');
	//str.Insert (3, '-');

	return str;
}

CString FoxjetDatabase::Encrypted::InkCodes::CInkCode::FormatString (CString str)
{
	/*
	static const std::string base32 = "234567ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static const std::string base36 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	diff: 0189
	*/

	str.Replace (_T ("-"), _T (""));
	str.Replace (_T ("0"), _T ("O"));
	str.Replace (_T ("1"), _T ("I"));
	str.MakeUpper ();
	
	return str;
}

bool FoxjetDatabase::Encrypted::InkCodes::CInkCode::Decode (const CString & strIn, const CTime * ptm)
{
	CString str = FormatString (strIn), strBin;

	if (!FoxjetDatabase::Encrypted::RSA::IsEnabled ())
		return false;

	if (str.GetLength () != 9)
		return false;

	for (int i = 0; i < str.GetLength (); i++) {
		char c = (char)str [i];
		unsigned __int8 nIndex = (unsigned __int8)Find (base32, c);

		ASSERT (nIndex != -1);
		CString s = ::ToBinString (nIndex);
		strBin += s.Right (5);
	}

	return FromBinString (strBin, ptm);
}
