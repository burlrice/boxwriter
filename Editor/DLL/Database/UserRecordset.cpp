// UserRecordset.cpp : implementation file
//

#include "stdafx.h"
#include "Database.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Debug.h"
#include "Serialize.h"
#include "compare.h"
#include "Parse.h"

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Users;

extern HINSTANCE hInstance;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)

FoxjetDatabase::tagUSERSTRUCT::tagUSERSTRUCT ()
{
}

FoxjetDatabase::tagUSERSTRUCT::tagUSERSTRUCT (const tagUSERSTRUCT & rhs)
:	m_lID				(rhs.m_lID),
	m_strFirstName		(rhs.m_strFirstName),
	m_strLastName		(rhs.m_strLastName),
	m_strUserName		(rhs.m_strUserName),
	m_strPassW			(rhs.m_strPassW),
	m_lSecurityGroupID	(rhs.m_lSecurityGroupID)
{
}

tagUSERSTRUCT & FoxjetDatabase::tagUSERSTRUCT::operator = (const tagUSERSTRUCT & rhs)
{
	if (this != &rhs) {
		m_lID				= rhs.m_lID;
		m_strFirstName		= rhs.m_strFirstName;
		m_strLastName		= rhs.m_strLastName;
		m_strUserName		= rhs.m_strUserName;
		m_strPassW			= rhs.m_strPassW;
		m_lSecurityGroupID	= rhs.m_lSecurityGroupID;
	}

	return * this;
}

FoxjetDatabase::tagUSERSTRUCT::~tagUSERSTRUCT ()
{
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::USERSTRUCT & lhs, const FoxjetDatabase::USERSTRUCT & rhs)
{
	return
		lhs.m_lID				== rhs.m_lID &&
		lhs.m_strFirstName		== rhs.m_strFirstName &&
		lhs.m_strLastName		== rhs.m_strLastName &&
		lhs.m_strUserName		== rhs.m_strUserName &&
		lhs.m_strPassW			== rhs.m_strPassW &&
		lhs.m_lSecurityGroupID	== rhs.m_lSecurityGroupID;
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::USERSTRUCT & lhs, const FoxjetDatabase::USERSTRUCT & rhs)
{
	return !(rhs == lhs);
}

DB_API const COdbcRecordset & FoxjetDatabase::operator >> (const COdbcRecordset & rst, USERSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		s.m_lID				= (long)rst.GetFieldValue (m_lpszID);
		s.m_strFirstName	= (CString)rst.GetFieldValue (m_lpszFirstName);
		s.m_strLastName		= (CString)rst.GetFieldValue (m_lpszLastName);
		s.m_strUserName		= (CString)rst.GetFieldValue (m_lpszUserName);
		s.m_strPassW		= (CString)rst.GetFieldValue (m_lpszPassW);
		s.m_lSecurityGroupID	= (long)rst.GetFieldValue (m_lpszSecurityGroupID);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API COdbcRecordset & FoxjetDatabase::operator << (COdbcRecordset & rst, USERSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	CStringArray name, value;

	if (s.m_lID == -1) 
		rst.SetEditMode (COdbcRecordset::addnew);

	name.Add (m_lpszFirstName);			value.Add (s.m_strFirstName);
	name.Add (m_lpszLastName);			value.Add (s.m_strLastName);
	name.Add (m_lpszUserName);			value.Add (s.m_strUserName);
	name.Add (m_lpszPassW);				value.Add (s.m_strPassW);
	name.Add (m_lpszSecurityGroupID);	value.Add (ToString ((long)s.m_lSecurityGroupID));

	for (int i = 0; i < value.GetSize (); i++)
		if (!value [i].GetLength ())
			value.SetAt (i, _T (" "));

	CString strSQL = FormatSQL (rst, m_lpszTable, name, value, m_lpszID, s.m_lID);
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

	if (s.m_lID == -1)
		s.m_lID = GetMaxID (rst.GetOdbcDatabase (), m_lpszTable, m_lpszID);
/*
	try {
		if (rst.IsAddNew ())
			rst.SetFieldValue (m_lpszID,	(long)GetNextID (rst.GetOdbcDatabase (), m_lpszTable));

		rst.SetFieldValue (m_lpszFirstName,		s.m_strFirstName);
		rst.SetFieldValue (m_lpszLastName,		s.m_strLastName);
		rst.SetFieldValue (m_lpszUserName,		s.m_strUserName);
		rst.SetFieldValue (m_lpszPassW,			s.m_strPassW);
		rst.SetFieldValue (m_lpszSecurityGroupID,	(long)s.m_lSecurityGroupID);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/

	return rst;
}


DB_API bool FoxjetDatabase::GetUserRecord (COdbcDatabase & database, ULONG lID, USERSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u"), 
			m_lpszTable, m_lpszID, lID);
		rst.Open (str);

		if (!rst.IsEOF ()) {
			rst >> s;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetUserRecord (COdbcDatabase & database, CString sUserName, USERSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s'"), 
			m_lpszTable, m_lpszUserName, sUserName);
		rst.Open (str);

		if (!rst.IsEOF ()) {
			rst >> s;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::AddUserRecord (COdbcDatabase & database, USERSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	int nField = 1;
	try {
		COdbcRecordset rst (&database);

		s.m_lID = -1;
		rst << s;

		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s]"), m_lpszTable);
		rst.Open (str, CRecordset::dynaset);
		rst.AddNew ();
		rst << s;
		rst.Update ();
		rst.MoveLast ();
		s.m_lID = (long)rst.GetFieldValue (m_lpszID);
		rst.Close ();
		*/

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::UpdateUserRecord (COdbcDatabase & database, USERSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);
		rst << s;

		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u"), 
			m_lpszTable, m_lpszID, s.m_lID);
		rst.Open (str, CRecordset::dynaset);
		rst.Edit ();
		rst << s;
		rst.Update();
		rst.Close ();
		*/

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::DeleteUserRecord (COdbcDatabase & database, ULONG lID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	CString str;
	
	str.Format (
		_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
		m_lpszTable, m_lpszID, lID);

	try {
		database.ExecuteSQL (str);
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetUserRecords (COdbcDatabase & database, CArray <USERSTRUCT, USERSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	v.RemoveAll ();

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s]"), 
			m_lpszTable);
		rst.Open (str);

		while (!rst.IsEOF ()) {
			USERSTRUCT s;
			rst >> s;
			v.Add (s);
			rst.MoveNext ();
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return v.GetSize () > 0;
}

