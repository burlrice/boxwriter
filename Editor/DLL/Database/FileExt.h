#if !defined (_FILEEXT_H_)
#define _FILEEXT_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DbApi.h"
#include "Database.h"

namespace FoxjetFile 
{
	DB_API typedef enum {	UNKNOWNDOCTYPE = 0, MESSAGE, TASK } DOCTYPE;
	DB_API CString GetRootPath ();
	DB_API DOCTYPE GetFileType (const CString & strPath);
	DB_API CString GetFileName (const CString & strPath);
	DB_API CString GetFileTitle (const CString & strPath);
	DB_API CString GetFileExt (const CString & strPath);
	DB_API CString GetFileExt (const DOCTYPE & type);
	DB_API CString GetFileHead (const CString & strPath);
	DB_API bool IsValidFileTitle (const CString & str);
	DB_API bool IsValidFileHead (const CString & str, FoxjetDatabase::COdbcDatabase & database);
	DB_API CString GetRelativeName (const CString & strPath);
	DB_API CString GetModifiedSuffix ();
	DB_API bool FileExists (const CString & strFile);
	DB_API CString GetFileTitle (const CString & strActualFile, CString & strFolder, CString & strTitle, CString & strSuffix);
	DB_API CString GetPath(const CString& strFile);

	DB_API void SetWriteDirect (bool b);
	DB_API bool IsWriteDirect ();
	DB_API DWORD WriteDirect (CString strFile, LPBYTE pData, DWORD dwLen);
	DB_API DWORD WriteDirect (CString strFile, const CString & strData);
	DB_API CString ReadDirect (CString strFile, const CString & strDefault = _T (""));

	DB_API DWORD GetFileAttributesTimeout (LPCTSTR lpszFile, DWORD dwTimeout = 5000);
};

#endif
