#ifndef __PARSE_H__
#define __PARSE_H__

#pragma once

#include <afxcoll.h>
#include "DbApi.h"
#include "CopyArray.h"
#include "Database.h"
#include <string>
#include <vector>

template <class T, class T2>
inline std::vector<T> explode (const T & str, const T2 delim = ',')
{
	CStringArray v;
	std::vector<T> result;

	ASSERT (delim != '\\');

	FoxjetDatabase::Tokenize (str.c_str (), v, delim);

	for (UINT i = 0; i < (UINT)v.GetSize (); i++) 
		result.push_back (T (v [i]));

	if (result.size ())
		if (!result [result.size () - 1].length ())
			result.erase (result.end () - 1);

	return result;
}

inline std::vector<std::wstring> explode (const CString & str, const TCHAR delim = ',')
{
	return explode<std::wstring, wchar_t> (std::wstring (str), (wchar_t)delim);
}

namespace FoxjetDatabase
{
	CString DB_API ToString (ULONG lVal); 
	CString DB_API ToString (int nVal); 
	CString DB_API ToString (bool bVal); 
	CString DB_API ToString (double dVal); 
	CString DB_API ToString (__int64 iVal); 
	CString DB_API ToString (unsigned __int64 iVal); 

	DB_API CString Tokenize (const CString &strInput, CStringArray &v, TCHAR cDelim = ',', bool bTrim = true);
	DB_API FoxjetCommon::CLongArray CountChars (const CString & str, TCHAR c);
	DB_API CString TokenizeCmdLine (const CString &strInput, CStringArray &v);

	DB_API CString FormatString(const CString &str, bool bFilename = false);
	DB_API CString UnformatString(const CString &str);
	DB_API CString Trim(CString str);
	DB_API CString ToString (const CStringArray & v);
	DB_API int FromString (const CString & strInput, CStringArray & v);

	inline std::vector<std::wstring> FromString (const CString & str)
	{
		CStringArray vTmp;
		std::vector<std::wstring> v;
		FoxjetDatabase::FromString (str, vTmp);
		for (int i = 0; i < vTmp.GetSize (); i++)
			v.push_back (std::wstring (vTmp [i]));
		return v;
	}
	
	DB_API CString GetKey (const CString & strPacket);
	DB_API void ParsePackets (const CString & str, CStringArray & v);
	DB_API void ExtractPackets (const CString & strKey, const CString & str, CStringArray & v);
	DB_API CString SetPackets (const CString & strPacket, const CString & strData);
	DB_API CString GetParam (const CStringArray & v, int nIndex, const CString & strDef = _T (""));
	
	inline std::vector<std::wstring> ParsePackets (const CString & str)
	{
		CStringArray vTmp;
		std::vector<std::wstring> v;
		FoxjetDatabase::ParsePackets (str, vTmp);
		for (int i = 0; i < vTmp.GetSize (); i++)
			v.push_back (std::wstring (vTmp [i]));
		return v;
	}

	DB_API CString Extract (const CString & strParse, const CString & strBegin);
	DB_API CString Extract (const CString & strParse, const CString & strBegin, const CString & strEnd, bool bInclusive = false);

	CString DB_API ToString (const FoxjetDatabase::VALVESTRUCT & s);
	bool DB_API FromString (const CString & str, FoxjetDatabase::VALVESTRUCT & s);

	DB_API std::string UnicodeToUTF8(const std::wstring &wstr);
	DB_API std::wstring UTF8ToUnicode(const std::string &str);

	DB_API CString UrlDecode (const CString & str);
	DB_API std::vector <std::wstring> UrlDecode (const std::wstring & str);

}; //namespace FoxjetDatabase


#endif //__PARSE_H__
