// OdbCOdbcTables.cpp : implementation file
//

#include "stdafx.h"
#include "OdbcTables.h"
#include "Debug.h"

#ifdef _DEBUG
	#include "OdbcRecordset.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;

/////////////////////////////////////////////////////////////////////////////
// COdbCOdbcTables

IMPLEMENT_DYNAMIC(COdbcTables, CRecordset)

COdbcTables::COdbcTables(CDatabase* pDatabase)
	: CRecordset(pDatabase)
{
	//{{AFX_FIELD_INIT(COdbcTables)
	m_strQualifier = _T ("");
	m_strOwner = _T ("");
	m_strName = _T ("");
	m_strType = _T ("");
	m_strRemarks = _T ("");
	m_nFields = 5;
	//}}AFX_FIELD_INIT
	m_strQualifierParam = _T ("");
	m_strOwnerParam = _T ("");
	m_strNameParam = _T ("");
	m_strTypeParam = _T ("'TABLE'");
}

BOOL COdbcTables::Open(UINT nOpenType, LPCSTR lpszSQL,
	DWORD dwOptions)
{
	ASSERT(lpszSQL == NULL);

	RETCODE nRetCode;

	// Cache state info and allocate hstmt
	SetState(nOpenType,NULL,noDirtyFieldCheck);
	if (!AllocHstmt())
		return FALSE;

	TRY
	{
		OnSetOptions(m_hstmt);
		AllocStatusArrays();
		
		/*
		#ifdef _UNICODE
		AFX_SQL_ASYNC(this, (::SQLTables)(m_hstmt,
			(m_strQualifierParam.IsEmpty()? (UCHAR FAR *)NULL: (UCHAR FAR *)(LPCSTR)w2a (m_strQualifierParam)), SQL_NTS,
			(m_strOwnerParam.IsEmpty()? (UCHAR FAR *)NULL: (UCHAR FAR *)(LPCSTR)w2a (m_strOwnerParam)), SQL_NTS,
			(m_strNameParam.IsEmpty()? (UCHAR FAR *)NULL: (UCHAR FAR *)(LPCSTR)w2a (m_strNameParam)), SQL_NTS,
			(m_strTypeParam.IsEmpty()? (UCHAR FAR *)NULL: (UCHAR FAR *)(LPCSTR)w2a (m_strTypeParam)), SQL_NTS));
		#else
		AFX_SQL_ASYNC(this, (::SQLTables)(m_hstmt,
			(m_strQualifierParam.IsEmpty()? (UCHAR FAR *)NULL: (UCHAR FAR *)(LPCTSTR)m_strQualifierParam), SQL_NTS,
			(m_strOwnerParam.IsEmpty()? (UCHAR FAR *)NULL: (UCHAR FAR *)(LPCTSTR)m_strOwnerParam), SQL_NTS,
			(m_strNameParam.IsEmpty()? (UCHAR FAR *)NULL: (UCHAR FAR *)(LPCTSTR)m_strNameParam), SQL_NTS,
			(m_strTypeParam.IsEmpty()? (UCHAR FAR *)NULL: (UCHAR FAR *)(LPCTSTR)m_strTypeParam), SQL_NTS));
		#endif
		*/
		
		/* from MSDN:
		// Get a list of all tables in the current database.
		SQLTables(hstmt, NULL, 0, NULL, 0, NULL, 0, NULL,0);

		// Get a list of all tables in all databases.
		SQLTables(hstmt, (SQLCHAR*) "%", SQL_NTS, NULL, 0, NULL, 0, NULL,0);
		
		// Get a list of databases on the current connectionís server.
		SQLTables(hstmt, (SQLCHAR*) "%", SQL_NTS, (SQLCHAR*)"", 0, (SQLCHAR*)"", 0, NULL, 0);
		*/

		#if 0 //#ifdef _DEBUG
		if (COdbcDatabase * pdb = DYNAMIC_DOWNCAST (COdbcDatabase, m_pDatabase)) {
			if (pdb->IsProgress ()) {
				try 
				{
					COdbcRecordset rst (pdb);
					CString str = _T ("SELECT TOP 10 * FROM [PUB].[Product]");

					TRACEF (pdb->GetConnect ());
					TRACEF (str);

					BOOL bResult = rst.Open (1, str, 36);

					if (bResult) {
						for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
							CODBCFieldInfo info;

							rst.GetODBCFieldInfo (i, info);
							TRACEF (info.m_strName);
						}

						while (!rst.IsEOF ()) {
							CString s;

							for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
								CODBCFieldInfo info;

								s += (CString)rst.GetFieldValue ((short)i);
								s += _T (", ");
							}

							TRACEF (s);

							rst.MoveNext ();
						}
					}

					int nBreak = 0;
				}
				catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
				catch (CDBException * e)		
				{ 
					HANDLEEXCEPTION_TRACEONLY (e); 
				}
			}
		}
		#endif //_DEBUG

		AFX_SQL_ASYNC (this, ::SQLTables (m_hstmt, NULL, 0, NULL, 0, NULL, 0, NULL, 0));

		#if 0
		if (COdbcDatabase * pdb = DYNAMIC_DOWNCAST (COdbcDatabase, m_pDatabase)) {
			if (pdb->IsProgress ()) {
				int nBreak = 0;
			}
		}
		#endif

		if (!Check(nRetCode))
			ThrowDBException(nRetCode, m_hstmt);

		// Allocate memory and cache info
		AllocAndCacheFieldInfo();
		AllocRowset();

		// Fetch the first row of data
		MoveNext();

		// If EOF, result set is empty, set BOF as well
		m_bBOF = m_bEOF;
	}

	CATCH_ALL(e)
	{
		Close();
		THROW_LAST();
	}
	END_CATCH_ALL

	return TRUE;
}

CString COdbcTables::GetDefaultConnect()
{
	return "ODBC;";
}

CString COdbcTables::GetDefaultSQL()
{
	// should SQLTables directly, so GetSQL should never be called
	ASSERT(FALSE);
	return "!";
}

void COdbcTables::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(COdbcTables)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Text(pFX, _T ("table_qualifier"), m_strQualifier);
	RFX_Text(pFX, _T ("table_owner"), m_strOwner);
	RFX_Text(pFX, _T ("table_name"), m_strName);
	RFX_Text(pFX, _T ("table_type"), m_strType);
	RFX_Text(pFX, _T ("remarks"), m_strRemarks);
	//}}AFX_FIELD_MAP
}
