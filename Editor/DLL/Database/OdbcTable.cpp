// OdbcTable.cpp: implementation of the COdbcTable class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "OdbcTable.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COdbcTable::COdbcTable(const COdbcDatabase & db)
:	m_db (db)
{

}

COdbcTable::COdbcTable(const COdbcTable & rhs)
:	m_db (rhs.m_db)
{
	m_strCatalog	= rhs.m_strCatalog;
	m_strSchema		= rhs.m_strSchema;
	m_strName		= rhs.m_strName;
	m_strType		= rhs.m_strType;
	m_strRemark		= rhs.m_strRemark;
}

COdbcTable::~COdbcTable()
{

}
