// BcScanRecordset.cpp : implementation file
//

#include "stdafx.h"
#include <math.h>
#include "Database.h"
#include "Debug.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Serialize.h"
#include "compare.h"
#include "Parse.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::BarcodeScans;

extern HINSTANCE hInstance;

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)

FoxjetDatabase::tagBCSCANSTRUCT::tagBCSCANSTRUCT ()
{
}

FoxjetDatabase::tagBCSCANSTRUCT::tagBCSCANSTRUCT (const FoxjetDatabase::tagBCSCANSTRUCT & rhs)
:	m_lID			(rhs.m_lID),
	m_dtTime		(rhs.m_dtTime),
	m_LineID		(rhs.m_LineID),
	m_strScanner	(rhs.m_strScanner),
	m_strTaskname	(rhs.m_strTaskname),
	m_strBarcode	(rhs.m_strBarcode),
	m_lTotalScans	(rhs.m_lTotalScans),
	m_lGoodScans	(rhs.m_lGoodScans)
{
}

FoxjetDatabase::tagBCSCANSTRUCT & FoxjetDatabase::tagBCSCANSTRUCT::operator = (const tagBCSCANSTRUCT & rhs)
{
	if (this != &rhs) {
		m_lID			= rhs.m_lID;
		m_dtTime		= rhs.m_dtTime;
		m_LineID		= rhs.m_LineID;
		m_strScanner	= rhs.m_strScanner;
		m_strTaskname	= rhs.m_strTaskname;
		m_strBarcode	= rhs.m_strBarcode;
		m_lTotalScans	= rhs.m_lTotalScans;
		m_lGoodScans	= rhs.m_lGoodScans;
	}

	return * this;
}

FoxjetDatabase::tagBCSCANSTRUCT::~tagBCSCANSTRUCT ()
{
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::BCSCANSTRUCT & lhs, const FoxjetDatabase::BCSCANSTRUCT & rhs)
{
	return
		lhs.m_lID			== rhs.m_lID &&
		lhs.m_dtTime		== rhs.m_dtTime &&
		lhs.m_LineID		== rhs.m_LineID &&
		lhs.m_strScanner	== rhs.m_strScanner &&
		lhs.m_strTaskname	== rhs.m_strTaskname &&
		lhs.m_lTotalScans	== rhs.m_lTotalScans &&
		lhs.m_lGoodScans	== rhs.m_lGoodScans &&
		lhs.m_LineID		== rhs.m_LineID &&
		lhs.m_strBarcode	== rhs.m_strBarcode;

}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::BCSCANSTRUCT & lhs, const FoxjetDatabase::BCSCANSTRUCT & rhs)
{
	return !(rhs == lhs);
}

DB_API const COdbcRecordset & FoxjetDatabase::operator >> (const COdbcRecordset & rst, BCSCANSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		s.m_lID				= (long)rst.GetFieldValue (m_lpszID);
		s.m_dtTime			= (COleDateTime)rst.GetFieldValue (m_lpszTime).ToOleDateTime();
		s.m_LineID			= (long)rst.GetFieldValue (m_lpszLineID);
		s.m_strScanner		= (CString)rst.GetFieldValue (m_lpszScanner);
		s.m_lTotalScans	= (long)rst.GetFieldValue (m_lpszTotalScans);
		s.m_lGoodScans		= (long)rst.GetFieldValue (m_lpszGoodScans);
		s.m_strTaskname	= (CString)rst.GetFieldValue (m_lpszTaskname);
		s.m_strBarcode		= (CString)rst.GetFieldValue (m_lpszBarcode);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API COdbcRecordset & FoxjetDatabase::operator << (COdbcRecordset & rst, BCSCANSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	CStringArray name, value;

	if (s.m_lID == -1) 
		rst.SetEditMode (COdbcRecordset::addnew);

	name.Add (m_lpszTime);				value.Add (s.m_dtTime.Format (MDB_DATE_STRFTIME));
	name.Add (m_lpszLineID);			value.Add (ToString ((long)s.m_LineID));
	name.Add (m_lpszScanner);			value.Add (s.m_strScanner);
	name.Add (m_lpszTaskname);			value.Add (s.m_strTaskname);
	name.Add (m_lpszBarcode);			value.Add (s.m_strBarcode);
	name.Add (m_lpszTotalScans);		value.Add (ToString ((long)s.m_lTotalScans));
	name.Add (m_lpszGoodScans);			value.Add (ToString ((long)s.m_lGoodScans));

	CString strSQL = FoxjetDatabase::FormatSQL (rst, m_lpszTable, name, value, m_lpszID, s.m_lID);
	TRACEF (strSQL);
	
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

	if (s.m_lID == -1)
		s.m_lID = GetMaxID (rst.GetOdbcDatabase (), m_lpszTable, m_lpszID);
	
	/*
	try {
		if (rst.IsAddNew ())
			rst.SetFieldValue (m_lpszID,	(long)GetNextID (rst.GetOdbcDatabase (), m_lpszTable));

		rst.SetFieldValue ( m_lpszTime,			s.m_dtTime);
		rst.SetFieldValue ( m_lpszLineID,		(long)s.m_LineID);
		rst.SetFieldValue ( m_lpszScanner,		s.m_strScanner);
		rst.SetFieldValue ( m_lpszTaskname,		s.m_strTaskname);
		rst.SetFieldValue ( m_lpszBarcode,		s.m_strBarcode);
		rst.SetFieldValue ( m_lpszTotalScans,	(long)s.m_lTotalScans);
		rst.SetFieldValue ( m_lpszGoodScans,	(long)s.m_lGoodScans);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/
	return rst;
}

DB_API bool FoxjetDatabase::AddScanRecord (COdbcDatabase & database, BCSCANSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	CString str;
	try {
		COdbcRecordset rst (&database);

		s.m_lID = -1;
		rst << s;
		/* TODO: rem
		str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);
		rst.Open (str, CRecordset::dynaset);
		rst.AddNew ();
		rst << s;
		rst.Update ();
		rst.MoveLast ();
		s.m_lID = (long)rst.GetFieldValue (m_lpszID);  
		rst.Close ();
		*/
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::UpdateScanRecord (COdbcDatabase & database, BCSCANSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);

		rst << s;
		bResult = true;
		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), m_lpszTable, m_lpszID, s.m_lID);
		rst.Open (str, CRecordset::dynaset);
		if (!rst.IsEOF ()) {
			rst.Edit ();
			rst << s;
			rst.Update ();
			bResult = true;
		}
		rst.Close ();
		*/
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetScanRecords (COdbcDatabase & database, ULONG lLineID, CArray <BCSCANSTRUCT, BCSCANSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	CString str;

	str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%lu ORDER BY [%s], [%s];"), 
		m_lpszTable, 
		m_lpszLineID, 
		lLineID,
		m_lpszLineID, 
		m_lpszTime);

	v.RemoveAll ();

	try {
		COdbcRecordset rst (&database);

		rst.Open (str);

		while (!rst.IsEOF ()) {
			BCSCANSTRUCT s;

			rst >> s;
			v.Add (s);

			rst.MoveNext ();
		}

		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::ClearScanRecords (COdbcDatabase & database, ULONG LineID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		CString strSQL;
		strSQL.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%lu;"), m_lpszTable, m_lpszLineID, LineID);
		database.ExecuteSQL (strSQL);
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}
