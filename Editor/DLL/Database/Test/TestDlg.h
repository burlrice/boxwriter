// TestDlg.h : header file
//

#if !defined(AFX_TESTDLG_H__03C6BD6A_3FE7_46C9_AFC8_EFAF442B77E0__INCLUDED_)
#define AFX_TESTDLG_H__03C6BD6A_3FE7_46C9_AFC8_EFAF442B77E0__INCLUDED_

#include "OdbcDatabase.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CTestDlg dialog

class CTestDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CTestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CTestDlg)
	enum { IDD = IDD_TEST_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void InitTables();
	HICON m_hIcon;
	FoxjetDatabase::COdbcDatabase m_db;

	// Generated message map functions
	//{{AFX_MSG(CTestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnRequery();
	afx_msg void OnDestroy();
	afx_msg void OnDblclkTables();
	afx_msg void OnAdd();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTDLG_H__03C6BD6A_3FE7_46C9_AFC8_EFAF442B77E0__INCLUDED_)
