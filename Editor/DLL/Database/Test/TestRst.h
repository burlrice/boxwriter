#if !defined(AFX_TESTRST_H__F1B86151_9001_4EA1_9E93_676D59DE39E9__INCLUDED_)
#define AFX_TESTRST_H__F1B86151_9001_4EA1_9E93_676D59DE39E9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestRst.h : header file
//

#include <afxdb.h>

/////////////////////////////////////////////////////////////////////////////
// CTestRst recordset

class CTestRst : public CRecordset
{
public:
	CTestRst(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CTestRst)

// Field/Param Data
	//{{AFX_FIELD(CTestRst, CRecordset)
	BOOL	m_DBVT_BOOL;
	BYTE	m_DBVT_UCHAR;
	int		m_DBVT_SHORT;
	long	m_DBVT_LONG;
	float	m_DBVT_SINGLE;
	double	m_DBVT_DOUBLE;
	CString	m_DBVT_STRING;
	CTime	m_DBVT_DATE;
	CString	m_DBVT_BINARY;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestRst)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTRST_H__F1B86151_9001_4EA1_9E93_676D59DE39E9__INCLUDED_)
