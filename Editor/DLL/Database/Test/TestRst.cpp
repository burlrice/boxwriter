// TestRst.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "TestRst.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestRst

IMPLEMENT_DYNAMIC(CTestRst, CRecordset)

CTestRst::CTestRst(CDatabase* pdb)
	: CRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CTestRst)
	m_DBVT_BOOL = FALSE;
	m_DBVT_UCHAR = 0;
	m_DBVT_SHORT = 0;
	m_DBVT_LONG = 0;
	m_DBVT_SINGLE = 0.0f;
	m_DBVT_DOUBLE = 0.0;
	m_DBVT_STRING = _T("");
	m_DBVT_BINARY = _T("");
	m_nFields = 9;
	//}}AFX_FIELD_INIT
	m_nDefaultType = snapshot;
}


CString CTestRst::GetDefaultConnect()
{
	return _T("ODBC;MarksmanELITE");
}

CString CTestRst::GetDefaultSQL()
{
	return _T("[test]");
}

void CTestRst::DoFieldExchange(CFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CTestRst)
	pFX->SetFieldType(CFieldExchange::outputColumn);
	RFX_Bool(pFX, _T("[DBVT_BOOL]"), m_DBVT_BOOL);
	RFX_Byte(pFX, _T("[DBVT_UCHAR]"), m_DBVT_UCHAR);
	RFX_Int(pFX, _T("[DBVT_SHORT]"), m_DBVT_SHORT);
	RFX_Long(pFX, _T("[DBVT_LONG]"), m_DBVT_LONG);
	RFX_Single(pFX, _T("[DBVT_SINGLE]"), m_DBVT_SINGLE);
	RFX_Double(pFX, _T("[DBVT_DOUBLE]"), m_DBVT_DOUBLE);
	RFX_Text(pFX, _T("[DBVT_STRING]"), m_DBVT_STRING);
	RFX_Date(pFX, _T("[DBVT_DATE]"), m_DBVT_DATE);
	RFX_Text(pFX, _T("[DBVT_BINARY]"), m_DBVT_BINARY);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CTestRst diagnostics

#ifdef _DEBUG
void CTestRst::AssertValid() const
{
	CRecordset::AssertValid();
}

void CTestRst::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG
