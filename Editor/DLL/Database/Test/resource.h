//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Test.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TEST_DIALOG                 102
#define IDR_MAINFRAME                   128
#define LV_RST                          1000
#define TXT_SQL                         1001
#define BTN_REQUERY                     1002
#define LB_TABLES                       1003
#define BTN_ADD                         1004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
