// TestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Test.h"
#include "TestDlg.h"

#include "Database.h"
#include "OdbcRecordset.h"
#include "OdbcTable.h"
#include "Debug.h"
#include "testrst.h"

using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestDlg dialog

CTestDlg::CTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTestDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CTestDlg, CDialog)
	//{{AFX_MSG_MAP(CTestDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_REQUERY, OnRequery)
	ON_WM_DESTROY()
	ON_LBN_DBLCLK(LB_TABLES, OnDblclkTables)
	ON_BN_CLICKED(BTN_ADD, OnAdd)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestDlg message handlers

BOOL CTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	TCHAR szCrLf [] = { 0x0D, 0x0A, 0x00 };
	CString strCrLf (szCrLf);
	CString str = 
		"SELECT Libraries.Name, Tasks.Name, Messages.Name, Messages.Data " + strCrLf + 
		"FROM (Libraries INNER JOIN Tasks ON Libraries.ID = Tasks.LibraryID) " + strCrLf + 
		"INNER JOIN (Messages INNER JOIN Links ON Messages.ID = Links.MessageID) " + strCrLf + 
		"ON Tasks.ID = Links.TaskID;";

	VERIFY (OpenDatabase (m_db));
	InitTables ();
	SetDlgItemText (TXT_SQL, str);
	ASSERT (GetDlgItem (LV_RST));
	CListCtrl & lv = * (CListCtrl *)GetDlgItem (LV_RST);
	lv.InsertColumn (0, "");

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CTestDlg::OnRequery() 
{
	BeginWaitCursor ();
	ASSERT (GetDlgItem (LV_RST));
	CListCtrl & lv = * (CListCtrl *)GetDlgItem (LV_RST);
	LVCOLUMN col;

	lv.DeleteAllItems ();
	
	for (int i = 0; i < 50; i++)
		lv.DeleteColumn (1);

	::ZeroMemory (&col, sizeof (col));
	col.mask = LVCF_TEXT | LVCF_WIDTH;
	col.pszText = "";
	col.cx = 0;
	lv.SetColumn (0, &col);

	lv.ModifyStyle (0, LVS_NOSORTHEADER | LVS_REPORT | LVS_SHOWSELALWAYS);
	lv.SetExtendedStyle (LVS_EX_FULLROWSELECT);
	lv.SetRedraw (FALSE);

	try {
		COdbcRecordset rst (m_db);
		CString str;

		GetDlgItemText (TXT_SQL, str);

		if (rst.Open (CRecordset::snapshot, str)) {
			int nRecord = 0;

			for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
				CString strField = rst.GetFieldName (i);
				CString str;
				int nIndex = rst.GetFieldIndex (strField);

				if (i)
					lv.InsertColumn (i, strField);

				LVCOLUMN col;
				::ZeroMemory (&col, sizeof (col));
				col.mask = LVCF_TEXT | LVCF_WIDTH;
				col.pszText = strField.GetBuffer (0);
				col.cx = 100;
				lv.SetColumn (i, &col);

				str.Format ("field [%u] = %s", nIndex, strField);
				TRACEF (str);
			}

			while (!rst.IsEOF ()) {
				for (int i = 0; i < rst.GetODBCFieldCount (); i++) {
					CString strField = rst.GetFieldName (i);
					CString str;
					int nIndex = rst.GetFieldIndex (strField);
					COdbcVariant varValue;
					CString strValue;
					CString strResult [4] = { "[0]", "[1]", "[2]", "[3]" };

					rst.GetFieldValue (strField, varValue);
					strResult [0] = varValue.ToString ();
					rst.GetFieldValue (i, varValue);
					strResult [1] = varValue.ToString ();
					strResult [2] = rst.GetFieldValue (strField);
					strResult [3] = rst.GetFieldValue (i);

					if (i == 0) 
						lv.InsertItem (nRecord, strResult [0]);
					else
						lv.SetItemText (nRecord, i, strResult [0]);

/*
					CString strTab ('\t', i + 1);
					TRACEF (
						CString ("rst.GetFieldValue: ") + 
						"\n" + strTab + strResult [0] +
						"\n" + strTab + strResult [1] +
						"\n" + strTab + strResult [2] +
						"\n" + strTab + strResult [3]);
*/					
					if ((strResult [0] != strResult [1]) ||
						(strResult [0] != strResult [2]) ||
						(strResult [0] != strResult [3]))
					{
						CString str = 
							"field = " + strField + 
							"\nGetFieldValue (LPCTSTR, COdbcVariant &) = " + strResult [0] + 
							"\nGetFieldValue (short, COdbcVariant &) = " + strResult [1] + 
							"\nGetFieldValue (LPCTSTR, CString &) = " + strResult [2] + 
							"\nGetFieldValue (short, CString &) = " + strResult [3];
						MsgBox (* this, str);
					}
				}

				TRACE0 ("\n\n");

				rst.MoveNext ();
				nRecord++;
			}

			rst.Close ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	lv.SetRedraw (TRUE);
	EndWaitCursor ();
}

void CTestDlg::InitTables()
{
	ASSERT (GetDlgItem (LB_TABLES));
	CListBox & lb = * (CListBox *)GetDlgItem (LB_TABLES);

	for (int i = 0; i < m_db.GetTableCount (); i++) {
		const COdbcTable * p = m_db.GetTable (i);
		ASSERT (p);
		lb.AddString (p->GetName ());
	}
}

void CTestDlg::OnDestroy() 
{
	if (m_db.IsOpen ())
		m_db.Close ();

	CDialog::OnDestroy();
}

void CTestDlg::OnDblclkTables() 
{
	ASSERT (GetDlgItem (LB_TABLES));
	CListBox & lb = * (CListBox *)GetDlgItem (LB_TABLES);
	int nIndex = lb.GetCurSel ();

	if (nIndex != LB_ERR) {
		CString str, strSQL;
		
		lb.GetText (nIndex, str);
		strSQL.Format ("SELECT * FROM [%s];", str);
		SetDlgItemText (TXT_SQL, strSQL);
	}
	else
		MsgBox (* this, "Nothing selected");
}

void CTestDlg::OnAdd() 
{
	try {
		COdbcRecordset rst (m_db);
		CString str = "SELECT * FROM test;";

		if (rst.Open (CRecordset::dynaset, str)) {
			CString strTrace;

			rst.AddNew ();
			rst.SetFieldValue ("DBVT_BOOL",		true);
			rst.SetFieldValue ("DBVT_UCHAR",	(BYTE)33);
			rst.SetFieldValue ("DBVT_SHORT",	(int)44);
			rst.SetFieldValue ("DBVT_LONG",		(long)55);
			rst.SetFieldValue ("DBVT_SINGLE",	(float)6.6);
			rst.SetFieldValue ("DBVT_DOUBLE",	(double)7.7);
			rst.SetFieldValue ("DBVT_STRING",	(CString)"Test");
			rst.SetFieldValue ("DBVT_DATE",		CTime::GetCurrentTime ());
			rst.SetFieldValue ("DBVT_BINARY",	(CString)"000209FE-0000-0000-C000-000000000046");
			rst.Update ();
			rst.MoveLast ();

			bool b			= rst.GetFieldValue ("DBVT_BOOL");
			BYTE ch			= rst.GetFieldValue ("DBVT_UCHAR");
			int i			= rst.GetFieldValue ("DBVT_SHORT");
			long l			= rst.GetFieldValue ("DBVT_LONG");
			float f			= rst.GetFieldValue ("DBVT_SINGLE");
			double d		= rst.GetFieldValue ("DBVT_DOUBLE");
			CString str		= rst.GetFieldValue ("DBVT_STRING");
			CTime tm		= rst.GetFieldValue ("DBVT_DATE");
			CString strBin	= rst.GetFieldValue ("DBVT_BINARY");

			TRACEF (			"DBVT_BOOL:   " + CString (b ? "true" : "false"));
			strTrace.Format (	"DBVT_UCHAR:  %u", ch); TRACEF (strTrace);
			strTrace.Format (	"DBVT_SHORT:  %u", i); TRACEF (strTrace);
			strTrace.Format (	"DBVT_LONG:   %u", l); TRACEF (strTrace);
			strTrace.Format (	"DBVT_SINGLE: %f", f); TRACEF (strTrace);
			strTrace.Format (	"DBVT_DOUBLE: %f", d); TRACEF (strTrace);
			TRACEF (			"DBVT_STRING: " + str);
			TRACEF (			"DBVT_DATE:   " + tm.Format ("%c"));
			TRACEF (			"DBVT_BINARY: " + strBin);

			rst.Close ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
}
/*
C:\DEVELOPMENT\Ritz\DLL\Database\Test\TestDlg.cpp(376) : DBVT_BOOL:   true
C:\DEVELOPMENT\Ritz\DLL\Database\Test\TestDlg.cpp(377) : DBVT_UCHAR:  33
C:\DEVELOPMENT\Ritz\DLL\Database\Test\TestDlg.cpp(378) : DBVT_SHORT:  44
C:\DEVELOPMENT\Ritz\DLL\Database\Test\TestDlg.cpp(379) : DBVT_LONG:   55
C:\DEVELOPMENT\Ritz\DLL\Database\Test\TestDlg.cpp(380) : DBVT_SINGLE: 6.600000
C:\DEVELOPMENT\Ritz\DLL\Database\Test\TestDlg.cpp(381) : DBVT_DOUBLE: 7.700000
C:\DEVELOPMENT\Ritz\DLL\Database\Test\TestDlg.cpp(382) : DBVT_STRING: Test
C:\DEVELOPMENT\Ritz\DLL\Database\Test\TestDlg.cpp(383) : DBVT_DATE:   05/31/01 13:37:31
C:\DEVELOPMENT\Ritz\DLL\Database\Test\TestDlg.cpp(384) : DBVT_BINARY: 000209FE-0000-0000-C000-000000000046
*/
