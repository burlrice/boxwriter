; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CTestRst
LastTemplate=CRecordset
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Test.h"

ClassCount=5
Class1=CTestApp
Class2=CTestDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Class5=CTestRst
Resource3=IDD_TEST_DIALOG

[CLS:CTestApp]
Type=0
HeaderFile=Test.h
ImplementationFile=Test.cpp
Filter=N

[CLS:CTestDlg]
Type=0
HeaderFile=TestDlg.h
ImplementationFile=TestDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=TestDlg.h
ImplementationFile=TestDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_TEST_DIALOG]
Type=1
Class=CTestDlg
ControlCount=5
Control1=LV_RST,SysListView32,1350631425
Control2=TXT_SQL,edit,1350631620
Control3=BTN_REQUERY,button,1342242817
Control4=LB_TABLES,listbox,1352728835
Control5=BTN_ADD,button,1342242816

[CLS:CTestRst]
Type=0
HeaderFile=TestRst.h
ImplementationFile=TestRst.cpp
BaseClass=CRecordset
Filter=N
VirtualFilter=r

[DB:CTestRst]
DB=1
DBType=ODBC
ColumnCount=9
Column1=[DBVT_BOOL], -7, 1
Column2=[DBVT_UCHAR], -6, 1
Column3=[DBVT_SHORT], 5, 2
Column4=[DBVT_LONG], 4, 4
Column5=[DBVT_SINGLE], 7, 4
Column6=[DBVT_DOUBLE], 8, 8
Column7=[DBVT_STRING], 12, 100
Column8=[DBVT_DATE], 11, 16
Column9=[DBVT_BINARY], -11, 16

