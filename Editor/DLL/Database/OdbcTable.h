// OdbcTable.h: interface for the COdbcTable class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ODBCTABLE_H__BDC56929_2097_4653_888C_ABFBD7A101F5__INCLUDED_)
#define AFX_ODBCTABLE_H__BDC56929_2097_4653_888C_ABFBD7A101F5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DbApi.h"
#include "OdbcDatabase.h"

namespace FoxjetDatabase
{
	class DB_API COdbcTable  
	{
		friend class COdbcDatabase;

	public:
		virtual ~COdbcTable();

		CString GetCatalog ()	const { return m_strCatalog; }
		CString GetSchema ()	const { return m_strSchema; }
		CString GetName ()		const { return m_strName; }
		CString GetType ()		const { return m_strType; }
		CString GetRemark ()	const { return m_strRemark; }

		const COdbcDatabase & GetDatabase () const { return m_db; }

	protected:
		COdbcTable(const COdbcDatabase & db);
		COdbcTable();
		COdbcTable(const COdbcTable & rhs);
		COdbcTable & operator = (const COdbcTable & rhs);

		CString m_strCatalog;
		CString m_strSchema;
		CString m_strName;
		CString m_strType;
		CString m_strRemark;

		const COdbcDatabase & m_db;
	};
}; //namespace FoxjetDatabase

#endif // !defined(AFX_ODBCTABLE_H__BDC56929_2097_4653_888C_ABFBD7A101F5__INCLUDED_)
