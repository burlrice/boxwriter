// OptionsRecordset.cpp : implementation file
//

#include "stdafx.h"
#include "Database.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Debug.h"
#include "Serialize.h"
#include "compare.h"
#include "Parse.h"

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Options;

extern HINSTANCE hInstance;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)

FoxjetDatabase::tagOPTIONSSTRUCT::tagOPTIONSSTRUCT ()
{
}

FoxjetDatabase::tagOPTIONSSTRUCT::tagOPTIONSSTRUCT (const tagOPTIONSSTRUCT & rhs)
:	m_lID			(rhs.m_lID),
	m_strOptionDesc (rhs.m_strOptionDesc)
{
}

tagOPTIONSSTRUCT & FoxjetDatabase::tagOPTIONSSTRUCT::operator = (const tagOPTIONSSTRUCT & rhs)
{
	if (this != &rhs) {
		m_lID			= rhs.m_lID;
		m_strOptionDesc = rhs.m_strOptionDesc;
	}

	return * this;
}

FoxjetDatabase::tagOPTIONSSTRUCT::~tagOPTIONSSTRUCT ()
{
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::OPTIONSSTRUCT & lhs, const FoxjetDatabase::OPTIONSSTRUCT & rhs)
{
	return
		lhs.m_lID				== rhs.m_lID &&
		lhs.m_strOptionDesc		== rhs.m_strOptionDesc;
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::OPTIONSSTRUCT & lhs, const FoxjetDatabase::OPTIONSSTRUCT & rhs)
{
	return !(rhs == lhs);
}

DB_API const FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator >> (const COdbcRecordset & rst, OPTIONSSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		s.m_lID				= (long)rst.GetFieldValue			(m_lpszID);
		s.m_strOptionDesc	= (CString)rst.GetFieldValue		(m_lpszOptionDesc);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator << (COdbcRecordset & rst, OPTIONSSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	CStringArray name, value;

	if (s.m_lID == -1) 
		rst.SetEditMode (COdbcRecordset::addnew);

	if (rst.IsAddNew ()) {
		name.Add (m_lpszID);			value.Add (ToString ((long)s.m_lID));
	}

	name.Add (m_lpszOptionDesc);		value.Add (s.m_strOptionDesc);

	CString strSQL = FormatSQL (rst, m_lpszTable, name, value, m_lpszID, s.m_lID);
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));
/*
	try {
		rst.SetFieldValue (m_lpszID,			(long)s.m_lID);
		rst.SetFieldValue (m_lpszOptionDesc,	s.m_strOptionDesc);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/

	return rst;
}


DB_API bool FoxjetDatabase::GetOptionsRecord (COdbcDatabase & database, ULONG lID, OPTIONSSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u"), 
			m_lpszTable, m_lpszID, lID);
		rst.Open (str);

		if (!rst.IsEOF ()) {
			rst >> s;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetOptionsRecords (COdbcDatabase & database, CArray <OPTIONSSTRUCT, OPTIONSSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	v.RemoveAll ();

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s]"), 
			m_lpszTable);
		rst.Open (str);

		while (!rst.IsEOF ()) {
			OPTIONSSTRUCT s;
			rst >> s;
			v.Add (s);
			rst.MoveNext ();
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return v.GetSize () > 0;
}

