// HeadRecordset.cpp : implementation file
//

#include "stdafx.h"
#include <math.h>
#include "Database.h"
#include "Debug.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Serialize.h"
#include "compare.h"
#include "Parse.h"
#include "DbImp.h"
#include "Registry.h"
#include "StackWalker.h"
#include "AppVer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Heads;

extern HINSTANCE hInstance;

CMap <ULONG, ULONG, UINT, UINT> mapMasterEncoderDivisor;
std::vector <std::vector <std::wstring> > vEncoderDivisor;
//CMap <ULONG, ULONG, UINT, UINT> mapEncoderDivisor;

//////////////////////////////////////////////////////////////////////

static CString GetValveKey (ULONG lID);
static CString GetValveKey (const HEADSTRUCT & h);
static CString GetHeadKey (const HEADSTRUCT & h);

//////////////////////////////////////////////////////////////////////

static CString GetHeadKey (const HEADSTRUCT & h)
{
	CString str;

	str.Format (_T ("HEADSTRUCT:%d"), h.m_lID);

	return str;
}

static CString GetValveKey (ULONG lID)
{
	CString str;

	str.Format (_T ("Valve head:%d"), lID);

	return str;
}

static CString GetValveKey (const HEADSTRUCT & h)
{
	return GetValveKey (h.m_lID);
}

static void InsertAscendingByIndex (CArray <VALVESTRUCT, VALVESTRUCT &> & v, VALVESTRUCT h) 
{
	for (int i = 0; i < v.GetSize (); i++) {
		if (h.m_nIndex <= v [i].m_nIndex) {
			v.InsertAt (i, h);
			return;
		}
	}

	v.Add (h);
}

void GetValveRecords (COdbcDatabase & database, HEADSTRUCT & h)
{
	#ifndef _DEBUG
		if (!IsValveHead (h.m_nHeadType)) 
			return;
	#endif //!_DEBUG

	SETTINGSSTRUCT s;

	if (GetSettingsRecord (database, 0, GetValveKey (h), s)) {
		CStringArray v;

		h.m_vValve.RemoveAll ();
		ParsePackets (s.m_strData, v);

		for (int i = 0; i < v.GetSize (); i++) {
			VALVESTRUCT valve;

			if (FromString (v [i], valve)) {
				valve.m_lCardID = h.m_lID;
				InsertAscendingByIndex (h.m_vValve, valve);
				//h.m_vValve.Add (valve);
			}
		}
	}

	if (!h.m_vValve.GetSize ())
		h.m_vValve.Add (VALVESTRUCT ());

	/*
	{ // TODO: rem
		for (int i = 0; i < h.m_vValve.GetSize (); i++) {
			TRACEF (ToString (h.m_vValve [i]));
		}
	}
	*/
}

void UpdateValveRecords (COdbcDatabase & database, const HEADSTRUCT & h)
{
	#ifndef _DEBUG
		if (!IsValveHead (h.m_nHeadType)) 
			return;
	#endif //!_DEBUG

	CString strKey = GetValveKey (h);
	SETTINGSSTRUCT s;

	s.m_lLineID = 0;
	s.m_strKey = strKey;

	for (int i = 0; i < h.m_vValve.GetSize (); i++) {
		VALVESTRUCT valve = h.m_vValve [i];

		valve.m_lCardID = h.m_lID;
		s.m_strData += ToString (valve);
	}

	if (!UpdateSettingsRecord (database, s)) 
		VERIFY (AddSettingsRecord (database, s)); 
}

////////////////////////////////////////////////////////////////////////////////

tagVALVESTRUCT::tagVALVESTRUCT ()
:	m_lID				(-1),
	m_lCardID			(-1),
	m_lPhotocellDelay	(2000),
	m_bEnabled			(true),
	m_bReversed			(false),
	m_type				(IV_12_9),
	m_lHeight			(0),
	m_lPanelID			(-1),
	m_nIndex			(1),
	m_nLinked			(0),
	m_bUpsidedown		(false)
{
}

tagVALVESTRUCT::tagVALVESTRUCT (const tagVALVESTRUCT & rhs)
:	m_lID				(rhs.m_lID),
	m_lCardID			(rhs.m_lCardID),
	m_lPhotocellDelay	(rhs.m_lPhotocellDelay),
	m_bEnabled			(rhs.m_bEnabled),
	m_bReversed			(rhs.m_bReversed),
	m_type				(rhs.m_type),
	m_lHeight			(rhs.m_lHeight),
	m_lPanelID			(rhs.m_lPanelID),
	m_strName			(rhs.m_strName),
	m_nIndex			(rhs.m_nIndex),
	m_nLinked			(rhs.m_nLinked),
	m_bUpsidedown		(rhs.m_bUpsidedown)
{
}

tagVALVESTRUCT & tagVALVESTRUCT::operator = (const tagVALVESTRUCT & rhs)
{
	if (this != &rhs) {
		m_lID				= rhs.m_lID;
		m_lCardID			= rhs.m_lCardID;
		m_lPhotocellDelay	= rhs.m_lPhotocellDelay;
		m_bEnabled			= rhs.m_bEnabled;
		m_bReversed			= rhs.m_bReversed;
		m_type				= rhs.m_type;	
		m_lHeight			= rhs.m_lHeight;
		m_lPanelID			= rhs.m_lPanelID;
		m_strName			= rhs.m_strName;
		m_nIndex			= rhs.m_nIndex;
		m_nLinked			= rhs.m_nLinked;
		m_bUpsidedown		= rhs.m_bUpsidedown;
	}

	return * this;
}

tagVALVESTRUCT::~tagVALVESTRUCT ()
{
}

CString	tagVALVESTRUCT::GetName () const
{
	CString str = m_strName;

	if (!str.GetLength ())
		str.Format (_T ("%d::%d {%d}"), m_lCardID, m_lPanelID, m_lID); 

	return str;
}

int tagVALVESTRUCT::Height () const
{
	HDC hdc = ::GetDC (NULL);
	const double dPI = PI;
	const int nVertDPI = ::GetDeviceCaps (hdc, LOGPIXELSY);
	double dNozzleSpan = GetValveHeight (m_type) / 1000.0;
	double dAngle = 90.0;  // m_dHeadAngle;

	::ReleaseDC (NULL, hdc);

	double dRadians = (double)(90.0 - dAngle) * dPI / 180.0;
	double dHypoteneuse = dNozzleSpan * (double)nVertDPI;
	double d = cos (dRadians) * (double)dHypoteneuse;

	return (int)ceil (d);
}

////////////////////////////////////////////////////////////////////////////////

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp 

tagHEADSTRUCT::tagHEADSTRUCT ()
:	m_lID				(-1),
	m_lPanelID			(-1),
	m_lRelativeHeight	(0),
	m_nChannels			(0),
	m_nHorzRes			(0),
	m_lPhotocellDelay	(0),
	m_nEncoder			(0),
	m_nDirection		(0),
	m_bEnabled			(false),
	m_bInverted			(false),
	m_dHeadAngle		(0),
	m_nPhotocell		(0),
	m_nSharePhoto		(0),
	m_nShareEnc			(0),
	m_nHeadType			(HEADTYPE_FIRST),
	m_bRemoteHead		(false),
	m_bMasterHead		(false),
	m_nIntTachSpeed		(0),
	m_dNozzleSpan		(0),
	m_dPhotocellRate	(0),
	m_nEncoderDivisor	(0),
	m_bDoublePulse		(false),
	m_bUpsidedown		(false),
	m_lLinkedTo			(0),
	m_bDigiNET			(false)
{
	m_nChannels = GetChannels ();
}

tagHEADSTRUCT::tagHEADSTRUCT (const tagHEADSTRUCT & rhs)
:	m_lID				(rhs.m_lID),
	m_lPanelID			(rhs.m_lPanelID),
	m_strName			(rhs.m_strName),
	m_strUID			(rhs.m_strUID),
	m_lRelativeHeight	(rhs.m_lRelativeHeight),
	m_nChannels			(rhs.m_nChannels),
	m_nHorzRes			(rhs.m_nHorzRes),
	m_lPhotocellDelay	(rhs.m_lPhotocellDelay),
	m_nEncoder			(rhs.m_nEncoder),
	m_nDirection		(rhs.m_nDirection),
	m_bEnabled			(rhs.m_bEnabled),
	m_bInverted			(rhs.m_bInverted),
	m_dHeadAngle		(rhs.m_dHeadAngle),
	m_nPhotocell		(rhs.m_nPhotocell),
	m_nSharePhoto		(rhs.m_nSharePhoto),
	m_nShareEnc			(rhs.m_nShareEnc),
	m_nHeadType			(rhs.m_nHeadType),
	m_bRemoteHead		(rhs.m_bRemoteHead),
	m_bMasterHead		(rhs.m_bMasterHead),
	m_nIntTachSpeed		(rhs.m_nIntTachSpeed),
	m_dNozzleSpan		(rhs.m_dNozzleSpan),
	m_dPhotocellRate	(rhs.m_dPhotocellRate),
	m_strSerialParams	(rhs.m_strSerialParams),
	m_nEncoderDivisor	(rhs.m_nEncoderDivisor),
	m_bDoublePulse		(rhs.m_bDoublePulse),
	m_bUpsidedown		(rhs.m_bUpsidedown),
	m_lLinkedTo			(rhs.m_lLinkedTo),
	m_bDigiNET			(rhs.m_bDigiNET)
{
	m_vValve.Copy (rhs.m_vValve);
//	TRACEF ("tagHEADSTRUCT::tagHEADSTRUCT");

	m_nChannels = GetChannels ();
}

tagHEADSTRUCT & tagHEADSTRUCT::operator = (const tagHEADSTRUCT & rhs)
{
	if (this != &rhs) {
		m_lID				= rhs.m_lID;
		m_lPanelID			= rhs.m_lPanelID;
		m_strName			= rhs.m_strName;
		m_strUID			= rhs.m_strUID;
		m_lRelativeHeight	= rhs.m_lRelativeHeight;
		m_nChannels			= rhs.m_nChannels;
		m_nHorzRes			= rhs.m_nHorzRes;
		m_lPhotocellDelay	= rhs.m_lPhotocellDelay;
		m_nEncoder			= rhs.m_nEncoder;
		m_nDirection		= rhs.m_nDirection;
		m_bEnabled			= rhs.m_bEnabled;
		m_bInverted			= rhs.m_bInverted;
		m_dHeadAngle		= rhs.m_dHeadAngle;
		m_nPhotocell		= rhs.m_nPhotocell;
		m_nSharePhoto		= rhs.m_nSharePhoto;
		m_nShareEnc			= rhs.m_nShareEnc;
		m_nHeadType			= rhs.m_nHeadType;
		m_bRemoteHead		= rhs.m_bRemoteHead;
		m_bMasterHead		= rhs.m_bMasterHead;
		m_nIntTachSpeed		= rhs.m_nIntTachSpeed;
		m_dNozzleSpan		= rhs.m_dNozzleSpan;
		m_dPhotocellRate	= rhs.m_dPhotocellRate;
		m_strSerialParams	= rhs.m_strSerialParams;
		m_nEncoderDivisor	= rhs.m_nEncoderDivisor;
		m_bDoublePulse		= rhs.m_bDoublePulse;
		m_bUpsidedown		= rhs.m_bUpsidedown;
		m_lLinkedTo			= rhs.m_lLinkedTo;
		m_bDigiNET			= rhs.m_bDigiNET;

		m_vValve.RemoveAll ();
		m_vValve.Copy (rhs.m_vValve);

		m_nChannels = GetChannels ();
	}

//	TRACEF ("tagHEADSTRUCT::operator =");

	return * this;
}

tagHEADSTRUCT::~tagHEADSTRUCT ()
{
}

int tagHEADSTRUCT::Height () const
{
	HDC hdc = ::GetDC (NULL);
	const double dPI = PI;
	const int nVertDPI = ::GetDeviceCaps (hdc, LOGPIXELSY);
	double dNozzleSpan = m_dNozzleSpan;

	if (IsValveHead (m_nHeadType)) {
		dNozzleSpan = 0.0;

		for (int i = 0; i < m_vValve.GetSize (); i++)
			dNozzleSpan += ((double)GetValveHeight (m_vValve [i].m_type) / 1000.0);
	}

	::ReleaseDC (NULL, hdc);

	double dRadians = (double)(90.0 - m_dHeadAngle) * dPI / 180.0;
	double dHypoteneuse = dNozzleSpan * (double)nVertDPI;
	double d = cos (dRadians) * (double)dHypoteneuse;

	return (int)ceil (d);
}

double tagHEADSTRUCT::GetSpan () const
{
	double dNozzleSpan = m_dNozzleSpan;

	if (IsValveHead (m_nHeadType)) {
		dNozzleSpan = 0.0;

		for (int i = 0; i < m_vValve.GetSize (); i++)
			dNozzleSpan += ((double)GetValveHeight (m_vValve [i].m_type) / 1000.0);
	}

	return dNozzleSpan;
}

int tagHEADSTRUCT::GetChannels () const
{
	int nChannels = m_nChannels;

	if (IsValveHead (m_nHeadType)) {
		nChannels = 0;

		for (int i = 0; i < m_vValve.GetSize (); i++)
			nChannels += GetValveChannels (m_vValve [i].m_type);
	}

	return nChannels;
}

double tagHEADSTRUCT::GetRes () const
{
	double dRes = m_nHorzRes / max (1, m_nEncoderDivisor);

	#ifdef __IPPRINTER__
	if (m_bDigiNET) {
		if (m_nEncoderDivisor == 3)
			dRes *= 2;
	}
	else
		dRes = (m_nHorzRes * 2) / max (1, m_nEncoderDivisor);
	#endif //__IPPRINTER__

	if (IsValveHead (m_nHeadType)) {
		int nDivideBy = (m_nHorzRes * 2) / 6;
		
		//nRes = DivideAndRound ((double)nDivideBy, (double)m_nEncoderDivisor);
		dRes = (double)nDivideBy / (double)m_nEncoderDivisor;
	}
	else {
		#ifdef __WINPRINTER__
		if (m_nEncoderDivisor == 3)
			dRes *= 2;
		#endif
	}

	return dRes;
}

double tagHEADSTRUCT::GetMaxRes () const
{
	int nEncoderDivisor = IsValveHead (m_nHeadType) ? 4 : 1;
	double dRes = m_nHorzRes / max (1, nEncoderDivisor);

	#ifdef __IPPRINTER__
	if (m_bDigiNET) {
		if (m_nEncoderDivisor == 3)
			dRes *= 2;
	}
	else
		dRes = (m_nHorzRes * 2) / max (1, m_nEncoderDivisor);
	#endif //__IPPRINTER__

	if (IsValveHead (m_nHeadType)) {
		int nDivideBy = (m_nHorzRes * 2) / 6;
		
		//nRes = DivideAndRound ((double)nDivideBy, (double)nEncoderDivisor);
		dRes = (double)nDivideBy / (double)nEncoderDivisor;
	}

	return dRes;
}

bool tagHEADSTRUCT::IsUSB () const
{
	ULONG lAddr = _tcstoul (m_strUID, NULL, 16);

	return lAddr < 0x300;
}

////////////////////////////////////////////////////////////////////////////////

static class CDefHead : public FoxjetDatabase::HEADSTRUCT 
{
public:
	CDefHead ()
	{
		m_lID				= -1;
		m_lPanelID			= -1;
		m_strName			= _T ("[defHead:Debug]");
		m_strUID			= _T ("[defHead:Debug]");
		m_lRelativeHeight	= 0;
		m_nChannels			= 256;
		m_nHorzRes			= 300;
		m_lPhotocellDelay	= 0;
		m_nDirection		= LTOR;
		m_bEnabled			= false;
		m_dHeadAngle		= 90.0;
		m_nEncoder			= 0;
		m_bInverted			= 0;
		m_nPhotocell		= 0;
		m_nSharePhoto		= 0;
		m_nShareEnc			= 0;
		m_nHeadType			= HEADTYPE_FIRST;
		m_bRemoteHead		= 0;
		m_bMasterHead		= 0;
		m_nIntTachSpeed		= 60;
		m_dNozzleSpan		= 1.88;
		m_dPhotocellRate	= 0.0;
		m_strSerialParams	= _T("9600,N,8,1,0");
		m_nEncoderDivisor	= 2;
		m_bDoublePulse		= false;
	}
} const defHead;

DB_API const FoxjetDatabase::HEADSTRUCT & FoxjetDatabase::GetDefaultHead ()
{
	return ::defHead;
}

DB_API int FoxjetDatabase::GetHeadChannels (HEADTYPE type)
{
	switch (type) {
	case IV_72:				return 72;
	case GRAPHICS_384_128:	return 128;
	case UR2:
	case GRAPHICS_768_256_L310:
	case GRAPHICS_768_256_L330:
	case GRAPHICS_768_256_0:
	case GRAPHICS_768_256:	return 256;
	case UR4:				return 384;
	}
	
	return 32;
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::HEADSTRUCT & lhs, const FoxjetDatabase::HEADSTRUCT & rhs)
{
	return
		lhs.m_lID				== rhs.m_lID &&
		lhs.m_lPanelID			== rhs.m_lPanelID &&
		lhs.m_strName			== rhs.m_strName &&
		lhs.m_strUID			== rhs.m_strUID &&
		lhs.m_lRelativeHeight	== rhs.m_lRelativeHeight &&
		lhs.m_nChannels			== rhs.m_nChannels &&
		lhs.m_nHorzRes			== rhs.m_nHorzRes &&
		lhs.m_lPhotocellDelay	== rhs.m_lPhotocellDelay &&
		lhs.m_nEncoder			== rhs.m_nEncoder &&
		lhs.m_nDirection		== rhs.m_nDirection &&
		lhs.m_bEnabled			== rhs.m_bEnabled &&
		lhs.m_bInverted			== rhs.m_bInverted &&
		lhs.m_dHeadAngle		== rhs.m_dHeadAngle &&
		lhs.m_nPhotocell		== rhs.m_nPhotocell &&
		lhs.m_nSharePhoto		== rhs.m_nSharePhoto &&
		lhs.m_nShareEnc			== rhs.m_nShareEnc &&
		lhs.m_nHeadType			== rhs.m_nHeadType &&
		lhs.m_bRemoteHead		== rhs.m_bRemoteHead &&
		lhs.m_bMasterHead		== rhs.m_bMasterHead &&
		lhs.m_nIntTachSpeed		== rhs.m_nIntTachSpeed &&
		lhs.m_dNozzleSpan		== rhs.m_dNozzleSpan &&
		lhs.m_dPhotocellRate	== rhs.m_dPhotocellRate &&
		lhs.m_strSerialParams	== rhs.m_strSerialParams &&
		lhs.m_nEncoderDivisor	== rhs.m_nEncoderDivisor &&
		lhs.m_bDoublePulse		== rhs.m_bDoublePulse &&
		lhs.m_lLinkedTo			== rhs.m_lLinkedTo &&
		lhs.m_bDigiNET			== rhs.m_bDigiNET;
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::HEADSTRUCT & lhs, const FoxjetDatabase::HEADSTRUCT & rhs)
{
	return !(rhs == lhs);
}

template <class T>
inline T implode (const std::vector<std::vector<T> > & v, const T & delim)
{
	T result;

	for (UINT i = 0; i < v.size (); i++) {
		result += '{';
		result += implode (v [i], delim);
		result += '}';
	}

	return result;
}

DB_API const COdbcRecordset & FoxjetDatabase::operator >> (const COdbcRecordset & rst, 
														   HEADSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		COdbcDatabase & db = * (COdbcDatabase *)(LPVOID)&rst.GetOdbcDatabase ();

		s.m_lID				= (long)rst.GetFieldValue (m_lpszID);
		s.m_lPanelID		= (long)rst.GetFieldValue (m_lpszPanelID);
		s.m_strName			= (CString)rst.GetFieldValue (m_lpszName);
		s.m_strUID			= (CString)rst.GetFieldValue (m_lpszUID);
		s.m_lRelativeHeight = (long)rst.GetFieldValue (m_lpszRelativeHeight);
		s.m_nChannels		= (long)rst.GetFieldValue (m_lpszChannels);
		s.m_nHorzRes		= (int)rst.GetFieldValue (m_lpszHorzRes);
		s.m_lPhotocellDelay = (long)rst.GetFieldValue (m_lpszPhotocellDelay);
		s.m_bEnabled		= (bool)rst.GetFieldValue (m_lpszEnabled);
		s.m_nEncoder		= ( int )rst.GetFieldValue (m_lpszExtEnc);
		s.m_nDirection		= (int)rst.GetFieldValue (m_lpszDirection);
		s.m_dHeadAngle		= (double)rst.GetFieldValue (m_lpszHeadAngle);
		s.m_bInverted		= (bool) rst.GetFieldValue (m_lpszInverted);
		s.m_nPhotocell		= (int) rst.GetFieldValue (m_lpszPhotocell);
		s.m_nSharePhoto		= (int) rst.GetFieldValue (m_lpszSharePhotocell);
		s.m_nShareEnc		= (int) rst.GetFieldValue (m_lpszShareEncoder);
		s.m_nHeadType		= (HEADTYPE)(int)rst.GetFieldValue (m_lpszHeadType);
		s.m_bRemoteHead		= (BOOL) rst.GetFieldValue (m_lpszRemoteHead);
		s.m_bMasterHead		= (BOOL) rst.GetFieldValue (m_lpszMasterHead);
		s.m_nIntTachSpeed	= (int) rst.GetFieldValue (m_lpszIntTachSpeed); // CGH 20020731
		s.m_dNozzleSpan		= (double) rst.GetFieldValue ( m_lpszNozzleSpan); // CGH 20020731
		s.m_dPhotocellRate	= (double)rst.GetFieldValue (m_lpszPhotocellRate);
		s.m_strSerialParams	= (CString)rst.GetFieldValue (m_lpszSerialParams);
		s.m_nEncoderDivisor	= (int)rst.GetFieldValue (m_lpszEncoderDivisor);
		s.m_bDoublePulse	= (bool)rst.GetFieldValue (m_lpszDoublePulse);

		GetValveRecords (db, s);

		if (IsValveHead (s)) {
			s.m_nChannels		= s.GetChannels ();
			s.m_bDoublePulse	= false;
		}

		{
			SETTINGSSTRUCT config;

			if (GetSettingsRecord (db, 0, GetHeadKey (s), config)) {
				CStringArray v;

				FromString (config.m_strData, v);

				if (v.GetSize () > 0)	s.m_bUpsidedown = _ttoi (v [0]) ? true : false;
				if (v.GetSize () > 1)	s.m_lLinkedTo	= _tcstoul (v [1], NULL, 10);
				if (v.GetSize () > 2)	s.m_bDigiNET	= _ttoi (v [2]) ? true : false;
			}
		}

	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	{
		CString strDebugFile = GetHomeDir () + _T ("\\.nEncoderDivisor");

		if (::mapMasterEncoderDivisor.GetSize () && ::GetFileAttributes (strDebugFile) != -1) {
			COdbcDatabase & db = * (COdbcDatabase *)(LPVOID)&rst.GetOdbcDatabase ();
			bool bLog = false, bStartup = false;
			CString str;
			ULONG lPrinterID = GetPrinterID (db);
			COdbcRecordset rst (db);

			str.Format (
				//_T ("SELECT Heads.EncoderDivisor, Count(Heads.EncoderDivisor) AS CountOfEncoderDivisor ") 
				//_T ("FROM (Heads INNER JOIN Panels ON Heads.PanelID = Panels.ID) INNER JOIN Lines ON Panels.LineID = Lines.ID ") 
				//_T ("GROUP BY Heads.EncoderDivisor, Heads.UID ") 
				//_T ("HAVING (((Heads.UID)='%s')) ") 
				//_T ("ORDER BY Count(Heads.EncoderDivisor) DESC; "),

				_T ("SELECT Count(Heads.EncoderDivisor) AS CountOfEncoderDivisor ")
				_T ("FROM (Heads INNER JOIN Panels ON Heads.PanelID = Panels.ID) INNER JOIN Lines ON Panels.LineID = Lines.ID ")
				_T ("GROUP BY Heads.EncoderDivisor, Heads.UID ")
				_T ("HAVING (((Heads.UID)='%s')) ")
				_T ("ORDER BY Count(Heads.EncoderDivisor) DESC; "),

				s.m_strUID);

			try {
				TRACEF (str);
				
				std::vector <std::vector <std::wstring> > v;

				for (rst.Open (str); !rst.IsEOF (); rst.MoveNext ()) {
					std::vector <std::wstring> vRow;

					for (short i = 0; i < rst.GetODBCFieldCount (); i++)
						vRow.push_back ((LPCTSTR)(CString)rst.GetFieldValue (i));
	
					v.push_back (vRow);
				}

				TRACEF (implode (v, std::wstring (_T (","))));

				if (::vEncoderDivisor.size () == 0) {
					::vEncoderDivisor = v;
					bLog = (v.size () > 1);
					bStartup = true;
				}

				if (implode (v, std::wstring (_T (","))) != implode (::vEncoderDivisor, std::wstring (_T (",")))) {
					::vEncoderDivisor = v;
					bLog = (v.size () > 1);
				}
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION_TRACEONLY (e); }
			catch (CMemoryException * e)	{ HANDLEEXCEPTION_TRACEONLY (e); }

			if (bLog) {
				REPORTSTRUCT r;
				FoxjetCommon::VERSIONSTRUCT v = FoxjetCommon::verApp;

				r.m_dtTime = COleDateTime::GetCurrentTime ();
				r.m_lPrinterID = lPrinterID;
				r.m_lType = REPORT_DIAGNOSTIC;

				r.m_dtTime += COleDateTimeSpan (0, 0, 0, 1);
				r.m_strTaskName = _T ("cmdline");
				r.m_strCounts.Format (_T ("%d.%d.%04d.%03d %s: %s"), v.m_nMajor, v.m_nMinor, v.m_nCustomMajor, v.m_nInternal, GetIpAddress (), ::GetCommandLine ());
				AddPrinterReportRecord (db, REPORT_DIAGNOSTIC, r, lPrinterID);

				r.m_dtTime += COleDateTimeSpan (0, 0, 0, 1);
				r.m_strTaskName = _T ("Changed externally") + CString (bStartup ? _T (" [startup]") : _T (""));
				AddPrinterReportRecord (db, REPORT_DIAGNOSTIC, r, lPrinterID);

				StackWalker stack;
				stack.ShowCallstack ();
				r.m_dtTime += COleDateTimeSpan (0, 0, 0, 1);
				r.m_strTaskName = _T ("call stack");
				r.m_strCounts = stack.GetOutput ();
				AddPrinterReportRecord (db, REPORT_DIAGNOSTIC, r, lPrinterID);
			}
		}
	}

	return rst;
}

DB_API COdbcRecordset & FoxjetDatabase::operator << (COdbcRecordset & rst, HEADSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());
	COdbcDatabase & db = * (COdbcDatabase *)(LPVOID)&rst.GetOdbcDatabase ();

	CStringArray name, value;

	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	if (s.m_lID == -1) 
		rst.SetEditMode (COdbcRecordset::addnew);

	name.Add (m_lpszPanelID);			value.Add (ToString ((long)s.m_lPanelID));
	name.Add (m_lpszName);				value.Add (s.m_strName);
	name.Add (m_lpszUID);				value.Add (s.m_strUID);
	name.Add (m_lpszRelativeHeight);	value.Add (ToString ((long)s.m_lRelativeHeight));
	name.Add (m_lpszChannels);			value.Add (ToString ((long)s.m_nChannels));
	name.Add (m_lpszHorzRes);			value.Add (ToString ((int)s.m_nHorzRes));
	name.Add (m_lpszPhotocellDelay);	value.Add (ToString ((long)s.m_lPhotocellDelay));
	name.Add (m_lpszEnabled);			value.Add (ToString ((int)s.m_bEnabled));
	name.Add (m_lpszExtEnc);			value.Add (ToString ((int)s.m_nEncoder));
	name.Add (m_lpszDirection);			value.Add (ToString ((int)s.m_nDirection));
	name.Add (m_lpszHeadAngle);			value.Add (ToString ((double)s.m_dHeadAngle));
	name.Add (m_lpszInverted);			value.Add (ToString ((int)s.m_bInverted));
	name.Add (m_lpszPhotocell);			value.Add (ToString ((int)s.m_nPhotocell));
	name.Add (m_lpszSharePhotocell);	value.Add (ToString ((int)s.m_nSharePhoto));
	name.Add (m_lpszShareEncoder);		value.Add (ToString ((int)s.m_nShareEnc));
	name.Add (m_lpszHeadType);			value.Add (ToString ((int)s.m_nHeadType));
	name.Add (m_lpszRemoteHead);		value.Add (ToString ((BOOL)s.m_bRemoteHead));
	name.Add (m_lpszMasterHead);		value.Add (ToString ((BOOL)s.m_bMasterHead));
	name.Add (m_lpszIntTachSpeed);		value.Add (ToString ((int)s.m_nIntTachSpeed));
	name.Add (m_lpszNozzleSpan);		value.Add (ToString ((double)s.m_dNozzleSpan));
	name.Add (m_lpszPhotocellRate);		value.Add (ToString ((double)s.m_dPhotocellRate));
	name.Add (m_lpszSerialParams);		value.Add (s.m_strSerialParams);
	name.Add (m_lpszEncoderDivisor);	value.Add (ToString (s.m_nEncoderDivisor));
	name.Add (m_lpszDoublePulse);		value.Add (ToString (s.m_bDoublePulse));

	CString strSQL = FormatSQL (rst, m_lpszTable, name, value, m_lpszID, s.m_lID);

	{
		CString strDebugFile = GetHomeDir () + _T ("\\.nEncoderDivisor");

		if (::GetFileAttributes (strDebugFile) != -1) {
			ULONG lUID = _tcstoul (s.m_strUID, NULL, 16);
			UINT n = -1;
			CString strFind;
			ULONG lPrinterID = GetPrinterID (db);

			::mapMasterEncoderDivisor.Lookup (lUID, n);
			strFind.Format (_T ("\"EncoderDivisor\"=%d"), n);

			if (lPrinterID == GetMasterPrinterID (db))
				if (s.m_nEncoderDivisor != n)
					::mapMasterEncoderDivisor.SetAt (lUID, s.m_nEncoderDivisor);

			if (strSQL.Find (strFind) == -1) {
				REPORTSTRUCT s;
				FoxjetCommon::VERSIONSTRUCT v = FoxjetCommon::verApp;

				s.m_dtTime = COleDateTime::GetCurrentTime ();
				s.m_lPrinterID = lPrinterID;
				s.m_lType = REPORT_DIAGNOSTIC;
			
				s.m_dtTime += COleDateTimeSpan (0, 0, 0, 1);
				s.m_strTaskName = _T ("cmdline");
				s.m_strCounts.Format (_T ("%d.%d.%04d.%03d %s: %s"), v.m_nMajor, v.m_nMinor, v.m_nCustomMajor, v.m_nInternal, GetIpAddress (), ::GetCommandLine ());
				AddPrinterReportRecord (db, REPORT_DIAGNOSTIC, s, lPrinterID);

				s.m_dtTime += COleDateTimeSpan (0, 0, 0, 1);
				s.m_strTaskName = _T ("SQL");
				s.m_strCounts = strSQL;
				AddPrinterReportRecord (db, REPORT_DIAGNOSTIC, s, lPrinterID);

				StackWalker stack;
				stack.ShowCallstack ();
				s.m_dtTime += COleDateTimeSpan (0, 0, 0, 1);
				s.m_strTaskName = _T ("call stack");
				s.m_strCounts = stack.GetOutput ();
				AddPrinterReportRecord (db, REPORT_DIAGNOSTIC, s, lPrinterID);
			}
		}
	}

	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

	if (s.m_lID == -1)
		s.m_lID = GetMaxID (rst.GetOdbcDatabase (), m_lpszTable, m_lpszID);

	UpdateValveRecords (db, s);

	{
		SETTINGSSTRUCT config;
		CStringArray v;

		config.m_lLineID = 0;
		config.m_strKey	= GetHeadKey (s);
		v.Add (ToString (s.m_bUpsidedown));
		v.Add (ToString (s.m_lLinkedTo));
		v.Add (ToString ((long)s.m_bDigiNET));
		config.m_strData = ToString (v);
			
		if (!UpdateSettingsRecord (db, config))
			VERIFY (AddSettingsRecord (db, config));
	}

/*
	try {
		COdbcDatabase & db = * (COdbcDatabase *)(LPVOID)&rst.GetOdbcDatabase ();

		if (rst.IsAddNew ())
			rst.SetFieldValue (m_lpszID,	(long)GetNextID (rst.GetOdbcDatabase (), m_lpszTable));

		rst.SetFieldValue (m_lpszPanelID,			(long)s.m_lPanelID);
		rst.SetFieldValue (m_lpszName,				s.m_strName);
		rst.SetFieldValue (m_lpszUID,				s.m_strUID);
		rst.SetFieldValue (m_lpszRelativeHeight,	(long)s.m_lRelativeHeight);
		rst.SetFieldValue (m_lpszChannels,			(long)s.m_nChannels);
		rst.SetFieldValue (m_lpszHorzRes,			(int)s.m_nHorzRes);
		rst.SetFieldValue (m_lpszPhotocellDelay,	(long)s.m_lPhotocellDelay);
		rst.SetFieldValue (m_lpszEnabled,			s.m_bEnabled);
		rst.SetFieldValue (m_lpszExtEnc,			(int)s.m_nEncoder);
		rst.SetFieldValue (m_lpszDirection,			(int)s.m_nDirection);
		rst.SetFieldValue (m_lpszHeadAngle,			(double)s.m_dHeadAngle);
		rst.SetFieldValue (m_lpszInverted,			s.m_bInverted);
		rst.SetFieldValue (m_lpszPhotocell,			(int)s.m_nPhotocell);
		rst.SetFieldValue (m_lpszSharePhotocell,	(int)s.m_nSharePhoto);
		rst.SetFieldValue (m_lpszShareEncoder,		(int)s.m_nShareEnc);
		rst.SetFieldValue (m_lpszHeadType,			(int)s.m_nHeadType);
		rst.SetFieldValue (m_lpszRemoteHead,		(BOOL)s.m_bRemoteHead);
		rst.SetFieldValue (m_lpszMasterHead,		(BOOL)s.m_bMasterHead);
		rst.SetFieldValue (m_lpszIntTachSpeed,		(int)s.m_nIntTachSpeed);	// CGH 20020731
		rst.SetFieldValue (m_lpszNozzleSpan,		(double)s.m_dNozzleSpan);	// CGH 20020731
		rst.SetFieldValue (m_lpszPhotocellRate,		(double)s.m_dPhotocellRate);
		rst.SetFieldValue (m_lpszSerialParams,		(CString)s.m_strSerialParams);
		rst.SetFieldValue (m_lpszEncoderDivisor,	s.m_nEncoderDivisor);
		rst.SetFieldValue (m_lpszDoublePulse,		s.m_bDoublePulse);

		UpdateValveRecords (db, s);

		{
			SETTINGSSTRUCT config;
			CStringArray v;

			config.m_lLineID = 0;
			config.m_strKey	= GetHeadKey (s);
			v.Add (ToString (s.m_bUpsidedown));
			v.Add (ToString (s.m_lLinkedTo));
			v.Add (ToString ((long)s.m_bDigiNET));
			config.m_strData = ToString (v);
			
			if (!UpdateSettingsRecord (db, config))
				VERIFY (AddSettingsRecord (db, config));
		}
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/
	return rst;
}

void DB_API HEADSTRUCT::WriteProfile (HKEY hKey, const CString & strSection, ULONG lLineID)
{
	WriteProfileInt		(hKey, strSection, m_lpszID,				m_lID);
	WriteProfileInt		(hKey, strSection, m_lpszPanelID,			m_lPanelID);
	WriteProfileString	(hKey, strSection, m_lpszName,				m_strName);
	WriteProfileString	(hKey, strSection, m_lpszUID,				m_strUID);
	WriteProfileInt		(hKey, strSection, m_lpszRelativeHeight,	m_lRelativeHeight);
	WriteProfileInt		(hKey, strSection, m_lpszChannels,			m_nChannels);
	WriteProfileInt		(hKey, strSection, m_lpszHorzRes,			m_nHorzRes);
	WriteProfileInt		(hKey, strSection, m_lpszPhotocellDelay,	m_lPhotocellDelay);
	WriteProfileInt		(hKey, strSection, m_lpszEnabled,			m_bEnabled);
	WriteProfileInt		(hKey, strSection, m_lpszExtEnc,			m_nEncoder);
	WriteProfileInt		(hKey, strSection, m_lpszDirection,			m_nDirection);
	WriteProfileInt		(hKey, strSection, m_lpszHeadAngle,			(int)(m_dHeadAngle * 100.0));
	WriteProfileInt		(hKey, strSection, m_lpszInverted,			m_bInverted);
	WriteProfileInt		(hKey, strSection, m_lpszPhotocell,			m_nPhotocell);
	WriteProfileInt		(hKey, strSection, m_lpszSharePhotocell,	m_nSharePhoto);
	WriteProfileInt		(hKey, strSection, m_lpszShareEncoder,		m_nShareEnc);
	WriteProfileInt		(hKey, strSection, m_lpszHeadType,			m_nHeadType);
	WriteProfileInt		(hKey, strSection, m_lpszRemoteHead,		m_bRemoteHead);
	WriteProfileInt		(hKey, strSection, m_lpszMasterHead,		m_bMasterHead);
	WriteProfileInt		(hKey, strSection, m_lpszIntTachSpeed,		m_nIntTachSpeed);
	WriteProfileInt		(hKey, strSection, m_lpszNozzleSpan,		(int)(m_dNozzleSpan * 100.0));
	WriteProfileInt		(hKey, strSection, m_lpszPhotocellRate,		(int)(m_dPhotocellRate * 100.0));
	WriteProfileString	(hKey, strSection, m_lpszSerialParams,		m_strSerialParams);
	WriteProfileInt		(hKey, strSection, m_lpszEncoderDivisor,	m_nEncoderDivisor);
	WriteProfileInt		(hKey, strSection, m_lpszDoublePulse,		m_bDoublePulse);
	WriteProfileInt		(hKey, strSection, Tasks::m_lpszLineID,		lLineID);
	WriteProfileInt		(hKey, strSection, m_lpszDigiNET,			m_bDigiNET);
	
}

DB_API int FoxjetDatabase::GetMinFontSize (COdbcDatabase & database, int nHead)
{
	ASSERT (database.IsOpen ());
	return 5;
}

DB_API int FoxjetDatabase::GetMaxFontSize (COdbcDatabase & database, int nHead)
{
	DBMANAGETHREADSTATE (database);

	HEADSTRUCT hs;
	
	ASSERT (database.IsOpen ());

	if (nHead == ALL) {
		int nMax = 0;
		CString str;

		str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);

		try {
			COdbcRecordset rst (database);
			
			rst.Open (str);

			for (rst.MoveFirst (); !rst.IsEOF (); rst.MoveNext ())
				nMax = max (nMax, (int)(rst.GetFieldValue (m_lpszChannels)));

			rst.Close ();
		}
		catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

		return nMax;
	}
	else if (!GetHeadRecord (database, nHead, hs))
		GetFirstHeadRecord (database, hs);

	return hs.m_nChannels;
}

DB_API UINT FoxjetDatabase::GetHeadRecordCount (COdbcDatabase & database)
{
	DBMANAGETHREADSTATE (database);

	UINT nCount = 0;
	CString str;

	str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (database);

		rst.Open (str);
		nCount = rst.CountRecords ();
		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return nCount;
}

DB_API bool FoxjetDatabase::GetHeadRecords (COdbcDatabase & database, CArray <HEADSTRUCT, HEADSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	CString str;

	str.Format (_T ("SELECT * FROM [%s] ORDER BY %s;"), m_lpszTable, m_lpszName);

	v.RemoveAll ();

	try {
		COdbcRecordset rst (database);
		rst.Open (str);

		while (!rst.IsEOF ()) {
			HEADSTRUCT h;
			rst >> h;
			v.Add (h);
			rst.MoveNext ();	
		}

		bResult = v.GetSize () > 0;
		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetHeadRecord (COdbcDatabase & database, ULONG lID, HEADSTRUCT & hs)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, lID);
		rst.Open (str);

		if (!rst.IsEOF ()) {
			rst >> hs;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API ULONG FoxjetDatabase::GetHeadID (COdbcDatabase & database, const CString & strUID, ULONG lPrinterID)
{
	DBMANAGETHREADSTATE (database);

	ULONG lID = NOTFOUND;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);
		CString str;

		if (lPrinterID == 0 || lPrinterID == ALL || !database.HasTable (Printers::m_lpszTable)) 
			str.Format (_T ("SELECT [%s] FROM [%s] WHERE [%s]='%s';"), 
				m_lpszID, m_lpszTable, m_lpszUID, strUID);
		else {
			str.Format (
				_T ("SELECT [%s].* FROM ([%s] INNER JOIN [%s] ON [%s].[%s] = [%s].[%s])")
				_T (" INNER JOIN [%s] ON [%s].[%s] = [%s].[%s]")
				_T (" WHERE [%s].[%s]=%d AND [%s].[%s]='%s';"),
				Heads::m_lpszTable, 
				Lines::m_lpszTable, 
				Panels::m_lpszTable, 
				Lines::m_lpszTable, Lines::m_lpszID,
				Panels::m_lpszTable, Panels::m_lpszLineID,
				Heads::m_lpszTable, 
				Panels::m_lpszTable, Panels::m_lpszID,
				Heads::m_lpszTable, Heads::m_lpszPanelID,
				Lines::m_lpszTable, Lines::m_lpszPrinterID,			lPrinterID, 
				Heads::m_lpszTable, Heads::m_lpszUID,				strUID);
		}

		//TRACEF (str);
		rst.Open (str);

		if (!rst.IsEOF ()) 
			lID = (long)rst.GetFieldValue (m_lpszID);

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return lID;
}

DB_API ULONG FoxjetDatabase::GetHeadID (COdbcDatabase & database, ULONG lLineID, const CString & strHead)
{
	DBMANAGETHREADSTATE (database);

	ULONG lID = NOTFOUND;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);
		CString str;

//		SELECT DISTINCTROW Heads.ID
//		FROM Panels INNER JOIN Heads ON Panels.ID = Heads.PanelID
//		WHERE (((Panels.LineID)=0) AND ((Heads.Name)=""));

		str.Format (
			_T ("SELECT DISTINCTROW [%s].[%s]")
			_T ("FROM [%s] INNER JOIN [%s] ON [%s].[%s] = [%s].[%s]")
			_T ("WHERE ((([%s].[%s])=%u) AND (([%s].[%s])='%s'));"),
			Heads::m_lpszTable,
			Heads::m_lpszID,
			Panels::m_lpszTable,
			Heads::m_lpszTable, 
			Panels::m_lpszTable,
			Panels::m_lpszID,
			Heads::m_lpszTable,
			Heads::m_lpszPanelID,
			Panels::m_lpszTable,
			Panels::m_lpszLineID,
			lLineID,
			Heads::m_lpszTable,
			Heads::m_lpszName,
			strHead);

		rst.Open (str);

		if (!rst.IsEOF ()) 
			lID = (long)rst.GetFieldValue (m_lpszID);

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return lID;
}

DB_API bool FoxjetDatabase::AddHeadRecord (COdbcDatabase & database, HEADSTRUCT & hs)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);

		hs.m_lID = -1;
		rst << hs;
		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);

		rst.Open (str, CRecordset::dynaset);
		rst.AddNew ();
		rst << hs;
		rst.Update ();
		rst.MoveLast ();
		
		hs.m_lID = (long)rst.GetFieldValue (m_lpszID);

		rst.Close ();
		*/

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::UpdateHeadRecord (COdbcDatabase & database, HEADSTRUCT & hs)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);

		rst << hs;
		bResult = true;

		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, hs.m_lID);
		rst.Open (str, CRecordset::dynaset);

		if (!rst.IsEOF ()) {
			rst.Edit ();
			rst << hs;
			rst.Update ();
			bResult = true;
		}

		rst.Close ();
		*/
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::DeleteHeadRecord (COdbcDatabase & database, ULONG lID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		CString strSQL;

		{
			COdbcRecordset rst (&database);
			CString str;

			str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), m_lpszTable, m_lpszID, lID);
			rst.Open (str);

			if (!rst.IsEOF ()) {
				DELETEDSTRUCT del;
				CStringArray v;

				for (int i = 0; i < rst.GetODBCFieldCount (); i++)
					v.Add ((CString)rst.GetFieldValue (i));

				del.m_lLineID = 0;
				del.m_strTaskName = _T ("HEADSTRUCT:") + ToString (lID);
				del.m_strData = ToString (v);
				TRACEF (del.m_strData);
				VERIFY (AddDeletedRecord (database, del));
			}
		}

		// delete related messages first
		strSQL.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
			Messages::m_lpszTable, Messages::m_lpszHeadID, lID);
		TRACEF (strSQL);
		database.ExecuteSQL (strSQL);

		// delete the head
		strSQL.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, lID);
		TRACEF (strSQL);
		database.ExecuteSQL (strSQL);

		#ifdef _DEBUG
		DeleteSettingsRecord (database, 0, GetValveKey (lID));
		#endif

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetFirstHeadRecord (COdbcDatabase & database, HEADSTRUCT & head)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	CString str;

	str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);

	try {
		COdbcRecordset rst (database);

		rst.Open (str);

		if (!rst.IsEOF ()) {
			rst >> head;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetHeadUnownedRecords (COdbcDatabase & database, CArray <HEADSTRUCT, HEADSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	
	if (GetPrinterHeads (database, GetPrinterID (database), v)) { //GetHeadRecords (database, v)) {
		for (int i = v.GetSize () - 1; i >= 0; i--)
			// Set to 0 if unowned;
			if (v [i].m_lPanelID)
				v.RemoveAt (i);

		bResult = true;
	}
	
	return bResult;
}

