#if !defined(AFX_COMPACTDLG_H__ACBC6DBE_3787_4B00_BFAF_BC9E3AD9DBF9__INCLUDED_)
#define AFX_COMPACTDLG_H__ACBC6DBE_3787_4B00_BFAF_BC9E3AD9DBF9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CompactDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCompactDlg dialog

namespace FoxjetDatabase
{
	class DB_API CCompactDlg : public CDialog
	{
	// Construction
	public:
		CCompactDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
		//{{AFX_DATA(CCompactDlg)
			// NOTE: the ClassWizard will add data members here
		//}}AFX_DATA


	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CCompactDlg)
		protected:
		virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
		//}}AFX_VIRTUAL

	// Implementation
	protected:

		// Generated message map functions
		//{{AFX_MSG(CCompactDlg)
			// NOTE: the ClassWizard will add member functions here
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace FoxjetDatabase

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMPACTDLG_H__ACBC6DBE_3787_4B00_BFAF_BC9E3AD9DBF9__INCLUDED_)
