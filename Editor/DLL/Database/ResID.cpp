#include "stdafx.h"
#include "ResID.h"
#include "..\..\Control\resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace Control;

extern DB_API const int Control::nConfigureDatabaseID = ID_CONFIGURE_DATABASE;

