#include "stdafx.h"
#include "Registry.h"
#include "Debug.h"
#include "Parse.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


DB_API bool FoxjetDatabase::WriteProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szData) 
{
	return WriteProfileString (HKEY_CURRENT_USER, szSection, szKey, szData);
}

DB_API bool FoxjetDatabase::WriteProfileString (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szData) 
{
	HKEY hKey;

	// if szSection created/opened okay
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = _tcslen (szData) + 1;

		#ifdef _UNICODE
		dwSize *= 2;
		#endif

		LONG lStatus = ::RegSetValueEx (hKey, szKey, 0, REG_SZ, 
			(const BYTE *) szData, dwSize);
		::RegCloseKey (hKey);

		return lStatus == ERROR_SUCCESS;
	}

	return false;
}

DB_API CString FoxjetDatabase::GetProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szDefault) 
{
	return GetProfileString (HKEY_CURRENT_USER, szSection, szKey, szDefault);
}

DB_API CString FoxjetDatabase::GetProfileString (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szDefault) 
{
	HKEY hKey;
	CString strResult (szDefault);

	// if szSection was in the registry
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwMaxValueLen = 256, dwType = REG_SZ;

		if (::RegQueryInfoKey (hKey, NULL, NULL, NULL, NULL, NULL, 
			NULL, NULL, NULL, &dwMaxValueLen, NULL, NULL) != ERROR_SUCCESS)
		{
			dwMaxValueLen = 256;
		}

		TCHAR * pszData = new TCHAR [dwMaxValueLen + 1];

		if (::RegQueryValueEx (hKey, szKey, NULL, &dwType, 
			(LPBYTE) pszData, &dwMaxValueLen) == ERROR_SUCCESS) 
		{
			strResult = pszData;
		}

		delete [] pszData;
		::RegCloseKey (hKey);
	}

	return strResult;
}

DB_API bool FoxjetDatabase::WriteProfileInt (LPCTSTR szSection, LPCTSTR szKey, UINT nData) 
{
	return WriteProfileInt (HKEY_CURRENT_USER, szSection, szKey, nData);
}

DB_API bool FoxjetDatabase::WriteProfileInt (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, UINT nData) 
{
	HKEY hKey;

	// if szSection created/opened okay
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = sizeof (DWORD);
		DWORD dwData = nData;

		LONG lStatus = ::RegSetValueEx (hKey, szKey, 0, REG_DWORD, 
			(const BYTE *) &dwData, dwSize);
		::RegCloseKey (hKey);
		return lStatus == ERROR_SUCCESS;
	}

	return false;
}

DB_API bool FoxjetDatabase::WriteProfileByte (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, BYTE nData) 
{
	HKEY hKey;

	// if szSection created/opened okay
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = sizeof (BYTE);
		DWORD dwData = nData;

		LONG lStatus = ::RegSetValueEx (hKey, szKey, 0, REG_BINARY, (const BYTE *) &dwData, dwSize);
		::RegCloseKey (hKey);
		return lStatus == ERROR_SUCCESS;
	}

	return false;
}

DB_API UINT FoxjetDatabase::GetProfileInt (LPCTSTR szSection, LPCTSTR szKey, UINT nDefault) 
{
	return FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, szSection, szKey, nDefault);
}

DB_API UINT FoxjetDatabase::GetProfileInt (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, UINT nDefault) 
{
	HKEY hKey;
	UINT nResult = nDefault;

	// if szSection was in the registry
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = sizeof (DWORD), dwType = REG_DWORD;
		DWORD dwData;

		if (::RegQueryValueEx (hKey, szKey, NULL, &dwType, (LPBYTE) &dwData, &dwSize) == ERROR_SUCCESS) 
		{
			nResult = (UINT)dwData;
		}

		::RegCloseKey (hKey);
	}

	return nResult;
}

DB_API bool FoxjetDatabase::WriteProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, unsigned __int64 nData)
{
	return WriteProfileInt64 (HKEY_CURRENT_USER, szSection, szKey, nData);
}

DB_API bool FoxjetDatabase::WriteProfileInt64 (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, unsigned __int64 nData)
{
	HKEY hKey;

	// if szSection created/opened okay
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = sizeof (QWORD);
		unsigned __int64 nData = nData;

		LONG lStatus = ::RegSetValueEx (hKey, szKey, 0, REG_DWORD, (const BYTE *) &nData, dwSize);
		::RegCloseKey (hKey);
		return lStatus == ERROR_SUCCESS;
	}

	return false;
}


DB_API unsigned __int64 FoxjetDatabase::GetProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, unsigned __int64 nDefault) 
{
	return FoxjetDatabase::GetProfileInt64 (HKEY_CURRENT_USER, szSection, szKey, nDefault);
}

DB_API unsigned __int64 FoxjetDatabase::GetProfileInt64 (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, unsigned __int64 nDefault) 
{
	HKEY hKey;
	unsigned __int64 nResult = nDefault;

	// if szSection was in the registry
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = sizeof (DWORD), dwType = REG_QWORD;
		unsigned __int64 nData;

		if (::RegQueryValueEx (hKey, szKey, NULL, &dwType, (LPBYTE) &nData, &dwSize) == ERROR_SUCCESS) 
		{
			nResult = (UINT)nData;
		}

		::RegCloseKey (hKey);
	}

	return nResult;
}

DB_API bool FoxjetDatabase::WriteProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, __int64 nData)
{
	return WriteProfileInt64 (HKEY_CURRENT_USER, szSection, szKey, nData);
}

DB_API bool FoxjetDatabase::WriteProfileInt64 (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, __int64 nData)
{
	HKEY hKey;

	// if szSection created/opened okay
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = sizeof (QWORD);
		__int64 nData = nData;

		LONG lStatus = ::RegSetValueEx (hKey, szKey, 0, REG_DWORD, (const BYTE *) &nData, dwSize);
		::RegCloseKey (hKey);
		return lStatus == ERROR_SUCCESS;
	}

	return false;
}


DB_API __int64 FoxjetDatabase::GetProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, __int64 nDefault) 
{
	return FoxjetDatabase::GetProfileInt64 (HKEY_CURRENT_USER, szSection, szKey, nDefault);
}

DB_API __int64 FoxjetDatabase::GetProfileInt64 (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, __int64 nDefault) 
{
	HKEY hKey;
	__int64 nResult = nDefault;

	// if szSection was in the registry
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = sizeof (DWORD), dwType = REG_QWORD;
		__int64 nData;

		if (::RegQueryValueEx (hKey, szKey, NULL, &dwType, (LPBYTE) &nData, &dwSize) == ERROR_SUCCESS) 
		{
			nResult = (UINT)nData;
		}

		::RegCloseKey (hKey);
	}

	return nResult;
}

DB_API BYTE FoxjetDatabase::GetProfileByte (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, UINT nDefault) 
{
	HKEY hKey;
	UINT nResult = nDefault;

	// if szSection was in the registry
	if (::RegCreateKey (hKeyRoot, szSection, &hKey) == ERROR_SUCCESS) {
		DWORD dwSize = sizeof (BYTE), dwType = REG_BINARY;
		DWORD dwData;

		if (::RegQueryValueEx (hKey, szKey, NULL, &dwType, (LPBYTE) &dwData, &dwSize) == ERROR_SUCCESS) 
		{
			nResult = (UINT)dwData;
		}

		::RegCloseKey (hKey);
	}

	return nResult;
}

DB_API CString FoxjetDatabase::GetCmdlineValue (const CString & strCmdLine, const CString & strFlag)
{
	CString str;
	int nIndex = strCmdLine.Find (strFlag);
	CStringArray v;
	
	if (nIndex != -1) {
		int nFirstChar = nIndex + strFlag.GetLength ();

		if ((nFirstChar >= 0 && nFirstChar < strCmdLine.GetLength ()) && (strCmdLine [nFirstChar] == '\"')) {
			int nLastChar = strCmdLine.Find (_T ("\""), nFirstChar + 1);

			str = strCmdLine.Mid (nFirstChar + 1, nLastChar - nFirstChar - 1);
		}
		else
			str = FoxjetDatabase::Tokenize (strCmdLine.Mid (nIndex + strFlag.GetLength ()), v, ' ');
	}

	return str;
}

CString FoxjetDatabase::ODBC::GetProfileString (LPCTSTR lpszDSN, LPCTSTR lpszKey, LPCTSTR lpszDefault)
{
	HKEY h [] = { HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE };
	const CString strKey = _T ("SOFTWARE\\ODBC\\ODBC.INI\\") + CString (lpszDSN);
	const CString strNotFound = _T ("!_NOT_FOUND_!");

	for (int i = 0; i < ARRAYSIZE (h); i++) {
		CString str = FoxjetDatabase::GetProfileString (h [i], strKey, lpszKey, strNotFound);

		if (str != strNotFound)
			return str;
	}

	return lpszDefault;
}

