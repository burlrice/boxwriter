#include "stdafx.h"
#include "Parse.h"
#include "Debug.h"
#include "Resource.h"
#include <stack>
#include <iostream>
#include <sstream>

using namespace FoxjetDatabase;

extern HINSTANCE hInstance;

/*
void FoxjetDatabase::Trace (const CString & strMsg, const CString & strFile, ULONG lLine)
{
	CString str;

	str.Format (_T ("%s, line: %d\n%s"), strFile, lLine, strMsg);
	int nResult = MsgBox (str, _T ("Database"), MB_OKCANCEL);

	if (nResult == IDCANCEL)
		::abort ();
}
*/

static DIAGNOSTIC_CRITICAL_SECTION (csString);

DB_API CString FoxjetDatabase::LoadString (UINT nID, LPCTSTR lpszModule)
{
	LOCK (::csString);

	CString strResult;
	LPCTSTR pwsz = NULL;

	if (HMODULE hModule = ::LoadLibrary (lpszModule)) {
		if (HRSRC hrsrc = ::FindResourceEx (hModule, RT_STRING, MAKEINTRESOURCE (nID / 16 + 1), MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL))) {
			if (HGLOBAL hglob = ::LoadResource (hModule, hrsrc)) {
				if (pwsz = reinterpret_cast<LPCWSTR>(::LockResource (hglob))) {
					for (UINT i = 0; i < (nID & 15); i++) 
						pwsz += 1 + (UINT)*pwsz;
					
					WORD wLen = (WORD)* pwsz;
					TCHAR * p = (TCHAR *)((TCHAR *)pwsz + 1);

					strResult = CString ((LPCTSTR)p, wLen);
					//for (WORD w = 0; w < wLen; w++)
					//	strResult += *p++;

					::UnlockResource ((LPVOID)pwsz);
				}
				else ASSERT (0);
				
				::FreeResource(hglob);
			}
			else ASSERT (0);
		}
		else ASSERT (0);

		::FreeLibrary (hModule);
	}
	else ASSERT (0);

	return strResult;
}

CString FoxjetDatabase::LoadString (UINT nID)
{
	return LoadString (nID, _T ("Database.dll"));
}

static BOOL CALLBACK EnumResStringsProc (HMODULE hModule, LPCTSTR lpszType, LPTSTR lpszName, LONG_PTR lParam)
{
	//TRACEF (_T ("[name, type]: ") + CString (IS_INTRESOURCE (lpszType) ? ToString ((int)lpszType) : lpszType) + _T (", ") + CString (IS_INTRESOURCE (lpszName) ? ToString ((int)lpszName) : lpszName));

	ASSERT (lParam);
	ASSERT (IS_INTRESOURCE (lpszName));

	if (!IS_INTRESOURCE (lpszName))
		return TRUE;

	FoxjetCommon::CCopyArray <UINT, UINT> & v = * (FoxjetCommon::CCopyArray <UINT, UINT> *)lParam;

	if (HRSRC hFind = ::FindResource (hModule, lpszName, lpszType)) {
		if (HGLOBAL hGlobal = ::LoadResource (hModule, hFind)) {
			if (TCHAR * lpData = (TCHAR *)::LockResource (hGlobal)) {
				DWORD cbData = ::SizeofResource (hModule, hFind);
				const UINT nBase = (16 * ((UINT)lpszName - 1));
				UINT nNull = 0, nID = nBase;
				TCHAR * p = lpData;

				while (p < (lpData + (cbData / sizeof (TCHAR)))) {
					while (!(* p++))
						nNull++;

					nID += nNull;

					if (HRSRC hrsrc = ::FindResourceEx (hModule, RT_STRING, MAKEINTRESOURCE (nID / 16 + 1), MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL))) 
						v.Add (nID);

					p += * (p - 1);
					nNull = 1;
				}

				::UnlockResource ((LPVOID)lpData);
			}

			::FreeResource (hGlobal);
		}
	}

/*
1705 Really delete the selected elements?
1707 Data
1710 ID
1711 Type

(nID & 15)		= (1705 & 15) = (6A9 & F) = 9 
(nID / 16 + 1)	= (1705 / 16 + 1) = 107			--> n = 16 * (107 - 1) = 1696		--> 1696 + 9 = 1705

		(nID & 15)	(nID / 16 + 1)		start index		#nulls
1705	9			107					11				9								= 9
1707	11			107					69				9 + (1 + 1)						= 11
1710	14			107					76				9 + (1 + 1)	+ (2 + 1)			= 14
1711	15			107					81				9 + (1 + 1)	+ (2 + 1) + (0 + 1)	= 15


[NULL][NULL][NULL][NULL][NULL][NULL][NULL][NULL][NULL]9�Confirma que desea eliminar los elementos seleccionados?[NULL]Datos[NULL][NULL]IDTipo:
.........9�Confirma que desea eliminar los elementos seleccionados?.Datos..IDTipo:
00 00 00 00 00 00 00 00 00 39  BF436F6E6669726D612071756520646573656120656C696D696E6172206C6F7320656C656D656E746F732073656C656363696F6E61646F733F00 05 4461746F7300 00 02 4944 05 5469706F3A
							   [�Confirma que desea eliminar los elementos seleccionados?                                                         ]    [Datos     ]       [ID]    [Tipo:   ]
							   [11-67                                                                                                             ]    [69-73     ]       [76-77] [81-85   ]
							   [57                                                                                                                ]    [5         ]       [2 ]    [5       ]
							   [1705                                                                                                              ]    [1707      ]       [1710]  [1711    ]
*/


	return TRUE;
}

static BOOL CALLBACK EnumResDialogsProc (HMODULE hModule, LPCTSTR lpszType, LPTSTR lpszName, LONG_PTR lParam)
{
	ASSERT (lParam);
//	ASSERT (IS_INTRESOURCE (lpszName));

	if (!IS_INTRESOURCE (lpszName))
		return TRUE;

	FoxjetCommon::CCopyArray <UINT, UINT> & v = * (FoxjetCommon::CCopyArray <UINT, UINT> *)lParam;

	v.Add ((UINT)lpszName);

	return TRUE;
}


DB_API FoxjetCommon::CCopyArray <UINT, UINT> FoxjetDatabase::GetStringResourceIDs (const CString & strModule)
{
	typedef BOOL (WINAPI * lpEnumResourceNamesEx) (HMODULE, LPCTSTR, ENUMRESNAMEPROC, LONG_PTR, DWORD, LANGID);

	FoxjetCommon::CCopyArray <UINT, UINT> v;
	
	if (HMODULE hModule = ::LoadLibrary (strModule)) {
		if (HMODULE hKernel32 = LoadLibrary (_T ("KERNEL32.DLL"))) {
			if (lpEnumResourceNamesEx pFct = (lpEnumResourceNamesEx)GetProcAddress (hKernel32, "EnumResourceNamesExW")) {
				(* pFct) (hModule, RT_STRING, (ENUMRESNAMEPROC)EnumResStringsProc, (LPARAM)&v, RESOURCE_ENUM_LN, MAKELANGID (LANG_NEUTRAL, SUBLANG_NEUTRAL));
				//::EnumResourceNamesEx (hModule, RT_STRING, (ENUMRESNAMEPROC)EnumResStringsProc, (LPARAM)&v, RESOURCE_ENUM_LN, MAKELANGID (LANG_NEUTRAL, SUBLANG_NEUTRAL));
			}

			::FreeLibrary (hKernel32);
		}
		else ASSERT (0);

		::FreeLibrary (hModule);
	}

	return v;
}

DB_API FoxjetCommon::CCopyArray <UINT, UINT> FoxjetDatabase::GetDialogResourceIDs (const CString & strModule)
{
	typedef BOOL (WINAPI * lpEnumResourceNamesEx) (HMODULE, LPCTSTR, ENUMRESNAMEPROC, LONG_PTR, DWORD, LANGID);

	FoxjetCommon::CCopyArray <UINT, UINT> v;

	if (HMODULE hModule = ::LoadLibrary (strModule)) {
		if (HMODULE hKernel32 = LoadLibrary (_T ("KERNEL32.DLL"))) {
			if (lpEnumResourceNamesEx pFct = (lpEnumResourceNamesEx)GetProcAddress (hKernel32, "EnumResourceNamesExW")) {
				(* pFct) (hModule, RT_STRING, (ENUMRESNAMEPROC)EnumResStringsProc, (LPARAM)&v, RESOURCE_ENUM_LN, MAKELANGID (LANG_NEUTRAL, SUBLANG_NEUTRAL));
				//::EnumResourceNamesEx (hModule, RT_DIALOG, (ENUMRESNAMEPROC)EnumResDialogsProc, (LPARAM)&v, RESOURCE_ENUM_LN, MAKELANGID (LANG_NEUTRAL, SUBLANG_NEUTRAL));
			}

			::FreeLibrary (hKernel32);
		}

		::FreeLibrary (hModule);
	}
	else ASSERT (0);

	return v;
}

DB_API CString FoxjetDatabase::RemoveWhitespace (const CString & str)
{
	using namespace std;
	CString strResult;

	wstring strIn (str);
	wstringstream iss (strIn);
	wstring word;
				
	while (iss >> word) {
		strResult += word.c_str ();
		strResult += _T (" ");
	}

	strResult.Trim ();

	return strResult;
}

DB_API CString FoxjetDatabase::RemovePunctuation (CString str)
{
	LPCTSTR lpsz [] = { _T ("\\r"), _T ("\\n"), _T ("\\t"),  };

	for (int i = str.GetLength () - 1; i >= 0; i--)
		if (_istpunct (str [i]))
			str.SetAt (i, ' ');

	for (int i = 0; i < ARRAYSIZE (lpsz); i++)
		str.Replace (lpsz [i], _T (""));

	return RemoveWhitespace (str);
}


DB_API UINT FoxjetDatabase::GetIdFromStringResource (const CString & strFind, const CString & strModule, bool bIgnorePunctuation)
{
	FoxjetCommon::CCopyArray <UINT, UINT> v = GetStringResourceIDs (strModule);
	const CString strFindNoPuncuation = RemovePunctuation (strFind);
	int nResult = 0;

	if (HMODULE hModule = ::LoadLibrary (strModule)) {
		for (int i = 0; !nResult && i < v.GetSize (); i++) {
			UINT nID = v [i];

			if (HRSRC hrsrc = ::FindResourceEx (hModule, RT_STRING, MAKEINTRESOURCE (nID / 16 + 1), MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL))) {
				if (HGLOBAL hglob = ::LoadResource (hModule, hrsrc)) {
					if (LPCTSTR pwsz = reinterpret_cast<LPCWSTR>(::LockResource (hglob))) {
						for (UINT i = 0; i < (nID & 15); i++) 
							pwsz += 1 + (UINT)*pwsz;
					
						WORD wLen = (WORD)* pwsz;
						TCHAR * p = (TCHAR *)((TCHAR *)pwsz + 1);

						if (wLen > 0) {
							//TRACEF (CString ((LPCTSTR)p, wLen));

							if (bIgnorePunctuation) {
								CString str = RemovePunctuation (CString ((LPCTSTR)p, wLen));

								//TRACEF (ToString ((int)nID) + _T (": ") + str);

								if (!str.CompareNoCase (strFindNoPuncuation))
									nResult = nID;
							}
							else {
								if (strFind.GetLength () == wLen) {
									if (!_tcsncmp (strFind, p, wLen)) 
										nResult = nID;
								}
							}
						}

						::UnlockResource ((LPVOID)pwsz);
					}
					else ASSERT (0);
				
					::FreeResource(hglob);
				}
				else ASSERT (0);
			}
			else ASSERT (0);
		}

		::FreeLibrary (hModule);
	}
	else ASSERT (0);

	return nResult;
}

DB_API INT_PTR CALLBACK FoxjetDatabase::HiddenDialogProc (HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg) {
    case WM_CLOSE:
		::EndDialog (hwndDlg, IDOK);
		return TRUE;
	case WM_WINDOWPOSCHANGING:
		WINDOWPOS * p = (WINDOWPOS *)lParam;
		p->flags &= ~SWP_SHOWWINDOW;
		return 0;
	}

	return 0;
}

DB_API DIALOG_SEARCH_STRUCT FoxjetDatabase::GetIdFromDialogResource (const CString & strFind, const CString & strModule)
{
	DIALOG_SEARCH_STRUCT result;
	FoxjetCommon::CCopyArray <UINT, UINT> v = FoxjetDatabase::GetDialogResourceIDs (strModule);
	bool bFound = false;

	result.m_nCtrlID = result.m_nDialogID = -1;

	if (HMODULE hModule = ::LoadLibrary (strModule)) {
		for (int i = 0; !bFound && i < v.GetSize (); i++) {
			UINT nID = v [i];

			if (HRSRC hrsrc = ::FindResourceEx (hModule, RT_DIALOG, MAKEINTRESOURCE (nID), MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL))) {
				if (HGLOBAL hglob = ::LoadResource (hModule, hrsrc)) {
					if (LPCDLGTEMPLATE lpDialogTemplate = (LPCDLGTEMPLATE)::LockResource(hglob)) {
						HWND hwnd = ::CreateDialogIndirect (::AfxGetInstanceHandle (), lpDialogTemplate, ::AfxGetMainWnd ()->m_hWnd, HiddenDialogProc);
						
						if (!hwnd)
							TRACEF (FormatMessage (::GetLastError ()));

						if (hwnd) {
							TCHAR sz [512] = { 0 };

							::ShowWindow (hwnd, SW_HIDE);
							::GetWindowText (hwnd, sz, ARRAYSIZE (sz) - 1);

							if (!strFind.Compare (sz)) {
								result.m_nDialogID = nID;
								bFound = true;
							}

							HWND h = ::GetTopWindow (hwnd);

							while (h && !bFound) {
								::GetWindowText (h, sz, ARRAYSIZE (sz) - 1);

								if (!strFind.Compare (sz)) {
									result.m_nDialogID = nID;
									result.m_nCtrlID = ::GetWindowLong (h, GWL_ID);
									bFound = true;
								}

								h = ::GetNextWindow (h, GW_HWNDNEXT);
							}

							::DestroyWindow (hwnd); //::SendMessage (hwnd, WM_CLOSE, 0, 0);
						}

						::UnlockResource (hglob);
					}

					::FreeResource(hglob);
				}
			}
		}

		::FreeLibrary (hModule);
	}

	return result;
}

DB_API CString FoxjetDatabase::GetStringFromDialogResource (const DIALOG_SEARCH_STRUCT & find, const CString & strModule)
{
	CString strResult;

	if (HMODULE hModule = ::LoadLibrary (strModule)) {
		if (HRSRC hrsrc = ::FindResourceEx (hModule, RT_DIALOG, MAKEINTRESOURCE (find.m_nDialogID), MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL))) {
			if (HGLOBAL hglob = ::LoadResource (hModule, hrsrc)) {
				if (LPCDLGTEMPLATE lpDialogTemplate = (LPCDLGTEMPLATE)::LockResource(hglob)) {
					HWND hwnd = ::CreateDialogIndirect (::AfxGetInstanceHandle (), lpDialogTemplate, ::AfxGetMainWnd ()->m_hWnd, HiddenDialogProc);
						
					if (!hwnd)
						TRACEF (FormatMessage (::GetLastError ()));

					if (hwnd) {
						TCHAR sz [512] = { 0 };

						::ShowWindow (hwnd, SW_HIDE);
						::GetWindowText (hwnd, sz, ARRAYSIZE (sz) - 1);

						if (find.m_nCtrlID == -1)
							strResult = sz;
						else {
							::GetDlgItemText (hwnd, find.m_nCtrlID, sz, ARRAYSIZE (sz) - 1);
							strResult = sz;
						}

						::DestroyWindow (hwnd); //::SendMessage (hwnd, WM_CLOSE, 0, 0);
					}

					::UnlockResource (hglob);
				}

				::FreeResource(hglob);
			}
		}

		::FreeLibrary (hModule);
	}

	return strResult;
}
