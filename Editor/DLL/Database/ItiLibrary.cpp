// ItiLibrary.cpp: implementation of the ItiLibrary class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ItiLibrary.h"
#include "Registry.h"
#include "Database.h"

using namespace FoxjetDatabase;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

DB_API void ItiLibrary::SaveCustomDateTimeFmts(const CString & strRegSection, const CString & strName, const CStringArray & v)
{
	FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER,
		strRegSection + _T ("\\") + strName, 
		_T ("nEntries"), v.GetSize ());

	for (int i = 0; i < v.GetSize (); i++) {
		CString str;
		
		str.Format (_T ("strEntry%d"), i);
		FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER,
			strRegSection + _T ("\\") + strName, 
			str, v [i]);
	}
}

DB_API int ItiLibrary::LoadCustomDateTimeFmts(const CString & strRegSection, const CString & strName, CStringArray & v)
{
	int nEntries = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER,
		strRegSection + _T ("\\") + strName, 
		_T ("nEntries"), 0);
	
	v.RemoveAll ();

	for (int i = 0; i < nEntries; i++) {
		CString str;
		str.Format (_T ("strEntry%d"), i);
		v.Add (FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER,
			strRegSection + _T ("\\") + strName, 
			str, _T ("%H:%M")));
	}

	return nEntries;
}

DB_API void ItiLibrary::DefaultCustomDateTimeFmts(const CString & strRegSection)
{
	CStringArray v;
	LPCTSTR pszDefs [] = {
		_T ("%H:%M:%S"),	_T ("%#H:%M:%S"),	_T ("%I:%M:%S"),
		_T ("%#I:%M:%S"),	_T ("%H"),			_T ("%#H"),
		_T ("%I"),			_T ("%#I"),			_T ("%M"),
		_T ("%S"),			_T ("%d"),			_T ("%#d"),
		_T ("%a"),			_T ("%b"),			_T ("%y"),			
		_T ("%Y"),			_T ("%%Y"),			NULL
	};

	if (!LoadCustomDateTimeFmts (strRegSection, _T ("Custom date/time formats"), v)) {
		for (int i = 0; pszDefs [i]; i++)
			v.Add ((CString)pszDefs [i]);

		SaveCustomDateTimeFmts (strRegSection, _T ("Custom date/time formats"), v);
	}
}
