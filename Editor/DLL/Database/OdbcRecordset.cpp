// OdbcRecordset.cpp: implementation of the COdbcRecordset class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Database.h"
#include "OdbcRecordset.h"
#include "OdbcBulkRecordset.h"
#include "Debug.h"
#include "Resource.h"
#include "Parse.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

////////////////////////////////////////////////////////////////////////

using namespace FoxjetDatabase;
using FoxjetDatabase::COdbcRecordset;
using FoxjetDatabase::CQualifiedFieldInfo;

////////////////////////////////////////////////////////////////////////
#include <ATLCONV.H>

// Note: CString.m_pchData must not be changed.  This address is registered
// with ODBC and must remain valid until the recordset is released.
void AFXAPI RFX_TextUnicode (CFieldExchange* pFX, LPCTSTR szName,
	CString& value, int nMaxLength, int nColumnType, short nScale)
{
	ASSERT(AfxIsValidAddress(pFX, sizeof(CFieldExchange)));
	ASSERT(AfxIsValidString(szName));
	ASSERT(AfxIsValidAddress(&value, sizeof(CString)));

	RETCODE nRetCode;
	UINT nField;
	if (!pFX->IsFieldType(&nField))
		return;

	LONG* plLength = pFX->m_prs->GetFieldLengthBuffer(
		nField - 1, pFX->m_nFieldType);
	switch (pFX->m_nOperation)
	{
	default:
		pFX->Default(szName, value.GetBuffer(0), plLength,
			SQL_C_WCHAR /*SQL_C_CHAR*/, value.GetLength() * 2, nMaxLength);
		value.ReleaseBuffer();
		return;

	case CFieldExchange::BindParam:
		{
			// Preallocate to nMaxLength and setup binding address
			value.GetBufferSetLength(nMaxLength);
			void* pvParam = value.LockBuffer(); // will be overwritten if UNICODE

#ifdef _UNICODE
			// Must use proxy to translate unicode data into non-unicode param
			pFX->m_prs->m_bRebindParams = TRUE;

			// Allocate proxy array if necessary
			if (pFX->m_prs->m_pvParamProxy == NULL)
			{
				pFX->m_prs->m_pvParamProxy = new void*[pFX->m_prs->m_nParams];
				memset(pFX->m_prs->m_pvParamProxy, 0,
					pFX->m_prs->m_nParams*sizeof(void*));
				pFX->m_prs->m_nProxyParams = pFX->m_prs->m_nParams;
			}

			// Allocate non-unicode string to nMaxLength if necessary for SQLBindParameter
			if (pFX->m_prs->m_pvParamProxy[nField-1] == NULL)
			{
				pvParam = new TCHAR[nMaxLength+1];
				pFX->m_prs->m_pvParamProxy[nField-1] = pvParam;
			}
			else
				pvParam = pFX->m_prs->m_pvParamProxy[nField-1];

			// Now fill in the data value
			USES_CONVERSION;
			lstrcpyA((char*)pvParam, T2A((LPCTSTR)value));
#endif // _UNICODE

			*plLength = pFX->m_prs->IsParamStatusNull(nField - 1) ?
				SQL_NULL_DATA : SQL_NTS;

			AFX_SQL_SYNC(::SQLBindParameter(pFX->m_hstmt, (UWORD)nField,
				 (SWORD)pFX->m_nFieldType, SQL_C_WCHAR /*SQL_C_CHAR*/, (SWORD)nColumnType,
				 nMaxLength, nScale, pvParam, nMaxLength, plLength));
			value.ReleaseBuffer();

			if (nRetCode != SQL_SUCCESS)
				pFX->m_prs->ThrowDBException(nRetCode, pFX->m_hstmt);

			// Add the member address to the param map
			pFX->m_prs->m_mapParamIndex.SetAt(&value, (void*)nField);
		}
		return;

#ifdef _UNICODE
	case CFieldExchange::RebindParam:
		*plLength = pFX->m_prs->IsParamStatusNull(nField - 1) ?
			SQL_NULL_DATA : SQL_NTS;
		if (pFX->m_prs->m_nProxyParams != 0)
		{
			// Fill buffer (expected by SQLBindParameter) with new param data
			USES_CONVERSION;
//			LPSTR lpszParam = (LPSTR)pFX->m_prs->m_pvParamProxy[nField-1];
//			lstrcpyA(lpszParam, T2A((LPCTSTR)value));
			LPTSTR lpszParam = (LPTSTR)pFX->m_prs->m_pvParamProxy[nField-1];
			lstrcpyW(lpszParam, value);
		}
		return;
#endif // _UNICODE

	case CFieldExchange::BindFieldToColumn:
		{
			// Assumes all bound fields BEFORE unbound fields
			CODBCFieldInfo* pODBCInfo =
				&pFX->m_prs->m_rgODBCFieldInfos[nField - 1];
			UINT cbColumn = pODBCInfo->m_nPrecision;

			switch (pODBCInfo->m_nSQLType)
			{
			default:
#ifdef _DEBUG
				// Warn of possible field schema mismatch
				if (afxTraceFlags & traceDatabase)
					TRACE1("Warning: CString converted from SQL type %ld.\n",
						pODBCInfo->m_nSQLType);
#endif // _DEBUG

				// Add room for extra information like sign, decimal point, etc.
				cbColumn += 10;
				break;

			case SQL_LONGVARCHAR:
			case SQL_CHAR:
			case SQL_VARCHAR:
				break;

			case SQL_FLOAT:
			case SQL_REAL:
			case SQL_DOUBLE:
				// Add room for sign, decimal point and " E +XXX"
				cbColumn += 10;
				break;

			case SQL_DECIMAL:
			case SQL_NUMERIC:
				// Add room for sign and decimal point
				cbColumn += 2;
				break;

			case SQL_TIMESTAMP:
			case SQL_DATE:
			case SQL_TIME:
				// May need extra space, i.e. "{TS mm/dd/yyyy hh:mm:ss}"
				cbColumn += 10;
				break;

			case SQL_TINYINT:
			case SQL_SMALLINT:
			case SQL_INTEGER:
			case SQL_BIGINT:
				// Add room for sign
				cbColumn += 1;
				break;
			}

			// Constrain to user specified max length, subject to 256 byte min
			if (cbColumn > (UINT)nMaxLength || cbColumn < 256)
				cbColumn = nMaxLength;

			// Set up binding addres
			void* pvData;
			value.GetBufferSetLength(cbColumn+1);
			pvData = value.LockBuffer();    // will be overwritten if UNICODE

#ifdef _UNICODE
			// Allocate proxy array if necessary
			if (pFX->m_prs->m_pvFieldProxy == NULL)
			{
				pFX->m_prs->m_pvFieldProxy = new void*[pFX->m_prs->m_nFields];
				memset(pFX->m_prs->m_pvFieldProxy, 0,
					pFX->m_prs->m_nFields*sizeof(void*));
				pFX->m_prs->m_nProxyFields = pFX->m_prs->m_nFields;
			}

			// Allocate non-unicode string for SQLBindCol (not necessary on Requery)
			if (pFX->m_prs->m_pvFieldProxy[nField-1] == NULL)
				pFX->m_prs->m_pvFieldProxy[nField-1] = new TCHAR[cbColumn+2];

			pvData = pFX->m_prs->m_pvFieldProxy[nField-1];
#endif // _UNICODE

			AFX_SQL_SYNC(::SQLBindCol(pFX->m_prs->m_hstmt, (UWORD)nField,
				SQL_C_WCHAR /*SQL_C_CHAR*/, pvData, (cbColumn+1) * 2, plLength));
			value.ReleaseBuffer();
			if (!pFX->m_prs->Check(nRetCode))
				pFX->m_prs->ThrowDBException(nRetCode);

			// Add the member address to the field map
			pFX->m_prs->m_mapFieldIndex.SetAt(&value, (void*)nField);
		}
		return;

#ifdef _UNICODE
	case CFieldExchange::BindFieldForUpdate:
		if (pFX->m_prs->m_nProxyFields != 0)
		{
			// Fill buffer (expected by SQLSetPos) with new field data
			USES_CONVERSION;
//			LPSTR lpszData = (LPSTR)pFX->m_prs->m_pvFieldProxy[nField-1];
//			lstrcpyA(lpszData, T2A((LPCTSTR)value));
			LPTSTR lpszData = (LPTSTR)pFX->m_prs->m_pvFieldProxy[nField-1];
			lstrcpyW(lpszData, value);

			pFX->Default(szName, (void *)lpszData, plLength, SQL_C_WCHAR /*SQL_C_CHAR*/,
				value.GetLength() * 2, nMaxLength);
		}
		return;
#endif // _UNICODE

	case CFieldExchange::Fixup:
		if (*plLength == SQL_NULL_DATA)
		{
			pFX->m_prs->SetNullFieldStatus(nField - 1);
			value.GetBufferSetLength(0);
			value.ReleaseBuffer();
		}
		else
		{
#ifdef _UNICODE
			// Copy value out of the proxy
			value = (LPCTSTR)pFX->m_prs->m_pvFieldProxy[nField-1];
#endif

			LPTSTR lpsz = value.GetBuffer(0);
			if (pFX->m_prs->m_pDatabase->m_bStripTrailingSpaces)
			{
				// find first trailing space
				LPTSTR lpszFirstTrailing = NULL;
				while (*lpsz != '\0')
				{
					if (*lpsz != ' ')
						lpszFirstTrailing = NULL;
					else
					{
						if (lpszFirstTrailing == NULL)
							lpszFirstTrailing = lpsz;
					}
					lpsz = _tcsinc(lpsz);
				}
				// truncate
				if (lpszFirstTrailing != NULL)
					*lpszFirstTrailing = '\0';

			}
			value.ReleaseBuffer();
			*plLength = value.GetLength() * 2;
		}
		return;

	case CFieldExchange::SetFieldNull:
		if ((pFX->m_pvField == NULL &&
			pFX->m_nFieldType == CFieldExchange::outputColumn) ||
			pFX->m_pvField == &value)
		{
			if (pFX->m_bField)
			{
				// Mark fields null
				pFX->m_prs->SetNullFieldStatus(nField - 1);
				// Set string 0 length
				value.GetBufferSetLength(0);
				value.ReleaseBuffer();
				*plLength = SQL_NULL_DATA;
			}
			else
			{
				pFX->m_prs->ClearNullFieldStatus(nField - 1);
				*plLength = SQL_NTS;
			}
#ifdef _DEBUG
			pFX->m_nFieldFound = nField;
#endif
		}
		return;


#ifdef _UNICODE
	case CFieldExchange::NameValue:
		if (pFX->m_prs->IsFieldStatusDirty(nField - 1))
		{
			*pFX->m_pstr += szName;
			*pFX->m_pstr += '=';
		}
		// Fall through

	case CFieldExchange::Value:
		if (pFX->m_prs->IsFieldStatusDirty(nField - 1))
		{
			// Get the field data
			CODBCFieldInfo* pODBCInfo =
					&pFX->m_prs->m_rgODBCFieldInfos[nField - 1];

//			LPSTR lpszData = (LPSTR)pFX->m_prs->m_pvFieldProxy[nField-1];
			LPTSTR lpszData = (LPTSTR)pFX->m_prs->m_pvFieldProxy[nField-1];
			if (pFX->m_prs->IsFieldStatusNull(nField - 1))
			{
				*plLength = SQL_NULL_DATA;
			}
			else
			{
				USES_CONVERSION;
//				lstrcpyA(lpszData, T2A((LPCTSTR)value));
//				*plLength = value.GetLength();
				lstrcpyW(lpszData, value);
				*plLength = value.GetLength() * 2;
			}

			// If optimizing for bulk add, only need lengths & proxy set correctly
			if(!(pFX->m_prs->m_dwOptions & CRecordset::optimizeBulkAdd))
			{
				*pFX->m_pstr += '?';
				*pFX->m_pstr += pFX->m_lpszSeparator;
				pFX->m_nParamFields++;
				AFX_SQL_SYNC(::SQLBindParameter(pFX->m_hstmt,
					(UWORD)pFX->m_nParamFields, SQL_PARAM_INPUT,
					SQL_C_WCHAR /*SQL_C_CHAR*/, pODBCInfo->m_nSQLType, nMaxLength,
					pODBCInfo->m_nScale, lpszData, 0, plLength));
			}
		}
		return;
#endif // _UNICODE

	case CFieldExchange::MarkForAddNew:
		// can force writing of psuedo-null value (as a non-null) by setting field dirty
		if (!value.IsEmpty())
		{
			pFX->m_prs->SetDirtyFieldStatus(nField - 1);
			pFX->m_prs->ClearNullFieldStatus(nField - 1);
		}
		return;

	case CFieldExchange::MarkForUpdate:
		if (value.IsEmpty())
			pFX->m_prs->SetNullFieldStatus(nField - 1);
		else
			pFX->m_prs->ClearNullFieldStatus(nField - 1);
		pFX->Default(szName, &value, plLength,
			SQL_C_WCHAR /*SQL_C_CHAR*/, value.GetLength() * 2, nMaxLength);
		return;

	case CFieldExchange::LoadField:
		{
			// Get the field data
			CFieldInfo* pInfo = &pFX->m_prs->m_rgFieldInfos[nField - 1];
			CString* pStrCachedValue = (CString*)pInfo->m_pvDataCache;

			// Restore the status
			pFX->m_prs->SetFieldStatus(nField - 1, pInfo->m_bStatus);

			// If not NULL, restore the value and length
			if (!pFX->m_prs->IsFieldStatusNull(nField - 1))
			{
				value = *pStrCachedValue;
				*plLength = value.GetLength() * 2;

#ifdef _UNICODE
				// Must restore proxy for correct WHERE CURRENT OF operation
				USES_CONVERSION;
//				LPSTR lpszData = (LPSTR)pFX->m_prs->m_pvFieldProxy[nField-1];
//				lstrcpyA(lpszData, T2A((LPCTSTR)value));
				LPTSTR lpszData = (LPTSTR)pFX->m_prs->m_pvFieldProxy[nField-1];
				lstrcpyW(lpszData, value);
#endif // _UNICODE
			}
			else
			{
				*plLength = SQL_NULL_DATA;
			}

#ifdef _DEBUG
			// Buffer address must not change - ODBC's SQLBindCol depends upon this
			void* pvBind;

	#ifdef _UNICODE
			pvBind = pFX->m_prs->m_pvFieldProxy[nField-1];
	#else // !_UNICODE
			pvBind = value.GetBuffer(0);
			value.ReleaseBuffer();
	#endif

			if (pvBind != pInfo->m_pvBindAddress)
			{
				TRACE1("Error: CString buffer (column %u) address has changed!\n",
					nField);
				ASSERT(FALSE);
			}
#endif // _DEBUG
		}
		return;

	case CFieldExchange::StoreField:
		AfxStoreField(*pFX->m_prs, nField, &value);
		return;

	case CFieldExchange::AllocCache:
		{
			CFieldInfo* pInfo = &pFX->m_prs->m_rgFieldInfos[nField - 1];
			pInfo->m_pvDataCache = new CString;
			pInfo->m_nDataType = AFX_RFX_TEXT;
		}
		return;

#ifdef _DEBUG
	case CFieldExchange::DumpField:
		*pFX->m_pdcDump << "\n" << szName << " = " << value;
		return;
#endif // _DEBUG

	}
}
////////////////////////////////////////////////////////////////////////

const CString COdbcRecordset::m_strBadFieldName = _T ("Field \"%s\" not found in this collection");
const CString COdbcRecordset::m_strBadFieldIndex = _T ("Field %u not found in this collection");
const CString COdbcRecordset::m_strBadVariantConversion = _T ("Field \"%s\" can't convert \"%s\" to %s");

typedef struct tagTYPEMAPSTRUCT
{
	tagTYPEMAPSTRUCT (short nSQLType, short nSQLCType, DWORD dwVariantType) 
	:	m_nSQLType (nSQLType), 
		m_nSQLCType (nSQLCType), 
		m_dwVariantType (dwVariantType)
	{ 
	}

	tagTYPEMAPSTRUCT (
		short nSQLType,						CString strSQLType, 
		short nSQLCType,					CString strSQLCType, 
		DWORD dwVariantType,				CString strVariantType) 
	:	m_nSQLType (nSQLType),				m_strSQLType (strSQLType), 
		m_nSQLCType (nSQLCType),			m_strSQLCType (strSQLCType), 
		m_dwVariantType (dwVariantType),	m_strVariantType (strVariantType)
	{ 
	}
	
	short	m_nSQLType;
	CString m_strSQLType;
	short	m_nSQLCType;
	CString m_strSQLCType;
	DWORD	m_dwVariantType;
	CString m_strVariantType;
} TYPEMAPSTRUCT;

#define MAKETYPEMAPSTRUCT(a, b, c) TYPEMAPSTRUCT(a, #a, b, #b, c, #c)

static bool operator == (const TYPEMAPSTRUCT & lhs, const TYPEMAPSTRUCT & rhs)
{
	return 
		lhs.m_dwVariantType == rhs.m_dwVariantType &&
		lhs.m_nSQLCType		== rhs.m_nSQLCType &&
		lhs.m_nSQLType		== rhs.m_nSQLType;
}

static bool operator != (const TYPEMAPSTRUCT & lhs, const TYPEMAPSTRUCT & rhs)
{
	return !(lhs == rhs);
}

static const TYPEMAPSTRUCT lastType (-1, -1, -1);
static const TYPEMAPSTRUCT types [] =
{
	MAKETYPEMAPSTRUCT (SQL_BIT,				SQL_C_BIT,			DBVT_BOOL),
	MAKETYPEMAPSTRUCT (SQL_TINYINT,			SQL_C_UTINYINT,		DBVT_UCHAR),
	MAKETYPEMAPSTRUCT (SQL_SMALLINT,		SQL_C_SSHORT,		DBVT_SHORT),
	MAKETYPEMAPSTRUCT (SQL_INTEGER,			SQL_C_SLONG,		DBVT_LONG),
	MAKETYPEMAPSTRUCT (SQL_REAL,			SQL_C_FLOAT,		DBVT_SINGLE),
	MAKETYPEMAPSTRUCT (SQL_FLOAT,			SQL_C_DOUBLE,		DBVT_DOUBLE),
	MAKETYPEMAPSTRUCT (SQL_DOUBLE,			SQL_C_DOUBLE,		DBVT_DOUBLE),
	MAKETYPEMAPSTRUCT (SQL_DATE,			SQL_C_TIMESTAMP,	DBVT_DATE),
	MAKETYPEMAPSTRUCT (SQL_TIME,			SQL_C_TIMESTAMP,	DBVT_DATE),
	MAKETYPEMAPSTRUCT (SQL_TIMESTAMP,		SQL_C_TIMESTAMP,	DBVT_DATE),
	MAKETYPEMAPSTRUCT (SQL_NUMERIC,			SQL_C_CHAR,			DBVT_STRING),
	MAKETYPEMAPSTRUCT (SQL_DECIMAL,			SQL_C_CHAR,			DBVT_STRING),
	MAKETYPEMAPSTRUCT (SQL_BIGINT,			SQL_C_CHAR,			DBVT_STRING),
	MAKETYPEMAPSTRUCT (SQL_CHAR,			SQL_C_CHAR,			DBVT_STRING),
	MAKETYPEMAPSTRUCT (SQL_VARCHAR,			SQL_C_CHAR,			DBVT_STRING),
	MAKETYPEMAPSTRUCT (SQL_LONGVARCHAR,		SQL_C_CHAR,			DBVT_STRING),
	MAKETYPEMAPSTRUCT (SQL_BINARY,			SQL_C_BINARY,		DBVT_BINARY),
	MAKETYPEMAPSTRUCT (SQL_VARBINARY,		SQL_C_BINARY,		DBVT_BINARY),
	MAKETYPEMAPSTRUCT (SQL_LONGVARBINARY,	SQL_C_BINARY,		DBVT_BINARY),
	::lastType
};

static CString GetVariantType (DWORD dwVarType)
{
	switch (dwVarType) {
	case DBVT_NULL:		return _T ("DBVT_NULL");	break;
	case DBVT_BOOL:		return _T ("DBVT_BOOL");	break;
	case DBVT_UCHAR:	return _T ("DBVT_UCHAR");	break;
	case DBVT_SHORT:	return _T ("DBVT_SHORT");	break;
	case DBVT_LONG:		return _T ("DBVT_LONG");	break;
	case DBVT_SINGLE:	return _T ("DBVT_SINGLE");	break;
	case DBVT_DOUBLE:	return _T ("DBVT_DOUBLE");	break;
	case DBVT_DATE:		return _T ("DBVT_DATE");	break;
	case DBVT_STRING:	return _T ("DBVT_STRING");	break;
	case DBVT_BINARY:	return _T ("DBVT_BINARY");	break;
	default:			return _T ("Unknown");		break;
	}
}

#ifdef _DEBUG
static CString GetSQLType (DWORD dwType)
{
	typedef struct tagSQLTYPESTRUCT
	{
		tagSQLTYPESTRUCT (DWORD dw, LPCTSTR lpsz = NULL) 
			: m_dw (dw), m_str (lpsz) 
		{ 
		}

		DWORD m_dw;
		CString m_str;

	} SQLTYPESTRUCT;

	#define MAKESQLTYPESTRUCT(n) SQLTYPESTRUCT(n, _T ( #n ))

	static const SQLTYPESTRUCT types [] = 
	{
		MAKESQLTYPESTRUCT (SQL_CHAR),
		MAKESQLTYPESTRUCT (SQL_VARCHAR),
		MAKESQLTYPESTRUCT (SQL_LONGVARCHAR),
		MAKESQLTYPESTRUCT (SQL_WCHAR),
		MAKESQLTYPESTRUCT (SQL_WVARCHAR),
		MAKESQLTYPESTRUCT (SQL_WLONGVARCHAR),
		MAKESQLTYPESTRUCT (SQL_DECIMAL),
		MAKESQLTYPESTRUCT (SQL_NUMERIC),
		MAKESQLTYPESTRUCT (SQL_SMALLINT),
		MAKESQLTYPESTRUCT (SQL_INTEGER),
		MAKESQLTYPESTRUCT (SQL_REAL),
		MAKESQLTYPESTRUCT (SQL_FLOAT),
		MAKESQLTYPESTRUCT (SQL_DOUBLE),
		MAKESQLTYPESTRUCT (SQL_BIT),
		MAKESQLTYPESTRUCT (SQL_TINYINT),
		MAKESQLTYPESTRUCT (SQL_BIGINT),
		MAKESQLTYPESTRUCT (SQL_BINARY),
		MAKESQLTYPESTRUCT (SQL_VARBINARY),
		MAKESQLTYPESTRUCT (SQL_LONGVARBINARY),
		MAKESQLTYPESTRUCT (SQL_TYPE_DATE),
		MAKESQLTYPESTRUCT (SQL_TYPE_TIME),
		MAKESQLTYPESTRUCT (SQL_TYPE_TIMESTAMP),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_MONTH),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_YEAR),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_YEAR_TO_MONTH),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_DAY),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_HOUR),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_MINUTE),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_SECOND),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_DAY_TO_HOUR),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_DAY_TO_MINUTE),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_DAY_TO_SECOND),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_HOUR_TO_MINUTE),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_HOUR_TO_SECOND),
		MAKESQLTYPESTRUCT (SQL_INTERVAL_MINUTE_TO_SECOND),
		MAKESQLTYPESTRUCT (SQL_GUID),

		MAKESQLTYPESTRUCT (SQL_WCHAR),
		MAKESQLTYPESTRUCT (SQL_WVARCHAR),
		MAKESQLTYPESTRUCT (SQL_WLONGVARCHAR),
		MAKESQLTYPESTRUCT (SQL_WCHAR),

		SQLTYPESTRUCT (0)
	};

	for (int i = 0; types [i].m_str.GetLength (); i++)
		if (types [i].m_dw == dwType)
			return types [i].m_str;

	CString str;

	str.Format (_T ("FOOBAR: %d"), dwType);
	return str;
}

static void DebugDump (const CODBCFieldInfo & info)
{
	CString str;
	TYPEMAPSTRUCT type (-1, "Unknown", -1, "Unknown", -1, "Unknown");

	for (int i = 0; ::types [i] != ::lastType; i++) {
		if (types [i].m_nSQLType == info.m_nSQLType) {
			type = types [i];
			break;
		}
	}

	str.Format (
		CString (_T ("info = ")) +
		CString (_T ("\n\tm_strName       = %s")) +
		CString (_T ("\n\tm_nSQLType      = 0x%p")) +
		CString (_T ("\n\t\tSQL type        = %s [0x%p]")) +
		CString (_T ("\n\t\tSQL C type      = %s [0x%p]")) +
		CString (_T ("\n\t\tCDBVariant type = %s [0x%p]")) +
		CString (_T ("\n\tm_nPrecision    = %u")) +
		CString (_T ("\n\tm_nScale        = %u")) +
		CString (_T ("\n\tm_nNullability  = %u")),
		info.m_strName,
		info.m_nSQLType, 
		type.m_strSQLType, type.m_nSQLType, 
		type.m_strSQLCType, type.m_nSQLCType, 
		type.m_strVariantType, type.m_dwVariantType,
		info.m_nPrecision,
		info.m_nScale,
		info.m_nNullability);
	TRACEF (str);
}

static void DebugDump (const FoxjetDatabase::CQualifiedFieldInfo & info)
{
	CString str;
	TYPEMAPSTRUCT type (-1, _T ("Unknown"), -1, _T ("Unknown"), -1, _T ("Unknown"));

	for (int i = 0; ::types [i] != ::lastType; i++) {
		if (types [i].m_nSQLType == info.m_nSQLType) {
			type = types [i];
			break;
		}
	}

	str.Format (
		CString (_T ("info = ")) +
		CString (_T ("\n\tm_strName       = %s")) +
		CString (_T ("\n\tm_strTable      = %s")) +
		CString (_T ("\n\tm_nSQLType      = 0x%p")) +
		CString (_T ("\n\t\tSQL type        = %s [0x%p]")) +
		CString (_T ("\n\t\tSQL C type      = %s [0x%p]")) +
		CString (_T ("\n\t\tCDBVariant type = %s [0x%p]")) +
		CString (_T ("\n\tm_nPrecision    = %u")) +
		CString (_T ("\n\tm_nScale        = %u")) +
		CString (_T ("\n\tm_nNullability  = %u")),
		info.m_strName,
		info.m_strTable,
		info.m_nSQLType, 
		type.m_strSQLType, type.m_nSQLType, 
		type.m_strSQLCType, type.m_nSQLCType, 
		type.m_strVariantType, type.m_dwVariantType,
		info.m_nPrecision,
		info.m_nScale,
		info.m_nNullability);
	TRACEF (str);
}
#endif //_DEBUG

IMPLEMENT_DYNAMIC(COdbcRecordset, CRecordset)
IMPLEMENT_DYNAMIC (CQualifiedFieldInfo, CObject)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COdbcRecordset::COdbcRecordset(COdbcDatabase & database)
:	m_db (database), 
	m_nOpenType (0),
	m_dwOptions (0),
	m_nActive (0),
	m_tmOpened (CTime::GetCurrentTime ()),
	CRecordset (&database)
{
	m_nDefaultType = snapshot;
	m_nFields = 0;
	m_nFieldsBound = 0;
}

COdbcRecordset::COdbcRecordset (COdbcDatabase * pDatabase)
:	m_db (* pDatabase), 
	m_nOpenType (0),
	m_dwOptions (0),
	m_nActive (0),
	m_tmOpened (CTime::GetCurrentTime ()),
	CRecordset (pDatabase)
{
	ASSERT (pDatabase);
	m_nDefaultType = snapshot;
	m_nFields = 0;
	m_nFieldsBound = 0;
}

COdbcRecordset::~COdbcRecordset()
{
	if (m_nActive > 0) {
		TRACEF (_T ("******** ACTIVE: ~COdbcRecordset: ") + m_strSQL);
		ASSERT (0);
	}

	if (IsOpen ())
		Close ();
}

CString COdbcRecordset::GetDefaultConnect ()
{
	return _T ("ODBC;DSN=MarksmanELITE");
}

CString COdbcRecordset::GetDefaultSQL () 
{
	return m_strSQL;
}

UINT COdbcRecordset::GetOpenType () const
{
	return m_nOpenType;
}

DWORD COdbcRecordset::GetOptions () const
{
	return m_dwOptions;
}

bool COdbcRecordset::IsAddNew () const
{ 
	return m_nEditMode == CRecordset::addnew; 
}

void COdbcRecordset::SetEditMode (int nMode) 
{ 
	m_nEditMode = nMode; 
}


BOOL COdbcRecordset::Open (LPCTSTR lpszSQL, UINT nOpenType, DWORD dwOptions) 
{ 
	return Open (nOpenType, COdbcVariant::FormatUnicode (lpszSQL), dwOptions);
}

BOOL COdbcRecordset::Open (UINT nOpenType, LPCTSTR lpszSQL, DWORD dwOptions)
{
	CString strSQL = m_db.FormatSQL (lpszSQL);

	if (strSQL.GetLength ()) {
		m_strFilter.Empty ();
		m_strSort.Empty ();
	}

	if (m_db.IsProgress ()) 
		TRACEF (strSQL);

	BOOL bResult = CRecordset::Open (nOpenType, COdbcVariant::FormatUnicode (strSQL), dwOptions);

	if (bResult) {
		m_nOpenType = nOpenType;
		m_strSQL = strSQL;
		m_dwOptions = dwOptions;
		m_tmOpened = CTime::GetCurrentTime ();
		m_strDSN = FoxjetDatabase::Extract (m_db.GetConnect (), _T ("DSN="), _T (";"));
	}

	return bResult;
}

void COdbcRecordset::PreBindFields()
{
	RETCODE nRetCode;
	const DWORD dwMax = USHRT_MAX;

	if ((int)m_nFields == (int)GetODBCFieldCount ()) 
		return;

	m_nFields = GetODBCFieldCount ();

	if (m_nFields > 0 || m_nParams > 0)
		AllocStatusArrays();
	
	for (UINT i = 0; i < m_nFields; i++) {
		SQLSMALLINT nReturnedLen = 0, nAttribute = 0;
		TCHAR sz [MAX_PATH];
		CQualifiedFieldInfo * p = new CQualifiedFieldInfo;

		GetODBCFieldInfo (i, * p);
		::ZeroMemory (sz, sizeof (sz [0]) * MAX_PATH);
		AFX_ODBC_CALL(::SQLColAttribute (m_hstmt, i + 1,
			SQL_DESC_BASE_TABLE_NAME, sz, MAX_PATH, 
			&nReturnedLen, &nAttribute));
		p->m_strTable = sz;//a2w (sz);

		if (p->m_strTable.IsEmpty ()) {
			p->m_strTable = GetTableName ();
			p->m_strTable.Replace (_T ("["), _T (""));
			p->m_strTable.Replace (_T ("]"), _T (""));
		}

		for (int j = 0; ::types [j] != ::lastType; j++) {
			if (p->m_nSQLType == types [j].m_nSQLType) {
				p->m_value.SetType (types [j].m_dwVariantType);
				break;
			}
		}

		if (p->m_value.m_dwType == DBVT_NULL)
			p->m_value.SetType (DBVT_STRING);

		if (p->m_nSQLType == SQL_WVARCHAR && !p->m_nPrecision) 
			p->m_nPrecision = dwMax;

		m_vFields.Add (p);

		#ifdef _DEBUG
//		DebugDump (* p);
		#endif //_DEBUG
	}
}

void COdbcRecordset::DebugDump ()
{
#ifdef _DEBUG
	TRACEF (m_strSQL);

	for (int i = 0; i < GetODBCFieldCount (); i++) {
		CODBCFieldInfo s;
			
		GetODBCFieldInfo ((short)i, s);
		::DebugDump (s);
	}
#endif //_DEBUG
}

void COdbcRecordset::Close ()
{
	for (int i = 0; i < m_vFields.GetSize (); i++) {
		CQualifiedFieldInfo * p = (CQualifiedFieldInfo *)m_vFields [i];
		delete p;
	}

	m_vFields.RemoveAll ();
	CRecordset::Close ();
}

void COdbcRecordset::DoFieldExchange (CFieldExchange * pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);

	for (int i = 0; i < m_vFields.GetSize (); i++) {
		CQualifiedFieldInfo & info = * (CQualifiedFieldInfo *)m_vFields [i];

		switch (info.m_value.m_dwType) { 
		case DBVT_BOOL:
			RFX_Bool (pFX, info.m_strName, info.m_value.m_boolVal);
			break;
		case DBVT_UCHAR:
			RFX_Byte (pFX, info.m_strName, info.m_value.m_chVal);
			break;
		case DBVT_SHORT:
			RFX_Int (pFX, info.m_strName, info.m_value.m_iVal);
			break;
		case DBVT_LONG:
			RFX_Long (pFX, info.m_strName, info.m_value.m_lVal);
			break;
		case DBVT_SINGLE:
			RFX_Single (pFX, info.m_strName, info.m_value.m_fltVal);
			break;
		case DBVT_DOUBLE:
			RFX_Double (pFX, info.m_strName, info.m_value.m_dblVal);
			break;
		case DBVT_DATE:
			ASSERT (info.m_value.m_pdate);
			RFX_Date (pFX, info.m_strName, * info.m_value.m_pdate);
			break;
		case DBVT_STRING:
			{
#ifdef _DEBUG
				try {
#endif //_DEBUG
					ASSERT (info.m_value.m_pstring);
					int nMax = max (1, min ((int)info.m_nPrecision, USHRT_MAX));
					ASSERT (nMax >= 1 && nMax <= USHRT_MAX);
					RFX_TextUnicode (pFX, info.m_strName, * info.m_value.m_pstring, nMax, info.m_nSQLType, info.m_nScale);
#ifdef _DEBUG
				}
				catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
				catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
#endif //_DEBUG
			}
			break;
		case DBVT_BINARY:
			try {
				int nMax = USHRT_MAX;
				TRACEF (info.m_strName);
				ASSERT (info.m_value.m_pbinary);

				UINT nField;
				pFX->IsFieldType(&nField);
				//RFX_Binary (pFX, info.m_strName, * info.m_value.m_pbinary, nMax);
			}
			catch (CDBException * e)		{ HANDLEEXCEPTION (e); 	}
			catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
			break;
		default:
		case DBVT_NULL:
			ASSERT (0);
			break;
		}
	}
}

CString COdbcRecordset::GetFieldName (short nIndex) const
{
	if (nIndex >= 0 && nIndex < m_vFields.GetSize ()) {
		CQualifiedFieldInfo * p = (CQualifiedFieldInfo *)m_vFields [nIndex];
		return _T ("[") + p->m_strTable + _T ("].[") + p->m_strName + _T ("]");
	}
	else {
		CDBException * p = new CDBException (SQL_ERROR);
		p->m_strError.Format (m_strBadFieldIndex, nIndex);
		THROW (p);
	}
}

short COdbcRecordset::GetFieldIndex (LPCTSTR lpszName) const
{
	CString strName (lpszName), strTable;
	int nIndex;

	strName.Remove ('[');
	strName.Remove (']');

	if ((nIndex = strName.Find (_T ("."))) != -1) {
		strTable = strName.Left (nIndex);
		strName = strName.Mid (nIndex + 1);

		for (int i = 0; i < m_vFields.GetSize (); i++) {
			const CQualifiedFieldInfo & info = * (CQualifiedFieldInfo *)m_vFields [i];

			if (!info.m_strName.CompareNoCase (strName) &&
				!info.m_strTable.CompareNoCase (strTable))
			{
				return i;
			}
		}
	}
	else {
		for (int i = 0; i < m_vFields.GetSize (); i++) {
			const CQualifiedFieldInfo & info = * (CQualifiedFieldInfo *)m_vFields [i];

			if (!info.m_strName.CompareNoCase (strName))
				return i;
		}
	}

	return -1;
}

void COdbcRecordset::GetFieldValue (LPCTSTR lpszName, COdbcVariant & v) const
{
	short nIndex = GetFieldIndex (lpszName);
	
	if (nIndex == -1) {
		CDBException * p = new CDBException (SQL_ERROR);
		p->m_strError.Format (m_strBadFieldName, lpszName);
		THROW (p);
	}
	else 
		GetFieldValue (nIndex, v);
}

void COdbcRecordset::GetFieldValue (short nIndex, COdbcVariant & v) const
{
	if (nIndex >= 0 && nIndex < m_vFields.GetSize ()) {
		CQualifiedFieldInfo & info = * (CQualifiedFieldInfo *)m_vFields [nIndex];

		switch (info.m_nSQLType) {
		case SQL_BINARY:
		case SQL_VARBINARY:
		case SQL_LONGVARBINARY:
			v = _T ("[") + LoadString (IDS_BINARYDATA) + _T ("]");
			return;
		}

		v = info.m_value;
	}
	else {
		CDBException * p = new CDBException (SQL_ERROR);
		p->m_strError.Format (m_strBadFieldIndex, nIndex);
		THROW (p);
	}
}

COdbcVariant COdbcRecordset::GetFieldValue (LPCTSTR lpszName) const
{
	COdbcVariant v;
	GetFieldValue (lpszName, v);
	return v;
}

COdbcVariant COdbcRecordset::GetFieldValue (short nIndex) const
{
	COdbcVariant v;
	GetFieldValue (nIndex, v);
	return v;
}

void COdbcRecordset::SetFieldValue (LPCTSTR lpszName, const COdbcVariant & varValue)
{
	short nIndex = GetFieldIndex (lpszName);
	
	if (nIndex == -1) {
		CDBException * p = new CDBException (SQL_ERROR);
		p->m_strError.Format (m_strBadFieldName, lpszName);
		THROW (p);
	}
	else 
		SetFieldValue (nIndex, varValue);
}

void COdbcRecordset::SetFieldValue (short nIndex, const COdbcVariant & varValue)
{
	if (nIndex >= 0 && nIndex < m_vFields.GetSize ()) {
		CQualifiedFieldInfo & info = * (CQualifiedFieldInfo *)m_vFields [nIndex];

		// address registered with SQLBindCol can't change
		if (!info.m_value.CopyInPlace (varValue)) {
			CDBException * p = new CDBException (SQL_ERROR);
			p->m_strError.Format (m_strBadVariantConversion, 
				GetFieldName (nIndex), varValue.ToString (), 
				GetVariantType (varValue.m_dwType));
			THROW (p);
		}
	}
	else {
		CDBException * p = new CDBException (SQL_ERROR);
		p->m_strError.Format (m_strBadFieldIndex, nIndex);
		THROW (p);
	}
}

ULONG COdbcRecordset::CountRecords ()
{
	ULONG lCount = 0;

	try {
		if (IsOpen ()) { 
			// TODO: no way to tell if rec count is really 0 or 1

			if (!IsBOF ())
				MoveFirst ();

			while (!IsEOF ()) {
				lCount++;
				MoveNext ();
			}

			if (!IsBOF ())
				MoveFirst ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return lCount;
}

void COdbcRecordset::AddNew ()
{
	ASSERT (0);
	CRecordset::AddNew ();
}

void COdbcRecordset::Edit ()
{
	ASSERT (0);
	CRecordset::Edit ();
}

BOOL COdbcRecordset::Update ()
{
	ASSERT (0);
	return CRecordset::Update ();
}

bool COdbcRecordset::GetFieldInfo (const CString & strField, CODBCFieldInfo & info)
{
	for (int i = 0; i < GetODBCFieldCount (); i++) {
		CODBCFieldInfo s;
			
		GetODBCFieldInfo ((short)i, s);

		if (!s.m_strName.CompareNoCase (strField)) {
			info = s;
			return true;
		}
	}

	return false;
}
