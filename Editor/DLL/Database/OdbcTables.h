#if !defined(AFX_ODBCOdbcTables_H__0B9395B2_FDBF_45FA_82DA_07569B1D861C__INCLUDED_)
#define AFX_ODBCOdbcTables_H__0B9395B2_FDBF_45FA_82DA_07569B1D861C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OdbcTables.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COdbcTables recordset
#include "DbApi.h"

namespace FoxjetDatabase 
{
	class DB_API COdbcTables : public CRecordset
	{
	DECLARE_DYNAMIC(COdbcTables)

	public:
		COdbcTables(CDatabase* pDatabase = NULL);
		BOOL Open(UINT nOpenType = forwardOnly, LPCSTR lpszSQL = NULL,
			DWORD dwOptions = readOnly);

	// Field/Param Data
		//{{AFX_FIELD(COdbcTables, CRecordset)
		CString m_strQualifier;
		CString m_strOwner;
		CString m_strName;
		CString m_strType;
		CString m_strRemarks;
		//}}AFX_FIELD

		CString m_strQualifierParam;
		CString m_strOwnerParam;
		CString m_strNameParam;
		CString m_strTypeParam;

	// Implementation
	protected:
		virtual CString GetDefaultConnect();    // default connection string
		virtual CString GetDefaultSQL();    // default SQL for Recordset
		virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	};
}; //namespace FoxjetDatabase

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ODBCOdbcTables_H__0B9395B2_FDBF_45FA_82DA_07569B1D861C__INCLUDED_)
