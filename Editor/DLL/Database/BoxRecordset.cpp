// BoxRecordset.cpp : implementation file
//

#include "stdafx.h"
#include "Database.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Debug.h"
#include "Serialize.h"
#include "compare.h"
#include "Parse.h"

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Boxes;

extern HINSTANCE hInstance;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)

FoxjetDatabase::tagBOXLINKSTRUCT::tagBOXLINKSTRUCT ()
{
}

FoxjetDatabase::tagBOXLINKSTRUCT::tagBOXLINKSTRUCT (const tagBOXLINKSTRUCT & rhs)
:	m_lLineID	(rhs.m_lLineID),
	m_lBoxID	(rhs.m_lBoxID)
{
}

tagBOXLINKSTRUCT & FoxjetDatabase::tagBOXLINKSTRUCT::operator = (const tagBOXLINKSTRUCT & rhs)
{
	if (this != &rhs) {
		m_lLineID	= rhs.m_lLineID;
		m_lBoxID	= rhs.m_lBoxID;
	}

	return * this;
}

FoxjetDatabase::tagBOXLINKSTRUCT::~tagBOXLINKSTRUCT ()
{
}

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)

FoxjetDatabase::tagBOXSTRUCT::tagBOXSTRUCT ()
{
}

FoxjetDatabase::tagBOXSTRUCT::tagBOXSTRUCT (const tagBOXSTRUCT & rhs)
:	m_lID		(rhs.m_lID),
	m_strName	(rhs.m_strName),
	m_strDesc	(rhs.m_strDesc),
	m_lWidth	(rhs.m_lWidth),
	m_lLength	(rhs.m_lLength),
	m_lHeight	(rhs.m_lHeight)
{
}

tagBOXSTRUCT & FoxjetDatabase::tagBOXSTRUCT::operator = (const tagBOXSTRUCT & rhs)
{
	if (this != &rhs) {
		m_lID		= rhs.m_lID;
		m_strName	= rhs.m_strName;
		m_strDesc	= rhs.m_strDesc;
		m_lWidth	= rhs.m_lWidth;
		m_lLength	= rhs.m_lLength;
		m_lHeight	= rhs.m_lHeight;
	}

	return * this;
}

FoxjetDatabase::tagBOXSTRUCT::~tagBOXSTRUCT ()
{
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::BOXSTRUCT & lhs, const FoxjetDatabase::BOXSTRUCT & rhs)
{
	return 
		lhs.m_lID		== rhs.m_lID &&
		lhs.m_strName	== rhs.m_strName &&
		lhs.m_strDesc	== rhs.m_strDesc &&
		lhs.m_lWidth	== rhs.m_lWidth &&
		lhs.m_lLength	== rhs.m_lLength &&
		lhs.m_lHeight	== rhs.m_lHeight;
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::BOXSTRUCT & lhs, const FoxjetDatabase::BOXSTRUCT & rhs)
{
	return !(lhs == rhs);
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::BOXLINKSTRUCT & lhs, const FoxjetDatabase::BOXLINKSTRUCT & rhs)
{
	return
		lhs.m_lLineID	== rhs.m_lLineID &&
		lhs.m_lBoxID	== rhs.m_lBoxID;
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::BOXLINKSTRUCT & lhs, const FoxjetDatabase::BOXLINKSTRUCT & rhs)
{
	return !(lhs == rhs);
}

DB_API const FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator >> (const FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::BOXSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		s.m_lID		= (long)rst.GetFieldValue (m_lpszID);
		s.m_strName	= (CString)rst.GetFieldValue (m_lpszName);
		s.m_strDesc	= (CString)rst.GetFieldValue (m_lpszDesc);
		s.m_lLength = (long)rst.GetFieldValue (m_lpszLength);
		s.m_lWidth	= (long)rst.GetFieldValue (m_lpszWidth);
		s.m_lHeight = (long)rst.GetFieldValue (m_lpszHeight);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator << (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::BOXSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	CStringArray name, value;

	if (s.m_lID == -1) 
		rst.SetEditMode (COdbcRecordset::addnew);

	name.Add (m_lpszName);			value.Add (s.m_strName);
	name.Add (m_lpszDesc);			value.Add (s.m_strDesc);
	name.Add (m_lpszLength);		value.Add (ToString ((long)s.m_lLength));
	name.Add (m_lpszWidth);			value.Add (ToString ((long)s.m_lWidth));
	name.Add (m_lpszHeight);		value.Add (ToString ((long)s.m_lHeight));

	CString strSQL = FoxjetDatabase::FormatSQL (rst, m_lpszTable, name, value, m_lpszID, s.m_lID);
	TRACEF (strSQL);
	
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

	if (s.m_lID == -1)
		s.m_lID = GetMaxID (rst.GetOdbcDatabase (), m_lpszTable, m_lpszID);
/*
	try {
		if (rst.IsAddNew ())
			rst.SetFieldValue (m_lpszID,	(long)GetNextID (rst.GetOdbcDatabase (), m_lpszTable));

		rst.SetFieldValue (m_lpszName,		s.m_strName);
		rst.SetFieldValue (m_lpszDesc,		s.m_strDesc);
		rst.SetFieldValue (m_lpszLength,	(long)s.m_lLength);
		rst.SetFieldValue (m_lpszWidth,		(long)s.m_lWidth);
		rst.SetFieldValue (m_lpszHeight,	(long)s.m_lHeight);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/

	return rst;
}

DB_API const FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator >> (const FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::BOXLINKSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		s.m_lLineID		= (long)rst.GetFieldValue (BoxToLine::m_lpszLineID);
		s.m_lBoxID		= (long)rst.GetFieldValue (BoxToLine::m_lpszBoxID);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator << (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::BOXLINKSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	CStringArray name, value;

	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (BoxToLine::m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	name.Add (BoxToLine::m_lpszLineID);			value.Add (ToString ((long)s.m_lLineID));
	name.Add (BoxToLine::m_lpszBoxID);			value.Add (ToString ((long)s.m_lBoxID));

	rst.SetEditMode (COdbcRecordset::addnew);
	CString strSQL = FoxjetDatabase::FormatSQL (rst, BoxToLine::m_lpszTable, name, value, _T (""), 0);
	rst.GetOdbcDatabase ().ExecuteSQL (strSQL);
/*
	try {
		rst.SetFieldValue (BoxToLine::m_lpszLineID,		(long)s.m_lLineID);
		rst.SetFieldValue (BoxToLine::m_lpszBoxID,		(long)s.m_lBoxID);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/

	return rst;
}

DB_API UINT FoxjetDatabase::GetBoxRecordCount (COdbcDatabase & database, ULONG lLineID)
{
	DBMANAGETHREADSTATE (database);

	UINT nCount = 0;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (database);
		CString str;

		if (lLineID == ALL)
			str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);
		else
			str.Format (
				_T ("SELECT [%s].* FROM [%s] INNER JOIN [%s]")
				_T (" ON [%s].[%s] = [%s].[%s] WHERE ((([%s].[%s])=%u));"), 
				m_lpszTable,
				m_lpszTable,
				BoxToLine::m_lpszTable,
				m_lpszTable, m_lpszID,
				BoxToLine::m_lpszTable, BoxToLine::m_lpszBoxID,
				BoxToLine::m_lpszTable, BoxToLine::m_lpszLineID, 
				lLineID);

		rst.Open (COdbcRecordset::snapshot, str);
		nCount = rst.CountRecords ();
		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return nCount;
}

DB_API bool FoxjetDatabase::GetBoxRecord (FoxjetDatabase::COdbcDatabase & database, ULONG lID, FoxjetDatabase::BOXSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, lID);
		rst.Open (str);
		
		if (!rst.IsEOF ()) {
			rst >> s;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API ULONG FoxjetDatabase::GetBoxID (COdbcDatabase & database, const CString & strName)
{
	DBMANAGETHREADSTATE (database);

	ULONG lID = NOTFOUND;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s';"), 
			m_lpszTable, m_lpszName, strName);
		rst.Open (str);
		
		if (!rst.IsEOF ()) 
			lID = (long)rst.GetFieldValue (m_lpszID);

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return lID;
}

DB_API bool FoxjetDatabase::AddBoxRecord (COdbcDatabase & database, BOXSTRUCT & s, ULONG lLineID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	if (GetBoxID (database, s.m_strName) != NOTFOUND) {
		ASSERT (0);
		return false;
	}

	try {
		CArray <LINESTRUCT, LINESTRUCT &> vLines;
		COdbcRecordset rst (&database);
		COdbcRecordset rstLinks (&database);
		CString str;

		s.m_lID = -1;
		rst << s;

		if (lLineID == ALL) 
			GetPrinterLines (database, GetPrinterID (database), vLines); //GetLineRecords (database, vLines);
		else {
			LINESTRUCT line;
	
			if (GetLineRecord (database, lLineID, line))
				vLines.Add (line);
		}

		for (int i = 0; i < vLines.GetSize (); i++) {
			BOXLINKSTRUCT link;

			link.m_lBoxID = s.m_lID;
			link.m_lLineID = vLines [i].m_lID;

			rstLinks << link;
		}

		rstLinks.Close ();
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::UpdateBoxRecord (COdbcDatabase & database, BOXSTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);

		rst << s;
		bResult = true;
		/* TODO
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, s.m_lID);
		rst.Open (str, CRecordset::dynaset);

		if (!rst.IsEOF ()) {
			rst.Edit ();
			rst << s;
			rst.Update ();
			bResult = true;
		}

		rst.Close ();
		*/
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::DeleteBoxRecord (COdbcDatabase & database, ULONG lID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		CString str;
		
		// remove the images referenced by this box
		str.Format (
			_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
			Images::m_lpszTable, Images::m_lpszBoxID, lID);
		database.ExecuteSQL (str);

		// remove the line(s) references to this box
		str.Format (
			_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
			BoxToLine::m_lpszTable, BoxToLine::m_lpszBoxID, lID);
		database.ExecuteSQL (str);

		// remove the box
		str.Format (
			_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, lID);
		database.ExecuteSQL (str);

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetNetworkedBoxRecords(COdbcDatabase & database, ULONG lLineID, CArray <BOXSTRUCT, BOXSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	
	ASSERT (database.IsOpen ());

	v.RemoveAll ();

	try {
		COdbcRecordset rst (&database);
		CString str;

		if (lLineID == ALL) {
			str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);
			ASSERT (0);
		}
		else
			str.Format (
				_T ("SELECT [%s].* ")
				_T ("FROM [%s] INNER JOIN ([%s] INNER JOIN [%s] ON [%s].[%s] = [%s].[%s]) ON [%s].[%s] = [%s].[%s] ")
				_T ("WHERE (((CStr ([%s].[%s])) In (SELECT CStr ([%s].[%s]) as x FROM [%s] WHERE [%s]=%d))); "), 
				m_lpszTable,
				Lines::m_lpszTable,
				m_lpszTable,
				BoxToLine::m_lpszTable,
				m_lpszTable, m_lpszID,
				BoxToLine::m_lpszTable, BoxToLine::m_lpszBoxID,
				Lines::m_lpszTable, Lines::m_lpszID,
				BoxToLine::m_lpszTable, BoxToLine::m_lpszLineID,
				Lines::m_lpszTable, Lines::m_lpszName,
				Lines::m_lpszTable, Lines::m_lpszName,
				Lines::m_lpszTable, 
				Lines::m_lpszID, 
				lLineID);
		TRACEF (str);
		rst.Open (str);

		while (!rst.IsEOF ()) {
			BOXSTRUCT box;
			rst >> box;
			v.Add (box);
			rst.MoveNext ();
		}

		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetBoxRecords (COdbcDatabase & database, ULONG lLineID, CArray <BOXSTRUCT, BOXSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	
	ASSERT (database.IsOpen ());

	v.RemoveAll ();

	try {
		COdbcRecordset rst (&database);
		CString str;

		if (lLineID == ALL)
			str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);
		else
			str.Format (
				_T ("SELECT * FROM [%s] INNER JOIN [%s]")
				_T (" ON [%s].[%s] = [%s].[%s] WHERE ((([%s].[%s])=%u));"), 
				m_lpszTable,
				BoxToLine::m_lpszTable,
				m_lpszTable, m_lpszID,
				BoxToLine::m_lpszTable, BoxToLine::m_lpszBoxID,
				BoxToLine::m_lpszTable, BoxToLine::m_lpszLineID, 
				lLineID);

		rst.Open (str);

		while (!rst.IsEOF ()) {
			BOXSTRUCT box;
			rst >> box;
			v.Add (box);
			rst.MoveNext ();
		}

		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetFirstBoxRecord (COdbcDatabase & database, ULONG lLineID, BOXSTRUCT & box)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		CString str;

		if (lLineID == ALL)
			str.Format (_T ("SELECT TOP 1 * FROM [%s];"), m_lpszTable);
		else
			str.Format (
				_T ("SELECT TOP 1 * FROM [%s] INNER JOIN [%s]")
				_T (" ON [%s].[%s] = [%s].[%s] WHERE ((([%s].[%s])=%u));"), 
				m_lpszTable,
				BoxToLine::m_lpszTable,
				m_lpszTable, m_lpszID,
				BoxToLine::m_lpszTable, BoxToLine::m_lpszBoxID,
				BoxToLine::m_lpszTable, BoxToLine::m_lpszLineID, 
				lLineID);

		COdbcRecordset rst (database);

		rst.Open (str);

		if (!rst.IsEOF ()) {
			rst >> box;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

