#ifndef __DBIMP_H__
#define __DBIMP_H__

#include "Database.h"
#include "OdbcDatabase.h"

void GetValveRecords (FoxjetDatabase::COdbcDatabase & database, FoxjetDatabase::HEADSTRUCT & h);
void UpdateValveRecords (FoxjetDatabase::COdbcDatabase & database, const FoxjetDatabase::HEADSTRUCT & h);

#endif //__DBIMP_H__
