#include "StdAfx.h"
#include <WinIoCtl.h>
#include "FileExt.h"
#include "Database.h"
#include "Debug.h"
#include "Resource.h"
#include "TemplExt.h"
#include "AnsiString.h"

using namespace FoxjetFile;
using namespace FoxjetDatabase;

static const CString strMsgExt = _T ("msg");
static const CString strTaskExt = _T ("tsk");

DB_API CString FoxjetFile::GetRootPath ()
{
	CString str = LoadString (IDS_PROJNAME);
	str = _T ("C:\\") + str + _T ("\\");
	return  str;
}

DB_API DOCTYPE FoxjetFile::GetFileType (const CString & strPath)
{
	if (!FoxjetFile::GetFileExt (strPath).CompareNoCase (::strMsgExt))
		return FoxjetFile::MESSAGE;
	if (!FoxjetFile::GetFileExt (strPath).CompareNoCase (::strTaskExt))
		return FoxjetFile::TASK;
	else
		return FoxjetFile::UNKNOWNDOCTYPE;
}

DB_API CString FoxjetFile::GetFileName (const CString & strPath)
{
	CString str (strPath);
	const CString strRoot = FoxjetFile::GetRootPath ();
	int nIndex = FindNoCase (str, strRoot);
	
	// remove the root path
	if (nIndex != -1) {
		nIndex += strRoot.GetLength ();

		if (nIndex >= 0 && nIndex < str.GetLength ()) 
			str = str.Mid (nIndex);
	}

	nIndex = str.Find (_T ("\\"));

	if (nIndex != -1) {
		nIndex++;
		if (nIndex >= 0 && nIndex < str.GetLength ()) 
			str = str.Mid (nIndex);
	}

	str.Replace (GetModifiedSuffix (), _T (""));

	return str;
}

DB_API CString FoxjetFile::GetFileTitle (const CString & strPath)
{
	CString str = FoxjetFile::GetFileName (strPath);
	int nIndex = str.Find (_T ("."));

	if (nIndex != -1) 
		str = str.Left (nIndex);

	str.Replace (GetModifiedSuffix (), _T (""));

	return str;
}

DB_API CString FoxjetFile::GetFileExt (const CString & strPath)
{
	CString str;
	int nIndex = strPath.ReverseFind ((TCHAR)'.');

	if (nIndex != -1) {
		nIndex++;

		if (nIndex >= 0 && nIndex < strPath.GetLength ()) 
			str = strPath.Mid (nIndex);
	}

	str.Replace (GetModifiedSuffix (), _T (""));

	return str;
}


DB_API CString FoxjetFile::GetFileExt (const FoxjetFile::DOCTYPE & type)
{	
	switch (type) {
	case FoxjetFile::MESSAGE:	return ::strMsgExt;	
	case FoxjetFile::TASK:		return ::strTaskExt;
	default:					return _T ("");
	}
}

DB_API CString FoxjetFile::GetFileHead (const CString & strPath)
{
	CString str (strPath);
	const CString strRoot = FoxjetFile::GetRootPath ();
	int nIndex = FindNoCase (str, strRoot);

	if (nIndex != -1) {
		nIndex += strRoot.GetLength ();

		if (nIndex >= 0 && nIndex < str.GetLength ()) 
			str = str.Mid (nIndex);
	}

	nIndex = str.Find (_T ("\\"));

	if (nIndex != -1)
		str = str.Left (nIndex);
	
	str.Replace (GetModifiedSuffix (), _T (""));

	return str;
}

DB_API bool FoxjetFile::IsValidFileTitle (const CString & str)
{
	CString strRestrict [] = 
	{
		_T ("\\"),
		_T ("/"),
		_T ("."),
		_T ("'"),
		_T ("\""),
		GetModifiedSuffix (),
	};

	for (int i = 0; i < ARRAYSIZE (strRestrict); i++) 
		if (str.Find (strRestrict [i]) != -1)
			return false;

	return true;
}

DB_API bool FoxjetFile::IsValidFileHead (const CString & str, FoxjetDatabase::COdbcDatabase & database)
{
	CArray <LINESTRUCT, LINESTRUCT &> vLines;

	if (GetPrinterLines (database, GetPrinterID (database), vLines)) //GetLineRecords (database, vLines)) 
		for (int i = 0; i < vLines.GetSize (); i++) 
			if (GetHeadID (database, vLines [i].m_lID, str) != NOTFOUND)
				return true;

	return false;
}

DB_API CString FoxjetFile::GetRelativeName (const CString & strPath)
{
	CString strFile = FoxjetFile::GetFileName (strPath);
	CString strLib = FoxjetFile::GetFileHead (strPath);

	if (strFile.GetLength () && strLib.GetLength ()) {
		CString str = strLib + _T ("\\") + strFile;

		str.Replace (GetModifiedSuffix (), _T (""));
		
		return str;
	}

	return strPath;
}

DB_API CString FoxjetFile::GetModifiedSuffix () 
{ 
	return _T (" *"); 
}

DB_API bool FoxjetFile::FileExists (const CString & strFile)
{
	bool bResult = false;
	HANDLE hFile = ::CreateFile (strFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 
		FILE_FLAG_SEQUENTIAL_SCAN, NULL);

	if (hFile != INVALID_HANDLE_VALUE) {
		bResult = true;
		::CloseHandle (hFile);
	}

	return bResult;
}

DB_API CString FoxjetFile::GetPath(const CString& strFile)
{
	CString str;
	int nIndex = strFile.ReverseFind('\\');

	if (nIndex != -1)
		str = strFile.Left(nIndex);

	return str;
}

DB_API CString FoxjetFile::GetFileTitle (const CString & strActualFile, CString & strFolder, CString & strTitle, CString & strSuffix)
{
	int nIndex = strActualFile.ReverseFind ('.');

	if (nIndex != -1 && (nIndex < strActualFile.GetLength () - 1)) 
		strSuffix = strActualFile.Mid (nIndex + 1);

	nIndex = strActualFile.ReverseFind ('\\');

	if (nIndex != -1 && (nIndex < strActualFile.GetLength () - 1)) {
		strTitle = strActualFile.Mid (nIndex + 1);
		strTitle.Replace (_T (".") + strSuffix, _T (""));
		strFolder = strActualFile.Left (nIndex);
		return strTitle;
	}

	return _T ("");
}

//#ifdef TRACEF
//	#undef TRACEF
//	#define TRACEF(str) TraceRelease (str, _T (__FILE__), __LINE__)
//#endif

static CString FormatFilename (CString strFile)
{
	int nIndex = strFile.ReverseFind ('\\');

	if (nIndex != -1) {
		const CString strOldTitle = strFile.Mid (nIndex + 1);
		CString strNewTitle (strOldTitle);
		TCHAR c [] = { '\\', '/', ':', '*', '?', '\"', '<', '>', };

		for (int i = 0; i < ARRAYSIZE (c); i++) { 
			CString str;

			str.Format (_T ("&#%02d;"), (int)c [i]);
			strNewTitle.Replace (CString (c [i]), str);
		}

		strFile.Replace (strOldTitle, strNewTitle);
	}

	return strFile;
}

static bool bWriteDirect = false;

DB_API void FoxjetFile::SetWriteDirect (bool b)
{
	::bWriteDirect = b;
}

DB_API bool FoxjetFile::IsWriteDirect ()
{
	return ::bWriteDirect;
}

DB_API DWORD FoxjetFile::WriteDirect (CString strFile, const CString & strData)
{
	strFile = FormatFilename (strFile);

	{
		CString strOldData = ReadDirect (strFile);

		if (strOldData == strData)
			return GetFileSize (strFile);
	}

	const BYTE nUTF16 [] = { 0xFF, 0xFE };
	const TCHAR cNULL = 0;
	const int nLen = (strData.GetLength () * sizeof (TCHAR)) + ARRAYSIZE (nUTF16) + sizeof (cNULL);
	int n;

	BYTE * pData = new BYTE [nLen + 2];
	BYTE * p = pData;

	n = ARRAYSIZE (nUTF16);
	memcpy (p, nUTF16, n); 
	p += n;

	n = strData.GetLength () * sizeof (TCHAR);
	memcpy (p, (LPVOID)(LPCTSTR)strData, n); 
	p += n;

	n = sizeof (cNULL);
	memcpy (p, &cNULL, n); 
	p += n;

	DWORD dwResult = FoxjetFile::WriteDirect (strFile, pData, nLen);

	delete [] pData;

	return dwResult;
}

DB_API DWORD FoxjetFile::WriteDirect (CString strFile, LPBYTE pData, DWORD dwLen)
{
	static DWORD dwSectorSize = -1;
	DWORD dwResult = 0;
	CString strDrive = _T ("c:");
	int nIndex = strFile.ReverseFind ('\\');

	if (nIndex != -1) {
		CString strDir = strFile.Left (nIndex);
		DWORD dw = ::GetFileAttributes (strDir);

		if (dw == -1) {
			TRACEF (_T ("CreateDir: ") + strDir);
			CreateDir (strDir);
		}
	}

	if ((nIndex = strFile.Find (_T (":"))) != -1) 
		strDrive = strFile.Left (nIndex + 1);

	if (dwSectorSize == -1) {
		const CString strHandle = _T ("\\\\.\\") + strDrive;
		HANDLE hFile = ::CreateFile (strHandle, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

		if (hFile == INVALID_HANDLE_VALUE) {
			dwSectorSize = 0;
			TRACEF (_T ("CreateFile: ") + strHandle + _T (" failed: ") + FORMATMESSAGE (::GetLastError ()));
		}
		else {
			DWORD dwBytesReturned = 0;
			DISK_GEOMETRY_EX info;

			ZeroMemory (&info, sizeof (info));
			BOOL b = ::DeviceIoControl (hFile, IOCTL_DISK_GET_DRIVE_GEOMETRY_EX, NULL, 0, &info, sizeof (info), &dwBytesReturned, NULL);

			if (!b)
				TRACEF (_T ("DeviceIoControl failed: ") + strHandle);

			dwSectorSize = info.Geometry.BytesPerSector;
			::CloseHandle (hFile);
		}
	}

	strFile = FormatFilename (strFile);
	//{ CString str; str.Format (_T ("WriteDirect (%s, 0x%p, %d), dwSectorSize: %d"), strFile, pData, dwLen, dwSectorSize); TRACEF (str); }

	if (!dwSectorSize)
		return -1;

	DWORD dwInternalBufferSize = dwSectorSize;

	while (dwInternalBufferSize < dwLen)
		dwInternalBufferSize += dwSectorSize;

	if (dwLen > dwInternalBufferSize) {
		ASSERT (0);
		return -1;
	}

	char * p, * buf = new char [2 * dwInternalBufferSize - 1];

	ZeroMemory (buf, 2 * dwInternalBufferSize - 1);
	p = (char *) ((DWORD) (buf + dwInternalBufferSize - 1) & ~(dwInternalBufferSize - 1));
	memcpy (p, pData, dwLen);
  
	const DWORD dwDesiredAccess = GENERIC_READ | GENERIC_WRITE;
	const DWORD dwShareMode = FILE_SHARE_READ | FILE_SHARE_WRITE;
	const DWORD dwFlagsAndAttributes = FILE_ATTRIBUTE_NORMAL | FILE_FLAG_NO_BUFFERING | FILE_FLAG_WRITE_THROUGH;

	HANDLE h = ::CreateFile (strFile, 
		dwDesiredAccess,
		dwShareMode,
		NULL, 
		CREATE_NEW,
		dwFlagsAndAttributes,
		0);

	if (h == INVALID_HANDLE_VALUE) {
		if (::GetLastError () != ERROR_FILE_EXISTS)
			TRACEF (strFile + _T (": ") + FORMATMESSAGE (::GetLastError ()));

		h = ::CreateFile (strFile, 
			dwDesiredAccess, 
			dwShareMode, 
			NULL, 
			TRUNCATE_EXISTING,
			dwFlagsAndAttributes,
			0);
	}

	if (h == INVALID_HANDLE_VALUE) 
		TRACEF (strFile + _T (": ") + FORMATMESSAGE (::GetLastError ()));

	if (h && h != INVALID_HANDLE_VALUE) {
		DWORD dwWritten = 0;

		::WriteFile (h, p, dwInternalBufferSize, &dwWritten, NULL); //::WriteFile (h, pData, dwLen, &dwWritten, NULL);

		if (dwWritten <= dwLen) 
			TRACEF (strFile + _T (": ") + FORMATMESSAGE (::GetLastError ()));
		else
			dwResult = dwWritten;

		::FlushFileBuffers (h);
		::CloseHandle (h);
	}

	//{ CString str; str.Format (_T ("WriteDirect: %d"), dwResult); TRACEF (str); }

	delete [] buf;

	return dwResult;
}


DB_API CString FoxjetFile::ReadDirect (CString strFile, const CString & strDefault)
{
	strFile = FormatFilename (strFile);
	DWORD dwSize = GetFileSize (strFile);
	CString strData = strDefault;

	if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
		const BYTE nUTF16 [] = { 0xFF, 0xFE };
		BYTE * p = new BYTE [dwSize + 2];

		memset (p, 0, dwSize + 2);
		int nRead = fread (p, dwSize, 1, fp);
		fclose (fp);

		if (nRead > 0) {
			if (!memcmp (p, nUTF16, ARRAYSIZE (nUTF16))) {
				TCHAR * pData = (TCHAR *)p;
				strData = ++pData;
			}
			else
				strData = a2w (p);
		}

		delete [] p;
	}

	return strData;
}

static ULONG CALLBACK GetFileAttributesTimeoutFunc (LPVOID lpData)
{
	DWORD dwResult = -1;

	if (CString * p = (CString *)lpData) {
		dwResult = ::GetFileAttributes (* p);
		delete p;
	}

	return dwResult;
}

DB_API DWORD FoxjetFile::GetFileAttributesTimeout (LPCTSTR lpszFile, DWORD dwTimeout)
{
	if (_tcsstr (lpszFile, _T ("C:\\")) != NULL)
		return ::GetFileAttributes (lpszFile);

	DWORD dwThreadID = 0;
	HANDLE h = ::CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)GetFileAttributesTimeoutFunc, (LPVOID)new CString (lpszFile), 0, &dwThreadID);

	DWORD dw = ::WaitForSingleObject (h, dwTimeout);

	if (dw == WAIT_OBJECT_0) {
		DWORD dwResult = -1;

		if (::GetExitCodeThread (h, &dwResult))
			return dwResult;
	}

	return -1;
}
