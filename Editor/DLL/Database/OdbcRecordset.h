// OdbcRecordset.h: interface for the COdbcRecordset class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ODBCRECORDSET_H__86C765D1_A2D6_49D2_BA1F_0314E2ABB1AE__INCLUDED_)
#define AFX_ODBCRECORDSET_H__86C765D1_A2D6_49D2_BA1F_0314E2ABB1AE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DbApi.h"
#include <afxdb.h>
#include <afxtempl.h>
#include "OdbcDatabase.h"

namespace FoxjetDatabase
{
	class DB_API COdbcVariant
	{
	public:
		COdbcVariant (bool bVal);
		COdbcVariant (BYTE chVal);
		COdbcVariant (int iVal);
		COdbcVariant (long lVal = 0);
		COdbcVariant (float fltVal); 
		COdbcVariant (double dblVal);
		COdbcVariant (const TIMESTAMP_STRUCT & tm);
		COdbcVariant (const CTime & tm);
		COdbcVariant (const COleDateTime & tm);
		COdbcVariant (const CString & str, DWORD dwType = DBVT_STRING);
		COdbcVariant (const CByteArray & bin); 
		COdbcVariant (const COdbcVariant & rhs);
		COdbcVariant & operator = (const COdbcVariant & rhs);
		virtual ~COdbcVariant ();

	public:
		DWORD m_dwType;

		union
		{
			BOOL				m_boolVal;
			BYTE				m_chVal;
			int					m_iVal;
			long				m_lVal;
			float				m_fltVal;
			double				m_dblVal;
			TIMESTAMP_STRUCT *	m_pdate;
			CString *			m_pstring;
			CByteArray *		m_pbinary;
		};

		operator bool () const			{ return m_boolVal ? true : false; }
		operator BYTE () const			{ return m_chVal; }
		operator int () const			{ return m_iVal; }
		operator long () const			{ return m_lVal; }
		operator float () const			{ return m_fltVal; }
		operator double () const		{ return m_dblVal; }
		operator CTime () const			{ return ToTime (); }
		operator COleDateTime () const	{ return ToOleDateTime (); }
		operator CString () const		{ return ToString (); }

		COleDateTime ToOleDateTime () const;
		// throw COleException, CMemoryException

		bool FromString (const CString & str, DWORD dwVarType = DBVT_STRING);
		// throw COleException, CMemoryException

		CTime ToTime () const;
		void SetType (DWORD dwType);
		void Free ();
		CString ToString () const;
		bool CopyInPlace (const COdbcVariant & rhs);

		static CString FormatUnicode (const CString & str);
		static CString UnformatUnicode (const CString & str);

	protected:
		static void Free (COdbcVariant & v);
		static void Copy (COdbcVariant & lhs, const COdbcVariant & rhs);
	};

	class DB_API CQualifiedFieldInfo : public CODBCFieldInfo
	{
		DECLARE_DYNAMIC (CQualifiedFieldInfo);
	public:
		CQualifiedFieldInfo ();
		CQualifiedFieldInfo (const CQualifiedFieldInfo & rhs);
		CQualifiedFieldInfo & operator = (const CQualifiedFieldInfo & rhs);
		virtual ~CQualifiedFieldInfo ();

		CString			m_strTable;
		COdbcVariant	m_value;
	};

	class DB_API COdbcRecordset : public CRecordset  
	{
		friend class COdbcCache;

		DECLARE_DYNAMIC(COdbcRecordset)
	public:
		COdbcRecordset (COdbcDatabase & database);
		COdbcRecordset (COdbcDatabase * pDatabase);
		virtual ~COdbcRecordset();

		using CRecordset::addnew;
		using CRecordset::edit;
		using CRecordset::none;

	public:
		virtual BOOL Open (LPCTSTR lpszSQL, UINT nOpenType = CRecordset::snapshot, DWORD dwOptions = CRecordset::noDirtyFieldCheck);
		// throw CDBException, CMemoryException 

		virtual BOOL Open (UINT nOpenType = AFX_DB_USE_DEFAULT_TYPE, LPCTSTR lpszSQL = NULL, DWORD dwOptions = CRecordset::noDirtyFieldCheck);
		// throw CDBException, CMemoryException 

		virtual void Close ();
		virtual CString GetDefaultConnect ();
		virtual CString GetDefaultSQL ();

		virtual void DoFieldExchange (CFieldExchange * pFX);
		// throw CDBException

		void GetFieldValue (LPCTSTR lpszName, COdbcVariant & v) const;
		void GetFieldValue (short nIndex, COdbcVariant & v) const;
		COdbcVariant GetFieldValue (LPCTSTR lpszName) const;
		COdbcVariant GetFieldValue (short nIndex) const;
		// throw CDBException

		void SetFieldValue (LPCTSTR lpszName, const COdbcVariant & varValue);
		void SetFieldValue (short nIndex, const COdbcVariant & varValue);
		// throw CDBException

		CString GetFieldName (short nIndex) const;
		// throw CDBException

		short GetFieldIndex (LPCTSTR lpszName) const;

		ULONG CountRecords ();

		const COdbcDatabase & GetOdbcDatabase () const { return m_db; }
		COdbcDatabase & GetOdbcDatabase () { return m_db; }

		bool IsAddNew () const;
		void SetEditMode (int nMode);

		virtual void AddNew ();
		virtual void Edit ();
		virtual BOOL Update ();
		
		//--- undocumented
		UINT GetOpenType () const;
		DWORD GetOptions () const;
		//--- undocumented

		CTime GetCreationTime () const { return m_tmOpened; }

		int GetActive () const { return m_nActive; }

		bool GetFieldInfo (const CString & strField, CODBCFieldInfo & info);
		void DebugDump ();

		CString GetDSN () const { return m_strDSN; }

		static const CString m_strBadFieldName;
		static const CString m_strBadFieldIndex;
		static const CString m_strBadVariantConversion;

	protected:
		COdbcRecordset ();
		COdbcRecordset (const COdbcRecordset &);
		COdbcRecordset & operator = (const COdbcRecordset &);
		
		virtual void PreBindFields();   

		COdbcDatabase & m_db;
		CPtrArray m_vFields;
		CString m_strSQL;
		UINT m_nOpenType;
		DWORD m_dwOptions;
		CTime m_tmOpened;
		int m_nActive;
		CString m_strDSN;
	};
}; //namespace FoxjetDatabase

#endif // !defined(AFX_ODBCRECORDSET_H__86C765D1_A2D6_49D2_BA1F_0314E2ABB1AE__INCLUDED_)
