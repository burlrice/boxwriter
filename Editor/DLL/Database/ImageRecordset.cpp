#include "stdafx.h"
#include "Database.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Debug.h"
#include "Serialize.h"
#include "compare.h"
#include "Parse.h"

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Images;

extern HINSTANCE hInstance;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)

FoxjetDatabase::tagIMAGESTRUCT::tagIMAGESTRUCT ()
{
}

FoxjetDatabase::tagIMAGESTRUCT::tagIMAGESTRUCT (const tagIMAGESTRUCT & rhs)
:	m_lBoxID		(rhs.m_lBoxID),
	m_nPanel		(rhs.m_nPanel),
	m_strImage		(rhs.m_strImage),
	m_lAttributes	(rhs.m_lAttributes)
{
}

tagIMAGESTRUCT & FoxjetDatabase::tagIMAGESTRUCT::operator = (const tagIMAGESTRUCT & rhs)
{
	if (this != &rhs) {
		m_lBoxID		= rhs.m_lBoxID;
		m_nPanel		= rhs.m_nPanel;
		m_strImage		= rhs.m_strImage;
		m_lAttributes	= rhs.m_lAttributes;
	}

	return * this;
}

FoxjetDatabase::tagIMAGESTRUCT::~tagIMAGESTRUCT ()
{
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::IMAGESTRUCT & lhs, const FoxjetDatabase::IMAGESTRUCT & rhs)
{
	return
		lhs.m_lBoxID		== rhs.m_lBoxID &&
		lhs.m_nPanel		== rhs.m_nPanel &&
		lhs.m_strImage		== rhs.m_strImage &&
		lhs.m_lAttributes	== rhs.m_lAttributes;
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::IMAGESTRUCT & lhs, const FoxjetDatabase::IMAGESTRUCT & rhs)
{
	return !(rhs == lhs);
}

DB_API const FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator >> (const FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::IMAGESTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		s.m_lBoxID		= (long)rst.GetFieldValue (m_lpszBoxID);
		s.m_nPanel		= (int)rst.GetFieldValue (m_lpszPanel);
		s.m_strImage	= (CString)rst.GetFieldValue (m_lpszImage);
		s.m_lAttributes	= (IMAGEATTRIBUTES)(long)rst.GetFieldValue (m_lpszAttributes);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator << (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::IMAGESTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());
	
	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	CStringArray name, value;
	CString strWhere;

	name.Add (m_lpszBoxID);			value.Add (ToString ((long)s.m_lBoxID));
	name.Add (m_lpszPanel);			value.Add (ToString ((int)s.m_nPanel));
	name.Add (m_lpszImage);			value.Add (s.m_strImage);
	name.Add (m_lpszAttributes);	value.Add (ToString ((long)s.m_lAttributes));

	strWhere.Format (_T (" WHERE [%s]=%d AND [%s]=%d"), 
		m_lpszBoxID, s.m_lBoxID,
		m_lpszPanel, s.m_nPanel);
	CString strSQL = FormatSQL (rst, m_lpszTable, name, value, _T (""), 0) + (!rst.IsAddNew () ? strWhere : _T (""));
	TRACEF (strSQL);
	
	if (!rst.GetOdbcDatabase ().ExecuteSQL (strSQL)) {
		rst.SetEditMode (COdbcRecordset::addnew);
		strSQL = FormatSQL (rst, m_lpszTable, name, value, _T (""), 0);
		TRACEF (strSQL);
		int nResult = rst.GetOdbcDatabase ().ExecuteSQL (strSQL);
		ASSERT (nResult > 0);
	}
/*
	try {
		rst.SetFieldValue (m_lpszBoxID,		(long)s.m_lBoxID);
		rst.SetFieldValue (m_lpszPanel,		(int)s.m_nPanel);
		rst.SetFieldValue (m_lpszImage,		s.m_strImage);
		rst.SetFieldValue (m_lpszAttributes, (long)s.m_lAttributes);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/

	return rst;
}

DB_API bool FoxjetDatabase::GetImageRecord (COdbcDatabase & database, 
											ULONG lBoxID, UINT nPanel, 
											IMAGESTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u AND [%s]=%u;"), 
			m_lpszTable, 
			m_lpszBoxID, lBoxID,
			m_lpszPanel, nPanel);
		rst.Open (str);
		
		if (!rst.IsEOF ()) {
			rst >> s;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::AddImageRecord (COdbcDatabase & database, IMAGESTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (&database);

		rst << s;
		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);
		rst.Open (str, CRecordset::dynaset);
		rst.AddNew ();
		rst << s;
		rst.Update ();
		rst.Close ();
		*/

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::UpdateImageRecord (COdbcDatabase & database, IMAGESTRUCT & s)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		COdbcRecordset rst (database);

		rst << s;

		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u AND [%s]=%u;"), 
			m_lpszTable, 
			m_lpszBoxID, s.m_lBoxID,
			m_lpszPanel, s.m_nPanel);
		rst.Open (str, CRecordset::dynaset);
		rst.Edit ();
		rst << s;
		rst.Update ();
		rst.Close ();
		*/

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::DeleteImageRecord (COdbcDatabase & database, ULONG lBoxID, UINT nPanel)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		CString str;

		if (nPanel == ALL) 
			str.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
				m_lpszTable, 
				m_lpszBoxID, lBoxID);
		else
			str.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%u AND [%s]=%u;"), 
				m_lpszTable, 
				m_lpszBoxID, lBoxID,
				m_lpszPanel, nPanel);

		database.ExecuteSQL (str);
		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetImageRecords (COdbcDatabase & database, ULONG lBoxID, CArray <IMAGESTRUCT, IMAGESTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	v.RemoveAll ();

	try {
		COdbcRecordset rst (database);
		CString str;

		str.Format (_T (
			"SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, 
			m_lpszBoxID, lBoxID);
		rst.Open (str);

		while (!rst.IsEOF ()) {
			IMAGESTRUCT image;
			rst >> image;
			v.Add (image);
			rst.MoveNext ();
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return v.GetSize () > 0;
}

