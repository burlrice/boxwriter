// OdbcDatabase.cpp: implementation of the COdbcDatabase class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "Database.h"
#include "Resource.h"
#include "OdbcDatabase.h"
#include "OdbcRecordset.h"
#include "OdbcTable.h"
#include "OdbcTables.h"
#include "Debug.h"
#include "Parse.h"
#include "Registry.h"
#include "TemplExt.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


using namespace FoxjetDatabase;

static DIAGNOSTIC_CRITICAL_SECTION (csOdbcDatabase);
static CArray <COdbcDatabase *, COdbcDatabase *> vDatabases;

static int FindDB (COdbcDatabase * p)
{
	LOCK (::csOdbcDatabase);

	for (int i = 0; i < ::vDatabases.GetSize (); i++)
		if (::vDatabases [i] == p)
			return i;

	return -1;
}

static int FindDB (DWORD dwThreadID, LPCTSTR lpszDSN)
{
	LOCK (::csOdbcDatabase);

	for (int i = 0; i < ::vDatabases.GetSize (); i++) {
		COdbcDatabase * p = ::vDatabases [i];
		CString strDSN = FoxjetDatabase::Extract (p->GetConnect (), _T ("DSN="), _T (";"));

		if (!strDSN.CompareNoCase (lpszDSN) && p->m_dwCreatedThreadID == dwThreadID)
			return i;
	}

	return -1;
}

static void RemoveDB (COdbcDatabase * p)
{
	LOCK (::csOdbcDatabase);

	int nIndex = FindDB (p);

	if (nIndex != -1)
		::vDatabases.RemoveAt (nIndex);
}

static CString gstrDSN;

void COdbcDatabase::SetAppDsn (const CString & strDSN)
{
	::gstrDSN = strDSN;
}

CString COdbcDatabase::GetAppDsn ()
{
	return ::gstrDSN;
}

COdbcDatabase * COdbcDatabase::GetDB (DWORD dwThreadID, LPCTSTR lpszDSN)
{
	LOCK (::csOdbcDatabase);
	CString strDSN = lpszDSN;

	if (!strDSN.GetLength ())
		strDSN = ::gstrDSN;

	COdbcDatabase * pResult = NULL;
	int nIndex = FindDB (dwThreadID, strDSN);

	if (nIndex != -1)
		pResult = ::vDatabases [nIndex];
	else {
		#ifdef _DEBUG
		CString str;
		str.Format (_T ("COdbcDatabase::GetDB (%d, %s): nIndex: %d"), dwThreadID, strDSN, nIndex); TRACEF (str);
		str.Format (_T ("vDatabases.GetSize (): %d"), ::vDatabases.GetSize ()); TRACEF (str);
		int nBreak = 0;
		#endif
	}

	return pResult;
}

int COdbcDatabase::GetInstanceCount ()
{
	LOCK (::csOdbcDatabase);

	return ::vDatabases.GetSize ();
}

void COdbcDatabase::TraceInstances ()
{
	#ifdef _DEBUG
	LOCK (::csOdbcDatabase);

	for (int i = 0; i < ::vDatabases.GetSize (); i++) {
		if (COdbcDatabase * p = ::vDatabases [i]) {
			CString str;

			str.Format (_T ("[%d] COdbcDatabase::m_dwOwnerThreadID: 0x%p [%d] (0x%p [%d])"), 
				i, 
				p->m_dwCreatedThreadID, p->m_dwCreatedThreadID, 
				p->m_dwOwnerThreadID, p->m_dwOwnerThreadID);
			CDebug::Trace (str, true, p->m_strFile, p->m_lLine);
		}
	}
	#endif
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

int COdbcDatabase::m_nInstances = 0;

COdbcDatabase::COdbcDatabase(LPCTSTR lpszFile, ULONG lLine)
:	m_nActive (0),
	m_dwOwnerThreadID (::GetCurrentThreadId ()),
	m_dwCreatedThreadID (::GetCurrentThreadId ()),
	m_strFile (lpszFile),
	m_lLine (lLine),
	m_bTransaction (false),
	m_cs (_T ("FoxjetDatabase::COdbcDatabase:") + ToString (++m_nInstances))
{
	ASSERT (FindDB (this) == -1);
	::vDatabases.Add (this);

/*
	static int nCalls = 0;
	CString str;
	str.Format ("COdbcDatabase::COdbcDatabase() [0x%p] [%d]", this, ++nCalls);
	MsgBox (str);
*/
}

COdbcDatabase::~COdbcDatabase()
{
	RemoveDB (this);

	if (m_nActive > 0) {
		TRACEF (_T ("******** ACTIVE: ~COdbcDatabase: ") + GetConnect ());
		ASSERT (0);
	}

/*
	static int nCalls = 0;
	CString str;
	str.Format ("COdbcDatabase::~COdbcDatabase() [0x%p] [%d]", this, ++nCalls);
	MsgBox  (str);
*/
	FreeTables ();

	for (POSITION pos = m_listRecordsets.GetHeadPosition(); pos != NULL; ) {
		CObject * p = (CObject *)m_listRecordsets.GetNext (pos);

		if (p && p->IsKindOf (RUNTIME_CLASS (COdbcRecordset))) {
			COdbcRecordset * pRst = (COdbcRecordset *)p;

			if (pRst->IsOpen ())
				pRst->Close ();

			delete pRst;
		}
	}

	m_listRecordsets.RemoveAll ();
}

BOOL COdbcDatabase::Connect(DWORD dwOptions, int)
{
	HWND hWndTop = NULL;
	HWND hWnd = NULL;
	
	if (!(dwOptions & CDatabase::noOdbcDialog)) {
		hWnd = CWnd::GetSafeOwner_(NULL, &hWndTop);
	
		if (hWnd == NULL)
			hWnd = ::GetDesktopWindow();
	}

	TCHAR szConnectOutput[MAX_CONNECT_LEN];
	TCHAR *pszConnectInput = const_cast<LPTSTR>(static_cast<LPCTSTR>(m_strConnect));
	RETCODE nRetCode;
	SWORD nResult;
	UWORD wConnectOption = SQL_DRIVER_COMPLETE;

	if (dwOptions & noOdbcDialog)
		wConnectOption = SQL_DRIVER_NOPROMPT;
	else if (dwOptions & forceOdbcDialog)
		wConnectOption = SQL_DRIVER_PROMPT;
	AFX_SQL_SYNC(::SQLDriverConnect(m_hdbc, hWnd, reinterpret_cast<SQLTCHAR *>(pszConnectInput),
		SQL_NTS, reinterpret_cast<SQLTCHAR *>(szConnectOutput), _countof(szConnectOutput),
		&nResult, wConnectOption));
	if (hWndTop != NULL)
		::EnableWindow(hWndTop, TRUE);

	// If user hit 'Cancel'
	if (nRetCode == SQL_NO_DATA_FOUND)
	{
		Free();
		return FALSE;
	}

	if (!Check(nRetCode))
	{
#ifdef _DEBUG
		if (hWnd == NULL)
			TRACE(traceDatabase, 0, _T("Error: No default window (AfxGetApp()->m_pMainWnd) for SQLDriverConnect.\n"));
#endif
		ThrowDBException(nRetCode);
	}

	static const CString str_afxODBCTrail = _T ("ODBC;");
	// Connect strings must have "ODBC;"
	m_strConnect = str_afxODBCTrail;
	// Save connect string returned from ODBC
	m_strConnect += szConnectOutput;

	return TRUE;
}

BOOL COdbcDatabase::OpenEx(LPCTSTR lpszConnectString, DWORD dwOptions, int)
{
	ENSURE_VALID(this);
	ENSURE_ARG(lpszConnectString == NULL || AfxIsValidString(lpszConnectString));
	ENSURE_ARG(!(dwOptions & noOdbcDialog && dwOptions & forceOdbcDialog));

	// Exclusive access not supported.
	ASSERT(!(dwOptions & openExclusive));

	m_bUpdatable = !(dwOptions & openReadOnly);

	TRY
	{
		m_strConnect = lpszConnectString;

		// Allocate the HDBC and make connection
		AllocConnect(dwOptions);
		if(!Connect(dwOptions, 0))
			return FALSE;

		// Verify support for required functionality and cache info
		VerifyConnect();
		GetConnectInfo();
	}
	CATCH_ALL(e)
	{
		Free();
		THROW_LAST();
	}
	END_CATCH_ALL

	return TRUE;
}

BOOL COdbcDatabase::OpenEx (LPCTSTR lpszConnectString, DWORD dwOptions)
{
	CString str = lpszConnectString;

	#ifdef _DEBUG
	{ 
		CString strDebug; 
		CString strDSN = Extract (lpszConnectString, _T ("DSN="), _T (";"));

		if (!strDSN.GetLength ())
			strDSN = Extract (lpszConnectString, _T ("DSN="));

		strDebug.Format (_T ("OpenEx: %s [this: 0x%p, threadID: 0x%p]"), str, this, ::GetCurrentThreadId ()); 
		TRACEF (strDebug); 
		::OutputDebugString (strDebug + _T ("\n")); 
		TRACEF (FoxjetDatabase::ODBC::GetProfileString (strDSN, _T ("DBQ")));
	}
	#endif

	if (dwOptions & CDatabase::openExclusive)	TRACER (_T ("CDatabase::openExclusive"));
	if (dwOptions & CDatabase::openReadOnly)	TRACER (_T ("CDatabase::openReadOnly"));
	if (dwOptions & CDatabase::useCursorLib)	TRACER (_T ("CDatabase::useCursorLib"));
	if (dwOptions & CDatabase::noOdbcDialog)	TRACER (_T ("CDatabase::noOdbcDialog"));
	if (dwOptions & CDatabase::forceOdbcDialog) TRACER (_T ("CDatabase::forceOdbcDialog"));

	//ASSERT (!(dwOptions & CDatabase::openReadOnly));
	ASSERT (!(dwOptions & CDatabase::useCursorLib));

	BOOL bResult = FALSE;

	bResult = COdbcDatabase::OpenEx (str, dwOptions, 0);

	TRACEF (str + _T (": OpenEx: ") + ToString (bResult));

	if (bResult) {
		RETCODE nRetCode = 0;
		SWORD nResult = 0;
		TCHAR sz [512] = { 0 };

		if (FindDB (this) == -1)
			::vDatabases.Add (this);

		AFX_SQL_SYNC (::SQLGetInfo (m_hdbc, SQL_DRIVER_NAME, sz, sizeof (sz [0]) * ARRAYSIZE (sz), &nResult));
		m_strDriver = sz;
		TRACEF (m_strDriver);
		GetTables ();
	}

	return bResult;
}

void COdbcDatabase::Close ()
{
	FreeTables ();	

	try {
		if (IsOpen ()) {
			if (IsSqlServer ()) {
				CString strDSN = Extract (GetConnect (), _T ("DSN="), _T (";"));
				CString strUID = Extract (GetConnect (), _T ("UID="), _T (";"));
				CString strPWD = Extract (GetConnect (), _T ("PWD="), _T (";"));
				CString strKey = _T ("Software\\Foxjet\\MarksmanELITE\\ODBC\\") + strDSN;

				FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("UID"), strUID); 
				FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER, strKey, _T ("PWD"), strPWD); 
			}

			TRACEF ("Close: " + GetConnect ());
			CDatabase::Close ();
		}
	}
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CException * e)			{ HANDLEEXCEPTION (e); }

	RemoveDB (this);
}

void COdbcDatabase::FreeTables ()
{
	FreeTables (m_vTables);
}

void COdbcDatabase::FreeTables (CPtrArray & vTables)
{
	for (int i = 0; i < vTables.GetSize (); i++) {
		COdbcTable * p = (COdbcTable *)vTables [i];
		delete p;
	}

	vTables.RemoveAll ();
}

void COdbcDatabase::CopyTables (CPtrArray & v)
{
	for (int i = 0; i < GetTableCount (); i++) 
		v.Add (new COdbcTable (* GetTable (i)));
}

bool COdbcDatabase::HasTable (const CString & str) const
{
	for (int i = 0; i < m_vTables.GetSize (); i++) 
		if (COdbcTable * p = (COdbcTable *)m_vTables [i])
			if (!str.CompareNoCase (p->GetName ()))
				return true;

	return false;
}


/* TODO: rem
void COdbcDatabase::GetTables (const CString & strType, CPtrArray & v)
{
	COdbcTables tables (this);

	tables.m_strTypeParam = strType;

	if (tables.m_strTypeParam.IsEmpty ()) {
		tables.m_strTypeParam =		_T ("'TABLE', 'VIEW'");
		tables.m_strTypeParam +=	_T (", 'SYSTEM TABLE'");
		tables.m_strTypeParam +=	_T (", 'GLOBAL TEMPORARY'");
		tables.m_strTypeParam +=	_T (", 'LOCAL TEMPORARY'");
		tables.m_strTypeParam +=	_T (", 'ALIAS'");
		tables.m_strTypeParam +=	_T (", 'SYNONYM'");
	}

	TRACEF (tables.m_strTypeParam);

	if (tables.Open ()) {
		while (!tables.IsEOF ()) {
			COdbcTable * p = new COdbcTable ( * this);
		
			p->m_strCatalog = tables.m_strOwner;
			p->m_strSchema	= tables.m_strQualifier;
			p->m_strName	= tables.m_strName;
			p->m_strType	= tables.m_strType;
			p->m_strRemark	= tables.m_strRemarks;
			
			//TRACEF (p->m_strName + ": " + p->m_strType);

			v.Add (p);
			tables.MoveNext ();
		}

		tables.Close ();
	}
}
*/

void COdbcDatabase::GetTables() 
{
	FreeTables ();

	if (IsSqlServer ()) {
		CString str, strDSN = GetSqlServerDatabaseName ();

		TRACEF (GetConnect ());

		try 
		{
			LPCTSTR lpsz [] =
			{
				_T ("SELECT * FROM [%s].[sys].[tables]"),
				_T ("SELECT * FROM [%s].[sys].[views]"),
			};

			for (int i = 0; i < ARRAYSIZE (lpsz); i++) {
				COdbcRecordset rst (* this);

				str.Format (lpsz [i], strDSN);
				TRACEF (str);

				rst.Open (str);

				while (!rst.IsEOF ()) {
					COdbcTable * p = new COdbcTable ( * this);
				
					//p->m_strCatalog = tables.m_strOwner;
					//p->m_strSchema = tables.m_strQualifier;
					p->m_strName = (CString)rst.GetFieldValue (_T ("name"));
					p->m_strType = (CString)rst.GetFieldValue (_T ("type_desc"));
					//p->m_strRemark = tables.m_strRemarks;
					
					p->m_strType.Replace (_T ("USER_TABLE"), _T ("TABLE"));

					m_vTables.Add (p);

					rst.MoveNext ();
				}
			}
		}
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
		catch (CDBException * e)		
		{ 
			if (!strDSN.GetLength ()) {
				str.Format (LoadString (IDS_NOODBCDESC), Extract (GetConnect (), _T ("DSN="), _T (";")));
				MsgBox (str);
				::abort ();
			}

			HANDLEEXCEPTION (e); 
		}

		return;
	}

	COdbcTables tables (this);

	#ifdef _DEBUG
	CStringArray vType;
	#endif

	/*
	tables.m_strTypeParam =		_T ("'TABLE', 'VIEW'");
	tables.m_strTypeParam +=	_T (", 'SYSTEM TABLE'");
	tables.m_strTypeParam +=	_T (", 'GLOBAL TEMPORARY'");
	tables.m_strTypeParam +=	_T (", 'LOCAL TEMPORARY'");
	tables.m_strTypeParam +=	_T (", 'ALIAS'");
	tables.m_strTypeParam +=	_T (", 'SYNONYM'");
	*/

	TRACEF (tables.m_strTypeParam);

	if (tables.Open ()) {
		while (!tables.IsEOF ()) {
			COdbcTable * p = new COdbcTable ( * this);
		
			p->m_strCatalog = tables.m_strOwner;
			p->m_strSchema = tables.m_strQualifier;
			p->m_strName = tables.m_strName;
			p->m_strType = tables.m_strType;
			p->m_strRemark = tables.m_strRemarks;
			
			//TRACEF (p->m_strName + ": " + p->m_strType);

			#ifdef _DEBUG
			if (::Find (vType, p->m_strType) == -1)
				vType.Add (p->m_strType);
			#endif 

			m_vTables.Add (p);
			tables.MoveNext ();
		}

		tables.Close ();
	}

	#ifdef _DEBUG
	{
		TRACEF (GetDriver ());

		for (int i = 0; i < vType.GetSize (); i++) {
			TRACEF (vType [i]);
		}
	}
	#endif
}

const COdbcTable * COdbcDatabase::GetTable(int nIndex) const
{
	const COdbcTable * pResult = NULL;

	if (nIndex >= 0 && nIndex < m_vTables.GetSize ()) 
		pResult = (COdbcTable *)m_vTables [nIndex];

	return pResult;
}

int COdbcDatabase::GetTableCount() const
{
	return m_vTables.GetSize ();
}


CString COdbcDatabase::GetDriver () const
{
	return m_strDriver;
}

void COdbcDatabase::CloseAllRecordsets()
{ 
	// transactions can't occur with access if any recordsets
	// are open (see technical note TN068)
	try {
		while (!m_listRecordsets.IsEmpty())
		{
			CRecordset* pSet = (CRecordset*)m_listRecordsets.GetHead();
			TRACEF (_T ("Closing: ") + pSet->GetDefaultSQL ());
			pSet->Close();  // will implicitly remove from list
			pSet->m_pDatabase = NULL;
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	//FreeTables (); // this may have been needed for oracle (BEGIN_TRANS, etc) ?  
	// if needed again, will have to call GetTables somewhere else
}

static const LPCTSTR lpszODBC = _T ("SOFTWARE\\ODBC\\ODBC.INI");
struct
{
	HKEY m_hkey;
	UINT m_nType;
}
static const keys [] = 
{
	//{ HKEY_CURRENT_USER,	IDS_USER,	},
	{ HKEY_LOCAL_MACHINE,	IDS_SYSTEM,	},
};

CString COdbcDatabase::GetFilePath (const CString & strDSN)
{
	CString str;

	for (int nKey = 0; nKey < ARRAYSIZE (::keys); nKey++) {
		str = FoxjetDatabase::GetProfileString (::keys [nKey].m_hkey, 
			::lpszODBC + CString (_T ("\\")) + strDSN, _T ("DBQ"), _T (""));

		if (str.GetLength ())
			break;
	}

	return str;
}

void COdbcDatabase::GetDsnEntries (CStringArray & v)
{
	for (int nKey = 0; nKey < ARRAYSIZE (::keys); nKey++) {
		const CString strType = LoadString (::keys [nKey].m_nType);
		DWORD dwSubKeyLength = _MAX_FNAME;
		DWORD dwSubKeyIndex = 0;
		TCHAR szSubKey [_MAX_FNAME] = { 0 };
		HKEY hKey = NULL;

		LONG lRegOpenKey = ::RegOpenKey (::keys [nKey].m_hkey, ::lpszODBC, &hKey);

		if (lRegOpenKey == ERROR_SUCCESS) {
			LONG lRegEnumKeyEx = 0;

			while (lRegEnumKeyEx != ERROR_NO_MORE_ITEMS) { 
				lRegEnumKeyEx = ::RegEnumKeyEx (hKey, dwSubKeyIndex, szSubKey, &dwSubKeyLength, 
					NULL, NULL, NULL, NULL);

				if (lRegEnumKeyEx == ERROR_SUCCESS) {
					const CString strSection = (::lpszODBC + CString (_T ("\\")) + szSubKey);
					CString strDriver = FoxjetDatabase::GetProfileString (::keys [nKey].m_hkey, 
						strSection, _T ("Driver"), _T (""));

					dwSubKeyIndex++;
					dwSubKeyLength = _MAX_FNAME;

					if (strDriver.GetLength ())
						InsertAscending (v, szSubKey);
				}
			}

			::RegCloseKey (hKey);
		}
	}
}

void COdbcDatabase::ClearStatus ()
{
	if (m_lpFct)
		(* m_lpFct) (_T ("[purge]"), false);
}
