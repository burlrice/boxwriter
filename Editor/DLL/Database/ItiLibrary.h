// ItiLibrary.h: interface for the ItiLibrary class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ITILIBRARY_H__FE320562_8BCB_11D4_915E_00104BEF6341__INCLUDED_)
#define AFX_ITILIBRARY_H__FE320562_8BCB_11D4_915E_00104BEF6341__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxcmn.h>
#include "DbApi.h"

namespace ItiLibrary  
{
	DB_API void SaveCustomDateTimeFmts(const CString & strRegSection, const CString & strSection, const CStringArray & v);
	DB_API int LoadCustomDateTimeFmts(const CString & strRegSection, const CString & strSection, CStringArray  & v);
	DB_API void DefaultCustomDateTimeFmts(const CString & strRegSection);
};

#endif // !defined(AFX_ITILIBRARY_H__FE320562_8BCB_11D4_915E_00104BEF6341__INCLUDED_)
