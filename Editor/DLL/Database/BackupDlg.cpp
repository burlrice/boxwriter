// BackupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "BackupDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBackupDlg dialog


CBackupDlg::CBackupDlg(CWnd* pParent /*=NULL*/)
:	CDialog ()//(CBackupDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBackupDlg)
	m_strTitle = _T("");
	m_strPath = _T("");
	//}}AFX_DATA_INIT
}


void CBackupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBackupDlg)
	DDX_Text(pDX, TXT_FILENAME, m_strTitle);
	DDX_Text(pDX, TXT_FILEPATH, m_strPath);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBackupDlg, CDialog)
	//{{AFX_MSG_MAP(CBackupDlg)
	ON_BN_CLICKED(BTN_BROWSE, OnBrowse)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBackupDlg message handlers

void CBackupDlg::OnBrowse() 
{
	CString strTitle, strPath;

	GetDlgItemText (TXT_FILEPATH, strPath);
	GetDlgItemText (TXT_FILENAME, strTitle);
	strPath += '\\' + strTitle;

	CFileDialog dlg (TRUE, _T ("*.mdb"), strPath, 
		OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY, 
		_T ("Microsoft Access databases (*.mdb)|*.mdb|All files (*.*)|*.*||"), this);

	if (dlg.DoModal () == IDOK) {
		strPath = dlg.GetPathName ();
		int nIndex = strPath.ReverseFind ('\\');

		if (nIndex != -1) {
			CString str = strPath.Left (nIndex);
			SetDlgItemText (TXT_FILEPATH, str);
		}
	}
}
