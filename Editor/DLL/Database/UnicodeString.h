#pragma once

#include "NtReg.h"
#include <afx.h>

class CUnicodeString : public UNICODE_STRING
{
public:
	CUnicodeString (WCHAR * pwsz = NULL, ULONG lLen = 0);
	CUnicodeString (const CString & str);
	CUnicodeString (const CUnicodeString & rhs);
	virtual ~CUnicodeString ();

	CUnicodeString & operator = (const CUnicodeString & rhs);
	operator CString () const;

	CString ToString () const;
};
