#include "stdafx.h"
#include "Database.h"
#include "Debug.h"
#include "Compare.h"

using namespace FoxjetDatabase;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

////////////////////////////////////////////////////////////////////////////
//
DB_API  bool FoxjetDatabase::operator == (const COrientation & lhs, const COrientation & rhs)
{
	return 
		lhs.m_nPanel == rhs.m_nPanel &&
		lhs.m_orient == rhs.m_orient;
}

DB_API  bool FoxjetDatabase::operator != (const COrientation & lhs, const COrientation & rhs)
{
	return !(lhs == rhs);
}

////////////////////////////////////////////////////////////////////////////
//
COrientation::COrientation (UINT nPanel, ORIENTATION orient)
:	m_nPanel (nPanel), 
	m_orient (orient)
{
}

COrientation::COrientation (const COrientation & rhs)
:	m_nPanel (rhs.m_nPanel), 
	m_orient (rhs.m_orient)
{
}

COrientation::COrientation (const PANELSTRUCT & rhs)
:	m_nPanel (rhs.m_nNumber), 
	m_orient (rhs.m_orientation)
{
}

COrientation & COrientation::operator = (const COrientation & rhs)
{
	if (this != &rhs) {
		m_nPanel = rhs.m_nPanel;
		m_orient = rhs.m_orient;
	}

	return * this;
}

COrientation::~COrientation ()
{
}

