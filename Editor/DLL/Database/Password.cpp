#include "stdafx.h"
#include "Registry.h"
#include "Debug.h"
#include "Parse.h"
#include "AnsiString.h"
#include "zlib.h"
#include "TemplExt.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Encrypted::InkCodes;


/*
std::vector <int> CEnablePassword::m_v;

const ENABLE_PASSWORD  CEnablePassword::m_range		= ENABLE_PASSWORD ();
const DISABLE_PASSWORD CDisablePassword::m_range	= DISABLE_PASSWORD ();

#define IMPLEMENT_PASSWORD_MEMBER(s,classname) \
	bool FoxjetDatabase::Encrypted::InkCodes::##classname::Set##s (unsigned __int32 n) \
	{ \
		static const unsigned __int32 lMax = (unsigned __int32)pow (2.0, (double)m_range.m_n##s) - 1;  \
		 \
		if (n >= 0 && n <= lMax) { \
			m_data.m_n##s = n; \
			return true; \
		} \
		 \
		return false; \
	} \
	\
	unsigned __int32 FoxjetDatabase::Encrypted::InkCodes::##classname::Get##s () const { return m_data.m_n##s; }


IMPLEMENT_PASSWORD_MEMBER (Factor1, CEnablePassword);
IMPLEMENT_PASSWORD_MEMBER (Factor2, CEnablePassword);

IMPLEMENT_PASSWORD_MEMBER (Factor, CDisablePassword);
IMPLEMENT_PASSWORD_MEMBER (ExpPeriod, CDisablePassword);

FoxjetDatabase::Encrypted::InkCodes::tagENABLE_PASSWORD::tagENABLE_PASSWORD ()
{ 	
	m_nFactor1 = 0;
	m_nFactor2 = 0;

	if (FoxjetDatabase::Encrypted::RSA::IsEnabled ()) {
		m_nFactor1 = 15;
		m_nFactor2 = 15;
	}
}

FoxjetDatabase::Encrypted::InkCodes::tagDISABLE_PASSWORD::tagDISABLE_PASSWORD ()
{ 	
	m_nExpPeriod	= 0;
	m_nFactor		= 0;

	if (FoxjetDatabase::Encrypted::RSA::IsEnabled ()) {
		m_nExpPeriod	= 9;
		m_nFactor		= 21;
	}
}

CEnablePassword::CEnablePassword ()
{
	Init (m_v, m_range.m_nFactor2);
}

void CEnablePassword::Init (std::vector <int> & v, int n)
{
	if (Encrypted::RSA::IsEnabled ()) {
		if (v.size () == 0) {
			const ULONG lMaxSqrt = sqrt (pow (2.0, n));
			const ULONG lExponentMax = pow (2.0, n);

			DECLARETRACECOUNT (20);
			MARKTRACECOUNT ();

			v.push_back (2);

			//for (int lTest = 2; lTest < lExponentMax; lTest++) {
			for (int lTest = 3; lTest < lExponentMax; lTest += 2) {
				ULONG n;

				//for (n = 2; n <= lTest - 1; n++) {
				for (n = 3; n <= lTest - 1; n += 2) {
					if ((lTest % n) == 0)
						break;
				}

				if (n == lTest) 
					v.push_back (lTest);
			}

			MARKTRACECOUNT ();
			TRACEF (ToString ((int)v.size ()));
			TRACECOUNTARRAY (); 
			int nBreak = 0;
		}
	}
}

CString CEnablePassword::Encode () const
{
	if (!FoxjetDatabase::Encrypted::RSA::IsEnabled ())
		return _T ("");

	CString strFactor1 = ToBinString (m_data.m_nFactor1);
	CString strFactor2 = ToBinString (m_data.m_nFactor2);

	strFactor1.Replace (_T (" "), _T (""));
	strFactor2.Replace (_T (" "), _T (""));

	CString str, strBin = strFactor1.Right (m_range.m_nFactor1) + strFactor2.Right (m_range.m_nFactor2);

	strBin.Replace (_T (" "), _T (""));
	strBin = Scramble (strBin.Right (m_range.GetSize ()));

	while (strBin.GetLength () > 0) {
		CString s = strBin.Left (5);
				
		strBin.Delete (0, 5);
		int nIndex = _tcstoul (s, NULL, 2);
		str += base32 [nIndex];
	}

	while (str.GetLength () < 6)
		str = '0' + str;

	return str;
}

bool CEnablePassword::Decode (const CString & strIn)
{
	if (!FoxjetDatabase::Encrypted::RSA::IsEnabled ())
		return false;

	CString str = FormatString (strIn), strBin;

	for (int i = 0; i < str.GetLength (); i++) {
		char c = (char)str [i];
		unsigned __int8 nIndex = (unsigned __int8)Find (base32, c);

		ASSERT (nIndex != -1);
		CString s = ::ToBinString (nIndex);
		strBin += s.Right (5);
	}

	if (strBin.GetLength () == m_range.GetSize ()) {
		unsigned __int64 lMax = (unsigned __int64)(pow (2.0, (double)m_range.m_nFactor1));
		CString str = Unscramble (strBin);
		CString strFactor1 = str.Left (m_range.m_nFactor1);
		CString strFactor2 = str.Right (m_range.m_nFactor2);
		unsigned __int64 lFactor1 = _tcstoui64 (strFactor1, NULL, 2);
		unsigned __int64 lFactor2 = _tcstoui64 (strFactor2, NULL, 2);

		std::vector <std::wstring> vMAC = GetMacAddresses ();

		for (int i = 0; i < vMAC.size (); i++) {
			CString str = vMAC [i].c_str ();

			TRACEF (str);
			str.Replace (_T (":"), _T (""));
			unsigned __int64 lMAC = _tcstoui64 (str, NULL, 16);

			if ((lMAC % lMax) == lFactor1) {
				SetFactor1 (lFactor1);
				SetFactor2 (lFactor2);
				return true;
			}
		}
	}

	return false;
}

std::vector <std::wstring> CEnablePassword::Generate (const CString & strMacAddress, int nPasswords)
{
	std::vector <CEnablePassword> v = CEnablePassword::Generate (strMacAddress, nPasswords, 0);
	std::vector <std::wstring> vResult;

	for (int i = 0; i < v.size (); i++)
		vResult.push_back (v [i]);

	return vResult;
}

std::vector <CEnablePassword> CEnablePassword::Generate (const CString & strMacAddress, int nPasswords, int)
{
	std::vector <CEnablePassword> v;
	CString strMAC (strMacAddress);

	if (nPasswords < 0)
		return v;

	Init (m_v, CEnablePassword::m_range.m_nFactor2);
	srand (time (NULL));

	strMAC.Replace (_T (":"), _T (""));

	if (m_v.size ()) {
		unsigned __int64 lMAC = _tcstoui64 (strMAC, NULL, 16);
		unsigned __int32 nFactor1 = (unsigned __int32)(lMAC % (unsigned __int64)(pow (2.0, (double)CEnablePassword::m_range.m_nFactor1)));
		CString strFactor1 = ToBinString (nFactor1);

		strFactor1.Replace (_T (" "), _T (""));
		strFactor1 = strFactor1.Right (CEnablePassword::m_range.m_nFactor1);

		#ifdef _DEBUG
		static const ULONG lMax = pow (2.0, CEnablePassword::m_range.GetSize ()) - 1;

		ASSERT ((unsigned __int64)nFactor1 < lMax);
		#endif //_DEBUG

		int nStep = m_v.size () / (nPasswords + 1);

		for (int i = m_v.size () - 1; i >= 0 && nPasswords > 0; i -= nStep, nPasswords--) {
			CEnablePassword pass;
			int nIndex = i - (rand () % nStep);

			if (nIndex < 0 || nIndex >= m_v.size ())
				nIndex = i;

			int nPrime = m_v [nIndex];
			unsigned __int32 nFactor2 = nPrime; 
			CString strFactor2 = ToBinString (nFactor2);

			//TRACEF (strFactor1 + _T (" ") + strFactor2);
			TRACEF (strMacAddress + _T (" ") + ToString (lMAC) + _T (", ") + ToString ((int)nFactor1) + _T (" ") + ToString ((int)nFactor2));

			strFactor2.Replace (_T (" "), _T (""));
			strFactor2 = strFactor2.Right (CEnablePassword::m_range.m_nFactor2);

			//CString str = strFactor1 + strFactor2; 
			//VERIFY (pass.SetFactor (_tcstoui64 (str, NULL, 2)));

			VERIFY (pass.SetFactor1 (_tcstoui64 (strFactor1, NULL, 2)));
			VERIFY (pass.SetFactor2 (_tcstoui64 (strFactor2, NULL, 2)));
			TRACEF (_T ("CEnablePassword::Generate: ") + ToString ((__int64)pass.GetFactor1 ()) + _T (" ") + ToString ((__int64)pass.GetFactor2 ()));

			v.push_back (pass);
		}
	}

	return v;
}


//////////////////////////////////////////////////////////////////////////////////////////

CString CDisablePassword::Encode () const
{
	if (!FoxjetDatabase::Encrypted::RSA::IsEnabled ())
		return _T ("");

	CString strExpPeriod	= ToBinString (m_data.m_nExpPeriod);
	CString strFactor		= ToBinString (m_data.m_nFactor);

	strExpPeriod.Replace (_T (" "), _T (""));
	strFactor.Replace (_T (" "), _T (""));

	CString str, strBin = strExpPeriod.Right (m_range.m_nExpPeriod) + strFactor.Right (m_range.m_nFactor);

	strBin.Replace (_T (" "), _T (""));
	strBin = Scramble (strBin.Right (m_range.GetSize ()));

	while (strBin.GetLength () > 0) {
		CString s = strBin.Left (5);
				
		strBin.Delete (0, 5);
		int nIndex = _tcstoul (s, NULL, 2);
		str += base32 [nIndex];
	}

	while (str.GetLength () < 6)
		str = '0' + str;

	return str;
}

bool CDisablePassword::Decode (const CString & strIn, const CEnablePassword & pass, const CTime * ptm)
{
	bool bValid = false;

	if (!FoxjetDatabase::Encrypted::RSA::IsEnabled ())
		return false;

	CString str = FormatString (strIn), strBin;

	for (int i = 0; i < str.GetLength (); i++) {
		char c = (char)str [i];
		unsigned __int8 nIndex = (unsigned __int8)Find (base32, c);

		ASSERT (nIndex != -1);
		CString s = ::ToBinString (nIndex);
		strBin += s.Right (5);
	}

	if (strBin.GetLength () == m_range.GetSize ()) {
		strBin = Unscramble (strBin);
		unsigned __int64 lExpPeriod = _tcstoui64 (strBin.Left (m_range.m_nExpPeriod), NULL, 2);
		unsigned __int64 lFactor = _tcstoui64 (strBin.Right (m_range.m_nFactor), NULL, 2);
		unsigned __int64 lFactor2 = pass.GetFactor2 ();

		//TRACEF (ToString ((__int64)lFactor) + _T (" ") + ToString ((__int64)lFactor2));

		if ((lFactor % lFactor2) == 0) {
			if (!ptm)
				SetExpPeriod ((UINT)lExpPeriod);

			SetFactor ((UINT)lFactor);

			if (ptm) {
				int nDay = 0;
				CTime tm = CInkCode::m_tmBase;
				unsigned __int32 n = GetExpPeriod ();

				//TRACEF (tm.Format (_T ("%c")));

				for (int i = 0; i < n; i++) {
					UINT n = GetDaysInMonth (tm.GetMonth (), tm.GetYear ());
					
					nDay += n;
					tm += CTimeSpan (n, 0, 0, 0);
				}

				//TRACEF (ptm->Format (_T ("%c")));
				bValid = (ptm->GetMonth () == tm.GetMonth ()) && (ptm->GetYear () == tm.GetYear ());

				if (bValid) 
					SetExpPeriod ((UINT)n);
			}
		}
	}

	return bValid;
}


std::vector <std::wstring> CDisablePassword::Generate (const CEnablePassword & master, int nPasswords)
{
	std::vector <CDisablePassword> v = CDisablePassword::Generate (master, nPasswords, 0);
	std::vector <std::wstring> vResult;

	for (int i = 0; i < v.size (); i++)
		vResult.push_back (v [i]);

	return vResult;
}

std::vector <CDisablePassword> CDisablePassword::Generate (const CEnablePassword & master, int nPasswords, int)
{
	std::vector <CDisablePassword> v;
	CString strBin = ToBinString (master.GetFactor1 ()) + ToBinString (master.GetFactor2 ());

	strBin.Replace (_T (" "), _T (""));
	unsigned __int64 lFactor2 = _tcstoui64 (strBin.Right (CEnablePassword::m_range.m_nFactor1), NULL, 2);
	static const unsigned __int64 lMax = (unsigned __int64)pow (2.0, (double)m_range.m_nFactor);

	srand (time (NULL));

	for (int i = master.m_v.size () - 1; i >= 0; i--) {
		unsigned __int64 nPrime = master.m_v [i];

		if ((lFactor2 % nPrime) == 0) {
			while (v.size () < nPasswords) {
				CDisablePassword pass;
				unsigned __int64 lFactor = (unsigned __int64 )floor (lMax / (long double)nPrime);

				ASSERT ((lFactor * nPrime) <= lMax);
				pass.SetExpPeriod (0);
				pass.SetFactor (nPrime * (rand () % lFactor));
				ASSERT (pass.GetFactor () <= lMax);

				if (Find (v, pass) == -1) {
					TRACEF (_T ("CDisablePassword::Generate: ") + ToString ((__int64)pass.GetFactor ()) + _T (" ") + ToString ((__int64)nPrime));
					v.push_back (pass);
				}
			}
		}
	} 

	return v;
}

bool CDisablePassword::SetExpPeriod (const CTime & tmExp)
{
	unsigned __int32 nMonth = 0;
	CTime tm = CInkCode::m_tmBase;

	//TRACEF (tm.Format (_T ("%c")));

	while (tm.GetYear () < tmExp.GetYear ()) {
		UINT nDays = GetDaysInMonth (tm.GetMonth (), tm.GetYear ());
		tm += CTimeSpan (nDays, 0, 0, 0);
		nMonth++;
	}

	while (tm.GetMonth () < tmExp.GetMonth ()) {
		UINT nDays = GetDaysInMonth (tm.GetMonth (), tm.GetYear ());
		tm += CTimeSpan (nDays, 0, 0, 0);
		nMonth++;
	}

	//TRACEF (tm.Format (_T ("%c")));

	return SetExpPeriod (nMonth);
}

*/