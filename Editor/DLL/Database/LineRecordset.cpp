// LineRecordset.cpp : implementation file
//

#include "stdafx.h"
#include "Database.h"
#include "OdbcRecordset.h"
#include "FieldDefs.h"
#include "Debug.h"
#include "Serialize.h"
#include "compare.h"
#include "Resource.h"
#include "Parse.h"
#include "DbImp.h"

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Lines;

extern HINSTANCE hInstance;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)

FoxjetDatabase::tagLINESTRUCT::tagLINESTRUCT ()
:	m_lPrinterID (NOTFOUND) // sw0867
{
}

FoxjetDatabase::tagLINESTRUCT::tagLINESTRUCT (const tagLINESTRUCT & rhs)
:	m_lID						(rhs.m_lID),
	m_strName					(rhs.m_strName),
	m_strDesc					(rhs.m_strDesc),
	m_tmMaintenance				(rhs.m_tmMaintenance),
	m_strNoRead					(rhs.m_strNoRead),
	m_bResetScanInfo			(rhs.m_bResetScanInfo),
	m_nMaxConsecutiveNoReads	(rhs.m_nMaxConsecutiveNoReads),
	m_nBufferOffset				(rhs.m_nBufferOffset),
	m_nSignificantChars			(rhs.m_nSignificantChars),
	m_strAuxBoxIp				(rhs.m_strAuxBoxIp),
	m_bAuxBoxEnabled			(rhs.m_bAuxBoxEnabled),
	m_strSerialParams1			(rhs.m_strSerialParams1),
	m_strSerialParams2			(rhs.m_strSerialParams2),
	m_strSerialParams3			(rhs.m_strSerialParams3),
	m_nAMS32_A1					(rhs.m_nAMS32_A1),
	m_nAMS32_A2					(rhs.m_nAMS32_A2),
	m_nAMS32_A3					(rhs.m_nAMS32_A3),
	m_nAMS32_A4					(rhs.m_nAMS32_A4),
	m_nAMS32_A5					(rhs.m_nAMS32_A5),
	m_nAMS32_A6					(rhs.m_nAMS32_A6),
	m_nAMS32_A7					(rhs.m_nAMS32_A7),
	m_nAMS256_A1				(rhs.m_nAMS256_A1),
	m_nAMS256_A2				(rhs.m_nAMS256_A2),
	m_nAMS256_A3				(rhs.m_nAMS256_A3),
	m_nAMS256_A4				(rhs.m_nAMS256_A4),
	m_nAMS256_A5				(rhs.m_nAMS256_A5),
	m_nAMS256_A6				(rhs.m_nAMS256_A6),
	m_nAMS256_A7				(rhs.m_nAMS256_A7),
	m_dAMS_Interval				(rhs.m_dAMS_Interval),
	m_lPrinterID				(rhs.m_lPrinterID) // sw0867
{
}

tagLINESTRUCT & FoxjetDatabase::tagLINESTRUCT::operator = (const tagLINESTRUCT & rhs)
{
	if (this != &rhs) {
		m_lID						= rhs.m_lID;
		m_strName					= rhs.m_strName;
		m_strDesc					= rhs.m_strDesc;
		m_tmMaintenance				= rhs.m_tmMaintenance;
		m_strNoRead					= rhs.m_strNoRead;
		m_bResetScanInfo			= rhs.m_bResetScanInfo;
		m_nMaxConsecutiveNoReads	= rhs.m_nMaxConsecutiveNoReads;
		m_nBufferOffset				= rhs.m_nBufferOffset;
		m_nSignificantChars			= rhs.m_nSignificantChars;
		m_strAuxBoxIp				= rhs.m_strAuxBoxIp;
		m_bAuxBoxEnabled			= rhs.m_bAuxBoxEnabled;
		m_strSerialParams1			= rhs.m_strSerialParams1;
		m_strSerialParams2			= rhs.m_strSerialParams2;
		m_strSerialParams3			= rhs.m_strSerialParams3;
		m_nAMS32_A1					= rhs.m_nAMS32_A1;
		m_nAMS32_A2					= rhs.m_nAMS32_A2;
		m_nAMS32_A3					= rhs.m_nAMS32_A3;
		m_nAMS32_A4					= rhs.m_nAMS32_A4;
		m_nAMS32_A5					= rhs.m_nAMS32_A5;
		m_nAMS32_A6					= rhs.m_nAMS32_A6;
		m_nAMS32_A7					= rhs.m_nAMS32_A7;
		m_nAMS256_A1				= rhs.m_nAMS256_A1;
		m_nAMS256_A2				= rhs.m_nAMS256_A2;
		m_nAMS256_A3				= rhs.m_nAMS256_A3;
		m_nAMS256_A4				= rhs.m_nAMS256_A4;
		m_nAMS256_A5				= rhs.m_nAMS256_A5;
		m_nAMS256_A6				= rhs.m_nAMS256_A6;
		m_nAMS256_A7				= rhs.m_nAMS256_A7;
		m_dAMS_Interval				= rhs.m_dAMS_Interval;
		m_lPrinterID				= rhs.m_lPrinterID; // sw0867
	}

	return * this;
}

FoxjetDatabase::tagLINESTRUCT::~tagLINESTRUCT ()
{
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::LINESTRUCT & lhs, const FoxjetDatabase::LINESTRUCT & rhs)
{
	bool bResult = true;

	bResult &= (lhs.m_lID						== rhs.m_lID); 
	bResult &= (lhs.m_strName					== rhs.m_strName);
	bResult &= (lhs.m_strDesc					== rhs.m_strDesc);
	bResult &= (lhs.m_tmMaintenance				== rhs.m_tmMaintenance) ? true : false;
	bResult &= (lhs.m_strNoRead					== rhs.m_strNoRead);
	bResult &= (lhs.m_bResetScanInfo			== rhs.m_bResetScanInfo);
	bResult &= (lhs.m_nMaxConsecutiveNoReads	== rhs.m_nMaxConsecutiveNoReads);
	bResult &= (lhs.m_nBufferOffset				== rhs.m_nBufferOffset);
	bResult &= (lhs.m_nSignificantChars			== rhs.m_nSignificantChars);
	bResult &= (lhs.m_strAuxBoxIp				== rhs.m_strAuxBoxIp);
	bResult &= (lhs.m_bAuxBoxEnabled			== rhs.m_bAuxBoxEnabled);
	bResult &= (lhs.m_strSerialParams1			== rhs.m_strSerialParams1);
	bResult &= (lhs.m_strSerialParams2			== rhs.m_strSerialParams2);
	bResult &= (lhs.m_strSerialParams3			== rhs.m_strSerialParams3);
	bResult &= (lhs.m_nAMS32_A1					== rhs.m_nAMS32_A1);
	bResult &= (lhs.m_nAMS32_A2					== rhs.m_nAMS32_A2);
	bResult &= (lhs.m_nAMS32_A3					== rhs.m_nAMS32_A3);
	bResult &= (lhs.m_nAMS32_A4					== rhs.m_nAMS32_A4);
	bResult &= (lhs.m_nAMS32_A5					== rhs.m_nAMS32_A5);
	bResult &= (lhs.m_nAMS32_A6					== rhs.m_nAMS32_A6);
	bResult &= (lhs.m_nAMS32_A7					== rhs.m_nAMS32_A7);
	bResult &= (lhs.m_nAMS256_A1				== rhs.m_nAMS256_A1);
	bResult &= (lhs.m_nAMS256_A2				== rhs.m_nAMS256_A2);
	bResult &= (lhs.m_nAMS256_A3				== rhs.m_nAMS256_A3);
	bResult &= (lhs.m_nAMS256_A4				== rhs.m_nAMS256_A4);
	bResult &= (lhs.m_nAMS256_A5				== rhs.m_nAMS256_A5);
	bResult &= (lhs.m_nAMS256_A6				== rhs.m_nAMS256_A6);
	bResult &= (lhs.m_nAMS256_A7				== rhs.m_nAMS256_A7);
	bResult &= (lhs.m_dAMS_Interval				== rhs.m_dAMS_Interval);
	bResult &= (lhs.m_lPrinterID				== rhs.m_lPrinterID); // sw0867

	return bResult;
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::LINESTRUCT & lhs, const FoxjetDatabase::LINESTRUCT & rhs)
{
	return !(rhs == lhs);
}

DB_API const FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator >> (const FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::LINESTRUCT & ls)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		ls.m_lID						= (long)rst.GetFieldValue (m_lpszID);
		ls.m_strName					= (CString)rst.GetFieldValue (m_lpszName);
		ls.m_strDesc					= (CString)rst.GetFieldValue (m_lpszDesc);
		ls.m_tmMaintenance				= (COleDateTime)rst.GetFieldValue (m_lpszMaintenance).ToOleDateTime();
		ls.m_strNoRead					= (CString) rst.GetFieldValue (m_lpszNoRead);
		ls.m_bResetScanInfo				= (BOOL) rst.GetFieldValue (m_lpszResetScanInfo);
		ls.m_nMaxConsecutiveNoReads		= (int) rst.GetFieldValue (m_lpszMaxConsecutiveNoReads);
		ls.m_nBufferOffset				= (int) rst.GetFieldValue (m_lpszBufferOffset);
		ls.m_nSignificantChars			= (int) rst.GetFieldValue (m_lpszSignificantChars);
		ls.m_strAuxBoxIp				= (CString) rst.GetFieldValue (m_lpszAuxBoxIp);
		ls.m_bAuxBoxEnabled				= (BOOL) rst.GetFieldValue (m_lpszAuxBoxEnabled);
		ls.m_strSerialParams1			= (CString) rst.GetFieldValue (m_lpszSerialParams1);
		ls.m_strSerialParams2			= (CString) rst.GetFieldValue (m_lpszSerialParams2);
		ls.m_strSerialParams3			= (CString) rst.GetFieldValue (m_lpszSerialParams3);
		ls.m_nAMS32_A1					= (INT) rst.GetFieldValue (m_lpszAMS32_A1);
		ls.m_nAMS32_A2					= (INT) rst.GetFieldValue (m_lpszAMS32_A2);
		ls.m_nAMS32_A3					= (INT) rst.GetFieldValue (m_lpszAMS32_A3);
		ls.m_nAMS32_A4					= (INT) rst.GetFieldValue (m_lpszAMS32_A4);
		ls.m_nAMS32_A5					= (INT) rst.GetFieldValue (m_lpszAMS32_A5);
		ls.m_nAMS32_A6					= (INT) rst.GetFieldValue (m_lpszAMS32_A6);
		ls.m_nAMS32_A7					= (INT) rst.GetFieldValue (m_lpszAMS32_A7);
		ls.m_nAMS256_A1					= (INT) rst.GetFieldValue (m_lpszAMS256_A1);
		ls.m_nAMS256_A2					= (INT) rst.GetFieldValue (m_lpszAMS256_A2);
		ls.m_nAMS256_A3					= (INT) rst.GetFieldValue (m_lpszAMS256_A3);
		ls.m_nAMS256_A4					= (INT) rst.GetFieldValue (m_lpszAMS256_A4);
		ls.m_nAMS256_A5					= (INT) rst.GetFieldValue (m_lpszAMS256_A5);
		ls.m_nAMS256_A6					= (INT) rst.GetFieldValue (m_lpszAMS256_A6);
		ls.m_nAMS256_A7					= (INT) rst.GetFieldValue (m_lpszAMS256_A7);
		ls.m_dAMS_Interval				= (double)rst.GetFieldValue (m_lpszAMS_Interval);

		if (rst.GetFieldIndex (m_lpszPrinterID) == -1) // sw0867
			ls.m_lPrinterID				= 0;
		else
			ls.m_lPrinterID				= (long)rst.GetFieldValue (m_lpszPrinterID);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API FoxjetDatabase::COdbcRecordset & FoxjetDatabase::operator << (FoxjetDatabase::COdbcRecordset & rst, FoxjetDatabase::LINESTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	CStringArray name, value;

	if (s.m_lID == -1) 
		rst.SetEditMode (COdbcRecordset::addnew);

	name.Add (m_lpszName);					value.Add (s.m_strName);
	name.Add (m_lpszDesc);					value.Add (s.m_strDesc);
	name.Add (m_lpszMaintenance);			value.Add (s.m_tmMaintenance.Format (_T ("%H:%M")));
	name.Add (m_lpszNoRead);				value.Add (s.m_strNoRead);
	name.Add (m_lpszResetScanInfo);			value.Add (ToString (s.m_bResetScanInfo));
	name.Add (m_lpszMaxConsecutiveNoReads);	value.Add (ToString (s.m_nMaxConsecutiveNoReads));
	name.Add (m_lpszBufferOffset);			value.Add (ToString (s.m_nBufferOffset));
	name.Add (m_lpszSignificantChars);		value.Add (ToString (s.m_nSignificantChars));
	name.Add (m_lpszAuxBoxIp);				value.Add (s.m_strAuxBoxIp);
	name.Add (m_lpszAuxBoxEnabled);			value.Add (ToString ((int)s.m_bAuxBoxEnabled));
	name.Add (m_lpszSerialParams1);			value.Add (s.m_strSerialParams1);
	name.Add (m_lpszSerialParams2);			value.Add (s.m_strSerialParams2);
	name.Add (m_lpszSerialParams3);			value.Add (s.m_strSerialParams3);
	name.Add (m_lpszAMS32_A1);				value.Add (ToString (s.m_nAMS32_A1));
	name.Add (m_lpszAMS32_A2);				value.Add (ToString (s.m_nAMS32_A2));
	name.Add (m_lpszAMS32_A3);				value.Add (ToString (s.m_nAMS32_A3));
	name.Add (m_lpszAMS32_A4);				value.Add (ToString (s.m_nAMS32_A4));
	name.Add (m_lpszAMS32_A5);				value.Add (ToString (s.m_nAMS32_A5));
	name.Add (m_lpszAMS32_A6);				value.Add (ToString (s.m_nAMS32_A6));
	name.Add (m_lpszAMS32_A7);				value.Add (ToString (s.m_nAMS32_A7));
	name.Add (m_lpszAMS256_A1);				value.Add (ToString (s.m_nAMS256_A1));
	name.Add (m_lpszAMS256_A2);				value.Add (ToString (s.m_nAMS256_A2));
	name.Add (m_lpszAMS256_A3);				value.Add (ToString (s.m_nAMS256_A3));
	name.Add (m_lpszAMS256_A4);				value.Add (ToString (s.m_nAMS256_A4));
	name.Add (m_lpszAMS256_A5);				value.Add (ToString (s.m_nAMS256_A5));
	name.Add (m_lpszAMS256_A6);				value.Add (ToString (s.m_nAMS256_A6));
	name.Add (m_lpszAMS256_A7);				value.Add (ToString (s.m_nAMS256_A7));
	name.Add (m_lpszAMS_Interval);			value.Add (ToString (s.m_dAMS_Interval));

	if (rst.GetFieldIndex (m_lpszPrinterID) != -1) {
		name.Add (m_lpszPrinterID); value.Add (ToString ((long)s.m_lPrinterID));
	}

	CString strSQL = FormatSQL (rst, m_lpszTable, name, value, m_lpszID, s.m_lID);
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

	if (s.m_lID == -1)
		s.m_lID = GetMaxID (rst.GetOdbcDatabase (), m_lpszTable, m_lpszID);

/*

	try {
		if (rst.IsAddNew ())
			rst.SetFieldValue (m_lpszID,	(long)GetNextID (rst.GetOdbcDatabase (), m_lpszTable));

		rst.SetFieldValue (m_lpszName,						ls.m_strName);
		rst.SetFieldValue (m_lpszDesc,						ls.m_strDesc);
		rst.SetFieldValue (m_lpszMaintenance,				ls.m_tmMaintenance);
		rst.SetFieldValue (m_lpszNoRead,					ls.m_strNoRead);
		rst.SetFieldValue (m_lpszResetScanInfo,				ls.m_bResetScanInfo);
		rst.SetFieldValue (m_lpszMaxConsecutiveNoReads,		ls.m_nMaxConsecutiveNoReads);
		rst.SetFieldValue (m_lpszBufferOffset,				ls.m_nBufferOffset);
		rst.SetFieldValue (m_lpszSignificantChars,			ls.m_nSignificantChars);
		rst.SetFieldValue (m_lpszAuxBoxIp,					ls.m_strAuxBoxIp);
		rst.SetFieldValue (m_lpszAuxBoxEnabled,				ls.m_bAuxBoxEnabled);
		rst.SetFieldValue (m_lpszSerialParams1,				ls.m_strSerialParams1);
		rst.SetFieldValue (m_lpszSerialParams2,				ls.m_strSerialParams2);
		rst.SetFieldValue (m_lpszSerialParams3,				ls.m_strSerialParams3);
		rst.SetFieldValue (m_lpszAMS32_A1,					ls.m_nAMS32_A1);
		rst.SetFieldValue (m_lpszAMS32_A2,					ls.m_nAMS32_A2);
		rst.SetFieldValue (m_lpszAMS32_A3,					ls.m_nAMS32_A3);
		rst.SetFieldValue (m_lpszAMS32_A4,					ls.m_nAMS32_A4);
		rst.SetFieldValue (m_lpszAMS32_A5,					ls.m_nAMS32_A5);
		rst.SetFieldValue (m_lpszAMS32_A6,					ls.m_nAMS32_A6);
		rst.SetFieldValue (m_lpszAMS32_A7,					ls.m_nAMS32_A7);
		rst.SetFieldValue (m_lpszAMS256_A1,					ls.m_nAMS256_A1);
		rst.SetFieldValue (m_lpszAMS256_A2,					ls.m_nAMS256_A2);
		rst.SetFieldValue (m_lpszAMS256_A3,					ls.m_nAMS256_A3);
		rst.SetFieldValue (m_lpszAMS256_A4,					ls.m_nAMS256_A4);
		rst.SetFieldValue (m_lpszAMS256_A5,					ls.m_nAMS256_A5);
		rst.SetFieldValue (m_lpszAMS256_A6,					ls.m_nAMS256_A6);
		rst.SetFieldValue (m_lpszAMS256_A7,					ls.m_nAMS256_A7);
		rst.SetFieldValue (m_lpszAMS_Interval,				ls.m_dAMS_Interval);

		if (rst.GetFieldIndex (m_lpszPrinterID) != -1)
			rst.SetFieldValue (m_lpszPrinterID,				(long)ls.m_lPrinterID); // sw0867
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/

	return rst;
}

DB_API UINT FoxjetDatabase::GetLineRecordCount (COdbcDatabase & database)
{
	DBMANAGETHREADSTATE (database);

	UINT nCount = 0;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);
		rst.Open (COdbcRecordset::snapshot, str);
		nCount = rst.CountRecords ();
		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return nCount;
}

DB_API ULONG FoxjetDatabase::GetLineID (COdbcDatabase & database, const CString & strLine, ULONG lPrinterID)
{
	DBMANAGETHREADSTATE (database);

	ULONG lID = NOTFOUND;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (database);
		CString str;

		if (lPrinterID == 0 || lPrinterID == ALL || !database.HasTable (Printers::m_lpszTable)) 
			str.Format (_T ("SELECT [%s] FROM [%s] WHERE [%s]='%s';"), 
				m_lpszID, m_lpszTable, m_lpszName, strLine);
		else
			str.Format (_T ("SELECT [%s] FROM [%s] WHERE [%s]='%s' AND [%s]=%d;"), 
				m_lpszID, m_lpszTable, m_lpszName, strLine, m_lpszPrinterID, lPrinterID);

		rst.Open (str);
		
		if (!rst.IsEOF ()) 
			lID = (long)rst.GetFieldValue (m_lpszID);

		rst.Close ();
	}
 	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return lID;
}

DB_API bool FoxjetDatabase::GetLineRecord (COdbcDatabase & database, ULONG lID, LINESTRUCT & cs)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, lID);
		rst.Open (str);
		
		if (!rst.IsEOF ()) {
			rst >> cs;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::AddLineRecord (COdbcDatabase & database, LINESTRUCT & cs)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	class CPanel : public PANELSTRUCT
	{
	public:
		CPanel (ULONG lLineID, int nNumber, DIRECTION d, const CString & strName) 
		{
			m_lLineID		= lLineID;
			m_nNumber		= nNumber;
			m_direction		= d;
			m_orientation	= ROT_0;
			m_strName		= strName;
		}
		virtual ~CPanel () { }
	};

	try {
		COdbcRecordset rst (&database);

		cs.m_lID = -1;
		rst << cs;
		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s];"), m_lpszTable);
		rst.Open (str, CRecordset::dynaset);
		rst.AddNew ();
		rst << cs;
		rst.Update ();
		rst.MoveLast ();
		cs.m_lID = (long)rst.GetFieldValue (m_lpszID);
		rst.Close ();
		*/


		if (UpdateLineRecord (database, cs)) {
			CArray <BOXSTRUCT, BOXSTRUCT &> vBoxes;
			COdbcRecordset rstBoxes (database);
			CPanel panels [6] = {
				CPanel (cs.m_lID, 1, LTOR, LoadString (IDS_FRONT)),
				CPanel (cs.m_lID, 2, LTOR, LoadString (IDS_RIGHT)),
				CPanel (cs.m_lID, 3, RTOL, LoadString (IDS_BACK)),
				CPanel (cs.m_lID, 4, RTOL, LoadString (IDS_LEFT)),
				CPanel (cs.m_lID, 5, LTOR, LoadString (IDS_TOP)),
				CPanel (cs.m_lID, 6, RTOL, LoadString (IDS_BOTTOM)),
			};
			CString str;

			for (int i = 0; i < 6; i++) {
				bool bAdd = AddPanelRecord (database, panels [i]);
//				ASSERT (bAdd);
			}

			GetBoxRecords (database, ALL, vBoxes);
			/* TODO: rem
			str.Format (_T ("SELECT * FROM [%s];"), BoxToLine::m_lpszTable);
			rstBoxes.Open (str, rstBoxes.dynaset);
			*/

			for (int i = 0; i < vBoxes.GetSize (); i++) {
				BOXLINKSTRUCT link;

				link.m_lBoxID = vBoxes [i].m_lID;
				link.m_lLineID = cs.m_lID;
				
				rstBoxes << link;
				/* TODO: rem
				rstBoxes.AddNew ();
				rstBoxes << link;
				rstBoxes.Update ();
				*/
			}

			// TODO: rem // rstBoxes.Close ();
			bResult = true;
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::UpdateLineRecord (COdbcDatabase & database, LINESTRUCT & cs)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);

		rst << cs;
		bResult = true;

		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, cs.m_lID);
		rst.Open (str, CRecordset::dynaset);

		if (!rst.IsEOF ()) {
			rst.Edit ();
			rst << cs;
			rst.Update ();
			bResult = true;
		}

		rst.Close ();
		*/
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::DeleteLineRecord (COdbcDatabase & database, ULONG lID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		CString strSQL;
		
		// mark the related heads as un-owned

		if (!database.IsSqlServer ()) {
			strSQL.Format (
				_T ("UPDATE ([%s] INNER JOIN [%s] ON [%s].[%s] = [%s].[%s])")
				_T (" INNER JOIN [%s] ON [%s].[%s] = [%s].[%s] SET")
				_T (" [%s].[%s] = 0 WHERE ((([%s].[%s])=%u));"), 
				Heads::m_lpszTable,
				Panels::m_lpszTable,
				Heads::m_lpszTable, Heads::m_lpszPanelID,
				Panels::m_lpszTable, Panels::m_lpszID,
				Lines::m_lpszTable,
				Panels::m_lpszTable, Panels::m_lpszLineID,
				Lines::m_lpszTable, Lines::m_lpszID,
				Heads::m_lpszTable, Heads::m_lpszPanelID,
				Lines::m_lpszTable, Lines::m_lpszID,
				ALL);//lID);
			database.ExecuteSQL (strSQL);
		}

		// delete the related tasks
		strSQL.Format (
			_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
			Tasks::m_lpszTable, Tasks::m_lpszLineID, lID);
		database.ExecuteSQL (strSQL);

		// delete the related panels
		strSQL.Format (
			_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
			Panels::m_lpszTable, Panels::m_lpszLineID, lID);
		database.ExecuteSQL (strSQL);

		// delete the box usage
		strSQL.Format (
			_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
			BoxToLine::m_lpszTable, BoxToLine::m_lpszLineID, lID);
		database.ExecuteSQL (strSQL);

		// delete the line
		strSQL.Format (
			_T ("DELETE * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, lID);
		database.ExecuteSQL (strSQL);

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetLineHeads (COdbcDatabase & database, ULONG lID, CArray <HEADSTRUCT, HEADSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	
	ASSERT (database.IsOpen ());

	v.RemoveAll ();

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (
			_T ("SELECT [%s].* FROM ([%s] INNER JOIN [%s] ON")
			_T (" [%s].[%s] = [%s].[%s]) INNER JOIN [%s] ON")
			_T (" [%s].[%s] = [%s].[%s] WHERE ((([%s].[%s])=%u)) ORDER BY [%s].[%s], [%s].[%s];"), 
			Heads::m_lpszTable,
			Heads::m_lpszTable,
			Panels::m_lpszTable,
			Heads::m_lpszTable, Heads::m_lpszPanelID,
			Panels::m_lpszTable, Panels::m_lpszID,
			Lines::m_lpszTable,
			Panels::m_lpszTable, Panels::m_lpszLineID,
			Lines::m_lpszTable, Lines::m_lpszID,
			Lines::m_lpszTable, Lines::m_lpszID,
			lID,
			Heads::m_lpszTable, Heads::m_lpszPanelID, 
			Heads::m_lpszTable, Heads::m_lpszRelativeHeight);
		rst.Open (str);

		while (!rst.IsEOF ()) {
			HEADSTRUCT hs;
			rst >> hs;
			v.Add (hs);
			rst.MoveNext ();
		}

		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetLineRecords (COdbcDatabase & database, CArray <LINESTRUCT, LINESTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	v.RemoveAll ();
	
	try {
		COdbcRecordset rst (database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] ORDER BY [%s] ASC;"), m_lpszTable, m_lpszName);
		rst.Open (COdbcRecordset::snapshot, str);

		while (!rst.IsEOF ()) {
			LINESTRUCT line;
			rst >> line;
			v.Add (line);
			rst.MoveNext ();
		}

		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetPrinterLines (COdbcDatabase & database, ULONG lPrinterID, CArray <LINESTRUCT, LINESTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	v.RemoveAll ();
	
	try {
		COdbcRecordset rst (database);
		CString str;

		if (/* lPrinterID == 0 || */ lPrinterID == ALL || !database.HasTable (Printers::m_lpszTable))
			str.Format (_T ("SELECT * FROM [%s] ORDER BY [%s] ASC;"), 
				m_lpszTable, m_lpszName);
		else
			str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u ORDER BY [%s] ASC;"), 
				m_lpszTable, m_lpszPrinterID, lPrinterID, m_lpszName);
		
		rst.Open (COdbcRecordset::snapshot, str);

		while (!rst.IsEOF ()) {
			LINESTRUCT line;
			rst >> line;
			v.Add (line);
			rst.MoveNext ();
		}

		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetFirstLineRecord (COdbcDatabase & database, LINESTRUCT & line, ULONG lPrinterID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	try {
		CString str;

		if ((lPrinterID != 0 && lPrinterID != ALL) && database.HasTable (Printers::m_lpszTable)) 
			str.Format (_T ("SELECT TOP 1 * FROM [%s] WHERE [%s]=%d;"), m_lpszTable, m_lpszPrinterID, lPrinterID);
		else
			str.Format (_T ("SELECT TOP 1 * FROM [%s];"), m_lpszTable);
	
		COdbcRecordset rst (database);
		
		rst.Open (str);

		if (!rst.IsEOF ()) {
			rst >> line;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

static ULONG lPrinterID = 0; // sw0867

DB_API ULONG FoxjetDatabase::GetPrinterID (COdbcDatabase & database) // sw0867
{
	DBMANAGETHREADSTATE (database);

	if (::lPrinterID > 0)
		return ::lPrinterID;
	else {
		if (!database.HasTable (Printers::m_lpszTable)) 
			return NOTFOUND;

		CArray <PRINTERSTRUCT, PRINTERSTRUCT &> v;

		GetPrinterRecords (database, v);

		if (v.GetSize () == 0) {
			PRINTERSTRUCT p;
			CArray <LINESTRUCT, LINESTRUCT &> vLines;
	
			/*
			{
				CString str = ::GetCommandLine ();

				str.MakeLower ();

				if (str.Find (_T ("config.exe")) != -1 && str.Find (_T ("/addnew")) != -1)
					return NOTFOUND;
			}
			*/


			AddDefaultPrinter (database);
			::lPrinterID = GetMasterPrinterID (database);

			GetPrinterLines (database, ALL, vLines);

			for (int i = 0; i < vLines.GetSize (); i++) {
				LINESTRUCT line = vLines [i];

				line.m_lPrinterID = ::lPrinterID;
				VERIFY (UpdateLineRecord (database, line));
			}

			return ::lPrinterID;
		}
		else if (v.GetSize () == 1) {
			PRINTERSTRUCT & p = v [0];

			if (!p.m_bMaster)
				p.m_bMaster = true;

			VERIFY (UpdatePrinterRecord (database, p));
		}

		if (::lPrinterID == 0 || ::lPrinterID == -1) {
			CString strAddr = GetIpAddress ();
			CString strName = GetComputerName ();

			/*
			#ifdef _DEBUG
			if (IsGojo ()) {
				strAddr = _T ("10.4.4.39");
				TRACEF (_T ("***** Gojo: ") + strAddr);
			}
			#endif
			*/

			#ifdef _DEBUG
			{
				for (int i = 0; i < v.GetSize (); i++) 
					TRACEF (ToString (v [i].m_lID) + _T (": ") + v [i].m_strName + ": " + v [i].m_strAddress + ": " + ToString (v [i].m_bMaster));
			}
			#endif

			// attempt to match name
			for (int i = 0; i < v.GetSize (); i++) {
				PRINTERSTRUCT & p = v [i];

				if (!strName.CompareNoCase (p.m_strName)) 
					return ::lPrinterID = p.m_lID;
			}

			// attempt to match ip address
			for (int i = 0; i < v.GetSize (); i++) {
				PRINTERSTRUCT & p = v [i];

				if (!strAddr.CompareNoCase (p.m_strAddress))
					return ::lPrinterID = p.m_lID;
			}
		}
	}

	return ::lPrinterID = GetMasterPrinterID (database);
}

DB_API ULONG FoxjetDatabase::GetMasterPrinterID (COdbcDatabase & database) // sw0867
{
	DBMANAGETHREADSTATE (database);
	const CString strMaster = LoadString (IDS_MASTERCONFIG);
	CArray <PRINTERSTRUCT, PRINTERSTRUCT &> v;

	GetPrinterRecords (database, v);

	if (!IsNetworked () && v.GetSize ())
		return v [0].m_lID;

	for (int i = 0; i < v.GetSize (); i++) 
		if (!v [i].m_strName.CompareNoCase (strMaster)) 
			return v [i].m_lID;

	return AddDefaultPrinter (database);

	/*
	if (v.GetSize () == 1) {
		PRINTERSTRUCT & p = v [0];

		if (!p.m_bMaster)
			p.m_bMaster = true;

		VERIFY (UpdatePrinterRecord (database, p));

		return p.m_lID;
	}

	for (int i = 0; i < v.GetSize (); i++) 
		if (v [i].m_bMaster)
			return v [i].m_lID;
	
	if (v.GetSize ()) {
		PRINTERSTRUCT & p = v [0];

		if (!p.m_bMaster)
			p.m_bMaster = true;

		VERIFY (UpdatePrinterRecord (database, p));

		return p.m_lID;
	}

	return GetPrinterID (database); //-1;
	*/
}

DB_API void FoxjetDatabase::SetPrinterID (ULONG lID) // sw0867
{
	TRACEF (_T ("SetPrinterID: ") + ToString (lID));
	::lPrinterID = lID;
}

DB_API bool FoxjetDatabase::GetPrinterHeads (COdbcDatabase & database, ULONG lPrinterID, CArray <HEADSTRUCT, HEADSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	v.RemoveAll ();
	
	try {
		COdbcRecordset rst (database);
		CString str;

		if (/* lPrinterID == 0 || */ lPrinterID == ALL || !database.HasTable (Printers::m_lpszTable)) 
			return /* GetPrinterHeads (database, lPrinterID, v); */ GetHeadRecords (database, v);
		else
			str.Format (
				_T ("SELECT [%s].* FROM ([%s] INNER JOIN [%s] ON [%s].[%s] = [%s].[%s])")
				_T (" INNER JOIN [%s] ON [%s].[%s] = [%s].[%s]")
				_T (" WHERE ((([%s].[%s])=%d)) ORDER BY [%s].[%s] ASC;"),
				Heads::m_lpszTable,
				Heads::m_lpszTable,
				Panels::m_lpszTable,
				Heads::m_lpszTable, Heads::m_lpszPanelID,
				Panels::m_lpszTable, Panels::m_lpszID,
				Lines::m_lpszTable,
				Panels::m_lpszTable, Panels::m_lpszLineID,
				Lines::m_lpszTable, Lines::m_lpszID,
				Lines::m_lpszTable, Lines::m_lpszPrinterID,
				lPrinterID,
				Heads::m_lpszTable, Heads::m_lpszUID);

		rst.Open (COdbcRecordset::snapshot, str);

		while (!rst.IsEOF ()) {
			HEADSTRUCT h;
			rst >> h;
			v.Add (h);
			rst.MoveNext ();
		}

		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

static int Find (CArray <HEADSTRUCT, HEADSTRUCT &> & v, ULONG lHeadID)
{
	for (int i = 0; i < v.GetSize (); i++) {
		ULONG lID = v [i].m_lID;
		
		if (lID == lHeadID) 
			return i;
	}

	return -1;
}

static ULONG GetPHCAddr (CArray <HEADSTRUCT, HEADSTRUCT &> & v, ULONG lHeadID)
{
	int n = Find (v, lHeadID);

	if (n >= 0 && n < v.GetSize ())
		return _tcstoul (v [n].m_strUID, NULL, 16);

	return -1;
}

struct 
{
	ULONG m_lPHC;
	ULONG m_lUSB;
} 
static const synonym [] = 
{
	{ 0x300, 1, },
	{ 0x310, 2, },
	{ 0x320, 3, },
	{ 0x330, 4, },
	{ 0x340, 5, },
	{ 0x350, 6, },
	{ 0x360, 7, },
	{ 0x370, 8, },
};

DB_API ULONG FoxjetDatabase::GetSynonymPhcAddr (ULONG lAddr)
{
	for (int n = 0; n < ARRAYSIZE (::synonym); n++) 
		if (::synonym [n].m_lUSB == lAddr)
			return ::synonym [n].m_lPHC;

	return lAddr;
}

DB_API ULONG FoxjetDatabase::GetSynonymUsbAddr (ULONG lAddr)
{
	for (int n = 0; n < ARRAYSIZE (::synonym); n++) 
		if (::synonym [n].m_lPHC == lAddr)
			return ::synonym [n].m_lUSB;

	return lAddr;
}

static ULONG FindPHCAddr (CArray <HEADSTRUCT, HEADSTRUCT &> & v, ULONG lAddr)
{
	for (int i = 0; i < v.GetSize (); i++) {
		ULONG lUID = _tcstoul (v [i].m_strUID, NULL, 16);
		ULONG lPHC = GetSynonymPhcAddr (lUID);
		ULONG lUSB = GetSynonymUsbAddr (lUID);

		if (lAddr == lPHC || lAddr == lUSB) 
			return i;
	}

	return -1;
}

DB_API void FoxjetDatabase::Remap (COdbcDatabase & db, TASKSTRUCT & task, ULONG lPrinterID, ULONG lMasterID) // sw0867
{
	DBMANAGETHREADSTATE (db);

	CArray <HEADSTRUCT, HEADSTRUCT &> vPrinter;
	CArray <HEADSTRUCT, HEADSTRUCT &> vMaster;
	CArray <LINESTRUCT, LINESTRUCT &> vLines;
	LINESTRUCT line;

	GetPrinterLines (db, lMasterID, vLines);
	GetPrinterHeads (db, lPrinterID, vPrinter);
	GetPrinterHeads (db, lMasterID, vMaster);

	for (int i = 0; i < task.m_vMsgs.GetSize (); i++) {
		MESSAGESTRUCT & msg = task.m_vMsgs [i];

		ULONG lAddr = GetPHCAddr (vMaster, msg.m_lHeadID);
		//ASSERT (lAddr != -1);

		int nIndex = FindPHCAddr (vPrinter, lAddr);
		//ASSERT (nIndex != -1);

		if (nIndex >= 0 && nIndex < vPrinter.GetSize ())
			msg.m_lHeadID = vPrinter [nIndex].m_lID;
	}

	VERIFY (GetLineRecord (db, task.m_lLineID, line));
	ULONG lLineID = GetLineID (db, line.m_strName, lPrinterID); 
	
	if (lLineID == NOTFOUND) {
		if (FoxjetDatabase::IsNetworked ()) {
			ASSERT (0);
		}
	}
	else
		task.m_lLineID = lLineID;
}

DB_API void FoxjetDatabase::Unmap (COdbcDatabase & db, TASKSTRUCT & task, ULONG lPrinterID, ULONG lMasterID) // sw0867
{
	DBMANAGETHREADSTATE (db);

	Remap (db, task, lMasterID, lPrinterID);
}

DB_API ULONG FoxjetDatabase::GetHeadMapping (COdbcDatabase & db, const HEADSTRUCT & head, ULONG lPrinterID) // sw0867
{
	DBMANAGETHREADSTATE (db);

	CArray <HEADSTRUCT, HEADSTRUCT &> vMaster;
	ULONG lAddr = _tcstoul (head.m_strUID, NULL, 16);

	GetPrinterHeads (db, lPrinterID, vMaster);

	int nIndex = FindPHCAddr (vMaster, lAddr);
	
	if (nIndex != -1) 
		return vMaster [nIndex].m_lID;

	return -1;
}

static int FindByAddress (CArray <HEADSTRUCT, HEADSTRUCT &> & v, const CString & strUID)
{
	ULONG lAddr = _tcstoul (strUID, NULL, 16);

	for (int i = 0; i < v.GetSize (); i++) {
		HEADSTRUCT & h = v [i];
		ULONG l = _tcstoul (h.m_strUID, NULL, 16);
		ULONG lPHC = GetSynonymPhcAddr (l);
		ULONG lUSB = GetSynonymUsbAddr (l);

		if (lPHC == lAddr || lUSB == lAddr)
			return i;
	}

	return -1;
}

DB_API ULONG FoxjetDatabase::CompareHeadConfigs (COdbcDatabase & db, CArray <HEADSTRUCT, HEADSTRUCT &> & vMaster, ULONG lPrinterID)
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vLocal;

	GetPrinterHeads (db, lPrinterID, vLocal);

	for (int nLocal = 0; nLocal < vLocal.GetSize (); nLocal++) {
		HEADSTRUCT & headLocal = vLocal [nLocal];
		int nMaster = FindByAddress (vMaster, headLocal.m_strUID);

		if (nMaster == -1)
			return false;
		else {
			HEADSTRUCT & headMaster = vMaster [nMaster];

			if (headLocal.Height () != headMaster.Height ()) {
				TRACEF (ToString (headMaster.m_dNozzleSpan));
				TRACEF (ToString (headLocal.m_dNozzleSpan));
				return false;
			}

			if (headLocal.GetRes () != headMaster.GetRes ())
				return false;
		}
	}

	return vMaster.GetSize () == vLocal.GetSize () ? true : false;
}

DB_API ULONG FoxjetDatabase::CompareHeadConfigs (COdbcDatabase & db, ULONG lMasterPrinterID, ULONG lPrinterID)
{
	CArray <HEADSTRUCT, HEADSTRUCT &> vMaster;

	GetPrinterHeads (db, lMasterPrinterID, vMaster);

	return CompareHeadConfigs (db, vMaster, lPrinterID);
}

static CMap <ULONG, ULONG, ULONG, ULONG> mapLineIDs;

DB_API ULONG FoxjetDatabase::GetMasterLineID(ULONG lLineID)
{
	ULONG lID = NOTFOUND;

	if (::mapLineIDs.Lookup (lLineID, lID))
		return lID;

	return lLineID;
}


DB_API void FoxjetDatabase::LoadLineMapping (COdbcDatabase & db)
{
	ULONG lMasterID = GetMasterPrinterID (db);
	CArray <LINESTRUCT, LINESTRUCT &> vMasterLines;
	CArray <PRINTERSTRUCT, PRINTERSTRUCT &> vPrinters;
	
	::mapLineIDs.RemoveAll ();
	GetPrinterRecords (db, vPrinters);
	GetPrinterLines (db, lMasterID, vMasterLines);

	for (int nPrinter = 0; nPrinter < vPrinters.GetSize (); nPrinter++) {
		CArray <LINESTRUCT, LINESTRUCT &> vLines;
		ULONG lPrinterID = vPrinters [nPrinter].m_lID;

		GetPrinterLines (db, lPrinterID, vLines);

		for (int nLine = 0; nLine < vLines.GetSize (); nLine++) {
			LINESTRUCT line = vLines [nLine];

			for (int nMasterLine = 0; nMasterLine < vMasterLines.GetSize (); nMasterLine++) {
				LINESTRUCT master = vMasterLines [nLine];

				if (!line.m_strName.CompareNoCase (master.m_strName)) {
					::mapLineIDs.SetAt (line.m_lID, master.m_lID);
					break;
				}
			}
		}
	}

	#ifdef _DEBUG
	for (POSITION pos = ::mapLineIDs.GetStartPosition (); pos; ) {
		CString str;
		ULONG lID = 0, lMasterID = 0;

		::mapLineIDs.GetNextAssoc (pos, lID, lMasterID);
		str.Format (_T ("%d --> %d"), lID, lMasterID);
		TRACEF (str);
	}
	#endif
}
