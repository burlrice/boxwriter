// TaskRecordset.cpp : implementation file
//

#include "stdafx.h"
#include "OdbcRecordset.h"
#include "Database.h"
#include "Debug.h"
#include "FieldDefs.h"
#include "Serialize.h"
#include "Compare.h"
#include "TemplExt.h"
#include "Parse.h"
#include "Resource.h"
#include "OdbcBulkRecordset.h" 

#define MAX_TASKS_PERLINE 10000 //100000

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;
using namespace FoxjetDatabase::Tasks;

extern HINSTANCE hInstance;

IMPLEMENT_DYNAMIC (COdbcException, CException);

////////////////////////////////////////////////////////////////////////////////////////////////////

COdbcException::COdbcException (const CString & str)
:	m_strError (str)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////

// NOTE: when members of this class are modified, need to update To/FromString funtcions
// in Export.cpp (Editor)


FoxjetDatabase::tagTASKSTRUCT::tagTASKSTRUCT ()
:	m_lID (-1),
	m_lLineID (-1),
	m_lBoxID (-1),
	m_lTransformation (0)
{
//TRACEF ("tagTASKSTRUCT::tagTASKSTRUCT ()");
}

FoxjetDatabase::tagTASKSTRUCT::tagTASKSTRUCT (const tagTASKSTRUCT & rhs)
:	m_lID (rhs.m_lID),
	m_strName (rhs.m_strName),
	m_lLineID (rhs.m_lLineID),
	m_lBoxID (rhs.m_lBoxID),
	m_strDesc (rhs.m_strDesc),
	m_strDownload (rhs.m_strDownload),
	m_lTransformation (rhs.m_lTransformation),
	m_strNotes (rhs.m_strNotes)
{
//TRACEF ("tagTASKSTRUCT::tagTASKSTRUCT (const tagTASKSTRUCT & rhs)");
	m_vMsgs.Copy (rhs.m_vMsgs);
	/*
	for (int i = 0; i < rhs.m_vMsgs.GetSize (); i++) {
		MESSAGESTRUCT msg = rhs.m_vMsgs [i];
		msg.m_lTaskID = m_lID;
		m_vMsgs.Add (msg);
	}
	*/
}

tagTASKSTRUCT & FoxjetDatabase::tagTASKSTRUCT::operator = (const tagTASKSTRUCT & rhs)
{
//TRACEF ("operator = (const tagTASKSTRUCT & rhs)");
	if (this != &rhs) {
		m_lID				= rhs.m_lID;
		m_strName			= rhs.m_strName;
		m_lLineID			= rhs.m_lLineID;
		m_lBoxID			= rhs.m_lBoxID;
		m_strDesc			= rhs.m_strDesc;
		m_strDownload		= rhs.m_strDownload;
		m_lTransformation	= rhs.m_lTransformation;
		m_strNotes			= rhs.m_strNotes;

		m_vMsgs.RemoveAll ();

		m_vMsgs.Copy (rhs.m_vMsgs);
		/*
		for (int i = 0; i < rhs.m_vMsgs.GetSize (); i++) {
			MESSAGESTRUCT msg = rhs.m_vMsgs [i];
			msg.m_lTaskID = m_lID;
			m_vMsgs.Add (msg);
		}
		*/
	}

	return * this;
}

FoxjetDatabase::tagTASKSTRUCT::~tagTASKSTRUCT ()
{
//TRACEF ("~tagTASKSTRUCT ()");
	m_vMsgs.RemoveAll ();
}

DB_API bool FoxjetDatabase::operator == (const FoxjetDatabase::TASKSTRUCT & lhs, const FoxjetDatabase::TASKSTRUCT & rhs)
{
	bool bResult =
		lhs.m_lID				== rhs.m_lID &&
		lhs.m_lLineID			== rhs.m_lLineID &&
		lhs.m_lBoxID 			== rhs.m_lBoxID &&
		lhs.m_strName			== rhs.m_strName &&
		lhs.m_strDesc			== rhs.m_strDesc &&
		lhs.m_strDownload		== rhs.m_strDownload &&
		lhs.m_lTransformation	== rhs.m_lTransformation &&
		lhs.m_strNotes			== rhs.m_strNotes;

	if (bResult) {
		TASKSTRUCT * pLhs = (TASKSTRUCT *)(LPVOID)&lhs;
		TASKSTRUCT * pRhs = (TASKSTRUCT *)(LPVOID)&rhs;

		bResult = pLhs->m_vMsgs == pRhs->m_vMsgs;
	}

	return bResult;
}

DB_API bool FoxjetDatabase::operator != (const FoxjetDatabase::TASKSTRUCT & lhs, const FoxjetDatabase::TASKSTRUCT & rhs)
{
	return !(rhs == lhs);
}

DB_API const COdbcRecordset & FoxjetDatabase::operator >> (const COdbcRecordset & rst, TASKSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());
	CString str;

	try {
		COdbcDatabase & db = * (COdbcDatabase *)rst.m_pDatabase;
		COdbcRecordset rst1 (&db), rst2 (&db);
		CArray <MESSAGESTRUCT, MESSAGESTRUCT &> vMsgs; 

		s.m_lID				= (long)rst.GetFieldValue (m_lpszID);
		s.m_lLineID			= (long)rst.GetFieldValue (m_lpszLineID);
		s.m_lBoxID			= (long)rst.GetFieldValue (m_lpszBoxID);
		s.m_strName			= (CString)rst.GetFieldValue (m_lpszName);
		s.m_strDesc			= (CString)rst.GetFieldValue (m_lpszDesc);
		s.m_strDownload		= (CString)rst.GetFieldValue (m_lpszDownload);
		s.m_lTransformation = (long)rst.GetFieldValue (m_lpszTransformation);

		str.Format (_T ("SELECT [%s], [%s], [%s], [%s], [%s], [%s], [%s] FROM [%s] WHERE [%s]=%u"), 
			Messages::m_lpszID, 
			Messages::m_lpszHeadID, 
			Messages::m_lpszTaskID, 
			Messages::m_lpszName, 
			Messages::m_lpszDesc, 
			Messages::m_lpszHeight, 
			Messages::m_lpszIsUpdated, 
			Messages::m_lpszTable,
			Messages::m_lpszTaskID, s.m_lID);
		//TRACEF (str);
		rst1.Open (str);

		str.Format (_T ("SELECT [%s] FROM [%s] WHERE [%s]=%u"), 
			Messages::m_lpszData, 
			Messages::m_lpszTable,
			Messages::m_lpszTaskID, s.m_lID);
		//TRACEF (str);
		rst2.Open (str);
		
		while (!rst1.IsEOF ()) {
			MESSAGESTRUCT msg;

			msg.m_lID			= (long)rst1.GetFieldValue		(Messages::m_lpszID);
			msg.m_lHeadID		= (long)rst1.GetFieldValue		(Messages::m_lpszHeadID);
			msg.m_lTaskID		= (long)rst1.GetFieldValue		(Messages::m_lpszTaskID);
			msg.m_strName		= (CString)rst1.GetFieldValue	(Messages::m_lpszName);
			msg.m_strDesc		= (CString)rst1.GetFieldValue	(Messages::m_lpszDesc);
			msg.m_lHeight		= (long)rst1.GetFieldValue		(Messages::m_lpszHeight);
			msg.m_bIsUpdated	= (bool)rst1.GetFieldValue		(Messages::m_lpszIsUpdated);
			
			vMsgs.Add (msg);
			rst1.MoveNext ();
		}

		for (int nIndex = 0; nIndex < vMsgs.GetSize (); nIndex++) {
			try 
			{
				vMsgs [nIndex].m_strData = (CString)rst2.GetFieldValue (Messages::m_lpszData);
				TRACEF (vMsgs [nIndex].m_strData);
				rst2.MoveNext ();
			}
			catch (CDBException * e) { 
				if (e->m_nRetCode == AFX_SQL_ERROR_DATA_TRUNCATED) {
					rst2.Move (2);
					nIndex++;
				}
			}
		}

		for (int i = 0; i < vMsgs.GetSize (); i++) {
			bool bFound = false;
			MESSAGESTRUCT msg = vMsgs [i];

			for (int j = 0; !bFound && (j < s.m_vMsgs.GetSize ()); j++) {
				if (msg.m_lHeadID == s.m_vMsgs [j].m_lHeadID) {
					s.m_vMsgs [j].m_strData += msg.m_strData;
					TRACEF (s.m_vMsgs [j].m_strData);
					bFound = true;
				}
			}

			if (!bFound)
				s.m_vMsgs.Add (msg);
		}

		rst1.Close ();
		rst2.Close ();

		{
			SETTINGSSTRUCT notes;
			CString strKey = _T ("Notes:") + ToString (s.m_lID);

			GetSettingsRecord (db, 0, strKey, notes);
			s.m_strNotes = notes.m_strData;
		}
	}
	catch (CDBException * e)		{ 
		//CString str; str.Format (_T ("s.m_vMsgs [%d].m_lID: %d"), s.m_vMsgs.GetSize () - 1, s.m_vMsgs [s.m_vMsgs.GetSize () - 1].m_lID); 
		TRACEF (str);
		throw (e); 
	}
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}

DB_API const COdbcBulkRecordset & FoxjetDatabase::operator >> (const FoxjetDatabase::COdbcBulkRecordset & rst, 
															   CArray <FoxjetDatabase::TASKSTRUCT, FoxjetDatabase::TASKSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	try {
		for (int i = 0; i < (int)rst.GetRowsFetched (); i++) {
			CString str;
			COdbcDatabase & db = * (COdbcDatabase *)rst.m_pDatabase;
			COdbcBulkRecordset rstMsgs (&db);
			TASKSTRUCT s;

CString strID = rst.GetFieldValue		(m_lpszID,				i); // TODO: rem

			s.m_lID				= (long)rst.GetFieldValue		(m_lpszID,				i);
			s.m_lLineID			= (long)rst.GetFieldValue		(m_lpszLineID,			i);
			s.m_lBoxID			= (long)rst.GetFieldValue		(m_lpszBoxID,			i);
			s.m_strName			= (CString)rst.GetFieldValue	(m_lpszName,			i);
			s.m_strDesc			= (CString)rst.GetFieldValue	(m_lpszDesc,			i);
			s.m_strDownload		= (CString)rst.GetFieldValue	(m_lpszDownload,		i);
			s.m_lTransformation = (long)rst.GetFieldValue		(m_lpszTransformation,	i);

			str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u"), 
				Messages::m_lpszTable, Messages::m_lpszTaskID, s.m_lID);
str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%s"), Messages::m_lpszTable, Messages::m_lpszTaskID, strID); // TODO: rem
			rstMsgs.Open (str, CRecordset::snapshot, CRecordset::useMultiRowFetch | CRecordset::noDirtyFieldCheck);
			
			while (!rstMsgs.IsEOF ()) {
				CArray <MESSAGESTRUCT, MESSAGESTRUCT &> vMsgs;

				rstMsgs >> vMsgs;
				s.m_vMsgs.Append (vMsgs);

				rstMsgs.MoveNext ();
			}

			rstMsgs.Close ();

			v.Add (s);
		}
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }

	return rst;
}


DB_API COdbcRecordset & FoxjetDatabase::operator << (COdbcRecordset & rst, TASKSTRUCT & s)
{
	DBMANAGETHREADSTATE (rst.GetOdbcDatabase ());

	if (!rst.IsOpen ()) {
		bool bAdd = rst.IsAddNew ();
		rst.Open (_T ("SELECT * FROM ") + CString (m_lpszTable));
		rst.SetEditMode (bAdd ? COdbcRecordset::addnew : COdbcRecordset::edit);
	}

	CStringArray name, value;

	if (s.m_lID == -1) 
		rst.SetEditMode (COdbcRecordset::addnew);

	name.Add (m_lpszLineID);			value.Add (ToString ((long)s.m_lLineID));
	name.Add (m_lpszBoxID);				value.Add (ToString ((long)s.m_lBoxID));
	name.Add (m_lpszName);				value.Add (s.m_strName);
	name.Add (m_lpszDesc);				value.Add (s.m_strDesc);
	name.Add (m_lpszDownload);			value.Add (COdbcVariant::FormatUnicode (s.m_strDownload));
	name.Add (m_lpszTransformation);	value.Add (ToString ((long)s.m_lTransformation));

	CString strSQL = FormatSQL (rst, m_lpszTable, name, value, m_lpszID, s.m_lID);
	VERIFY (rst.GetOdbcDatabase ().ExecuteSQL (strSQL));

	if (s.m_lID == -1)
		s.m_lID = GetMaxID (rst.GetOdbcDatabase (), m_lpszTable, m_lpszID);
/*
// NOTE: when this changes, update the version in Import.cpp (Editor)

	try {
		if (rst.IsAddNew ())
			rst.SetFieldValue (m_lpszID,	(long)GetNextID (rst.GetOdbcDatabase (), m_lpszTable));

		rst.SetFieldValue (m_lpszLineID,			(long)s.m_lLineID);
		rst.SetFieldValue (m_lpszBoxID,				(long)s.m_lBoxID);
		rst.SetFieldValue (m_lpszName,				s.m_strName);
		rst.SetFieldValue (m_lpszDesc,				s.m_strDesc);
		rst.SetFieldValue (m_lpszDownload,			s.m_strDownload);
		rst.SetFieldValue (m_lpszTransformation,	(long)s.m_lTransformation);
	}
	catch (CDBException * e)		{ throw (e); }
	catch (CMemoryException * e)	{ throw (e); }
*/

	{
		SETTINGSSTRUCT notes;

		notes.m_lLineID = 0;
		notes.m_strKey	= _T ("Notes:") + ToString (s.m_lID);
		notes.m_strData = s.m_strNotes;

		if (!UpdateSettingsRecord (rst.GetOdbcDatabase (), notes))
			VERIFY (AddSettingsRecord (rst.GetOdbcDatabase (), notes));
	}

	return rst;
}

DB_API FoxjetDatabase::COrientation FoxjetDatabase::MapPanel (UINT nPanel, const FoxjetDatabase::COrientation & o) 
{
	typedef COrientation _R;
	static const ORIENTATION _0 = ROT_0;
	static const ORIENTATION _1 = ROT_90;
	static const ORIENTATION _2 = ROT_180;
	static const ORIENTATION _3 = ROT_270;
	static const COrientation table [6][4][6] =  
	{	{	// relative face 1
			{ _R (1, _0), _R (2, _0), _R (3, _0), _R (4, _0), _R (5, _0), _R (6, _2) }, // ROT_0   
			{ _R (1, _1), _R (5, _1), _R (3, _3), _R (6, _3), _R (4, _1), _R (2, _1) }, // ROT_90  
			{ _R (1, _2), _R (4, _2), _R (3, _2), _R (2, _2), _R (6, _0), _R (5, _2) }, // ROT_180 
			{ _R (1, _3), _R (6, _1), _R (3, _1), _R (5, _3), _R (2, _3), _R (4, _3) }, // ROT_270 
		}, {// relative face 2
			{ _R (2, _0), _R (3, _0), _R (4, _0), _R (1, _0), _R (5, _1), _R (6, _1) }, // ROT_0   
			{ _R (2, _1), _R (5, _2), _R (4, _3), _R (6, _2), _R (1, _1), _R (3, _1) }, // ROT_90  
			{ _R (2, _2), _R (1, _2), _R (4, _2), _R (3, _2), _R (6, _3), _R (5, _3) }, // ROT_180 
			{ _R (2, _3), _R (6, _0), _R (4, _1), _R (5, _0), _R (3, _3), _R (1, _3) }, // ROT_270 
		}, {// relative face 3
			{ _R (3, _0), _R (4, _0), _R (1, _0), _R (2, _0), _R (5, _2), _R (6, _0) }, // ROT_0   
			{ _R (3, _1), _R (5, _3), _R (1, _3), _R (6, _1), _R (2, _1), _R (4, _1) }, // ROT_90  
			{ _R (3, _2), _R (2, _2), _R (1, _2), _R (4, _2), _R (6, _2), _R (5, _0) }, // ROT_180 
			{ _R (3, _3), _R (6, _3), _R (1, _1), _R (5, _1), _R (4, _3), _R (2, _3) }, // ROT_270 
		}, {// relative face 4
			{ _R (4, _0), _R (1, _0), _R (2, _0), _R (3, _0), _R (5, _3), _R (6, _3) }, // ROT_0   
			{ _R (4, _1), _R (5, _0), _R (2, _3), _R (6, _0), _R (3, _1), _R (1, _1) }, // ROT_90  
			{ _R (4, _2), _R (3, _2), _R (2, _2), _R (1, _2), _R (6, _1), _R (5, _1) }, // ROT_180 
			{ _R (4, _3), _R (6, _2), _R (2, _1), _R (5, _2), _R (1, _3), _R (3, _3) }, // ROT_270 
		}, {// relative face 5
			{ _R (5, _0), _R (2, _3), _R (6, _0), _R (4, _1), _R (3, _2), _R (1, _0) }, // ROT_0   
			{ _R (5, _1), _R (3, _3), _R (6, _3), _R (1, _1), _R (4, _2), _R (2, _0) }, // ROT_90  
			{ _R (5, _2), _R (4, _3), _R (6, _2), _R (2, _1), _R (1, _2), _R (3, _0) }, // ROT_180 
			{ _R (5, _3), _R (1, _3), _R (6, _1), _R (3, _1), _R (2, _2), _R (4, _0) }, // ROT_270 
		}, {// relative face 6
			{ _R (6, _0), _R (4, _1), _R (5, _0), _R (2, _3), _R (3, _0), _R (1, _2) }, // ROT_0   
			{ _R (6, _1), _R (3, _1), _R (5, _3), _R (1, _3), _R (2, _0), _R (4, _2) }, // ROT_90  
			{ _R (6, _2), _R (2, _1), _R (5, _2), _R (4, _3), _R (1, _0), _R (3, _2) }, // ROT_180 
			{ _R (6, _3), _R (1, _1), _R (5, _1), _R (3, _3), _R (4, _0), _R (2, _2) }, // ROT_270 
		}
	};

	ASSERT (o.m_nPanel >= 1 && o.m_nPanel <= 6);
	ASSERT ((int)o.m_orient >= 0 && (int)o.m_orient < 4);
	ASSERT (nPanel >= 1 && nPanel <= 6);

	_R map = table [o.m_nPanel - 1][(int)o.m_orient][nPanel - 1];

	return map;
}

DB_API UINT FoxjetDatabase::GetTaskRecordCount (COdbcDatabase & database, ULONG lLineID)
{
	DBMANAGETHREADSTATE (database);

	UINT nCount = 0;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);
		CString str = _T ("SELECT * FROM [" + CString (m_lpszTable) + "];");

		if (lLineID != ALL)
			str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
				m_lpszTable, m_lpszLineID, lLineID);

		rst.Open (str);
		nCount = rst.CountRecords ();
		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return nCount;
}

DB_API ULONG FoxjetDatabase::GetTaskID (COdbcDatabase & database, const CString & strTask, const CString & strLine, ULONG lPrinterID)
{
	DBMANAGETHREADSTATE (database);

	ULONG lResult = NOTFOUND;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);
		ULONG lLineID = GetLineID (database, strLine, lPrinterID);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s' AND [%s]=%u;"),
			m_lpszTable, m_lpszName, strTask, m_lpszLineID, lLineID);

		rst.Open (str);

		if (!rst.IsEOF ()) 
			lResult = (long)rst.GetFieldValue (m_lpszID);

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return lResult;
}

DB_API bool FoxjetDatabase::GetTaskRecord (COdbcDatabase & database, 
										   const CString & strTask, 
										   ULONG lLineID, TASKSTRUCT & task)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]='%s' AND [%s]=%u;"),
			m_lpszTable, 
			m_lpszName, strTask, 
			m_lpszLineID, lLineID);
		rst.Open (str);

		if (!rst.IsEOF ()) {
			rst >> task;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetTaskRecord (COdbcDatabase & database, 
										   ULONG lID, TASKSTRUCT & task)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);
		COdbcRecordset rstMsgs (&database);
		CString str;
		
		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, lID);
		rst.Open (str);

		if (!rst.IsEOF ()) {
			rst >> task;
			bResult = true;
		}

		rst.Close ();
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

static int Find (CArray <HEADSTRUCT, HEADSTRUCT &> & v, ULONG lHeadID)
{
	for (int i = 0; i < v.GetSize (); i++) {
		ULONG lID = v [i].m_lID;
		
		if (lID == lHeadID) 
			return i;
	}

	return -1;
}

static void DoIntegrityCheck (COdbcDatabase & database)
{
#ifdef _DEBUG
	try // integrity check
	{
		bool bFailed = false;

		COdbcRecordset rst (&database);
		CString strSQL = 
			_T ("SELECT Messages.TaskID, Messages.HeadID, Count(Messages.HeadID) AS CountOfHeadID ")
			_T ("FROM Messages ")
			_T ("GROUP BY Messages.TaskID, Messages.HeadID ")
			_T ("HAVING (((Count(Messages.HeadID))>1));");
		
		for (rst.Open (strSQL); !rst.IsEOF (); rst.MoveNext ()) {
			TRACEF (_T ("duplicate message records: ") + (CString)rst.GetFieldValue ((short)0));
			bFailed = true;
		}

		if (bFailed) 
			ASSERT (_T ("duplicate message records: ") == 0);
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
#endif
}

DB_API bool FoxjetDatabase::UpdateMessages (COdbcDatabase & database, TASKSTRUCT & s)
{
	ULONG lPrinterID = GetMasterPrinterID (database);

	DoIntegrityCheck (database);

	try 
	{
		COdbcRecordset rst (&database);
		CString str;

		{ // delete Message records that have an invalid HeadID
			/*
			valid head IDs:		SELECT Heads.ID FROM (Heads INNER JOIN Panels ON Heads.PanelID = Panels.ID) INNER JOIN Lines ON Panels.LineID = Lines.ID WHERE (((Lines.ID)=2));

			message records:	SELECT Messages.ID, Messages.HeadID FROM Messages WHERE (((Messages.TaskID)=712));

			SELECT Messages.ID, Messages.HeadID, Messages.TaskID
			FROM Messages
			WHERE (((Messages.TaskID)=712) AND (((([Messages].[TaskID])=712)) AND Messages.HeadID 
			Not In (SELECT Heads.ID FROM (Heads INNER JOIN Panels ON Heads.PanelID = Panels.ID) INNER JOIN Lines ON Panels.LineID = Lines.ID WHERE (((Lines.ID)=2)))));

			DELETE *
			FROM Messages
			WHERE (((Messages.TaskID)=712) AND (((([Messages].[TaskID])=712)) AND Messages.HeadID 
			Not In (SELECT Heads.ID FROM (Heads INNER JOIN Panels ON Heads.PanelID = Panels.ID) INNER JOIN Lines ON Panels.LineID = Lines.ID WHERE (((Lines.ID)=2)))));
			*/

			str.Format (
				_T ("DELETE * FROM [%s] ")
				_T ("WHERE ((([%s].[%s])=%d) AND (((([%s].[%s])=%d)) AND [%s].[%s] ")
				_T ("Not In (SELECT [%s].[%s] FROM ([%s] INNER JOIN [%s] ON [%s].[%s] = [%s].[%s]) INNER JOIN [%s] ON [%s].[%s] = [%s].[%s] WHERE ((([%s].[%s])=%d)))));"),
				Messages::m_lpszTable,
				Messages::m_lpszTable, Messages::m_lpszTaskID, s.m_lID,
				Messages::m_lpszTable, Messages::m_lpszTaskID, s.m_lID, 
				Messages::m_lpszTable, Messages::m_lpszHeadID,
				Heads::m_lpszTable, Heads::m_lpszID,
				Heads::m_lpszTable, Panels::m_lpszTable,
				Heads::m_lpszTable, Heads::m_lpszPanelID,
				Panels::m_lpszTable, Panels::m_lpszID,
				Lines::m_lpszTable,  Panels::m_lpszTable, Panels::m_lpszLineID,
				Lines::m_lpszTable, Lines::m_lpszID,
				Lines::m_lpszTable, Lines::m_lpszID, s.m_lLineID);
			TRACEF (str);

			int nDelete = database.ExecuteSQL (str);

			if (nDelete) { TRACEF (ToString (nDelete) + _T (" records deleted")); }
		}

		DoIntegrityCheck (database);

		{ // delete Message records that have a duplicate HeadID
			str.Format (_T ("SELECT Count([%s].[%s]) AS CountOfHeadID, [%s].[%s] FROM [%s] GROUP BY [%s].[%s], [%s].[%s] HAVING ((([%s].[%s])=%d));"),
				Messages::m_lpszTable, Messages::m_lpszHeadID,
				Messages::m_lpszTable, Messages::m_lpszHeadID,
				Messages::m_lpszTable, 
				Messages::m_lpszTable, Messages::m_lpszHeadID,
				Messages::m_lpszTable, Messages::m_lpszTaskID,
				Messages::m_lpszTable, Messages::m_lpszTaskID,
				s.m_lID);
			TRACEF (str);

			for (rst.Open (str); !rst.IsEOF (); rst.MoveNext ()) {
				int nCount = rst.GetFieldValue ((short)0);
				ULONG lHeadID = (ULONG)(long)rst.GetFieldValue ((short)1);

				if (nCount > 1) {
					str.Format (_T ("DELETE * FROM [%s] WHERE [%s]=%d AND [%s]=%d AND [%s] NOT IN (SELECT TOP 1 [%s] FROM [%s] WHERE [%s]=%u AND [%s]=%u ORDER BY [%s] DESC)"),
						Messages::m_lpszTable,
						Messages::m_lpszTaskID, s.m_lID,
						Messages::m_lpszHeadID, lHeadID,
						Messages::m_lpszID, 
						Messages::m_lpszID,
						Messages::m_lpszTable,
						Messages::m_lpszTaskID, s.m_lID,
						Messages::m_lpszHeadID, lHeadID,
						Messages::m_lpszID);
					TRACEF (str);
					database.ExecuteSQL (str);
					DoIntegrityCheck (database);
				}
			}
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	try {
		CArray <HEADSTRUCT, HEADSTRUCT &> vHeads;

		GetHeadRecords (database, vHeads);

		for (int i = 0; i < s.m_vMsgs.GetSize (); i++) {
			MESSAGESTRUCT & msg = s.m_vMsgs [i];
			int nHead = Find (vHeads, msg.m_lHeadID);

			if (nHead == -1) {
				VERIFY (DeleteMessageRecord (database, msg.m_lID));
			}
			else {
				const ULONG lID = msg.m_lID;

				//#ifdef _DEBUG
				//{
				//	CString str;
				//	TRACEF (_T ("nHead: ") + ToString (nHead));

				//	for (int i = 0; i < vHeads.GetSize (); i++) {
				//		str.Format (_T ("%d, %s, %s"), vHeads [i].m_lID, vHeads [i].m_strUID, vHeads [i].m_strName);
				//		TRACEF (str);
				//	}
				//}
				//#endif

				if (msg.m_lID == -1) {
					ULONG lID = GetMessageID (database, msg.m_strName, vHeads [nHead].m_strUID, lPrinterID);

					if (lID != NOTFOUND)
						msg.m_lID = lID;
				}

				ASSERT (msg.m_lID != NOTFOUND);

				msg.m_lTaskID = s.m_lID;

				//#ifdef _DEBUG
				//{
				//	CString str;
				//	str.Format (_T ("[%d] msg.m_lID: %d, msg.m_lTaskID: %d"), i, msg.m_lID, msg.m_lTaskID);
				//	TRACEF (str);
				//	TRACEF (msg.m_strData);
				//}
				//#endif


				DoIntegrityCheck (database);

				if (!UpdateMessageRecord (database, msg)) {
					DoIntegrityCheck (database);
					VERIFY (AddMessageRecord (database, msg));
					s.m_vMsgs [i].m_lID = msg.m_lID;
				}

				DoIntegrityCheck (database);
			}
		}

		DoIntegrityCheck (database);

		return true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return false;
}

DB_API ULONG FoxjetDatabase::GetMaxTasks ()
{
	return (MAX_TASKS_PERLINE == -1) ? LONG_MAX : MAX_TASKS_PERLINE;
}

DB_API bool FoxjetDatabase::AddTaskRecord (COdbcDatabase & database, TASKSTRUCT & task)
{
	DBMANAGETHREADSTATE (database);
	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%d;"),
			m_lpszTable,
			m_lpszLineID,
			task.m_lLineID);

		rst.Open (str, CRecordset::dynaset);

		if (MAX_TASKS_PERLINE != -1) {
			ULONG lCount = rst.CountRecords ();

			if ((lCount + 1) > MAX_TASKS_PERLINE) {
				CString strError;
				
				strError.Format (LoadString (IDS_MAX_TASKS_PERLINE), MAX_TASKS_PERLINE);

				rst.Close ();
				throw new COdbcException (strError);

				return false;
			}
		}

		task.m_lID = -1;
		rst << task;

		/* TODO: rem
		rst.AddNew ();
		rst << task;
		rst.Update ();
		rst.MoveLast ();

		ULONG lID = (long)rst.GetFieldValue (m_lpszID);
		task.SetID (lID);
		rst.Close ();
		*/

		for (int i = 0; i < task.m_vMsgs.GetSize (); i++) 
			task.m_vMsgs [i].m_lID = -1;

		bResult = UpdateMessages (database, task);
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::UpdateTaskRecord (COdbcDatabase & database, TASKSTRUCT & task)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);

		rst << task;
		bResult = true;

		/* TODO: rem
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszID, task.m_lID);
		rst.Open (str, CRecordset::dynaset);

		if (!rst.IsEOF ()) {
			rst.Edit ();
			rst << task;
			rst.Update ();
			bResult = true;
		}

		rst.Close ();
		*/

		if (bResult)
			bResult = UpdateMessages (database, task);
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::DeleteTaskRecord (COdbcDatabase & database, ULONG lID)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	ASSERT (database.IsOpen ());

	try {
		CString str;

		str.Format (_T ("DELETE * FROM [%s] WHERE %s=%u;"), 
			Messages::m_lpszTable, Messages::m_lpszTaskID, lID);
		database.ExecuteSQL (str);

		str.Format (_T ("DELETE * FROM [%s] WHERE %s=%u;"), 
			m_lpszTable, m_lpszID, lID);
		database.ExecuteSQL (str);

		DeleteSettingsRecord (database, 0, _T ("Notes:") + ToString (lID));

		bResult = true;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetTaskRecords (COdbcDatabase & database, 
											ULONG lLineID, 
											CArray <TASKSTRUCT, TASKSTRUCT &> & v)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	v.RemoveAll ();

	ASSERT (database.IsOpen ());

	try {
		COdbcRecordset rst (&database);
		CString str;

		if (lLineID == FoxjetDatabase::ALL)
			str.Format (_T ("SELECT * FROM [%s] ORDER BY [%s] ASC;"), 
				m_lpszTable, m_lpszName);
		else
			str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
				m_lpszTable, m_lpszLineID, lLineID);

		rst.Open (str);

		while (!rst.IsEOF ()) {
			TASKSTRUCT task;
			rst >> task;
			v.Add (task);
			rst.MoveNext ();
		}
		
		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ 
		CString str;
		str.Format (_T ("v [%d].m_lID: %d"), v.GetSize () - 1, v [v.GetSize () - 1].m_lID);
		TRACEF (str);
		HANDLEEXCEPTION (e); 
	}
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

DB_API bool FoxjetDatabase::GetTaskRecord (COdbcDatabase & database, 
										   const CString & strTask, 
										   const CString & strLine, 
										   ULONG lPrinterID,
										   TASKSTRUCT & task)
{
	DBMANAGETHREADSTATE (database);

	ULONG lLineID = GetLineID (database, strLine, lPrinterID);
	
	bool bResult = GetTaskRecord (database, strTask, lLineID, task);

	return bResult;
}

bool DB_API GetTaskRecordsBulk (COdbcDatabase & database, ULONG lLineID, CArray <TASKSTRUCT, TASKSTRUCT &> & v) // TODO: rem
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;

	v.RemoveAll ();

	ASSERT (database.IsOpen ());

	try {
		COdbcBulkRecordset rst (&database);
		CString str;

		str.Format (_T ("SELECT * FROM [%s] WHERE [%s]=%u;"), 
			m_lpszTable, m_lpszLineID, lLineID);
		rst.SetRowsetSize (50);
		rst.Open (str, CRecordset::snapshot, CRecordset::useMultiRowFetch | CRecordset::noDirtyFieldCheck);

		while (!rst.IsEOF ()) {
			CArray <TASKSTRUCT, TASKSTRUCT &> vRow;

			rst >> vRow;
			v.Append (vRow);

			rst.MoveNext ();
		}
		
		rst.Close ();
		bResult = v.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return bResult;
}

//////////////////////////////////////////////////////////////////////////////////////////

static const CString strTaskID		= CString (_T ("[")) + Tasks::m_lpszTable		+ CString (_T ("].[")) + Tasks::m_lpszID		+ CString (_T ("]"));
static const CString strTaskName	= CString (_T ("[")) + Tasks::m_lpszTable		+ CString (_T ("].[")) + Tasks::m_lpszName		+ CString (_T ("]"));
static const CString strMsgID		= CString (_T ("[")) + Messages::m_lpszTable	+ CString (_T ("].[")) + Messages::m_lpszID		+ CString (_T ("]"));
static const CString strMsgName	= CString (_T ("[")) + Messages::m_lpszTable	+ CString (_T ("].[")) + Messages::m_lpszName	+ CString (_T ("]"));
static const CString strMsgDesc	= CString (_T ("[")) + Messages::m_lpszTable	+ CString (_T ("].[")) + Messages::m_lpszDesc	+ CString (_T ("]"));

static TASKSTRUCT * CreateTask (COdbcRecordset & rst, CArray <TASKSTRUCT *, TASKSTRUCT *> & vTasks)
{
	vTasks.Add (new TASKSTRUCT ());
	TASKSTRUCT * pTask = vTasks [vTasks.GetSize () - 1];
	pTask->m_vMsgs.Add (MESSAGESTRUCT ());

	pTask->m_lID				= (long)rst.GetFieldValue		(strTaskID);
	pTask->m_lLineID			= (long)rst.GetFieldValue		(Tasks::m_lpszLineID);
	pTask->m_lBoxID				= (long)rst.GetFieldValue		(Tasks::m_lpszBoxID);
	pTask->m_strName			= (CString)rst.GetFieldValue	(strTaskName);
	pTask->m_strDesc			= (CString)rst.GetFieldValue	(Tasks::m_lpszDesc);
	pTask->m_strDownload		= (CString)rst.GetFieldValue	(Tasks::m_lpszDownload);
	pTask->m_lTransformation	= (long)rst.GetFieldValue		(Tasks::m_lpszTransformation);

	return pTask;
}
//////////////////////////////////////////////////////////////////////////////////////////

DB_API void FoxjetDatabase::Free (CArray <TASKSTRUCT *, TASKSTRUCT *> & v)
{
	for (int i = 0; i < v.GetSize (); i++)
		if (TASKSTRUCT * p = v [i])
			delete p;

	v.RemoveAll ();
}

DB_API bool FoxjetDatabase::GetTaskRecords (COdbcDatabase & database, 
											ULONG lLineID, 
											CArray <TASKSTRUCT *, TASKSTRUCT *> & vTasks)
{
	DBMANAGETHREADSTATE (database);

	bool bResult = false;
	COdbcRecordset rst (database);
	CArray <CDBException *, CDBException *> vDbExceptions;
	TASKSTRUCT * pTask = NULL;
	MESSAGESTRUCT * pMsg = NULL;
	ULONG lLastTaskID = -1;
	CString str;

	vTasks.RemoveAll ();

	ASSERT (database.IsOpen ());
	
	try
	{
		if (lLineID == FoxjetDatabase::ALL) {
			str.Format (
				_T ("SELECT [%s].*, [%s].*")
				_T (" FROM [%s] LEFT JOIN [%s] ON [%s].[%s] = [%s].[%s]")
				_T (" ORDER BY [%s].[%s], [%s].[%s];"), 
				Tasks::m_lpszTable, Messages::m_lpszTable,
				Tasks::m_lpszTable, Messages::m_lpszTable, Tasks::m_lpszTable, Tasks::m_lpszID, Messages::m_lpszTable, Messages::m_lpszTaskID,
				//Tasks::m_lpszTable, Tasks::m_lpszID, Messages::m_lpszTable, Messages::m_lpszHeadID);
				Tasks::m_lpszTable, Tasks::m_lpszName, Messages::m_lpszTable, Messages::m_lpszHeadID);
		}
		else {
			str.Format (
				_T ("SELECT [%s].*, [%s].*")
				_T (" FROM [%s] LEFT JOIN [%s] ON [%s].[%s] = [%s].[%s]")
				_T (" WHERE ((([%s].[%s])=%d))")
				_T (" ORDER BY [%s].[%s], [%s].[%s];"), 
				Tasks::m_lpszTable, Messages::m_lpszTable,
				Tasks::m_lpszTable, Messages::m_lpszTable, Tasks::m_lpszTable, Tasks::m_lpszID, Messages::m_lpszTable, Messages::m_lpszTaskID,
				Tasks::m_lpszTable, Tasks::m_lpszLineID, lLineID,
				//Tasks::m_lpszTable, Tasks::m_lpszID, Messages::m_lpszTable, Messages::m_lpszHeadID);
				Tasks::m_lpszTable, Tasks::m_lpszName, Messages::m_lpszTable, Messages::m_lpszHeadID);
		}
		
		TRACEF (str);

		rst.Open (str, CRecordset::snapshot, CRecordset::readOnly | CRecordset::executeDirect | CRecordset::noDirtyFieldCheck);
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	while (!rst.IsEOF ()) {
		try 
		{
			ULONG lTaskID = (long)rst.GetFieldValue (strTaskID);

			if (pMsg == NULL || lTaskID != lLastTaskID) 
				VERIFY (pTask = CreateTask (rst, vTasks));
			else 
				pTask->m_vMsgs.Add (MESSAGESTRUCT ());

			VERIFY (pMsg = &pTask->m_vMsgs [pTask->m_vMsgs.GetSize () - 1]);

			pMsg->m_lID			= (long)rst.GetFieldValue		(strMsgID);
			pMsg->m_lHeadID		= (long)rst.GetFieldValue		(Messages::m_lpszHeadID);
			pMsg->m_lTaskID		= (long)rst.GetFieldValue		(Messages::m_lpszTaskID);
			pMsg->m_strName		= (CString)rst.GetFieldValue	(strMsgName);
			pMsg->m_strDesc		= (CString)rst.GetFieldValue	(strMsgDesc);
			pMsg->m_lHeight		= (long)rst.GetFieldValue		(Messages::m_lpszHeight);
			pMsg->m_bIsUpdated	= (bool)rst.GetFieldValue		(Messages::m_lpszIsUpdated);
			pMsg->m_strData		= (CString)rst.GetFieldValue	(Messages::m_lpszData);

			lLastTaskID = lTaskID;
			rst.MoveNext ();
		}
		catch (CDBException * e)		{ vDbExceptions.Add (e); }
		catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
	}

	try 
	{
		rst.Close ();
		bResult = vTasks.GetSize () > 0;
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	for (int i = vDbExceptions.GetSize () - 1; i >= 0; i--) {
		for (int j = vDbExceptions.GetSize () - 1; j > i; j--) {
			CDBException * e1 = vDbExceptions [i];
			CDBException * e2 = vDbExceptions [j];

			if (e1->m_nRetCode == e2->m_nRetCode)
				vDbExceptions.RemoveAt (j);
		}
	}

	for (int i = 0; i < vDbExceptions.GetSize (); i++) 
		HANDLEEXCEPTION (vDbExceptions [i]); 

	return bResult;
}

