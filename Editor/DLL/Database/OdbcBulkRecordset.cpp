// OdbcBulkRecordset.cpp: implementation of the COdbcBulkRecordset class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "OdbcBulkRecordset.h"
#include "Database.h"
#include "Debug.h"

using namespace FoxjetDatabase;
using FoxjetDatabase::CQualifiedFieldInfo;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

const CString COdbcBulkRecordset::m_strBadRow = _T ("Row \"%d\" is out of range");
const int nMax = 3000; // TODO: rem


IMPLEMENT_DYNAMIC(COdbcBulkRecordset, COdbcRecordset)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COdbcBulkRecordset::COdbcBulkRecordset (COdbcDatabase & database)
:	m_ppvData (NULL),
	m_ppvLength (NULL),
	m_nAllocatedFields (0),
	COdbcRecordset (database)
{
}

COdbcBulkRecordset::COdbcBulkRecordset (COdbcDatabase * pDatabase)
:	m_ppvData (NULL),
	m_ppvLength (NULL),
	m_nAllocatedFields (0),
	COdbcRecordset (pDatabase)
{
}

COdbcBulkRecordset::~COdbcBulkRecordset()
{
}

void COdbcBulkRecordset::Close () 
{
	COdbcRecordset::Close ();

	if (m_ppvData) {
		delete [] m_ppvData;
		m_ppvData = NULL;
	}
	if (m_ppvLength) {
		delete [] m_ppvLength;
		m_ppvLength = NULL;
	}

	m_nAllocatedFields = 0;
}

void COdbcBulkRecordset::GetFieldValue (LPCTSTR lpszName, int nRow, COdbcVariant & v) const
{
	short nIndex = GetFieldIndex (lpszName);
	
	if (nIndex == -1) {
		CDBException * p = new CDBException (SQL_ERROR);
		p->m_strError.Format (COdbcRecordset::m_strBadFieldName, lpszName);
		THROW (p);
	}
	else if (nRow < 0 || nRow >= (int)GetRowsFetched ()) {
		CDBException * p = new CDBException (SQL_ERROR);
		p->m_strError.Format (m_strBadRow, nRow);
		THROW (p);
	}
	else 
		GetFieldValue (nIndex, nRow, v);
}

void COdbcBulkRecordset::GetFieldValue (short nIndex, int nRow, COdbcVariant & v) const
{
	if (nIndex < 0 || nIndex >= m_vFields.GetSize ()) {
		CDBException * p = new CDBException (SQL_ERROR);
		p->m_strError.Format (m_strBadFieldIndex, nIndex);
		THROW (p);
	}
	if (nRow < 0 || nRow >= (int)GetRowsFetched ()) {
		CDBException * p = new CDBException (SQL_ERROR);
		p->m_strError.Format (m_strBadRow, nRow);
		THROW (p);
	}
	else {
		LPVOID lpData = (LPVOID)m_ppvData [nIndex];
		long * pLength = (long*)m_ppvLength [nIndex];
		const CQualifiedFieldInfo & info = * (CQualifiedFieldInfo *)m_vFields [nIndex];

		v.SetType (info.m_nSQLType);

		if (info.m_nSQLType == DBVT_STRING) { 
			CString str;

			if (pLength [nRow] != SQL_NULL_DATA) 
				str = &((LPCTSTR)lpData) [nRow * nMax];

			v = str;
		}
		else {
			/* TODO: rem 
			// try creating a pointer array for each field of the appropriate type, instead
			// of LPVOID. let the framework allocate the buffers (array entries).

			switch (info.m_value.m_dwType) { 
			case DBVT_BOOL:		v = (bool)lpData [nRow];				break;
			case DBVT_UCHAR:	v = (UCHAR)lpData [nRow];				break;
			case DBVT_SHORT:	v = (short)lpData [nRow];				break;
			case DBVT_LONG:		v = (long)lpData [nRow];				break;
			case DBVT_SINGLE:	v = (float)lpsata [nRow];				break;
			case DBVT_DOUBLE:	v = (double)lpData [nRow];				break;
			case DBVT_DATE:		v = (COleDateTime)lpsata [nRow];		break;
			case DBVT_BINARY:	v = (CByteArray)lpData [nRow];			break;
			}
			*/
		}
	}
}

COdbcVariant COdbcBulkRecordset::GetFieldValue (LPCTSTR lpszName, int nRow) const
{
	COdbcVariant v;
	GetFieldValue (lpszName, nRow, v);
	return v;
}

COdbcVariant COdbcBulkRecordset::GetFieldValue (short nIndex, int nRow) const
{
	COdbcVariant v;
	GetFieldValue (nIndex, nRow, v);
	return v;
}

BOOL COdbcBulkRecordset::Open (LPCTSTR lpszSQL, UINT nOpenType, DWORD dwOptions)
{
	ASSERT (dwOptions & CRecordset::useMultiRowFetch);
	ASSERT (nOpenType & CRecordset::snapshot);

	return COdbcRecordset::Open (COdbcVariant::FormatUnicode (lpszSQL), nOpenType, dwOptions);
}

BOOL COdbcBulkRecordset::Open (UINT nOpenType, LPCTSTR lpszSQL, DWORD dwOptions)
{
	ASSERT (dwOptions & CRecordset::useMultiRowFetch);
	ASSERT (nOpenType & CRecordset::snapshot);

	return COdbcRecordset::Open (nOpenType, COdbcVariant::FormatUnicode (lpszSQL), dwOptions);
}

void COdbcBulkRecordset::DoBulkFieldExchange(CFieldExchange* pFX)
{
	const int nRows = GetRowsetSize ();

	if (pFX->m_nOperation == CFieldExchange::AllocMultiRowBuffer && !m_nAllocatedFields) {
		int nCount = GetODBCFieldCount ();

		m_ppvData = new void * [nCount];
		m_ppvLength = new void * [nCount];

		::ZeroMemory (m_ppvData, sizeof (void *) * nCount);
		::ZeroMemory (m_ppvLength, sizeof (void *) * nCount);

		PreBindFields ();

		m_nFields = m_nAllocatedFields = nCount;
	}

	pFX->SetFieldType (CFieldExchange::outputColumn);

/* TODO: rem
	for (int i = 0; i < GetODBCFieldCount (); i++) {
// TODO: rem //const int nMax = 400;// TODO: rem //USHRT_MAX;

// TODO: rem //ASSERT (nMax >= 1 && nMax <= USHRT_MAX);

// TODO: rem //		RFX_Text_Bulk (pFX, info.m_strName, (LPSTR *)&m_ppvData [i], (long **)&m_ppvLength [i], nMax);
		RFX_Text_Bulk (pFX, _T (""), (LPSTR *)&m_ppvData [i], (long **)&m_ppvLength [i], nMax);
	}
*/
	
	for (int i = 0; i < m_vFields.GetSize (); i++) {
		CQualifiedFieldInfo & info = * (CQualifiedFieldInfo *)m_vFields [i];
		const int nMax = max (1, min ((int)info.m_nPrecision, USHRT_MAX));
		ASSERT (nMax >= 1 && nMax <= USHRT_MAX);
 
		switch (info.m_value.m_dwType) { 
		case DBVT_BOOL:
			RFX_Bool_Bulk (pFX, info.m_strName, (int **)m_ppvData [i], (long **)&m_ppvLength [i]);
			break;
		case DBVT_UCHAR:
			RFX_Byte_Bulk (pFX, info.m_strName, (UCHAR **)m_ppvData [i], (long **)&m_ppvLength [i]);
			break;
		case DBVT_SHORT:
			RFX_Int_Bulk (pFX, info.m_strName, (int **)m_ppvData [i], (long **)&m_ppvLength [i]);
			break;
		case DBVT_LONG:
			RFX_Long_Bulk (pFX, info.m_strName, (long **)m_ppvData [i], (long **)&m_ppvLength [i]);
			break;
		case DBVT_SINGLE:
			RFX_Single_Bulk (pFX, info.m_strName, (float **)m_ppvData [i], (long **)&m_ppvLength [i]);
			break;
		case DBVT_DOUBLE:
			RFX_Double_Bulk (pFX, info.m_strName, (double **)m_ppvData [i], (long **)&m_ppvLength [i]);
			break;
		case DBVT_DATE:
			RFX_Date_Bulk (pFX, info.m_strName, (TIMESTAMP_STRUCT **)m_ppvData [i], (long **)&m_ppvLength [i]);
			break;
		case DBVT_STRING:
			{
#ifdef _DEBUG
				try {
#endif //_DEBUG
					RFX_Text_Bulk (pFX, info.m_strName, (char **)m_ppvData [i], (long **)&m_ppvLength [i], nMax);
#ifdef _DEBUG
				}
				catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
				catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }
#endif //_DEBUG
			}
			break;
		case DBVT_BINARY:
			RFX_Binary_Bulk (pFX, info.m_strName, (BYTE **)m_ppvData [i], (long **)&m_ppvLength [i], nMax);
			break;
		default:
		case DBVT_NULL:
			ASSERT (0);
			break;
		}
	}
}

ULONG COdbcBulkRecordset::CountRecords ()
{
	ULONG lCount = 0;

	try {
		if (IsOpen ()) { 
			// TODO: no way to tell if rec count is really 0 or 1

			if (!IsBOF ())
				MoveFirst ();

			while (!IsEOF ()) {
				lCount += GetRowsFetched ();
				MoveNext ();
			}

			if (!IsBOF ())
				MoveFirst ();
		}
	}
	catch (CDBException * e)		{ HANDLEEXCEPTION (e); }
	catch (CMemoryException * e)	{ HANDLEEXCEPTION (e); }

	return lCount;
}
