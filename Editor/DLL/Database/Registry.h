#ifndef __REGISTRY_H__
#define __REGISTRY_H__

#include "DbApi.h"
#include <string>
#include <vector>

namespace FoxjetDatabase
{
	DB_API bool WriteProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szData);
	DB_API bool WriteProfileString (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szData);
	DB_API CString GetProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szDefault);
	DB_API CString GetProfileString (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szDefault);
	DB_API bool WriteProfileInt (LPCTSTR szSection, LPCTSTR szKey, UINT nData);
	DB_API bool WriteProfileInt (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, UINT nData);
	DB_API UINT GetProfileInt (LPCTSTR szSection, LPCTSTR szKey, UINT nDefault);
	DB_API UINT GetProfileInt (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, UINT nDefault);
	DB_API bool WriteProfileByte (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, BYTE nData);
	DB_API BYTE GetProfileByte (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, UINT nDefault); 

	DB_API unsigned __int64 GetProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, unsigned __int64 nDefault);
	DB_API unsigned __int64 GetProfileInt64 (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, unsigned __int64 nDefault);
	DB_API bool WriteProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, unsigned __int64 nData);
	DB_API bool WriteProfileInt64 (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, unsigned __int64 nData);

	DB_API __int64 GetProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, __int64 nDefault);
	DB_API __int64 GetProfileInt64 (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, __int64 nDefault);
	DB_API bool WriteProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, __int64 nData);
	DB_API bool WriteProfileInt64 (HKEY hKeyRoot, LPCTSTR szSection, LPCTSTR szKey, __int64 nData);

	namespace ODBC
	{
		DB_API CString GetProfileString (LPCTSTR lpszDSN, LPCTSTR lpszKey, LPCTSTR lpszDefault = _T (""));
	};

	namespace Encrypted
	{
		CString DB_API GetSha256 (LPBYTE lpBuffer, ULONG lLen);

		namespace RSA
		{
			DB_API CString Encrypt (const CString & str);
			DB_API CString Decrypt (const CString & str);
			DB_API void SetPublicKey (const LPBYTE p, DWORD dwLen);
			DB_API void SetPrivateKey (const LPBYTE p, DWORD dwLen);
			DB_API bool IsEnabled ();
		};

		namespace InkCodes
		{
			typedef struct DB_API tagINK_CODE
			{
				tagINK_CODE ();

				unsigned __int32 m_nManufacturer;
				unsigned __int32 m_nDay;
				unsigned __int32 m_nSerialNumber;
				unsigned __int32 m_nExpPeriod;
				unsigned __int32 m_nCRC;
			} INK_CODE;

			#define DECLARE_INK_CODE_MEMBER(s) \
				unsigned __int32 Get##s () const; \
				bool Set##s (unsigned __int32 n);

			class DB_API CInkCode
			{
			public:
				CInkCode ();
				CInkCode (const CInkCode & rhs);
				virtual ~CInkCode ();

				CInkCode & operator = (const CInkCode & rhs);

				typedef enum { EXP_PERIOD_1 = 0, EXP_PERIOD_12 = 1, EXP_PERIOD_18, EXP_PERIOD_24 } EXP_PERIOD;
				typedef enum { MFG_FOXJET = 0, MFG_DIAGRAPH } MFG;

				DECLARE_INK_CODE_MEMBER (Manufacturer);
				DECLARE_INK_CODE_MEMBER (Day);
				DECLARE_INK_CODE_MEMBER (SerialNumber);
				DECLARE_INK_CODE_MEMBER (ExpPeriod);
				DECLARE_INK_CODE_MEMBER (CRC);

				bool SetCRC () { return SetCRC (CInkCode::CalcCRC (* this)); }

				CString ToString () const;
				CString ToBinString () const;
				CString ToDebugString () const;
				CString Encode ();

				bool IsValid (const CTime * ptm = NULL) const;
				static bool IsValid (const CTime * ptm, __int32 nDay, __int32 nExpPeriod);

				bool FromBinString (const CString & str, const CTime * ptm = NULL);
				bool Decode (const CString & str, const CTime * ptm = NULL);

				static const INK_CODE m_range;

				operator const INK_CODE & () const { return m_code; }

				static unsigned __int32 CalcCRC (const CInkCode & c);
				static CString GetCRC (const CInkCode & c);
				static CString FormatString (CString str);

				static const CTime m_tmBase;
				static const CString m_strFormat;
				static const CString m_strFormatDay;

			protected:

				INK_CODE m_code;
			};

			/*
			typedef struct DB_API tagENABLE_PASSWORD
			{
				tagENABLE_PASSWORD ();

				unsigned __int32 m_nFactor1;
				unsigned __int32 m_nFactor2;

				int GetSize () const { return m_nFactor1 + m_nFactor2; }
			} ENABLE_PASSWORD;

			typedef struct DB_API tagDISABLE_PASSWORD
			{
				tagDISABLE_PASSWORD ();

				unsigned __int32 m_nExpPeriod;
				unsigned __int32 m_nFactor;

				int GetSize () const { return m_nExpPeriod + m_nFactor; }
			} DISABLE_PASSWORD;

			class DB_API CEnablePassword
			{
			public:
				CEnablePassword ();

				DECLARE_INK_CODE_MEMBER (Factor1);
				DECLARE_INK_CODE_MEMBER (Factor2);

				static std::vector <CEnablePassword> Generate (const CString & strMacAddress, int nPasswords, int);
				static std::vector <std::wstring> Generate (const CString & strMacAddress, int nPasswords);

				CString Encode () const;
				bool Decode (const CString & str);

				operator const ENABLE_PASSWORD & () const { return m_data; }

				friend inline bool operator == (const CEnablePassword & lhs, const CEnablePassword & rhs) 
				{ 
					const FoxjetDatabase::Encrypted::InkCodes::ENABLE_PASSWORD & r = rhs;
					const FoxjetDatabase::Encrypted::InkCodes::ENABLE_PASSWORD & l = lhs;
					return memcmp (&r, &l, sizeof (r)) == 0 ? true : false;
				}
				friend inline bool operator != (const CEnablePassword & lhs, const CEnablePassword & rhs) { return !(lhs == rhs); }

				static void Init (std::vector <int> & v, int n);
				static std::vector <int> m_v;

				operator CString () const { return Encode (); }
				operator std::wstring () const { return std::wstring (Encode ()); }

				static const ENABLE_PASSWORD m_range;

			protected:
				ENABLE_PASSWORD m_data;
			};

			class DB_API CDisablePassword
			{
			public:
				DECLARE_INK_CODE_MEMBER (ExpPeriod);
				DECLARE_INK_CODE_MEMBER (Factor);

				CString Encode () const;
				bool Decode (const CString & strIn, const CEnablePassword & pass, const CTime * ptm = NULL);
				bool SetExpPeriod (const CTime & tmExp);

				operator CString () const { return Encode (); }
				operator std::wstring () const { return std::wstring (Encode ()); }

				static std::vector <std::wstring> Generate (const CEnablePassword & master, int nPasswords);
				static std::vector <CDisablePassword> Generate (const CEnablePassword & pass, int nPasswords, int);

				operator const DISABLE_PASSWORD & () const { return m_data; }

				friend inline bool operator == (const CDisablePassword & lhs, const CDisablePassword & rhs) 
				{ 
					const FoxjetDatabase::Encrypted::InkCodes::DISABLE_PASSWORD & r = rhs;
					const FoxjetDatabase::Encrypted::InkCodes::DISABLE_PASSWORD & l = lhs;
					return memcmp (&r, &l, sizeof (r)) == 0 ? true : false;
				}
				friend inline bool operator != (const CDisablePassword & lhs, const CDisablePassword & rhs) { return !(lhs == rhs); }

				static const DISABLE_PASSWORD m_range;
			protected:
				DISABLE_PASSWORD m_data;
			};
			*/

			DB_API void Enable (bool bEnable);
			DB_API bool IsEnabled ();
			DB_API CString GetRegRootSection ();
			DB_API CString GetRegSection ();
			DB_API CString GetEnablePassword ();
			DB_API CString GetDisablePassword ();
			DB_API void SetLockout (const CTime * ptm = NULL);
			DB_API bool GetLockout (CTime & tm);
			DB_API void Restore (const CString & strFile);

			DB_API inline CString Scramble(const CString& str);
			DB_API inline CString Unscramble(const CString& str);
		};

		DB_API unsigned __int64 GetProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, unsigned __int64 nDefault);
		DB_API bool WriteProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, unsigned __int64 nData);

		DB_API __int64 GetProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, __int64 nDefault);
		DB_API bool WriteProfileInt64 (LPCTSTR szSection, LPCTSTR szKey, __int64 nData);

		DB_API bool WriteProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szData);
		DB_API CString GetProfileString (LPCTSTR szSection, LPCTSTR szKey, LPCTSTR szDefault);

		#define ENUMERATE_KEYS		0x01
		#define ENUMERATE_VALUES	0x02

		DB_API CString Enumerate (LPCTSTR lpszSection, std::vector <std::wstring> & v, DWORD dwFlags = ENUMERATE_KEYS | ENUMERATE_VALUES);
		DB_API CString GetInkCodes ();
		DB_API bool IsValidInkCode (const std::vector <std::wstring> & vRegistry, const CString & str);
		DB_API CString GetCommandLine (CString str = _T (""));

		DB_API void Delete (LPCTSTR lpszSection);

		DB_API std::vector <std::wstring> explode (CString str);

		void DB_API Init ();
		void DB_API Free ();
		DB_API bool SetNotify (HWND hwnd, UINT nMsg = 0);

		#ifdef _DEBUG
		DB_API void Trace (LPCTSTR lpszSection, int nDepth = 10);
		#endif

	}; //namespace Encrypted

	DB_API CString GetCmdlineValue (const CString & strCmdLine, const CString & strFlag);
}; // namespace FoxjetDatabase

inline bool operator == (const FoxjetDatabase::Encrypted::InkCodes::CInkCode & lhs, const FoxjetDatabase::Encrypted::InkCodes::CInkCode & rhs)
{
	const FoxjetDatabase::Encrypted::InkCodes::INK_CODE & r = rhs;
	const FoxjetDatabase::Encrypted::InkCodes::INK_CODE & l = lhs;

	return memcmp (&r, &l, sizeof (r)) == 0 ? true : false;
}

inline bool operator != (const FoxjetDatabase::Encrypted::InkCodes::CInkCode & lhs, const FoxjetDatabase::Encrypted::InkCodes::CInkCode & rhs) { return !(lhs == rhs); }


#endif //__REGISTRY_H__
