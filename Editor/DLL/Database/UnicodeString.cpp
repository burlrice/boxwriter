#include "StdAfx.h"
#include "UnicodeString.h"
#include "Rtl.h"
#include <string>
#include <vector>
#include <algorithm>
#include "AnsiString.h"
#include "TemplExt.h"
#include "Parse.h"


//////////////////////////////////////////////////////////////////////////////////////////

CUnicodeString::CUnicodeString (WCHAR * pwsz, ULONG lLen)
{
	Length = MaximumLength = lLen;
	Buffer = new WCHAR [MaximumLength];
	memset (Buffer, 0, MaximumLength);
	memcpy (Buffer, pwsz, lLen); //(lLen / 2) + 2);
}

CUnicodeString::CUnicodeString (const CString & str)
{
	MaximumLength = (str.GetLength () + 1) * 2;
	Buffer = new WCHAR [MaximumLength];
	memset (Buffer, 0, MaximumLength);
	Length = FoxjetDatabase::Unformat (str, Buffer) * 2;
}

CUnicodeString::CUnicodeString (const CUnicodeString & rhs)
{
	Length			= rhs.Length;
	MaximumLength	= rhs.MaximumLength;
	Buffer = new WCHAR [MaximumLength];
	memset (Buffer, 0, MaximumLength);
	memcpy (Buffer, rhs.Buffer, Length);
}

CUnicodeString::~CUnicodeString ()
{
	if (Buffer)
		delete [] (WCHAR *)Buffer;

	Length			= 0;
	MaximumLength	= 0;
	Buffer			= NULL;
}

CUnicodeString & CUnicodeString::operator = (const CUnicodeString & rhs)
{
	if (this != &rhs) {
		if (Buffer) {
			delete [] (WCHAR *)Buffer;
			Buffer = NULL;
		}

		Length			= rhs.Length;
		MaximumLength	= rhs.MaximumLength;
		Buffer = new WCHAR [MaximumLength];
		memset (Buffer, 0, MaximumLength);
		memcpy (Buffer, rhs.Buffer, MaximumLength);
	}

	return * this;
}

CUnicodeString::operator CString () const
{
	return FoxjetDatabase::Format (Buffer, Length / 2); 
}

CString CUnicodeString::ToString () const
{
	CString str = (CString)* this;

	return _T ("[") + FoxjetDatabase::ToString (Length) + _T ("] ") + str;
}

