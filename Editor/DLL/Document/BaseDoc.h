// BaseDoc.h: interface for the CBaseDoc class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BASEDOC_H__67DC5781_5B2A_4AAE_A376_F027AE9B09FD__INCLUDED_)
#define AFX_BASEDOC_H__67DC5781_5B2A_4AAE_A376_F027AE9B09FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxole.h>

#include "FileExt.h"
#include "TemplExt.h"
#include "BaseMembers.h"
#include "Version.h"
#include "Color.h"
#include "DocApi.h"

namespace FoxjetDocument
{
	// This identifier was generated to be statistically unique for your app.
	// You may change it if you prefer to choose a specific identifier.
	// {BC896075-ACA2-4E48-A057-F0CA63BF7B71}
	static const CLSID m_clsidMsg = { 0xbc896075, 0xaca2, 0x4e48, { 0xa0, 0x57, 0xf0, 0xca, 0x63, 0xbf, 0x7b, 0x71 } };
	static const UINT cfNewDocParams = ::RegisterClipboardFormat (_T ("FoxjetDocument::NewDocParams"));

	typedef enum 
	{
		NONE		= 0,
		ENABLED		= (1 << 1),
		MOVABLE		= (1 << 2),
		RESIZABLE	= (1 << 3),
	} HATCHEDRECTTYPE;

	void DOC_API DrawHatchedRect (CDC & dc, const CRect & rcDest, ULONG lType = ENABLED,
		COLORREF rgbFore = Color::rgbBlack, COLORREF rgbBack = Color::rgbWhite, int nWidth = 5, int nHandle = 5);

	// this class is for use with undo/redo operations
	class DOC_API CSnapshot : public virtual CBaseMembers
	{
	public:
		CSnapshot (const CBaseMembers & rhs);
		CSnapshot & operator = (const CBaseMembers & rhs);
		virtual ~CSnapshot ();
	private:
		CSnapshot ()								{ ASSERT (0); }					// no impl
		CSnapshot (const CSnapshot & rhs)			{ ASSERT (0); }					// no impl
		CSnapshot & operator = (CSnapshot & rhs)	{ ASSERT (0); return * this; }	// no impl
	};

	namespace Hint
	{
		typedef enum
		{ 
			NONE		= 0,
			UNITS		= (1 << 0), 
			READONLY	= (1 << 1), 
			VERSION		= (1 << 2), 
			ENUM_END	= VERSION,
			ALL			= 0xFFFFFFFF
		} HINT_TYPE;
	};

	class DOC_API CHint : public CObject, protected virtual CBaseMembers
	{
		DECLARE_DYNAMIC (CHint)
	public:

		CHint (const CBaseMembers * pNew, const CBaseMembers * pOld = NULL);
		virtual ~CHint ();

		ULONG GetType () const;
		bool IsType (ULONG lType) const;
			
		DECLARE_CONSTBASEACCESS (CBaseMembers)

	protected:
		ULONG m_lType;
		
	private:
		CHint ();								// no impl
		CHint (const CHint &);					// no impl
		CHint & operator = (const CHint &);		// no impl
	};


	class DOC_API CBaseDoc : public CDocument, private virtual CBaseMembers
	{
	public:

		DECLARE_CONSTBASEACCESS (CBaseMembers)

	protected:
		// protected constructor used by dynamic creation
		CBaseDoc(FoxjetFile::DOCTYPE type = FoxjetFile::UNKNOWNDOCTYPE);

		DECLARE_DYNCREATE(CBaseDoc)

	// Attributes
	public:
		virtual ~CBaseDoc ();

		virtual CBaseDoc & Copy (const CSnapshot * pSnapshot, CSnapshot * pNext = NULL);
		virtual CSnapshot * GetSnapshot ();
		void AddToUndoList (const CSnapshot * pSnapshot);

		virtual bool LoadFromFile(LPCTSTR pszName, FoxjetCommon::CVersion & ver);
		virtual bool StoreToFile(LPCTSTR pszName);
		virtual void SetPathName(LPCTSTR lpszPathName, BOOL bAddToMRU);
		virtual bool Save();
		virtual void SetModifiedFlag (bool bModified, const CSnapshot * pPreModify);
		virtual void SetModifiedFlag (const CSnapshot * pPreModify);
		virtual bool SetUnits (UNITS units);
		virtual bool SetReadOnly (bool bReadOnly);
		virtual bool GetReadOnly () const { return m_bReadOnly; }
		virtual bool SetVersion (const FoxjetCommon::CVersion & ver);
		virtual void OnUpdateDocView ();
		void UpdateTemplate (CDocTemplate * p);


		virtual bool SetDatabase (FoxjetDatabase::COdbcDatabase * pDatabase);
		virtual FoxjetDatabase::COdbcDatabase * GetDatabase () const;

		virtual const FoxjetDatabase::HEADSTRUCT * GetSelectedHead () const;

		virtual CString GetDesc () const { return _T (""); }
		virtual bool SetDesc (const CString & str) { return false; }

		void DeleteEditVector (CArray <const CSnapshot *, const CSnapshot *> &v);
		CString GetPathName ();
		UNITS GetUnits () const { return m_units; }
				
		FoxjetCommon::CVersion GetVersion () const { return m_version; }
		bool SetUndoDepth (int nDepth);
		int GetUndoDepth () const { return m_nUndoDepth; }
		FoxjetFile::DOCTYPE GetType () const { return m_type; }

		void OnReadOnly(); 
		void OnEditUndo(); 
		void OnEditRedo(); 
		void OnUpdateEditUndo(CCmdUI* pCmdUI);
		void OnUpdateEditRedo(CCmdUI* pCmdUI);
		void OnUpdateFileSave(CCmdUI *pCmdUI);
		void OnUpdateFileSaveAs(CCmdUI *pCmdUI);
		void OnUpdateDebuggingstuffReadonly(CCmdUI* pCmdUI);

		const CArray <const CSnapshot *, const CSnapshot *>	& GetRedo () const { return m_vRedo; }
		const CArray <const CSnapshot *, const CSnapshot *>	& GetUndo () const { return m_vUndo; }

	protected:
		
		int														m_nUndoDepth;
		const FoxjetFile::DOCTYPE								m_type;
		CArray <const CSnapshot *, const CSnapshot *>			m_vRedo;
		CArray <const CSnapshot *, const CSnapshot *>			m_vUndo;

	// Operations
	public:

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CBaseDoc)
		public:
		virtual void DeleteContents();
		virtual BOOL OnNewDocument();
		virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
		virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
		protected:
		virtual BOOL SaveModified();
		//}}AFX_VIRTUAL

	// Implementation
	public:
		CBitmap * GetClipboardBitmap() const;
		CBitmap * GetClipboardBackground ();
		bool SetClipboardBackground (HBITMAP hBmp);
		bool SetClipboardParams (const CString & strParams) const;
		CString GetClipboardParams () const;
		bool PromptSaveModified () const;

	#ifdef _DEBUG
		virtual void AssertValid() const;
		virtual void Dump(CDumpContext& dc) const;
	#endif

	private:
		CBitmap * m_pBmpClipboard;
		bool m_bSaveModified;

		// Generated message map functions
	protected:

		//{{AFX_MSG(CBaseDoc)
		afx_msg void OnSave ();
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	};
}; //namespace FoxjetDocument

#endif // !defined(AFX_BASEDOC_H__67DC5781_5B2A_4AAE_A376_F027AE9B09FD__INCLUDED_)
