// Document.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include <shlwapi.h>
#include "Document.h"
#include "Debug.h"
#include "DllVer.h"
#include "AppVer.h"

using namespace FoxjetCommon;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static HINSTANCE hInstance = NULL;

BOOL APIENTRY DllMain (HANDLE hModule, DWORD dwReason, LPVOID lpReserved)
{
	switch (dwReason) {
	case DLL_THREAD_ATTACH:
		::hInstance = (HINSTANCE)hModule;
		break;
	case DLL_PROCESS_ATTACH:
		::hInstance = (HINSTANCE)hModule;
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}

    return TRUE;
}

void CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer)
{
	if (lpVer) 
		* lpVer = FoxjetCommon::verApp; // memcpy (lpVer, &FoxjetCommon::verApp, sizeof (FoxjetCommon::VERSIONSTRUCT));
}

HRESULT CALLBACK DllGetVersion (DLLVERSIONINFO * pdvi)
{
	FoxjetCommon::VERSIONSTRUCT ver;

	fj_GetDllVersion (&ver);

	if (pdvi) {
		pdvi->dwMajorVersion	= ver.m_nMajor;
		pdvi->dwMinorVersion	= ver.m_nMinor;
		pdvi->dwBuildNumber		= ver.m_nInternal;
		pdvi->dwPlatformID		= DLLVER_PLATFORM_WINDOWS;
	}

	return NOERROR;
}

CString FoxjetDocument::LoadString (UINT nID)
{
	return FoxjetDatabase::LoadString (nID, _T ("Document.dll"));
}

CWinApp & FoxjetDocument::GetApp ()
{
	CWinApp * pApp = ::AfxGetApp ();
	ASSERT (pApp);
	return * pApp;
}