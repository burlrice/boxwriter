#include "document.h"
#include "stdafx.h"
#include "BaseDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CSnapshot::CSnapshot (const CBaseMembers & rhs)
:	CBaseMembers (rhs)
{
}

CSnapshot & CSnapshot::operator = (const CBaseMembers & rhs)
{
	CBaseMembers::operator = (rhs);
	return * this;
}

CSnapshot::~CSnapshot ()
{
}

