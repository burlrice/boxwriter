#ifndef __EXTERN_H__
#define __EXTERN_H__

#include "ElementDefaults.h"
#include "OdbcDatabase.h"

namespace DocumentGlobals
{
	extern FoxjetCommon::CElementDefaults & defElements;
}; //namespace DocumentGlobals

#endif //__EXTERN_H__
