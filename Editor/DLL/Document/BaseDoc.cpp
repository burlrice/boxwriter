// BaseDoc.cpp: implementation of the CBaseDoc class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <math.h>

#include "Document.h"
#include "BaseDoc.h"
#include "BaseView.h"
#include "Resource.h"
#include "Utils.h"
#include "MsgBox.h"
#include "BaseDocTemplate.h"


using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void DOC_API FoxjetDocument::DrawHatchedRect (CDC & dc, const CRect & rcDest, ULONG lType, COLORREF rgbFore, COLORREF rgbBack, int nWidth, int nHandle)
{
	CBrush brush;
	CRect rc (rcDest);
	CRgn rgnInner, rgnOutter;
	int nBkMode = dc.SetBkMode (TRANSPARENT);
	COLORREF rgbOldBack = dc.SetBkColor (rgbBack);
	
	brush.CreateHatchBrush (HS_BDIAGONAL, rgbFore);
	rgnInner.CreateRectRgn (rc.left, rc.top, rc.right, rc.bottom);
	rc.InflateRect (nWidth, nWidth);
	rgnOutter.CreateRectRgn (rc.left, rc.top, rc.right, rc.bottom);
	rgnOutter.CombineRgn (&rgnInner, &rgnOutter, RGN_XOR);

	dc.FillRgn (&rgnOutter, &brush);
	dc.FrameRect (rcDest, &CBrush (rgbFore));

	if (lType & ENABLED) {
		double dPoint [][2] = 
		{
			{ rc.left,												rc.top,												},
			{ (double)(rc.left + (rc.Width () - nHandle)) / 2.0,	rc.top,												},
			{ rc.right - nHandle,									rc.top,												},
			{ rc.right - nHandle,									(double)(rc.top + (rc.Height () - nHandle)) / 2.0,	},
			{ rc.right - nHandle,									rc.bottom - nHandle - 1,							},
			{ (double)(rc.left + (rc.Width () - nHandle)) / 2.0,	rc.bottom - nHandle - 1,							},
			{ rc.left,												rc.bottom - nHandle - 1,							},
			{ rc.left,												(double)(rc.top + (rc.Height () - nHandle)) / 2.0,	},
		};

		for (int i = 0; i < 8; i++) {
			if (lType & RESIZABLE || !(i % 2)) {
				CPoint pt ((int)floor (dPoint [i][0]), (int)floor (dPoint [i][1]));

				if (lType & MOVABLE) 
					dc.FillSolidRect (CRect (0, 0, nHandle + 1, nHandle + 1) + pt, rgbFore);
				else {
					dc.FillSolidRect (CRect (0, 0, nHandle + 1, nHandle + 1) + pt, rgbBack);
					dc.FrameRect (CRect (0, 0, nHandle + 1, nHandle + 1) + pt, &CBrush (rgbFore));
				}
			}
		}
	}

	dc.SetBkMode (nBkMode);
	dc.SetBkColor (rgbOldBack);
}


IMPLEMENT_DYNCREATE(CBaseDoc, CDocument)

CBaseDoc::CBaseDoc(FoxjetFile::DOCTYPE type)
:	m_nUndoDepth (100),
	m_type (type),
	m_pBmpClipboard (NULL),
	m_bSaveModified (true),
	CBaseMembers (),
	CDocument ()
{
	TRACEF (_T ("CBaseDoc::CBaseDoc ()"));
}

CBaseDoc::~CBaseDoc()
{
	DeleteEditVector (m_vUndo);
	DeleteEditVector (m_vRedo);

	if (m_pBmpClipboard) {
		delete m_pBmpClipboard;
		m_pBmpClipboard = NULL;
	}
}

CBaseDoc & CBaseDoc::Copy (const CSnapshot * pSnapshot,
						   FoxjetDocument::CSnapshot * pNext)
{
	ASSERT (pSnapshot);

	if ((* this) != (* pSnapshot)) {
		CHint hint (pSnapshot, this);

		CBaseMembers::operator = (* pSnapshot);

		SetReadOnly (pSnapshot->m_bReadOnly);
		SetUnits (pSnapshot->m_units);
		//SetVersion (pSnapshot->m_version);
		UpdateAllViews (NULL, 0, &hint);
	}

	return * this;
}

BEGIN_MESSAGE_MAP(CBaseDoc, CDocument) //CDocument)
	//{{AFX_MSG_MAP(CBaseDoc)
	ON_COMMAND(ID_FILE_SAVE, OnSave) 
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_COMMAND(ID_EDIT_REDO, OnEditRedo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_REDO, OnUpdateEditRedo)
	ON_COMMAND(ID_DEBUGGINGSTUFF_READONLY, OnReadOnly)
	ON_UPDATE_COMMAND_UI(ID_DEBUGGINGSTUFF_READONLY, OnUpdateDebuggingstuffReadonly)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnUpdateFileSaveAs)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CBaseDoc::DeleteContents() 
{
	DeleteEditVector (m_vUndo);
	DeleteEditVector (m_vRedo);
	UpdateAllViews (NULL, 0, &CHint (this));
	SetModifiedFlag (false, NULL);

	CDocument::DeleteContents();
}

BOOL CBaseDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	SetUnits (defElements.GetUnits ());

	return TRUE;
}

bool CBaseDoc::LoadFromFile(LPCTSTR pszName, FoxjetCommon::CVersion & ver)
{
	return false;
}

bool CBaseDoc::StoreToFile(LPCTSTR pszName)
{
	return false;
}

bool CBaseDoc::SetUnits(UNITS units)
{
	m_units = units;
	return true;
}

void CBaseDoc::OnUpdateEditUndo(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable (m_vUndo.GetSize () > 0);	
}

void CBaseDoc::OnUpdateEditRedo(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable (m_vRedo.GetSize () > 0);	
}

void CBaseDoc::OnEditUndo() 
{
	BeginWaitCursor ();
	
	ASSERT (m_vUndo.GetSize () > 0);

	if (m_vUndo.GetSize () > 0) {
		int nIndex = m_vUndo.GetSize () - 1;
		const CSnapshot * p = m_vUndo [nIndex];
		
		ASSERT (p);
		CSnapshot * pRedo = GetSnapshot ();
		Copy (p, pRedo);
		m_vRedo.Add (pRedo);
		m_vUndo.RemoveAt (nIndex);
		
		delete p;
	//	SetModifiedFlag (m_vUndo.GetSize () ? true : false, NULL);
	}
	
	EndWaitCursor ();
}

void CBaseDoc::OnEditRedo() 
{
	BeginWaitCursor ();
	
	ASSERT (m_vRedo.GetSize () > 0);

	if (m_vRedo.GetSize () > 0) {
		int nIndex = m_vRedo.GetSize () - 1;
		const CSnapshot * p = m_vRedo [nIndex];

		ASSERT (p);
		CSnapshot * pUndo = GetSnapshot ();
		Copy (p, pUndo);
		m_vUndo.Add (pUndo);
		m_vRedo.RemoveAt (nIndex);
		
		delete p;
	//	SetModifiedFlag (true, NULL);
	}
	
	EndWaitCursor ();
}

void CBaseDoc::OnUpdateFileSave(CCmdUI *pCmdUI)
{
	pCmdUI->Enable (!m_bReadOnly);
}

void CBaseDoc::OnUpdateFileSaveAs(CCmdUI *pCmdUI)
{
	pCmdUI->Enable (!m_bReadOnly);
}

CSnapshot * CBaseDoc::GetSnapshot()
{
	return new CSnapshot (* this);
}

/////////////////////////////////////////////////////////////////////////////
// CBaseDoc diagnostics

void CBaseDoc::OnUpdateDebuggingstuffReadonly(CCmdUI* pCmdUI) 
{
	#ifdef _DEBUG
	pCmdUI->Enable (true);
	pCmdUI->SetCheck (GetReadOnly () ? 1 : 0);
	#else
	pCmdUI->Enable (false);
	#endif //_DEBUG
}

void CBaseDoc::OnReadOnly() 
{
	#ifdef _DEBUG
	SetReadOnly (!GetReadOnly ());
	#endif //_DEBUG
}

#ifdef _DEBUG
void CBaseDoc::AssertValid() const
{
//	CDocument::AssertValid();
	CDocument::AssertValid();
}

void CBaseDoc::Dump(CDumpContext& dc) const
{
//	CDocument::Dump(dc);
	CDocument::Dump(dc);
}
#endif //_DEBUG

bool CBaseDoc::SetUndoDepth (int nDepth)
{
	if (nDepth > 0) {
		m_nUndoDepth = nDepth;
		return true;
	}

	return false;
}

void CBaseDoc::SetPathName(LPCTSTR lpszPathName, BOOL bAddToMRU)
{
	using namespace FoxjetFile;
	const CString strRoot = FoxjetFile::GetRootPath ();

	if (lpszPathName && _tcslen (lpszPathName)) 
		ASSERT (m_type == FoxjetFile::GetFileType (lpszPathName));

	m_strPathName = lpszPathName;

	if (m_strPathName.Find (strRoot) == -1)  
		m_strPathName = strRoot + m_strPathName;

	CString strTitle = 
		GetFileHead (m_strPathName) + _T ("\\") +
		GetFileTitle (m_strPathName);
	SetTitle (strTitle);

	bAddToMRU &= GetFileType (m_strPathName) == TASK;

	if (bAddToMRU) 
		GetApp ().AddToRecentFileList (m_strPathName);
}

void CBaseDoc::AddToUndoList (const CSnapshot * pSnapshot)
{
	if (pSnapshot) {
		DeleteEditVector (m_vRedo);
		m_vUndo.Add (pSnapshot);

		while (m_vUndo.GetSize () > m_nUndoDepth && m_vUndo.GetSize () > 0) {
			const CSnapshot * p = m_vUndo [0];
			m_vUndo.RemoveAt (0);
			delete p;
		}
	}
}

void CBaseDoc::DeleteEditVector (CArray <const CSnapshot *, const CSnapshot *> &v)
{
	for (int i = 0; i < v.GetSize (); i++) {
		const CSnapshot * p = v [i];
		delete p;
	}

	v.RemoveAll ();
}

bool CBaseDoc::Save() 
{
	bool bSaved = false;

	if (!GetReadOnly ()) {
		CString strPath = GetPathName ();

		bSaved = StoreToFile (strPath);

		if (bSaved) 
			SetModifiedFlag (NULL, false);

		if (!bSaved) {
			CString str;

			str.Format (LoadString (IDS_TASKSAVEFAILED), GetTitle ());
			MsgBox (str, MB_ICONWARNING);
		}
		else {
			TRACEF ("Saved to " + strPath);
		}
	}

	return bSaved;
}

void CBaseDoc::OnSave() 
{
	if (!Save ()) {
		TRACEF (_T ("Save failed"));
	}
}

CString CBaseDoc::GetPathName ()
{
//	CString strPath = CDocument::GetPathName ();
	CString strPath = CDocument::GetPathName ();

	CString strPrefix = FoxjetFile::GetFileHead (strPath);
	CString strTitle = FoxjetFile::GetFileTitle (strPath);
	CString strFile = FoxjetFile::GetRootPath ()
		+ strPrefix + _T ("\\") 
		+ strTitle + _T (".") 
		+ FoxjetFile::GetFileExt (m_type);

	return strFile;
}


void CBaseDoc::UpdateTemplate (CDocTemplate * p)
{
	m_pDocTemplate = p;
}


void CBaseDoc::SetModifiedFlag (bool bModified, const CSnapshot * pPreModify)
{
	CString strTitle = GetTitle ();
	const CString strSuffix = FoxjetFile::GetModifiedSuffix ();

	if (bModified) { ASSERT (!m_bReadOnly); }

	CDocument::SetModifiedFlag (bModified);
	OnUpdateDocView ();

	if (bModified) {
		if (strTitle.Find (strSuffix) == -1)
			SetTitle (strTitle + strSuffix);

		if (pPreModify) 
			AddToUndoList (pPreModify);
	}
	else {
		if (strTitle.Replace (strSuffix, _T ("")))
			SetTitle (strTitle);
	}
}

void CBaseDoc::SetModifiedFlag (const CSnapshot * pPreModify)
{
	SetModifiedFlag (true, pPreModify);
}

bool CBaseDoc::PromptSaveModified () const
{
	return m_bSaveModified;
}

BOOL CBaseDoc::SaveModified() 
{
	BOOL bResult = TRUE;

	if (!GetReadOnly ()) {
		const CString strTitle = GetTitle ();
		CString strTmpTitle = strTitle;
		
		m_bSaveModified = false;

		if (strTmpTitle.Replace (FoxjetFile::GetModifiedSuffix (), _T (""))) 
			SetTitle (strTmpTitle);	

		bResult = CDocument::SaveModified();

		if (!bResult)
			SetTitle (strTitle);

		m_bSaveModified = true;
	}

	return bResult;
}

bool CBaseDoc::SetReadOnly (bool bReadOnly)
{
	m_bReadOnly = bReadOnly;
	return true;
}

bool CBaseDoc::SetVersion (const FoxjetCommon::CVersion & ver)
{
	m_version = ver;
	return true;
}

BOOL CBaseDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	CVersion ver = GetElementListVersion (); 
	CString strName = FoxjetFile::GetRelativeName (lpszPathName);
	CString strTask = FoxjetFile::GetFileTitle (strName);
	
	TRACEF (_T ("CBaseDoc::OnOpenDocument: ") + CString (lpszPathName) + _T (" --> ") + strName + _T (", ") + strTask);
	DeleteContents ();

	if (strTask.GetLength ()) {
		ASSERT (IsValidFileTitle (strTask));

		bool bStatus = LoadFromFile (strName, ver);
		return bStatus ? TRUE : FALSE;
	}

	return TRUE;
}

BOOL CBaseDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	CString strName = FoxjetFile::GetRelativeName (lpszPathName);
	bool bStatus = StoreToFile (strName);

	if (bStatus) {
		SetPathName (strName, FALSE);
		SetModifiedFlag (false, NULL);
	}

	return bStatus ? TRUE : FALSE;
}

bool CBaseDoc::SetDatabase (FoxjetDatabase::COdbcDatabase * pDatabase)
{
	return false;
}

FoxjetDatabase::COdbcDatabase * CBaseDoc::GetDatabase () const
{
	return NULL;
}

CString CBaseDoc::GetClipboardParams() const
{
	CString str;

	if (::OpenClipboard (NULL)) {
		str = (LPCTSTR)::GetClipboardData (FoxjetDocument::cfNewDocParams);
//		::SetClipboardData (FoxjetDocument::cfNewDocParams, NULL);
		::CloseClipboard ();
	}
	
	return str;
}

bool CBaseDoc::SetClipboardParams(const CString &strParams) const
{
	bool bResult = false;
	HGLOBAL hClipboardText = ::GlobalAlloc (GMEM_MOVEABLE | GMEM_DDESHARE | GMEM_ZEROINIT,
		strParams.GetLength () + 1);
	LPVOID lpCbTextData = ::GlobalLock (hClipboardText);
	memcpy (lpCbTextData, (LPCTSTR)strParams, strParams.GetLength ());

	TRACEF ("SetClipboardParams:\n\t" + strParams);
	VERIFY (::OpenClipboard (NULL));
	VERIFY (::SetClipboardData (FoxjetDocument::cfNewDocParams, hClipboardText));
	VERIFY (::CloseClipboard ());

	if (hClipboardText) {
		::GlobalUnlock (hClipboardText);
		bResult = true;
	}

	return bResult;
}

bool CBaseDoc::SetClipboardBackground(HBITMAP hBmp)
{
	m_pBmpClipboard = GetClipboardBitmap ();

	VERIFY (::OpenClipboard (NULL));
	VERIFY (::SetClipboardData (CF_BITMAP, hBmp));
	VERIFY (::CloseClipboard ());

	return true;
}
/*
bool CBaseDoc::SetClipboardBackground(CDC & dc, const CRect &rc)
{
	HDC hdcMem = ::CreateCompatibleDC (dc);
	HBITMAP hBmp = ::CreateCompatibleBitmap (dc, rc.Width (), rc.Height ());
	HGDIOBJ hSelect = ::SelectObject (hdcMem, hBmp);

	m_pBmpClipboard = GetClipboardBitmap ();

	::BitBlt (hdcMem, 0, 0, rc.Width (), rc.Height (),
		dc, rc.left, rc.top, SRCCOPY);

	VERIFY (::OpenClipboard (NULL));
	VERIFY (::SetClipboardData (CF_BITMAP, hBmp));
	VERIFY (::CloseClipboard ());

	::SelectObject (hdcMem, hSelect);
	::DeleteDC (hdcMem);
	return true;
}
*/

CBitmap * CBaseDoc::GetClipboardBitmap() const
{
	CBitmap * pBmp = new CBitmap ();
	CDC * pDC = ::AfxGetMainWnd ()->GetDC ();
	CDC dcClipboard, dcMem;

	dcMem.CreateCompatibleDC (pDC);
	dcClipboard.CreateCompatibleDC (pDC);

	VERIFY (::OpenClipboard (NULL));
	HBITMAP hBmp = (HBITMAP)::GetClipboardData (CF_BITMAP);

	if (hBmp) {
		DIBSECTION ds;
		::ZeroMemory (&ds, sizeof (ds));
		VERIFY (::GetObject (hBmp, sizeof (DIBSECTION), &ds));
		CSize size = CSize (ds.dsBm.bmWidth, ds.dsBm.bmHeight);
		CBitmap * pOldClipboard = dcClipboard.SelectObject (CBitmap::FromHandle (hBmp));
		VERIFY (::CloseClipboard ());

		pBmp->CreateCompatibleBitmap (pDC, size.cx, size.cy);
		CBitmap * pOldMem = dcMem.SelectObject (pBmp);

		::BitBlt (dcMem, 0, 0, size.cx, size.cy, 
			dcClipboard, 0, 0, SRCCOPY);

		dcMem.SelectObject (pOldMem);
		dcClipboard.SelectObject (pOldClipboard);
	}

	return pBmp;
}

CBitmap * CBaseDoc::GetClipboardBackground() 
{
	CBitmap * pBmp = GetClipboardBitmap ();

	VERIFY (::OpenClipboard (NULL));

	if (m_pBmpClipboard) {
		VERIFY (::SetClipboardData (CF_BITMAP, * m_pBmpClipboard));
		delete m_pBmpClipboard;
		m_pBmpClipboard = NULL;
	}
	else
		::SetClipboardData (CF_BITMAP, NULL);
	
	VERIFY (::CloseClipboard ());

	return pBmp;
}

void CBaseDoc::OnUpdateDocView()
{

}

const FoxjetDatabase::HEADSTRUCT * CBaseDoc::GetSelectedHead () const
{
	ASSERT (0);
	return NULL;
}
