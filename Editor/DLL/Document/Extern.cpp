#include "stdafx.h"
#include "extern.h"

#if defined(  __IPPRINTER__ )
	#pragma include_alias( "ListGlobals.h",			"..\..\ElementList\IP\Extern.h" )
#elif defined( __WINPRINTER__ )
	#pragma include_alias( "ListGlobals.h",			"..\..\ElementList\Win\Extern.h" )
#else
	#error Document\Extern.h: Incorrect configuration
#endif 

#include "ListGlobals.h"

FoxjetCommon::CElementDefaults & DocumentGlobals::defElements = ListGlobals::defElements;

