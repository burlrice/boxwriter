#include "document.h"
#include "stdafx.h"
#include "BaseDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDocument;

IMPLEMENT_DYNAMIC (CHint, CObject);

CHint::CHint (const CBaseMembers * pNew, const CBaseMembers * pOld)
:	m_lType (NONE)
{
	ASSERT (pNew);

	CBaseMembers::operator = (* pNew);

	if (!pOld)
		m_lType = Hint::ALL;
	else {
		if (pNew->m_units != pOld->m_units)
			m_lType |= Hint::UNITS;
		if (pNew->m_version != pOld->m_version)
			m_lType |= Hint::VERSION;
		if (pNew->m_bReadOnly != pOld->m_bReadOnly) 
			m_lType |= Hint::READONLY;
	}
}

CHint::~CHint ()
{
}

ULONG CHint::GetType () const
{
	return m_lType;
}

bool CHint::IsType (ULONG lType) const
{
	return (lType & m_lType) != 0;
}

