// BaseDocTemplate.cpp: implementation of the CBaseDocTemplate class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Document.h"
#include "BaseDocTemplate.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNAMIC (CBaseDocTemplate, CMultiDocTemplate)

CBaseDocTemplate::CBaseDocTemplate (UINT nIDResource, CRuntimeClass* pDocClass,
	CRuntimeClass* pFrameClass, CRuntimeClass* pViewClass,
	FoxjetFile::DOCTYPE	type)
:	m_type (type), 
	CMultiDocTemplate (nIDResource, pDocClass, pFrameClass, pViewClass)
{
}
CBaseDocTemplate::~CBaseDocTemplate ()
{
}

BOOL CBaseDocTemplate::GetDocString (CString & str, enum DocStringIndex index) const
{
	BOOL bResult = CMultiDocTemplate::GetDocString (str, index);
	CString strHead = FoxjetFile::GetFileHead (str);

	switch (index) {
	case CMultiDocTemplate::windowTitle:
	case CMultiDocTemplate::docName:
		str = GetDefPrefix () + _T ("\\") + FoxjetFile::GetFileTitle (str);
		break;
	case CDocTemplate::filterName:
		str = FoxjetFile::GetFileExt (m_type);
		break;
	}

	if (index == CDocTemplate::windowTitle) 
		str = _T ("[") + str + _T ("]");

	return bResult;
}

bool CBaseDocTemplate::IsExistingDoc (const CString & str) const
{
	const CString strTitle = FoxjetFile::GetRelativeName (str);

	if (GetOpenDoc (strTitle))
		return true;

	return false;
}

CDocument * CBaseDocTemplate::GetOpenDoc (const CString & str) const
{
	const CString strTitle = FoxjetFile::GetRelativeName (str);
	POSITION pos = GetFirstDocPosition ();

	while (pos != NULL) {
		CDocument * pDoc = GetNextDoc (pos);
		CString strCompare = FoxjetFile::GetRelativeName (pDoc->GetTitle ());

		if (!strCompare.CompareNoCase (strTitle)) 
			return pDoc;
	}

	return NULL;
}

void CBaseDocTemplate::UpdateDocument(CDocument* pDoc)
{
	if (m_docList.Find (pDoc, NULL) == NULL)
		m_docList.AddTail (pDoc);
}

void CBaseDocTemplate::UpdateDocStrings (const CString & strDir, const CString & strFile)
{
	/*
	Message document
	LINE001\Untitled
	Untitled
	Messages (*.tsk)
	.tsk
	Mkdraw.Task
	Blank Message
	*/

	m_strDocStrings.Replace (_T ("LINE001"), strDir);
	m_strDocStrings.Replace (_T ("Untitled"), strFile);
	m_strDocStrings.Replace (_T ("Blank Message"), strFile);

	TRACEF (m_strDocStrings);
	m_strTitle = strFile;
	m_strDesc = strDir;
}

CString CBaseDocTemplate::GetNextDefaultName () const
{
	int nTries = 0;
	CString strDocName, strTitle;

	GetDocString (strDocName, CDocTemplate::docName);strTitle = strDocName;

	while (nTries < UINT_MAX) {
		if (nTries > 50) {
			TRACEF (_T ("(nTries > 50)"));
		}

		bool bExists = IsExistingDoc (strTitle);

		if (!bExists)
			return strTitle + _T (".") + FoxjetFile::GetFileExt (m_type);
		else {
			CString str;

			str.Format (_T ("%u"), ++nTries);
			strTitle = strDocName + str;
		}
	}

	return strTitle + _T (".") + FoxjetFile::GetFileExt (m_type);
}

void CBaseDocTemplate::SetDefaultTitle (CDocument * pDocument)
{
	CString str = GetNextDefaultName ();
	CString strPrefix = FoxjetFile::GetFileHead (str);
	CString strTitle = FoxjetFile::GetFileTitle (str);
	pDocument->SetTitle (strPrefix + _T ("\\") + strTitle);
}

CDocument * CBaseDocTemplate::CreateNewDocument ()
{
	CDocument * p = CMultiDocTemplate::CreateNewDocument ();
	ASSERT (p);
	return p;
}
