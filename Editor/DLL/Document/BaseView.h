#if !defined(AFX_BASEVIEW_H__12797B7A_452B_4AC9_BF23_73CFF3F8FB09__INCLUDED_)
#define AFX_BASEVIEW_H__12797B7A_452B_4AC9_BF23_73CFF3F8FB09__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BaseView.h : header file
//

#include <afxwin.h>

#include "DocApi.h"
#include "ZoomDlg.h"

namespace FoxjetDocument
{
	class DOC_API CBaseDoc;

	class DOC_API CBaseView : public CScrollView
	{
	protected:
		CBaseView();           // protected constructor used by dynamic creation
		DECLARE_DYNCREATE(CBaseView)

	public:
		CSize GetLogicalSize () const;
		void Invalidate( BOOL bErase = TRUE );
		CPoint GetOffset () const;
		CBaseDoc * GetDocument () const;

		static const UINT m_nDefineMenuIDs [];

		// GetPageSize should really be pure virtual, but 
		// DECLARE_DYNCREATE won't allow it
		virtual void SetZoomStatus ();
		virtual bool SetZoom (double dZoom, bool bAutoScroll = false);
		virtual double GetZoom () const;
		virtual CSize GetPageSize () const;
		virtual void InitMenu (CMenu * pMenu);
		virtual CBitmap * CreateBitmap (CDC & dc);
		virtual void InitDefineMenu (CMenu * pMenu);
		virtual void InitContextMenu (CMenu * pMenu);
		virtual void OnDrawTrackerRect (CDC * pDC, const CRect & rc, DWORD dwElement);


		afx_msg void OnZoomDeltaPosChanged (NMHDR * pnmh, LRESULT * pResult);
		afx_msg void OnViewZoomin();
		afx_msg void OnViewZoomout();
		afx_msg void OnViewZoomnormal100();
		afx_msg void OnUpdateZoom (CCmdUI * pCmdUI);
		afx_msg void OnViewZoomCustom();

		static bool CALLBACK ZoomFct (FoxjetCommon::ZOOMSTRUCT & zs);

	private:
		double m_dZoom;

	protected:

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CBaseView)
		protected:
		virtual void OnDraw(CDC* pDC);      // overridden to draw this view
		virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
		//}}AFX_VIRTUAL

	// Implementation
	protected:
		virtual ~CBaseView();
	#ifdef _DEBUG
		virtual void AssertValid() const;
		virtual void Dump(CDumpContext& dc) const;
	#endif

		// Generated message map functions
	protected:
		//{{AFX_MSG(CBaseView)
			// NOTE - the ClassWizard will add and remove member functions here.
		//}}AFX_MSG
		DECLARE_MESSAGE_MAP()
	private:
		CBitmap * m_pBmp;
	};
}; //namespace FoxjetDocument

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BASEVIEW_H__12797B7A_452B_4AC9_BF23_73CFF3F8FB09__INCLUDED_)
