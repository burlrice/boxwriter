// Document.h : main header file for the DOCUMENT DLL
//

#if !defined(AFX_DOCUMENT_H__47F581F8_8913_45DC_BF1E_2C3BA58A0BE0__INCLUDED_)
#define AFX_DOCUMENT_H__47F581F8_8913_45DC_BF1E_2C3BA58A0BE0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "Version.h"

namespace FoxjetDocument
{
	CString LoadString (UINT nID);
	CWinApp & GetApp ();
}; //namespace FoxjetDocument

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DOCUMENT_H__47F581F8_8913_45DC_BF1E_2C3BA58A0BE0__INCLUDED_)
