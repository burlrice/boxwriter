// BaseView.cpp : implementation file
//

#include "stdafx.h"
#include "Document.h"
#include "BaseView.h"
#include "BaseDoc.h"
#include "Resource.h"
#include "ZoomDlg.h"
#include "Coord.h"
#include "Color.h"
#include "AnsiString.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;

const UINT CBaseView::m_nDefineMenuIDs [] = {
	ID_DEFINE_01, ID_DEFINE_02, ID_DEFINE_03, ID_DEFINE_04,
	ID_DEFINE_05, ID_DEFINE_06, ID_DEFINE_07, ID_DEFINE_08,
	ID_DEFINE_09, ID_DEFINE_10, ID_DEFINE_11, ID_DEFINE_12,
	ID_DEFINE_13, ID_DEFINE_14, ID_DEFINE_15, ID_DEFINE_16,
	ID_DEFINE_17, ID_DEFINE_18, ID_DEFINE_19, ID_DEFINE_20,
	ID_DEFINE_21, ID_DEFINE_22, ID_DEFINE_23, ID_DEFINE_24,
	ID_DEFINE_25, ID_DEFINE_26, ID_DEFINE_27, ID_DEFINE_28,
	ID_DEFINE_29, ID_DEFINE_30,
	-1
};

/////////////////////////////////////////////////////////////////////////////
// CBaseView

IMPLEMENT_DYNCREATE(CBaseView, CScrollView)

CBaseView::CBaseView()
:	m_dZoom (1.0),
	m_pBmp (NULL)
{
	m_nMapMode = MM_TEXT; // TODO
}

CBaseView::~CBaseView()
{
	if (m_pBmp)
		delete m_pBmp;
}


BEGIN_MESSAGE_MAP(CBaseView, CScrollView)
	//{{AFX_MSG_MAP(CBaseView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_VIEW_ZOOMIN, OnViewZoomin)
	ON_COMMAND(ID_VIEW_ZOOMOUT, OnViewZoomout)
	ON_COMMAND(ID_VIEW_ZOOMNORMAL100, OnViewZoomnormal100)
	ON_COMMAND(ID_VIEW_ZOOM_CUSTOM, OnViewZoomCustom)

	ON_UPDATE_COMMAND_UI(ID_STATUS_ZOOM, OnUpdateZoom)
END_MESSAGE_MAP()

CBaseDoc * CBaseView::GetDocument () const
{
	CDocument * pDoc = CScrollView::GetDocument ();
	ASSERT (pDoc);
	ASSERT (pDoc->IsKindOf (RUNTIME_CLASS (CBaseDoc)));
	return (CBaseDoc *)pDoc;
}

/////////////////////////////////////////////////////////////////////////////
// CBaseView drawing

void CBaseView::OnDraw(CDC* pDC)
{
	CRect rc;

	GetClientRect (&rc);
	int nMapMode = pDC->SetMapMode (MM_ANISOTROPIC);

	if (!m_pBmp) {
		m_pBmp = CreateBitmap (* pDC);
		const int nAdjust = 1000;
		CSize size = GetLogicalSize () * GetZoom ();
		CSize sizeTotal (size.cx + nAdjust, size.cy + nAdjust);
		SetScrollSizes (MM_TEXT, sizeTotal);
	}

	if (m_pBmp) {
		CDC dcMem;
		DIBSECTION ds;
		CPoint ptOffset = GetOffset ();
		double dZoom = GetZoom ();

		::ZeroMemory (&ds, sizeof (ds));
		VERIFY (m_pBmp->GetObject (sizeof (DIBSECTION), &ds));
		CSize logical (ds.dsBm.bmWidth, ds.dsBm.bmHeight);
		logical.cx = (int)((double)logical.cx * dZoom);
		logical.cy = (int)((double)logical.cy * dZoom);

		pDC->SetWindowExt (logical);
//		pDC->SetViewportOrg (ptOrg);
		pDC->SetViewportExt (logical); 

		dcMem.CreateCompatibleDC (pDC);

		CGdiObject * pOld = dcMem.SelectObject (m_pBmp);
//		pDC->FillSolidRect (rc, Color::rgbRed);
//		pDC->FillSolidRect (CRect (0, 0, logical.cx, logical.cy), Color::rgbDarkRed);
		pDC->StretchBlt (0, 0, //ptOffset.x, ptOffset.y, 
			logical.cx, logical.cy,
			&dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, 
			SRCCOPY);
		dcMem.SelectObject (pOld);
	}
	#ifdef _DEBUG
	else {
		CString str;
		str.Format (_T ("%s::CreateBitmap failed"),
			a2w (GetRuntimeClass ()->m_lpszClassName));
		pDC->DrawText (str, rc, 0);
	}
	#endif //_DEBUG

	pDC->SetMapMode (nMapMode);

}

/////////////////////////////////////////////////////////////////////////////
// CBaseView diagnostics

#ifdef _DEBUG
void CBaseView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CBaseView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CBaseView message handlers

void CBaseView::OnUpdateZoom(CCmdUI* pCmdUI) 
{
	CString str;
	str.Format (_T ("%d %%"), (int)(GetZoom () * 100.0));
	pCmdUI->Enable (true);	
	pCmdUI->SetText (str);
}


double CBaseView::GetZoom () const
{
	return m_dZoom;
}

CSize CBaseView::GetPageSize () const
{
	if (m_pBmp) {
		DIBSECTION ds;
		double dZoom = GetZoom ();

		::ZeroMemory (&ds, sizeof (ds));
		VERIFY (m_pBmp->GetObject (sizeof (DIBSECTION), &ds));
		CSize logical (ds.dsBm.bmWidth, ds.dsBm.bmHeight);
		logical.cx = (int)((double)logical.cx * dZoom);
		logical.cy = (int)((double)logical.cy * dZoom);

		return logical;
	}

	CRect rc;
	GetClientRect (rc);
	return rc.Size ();
}

void CBaseView::SetZoomStatus()
{
	// this updates the main window's zoom ctrls
	SetZoom (GetZoom ()); 
}

void CBaseView::OnViewZoomin() 
{
	SetZoom (min (GetZoom () + 0.25, 2.0), true);
}

void CBaseView::OnViewZoomout() 
{
	SetZoom (max (GetZoom () - 0.25, 0.05), true);
}

bool CBaseView::SetZoom(double dZoom, bool bAutoScroll)
{
	if (dZoom >= 0.10 && dZoom <= 2.0) {
		bool bUpdate = m_dZoom != dZoom;

		if (bUpdate) {
			m_dZoom = dZoom;
			const CSize page = GetPageSize ();
			SetScrollSizes (MM_TEXT, page);
			CView::Invalidate ();

			return true;
		}
	}

	return false;
}

void CBaseView::OnZoomDeltaPosChanged(NMHDR * pnmh, LRESULT * pResult)
{
	NMUPDOWN * pnud = (NMUPDOWN *) pnmh;
	ASSERT (pnud);
	double dZoom = (double)pnud->iPos / 100.0;
	SetZoom (dZoom, true);
}

void CBaseView::OnViewZoomnormal100() 
{
	SetZoom (1.0, true);	
}


void CBaseView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	CScrollView::OnUpdate (pSender, lHint, pHint);
	CSize size = GetLogicalSize () * GetZoom ();
	CSize sizeTotal (size.cx + 500, size.cy + 500);
//	SetScrollSizes (MM_LOENGLISH, sizeTotal);
	Invalidate ();
}

void CBaseView::InitMenu (CMenu * pMenu)
{
	InitDefineMenu (pMenu);
	InitContextMenu (pMenu);
}

void CBaseView::Invalidate(BOOL bErase)
{
//	TODO ("does this really need to be destroyed when invalid?");

	if (m_pBmp) {
		delete m_pBmp;
		m_pBmp = NULL;
	}
}

CBitmap * CBaseView::CreateBitmap(CDC &dc)
{
	return NULL;
}

CPoint CBaseView::GetOffset() const
{
	CPoint pt = GetScrollPosition ();
	return CPoint (pt.x, -pt.y);
}

void CBaseView::InitDefineMenu(CMenu *pMenu)
{
	for (int i = 0; m_nDefineMenuIDs [i] != -1; i++) {
		UINT nID = m_nDefineMenuIDs [i];

		if (!IsDefineCmdEnabled (nID))
			pMenu->DeleteMenu (nID, MF_BYCOMMAND);
		else {
			CString str = GetDefineMenuStr (nID);

			pMenu->ModifyMenu (nID, MF_BYCOMMAND, nID, str);
		}
	}
}

void CBaseView::InitContextMenu (CMenu * pMenu)
{
	ASSERT (pMenu);

	if (const CBaseDoc * pDoc = GetDocument ()) {
		UINT nReadOnly = pDoc->GetReadOnly () ? MF_ENABLED : MF_GRAYED;
		UINT nUndo = pDoc->GetUndo ().GetSize () ? MF_ENABLED : MF_GRAYED;
		UINT nRedo = pDoc->GetRedo ().GetSize () ? MF_ENABLED : MF_GRAYED;

		pMenu->EnableMenuItem (ID_EDIT_UNDO, nUndo | MF_BYCOMMAND);
		pMenu->EnableMenuItem (ID_EDIT_REDO, nRedo | MF_BYCOMMAND);
		pMenu->EnableMenuItem (ID_EDIT_CUT, nReadOnly | MF_BYCOMMAND);
		pMenu->EnableMenuItem (ID_EDIT_PASTE, nReadOnly | MF_BYCOMMAND);
	}
}

CSize CBaseView::GetLogicalSize() const
{
	CSize size;

	if (m_pBmp) {
		DIBSECTION ds;

		::ZeroMemory (&ds, sizeof (ds));
		VERIFY (m_pBmp->GetObject (sizeof (DIBSECTION), &ds));
		size = CSize (ds.dsBm.bmWidth, ds.dsBm.bmHeight);
	}
	else {
		CRect rc;

		GetClientRect (rc);
		size = rc.Size ();
	}

	return size;
}

void CBaseView::OnViewZoomCustom() 
{
	double dZoom = GetZoom ();
	ZOOMSTRUCT zs = { 10, 200, (int)(dZoom * 100.0), (LPVOID)this };
	CZoomDlg dlg (ZoomFct, &zs, this);

	dlg.m_nZoom = (int)(dZoom * 100.0);

	if (dlg.DoModal () == IDOK)
		SetZoom ((double)dlg.m_nZoom / 100.0, true);

	SetZoomStatus ();
}

bool CALLBACK CBaseView::ZoomFct (FoxjetCommon::ZOOMSTRUCT & zs)
{
	CBaseView * pView = (CBaseView *)zs.m_lpData;
	ASSERT (pView);

	if (pView->SetZoom ((double)zs.m_nZoom / 100.0), true)
		return true;
	else {
		zs.m_nZoom = (int)(pView->GetZoom () * 100.0);
		return false;
	}
}

void CBaseView::OnDrawTrackerRect (CDC * pDC, const CRect & rc, DWORD dwElement)
{
}
