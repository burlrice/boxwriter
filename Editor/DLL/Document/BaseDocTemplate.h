// BaseDocTemplate.h: interface for the CBaseDocTemplate class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BASEDOCTEMPLATE_H__B0540F1A_692D_41F7_94BB_F69A53EEEEE4__INCLUDED_)
#define AFX_BASEDOCTEMPLATE_H__B0540F1A_692D_41F7_94BB_F69A53EEEEE4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxwin.h>
#include "DocApi.h"
#include "FileExt.h"

namespace FoxjetDocument
{
	class DOC_API CBaseDocTemplate : public CMultiDocTemplate  
	{
		DECLARE_DYNAMIC (CBaseDocTemplate)
	public:
		CBaseDocTemplate (UINT nIDResource, CRuntimeClass* pDocClass,
			CRuntimeClass* pFrameClass, CRuntimeClass* pViewClass,
			FoxjetFile::DOCTYPE	type);
		virtual ~CBaseDocTemplate ();
		
		virtual BOOL GetDocString (CString & str, enum DocStringIndex index) const;
		virtual void SetDefaultTitle (CDocument * pDocument);
		virtual CDocument * CreateNewDocument ();

		virtual CString GetNextDefaultName () const;
		virtual CDocument * GetOpenDoc (const CString & strTitle) const;
		virtual bool IsExistingDoc (const CString & strTitle) const;
		virtual CString GetDefPrefix () const = 0;

		virtual void UpdateDocument (CDocument* pDoc);
		virtual void UpdateDocStrings (const CString & strDir, const CString & strFile);

		const FoxjetFile::DOCTYPE	m_type;

		// to be filled in by CNewDocType
		CString						m_strPrefix;
		CString						m_strTitle;
		CString						m_strDesc;	

	protected:
		CBaseDocTemplate ();
		CBaseDocTemplate (const CBaseDocTemplate & rhs);
		CBaseDocTemplate & operator = (const CBaseDocTemplate & rhs);
	};
}; //namespace FoxjetDocument

#endif // !defined(AFX_BASEDOCTEMPLATE_H__B0540F1A_692D_41F7_94BB_F69A53EEEEE4__INCLUDED_)
