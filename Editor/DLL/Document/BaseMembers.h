// BaseMembers.h: interface for the CBaseMembers class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BASEMEMBERS_H__AD0B0165_9C4F_4DD9_B4FF_7167E5BE6DAB__INCLUDED_)
#define AFX_BASEMEMBERS_H__AD0B0165_9C4F_4DD9_B4FF_7167E5BE6DAB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DocApi.h"
#include "List.h"
#include "Version.h"

namespace FoxjetDocument
{
	class DOC_API CBaseMembers  
	{
	protected:
		CBaseMembers ();
		CBaseMembers (const CBaseMembers & rhs);
		CBaseMembers & operator = (const CBaseMembers & rhs);
		virtual ~CBaseMembers ();

	public:
		UNITS					m_units;
		bool					m_bReadOnly;
		FoxjetCommon::CVersion	m_version;
	};

	DOC_API bool operator == (const CBaseMembers & lhs, const CBaseMembers & rhs);
	DOC_API bool operator != (const CBaseMembers & lhs, const CBaseMembers & rhs);
}; //namespace FoxjetDocument

#endif // !defined(AFX_BASEMEMBERS_H__AD0B0165_9C4F_4DD9_B4FF_7167E5BE6DAB__INCLUDED_)
