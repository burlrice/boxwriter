// BaseMembers.cpp: implementation of the CBaseMembers class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Document.h"
#include "BaseMembers.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

DOC_API bool FoxjetDocument::operator == (const FoxjetDocument::CBaseMembers & lhs, const FoxjetDocument::CBaseMembers & rhs)
{
	return 
		lhs.m_units		== rhs.m_units &&
		lhs.m_bReadOnly	== rhs.m_bReadOnly &&
		lhs.m_version	== rhs.m_version;

}

DOC_API bool FoxjetDocument::operator != (const FoxjetDocument::CBaseMembers & lhs, const FoxjetDocument::CBaseMembers & rhs)
{
	return !(lhs == rhs);
}

CBaseMembers::CBaseMembers ()
:	m_units	(INCHES),
	m_bReadOnly (false)
{
}

CBaseMembers::CBaseMembers (const CBaseMembers & rhs)
:	m_units	(rhs.m_units),
	m_bReadOnly (rhs.m_bReadOnly),
	m_version (rhs.m_version)
{
}

CBaseMembers & CBaseMembers::operator = (const CBaseMembers & rhs)
{
	if (this != &rhs) {
		m_units = rhs.m_units;
		m_bReadOnly = rhs.m_bReadOnly;
		m_version = rhs.m_version;
	}

	return * this;
}

CBaseMembers::~CBaseMembers ()
{
}
