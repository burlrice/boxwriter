/* Template filled out by CMake */

/*
 * *** THIS HEADER IS INCLUDED BY PdfCompilerCompat.h ***
 * *** DO NOT INCLUDE DIRECTLY ***
 */
#ifndef _PDF_COMPILERCOMPAT_H
#error Please include PdfDefines.h instead
#endif

#include <Winsock2.h>
//#include <algorithm>
//#include <limits>

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

namespace hack
{
	template <typename T> T min (T a, T b) { return a < b ? a : b; }	
	template <typename T> T max (T a, T b) { return a > b ? a : b; }
};

#define PODOFO_VERSION_MAJOR 0
#define PODOFO_VERSION_MINOR 9
#define PODOFO_VERSION_PATCH 6

/* PoDoFo configuration options */
//#define PODOFO_MULTI_THREAD

/* somewhat platform-specific headers */
//#define PODOFO_HAVE_STRINGS_H 1
//#define PODOFO_HAVE_ARPA_INET_H 1
//#define PODOFO_HAVE_WINSOCK2_H 1
//#define PODOFO_HAVE_MEM_H 1
//#define PODOFO_HAVE_CTYPE_H 1

/* Integer types - headers */
//#define PODOFO_HAVE_STDINT_H 1
//#define PODOFO_HAVE_BASETSD_H 1 
//#define PODOFO_HAVE_SYS_TYPES_H 1
/* Integer types - type names */
#define PDF_INT8_TYPENAME   __int8
#define PDF_INT16_TYPENAME  __int16
#define PDF_INT32_TYPENAME  __int32
#define PDF_INT64_TYPENAME  __int64
#define PDF_UINT8_TYPENAME  unsigned __int8
#define PDF_UINT16_TYPENAME unsigned __int16
#define PDF_UINT32_TYPENAME unsigned __int32
#define PDF_UINT64_TYPENAME unsigned __int64

/* Sizes of int64 and long, to pick proper printf format */
//#define SZ_INT64 @SZ_INT64@
//#define SZ_LONG @SZ_LONG@

/* Endianness */
//#define TEST_BIG

/* Features */
//#define PODOFO_NO_FONTMANAGER

/* Libraries */
//#define PODOFO_HAVE_JPEG_LIB
//#define PODOFO_HAVE_PNG_LIB
//#define PODOFO_HAVE_TIFF_LIB
//#define PODOFO_HAVE_FONTCONFIG
//#define PODOFO_HAVE_LUA
//#define PODOFO_HAVE_BOOST
//#define PODOFO_HAVE_CPPUNIT
//#define PODOFO_HAVE_OPENSSL
//#define PODOFO_HAVE_OPENSSL_1_1
//#define PODOFO_HAVE_OPENSSL_NO_RC4
//#define PODOFO_HAVE_LIBIDN
//#define PODOFO_HAVE_UNISTRING_LIB
