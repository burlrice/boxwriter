IF(NOT EXISTS "C:/Dev/sw0925/podofo-0.9.4/install_manifest.txt")
  MESSAGE(FATAL_ERROR "Cannot find install manifest: \"C:/Dev/sw0925/podofo-0.9.4/install_manifest.txt\"")
ENDIF(NOT EXISTS "C:/Dev/sw0925/podofo-0.9.4/install_manifest.txt")

FILE(READ "C:/Dev/sw0925/podofo-0.9.4/install_manifest.txt" files)
STRING(REGEX REPLACE "\n" ";" files "${files}")
FOREACH(file ${files})
  MESSAGE(STATUS "Uninstalling \"${file}\"")
  IF(NOT EXISTS "${file}")
    MESSAGE(FATAL_ERROR "File \"${file}\" does not exists.")
  ENDIF(NOT EXISTS "${file}")
  EXEC_PROGRAM("C:/Program Files (x86)/CMake/bin/cmake.exe" ARGS "-E remove \"${file}\""
    OUTPUT_VARIABLE rm_out
    RETURN_VARIABLE rm_retval)
  IF("${rm_retval}" GREATER 0)
    MESSAGE(FATAL_ERROR "Problem when removing \"${file}\"")
  ENDIF("${rm_retval}" GREATER 0)
ENDFOREACH(file)
