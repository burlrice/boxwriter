#if !defined(AFX_CHANGEDSNDLG_H__938E725A_DD16_4173_AA1E_5529BE0C9D43__INCLUDED_)
#define AFX_CHANGEDSNDLG_H__938E725A_DD16_4173_AA1E_5529BE0C9D43__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChangeDsnDlg.h : header file
//

#include "Utils.h"

/////////////////////////////////////////////////////////////////////////////
// CChangeDsnDlg dialog

class CChangeDsnDlg : public FoxjetCommon::CEliteDlg
{
// Construction
public:
	CChangeDsnDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CChangeDsnDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	CString m_strFrom;
	CString m_strTo;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChangeDsnDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnOK();

	// Generated message map functions
	//{{AFX_MSG(CChangeDsnDlg)
	afx_msg void OnFind();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHANGEDSNDLG_H__938E725A_DD16_4173_AA1E_5529BE0C9D43__INCLUDED_)
