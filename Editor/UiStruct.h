#ifndef __UISTRUCT_H__
#define __UISTRUCT_H__

typedef struct tagCMDUISTRUCT
{
	tagCMDUISTRUCT () : m_bEnabled (false), m_bChecked (false), m_nState (0) { }

	bool m_bEnabled;
	bool m_bChecked;
	int m_nState;
} CMDUISTRUCT;

typedef struct tagFONTUISTRUCT
{
	tagFONTUISTRUCT () : m_nSize (0), m_nWidth (0) { }

	CMDUISTRUCT m_name;
	CMDUISTRUCT m_size;
	CMDUISTRUCT m_bold;
	CMDUISTRUCT m_italic;
	CMDUISTRUCT	m_width;
	CString		m_strName;
	int			m_nSize;
	int			m_nWidth;
} FONTUISTRUCT;

typedef struct tagSELUISTRUCT
{
	tagSELUISTRUCT () : m_nSelCount (0) { }

	int m_nSelCount;
} SELUISTRUCT;
#endif // __UISTRUCT_H__