// AuxTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include <Winsock2.h>
#include "Defines.h"
#include "AuxTest.h"
#include "AuxTestDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

#define WM_UPDATE_CONTROLS WM_APP + 0x01

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAuxTestDlg dialog

CAuxTestDlg::CAuxTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAuxTestDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAuxTestDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAuxTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAuxTestDlg)
	DDX_Control(pDX, IDC_SERIAL_DATA, m_SerialData);
	DDX_Control(pDX, IDC_IPADDRESS1, m_IpAddress);
	DDX_Control(pDX, IDC_CONNECT_LIGHT, m_ConnectLight);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAuxTestDlg, CDialog)
	//{{AFX_MSG_MAP(CAuxTestDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_CONNECT, OnConnect)
	ON_BN_CLICKED(IDC_STROBE1_RED_OFF, OnStrobe1RedOff)
	ON_BN_CLICKED(IDC_STROBE1_RED_ON, OnStrobe1RedOn)
	ON_BN_CLICKED(IDC_STROBE1_RED_FLASH, OnStrobe1RedFlash)
	ON_BN_CLICKED(IDC_STROBE1_GREEN_OFF, OnStrobe1GreenOff)
	ON_BN_CLICKED(IDC_STROBE1_GREEN_ON, OnStrobe1GreenOn)
	ON_BN_CLICKED(IDC_STROBE1_GREEN_FLASH, OnStrobe1GreenFlash)
	ON_BN_CLICKED(IDC_STROBE2_RED_OFF, OnStrobe2RedOff)
	ON_BN_CLICKED(IDC_STROBE2_RED_ON, OnStrobe2RedOn)
	ON_BN_CLICKED(IDC_STROBE2_RED_FLASH, OnStrobe2RedFlash)
	ON_BN_CLICKED(IDC_STROBE2_GREEN_OFF, OnStrobe2GreenOff)
	ON_BN_CLICKED(IDC_STROBE2_GREEN_ON, OnStrobe2GreenOn)
	ON_BN_CLICKED(IDC_STROBE2_GREEN_FLASH, OnStrobe2GreenFlash)
	ON_BN_CLICKED(IDC_TX_PORT_A, OnTxPortA)
	ON_BN_CLICKED(IDC_TX_PORT_B, OnTxPortB)
	ON_BN_CLICKED(IDC_TX_PORT_C, OnTxPortC)
	ON_BN_CLICKED(IDC_TX_PORT_D, OnTxPortD)
	ON_BN_CLICKED(IDC_DISCONNECT, OnDisconnect)
	ON_BN_CLICKED(IDC_CLEAR, OnClear)
	//}}AFX_MSG_MAP
	ON_MESSAGE (WM_SERIAL_DATA, OnSerialData)
	ON_MESSAGE (WM_PS2_DATA, OnPS2Data)
	ON_MESSAGE (WM_CONNECT_STATE_CHANGED, OnConnectionStateChanged)
	ON_MESSAGE (WM_DIGITAL_INPUT_DATA, OnDigitialInputData)
	ON_MESSAGE (WM_UPDATE_CONTROLS, OnUpdateControls)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAuxTestDlg message handlers

BOOL CAuxTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	m_IpAddress.SetAddress ( ntohl (inet_addr ( "10.1.2.50")) );

	CButton *p;
	p = ( CButton * ) GetDlgItem (IDC_STROBE1_RED_OFF);
	p->SetCheck ( 1 );
	p = ( CButton * ) GetDlgItem (IDC_STROBE2_RED_OFF);
	p->SetCheck ( 1 );
	
	p = ( CButton * ) GetDlgItem (IDC_STROBE1_GREEN_OFF);
	p->SetCheck ( 1 );
	p = ( CButton * ) GetDlgItem (IDC_STROBE2_GREEN_OFF);
	p->SetCheck ( 1 );

	UpdateData();
	while (!PostMessage (WM_UPDATE_CONTROLS, FALSE, 0 )) Sleep (0);
//	EnableControls ( false );
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAuxTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAuxTestDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAuxTestDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CAuxTestDlg::OnConnect() 
{
	if ( UpdateData() )
	{
		DWORD dwAddress;
		m_IpAddress.GetAddress ( dwAddress );
		in_addr Addr;
		Addr.S_un.S_addr = ntohl (dwAddress);
		CString sIpAddress = inet_ntoa(Addr);
		if ( !m_AuxBox.IsOpen() )
			m_AuxBox.Open ( sIpAddress, 1200, m_hWnd );
	}
}

LRESULT CAuxTestDlg::OnSerialData( WPARAM wParam, LPARAM lParam )
{
	short Len = 1024;
	VARIANT v;
	VariantInit ( &v );
	v.vt = VT_UI1|VT_BYREF;
	v.pbVal = new unsigned char [ Len ];
	memset ( v.pbVal, 0x00, Len);
	char PortID;
	m_AuxBox.GetSerialData (wParam, &v, &Len);
	while ( Len > 0 )
	{
		CString sData;
		switch ( wParam )
		{
			case 0:
				PortID = 'A';
				break;
			case 1:
				PortID = 'B';
				break;
			case 2:
				PortID = 'C';
				break;
			case 3:
				PortID = 'D';
				break;
		}
		sData.Format ( "[PORT%c][%s]", PortID, v.pbVal );
		m_SerialData.AddString ( sData );
		Len = 1024;
		memset ( v.pbVal, 0x00, Len);
		m_AuxBox.GetSerialData (wParam, &v, &Len);
	}
	delete [] v.pbVal;
	return 0L;
}

LRESULT CAuxTestDlg::OnPS2Data( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

LRESULT CAuxTestDlg::OnConnectionStateChanged( WPARAM wParam, LPARAM lParam )
{
	CBitmap bm;
	if ( wParam == 1 )
	{
		m_AuxBox.SetStrobe ( 1, 1, STROBE_OFF );
		m_AuxBox.SetStrobe ( 1, 2, STROBE_OFF );
		m_AuxBox.SetStrobe ( 2, 1, STROBE_OFF );
		m_AuxBox.SetStrobe ( 2, 2, STROBE_OFF );

		bm.LoadBitmap (IDB_GREEN);
		m_ConnectLight.SetBitmap ( bm );
		while (!PostMessage (WM_UPDATE_CONTROLS, TRUE, 0 )) Sleep (0);
//		EnableControls ( true );
	}
	else
	{
		bm.LoadBitmap (IDB_RED);
		m_ConnectLight.SetBitmap ( bm );
		while (!PostMessage (WM_UPDATE_CONTROLS, FALSE, 0 )) Sleep (0);
//		EnableControls ( false );
	}

	CButton *p;
	p = ( CButton * ) GetDlgItem (IDC_STROBE1_RED_OFF);
	p->SetCheck ( 1 );
	p = ( CButton * ) GetDlgItem (IDC_STROBE1_RED_ON);
	p->SetCheck ( 0 );
	p = ( CButton * ) GetDlgItem (IDC_STROBE1_RED_FLASH);
	p->SetCheck ( 0 );
	
	p = ( CButton * ) GetDlgItem (IDC_STROBE1_GREEN_OFF);
	p->SetCheck ( 1 );
	p = ( CButton * ) GetDlgItem (IDC_STROBE1_GREEN_ON);
	p->SetCheck ( 0 );
	p = ( CButton * ) GetDlgItem (IDC_STROBE1_GREEN_FLASH);
	p->SetCheck ( 0 );


	p = ( CButton * ) GetDlgItem (IDC_STROBE2_RED_OFF);
	p->SetCheck ( 1 );
	p = ( CButton * ) GetDlgItem (IDC_STROBE2_RED_ON);
	p->SetCheck ( 0 );
	p = ( CButton * ) GetDlgItem (IDC_STROBE2_RED_FLASH);
	p->SetCheck ( 0 );
	
	p = ( CButton * ) GetDlgItem (IDC_STROBE2_GREEN_OFF);
	p->SetCheck ( 1 );
	p = ( CButton * ) GetDlgItem (IDC_STROBE2_GREEN_ON);
	p->SetCheck ( 0 );
	p = ( CButton * ) GetDlgItem (IDC_STROBE2_GREEN_FLASH);
	p->SetCheck ( 0 );
	return 0L;
}

LRESULT CAuxTestDlg::OnDigitialInputData( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

void CAuxTestDlg::OnStrobe1RedOff() 
{
	m_AuxBox.SetStrobe ( 1, 1, STROBE_OFF );
}

void CAuxTestDlg::OnStrobe1RedOn() 
{
	m_AuxBox.SetStrobe ( 1, 1, STROBE_ON );
}

void CAuxTestDlg::OnStrobe1RedFlash() 
{
	m_AuxBox.SetStrobe ( 1, 1, STROBE_FLASH );
}

void CAuxTestDlg::OnStrobe1GreenOff() 
{
	m_AuxBox.SetStrobe (1, 2, STROBE_OFF);
}

void CAuxTestDlg::OnStrobe1GreenOn() 
{
	m_AuxBox.SetStrobe (1, 2, STROBE_ON);
}

void CAuxTestDlg::OnStrobe1GreenFlash() 
{
	m_AuxBox.SetStrobe (1, 2, STROBE_FLASH);
}

void CAuxTestDlg::OnStrobe2RedOff() 
{
	m_AuxBox.SetStrobe (2, 1, STROBE_OFF);
}

void CAuxTestDlg::OnStrobe2RedOn() 
{
	m_AuxBox.SetStrobe (2, 1, STROBE_ON);
}

void CAuxTestDlg::OnStrobe2RedFlash() 
{
	m_AuxBox.SetStrobe (2, 1, STROBE_FLASH);
}

void CAuxTestDlg::OnStrobe2GreenOff() 
{
	m_AuxBox.SetStrobe (2, 2, STROBE_OFF);
}

void CAuxTestDlg::OnStrobe2GreenOn() 
{
	m_AuxBox.SetStrobe (2, 2, STROBE_ON);
}

void CAuxTestDlg::OnStrobe2GreenFlash() 
{
	m_AuxBox.SetStrobe (2, 2, STROBE_FLASH);
}

LRESULT CAuxTestDlg::OnUpdateControls( WPARAM wParam, LPARAM lParam )
{
	EnableControls ( wParam ? true:false );
	return 1L;
}

void CAuxTestDlg::EnableControls(bool Enabled)
{
	CWnd *pWnd;

	pWnd = GetDlgItem (IDC_STROBE1_RED_OFF);
	pWnd->EnableWindow(Enabled);
	pWnd = GetDlgItem (IDC_STROBE1_RED_ON);
	pWnd->EnableWindow(Enabled);
	pWnd = GetDlgItem (IDC_STROBE1_RED_FLASH);
	pWnd->EnableWindow(Enabled);

	pWnd = GetDlgItem (IDC_STROBE2_RED_OFF);
	pWnd->EnableWindow(Enabled);
	pWnd = GetDlgItem (IDC_STROBE2_RED_ON);
	pWnd->EnableWindow(Enabled);
	pWnd = GetDlgItem (IDC_STROBE2_RED_FLASH);
	pWnd->EnableWindow(Enabled);

	pWnd = GetDlgItem (IDC_STROBE1_GREEN_OFF);
	pWnd->EnableWindow(Enabled);
	pWnd = GetDlgItem (IDC_STROBE1_GREEN_ON);
	pWnd->EnableWindow(Enabled);
	pWnd = GetDlgItem (IDC_STROBE1_GREEN_FLASH);
	pWnd->EnableWindow(Enabled);

	pWnd = GetDlgItem (IDC_STROBE2_GREEN_OFF);
	pWnd->EnableWindow(Enabled);
	pWnd = GetDlgItem (IDC_STROBE2_GREEN_ON);
	pWnd->EnableWindow(Enabled);
	pWnd = GetDlgItem (IDC_STROBE2_GREEN_FLASH);
	pWnd->EnableWindow(Enabled);
	
	pWnd = GetDlgItem ( IDC_TX_PORT_A );
	pWnd->EnableWindow(Enabled);
	pWnd = GetDlgItem ( IDC_TX_PORT_B );
	pWnd->EnableWindow(Enabled);
	pWnd = GetDlgItem ( IDC_TX_PORT_C );
	pWnd->EnableWindow(Enabled);
	pWnd = GetDlgItem ( IDC_TX_PORT_D );
	pWnd->EnableWindow(Enabled);

	CBitmap bm;
	if ( Enabled ) 
		bm.LoadBitmap (IDB_GREEN);
	else
		bm.LoadBitmap (IDB_RED);
	CStatic *pLed = (CStatic *) GetDlgItem ( IDC_CONNECT_LIGHT );
	pLed->SetBitmap ( bm );
	
//	m_ConnectLight.SetBitmap ( bm );
}

void CAuxTestDlg::OnOK() 
{
	m_AuxBox.Close();	
	CDialog::OnOK();
}

void CAuxTestDlg::OnTxPortA() 
{
	short Len = 1024;
	VARIANT v;
	VariantInit ( &v );
	v.vt = VT_UI1|VT_BYREF;
	v.pbVal = new unsigned char [ Len ];
	memset ( v.pbVal, 0x00, Len);
	strcpy ( (PCHAR)v.pbVal, "Serial Port Good\r");
	m_AuxBox.PutSerialData(1, &v, strlen ( (PCHAR)v.pbVal ));
	delete v.pbVal;
}

void CAuxTestDlg::OnTxPortB() 
{
	short Len = 1024;
	VARIANT v;
	VariantInit ( &v );
	v.vt = VT_UI1|VT_BYREF;
	v.pbVal = new unsigned char [ Len ];
	memset ( v.pbVal, 0x00, Len);
	strcpy ( (PCHAR)v.pbVal, "Serial Port Good\r");
	m_AuxBox.PutSerialData(2, &v, strlen ( (PCHAR)v.pbVal ));
	delete v.pbVal;
}

void CAuxTestDlg::OnTxPortC() 
{
	short Len = 1024;
	VARIANT v;
	VariantInit ( &v );
	v.vt = VT_UI1|VT_BYREF;
	v.pbVal = new unsigned char [ Len ];
	memset ( v.pbVal, 0x00, Len);
	strcpy ( (PCHAR)v.pbVal, "Serial Port Good\r");
	m_AuxBox.PutSerialData(3, &v, strlen ( (PCHAR)v.pbVal ));
	delete v.pbVal;
}

void CAuxTestDlg::OnTxPortD() 
{
	short Len = 1024;
	VARIANT v;
	VariantInit ( &v );
	v.vt = VT_UI1|VT_BYREF;
	v.pbVal = new unsigned char [ Len ];
	memset ( v.pbVal, 0x00, Len);
	strcpy ( (PCHAR)v.pbVal, "Serial Port Good\r");
	m_AuxBox.PutSerialData(4, &v, strlen ( (PCHAR)v.pbVal ));
	delete v.pbVal;
}

void CAuxTestDlg::OnDisconnect() 
{
	m_AuxBox.Close();	
}

void CAuxTestDlg::OnClear() 
{
	m_SerialData.ResetContent();	
}
