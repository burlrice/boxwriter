//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by AuxTest.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_AUXTEST_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDB_GREEN                       129
#define IDB_RED                         130
#define IDC_IPADDRESS1                  1000
#define IDC_CONNECT                     1001
#define IDC_SERIAL_DATA                 1003
#define IDC_STROBE1_RED_OFF             1005
#define IDC_STROBE1_RED_ON              1006
#define IDC_STROBE1_RED_FLASH           1007
#define IDC_CONNECT_LIGHT               1011
#define IDC_STROBE1_GREEN_OFF           1012
#define IDC_STROBE1_GREEN_ON            1013
#define IDC_STROBE1_GREEN_FLASH         1014
#define IDC_STROBE2_RED_OFF             1015
#define IDC_STROBE2_RED_ON              1016
#define IDC_STROBE2_RED_FLASH           1017
#define IDC_STROBE2_GREEN_OFF           1018
#define IDC_STROBE2_GREEN_ON            1019
#define IDC_STROBE2_GREEN_FLASH         1020
#define IDC_TX_PORT_A                   1021
#define IDC_TX_PORT_B                   1022
#define IDC_TX_PORT_C                   1023
#define IDC_TX_PORT_D                   1024
#define IDC_DISCONNECT                  1025
#define IDC_CLEAR                       1026

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1027
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
