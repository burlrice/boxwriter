; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CAuxTestDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "AuxTest.h"

ClassCount=3
Class1=CAuxTestApp
Class2=CAuxTestDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_AUXTEST_DIALOG

[CLS:CAuxTestApp]
Type=0
HeaderFile=AuxTest.h
ImplementationFile=AuxTest.cpp
Filter=N

[CLS:CAuxTestDlg]
Type=0
HeaderFile=AuxTestDlg.h
ImplementationFile=AuxTestDlg.cpp
Filter=D
LastObject=IDC_CONNECT_LIGHT
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=AuxTestDlg.h
ImplementationFile=AuxTestDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_AUXTEST_DIALOG]
Type=1
Class=CAuxTestDlg
ControlCount=30
Control1=IDC_CONNECT_LIGHT,static,1342177806
Control2=IDC_IPADDRESS1,SysIPAddress32,1342242816
Control3=IDC_CONNECT,button,1342242816
Control4=IDC_SERIAL_DATA,listbox,1352728833
Control5=IDC_STATIC,button,1342178055
Control6=IDC_STATIC,button,1342309127
Control7=IDC_STROBE1_RED_OFF,button,1342181385
Control8=IDC_STROBE1_RED_ON,button,1342181385
Control9=IDC_STROBE1_RED_FLASH,button,1342181385
Control10=IDC_STATIC,button,1342309127
Control11=IDC_STROBE1_GREEN_OFF,button,1342181385
Control12=IDC_STROBE1_GREEN_ON,button,1342181385
Control13=IDC_STROBE1_GREEN_FLASH,button,1342181385
Control14=IDC_STATIC,button,1342178055
Control15=IDC_STATIC,button,1342309127
Control16=IDC_STROBE2_RED_OFF,button,1342181385
Control17=IDC_STROBE2_RED_ON,button,1342181385
Control18=IDC_STROBE2_RED_FLASH,button,1342181385
Control19=IDC_STATIC,button,1342309127
Control20=IDC_STROBE2_GREEN_OFF,button,1342181385
Control21=IDC_STROBE2_GREEN_ON,button,1342181385
Control22=IDC_STROBE2_GREEN_FLASH,button,1342181385
Control23=IDOK,button,1342242817
Control24=IDC_TX_PORT_A,button,1342242816
Control25=IDC_TX_PORT_B,button,1342242816
Control26=IDC_TX_PORT_C,button,1342242816
Control27=IDC_TX_PORT_D,button,1342242816
Control28=IDC_STATIC,button,1342178055
Control29=IDC_DISCONNECT,button,1342242816
Control30=IDC_CLEAR,button,1342242816

