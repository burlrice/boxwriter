// AuxTest.h : main header file for the AUXTEST application
//

#if !defined(AFX_AUXTEST_H__C2E6C4CA_F6DC_4141_9000_CBB87BBF9376__INCLUDED_)
#define AFX_AUXTEST_H__C2E6C4CA_F6DC_4141_9000_CBB87BBF9376__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CAuxTestApp:
// See AuxTest.cpp for the implementation of this class
//

class CAuxTestApp : public CWinApp
{
public:
	CAuxTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAuxTestApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CAuxTestApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUXTEST_H__C2E6C4CA_F6DC_4141_9000_CBB87BBF9376__INCLUDED_)
