// AuxTestDlg.h : header file
//

#if !defined(AFX_AUXTESTDLG_H__516093CA_D073_4F67_9093_D5BBD7ED1D50__INCLUDED_)
#define AFX_AUXTESTDLG_H__516093CA_D073_4F67_9093_D5BBD7ED1D50__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "AuxBox.h"

/////////////////////////////////////////////////////////////////////////////
// CAuxTestDlg dialog

class CAuxTestDlg : public CDialog
{
// Construction
public:
	CAuxTestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CAuxTestDlg)
	enum { IDD = IDD_AUXTEST_DIALOG };
	CListBox	m_SerialData;
	CIPAddressCtrl	m_IpAddress;
	CStatic	m_ConnectLight;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAuxTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void EnableControls ( bool Enabled );
	CAuxBox m_AuxBox;
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CAuxTestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnConnect();
	afx_msg void OnStrobe1RedOff();
	afx_msg void OnStrobe1RedOn();
	afx_msg void OnStrobe1RedFlash();
	afx_msg void OnStrobe1GreenOff();
	afx_msg void OnStrobe1GreenOn();
	afx_msg void OnStrobe1GreenFlash();
	afx_msg void OnStrobe2RedOff();
	afx_msg void OnStrobe2RedOn();
	afx_msg void OnStrobe2RedFlash();
	afx_msg void OnStrobe2GreenOff();
	afx_msg void OnStrobe2GreenOn();
	afx_msg void OnStrobe2GreenFlash();
	virtual void OnOK();
	afx_msg void OnTxPortA();
	afx_msg void OnTxPortB();
	afx_msg void OnTxPortC();
	afx_msg void OnTxPortD();
	afx_msg void OnDisconnect();
	afx_msg void OnClear();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg LRESULT OnSerialData( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnPS2Data( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnConnectionStateChanged( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnDigitialInputData( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnUpdateControls( WPARAM wParam, LPARAM lParam );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUXTESTDLG_H__516093CA_D073_4F67_9093_D5BBD7ED1D50__INCLUDED_)
