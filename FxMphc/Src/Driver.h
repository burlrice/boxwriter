// Declarations for FxMphc driver
// Copyright (C) 1999 by Walter Oney
// All rights reserved

#ifndef DRIVER_H
#define DRIVER_H
#include "DevQueue.h"
#include "TypeDefs.h"
#include "MphcCard.h"

#define MAX_DEVICES	6

#define DRIVERNAME "FXMPHC"				// for use in messages
#define LDRIVERNAME L"FXMPHC"				// for use in UNICODE string constants

///////////////////////////////////////////////////////////////////////////////
// Device extension structure
/*
typedef struct
{
	PKEVENT PhotoA;	// Photocell Trigger Event Handle
	PKEVENT PhotoB;	// Over The Top Photo #1 Event Handle
	PKEVENT PhotoC;	// Over The Top Photo #2 Event Handle
	PKEVENT Serial;	// Serial Data Recieved Event Handle
}_EVENT_HANDLES;
*/
enum DEVSTATE {
	STOPPED,								// device stopped
	WORKING,								// started and working
	PENDINGSTOP,							// stop pending
	PENDINGREMOVE,							// remove pending
	SURPRISEREMOVED,						// removed by surprise
	REMOVED,								// removed
};

typedef struct _DEVICE_EXTENSION {
	PDEVICE_OBJECT DeviceObject;			// device object this extension belongs to
	PDEVICE_OBJECT LowerDeviceObject;		// next lower driver in same stack
	PDEVICE_OBJECT Pdo;						// the PDO
	IO_REMOVE_LOCK RemoveLock;				// removal control locking structure
	UNICODE_STRING ifname;					// interface name
	DEVSTATE state;							// current state of device
	DEVSTATE prevstate;						// state prior to removal query
	DEVICE_POWER_STATE devpower;			// current device power state
	SYSTEM_POWER_STATE syspower;			// current system power state
	DEVICE_CAPABILITIES devcaps;			// copy of most recent device capabilities
	DEVQUEUE dqReadWrite;					// queue for reads and writes
	LONG handles;							// # open handles
	PKINTERRUPT InterruptObject;			// address of interrupt object
	ULONG IntVectorNum;					// Interrupt Vector Number.
	PUCHAR portbase;						// I/O port base address
	ULONG nports;							// number of assigned ports
	PUCHAR membase;							// mapped memory base address
	ULONG memlength;							// size of mapped memory area
////////////////////////////////////////////////////////////////////////////////////
	KSPIN_LOCK IsrSpinLock;					// ISR Spin Lock for shared varaible access.
	KDPC DpcCustomQue;
	PKEVENT SerialDataEvent;
	UCHAR LCRSave;
	CMphcCard m_Devices[MAX_DEVICES];	// Struct holding info about each card controlled by this driver.
	CMphcCard *m_pMasterController;
////////////////////////////////////////////////////////////////////////////////////
	BOOLEAN mappedport;						// true if we mapped port addr in StartDevice
	BOOLEAN busy;							// true if device busy with a request
	BOOLEAN StalledForPower;				// power management has stalled IRP queue
	BOOLEAN bActiveRequest;
	BOOLEAN bActiveRequestComplete;
} DEVICE_EXTENSION, *PDEVICE_EXTENSION;

///////////////////////////////////////////////////////////////////////////////
// Global functions
////////////////////////////////////////////////////////////////////////////////////////
BOOLEAN EnableCommandMode ( IN PDEVICE_EXTENSION pdx );
BOOLEAN EnableProgrammingMode ( IN PDEVICE_EXTENSION pdx );
BOOLEAN RestoreNormalOperation ( IN PDEVICE_EXTENSION pdx );

VOID RunIOCTLSync (PDEVICE_EXTENSION pdx);
VOID RunWriteMajorSync (PDEVICE_EXTENSION pdx);
VOID RunReadMajorSync (PDEVICE_EXTENSION pdx);
/*
VOID WriteByte (IN PUCHAR pBaseAddr, IN UCHAR offset, IN UCHAR data, IN BOOLEAN MappedPort = FALSE);
VOID WriteBuffer (IN PUCHAR pBaseAddr, IN UCHAR offset, IN PUCHAR pData, IN ULONG len, IN BOOLEAN MappedPort = FALSE);
UCHAR ReadByte (IN PUCHAR pBaseAddr, IN UCHAR offset, BOOLEAN MappedPort = FALSE);
VOID ReadBuffer (IN PUCHAR pBaseAddr, IN UCHAR offset, IN PUCHAR pData, IN ULONG len, BOOLEAN MappedPort = FALSE);

VOID WriteByte (IN PDEVICE_EXTENSION pdx, IN UCHAR offset, IN UCHAR data);
VOID WriteBuffer (IN PDEVICE_EXTENSION pdx, IN UCHAR offset, IN UCHAR *data, IN ULONG len);
UCHAR ReadByte (IN PDEVICE_EXTENSION pdx, IN UCHAR offset);
VOID ReadBuffer (IN PDEVICE_EXTENSION pdx, IN UCHAR offset, IN PUCHAR data, IN ULONG len);
*/
////////////////////////////////////////////////////////////////////////////////////////

VOID RemoveDevice(IN PDEVICE_OBJECT fdo);
NTSTATUS CompleteRequest(IN PIRP Irp, IN NTSTATUS status, IN ULONG_PTR info);
NTSTATUS CompleteRequest(IN PIRP Irp, IN NTSTATUS status);
NTSTATUS ForwardAndWait(IN PDEVICE_OBJECT fdo, IN PIRP Irp);
NTSTATUS SendDeviceSetPower(PDEVICE_EXTENSION fdo, DEVICE_POWER_STATE state, BOOLEAN wait = FALSE);
VOID SendAsyncNotification(PVOID context);
VOID EnableAllInterfaces(PDEVICE_EXTENSION pdx, BOOLEAN enable);
VOID DeregisterAllInterfaces(PDEVICE_EXTENSION pdx);
NTSTATUS StartDevice(PDEVICE_OBJECT fdo, PCM_PARTIAL_RESOURCE_LIST raw, PCM_PARTIAL_RESOURCE_LIST translated);
VOID StartIo(PDEVICE_OBJECT fdo, PIRP Irp);
VOID StopDevice(PDEVICE_OBJECT fdo, BOOLEAN oktouch = FALSE);
VOID DpcForIsr(PKDPC Dpc, PDEVICE_OBJECT fdo, PIRP Irp, PDEVICE_EXTENSION pdx);
VOID DpcCustom(PKDPC Dpc, PDEVICE_OBJECT fdo, PIRP Irp, PDEVICE_EXTENSION pdx);
VOID OnCancelReadWrite(IN PDEVICE_OBJECT fdo, IN PIRP Irp);

// I/O request handlers

NTSTATUS DispatchCreate(PDEVICE_OBJECT fdo, PIRP Irp);
NTSTATUS DispatchClose(PDEVICE_OBJECT fdo, PIRP Irp);
NTSTATUS DispatchReadWrite(PDEVICE_OBJECT fdo, PIRP Irp);
NTSTATUS DispatchControl(PDEVICE_OBJECT fdo, PIRP Irp);
NTSTATUS DispatchPower(PDEVICE_OBJECT fdo, PIRP Irp);
NTSTATUS DispatchPnp(PDEVICE_OBJECT fdo, PIRP Irp);

extern BOOLEAN win98;
extern UNICODE_STRING servkey;

#endif // DRIVER_H
