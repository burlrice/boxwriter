///////////////////////////////////////////////////////////////////////////////
// Read/Write request processors for FxMphc driver
// Copyright (C) 2002 By FoxJet Tech Center
// All rights reserved
//
//	Author: Chris G. Hodge
//	Version History:
//		1.00	Creation
///////////////////////////////////////////////////////////////////////////////

#include "stddcls.h"
#include "Defines.h"
#include "driver.h"
#include "IoCtls.h"

#pragma PAGEDCODE

NTSTATUS DispatchCreate(PDEVICE_OBJECT fdo, PIRP Irp)
{
	PAGED_CODE();
	KdPrint((DRIVERNAME " - IRP_MJ_CREATE\n"));
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;

	PIO_STACK_LOCATION stack = IoGetCurrentIrpStackLocation(Irp);

	// Claim the remove lock in Win2K so that removal waits until the
	// handle closes. Don't do this in Win98, however, because this
	// device might be removed by surprise with handles open, whereupon
	// we'll deadlock in HandleRemoveDevice waiting for a close that
	// can never happen because we can't run the user-mode code that
	// would do the close.
	NTSTATUS status;
	if (win98)
		status = STATUS_SUCCESS;
	else 
		status = IoAcquireRemoveLock(&pdx->RemoveLock, stack->FileObject);

	if (NT_SUCCESS(status))
	{
		if (InterlockedIncrement(&pdx->handles) == 1)
		{
		}
      DbgPrint ( "%s - Instance 0x%x Created", DRIVERNAME, pdx->handles );
	}
	return CompleteRequest(Irp, status, 0);
}

#pragma PAGEDCODE

NTSTATUS DispatchClose(PDEVICE_OBJECT fdo, PIRP Irp)
{
	PAGED_CODE();
	KdPrint((DRIVERNAME " - IRP_MJ_CLOSE\n"));
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;
	PIO_STACK_LOCATION stack = IoGetCurrentIrpStackLocation(Irp);
	if (InterlockedDecrement(&pdx->handles) == 0)
	{
	   for ( int i = 0; i < MAX_DEVICES; i++ ) {
			if ( pdx->m_Devices[i].GetIOAddress() != NULL )
			{
				DbgPrint ( "%s- Destroying Device 0x%X", DRIVERNAME, pdx->m_Devices[i].GetIOAddress() );
				pdx->m_Devices[i].Destroy();
			}
	   }
	}

	// Release the remove lock to match the acquisition done in DispatchCreate
	if (!win98)
		IoReleaseRemoveLock(&pdx->RemoveLock, stack->FileObject);

	return CompleteRequest(Irp, STATUS_SUCCESS, 0);
}

#pragma PAGEDCODE

NTSTATUS DispatchReadWrite(PDEVICE_OBJECT fdo, PIRP Irp)
{
	PAGED_CODE();
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;

	IoMarkIrpPending(Irp);
	StartPacket(&pdx->dqReadWrite, fdo, Irp, OnCancelReadWrite);
	return STATUS_PENDING;
}

#pragma LOCKEDCODE

VOID OnCancelReadWrite(IN PDEVICE_OBJECT fdo, IN PIRP Irp)
{
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;
	CancelRequest(&pdx->dqReadWrite, Irp);
}

#pragma LOCKEDCODE

// This is the routine which process all IRPs form the Interrupt function or
// from any function which must run sync. with the ISR.
VOID DpcCustom(PKDPC Dpc, PDEVICE_OBJECT fdo, PIRP Irp, PDEVICE_EXTENSION pdx)
{
	KeAcquireSpinLockAtDpcLevel (&pdx->IsrSpinLock);
	int i = 0;

	KeRemoveQueueDpc( &pdx->DpcCustomQue );

	do {
		if ( pdx->m_Devices[i].IsPresent() ) {


			if ( pdx->m_Devices[i].bPhotocellActivated )
				pdx->m_Devices[i].FirePhotoEvent ();

//			if ( pdx->m_Devices[i].m_StartOfPrint )	// Don't do this because application layer is not expecting this.
//				pdx->m_Devices[i].FirePrintEvent ();	// cgh 05e23

			if ( pdx->m_Devices[i].m_EndOfPrint ) 		// cgh 05e23
				pdx->m_Devices[i].FirePrintEvent ();	// cgh 05e23

			if ( pdx->m_Devices[i].m_bFireAmsEvent )
				pdx->m_Devices[i].FireAmsEvent ();		// cgh 05e25

			if ( pdx->m_Devices[i].bSerialDataReady == TRUE)
				pdx->m_Devices[i].FireSerialEvent();			
		}
		i++;
	} while ( i < MAX_DEVICES );
	
	KeReleaseSpinLockFromDpcLevel (&pdx->IsrSpinLock);
}

VOID DpcForIsr(PKDPC Dpc, PDEVICE_OBJECT fdo, PIRP Irp, PDEVICE_EXTENSION pdx)
{
//	int i = 0;

	IoAcquireRemoveLock(&pdx->RemoveLock, Irp);
		Irp = GetCurrentIrp(&pdx->dqReadWrite);
		if  ( Irp )
		{
			pdx->bActiveRequest = pdx->bActiveRequestComplete = FALSE;
			StartNextPacket(&pdx->dqReadWrite, fdo);
			IoCompleteRequest(Irp, IO_SERIAL_INCREMENT);
		}
	IoReleaseRemoveLock(&pdx->RemoveLock, Irp);

}							// DpcForIsr

#pragma LOCKEDCODE
/*

        Port 3FA - Interrupt Identification Register - IIR  (read only)

        񕺼񕓢񔬈񔄮�  2FA, 3FA Interrupt ID Register
         � � � � � � � +---- 1 = no int. pending, 0=int. pending
         � � � � � +------- Interrupt Id bits (see below)
         � � � � +-------- 16550  1 = timeout int. pending, 0 for 8250/16450
         � � +----------- reserved (zero)
         +-------------- 16550  set to 1 if FIFO queues are enabled

        Bits
         21       Meaning            Priority           To reset
         00  modem-status-change      lowest      read MSR
         01  transmit-register-empty  low         read IIR / write THR
         10  data-available           high        read rec buffer reg
         11  line-status              highest     read LSR

        - interrupt pending flag uses reverse logic, 0 = pending, 1 = none
        - interrupt will occur if any of the line status bits are set
        - THRE bit is set when THRE register is emptied into the TSR
*/

BOOLEAN OnInterrupt(PKINTERRUPT InterruptObject, PDEVICE_EXTENSION pdx)
{
    UCHAR uData;
    UCHAR uBitMask;
    int idx = 0;
    UCHAR uIIR;

    if ( pdx->m_pMasterController->bFpgaLoaded == FALSE )
        return FALSE;

    uIIR = pdx->m_pMasterController->ReadByte ( IIR );
    if (  uIIR & 0x01 )
        return FALSE;
    else
        pdx->m_pMasterController->flagISR++;

    PIRP Irp = GetCurrentIrp(&pdx->dqReadWrite);
    if ( Irp )
        if ( Irp->Cancel || AreRequestsBeingAborted(&pdx->dqReadWrite) )
            return TRUE;

    do {
		switch ( uIIR & 0x07 ) {// We are only interrested in the least 3 Bits;
            case 0:  // MSR See MPHC.Doc for description of these bits.
                uData = pdx->m_pMasterController->ReadByte (MSR);        // See what interrupted us
                uBitMask = 0x01;
                while ( uBitMask & 0x0F) {
                    switch ( uData & uBitMask ) {
                        case PC_INT:                // DeltaCD
                            pdx->m_pMasterController->bPhotocellActivated = TRUE;    // Photocell event has occured on the master device.
                            pdx->m_pMasterController->flagPhotoInt++;
                            break;
                        case AMS_STATE_CHANGE:     //Delta RI
									pdx->m_pMasterController->m_AmsActive = false;
									pdx->m_pMasterController->m_bFireAmsEvent = true;
                           break;
                        case PRINT_CYCLE_STATE:                // Delta DSR; DSR=1 -> Print Start; DSR=0 -> Print Finished;
                            switch ( uData & PRINT_CYCLE_ACTIVE ) {    //                     //    cgh 04d07
                                case 0:    // Complete print-cycle is finished.               //    cgh 04d07
                                    pdx->m_pMasterController->m_EndOfPrint = TRUE;            //    cgh 04d10
                                    break;
                                case PRINT_CYCLE_ACTIVE:    // Photo delay is finished and Printing has begun.        cgh 05e24
                                    pdx->m_pMasterController->m_StartOfPrint = TRUE;            //    cgh 04d10
                                    break;
                            }
                            break;
                        case OTB_INT: // Delta CTS ->This is the Over-the-Top-Bus Interrupt.  Meaning a slave device interrupted us.
                            break;    // Removed cgh 05b19
                    }
                    uBitMask <<= 1;
                }
                break;
            case 1:
                idx = 0;
                do {
                    if (pdx->m_Devices[idx].IsPresent() && ( !pdx->m_Devices[idx].bMaster ) ) {
                        if ( pdx->m_Devices[idx].OnInterrupt() ) {
                            pdx->m_pMasterController->flagReserved2++;
						}
                        break;
                    }
                    idx++;
                }while ( idx < MAX_DEVICES );
                pdx->m_pMasterController->flagReserved1++;
                break;
            case 2:  // THR Empty
                break;
            case 4:  // Data Aval.
                pdx->m_pMasterController->flagSerialInt++;
                pdx->m_pMasterController->ProcessSerialData ();
                break;
            case 6:  // LSR Error
                // This sectionn handles the serial UART of the card.
                uData = pdx->m_pMasterController->ReadByte ( LSR );
                pdx->m_pMasterController->flagSerialInt++;
                break;
            default:
                break;
        }
        uIIR = pdx->m_pMasterController->ReadByte ( IIR );
    } while (  !( uIIR & 0x01 ) );

    // cgh 05b19
    UCHAR uStatus;
    int i;
    uStatus = pdx->m_pMasterController->ReadOTBStatusReg();
    do {
        int IrqID = 7;
        pdx->m_pMasterController->flagOTBInt++;
        UCHAR uOTBMask = 0x80;    

        while ( uOTBMask ) {
            if ( uStatus & uOTBMask ) {
                i = 0;
                do {
                    if (pdx->m_Devices[i].m_OTBIrqId == IrqID) {
                        pdx->m_Devices[i].OnInterrupt();
                        break;
                    }
                    i++;
                }while ( i < MAX_DEVICES );
            }
            uOTBMask >>= 1;
            IrqID--;
        }
        uStatus = pdx->m_pMasterController->ReadOTBStatusReg();
    } while ( uStatus );

 	KeInsertQueueDpc( &pdx->DpcCustomQue, NULL, pdx );

   return TRUE;
}

#pragma PAGEDCODE
NTSTATUS StartDevice(PDEVICE_OBJECT fdo, PCM_PARTIAL_RESOURCE_LIST raw, PCM_PARTIAL_RESOURCE_LIST translated)
{
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;
	NTSTATUS status;
	KIRQL irql;
	KINTERRUPT_MODE mode;
	KAFFINITY affinity;
	BOOLEAN irqshare;
	BOOLEAN gotinterrupt = FALSE;

	PHYSICAL_ADDRESS portbase;
	BOOLEAN gotport = FALSE;

	PHYSICAL_ADDRESS membase;
	BOOLEAN gotmemory = FALSE;
	unsigned long i = 0;
	
	DbgPrint ( "%s - Start Device.\n", DRIVERNAME);
	if (!translated) {
		DbgPrint ( "%s - No Resources Assigned\n", DRIVERNAME);
		return STATUS_DEVICE_CONFIGURATION_ERROR;		// no resources assigned??
	}

	PCM_PARTIAL_RESOURCE_DESCRIPTOR resource = translated->PartialDescriptors;
	ULONG nPorts = 0;
	ULONG nres = translated->Count;
	for ( i = 0; i < MAX_DEVICES; i++ )
	{
		if ( (status = pdx->m_Devices[i].Initialize() ) != STATUS_SUCCESS)
		{
			DbgPrint ( "%s - Card Object %i Failed initialize()\n", DRIVERNAME, i );
			return status;
		}
	}

	for (i = 0; i < nres; ++i, ++resource)
	{
		switch (resource->Type)
		{					// switch on resource type
			case CmResourceTypePort:
				pdx->m_Devices[nPorts].PhyAddress = resource->u.Port.Start;
				pdx->m_Devices[nPorts].PortLen = resource->u.Port.Length;
				pdx->m_Devices[nPorts].bMappedPort = (resource->Flags & CM_RESOURCE_PORT_IO) == 0;
				nPorts++;
//				if ( nPorts == MAX_DEVICES )
					gotport = TRUE;
				break;
			case CmResourceTypeInterrupt:
				irql = (KIRQL) resource->u.Interrupt.Level;
				pdx->IntVectorNum = resource->u.Interrupt.Vector;
				affinity = resource->u.Interrupt.Affinity;
				mode = (resource->Flags == CM_RESOURCE_INTERRUPT_LATCHED)
					? Latched : LevelSensitive;
				irqshare = resource->ShareDisposition == CmResourceShareShared;
				gotinterrupt = TRUE;
				break;
			case CmResourceTypeMemory:
				membase = resource->u.Memory.Start;
				pdx->memlength = resource->u.Memory.Length;
				switch ( resource->Flags ) {
					case CM_RESOURCE_MEMORY_READ_WRITE:
						DbgPrint("%s - Memory Base: %X Length %lu [READ_WRITE]\n", DRIVERNAME, membase, pdx->memlength);
						break;
					case CM_RESOURCE_MEMORY_WRITE_ONLY:
						DbgPrint("%s - Memory Base: %X Length %lu [WRITE_ONLY]\n", DRIVERNAME, membase, pdx->memlength);
						break;
					case CM_RESOURCE_MEMORY_READ_ONLY:
						DbgPrint("%s - Memory Base: %X Length %lu [READ_ONLY]\n", DRIVERNAME, membase, pdx->memlength);
						break;
				}
				gotmemory = TRUE;
				break;
			default:
				KdPrint((DRIVERNAME " - Unexpected I/O resource type %d\n", resource->Type));
				break;
		}					// switch on resource type
	}

	pdx->SerialDataEvent = NULL;

   // Verify that we got all the resources we were expecting
//	if ( !(TRUE && gotinterrupt && gotport && gotmemory) ) {
//		if ( !gotport )
//			KdPrint((DRIVERNAME " - Didn't get expected I/O resources.\n"));
//		if ( !gotinterrupt )
//			KdPrint((DRIVERNAME " - Didn't get expected Interrupt resource.\n"));
//		if ( !gotmemory )
//			KdPrint((DRIVERNAME " - Didn't get expected Memory resource.\n"));
//		return STATUS_DEVICE_CONFIGURATION_ERROR;
//	}

	if ( !(TRUE && gotinterrupt && gotport) ) {
		if ( !gotport )
			KdPrint((DRIVERNAME " - Didn't get expected I/O resources.\n"));
		if ( !gotinterrupt )
			KdPrint((DRIVERNAME " - Didn't get expected Interrupt resource.\n"));
		return STATUS_DEVICE_CONFIGURATION_ERROR;
	}

	ULONG lowAddr = 0xFFFFFFFF;
	for ( i = 0; i < nPorts; i++ ) {
		if (pdx->m_Devices[i].bMappedPort)
		{
			PUCHAR Address;
			Address = (PUCHAR) MmMapIoSpace(pdx->m_Devices[i].PhyAddress, pdx->m_Devices[i].PortLen, MmNonCached);
			if (!Address) {
				KdPrint((DRIVERNAME " - Unable to map port range %I64X, length %X\n",pdx->m_Devices[i].PhyAddress, pdx->m_Devices[i].PortLen));
				return STATUS_INSUFFICIENT_RESOURCES;
			}
			pdx->m_Devices[i].AssignAddress ( Address );
		}
		else
			pdx->m_Devices[i].AssignAddress ( (PUCHAR) pdx->m_Devices[i].PhyAddress.QuadPart);

		pdx->m_Devices[i].m_PhysicalIrq = 0x0F & (UCHAR)pdx->IntVectorNum;

		// The lowest address will be the Master board, the one that maps its interrupt to the ISA Bus.
		if ( pdx->m_Devices[i].IsPresent() )
		{
			if ( lowAddr > (ULONG)pdx->m_Devices[i].GetIOAddress() )
				lowAddr = (ULONG)pdx->m_Devices[i].GetIOAddress();
		}
	}

	int nIrqNum = 1;
	for ( i = 0; i < nPorts; i++ ) 
	{
		if ( ((ULONG)pdx->m_Devices[i].GetIOAddress()) == lowAddr ) 
		{
			pdx->m_Devices[i].SetInterruptMode ( MASTER );
			pdx->m_pMasterController = &pdx->m_Devices[i];
			DbgPrint( "%s - SetInterrputMode to MASTER for 0x%X \n", DRIVERNAME, lowAddr);
		}
		else
		{
			pdx->m_Devices[i].m_OTBIrqId = nIrqNum++;
			pdx->m_Devices[i].m_PhysicalIrq = 0x0F & (UCHAR)pdx->IntVectorNum;
			pdx->m_Devices[i].SetInterruptMode ( SLAVE );
			if ( pdx->m_Devices[i].IsPresent() )
				DbgPrint( "%s - SetInterrputMode to SLAVE for 0x%X \n", DRIVERNAME, lowAddr);
		}
	}

	DbgPrint ( "%s - Initializing Isr SpinLock\n", DRIVERNAME );
	KeInitializeSpinLock ( &pdx->IsrSpinLock );

	DbgPrint ("%s - Connecting Interrupt\n", DRIVERNAME );
	status = IoConnectInterrupt(&pdx->InterruptObject, (PKSERVICE_ROUTINE) OnInterrupt,
		(PVOID) pdx, NULL, pdx->IntVectorNum, irql, irql, mode, irqshare, affinity, FALSE);

	if (!NT_SUCCESS(status)) 
	{
		DbgPrint("%s - IoConnectInterrupt failed - 0x%X\n", DRIVERNAME, status);
		return status;
	}
	
	DbgPrint ("%s - StartDevice Successful.\n", DRIVERNAME );
	return STATUS_SUCCESS;
}

#pragma LOCKEDCODE
VOID RunWriteMajorSync (PDEVICE_EXTENSION pdx)
{
	PIRP Irp = GetCurrentIrp( &pdx->dqReadWrite );
	PIO_STACK_LOCATION IrpStack = IoGetCurrentIrpStackLocation(Irp);
	ULONG InputLength		= IrpStack->Parameters.Write.Length;
	PVOID SystemBuffer	= Irp->AssociatedIrp.SystemBuffer;

	ULONG info = 0;
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ( "Write Major Called" );
	Irp->IoStatus.Status = status;
	Irp->IoStatus.Information = info;
	IoRequestDpc(pdx->DeviceObject, Irp, pdx);
}

VOID RunReadMajorSync (PDEVICE_EXTENSION pdx)
{
	PIRP Irp = GetCurrentIrp( &pdx->dqReadWrite );
	PIO_STACK_LOCATION IrpStack = IoGetCurrentIrpStackLocation(Irp);
	ULONG OutputLength	= IrpStack->Parameters.DeviceIoControl.InputBufferLength;
	PVOID SystemBuffer	= Irp->AssociatedIrp.SystemBuffer;

	ULONG info = 0;
	NTSTATUS status = STATUS_SUCCESS;
	Irp->IoStatus.Status = status;
	Irp->IoStatus.Information = info;
	IoRequestDpc(pdx->DeviceObject, Irp, pdx);
}

VOID RunIOCTLSync (PDEVICE_EXTENSION pdx) 
{
	PIRP Irp = GetCurrentIrp( &pdx->dqReadWrite );
	ULONG info = 0;
	NTSTATUS status = STATUS_SUCCESS;
	PIO_STACK_LOCATION IrpStack = IoGetCurrentIrpStackLocation(Irp);
	ULONG ControlCode		= IrpStack->Parameters.DeviceIoControl.IoControlCode;
	ULONG InputLength		= IrpStack->Parameters.DeviceIoControl.InputBufferLength;
	ULONG OutputLength	= IrpStack->Parameters.DeviceIoControl.OutputBufferLength;
	PVOID SystemBuffer	= Irp->AssociatedIrp.SystemBuffer;
	
	pdx->bActiveRequest = TRUE;
	pdx->bActiveRequestComplete = FALSE;

	switch ( ControlCode ) 	// process request
	{
		case IOCTL_IMAGE_UPDATE:
			{
				status = STATUS_SUCCESS;
				info = 0;

				if (InputLength < sizeof(tagImgUpdateHdr)) {	// Input Buffer too Small
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - IOCTL_IMAGE_UPDATE: Input Paramater INVALID\n", DRIVERNAME);
					break;
				}

				PUCHAR pData = (PUCHAR) SystemBuffer;
				PHCB *pPHCB  = (PHCB *) SystemBuffer;
				BOOLEAN found = FALSE;
				tagImgUpdateHdr ImageHdr;
				RtlCopyMemory ( &ImageHdr, pPHCB->Data, sizeof ( tagImgUpdateHdr ) );
				
				// Skip over Print Head Control Block.
				pData += sizeof ( PHCB );

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].GetIOAddress() == pPHCB->lAddress ) {
						pdx->m_Devices[i].TxImage ( ImageHdr._wStartCol, ImageHdr._wNumCols, ImageHdr._nBPC, pData );
						found = TRUE;
						break;
					}
				}
				if ( !found )
				{
					DbgPrint ( "Unable to Locate Device Address %X", pPHCB->lAddress);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_REGISTER_PHOTO_EVENT:
		case IOCTL_REGISTER_EOP_EVENT:		// End-of-Print Event	cgh 04d10
		case IOCTL_REGISTER_AMS_EVENT:		// cgh 05e25
			{
				status = STATUS_SUCCESS;
				info = 0;
				if (InputLength < sizeof(ULONG) * 2) {	
					DbgPrint("Invalid Input Parameter\n");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}
				PULONG p = (PULONG)SystemBuffer;
				ULONG Address = *p++;
				HANDLE hEvent = *(PHANDLE)p;
				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].GetIOAddress() == Address ) {
						switch ( ControlCode ) {
						case IOCTL_REGISTER_PHOTO_EVENT:
							status = pdx->m_Devices[i].RegisterPhotoEvent ( hEvent );
							break;
						case IOCTL_REGISTER_EOP_EVENT:
							status = pdx->m_Devices[i].RegisterEOPEvent ( hEvent );
							break;
						case IOCTL_REGISTER_AMS_EVENT:
							status = pdx->m_Devices[i].RegisterAmsEvent ( hEvent );
							break;
						}
						break;
					}
				}

				if (status != STATUS_SUCCESS) {
					switch (status) {
						case STATUS_OBJECT_TYPE_MISMATCH:
							DbgPrint ("ObReferenceObjectByHandle Failed - Object Type Mismatch\n");
							break;
						case STATUS_ACCESS_DENIED:
							DbgPrint ("ObReferenceObjectByHandle Failed - Access Denied\n");
							break;
						case STATUS_INVALID_HANDLE:
							DbgPrint ("ObReferenceObjectByHandle Failed - Invalid Handle\n");
							break;
						default:
							DbgPrint ("ObReferenceObjectByHandle Failed - UnKnown Error\n");
					}
				}
			}
			break;
		case IOCTL_REGISTER_DRIVER_PHOTO_EVENT:
		case IOCTL_REGISTER_DRIVER_EOP_EVENT:
			{
				status = STATUS_SUCCESS;
				info = 0;
				if (InputLength < sizeof(ULONG) * 2) {	
					DbgPrint("Invalid Input Parameter\n");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}
				PULONG p = (PULONG)SystemBuffer;
				ULONG Address = *p++;
				HANDLE hEvent = *(PHANDLE)p;
				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].GetIOAddress() == Address ) {
						switch ( ControlCode ) {
						case IOCTL_REGISTER_DRIVER_PHOTO_EVENT:
							status = pdx->m_Devices[i].RegisterDriverPhotoEvent ( hEvent );
							break;
						case IOCTL_REGISTER_DRIVER_EOP_EVENT:
							status = pdx->m_Devices[i].RegisterDriverEOPEvent ( hEvent );
							break;
						}
						break;
					}
				}

				if (status != STATUS_SUCCESS) {
					switch (status) {
						case STATUS_OBJECT_TYPE_MISMATCH:
							DbgPrint ("ObReferenceObjectByHandle Failed - Object Type Mismatch\n");
							break;
						case STATUS_ACCESS_DENIED:
							DbgPrint ("ObReferenceObjectByHandle Failed - Access Denied\n");
							break;
						case STATUS_INVALID_HANDLE:
							DbgPrint ("ObReferenceObjectByHandle Failed - Invalid Handle\n");
							break;
						default:
							DbgPrint ("ObReferenceObjectByHandle Failed - UnKnown Error\n");
					}
				}
			}
			break;
		case IOCTL_UNREGISTER_PHOTO_EVENT:
		case IOCTL_UNREGISTER_EOP_EVENT:		// End-of-Print Event	cgh04d10
		case IOCTL_UNREGISTER_AMS_EVENT:		// cgh 05e25
			{
				status = STATUS_SUCCESS;
				info = 0;
				if (InputLength < sizeof(ULONG)) {	
					DbgPrint("Invalid Input Parameter\n");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}
				ULONG Address = *( ( PULONG ) SystemBuffer);
				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].GetIOAddress() == Address ) {
						switch ( ControlCode ) {
						case IOCTL_UNREGISTER_PHOTO_EVENT://IOCTL_REGISTER_PHOTO_EVENT:
							status = pdx->m_Devices[i].UnregisterPhotoEvent ( );
							break;
						case IOCTL_UNREGISTER_EOP_EVENT:
							status = pdx->m_Devices[i].UnregisterEOPEvent ( );
							break;
						case IOCTL_UNREGISTER_AMS_EVENT:
							status = pdx->m_Devices[i].UnregisterAmsEvent ( );
							break;
						}
						break;
					}
				}
			}
			break;
		case IOCTL_UNREGISTER_DRIVER_PHOTO_EVENT:
		case IOCTL_UNREGISTER_DRIVER_EOP_EVENT:
			{
				status = STATUS_SUCCESS;
				info = 0;
				if (InputLength < sizeof(ULONG)) {	
					DbgPrint("Invalid Input Parameter\n");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}
				ULONG Address = *( ( PULONG ) SystemBuffer);
				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].GetIOAddress() == Address ) {
						switch ( ControlCode ) {
						case IOCTL_UNREGISTER_DRIVER_PHOTO_EVENT:
							status = pdx->m_Devices[i].UnregisterDriverPhotoEvent ( );
							break;
						case IOCTL_UNREGISTER_DRIVER_EOP_EVENT:
							status = pdx->m_Devices[i].UnregisterDriverEOPEvent ( );
							break;
						}
						break;
					}
				}
			}
			break;
		case IOCTL_READ_SERIAL_DATA:
			{
				status = STATUS_SUCCESS;
				info = 0;
				DbgPrint ( "%s - IOCTL_READ_SERIAL_DATA\n", DRIVERNAME );
				BOOLEAN found = FALSE;
				if ( InputLength < sizeof(HANDLE) )
				{	
					DbgPrint("Invalid Input Parameter");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}

				if (OutputLength < SERIAL_BUFFER_SIZE )
				{	
					DbgPrint("Invalid Output Buffer Size");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}

				ULONG Address	= *( (PULONG) SystemBuffer );
				PUCHAR pData	=  (PUCHAR) SystemBuffer;

				for ( int i = 0; i < MAX_DEVICES; i++ ) 
				{
					if ( pdx->m_Devices[i].IsPresent() )
					{
						if ( pdx->m_Devices[i].PhyAddress.QuadPart == Address ) 
						{
							DbgPrint ( "%s - Getting Serial Data from 0x%X\n", DRIVERNAME, pdx->m_Devices[i].PhyAddress.QuadPart );
							info = pdx->m_Devices[i].GetSerialData ( pData );
							found = TRUE;
						}
					}
				}

				if ( found == FALSE ) 
				{
					info = 0;
					DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, Address);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_SET_MASTER_MODE:
			{
				status = STATUS_SUCCESS;
				info = 0;
				DbgPrint ( "%s - IOCTL_SET_INTERRUPT_MODE\n", DRIVERNAME );
				BOOLEAN found = FALSE;
				if (InputLength < sizeof(HANDLE)) {	
					DbgPrint("Invalid Input Parameter");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}

				ULONG Address = *( (PULONG) SystemBuffer );

				for ( int i = 0; i < MAX_DEVICES; i++ ) 
				{
					if ( pdx->m_Devices[i].IsPresent() )
					{
						if ( pdx->m_Devices[i].PhyAddress.QuadPart == Address ) 
						{
							DbgPrint ( "%s - Setting %x to MASTER Mode", DRIVERNAME, pdx->m_Devices[i].PhyAddress.QuadPart );
							pdx->m_pMasterController = &pdx->m_Devices[i];
							pdx->m_Devices[i].SetInterruptMode ( MASTER );
							found = TRUE;
						}
						else
						{
							DbgPrint ( "%s - Setting %x to SLAVE Mode", DRIVERNAME, pdx->m_Devices[i].PhyAddress.QuadPart );
							pdx->m_Devices[i].SetInterruptMode ( SLAVE );
						}
					}
				}

				if ( found == FALSE ) 
				{
					DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, Address);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_WRITE_FPGA:
			{
				status = STATUS_SUCCESS;
				info = 0;
				DbgPrint ( "%s - IOCTL_WRITE_FPGA\n", DRIVERNAME );
				BOOLEAN found = FALSE;
				ULONG Address = *( (PULONG) SystemBuffer );
				PUCHAR pData = (PUCHAR) SystemBuffer;
				pData += sizeof (ULONG);

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].PhyAddress.QuadPart == Address ) {
						DbgPrint ( "%s - Programming FPGA at %x", DRIVERNAME, Address);
						status = pdx->m_Devices[i].ProgramFPGA ( pData, InputLength - sizeof ( ULONG ) );
						found = TRUE;
						break;
					}
				}

				if ( found == FALSE ) {
					info = 0;
					DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, Address);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_ACTIVATE:
			{
				status = STATUS_SUCCESS;
				info = 0;
				DbgPrint ( "%s - IOCTL_ACTIVATE\n", DRIVERNAME );
				BOOLEAN found = FALSE;
				ULONG Address = *( (PULONG) SystemBuffer );
				PUCHAR pData = (PUCHAR) SystemBuffer;
				pData += sizeof (ULONG);

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].PhyAddress.QuadPart == Address ) {
						DbgPrint ( "%s - activating %x", DRIVERNAME, Address);
						status = pdx->m_Devices[i].Activate ();
						found = TRUE;
						break;
					}
				}

				if ( found == FALSE ) {
					info = 0;
					DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, Address);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_CLEAR_BUFFER:
			{
				status = STATUS_SUCCESS;
				info = 0;
				DbgPrint ( "%s - IOCTL_CLEAR_BUFFER\n", DRIVERNAME );
				BOOLEAN found = FALSE;
				if ( InputLength < sizeof(ULONG) )
				{	
					DbgPrint("Invalid Input Parameter\n");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}

				ULONG Address	= *( (PULONG) SystemBuffer );
				for ( int i = 0; i < MAX_DEVICES; i++ ) 
				{
					if ( pdx->m_Devices[i].IsPresent() )
					{
						if ( pdx->m_Devices[i].PhyAddress.QuadPart == Address ) 
						{
							pdx->m_Devices[i].ClearOnBoardRam();
							found = TRUE;
						}
					}
				}

				if ( found == FALSE ) 
				{
					DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, Address);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_READ_CTRL_REG:
			{
				status = STATUS_SUCCESS;
				info = 0;
				if ( InputLength < sizeof (PHCB) ) {
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - Invalid Input Paramater.\n", DRIVERNAME );
					break;
				}

				if ( OutputLength < sizeof (PHCB) ) {
					status = STATUS_INVALID_BUFFER_SIZE;
					DbgPrint("%s - Invalid Output Buffer.\n", DRIVERNAME );
					break;
				}

				PHCB *pPHCB = (PHCB *) SystemBuffer;
				BOOLEAN found = FALSE;

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].GetIOAddress() == pPHCB->lAddress ) {
						status = pdx->m_Devices[i].ReadCtrlReg ( pPHCB, info);
						info = sizeof ( PHCB );
						found = TRUE;
						break;
					}
				}
				
				if ( found == FALSE ) {
					DbgPrint ("%s - Unable to locate Device Address: %X", DRIVERNAME, pPHCB->lAddress);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_WRITE_CTRL_REG:
			{
				status = STATUS_SUCCESS;
				info = 0;

				if (InputLength < sizeof( PHCB ) ) {	// Output Buffer too Small
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - Invalid Input Parameter.\n", DRIVERNAME );
					break;
				}
		
				PHCB *pPHCB = ( PHCB * ) SystemBuffer;
				BOOLEAN found = FALSE;

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].GetIOAddress() == pPHCB->lAddress ) {
						status = pdx->m_Devices[i].WriteCtrlReg( pPHCB, info);
						found = TRUE;
						break;
					}
				}
				if ( found == FALSE ) {
					DbgPrint ("%s - Unable to locate Address %X", DRIVERNAME, pPHCB->lAddress);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_READ_WATCHDOG:
			{
				status = STATUS_SUCCESS;
				info = 0;

				if (InputLength < sizeof( PHCB ) ) {	// Output Buffer too Small
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - Invalid Input Parameter.\n", DRIVERNAME );
					break;
				}
		
				PHCB *pPHCB = ( PHCB * ) SystemBuffer;
				BOOLEAN found = FALSE;

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].GetIOAddress() == pPHCB->lAddress ) {
						status = pdx->m_Devices[i].ReadWatchdog ( pPHCB, info);
						found = TRUE;
						break;
					}
				}
				if ( found == FALSE ) {
					DbgPrint ("%s - Unable to locate Address %X", DRIVERNAME, pPHCB->lAddress);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_WRITE_WATCHDOG:
			{
				status = STATUS_SUCCESS;
				info = 0;

				if (InputLength < sizeof( PHCB ) ) {	// Output Buffer too Small
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - Invalid Input Parameter.\n", DRIVERNAME );
					break;
				}
		
				PHCB *pPHCB = ( PHCB * ) SystemBuffer;
				BOOLEAN found = FALSE;

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].GetIOAddress() == pPHCB->lAddress ) {
						status = pdx->m_Devices[i].WriteWatchdog (pPHCB, info);
						found = TRUE;
						break;
					}
				}
				if ( found == FALSE ) {
					DbgPrint ("%s - Unable to locate Address %X", DRIVERNAME, pPHCB->lAddress);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_SET_SERIAL_PARAMS:
			{
				status = STATUS_SUCCESS;
				info = 0;

				if (InputLength < sizeof( PHCB ) ) {	// Output Buffer too Small
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - Invalid Input Parameter.\n", DRIVERNAME );
					break;
				}
		
				PHCB *pPHCB = ( PHCB * ) SystemBuffer;
				tagSerialParams *Port = ( tagSerialParams * ) pPHCB->Data;
				
				DbgPrint ( "%s - Serial Params: Baud %lu, Data %i, Parity %i, Stop %i", DRIVERNAME, Port->BaudRate, Port->DataBits, Port->Parity, Port->StopBits);
				BOOLEAN found = false;
				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].PhyAddress.QuadPart == pPHCB->lAddress ) {
						found = TRUE;
						status = pdx->m_Devices[i].SetSerialParams ( Port );
						break;
					}
				}

				if ( found == FALSE ) {
					DbgPrint( "%s - Address Located. Writing Params\n", DRIVERNAME );
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		default:
			DbgPrint ( "%s - Invalid Device Request: Control Code: 0x%X\n", DRIVERNAME, ControlCode );
			status = STATUS_INVALID_DEVICE_REQUEST;
			break;

	}						// process request
	Irp->IoStatus.Status = status;
	Irp->IoStatus.Information = info;
	pdx->bActiveRequestComplete = TRUE;
	IoRequestDpc(pdx->DeviceObject, Irp, pdx);
}

VOID StartIo(IN PDEVICE_OBJECT fdo, IN PIRP Irp)
{
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;
	PIO_STACK_LOCATION IrpStack = IoGetCurrentIrpStackLocation(Irp);
	NTSTATUS status = IoAcquireRemoveLock(&pdx->RemoveLock, Irp);
	ULONG ControlCode		= IrpStack->Parameters.DeviceIoControl.IoControlCode;
	ULONG info = 0;

	if (!NT_SUCCESS(status)) {
		CompleteRequest(Irp, status, 0);
		return;
	}

	switch ( IrpStack->MajorFunction )	{
		case IRP_MJ_DEVICE_CONTROL:
			if(!KeSynchronizeExecution(pdx->InterruptObject, (PKSYNCHRONIZE_ROUTINE)RunIOCTLSync, (PVOID)pdx))
				status = STATUS_UNSUCCESSFUL;
			else
				return;
			break;
		case IRP_MJ_WRITE:
			if(!KeSynchronizeExecution(pdx->InterruptObject, (PKSYNCHRONIZE_ROUTINE)RunWriteMajorSync, (PVOID)pdx))
				status = STATUS_UNSUCCESSFUL;
			else
				return;
			break;
		case IRP_MJ_READ:
			if(!KeSynchronizeExecution(pdx->InterruptObject, (PKSYNCHRONIZE_ROUTINE)RunReadMajorSync, (PVOID)pdx))
				status = STATUS_UNSUCCESSFUL;
			else
				return;
			break;
		default:
			status = STATUS_INVALID_DEVICE_REQUEST;
	}
	IoReleaseRemoveLock(&pdx->RemoveLock, Irp);
	CompleteRequest(Irp, status, info);
}

#pragma PAGEDCODE

VOID StopDevice(IN PDEVICE_OBJECT fdo, BOOLEAN oktouch /* = FALSE */)
{
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;

	DbgPrint ( "%s- Disconnecting interrupt", DRIVERNAME);
	if (pdx->InterruptObject)
	{
		// TODO prevent device from generating more interrupts if possible
		IoDisconnectInterrupt(pdx->InterruptObject);
		pdx->InterruptObject = NULL;
	}
}
