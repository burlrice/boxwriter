// MphcCard.h: interface for the CMphcCard class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MPHCCARD_H__B87F81C6_C93A_4A60_B610_EC4E7C10790C__INCLUDED_)
#define AFX_MPHCCARD_H__B87F81C6_C93A_4A60_B610_EC4E7C10790C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Defines.h"
#include "TypeDefs.h"

class CMphcCard  
{
public:
	CMphcCard();
	~CMphcCard();

protected:
	UCHAR LCRSave;
	DEVICE_MODE CurMode;
	BOOLEAN m_bPresent;
	PUCHAR m_Address;
   PUCHAR pSerialRxBuffer;
   PUCHAR pSerialTxBuffer;
   PUCHAR pSerialTxTail;
   PUCHAR pSerialRxTail;
   PUCHAR pSerialRxHead;
   PUCHAR pSerialTxHead;

public:
	void ClearDebugFlags ( void );
	unsigned long flagISR;
	unsigned long flagOTBInt;
	unsigned long flagPhotoInt;
	unsigned long flagSerialInt;
	unsigned long flagTachInt;
	unsigned long flagReserved1;
	unsigned long flagReserved2;

	int GetSerialData ( PUCHAR pData );
	void ClearOnBoardRam ( void );
	BOOLEAN OnInterrupt ( void );
	
	NTSTATUS CueSerialData ( PUCHAR pData, ULONG uLen );
	NTSTATUS WriteCtrlReg( PHCB *pPHCB, ULONG &info);
	NTSTATUS ReadCtrlReg( PHCB *pPHCB, ULONG &info);
	NTSTATUS ReadWatchdog (PHCB *pPHCB, ULONG &info);
	NTSTATUS WriteWatchdog (PHCB *pPHCB, ULONG &info);

	void Destroy(void);
	void AssignAddress ( PUCHAR Addr );
	void Set_ICR ( UCHAR data );
	void SetInterruptMode ( BOOLEAN nMode );
	void FirePhotoEvent ( void );
	void FirePrintEvent ( void );	// cgh 04d10
	void SetControlRegister ( UCHAR data );
	void FireSerialEvent ( void );
	void FireAmsEvent ( void );
	void ProcessSerialData ( void );
	void RxImage ( unsigned short wStartCol, unsigned short wNumCols, unsigned char nBPC, unsigned char * pImage );
	void TxImage ( unsigned short wStartCol, unsigned short wNumCols, unsigned char nBPC, unsigned char * pImage );
	BOOLEAN IsPresent(void);
	NTSTATUS Activate (void);
	unsigned long GetIOAddress ( void );
	UCHAR GetControlRegister ( void );
	UCHAR Get_ICR ( void );
	NTSTATUS Initialize ( void );
	NTSTATUS RegisterPhotoEvent ( HANDLE hEvent );
	NTSTATUS UnregisterPhotoEvent( void );
	NTSTATUS RegisterEOPEvent ( HANDLE hEvent );		// cgh 04d10
	NTSTATUS RegisterAmsEvent ( HANDLE hEvent );		// cgh 05e25
	NTSTATUS UnregisterAmsEvent( void );				// cgh 05e25
	NTSTATUS UnregisterEOPEvent( void );				// cgh 04d10
	NTSTATUS RegisterDriverPhotoEvent ( HANDLE hEvent );
	NTSTATUS UnregisterDriverPhotoEvent( void );
	NTSTATUS RegisterDriverEOPEvent ( HANDLE hEvent );
	NTSTATUS UnregisterDriverEOPEvent( void );
	NTSTATUS RegisterSerialEvent ( HANDLE hEvent );
	NTSTATUS UnregisterSerialEvent( void );
	BOOLEAN EnableDataTransferMode ( IN BOOLEAN Enabled, IN BOOLEAN Direction);
	UCHAR ReadOTBStatusReg ( void );
	NTSTATUS SetSerialParams ( tagSerialParams *pParams );
	NTSTATUS ProgramFPGA ( PVOID pCode, ULONG lBytes );
	BOOLEAN EnableCommandMode ( BOOLEAN Enabled );
	BOOLEAN EnableProgrammingMode ( IN BOOLEAN Enabled );
	VOID ReadBuffer ( IN UCHAR offset, IN PUCHAR pData, IN ULONG len );
	UCHAR ReadByte ( IN UCHAR Offset );
	VOID WriteBuffer ( IN UCHAR offset, IN PUCHAR pData, IN ULONG len);
	VOID WriteByte ( IN UCHAR offset, IN UCHAR data );

	UCHAR m_PhysicalIrq;		// Physical Interrupt;  This is assigned by the resource manager.  Should be 0 for Slave Devices.
	UCHAR m_OTBIrqId;			// This is Interrupt Id for slave devices.  The LSB is reserved thus valid Ids are 0x02 - 0x80
	PHYSICAL_ADDRESS PhyAddress;
	ULONG PortLen;
	BOOLEAN bMaster;
	BOOLEAN bMappedPort;
	PKEVENT PhotoEvent;
	PKEVENT EOPEvent;
	PKEVENT PhotoEventDriver;
	PKEVENT EOPEventDriver;
	PKEVENT m_AmsEvent;
	PKEVENT SerialEvent;
	BOOLEAN bSerialDataReady;
	BOOLEAN bPhotocellActivated;
	BOOLEAN m_EndOfPrint;	// cgh 04d10
	BOOLEAN m_StartOfPrint;	// cgh 04d10
	BOOLEAN bFpgaLoaded;
	BOOLEAN m_AmsActive;
	BOOLEAN m_bFireAmsEvent;	// cgh 05e25
	tagImgUpdateHdr m_ImageHdr;
};
#endif // !defined(AFX_MPHCCARD_H__B87F81C6_C93A_4A60_B610_EC4E7C10790C__INCLUDED_)
