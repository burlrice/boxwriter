///////////////////////////////////////////////////////////////////////////////
// Control.cpp -- IOCTL handlers for FxMphc driver
// Copyright (C) 2002 By FoxJet Tech Center
// All rights reserved.
//
//	Author: Chris G. Hodge
//	Version History:
//		1.00	Creation
///////////////////////////////////////////////////////////////////////////////

#include "stddcls.h"
#include "Defines.h"
#include "driver.h"
#include "ioctls.h"

#pragma LOCKEDCODE

PVOID GenericGetSystemAddressForMdl(PMDL mdl) 
{							
	if (!mdl)
		return NULL;

	CSHORT oldfail = mdl->MdlFlags & MDL_MAPPING_CAN_FAIL;
	mdl->MdlFlags |= MDL_MAPPING_CAN_FAIL;

	PVOID address = MmMapLockedPages(mdl, KernelMode);

	if (!oldfail)
		mdl->MdlFlags &= ~MDL_MAPPING_CAN_FAIL;

	return address;
}							

#pragma PAGEDCODE

NTSTATUS DispatchControl(PDEVICE_OBJECT fdo, PIRP Irp)
{
	PAGED_CODE();
	PDEVICE_EXTENSION pdx = (PDEVICE_EXTENSION) fdo->DeviceExtension;

	NTSTATUS status = IoAcquireRemoveLock(&pdx->RemoveLock, Irp);
	if (!NT_SUCCESS(status))
		return CompleteRequest(Irp, status, 0);

	ULONG info = 0;
	PIO_STACK_LOCATION IrpStack = IoGetCurrentIrpStackLocation(Irp);

	ULONG ControlCode		= IrpStack->Parameters.DeviceIoControl.IoControlCode;
	ULONG InputLength		= IrpStack->Parameters.DeviceIoControl.InputBufferLength;
	ULONG OutputLength		= IrpStack->Parameters.DeviceIoControl.OutputBufferLength;
	PVOID SystemBuffer		= Irp->AssociatedIrp.SystemBuffer;

{ //////////////////////////////////////////////////////////////////////
//	DbgPrint (DRIVERNAME " - DispatchControl: Irp->MdlAddress: 0x%p", Irp->MdlAddress);
//	IoReleaseRemoveLock(&pdx->RemoveLock, Irp);
//	return CompleteRequest(Irp, status, info);
} //////////////////////////////////////////////////////////////////////

	switch ( ControlCode ) {						// process request
		case IOCTL_IMAGE_UPDATE_DIRECTIO:
			{
				status = STATUS_SUCCESS;
				info = 0;

				//PUCHAR pData = (PUCHAR)GenericGetSystemAddressForMdl (Irp->MdlAddress);
				PUCHAR pData = (PUCHAR)MmGetSystemAddressForMdlSafe (Irp->MdlAddress, NormalPagePriority);
				PHCB * pPHCB = (PHCB *)pData;

				InputLength = MmGetMdlByteCount (Irp->MdlAddress);

				if (InputLength < sizeof(tagImgUpdateHdr)) {	// Input Buffer too Small
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - IOCTL_IMAGE_UPDATE: Input Paramater INVALID\n", DRIVERNAME);
					break;
				}

				BOOLEAN found = FALSE;
//				tagImgUpdateHdr ImageHdr;
//				RtlCopyMemory ( &ImageHdr, pPHCB->Data, sizeof ( tagImgUpdateHdr ) );
				tagImgUpdateHdr * pImageHdr = (tagImgUpdateHdr *)pPHCB->Data;
				
				// Skip over Print Head Control Block.
				pData += sizeof ( PHCB );

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].GetIOAddress() == pPHCB->lAddress ) {
						pdx->m_Devices[i].TxImage (pImageHdr->_wStartCol, pImageHdr->_wNumCols, pImageHdr->_nBPC, pData);
						found = TRUE;
						break;
					}
				}
				if ( !found )
				{
					DbgPrint ( "Unable to Locate Device Address %X", pPHCB->lAddress);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		// Begin Debug Code
		case IOCTL_RESET_FLAGS:
			{
				status = STATUS_SUCCESS;
				info = 0;
//				DbgPrint ( "%s - IOCTL_RESET_FLAGS\n", DRIVERNAME );
				BOOLEAN found = FALSE;
				if ( InputLength < sizeof(ULONG) )
				{	
					DbgPrint("Invalid Input Parameter\n");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}

				ULONG Address	= *( (PULONG) SystemBuffer );
				for ( int i = 0; i < MAX_DEVICES; i++ ) 
				{
					if ( pdx->m_Devices[i].IsPresent() )
					{
						if ( pdx->m_Devices[i].PhyAddress.QuadPart == Address ) 
						{
							pdx->m_Devices[i].ClearDebugFlags();
							found = TRUE;
						}
					}
				}

				if ( found == FALSE ) 
				{
					DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, Address);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		case IOCTL_GET_FLAGS:
			{
				status = STATUS_SUCCESS;
				info = 0;
				if ( InputLength < sizeof (PHCB) ) {
					status = STATUS_INVALID_PARAMETER;
					DbgPrint( "%s - IOCTL_GET_FLAGS: Input Paramater INVALID\n", DRIVERNAME);
					break;
				}


				PHCB *pPHCB  = (PHCB *) SystemBuffer;
				BOOLEAN found = FALSE;

				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].GetIOAddress() == pPHCB->lAddress ) {
						_DBG_FLAGS *pFlags = ( _DBG_FLAGS * ) pPHCB->Data;
						pFlags->ISRInt		= pdx->m_Devices[i].flagISR;
						pFlags->OTBInt		= pdx->m_Devices[i].flagOTBInt;
						pFlags->PhotoInt	= pdx->m_Devices[i].flagPhotoInt;
						pFlags->SerialInt	= pdx->m_Devices[i].flagSerialInt;
						pFlags->TachInt		= pdx->m_Devices[i].flagTachInt;
						pFlags->Reserved1	= pdx->m_Devices[i].flagReserved1;
						pFlags->Reserved2	= pdx->m_Devices[i].flagReserved2;
						pFlags->uICR		= pdx->m_Devices[i].Get_ICR (); // Burl
						pFlags->uIER		= pdx->m_Devices[i].ReadByte (IER); // Burl

						info = sizeof ( PHCB );
						found = TRUE;
						break;
					}
				}

				if ( found == FALSE ) {
					DbgPrint ("Unable to locate Device Address: %X", pPHCB->lAddress);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		// End Debug Code.
		case IOCTL_GET_VERSION:					
			DbgPrint ("%s - GET_VERSION\n", DRIVERNAME);
			status = STATUS_SUCCESS;
			if (OutputLength < sizeof(_VERSION)) {	// Output Buffer too Small
				status = STATUS_INVALID_BUFFER_SIZE;
				DbgPrint("Invalid Output Lenght.\n");
				break;
			}
			_VERSION Version;
			Version.Major = MAJOR;
			Version.Minor = MINOR;
			Version.Revision = REV;
			RtlCopyMemory (SystemBuffer, (PVOID)&Version, sizeof(_VERSION));
			info = sizeof(_VERSION);
			break;
		case IOCTL_SET_SERIAL_EVENT_HANDLE:
			{
				DbgPrint ("%s - IOCTL_SET_SERIAL_EVENT_HANDLE\n", DRIVERNAME);
				status = STATUS_SUCCESS;
				info = 0;
				if (InputLength < sizeof(HANDLE)) {	
					DbgPrint("Invalid Input Parameter\n");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}

				HANDLE hEvent = *(PHANDLE)SystemBuffer;
				if (pdx->SerialDataEvent) {
					ObDereferenceObject(pdx->SerialDataEvent);
				}
				pdx->SerialDataEvent = NULL;
				if (hEvent) {
					PKEVENT pEvent;
					status = ObReferenceObjectByHandle(hEvent, EVENT_MODIFY_STATE, *ExEventObjectType, UserMode, (PVOID*)&pEvent, NULL);
					if (status != STATUS_SUCCESS) {
						switch (status) {
							case STATUS_OBJECT_TYPE_MISMATCH:
								DbgPrint ("ObReferenceObjectByHandle Failed - Object Type Mismatch\n");
								break;
							case STATUS_ACCESS_DENIED:
								DbgPrint ("ObReferenceObjectByHandle Failed - Access Denied\n");
								break;
							case STATUS_INVALID_HANDLE:
								DbgPrint ("ObReferenceObjectByHandle Failed - Invalid Handle\n");
								break;
							default:
								DbgPrint ("ObReferenceObjectByHandle Failed - UnKnown Error\n");
						}
					}
					else 
						pdx->SerialDataEvent = pEvent;
				}
			}
			break;
		case IOCTL_REGISTER_SERIAL_EVENT:
			{
				status = STATUS_SUCCESS;
				info = 0;
				if (InputLength < sizeof(ULONG) * 2) {	
					DbgPrint("Invalid Input Parameter\n");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}
				PULONG p = (PULONG)SystemBuffer;
				ULONG Address = *p++;
				HANDLE hEvent = *(PHANDLE)p;
				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].GetIOAddress() == Address ) {
						status = pdx->m_Devices[i].RegisterSerialEvent ( hEvent );
						break;
					}
				}

				if (status != STATUS_SUCCESS) {
					switch (status) {
						case STATUS_OBJECT_TYPE_MISMATCH:
							DbgPrint ("ObReferenceObjectByHandle Failed - Object Type Mismatch\n");
							break;
						case STATUS_ACCESS_DENIED:
							DbgPrint ("ObReferenceObjectByHandle Failed - Access Denied\n");
							break;
						case STATUS_INVALID_HANDLE:
							DbgPrint ("ObReferenceObjectByHandle Failed - Invalid Handle\n");
							break;
						default:
							DbgPrint ("ObReferenceObjectByHandle Failed - UnKnown Error\n");
					}
				}
			}
			break;
		case IOCTL_UNREGISTER_SERIAL_EVENT:
			{
				status = STATUS_SUCCESS;
				info = 0;
				if (InputLength < sizeof(ULONG)) {	
					DbgPrint("Invalid Input Parameter\n");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}
				ULONG Address = *( ( PULONG ) SystemBuffer);
				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].GetIOAddress() == Address ) {
						status = pdx->m_Devices[i].UnregisterSerialEvent ( );
						break;
					}
				}
			}
			break;
		case IOCTL_ENUM_IO_ADDRESSES:
			{
				status = STATUS_SUCCESS;
				info = 0;
				if (OutputLength < ( MAX_DEVICES * sizeof(ULONG) ) ) {	
					DbgPrint("Invalid Output Buffer Size -EnumIOAddresses\n");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}
				PULONG p = (PULONG) SystemBuffer;
				for ( int i = 0; i < MAX_DEVICES; i++ ) {
					if ( pdx->m_Devices[i].IsPresent() )
					{
						*p = (ULONG) pdx->m_Devices[i].GetIOAddress();
						p++;
						info += sizeof (ULONG);
					}
				}
			}
			break;
		case IOCTL_READ_AMS_STATE:
		{
				status = STATUS_SUCCESS;
				info = 0;
				DbgPrint ( "%s - IOCTL_READ_AMS_STATE\n", DRIVERNAME );
				BOOLEAN found = FALSE;
				if ( InputLength < sizeof(ULONG) )
				{	
					DbgPrint("Invalid Input Parameter\n");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}

				if (OutputLength < sizeof (BOOLEAN) )
				{	
					DbgPrint("Invalid Output Buffer Size\n");
					status = STATUS_INVALID_BUFFER_SIZE;
					break;
				}

				ULONG Address	= *( (PULONG) SystemBuffer );
				BOOLEAN *pData	=  ( BOOLEAN *) SystemBuffer;

				for ( int i = 0; i < MAX_DEVICES; i++ ) 
				{
					if ( pdx->m_Devices[i].IsPresent() )
					{
						if ( pdx->m_Devices[i].PhyAddress.QuadPart == Address ) 
						{
							*pData = pdx->m_Devices[i].m_AmsActive;
							if ( pdx->m_Devices[i].m_AmsActive )
								KdPrint ((DRIVERNAME " - AMS State for 0x%X is ON\n", pdx->m_Devices[i].PhyAddress.QuadPart ));
							else
								KdPrint ((DRIVERNAME " - AMS State for 0x%X is OFF\n", pdx->m_Devices[i].PhyAddress.QuadPart ));
							found = TRUE;
							info = sizeof ( BOOLEAN );
						}
					}
				}

				if ( found == FALSE ) 
				{
					DbgPrint ( "%s - Unable to locate Address:%X", DRIVERNAME, Address);
					status = STATUS_INVALID_PARAMETER;
				}
			}
			break;
		// Any operation which require changing any of the regesters of the card must be syncronized
		// with the interrupt routine.  Thus we mark them as pending IO so they will be ran later in
		// the RunIOCTLSync routine.
		case IOCTL_REGISTER_PHOTO_EVENT:
		case IOCTL_REGISTER_EOP_EVENT:		// cgh 04d10
		case IOCTL_UNREGISTER_PHOTO_EVENT:
		case IOCTL_UNREGISTER_EOP_EVENT:		// cgh 04d10
		case IOCTL_REGISTER_DRIVER_PHOTO_EVENT:
		case IOCTL_REGISTER_DRIVER_EOP_EVENT:
		case IOCTL_UNREGISTER_DRIVER_PHOTO_EVENT:
		case IOCTL_UNREGISTER_DRIVER_EOP_EVENT:
		case IOCTL_REGISTER_AMS_EVENT:		// cgh 05e25
		case IOCTL_UNREGISTER_AMS_EVENT:		// cgh 05e25
		case IOCTL_IMAGE_UPDATE:
		case IOCTL_WRITE_FPGA:
		case IOCTL_ACTIVATE:
		case IOCTL_READ_CTRL_REG:
		case IOCTL_WRITE_CTRL_REG:
		case IOCTL_SET_SERIAL_PARAMS:
		case IOCTL_READ_IMAGE:
		case IOCTL_SET_MASTER_MODE:
		case IOCTL_READ_SERIAL_DATA:
		case IOCTL_CLEAR_BUFFER:
		case IOCTL_GET_STATE:
		case IOCTL_SET_CONFIG:
		case IOCTL_READ_WATCHDOG:
		case IOCTL_WRITE_WATCHDOG: 
			IoMarkIrpPending(Irp);
			StartPacket(&pdx->dqReadWrite, fdo, Irp, OnCancelReadWrite);
			return STATUS_PENDING;
			break;
		default:
			status = STATUS_INVALID_DEVICE_REQUEST;
			break;
	}						// process request
	IoReleaseRemoveLock(&pdx->RemoveLock, Irp);
	return CompleteRequest(Irp, status, info);
}
