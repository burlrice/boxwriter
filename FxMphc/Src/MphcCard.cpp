// MphcCard.cpp: implementation of the CMphcCard class.
//
//////////////////////////////////////////////////////////////////////

#include "stddcls.h"
#include "MphcCard.h"
#include "TypeDefs.h"
#include "Driver.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
//
// Classes are general not used in driver development, but is seems to 
// make what might other wise be complicated code much more readable.
//
//////////////////////////////////////////////////////////////////////
CMphcCard::CMphcCard()
{
	// Since the operator::new is not availible at the driver level this will never get called.
}

CMphcCard::~CMphcCard()
{
	// Do not call this manaully, linker better not let you anyway!!!!
}

NTSTATUS CMphcCard::Initialize()
{
	NTSTATUS status = STATUS_SUCCESS;

	m_PhysicalIrq		= 0x00;
	m_OTBIrqId			= 0x00;
	m_Address			= NULL;
	m_bPresent			= FALSE;
	bMappedPort			= FALSE;
	PhotoEvent			= NULL;	
	EOPEvent			= NULL;
	PhotoEventDriver	= NULL;	
	EOPEventDriver		= NULL;
	bPhotocellActivated = FALSE;
	bFpgaLoaded			= FALSE;
	pSerialTxBuffer		= NULL;
	pSerialTxTail		= pSerialTxHead = pSerialTxBuffer;
	pSerialRxBuffer		= NULL;
	pSerialRxTail		= pSerialRxHead = pSerialRxBuffer;
	
	CurMode = NORMAL;
	flagISR = 0;
	flagOTBInt = 0;
	flagPhotoInt = 0;
	flagSerialInt = 0;
	flagTachInt = 0;
	flagReserved1 = 0;
	flagReserved2 = 0;

	bSerialDataReady = FALSE;
	m_AmsActive = FALSE;
	m_StartOfPrint = false;
	m_EndOfPrint = false;
	return status;
}

UCHAR CMphcCard::ReadByte( IN UCHAR Offset )
{
	UCHAR data;
	if ( bMappedPort ) 	// Memory mapped addressing 
		data = READ_REGISTER_UCHAR ( m_Address + Offset );
	else
		data = READ_PORT_UCHAR ( m_Address + Offset );
	return data;
}

VOID CMphcCard::ReadBuffer ( IN UCHAR offset, IN PUCHAR pData, IN ULONG len)
{
	if ( bMappedPort ) 	// Memory mapped addressing 
		READ_REGISTER_BUFFER_UCHAR ( m_Address + offset, pData, len);
	else 
		READ_PORT_BUFFER_UCHAR ( m_Address + offset, pData, len);
}

VOID CMphcCard::WriteBuffer (IN UCHAR offset, IN PUCHAR pData, IN ULONG len)
{
	if ( bMappedPort ) 	// Memory mapped addressing 
		WRITE_REGISTER_BUFFER_UCHAR ( m_Address + offset, pData, len );
	else 
		WRITE_PORT_BUFFER_UCHAR ( m_Address + offset, pData, len );
}

VOID CMphcCard::WriteByte (IN UCHAR offset, IN UCHAR data)
{
	if ( bMappedPort ) 	// Memory mapped addressing 
		WRITE_REGISTER_UCHAR ( m_Address + offset, data );
	else 
		WRITE_PORT_UCHAR ( m_Address + offset, data );
}

//////////////////////////////////////////////////////////////////////
//	It is important to realize that none of the functions which require
// ANY of the Enable Functions except for EnableProgrammingMode will 
// work until the FPGA is programmed.  I wuold like to programm the FPGA
// from the driver but since this must run on Win98 I do no know how
// to at this time as the Zwxxx functions for file IO are not available
// until NT.
//////////////////////////////////////////////////////////////////////
//	FPGA is told that programming is about to begin by setting the
//		UART for 8 Databits, 1 Parity, and 2 Stopbits.  Reset to 8,N,1 
//		before actual programming begins.
//////////////////////////////////////////////////////////////////////
BOOLEAN CMphcCard::EnableProgrammingMode ( IN BOOLEAN Enabled )
{
	BOOLEAN result = FALSE;
	if ( Enabled == TRUE ) {
		if ( CurMode == NORMAL ) 
		{
			ReadByte ( MSR);
			WriteByte ( LCR, LCR_8_MARK_2);	// Tell UART we want to program it.
			WriteByte ( MCR, DTR);
			WriteByte ( LCR, LCR_8_NONE_1 );	// Reset to 8,N,1
			CurMode = PROGRAM;
			result = TRUE;
		}
	}
	else
	{
		if ( CurMode == PROGRAM ) {
			WriteByte ( LCR, 0x03 );
			CurMode = NORMAL;
			result = TRUE;
		}
	}
   if ( result == FALSE )
      DbgPrint ( "%s - Error Enabling Program Mode\n", DRIVERNAME );
	return result;
}

BOOLEAN CMphcCard::EnableDataTransferMode(BOOLEAN Enabled, BOOLEAN Direction)
{
	BOOLEAN result = FALSE;
	if ( Enabled == TRUE )
	{
		if ( CurMode == NORMAL )
		{
			WriteByte ( LCR, 0x03 );					// Ensure Baud Rate Divisor is NOT set.
			if ( Direction == WRITE_MODE )
				WriteByte ( LCR, LCR_6_MARK_2 );			// Set Data Transfer Mode. ( Write )
			else
				WriteByte ( LCR, LCR_6_SPACE_2 );		// Set Data Transfer Mode. ( Read )
			CurMode = DATA_TRANSFER;
			result = TRUE;
		}
	}
	else
	{
		if ( CurMode == DATA_TRANSFER )
		{
			WriteByte (LCR, 0x03);
			CurMode = NORMAL;
			result = TRUE;
		}
	}
   if ( result == FALSE )
      DbgPrint ( "%s - Error Enabling Transfer Mode\n", DRIVERNAME );
	return result;
}

BOOLEAN CMphcCard::EnableCommandMode(BOOLEAN Enabled)
{
	BOOLEAN result = FALSE;
	if ( Enabled == TRUE )
	{
		if ( CurMode == NORMAL )
		{
			WriteByte ( LCR, 0x03 );				// Ensure we are in a know state.
			WriteByte ( LCR, LCR_8_SPACE_2 );			// Set Command Mode.
			CurMode = COMMAND;
         result = TRUE;
		}
	}
	else
	{
		if ( CurMode == COMMAND )
		{
			WriteByte ( LCR, 0x03 );
			CurMode = NORMAL;
			result = TRUE;
		}
	}
   if ( result == FALSE )
      DbgPrint ( "%s - Error Enabling Command Mode CardID 0x%X\n", DRIVERNAME, m_Address );
	return result;
}

NTSTATUS CMphcCard::ProgramFPGA(PVOID pCode, ULONG lBytes)
{
	NTSTATUS status = STATUS_SUCCESS;
	int cnt= 0;
	DbgPrint ( "%s - Programming FPGA at 0x%X with %ld Bytes\n", DRIVERNAME, m_Address, lBytes);
	LARGE_INTEGER Start;
	LARGE_INTEGER End;
	LARGE_INTEGER Freq;
	ULONG Elasped;

	if ( EnableProgrammingMode ( TRUE ) ) {
		// Wait for UART to get ready.  This should be < 1 micro-second
		// We will allow upto 10 micro-seconds before failing IRP with
		// a Timeout.
		while ( !( ReadByte ( MSR ) & 0x10 ) ) 
		{
			KeStallExecutionProcessor(1);
			cnt++;
			if ( cnt < 10 )
				break;
		}

		if ( cnt < 10 ) 
		{
			Start = KeQueryPerformanceCounter(NULL);
			WriteBuffer (0x00, (PUCHAR)pCode, lBytes);
			End = KeQueryPerformanceCounter(&Freq);
			Elasped = (ULONG) (End.QuadPart - Start.QuadPart);
			Elasped = Elasped / (ULONG)( Freq.QuadPart / 1000);

			DbgPrint ( "%s - FPGA Program Transfered: %lu(ms).\n", DRIVERNAME, Elasped );
			
			cnt = 0;
			DbgPrint ( "%s - Waiting For \"Done\" pin assert\n", DRIVERNAME );
			
			WriteByte ( LCR, 0x00 );	
			while ( !(ReadByte( MSR ) & 0x20) ) 
			{	// See if it is done
				KeStallExecutionProcessor(1); cnt++;
				if ( cnt > 100 ) 
				{
					status = STATUS_IO_TIMEOUT;
					DbgPrint ( "%s - Timeout Waiting For Done Pin\n", DRIVERNAME );
//					return status;
					break;
				}
			}

			if ( status == STATUS_SUCCESS )
			{
				DbgPrint ( "%s - FPGA at 0x%X Programmed Successfully.\n", DRIVERNAME, m_Address );
		      bFpgaLoaded = TRUE;
			}
		}
		else 
		{
			status = STATUS_IO_TIMEOUT;
			DbgPrint ("%s - Timeout Waiting For UART at 0x%X to Get Ready\n", DRIVERNAME, m_Address );
		}
		EnableProgrammingMode ( FALSE );

		// The main reason for this is to set the print enable bit to 0
		EnableCommandMode ( TRUE );
		WriteByte ( 0x00, 0x00 );	
		WriteByte ( 0x00, 0x01 );	
		WriteByte ( 0x00, 0x00 );	
		EnableCommandMode ( FALSE );

		if ( status == STATUS_SUCCESS )
			status = Activate();
	}
	else
		status = STATUS_DEVICE_BUSY;
	return status;
}

NTSTATUS CMphcCard::SetSerialParams( tagSerialParams *pParams )
{
	tagSerialParams params;
	RtlCopyMemory ( (PVOID)&params, pParams, sizeof (tagSerialParams) );

	if (( params.BaudRate < 300 ) || ( params.BaudRate > 115200) ) 
			return STATUS_INVALID_PARAMETER;
	if (( params.DataBits < 7 ) || ( params.DataBits > 8) )
			return STATUS_INVALID_PARAMETER;
	if (( params.Parity < NO_PARITY)|| (params.Parity > ODD_PARITY))
			return STATUS_INVALID_PARAMETER;
	if (( params.StopBits < 1 ) || (params.StopBits > 2))
			return STATUS_INVALID_PARAMETER;

	UCHAR value = params.DataBits - 5;
	unsigned short Baud = (unsigned short) ( 115200 / params.BaudRate );
	value |= (params.StopBits == 1) ? 0 : 4;
	switch ( params.Parity ) {
		case NO_PARITY:
			break;
		case EVEN_PARITY:
			value |= 0x18;
			break;
		case ODD_PARITY:
			value |= 0x80;
			break;
		default:
			return STATUS_INVALID_PARAMETER;
	}
	WriteByte ( LCR, DLAB);
	WriteByte ( DLL, (UCHAR) (Baud & 0x00FF) );
	WriteByte ( DLH, (UCHAR) ( (Baud >> 8) & 0x00FF) );
	WriteByte ( LCR, value );
	return STATUS_SUCCESS;
}

UCHAR CMphcCard::ReadOTBStatusReg()
{
/*
	UCHAR uData = 0x00;
	EnableCommandMode ( TRUE );
	unsigned int nBytes = 1;
	WriteByte ( CMD_REG, 0x83 );	// Read mode; Start Address = 0x00
	WriteByte ( CMD_REG, 1 );		// Bytes To Transfer
	uData = ReadByte ( CMD_REG );
	EnableCommandMode ( FALSE );
*/
	UCHAR uData = 0x00;
	uData = ReadByte ( OTB_STATUS );
	return uData;
}

void CMphcCard::RxImage ( unsigned short wStartCol, unsigned short wNumCols, unsigned char nBPC, unsigned char * pImage )
{
//	EnableCommandMode ( true );
//	WriteByte ( 0x00, 0x08 );
//	WriteByte ( 0x00, 0x02 );
//	WriteByte ( 0x00, wStartCol & 0x00FF );
//	WriteByte ( 0x00, wStartCol & 0xFF00 );
//	EnableCommandMode ( false );
	unsigned short Location = wStartCol * ( nBPC / 2 );
	EnableCommandMode ( true );
	WriteByte ( 0x00, 0x08 );
	WriteByte ( 0x00, 0x02 );
	unsigned char byte;
	byte = Location & 0x00FF;
	WriteByte ( 0x00, byte );
	byte = ((Location>>8) & 0xFF); 
	WriteByte ( 0x00, byte );
	EnableCommandMode ( false );
	
	EnableDataTransferMode ( true, READ_MODE );
	ReadBuffer ( 0x00, pImage, nBPC * wNumCols );
	EnableDataTransferMode ( false, READ_MODE );
	
	DbgPrint ( "%s - RxImage() StartCol %i EndCol %i at %i BPC\n", DRIVERNAME, wStartCol, wNumCols, nBPC);
}

void CMphcCard::TxImage ( unsigned short wStartCol, unsigned short wNumCols, unsigned char nBPC, unsigned char * pImage )
{
	unsigned short Location = wStartCol * ( nBPC / 2 );

	EnableCommandMode ( true );
		WriteByte ( 0x00, 0x08 );
		WriteByte ( 0x00, 0x02 );
		unsigned char byte;
		byte = Location & 0x00FF;
		WriteByte ( 0x00, byte );
		byte = ((Location>>8) & 0xFF); 
		WriteByte ( 0x00, byte );
	EnableCommandMode ( false );
	
	KdPrint (( DRIVERNAME " - TxImage Start\n"));
	EnableDataTransferMode ( true, WRITE_MODE );
		WriteBuffer ( 0x00, pImage, nBPC * wNumCols );
	EnableDataTransferMode ( false, WRITE_MODE );
	
	KdPrint ((DRIVERNAME " - TxImage() Complete - StartCol %i NumCol %i at %i BPC to 0x%x\n", wStartCol, wNumCols, nBPC, m_Address ));
}

NTSTATUS CMphcCard::RegisterPhotoEvent( HANDLE hEvent )
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - Register Photo Event() for 0x%X", DRIVERNAME, m_Address);

	if (hEvent) {
		if ( PhotoEvent )
			ObDereferenceObject ( PhotoEvent );

		PKEVENT pEvent;
		if ( (status = ObReferenceObjectByHandle(hEvent, EVENT_MODIFY_STATE, *ExEventObjectType, KernelMode, (PVOID*)&pEvent, NULL)) == STATUS_SUCCESS)
			PhotoEvent = pEvent;
		else
			DbgPrint ("%s - Register Photo Event() for 0x%X FAILED\n", DRIVERNAME, m_Address);
	}
	else
		DbgPrint ("%s - Register Photo Event() for 0x%X Passed NULL Handle\n", DRIVERNAME, m_Address);
	return status;
}

// cgh 04d10
NTSTATUS CMphcCard::RegisterEOPEvent( HANDLE hEvent )
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - Register EOP Event() for 0x%X", DRIVERNAME, m_Address);

	if (hEvent) {
		if ( EOPEvent )
			ObDereferenceObject ( EOPEvent );

		PKEVENT pEvent;
		if ( (status = ObReferenceObjectByHandle(hEvent, EVENT_MODIFY_STATE, *ExEventObjectType, KernelMode, (PVOID*)&pEvent, NULL)) == STATUS_SUCCESS)
			EOPEvent = pEvent;
		else
			DbgPrint ("%s - Register EOP Event() for 0x%X FAILED\n", DRIVERNAME, m_Address);
	}
	else
		DbgPrint ("%s - Register EOP Event() for 0x%X Passed NULL Handle\n", DRIVERNAME, m_Address);
	return status;
}
// cgh 05e25
NTSTATUS CMphcCard::RegisterAmsEvent( HANDLE hEvent )
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - Register AMS Event() for 0x%X", DRIVERNAME, m_Address);

	if (hEvent) {
		if ( m_AmsEvent )
			ObDereferenceObject ( m_AmsEvent );

		PKEVENT pEvent;
		if ( (status = ObReferenceObjectByHandle(hEvent, EVENT_MODIFY_STATE, *ExEventObjectType, KernelMode, (PVOID*)&pEvent, NULL)) == STATUS_SUCCESS)
			m_AmsEvent = pEvent;
		else
			DbgPrint ("%s - Register AMS Event() for 0x%X FAILED\n", DRIVERNAME, m_Address);
	}
	else
		DbgPrint ("%s - Register AMS Event() for 0x%X Passed NULL Handle\n", DRIVERNAME, m_Address);
	return status;
}

NTSTATUS CMphcCard::RegisterSerialEvent( HANDLE hEvent )
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - RegisterSerialEvent() for 0x%X", DRIVERNAME, m_Address);

	if (hEvent) {
		if ( SerialEvent )
			ObDereferenceObject ( SerialEvent );

		PKEVENT pEvent;
		if ( (status = ObReferenceObjectByHandle(hEvent, EVENT_MODIFY_STATE, *ExEventObjectType, KernelMode, (PVOID*)&pEvent, NULL)) == STATUS_SUCCESS)
			SerialEvent = pEvent;
		else
			DbgPrint ("%s - RegisterSerialEvent() for 0x%X FAILED\n", DRIVERNAME, m_Address);
	}
	else
		DbgPrint ("%s - RegisterSerialEvent() for 0x%X Passed NULL Handle\n", DRIVERNAME, m_Address);
	return status;
}

NTSTATUS CMphcCard::UnregisterPhotoEvent( )
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - Unregister Photo Event for 0x%X\n", DRIVERNAME, m_Address);

//	WriteByte ( IER, 0x00 );	// Disable Interrupts
	ReadByte ( IIR );				// Clear Interrupt Identification Register
	ReadByte ( MSR);				// Clear Pending Interrupts;
   if ( PhotoEvent )
		ObDereferenceObject ( PhotoEvent );
   PhotoEvent = NULL;
   return status;
}

// cgh 04d10
NTSTATUS CMphcCard::UnregisterEOPEvent( )
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - Unregister EOP Event for 0x%X\n", DRIVERNAME, m_Address);

   if ( EOPEvent )
		ObDereferenceObject ( EOPEvent );
   EOPEvent = NULL;
   return status;
}

NTSTATUS CMphcCard::UnregisterAmsEvent( )
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - Unregister AMS Event for 0x%X\n", DRIVERNAME, m_Address);

   if ( m_AmsEvent )
		ObDereferenceObject ( m_AmsEvent );
   m_AmsEvent = NULL;
   return status;
}

NTSTATUS CMphcCard::UnregisterSerialEvent( )
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - Unregister Serial Event for 0x%X\n", DRIVERNAME, m_Address);

	WriteByte ( IER, 0x00 );	// Disable Interrupts
	ReadByte ( IIR );				// Clear Interrupt Identification Register
	ReadByte ( MSR);				// Clear Pending Interrupts;
   if ( SerialEvent )
		ObDereferenceObject ( SerialEvent );
   SerialEvent = NULL;
	WriteByte ( IER, 0x0F );	// Enable Interrupts
   return status;
}

void CMphcCard::FirePhotoEvent( void )
{
	if (PhotoEventDriver != NULL) {
		KdPrint ((DRIVERNAME " - PhotoEventDriver - 0x%x\n", m_Address));
		KeSetEvent (PhotoEventDriver, HIGH_PRIORITY, FALSE);
	}
	else if (PhotoEvent != NULL) {
		KdPrint ((DRIVERNAME " - Photocell Event - 0x%x\n", m_Address));
		KeSetEvent ( PhotoEvent, HIGH_PRIORITY, FALSE);
	}

	bPhotocellActivated = FALSE;
}

void CMphcCard::FireAmsEvent( void )
{
	if ( m_AmsEvent != NULL) {
	   KdPrint ((DRIVERNAME " - AMS Complete on - 0x%x\n", m_Address));
		KeSetEvent ( m_AmsEvent, HIGH_PRIORITY, FALSE);
   }
	m_bFireAmsEvent = FALSE;
}

// cgh 04d10
void CMphcCard::FirePrintEvent( void )
{
//	if ( m_StartOfPrint ) {
//		KdPrint ((DRIVERNAME " - Photo Delay Done - Starting Print - 0x%X\n", m_Address));
//		m_StartOfPrint = FALSE;
//	}

	if ( m_EndOfPrint ) {
		KdPrint ((DRIVERNAME " - Print Cycle Complete - 0x%X\n", m_Address ));
		m_EndOfPrint = FALSE;

		if (EOPEventDriver != NULL) {
			KdPrint ((DRIVERNAME " - Fire driver EOP event - 0x%X\n", m_Address ));
			KeSetEvent (EOPEventDriver, HIGH_PRIORITY, FALSE);
		}
		else if (EOPEvent != NULL) {
			KdPrint ((DRIVERNAME " - Fire EOP event - 0x%X\n", m_Address ));
			KeSetEvent (EOPEvent, HIGH_PRIORITY, FALSE);
		}
	}
}

void CMphcCard::SetInterruptMode(BOOLEAN Master )
{
	bMaster = Master;
	if ( bMaster )
	{
		Set_ICR ( m_PhysicalIrq | 0x10 );	// Set bit 4 to 1 for ISA interrupts
		if ( IsPresent() )
			DbgPrint ( "%s - 0x%X Set to MASTER on IRQ 0x%x\n", DRIVERNAME, m_Address, m_PhysicalIrq);
	}
	else
	{
		Set_ICR ( 0x0F & m_OTBIrqId );	// Set bit 4 to 0 for OTB Interrupt.
		if ( IsPresent() )
			DbgPrint ( "%s - 0x%X Set to SLAVE on IRQ 0x%x\n", DRIVERNAME, m_Address, m_OTBIrqId);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////
//
// Set Interrupt Control Register
//
//////////////////////////////////////////////////////////////////////////////////////////

void CMphcCard::Set_ICR(UCHAR data)
{
	WriteByte ( IER, 0x00 );   // Disable Interrupts
	EnableCommandMode ( true );
	WriteByte ( CMD_REG, 0x01 );	// Write mode; Start Address = 0x01 ICR
	WriteByte ( CMD_REG, 0x01 );	// Bytes To Transfer
	WriteByte ( CMD_REG, data);		// Write data
	EnableCommandMode ( false );
	WriteByte ( IER, 0x0F );   // Enable Interrupts
}

UCHAR CMphcCard::Get_ICR()
{
	UCHAR Byte;
	EnableCommandMode ( true );
	WriteByte ( CMD_REG, 0x01 );			// Write mode; Start Address = 0x01 ICR
	WriteByte ( CMD_REG, (UCHAR) 1 );	// Bytes To Transfer
	Byte = ReadByte ( CMD_REG);			// Read data
	EnableCommandMode ( false );
	return Byte;
}

void CMphcCard::SetControlRegister(UCHAR data)
{
	EnableCommandMode ( true );
	WriteByte ( CMD_REG, 0x00 );			// Write mode; Start Address = 0x00 CR
	WriteByte ( CMD_REG, (UCHAR) 1 );	// Bytes To Transfer
	WriteByte ( CMD_REG, data);			// Write data
	EnableCommandMode ( false );
}

UCHAR CMphcCard::GetControlRegister()
{
	UCHAR Byte;
	EnableCommandMode ( true );
	WriteByte ( CMD_REG, 0x00 );			// Write mode; Start Address = 0x0 CR
	WriteByte ( CMD_REG, (UCHAR) 1 );	// Bytes To Transfer
	Byte = ReadByte ( CMD_REG);			// Read data
	EnableCommandMode ( false );
	return Byte;
}

unsigned long CMphcCard::GetIOAddress()
{
	return (unsigned long) m_Address;
}

NTSTATUS CMphcCard::Activate()
{
	UCHAR Byte = 0;
	DbgPrint ( "%s - Activating Card:0x%x\n", DRIVERNAME, m_Address );

	WriteByte ( LCR, 03 );     // Reset LCR;
	WriteByte ( IER, 0x00 );   // Disable Interrupts

	pSerialTxTail = pSerialTxHead = pSerialTxBuffer;
	if ( (pSerialRxBuffer = (PUCHAR) ExAllocatePool(NonPagedPool, SERIAL_BUFFER_SIZE)) == NULL )
	{
		DbgPrint ( "%s - SerialRxBuffer Allocation for 0x%X FALIED\n", DRIVERNAME, m_Address );
		return  STATUS_INSUFFICIENT_RESOURCES;
	}
	else
		pSerialRxTail = pSerialRxHead = pSerialRxBuffer;

	//Defaut Serial Params;
	tagSerialParams Params;
	Params.BaudRate = 115200;
	Params.DataBits = 8;
	Params.Parity = 0;
	Params.StopBits = 1;
	SetSerialParams ( &Params );

	flagISR = 0;
	flagOTBInt = 0;
	flagPhotoInt = 0;
	flagSerialInt = 0;
	flagTachInt = 0;
	flagReserved1 = 0;
	flagReserved2 = 0;

	bSerialDataReady = FALSE;
	m_AmsActive = FALSE;
	m_StartOfPrint = false;
	m_EndOfPrint = false;

	ClearOnBoardRam();
	SetInterruptMode ( bMaster );
	WriteByte ( IER, 0x0F );   // Enable Interrupts
	return STATUS_SUCCESS;
}

BOOLEAN CMphcCard::IsPresent()
{
	return m_bPresent;
}

void CMphcCard::AssignAddress(PUCHAR Addr)
{
	m_Address = Addr;
	if ( ReadByte ( 0x00 ) == 0xFF )
		m_bPresent = FALSE;
	else {
		m_bPresent = TRUE;
		DbgPrint ( "%s - Card at 0x%X Found\n", DRIVERNAME, m_Address );
	}
}

void CMphcCard::Destroy()
{
	// We call this manually just before deallocating the object;
	WriteByte ( IER, 0x00 );   // Disable Interrupts

	// These should only be valid for Master device.

	if ( pSerialTxBuffer != NULL )
	{
		ExFreePool ( pSerialTxBuffer );
		pSerialTxBuffer = NULL;
	}

	if ( pSerialRxBuffer != NULL )
	{
		ExFreePool ( pSerialRxBuffer );
		pSerialRxBuffer = NULL;
	}
}

NTSTATUS CMphcCard::ReadCtrlReg( PHCB *pPHCB, ULONG &info)
{
	NTSTATUS status = STATUS_SUCCESS;
	if ( EnableCommandMode ( TRUE )) {
		WriteByte ( CMD_REG, (0x80 | pPHCB->RegID ) );		// Read mode; 
		WriteByte ( CMD_REG, pPHCB->TxBytes );					// Bytes To Transfer
		ReadBuffer ( CMD_REG, pPHCB->Data, pPHCB->TxBytes);	// Read Data
		EnableCommandMode ( FALSE );
//		if ( pPHCB->RegID == 0x01 )
//			DbgPrint ( "%s - ReadCtrlReg() CardID 0x%X Data %X\n", DRIVERNAME, m_Address, pPHCB->Data[0] );
	}
	else {
		info = 0;
		status = STATUS_DEVICE_BUSY;
      DbgPrint ( "%s - ReadCtrlReg() Device Busy! CardID 0x%X\n", DRIVERNAME, m_Address );
	}
	return status;
}

NTSTATUS CMphcCard::WriteWatchdog (PHCB *pPHCB, ULONG &info)
{
	UCHAR n = pPHCB->Data [0];

	info = 0;
	//DbgPrint ("%s - WriteWatchdog [0x%X]: 0x%p", DRIVERNAME, m_Address, n);

	if (bMappedPort) 	
		WRITE_REGISTER_UCHAR (WATCHDOG_REGISTER, n);
	else 
		WRITE_PORT_UCHAR (WATCHDOG_REGISTER, n);

	return STATUS_SUCCESS;
}

NTSTATUS CMphcCard::ReadWatchdog (PHCB *pPHCB, ULONG &info)
{
	UCHAR n = bMappedPort ? READ_REGISTER_UCHAR (WATCHDOG_REGISTER) : READ_PORT_UCHAR (WATCHDOG_REGISTER);

	//DbgPrint ("%s - ReadWatchdog [0x%X]: 0x%p", DRIVERNAME, m_Address, n);
	pPHCB->Data [0] = n;
	info = 0;

	return STATUS_SUCCESS;
}

NTSTATUS CMphcCard::WriteCtrlReg( PHCB *pPHCB, ULONG &info)
{
	NTSTATUS status = STATUS_SUCCESS;
	if ( EnableCommandMode ( TRUE ) ) 
	{
		WriteByte ( CMD_REG, 0x7F & pPHCB->RegID);				// Write mode; 
		WriteByte ( CMD_REG, pPHCB->TxBytes );						// Bytes To Transfer
		WriteBuffer ( CMD_REG, pPHCB->Data, pPHCB->TxBytes );	// Write data
		EnableCommandMode ( FALSE );
		if ( pPHCB->RegID == 0x00 ) {
			if ( pPHCB->Data[0] & 0x20 ) {
				m_AmsActive = true;
				KdPrint ((DRIVERNAME " - Activating AMS on 0x%X\n", pPHCB->lAddress ));
			}

/*
			{ // TODO: rem
				int bPrintEnable = pPHCB->Data [0] & (1 << 7) ? 1 : 0;
				USHORT nFreq = * (USHORT *)&pPHCB->Data [10];
				USHORT nSpeed300 = (nFreq * 1843200 * 5) / (300 * 65536);
				USHORT nSpeed426 = (nFreq * 1843200 * 5) / (426 * 65536);

				DbgPrint (DRIVERNAME " - 0x%X: _IntTachGenReg %lu [%lu], _PrintEnable: %lu\n", 
					pPHCB->lAddress, 
					nSpeed300, nFreq,
					bPrintEnable);
			}
*/
		}
	}
	else 
	{
		info = 0;
		status = STATUS_DEVICE_BUSY;
	}
	return status;
}

/*
USHORT CMphcCard::ReadInterfaceStatusRegister()
{
	USHORT uData = 0x0000;
	USHORT uMask = 0x0010;
	unsigned int nBytes = 2;

	EnableCommandMode ( TRUE );
	WriteByte ( CMD_REG, 0x84 );		// Read mode; Start Address = 0x00
	WriteByte ( CMD_REG, nBytes );	// Bytes To Transfer
	ReadBuffer(CMD_REG, uData, nBytes);
	EnableCommandMode ( FALSE );

	return uData;
}
*/

NTSTATUS CMphcCard::CueSerialData(PUCHAR pData, ULONG uLen)
{
   NTSTATUS status = STATUS_SUCCESS;
   return status;
}

BOOLEAN CMphcCard::OnInterrupt()
{
	BOOLEAN IsInterrupting = TRUE;
	UCHAR uIIR;
	UCHAR uData;
	UCHAR uBitMask;

	flagISR++;
	uIIR = ReadByte ( IIR );
	do 
	{
		switch ( uIIR & 0x07 ) {
			case 0:		// MSR
				uData = ReadByte (MSR);
				uBitMask = 0x01;
//				while ( uBitMask & 0x0F ) 
				while ( uBitMask ) 
				{
					switch ( uData & uBitMask ) 
					{
						case PC_INT:
							flagPhotoInt++;
							bPhotocellActivated = TRUE;	// Photocell event has occured on the slave device.
							break;
						case AMS_STATE_CHANGE:
							m_AmsActive = false;
							m_bFireAmsEvent = true;
							break;
						case PRINT_CYCLE_STATE:	//										// cgh 04d07
							switch ( uData & PRINT_CYCLE_ACTIVE ) {	//							// cgh 04d07
								case 0:	// Complete print-cycle is finished.	// cgh 04d07
									m_EndOfPrint = TRUE;							// cgh 04d10
									break;
								case 1:	// Photo delay is finished and Printing has begun.		// cgh 04d07
									m_StartOfPrint = TRUE;
									break;
							}
						case OTB_INT:
							break;
					}
					uBitMask <<= 1;
				}
				break;
			case 2:  // THR Empty
				break;
			case 4:  // Data Aval.
				flagSerialInt++;
				ProcessSerialData ();
				break;
			case 6:  // LSR Error
				flagSerialInt++;
				ReadByte ( LSR );
				break;
			default:
				flagReserved1++;
		}
		uIIR = ReadByte ( IIR );
	} while (  !( uIIR & 0x01 ) );
	return IsInterrupting;
}

void CMphcCard::ClearOnBoardRam()
{
// Each board has 256k of RAM divided into 2 128K Buffers
	unsigned char Data [ 1024 ];
	unsigned char RxData [ 1024 ];
	unsigned char RegValue;

	RtlZeroMemory ( Data, sizeof (Data) );
//	RtlFillMemory ( Data, sizeof (Data), 0xFF);
	DbgPrint ( "%s - Clearing On Board RAM for 0x%x\n", DRIVERNAME, m_Address);

	pSerialRxTail = pSerialRxHead = pSerialRxBuffer;

	// Select buffer 1
	EnableCommandMode ( true );
	WriteByte ( 0x00, 0x80 );
	WriteByte ( 0x00, 0x01 );
	RegValue = ReadByte ( 0x00 );
	WriteByte ( 0x00, 0x00 );
	WriteByte ( 0x00, 0x01 );
	WriteByte ( 0x00,  (RegValue | 0x07) );
	EnableCommandMode ( false );

	EnableDataTransferMode ( true, WRITE_MODE );
	for ( int i = 0; i < 256; i++ )
		WriteBuffer ( 0x00, Data, sizeof (Data) );
	EnableDataTransferMode ( false, WRITE_MODE );

	EnableCommandMode ( true );
	WriteByte ( 0x00, 0x00 );
	WriteByte ( 0x00, 0x01 );
	WriteByte ( 0x00,  RegValue );
	EnableCommandMode ( false );

//	EnableDataTransferMode ( true, READ_MODE );
/*	
	DbgPrint ( "%s - Verify Memory Bank 0 for 0x%x\n", DRIVERNAME, m_Address);
	for ( i = 0; i < 128; i++ )
	{
		RtlZeroMemory ( RxData, sizeof (Data) );
		ReadBuffer ( 0x00, RxData, sizeof (Data) );
		if ( RtlCompareMemory ( Data, RxData, sizeof(Data) ) != sizeof(Data) )
			DbgPrint ( "%s - Memory Block %i Verification Failed for 0x%x\n", DRIVERNAME, i, m_Address);
	}
	EnableDataTransferMode ( false, READ_MODE );
*/	
/*
	// Select buffer 0
	EnableCommandMode ( true );
	WriteByte ( 0x00, 0x00 );
	WriteByte ( 0x00, 0x01 );
	WriteByte ( 0x00, 0x04 );
	EnableCommandMode ( false );
	
	EnableDataTransferMode ( true, WRITE_MODE );
	for ( i = 0; i < 128; i++ )
		WriteBuffer ( 0x00, Data, sizeof(Data) );
	EnableDataTransferMode ( false, WRITE_MODE );
*/
/*	
	DbgPrint ( "%s - Verify Memory Bank 1 for 0x%x\n", DRIVERNAME, m_Address);
	EnableDataTransferMode ( true, READ_MODE );
	for ( i = 0; i < 128; i++ )
	{
		RtlZeroMemory ( RxData, sizeof(Data) );
		ReadBuffer ( 0x00, RxData, sizeof(Data) );
		if ( RtlCompareMemory ( Data, RxData, sizeof(Data) ) != sizeof(Data) )
			DbgPrint ( "%s - Memory Bank 1 Block %i Verification Failed for 0x%x\n", DRIVERNAME, i, m_Address);
	}
	EnableDataTransferMode ( false, READ_MODE );
*/
}

void CMphcCard::ProcessSerialData()
{
	*pSerialRxTail = ReadByte ( 0x00 );
	if ( *pSerialRxTail == '\r' )
		bSerialDataReady = TRUE;

	if ( (pSerialRxTail - pSerialRxBuffer ) < SERIAL_BUFFER_SIZE )
		pSerialRxTail++;
}

int CMphcCard::GetSerialData(PUCHAR pData)
{
	int count = pSerialRxTail - pSerialRxBuffer;
	RtlZeroMemory ( pData, count + 1 );
	RtlCopyMemory ( pData, pSerialRxBuffer, count);
	pSerialRxTail = pSerialRxBuffer;
	DbgPrint ("%s - GetSerialData() Count 0x%X Data %s\n", DRIVERNAME, count, pData);
	return count;
}

void CMphcCard::FireSerialEvent()
{
	if ( SerialEvent != NULL)
   {
	   DbgPrint ( "%s - SerialEvent - 0x%x\n", DRIVERNAME, m_Address);
		KeSetEvent ( SerialEvent, LOW_REALTIME_PRIORITY, FALSE);
   }
	bSerialDataReady = FALSE;

}

void CMphcCard::ClearDebugFlags()
{
	flagISR = 0;
	flagOTBInt = 0;
	flagPhotoInt = 0;
	flagSerialInt = 0;
	flagTachInt = 0;
	flagReserved1 = 0;
	flagReserved2 = 0;
}


NTSTATUS CMphcCard::RegisterDriverPhotoEvent( HANDLE hEvent )
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - Register Driver Photo Event() for 0x%X", DRIVERNAME, m_Address);

	if (hEvent) {
		if ( PhotoEventDriver )
			ObDereferenceObject ( PhotoEventDriver );

		PKEVENT pEvent;
		if ( (status = ObReferenceObjectByHandle(hEvent, EVENT_MODIFY_STATE, *ExEventObjectType, KernelMode, (PVOID*)&pEvent, NULL)) == STATUS_SUCCESS)
			PhotoEventDriver = pEvent;
		else
			DbgPrint ("%s - Register Driver Photo Event() for 0x%X FAILED\n", DRIVERNAME, m_Address);
	}
	else
		DbgPrint ("%s - Register Driver Photo Event() for 0x%X Passed NULL Handle\n", DRIVERNAME, m_Address);

	return status;
}

NTSTATUS CMphcCard::RegisterDriverEOPEvent( HANDLE hEvent )
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - Register Driver EOP Event() for 0x%X", DRIVERNAME, m_Address);

	if (hEvent) {
		if ( EOPEventDriver )
			ObDereferenceObject ( EOPEventDriver );

		PKEVENT pEvent;
		if ( (status = ObReferenceObjectByHandle(hEvent, EVENT_MODIFY_STATE, *ExEventObjectType, KernelMode, (PVOID*)&pEvent, NULL)) == STATUS_SUCCESS)
			EOPEventDriver = pEvent;
		else
			DbgPrint ("%s - Register Driver EOP Event() for 0x%X FAILED\n", DRIVERNAME, m_Address);
	}
	else
		DbgPrint ("%s - Register Driver EOP Event() for 0x%X Passed NULL Handle\n", DRIVERNAME, m_Address);
	return status;
}

NTSTATUS CMphcCard::UnregisterDriverPhotoEvent( )
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - Unregister Driver Photo Event for 0x%X\n", DRIVERNAME, m_Address);

//	WriteByte ( IER, 0x00 );	// Disable Interrupts
	ReadByte ( IIR );				// Clear Interrupt Identification Register
	ReadByte ( MSR);				// Clear Pending Interrupts;
   if ( PhotoEventDriver )
		ObDereferenceObject ( PhotoEventDriver );
   PhotoEventDriver = NULL;
   return status;
}

NTSTATUS CMphcCard::UnregisterDriverEOPEvent( )
{
	NTSTATUS status = STATUS_SUCCESS;
	DbgPrint ("%s - Unregister Driver EOP Event for 0x%X\n", DRIVERNAME, m_Address);

   if ( EOPEventDriver )
		ObDereferenceObject ( EOPEventDriver );
   EOPEventDriver = NULL;
   return status;
}
