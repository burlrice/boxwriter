///////////////////////////////////////////////////////////////////////////////
// RemoveLock.cpp -- Portable implementation for remove-lock functions
// Copyright (C) 2002 By FoxJet Tech Center
// All rights reserved
//
//	Author: Chris G. Hodge
//	Version History:
//		1.00	Creation
///////////////////////////////////////////////////////////////////////////////

#include "stddcls.h"

///////////////////////////////////////////////////////////////////////////////

#pragma PAGEDCODE

VOID InitializeRemoveLock(PREMOVE_LOCK lock, ULONG tag, ULONG minutes, ULONG maxcount)
{
	PAGED_CODE();
	KeInitializeEvent(&lock->evRemove, NotificationEvent, FALSE);
	lock->usage = 1;
	lock->removing = FALSE;
}

#pragma LOCKEDCODE

NTSTATUS AcquireRemoveLock(PREMOVE_LOCK lock, PVOID tag)
{
	LONG usage = InterlockedIncrement(&lock->usage);
	if (lock->removing)
	{
		if (InterlockedDecrement(&lock->usage) == 0)
			KeSetEvent(&lock->evRemove, 0, FALSE);
		return STATUS_DELETE_PENDING;
	}
	return STATUS_SUCCESS;
}

VOID ReleaseRemoveLock(PREMOVE_LOCK lock, PVOID tag)
{
	if (InterlockedDecrement(&lock->usage) == 0)
		KeSetEvent(&lock->evRemove, 0, FALSE);
}

#pragma PAGEDCODE

VOID ReleaseRemoveLockAndWait(PREMOVE_LOCK lock, PVOID tag)
{
	PAGED_CODE();
	lock->removing = TRUE;
	ReleaseRemoveLock(lock, tag);
	ReleaseRemoveLock(lock, NULL);
	KeWaitForSingleObject(&lock->evRemove, Executive, KernelMode, FALSE, NULL);
}
