#if !defined (__ISA_TYPEDEFS_H__)
#define __ISA_TYPEDEFS_H__

enum DEVICE_MODE {
	NORMAL,
	PROGRAM,
	COMMAND,
	DATA_TRANSFER
};

enum INTERRUPT_MODES {
	SLAVE,
	MASTER
};

enum PARITY {
	NO_PARITY,
	EVEN_PARITY,
	ODD_PARITY
};

enum STROBE_COLOR
{
	STROBE_LIGHT_OFF	= 0x00,
	RED_STROBE			= 0x01,
	GREEN_STROBE		= 0x02,
	YELLOW_STROBE		= 0x04,
};


// The following structures must be one byte aligned.
#pragma pack (1)

typedef struct {
	unsigned long ISRInt;
	unsigned long OTBInt;
	unsigned long PhotoInt;
	unsigned long SerialInt;
	unsigned long TachInt;
	unsigned long Reserved1;
	unsigned long Reserved2;
	unsigned char uICR; // Burl
	unsigned char uIER; // Burl
}_DBG_FLAGS;

typedef struct {
	unsigned long lAddress;		// Card Address
	unsigned char RegID;			// Register ID
	unsigned char TxBytes;		// Bytes to Transfer
	unsigned char Data[256];	// Data Read or Written
}PHCB;

typedef struct
{
	UCHAR Major;		// Major Version		(0..255)
	UCHAR Minor;		// Minor Version		(0..255)
	UCHAR Revision;	// Revision Number	(0..255)
}_VERSION;
#pragma pack()

typedef struct
{
//	ULONG Addr;
	ULONG BaudRate;
	CHAR	DataBits;
	CHAR	StopBits;
	CHAR	Parity;
}tagSerialParams;
/*
typedef struct tagImgUpdateHdr
{
	ULONG _lAddress;
	ULONG _lStartCol;
	ULONG _lBytes;
	ULONG _lBytesPerCol;
}_IMG_UPDATE_HDR;
*/
typedef struct tagImgUpdateHdr
{
	unsigned short	_wStartCol;
	unsigned short	_wNumCols;
	unsigned char	_nBPC;
}_IMG_UPDATE_HDR;
#pragma pack()
#endif