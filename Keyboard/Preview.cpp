// Preview.cpp : implementation file
//

#include "stdafx.h"
#include "Keyboard.h"
#include "Preview.h"
#include "Debug.h"
#include "KeyboardDlg.h"
#include "Color.h"
#include "Parse.h"
#include "WinMsg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;

#define TXT_TEXT                        1016

/////////////////////////////////////////////////////////////////////////////
// CPreview

CPreview::CPreview(CKeyboardDlg * pParent)
:	m_ptDrag (NULL),
	m_ptScroll (NULL),
	m_pKeyboard (pParent),
	m_hwndCapture (NULL),
	m_hbmpScreen (NULL),
	m_hbmpCapture (NULL),
	CScrollView ()
{
	m_nMapMode = MM_TEXT;
}

CPreview::~CPreview()
{
	if (m_hbmpScreen) {
		::DeleteObject (m_hbmpScreen);
		m_hbmpScreen = NULL;
	}

	if (m_hbmpCapture) {
		::DeleteObject (m_hbmpCapture);
		m_hbmpCapture = NULL;
	}
}


BEGIN_MESSAGE_MAP(CPreview, CScrollView)
	//{{AFX_MSG_MAP(CPreview)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEACTIVATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_MESSAGE (WM_SENTINPUT, OnInputSent)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPreview message handlers

void CPreview::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CRect rc;

	CScrollView::OnLButtonDown(nFlags, point);

	if (m_ptDrag) 
		delete m_ptDrag;

	if (m_ptScroll)
		delete m_ptScroll;

	m_ptDrag = new CPoint (point);
	m_ptScroll = new CPoint (GetScrollPos (SB_HORZ), GetScrollPos (SB_VERT));

	GetClientRect (rc);
	ClientToScreen (rc);
	::ClipCursor (rc);
}

void CPreview::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if (m_ptDrag) {
		delete m_ptDrag;
		m_ptDrag = NULL;
	}

	if (m_ptScroll) {
		delete m_ptScroll;
		m_ptScroll = NULL;
	}

	::ClipCursor (NULL);
	REPAINT (this); 
	
	CScrollView::OnLButtonUp(nFlags, point);
}

void CPreview::OnMouseMove(UINT nFlags, CPoint point) 
{
	CScrollView::OnMouseMove(nFlags, point);

	if (m_ptDrag && m_ptScroll) {
		CPoint pt = point - (* m_ptDrag);
		CPoint ptScroll = (* m_ptScroll) - pt;
		CRect rcInvalid;// (m_rcZoom);

		GetClientRect (rcInvalid);

		//rcInvalid.UnionRect (rcInvalid, m_rcZoom - ptScroll);
		//rcInvalid.UnionRect (rcInvalid, m_rcZoom + ptScroll);
		rcInvalid.InflateRect (5, 5);

		ptScroll.x = BindTo ((int)ptScroll.x, 0, GetScrollLimit (SB_HORZ));
		ptScroll.y = BindTo ((int)ptScroll.y, 0, GetScrollLimit (SB_VERT));

		if (ptScroll.x < 0 || ptScroll.x > GetScrollLimit (SB_HORZ) || ptScroll.y < 0 || ptScroll.y > GetScrollLimit (SB_VERT)) {
			TRACEF ("out of bounds");
		}
		else {
			//{ CString str; str.Format (_T ("(%-3d, %-3d)"), ptScroll.x, ptScroll.y); TRACEF (str); }
			SetScrollPos (SB_HORZ, ptScroll.x);
			SetScrollPos (SB_VERT, ptScroll.y);
			InvalidateRect (rcInvalid);
		}
	}

	MoveEditCtrl ();
}


bool CPreview::Create (CWnd * pParent, const CRect & rc, UINT nID)
{
	if (CScrollView::Create (NULL, NULL, WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL | SS_OWNERDRAW,
		rc, pParent, nID))
	{

//		VERIFY (m_wndTxt.Create (ES_MULTILINE | WS_CHILD | WS_VISIBLE | WS_TABSTOP, CRect (0, 0, 80, 12), this, TXT_TEXT));

		return true;
	}

	return false;
}

BOOL CPreview::OnEraseBkgnd(CDC* pDC) 
{
	CRect rc;

	GetClientRect (rc);
	pDC->FillSolidRect (rc, Color::rgbDarkGray);

	return TRUE;
}

int CPreview::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message) 
{
	// NOTE: do NOT call CScrollView::OnMouseActivate
	return CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

void CPreview::OnDraw(CDC* pDC)
{
	CRect rc;
	CPoint ptScroll = CPoint (GetScrollPos (SB_HORZ), GetScrollPos (SB_VERT));

	GetClientRect (rc);
	pDC->FillSolidRect (rc, Color::rgbDarkGray);

	if (m_hbmpScreen) {
		CDC dcMem;
		CBitmap * pbmpDesktop = CBitmap::FromHandle (m_hbmpScreen);
		DIBSECTION ds;

		ZeroMemory (&ds, sizeof (ds));
		::GetObject (m_hbmpScreen, sizeof (ds), &ds);

		//TRACEF (ToString (ptScroll.x) + _T (", ") + ToString (ptScroll.y));

		int x = 0;//ptScroll.x;
		int y = 0;//ptScroll.y;
		int cx = ds.dsBm.bmWidth;
		int cy = ds.dsBm.bmHeight;

		dcMem.CreateCompatibleDC (pDC);
		CBitmap * pBitmap = dcMem.SelectObject (pbmpDesktop);

		pDC->BitBlt (x, y, cx, cy, &dcMem, 0, 0, SRCCOPY);
		pDC->DrawEdge (rc + ptScroll, EDGE_SUNKEN, BF_LEFT | BF_RIGHT | BF_TOP | BF_BOTTOM | BF_ADJUST);

		if (m_rcClick.Width () && m_rcClick.Height ()) {
			CRect rc = m_rcClick;

			if (m_hbmpCapture) {
				CBitmap * pbmpCapture = CBitmap::FromHandle (m_hbmpCapture);

				CBitmap * pOld = dcMem.SelectObject (pbmpCapture);
				::BitBlt (pDC->m_hDC, rc.left, rc.top, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
				pDC->BitBlt (rc.left, rc.top, rc.Width (), rc.Height (), &dcMem, 0, 0, SRCCOPY);
				dcMem.SelectObject (pOld);
			}

			rc.InflateRect (1, 1);
			pDC->FrameRect (rc, &CBrush (Color::rgbRed));
		}

		dcMem.SelectObject (pBitmap);
	}
}

void CPreview::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CScrollView::OnVScroll(nSBCode, nPos, pScrollBar);
	MoveEditCtrl ();
	REPAINT (this); 
}

void CPreview::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CScrollView::OnHScroll(nSBCode, nPos, pScrollBar);
	MoveEditCtrl ();
	REPAINT (this); 
}

void CPreview::OnSize(UINT nType, int cx, int cy) 
{
	CScrollView::OnSize(nType, cx, cy);
	EnableScrollBarCtrl(SB_BOTH, FALSE);
}

void CPreview::MoveEditCtrl()
{
	CPoint ptScroll = CPoint (GetScrollPos (SB_HORZ), GetScrollPos (SB_VERT));
	CRect rc;
	CRect rcEdit = m_rcClick - ptScroll;

	GetClientRect (rc);
	rcEdit.IntersectRect (rc, rcEdit);

	int x = rcEdit.left;
	int y = rcEdit.top;
	int cx = rcEdit.Width ();
	int cy = rcEdit.Height ();

	//TRACEF (ToString (x) + _T (", ") + ToString (y) + _T (" [") + ToString (cx) + _T (", ") + ToString (cy) + _T ("]"));

//	m_wndTxt.SetWindowPos (&CWnd::wndTop, x, y, cx, cy, SWP_SHOWWINDOW);

	//REPAINT (this);
}

void CPreview::SetWindow (HWND hwnd, const CPoint & ptClick)
{
	CPoint pt (0, 0), ptOffset (0, 0);

	m_hwndCapture = hwnd;

	{
		CRect rcClick (0, 0, 0, 0);
		HWND hwnd = ::GetDesktopWindow (); //GetMainWnd (m_hwndCapture);

		::GetClientRect (hwnd, &rcClick);
		ptOffset = rcClick.TopLeft ();
		::ClientToScreen (hwnd, &ptOffset);

		m_rcClick -= ptOffset;
	}

	Capture (NULL);
	::GetWindowRect (hwnd, &m_rcClick);

	{
		CRect rc (0, 0, 0, 0);
		
		::GetWindowRect (::GetDesktopWindow (), rc);
		SetScrollSizes (MM_TEXT, rc.Size ());
	}

//	pt = ptClick;
//	::ScreenToClient (hwnd, &pt);
//	ScrollToPosition (pt);
	ScrollToPosition (m_rcClick.TopLeft ());

	REPAINT (this);
}

HWND CPreview::GetMainWnd (HWND hwndChild)
{
	const DWORD dwStop [] = 
	{
		WS_OVERLAPPED,
		WS_OVERLAPPEDWINDOW,
		WS_POPUP,
		WS_POPUPWINDOW,
		WS_DLGFRAME,
	};
	int n = 0;
	HWND hwndLast = hwndChild;
	bool bMore = true;

	for (HWND hwnd = hwndChild; bMore && hwnd; n++) {
		DWORD dwStyle = ::GetWindowLong (hwnd, GWL_STYLE);

		#ifdef _DEBUG
		//{ CString str; TCHAR sz [MAX_PATH] = { 0 }; ::GetWindowText (hwnd, sz, ARRAYSIZE (sz)); str.Format (_T ("%s0x%p, %s: %s"), CString ('\t', n), hwnd, GetClassName (hwnd), sz); TRACEF (str); }
		#endif //_DEBUG

		hwndLast = hwnd;
		hwnd = ::GetParent (hwnd);

//		for (int i = 0; bMore && i < ARRAYSIZE (dwStop); i++)
//			bMore = (dwStop [i] & dwStyle) ? false : true;
	}

	#ifdef _DEBUG
	//{ CString str; TCHAR sz [MAX_PATH] = { 0 }; ::GetWindowText (hwnd, sz, ARRAYSIZE (sz)); str.Format (_T ("%s0x%p, %s: %s"), CString ('\t', n), hwnd, GetClassName (hwnd), sz); TRACEF (str); }
	#endif //_DEBUG

	return hwndLast;
}

void CPreview::Capture (HWND hwnd)
{
	if (!hwnd) {
		CxImage img;
		const bool bVisible = m_pKeyboard->IsWindowVisible () ? true : false;

		m_rcClick = CRect (0, 0, 0, 0);

		if (m_hbmpScreen) {
			::DeleteObject (m_hbmpScreen);
			m_hbmpScreen = NULL;
		}

		if (m_hbmpCapture) {
			::DeleteObject (m_hbmpCapture);
			m_hbmpCapture = NULL;
		}

		if (bVisible) {
			DWORD dwResult = 0;
			LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_SCREENSHOT, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);

			do
			{
				dwResult = 0;
				lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_ISKEYBOARDVISIBLE, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);

				if (!dwResult)
					::Sleep (100);
			} 
			while (dwResult);
		}

		{
			HDC hdc = ::GetDC (NULL);

			CDC * pdc = CDC::FromHandle (hdc);
			CRect rc (0, 0, 0, 0);
			CDC dcMem;
			CBitmap bmpMem;

			::GetWindowRect (::WindowFromDC (hdc), rc);

			dcMem.CreateCompatibleDC (pdc);
			bmpMem.CreateCompatibleBitmap (pdc, rc.Width (), rc.Height ());

			CBitmap * pBitamp = dcMem.SelectObject (&bmpMem);

			::BitBlt (dcMem, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);
			REPAINT (GetMainWnd (hwnd));
			dcMem.BitBlt (0, 0, rc.Width (), rc.Height (), pdc, 0, 0, SRCCOPY);

			m_hbmpScreen = (HBITMAP)bmpMem.Detach ();
			//SAVEBITMAP (m_hbmpScreen, _T ("C:\\Temp\\Debug\\m_hbmpScreen.bmp"));

			dcMem.SelectObject (pBitamp);
			::ReleaseDC (NULL, hdc);
		}

		if (bVisible) {
			DWORD dwResult = 0;
			LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_SCREENSHOT, 1, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);
		}
	}
	else {
		HWND hwndDesktop = ::GetDesktopWindow ();
		CRect rc = m_rcClick;

		if (m_hbmpCapture) {
			::DeleteObject (m_hbmpCapture);
			m_hbmpCapture = NULL;
		}

		if (HDC hdcDesktop = ::GetDC (hwndDesktop)) {
			if (HDC hdcMem = ::CreateCompatibleDC (hdcDesktop)) {

				hwnd = m_hwndCapture;
				::GetClientRect (hwnd, rc);

				if (HBITMAP hbmpMem = ::CreateCompatibleBitmap (GetDC ()->m_hDC, rc.Width (), rc.Height ())) {
					HBITMAP hbmpOld = (HBITMAP)::SelectObject (hdcMem, hbmpMem);
					DWORD dwResult = 0;

					::BitBlt (hdcMem, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);

					{
						CRect rc (0, 0, 0, 0);
						HWND hwnd = m_hwndCapture;

						#ifdef _DEBUG
						{ CString str; str.Format (_T ("0x%p, %s, %s"), hwnd, GetClassName (hwnd), GetWndText (hwnd)); TRACEF (str); }
						#endif

						::GetWindowRect (hwnd, &rc);

						HDC hdc = ::GetDC (hwnd);

						const bool bVisible = m_pKeyboard->IsWindowVisible () ? true : false;


						if (::GetCurrentObject (hdc, OBJ_BITMAP)) {
							CDC * pdc = CDC::FromHandle (hdc);
							CBitmap * pbmp = pdc->GetCurrentBitmap ();
							CDC dcMem;
							CBitmap bmpMem;

							dcMem.CreateCompatibleDC (GetDC ());
							bmpMem.CreateCompatibleBitmap (GetDC (), rc.Width (), rc.Height ());

							CBitmap * pOld = dcMem.SelectObject (&bmpMem);

							::BitBlt (dcMem, 0, 0, rc.Width (), rc.Height (), NULL, 0, 0, WHITENESS);

							if (bVisible) {
								DWORD dwResult = 0;
								LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_SCREENSHOT, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);

								/*
								do
								{
									dwResult = 0;
									lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_ISKEYBOARDVISIBLE, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);

									if (!dwResult)
										::Sleep (100);
								} 
								while (dwResult);
								*/
							}

							dcMem.BitBlt (0, 0, rc.Width (), rc.Height (), pdc, 0, 0, SRCCOPY);

							if (bVisible) {
								DWORD dwResult = 0;
								LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_SCREENSHOT, 1, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);
							}

							/*
							HWND hwndPrint = ::GetParent (hwnd);
							LRESULT lResult = ::SendMessage (hwndPrint, WM_PRINT, (WPARAM)dcMem.m_hDC, PRF_CLIENT | PRF_NONCLIENT | PRF_CHILDREN);
							{ CString str; TCHAR sz [MAX_PATH] = { 0 }; ::GetWindowText (hwnd, sz, ARRAYSIZE (sz)); str.Format (_T ("WM_PRINT: 0x%p, %s [%s]: %d"), hwndPrint, GetClassName (hwndPrint), sz, lResult); TRACEF (str); }
							*/

							m_hbmpCapture = (HBITMAP)bmpMem.Detach ();
							//SAVEBITMAP (m_hbmpCapture, _T ("C:\\Temp\\Debug\\m_hbmpCapture.bmp"));

							dcMem.SelectObject (pOld);
						}
					}

					/*
					if (HINSTANCE hLib = LoadLibrary (_T ("User32.dll"))) {
						bool bCaptured = false;

						typedef BOOL (WINAPI* lpfnPrintWindow)(HWND hwnd, HDC hdcBlt, UINT nFlags);

						if (lpfnPrintWindow lp = (lpfnPrintWindow)::GetProcAddress (hLib, "PrintWindow")) {
							bCaptured = (* lp) (hwnd, hdcMem, 0) ? true : false;

							#ifdef _DEBUG
							{ CString str; str.Format (_T ("PrintWindow [0x%p, %s, %s]: %s"), hwnd, GetClassName (hwnd), GetWndText (hwnd), ToString (bCaptured)); TRACEF (str); }
							#endif

							m_hbmpScreen = hbmpMem;
							//SAVEBITMAP (m_hbmpScreen, _T ("C:\\Temp\\Debug\\m_hbmpScreen.bmp"));
						}
					}
					*/

					::SelectObject (hdcMem, hbmpOld);
				}

				::DeleteObject (hdcMem);
			}

			::ReleaseDC (hwndDesktop, hdcDesktop);
		}
	}

	REPAINT (this);
}

LRESULT CPreview::OnInputSent(WPARAM wParam, LPARAM lParam)
{
	MSG msg;
	
	while (::PeekMessage (&msg, m_hWnd, WM_SENTINPUT, WM_SENTINPUT, PM_REMOVE)) {
	}

	//Capture (m_hwndCapture);

	REPAINT (this);

	return 0;
}
