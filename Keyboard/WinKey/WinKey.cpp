// WinKey.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "WinKey.h"
#include <atlbase.h>
#include <Richedit.h>
#include <Windows.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWinKeyApp

BEGIN_MESSAGE_MAP(CWinKeyApp, CWinApp)
	//{{AFX_MSG_MAP(CWinKeyApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWinKeyApp construction

CWinKeyApp::CWinKeyApp()
{
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CWinKeyApp object

CWinKeyApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CWinKeyApp initialization

BOOL CWinKeyApp::InitInstance()
{
	keybd_event (VK_LWIN, 0, 0, 0);
	keybd_event (VK_LWIN, 0, KEYEVENTF_KEYUP, 0);

	return FALSE;
}
