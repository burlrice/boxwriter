// WinKeyDlg.h : header file
//

#if !defined(AFX_WINKEYDLG_H__6E720EFF_1866_4BF1_A193_C1D6A526B16C__INCLUDED_)
#define AFX_WINKEYDLG_H__6E720EFF_1866_4BF1_A193_C1D6A526B16C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CWinKeyDlg dialog

class CWinKeyDlg : public CDialog
{
// Construction
public:
	CWinKeyDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CWinKeyDlg)
	enum { IDD = IDD_WINKEY_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWinKeyDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CWinKeyDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WINKEYDLG_H__6E720EFF_1866_4BF1_A193_C1D6A526B16C__INCLUDED_)
