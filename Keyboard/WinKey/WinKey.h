// WinKey.h : main header file for the WINKEY application
//

#if !defined(AFX_WINKEY_H__252AC3CD_2A04_434A_B520_B8A63F6D268F__INCLUDED_)
#define AFX_WINKEY_H__252AC3CD_2A04_434A_B520_B8A63F6D268F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CWinKeyApp:
// See WinKey.cpp for the implementation of this class
//

class CWinKeyApp : public CWinApp
{
public:
	CWinKeyApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWinKeyApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CWinKeyApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WINKEY_H__252AC3CD_2A04_434A_B520_B8A63F6D268F__INCLUDED_)
