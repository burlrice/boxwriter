#if !defined(AFX_KEYBOARDDLG_H__E272FBD3_BF84_46FA_AC39_080A5E3F6ACB__INCLUDED_)
#define AFX_KEYBOARDDLG_H__E272FBD3_BF84_46FA_AC39_080A5E3F6ACB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// KeyboardDlg.h : header file
//

#include <afxtempl.h>
#include "Key.h"
#include "ximage.h"
#include "RoundButton2.h"
#include "Preview.h"
#include <string>
#include <map>

/////////////////////////////////////////////////////////////////////////////
// CKeyboardDlg dialog

inline void Repaint (HWND hwnd)
{
	::InvalidateRect (hwnd, NULL, FALSE); 
	::RedrawWindow (hwnd, NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW); 
}

/*
inline void Repaint (CWnd * p)
{
	if (p)
		Repaint (p->m_hWnd);
}
*/

//#define REPAINT(wnd) Repaint (wnd); //{ (wnd)->Invalidate (FALSE); (wnd)->RedrawWindow (NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW); /* (wnd)->RedrawWindow (); */ /* (wnd)->UpdateWindow (); */ }

class CKeyboardDlg;

class CTarget
{
public:
	CTarget (CKeyboardDlg * pParent) : m_pParent (pParent), m_hTarget (NULL) { }

	void SetTarget (HWND hWnd);
	HWND GetTarget () const { return m_hTarget; }

private:
	CKeyboardDlg * m_pParent;
	HWND m_hTarget;
};

typedef struct tagKEYSTRUCT
{
	tagKEYSTRUCT (UINT nID = 0, UCHAR c = 0, UCHAR cShift = 0, BYTE nVK = 0) 
	:	m_nID (nID),
		m_c (c),
		m_cShift (cShift),
		m_cShiftAltL (0),
		m_cShiftAltU (0),
		m_nVK (nVK)
	{
	}

	UINT m_nID;
		
	TCHAR m_c;
	TCHAR m_cShift;
	TCHAR m_cShiftAltL;
	TCHAR m_cShiftAltU;
	BYTE m_nVK;
	CString m_str;
	CString m_strShift;
	CString m_strShiftAltL;
	CString m_strShiftAltU;
} KEYSTRUCT;

class CKeyboardDlg : public CDialog
{
	friend class CKey;
	friend class CKeyboardApp;

// Construction
public:
	afx_msg void OnExpand();
	afx_msg void OnEsc ();
	CKeyboardDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CKeyboardDlg ();

	static const CString m_strWndClassesExclude;
	static const CString m_strWndClasses;

// Dialog Data
	//{{AFX_DATA(CKeyboardDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	static CFont * CreateArrowFont (int nSize);
	static CFont * CreateFontFromRegistry (LPCTSTR lpszName);
	static void SaveFont (CFont * p, LPCTSTR lpszName);

	static void WriteDefConfig ();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKeyboardDlg)
	public:
virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	void CheckIDs();
	void CheckUserKeys ();
	void ReadConfig (const CString & strFile);
	void LoadInputClasses ();

	virtual void OnCancel();

	KEYSTRUCT m_map [67];
	NOTIFYICONDATA m_nid;
	HICON m_hIcon;
	HANDLE m_hCharMapThread;

	void Uncheck ();
	bool IsCheckbox (UINT nID);
	int GetRowCount (const UINT * p);

	bool IsWinKey ();
	bool IsAlt ();
	bool IsCtrl ();
	bool IsShift ();
	bool IsShift2 ();
	bool IsCaps ();
	void OnShift ();
	void OnShift2 ();
	void Dock ();
	void Undock ();
	void Fullscreen ();
	CRect GetDesktopRect ();
	void SaveState ();
	void RestoreState ();
	void StartKbMonitor ();
	void CreateShortcut ();

	HWND StartKbWin ();
	void CheckInputClass (HWND hwnd);
	void OnChangeColors ();
//	void CaptureDesktop ();
	HWND FindKbWin();

	FoxjetUtils::CRoundButton2 m_btnCaps;
	FoxjetUtils::CRoundButton2 m_btnShift1;
	FoxjetUtils::CRoundButton2 m_btnShift2;
	FoxjetUtils::CRoundButton2 m_btnCtrl1;
	FoxjetUtils::CRoundButton2 m_btnCtrl2;
	FoxjetUtils::CRoundButton2 m_btnAlt1;
	FoxjetUtils::CRoundButton2 m_btnAlt2;
	FoxjetUtils::CRoundButton2 m_btnKeypadNumlock;
	FoxjetUtils::CRoundButton2 m_btnKeypadScrollLock;
	FoxjetUtils::CRoundButton2 m_btnMouse;
	FoxjetUtils::CRoundButton2 m_btnExpand;
	FoxjetUtils::CRoundButton2 m_btnEsc;
	CKey m_btnCharMap;

	FoxjetUtils::CRoundButtonStyle m_styleDefault;

	CTarget m_target;
	HACCEL m_hAccel;
	CFont * m_pfntKey;
	CFont * m_pfntArrow;
	CFont * m_pfntNumpad;
	CBitmap m_bmpMouseLeft;
	CBitmap m_bmpMouseRight;
	CBitmap m_bmpLanguage;
	CBitmap m_bmpCharMap;
	CBitmap m_bmpExpand;
	CBitmap m_bmpCollapse;
	CBitmap m_bmpEsc;
	CBitmap m_bmpWinKey;
	CBitmap m_bmpFont;
	CBitmap m_bmpHelp;
	CBitmap m_bmpKeyboard;
	CStringArray m_vInputClasses;
	CStringArray m_vInputClassesExclude;
	CBitmap m_bmpArrowUp;
	CBitmap m_bmpArrowDown;
	CBitmap m_bmpArrowLeft;
	CBitmap m_bmpArrowRight;
	/* TODO: rem
	CPreview m_wndPreview;
	CRect m_rcRestore;
	*/
	HWND m_hwndRestore;
	CRect m_rcKbWin;
	bool m_bIgnoreNextClick;
	STARTUPINFO m_siKbWin;
	PROCESS_INFORMATION m_piKbWin;

	bool m_bDock;
	bool m_bNumpad;
	bool m_bXP;
	bool m_bUncheck;
	bool m_bStartKbMonitor;
	//bool m_bStartupShortcut;
	bool m_bExtendedChars;
	bool m_bLangMenuOpen;
	bool m_bPassword;
	struct
	{
		bool m_bHidden;
		bool m_bDocked;
		bool m_bUserOpened;
		bool m_bAnimate;
		int	m_nHide;
	} m_hide;

	// TODO: rem //HANDLE m_hThread;
	// TODO: rem //HANDLE m_hExit;
	HWND m_hwndFocus;
	HWND m_hwndLastFocus;
	clock_t m_tFocus;
	bool m_bLabelEdit;
	DWORD m_dwKbWinStyle;
	CString m_strConfigFile;

	static ULONG CALLBACK CharMapFunc (LPVOID lpData);
	// TODO: rem //static ULONG CALLBACK ThreadFunc (LPVOID lpData);

	void Resize ();

	CArray <CKey *, CKey *> m_vKeys; 
	std::map <std::wstring, std::wstring> m_mapLang;

	afx_msg void OnKey (UINT nID);
	afx_msg void OnWinKey ();
	afx_msg void OnNumpad (UINT nID);
	afx_msg void OnCaps ();
	afx_msg void OnNumlock ();
	afx_msg void OnAlt ();
	afx_msg void OnCharMap ();
	afx_msg void OnShow ();
	afx_msg void OnHide ();

	afx_msg void OnMenuHide ();		
	afx_msg void OnMenuDock ();		
	afx_msg void OnMenuF1 ();		
	afx_msg void OnMenuNumpad ();	
	afx_msg void OnMenuFont ();		
	afx_msg void OnLanguage();
	afx_msg void OnEditLayout ();

	afx_msg LRESULT OnIsKeyboard (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnIsAppRunning (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnIsKeyboardVisible (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnClick (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnRightClick (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFocusChanged (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSreenshot (WPARAM wParam, LPARAM lParam);

	afx_msg void OnMove(int x, int y);
	afx_msg LRESULT OnTrayNotify (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnShutdown (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnKbWinCreated (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetCheck (WPARAM wParam, LPARAM lParam);

	void SetTarget (CWnd* p, LPCTSTR lpszFile, ULONG lLine);
	void SendKey (int nVK, bool bDown);
	//bool HasStartMenuShortcut ();


	// Generated message map functions
	//{{AFX_MSG(CKeyboardDlg)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
	virtual BOOL OnInitDialog();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnActivateApp(BOOL bActive, DWORD hTask);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnNcLButtonDown(UINT nHitTest, CPoint point);
	afx_msg void OnNcLButtonUp(UINT nHitTest, CPoint point);
	afx_msg void OnWindowPosChanged(WINDOWPOS FAR* lpwndpos);
	afx_msg void OnDock();
	afx_msg void OnUndock();
	afx_msg void OnWindowPosChanging(WINDOWPOS FAR* lpwndpos);
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnNcLButtonDblClk(UINT nHitTest, CPoint point);
	afx_msg void OnMoving(UINT fwSide, LPRECT pRect);
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnFileClose();
	afx_msg void OnParentNotify(UINT message, LPARAM lParam);
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg void OnNcRButtonDown(UINT nHitTest, CPoint point);
	afx_msg void OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnNcRButtonUp(UINT nHitTest, CPoint point);
	afx_msg void OnDestroy();
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnF1();
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KEYBOARDDLG_H__E272FBD3_BF84_46FA_AC39_080A5E3F6ACB__INCLUDED_)
