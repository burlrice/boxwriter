// KeyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Keyboard.h"
#include "KeyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKeyDlg dialog


CKeyDlg::CKeyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CKeyDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CKeyDlg)
	m_strKey = _T("");
	m_strLower = _T("");
	m_strLowerExt = _T("");
	m_strUpper = _T("");
	m_strUpperExt = _T("");
	//}}AFX_DATA_INIT
}


void CKeyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CKeyDlg)
	DDX_Text(pDX, TXT_KEY, m_strKey);
	DDX_Text(pDX, TXT_LOWER, m_strLower);
	DDX_Text(pDX, TXT_LOWER_EXT, m_strLowerExt);
	DDX_Text(pDX, TXT_UPPER, m_strUpper);
	DDX_Text(pDX, TXT_UPPER_EXT, m_strUpperExt);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CKeyDlg, CDialog)
	//{{AFX_MSG_MAP(CKeyDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKeyDlg message handlers
