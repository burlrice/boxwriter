// KeyboardDlg.cpp : implementation file
//

#include "stdafx.h"

#include <atlbase.h>
#include <Richedit.h>
#include <Windows.h>

#include "utils.h"
#include "Debug.h"
#include "Resource.h"
#include "Keyboard.h"
#include "KeyboardDlg.h"
#include "Database.h"
#include "AnsiString.h"
//#include "WINABLE.H"
#include "WinMsg.H"
#include "AppVer.h"
#include "AboutDlg.h"
#include "Parse.h"
#include "FontDlg.h"
#include "Utils.h"
#include "..\..\..\Control\Host.h"
#include "Registry.h"
#include "AutoHideDlg.h"
#include "Color.h"
#include "LayoutDlg.h"

using namespace FoxjetUtils;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define SETFOREGROUNDWINDOW(w) SetForegroundWindowDebug ((w), _T (__FILE__), __LINE__)
#define SETTARGET(w) SetTarget ((w), _T (__FILE__), __LINE__)

using namespace FoxjetCommon;
using namespace FoxjetDatabase;

extern CKeyboardApp theApp;

const UINT nFlags = SWP_NOSIZE | SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE;
const int nRows				= 6;
const int nButtonColumns	= 15;
const int nKeypadColumns	= 9;
const UINT nButtons [::nRows][::nButtonColumns] =
{																	            /* [----------- arrow keys ----------------]    page up     page down   delete */
	{ BTN_1_1,		BTN_2_1,	BTN_3_1,	BTN_4_1,	BTN_5_1,	BTN_6_1,	BTN_7_1,	BTN_8_1,	BTN_9_1,	BTN_10_1,	BTN_11_1,	BTN_12_1,	BTN_13_1,	BTN_EXPAND,	-1,	},
	{ BTN_1_2,		BTN_2_2,	BTN_3_2,	BTN_4_2,	BTN_5_2,	BTN_6_2,	BTN_7_2,	BTN_8_2,	BTN_9_2,	BTN_10_2,	BTN_11_2,	BTN_12_2,	BTN_13_2,	BTN_14_2,	-1,	},
	{ BTN_1_3,		BTN_2_3,	BTN_3_3,	BTN_4_3,	BTN_5_3,	BTN_6_3,	BTN_7_3,	BTN_8_3,	BTN_9_3,	BTN_10_3,	BTN_11_3,	BTN_12_3,	BTN_13_3,	BTN_14_3,	-1,	},
	{ BTN_CAPS,		BTN_2_4,	BTN_3_4,	BTN_4_4,	BTN_5_4,	BTN_6_4,	BTN_7_4,	BTN_8_4,	BTN_9_4,	BTN_10_4,	BTN_11_4,	BTN_12_4,	BTN_13_4,	-1,			-1,	},
	{ BTN_SHIFT1,	BTN_2_5,	BTN_3_5,	BTN_4_5,	BTN_5_5,	BTN_6_5,	BTN_7_5,	BTN_8_5,	BTN_9_5,	BTN_10_5,	BTN_11_5,	BTN_SHIFT2,	-1,			-1,			-1,	},
	//{ BTN_CTRL1,	BTN_WINKEY,	BTN_ALT1,	BTN_CHARMAP,BTN_3_6,	BTN_ALT2,	BTN_MOUSE,	BTN_CTRL2,	-1,			-1,			-1,			-1,			-1,			-1,			-1,	},
	{ BTN_CTRL1,	BTN_ALT1,	BTN_3_6,	BTN_WINKEY,	BTN_MOUSE,	BTN_CHARMAP,-1,			-1,	-1,			-1,			-1,			-1,			-1,			-1,			-1,	},
};
const UINT nKeypad [::nRows][::nKeypadColumns] =
{
	{ BTN_KEYPAD_SYSREQ,	BTN_KEYPAD_SCROLLLOCK,	BTN_KEYPAD_BREAK,			0,	BTN_KEYPAD_EQUAL,		BTN_KEYPAD_LPAREN,		BTN_KEYPAD_RPAREN,		BTN_KEYPAD_BACK,	-1, },
	{ BTN_KEYPAD_INSERT,	BTN_KEYPAD_HOME,		BTN_KEYPAD_PAGEUP,			0,	BTN_KEYPAD_NUMLOCK,		BTN_KEYPAD_DIV,			BTN_KEYPAD_MULT,		BTN_KEYPAD_MINUS,	-1, },
	{ BTN_KEYPAD_DEL,		BTN_KEYPAD_END,			BTN_KEYPAD_PAGEDOWN,		0,	BTN_KEYPAD_7,			BTN_KEYPAD_8,			BTN_KEYPAD_9,			BTN_KEYPAD_PLUS,	-1, },
	{ -2,					-2,						-2,							0,	BTN_KEYPAD_4,			BTN_KEYPAD_5,			BTN_KEYPAD_6,			-1,					-1, },
	{ -2,					BTN_ARROW_UP,			-2,							0,	BTN_KEYPAD_1,			BTN_KEYPAD_2,			BTN_KEYPAD_3,			BTN_KEYPAD_ENTER,	-1, },
	{ BTN_ARROW_LEFT,		BTN_ARROW_DOWN,			BTN_ARROW_RIGHT,			0,	BTN_KEYPAD_0,			-2,						BTN_KEYPAD_PERIOD,		-1,					-1, },
};

static const KEYSTRUCT defKeys [67] = 
{
	KEYSTRUCT (BTN_1_1,		27,		0,		VK_ESCAPE		),	// 0
	KEYSTRUCT (BTN_2_1,		112,	0,		VK_F1			),	// 1 
	KEYSTRUCT (BTN_3_1,		113,	0,		VK_F2			),	// 2 
	KEYSTRUCT (BTN_4_1,		114,	0,		VK_F3			),	// 3 
	KEYSTRUCT (BTN_5_1,		115,	0,		VK_F4			),	// 4 
	KEYSTRUCT (BTN_6_1,		116,	0,		VK_F5			),	// 5 
	KEYSTRUCT (BTN_7_1,		117,	0,		VK_F6			),	// 6 
	KEYSTRUCT (BTN_8_1,		118,	0,		VK_F7			),	// 7 
	KEYSTRUCT (BTN_9_1,		119,	0,		VK_F8			),	// 8 
	KEYSTRUCT (BTN_10_1,	120,	0,		VK_F9			),	// 9 
	KEYSTRUCT (BTN_11_1,	121,	0,		VK_F10			),	// 10	
	KEYSTRUCT (BTN_12_1,	122,	0,		VK_F11			),	// 11 	
	KEYSTRUCT (BTN_13_1,	123,	0,		VK_F12			),	// 12 

	KEYSTRUCT (BTN_1_2,		'`',	'~',	0				),	// 13 		
	KEYSTRUCT (BTN_2_2,		'1',	'!',	0				),	// 14 
	KEYSTRUCT (BTN_3_2,		'2',	'@',	0				),	// 15 
	KEYSTRUCT (BTN_4_2,		'3',	'#',	0				),	// 16 
	KEYSTRUCT (BTN_5_2,		'4',	'$',	0				),	// 17 
	KEYSTRUCT (BTN_6_2,		'5',	'%',	0				),	// 18 
	KEYSTRUCT (BTN_7_2,		'6',	'^',	0				),	// 19 
	KEYSTRUCT (BTN_8_2,		'7',	'&',	0				),	// 20
	KEYSTRUCT (BTN_9_2,		'8',	'*',	0				),	// 21 
	KEYSTRUCT (BTN_10_2,	'9',	'(',	0				),	// 22 
	KEYSTRUCT (BTN_11_2,	'0',	')',	0				),	// 23 
	KEYSTRUCT (BTN_12_2,	'-',	'_',	0				),	// 24 
	KEYSTRUCT (BTN_13_2,	'=',	'+',	0				),	// 25 
	KEYSTRUCT (BTN_14_2,	8,		0,		VK_BACK			),	// 26 

	KEYSTRUCT (BTN_1_3,		'\t',	'\t',	VK_TAB			),	// 27 	
	KEYSTRUCT (BTN_2_3,		'q',	'Q',	0				),	// 28 
	KEYSTRUCT (BTN_3_3,		'w',	'W',	0				),	// 29 
	KEYSTRUCT (BTN_4_3,		'e',	'E',	0				),	// 30
	KEYSTRUCT (BTN_5_3,		'r',	'R',	0				),	// 31 
	KEYSTRUCT (BTN_6_3,		't',	'T',	0				),	// 32 
	KEYSTRUCT (BTN_7_3,		'y',	'Y',	0				),	// 33 
	KEYSTRUCT (BTN_8_3,		'u',	'U',	0				),	// 34 
	KEYSTRUCT (BTN_9_3,		'i',	'I',	0				),	// 35 
	KEYSTRUCT (BTN_10_3,	 'o',	'O',	0				),	// 36 
	KEYSTRUCT (BTN_11_3,	 'p',	'P',	0				),	// 37 
	KEYSTRUCT (BTN_12_3,	 '[',	'{',	0				),	// 38 
	KEYSTRUCT (BTN_13_3,	 ']',	'}',	0				),	// 39 
	KEYSTRUCT (BTN_14_3,	'\\',	'|',	0				),	// 40

	KEYSTRUCT (BTN_CAPS,	0,		0,		VK_CAPITAL		),	// 41 
	KEYSTRUCT (BTN_2_4,		'a',	'A',	0				),	// 42 	
	KEYSTRUCT (BTN_3_4,		's',	'S',	0				),	// 43 
	KEYSTRUCT (BTN_4_4,		'd',	'D',	0				),	// 44 
	KEYSTRUCT (BTN_5_4,		'f',	'F',	0				),	// 45 
	KEYSTRUCT (BTN_6_4,		'g',	'G',	0				),	// 46 
	KEYSTRUCT (BTN_7_4,		'h',	'H',	0				),	// 47 
	KEYSTRUCT (BTN_8_4,		'j',	'J',	0				),	// 48 
	KEYSTRUCT (BTN_9_4,		'k',	'K',	0				),	// 49 
	KEYSTRUCT (BTN_10_4,	 'l',	'L',	0				),	// 50
	KEYSTRUCT (BTN_11_4,	 ';',	':',	0				),	// 51 
	KEYSTRUCT (BTN_12_4,	 '\'',	'\"',	0				),	// 52 
	KEYSTRUCT (BTN_13_4,	'\r',	0,		VK_RETURN		),	// 53 	
	
	KEYSTRUCT (BTN_2_5,		'z',	'Z',	0				),	// 54 	
	KEYSTRUCT (BTN_3_5,		'x',	'X',	0				),	// 55 
	KEYSTRUCT (BTN_4_5,		'c',	'C',	0				),	// 56 
	KEYSTRUCT (BTN_5_5,		'v',	'V',	0				),	// 57 
	KEYSTRUCT (BTN_6_5,		'b',	'B',	0				),	// 58 
	KEYSTRUCT (BTN_7_5,		'n',	'N',	0				),	// 59 
	KEYSTRUCT (BTN_8_5,		'm',	'M',	0				),	// 60
	KEYSTRUCT (BTN_9_5,		',',	'<',	0				),	// 61 
	KEYSTRUCT (BTN_10_5,	'.',	'>',	0				),	// 62 
	KEYSTRUCT (BTN_11_5,	'/',	'?',	0				),	// 63 

	KEYSTRUCT (BTN_3_6,		' ',	0,		0				),	// 64 
	KEYSTRUCT (BTN_5_6,		8,		0,		VK_DELETE		),	// 65 

	KEYSTRUCT (BTN_WINKEY, VK_LWIN,	VK_LWIN,	VK_LWIN		), // 66
};	

/////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
CString GetMessageString (UINT nID)
{
	struct
	{
		UINT m_n;
		LPCTSTR m_lpsz;
	}
	static const map [] =
	{
		{ WM_ACTIVATE,		_T ("WM_ACTIVATE"),		},
//		{ WM_APPCOMMAND,	_T ("WM_APPCOMMAND"),	},
		{ WM_CHAR,			_T ("WM_CHAR"),			},
		{ WM_DEADCHAR,		_T ("WM_DEADCHAR"),		},
		{ WM_GETHOTKEY,		_T ("WM_GETHOTKEY"),	},
		{ WM_HOTKEY,		_T ("WM_HOTKEY"),		},
		{ WM_KEYDOWN,		_T ("WM_KEYDOWN"),		},
		{ WM_KEYUP,			_T ("WM_KEYUP"),		},
		{ WM_KILLFOCUS,		_T ("WM_KILLFOCUS"),	},
		{ WM_SETFOCUS,		_T ("WM_SETFOCUS"),		},
		{ WM_SETHOTKEY,		_T ("WM_SETHOTKEY"),	},
		{ WM_SYSCHAR,		_T ("WM_SYSCHAR"),		},
		{ WM_SYSDEADCHAR,	_T ("WM_SYSDEADCHAR"),	},
		{ WM_SYSKEYDOWN,	_T ("WM_SYSKEYDOWN"),	},
		{ WM_SYSKEYUP ,		_T ("WM_SYSKEYUP "),	},
	};

	for (int i = 0; i < ARRAYSIZE (map); i++)
		if (map [i].m_n == nID)
			return map [i].m_lpsz;

	CString str;
	str.Format (_T ("0x%p"), nID);
	return str;
}

CString GetStyleString (UINT n, UINT nEX)
{
	struct
	{
		UINT m_dw;
		LPCTSTR m_lpsz;
	} const style [] = 
	{
		{ WS_BORDER,				_T ("WS_BORDER"),				},
		{ WS_CAPTION,				_T ("WS_CAPTION"),				},
		{ WS_CHILD,					_T ("WS_CHILD"),				},
		{ WS_CHILDWINDOW,			_T ("WS_CHILDWINDOW"),			},
		{ WS_CLIPCHILDREN,			_T ("WS_CLIPCHILDREN"),			},
		{ WS_CLIPSIBLINGS,			_T ("WS_CLIPSIBLINGS"),			},
		{ WS_DISABLED,				_T ("WS_DISABLED"),				},
		{ WS_DLGFRAME,				_T ("WS_DLGFRAME"),				},
		{ WS_GROUP,					_T ("WS_GROUP"),				},
		{ WS_HSCROLL,				_T ("WS_HSCROLL"),				},
		{ WS_ICONIC,				_T ("WS_ICONIC"),				},
		{ WS_MAXIMIZE,				_T ("WS_MAXIMIZE"),				},
		{ WS_MAXIMIZEBOX,			_T ("WS_MAXIMIZEBOX"),			},
		{ WS_MINIMIZE,				_T ("WS_MINIMIZE"),				},
		{ WS_MINIMIZEBOX,			_T ("WS_MINIMIZEBOX"),			},
		{ WS_OVERLAPPED,			_T ("WS_OVERLAPPED"),			},
		{ WS_OVERLAPPEDWINDOW,		_T ("WS_OVERLAPPEDWINDOW"),		},
		{ WS_POPUP,					_T ("WS_POPUP"),				},
		{ WS_POPUPWINDOW,			_T ("WS_POPUPWINDOW"),			},
		{ WS_SIZEBOX,				_T ("WS_SIZEBOX"),				},
		{ WS_SYSMENU,				_T ("WS_SYSMENU"),				},
		{ WS_TABSTOP,				_T ("WS_TABSTOP"),				},
		{ WS_THICKFRAME,			_T ("WS_THICKFRAME"),			},
		{ WS_TILED,					_T ("WS_TILED"),				},
		{ WS_TILEDWINDOW,			_T ("WS_TILEDWINDOW"),			},
		{ WS_VISIBLE,				_T ("WS_VISIBLE"),				},
		{ WS_VSCROLL,				_T ("WS_VSCROLL"),				},

		{ ES_AUTOHSCROLL,			_T ("ES_AUTOHSCROLL"),			},
		{ ES_AUTOVSCROLL,			_T ("ES_AUTOVSCROLL"),			},
		{ ES_CENTER,				_T ("ES_CENTER"),				},
		{ ES_LEFT,					_T ("ES_LEFT"),					},
		{ ES_LOWERCASE,				_T ("ES_LOWERCASE"),			},
		{ ES_MULTILINE,				_T ("ES_MULTILINE"),			},
		{ ES_NOHIDESEL,				_T ("ES_NOHIDESEL"),			},
		{ ES_OEMCONVERT,			_T ("ES_OEMCONVERT"),			},
		{ ES_PASSWORD,				_T ("ES_PASSWORD"),				},
		{ ES_RIGHT,					_T ("ES_RIGHT"),				},
		{ ES_UPPERCASE,				_T ("ES_UPPERCASE"),			},
		{ ES_READONLY,				_T ("ES_READONLY"),				},
		{ ES_WANTRETURN,			_T ("ES_WANTRETURN "),			},
	};
	struct
	{
		UINT m_dw;
		LPCTSTR m_lpsz;
	} const exstyle [] = 
	{
		{ WS_EX_ACCEPTFILES,		_T ("WS_EX_ACCEPTFILES"),		},	
		{ WS_EX_APPWINDOW,			_T ("WS_EX_APPWINDOW"),			},	
		{ WS_EX_CLIENTEDGE,			_T ("WS_EX_CLIENTEDGE"),		},		
		//{ WS_EX_COMPOSITED,			_T ("WS_EX_COMPOSITED"),		},
		{ WS_EX_CONTEXTHELP,		_T ("WS_EX_CONTEXTHELP"),		},	
		{ WS_EX_CONTROLPARENT,		_T ("WS_EX_CONTROLPARENT"),		},	
		{ WS_EX_DLGMODALFRAME,		_T ("WS_EX_DLGMODALFRAME"),		},	
		//{ WS_EX_LAYERED,			_T ("WS_EX_LAYERED"),			},	
		//{ WS_EX_LAYOUTRTL,			_T ("WS_EX_LAYOUTRTL"),			},	
		{ WS_EX_LEFT,				_T ("WS_EX_LEFT"),				},	
		{ WS_EX_LEFTSCROLLBAR,		_T ("WS_EX_LEFTSCROLLBAR"),		},	
		{ WS_EX_LTRREADING,			_T ("WS_EX_LTRREADING"),		},	
		{ WS_EX_MDICHILD,			_T ("WS_EX_MDICHILD"),			},	
		{ WS_EX_NOACTIVATE,			_T ("WS_EX_NOACTIVATE"),		},	
		//{ WS_EX_NOINHERITLAYOUT,	_T ("WS_EX_NOINHERITLAYOUT"),	},	
		{ WS_EX_NOPARENTNOTIFY,		_T ("WS_EX_NOPARENTNOTIFY"),	},	
		{ WS_EX_OVERLAPPEDWINDOW,	_T ("WS_EX_OVERLAPPEDWINDOW"),	},	
		{ WS_EX_PALETTEWINDOW,		_T ("WS_EX_PALETTEWINDOW"),		},	
		{ WS_EX_RIGHT,				_T ("WS_EX_RIGHT"),				},	
		{ WS_EX_RIGHTSCROLLBAR,		_T ("WS_EX_RIGHTSCROLLBAR"),	},	
		{ WS_EX_RTLREADING,			_T ("WS_EX_RTLREADING"),		},	
		{ WS_EX_STATICEDGE,			_T ("WS_EX_STATICEDGE"),		},	
		{ WS_EX_TOOLWINDOW,			_T ("WS_EX_TOOLWINDOW"),		},	
		{ WS_EX_TOPMOST,			_T ("WS_EX_TOPMOST"),			},
		{ WS_EX_TRANSPARENT,		_T ("WS_EX_TRANSPARENT"),		},			
		{ WS_EX_WINDOWEDGE,			_T ("WS_EX_WINDOWEDGE"),		},
	}; 
	CString str;

	for (int i = 0; i < ARRAYSIZE (style); i++)
		if (style [i].m_dw & n)
			str += style [i].m_lpsz + CString (_T (" "));

	for (int i = 0; i < ARRAYSIZE (exstyle); i++)
		if (exstyle [i].m_dw & nEX)
			str += exstyle [i].m_lpsz + CString (_T (" "));

	return str;
}
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct tagVK_DEBUG_STRUCT
{
	tagVK_DEBUG_STRUCT (UINT nID, LPCTSTR lpsz) : m_nID (nID), m_lpsz (lpsz) { }

	UINT m_nID;
	LPCTSTR m_lpsz;
} VK_DEBUG_STRUCT;

#define VK_DEBUG_STRING(s) VK_DEBUG_STRUCT (s, _T (#s))

static const VK_DEBUG_STRUCT vkDebug [] =
{
	VK_DEBUG_STRING (VK_LBUTTON),        
	VK_DEBUG_STRING (VK_RBUTTON),        
	VK_DEBUG_STRING (VK_CANCEL),
	VK_DEBUG_STRING (VK_MBUTTON),
	VK_DEBUG_STRING (VK_BACK),
	VK_DEBUG_STRING (VK_TAB),
	VK_DEBUG_STRING (VK_CLEAR),
	VK_DEBUG_STRING (VK_RETURN),
	VK_DEBUG_STRING (VK_SHIFT),
	VK_DEBUG_STRING (VK_CONTROL),
	VK_DEBUG_STRING (VK_MENU),
	VK_DEBUG_STRING (VK_PAUSE),
	VK_DEBUG_STRING (VK_CAPITAL),
	VK_DEBUG_STRING (VK_KANA),
	VK_DEBUG_STRING (VK_HANGEUL),
	VK_DEBUG_STRING (VK_HANGUL),
	VK_DEBUG_STRING (VK_JUNJA),
	VK_DEBUG_STRING (VK_FINAL),
	VK_DEBUG_STRING (VK_HANJA),
	VK_DEBUG_STRING (VK_KANJI),
	VK_DEBUG_STRING (VK_ESCAPE),
	VK_DEBUG_STRING (VK_CONVERT),
	VK_DEBUG_STRING (VK_NONCONVERT),
	VK_DEBUG_STRING (VK_ACCEPT),
	VK_DEBUG_STRING (VK_MODECHANGE),
	VK_DEBUG_STRING (VK_SPACE),
	VK_DEBUG_STRING (VK_PRIOR),
	VK_DEBUG_STRING (VK_NEXT),
	VK_DEBUG_STRING (VK_END),
	VK_DEBUG_STRING (VK_HOME),
	VK_DEBUG_STRING (VK_LEFT),
	VK_DEBUG_STRING (VK_UP),
	VK_DEBUG_STRING (VK_RIGHT),
	VK_DEBUG_STRING (VK_DOWN),
	VK_DEBUG_STRING (VK_SELECT),
	VK_DEBUG_STRING (VK_PRINT),
	VK_DEBUG_STRING (VK_EXECUTE),
	VK_DEBUG_STRING (VK_SNAPSHOT),
	VK_DEBUG_STRING (VK_INSERT),
	VK_DEBUG_STRING (VK_DELETE),
	VK_DEBUG_STRING (VK_HELP),
	VK_DEBUG_STRING (VK_LWIN),
	VK_DEBUG_STRING (VK_RWIN),
	VK_DEBUG_STRING (VK_APPS),
	VK_DEBUG_STRING (VK_NUMPAD0),
	VK_DEBUG_STRING (VK_NUMPAD1),
	VK_DEBUG_STRING (VK_NUMPAD2),
	VK_DEBUG_STRING (VK_NUMPAD3),
	VK_DEBUG_STRING (VK_NUMPAD4),
	VK_DEBUG_STRING (VK_NUMPAD5),
	VK_DEBUG_STRING (VK_NUMPAD6),
	VK_DEBUG_STRING (VK_NUMPAD7),
	VK_DEBUG_STRING (VK_NUMPAD8),
	VK_DEBUG_STRING (VK_NUMPAD9),
	VK_DEBUG_STRING (VK_MULTIPLY),
	VK_DEBUG_STRING (VK_ADD),
	VK_DEBUG_STRING (VK_SEPARATOR),
	VK_DEBUG_STRING (VK_SUBTRACT),
	VK_DEBUG_STRING (VK_DECIMAL),
	VK_DEBUG_STRING (VK_DIVIDE),
	VK_DEBUG_STRING (VK_F1),
	VK_DEBUG_STRING (VK_F2),
	VK_DEBUG_STRING (VK_F3),
	VK_DEBUG_STRING (VK_F4),
	VK_DEBUG_STRING (VK_F5),
	VK_DEBUG_STRING (VK_F6),
	VK_DEBUG_STRING (VK_F7),
	VK_DEBUG_STRING (VK_F8),
	VK_DEBUG_STRING (VK_F9),
	VK_DEBUG_STRING (VK_F10),
	VK_DEBUG_STRING (VK_F11),
	VK_DEBUG_STRING (VK_F12),
	VK_DEBUG_STRING (VK_F13),
	VK_DEBUG_STRING (VK_F14),
	VK_DEBUG_STRING (VK_F15),
	VK_DEBUG_STRING (VK_F16),
	VK_DEBUG_STRING (VK_F17),
	VK_DEBUG_STRING (VK_F18),
	VK_DEBUG_STRING (VK_F19),
	VK_DEBUG_STRING (VK_F20),
	VK_DEBUG_STRING (VK_F21),
	VK_DEBUG_STRING (VK_F22),
	VK_DEBUG_STRING (VK_F23),
	VK_DEBUG_STRING (VK_F24),
	VK_DEBUG_STRING (VK_NUMLOCK),
	VK_DEBUG_STRING (VK_SCROLL),
	VK_DEBUG_STRING (VK_LSHIFT),
	VK_DEBUG_STRING (VK_RSHIFT),
	VK_DEBUG_STRING (VK_LCONTROL),
	VK_DEBUG_STRING (VK_RCONTROL),
	VK_DEBUG_STRING (VK_LMENU),
	VK_DEBUG_STRING (VK_RMENU),
	VK_DEBUG_STRING (VK_ATTN),
	VK_DEBUG_STRING (VK_CRSEL),
	VK_DEBUG_STRING (VK_EXSEL),
	VK_DEBUG_STRING (VK_EREOF),
	VK_DEBUG_STRING (VK_PLAY),
	VK_DEBUG_STRING (VK_ZOOM),
	VK_DEBUG_STRING (VK_NONAME),
	VK_DEBUG_STRING (VK_PA1),
	VK_DEBUG_STRING (VK_OEM_CLEAR),
};

static const VK_DEBUG_STRUCT wsDebug [] =
{
	VK_DEBUG_STRING (WS_OVERLAPPED	 ),
	VK_DEBUG_STRING (WS_POPUP        ),
	VK_DEBUG_STRING (WS_CHILD        ),
	VK_DEBUG_STRING (WS_MINIMIZE     ),
	VK_DEBUG_STRING (WS_VISIBLE      ),
	VK_DEBUG_STRING (WS_DISABLED     ),
	VK_DEBUG_STRING (WS_CLIPSIBLINGS ),
	VK_DEBUG_STRING (WS_CLIPCHILDREN ),
	VK_DEBUG_STRING (WS_MAXIMIZE     ),
	VK_DEBUG_STRING (WS_CAPTION      ),
	VK_DEBUG_STRING (WS_BORDER       ),
	VK_DEBUG_STRING (WS_DLGFRAME     ),
	VK_DEBUG_STRING (WS_VSCROLL      ),
	VK_DEBUG_STRING (WS_HSCROLL      ),
	VK_DEBUG_STRING (WS_SYSMENU      ),
	VK_DEBUG_STRING (WS_THICKFRAME   ),
	VK_DEBUG_STRING (WS_GROUP        ),
	VK_DEBUG_STRING (WS_TABSTOP      ),
	VK_DEBUG_STRING (WS_MINIMIZEBOX  ),
	VK_DEBUG_STRING (WS_MAXIMIZEBOX  ),
};

static const VK_DEBUG_STRUCT wsExDebug [] =
{
	VK_DEBUG_STRING (WS_EX_DLGMODALFRAME     ),
	VK_DEBUG_STRING (WS_EX_NOPARENTNOTIFY    ),
	VK_DEBUG_STRING (WS_EX_TOPMOST           ),
	VK_DEBUG_STRING (WS_EX_ACCEPTFILES       ),
	VK_DEBUG_STRING (WS_EX_TRANSPARENT       ),
	VK_DEBUG_STRING (WS_EX_MDICHILD          ),
	VK_DEBUG_STRING (WS_EX_TOOLWINDOW        ),
	VK_DEBUG_STRING (WS_EX_WINDOWEDGE        ),
	VK_DEBUG_STRING (WS_EX_CLIENTEDGE        ),
	VK_DEBUG_STRING (WS_EX_CONTEXTHELP       ),
	VK_DEBUG_STRING (WS_EX_RIGHT             ),
	VK_DEBUG_STRING (WS_EX_LEFT              ),
	VK_DEBUG_STRING (WS_EX_RTLREADING        ),
	VK_DEBUG_STRING (WS_EX_LTRREADING        ),
	VK_DEBUG_STRING (WS_EX_LEFTSCROLLBAR     ),
	VK_DEBUG_STRING (WS_EX_RIGHTSCROLLBAR    ),
	VK_DEBUG_STRING (WS_EX_CONTROLPARENT     ),
	VK_DEBUG_STRING (WS_EX_STATICEDGE        ),
	VK_DEBUG_STRING (WS_EX_APPWINDOW         ),
};

static CString GetVkString (UINT nID)
{
	CString str;

	for (int i = 0; i < ARRAYSIZE (::vkDebug); i++) 
		if (::vkDebug [i].m_nID == nID)
			return ::vkDebug [i].m_lpsz;

	str.Format (_T ("VK unknown: 0x%p"), nID);

	return str;
}

static CString GetWsStyle (DWORD dw)
{
	CString str;

	for (int i = 0; i < ARRAYSIZE (::wsDebug); i++) {
		if (::wsDebug [i].m_nID & dw) {
			str += ::wsDebug [i].m_lpsz + CString (_T (" "));
		}
	}

	return str;
}

static CString GetWsExStyle (DWORD dw)
{
	CString str;

	for (int i = 0; i < ARRAYSIZE (::wsExDebug); i++) {
		if (::wsExDebug [i].m_nID & dw) {
			str += ::wsExDebug [i].m_lpsz + CString (_T (" "));
		}
	}

	return str;
}

static BOOL SetForegroundWindowDebug (HWND hwnd, LPCTSTR lpszFile, ULONG lLine)
{
	return TRUE;

//	/*
	#ifdef _DEBUG
	TCHAR sz [128] = { 0 };
	CString str;

	::GetClassName (::GetForegroundWindow (), sz, ARRAYSIZE (sz) - 1);
	str.Format (_T ("                     0x%p: %s"), ::GetForegroundWindow (), sz);
	CDebug::Trace (str, true, lpszFile, lLine);

	::GetClassName (hwnd, sz, ARRAYSIZE (sz) - 1);
	str.Format (_T ("SetForegroundWindow: 0x%p: %s"), hwnd, sz);
	CDebug::Trace (str, true, lpszFile, lLine);
	#endif
//	*/

	return ::SetForegroundWindow (hwnd);
}

void CTarget::SetTarget (HWND hWnd)
{
	if (m_hTarget != hWnd) {
		m_hTarget = hWnd;
		REPAINT (m_pParent);
	}
}

HRESULT CreateShortcut (CString strTargetfile, CString strTargetargs, CString strLinkfile, CString strDescription, 
	int iShowmode, CString strCurdir, CString strIconfile, int iIconindex)
{
	HRESULT       hRes;
	IShellLink*   pShellLink;
	IPersistFile* pPersistFile;

	{
		TCHAR szTarget [MAX_PATH] = { 0 };
		TCHAR szLink [MAX_PATH] = { 0 };

		::GetFileTitle (strTargetfile, szTarget, ARRAYSIZE (szTarget));
		::GetFileTitle (strLinkfile, szLink, ARRAYSIZE (szLink));

		CString strTarget = szTarget;
		CString strLink = szLink;

		strTarget.MakeLower ();
		strLink.MakeLower ();

		strTarget.Replace (_T (".exe"), _T (""));
		strLink.Replace (_T (".lnk"), _T (""));

		//ASSERT (strTarget == strLink || strLink.Find (strTarget) != -1);
	}

	hRes = E_INVALIDARG;
	if ((strTargetfile.GetLength () > 0) &&
		(strLinkfile.GetLength () > 0) &&
		(iShowmode >= 0) &&
		(iIconindex >= 0))
	{
		
		::DeleteFile (strLinkfile);

		hRes = CoCreateInstance (
			CLSID_ShellLink,
			NULL,
			CLSCTX_INPROC_SERVER,
			IID_IShellLink,
			(LPVOID *)&pShellLink);
		
		if (SUCCEEDED (hRes)) {
			hRes = pShellLink->SetPath(strTargetfile);
			hRes = pShellLink->SetArguments(strTargetargs);
	
			if (strDescription.GetLength () > 0) 
				hRes = pShellLink->SetDescription(strDescription);
			
			if (iShowmode > 0) 
				hRes = pShellLink->SetShowCmd(iShowmode);

			if (strCurdir.GetLength () > 0) 
				hRes = pShellLink->SetWorkingDirectory(strCurdir);

			if (strIconfile.GetLength () > 0 && iIconindex >= 0) 
				hRes = pShellLink->SetIconLocation(strIconfile, iIconindex);

			hRes = pShellLink->QueryInterface (IID_IPersistFile, (LPVOID*)&pPersistFile);

			if (SUCCEEDED(hRes)) {
				BSTR b = strLinkfile.AllocSysString ();
				hRes = pPersistFile->Save (b, TRUE);

				//#ifdef _DEBUG
				{
					CString str;

					str.Format (
						_T ("\r\n\tCreateShortcut: %s")
						_T ("\r\n\tstrTargetfile:  %s")
						_T ("\r\n\tstrTargetargs:  %s")
						_T ("\r\n\tstrLinkfile:    %s")
						_T ("\r\n\tstrDescription: %s")
						_T ("\r\n\tiShowmode:      %d")
						_T ("\r\n\tstrCurdir:      %s")
						_T ("\r\n\tstrIconfile:    %s")
						_T ("\r\n\tiIconindex:     %d")
						_T ("\r\n\t%s"),
						CString (b),
						strTargetfile, 
						strTargetargs, 
						strLinkfile, 
						strDescription, 
						iShowmode, 
						strCurdir, 
						strIconfile, 
						iIconindex,
						SUCCEEDED (hRes) ? _T ("SUCCEEDED") : _T ("FAILED"));
					TRACEF (str);
				}
				//#endif

				::SysFreeString (b);

				pPersistFile->Release();
			}

			pShellLink->Release();
		}
	}

	return hRes;
}

/////////////////////////////////////////////////////////////////////////////
// CKeyboardDlg dialog

const CString CKeyboardDlg::m_strWndClassesExclude = 
	_T ("{")
	_T ("Button,")
	_T ("ComboBox,")
	_T ("ListBox,")
	_T ("ScrollBar,")
	_T ("Static,")
	_T ("SysListView32,")
	_T ("SysTreeView32,")
	_T ("ComboLBox,")		// The class for the list box contained in a combo box.
	_T ("DDEMLEvent,")		// The class for Dynamic Data Exchange Management Library (DDEML) events.
	_T ("Message,")			// The class for a message-only window.
	_T ("#32768,")			// The class for a menu.
	_T ("#32769,")			// The class for the desktop window.
	_T ("#32770,")			// The class for a dialog box.
	_T ("#32771,")			// The class for the task switch window.
	_T ("#32772,")			// The class for icon titles.
	_T ("ToolbarWindow32,")
	_T ("Shell_TrayWnd,")
	_T ("AfxFrameOrView42ud,")
	_T ("SysDateTimePick32,")
	_T ("}");
const CString CKeyboardDlg::m_strWndClasses = _T ("{Edit,RICHEDIT50,ComboBox}");

CKeyboardDlg::CKeyboardDlg(CWnd* pParent /*=NULL*/)
:	m_target (this),
	m_hAccel (NULL),
	m_bDock (true),
	m_bNumpad (false),
	m_pfntKey (NULL),
	m_pfntArrow (NULL),
	m_pfntNumpad (NULL),
	m_bXP (false),
	m_bUncheck (false),
	m_bStartKbMonitor (true),
	m_hCharMapThread (NULL),
	m_hwndFocus (NULL),
	m_hwndLastFocus (NULL),
	m_bLabelEdit (false),
	m_tFocus (clock ()),
	m_bExtendedChars (false),
	m_hwndRestore (NULL),
	m_bIgnoreNextClick (false),
	m_rcKbWin (0, 0, 0, 0),
	m_bLangMenuOpen (false),
	m_dwKbWinStyle (0),
	m_strConfigFile (_T ("Keyboard_en.txt")),
	m_bPassword (false),
	CDialog (IDD_KEYBOARD, pParent)
{
	//{{AFX_DATA_INIT(CKeyboardDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_hwndLastFocus = ::GetForegroundWindow ();

	m_hide.m_bDocked		= false;
	m_hide.m_bHidden		= false;
	m_hide.m_bUserOpened	= false;
	m_hide.m_bAnimate		= true;
	m_hide.m_nHide			= 0;
	//m_bStartupShortcut = HasStartMenuShortcut ();

	memset (&m_piKbWin, 0, sizeof(m_piKbWin));
	memset (&m_siKbWin, 0, sizeof(m_siKbWin));

	VERIFY (m_bmpMouseLeft.LoadBitmap (IDB_MOUSELEFT));
	VERIFY (m_bmpMouseRight.LoadBitmap (IDB_MOUSERIGHT));
	VERIFY (m_bmpCharMap.LoadBitmap (IDB_CHARMAP));
	VERIFY (m_bmpExpand.LoadBitmap (IDB_EXPAND));
	VERIFY (m_bmpCollapse.LoadBitmap (!IsMatrix () ? IDB_COLLAPSE : IDB_OK));
	VERIFY (m_bmpEsc.LoadBitmap (IDB_CANCEL));
	VERIFY (m_bmpWinKey.LoadBitmap (IDB_WINDOWS));
	VERIFY (m_bmpLanguage.LoadBitmap (IDB_LANGUAGE));
	VERIFY (m_bmpFont.LoadBitmap (IDB_FONT));
	VERIFY (m_bmpHelp.LoadBitmap (IDB_HELP));
	VERIFY (m_bmpKeyboard.LoadBitmap (IDB_KEYBOARD));
	VERIFY (m_bmpArrowUp.LoadBitmap (IDB_ARROWUP));
	VERIFY (m_bmpArrowDown.LoadBitmap (IDB_ARROWDOWN));
	VERIFY (m_bmpArrowLeft.LoadBitmap (IDB_ARROWLEFT));
	VERIFY (m_bmpArrowRight.LoadBitmap (IDB_ARROWRIGHT));

	m_hIcon = theApp.LoadIcon (IDI_TRAY);

	m_pfntKey = CreateFontFromRegistry (_T ("Normal"));
	m_pfntNumpad = CreateFontFromRegistry (_T ("Small"));
	m_pfntArrow = CreateArrowFont (theApp.GetProfileInt (_T ("CKeyboardDlg"), _T ("arrow size"), 22));

	{ LOGFONT lf; m_pfntKey->GetLogFont (&lf); TRACEF (_T ("normal: ") + ToString (lf.lfHeight)); }
	{ LOGFONT lf; m_pfntNumpad->GetLogFont (&lf); TRACEF (_T ("small: ") + ToString (lf.lfHeight)); }

	LoadInputClasses ();
}

CKeyboardDlg::~CKeyboardDlg ()
{
	if (m_pfntKey) delete m_pfntKey;
	if (m_pfntArrow) delete m_pfntArrow;
	if (m_pfntNumpad) delete m_pfntNumpad;

	while (m_vKeys.GetSize ()> 0) {
		if (CKey * p = m_vKeys [0]) 
			delete p;

		m_vKeys.RemoveAt (0);
	}

	/* TODO: rem
	if (m_hExit) {
		CloseHandle (m_hExit);
		m_hExit = NULL;
	}
	*/
}

void CKeyboardDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CKeyboardDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	DDX_Control(pDX, BTN_CAPS,				m_btnCaps);
	DDX_Control(pDX, BTN_SHIFT1,			m_btnShift1);
	DDX_Control(pDX, BTN_SHIFT2,			m_btnShift2);
	DDX_Control(pDX, BTN_CTRL1,				m_btnCtrl1);
	DDX_Control(pDX, BTN_ALT1,				m_btnCtrl2);
//	DDX_Control(pDX, BTN_ALT2,				m_btnAlt1);
//	DDX_Control(pDX, BTN_CTRL2,				m_btnAlt2);
	DDX_Control(pDX, BTN_KEYPAD_NUMLOCK,	m_btnKeypadNumlock);
	DDX_Control(pDX, BTN_KEYPAD_SCROLLLOCK, m_btnKeypadScrollLock);
	DDX_Control(pDX, BTN_MOUSE,				m_btnMouse);
	DDX_Control(pDX, BTN_CHARMAP,			m_btnCharMap);
	DDX_Control(pDX, BTN_EXPAND,			m_btnExpand);
	DDX_Control(pDX, BTN_ESC,				m_btnEsc);
}

BEGIN_MESSAGE_MAP(CKeyboardDlg, CDialog)
	//{{AFX_MSG_MAP(CKeyboardDlg)
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_CHAR()
	ON_WM_ACTIVATEAPP()
	ON_WM_TIMER()
	ON_WM_CREATE()
	ON_WM_NCLBUTTONDOWN()
	ON_WM_NCLBUTTONUP()
	ON_WM_WINDOWPOSCHANGED()
	ON_COMMAND(IDA_DOCK, OnDock)
	ON_COMMAND(IDA_UNDOCK, OnUndock)
	ON_WM_WINDOWPOSCHANGING()
	ON_WM_HELPINFO()
	ON_WM_NCLBUTTONDBLCLK()
	ON_WM_MOVING()
	ON_WM_INITMENUPOPUP()
	ON_WM_SYSCOMMAND()
	ON_COMMAND(ID_EXIT, OnFileClose)
	ON_WM_PARENTNOTIFY()
	ON_WM_NCHITTEST()
	ON_WM_NCRBUTTONDOWN()
	ON_WM_SYSKEYDOWN()
	ON_WM_NCRBUTTONUP()
	ON_WM_DESTROY()
	ON_WM_MEASUREITEM()
	ON_WM_DRAWITEM()
	ON_COMMAND(IDA_F1, OnF1)
	ON_WM_ACTIVATE()
	//}}AFX_MSG_MAP

	ON_COMMAND(IDM_HIDE, OnMenuHide)
	ON_COMMAND(IDM_DOCK, OnMenuDock)
	ON_COMMAND(IDM_NUMPAD, OnMenuNumpad)
	ON_COMMAND(IDM_FONT, OnMenuFont)
	ON_COMMAND(IDA_F1, OnMenuF1)
	ON_COMMAND(IDM_EDIT_LAYOUT, OnEditLayout)


	ON_BN_CLICKED (BTN_EXPAND, OnExpand)
	ON_BN_CLICKED (BTN_ESC, OnEsc)
	ON_BN_CLICKED (BTN_CAPS, OnCaps)

	ON_CONTROL_RANGE (BN_CLICKED, BTN_1_1, BTN_5_6, OnKey)
	ON_CONTROL_RANGE (BN_CLICKED, BTN_SHIFT1, BTN_ALT2, OnKey)
	ON_CONTROL_RANGE (BN_CLICKED, BTN_KEYPAD_NUMLOCK, BTN_ARROW_RIGHT, OnNumpad)

	ON_MESSAGE(NM_SYSTRAY, OnTrayNotify)

	ON_REGISTERED_MESSAGE (WM_SCREENSHOT, OnSreenshot)
	ON_REGISTERED_MESSAGE (WM_ISKEYBOARD, OnIsKeyboard)
	ON_REGISTERED_MESSAGE (WM_ISKEYBOARDRUNNING, OnIsAppRunning)
	ON_REGISTERED_MESSAGE (WM_ISKEYBOARDVISIBLE, OnIsKeyboardVisible)
	ON_REGISTERED_MESSAGE (WM_RIGHT_CLICK_DETECTED, OnRightClick)
	ON_REGISTERED_MESSAGE (WM_CLICK_DETECTED, OnClick)
	ON_REGISTERED_MESSAGE (WM_FOCUS_CHANGED, OnFocusChanged)
	ON_REGISTERED_MESSAGE (WM_SHUTDOWNKEYBOARD, OnShutdown)
	ON_REGISTERED_MESSAGE (WM_SHUTDOWNAPP, OnShutdown)
	ON_REGISTERED_MESSAGE (WM_KBWINCREATED, OnKbWinCreated)
	ON_REGISTERED_MESSAGE (WM_SETCHECK, OnSetCheck)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKeyboardDlg message handlers

void CKeyboardDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	if (bShow) {
		/*
		CRect rc;

		GetWindowRect (&rc);
		SetWindowPos (NULL, 0, 0, rc.Width (), rc.Height (), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
		*/
	}
}

int CKeyboardDlg::GetRowCount (const UINT * p)
{
	int i;

	for (i = 0; p [i] != -1; i++)
		;

	return i;
}

BOOL CKeyboardDlg::OnInitDialog() 
{
	{
		FoxjetUtils::tButtonStyle tStyle;

		m_styleDefault.GetButtonStyle (&tStyle);

		tStyle.m_dRadius = 5.0;
		tStyle.m_tColorFace.m_tEnabled		= CFontDlg::GetButtonColor ();
		tStyle.m_tColorBorder.m_tEnabled	= RGB(0x40, 0x40, 0xFF);

		tStyle.m_tColorFace.m_tClicked		= RGB(0x80, 0xFF, 0x80);
		tStyle.m_tColorBorder.m_tClicked	= RGB(0x40, 0xFF, 0x40);

		tStyle.m_tColorFace.m_tPressed		= RGB(0xFF, 0x80, 0x80);
		tStyle.m_tColorBorder.m_tPressed	= RGB(0xFF, 0x40, 0x40);

		tStyle.m_tColorFace.m_tDisabled		= RGB(0x60, 0x60, 0x60);
		//tStyle.m_tColorBorder.m_tDisabled	= RGB(? ? ?);
		
		tStyle.m_tColorBack.m_tEnabled		= 
		tStyle.m_tColorBack.m_tClicked		= 
		tStyle.m_tColorBack.m_tPressed		= 
		tStyle.m_tColorBack.m_tDisabled		= 
		tStyle.m_tColorBack.m_tHot			= ::GetSysColor (COLOR_BTNFACE);

		m_styleDefault.SetButtonStyle (&tStyle);
	}

	/* TODO: rem
	{
		DWORD dw;

		m_hExit = CreateEvent (NULL, true, false, _T ("Keyboard::Exit"));
		VERIFY (m_hThread = ::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0, (LPTHREAD_START_ROUTINE)ThreadFunc, (LPVOID)this, 0, &dw));
	}
	*/

	StartKbMonitor ();

	if (IsMatrix ()) 
		CreateShortcut ();

	CString strLangExt = GetLangExt ();

	TRACER (strLangExt);

	if (strLangExt == _T ("sp"))
		strLangExt = _T ("es");
	else if (strLangExt == _T ("po"))
		strLangExt = _T ("pt");

	TRACER (strLangExt);

	ReadConfig (GetHomeDir () + _T ("\\Keyboard_") + strLangExt + _T (".txt"));
	CheckIDs ();
	CheckUserKeys ();
	CheckUserKeys ();

	CDialog::OnInitDialog();
	OnChangeColors ();

	{
		struct 
		{
			FoxjetUtils::CRoundButton2 * m_pKey;
			bool m_bCheck;
		}
		static const map [] =
		{
			{ &m_btnCaps,				true, },
			{ &m_btnShift1,				true, },
			{ &m_btnShift2,				true, },
			{ &m_btnCtrl1,				true, },
			{ &m_btnCtrl2,				true, },
			{ &m_btnAlt1,				true, },
			{ &m_btnAlt2,				true, },
			{ &m_btnKeypadNumlock,		true, },
			{ &m_btnKeypadScrollLock,	true, },
			{ &m_btnMouse,				false, },
			{ &m_btnCharMap,			false, },
			{ &m_btnExpand,				false, },
			{ &m_btnEsc,				false, },
		};
		COLORREF rgb = CFontDlg::GetFontColor ();
		tColorScheme tColor = { rgb, rgb, rgb, rgb, rgb };

		for (int i = 0; i < ARRAYSIZE (map); i++) {
			map [i].m_pKey->SetRoundButtonStyle (&m_styleDefault);
			map [i].m_pKey->SetFont (m_pfntKey);
			map [i].m_pKey->SetCheckButton (map [i].m_bCheck);
			map [i].m_pKey->SetTextColor (&tColor);

			if (CKey * pKey = dynamic_cast <CKey *> (map [i].m_pKey))
				pKey->SetParent (this);
		}
	}

	m_btnMouse.SetWindowText (_T (""));
	m_btnCharMap.SetWindowText (_T (""));
	m_btnExpand.SetWindowText (_T (""));

	HINSTANCE hInstance = AfxGetInstanceHandle ();
	
	VERIFY (m_hAccel = ::LoadAccelerators (hInstance, MAKEINTRESOURCE (IDA_KEYBOARD)));

	{
		OSVERSIONINFO os;

		memset (&os, 0, sizeof (os));
		os.dwOSVersionInfoSize = sizeof (os);
		
		if (::GetVersionEx (&os)) {
			CString str;
	
			str.Format (_T ("%d.%d.%d.%d %s"), 
				os.dwMajorVersion,
				os.dwMinorVersion,
				os.dwBuildNumber,
				os.dwPlatformId,
				os.szCSDVersion);
			TRACEF (str);

			m_bXP = os.dwMajorVersion <= 5 ? true : false;
		}
	}

	if (CMenu * pMenu = GetSystemMenu (FALSE)) {
		pMenu->AppendMenu (MF_SEPARATOR);
		pMenu->AppendMenu (MF_STRING, IDM_DOCK, LoadString (IDS_DOCK));
		pMenu->AppendMenu (MF_STRING, IDM_NUMPAD, LoadString (IDS_SHOWNUMPAD));
		pMenu->AppendMenu (MF_STRING, IDM_FONT, LoadString (IDS_SETFONT));
		pMenu->AppendMenu (MF_SEPARATOR);
		pMenu->AppendMenu (MF_STRING, IDA_F1, LoadString (IDS_ABOUTCONTEXT));
	}

	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_MOUSE)) {
		p->SetBitmap (m_bmpMouseLeft);
		p->EnableWindow (FALSE);
	}

	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_CHARMAP)) 
		p->SetBitmap (m_bmpLanguage);

	if (HMODULE hDLL = theApp.m_hHookLib = ::LoadLibrary (HOOKDLL)) {
		if (LPHOOKPROC lpHook = (LPHOOKPROC)::GetProcAddress (hDLL, UTILSPROC_HOOK)) {
			int nResult = (* lpHook) (m_hWnd, WM_CLICK_DETECTED, WM_RIGHT_CLICK_DETECTED, WM_FOCUS_CHANGED);

			#ifdef _DEBUG
			CString str;
			str.Format (_T ("Hook (%d): m_hWnd: 0x%p, WM_CLICK_DETECTED: %d, WM_RIGHT_CLICK_DETECTED: %d, WM_FOCUS_CHANGED: %d"), 
				nResult,
				m_hWnd, WM_CLICK_DETECTED, WM_RIGHT_CLICK_DETECTED, WM_FOCUS_CHANGED);
			TRACEF (str);
			#endif

			if (!nResult)
				TRACEF (FORMATMESSAGE (::GetLastError ()));
			else {
				if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_MOUSE)) 
					p->EnableWindow (TRUE);
			}
		}
	}

	OnShift ();
	m_hide.m_bDocked = m_bDock = theApp.GetProfileInt (_T ("CKeyboardDlg"), _T ("dock"), true) ? true : false;
	m_bNumpad = theApp.GetProfileInt (_T ("CKeyboardDlg"), _T ("numpad"), false) ? true : false;
	//::SetWindowPos (m_hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);


	if (!m_bDock)
		RestoreState ();
	else {
		CString strKey = _T ("CKeyboardDlg\\WindowState");
		int nExpanded = theApp.GetProfileInt (strKey, _T ("expanded"), 0);
		int l = theApp.GetProfileInt (strKey, _T ("left"), -1);
		int t = theApp.GetProfileInt (strKey, _T ("top"), -1);
		int r = theApp.GetProfileInt (strKey, _T ("right"), -1);
		int b = theApp.GetProfileInt (strKey, nExpanded == 1 ? _T ("bottom ex") : _T ("bottom"), -1);

		//if (b != -1)
		//	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_EXPAND)) 
		//		p->SetCheck (theApp.GetProfileInt (strKey, _T ("expanded"), 1));

		//{ CString str; str.Format (_T ("OnInitDialog: %d, %d (%d, %d)"), l, t, r, b); TRACEF (str); }

		Dock ();

		if (l == -1 || r == -1 || t == -1 || b == -1) 
			SaveState ();

		if (b != -1 && t != -1) {
			CRect rc (0, 0, 0, 0);

			GetWindowRect (rc);
			//::SetWindowPos (m_hWnd, HWND_TOPMOST, 0, 0, rc.Width (), b, SWP_NOMOVE | SWP_NOACTIVATE);
			//{ CString str; str.Format (_T ("SetWindowPos: 0, 0, (%d, %d)"), rc.Width (), b); TRACEF (str); }
		}
	}

	SetTimer (IDT_ACTIVEWINDOW, 500, NULL);
	//SetTimer (IDT_CHECKSTARTMENU, 5000, NULL);
	
	OnNumlock ();
	OnAlt ();
	//OnExpand ();
	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_EXPAND)) 
		p->SetBitmap (m_bmpCollapse);

	memset (&m_nid, 0, sizeof (m_nid));
	m_nid.cbSize = sizeof (m_nid);
	m_nid.hWnd = m_hWnd;
	m_nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	m_nid.hIcon = m_hIcon;
	m_nid.uCallbackMessage = NM_SYSTRAY;
	_tcsncpy (m_nid.szTip, LoadString (IDS_KEYBOARD), ARRAYSIZE (m_nid.szTip));

	::Shell_NotifyIcon (NIM_ADD, &m_nid);

	SetWindowLong (m_hWnd, GWL_STYLE, WS_SIZEBOX);
	::SetWindowPos (m_hWnd, 0, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);

	if (IsMatrix ()) {
		m_hide.m_bHidden = true;

		for (HWND hwndChild = ::GetWindow (m_hWnd, GW_CHILD); hwndChild != NULL; hwndChild = ::GetWindow (hwndChild, GW_HWNDNEXT)) 
			::ShowWindow (hwndChild, SW_HIDE);

		//SetTimer (IDT_STARTUP, 5, NULL);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

static BOOL CALLBACK EnumChildWindowsProc (HWND hWnd, LPARAM lParam)
{
	CArray <HWND, HWND> & v = * (CArray <HWND, HWND> *)lParam;

	v.Add (hWnd);

	return TRUE;
}

void CKeyboardDlg::OnSize(UINT nType, int cx, int cy) 
{
	using namespace FoxjetDatabase;

	UINT nLocalButtons [::nRows][::nButtonColumns] = { 0 };
	UINT nLocalKeypad [::nRows][::nKeypadColumns] = { 0 };

	memcpy (nLocalButtons, ::nButtons, sizeof (UINT) * ::nRows * ::nButtonColumns);
	memcpy (nLocalKeypad, ::nKeypad, sizeof (UINT) * ::nRows * ::nKeypadColumns);

	if (!m_bNumpad) {
		swap (nLocalButtons [0][6], nLocalKeypad [5][0]);		// BTN_7_1	-->	BTN_ARROW_LEFT
		swap (nLocalButtons [0][7], nLocalKeypad [5][2]);		// BTN_8_1	-->	BTN_ARROW_RIGHT
		swap (nLocalButtons [0][8], nLocalKeypad [4][1]);		// BTN_9_1	-->	BTN_ARROW_UP
		swap (nLocalButtons [0][9], nLocalKeypad [5][1]);		// BTN_10_1	-->	BTN_ARROW_DOWN
		swap (nLocalButtons [0][10], nLocalKeypad [1][2]);		// BTN_11_1	-->	BTN_KEYPAD_PAGEUP
		swap (nLocalButtons [0][11], nLocalKeypad [2][2]);		// BTN_12_1	-->	BTN_KEYPAD_PAGEDOWN
		swap (nLocalButtons [0][12], nLocalKeypad [2][0]);		// BTN_13_1	-->	BTN_KEYPAD_DEL
	}
 
	CDialog::OnSize(nType, cx, cy);

	if (IsWindowVisible ()) {
		CSize sizeKeypad (10, 10);
		CRect rcClient;
		const int nBorder = 0;
		const double dButtonGap = 0.0;
		int cyMain = cy;
		const double dBottomPaneRatio = 1.0;//0.70;

		if (IsMatrix ()) 
			cyMain = (int)((double)cyMain * dBottomPaneRatio);

		int nHeight = Round (((double)(cyMain - (nBorder * 2)) / ARRAYSIZE (nLocalButtons)) - 0);
		
		GetClientRect (&rcClient);
		rcClient.right -= 10;

		if (IsMatrix ()) {
			//m_wndPreview.SetWindowPos (NULL, 0, 0, cx, (int)((double)cy * (1.0 - dBottomPaneRatio)) - nBorder - 1, SWP_NOZORDER | SWP_NOACTIVATE);
			//m_wndPreview.ShowWindow (IsMatrix () ? SW_SHOW : SW_HIDE);
		}

		{
			//int cxMain = (int)((double)cx * (m_bNumpad ? 0.66 : 1.0));
			int cxMain = (int)((double)rcClient.Width () * (m_bNumpad ? 0.66 : 1.0));

			TRACEF (ToString (cx) + _T (": ") + ToString (rcClient.Width ()));

			for (int y = 0; y < ARRAYSIZE (nLocalButtons); y++) {
				int n = GetRowCount (nLocalButtons [y]);
				double dWidth = (((double)cxMain - ((double)nBorder * 2.0)) / (double)n) - dButtonGap;

				for (int x = 0; x < n; x++) {
					if (CWnd * p = GetDlgItem (nLocalButtons [y][x])) {
						int nX	= Round ((double)nBorder + ((dWidth + dButtonGap) * (double)x));
						int nY	= Round ((double)nBorder + (nHeight * y) + dButtonGap);
						int nCX	= Round (dWidth);
						int nCY	= Round ((double)nHeight - dButtonGap);

						if (IsMatrix ()) 
							nY += (int)((double)cy * (1.0 - dBottomPaneRatio));

						p->SetFont (m_pfntKey);
						p->SetWindowPos (NULL, nX, nY, nCX, nCY, SWP_NOZORDER | SWP_NOACTIVATE);
						p->ShowWindow (SW_SHOW);

						if (nLocalButtons [y][x] == BTN_1_1)
							sizeKeypad = CSize (nCX, nCY);
					}
				}
			}

			{ // special case for space bar
				double dOther = Round ((int)cxMain / 8.0);
				double dSpaceBar = cxMain - (dOther * (double)(GetRowCount (nLocalButtons [5]) - 1));
				double d [6] = { dOther, dOther, dSpaceBar, dOther, dOther, dOther, };
				double dTemp = 0;

				for (int i = 0; i < ARRAYSIZE (d); i++) {
					if (CWnd * p = GetDlgItem (nLocalButtons [5][i])) {
						CRect rc (0, 0, 0, 0);

						p->GetWindowRect (&rc);
						ScreenToClient (&rc);
						rc.left = Round (dTemp);
						rc.right = Round ((double)rc.left + d [i] - dButtonGap);
						p->SetWindowPos (NULL, rc.left, rc.top, rc.Width (), rc.Height (), SWP_NOZORDER | SWP_NOACTIVATE);
					}

					dTemp += d [i];
				}
			}
		}

		{ // keypad
			sizeKeypad.cx = Round ((double)sizeKeypad.cx * 0.88);

			int nSpace = Round ((double)sizeKeypad.cx * 0.20);

			for (int y = 0; y < ARRAYSIZE (nLocalKeypad); y++) {
				int n = GetRowCount (nLocalKeypad [y]);
				int nPos = Round ((double)rcClient.Width () * 0.66) + (nSpace * 2);
				int nTop = 0;

				if (CWnd * p = GetDlgItem (nLocalButtons [y][0])) {
					CRect rc (0, 0, 0, 0);

					p->GetWindowRect (&rc);
					ScreenToClient (&rc);
					nTop = rc.top;
				}

				for (int x = 0; x < n; x++) {
					UINT nID = nLocalKeypad [y][x];

					if (nID == 0) 
						nPos += nSpace;
					else if (nID == -2)
						nPos += sizeKeypad.cx;
					else if (CWnd * p = GetDlgItem (nID)) {
						CRect rc (0, 0, 0, 0);

						p->ShowWindow (m_bNumpad ? SW_SHOW : SW_HIDE);
						p->GetWindowRect (&rc);
						ScreenToClient (&rc);

						rc = CRect (CPoint (nPos, nTop), sizeKeypad);

						switch (nID) {
						case BTN_KEYPAD_PLUS:
						case BTN_KEYPAD_ENTER:
							rc.InflateRect (0, 0, 0, sizeKeypad.cy);
							break;
						case BTN_KEYPAD_0:
							rc.InflateRect (0, 0, sizeKeypad.cx, 0);
							break;
						}

						p->SetWindowPos (NULL, Round ((double)rc.left + (dButtonGap * x)), rc.top, rc.Width (), rc.Height (), SWP_NOZORDER | SWP_NOACTIVATE);
						//{ CString str, strLabel; p->GetWindowText (strLabel); str.Format (_T ("[%d, %d] (%d: %s): %d, %d (%d, %d)"), x, y, nID, strLabel, rc.left, rc.top, rc.Width (), rc.Height ()); TRACEF (str); }
						nPos += sizeKeypad.cx;
					}
				}
			}
		}
	}

	{
		UINT map [] = 
		{
			BTN_KEYPAD_SYSREQ,		
			BTN_KEYPAD_SCROLLLOCK,	
			BTN_KEYPAD_BREAK,		
			BTN_KEYPAD_PAGEUP,		
			BTN_KEYPAD_NUMLOCK,		
			BTN_KEYPAD_PAGEDOWN,	
			//BTN_KEYPAD_ENTER,		
			BTN_KEYPAD_BACK,		
			BTN_KEYPAD_INSERT,		
			BTN_KEYPAD_HOME,		
			BTN_KEYPAD_DEL,			
			BTN_KEYPAD_END,			
		};

		for (int i = 0; i < ARRAYSIZE (map); i++) 
			if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (map [i])) 
				p->SetFont (m_pfntNumpad);
	}
	{
		UINT map [] = 
		{
			BTN_KEYPAD_EQUAL,
			BTN_KEYPAD_LPAREN,
			BTN_KEYPAD_RPAREN,
			BTN_KEYPAD_BACK,
			BTN_KEYPAD_MINUS,
			BTN_KEYPAD_PLUS,
			BTN_KEYPAD_ENTER,
			BTN_KEYPAD_DIV,
			BTN_KEYPAD_MULT,
		};

		for (int i = 0; i < ARRAYSIZE (map); i++) 
			if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (map [i])) 
				p->SetFont (m_pfntKey);
	}

	/*
	{
		struct
		{
			UINT m_nID;
			TCHAR m_c;
		}
		static const arrows [] = 
		{
			{ BTN_ARROW_UP,		0xE1, },
			{ BTN_ARROW_DOWN,	0xE2, },
			{ BTN_ARROW_RIGHT,	0xE0, },
			{ BTN_ARROW_LEFT,	0xDF, },
		};

		for (int i = 0; i < ARRAYSIZE (arrows); i++) {
			if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (arrows [i].m_nID)) {
				p->SetFont (m_pfntArrow);
				TRACEF (CString (arrows [i].m_c));
				p->SetWindowText (CString (arrows [i].m_c));
			}
		}
	}
	*/

	if (!m_hide.m_bHidden && m_bDock)
		Dock ();
}

bool CKeyboardDlg::IsCheckbox (UINT nID)
{
	switch (nID) {
	case BTN_SHIFT1:
	case BTN_SHIFT2:
	case BTN_CTRL1: 
	case BTN_CTRL2: 
	case BTN_CAPS:  
	case BTN_ALT1:  
	case BTN_ALT2:  
	case BTN_KEYPAD_NUMLOCK:
	case BTN_MOUSE:
		return true;
	}

	return false;
}

bool CKeyboardDlg::IsAlt ()
{
	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_ALT1)) 
		if (p->GetCheck () == 1)
			return true;

	return false;
}

bool CKeyboardDlg::IsWinKey ()
{
	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_WINKEY)) 
		if (p->GetCheck () == 1)
			return true;

	return false;
}

bool CKeyboardDlg::IsCtrl ()
{
	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_CTRL1)) 
		if (p->GetCheck () == 1)
			return true;

	return false;
}

bool CKeyboardDlg::IsShift ()
{
	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_SHIFT1)) 
		if (p->GetCheck () == 1)
			return true;

	if (!m_bExtendedChars)
		if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_SHIFT2)) 
			if (p->GetCheck () == 1)
				return true;

	return false;
}

bool CKeyboardDlg::IsShift2 ()
{
	if (!m_bExtendedChars)
		return false;

	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_SHIFT2)) 
		if (p->GetCheck () == 1)
			return true;

	return false;
}

bool CKeyboardDlg::IsCaps ()
{
	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_CAPS)) 
		if (p->GetCheck () == 1)
			return true;

	return false;
}

void CKeyboardDlg::OnWinKey ()
{
//	OnKey (BTN_WINKEY);
}

void CKeyboardDlg::OnKey (UINT nID)
{
	bool bUncheck = false;

	if (IsCheckbox (nID)) { 
		bool bDown = false;

		if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (nID))
			bDown = p->GetCheck () == 1;

		if (m_bExtendedChars) {
			switch (nID) {
			case BTN_SHIFT1:
				if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_SHIFT1))
					p->SetCheck (bDown ? 1 : 0);
				OnShift ();
				break;
			case BTN_SHIFT2:
				if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_SHIFT2))
					p->SetCheck (bDown ? 1 : 0);
				OnShift ();
				break;
			}
		}
		else {
			switch (nID) {
			case BTN_SHIFT1:
			case BTN_SHIFT2:
				if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_SHIFT1))
					p->SetCheck (bDown ? 1 : 0);
				if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_SHIFT2))
					p->SetCheck (bDown ? 1 : 0);
				OnShift ();
				break;
			}
		}

		switch (nID) {
		case BTN_MOUSE:
			if (theApp.m_hHookLib) {
				if (LPSWAPPROC lpSwap = (LPSWAPPROC)::GetProcAddress (theApp.m_hHookLib, UTILSPROC_SWAP)) {
					if ((* lpSwap) ()) {
						if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_MOUSE)) 
							p->SetBitmap (m_bmpMouseRight);
						if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_CHARMAP)) 
							p->SetBitmap (m_bmpCharMap);
						if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_5_4)) {
							p->SetWindowText (_T (""));
							p->SetBitmap (m_bmpFont);
						}
						if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_7_4)) {
							p->SetWindowText (_T (""));
							p->SetBitmap (m_bmpHelp);
						}
						if (theApp.IsEditMode ()) {
							if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_9_4)) {
								p->SetWindowText (_T (""));
								p->SetBitmap (m_bmpKeyboard);
							}
						}
					}
				}
			}
			return;
		case BTN_CTRL1: 
		case BTN_CTRL2: 
			if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_CTRL1))
				p->SetCheck (bDown ? 1 : 0);
			//if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_CTRL2))
			//	p->SetCheck (bDown ? 1 : 0);
			break;
		case BTN_ALT1:  
		case BTN_ALT2:  
			if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_ALT1))
				p->SetCheck (bDown ? 1 : 0);
			//if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_ALT2))
			//	p->SetCheck (bDown ? 1 : 0);
			OnAlt ();
			break;
		case BTN_CAPS:
			OnShift ();
			SendKey (VK_CAPITAL, true);
			SendKey (VK_CAPITAL, false);
			break;
		case BTN_KEYPAD_NUMLOCK:
			OnNumlock ();
			SendKey (VK_NUMLOCK, true);
			SendKey (VK_NUMLOCK, false);
			return;
			break;
		}

		return;
	}

	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_MOUSE)) 
		if (p->GetCheck () == 1)
			return;

	/*
	#ifdef _DEBUG
	{
		CString str;
		TCHAR szTarget [256] = { 0 };
		TCHAR szForeground [256] = { 0 };

		::GetWindowText (m_target.GetTarget (), szTarget, ARRAYSIZE (szTarget));
		::GetClassName (::GetForegroundWindow (), szForeground, ARRAYSIZE (szForeground) - 1);

		str.Format (_T ("target: %s [0x%p], foreground: %s [0x%p]"), szTarget, m_target.GetTarget (), szForeground, ::GetForegroundWindow ());
		TRACEF (str);
	}
	#endif
	*/

	if (m_target.GetTarget ()) {
		TCHAR c = 0;
		ULONG lParam = 0;
		bool bShift = IsShift ();
		bool bAlt = IsAlt ();
		bool bCtrl = IsCtrl ();
		bool bWinKey = (nID != BTN_WINKEY) && IsWinKey ();
		WORD nKey = 0, nVK = 0;

		HWND h = ::GetLastActivePopup (m_target.GetTarget ());
		//TCHAR sz [512] = { 0 }, szTarget [512] = { 0 };
		//::GetWindowText (h, sz, ARRAYSIZE (sz));
		//CString str; str.Format (_T ("h: 0x%p [%s], m_target.GetTarget (): 0x%p [%s]"), h, sz, m_target.GetTarget (), szTarget); TRACEF (str);
		m_target.SetTarget (h);

		for (int i = 0; i < ARRAYSIZE (m_map); i++) {
			if (nID == m_map [i].m_nID) {
				nKey = !bShift ? m_map [i].m_c : m_map [i].m_cShift;

				if (m_map [i].m_nVK) 
					nVK = m_map [i].m_nVK;

				break;
			}
		}

		//{ CString str; str.Format (_T ("nID: %d, nKey: %d %s"), nID, nKey, GetVkString (nVK)); TRACEF (str); 

		if (nKey || bCtrl || bAlt) {
			/*
			TCHAR c = (TCHAR)nKey;

			if (_istalpha (nKey)) {
				//CString str = CString ((TCHAR)nKey) + _T (": ");
				//nKey = toupper (nKey) - 'A' + 1;
				//str += CString ((TCHAR)nKey) + _T (" ");
				nKey = LOBYTE (::VkKeyScan (nKey));
				//str += CString ((TCHAR)nKey) + _T (" ");
				//TRACEF (str);
			}
			*/

			const bool bVK = (nVK >= 0x01 && nVK <= 0xFE);
			const bool bNumlock = ::GetKeyState (VK_NUMLOCK) > 0 ? true : false;
			INPUT key [2];

			ZeroMemory (key, sizeof (key));

			if (bNumlock) // numlock interferes with VK_SHIFT
				OnKey (BTN_KEYPAD_NUMLOCK);

			if (!bCtrl && !bAlt && !bWinKey && !bVK) {
				CString strKey = CString ((TCHAR)nKey);

				if (nKey != ' ')
					if (CWnd * p = GetDlgItem (nID)) 
						p->GetWindowText (strKey);

				#ifdef _DEBUG
				for (int i = 0; i < strKey.GetLength (); i++) {
					CString strDebug; 
					
					strDebug.Format (_T ("0x%04X"), strKey [i]); 
					TRACEF (strDebug); 
				}
				#endif _DEBUG

				if (bCtrl)		SendKey (VK_CONTROL,	true);
				if (bAlt)		SendKey (VK_MENU,		true);
				if (bWinKey)	SendKey (VK_LWIN,		true);

				for (int i = 0; i < strKey.GetLength (); i++) {
					ZeroMemory (key, sizeof (key));
					key [0].type		= INPUT_KEYBOARD;
					key [0].ki.dwFlags	= KEYEVENTF_UNICODE;
					key [1]				= key [0];
					key [1].ki.dwFlags |= KEYEVENTF_KEYUP;
					key [0].ki.wScan	= key [1].ki.wScan = strKey [i];

					::SendInput (ARRAYSIZE (key), key, sizeof (INPUT));
				}

				if (bCtrl)		SendKey (VK_CONTROL,	false);
				if (bAlt)		SendKey (VK_MENU,		false);
				if (bWinKey)	SendKey (VK_LWIN,		false);

				CheckInputClass (m_hwndFocus);
//				::PostMessage (m_wndPreview.m_hWnd, WM_SENTINPUT, 0, 0);
			}
			else {
				WORD w = ::VkKeyScan (nKey);

				//{ CString str; str.Format (_T ("nID: %d, nKey: %d (%c), w: %d, nVK: %d, bVK: %d"), nID, nKey, nKey, w, nVK, bVK); TRACEF (str); }

				key [1].ki.dwFlags	= KEYEVENTF_KEYUP;

				for (int i = 0; i < 2; i++) {
					key [i].type		= INPUT_KEYBOARD;
					key [i].ki.wVk		= bVK ? nVK : w;
					key [i].ki.wScan	= bVK ? 0	: w;

					if (c == 'u' || c == 'U') { // no explanation for this, but 'U' won't work without it
						key [i].ki.wVk		= w;
						key [i].ki.wScan	= 0;
					}
				}

				if (bShift)		SendKey (VK_SHIFT,		true);
				if (bCtrl)		SendKey (VK_CONTROL,	true);
				if (bAlt)		SendKey (VK_MENU,		true);
				if (bWinKey)	SendKey (VK_LWIN,		true);

				::SendInput (ARRAYSIZE (key), key, sizeof (INPUT));

				if (bShift)		SendKey (VK_SHIFT,		false);
				if (bCtrl)		SendKey (VK_CONTROL,	false);
				if (bAlt)		SendKey (VK_MENU,		false);
				if (bWinKey)	SendKey (VK_LWIN,		false);

				if (IsCtrl () && IsAlt () && nVK == VK_DELETE) {
					typedef DWORD (WINAPI* lpfnWmsgSendMessage)(DWORD dwSessionId, UINT uMsg, WPARAM wParam, LPARAM lParam);
					typedef BOOL (WINAPI* lpfnProcessIdToSessionId)(DWORD dwProcessId, DWORD *pSessionId);

					if (HINSTANCE hKernelLib = LoadLibrary (_T ("Kernel32.dll"))) {
						if (HINSTANCE hLib = LoadLibrary (_T ("wmsgapi.dll"))) {
							if (lpfnWmsgSendMessage lp = (lpfnWmsgSendMessage)::GetProcAddress (hLib, "WmsgSendMessage")) {
								if (lpfnProcessIdToSessionId lpProcID = (lpfnProcessIdToSessionId)::GetProcAddress (hKernelLib, "ProcessIdToSessionId")) {
									DWORD dw = 0;
									LPARAM lParam = 0;

									BOOL bResult = (* lpProcID) (::GetCurrentProcessId (), &dw);
									DWORD dwRet = (* lp) (dw, 0x208, 0, (LPARAM)&lParam);
								}		
							}
						}
					}
				}

				CheckInputClass (m_hwndFocus);
//				::PostMessage (m_wndPreview.m_hWnd, WM_SENTINPUT, 0, 0);
			}

			if (nID == BTN_4_4 && bWinKey) { // LWIN+D
				TRACEF ("LWIN+D");
			}//

			if (bNumlock) // restore numlock 
				OnKey (BTN_KEYPAD_NUMLOCK);

			if (nID == BTN_WINKEY) { 
				if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_WINKEY)) {
					//TRACEF (ToString (p->GetCheck ()));

					if (p->GetCheck () == 1) 
						return;
				}
			}

			m_bUncheck = true; //Uncheck ();
			return;
		}
	}

	if (bUncheck) 
		Uncheck ();

}

void CKeyboardDlg::OnNumpad (UINT nID)
{
	struct
	{
		UINT m_nID;
		UCHAR m_c;
		BYTE m_nVK;
		BYTE m_nVKLock;
	}
	static const keys [34] = 
	{
		{ BTN_KEYPAD_SYSREQ,		0,		VK_SNAPSHOT,	VK_SNAPSHOT,	},
		{ BTN_KEYPAD_SCROLLLOCK,	0,		VK_SCROLL,		VK_SCROLL,		},
		{ BTN_KEYPAD_BREAK,			0,		VK_CANCEL,		VK_CANCEL,		},
		{ BTN_KEYPAD_INSERT,		0,		VK_INSERT,		VK_INSERT,		},
		{ BTN_KEYPAD_HOME,			0,		VK_HOME,		VK_HOME,		},
		{ BTN_KEYPAD_PAGEUP,		0,		VK_PRIOR,		VK_PRIOR,		},
		{ BTN_KEYPAD_NUMLOCK,		0,		VK_NUMLOCK,		VK_NUMLOCK,		},
		{ BTN_KEYPAD_DIV,			0,		VK_DIVIDE,		VK_DIVIDE,		},
		{ BTN_KEYPAD_MULT,			0,		VK_MULTIPLY,	VK_MULTIPLY,	},
		{ BTN_KEYPAD_MINUS,			0,		VK_SUBTRACT,	VK_SUBTRACT,	},
		{ BTN_KEYPAD_DEL,			0,		VK_DELETE,		VK_DELETE,		},
		{ BTN_KEYPAD_END,			0,		VK_END,			VK_END,			},
		{ BTN_KEYPAD_PAGEDOWN,		0,		VK_NEXT,		VK_NEXT,		},
		{ BTN_KEYPAD_7,				0,		VK_NUMPAD7,		VK_HOME,		},
		{ BTN_KEYPAD_8,				0,		VK_NUMPAD8,		VK_UP,			},
		{ BTN_KEYPAD_9,				0,		VK_NUMPAD9,		VK_PRIOR,		},
		{ BTN_KEYPAD_PLUS,			0,		VK_ADD,			VK_ADD,			},
		{ BTN_KEYPAD_4,				0,		VK_NUMPAD4,		VK_LEFT,		},
		{ BTN_KEYPAD_5,				0,		VK_NUMPAD5,		0,				},
		{ BTN_KEYPAD_6,				0,		VK_NUMPAD6,		VK_RIGHT,		},
		{ BTN_ARROW_UP,				0,		VK_UP,			VK_UP,			},
		{ BTN_KEYPAD_1,				0,		VK_NUMPAD1,		VK_END,			},
		{ BTN_KEYPAD_2,				0,		VK_NUMPAD2,		VK_DOWN,		},
		{ BTN_KEYPAD_3,				0,		VK_NUMPAD3,		VK_NEXT,		},
		{ BTN_KEYPAD_ENTER,			0,		VK_RETURN,		VK_RETURN		},
		{ BTN_ARROW_LEFT,			0,		VK_LEFT,		VK_LEFT,		},
		{ BTN_ARROW_DOWN,			0,		VK_DOWN,		VK_DOWN,		},
		{ BTN_ARROW_RIGHT,			0,		VK_RIGHT,		VK_RIGHT,		},
		{ BTN_KEYPAD_0,				0,		VK_NUMPAD0,		VK_INSERT,		},
		{ BTN_KEYPAD_PERIOD,		0,		VK_DECIMAL,		VK_DELETE,		},
		{ BTN_KEYPAD_EQUAL,			'=',	0,				0,				},
		{ BTN_KEYPAD_LPAREN,		'(',	0,				0,				},
		{ BTN_KEYPAD_RPAREN,		')',	0,				0,				},
		{ BTN_KEYPAD_BACK,			0,		VK_BACK,		VK_BACK,		},
	};

	const bool bNumlock = ::GetKeyState (VK_NUMLOCK) > 0 ? true : false;

	if (!bNumlock && nID == BTN_KEYPAD_5)
		return;

	for (int i = 0; i < ARRAYSIZE (keys); i++) {
		if (nID == keys [i].m_nID) {
			WORD w = bNumlock ? keys [i].m_nVK : keys [i].m_nVKLock;

			if (!w) 
				w = ::VkKeyScan (keys [i].m_c);

			if (w) {
				bool bShift = IsShift () || nID == BTN_KEYPAD_LPAREN || nID == BTN_KEYPAD_RPAREN;
				bool bAlt = IsAlt ();
				bool bCtrl = IsCtrl ();
				bool bWinKey = (nID != BTN_WINKEY) && IsWinKey ();

				m_target.SetTarget (::GetLastActivePopup (m_target.GetTarget ()));

				const int n = 1 + (bNumlock ? 1 : 0) + (bShift ? 1 : 0) + (bAlt ? 1 : 0) + (bCtrl ? 1 : 0) + (bWinKey ? 1 : 0);
				int nIndex = 0;
				INPUT * key = new INPUT [n];

				memset (key, 0, sizeof (INPUT) * n);

				for (int i = 0; i < n; i++)
					key [i].type = INPUT_KEYBOARD;

				if (bNumlock) // numlock interferes with VK_SHIFT
					OnKey (BTN_KEYPAD_NUMLOCK);

				if (bShift) 
					key [nIndex].ki.wScan	= key [nIndex].ki.wScan	= key [nIndex].ki.wVk		= key [nIndex++].ki.wVk	= VK_SHIFT;
				if (bCtrl) 
					key [nIndex].ki.wScan	= key [nIndex].ki.wScan	= key [nIndex].ki.wVk		= key [nIndex++].ki.wVk	= VK_CONTROL;
				if (bAlt) 
					key [nIndex].ki.wScan	= key [nIndex].ki.wScan	= key [nIndex].ki.wVk		= key [nIndex++].ki.wVk	= VK_MENU;
				if (bWinKey) 
					key [nIndex].ki.wScan	= key [nIndex].ki.wScan	= key [nIndex].ki.wVk		= key [nIndex++].ki.wVk	= VK_LWIN;

				key [nIndex].ki.wScan		= key [nIndex].ki.wScan	= key [nIndex].ki.wVk		= key [nIndex].ki.wVk	= w;

				ASSERT (nIndex < n);

				::SendInput (n, key, sizeof (INPUT));

				for (i = 0; i < n; i++)
					key [i].ki.dwFlags |= KEYEVENTF_KEYUP;

				::SendInput (n, key, sizeof (INPUT));

				if (bNumlock) // restore numlock state
					OnKey (BTN_KEYPAD_NUMLOCK);

				delete [] key;

				if (bCtrl && bAlt && (w == VK_DELETE)) {
					bool bUncheck = false;

					//::PostMessage (HWND_BROADCAST, WM_HOTKEY, 0, MAKELONG (MOD_ALT | MOD_CONTROL, VK_DELETE));

					if (HINSTANCE hLib = LoadLibrary (_T ("sas.dll"))) {
						typedef VOID (WINAPI *lpfnSendSas)(BOOL asUser);

						if (lpfnSendSas lp = (lpfnSendSas)GetProcAddress(hLib, "SendSAS")) {
							(* lp) (FALSE);
							bUncheck = true;
						}
					}

					if (HINSTANCE hKernelLib = LoadLibrary (_T ("Kernel32.dll"))) {
						if (HINSTANCE hLib = LoadLibrary (_T ("wmsgapi.dll"))) {
							typedef DWORD (WINAPI* lpfnWmsgSendMessage)(DWORD dwSessionId, UINT uMsg, WPARAM wParam, LPARAM lParam);
							typedef BOOL (WINAPI* lpfnProcessIdToSessionId)(DWORD dwProcessId, DWORD *pSessionId);

							if (lpfnWmsgSendMessage lp = (lpfnWmsgSendMessage)::GetProcAddress (hLib, "WmsgSendMessage")) {
								if (lpfnProcessIdToSessionId lpProcID = (lpfnProcessIdToSessionId)::GetProcAddress (hKernelLib, "ProcessIdToSessionId")) {
									DWORD dw = 0;
									LPARAM lParam = 0;

									BOOL bResult = (* lpProcID) (::GetCurrentProcessId (), &dw);
									DWORD dwRet = (* lp) (dw, 0x208, 0, (LPARAM)&lParam);
									
									#ifdef _DEBUG
									{ CString str; str.Format (_T ("WmsgSendMessage (%d ... %d): %d"), dw, lParam, dwRet); TRACEF (str); }
									#endif

									bUncheck = true;
								}		
							}
						}
					}

					if (bUncheck) 
						Uncheck ();
				}

				CheckInputClass (m_hwndFocus);
//				::PostMessage (m_wndPreview.m_hWnd, WM_SENTINPUT, 0, 0);
			}

			break;
		}
	}
}

void CKeyboardDlg::SendKey (int nVK, bool bDown)
{
	INPUT key;

	memset (&key, 0, sizeof (INPUT));

	key.ki.dwFlags	= bDown ? 0 : KEYEVENTF_KEYUP;

	key.type		= INPUT_KEYBOARD;
	key.ki.wVk		= nVK;
	key.ki.wScan	= nVK;

	if (!m_bXP) SETFOREGROUNDWINDOW (m_target.GetTarget ());

	::SendInput (1, &key, sizeof (INPUT));
	::Sleep (1);
//	::PostMessage (m_wndPreview.m_hWnd, WM_SENTINPUT, 0, 0);
}

void CKeyboardDlg::Uncheck () 
{
	UINT n [] = 
	{
		BTN_SHIFT1,
		BTN_SHIFT2,
		BTN_CTRL1, 
		BTN_CTRL2, 
		BTN_ALT1,  
		BTN_ALT2,
		BTN_WINKEY,
	};

	for (int i = 0; i < ARRAYSIZE (n); i++) {
		if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (n [i])) {
			if (p->GetCheck () == 1)
				p->SetCheck (0);
		}
	}

	OnShift ();
	m_bUncheck = false;
}

void CKeyboardDlg::OnClose() 
{
	if (HWND hNext = ::GetNextWindow (m_hWnd, GW_HWNDNEXT)) {
		TCHAR szClass [128] = { 0 };

		::GetClassName (hNext, szClass, ARRAYSIZE (szClass) - 1);
		TRACEF (CString (_T ("GetNextWindow: ")) + szClass);
		//::SetFocus (hNext);
	}
	if (HWND hNext = ::GetForegroundWindow ()) {
		TCHAR szClass [128] = { 0 };

		::GetClassName (hNext, szClass, ARRAYSIZE (szClass) - 1);
		TRACEF (CString (_T ("GetForegroundWindow: ")) + szClass);
	}

	OnHide ();
	m_hide.m_bUserOpened = false;
}

void CKeyboardDlg::OnShift ()
{
	const bool bCaps = IsCaps ();
	const bool bShift = IsShift ();
	const bool bShift2 = IsShift2 ();

	for (int i = 0; i < ARRAYSIZE (m_map); i++) {
		if (m_map [i].m_cShift) {
			if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (m_map [i].m_nID)) {
				if (p->GetBitmap () == NULL) {
					TCHAR c = (bShift | bCaps) ? m_map [i].m_cShift : m_map [i].m_c;

					if (bShift2 && c != '\t') {
						c = bShift ? m_map [i].m_cShiftAltU : m_map [i].m_cShiftAltL;

						if (!c) c = m_map [i].m_cShiftAltL;
						if (!c) c = m_map [i].m_cShiftAltU;
						//if (!c) c = (bShift2 | bCaps) ? m_map [i].m_cShift : m_map [i].m_c;;
					}
					else {
						if (bCaps && _istalpha (c))
							c = toupper (c);

						if (_istdigit (m_map [i].m_c) && /* bCaps && */ !bShift && !bShift2) 
							c = m_map [i].m_c;
					}

					CString str = CString (c);

					switch (c) {
					case '&':	
						str = _T ("&&");			
						break;
					case '\t':	
						str.LoadString (IDS_TAB);

						if ((bShift | bCaps))
							str = _T ("<- ") + str;

						break;
					}

					{
						CString strUserDefined = (bShift | bCaps) ? m_map [i].m_strShift : m_map [i].m_str;

						if (bShift2) {
							strUserDefined = bShift ? m_map [i].m_strShiftAltU : m_map [i].m_strShiftAltL;

							if (!strUserDefined.GetLength ()) strUserDefined = m_map [i].m_strShiftAltL; 
							if (!strUserDefined.GetLength ()) strUserDefined = m_map [i].m_strShiftAltU; 
							//if (!strUserDefined.GetLength ()) strUserDefined = (bShift2 | bCaps) ? m_map [i].m_strShift : m_map [i].m_str;
						}
						else
							if (_istdigit (m_map [i].m_c) /* && bCaps */ && !bShift && !bShift2) 
								strUserDefined = m_map [i].m_str;

						if (strUserDefined.GetLength ()) 
							str = strUserDefined;
					}

					p->SetWindowText (str);
				}
			}
		}
	}
}

void CKeyboardDlg::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	#ifdef _DEBUG
	CString str;
	str.Format (_T ("%c [%d]: nRepCnt: %d, nFlags: 0x%p"), nChar, nChar, nRepCnt, nFlags);
	TRACEF (str);
	#endif

	CDialog::OnChar(nChar, nRepCnt, nFlags);
}

#ifdef _DEBUG
static CString GetMsgString (UINT msg)
{
	switch (msg) {
	case WM_SYSCHAR:		return _T ("WM_SYSCHAR    ");
	case WM_CHAR:			return _T ("WM_CHAR       ");
	case WM_SYSDEADCHAR:	return _T ("WM_SYSDEADCHAR");
	case WM_SYSKEYDOWN:		return _T ("WM_SYSKEYDOWN ");
	case WM_KEYDOWN:		return _T ("WM_KEYDOWN    ");
	}

	return _T ("");
}
#endif

BOOL CKeyboardDlg::PreTranslateMessage(MSG* pMsg) 
{
	switch (pMsg->message) {
	case WM_SYSCHAR:
	case WM_CHAR:
	case WM_SYSDEADCHAR:
	case WM_SYSKEYDOWN:
	case WM_KEYDOWN:
		TCHAR c = pMsg->wParam;

/*
WM_KEYDOWN    : [ [91]: wParam: 0x0000005B, lParam: 0x015B0001

#define VK_LWIN           0x5B
#define VK_RWIN           0x5C
#define VK_APPS           0x5D
*/
		#ifdef _DEBUG
		CString str;
		str.Format (_T ("%s: %c [%d]: wParam: 0x%p, lParam: 0x%p"), GetMsgString (pMsg->message), c, c, pMsg->wParam, pMsg->lParam);
		//TRACEF (str);
		#endif
	}

	if (::TranslateAccelerator (m_hWnd, m_hAccel, pMsg))
		return TRUE;

	return CDialog::PreTranslateMessage(pMsg);
}

void CKeyboardDlg::OnCaps ()
{
	OnShift ();
}

void CKeyboardDlg::OnCancel()
{
	OnKey (27);
}

void CKeyboardDlg::SetTarget(CWnd* p, LPCTSTR lpszFile, ULONG lLine) 
{
	const CString strTitle = _T ("Virtual keyboard");

#ifdef _DEBUG
	struct
	{
		UINT m_dw;
		LPCTSTR m_lpsz;
	} style [] = 
	{
		{ WS_BORDER,				_T ("WS_BORDER"),				},
		{ WS_CAPTION,				_T ("WS_CAPTION"),				},
		{ WS_CHILD,					_T ("WS_CHILD"),				},
		{ WS_CHILDWINDOW,			_T ("WS_CHILDWINDOW"),			},
		{ WS_CLIPCHILDREN,			_T ("WS_CLIPCHILDREN"),			},
		{ WS_CLIPSIBLINGS,			_T ("WS_CLIPSIBLINGS"),			},
		{ WS_DISABLED,				_T ("WS_DISABLED"),				},
		{ WS_DLGFRAME,				_T ("WS_DLGFRAME"),				},
		{ WS_GROUP,					_T ("WS_GROUP"),				},
		{ WS_HSCROLL,				_T ("WS_HSCROLL"),				},
		{ WS_ICONIC,				_T ("WS_ICONIC"),				},
		{ WS_MAXIMIZE,				_T ("WS_MAXIMIZE"),				},
		{ WS_MAXIMIZEBOX,			_T ("WS_MAXIMIZEBOX"),			},
		{ WS_MINIMIZE,				_T ("WS_MINIMIZE"),				},
		{ WS_MINIMIZEBOX,			_T ("WS_MINIMIZEBOX"),			},
		{ WS_OVERLAPPED,			_T ("WS_OVERLAPPED"),			},
		{ WS_OVERLAPPEDWINDOW,		_T ("WS_OVERLAPPEDWINDOW"),		},
		{ WS_POPUP,					_T ("WS_POPUP"),				},
		{ WS_POPUPWINDOW,			_T ("WS_POPUPWINDOW"),			},
		{ WS_SIZEBOX,				_T ("WS_SIZEBOX"),				},
		{ WS_SYSMENU,				_T ("WS_SYSMENU"),				},
		{ WS_TABSTOP,				_T ("WS_TABSTOP"),				},
		{ WS_THICKFRAME,			_T ("WS_THICKFRAME"),			},
		{ WS_TILED,					_T ("WS_TILED"),				},
		{ WS_TILEDWINDOW,			_T ("WS_TILEDWINDOW"),			},
		{ WS_VISIBLE,				_T ("WS_VISIBLE"),				},
		{ WS_VSCROLL,				_T ("WS_VSCROLL"),				},
	};
	struct
	{
		UINT m_dw;
		LPCTSTR m_lpsz;
	} exstyle [] = 
	{
		{ WS_EX_ACCEPTFILES,		_T ("WS_EX_ACCEPTFILES"),		},	
		{ WS_EX_APPWINDOW,			_T ("WS_EX_APPWINDOW"),			},	
		{ WS_EX_CLIENTEDGE,			_T ("WS_EX_CLIENTEDGE"),		},		
		//{ WS_EX_COMPOSITED,			_T ("WS_EX_COMPOSITED"),		},
		{ WS_EX_CONTEXTHELP,		_T ("WS_EX_CONTEXTHELP"),		},	
		{ WS_EX_CONTROLPARENT,		_T ("WS_EX_CONTROLPARENT"),		},	
		{ WS_EX_DLGMODALFRAME,		_T ("WS_EX_DLGMODALFRAME"),		},	
		//{ WS_EX_LAYERED,			_T ("WS_EX_LAYERED"),			},	
		//{ WS_EX_LAYOUTRTL,			_T ("WS_EX_LAYOUTRTL"),			},	
		{ WS_EX_LEFT,				_T ("WS_EX_LEFT"),				},	
		{ WS_EX_LEFTSCROLLBAR,		_T ("WS_EX_LEFTSCROLLBAR"),		},	
		{ WS_EX_LTRREADING,			_T ("WS_EX_LTRREADING"),		},	
		{ WS_EX_MDICHILD,			_T ("WS_EX_MDICHILD"),			},	
		{ WS_EX_NOACTIVATE,			_T ("WS_EX_NOACTIVATE"),		},	
		//{ WS_EX_NOINHERITLAYOUT,	_T ("WS_EX_NOINHERITLAYOUT"),	},	
		{ WS_EX_NOPARENTNOTIFY,		_T ("WS_EX_NOPARENTNOTIFY"),	},	
		{ WS_EX_OVERLAPPEDWINDOW,	_T ("WS_EX_OVERLAPPEDWINDOW"),	},	
		{ WS_EX_PALETTEWINDOW,		_T ("WS_EX_PALETTEWINDOW"),		},	
		{ WS_EX_RIGHT,				_T ("WS_EX_RIGHT"),				},	
		{ WS_EX_RIGHTSCROLLBAR,		_T ("WS_EX_RIGHTSCROLLBAR"),	},	
		{ WS_EX_RTLREADING,			_T ("WS_EX_RTLREADING"),		},	
		{ WS_EX_STATICEDGE,			_T ("WS_EX_STATICEDGE"),		},	
		{ WS_EX_TOOLWINDOW,			_T ("WS_EX_TOOLWINDOW"),		},	
		{ WS_EX_TOPMOST,			_T ("WS_EX_TOPMOST"),			},
		{ WS_EX_TRANSPARENT,		_T ("WS_EX_TRANSPARENT"),		},			
		{ WS_EX_WINDOWEDGE,			_T ("WS_EX_WINDOWEDGE"),		},
	}; 
#endif

	if (p && (m_hWnd != m_target.GetTarget ())) {
		if (!IsChild (p)) {
			const DWORD dwStyle = p->GetStyle ();
			const DWORD dwExStyle = p->GetExStyle ();
			CString str;

			p->GetWindowText (str);

			if (str.Find (strTitle) == 0)
				return;

			/* 
			{
				CString strTmp;

				strTmp.Format (_T ("0x%p [0x%p]: %s "), dwStyle, dwExStyle, str);

				for (int i = 0; i < ARRAYSIZE (style); i++) 
					if (style [i].m_dw & dwStyle)
						strTmp += style [i].m_lpsz + CString (' ');

				strTmp += _T (" - ");

				for (i = 0; i < ARRAYSIZE (exstyle); i++) 
					if (style [i].m_dw & dwExStyle)
						strTmp += exstyle [i].m_lpsz + CString (' ');

				TRACEF (strTmp);
			}
			*/

			if (str.GetLength ()) {
				str = strTitle + _T (" --> ") + str;
			}
			else {
				str = strTitle;
			}

			/*
			TRACEF (GetClassName (m_target.GetTarget ()));
			TRACEF (GetClassName (::GetDesktopWindow ()));

			if (GetClassName (m_target.GetTarget ()) == GetClassName (::GetDesktopWindow ()))
				Dock ();
			*/

			TRACEF (_T ("SetTarget: ") + str);
			SetWindowText (str);
			m_target.SetTarget (p->m_hWnd);


			/*
			bool bIsStartMenuOpen = IsStartMenuOpen ();

			if (!IsWin7 ()) {
				if (!bIsStartMenuOpen ) {
					if (dwStyle & WS_POPUP)
						Undock ();
					else
						Dock ();
				}
			}
			else {
				if (bIsStartMenuOpen)
					Undock ();
				else 
					Dock ();
			}
			*/

			/*
			#ifdef _DEBUG
			CString strTrace;
			
			strTrace.Format (_T ("SetTarget: 0x%p [%s]"), m_target.GetTarget (), str);
			CDebug::Trace (strTrace, true, lpszFile, lLine);
			#endif
			*/
		}
	}
}

void CKeyboardDlg::OnActivateApp(BOOL bActive, DWORD hTask) 
{
/*
	#ifdef _DEBUG
	CString str;
	str.Format (_T ("CKeyboardDlg::OnActivateApp: %s, 0x%p"), bActive ? _T ("true") : _T ("false"), hTask);
	TRACEF (str);
	#endif //_DEBUG
*/
	OnTimer (IDT_ACTIVEWINDOW);
	CDialog::OnActivateApp(bActive, hTask);
}

void CKeyboardDlg::CheckInputClass (HWND hwnd)
{
	TCHAR szClass [256] = { 0 };

	::GetClassName (m_hwndFocus, szClass, ARRAYSIZE (szClass) - 1);

	if (_tcslen (szClass) && Find (m_vInputClassesExclude, szClass) == -1) {
		if (Find (m_vInputClasses, szClass) == -1) {
			m_vInputClasses.Add (szClass);
			theApp.WriteProfileString (_T ("CKeyboardDlg"), _T ("input classes"), ToString (m_vInputClasses));

			TRACEF (_T ("adding window class: ") + CString (szClass));
			TRACEF (theApp.GetProfileString (_T ("CKeyboardDlg"), _T ("input classes")));
		}
	}
}

void CKeyboardDlg::OnTimer(UINT nIDEvent) 
{
	//TRACEF (GetWndText (::GetForegroundWindow ()) + _T (": ") + GetClassName (::GetForegroundWindow ()));

	//if (IsMatrix ()) 
	{
		if (m_bLangMenuOpen) {
			TRACEF (_T ("OnTimer: ignore: m_bLangMenuOpen: ") + ToString (m_bLangMenuOpen));
			return;
		}
	}

	if (nIDEvent == IDT_STARTUP) {
		if (IsMatrix () && IsWindowVisible ()) {
			OnHide ();
			KillTimer (IDT_STARTUP);
		}
	}

	if (nIDEvent == IDT_ACTIVEWINDOW) {
		if (IsMatrix () && m_hide.m_bHidden) {
			DWORD dw = 0;

			::SendMessageTimeout (HWND_BROADCAST, WM_SHOWKBWIN, 0, 0, SMTO_NORMAL, 1, &dw);
		}

		if (!m_hide.m_bHidden) {
			bool bIsStartMenuOpen = IsStartMenuOpen ();
			const DWORD dwStyle		= ::GetWindowLong (m_target.GetTarget (), GWL_STYLE);
			const DWORD dwExStyle	= ::GetWindowLong (m_target.GetTarget (), GWL_EXSTYLE);

			/*
			if (dwStyle & WS_POPUP) {
				CRect rc (0, 0, 0, 0);
				CString str;

				::GetWindowRect (m_target.GetTarget (), rc);
				str.Format (_T ("%s: %d, %d (%d, %d)"), GetWndText (m_target.GetTarget ()), rc.left, rc.top, rc.Width (), rc.Height ());
				TRACEF (str);
			}
			*/

			/*
			if (!IsWin7 ()) {
				const DWORD dwStyle		= ::GetWindowLong (m_target.GetTarget (), GWL_STYLE);
				const DWORD dwExStyle	= ::GetWindowLong (m_target.GetTarget (), GWL_EXSTYLE);

				if (!bIsStartMenuOpen) {
					if (dwStyle & WS_POPUP)
						Undock ();
					else
						Dock ();
				}
			}
			else {
			*/
				if (bIsStartMenuOpen) {
					//if (m_bDock)
					//	Undock ();
				}
				else {
					bool bUndock = (dwStyle & WS_POPUP) ? true : false;
					HWND h = m_target.GetTarget (); 

					while (h = ::GetWindow (h, GW_OWNER)) {
						//{ TCHAR sz [128] = { 0 }; ::GetWindowText (h, sz, ARRAYSIZE (sz)); TRACEF (sz); }
						
						bUndock = (::GetWindowLong (h, GWL_STYLE) & WS_POPUP) ? true : false;
					}

					if (bUndock)
						Undock ();
					else
						Dock ();
				}
			//}
		}

		/*
		{
			if (HWND hwnd = ::GetTopWindow (NULL)) {
				TCHAR sz [256] = { 0 };
				::GetClassName (hwnd, sz, ARRAYSIZE (sz) - 1);
				TRACEF (CString (_T ("topmost: ")) + sz);
			}
		}
		*/

		if (!IsMatrix ()) {
			if (theApp.IsAutoHide ()) { // && m_hide.m_bHidden) {
				if (m_hwndLastFocus != m_hwndFocus) {
					clock_t clock_tNow = clock ();
					int nDiff = (int)((double)(clock () - m_tFocus) / CLOCKS_PER_SEC * 1000.0);

					if (m_hwndFocus) { // && (nDiff >= 250)) {
						TCHAR szClass [256] = { 0 };
						CString str;
						DWORD dwStyle = ::GetWindowLong (m_hwndFocus, GWL_STYLE);
						DWORD dwExStyle = ::GetWindowLong (m_hwndFocus, GWL_EXSTYLE);
						bool bShow = false;

						::GetClassName (m_hwndFocus, szClass, ARRAYSIZE (szClass) - 1);

						if (_tcsicmp (szClass, _T ("Shell_TrayWnd")) != 0 &&
							_tcslen (szClass) > 0) 
						{
							for (int i = 0; i < m_vInputClasses.GetSize (); i++) {
								CString strClass = m_vInputClasses [i];

								if (!_tcsicmp (szClass, strClass)) {
									bShow = true;
									break;
								}
							}

							if (!_tcsicmp (szClass, _T ("Edit")) && (dwStyle & ES_READONLY))
								bShow = false;

							/*
							#ifdef _DEBUG
							{
								TCHAR szTmp [256] = { 0 };
								::GetClassName (m_hwndLastFocus, szTmp, ARRAYSIZE (szTmp) - 1);
								//str.Format (_T ("%sm_hwndFocus: 0x%p [%-30s] style: 0x%p 0x%p %s"), bShow ? _T ("SHOW ") : _T ("     "), m_hwndFocus, szClass, dwStyle, dwExStyle, GetStyleString (dwStyle, dwExStyle)); TRACEF (str);
								str.Format (_T ("%sm_hwndFocus: 0x%p [%-30s] [last focus: 0x%p [%-30s]]"), bShow ? _T ("SHOW ") : _T ("     "), m_hwndFocus, szClass, m_hwndLastFocus, szTmp); TRACEF (str);
							}
							#endif
							*/

							if (!m_hide.m_bUserOpened && !m_bLabelEdit) {
								if (m_hide.m_bHidden && bShow) {
									m_target.SetTarget (m_hwndFocus);
									OnShow ();
									SETFOREGROUNDWINDOW (m_hwndFocus);
								}
								else if (!m_hide.m_bHidden && !bShow) {
									if (!_tcsicmp (szClass, _T ("ComboBox"))) 
										while (::SendMessage (m_hwndFocus, CB_GETDROPPEDSTATE, 0, 0))
											::Sleep (10); // wait till it closes

									m_target.SetTarget (m_hwndFocus);
									OnHide ();
									SETFOREGROUNDWINDOW (m_hwndFocus);
								}
							}

							m_hwndLastFocus = m_hwndFocus;
						}
					}
				}
			}
		}
	}

	if (!IsMatrix () && !m_hide.m_bHidden && nIDEvent == IDT_ACTIVEWINDOW) {
		static int nLastScrLk = -1;
		static int nLastNumlock = -1;
		static int nLastCapslock = -1;
		static int nLastShift = -1;

		int nScrLk = ::GetKeyState (VK_SCROLL);
		int nNumlock = ::GetKeyState (VK_NUMLOCK);
		int nCapslock = ::GetKeyState (VK_CAPITAL);

		if (nLastNumlock != nNumlock) {
			if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_KEYPAD_NUMLOCK)) {
				p->SetCheck (nNumlock > 0 ? 1 : 0);
				OnNumlock ();
			}

			nLastNumlock = nNumlock;
		}

		if (nLastScrLk != nScrLk) {
			if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_KEYPAD_SCROLLLOCK)) 
				p->SetCheck (nScrLk > 0 ? 1 : 0);

			nLastScrLk = nScrLk;
		}

		if (nLastCapslock != nCapslock) {
			if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_CAPS)) 
				p->SetCheck (nCapslock > 0 ? 1 : 0);

			nLastCapslock = nCapslock;
			OnCaps ();
		}

		if (CWnd * pForeground = CWnd::GetForegroundWindow ()) {
			if (CWnd * p = pForeground->GetTopLevelOwner ()) {
				if ((p->m_hWnd != m_hWnd) && m_bDock) {
					CRect rc (0, 0, 0, 0), rcWin (0, 0, 0, 0);

					if (IsStartMenuOpen ())
						::SetWindowPos (m_hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);

					::GetWindowRect (m_hWnd, &rc);
					::GetWindowRect (p->m_hWnd, &rcWin);

					if (rcWin.bottom > rc.top) {
						//Dock ();
					}
				}

				if (p->m_hWnd != m_hWnd && p->m_hWnd != m_target.GetTarget ()) {
					SETTARGET (p);
					//{ CString str; p->GetWindowText (str); TRACEF (str); }
				}
			}
		}
	}

	/*
	if (nIDEvent == IDT_CHECKSTARTMENU) {
		if (m_bStartupShortcut) {
			if (!HasStartMenuShortcut ()) {
				KillTimer (IDT_CHECKSTARTMENU);

				CString str = LoadString (IDS_SHORTCUTDELETED);

				if (::MessageBox (NULL, str, LoadString (IDS_KEYBOARD), MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON1 | MB_SYSTEMMODAL) == IDYES) {
					m_bStartKbMonitor = false;
					OnFileClose ();

					if (HWND hwnd = FindKeyboardMonitor ()) {
						DWORD dwResult = 0;
						LRESULT lResult = ::SendMessageTimeout (hwnd, WM_SHUTDOWNAPP, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);
					}
				}
				else {
					m_bStartupShortcut = false;
					SetTimer (IDT_CHECKSTARTMENU, 5000, NULL);
				}
			}
		}
		else {
			if (HasStartMenuShortcut ())
				m_bStartupShortcut = true;
		}
	}
	*/


	CDialog::OnTimer(nIDEvent);
}

/*
bool CKeyboardDlg::HasStartMenuShortcut ()
{
	CString strFolder [2] = 
	{	
		FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, _T ("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders"), _T ("Startup"), _T ("")),
		FoxjetDatabase::GetProfileString (HKEY_LOCAL_MACHINE, _T ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\explorer\\Shell Folders"), _T ("Common Startup"), _T ("")),
	};
	TCHAR szThis [MAX_PATH] = { 0 };
	bool bResult = false;

	HMODULE hModule = ::GetModuleHandle (NULL);
	::GetModuleFileName (hModule, szThis, ARRAYSIZE (szThis) - 1);

	for (int nFolder = 0; nFolder < ARRAYSIZE (strFolder); nFolder++) {
		CStringArray v;

		GetFiles (strFolder [nFolder], _T ("*.lnk"), v);

		for (int i = 0; i < v.GetSize (); i++) {
			CString strLink = v [i];
			CComPtr<IShellLink> ipShellLink;

			HRESULT hRes = ::CoCreateInstance (CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER, IID_IShellLink, (void**)&ipShellLink); 

			if (SUCCEEDED (hRes)) { 
			    CComQIPtr<IPersistFile> ipPersistFile (ipShellLink);

				hRes = ipPersistFile->Load (strLink, STGM_READ); 

				if (SUCCEEDED (hRes)) {
					hRes = ipShellLink->Resolve(NULL, SLR_UPDATE); 

					if (SUCCEEDED (hRes)) {
						WIN32_FIND_DATA wfd;    
						TCHAR szPath [_MAX_PATH] = { 0 };

						hRes = ipShellLink->GetPath (szPath, ARRAYSIZE (szPath), &wfd, SLGP_RAWPATH); 

						if (SUCCEEDED (hRes)) {
							if (!_tcsicmp (szThis, szPath)) 
								bResult = true;
						}
					}
				}

				ipPersistFile.Release ();
			}

			ipShellLink.Release ();
		}
	}

	return bResult;
}
*/

LRESULT CKeyboardDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	switch (message) {
	case WM_MOVE:
	case WM_MOVING:
		if (m_bDock)
			return FALSE;
	}

	return CDialog::WindowProc(message, wParam, lParam);
}

int CKeyboardDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{

/*
IDD_KEYBOARD DIALOGEX 0, 0, 400, 100
STYLE WS_CAPTION | WS_SYSMENU | WS_THICKFRAME
EXSTYLE WS_EX_TOOLWINDOW
CAPTION "Virtual keyboard"
FONT 8, "MS Sans Serif"


IDD_KEYBOARD DIALOGEX 0, 0, 400, 100
STYLE WS_THICKFRAME | WS_CAPTION | WS_VISIBLE | WS_CLIPSIBLINGS | WS_SYSMENU | WS_OVERLAPPED | WS_MINIMIZEBOX
EXSTYLE  WS_EX_TOOLWINDOW | WS_EX_TOPMOST | WS_EX_WINDOWEDGE | WS_EX_APPWINDOW | WS_EX_NOACTIVATE
CAPTION "Virtual keyboard"
FONT 8, "MS Sans Serif"
*/

	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

//	VERIFY (m_wndPreview.Create (this, CRect (), LBL_PREVIEW));


	ModifyStyleEx (0, WS_EX_NOACTIVATE, ::nFlags);

	return 0;
}

void CKeyboardDlg::OnNcLButtonDown(UINT nHitTest, CPoint point) 
{
	if (!m_target.GetTarget ()) 
		m_target.SetTarget (::GetForegroundWindow ());

	ModifyStyleEx (WS_EX_NOACTIVATE, 0, ::nFlags);
	if (!m_bXP) 		SETFOREGROUNDWINDOW (m_hWnd);
	CDialog::OnNcLButtonDown(nHitTest, point);
	ModifyStyleEx (0, WS_EX_NOACTIVATE, ::nFlags);
}

void CKeyboardDlg::OnNcLButtonUp(UINT nHitTest, CPoint point) 
{
	CDialog::OnNcLButtonUp(nHitTest, point);
}

void CKeyboardDlg::OnMove(int x, int y) 
{
	if (!m_bXP && m_target.GetTarget ()) SETFOREGROUNDWINDOW (m_target.GetTarget ());
	CDialog::OnMove (x, y);
}

void CKeyboardDlg::OnAlt ()
{
	struct
	{
		UINT m_nID;
		LPCTSTR m_lpsz1;
		LPCTSTR m_lpsz2;
	}
	static const map [] = 
	{
		{ BTN_KEYPAD_SYSREQ,		_T ("Sys Rq"),		_T ("Prt Scn"),		}, 
		{ BTN_KEYPAD_BREAK,			_T ("Break"),		_T ("Pause"),		}, 
	};
	bool bAlt = IsAlt ();

	for (int i = 0; i < ARRAYSIZE (map); i++) {
		if (CWnd * p = GetDlgItem (map [i].m_nID)) {
			CString str = bAlt ? map [i].m_lpsz1 : map [i].m_lpsz2;

			p->SetWindowText (str);
		}
	}
}

void CKeyboardDlg::OnNumlock ()
{
	struct
	{
		UINT m_nID;
		LPCTSTR m_lpsz1;
		LPCTSTR m_lpsz2;
		TCHAR m_c;
	}
	static const map [] = 
	{
		{ BTN_KEYPAD_0,				_T ("0"),			_T ("Ins"),			0,			},
		{ BTN_KEYPAD_1,				_T ("1"),			_T ("End"),			0,			},
		{ BTN_KEYPAD_2,				_T ("2"),			_T (""),			0xE2,		},
		{ BTN_KEYPAD_3,				_T ("3"),			_T ("PgDn"),		0,			},
		{ BTN_KEYPAD_4,				_T ("4"),			_T (""),			0xDF,		},
		{ BTN_KEYPAD_5,				_T ("5"),			_T (""),			0,			},
		{ BTN_KEYPAD_6,				_T ("6"),			_T (""),			0xE0,		},
		{ BTN_KEYPAD_7,				_T ("7"),			_T ("Home"),		0,			},
		{ BTN_KEYPAD_8,				_T ("8"),			_T (""),			0xE1,		},
		{ BTN_KEYPAD_9,				_T ("9"),			_T ("PgUp"),		0,			},
		{ BTN_KEYPAD_PERIOD,		_T ("."),			_T ("Del"),			0,			},
	};

	CFont * pfntNormal = m_pfntKey;//GetFont ();

	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_KEYPAD_NUMLOCK)) {
		bool bNumLock = p->GetCheck () == 1 ? true : false;

		for (int i = 0; i < ARRAYSIZE (map); i++) {
			if (CWnd * p = GetDlgItem (map [i].m_nID)) {
				CString str = bNumLock ? map [i].m_lpsz1 : map [i].m_lpsz2;

				if (bNumLock) {
					p->SetWindowText (str);
					p->SetFont (str.GetLength () == 1 ? pfntNormal : m_pfntNumpad);
				}
				else {
					if (map [i].m_c) {
						p->SetFont (m_pfntArrow);
						p->SetWindowText (CString (map [i].m_c));
					}
					else {
						CFont * pfnt = str.GetLength () == 1 ? pfntNormal : m_pfntNumpad;

						switch (map [i].m_nID) {
						case BTN_KEYPAD_PERIOD:
						case BTN_KEYPAD_0:
							pfnt = pfntNormal;
							break;
						}

						p->SetFont (pfnt);
						p->SetWindowText (str);
					}
				}
			}
		}
	}
}

LRESULT CKeyboardDlg::OnFocusChanged (WPARAM wParam, LPARAM lParam)
{
	HWND hwnd = (HWND)lParam;

	{
		/*
		#ifdef _DEBUG
		TCHAR szClass [128] = { 0 };
		CString str;

		::GetClassName (hwnd, szClass, ARRAYSIZE (szClass) - 1);
		str.Format (_T ("OnFocusChanged: 0x%p [%s]"), hwnd, szClass);
		TRACEF (str);
		#endif //_DEBUG
		*/

		HWND hOwner = hwnd;
		
		for (int i = 1; hOwner; i++)  {
			TCHAR sz [128] = { 0 };
			TCHAR szClass [128] = { 0 };

			::GetClassName (hOwner, szClass, ARRAYSIZE (szClass) - 1);

			/*
			#ifdef _DEBUG
			CString str;
			::GetWindowText (hOwner, sz, ARRAYSIZE (sz));
			str.Format (_T ("%s0x%p [%s] [%s]"), CString ('\t', i), hOwner, szClass, sz);
			TRACEF (str);
			#endif //_DEBUG
			*/

			if (!_tcslen (szClass))
				return 0;
			
			if (hOwner == m_hWnd) {
				/*
				#ifdef _DEBUG
				::GetWindowText (m_hwndLastFocus, sz, ARRAYSIZE (sz));
				::GetClassName (m_hwndLastFocus, szClass, ARRAYSIZE (szClass) - 1);
				str.Format (_T ("m_hwndLastFocus: 0x%p [%s] [%s]"), hOwner, szClass, sz);
				TRACEF (str);
				#endif //_DEBUG
				*/

				//SETFOREGROUNDWINDOW (m_hwndLastFocus);

				return 0;
			}

			hOwner = ::GetParent (hOwner);
		}
	}

	if (m_hWnd != hwnd && !::IsChild (m_hWnd, hwnd)) {
		if (m_hwndFocus != hwnd) {
			m_hwndFocus = hwnd;
			m_tFocus = clock ();
		}
	}
	else {
		/*
		if (HWND hNext = ::GetNextWindow (m_hWnd, GW_HWNDNEXT)) {
			TCHAR szClass [128] = { 0 };

			::GetClassName (hNext, szClass, ARRAYSIZE (szClass) - 1);
			TRACEF (CString (_T ("GetNextWindow: ")) + szClass);
		}
		*/
	}

	return 0;
}

LRESULT CKeyboardDlg::OnIsKeyboard (WPARAM wParam, LPARAM lParam)
{
	return BW_HOST_TCP_PORT;
}

LRESULT CKeyboardDlg::OnIsAppRunning (WPARAM wParam, LPARAM lParam)
{
	if (!wParam) {
		m_hide.m_bUserOpened = true;
		OnShow ();
		Resize ();
		SETFOREGROUNDWINDOW (m_hWnd);
	}

	return IsMatrix () ? 1 : -1;
}

LRESULT CKeyboardDlg::OnIsKeyboardVisible (WPARAM wParam, LPARAM lParam)
{
	return IsWindowVisible () ?(LRESULT)m_hWnd : NULL;
}

LRESULT CKeyboardDlg::OnSreenshot (WPARAM wParam, LPARAM lParam)
{
	m_hide.m_bAnimate = false;

	if (!wParam)
		OnHide ();
	else
		OnShow ();

	m_hide.m_bAnimate = true;

	return WM_SCREENSHOT;
}

static BOOL CALLBACK EnumWindowsProc (HWND hWnd, LPARAM lParam)
{
	CArray <HWND, HWND> & v = * (CArray <HWND, HWND> *)lParam;

	TCHAR sz [512] = { 0 };

	if (::IsWindowVisible (hWnd)) {
		DWORD dwStyle = ::GetWindowLong (hWnd, GWL_STYLE);
		DWORD dwExStyle = ::GetWindowLong (hWnd, GWL_EXSTYLE);
		
		/*
		CString str;

		::GetWindowText (hWnd, sz, sizeof (sz));	str += sz;
		str += _T (" - ");
		::GetClassName (hWnd, sz, sizeof (sz));		str += sz;
		str += _T (" [");
		str += GetWsStyle (::GetWindowLong (hWnd, GWL_STYLE));
		str += _T ("] [");
		str += GetWsExStyle (::GetWindowLong (hWnd, GWL_EXSTYLE));
		str += _T ("] ");

		TRACEF (str);
		*/

		if ((dwStyle & WS_MINIMIZEBOX) || (dwStyle & WS_MAXIMIZEBOX)) 
			v.Add (hWnd);
	}

	return TRUE;
}

void CKeyboardDlg::OnDock() 
{
	m_hide.m_bDocked = m_bDock = true;
	theApp.WriteProfileInt (_T ("CKeyboardDlg"), _T ("dock"), m_bDock);
	Dock ();
}

void CKeyboardDlg::OnUndock() 
{
	m_hide.m_bDocked = m_bDock = false;
	theApp.WriteProfileInt (_T ("CKeyboardDlg"), _T ("dock"), m_bDock);
	Undock ();
	RestoreState ();
}

CRect CKeyboardDlg::GetDesktopRect ()
{
	CRect rcDesktop (0, 0, 0, 0);

	::GetWindowRect (::GetDesktopWindow (), &rcDesktop);

	if (HWND hwndStart = ::FindWindow (_T ("Shell_TrayWnd"), _T (""))) {
		CRect rcStart (0, 0, 0, 0);

		::GetWindowRect (hwndStart, &rcStart);
		rcDesktop.bottom = rcStart.top;
	}

	return rcDesktop;
}

void CKeyboardDlg::Fullscreen () 
{
	if (IsMatrix ()) {
		CRect rcDesktop = GetDesktopRect ();

		#ifdef _DEBUG
		rcDesktop.right = rcDesktop.left + 1024;
		rcDesktop.bottom = rcDesktop.top + 600;
		rcDesktop += CPoint (
			(::GetSystemMetrics (SM_CXSCREEN) - rcDesktop.Width ()) / 2,
			(::GetSystemMetrics (SM_CYSCREEN) - rcDesktop.Height ()) / 2);
		#endif

		CRect rcKeyboard = rcDesktop;
		CRect rcText = rcKeyboard;

		rcKeyboard.top += (int)((double)rcKeyboard.Height () * 0.3333);
		rcText.bottom = rcKeyboard.top;
		m_rcKbWin = rcText;

		::SetWindowPos (m_hWnd, HWND_TOPMOST, rcKeyboard.left, rcKeyboard.top, rcKeyboard.Width (), rcKeyboard.Height (), 0);
	}
}

void CKeyboardDlg::Dock() 
{
	if (IsMatrix ()) {
		Fullscreen ();
		return;
	}

	TRACEF ("Dock");
	CArray <HWND, HWND> v;
	CRect rc (0, 0, 0, 0);
	CRect rcDesktop = GetDesktopRect ();

	::EnumWindows (::EnumWindowsProc, (LPARAM)&v);
	m_bDock = true;

	::GetWindowRect (m_hWnd, &rc);
	rc = CRect (CPoint (0, rcDesktop.bottom - rc.Height ()), CSize (rcDesktop.Width (), rc.Height ()));
	//rc.DeflateRect (10, 0, 20, 0);
	::SetWindowPos (m_hWnd, NULL, rc.left, rc.top, rc.Width (), rc.Height (), SWP_SHOWWINDOW | SWP_NOACTIVATE);// | SWP_NOZORDER);

	for (int i = 0; i < v.GetSize (); i++) {
		HWND hWnd = v [i];
		CRect rcWin (0, 0, 0, 0);
		TCHAR sz [512] = { 0 };
		WINDOWPLACEMENT wp;

		ZeroMemory (&wp, sizeof (wp));

		::GetWindowRect (hWnd, &rcWin);
		::GetWindowText (hWnd, sz, ARRAYSIZE (sz) - 1);
		::GetWindowPlacement (hWnd, &wp);

		bool bResize = (rcWin.bottom > rc.top) ? true : false;

//		if (wp.showCmd == SW_MAXIMIZE)
//			bResize = true;

		/*
		{
			CString str = sz + CString (_T (": "));
			
			switch (wp.showCmd) {
			case SW_HIDE:				str +=  (_T ("SW_HIDE"));			break;
			case SW_MAXIMIZE:			str +=  (_T ("SW_MAXIMIZE"));		break;
			case SW_MINIMIZE:			str +=  (_T ("SW_MINIMIZE"));		break;
			case SW_RESTORE:			str +=  (_T ("SW_RESTORE"));			break;
			case SW_SHOW:				str +=  (_T ("SW_SHOW"));			break;
			//case SW_SHOWMAXIMIZED:		str +=  (_T ("SW_SHOWMAXIMIZED"));	break;
			case SW_SHOWMINIMIZED:		str +=  (_T ("SW_SHOWMINIMIZED"));	break;
			case SW_SHOWMINNOACTIVE:	str +=  (_T ("SW_SHOWMINNOACTIVE"));	break;
			case SW_SHOWNA:				str +=  (_T ("SW_SHOWNA"));			break;
			case SW_SHOWNOACTIVATE:		str +=  (_T ("SW_SHOWNOACTIVATE"));	break;
			case SW_SHOWNORMAL:			str +=  (_T ("SW_SHOWNORMAL"));		break;
			}

			TRACEF (str);
		}
		*/

		if (bResize) {
			CString str;

			rcWin.bottom = rc.top;
			//str.Format (_T ("%s: %d, %d (%d, %d)"), sz, rcWin.top, rcWin.left, rcWin.Width (), rcWin.Height ()); TRACEF (str);
	
			::PostMessage (HWND_BROADCAST, WM_KEYBOARDCHANGE, KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_DOCK);

			if (!::SetWindowPos (hWnd, NULL, rcWin.left, rcWin.top, rcWin.Width (), rcWin.Height (), SWP_NOZORDER | SWP_NOACTIVATE))
				TRACEF (FORMATMESSAGE (::GetLastError ()));
		}
	}
}

void CKeyboardDlg::Undock() 
{
	if (IsMatrix ()) {
		Fullscreen ();
		return;
	}

	//TRACEF ("Undock");
	CArray <HWND, HWND> v;
	CRect rcDesktop = GetDesktopRect ();

	::EnumWindows (::EnumWindowsProc, (LPARAM)&v);
	m_bDock = false;

	::SetWindowPos (m_hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);

	for (int i = 0; i < v.GetSize (); i++) {
		HWND hWnd = v [i];
		CRect rcWin (rcDesktop);
		TCHAR sz [512] = { 0 };
		WINDOWPLACEMENT wp;

		ZeroMemory (&wp, sizeof (wp));

		::GetWindowText (hWnd, sz, ARRAYSIZE (sz) - 1);
		::GetWindowPlacement (hWnd, &wp);

		if (wp.showCmd == SW_MAXIMIZE) {
			CString str;

			::PostMessage (HWND_BROADCAST, WM_KEYBOARDCHANGE, KBCHANGETYPE_DOCKSTATE, KBCHANGETYPE_DOCKSTATE_UNDOCK);

			if (!::SetWindowPos (hWnd, NULL, rcWin.left, rcWin.top, rcWin.Width (), rcWin.Height (), SWP_NOZORDER | SWP_NOACTIVATE))
				TRACEF (FORMATMESSAGE (::GetLastError ()));
		}
	}
}

void CKeyboardDlg::OnMenuF1() 
{
	CString strTitle = LoadString (AFX_IDS_APP_TITLE);
	CStringArray vCmdLine;
	CAutoHideDlg dlgAutoHide;

	TokenizeCmdLine (::GetCommandLine (), vCmdLine);

	FoxjetCommon::CAboutDlg dlg (CVersion (verApp), vCmdLine, LoadString (IDS_ABOUT) + _T (" ") + strTitle);

	if (theApp.IsAutoHide ()) 
		dlg.AddPage (&dlgAutoHide);

	dlg.m_strApp		= strTitle;
	
	bool bUserOpened	= m_hide.m_bUserOpened;
	HWND hwndFocus		= m_hwndFocus;

	m_hide.m_bUserOpened = true;

	if (dlg.DoModal () == IDOK) {
	}

	LoadInputClasses ();

	m_hide.m_bUserOpened	= bUserOpened;
	m_hwndFocus				= hwndFocus;
	::BringWindowToTop (m_target.GetTarget ());
}

BOOL CKeyboardDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	return FALSE;//CDialog::OnHelpInfo(pHelpInfo);
}

void CKeyboardDlg::OnNcLButtonDblClk(UINT nHitTest, CPoint point) 
{
/*
	if (!m_bDock)
		OnDock ();
	else
		OnUndock ();
*/

	CDialog::OnNcLButtonDblClk(nHitTest, point);
}

void CKeyboardDlg::OnMoving(UINT fwSide, LPRECT pRect) 
{
	CDialog::OnMoving(fwSide, pRect);
}


void CKeyboardDlg::OnWindowPosChanged(WINDOWPOS FAR* lpwndpos) 
{
	CString strKey = _T ("CKeyboardDlg\\WindowState");

	CDialog::OnWindowPosChanged(lpwndpos);
	
	if (!m_bXP && m_target.GetTarget ()) SETFOREGROUNDWINDOW (m_target.GetTarget ());
	
	if (!m_bDock) {
		if (!(lpwndpos->flags & SWP_NOSIZE)) {
			int nExpanded = 0;

			//if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_EXPAND)) 
			//	nExpanded = p->GetCheck ();

			theApp.WriteProfileInt (strKey, _T ("right"), lpwndpos->cx);
			theApp.WriteProfileInt (strKey, nExpanded == 1 ? _T ("bottom ex") : _T ("bottom"), lpwndpos->cy);

			TRACEF (_T ("size: ") + ToString (lpwndpos->cx) + _T (" ") + ToString (lpwndpos->cy));
		}
		if (!(lpwndpos->flags & SWP_NOMOVE)) {
			theApp.WriteProfileInt (strKey, _T ("left"), lpwndpos->x);
			theApp.WriteProfileInt (strKey, _T ("top"), lpwndpos->y);
			TRACEF (_T ("move: ") + ToString (lpwndpos->x) + _T (" ") + ToString (lpwndpos->y));
		}
	}
}

void CKeyboardDlg::OnWindowPosChanging(WINDOWPOS FAR* lpwndpos) 
{
	if (IsMatrix ()) {
		if (m_hide.m_bHidden)
			lpwndpos->flags &= ~SWP_SHOWWINDOW;
	}

	static const CSize sizeMin (600, 150);
	CRect rcDesktop = GetDesktopRect ();

	if (!(lpwndpos->flags & SWP_NOSIZE)) {
		if (lpwndpos->cx < sizeMin.cx) 
			lpwndpos->cx = sizeMin.cx;
		if (lpwndpos->cy < sizeMin.cy) 
			lpwndpos->cy = sizeMin.cy;

		if ((lpwndpos->x + lpwndpos->cx) > rcDesktop.Width ())
			lpwndpos->x = rcDesktop.Width () - lpwndpos->cx;
		if ((lpwndpos->y + lpwndpos->cy) > rcDesktop.Height ())
			lpwndpos->y = rcDesktop.Height () - lpwndpos->cy;
	}

	if (!(lpwndpos->flags & SWP_NOMOVE)) {
		if (lpwndpos->x < 0) 
			lpwndpos->x = 0;
		if (lpwndpos->y < 0) 
			lpwndpos->y = 0;
	}

	CDialog::OnWindowPosChanging(lpwndpos);
}

void CKeyboardDlg::SaveState ()
{
	CString strKey = _T ("CKeyboardDlg\\WindowState");
	CRect rc (-1, -1, -1, -1);
	int nExpanded = 1;
	
	//if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_EXPAND))
	//	nExpanded = p->GetCheck ();

	GetWindowRect (rc);
	
	theApp.WriteProfileInt (strKey, _T ("left"), rc.left);
	theApp.WriteProfileInt (strKey, _T ("top"), rc.top);
	theApp.WriteProfileInt (strKey, _T ("right"), rc.Width ());
	theApp.WriteProfileInt (strKey, nExpanded == 1 ? _T ("bottom ex") : _T ("bottom"), rc.Height ());
	theApp.WriteProfileInt (strKey, _T ("expanded"), nExpanded);

	//{ CString str; str.Format (_T ("SaveState: %d, %d (%d, %d)"), rc.left, rc.top, rc.Width (), rc.Height ()); TRACEF (str); }
}

void CKeyboardDlg::RestoreState ()
{
	CString strKey = _T ("CKeyboardDlg\\WindowState");
	int nExpanded = theApp.GetProfileInt (strKey, _T ("expanded"), 0);
	int x = theApp.GetProfileInt (strKey, _T ("left"), -1);
	int y = theApp.GetProfileInt (strKey, _T ("top"), -1);
	int cx = theApp.GetProfileInt (strKey, _T ("right"), -1);
	int cy = theApp.GetProfileInt (strKey, nExpanded == 1 ? _T ("bottom ex") : _T ("bottom"), -1);

	{ CString str; str.Format (_T ("RestoreState: %d, %d (%d, %d)"), x, y, cx, cy); TRACEF (str); }

	//if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_EXPAND)) {
	//	p->SetCheck (nExpanded);
	//	OnExpand ();
	//}

	if (x != -1 && cx != -1 && y != -1 && cy != -1) 
		//::SetWindowPos (m_hWnd, HWND_TOPMOST, x, y, cx, cy, SWP_SHOWWINDOW | SWP_NOZORDER | SWP_NOACTIVATE);
		::SetWindowPos (m_hWnd, NULL, x, y, cx, cy, SWP_SHOWWINDOW | SWP_NOZORDER | SWP_NOACTIVATE);
}

void CKeyboardDlg::OnMenuHide ()
{
	m_hide.m_bUserOpened = false;
	OnHide ();
}

void CKeyboardDlg::OnMenuDock()
{
	if (m_bDock)
		OnUndock ();
	else
		OnDock ();	
}

void CKeyboardDlg::OnMenuNumpad ()
{
	CRect rc (0, 0, 0, 0);

	m_bNumpad = !m_bNumpad;
	theApp.WriteProfileInt (_T ("CKeyboardDlg"), _T ("numpad"), m_bNumpad);
	Resize ();
	::BringWindowToTop (m_target.GetTarget ());
}

void CKeyboardDlg::OnMenuFont ()
{
	CFontDlg dlg (this);
	LOGFONT lf;

	VERIFY (m_pfntKey->GetLogFont (&dlg.m_lfNormal));
	VERIFY (m_pfntNumpad->GetLogFont (&dlg.m_lfSmall));
	VERIFY (m_pfntArrow->GetLogFont (&lf));
	dlg.m_nArrow = lf.lfHeight;

	bool bUserOpened	= m_hide.m_bUserOpened;
	HWND hwndFocus		= m_hwndFocus;

	m_hide.m_bUserOpened = true;

	if (dlg.DoModal () == IDOK) {
		CRect rc (0, 0, 0, 0);

		if (m_pfntKey) delete m_pfntKey;
		if (m_pfntNumpad) delete m_pfntNumpad;
		if (m_pfntArrow) delete m_pfntArrow;

		m_pfntKey = new CFont ();
		m_pfntKey->CreateFontIndirect (&dlg.m_lfNormal);

		m_pfntNumpad = new CFont ();
		m_pfntNumpad->CreateFontIndirect (&dlg.m_lfSmall);

		m_pfntArrow = CreateArrowFont (dlg.m_nArrow);

		SaveFont (m_pfntKey, _T ("Normal"));
		SaveFont (m_pfntNumpad, _T ("Small"));
		theApp.WriteProfileInt (_T ("CKeyboardDlg"), _T ("arrow size"), dlg.m_nArrow);
		OnChangeColors ();

		Resize ();
	}

	m_hide.m_bUserOpened	= bUserOpened;
	m_hwndFocus				= hwndFocus;

	::BringWindowToTop (m_target.GetTarget ());
}

void CKeyboardDlg::OnSysCommand(UINT nID, LPARAM lParam) 
{
	{ CString str; str.Format (_T ("OnSysCommand: %d, %d"), nID, lParam); TRACEF (str);	} 

	switch (nID)
	{
	case IDM_DOCK:		OnMenuDock ();		break;
	case IDA_F1:		OnMenuF1 ();		break;
	case IDM_NUMPAD:	OnMenuNumpad ();	break;
	case IDM_FONT:		OnMenuFont ();		break;
	default:
		CDialog::OnSysCommand(nID, lParam);
		break;
	}
}

CFont * CKeyboardDlg::CreateFontFromRegistry (LPCTSTR lpszName)
{
	CFont * p = new CFont ();
	LOGFONT lf;
	CString strKey;
	
	strKey.Format (_T ("CKeyboardDlg\\font\\%s"), lpszName);

	CString strName		= theApp.GetProfileString (strKey, _T ("lfFaceName"),		_T ("Microsoft Sans Serif"));
	lf.lfHeight			= theApp.GetProfileInt (strKey, _T ("lfHeight"),			!_tcscmp (lpszName, _T ("Normal")) ? 0xffffffec : 0xffffffed);
	lf.lfWidth			= theApp.GetProfileInt (strKey, _T ("lfWidth"),				0);
	lf.lfEscapement		= theApp.GetProfileInt (strKey, _T ("lfEscapement"),		0);
	lf.lfOrientation	= theApp.GetProfileInt (strKey, _T ("lfOrientation"),		0);
	lf.lfWeight			= theApp.GetProfileInt (strKey, _T ("lfWeight"),			FW_BOLD);
	lf.lfItalic			= theApp.GetProfileInt (strKey, _T ("lfItalic"),			0);
	lf.lfUnderline		= theApp.GetProfileInt (strKey, _T ("lfUnderline"),			0);
	lf.lfStrikeOut		= theApp.GetProfileInt (strKey, _T ("lfStrikeOut"),			0);
	lf.lfCharSet		= theApp.GetProfileInt (strKey, _T ("lfCharSet"),			0);
	lf.lfOutPrecision	= theApp.GetProfileInt (strKey, _T ("lfOutPrecision"),		0);
	lf.lfClipPrecision	= theApp.GetProfileInt (strKey, _T ("lfClipPrecision"),		0);
	lf.lfQuality		= theApp.GetProfileInt (strKey, _T ("lfQuality"),			0);
	lf.lfPitchAndFamily = theApp.GetProfileInt (strKey, _T ("lfPitchAndFamily"),	0);

	_tcscpy (lf.lfFaceName, strName);

	p->CreateFontIndirect (&lf);

	return p;
}

void CKeyboardDlg::SaveFont (CFont * p, LPCTSTR lpszName)
{
	LOGFONT lf;
	CString strKey;
	
	memset (&lf, 0, sizeof (lf));	
	strKey.Format (_T ("CKeyboardDlg\\font\\%s"), lpszName);
	p->GetLogFont (&lf);

	theApp.WriteProfileInt (strKey, _T ("lfHeight"),			lf.lfHeight);
	theApp.WriteProfileInt (strKey, _T ("lfWidth"),				lf.lfWidth);
	theApp.WriteProfileInt (strKey, _T ("lfEscapement"),		lf.lfEscapement);
	theApp.WriteProfileInt (strKey, _T ("lfOrientation"),		lf.lfOrientation);
	theApp.WriteProfileInt (strKey, _T ("lfWeight"),			lf.lfWeight);
	theApp.WriteProfileInt (strKey, _T ("lfItalic"),			lf.lfItalic);
	theApp.WriteProfileInt (strKey, _T ("lfUnderline"),			lf.lfUnderline);
	theApp.WriteProfileInt (strKey, _T ("lfStrikeOut"),			lf.lfStrikeOut);
	theApp.WriteProfileInt (strKey, _T ("lfCharSet"),			lf.lfCharSet);
	theApp.WriteProfileInt (strKey, _T ("lfOutPrecision"),		lf.lfOutPrecision);
	theApp.WriteProfileInt (strKey, _T ("lfClipPrecision"),		lf.lfClipPrecision);
	theApp.WriteProfileInt (strKey, _T ("lfQuality"),			lf.lfQuality);
	theApp.WriteProfileInt (strKey, _T ("lfPitchAndFamily"),	lf.lfPitchAndFamily);
	theApp.WriteProfileString (strKey, _T ("lfFaceName"),		lf.lfFaceName);
}

CFont * CKeyboardDlg::CreateArrowFont (int nSize)
{
	CFont * p = new CFont ();

	p->CreateFont (nSize, 0, 0, 0, 0, FALSE, 0, 0,
		DEFAULT_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_CHARACTER_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, _T ("Wingdings"));

	return p;
}

static CString _GetLanguage (const CString & strLcid)
{
	struct 
	{
		LPCTSTR m_lpszLcid;
		LPCTSTR m_lpszName;
	} map [] = 
	{
		{ _T ("en"), _T ("English") },
		{ _T ("sp"), _T ("Spanish") },
		{ _T ("es"), _T ("Spanish") },
		{ _T ("po"), _T ("Potuguese") },
		{ _T ("pt"), _T ("Potuguese") },
		{ _T ("ru"), _T ("Russian") },
		{ _T ("fr"), _T ("French") },
	};

	for (int i = 0; i < ARRAYSIZE (map); i++)
		if (strLcid == map [i].m_lpszLcid)
			return map [i].m_lpszName;

	return _T ("");
}

typedef struct
{
	CString	m_str;
	UINT	m_nCmdID;
	UINT	m_nBitmapID;
} MENUSTRUCT;

CArray <MENUSTRUCT, MENUSTRUCT &> vMenu;

void CKeyboardDlg::OnLanguage() 
{
	CStringArray v;
	CRect rc;
	CMenu menu;
	const CString strConfigSuffix = Extract (m_strConfigFile, _T ("Keyboard_"), _T (".txt"));
	bool bDefault = true;
	int i;

	m_btnCharMap.GetWindowRect (rc);
	menu.CreatePopupMenu ();

	GetFiles (GetHomeDir (), _T ("Keyboard*.txt"), v);
	::vMenu.RemoveAll ();

	for (i = 0; i < v.GetSize (); i++) {
		const CString strSuffix = Extract (v [i], _T ("Keyboard_"), _T (".txt"));
		CString str;
		MENUSTRUCT item;

		TRACEF (v [i]);

		str = FoxjetDatabase::GetLanguage (strSuffix);

		TRACER (strSuffix + _T (": ") + str);

		if (str.IsEmpty ())
			str = ::_GetLanguage (strSuffix);

		TRACER (strSuffix + _T (": ") + str);

		if (str.IsEmpty ())
			str = strSuffix;

		TRACER (strSuffix + _T (": ") + str);

		m_mapLang [(std::wstring)(LPCTSTR)str] = (std::wstring)(LPCTSTR)strSuffix;

		if (!strSuffix.CompareNoCase (_T ("en"))) 
			bDefault = false;

		item.m_str			= str;
		item.m_nCmdID		= IDM_LANGUAGE + i + 1;
		item.m_nBitmapID	= !strConfigSuffix.CompareNoCase (strSuffix) ? IDB_CHECKED : -1;

		::vMenu.Add (item);
	}

	if (bDefault) {
		MENUSTRUCT item;

		item.m_str			= LoadString (IDS_ENGLISH);
		item.m_nCmdID		= IDM_LANGUAGE + i + 1;
		item.m_nBitmapID	= IDB_CHECKED;//-1;

		::vMenu.Add (item);
	}

	for (i = 0; i < ::vMenu.GetSize (); i++) {
		MENUITEMINFO info;

		memset (&info, 0, sizeof (info));
		info.cbSize		= sizeof (info);
		info.fMask		= MIIM_TYPE | MIIM_ID | MIIM_DATA | MIIM_SUBMENU;
		info.fType		= MFT_OWNERDRAW;
		info.wID		= ::vMenu [i].m_nCmdID;
	
		::InsertMenuItem (menu, i, TRUE, &info);
	}

	//if (IsMatrix ()) 
	{
		m_bLangMenuOpen = true;
		TRACEF (_T ("m_bLangMenuOpen: ") + ToString (m_bLangMenuOpen));
	}

	menu.TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON, rc.left, rc.bottom, this);
}	

#ifdef _DEBUG
static CString GetText (HWND hwnd)
{
	CString str;
	DWORD dwLen = 0, dwCopied = 0;

	LRESULT lResult = ::SendMessageTimeout (hwnd, WM_GETTEXTLENGTH, 0, 0, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dwLen);
	const int nLen = dwLen + 1;
	TCHAR * psz = new TCHAR [nLen];

	ZeroMemory (psz, sizeof (TCHAR) * nLen);
	lResult = ::SendMessageTimeout (hwnd, WM_GETTEXT, (WPARAM)nLen, (LPARAM)psz, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dwCopied);

	str = psz;
	delete [] psz;

	return str;
}
#endif

LRESULT CKeyboardDlg::OnClick (WPARAM wParam, LPARAM lParam)
{
	bool bShow = false;
	CPoint pt (LOWORD (lParam), HIWORD (lParam)); 
	MSG msg;
	HWND hwndHit = NULL;
	CMap <HWND, HWND, CRect, CRect &> v;
	DWORD dwCaret = 0;
	
	while (::PeekMessage (&msg, m_hWnd, WM_CLICK_DETECTED, WM_CLICK_DETECTED, PM_REMOVE)) {
	}

	//if (IsMatrix ()) 
	{
		if (m_bLangMenuOpen) {
			TRACEF (_T ("OnClick: ignore: m_bLangMenuOpen: ") + ToString (m_bLangMenuOpen));
			return 0;
		}
	}

	if (!IsMatrix ()) 
		return 0;

	if (!m_hide.m_bHidden)
		return 0;

	if (m_bIgnoreNextClick) {
		TRACEF (_T ("ignored: ") + ToString (pt.x) + _T (", ") + ToString (pt.y));
		m_bIgnoreNextClick = false;
		return 0;
	}

	HWND hwnd = (HWND)wParam;

	#ifdef _DEBUG
	{ CString str; str.Format (_T ("0x%p [%d]: %s"), hwnd, ::GetWindowLong (hwnd, GWL_ID), GetClassName (hwnd)); TRACEF (str); }
	#endif

	if (!hwnd)
		hwnd = ::WindowFromPoint (pt);

	#ifdef _DEBUG
	{ CString str; str.Format (_T ("0x%p [%d]: %s"), hwnd, ::GetWindowLong (hwnd, GWL_ID), GetClassName (hwnd)); TRACEF (str); }
	#endif

	if (hwnd) {
		#ifdef _DEBUG
		//{ CString str; str.Format (_T ("WindowFromPoint: 0x%p: %s"), hwnd, GetClassName (hwnd)); TRACEF (str); }
		#endif

		if (HWND hwndParent = ::GetParent (hwnd)) {
			#ifdef _DEBUG
			//{ CString str; str.Format (_T ("GetParent: 0x%p: %s"), hwndParent, GetClassName (hwndParent)); TRACEF (str); }
			#endif

			for (HWND hwndChild = ::GetWindow (hwndParent, GW_CHILD); hwndChild != NULL; hwndChild = ::GetWindow (hwndChild, GW_HWNDNEXT)) {
				DWORD dw = ::GetWindowLong (hwndChild, GWL_STYLE);
				CString strClass = GetClassName (hwndChild);
				CRect rc (0, 0, 0, 0);

				#ifdef _DEBUG
				{ CString str; str.Format (_T ("[%d] %s"), ::GetWindowLong (hwndChild, GWL_ID), GetClassName (hwndChild)); TRACEF (str); }
				{ CString str; str.Format (_T ("[%d] %s"), ::GetWindowLong (::GetParent (hwndChild), GWL_ID), GetClassName (::GetParent (hwndChild))); TRACEF (str); }
				#endif

				if (dw & WS_VISIBLE) {
					if (!strClass.CompareNoCase (_T ("Button"))) {
						if (dw & BS_GROUPBOX) 
							continue;
					}
					else if (!strClass.CompareNoCase (_T ("SysTabControl32"))) {
						continue;
					}

					::GetWindowRect (hwndChild, rc);
					v.SetAt (hwndChild, rc);

					/*
					if (!strClass.CompareNoCase (_T ("Edit")) && !GetClassName (::GetParent (hwndChild)).CompareNoCase (_T ("ComboBox"))) {
						v.RemoveKey (hwndChild);
						v.SetAt (::GetParent (hwndChild), rc);
					}
					*/
				}


				#ifdef _DEBUG
				//{ CString str; str.Format (_T ("[%d] %s [%s], 0x%p: %d, %d, %d, %d"), ::GetWindowLong (hwndChild, GWL_ID), GetClassName (hwndChild), GetText (hwndChild), hwnd, rc.left, rc.top, rc.Width (), rc.Height ()); TRACEF (str); }
				#endif
			}
		}
	}

	for (POSITION pos = v.GetStartPosition (); pos; ) {
		HWND hwnd = NULL;
		CRect rc (0, 0, 0, 0);
		
		v.GetNextAssoc (pos, hwnd, rc);

		#ifdef _DEBUG
		//{ CString str; str.Format (_T ("%s [%s], 0x%p: %d, %d, %d, %d"), GetClassName (hwnd), GetText (hwnd), hwnd, rc.left, rc.top, rc.Width (), rc.Height ()); TRACEF (str); }
		#endif 

		if (rc.PtInRect (pt)) {
			hwndHit = hwnd;
			break;
		}
	}

	#ifdef _DEBUG
	{ CString str; str.Format (_T ("0x%p [%d]: %s"), hwndHit, ::GetWindowLong (hwndHit, GWL_ID), GetClassName (hwndHit)); TRACEF (str); }
	#endif

	if (wParam)
		hwndHit = (HWND)wParam;

	//if (HWND hwnd = ::WindowFromPoint (pt)) {
	if (HWND hwnd = hwndHit) {
		if (m_hWnd == hwnd || ::IsChild (m_hWnd, hwnd) || m_hWnd == CPreview::GetMainWnd (hwnd)) {
			TRACEF (_T ("ignored: ") + ToString (pt.x) + _T (", ") + ToString (pt.y));

			return WM_CLICK_DETECTED;
		}

		CString strClass = GetClassName (hwnd);

		#ifdef _DEBUG
		{ CString str; str.Format (_T ("0x%p [%d]: %s"), hwnd, ::GetWindowLong (hwnd, GWL_ID), strClass); TRACEF (str); }
		#endif

		if (Find (m_vInputClassesExclude, strClass) == -1) {
			if (Find (m_vInputClasses, strClass) != -1) {
				bShow = true;
			}

			if (!strClass.CompareNoCase (_T ("ComboBox"))) {
				DWORD dw = ::GetWindowLong (hwnd, GWL_STYLE);

				if (!(dw & CBS_DROPDOWNLIST)) 
					if (!::SendMessage (hwnd, CB_GETDROPPEDSTATE, 0, 0))
						bShow = true;
			}
			else if (!strClass.CompareNoCase (_T ("Edit"))) {
				DWORD dw = ::GetWindowLong (hwnd, GWL_STYLE);

				TRACEF (GetText (hwnd));

				if (dw & ES_READONLY)
					bShow = false;
				else {
					DWORD dw = 0;
					CPoint ptClient = pt;

					::ScreenToClient (hwnd, &ptClient);

					::SendMessageTimeout (hwnd, EM_CHARFROMPOS, 0, MAKELPARAM (ptClient.x, ptClient.y), SMTO_BLOCK | SMTO_ABORTIFHUNG, 500, &dw);
					dwCaret = dw;
					TRACEF (ToString (LOWORD (dw)) + _T (", ") + ToString (HIWORD (dw)));
				}
			}
			/*
			else if (!strClass.CompareNoCase (_T ("SysDateTimePick32"))) {
				SYSTEMTIME tm;
				DWORD dw = 0;

				ZeroMemory (&tm, sizeof (tm));

				::SendMessageTimeout (hwnd, DTM_GETSYSTEMTIME, 0, (LPARAM)&tm, SMTO_BLOCK | SMTO_ABORTIFHUNG, 500, &dw);
				TRACEF (CTime (tm).Format (_T ("%c")));
			}
			*/

			if (bShow) {
				//m_wndPreview.SetWindow (hwnd, pt);
				m_target.SetTarget (hwnd);
				m_hwndRestore = hwnd;
			}
		}
	}

	if (bShow) {
		{
			CRect rc (0, 0, 0, 0);

			GetWindowRect (rc);
			::SetWindowPos (m_hWnd, NULL, rc.left, rc.top, rc.Width () - 1, rc.Height (), SWP_NOZORDER | SWP_NOACTIVATE);
			OnShow ();
			::SetWindowPos (m_hWnd, NULL, rc.left, rc.top, rc.Width (), rc.Height (), SWP_NOZORDER | SWP_NOACTIVATE);
		}

		if (m_hwndRestore) {
			if (HWND hwnd = FindKbWin ()) {
				//::PostMessage (hwnd, WM_KBWINSETTEXT, (WPARAM)m_hwndRestore, 0);
				ULONG lResult = ::SendMessage (hwnd, WM_KBWINSETTEXT, (WPARAM)m_hwndRestore, 0);
				m_bPassword = lResult == 2 ? true : false;
				//::PostMessage (hwnd, WM_KBWINSETCARET, dwCaret, 0);
			}
			else { 
				CString str; 
				
				str.Format (_T ("%s(%d): FindKbWin failed\n"), _T (__FILE__), __LINE__); 
				::OutputDebugString (str); 
			}
		}

		/* TODO: rem
		{
			HWND hwnd = m_target.GetTarget (); 
			bool bMove = false;

			while ((hwnd = ::GetParent (hwnd)) && !bMove) {
				TCHAR sz [128] = { 0 }; 
				
				::GetClassName (hwnd, sz, ARRAYSIZE (sz)); 
				TRACEF (sz); 
				
				//bMove = (::GetWindowLong (hwnd, GWL_STYLE) & WS_POPUP) ? true : false;
				bMove = _tcscmp (_T ("#32770"), sz) == 0 ? true : false;
			}

			if (bMove) {
				if (HWND hwnd = ::GetParent (m_target.GetTarget ())) {
					bMove = false;
					const int nTries = ::GetSystemMetrics (SM_CYSCREEN) / 10;

					m_hwndRestore = hwnd;
					::GetWindowRect (hwnd, m_rcRestore);
					TRACEF (GetClassName (hwnd) + _T (" [") + GetClassName (m_target.GetTarget ()) + _T ("]"));

					for (int i = 0; i < nTries; i++) {
						CRect rcKeyboard (0, 0, 0, 0);
						CRect rcParent (0, 0, 0, 0);
						CRect rcCtrl (0, 0, 0, 0);

						GetWindowRect (rcKeyboard);
						::GetWindowRect (hwnd, rcParent);
						::GetWindowRect (m_target.GetTarget (), rcCtrl);
						rcCtrl.InflateRect (10, 10);

						if (!rcKeyboard.IntersectRect (rcKeyboard, rcCtrl)) 
							break;
						else {
							if (!i) {
								::ShowWindow (hwnd, SW_HIDE);
								::LockWindowUpdate (hwnd);

								/*
								if (HINSTANCE hLib = LoadLibrary (_T ("User32.dll"))) {
									if (lpfnAnimateWindow lp = (lpfnAnimateWindow)::GetProcAddress (hLib, "AnimateWindow")) {
										(* lp) (hwnd, 200, AW_HIDE| AW_CENTER);
									}
								}
								* /

								bMove = true;
							}

							::SetWindowPos (hwnd, NULL, rcParent.left, rcParent.top - 10, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
						}
					}

					if (bMove) {
						/*
						if (HINSTANCE hLib = LoadLibrary (_T ("User32.dll"))) {
							if (lpfnAnimateWindow lp = (lpfnAnimateWindow)::GetProcAddress (hLib, "AnimateWindow")) {
								(* lp) (hwnd, 200, AW_ACTIVATE | AW_CENTER);
							}
						}
						* /

						::LockWindowUpdate (NULL);
						::ShowWindow (hwnd, SW_SHOW);
					}
					else {
						::SetWindowPos (m_hwndRestore, NULL, m_rcRestore.left, m_rcRestore.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
						m_hwndRestore = NULL;
					}
				}
			}
		}
		*/
	}
	else {
		if (!m_hide.m_bHidden)
			OnHide ();
	}

	return WM_CLICK_DETECTED;
}

LRESULT CKeyboardDlg::OnRightClick (WPARAM wParam, LPARAM lParam)
{
	::SwapMouseButton (FALSE);
	TRACEF ("SwapMouseButton (FALSE)");

	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_MOUSE)) {
		p->SetBitmap (m_bmpMouseLeft);
		p->SetCheck (0);
	}

	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_CHARMAP)) 
		p->SetBitmap (m_bmpLanguage);

	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_5_4)) 
		p->SetBitmap (NULL);

	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_7_4)) 
		p->SetBitmap (NULL);

	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_9_4)) 
		p->SetBitmap (NULL);

	OnShift ();

	return WM_RIGHT_CLICK_DETECTED;
}

void CKeyboardDlg::OnCharMap ()
{
	if (!m_hCharMapThread) {
		DWORD dw;

		m_hCharMapThread = ::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0, (LPTHREAD_START_ROUTINE)CharMapFunc, (LPVOID)this, 0, &dw);
		ASSERT (m_hCharMapThread);
	}
}

ULONG CALLBACK CKeyboardDlg::CharMapFunc (LPVOID lpData)
{
	ASSERT (lpData);

	CKeyboardDlg & dlg = * (CKeyboardDlg *)lpData;
	CString strApp = _tgetenv (_T ("SystemRoot")) + CString (_T ("\\System32\\charmap.exe"));
	CString strINI = _tgetenv (_T ("SystemRoot")) + CString (_T ("\\win.ini"));
	//CString strFont = dlg.GetElement ().GetFontName ();
	CString strCmdLine = strApp;
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [MAX_PATH];

	_tcsncpy (szCmdLine, strCmdLine, MAX_PATH);
    memset (&pi, 0, sizeof(pi));
    memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	VERIFY (::WritePrivateProfileString (_T ("MSUCE"), _T ("Advanced"), _T ("1"),		strINI));
	VERIFY (::WritePrivateProfileString (_T ("MSUCE"), _T ("CodePage"), _T ("Unicode"), strINI));
	//VERIFY (::WritePrivateProfileString (_T ("MSUCE"), _T ("Font"),		strFont,		strINI));

	TRACEF (szCmdLine);
	BOOL bResult = ::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi); 

	if (CWnd * p = dlg.GetDlgItem (BTN_CHARMAP))
		p->EnableWindow (FALSE);

	if (!bResult) {
		dlg.MessageBox (LoadString (IDS_FAILEDTOLAUNCHCHARMAP));
	}
	else {
		bool bMore;
		
		TRACEF (_T ("begin edit"));

		do {
			DWORD dwExitCode = 0;
			
			if (!GetExitCodeProcess (pi.hProcess, &dwExitCode))
				bMore = false;
			else if (!::IsWindow (dlg.m_hWnd))
				bMore = false;
			else {
				bMore = (dwExitCode == STILL_ACTIVE);
				WaitForSingleObjectEx(pi.hProcess, 3000, TRUE);
				TRACEF (_T ("still waiting"));
			}
		}
		while (bMore);

		TRACEF (_T ("edit complete"));
	}

	dlg.m_hCharMapThread = NULL;

	if (CWnd * p = dlg.GetDlgItem (BTN_CHARMAP))
		p->EnableWindow (TRUE);

	return 0;
}

LRESULT CKeyboardDlg::OnTrayNotify (WPARAM wParam, LPARAM lParam)
{
	//TRACEF (ToString (lParam));

	switch (lParam) {
	case WM_LBUTTONDOWN:
//	case WM_LBUTTONDBLCLK:
		m_hide.m_bUserOpened = true;
		OnShow ();
		break;
	case WM_RBUTTONDOWN:
		{
			CMenu menu;
			CPoint pt (0, 0);
			
			::GetCursorPos (&pt);
			menu.LoadMenu (IDM_SYSTRAY);
			
			if (CMenu * pMenu = menu.GetSubMenu (0)) {
				SETFOREGROUNDWINDOW (m_hWnd);
				ULONG lTrack = ::TrackPopupMenu (pMenu->m_hMenu, TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RETURNCMD, pt.x, pt.y, 0, m_hWnd, NULL);
				
				if (lTrack == ID_EXIT) 
					OnFileClose ();
			}
		}
		break;
	}

	return 0;
}

void CKeyboardDlg::OnShow ()
{
	bool bAnimated = false;
	HWND hwndText = NULL;
	
	if (IsMatrix ()) 
		m_hide.m_bHidden = false;

	if (m_hide.m_bAnimate) {
		if (HINSTANCE hLib = LoadLibrary (_T ("User32.dll"))) {
			if (lpfnAnimateWindow lp = (lpfnAnimateWindow)::GetProcAddress (hLib, "AnimateWindow")) {
				bAnimated = (* lp) (m_hWnd, 200, AW_ACTIVATE | AW_SLIDE | AW_VER_NEGATIVE) ? true : false;			

				if (bAnimated) {
//					REPAINT (m_wndPreview);
				}
			}
		}
	}

	if (!bAnimated) 
		ShowWindow (SW_SHOWNOACTIVATE);
	
	if (IsMatrix ()) 
		StartKbWin ();

	if (m_hide.m_bDocked)
		Dock ();

	m_hide.m_bHidden = false;
	::PostMessage (HWND_BROADCAST, WM_KEYBOARDCHANGE, KBCHANGETYPE_SHOWSTATE, m_hide.m_bHidden ? KBCHANGETYPE_SHOWSTATE_HIDE : KBCHANGETYPE_SHOWSTATE_SHOW);
}

void CKeyboardDlg::OnHide ()
{
	bool bDock = m_bDock;
	bool bAnimated = false;
	
	m_hide.m_bHidden = true;
	Undock ();

	if (m_hide.m_nHide++ > 0) {
		if (m_hide.m_bAnimate) {
			if (HINSTANCE hLib = LoadLibrary (_T ("User32.dll"))) {
				if (lpfnAnimateWindow lp = (lpfnAnimateWindow)::GetProcAddress (hLib, "AnimateWindow")) {
					bAnimated = (* lp) (m_hWnd, 200, AW_HIDE | AW_SLIDE | AW_VER_POSITIVE) ? true : false;			
				}
			}
		}
	}
	
	if (!bAnimated) 
		ShowWindow (SW_HIDE);

	m_hide.m_bDocked	= bDock;

	if (m_hwndRestore) {
		// TODO: rem //::SetWindowPos (m_hwndRestore, NULL, m_rcRestore.left, m_rcRestore.top, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		m_hwndRestore = NULL;
	}

	::PostMessage (HWND_BROADCAST, WM_KEYBOARDCHANGE, KBCHANGETYPE_SHOWSTATE, m_hide.m_bHidden ? KBCHANGETYPE_SHOWSTATE_HIDE : KBCHANGETYPE_SHOWSTATE_SHOW);
	StartKbMonitor ();
}

void CKeyboardDlg::OnFileClose() 
{
	if (m_hCharMapThread) {
	}

	::Shell_NotifyIcon (NIM_DELETE, &m_nid);

	if (theApp.m_hHookLib) {
		if (LPUNHOOKPROC lpUnhook = (LPUNHOOKPROC)::GetProcAddress (theApp.m_hHookLib, UTILSPROC_UNHOOK)) {
			int nResult = (* lpUnhook) (m_hWnd);
		}

		::FreeLibrary (theApp.m_hHookLib);
		theApp.m_hHookLib = NULL;
	}

	if (!m_bDock)
		SaveState ();	
	else {
		CRect rc;
		int nExpanded = 0;

		//if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_EXPAND)) 
		//	nExpanded = p->GetCheck ();

		GetWindowRect (rc);
		theApp.WriteProfileInt (_T ("CKeyboardDlg\\WindowState"), nExpanded == 1 ? _T ("bottom ex") : _T ("bottom"), rc.Height ());

		{ CString str; str.Format (_T ("OnFileClose: %d"), rc.Height ()); TRACEF (str); }
	}

	if (m_bStartKbMonitor) 
		StartKbMonitor ();
	else {
		if (HWND hwnd = FindKeyboardMonitor ()) {
			DWORD dwResult = 0;
			LRESULT lResult = ::SendMessageTimeout (hwnd, WM_SHUTDOWNAPP, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 2000, &dwResult);
		}
	}

	Undock ();
	CDialog::EndDialog (IDOK);
}

LRESULT CKeyboardDlg::OnShutdown (WPARAM wParam, LPARAM lParam)
{
	m_bStartKbMonitor = wParam /* == 1 */ ? false : true;
	OnFileClose ();

	if ((wParam & SHUTDOWN_CONTROL) || (!wParam)) {
		CStringArray v;
		CString str = ::GetCommandLine ();
		
		TokenizeCmdLine (str, v);

		if (v.GetSize ()) {
			CString strExe = v [0];
			
			str.Delete (0, strExe.GetLength ());
			FoxjetDatabase::WriteProfileString (HKEY_CURRENT_USER,
				_T ("Software\\FoxJet\\Translator\\Cmd line"), 
				_T ("Keyboard"), str); // for Translator
		}
	}

	return WM_SHUTDOWNKEYBOARD;
}

void CKeyboardDlg::OnParentNotify(UINT message, LPARAM lParam) 
{
	/*
	CString str;
	str.Format (_T ("%d, %d"), message, lParam);
	TRACEF (str);		
	*/

	CDialog::OnParentNotify(message, lParam);
}

LRESULT CKeyboardDlg::OnNcHitTest(CPoint point) 
{
	return HTNOWHERE;
	/*
	UINT nResult = CDialog::OnNcHitTest(point);
	
	return nResult;
	*/
}

void CKeyboardDlg::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu) 
{
	pPopupMenu->ModifyMenu (IDM_DOCK, MF_BYCOMMAND, IDM_DOCK, LoadString (m_bDock ? IDS_UNDOCK : IDS_DOCK));
	pPopupMenu->ModifyMenu (IDM_NUMPAD, MF_BYCOMMAND, IDM_NUMPAD, LoadString (m_bNumpad ? IDS_HIDENUMPAD: IDS_SHOWNUMPAD));
	pPopupMenu->SetDefaultItem (IDM_DOCK);
	pPopupMenu->ModifyMenu (SC_CLOSE, MF_BYCOMMAND, SC_CLOSE, LoadString (IDS_HIDE));

	CDialog::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
}

void CKeyboardDlg::OnNcRButtonDown(UINT nHitTest, CPoint point) 
{
	if (nHitTest == HTCAPTION) 
		return;

	CDialog::OnNcRButtonDown(nHitTest, point);
}

void CKeyboardDlg::OnNcRButtonUp(UINT nHitTest, CPoint point) 
{
	if (nHitTest == HTCAPTION) {
		CMenu menu;

		menu.LoadMenu (IDM_CONTEXTMENU);
		
		if (CMenu * pMenu = menu.GetSubMenu (0)) {
			SETFOREGROUNDWINDOW (m_hWnd);
			OnInitMenuPopup (pMenu, 0, FALSE);
			pMenu->TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON, point.x, point.y, this);
		}

		return;
	}

	
	CDialog::OnNcRButtonUp(nHitTest, point);
}

void CKeyboardDlg::OnSysKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CString str; str.Format (_T ("OnSysKeyDown: nChar: %d, nRepCnt: %d, nFlags: %d"), nChar, nRepCnt, nFlags); TRACEF (str);		
	
	CDialog::OnSysKeyDown(nChar, nRepCnt, nFlags);
}

void CKeyboardDlg::OnEsc()
{
	if (IsMatrix ()) {
		if (HWND hwnd = FindKbWin ()) {
			::SendMessage (hwnd, WM_KBWINSETTEXT, (WPARAM)m_hwndRestore, 2);
			::PostMessage (hwnd, WM_SHUTDOWNAPP, 0, 0);
		}

		OnClose ();
		m_bIgnoreNextClick = true;
	}
	else
		OnKey (BTN_1_1);
}

void CKeyboardDlg::OnExpand()
{
	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_EXPAND)) {
		p->SetCheck (0);

		if (IsMatrix ()) {
			if (HWND hwnd = FindKbWin ()) {
				::SendMessage (hwnd, WM_KBWINSETTEXT, (WPARAM)m_hwndRestore, 1);
				::PostMessage (hwnd, WM_SHUTDOWNAPP, 0, 0);
			}
		}

		OnClose ();
	}
}

void CKeyboardDlg::CheckIDs()
{
	bool bFailed = false;
	
	{
		for (int x = 0; x < ::nButtonColumns; x++) {
			for (int y = 0; y < ::nRows; y++) {
				int n = ::nButtons [y][x];

				if (n > 0) {
					for (int i = 0; i < ::nButtonColumns; i++) {
						for (int j = 0; j < ::nRows; j++) {
							if (i != x && y != j) {
								if (n == (int)nButtons [j][i]) {
									CString str;

									str.Format (_T ("::nButtons [%d][%d]: %d is duplicated at [%d][%d]"), y, x, n, j, i); 
									TRACEF (str);
									bFailed = true;
								}
							}
						}
					}
				}
			}
		}
	}

	{
		for (int x = 0; x < ::nKeypadColumns; x++) {
			for (int y = 0; y < ::nRows; y++) {
				int n = ::nKeypad [y][x];

				if (n > 0) {
					for (int i = 0; i < ::nKeypadColumns; i++) {
						for (int j = 0; j < ::nRows; j++) {
							if (i != x && y != j) {
								if (n == (int)nKeypad [j][i]) {
									CString str;

									str.Format (_T ("::nKeypad [%d][%d]: %d is duplicated at [%d][%d]"), y, x, n, j, i); 
									TRACEF (str);
									bFailed = true;
								}
							}
						}
					}
				}
			}
		}
	}

//	ASSERT (bFailed == false);
}

void CKeyboardDlg::CreateShortcut ()
{
	/*
	TCHAR sz [MAX_PATH] = { 0 };

	if (::SHGetSpecialFolderPath (NULL, sz, CSIDL_DESKTOP, TRUE)) {
		CString strDir = GetHomeDir ();
		CString strLink = CString (sz) + _T ("\\Start.lnk");

		if (::GetFileAttributes (strLink) == -1) {
			CString strFile = strDir + _T ("\\WinKey.exe");

			if (::GetFileAttributes (strFile) != -1) {
				CString strIconFile = strFile;
				CString strDesc = LoadString (IDS_WINKEY);
				CString strCmdLine;

				VERIFY (SUCCEEDED (::CreateShortcut (strFile, strCmdLine, strLink, strDesc, SW_SHOWNORMAL, strDir, strIconFile, 0)));
			}
		}
	}
	*/
}

void CKeyboardDlg::StartKbMonitor()
{
	if (!IsMatrix ()) {
		STARTUPINFO si;     
		PROCESS_INFORMATION pi; 
		TCHAR szCmdLine [MAX_PATH];
		CString strCmdLine = FoxjetDatabase::GetHomeDir () + _T ("\\KbMonitor.exe");

		//if (IsMatrix ())
		//	strCmdLine += _T (" /matrix");

		_tcsncpy (szCmdLine, strCmdLine, MAX_PATH);
		memset (&pi, 0, sizeof(pi));
		memset (&si, 0, sizeof(si));
		si.cb = sizeof(si);     
		si.dwFlags = STARTF_USESHOWWINDOW;     
		si.wShowWindow = SW_SHOW;     

		#ifndef _DEBUG
		TRACEF (_T (" ************** TODO **************** "));
		TRACEF (szCmdLine);
		VERIFY (::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)); 
		::Sleep (500);
		#endif
	}
}

HWND CKeyboardDlg::StartKbWin()
{
	HWND hwndResult = NULL;

	if (!m_hwndRestore) {
		TRACEF (_T ("StartKbWin failed: m_hwndRestore: NULL"));
	}
	else {
		TCHAR szCmdLine [MAX_PATH];
		CString strCmdLine; 
		
		strCmdLine.Format (_T ("%s\\%s /rc=%d,%d,%d,%d"), 
			FoxjetDatabase::GetHomeDir (), 
			_T ("KbWin.exe"),
			m_rcKbWin.left,
			m_rcKbWin.top,
			m_rcKbWin.Width (),
			m_rcKbWin.Height ());

		_tcsncpy (szCmdLine, strCmdLine, MAX_PATH);
		memset (&m_piKbWin, 0, sizeof(m_piKbWin));
		memset (&m_siKbWin, 0, sizeof(m_siKbWin));
		m_siKbWin.cb = sizeof (m_siKbWin);     
		m_siKbWin.dwFlags = STARTF_USESHOWWINDOW;
		m_siKbWin.wShowWindow = SW_SHOW;

		TRACEF (szCmdLine);
		
		if (!::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &m_siKbWin, &m_piKbWin))
			MessageBox (_T ("CreateProcess failed: ") + strCmdLine);
		else {
			::WaitForInputIdle (m_piKbWin.hProcess, 1000);

			for (int i = 0; i < 30; i++) {
				if (HWND hwnd = FindKbWin ()) {
					hwndResult = hwnd;
					break;
				}

				::Sleep (100);
			}
		}
	}

	return hwndResult;
}

/* TODO: rem
ULONG CALLBACK CKeyboardDlg::ThreadFunc (LPVOID lpData)
{
	CKeyboardDlg & dlg = * (CKeyboardDlg *)lpData;

	while (1) {
		DWORD dwWait = ::WaitForSingleObject (dlg.m_hExit, 500);

		if (dwWait == WAIT_OBJECT_0)
			break;
		bool bExit = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, _T ("Software\\FoxJet\\BoxWriter ELITE"), _T ("InstallerRunning"), 0) ? true : false;

		if (bExit) {
			dlg.m_hThread = NULL;
			dlg.OnFileClose ();
			return 0;
		}
	}

	return 0;
}
*/

void CKeyboardDlg::OnDestroy() 
{
	/* TODO: rem
	if (m_hThread) {
		::SetEvent (m_hExit);
		VERIFY (::WaitForSingleObject (m_hThread, 2000) == WAIT_OBJECT_0);	
	}
	*/

	CDialog::OnDestroy();
}

BOOL CKeyboardDlg::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	//{ CString str; str.Format (_T ("OnCommand: %d, %d"), wParam, lParam); TRACEF (str);	} 

	ULONG lCmdID = LOWORD (wParam);
	
	for (int i = 0; i < ::vMenu.GetSize (); i++) {
		if (lCmdID == ::vMenu [i].m_nCmdID) {
			CString str = ::vMenu [i].m_str;

			str = m_mapLang [(std::wstring)(LPCTSTR)str].c_str ();

			CString strFile = GetHomeDir () + _T ("\\Keyboard_") + str + _T (".txt");

			if (::GetFileAttributes (strFile) != -1) 
				ReadConfig (strFile);
			else {
				for (int j = 0; j < ARRAYSIZE (m_map); j++) {
					m_map [j].m_nID			= ::defKeys [j].m_nID;
					m_map [j].m_c			= ::defKeys [j].m_c;
					m_map [j].m_cShift		= ::defKeys [j].m_cShift;
					m_map [j].m_nVK			= ::defKeys [j].m_nVK;
					m_map [j].m_str			= CString (m_map [j].m_c);
					m_map [j].m_strShift	= CString (m_map [j].m_cShift);
				}
			}

			CheckUserKeys ();
			OnShift ();
			
			//if (IsMatrix ()) 
			{
				m_bLangMenuOpen = false;
				TRACEF (_T ("m_bLangMenuOpen: ") + ToString (m_bLangMenuOpen));
				::BringWindowToTop (FindKbWin ());
			}

			break;
		}
	}

	if (!IsMatrix () && m_target.GetTarget ()) 
		::BringWindowToTop (m_target.GetTarget ());

	return CDialog::OnCommand(wParam, lParam);
}

void CKeyboardDlg::CheckUserKeys ()
{
	for (int i = 0; i < ARRAYSIZE (m_map); i++) {
		if (!m_map [i].m_nVK && m_map [i].m_c != ' ') {
			ASSERT (m_map [i].m_c);
			ASSERT (m_map [i].m_cShift);
		}
	}
}

void CKeyboardDlg::ReadConfig (const CString & strFile)
{
	bool bSetFont = false;
	TRACEF (strFile);
	struct
	{
		LPCTSTR m_lpsz;
		UINT m_nID;
	} 
	static const keymap [] = 
	{
		{ _T ("Esc"),		BTN_1_1,				},
		{ _T ("Tab"),		BTN_1_3,				},
		{ _T ("Caps"),		BTN_CAPS,				},
		{ _T ("Shift1"),	BTN_SHIFT1,				},
		{ _T ("Shift2"),	BTN_SHIFT2,				},
		{ _T ("Shift"),		BTN_SHIFT1,				},
		{ _T ("Shift"),		BTN_SHIFT2,				},
		{ _T ("Ctrl"),		BTN_CTRL1,				},
		{ _T ("Alt"),		BTN_ALT1,				},
		{ _T ("Space"),		BTN_3_6,				},
		{ _T ("Back"),		BTN_14_2,				},
		{ _T ("Enter"),		BTN_13_4,				},
		{ _T ("Pg Up"),		BTN_KEYPAD_PAGEUP,		},
		{ _T ("Pg Dn"),		BTN_KEYPAD_PAGEDOWN,	},
		{ _T ("Del"),		BTN_KEYPAD_DEL,			},
	};

	TRACER (strFile);

	m_bExtendedChars = false;

	{
		ASSERT (ARRAYSIZE (m_map) == ARRAYSIZE (::defKeys));

		for (int i = 0; i < ARRAYSIZE (m_map); i++) {
			m_map [i].m_nID				= ::defKeys [i].m_nID;
			m_map [i].m_c				= ::defKeys [i].m_c;
			m_map [i].m_cShift			= ::defKeys [i].m_cShift;
			m_map [i].m_cShiftAltL		= ::defKeys [i].m_cShiftAltL;
			m_map [i].m_cShiftAltU		= ::defKeys [i].m_cShiftAltU;
			m_map [i].m_strShiftAltL	= ::defKeys [i].m_strShiftAltL;
			m_map [i].m_strShiftAltU	= ::defKeys [i].m_strShiftAltU;
			m_map [i].m_nVK				= ::defKeys [i].m_nVK;
		}

		CArray <UINT, UINT> v;
		
		for (int y = 0; y < ARRAYSIZE (::nButtons); y++) {
			int n = GetRowCount (::nButtons [y]);

			for (int x = 0; x < n; x++) 
				if (::nButtons [y][x] > 0)
					v.Add (::nButtons [y][x]);
		}
		
		for (int y = 0; y < ARRAYSIZE (::nKeypad); y++) {
			int n = GetRowCount (::nKeypad [y]);

			for (int x = 0; x < n; x++) 
				if (::nKeypad [y][x] > 0)
					v.Add (::nKeypad [y][x]);
		}

		for (int y = 0; y < v.GetSize (); y++) {
			int nID = v [y];
			int nIndex = nID - BTN_1_1;
			CKey * pKey = new CKey ();
			DWORD dwStyle = WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_MULTILINE;

			switch (nID) {
			case BTN_WINKEY:
			case BTN_5_4:
			case BTN_7_4:
			case BTN_9_4:
			case BTN_ARROW_UP:
			case BTN_ARROW_DOWN:
			case BTN_ARROW_RIGHT:
			case BTN_ARROW_LEFT:
			case BTN_7_1:
			case BTN_8_1:
			case BTN_9_1:
			case BTN_10_1:
			case BTN_1_1:
				dwStyle = BS_AUTOCHECKBOX | BS_BITMAP | BS_PUSHLIKE | BS_MULTILINE | WS_VISIBLE;
			}

			m_vKeys.Add (pKey);
			BOOL bCreate = pKey->Create (_T (""), dwStyle, CRect (0, 0, 0, 0), this, nID);
			ASSERT (bCreate);

			pKey->SetRoundButtonStyle (&m_styleDefault);
			pKey->SetFont (m_pfntKey);

			switch (nID) {
			case BTN_1_1:				pKey->SetWindowText (LoadString (IDS_ESC));			break;
			case BTN_2_1:				pKey->SetWindowText (LoadString (IDS_F1));			break;
			case BTN_3_1:				pKey->SetWindowText (LoadString (IDS_F2));			break;
			case BTN_4_1:				pKey->SetWindowText (LoadString (IDS_F3));			break;
			case BTN_5_1:				pKey->SetWindowText (LoadString (IDS_F4));			break;
			case BTN_6_1:				pKey->SetWindowText (LoadString (IDS_F5));			break;
			case BTN_7_1:				pKey->SetWindowText (LoadString (IDS_F6));			
										pKey->SetBitmap (m_bmpArrowLeft);					break;
			case BTN_8_1:				pKey->SetWindowText (LoadString (IDS_F7));	
										pKey->SetBitmap (m_bmpArrowRight);					break;
			case BTN_9_1:				pKey->SetWindowText (LoadString (IDS_F8));	
										pKey->SetBitmap (m_bmpArrowUp);						break;
			case BTN_10_1:				pKey->SetWindowText (LoadString (IDS_F9));	
										pKey->SetBitmap (m_bmpArrowDown);					break;
			case BTN_11_1:				pKey->SetWindowText (LoadString (IDS_F10));			break;
			case BTN_12_1:				pKey->SetWindowText (LoadString (IDS_F11));			break;
			case BTN_13_1:				pKey->SetWindowText (LoadString (IDS_F12));			break;
			case BTN_3_6:				pKey->SetWindowText (LoadString (IDS_SPACE));		break;
			case BTN_14_2:				pKey->SetWindowText (LoadString (IDS_BACK));		break;
			case BTN_1_3:				pKey->SetWindowText (LoadString (IDS_TAB));			break;
			case BTN_13_4:				pKey->SetWindowText (LoadString (IDS_ENTER));		break;
			case BTN_KEYPAD_SYSREQ:		pKey->SetWindowText (LoadString (IDS_SYSREQ));		break;
			case BTN_KEYPAD_SCROLLLOCK:	pKey->SetWindowText (LoadString (IDS_SCROLLLOCK));	break;
			case BTN_KEYPAD_BREAK:		pKey->SetWindowText (LoadString (IDS_BREAK));		break;
			case BTN_KEYPAD_PAGEUP:		pKey->SetWindowText (LoadString (IDS_PAGEUP));		break;
			case BTN_KEYPAD_NUMLOCK:	pKey->SetWindowText (LoadString (IDS_NUMLOCK));		break;
			case BTN_KEYPAD_PAGEDOWN:	pKey->SetWindowText (LoadString (IDS_PAGEDOWN));	break;
			case BTN_KEYPAD_ENTER:		pKey->SetWindowText (LoadString (IDS_ENTER));		break;
			case BTN_KEYPAD_BACK:		pKey->SetWindowText (LoadString (IDS_BACK));		break;
			case BTN_KEYPAD_INSERT:		pKey->SetWindowText (LoadString (IDS_INSERT));		break;
			case BTN_KEYPAD_HOME:		pKey->SetWindowText (LoadString (IDS_HOME));		break;
			case BTN_KEYPAD_DEL:		pKey->SetWindowText (LoadString (IDS_DEL));			break;
			case BTN_KEYPAD_END:		pKey->SetWindowText (LoadString (IDS_END));			break;
			case BTN_KEYPAD_EQUAL:		pKey->SetWindowText (_T ("="));						break;
			case BTN_KEYPAD_LPAREN:		pKey->SetWindowText (_T ("("));						break;
			case BTN_KEYPAD_RPAREN:		pKey->SetWindowText (_T (")"));						break;
			case BTN_KEYPAD_DIV:		pKey->SetWindowText (_T ("/"));						break;
			case BTN_KEYPAD_MULT:		pKey->SetWindowText (_T ("*"));						break;
			case BTN_KEYPAD_MINUS:		pKey->SetWindowText (_T ("-"));						break;
			case BTN_KEYPAD_PERIOD:		pKey->SetWindowText (_T ("."));						break;
			case BTN_KEYPAD_PLUS:		pKey->SetWindowText (_T ("+"));						break;
			case BTN_WINKEY:			
				pKey->SetWindowText (_T (""));
				pKey->SetBitmap (m_bmpWinKey);
				pKey->SetCheckButton (true);
				break;
			case BTN_ARROW_UP:			pKey->SetBitmap (m_bmpArrowUp);						break;
			case BTN_ARROW_DOWN:		pKey->SetBitmap (m_bmpArrowDown);					break;
			case BTN_ARROW_RIGHT:		pKey->SetBitmap (m_bmpArrowRight);					break;
			case BTN_ARROW_LEFT:		pKey->SetBitmap (m_bmpArrowLeft);					break;
			}
		}

		for (int j = 0; j < ARRAYSIZE (keymap); j++) 
			if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (keymap [j].m_nID)) 
				p->SetWindowText (keymap [j].m_lpsz);
	}

	if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
		//TCHAR sz [256];
		bool bMore = true;
		CStringArray v;
		WORD w = 0;

		fread (&w, sizeof (w), 1, fp); 
		
		if (w != 0xFEFF) {
			::MessageBox (NULL, LoadString (IDS_NOTUNICODE) + strFile, LoadString (IDS_KEYBOARD), MB_ICONERROR | MB_SYSTEMMODAL);
			ASSERT (0);
			fclose (fp);
			return;
		}

		{
			CString str;

			do
			{
				w = 0;
				fread (&w, sizeof (w), 1, fp);
				//BYTE n [2] = { LOBYTE (w), HIBYTE (w) };
				//TCHAR c = n [0] ? (TCHAR)w : (TCHAR)n [0];

				//::OutputDebugString (CString (c));

				if (w == 0x0D || w == 0x0A) {
					if (str.GetLength ()) {
						str.Replace(_T("\\n"), _T("\n"));
						str.Replace(_T("\\r"), _T("\r"));
						v.Add (str);
						str.Empty ();
					}
				}
				else
					str += (TCHAR)w;
			}
			while (!feof (fp));
		}

		fclose (fp);
		m_strConfigFile = strFile;

		for (int i = 0; i < v.GetSize (); i++) {
			bool bLangString = false;
			CStringArray vKey;

			FromString (v [i], vKey);

			CString strIndex = GetParam (vKey, 0);

			strIndex.TrimLeft ();
			strIndex.TrimRight ();

			for (int j = 0; j < ARRAYSIZE (keymap); j++) {
				if (!strIndex.CompareNoCase (keymap [j].m_lpsz)) {
					if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (keymap [j].m_nID)) {
						CString str = GetParam (vKey, 1);

						p->SetWindowText (str);
					}

					bLangString = true;
				}
			}

			if (bLangString)
				continue;

			if (!strIndex.CompareNoCase (_T ("Font"))) {
				CString strFont = GetParam (vKey, 1);
				LOGFONT lf;

				memset (&lf, 0, sizeof (lf));

				ASSERT (m_pfntKey);
				VERIFY (m_pfntKey->GetLogFont (&lf));

				delete m_pfntKey;
				m_pfntKey = new CFont ();

				_tcscpy (lf.lfFaceName, strFont);
				VERIFY (m_pfntKey->CreateFontIndirect (&lf));
				TRACEF (strFont);

				//SaveFont (m_pfntKey, _T ("Normal"));

				bSetFont = true;
			}
			else {
				if (strIndex.GetLength ()) {
					TCHAR c = strIndex [0];
					const CString str1 = GetParam (vKey, 1);
					const CString str2 = GetParam (vKey, 2);
					const CString str3 = GetParam (vKey, 3);
					const CString str4 = GetParam (vKey, 4);

					if (str3.GetLength () || str4.GetLength ())
						m_bExtendedChars = true;

					/*
					#ifdef _DEBUG
					CString strTmp;

					strTmp.Format (_T ("{%s,%s,%s}"), 
						FormatString (CString (c)), 
						FormatString (str1), 
						FormatString (str2));
		
					if (v [i].Compare (strTmp) != 0) {
						TRACEF (v [i] + _T (" --> ") + strTmp);
						ASSERT (0);
					}
					#endif
					*/

					bool bFound = false;

					for (int j = 0; !bFound && j < ARRAYSIZE (m_map); j++) {
						if (::defKeys [j].m_c == c && ::defKeys [j].m_nVK == 0) {
							KEYSTRUCT key = ::defKeys [j];

							//memcpy (&key, &m_map [j], sizeof (key));
							key.m_c				= str1.GetLength () ? str1 [0] : 0;
							key.m_cShift		= str2.GetLength () ? str2 [0] : 0;
							key.m_cShiftAltL	= str3.GetLength () ? str3 [0] : 0;
							key.m_cShiftAltU	= str4.GetLength () ? str3 [0] : 0;

							#ifdef _DEBUG
							if (m_map [j].m_c			!= key.m_c || 
								m_map [j].m_cShift		!= key.m_cShift ||
								m_map [j].m_cShiftAltL	!= key.m_cShiftAltL ||
								m_map [j].m_cShiftAltU	!= key.m_cShiftAltU) 
							{
								CString strTmp;
								strTmp.Format (_T ("[%c] %d, %d --> %d, %d"), 
									::defKeys [j].m_c, 
									m_map [j].m_c,	m_map [j].m_cShift, 
									key.m_c,		key.m_cShift);
								TRACEF (strTmp);
								int nBreak = 0;
							}
							#endif
							
							m_map [j].m_c			= key.m_c;
							m_map [j].m_cShift		= key.m_cShift;
							m_map [j].m_cShiftAltL	= key.m_cShiftAltL;
							m_map [j].m_cShiftAltU	= key.m_cShiftAltU;


							#ifdef _DEBUG
							if (!m_map [j].m_cShift) {
								TRACEF (v [i]);
								TRACEF (ToString (i) + _T (" : ") + CString (m_map [j].m_c		));
								TRACEF (ToString (i) + _T (" : ") + CString (m_map [j].m_cShift	));
								TRACEF (ToString (i) + _T (" : ") + CString (m_map [j].m_cShiftAltL));
								TRACEF (ToString (i) + _T (" : ") + CString (m_map [j].m_cShiftAltU));
								int nBreak = 0;
								ASSERT (0);
							}
							#endif //_DEBUG

							ASSERT (m_map [j].m_c);
							ASSERT (m_map [j].m_cShift);

							if (str1.GetLength () >= 1)
								m_map [j].m_str = str1;
							
							if (str2.GetLength () >= 1)
								m_map [j].m_strShift = str2;

							if (str3.GetLength () >= 1)
								m_map [j].m_strShiftAltL = str3;

							if (str4.GetLength () >= 1)
								m_map [j].m_strShiftAltU = str4;

							bFound = true;

							break;
						}
					}

					ASSERT (bFound);
				}
			}
		}
	}

	if (IsMatrix ()) {
		if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_1_1)) {
			p->SetBitmap (m_bmpEsc);
			p->SetWindowText (_T (""));
		}
	}

	if (bSetFont) {
		Resize ();
	}

//	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (BTN_SHIFT2))
//		p->EnableWindow (m_bExtendedChars);
}

void CKeyboardDlg::WriteDefConfig ()
{ 
	if (FILE * fp = _tfopen (GetHomeDir () + _T ("\\Keyboard_en.txt"), _T ("wb"))) {
		const WORD w = 0xFEFF;

		fwrite (&w, sizeof (w), 1, fp); // Unicode header

		for (int i = 0; i < ARRAYSIZE (::defKeys); i++) {
			if (::defKeys [i].m_nVK == 0) {
				CString str;

				str.Format (_T ("{%s,%s,%s}\r\n"),
					FormatString (CString (::defKeys [i].m_c)),
					FormatString (CString (::defKeys [i].m_c)),
					FormatString (CString (::defKeys [i].m_cShift)));
				TRACEF (str);

				fwrite ((LPVOID)(LPCTSTR)str, str.GetLength () * sizeof (TCHAR), 1, fp);
			}
		}

		fclose (fp);
	}
}

void CKeyboardDlg::LoadInputClasses ()
{
	FromString (theApp.GetProfileString (_T ("CKeyboardDlg"), _T ("input classes"), m_strWndClasses), m_vInputClasses);
	FromString (theApp.GetProfileString (_T ("CKeyboardDlg"), _T ("excluded classes"), m_strWndClassesExclude), m_vInputClassesExclude);

	for (int i = m_vInputClasses.GetSize () - 1; i >= 0; i--) {
		if (Find (m_vInputClassesExclude, m_vInputClasses [i]) != -1)
			m_vInputClasses.RemoveAt (i);
	}

	#ifdef _DEBUG
	for (int i = 0; i < m_vInputClasses.GetSize (); i++)
		TRACEF (_T ("auto:    ") + m_vInputClasses [i]);
	for (int i = 0; i < m_vInputClassesExclude.GetSize (); i++)
		TRACEF (_T ("exclude: ") + m_vInputClassesExclude [i]);
	#endif

	theApp.WriteProfileString (_T ("CKeyboardDlg"), _T ("input classes"), ToString (m_vInputClasses));
}

void CKeyboardDlg::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	if (lpMeasureItemStruct->CtlType == ODT_MENU) {
		lpMeasureItemStruct->itemWidth = 200;
		lpMeasureItemStruct->itemHeight = 50;
	}
	
	CDialog::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CKeyboardDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	using namespace Color;
	using namespace FoxjetCommon;

	DRAWITEMSTRUCT dis;
	
	memcpy (&dis, lpDrawItemStruct, sizeof (dis));

	if (dis.CtlType == ODT_MENU) {
		bool bSelected = (dis.itemState & (ODS_SELECTED | ODS_FOCUS)) ? false : true;
		CString str;
		UINT nBitmapID = -1;
		CRect rc = dis.rcItem;
		CDC dc;
		MEASUREITEMSTRUCT m;
		const CSize sizeBitmap (32, 32);

		memset (&m, 0, sizeof (m));

		for (int i = 0; i < ::vMenu.GetSize (); i++) {
			if (::vMenu [i].m_nCmdID == dis.itemID) {
				str			= ::vMenu [i].m_str;
				nBitmapID	= ::vMenu [i].m_nBitmapID;
				//TRACEF (str + _T (": ") + FoxjetDatabase::ToString ((int)nBitmapID));
				break;
			}
		}

		dc.Attach (dis.hDC);
		COLORREF rgbSetTextColor = dc.SetTextColor (bSelected ? Color::rgbCOLOR_WINDOWTEXT : Color::rgbCOLOR_HIGHLIGHTTEXT);
		int nSetBkMode = dc.SetBkMode (TRANSPARENT);
		dc.FillRect (rc, &CBrush (bSelected ? Color::rgbCOLOR_MENU : Color::rgbCOLOR_HIGHLIGHT));
		dc.DrawEdge (rc, EDGE_SUNKEN, BF_LEFT | BF_RIGHT | BF_TOP | BF_BOTTOM | BF_ADJUST);
		rc.left += sizeBitmap.cy + 5; 

		if (nBitmapID != -1) {
			CBitmap bmp;

			if (bmp.LoadBitmap (nBitmapID)) {
				CDC dcMem;
				DIBSECTION ds;

				memset (&ds, 0, sizeof (ds));
				bmp.GetObject (sizeof (ds), &ds);
				int x = 0;
				int y = rc.top + (rc.Height () - sizeBitmap.cy) / 2;

				dcMem.CreateCompatibleDC (&dc);
				CBitmap * pBitmap = dcMem.SelectObject (&bmp);
				COLORREF rgb = dcMem.GetPixel (0, 0);
				::TransparentBlt (dc, x, y, sizeBitmap.cx, sizeBitmap.cy, dcMem, 0, 0, ds.dsBm.bmWidth, ds.dsBm.bmHeight, rgb);
				dcMem.SelectObject (pBitmap);
			}
		}

		//{ CString strDebug; strDebug.Format (_T ("[%d] %s"), dis.itemID, str); TRACEF (strDebug); }
		dc.DrawText (str, rc, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_EXPANDTABS);
		dc.SetTextColor (rgbSetTextColor);
		dc.SetBkMode (nSetBkMode);
		dc.Detach ();
	}
	else
		CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CKeyboardDlg::OnChangeColors ()
{
	CArray <UINT, UINT> v;
	
	for (int y = 0; y < ARRAYSIZE (::nButtons); y++) {
		int n = GetRowCount (::nButtons [y]);

		for (int x = 0; x < n; x++) 
			if (::nButtons [y][x] > 0)
				v.Add (::nButtons [y][x]);
	}
	
	for (int y = 0; y < ARRAYSIZE (::nKeypad); y++) {
		int n = GetRowCount (::nKeypad [y]);

		for (int x = 0; x < n; x++) 
			if (::nKeypad [y][x] > 0)
				v.Add (::nKeypad [y][x]);
	}

	for (int i = 0; i < v.GetSize (); i++) {
		if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (v [i])) {
			COLORREF rgb = CFontDlg::GetFontColor ();
			tColorScheme tColor = { rgb, rgb, rgb, rgb, rgb };
			FoxjetUtils::tButtonStyle tStyle;

			m_styleDefault.GetButtonStyle (&tStyle);
			tStyle.m_tColorFace.m_tEnabled = CFontDlg::GetButtonColor ();
			m_styleDefault.SetButtonStyle (&tStyle);

			p->SetTextColor (&tColor);
			p->SetFont (m_pfntKey);
			p->SetRoundButtonStyle (&m_styleDefault);
		}
	}
}

void CKeyboardDlg::OnEditLayout ()
{
	CLayoutDlg dlg (this);
	
	dlg.DoModal ();
	OnShift ();
}

void CKeyboardDlg::OnF1() 
{
	OnMenuF1 ();
}

static BOOL CALLBACK EnumThreadWndProc (HWND hwnd, LPARAM lParam)
{
	CArray <HWND, HWND> & v = * (CArray <HWND, HWND> *)lParam;

	DWORD dw = ::GetWindowLong (hwnd, GWL_USERDATA);

	if (dw == 5376550)
		v.Add (hwnd);

	return TRUE;
}
  
HWND CKeyboardDlg::FindKbWin()
{
	CArray <HWND, HWND> v;

	//#define DEBUG_FINDKBWIN

	#ifdef DEBUG_FINDKBWIN
	{ CString str; str.Format (_T ("%s(%d): m_piKbWin.dwThreadId: 0x%p\n"), _T (__FILE__), __LINE__, m_piKbWin.dwThreadId); ::OutputDebugString (str); }
	#endif

	if (m_piKbWin.dwThreadId) {
		#ifdef DEBUG_FINDKBWIN
		{ CString str; str.Format (_T ("%s(%d): EnumThreadWindows\n"), _T (__FILE__), __LINE__); ::OutputDebugString (str); }
		#endif

		::EnumThreadWindows (m_piKbWin.dwThreadId, EnumThreadWndProc, (LPARAM)&v);
	}
	else {
		#ifdef DEBUG_FINDKBWIN
		{ CString str; str.Format (_T ("%s(%d): EnumWindows\n"), _T (__FILE__), __LINE__); ::OutputDebugString (str); }
		#endif

		::EnumWindows (EnumThreadWndProc, (LPARAM)&v);
	}

	#ifdef DEBUG_FINDKBWIN
	{ CString str; str.Format (_T ("%s(%d): v.GetSize (): %d\n"), _T (__FILE__), __LINE__, v.GetSize ()); ::OutputDebugString (str); }
	#endif

	if (!v.GetSize ()) {
		#ifdef DEBUG_FINDKBWIN
		{ CString str; str.Format (_T ("%s(%d): EnumWindows\n"), _T (__FILE__), __LINE__); ::OutputDebugString (str); }
		#endif

		::EnumWindows (EnumThreadWndProc, (LPARAM)&v);

		#ifdef DEBUG_FINDKBWIN
		{ CString str; str.Format (_T ("%s(%d): v.GetSize (): %d\n"), _T (__FILE__), __LINE__, v.GetSize ()); ::OutputDebugString (str); }
		#endif
	}

	for (int i = 0; i < v.GetSize (); i++) {
		HWND hwnd = v [i];

		#ifdef DEBUG_FINDKBWIN
		{
			TCHAR sz [512] = { 0 };
			CString str;

			::GetWindowText (hwnd, sz, ARRAYSIZE (sz) - 1);
			str.Format (_T ("%s(%d): 0x%p: %s [%s]\n"), _T (__FILE__), __LINE__, hwnd, GetClassName (hwnd), sz);
			::OutputDebugString (str);
		}
		#endif

		DWORD dwResult = 0;
		LRESULT lResult = ::SendMessageTimeout (hwnd, WM_ISKBWINRUNNING, 0, 0, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dwResult);

		#ifdef DEBUG_FINDKBWIN
		{ CString str; str.Format (_T ("%s(%d): SendMessageTimeout: %d\n"), _T (__FILE__), __LINE__, lResult); ::OutputDebugString (str); }
		#endif

		//if (lResult) 
		{
			#ifdef DEBUG_FINDKBWIN
			{ CString str; str.Format (_T ("%s(%d): dwResult: 0x%p\n"), _T (__FILE__), __LINE__, dwResult); ::OutputDebugString (str); }
			#endif

			if (dwResult)
				return (HWND)dwResult;
		}
	}

	#ifdef DEBUG_FINDKBWIN
	{ CString str; str.Format (_T ("%s(%d): return NULL\n\n"), _T (__FILE__), __LINE__); ::OutputDebugString (str); }
	#endif

	return NULL;
}

LRESULT CKeyboardDlg::OnKbWinCreated (WPARAM wParam, LPARAM lParam)
{
	#ifdef _DEBUG
	{ CString str; str.Format (_T ("OnKbWinCreated: 0x%p, 0x%p"), wParam, lParam); TRACEF (str); }
	#endif

	m_dwKbWinStyle = (DWORD)lParam;
	return 0;
}


void CKeyboardDlg::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);
}


void CKeyboardDlg::Resize () 
{
	CRect rc (0, 0, 0, 0);

	GetWindowRect (rc);
	::SetWindowPos (m_hWnd, NULL, rc.left, rc.top, rc.Width () - 1, rc.Height (), SWP_NOZORDER | SWP_SHOWWINDOW | SWP_NOACTIVATE);
	::SetWindowPos (m_hWnd, NULL, rc.left, rc.top, rc.Width (), rc.Height (), SWP_NOZORDER | SWP_SHOWWINDOW | SWP_NOACTIVATE);
}

LRESULT CKeyboardDlg::OnSetCheck (WPARAM wParam, LPARAM lParam)
{
	if (CRoundButton2 * p = (CRoundButton2 *)GetDlgItem (wParam)) 
		p->SetCheck (lParam ? true : false);

	return 0;
}
