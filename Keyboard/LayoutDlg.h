#if !defined(AFX_LAYOUTDLG_H__ED50403E_3EE7_4952_8A5A_4A6C76AD1889__INCLUDED_)
#define AFX_LAYOUTDLG_H__ED50403E_3EE7_4952_8A5A_4A6C76AD1889__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LayoutDlg.h : header file
//

#include "ListCtrlImp.h"

/////////////////////////////////////////////////////////////////////////////
// CLayoutDlg dialog
class CKeyItem : public ItiLibrary::ListCtrlImp::CItem
{
public:

	virtual CString GetDispText (int nColumn) const;

	CString m_strKey;
	CString m_str [4];
};

class CLayoutDlg : public CDialog
{
// Construction
public:

	CLayoutDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLayoutDlg)
	enum { IDD = IDD_LAYOUT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLayoutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;

	// Generated message map functions
	//{{AFX_MSG(CLayoutDlg)
	afx_msg void OnEdit();
	afx_msg void OnSelchangeFile();
	afx_msg void OnDblclkChar(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LAYOUTDLG_H__ED50403E_3EE7_4952_8A5A_4A6C76AD1889__INCLUDED_)
