#if !defined(AFX_PREVIEW_H__F4704E1F_F5AB_471B_B794_EE3877BA62EC__INCLUDED_)
#define AFX_PREVIEW_H__F4704E1F_F5AB_471B_B794_EE3877BA62EC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Preview.h : header file
//

class CKeyboardDlg;

/////////////////////////////////////////////////////////////////////////////
// CPreview window

class CPreview : public CScrollView
{
// Construction
public:
	CPreview (CKeyboardDlg * pParent);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPreview)
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetWindow (HWND hwnd, const CPoint & ptClick);
	virtual ~CPreview();

	void Capture (HWND hwnd);
	bool Create (CWnd * pParent, const CRect & rc, UINT nID);
	afx_msg BOOL CPreview::OnEraseBkgnd(CDC* pDC);
	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
	virtual void OnDraw(CDC* pDC);
	static HWND GetMainWnd (HWND hwndChild);

	//CEdit m_wndTxt;

	// Generated message map functions
protected:
	afx_msg LRESULT OnInputSent(WPARAM wParam, LPARAM lParam);
	void MoveEditCtrl();
	CPoint * m_ptDrag;
	CPoint * m_ptScroll;
	CKeyboardDlg * m_pKeyboard;
	CRect m_rcClick;
	HWND m_hwndCapture;
	HBITMAP m_hbmpScreen;
	HBITMAP m_hbmpCapture;

	//{{AFX_MSG(CPreview)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PREVIEW_H__F4704E1F_F5AB_471B_B794_EE3877BA62EC__INCLUDED_)
