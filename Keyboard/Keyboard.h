// Keyboard.h : main header file for the KEYBOARD application
//

#if !defined(AFX_KEYBOARD_H__4ECE7727_3DA2_49CA_BC70_FE56EA302A9F__INCLUDED_)
#define AFX_KEYBOARD_H__4ECE7727_3DA2_49CA_BC70_FE56EA302A9F__INCLUDED_

#define WS_EX_NOACTIVATE 0x08000000

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "Database.h"

/////////////////////////////////////////////////////////////////////////////
// CKeyboardApp:
// See Keyboard.cpp for the implementation of this class
//

class CKeyboardApp : public CWinApp
{
public:
	CKeyboardApp();

	HMODULE m_hHookLib;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKeyboardApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

	bool IsAutoHide () const { return m_bAutoHide; }
	bool IsEditMode () const { return m_bEdit; }
	FoxjetDatabase::CInstance m_instance;

// Implementation

protected:
	bool m_bAutoHide;
	bool m_bEdit;

	//{{AFX_MSG(CKeyboardApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KEYBOARD_H__4ECE7727_3DA2_49CA_BC70_FE56EA302A9F__INCLUDED_)
