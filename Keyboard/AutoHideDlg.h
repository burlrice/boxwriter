#if !defined(AFX_AUTOHIDEDLG_H__D152CA05_CCAF_4540_8E7D_889211781F2B__INCLUDED_)
#define AFX_AUTOHIDEDLG_H__D152CA05_CCAF_4540_8E7D_889211781F2B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AutoHideDlg.h : header file
//

#include "ListCtrlImp.h"

/////////////////////////////////////////////////////////////////////////////
// CAutoHideDlg dialog

class CAutoHideDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CAutoHideDlg)

// Construction
public:
	CAutoHideDlg();
	~CAutoHideDlg();

// Dialog Data
	//{{AFX_DATA(CAutoHideDlg)
	enum { IDD = IDD_AUTOHIDE };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CAutoHideDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	class CClassItem : public ItiLibrary::ListCtrlImp::CItem
	{
	public:
		CClassItem (const CString & strName, bool bEnabled) : m_strName (strName), m_bEnabled (bEnabled) { }

		virtual CString GetDispText (int nColumn) const;

		CString m_strName;
		bool m_bEnabled;
	};

	ItiLibrary::ListCtrlImp::CListCtrlImp m_lv;
	HBITMAP m_hbmpCheck;

	// Generated message map functions
	//{{AFX_MSG(CAutoHideDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkClasses(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnToggle();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUTOHIDEDLG_H__D152CA05_CCAF_4540_8E7D_889211781F2B__INCLUDED_)
