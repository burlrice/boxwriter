// Key.cpp : implementation file
//

#include "stdafx.h"
#include "Keyboard.h"
#include "KeyboardDlg.h"
#include "Key.h"
#include "Debug.h"
#include "Parse.h"
#include "WinMsg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace FoxjetUtils;

extern CKeyboardApp theApp;

#define ID_KEYDOWN	1

/////////////////////////////////////////////////////////////////////////////
// CKey

CKey::CKey()
:	m_bDown (false),
	m_pParent (NULL)
{
}

CKey::~CKey()
{
}


BEGIN_MESSAGE_MAP(CKey, CRoundButton2)
	//{{AFX_MSG_MAP(CKey)
	ON_CONTROL_REFLECT(BN_CLICKED, OnClicked)
	ON_WM_TIMER()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_NCRBUTTONUP()
	ON_WM_RBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKey message handlers

void CKey::OnClicked() 
{
	ULONG lID = GetWindowLong (m_hWnd, GWL_ID);
	int nDiff = (int)(((double)(clock () - m_clock_tLast) / CLOCKS_PER_SEC) * 1000.0);
	//{ CString str; str.Format (_T ("OnClicked: ID: %d [%.03f]"), lID, (double)(nDiff / 1000.0)); TRACEF (str); }
}

BOOL CKey::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CRoundButton2::OnCommand(wParam, lParam);
}

LRESULT CKey::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	ULONG lID = GetWindowLong (m_hWnd, GWL_ID);

	switch (message) {
	case WM_LBUTTONDOWN:	
		if (m_pParent)
			m_pParent->m_bUncheck = false;
		m_clock_tLast = clock ();
		SetTimer (ID_KEYDOWN, 50, NULL);

		//{ CString str; str.Format (_T ("WM_LBUTTONDOWN: ID: %d"), lID); TRACEF (str); }
		if (lID != BTN_WINKEY)
			OnKeyDown ();

		m_bDown = true;
		break;
	case WM_LBUTTONUP:
		KillTimer (ID_KEYDOWN);

		OnKeyUp ();

		if (lID == BTN_WINKEY) {
			int nDiff = (int)(((double)(clock () - m_clock_tLast) / CLOCKS_PER_SEC) * 1000.0);

			TRACEF (ToString (nDiff));
			TRACEF (_T ("check: ") + ToString (((CRoundButton2 *)(m_pParent->GetDlgItem (BTN_WINKEY)))->GetCheck ()));
			TRACEF (_T ("check: ") + ToString (GetCheck ()));

			if (GetCheck () == 0) {
				if (nDiff < 1000) {
					m_pParent->OnKey (lID);
					m_pParent->PostMessage (WM_SETCHECK, lID, 0);
				}
			}
		}

		//{ CString str; str.Format (_T ("WM_LBUTTONUP: ID: %d"), lID); TRACEF (str); }
		m_bDown = false;
		
		if (m_pParent)
			if (m_pParent->m_bUncheck)
				m_pParent->Uncheck ();
		break;
	}

	return CRoundButton2::WindowProc(message, wParam, lParam);
}

void CKey::OnTimer(UINT nIDEvent) 
{
	ULONG lID = GetWindowLong (m_hWnd, GWL_ID);

	switch (lID) {
	case BTN_WINKEY:
	case BTN_CAPS:
	case BTN_SHIFT1:
	case BTN_SHIFT2:
	case BTN_CTRL1:
	case BTN_ALT1:
	case BTN_ALT2:
	case BTN_CTRL2:
	case BTN_KEYPAD_NUMLOCK:
	case BTN_KEYPAD_SCROLLLOCK:
	case BTN_MOUSE:
	case BTN_CHARMAP:
	case BTN_EXPAND:
		return;
	}

	if (nIDEvent == ID_KEYDOWN) {
		if (m_bDown) {
			clock_t clock_tNow = clock ();
			int nDiff = (int)(((double)(clock_tNow - m_clock_tLast) / CLOCKS_PER_SEC) * 1000.0);

			if (nDiff >= 500) {
				if (lID == BTN_WINKEY) {
					TRACEF ("BTN_WINKEY");
				}
				else
					OnKeyDown ();
			}
		}
	}
	
	CRoundButton2::OnTimer(nIDEvent);
}

BOOL CKey::Create (LPCTSTR lpszCaption, DWORD dwStyle, const RECT& rect, CKeyboardDlg * pParentWnd, UINT nID)
{
	m_pParent = pParentWnd;
	return CRoundButton2::Create (lpszCaption, dwStyle, rect, pParentWnd, nID);
}

void CKey::SetParent (CKeyboardDlg * p)
{
	m_pParent = p;
}

void CKey::OnKeyUp()
{
	bool bEnabled = IsWindowEnabled () ? true : false;

	if (!bEnabled)
		return;

	const ULONG lID = GetWindowLong (m_hWnd, GWL_ID);

	if (m_pParent) {
		if (lID == BTN_CHARMAP) {
			m_pParent->OnLanguage ();
			return;
		}
	}
}

void CKey::OnKeyDown()
{
	bool bEnabled = IsWindowEnabled () ? true : false;

	if (!bEnabled)
		return;

	//TRACEF ("OnKeyDown");return;
	const ULONG lID = GetWindowLong (m_hWnd, GWL_ID);

	//CString str; str.Format (_T ("KeyDown: ID: %d%s"), lID, m_bDown ? _T (" [DOWN]") : _T (" ")); TRACEF (str);	

	/*
	ON_CONTROL_RANGE (BN_CLICKED, BTN_1_1, BTN_5_6, OnKey)
	ON_CONTROL_RANGE (BN_CLICKED, BTN_SHIFT1, BTN_ALT2, OnKey)
	ON_CONTROL_RANGE (BN_CLICKED, BTN_KEYPAD_NUMLOCK, BTN_ARROW_RIGHT, OnNumpad)
	*/

	if (m_pParent) {
		if (IsMatrix () && lID == BTN_1_1) {
			m_pParent->OnEsc ();
			return;
		}

		if (lID == BTN_WINKEY) { 
			if (GetCheck () == 0)
				m_pParent->OnKey (lID);

			return;
		}

		if (IsMatrix ()) {
			if (lID == BTN_13_4) {
				if (m_pParent->m_dwKbWinStyle & ES_MULTILINE) {
					if (CRoundButton2 * p = (CRoundButton2 *)m_pParent->GetDlgItem (BTN_CTRL1))
						p->SetCheck (1);

					m_pParent->OnKey (lID);
				}
				else {
					m_pParent->m_bIgnoreNextClick = true;

					if (!m_pParent->m_bPassword)
						m_pParent->OnExpand ();
					else {
						if (HWND hwnd = m_pParent->FindKbWin ())
							::PostMessage (hwnd, WM_NEXTDLGCTL, 0, 0);

						m_pParent->m_bPassword = false;
					}
				}
				return;
			}
		}

		if ((lID >= BTN_1_1 && lID <= BTN_5_6) || (lID >= BTN_SHIFT1 && lID <= BTN_ALT2))
			m_pParent->OnKey (lID);
		else if (lID > BTN_KEYPAD_NUMLOCK && lID <= BTN_ARROW_RIGHT)
			m_pParent->OnNumpad (lID);
		else if (lID == BTN_EXPAND)
			m_pParent->OnExpand ();
	}	
}

void CKey::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	if (GetCheckButton ()) {
		TRACEF ("ignore");
		CRoundButton2::OnLButtonDblClk(nFlags, point);
	}
	else
		OnKeyDown ();
}

void CKey::OnNcRButtonUp(UINT nHitTest, CPoint point) 
{
	CRoundButton2::OnNcRButtonUp(nHitTest, point);
}

void CKey::OnRButtonUp(UINT nFlags, CPoint point) 
{
	const ULONG lID = GetWindowLong (m_hWnd, GWL_ID);
	
	if (lID == BTN_5_4) {
		::PostMessage (::GetParent (m_hWnd), WM_COMMAND, MAKEWPARAM (IDM_FONT, 0), 0);
		return;
	}
	if (lID == BTN_7_4) {
		::PostMessage (::GetParent (m_hWnd), WM_COMMAND, MAKEWPARAM (IDA_F1, 0), 0);
		return;
	}
	if (lID == BTN_9_4) {
		if (theApp.IsEditMode ()) {
			::PostMessage (::GetParent (m_hWnd), WM_COMMAND, MAKEWPARAM (IDM_EDIT_LAYOUT, 0), 0);
			return;
		}
	}
	else if (lID == BTN_CHARMAP) {
		m_pParent->OnCharMap ();
		//::PostMessage (::GetParent (m_hWnd), WM_COMMAND, MAKEWPARAM (IDM_CHARMAP, 0), 0);
		return;
	}

	CRoundButton2::OnRButtonUp(nFlags, point);
}
