// kb.cpp : Defines the entry point for the DLL application.
//

#include <afx.h>
#include "stdafx.h"
#include "kb.h"
#include <windows.h>
#include <SHLWAPI.H>
#include <stdio.h>
#include <tchar.h>
#include <COMMCTRL.H>
#include "DllVer.h"
#include "AppVer.h"

#ifdef ARRAYSIZE
	#undef ARRAYSIZE
#endif

#define ARRAYSIZE(s) (sizeof ((s)) / sizeof ((s) [0]))

#ifdef _DEBUG
	#define TRACEF(s) Trace ((s), _T (__FILE__), __LINE__)
#else
	#define TRACEF(s)
#endif

void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	TCHAR sz [512] = { 0 };

	_stprintf (sz, _T ("%s(%d): "), lpszFile, lLine, lpsz);
	int nOffset = (_tcsclen (sz) + _tcsclen (lpsz));
	int nLen = max (0, (ARRAYSIZE (sz) - 2) - nOffset);
	_tcsncat (sz, lpsz, nLen);
	_tcscat (sz, _T ("\n"));
	::OutputDebugString (sz);
}


#pragma data_seg(".HKT")
HWND ghwndTarget		= NULL;
HINSTANCE hInstance		= NULL;
HHOOK ghMouseHook		= NULL;
HHOOK ghCallWndProc		= NULL;
ULONG glWinMsgRClick	= 0;
ULONG glWinMsgClick		= 0;
ULONG glWinMsgFocus		= 0;
ULONG glWinMsgNotify	= 0;
bool gbSwap				= false;
#pragma data_seg()

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			::hInstance = (HINSTANCE)hModule;
			break;
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}


long CALLBACK MouseProc (int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode == HC_ACTION) {
		switch (wParam) {
		//case WM_RBUTTONDOWN:
		case WM_LBUTTONDOWN:
		//case WM_MBUTTONDOWN:
		//case WM_LBUTTONDBLCLK:
		//case WM_MBUTTONDBLCLK:
		//case WM_RBUTTONDBLCLK:
			if (MOUSEHOOKSTRUCT * p = (MOUSEHOOKSTRUCT *)lParam) {
				if (::ghwndTarget != NULL && ::glWinMsgClick != 0) {
					#ifdef _DEBUG
					TCHAR sz [512] = { 0 };

					_stprintf (sz, _T ("PostMessage (0x%p, WM_CLICK_DETECTED, 0, [%d, %d])"), ::ghwndTarget, p->pt.x, p->pt.y);
					TRACEF (sz);
					#endif //_DEBUG

					::PostMessage (::ghwndTarget, ::glWinMsgClick, 0, MAKELPARAM (p->pt.x, p->pt.y));
				}
			}

			break;
		}

		switch (wParam) {
		case WM_RBUTTONUP:
			if (::gbSwap) {
				SwapMouseButton (FALSE);
				::gbSwap = false;
					
				if (::ghwndTarget != NULL && ::glWinMsgRClick != 0) {
					TRACEF (_T ("WM_RIGHT_CLICK_DETECTED"));
					::PostMessage (::ghwndTarget, ::glWinMsgRClick, 0, 0);
				}
			}

			break;
		}
	}

	return CallNextHookEx (::ghMouseHook, nCode, wParam, lParam);
}

long CALLBACK CallWndProc (int nCode, WPARAM wParam, LPARAM lParam)
{
	static TCHAR szLastDebug [512] = { 0 };

	if (CWPSTRUCT * p = (CWPSTRUCT *)lParam) {
		switch (p->message) {
		//case WM_ACTIVATE:
		case WM_SETFOCUS:
		//case WM_KILLFOCUS:
		//case WM_CAPTURECHANGED:
			if (::ghwndTarget != NULL && ::glWinMsgFocus != 0) 
				::PostMessage (::ghwndTarget, ::glWinMsgFocus, (WPARAM)p->message, (LPARAM)p->hwnd);
				//::SendMessage (::ghwndTarget, ::glWinMsgFocus, (WPARAM)p->message, (LPARAM)p->hwnd);

			break;
		/*
		case WM_NOTIFY:
			if (::ghwndTarget != NULL && ::glWinMsgNotify != 0) {
				if (LPNMHDR pHeader = (LPNMHDR)p->lParam) {
					if (HWND h = pHeader->hwndFrom) {
						TCHAR szClass [128] = { 0 };

						::GetClassName (h, szClass, ARRAYSIZE (szClass) - 1);

						if (!_tcsicmp (szClass, _T ("SysListView32")) || !_tcsicmp (szClass, _T ("SysTreeView32"))) {
							::PostMessage (::ghwndTarget, ::glWinMsgNotify, (WPARAM)pHeader->code, (LPARAM)pHeader->hwndFrom);
							::SendMessage (::ghwndTarget, ::glWinMsgNotify, (WPARAM)pHeader->code, (LPARAM)pHeader->hwndFrom);
						}
					}
				}
			}
			break;
		*/
		}
	}

	return CallNextHookEx (::ghCallWndProc, nCode, wParam, lParam);
}

LRESULT CALLBACK Hook (HWND hwnd, ULONG lMsgClick, ULONG lMsgRClick, ULONG lMsgFocus)
{
	::ghwndTarget		= hwnd;
	::glWinMsgClick		= lMsgClick;
	::glWinMsgRClick	= lMsgRClick;
	::glWinMsgFocus		= lMsgFocus;
	::ghMouseHook		= SetWindowsHookEx (WH_MOUSE, (HOOKPROC)MouseProc, ::hInstance, 0);
	::ghCallWndProc		= SetWindowsHookEx (WH_CALLWNDPROC, (HOOKPROC)CallWndProc, ::hInstance, 0);

	return (ULONG)::ghMouseHook;
}

LRESULT CALLBACK Unhook (HWND hwnd)
{
	LRESULT lResult = 0;
	
	lResult |= UnhookWindowsHookEx (::ghMouseHook);
	lResult |= UnhookWindowsHookEx (::ghCallWndProc);

	::ghwndTarget		= NULL;
	::glWinMsgClick		= 0;
	::glWinMsgRClick	= 0;
	::glWinMsgFocus		= 0;
	::ghMouseHook		= NULL;
	::ghCallWndProc		= NULL;

	return lResult;
}

LRESULT CALLBACK Swap ()
{
	::gbSwap = true;
	SwapMouseButton (TRUE);
	return 1;
}

void CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer)
{
	if (lpVer) 
		* lpVer = FoxjetCommon::verApp; // memcpy (lpVer, &FoxjetCommon::verApp, sizeof (FoxjetCommon::VERSIONSTRUCT));
}

HRESULT CALLBACK DllGetVersion (DLLVERSIONINFO * pdvi)
{
	FoxjetCommon::VERSIONSTRUCT ver;

	fj_GetDllVersion (&ver);

	if (pdvi) {
		pdvi->dwMajorVersion	= ver.m_nMajor;
		pdvi->dwMinorVersion	= ver.m_nMinor;
		pdvi->dwBuildNumber		= ver.m_nInternal;
		pdvi->dwPlatformID		= DLLVER_PLATFORM_WINDOWS;
	}

	return NOERROR;
}

