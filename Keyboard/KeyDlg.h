#if !defined(AFX_KEYDLG_H__F3D017F6_24CE_46D5_96F7_D09C72EF4770__INCLUDED_)
#define AFX_KEYDLG_H__F3D017F6_24CE_46D5_96F7_D09C72EF4770__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// KeyDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CKeyDlg dialog

class CKeyDlg : public CDialog
{
// Construction
public:
	CKeyDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CKeyDlg)
	enum { IDD = IDD_KEY };
	CString	m_strKey;
	CString	m_strLower;
	CString	m_strLowerExt;
	CString	m_strUpper;
	CString	m_strUpperExt;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKeyDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CKeyDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KEYDLG_H__F3D017F6_24CE_46D5_96F7_D09C72EF4770__INCLUDED_)
