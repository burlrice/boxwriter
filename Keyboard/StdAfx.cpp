// stdafx.cpp : source file that includes just the standard includes
//	Keyboard.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include "Keyboard.h"
#include "Debug.h"
#include "Database.h"

extern CKeyboardApp theApp;

CString LoadString (UINT nID)
{
	return FoxjetDatabase::LoadString (nID, _T ("Keyboard.exe"));
}

void WINAPI CALLBACK fj_GetDllVersion (FoxjetCommon::LPVERSIONSTRUCT lpVer)
{
	if (lpVer) 
		* lpVer = FoxjetCommon::verApp; // memcpy (lpVer, &FoxjetCommon::verApp, sizeof (FoxjetCommon::VERSIONSTRUCT));
}

CString GetClassName (HWND hWnd)
{
	TCHAR szClass [128] = { 0 };

	::GetClassName (hWnd, szClass, ARRAYSIZE (szClass) - 1);
	
	return szClass;
}

CString GetWndText (HWND hWnd)
{
	TCHAR szClass [128] = { 0 };

	::GetWindowText (hWnd, szClass, ARRAYSIZE (szClass) - 1);
	
	return szClass;
}
