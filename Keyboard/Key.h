#if !defined(AFX_KEY_H__ABD241DF_11CC_4060_81ED_2D2605CA9204__INCLUDED_)
#define AFX_KEY_H__ABD241DF_11CC_4060_81ED_2D2605CA9204__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Key.h : header file
//

#include "RoundButton2.h"

/////////////////////////////////////////////////////////////////////////////
// CKey window

class CKeyboardDlg;

class CKey : public FoxjetUtils::CRoundButton2
{
// Construction
public:
	CKey();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKey)
	protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CKey();

	BOOL Create( LPCTSTR lpszCaption, DWORD dwStyle, const RECT& rect, CKeyboardDlg * pParentWnd, UINT nID );
	void SetParent (CKeyboardDlg * p);
	

	// Generated message map functions
protected:

	void OnKeyDown();
	void OnKeyUp();

	//{{AFX_MSG(CKey)
	afx_msg void OnClicked();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnNcRButtonUp(UINT nHitTest, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG

	bool m_bDown;
	clock_t m_clock_tLast;
	CKeyboardDlg * m_pParent;

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KEY_H__ABD241DF_11CC_4060_81ED_2D2605CA9204__INCLUDED_)
