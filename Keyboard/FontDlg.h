#if !defined(AFX_FONTDLG_H__3A1B8085_8F66_435F_A948_23312C046DCA__INCLUDED_)
#define AFX_FONTDLG_H__3A1B8085_8F66_435F_A948_23312C046DCA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FontDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFontDlg dialog

class CFontDlg : public CDialog
{
// Construction
public:
	CFontDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFontDlg ();

// Dialog Data
	//{{AFX_DATA(CFontDlg)
	enum { IDD = IDD_FONT };
	int m_nArrow;
	//}}AFX_DATA

	LOGFONT m_lfNormal;
	LOGFONT m_lfSmall;

	static COLORREF GetFontColor ();
	static COLORREF GetButtonColor ();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFontDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	CFont * m_pfntNormal;
	CFont * m_pfntSmall;
	CFont * m_pfntArrow;

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFontDlg)
	afx_msg void OnChangenormal();
	afx_msg void OnChangesmall();
	afx_msg void OnDeltaposSize(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeSize();
	afx_msg void OnChangeFontColor();
	afx_msg void OnChangeButtonColor();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FONTDLG_H__3A1B8085_8F66_435F_A948_23312C046DCA__INCLUDED_)
