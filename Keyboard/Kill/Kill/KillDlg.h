// KillDlg.h : header file
//

#pragma once


// CKillDlg dialog
class CKillDlg : public CDialog
{
// Construction
public:
	CKillDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_KILL_DIALOG};

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	enum 
	{ 
		LANGUAGE_ENGLISH		= 1033,
		LANGUAGE_DUTCH			= 1043,
		LANGUAGE_PORTUGUESE		= 2070,
		LANGUAGE_SPANISH		= 1034,
	};

// Implementation
protected:
	HICON m_hIcon;
	int m_nLang;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);

protected:
};
