// KillDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Kill.h"
#include "KillDlg.h"
#include "..\..\..\Editor\DLL\Common\WinMsg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine);

#define TRACEF(s) Trace ((s), _T (__FILE__), __LINE__)
// CKillDlg dialog

#define ID_KILL 100

void Trace (LPCTSTR lpsz, LPCTSTR lpszFile, ULONG lLine)
{
	CString str;

	str.Format (_T ("%s(%d): %s\n"), lpszFile, lLine, lpsz);
	::OutputDebugString (str);
}

CKillDlg::CKillDlg(CWnd* pParent /*=NULL*/)
:	m_nLang (1033),
	CDialog(CKillDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CKillDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CKillDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CKillDlg message handlers

BOOL CKillDlg::OnInitDialog()
{
	CString str, strCmdLine = ::GetCommandLine ();

	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	int nIndex = strCmdLine.Find (_T ("/lang="));

	if (nIndex >= 0 && nIndex < strCmdLine.GetLength ()) {
		str = strCmdLine.Mid (nIndex);
		swscanf_s (str, _T ("/lang=%d"), &m_nLang);
	}
	
	switch (m_nLang) {
	case LANGUAGE_ENGLISH:		str.LoadString (IDS_ENGLISH_SHUTTTINGDOWN);		break;
	case LANGUAGE_SPANISH:		str.LoadString (IDS_SPANISH_SHUTTTINGDOWN);		break;
	case LANGUAGE_PORTUGUESE:	str.LoadString (IDS_PORTUGUESE_SHUTTTINGDOWN);	break;
	case LANGUAGE_DUTCH:		str.LoadString (IDS_DUTCH_SHUTTTINGDOWN);		break;
	}

	SetDlgItemText (IDS_STATUS, str);
	SetTimer (ID_KILL, 2000, NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CKillDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CKillDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


static bool IsRunning (LPCTSTR lpsz)
{
	HANDLE h = ::CreateMutex (NULL, FALSE, lpsz);
	bool bRunning = 
		GetLastError () == ERROR_ALREADY_EXISTS || 
		GetLastError () == ERROR_ACCESS_DENIED;

	::CloseHandle (h);

	return bRunning;
}

void CKillDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent) {
	case ID_KILL:
		HWND hwnd = NULL;
		const int nMax = 20;
		int i;
		DWORD dwResult = 0;

		for (i = 0; (i < nMax) && (hwnd = FindKeyboard ()); i++) {
			LRESULT lResult = ::SendMessageTimeout (hwnd, WM_SHUTDOWNAPP, SHUTDOWN_ALL, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 1, &dwResult);
			::PostMessage (hwnd, WM_COMMAND, 32778, 0);
			::Sleep (250);
		}

		LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_SHUTDOWNAPP, SHUTDOWN_ALL, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 10000, &dwResult);

		if (i >= nMax || IsRunning (ISKEYBOARDRUNNING) || IsRunning (ISKEYBOARDMONITORRUNNING)) {	
			CString strFailed, strClose;

			switch (m_nLang) {
			case LANGUAGE_ENGLISH:		strFailed.LoadString (IDS_ENGLISH_FAILED);		strClose.LoadString (IDS_ENGLISH_CLOSE);		break;
			case LANGUAGE_SPANISH:		strFailed.LoadString (IDS_SPANISH_FAILED);		strClose.LoadString (IDS_SPANISH_CLOSE);		break;
			case LANGUAGE_PORTUGUESE:	strFailed.LoadString (IDS_PORTUGUESE_FAILED);	strClose.LoadString (IDS_PORTUGUESE_CLOSE);		break;
			case LANGUAGE_DUTCH:		strFailed.LoadString (IDS_DUTCH_FAILED);		strClose.LoadString (IDS_DUTCH_CLOSE);			break;
			}

			SetDlgItemText (IDS_STATUS, strFailed);

			if (CWnd * p = GetDlgItem (IDOK)) {
				p->ShowWindow (SW_SHOW);
				p->SetWindowText (strClose);
			}

			KillTimer (nIDEvent);
		}
		else {
			CDialog::EndDialog (IDOK);
			return;
		}
	}

	CDialog::OnTimer(nIDEvent);
}

static BOOL CALLBACK EnumWindowsProc (HWND hWnd, LPARAM lParam)
{
	CArray <HWND, HWND> & v = * (CArray <HWND, HWND> *)lParam;

	/*
	if (::IsWindowVisible (hWnd)) {
		DWORD dwStyle = ::GetWindowLong (hWnd, GWL_STYLE);
		DWORD dwExStyle = ::GetWindowLong (hWnd, GWL_EXSTYLE);

		//if ((dwStyle & WS_MINIMIZEBOX) || (dwStyle & WS_MAXIMIZEBOX)) 
	*/
			v.Add (hWnd);
	//}

	return TRUE;
}

HWND CKillDlg::FindKeyboard(void)
{
	const CString strFind =  _T ("Virtual keyboard");
	CArray <HWND, HWND> v;

	::EnumWindows (EnumWindowsProc, (LPARAM)&v);

	for (int i = 0; i < v.GetSize (); i++) {
		HWND hwnd = v [i];
		TCHAR sz [512] = { 0 };

		::GetWindowText (hwnd, sz, ARRAYSIZE (sz) - 1);

		if (!_tcsncmp (sz, strFind, strFind.GetLength ()))
			return hwnd;
	}

	return NULL;
}
