//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Kill.rc
//
#define IDD_KILL_DIALOG                 102
#define IDR_MAINFRAME                   128
#define IDS_ENGLISH_SHUTTTINGDOWN       129
#define IDS_SPANISH_SHUTTTINGDOWN       130
#define IDS_PORTUGUESE_SHUTTTINGDOWN    131
#define IDS_DUTCH_SHUTTTINGDOWN         132
#define IDS_ENGLISH_FAILED              134
#define IDS_SPANISH_FAILED              135
#define IDS_PORTUGUESE_FAILED           136
#define IDS_DUTCH_FAILE4                137
#define IDS_DUTCH_FAILED                137
#define IDS_ENGLISH_CLOSE               139
#define IDS_SPANISH_CLOSE               140
#define IDS_PORTUGUESE_CLOSE            141
#define IDS_DUTCH_CLOSE                 142
#define IDS_STATUS                      1000

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
