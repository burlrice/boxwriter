// Keyboard.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Keyboard.h"
#include "KeyboardDlg.h"
#include "WinMsg.h"
#include "Debug.h"
#include "Parse.h"
#include "Registry.h"
#include "Utils.h"
#include "Database.h"

using namespace FoxjetUtils;
using namespace FoxjetCommon;
using namespace FoxjetDatabase;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKeyboardApp

BEGIN_MESSAGE_MAP(CKeyboardApp, CWinApp)
	//{{AFX_MSG_MAP(CKeyboardApp)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKeyboardApp construction

CKeyboardApp::CKeyboardApp()
:	m_hHookLib (NULL),
	m_bAutoHide (false),
	m_bEdit (false)
{
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CKeyboardApp object

CKeyboardApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CKeyboardApp initialization


BOOL CKeyboardApp::InitInstance()
{
	FoxjetDatabase::IsInstallerInvoked ();

#ifndef _DEBUG
	if (!CInstance::Elevate ())
		return FALSE;
#endif

	// Standard initialization

	bool bUserOpened = false;

	{
		const CString strDel = _T ("/DELETE=");
		CStringArray vCmdLine;

		TokenizeCmdLine (::GetCommandLine (), vCmdLine);
		FoxjetDatabase::SetHighResDisplay (true);

		for (int i = 0; i < vCmdLine.GetSize (); i++) {
			CString str = vCmdLine [i];

			if (!str.CompareNoCase (_T ("/WriteDefConfig"))) {	
				CKeyboardDlg::WriteDefConfig ();
				return TRUE;
			}

			if (!str.CompareNoCase (_T ("/EditKeyboard"))) 
				m_bEdit = true;

			if (!str.CompareNoCase (_T ("/manually_started")))
				bUserOpened = true;

			if (!str.CompareNoCase (_T ("/MATRIX")))
				FoxjetDatabase::SetHighResDisplay (false);

			if (str.GetLength () > strDel.GetLength ()) {
				if (!str.Left (strDel.GetLength ()).CompareNoCase (strDel)) {
					CString strFile = str.Mid (strDel.GetLength () + 1);
					DWORD dwSize = 256;
					TCHAR szUser [256] = { 0 };

					::DeleteFile (strFile);

					// this is a hack.  installer creates shortcuts under
					// "all users", uninstall.exe attempts to delete using current username

					::GetUserName (szUser, &dwSize);
					strFile.Replace (szUser, _T ("All Users"));
					::DeleteFile (strFile);

					return TRUE;
				}
			}
		}

		if (IsMatrix ())
			m_bAutoHide = true;
	}


	if (FoxjetDatabase::IsRunning (ISKEYBOARDRUNNING)) {	
		DWORD dwResult;

		LRESULT lResult = ::SendMessageTimeout (HWND_BROADCAST, WM_ISKEYBOARDRUNNING, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 5000, &dwResult);
		
		return FALSE; 
	}

	m_instance.Create (ISKEYBOARDRUNNING);

	SetRegistryKey (_T ("FoxJet\\Keyboard"));

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CKeyboardDlg dlg;
	m_pMainWnd = &dlg;

	if (bUserOpened)
		dlg.m_hide.m_bUserOpened = true;

	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
	}
	else if (nResponse == IDCANCEL)
	{
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


int CKeyboardApp::ExitInstance() 
{
	return CWinApp::ExitInstance();
}
