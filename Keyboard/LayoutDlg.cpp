// LayoutDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Keyboard.h"
#include "LayoutDlg.h"
#include "Database.h"
#include "Utils.h"
#include "Parse.h"
#include "KeyDlg.h"
#include "Debug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;
using namespace FoxjetDatabase;
using namespace ItiLibrary;

CString CKeyItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:		return m_strKey;
	case 1:		
	case 2:
	case 3:		
	case 4:		return m_str [nColumn - 1];
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CLayoutDlg dialog


CLayoutDlg::CLayoutDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLayoutDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLayoutDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CLayoutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLayoutDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLayoutDlg, CDialog)
	//{{AFX_MSG_MAP(CLayoutDlg)
	ON_BN_CLICKED(BTN_EDIT, OnEdit)
	ON_CBN_SELCHANGE(CB_FILE, OnSelchangeFile)
	ON_NOTIFY(NM_DBLCLK, LV_CHAR, OnDblclkChar)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLayoutDlg message handlers

void CLayoutDlg::OnEdit() 
{
	CLongArray sel = GetSelectedItems (m_lv);

	if (sel.GetSize ()) {
		if (CKeyItem * p = (CKeyItem *)m_lv.GetCtrlData (sel [0])) {
			CKeyDlg dlg (this);

			dlg.m_strKey		= p->m_strKey;
			dlg.m_strLower		= p->m_str [0];
			dlg.m_strUpper		= p->m_str [1];
			dlg.m_strLowerExt	= p->m_str [2];
			dlg.m_strUpperExt	= p->m_str [3];

			if (dlg.DoModal () == IDOK) {
				p->m_str [0] = dlg.m_strLower;
				p->m_str [1] = dlg.m_strUpper;
				p->m_str [2] = dlg.m_strLowerExt;
				p->m_str [3] = dlg.m_strUpperExt;

				m_lv.UpdateCtrlData (p);

				if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_FILE)) {
					CString strFile;

					pCB->GetLBText (pCB->GetCurSel (), strFile);
					strFile = GetHomeDir () + _T ("\\") + strFile;

					if (FILE * fp = _tfopen (strFile, _T ("wb"))) {
						const WORD w = 0xFEFF;

						fwrite (&w, sizeof (w), 1, fp); // Unicode header

						for (int i = 0; i < m_lv->GetItemCount (); i++) {
							if (CKeyItem * p = (CKeyItem *)m_lv.GetCtrlData (i)) {
								CStringArray v;

								v.Add (p->m_strKey);
								v.Add (p->m_str [0]);
								v.Add (p->m_str [1]);
								v.Add (p->m_str [2]);
								v.Add (p->m_str [3]);

								CString str = ToString (v) + _T ("\r\n");
								TRACEF (str);
								fwrite ((LPVOID)(LPCTSTR)str, str.GetLength () * sizeof (TCHAR), 1, fp);
							}
						}

						fclose (fp);
					}
				}
			}
		}
	}
}

void CLayoutDlg::OnSelchangeFile() 
{
	m_lv.DeleteCtrlData ();

	if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_FILE)) {
		CString strFile;

		pCB->GetLBText (pCB->GetCurSel (), strFile);
		strFile = GetHomeDir () + _T ("\\") + strFile;

		if (FILE * fp = _tfopen (strFile, _T ("rb"))) {
			bool bMore = true;
			CStringArray v;
			WORD w = 0;

			fread (&w, sizeof (w), 1, fp); 
			
			if (w != 0xFEFF) {
				::MessageBox (NULL, LoadString (IDS_NOTUNICODE) + strFile, LoadString (IDS_KEYBOARD), MB_ICONERROR | MB_SYSTEMMODAL);
				ASSERT (0);
				return;
			}

			{
				CString str;

				do
				{
					w = 0;
					fread (&w, sizeof (w), 1, fp);
					//BYTE n [2] = { LOBYTE (w), HIBYTE (w) };
					//TCHAR c = n [0] ? (TCHAR)w : (TCHAR)n [0];

					//::OutputDebugString (CString (c));

					if (w == 0x0D || w == 0x0A) {
						if (str.GetLength ()) {
							v.Add (str);
							str.Empty ();
						}
					}
					else
						str += (TCHAR)w;
				}
				while (!feof (fp));

				//for (int i = 0; i < v.GetSize (); i++)
				//	TRACEF (v [i]);
			}

			fclose (fp);
			
			for (int i = 0; i < v.GetSize (); i++) {
				CStringArray vKey;
				CKeyItem * p = new CKeyItem ();

				FromString (v [i], vKey);

				p->m_strKey		= GetParam (vKey, 0);
				p->m_str [0]	= GetParam (vKey, 1);
				p->m_str [1]	= GetParam (vKey, 2);
				p->m_str [2]	= GetParam (vKey, 3);
				p->m_str [3]	= GetParam (vKey, 4);

				m_lv.InsertCtrlData (p);
			}
		}
	}
}

void CLayoutDlg::OnDblclkChar(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnEdit ();	
	*pResult = 0;
}

BOOL CLayoutDlg::OnInitDialog() 
{
	using namespace ListCtrlImp;

	CArray <CColumn, CColumn> vCols;

	vCols.Add (CColumn (_T ("Key")));
	vCols.Add (CColumn (_T ("Lower")));
	vCols.Add (CColumn (_T ("Upper")));
	vCols.Add (CColumn (_T ("Ext lower")));
	vCols.Add (CColumn (_T ("Ext upper")));

	m_lv.Create (LV_CHAR, vCols, this, _T ("Software\\FoxJet\\Keyboard"));

	CDialog::OnInitDialog();

	if (CComboBox * pCB = (CComboBox *)GetDlgItem (CB_FILE)) {
		CStringArray v;
		CString strDir = GetHomeDir ();

		GetFiles (strDir, _T ("Keyboard*.txt"), v);

		for (int i = 0; i < v.GetSize (); i++) {
			CString strFile = v [i];

			strFile.Replace (strDir + _T ("\\"), _T (""));
			pCB->AddString (strFile);
		}

		pCB->SetCurSel (0);
	}

	OnSelchangeFile	();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
