// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__46C2F042_07C2_425C_ABA4_CAFDDB19A6D9__INCLUDED_)
#define AFX_STDAFX_H__46C2F042_07C2_425C_ABA4_CAFDDB19A6D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include "Database.h"

#define KEYEVENTF_SCANCODE	0x0008
#define KEYEVENTF_UNICODE	0x0004
#define IDT_ACTIVEWINDOW	100
#define IDT_CHECKSTARTMENU	101
#define IDT_STARTUP			102
#define LBL_PREVIEW			103
#define NM_SYSTRAY			(WM_APP+1)
#define WM_SENTINPUT		(WM_APP+2)
#define HOOKDLL				_T ("kb.dll")

#define AW_HOR_POSITIVE             0x00000001
#define AW_HOR_NEGATIVE             0x00000002
#define AW_VER_POSITIVE             0x00000004
#define AW_VER_NEGATIVE             0x00000008
#define AW_CENTER                   0x00000010
#define AW_HIDE                     0x00010000
#define AW_ACTIVATE                 0x00020000
#define AW_SLIDE                    0x00040000
#define AW_BLEND                    0x00080000

typedef DWORD (WINAPI* lpfnAnimateWindow) (HWND hwnd, DWORD dwTime, DWORD dwFlags);
typedef VOID (WINAPI * lpfnSwitchToThisWindow) (HWND hWnd, BOOL fAltTab);

CString LoadString (UINT nID);
CString GetClassName (HWND hWnd);
CString GetWndText (HWND hWnd);

const UINT WM_SCREENSHOT	= ::RegisterWindowMessage (_T ("Foxjet::Keyboard::WM_SCREENSHOT"));
const UINT WM_SETCHECK		= ::RegisterWindowMessage (_T ("Foxjet::Keyboard::WM_SETCHECK"));

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__46C2F042_07C2_425C_ABA4_CAFDDB19A6D9__INCLUDED_)
