# Microsoft Developer Studio Project File - Name="KbWin" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=KbWin - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "KbWin.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "KbWin.mak" CFG="KbWin - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "KbWin - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "KbWin - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "KbWin - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\..\Editor\DLL\Utils" /I "..\..\Editor\DLL\Database" /I "..\..\Editor\DLL\Debug" /I "..\..\Editor\DLL\Common" /I "..\..\CxImage\CxImage" /D "NDEBUG" /D "_UNICODE" /D "UNICODE" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 Utils.lib Database.lib Msimg32.lib /nologo /entry:"wWinMainCRTStartup " /subsystem:windows /pdb:none /machine:I386 /libpath:"..\..\Editor\DLL\Utils\Release" /libpath:"..\..\Editor\DLL\Database\Release" /libpath:"..\..\CxImage\png\Release" /libpath:"..\..\CxImage\jpeg\Release" /libpath:"..\..\CxImage\zlib\Release" /libpath:"..\..\CxImage\tiff\Release" /libpath:"..\..\CxImage\j2k\Release" /libpath:"..\..\CxImage\jbig\Release" /libpath:"..\..\CxImage\jasper\Release" /libpath:"..\..\CxImage\cximage\Release"
# Begin Custom Build - Updating $(TargetName)
TargetPath=.\Release\KbWin.exe
TargetName=KbWin
InputPath=.\Release\KbWin.exe
SOURCE="$(InputPath)"

"\Foxjet\$(TargetName).exe" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(TargetPath) \Foxjet

# End Custom Build

!ELSEIF  "$(CFG)" == "KbWin - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /vmb /GX /ZI /Od /I "..\..\Editor\DLL\Utils" /I "..\..\Editor\DLL\Database" /I "..\..\Editor\DLL\Debug" /I "..\..\Editor\DLL\Common" /I "..\..\CxImage\CxImage" /D "_DEBUG" /D "_UNICODE" /D "UNICODE" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Debug.lib Utils.lib Database.lib Msimg32.lib /nologo /entry:"wWinMainCRTStartup " /subsystem:windows /pdb:none /debug /machine:I386 /libpath:"..\..\Editor\DLL\Utils\Debug" /libpath:"..\..\Editor\DLL\Database\Debug" /libpath:"..\..\Editor\DLL\Debug\Debug" /libpath:"..\..\CxImage\png\Debug" /libpath:"..\..\CxImage\jpeg\Debug" /libpath:"..\..\CxImage\zlib\Debug" /libpath:"..\..\CxImage\tiff\Debug" /libpath:"..\..\CxImage\j2k\Debug" /libpath:"..\..\CxImage\jbig\Debug" /libpath:"..\..\CxImage\jasper\Debug" /libpath:"..\..\CxImage\cximage\Debug"
# Begin Custom Build - Updating $(TargetName)
TargetPath=.\Debug\KbWin.exe
TargetName=KbWin
InputPath=.\Debug\KbWin.exe
SOURCE="$(InputPath)"

"\Foxjet\$(TargetName).exe" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	Copy $(TargetPath) \Foxjet

# End Custom Build

!ENDIF 

# Begin Target

# Name "KbWin - Win32 Release"
# Name "KbWin - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\KbWin.cpp
# End Source File
# Begin Source File

SOURCE=.\KbWin.rc
# End Source File
# Begin Source File

SOURCE=.\KbWinDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\KbWin.h
# End Source File
# Begin Source File

SOURCE=.\KbWinDlg.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\KbWin.ico
# End Source File
# Begin Source File

SOURCE=.\res\KbWin.rc2
# End Source File
# End Group
# End Target
# End Project
