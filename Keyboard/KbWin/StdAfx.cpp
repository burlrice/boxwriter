// stdafx.cpp : source file that includes just the standard includes
//	KbWin.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

CString GetClassName (HWND hWnd)
{
	TCHAR szClass [128] = { 0 };

	::GetClassName (hWnd, szClass, ARRAYSIZE (szClass) - 1);
	
	return szClass;
}


