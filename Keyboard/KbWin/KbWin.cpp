// KbWin.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "KbWin.h"
#include "KbWinDlg.h"
#include "WinMsg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKbWinApp

BEGIN_MESSAGE_MAP(CKbWinApp, CWinApp)
	//{{AFX_MSG_MAP(CKbWinApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKbWinApp construction

CKbWinApp::CKbWinApp()
{
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CKbWinApp object

CKbWinApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CKbWinApp initialization

BOOL CKbWinApp::InitInstance()
{
	if (!FoxjetDatabase::CInstance::Elevate ())
		return FALSE;

	if (FoxjetDatabase::IsRunning (ISKBWINRUNNING))
		return FALSE;

	m_instance.Create (ISKBWINRUNNING);

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CKbWinDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
	}
	else if (nResponse == IDCANCEL)
	{
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
