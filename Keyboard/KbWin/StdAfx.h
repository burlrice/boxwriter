// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__E6BAE1B3_B85F_4E39_A9A9_281382D1B302__INCLUDED_)
#define AFX_STDAFX_H__E6BAE1B3_B85F_4E39_A9A9_281382D1B302__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#ifdef ARRAYSIZE
	#undef ARRAYSIZE
#endif

#define ARRAYSIZE(s) (sizeof ((s)) / sizeof ((s) [0]))


#define TXT_DATA                        1002
#define TXT_PASS                        1003

CString GetClassName (HWND hWnd);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__E6BAE1B3_B85F_4E39_A9A9_281382D1B302__INCLUDED_)
