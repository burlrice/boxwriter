// KbWin.h : main header file for the KBWIN application
//

#if !defined(AFX_KBWIN_H__A0E7857D_349D_4E87_ADD2_D88922BFD10D__INCLUDED_)
#define AFX_KBWIN_H__A0E7857D_349D_4E87_ADD2_D88922BFD10D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "Database.h"


/////////////////////////////////////////////////////////////////////////////
// CKbWinApp:
// See KbWin.cpp for the implementation of this class
//

class CKbWinApp : public CWinApp
{
public:
	CKbWinApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKbWinApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

protected:
	FoxjetDatabase::CInstance m_instance;

// Implementation

	//{{AFX_MSG(CKbWinApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KBWIN_H__A0E7857D_349D_4E87_ADD2_D88922BFD10D__INCLUDED_)
