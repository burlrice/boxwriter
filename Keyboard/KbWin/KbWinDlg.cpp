// KbWinDlg.cpp : implementation file
//

#include "stdafx.h"
#include <Windows.h>
#include "KbWin.h"
#include "KbWinDlg.h"
#include "WinMsg.h"
#include "Debug.h"
#include "Utils.h"
#include "Parse.h"
#include "Registry.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetDatabase;

#define TIMER_CHECKSTATE 1001

/////////////////////////////////////////////////////////////////////////////
// CKbWinDlg dialog

CKbWinDlg::CKbWinDlg(CWnd* pParent /*=NULL*/)
:	m_pTaskbar (NULL),
	m_rcText (0, 0, 100, 100),
	m_hwndUser (NULL),
	m_hwndPass (NULL),
	CDialog(CKbWinDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CKbWinDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	::CoInitialize(NULL);
	VERIFY (SUCCEEDED (::CoCreateInstance (CLSID_TaskbarList, NULL, CLSCTX_ALL, IID_ITaskbarList, (void**)&m_pTaskbar)));
}

CKbWinDlg::~CKbWinDlg ()
{
	if (m_pTaskbar) {
		m_pTaskbar->Release ();
		m_pTaskbar = NULL;
	}
}

void CKbWinDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CKbWinDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CKbWinDlg, CDialog)
	//{{AFX_MSG_MAP(CKbWinDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_WM_NCHITTEST()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
	ON_REGISTERED_MESSAGE (WM_ISKBWINRUNNING, OnIsAppRunning)
	ON_REGISTERED_MESSAGE (WM_SHUTDOWNAPP, OnShutdown)
	ON_REGISTERED_MESSAGE (WM_SHOWKBWIN, OnShowKbWin)
	ON_REGISTERED_MESSAGE (WM_KBWINSETTEXT, OnSetText)
	ON_REGISTERED_MESSAGE (WM_KBWINSETCARET, OnSetCaret)
	END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKbWinDlg message handlers

BOOL CKbWinDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	Show ();
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	SetTimer (TIMER_CHECKSTATE, 500, NULL);
	SetDlgItemText (LBL_FIELD, _T (""));

	FromString (FoxjetDatabase::GetProfileString (HKEY_CURRENT_USER, _T ("Software\\Foxjet\\Keyboard\\About\\CKeyboardDlg"), _T ("input classes"), _T ("")), m_vInputClasses);

	for (int i = 0; i < m_vInputClasses.GetSize (); i++)
		TRACEF (m_vInputClasses [i]);

	::SetWindowLong (m_hWnd, GWL_USERDATA, 5376550);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CKbWinDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CKbWinDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

LRESULT CKbWinDlg::OnIsAppRunning (WPARAM wParam, LPARAM lParam)
{
	return (LRESULT)m_hWnd;
}

LRESULT CKbWinDlg::OnShutdown (WPARAM wParam, LPARAM lParam)
{
	TRACEF (_T ("CKbWinDlg::OnShutdown"));
	CDialog::OnOK ();
	exit (0);
	return WM_SHUTDOWNAPP;
}

LRESULT CKbWinDlg::OnShowKbWin (WPARAM wParam, LPARAM lParam)
{
	ShowWindow (wParam ? SW_SHOW : SW_HIDE);

	/*
	if (wParam) {
		//::BringWindowToTop (m_hWnd);
		//::SetActiveWindow (m_hWnd);
		::SetWindowPos (m_hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
		//::SetFocus (m_hWnd);
		::SetForegroundWindow (m_hWnd);
	}
	*/

	return 1;
}

void CKbWinDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if (m_pTaskbar) {
		m_pTaskbar->DeleteTab (m_hWnd);
		m_pTaskbar->DeleteTab (::AfxGetMainWnd ()->m_hWnd);
	}
	
	const CRect rc (0, 0, cx, cy);
	CRect rcLabel = rc;
	CRect rcText = rc;

	rcLabel.right = (int)((double)rc.Width () * 0.33);
	rcText.left = rcLabel.right;
	CRect rcTitle = rcLabel;

	if (!m_fntLabel.m_hObject) {
		if (CFont * p = GetFont ()) {
			LOGFONT lf;

			ZeroMemory (&lf, sizeof (lf));
			p->GetLogFont (&lf);

			lf.lfHeight = rcText.Height () / 6;
			m_fntEdit.CreateFontIndirect (&lf);

			//lf.lfHeight = (int)(lf.lfHeight * 0.66);
			lf.lfWeight = FW_BOLD;
			m_fntLabel.CreateFontIndirect (&lf);

			if (CWnd * p = GetDlgItem (LBL_FIELD))
				p->SetFont (&m_fntLabel);
		}
	}

	if (CWnd * p = GetDlgItem (LBL_FIELD))
		p->SetWindowPos (NULL, rcLabel.left, rcLabel.top, rcLabel.Width (), rcLabel.Height (), SWP_NOZORDER);

	m_rcText = rcText;
}

void CKbWinDlg::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent == TIMER_CHECKSTATE) {
		TRACEF (_T ("TIMER_CHECKSTATE"));

		if (!FoxjetDatabase::IsRunning (ISKEYBOARDRUNNING)) {
			OnShutdown (0, 0);
		}
		else {
			if (!IsStartMenuOpen ()) {
				if (::GetForegroundWindow () != m_hWnd)
					Show ();
			}
		}
	}

	CDialog::OnTimer(nIDEvent);
}

LRESULT CKbWinDlg::OnNcHitTest(CPoint point) 
{
	LRESULT nResult = CDialog::OnNcHitTest(point);

	switch (nResult) {
	case HTCAPTION:
		nResult = HTCLIENT;
		break;
	}

	return nResult;
}

static HWND GetTopLevelOwner (HWND hwnd)
{
	for (HWND h = hwnd; h; ) {
		HWND hParent = ::GetParent (h);

		if (!hParent)
			return h;

		h = hParent;
	}

	return NULL;
}

CString CKbWinDlg::GetText (HWND hwnd)
{
	CString str;
	DWORD dwLen = 0, dwCopied = 0;

	LRESULT lResult = ::SendMessageTimeout (hwnd, WM_GETTEXTLENGTH, 0, 0, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dwLen);
	const int nLen = dwLen + 1;
	TCHAR * psz = new TCHAR [nLen];

	ZeroMemory (psz, sizeof (TCHAR) * nLen);
	lResult = ::SendMessageTimeout (hwnd, WM_GETTEXT, (WPARAM)nLen, (LPARAM)psz, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dwCopied);

	str = psz;
	delete [] psz;

	return str;
}

LRESULT CKbWinDlg::OnSetCaret (WPARAM wParam, LPARAM lParam)
{
	int x = LOWORD (wParam);
	int y = HIWORD (wParam);

	if (CEdit * p = (CEdit *)GetDlgItem (TXT_DATA))
		p->SetSel (x, x);

	return 0;
}

LRESULT CKbWinDlg::OnSetText (WPARAM wParam, LPARAM lParam)
{
	ULONG lResult = 1;

	#define DEBUG_SETTEXT

	if (HWND hwnd = (HWND)wParam) {
		{ CString str; str.Format (_T ("[%d] %s"), ::GetWindowLong (hwnd, GWL_ID), GetClassName (hwnd)); TRACEF (str); }

		if (lParam == 0) {
			CString strLabel;
			bool bPassword = (::GetWindowLong (hwnd, GWL_STYLE) & ES_PASSWORD) ? true : false;
			bool bMultiline = (::GetWindowLong (hwnd, GWL_STYLE) & ES_MULTILINE) ? true : false;
			CString strClass = GetClassName (hwnd);

			m_hwndUser = m_hwndPass = NULL;

			#ifdef DEBUG_SETTEXT
			{ CString str; str.Format (_T ("::GetWindowLong (0x%p, GWL_STYLE): %d\n"), hwnd, ::GetWindowLong (hwnd, GWL_STYLE)); ::OutputDebugString (str); }
			{ CString str; str.Format (_T ("bPassword: %d\n"), bPassword); ::OutputDebugString (str); }
			{ CString str; str.Format (_T ("bMultiline: %d\n"), bMultiline); ::OutputDebugString (str); }
			#endif
			DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER | ES_AUTOHSCROLL;

			if (!strClass.CompareNoCase (_T ("Edit")) || !strClass.CompareNoCase (_T ("RICHEDIT50"))) {
				if (bMultiline)
					dwStyle |= ES_MULTILINE;

				if (bPassword)
					dwStyle |= ES_PASSWORD;
			}

			if (HWND hwndParent = ::GetParent (hwnd)) {
				CArray <HWND, HWND> v;

				for (HWND hwndChild = ::GetWindow (hwndParent, GW_CHILD); hwndChild != NULL; hwndChild = ::GetWindow (hwndChild, GW_HWNDNEXT)) {
					v.Add (hwndChild);
				}

				/*
				for (int i = 0; i < v.GetSize (); i++) {
					CString str;

					str.Format (_T ("[%d]: %s 0x%p\n"), i, GetClassName (v [i]), ::GetWindowLong (v [i], GWL_STYLE));
					::OutputDebugString (str);
				}
				*/

				if (v.GetSize () >= 4) {
					if (!GetClassName (v [1]).CompareNoCase (_T ("Edit")) && !GetClassName (v [3]).CompareNoCase (_T ("Edit"))) { 
						DWORD dw [] = 
						{ 
							::GetWindowLong (v [0], GWL_STYLE),
							::GetWindowLong (v [1], GWL_STYLE),
							::GetWindowLong (v [2], GWL_STYLE),
							::GetWindowLong (v [3], GWL_STYLE),
						};

						if (((dw [1] & ES_PASSWORD) == 0) && (dw [3] & ES_PASSWORD)) {
							strLabel = GetText (v [0]) + _T (" / ") + GetText (v [2]);
							hwnd = m_hwndUser = v [1];
							m_hwndPass = v [3];
							lResult = 2;
						}
					}
				}
			}

			if (!m_wndData.m_hWnd) {
				DWORD dw = 0;

				::SendMessageTimeout (HWND_BROADCAST, WM_KBWINCREATED, (WPARAM)m_hWnd, (LPARAM)dwStyle, SMTO_NORMAL, 1, &dw);

				if (m_hwndUser && m_hwndPass) {
					CRect rc [2] = { m_rcText, m_rcText };

					rc [0].bottom = rc [0].Height () / 2;
					rc [1].top = rc [0].bottom;

					dwStyle &= ~ES_PASSWORD;
					m_wndData.CreateEx (WS_EX_CLIENTEDGE, _T ("EDIT"), _T (""), dwStyle, rc [0], this, TXT_DATA);
					m_wndData.SetFont (&m_fntEdit);

					dwStyle |= ES_PASSWORD;
					m_wndPassword.CreateEx (WS_EX_CLIENTEDGE, _T ("EDIT"), _T (""), dwStyle, rc [1], this, TXT_PASS);
					m_wndPassword.SetFont (&m_fntEdit);
				}
				else {
					m_wndData.CreateEx (WS_EX_CLIENTEDGE, _T ("EDIT"), _T (""), dwStyle, m_rcText, this, TXT_DATA);
					m_wndData.SetFont (&m_fntEdit);
				}

				m_wndData.SetFocus ();
			}

			SetDlgItemText (TXT_DATA, GetText (hwnd));
			m_wndData.PostMessage (EM_SETSEL, 0, -1);

			if (strLabel.IsEmpty ()) {
				if (HWND h = ::GetWindow (hwnd, GW_HWNDPREV)) {
					TCHAR sz [MAX_PATH] = { 0 };

					GetClassName (h, sz, ARRAYSIZE (sz));

					if (Find (m_vInputClasses, sz) == -1) {
						strLabel = GetText (h);

						#ifdef DEBUG_SETTEXT
						{ CString str; str.Format (_T ("%s(%d): %s [%s]\n"), _T (__FILE__), __LINE__, sz, strLabel); ::OutputDebugString (str); }
						#endif
					}
				}
			}

			if (strLabel.IsEmpty ()) {
				if (HWND hwndParent = ::GetParent (hwnd)) {
					CRect rc (0, 0, 0, 0);
					CStringArray vUp;
					CString strButton;

					SetWindowText (GetText (hwndParent));

					::GetWindowRect (hwnd, &rc);

					CPoint pt (rc.left + 10, rc.top); 

					::ScreenToClient (hwndParent, &pt);

					for ( ; pt.y > 0; pt.y--) {
						if (HWND hwndHit = ::ChildWindowFromPoint (hwndParent, pt)) {
							if (hwndHit != hwnd && ::IsWindowVisible (hwndHit)) {
								TCHAR sz [MAX_PATH] = { 0 };
								CString strInfo;
								
								GetClassName (hwndHit, sz, ARRAYSIZE (sz));
								strInfo.Format (_T ("0x%p: %s"), hwndHit, sz);
								TRACEF (_T ("(") + ToString (pt.x) + _T (", ") + ToString (pt.y) + _T ("): ") + strInfo);

								if (!_tcscmp (sz, _T ("#32770"))) 
									continue;
								else if (!_tcscmp (sz, _T ("Button"))) {
									DWORD dw = ::GetWindowLong (hwndHit, GWL_STYLE);

									if (dw & BS_GROUPBOX)
										if (strButton.IsEmpty ())
											strButton = GetText (hwndHit);
									
									continue;
								}

								if (!_tcscmp (sz, _T ("Static"))) {
									strLabel = GetText (hwndHit);
									break;
								}
								else
									break;
							}
						}
					}

					if (strLabel.IsEmpty ())
						strLabel = strButton;
				}
			}
			
			SetDlgItemText (LBL_FIELD, strLabel + CString (' ', 2));
		}
		else {
			CString str;
			DWORD dw = 0;

			TRACEF (GetClassName (hwnd));

			if (::GetWindowLong (hwnd, GWL_STYLE) & ES_READONLY)
				return 0;

			if (m_hwndUser && m_hwndPass) {
				if (lParam == 1) {
					CString strPass;

					GetDlgItemText (TXT_DATA, str);
					GetDlgItemText (TXT_PASS, strPass);


					::SendMessageTimeout (m_hwndUser, EM_SETSEL, 0, -1, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dw);
					::SendMessageTimeout (m_hwndUser, EM_REPLACESEL, 0, (LPARAM)(LPCTSTR)str, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dw);

					::SendMessageTimeout (m_hwndPass, EM_SETSEL, 0, -1, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dw);
					::SendMessageTimeout (m_hwndPass, EM_REPLACESEL, 0, (LPARAM)(LPCTSTR)strPass, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dw);
				}

				if (HWND hwndParent = ::GetParent (m_hwndUser)) {
					UINT nID = lParam == 1 ? IDOK : IDCANCEL;
					
					if (HWND hwndOK = ::GetDlgItem (hwndParent, nID)) 
						::SendMessageTimeout (hwndParent, WM_COMMAND, MAKEWPARAM (nID, BN_CLICKED), (LPARAM)hwndOK, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dw);
				}
			}
			else {
				if (lParam == 1) {
					GetDlgItemText (TXT_DATA, str);

					#ifdef _DEBUG
					{ CString strDebug; strDebug.Format (_T ("%s(%d): EM_REPLACESEL: (%d) %s"), _T (__FILE__), __LINE__, str.GetLength (), str); ::OutputDebugString (strDebug); }
					#endif

					::SendMessageTimeout (hwnd, EM_SETSEL, 0, -1, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dw);
					::SendMessageTimeout (hwnd, EM_REPLACESEL, 0, (LPARAM)(LPCTSTR)str, SMTO_BLOCK | SMTO_ABORTIFHUNG, 1000, &dw);

					if (HWND h = ::GetParent (hwnd)) { 
						LPCTSTR lpsz [] = 
						{
							_T ("ComboBox"),
							//_T ("ComboLBox"),
						};
						CString strClass = GetClassName (h);

						for (int nClass = 0; nClass < ARRAYSIZE (lpsz); nClass++) {
							if (!strClass.CompareNoCase (lpsz [nClass])) {
								CArray <HWND, HWND> v;

								for (HWND htmp = hwnd; htmp != NULL; htmp = ::GetParent (htmp)) {
									v.Add (htmp);

									//#ifdef _DEBUG
									//if (HWND h = htmp) { CString str; str.Format (_T ("[%d] %s"), ::GetWindowLong (h, GWL_ID), GetClassName (::GetParent (h))); TRACEF (str); }
									//if (HWND h = ::GetParent (htmp)) { CString str; str.Format (_T ("[%d] %s"), ::GetWindowLong (h, GWL_ID), GetClassName (::GetParent (h))); TRACEF (str); }
									//#endif //_DEBUG
								}

								for (int i = 0; i < v.GetSize (); i++) {
									CString str;
									DWORD dwResult = 0;
									HWND htmp = ::GetParent (v [i]);
									LONG lID = ::GetWindowLong (v [i], GWL_ID);
									LRESULT lResult = ::SendMessageTimeout (htmp, WM_KB_ON_EN_CHANGE, lID, (LPARAM)hwnd, SMTO_NORMAL | SMTO_ABORTIFHUNG, 1, &dwResult);

									str.Format (_T ("SendMessageTimeout (0x%p [%s], WM_KB_ON_EN_CHANGE, %d, 0x%p [%s], SMTO_NORMAL | SMTO_ABORTIFHUNG, 1, %d)"),
										htmp, GetClassName (htmp), 
										lID, 
										hwnd, GetClassName (hwnd), 
										dwResult);
									TRACEF (str);
								}

								break;
							}
						}
					}
				}
			}
		}
	}

	return lResult;
}


void CKbWinDlg::OnOK()
{
}

void CKbWinDlg::OnCancel()
{
}

void CKbWinDlg::OnClose() 
{
}

void CKbWinDlg::Show()
{
	CString strCmdLine = ::GetCommandLine ();

	int nIndex = strCmdLine.Find (_T ("/rc="));

	if (nIndex != -1) {
		CString strRC = strCmdLine.Mid (nIndex);
		int x = 0, y = 0, cx = 100, cy = 50;

		int n = _stscanf (strRC, _T ("/rc=%d,%d,%d,%d"), &x, &y, &cx, &cy);

		if (n == 4) 
			::SetWindowPos (m_hWnd, HWND_TOPMOST, x, y, cx, cy, SWP_SHOWWINDOW);
	}
}
