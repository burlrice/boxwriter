// KbWinDlg.h : header file
//

#if !defined(AFX_KBWINDLG_H__8D7D47E0_568F_450E_917F_847E4A7FAAC6__INCLUDED_)
#define AFX_KBWINDLG_H__8D7D47E0_568F_450E_917F_847E4A7FAAC6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/*
interface ITaskbarList : public IUnknown {
   virtual HRESULT __stdcall ActivateTab(HWND hwnd) = 0;
   virtual HRESULT __stdcall AddTab(HWND hwnd) = 0;
   virtual HRESULT __stdcall DeleteTab(HWND hwnd) = 0;
   virtual HRESULT __stdcall HrInit(void) = 0;
   virtual HRESULT __stdcall SetActiveAlt(HWND hwnd);
};
*/

/////////////////////////////////////////////////////////////////////////////
// CKbWinDlg dialog

class CKbWinDlg : public CDialog
{
// Construction
public:
	void Show();
	CKbWinDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CKbWinDlg ();

// Dialog Data
	//{{AFX_DATA(CKbWinDlg)
	enum { IDD = IDD_KBWIN_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKbWinDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString GetText (HWND hwnd);
	ITaskbarList * m_pTaskbar;
	HICON m_hIcon;
	CFont m_fntEdit;
	CFont m_fntLabel;
	CStringArray m_vInputClasses;
	CEdit m_wndData;
	CEdit m_wndPassword;
	CRect m_rcText;
	HWND m_hwndUser;
	HWND m_hwndPass;

	// Generated message map functions
	//{{AFX_MSG(CKbWinDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg void OnClose();
	//}}AFX_MSG

	afx_msg LRESULT OnIsAppRunning (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnShutdown (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnShowKbWin (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetText (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSetCaret (WPARAM wParam, LPARAM lParam);

	virtual void OnCancel();
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KBWINDLG_H__8D7D47E0_568F_450E_917F_847E4A7FAAC6__INCLUDED_)
