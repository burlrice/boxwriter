// KbMonitor.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "KbMonitor.h"
#include "KbMonitorDlg.h"
#include "WinMsg.h"
#include "Database.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CKbMonitorApp

BEGIN_MESSAGE_MAP(CKbMonitorApp, CWinApp)
	//{{AFX_MSG_MAP(CKbMonitorApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKbMonitorApp construction

CKbMonitorApp::CKbMonitorApp()
{
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CKbMonitorApp object

CKbMonitorApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CKbMonitorApp initialization

BOOL CKbMonitorApp::InitInstance()
{
	if (!FoxjetDatabase::CInstance::Elevate ())
		return FALSE;

	if (FoxjetDatabase::IsRunning (ISKEYBOARDMONITORRUNNING))
		return FALSE;

	m_instance.Create (ISKEYBOARDMONITORRUNNING);

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CKbMonitorDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
	}
	else if (nResponse == IDCANCEL)
	{
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
