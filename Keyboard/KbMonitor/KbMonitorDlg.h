// KbMonitorDlg.h : header file
//

#if !defined(AFX_KBMONITORDLG_H__C0C2BE0E_0D49_4E3C_8541_F679F9411B25__INCLUDED_)
#define AFX_KBMONITORDLG_H__C0C2BE0E_0D49_4E3C_8541_F679F9411B25__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CKbMonitorDlg dialog

#include "ximage.h"

/*
interface ITaskbarList : public IUnknown {
   virtual HRESULT __stdcall ActivateTab(HWND hwnd) = 0;
   virtual HRESULT __stdcall AddTab(HWND hwnd) = 0;
   virtual HRESULT __stdcall DeleteTab(HWND hwnd) = 0;
   virtual HRESULT __stdcall HrInit(void) = 0;
   virtual HRESULT __stdcall SetActiveAlt(HWND hwnd);
};
*/

class CKbMonitorDlg : public CDialog
{
// Construction
public:
	CKbMonitorDlg(CWnd* pParent = NULL);	// standard constructor
	~CKbMonitorDlg ();

// Dialog Data
	//{{AFX_DATA(CKbMonitorDlg)
	enum { IDD = IDD_KBMONITOR_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKbMonitorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnCancel();
	virtual void OnOK();
	HICON m_hIcon;
	CxImage m_img;
	ITaskbarList * m_pTaskbar;
	static ULONG CALLBACK ThreadFunc (LPVOID lpData);
	HANDLE m_hExit;
	HANDLE m_hThread;

	static BOOL CALLBACK EnumWindowsProc (HWND hWnd, LPARAM lParam);

	afx_msg LRESULT OnIsAppRunning (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnShutdown (WPARAM wParam, LPARAM lParam);

	// Generated message map functions
	//{{AFX_MSG(CKbMonitorDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnKb();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KBMONITORDLG_H__C0C2BE0E_0D49_4E3C_8541_F679F9411B25__INCLUDED_)
