// KbMonitor.h : main header file for the KBMONITOR application
//

#if !defined(AFX_KBMONITOR_H__8C0B5E79_4919_4DD7_BC6C_7E42AB0BD7D9__INCLUDED_)
#define AFX_KBMONITOR_H__8C0B5E79_4919_4DD7_BC6C_7E42AB0BD7D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "Database.h"

/////////////////////////////////////////////////////////////////////////////
// CKbMonitorApp:
// See KbMonitor.cpp for the implementation of this class
//

class CKbMonitorApp : public CWinApp
{
public:
	CKbMonitorApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CKbMonitorApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

	FoxjetDatabase::CInstance m_instance;

// Implementation

	//{{AFX_MSG(CKbMonitorApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_KBMONITOR_H__8C0B5E79_4919_4DD7_BC6C_7E42AB0BD7D9__INCLUDED_)
