// KbMonitorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "KbMonitor.h"
#include "KbMonitorDlg.h"
#include "Database.h"
#include "Parse.h"
#include "Debug.h"
#include "WinMsg.h"
#include "..\..\..\Control\Host.h"
#include "Registry.h"
#include "Utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace FoxjetCommon;

/////////////////////////////////////////////////////////////////////////////
// CKbMonitorDlg dialog

CKbMonitorDlg::CKbMonitorDlg(CWnd* pParent /*=NULL*/)
:	m_pTaskbar (NULL),
	m_hExit (NULL),
	CDialog(CKbMonitorDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CKbMonitorDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	::CoInitialize(NULL);
	VERIFY (SUCCEEDED (::CoCreateInstance (CLSID_TaskbarList, NULL, CLSCTX_ALL, IID_ITaskbarList, (void**)&m_pTaskbar)));
}

CKbMonitorDlg::~CKbMonitorDlg ()
{
	if (m_pTaskbar) {
		m_pTaskbar->Release ();
		m_pTaskbar = NULL;
	}

	if (m_hExit) {
		CloseHandle (m_hExit);
		m_hExit = NULL;
	}
}

void CKbMonitorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CKbMonitorDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CKbMonitorDlg, CDialog)
	//{{AFX_MSG_MAP(CKbMonitorDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_SIZE()
	ON_WM_DRAWITEM()
	ON_BN_CLICKED(BTN_KB, OnKb)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP

	ON_REGISTERED_MESSAGE (WM_ISKEYBOARDMONITORRUNNING, OnIsAppRunning)
	ON_REGISTERED_MESSAGE (WM_SHUTDOWNAPP, OnShutdown)
	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKbMonitorDlg message handlers

BOOL CKbMonitorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	CRect rcTaskBar (0, 0, 0, 0);
	CRect rcDesktop (0, 0, 0, 0);
	CRect rc (0, 0, 0, 0);

	::GetWindowRect (::GetDesktopWindow (), &rcDesktop);
	GetWindowRect (&rc);

	rc.right = rc.left + 96;
	rc.bottom = rc.top + 96;

	if (HWND hTaskBar = ::FindWindow (_T ("Shell_traywnd"), NULL)) 
		::GetWindowRect (hTaskBar, &rcTaskBar);

	int x = rcDesktop.right - rc.Width ();
	int y = rcDesktop.bottom - rc.Height () - rcTaskBar.Height ();
	int cx = rc.Width ();
	int cy = rc.Height ();

	SetWindowPos (&CWnd::wndTopMost, x, y, cx - 1, cy - 1, SWP_SHOWWINDOW | SWP_DRAWFRAME);
	SetWindowPos (&CWnd::wndTopMost, x, y, cx, cy, SWP_SHOWWINDOW | SWP_DRAWFRAME);
	
	HICON hIcon [1] = { NULL };
	CString strKB = FoxjetDatabase::GetHomeDir () + _T ("\\Keyboard.exe");
	UINT nExtractIconEx = ExtractIconEx (strKB, 0, hIcon, NULL, 1);

	if (hIcon [0]) {
		VERIFY (m_img.CreateFromHICON (hIcon [0]));
		::DestroyIcon (hIcon [0]);
	}
	else {
		HICON hIcon = AfxGetApp()->LoadIcon (IDR_MAINFRAME);
		VERIFY (m_img.CreateFromHICON (hIcon));
		::DestroyIcon (hIcon);
	}

	{
		DWORD dw;

		m_hExit = CreateEvent (NULL, true, false, _T ("KbMonitor::Exit"));
		VERIFY (m_hThread = ::CreateThread ((LPSECURITY_ATTRIBUTES) NULL, 0, (LPTHREAD_START_ROUTINE)ThreadFunc, (LPVOID)this, 0, &dw));
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CKbMonitorDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CKbMonitorDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CKbMonitorDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	if (CWnd * p = GetDlgItem (BTN_KB)) {
		p->SetWindowPos (NULL, 0, 0, cx, cy, SWP_SHOWWINDOW | SWP_NOZORDER);
	}
}

void CKbMonitorDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if (nIDCtl == BTN_KB) {
		CDC & dc = * CDC::FromHandle (lpDrawItemStruct->hDC);
		UINT uStyle = DFCS_BUTTONPUSH;
		CRect rc = lpDrawItemStruct->rcItem;

		if (lpDrawItemStruct->itemState & ODS_SELECTED)
			uStyle |= DFCS_PUSHED;

		::DrawFrameControl(lpDrawItemStruct->hDC, &lpDrawItemStruct->rcItem, DFC_BUTTON, uStyle);

		if (uStyle & DFCS_PUSHED)
			rc += CPoint (1, 1);

		m_img.Draw (lpDrawItemStruct->hDC, rc);

		if (m_pTaskbar) {
			m_pTaskbar->DeleteTab (m_hWnd);
			m_pTaskbar->DeleteTab (::AfxGetMainWnd ()->m_hWnd);
		}

		return;
	}

	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CKbMonitorDlg::OnOK()
{

}

void CKbMonitorDlg::OnCancel()
{
#ifdef _DEBUG
	CDialog::OnCancel ();
#endif
}

void CKbMonitorDlg::OnKb() 
{
	STARTUPINFO si;     
	PROCESS_INFORMATION pi; 
	TCHAR szCmdLine [MAX_PATH];
	CString strCmdLine = FoxjetDatabase::GetHomeDir () + _T ("\\Keyboard.exe /manually_started");

	_tcsncpy (szCmdLine, strCmdLine, MAX_PATH);
    memset (&pi, 0, sizeof(pi));
    memset (&si, 0, sizeof(si));
	si.cb = sizeof(si);     
	si.dwFlags = STARTF_USESHOWWINDOW;     
	si.wShowWindow = SW_SHOW;     

	VERIFY (::CreateProcess (NULL, szCmdLine, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)); 
}

BOOL CALLBACK CKbMonitorDlg::EnumWindowsProc (HWND hWnd, LPARAM lParam)
{
	CArray <HWND, HWND> & v = * (CArray <HWND, HWND> *)lParam;

//	if (::IsWindowVisible (hWnd)) {
		DWORD dwStyle = ::GetWindowLong (hWnd, GWL_STYLE);
		DWORD dwExStyle = ::GetWindowLong (hWnd, GWL_EXSTYLE);

		v.Add (hWnd);
//	}

	return TRUE;
}

ULONG CALLBACK CKbMonitorDlg::ThreadFunc (LPVOID lpData)
{
	CKbMonitorDlg & dlg = * (CKbMonitorDlg *)lpData;
	HWND hKeyboard = FindKeyboard ();
	CString strCmdLine = ::GetCommandLine ();
	
	strCmdLine.MakeLower ();

	const bool bMatrix = strCmdLine.Find (_T ("/matrix")) != -1;

	while (1) {
		DWORD dwWait = ::WaitForSingleObject (dlg.m_hExit, 1000);

		if (dwWait == WAIT_OBJECT_0)
			break;
		else {
			bool bShow = false;
			/* TODO: rem
			bool bExit = FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, _T ("Software\\FoxJet\\BoxWriter ELITE"), _T ("InstallerRunning"), 0) ? true : false;

			if (bExit) {
				dlg.m_hThread = NULL;
				dlg.EndDialog (IDOK);
				return 0;
			}
			*/

			if (bMatrix && FoxjetDatabase::IsRunning (ISPRINTERRUNNING))
				bShow = false;
			else {
				if (!::IsWindow (hKeyboard))
					hKeyboard = FindKeyboard ();

				if (!hKeyboard)
					bShow = true;
				else {
					DWORD dwResult = 0;
					LRESULT lResult = ::SendMessageTimeout (hKeyboard, WM_ISKEYBOARDVISIBLE, 0, 0,  SMTO_BLOCK | SMTO_ABORTIFHUNG, 100, &dwResult);
					
					TRACEF (_T ("WM_ISKEYBOARDVISIBLE: ") + FoxjetDatabase::ToString ((int)lResult) + _T (" :") + FoxjetDatabase::ToString ((int)dwResult));
					bShow = dwResult == 0 ? true : false;
				}
			}
			
			dlg.ShowWindow (bShow ? SW_SHOW : SW_HIDE);
		}
	}

	return 0;
}

void CKbMonitorDlg::OnDestroy() 
{
	if (m_hThread) {
		::SetEvent (m_hExit);
		VERIFY (::WaitForSingleObject (m_hThread, 2000) == WAIT_OBJECT_0);	
	}

	CDialog::OnDestroy();
}


LRESULT CKbMonitorDlg::OnIsAppRunning (WPARAM wParam, LPARAM lParam)
{
	return WM_ISKEYBOARDMONITORRUNNING;
}

LRESULT CKbMonitorDlg::OnShutdown (WPARAM wParam, LPARAM lParam)
{
	CDialog::OnOK ();
	exit (0);
	return WM_SHUTDOWNAPP;
}

