// FontDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Keyboard.h"
#include "FontDlg.h"
#include <afxdlgs.h>
#include "KeyboardDlg.h"
#include "Registry.h"
#include "Color.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFontDlg dialog


CFontDlg::CFontDlg(CWnd* pParent /*=NULL*/)
:	m_pfntNormal (NULL),
	m_pfntSmall (NULL),
	m_pfntArrow (NULL),
	CDialog(CFontDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFontDlg)
	m_nArrow = 15;
	//}}AFX_DATA_INIT
}

CFontDlg::~CFontDlg ()
{
	if (m_pfntNormal) delete m_pfntNormal;
	if (m_pfntSmall) delete m_pfntSmall;
	if (m_pfntArrow) delete m_pfntArrow;
}


void CFontDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFontDlg)
	DDX_Text(pDX, TXT_SIZE, m_nArrow);
	DDV_MinMaxInt(pDX, m_nArrow, 5, 100);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFontDlg, CDialog)
	//{{AFX_MSG_MAP(CFontDlg)
	ON_BN_CLICKED(BTN_CHANGENORMAL, OnChangenormal)
	ON_BN_CLICKED(BTN_CHANGESMALL, OnChangesmall)
	ON_NOTIFY(UDN_DELTAPOS, SPN_SIZE, OnDeltaposSize)
	ON_EN_CHANGE(TXT_SIZE, OnChangeSize)
	ON_BN_CLICKED(BTN_CHANGE_FONT_COLOR, OnChangeFontColor)
	ON_BN_CLICKED(BTN_CHANGE_BUTTON_COLOR, OnChangeButtonColor)
	ON_WM_DRAWITEM()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFontDlg message handlers

UINT_PTR CALLBACK CFHookProc (HWND hdlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);

void CFontDlg::OnChangenormal() 
{
	CFontDialog dlg (&m_lfNormal, CF_BOTH | CF_INITTOLOGFONTSTRUCT | CF_FORCEFONTEXIST, NULL, this);
	
	if (dlg.DoModal() == IDOK) {
		dlg.GetCurrentFont (&m_lfNormal);

		if (m_pfntNormal) {
			delete m_pfntNormal;
			m_pfntNormal = NULL;
		}

		m_pfntNormal = new CFont ();
		m_pfntNormal->CreateFontIndirect (&m_lfNormal);
		
		if (CWnd * p = GetDlgItem (LBL_NORMAL))
			p->SetFont (m_pfntNormal);
	}
}

void CFontDlg::OnChangesmall() 
{
	CFontDialog dlg (&m_lfSmall, CF_BOTH | CF_INITTOLOGFONTSTRUCT | CF_FORCEFONTEXIST, NULL, this);
	
	if (dlg.DoModal() == IDOK) {
		dlg.GetCurrentFont (&m_lfSmall);

		if (m_pfntSmall) {
			delete m_pfntSmall;
			m_pfntSmall = NULL;
		}

		m_pfntSmall = new CFont ();
		m_pfntSmall->CreateFontIndirect (&m_lfSmall);
		
		if (CWnd * p = GetDlgItem (LBL_SMALL))
			p->SetFont (m_pfntSmall);
	}
}

void CFontDlg::OnDeltaposSize(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	CString str;

	str.Format (_T ("%d"), pNMUpDown->iPos);
	SetDlgItemText (TXT_SIZE, str);
	
	*pResult = 0;
}

BOOL CFontDlg::OnInitDialog() 
{
	CFont * m_pfntNormal;
	CFont * m_pfntSmall;
	CFont * m_pfntArrow;

	CDialog::OnInitDialog();
	
	m_pfntNormal = new CFont ();
	m_pfntSmall = new CFont ();
	m_pfntArrow = new CFont ();

	m_pfntNormal->CreateFontIndirect (&m_lfNormal);
	m_pfntSmall->CreateFontIndirect (&m_lfSmall);
	m_pfntArrow = CKeyboardDlg::CreateArrowFont (m_nArrow);

	if (CWnd * p = GetDlgItem (LBL_NORMAL))
		p->SetFont (m_pfntNormal);
	if (CWnd * p = GetDlgItem (LBL_SMALL))
		p->SetFont (m_pfntSmall);
	if (CWnd * p = GetDlgItem (LBL_ARROW)) {
		CString str;

		str += (TCHAR)0xE1;
		str += (TCHAR)0xE2;
		str += (TCHAR)0xE0;
		str += (TCHAR)0xDF;

		p->SetFont (m_pfntArrow);
		p->SetWindowText (str);
	}

	if (CSpinButtonCtrl * p = (CSpinButtonCtrl *)GetDlgItem (SPN_SIZE)) {
		p->SetRange (5, 100);
		p->SetPos (m_nArrow);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFontDlg::OnChangeSize() 
{
	if (m_pfntArrow) {
		delete m_pfntArrow;
		m_pfntArrow = NULL;
	}

	m_pfntArrow = CKeyboardDlg::CreateArrowFont (m_nArrow = GetDlgItemInt (TXT_SIZE));

	if (CWnd * p = GetDlgItem (LBL_ARROW))
		p->SetFont (m_pfntArrow);
}

void CFontDlg::OnChangeFontColor() 
{
	CColorDialog dlg;
	
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	dlg.m_cc.rgbResult = GetFontColor ();
	
	if (dlg.DoModal () == IDOK) {
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, _T ("Software\\Foxjet\\Keyboard"), _T ("font color"), dlg.m_cc.rgbResult);

		if (CWnd * p = GetDlgItem (BTN_FONT_COLOR)) {
			p->Invalidate ();
			p->RedrawWindow ();
		}
	}
}

void CFontDlg::OnChangeButtonColor() 
{
	CColorDialog dlg;
	
	dlg.m_cc.Flags |= CC_FULLOPEN | CC_RGBINIT;
	dlg.m_cc.rgbResult = GetButtonColor ();
	
	if (dlg.DoModal () == IDOK) {
		FoxjetDatabase::WriteProfileInt (HKEY_CURRENT_USER, _T ("Software\\Foxjet\\Keyboard"), _T ("button color"), dlg.m_cc.rgbResult);
		
		if (CWnd * p = GetDlgItem (BTN_BUTTON_COLOR)) {
			p->Invalidate ();
			p->RedrawWindow ();
		}
	}
}

void CFontDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if (nIDCtl == BTN_FONT_COLOR) {
		::DrawEdge (lpDrawItemStruct->hDC, &lpDrawItemStruct->rcItem, EDGE_SUNKEN, BF_LEFT | BF_RIGHT | BF_TOP | BF_BOTTOM | BF_ADJUST);
		::FillRect (lpDrawItemStruct->hDC, &lpDrawItemStruct->rcItem, CBrush (GetFontColor ()));
	}
	else if (nIDCtl == BTN_BUTTON_COLOR) {
		::DrawEdge (lpDrawItemStruct->hDC, &lpDrawItemStruct->rcItem, EDGE_SUNKEN, BF_LEFT | BF_RIGHT | BF_TOP | BF_BOTTOM | BF_ADJUST);
		::FillRect (lpDrawItemStruct->hDC, &lpDrawItemStruct->rcItem, CBrush (GetButtonColor ()));
	}

	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

COLORREF CFontDlg::GetFontColor ()
{
	return FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, _T ("Software\\Foxjet\\Keyboard"), _T ("font color"), Color::rgbBlack);
}

COLORREF CFontDlg::GetButtonColor ()
{
	return FoxjetDatabase::GetProfileInt (HKEY_CURRENT_USER, _T ("Software\\Foxjet\\Keyboard"), _T ("button color"), RGB(0xC0, 0xC0, 0xC0));
}
