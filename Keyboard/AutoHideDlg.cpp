// AutoHideDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Keyboard.h"
#include "AutoHideDlg.h"
#include "Registry.h"
#include "Parse.h"
#include "KeyboardDlg.h"
#include "Debug.h"
#include "CopyArray.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CKeyboardApp theApp;

using namespace ItiLibrary;
using namespace FoxjetDatabase;

CString CAutoHideDlg::CClassItem::GetDispText (int nColumn) const
{
	switch (nColumn) {
	case 0:	return LoadString (m_bEnabled ? IDS_AUTO_KB_ENABLE : IDS_AUTO_KB_DISABLE);
	case 1:	return m_strName;
	}

	return _T ("");
}

/////////////////////////////////////////////////////////////////////////////
// CAutoHideDlg property page

IMPLEMENT_DYNCREATE(CAutoHideDlg, CPropertyPage)

CAutoHideDlg::CAutoHideDlg() : CPropertyPage(CAutoHideDlg::IDD)
{
	//{{AFX_DATA_INIT(CAutoHideDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CAutoHideDlg::~CAutoHideDlg()
{
	if (m_hbmpCheck) {
		::DeleteObject (m_hbmpCheck);
		m_hbmpCheck = NULL;
	}
}

void CAutoHideDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAutoHideDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAutoHideDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CAutoHideDlg)
	ON_NOTIFY(NM_DBLCLK, LV_CLASSES, OnDblclkClasses)
	ON_BN_CLICKED(BTN_TOGGLE, OnToggle)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAutoHideDlg message handlers

BOOL CAutoHideDlg::OnInitDialog() 
{

	using namespace ListCtrlImp;

	CArray <CColumn, CColumn> vCols;
	CStringArray vData, vDataExclude;

	CPropertyPage::OnInitDialog ();

	vCols.Add (CColumn (LoadString (IDS_USESKB), 80));
	vCols.Add (CColumn (LoadString (IDS_CLASS), 200));
	m_hbmpCheck = (HBITMAP)::LoadImage (::AfxGetInstanceHandle (), MAKEINTRESOURCE (IDB_CHECK), IMAGE_BITMAP, 0, 0, LR_DEFAULTSIZE);

	if (CButton * p = (CButton *)GetDlgItem (BTN_TOGGLE))
		p->SetBitmap (m_hbmpCheck);

	m_lv.Create (LV_CLASSES, vCols, this, _T ("Settings\\CAutoHideDlg"));
	//m_lv->SetExtendedStyle (m_lv->GetExtendedStyle () | LVS_EX_CHECKBOXES);

	FromString (theApp.GetProfileString (_T ("CKeyboardDlg"), _T ("input classes"), CKeyboardDlg::m_strWndClasses), vData);
	FromString (theApp.GetProfileString (_T ("CKeyboardDlg"), _T ("excluded classes"), CKeyboardDlg::m_strWndClassesExclude), vDataExclude);

	for (int i = 0; i < vData.GetSize (); i++) {
		CString str = vData [i];
		TRACEF (_T ("         ") + str);
		int nIndex = m_lv.InsertCtrlData (new CClassItem (str, true));
		m_lv->SetCheck (nIndex, TRUE);
	}

	for (int i = 0; i < vDataExclude.GetSize (); i++) {
		CString str = vDataExclude [i];
		TRACEF (_T ("exclude: ") + str);
		int nIndex = m_lv.InsertCtrlData (new CClassItem (str, false));
		m_lv->SetCheck (nIndex, FALSE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAutoHideDlg::OnDblclkClasses(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnToggle ();

	if (pResult)
		* pResult = 0;
}

void CAutoHideDlg::OnToggle() 
{
	CStringArray v, vExclude, vHardcodedExclude;
	FoxjetCommon::CLongArray sel = GetSelectedItems (m_lv);

	FromString (CKeyboardDlg::m_strWndClassesExclude, vHardcodedExclude);

	for (int i = 0; i < sel.GetSize (); i++) {
		if (CClassItem * p = (CClassItem *)m_lv.GetCtrlData (sel [i])) {
			if (Find (vHardcodedExclude, p->m_strName) == -1) {
				p->m_bEnabled = !p->m_bEnabled;
				m_lv.UpdateCtrlData (p);
			}
		}
	}

	for (int i = 0; i < m_lv->GetItemCount (); i++) {
		if (CClassItem * p = (CClassItem *)m_lv.GetCtrlData (i)) {
			if (p->m_bEnabled)
				v.Add (p->m_strName);
			else
				vExclude.Add (p->m_strName);
		}
	}

	TRACEF (_T ("         ") + ToString (v));
	TRACEF (_T ("exclude: ") + ToString (vExclude));

	theApp.WriteProfileString (_T ("CKeyboardDlg"), _T ("input classes"), ToString (v));
	theApp.WriteProfileString (_T ("CKeyboardDlg"), _T ("excluded classes"), ToString (vExclude));
}
