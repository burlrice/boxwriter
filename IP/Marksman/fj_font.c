#include "fj.h"
#include <stdio.h>

#include "fj_mem.h"
//#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_font.h"

// used for MemStor identification
const  char fj_FontID[]   = "Font";

//
//  FUNCTION: fj_FontNew()
//
//  PURPOSE:  Create a new FJONT structure
//
FJDLLExport LPFJFONT fj_FontNew( VOID )
{
	LPFJFONT pff;

	pff = (LPFJFONT)fj_calloc( 1, sizeof (FJFONT) );

	pff->lType         = 0;				// font type
	pff->lHeight       = 0;				// height of font charascter in pixels  - depends on font
	pff->lWidth        = 0;				// width of font character in pixels - depends on font
	pff->lFirstChar    = 0;				// first defined character in font
	pff->lLastChar     = 0;				// last defined character in font
	pff->pBuffer       = NULL;			// pointer to font rasters
	pff->lBufLen       = 0;				// Length of font rasters buffer
	pff->pWBuffer      = NULL;			// pointer to font width array
	pff->strName[0]    =  0;			// font name
	return( pff );
}
//
//  FUNCTION: fj_FontDestroy()
//
//  PURPOSE:  Destroy a FJFONT structure
//
FJDLLExport VOID fj_FontDestroy( LPFJFONT pff )
{
	if ( NULL != pff->pWBuffer      ) fj_free( pff->pWBuffer      );
	if ( NULL != pff->pBuffer       ) fj_free( pff->pBuffer       );
	fj_free( pff );
	return;
}

//  FUNCTION: fj_FontToBuffer()
//
//  PURPOSE:  Convert a FJFONT structure into a descriptive string with binary buffer
//
FJDLLExport LONG fj_FontToBuffer( CLPCFJFONT pff, LPBYTE pBuff )
{
	LONG lLen;
	STR  sName[FJFONT_NAME_SIZE*2];

										// font Name
	fj_processParameterStringOutput( sName, pff->strName );

	lLen = 1 + sprintf ((CHAR*)pBuff, "{%s,%s,%d,%d,%d,%d,%d,%d}",
		fj_FontID,
		sName,
		pff->lType,						// font type
		pff->lHeight,					// height of font charascter in pixels  - depends on font
		pff->lWidth,					// width of font character in pixels - depends on font
		pff->lFirstChar,				// first defined character in font
		pff->lLastChar,					// last defined character in font
		pff->lBufLen);					// Length of font rasters buffer

	if ( NULL != pff->pBuffer )
	{
		memcpy( pBuff + lLen, pff->pBuffer, pff->lBufLen );
		lLen += pff->lBufLen;
	}

	return( lLen );
}

//
//  FUNCTION: fj_FontFromBuffer
//
//  PURPOSE:  Convert a descriptive string into a FJFONT structure
//
FJDLLExport LPFJFONT fj_FontFromBuffer( LPBYTE pBuff )
{
#define FONT_PARAMETERS 8
	LPFJFONT pff;
	LPSTR  pParams[FONT_PARAMETERS];
	LPSTR  pStrM;
	LPBYTE pBinary;
	LPBYTE pRast;
	USHORT sWidth;
	BOOL   bError;
	ULONG  sLen;
	int iBytes;
	int res;
	int i, n;

	pff = NULL;
	res = 0;
	pStrM = (LPSTR)fj_malloc( FJFONT_MEMSTOR_SIZE );
	strcpy( pStrM, (LPSTR)pBuff );

	res = (int)fj_ParseKeywordsFromStr( pStrM, ',', pParams, FONT_PARAMETERS );
	if ( 0 != strcmp( pParams[0], fj_FontID ) ) res = 0;

	if ( res == FONT_PARAMETERS )		// FJ_FONT string must have 10 field delimiters!!!!!
	{
		bError = TRUE;
		pff = fj_FontNew();
		fj_processParameterStringInput( pParams[1] );
		strcpy( pff->strName,           pParams[1] );
										// font type
		pff->lType          = atol(pParams[2]);
										// height of font charascter in pixels  - depends on font
		pff->lHeight        = atol(pParams[3]);
										// width of font character in pixels - depends on font
		pff->lWidth         = atol(pParams[4]);
										// first defined character in font
		pff->lFirstChar     = atol(pParams[5]);
										// last defined character in font
		pff->lLastChar      = atol(pParams[6]);
										// Length of font rasters buffer
		pff->lBufLen        = atol(pParams[7]);

		if ( 0 != pff->lBufLen )
		{
			pBinary = pBuff + 1 + strlen((LPSTR)pBuff);
			pff->pBuffer = fj_malloc( pff->lBufLen );
			if ( NULL != pff->pBuffer )
			{
				bError = FALSE;
				memcpy( pff->pBuffer, pBinary, pff->lBufLen );
				n = pff->lLastChar - pff->lFirstChar + 1;
				pff->pWBuffer = fj_malloc( n * sizeof(ULONG) );

				pRast = pff->pBuffer;
				iBytes = ( pff->lHeight + 7 ) / 8;
				for ( i = 0; i < n; i++)
				{
					sLen = pRast - pff->pBuffer;
					if ( sLen > pff->lBufLen )
					{
						bError = TRUE;
						break;
					}
										// store index of char
					*(pff->pWBuffer + i) = sLen;
					sWidth = (((*(pRast))<<8)&0x0000ff00) | *(pRast+1);
					if ( (sWidth > pff->lWidth) || (sWidth < 0) )
					{
						bError = TRUE;
						break;
					}
					pRast += 2 + sWidth * iBytes;
				}
				i = pRast - pff->pBuffer;
				if ( i != (LONG)(pff->lBufLen) )
				{
					bError = TRUE;
				}
			}
		}
		if ( TRUE == bError )
		{
			fj_FontDestroy( pff );
			pff = NULL;
		}
	}
	fj_free( pStrM );
	return ( pff );
}

//
//	FUNCTION: fj_FontBuildFromName
//
//	Return: TRUE if success.
//
//  Extract and build a FJFONT from FJMEMSTOR
//
FJDLLExport LPFJFONT fj_FontBuildFromName( LPCSTR pName )
{
	LPFJFONT pff = NULL;
	char szFile [FJSYS_MAX_FILENAME];

	sprintf (szFile, "%s/%s", FJ_DIR_FONTS, pName);
	
	int nLen = ReadFileSize (szFile);
	
	if (nLen > 0) {
		char * psz = fj_malloc (nLen);
		
		memset (psz, 0, nLen);
		
		if (ReadFile (&nLen, psz, szFile)) 
			pff = fj_FontFromBuffer (psz);
		
		fj_free (psz);
	}
	
	return pff;
}
//
//	FUNCTION: fj_FontAddToMemstor
//
//	Return: TRUE if success.
//
//  Add FJFONT element to FJMEMSTOR
//
FJDLLExport BOOL fj_FontAddToMemstor( LPCFJFONT pff )
{
	BOOL   bRet = FALSE;
/* TODO
	LPBYTE pBuf;
	LONG   lSize;

	pBuf = fj_malloc( FJFONT_MEMSTOR_SIZE + pff->lBufLen );
	lSize = fj_FontToBuffer( pff, pBuf );

	bRet = fj_MemStorAddElementBinary( pfms, pBuf, lSize );

	fj_free( pBuf );
*/	
	return( bRet );
}
