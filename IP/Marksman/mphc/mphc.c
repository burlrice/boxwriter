#include <stdio.h>
#include <stdlib.h>
#include <tx_api.h>
#include <bsp.h>
#include <i2c_api.h>

#include "gpiomux_def.h"
#include "mc_isr.h"
#include "nagpio.h"
#include "appconf.h"
#include "netosIo.h"
#include "mphc_defines.h"
#include "mphc.h"
#include "fj_base.h"
#include "fj.h"

#define FPGA_PROG	80
#define FPGA_INIT	95
#define FPGA_DONE	94
#define FPGA_WRITE	102
#define HIGH		1
#define LOW		0
	
// This is the units when the conveyor speed is 100 ft/min
// This means, if this value is 1000 then when the conveyor is running 
// at 100 ft/min the value returned when you read the speed register 
// will be 1000  This allows for as much presision as needed.
const double FTACH_100 = 1000.0;

// This is a fixed constant within the MPHC Board.
const double FGATE = 115200.0;

volatile tagPHC_Flags gPHCFlags;
volatile unsigned short *g_pAddress;

BOOL gbTraceWrite 	= FALSE;
BOOL gbTraceRead 	= FALSE;
BOOL gbFPGA 		= FALSE;

TX_MUTEX gMutexFPGA;

BOOL FpgaLock (ULONG lTicks, const char * pszFile, ULONG lLine)
{
	//printf ("%s(%d): FpgaLock\n", pszFile, lLine);
	return tx_mutex_get (&gMutexFPGA, lTicks) == TX_SUCCESS;
}

BOOL FpgaUnlock (const char * pszFile, ULONG lLine)
{
	//printf ("%s(%d): FpgaUnlock\n", pszFile, lLine);
	return tx_mutex_put (&gMutexFPGA) == TX_SUCCESS;
}

static int irq3Isr(void *context)
{
	static unsigned char uIIR;
	static unsigned char uData;
	static unsigned char uBitMask;
	//volatile tagPHC_Flags *pFlags;
	volatile tagPHC_Flags *pFlags = context;

	pFlags->flagISR++;
	
//	if (FPGA_TRY_LOCK (NS_MILLISECONDS_TO_TICKS (100))) {
		uIIR = ReadByte ( IIR );
		//printf ("%s(%d): uIIR: 0x%02X\n", __FILE__, __LINE__, uIIR);
		//naGpioSetOutputValue (GPIO_USER_LED1, pFlags->flagISR % 2);
		
		do {
			switch ( uIIR & 0x07 ) {
			case 0:		// MSR
				uData = ReadByte (MSR);
				uBitMask = 0x01;
				while ( uBitMask & 0x0F ) {
					switch ( uData & uBitMask ){
					case PC_INT:
						pFlags->flagPhotoInt++;
						pFlags->PhotocellActivated = TRUE;	// Photocell event has occured on the slave device.
						//naGpioSetOutputValue (GPIO_USER_LED1, pFlags->flagPhotoInt % 2);
						break;
					case AMS_STATE_CHANGE:
						pFlags->AmsActive = !pFlags->AmsActive;
						break;
					case PRINT_CYCLE_STATE:
						switch (uData & 0x20) {
							case 0:	// Complete print-cycle is finished.
								pFlags->flagEOPInt++;
								pFlags->EndOfPrint = TRUE;
								//naGpioSetOutputValue (GPIO_USER_LED2, pFlags->flagEOPInt % 2);
								break;
							case 1:	// Photo delay is finished and Printing has begun.
								pFlags->StartOfPrint = TRUE;
								break;
						}
					case OTB_INT:
						break;
					}
					uBitMask <<= 1;
				}
				break;
			case 2:  // THR Empty
				break;
			case 4:  // Serial Data Aval.
				pFlags->flagSerial++;
	//				ProcessSerialData();
				break;
			case 6:  // LSR Error
				pFlags->flagSerial++;
				ReadByte ( LSR );
				break;
			default:
				pFlags->flagReserved++;
				//printf ("%s(%d): pFlags->flagReserved: %d [uIIR: %d]\n", __FILE__, __LINE__, pFlags->flagReserved, uIIR);
				break;
			}
			uIIR = ReadByte ( IIR );
			//printf ("%s(%d): uIIR: 0x%02X\n", __FILE__, __LINE__, uIIR);
		} while (  !( uIIR & 0x01 ) );
		
//		FPGA_UNLOCK ();		
//	}
		
	// clear the interrupt 
	narm_write_reg (NA_SCM_EICR_REG, NA_SCM_EICR_2_ADDRESS, clr, 1);
	narm_write_reg (NA_SCM_EICR_REG, NA_SCM_EICR_2_ADDRESS, clr, 0);
	
	return 0;	
}

const BYTE bReverse [] =
{
	0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0, // 0
	0x10, 0x90, 0x50, 0xD0, 0x30, 0xB0, 0x70, 0xF0, // 1 
	0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68, 0xE8, // 2 
	0x18, 0x98, 0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8, // 3 
	0x04, 0x84, 0x44, 0xC4, 0x24, 0xA4, 0x64, 0xE4, // 4 
	0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74, 0xF4, // 5 
	0x0C, 0x8C, 0x4C, 0xCC, 0x2C, 0xAC, 0x6C, 0xEC, // 6 
	0x1C, 0x9C, 0x5C, 0xDC, 0x3C, 0xBC, 0x7C, 0xFC, // 7 
	0x02, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2, // 8 
	0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, 0xF2, // 9 
	0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA, // 10 
	0x1A, 0x9A, 0x5A, 0xDA, 0x3A, 0xBA, 0x7A, 0xFA, // 11 
	0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6, // 12 
	0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6, // 13 
	0x0E, 0x8E, 0x4E, 0xCE, 0x2E, 0xAE, 0x6E, 0xEE, // 14 
	0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE, // 15 
	0x01, 0x81, 0x41, 0xC1, 0x21, 0xA1, 0x61, 0xE1, // 16 
	0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71, 0xF1, // 17 
	0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9, // 18 
	0x19, 0x99, 0x59, 0xD9, 0x39, 0xB9, 0x79, 0xF9, // 19 
	0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65, 0xE5, // 20 
	0x15, 0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5, // 21 
	0x0D, 0x8D, 0x4D, 0xCD, 0x2D, 0xAD, 0x6D, 0xED, // 22 
	0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D, 0xFD, // 23 
	0x03, 0x83, 0x43, 0xC3, 0x23, 0xA3, 0x63, 0xE3, // 24 
	0x13, 0x93, 0x53, 0xD3, 0x33, 0xB3, 0x73, 0xF3, // 25 
	0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B, 0xEB, // 26 
	0x1B, 0x9B, 0x5B, 0xDB, 0x3B, 0xBB, 0x7B, 0xFB, // 27 
	0x07, 0x87, 0x47, 0xC7, 0x27, 0xA7, 0x67, 0xE7, // 28 
	0x17, 0x97, 0x57, 0xD7, 0x37, 0xB7, 0x77, 0xF7, // 29 
	0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F, 0xEF, // 30 
	0x1F, 0x9F, 0x5F, 0xDF, 0x3F, 0xBF, 0x7F, 0xFF, // 31
};

BYTE revbyte (BYTE b)
{
	return bReverse [b];
/*
	int i;
	BYTE bResult = 0;
	
	for (i = 0; i < 8; i++)
		bResult |= ((b & (1 << i)) ? 1 : 0) << (7 - i);
		
	return bResult;
*/
}

BYTE revfield (BYTE b, int nBits)
{
	int i;
	BYTE bResult = 0;

	if (nBits <= 8) 
		for (i = 0; i < nBits; i++)
			bResult |= ((b & (1 << i)) ? 1 : 0) << ((nBits - 1) - i);

	return bResult;
}

BYTE * revcpy (BYTE * p, size_t size)
{
	BYTE * pResult = (BYTE *)fj_malloc (size);
	int i;
	
	for (i = 0; i < (int)size; i++)
		pResult [i] = revbyte (p [i]);
		
	return pResult;
}

void SetMphcBaseAddress ( unsigned short *pAddress )
{
	g_pAddress = (unsigned short *)pAddress;
}

unsigned char ReadByte( unsigned char offset )
{
	FPGA_LOCK ();
	
	unsigned short value;
	volatile unsigned short *pAddress;
	
	pAddress = g_pAddress;
	pAddress += offset;
	value = *pAddress;

	if (gbTraceRead)
		printf ("%s(%d): ReadByte: %p [0x%02X]: 0x%02X\n", __FILE__, __LINE__, pAddress, pAddress - g_pAddress, value);

	FPGA_UNLOCK ();
	
	return value;
}

void WriteByte (unsigned char offset, unsigned char data)
{
	FPGA_LOCK ();
	
	unsigned char byte = data;
	volatile unsigned short * pAddress = g_pAddress;

	pAddress += offset;
	*pAddress = byte;
	
	if (gbTraceWrite)
		printf ("%s(%d): WriteByte: %p [0x%02X]: 0x%02X\n", __FILE__, __LINE__, pAddress, pAddress - g_pAddress, byte);
		
	FPGA_UNLOCK ();
}

void WriteBuffer (unsigned char offset, unsigned char *pBuffer, unsigned long lBytes)
{
	FPGA_LOCK ();
	
	unsigned long i;
	
	for ( i = 0; i < lBytes; i++ ) {
		WriteByte ( offset, pBuffer[i] );
	}
	
	FPGA_UNLOCK ();
}

void ReadBuffer (unsigned char offset, unsigned char *pBuffer, unsigned long lBytes)
{
	FPGA_LOCK ();
	
	unsigned long i;
	
	for ( i = 0; i < lBytes; i++ ) {
		pBuffer[i] = ReadByte ( offset );
	}
	
	FPGA_UNLOCK ();
}

//////////////////////////////////////////////////////////////////////
//	FPGA is told that programming is about to begin by setting the
//		UART for 8 Databits, 1 Parity, and 2 Stopbits.  Reset to 8,N,1 
//		before actual programming begins.
//////////////////////////////////////////////////////////////////////
void EnableProgrammingMode ( bool Enabled )
{
	FPGA_LOCK ();
	
	unsigned char value;
	if ( Enabled ) {
		value = ReadByte ( MSR);
		WriteByte ( LCR, LCR_8_MARK_2);	// Tell UART we want to program it.
		WriteByte ( MCR, DTR);
		WriteByte ( LCR, LCR_8_NONE_1 );	// Reset to 8,N,1
	}
	else
		WriteByte ( LCR, 0x03 );
		
	FPGA_UNLOCK ();
}

void EnableDataTransferMode( bool Enabled, bool Direction)
{
	FPGA_LOCK ();
	
	if ( Enabled ) {
		WriteByte ( LCR, 0x03 );				// Ensure Baud Rate Divisor is NOT set.
		if ( Direction == WRITE_MODE )
			WriteByte ( LCR, LCR_6_MARK_2 );	// Set Data Transfer Mode. ( Write )
		else
			WriteByte ( LCR, LCR_6_SPACE_2 );	// Set Data Transfer Mode. ( Read )
	}
	else
		WriteByte (LCR, 0x03);
	
	FPGA_UNLOCK ();
}

void EnableCommandMode( bool Enabled)
{
	FPGA_LOCK ();
	
	if ( Enabled == TRUE ) {
		WriteByte ( LCR, 0x03 );			// Ensure we are in a known state.
		WriteByte ( LCR, LCR_8_SPACE_2 );	// Set Command Mode.
	}
	else
		WriteByte ( LCR, 0x03 );
		
	FPGA_UNLOCK ();
}

void ReadRegister( int RegID, void *pData, int nBytes)
{
	FPGA_LOCK ();
	
	EnableCommandMode ( TRUE );
		WriteByte ( CMD_REG, (0x80 | RegID ) );	// Read mode; 
		WriteByte ( CMD_REG, nBytes );			// Bytes To Transfer
		ReadBuffer ( CMD_REG, pData, nBytes);	// Read Data
	EnableCommandMode ( FALSE );
	
	FPGA_UNLOCK ();
}

void WriteRegister( int RegID, void *pData, int nBytes)
{
	FPGA_LOCK ();
	
	EnableCommandMode ( TRUE );
		WriteByte ( CMD_REG, 0x7F & RegID);		// Write mode; 
		WriteByte ( CMD_REG, nBytes );			// Bytes To Transfer
		WriteBuffer ( CMD_REG, pData, nBytes );	// Write data
	EnableCommandMode ( FALSE );
	
	FPGA_UNLOCK ();
}

#define EXECUTE(s, t, nSleep, nResult) \
	{ \
		int i; \
		BOOL bSignalled = FALSE; \
		nResult = 0; \
		for (i = 0; (i < 10) && !bSignalled; i++) { \
			if (!(bSignalled = ((nResult = (s)) == (t)))) \
				tx_thread_sleep ((nSleep)); \
		} \
		if (bSignalled) printf ("%s(%d): " #s "\n", __FILE__, __LINE__); \
		else 			printf ("%s(%d): NOT SIGNALLED: " #s "\n", __FILE__, __LINE__); \
	} 
		
int ProgramFPGA(void * pCode, unsigned long lBytes)
{
	ULONG lTickStart = tx_time_get ();
	int status = 0;
	int nResult;

	gbFPGA = FALSE;
	
	FPGA_LOCK ();

	printf ("%s(%d): Enabling Programming Mode on 0x%X\n", __FILE__, __LINE__, g_pAddress);
	
	EXECUTE (naGpioSetOutput (FPGA_PROG, HIGH), 1, 1, nResult); 	// Set FPGA_PROG High to start
	EXECUTE (naGpioSetOutput (FPGA_WRITE, HIGH), 1, 1, nResult);	// Set FPGA_WRITE High to start

	tx_thread_sleep(NABspTicksPerSecond/2);						// Wait FOR PERIOD OF TIME
	EXECUTE (naGpioSetOutput (FPGA_PROG, LOW), 1, 1, nResult);	// Set FPGA_PROG LOW THEN HIGH TO START PROCESS, CLEAR FPGA

	EXECUTE (naGpioGetValue (FPGA_INIT), 0, 10, nResult);		//Wait for FPGA_INIT to go low
    EXECUTE (naGpioSetOutput (FPGA_PROG, HIGH), 1, 1, nResult);	// Set high to clear FPGA mem
    
	printf ("%s(%d): Waiting for 0x%X to become ready\n", __FILE__, __LINE__, g_pAddress);
	
	EXECUTE (naGpioGetValue (FPGA_INIT), 1, (NABspTicksPerSecond / 500.0), nResult);
	
	if (!nResult) {
		printf ("%s(%d): ERROR 0x%X NOT READY\n", __FILE__, __LINE__, g_pAddress);
		FPGA_UNLOCK ();
		return -3;
	}
	
	EXECUTE (naGpioSetOutput (FPGA_WRITE, LOW), 1, 1, nResult);	// Set FPGA_WRITE Low to start programming

	printf ( "%s(%d): Programming FPGA with %lu\n", __FILE__, __LINE__, lBytes);
	WriteBuffer (0x00, (unsigned char *)pCode, lBytes);
	printf ("%s(%d): Waiting For \"Done\" pin assert\n", __FILE__, __LINE__);

	EXECUTE (naGpioGetValue (FPGA_DONE), 1, 100, nResult);
	
	if (!nResult) {
		status = -2;
		printf ( "%s(%d): Timeout Waiting For Done Pin\n", __FILE__, __LINE__);
	}

	if ( status == 0x00 )	{
		EXECUTE (naGpioSetOutput (FPGA_WRITE, HIGH), 1, 1, nResult);		// Set WRITE high done FPGA programmed
		gbFPGA = TRUE;
		printf ("%s(%d): FPGA at 0x%X Programmed Successfully.\n", __FILE__, __LINE__, g_pAddress);
	}

	/*
	// The main reason for this is to set the print enable bit to 0
	EnableCommandMode ( TRUE );
	WriteByte ( 0x00, 0x00 );
	WriteByte ( 0x00, 0x01 );
	WriteByte ( 0x00, 0x87 );
	EnableCommandMode ( FALSE );
	*/

	FPGA_UNLOCK ();

	return status;
}

/*
int ProgramFPGA(void * pCode, unsigned long lBytes)
{
	int status = 0;
	int cnt= 0;

	gbFPGA = FALSE;
	
	FPGA_LOCK ();
	
	printf ( "%s(%d): Enabling Programming Mode on 0x%X\n", __FILE__, __LINE__, g_pAddress);
	EnableProgrammingMode ( TRUE );
	// Wait for UART to get ready.  This should be < 1 uSec
	printf ( "%s(%d): Waiting for 0x%X to become ready\n", __FILE__, __LINE__, g_pAddress);
	while ( !( ReadByte ( MSR ) & 0x10 ) ) {
	    tx_thread_sleep( (NABspTicksPerSecond / 500.0) ); // 2 milli-second this is smallest I can sleep
		cnt++;
		if ( cnt > 3 ) {
			printf ("%s(%d): ProgramFPGA failed\n", __FILE__, __LINE__);
			FPGA_UNLOCK ();
			return -3;
		}
	}

	if ( cnt < 3 ) 
	{
		printf ( "%s(%d): Programming FPGA with %lu\n", __FILE__, __LINE__, lBytes);
		WriteBuffer (0x00, (unsigned char *)pCode, lBytes);
		cnt = 0;
		printf ( "%s(%d): Waiting For \"Done\" pin assert\n", __FILE__, __LINE__);
		
		WriteByte ( LCR, 0x00 );	
		while ( !(ReadByte( MSR ) & 0x20) ) 
		{	// See if it is done
		    tx_thread_sleep(2);
			cnt++;
			if ( cnt > 10 ) 
			{
				status = -2;
				printf ( "%s(%d): Timeout Waiting For Done Pin\n", __FILE__, __LINE__);
				break;
			}
		}

		if ( status == 0x00 ) {
			gbFPGA = TRUE;
			printf ( "%s(%d): FPGA at 0x%X Programmed Successfully.\n", __FILE__, __LINE__, g_pAddress );
		}
	}
	else 
		printf ("%s(%d): Timeout Waiting For UART at 0x%X to Get Ready\n", __FILE__, __LINE__, g_pAddress );
	
	EnableProgrammingMode ( FALSE );

	// The main reason for this is to set the print enable bit to 0
	EnableCommandMode ( TRUE );
	WriteByte ( 0x00, 0x00 );	
	WriteByte ( 0x00, 0x01 );	
	WriteByte ( 0x00, 0x00 );	
	EnableCommandMode ( FALSE );
	
	FPGA_UNLOCK ();
	
	return status;
}
*/

void Set_ICR(UCHAR data)
{
	FPGA_LOCK ();
	
	WriteByte ( IER, 0x00 );   // Disable Interrupts

	EnableCommandMode ( TRUE );
		WriteByte ( CMD_REG, 0x01 );	// Write mode; Start Address = 0x01 ICR
		WriteByte ( CMD_REG, 0x01 );	// Bytes To Transfer
		WriteByte ( CMD_REG, data);		// Write data
	EnableCommandMode ( FALSE );

	WriteByte ( IER, 0x0F );   // Enable Interrupts
	
	FPGA_UNLOCK ();
}

int Activate ( void ) 
{
	memset (&gPHCFlags, 0, sizeof (gPHCFlags));
	
	if (!gbFPGA) {
		printf ("%s(%d): Activate failed: FPGA not loaded\n", __FILE__, __LINE__); return SUCCESS;	
		return 0;	
	}
	
	FPGA_LOCK ();
	
	WriteByte ( LCR, 03 );     // Reset LCR;
	WriteByte ( IER, 0x00 );   // Disable Interrupts

	ReadByte (IIR); //printf ("%s(%d): ***** TODO ***** Activate\n", __FILE__, __LINE__); return SUCCESS;
	
	int nnaGpioSetInterrupt = naGpioSetInterrupt (4, 0, 1);
	printf ("%s(%d): naGpioSetInterrupt: %d\n", __FILE__, __LINE__, nnaGpioSetInterrupt);
		
	//setup ISR
	int nnaIsrInstall = naIsrInstall(EXTERNAL2_INTERRUPT, irq3Isr, (void *)&gPHCFlags);
	printf ("%s(%d): naIsrInstall: %d\n", __FILE__, __LINE__, nnaIsrInstall);
	
	if (nnaIsrInstall == SUCCESS) {
	//setup GPIO
	/*
		int nNAconfigureGPIOpin = NAconfigureGPIOpin(101, NA_GPIO_FUNC2_STATE, 0);
		printf ("%s(%d): NAconfigureGPIOpin: %d\n", __FILE__, __LINE__, nNAconfigureGPIOpin);
		
		if (nNAconfigureGPIOpin != SUCCESS) {
			FPGA_UNLOCK ();
			return -1;
		}
	*/
	}
	
	Set_ICR ( 0x05 | 0x10 );	// Set bit 4 to 1 for ISA interrupts
	WriteByte ( IER, 0x0F );	// Enable Interrupts
	
	FPGA_UNLOCK ();
	
	return SUCCESS;
}

void ClearOnBoardRam()
{
// Each board has 256k of RAM divided into 2 128K Buffers
	unsigned char Data [ 1024 ];
	unsigned char RegValue;
	int i = 0;

	FPGA_LOCK ();
	
	memset( Data, 0x00, sizeof (Data) );
	
	// Select buffer 1
	EnableCommandMode ( TRUE );
		WriteByte ( 0x00, 0x80 );
		WriteByte ( 0x00, 0x01 );
		RegValue = ReadByte ( 0x00 );
		WriteByte ( 0x00, 0x00 );
		WriteByte ( 0x00, 0x01 );
		WriteByte ( 0x00,  (RegValue | 0x07) );
	EnableCommandMode ( FALSE );

	EnableDataTransferMode ( TRUE, WRITE_MODE );
		for ( i = 0; i < 256; i++ )
			WriteBuffer ( 0x00, Data, sizeof (Data) );
	EnableDataTransferMode ( FALSE, WRITE_MODE );

	EnableCommandMode ( TRUE );
		WriteByte ( 0x00, 0x00 );
		WriteByte ( 0x00, 0x01 );
		WriteByte ( 0x00,  RegValue );
	EnableCommandMode ( FALSE );
	
	FPGA_UNLOCK ();
}

void SetPrintDelay ( unsigned short nDelay )
{
	FPGA_LOCK ();
	
	unsigned char n [2] = { LOBYTE (nDelay), HIBYTE (nDelay) };
	
	WriteRegister (PHOTO_DELAY_REG, n, ARRAYSIZE (n));
	//WriteRegister (PHOTO_DELAY_REG, &nDelay, sizeof (nDelay));
	
	FPGA_UNLOCK ();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Sets the Product Lenght.
// The MPHC treats this as a delay, so this is the number of columns to delay after the
// image is printed before allowing the next photocell.
//////////////////////////////////////////////////////////////////////////////////////////////////
void SetProductLen ( unsigned short nLen )
{
	FPGA_LOCK ();
	
	unsigned char n [2] = { LOBYTE (nLen), HIBYTE (nLen) };
	
	WriteRegister (PRODUCT_LEN_REG, n, ARRAYSIZE (n));
	//WriteRegister (PRODUCT_LEN_REG, &nLen, sizeof (nLen));
	
	FPGA_UNLOCK ();
}

double ReadLineSpeed ( void )
{
	FPGA_LOCK ();
	
	unsigned char value [2] = { 0 };
	unsigned short wSpeed = 0;
	
	ReadRegister ( SPEED_REG, value, sizeof ( value ) );
	wSpeed = value[1];
	wSpeed <<= 8;
	wSpeed |= value[0];

	double dSpeed = ((double)wSpeed / (double)((double) FTACH_100/ (double)100));
	
	switch (fj_CVT.pfsys->lEncoderDivisor) {
	case 1:					break; // 300
	default:
	case 2:	dSpeed /= 2.0;	break; // 150
	case 3:	dSpeed /= 1.5;	break; // 200
	}
		
	// 60 fpm: 0x02 0x58
	//printf ("%s(%d): ReadLineSpeed: 0x%04X [0x%02X 0x%02X]\n", __FILE__, __LINE__, wSpeed, value [1], value [0]);
	
	FPGA_UNLOCK ();
	
	return dSpeed;
}
//////////////////////////////////////////////////////////////////////////////////////////////////
//Sets the Length of the actual image.  This needs to include flush buffer
//////////////////////////////////////////////////////////////////////////////////////////////////
void SetImageLen( unsigned short nImageCols)
{
	FPGA_LOCK ();
	
	unsigned char n [2] = { LOBYTE(nImageCols), HIBYTE(nImageCols) };
	
	WriteRegister (IMAGE_LEN_REG, n, ARRAYSIZE (n));
	//WriteRegister (IMAGE_LEN_REG, &nImageCols, sizeof (nImageCols));
	
	FPGA_UNLOCK ();
}

void ProgramSlantRegs(tagSlantRegs Regs)
{
	FPGA_LOCK ();
	WriteRegister ( 0x24, &Regs, sizeof ( Regs ) );
	FPGA_UNLOCK ();
}

void ReadAmsRegs(tagAMSRegs *Regs)
{
	FPGA_LOCK ();
	ReadRegister ( 0x30, Regs, sizeof ( Regs ) );
	FPGA_UNLOCK ();
}

void ProgramAmsRegs(tagAMSRegs Regs)
{
	FPGA_LOCK ();
	WriteRegister ( 0x30, &Regs, sizeof ( Regs ) );
	FPGA_UNLOCK ();
}

void FireAMS( void )
{
	FPGA_LOCK ();
	FPGA_UNLOCK ();
}

void ReadConfiguration(tagRegisters *Regs)
{
	FPGA_LOCK ();
	ReadRegister ( 0x00, Regs, sizeof ( Regs ) );
	FPGA_UNLOCK ();
}

unsigned short ComputeSpeedGateWidth(unsigned short nEncoderRes)
{
   return ( unsigned short )((double) (FTACH_100 * FGATE ) / (double) ( 20 * nEncoderRes));
}

void SetSpeedGateWidth( unsigned short nEncRes)
{
	FPGA_LOCK ();
	
	unsigned short nSpeed = ComputeSpeedGateWidth (nEncRes);
	unsigned char n [2] = { LOBYTE (nSpeed), HIBYTE (nSpeed) };
	
	//printf ("%s(%d): SetSpeedGateWidth: 0x%04X [0x%02X 0x%02X]\n", __FILE__, __LINE__, nSpeed, n [0], n [1]);
	WriteRegister (0x0E, n, ARRAYSIZE (n));
	
	FPGA_UNLOCK ();
}

UCHAR EncodeIndicators(tagIndicators *Indicators)
{
	UCHAR uStatus = 0;
	
	uStatus |= Indicators->HVOK		? 0x10 : 0x00;
	uStatus |= Indicators->InkLow	? 0x20 : 0x00;
	uStatus |= Indicators->AtTemp	? 0x40 : 0x00;
	uStatus |= Indicators->HeaterOn	? 0x80 : 0x00;
	
	return uStatus;
}

static ULONG lHVOKTicks = 0;
static ULONG lInkLowTicks = 0;
static ULONG lAtTempTicks = 0;

void TriggerLowInk ()
{
	lInkLowTicks = tx_time_get () - NS_MILLISECONDS_TO_TICKS (5000);
}

void DecodeIndicators(tagIndicators *Indicators, UCHAR uStatus)
{
	ULONG lNow = tx_time_get ();
	unsigned char uMask = 0x10;

	memset (Indicators, 0, sizeof (tagIndicators));

	Indicators->HVOK		= (uStatus & uMask) ? TRUE : FALSE; uMask <<= 1;
	Indicators->InkLow		= (uStatus & uMask) ? TRUE : FALSE; uMask <<= 1;
	Indicators->AtTemp		= (uStatus & uMask) ? TRUE : FALSE; uMask <<= 1;
	Indicators->HeaterOn	= (uStatus & uMask) ? TRUE : FALSE;

	if (Indicators->HVOK) 		lHVOKTicks 		= tx_time_get ();
	if (Indicators->AtTemp) 	lAtTempTicks 	= tx_time_get ();
	if (!Indicators->InkLow) 	lInkLowTicks 	= tx_time_get ();
	
	if (!Indicators->HVOK) 
		Indicators->HVOK 	= ((lNow - lHVOKTicks) 	> NS_MILLISECONDS_TO_TICKS (4000)) 		? FALSE : TRUE;
	if (!Indicators->AtTemp) 
		Indicators->AtTemp 	= ((lNow - lAtTempTicks) > NS_MILLISECONDS_TO_TICKS (30000))	? FALSE : TRUE;
	if (Indicators->InkLow) 
		Indicators->InkLow 	= ((lNow - lInkLowTicks) > NS_MILLISECONDS_TO_TICKS (4000)) 	? TRUE : FALSE;
}

/*
void GetIndicators(tagIndicators *Indicators)
{
	unsigned char uData;
	
	ReadRegister (IFACE_STAUS_REG, &uData, sizeof ( uData ) );
	DecodeIndicators (Indicators, uData);	
}
*/

void WriteBufferByteSwapped (unsigned char offset, unsigned char *pBuffer, unsigned long lBytes)
{
	FPGA_LOCK ();
	
	unsigned long i;
	
	for (i = 0; i < lBytes; i += 2) {
		WriteByte (offset, pBuffer [i + 1]);
		WriteByte (offset, pBuffer [i]);
	}
	
	FPGA_UNLOCK ();
}

void TxImage ( unsigned short wStartCol, unsigned short wNumCols, unsigned char nBPC, unsigned char * pImage )
{
	FPGA_LOCK ();
	
	unsigned short Location = wStartCol * ( nBPC / 2 );

	EnableCommandMode ( TRUE );
		WriteByte ( 0x00, 0x08 );
		WriteByte ( 0x00, 0x02 );
		unsigned char byte;
		byte = Location & 0x00FF;
		WriteByte ( 0x00, byte );
		byte = ((Location>>8) & 0xFF); 
		WriteByte ( 0x00, byte );
	EnableCommandMode ( FALSE );
	
	EnableDataTransferMode ( TRUE, WRITE_MODE );
		WriteBuffer ( 0x00, pImage, nBPC * wNumCols );
		//WriteBufferByteSwapped ( 0x00, pImage, nBPC * wNumCols );
	EnableDataTransferMode ( FALSE, WRITE_MODE );
	
	FPGA_UNLOCK ();
}

void RxImage ( unsigned short wStartCol, unsigned short wNumCols, unsigned char nBPC, unsigned char * pImage )
{
	FPGA_LOCK ();
	
	unsigned short Location = wStartCol * ( nBPC / 2 );
		EnableCommandMode ( TRUE );
		WriteByte ( 0x00, 0x08 );
		WriteByte ( 0x00, 0x02 );
		unsigned char byte;
		byte = Location & 0x00FF;
		WriteByte ( 0x00, byte );
		byte = ((Location>>8) & 0xFF); 
		WriteByte ( 0x00, byte );
	EnableCommandMode ( FALSE );
	
	EnableDataTransferMode ( TRUE, READ_MODE );
		ReadBuffer ( 0x00, pImage, nBPC * wNumCols );
	EnableDataTransferMode ( FALSE, READ_MODE );
	
	FPGA_UNLOCK ();
}
