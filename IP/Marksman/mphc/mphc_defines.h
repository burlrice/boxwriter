#ifndef MPHC_DEFINES_H_
#define MPHC_DEFINES_H_
	#define DEFINES_H_

	#define bool BOOL
	#define LOBYTE(w)           ((BYTE)(((w)) & 0xff))
	#define HIBYTE(w)           ((BYTE)((((w)) >> 8) & 0xff))
	#define BYTESWAP(w) 		((w) = (LOBYTE (w) << 8) | HIBYTE (w))

	#define WRITE_MODE	0
	#define READ_MODE	1
	
	#define PC_INT				0x08
	#define AMS_STATE_CHANGE	0x04
	#define PRINT_CYCLE_STATE	0x02
	#define OTB_INT				0x10
	
	#define CMD_REG		0x00
	#define IER			0x01
	#define IIR			0x02
	#define LCR			0x03
	#define MCR			0x04
	#define LSR			0x05
	#define MSR			0x06
	#define SCRATCH_PAD	0x07
	#define OTB_STATUS	SCRATCH_PAD
	#define DLAB		0x83
	#define DLL			0x00
	#define DLH			0x01
	
	#define DTR			0x01
	#define RTS			0x02	
	
	#define LCR_6_SPACE_1	0x29	
	#define LCR_6_SPACE_2	0x3D	// Data Transfer Mode Read
	#define LCR_6_MARK_2	0x2D	// Data Transfer mode Write.
	
	#define LCR_7_SPACE_2	0x3E	// Non Vol .
	
	#define LCR_8_NONE_1	0x03	// Normal Operation mode.
	#define LCR_8_MARK_2	0x2F	// Programming Mode
	#define LCR_8_SPACE_2	0x3F	// Command Mode.
	
	#define CONTROL_REG			0x00
	#define INTR_CONTROL_REG	0x01
	#define IFACE_CONTROL_REG	0x02
	#define IFACE_STAUS_REG		0x04
	#define DIV_CONTROL_REG		0x06
	#define IMAGE_OFFSET_REG	0x08
	#define INT_TACH_REG		0x0A
	#define	INT_PHOTOCELL_REG	0x0C
	#define SPEED_REG			0x0E
	#define FIRE_PLUSE_REG		0x10
	#define PHOTO_DELAY_REG		0x20
	#define IMAGE_LEN_REG		0x22
	#define IMAGE_ADDR0_REG		0x24
	#define IMAGE_ADDR1_REG		0x26
	#define IMAGE_ADDR_INC_REG	0x28
	#define COLUMN_ADJUST_REG	0x2A
	#define PRODUCT_LEN_REG		0x2C
	
#endif /*MPHC_DEFINES_H_*/
