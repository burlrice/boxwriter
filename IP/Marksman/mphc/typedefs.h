#ifndef TYPEDEFS_H_
#define TYPEDEFS_H_

#ifdef __cplusplus
namespace NEXT {
#endif

typedef enum 
{
	STROBESTATE_OFF = 0,
	STROBESTATE_ON,
} STROBESTATE;

typedef struct 
{
	STROBESTATE m_green;
	STROBESTATE m_yellow;
	STROBESTATE m_red;
} STROBESTRUCT;

typedef struct 
{
	STROBESTRUCT m_Ready;
	STROBESTRUCT m_APS;
	STROBESTRUCT m_LowTemp;
	STROBESTRUCT m_HighVoltage;
	STROBESTRUCT m_LowInk;
	STROBESTRUCT m_OutOfInk;
	STROBESTRUCT m_Driver;
} STROBE_SETTING_STRUCT;

typedef struct {
	unsigned char flagISR;
	unsigned char flagPhotoInt;
	unsigned char flagEOPInt;
	unsigned char flagSerial;
	unsigned char flagReserved;
	BOOL AmsActive;
	BOOL PhotocellActivated;	// Photocell event has occured
	BOOL StartOfPrint;
	BOOL EndOfPrint;
}tagPHC_Flags;

typedef struct tagImagePkt
{
	ULONG _lAddress;
	ULONG _lStartCol;
	ULONG _lBytes;
	ULONG _lBytesPerCol;
}tagImagePkt;

typedef struct tagIndicators
{
   BOOL HVOK;
   BOOL AtTemp;
   BOOL InkLow;
   BOOL HeaterOn;
}tagIndicators;

typedef struct tagCtrlReg {							// Control Register
	unsigned char _PrintEnable		: 1;	// 7 	// 1-Enable Printing.
	unsigned char _PrintBuffer		: 1;	// 6	// 0-Buffer0, 1-Buffer1 // Selects the buffer which the printer uses when we filling the image. 
	unsigned char _FireAMS	   		: 1;	// 5 	// Fire AMS Cycle.
	unsigned char _DoublePulse		: 1;	// 4	// Enable Double Pulse.
	unsigned char _Inverted			: 1;	// 3	// 0-Normal, 1-Inverted
	unsigned char _SingleBuffer		: 1;	// 2	// 1-Single Buffer, 0-DoubleBuffers.
	unsigned char _HeadType			: 2;	// 0-1 	// 0-Disabled, 1-Slanted, 2 Staggered, 3-Graphic
}__attribute__((__packed__)) tagCtrlReg;

typedef struct tagInterruptCtrlReg {			// interrupt Control Register
	unsigned char _ErrDrv		: 3;	// 5-7	// Strobe: 5=Red, 6=Green, 7=Yellow 	*****PHJ
	unsigned char _Mode		: 1;	// 4	// Interrupt mode	0-Debug Sys Bus, 1-Net	*****PHJ
	unsigned char _Channel		: 4;	// 0-3	// Interrupt Channel Number [0 - 7]
//Need to Change
//	unsigned char _IntEdge		: 1	// 3    // 0=Leading Edge  1=Falling Edge	*****PHJ
//	unsigned char _Channel		: 2	// 0-1  // Interrupt Channel Number [0 - 3]	*****PHJ
}__attribute__((__packed__)) tagInterruptCtrlReg;

typedef struct tagInterfaceCtrlReg {			// Interface Control Register
	unsigned char _OTBPhoto2	: 1;	// 7	// Drive Photocell Out 	*****PHJ
	unsigned char _OTBPhoto1	: 1;	// 6	// Not used		*****PHJ
	unsigned char _PhotoSrc		: 2;	// 4-5	// Photocell Source. 0-External(head), 1-Internal, 2-Daisy PC, 3-Not Used*****PHJ
	unsigned char _OTBTach2		: 1;	// 3	// Drive Daisy Tach 	*****PHJ
	unsigned char _OTBTach1		: 1;	// 2	// Not used		*****PHJ
	unsigned char _TachSrc		: 2;	// 0-1	// Tach Source; 0-External, 1-internal, 2-Not used, 3-Not used	*****PHJ
}__attribute__((__packed__)) tagInterfaceCtrlReg;

typedef struct tagOTBStatusReg {			// Over-the-Top-Bus Status Register
	unsigned char _Reserved		: 1;	// 7	// Reserved.
	unsigned char _OTBI			: 7;	// 0-6	// OTB Interrupt
}__attribute__((__packed__)) tagOTBStatusReg;

typedef struct tagInterfaceStatusReg {			// Interface Status Register
	unsigned char _HeaterOn		: 1;	// 7	// Heater ON
	unsigned char _AtTemp		: 1;	// 6	// At Temp.
	unsigned char _InkLow		: 1;	// 5	// Ink Low
	unsigned char _HVOK			: 1;	// 4	// High Voltage Ok
	unsigned char _HeadError	: 1;	// 3	// Print Head Error.
	unsigned char _Photocell	: 1;	// 2	// Photocell Interrupt.
	unsigned char _TachIntr		: 1;	// 1	// Tachometer interrupt
	unsigned char _OTBIP		: 1;	// 0	// Over-the-Top interrupt Pending. Not used  *****PHJ
	unsigned char _Reserved		: 8;	// 8-15	// Reserved. (Read as 0 ).
}__attribute__((__packed__)) tagInterfaceStatusReg;

typedef struct tagDividerCtrlReg {				// Divider Control Register
	unsigned char _Reserved		: 1;	// 7	// Reserved
	unsigned char _TackIntRate	: 3;	// 3-6	// Tach Interrupt Rate 16*2^x
	unsigned char _SClkRate		: 2;	// 2-3	// 3- 7.3728MHz, 2- 3.6864MHz, 1- 1.8432MHz, 0- 921.6kHz
	unsigned char _TachDiv		: 2;	// 0-1	// 0=150dpi, 1=200dpi, 2=300dpi, 3=120dpi		*****PHJ
}__attribute__((__packed__)) tagDividerCtrlReg;

/* 
typedef struct tagInterruptCtrlReg {			// interrupt Control Register
	unsigned char _ErrDrv		: 3;	// 5-7	// Do not know what this is.
	unsigned char _Mode			: 1;	// 4	// Interrupt mode	0-OTB, 1-ISA
	unsigned char _Channel		: 4;	// 0-3	// Interrupt Channel Number [0 - 7]
}__attribute__((__packed__)) tagInterruptCtrlReg;

typedef struct tagInterfaceCtrlReg {			// Interface Control Register
	unsigned char _OTBPhoto2	: 1;	// 7	// Drive OTB Photocell 2 (PC2)
	unsigned char _OTBPhoto1	: 1;	// 6	// Drive OTB Photocell 1 (PC1)
	unsigned char _PhotoSrc		: 2;	// 4-5	// Photocell Source. 0-External, 1-Internal, 2-OTB PC1, 3-OTB PC2
	unsigned char _OTBTach2		: 1;	// 3	// Drive OTB Tach 2
	unsigned char _OTBTach1		: 1;	// 2	// Drive OTB Tach 1
	unsigned char _TachSrc		: 2;	// 0-1	// Tach Source; 0-External, 1-internal, 2-OTB Tach 1, 3-OTB Tach2
}__attribute__((__packed__)) tagInterfaceCtrlReg;

typedef struct tagOTBStatusReg {				// Over-the-Top-Bus Status Register
	unsigned char _Reserved		: 1;	// 7	// Reserved.
	unsigned char _OTBI			: 7;	// 0-6	// OTB Interrupt
}__attribute__((__packed__)) tagOTBStatusReg;

typedef struct tagInterfaceStatusReg {			// Interface Status Register
	unsigned char _HeaterOn		: 1;	// 7	// Heater ON
	unsigned char _AtTemp		: 1;	// 6	// At Temp.
	unsigned char _InkLow		: 1;	// 5	// Ink Low
	unsigned char _HVOK			: 1;	// 4	// High Voltage Ok
	unsigned char _HeadError	: 1;	// 3	// Print Head Error.
	unsigned char _Photocell	: 1;	// 2	// Photocell Interrupt.
	unsigned char _TachIntr		: 1;	// 1	// Tachometer interrupt
	unsigned char _OTBIP		: 1;	// 0	// Over-the-Top interrupt Pending.
	unsigned char _Reserved		: 8;	// 8-15	// Reserved. (Read as 0 ).
}__attribute__((__packed__)) tagInterfaceStatusReg;

typedef struct tagDividerCtrlReg {				// Divider Control Register
	unsigned char _Reserved		: 1;	// 7	// Reserved
	unsigned char _TackIntRate	: 3;	// 3-6	// Tach Interrupt Rate 16*2^x
	unsigned char _SClkRate		: 2;	// 2-3	// 3- 7.3728MHz, 2- 3.6864MHz, 1- 1.8432MHz, 0- 921.6kHz
	unsigned char _TachDiv		: 2;	// 0-1	// Reserved
}__attribute__((__packed__)) tagDividerCtrlReg;
*/

typedef struct tagImgBufBaseAddrReg {			// Image Buffer Base Address Register
	unsigned char _WindowBase	: 6;	// 2-7	// ???
	unsigned char _WindowSize	: 2;	// 0-1	// Window Size: 0- Disabled, 1- 16kB, 2- 32kB, 3- 64kB
}__attribute__((__packed__)) tagImgBufBaseAddrReg;

typedef struct tagImgLenReg {			// Image Buffer Offset Register
	unsigned short _Len	: 16;
}__attribute__((__packed__)) tagImgLenReg;

typedef struct tagIntTachGenReg {		// Internal Tach Generator Register
	unsigned short _TachGenFreg	: 16;	// Freguency given by (desired freg / 7.3728) * 2^16
}__attribute__((__packed__)) tagIntTachGenReg;

typedef struct tagIntPhotoGenReg {		// Internal Photocell Generator Count Register
	unsigned short _PhotoGenFreg	: 16;
}__attribute__((__packed__)) tagIntPhotoGenReg;

typedef struct tagConveyorSpeed {		// Conveyor Speed Register
	unsigned short _Speed		: 16; 	// Read as ft/min
}__attribute__((__packed__)) tagConveyorSpeed;

typedef struct tagFireVibPulse {		// Fire / Vibration Pluse Register
	unsigned short _FirePulseDelay	: 16; // 
	unsigned short _FirePulseWidth	: 16; // 
	unsigned short _DampPulseDelay	: 16; // 
	unsigned short _DampPulseWidth	: 16; // 
	// Remaining values are for vibration which are no longer used.
	unsigned short _v5				: 16; // 
	unsigned short _v6				: 16; // 
	unsigned short _v7				: 16; // 
	unsigned short _v8				: 16; // 
}__attribute__((__packed__)) tagFireVibPulse;

typedef struct tagPrintDelay {			// Print Delay Register.
	unsigned short _PrintDelay	: 16; 	// (Value = Columns)
}__attribute__((__packed__)) tagPrintDelay;

typedef struct tagImageLen {			// Image Length Register.
	unsigned short _Len			: 16; 	// (Value = Columns)
}__attribute__((__packed__)) tagImageLen;

typedef struct tagImageStartAddr {		// Address of Image Register
	unsigned short _Addr1		: 16; 	// Address of first image WORD 
	unsigned short _Addr2		: 16; 	// Address of second image WORD
}__attribute__((__packed__)) tagImageStartAddr;

typedef struct tagAddrIncFactor {		// Address Increment Factor Register
	unsigned short _Incr		: 16; 	// 
}__attribute__((__packed__)) tagAddrIncFactor;

typedef struct tagColAdjustFactor {		// Column Adjust Factor Register
	unsigned short _Adjust		: 16; 	// 
}__attribute__((__packed__)) tagColAdjustFactor;

typedef struct tagProdLenDelay {		// Product Length Delay Register
	unsigned short _Delay		: 16; 	// ( Value = Columns )
}__attribute__((__packed__)) tagProdLenDelay;

typedef struct tagRegisters {
	tagCtrlReg				_CtrlReg;				// Control Register.
	tagInterruptCtrlReg		_Interrupt_Reg;			// interrupt Control Register.
	tagInterfaceCtrlReg		_InterfaceCtrlReg;		// Interface Control Register.
	tagOTBStatusReg			_OTBStatusReg;			// Over-the-Top-Bus Status Register.
	tagInterfaceStatusReg	_InterfaceStatusReg;	// Interface Status Register.
	tagDividerCtrlReg		_DividerCtrlReg;		// Divider Control Register.
	tagImgBufBaseAddrReg	_ImgBufBaseAddr;		// Image Buffer Base Address Register
	tagImgLenReg			_ImgLenReg;				// Image Length Register
	tagIntTachGenReg		_IntTachGenReg;			// Internal Tach Generator Register
	tagIntPhotoGenReg		_IntPhotoGenReg;		// Internal Photocell Generator Count Register
	tagConveyorSpeed		_Conveyor;				// Conveyor Speed Register
	tagFireVibPulse			_FireVibPulse;			// Fire / Vibration Pulse Register
	tagPrintDelay			_PrintDelay;			// Print Delay Register.
	tagImageLen				_ImageLen;				// Image Length Register.
	tagImageStartAddr		_ImageStartAddr;		// Address of Image Register
	tagAddrIncFactor		_AddrIncFactor;			// Address Increment Factor Register
	tagColAdjustFactor		_ColAdjustFactor;		// Column Adjust Factor Register
	tagProdLenDelay			_ProdLenDelay;			// Product Length Delay Register
}__attribute__((__packed__)) tagRegisters;

typedef struct tagSetupRegs {
	tagCtrlReg				_CtrlReg;				// Control Register.
	tagInterruptCtrlReg		_Interrupt_Reg;			// interrupt Control Register.
	tagInterfaceCtrlReg		_InterfaceCtrlReg;		// Interface Control Register.
	tagOTBStatusReg			_OTBStatusReg;			// Over-the-Top-Bus Status Register.
	tagInterfaceStatusReg	_InterfaceStatusReg;	// Interface Status Register.
	tagDividerCtrlReg		_DividerCtrlReg;		// Divider Control Register.
	tagImgBufBaseAddrReg	_ImgBufBaseAddr;		// Image Buffer Base Address Register
	tagImgLenReg		    _ImgLenReg;				// Image Length Register
	tagIntTachGenReg		_IntTachGenReg;			// Internal Tach Generator Register
	tagIntPhotoGenReg		_IntPhotoGenReg;		// Internal Photocell Generator Count Register
}__attribute__((__packed__)) tagSetupRegs;

typedef struct tagSlantRegs {
	tagImageStartAddr		_ImageStartAddr;		// Address of Image Register
	tagAddrIncFactor		_AddrIncFactor;			// Address Increment Factor Register
	tagColAdjustFactor		_ColAdjustFactor;		// Column Adjust Factor Register
}__attribute__((__packed__)) tagSlantRegs;

typedef struct tagAMSRegs
{
   unsigned short A1;	// A1:Number of pulse sets ( purge cycles )
   unsigned short A2;	// A2:Time from vaccum on until purge cyle start
   unsigned short A3;	// A3:
   unsigned short A4;	// A4:
   unsigned short A5;	// A5:
   unsigned short A6;	// A6:
   unsigned short A7;	// A7:
}__attribute__((__packed__)) tagAMSRegs;

#ifdef __cplusplus
}; //namespace NEXT 
#endif

#endif /*TYPEDEFS_H_*/
