#ifndef MPHC_H_
#define MPHC_H_
	#define MPHC_H_
	#include "mphc_defines.h"
	#include "typedefs.h"

	#define FPGA_LOCK()			FpgaLock (TX_WAIT_FOREVER, __FILE__, __LINE__)
	#define FPGA_TRY_LOCK(w)	FpgaLock ((w), __FILE__, __LINE__)
	#define FPGA_UNLOCK() 		FpgaUnlock (__FILE__, __LINE__)

	BOOL FpgaLock (ULONG lTicks, const char * pszFile, ULONG lLine);
	BOOL FpgaUnlock (const char * pszFile, ULONG lLine);
	
	unsigned char ReadByte( unsigned char offset );
	unsigned char ReadByte( unsigned char offset );
	void ReadRegister( int RegID, void *pData, int nBytes);
	void WriteRegister( int RegID, void *pData, int nBytes);
	void SetMphcBaseAddress ( unsigned short * pAddress );
	void WriteByte (unsigned char offset, unsigned char data);
	void ReadBuffer (unsigned char offset, unsigned char *pBuffer, unsigned long lBytes);
	void WriteBuffer (unsigned char offset, unsigned char *pBuffer, unsigned long lBytes);
	void EnableProgrammingMode ( bool Enabled );
	void EnableDataTransferMode( bool Enabled, bool Direction);
	void EnableCommandMode( bool Enabled);
	void ProgramSlantRegs(tagSlantRegs Regs);
	void ReadAmsRegs(tagAMSRegs *Regs);
	void ProgramAmsRegs(tagAMSRegs Regs);
	void ReadConfiguration(tagRegisters *Regs);
	void SetSpeedGateWidth( unsigned short nEncRes);
	void DecodeIndicators(tagIndicators *Indicators, unsigned char uData);
	UCHAR EncodeIndicators(tagIndicators *Indicators);
	void SetImageLen( unsigned short nImageCols);
	void FireAMS( void );
	void ClearOnBoardRam();
	void TxImage ( unsigned short wStartCol, unsigned short wNumCols, unsigned char nBPC, unsigned char * pImage );
	void RxImage ( unsigned short wStartCol, unsigned short wNumCols, unsigned char nBPC, unsigned char * pImage );
	void SetProductLen ( unsigned short nLen );
	double ReadLineSpeed ( void );
	int ProgramFPGA(void * pCode, unsigned long lBytes);
	BYTE revbyte (BYTE b);
	void TriggerLowInk ();
	
	extern TX_MUTEX gMutexFPGA;
#endif /*MPHC_H_*/
