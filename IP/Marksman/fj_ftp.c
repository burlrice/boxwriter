/*
 * This file implements a NET+ARM FTP server application.
 */

#include <fservapi.h>

#include "fj.h"
#include "fj_mem.h"
//#include "fj_memstorage.h"
#include "fj_misc.h"
#include "fj_font.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_printhead.h"
#include "fj_system.h"

extern const CHAR fj_bmpID[];
extern const CHAR fj_FontID[];
extern const CHAR fj_LabelID[];
extern const CHAR fj_MessageID[];
extern const CHAR fj_TextID[];
extern const CHAR fj_BarCodeID[];
extern const CHAR fj_BitmapID[];
extern const CHAR fj_DynimageID[];
extern const CHAR fj_DatetimeID[];
extern const CHAR fj_CounterID[];

#define USERNAME_SIZE   16
#define PASSWORD_SIZE   16

extern ROMENTRY        fj_RomTable[];

//
// some FTP commands produce a lot of output.
// these FTP commands get the ID of the FTP data socket and write to it.
// in order to use code written for the socket connections,
// FTP must use one of the socket structures.
// this index is set up when FTP is initialized.
//
static LONG   fj_ftp_lSocketIndex = -1;	// index into socket table for FTP socket

// only one FTP connection at a time is allowed, so that fields are OK here.
										// pointer to download in progress
static LPROMENTRY fj_ftp_pRomEntry = NULL;
static ULONG      fj_ftp_lCurrentLength;// length of download
//static LPBYTE     fj_ftp_lRamBuffer = NULL;                      // buffer for ftp download into ram
//static BOOL       fj_ftp_bBufferInUse = FALSE;                   // set from start of read until end of write into ROM
// file name being downloaded
static CHAR       fj_ftp_stFileName[FJSYS_LARGEST_USER_NAME+16];

static BOOL   fj_ftp_bFileMemStor = FALSE;
static BOOL   fj_ftp_bFileGood = FALSE;
static LONG   fj_ftp_lDivisor;

static char * fj_ftp_pConnect    = NULL;// pointer to the ftpsvr connect message
static char * fj_ftp_pCWDError   = NULL;// pointer to the ftpsvr CWD error message
static char * fj_ftp_pRETRError  = NULL;// pointer to the ftpsvr RETR error message
static char * fj_ftp_pSTORError  = NULL;// pointer to the ftpsvr STOR error message
static char * fj_ftp_pLISTError  = NULL;// pointer to the ftpsvr LIST error message
static char * fj_ftp_pDELEError1 = NULL;// pointer to the ftpsvr DELE error message ?
static char * fj_ftp_pDELEError2 = NULL;// pointer to the ftpsvr DELE error message ?
static char * fj_ftp_pDELEError3 = NULL;// pointer to the ftpsvr DELE error message ?
static char * fj_ftp_pDELEError4 = NULL;// pointer to the ftpsvr DELE error message ?

typedef struct ftpdir
{
	LONG   lParent;						// parent directory number
	LPCSTR pName;						// external directory name
	LPCSTR pExt;						// file extension
	LPCSTR pAll;						// name of file that contains all elements
	LPCSTR pMemStorID;					// memstor ID
	LONG   lCmdGetOne;					// socket command to get one element
	LONG   lCmdGetAll;					// socket command to get all elements
	LPCSTR pCmdKeyword;					// keyword for socket command
} FTPDIR, FAR * LPFTPDIR;

// these constants must match the table entries below
#define DIR_ROOT  0
#define DIR_FONT  1
#define DIR_LABEL 2
#define DIR_MSG   3
#define DIR_BMP   4
static LONG   lDir = DIR_ROOT;

FTPDIR fj_ftp_dirs[] =
{
	DIR_ROOT, "",         "",     "AllData",     "",           0,             0,              "",
	DIR_ROOT, "Fonts",    ".FJF", "AllFonts",    fj_FontID,    IPC_GET_FONT,  IPC_GET_FONTS,  "FONTID=",
	DIR_ROOT, "Labels",   ".FJL", "AllLabels",   fj_LabelID,   IPC_GET_LABEL, IPC_GET_LABELS, "LABELID=",
	DIR_ROOT, "Messages", ".FJM", "AllMessages", fj_MessageID, IPC_GET_MSG,   IPC_GET_MSGS,   "MESSAGEID=",
	DIR_ROOT, "Bitmaps",  ".BMP", "AllBitmaps",  fj_bmpID,     IPC_GET_BMP,   IPC_GET_BMPS,   "BMPID="
};

// the entries in this list must match the TYPE definitions in fj_element.h
FTPDIR fj_ftp_dirs2[] =
{
	DIR_ROOT, "", "", "", NULL,          0,                0,                         "",
	DIR_ROOT, "", "", "", fj_TextID,     IPC_GET_TEXT,     IPC_GET_TEXT_ELEMENTS,     "TEXTID=",
	DIR_ROOT, "", "", "", fj_BarCodeID,  IPC_GET_BARCODE,  IPC_GET_BARCODE_ELEMENTS,  "BARCODEID=",
	DIR_ROOT, "", "", "", fj_BitmapID,   IPC_GET_BITMAP,   IPC_GET_BITMAP_ELEMENTS,   "BITMAPID=",
	DIR_ROOT, "", "", "", fj_DynimageID, IPC_GET_DYNIMAGE, IPC_GET_DYNIMAGE_ELEMENTS, "DYNIMAGEID=",
	DIR_ROOT, "", "", "", fj_DatetimeID, IPC_GET_DATETIME, IPC_GET_DATETIME_ELEMENTS, "DATETIMEID=",
	DIR_ROOT, "", "", "", fj_CounterID,  IPC_GET_COUNTER,  IPC_GET_COUNTER_ELEMENTS,  "COUNTERID="
};

static  CHAR  fj_ftp_date_time_str[44];

//
// call back routine when ROM write is complete
//
void fj_ftp_ROM_callback( LONG l1, LONG l2 )
{
	LPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	pGlobal->fj_ftp_bBufferInUse = FALSE;

	return;
}
//
// format a unix date time string for general ftp use
//
void fj_ftp_get_date_time( void )
{
	struct tm * pTM;

	// get some date here. if never been edited, use in service or manufacture date
	pTM = &(fj_CVT.pfph->tmEditVersion);
	if ( 2000 > pTM->tm_year ) pTM = &(fj_CVT.pfph->tmDateInservice);
	if ( 2000 > pTM->tm_year ) pTM = &(fj_CVT.pfph->tmDateManu);
	fj_SystemGetDateTimeUnixFileString( fj_CVT.pfsys, fj_ftp_date_time_str, pTM );
	strcat( fj_ftp_date_time_str, " " );

	return;
}
//
// set up our FTP socket entry
//
void fj_ftp_set_socket( unsigned long handle )
{
	printf ("%s(%d): fj_ftp_set_socket\n", __FILE__, __LINE__); return;
/*
	struct sockaddr_in addrPeer;
	unsigned long ipaddr;
	int iaddrLen;
	int iSocket;
	int ret;

	ipaddr = 0;
	iSocket = FSHandleToSocket(handle);
	if ( iSocket > 0 )
	{
		iaddrLen = sizeof( addrPeer );
		ret = getpeername( iSocket, &addrPeer, &iaddrLen );
		ipaddr = addrPeer.sin_addr.s_addr;
		fj_asockets[fj_ftp_lSocketIndex].lIPaddr = ipaddr;
		fj_asockets[fj_ftp_lSocketIndex].iSocketRW = iSocket;
	}
	return;
*/
}
//
// reset our FTP socket entry
//
void fj_ftp_reset_socket( void )
{
	printf ("%s(%d): fj_ftp_reset_socket\n", __FILE__, __LINE__); return;
/*
	fj_asockets[fj_ftp_lSocketIndex].iSocketRW = -1;
	fj_asockets[fj_ftp_lSocketIndex].lAckTime = 0;

	return;
*/
}
//
// find FTP socket level
//
void fj_ftp_find_level( unsigned long handle )
{
	printf ("%s(%d): fj_ftp_find_level\n", __FILE__, __LINE__); return;
/*
	int   i;

	for ( i = 0; i < FJSYS_NUMBER_SOCKETS; i++ )
	{
		if ( i != fj_ftp_lSocketIndex )
		{
			if ( ((TRUE == fj_asockets[i].bInUse)||(0 < fj_asockets[i].lSeconds)) && (fj_asockets[fj_ftp_lSocketIndex].lIPaddr == fj_asockets[i].lIPaddr) )
			{
				if ( fj_asockets[i].lSecLevel > fj_asockets[fj_ftp_lSocketIndex].lSecLevel )
				{
					fj_asockets[fj_ftp_lSocketIndex].lSecLevel = fj_asockets[i].lSecLevel;
				}
			}
		}
	}
	return;
*/
}
//
// send a buffer
//
void fj_ftp_send_buffer( unsigned long handle, char *buffer, int length )
{
	printf ("%s(%d): fj_ftp_send_buffer\n", __FILE__, __LINE__); return;
/*
	fj_ftp_set_socket( handle );
	fj_socketSendBuffer( (LPVOID)fj_ftp_lSocketIndex, (LPBYTE)buffer, length );
	fj_ftp_reset_socket();

	return;
*/
}
//
// this is not called from the NETsilicon FTP server
// it is detected by the first STOR
//
int fj_ftp_data_open(char *pFileName, unsigned long handle)
{
	printf ("%s(%d): fj_ftp_data_open\n", __FILE__, __LINE__); return -1;
/*
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	BOOL   bEdit;
	int    i, j;
	int    iRet;
	STRING str[FJSYS_LARGEST_USER_NAME+32];

	LPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;
	LPBYTE  fj_ftp_lRamBuffer   = pGlobal->fj_ftp_lRamBuffer;

	if ( TRUE == pfph->bInfoROM )
	{
		sprintf(str,"FTP DOPN %d argp=%s\n",(int)handle, pFileName);
		fj_writeInfo( str );
	}
	iRet = -1;

	fj_ftp_set_socket( handle );
	fj_ftp_find_level( handle );
	fj_ftp_reset_socket();
	// if not priviledged to save/restore, do nothing
	if ( fj_asockets[fj_ftp_lSocketIndex].lSecLevel >= FJSYS_PRIORITY_RESTORE )
	{
		// don't start if a FTP is in progress
		if ( (NULL == fj_ftp_pRomEntry) && (NULL != pFileName) && (FALSE == pGlobal->fj_ftp_bBufferInUse) )
		{
			bEdit = FALSE;
			bEdit = fj_PrintHeadIsEdit( pfph, fj_asockets[fj_ftp_lSocketIndex].lIPaddr );
			if ( TRUE == bEdit )
			{
				fj_ftp_bFileMemStor = FALSE;
				fj_ftp_bFileGood = FALSE;
				fj_ftp_pRomEntry = NULL;

				i = strlen( pFileName );
				if ( i < sizeof(fj_ftp_stFileName) )
				{
					for ( j = 0; j <= i; j++ ) str[j] = toupper( *(pFileName + j) );
					for ( j = 0; j < (sizeof(fj_ftp_dirs)/sizeof(FTPDIR)); j++ )
					{
						if ( 0 == strcmp(fj_ftp_dirs[j].pExt, &(str[i-4])) )
						{
							fj_ftp_pRomEntry = &(fj_RomTable[ROM_FONTMSG]);
							strcpy( fj_ftp_stFileName, pFileName );
							fj_ftp_bFileMemStor = TRUE;
							fj_ftp_bFileGood = TRUE;
							break;
						}
						if ( 0 == strcmp(fj_ftp_dirs[j].pAll, pFileName) )
						{
							fj_ftp_pRomEntry = &(fj_RomTable[ROM_FONTMSG]);
							strcpy( fj_ftp_stFileName, pFileName );
							fj_ftp_bFileMemStor = TRUE;
							fj_ftp_bFileGood = TRUE;
							break;
						}
					}

					for ( i = 1; i < fj_CVT.lFJRomTableCount; i++ )
					{
						if ( (ROM_FONTMSG != i) && (0 == strcmp(fj_RomTable[i].strFileName,pFileName)) )
						{
							fj_ftp_pRomEntry = &(fj_RomTable[i]);
							// clear buffer for GABIN.
							// for GABIN, the entire ROM area is written to put the length at the end.
							// having consistent zeros in unused space will shorten ROM write time.
							// not needed for ROM_LOGOJPG because it is so small.
							if ( ROM_GABIN == i )
							{
								memset( fj_ftp_lRamBuffer, 0, fj_ftp_pRomEntry->lMaxLength);
							}
							fj_ftp_bFileGood = TRUE;
							break;
						}
					}
				}

				if ( TRUE == fj_ftp_bFileGood )
				{
					fj_ftp_lCurrentLength = 0;
					pGlobal->fj_ftp_bBufferInUse = TRUE;
					iRet = 0;
				}
				else
				{
					if ( NULL != fj_ftp_pSTORError ) strcpy( fj_ftp_pSTORError, "Cancelled: File name error." );
				}
			}
			else
			{
				if ( NULL != fj_ftp_pSTORError ) strcpy( fj_ftp_pSTORError, "Cancelled: Not in edit mode." );
			}
		}
		else
		{
			if ( NULL != fj_ftp_pSTORError ) strcpy( fj_ftp_pSTORError, "Cancelled: ROM Update in progress." );
		}
	}
	else
	{
		if ( NULL != fj_ftp_pSTORError ) strcpy( fj_ftp_pSTORError, "Cancelled: Not authorized." );
	}
	return iRet;
*/
}
/*
 * fj_ftp_stor
 *
 * This routine is the handler for the FTP server STOR command.
 * This routine collects the FTP buffers into one large buffer.
 *
 * INPUT
 *
 *    bufferp    Contains data read by the FTP server.
 *    buflen     Specifies length of bufferp data.
 *    argp       Specifies file name being read.
 *    handle     Handle to FTP server client connection object.
 */
int fj_ftp_stor(char *bufferp, int buflen, char *argp, unsigned long handle)
{
	printf ("%s(%d): fj_ftp_stor\n", __FILE__, __LINE__); return -1;
/*
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	ULONG  lPrevCount;
	LONG   lMaxLength;
	int    iRet;
	STRING str[FJSYS_LARGEST_USER_NAME+88];

	CLPFJ_GLOBAL    pGlobal = fj_CVT.pFJglobal;
	LPBYTE          fj_ftp_lRamBuffer = pGlobal->fj_ftp_lRamBuffer;

	iRet = -1;

	// check for first pass
	if ( NULL == fj_ftp_pRomEntry )
	{
		iRet = fj_ftp_data_open( argp, handle );
		fj_ftp_lDivisor = 10000;
	}

	// check for good entry
	if ( NULL != fj_ftp_pRomEntry )
	{
		// safety check for same file as the one we started
		//		if ( 0 == strcmp(argp, fj_ftp_pRomEntry->strFileName) )
		{
			iRet = -1;

			lPrevCount = fj_ftp_lCurrentLength;
			fj_ftp_lCurrentLength += buflen;

			if ( TRUE == pfph->bInfoROM )
			{
				// report by 10,000, then by 100,000
				if ( fj_ftp_lCurrentLength > 100000 ) fj_ftp_lDivisor = 100000;
				if ( (lPrevCount/fj_ftp_lDivisor) != (fj_ftp_lCurrentLength/fj_ftp_lDivisor) )
				{
					sprintf(str,"FTP STOR %d %s %d bytes %d\n",(int)handle,argp,(int)buflen,fj_ftp_lCurrentLength);
					fj_writeInfo( str );
				}
			}

			lMaxLength = fj_ftp_pRomEntry->lMaxLength - 4;
			// leave room for size, height, width
			if ( fj_ftp_pRomEntry == &(fj_RomTable[ROM_LOGOJPG]) ) lMaxLength -= 12;
			if ( fj_ftp_lCurrentLength <= lMaxLength )
			{
				memcpy( fj_ftp_lRamBuffer+lPrevCount, bufferp, buflen);
				iRet = 0;
			}
			else
			{
				if ( TRUE == pfph->bInfoROM )
				{
					sprintf(str,"FTP STOR %d %s Transfer aborted. Max length %d exceeded.\n",(int)handle,argp,(int)fj_ftp_pRomEntry->lMaxLength);
					fj_writeInfo( str );
				}
				iRet = -1;
				fj_ftp_bFileGood = FALSE;
				pGlobal->fj_ftp_bBufferInUse = FALSE;
				if ( NULL != fj_ftp_pSTORError ) strcpy( fj_ftp_pSTORError, "Cancelled: File too large." );
			}
		}
	}

	//    return 0;		// for looking. will allow the FTP client to continue to send
	return iRet;						// -1 will stop the ftp client from sending more
*/
}
//
// process the close of a file transfer
//
// write the FTP RAM buffer into ROM
//
int fj_ftp_data_close(char *argp, unsigned long handle)
{
	printf ("%s(%d): fj_ftp_data_close\n", __FILE__, __LINE__); return -1;
/*
	LPFJPRINTHEAD pfph = fj_CVT.pfph;
	LPFJFONT      pff;
	ROMQUEUE      q;
	_FJ_SOCKET_MESSAGE msgOut;
	_FJ_SOCKET_MESSAGE *pMsgIn;
	LPBYTE pByte;
	LPSTR  pStr;
	LONG   lLength;
	LPSTR  apList[3];
	int    i, j, k;
	int    iRet;
	BOOL   bLoop;
	BOOL   bEdit;
	BOOL   bRet;
	CHAR   str[FJSYS_LARGEST_USER_NAME+16];

	CLPFJ_GLOBAL    pGlobal = fj_CVT.pFJglobal;
	LPBYTE          fj_ftp_lRamBuffer = pGlobal->fj_ftp_lRamBuffer;

	if ( TRUE == pfph->bInfoROM )
	{
		if ( NULL == fj_ftp_pRomEntry )pStr = "(no RomEntry)";
		else                           pStr = fj_ftp_pRomEntry->strFileName;
		sprintf(str,"FTP DCLO %d argp=%s %s %i bytes\n",(int)handle,argp,pStr,(int)fj_ftp_lCurrentLength);
		fj_writeInfo( str );
	}

	iRet = -1;

	if ( NULL != fj_ftp_pRomEntry )
	{
		if ( &fj_RomTable[0] != fj_ftp_pRomEntry )
		{
			bEdit = FALSE;
			bEdit = fj_PrintHeadIsEdit( pfph, fj_asockets[fj_ftp_lSocketIndex].lIPaddr );
			if ( TRUE == bEdit )
			{
				if( TRUE == fj_ftp_bFileMemStor )
				{
					// we need to set up for a call to fj_socketCmd
					// it should never write anything on these calls, but let's be ready for the unexpected
					fj_ftp_set_socket( handle );

					i = strlen(fj_ftp_stFileName);
					if ( i < sizeof(fj_ftp_stFileName) )
					{
						for ( j = 0; j <= i; j++ ) str[j] = toupper( fj_ftp_stFileName[j] );

						bLoop = FALSE;
						for ( k = 0; k < (sizeof(fj_ftp_dirs)/sizeof(FTPDIR)); k++ )
						{
							if ( 0 == strcmp(fj_ftp_dirs[k].pExt,&(str[i-4])) )
							{
								if ( DIR_FONT == k )
								{
									pff = fj_FontFromBuffer( (LPBYTE)fj_ftp_lRamBuffer );
									if ( NULL != pff )
									{
										bRet = fj_MemStorAddElementBinary( pfph->pMemStorEdit, (LPBYTE)fj_ftp_lRamBuffer, fj_ftp_lCurrentLength );
										if ( TRUE == bRet ) iRet = 0;
										fj_FontDestroy( pff );
									}
									break;
								}
								else if ( DIR_BMP == k )
								{
									bRet = fj_bmpCheckFormat( fj_ftp_lRamBuffer );
									if ( TRUE == bRet )
									{
										fj_ftp_stFileName[i-4] = 0;
										bRet = fj_bmpAddToMemstor( pfph->pMemStorEdit, fj_ftp_lRamBuffer, fj_ftp_stFileName );
										if ( TRUE == bRet ) iRet = 0;
									}
									break;
								}
								else
								{
									bLoop = TRUE;
								}
							}
							else if ( 0 == strcmp(fj_ftp_dirs[k].pAll,fj_ftp_stFileName) )
							{
								bLoop = TRUE;
							}
							if ( TRUE == bLoop )
							{
								k = 0;
								while ( k < fj_ftp_lCurrentLength )
								{
									pMsgIn = (_FJ_SOCKET_MESSAGE *)( fj_ftp_lRamBuffer + k );
									fj_socketCmd( pfph, (LPVOID)fj_ftp_lSocketIndex, fj_asockets[fj_ftp_lSocketIndex].lIPaddr, pMsgIn, &msgOut, fj_socketSendBuffer );
									memcpy( &lLength, &(pMsgIn->Header.Length), sizeof(ULONG) );
									k += lLength + sizeof(_FJ_SOCKET_MESSAGE_HEADER);
								}
								iRet = 0;
								break;
							}
						}
					}
					fj_ftp_reset_socket();
					fj_ftp_bFileMemStor = FALSE;
					pGlobal->fj_ftp_bBufferInUse = FALSE;
				}
				else if( TRUE == fj_ftp_bFileGood )
				{
					if ( fj_ftp_pRomEntry == &(fj_RomTable[ROM_LOGOJPG]) )
					{
						// add length of file as last word of rom space
						// add image width and length before length of file
						LONG lWidth = 160;
						LONG lHeight = 60;
						pByte = fj_ftp_lRamBuffer;
						if ( (0xff == *pByte) && (0xd8 == *(pByte+1)) )
						{
							while ( (0xda != *pByte) && ((4096-12) > (pByte-fj_ftp_lRamBuffer)) )
							{
								if ( (0xc0 <= *pByte) && (0xc3 >= *pByte) )
								{
									pByte += 4;
									lHeight = *pByte * 256 + *(pByte+1);
									pByte += 2;
									lWidth  = *pByte * 256 + *(pByte+1);
									break;
								}
								pByte++;
							}
						}
						pByte = fj_ftp_lRamBuffer + fj_ftp_pRomEntry->lMaxLength - 4;
						*((LONG *)(pByte- 4)) = (LONG)fj_ftp_lCurrentLength;
						*((LONG *)(pByte- 8)) = (LONG)lWidth;
						*((LONG *)(pByte-12)) = (LONG)lHeight;
						// now update current copy
						fj_CVT.pFJglobal->lLogoSize   = fj_ftp_lCurrentLength;
						fj_CVT.pFJglobal->lLogoWidth  = lWidth;
						fj_CVT.pFJglobal->lLogoHeight = lHeight;
						memcpy( fj_CVT.pFJglobal->pLogo, fj_ftp_lRamBuffer, fj_ftp_lCurrentLength );
						// set maximum length of romentry so that size, width, and height are written also
						fj_ftp_lCurrentLength = fj_ftp_pRomEntry->lMaxLength;
					}
					if ( fj_ftp_pRomEntry == &(fj_RomTable[ROM_GABIN]) )
					{
						// add length of file as last word of rom space
						pByte = fj_ftp_lRamBuffer + fj_ftp_pRomEntry->lMaxLength - 4;
						*((LONG *)(pByte- 4)) = (LONG)fj_ftp_lCurrentLength;
						// set maximum length of romentry so that size, width, and height are written also
						fj_ftp_lCurrentLength = fj_ftp_pRomEntry->lMaxLength;
					}
					// set lock to write ROM
					tx_semaphore_get( &(fj_CVT.pFJglobal->semaROMLock), TX_WAIT_FOREVER );
					q.lRomEntry   = ((LPBYTE)fj_ftp_pRomEntry - (LPBYTE)&(fj_RomTable[0])) / sizeof(ROMENTRY);
					q.pBuffer     = fj_ftp_lRamBuffer;
					q.lBufferSize = fj_ftp_lCurrentLength;
					q.bFreeBuffer = FALSE;
					q.bFreeLock   = TRUE;
					q.pCallBack   = fj_ftp_ROM_callback;
					tx_queue_send( &(fj_CVT.pFJglobal->qROM), &q, TX_WAIT_FOREVER );

					fj_ftp_lCurrentLength = 0;
					fj_ftp_bFileGood = FALSE;
					iRet = 0;
				}
			}
			else
			{
				// not edit
			}
		}
		fj_ftp_pRomEntry = NULL;
	}

	return( iRet );
*/
}
/* 
 * fj_ftp_retr
 *
 * This routine is for client to get the ID of the ftp server.
 * The return of the 2 PRGRID?? strings is for the NETsilicon FTP program only.
 *
 * INPUTS
 *
 *    argp       Points to string containing file to be created at client.
 *    handle     Used to access FTP server client connection object.
 *
 */
int fj_ftp_retr(char *argp, unsigned long handle)
{
	printf ("%s(%d): fj_ftp_retr\n", __FILE__, __LINE__); return -1;
	
/*
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	LPFJMEMSTOR     pfms;
	_FJ_SOCKET_MESSAGE msgIn;
	_FJ_SOCKET_MESSAGE msgOut;
	BOOL   bEdit;
	LPSTR  pStr;
	STRING sName[FJFONT_NAME_SIZE*2];
	STRING str[FJSYS_LARGEST_USER_NAME+88];
	LONG   lDataLength;
	int    i, j, k, l;
	int    ret;

	if ( TRUE == pfph->bInfoROM )
	{
		sprintf(str,"FTP RETR %d %s \n",(int)handle,argp);
		fj_writeInfo( str );
	}

	fj_ftp_set_socket( handle );
	fj_ftp_find_level( handle );
	// if not priviledged to save/restore, do nothing
	if ( fj_asockets[fj_ftp_lSocketIndex].lSecLevel >= FJSYS_PRIORITY_RESTORE )
	{
		bEdit = FALSE;
		bEdit = fj_PrintHeadIsEdit( pfph, fj_asockets[fj_ftp_lSocketIndex].lIPaddr );
		if ( TRUE == bEdit ) pfms = pfph->pMemStorEdit;
		else                 pfms = pfph->pMemStor;

		ret = -1;
		i = strlen( argp );
		if ( i <= FJSYS_LARGEST_USER_NAME )
		{
			for ( j = 0; j <= i; j++ ) str[j] = toupper( *(argp + j) );
			j = i - 4;

			for ( i = 0; i < (sizeof(fj_ftp_dirs)/sizeof(FTPDIR)); i++ )
			{
				if ( 0 == strcmp(fj_ftp_dirs[i].pAll, argp) )
				{
					if ( DIR_ROOT == i )
					{
						i = 1;
						j = sizeof(fj_ftp_dirs)/sizeof(FTPDIR) - 1;
					}
					else
					{
						j = i;
					}
					for ( k = i; k <=j; k++ )
					{
						msgIn.Header.Cmd = fj_ftp_dirs[k].lCmdGetAll;
						msgIn.Header.BatchID = 1234;
						msgIn.Header.Length = 0;
						fj_socketCmd( pfph, (LPVOID)fj_ftp_lSocketIndex, fj_asockets[fj_ftp_lSocketIndex].lIPaddr, &msgIn, &msgOut, fj_socketSendBuffer );
						if ( DIR_MSG == k )
						{
							for ( l = 1; l < (sizeof(fj_ftp_dirs2)/sizeof(FTPDIR)); l++ )
							{
								msgIn.Header.Cmd = fj_ftp_dirs2[l].lCmdGetAll;
								msgIn.Header.BatchID = 1234;
								msgIn.Header.Length = 0;
								fj_socketCmd( pfph, (LPVOID)fj_ftp_lSocketIndex, fj_asockets[fj_ftp_lSocketIndex].lIPaddr, &msgIn, &msgOut, fj_socketSendBuffer );
							}
						}
					}
					ret = 0;
					break;
				}
				else if ( 0 == strcmp(fj_ftp_dirs[i].pExt, &(str[j])) )
				{
					argp[j] = 0;
					fj_processParameterStringOutput( sName, argp );
										// if it is a font or bmp,
					if ( (DIR_BMP == i) || (DIR_FONT == i) )
					{					// do not send command header
						pStr = (LPSTR)fj_MemStorFindElement( pfms, fj_ftp_dirs[i].pMemStorID, sName );
						if ( NULL != pStr )
						{
							memcpy( &lDataLength, (pStr-sizeof(LONG)), sizeof(LONG) );
										// memstor size includes the LONG size
							lDataLength -= sizeof(LONG);
										// if it is a bmp,
							if ( DIR_BMP == i )
							{
								j = 1 + strlen( pStr );
										// send only bmp and not our string
								lDataLength -= j;
								pStr += j;
							}
							// send element
							fj_ftp_send_buffer( fj_asockets[fj_ftp_lSocketIndex].iSocketRW, pStr, lDataLength );
							ret = 0;
						}
					}
					else
					{
						strcpy( (LPSTR)msgIn.Data, fj_ftp_dirs[i].pCmdKeyword );
						strcat( (LPSTR)msgIn.Data, sName );
						msgIn.Header.Cmd = fj_ftp_dirs[i].lCmdGetOne;
						msgIn.Header.BatchID = 1234;
						msgIn.Header.Length = strlen( (LPSTR)msgIn.Data );
						fj_socketCmd( pfph, (LPVOID)fj_ftp_lSocketIndex, fj_asockets[fj_ftp_lSocketIndex].lIPaddr, &msgIn, &msgOut, fj_socketSendBuffer );
						if ( DIR_MSG == i )
						{
							LPFJMESSAGE pfm = NULL;
							LPFJELEMENT pfe;
							LONG lType;
							pStr = (LPSTR)fj_MemStorFindElement( pfms, fj_ftp_dirs[i].pMemStorID, sName );
							if ( NULL != pStr )
							{
								pfm = fj_MessageBuildFromString( pfms, pStr );
								if ( NULL != pfm )
								{
									for ( k = 0; k < pfm->lCount; k++ )
									{
										pfe = pfm->papfe[k];
										lType = pfe->pActions->fj_DescGetType();
										strcpy( (LPSTR)msgIn.Data, fj_ftp_dirs2[lType].pCmdKeyword );
										fj_processParameterStringOutput( (LPSTR)msgIn.Data+strlen((LPSTR)msgIn.Data), pfe->strName );
										msgIn.Header.Cmd = fj_ftp_dirs2[lType].lCmdGetOne;
										msgIn.Header.Length = strlen( (LPSTR)msgIn.Data );
										fj_socketCmd( pfph, (LPVOID)fj_ftp_lSocketIndex, fj_asockets[fj_ftp_lSocketIndex].lIPaddr, &msgIn, &msgOut, fj_socketSendBuffer );
									}
									fj_MessageDestroy( pfm );
								}
							}
						}
						ret = 0;
						break;
					}
				}
			}
		}
		if ( 0 != ret )
		{
			if ( NULL != fj_ftp_pRETRError ) strcpy( fj_ftp_pRETRError, "Cannot find the file %s." );
		}
	}
	else
	{
		if ( NULL != fj_ftp_pRETRError ) strcpy( fj_ftp_pRETRError, "Not authorized." );
	}
	fj_ftp_reset_socket();
	return( ret );
*/
}
/* 
 * fj_ftp_control_close
 *
 * This routine is used run the rom image after a successful download
 *
 * INPUTS
 *
 *    argp       Points to string containing file to be created at client.
 *    handle     Used to access FTP server client connection object.
 *
 */
int fj_ftp_control_close(char *argp, unsigned long handle)
{
	printf ("%s(%d): fj_ftp_control_close\n", __FILE__, __LINE__); return -1;
/*
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	int  iRet;
	BOOL b = TRUE;
	char str[FJSYS_LARGEST_USER_NAME+88];
	int  i;

	if ( TRUE == pfph->bInfoROM )
	{
		sprintf(str,"FTP CCLO %d %s \n",(int)handle,argp);
		fj_writeInfo( str );
	}

	fj_ftp_pRomEntry = NULL;
	fj_ftp_bFileGood = FALSE;
	lDir = DIR_ROOT;
	fj_asockets[fj_ftp_lSocketIndex].iSocketRW = -1;
	fj_asockets[fj_ftp_lSocketIndex].lIPaddr = 0;
	fj_asockets[fj_ftp_lSocketIndex].lSecLevel = 0;
	fj_asockets[fj_ftp_lSocketIndex].lSeconds = fj_CVT.pFJglobal->lSeconds;

	return 0;
*/
}
//
// Delete
//
int fj_ftp_DELE(char *argp, unsigned long handle)
{
	printf ("%s(%d): fj_ftp_DELE\n", __FILE__, __LINE__); return -1;
/*	
	LPFJPRINTHEAD pfph = fj_CVT.pfph;
	LPSTR pStr;
	BOOL  bEdit;
	int   i, j, k;
	int   iRet;
	BOOL  bRet;
	char  str[FJSYS_LARGEST_USER_NAME+32];

	if ( TRUE == pfph->bInfoROM )
	{
		sprintf(str,"FTP DELE %d argp=%s \n",(int)handle,argp);
		fj_writeInfo( str );
	}

	iRet = -1;
	fj_ftp_set_socket( handle );

	fj_ftp_find_level( handle );
	// if not priviledged to save/restore, set bEdit FALSE
	if ( fj_asockets[fj_ftp_lSocketIndex].lSecLevel >= FJSYS_PRIORITY_RESTORE )
	{
		bEdit = FALSE;
		bEdit = fj_PrintHeadIsEdit( pfph, fj_asockets[fj_ftp_lSocketIndex].lIPaddr );
		if ( TRUE == bEdit )
		{
			i = strlen(argp);
			if ( i <= FJSYS_LARGEST_USER_NAME )
			{
				for ( j = 0; j <= i; j++ ) str[j] = toupper( *(argp + j) );

				for ( k = 0; k < (sizeof(fj_ftp_dirs)/sizeof(FTPDIR)); k++ )
				{
					if ( 0 == strcmp(fj_ftp_dirs[k].pExt,&(str[i-4])) )
					{
						argp[i-4] = 0;
						fj_processParameterStringOutput( str, argp );
						if ( DIR_MSG != k )
						{
							bRet = fj_MemStorDeleteElement( pfph->pMemStorEdit, fj_ftp_dirs[k].pMemStorID, str );
							if ( TRUE == bRet ) iRet = 0;
						}
						else
						{
							LPFJMESSAGE pfm = NULL;
							LPFJELEMENT pfe;
							LONG lType;
							pStr = (LPSTR)fj_MemStorFindElement( pfph->pMemStorEdit, fj_ftp_dirs[k].pMemStorID, str );
							if ( NULL != pStr )
							{
								pfm = fj_MessageBuildFromString( pfph->pMemStorEdit, pStr );
								if ( NULL != pfm )
								{
									bRet = fj_MessageDeleteFromMemstor( pfph->pMemStorEdit, pfm );
									if ( TRUE == bRet ) iRet = 0;
									fj_MessageDestroy( pfm );
								}
							}
						}
						break;
					}
				}
			}
			if ( 0 != iRet )
			{
				pStr = "Can not find file.";
				if ( NULL != fj_ftp_pDELEError1 ) strcpy( fj_ftp_pDELEError1, pStr );
				if ( NULL != fj_ftp_pDELEError2 ) strcpy( fj_ftp_pDELEError2, pStr );
				if ( NULL != fj_ftp_pDELEError3 ) strcpy( fj_ftp_pDELEError3, pStr );
				if ( NULL != fj_ftp_pDELEError4 ) strcpy( fj_ftp_pDELEError4, pStr );
			}
		}
		else
		{
			pStr = "Not in edit mode.";
			if ( NULL != fj_ftp_pDELEError1 ) strcpy( fj_ftp_pDELEError1, pStr );
			if ( NULL != fj_ftp_pDELEError2 ) strcpy( fj_ftp_pDELEError2, pStr );
			if ( NULL != fj_ftp_pDELEError3 ) strcpy( fj_ftp_pDELEError3, pStr );
			if ( NULL != fj_ftp_pDELEError4 ) strcpy( fj_ftp_pDELEError4, pStr );
		}
	}
	else
	{
		pStr = "Not authorized.";
		if ( NULL != fj_ftp_pDELEError1 ) strcpy( fj_ftp_pDELEError1, pStr );
		if ( NULL != fj_ftp_pDELEError2 ) strcpy( fj_ftp_pDELEError2, pStr );
		if ( NULL != fj_ftp_pDELEError3 ) strcpy( fj_ftp_pDELEError3, pStr );
		if ( NULL != fj_ftp_pDELEError4 ) strcpy( fj_ftp_pDELEError4, pStr );
	}

	fj_ftp_reset_socket();
	return( iRet );
*/
}
//
// just to see if this ever gets called
//
int fj_ftp_SYST(char *argp, char*infop, unsigned long handle)
{
	printf ("%s(%d): fj_ftp_SYST\n", __FILE__, __LINE__); return -1;
/*
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	char str[FJSYS_LARGEST_USER_NAME+32];
	if ( TRUE == pfph->bInfoROM )
	{
		sprintf(str,"FTP SYST %d %s \n",(int)handle,argp);
		fj_writeInfo( str );
	}
	memset(infop,0,256);
	sprintf(infop,"UNKNOWN FOXJET %s", fj_getVersionNumber() );
	return 0;
*/
}
//
// Present Working Directory
//
int fj_ftp_PWD(char *argp, unsigned long handle, char *buffer)
{
	printf ("%s(%d): fj_ftp_PWD\n", __FILE__, __LINE__); return -1;
/*
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	int  rc;
	char str[FJSYS_LARGEST_USER_NAME+256];

	*buffer = ' ';
	strcpy( buffer+1, fj_ftp_dirs[lDir].pName );

	if ( TRUE == pfph->bInfoROM )
	{
		sprintf(str,"FTP PWD  %d return=%s  argp=%s \n",(int)handle, buffer, argp);
		fj_writeInfo( str );
	}
	return 0;
*/
}
//
// just to see if this ever gets called
//
int fj_ftp_CDUP(char *argp, unsigned long handle)
{
	printf ("%s(%d): fj_ftp_CDUP\n", __FILE__, __LINE__); return -1;
/*
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	char str[FJSYS_LARGEST_USER_NAME+32];

	lDir = fj_ftp_dirs[lDir].lParent;

	if ( TRUE == pfph->bInfoROM )
	{
		sprintf(str,"FTP CDUP %d %s \n",(int)handle, argp);
		fj_writeInfo( str );
	}
	return 0;
*/
}
//
// Change Working Directory
//
int fj_ftp_CWD(char *argp, unsigned long handle)
{
	printf ("%s(%d): fj_ftp_CWD\n", __FILE__, __LINE__); return -1;
/*
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	int  i;
	int  ret;
	char *arg;
	char *argSlash;
	char str[FJSYS_LARGEST_USER_NAME+32];

	ret = -1;

	if ( TRUE == pfph->bInfoROM )
	{
		sprintf(str,"FTP CWD  %d %s \n",(int)handle,argp);
		fj_writeInfo( str );
	}

	fj_ftp_set_socket( handle );
	fj_ftp_find_level( handle );
	fj_ftp_reset_socket();

	if ( NULL != argp )
	{
		argSlash = NULL;
		arg = argp;
		while ( 0 != *arg )
		{
			if ( '/' == *arg ) *arg = '\\';
			if ( ('\\' == *arg) && (NULL == argSlash) ) argSlash = arg;
			arg++;
		}

		if ( NULL != argSlash )
		{
			argSlash++;
			for ( i = 0; i < (sizeof(fj_ftp_dirs)/sizeof(FTPDIR)); i++ )
			{
				if ( 0 == strcmp(argSlash,fj_ftp_dirs[i].pName) )
				{
					lDir = i;
					ret = 0;
					break;
				}
			}
		}
		else if ( lDir == DIR_ROOT )
		{
			for ( i = 0; i < (sizeof(fj_ftp_dirs)/sizeof(FTPDIR)); i++ )
			{
				if ( 0 == strcmp(argp,fj_ftp_dirs[i].pName) )
				{
					lDir = i;
					ret = 0;
					break;
				}
			}
		}
	}
	if ( -1 == ret )
	{
		if ( NULL != fj_ftp_pCWDError ) strcpy( fj_ftp_pCWDError, "Cannot find the %s directory." );
	}
	else
	{
		// if not priviledged to see all, set directory to root
		if ( fj_asockets[fj_ftp_lSocketIndex].lSecLevel < FJSYS_PRIORITY_VIEW_ALL )
		{
			if ( DIR_ROOT != lDir )
			{
				lDir = DIR_ROOT;
				if ( NULL != fj_ftp_pCWDError ) strcpy( fj_ftp_pCWDError, "Not authorized." );
				ret = -1;
			}
		}
	}

	return( ret );
*/
}
//
// just to see if this ever gets called
//
int fj_ftp_MKD(char *argp, unsigned long handle)
{
	printf ("%s(%d): fj_ftpMKD\n", __FILE__, __LINE__); return -1;
/*
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	char str[FJSYS_LARGEST_USER_NAME+32];
	if ( TRUE == pfph->bInfoROM )
	{
		sprintf(str,"FTP MKD  %d %s \n",(int)handle,argp);
		fj_writeInfo( str );
	}
	return -1;
*/
}
//
// just to see if this ever gets called
//
int fj_ftp_RMD(char *argp, unsigned long handle)
{
	printf ("%s(%d): fj_ftp_RMD\n", __FILE__, __LINE__); return -1;
/*
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	char str[FJSYS_LARGEST_USER_NAME+32];
	if ( TRUE == pfph->bInfoROM )
	{
		sprintf(str,"FTP RMD  %d %s \n",(int)handle,argp);
		fj_writeInfo( str );
	}
	return -1;
*/
}
//
// just to see if this ever gets called
//
int fj_ftp_RNFR(char *argp, unsigned long handle)
{
	printf ("%s(%d): fj_ftp_RNFR\n", __FILE__, __LINE__); return -1;
/*
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	int  ret;
	char str[FJSYS_LARGEST_USER_NAME+88];

	ret = -1;
	ret = 0;
	if ( TRUE == pfph->bInfoROM )
	{
		sprintf(str,"FTP RNFR %d %s \n",(int)handle,argp);
		fj_writeInfo( str );
	}
	return( ret );
*/
}
//
// just to see if this ever gets called
//
int fj_ftp_RNTO(char *argp, unsigned long handle)
{
	printf ("%s(%d): fj_ftp_RNTO\n", __FILE__, __LINE__); return -1;
/*
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	int  ret;
	char str[FJSYS_LARGEST_USER_NAME+88];

	ret = -1;
	ret = 0;
	if ( TRUE == pfph->bInfoROM )
	{
		sprintf(str,"FTP RNTO %d %s \n",(int)handle,argp);
		fj_writeInfo( str );
	}
	return( ret );
*/
}
//
// give back a directory list of memstor elements
//
// adjust size of individual bmp and fonts to reflect different read/write algorithms.
//
/*
int fj_ftp_LIST_memstor( unsigned long handle, LPSTR idstr, LPFJMEMSTOR pfms, int iFTPindex )
{
	printf ("%s(%d): fj_ftp_LIST_memstor\n", __FILE__, __LINE__); return -1;
	LPSTR *papElements;
	LPSTR pParams[3];
	LONG  lDataLength;
	int   i, j, k, l, m;
	CHAR  buffer[FJSYS_LARGEST_USER_NAME+88];
	CHAR  str[88];

	i = 0;
	if ( DIR_ROOT == iFTPindex )
	{
		k = 1;
		l = sizeof(fj_ftp_dirs)/sizeof(FTPDIR) - 1;
	}
	else
	{
		k = iFTPindex;
		l = iFTPindex;
	}
	for ( m = k; m <= l; m++ )
	{
		papElements = fj_MemStorFindAllElements( pfms, fj_ftp_dirs[m].pMemStorID );
										// a group will have a null command header at the end
		i += sizeof(_FJ_SOCKET_MESSAGE_HEADER);
		if ( DIR_MSG == m )
		{
			// null terminating entry for all element types
			i += ((sizeof(fj_ftp_dirs2)/sizeof(FTPDIR))-1) * sizeof(_FJ_SOCKET_MESSAGE_HEADER);
		}

		while ( NULL != *papElements )
		{
										// copy part of string
			strncpy( str, *papElements, sizeof(str) );
			str[sizeof(str)-2] = '}';	// guarantee end for partial scan
			str[sizeof(str)-1] = 0;
			fj_ParseKeywordsFromStr( str, ',', pParams, 3 );
			fj_processParameterStringInput( pParams[1] );
			memcpy( &lDataLength, (((LPBYTE)*papElements)-sizeof(LONG)), sizeof(LONG) );
			lDataLength -= sizeof(LONG);// memstor size includes the LONG size
			if ( DIR_BMP == m )
			{
				j = 1 + strlen( *papElements );
				lDataLength -= j;		// send only bmp and not our string
				i += j + sizeof(_FJ_SOCKET_MESSAGE_HEADER);
			}
			else if ( DIR_FONT == m )
			{
										// font has no command header
				i += sizeof(_FJ_SOCKET_MESSAGE_HEADER);
			}
			else if ( DIR_MSG == m )
			{
				lDataLength--;			// memstor size includes zero at end of string - socket command does not
				// include command header size
				lDataLength += sizeof(_FJ_SOCKET_MESSAGE_HEADER);
				{
					LPFJMESSAGE pfm = NULL;
					LPFJELEMENT pfe;
					LPSTR pStr;
					LONG  lType;
					{
						pfm = fj_MessageBuildFromString( pfms, *papElements );
						if ( NULL != pfm )
						{
							for ( k = 0; k < pfm->lCount; k++ )
							{
								pfe = pfm->papfe[k];
								lType = pfe->pActions->fj_DescGetType();
								fj_processParameterStringOutput( buffer, pfe->strName );
								pStr = (LPSTR)fj_MemStorFindElement( pfms, fj_ftp_dirs2[lType].pMemStorID, buffer );
								if ( NULL != pStr )
								{
									lDataLength += strlen( pStr ) + sizeof(_FJ_SOCKET_MESSAGE_HEADER);
								}
							}
							fj_MessageDestroy( pfm );
						}
					}
				}
			}
			else
			{
				lDataLength--;			// memstor size includes zero at end of string - socket command does not
				// include command header size
				lDataLength += sizeof(_FJ_SOCKET_MESSAGE_HEADER);
			}
			if ( DIR_ROOT != iFTPindex )
			{
				sprintf( buffer, "-rw-rw-r--   1 %s %6d %s %s%s\r\n", idstr, lDataLength, fj_ftp_date_time_str, pParams[1], fj_ftp_dirs[m].pExt);
				fj_ftp_send_buffer( handle, buffer, strlen(buffer) );
			}
			i += lDataLength;
			papElements++;
		}
	}
	if ( 0 != i )
	{
		sprintf( buffer, "-r--r--r--   1 %s %6d %s %s%s\r\n", idstr, i, fj_ftp_date_time_str, fj_ftp_dirs[iFTPindex].pAll, "");
		fj_ftp_send_buffer( handle, buffer, strlen(buffer) );
	}
	return( i );
}
*/
//
// give back a directory list
//
int fj_ftp_LIST(char *argp, unsigned long handle)
{
	printf ("%s(%d): fj_ftp_LIST\n", __FILE__, __LINE__); return -1;
/*
	CLPCFJ_PARAM  pFJparam = fj_CVT.pFJparam;
	LPFJPRINTHEAD pfph = fj_CVT.pfph;
	LPFJMEMSTOR   pfms;
	int   ret;
	int   socket;
	int   i;
	char *arg;
	char *argb;
	char *argSlash;
	BOOL  bEdit;
	LONG  lDirList;
	CHAR  buffer[111*4];
	CHAR  idstr[44];
	ULONG rc;
	CHAR  str[88];
	CHAR  strSave[64];

	if ( TRUE == pfph->bInfoROM )
	{
		sprintf(buffer,"FTP LIST %d argp=%s \n",(int)handle,argp);
		fj_writeInfo( buffer );
	}

	fj_ftp_set_socket( handle );
	fj_ftp_find_level( handle );

	bEdit = FALSE;
	bEdit = fj_PrintHeadIsEdit( pfph, fj_asockets[fj_ftp_lSocketIndex].lIPaddr );
	if ( TRUE == bEdit ) pfms = pfph->pMemStorEdit;
	else                 pfms = pfph->pMemStor;

	fj_ftp_find_level( handle );

	lDirList = -1;
	buffer[0] = 0;
	ret = -1;

	strcpy( idstr, fj_CVT.pfph->strID );
	strcat( idstr, " " );
	strcat( idstr, fj_CVT.pfph->strID );

	if ( NULL != argp )
	{
		if ( 0xe5 == *argp ) *argp = 0;	// temp NetSilicon bug
		argSlash = NULL;
		argb = buffer;					// convert to UPPERCASE in argb/buffer
		arg = argp;
		while ( 0 != *arg )
		{
			*argb = toupper(*arg);
			if ( '/' == *arg ) *arg = '\\';
			if ( ('\\' == *arg) && (NULL == argSlash) ) argSlash = arg;
			argb++;
			arg++;
		}
		if      ( 0 == strcmp(buffer, "LIST ") ) argp += 5;
		else if ( 0 == strcmp(buffer, "LIST" ) ) argp += 4;
		else if ( 0 == strcmp(buffer, "DIR " ) ) argp += 4;
		else if ( 0 == strcmp(buffer, "DIR"  ) ) argp += 3;

		if ( 0 == strlen(argp) )
		{
			lDirList = lDir;
		}
		else
		{
			if ( NULL != argSlash )
			{
				argSlash++;
				for ( i = 0; i < (sizeof(fj_ftp_dirs)/sizeof(FTPDIR)); i++ )
				{
					if ( 0 == strcmp(argSlash,fj_ftp_dirs[i].pName) )
					{
						lDirList = i;
						break;
					}
				}
			}
			else if ( DIR_ROOT == lDir )
			{
				for ( i = 0; i < (sizeof(fj_ftp_dirs)/sizeof(FTPDIR)); i++ )
				{
					if ( 0 == strcmp(argp,fj_ftp_dirs[i].pName) )
					{
						lDirList = i;
						break;
					}
				}
			}
		}
		// if not priviledged to see all, set directory to root
		if ( fj_asockets[fj_ftp_lSocketIndex].lSecLevel < FJSYS_PRIORITY_VIEW_ALL )
		{
			if ( NULL != fj_ftp_pLISTError ) strcpy( fj_ftp_pLISTError, "Not authorized." );
		}
		else
		{
			fj_ftp_get_date_time();
			if ( DIR_ROOT == lDirList )
			{
				strcpy( buffer, "drwxrwxr-x  3 " );
				strcat( buffer, idstr );
				strcat( buffer, " 1024 " );
				strcat( buffer, fj_ftp_date_time_str );
				strcat( buffer, " Fonts\r\n" );
				strcat( buffer, "drwxrwxr-x  3 " );
				strcat( buffer, idstr );
				strcat( buffer, " 1024 " );
				strcat( buffer, fj_ftp_date_time_str );
				strcat( buffer, " Labels\r\n" );
				strcat( buffer, "drwxrwxr-x  3 " );
				strcat( buffer, idstr );
				strcat( buffer, " 1024 " );
				strcat( buffer, fj_ftp_date_time_str );
				strcat( buffer, " Messages\r\n" );
				strcat( buffer, "drwxrwxr-x  3 " );
				strcat( buffer, idstr );
				strcat( buffer, " 1024 " );
				strcat( buffer, fj_ftp_date_time_str );
				strcat( buffer, " Bitmaps\r\n" );
				fj_ftp_send_buffer( handle, buffer, strlen(buffer) );
				fj_ftp_LIST_memstor( handle, idstr, pfms, lDirList );
				ret = 0;
			}
			else if ( lDirList > 0 )
			{
				fj_ftp_LIST_memstor( handle, idstr, pfms, lDirList );
				ret = 0;
			}
			else
			{
				if ( NULL != fj_ftp_pLISTError ) strcpy( fj_ftp_pLISTError, "Could not find the file(s) %s." );
			}
		}
	}
	fj_ftp_reset_socket();
	return( ret );
*/
}
//
// just to see if this ever gets called
//
int fj_ftp_NLST(char *argp, unsigned long handle)
{
	printf ("%s(%d): fj_ftp_NLST\n", __FILE__, __LINE__); return -1;
/*
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	char str[FJSYS_LARGEST_USER_NAME+32];
	if ( TRUE == pfph->bInfoROM )
	{
		sprintf(str,"FTP NLST %d %s \n",(int)handle,argp);
		fj_writeInfo( str );
	}
	return 0;
*/
}
extern char __ghsbegin_rodata, __ghsend_rodata;
//
// find string inside NetSilicon code
//
char * fj_ftp_find_string( char * pFind )
{
	printf ("%s(%d): fj_ftp_find_string\n", __FILE__, __LINE__); return NULL;
/*
	char *pFound = NULL;
	char *pLook;
	char *pLookEnd;
	char  c;
	int   i;

	i = strlen( pFind );
	c = *pFind;
	pLook    = &__ghsbegin_rodata;
	pLookEnd = &__ghsend_rodata;
	if ( NULL != fj_ftp_pConnect )
	{
		pLook = fj_ftp_pConnect;
		pLookEnd = pLook + 2400;
	}
	for ( ; pLook <= pLookEnd; pLook++ )
	{
		if ( c == *pLook )
		{
			if ( (pLook != pFind) && (0 == strncmp( pLook, pFind, i )) )
			{
				pFound = pLook;
				break;
			}
		}
	}

	return( pFound );
*/
}
/*
 * fj_ftp_init
 *
 * This routine initializes the FTP server and starts it so that a flash
 * download can occur.
 *
 */

void fj_ftp_init(void)
{
	printf ("%s(%d): fj_ftp_init\n", __FILE__, __LINE__); return;
/*
	CHAR cInternetControl = fj_CVT.pfsys->cInternetControl;
	int   i, j;
	STR   str[48];
	LPSTR pStr;
	LPFJ_GLOBAL pGlobal = fj_CVT.pFJglobal;

	if ( (FJSYS_INTERNET_TCPIP != cInternetControl) && (FJSYS_INTERNET_SLAVE != cInternetControl) )
	{
		for ( j = 0; j < fj_CVT.pFJglobal->lSocketCount; j++ )
		{
			if ( FALSE == fj_asockets[j].bInUse )
			{
				fj_asockets[j].bInUse = TRUE;
				fj_asockets[j].bFTP   = TRUE;
				fj_asockets[j].bIOBox = FALSE;
				fj_asockets[j].bBrowserApplet = FALSE;
										// security level for this socket
				fj_asockets[j].lSecLevel    = 0;
										// time when entry as last active. 0 = never. zeroed after some interval.
				fj_asockets[j].lSeconds     = 0;
				fj_asockets[j].iSocketRW    = -1;
										// disable our time out
				fj_asockets[j].lAckTime     = 0;
				fj_asockets[j].lBatchIDSend = 0;
				fj_asockets[j].lBatchIDRecv = 0;
				fj_asockets[j].lEchoBatchID = 0;
				fj_ftp_lSocketIndex = j;
				break;
			}
		}

		// get memeory for the largest possible RAM buffer that we will need
		j = 0;
		for ( i = 0; i < fj_CVT.lFJRomTableCount; i++ )
		{
			if ( j < fj_RomTable[i].lMaxLength )
			{
				j = fj_RomTable[i].lMaxLength;
			}
		}

		pGlobal->fj_ftp_lRamBuffer = fj_memory_getOnce( j );
		if ( NULL != pGlobal->fj_ftp_lRamBuffer) pGlobal->fj_ftp_lRamBufferSize = j;
		else pGlobal->fj_ftp_lRamBufferSize = 0;

		// Initialize the FTP server internal data structures. The following
		// allocates control data structures for one concurrent user.
		FSInitialize (1);
		// increase stack size. we have 2 large buffers.
		FSProperties ( "FSV", FS_NOT_DEFINED, 4096+2*sizeof(_FJ_SOCKET_MESSAGE), FS_NOT_DEFINED, FS_NOT_DEFINED, FS_NOT_DEFINED );
		// set up our callback routines
		FSRegisterSYST (fj_ftp_SYST);
		FSRegisterPWD  (fj_ftp_PWD);
		FSRegisterCDUP (fj_ftp_CDUP);
		FSRegisterCWD  (fj_ftp_CWD);
		FSRegisterDELE (fj_ftp_DELE);
		FSRegisterSTOR (fj_ftp_stor);
		FSRegisterRETR (fj_ftp_retr);
		//		FSRegisterMKD  (fj_ftp_MKD);
		//		FSRegisterRMD  (fj_ftp_RMD);
		//		FSRegisterRNFR (fj_ftp_RNFR);
		//		FSRegisterRNTO (fj_ftp_RNTO);
		FSRegisterLIST (fj_ftp_LIST);
		FSRegisterNLST (fj_ftp_NLST);
		FSRegisterDataClose    (fj_ftp_data_close);
		FSRegisterControlClose (fj_ftp_control_close);

		// The NetSilicon FTP server requires a userid and password.
		// I have found no way around it.
		// Add two common FTP login names with no password.
		NAsetSysAccess( NASYSACC_ADD, "(none)",    "", NASYSACC_LEVEL_RW, NULL );
		NAsetSysAccess( NASYSACC_ADD, "anonymous", "", NASYSACC_LEVEL_RW, NULL );
		NAsetSysAccess( NASYSACC_ADD, "password", "password", NASYSACC_LEVEL_RW, NULL );

		// Actually start the FTP server.
		FSStartServer ();

		fj_ftp_pConnect = fj_ftp_find_string( "NET+ARM FTP Server" );
		if ( NULL != fj_ftp_pConnect )
		{
			j = strlen( fj_ftp_pConnect );
			strcpy( str, fj_CVT.pfph->strNameLong );
			strcat( str, " FTP " );
			strcat( str, fj_getVersionNumber() );
			strcat( str, " ready." );
			str[j] = 0;
			strcpy( fj_ftp_pConnect, str );
		}
		fj_ftp_pCWDError  = fj_ftp_find_string( "Cannot find the %s directory." );
		fj_ftp_pRETRError = fj_ftp_find_string( "Cannot find the file %s." );
		fj_ftp_pSTORError = fj_ftp_find_string( "Connection closed; transfer aborted." );
		fj_ftp_pLISTError = fj_ftp_find_string( "Could not find the file(s) %s." );
		pStr = "No such file or directory.";
		fj_ftp_pDELEError1 = fj_ftp_find_string( pStr );
		if ( NULL != fj_ftp_pDELEError1  ) *fj_ftp_pDELEError1 = 0;
		fj_ftp_pDELEError2 = fj_ftp_find_string( pStr );
		if ( NULL != fj_ftp_pDELEError2  ) *fj_ftp_pDELEError2 = 0;
		fj_ftp_pDELEError3 = fj_ftp_find_string( pStr );
		if ( NULL != fj_ftp_pDELEError3  ) *fj_ftp_pDELEError3 = 0;
		fj_ftp_pDELEError4 = fj_ftp_find_string( pStr );
		if ( NULL != fj_ftp_pDELEError1  ) *fj_ftp_pDELEError1 = *pStr;
		if ( NULL != fj_ftp_pDELEError2  ) *fj_ftp_pDELEError2 = *pStr;
		if ( NULL != fj_ftp_pDELEError3  ) *fj_ftp_pDELEError3 = *pStr;
	}

	return;
*/
}
