#include "math.h"
#include "fj_netsilicon.h"
#include <narmled.h>
#include <na_isr.h>
//#include <narmsrln.h>					// for NETOS ROM parameters
#include "fj.h"
//#include "fj_memstorage.h"
#include "fj_font.h"
#include "fj_image.h"
#include "fj_element.h"
#include "fj_message.h"
#include "fj_counter.h"
#include "fj_printhead.h"
#include "fj_label.h"
#include "fj_system.h"
#include "mphc_defines.h"
#include "mphc.h"
#include "fj_scratchpad.h"

#ifndef PI
	#define PI			3.1415926535897932384626433832795
#endif

#define DEG2RAD( n )	n * ((PI) / 180.00)			// degrees to radians
#define RAD2DEG( n )	((n * 180) / (PI))			// radian to degrees

extern BOOL gbTraceWrite;
extern BOOL gbTraceRead;
extern UCHAR gnErrDrv;
extern BOOL gbFPGA;
extern BOOL gbIdledOnError;

extern struct tm fj_tmStandby;

void ToRegisters (tagSetupRegs * tConfig, tagFireVibPulse * tPulseRegs);

FJDLLExport int fj_GetPhcChannels ()
{
	int nHeadType = fj_GetMphcType (fj_CVT.pfph->lFJPhOpIndex);

	switch (nHeadType) {
	case GRAPHICS_768_256:
	case GRAPHICS_384_128:
	case GRAPHICS_768_256_L310:
	case GRAPHICS_768_256_0:
	case GRAPHICS_768_256_L330:
		return 256;
	}
	
	return 32;
}

FJDLLExport int fj_GetMphcType (int nNetHead)
{
	switch (nNetHead) {
		case 0: return UJ2_352_32;	// UJ II 352/32
		case 1: return UJ2_192_32;	// UJ II 192/32
		case 2: return UJ2_96_32;	// UJ II 96/32
		case 3: return GRAPHICS_768_256;	// Graphics 768
		case 4: return GRAPHICS_384_128;
		case 5: return UJ2_192_32NP;
		case 6: return ALPHA_CODER;	// AlphaCoder
		case 7: return GRAPHICS_768_256_L330;
		case 8: return GRAPHICS_768_256_L310;
		case 9: return GRAPHICS_768_256_0;
	}
	
	return GRAPHICS_768_256;
}

FJDLLExport double fj_GetPrintRes ()
{
	double dEncoder = 150.0;

	if (fj_CVT.pfsys->lEncoderDivisor && fj_CVT.pfsys->fEncoderWheel) {
		switch (fj_CVT.pfsys->lEncoderDivisor) {
		case 1:	dEncoder = fj_CVT.pfsys->fEncoderWheel;		break; // 300
		default:
		case 2:	dEncoder = fj_CVT.pfsys->fEncoderWheel / 2.0;	break; // 150
		case 3:	dEncoder = fj_CVT.pfsys->fEncoderWheel / 1.5;	break; // 200
		}
	}
	
	return floor (dEncoder);
}

FJDLLExport int fj_CalculateNFactor (double angle, double res)
{
	double NFactor;
	int nHeadType = fj_GetMphcType (fj_CVT.pfph->lFJPhOpIndex);
	int nHorzRes = (int)fj_GetPrintRes ();

	if (angle >= 90.0) {
		double dDiv;
		
		switch (fj_CVT.pfsys->lEncoderDivisor) {
		case 1:	dDiv = 2.0;			break; // 300
		default:
		case 2:	dDiv = 1.0;			break; // 150
		case 3:	dDiv = 4.0 / 3.0;	break; // 200
		}
		
		switch (nHeadType) {
		case GRAPHICS_768_256: 
		case GRAPHICS_384_128:
		case UJ2_352_32:
		{
			if (nHorzRes == 426)
				return (int)(8.0 * dDiv);//return (int)(8.0 / dDiv);

			return (int)(6.0 * dDiv);//return (int)(6.0 / dDiv);
		}
		case GRAPHICS_768_256_L330:
		case GRAPHICS_768_256_L310:
			return (int)ceil (46.0 * dDiv);//return (int)(46.0 / dDiv);
		case GRAPHICS_768_256_0:
			return 0;
			break;
		default:
			return 0;
		}
	}

	NFactor = 0.05848 * cos ( DEG2RAD(angle) );
//	NFactor *= res / 2;
	NFactor *= res;
	NFactor += 0.5;

	int t = ( int ) floor(NFactor);

	{ char sz [128]; sprintf (sz, "fj_CalculateNFactor (angle: %f, res: %f): %d", angle, res, t); TRACEF (sz); }
	
	return t;
}

FJDLLExport UINT fj_CalcFlushBuffer () 
{
	UINT nFlush 		= 0;
	int N_Factor 		= fj_CalculateNFactor (fj_CVT.pfph->fSlantAngle, fj_GetPrintRes ());
	INT nWordsPerCol 	= fj_GetPhcChannels () / 16;
	int nHeadType 		= fj_GetMphcType (fj_CVT.pfph->lFJPhOpIndex);
	
	UCHAR state = (fj_CVT.pfph->lDirection == FJPH_DIRECTION_LEFT_TO_RIGHT) ? 1 : 0;
	state |= fj_CVT.pfph->pfop->bHeadInvert ? 0x02 : 0x00;

	switch ( state ) 
	{
		case 0:	// R->L Normal
		case 3:	// L->R Inverted
			switch (nHeadType)
			{
				case ALPHA_CODER:
				case UJ2_192_32:
				case UJ2_192_32NP:
				case UJ2_96_32:
				case UJI_96_32:
				case UJI_192_32:
				case UJI_256_32:
					//nFlush = (31 * N_Factor * nWordsPerCol); 
					nFlush = (31 * N_Factor * nWordsPerCol) / 2; //m_Config.m_HeadSettings.m_nEncoderDivisor; 
					break;
				case UJ2_352_32:
					nFlush = N_Factor * nWordsPerCol; 
					break;
				case GRAPHICS_384_128:
				case GRAPHICS_768_256:
				case GRAPHICS_768_256_L310:
				case GRAPHICS_768_256_L330:
				case GRAPHICS_768_256_0:
					nFlush = N_Factor * nWordsPerCol; 
					break;
			}
			break;
		case 1:	// L->R Normal
		case 2:	// R->L Inverted
			switch (nHeadType)
			{
				case ALPHA_CODER:
				case UJ2_192_32:
				case UJ2_192_32NP:
				case UJ2_96_32:
				case UJI_96_32:
				case UJI_192_32:
				case UJI_256_32:
					//nFlush = (31 * N_Factor * nWordsPerCol); 
					nFlush = (31 * N_Factor * nWordsPerCol) / 2; //m_Config.m_HeadSettings.m_nEncoderDivisor; 
					break;
				case UJ2_352_32:	
					nFlush = N_Factor * nWordsPerCol; 
					break;
				case GRAPHICS_384_128:
				case GRAPHICS_768_256:	
				case GRAPHICS_768_256_L310:
				case GRAPHICS_768_256_L330:
				case GRAPHICS_768_256_0:
					nFlush = N_Factor * nWordsPerCol; 
					break;
			}
			break;
	}
	
	//{ char sz [128]; sprintf (sz, "flush: %d (state: 0x%02X) (n-factor: %d, angle: %f, res: %f)", nFlush, state, N_Factor, fj_CVT.pfph->fSlantAngle, fj_GetPrintRes ()); TRACEF (sz); }
	
	return nFlush;
}

FJDLLExport void fj_ProgramHeadConfig (WORD wImageCols)
{
	tagSlantRegs SlantRegs;
	const int flush 		= fj_CalcFlushBuffer ();
	const int N_Factor 		= fj_CalculateNFactor (fj_CVT.pfph->fSlantAngle, fj_GetPrintRes ());
	const int ibpc 			= fj_GetPhcChannels () / 8;
	const INT nWordsPerCol	= ibpc / 2;
	const int nHeadType = fj_GetMphcType (fj_CVT.pfph->lFJPhOpIndex);

	//{ char sz [128]; sprintf (sz, "fj_CalculateNFactor (angle: %f, res: %f): %d", pfphy->fSlantAngle, fj_GetPrintRes (), N_Factor); TRACEF (sz); }

	UCHAR state = (fj_CVT.pfph->lDirection == FJPH_DIRECTION_LEFT_TO_RIGHT) ? 1 : 0;
	state |= fj_CVT.pfph->pfop->bHeadInvert ? 0x02 : 0x00;

	switch ( state ) 
	{
		case 0:	// R->L Normal
		case 3:	// L->R Inverted
			switch (nHeadType )
			{
				case ALPHA_CODER:
				case UJ2_192_32:
				case UJ2_192_32NP:
				case UJ2_96_32:
				case UJI_96_32:
				case UJI_192_32:
				case UJI_256_32:
					//flush = (31 * N_Factor * nWordsPerCol); 
					SlantRegs._ImageStartAddr._Addr1	= (WORD) 0x0000;
					SlantRegs._ImageStartAddr._Addr2	= (WORD) ( -nWordsPerCol * N_Factor );
					SlantRegs._AddrIncFactor._Incr		= (WORD) ( -2 * nWordsPerCol * N_Factor );
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) ( 32 * nWordsPerCol * N_Factor );
					break;
				case UJ2_352_32:
					//flush = N_Factor * nWordsPerCol; 
					SlantRegs._ImageStartAddr._Addr1	= (WORD) 0x0000;
					SlantRegs._ImageStartAddr._Addr2	= (WORD) (-nWordsPerCol * N_Factor);
					SlantRegs._AddrIncFactor._Incr		= (WORD) 0x0000;
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) 0x0000;
					break;
				case GRAPHICS_384_128:
				case GRAPHICS_768_256:
				case GRAPHICS_768_256_L310:
				case GRAPHICS_768_256_L330:
				case GRAPHICS_768_256_0:
					//flush = N_Factor * nWordsPerCol; 
					SlantRegs._ImageStartAddr._Addr2	= (WORD) 0x0000;
					SlantRegs._ImageStartAddr._Addr1	= (WORD) (-nWordsPerCol * N_Factor);
					SlantRegs._AddrIncFactor._Incr		= (WORD) 0x0000;
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) 0x0000;
					break;
			}
			break;
		case 1:	// L->R Normal
		case 2:	// R->L Inverted
			switch (nHeadType)
			{
				case ALPHA_CODER:
				case UJ2_192_32:
				case UJ2_192_32NP:
				case UJ2_96_32:
				case UJI_96_32:
				case UJI_192_32:
				case UJI_256_32:
					//flush = (31 * N_Factor * nWordsPerCol); 
					SlantRegs._ImageStartAddr._Addr1	= (WORD) ( nWordsPerCol * ( wImageCols + flush - 1) );
					SlantRegs._ImageStartAddr._Addr2	= (WORD) ( SlantRegs._ImageStartAddr._Addr1 - ( N_Factor * nWordsPerCol) );
					SlantRegs._AddrIncFactor._Incr		= (WORD) ( -2 * nWordsPerCol * N_Factor );
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) (((32 * nWordsPerCol * N_Factor ) - 2 * nWordsPerCol));
					break;
				case UJ2_352_32:	
					// wImageCols: 		14400 
					//flush = N_Factor * nWordsPerCol; 
					SlantRegs._ImageStartAddr._Addr1	= (WORD) (  nWordsPerCol * (wImageCols + N_Factor - 1) );					// 28810
					SlantRegs._ImageStartAddr._Addr2	= (WORD) ( SlantRegs._ImageStartAddr._Addr1 - ( N_Factor * nWordsPerCol) );	// 28798
					SlantRegs._AddrIncFactor._Incr		= (WORD) 0x0000;
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) -2 * nWordsPerCol;
					break;
				case GRAPHICS_384_128:
				case GRAPHICS_768_256:	
				case GRAPHICS_768_256_L310:
				case GRAPHICS_768_256_0:
				case GRAPHICS_768_256_L330:
					//flush = N_Factor * nWordsPerCol; 
					SlantRegs._ImageStartAddr._Addr2	= (WORD) (  nWordsPerCol * (wImageCols + N_Factor - 1) );
					SlantRegs._ImageStartAddr._Addr1	= (WORD) ( SlantRegs._ImageStartAddr._Addr2 - ( N_Factor * nWordsPerCol) );
					SlantRegs._AddrIncFactor._Incr		= (WORD) 0x0000;
					SlantRegs._ColAdjustFactor._Adjust	= (WORD) -2 * nWordsPerCol;
					break;
			}
			break;
	}
	
	BYTESWAP (SlantRegs._ImageStartAddr._Addr2);
	BYTESWAP (SlantRegs._ImageStartAddr._Addr1);
	BYTESWAP (SlantRegs._AddrIncFactor._Incr);
	BYTESWAP (SlantRegs._ColAdjustFactor._Adjust);
	
	ProgramSlantRegs (SlantRegs);
	
	WORD wCols = (WORD)(fj_CVT.pfph->fDistancePC * fj_CVT.pfsys->fPrintResolution);
	WORD wProdLen = wCols + 1;

	/*
	{
		char sz [512];
		sprintf (sz, "SetProductLen: %d, SetImageLen: %d, flush: %d, N_Factor: %d, PrintRes: %f", 
			wProdLen, wImageCols + flush, flush, N_Factor, fj_GetPrintRes ());	
		TRACEF (sz);
	}
	*/
	
	SetProductLen (wProdLen);
	SetImageLen (wImageCols + flush);
}
 
FJDLLExport VOID fj_ProgramSetupRegs ()
{
//	int nEncoderDivisor = fj_CVT.pfsys->lEncoderDivisor;
	int nHorzRes = fj_GetPrintRes ();//(int)((double)fj_CVT.pfsys->fEncoderWheel / (double)nEncoderDivisor);
	ULONG lPhotocellDelay = fj_CVT.pfph->fDistancePC * 1000.0;
	tagRegisters tCurrent;
	tagSetupRegs tSetup;
	tagFireVibPulse tPulseRegs;
	
	if (/* nEncoderDivisor == 0 || */ nHorzRes == 0)
		return;
	
	//TRACEF ("fj_ProgramSetupRegs");
	
	memset (&tCurrent, 0, sizeof (tCurrent));
	memset (&tSetup, 0, sizeof (tSetup));
	memset (&tPulseRegs, 0, sizeof (tPulseRegs));
	
	ReadConfiguration (&tCurrent);

	ToRegisters (&tSetup, &tPulseRegs);
	
	tSetup._CtrlReg._PrintEnable =           tCurrent._CtrlReg._PrintEnable;
	
	tSetup._Interrupt_Reg._Channel =         2;//tCurrent._Interrupt_Reg._Channel; //12;
	tSetup._Interrupt_Reg._Mode =            1;
	tSetup._Interrupt_Reg._ErrDrv =          gnErrDrv;
	
	//gbTraceWrite = TRUE; printf ("%s(%d): write setup\n", __FILE__, __LINE__);
	WriteRegister (0x00, &tSetup, sizeof (tSetup));
	//printf ("\n");	gbTraceWrite = FALSE; tx_thread_sleep (1);

	WriteRegister (0x10, &tPulseRegs, sizeof (tPulseRegs));

	if (nHorzRes /* && nEncoderDivisor */) {
		int nSpeedGateWidth = nHorzRes;// * nEncoderDivisor;
		
		//gbTraceWrite = TRUE; printf ("%s(%d): SetSpeedGateWidth\n", __FILE__, __LINE__);		
		SetSpeedGateWidth (nSpeedGateWidth);
	
		//printf ("\n%s(%d): SetPrintDelay\n", __FILE__, __LINE__);		
		WORD wPrintDly = (WORD)(((double)lPhotocellDelay / (double)1000.0) * (double)nHorzRes);
		SetPrintDelay (wPrintDly);
		//printf ("\n"); gbTraceWrite = FALSE; tx_thread_sleep (1);
		
		/*
		{
			char sz [512];
			sprintf (sz, "SpeedGateWidth: %d, SetPrintDelay: %d, lPhotocellDelay: %d, fEncoderWheel: %f, lEncoderDivisor: %d, fSpeed: %f [%f fpm]", 
				nSpeedGateWidth, wPrintDly, lPhotocellDelay, fj_CVT.pfsys->fEncoderWheel, fj_CVT.pfsys->lEncoderDivisor,
				fj_CVT.pfsys->fSpeed, ReadLineSpeed ());
			TRACEF (sz);
		}
		*/
	}
}

double fj_PrintHeadGetInkLowVolume (CLPCFJPRINTHEAD pfph)
{
    if (0 == strcmp(((*(pCVT->paFJPhOp))[pfph->lFJPhOpIndex].strName),"ProSeries NP192"))
	return FJSYS_MAX_INKLOW_UJ2_192_32NP_ML;

    return FJSYS_MAX_INKLOW_ML;
}

//
// set the print head status for a heater
//
BOOL fj_print_head_SetStatusHeater( LPHEATER pHeater )
{
	BOOL     bStatusReady;

	bStatusReady = TRUE;
	return( bStatusReady );
}
//
// set the print head status
//
extern volatile tagPHC_Flags gPHCFlags;
extern UCHAR gnStatusReg;
VOID fj_print_head_SetStatus(VOID)
{
	CLPFJ_GLOBAL     pGlobal = fj_CVT.pFJglobal;
	CLPCFJPRINTHEAD  pfph    = fj_CVT.pfph;
	LPFJPRINTHEAD_OP pfop    = fj_CVT.pfph->pfop;
/*
	LPFJ_BRAM        pBRAM   = fj_CVT.pBRAM;
	GA_READ_STATUS wStatus = fj_CVT.pGA->rw.read.status;
*/
	BOOL     bStatusReady = TRUE;
	tagIndicators status;
	
	strcpy( pGlobal->strHeadStatus, "Ready;");

	if ( TRUE == pGlobal->bReset )
	{
		bStatusReady = FALSE;
		strcpy (pGlobal->strHeadStatus, "Please reboot printer now!");
	}
	else if ( TRUE == pGlobal->bGAUpgrade )
	{
		strcat(pGlobal->strHeadStatus, ". GA has been updated.");
	}
	/*
	else if ( FALSE == pfph->bHeadSelected )
	{
		bStatusReady = FALSE;
		pStatus = "Head type not selected";
	}
	else if ( FALSE == pGlobal->bADReady )
	{
		bStatusReady = FALSE;
		pStatus = "A/D values not read";
	}
	else if ( FALSE == pGlobal->bPrintHeadPresent )
	{
		bStatusReady = FALSE;
		pStatus = "Print Head not present";
	}
	else if ( FALSE == wStatus.bits.AtTemp )
	{
		bStatusReady = FALSE;
		pStatus = "Print Head not at operating temperature";
	}
	else if (fj_PrintHeadGetInkLowVolume (pfph) <= pBRAM->dInkLowML )
	{
		bStatusReady = FALSE;
		pStatus = "Printing halted due to Out of Ink condition. Replace Inkbottle.";
	}
	// Bypass for old board shoud be here!!!!!!!!!
	else if ( FALSE == pGlobal->bVoltageReady )
	{
		bStatusReady = FALSE;
		pStatus = "High Voltage not present";
		fj_print_head_SendVoltageInfo();
	}
	else if ( TRUE == pGlobal->bStandByOn )
	{
		pStatus = "StandBy";
	}
	else if ( TRUE == pBRAM->bPrintIdle )
	{
		pStatus = "Idle";
	}
	else if ( TRUE == pBRAM->bInkLow )
	{
		pStatus = "Low Ink";
	}
	*/

	memset (&status, 0, sizeof (status));
	DecodeIndicators (&status, gnStatusReg);	

	{
		char sz [128]; 
		char strInk [12]; 
		double dSpeed = ReadLineSpeed ();
		double dInk = fj_scratchpadReadInkUsage ();	
		
		if (status.InkLow) 
			strcpy (strInk, dInk <= 0.0 ? "EMPTY" : "LOW");
		else 
			strcpy (strInk, "OK");

		sprintf (sz, "InkUsage=%f;CONVEYOR_SPEED=%f;InkLevel=%s;VoltReady=%s;Heater1Ready=%s;Heater1On=%s",
			dInk,
			dSpeed,
			strInk, 							// InkLevel
			status.HVOK 	? "T" : "F", 		// VoltReady
			status.AtTemp	? "T" : "F",		// Heater1Ready
		 	status.HeaterOn	? "T" : "F");		// Heater1On
		 	
		 if (!gbFPGA)
			strcat (sz, ";FPGA=Missing");
			
		/*
		printf ("%s(%d): %s  gPHCFlags [%d %d %d %d %d] [%d %d %d %d]\n", __FILE__, __LINE__, sz,
			gPHCFlags.flagISR, gPHCFlags.flagPhotoInt, gPHCFlags.flagEOPInt, gPHCFlags.flagSerial, gPHCFlags.flagReserved, 
			gPHCFlags.AmsActive, gPHCFlags.PhotocellActivated, gPHCFlags.StartOfPrint, gPHCFlags.EndOfPrint);
		*/
		
		strcat (pGlobal->strHeadStatus, sz);
	}
	 	
/*
	{
		tagInterfaceStatusReg s;

		memset (&s, 0, sizeof (s));
		ReadRegister (0x04, &s, sizeof (s));
		///*
		printf ("%s(%d): tagInterfaceStatusReg: %d%d%d%d %d%d%d%d %d\n", 
			__FILE__, __LINE__,
			s._HeaterOn,
			s._AtTemp,
			s._InkLow,
			s._HVOK,
			s._HeadError,
			s._Photocell,
			s._TachIntr,
			s._OTBIP,
			s._Reserved);
		//* /
		
		if (s._OTBIP) {
			if (fj_CVT.pfsys->bPrintDriver) {
				fj_EndPrintDriverJob ();
				strcat (pGlobal->strHeadStatus, ";CancelPrintJob=1");
				TRACEF ("PrintDriver: CANCEL");
			}
		}
	}
*/
}
//
// start an edit session
//
BOOL fj_print_head_EditStart( ULONG ipAdd )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	LPFJPRINTHEAD pfph    = fj_CVT.pfph;
	BOOL   bRet;

	bRet = FALSE;
	if ( TX_SUCCESS == tx_semaphore_get( &(pGlobal->semaEditLock), TX_NO_WAIT ) )
	{
										// set up MemStor for editing
		bRet = fj_PrintHeadStartEdit( pfph, ipAdd );
		if ( FALSE == bRet )
		{
			tx_semaphore_put( &(pGlobal->semaEditLock) );
		}
		fj_print_head_SendEditStatus();
	}
	else
		printf ("%s(%d): tx_semaphore_get failed\n", __FILE__, __LINE__);
	
	//printf ("%s(%d): fj_print_head_EditStart: %d\n", __FILE__, __LINE__, bRet);
	
	return ( bRet );
}
//
// cancel an edit session
//
VOID fj_print_head_EditCancel( ULONG ipAdd )
{
/*
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	LPFJPRINTHEAD pfph    = fj_CVT.pfph;

	if ( 0 == pGlobal->semaEditLock.tx_semaphore_count )
	{
		if ( TRUE == fj_PrintHeadCancelEdit( pfph, ipAdd ) )
		{
			tx_semaphore_put( &(pGlobal->semaEditLock) );
			fj_print_head_SendEditStatus();
		}
	}
*/	
}

//
// save an edit session
//
VOID fj_print_head_EditSave( ULONG ipAdd )
{
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	LPFJPRINTHEAD pfph    = fj_CVT.pfph;
	LPFJ_PARAM    pParam  = fj_CVT.pFJparam;
	LPFJPRINTHEAD pfphP   = &(pParam->FJPh);
	int    retrom;
	struct tm tm;
	BOOL bRet = FALSE;

	if ( 0 == pGlobal->semaEditLock.tx_semaphore_count )
	{
		tx_semaphore_get( &(pGlobal->semaROMLock), TX_WAIT_FOREVER );
		fj_SystemGetTime(&tm);
		
		if (fj_PrintHeadSaveEdit (pfph, ipAdd, &tm)) {
			// TODO //fj_SystemBuildSelectedLabel( fj_CVT.pfsys, fj_CVT.pBRAM->strLabelName );
			// TODO //pCVT->fj_RestoreBRAMCounters( pfph->pfmSelected );
			fj_print_head_SendEditStatus();

			retrom = fj_ReadFJRomParams( pParam );

			memcpy( &(pfphP->tmEditVersion), &tm, sizeof(struct tm) );
			/*
			printf ("%s(%d): tmEditVersion: %02d:%02d:%02d %d/%d/%d\n", 	
				__FILE__, __LINE__,
				pfphP->tmEditVersion.tm_hour,
				pfphP->tmEditVersion.tm_min,
				pfphP->tmEditVersion.tm_sec,
				pfphP->tmEditVersion.tm_mday,
				pfphP->tmEditVersion.tm_mon,
				pfphP->tmEditVersion.tm_year);
			*/
			//fj_WriteFJRomParams( pParam, FALSE, NULL, 0 );
			bRet = TRUE;
			tx_semaphore_put (&(pGlobal->semaEditLock));
		}
		else 
			printf ("%s(%d): fj_PrintHeadSaveEdit failed", __FILE__, __LINE__);

		tx_semaphore_put (&(pGlobal->semaROMLock));
	}
	else
		printf ("%s(%d): fj_print_head_EditSave failed: semaEditLock: %d\n", __FILE__, __LINE__, pGlobal->semaEditLock.tx_semaphore_count);

	//printf ("%s(%d): fj_print_head_EditSave: %d\n", __FILE__, __LINE__, bRet);
}

//
// schedule AMS
//
VOID fj_print_head_ScheduleAMS( VOID )
{
	// seconds - time for next AMS to be scheduled
	//fj_CVT.pBRAM->lAMS_SST = fj_CVT.pFJglobal->lSeconds + (LONG)(fj_CVT.pfph->fAMSInterval*60.*60.);
	return;
}

//
// check if time for AMS cycle
//
//static BOOL bSetAMS_PC_Dis = FALSE;
BOOL gbApsNow = FALSE;
extern struct tm fj_tmAPS;
extern BOOL gbPrintCycle;
static ULONG glTimeout = 3000;
VOID fj_print_head_AMSCheck( VOID )
{
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	static BOOL bEndAps = FALSE;
	static struct tm tmApsStarted;
    struct tm tmNow;
	time_t now;
	time_t last;
	double dDiff;
	
	if (bEndAps) {
	    fj_SystemGetTime (&tmNow);
	    now = mktime (&tmNow);
	    last = mktime (&fj_tmAPS);
	    dDiff = difftime (now, last);
			    
		if (dDiff > glTimeout || !gPHCFlags.AmsActive) {
			tagSetupRegs tSetup;
			tagFireVibPulse tPulseRegs;
			
			memset (&tSetup, 0, sizeof (tSetup));
			memset (&tPulseRegs, 0, sizeof (tPulseRegs));
			ToRegisters (&tSetup, &tPulseRegs);
			
			tSetup._CtrlReg._PrintEnable	= 1;	
			tSetup._CtrlReg._FireAMS		= 0;
			WriteRegister (0x00, &tSetup._CtrlReg, sizeof (tSetup._CtrlReg));
			
		    fj_SystemGetTime (&fj_tmAPS);
		    gbApsNow = FALSE;
		    bEndAps = FALSE;
		}
	}
	else {
		if (!gbPrintCycle) {
			if (pfph->bAPSenable) {
			    fj_SystemGetTime (&tmNow);
			    now = mktime (&tmNow);
			    last = mktime (&fj_tmAPS);
			    dDiff = difftime (now, last);
			    
			    if ((dDiff > (pfph->fAMSInterval * 60.0 * 60.0)) || gbApsNow) {
					tagSetupRegs tSetup;
					tagFireVibPulse tPulseRegs;
					tagAMSRegs ams;
					
					glTimeout = 3000;
			    	glTimeout += pfph->pfop->lAMSA1;
			    	glTimeout += pfph->pfop->lAMSA2;
			    	glTimeout += pfph->pfop->lAMSA3;
			    	glTimeout += pfph->pfop->lAMSA4;
			    	glTimeout += pfph->pfop->lAMSA5;
			    	glTimeout += pfph->pfop->lAMSA6;
			    	glTimeout += pfph->pfop->lAMSA7;
			    	
					memset (&ams, 0, sizeof (ams));
					memset (&tSetup, 0, sizeof (tSetup));
					memset (&tPulseRegs, 0, sizeof (tPulseRegs));
					ToRegisters (&tSetup, &tPulseRegs);
	
			    	ams.A1 = pfph->pfop->lAMSA1;
			    	ams.A2 = pfph->pfop->lAMSA2;
			    	ams.A3 = pfph->pfop->lAMSA3;
			    	ams.A4 = pfph->pfop->lAMSA4;
			    	ams.A5 = pfph->pfop->lAMSA5;
			    	ams.A6 = pfph->pfop->lAMSA6;
			    	ams.A7 = pfph->pfop->lAMSA7;
			    	ProgramAmsRegs (ams);
		
					tSetup._CtrlReg._PrintEnable	= 0;	
					tSetup._CtrlReg._FireAMS		= 1;
					WriteRegister (0x00, &tSetup._CtrlReg, sizeof (tSetup._CtrlReg));

					{
						ULONG lTickStart = tx_time_get ();
	
						do {
							struct tm tmAPS;
							
							if (gPHCFlags.AmsActive)
								break;
	
						    fj_SystemGetTime (&tmAPS);
						    now = mktime (&tmAPS);
						    last = mktime (&tmNow);
						    dDiff = difftime (now, last);
					        tx_thread_sleep (1);
						}
						while ((tx_time_get () - lTickStart) < NS_MILLISECONDS_TO_TICKS (glTimeout));
					}
					
					fj_SystemGetTime (&tmApsStarted);
					bEndAps = TRUE;
			    }
			}
		}
	}
}
//
// Check the ink level
//
VOID fj_print_head_InkCheck(VOID)
{
/*
	CLPFJ_GLOBAL pGlobal   = fj_CVT.pFJglobal;
	CLPFJ_BRAM   pBRAM     = fj_CVT.pBRAM;
	CLPCFJ_PARAM pFJparam  = fj_CVT.pFJparam;
	GA_READ_STATUS wStatus = fj_CVT.pGA->rw.read.status;
	STR str[88];

	//	if ( 512 < pGlobal->adValues.wInkLevel )	// high number is HIGH
	if ( 0 != wStatus.bits.InkLevelLow )// high number is HIGH
	{
		if ( TRUE == pBRAM->bInkLow )	// was LOW,
		{
			pBRAM->bInkLow = FALSE;		// now HIGH
										// send Low Ink flag to GA
			//fj_print_head_SetGAWriteSwitches();

			if ( FJ_PC_MODE_ENABLED != pGlobal->lPhotocellMode )
			{
				pGlobal->lPhotocellMode = FJ_PC_MODE_ENABLED;
				pCVT->fj_print_head_SendPhotocellMode();
			}

										// ZAP countInkLow
			pBRAM->countInkLow.lMessages     = 0;
			pBRAM->countInkLow.lDotsHi       = 0;
			pBRAM->countInkLow.lDotsLo       = 0;
			pBRAM->dInkLowML = 0.0;
			pBRAM->bInkLowMax= FALSE;
			fj_print_head_SetStatus();
			fj_print_head_SendInfoAll();

			fj_SystemGetTime( &(pBRAM->tmInkFull) );
		}
	}
	else
	{
		if ( FALSE == pBRAM->bInkLow )	// was HIGH,
		{
			pBRAM->bInkLow = TRUE;		// now LOW
										// send Low Ink flag to GA
			//fj_print_head_SetGAWriteSwitches();
			fj_SystemGetTime( &(pBRAM->tmInkLow) );
			fj_print_head_IOBoxInfo();
			sprintf( str, "Printer %s in group %s at %u.%u.%u.%u is low on ink.", fj_CVT.pfph->strID, fj_CVT.pfsys->strID, PUSH_IPADDR(((ULONG)fj_CVT.pfph->lIPaddr)) );
			// TODO //fj_print_head_SendEmailInk( str, "Low Ink" );
			fj_print_head_SetStatus();
			fj_print_head_SendInfoAll();
		}
		else if ( (fj_PrintHeadGetInkLowVolume (fj_CVT.pfph) <= pBRAM->dInkLowML) && ( FALSE == pBRAM->bInkLowMax ))
		{
			pBRAM->bInkLowMax = TRUE;
			if ( FJ_PC_MODE_DISABLED != pGlobal->lPhotocellMode )
			{
				pGlobal->lPhotocellMode = FJ_PC_MODE_DISABLED;
				pCVT->fj_print_head_SendPhotocellMode();
				sprintf( str, "Printer %s in group %s at %u.%u.%u.%u is out of ink.", fj_CVT.pfph->strID, fj_CVT.pfsys->strID, PUSH_IPADDR(((ULONG)fj_CVT.pfph->lIPaddr)) );
				// TODO //fj_print_head_SendEmailInk( str, "Out of Ink" );
			}
			fj_print_head_SetStatus();
			fj_print_head_SendInfoAll();
		}
	}
	return;
*/
}
//
// Check the board temperature
//
VOID fj_print_head_BoardTempCheck(VOID)
{
/*
	CLPFJ_GLOBAL pGlobal  = fj_CVT.pFJglobal;
	CLPCFJ_PARAM pFJparam = fj_CVT.pFJparam;
	LPCTEMPINFO  *paTempInfo;
	LPCTEMPINFO  pTempInfo;
	WORD16 (*pawTempTable)[];			// pointer to array of A/D values
	WORD16  wFirstTemp;					// first temp in table
	WORD16  wLastTemp;					// last temp in table
	LONG    lTemp;						// current temp - degrees C
	int     i,l;

	paTempInfo = fj_CVT.paTempInfo;
	pTempInfo = paTempInfo[0];
										// board thermistor is same as 60 degree head
	pawTempTable = pTempInfo->pawTempTable;
	wFirstTemp = pTempInfo->wFirstTemp;
	wLastTemp  = pTempInfo->wLastTemp;
	l = wLastTemp - wFirstTemp;
	// don't use first and last entry in table.
	// the algorithms will look at the entries before and after the one found.
	for ( i = 1; i < l; i++ )
	{
		// temperature found
		if ( pGlobal->adValues.wTempBoard >= (*pawTempTable)[i] )
		{
										// get current time
			ULONG lNow = pGlobal->lSeconds;
			static LONG lTimePrev = 0;
			lTemp = wFirstTemp + i;
			if ( lTemp != pGlobal->lBoardTemp )
			{
				if ( lNow > (lTimePrev + 2) )
				{
					lTimePrev = lNow;
					pGlobal->lBoardTemp = lTemp;
					fj_print_head_SendBoardTempInfo();
				}
			}
			else
			{
				lTimePrev = lNow;
			}
			break;
		}
	}
	return;
*/
}

//
// Check the print head high voltage
//
VOID fj_print_head_VoltageCheck(VOID)
{
/*
	volatile LPGA    pga      = fj_CVT.pGA;
	CLPFJ_GLOBAL     pGlobal  = fj_CVT.pFJglobal;
	LONG lVoltage;
	BOOL bVoltageReady;
	ULONG lNow;
	int   i;
	static LONG lTimePrev = 0;
	static LONG lTimePrevAdj = 0;

	GA_READ_STATUS wStatus = fj_CVT.pGA->rw.read.status;

	// save
	pGlobal->gaRead.rw.read.status.all = pga->rw.read.status.all;

	if ( 0 == wStatus.bits.HVStatus )
	{
		bVoltageReady = FALSE;			// set not ready
	}
	else
	{
		bVoltageReady = TRUE;			// if reeady (within tolerance)
										// make it look right on to avoid reports +- one volt
		lVoltage = pGlobal->lHighVoltageWanted;
	}

	// set global status and report it, if READY condition has changed
	if ( bVoltageReady != pGlobal->bVoltageReady )
	{
		pGlobal->bVoltageReady = bVoltageReady;
		fj_print_head_SetStatus();
		fj_print_head_SendInfoAll();
	}
	//		}
	//	}
	return;
*/
}
//
// Call the proper routine depending on which channel was last read
//
VOID fj_print_head_ADHeaterCheck( LPHEATER pHeater )
{
/*
	volatile LPGA    pga      = fj_CVT.pGA;
	CLPFJ_GLOBAL     pGlobal  = fj_CVT.pFJglobal;
	CLPCFJ_PARAM     pFJparam = fj_CVT.pFJparam;
	CLPCFJPRINTHEAD  pfph     = fj_CVT.pfph;
	LPFJPRINTHEAD_OP pfop     = fj_CVT.pfph->pfop;
	BOOL  bHeaterOn;
	BOOL  bHeaterReady;
	ULONG lNow;
	int   i;
	GA_READ_STATUS wStatus = fj_CVT.pGA->rw.read.status;

	// save
	pGlobal->gaRead.rw.read.status.all = pga->rw.read.status.all;
	if ( 0 != wStatus.bits.HeaterOn )   bHeaterOn = TRUE;
	else                    bHeaterOn = FALSE;
	if ( 0 != wStatus.bits.AtTemp )     bHeaterReady = TRUE;
	else                    bHeaterReady = FALSE;

	pHeater->bHeaterPresent = TRUE;		// force AD check

	if ( (bHeaterOn != pHeater->bHeaterOn) || (bHeaterReady != pHeater->bReady))
	{
		pHeater->bHeaterOn = bHeaterOn;
		pHeater->bReady    = bHeaterReady;
		fj_print_head_SetStatus();
		fj_print_head_SendInfoAll();
	}
	return;
*/
}
//
// Call the proper routine depending on which channel was last read
//
VOID fj_print_head_ADCheck(VOID)
{
/*
	volatile LPGA    pga      = fj_CVT.pGA;
	CLPFJ_GLOBAL     pGlobal  = fj_CVT.pFJglobal;
	CLPCFJ_PARAM     pFJparam = fj_CVT.pFJparam;
	CLPCFJPRINTHEAD  pfph     = fj_CVT.pfph;
	LPFJPRINTHEAD_OP pfop     = fj_CVT.pfph->pfop;
	LPHEATER         pHeater;
	//BOOL    bSendStatus;
	BOOL    bPresent;
	LONG    lChannel;
	//GA_READ_STATUS wStatus = fj_CVT.pGA->rw.read.status;

	// save
	pGlobal->gaRead.rw.read.status.all = pga->rw.read.status.all;

	if ( (TRUE == pGlobal->bADReady) && (TRUE == pfph->bHeadSelected) )
	{
		bPresent = TRUE;
		if ( TRUE == pfop->bForceHeadPresent )
		{
			if ( FALSE == pGlobal->bGAAvail ) bPresent = FALSE;
		}
		else
		{
			//			if ( 0 == wStatus.bits.HeadPresent )
			//			{
			//				bPresent = FALSE;
			//			}
		}
		if ( FALSE == bPresent )
		{
			if ( TRUE == pGlobal->bPrintHeadPresent )
			{
				if ( TRUE == pfph->bInfoStatus )
				{
					fj_writeInfo( "Head not present\n" );
				}
				pGlobal->bPrintHeadPresent = FALSE;
				fj_print_head_SetStatus();
				fj_print_head_SendInfoAll();
			}
		}
		else
		{
			if ( FALSE == pGlobal->bPrintHeadPresent )
			{
				if ( TRUE == pfph->bInfoStatus )
				{
					fj_writeInfo( "Head present\n" );
				}
				pGlobal->bPrintHeadPresent = TRUE;
				fj_print_head_SetStatus();
				fj_print_head_SendInfoAll();
			}
		}

		// the data returned during the last write was for the previous channel
		lChannel = pGlobal->lChannel - 1;
		if ( lChannel < 0 ) lChannel = 7;

		if ( 0 == lChannel )			// board temperature
		{
			//fj_print_head_BoardTempCheck();
		}

		if ( TRUE == pGlobal->bPrintHeadPresent )
		{
			if ( 1 == lChannel )		// head voltage reading
			{

			}

			else if ( 2 == lChannel )	// head voltage setting
			{
				fj_print_head_VoltageCheck();
			}

			else if ( 3 == lChannel )	// heater 1 temperature
			{
				pHeater = &(pGlobal->heater1);
				pHeater->wADValue = pGlobal->adValues.wTemp1;
				fj_print_head_ADHeaterCheck( pHeater );
			}
			//else if ( 4 == lChannel )			// ink low
			//{
			fj_print_head_InkCheck();
			//}
		}
	}
	return;
*/
}

VOID fj_print_head_SetStandBy (bool bStandby)
{
	fj_CVT.pFJglobal->bStandByOn = bStandby;
	fj_ProgramSetupRegs ();
	return;
}
BOOL fj_print_head_GetStandBy ()
{
	return fj_CVT.pFJglobal->bStandByOn;
}
void fj_print_head_StandByCheck( VOID )
{
	LPFJ_BRAM     pBRAM   = fj_CVT.pBRAM;
	LPFJ_GLOBAL   pGlobal = fj_CVT.pFJglobal;
	LPCFJPRINTHEAD  pfph  = fj_CVT.pfph;

	if (pfph->bStandByEnable && !pGlobal->bStandByOn && !fj_print_head_GetStandBy ()) {
		if (0 == strcmp(((*(pCVT->paFJPhOp))[pfph->lFJPhOpIndex].strName),"ProSeries NP192")) {
		    struct tm tmNow;
			time_t now;
			time_t last;
			double dDiff;

		    fj_SystemGetTime (&tmNow);
		    now = mktime (&tmNow);
		    last = mktime (&fj_tmStandby);
		    dDiff = difftime (now, last);
		    
		    if (dDiff > (pfph->fStandByInactive * 60.0 * 60.0)) {
			    fj_SystemGetTime (&fj_tmStandby);
			    fj_print_head_SetStandBy (TRUE);
		    }
		}
	}
}
//
// check the print head
//
VOID fj_print_head_Check(VOID)
{
//	fj_print_head_PhotoCellCheck ();
/*
	fj_print_head_RasterCheck();
*/
	fj_print_head_AMSCheck();
	fj_print_head_StandByCheck();
/*	
	if ( TRUE != fj_noPrintDev )		// if true, do not use any FoxJet chips and/or devices
	{
		fj_print_head_ADRead();
		fj_print_head_ADCheck();
		fj_print_head_ShaftEncoderCheck();
	}
	if ( TRUE == fj_CVT.pFJglobal->bBuildFailure )
	{
		fj_CVT.pFJglobal->bBuildFailure = FALSE;
		fj_writeInfo( "Message not built in time. Not enough distance between PhotoCell and PrintHead.\n" );
	}
	if ( TRUE == fj_CVT.pFJglobal->bBufferFailure )
	{
		fj_CVT.pFJglobal->bBufferFailure = FALSE;
		fj_writeInfo( "FIFO buffer not filled in time. Message is too complex. Reduce DPI or FPM.\n" );
	}
*/
	return;
}

