#ifndef _WINDOWS // Burl 1.2009
	#include <appconf.h>					// for DEFINEd application options
	#include <netosIo.h>					// for OPEN options
	#include <serParam.h>					// for serial port options
	#include "fj.h"
	#include "bsp.h"
	#include "watchdog.h"
#else
	#include <windows.h> // Burl 1.2009
#endif

//#include "..\IP_BSP_v01\devicenetos_serl.h"		// for serial port options
#include "fj_printhead.h"
#include "fj_label.h"
#include "fj_element.h"
#include "fj_dyntext.h"
#include "fj_dynbarcode.h"
#include "fj_message.h"

static const STRING    fj_sVersionNumber[] = "3.5017";

const STRING * fj_getVersionNumber(VOID)
{
	return( fj_sVersionNumber );
}
const STRING * fj_getVersionDate(VOID)
{
	return NAGetAppTimeStamp ();
}

#ifndef _WINDOWS  // Burl 1.2009
// build NETBIOS name

VOID fj_setGroupID( VOID )

{
	// TODO //fj_groupNETBIOS();					// build netbios-format name
	return;
}
// id is set from short name + serial number
// build NETBIOS name
VOID fj_setID(VOID)
{
	fj_PrintHeadSetID( fj_CVT.pfph );
	// TODO //fj_nameNETBIOS();					// build netbios-format name
	return;
}

// serial number must be greater than FJ_SYS_SERIAL_MIN and less than FJ_SYS_SERIAL_MAX.
// or, for testing, serial can be less than FJ_SYS_SERIAL_TEST if previous serial is less than FJ_SYS_SERIAL_TEST.
BOOL fj_setSerial( LPSTR pInput )
{
//	LPFJ_NETOSPARAM pParamNET = fj_CVT.pNETOSparam;
	int  iLen;
	LONG lInput;
	LONG lSerial;
	BOOL bRet = FALSE;

	iLen = strlen( pInput );
	if ( (0 < iLen) && (iLen <= FJPH_SERIAL_SIZE ) )
	{
		//fj_FJtoNETSerial( pInput, pParamNET->serialNumber );
		fj_setID();
		bRet = TRUE;
	}
	return( bRet );
}

// this can only come from a password page.
// no need to check if previously set.
BOOL fj_setDateManu( struct tm *pTM )
{
	CLPFJ_GLOBAL  pGlobal = fj_CVT.pFJglobal;
	CLPFJ_BRAM    pBRAM   = fj_CVT.pBRAM;
	BOOL  bRet = FALSE;

	if ( pTM->tm_year > 100 )			// good date?  year has origin of 1900
	{
		memcpy( &(fj_CVT.pfph->tmDateManu),          pTM, sizeof(struct tm) );
		memcpy( &(fj_CVT.pFJparam->FJPh.tmDateManu), pTM, sizeof(struct tm) );
		bRet = TRUE;

		memcpy( &(pBRAM->tmLastPrint),     pTM, sizeof(struct tm) );
		memcpy( &(pBRAM->tmCustomerReset), pTM, sizeof(struct tm) );
		memcpy( &(pBRAM->tmInkFull),       pTM, sizeof(struct tm) );
	}
	return( bRet );
}
// this can only come from a password page.
// no need to check if previously set.
BOOL fj_setDateInservice( struct tm *pTM )
{
	CLPFJ_GLOBAL  pGlobal = fj_CVT.pFJglobal;
	CLPFJ_BRAM    pBRAM   = fj_CVT.pBRAM;
	BOOL  bRet = FALSE;

										// good date?  year has origin of 1900
	if ( (pTM->tm_year > 100) || (0 == pTM->tm_year) )
	{
		memcpy( &(fj_CVT.pfph->tmDateInservice),          pTM, sizeof(struct tm) );
		memcpy( &(fj_CVT.pFJparam->FJPh.tmDateInservice), pTM, sizeof(struct tm) );
		bRet = TRUE;

		memcpy( &(pBRAM->tmLastPrint),     pTM, sizeof(struct tm) );
		memcpy( &(pBRAM->tmCustomerReset), pTM, sizeof(struct tm) );
		memcpy( &(pBRAM->tmInkFull),       pTM, sizeof(struct tm) );
	}
	return( bRet );
}

// prototyped here to avoid errors on STRICT compile
//int open(const char *, int);
//int close(int);
//int write(int, const void *, unsigned int);
//int ioctl(int, unsigned long, unsigned long*);

#include "fj_serial.h"
#include "termios.h"

extern TX_THREAD thrSerial;

int fj_checkSerialPort( void )
{
	LPFJ_GLOBAL     pGlobal = fj_CVT.pFJglobal;
    CLPFJSYSTEM     pfsys = fj_CVT.pfsys;
    LPFJPRINTHEAD   pfph  = fj_CVT.pfph;
    LPFJLABEL       pfl;
    char c;
    const char cSOP = 17;
    int  i;
    int  iRet = 0;
	char sz [512];

	tx_thread_resume (&thrSerial);

	int nRead = ReadSerial (sz, ARRAYSIZE (sz));

    if ((nRead > 0) && (pfph->bSerialScannerLabel || pfph->bSerialVarData || pfph->bSerialDynMem) && (fj_CVT.pFJglobal->bInitComplete)) {
		if (pfph->bSerialScannerLabel) {
		    if (0 != strcmp(pCVT->pBRAM->strLabelName, sz))
				pCVT->fj_ResetBRAMCounters( pCVT->pBRAM );
	
		    pfl = fj_SystemBuildSelectedLabel( pfsys, sz );
	
		    if (pfl == NULL) {
				if (pfsys->pflScanner != NULL) 
				    pfl = fj_SystemCopyScannerDefaultLabelToSelectedLabel( pfsys );
		    }
		    else // save selected label name
				strcpy (fj_CVT.pBRAM->strLabelName, pfl->strName);
		}
		else if (TRUE == pfph->bSerialVarData) {
		    strncpy (pGlobal->DynamicSegments[pfph->lSerialVarDataID].strDynData, sz, FJTEXT_SIZE);
		    pGlobal->DynamicSegments[pfph->lSerialVarDataID].bChanged = TRUE;

			if (pfph->bSaveSerialData) {
				char szFile [FJSYS_MAX_FILENAME];
				int nLen = sizeof (DYNSEGMENT);
				
				sprintf (szFile, "%s/DynData%d.txt", FJ_DIR_PARAMS, pfph->lSerialVarDataID);

				if (!WriteFile (nLen, (char *)&fj_CVT.pFJglobal->DynamicSegments [pfph->lSerialVarDataID], szFile)) {
					char sz [FJSYS_MAX_FILENAME + 128];
					sprintf (sz, "Failed to write: %s", szFile);
					TRACEF (sz);
				}
			}
		}
		else if (TRUE == pfph->bSerialDynMem) {
		    LONG lIndex = 0;
		    CHAR szIndex [3] = { 0, 0, 0 };
	
		    szIndex [0] = sz [0];
		    szIndex [1] = sz [1];
		    sscanf (szIndex, "%d", &lIndex);
	
		    if (lIndex > 0)
				lIndex--;
	
		    if (strlen (sz) > 2) {
				strncpy (pGlobal->DynamicSegments[lIndex].strDynData, &sz [2], FJTEXT_SIZE);
				pGlobal->DynamicSegments[lIndex].bChanged = TRUE;
		    }
		    else {
				LPFJELEMENT pfe;
				LONG iSeg;
				LPFJMESSAGE pfm = pfph->pfmSelected;
		
				if (pfm) {
				    for (iSeg = 0; iSeg < pfm->lCount; iSeg++) {
						pfe = pfm->papfe[iSeg];
			
						if (NULL != pfe) {
						    switch (pfe->pActions->fj_DescGetType ()) {
							case FJ_TYPE_DYNTEXT:
							    {
									LPFJDYNTEXT pfc = (LPFJDYNTEXT)pfe->pDesc;
									pfc->lDynamicID = lIndex;
									fj_ElementPhotocellBuild (pfe);
							    }
							    break;
							case FJ_TYPE_DYNBARCODE:
							    {
									LPFJDYNBARCODE pfc = (LPFJDYNBARCODE)pfe->pDesc;
									pfc->lDynamicID = lIndex;
									fj_ElementPhotocellBuild (pfe);
							    }
							    break;
						    }
						}
					}

					fj_MessageResetInternal (pfm);
			    }
			}
		}
	}

    if (pfph->bSOP) {
		while (pfph->lSOP > 0) {
		    pfph->lSOP--;
		    WriteSerial (&cSOP, 1);
		}
    }

    return iRet;
}

int fj_onSOP (LPFJPRINTHEAD pfph)
{
	LONG lDirection = 0;

    if (pfph && pfph->bSOP)
		pfph->lSOP++;

	if (pfph && pfph->bBiDirectional) {
		lDirection = pfph->lDirection;

		if (lDirection == FJPH_DIRECTION_LEFT_TO_RIGHT)
			lDirection == FJPH_DIRECTION_RIGHT_TO_LEFT;
		else
			lDirection == FJPH_DIRECTION_LEFT_TO_RIGHT;

		if ((pfph->lBiDirectionalCount % 2) == 0) 
			pfph->fDistancePC = pfph->fDistancePC1;
		else
			pfph->fDistancePC = pfph->fDistancePC2;

		pfph->lBiDirectionalCount++;
	}
}

void fj_writeInfo( char *pIn )
{
	CLPCFJPRINTHEAD pfph = fj_CVT.pfph;
	int iRet;
	int ilen;

	{
		printf (pIn);
		
		if (strstr (pIn, "\n") == NULL)
			printf ("\n");		
	}
	
	if ( TRUE == pfph->bSerialInfo )
	{
		// write to serial port
		ilen = strlen(pIn);
		if ( '\n' == pIn[0] ) WriteSerial ("\r", 1);
		WriteSerial (pIn, ilen);
		if ( '\n' == pIn[ilen-1] ) WriteSerial ("\r", 1);

		// write to any debugger sockets
		fj_socketSendAllDebug( pIn );
	}
	
	return;
}

VOID fj_reboot ()
{
    char sz [128];

    fj_bKeepAlive = FALSE;
    fj_bStrokeWatchdog = FALSE;
    
    fj_writeInfo ("Rebooting...\r\n");
    TRACEF ("Rebooting...\r\n");
    tx_thread_sleep (5);
    
    customizeReset ();
}
#endif //_WINDOWS  // Burl 1.2009

