#ifndef FJ_MISC_H_
#define FJ_MISC_H_

#include <time.h>

#include "fj_base.h"
#include "fj_defines.h"

// Macro for passing IP addresses to printf octet at a time. usage: printf("IP is %u.%u.%u.%u\n", PUSH_IPADDR(ip));
#define PUSH_IPADDR(ip) (unsigned)(ip>>24),(unsigned)((ip>>16)&0xff),(unsigned)((ip>>8)&0xff),(unsigned)(ip&0xff)

struct fjtable
{
	LPSTR pStr;
	VOID (*pGet)( LPVOID pStruct, LPSTR pString );
	VOID (*pSet)( LPVOID pStruct, LPSTR pString );
};

#ifdef  __cplusplus						// Burl
extern "C"
{
#endif
	FJDLLExport time_t     fj_mktime( struct tm *pTM );
	FJDLLExport FLOAT      fj_FloatUnits( FLOAT fInput, LONG lUnits );
	FJDLLExport VOID       fj_DoubleToStr( LPSTR pStr, double dInput);
	FJDLLExport VOID       fj_LongToStr( LPSTR pStr, LONG lInput);
	FJDLLExport VOID       fj_BoolToStr( LPSTR pStr, BOOL bInput);
	FJDLLExport VOID       fj_IPToStr( ULONG lInput, LPSTR pStr );
	FJDLLExport ULONG      fj_StrToIP( LPSTR pStr );
	FJDLLExport LPSTR      fj_FindEndQuotedString( LPSTR pStr );
	FJDLLExport VOID       fj_processParameterStringInput( LPSTR pInput );
	FJDLLExport VOID       fj_processParameterStringOutput( LPSTR pOutput, LPCSTR pInput );
	FJDLLExport BOOL       fj_SetParameter( struct fjtable *pTable, LONG lTableFirst, LONG lTableLast, LPVOID pStruct, LPSTR pKey, LPSTR pValue );
	FJDLLExport VOID       fj_MakeParameterString( struct fjtable *pTable, LONG lTableFirst, LONG lTableLast, LPVOID pStruct, LPSTR pString );
	FJDLLExport BOOL       fj_SetParametersFromString( struct fjtable *pTable, LONG lTableFirst, LONG lTableLast, LPVOID pStruct, LPSTR pString );
	FJDLLExport LONG       fj_ParseKeywordsFromStr( LPSTR pStr, CHAR chDelim, LPSTR *pList, LONG lListSize );
#ifdef  __cplusplus
}
#endif

#endif /*FJ_MISC_H_*/
