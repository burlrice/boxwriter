#ifdef NET_OS
#include "fj.h"
#else
#include <windows.h>
#endif
#include "fj_mem.h"
#include "fj_defines.h"
#include "fj_image.h"

//
//  FUNCTION: fj_ImageNew()
//
//  PURPOSE:  Create a new FJIMAGE structure
//
FJDLLExport LPFJIMAGE fj_ImageNew( LONG lLength )
{
	LPFJIMAGE pfi;

	pfi = (LPFJIMAGE)fj_calloc( 1, sizeof (FJIMAGE) + lLength );

	if ( NULL != pfi )
	{
		pfi->lHeight       =  0;		// height of image in pixels  - depends on font
		pfi->lLength       =  lLength;	// number of bytes used in image buffer
		pfi->lTransforms   =  0;		// transforms used to change image
		pfi->lBits         =  0;		// number of 'on bits' (dots) in the image buffer
		pfi->lUseCounter   =  1;		// number of users
	        pfi->lShifted	   =  0;

		pfi = (LPFJIMAGE)(LONG)pfi;				// | 0x08000000;
	}
	return( pfi );
}
//
//  FUNCTION: fj_ImageDestroy()
//
//  PURPOSE:  Destroy a FJIMAGE structure
//
FJDLLExport VOID fj_ImageDestroy( LPFJIMAGE pfi )
{
	if ( 0 == pfi->lUseCounter )
	{
		pfi = (LPFJIMAGE)(LONG)pfi;				// & 0x00FFFFFF;
		fj_free( pfi );
	}
	else
	{
		pfi->lBits = 0;
										// divide by 0. cause a program interrupt.
		pfi->lUseCounter = 43 / pfi->lBits;
	}
	return;
}
//
//  FUNCTION: fj_ImageUse()
//
//  PURPOSE:  increment the use counter of a FJIMAGE structure
//
FJDLLExport VOID fj_ImageUse( LPFJIMAGE pfi )
{
	if ( NULL != pfi ) pfi->lUseCounter++;
	return;
}
//
//  FUNCTION: fj_ImageRelease()
//
//  PURPOSE:  decrement the use counter of a FJIMAGE structure.
//            if zero, destroy the FJIMAGE
//
FJDLLExport VOID fj_ImageRelease( LPFJIMAGE pfi )
{
	LONG lRet = 0;
	if ( NULL != pfi )
	{
		if ( 1 > pfi->lUseCounter )
		{
			pfi->lBits = 0;
										// divide by 0. cause a program interrupt.
			pfi->lUseCounter = 43 / pfi->lBits;
		}
		pfi->lUseCounter--;
		if ( 0 == pfi->lUseCounter ) fj_ImageDestroy( pfi );
	}
	else
	{
		lRet = 43;
	}
	return;
}
//
//  FUNCTION: fj_ImageReverse()
//
//  PURPOSE:  apply a reverse ( right-to-left) transform to a raster image
//
//

static LPFJIMAGE fj_ImageReverse( LPFJIMAGE pfiIn )
{
	LPFJIMAGE pfiOut;
	LPFJIMAGE pfiTest;
	register int   i,j;
	register LONG  lBytes;
	register LONG  lLengthm;
	register LONG  lLength;
	register LPBYTE pBufferIn;
	register LPBYTE pBufferOut;

	pfiOut = pfiIn;
	if ( NULL != pfiIn )
	{
		pfiTest = fj_ImageNew( pfiIn->lLength );
		if ( NULL != pfiTest )
		{
			pfiOut = pfiTest;
			pfiOut->lHeight     = pfiIn->lHeight;
			pfiOut->lTransforms = pfiIn->lTransforms;
			pfiOut->lBits       = pfiIn->lBits;

			lLength  = pfiIn->lLength;
			lBytes   = (pfiIn->lHeight + 7) / 8;
			lLengthm = lLength - lBytes;
			pBufferIn  = (LPBYTE)pfiIn  + sizeof(FJIMAGE);
			pBufferOut = (LPBYTE)pfiOut + sizeof(FJIMAGE);

			for ( i = 0; i < lLength; i += lBytes )
			{
				for ( j = 0; j < lBytes; j++ )
				{
					pBufferOut[lLengthm - i + j] = pBufferIn[ i + j ];
				}
			}
			fj_ImageRelease( pfiIn );
		}
	}
	return ( pfiOut );
}
//  table to reverse bits in a byte
BYTE  rbits[256] =
{
	0x00,0x80,0x40,0xC0,0x20,0xA0,0x60,0xE0,0x10,0x90,0x50,0xD0,0x30,0xB0,0x70,0xF0,
	0x08,0x88,0x48,0xC8,0x28,0xA8,0x68,0xE8,0x18,0x98,0x58,0xD8,0x38,0xB8,0x78,0xF8,
	0x04,0x84,0x44,0xC4,0x24,0xA4,0x64,0xE4,0x14,0x94,0x54,0xD4,0x34,0xB4,0x74,0xF4,
	0x0C,0x8C,0x4C,0xCC,0x2C,0xAC,0x6C,0xEC,0x1C,0x9C,0x5C,0xDC,0x3C,0xBC,0x7C,0xFC,
	0x02,0x82,0x42,0xC2,0x22,0xA2,0x62,0xE2,0x12,0x92,0x52,0xD2,0x32,0xB2,0x72,0xF2,
	0x0A,0x8A,0x4A,0xCA,0x2A,0xAA,0x6A,0xEA,0x1A,0x9A,0x5A,0xDA,0x3A,0xBA,0x7A,0xFA,
	0x06,0x86,0x46,0xC6,0x26,0xA6,0x66,0xE6,0x16,0x96,0x56,0xD6,0x36,0xB6,0x76,0xF6,
	0x0E,0x8E,0x4E,0xCE,0x2E,0xAE,0x6E,0xEE,0x1E,0x9E,0x5E,0xDE,0x3E,0xBE,0x7E,0xFE,
	0x01,0x81,0x41,0xC1,0x21,0xA1,0x61,0xE1,0x11,0x91,0x51,0xD1,0x31,0xB1,0x71,0xF1,
	0x09,0x89,0x49,0xC9,0x29,0xA9,0x69,0xE9,0x19,0x99,0x59,0xD9,0x39,0xB9,0x79,0xF9,
	0x05,0x85,0x45,0xC5,0x25,0xA5,0x65,0xE5,0x15,0x95,0x55,0xD5,0x35,0xB5,0x75,0xF5,
	0x0D,0x8D,0x4D,0xCD,0x2D,0xAD,0x6D,0xED,0x1D,0x9D,0x5D,0xDD,0x3D,0xBD,0x7D,0xFD,
	0x03,0x83,0x43,0xC3,0x23,0xA3,0x63,0xE3,0x13,0x93,0x53,0xD3,0x33,0xB3,0x73,0xF3,
	0x0B,0x8B,0x4B,0xCB,0x2B,0xAB,0x6B,0xEB,0x1B,0x9B,0x5B,0xDB,0x3B,0xBB,0x7B,0xFB,
	0x07,0x87,0x47,0xC7,0x27,0xA7,0x67,0xE7,0x17,0x97,0x57,0xD7,0x37,0xB7,0x77,0xF7,
	0x0F,0x8F,0x4F,0xCF,0x2F,0xAF,0x6F,0xEF,0x1F,0x9F,0x5F,0xDF,0x3F,0xBF,0x7F,0xFF
};

//
//  FUNCTION: fj_ImageInverse()
//
//  PURPOSE:  apply a inverse ( upside down ) transform to a raster image
//
//

static VOID fj_ImageInverse( LPFJIMAGE pfi )
{
	LPFJIMAGE pfiTmp;
	//register long   BitRT = 0;
	register int    i,j;
	register int    in_bytes;
	register int    in_length;
	register BYTE   c;
	register LPBYTE pmybuff;
	register LPBYTE pBuffer;
	UCHAR mybuff[FJSYS_NOZZLES/8];

	if ( NULL != pfi )
	{
		if  (pfi->lHeight & 0x7)		// if not 8***
		{
			pfiTmp = fj_ImageNew( pfi->lLength );
			memcpy(((LPBYTE)pfiTmp + sizeof(FJIMAGE)),((LPBYTE)pfi + sizeof(FJIMAGE)),pfi->lLength);
			pfiTmp->lHeight = pfi->lHeight;
			pfiTmp = fj_ImageShift( pfiTmp, 8-(pfi->lHeight & 0x7) );
			memcpy(((LPBYTE)pfi + sizeof(FJIMAGE)),((LPBYTE)pfiTmp + sizeof(FJIMAGE)),pfi->lLength);
			fj_ImageRelease( pfiTmp );
		}
		in_length = pfi->lLength;
		in_bytes  = (pfi->lHeight + 7) / 8;
		pmybuff   = mybuff;
		pBuffer   = (LPBYTE)pfi + sizeof(FJIMAGE);

		for ( i = 0; i < in_length; i += in_bytes )
		{
			for ( j = 0; j < in_bytes; j++ )
			{
				c = pBuffer[ i + j ];
				pmybuff[ j ] = rbits[c];
			}							// end for

			for ( j = 0; j < in_bytes; j++ )
			{
				pBuffer[ i + (in_bytes - 1 - j) ] = pmybuff[ j];
			}							// end for

		}
	}
	return;
}
//
//  FUNCTION: fj_ImageNegative()
//
//  PURPOSE:  apply a negative ( black to white ) transform to a raster image
//
//

FJDLLExport VOID fj_ImageNegative( LPFJIMAGE pfi )
{
	register int  i;
	register LONG Counter;
	register LONG lLength;
	register LONG lBytes;
	register LPBYTE pBuffer;
	register BYTE bMask;
	
	if ( NULL != pfi )
	{
		lLength = pfi->lLength;
		pBuffer = (LPBYTE)pfi + sizeof(FJIMAGE);
		lBytes  = (pfi->lHeight + 7) / 8;
		bMask   = 0xFF << ((lBytes * 8) - pfi->lHeight);

		Counter = lBytes;

		for ( i = 0; i < lLength ; i++ )
		{
			Counter--;
			pBuffer[i] = ~pBuffer[i];
			if (0 == Counter)
			{
				pBuffer[i] &= bMask;
				Counter = lBytes;
			}
		}
		// recalculate the number of bits/drops
		pfi->lBits = pfi->lLength*pfi->lHeight/((pfi->lHeight + 7) / 8) - pfi->lBits;
	}
	return;
}
//
//  FUNCTION: fj_ImageWidth()
//
//  PURPOSE:  apply a width transform to a raster image
//
//

FJDLLExport LPFJIMAGE fj_ImageWidth( LPFJIMAGE pfiIn, LONG lWidth )
{
	LPFJIMAGE pfiOut;
	LPFJIMAGE pfiTest;
	register LPBYTE pBufferIn;
	register LPBYTE pBufferOut;
	register LPBYTE pBufferOutEnd;

	register LONG byte;
	register LONG lBytes;
	register LONG lBytesWidth;
	LONG lLength;

	pfiOut = pfiIn;
	if ( NULL != pfiIn )
	{
		lBytes = (pfiIn->lHeight + 7) / 8;
		lBytesWidth = lBytes * (lWidth-1);
		lLength = (pfiIn->lLength * lWidth);

		pfiTest = fj_ImageNew( lLength );
		if ( NULL != pfiTest )
		{
			pfiOut = pfiTest;
			pfiOut->lHeight     = pfiIn->lHeight;
			pfiOut->lTransforms = pfiIn->lTransforms;
			pfiOut->lBits       = pfiIn->lBits;
			pBufferIn  = (LPBYTE)pfiIn  + sizeof(FJIMAGE);
			pBufferOut = (LPBYTE)pfiOut + sizeof(FJIMAGE);
			pBufferOutEnd = pBufferOut + lLength;

			while ( pBufferOut < pBufferOutEnd )
			{
				for ( byte = 0; byte < lBytes; byte++ )
				{
					*pBufferOut++ = *pBufferIn++;
				}
				pBufferOut += lBytesWidth;
			}
			fj_ImageRelease( pfiIn );
		}
	}
	return ( pfiOut );
}
//
//  FUNCTION: fj_ImageBold()
//
//  PURPOSE:  apply a bold transform to a raster image
//
//

FJDLLExport LPFJIMAGE fj_ImageBold( LPFJIMAGE pfiIn, LONG lBold )
{
	LPFJIMAGE pfiOut;
	LPFJIMAGE pfiTest;
	register LPBYTE pBufferIn;
	register LPBYTE pBufferOut;
	//	register LPBYTE pBufferOutEnd;

	register int p1;
	register int p2;
	register int byte;
	register int bold;
	register LONG lBytes;
	LONG lLength;

	pfiOut = pfiIn;
	if ( NULL != pfiIn )
	{
		lBytes     = (pfiIn->lHeight + 7) / 8;
		lLength = (pfiIn->lLength * (lBold + 1));
		pfiTest = fj_ImageNew( lLength );
		if ( NULL != pfiTest )
		{
			pfiOut = pfiTest;
			pfiOut->lHeight     = pfiIn->lHeight;
			pfiOut->lTransforms = pfiIn->lTransforms;
			pfiOut->lBits       = pfiIn->lBits * (lBold + 1);
			pBufferIn  = (LPBYTE)pfiIn  + sizeof(FJIMAGE);
			pBufferOut = (LPBYTE)pfiOut + sizeof(FJIMAGE);
			//	pBufferOutEnd = pBufferOut + lLength;
			p1 = 0;
			p2 = 0;

			while (p1 < lLength)
			{
				for (bold = 0; bold <= lBold; bold++)
				{
					for (byte = 0; byte < lBytes; byte++)
					{
						pBufferOut[p1++] = pBufferIn[p2 + byte];
					}
				}
				p2 += lBytes;
			}
			fj_ImageRelease( pfiIn );
		}
	}
	return( pfiOut );
}
/* TODO: rem
//
//  FUNCTION: fj_ImageSlant()
//
//  PURPOSE:  apply a slant transform to a raster image
//
//

FJDLLExport LPFJIMAGE fj_ImageSlant( LPFJIMAGE pfiIn, LONG lSlant )
{
  
	LPFJIMAGE pfiOut;
	LPFJIMAGE pfiTest;
	LPBYTE pBufferIn;
	LPBYTE pBufferOut;
	//	LPBYTE pBufferOutEnd;

	int   lLength;
	int   slantspace;
	LONG  lBytes;
	register BYTE chin;
	register BYTE chout;
	register BYTE bit;
	register int b;
	register LPBYTE sBuff2;
	register LPBYTE pBuff1;
	LONG    lSlantBytes;
	LONG    lSlantBytes8;
	LPBYTE sBuff1;
	LPBYTE pBuff2;

	pfiOut = pfiIn;
	if ( NULL != pfiIn )
	{
		lBytes       = (pfiIn->lHeight + 7) / 8;
		lSlantBytes  = lSlant * lBytes;
		lSlantBytes8 = 8 * lSlantBytes;

		slantspace = ( ((pfiIn->lHeight+7)/8)*8 * abs(lSlantBytes));
		lLength = pfiIn->lLength + slantspace;
/*
{
    char sz [512];
    sprintf (sz, "TODO: rem // slantspace: %d (((%-3d+7)/8)*8 * abs(%-3d) [%-3d * (%-3d + 7) / 8)] ",
	    slantspace,
	    pfiIn->lHeight,
	    lSlantBytes,
	    lSlant,
	    pfiIn->lHeight);		
    fj_socketSendAll(IPC_STATUS, (LPBYTE)sz, strlen (sz));
}
* /
		
		pfiTest = fj_ImageNew( lLength );
		if ( NULL != pfiTest )
		{
			pfiOut = pfiTest;
			pfiOut->lHeight     = pfiIn->lHeight;
			pfiOut->lTransforms = pfiIn->lTransforms;
			pfiOut->lBits       = pfiIn->lBits;
			pBufferIn  = (LPBYTE)pfiIn  + sizeof(FJIMAGE);
			pBufferOut = (LPBYTE)pfiOut + sizeof(FJIMAGE);
			//	pBufferOutEnd = pBufferOut + lLength;

			pBuff1 = pBufferIn;
			pBuff2 = pBufferIn + pfiIn->lLength;
			sBuff1 = pBufferOut;
			if ( 0 < lSlant ) sBuff1 = pBufferOut + slantspace;
			while ( pBuff1 < pBuff2 )
			{
				sBuff2 = sBuff1;
				for (b = 0; b < lBytes; b++)
				{
					chin = *pBuff1;
					if ( 0 != chin )
					{
						for ( bit = 0x80; bit != 0; bit>>=1 )
						{
							chout = chin & bit;
							if ( 0 != chout )
							{
								*sBuff2 |= chout;
							}
							sBuff2 -= lSlantBytes;
						}
					}
					else
					{
						sBuff2 -= lSlantBytes8;
					}
					sBuff2++;
					pBuff1++;
					sBuff1++;
				}

			}
			fj_ImageRelease( pfiIn );
		}
	}
	return( pfiOut );
}
*/
//  table to count bits in a byte
BYTE  fj_ImageCountBits[256] =
{
	0x00,0x01,0x01,0x02,0x01,0x02,0x02,0x03,0x01,0x02,0x02,0x03,0x02,0x03,0x03,0x04,
	0x01,0x02,0x02,0x03,0x02,0x03,0x03,0x04,0x02,0x03,0x03,0x04,0x03,0x04,0x04,0x05,
	0x01,0x02,0x02,0x03,0x02,0x03,0x03,0x04,0x02,0x03,0x03,0x04,0x03,0x04,0x04,0x05,
	0x02,0x03,0x03,0x04,0x03,0x04,0x04,0x05,0x03,0x04,0x04,0x05,0x04,0x05,0x05,0x06,
	0x01,0x02,0x02,0x03,0x02,0x03,0x03,0x04,0x02,0x03,0x03,0x04,0x03,0x04,0x04,0x05,
	0x02,0x03,0x03,0x04,0x03,0x04,0x04,0x05,0x03,0x04,0x04,0x05,0x04,0x05,0x05,0x06,
	0x02,0x03,0x03,0x04,0x03,0x04,0x04,0x05,0x03,0x04,0x04,0x05,0x04,0x05,0x05,0x06,
	0x03,0x04,0x04,0x05,0x04,0x05,0x05,0x06,0x04,0x05,0x05,0x06,0x05,0x06,0x06,0x07,
	0x01,0x02,0x02,0x03,0x02,0x03,0x03,0x04,0x02,0x03,0x03,0x04,0x03,0x04,0x04,0x05,
	0x02,0x03,0x03,0x04,0x03,0x04,0x04,0x05,0x03,0x04,0x04,0x05,0x04,0x05,0x05,0x06,
	0x02,0x03,0x03,0x04,0x03,0x04,0x04,0x05,0x03,0x04,0x04,0x05,0x04,0x05,0x05,0x06,
	0x03,0x04,0x04,0x05,0x04,0x05,0x05,0x06,0x04,0x05,0x05,0x06,0x05,0x06,0x06,0x07,
	0x02,0x03,0x03,0x04,0x03,0x04,0x04,0x05,0x03,0x04,0x04,0x05,0x04,0x05,0x05,0x06,
	0x03,0x04,0x04,0x05,0x04,0x05,0x05,0x06,0x04,0x05,0x05,0x06,0x05,0x06,0x06,0x07,
	0x03,0x04,0x04,0x05,0x04,0x05,0x05,0x06,0x04,0x05,0x05,0x06,0x05,0x06,0x06,0x07,
	0x04,0x05,0x05,0x06,0x05,0x06,0x06,0x07,0x05,0x06,0x06,0x07,0x06,0x07,0x07,0x08
};

FJDLLExport LPFJIMAGE fj_ImageRasterTransform( LPFJIMAGE pfiIn, LONG lTransforms )
{
	LPFJIMAGE pfiOut;

	pfiOut = pfiIn;
	if ( NULL != pfiIn )
	{
		pfiIn->lTransforms = lTransforms;
		if ( 0 != ( lTransforms & FJ_TRANS_REVERSE  ) ) pfiOut = fj_ImageReverse( pfiOut );
		if ( 0 != ( lTransforms & FJ_TRANS_INVERSE  ) )          fj_ImageInverse( pfiOut );
		if ( 0 != ( lTransforms & FJ_TRANS_NEGATIVE ) )          fj_ImageNegative( pfiOut );
	}
	return( pfiOut );
}
FJDLLExport LPFJIMAGE fj_ImageShift( LPFJIMAGE pfiIn, LONG lBits )
{
	LPFJIMAGE pfiOut;
	LPFJIMAGE pfiTest;
	register LPBYTE pBufferIn;
	register LPBYTE pBufferOut;
	//	register LONG* plBufferIn;
	//	register LONG* plBufferOut;
	register LONG lBytesIn;
	register LONG lBytesOut;
	register LONG lCol;
	//	register ULONG lData;
	register BYTE bData;
	register BYTE bShift;
	int i,ii;

	pfiOut = pfiIn;
	if ( NULL != pfiIn )
	{
		lBytesOut = (pfiIn->lHeight + lBits + 7) / 8;
		lBytesIn = (pfiIn->lHeight + 7) / 8;
		lCol = pfiIn->lLength / lBytesIn;
		pfiTest = fj_ImageNew( lBytesOut * lCol );
		if ( NULL != pfiTest )
		{
			pfiOut = pfiTest;
			pfiOut->lHeight     = pfiIn->lHeight + lBits;
			pfiOut->lTransforms = pfiIn->lTransforms;
			pfiOut->lBits       = pfiIn->lBits;
			pfiOut->lShifted    = lBits;
			pBufferIn  = (LPBYTE)pfiIn  + sizeof(FJIMAGE);
			pBufferOut = (LPBYTE)pfiOut + sizeof(FJIMAGE);
			for ( i=0; i < lCol; i++)
			{
				bShift = 0;
				for ( ii=0; ii < lBytesIn; ii++)
				{
					bData = pBufferIn[ii];
					pBufferOut[ii] |= ((bData >> lBits) | bShift);
					bShift = (bData << (8-lBits));
					if (lBytesIn != lBytesOut) pBufferOut[ii+1] = bShift;
				}
				pBufferIn += lBytesIn;
				pBufferOut+= lBytesOut;
			}
			fj_ImageRelease( pfiIn );
		}
	}
	return( pfiOut );
}

FJDLLExport VOID fj_ImageTrace (LPFJIMAGE pfi)
{
#ifdef _WINDOWS
#pragma warning(disable:4013)

	int i, x, y;
	LONG lHeightBytes = ((pfi->lHeight + 7) / 8);
	LONG lWidth = pfi->lLength / lHeightBytes;
	LPBYTE pBuffer = (LPBYTE)pfi + sizeof (FJIMAGE);
	char sz [256];

	// "Text element"	: pfi->lHeight: 32 [4 bytes], pfi->lLength: 1056
	// "TEST"			: pfi->lHeight: 12 [2 bytes], pfi->lLength: 24

	sprintf (sz, "pfi->lHeight: %d [%d bytes], pfi->lLength: %d\n", pfi->lHeight, lHeightBytes, pfi->lLength);
	OutputDebugString (sz); 

	for (y = 0; y < lWidth; y++) {
		strcpy (sz, "");

		for (x = 0; x < lHeightBytes; x++) {
			BYTE n = pBuffer [(lHeightBytes * y) + x];

			for (i = 7; i >= 0; i--)
				strcat (sz, (n & (1 << i)) ? "X" : ".");

			//strcat (sz, " ");
		}

		strcat (sz, "\n");
		OutputDebugString (sz); 
	}

	OutputDebugString ("\n"); 
#endif
}

FJDLLExport LPFJIMAGE fj_ImageHeightBold (LPFJIMAGE pfiIn, LONG lBold)
{
	LPFJIMAGE pfiOut;
	LPFJIMAGE pfiTest;
	register LONG  lFactor = lBold + 1;
	register int   i, j, x;
	register LONG  lBytes;
	register LPBYTE pBufferIn;
	register LPBYTE pBufferOut;

	if (lBold <= 1)
		return pfiIn;

	pfiOut = pfiIn;

	if (pfiIn && pfiIn->lHeight) {
		ULONG lHeightSrc		= ((pfiIn->lHeight) + 7) / 8;
		ULONG lWidthSrc			= (pfiIn->lLength) / lHeightSrc;
		//ULONG lBufferLenSrc		= lWidthSrc * lHeightSrc;

		ULONG lHeightDest		= ((pfiIn->lHeight * lFactor) + 7) / 8;
		ULONG lWidthDest		= lWidthSrc;
		ULONG lBufferLenDest	= lWidthDest * lHeightDest;

		pfiTest = fj_ImageNew (lBufferLenDest);

		if (pfiTest) {
			LONG lDestBit = 0;

			pfiOut				= pfiTest;
			pfiOut->lHeight     = pfiIn->lHeight * lFactor;
			pfiOut->lTransforms = pfiIn->lTransforms;
			pfiOut->lBits       = pfiIn->lBits * lFactor;

			lBytes   = (pfiIn->lHeight + 7) / 8;
			pBufferIn  = (LPBYTE)pfiIn  + sizeof(FJIMAGE);
			pBufferOut = (LPBYTE)pfiOut + sizeof(FJIMAGE);

			for (i = 0; i < pfiIn->lLength; i += lBytes) {
				for (j = 0; j < pfiIn->lHeight; j++) {
					LONG lSrcBit = ((i * 8) + j);
					LONG lSrcByte = lSrcBit / 8;
					LPBYTE pSrc = (LPBYTE)((LONG)pBufferIn + (LONG)lSrcByte);
					BOOL bBit = * pSrc & (1 << (7 - (lSrcBit % 8)));
					
					for (x = 0; x < lFactor; x++, lDestBit++) {
						LONG lDestByte = lDestBit / 8;
						LPBYTE pDest = (LPBYTE)((LONG)pBufferOut + (LONG)lDestByte);

						if (lDestByte < pfiOut->lLength) {
							if (bBit)
								* pDest |= (1 << (7 - (lDestBit % 8)));
						}
						else {
							#ifdef _WINDOWS
							OutputDebugString (bBit ? "ERROR" : "");
							#endif 
						}
					}
				}

				while (lDestBit % 8)
					lDestBit++;
			}

			fj_ImageRelease (pfiIn);
		}
	}

	return pfiOut;
}

FJDLLExport LPFJIMAGE fj_ImageHeight (LPFJIMAGE pfiIn, LONG lHeight)
{
	LPFJIMAGE pfiOut;
	LPFJIMAGE pfiTest;
	register LONG  lFactor = lHeight;
	register int   i, j, x;
	register LONG  lBytes;
	register LPBYTE pBufferIn;
	register LPBYTE pBufferOut;

	if (lHeight <= 0)
		return pfiIn;

	pfiOut = pfiIn;

	if (pfiIn) {
		ULONG lHeightSrc		= ((pfiIn->lHeight) + 7) / 8;
		ULONG lWidthSrc			= (pfiIn->lLength) / lHeightSrc;
		//ULONG lBufferLenSrc		= lWidthSrc * lHeightSrc;

		ULONG lHeightDest		= ((pfiIn->lHeight * lFactor) + 7) / 8;
		ULONG lWidthDest		= lWidthSrc;
		ULONG lBufferLenDest	= lWidthDest * lHeightDest;

		pfiTest = fj_ImageNew (lBufferLenDest);

		if (pfiTest) {
			LONG lDestBit = 0;

			pfiOut				= pfiTest;
			pfiOut->lHeight     = pfiIn->lHeight * lFactor;
			pfiOut->lTransforms = pfiIn->lTransforms;
			pfiOut->lBits       = pfiIn->lBits;

			lBytes   = (pfiIn->lHeight + 7) / 8;
			pBufferIn  = (LPBYTE)pfiIn  + sizeof(FJIMAGE);
			pBufferOut = (LPBYTE)pfiOut + sizeof(FJIMAGE);

			for (i = 0; i < pfiIn->lLength; i += lBytes) {
				for (j = 0; j < pfiIn->lHeight; j++) {
					LONG lSrcBit = ((i * 8) + j);
					LONG lSrcByte = lSrcBit / 8;
					LPBYTE pSrc = (LPBYTE)((LONG)pBufferIn + (LONG)lSrcByte);
					BOOL bBit = * pSrc & (1 << (7 - (lSrcBit % 8)));
					
					for (x = 0; x < lFactor; x++, lDestBit++) {
						LONG lDestByte = lDestBit / 8;
						LPBYTE pDest = (LPBYTE)((LONG)pBufferOut + (LONG)lDestByte);

						if (lDestByte < pfiOut->lLength) {
							if (bBit && x == 0)
								* pDest |= (1 << (7 - (lDestBit % 8)));
						}
						else {
							#ifdef _WINDOWS
							OutputDebugString (bBit ? "ERROR" : "");
							#endif 
						}
					}
				}

				while (lDestBit % 8)
					lDestBit++;
			}

			fj_ImageRelease (pfiIn);
		}
	}

	return pfiOut;
}
