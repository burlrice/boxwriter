#ifndef FJ_FILE_H_
#define FJ_FILE_H_

#include "fj_base.h"

#ifdef __cplusplus
extern "C"
{
#endif

FJDLLExport ULONG ReadFileSize (char *file_name);
FJDLLExport BOOL ReadFile (int *file_length, char *file_data, char *file_name);
FJDLLExport BOOL WriteFile (int file_length, char *file_data, char *file_name);
FJDLLExport int GetFileNames (const char * lpszDir, const char * pKeyword, char * lpsz, int nLen);
FJDLLExport BOOL DeleteFile (const char * lpszDir, const char * lpszFile);
FJDLLExport BOOL DeleteAllFiles (const char * lpszDir);
FJDLLExport BOOL FileExists (const char * lpszDir, const char * lpszFile);

#ifdef __cplusplus
}
#endif

#endif /*FJ_FILE_H_*/
