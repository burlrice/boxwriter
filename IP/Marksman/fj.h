#ifndef FJ_H_
#define FJ_H_

#include <time.h>

#include "fj_defines.h"
#include "fj_base.h"
#include "fj_socket.h"
#include "fj_printhead.h"
#include "fj_system.h"

#ifndef _WINDOWS
#include "tx_api.h"
#include <boardParams.h>

typedef  devBoardParamsType        FJ_NETOSPARAM;
typedef  devBoardParamsType     * PFJ_NETOSPARAM;
typedef  devBoardParamsType     *LPFJ_NETOSPARAM;

#endif //_WINDOWS

// forward declarations
struct fjmessage;
struct fjelement;
struct fjmemstor;
struct sockaddr_in;

// ThreadX priorities run from 0 to 31 with 0 being the highest.
#define FJ_THREAD_HIGHEST_PRIORITY   0
#define FJ_THREAD_HIGH_PRIORITY      7	// NetSilicon has an Ethernet receive thread at 2 - changed to 8
#define FJ_THREAD_MEDIUM_PRIORITY   16
#define FJ_THREAD_LOW_PRIORITY      24
#define FJ_THREAD_LOWEST_PRIORITY   31
#define FJ_THREAD_PRINT_STACK_SIZE       (2*4096)
#define FJ_THREAD_PRINT_PRIORITY         (FJ_THREAD_HIGH_PRIORITY)
#define FJ_THREAD_TIME_STACK_SIZE        (2*4096)
#define FJ_THREAD_TIME_PRIORITY          (FJ_THREAD_LOW_PRIORITY+1)
#define FJ_THREAD_NAMES_STACK_SIZE       (3*4096)
#define FJ_THREAD_NAMES_PRIORITY         (FJ_THREAD_LOW_PRIORITY+2)
#define FJ_THREAD_ROM_STACK_SIZE         (2*4096)
#define FJ_THREAD_ROM_PRIORITY           (FJ_THREAD_LOW_PRIORITY+3)

// some notes about time.
// the C routines to convert a binary time to "struct tm" assume USA DST.
// it has proven difficult to control this.
// the hardware RT clock that we use gives back its data as individual fields and not as one binary number.
// therefore, all times will be stored as "struct tm" and not as binary.
//
#define FJ_RT_SECONDS            0		// register numbers for Real Time clock
#define FJ_RT_SECONDS_ALARM      1
#define FJ_RT_MINUTES            2
#define FJ_RT_MINUTES_ALARM      3
#define FJ_RT_HOURS              4
#define FJ_RT_HOURS_ALARM        5
#define FJ_RT_DAY_OF_WEEK        6
#define FJ_RT_DAY_OF_MONTH       7
#define FJ_RT_MONTH              8
#define FJ_RT_YEAR               9
#define FJ_RT_REGISTER_A        10
#define FJ_RT_REGISTER_B        11
#define FJ_RT_REGISTER_C        12
#define FJ_RT_REGISTER_D        13
#define FJ_RT_CENTURY           0x48
#define FJ_RT_REGISTER_4A       0x4A
#define FJ_RT_REGISTER_4B       0x4B
#define FJ_RT_REGISTER_A_UIP    0x80	// Update In Progress. When 0, date/time data is locked and may be safely read.
#define FJ_RT_REGISTER_A_DV2    0x40	// Countdown chain. When 0, timer runs.
#define FJ_RT_REGISTER_A_DV1    0x20	// Oscillator on. When 1, oscillator timer runs.
#define FJ_RT_REGISTER_A_DV0    0x10	// Bank select. When 0, uses original bank f registers.
#define FJ_RT_REGISTER_B_SET    0x80	// SET. When 1, locks and sets the values in the date/time registers.
#define FJ_RT_REGISTER_B_DATA_B 0x04	// Data Mode. When 1, info is in binary.
#define FJ_RT_REGISTER_B_24     0x02	// 24/12 mode. When 1, info is in 24-hour mode
#define FJ_RT_REGISTER_B_DS     0x01	// Daylight Savings Enable. When 1, the US DST rules will be internally used.
#define FJ_RT_REGISTER_D_VRT    0x80	// Valid RT bit.
#define FJ_RT_REGISTER_4B_CS    0x20	// Crystal Select for 12.5 pF capacitance

// Macro for passing IP addresses to printf octet at a time. usage: printf("IP is %u.%u.%u.%u\n", PUSH_IPADDR(ip));
#define PUSH_IPADDR(ip) (unsigned)(ip>>24),(unsigned)((ip>>16)&0xff),(unsigned)((ip>>8)&0xff),(unsigned)(ip&0xff)

#ifndef _WINDOWS
typedef struct tempinfo
{
	WORD16  wFirstTemp;					// temperature of first entry in table
	WORD16  wLastTemp;					// temperature of last entry in table
	WORD16  (*pawTempTable)[];			// pointer to array of A/D values
} TEMPINFO, FAR *LPTEMPINFO, FAR * const CLPTEMPINFO;
typedef const struct tempinfo FAR *LPCTEMPINFO, FAR * const CLPCTEMPINFO;
typedef struct voltinfo
{
	WORD16  wFirstVolt;					// voltage of first entry in table
	WORD16  wLastVolt;					// voltage of last entry in table
	WORD16  (*pawVoltTable)[];			// pointer to array of A/D values
} VOLTINFO, FAR *LPVOLTINFO, FAR * const CLPVOLTINFO;
typedef const struct voltinfo FAR *LPCVOLTINFO, FAR * const CLPCVOLTINFO;

#define FJ_AD_AVERAGE           8		// number of A/D values to average
#define FJ_AD_DELTA             20		// difference between reading to be declared "close" or "not close"

typedef struct ad						// structure for values returned from A/D converter
{
	WORD16 wTempBoard;					// AD 0
	WORD16 wHeadVoltActual;				// AD 1
	WORD16 wHeadVoltRequired;			// AD 2
	WORD16 wTemp1;						// AD 3
	WORD16 wInkLevel;					// AD 4
	WORD16 wTemp2;						// AD 5
	WORD16 wTemp3;						// AD 6
	WORD16 wUnused7;					// AD 7
} AD, FAR *LPAD;

#define HEATER_CYCLES  10
typedef struct heater					// structure for heater information
{
	LPSTRING  pNameJava;				// name of heater for Java applets
	LPSTRING  pNameText;				// name of heater for reporting
	BOOL      bHeaterPresent;			// TRUE if heater detected
	LONG      lTableNumber;				// temp table
	LONG      lTempFromTable;			// factory temp from global table
	LONG      lTempRange;				// factory temp range from global table
	LONG      lCalibration;				// calibration adjustment
	WORD16    wADValue;					// last AD value read
	ULONG     lReadyTime;				// time when 'Ready' status is to be reported
	ULONG     lReadyPercent;			// percentage for 'Ready' Time
	LONG      lTempSent;				// last temperature reported
	ULONG     lTempOnTime;				// time of heater on
	BOOL      bReady;					// ready for printing
	BOOL      bColdStart;				// temperature was cold
	BOOL      bColdStartDelay;			// ready state delayed for a short time
	BOOL      bHeaterOn;				// heater switch status
	ULONG     lHeaterTime;				// start of this cycle
	ULONG     lHeaterOn [HEATER_CYCLES];// counters for duty cycle calculations
	ULONG     lHeaterOff[HEATER_CYCLES];// counters for duty cycle calculations
} HEATER, FAR *LPHEATER;
#endif									// _WINDOWS

#define BRAM_BASE 0x03800000			// base address to use for FoxJet Battery backed up RAM
#define GA_BASE   0x03000000			// base address to use for FoxJet Gate Array
#define GA_CLOCK1_SCALE  16				// clock1 is 1/16 of the system clock

#pragma pack(1)
typedef union
{
	WORD8 all;
	struct
	{
		unsigned InternalPC    : 1;		// d7    1 = enable internal photocell; 0 = disable
		unsigned Vibrate       : 1;		// d6
		unsigned PhotoCellInt  : 1;		// d5    1 = enable PhotoCell as ENI interrupt
		unsigned FlashErrorLED : 1;		// d4    0 = Ink Full; 1 = Ink Low.   Used to tell GA about ink level
		unsigned ErrorLED      : 1;		// d3
		unsigned MasterSlave   : 1;		// d2    0 = Master (PC at head); 1 = Slave (PC at back)
		unsigned ShaftEncoder  : 1;		// d1    0 = internal clock; 1 = external shaft encoder
		unsigned HVswitch      : 1;		// d0    0 = HV on; 1 = HV off
	} bits;
} GA_WRITE_SWITCHES;
typedef union
{
	WORD8 all;
	struct
	{
		unsigned InkLevelLow   : 1;		// d7	 0 = Ink Level low
		unsigned HVStatus      : 1;		// d6   1 = HV Status OK
		unsigned AMSpurge      : 1;		// d5	 0 = not purging; 1 = purge in progress
		unsigned reservedD4    : 1;		// d4
		unsigned SEstatus      : 1;		// d3   1 = SE running
		unsigned AtTemp        : 1;		// d2	 0 = head not at temp; 1 = head at temp
		unsigned HeaterOn      : 1;		// d1    0 = heater switch off; 1 = heater switch on
		unsigned PhotoCell     : 1;		// d0    0 = photocell unblocked; 1 = photocell blocked (for software)
	} bits;
} GA_READ_STATUS;

#define GA_AMS_START   0x01
#define GA_AMS_STOP    0x02

//
// NOTE: in the following definition, the GA write switches are in a byte that has different meanings on read and write.
//       normal instructions can not be used to turn a bit on or off.
//       a routine must turn on all desired bits and write a complete byte to the GA.
//
typedef struct ga						// structure for FoxJet Gate Array interface
{
	// definitions for FoxJet board - version 2
	union
	{
		struct
		{
			WORD8  initializeENI;		// 00 initialize ENI interface
			WORD8  curve1;				// 01 curve 1 time on  - in clock1 units
			WORD8  curve2;				// 02 curve 2 time off - in clock1 units
			WORD8  curve3;				// 03 curve 3 time on  - in clock1 units
			WORD16 encoder;				// 04-05 encoder value = clock1 / frequency wanted.  0 = off.
			WORD8  vib_curve;			// 06 vibration curve time on  - in clock1 units
			WORD8  enc_divisor;			// 07 encoder divisor. effective encoder value = encoder value / encoder divisor.
			WORD16 vibration;			// 08-09 vibration value = clock1 / frequency wanted.  0 = off. hard coded to 20.
			WORD8  pwmHV;				// 0A pwm H.V. value
			WORD8  AMScontrol;			// 0B AMS control
			WORD8  RTclockAdd;			// 0C RT clock register address
			WORD8  RTclockData;			// 0D RT clock data - write
			WORD8  channels;			// 0E number of printhead channels/8. number of bytes to feed the FIFO for each print pulse.
			GA_WRITE_SWITCHES switches;	// 0F switches
			WORD8  AMSA1;				// 10    AMS A1 - number of sets of prime pulses
			WORD8  AMSA6;				// 11    AMS A6 - number of prime pulses in one set
			WORD16 AMSA2;				// 12-13 AMS A2 - vacuum pump lead time
			WORD16 AMSA3;				// 14-15 AMS A3 - width of one prime pump pulse
			WORD16 AMSA4;				// 16-17 AMS A4 - delay between sets of prime pulses
			WORD16 AMSA5;				// 18-19 AMS A5 - vacuum pump end time
			WORD16 AMSA7;				// 1A-1B AMS A7 - delay between prime pulses within one set
			WORD16 AutoPrint;			// 1C-1D Auto print inches (for internal photocell)
		} write;
		struct
		{
			WORD8  version;				// 00 version number.
			WORD8  reserved01;
			WORD8  reserved02;
			WORD8  reserved03;
			WORD16 rastercounter;		// 04-05 raster counter.
			WORD8  reserved06;
			WORD8  reserved07;
			WORD16 HStimer;				// 08-09 high speed counter. 1 microsecond. wrap in 65 milliseconds.
			WORD8  reserved0A;
			WORD8  reserved0B;
			WORD8  RTclockAdd;			// 0C RT clock register address
			WORD8  RTclockData;			// 0D RT clock data - read
			WORD8  reserved0E;
			GA_READ_STATUS status;		// 0F status bits
		} read;
	} rw;
} GA, FAR *LPGA;
#pragma pack()

// structures for print head statistics
typedef struct rastercounts
{
	ULONG  lMessages;
	ULONG  lDotsHi;						// millions
	ULONG  lDotsLo;
} RASTERCOUNTS, FAR *LPRASTERCOUNTS;
typedef struct labelcounter
{
										// name of label
	CHAR   cLabelName[FJLABEL_NAME_SIZE+1];
	ULONG  lCount;						// counter
	struct tm tmFirst;					// time of first print
	struct tm tmLast;					// time of last print
	LONG  lCount1First;					// first value for counter 1
	LONG  lCount1Last;					// last value for counter 1
	LONG  lCount2First;					// first value for counter 2
	LONG  lCount2Last;					// last value for counter 2
} LABELCOUNTER, FAR *LPLABELCOUNTER;

// structure for ROM queue entries
typedef struct romqueue
{
	ULONG  lRomEntry;					// number of entry in ROM table
	LPBYTE pBuffer;						// buffer to write into ROM entry
	ULONG  lBufferSize;					// size of buffer to write
	BOOL   bFreeBuffer;					// TRUE to free buffer after write
	BOOL   bFreeLock;					// TRUE to free ROM lock after write
	VOID   (*pCallBack)(LONG,LONG);		// pointer to routine to be called after write is complete
	// return values are ( lCallBackValue, lRet ) lRet = 0 is success.
	LONG   lCallBackValue;				// value to be passed to callback routine
	ULONG  lUnused1;
	ULONG  lUnused2;
} ROMQUEUE, FAR *LPROMQUEUE;

// structure of dynamic data segment of BRAM
typedef struct dynamicsegment
{
	BOOL    bChanged;
	STRING strDynData[FJTEXT_SIZE+1];
} DYNSEGMENT, FAR *LPDYNSEGMENT;

// structure of selected label counters in BRAM
typedef struct bramcounter
{
	LONG   lCurValue;					// Current value of counter
	STRING strElmCounter[FJCOUNTER_NAME_SIZE+1];
} BRAMCOUNTER, FAR *LPBRAMCOUNTER;

// structure to describe parts of ROM
typedef struct rom_entry
{
	STRING  strFileName[16];
	ULONG   lOffset;
	ULONG   lMaxLength;
	BOOL    bExecutable;				// execute RomEntry after download
	BOOL    bReboot;					// reset and reboot after download
} ROMENTRY, FAR *LPROMENTRY, FAR * const CLPROMENTRY;
typedef const struct rom_entry FAR *LPCROMENTRY, FAR * const CLPCROMENTRY;
// NOTE: these #defines must agree with the first ROMENTRY's in fj_RomTable
#define ROM_LOADERBIN 1
#define ROM_RAMBIN    2
#define ROM_GABIN     3
#define ROM_FONTMSG   4
#define ROM_LOGOJPG   5
#define ROM_PARMFJ    6
#define ROM_PARMNETOS 7

#ifndef _WINDOWS
typedef struct fjsocket
{
	BOOL   bInUse;						// true if this structure is in use
	BOOL   bBrowserApplet;				// true if this socket is for a browser applet
	BOOL   bDebugger;					// true if this socket is for the debugger application
	BOOL   bFTP;						// true if this for a FTP connection
	BOOL   bIOBox;						// true if this socket is to the Net I/O box
	ULONG  lIPaddr;						// remote address of connection
	int    iSocketRW;					// socket for read/write to a port
	LONG   lSecLevel;					// security level for this socket. set from web page. used by FTP.
	ULONG  lSeconds;					// time when entry as last active. 0 = never. zeroed after some interval.
	ULONG  lAckTime;					// time for a "keep alive" write
	ULONG  lBatchIDSend;				// this stays the same for all packets that are sent as part of one command.
	ULONG  lBatchIDRecv;				// this stays the same for all packets that are read as part of one command.
	ULONG  lEchoBatchID;				// saved to avoid endless re-echos.
} FJSOCKET, *PFJSOCKET, FAR *LPFJSOCKET;

FJSOCKET fj_asockets[FJSYS_NUMBER_SOCKETS];
BOOL 	  fj_bKeepAlive;				// Burl 1.2040
struct tm fj_tmKeepAlive;				// Burl 1.2040 // last KEEP_ALIVE
BOOL 	  fj_bStrokeWatchdog;				// Burl 1.2041

#define BOOT_NUMBER          10
#define BOOT_REASON_EXTERAL  1
#define BOOT_REASON_WATCHDOG 2
#define BOOT_REASON_DEBUG    3
typedef struct bootinfo
{
	struct tm tmBoot;
	BOOL bClock;
	BOOL bBatt;
	LONG lReason;
} BOOTINFO, FAR *LPBOOTINFO, FAR * const CLPBOOTINFO;

// structure for battery backed up RAM
typedef struct fj_bram
{
	WORD32    w32Test[16];				// for verification

	struct tm tmCustomerReset;			// last time customer reset statistics
	struct tm tmInkLow;					// last time ink went low
	struct tm tmInkFull;				// last time ink went high
	struct tm tmLastPrint;				// last time a message was printed
	CHAR      sLastMessage[64];			// copy of last message printed

	LONG      lAMS_SST;					// seconds - time for next AMS to be scheduled
	LONG      lAMS_SST_wait;			// seconds - time for next AMS to actually start, after inactive delay
	BOOL      bAMS_wait;				// waiting for inactive period
	BOOL      bAMS_on;					// AMS wanted on - relayed to GA in common subroutine

	LONG      lSTDB_SST;				// seconds - time of inactivity to wait before activating StandBy mode

	BOOL      bInkLow;					// ink low indicator. derived from A/D value.

	BOOTINFO aBoots[BOOT_NUMBER];		// boot log

	WORD32    wID;						// for verification

										// selected label name
	STR       strLabelName[1+FJLABEL_NAME_SIZE];
	LONG      lSelectedCount;
										// default label name for scanner
	STR       strScannerLabelName[1+FJLABEL_NAME_SIZE];
	BOOL    bPrintIdle;					// print idle

	RASTERCOUNTS countInService;
	RASTERCOUNTS countCalMonth;
	RASTERCOUNTS countCalDay;
	RASTERCOUNTS countClockHour;
	RASTERCOUNTS count30Days;
	RASTERCOUNTS count24Hours;
	RASTERCOUNTS count60Minutes;
	RASTERCOUNTS countCustomer;
	RASTERCOUNTS countInkFull;
	RASTERCOUNTS countInkLow;
	RASTERCOUNTS countMinutes[60];
	RASTERCOUNTS countHours[24];
	RASTERCOUNTS countDays[30];

	DOUBLE  dInkLowML;					// ink low ML counter
	BOOL    bInkLowMax;					// ink low ML counter

	LABELCOUNTER LabelCounters[FJSYS_NUMBER_LABEL_COUNTERS];
	BRAMCOUNTER LabelBRAMCounters[FJMESSAGE_NUMBER_SEGMENTS];
	BYTE BRAMcountElm;
	devBoardParamsType nvParamsBRAM;


	WORD8     w8Test[16];				// for verification
} FJ_BRAM, FAR *LPFJ_BRAM, FAR * const CLPFJ_BRAM;
typedef const struct fj_bram FAR *LPCFJ_BRAM, FAR * const CLPCFJ_BRAM;

#define FJ_PC_MODE_ENABLED   1
#define FJ_PC_MODE_DISABLED  2
#define FJ_PC_MODE_TEST      3			// photocell on, but date/time/counters do not change

// structure for global dynamic variables
typedef struct fj_global
{
	LPBYTE    pROMLow;					// low address of ROM
	LPBYTE    pROMHigh;					// high address of ROM
	ULONG     lROMSize;					// size of ROM
	LPBYTE    pRAMLow;					// low address of RAM
	LPBYTE    pRAMHigh;					// high address of RAM
	ULONG     lRAMSize;					// size of RAM
	LPBYTE    pRAMUnused;				// lowest unused RAM location
	LPBYTE    pBRAMLow;					// low address of battery backed up RAM
	LPBYTE    pBRAMHigh;				// high address of battery backed up RAM
	ULONG     lBRAMSize;				// size of battery backed up RAM
	LPBYTE    pBRAMUnused;				// lowest unused battery backed up RAM location
	LPBYTE    fj_ftp_lRamBuffer;		// buffer for ftp download into ram
	ULONG     fj_ftp_lRamBufferSize;	// Size of buffer for ftp download into ram
	BOOL      fj_ftp_bBufferInUse;		// set from start of read until end of write into ROM

	ip_addr   ipaddress;				// IP address
	ip_addr   ipsubnet;					// IP subnet
	ip_addr   ipdefgateway;				// IP gateway
	ip_addr   iploopback;				// IP address for loopback
	ip_addr   ipbroadcastsubnet;		// IP subnet broadcast address
	ip_addr   ipbroadcastnet;			// IP net broadcast address
										// DNS internet addresses
	ip_addr   aipAddrDNS[FJSYS_NUMBER_DNS];
	ip_addr   ipAddrEmail;				// ip address for Email server
	ULONG     lEmailHandle;				// Email handle

	BYTE      macaddr[6];				// Ethernet MAC address;

	LONG      lSocketCount;				// number of sockets to process - either 1 or FJSYS_NUMBER_SOCKETS
	LONG      lIOBoxSocketIndex;		// IOBox Socket Index

	BOOL      bGAAvail;					// TRUE if GA available and if it is to be used
	LONG      lGASeconds;				// used to simulate a RT clock if GA not available
	struct tm tmGA;						// used to simulate a RT clock if GA not available

	BOOL      bInitComplete;			// init has finished
	BOOL      bReset;					// TRUE = reset the chip. stops the WatchDog strobe to cause a chip reset.
	BOOL      bGAUpgrade;				// TRUE = reset the chip. stops the WatchDog strobe to cause a chip reset.
	BOOL      bReady;					// all systems ready to print
	BOOL      bADReady;					// AD average values computed
	BOOL      bVoltageReady;			// voltage has been read back after setting
	BOOL      bBatteryOK;				// battery reported OK by RTC chip
	BOOL      bClockOK;					// clock OK at poewr on - or clock set after poewr on
	BOOL      bShaftEncoderRunning;		// shaft encoder is generating pulses
	BOOL      bPrintHeadPresent;		// print head detected
	BOOL      bHighVoltageOn;			// turn hign voltage on
	BOOL      bStandByOn;				// turn StandBy mode on for WaxJet PH
	LONG      lHighVoltageWanted;		// voltage wanted - determined from printhead resister
	LONG      lHighVoltageActual;		// actual voltage read from
	LONG      lPWMHV;					// PWM value for high voltage supply
	LONG      lBoardTemp;				// Board temperature
	HEATER    heater1;					// heater 1 temperature info
	STRING    strHeadStatus[256];		// head status string

	LONG      lunused;					// for some strange reason, without this line, the compiler
	// blows up with the 2 lHighVoltage variable above - only for compiling _test.c

	GA        gaRead;					// save the last values read from the Gate Array
	GA        gaWrite;					// save the last values written to the Gate Array

	BOOL      bDebug;					// TRUE = debug in RAM. FALSE = ROM.
	ULONG     lXtalFreq;				// internal chip frequency for GEN and SER modules
	ULONG     lCPUClockFreq;			// cpu clock frequency
	ULONG     lGAClockFreq1;			// gate array clock1 frequency
	ULONG     lSeconds;					// seconds counter. good for 136 years after each power on.
	// the real time may go backwards or forwards when it is synched with an external clock.
	// this seconds counter should be used for all interval or elapsed time calculations.
	// if you want to wait for 10 seconds or 5 minutes, use this lSeconds value.

	// These fields are used with the RealTime clock.
	// The RT clock has a battery to keep it running.
	// This single battery also backs up the RAM chip.
	// If the battery is low, the entire board will not run because the dynmaic data in RAM will be bad, as well as the clock.
	//
	// these two ip adresses are also switches - only one should be non-zero.
	// if both are 0, an external clock was never found.
	ip_addr   ipTimeUDP;				// UDP ip address found/used for tod clock.
	ip_addr   ipTimeTCP;				// TCP ip address found/used for tod clock.
	ip_addr   ipTimeIP;					// IP printer address found/used for tod clock.

	ULONG     lRasterCount;				// counter for segment timing/distance
	ULONG     lRasterCountPC;			// RasterCount at the time the photocell was detected
	LONG      lPhotocellCount;			// counter for photocell timing/arming/de-bounce
	BOOL      bDataReady;				// TRUE when data queued for printing
	BOOL      bPhotocell;				// TRUE when photocell detected
	BOOL      bPhotocellInt;			// TRUE when photocell interrupt wanted from GA
	LONG      lPhotocellMode;			// enabled/disabled/test - test will print but not change date/time/counters
	BOOL      bStatusReport;			// TRUE when the system status reporting to socket connections

	// these flags are set in interrupt routines - where extensive processing and/or use of stack is not allowed.
	// these flags are checked by the high priority thread to send the messages outside the interrupt routine.
	BOOL      bPrint;					// print the current message
	BOOL      bPrintTestLoop;			// buld the current message, but don't print it
	BOOL      bSendDataReady;			// send a DataReady status message
	BOOL      bSendPhotocell;			// send a Photocell status message
	BOOL      bSendPhotocellDetect;		// send a Photocell Detect message
	// these flags are checked by the low priority background thread.
	// bBuildFailure is only used for informational messages - not status - not Email
	BOOL      bBuildFailure;			// set when a message can not be built between the photocell and the print head
	// bBufferFailure is only used for informational messages - not status - not Email
	BOOL      bBufferFailure;			// set when a new ENI FIFO buffer can not be filled before the active one is finished

	LPBYTE    pMemStorBuffer;			// memstor buffer for normal use
	LPBYTE    pMemStorBufferEdit;		// memstor buffer when editing

	LPBYTE    pLogo;					// address of JPEG logo to use in web pages
	LONG      lLogoSize;				// size of logo
	LONG      lLogoWidth;				// width of logo
	LONG      lLogoHeight;				// height of logo

	TX_THREAD thrPrint;					// thread control block for print thread
	TX_THREAD thrNames;					// thread control block for names thread
	TX_THREAD thrTime;					// thread control block for time thread
	TX_THREAD thrROM;					// thread control block for rom thread

	TX_QUEUE  qROM;						// queue control block for rom thread

	TX_SEMAPHORE semaROMLock;			// semaphore to lock ROM for reading and writing
	TX_SEMAPHORE semaEditLock;			// semaphore to limit edits to one external source
	TX_SEMAPHORE semaPrintChainLock;	// semaphore to cover print chain updates - pfph->pfcLast & pfph->pfcFirst

	WORD16    wSPIReadBuffer;			// 2 byte buffer for the SPI read of A/D data
	LONG      lSPIReadLength;			// length of SPI A/D data read
	WORD16    wADRaw[8][FJ_AD_AVERAGE];	// raw values from A/D converter. keep last 8 values. some are averaged.
	WORD16    wADRaw2[8][FJ_AD_AVERAGE];// raw values from A/D converter. second set for discontinuity check.
	LONG      lADCount[8];				// count of values in wADRaw
	LONG      lADCount2[8];				// count of values in wADRaw2
	LONG      lChannel;					// current A/D channel being accessed
	AD        adValues;					// values returned from A/D converter

	struct tm tmNewManu;				// new manufacture date
	struct tm tmNewInservice;			// new inservice date
	struct tm tmFPGAProgram;			// FPGA program date

	DYNSEGMENT DynamicSegments[FJMESSAGE_DYNAMIC_SEGMENTS];
	
} FJ_GLOBAL, FAR *LPFJ_GLOBAL, FAR * const CLPFJ_GLOBAL;
typedef const struct fj_global FAR *LPCFJ_GLOBAL, FAR * const CLPCFJ_GLOBAL;
#endif									// _WINDOWS

//
// structure for "permanent" parameters
//
// this structure is used to modify the FoxJet permanent paramters in ROM.
// ROM contains these in the strings created by the opjects.
// if these are to be changed, ROM is read and these structures are updated from the permanent values.
// these values are changed, then new strings are created and written to ROM.
typedef struct fj_param
{
	FJPRINTHEAD_OP   FJPhOp;			// the operational print head descriptor being used
	FJPRINTHEAD_PHY  FJPhPhy;			// the physical print head descriptor being used
	FJPRINTHEAD_PKG  FJPhPkg;			// the package print head descriptor being used
	FJPRINTHEAD      FJPh;				// the print head information
	FJSYSTEM         FJSys;				// the system information
} FJ_PARAM, FAR *LPFJ_PARAM, FAR * const CLPFJ_PARAM;
typedef const struct fj_param FAR *LPCFJ_PARAM, FAR * const CLPCFJ_PARAM;

#ifndef _WINDOWS
// from fj_init.c
VOID fj_init(VOID);						// initialize everything
BOOL fj_noPrintDev;						// if true, do not use any FoxJet chips and/or devices

// from fj_info_soft.c
int      fj_checkSerialPort( void );
VOID     fj_writeInfo( LPCHAR pIn );
VOID     fj_reboot();
int      fj_onSOP( struct fjprinthead FAR * );
const STRING * fj_getVersionNumber(VOID);
const STRING * fj_getVersionDate(VOID);
// serial number must be greater than 20000 and less than 16,000,000
BOOL     fj_setSerial( LPSTRING pInput );
VOID     fj_setID( VOID );
VOID     fj_setGroupID( VOID );
BOOL     fj_setDateManu( struct tm *pTM );
BOOL     fj_setDateInservice( struct tm *pTM );

// from fj_memory.c
VOID  fj_memory_init(VOID);				// set up allocation blocks
BYTE *fj_memory_getOnce( LONG lSize );	// get permanent memory
//BYTE *fj_memory_getStack( LONG lSize );	// get memory for stack use. proper stack address is returned.
// definition of fj_malloc, fj_calloc and fj_free are in fjmem.h

// from fj_name_netbios.c
VOID   fj_nameInit(VOID);
VOID   fj_nameCheck(VOID);
VOID   fj_nameNETBIOS(VOID);			// create the netbios-format name from the normal ID
VOID   fj_groupNETBIOS(VOID);			// create the netbios-format name from the group ID

// from fj_print_head.c
VOID   fj_print_head_PrintMessage( struct fjmessage FAR *pfm );
VOID   fj_print_head_SetStatus(VOID);
BOOL   fj_print_head_EditStart( ULONG ipAdd );
VOID   fj_print_head_EditCancel( ULONG ipAdd );
VOID   fj_print_head_EditSave( ULONG ipAdd );
VOID   fj_print_head_Check(VOID);
VOID   fj_print_head_SetGAValues(VOID);	// set all the values into the Gate Array.

// from fj_print_head_info.c
VOID   fj_print_head_SendPhotocellInfo(BOOL bPhotocellOn);
VOID   fj_print_head_SendPhotocellDetect( VOID );
VOID   fj_print_head_SendPhotocellMode( VOID );
VOID   fj_print_head_SendSelectedCNT( VOID );
VOID   fj_print_head_SendDataReadyInfo( VOID );
VOID   fj_print_head_SendStatus( VOID );
VOID   fj_print_head_SendEditStatus( VOID );
VOID   fj_print_head_SendBoardTempInfo( VOID );
VOID   fj_print_head_IOBoxInfo( VOID );
VOID   fj_print_head_SendVoltageInfo( VOID );
VOID   fj_print_head_SendHeaterInfo( LPHEATER pHeater );
VOID   fj_print_head_SendAvailStorage( VOID );
VOID   fj_print_head_SendInfoAll(VOID);
VOID   fj_print_head_Counters( LONG lMessages, LONG lDots );
VOID   fj_print_head_CountsResetAll( VOID );
VOID   fj_print_head_CountsCustomerReset(VOID);

// from fj_print_head_init.c
VOID   fj_print_head_Init(VOID);

// from fj_print_head_email.c
VOID   fj_print_head_InitEmail( VOID );
VOID   fj_print_head_SendEmail( LPSTR pStrText, LPSTR pStrTitle, LPSTR pStrTo );
VOID   fj_print_head_SendEmailInk( LPSTR pStrText, LPSTR pStrTitle );
VOID   fj_print_head_SendEmailService( LPSTR pStrText, LPSTR pStrTitle );

// from fj_print_headGAENI.c
struct tm * fj_print_head_GetRTClockTM( struct tm *pTM );
VOID   fj_print_head_SetRTClockTM( struct tm *pTM );
VOID   fj_print_head_GetRTClockBattery( VOID );
VOID   fj_print_head_SetGAHV( VOID );
VOID   fj_print_head_SetGAEncoder( VOID );
VOID   fj_print_head_SetGAWriteSwitches( VOID );
VOID   fj_print_head_SetGACurve( VOID );
VOID   fj_print_head_SetGAValues( VOID );
VOID   fj_print_head_PhotoCell( VOID );
VOID   fj_print_head_PrintStepPattern( VOID );
VOID   fj_print_head_PrintTestPattern( LPSTR pPattern, LONG lCount );
VOID   fj_print_head_RasterCheck(VOID);
VOID   fj_print_head_InitENI(VOID);
VOID   fj_print_head_KillENI(VOID);

// from fj_socket.c
VOID   fj_socketInit(VOID);
VOID   fj_socketCheck(VOID);
// send a message out one socket
VOID   fj_socketSendOne( LONG lIndex, IPSockCmds cmd, LPBYTE pbyte, LONG lLength );
// send a message out all sockets
VOID   fj_socketSendAll( IPSockCmds cmd, LPBYTE pbyte, LONG lLength );
										// send a string out all debug sockets
VOID   fj_socketSendAllDebug( LPSTR pStr );
// send a buffer
VOID   fj_socketSendBuffer( LPVOID pVoid, LPBYTE pByte, LONG lLength );
// send a UDP message
VOID   fj_socketSendBufferUDP( LONG ipAddr, LPBYTE pByte, LONG lLength );

// from fj_rom.c
VOID   fj_romInit( VOID );				// create queue and thread
										// read FoxJet parameters from ROM

int    fj_ReadFJRomParams ( LPFJ_PARAM pFJ_PARAM );
// write FoxJet parameters to ROM
VOID   fj_WriteFJRomParams( LPFJ_PARAM pFJ_PARAM, BOOL bFreeLock, VOID pCallBack(LONG,LONG), LONG lCallBackValue );
//int    fj_ReadNETOSRomParams( devBoardParamsType * pFJ_NETOSPARAM );
//VOID   fj_WriteNETOSRomParams( VOID *pFJ_NETOSPARAM, BOOL bFreeLock, VOID pCallBack(LONG,LONG), LONG lCallBackValue );
WORD32 fj_checksumRomEntry( CLPCROMENTRY pRomEntry, CLPCBYTE pBuffer );
										// convert 8 byte NETsilicon serial number to string
VOID   fj_NETtoFJSerial( CHAR *pNET, LPSTR pFJ );
										// convert string to 8 byte NETsilicon serial number
VOID   fj_FJtoNETSerial( LPSTR pFJ, CHAR* pNET );

// from fj_writeROM.c
LONG   fj_writeROM( LPCROMENTRY pRomEntry, CLPCBYTE pRamBuffer, LONG lCurrentLength );

void OutputDebugString (const char * lpsz, const char * lpszFile, ULONG lLine);

#define TRACEF(s) OutputDebugString ((s), __FILE__, __LINE__)

#include "fj_cvt.h"
#endif									// _WINDOWS

extern const FJ_CVT fj_CVT;
#define pCVT            (&fj_CVT)

#endif /*FJ_H_*/
