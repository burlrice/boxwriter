/* Created with PageBuilder version 4.04 on Tue Jun  8 10:07:20 2010 */

#include "AsExtern.h"

#if RomPagerServer



/* ********************************************** */
/* *    Built from "html\network_config.htm"    * */
/* ********************************************** */

extern rpObjectDescription Pgnetwork_config;

static char Pgnetwork_config_Item_1[] =
	C_oHTML;

static char Pgnetwork_config_Item_2[] =
	C_oHEAD "\n"
	C_oMETA " http-equiv=\"Content-Type\"" C_CONTENT "\"text/html; "
	"charset=UTF-8\">\n"
	C_oTITLE "\n";

extern rpTextDisplayItem htmlProductName;

static char Pgnetwork_config_Item_4[] =
	C_xTITLE C_oMETA C_CONTENT "\"text/html; charset=windows-1252\" "
	"http-equiv=\"Content-Type\">\n"
	"<link type=\"text/css\" href=\"/stylesheet.css\" rel=\"stylesheet\">\n"
	"<script src=\"/utils.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/errhandler.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/validation.js\" language=\"JavaScript\"></script>\n"
	"<script language=\"JavaScript\">\n"
	"<!--\n"
	"function setDhcpState() {\n"
	"if (document.network_form.rbDHCP[0].checked) {\n"
	"document.network_form.ip_addr.disabled = true;\n";

static char Pgnetwork_config_Item_5[] =
	"document.network_form.submask.disabled = true;\n"
	"document.network_form.gw_addr.disabled = true;\n"
	"document.network_form.pdns_addr.disabled = true;\n"
	"document.network_form.sdns_addr.disabled = true;\n"
	"} else {\n"
	"document.network_form.ip_addr.disabled = false;\n"
	"document.network_form.submask.disabled = false;\n"
	"document.network_form.gw_addr.disabled = false;\n"
	"document.network_form.pdns_addr.disabled = false;\n"
	"document.network_form.sdns_addr.disabled = false;\n"
	"document.network_form.ip_addr.focus();\n"
	"}\n"
	"}\n";

static char Pgnetwork_config_Item_6[] =
	"var err = new ErrorHandler();\n"
	"function doValidate() {\n"
	"err.clearErrors(); // reset\n"
	"var ip = document.network_form.ip_addr;\n"
	"var submask = document.network_form.submask;\n"
	"var gw = document.network_form.gw_addr;\n"
	"var pDNS = document.network_form.pdns_addr;\n"
	"var sDNS = document.network_form.sdns_addr;\n"
	"var prefix = document.network_form.prefix_len;\n"
	"// Trim values before validation.\n"
	"ip.value = trim(ip.value);\n"
	"submask.value = trim(submask.value);\n"
	"gw.value = trim(gw.value);\n"
	"pDNS.value = trim(pDNS.value);\n";

static char Pgnetwork_config_Item_7[] =
	"sDNS.value = trim(sDNS.value);\n"
	"// name server is optional. Set to default value\n"
	"// if left blank.\n"
	"if (gw.value.length == 0)\n"
	"gw.value = \'0.0.0.0\';\n"
	"// If DHCP is enabled make sure the other ip settings are at least\n"
	"// of valid form to avoid ROMPager parsing errors. They should get\n"
	"// tossed anyway since DHCP will aquire the values.\n"
	"if (document.network_form.rbDHCP[0].checked) {\n"
	"if (!isValidIP(ip.value))\n"
	"ip.value = \'0.0.0.0\';\n"
	"if (!isDottedForm(submask.value))\n"
	"submask.value = \'0.0.0.0\';\n";

static char Pgnetwork_config_Item_8[] =
	"if (!isValidIP(gw.value))\n"
	"gw.value = \'0.0.0.0\';\n"
	"if (!isValidIP(pDNS.value))\n"
	"pDNS.value = \'0.0.0.0\';\n"
	"if (!isValidIP(sDNS.value))\n"
	"sDNS.value = \'0.0.0.0\';\n"
	"} else {\n"
	"if (!isValidIP(ip.value))\n"
	"err.addError(ip, \'Invalid or missing IP address.\');\n"
	"if (pDNS.value != \'0.0.0.0\' && !isValidIP(pDNS.value))\n"
	"err.addError(pDNS, \'Invalid or missing Primary DNS address.\');\n"
	"if (sDNS.value != \'0.0.0.0\' && !isValidIP(sDNS.value))\n"
	"err.addError(sDNS, \'Invalid or missing Secondary DNS address.\');\n";

static char Pgnetwork_config_Item_9[] =
	"if (!isDottedForm(submask.value))\n"
	"err.addError(submask, \'Invalid or missing subnet mask.\');\n"
	"if (gw.value != \'0.0.0.0\' && !isValidIP(gw.value))\n"
	"err.addError(gw, \'Invalid or missing default gateway address.\');\n"
	"else if (!isValidGateway(gw.value))\n"
	"err.addError(gw, \'Invalid gateway address can not be 0.0.0.0.\');\n"
	"}\n"
	"if (document.network_form.ipv6Static.checked) {\n"
	"if (!isInRange(prefix.value, 0, 128)) {\n"
	"err.addError(prefix, \'Invalid Prefix Length - enter value between 0 "
	"to 128.\');\n"
	"}\n"
	"}\n";

static char Pgnetwork_config_Item_10[] =
	"err.showError();\n"
	"return !err.hasError();\n"
	"}\n"
	"function setIpv6StaticState() {\n"
	"if (document.network_form.ipv6Static.checked) {\n"
	"document.network_form.ipv6_addr.disabled = false;\n"
	"document.network_form.prefix_len.disabled = false;\n"
	"document.network_form.ipv6_addr.focus();\n"
	"} else {\n"
	"document.network_form.ipv6_addr.disabled = true;\n"
	"document.network_form.prefix_len.disabled = true;\n"
	"}\n"
	"}\n"
	"function setNetworState() {\n"
	"setDhcpState();\n"
	"setIpv6StaticState();\n"
	"}\n"
	"//-->\n"
	"</script>\n";

static char Pgnetwork_config_Item_11[] =
	C_xHEAD C_oBODY " onunload=\"\" onload=\"setNetworState();\">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"4\""
	C_CELLSPACING "\"0\">\n"
	"<tr" C_BGCOLOR "ffffff\">\n"
	C_oTD;

extern char htmlLogo[];

static char Pgnetwork_config_Item_13[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<td nowrap=\"\"" C_WIDTH "\"100%\"" C_VALIGN_MIDDLE " "
	"class=\"PAGEHEADER\">\n";

extern rpTextDisplayItem htmlProductName;

static char Pgnetwork_config_Item_15[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- Messages -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	"<!-- STATUS MESSAGES -->\n";

extern rpRepeatGroupDynItem htmlStatusMsgs;

static char Pgnetwork_config_Item_17[] =
	"<!-- ERROR MESSAGES -->\n";

extern rpRepeatGroupDynItem htmlErrorMsgs;

static char Pgnetwork_config_Item_19[] =
	C_xTD C_xTR C_oTR "\n"
	"<!-- MENU -->\n"
	"<td" C_VALIGN_TOP " nowrap=\"\">\n";

extern rpItem htmlMenu[];

static char Pgnetwork_config_Item_21[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- PAGE CONTENT -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"10\""
	C_CELLSPACING "\"0\" class=\"BORDER\">\n"
	C_oTR "\n"
	"<td" C_WIDTH "\"100%\" class=\"PAGECONTENT\">\n"
	"<h1 class=\"CONTENTHEADER\"" C_ALIGN_CENTER ">Network Settings" C_xH1
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"0\""
	C_CELLSPACING "\"0\">\n"
	C_oTR " " C_oTD C_oH2 "IP v4 Settings" C_xH2 C_xTD C_xTR C_oTR
	C_oTD;

static char Pgnetwork_config_Item_22[] =
	C_NAME "\"network_form\" onsubmit=\"return doValidate();\"";

static char Pgnetwork_config_Item_24[] =
	C_oTABLE_CELLSPACING "\"0\"" C_CELLPADDING "\"2\"" C_BORDER "\"0\">\n";

extern rpOneOfSeveral getDhcp(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setDhcp(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpRadioGroupInfo Pgnetwork_config_RadioGroup_1 = {
	"rbDHCP",
	(void *) getDhcp,
	(void *) setDhcp,
	eRpVarType_Complex,
	eRpVarType_Complex
};

static char Pgnetwork_config_Item_25[] =
	C_oTR "\n"
	C_oTD;

static char Pgnetwork_config_Item_26[] =
	" onclick=\"setDhcpState();\"";

rpRadioButtonFormItem enableDHCP = {
	&Pgnetwork_config_RadioGroup_1,
	"on",
	0,
	(char *) Pgnetwork_config_Item_26
};

static char Pgnetwork_config_Item_28[] =
	C_NBSP "\n"
	"<label for=\"enableDHCP\">Obtain an IP address automatically</label>\n"
	C_xTD C_xTR C_oTR "\n"
	C_oTD;

static char Pgnetwork_config_Item_29[] =
	" onclick=\"setDhcpState();\"";

rpRadioButtonFormItem disableDHCP = {
	&Pgnetwork_config_RadioGroup_1,
	"off",
	1,
	(char *) Pgnetwork_config_Item_29
};

static char Pgnetwork_config_Item_31[] =
	C_NBSP "\n"
	"<label for=\"disableDHCP\">Use the following IP address</label>\n"
	C_xTD C_xTR C_xTABLE C_oTABLE_CELLSPACING "\"0\"" C_CELLPADDING "\"2\""
	C_BORDER "\"0\">\n"
	C_oTR "\n"
	"<td" C_WIDTH "\"40\">" C_NBSP C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">"
	"IP v4 Address:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD
	C_oTD;

extern char *getIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setIPAddress(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgnetwork_config_Item_32 = {
	"ip_addr",
	(void *) getIpAddress,
	(void *) setIPAddress,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_DotForm,
	15,
	15,
	(char *) 0
};

static char Pgnetwork_config_Item_33[] =
	C_xTD C_xTR C_oTR "\n"
	"<td" C_WIDTH "\"40\">" C_NBSP C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">"
	"Subnet Mask:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD
	C_oTD;

extern char *getSubMask(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setSubMask(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgnetwork_config_Item_34 = {
	"submask",
	(void *) getSubMask,
	(void *) setSubMask,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_DotForm,
	15,
	15,
	(char *) 0
};

static char Pgnetwork_config_Item_35[] =
	C_xTD C_xTR C_oTR "\n"
	"<td" C_WIDTH "\"40\">" C_NBSP C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">"
	"Default Gateway:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD
	C_oTD;

extern char *getGateway(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setGateway(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgnetwork_config_Item_36 = {
	"gw_addr",
	(void *) getGateway,
	(void *) setGateway,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_DotForm,
	15,
	15,
	(char *) 0
};

static char Pgnetwork_config_Item_37[] =
	C_xTD C_xTR C_oTR "\n"
	"<td" C_WIDTH "\"40\">" C_NBSP C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">"
	"Primary DNS:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD
	C_oTD;

extern char *getPrimaryDNS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setPrimaryDNS(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgnetwork_config_Item_38 = {
	"pdns_addr",
	(void *) getPrimaryDNS,
	(void *) setPrimaryDNS,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_DotForm,
	15,
	15,
	(char *) 0
};

static char Pgnetwork_config_Item_39[] =
	C_xTD C_xTR C_oTR "\n"
	"<td" C_WIDTH "\"40\">" C_NBSP C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">"
	"Secondary DNS:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD
	C_oTD;

extern char *getSecondaryDNS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setSecondaryDNS(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgnetwork_config_Item_40 = {
	"sdns_addr",
	(void *) getSecondaryDNS,
	(void *) setSecondaryDNS,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_DotForm,
	15,
	15,
	(char *) 0
};

static char Pgnetwork_config_Item_41[] =
	C_xTD C_xTR C_xTABLE C_oBR C_xTD C_xTR C_oTR " " C_oTD C_oH2 "IP v6 "
	"Settings" C_xH2 C_xTD C_xTR C_oTR " " C_oTD C_oTABLE_CELLSPACING "\"0\""
	C_CELLPADDING "\"2\"" C_BORDER "\"0\">\n"
	C_oTR "\n"
	C_oTD;

extern Boolean getIpv6Dhcp(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setIpv6Dhcp(void *theTaskDataPtr, Boolean theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpCheckboxFormItem Pgnetwork_config_Item_42 = {
	"ipv6DHCP",
	(void *) getIpv6Dhcp,
	(void *) setIpv6Dhcp,
	eRpVarType_Complex,
	eRpVarType_Complex,
	(char *) 0
};

static char Pgnetwork_config_Item_43[] =
	C_NBSP "\n"
	"<label for=\"enableDHCP\">Enable DHCP v6</label>\n"
	C_xTD C_xTR C_oTR "\n"
	C_oTD;

static char Pgnetwork_config_Item_44[] =
	" onclick=\"setIpv6StaticState();\"";

extern Boolean getIpv6Static(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setIpv6Static(void *theTaskDataPtr, Boolean theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpCheckboxFormItem Pgnetwork_config_Item_45 = {
	"ipv6Static",
	(void *) getIpv6Static,
	(void *) setIpv6Static,
	eRpVarType_Complex,
	eRpVarType_Complex,
	(char *) Pgnetwork_config_Item_44
};

static char Pgnetwork_config_Item_46[] =
	C_NBSP "\n"
	"<label for=\"disableDHCP\">Use the following static IP v6 address"
	"</label>\n"
	C_xTD C_xTR C_xTABLE C_oTABLE_CELLSPACING "\"0\"" C_CELLPADDING "\"2\""
	C_BORDER "\"0\">\n"
	C_oTR "\n"
	"<td" C_WIDTH "\"40\">" C_NBSP C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">"
	"IP v6 Address:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD
	C_oTD;

extern char *getIpv6Address(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setIPv6Address(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgnetwork_config_Item_47 = {
	"ipv6_addr",
	(void *) getIpv6Address,
	(void *) setIPv6Address,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	32,
	32,
	(char *) 0
};

static char Pgnetwork_config_Item_48[] =
	C_xTD C_xTR C_oTR "\n"
	"<td" C_WIDTH "\"40\">" C_NBSP C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">"
	"Prefix Length:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD
	C_oTD;

extern Unsigned8 getPrefixLength(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setPrefixLength(void *theTaskDataPtr, Unsigned8 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgnetwork_config_Item_49 = {
	"prefix_len",
	(void *) getPrefixLength,
	(void *) setPrefixLength,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_Unsigned8,
	3,
	3,
	(char *) 0
};

static char Pgnetwork_config_Item_50[] =
	C_xTD C_xTR C_xTABLE C_oBR "<hr class=\"SEPARATORLINE\">\n"
	C_oTABLE_CELLSPACING "\"0\"" C_CELLPADDING "\"0\"" C_BORDER "\"0\">\n"
	C_oTR "\n"
	C_oTD;

static rpButtonFormItem Pgnetwork_config_Item_51 = {
	"Apply",
	(char *) 0
};

static char Pgnetwork_config_Item_52[] =
	C_xTD C_xTR C_xTABLE C_xFORM C_xTD C_xTR C_xTABLE C_xTD C_xTR C_xTABLE
	C_xTD C_xTR
	C_xTABLE;

extern char htmlCopyright[];

static char Pgnetwork_config_Item_54[] =
	C_xBODY_xHTML;



static rpItem Pgnetwork_config_Form_1_Items[] = { 
	{ eRpItemType_FormRadioButton, (void *) &enableDHCP }, 
	{ eRpItemType_FormRadioButton, (void *) &disableDHCP }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgnetwork_config_Item_32 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgnetwork_config_Item_34 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgnetwork_config_Item_36 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgnetwork_config_Item_38 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgnetwork_config_Item_40 }, 
	{ eRpItemType_FormCheckbox, (void *) &Pgnetwork_config_Item_42 }, 
	{ eRpItemType_FormCheckbox, (void *) &Pgnetwork_config_Item_45 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgnetwork_config_Item_47 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgnetwork_config_Item_49 }, 
	{ eRpItemType_FormSubmit, (void *) &Pgnetwork_config_Item_51 }, 
	{ eRpItemType_LastItemInList } 
};

extern void networkSubmit(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpObjectExtension Pgnetwork_config_Form_1_ObjectExtension = {
	networkSubmit,
	&Pgnetwork_config,
	(rpObjectDescriptionPtr) 0,
	0,
	kRpObjFlags_None,
	(char *) Pgnetwork_config_Item_22
};

rpObjectDescription Pgnetwork_config_Form_1 = {
	"/Forms/network_config_1",
	Pgnetwork_config_Form_1_Items,
	&Pgnetwork_config_Form_1_ObjectExtension,
	(Unsigned32) 0,
	kRpPageAccess_Unprotected,
	eRpDataTypeForm,
	eRpObjectTypeDynamic
};

static rpItem Pgnetwork_config_Items[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_2 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_4 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_5 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_6 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_7 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_8 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_9 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_10 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_11 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlLogo }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_13 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_15 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlStatusMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_17 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlErrorMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_19 }, 
	{ eRpItemType_ItemGroup, (void *) &htmlMenu }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_21 }, 
	{ eRpItemType_FormHeader, (void *) &Pgnetwork_config_Form_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_24 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_25 }, 
	{ eRpItemType_FormRadioButton, (void *) &enableDHCP }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_28 }, 
	{ eRpItemType_FormRadioButton, (void *) &disableDHCP }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_31 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgnetwork_config_Item_32 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_33 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgnetwork_config_Item_34 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_35 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgnetwork_config_Item_36 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_37 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgnetwork_config_Item_38 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_39 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgnetwork_config_Item_40 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_41 }, 
	{ eRpItemType_FormCheckbox, (void *) &Pgnetwork_config_Item_42 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_43 }, 
	{ eRpItemType_FormCheckbox, (void *) &Pgnetwork_config_Item_45 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_46 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgnetwork_config_Item_47 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_48 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgnetwork_config_Item_49 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_50 }, 
	{ eRpItemType_FormSubmit, (void *) &Pgnetwork_config_Item_51 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_52 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlCopyright }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgnetwork_config_Item_54 }, 
	{ eRpItemType_LastItemInList } 
};


extern void networkConfigPagePreProc(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpObjectExtension Pgnetwork_config_ObjectExtension = {
	networkConfigPagePreProc,
	(rpObjectDescriptionPtr) 0,
	(rpObjectDescriptionPtr) 0,
	0,
	kRpObjFlags_None,
	(char *) 0
};

rpObjectDescription Pgnetwork_config = {
	"/network_config.htm",
	Pgnetwork_config_Items,
	&Pgnetwork_config_ObjectExtension,
	(Unsigned32) 0,
	kRpPageAccess_Realm1,
	eRpDataTypeHtml,
	eRpObjectTypeDynamic
};

#endif	/* RomPagerServer */
