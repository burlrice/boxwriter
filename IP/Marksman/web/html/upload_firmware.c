/* Created with PageBuilder version 4.04 on Tue Jun  8 10:07:20 2010 */

#include "AsExtern.h"

#if RomPagerServer



/* *********************************************** */
/* *    Built from "html\upload_firmware.htm"    * */
/* *********************************************** */

extern rpObjectDescription Pgupload_firmware;

static char Pgupload_firmware_Item_1[] =
	C_oHTML;

static char Pgupload_firmware_Item_2[] =
	C_oHEAD "\n"
	C_oMETA " http-equiv=\"Content-Type\"" C_CONTENT "\"text/html; "
	"charset=UTF-8\">\n"
	C_oTITLE "\n";

extern rpTextDisplayItem htmlProductName;

static char Pgupload_firmware_Item_4[] =
	C_xTITLE C_oMETA C_CONTENT "\"text/html; charset=windows-1252\" "
	"http-equiv=\"Content-Type\">\n"
	"<link type=\"text/css\" href=\"/stylesheet.css\" rel=\"stylesheet\">\n"
	"<script src=\"/utils.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/errhandler.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/validation.js\" language=\"JavaScript\"></script>\n"
	"<script language=\"JavaScript\">\n"
	"<!--\n"
	"function initPage() {\n"
	"document.upgrade_form.firmware.focus();\n"
	"}\n"
	"var err = new ErrorHandler();\n"
	"function doValidate() {\n";

static char Pgupload_firmware_Item_5[] =
	"// Clear All Errors\n"
	"err.clearErrors();\n"
	"var firmware = document.upgrade_form.firmware;\n"
	"if (isEmptyString(firmware.value))\n"
	"err.addError(firmware, \'You must select the firmware or ROM image "
	"file.\');\n"
	"err.showError();\n"
	"if (err.hasError()) return !err.hasError();\n"
	"// No Error, so Display Upgrading Message and Disable Submit\n"
	"var update_message = new getObj(\'update_message_label\');\n"
	"update_message.obj.innerHTML = \'Upload in progress. Please wait...\';"
	"\n"
	"document.upgrade_form.Upload.disabled = true;\n";

static char Pgupload_firmware_Item_6[] =
	"return 1;\n"
	"}\n"
	"//-->\n"
	"</script>\n"
	C_xHEAD C_oBODY " onload=\"initPage();\">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"4\""
	C_CELLSPACING "\"0\">\n"
	"<tr" C_BGCOLOR "ffffff\">\n"
	C_oTD;

extern char htmlLogo[];

static char Pgupload_firmware_Item_8[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<td nowrap=\"\"" C_WIDTH "\"100%\"" C_VALIGN_MIDDLE " "
	"class=\"PAGEHEADER\">\n";

extern rpTextDisplayItem htmlProductName;

static char Pgupload_firmware_Item_10[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- Messages -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	"<!-- STATUS MESSAGES -->\n";

extern rpRepeatGroupDynItem htmlStatusMsgs;

static char Pgupload_firmware_Item_12[] =
	"<!-- ERROR MESSAGES -->\n";

extern rpRepeatGroupDynItem htmlErrorMsgs;

static char Pgupload_firmware_Item_14[] =
	C_xTD C_xTR C_oTR "\n"
	"<!-- Menu -->\n"
	"<td" C_VALIGN_TOP " nowrap=\"\">\n";

extern rpItem htmlMenu[];

static char Pgupload_firmware_Item_16[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- Page Content -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"10\""
	C_CELLSPACING "\"0\" class=\"BORDER\">\n"
	C_oTR "\n"
	"<td" C_WIDTH "\"100%\" class=\"PAGECONTENT\">\n"
	"<h1 class=\"CONTENTHEADER\">Upload Firmware"
	C_xH1;

static char Pgupload_firmware_Item_17[] =
	C_NAME "\"upgrade_form\" onSubmit=\"return doValidate();\"";

static char Pgupload_firmware_Item_19[] =
	C_oP "Upload a new firmware or ROM image into flash." C_oBR "(A "
	"firmware image file must be called " C_oI "image.bin" C_xI ".\n"
	"A firmware backup/recovery image file must be called " C_oI
	"backup.bin" C_xI ".\n"
	"A ROM image file must be called " C_oI "rom.bin" C_xI ", " C_oI
	"spi_rom.bin" C_xI ", or " C_oI "romzip.bin" C_xI ".)\n"
	C_oP C_oTABLE_BORDER "\"0\"" C_CELLPADDING "\"2\"" C_CELLSPACING
	"\"0\">\n"
	C_oTR "\n"
	C_oTD "Select Image:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD C_oTD
	"<!-- The following hidden field must be before the file field. -->\n";

extern char *getWebUpgradeType(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWebUpgradeType(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgupload_firmware_Item_20 = {
	"storage",
	(void *) getWebUpgradeType,
	(void *) setWebUpgradeType,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20,
	20,
	(char *) 0
};

static rpFileFormItem Pgupload_firmware_Item_21 = {
	"firmware",
	48,
	255,
	(char *) 0
};

static char Pgupload_firmware_Item_22[] =
	C_xTD C_xTR C_xTABLE C_oBR "<hr class=\"SEPARATORLINE\">\n"
	C_oTABLE_CELLSPACING "\"0\"" C_CELLPADDING "\"0\"" C_BORDER "\"0\">\n"
	C_oTR "\n"
	C_oTD;

static rpButtonFormItem Pgupload_firmware_Item_23 = {
	"Upload",
	(char *) 0
};

static char Pgupload_firmware_Item_24[] =
	C_NBSP C_NBSP "\n"
	"<span id=\"update_message_label\">" C_NBSP "</span>\n"
	C_xTD C_xTR C_xTABLE C_xFORM C_xTD C_xTR C_xTABLE C_xTD C_xTR
	C_xTABLE;

extern char htmlCopyright[];

static char Pgupload_firmware_Item_26[] =
	C_xBODY_xHTML;



static rpItem Pgupload_firmware_Form_1_Items[] = { 
	{ eRpItemType_FormHiddenText, (void *) &Pgupload_firmware_Item_20 }, 
	{ eRpItemType_FormFile, (void *) &Pgupload_firmware_Item_21 }, 
	{ eRpItemType_FormSubmit, (void *) &Pgupload_firmware_Item_23 }, 
	{ eRpItemType_LastItemInList } 
};

extern void submitUpgradeFirmware(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpObjectExtension Pgupload_firmware_Form_1_ObjectExtension = {
	submitUpgradeFirmware,
	&Pgupload_firmware,
	(rpObjectDescriptionPtr) 0,
	0,
	kRpObjFlags_None,
	(char *) Pgupload_firmware_Item_17
};

rpObjectDescription Pgupload_firmware_Form_1 = {
	"/Forms/upload_firmware_1",
	Pgupload_firmware_Form_1_Items,
	&Pgupload_firmware_Form_1_ObjectExtension,
	(Unsigned32) 0,
	kRpPageAccess_Unprotected,
	eRpDataTypeFormMultipart,
	eRpObjectTypeDynamic
};

static rpItem Pgupload_firmware_Items[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgupload_firmware_Item_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgupload_firmware_Item_2 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgupload_firmware_Item_4 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgupload_firmware_Item_5 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgupload_firmware_Item_6 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlLogo }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgupload_firmware_Item_8 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgupload_firmware_Item_10 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlStatusMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgupload_firmware_Item_12 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlErrorMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgupload_firmware_Item_14 }, 
	{ eRpItemType_ItemGroup, (void *) &htmlMenu }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgupload_firmware_Item_16 }, 
	{ eRpItemType_FormHeader, (void *) &Pgupload_firmware_Form_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgupload_firmware_Item_19 }, 
	{ eRpItemType_FormHiddenText, (void *) &Pgupload_firmware_Item_20 }, 
	{ eRpItemType_FormFile, (void *) &Pgupload_firmware_Item_21 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgupload_firmware_Item_22 }, 
	{ eRpItemType_FormSubmit, (void *) &Pgupload_firmware_Item_23 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgupload_firmware_Item_24 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlCopyright }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgupload_firmware_Item_26 }, 
	{ eRpItemType_LastItemInList } 
};


extern void initFirmwareUpload(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpObjectExtension Pgupload_firmware_ObjectExtension = {
	initFirmwareUpload,
	(rpObjectDescriptionPtr) 0,
	(rpObjectDescriptionPtr) 0,
	0,
	kRpObjFlags_None,
	(char *) 0
};

rpObjectDescription Pgupload_firmware = {
	"/upload_firmware.htm",
	Pgupload_firmware_Items,
	&Pgupload_firmware_ObjectExtension,
	(Unsigned32) 0,
	kRpPageAccess_Realm1,
	eRpDataTypeHtml,
	eRpObjectTypeDynamic
};

#endif	/* RomPagerServer */
