/* Created with PageBuilder version 4.04 on Tue Jun  8 10:07:20 2010 */

#include "AsExtern.h"

#if RomPagerServer



/* **************************************** */
/* *    Built from "html\filelist.htm"    * */
/* **************************************** */

extern rpObjectDescription Pgfilelist;

static char Pgfilelist_Item_1[] =
	C_oHTML;

static char Pgfilelist_Item_2[] =
	C_oHEAD "\n"
	C_oMETA " http-equiv=\"Content-Type\"" C_CONTENT "\"text/html; "
	"charset=UTF-8\">\n"
	C_oTITLE "\n";

extern rpTextDisplayItem htmlProductName;

static char Pgfilelist_Item_4[] =
	C_xTITLE C_oMETA C_CONTENT "\"text/html; charset=windows-1252\" "
	"http-equiv=\"Content-Type\">\n"
	"<link type=\"text/css\" href=\"/stylesheet.css\" rel=\"stylesheet\">\n"
	"<script src=\"/utils.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/errhandler.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/validation.js\" language=\"JavaScript\"></script>\n"
	"<script language=\"JavaScript\">\n"
	"<!--\n"
	"function initPage() {\n"
	"document.upload_form.file.focus();\n"
	"}\n"
	"var err = new ErrorHandler();\n"
	"function doValidate_Upload() {\n";

static char Pgfilelist_Item_5[] =
	"// Clear All Errors\n"
	"err.clearErrors();\n"
	"var file = document.upload_form.file;\n"
	"if (isEmptyString(file.value)) {\n"
	"err.addError(file, \'You must select a file to upload.\');\n"
	"}\n"
	"err.showError();\n"
	"return !err.hasError();\n"
	"}\n"
	"//-->\n"
	"</script>\n"
	C_xHEAD C_oBODY " onload=\"initPage();\">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"4\""
	C_CELLSPACING "\"0\">\n"
	"<tr" C_BGCOLOR "ffffff\">\n"
	C_oTD;

extern char htmlLogo[];

static char Pgfilelist_Item_7[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<td nowrap=\"\"" C_WIDTH "\"100%\"" C_VALIGN_MIDDLE " "
	"class=\"PAGEHEADER\">\n";

extern rpTextDisplayItem htmlProductName;

static char Pgfilelist_Item_9[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- Messages -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	"<!-- STATUS MESSAGES -->\n";

extern rpRepeatGroupDynItem htmlStatusMsgs;

static char Pgfilelist_Item_11[] =
	"<!-- ERROR MESSAGES -->\n";

extern rpRepeatGroupDynItem htmlErrorMsgs;

static char Pgfilelist_Item_13[] =
	C_xTD C_xTR C_oTR "\n"
	"<!-- Menu -->\n"
	"<td" C_VALIGN_TOP " nowrap=\"\">\n";

extern rpItem htmlMenu[];

static char Pgfilelist_Item_15[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- Page Content -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"10\""
	C_CELLSPACING "\"0\" class=\"BORDER\">\n"
	C_oTR "\n"
	"<td" C_WIDTH "\"100%\" class=\"PAGECONTENT\">\n"
	"<h1 class=\"CONTENTHEADER\">File Management" C_xH1 C_oH3 "\n";

extern char *html_getDirName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
rpTextDisplayItem getDirName = {
	(void *) html_getDirName,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgfilelist_Item_17[] =
	C_NBSP "Directory\n"
	C_xH3;

static char Pgfilelist_Item_18[] =
	C_NAME "\"upload_form\" onSubmit=\"return doValidate_Upload();\"";

static char Pgfilelist_Item_20[] =
	C_oTABLE_BORDER "\"0\"" C_CELLPADDING "\"2\"" C_CELLSPACING "\"0\">\n"
	C_oTR "\n"
	C_oTD "Upload File:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD C_oTD
	"<!-- The following hidden field must be before the file field. -->\n";

extern char *html_getCurrentDir(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void html_setCurrentDir(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgfilelist_Item_21 = {
	"curdir",
	(void *) html_getCurrentDir,
	(void *) html_setCurrentDir,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20,
	20,
	(char *) 0
};

static rpFileFormItem Pgfilelist_Item_22 = {
	"file",
	32,
	64,
	(char *) 0
};

static char Pgfilelist_Item_23[] =
	C_xTD "<td" C_WIDTH "\"20\">" C_NBSP C_xTD
	C_oTD;

static rpButtonFormItem Pgfilelist_Item_24 = {
	"Upload",
	(char *) 0
};

static char Pgfilelist_Item_25[] =
	C_xTD C_xTR C_xTABLE C_xFORM C_oHR C_oP C_oB "Directory Contents" C_xB
	C_xP C_oBR C_oTABLE_BORDER "\"0\"" C_CELLPADDING "\"2\"" C_CELLSPACING
	"\"0\">\n"
	C_oTR "\n"
	C_oTH_ALIGN_LEFT C_WIDTH "\"60%\">Name" C_xTH C_oTH_ALIGN_RIGHT
	C_WIDTH "\"20%\">Type" C_xTH C_oTH_ALIGN_RIGHT C_WIDTH "\"20%\">Size"
	C_xTH
	C_xTR;

static char Pgfilelist_Item_27[] =
	C_oTR "\n"
	C_oTD_ALIGN_LEFT C_WIDTH "\"60%\">\n";

static char Pgfilelist_Item_29[] =
	"\n"
	C_oANCHOR_HREF "/FS/\n";

static char Pgfilelist_Item_31[] =
	"\n"
	C_oANCHOR_HREF "\n";

extern char *html_fileListUrl(void);
static rpTextDisplayItem Pgfilelist_Item_32 = {
	(void *) html_fileListUrl,
	eRpVarType_Function,
	eRpTextType_ASCII,
	20
};

static char Pgfilelist_Item_33[] =
	"?\n";

static rpItem Pgfilelist_Item_30[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_31 }, 
	{ eRpItemType_DisplayText, (void *) &Pgfilelist_Item_32 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_33 }, 
	{ eRpItemType_LastItemInList } 
};

static rpItem Pgfilelist_Item_28_Group[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_29 }, 
	{ eRpItemType_ItemGroup, (void *) &Pgfilelist_Item_30 }, 
	{ eRpItemType_LastItemInList } 
};

extern Unsigned8 html_getFileTypeIndex(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpDynamicDisplayItem Pgfilelist_Item_28 = {
	(void *) html_getFileTypeIndex,
	eRpVarType_Complex,
	2,
	Pgfilelist_Item_28_Group
};

extern rpTextDisplayItem getDirName;

static char Pgfilelist_Item_36[] =
	"\n"
	"/\n";

static char Pgfilelist_Item_37[] =
	"\n";

static rpItem Pgfilelist_Item_35_Group[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_36 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_37 }, 
	{ eRpItemType_LastItemInList } 
};

extern Unsigned8 html_getFileDirTypeIndex(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpDynamicDisplayItem Pgfilelist_Item_35 = {
	(void *) html_getFileDirTypeIndex,
	eRpVarType_Complex,
	2,
	Pgfilelist_Item_35_Group
};

extern char *html_getFileName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
rpTextDisplayItem getFileName = {
	(void *) html_getFileName,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgfilelist_Item_39[] =
	"\">\n";

extern rpTextDisplayItem getFileName;

static char Pgfilelist_Item_41[] =
	C_xANCHOR_xTD C_oTD_ALIGN_RIGHT C_WIDTH "\"20%\">\n";

extern char *html_getFileType(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgfilelist_Item_42 = {
	(void *) html_getFileType,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgfilelist_Item_43[] =
	C_xTD C_oTD_ALIGN_RIGHT C_WIDTH "\"20%\">\n";

extern Unsigned32 html_getFileSize(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgfilelist_Item_44 = {
	(void *) html_getFileSize,
	eRpVarType_Complex,
	eRpTextType_Unsigned32,
	20
};

static char Pgfilelist_Item_45[] =
	C_xTD
	C_xTR;

static rpItem Pgfilelist_Item_26_Group[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_27 }, 
	{ eRpItemType_DynamicDisplay, (void *) &Pgfilelist_Item_28 }, 
	{ eRpItemType_DisplayText, (void *) &getDirName }, 
	{ eRpItemType_DynamicDisplay, (void *) &Pgfilelist_Item_35 }, 
	{ eRpItemType_DisplayText, (void *) &getFileName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_39 }, 
	{ eRpItemType_DisplayText, (void *) &getFileName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_41 }, 
	{ eRpItemType_DisplayText, (void *) &Pgfilelist_Item_42 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_43 }, 
	{ eRpItemType_DisplayText, (void *) &Pgfilelist_Item_44 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_45 }, 
	{ eRpItemType_LastItemInList } 
};

extern void html_getFileListWhile(void *theTaskDataPtr, Signed16Ptr theIndexPtr,
		void **theRepeatGroupValuePtr);
static rpRepeatGroupDynItem Pgfilelist_Item_26 = {
	(void *) html_getFileListWhile,
	Pgfilelist_Item_26_Group
};

static char Pgfilelist_Item_46[] =
	C_xTABLE C_xTD C_xTR C_xTABLE C_xTD C_xTR
	C_xTABLE;

extern char htmlCopyright[];

static char Pgfilelist_Item_48[] =
	C_xBODY_xHTML;



static rpItem Pgfilelist_Form_1_Items[] = { 
	{ eRpItemType_FormHiddenText, (void *) &Pgfilelist_Item_21 }, 
	{ eRpItemType_FormFile, (void *) &Pgfilelist_Item_22 }, 
	{ eRpItemType_FormSubmit, (void *) &Pgfilelist_Item_24 }, 
	{ eRpItemType_LastItemInList } 
};

extern void upload_submit(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpObjectExtension Pgfilelist_Form_1_ObjectExtension = {
	upload_submit,
	&Pgfilelist,
	(rpObjectDescriptionPtr) 0,
	0,
	kRpObjFlags_None,
	(char *) Pgfilelist_Item_18
};

rpObjectDescription Pgfilelist_Form_1 = {
	"/Forms/filelist_1",
	Pgfilelist_Form_1_Items,
	&Pgfilelist_Form_1_ObjectExtension,
	(Unsigned32) 0,
	kRpPageAccess_Unprotected,
	eRpDataTypeFormMultipart,
	eRpObjectTypeDynamic
};

static rpItem Pgfilelist_Items[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_2 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_4 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_5 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlLogo }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_7 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_9 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlStatusMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_11 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlErrorMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_13 }, 
	{ eRpItemType_ItemGroup, (void *) &htmlMenu }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_15 }, 
	{ eRpItemType_DisplayText, (void *) &getDirName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_17 }, 
	{ eRpItemType_FormHeader, (void *) &Pgfilelist_Form_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_20 }, 
	{ eRpItemType_FormHiddenText, (void *) &Pgfilelist_Item_21 }, 
	{ eRpItemType_FormFile, (void *) &Pgfilelist_Item_22 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_23 }, 
	{ eRpItemType_FormSubmit, (void *) &Pgfilelist_Item_24 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_25 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &Pgfilelist_Item_26 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_46 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlCopyright }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgfilelist_Item_48 }, 
	{ eRpItemType_LastItemInList } 
};


extern void html_initFileList(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpObjectExtension Pgfilelist_ObjectExtension = {
	html_initFileList,
	(rpObjectDescriptionPtr) 0,
	(rpObjectDescriptionPtr) 0,
	0,
	kRpObjFlags_None,
	(char *) 0
};

rpObjectDescription Pgfilelist = {
	"/filelist.htm",
	Pgfilelist_Items,
	&Pgfilelist_ObjectExtension,
	(Unsigned32) 0,
	kRpPageAccess_Realm1,
	eRpDataTypeHtml,
	eRpObjectTypeDynamic
};

#endif	/* RomPagerServer */
