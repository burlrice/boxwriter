/* Created with PageBuilder version 4.04 on Tue Jun  8 10:07:20 2010 */

#include "AsExtern.h"

#if RomPagerServer



/* ************************************** */
/* *    Built from "html\reboot.htm"    * */
/* ************************************** */

extern rpObjectDescription Pgreboot;

static char Pgreboot_Item_1[] =
	C_oHTML;

static char Pgreboot_Item_2[] =
	C_oHEAD "\n"
	C_oMETA " http-equiv=\"Content-Type\"" C_CONTENT "\"text/html; "
	"charset=UTF-8\">\n"
	C_oTITLE "\n";

extern rpTextDisplayItem htmlProductName;

static char Pgreboot_Item_4[] =
	C_xTITLE C_oMETA C_CONTENT "\"text/html; charset=windows-1252\" "
	"http-equiv=\"Content-Type\">\n"
	"<link type=\"text/css\" href=\"/stylesheet.css\" rel=\"stylesheet\">\n"
	"<script src=\"/utils.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/errhandler.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/validation.js\" language=\"JavaScript\"></script>\n"
	C_xHEAD C_oBODY " onunload=\"\">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"4\""
	C_CELLSPACING "\"0\">\n"
	"<tr" C_BGCOLOR "ffffff\">\n"
	C_oTD;

extern char htmlLogo[];

static char Pgreboot_Item_6[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<td nowrap=\"\"" C_WIDTH "\"100%\"" C_VALIGN_MIDDLE " "
	"class=\"PAGEHEADER\">\n";

extern rpTextDisplayItem htmlProductName;

static char Pgreboot_Item_8[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- Messages -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	"<!-- STATUS MESSAGES -->\n";

extern rpRepeatGroupDynItem htmlStatusMsgs;

static char Pgreboot_Item_10[] =
	"<!-- ERROR MESSAGES -->\n";

extern rpRepeatGroupDynItem htmlErrorMsgs;

static char Pgreboot_Item_12[] =
	C_xTD C_xTR C_oTR "\n"
	"<!-- MENU -->\n"
	"<td" C_VALIGN_TOP " nowrap=\"\">\n";

extern rpItem htmlMenu[];

static char Pgreboot_Item_14[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- PAGE CONTENT -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"10\""
	C_CELLSPACING "\"0\" class=\"BORDER\">\n"
	C_oTR "\n"
	"<td" C_WIDTH "\"100%\" class=\"PAGECONTENT\">\n"
	"<h1 class=\"CONTENTHEADER\">Reboot" C_xH1 C_oTABLE_BORDER "\"0\""
	C_WIDTH "\"100%\"" C_CELLPADDING "\"0\"" C_CELLSPACING "\"0\">\n"
	C_oTR "\n"
	C_oTD;

static char Pgreboot_Item_15[] =
	C_NAME "\"reboot_form\"";

static char Pgreboot_Item_17[] =
	C_oTABLE_CELLSPACING "\"2\"" C_CELLPADDING "\"0\"" C_BORDER "\"0\">\n"
	C_oTR "\n"
	C_oTD "Click Reboot to reboot this device." C_xTD C_xTR C_xTABLE C_oBR
	"<hr class=\"SEPARATORLINE\">\n"
	C_oTABLE_CELLSPACING "\"0\"" C_CELLPADDING "\"0\"" C_BORDER "\"0\">\n"
	C_oTR "\n"
	C_oTD;

static rpButtonFormItem Pgreboot_Item_18 = {
	"Reboot",
	(char *) 0
};

static char Pgreboot_Item_19[] =
	C_xTD C_xTR C_xTABLE C_xFORM C_xTD C_xTR C_xTABLE C_xTD C_xTR C_xTABLE
	C_xTD C_xTR
	C_xTABLE;

extern char htmlCopyright[];

static char Pgreboot_Item_21[] =
	C_xBODY_xHTML;



static rpItem Pgreboot_Form_1_Items[] = { 
	{ eRpItemType_FormSubmit, (void *) &Pgreboot_Item_18 }, 
	{ eRpItemType_LastItemInList } 
};

extern void rebootSubmit(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpObjectExtension Pgreboot_Form_1_ObjectExtension = {
	rebootSubmit,
	&Pgreboot,
	(rpObjectDescriptionPtr) 0,
	0,
	kRpObjFlags_None,
	(char *) Pgreboot_Item_15
};

rpObjectDescription Pgreboot_Form_1 = {
	"/Forms/reboot_1",
	Pgreboot_Form_1_Items,
	&Pgreboot_Form_1_ObjectExtension,
	(Unsigned32) 0,
	kRpPageAccess_Unprotected,
	eRpDataTypeForm,
	eRpObjectTypeDynamic
};

static rpItem Pgreboot_Items[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_Item_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_Item_2 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_Item_4 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlLogo }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_Item_6 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_Item_8 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlStatusMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_Item_10 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlErrorMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_Item_12 }, 
	{ eRpItemType_ItemGroup, (void *) &htmlMenu }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_Item_14 }, 
	{ eRpItemType_FormHeader, (void *) &Pgreboot_Form_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_Item_17 }, 
	{ eRpItemType_FormSubmit, (void *) &Pgreboot_Item_18 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_Item_19 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlCopyright }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_Item_21 }, 
	{ eRpItemType_LastItemInList } 
};


rpObjectDescription Pgreboot = {
	"/reboot.htm",
	Pgreboot_Items,
	(rpObjectExtensionPtr) 0,
	(Unsigned32) 0,
	kRpPageAccess_Realm1,
	eRpDataTypeHtml,
	eRpObjectTypeDynamic
};

#endif	/* RomPagerServer */
