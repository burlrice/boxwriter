/* Created with PageBuilder version 4.04 on Tue Jun  8 10:07:20 2010 */

#include "AsExtern.h"

#if RomPagerServer



/* ******************************************************* */
/* *    Built from "html\wireless_network_config.htm"    * */
/* ******************************************************* */

extern rpObjectDescription Pgwireless_network_config;

static char Pgwireless_network_config_Item_1[] =
	C_oHTML;

static char Pgwireless_network_config_Item_2[] =
	C_oHEAD "\n"
	C_oMETA " http-equiv=\"Content-Type\"" C_CONTENT "\"text/html; "
	"charset=UTF-8\">\n"
	C_oTITLE "\n";

extern rpTextDisplayItem htmlProductName;

static char Pgwireless_network_config_Item_4[] =
	C_xTITLE C_oMETA C_CONTENT "\"text/html; charset=windows-1252\" "
	"http-equiv=\"Content-Type\">\n"
	"<link type=\"text/css\" href=\"/stylesheet.css\" rel=\"stylesheet\">\n";

static char Pgwireless_network_config_Item_7[] =
	"<script src=\"/utils.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/errhandler.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/validation.js\" language=\"JavaScript\"></script>\n"
	"<script language=\"JavaScript\">\n"
	"<!--\n"
	"var SUBMIT_TYPE_APPLY = 0;\n"
	"var SUBMIT_TYPE_UPDATE_COUNTRY = 1;\n"
	"var SUBMIT_TYPE_UPDATE_BAND = 2;\n"
	"var allWEPEncryptTypes=[\"None|0\", \"64-bit|5\", \"128-bit|13\"];\n"
	"var wpaWEPEncryptTypes=[\"64-bit|5\", \"128-bit|13\"];\n"
	"var err = new ErrorHandler(); // for doValidate\n"
	"function validateWEP() {\n";

static char Pgwireless_network_config_Item_8[] =
	"var wep = document.wireless_net_form.wep_encrypt;\n"
	"var wepkey0 = document.wireless_net_form.wepKey0;\n"
	"var wepkey1 = document.wireless_net_form.wepKey1;\n"
	"var wepkey2 = document.wireless_net_form.wepKey2;\n"
	"var wepkey3 = document.wireless_net_form.wepKey3;\n"
	"// Trim values before validation.\n"
	"wepkey0.value = trim(wepkey0.value);\n"
	"wepkey1.value = trim(wepkey1.value);\n"
	"wepkey2.value = trim(wepkey2.value);\n"
	"wepkey3.value = trim(wepkey3.value);\n"
	"var wep_len = (wep.value * 2);\n";

static char Pgwireless_network_config_Item_9[] =
	"// Make sure each key is a valid length. (The resizeWEPKeys function "
	"doesn\'t affect\n"
	"// the actual values).\n"
	"wepkey0.value = wepkey0.value.substring(0, wep_len);\n"
	"wepkey1.value = wepkey1.value.substring(0, wep_len);\n"
	"wepkey2.value = wepkey2.value.substring(0, wep_len);\n"
	"wepkey3.value = wepkey3.value.substring(0, wep_len);\n"
	"// Make sure each key is empty or the proper length and all hex "
	"digits.\n"
	"if ((wepkey0.value.length != 0 && wepkey0.value.length != wep_len) || "
	"!isValidHex(wepkey0.value))\n";

static char Pgwireless_network_config_Item_10[] =
	"err.addError(wepkey0, \'WEP Key 1 must be empty or \' + wep_len + \' "
	"hex digits\');\n"
	"if ((wepkey1.value.length != 0 && wepkey1.value.length != wep_len) || "
	"!isValidHex(wepkey1.value))\n"
	"err.addError(wepkey1, \'WEP Key 2 must be empty or \' + wep_len + \' "
	"hex digits\');\n"
	"if ((wepkey2.value.length != 0 && wepkey2.value.length != wep_len) || "
	"!isValidHex(wepkey2.value))\n"
	"err.addError(wepkey2, \'WEP Key 3 must be empty or \' + wep_len + \' "
	"hex digits\');\n";

static char Pgwireless_network_config_Item_11[] =
	"if ((wepkey3.value.length != 0 && wepkey3.value.length != wep_len) || "
	"!isValidHex(wepkey3.value))\n"
	"err.addError(wepkey3, \'WEP Key 4 must be empty or \' + wep_len + \' "
	"hex digits\');\n"
	"}\n"
	"function resizeWEPKeys() {\n"
	"var wep = document.wireless_net_form.wep_encrypt;\n"
	"var wep_key_len = 0;\n"
	"if (wep.value == 5) {\n"
	"wep_key_len = 10; // 64-bit => 10 hex digits\n"
	"} else {\n"
	"wep_key_len = 26; // 128-bit => 26 hex digits\n"
	"}\n"
	"// Resize the WEP key controls\n"
	"document.wireless_net_form.wepKey0.size = wep_key_len;\n";

static char Pgwireless_network_config_Item_12[] =
	"document.wireless_net_form.wepKey1.size = wep_key_len;\n"
	"document.wireless_net_form.wepKey2.size = wep_key_len;\n"
	"document.wireless_net_form.wepKey3.size = wep_key_len;\n"
	"document.wireless_net_form.wepKey0.MAXLENGTH = wep_key_len;\n"
	"document.wireless_net_form.wepKey1.MAXLENGTH = wep_key_len;\n"
	"document.wireless_net_form.wepKey2.MAXLENGTH = wep_key_len;\n"
	"document.wireless_net_form.wepKey3.MAXLENGTH = wep_key_len;\n"
	"}\n"
	"// listCtrl is the list form control. list is a list of strings. each ";

static char Pgwireless_network_config_Item_13[] =
	"string is of the form \"text|value\".\n"
	"// Attempts to retain the currently selected value if possible. "
	"Otherwise defaults to first in list.\n"
	"function setListOptions(listCtrl, list) {\n"
	"var oldValue = listCtrl.value;\n"
	"// clear the list control and then add items from the list.\n"
	"listCtrl.options.length = 0;\n"
	"for (i=0; i<list.length; i++) {\n"
	"listCtrl.options[i]=new Option(list[i].split(\"|\")[0], "
	"list[i].split(\"|\")[1]);\n"
	"if (listCtrl.options[i].value == oldValue) {\n"
	"listCtrl.selectedIndex = i;\n"
	"}\n"
	"}\n"
	"}\n";

static char Pgwireless_network_config_Item_14[] =
	"function updateCountryName() {\n"
	"document.wireless_net_form.submit_type.value = "
	"SUBMIT_TYPE_UPDATE_COUNTRY;\n"
	"document.wireless_net_form.submit();\n"
	"}\n"
	"function updateWirelessBand() {\n"
	"document.wireless_net_form.submit_type.value = "
	"SUBMIT_TYPE_UPDATE_BAND;\n"
	"document.wireless_net_form.submit();\n"
	"}\n";

static char Pgwireless_network_config_Item_17[] =
	"/* Encryption types for dynamic list */\n"
	"var allEncrTypes=[\"Any available encryption|0\", \"Open (no "
	"encryption)|1\", \"WEP|2\", \"TKIP|3\", \"CCMP (AES)|4\"];\n"
	"var wpaEncrTypes=[\"Any available encryption|0\", \"WEP|2\", "
	"\"TKIP|3\", \"CCMP (AES)|4\"];\n"
	"var requiredWepEncrTypes=[\"WEP|2\"];\n"
	"var optionalWepEncrTypes=[\"Any available encryption|0\", \"Open (no "
	"encryption)|1\", \"WEP|2\"];\n"
	"var noAnyEncrTypes=[\"Open (no encryption)|1\", \"WEP|2\", \"TKIP|3\","
	" \"CCMP (AES)|4\"];\n"
	"function initPage() {\n";

static char Pgwireless_network_config_Item_18[] =
	"document.wireless_net_form.submit_type.value = SUBMIT_TYPE_APPLY;\n"
	"// WPA manages WEP via the encryption flag.\n"
	"setListOptions(document.wireless_net_form.wep_encrypt, "
	"wpaWEPEncryptTypes);\n"
	"updatePageState();\n"
	"}\n"
	"function updatePageState() {\n"
	"setAuthState();\n"
	"setEncryptState();\n"
	"setPassphraseState();\n"
	"setLoginState();\n"
	"setWEPState();\n"
	"}\n"
	"function setAuthState() {\n"
	"var authList = document.wireless_net_form.auth;\n"
	"var bss_type = document.wireless_net_form.bss_type;\n"
	"// WPA not supported in ad-hoc modes.\n";

static char Pgwireless_network_config_Item_19[] =
	"authList.disabled = (bss_type.value == 1 || bss_type.value == 2);\n"
	"}\n"
	"function setEncryptState() {\n"
	"var authList = document.wireless_net_form.auth;\n"
	"var encrList = document.wireless_net_form.encryp;\n"
	"var bss_type = document.wireless_net_form.bss_type;\n"
	"if (bss_type.value == 1 || bss_type.value == 2) {\n"
	"// Only WEP and Open supported in ad-hoc modes.\n"
	"setListOptions(encrList, optionalWepEncrTypes);\n"
	"} else if ((authList.value == 1) || (authList.value == 2)) {\n"
	"// Open and Shared Key\n";

static char Pgwireless_network_config_Item_20[] =
	"setListOptions(encrList, optionalWepEncrTypes);\n"
	"} else if ((authList.value == 4) || (authList.value == 5)) {\n"
	"// WPA personal and WPA 802.1x\n"
	"setListOptions(encrList, wpaEncrTypes);\n"
	"} else if ((authList.value == 3) || (authList.value == 6)) {\n"
	"// WEP 802.1x, and LEAP only support WEP (keys aquired dynamically)\n"
	"setListOptions(encrList, requiredWepEncrTypes);\n"
	"} else if ((authList.value == 0)) {\n"
	"// No Any Encryption due to the limitation in WPA for any "
	"authentication\n";

static char Pgwireless_network_config_Item_21[] =
	"setListOptions(encrList, noAnyEncrTypes);\n"
	"} else {\n"
	"// Any\n"
	"setListOptions(encrList, allEncrTypes);\n"
	"}\n"
	"}\n"
	"function setPassphraseState() {\n"
	"var passphrase = document.wireless_net_form.wpaPassphrase;\n"
	"var authList = document.wireless_net_form.auth;\n"
	"var bss_type = document.wireless_net_form.bss_type;\n"
	"// Passphrase only used for WPA personal (i.e. PSK). Not supported in "
	"ad-hoc modes.\n"
	"passphrase.disabled = (((authList.value != 0) && (authList.value != "
	"4)) ||\n";

static char Pgwireless_network_config_Item_22[] =
	"(bss_type.value == 1) || (bss_type.value == 2));\n"
	"}\n"
	"function setLoginState() {\n"
	"var authList = document.wireless_net_form.auth;\n"
	"var bss_type = document.wireless_net_form.bss_type;\n"
	"var login = document.wireless_net_form.wLogin;\n"
	"var password = document.wireless_net_form.wPassword;\n"
	"// Login credentials only supported for WEP 802.1x, WPA 802.1x, and "
	"LEAP (and any).\n"
	"// Not supported for Ad-hoc modes, either.\n"
	"disableLogin = (((authList.value != 0) && (authList.value != 3) && ";

static char Pgwireless_network_config_Item_23[] =
	"(authList.value != 5) && (authList.value != 6)) ||\n"
	"(bss_type.value == 1) || (bss_type.value == 2));\n"
	"login.disabled = disableLogin;\n"
	"password.disabled = disableLogin;\n"
	"}\n"
	"function setWEPState() {\n"
	"var authList = document.wireless_net_form.auth;\n"
	"var encrList = document.wireless_net_form.encryp;\n"
	"var bss_type = document.wireless_net_form.bss_type;\n"
	"// Only Open and Shared Key authentication require us to prompt\n"
	"// for WEP keys. Shared key always requires WEP keys because\n";

static char Pgwireless_network_config_Item_24[] =
	"// they are used for the authentication (not just data encryption).\n"
	"// The other authentication types obtain keys dynamically.\n"
	"var wep_disabled = true;\n"
	"if ((bss_type.value == 1) || (bss_type.value == 2)) {\n"
	"// Ad-hoc modes. Only prompt for WEP keys if any or WEP encryption.\n"
	"if ((encrList.value == 0) || (encrList.value == 2)) {\n"
	"wep_disabled = false;\n"
	"}\n"
	"} else if (authList.value == 0) {\n"
	"// Any authentication. Must prompt for WEP keys.\n"
	"// (NOTE: any authentication includes shared key, which always ";

static char Pgwireless_network_config_Item_25[] =
	"requires\n"
	"// WEP keys for authentication.)\n"
	"wep_disabled = false;\n"
	"} else if ((authList.value == 1) && ((encrList.value == 0) || "
	"(encrList.value == 2))) {\n"
	"// Open authentication and any or WEP encryption. Must Prompt for WEP "
	"keys.\n"
	"wep_disabled = false;\n"
	"} else if (authList.value == 2) {\n"
	"// Shared key authentication always requires WEP keys (for "
	"authentication).\n"
	"wep_disabled = false;\n"
	"}\n"
	"document.wireless_net_form.wep_encrypt.disabled = wep_disabled;\n";

static char Pgwireless_network_config_Item_26[] =
	"document.wireless_net_form.defaultWEPKey.disabled = wep_disabled;\n"
	"document.wireless_net_form.wepKey0.disabled = wep_disabled;\n"
	"document.wireless_net_form.wepKey1.disabled = wep_disabled;\n"
	"document.wireless_net_form.wepKey2.disabled = wep_disabled;\n"
	"document.wireless_net_form.wepKey3.disabled = wep_disabled;\n"
	"if (!wep_disabled) {\n"
	"resizeWEPKeys();\n"
	"}\n"
	"// Show a nice message if WEP keys obtained dynamically.\n"
	"// (WEP 802.1x, WPA Personal, WPA 802.1x, or LEAP authentication\n"
	"// and any or WEP encryption)\n";

static char Pgwireless_network_config_Item_27[] =
	"var dynamic_wep_key_label = new getObj(\'dynamic_wep_key_label\');\n"
	"if ((bss_type.value == 1) || (bss_type.value == 2)) {\n"
	"dynamic_wep_key_label.obj.innerHTML = \' \';\n"
	"} else if (authList.value == 2) {\n"
	"dynamic_wep_key_label.obj.innerHTML = \'(WEP keys required for Shared "
	"Key authentication)\';\n"
	"} else if (((authList.value == 3) || (authList.value == 4) ||\n"
	"(authList.value == 5) || (authList.value == 6)) &&\n"
	"((encrList.value == 0) || (encrList.value == 2))) {\n";

static char Pgwireless_network_config_Item_28[] =
	"dynamic_wep_key_label.obj.innerHTML = \'(WEP keys obtained "
	"dynamically)\';\n"
	"} else {\n"
	"dynamic_wep_key_label.obj.innerHTML = \' \';\n"
	"}\n"
	"}\n"
	"function doValidate() {\n"
	"err.clearErrors(); // reset\n"
	"var bss_type = document.wireless_net_form.bss_type;\n"
	"var authList = document.wireless_net_form.auth;\n"
	"var encrList = document.wireless_net_form.encryp;\n"
	"// WPA not supported in Ad-hoc modes.\n"
	"if ((bss_type.value != 1) && (bss_type.value != 2)) {\n"
	"var ssid = document.wireless_net_form.ssid;\n";

static char Pgwireless_network_config_Item_29[] =
	"var passphrase = document.wireless_net_form.wpaPassphrase;\n"
	"ssid.value = trim(ssid.value);\n"
	"// Validate passphrase if WPA personal authentication (or if Any "
	"authentication\n"
	"// specified and a passphrase was entered).\n"
	"if (authList.value == 4 || (authList.value == 0 && "
	"passphrase.value.length > 0)) {\n"
	"if (ssid.value.length == 0) {\n"
	"err.addError(ssid, \'WPA Personal authentication requires an SSID\');\n"
	"} else if (passphrase.value.length < 8) {\n"
	"err.addError(passphrase, \'Passphrase must be at least 8 ";

static char Pgwireless_network_config_Item_30[] =
	"characters\');\n"
	"}\n"
	"}\n"
	"// Only Open and Shared Key authentication require WEP keys. Shared "
	"key\n"
	"// always requires WEP keys (for authentication).\n"
	"// The other authentication types obtain keys dynamically.\n"
	"if ((authList.value == 0) ||\n"
	"((authList.value == 1) && ((encrList.value == 0) || (encrList.value =="
	" 2))) ||\n"
	"(authList.value == 2)) {\n"
	"// Any authentication OR\n"
	"// open authentication and any or WEP encryption OR\n"
	"// shared key authentication\n"
	"validateWEP();\n"
	"}\n"
	"} else {\n"
	"// Ad-hoc mode\n";

static char Pgwireless_network_config_Item_31[] =
	"if ((encrList.value == 0) || (encrList.value == 2)) {\n"
	"// Any or WEP encryption\n"
	"validateWEP();\n"
	"}\n"
	"}\n"
	"err.showError();\n"
	"return !err.hasError();\n"
	"}\n";

static rpItem Pgwireless_network_config_Item_16[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_17 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_18 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_19 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_20 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_21 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_22 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_23 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_24 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_25 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_26 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_27 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_28 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_29 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_30 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_31 }, 
	{ eRpItemType_LastItemInList } 
};

static char Pgwireless_network_config_Item_33[] =
	"function initPage() {\n"
	"document.wireless_net_form.submit_type.value = SUBMIT_TYPE_APPLY;\n"
	"// non-WPA manages WEP via wep encryption size option (includes "
	"\'none\').\n"
	"setListOptions(document.wireless_net_form.wep_encrypt, "
	"allWEPEncryptTypes);\n"
	"updatePageState();\n"
	"}\n"
	"function updatePageState() {\n"
	"setWEPState();\n"
	"}\n"
	"function setWEPState() {\n"
	"// Disable WEP keys if \'none\' selected.\n"
	"var wep = document.wireless_net_form.wep_encrypt;\n"
	"var wep_disabled = (wep.value == 0);\n";

static char Pgwireless_network_config_Item_34[] =
	"document.wireless_net_form.wepKey0.disabled = wep_disabled;\n"
	"document.wireless_net_form.wepKey1.disabled = wep_disabled;\n"
	"document.wireless_net_form.wepKey2.disabled = wep_disabled;\n"
	"document.wireless_net_form.wepKey3.disabled = wep_disabled;\n"
	"document.wireless_net_form.defaultWEPKey.disabled = wep_disabled;\n"
	"if (!wep_disabled) {\n"
	"resizeWEPKeys();\n"
	"}\n"
	"}\n"
	"function doValidate() {\n"
	"err.clearErrors(); // reset\n"
	"// Only validate WEP if it is enabled.\n"
	"if (document.wireless_net_form.wep_encrypt.value != 0) {\n";

static char Pgwireless_network_config_Item_35[] =
	"validateWEP();\n"
	"}\n"
	"err.showError();\n"
	"return !err.hasError();\n"
	"}\n";

static rpItem Pgwireless_network_config_Item_32[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_33 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_34 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_35 }, 
	{ eRpItemType_LastItemInList } 
};

static rpItem Pgwireless_network_config_Item_15_Group[] = { 
	{ eRpItemType_ItemGroup, (void *) &Pgwireless_network_config_Item_16 }, 
	{ eRpItemType_ItemGroup, (void *) &Pgwireless_network_config_Item_32 }, 
	{ eRpItemType_LastItemInList } 
};

extern Unsigned8 IS_WPA_SUPPORTED(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpDynamicDisplayItem Pgwireless_network_config_Item_15 = {
	(void *) IS_WPA_SUPPORTED,
	eRpVarType_Complex,
	2,
	Pgwireless_network_config_Item_15_Group
};

static char Pgwireless_network_config_Item_36[] =
	"//-->\n"
	"</script>\n"
	C_xHEAD C_oBODY " onunload=\"\" onload=\"initPage();\">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"4\""
	C_CELLSPACING "\"0\">\n"
	"<tr" C_BGCOLOR "ffffff\">\n"
	C_oTD;

extern char htmlLogo[];

static char Pgwireless_network_config_Item_38[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<td nowrap=\"\"" C_WIDTH "\"100%\"" C_VALIGN_MIDDLE " "
	"class=\"PAGEHEADER\">\n";

extern rpTextDisplayItem htmlProductName;

static char Pgwireless_network_config_Item_40[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- Messages -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	"<!-- STATUS MESSAGES -->\n";

extern rpRepeatGroupDynItem htmlStatusMsgs;

static char Pgwireless_network_config_Item_42[] =
	"<!-- ERROR MESSAGES -->\n";

extern rpRepeatGroupDynItem htmlErrorMsgs;

static char Pgwireless_network_config_Item_44[] =
	C_xTD C_xTR C_oTR "\n"
	"<!-- MENU -->\n"
	"<td" C_VALIGN_TOP " nowrap=\"\">\n";

extern rpItem htmlMenu[];

static char Pgwireless_network_config_Item_46[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- PAGE CONTENT -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"10\""
	C_CELLSPACING "\"0\" class=\"BORDER\">\n"
	C_oTR "\n"
	"<td" C_WIDTH "\"100%\" class=\"PAGECONTENT\">\n"
	"<h1 class=\"CONTENTHEADER\">Wireless Network Settings" C_xH1 "<!-- "
	"Tabs -->\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"0\""
	C_CELLSPACING "\"0\">\n"
	C_oTR "\n"
	"<td class=\"PAGETAB\" nowrap=\"true\"" C_WIDTH "\"10\">" C_NBSP C_xTD
	"<!-- left fill -->\n"
	"<td class=\"PAGETAB-SELECTED\" nowrap=\"true\">Wireless Network";

static char Pgwireless_network_config_Item_47[] =
	C_xTD "<td class=\"PAGETAB\" nowrap=\"true\">|" C_xTD "<!-- separator "
	"-->\n"
	"<td class=\"PAGETAB\" nowrap=\"true\">" C_oANCHOR_HREF
	"/wireless_ip_config.htm\">TCP/IP" C_xANCHOR_xTD "<td class=\"PAGETAB\""
	C_WIDTH "\"100%\">" C_NBSP C_xTD "<!-- right fill -->\n"
	C_xTR C_xTABLE "<br/>\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"0\""
	C_CELLSPACING "\"0\">\n"
	C_oTR "\n"
	C_oTD;

static char Pgwireless_network_config_Item_48[] =
	C_NAME "\"wireless_net_form\" onsubmit=\"return doValidate();\"";

extern Unsigned16 getSubmitType(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setSubmitType(void *theTaskDataPtr, Unsigned16 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_network_config_Item_50 = {
	"submit_type",
	(void *) getSubmitType,
	(void *) setSubmitType,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_Unsigned16,
	20,
	20,
	(char *) 0
};

static char Pgwireless_network_config_Item_51[] =
	C_oTABLE_CELLSPACING "\"0\"" C_CELLPADDING "\"2\"" C_BORDER "\"0\">\n"
	C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">Wireless Network Type:" C_xTD "<td"
	C_WIDTH "\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

static char Pgwireless_network_config_Item_52[] =
	" onChange=\"updatePageState()\"";

extern rpOneOfSeveral getWirelessNetworkType(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessNetworkType(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
extern rpOption_OneSelect Pgwireless_network_config_Item_53_Option_1;
extern char Pgwireless_network_config_Item_52[];
static rpFixedSingleSelectFormItem Pgwireless_network_config_Item_53 = {
	"bss_type",
	&Pgwireless_network_config_Item_53_Option_1,
	(void *) getWirelessNetworkType,
	(void *) setWirelessNetworkType,
	eRpVarType_Complex,
	eRpVarType_Complex,
	1,
	(char *) Pgwireless_network_config_Item_52
};

extern rpOption_OneSelect Pgwireless_network_config_Item_53_Option_2;
rpOption_OneSelect Pgwireless_network_config_Item_53_Option_1 = {
	&Pgwireless_network_config_Item_53_Option_2,
	"Infrastructure",
	0
};

extern rpOption_OneSelect Pgwireless_network_config_Item_53_Option_3;
rpOption_OneSelect Pgwireless_network_config_Item_53_Option_2 = {
	&Pgwireless_network_config_Item_53_Option_3,
	"Ad hoc (join or create)",
	1
};

extern rpOption_OneSelect Pgwireless_network_config_Item_53_Option_4;
rpOption_OneSelect Pgwireless_network_config_Item_53_Option_3 = {
	&Pgwireless_network_config_Item_53_Option_4,
	"Ad hoc (join only)",
	2
};

rpOption_OneSelect Pgwireless_network_config_Item_53_Option_4 = {
	(rpOption_OneSelectPtr) 0,
	"Any type",
	3
};

static char Pgwireless_network_config_Item_54[] =
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">Wireless Network Name (SSID):" C_xTD
	"<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

extern char *getSSID(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setSSID(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_network_config_Item_55 = {
	"ssid",
	(void *) getSSID,
	(void *) setSSID,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	32,
	32,
	(char *) 0
};

static char Pgwireless_network_config_Item_56[] =
	C_NBSP "(leave empty to search)\n"
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">Country Name:" C_xTD "<td" C_WIDTH
	"\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

static char Pgwireless_network_config_Item_57[] =
	" onchange=\"updateCountryName()\"";

extern void *getWirelessCountryName(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr, Unsigned8 theItemNumber, 
		Boolean *theOptionSelectedFlag, Unsigned32Ptr theValuePtr);
extern void setWirelessCountryName(void *theTaskDataPtr, Unsigned32 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
extern char Pgwireless_network_config_Item_57[];
static rpVariableSelectFormItem Pgwireless_network_config_Item_58 = {
	"country_name",
	(rpResetVarSelectPtr) 0,
	(void *) getWirelessCountryName,
	(void *) setWirelessCountryName,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	0,
	1,
	(char *) Pgwireless_network_config_Item_57
};

static char Pgwireless_network_config_Item_59[] =
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">802.11 Options: " C_xTD "<td"
	C_WIDTH "\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

extern void *get80211Options(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr, Unsigned8 theItemNumber, 
		Boolean *theOptionSelectedFlag, Unsigned32Ptr theValuePtr);
extern void set80211Options(void *theTaskDataPtr, Unsigned32 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpVariableSelectFormItem Pgwireless_network_config_Item_60 = {
	"80211_options",
	(rpResetVarSelectPtr) 0,
	(void *) get80211Options,
	(void *) set80211Options,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	0,
	1,
	(char *) 0
};

static char Pgwireless_network_config_Item_61[] =
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">Frequency Band:" C_xTD "<td" C_WIDTH
	"\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

extern char *displayWirelessBandB(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgwireless_network_config_Item_63 = {
	(void *) displayWirelessBandB,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	20
};

static char Pgwireless_network_config_Item_64[] =
	" onchange=\"updateWirelessBand()\"";

extern void *getWirelessBand(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr, Unsigned8 theItemNumber, 
		Boolean *theOptionSelectedFlag, Unsigned32Ptr theValuePtr);
extern void setWirelessBand(void *theTaskDataPtr, Unsigned32 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
extern char Pgwireless_network_config_Item_64[];
static rpVariableSelectFormItem Pgwireless_network_config_Item_65 = {
	"freq_band",
	(rpResetVarSelectPtr) 0,
	(void *) getWirelessBand,
	(void *) setWirelessBand,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	0,
	1,
	(char *) Pgwireless_network_config_Item_64
};

static rpItem Pgwireless_network_config_Item_62_Group[] = { 
	{ eRpItemType_DisplayText, (void *) &Pgwireless_network_config_Item_63 }, 
	{ eRpItemType_FormVarValueSingleDyn, (void *) &Pgwireless_network_config_Item_65 }, 
	{ eRpItemType_LastItemInList } 
};

extern Unsigned8 IS_ARM9(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpDynamicDisplayItem Pgwireless_network_config_Item_62 = {
	(void *) IS_ARM9,
	eRpVarType_Complex,
	2,
	Pgwireless_network_config_Item_62_Group
};

static char Pgwireless_network_config_Item_66[] =
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">Channel:" C_xTD "<td" C_WIDTH
	"\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

extern void *getWirelessChannel(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr, Unsigned8 theItemNumber, 
		Boolean *theOptionSelectedFlag, Unsigned32Ptr theValuePtr);
extern void setWirelessChannel(void *theTaskDataPtr, Unsigned32 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpVariableSelectFormItem Pgwireless_network_config_Item_67 = {
	"channel",
	(rpResetVarSelectPtr) 0,
	(void *) getWirelessChannel,
	(void *) setWirelessChannel,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	0,
	1,
	(char *) 0
};

static char Pgwireless_network_config_Item_68[] =
	C_xTD C_xTR C_oTR C_oTD C_NBSP C_xTD C_xTR "<!-- WPA -->\n";

static char Pgwireless_network_config_Item_71[] =
	C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">Authentication:" C_xTD "<td" C_WIDTH
	"\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

static char Pgwireless_network_config_Item_72[] =
	" onChange=\"updatePageState()\"";

extern rpOneOfSeveral getWirelessAuthentication(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessAuthentication(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
extern rpOption_OneSelect Pgwireless_network_config_Item_73_Option_1;
extern char Pgwireless_network_config_Item_72[];
static rpFixedSingleSelectFormItem Pgwireless_network_config_Item_73 = {
	"auth",
	&Pgwireless_network_config_Item_73_Option_1,
	(void *) getWirelessAuthentication,
	(void *) setWirelessAuthentication,
	eRpVarType_Complex,
	eRpVarType_Complex,
	1,
	(char *) Pgwireless_network_config_Item_72
};

extern rpOption_OneSelect Pgwireless_network_config_Item_73_Option_2;
rpOption_OneSelect Pgwireless_network_config_Item_73_Option_1 = {
	&Pgwireless_network_config_Item_73_Option_2,
	"Any available authentication",
	0
};

extern rpOption_OneSelect Pgwireless_network_config_Item_73_Option_3;
rpOption_OneSelect Pgwireless_network_config_Item_73_Option_2 = {
	&Pgwireless_network_config_Item_73_Option_3,
	"Open System",
	1
};

extern rpOption_OneSelect Pgwireless_network_config_Item_73_Option_4;
rpOption_OneSelect Pgwireless_network_config_Item_73_Option_3 = {
	&Pgwireless_network_config_Item_73_Option_4,
	"Shared Key",
	2
};

extern rpOption_OneSelect Pgwireless_network_config_Item_73_Option_5;
rpOption_OneSelect Pgwireless_network_config_Item_73_Option_4 = {
	&Pgwireless_network_config_Item_73_Option_5,
	"WEP (802.1x)",
	3
};

extern rpOption_OneSelect Pgwireless_network_config_Item_73_Option_6;
rpOption_OneSelect Pgwireless_network_config_Item_73_Option_5 = {
	&Pgwireless_network_config_Item_73_Option_6,
	"WPA Personal (WPA-PSK)",
	4
};

extern rpOption_OneSelect Pgwireless_network_config_Item_73_Option_7;
rpOption_OneSelect Pgwireless_network_config_Item_73_Option_6 = {
	&Pgwireless_network_config_Item_73_Option_7,
	"WPA Enterprise (802.1x)",
	5
};

rpOption_OneSelect Pgwireless_network_config_Item_73_Option_7 = {
	(rpOption_OneSelectPtr) 0,
	"Cisco LEAP",
	6
};

static char Pgwireless_network_config_Item_74[] =
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">Data Encryption:" C_xTD "<td"
	C_WIDTH "\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

static char Pgwireless_network_config_Item_75[] =
	" onChange=\"updatePageState()\"";

extern rpOneOfSeveral getWirelessEncryption(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessEncryption(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
extern rpOption_OneSelect Pgwireless_network_config_Item_76_Option_1;
extern char Pgwireless_network_config_Item_75[];
static rpFixedSingleSelectFormItem Pgwireless_network_config_Item_76 = {
	"encryp",
	&Pgwireless_network_config_Item_76_Option_1,
	(void *) getWirelessEncryption,
	(void *) setWirelessEncryption,
	eRpVarType_Complex,
	eRpVarType_Complex,
	1,
	(char *) Pgwireless_network_config_Item_75
};

extern rpOption_OneSelect Pgwireless_network_config_Item_76_Option_2;
rpOption_OneSelect Pgwireless_network_config_Item_76_Option_1 = {
	&Pgwireless_network_config_Item_76_Option_2,
	"Any",
	0
};

extern rpOption_OneSelect Pgwireless_network_config_Item_76_Option_3;
rpOption_OneSelect Pgwireless_network_config_Item_76_Option_2 = {
	&Pgwireless_network_config_Item_76_Option_3,
	"Open (no encryption)",
	1
};

extern rpOption_OneSelect Pgwireless_network_config_Item_76_Option_4;
rpOption_OneSelect Pgwireless_network_config_Item_76_Option_3 = {
	&Pgwireless_network_config_Item_76_Option_4,
	"WEP",
	2
};

extern rpOption_OneSelect Pgwireless_network_config_Item_76_Option_5;
rpOption_OneSelect Pgwireless_network_config_Item_76_Option_4 = {
	&Pgwireless_network_config_Item_76_Option_5,
	"TKIP",
	3
};

rpOption_OneSelect Pgwireless_network_config_Item_76_Option_5 = {
	(rpOption_OneSelectPtr) 0,
	"CCMP (AES)",
	4
};

static char Pgwireless_network_config_Item_77[] =
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">Passphrase:" C_xTD "<td" C_WIDTH
	"\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

extern char *getWPAPassphrase(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWPAPassphrase(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_network_config_Item_78 = {
	"wpaPassphrase",
	(void *) getWPAPassphrase,
	(void *) setWPAPassphrase,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	40,
	63,
	(char *) 0
};

static char Pgwireless_network_config_Item_79[] =
	C_NBSP "(8-63 characters)\n"
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">Wireless Login :" C_xTD "<td"
	C_WIDTH "\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

extern char *getWirelessLogin(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessLogin(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_network_config_Item_80 = {
	"wLogin",
	(void *) getWirelessLogin,
	(void *) setWirelessLogin,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	32,
	32,
	(char *) 0
};

static char Pgwireless_network_config_Item_81[] =
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">Wireless Password :" C_xTD "<td"
	C_WIDTH "\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

extern char *getWirelessPassword(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessPassword(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_network_config_Item_82 = {
	"wPassword",
	(void *) getWirelessPassword,
	(void *) setWirelessPassword,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	32,
	32,
	(char *) 0
};

static char Pgwireless_network_config_Item_83[] =
	C_xTD C_xTR C_oTR C_oTD C_NBSP C_xTD
	C_xTR;

static rpItem Pgwireless_network_config_Item_70[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_71 }, 
	{ eRpItemType_FormFixedSingleSelect, (void *) &Pgwireless_network_config_Item_73 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_74 }, 
	{ eRpItemType_FormFixedSingleSelect, (void *) &Pgwireless_network_config_Item_76 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_77 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_network_config_Item_78 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_79 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_network_config_Item_80 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_81 }, 
	{ eRpItemType_FormPasswordText, (void *) &Pgwireless_network_config_Item_82 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_83 }, 
	{ eRpItemType_LastItemInList } 
};

static char Pgwireless_network_config_Item_85[] =
	"<!-- WPA not supported -->\n";

static rpItem Pgwireless_network_config_Item_84[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_85 }, 
	{ eRpItemType_LastItemInList } 
};

static rpItem Pgwireless_network_config_Item_69_Group[] = { 
	{ eRpItemType_ItemGroup, (void *) &Pgwireless_network_config_Item_70 }, 
	{ eRpItemType_ItemGroup, (void *) &Pgwireless_network_config_Item_84 }, 
	{ eRpItemType_LastItemInList } 
};

extern Unsigned8 IS_WPA_SUPPORTED(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpDynamicDisplayItem Pgwireless_network_config_Item_69 = {
	(void *) IS_WPA_SUPPORTED,
	eRpVarType_Complex,
	2,
	Pgwireless_network_config_Item_69_Group
};

static char Pgwireless_network_config_Item_86[] =
	"<!-- End WPA -->\n"
	C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">WEP Encryption:" C_xTD "<td" C_WIDTH
	"\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

static char Pgwireless_network_config_Item_87[] =
	" onChange=\"updatePageState()\"";

extern rpOneOfSeveral getWirelessWEPEncryption(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessWEPEncryption(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
extern rpOption_OneSelect Pgwireless_network_config_Item_88_Option_1;
extern char Pgwireless_network_config_Item_87[];
static rpFixedSingleSelectFormItem Pgwireless_network_config_Item_88 = {
	"wep_encrypt",
	&Pgwireless_network_config_Item_88_Option_1,
	(void *) getWirelessWEPEncryption,
	(void *) setWirelessWEPEncryption,
	eRpVarType_Complex,
	eRpVarType_Complex,
	1,
	(char *) Pgwireless_network_config_Item_87
};

extern rpOption_OneSelect Pgwireless_network_config_Item_88_Option_2;
rpOption_OneSelect Pgwireless_network_config_Item_88_Option_1 = {
	&Pgwireless_network_config_Item_88_Option_2,
	"None",
	0
};

extern rpOption_OneSelect Pgwireless_network_config_Item_88_Option_3;
rpOption_OneSelect Pgwireless_network_config_Item_88_Option_2 = {
	&Pgwireless_network_config_Item_88_Option_3,
	"64-bit",
	5
};

rpOption_OneSelect Pgwireless_network_config_Item_88_Option_3 = {
	(rpOption_OneSelectPtr) 0,
	"128-bit",
	13
};

static char Pgwireless_network_config_Item_89[] =
	C_NBSP C_NBSP "\n"
	"<span id=\"dynamic_wep_key_label\">" C_NBSP "</span>\n"
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">Default WEP Key Index:" C_xTD "<td"
	C_WIDTH "\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

extern rpOneOfSeveral getDefaultWEPKey(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setDefaultWEPKey(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
extern rpOption_OneSelect Pgwireless_network_config_Item_90_Option_1;
static rpFixedSingleSelectFormItem Pgwireless_network_config_Item_90 = {
	"defaultWEPKey",
	&Pgwireless_network_config_Item_90_Option_1,
	(void *) getDefaultWEPKey,
	(void *) setDefaultWEPKey,
	eRpVarType_Complex,
	eRpVarType_Complex,
	1,
	(char *) 0
};

extern rpOption_OneSelect Pgwireless_network_config_Item_90_Option_2;
rpOption_OneSelect Pgwireless_network_config_Item_90_Option_1 = {
	&Pgwireless_network_config_Item_90_Option_2,
	"1",
	0
};

extern rpOption_OneSelect Pgwireless_network_config_Item_90_Option_3;
rpOption_OneSelect Pgwireless_network_config_Item_90_Option_2 = {
	&Pgwireless_network_config_Item_90_Option_3,
	"2",
	1
};

extern rpOption_OneSelect Pgwireless_network_config_Item_90_Option_4;
rpOption_OneSelect Pgwireless_network_config_Item_90_Option_3 = {
	&Pgwireless_network_config_Item_90_Option_4,
	"3",
	2
};

rpOption_OneSelect Pgwireless_network_config_Item_90_Option_4 = {
	(rpOption_OneSelectPtr) 0,
	"4",
	3
};

static char Pgwireless_network_config_Item_91[] =
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">WEP key 1:" C_xTD "<td" C_WIDTH
	"\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

extern char *getWEPKey0(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWEPKey0(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_network_config_Item_92 = {
	"wepKey0",
	(void *) getWEPKey0,
	(void *) setWEPKey0,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	26,
	26,
	(char *) 0
};

static char Pgwireless_network_config_Item_93[] =
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">WEP key 2:" C_xTD "<td" C_WIDTH
	"\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

extern char *getWEPKey1(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWEPKey1(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_network_config_Item_94 = {
	"wepKey1",
	(void *) getWEPKey1,
	(void *) setWEPKey1,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	26,
	26,
	(char *) 0
};

static char Pgwireless_network_config_Item_95[] =
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">WEP key 3:" C_xTD "<td" C_WIDTH
	"\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

extern char *getWEPKey2(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWEPKey2(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_network_config_Item_96 = {
	"wepKey2",
	(void *) getWEPKey2,
	(void *) setWEPKey2,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	26,
	26,
	(char *) 0
};

static char Pgwireless_network_config_Item_97[] =
	C_xTD C_xTR C_oTR "\n"
	"<td nowrap=\"\"" C_ALIGN_RIGHT ">WEP key 4:" C_xTD "<td" C_WIDTH
	"\"10\">" C_NBSP C_xTD "<td nowrap=\"\">\n";

extern char *getWEPKey3(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWEPKey3(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_network_config_Item_98 = {
	"wepKey3",
	(void *) getWEPKey3,
	(void *) setWEPKey3,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	26,
	26,
	(char *) 0
};

static char Pgwireless_network_config_Item_99[] =
	C_xTD C_xTR C_xTABLE C_oBR "<hr class=\"SEPARATORLINE\">\n"
	C_oTABLE_CELLSPACING "\"0\"" C_CELLPADDING "\"0\"" C_BORDER "\"0\">\n"
	C_oTR "\n"
	C_oTD;

static rpButtonFormItem Pgwireless_network_config_Item_100 = {
	"Apply",
	(char *) 0
};

static char Pgwireless_network_config_Item_101[] =
	C_xTD C_xTR C_xTABLE C_xFORM C_xTD C_xTR C_xTABLE C_xTD C_xTR C_xTABLE
	C_xTD C_xTR
	C_xTABLE;

extern char htmlCopyright[];

static char Pgwireless_network_config_Item_103[] =
	C_xBODY;

extern rpObjectDescription Pgwireless_network_config_Form_1;
static rpItem Pgwireless_network_config_Item_6[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_7 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_8 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_9 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_10 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_11 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_12 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_13 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_14 }, 
	{ eRpItemType_DynamicDisplay, (void *) &Pgwireless_network_config_Item_15 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_36 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlLogo }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_38 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_40 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlStatusMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_42 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlErrorMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_44 }, 
	{ eRpItemType_ItemGroup, (void *) &htmlMenu }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_46 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_47 }, 
	{ eRpItemType_FormHeader, (void *) &Pgwireless_network_config_Form_1 }, 
	{ eRpItemType_FormHiddenText, (void *) &Pgwireless_network_config_Item_50 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_51 }, 
	{ eRpItemType_FormFixedSingleSelect, (void *) &Pgwireless_network_config_Item_53 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_54 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_network_config_Item_55 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_56 }, 
	{ eRpItemType_FormVarValueSingleDyn, (void *) &Pgwireless_network_config_Item_58 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_59 }, 
	{ eRpItemType_FormVarValueSingleDyn, (void *) &Pgwireless_network_config_Item_60 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_61 }, 
	{ eRpItemType_DynamicDisplay, (void *) &Pgwireless_network_config_Item_62 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_66 }, 
	{ eRpItemType_FormVarValueSingleDyn, (void *) &Pgwireless_network_config_Item_67 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_68 }, 
	{ eRpItemType_DynamicDisplay, (void *) &Pgwireless_network_config_Item_69 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_86 }, 
	{ eRpItemType_FormFixedSingleSelect, (void *) &Pgwireless_network_config_Item_88 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_89 }, 
	{ eRpItemType_FormFixedSingleSelect, (void *) &Pgwireless_network_config_Item_90 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_91 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_network_config_Item_92 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_93 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_network_config_Item_94 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_95 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_network_config_Item_96 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_97 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_network_config_Item_98 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_99 }, 
	{ eRpItemType_FormSubmit, (void *) &Pgwireless_network_config_Item_100 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_101 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlCopyright }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_103 }, 
	{ eRpItemType_LastItemInList } 
};

static char Pgwireless_network_config_Item_105[] =
	C_oBODY ">\n"
	C_oP "Wireless not supported" C_xP
	C_xBODY;

static rpItem Pgwireless_network_config_Item_104[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_105 }, 
	{ eRpItemType_LastItemInList } 
};

static rpItem Pgwireless_network_config_Item_5_Group[] = { 
	{ eRpItemType_ItemGroup, (void *) &Pgwireless_network_config_Item_6 }, 
	{ eRpItemType_ItemGroup, (void *) &Pgwireless_network_config_Item_104 }, 
	{ eRpItemType_LastItemInList } 
};

extern Unsigned8 IS_WIRELESS_SUPPORTED(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpDynamicDisplayItem Pgwireless_network_config_Item_5 = {
	(void *) IS_WIRELESS_SUPPORTED,
	eRpVarType_Complex,
	2,
	Pgwireless_network_config_Item_5_Group
};

static char Pgwireless_network_config_Item_106[] =
	C_xHTML;



static rpItem Pgwireless_network_config_Form_1_Items[] = { 
	{ eRpItemType_FormHiddenText, (void *) &Pgwireless_network_config_Item_50 }, 
	{ eRpItemType_FormFixedSingleSelect, (void *) &Pgwireless_network_config_Item_53 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_network_config_Item_55 }, 
	{ eRpItemType_FormVarValueSingleDyn, (void *) &Pgwireless_network_config_Item_58 }, 
	{ eRpItemType_FormVarValueSingleDyn, (void *) &Pgwireless_network_config_Item_60 }, 
	{ eRpItemType_FormVarValueSingleDyn, (void *) &Pgwireless_network_config_Item_65 }, 
	{ eRpItemType_FormVarValueSingleDyn, (void *) &Pgwireless_network_config_Item_67 }, 
	{ eRpItemType_FormFixedSingleSelect, (void *) &Pgwireless_network_config_Item_73 }, 
	{ eRpItemType_FormFixedSingleSelect, (void *) &Pgwireless_network_config_Item_76 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_network_config_Item_78 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_network_config_Item_80 }, 
	{ eRpItemType_FormPasswordText, (void *) &Pgwireless_network_config_Item_82 }, 
	{ eRpItemType_FormFixedSingleSelect, (void *) &Pgwireless_network_config_Item_88 }, 
	{ eRpItemType_FormFixedSingleSelect, (void *) &Pgwireless_network_config_Item_90 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_network_config_Item_92 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_network_config_Item_94 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_network_config_Item_96 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_network_config_Item_98 }, 
	{ eRpItemType_FormSubmit, (void *) &Pgwireless_network_config_Item_100 }, 
	{ eRpItemType_LastItemInList } 
};

extern void wirelessNetworkSubmit(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpObjectExtension Pgwireless_network_config_Form_1_ObjectExtension = {
	wirelessNetworkSubmit,
	&Pgwireless_network_config,
	(rpObjectDescriptionPtr) 0,
	0,
	kRpObjFlags_None,
	(char *) Pgwireless_network_config_Item_48
};

rpObjectDescription Pgwireless_network_config_Form_1 = {
	"/Forms/wireless_network_config_1",
	Pgwireless_network_config_Form_1_Items,
	&Pgwireless_network_config_Form_1_ObjectExtension,
	(Unsigned32) 0,
	kRpPageAccess_Unprotected,
	eRpDataTypeForm,
	eRpObjectTypeDynamic
};

static rpItem Pgwireless_network_config_Items[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_2 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_4 }, 
	{ eRpItemType_DynamicDisplay, (void *) &Pgwireless_network_config_Item_5 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_network_config_Item_106 }, 
	{ eRpItemType_LastItemInList } 
};


extern void wirelessNetworkConfigPagePreProc(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpObjectExtension Pgwireless_network_config_ObjectExtension = {
	wirelessNetworkConfigPagePreProc,
	(rpObjectDescriptionPtr) 0,
	(rpObjectDescriptionPtr) 0,
	0,
	kRpObjFlags_None,
	(char *) 0
};

rpObjectDescription Pgwireless_network_config = {
	"/wireless_network_config.htm",
	Pgwireless_network_config_Items,
	&Pgwireless_network_config_ObjectExtension,
	(Unsigned32) 0,
	kRpPageAccess_Realm1,
	eRpDataTypeHtml,
	eRpObjectTypeDynamic
};

#endif	/* RomPagerServer */
