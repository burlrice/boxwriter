/* Created with PageBuilder version 4.04 on Tue Jun  8 10:07:20 2010 */

#include "AsExtern.h"

#if RomPagerServer



/* ************************************************** */
/* *    Built from "html\wireless_ip_config.htm"    * */
/* ************************************************** */

extern rpObjectDescription Pgwireless_ip_config;

static char Pgwireless_ip_config_Item_1[] =
	C_oHTML;

static char Pgwireless_ip_config_Item_2[] =
	C_oHEAD "\n"
	C_oMETA " http-equiv=\"Content-Type\"" C_CONTENT "\"text/html; "
	"charset=UTF-8\">\n"
	C_oTITLE "\n";

extern rpTextDisplayItem htmlProductName;

static char Pgwireless_ip_config_Item_4[] =
	C_xTITLE C_oMETA C_CONTENT "\"text/html; charset=windows-1252\" "
	"http-equiv=\"Content-Type\">\n"
	"<link type=\"text/css\" href=\"/stylesheet.css\" rel=\"stylesheet\">\n";

static char Pgwireless_ip_config_Item_7[] =
	"<script src=\"/utils.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/errhandler.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/validation.js\" language=\"JavaScript\"></script>\n"
	"<script language=\"JavaScript\">\n"
	"<!--\n"
	"function setWirelessDhcpState() {\n"
	"if (document.wireless_form.rbWDHCP[0].checked) {\n"
	"document.wireless_form.ip_addr.disabled = true;\n"
	"document.wireless_form.submask.disabled = true;\n"
	"document.wireless_form.gw_addr.disabled = true;\n"
	"document.wireless_form.pdns_addr.disabled = true;\n";

static char Pgwireless_ip_config_Item_8[] =
	"document.wireless_form.sdns_addr.disabled = true;\n"
	"} else {\n"
	"document.wireless_form.ip_addr.disabled = false;\n"
	"document.wireless_form.submask.disabled = false;\n"
	"document.wireless_form.gw_addr.disabled = false;\n"
	"document.wireless_form.pdns_addr.disabled = false;\n"
	"document.wireless_form.sdns_addr.disabled = false;\n"
	"document.wireless_form.ip_addr.focus();\n"
	"}\n"
	"}\n"
	"var err = new ErrorHandler();\n"
	"function doWValidate() {\n"
	"err.clearErrors(); // reset\n"
	"var ip = document.wireless_form.ip_addr;\n";

static char Pgwireless_ip_config_Item_9[] =
	"var submask = document.wireless_form.submask;\n"
	"var gw = document.wireless_form.gw_addr;\n"
	"var pDNS = document.wireless_form.pdns_addr;\n"
	"var sDNS = document.wireless_form.sdns_addr;\n"
	"var prefix = document.wireless_form.prefix_len;\n"
	"// Trim values before validation.\n"
	"ip.value = trim(ip.value);\n"
	"submask.value = trim(submask.value);\n"
	"gw.value = trim(gw.value);\n"
	"pDNS.value = trim(pDNS.value);\n"
	"sDNS.value = trim(sDNS.value);\n"
	"// name server is optional. Set to default value\n"
	"// if left blank.\n";

static char Pgwireless_ip_config_Item_10[] =
	"if (gw.value.length == 0)\n"
	"gw.value = \'0.0.0.0\';\n"
	"// If DHCP is enabled make sure the other ip settings are at least\n"
	"// of valid form to avoid ROMPager parsing errors. They should get\n"
	"// tossed anyway since DHCP will aquire the values.\n"
	"if (document.wireless_form.rbWDHCP[0].checked) {\n"
	"if (!isValidIP(ip.value))\n"
	"ip.value = \'0.0.0.0\';\n"
	"if (!isDottedForm(submask.value))\n"
	"submask.value = \'0.0.0.0\';\n"
	"if (!isValidIP(gw.value))\n"
	"gw.value = \'0.0.0.0\';\n"
	"if (!isValidIP(pDNS.value))\n"
	"pDNS.value = \'0.0.0.0\';\n";

static char Pgwireless_ip_config_Item_11[] =
	"if (!isValidIP(sDNS.value))\n"
	"sDNS.value = \'0.0.0.0\';\n"
	"} else {\n"
	"if (!isValidIP(ip.value))\n"
	"err.addError(ip, \'Invalid or missing IP address.\');\n"
	"if (pDNS.value != \'0.0.0.0\' && !isValidIP(pDNS.value))\n"
	"err.addError(pDNS, \'Invalid or missing Primary DNS address.\');\n"
	"if (sDNS.value != \'0.0.0.0\' && !isValidIP(sDNS.value))\n"
	"err.addError(sDNS, \'Invalid or missing Secondary DNS address.\');\n"
	"if (!isDottedForm(submask.value))\n"
	"err.addError(submask, \'Invalid or missing subnet mask.\');\n";

static char Pgwireless_ip_config_Item_12[] =
	"if (gw.value != \'0.0.0.0\' && !isValidIP(gw.value))\n"
	"err.addError(gw, \'Invalid or missing default gateway address.\');\n"
	"else if (!isValidGateway(gw.value))\n"
	"err.addError(gw, \'Invalid gateway address can not be 0.0.0.0.\');\n"
	"}\n"
	"if (document.wireless_form.wIpv6Static.checked) {\n"
	"if (!isInRange(prefix.value, 0, 128)) {\n"
	"err.addError(prefix, \'Invalid Prefix Length - enter value between 0 "
	"to 128.\');\n"
	"}\n"
	"}\n"
	"err.showError();\n"
	"return !err.hasError();\n"
	"}\n"
	"function setWirelessIpv6StaticState() {\n";

static char Pgwireless_ip_config_Item_13[] =
	"if (document.wireless_form.wIpv6Static.checked) {\n"
	"document.wireless_form.ipv6_addr.disabled = false;\n"
	"document.wireless_form.prefix_len.disabled = false;\n"
	"document.wireless_form.ipv6_addr.focus();\n"
	"} else {\n"
	"document.wireless_form.ipv6_addr.disabled = true;\n"
	"document.wireless_form.prefix_len.disabled = true;\n"
	"}\n"
	"}\n"
	"function setWirelessNetworState() {\n"
	"setWirelessDhcpState();\n"
	"setWirelessIpv6StaticState();\n"
	"}\n"
	"//-->\n"
	"</script>\n"
	C_xHEAD C_oBODY " onunload=\"\" onload=\"setWirelessNetworState();\">\n";

static char Pgwireless_ip_config_Item_14[] =
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"4\""
	C_CELLSPACING "\"0\">\n"
	"<tr" C_BGCOLOR "ffffff\">\n"
	C_oTD;

extern char htmlLogo[];

static char Pgwireless_ip_config_Item_16[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<td nowrap=\"\"" C_WIDTH "\"100%\"" C_VALIGN_MIDDLE " "
	"class=\"PAGEHEADER\">\n";

extern rpTextDisplayItem htmlProductName;

static char Pgwireless_ip_config_Item_18[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- Messages -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	"<!-- STATUS MESSAGES -->\n";

extern rpRepeatGroupDynItem htmlStatusMsgs;

static char Pgwireless_ip_config_Item_20[] =
	"<!-- ERROR MESSAGES -->\n";

extern rpRepeatGroupDynItem htmlErrorMsgs;

static char Pgwireless_ip_config_Item_22[] =
	C_xTD C_xTR C_oTR "\n"
	"<!-- MENU -->\n"
	"<td" C_VALIGN_TOP " nowrap=\"\">\n";

extern rpItem htmlMenu[];

static char Pgwireless_ip_config_Item_24[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- PAGE CONTENT -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"10\""
	C_CELLSPACING "\"0\" class=\"BORDER\">\n"
	C_oTR "\n"
	"<td" C_WIDTH "\"100%\" class=\"PAGECONTENT\">\n"
	"<h1 class=\"CONTENTHEADER\">Wireless TCP/IP Settings" C_xH1 "<!-- Tabs"
	" -->\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"0\""
	C_CELLSPACING "\"0\">\n"
	C_oTR "\n"
	"<td class=\"PAGETAB\" nowrap=\"true\"" C_WIDTH "\"10\">" C_NBSP C_xTD
	"<!-- left fill -->\n"
	"<td class=\"PAGETAB\" nowrap=\"true\">" C_oANCHOR_HREF;

static char Pgwireless_ip_config_Item_25[] =
	"/wireless_network_config.htm\">Wireless Network" C_xANCHOR_xTD "<td "
	"class=\"PAGETAB\" nowrap=\"true\">|" C_xTD "<!-- separator -->\n"
	"<td class=\"PAGETAB-SELECTED\" nowrap=\"true\">TCP/IP" C_xTD "<td "
	"class=\"PAGETAB\"" C_WIDTH "\"100%\">" C_NBSP C_xTD "<!-- right fill "
	"-->\n"
	C_xTR C_xTABLE "<br/>\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"0\""
	C_CELLSPACING "\"0\">\n"
	C_oTR " " C_oTD C_oH2 "IP v4 Settings" C_xH2 C_xTD C_xTR C_oTR "\n"
	C_oTD;

static char Pgwireless_ip_config_Item_26[] =
	C_NAME "\"wireless_form\" onsubmit=\"return doWValidate();\"";

static char Pgwireless_ip_config_Item_28[] =
	C_oTABLE_CELLSPACING "\"0\"" C_CELLPADDING "\"2\"" C_BORDER "\"0\">\n";

extern rpOneOfSeveral getWirelessDhcp(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessDhcp(void *theTaskDataPtr, rpOneOfSeveral theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpRadioGroupInfo Pgwireless_ip_config_RadioGroup_1 = {
	"rbWDHCP",
	(void *) getWirelessDhcp,
	(void *) setWirelessDhcp,
	eRpVarType_Complex,
	eRpVarType_Complex
};

static char Pgwireless_ip_config_Item_29[] =
	C_oTR "\n"
	C_oTD;

static char Pgwireless_ip_config_Item_30[] =
	" onclick=\"setWirelessDhcpState();\"";

rpRadioButtonFormItem enableWirelessDHCP = {
	&Pgwireless_ip_config_RadioGroup_1,
	"on",
	0,
	(char *) Pgwireless_ip_config_Item_30
};

static char Pgwireless_ip_config_Item_32[] =
	C_NBSP "\n"
	"<label for=\"enableWirelessDHCP\">Obtain an IP address automatically"
	"</label>\n"
	C_xTD C_xTR C_oTR "\n"
	C_oTD;

static char Pgwireless_ip_config_Item_33[] =
	" onclick=\"setWirelessDhcpState();\"";

rpRadioButtonFormItem disableWirelessDHCP = {
	&Pgwireless_ip_config_RadioGroup_1,
	"off",
	1,
	(char *) Pgwireless_ip_config_Item_33
};

static char Pgwireless_ip_config_Item_35[] =
	C_NBSP "\n"
	"<label for=\"disableWirelessDHCP\">Use the following IP address"
	"</label>\n"
	C_xTD C_xTR C_xTABLE C_oTABLE_CELLSPACING "\"0\"" C_CELLPADDING "\"2\""
	C_BORDER "\"0\">\n"
	C_oTR "\n"
	"<td" C_WIDTH "\"40\">" C_NBSP C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">"
	"IP v4 Address:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD
	C_oTD;

extern char *getWirelessIpAddress(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessIPAddress(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_ip_config_Item_36 = {
	"ip_addr",
	(void *) getWirelessIpAddress,
	(void *) setWirelessIPAddress,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_DotForm,
	15,
	15,
	(char *) 0
};

static char Pgwireless_ip_config_Item_37[] =
	C_xTD C_xTR C_oTR "\n"
	"<td" C_WIDTH "\"40\">" C_NBSP C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">"
	"Subnet Mask:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD
	C_oTD;

extern char *getWirelessSubMask(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessSubMask(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_ip_config_Item_38 = {
	"submask",
	(void *) getWirelessSubMask,
	(void *) setWirelessSubMask,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_DotForm,
	15,
	15,
	(char *) 0
};

static char Pgwireless_ip_config_Item_39[] =
	C_xTD C_xTR C_oTR "\n"
	"<td" C_WIDTH "\"40\">" C_NBSP C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">"
	"Default Gateway:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD
	C_oTD;

extern char *getWirelessGateway(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessGateway(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_ip_config_Item_40 = {
	"gw_addr",
	(void *) getWirelessGateway,
	(void *) setWirelessGateway,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_DotForm,
	15,
	15,
	(char *) 0
};

static char Pgwireless_ip_config_Item_41[] =
	C_xTD C_xTR C_oTR "\n"
	"<td" C_WIDTH "\"40\">" C_NBSP C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">"
	"Primary DNS:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD
	C_oTD;

extern char *getWirelessPrimaryDNS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessPrimaryDNS(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_ip_config_Item_42 = {
	"pdns_addr",
	(void *) getWirelessPrimaryDNS,
	(void *) setWirelessPrimaryDNS,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_DotForm,
	15,
	15,
	(char *) 0
};

static char Pgwireless_ip_config_Item_43[] =
	C_xTD C_xTR C_oTR "\n"
	"<td" C_WIDTH "\"40\">" C_NBSP C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">"
	"Secondary DNS:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD
	C_oTD;

extern char *getWirelessSecondaryDNS(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessSecondaryDNS(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_ip_config_Item_44 = {
	"sdns_addr",
	(void *) getWirelessSecondaryDNS,
	(void *) setWirelessSecondaryDNS,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_DotForm,
	15,
	15,
	(char *) 0
};

static char Pgwireless_ip_config_Item_45[] =
	C_xTD C_xTR C_xTABLE C_oBR C_xTD C_xTR C_oTR " " C_oTD C_oH2 "IP v6 "
	"Settings" C_xH2 C_xTD C_xTR C_oTR " " C_oTD C_oTABLE_CELLSPACING "\"0\""
	C_CELLPADDING "\"2\"" C_BORDER "\"0\">\n"
	C_oTR "\n"
	C_oTD;

extern Boolean getWirelessIpv6Dhcp(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessIpv6Dhcp(void *theTaskDataPtr, Boolean theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpCheckboxFormItem Pgwireless_ip_config_Item_46 = {
	"ipv6DHCP",
	(void *) getWirelessIpv6Dhcp,
	(void *) setWirelessIpv6Dhcp,
	eRpVarType_Complex,
	eRpVarType_Complex,
	(char *) 0
};

static char Pgwireless_ip_config_Item_47[] =
	C_NBSP "\n"
	"<label for=\"enableDHCPv6\">Enable DHCP v6</label>\n"
	C_xTD C_xTR C_oTR "\n"
	C_oTD;

static char Pgwireless_ip_config_Item_48[] =
	" onclick=\"setWirelessIpv6StaticState();\"";

extern Boolean getWirelessIpv6Static(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessIpv6Static(void *theTaskDataPtr, Boolean theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpCheckboxFormItem Pgwireless_ip_config_Item_49 = {
	"wIpv6Static",
	(void *) getWirelessIpv6Static,
	(void *) setWirelessIpv6Static,
	eRpVarType_Complex,
	eRpVarType_Complex,
	(char *) Pgwireless_ip_config_Item_48
};

static char Pgwireless_ip_config_Item_50[] =
	C_NBSP "\n"
	"<label for=\"enableStaticv6\">Use the following static IP v6 address"
	"</label>\n"
	C_xTD C_xTR C_xTABLE C_oTABLE_CELLSPACING "\"0\"" C_CELLPADDING "\"2\""
	C_BORDER "\"0\">\n"
	C_oTR "\n"
	"<td" C_WIDTH "\"40\">" C_NBSP C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">"
	"IP v6 Address:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD
	C_oTD;

extern char *getWirelessIpv6Address(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessIPv6Address(void *theTaskDataPtr, char *theValuePtr,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_ip_config_Item_51 = {
	"ipv6_addr",
	(void *) getWirelessIpv6Address,
	(void *) setWirelessIPv6Address,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_ASCII,
	32,
	32,
	(char *) 0
};

static char Pgwireless_ip_config_Item_52[] =
	C_xTD C_xTR C_oTR "\n"
	"<td" C_WIDTH "\"40\">" C_NBSP C_xTD "<td nowrap=\"\"" C_ALIGN_RIGHT ">"
	"Prefix Length:" C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD
	C_oTD;

extern Unsigned8 getWirelessPrefixLength(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
extern void setWirelessPrefixLength(void *theTaskDataPtr, Unsigned8 theValue,
		char *theNamePtr, Signed16Ptr theIndexValuesPtr);
static rpTextFormItem Pgwireless_ip_config_Item_53 = {
	"prefix_len",
	(void *) getWirelessPrefixLength,
	(void *) setWirelessPrefixLength,
	eRpVarType_Complex,
	eRpVarType_Complex,
	eRpTextType_Unsigned8,
	3,
	3,
	(char *) 0
};

static char Pgwireless_ip_config_Item_54[] =
	C_xTD C_xTR C_xTABLE C_oBR "<hr class=\"SEPARATORLINE\">\n"
	C_oTABLE_CELLSPACING "\"0\"" C_CELLPADDING "\"0\"" C_BORDER "\"0\">\n"
	C_oTR "\n"
	C_oTD;

static rpButtonFormItem Pgwireless_ip_config_Item_55 = {
	"Apply",
	(char *) 0
};

static char Pgwireless_ip_config_Item_56[] =
	C_xTD C_xTR C_xTABLE C_xFORM C_xTD C_xTR C_xTABLE C_xTD C_xTR C_xTABLE
	C_xTD C_xTR
	C_xTABLE;

extern char htmlCopyright[];

static char Pgwireless_ip_config_Item_58[] =
	C_xBODY;

extern rpObjectDescription Pgwireless_ip_config_Form_1;
static rpItem Pgwireless_ip_config_Item_6[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_7 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_8 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_9 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_10 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_11 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_12 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_13 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_14 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlLogo }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_16 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_18 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlStatusMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_20 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlErrorMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_22 }, 
	{ eRpItemType_ItemGroup, (void *) &htmlMenu }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_24 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_25 }, 
	{ eRpItemType_FormHeader, (void *) &Pgwireless_ip_config_Form_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_28 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_29 }, 
	{ eRpItemType_FormRadioButton, (void *) &enableWirelessDHCP }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_32 }, 
	{ eRpItemType_FormRadioButton, (void *) &disableWirelessDHCP }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_35 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_ip_config_Item_36 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_37 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_ip_config_Item_38 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_39 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_ip_config_Item_40 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_41 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_ip_config_Item_42 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_43 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_ip_config_Item_44 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_45 }, 
	{ eRpItemType_FormCheckbox, (void *) &Pgwireless_ip_config_Item_46 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_47 }, 
	{ eRpItemType_FormCheckbox, (void *) &Pgwireless_ip_config_Item_49 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_50 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_ip_config_Item_51 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_52 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_ip_config_Item_53 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_54 }, 
	{ eRpItemType_FormSubmit, (void *) &Pgwireless_ip_config_Item_55 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_56 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlCopyright }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_58 }, 
	{ eRpItemType_LastItemInList } 
};

static char Pgwireless_ip_config_Item_60[] =
	C_oBODY ">\n"
	C_oP "Wireless not supported" C_xP
	C_xBODY;

static rpItem Pgwireless_ip_config_Item_59[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_60 }, 
	{ eRpItemType_LastItemInList } 
};

static rpItem Pgwireless_ip_config_Item_5_Group[] = { 
	{ eRpItemType_ItemGroup, (void *) &Pgwireless_ip_config_Item_6 }, 
	{ eRpItemType_ItemGroup, (void *) &Pgwireless_ip_config_Item_59 }, 
	{ eRpItemType_LastItemInList } 
};

extern Unsigned8 IS_WIRELESS_SUPPORTED(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpDynamicDisplayItem Pgwireless_ip_config_Item_5 = {
	(void *) IS_WIRELESS_SUPPORTED,
	eRpVarType_Complex,
	2,
	Pgwireless_ip_config_Item_5_Group
};

static char Pgwireless_ip_config_Item_61[] =
	C_xHTML;



static rpItem Pgwireless_ip_config_Form_1_Items[] = { 
	{ eRpItemType_FormRadioButton, (void *) &enableWirelessDHCP }, 
	{ eRpItemType_FormRadioButton, (void *) &disableWirelessDHCP }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_ip_config_Item_36 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_ip_config_Item_38 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_ip_config_Item_40 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_ip_config_Item_42 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_ip_config_Item_44 }, 
	{ eRpItemType_FormCheckbox, (void *) &Pgwireless_ip_config_Item_46 }, 
	{ eRpItemType_FormCheckbox, (void *) &Pgwireless_ip_config_Item_49 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_ip_config_Item_51 }, 
	{ eRpItemType_FormAsciiText, (void *) &Pgwireless_ip_config_Item_53 }, 
	{ eRpItemType_FormSubmit, (void *) &Pgwireless_ip_config_Item_55 }, 
	{ eRpItemType_LastItemInList } 
};

extern void wirelessSubmit(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpObjectExtension Pgwireless_ip_config_Form_1_ObjectExtension = {
	wirelessSubmit,
	&Pgwireless_ip_config,
	(rpObjectDescriptionPtr) 0,
	0,
	kRpObjFlags_None,
	(char *) Pgwireless_ip_config_Item_26
};

rpObjectDescription Pgwireless_ip_config_Form_1 = {
	"/Forms/wireless_ip_config_1",
	Pgwireless_ip_config_Form_1_Items,
	&Pgwireless_ip_config_Form_1_ObjectExtension,
	(Unsigned32) 0,
	kRpPageAccess_Unprotected,
	eRpDataTypeForm,
	eRpObjectTypeDynamic
};

static rpItem Pgwireless_ip_config_Items[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_2 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_4 }, 
	{ eRpItemType_DynamicDisplay, (void *) &Pgwireless_ip_config_Item_5 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgwireless_ip_config_Item_61 }, 
	{ eRpItemType_LastItemInList } 
};


extern void wirelessIpConfigPagePreProc(void *theTaskDataPtr, Signed16Ptr theIndexValuesPtr);
static rpObjectExtension Pgwireless_ip_config_ObjectExtension = {
	wirelessIpConfigPagePreProc,
	(rpObjectDescriptionPtr) 0,
	(rpObjectDescriptionPtr) 0,
	0,
	kRpObjFlags_None,
	(char *) 0
};

rpObjectDescription Pgwireless_ip_config = {
	"/wireless_ip_config.htm",
	Pgwireless_ip_config_Items,
	&Pgwireless_ip_config_ObjectExtension,
	(Unsigned32) 0,
	kRpPageAccess_Realm1,
	eRpDataTypeHtml,
	eRpObjectTypeDynamic
};

#endif	/* RomPagerServer */
