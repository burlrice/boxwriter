/* Created with PageBuilder version 4.04 on Tue Jun  8 10:07:20 2010 */

#include "AsExtern.h"

#if RomPagerServer



/* ********************************************* */
/* *    Built from "html\reboot_status.htm"    * */
/* ********************************************* */

extern rpObjectDescription Pgreboot_status;

static char Pgreboot_status_Item_1[] =
	C_oHTML;

static char Pgreboot_status_Item_2[] =
	C_oHEAD "\n";

extern char *getRebootStatusMetaTag(void *theTaskDataPtr, char *theNamePtr,
		Signed16Ptr theIndexValuesPtr);
static rpTextDisplayItem Pgreboot_status_Item_3 = {
	(void *) getRebootStatusMetaTag,
	eRpVarType_Complex,
	eRpTextType_ASCII_Extended,
	20
};

static char Pgreboot_status_Item_4[] =
	C_oMETA " http-equiv=\"Content-Type\"" C_CONTENT "\"text/html; "
	"charset=UTF-8\">\n"
	C_oTITLE "\n";

extern rpTextDisplayItem htmlProductName;

static char Pgreboot_status_Item_6[] =
	C_xTITLE C_oMETA C_CONTENT "\"text/html; charset=windows-1252\" "
	"http-equiv=\"Content-Type\">\n"
	"<link type=\"text/css\" href=\"/stylesheet.css\" rel=\"stylesheet\">\n"
	"<script src=\"/utils.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/errhandler.js\" language=\"JavaScript\"></script>\n"
	"<script src=\"/validation.js\" language=\"JavaScript\"></script>\n"
	C_xHEAD C_oBODY " onunload=\"\" onload=\"\">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"4\""
	C_CELLSPACING "\"0\">\n"
	"<tr" C_BGCOLOR "ffffff\">\n"
	C_oTD;

extern char htmlLogo[];

static char Pgreboot_status_Item_8[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<td nowrap=\"\"" C_WIDTH "\"100%\"" C_VALIGN_MIDDLE " "
	"class=\"PAGEHEADER\">\n";

extern rpTextDisplayItem htmlProductName;

static char Pgreboot_status_Item_10[] =
	C_xTD C_xTR C_oTR "\n"
	C_oTD C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- Messages -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	"<!-- STATUS MESSAGES -->\n";

extern rpRepeatGroupDynItem htmlStatusMsgs;

static char Pgreboot_status_Item_12[] =
	"<!-- ERROR MESSAGES -->\n";

extern rpRepeatGroupDynItem htmlErrorMsgs;

static char Pgreboot_status_Item_14[] =
	C_xTD C_xTR C_oTR "\n"
	"<!-- Menu -->\n"
	"<td" C_VALIGN_TOP " nowrap=\"\">\n";

extern rpItem htmlMenu[];

static char Pgreboot_status_Item_16[] =
	C_xTD "<td" C_WIDTH "\"10\">" C_NBSP C_xTD "<!-- Spacer -->\n"
	"<!-- Page Content -->\n"
	"<td" C_VALIGN_TOP C_WIDTH "\"100%\">\n"
	C_oTABLE_BORDER "\"0\"" C_WIDTH "\"100%\"" C_CELLPADDING "\"10\""
	C_CELLSPACING "\"0\" class=\"BORDER\">\n"
	C_oTR "\n"
	"<td" C_WIDTH "\"100%\" class=\"PAGECONTENT\">\n"
	"<h1 class=\"CONTENTHEADER\">Rebooting..." C_xH1 C_oTABLE_BORDER "\"0\""
	C_WIDTH "\"100%\"" C_CELLPADDING "\"0\"" C_CELLSPACING "\"0\">\n"
	C_oTR "\n"
	C_oTD C_oP "Reboot is in progress. You will be reconnected in "
	"approximately 45 seconds.\n"
	C_oP "If you are not reconnected automatically it may mean that the IP ";

static char Pgreboot_status_Item_17[] =
	"address has changed.\n"
	"The new IP address is displayed on the serial port during start up. "
	"Type the new IP address\n"
	"in your browser\'s address bar to reconnect manually.\n"
	C_xTD C_xTR C_xTABLE C_xTD C_xTR C_xTABLE C_xTD C_xTR
	C_xTABLE;

extern char htmlCopyright[];

static char Pgreboot_status_Item_19[] =
	C_xBODY_xHTML;


static rpItem Pgreboot_status_Items[] = { 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_status_Item_1 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_status_Item_2 }, 
	{ eRpItemType_DisplayText, (void *) &Pgreboot_status_Item_3 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_status_Item_4 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_status_Item_6 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlLogo }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_status_Item_8 }, 
	{ eRpItemType_DisplayText, (void *) &htmlProductName }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_status_Item_10 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlStatusMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_status_Item_12 }, 
	{ eRpItemType_RepeatGroupWhile, (void *) &htmlErrorMsgs }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_status_Item_14 }, 
	{ eRpItemType_ItemGroup, (void *) &htmlMenu }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_status_Item_16 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_status_Item_17 }, 
	{ eRpItemType_DataZeroTerminated, (void *) &htmlCopyright }, 
	{ eRpItemType_DataZeroTerminated, (void *) &Pgreboot_status_Item_19 }, 
	{ eRpItemType_LastItemInList } 
};


#endif	/* RomPagerServer */
